<s>
Sluneční	sluneční	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
Slunce	slunce	k1gNnSc1
</s>
<s>
Sluneční	sluneční	k2eAgFnSc1d1
energie	energie	k1gFnSc1
(	(	kIx(
<g/>
sluneční	sluneční	k2eAgNnSc1d1
záření	záření	k1gNnSc1
<g/>
,	,	kIx,
solární	solární	k2eAgFnSc1d1
radiace	radiace	k1gFnSc1
<g/>
)	)	kIx)
představuje	představovat	k5eAaImIp3nS
drtivou	drtivý	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
energie	energie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
na	na	k7c6
Zemi	zem	k1gFnSc6
nachází	nacházet	k5eAaImIp3nS
a	a	k8xC
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniká	vznikat	k5eAaImIp3nS
jadernými	jaderný	k2eAgFnPc7d1
přeměnami	přeměna	k1gFnPc7
v	v	k7c6
nitru	nitro	k1gNnSc6
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
vyčerpání	vyčerpání	k1gNnSc1
zásob	zásoba	k1gFnPc2
vodíku	vodík	k1gInSc2
na	na	k7c6
Slunci	slunce	k1gNnSc6
je	být	k5eAaImIp3nS
očekáváno	očekávat	k5eAaImNgNnS
až	až	k9
v	v	k7c6
řádu	řád	k1gInSc6
miliard	miliarda	k4xCgFnPc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
označuje	označovat	k5eAaImIp3nS
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
zdroj	zdroj	k1gInSc1
energie	energie	k1gFnSc2
jako	jako	k8xS,k8xC
obnovitelný	obnovitelný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
ovšem	ovšem	k9
neznamená	znamenat	k5eNaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
zdroj	zdroj	k1gInSc4
bezemisní	bezemisní	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Projevy	projev	k1gInPc1
sluneční	sluneční	k2eAgFnSc2d1
energie	energie	k1gFnSc2
na	na	k7c6
Zemi	zem	k1gFnSc6
</s>
<s>
Podle	podle	k7c2
zákona	zákon	k1gInSc2
zachování	zachování	k1gNnSc2
energie	energie	k1gFnSc2
se	se	k3xPyFc4
sluneční	sluneční	k2eAgFnSc1d1
energie	energie	k1gFnSc1
<g/>
,	,	kIx,
dopadající	dopadající	k2eAgFnSc1d1
na	na	k7c4
planetu	planeta	k1gFnSc4
Zemi	zem	k1gFnSc4
<g/>
,	,	kIx,
přeměňuje	přeměňovat	k5eAaImIp3nS
beze	beze	k7c2
zbytku	zbytek	k1gInSc2
v	v	k7c4
jiné	jiný	k2eAgFnPc4d1
formy	forma	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
projevy	projev	k1gInPc7
sluneční	sluneční	k2eAgFnSc2d1
energie	energie	k1gFnSc2
na	na	k7c6
Zemi	zem	k1gFnSc6
tak	tak	k6eAd1
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Energie	energie	k1gFnSc1
fosilních	fosilní	k2eAgNnPc2d1
paliv	palivo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
dávné	dávný	k2eAgFnSc6d1
minulosti	minulost	k1gFnSc6
z	z	k7c2
rostlinné	rostlinný	k2eAgFnSc2d1
nebo	nebo	k8xC
živočišné	živočišný	k2eAgFnSc2d1
biomasy	biomasa	k1gFnSc2
</s>
<s>
uhlí	uhlí	k1gNnSc1
</s>
<s>
ropa	ropa	k1gFnSc1
</s>
<s>
zemní	zemní	k2eAgInSc1d1
plyn	plyn	k1gInSc1
</s>
<s>
Energie	energie	k1gFnSc1
větru	vítr	k1gInSc2
–	–	k?
lišící	lišící	k2eAgFnSc1d1
se	se	k3xPyFc4
intenzita	intenzita	k1gFnSc1
ohřevu	ohřev	k1gInSc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
částí	část	k1gFnPc2
planety	planeta	k1gFnSc2
vyvolává	vyvolávat	k5eAaImIp3nS
větrné	větrný	k2eAgNnSc1d1
proudění	proudění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítr	vítr	k1gInSc1
může	moct	k5eAaImIp3nS
navíc	navíc	k6eAd1
vyvolávat	vyvolávat	k5eAaImF
na	na	k7c6
vodní	vodní	k2eAgFnSc6d1
hladině	hladina	k1gFnSc6
vznik	vznik	k1gInSc1
vln	vlna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Energie	energie	k1gFnSc1
biomasy	biomasa	k1gFnSc2
<g/>
,	,	kIx,
vzniklá	vzniklý	k2eAgFnSc1d1
přeměnou	přeměna	k1gFnSc7
sluneční	sluneční	k2eAgFnSc2d1
energie	energie	k1gFnSc2
na	na	k7c4
energii	energie	k1gFnSc4
chemických	chemický	k2eAgFnPc2d1
vazeb	vazba	k1gFnPc2
v	v	k7c6
organických	organický	k2eAgFnPc6d1
sloučeninách	sloučenina	k1gFnPc6
fotosyntézou	fotosyntéza	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sem	sem	k6eAd1
patří	patřit	k5eAaImIp3nS
nejen	nejen	k6eAd1
energetické	energetický	k2eAgNnSc1d1
využití	využití	k1gNnSc1
biomasy	biomasa	k1gFnSc2
při	při	k7c6
spalování	spalování	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
potravní	potravní	k2eAgNnSc1d1
využití	využití	k1gNnSc1
živočichy	živočich	k1gMnPc7
(	(	kIx(
<g/>
konzumenty	konzument	k1gMnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1
energie	energie	k1gFnPc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
sluneční	sluneční	k2eAgFnSc2d1
energie	energie	k1gFnSc2
představuje	představovat	k5eAaImIp3nS
hybnou	hybný	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
pro	pro	k7c4
koloběh	koloběh	k1gInSc4
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Teplo	teplo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
většinou	většinou	k6eAd1
projevem	projev	k1gInSc7
ztrát	ztráta	k1gFnPc2
při	při	k7c6
energetických	energetický	k2eAgFnPc6d1
přeměnách	přeměna	k1gFnPc6
</s>
<s>
Nepřeměněné	přeměněný	k2eNgNnSc1d1
elektromagnetické	elektromagnetický	k2eAgNnSc1d1
záření	záření	k1gNnSc1
Slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
o	o	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
bude	být	k5eAaImBp3nS
řeč	řeč	k1gFnSc1
v	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Sluneční	sluneční	k2eAgInSc1d1
vítr	vítr	k1gInSc1
–	–	k?
proud	proud	k1gInSc1
elementárních	elementární	k2eAgFnPc2d1
částic	částice	k1gFnPc2
a	a	k8xC
jader	jádro	k1gNnPc2
helia	helium	k1gNnSc2
ze	z	k7c2
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
projevy	projev	k1gInPc7
sluneční	sluneční	k2eAgFnSc2d1
energie	energie	k1gFnSc2
na	na	k7c6
Zemi	zem	k1gFnSc6
nepatří	patřit	k5eNaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Geotermální	geotermální	k2eAgFnSc1d1
energie	energie	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gInPc1
projevy	projev	k1gInPc1
(	(	kIx(
<g/>
tato	tento	k3xDgFnSc1
energie	energie	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
období	období	k1gNnSc2
vzniku	vznik	k1gInSc2
Země	zem	k1gFnSc2
a	a	k8xC
sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
–	–	k?
vzniká	vznikat	k5eAaImIp3nS
jaderným	jaderný	k2eAgInSc7d1
rozpadem	rozpad	k1gInSc7
a	a	k8xC
působením	působení	k1gNnSc7
slapových	slapový	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
termální	termální	k2eAgInPc1d1
prameny	pramen	k1gInPc1
</s>
<s>
projevy	projev	k1gInPc4
posunu	posun	k1gInSc2
litosférických	litosférický	k2eAgFnPc2d1
desek	deska	k1gFnPc2
–	–	k?
zemětřesení	zemětřesení	k1gNnSc1
<g/>
,	,	kIx,
sopečná	sopečný	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
<g/>
,	,	kIx,
vlny	vlna	k1gFnPc1
tsunami	tsunami	k1gNnSc1
</s>
<s>
teplotní	teplotní	k2eAgInSc1d1
ohřev	ohřev	k1gInSc1
hlouběji	hluboko	k6eAd2
položených	položený	k2eAgNnPc2d1
míst	místo	k1gNnPc2
(	(	kIx(
<g/>
v	v	k7c6
praxi	praxe	k1gFnSc6
je	být	k5eAaImIp3nS
využíván	využívat	k5eAaImNgInS,k5eAaPmNgInS
tepelnými	tepelný	k2eAgNnPc7d1
čerpadly	čerpadlo	k1gNnPc7
<g/>
)	)	kIx)
</s>
<s>
Energie	energie	k1gFnSc1
gravitačních	gravitační	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
,	,	kIx,
především	především	k6eAd1
kinetická	kinetický	k2eAgFnSc1d1
energie	energie	k1gFnSc1
soustavy	soustava	k1gFnSc2
Měsíc	měsíc	k1gInSc1
–	–	k?
Země	zem	k1gFnSc2
–	–	k?
Slunce	slunce	k1gNnSc1
<g/>
,	,	kIx,
projevující	projevující	k2eAgFnSc4d1
se	se	k3xPyFc4
jako	jako	k9
příliv	příliv	k1gInSc1
</s>
<s>
Energie	energie	k1gFnSc1
atomových	atomový	k2eAgNnPc2d1
jader	jádro	k1gNnPc2
<g/>
,	,	kIx,
vznikající	vznikající	k2eAgFnSc4d1
při	při	k7c6
radioaktivním	radioaktivní	k2eAgInSc6d1
rozpadu	rozpad	k1gInSc6
prvků	prvek	k1gInPc2
těžších	těžký	k2eAgNnPc2d2
než	než	k8xS
železo	železo	k1gNnSc4
nebo	nebo	k8xC
naopak	naopak	k6eAd1
slučování	slučování	k1gNnSc1
prvků	prvek	k1gInPc2
lehčích	lehký	k2eAgInPc2d2
</s>
<s>
Energie	energie	k1gFnSc1
kosmického	kosmický	k2eAgNnSc2d1
záření	záření	k1gNnSc2
<g/>
,	,	kIx,
pocházející	pocházející	k2eAgMnPc1d1
ze	z	k7c2
zdrojů	zdroj	k1gInPc2
mimo	mimo	k7c4
sluneční	sluneční	k2eAgFnSc4d1
soustavu	soustava	k1gFnSc4
</s>
<s>
Dopad	dopad	k1gInSc1
sluneční	sluneční	k2eAgFnSc2d1
energie	energie	k1gFnSc2
na	na	k7c6
Zemi	zem	k1gFnSc6
</s>
<s>
Solární	solární	k2eAgNnSc1d1
záření	záření	k1gNnSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
Solární	solární	k2eAgNnSc1d1
záření	záření	k1gNnSc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
Mapa	mapa	k1gFnSc1
intenzity	intenzita	k1gFnSc2
sluneční	sluneční	k2eAgFnSc2d1
energie	energie	k1gFnSc2
dopadající	dopadající	k2eAgFnSc2d1
na	na	k7c6
Zemi	zem	k1gFnSc6
</s>
<s>
Sluneční	sluneční	k2eAgFnSc1d1
energie	energie	k1gFnSc1
je	být	k5eAaImIp3nS
energií	energie	k1gFnSc7
elektromagnetického	elektromagnetický	k2eAgNnSc2d1
záření	záření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spektrum	spektrum	k1gNnSc1
slunečního	sluneční	k2eAgNnSc2d1
záření	záření	k1gNnSc2
lze	lze	k6eAd1
rozdělit	rozdělit	k5eAaPmF
na	na	k7c4
<g/>
:	:	kIx,
</s>
<s>
záření	záření	k1gNnSc1
ultrafialové	ultrafialový	k2eAgNnSc1d1
(	(	kIx(
<g/>
vlnová	vlnový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
pod	pod	k7c7
380	#num#	k4
nm	nm	k?
<g/>
)	)	kIx)
</s>
<s>
záření	záření	k1gNnSc1
viditelné	viditelný	k2eAgNnSc1d1
(	(	kIx(
<g/>
vlnová	vlnový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
380	#num#	k4
až	až	k9
780	#num#	k4
nm	nm	k?
<g/>
)	)	kIx)
</s>
<s>
záření	záření	k1gNnSc1
infračervené	infračervený	k2eAgNnSc1d1
(	(	kIx(
<g/>
vlnová	vlnový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
přes	přes	k7c4
780	#num#	k4
nm	nm	k?
<g/>
)	)	kIx)
</s>
<s>
Viditelné	viditelný	k2eAgNnSc1d1
záření	záření	k1gNnSc1
tvoří	tvořit	k5eAaImIp3nS
asi	asi	k9
45	#num#	k4
%	%	kIx~
dopadajícího	dopadající	k2eAgNnSc2d1
záření	záření	k1gNnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jeho	jeho	k3xOp3gInSc1
podíl	podíl	k1gInSc1
je	být	k5eAaImIp3nS
vyšší	vysoký	k2eAgMnSc1d2
při	při	k7c6
zatažené	zatažený	k2eAgFnSc6d1
obloze	obloha	k1gFnSc6
(	(	kIx(
<g/>
může	moct	k5eAaImIp3nS
dosáhnout	dosáhnout	k5eAaPmF
až	až	k9
60	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rostlinné	rostlinný	k2eAgFnSc6d1
fyziologii	fyziologie	k1gFnSc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
též	též	k9
pojem	pojem	k1gInSc1
fotosynteticky	fotosynteticky	k6eAd1
aktivní	aktivní	k2eAgInSc1d1
záření	záření	k1gNnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
záření	záření	k1gNnSc1
o	o	k7c6
vlnových	vlnový	k2eAgFnPc6d1
délkách	délka	k1gFnPc6
přibližně	přibližně	k6eAd1
odpovídajících	odpovídající	k2eAgMnPc2d1
viditelnému	viditelný	k2eAgNnSc3d1
záření	záření	k1gNnSc3
(	(	kIx(
<g/>
většinou	většinou	k6eAd1
se	se	k3xPyFc4
udává	udávat	k5eAaImIp3nS
rozsah	rozsah	k1gInSc1
380	#num#	k4
<g/>
–	–	k?
<g/>
720	#num#	k4
nm	nm	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Příkon	příkon	k1gInSc1
záření	záření	k1gNnSc2
dopadajícího	dopadající	k2eAgNnSc2d1
na	na	k7c4
povrch	povrch	k1gInSc4
zemské	zemský	k2eAgFnSc2d1
atmosféry	atmosféra	k1gFnSc2
činí	činit	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
1360	#num#	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
množství	množství	k1gNnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
solární	solární	k2eAgFnSc1d1
konstanta	konstanta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
není	být	k5eNaImIp3nS
konstantní	konstantní	k2eAgFnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
oběžná	oběžný	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
Země	zem	k1gFnSc2
kolem	kolem	k7c2
Slunce	slunce	k1gNnSc2
je	být	k5eAaImIp3nS
eliptická	eliptický	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
způsobuje	způsobovat	k5eAaImIp3nS
kolísání	kolísání	k1gNnSc3
ve	v	k7c6
velikosti	velikost	k1gFnSc6
solární	solární	k2eAgFnSc2d1
konstanty	konstanta	k1gFnSc2
přibližně	přibližně	k6eAd1
3	#num#	k4
%	%	kIx~
(	(	kIx(
<g/>
asi	asi	k9
40	#num#	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malé	Malé	k2eAgFnPc1d1
změny	změna	k1gFnPc1
solární	solární	k2eAgFnPc1d1
konstanty	konstanta	k1gFnPc1
jsou	být	k5eAaImIp3nP
též	též	k6eAd1
spjaty	spjat	k2eAgInPc4d1
s	s	k7c7
cykly	cyklus	k1gInPc7
sluneční	sluneční	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
ale	ale	k8xC
dosahují	dosahovat	k5eAaImIp3nP
maximálně	maximálně	k6eAd1
desetin	desetina	k1gFnPc2
procenta	procento	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Pohlcování	pohlcování	k1gNnSc1
záření	záření	k1gNnSc2
</s>
<s>
Část	část	k1gFnSc1
záření	záření	k1gNnSc2
je	být	k5eAaImIp3nS
pohlcena	pohltit	k5eAaPmNgFnS
atmosférou	atmosféra	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohlcení	pohlcení	k1gNnSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
ovšem	ovšem	k9
jen	jen	k9
některých	některý	k3yIgFnPc2
vlnových	vlnový	k2eAgFnPc2d1
délek	délka	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
prakticky	prakticky	k6eAd1
celé	celý	k2eAgFnSc6d1
nejkratší	krátký	k2eAgFnSc6d3
části	část	k1gFnSc6
ultrafialového	ultrafialový	k2eAgNnSc2d1
záření	záření	k1gNnSc2
(	(	kIx(
<g/>
do	do	k7c2
vlnové	vlnový	k2eAgFnSc2d1
délky	délka	k1gFnSc2
290	#num#	k4
nm	nm	k?
je	být	k5eAaImIp3nS
pohlceno	pohltit	k5eAaPmNgNnS
zcela	zcela	k6eAd1
<g/>
,	,	kIx,
od	od	k7c2
290	#num#	k4
do	do	k7c2
320	#num#	k4
nm	nm	k?
asi	asi	k9
ze	z	k7c2
2	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
)	)	kIx)
–	–	k?
pohlcuje	pohlcovat	k5eAaImIp3nS
jej	on	k3xPp3gMnSc4
především	především	k6eAd1
ozónová	ozónový	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
menší	malý	k2eAgFnSc6d2
míře	míra	k1gFnSc6
i	i	k9
další	další	k2eAgInPc1d1
plyny	plyn	k1gInPc1
jako	jako	k8xS,k8xC
kyslík	kyslík	k1gInSc1
O	o	k7c4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
oxid	oxid	k1gInSc1
dusičitý	dusičitý	k2eAgMnSc1d1
NO2	NO2	k1gMnSc1
nebo	nebo	k8xC
oxid	oxid	k1gInSc1
dusný	dusný	k2eAgInSc1d1
N2O	N2O	k1gMnSc7
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
vybraných	vybraný	k2eAgFnPc2d1
vlnových	vlnový	k2eAgFnPc2d1
délek	délka	k1gFnPc2
infračerveného	infračervený	k2eAgNnSc2d1
záření	záření	k1gNnSc2
(	(	kIx(
<g/>
pohlcení	pohlcení	k1gNnSc2
především	především	k6eAd1
oxidem	oxid	k1gInSc7
uhličitým	uhličitý	k2eAgInSc7d1
a	a	k8xC
vodní	vodní	k2eAgFnSc7d1
párou	pára	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
Ve	v	k7c6
viditelné	viditelný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
je	být	k5eAaImIp3nS
pohlcení	pohlcení	k1gNnSc1
jen	jen	k9
částečné	částečný	k2eAgNnSc1d1
a	a	k8xC
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
síle	síla	k1gFnSc6
vrstvy	vrstva	k1gFnSc2
atmosféry	atmosféra	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
musí	muset	k5eAaImIp3nS
záření	záření	k1gNnSc1
projít	projít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
stejné	stejný	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
Slunce	slunce	k1gNnSc2
nad	nad	k7c7
obzorem	obzor	k1gInSc7
se	se	k3xPyFc4
tedy	tedy	k9
větší	veliký	k2eAgNnPc1d2
pohlcení	pohlcení	k1gNnPc1
odehrává	odehrávat	k5eAaImIp3nS
v	v	k7c6
rovníkových	rovníkový	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
(	(	kIx(
<g/>
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
nejnižší	nízký	k2eAgFnSc1d3
část	část	k1gFnSc1
atmosféry	atmosféra	k1gFnSc2
–	–	k?
troposféra	troposféra	k1gFnSc1
–	–	k?
nejsilnější	silný	k2eAgFnPc1d3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
menší	malý	k2eAgFnSc1d2
v	v	k7c6
polárních	polární	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
a	a	k8xC
na	na	k7c6
horách	hora	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohlcení	pohlcení	k1gNnSc1
v	v	k7c6
polárních	polární	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
zároveň	zároveň	k6eAd1
zvětšeno	zvětšit	k5eAaPmNgNnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
sluneční	sluneční	k2eAgInPc1d1
paprsky	paprsek	k1gInPc1
pronikají	pronikat	k5eAaImIp3nP
do	do	k7c2
atmosféry	atmosféra	k1gFnSc2
pod	pod	k7c7
ostrým	ostrý	k2eAgInSc7d1
úhlem	úhel	k1gInSc7
a	a	k8xC
musí	muset	k5eAaImIp3nS
tak	tak	k6eAd1
proniknout	proniknout	k5eAaPmF
delší	dlouhý	k2eAgFnSc7d2
vrstvou	vrstva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Množství	množství	k1gNnSc1
prošlého	prošlý	k2eAgNnSc2d1
záření	záření	k1gNnSc2
udává	udávat	k5eAaImIp3nS
vztah	vztah	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Rg	Rg	k?
=	=	kIx~
Rs	Rs	k1gMnSc1
*	*	kIx~
kcosec	kcosec	k1gMnSc1
α	α	k?
*	*	kIx~
sin	sin	kA
α	α	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
kde	kde	k6eAd1
jednotlivé	jednotlivý	k2eAgFnPc1d1
veličiny	veličina	k1gFnPc1
znamenají	znamenat	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
Rg	Rg	k?
–	–	k?
globální	globální	k2eAgNnSc1d1
záření	záření	k1gNnSc1
dopadající	dopadající	k2eAgNnSc1d1
na	na	k7c4
vodorovný	vodorovný	k2eAgInSc4d1
povrch	povrch	k1gInSc4
Země	zem	k1gFnSc2
v	v	k7c6
nulové	nulový	k2eAgFnSc6d1
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
(	(	kIx(
<g/>
na	na	k7c4
hladinu	hladina	k1gFnSc4
moře	moře	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Rs	Rs	k?
–	–	k?
solární	solární	k2eAgFnSc1d1
konstanta	konstanta	k1gFnSc1
(	(	kIx(
<g/>
s	s	k7c7
korekcí	korekce	k1gFnSc7
na	na	k7c4
aktuální	aktuální	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
Země	zem	k1gFnSc2
od	od	k7c2
Slunce	slunce	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
k	k	k7c3
–	–	k?
koeficient	koeficient	k1gInSc1
propustnosti	propustnost	k1gFnSc2
atmosféry	atmosféra	k1gFnSc2
(	(	kIx(
<g/>
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
jejím	její	k3xOp3gNnSc6
„	„	k?
<g/>
zašpinění	zašpinění	k1gNnSc6
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
mezi	mezi	k7c7
0,7	0,7	k4
a	a	k8xC
0,9	0,9	k4
</s>
<s>
α	α	k?
–	–	k?
úhel	úhel	k1gInSc4
výšky	výška	k1gFnSc2
slunce	slunce	k1gNnSc2
nad	nad	k7c7
obzorem	obzor	k1gInSc7
</s>
<s>
cosec	cosec	k1gInSc1
α	α	k?
–	–	k?
cosecans	cosecans	k1gInSc1
úhlu	úhel	k1gInSc2
α	α	k?
<g/>
,	,	kIx,
tj.	tj.	kA
1	#num#	k4
/	/	kIx~
sin	sin	kA
α	α	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
povrch	povrch	k1gInSc4
vodorovný	vodorovný	k2eAgInSc4d1
<g/>
,	,	kIx,
lze	lze	k6eAd1
psát	psát	k5eAaImF
vzorec	vzorec	k1gInSc4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
přibližně	přibližně	k6eAd1
jako	jako	k8xC,k8xS
</s>
<s>
Rg	Rg	k?
=	=	kIx~
Rs	Rs	k1gMnSc1
*	*	kIx~
kcosec	kcosec	k1gMnSc1
α	α	k?
*	*	kIx~
sin	sin	kA
[	[	kIx(
<g/>
α	α	k?
–	–	k?
(	(	kIx(
<g/>
α	α	k?
<g/>
'	'	kIx"
*	*	kIx~
cos	cos	kA
β	β	k?
<g/>
)	)	kIx)
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
a	a	k8xC
pak	pak	k6eAd1
nově	nově	k6eAd1
použité	použitý	k2eAgFnPc1d1
veličiny	veličina	k1gFnPc1
značí	značit	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
α	α	k?
<g/>
'	'	kIx"
–	–	k?
úhel	úhel	k1gInSc1
naklonění	naklonění	k1gNnSc2
plochy	plocha	k1gFnSc2
směrem	směr	k1gInSc7
k	k	k7c3
jihu	jih	k1gInSc3
</s>
<s>
β	β	k?
–	–	k?
azimut	azimut	k1gInSc1
Slunce	slunce	k1gNnSc1
(	(	kIx(
<g/>
jih	jih	k1gInSc1
=	=	kIx~
180	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
našich	náš	k3xOp1gFnPc6
podmínkách	podmínka	k1gFnPc6
činí	činit	k5eAaImIp3nP
globální	globální	k2eAgFnPc1d1
radiace	radiace	k1gFnPc1
na	na	k7c6
vodorovném	vodorovný	k2eAgInSc6d1
povrchu	povrch	k1gInSc6
<g/>
:	:	kIx,
</s>
<s>
v	v	k7c6
letním	letní	k2eAgNnSc6d1
poledni	poledne	k1gNnSc6
max	max	kA
<g/>
.	.	kIx.
1	#num#	k4
000	#num#	k4
až	až	k9
1	#num#	k4
050	#num#	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
v	v	k7c6
zimním	zimní	k2eAgNnSc6d1
poledni	poledne	k1gNnSc6
max	max	kA
<g/>
.	.	kIx.
300	#num#	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
</s>
<s>
při	při	k7c6
souvisle	souvisle	k6eAd1
zatažené	zatažený	k2eAgFnSc6d1
obloze	obloha	k1gFnSc6
max	max	kA
<g/>
.	.	kIx.
100	#num#	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
</s>
<s>
v	v	k7c6
noci	noc	k1gFnSc6
(	(	kIx(
<g/>
při	při	k7c6
úplňku	úplněk	k1gInSc6
<g/>
)	)	kIx)
max	max	kA
<g/>
.	.	kIx.
0,01	0,01	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
</s>
<s>
Teoreticky	teoreticky	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
hodnota	hodnota	k1gFnSc1
krátkodobě	krátkodobě	k6eAd1
i	i	k9
přes	přes	k7c4
1	#num#	k4
100	#num#	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
na	na	k7c6
obloze	obloha	k1gFnSc6
rozptýlená	rozptýlený	k2eAgNnPc4d1
světlá	světlý	k2eAgNnPc4d1
oblaka	oblaka	k1gNnPc4
<g/>
,	,	kIx,
od	od	k7c2
nichž	jenž	k3xRgInPc2
se	se	k3xPyFc4
záření	záření	k1gNnSc2
silně	silně	k6eAd1
odráží	odrážet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
výpar	výpar	k1gInSc4
lze	lze	k6eAd1
dosáhnout	dosáhnout	k5eAaPmF
výkonu	výkon	k1gInSc2
jednotek	jednotka	k1gFnPc2
W	W	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Základní	základní	k2eAgFnPc1d1
přeměny	přeměna	k1gFnPc1
sluneční	sluneční	k2eAgFnSc2d1
energie	energie	k1gFnSc2
</s>
<s>
Lze	lze	k6eAd1
ji	on	k3xPp3gFnSc4
popsat	popsat	k5eAaPmF
rovnicemi	rovnice	k1gFnPc7
pro	pro	k7c4
čistou	čistá	k1gFnSc4
radiaci	radiace	k1gFnSc4
</s>
<s>
Rn	Rn	k?
=	=	kIx~
Rg	Rg	k1gMnSc1
-	-	kIx~
Ra	ra	k0
-	-	kIx~
Rlw	Rlw	k1gMnSc6
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rn	Rn	k?
=	=	kIx~
A	A	kA
+	+	kIx~
Q	Q	kA
+	+	kIx~
G	G	kA
+	+	kIx~
H	H	kA
+	+	kIx~
LE	LE	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
kde	kde	k6eAd1
jednotlivé	jednotlivý	k2eAgFnPc1d1
veličiny	veličina	k1gFnPc1
vyjadřují	vyjadřovat	k5eAaImIp3nP
následující	následující	k2eAgFnPc1d1
<g/>
:	:	kIx,
</s>
<s>
Rn	Rn	k?
–	–	k?
čistá	čistý	k2eAgFnSc1d1
radiace	radiace	k1gFnSc1
(	(	kIx(
<g/>
po	po	k7c6
odečtení	odečtení	k1gNnSc6
albeda	albed	k1gMnSc2
a	a	k8xC
dlouhovlnného	dlouhovlnný	k2eAgNnSc2d1
vyzařování	vyzařování	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Rg	Rg	k?
–	–	k?
dopadající	dopadající	k2eAgNnSc1d1
záření	záření	k1gNnSc1
</s>
<s>
Ra	ra	k0
–	–	k?
albedo	albedo	k1gNnSc1
(	(	kIx(
<g/>
procento	procento	k1gNnSc1
odraženého	odražený	k2eAgNnSc2d1
záření	záření	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Rlw	Rlw	k?
–	–	k?
dlouhovlnné	dlouhovlnný	k2eAgNnSc1d1
záření	záření	k1gNnSc1
</s>
<s>
A	a	k9
–	–	k?
fotosyntéza	fotosyntéza	k1gFnSc1
(	(	kIx(
<g/>
vyjádřená	vyjádřený	k2eAgFnSc1d1
v	v	k7c6
energetických	energetický	k2eAgInPc6d1
tocích	tok	k1gInPc6
<g/>
)	)	kIx)
</s>
<s>
Q	Q	kA
–	–	k?
teplo	teplo	k6eAd1
spotřebované	spotřebovaný	k2eAgNnSc4d1
na	na	k7c4
ohřev	ohřev	k1gInSc4
vegetace	vegetace	k1gFnSc2
</s>
<s>
G	G	kA
–	–	k?
tok	tok	k1gInSc1
tepla	teplo	k1gNnSc2
do	do	k7c2
půdy	půda	k1gFnSc2
</s>
<s>
H	H	kA
–	–	k?
pocitové	pocitový	k2eAgNnSc4d1
teplo	teplo	k1gNnSc4
</s>
<s>
LE	LE	kA
–	–	k?
latentní	latentní	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
výparu	výpar	k1gInSc2
neboli	neboli	k8xC
evapotranspirace	evapotranspirace	k1gFnSc2
</s>
<s>
Součin	součin	k1gInSc1
LE	LE	kA
představuje	představovat	k5eAaImIp3nS
energetickou	energetický	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
vypařené	vypařený	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
lze	lze	k6eAd1
vypočítat	vypočítat	k5eAaPmF
jako	jako	k9
množství	množství	k1gNnSc4
vody	voda	k1gFnSc2
E	E	kA
(	(	kIx(
<g/>
v	v	k7c6
mm	mm	kA
<g/>
,	,	kIx,
neboli	neboli	k8xC
l	l	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
vynásobené	vynásobený	k2eAgNnSc1d1
měrným	měrný	k2eAgNnSc7d1
latentním	latentní	k2eAgNnSc7d1
teplem	teplo	k1gNnSc7
výparu	výpar	k1gInSc2
L	L	kA
(	(	kIx(
<g/>
při	při	k7c6
teplotě	teplota	k1gFnSc6
20	#num#	k4
°	°	k?
<g/>
C	C	kA
platí	platit	k5eAaImIp3nS
L	L	kA
=	=	kIx~
2439	#num#	k4
kJ	kJ	k?
<g/>
.	.	kIx.
<g/>
kg	kg	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Protože	protože	k8xS
A	A	kA
a	a	k8xC
Q	Q	kA
jsou	být	k5eAaImIp3nP
svým	svůj	k3xOyFgInSc7
podílem	podíl	k1gInSc7
zanedbatelné	zanedbatelný	k2eAgInPc4d1
(	(	kIx(
<g/>
obě	dva	k4xCgFnPc1
složky	složka	k1gFnPc1
činí	činit	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
do	do	k7c2
1	#num#	k4
%	%	kIx~
čisté	čistý	k2eAgFnSc2d1
radiace	radiace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lze	lze	k6eAd1
tyto	tento	k3xDgMnPc4
členy	člen	k1gMnPc4
v	v	k7c6
rovnici	rovnice	k1gFnSc6
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
zanedbat	zanedbat	k5eAaPmF
a	a	k8xC
psát	psát	k5eAaImF
ji	on	k3xPp3gFnSc4
v	v	k7c6
zjednodušeném	zjednodušený	k2eAgInSc6d1
tvaru	tvar	k1gInSc6
jako	jako	k8xC,k8xS
</s>
<s>
Rn	Rn	k?
=	=	kIx~
G	G	kA
+	+	kIx~
H	H	kA
+	+	kIx~
LE	LE	kA
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Poměr	poměr	k1gInSc1
nejvýznamnější	významný	k2eAgFnSc7d3
složek	složka	k1gFnPc2
<g/>
,	,	kIx,
tedy	tedy	k9
pocitového	pocitový	k2eAgNnSc2d1
tepla	teplo	k1gNnSc2
a	a	k8xC
latentního	latentní	k2eAgNnSc2d1
tepla	teplo	k1gNnSc2
<g/>
,	,	kIx,
označujeme	označovat	k5eAaImIp1nP
jako	jako	k9
Bowenův	Bowenův	k2eAgInSc4d1
poměr	poměr	k1gInSc4
β	β	k?
</s>
<s>
β	β	k?
=	=	kIx~
H	H	kA
/	/	kIx~
LE	LE	kA
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Využití	využití	k1gNnSc1
sluneční	sluneční	k2eAgFnSc2d1
energie	energie	k1gFnSc2
</s>
<s>
Přímé	přímý	k2eAgFnPc1d1
</s>
<s>
solární	solární	k2eAgFnSc1d1
konstanta	konstanta	k1gFnSc1
<g/>
(	(	kIx(
<g/>
1348,3	1348,3	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
energie	energie	k1gFnSc1
od	od	k7c2
Slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
za	za	k7c4
jednotku	jednotka	k1gFnSc4
času	čas	k1gInSc2
<g/>
,	,	kIx,
dopadající	dopadající	k2eAgFnSc7d1
na	na	k7c4
jednotku	jednotka	k1gFnSc4
plochy	plocha	k1gFnSc2
kolmou	kolmý	k2eAgFnSc4d1
ke	k	k7c3
směru	směr	k1gInSc2
šíření	šíření	k1gNnSc2
záření	záření	k1gNnSc2
<g/>
,	,	kIx,
při	při	k7c6
průměrné	průměrný	k2eAgFnSc6d1
vzdálenosti	vzdálenost	k1gFnSc6
Slunce	slunce	k1gNnSc2
od	od	k7c2
Země	zem	k1gFnSc2
(	(	kIx(
<g/>
149,6	149,6	k4
*	*	kIx~
106	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mimo	mimo	k7c4
zemskou	zemský	k2eAgFnSc4d1
atmosféru	atmosféra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
Zemský	zemský	k2eAgInSc4d1
povrch	povrch	k1gInSc4
dopadne	dopadnout	k5eAaPmIp3nS
maximálně	maximálně	k6eAd1
1100	#num#	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
však	však	k9
stále	stále	k6eAd1
jen	jen	k9
o	o	k7c4
kolmé	kolmý	k2eAgFnPc4d1
plochy	plocha	k1gFnPc4
na	na	k7c4
směr	směr	k1gInSc4
toku	tok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
je	být	k5eAaImIp3nS
nejrozšířenější	rozšířený	k2eAgInSc1d3
fotovoltaický	fotovoltaický	k2eAgInSc1d1
článek	článek	k1gInSc1
<g/>
,	,	kIx,
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
se	se	k3xPyFc4
však	však	k9
i	i	k9
jiné	jiný	k2eAgInPc4d1
způsoby	způsob	k1gInPc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
stirlingův	stirlingův	k2eAgInSc1d1
motor	motor	k1gInSc1
<g/>
,	,	kIx,
ohnisková	ohniskový	k2eAgNnPc1d1
zrcadla	zrcadlo	k1gNnPc1
<g/>
,	,	kIx,
Solární	solární	k2eAgFnPc1d1
věže	věž	k1gFnPc1
</s>
<s>
v	v	k7c6
zemědělství	zemědělství	k1gNnSc6
(	(	kIx(
<g/>
skleníky	skleník	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
zpracování	zpracování	k1gNnSc1
užitkové	užitkový	k2eAgFnSc2d1
vody	voda	k1gFnSc2
(	(	kIx(
<g/>
ohřev	ohřev	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
též	též	k9
desalinace	desalinace	k1gFnSc1
a	a	k8xC
desinfekce	desinfekce	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
vytápění	vytápění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Nepřímé	přímý	k2eNgNnSc1d1
</s>
<s>
Nepřímo	přímo	k6eNd1
se	se	k3xPyFc4
sluneční	sluneční	k2eAgFnSc2d1
energie	energie	k1gFnSc2
v	v	k7c6
přírodě	příroda	k1gFnSc6
přeměňuje	přeměňovat	k5eAaImIp3nS
na	na	k7c4
<g/>
:	:	kIx,
</s>
<s>
potenciální	potenciální	k2eAgFnSc4d1
energii	energie	k1gFnSc4
vody	voda	k1gFnSc2
(	(	kIx(
<g/>
využívaná	využívaný	k2eAgFnSc1d1
ve	v	k7c6
vodních	vodní	k2eAgFnPc6d1
elektrárnách	elektrárna	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
kinetickou	kinetický	k2eAgFnSc4d1
energii	energie	k1gFnSc4
vzdušných	vzdušný	k2eAgFnPc2d1
mas	masa	k1gFnPc2
(	(	kIx(
<g/>
vítr	vítr	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
chemickou	chemický	k2eAgFnSc4d1
energii	energie	k1gFnSc4
biomasy	biomasa	k1gFnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
fosilních	fosilní	k2eAgNnPc2d1
paliv	palivo	k1gNnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
akumulace	akumulace	k1gFnSc1
sluneční	sluneční	k2eAgFnSc2d1
energie	energie	k1gFnSc2
proběhla	proběhnout	k5eAaPmAgFnS
před	před	k7c7
dlouhou	dlouhý	k2eAgFnSc7d1
dobou	doba	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Solární	solární	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Solární	solární	k2eAgInPc1d1
panely	panel	k1gInPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fotovoltaický	fotovoltaický	k2eAgInSc4d1
článek	článek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Solární	solární	k2eAgInPc1d1
články	článek	k1gInPc1
(	(	kIx(
<g/>
sluneční	sluneční	k2eAgFnPc1d1
baterie	baterie	k1gFnPc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
polovodičové	polovodičový	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
mění	měnit	k5eAaImIp3nP
světelnou	světelný	k2eAgFnSc4d1
energii	energie	k1gFnSc4
v	v	k7c4
energii	energie	k1gFnSc4
elektrickou	elektrický	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotoelektrický	fotoelektrický	k2eAgInSc1d1
efekt	efekt	k1gInSc1
vysvětluje	vysvětlovat	k5eAaImIp3nS
vznik	vznik	k1gInSc4
volných	volný	k2eAgInPc2d1
elektrických	elektrický	k2eAgInPc2d1
nosičů	nosič	k1gInPc2
dopadem	dopad	k1gInSc7
záření	záření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
se	se	k3xPyFc4
daří	dařit	k5eAaImIp3nS
přeměnit	přeměnit	k5eAaPmF
v	v	k7c4
elektrickou	elektrický	k2eAgFnSc4d1
energii	energie	k1gFnSc4
jen	jen	k9
asi	asi	k9
17	#num#	k4
%	%	kIx~
energie	energie	k1gFnSc2
dopadajícího	dopadající	k2eAgNnSc2d1
záření	záření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Solární	solární	k2eAgInPc1d1
články	článek	k1gInPc1
jsou	být	k5eAaImIp3nP
tvořeny	tvořit	k5eAaImNgInP
polovodičovými	polovodičový	k2eAgInPc7d1
plátky	plátek	k1gInPc4
tenčími	tenčí	k1gNnPc7
než	než	k8xS
1	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
spodní	spodní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
plošná	plošný	k2eAgFnSc1d1
průchozí	průchozí	k2eAgFnSc1d1
elektroda	elektroda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horní	horní	k2eAgFnSc1d1
elektroda	elektroda	k1gFnSc1
má	mít	k5eAaImIp3nS
plošné	plošný	k2eAgNnSc1d1
uspořádání	uspořádání	k1gNnSc1
tvaru	tvar	k1gInSc2
dlouhých	dlouhý	k2eAgInPc2d1
prstů	prst	k1gInPc2
zasahujících	zasahující	k2eAgInPc2d1
do	do	k7c2
plochy	plocha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
může	moct	k5eAaImIp3nS
světlo	světlo	k1gNnSc1
na	na	k7c4
plochu	plocha	k1gFnSc4
svítit	svítit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povrch	povrch	k1gInSc1
solárního	solární	k2eAgInSc2d1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
chráněn	chránit	k5eAaImNgInS
skleněnou	skleněný	k2eAgFnSc7d1
vrstvou	vrstva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sloužící	sloužící	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
antireflexní	antireflexní	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
a	a	k8xC
zabezpečuje	zabezpečovat	k5eAaImIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
nejvíce	hodně	k6eAd3,k6eAd1
světla	světlo	k1gNnSc2
vniklo	vniknout	k5eAaPmAgNnS
do	do	k7c2
polovodiče	polovodič	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antireflexní	antireflexní	k2eAgFnPc4d1
vrstvy	vrstva	k1gFnPc4
se	se	k3xPyFc4
většinou	většina	k1gFnSc7
tvoří	tvořit	k5eAaImIp3nP
napařením	napaření	k1gNnSc7
oxidu	oxid	k1gInSc2
titanu	titan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgInSc7
získá	získat	k5eAaPmIp3nS
článek	článek	k1gInSc1
svůj	svůj	k3xOyFgInSc4
tmavomodrý	tmavomodrý	k2eAgInSc4d1
vzhled	vzhled	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
polovodičový	polovodičový	k2eAgInSc1d1
materiál	materiál	k1gInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
převážně	převážně	k6eAd1
křemík	křemík	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiné	jiný	k2eAgInPc1d1
polovodičové	polovodičový	k2eAgInPc1d1
materiály	materiál	k1gInPc1
<g/>
,	,	kIx,
např.	např.	kA
galium	galium	k1gNnSc1
arsenid	arsenid	k1gInSc1
<g/>
,	,	kIx,
kadmiumsulfid	kadmiumsulfid	k1gInSc1
<g/>
,	,	kIx,
kadmiumtellurid	kadmiumtellurid	k1gInSc1
<g/>
,	,	kIx,
selenid	selenid	k1gInSc1
mědi	měď	k1gFnSc2
a	a	k8xC
india	indium	k1gNnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
sulfid	sulfid	k1gInSc1
galia	galium	k1gNnSc2
se	se	k3xPyFc4
zatím	zatím	k6eAd1
zkoušejí	zkoušet	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krycí	krycí	k2eAgInSc4d1
sklo	sklo	k1gNnSc1
chrání	chránit	k5eAaImIp3nS
povrch	povrch	k1gInSc4
solárních	solární	k2eAgInPc2d1
článků	článek	k1gInPc2
i	i	k9
před	před	k7c7
vlivy	vliv	k1gInPc7
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
solárních	solární	k2eAgInPc2d1
článků	článek	k1gInPc2
</s>
<s>
Využití	využití	k1gNnSc1
těchto	tento	k3xDgInPc2
článků	článek	k1gInPc2
je	být	k5eAaImIp3nS
různorodé	různorodý	k2eAgNnSc1d1
<g/>
:	:	kIx,
od	od	k7c2
solárních	solární	k2eAgFnPc2d1
kalkulaček	kalkulačka	k1gFnPc2
až	až	k9
po	po	k7c4
energetické	energetický	k2eAgNnSc4d1
zabezpečení	zabezpečení	k1gNnSc4
horských	horský	k2eAgFnPc2d1
chat	chata	k1gFnPc2
v	v	k7c6
rozsahu	rozsah	k1gInSc6
jednotek	jednotka	k1gFnPc2
až	až	k8xS
desítek	desítka	k1gFnPc2
kW	kW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektrický	elektrický	k2eAgInSc1d1
výkon	výkon	k1gInSc1
je	být	k5eAaImIp3nS
dán	dát	k5eAaPmNgInS
celkovou	celkový	k2eAgFnSc7d1
plochou	plocha	k1gFnSc7
a	a	k8xC
účinností	účinnost	k1gFnSc7
solárních	solární	k2eAgInPc2d1
článků	článek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
ploše	plocha	k1gFnSc6
1	#num#	k4
dm²	dm²	k?
a	a	k8xC
plném	plný	k2eAgInSc6d1
slunečním	sluneční	k2eAgInSc6d1
svitu	svit	k1gInSc6
může	moct	k5eAaImIp3nS
při	při	k7c6
napětí	napětí	k1gNnSc6
0,5	0,5	k4
V	V	kA
a	a	k8xC
proudu	proud	k1gInSc3
2,5	2,5	k4
A	a	k9
dávat	dávat	k5eAaImF
článek	článek	k1gInSc4
výkon	výkon	k1gInSc4
1,25	1,25	k4
W.	W.	kA
Vyšší	vysoký	k2eAgNnSc1d2
napětí	napětí	k1gNnSc1
se	se	k3xPyFc4
získá	získat	k5eAaPmIp3nS
sériovým	sériový	k2eAgNnSc7d1
řazením	řazení	k1gNnSc7
a	a	k8xC
větší	veliký	k2eAgInSc1d2
proud	proud	k1gInSc1
paralelním	paralelní	k2eAgNnSc7d1
řazením	řazení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Panel	panel	k1gInSc1
bývá	bývat	k5eAaImIp3nS
složen	složit	k5eAaPmNgInS
z	z	k7c2
33	#num#	k4
až	až	k9
36	#num#	k4
křemíkových	křemíkový	k2eAgInPc2d1
solárních	solární	k2eAgInPc2d1
článků	článek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevýhodou	nevýhoda	k1gFnSc7
je	být	k5eAaImIp3nS
ale	ale	k9
vyšší	vysoký	k2eAgFnSc1d2
cena	cena	k1gFnSc1
proti	proti	k7c3
klasickým	klasický	k2eAgInPc3d1
zdrojům	zdroj	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Oblast	oblast	k1gFnSc1
průmyslu	průmysl	k1gInSc2
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
solárních	solární	k2eAgInPc2d1
článků	článek	k1gInPc2
a	a	k8xC
adaptaci	adaptace	k1gFnSc4
sluneční	sluneční	k2eAgFnSc2d1
energie	energie	k1gFnSc2
zažívá	zažívat	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
rychlý	rychlý	k2eAgInSc1d1
růst	růst	k1gInSc1
a	a	k8xC
některé	některý	k3yIgInPc1
programy	program	k1gInPc1
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
např.	např.	kA
Clean	Clean	k1gMnSc1
Power	Power	k1gMnSc1
from	from	k1gMnSc1
Deserts	Deserts	k1gInSc4
od	od	k7c2
kooperace	kooperace	k1gFnSc2
TREC	TREC	kA
při	při	k7c6
Římském	římský	k2eAgInSc6d1
klubu	klub	k1gInSc6
či	či	k8xC
projekt	projekt	k1gInSc1
solárních	solární	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
Spojených	spojený	k2eAgInPc2d1
arabských	arabský	k2eAgInPc2d1
emirátů	emirát	k1gInPc2
<g/>
)	)	kIx)
začínají	začínat	k5eAaImIp3nP
pozvolna	pozvolna	k6eAd1
získávat	získávat	k5eAaImF
prostor	prostor	k1gInSc4
v	v	k7c6
energetických	energetický	k2eAgFnPc6d1
strategiích	strategie	k1gFnPc6
jednotlivých	jednotlivý	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Soustředění	soustředění	k1gNnSc1
sluneční	sluneční	k2eAgFnSc2d1
energie	energie	k1gFnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Sluneční	sluneční	k2eAgFnSc4d1
pec	pec	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1
způsob	způsob	k1gInSc1
využití	využití	k1gNnSc2
sluneční	sluneční	k2eAgFnSc2d1
energie	energie	k1gFnSc2
je	být	k5eAaImIp3nS
nikoli	nikoli	k9
přijímat	přijímat	k5eAaImF
ji	on	k3xPp3gFnSc4
plošně	plošně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
pomocí	pomocí	k7c2
soustavy	soustava	k1gFnSc2
parabolických	parabolický	k2eAgNnPc2d1
zrcadel	zrcadlo	k1gNnPc2
ji	on	k3xPp3gFnSc4
odrážet	odrážet	k5eAaImF
a	a	k8xC
soustředit	soustředit	k5eAaPmF
do	do	k7c2
jednoho	jeden	k4xCgNnSc2
místa	místo	k1gNnSc2
s	s	k7c7
„	„	k?
<g/>
receptorem	receptor	k1gInSc7
<g/>
“	“	k?
schopným	schopný	k2eAgMnPc3d1
tuto	tento	k3xDgFnSc4
přesměrovanou	přesměrovaný	k2eAgFnSc4d1
energii	energie	k1gFnSc4
zpracovat	zpracovat	k5eAaPmF
nebo	nebo	k8xC
uchovat	uchovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
receptorem	receptor	k1gInSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
i	i	k9
Stirlingův	Stirlingův	k2eAgInSc4d1
motor	motor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Konstrukce	konstrukce	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
tento	tento	k3xDgInSc1
způsob	způsob	k1gInSc1
získávání	získávání	k1gNnSc2
energie	energie	k1gFnSc2
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
<g/>
,	,	kIx,
se	se	k3xPyFc4
označují	označovat	k5eAaImIp3nP
jako	jako	k8xS,k8xC
koncentrační	koncentrační	k2eAgFnSc1d1
solární	solární	k2eAgFnSc1d1
elektrárna	elektrárna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Solární	solární	k2eAgFnPc1d1
elektrárny	elektrárna	k1gFnPc1
</s>
<s>
Je	být	k5eAaImIp3nS
navrženo	navrhnout	k5eAaPmNgNnS
vyjmutí	vyjmutí	k1gNnSc1
této	tento	k3xDgFnSc2
části	část	k1gFnSc2
článku	článek	k1gInSc2
a	a	k8xC
její	její	k3xOp3gNnSc4
přesunutí	přesunutí	k1gNnSc4
do	do	k7c2
článku	článek	k1gInSc2
Fotovoltaický	fotovoltaický	k2eAgInSc4d1
článek	článek	k1gInSc4
nebo	nebo	k8xC
článku	článek	k1gInSc6
Solární	solární	k2eAgInSc4d1
ohřev	ohřev	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
návrhu	návrh	k1gInSc3
se	se	k3xPyFc4
můžete	moct	k5eAaImIp2nP
vyjádřit	vyjádřit	k5eAaPmF
v	v	k7c6
diskusi	diskuse	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
je	být	k5eAaImIp3nS
využití	využití	k1gNnSc1
sluneční	sluneční	k2eAgFnSc2d1
energie	energie	k1gFnSc2
na	na	k7c4
výrobu	výroba	k1gFnSc4
elektřiny	elektřina	k1gFnSc2
(	(	kIx(
<g/>
fotovoltaika	fotovoltaika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolik	kolika	k1gFnPc2
energie	energie	k1gFnSc2
sluneční	sluneční	k2eAgFnSc1d1
elektrárna	elektrárna	k1gFnSc1
vyrobí	vyrobit	k5eAaPmIp3nS
se	se	k3xPyFc4
odvíjí	odvíjet	k5eAaImIp3nS
od	od	k7c2
intenzity	intenzita	k1gFnSc2
slunečního	sluneční	k2eAgNnSc2d1
záření	záření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
obloha	obloha	k1gFnSc1
bez	bez	k7c2
mráčku	mráček	k1gInSc2
<g/>
,	,	kIx,
výkon	výkon	k1gInSc1
slunečního	sluneční	k2eAgNnSc2d1
záření	záření	k1gNnSc2
je	být	k5eAaImIp3nS
kolem	kolem	k7c2
1	#num#	k4
<g/>
kW	kW	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
však	však	k9
obloha	obloha	k1gFnSc1
zatáhne	zatáhnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
sluneční	sluneční	k2eAgNnSc1d1
záření	záření	k1gNnSc1
je	být	k5eAaImIp3nS
až	až	k9
10	#num#	k4
<g/>
krát	krát	k6eAd1
méně	málo	k6eAd2
intenzivní	intenzivní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
slunečních	sluneční	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
je	být	k5eAaImIp3nS
v	v	k7c6
průměru	průměr	k1gInSc6
1330	#num#	k4
<g/>
–	–	k?
<g/>
1800	#num#	k4
hodin	hodina	k1gFnPc2
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konkrétní	konkrétní	k2eAgInSc1d1
údaj	údaj	k1gInSc1
vážící	vážící	k2eAgInSc1d1
se	se	k3xPyFc4
k	k	k7c3
místu	místo	k1gNnSc3
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
plánujete	plánovat	k5eAaImIp2nP
stavět	stavět	k5eAaImF
solární	solární	k2eAgFnSc4d1
elektrárnu	elektrárna	k1gFnSc4
<g/>
,	,	kIx,
poskytuje	poskytovat	k5eAaImIp3nS
Český	český	k2eAgInSc1d1
hydrometeorologický	hydrometeorologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Vždy	vždy	k6eAd1
nicméně	nicméně	k8xC
záleží	záležet	k5eAaImIp3nS
na	na	k7c6
konkrétním	konkrétní	k2eAgNnSc6d1
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
pro	pro	k7c4
stavbu	stavba	k1gFnSc4
solární	solární	k2eAgFnSc2d1
elektrárny	elektrárna	k1gFnSc2
zvolíme	zvolit	k5eAaPmIp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Intenzitu	intenzita	k1gFnSc4
a	a	k8xC
dobu	doba	k1gFnSc4
slunečního	sluneční	k2eAgNnSc2d1
záření	záření	k1gNnSc2
ovlivňuje	ovlivňovat	k5eAaImIp3nS
nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
<g/>
,	,	kIx,
oblačnost	oblačnost	k1gFnSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
lokální	lokální	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
jako	jako	k9
jsou	být	k5eAaImIp3nP
časté	častý	k2eAgFnPc1d1
ranní	ranní	k2eAgFnPc1d1
mlhy	mlha	k1gFnPc1
<g/>
,	,	kIx,
znečištění	znečištění	k1gNnSc1
ovzduší	ovzduší	k1gNnSc1
či	či	k8xC
úhel	úhel	k1gInSc1
dopadu	dopad	k1gInSc2
slunečních	sluneční	k2eAgInPc2d1
paprsků	paprsek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Množství	množství	k1gNnSc1
energie	energie	k1gFnSc2
z	z	k7c2
fotovoltaických	fotovoltaický	k2eAgInPc2d1
panelů	panel	k1gInPc2
pro	pro	k7c4
různá	různý	k2eAgNnPc4d1
místa	místo	k1gNnPc4
<g/>
,	,	kIx,
čas	čas	k1gInSc4
a	a	k8xC
sklon	sklon	k1gInSc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
spočítat	spočítat	k5eAaPmF
zde	zde	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
místě	místo	k1gNnSc6
je	být	k5eAaImIp3nS
samozřejmě	samozřejmě	k6eAd1
také	také	k9
otázka	otázka	k1gFnSc1
kapacity	kapacita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinými	jiný	k2eAgNnPc7d1
slovy	slovo	k1gNnPc7
<g/>
:	:	kIx,
kolik	kolik	k9
se	se	k3xPyFc4
na	na	k7c4
plochu	plocha	k1gFnSc4
střechy	střecha	k1gFnSc2
(	(	kIx(
<g/>
či	či	k8xC
na	na	k7c4
jiné	jiný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
zvolené	zvolený	k2eAgNnSc4d1
pro	pro	k7c4
instalaci	instalace	k1gFnSc4
elektrárny	elektrárna	k1gFnSc2
<g/>
)	)	kIx)
vejde	vejít	k5eAaPmIp3nS
solárních	solární	k2eAgInPc2d1
panelů	panel	k1gInPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Obecně	obecně	k6eAd1
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
1	#num#	k4
kWp	kWp	k?
(	(	kIx(
<g/>
maximální	maximální	k2eAgInSc4d1
výkon	výkon	k1gInSc4
elektrárny	elektrárna	k1gFnSc2
<g/>
)	)	kIx)
zabere	zabrat	k5eAaPmIp3nS
asi	asi	k9
8	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
m	m	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
plocha	plocha	k1gFnSc1
je	být	k5eAaImIp3nS
schopna	schopen	k2eAgFnSc1d1
vyrobit	vyrobit	k5eAaPmF
přibližně	přibližně	k6eAd1
1	#num#	k4
MWh	MWh	k1gFnPc2
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Prodej	prodej	k1gInSc1
nebo	nebo	k8xC
spotřeba	spotřeba	k1gFnSc1
elektřiny	elektřina	k1gFnSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
nepokrývá	pokrývat	k5eNaImIp3nS
téma	téma	k1gNnSc4
dostatečně	dostatečně	k6eAd1
z	z	k7c2
celosvětového	celosvětový	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Pokuste	pokusit	k5eAaPmRp2nP
se	se	k3xPyFc4
<g/>
,	,	kIx,
prosíme	prosit	k5eAaImIp1nP
<g/>
,	,	kIx,
článek	článek	k1gInSc4
vylepšit	vylepšit	k5eAaPmF
a	a	k8xC
doplnit	doplnit	k5eAaPmF
nebo	nebo	k8xC
k	k	k7c3
možnému	možný	k2eAgNnSc3d1
zdokonalení	zdokonalení	k1gNnSc3
přispět	přispět	k5eAaPmF
v	v	k7c6
diskusi	diskuse	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Majitel	majitel	k1gMnSc1
solární	solární	k2eAgFnSc2d1
elektrárny	elektrárna	k1gFnSc2
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
rozhodnout	rozhodnout	k5eAaPmF
<g/>
,	,	kIx,
zda	zda	k8xS
využije	využít	k5eAaPmIp3nS
garantovanou	garantovaný	k2eAgFnSc4d1
výkupní	výkupní	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
elektřiny	elektřina	k1gFnSc2
a	a	k8xC
bude	být	k5eAaImBp3nS
veškerou	veškerý	k3xTgFnSc4
elektřinu	elektřina	k1gFnSc4
prodávat	prodávat	k5eAaImF
regionálnímu	regionální	k2eAgMnSc3d1
distributorovi	distributor	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
ji	on	k3xPp3gFnSc4
musí	muset	k5eAaImIp3nS
od	od	k7c2
majitele	majitel	k1gMnSc2
solární	solární	k2eAgFnSc2d1
elektrárny	elektrárna	k1gFnSc2
podle	podle	k7c2
legislativy	legislativa	k1gFnSc2
EU	EU	kA
vykupovat	vykupovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Vlastník	vlastník	k1gMnSc1
solární	solární	k2eAgFnSc2d1
elektrárny	elektrárna	k1gFnSc2
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
rozhodnout	rozhodnout	k5eAaPmF
pro	pro	k7c4
samostatný	samostatný	k2eAgInSc4d1
prodej	prodej	k1gInSc4
elektřiny	elektřina	k1gFnSc2
a	a	k8xC
získat	získat	k5eAaPmF
podporu	podpora	k1gFnSc4
formou	forma	k1gFnSc7
zelených	zelený	k2eAgMnPc2d1
bonusů	bonus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tom	ten	k3xDgInSc6
případě	případ	k1gInSc6
si	se	k3xPyFc3
prodává	prodávat	k5eAaImIp3nS
elektřinu	elektřina	k1gFnSc4
sám	sám	k3xTgMnSc1
(	(	kIx(
<g/>
tedy	tedy	k8xC
jakémukoli	jakýkoli	k3yIgMnSc3
koncovému	koncový	k2eAgMnSc3d1
uživateli	uživatel	k1gMnSc3
<g/>
)	)	kIx)
a	a	k8xC
od	od	k7c2
ČEZu	ČEZus	k1gInSc2
<g/>
,	,	kIx,
E.	E.	kA
<g/>
Onu	onen	k3xDgFnSc4
či	či	k8xC
PRE	PRE	kA
získává	získávat	k5eAaImIp3nS
zmíněné	zmíněný	k2eAgInPc4d1
zelené	zelený	k2eAgInPc4d1
bonusy	bonus	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
upozornit	upozornit	k5eAaPmF
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
výkupní	výkupní	k2eAgFnPc1d1
ceny	cena	k1gFnPc1
elektřiny	elektřina	k1gFnSc2
a	a	k8xC
zelené	zelený	k2eAgInPc1d1
bonusy	bonus	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
každoročně	každoročně	k6eAd1
stanovuje	stanovovat	k5eAaImIp3nS
Energetický	energetický	k2eAgInSc1d1
regulační	regulační	k2eAgInSc1d1
úřad	úřad	k1gInSc1
(	(	kIx(
<g/>
ERÚ	ERÚ	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	se	k3xPyFc4
každoročně	každoročně	k6eAd1
mění	měnit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
fotovoltaická	fotovoltaický	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
uvedená	uvedený	k2eAgNnPc4d1
do	do	k7c2
provozu	provoz	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
platí	platit	k5eAaImIp3nP
tyto	tento	k3xDgFnPc4
ceny	cena	k1gFnPc4
<g/>
:	:	kIx,
V	v	k7c6
případě	případ	k1gInSc6
solární	solární	k2eAgFnSc2d1
elektrárny	elektrárna	k1gFnSc2
do	do	k7c2
30	#num#	k4
kW	kW	kA
je	být	k5eAaImIp3nS
stanovena	stanoven	k2eAgFnSc1d1
na	na	k7c4
12,89	12,89	k4
Kč	Kč	kA
za	za	k7c4
1	#num#	k4
kWh	kwh	kA
(	(	kIx(
<g/>
zelený	zelený	k2eAgInSc1d1
bonus	bonus	k1gInSc1
na	na	k7c4
11,91	11,91	k4
Kč	Kč	kA
za	za	k7c4
1	#num#	k4
kWh	kwh	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
instalovaný	instalovaný	k2eAgInSc4d1
výkon	výkon	k1gInSc4
sluneční	sluneční	k2eAgFnSc2d1
elektrárny	elektrárna	k1gFnSc2
nad	nad	k7c7
30	#num#	k4
kW	kW	kA
<g/>
,	,	kIx,
pak	pak	k6eAd1
je	být	k5eAaImIp3nS
výkupní	výkupní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
stanovena	stanovit	k5eAaPmNgFnS
na	na	k7c4
12,79	12,79	k4
Kč	Kč	kA
za	za	k7c4
1	#num#	k4
kWh	kwh	kA
(	(	kIx(
<g/>
zelený	zelený	k2eAgInSc1d1
bonus	bonus	k1gInSc1
na	na	k7c4
11,81	11,81	k4
Kč	Kč	kA
za	za	k7c4
1	#num#	k4
kWh	kwh	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
ceny	cena	k1gFnPc1
jsou	být	k5eAaImIp3nP
garantovány	garantovat	k5eAaBmNgFnP
po	po	k7c4
dobu	doba	k1gFnSc4
20	#num#	k4
let	léto	k1gNnPc2
provozování	provozování	k1gNnSc2
konkrétního	konkrétní	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tzv.	tzv.	kA
zelený	zelený	k2eAgInSc4d1
bonus	bonus	k1gInSc4
se	se	k3xPyFc4
však	však	k9
mění	měnit	k5eAaImIp3nS
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
a	a	k8xC
pro	pro	k7c4
konkrétní	konkrétní	k2eAgInSc4d1
výpočet	výpočet	k1gInSc4
návratnosti	návratnost	k1gFnSc2
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
zohlednit	zohlednit	k5eAaPmF
cenový	cenový	k2eAgInSc4d1
výnos	výnos	k1gInSc4
ERU	ERU	kA
v	v	k7c6
době	doba	k1gFnSc6
pořízení	pořízení	k1gNnSc1
fotovoltaické	fotovoltaický	k2eAgFnSc2d1
elektrárny	elektrárna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
provozovatele	provozovatel	k1gMnSc4
je	být	k5eAaImIp3nS
jistě	jistě	k6eAd1
zajímavá	zajímavý	k2eAgFnSc1d1
i	i	k8xC
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
po	po	k7c4
dobu	doba	k1gFnSc4
pěti	pět	k4xCc2
let	léto	k1gNnPc2
osvobozen	osvobodit	k5eAaPmNgInS
od	od	k7c2
daní	daň	k1gFnPc2
z	z	k7c2
příjmů	příjem	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Živnostenský	živnostenský	k2eAgInSc1d1
list	list	k1gInSc1
není	být	k5eNaImIp3nS
nutné	nutný	k2eAgNnSc1d1
zřizovat	zřizovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
pro	pro	k7c4
provoz	provoz	k1gInSc4
FVE	FVE	kA
byla	být	k5eAaImAgFnS
nutná	nutný	k2eAgFnSc1d1
licence	licence	k1gFnSc1
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
nutná	nutný	k2eAgFnSc1d1
pouze	pouze	k6eAd1
u	u	k7c2
FVE	FVE	kA
s	s	k7c7
instalovaným	instalovaný	k2eAgInSc7d1
výkonem	výkon	k1gInSc7
vyšším	vysoký	k2eAgInSc7d2
než	než	k8xS
10	#num#	k4
kWp	kWp	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opět	opět	k6eAd1
jde	jít	k5eAaImIp3nS
o	o	k7c4
proměnlivý	proměnlivý	k2eAgInSc4d1
údaj	údaj	k1gInSc4
a	a	k8xC
v	v	k7c6
případě	případ	k1gInSc6
zájmu	zájem	k1gInSc2
je	být	k5eAaImIp3nS
vhodné	vhodný	k2eAgNnSc1d1
sledovat	sledovat	k5eAaImF
aktuální	aktuální	k2eAgFnSc4d1
legislativu	legislativa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Solární	solární	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Solární	solární	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Solární	solární	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
(	(	kIx(
<g/>
Solar	Solar	k1gInSc1
island	islanda	k1gFnPc2
<g/>
)	)	kIx)
dostalo	dostat	k5eAaPmAgNnS
označení	označení	k1gNnSc1
projektu	projekt	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
spolupracují	spolupracovat	k5eAaImIp3nP
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
jistou	jistý	k2eAgFnSc7d1
švýcarskou	švýcarský	k2eAgFnSc7d1
inženýrskou	inženýrský	k2eAgFnSc7d1
firmou	firma	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
návrhu	návrh	k1gInSc2
je	být	k5eAaImIp3nS
umělý	umělý	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
–	–	k?
na	na	k7c6
moři	moře	k1gNnSc6
plovoucí	plovoucí	k2eAgInSc4d1
objekt	objekt	k1gInSc4
kruhového	kruhový	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
s	s	k7c7
několika	několik	k4yIc7
stovkami	stovka	k1gFnPc7
až	až	k8xS
tisíci	tisíc	k4xCgInPc7
zrcadel	zrcadlo	k1gNnPc2
<g/>
,	,	kIx,
odrážejících	odrážející	k2eAgMnPc2d1
dopadající	dopadající	k2eAgFnSc4d1
sluneční	sluneční	k2eAgFnSc4d1
energii	energie	k1gFnSc4
na	na	k7c4
(	(	kIx(
<g/>
pravděpodobně	pravděpodobně	k6eAd1
kovovou	kovový	k2eAgFnSc4d1
<g/>
)	)	kIx)
kolonu	kolona	k1gFnSc4
(	(	kIx(
<g/>
potrubí	potrubí	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
taktéž	taktéž	k?
tvoří	tvořit	k5eAaImIp3nP
svrchní	svrchní	k2eAgFnSc4d1
část	část	k1gFnSc4
solárního	solární	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
potrubí	potrubí	k1gNnSc6
cirkuluje	cirkulovat	k5eAaImIp3nS
voda	voda	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
přijmuté	přijmutý	k2eAgNnSc1d1
záření	záření	k1gNnSc1
dokáže	dokázat	k5eAaPmIp3nS
přivést	přivést	k5eAaPmF
k	k	k7c3
varu	var	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklá	vzniklý	k2eAgFnSc1d1
pára	pára	k1gFnSc1
poté	poté	k6eAd1
začne	začít	k5eAaPmIp3nS
pohánět	pohánět	k5eAaImF
turbínu	turbína	k1gFnSc4
ve	v	k7c6
středu	střed	k1gInSc6
ostrova	ostrov	k1gInSc2
(	(	kIx(
<g/>
kam	kam	k6eAd1
se	se	k3xPyFc4
toto	tento	k3xDgNnSc1
potrubí	potrubí	k1gNnSc1
sbíhá	sbíhat	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
tato	tento	k3xDgFnSc1
energie	energie	k1gFnSc1
některou	některý	k3yIgFnSc4
z	z	k7c2
dostupných	dostupný	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
uložena	uložit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jeden	jeden	k4xCgMnSc1
takovýto	takovýto	k3xDgInSc1
průměrný	průměrný	k2eAgInSc1d1
solární	solární	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
bude	být	k5eAaImBp3nS
vyrábět	vyrábět	k5eAaImF
řádově	řádově	k6eAd1
gigawatty	gigawatta	k1gFnSc2
energie	energie	k1gFnSc2
z	z	k7c2
dopadajícího	dopadající	k2eAgNnSc2d1
slunečního	sluneční	k2eAgNnSc2d1
záření	záření	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://techxplore.com/news/2019-05-renewables-doesnt-equal-zero-carbon-energy.html	https://techxplore.com/news/2019-05-renewables-doesnt-equal-zero-carbon-energy.html	k1gInSc1
-	-	kIx~
100	#num#	k4
<g/>
%	%	kIx~
renewables	renewables	k1gMnSc1
doesn	doesn	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
equal	equal	k1gMnSc1
zero-carbon	zero-carbon	k1gMnSc1
energy	energ	k1gInPc4
<g/>
,	,	kIx,
and	and	k?
the	the	k?
difference	difference	k1gFnSc2
is	is	k?
growing	growing	k1gInSc1
<g/>
↑	↑	k?
MOLDANOVÁ	MOLDANOVÁ	kA
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemie	chemie	k1gFnSc1
plynné	plynný	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
BRANIŠ	BRANIŠ	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
;	;	kIx,
HŮNOVÁ	HŮNOVÁ	kA
<g/>
,	,	kIx,
Iva	Iva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atmosféra	atmosféra	k1gFnSc1
a	a	k8xC
klima	klima	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
246	#num#	k4
<g/>
-	-	kIx~
<g/>
1598	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Důležité	důležitý	k2eAgNnSc1d1
absorbující	absorbující	k2eAgInPc1d1
plyny	plyn	k1gInPc1
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
fotolýza	fotolýza	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
101	#num#	k4
<g/>
-	-	kIx~
<g/>
103	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
https://techxplore.com/news/2017-09-energy-harvested-evaporation-power.html	https://techxplore.com/news/2017-09-energy-harvested-evaporation-power.html	k1gInSc4
-	-	kIx~
Energy	Energ	k1gInPc4
harvested	harvested	k1gInSc1
from	froma	k1gFnPc2
evaporation	evaporation	k1gInSc1
could	could	k1gMnSc1
power	power	k1gMnSc1
much	moucha	k1gFnPc2
of	of	k?
US	US	kA
<g/>
,	,	kIx,
says	says	k6eAd1
study	stud	k1gInPc1
<g/>
↑	↑	k?
Here	Here	k1gInSc1
Comes	Comes	k1gInSc1
the	the	k?
Sun	Sun	kA
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
37	#num#	k4
<g/>
.	.	kIx.
minuta	minuta	k1gFnSc1
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
sekunda	sekunda	k1gFnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Sluneční	sluneční	k2eAgFnSc1d1
konstanta	konstanta	k1gFnSc1
</s>
<s>
Obnovitelný	obnovitelný	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
energie	energie	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
největších	veliký	k2eAgFnPc2d3
fotovoltaických	fotovoltaický	k2eAgFnPc2d1
elektráren	elektrárna	k1gFnPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
sluneční	sluneční	k2eAgFnSc2d1
energie	energie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
And	Anda	k1gFnPc2
Here	Here	k1gInSc1
Comes	Comes	k1gMnSc1
the	the	k?
Sun	sun	k1gInSc1
–	–	k?
nizozemský	nizozemský	k2eAgInSc1d1
dokument	dokument	k1gInSc1
snažící	snažící	k2eAgFnSc2d1
se	se	k3xPyFc4
vyvrátit	vyvrátit	k5eAaPmF
tvrzení	tvrzení	k1gNnSc3
o	o	k7c6
neefektivitě	neefektivita	k1gFnSc6
adaptace	adaptace	k1gFnSc2
sluneční	sluneční	k2eAgFnSc2d1
energie	energie	k1gFnSc2
(	(	kIx(
<g/>
česká	český	k2eAgFnSc1d1
verze	verze	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Solární	solární	k2eAgFnSc1d1
elektrárna	elektrárna	k1gFnSc1
v	v	k7c6
praxi	praxe	k1gFnSc6
<g/>
:	:	kIx,
http://www.nazeleno.cz/energie/fotovoltaika-1/domaci-solarni-elektrarna-kolik-stoji-vyplati-se.aspx	http://www.nazeleno.cz/energie/fotovoltaika-1/domaci-solarni-elektrarna-kolik-stoji-vyplati-se.aspx	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Obnovitelná	obnovitelný	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
biopalivo	biopalivo	k1gNnSc1
•	•	k?
biomasa	biomasa	k1gFnSc1
•	•	k?
geotermální	geotermální	k2eAgFnSc1d1
•	•	k?
přílivová	přílivový	k2eAgFnSc1d1
•	•	k?
solární	solární	k2eAgFnSc1d1
•	•	k?
větrná	větrný	k2eAgFnSc1d1
•	•	k?
vlnová	vlnový	k2eAgFnSc1d1
•	•	k?
vodní	vodní	k2eAgFnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4055572-0	4055572-0	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
13517	#num#	k4
</s>
