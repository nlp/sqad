<p>
<s>
Antigua	Antigua	k1gMnSc1	Antigua
a	a	k8xC	a
Barbuda	Barbuda	k1gMnSc1	Barbuda
je	být	k5eAaImIp3nS	být
ostrovní	ostrovní	k2eAgInSc4d1	ostrovní
stát	stát	k1gInSc4	stát
v	v	k7c6	v
Karibiku	Karibik	k1gInSc6	Karibik
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
ostrovech	ostrov	k1gInPc6	ostrov
(	(	kIx(	(
<g/>
Antigua	Antigua	k1gMnSc1	Antigua
279,6	[number]	k4	279,6
km2	km2	k4	km2
<g/>
,	,	kIx,	,
Barbuda	Barbuda	k1gFnSc1	Barbuda
160,5	[number]	k4	160,5
km2	km2	k4	km2
a	a	k8xC	a
Redonda	Redonda	k1gFnSc1	Redonda
1,5	[number]	k4	1,5
km2	km2	k4	km2
<g/>
)	)	kIx)	)
v	v	k7c6	v
Malých	Malých	k2eAgFnPc6d1	Malých
Antilách	Antily	k1gFnPc6	Antily
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Atlantiku	Atlantik	k1gInSc2	Atlantik
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Karibského	karibský	k2eAgNnSc2d1	Karibské
moře	moře	k1gNnSc2	moře
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
153	[number]	k4	153
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Lidé	člověk	k1gMnPc1	člověk
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
na	na	k7c4	na
Antiguu	Antigua	k1gFnSc4	Antigua
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
asi	asi	k9	asi
před	před	k7c7	před
4000	[number]	k4	4000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1493	[number]	k4	1493
objevil	objevit	k5eAaPmAgMnS	objevit
Antiguu	Antigua	k1gFnSc4	Antigua
Kryštof	Kryštof	k1gMnSc1	Kryštof
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
bělochem	běloch	k1gMnSc7	běloch
<g/>
,	,	kIx,	,
kterého	který	k3yIgNnSc2	který
kdy	kdy	k6eAd1	kdy
domorodí	domorodý	k2eAgMnPc1d1	domorodý
Arawakové	Arawakový	k2eAgFnPc1d1	Arawakový
viděli	vidět	k5eAaImAgMnP	vidět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1620	[number]	k4	1620
ostrov	ostrov	k1gInSc4	ostrov
zabrali	zabrat	k5eAaPmAgMnP	zabrat
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1632	[number]	k4	1632
ho	on	k3xPp3gMnSc4	on
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Britové	Brit	k1gMnPc1	Brit
a	a	k8xC	a
po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
období	období	k1gNnSc6	období
francouzské	francouzský	k2eAgFnSc2d1	francouzská
okupace	okupace	k1gFnSc2	okupace
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
ustanoveno	ustanovit	k5eAaPmNgNnS	ustanovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1667	[number]	k4	1667
britské	britský	k2eAgNnSc4d1	Britské
panství	panství	k1gNnSc4	panství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Antiguy	Antigua	k1gFnSc2	Antigua
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1764	[number]	k4	1764
vojenský	vojenský	k2eAgInSc1d1	vojenský
přístav	přístav	k1gInSc1	přístav
English	English	k1gMnSc1	English
Harbour	Harbour	k1gMnSc1	Harbour
jako	jako	k8xS	jako
základna	základna	k1gFnSc1	základna
pro	pro	k7c4	pro
britskou	britský	k2eAgFnSc4d1	britská
karibskou	karibský	k2eAgFnSc4d1	karibská
flotilu	flotila	k1gFnSc4	flotila
<g/>
.	.	kIx.	.
</s>
<s>
Otroctví	otroctví	k1gNnSc4	otroctví
<g/>
,	,	kIx,	,
využívané	využívaný	k2eAgFnPc4d1	využívaná
k	k	k7c3	k
pěstováním	pěstování	k1gNnSc7	pěstování
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1967	[number]	k4	1967
získala	získat	k5eAaPmAgFnS	získat
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
autonomii	autonomie	k1gFnSc4	autonomie
jako	jako	k8xS	jako
přidružený	přidružený	k2eAgInSc4d1	přidružený
stát	stát	k1gInSc4	stát
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1981	[number]	k4	1981
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Antigua	Antigu	k1gInSc2	Antigu
a	a	k8xC	a
Barbuda	Barbudo	k1gNnSc2	Barbudo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
prohloubila	prohloubit	k5eAaPmAgFnS	prohloubit
na	na	k7c4	na
Barbudě	Barbudě	k1gMnSc4	Barbudě
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
s	s	k7c7	s
ústřední	ústřední	k2eAgFnSc7d1	ústřední
vládou	vláda	k1gFnSc7	vláda
pro	pro	k7c4	pro
omezenou	omezený	k2eAgFnSc4d1	omezená
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
rozhodnutích	rozhodnutí	k1gNnPc6	rozhodnutí
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Barbuda	Barbuda	k1gFnSc1	Barbuda
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
status	status	k1gInSc4	status
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
ostrov	ostrov	k1gInSc1	ostrov
Barbuda	Barbudo	k1gNnSc2	Barbudo
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
Hurikánu	hurikán	k1gInSc2	hurikán
Irma	Irma	k1gFnSc1	Irma
značně	značně	k6eAd1	značně
poničen	poničen	k2eAgInSc4d1	poničen
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
veškeré	veškerý	k3xTgNnSc1	veškerý
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
bylo	být	k5eAaImAgNnS	být
dočasně	dočasně	k6eAd1	dočasně
evakuováno	evakuován	k2eAgNnSc1d1	evakuováno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Antigua	Antigua	k1gMnSc1	Antigua
a	a	k8xC	a
Barbuda	Barbuda	k1gMnSc1	Barbuda
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
,	,	kIx,	,
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
<g/>
.	.	kIx.	.
</s>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
(	(	kIx(	(
<g/>
18	[number]	k4	18
členů	člen	k1gInPc2	člen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
každých	každý	k3xTgNnPc2	každý
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
komora	komora	k1gFnSc1	komora
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
Senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
17	[number]	k4	17
členů	člen	k1gInPc2	člen
jmenovaných	jmenovaná	k1gFnPc2	jmenovaná
generálním	generální	k2eAgMnSc7d1	generální
guvernérem	guvernér	k1gMnSc7	guvernér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Antigua	Antigua	k1gMnSc1	Antigua
a	a	k8xC	a
Barbuda	Barbuda	k1gMnSc1	Barbuda
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
několika	několik	k4yIc2	několik
regionálních	regionální	k2eAgNnPc2d1	regionální
společenství	společenství	k1gNnPc2	společenství
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Organizace	organizace	k1gFnSc1	organizace
východokaribských	východokaribský	k2eAgInPc2d1	východokaribský
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
OECS	OECS	kA	OECS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karibského	karibský	k2eAgNnSc2d1	Karibské
společenství	společenství	k1gNnSc2	společenství
(	(	kIx(	(
<g/>
CARICOM	CARICOM	kA	CARICOM
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bolívarovského	Bolívarovský	k2eAgInSc2d1	Bolívarovský
svazu	svaz	k1gInSc2	svaz
pro	pro	k7c4	pro
lid	lid	k1gInSc4	lid
naší	náš	k3xOp1gFnSc2	náš
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
ALBA	alba	k1gFnSc1	alba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Společenství	společenství	k1gNnSc1	společenství
latinskoamerických	latinskoamerický	k2eAgInPc2d1	latinskoamerický
a	a	k8xC	a
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
CELAC	CELAC	kA	CELAC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sdružení	sdružení	k1gNnSc1	sdružení
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
ACS-AEC	ACS-AEC	k1gFnPc2	ACS-AEC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Regionální	regionální	k2eAgInSc1d1	regionální
bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
RSS	RSS	kA	RSS
<g/>
)	)	kIx)	)
či	či	k8xC	či
Petrocaribe	Petrocarib	k1gInSc5	Petrocarib
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
rozdělení	rozdělení	k1gNnSc1	rozdělení
==	==	k?	==
</s>
</p>
<p>
<s>
Stát	stát	k1gInSc1	stát
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
6	[number]	k4	6
farních	farní	k2eAgInPc2d1	farní
okrsků	okrsek	k1gInPc2	okrsek
(	(	kIx(	(
<g/>
parish	parish	k1gMnSc1	parish
<g/>
,	,	kIx,	,
mn	mn	k?	mn
<g/>
.	.	kIx.	.
č.	č.	k?	č.
parishes	parishes	k1gInSc1	parishes
<g/>
)	)	kIx)	)
a	a	k8xC	a
2	[number]	k4	2
dependencí	dependence	k1gFnPc2	dependence
(	(	kIx(	(
<g/>
dependency	dependenca	k1gFnPc1	dependenca
<g/>
,	,	kIx,	,
mn	mn	k?	mn
<g/>
.	.	kIx.	.
č.	č.	k?	č.
dependencies	dependencies	k1gInSc1	dependencies
<g/>
;	;	kIx,	;
Barbuda	Barbuda	k1gFnSc1	Barbuda
a	a	k8xC	a
Redonda	Redonda	k1gFnSc1	Redonda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
podmínky	podmínka	k1gFnPc1	podmínka
==	==	k?	==
</s>
</p>
<p>
<s>
Antigua	Antigua	k1gFnSc1	Antigua
a	a	k8xC	a
Barbuda	Barbuda	k1gFnSc1	Barbuda
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
na	na	k7c6	na
největší	veliký	k2eAgFnSc6d3	veliký
Antigue	Antigue	k1gFnSc6	Antigue
<g/>
,	,	kIx,	,
100	[number]	k4	100
km	km	kA	km
vzdálené	vzdálený	k2eAgFnSc6d1	vzdálená
Barbudě	Barbudě	k1gFnSc6	Barbudě
a	a	k8xC	a
na	na	k7c6	na
neobydlené	obydlený	k2eNgFnSc6d1	neobydlená
skalnaté	skalnatý	k2eAgFnSc6d1	skalnatá
Redondě	Redonda	k1gFnSc6	Redonda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
ostrova	ostrov	k1gInSc2	ostrov
Antigua	Antigu	k1gInSc2	Antigu
se	se	k3xPyFc4	se
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
100	[number]	k4	100
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
zvedá	zvedat	k5eAaImIp3nS	zvedat
silně	silně	k6eAd1	silně
rozčleněná	rozčleněný	k2eAgFnSc1d1	rozčleněná
vápencová	vápencový	k2eAgFnSc1d1	vápencová
tabule	tabule	k1gFnSc1	tabule
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
zbytek	zbytek	k1gInSc1	zbytek
denudované	denudovaný	k2eAgFnSc2d1	denudovaný
sopky	sopka	k1gFnSc2	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Zarovnaný	zarovnaný	k2eAgInSc4d1	zarovnaný
povrch	povrch	k1gInSc4	povrch
Barbudy	Barbuda	k1gFnSc2	Barbuda
utvářejí	utvářet	k5eAaImIp3nP	utvářet
korálové	korálový	k2eAgInPc4d1	korálový
vápence	vápenec	k1gInPc4	vápenec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
300	[number]	k4	300
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
se	se	k3xPyFc4	se
zvedající	zvedající	k2eAgFnPc1d1	zvedající
skály	skála	k1gFnPc1	skála
ostrova	ostrov	k1gInSc2	ostrov
Redonda	Redond	k1gMnSc2	Redond
mají	mít	k5eAaImIp3nP	mít
sopečný	sopečný	k2eAgInSc4d1	sopečný
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýše	nejvýše	k6eAd1	nejvýše
položeným	položený	k2eAgNnSc7d1	položené
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
Boggy	Bogg	k1gMnPc4	Bogg
Peak	Peak	k1gInSc1	Peak
s	s	k7c7	s
402	[number]	k4	402
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Antigua	Antigua	k1gMnSc1	Antigua
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
ze	z	k7c2	z
Závětrných	závětrný	k2eAgMnPc2d1	závětrný
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
mají	mít	k5eAaImIp3nP	mít
teplé	teplé	k1gNnSc4	teplé
<g/>
,	,	kIx,	,
tropické	tropický	k2eAgNnSc4d1	tropické
klima	klima	k1gNnSc4	klima
s	s	k7c7	s
vyrovnanými	vyrovnaný	k2eAgFnPc7d1	vyrovnaná
teplotami	teplota	k1gFnPc7	teplota
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Turistika	turistika	k1gFnSc1	turistika
stále	stále	k6eAd1	stále
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovinu	polovina	k1gFnSc4	polovina
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Nízký	nízký	k2eAgInSc1d1	nízký
počet	počet	k1gInSc1	počet
turistů	turist	k1gMnPc2	turist
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
zpomalil	zpomalit	k5eAaPmAgInS	zpomalit
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
znamenal	znamenat	k5eAaImAgInS	znamenat
napjatější	napjatý	k2eAgInSc1d2	napjatější
státní	státní	k2eAgInSc1d1	státní
rozpočet	rozpočet	k1gInSc1	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
produkce	produkce	k1gFnSc1	produkce
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
domácí	domácí	k2eAgInSc4d1	domácí
trh	trh	k1gInSc4	trh
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
malými	malý	k2eAgFnPc7d1	malá
zásobami	zásoba	k1gFnPc7	zásoba
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
nedostatkem	nedostatek	k1gInSc7	nedostatek
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
raději	rád	k6eAd2	rád
za	za	k7c4	za
vyšší	vysoký	k2eAgInPc4d2	vyšší
platy	plat	k1gInPc4	plat
v	v	k7c6	v
turismu	turismus	k1gInSc6	turismus
nebo	nebo	k8xC	nebo
na	na	k7c6	na
stavbách	stavba	k1gFnPc6	stavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
Antiguy	Antigua	k1gFnSc2	Antigua
a	a	k8xC	a
Barbudy	Barbuda	k1gFnSc2	Barbuda
je	být	k5eAaImIp3nS	být
orientováno	orientovat	k5eAaBmNgNnS	orientovat
na	na	k7c4	na
pěstování	pěstování	k1gNnSc4	pěstování
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnSc1	ovoce
a	a	k8xC	a
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
dobytek	dobytek	k1gInSc1	dobytek
a	a	k8xC	a
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
montáž	montáž	k1gFnSc4	montáž
na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
a	a	k8xC	a
řemeslnou	řemeslný	k2eAgFnSc4d1	řemeslná
výrobu	výroba	k1gFnSc4	výroba
<g/>
;	;	kIx,	;
hlavními	hlavní	k2eAgInPc7d1	hlavní
produkty	produkt	k1gInPc7	produkt
jsou	být	k5eAaImIp3nP	být
lůžkoviny	lůžkovina	k1gFnSc2	lůžkovina
a	a	k8xC	a
elektronické	elektronický	k2eAgFnSc2d1	elektronická
součástky	součástka	k1gFnSc2	součástka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
potomci	potomek	k1gMnPc1	potomek
otroků	otrok	k1gMnPc2	otrok
<g/>
,	,	kIx,	,
dovezených	dovezený	k2eAgFnPc2d1	dovezená
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pracovali	pracovat	k5eAaImAgMnP	pracovat
na	na	k7c6	na
plantážích	plantáž	k1gFnPc6	plantáž
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
také	také	k9	také
skupiny	skupina	k1gFnPc1	skupina
Evropanů	Evropan	k1gMnPc2	Evropan
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
mluví	mluvit	k5eAaImIp3nS	mluvit
směsicí	směsice	k1gFnSc7	směsice
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
,	,	kIx,	,
místních	místní	k2eAgInPc2d1	místní
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
nějakého	nějaký	k3yIgInSc2	nějaký
dalšího	další	k2eAgInSc2d1	další
evropského	evropský	k2eAgInSc2d1	evropský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Téměř	téměř	k6eAd1	téměř
všichni	všechen	k3xTgMnPc1	všechen
obyvatelé	obyvatel	k1gMnPc1	obyvatel
jsou	být	k5eAaImIp3nP	být
křesťané	křesťan	k1gMnPc1	křesťan
s	s	k7c7	s
největším	veliký	k2eAgNnSc7d3	veliký
zastoupením	zastoupení	k1gNnSc7	zastoupení
anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
negramotnosti	negramotnost	k1gFnSc2	negramotnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
5	[number]	k4	5
%	%	kIx~	%
<g/>
,	,	kIx,	,
úroveň	úroveň	k1gFnSc1	úroveň
vzdělání	vzdělání	k1gNnSc2	vzdělání
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
karibskými	karibský	k2eAgInPc7d1	karibský
státy	stát	k1gInPc7	stát
poměrně	poměrně	k6eAd1	poměrně
vysoká	vysoká	k1gFnSc1	vysoká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KAŠPAR	Kašpar	k1gMnSc1	Kašpar
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Karibské	karibský	k2eAgFnSc2d1	karibská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
557	[number]	k4	557
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Antigua	Antigu	k1gInSc2	Antigu
a	a	k8xC	a
Barbuda	Barbudo	k1gNnSc2	Barbudo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Antigua	Antigua	k1gFnSc1	Antigua
a	a	k8xC	a
Barbuda	Barbuda	k1gFnSc1	Barbuda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Antigua	Antigu	k1gInSc2	Antigu
a	a	k8xC	a
Barbuda	Barbudo	k1gNnSc2	Barbudo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Antigua	Antigu	k2eAgFnSc1d1	Antigua
and	and	k?	and
Barbuda	Barbuda	k1gFnSc1	Barbuda
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
Western	western	k1gInSc1	western
Hemispehere	Hemispeher	k1gInSc5	Hemispeher
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Antigua	Antigu	k2eAgFnSc1d1	Antigua
and	and	k?	and
Barbuda	Barbuda	k1gFnSc1	Barbuda
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-08-12	[number]	k4	2011-08-12
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Antigua	Antigu	k2eAgFnSc1d1	Antigua
and	and	k?	and
Barbuda	Barbuda	k1gFnSc1	Barbuda
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Caracasu	Caracas	k1gInSc6	Caracas
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Antigua	Antigua	k1gFnSc1	Antigua
a	a	k8xC	a
Barbuda	Barbuda	k1gFnSc1	Barbuda
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-01-31	[number]	k4	2011-01-31
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MOMSEN	MOMSEN	kA	MOMSEN
<g/>
,	,	kIx,	,
Janet	Janet	k1gInSc1	Janet
D	D	kA	D
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Antigua	Antigu	k2eAgFnSc1d1	Antigua
and	and	k?	and
Barbuda	Barbuda	k1gFnSc1	Barbuda
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
