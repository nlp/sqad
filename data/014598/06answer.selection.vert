<s>
Rychlost	rychlost	k1gFnSc1
světla	světlo	k1gNnSc2
v	v	k7c6
dokonalém	dokonalý	k2eAgNnSc6d1
vakuu	vakuum	k1gNnSc6
je	být	k5eAaImIp3nS
univerzální	univerzální	k2eAgNnSc1d1
fyzikální	fyzikální	k2eAgFnSc7d1
konstantou	konstanta	k1gFnSc7
s	s	k7c7
hodnotou	hodnota	k1gFnSc7
c	c	k0
=	=	kIx~
299	#num#	k4
792	#num#	k4
458	#num#	k4
m	m	kA
<g/>
·	·	kIx.
<g/>
s	s	k7c7
<g/>
−	−	kIx~
<g/>
1	#num#	k4
(	(	kIx(
<g/>
z	z	k7c2
definice	definice	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
přesně	přesně	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>