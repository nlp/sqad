<s>
Animismus	animismus	k1gInSc1	animismus
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
anima	animo	k1gNnSc2	animo
<g/>
,	,	kIx,	,
duše	duše	k1gFnSc2	duše
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
víra	víra	k1gFnSc1	víra
v	v	k7c6	v
existenci	existence	k1gFnSc6	existence
nesmrtelné	smrtelný	k2eNgFnSc2d1	nesmrtelná
<g/>
,	,	kIx,	,
samostatné	samostatný	k2eAgFnSc2d1	samostatná
duše	duše	k1gFnSc2	duše
a	a	k8xC	a
duchovních	duchovní	k2eAgFnPc2d1	duchovní
bytostí	bytost	k1gFnPc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
teorií	teorie	k1gFnPc2	teorie
o	o	k7c6	o
původu	původ	k1gInSc6	původ
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
prvním	první	k4xOgMnSc7	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
tuto	tento	k3xDgFnSc4	tento
koncepci	koncepce	k1gFnSc4	koncepce
sestavil	sestavit	k5eAaPmAgMnS	sestavit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Edward	Edward	k1gMnSc1	Edward
Burnett	Burnett	k1gMnSc1	Burnett
Tylor	Tylor	k1gMnSc1	Tylor
<g/>
.	.	kIx.	.
</s>
<s>
Tylor	Tylor	k1gMnSc1	Tylor
spatřoval	spatřovat	k5eAaImAgMnS	spatřovat
původ	původ	k1gInSc4	původ
náboženství	náboženství	k1gNnSc2	náboženství
z	z	k7c2	z
kdysi	kdysi	k6eAd1	kdysi
všeobecně	všeobecně	k6eAd1	všeobecně
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
víry	víra	k1gFnPc4	víra
v	v	k7c4	v
nesmrtelnou	smrtelný	k2eNgFnSc4d1	nesmrtelná
<g/>
,	,	kIx,	,
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
duši	duše	k1gFnSc4	duše
<g/>
.	.	kIx.	.
</s>
<s>
Víra	víra	k1gFnSc1	víra
v	v	k7c6	v
duši	duše	k1gFnSc6	duše
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
z	z	k7c2	z
dualismu	dualismus	k1gInSc2	dualismus
lidské	lidský	k2eAgFnSc2d1	lidská
zkušenosti	zkušenost	k1gFnSc2	zkušenost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
člověk	člověk	k1gMnSc1	člověk
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
konfrontovat	konfrontovat	k5eAaBmF	konfrontovat
každodenní	každodenní	k2eAgFnSc2d1	každodenní
zkušenosti	zkušenost	k1gFnSc2	zkušenost
se	s	k7c7	s
zážitky	zážitek	k1gInPc7	zážitek
např.	např.	kA	např.
ze	z	k7c2	z
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Nedokázal	dokázat	k5eNaPmAgInS	dokázat
si	se	k3xPyFc3	se
racionálně	racionálně	k6eAd1	racionálně
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
ve	v	k7c6	v
snech	sen	k1gInPc6	sen
a	a	k8xC	a
těmi	ten	k3xDgMnPc7	ten
"	"	kIx"	"
<g/>
opravdovými	opravdový	k2eAgMnPc7d1	opravdový
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
si	se	k3xPyFc3	se
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
mrtvým	mrtvý	k2eAgNnSc7d1	mrtvé
a	a	k8xC	a
živým	živý	k2eAgNnSc7d1	živé
tělem	tělo	k1gNnSc7	tělo
apod.	apod.	kA	apod.
Výsledkem	výsledek	k1gInSc7	výsledek
těchto	tento	k3xDgFnPc2	tento
protikladných	protikladný	k2eAgFnPc2d1	protikladná
zkušeností	zkušenost	k1gFnPc2	zkušenost
a	a	k8xC	a
úsilí	úsilí	k1gNnSc2	úsilí
o	o	k7c4	o
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
víra	víra	k1gFnSc1	víra
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
duši	duše	k1gFnSc4	duše
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
stojí	stát	k5eAaImIp3nS	stát
mimo	mimo	k7c4	mimo
každodenní	každodenní	k2eAgFnSc4d1	každodenní
zkušenost	zkušenost	k1gFnSc4	zkušenost
a	a	k8xC	a
mimo	mimo	k7c4	mimo
hmotný	hmotný	k2eAgInSc4d1	hmotný
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Animistický	animistický	k2eAgInSc1d1	animistický
model	model	k1gInSc1	model
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Tylora	Tylor	k1gMnSc2	Tylor
pouze	pouze	k6eAd1	pouze
minimální	minimální	k2eAgFnSc7d1	minimální
definicí	definice	k1gFnSc7	definice
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
z	z	k7c2	z
víry	víra	k1gFnSc2	víra
v	v	k7c6	v
duši	duše	k1gFnSc6	duše
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
víra	víra	k1gFnSc1	víra
v	v	k7c4	v
duchy	duch	k1gMnPc4	duch
a	a	k8xC	a
duchovní	duchovní	k2eAgFnPc4d1	duchovní
bytosti	bytost	k1gFnPc4	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
představy	představa	k1gFnPc1	představa
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
polyteismu	polyteismus	k1gInSc3	polyteismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
později	pozdě	k6eAd2	pozdě
vyústil	vyústit	k5eAaPmAgInS	vyústit
až	až	k9	až
v	v	k7c4	v
monoteismus	monoteismus	k1gInSc4	monoteismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
antropologii	antropologie	k1gFnSc6	antropologie
většina	většina	k1gFnSc1	většina
animistických	animistický	k2eAgFnPc2d1	animistická
představ	představa	k1gFnPc2	představa
spadá	spadat	k5eAaPmIp3nS	spadat
pod	pod	k7c4	pod
model	model	k1gInSc4	model
náboženského	náboženský	k2eAgNnSc2d1	náboženské
chování	chování	k1gNnSc2	chování
zvaný	zvaný	k2eAgInSc1d1	zvaný
představy	představa	k1gFnPc4	představa
o	o	k7c6	o
světech	svět	k1gInPc6	svět
duchů	duch	k1gMnPc2	duch
a	a	k8xC	a
duší	duše	k1gFnPc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Představy	představa	k1gFnPc1	představa
o	o	k7c6	o
světech	svět	k1gInPc6	svět
duchů	duch	k1gMnPc2	duch
a	a	k8xC	a
duší	duše	k1gFnPc2	duše
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgInP	založit
na	na	k7c6	na
víře	víra	k1gFnSc6	víra
v	v	k7c6	v
existenci	existence	k1gFnSc6	existence
duchů	duch	k1gMnPc2	duch
a	a	k8xC	a
duší	duše	k1gFnPc2	duše
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
jediných	jediný	k2eAgFnPc2d1	jediná
skutečných	skutečný	k2eAgFnPc2d1	skutečná
bytostí	bytost	k1gFnPc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Těla	tělo	k1gNnPc1	tělo
živých	živý	k2eAgFnPc2d1	živá
bytostí	bytost	k1gFnPc2	bytost
i	i	k8xC	i
neživé	živý	k2eNgInPc1d1	neživý
předměty	předmět	k1gInPc1	předmět
jsou	být	k5eAaImIp3nP	být
pouhými	pouhý	k2eAgFnPc7d1	pouhá
schránkami	schránka	k1gFnPc7	schránka
pro	pro	k7c4	pro
přebývání	přebývání	k1gNnSc4	přebývání
těchto	tento	k3xDgFnPc2	tento
bytostí	bytost	k1gFnPc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
duchy	duch	k1gMnPc7	duch
a	a	k8xC	a
dušemi	duše	k1gFnPc7	duše
tkví	tkvět	k5eAaImIp3nS	tkvět
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
že	že	k8xS	že
duše	duše	k1gFnSc1	duše
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
existenci	existence	k1gFnSc3	existence
schránku	schránka	k1gFnSc4	schránka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
duch	duch	k1gMnSc1	duch
existuje	existovat	k5eAaImIp3nS	existovat
beze	beze	k7c2	beze
schránky	schránka	k1gFnSc2	schránka
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
skutečná	skutečný	k2eAgFnSc1d1	skutečná
podoba	podoba	k1gFnSc1	podoba
je	být	k5eAaImIp3nS	být
neznámá	známý	k2eNgFnSc1d1	neznámá
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
bytostí	bytost	k1gFnPc2	bytost
jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
rovnocenné	rovnocenný	k2eAgFnPc1d1	rovnocenná
<g/>
,	,	kIx,	,
nesmrtelné	smrtelný	k2eNgFnPc1d1	nesmrtelná
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
jedna	jeden	k4xCgFnSc1	jeden
v	v	k7c6	v
druhou	druhý	k4xOgFnSc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
schránka	schránka	k1gFnSc1	schránka
je	být	k5eAaImIp3nS	být
obývána	obývat	k5eAaImNgFnS	obývat
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
duší	duše	k1gFnSc7	duše
<g/>
,	,	kIx,	,
když	když	k8xS	když
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
ještě	ještě	k6eAd1	ještě
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
duch	duch	k1gMnSc1	duch
vede	vést	k5eAaImIp3nS	vést
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
onemocnění	onemocnění	k1gNnSc3	onemocnění
či	či	k8xC	či
posedlosti	posedlost	k1gFnSc3	posedlost
<g/>
.	.	kIx.	.
</s>
<s>
Duše	duše	k1gFnSc1	duše
může	moct	k5eAaImIp3nS	moct
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
své	svůj	k3xOyFgFnSc2	svůj
schránky	schránka	k1gFnSc2	schránka
přecházet	přecházet	k5eAaImF	přecházet
do	do	k7c2	do
světa	svět	k1gInSc2	svět
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
představě	představa	k1gFnSc3	představa
o	o	k7c4	o
udržování	udržování	k1gNnSc4	udržování
rovnováhy	rovnováha	k1gFnSc2	rovnováha
mezi	mezi	k7c7	mezi
dušemi	duše	k1gFnPc7	duše
v	v	k7c6	v
našem	náš	k3xOp1gNnSc6	náš
a	a	k8xC	a
jiném	jiný	k2eAgInSc6d1	jiný
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
