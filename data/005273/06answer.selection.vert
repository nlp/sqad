<s>
Animismus	animismus	k1gInSc1	animismus
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
anima	animo	k1gNnSc2	animo
<g/>
,	,	kIx,	,
duše	duše	k1gFnSc2	duše
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
víra	víra	k1gFnSc1	víra
v	v	k7c6	v
existenci	existence	k1gFnSc6	existence
nesmrtelné	smrtelný	k2eNgFnSc2d1	nesmrtelná
<g/>
,	,	kIx,	,
samostatné	samostatný	k2eAgFnSc2d1	samostatná
duše	duše	k1gFnSc2	duše
a	a	k8xC	a
duchovních	duchovní	k2eAgFnPc2d1	duchovní
bytostí	bytost	k1gFnPc2	bytost
<g/>
.	.	kIx.	.
</s>
