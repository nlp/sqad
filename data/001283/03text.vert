<s>
Astat	astat	k1gInSc1	astat
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
At	At	k1gFnSc1	At
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Astatinum	Astatinum	k1gInSc1	Astatinum
je	být	k5eAaImIp3nS	být
nejtěžším	těžký	k2eAgInSc7d3	nejtěžší
známým	známý	k2eAgInSc7d1	známý
prvkem	prvek	k1gInSc7	prvek
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
A	A	kA	A
(	(	kIx(	(
<g/>
halogeny	halogen	k1gInPc7	halogen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
nestabilních	stabilní	k2eNgInPc2d1	nestabilní
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
izotopů	izotop	k1gInPc2	izotop
<g/>
.	.	kIx.	.
</s>
<s>
Astat	astat	k1gInSc1	astat
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
stabilní	stabilní	k2eAgInSc4d1	stabilní
izotop	izotop	k1gInSc4	izotop
a	a	k8xC	a
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
v	v	k7c6	v
naprosto	naprosto	k6eAd1	naprosto
nepatrných	nepatrný	k2eAgNnPc6d1	nepatrný
množstvích	množství	k1gNnPc6	množství
jako	jako	k8xC	jako
člen	člen	k1gInSc1	člen
některé	některý	k3yIgNnSc1	některý
z	z	k7c2	z
uranových	uranový	k2eAgNnPc2d1	uranové
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
thoriové	thoriový	k2eAgFnSc2d1	thoriová
rozpadové	rozpadový	k2eAgFnSc2d1	rozpadová
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
dále	daleko	k6eAd2	daleko
radioaktivně	radioaktivně	k6eAd1	radioaktivně
přeměňuje	přeměňovat	k5eAaImIp3nS	přeměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
objeven	objevit	k5eAaPmNgInS	objevit
teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
pomocí	pomoc	k1gFnPc2	pomoc
cyklotronem	cyklotron	k1gInSc7	cyklotron
iniciované	iniciovaný	k2eAgFnSc2d1	iniciovaná
přeměny	přeměna	k1gFnSc2	přeměna
izotopu	izotop	k1gInSc2	izotop
bismutu	bismut	k1gInSc2	bismut
209	[number]	k4	209
<g/>
Bi	Bi	k1gFnPc2	Bi
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
přibližně	přibližně	k6eAd1	přibližně
dvacet	dvacet	k4xCc1	dvacet
radioizotopů	radioizotop	k1gInPc2	radioizotop
astatu	astat	k1gInSc2	astat
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejstabilnější	stabilní	k2eAgInSc1d3	nejstabilnější
210	[number]	k4	210
<g/>
At	At	k1gFnPc7	At
má	mít	k5eAaImIp3nS	mít
poločas	poločas	k1gInSc1	poločas
rozpadu	rozpad	k1gInSc6	rozpad
8,3	[number]	k4	8,3
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
nestabilitě	nestabilita	k1gFnSc3	nestabilita
jader	jádro	k1gNnPc2	jádro
astatu	astat	k1gInSc3	astat
nebylo	být	k5eNaImAgNnS	být
doposud	doposud	k6eAd1	doposud
nikdy	nikdy	k6eAd1	nikdy
připraveno	připravit	k5eAaPmNgNnS	připravit
takové	takový	k3xDgNnSc1	takový
množství	množství	k1gNnSc1	množství
tohoto	tento	k3xDgInSc2	tento
prvku	prvek	k1gInSc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nS	by
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
podrobnějším	podrobný	k2eAgInSc7d2	podrobnější
studium	studium	k1gNnSc4	studium
jeho	on	k3xPp3gNnSc2	on
fyzikálního	fyzikální	k2eAgNnSc2d1	fyzikální
a	a	k8xC	a
chemického	chemický	k2eAgNnSc2d1	chemické
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
eka-jódu	ekaódu	k6eAd1	eka-jódu
byla	být	k5eAaImAgFnS	být
předpovězena	předpovězet	k5eAaImNgFnS	předpovězet
již	již	k6eAd1	již
Mendělejevem	Mendělejev	k1gInSc7	Mendělejev
<g/>
.	.	kIx.	.
</s>
<s>
Astat	astat	k1gInSc1	astat
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
připraven	připravit	k5eAaPmNgInS	připravit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
California	Californium	k1gNnSc2	Californium
v	v	k7c4	v
Berkeley	Berkelea	k1gFnPc4	Berkelea
ostřelováním	ostřelování	k1gNnSc7	ostřelování
bismutu	bismut	k1gInSc2	bismut
částicemi	částice	k1gFnPc7	částice
alfa	alfa	k1gNnPc2	alfa
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
sloučenin	sloučenina	k1gFnPc2	sloučenina
astatu	astat	k1gInSc2	astat
bylo	být	k5eAaImAgNnS	být
připraveno	připravit	k5eAaPmNgNnS	připravit
a	a	k8xC	a
studováno	studovat	k5eAaImNgNnS	studovat
v	v	k7c6	v
mikroskopickém	mikroskopický	k2eAgNnSc6d1	mikroskopické
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
rozpadají	rozpadat	k5eAaImIp3nP	rozpadat
vlivem	vliv	k1gInSc7	vliv
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
těchto	tento	k3xDgFnPc2	tento
sloučenin	sloučenina	k1gFnPc2	sloučenina
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
teoretické	teoretický	k2eAgFnPc4d1	teoretická
studie	studie	k1gFnPc4	studie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
i	i	k9	i
možné	možný	k2eAgNnSc1d1	možné
využití	využití	k1gNnSc1	využití
v	v	k7c6	v
nukleární	nukleární	k2eAgFnSc6d1	nukleární
medicíně	medicína	k1gFnSc6	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
žádné	žádný	k3yNgFnPc1	žádný
kyseliny	kyselina	k1gFnPc1	kyselina
odvozené	odvozený	k2eAgFnPc1d1	odvozená
od	od	k7c2	od
astatu	astat	k1gInSc2	astat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
astat	astat	k1gInSc1	astat
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
stabilní	stabilní	k2eAgInSc4d1	stabilní
izotop	izotop	k1gInSc4	izotop
a	a	k8xC	a
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
kyselina	kyselina	k1gFnSc1	kyselina
astatovodíková	astatovodíková	k1gFnSc1	astatovodíková
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Astat	astat	k1gInSc1	astat
má	mít	k5eAaImIp3nS	mít
33	[number]	k4	33
známých	známý	k2eAgInPc2d1	známý
izotopů	izotop	k1gInPc2	izotop
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
jsou	být	k5eAaImIp3nP	být
radioaktivní	radioaktivní	k2eAgInPc1d1	radioaktivní
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
nukleonová	nukleonový	k2eAgNnPc1d1	nukleonový
čísla	číslo	k1gNnPc1	číslo
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
191	[number]	k4	191
až	až	k9	až
223	[number]	k4	223
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
známe	znát	k5eAaImIp1nP	znát
23	[number]	k4	23
metastabilních	metastabilní	k2eAgMnPc2d1	metastabilní
excitovaných	excitovaný	k2eAgMnPc2d1	excitovaný
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Nejdéle	dlouho	k6eAd3	dlouho
žijícím	žijící	k2eAgInSc7d1	žijící
izotopem	izotop	k1gInSc7	izotop
je	být	k5eAaImIp3nS	být
210	[number]	k4	210
<g/>
At	At	k1gFnPc2	At
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
8,3	[number]	k4	8,3
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Nejkratší	krátký	k2eAgInSc4d3	nejkratší
poločas	poločas	k1gInSc4	poločas
rozpadu	rozpad	k1gInSc2	rozpad
ze	z	k7c2	z
známých	známý	k2eAgInPc2d1	známý
izotopů	izotop	k1gInPc2	izotop
má	mít	k5eAaImIp3nS	mít
213	[number]	k4	213
<g/>
At	At	k1gFnPc2	At
(	(	kIx(	(
<g/>
125	[number]	k4	125
ns	ns	k?	ns
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Astatine	astatin	k1gInSc5	astatin
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
astat	astat	k1gInSc4	astat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
astat	astat	k1gInSc1	astat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
astat	astat	k1gInSc1	astat
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
