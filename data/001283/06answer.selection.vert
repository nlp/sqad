<s>
Astat	astat	k1gInSc1	astat
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
At	At	k1gFnSc1	At
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Astatinum	Astatinum	k1gInSc1	Astatinum
je	být	k5eAaImIp3nS	být
nejtěžším	těžký	k2eAgInSc7d3	nejtěžší
známým	známý	k2eAgInSc7d1	známý
prvkem	prvek	k1gInSc7	prvek
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
