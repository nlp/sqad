<s>
Teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
nedělitelných	dělitelný	k2eNgInPc6d1	nedělitelný
atomech	atom	k1gInPc6	atom
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
atomy	atom	k1gInPc4	atom
nazval	nazvat	k5eAaPmAgInS	nazvat
Dalton	Dalton	k1gInSc1	Dalton
<g/>
)	)	kIx)	)
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
vyvrátil	vyvrátit	k5eAaPmAgMnS	vyvrátit
J.	J.	kA	J.
J.	J.	kA	J.
Thomson	Thomson	k1gMnSc1	Thomson
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
katodového	katodový	k2eAgNnSc2d1	katodové
záření	záření	k1gNnSc2	záření
objevil	objevit	k5eAaPmAgInS	objevit
elektron	elektron	k1gInSc1	elektron
–	–	k?	–
tedy	tedy	k8xC	tedy
první	první	k4xOgFnSc4	první
subatomární	subatomární	k2eAgFnSc4d1	subatomární
částici	částice	k1gFnSc4	částice
<g/>
.	.	kIx.	.
</s>
