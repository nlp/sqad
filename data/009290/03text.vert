<p>
<s>
Brkoslav	brkoslav	k1gMnSc1	brkoslav
(	(	kIx(	(
<g/>
Bombycilla	Bombycilla	k1gMnSc1	Bombycilla
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
zpěvných	zpěvný	k2eAgMnPc2d1	zpěvný
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
většinového	většinový	k2eAgInSc2d1	většinový
názoru	názor	k1gInSc2	názor
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jediný	jediný	k2eAgInSc4d1	jediný
rod	rod	k1gInSc4	rod
čeledi	čeleď	k1gFnSc2	čeleď
brkoslavovití	brkoslavovitý	k2eAgMnPc1d1	brkoslavovitý
(	(	kIx(	(
<g/>
Bombycillidae	Bombycillidae	k1gNnSc7	Bombycillidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Brkoslavi	brkoslav	k1gMnPc1	brkoslav
mají	mít	k5eAaImIp3nP	mít
jemné	jemný	k2eAgNnSc4d1	jemné
měkké	měkký	k2eAgNnSc4d1	měkké
peří	peří	k1gNnSc4	peří
(	(	kIx(	(
<g/>
vědecké	vědecký	k2eAgNnSc4d1	vědecké
rodové	rodový	k2eAgNnSc4d1	rodové
jméno	jméno	k1gNnSc4	jméno
Bombycilla	Bombycilla	k1gFnSc1	Bombycilla
je	být	k5eAaImIp3nS	být
Vieillotovým	Vieillotův	k2eAgInSc7d1	Vieillotův
pokusem	pokus	k1gInSc7	pokus
o	o	k7c4	o
latinský	latinský	k2eAgInSc4d1	latinský
překlad	překlad	k1gInSc4	překlad
německého	německý	k2eAgInSc2d1	německý
názvu	název	k1gInSc2	název
Seidenschwänze	Seidenschwänze	k1gFnSc2	Seidenschwänze
=	=	kIx~	=
"	"	kIx"	"
<g/>
hedvábný	hedvábný	k2eAgInSc1d1	hedvábný
ocas	ocas	k1gInSc1	ocas
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
unikátní	unikátní	k2eAgFnSc2d1	unikátní
červené	červený	k2eAgFnSc2d1	červená
špičky	špička	k1gFnSc2	špička
některých	některý	k3yIgNnPc2	některý
křídelních	křídelní	k2eAgNnPc2d1	křídelní
per	pero	k1gNnPc2	pero
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
osten	osten	k1gInSc1	osten
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
prapor	prapor	k1gInSc4	prapor
<g/>
;	;	kIx,	;
u	u	k7c2	u
brkoslava	brkoslav	k1gMnSc2	brkoslav
severního	severní	k2eAgMnSc2d1	severní
(	(	kIx(	(
<g/>
Bombycilla	Bombycill	k1gMnSc2	Bombycill
garrulus	garrulus	k1gMnSc1	garrulus
<g/>
)	)	kIx)	)
a	a	k8xC	a
brkoslava	brkoslav	k1gMnSc2	brkoslav
amerického	americký	k2eAgMnSc2d1	americký
(	(	kIx(	(
<g/>
Bombycilla	Bombycilla	k1gFnSc1	Bombycilla
cedrorum	cedrorum	k1gInSc1	cedrorum
<g/>
)	)	kIx)	)
vypadají	vypadat	k5eAaImIp3nP	vypadat
tyto	tento	k3xDgFnPc1	tento
špičky	špička	k1gFnPc1	špička
jako	jako	k8xS	jako
voskové	voskový	k2eAgInPc1d1	voskový
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
čehož	což	k3yRnSc2	což
získali	získat	k5eAaPmAgMnP	získat
své	svůj	k3xOyFgNnSc4	svůj
anglické	anglický	k2eAgNnSc4d1	anglické
jméno	jméno	k1gNnSc4	jméno
(	(	kIx(	(
<g/>
waxwing	waxwing	k1gInSc1	waxwing
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
krátké	krátký	k2eAgFnPc1d1	krátká
a	a	k8xC	a
silné	silný	k2eAgFnPc1d1	silná
<g/>
,	,	kIx,	,
křídla	křídlo	k1gNnPc4	křídlo
zašpičatělá	zašpičatělý	k2eAgNnPc4d1	zašpičatělé
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
a	a	k8xC	a
samice	samice	k1gFnPc1	samice
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
mají	mít	k5eAaImIp3nP	mít
převážně	převážně	k6eAd1	převážně
hnědé	hnědý	k2eAgNnSc4d1	hnědé
opeření	opeření	k1gNnSc4	opeření
<g/>
,	,	kIx,	,
černou	černý	k2eAgFnSc4d1	černá
masku	maska	k1gFnSc4	maska
<g/>
,	,	kIx,	,
táhnoucí	táhnoucí	k2eAgInPc4d1	táhnoucí
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
oko	oko	k1gNnSc4	oko
<g/>
,	,	kIx,	,
černou	černý	k2eAgFnSc4d1	černá
skvrnu	skvrna	k1gFnSc4	skvrna
na	na	k7c6	na
bradě	brada	k1gFnSc6	brada
<g/>
,	,	kIx,	,
hranatý	hranatý	k2eAgInSc4d1	hranatý
ocas	ocas	k1gInSc4	ocas
s	s	k7c7	s
červeným	červený	k2eAgInSc7d1	červený
nebo	nebo	k8xC	nebo
žlutým	žlutý	k2eAgInSc7d1	žlutý
konečkem	koneček	k1gInSc7	koneček
a	a	k8xC	a
špičatou	špičatý	k2eAgFnSc4d1	špičatá
chocholku	chocholka	k1gFnSc4	chocholka
<g/>
.	.	kIx.	.
</s>
<s>
Zobák	zobák	k1gInSc1	zobák
<g/>
,	,	kIx,	,
oko	oko	k1gNnSc1	oko
a	a	k8xC	a
nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
tmavé	tmavý	k2eAgFnPc1d1	tmavá
<g/>
.	.	kIx.	.
</s>
<s>
Volání	volání	k1gNnSc1	volání
je	být	k5eAaImIp3nS	být
jednoslabičné	jednoslabičný	k2eAgNnSc1d1	jednoslabičné
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
posazené	posazený	k2eAgInPc1d1	posazený
<g/>
,	,	kIx,	,
drnčivé	drnčivý	k2eAgInPc1d1	drnčivý
nebo	nebo	k8xC	nebo
zvonivé	zvonivý	k2eAgInPc1d1	zvonivý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Brkoslavi	brkoslav	k1gMnPc1	brkoslav
jsou	být	k5eAaImIp3nP	být
stromoví	stromový	k2eAgMnPc1d1	stromový
ptáci	pták	k1gMnPc1	pták
severských	severský	k2eAgInPc2d1	severský
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
hlavní	hlavní	k2eAgFnSc7d1	hlavní
potravou	potrava	k1gFnSc7	potrava
jsou	být	k5eAaImIp3nP	být
plody	plod	k1gInPc1	plod
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
od	od	k7c2	od
časného	časný	k2eAgNnSc2d1	časné
léta	léto	k1gNnSc2	léto
(	(	kIx(	(
<g/>
jahody	jahoda	k1gFnPc4	jahoda
<g/>
,	,	kIx,	,
plody	plod	k1gInPc4	plod
moruše	moruše	k1gFnSc2	moruše
a	a	k8xC	a
muchovníku	muchovník	k1gInSc2	muchovník
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
pozdní	pozdní	k2eAgNnSc4d1	pozdní
léto	léto	k1gNnSc4	léto
a	a	k8xC	a
podzim	podzim	k1gInSc4	podzim
(	(	kIx(	(
<g/>
maliny	malina	k1gFnPc1	malina
<g/>
,	,	kIx,	,
ostružiny	ostružina	k1gFnPc1	ostružina
<g/>
,	,	kIx,	,
třešně	třešeň	k1gFnPc1	třešeň
a	a	k8xC	a
plody	plod	k1gInPc1	plod
zimolezu	zimolez	k1gInSc2	zimolez
<g/>
)	)	kIx)	)
do	do	k7c2	do
pozdního	pozdní	k2eAgInSc2d1	pozdní
podzimu	podzim	k1gInSc2	podzim
a	a	k8xC	a
zimy	zima	k1gFnSc2	zima
(	(	kIx(	(
<g/>
plody	plod	k1gInPc1	plod
jalovce	jalovec	k1gInSc2	jalovec
<g/>
,	,	kIx,	,
hrozny	hrozen	k1gInPc1	hrozen
<g/>
,	,	kIx,	,
šípky	šípek	k1gInPc1	šípek
<g/>
,	,	kIx,	,
jablka	jablko	k1gNnPc1	jablko
<g/>
,	,	kIx,	,
jeřabiny	jeřabina	k1gFnPc1	jeřabina
<g/>
,	,	kIx,	,
plody	plod	k1gInPc1	plod
skalníku	skalník	k1gInSc2	skalník
<g/>
,	,	kIx,	,
dřínu	dřín	k1gInSc2	dřín
<g/>
,	,	kIx,	,
jmelí	jmelí	k1gNnSc2	jmelí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
trhají	trhat	k5eAaImIp3nP	trhat
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
a	a	k8xC	a
keřích	keř	k1gInPc6	keř
<g/>
,	,	kIx,	,
příležitostně	příležitostně	k6eAd1	příležitostně
i	i	k9	i
za	za	k7c2	za
letu	let	k1gInSc2	let
jako	jako	k8xS	jako
kolibříci	kolibřík	k1gMnPc1	kolibřík
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
plody	plod	k1gInPc4	plod
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
mízou	míza	k1gFnSc7	míza
<g/>
,	,	kIx,	,
poupaty	poupě	k1gNnPc7	poupě
a	a	k8xC	a
květy	květ	k1gInPc7	květ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teplejších	teplý	k2eAgNnPc6d2	teplejší
obdobích	období	k1gNnPc6	období
roku	rok	k1gInSc2	rok
sbírají	sbírat	k5eAaImIp3nP	sbírat
řadu	řada	k1gFnSc4	řada
druhů	druh	k1gMnPc2	druh
hmyzu	hmyz	k1gInSc2	hmyz
z	z	k7c2	z
listí	listí	k1gNnSc2	listí
<g/>
,	,	kIx,	,
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
nebo	nebo	k8xC	nebo
je	on	k3xPp3gInPc4	on
chytají	chytat	k5eAaImIp3nP	chytat
v	v	k7c6	v
letu	let	k1gInSc6	let
<g/>
;	;	kIx,	;
často	často	k6eAd1	často
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
létající	létající	k2eAgInSc4d1	létající
hmyz	hmyz	k1gInSc4	hmyz
početný	početný	k2eAgInSc4d1	početný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Brkoslavi	brkoslav	k1gMnPc1	brkoslav
si	se	k3xPyFc3	se
často	často	k6eAd1	často
volí	volit	k5eAaImIp3nP	volit
hnízdiště	hnízdiště	k1gNnSc4	hnízdiště
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
nabídkou	nabídka	k1gFnSc7	nabídka
plodů	plod	k1gInPc2	plod
a	a	k8xC	a
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
části	část	k1gFnSc6	část
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
využili	využít	k5eAaPmAgMnP	využít
období	období	k1gNnSc4	období
zrání	zrání	k1gNnSc2	zrání
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Namlouvání	namlouvání	k1gNnSc1	namlouvání
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
začít	začít	k5eAaPmF	začít
probíhat	probíhat	k5eAaImF	probíhat
již	již	k6eAd1	již
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
vytváření	vytváření	k1gNnSc2	vytváření
párů	pár	k1gInPc2	pár
je	být	k5eAaImIp3nS	být
rituál	rituál	k1gInSc4	rituál
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
si	se	k3xPyFc3	se
partneři	partner	k1gMnPc1	partner
předávají	předávat	k5eAaImIp3nP	předávat
několikrát	několikrát	k6eAd1	několikrát
kus	kus	k1gInSc4	kus
ovoce	ovoce	k1gNnSc2	ovoce
nebo	nebo	k8xC	nebo
i	i	k9	i
nejedlé	jedlý	k2eNgInPc1d1	nejedlý
předměty	předmět	k1gInPc1	předmět
(	(	kIx(	(
<g/>
ovoce	ovoce	k1gNnSc1	ovoce
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
partnerů	partner	k1gMnPc2	partner
nakonec	nakonec	k6eAd1	nakonec
sní	snít	k5eAaImIp3nS	snít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
rituálu	rituál	k1gInSc6	rituál
může	moct	k5eAaImIp3nS	moct
následovat	následovat	k5eAaImF	následovat
páření	páření	k1gNnSc4	páření
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
s	s	k7c7	s
bohatými	bohatý	k2eAgInPc7d1	bohatý
zdroji	zdroj	k1gInPc7	zdroj
potravy	potrava	k1gFnSc2	potrava
může	moct	k5eAaImIp3nS	moct
hnízdit	hnízdit	k5eAaImF	hnízdit
mnoho	mnoho	k4c4	mnoho
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
brkoslavi	brkoslav	k1gMnPc1	brkoslav
nejsou	být	k5eNaImIp3nP	být
teritoriální	teritoriální	k2eAgFnSc4d1	teritoriální
-	-	kIx~	-
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
neexistuje	existovat	k5eNaImIp3nS	existovat
pravý	pravý	k2eAgInSc4d1	pravý
zpěv	zpěv	k1gInSc4	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
ptáci	pták	k1gMnPc1	pták
mohou	moct	k5eAaImIp3nP	moct
napadat	napadat	k5eAaBmF	napadat
vetřelce	vetřelec	k1gMnSc4	vetřelec
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
chrání	chránit	k5eAaImIp3nS	chránit
partnera	partner	k1gMnSc4	partner
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
partneři	partner	k1gMnPc1	partner
nosí	nosit	k5eAaImIp3nP	nosit
hnízdní	hnízdní	k2eAgInSc4d1	hnízdní
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
hnízdo	hnízdo	k1gNnSc4	hnízdo
staví	stavit	k5eAaImIp3nS	stavit
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
samice	samice	k1gFnSc2	samice
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
vodorovné	vodorovný	k2eAgFnSc6d1	vodorovná
větvi	větev	k1gFnSc6	větev
nebo	nebo	k8xC	nebo
v	v	k7c6	v
rozvětvení	rozvětvení	k1gNnSc6	rozvětvení
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
kmene	kmen	k1gInSc2	kmen
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
různé	různý	k2eAgFnSc6d1	různá
výšce	výška	k1gFnSc6	výška
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
je	být	k5eAaImIp3nS	být
velké	velký	k2eAgNnSc1d1	velké
<g/>
,	,	kIx,	,
z	z	k7c2	z
řídce	řídce	k6eAd1	řídce
propletených	propletený	k2eAgFnPc2d1	propletená
větviček	větvička	k1gFnPc2	větvička
<g/>
,	,	kIx,	,
trávy	tráva	k1gFnSc2	tráva
a	a	k8xC	a
lišejníků	lišejník	k1gInPc2	lišejník
<g/>
;	;	kIx,	;
samice	samice	k1gFnSc1	samice
jej	on	k3xPp3gMnSc4	on
vystýlá	vystýlat	k5eAaImIp3nS	vystýlat
trávou	tráva	k1gFnSc7	tráva
<g/>
,	,	kIx,	,
mechem	mech	k1gInSc7	mech
a	a	k8xC	a
jehlicemi	jehlice	k1gFnPc7	jehlice
borovic	borovice	k1gFnPc2	borovice
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
jej	on	k3xPp3gNnSc4	on
maskovat	maskovat	k5eAaBmF	maskovat
kousky	kousek	k1gInPc4	kousek
trávy	tráva	k1gFnSc2	tráva
<g/>
,	,	kIx,	,
květů	květ	k1gInPc2	květ
<g/>
,	,	kIx,	,
mechů	mech	k1gInPc2	mech
a	a	k8xC	a
lišejníků	lišejník	k1gInPc2	lišejník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
sedí	sedit	k5eAaImIp3nS	sedit
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
samec	samec	k1gMnSc1	samec
krmí	krmit	k5eAaImIp3nS	krmit
<g/>
,	,	kIx,	,
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
mláďat	mládě	k1gNnPc2	mládě
se	se	k3xPyFc4	se
v	v	k7c6	v
krmení	krmení	k1gNnSc4	krmení
střídají	střídat	k5eAaImIp3nP	střídat
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tah	tah	k1gInSc1	tah
==	==	k?	==
</s>
</p>
<p>
<s>
Brkoslavi	brkoslav	k1gMnPc1	brkoslav
nejsou	být	k5eNaImIp3nP	být
praví	pravý	k2eAgMnPc1d1	pravý
tažní	tažní	k2eAgMnPc1d1	tažní
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
v	v	k7c6	v
mimohnízdní	mimohnízdní	k2eAgFnSc6d1	mimohnízdní
sezóně	sezóna	k1gFnSc6	sezóna
se	se	k3xPyFc4	se
však	však	k9	však
potulují	potulovat	k5eAaImIp3nP	potulovat
mimo	mimo	k7c4	mimo
hnízdiště	hnízdiště	k1gNnPc4	hnízdiště
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
přesunují	přesunovat	k5eAaImIp3nP	přesunovat
se	se	k3xPyFc4	se
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
hnízdního	hnízdní	k2eAgInSc2d1	hnízdní
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
nedostatek	nedostatek	k1gInSc1	nedostatek
plodů	plod	k1gInPc2	plod
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
zalétnout	zalétnout	k5eAaPmF	zalétnout
velké	velký	k2eAgInPc1d1	velký
počty	počet	k1gInPc1	počet
ptáků	pták	k1gMnPc2	pták
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
areálu	areál	k1gInSc2	areál
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
v	v	k7c6	v
hejnech	hejno	k1gNnPc6	hejno
čítajících	čítající	k2eAgNnPc2d1	čítající
až	až	k6eAd1	až
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
řadí	řadit	k5eAaImIp3nP	řadit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
brkoslavovitých	brkoslavovitý	k2eAgInPc2d1	brkoslavovitý
další	další	k2eAgInPc4d1	další
druhy	druh	k1gInPc4	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
palmovníky	palmovník	k1gInPc1	palmovník
(	(	kIx(	(
<g/>
Ptilogonatidae	Ptilogonatidae	k1gFnSc1	Ptilogonatidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hedvábníky	hedvábník	k1gInPc1	hedvábník
(	(	kIx(	(
<g/>
Hypocoliidae	Hypocoliidae	k1gFnSc1	Hypocoliidae
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
brkoslavce	brkoslavka	k1gFnSc6	brkoslavka
(	(	kIx(	(
<g/>
Dulidae	Dulidae	k1gInSc1	Dulidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnPc1d1	současná
molekulární	molekulární	k2eAgFnPc1d1	molekulární
analýzy	analýza	k1gFnPc1	analýza
jejich	jejich	k3xOp3gFnSc4	jejich
příbuznost	příbuznost	k1gFnSc4	příbuznost
potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
<g/>
,	,	kIx,	,
když	když	k8xS	když
identifikovaly	identifikovat	k5eAaBmAgInP	identifikovat
klad	klad	k1gInSc4	klad
zahrnující	zahrnující	k2eAgNnSc1d1	zahrnující
tyto	tento	k3xDgFnPc4	tento
skupiny	skupina	k1gFnPc4	skupina
a	a	k8xC	a
pištce	pištec	k1gMnPc4	pištec
žlutobokého	žlutoboký	k2eAgInSc2d1	žlutoboký
(	(	kIx(	(
<g/>
Hylocitrea	Hylocitre	k1gInSc2	Hylocitre
bonensis	bonensis	k1gInSc1	bonensis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
patří	patřit	k5eAaImIp3nP	patřit
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
brkoslavů	brkoslav	k1gMnPc2	brkoslav
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bombycilla	Bombycilla	k1gMnSc1	Bombycilla
garrulus	garrulus	k1gMnSc1	garrulus
<g/>
,	,	kIx,	,
brkoslav	brkoslav	k1gMnSc1	brkoslav
severní	severní	k2eAgFnSc2d1	severní
</s>
</p>
<p>
<s>
B.	B.	kA	B.
cedrorum	cedrorum	k1gInSc1	cedrorum
<g/>
,	,	kIx,	,
brkoslav	brkoslav	k1gMnSc1	brkoslav
americký	americký	k2eAgMnSc1d1	americký
</s>
</p>
<p>
<s>
B.	B.	kA	B.
japonica	japonica	k1gMnSc1	japonica
<g/>
,	,	kIx,	,
brkoslav	brkoslav	k1gMnSc1	brkoslav
japonský	japonský	k2eAgMnSc1d1	japonský
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Waxwing	Waxwing	k1gInSc1	Waxwing
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
brkoslav	brkoslav	k1gMnSc1	brkoslav
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Bombycilla	Bombycillo	k1gNnSc2	Bombycillo
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
