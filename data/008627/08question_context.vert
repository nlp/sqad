<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
spektrální	spektrální	k2eAgFnPc1d1	spektrální
třídy	třída	k1gFnPc1	třída
G	G	kA	G
<g/>
2	[number]	k4	2
<g/>
V.	V.	kA	V.
Obíhá	obíhat	k5eAaImIp3nS	obíhat
okolo	okolo	k7c2	okolo
středu	střed	k1gInSc2	střed
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
25	[number]	k4	25
000	[number]	k4	000
do	do	k7c2	do
28	[number]	k4	28
000	[number]	k4	000
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Oběh	oběh	k1gInSc1	oběh
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
226	[number]	k4	226
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
centrum	centrum	k1gNnSc1	centrum
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
1	[number]	k4	1
AU	au	k0	au
(	(	kIx(	(
<g/>
asi	asi	k9	asi
150	[number]	k4	150
milionů	milion	k4xCgInPc2	milion
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>

