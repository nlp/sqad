<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
spektrální	spektrální	k2eAgFnPc1d1	spektrální
třídy	třída	k1gFnPc1	třída
G	G	kA	G
<g/>
2	[number]	k4	2
<g/>
V.	V.	kA	V.
Obíhá	obíhat	k5eAaImIp3nS	obíhat
okolo	okolo	k7c2	okolo
středu	střed	k1gInSc2	střed
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
25	[number]	k4	25
000	[number]	k4	000
do	do	k7c2	do
28	[number]	k4	28
000	[number]	k4	000
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
