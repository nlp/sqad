<s>
Klávesa	klávesa	k1gFnSc1
Alt	Alt	kA
</s>
<s>
Klávesy	kláves	k1gInPc1
Ctrl	Ctrl	kA
<g/>
,	,	kIx,
Windows	Windows	kA
<g/>
,	,	kIx,
Alt	Alt	kA
</s>
<s>
Klávesa	klávesa	k1gFnSc1
Alt	Alt	kA
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
počítačové	počítačový	k2eAgFnSc6d1
klávesnici	klávesnice	k1gFnSc6
vlevo	vlevo	k6eAd1
vedle	vedle	k7c2
mezerníku	mezerník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alt	alt	k1gInSc1
lze	lze	k6eAd1
chápat	chápat	k5eAaImF
jako	jako	k9
zkratku	zkratka	k1gFnSc4
pro	pro	k7c4
slovo	slovo	k1gNnSc4
alternativa	alternativa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k8xS,k8xC
modifikační	modifikační	k2eAgFnSc1d1
klávesa	klávesa	k1gFnSc1
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
klávesa	klávesa	k1gFnSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
kbd	kbd	k?
<g/>
.	.	kIx.
<g/>
Sablona__Klavesa	Sablona__Klavesa	k1gFnSc1
<g/>
{	{	kIx(
<g/>
background-color	background-color	k1gInSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
F	F	kA
<g/>
7	#num#	k4
<g/>
F	F	kA
<g/>
7	#num#	k4
<g/>
F	F	kA
<g/>
7	#num#	k4
<g/>
;	;	kIx,
<g/>
background-image	background-imag	k1gInSc2
<g/>
:	:	kIx,
<g/>
linear-gradient	linear-gradient	k1gInSc1
<g/>
(	(	kIx(
<g/>
rgba	rgba	k1gFnSc1
<g/>
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
255,255,255	255,255,255	k4
<g/>
,	,	kIx,
<g/>
.4	.4	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
rgba	rgba	k1gMnSc1
<g/>
(	(	kIx(
<g/>
0,0	0,0	k4
<g/>
,0	,0	k4
<g/>
,	,	kIx,
<g/>
.1	.1	k4
<g/>
))	))	k?
<g/>
;	;	kIx,
<g/>
border	border	k1gInSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
px	px	k?
solid	solid	k1gInSc1
<g/>
;	;	kIx,
<g/>
border-color	border-color	k1gInSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
DDD	DDD	kA
#	#	kIx~
<g/>
a	a	k8xC
<g/>
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
b	b	k?
<g/>
1	#num#	k4
#	#	kIx~
<g/>
888	#num#	k4
#	#	kIx~
<g/>
CCC	CCC	kA
<g/>
;	;	kIx,
<g/>
border-radius	border-radius	k1gMnSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
px	px	k?
<g/>
;	;	kIx,
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
.4	.4	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
text-shadow	text-shadow	k?
<g/>
:	:	kIx,
<g/>
0	#num#	k4
1	#num#	k4
<g/>
px	px	k?
rgba	rgba	k1gMnSc1
<g/>
(	(	kIx(
<g/>
255,255,255	255,255,255	k4
<g/>
,	,	kIx,
<g/>
.5	.5	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
<g/>
white-space	white-space	k1gFnSc1
<g/>
:	:	kIx,
<g/>
nowrap	nowrap	k1gInSc1
<g/>
}	}	kIx)
<g/>
⇧	⇧	k?
Shift	Shift	kA
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
klávesou	klávesa	k1gFnSc7
Ctrl	Ctrl	kA
představuje	představovat	k5eAaImIp3nS
třetí	třetí	k4xOgFnSc4
možnost	možnost	k1gFnSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
změnit	změnit	k5eAaPmF
význam	význam	k1gInSc4
původní	původní	k2eAgFnSc2d1
klávesy	klávesa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Klávesa	klávesa	k1gFnSc1
Alt	Alt	kA
se	se	k3xPyFc4
nepoužívá	používat	k5eNaImIp3nS
samostatně	samostatně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
vždy	vždy	k6eAd1
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
jinou	jiný	k2eAgFnSc7d1
klávesou	klávesa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimkou	výjimka	k1gFnSc7
jsou	být	k5eAaImIp3nP
některé	některý	k3yIgFnPc1
počítačové	počítačový	k2eAgFnPc1d1
hry	hra	k1gFnPc1
nebo	nebo	k8xC
aktivace	aktivace	k1gFnSc1
menu	menu	k1gNnSc2
v	v	k7c6
operačním	operační	k2eAgInSc6d1
systému	systém	k1gInSc6
Microsoft	Microsoft	kA
Windows	Windows	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Adobe	Adobe	kA
Photoshop	Photoshop	k1gInSc4
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
klávesa	klávesa	k1gFnSc1
Alt	alt	k1gInSc4
na	na	k7c4
kopírování	kopírování	k1gNnSc4
+	+	kIx~
přesun	přesun	k1gInSc1
obrazů	obraz	k1gInPc2
nebo	nebo	k8xC
písmen	písmeno	k1gNnPc2
</s>
<s>
Anglická	anglický	k2eAgFnSc1d1
klávesnice	klávesnice	k1gFnSc1
má	mít	k5eAaImIp3nS
druhé	druhý	k4xOgNnSc1
tlačítko	tlačítko	k1gNnSc1
Alt	Alt	kA
na	na	k7c6
pravé	pravý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
mezerníku	mezerník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
klávesnice	klávesnice	k1gFnSc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
mnoho	mnoho	k4c1
jiných	jiný	k2eAgFnPc2d1
neanglických	anglický	k2eNgFnPc2d1
klávesnic	klávesnice	k1gFnPc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
na	na	k7c6
pravé	pravý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
od	od	k7c2
mezerníku	mezerník	k1gInSc2
jinou	jiný	k2eAgFnSc4d1
klávesu	klávesa	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
bývá	bývat	k5eAaImIp3nS
označená	označený	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
buď	buď	k8xC
stejně	stejně	k6eAd1
jako	jako	k9
Alt	Alt	kA
nebo	nebo	k8xC
Alt	Alt	kA
Gr	Gr	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
pravý	pravý	k2eAgInSc1d1
alt	alt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klávesa	klávesa	k1gFnSc1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
zadávání	zadávání	k1gNnSc3
dalších	další	k2eAgInPc2d1
zvláštních	zvláštní	k2eAgInPc2d1
znaků	znak	k1gInPc2
příslušného	příslušný	k2eAgInSc2d1
cizího	cizí	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
počítačích	počítač	k1gInPc6
Macintosh	Macintosh	kA
existuje	existovat	k5eAaImIp3nS
ekvivalentní	ekvivalentní	k2eAgFnSc1d1
klávesa	klávesa	k1gFnSc1
⌥	⌥	k?
Option	Option	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
volba	volba	k1gFnSc1
<g/>
)	)	kIx)
se	s	k7c7
značkou	značka	k1gFnSc7
„	„	k?
<g/>
výhybky	výhybka	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
k	k	k7c3
psaní	psaní	k1gNnSc3
zvláštních	zvláštní	k2eAgInPc2d1
znaků	znak	k1gInPc2
(	(	kIx(
<g/>
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
v	v	k7c6
jiných	jiný	k2eAgInPc6d1
systémech	systém	k1gInPc6
pouze	pouze	k6eAd1
pravý	pravý	k2eAgMnSc1d1
Alt	Alt	kA
<g/>
)	)	kIx)
a	a	k8xC
jako	jako	k9
modifikátor	modifikátor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
typické	typický	k2eAgFnSc6d1
klávesnici	klávesnice	k1gFnSc6
pro	pro	k7c4
Macintoshe	Macintoshe	k1gInSc4
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
mezi	mezi	k7c7
klávesami	klávesa	k1gFnPc7
Ctrl	Ctrl	kA
a	a	k8xC
⌘	⌘	k?
Command	Command	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Známé	známý	k2eAgFnPc1d1
kombinace	kombinace	k1gFnPc1
</s>
<s>
Alt	Alt	kA
+	+	kIx~
Tab	tab	kA
↹	↹	k?
umožňuje	umožňovat	k5eAaImIp3nS
přepínat	přepínat	k5eAaImF
mezi	mezi	k7c7
běžícími	běžící	k2eAgInPc7d1
programy	program	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
Alt	Alt	kA
+	+	kIx~
Del	Del	k1gMnSc1
vyvolává	vyvolávat	k5eAaImIp3nS
v	v	k7c6
MS	MS	kA
Windows	Windows	kA
správce	správce	k1gMnSc2
úloh	úloha	k1gFnPc2
nebo	nebo	k8xC
v	v	k7c6
OS	OS	kA
Unix	Unix	k1gInSc4
restartuje	restartovat	k5eAaBmIp3nS
počítač	počítač	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
Alt	Alt	kA
+	+	kIx~
F1	F1	k1gMnSc1
až	až	k8xS
F12	F12	k1gMnSc1
vybírá	vybírat	k5eAaImIp3nS
příslušnou	příslušný	k2eAgFnSc4d1
konzoli	konzole	k1gFnSc4
(	(	kIx(
<g/>
Unix	Unix	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
Windows	Windows	kA
v	v	k7c6
mnoha	mnoho	k4c6
aplikacích	aplikace	k1gFnPc6
lze	lze	k6eAd1
přidržením	přidržení	k1gNnSc7
Altu	alt	k1gInSc2
vyvolat	vyvolat	k5eAaPmF
položky	položka	k1gFnPc4
menu	menu	k1gNnSc4
postupným	postupný	k2eAgNnSc7d1
zadáváním	zadávání	k1gNnSc7
podtržených	podtržený	k2eAgNnPc2d1
písmen	písmeno	k1gNnPc2
v	v	k7c6
jejich	jejich	k3xOp3gInSc6
názvu	název	k1gInSc6
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
např.	např.	kA
Alt	Alt	kA
+	+	kIx~
Z	z	k7c2
<g/>
;	;	kIx,
V	V	kA
<g/>
;	;	kIx,
M	M	kA
=	=	kIx~
zvolení	zvolení	k1gNnSc4
položky	položka	k1gFnSc2
Zmenšit	zmenšit	k5eAaPmF
v	v	k7c6
menu	menu	k1gNnSc6
Zobrazit	zobrazit	k5eAaPmF
→	→	k?
Velikost	velikost	k1gFnSc4
stránky	stránka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
Windows	Windows	kA
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
stisknout	stisknout	k5eAaPmF
klávesu	klávesa	k1gFnSc4
Alt	Alt	kA
a	a	k8xC
současně	současně	k6eAd1
na	na	k7c6
numerické	numerický	k2eAgFnSc6d1
klávesnici	klávesnice	k1gFnSc6
zadat	zadat	k5eAaPmF
sekvenci	sekvence	k1gFnSc4
čísel	číslo	k1gNnPc2
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
alt	alt	k1gInSc4
kódování	kódování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Alt	Alt	kA
kódování	kódování	k1gNnSc1
</s>
<s>
Zde	zde	k6eAd1
jsou	být	k5eAaImIp3nP
uvedené	uvedený	k2eAgFnPc1d1
některé	některý	k3yIgFnPc1
klávesové	klávesový	k2eAgFnPc1d1
zkratky	zkratka	k1gFnPc1
s	s	k7c7
klávesou	klávesa	k1gFnSc7
Alt	Alt	kA
ve	v	k7c6
Windows	Windows	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vždy	vždy	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
levý	levý	k2eAgInSc1d1
Alt	alt	k1gInSc1
+	+	kIx~
číslo	číslo	k1gNnSc1
na	na	k7c6
numerické	numerický	k2eAgFnSc6d1
klávesnici	klávesnice	k1gFnSc6
<g/>
,	,	kIx,
např.	např.	kA
</s>
<s>
à	à	k?
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
133	#num#	k4
</s>
<s>
é	é	k0
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
130	#num#	k4
</s>
<s>
í	í	k0
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
161	#num#	k4
</s>
<s>
ó	ó	k0
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
162	#num#	k4
</s>
<s>
á	á	k0
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
160	#num#	k4
</s>
<s>
ú	ú	k0
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
163	#num#	k4
</s>
<s>
ü	ü	k?
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
129	#num#	k4
</s>
<s>
¡	¡	k?
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
173	#num#	k4
</s>
<s>
¿	¿	k?
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
168	#num#	k4
</s>
<s>
ñ	ñ	k?
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
164	#num#	k4
</s>
<s>
Ñ	Ñ	k?
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
165	#num#	k4
</s>
<s>
Á	Á	kA
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
0193	#num#	k4
</s>
<s>
É	É	kA
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
0201	#num#	k4
</s>
<s>
Í	Í	kA
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
0205	#num#	k4
</s>
<s>
Ó	Ó	kA
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
0211	#num#	k4
</s>
<s>
Ú	Ú	kA
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
0218	#num#	k4
</s>
<s>
Ü	Ü	kA
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
0220	#num#	k4
</s>
<s>
©	©	k?
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
0169	#num#	k4
</s>
<s>
®	®	k?
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
0174	#num#	k4
</s>
<s>
™	™	k?
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
0153	#num#	k4
</s>
<s>
[	[	kIx(
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
91	#num#	k4
</s>
<s>
]	]	kIx)
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
93	#num#	k4
</s>
<s>
{	{	kIx(
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
123	#num#	k4
</s>
<s>
}	}	kIx)
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
125	#num#	k4
</s>
<s>
Q	Q	kA
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
81	#num#	k4
</s>
<s>
☺	☺	k?
=	=	kIx~
ALT	Alt	kA
+	+	kIx~
1	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
V	v	k7c6
aplikacích	aplikace	k1gFnPc6
používajících	používající	k2eAgFnPc2d1
ribbon	ribbona	k1gFnPc2
místo	místo	k7c2
roletového	roletový	k2eAgNnSc2d1
menu	menu	k1gNnSc2
(	(	kIx(
<g/>
např.	např.	kA
MS	MS	kA
Office	Office	kA
od	od	k7c2
verze	verze	k1gFnSc2
2007	#num#	k4
<g/>
)	)	kIx)
nejsou	být	k5eNaImIp3nP
podtržená	podtržený	k2eAgNnPc1d1
písmena	písmeno	k1gNnPc1
<g/>
,	,	kIx,
po	po	k7c6
stisknutí	stisknutí	k1gNnSc6
altu	alt	k1gInSc2
se	se	k3xPyFc4
u	u	k7c2
jednotlivých	jednotlivý	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
zobrazí	zobrazit	k5eAaPmIp3nS
funkční	funkční	k2eAgNnPc4d1
písmena	písmeno	k1gNnPc4
v	v	k7c6
černých	černý	k2eAgInPc6d1
rámečcích	rámeček	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Bà	Bà	k1gMnSc1
phím	phím	k1gMnSc1
số	số	k?
na	na	k7c6
vietnamské	vietnamský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
počítačová	počítačový	k2eAgFnSc1d1
klávesnice	klávesnice	k1gFnSc1
QWERTZ	QWERTZ	kA
</s>
<s>
Esc	Esc	k?
</s>
<s>
F1	F1	k4
</s>
<s>
F2	F2	k4
</s>
<s>
F3	F3	k4
</s>
<s>
F4	F4	k4
</s>
<s>
F5	F5	k4
</s>
<s>
F6	F6	k4
</s>
<s>
F7	F7	k4
</s>
<s>
F8	F8	k4
</s>
<s>
F9	F9	k4
</s>
<s>
F10	F10	k4
</s>
<s>
F11	F11	k4
</s>
<s>
F12	F12	k4
</s>
<s>
PrtScSysRq	PrtScSysRq	k?
</s>
<s>
ScrLk	ScrLk	k6eAd1
</s>
<s>
Pause	pausa	k1gFnSc3
</s>
<s>
Ins	Ins	k?
</s>
<s>
Home	Home	k6eAd1
</s>
<s>
PgUp	PgUp	k1gMnSc1
</s>
<s>
NumLk	NumLk	k6eAd1
</s>
<s>
/	/	kIx~
</s>
<s>
*	*	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
Del	Del	k?
</s>
<s>
End	End	k?
</s>
<s>
PgDn	PgDn	k1gMnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
↑	↑	k?
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Ent	Ent	k?
</s>
<s>
←	←	k?
</s>
<s>
↓	↓	k?
</s>
<s>
→	→	k?
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
