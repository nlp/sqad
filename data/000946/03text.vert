<p>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
úředním	úřední	k2eAgInSc7d1	úřední
názvem	název	k1gInSc7	název
Chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
starším	starý	k2eAgInSc7d2	starší
názvem	název	k1gInSc7	název
Charvátsko	Charvátsko	k1gNnSc1	Charvátsko
<g/>
,	,	kIx,	,
Charvátská	charvátský	k2eAgFnSc1d1	Charvátská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
chorvatsky	chorvatsky	k6eAd1	chorvatsky
Hrvatska	Hrvatska	k1gFnSc1	Hrvatska
<g/>
,	,	kIx,	,
Republika	republika	k1gFnSc1	republika
Hrvatska	Hrvatska	k1gFnSc1	Hrvatska
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
evropský	evropský	k2eAgInSc1d1	evropský
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
geograficky	geograficky	k6eAd1	geograficky
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
;	;	kIx,	;
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nástupnických	nástupnický	k2eAgInPc2d1	nástupnický
států	stát	k1gInPc2	stát
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
sousedy	soused	k1gMnPc7	soused
jsou	být	k5eAaImIp3nP	být
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
,	,	kIx,	,
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
a	a	k8xC	a
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
OBSE	OBSE	kA	OBSE
(	(	kIx(	(
<g/>
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
od	od	k7c2	od
22	[number]	k4	22
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
WTO	WTO	kA	WTO
(	(	kIx(	(
<g/>
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
NATO	NATO	kA	NATO
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
i	i	k9	i
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
21	[number]	k4	21
tamních	tamní	k2eAgInPc2d1	tamní
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
žup	župa	k1gFnPc2	župa
<g/>
,	,	kIx,	,
chorvatsky	chorvatsky	k6eAd1	chorvatsky
<g/>
:	:	kIx,	:
županije	županít	k5eAaPmIp3nS	županít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
je	být	k5eAaImIp3nS	být
56	[number]	k4	56
594	[number]	k4	594
km2	km2	k4	km2
<g/>
,	,	kIx,	,
převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
kontinentálním	kontinentální	k2eAgNnSc6d1	kontinentální
podnebí	podnebí	k1gNnSc6	podnebí
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
pak	pak	k6eAd1	pak
v	v	k7c6	v
podnebí	podnebí	k1gNnSc6	podnebí
středozemním	středozemní	k2eAgNnSc6d1	středozemní
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
rozlohy	rozloha	k1gFnSc2	rozloha
je	být	k5eAaImIp3nS	být
započten	započíst	k5eAaPmNgInS	započíst
i	i	k9	i
povrch	povrch	k1gInSc4	povrch
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
000	[number]	k4	000
chorvatských	chorvatský	k2eAgInPc2d1	chorvatský
ostrovů	ostrov	k1gInPc2	ostrov
různých	různý	k2eAgFnPc2d1	různá
velikostí	velikost	k1gFnPc2	velikost
od	od	k7c2	od
největších	veliký	k2eAgMnPc2d3	veliký
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
nad	nad	k7c4	nad
400	[number]	k4	400
km2	km2	k4	km2
(	(	kIx(	(
<g/>
Krk	krk	k1gInSc1	krk
<g/>
,	,	kIx,	,
Cres	Cres	k1gInSc1	Cres
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
menší	malý	k2eAgMnPc4d2	menší
(	(	kIx(	(
<g/>
Brač	Brač	k1gInSc1	Brač
<g/>
,	,	kIx,	,
Hvar	Hvar	k1gInSc1	Hvar
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgInPc4d1	malý
útesy	útes	k1gInPc4	útes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
žije	žít	k5eAaImIp3nS	žít
4,29	[number]	k4	4,29
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nP	hlásit
k	k	k7c3	k
chorvatské	chorvatský	k2eAgFnSc3d1	chorvatská
národnosti	národnost	k1gFnSc3	národnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgMnPc1	první
Slované	Slovan	k1gMnPc1	Slovan
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
dorazili	dorazit	k5eAaPmAgMnP	dorazit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
státní	státní	k2eAgInSc4d1	státní
útvar	útvar	k1gInSc4	útvar
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
vévodstvími	vévodství	k1gNnPc7	vévodství
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
chorvatským	chorvatský	k2eAgMnSc7d1	chorvatský
králem	král	k1gMnSc7	král
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Tomislav	Tomislav	k1gMnSc1	Tomislav
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
925	[number]	k4	925
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
910	[number]	k4	910
do	do	k7c2	do
roku	rok	k1gInSc2	rok
925	[number]	k4	925
vévoda	vévoda	k1gMnSc1	vévoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
samostatné	samostatný	k2eAgNnSc1d1	samostatné
království	království	k1gNnSc1	království
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
vystupovalo	vystupovat	k5eAaImAgNnS	vystupovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1102	[number]	k4	1102
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
do	do	k7c2	do
personální	personální	k2eAgFnSc2d1	personální
unie	unie	k1gFnSc2	unie
s	s	k7c7	s
Uhrami	Uhry	k1gFnPc7	Uhry
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
s	s	k7c7	s
dnešním	dnešní	k2eAgNnSc7d1	dnešní
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1527	[number]	k4	1527
byl	být	k5eAaImAgMnS	být
kvůli	kvůli	k7c3	kvůli
probíhající	probíhající	k2eAgFnSc3d1	probíhající
stoleté	stoletý	k2eAgFnSc3d1	stoletá
válce	válka	k1gFnSc3	válka
(	(	kIx(	(
<g/>
1493	[number]	k4	1493
<g/>
–	–	k?	–
<g/>
1593	[number]	k4	1593
<g/>
)	)	kIx)	)
s	s	k7c7	s
Turky	turek	k1gInPc7	turek
(	(	kIx(	(
<g/>
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
<g/>
)	)	kIx)	)
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
<g/>
,	,	kIx,	,
a	a	k8xC	a
země	zem	k1gFnPc1	zem
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
zůstala	zůstat	k5eAaPmAgFnS	zůstat
součástí	součást	k1gFnSc7	součást
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Chorvaté	Chorvat	k1gMnPc1	Chorvat
se	s	k7c7	s
Slovinci	Slovinec	k1gMnPc7	Slovinec
a	a	k8xC	a
Srby	Srb	k1gMnPc7	Srb
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
mezinárodně	mezinárodně	k6eAd1	mezinárodně
neuznaný	uznaný	k2eNgInSc4d1	neuznaný
stát	stát	k1gInSc4	stát
-	-	kIx~	-
Stát	stát	k1gInSc1	stát
Slovinců	Slovinec	k1gMnPc2	Slovinec
<g/>
,	,	kIx,	,
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
existoval	existovat	k5eAaImAgInS	existovat
jen	jen	k6eAd1	jen
necelý	celý	k2eNgInSc4d1	necelý
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
Království	království	k1gNnSc2	království
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
Slovinců	Slovinec	k1gMnPc2	Slovinec
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
existovalo	existovat	k5eAaImAgNnS	existovat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
zeměmi	zem	k1gFnPc7	zem
založilo	založit	k5eAaPmAgNnS	založit
federativní	federativní	k2eAgFnSc4d1	federativní
republiku	republika	k1gFnSc4	republika
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
setrvalo	setrvat	k5eAaPmAgNnS	setrvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznaná	uznaný	k2eAgFnSc1d1	uznaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
však	však	k9	však
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
Chorvatskou	chorvatský	k2eAgFnSc4d1	chorvatská
válku	válka	k1gFnSc4	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
a	a	k8xC	a
kterou	který	k3yRgFnSc4	který
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgNnSc1d1	moderní
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
18	[number]	k4	18
<g/>
.	.	kIx.	.
nejpopulárnější	populární	k2eAgFnSc4d3	nejpopulárnější
turistickou	turistický	k2eAgFnSc4d1	turistická
destinaci	destinace	k1gFnSc4	destinace
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Turismus	turismus	k1gInSc1	turismus
je	být	k5eAaImIp3nS	být
i	i	k9	i
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
zdrojů	zdroj	k1gInPc2	zdroj
příjmu	příjem	k1gInSc2	příjem
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
podílí	podílet	k5eAaImIp3nS	podílet
se	s	k7c7	s
20	[number]	k4	20
%	%	kIx~	%
na	na	k7c6	na
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělání	vzdělání	k1gNnSc1	vzdělání
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
občany	občan	k1gMnPc4	občan
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
státních	státní	k2eAgFnPc2d1	státní
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
nejdůležitějšími	důležitý	k2eAgMnPc7d3	nejdůležitější
obchodními	obchodní	k2eAgMnPc7d1	obchodní
partnery	partner	k1gMnPc7	partner
jsou	být	k5eAaImIp3nP	být
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgFnPc1d1	historická
země	zem	k1gFnPc1	zem
==	==	k?	==
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgNnSc1d1	moderní
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
Středního	střední	k2eAgNnSc2d1	střední
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
s	s	k7c7	s
Chorvatským	chorvatský	k2eAgNnSc7d1	Chorvatské
Přímořím	Přímoří	k1gNnSc7	Přímoří
(	(	kIx(	(
<g/>
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
až	až	k9	až
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Ilově	Ilova	k1gFnSc3	Ilova
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
Karlovacká	karlovacký	k2eAgFnSc1d1	Karlovacká
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
Rijeka	Rijeka	k1gFnSc1	Rijeka
a	a	k8xC	a
okolí	okolí	k1gNnSc1	okolí
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
Liky	Lika	k1gFnSc2	Lika
až	až	k9	až
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Zrmanji	Zrmanj	k1gFnSc3	Zrmanj
<g/>
;	;	kIx,	;
přirozeným	přirozený	k2eAgNnSc7d1	přirozené
centrem	centrum	k1gNnSc7	centrum
je	být	k5eAaImIp3nS	být
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
a	a	k8xC	a
na	na	k7c6	na
Chorvatském	chorvatský	k2eAgNnSc6d1	Chorvatské
Přímoří	Přímoří	k1gNnSc6	Přímoří
pak	pak	k6eAd1	pak
Rijeka	Rijeka	k1gFnSc1	Rijeka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dalmácie	Dalmácie	k1gFnSc1	Dalmácie
bez	bez	k7c2	bez
Kotoru	Kotor	k1gInSc2	Kotor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
územím	území	k1gNnSc7	území
bývalé	bývalý	k2eAgFnSc2d1	bývalá
republiky	republika	k1gFnSc2	republika
Dubrovník	Dubrovník	k1gInSc1	Dubrovník
<g/>
,	,	kIx,	,
Slavonie	Slavonie	k1gFnSc1	Slavonie
na	na	k7c6	na
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Ilovy	Ilova	k1gFnSc2	Ilova
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
ke	k	k7c3	k
Slavonii	Slavonie	k1gFnSc3	Slavonie
však	však	k9	však
historicky	historicky	k6eAd1	historicky
náleží	náležet	k5eAaImIp3nS	náležet
také	také	k9	také
Sremský	Sremský	k2eAgInSc4d1	Sremský
okruh	okruh	k1gInSc4	okruh
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Srbské	srbský	k2eAgFnSc6d1	Srbská
autonomní	autonomní	k2eAgFnSc6d1	autonomní
oblasti	oblast	k1gFnSc6	oblast
Vojvodině	Vojvodina	k1gFnSc6	Vojvodina
<g/>
;	;	kIx,	;
přirozeným	přirozený	k2eAgNnSc7d1	přirozené
centrem	centrum	k1gNnSc7	centrum
je	být	k5eAaImIp3nS	být
Osijek	Osijka	k1gFnPc2	Osijka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
bývalého	bývalý	k2eAgNnSc2d1	bývalé
markrabství	markrabství	k1gNnSc2	markrabství
Istrie	Istrie	k1gFnSc2	Istrie
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Istrijského	istrijský	k2eAgInSc2d1	istrijský
poloostrova	poloostrov	k1gInSc2	poloostrov
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
,	,	kIx,	,
centrem	centr	k1gInSc7	centr
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
Pula	Pulum	k1gNnSc2	Pulum
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
k	k	k7c3	k
Chorvatsku	Chorvatsko	k1gNnSc3	Chorvatsko
patří	patřit	k5eAaImIp3nS	patřit
ještě	ještě	k6eAd1	ještě
původně	původně	k6eAd1	původně
uherské	uherský	k2eAgFnSc6d1	uherská
oblasti	oblast	k1gFnSc6	oblast
Mezimuří	Mezimuří	k2eAgFnSc6d1	Mezimuří
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Baranje	Baranj	k1gInSc2	Baranj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znaky	znak	k1gInPc1	znak
hlavních	hlavní	k2eAgFnPc2d1	hlavní
zemí	zem	k1gFnPc2	zem
jsou	být	k5eAaImIp3nP	být
vyobrazeny	vyobrazen	k2eAgInPc1d1	vyobrazen
i	i	k9	i
ve	v	k7c6	v
složeném	složený	k2eAgInSc6d1	složený
znaku	znak	k1gInSc6	znak
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kromě	kromě	k7c2	kromě
červenostříbrného	červenostříbrný	k2eAgInSc2d1	červenostříbrný
šachovnicového	šachovnicový	k2eAgInSc2d1	šachovnicový
štítu	štít	k1gInSc2	štít
vlastního	vlastní	k2eAgNnSc2d1	vlastní
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
též	též	k9	též
znaky	znak	k1gInPc1	znak
Slavonie	Slavonie	k1gFnSc2	Slavonie
(	(	kIx(	(
<g/>
černá	černý	k2eAgFnSc1d1	černá
kuna	kuna	k1gFnSc1	kuna
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
řekami	řeka	k1gFnPc7	řeka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dalmácie	Dalmácie	k1gFnSc1	Dalmácie
(	(	kIx(	(
<g/>
tři	tři	k4xCgFnPc1	tři
zlaté	zlatý	k2eAgFnPc1d1	zlatá
korunované	korunovaný	k2eAgFnPc1d1	korunovaná
lví	lví	k2eAgFnPc1d1	lví
hlavy	hlava	k1gFnPc1	hlava
na	na	k7c6	na
modrém	modrý	k2eAgNnSc6d1	modré
poli	pole	k1gNnSc6	pole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Istrie	Istrie	k1gFnSc1	Istrie
(	(	kIx(	(
<g/>
zlatá	zlatý	k2eAgFnSc1d1	zlatá
koza	koza	k1gFnSc1	koza
na	na	k7c6	na
modrém	modrý	k2eAgNnSc6d1	modré
poli	pole	k1gNnSc6	pole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dubrovníku	Dubrovník	k1gInSc2	Dubrovník
(	(	kIx(	(
<g/>
stříbrné	stříbrný	k2eAgInPc4d1	stříbrný
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
na	na	k7c6	na
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
<g/>
)	)	kIx)	)
a	a	k8xC	a
Středního	střední	k2eAgNnSc2d1	střední
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
(	(	kIx(	(
<g/>
půlměsíc	půlměsíc	k1gInSc1	půlměsíc
s	s	k7c7	s
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Starší	starý	k2eAgInPc1d2	starší
úřední	úřední	k2eAgInPc1d1	úřední
názvy	název	k1gInPc1	název
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
===	===	k?	===
</s>
</p>
<p>
<s>
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
Chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
bánovina	bánovina	k1gFnSc1	bánovina
(	(	kIx(	(
<g/>
Banovina	Banovina	k1gFnSc1	Banovina
Hrvatska	Hrvatska	k1gFnSc1	Hrvatska
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Jugoslávského	jugoslávský	k2eAgNnSc2d1	jugoslávské
království	království	k1gNnSc2	království
</s>
</p>
<p>
<s>
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
Nezávislý	závislý	k2eNgInSc1d1	nezávislý
stát	stát	k1gInSc1	stát
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
(	(	kIx(	(
<g/>
Nezavisna	Nezavisen	k2eAgFnSc1d1	Nezavisna
Država	država	k1gFnSc1	država
Hrvatska	Hrvatska	k1gFnSc1	Hrvatska
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1963	[number]	k4	1963
Chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
Narodna	Narodna	k1gFnSc1	Narodna
Republika	republika	k1gFnSc1	republika
Hrvatska	Hrvatska	k1gFnSc1	Hrvatska
–	–	k?	–
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Federativní	federativní	k2eAgFnSc2d1	federativní
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
Chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
Socijalistička	Socijalistička	k1gFnSc1	Socijalistička
Republika	republika	k1gFnSc1	republika
Hrvatska	Hrvatska	k1gFnSc1	Hrvatska
–	–	k?	–
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Socialistické	socialistický	k2eAgFnSc2d1	socialistická
federativní	federativní	k2eAgFnSc2d1	federativní
republiky	republika	k1gFnSc2	republika
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Před	před	k7c7	před
130	[number]	k4	130
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
žili	žít	k5eAaImAgMnP	žít
na	na	k7c6	na
chorvatském	chorvatský	k2eAgNnSc6d1	Chorvatské
území	území	k1gNnSc6	území
významné	významný	k2eAgFnSc2d1	významná
komunity	komunita	k1gFnSc2	komunita
neandrtálců	neandrtálec	k1gMnPc2	neandrtálec
<g/>
.	.	kIx.	.
</s>
<s>
Pozůstatky	pozůstatek	k1gInPc1	pozůstatek
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
nad	nad	k7c7	nad
obcí	obec	k1gFnSc7	obec
Krapina	Krapina	k1gFnSc1	Krapina
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
paleontolog	paleontolog	k1gMnSc1	paleontolog
Dragutin	Dragutin	k1gMnSc1	Dragutin
Gorjanovič	Gorjanovič	k1gMnSc1	Gorjanovič
Kramberger	Kramberger	k1gMnSc1	Kramberger
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
osm	osm	k4xCc1	osm
opracovaných	opracovaný	k2eAgInPc2d1	opracovaný
drápů	dráp	k1gInPc2	dráp
orla	orel	k1gMnSc2	orel
mořského	mořský	k2eAgMnSc2d1	mořský
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
archeologové	archeolog	k1gMnPc1	archeolog
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
za	za	k7c4	za
nejstarší	starý	k2eAgInPc4d3	nejstarší
šperky	šperk	k1gInPc4	šperk
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
<g/>
Existenci	existence	k1gFnSc4	existence
rozvinutého	rozvinutý	k2eAgInSc2d1	rozvinutý
<g/>
,	,	kIx,	,
ba	ba	k9	ba
inovativního	inovativní	k2eAgNnSc2d1	inovativní
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
chovatelství	chovatelství	k1gNnSc2	chovatelství
již	již	k6eAd1	již
v	v	k7c6	v
období	období	k1gNnSc6	období
<g />
.	.	kIx.	.
</s>
<s>
5000	[number]	k4	5000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dokázal	dokázat	k5eAaPmAgInS	dokázat
průlomový	průlomový	k2eAgInSc1d1	průlomový
objev	objev	k1gInSc1	objev
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
během	během	k7c2	během
zkoumání	zkoumání	k1gNnSc2	zkoumání
hrnčířských	hrnčířský	k2eAgInPc2d1	hrnčířský
výrobků	výrobek	k1gInPc2	výrobek
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
archeologických	archeologický	k2eAgNnPc2d1	Archeologické
nalezišť	naleziště	k1gNnPc2	naleziště
v	v	k7c6	v
Pokrovniku	Pokrovnik	k1gInSc6	Pokrovnik
a	a	k8xC	a
Danilo	danit	k5eAaImAgNnS	danit
Bitinu	Bitina	k1gFnSc4	Bitina
badatelé	badatel	k1gMnPc1	badatel
objevili	objevit	k5eAaPmAgMnP	objevit
stopy	stopa	k1gFnPc4	stopa
sýra	sýr	k1gInSc2	sýr
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
stáří	stáří	k1gNnSc4	stáří
odhadli	odhadnout	k5eAaPmAgMnP	odhadnout
na	na	k7c4	na
nejméně	málo	k6eAd3	málo
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
museli	muset	k5eAaImAgMnP	muset
dosud	dosud	k6eAd1	dosud
odhadované	odhadovaný	k2eAgNnSc4d1	odhadované
stáří	stáří	k1gNnSc4	stáří
této	tento	k3xDgFnSc2	tento
potraviny	potravina	k1gFnSc2	potravina
posunout	posunout	k5eAaPmF	posunout
o	o	k7c6	o
celých	celý	k2eAgFnPc6d1	celá
čtyři	čtyři	k4xCgInPc4	čtyři
tisíce	tisíc	k4xCgInPc4	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
žily	žít	k5eAaImAgFnP	žít
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
ilyrské	ilyrský	k2eAgInPc4d1	ilyrský
kmeny	kmen	k1gInPc4	kmen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stáhly	stáhnout	k5eAaPmAgInP	stáhnout
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Albánie	Albánie	k1gFnSc2	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Římané	Říman	k1gMnPc1	Říman
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
provincii	provincie	k1gFnSc4	provincie
nazývanou	nazývaný	k2eAgFnSc7d1	nazývaná
Illyricum	Illyricum	k1gInSc4	Illyricum
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnula	zahrnout	k5eAaPmAgFnS	zahrnout
také	také	k9	také
dnešní	dnešní	k2eAgNnSc4d1	dnešní
chorvatské	chorvatský	k2eAgNnSc4d1	Chorvatské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
začali	začít	k5eAaPmAgMnP	začít
usazovat	usazovat	k5eAaImF	usazovat
Slované	Slovan	k1gMnPc1	Slovan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
později	pozdě	k6eAd2	pozdě
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
jednotný	jednotný	k2eAgInSc4d1	jednotný
chorvatský	chorvatský	k2eAgInSc4d1	chorvatský
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
chorvatským	chorvatský	k2eAgMnSc7d1	chorvatský
králem	král	k1gMnSc7	král
byl	být	k5eAaImAgMnS	být
Tomislav	Tomislav	k1gMnSc1	Tomislav
I.	I.	kA	I.
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Trpimírovců	Trpimírovec	k1gMnPc2	Trpimírovec
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1102	[number]	k4	1102
bylo	být	k5eAaImAgNnS	být
chorvatské	chorvatský	k2eAgNnSc1d1	Chorvatské
království	království	k1gNnSc1	království
spojeno	spojit	k5eAaPmNgNnS	spojit
personální	personální	k2eAgFnSc7d1	personální
unií	unie	k1gFnSc7	unie
s	s	k7c7	s
uherským	uherský	k2eAgInSc7d1	uherský
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
někteří	některý	k3yIgMnPc1	některý
historici	historik	k1gMnPc1	historik
se	se	k3xPyFc4	se
však	však	k9	však
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
spíše	spíše	k9	spíše
o	o	k7c6	o
anexi	anexe	k1gFnSc6	anexe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Habsburků	Habsburk	k1gMnPc2	Habsburk
a	a	k8xC	a
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
pod	pod	k7c7	pod
uherskou	uherský	k2eAgFnSc7d1	uherská
nadvládou	nadvláda	k1gFnSc7	nadvláda
<g/>
,	,	kIx,	,
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
jediným	jediný	k2eAgInSc7d1	jediný
přístupem	přístup	k1gInSc7	přístup
Uherska	Uhersko	k1gNnSc2	Uhersko
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ale	ale	k8xC	ale
také	také	k9	také
čelila	čelit	k5eAaImAgFnS	čelit
výbojům	výboj	k1gInPc3	výboj
z	z	k7c2	z
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
turecké	turecký	k2eAgFnSc3d1	turecká
hrozbě	hrozba	k1gFnSc3	hrozba
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
vojenská	vojenský	k2eAgFnSc1d1	vojenská
hranice	hranice	k1gFnSc1	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatský	chorvatský	k2eAgMnSc1d1	chorvatský
šlechtic	šlechtic	k1gMnSc1	šlechtic
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Šubič	Šubič	k1gMnSc1	Šubič
Zrinský	Zrinský	k1gMnSc1	Zrinský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
roku	rok	k1gInSc2	rok
1566	[number]	k4	1566
zahynul	zahynout	k5eAaPmAgInS	zahynout
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
svou	svůj	k3xOyFgFnSc7	svůj
posádkou	posádka	k1gFnSc7	posádka
při	při	k7c6	při
obraně	obrana	k1gFnSc6	obrana
pevnosti	pevnost	k1gFnSc3	pevnost
Szigetvár	Szigetvár	k1gInSc4	Szigetvár
proti	proti	k7c3	proti
padesátinásobné	padesátinásobný	k2eAgFnSc3d1	padesátinásobná
přesile	přesila	k1gFnSc3	přesila
Osmanských	osmanský	k2eAgInPc2d1	osmanský
Turků	turek	k1gInPc2	turek
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
smrti	smrt	k1gFnSc6	smrt
chorvatským	chorvatský	k2eAgMnSc7d1	chorvatský
národním	národní	k2eAgMnSc7d1	národní
hrdinou	hrdina	k1gMnSc7	hrdina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1918	[number]	k4	1918
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Království	království	k1gNnSc1	království
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
Slovinců	Slovinec	k1gMnPc2	Slovinec
(	(	kIx(	(
<g/>
Království	království	k1gNnSc1	království
SHS	SHS	kA	SHS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
spojením	spojení	k1gNnSc7	spojení
Státu	stát	k1gInSc2	stát
Slovinců	Slovinec	k1gMnPc2	Slovinec
<g/>
,	,	kIx,	,
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1918	[number]	k4	1918
utvořil	utvořit	k5eAaPmAgMnS	utvořit
z	z	k7c2	z
jihoslovanských	jihoslovanský	k2eAgNnPc2d1	jihoslovanské
území	území	k1gNnPc2	území
Rakousko-Uherska	Rakousko-Uherska	k1gFnSc1	Rakousko-Uherska
(	(	kIx(	(
<g/>
Centrální	centrální	k2eAgNnSc1d1	centrální
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
,	,	kIx,	,
Slavonie	Slavonie	k1gFnSc1	Slavonie
<g/>
,	,	kIx,	,
Dalmácie	Dalmácie	k1gFnSc1	Dalmácie
<g/>
,	,	kIx,	,
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
<g/>
,	,	kIx,	,
Kraňsko	Kraňsko	k1gNnSc1	Kraňsko
a	a	k8xC	a
Dolní	dolní	k2eAgNnSc1d1	dolní
Štýrsko	Štýrsko	k1gNnSc1	Štýrsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
Srbským	srbský	k2eAgNnSc7d1	srbské
královstvím	království	k1gNnSc7	království
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1913	[number]	k4	1913
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
o	o	k7c4	o
Vardarskou	Vardarský	k2eAgFnSc4d1	Vardarská
Makedonii	Makedonie	k1gFnSc4	Makedonie
<g/>
,	,	kIx,	,
Kosovo	Kosův	k2eAgNnSc4d1	Kosovo
<g/>
,	,	kIx,	,
Vojvodinu	Vojvodina	k1gFnSc4	Vojvodina
a	a	k8xC	a
Černou	černý	k2eAgFnSc4d1	černá
Horu	hora	k1gFnSc4	hora
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
se	se	k3xPyFc4	se
Království	království	k1gNnSc2	království
SHS	SHS	kA	SHS
přejmenovalo	přejmenovat	k5eAaPmAgNnS	přejmenovat
na	na	k7c6	na
Království	království	k1gNnSc6	království
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
královské	královský	k2eAgFnSc2d1	královská
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
získalo	získat	k5eAaPmAgNnS	získat
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
jistou	jistý	k2eAgFnSc4d1	jistá
autonomii	autonomie	k1gFnSc4	autonomie
vytvořením	vytvoření	k1gNnSc7	vytvoření
Chorvatské	chorvatský	k2eAgFnSc2d1	chorvatská
bánoviny	bánovina	k1gFnSc2	bánovina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
faktické	faktický	k2eAgFnSc6d1	faktická
likvidaci	likvidace	k1gFnSc6	likvidace
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
chorvatští	chorvatský	k2eAgMnPc1d1	chorvatský
fašisté	fašista	k1gMnPc1	fašista
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
ustašovci	ustašovec	k1gMnPc1	ustašovec
<g/>
,	,	kIx,	,
Nezávislý	závislý	k2eNgInSc4d1	nezávislý
stát	stát	k5eAaPmF	stát
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
chorvatských	chorvatský	k2eAgFnPc2d1	chorvatská
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
Bosnu	Bosna	k1gFnSc4	Bosna
a	a	k8xC	a
Hercegovinu	Hercegovina	k1gFnSc4	Hercegovina
<g/>
.	.	kIx.	.
</s>
<s>
Ustašovský	ustašovský	k2eAgInSc1d1	ustašovský
režim	režim	k1gInSc1	režim
Ante	Ant	k1gMnSc5	Ant
Paveliće	Paveliće	k1gFnPc3	Paveliće
povraždil	povraždit	k5eAaPmAgInS	povraždit
až	až	k9	až
335	[number]	k4	335
000	[number]	k4	000
příslušníků	příslušník	k1gMnPc2	příslušník
srbské	srbský	k2eAgFnSc2d1	Srbská
menšiny	menšina	k1gFnSc2	menšina
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgFnPc4d1	mnohá
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
v	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
Jasenovac	Jasenovac	k1gFnSc1	Jasenovac
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
období	období	k1gNnSc6	období
bylo	být	k5eAaImAgNnS	být
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
lidovou	lidový	k2eAgFnSc7d1	lidová
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
socialistickou	socialistický	k2eAgFnSc7d1	socialistická
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
republikou	republika	k1gFnSc7	republika
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Federativní	federativní	k2eAgFnSc2d1	federativní
lidové	lidový	k2eAgFnSc2d1	lidová
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Socialistické	socialistický	k2eAgFnSc2d1	socialistická
federativní	federativní	k2eAgFnSc2d1	federativní
republiky	republika	k1gFnSc2	republika
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
socialistické	socialistický	k2eAgFnSc2d1	socialistická
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
stál	stát	k5eAaImAgInS	stát
pětatřicet	pětatřicet	k4xCc4	pětatřicet
let	léto	k1gNnPc2	léto
Josif	Josif	k1gMnSc1	Josif
Broz	Broz	k1gMnSc1	Broz
Tito	tento	k3xDgMnPc1	tento
<g/>
,	,	kIx,	,
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
chorvatského	chorvatský	k2eAgMnSc2d1	chorvatský
Kumrovce	Kumrovec	k1gMnSc2	Kumrovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
bylo	být	k5eAaImAgNnS	být
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
po	po	k7c6	po
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
druhou	druhý	k4xOgFnSc7	druhý
nejrozvinutější	rozvinutý	k2eAgFnSc7d3	nejrozvinutější
jugoslávskou	jugoslávský	k2eAgFnSc7d1	jugoslávská
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Nezávislost	nezávislost	k1gFnSc4	nezávislost
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
Republiku	republika	k1gFnSc4	republika
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1991	[number]	k4	1991
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Slovinskem	Slovinsko	k1gNnSc7	Slovinsko
nezávislost	nezávislost	k1gFnSc4	nezávislost
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
uznána	uznán	k2eAgFnSc1d1	uznána
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1992	[number]	k4	1992
Evropským	evropský	k2eAgNnSc7d1	Evropské
společenstvím	společenství	k1gNnSc7	společenství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jugoslávská	jugoslávský	k2eAgFnSc1d1	jugoslávská
federace	federace	k1gFnSc1	federace
však	však	k9	však
samostatnost	samostatnost	k1gFnSc4	samostatnost
neuznala	uznat	k5eNaPmAgFnS	uznat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
rozpoutalo	rozpoutat	k5eAaPmAgNnS	rozpoutat
válečný	válečný	k2eAgInSc4d1	válečný
konflikt	konflikt	k1gInSc4	konflikt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
přestávkami	přestávka	k1gFnPc7	přestávka
vlekl	vleknout	k5eAaImAgMnS	vleknout
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
začalo	začít	k5eAaPmAgNnS	začít
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
směřovat	směřovat	k5eAaImF	směřovat
do	do	k7c2	do
NATO	NATO	kA	NATO
a	a	k8xC	a
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
kandidátskou	kandidátský	k2eAgFnSc7d1	kandidátská
zemí	zem	k1gFnSc7	zem
EU	EU	kA	EU
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
zahájila	zahájit	k5eAaPmAgFnS	zahájit
přístupová	přístupový	k2eAgNnPc1d1	přístupové
jednání	jednání	k1gNnPc4	jednání
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
v	v	k7c4	v
týž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
jako	jako	k9	jako
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
chvíle	chvíle	k1gFnSc2	chvíle
nebylo	být	k5eNaImAgNnS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
přístupová	přístupový	k2eAgNnPc1d1	přístupové
jednání	jednání	k1gNnPc1	jednání
vůbec	vůbec	k9	vůbec
začnou	začít	k5eAaPmIp3nP	začít
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
dřívější	dřívější	k2eAgFnSc3d1	dřívější
neochotě	neochota	k1gFnSc3	neochota
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
Mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
trestním	trestní	k2eAgInSc7d1	trestní
tribunálem	tribunál	k1gInSc7	tribunál
pro	pro	k7c4	pro
bývalou	bývalý	k2eAgFnSc4d1	bývalá
Jugoslávii	Jugoslávie	k1gFnSc4	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
však	však	k9	však
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
udělalo	udělat	k5eAaPmAgNnS	udělat
pokrok	pokrok	k1gInSc4	pokrok
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
generála	generál	k1gMnSc2	generál
Ante	Ant	k1gMnSc5	Ant
Gotoviny	Gotovina	k1gFnSc2	Gotovina
<g/>
,	,	kIx,	,
obviněného	obviněný	k2eAgMnSc2d1	obviněný
z	z	k7c2	z
válečných	válečný	k2eAgInPc2d1	válečný
zločinů	zločin	k1gInPc2	zločin
během	během	k7c2	během
Operace	operace	k1gFnSc2	operace
Bouře	bouř	k1gFnSc2	bouř
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
muselo	muset	k5eAaImAgNnS	muset
na	na	k7c4	na
200	[number]	k4	200
tisíc	tisíc	k4xCgInPc2	tisíc
Srbů	Srb	k1gMnPc2	Srb
opustit	opustit	k5eAaPmF	opustit
své	svůj	k3xOyFgInPc4	svůj
domovy	domov	k1gInPc4	domov
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
splnilo	splnit	k5eAaPmAgNnS	splnit
zbývající	zbývající	k2eAgFnSc4d1	zbývající
podmínku	podmínka	k1gFnSc4	podmínka
pro	pro	k7c4	pro
zahájení	zahájení	k1gNnSc4	zahájení
přístupových	přístupový	k2eAgNnPc2d1	přístupové
jednání	jednání	k1gNnPc2	jednání
<g/>
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
do	do	k7c2	do
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
se	se	k3xPyFc4	se
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
stalo	stát	k5eAaPmAgNnS	stát
28	[number]	k4	28
<g/>
.	.	kIx.	.
členem	člen	k1gMnSc7	člen
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Gotovina	Gotovina	k1gFnSc1	Gotovina
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
chorvatští	chorvatský	k2eAgMnPc1d1	chorvatský
generálové	generál	k1gMnPc1	generál
byli	být	k5eAaImAgMnP	být
tribunálem	tribunál	k1gInSc7	tribunál
v	v	k7c6	v
Haagu	Haag	k1gInSc6	Haag
zproštěni	zproštěn	k2eAgMnPc1d1	zproštěn
viny	vina	k1gFnSc2	vina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
Srbskem	Srbsko	k1gNnSc7	Srbsko
jako	jako	k9	jako
"	"	kIx"	"
<g/>
politické	politický	k2eAgNnSc1d1	politické
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
na	na	k7c6	na
Balkánském	balkánský	k2eAgInSc6d1	balkánský
poloostrově	poloostrov	k1gInSc6	poloostrov
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
ostrovech	ostrov	k1gInPc6	ostrov
v	v	k7c6	v
Jaderském	jaderský	k2eAgNnSc6d1	Jaderské
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mu	on	k3xPp3gMnSc3	on
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc1	třetina
kamenitého	kamenitý	k2eAgNnSc2d1	kamenité
pobřeží	pobřeží	k1gNnSc2	pobřeží
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
poloostrov	poloostrov	k1gInSc1	poloostrov
Istrie	Istrie	k1gFnSc2	Istrie
a	a	k8xC	a
Dalmácie	Dalmácie	k1gFnSc2	Dalmácie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
<g/>
,	,	kIx,	,
Srbskem	Srbsko	k1gNnSc7	Srbsko
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
Černou	černý	k2eAgFnSc7d1	černá
horou	hora	k1gFnSc7	hora
a	a	k8xC	a
Bosnou	Bosna	k1gFnSc7	Bosna
a	a	k8xC	a
Hercegovinou	Hercegovina	k1gFnSc7	Hercegovina
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
a	a	k8xC	a
Slovinskem	Slovinsko	k1gNnSc7	Slovinsko
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
je	být	k5eAaImIp3nS	být
56	[number]	k4	56
594	[number]	k4	594
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
128	[number]	k4	128
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
kilometrů	kilometr	k1gInPc2	kilometr
tvoří	tvořit	k5eAaImIp3nS	tvořit
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
127	[number]	k4	127
<g/>
.	.	kIx.	.
největší	veliký	k2eAgFnSc2d3	veliký
země	zem	k1gFnSc2	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
pohořím	pohoří	k1gNnSc7	pohoří
jsou	být	k5eAaImIp3nP	být
Dinárské	dinárský	k2eAgFnPc4d1	Dinárská
hory	hora	k1gFnPc4	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
Dinara	Dinara	k1gFnSc1	Dinara
<g/>
,	,	kIx,	,
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
1831	[number]	k4	1831
m.	m.	k?	m.
n.	n.	k?	n.
m.	m.	k?	m.
Tyčí	tyčit	k5eAaImIp3nS	tyčit
se	se	k3xPyFc4	se
blízko	blízko	k7c2	blízko
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Bosnou	Bosna	k1gFnSc7	Bosna
a	a	k8xC	a
Hercegovinou	Hercegovina	k1gFnSc7	Hercegovina
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
se	se	k3xPyFc4	se
krom	krom	k7c2	krom
pevninské	pevninský	k2eAgFnSc2d1	pevninská
části	část	k1gFnSc2	část
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tisíce	tisíc	k4xCgInPc4	tisíc
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
různých	různý	k2eAgFnPc2d1	různá
velikostí	velikost	k1gFnPc2	velikost
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
48	[number]	k4	48
je	být	k5eAaImIp3nS	být
trvale	trvale	k6eAd1	trvale
obydleno	obydlet	k5eAaPmNgNnS	obydlet
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
ostrovy	ostrov	k1gInPc7	ostrov
jsou	být	k5eAaImIp3nP	být
Cres	Cres	k1gInSc4	Cres
a	a	k8xC	a
Krk	krk	k1gInSc4	krk
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
přibližně	přibližně	k6eAd1	přibližně
405	[number]	k4	405
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hornatými	hornatý	k2eAgFnPc7d1	hornatá
severními	severní	k2eAgFnPc7d1	severní
částmi	část	k1gFnPc7	část
Chorvatského	chorvatský	k2eAgInSc2d1	chorvatský
Záhoří	Záhoří	k1gNnSc2	Záhoří
a	a	k8xC	a
rovinatými	rovinatý	k2eAgFnPc7d1	rovinatá
pláněmi	pláň	k1gFnPc7	pláň
Slavonie	Slavonie	k1gFnSc2	Slavonie
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
Panonské	panonský	k2eAgFnSc2d1	Panonská
pánve	pánev	k1gFnSc2	pánev
<g/>
,	,	kIx,	,
protékají	protékat	k5eAaImIp3nP	protékat
hlavní	hlavní	k2eAgFnPc1d1	hlavní
řeky	řeka	k1gFnPc1	řeka
<g/>
:	:	kIx,	:
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
,	,	kIx,	,
Dráva	Dráva	k1gFnSc1	Dráva
<g/>
,	,	kIx,	,
Kupa	kupa	k1gFnSc1	kupa
a	a	k8xC	a
Sáva	Sáva	k1gFnSc1	Sáva
<g/>
.	.	kIx.	.
</s>
<s>
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
městem	město	k1gNnSc7	město
Vukovar	Vukovar	k1gInSc4	Vukovar
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
součást	součást	k1gFnSc4	součást
hranice	hranice	k1gFnSc2	hranice
se	s	k7c7	s
Srbskem	Srbsko	k1gNnSc7	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnPc1d1	střední
a	a	k8xC	a
jižní	jižní	k2eAgFnPc1d1	jižní
oblasti	oblast	k1gFnPc1	oblast
poblíž	poblíž	k6eAd1	poblíž
jadranského	jadranský	k2eAgNnSc2d1	Jadranské
pobřeží	pobřeží	k1gNnSc2	pobřeží
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
nízkých	nízký	k2eAgFnPc2d1	nízká
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
zalesněných	zalesněný	k2eAgFnPc2d1	zalesněná
vrchovin	vrchovina	k1gFnPc2	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1	přírodní
zdroje	zdroj	k1gInPc1	zdroj
nalezené	nalezený	k2eAgInPc1d1	nalezený
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
dostatečně	dostatečně	k6eAd1	dostatečně
významném	významný	k2eAgNnSc6d1	významné
pro	pro	k7c4	pro
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
zpracování	zpracování	k1gNnSc4	zpracování
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
ropu	ropa	k1gFnSc4	ropa
<g/>
,	,	kIx,	,
uhlí	uhlí	k1gNnSc4	uhlí
<g/>
,	,	kIx,	,
bauxit	bauxit	k1gInSc4	bauxit
<g/>
,	,	kIx,	,
železnou	železný	k2eAgFnSc4d1	železná
rudu	ruda	k1gFnSc4	ruda
<g/>
,	,	kIx,	,
vápenec	vápenec	k1gInSc4	vápenec
<g/>
,	,	kIx,	,
křemen	křemen	k1gInSc4	křemen
<g/>
,	,	kIx,	,
slídu	slída	k1gFnSc4	slída
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc4	sůl
atd.	atd.	kA	atd.
Krasové	krasový	k2eAgInPc1d1	krasový
útvary	útvar	k1gInPc1	útvar
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
nalezení	nalezení	k1gNnSc3	nalezení
asi	asi	k9	asi
na	na	k7c6	na
polovině	polovina	k1gFnSc6	polovina
území	území	k1gNnSc2	území
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
četné	četný	k2eAgFnPc1d1	četná
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Dinárských	dinárský	k2eAgFnPc6d1	Dinárská
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Speleologové	speleolog	k1gMnPc1	speleolog
evidují	evidovat	k5eAaImIp3nP	evidovat
7	[number]	k4	7
000	[number]	k4	000
jeskyň	jeskyně	k1gFnPc2	jeskyně
<g/>
,	,	kIx,	,
49	[number]	k4	49
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
hlubších	hluboký	k2eAgFnPc2d2	hlubší
než	než	k8xS	než
250	[number]	k4	250
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
14	[number]	k4	14
než	než	k8xS	než
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
jeskyně	jeskyně	k1gFnPc4	jeskyně
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
hloubky	hloubka	k1gFnPc1	hloubka
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejznámějšími	známý	k2eAgNnPc7d3	nejznámější
jezery	jezero	k1gNnPc7	jezero
jsou	být	k5eAaImIp3nP	být
Plitvická	Plitvický	k2eAgNnPc1d1	Plitvický
jezera	jezero	k1gNnPc1	jezero
<g/>
,	,	kIx,	,
systém	systém	k1gInSc1	systém
16	[number]	k4	16
jezer	jezero	k1gNnPc2	jezero
s	s	k7c7	s
vodopády	vodopád	k1gInPc7	vodopád
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
je	on	k3xPp3gInPc4	on
spojují	spojovat	k5eAaImIp3nP	spojovat
přes	přes	k7c4	přes
dolomitové	dolomitový	k2eAgFnPc4d1	dolomitová
a	a	k8xC	a
vápencové	vápencový	k2eAgFnPc4d1	vápencová
kaskády	kaskáda	k1gFnPc4	kaskáda
<g/>
.	.	kIx.	.
</s>
<s>
Jezera	jezero	k1gNnPc1	jezero
jsou	být	k5eAaImIp3nP	být
proslulá	proslulý	k2eAgNnPc1d1	proslulé
svými	svůj	k3xOyFgFnPc7	svůj
výraznými	výrazný	k2eAgFnPc7d1	výrazná
barvami	barva	k1gFnPc7	barva
<g/>
,	,	kIx,	,
od	od	k7c2	od
tyrkysové	tyrkysový	k2eAgFnSc2d1	tyrkysová
až	až	k9	až
po	po	k7c4	po
mátově	mátově	k6eAd1	mátově
zelenou	zelený	k2eAgFnSc4d1	zelená
<g/>
.	.	kIx.	.
<g/>
Lesy	les	k1gInPc7	les
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
2	[number]	k4	2
490	[number]	k4	490
000	[number]	k4	000
hektarů	hektar	k1gInPc2	hektar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
44	[number]	k4	44
<g/>
%	%	kIx~	%
chorvatského	chorvatský	k2eAgNnSc2d1	Chorvatské
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
žije	žít	k5eAaImIp3nS	žít
37	[number]	k4	37
000	[number]	k4	000
známých	známý	k2eAgInPc2d1	známý
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
skutečný	skutečný	k2eAgInSc1d1	skutečný
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
50	[number]	k4	50
000	[number]	k4	000
až	až	k9	až
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
nové	nový	k2eAgInPc1d1	nový
odhady	odhad	k1gInPc1	odhad
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
objevu	objev	k1gInSc2	objev
téměř	téměř	k6eAd1	téměř
400	[number]	k4	400
nových	nový	k2eAgInPc2d1	nový
druhů	druh	k1gInPc2	druh
bezobratlých	bezobratlý	k2eAgInPc2d1	bezobratlý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
chorvatském	chorvatský	k2eAgNnSc6d1	Chorvatské
území	území	k1gNnSc6	území
žije	žít	k5eAaImIp3nS	žít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tisíc	tisíc	k4xCgInPc2	tisíc
endemických	endemický	k2eAgInPc2d1	endemický
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
k	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
macarát	macarát	k1gMnSc1	macarát
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
,	,	kIx,	,
<g/>
obývající	obývající	k2eAgFnPc4d1	obývající
jeskyně	jeskyně	k1gFnPc4	jeskyně
Dinárských	dinárský	k2eAgMnPc2d1	dinárský
hor.	hor.	k?	hor.
Pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
endemitů	endemit	k1gInPc2	endemit
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
stává	stávat	k5eAaImIp3nS	stávat
hrozbou	hrozba	k1gFnSc7	hrozba
invazivní	invazivní	k2eAgFnSc1d1	invazivní
řasa	řasa	k1gFnSc1	řasa
Lazucha	Lazucha	k1gFnSc1	Lazucha
tisolistá	tisolistý	k2eAgFnSc1d1	tisolistá
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
legislativa	legislativa	k1gFnSc1	legislativa
chrání	chránit	k5eAaImIp3nS	chránit
1	[number]	k4	1
131	[number]	k4	131
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
má	mít	k5eAaImIp3nS	mít
též	též	k9	též
444	[number]	k4	444
chráněných	chráněný	k2eAgFnPc2d1	chráněná
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
rozloha	rozloha	k1gFnSc1	rozloha
představuje	představovat	k5eAaImIp3nS	představovat
9	[number]	k4	9
<g/>
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
plochy	plocha	k1gFnSc2	plocha
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
osm	osm	k4xCc4	osm
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
a	a	k8xC	a
deset	deset	k4xCc4	deset
přírodních	přírodní	k2eAgInPc2d1	přírodní
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c4	mezi
600	[number]	k4	600
milimetry	milimetr	k1gInPc4	milimetr
a	a	k8xC	a
3500	[number]	k4	3500
milimetry	milimetr	k1gInPc1	milimetr
na	na	k7c4	na
metr	metr	k1gInSc4	metr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgFnPc1d3	nejmenší
srážky	srážka	k1gFnPc1	srážka
jsou	být	k5eAaImIp3nP	být
zaznamenávány	zaznamenávat	k5eAaImNgFnP	zaznamenávat
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
(	(	kIx(	(
<g/>
Biševo	Biševo	k1gNnSc4	Biševo
<g/>
,	,	kIx,	,
Lastovo	Lastův	k2eAgNnSc1d1	Lastovo
<g/>
,	,	kIx,	,
Svetac	Svetac	k1gInSc1	Svetac
<g/>
,	,	kIx,	,
Vis	vis	k1gInSc1	vis
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
částech	část	k1gFnPc6	část
Slavonie	Slavonie	k1gFnSc2	Slavonie
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
Dinárských	dinárský	k2eAgFnPc6d1	Dinárská
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
v	v	k7c6	v
Gorském	Gorský	k2eAgInSc6d1	Gorský
kotaru	kotar	k1gInSc6	kotar
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnPc1d2	vyšší
rychlosti	rychlost	k1gFnPc1	rychlost
větru	vítr	k1gInSc2	vítr
jsou	být	k5eAaImIp3nP	být
častěji	často	k6eAd2	často
zaznamenávány	zaznamenávat	k5eAaImNgInP	zaznamenávat
v	v	k7c6	v
chladnějších	chladný	k2eAgInPc6d2	chladnější
měsících	měsíc	k1gInPc6	měsíc
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Nejslunnějšími	slunný	k2eAgFnPc7d3	nejslunnější
částmi	část	k1gFnPc7	část
země	zem	k1gFnPc4	zem
jsou	být	k5eAaImIp3nP	být
ostrovy	ostrov	k1gInPc1	ostrov
Hvar	Hvar	k1gInSc1	Hvar
a	a	k8xC	a
Korčula	Korčula	k1gFnSc1	Korčula
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
naměřeno	naměřit	k5eAaBmNgNnS	naměřit
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2700	[number]	k4	2700
hodin	hodina	k1gFnPc2	hodina
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
je	být	k5eAaImIp3nS	být
unitární	unitární	k2eAgInSc4d1	unitární
stát	stát	k1gInSc4	stát
a	a	k8xC	a
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
bylo	být	k5eAaImAgNnS	být
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
poloprezidentským	poloprezidentský	k2eAgInSc7d1	poloprezidentský
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
kvůli	kvůli	k7c3	kvůli
osobě	osoba	k1gFnSc3	osoba
zakladatele	zakladatel	k1gMnSc2	zakladatel
státu	stát	k1gInSc2	stát
Franjo	Franjo	k1gMnSc1	Franjo
Tuđmana	Tuđman	k1gMnSc2	Tuđman
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
byla	být	k5eAaImAgFnS	být
pozice	pozice	k1gFnSc1	pozice
prezidenta	prezident	k1gMnSc2	prezident
zákonem	zákon	k1gInSc7	zákon
oslabena	oslabit	k5eAaPmNgFnS	oslabit
a	a	k8xC	a
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
přešlo	přejít	k5eAaPmAgNnS	přejít
na	na	k7c4	na
systém	systém	k1gInSc4	systém
parlamentní	parlamentní	k2eAgFnPc1d1	parlamentní
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
drží	držet	k5eAaImIp3nS	držet
především	především	k9	především
premiér	premiér	k1gMnSc1	premiér
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Rada	rada	k1gFnSc1	rada
ministrů	ministr	k1gMnPc2	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
odpovědná	odpovědný	k2eAgFnSc1d1	odpovědná
parlamentu	parlament	k1gInSc3	parlament
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
jednokomorový	jednokomorový	k2eAgInSc1d1	jednokomorový
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
Sabor	sabor	k1gInSc1	sabor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
Predsjednik	Predsjednik	k1gInSc1	Predsjednik
Republike	Republik	k1gInSc2	Republik
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
volený	volený	k2eAgInSc1d1	volený
občany	občan	k1gMnPc7	občan
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
mandát	mandát	k1gInSc1	mandát
trvá	trvat	k5eAaImIp3nS	trvat
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
dává	dávat	k5eAaImIp3nS	dávat
jedné	jeden	k4xCgFnSc2	jeden
osobě	osoba	k1gFnSc3	osoba
možnost	možnost	k1gFnSc4	možnost
být	být	k5eAaImF	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
pouze	pouze	k6eAd1	pouze
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
určitý	určitý	k2eAgInSc4d1	určitý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentkou	prezidentka	k1gFnSc7	prezidentka
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
Kolinda	Kolinda	k1gFnSc1	Kolinda
Grabarová	Grabarová	k1gFnSc1	Grabarová
Kitarovićová	Kitarovićová	k1gFnSc1	Kitarovićová
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgMnPc4	čtyři
místopředsedy	místopředseda	k1gMnPc4	místopředseda
a	a	k8xC	a
šestnáct	šestnáct	k4xCc1	šestnáct
ministrů	ministr	k1gMnPc2	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc4	sídlo
u	u	k7c2	u
Banského	banský	k2eAgInSc2d1	banský
dvora	dvůr	k1gInSc2	dvůr
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
naproti	naproti	k7c3	naproti
budově	budova	k1gFnSc3	budova
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
Andrej	Andrej	k1gMnSc1	Andrej
Plenković	Plenković	k1gMnSc1	Plenković
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
největší	veliký	k2eAgFnSc2d3	veliký
pravicové	pravicový	k2eAgFnSc2d1	pravicová
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
většinu	většina	k1gFnSc4	většina
vlád	vláda	k1gFnPc2	vláda
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
Chorvatského	chorvatský	k2eAgNnSc2d1	Chorvatské
demokratického	demokratický	k2eAgNnSc2d1	demokratické
společenství	společenství	k1gNnSc2	společenství
<g/>
.	.	kIx.	.
<g/>
Jednokomorový	jednokomorový	k2eAgInSc1d1	jednokomorový
parlament	parlament	k1gInSc1	parlament
(	(	kIx(	(
<g/>
Sabor	sabor	k1gInSc1	sabor
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
komora	komora	k1gFnSc1	komora
existovala	existovat	k5eAaImAgFnS	existovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
členů	člen	k1gMnPc2	člen
Saboru	sabor	k1gInSc2	sabor
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
od	od	k7c2	od
100	[number]	k4	100
do	do	k7c2	do
160	[number]	k4	160
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
lidovým	lidový	k2eAgNnSc7d1	lidové
hlasováním	hlasování	k1gNnSc7	hlasování
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zasedání	zasedání	k1gNnSc1	zasedání
Saboru	sabor	k1gInSc2	sabor
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
21	[number]	k4	21
žup	župa	k1gFnPc2	župa
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
opčiny	opčina	k1gFnPc4	opčina
(	(	kIx(	(
<g/>
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
kolem	kolem	k7c2	kolem
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
)	)	kIx)	)
které	který	k3yIgNnSc1	který
mají	mít	k5eAaImIp3nP	mít
podobné	podobný	k2eAgNnSc4d1	podobné
postavení	postavení	k1gNnSc4	postavení
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
české	český	k2eAgFnPc1d1	Česká
obce	obec	k1gFnPc1	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
ekonomika	ekonomika	k1gFnSc1	ekonomika
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
jako	jako	k9	jako
středně	středně	k6eAd1	středně
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
7	[number]	k4	7
000	[number]	k4	000
USD	USD	kA	USD
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
patřilo	patřit	k5eAaImAgNnS	patřit
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
k	k	k7c3	k
nejrozvinutějším	rozvinutý	k2eAgFnPc3d3	nejrozvinutější
zemím	zem	k1gFnPc3	zem
tohoto	tento	k3xDgNnSc2	tento
soustátí	soustátí	k1gNnSc2	soustátí
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zde	zde	k6eAd1	zde
povoleno	povolen	k2eAgNnSc1d1	povoleno
soukromé	soukromý	k2eAgNnSc1d1	soukromé
podnikání	podnikání	k1gNnSc1	podnikání
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
turistický	turistický	k2eAgInSc1d1	turistický
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zde	zde	k6eAd1	zde
koncentrováno	koncentrovat	k5eAaBmNgNnS	koncentrovat
i	i	k9	i
několik	několik	k4yIc1	několik
větších	veliký	k2eAgFnPc2d2	veliký
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
investic	investice	k1gFnPc2	investice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
země	zem	k1gFnSc2	zem
silně	silně	k6eAd1	silně
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
v	v	k7c6	v
letech	let	k1gInPc6	let
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
95	[number]	k4	95
<g/>
,	,	kIx,	,
během	během	k7c2	během
vleklého	vleklý	k2eAgInSc2d1	vleklý
válečného	válečný	k2eAgInSc2d1	válečný
konfliktu	konflikt	k1gInSc2	konflikt
se	s	k7c7	s
Srby	Srb	k1gMnPc7	Srb
a	a	k8xC	a
Bosňany	Bosňan	k1gMnPc7	Bosňan
<g/>
.	.	kIx.	.
</s>
<s>
Nejenže	nejenže	k6eAd1	nejenže
tato	tento	k3xDgFnSc1	tento
válka	válka	k1gFnSc1	válka
zemi	zem	k1gFnSc4	zem
finančně	finančně	k6eAd1	finančně
vyčerpala	vyčerpat	k5eAaPmAgFnS	vyčerpat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
velká	velký	k2eAgFnSc1d1	velká
vlna	vlna	k1gFnSc1	vlna
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
investic	investice	k1gFnPc2	investice
do	do	k7c2	do
postkomunistického	postkomunistický	k2eAgInSc2d1	postkomunistický
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tak	tak	k6eAd1	tak
získaly	získat	k5eAaPmAgFnP	získat
zejména	zejména	k9	zejména
země	zem	k1gFnPc1	zem
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
začalo	začít	k5eAaPmAgNnS	začít
silně	silně	k6eAd1	silně
zaměřovat	zaměřovat	k5eAaImF	zaměřovat
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
turistiky	turistika	k1gFnSc2	turistika
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
oživení	oživení	k1gNnSc3	oživení
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
situace	situace	k1gFnSc1	situace
znatelně	znatelně	k6eAd1	znatelně
lepší	dobrý	k2eAgFnSc1d2	lepší
a	a	k8xC	a
roční	roční	k2eAgInSc1d1	roční
růst	růst	k1gInSc1	růst
HDP	HDP	kA	HDP
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
měrou	míra	k1gFnSc7wR	míra
se	se	k3xPyFc4	se
na	na	k7c6	na
ekonomickém	ekonomický	k2eAgInSc6d1	ekonomický
růstu	růst	k1gInSc6	růst
podílí	podílet	k5eAaImIp3nS	podílet
spotřeba	spotřeba	k1gFnSc1	spotřeba
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
snadná	snadný	k2eAgFnSc1d1	snadná
dostupnost	dostupnost	k1gFnSc1	dostupnost
úvěrů	úvěr	k1gInPc2	úvěr
<g/>
.	.	kIx.	.
</s>
<s>
Inflace	inflace	k1gFnSc1	inflace
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
a	a	k8xC	a
chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
měna	měna	k1gFnSc1	měna
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
i	i	k9	i
nadále	nadále	k6eAd1	nadále
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
ekonomickými	ekonomický	k2eAgInPc7d1	ekonomický
problémy	problém	k1gInPc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
velká	velký	k2eAgFnSc1d1	velká
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
,	,	kIx,	,
až	až	k9	až
20	[number]	k4	20
%	%	kIx~	%
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivního	aktivní	k2eAgNnSc2d1	aktivní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
záporná	záporný	k2eAgFnSc1d1	záporná
bilance	bilance	k1gFnSc1	bilance
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
státní	státní	k2eAgFnSc1d1	státní
účast	účast	k1gFnSc1	účast
v	v	k7c6	v
hospodářství	hospodářství	k1gNnSc6	hospodářství
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
neochotě	neochota	k1gFnSc3	neochota
vlád	vláda	k1gFnPc2	vláda
dát	dát	k5eAaPmF	dát
zelenou	zelená	k1gFnSc4	zelená
privatizacím	privatizace	k1gFnPc3	privatizace
větších	veliký	k2eAgInPc2d2	veliký
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
i	i	k8xC	i
kvůli	kvůli	k7c3	kvůli
odporu	odpor	k1gInSc3	odpor
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
se	se	k3xPyFc4	se
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
rozdíly	rozdíl	k1gInPc7	rozdíl
mezi	mezi	k7c4	mezi
regiony	region	k1gInPc4	region
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
nerovnoměrné	rovnoměrný	k2eNgFnSc3d1	nerovnoměrná
politice	politika	k1gFnSc3	politika
regionálního	regionální	k2eAgInSc2d1	regionální
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
vlády	vláda	k1gFnPc1	vláda
soustředí	soustředit	k5eAaPmIp3nP	soustředit
na	na	k7c4	na
dynamický	dynamický	k2eAgInSc4d1	dynamický
rozvoj	rozvoj	k1gInSc4	rozvoj
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
vnitrozemí	vnitrozemí	k1gNnSc1	vnitrozemí
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
větších	veliký	k2eAgFnPc2d2	veliký
šancí	šance	k1gFnPc2	šance
na	na	k7c4	na
získání	získání	k1gNnSc4	získání
státních	státní	k2eAgInPc2d1	státní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nutně	nutně	k6eAd1	nutně
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Hospodářské	hospodářský	k2eAgInPc1d1	hospodářský
ukazateleHDP	ukazateleHDP	k?	ukazateleHDP
–	–	k?	–
$	$	kIx~	$
92,309	[number]	k4	92,309
mld.	mld.	k?	mld.
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
-	-	kIx~	-
odhad	odhad	k1gInSc1	odhad
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Růst	růst	k1gInSc1	růst
HDP	HDP	kA	HDP
–	–	k?	–
-1,5	-1,5	k4	-1,5
%	%	kIx~	%
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnSc4	obyvatel
–	–	k?	–
$	$	kIx~	$
21	[number]	k4	21
791	[number]	k4	791
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Složení	složení	k1gNnSc1	složení
ekonomiky	ekonomika	k1gFnSc2	ekonomika
–	–	k?	–
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
:	:	kIx,	:
7,2	[number]	k4	7,2
%	%	kIx~	%
<g/>
,	,	kIx,	,
průmysl	průmysl	k1gInSc1	průmysl
<g/>
:	:	kIx,	:
31,7	[number]	k4	31,7
%	%	kIx~	%
<g/>
,	,	kIx,	,
služby	služba	k1gFnPc4	služba
<g/>
:	:	kIx,	:
61,2	[number]	k4	61,2
%	%	kIx~	%
</s>
</p>
<p>
<s>
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
–	–	k?	–
20	[number]	k4	20
%	%	kIx~	%
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Veřejný	veřejný	k2eAgInSc1d1	veřejný
dluh	dluh	k1gInSc1	dluh
–	–	k?	–
46,4	[number]	k4	46,4
%	%	kIx~	%
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Inflace	inflace	k1gFnSc1	inflace
–	–	k?	–
2,4	[number]	k4	2,4
%	%	kIx~	%
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
hrubý	hrubý	k2eAgInSc1d1	hrubý
příjem	příjem	k1gInSc1	příjem
–	–	k?	–
HRK	hrk	k1gInSc1	hrk
7735	[number]	k4	7735
<g/>
,	,	kIx,	,
<g/>
–	–	k?	–
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
vývozní	vývozní	k2eAgFnPc1d1	vývozní
komodity	komodita	k1gFnPc1	komodita
</s>
</p>
<p>
<s>
dopravní	dopravní	k2eAgInPc1d1	dopravní
prostředky	prostředek	k1gInPc1	prostředek
<g/>
,	,	kIx,	,
textil	textil	k1gInSc1	textil
<g/>
,	,	kIx,	,
chemikálie	chemikálie	k1gFnPc1	chemikálie
<g/>
,	,	kIx,	,
potraviny	potravina	k1gFnPc1	potravina
<g/>
,	,	kIx,	,
palivaHlavní	palivaHlavní	k2eAgFnPc1d1	palivaHlavní
dovozní	dovozní	k2eAgFnPc1d1	dovozní
komodity	komodita	k1gFnPc1	komodita
</s>
</p>
<p>
<s>
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgNnPc1d1	dopravní
a	a	k8xC	a
elektrická	elektrický	k2eAgNnPc1d1	elektrické
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgFnPc1d1	chemická
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
paliva	palivo	k1gNnPc1	palivo
a	a	k8xC	a
maziva	mazivo	k1gNnPc1	mazivo
<g/>
,	,	kIx,	,
potraviny	potravina	k1gFnPc1	potravina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Turistika	turistika	k1gFnSc1	turistika
===	===	k?	===
</s>
</p>
<p>
<s>
Chorvatské	chorvatský	k2eAgFnSc3d1	chorvatská
ekonomice	ekonomika	k1gFnSc3	ekonomika
dominuje	dominovat	k5eAaImIp3nS	dominovat
turistický	turistický	k2eAgInSc1d1	turistický
ruch	ruch	k1gInSc1	ruch
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
až	až	k9	až
20	[number]	k4	20
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
HDP	HDP	kA	HDP
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
roční	roční	k2eAgInSc1d1	roční
příjem	příjem	k1gInSc1	příjem
z	z	k7c2	z
turistiky	turistika	k1gFnSc2	turistika
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c6	na
6,61	[number]	k4	6,61
miliardy	miliarda	k4xCgFnPc4	miliarda
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
do	do	k7c2	do
země	zem	k1gFnSc2	zem
přijelo	přijet	k5eAaPmAgNnS	přijet
přes	přes	k7c4	přes
13	[number]	k4	13
milionů	milion	k4xCgInPc2	milion
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
Slovinců	Slovinec	k1gMnPc2	Slovinec
a	a	k8xC	a
Rakušanů	Rakušan	k1gMnPc2	Rakušan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
místě	místo	k1gNnSc6	místo
jsou	být	k5eAaImIp3nP	být
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
přijelo	přijet	k5eAaPmAgNnS	přijet
přes	přes	k7c4	přes
739	[number]	k4	739
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Slovensku	Slovensko	k1gNnSc6	Slovensko
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
nejnavštěvovanější	navštěvovaný	k2eAgNnSc1d3	nejnavštěvovanější
evropskou	evropský	k2eAgFnSc7d1	Evropská
zemí	zem	k1gFnSc7	zem
českými	český	k2eAgMnPc7d1	český
turisty	turist	k1gMnPc7	turist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
proto	proto	k8xC	proto
česká	český	k2eAgNnPc1d1	české
média	médium	k1gNnPc1	médium
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
informační	informační	k2eAgInSc4d1	informační
servis	servis	k1gInSc4	servis
o	o	k7c6	o
cestách	cesta	k1gFnPc6	cesta
do	do	k7c2	do
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
poslouchat	poslouchat	k5eAaImF	poslouchat
stanici	stanice	k1gFnSc4	stanice
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Radiožurnál	radiožurnál	k1gInSc1	radiožurnál
a	a	k8xC	a
chorvatské	chorvatský	k2eAgFnSc6d1	chorvatská
policii	policie	k1gFnSc6	policie
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
pomáhat	pomáhat	k5eAaImF	pomáhat
i	i	k9	i
čeští	český	k2eAgMnPc1d1	český
policisté	policista	k1gMnPc1	policista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
první	první	k4xOgFnSc4	první
hojně	hojně	k6eAd1	hojně
navštěvovaná	navštěvovaný	k2eAgNnPc1d1	navštěvované
turistická	turistický	k2eAgNnPc1d1	turistické
místa	místo	k1gNnPc1	místo
patří	patřit	k5eAaImIp3nP	patřit
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Opatija	Opatij	k1gInSc2	Opatij
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
evropských	evropský	k2eAgNnPc2d1	Evropské
lázeňských	lázeňský	k2eAgNnPc2d1	lázeňské
středisek	středisko	k1gNnPc2	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
střediska	středisko	k1gNnPc1	středisko
postupně	postupně	k6eAd1	postupně
vyrostla	vyrůst	k5eAaPmAgNnP	vyrůst
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
turisté	turist	k1gMnPc1	turist
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
především	především	k9	především
členité	členitý	k2eAgNnSc4d1	členité
pobřeží	pobřeží	k1gNnSc4	pobřeží
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
ostrovy	ostrov	k1gInPc7	ostrov
a	a	k8xC	a
historickými	historický	k2eAgNnPc7d1	historické
městy	město	k1gNnPc7	město
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Dubrovník	Dubrovník	k1gInSc1	Dubrovník
<g/>
,	,	kIx,	,
Split	Split	k1gInSc1	Split
<g/>
,	,	kIx,	,
Zadar	Zadar	k1gInSc1	Zadar
<g/>
,	,	kIx,	,
Šibenik	Šibenik	k1gInSc1	Šibenik
nebo	nebo	k8xC	nebo
Rijeka	Rijeka	k1gFnSc1	Rijeka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
město	město	k1gNnSc1	město
Pula	Pulum	k1gNnSc2	Pulum
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
cípu	cíp	k1gInSc6	cíp
poloostrova	poloostrov	k1gInSc2	poloostrov
Istrie	Istrie	k1gFnSc2	Istrie
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
totiž	totiž	k9	totiž
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
a	a	k8xC	a
nejvyhlášenějších	vyhlášený	k2eAgInPc2d3	nejvyhlášenější
antických	antický	k2eAgInPc2d1	antický
amfiteátrů	amfiteátr	k1gInPc2	amfiteátr
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
navštěvované	navštěvovaný	k2eAgNnSc1d1	navštěvované
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Trogir	Trogir	k1gInSc1	Trogir
<g/>
,	,	kIx,	,
malebné	malebný	k2eAgNnSc1d1	malebné
městečko	městečko	k1gNnSc1	městečko
nedaleko	nedaleko	k7c2	nedaleko
Splitu	Split	k1gInSc2	Split
je	být	k5eAaImIp3nS	být
zařazené	zařazený	k2eAgNnSc1d1	zařazené
mezi	mezi	k7c4	mezi
světové	světový	k2eAgNnSc4d1	světové
kulturní	kulturní	k2eAgNnSc4d1	kulturní
dědictví	dědictví	k1gNnSc4	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
zbytky	zbytek	k1gInPc1	zbytek
antických	antický	k2eAgFnPc2d1	antická
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gInPc6	jejichž
pozůstatcích	pozůstatek	k1gInPc6	pozůstatek
vyrostly	vyrůst	k5eAaPmAgFnP	vyrůst
středověké	středověký	k2eAgFnPc1d1	středověká
budovy	budova	k1gFnPc1	budova
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pevnost	pevnost	k1gFnSc4	pevnost
s	s	k7c7	s
obrannou	obranný	k2eAgFnSc7d1	obranná
věží	věž	k1gFnSc7	věž
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
knížecí	knížecí	k2eAgInSc1d1	knížecí
palác	palác	k1gInSc1	palác
nebo	nebo	k8xC	nebo
renesanční	renesanční	k2eAgFnSc1d1	renesanční
radnice	radnice	k1gFnSc1	radnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
je	být	k5eAaImIp3nS	být
nejnavštěvovanější	navštěvovaný	k2eAgInSc4d3	nejnavštěvovanější
národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
Plitvická	Plitvická	k1gFnSc1	Plitvická
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
zapsaný	zapsaný	k2eAgInSc1d1	zapsaný
mezi	mezi	k7c4	mezi
přírodní	přírodní	k2eAgFnPc4d1	přírodní
památky	památka	k1gFnPc4	památka
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
a	a	k8xC	a
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
8	[number]	k4	8
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
a	a	k8xC	a
7	[number]	k4	7
památek	památka	k1gFnPc2	památka
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Potápění	potápění	k1gNnSc2	potápění
===	===	k?	===
</s>
</p>
<p>
<s>
Jaderské	jaderský	k2eAgNnSc1d1	Jaderské
moře	moře	k1gNnSc1	moře
nabízí	nabízet	k5eAaImIp3nS	nabízet
spoustu	spousta	k1gFnSc4	spousta
možností	možnost	k1gFnPc2	možnost
pro	pro	k7c4	pro
potápěče	potápěč	k1gMnPc4	potápěč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
se	se	k3xPyFc4	se
aktuálně	aktuálně	k6eAd1	aktuálně
nachází	nacházet	k5eAaImIp3nS	nacházet
278	[number]	k4	278
potápěčských	potápěčský	k2eAgFnPc2d1	potápěčská
lokalit	lokalita	k1gFnPc2	lokalita
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
140	[number]	k4	140
potápěčských	potápěčský	k2eAgNnPc2d1	potápěčské
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Potápění	potápění	k1gNnSc1	potápění
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
je	být	k5eAaImIp3nS	být
regulováno	regulovat	k5eAaImNgNnS	regulovat
Chorvatskou	chorvatský	k2eAgFnSc7d1	chorvatská
asociací	asociace	k1gFnSc7	asociace
pro	pro	k7c4	pro
potápění	potápění	k1gNnSc4	potápění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvýznamnějšími	významný	k2eAgFnPc7d3	nejvýznamnější
potápěčskými	potápěčský	k2eAgFnPc7d1	potápěčská
lokalitami	lokalita	k1gFnPc7	lokalita
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Kampanel	Kampanel	k1gInSc4	Kampanel
<g/>
,	,	kIx,	,
ostrov	ostrov	k1gInSc4	ostrov
Jabuka	Jabuk	k1gMnSc2	Jabuk
<g/>
,	,	kIx,	,
ostrov	ostrov	k1gInSc1	ostrov
Vis	vis	k1gInSc1	vis
<g/>
,	,	kIx,	,
Rogoznica	Rogoznica	k1gFnSc1	Rogoznica
nebo	nebo	k8xC	nebo
ostrov	ostrov	k1gInSc1	ostrov
Male	male	k6eAd1	male
Srakane	Srakan	k1gMnSc5	Srakan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
i	i	k9	i
vrak	vrak	k1gInSc1	vrak
lodi	loď	k1gFnSc2	loď
Baron	baron	k1gMnSc1	baron
Gautsch	Gautsch	k1gMnSc1	Gautsch
</s>
</p>
<p>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
ponor	ponor	k1gInSc4	ponor
zorganizovaný	zorganizovaný	k2eAgInSc4d1	zorganizovaný
pomocí	pomocí	k7c2	pomocí
potápěčského	potápěčský	k2eAgNnSc2d1	potápěčské
centra	centrum	k1gNnSc2	centrum
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
okolo	okolo	k7c2	okolo
450	[number]	k4	450
kun	kuna	k1gFnPc2	kuna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
přísně	přísně	k6eAd1	přísně
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
se	se	k3xPyFc4	se
potápět	potápět	k5eAaImF	potápět
v	v	k7c6	v
národních	národní	k2eAgInPc6d1	národní
parcích	park	k1gInPc6	park
Brijuni	Brijuen	k2eAgMnPc1d1	Brijuen
a	a	k8xC	a
Krka	Krka	k1gMnSc1	Krka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
mělo	mít	k5eAaImAgNnS	mít
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
28	[number]	k4	28
344	[number]	k4	344
km	km	kA	km
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
23	[number]	k4	23
979	[number]	k4	979
km	km	kA	km
zpevněných	zpevněný	k2eAgInPc2d1	zpevněný
a	a	k8xC	a
4	[number]	k4	4
365	[number]	k4	365
km	km	kA	km
nezpevněných	zpevněný	k2eNgFnPc2d1	nezpevněná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
výstavba	výstavba	k1gFnSc1	výstavba
dálniční	dálniční	k2eAgFnSc2d1	dálniční
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
celkem	celkem	k6eAd1	celkem
1	[number]	k4	1
100	[number]	k4	100
km	km	kA	km
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c4	o
propojení	propojení	k1gNnSc4	propojení
pevniny	pevnina	k1gFnSc2	pevnina
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
ostrovy	ostrov	k1gInPc7	ostrov
pomocí	pomocí	k7c2	pomocí
tunelů	tunel	k1gInPc2	tunel
<g/>
.	.	kIx.	.
<g/>
Největší	veliký	k2eAgInSc4d3	veliký
nákladní	nákladní	k2eAgInSc4d1	nákladní
přístav	přístav	k1gInSc4	přístav
je	být	k5eAaImIp3nS	být
Rijeka	Rijeka	k1gFnSc1	Rijeka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
dopravu	doprava	k1gFnSc4	doprava
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
především	především	k9	především
přístavy	přístav	k1gInPc1	přístav
ve	v	k7c6	v
Splitu	Split	k1gInSc6	Split
a	a	k8xC	a
Zadaru	Zadar	k1gInSc6	Zadar
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
ovšem	ovšem	k9	ovšem
spousta	spousta	k1gFnSc1	spousta
malých	malý	k2eAgInPc2d1	malý
přístavů	přístav	k1gInPc2	přístav
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
zajišťujících	zajišťující	k2eAgMnPc2d1	zajišťující
trajektovou	trajektový	k2eAgFnSc4d1	trajektová
přepravu	přeprava	k1gFnSc4	přeprava
na	na	k7c4	na
ostrovy	ostrov	k1gInPc4	ostrov
a	a	k8xC	a
do	do	k7c2	do
některých	některý	k3yIgNnPc2	některý
italských	italský	k2eAgNnPc2d1	italské
měst	město	k1gNnPc2	město
přes	přes	k7c4	přes
Jaderské	jaderský	k2eAgNnSc4d1	Jaderské
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
vnitrozemským	vnitrozemský	k2eAgInSc7d1	vnitrozemský
říčním	říční	k2eAgInSc7d1	říční
přístavem	přístav	k1gInSc7	přístav
je	být	k5eAaImIp3nS	být
Vukovar	Vukovar	k1gInSc1	Vukovar
na	na	k7c6	na
Dunaji	Dunaj	k1gInSc6	Dunaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
celkem	celkem	k6eAd1	celkem
2	[number]	k4	2
722	[number]	k4	722
km	km	kA	km
železnic	železnice	k1gFnPc2	železnice
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
985	[number]	k4	985
km	km	kA	km
je	být	k5eAaImIp3nS	být
elektrifikováno	elektrifikovat	k5eAaBmNgNnS	elektrifikovat
a	a	k8xC	a
254	[number]	k4	254
km	km	kA	km
je	být	k5eAaImIp3nS	být
dvoukolejných	dvoukolejný	k2eAgFnPc2d1	dvoukolejná
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
<g/>
,	,	kIx,	,
Zadaru	Zadar	k1gInSc6	Zadar
<g/>
,	,	kIx,	,
Splitu	Split	k1gInSc6	Split
<g/>
,	,	kIx,	,
Dubrovníku	Dubrovník	k1gInSc6	Dubrovník
<g/>
,	,	kIx,	,
Rijece	Rijeka	k1gFnSc6	Rijeka
<g/>
,	,	kIx,	,
Osijeku	Osijek	k1gInSc6	Osijek
a	a	k8xC	a
Pule	Pule	k1gFnSc6	Pule
<g/>
.	.	kIx.	.
</s>
<s>
Národním	národní	k2eAgMnSc7d1	národní
leteckým	letecký	k2eAgMnSc7d1	letecký
dopravcem	dopravce	k1gMnSc7	dopravce
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
Croatia	Croatium	k1gNnSc2	Croatium
Airlines	Airlinesa	k1gFnPc2	Airlinesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
žije	žít	k5eAaImIp3nS	žít
4	[number]	k4	4
437	[number]	k4	437
460	[number]	k4	460
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
301	[number]	k4	301
560	[number]	k4	560
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
2	[number]	k4	2
135	[number]	k4	135
900	[number]	k4	900
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
je	být	k5eAaImIp3nS	být
39,3	[number]	k4	39,3
(	(	kIx(	(
<g/>
muži	muž	k1gMnPc7	muž
37,5	[number]	k4	37,5
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
41,0	[number]	k4	41,0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
75	[number]	k4	75
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
gramotných	gramotný	k2eAgInPc2d1	gramotný
je	být	k5eAaImIp3nS	být
98,5	[number]	k4	98,5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
přírůstek	přírůstek	k1gInSc1	přírůstek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
0,4	[number]	k4	0,4
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
(	(	kIx(	(
<g/>
z	z	k7c2	z
0,82	[number]	k4	0,82
%	%	kIx~	%
v	v	k7c6	v
r.	r.	kA	r.
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Národnostní	národnostní	k2eAgNnSc4d1	národnostní
složení	složení	k1gNnSc4	složení
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Česká	český	k2eAgFnSc1d1	Česká
menšina	menšina	k1gFnSc1	menšina
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Češi	Čech	k1gMnPc1	Čech
se	se	k3xPyFc4	se
do	do	k7c2	do
chorvatské	chorvatský	k2eAgFnSc2d1	chorvatská
Slavonie	Slavonie	k1gFnSc2	Slavonie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
stěhovat	stěhovat	k5eAaImF	stěhovat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
vylidněná	vylidněný	k2eAgFnSc1d1	vylidněná
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tureckých	turecký	k2eAgFnPc2d1	turecká
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
Čechů	Čech	k1gMnPc2	Čech
se	se	k3xPyFc4	se
ve	v	k7c6	v
Slavonii	Slavonie	k1gFnSc6	Slavonie
usazovali	usazovat	k5eAaImAgMnP	usazovat
také	také	k9	také
Rusíni	Rusín	k1gMnPc1	Rusín
<g/>
,	,	kIx,	,
Slováci	Slovák	k1gMnPc1	Slovák
nebo	nebo	k8xC	nebo
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
na	na	k7c4	na
40	[number]	k4	40
tisíc	tisíc	k4xCgInPc2	tisíc
Čechů	Čech	k1gMnPc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
10	[number]	k4	10
510	[number]	k4	510
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
římskokatolíci	římskokatolík	k1gMnPc1	římskokatolík
87,83	[number]	k4	87,83
%	%	kIx~	%
</s>
</p>
<p>
<s>
pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
4,4	[number]	k4	4,4
%	%	kIx~	%
</s>
</p>
<p>
<s>
muslimové	muslim	k1gMnPc1	muslim
1,3	[number]	k4	1,3
%	%	kIx~	%
</s>
</p>
<p>
<s>
Procentuální	procentuální	k2eAgNnSc1d1	procentuální
zastoupení	zastoupení	k1gNnSc1	zastoupení
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
etnické	etnický	k2eAgNnSc1d1	etnické
složení	složení	k1gNnSc1	složení
<g/>
:	:	kIx,	:
katolíci	katolík	k1gMnPc1	katolík
jsou	být	k5eAaImIp3nP	být
Chorvati	Chorvat	k1gMnPc1	Chorvat
<g/>
,	,	kIx,	,
s	s	k7c7	s
pravoslavím	pravoslaví	k1gNnSc7	pravoslaví
se	se	k3xPyFc4	se
ztotožňují	ztotožňovat	k5eAaImIp3nP	ztotožňovat
Srbové	Srb	k1gMnPc1	Srb
<g/>
,	,	kIx,	,
s	s	k7c7	s
islámem	islám	k1gInSc7	islám
Bosňáci	Bosňáci	k?	Bosňáci
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Za	za	k7c4	za
Chorvata	Chorvat	k1gMnSc4	Chorvat
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
považován	považován	k2eAgMnSc1d1	považován
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
Ivo	Ivo	k1gMnSc1	Ivo
Andrić	Andrić	k1gMnSc1	Andrić
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Bosně	Bosna	k1gFnSc6	Bosna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
barokní	barokní	k2eAgFnSc6d1	barokní
éře	éra	k1gFnSc6	éra
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
dubrovnický	dubrovnický	k2eAgMnSc1d1	dubrovnický
básník	básník	k1gMnSc1	básník
Ivan	Ivan	k1gMnSc1	Ivan
Gundulić	Gundulić	k1gMnSc1	Gundulić
<g/>
.	.	kIx.	.
</s>
<s>
Symboly	symbol	k1gInPc1	symbol
renesančního	renesanční	k2eAgInSc2d1	renesanční
humanismu	humanismus	k1gInSc2	humanismus
jsou	být	k5eAaImIp3nP	být
Marko	Marko	k1gMnSc1	Marko
Marulić	Marulić	k1gMnSc1	Marulić
a	a	k8xC	a
Janus	Janus	k1gMnSc1	Janus
Pannonius	Pannonius	k1gMnSc1	Pannonius
<g/>
.	.	kIx.	.
</s>
<s>
Klasikem	klasik	k1gMnSc7	klasik
realistické	realistický	k2eAgFnSc2d1	realistická
prózy	próza	k1gFnSc2	próza
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
August	August	k1gMnSc1	August
Šenoa	Šenoa	k1gMnSc1	Šenoa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
meziválečné	meziválečný	k2eAgFnSc6d1	meziválečná
éře	éra	k1gFnSc6	éra
proslul	proslout	k5eAaPmAgMnS	proslout
básník	básník	k1gMnSc1	básník
Tin	Tina	k1gFnPc2	Tina
Ujević	Ujević	k1gMnSc1	Ujević
či	či	k8xC	či
Vladimir	Vladimir	k1gMnSc1	Vladimir
Nazor	Nazor	k1gMnSc1	Nazor
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
autorem	autor	k1gMnSc7	autor
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgMnS	být
Miroslav	Miroslav	k1gMnSc1	Miroslav
Krleža	Krleža	k1gMnSc1	Krleža
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
Dubravka	Dubravka	k1gFnSc1	Dubravka
Ugrešićová	Ugrešićová	k1gFnSc1	Ugrešićová
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
autorkou	autorka	k1gFnSc7	autorka
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
je	být	k5eAaImIp3nS	být
Ivana	Ivana	k1gFnSc1	Ivana
Brlićová-Mažuranićová	Brlićová-Mažuranićová	k1gFnSc1	Brlićová-Mažuranićová
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejpřekládanějším	překládaný	k2eAgMnPc3d3	nejpřekládanější
současným	současný	k2eAgMnPc3d1	současný
autorům	autor	k1gMnPc3	autor
patří	patřit	k5eAaImIp3nS	patřit
Slavenka	Slavenka	k1gFnSc1	Slavenka
Drakulićová	Drakulićová	k1gFnSc1	Drakulićová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
hudebním	hudební	k2eAgMnSc7d1	hudební
skladatelem	skladatel	k1gMnSc7	skladatel
je	být	k5eAaImIp3nS	být
Ivan	Ivan	k1gMnSc1	Ivan
Zajc	Zajc	k1gInSc1	Zajc
<g/>
.	.	kIx.	.
</s>
<s>
Illyrismus	Illyrismus	k1gInSc1	Illyrismus
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
reprezentoval	reprezentovat	k5eAaImAgMnS	reprezentovat
zejména	zejména	k9	zejména
skladatel	skladatel	k1gMnSc1	skladatel
Vatroslav	Vatroslav	k1gMnSc1	Vatroslav
Lisinski	Lisinski	k1gNnPc2	Lisinski
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
se	se	k3xPyFc4	se
chorvatskému	chorvatský	k2eAgInSc3d1	chorvatský
otci	otec	k1gMnSc6	otec
narodil	narodit	k5eAaPmAgMnS	narodit
Goran	Goran	k1gMnSc1	Goran
Bregović	Bregović	k1gMnSc1	Bregović
<g/>
,	,	kIx,	,
proslulý	proslulý	k2eAgMnSc1d1	proslulý
představitel	představitel	k1gMnSc1	představitel
world	world	k1gMnSc1	world
music	music	k1gMnSc1	music
<g/>
.	.	kIx.	.
</s>
<s>
Maksim	Maksim	k1gMnSc1	Maksim
Mrvica	Mrvica	k1gMnSc1	Mrvica
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
jako	jako	k8xS	jako
klavírista	klavírista	k1gMnSc1	klavírista
kombinující	kombinující	k2eAgInSc4d1	kombinující
pop	pop	k1gInSc4	pop
a	a	k8xC	a
klasiku	klasika	k1gFnSc4	klasika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
klavírista	klavírista	k1gMnSc1	klavírista
proslul	proslout	k5eAaPmAgMnS	proslout
Ivo	Ivo	k1gMnSc1	Ivo
Pogorelich	Pogorelich	k1gMnSc1	Pogorelich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejznámějších	známý	k2eAgInPc2d3	nejznámější
sochařem	sochař	k1gMnSc7	sochař
je	být	k5eAaImIp3nS	být
Ivan	Ivan	k1gMnSc1	Ivan
Meštrović	Meštrović	k1gMnSc7	Meštrović
<g/>
,	,	kIx,	,
malířem	malíř	k1gMnSc7	malíř
Vlaho	vlaho	k1gNnSc1	vlaho
Bukovac	Bukovac	k1gInSc4	Bukovac
<g/>
,	,	kIx,	,
či	či	k8xC	či
Giulio	Giulio	k1gMnSc1	Giulio
Clovio	Clovio	k1gMnSc1	Clovio
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
miniatur	miniatura	k1gFnPc2	miniatura
a	a	k8xC	a
iluminátor	iluminátor	k1gInSc1	iluminátor
středověkých	středověký	k2eAgMnPc2d1	středověký
rukopisů	rukopis	k1gInPc2	rukopis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Goran	Goran	k1gMnSc1	Goran
Višnjić	Višnjić	k1gMnSc1	Višnjić
a	a	k8xC	a
Mira	Mira	k1gFnSc1	Mira
Furlanová	Furlanová	k1gFnSc1	Furlanová
se	se	k3xPyFc4	se
prosadili	prosadit	k5eAaPmAgMnP	prosadit
jako	jako	k9	jako
televizní	televizní	k2eAgMnPc1d1	televizní
a	a	k8xC	a
filmoví	filmový	k2eAgMnPc1d1	filmový
herci	herec	k1gMnPc1	herec
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Architektonickými	architektonický	k2eAgFnPc7d1	architektonická
památkami	památka	k1gFnPc7	památka
zapsanými	zapsaný	k2eAgFnPc7d1	zapsaná
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
jsou	být	k5eAaImIp3nP	být
Diokleciánův	Diokleciánův	k2eAgInSc4d1	Diokleciánův
palác	palác	k1gInSc4	palác
ve	v	k7c6	v
Splitu	Split	k1gInSc6	Split
<g/>
,	,	kIx,	,
Eufraziova	Eufraziův	k2eAgFnSc1d1	Eufraziova
bazilika	bazilika	k1gFnSc1	bazilika
v	v	k7c6	v
Poreči	Poreč	k1gInSc6	Poreč
a	a	k8xC	a
Katedrála	katedrála	k1gFnSc1	katedrála
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
v	v	k7c6	v
Šibeniku	Šibenik	k1gInSc6	Šibenik
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
zapsána	zapsán	k2eAgFnSc1d1	zapsána
i	i	k8xC	i
celá	celý	k2eAgFnSc1d1	celá
dvě	dva	k4xCgNnPc1	dva
historická	historický	k2eAgNnPc1d1	historické
města	město	k1gNnPc1	město
-	-	kIx~	-
Dubrovník	Dubrovník	k1gInSc1	Dubrovník
a	a	k8xC	a
Trogir	Trogir	k1gInSc1	Trogir
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
k	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
stavbám	stavba	k1gFnPc3	stavba
patří	patřit	k5eAaImIp3nS	patřit
biskupský	biskupský	k2eAgInSc4d1	biskupský
komplex	komplex	k1gInSc4	komplex
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
jádru	jádro	k1gNnSc6	jádro
města	město	k1gNnSc2	město
Zadaru	Zadar	k1gInSc2	Zadar
<g/>
,	,	kIx,	,
středověký	středověký	k2eAgInSc1d1	středověký
hrad	hrad	k1gInSc1	hrad
Veliki	Veliki	k1gNnSc2	Veliki
Tabor	Tabor	k1gInSc1	Tabor
či	či	k8xC	či
skalní	skalní	k2eAgInSc1d1	skalní
klášter	klášter	k1gInSc1	klášter
Blaca	Blac	k1gInSc2	Blac
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Brač	Brač	k1gInSc1	Brač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Věda	věda	k1gFnSc1	věda
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
vesničce	vesnička	k1gFnSc6	vesnička
Smiljan	Smiljana	k1gFnPc2	Smiljana
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
slavný	slavný	k2eAgMnSc1d1	slavný
fyzik	fyzik	k1gMnSc1	fyzik
Nikola	Nikola	k1gMnSc1	Nikola
Tesla	Tesla	k1gMnSc1	Tesla
<g/>
.	.	kIx.	.
</s>
<s>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
získali	získat	k5eAaPmAgMnP	získat
Vladimir	Vladimir	k1gMnSc1	Vladimir
Prelog	Prelog	k1gMnSc1	Prelog
a	a	k8xC	a
Leopold	Leopold	k1gMnSc1	Leopold
Ružička	Ružička	k1gMnSc1	Ružička
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
meteorologem	meteorolog	k1gMnSc7	meteorolog
a	a	k8xC	a
seismologem	seismolog	k1gMnSc7	seismolog
byl	být	k5eAaImAgMnS	být
Andrija	Andrija	k1gMnSc1	Andrija
Mohorovičić	Mohorovičić	k1gMnSc1	Mohorovičić
<g/>
,	,	kIx,	,
optikem	optik	k1gMnSc7	optik
pak	pak	k6eAd1	pak
Marin	Marina	k1gFnPc2	Marina
Getaldić	Getaldić	k1gMnPc2	Getaldić
<g/>
.	.	kIx.	.
</s>
<s>
Všestranným	všestranný	k2eAgMnSc7d1	všestranný
badatelem	badatel	k1gMnSc7	badatel
byl	být	k5eAaImAgMnS	být
jezuita	jezuita	k1gMnSc1	jezuita
Ruđer	Ruđer	k1gMnSc1	Ruđer
Bošković	Bošković	k1gMnSc1	Bošković
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
astronomie	astronomie	k1gFnSc2	astronomie
mj.	mj.	kA	mj.
našel	najít	k5eAaPmAgMnS	najít
způsob	způsob	k1gInSc4	způsob
výpočtu	výpočet	k1gInSc2	výpočet
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
planet	planeta	k1gFnPc2	planeta
a	a	k8xC	a
objevil	objevit	k5eAaPmAgMnS	objevit
neexistenci	neexistence	k1gFnSc4	neexistence
atmosféry	atmosféra	k1gFnSc2	atmosféra
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníkem	průkopník	k1gMnSc7	průkopník
padákového	padákový	k2eAgNnSc2d1	padákové
létání	létání	k1gNnSc2	létání
byl	být	k5eAaImAgMnS	být
Faust	Faust	k1gMnSc1	Faust
Vrančić	Vrančić	k1gMnSc1	Vrančić
<g/>
.	.	kIx.	.
</s>
<s>
Slavoljub	Slavoljub	k1gMnSc1	Slavoljub
Eduard	Eduard	k1gMnSc1	Eduard
Penkala	Penkal	k1gMnSc2	Penkal
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
termofor	termofor	k1gInSc1	termofor
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Vukić	Vukić	k1gMnSc1	Vukić
zase	zase	k9	zase
torpédo	torpédo	k1gNnSc4	torpédo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Cresu	Cres	k1gInSc6	Cres
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
benátský	benátský	k2eAgMnSc1d1	benátský
filozof	filozof	k1gMnSc1	filozof
Franciscus	Franciscus	k1gMnSc1	Franciscus
Patricius	Patricius	k1gMnSc1	Patricius
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Donji	Donj	k1gMnSc6	Donj
Kraljevci	Kraljevce	k1gMnSc6	Kraljevce
zakladatel	zakladatel	k1gMnSc1	zakladatel
antroposofie	antroposofie	k1gFnSc2	antroposofie
Rudolf	Rudolf	k1gMnSc1	Rudolf
Steiner	Steiner	k1gMnSc1	Steiner
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatský	chorvatský	k2eAgInSc1d1	chorvatský
původ	původ	k1gInSc1	původ
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
filozof	filozof	k1gMnSc1	filozof
Ivan	Ivan	k1gMnSc1	Ivan
Illich	Illich	k1gMnSc1	Illich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
vědeckým	vědecký	k2eAgFnPc3d1	vědecká
a	a	k8xC	a
vzdělávacím	vzdělávací	k2eAgFnPc3d1	vzdělávací
institucím	instituce	k1gFnPc3	instituce
patří	patřit	k5eAaImIp3nS	patřit
Institut	institut	k1gInSc1	institut
Ruđer	Ruđer	k1gMnSc1	Ruđer
Bošković	Bošković	k1gMnSc1	Bošković
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
<g/>
,	,	kIx,	,
Chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
Hrvatska	Hrvatsko	k1gNnSc2	Hrvatsko
akademija	akademij	k1gInSc2	akademij
znanosti	znanost	k1gFnSc2	znanost
i	i	k8xC	i
umjetnosti	umjetnost	k1gFnSc2	umjetnost
<g/>
)	)	kIx)	)
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
(	(	kIx(	(
<g/>
jakožto	jakožto	k8xS	jakožto
nejstarší	starý	k2eAgFnPc1d3	nejstarší
instituce	instituce	k1gFnPc1	instituce
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
univerzita	univerzita	k1gFnSc1	univerzita
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1396	[number]	k4	1396
v	v	k7c6	v
Zadaru	Zadar	k1gInSc6	Zadar
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1807	[number]	k4	1807
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gFnSc1	její
činnost	činnost	k1gFnSc1	činnost
zastavena	zastaven	k2eAgFnSc1d1	zastavena
(	(	kIx(	(
<g/>
obnovena	obnoven	k2eAgFnSc1d1	obnovena
až	až	k8xS	až
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
má	mít	k5eAaImIp3nS	mít
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
osm	osm	k4xCc1	osm
veřejných	veřejný	k2eAgFnPc2d1	veřejná
univerzit	univerzita	k1gFnPc2	univerzita
(	(	kIx(	(
<g/>
v	v	k7c6	v
Dubrovníku	Dubrovník	k1gInSc6	Dubrovník
<g/>
,	,	kIx,	,
Osijeku	Osijek	k1gInSc6	Osijek
<g/>
,	,	kIx,	,
Pule	Pule	k1gFnSc6	Pule
<g/>
,	,	kIx,	,
Rijece	Rijeka	k1gFnSc6	Rijeka
<g/>
,	,	kIx,	,
Splitu	Split	k1gInSc6	Split
<g/>
,	,	kIx,	,
Zadaru	Zadar	k1gInSc2	Zadar
a	a	k8xC	a
Záhřebu	Záhřeb	k1gInSc2	Záhřeb
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
soukromé	soukromý	k2eAgFnPc4d1	soukromá
(	(	kIx(	(
<g/>
Katolická	katolický	k2eAgFnSc1d1	katolická
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
a	a	k8xC	a
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Dubrovníku	Dubrovník	k1gInSc6	Dubrovník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
je	být	k5eAaImIp3nS	být
Záhřebská	záhřebský	k2eAgFnSc1d1	Záhřebská
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
Sveučilište	Sveučilište	k1gFnSc1	Sveučilište
u	u	k7c2	u
Zagrebu	Zagreb	k1gInSc2	Zagreb
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
studovalo	studovat	k5eAaImAgNnS	studovat
72	[number]	k4	72
480	[number]	k4	480
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Nejpopulárnějším	populární	k2eAgInSc7d3	nejpopulárnější
sportem	sport	k1gInSc7	sport
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
je	být	k5eAaImIp3nS	být
fotbal	fotbal	k1gInSc1	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
reprezentace	reprezentace	k1gFnSc1	reprezentace
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
svého	svůj	k3xOyFgInSc2	svůj
největšího	veliký	k2eAgInSc2d3	veliký
úspěchu	úspěch	k1gInSc2	úspěch
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Tahounem	tahoun	k1gInSc7	tahoun
tohoto	tento	k3xDgNnSc2	tento
mužstva	mužstvo	k1gNnSc2	mužstvo
byl	být	k5eAaImAgInS	být
především	především	k9	především
Luka	luka	k1gNnPc4	luka
Modrić	Modrić	k1gFnPc2	Modrić
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
čtyřnásobný	čtyřnásobný	k2eAgMnSc1d1	čtyřnásobný
vítěz	vítěz	k1gMnSc1	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
s	s	k7c7	s
Realem	Real	k1gInSc7	Real
Madrid	Madrid	k1gInSc1	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
výraznými	výrazný	k2eAgFnPc7d1	výrazná
osobnostmi	osobnost	k1gFnPc7	osobnost
stříbrného	stříbrný	k2eAgInSc2d1	stříbrný
týmu	tým	k1gInSc2	tým
byli	být	k5eAaImAgMnP	být
Ivan	Ivan	k1gMnSc1	Ivan
Perišić	Perišić	k1gMnSc1	Perišić
<g/>
,	,	kIx,	,
Mario	Mario	k1gMnSc1	Mario
Mandžukić	Mandžukić	k1gMnSc1	Mandžukić
a	a	k8xC	a
Ivan	Ivan	k1gMnSc1	Ivan
Rakitić	Rakitić	k1gMnSc1	Rakitić
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
získali	získat	k5eAaPmAgMnP	získat
Chorvati	Chorvat	k1gMnPc1	Chorvat
medaili	medaile	k1gFnSc4	medaile
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
<g/>
.	.	kIx.	.
</s>
<s>
Pilířem	pilíř	k1gInSc7	pilíř
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
především	především	k9	především
útočník	útočník	k1gMnSc1	útočník
Davor	Davor	k1gMnSc1	Davor
Šuker	Šuker	k1gMnSc1	Šuker
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
celého	celý	k2eAgInSc2d1	celý
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
týmu	tým	k1gInSc2	tým
byl	být	k5eAaImAgMnS	být
i	i	k8xC	i
Robert	Robert	k1gMnSc1	Robert
Prosinečki	Prosinečk	k1gFnSc2	Prosinečk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
nejvýraznějším	výrazný	k2eAgMnPc3d3	nejvýraznější
hráčům	hráč	k1gMnPc3	hráč
Jugoslávské	jugoslávský	k2eAgFnSc2d1	jugoslávská
reprezentace	reprezentace	k1gFnSc2	reprezentace
již	již	k6eAd1	již
na	na	k7c6	na
šampionátu	šampionát	k1gInSc6	šampionát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
jsou	být	k5eAaImIp3nP	být
Chorvati	Chorvat	k1gMnPc1	Chorvat
také	také	k9	také
v	v	k7c6	v
házené	házená	k1gFnSc6	házená
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
dvakrát	dvakrát	k6eAd1	dvakrát
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
olympijský	olympijský	k2eAgInSc4d1	olympijský
turnaj	turnaj	k1gInSc4	turnaj
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
také	také	k9	také
mistry	mistr	k1gMnPc4	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
hvězdou	hvězda	k1gFnSc7	hvězda
tohoto	tento	k3xDgInSc2	tento
týmu	tým	k1gInSc2	tým
byl	být	k5eAaImAgInS	být
Ivano	Ivana	k1gFnSc5	Ivana
Balić	Balić	k1gMnSc2	Balić
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
házenkářem	házenkář	k1gMnSc7	házenkář
světa	svět	k1gInSc2	svět
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003	[number]	k4	2003
a	a	k8xC	a
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
stejné	stejný	k2eAgFnSc2d1	stejná
pocty	pocta	k1gFnSc2	pocta
dostalo	dostat	k5eAaPmAgNnS	dostat
i	i	k9	i
Domagoji	Domagoj	k1gInSc6	Domagoj
Duvnjakovi	Duvnjak	k1gMnSc3	Duvnjak
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
Chorvatů	Chorvat	k1gMnPc2	Chorvat
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
již	již	k6eAd1	již
na	na	k7c6	na
zisku	zisk	k1gInSc6	zisk
dvou	dva	k4xCgFnPc2	dva
zlatých	zlatý	k2eAgFnPc2d1	zlatá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
za	za	k7c2	za
časů	čas	k1gInPc2	čas
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nesrovnatelně	srovnatelně	k6eNd1	srovnatelně
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
zeměmi	zem	k1gFnPc7	zem
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
oblíbené	oblíbený	k2eAgNnSc4d1	oblíbené
vodní	vodní	k2eAgNnSc4d1	vodní
pólo	pólo	k1gNnSc4	pólo
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
reprezentace	reprezentace	k1gFnSc1	reprezentace
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
sportu	sport	k1gInSc6	sport
získala	získat	k5eAaPmAgFnS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgInSc2	ten
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
OH	OH	kA	OH
dvě	dva	k4xCgNnPc1	dva
stříbra	stříbro	k1gNnPc1	stříbro
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatští	chorvatský	k2eAgMnPc1d1	chorvatský
basketbalisté	basketbalista	k1gMnPc1	basketbalista
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
kontě	konto	k1gNnSc6	konto
jedno	jeden	k4xCgNnSc4	jeden
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
stříbro	stříbro	k1gNnSc4	stříbro
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
nejúspěšnějším	úspěšný	k2eAgMnPc3d3	nejúspěšnější
košíkářům	košíkář	k1gMnPc3	košíkář
patřili	patřit	k5eAaImAgMnP	patřit
Toni	Toni	k1gFnPc1	Toni
Kukoč	Kukoč	k1gFnSc7	Kukoč
(	(	kIx(	(
<g/>
pětkrát	pětkrát	k6eAd1	pětkrát
vítěz	vítěz	k1gMnSc1	vítěz
ankety	anketa	k1gFnSc2	anketa
Euroscar	Euroscar	k1gMnSc1	Euroscar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dražen	dražen	k2eAgMnSc1d1	dražen
Petrović	Petrović	k1gMnSc1	Petrović
(	(	kIx(	(
<g/>
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
vítěz	vítěz	k1gMnSc1	vítěz
Euroscar	Euroscar	k1gMnSc1	Euroscar
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Krešimir	Krešimir	k1gMnSc1	Krešimir
Ćosić	Ćosić	k1gMnSc1	Ćosić
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
basketbalové	basketbalový	k2eAgFnSc2d1	basketbalová
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
volejbalové	volejbalový	k2eAgFnSc2d1	volejbalová
federace	federace	k1gFnSc2	federace
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
Irina	Irina	k1gFnSc1	Irina
Kirillova	Kirillův	k2eAgFnSc1d1	Kirillova
<g/>
.	.	kIx.	.
<g/>
Samostatné	samostatný	k2eAgNnSc1d1	samostatné
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
vybojovalo	vybojovat	k5eAaPmAgNnS	vybojovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
patnáct	patnáct	k4xCc1	patnáct
zlatých	zlatý	k2eAgFnPc2d1	zlatá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
hned	hned	k6eAd1	hned
čtyři	čtyři	k4xCgMnPc4	čtyři
sjezdařka	sjezdařka	k1gFnSc1	sjezdařka
Janica	Janica	k1gFnSc1	Janica
Kostelićová	Kostelićová	k1gFnSc1	Kostelićová
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
pak	pak	k8xC	pak
diskařka	diskařka	k1gFnSc1	diskařka
Sandra	Sandra	k1gFnSc1	Sandra
Perkovićová	Perkovićová	k1gFnSc1	Perkovićová
<g/>
.	.	kIx.	.
</s>
<s>
Individuální	individuální	k2eAgFnPc1d1	individuální
zlaté	zlatý	k2eAgFnPc1d1	zlatá
medaile	medaile	k1gFnPc1	medaile
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
oštěpařka	oštěpařka	k1gFnSc1	oštěpařka
Sara	Sarum	k1gNnSc2	Sarum
Kolaková	Kolakový	k2eAgFnSc1d1	Kolakový
<g/>
,	,	kIx,	,
vzpěrač	vzpěrač	k1gMnSc1	vzpěrač
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Pešalov	Pešalov	k1gInSc4	Pešalov
<g/>
,	,	kIx,	,
střelci	střelec	k1gMnSc3	střelec
Giovanni	Giovanň	k1gMnSc3	Giovanň
Cernogoraz	Cernogoraz	k1gInSc4	Cernogoraz
a	a	k8xC	a
Josip	Josip	k1gInSc4	Josip
Glasnović	Glasnović	k1gFnSc2	Glasnović
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgInPc1d1	zbylý
nejcennější	cenný	k2eAgInPc1d3	nejcennější
kovy	kov	k1gInPc1	kov
připadají	připadat	k5eAaImIp3nP	připadat
na	na	k7c4	na
veslařské	veslařský	k2eAgFnPc4d1	Veslařská
a	a	k8xC	a
jachtařské	jachtařský	k2eAgFnPc4d1	jachtařská
posádky	posádka	k1gFnPc4	posádka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dresu	dres	k1gInSc6	dres
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
na	na	k7c4	na
nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
olympijské	olympijský	k2eAgInPc4d1	olympijský
vavříny	vavřín	k1gInPc4	vavřín
plavkyně	plavkyně	k1gFnSc1	plavkyně
Đurđica	Đurđica	k1gFnSc1	Đurđica
Bjedovová	Bjedovová	k1gFnSc1	Bjedovová
<g/>
,	,	kIx,	,
kanoista	kanoista	k1gMnSc1	kanoista
Matija	Matija	k1gMnSc1	Matija
Ljubek	Ljubek	k1gMnSc1	Ljubek
<g/>
,	,	kIx,	,
boxer	boxer	k1gMnSc1	boxer
Mate	mást	k5eAaImIp3nS	mást
Parlov	Parlov	k1gInSc4	Parlov
a	a	k8xC	a
zápasník	zápasník	k1gMnSc1	zápasník
Vlado	Vlado	k1gNnSc1	Vlado
Lisjak	Lisjak	k1gMnSc1	Lisjak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
tenista	tenista	k1gMnSc1	tenista
Goran	Goran	k1gMnSc1	Goran
Ivanišević	Ivanišević	k1gMnSc1	Ivanišević
<g/>
.	.	kIx.	.
</s>
<s>
Marin	Marina	k1gFnPc2	Marina
Čilić	Čilić	k1gMnSc1	Čilić
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
US	US	kA	US
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
Iva	Iva	k1gFnSc1	Iva
Majoliová	Majoliový	k2eAgFnSc1d1	Majoliová
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Chorvati	Chorvat	k1gMnPc1	Chorvat
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
též	též	k9	též
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
Davis	Davis	k1gFnPc4	Davis
Cup	cup	k1gInSc1	cup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Státní	státní	k2eAgInPc1d1	státní
svátky	svátek	k1gInPc1	svátek
a	a	k8xC	a
dny	den	k1gInPc1	den
pracovního	pracovní	k2eAgInSc2d1	pracovní
klidu	klid	k1gInSc2	klid
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Státní	státní	k2eAgInPc1d1	státní
svátky	svátek	k1gInPc1	svátek
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
leden	leden	k1gInSc4	leden
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
Svátek	svátek	k1gInSc4	svátek
práce	práce	k1gFnSc2	práce
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
Den	den	k1gInSc4	den
Chorvatského	chorvatský	k2eAgInSc2d1	chorvatský
parlamentu	parlament	k1gInSc2	parlament
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
červen	červen	k2eAgInSc1d1	červen
Den	den	k1gInSc1	den
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
fašismu	fašismus	k1gInSc3	fašismus
−	−	k?	−
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
se	se	k3xPyFc4	se
k	k	k7c3	k
začátku	začátek	k1gInSc2	začátek
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
červen	červen	k2eAgInSc1d1	červen
Den	den	k1gInSc1	den
státnosti	státnost	k1gFnSc2	státnost
−	−	k?	−
výročí	výročí	k1gNnSc1	výročí
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
Den	den	k1gInSc1	den
národní	národní	k2eAgFnSc2d1	národní
vděčnosti	vděčnost	k1gFnSc2	vděčnost
(	(	kIx(	(
<g/>
osvobození	osvobození	k1gNnSc4	osvobození
Kninu	Knin	k1gInSc2	Knin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
Svátek	svátek	k1gInSc1	svátek
Nanebevzetí	nanebevzetí	k1gNnSc6	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
říjen	říjen	k2eAgInSc1d1	říjen
Den	den	k1gInSc1	den
nezávislosti	nezávislost	k1gFnSc2	nezávislost
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
1	[number]	k4	1
<g/>
.	.	kIx.	.
svátek	svátek	k1gInSc4	svátek
vánoční	vánoční	k2eAgInSc4d1	vánoční
</s>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
2	[number]	k4	2
<g/>
.	.	kIx.	.
svátek	svátek	k1gInSc4	svátek
vánoční	vánoční	k2eAgInSc4d1	vánoční
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Dny	dna	k1gFnPc4	dna
pracovního	pracovní	k2eAgInSc2d1	pracovní
klidu	klid	k1gInSc2	klid
===	===	k?	===
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
Tři	tři	k4xCgMnPc4	tři
králové	králová	k1gFnSc6	králová
</s>
</p>
<p>
<s>
Velikonoční	velikonoční	k2eAgNnSc1d1	velikonoční
pondělí	pondělí	k1gNnSc1	pondělí
(	(	kIx(	(
<g/>
pohyblivý	pohyblivý	k2eAgInSc1d1	pohyblivý
svátek	svátek	k1gInSc1	svátek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Svátek	svátek	k1gInSc4	svátek
Božího	boží	k2eAgNnSc2d1	boží
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
pohyblivý	pohyblivý	k2eAgInSc1d1	pohyblivý
svátek	svátek	k1gInSc1	svátek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
Památka	památka	k1gFnSc1	památka
zesnulých	zesnulý	k1gMnPc2	zesnulý
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HAVLÍKOVÁ	Havlíková	k1gFnSc1	Havlíková
LUBOMÍRA	Lubomíra	k1gFnSc1	Lubomíra
<g/>
,	,	kIx,	,
HLADKÝ	Hladký	k1gMnSc1	Hladký
LADISLAV	Ladislav	k1gMnSc1	Ladislav
<g/>
,	,	kIx,	,
PELIKÁN	Pelikán	k1gMnSc1	Pelikán
JAN	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
ŠESTÁK	Šesták	k1gMnSc1	Šesták
MIROSLAV	Miroslav	k1gMnSc1	Miroslav
<g/>
,	,	kIx,	,
TEJCHMAN	TEJCHMAN	kA	TEJCHMAN
MIROSLAV	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Jihoslovanských	jihoslovanský	k2eAgFnPc2d1	Jihoslovanská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
375	[number]	k4	375
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RYCHLÍK	Rychlík	k1gMnSc1	Rychlík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
PERENČEVIČ	PERENČEVIČ	kA	PERENČEVIČ
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
576	[number]	k4	576
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
885	[number]	k4	885
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠESTÁK	Šesták	k1gMnSc1	Šesták
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Slovanský	slovanský	k2eAgInSc1d1	slovanský
přehled	přehled	k1gInSc1	přehled
<g/>
.	.	kIx.	.
</s>
<s>
Review	Review	k?	Review
for	forum	k1gNnPc2	forum
Central	Central	k1gMnPc2	Central
<g/>
,	,	kIx,	,
Eastern	Eastern	k1gMnSc1	Eastern
and	and	k?	and
Southeastern	Southeastern	k1gInSc1	Southeastern
European	European	k1gInSc1	European
History	Histor	k1gInPc1	Histor
<g/>
.	.	kIx.	.
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
82	[number]	k4	82
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
45	[number]	k4	45
<g/>
-	-	kIx~	-
<g/>
86	[number]	k4	86
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
37	[number]	k4	37
<g/>
-	-	kIx~	-
<g/>
6922	[number]	k4	6922
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
představitelů	představitel	k1gMnPc2	představitel
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
chorvatských	chorvatský	k2eAgMnPc2d1	chorvatský
panovníků	panovník	k1gMnPc2	panovník
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
chorvatských	chorvatský	k2eAgFnPc2d1	chorvatská
královen	královna	k1gFnPc2	královna
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
měst	město	k1gNnPc2	město
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
letišť	letiště	k1gNnPc2	letiště
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
Chorvat	Chorvat	k1gMnSc1	Chorvat
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInPc1d1	státní
symboly	symbol	k1gInPc1	symbol
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
</s>
</p>
<p>
<s>
Chorvatština	chorvatština	k1gFnSc1	chorvatština
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Průvodce	průvodka	k1gFnSc3	průvodka
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
ve	v	k7c6	v
Wikicestách	Wikicesta	k1gFnPc6	Wikicesta
</s>
</p>
<p>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
</p>
<p>
<s>
Chorvatské	chorvatský	k2eAgNnSc1d1	Chorvatské
turistické	turistický	k2eAgNnSc1d1	turistické
sdružení	sdružení	k1gNnSc1	sdružení
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
chorvatsky	chorvatsky	k6eAd1	chorvatsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Chorvatský	chorvatský	k2eAgInSc1d1	chorvatský
hydrometeorologický	hydrometeorologický	k2eAgInSc1d1	hydrometeorologický
ústav	ústav	k1gInSc1	ústav
</s>
</p>
<p>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
-	-	kIx~	-
historie	historie	k1gFnSc1	historie
krajanů	krajan	k1gMnPc2	krajan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
2013-01-25	[number]	k4	2013-01-25
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Croatia	Croatia	k1gFnSc1	Croatia
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Croatia	Croatia	k1gFnSc1	Croatia
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Croatia	Croatius	k1gMnSc2	Croatius
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
European	European	k1gInSc1	European
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Croatia	Croatia	k1gFnSc1	Croatia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-04-06	[number]	k4	2011-04-06
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Croatia	Croatia	k1gFnSc1	Croatia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-06-06	[number]	k4	2011-06-06
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BRACEWELL	BRACEWELL	kA	BRACEWELL
<g/>
,	,	kIx,	,
C.	C.	kA	C.
W	W	kA	W
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Croatia	Croatia	k1gFnSc1	Croatia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
