<p>
<s>
Hana	Hana	k1gFnSc1	Hana
je	být	k5eAaImIp3nS	být
ženské	ženský	k2eAgNnSc4d1	ženské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
odvozenina	odvozenina	k1gFnSc1	odvozenina
od	od	k7c2	od
hebrejského	hebrejský	k2eAgMnSc2d1	hebrejský
Hannah	Hannah	k1gInSc4	Hannah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
jazyce	jazyk	k1gInSc6	jazyk
花	花	k?	花
nebo	nebo	k8xC	nebo
華	華	k?	華
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
květina	květina	k1gFnSc1	květina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
arabském	arabský	k2eAgInSc6d1	arabský
jazyce	jazyk	k1gInSc6	jazyk
znamená	znamenat	k5eAaImIp3nS	znamenat
blažená	blažený	k2eAgFnSc1d1	blažená
<g/>
,	,	kIx,	,
šťastná	šťastný	k2eAgFnSc1d1	šťastná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Hopiů	Hopi	k1gInPc2	Hopi
toto	tento	k3xDgNnSc4	tento
jméno	jméno	k1gNnSc4	jméno
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
"	"	kIx"	"
a	a	k8xC	a
patřilo	patřit	k5eAaImAgNnS	patřit
pouze	pouze	k6eAd1	pouze
chlapcům	chlapec	k1gMnPc3	chlapec
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
Hana	Hana	k1gFnSc1	Hana
svátek	svátek	k1gInSc1	svátek
(	(	kIx(	(
<g/>
jmeniny	jmeniny	k1gFnPc1	jmeniny
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
patřilo	patřit	k5eAaImAgNnS	patřit
toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
k	k	k7c3	k
nejoblíbenějším	oblíbený	k2eAgNnPc3d3	nejoblíbenější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Domácké	domácký	k2eAgFnSc2d1	domácká
podoby	podoba	k1gFnSc2	podoba
==	==	k?	==
</s>
</p>
<p>
<s>
Hanka	Hanka	k1gFnSc1	Hanka
<g/>
,	,	kIx,	,
Hanička	Hanička	k1gFnSc1	Hanička
<g/>
,	,	kIx,	,
Hanča	Hanča	k1gFnSc1	Hanča
<g/>
,	,	kIx,	,
Haninka	Haninka	k1gFnSc1	Haninka
<g/>
,	,	kIx,	,
Haňule	Haňule	k1gFnPc1	Haňule
<g/>
,	,	kIx,	,
Hanele	Hanel	k1gInPc1	Hanel
<g/>
,	,	kIx,	,
Hani	Hani	k?	Hani
<g/>
,	,	kIx,	,
Háňa	Háňa	k1gMnSc1	Háňa
<g/>
,	,	kIx,	,
<g/>
Hanýsek	Hanýsek	k1gMnSc1	Hanýsek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cizojazyčné	cizojazyčný	k2eAgFnPc1d1	cizojazyčná
varianty	varianta	k1gFnPc1	varianta
<g/>
:	:	kIx,	:
Hania	Hania	k1gFnSc1	Hania
<g/>
,	,	kIx,	,
Hannah	Hannah	k1gInSc1	Hannah
<g/>
,	,	kIx,	,
Hannele	Hannel	k1gInPc1	Hannel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Statistické	statistický	k2eAgInPc1d1	statistický
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
ženskými	ženský	k2eAgNnPc7d1	ženské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaPmF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgFnPc7d1	žijící
ženami	žena	k1gFnPc7	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
+0,8	+0,8	k4	+0,8
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
procentní	procentní	k2eAgNnSc1d1	procentní
zastoupení	zastoupení	k1gNnSc1	zastoupení
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
absolutní	absolutní	k2eAgInSc1d1	absolutní
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
číst	číst	k5eAaImF	číst
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Známé	známý	k2eAgFnPc1d1	známá
nositelky	nositelka	k1gFnPc1	nositelka
==	==	k?	==
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Andronikova	Andronikův	k2eAgFnSc1d1	Andronikova
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Aulická	Aulická	k1gFnSc1	Aulická
Jírovcová	jírovcový	k2eAgFnSc1d1	jírovcová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Garde	garde	k1gFnSc1	garde
Bajtošová	Bajtošová	k1gFnSc1	Bajtošová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovenská	slovenský	k2eAgFnSc1d1	slovenská
reprezentantka	reprezentantka	k1gFnSc1	reprezentantka
v	v	k7c6	v
MTBO	MTBO	kA	MTBO
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Baroňová	Baroňová	k1gFnSc1	Baroňová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Bartková	Bartková	k1gFnSc1	Bartková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
regionální	regionální	k2eAgFnSc1d1	regionální
historička	historička	k1gFnSc1	historička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Benešová	Benešová	k1gFnSc1	Benešová
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
-	-	kIx~	-
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
prezidenta	prezident	k1gMnSc2	prezident
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Benešová	Benešová	k1gFnSc1	Benešová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
atletka	atletka	k1gFnSc1	atletka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Benoniová	Benoniový	k2eAgFnSc1d1	Benoniová
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
-	-	kIx~	-
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Blažíková	Blažíková	k1gFnSc1	Blažíková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
sopranistka	sopranistka	k1gFnSc1	sopranistka
a	a	k8xC	a
harfenistka	harfenistka	k1gFnSc1	harfenistka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Bobková	Bobková	k1gFnSc1	Bobková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
sportovní	sportovní	k2eAgFnSc1d1	sportovní
gymnastka	gymnastka	k1gFnSc1	gymnastka
a	a	k8xC	a
olympijská	olympijský	k2eAgFnSc1d1	olympijská
medailistka	medailistka	k1gFnSc1	medailistka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Bořkovcová	Bořkovcový	k2eAgFnSc1d1	Bořkovcová
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Bradyová	Bradyová	k1gFnSc1	Bradyová
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
židovská	židovský	k2eAgFnSc1d1	židovská
dívka	dívka	k1gFnSc1	dívka
zahnylá	zahnylat	k5eAaPmIp3nS	zahnylat
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
v	v	k7c6	v
Osvětimi	Osvětim	k1gFnSc6	Osvětim
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Brejchová	Brejchová	k1gFnSc1	Brejchová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Brixi	Brixe	k1gFnSc4	Brixe
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
ekonomka	ekonomka	k1gFnSc1	ekonomka
a	a	k8xC	a
odbornice	odbornice	k1gFnSc1	odbornice
na	na	k7c4	na
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
politiku	politika	k1gFnSc4	politika
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Brůhová	Brůhová	k1gFnSc1	Brůhová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československá	československý	k2eAgFnSc1d1	Československá
hráčka	hráčka	k1gFnSc1	hráčka
basketbalu	basketbal	k1gInSc2	basketbal
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Burešová	Burešová	k1gFnSc1	Burešová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
divadelní	divadelní	k2eAgFnSc1d1	divadelní
režisérka	režisérka	k1gFnSc1	režisérka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Buštíková	Buštíková	k1gFnSc1	Buštíková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Cavallarová	Cavallarová	k1gFnSc1	Cavallarová
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
operní	operní	k2eAgFnSc1d1	operní
pěvkyně-sopranistka	pěvkyněopranistka	k1gFnSc1	pěvkyně-sopranistka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Černá-Netrefová	Černá-Netrefový	k2eAgFnSc1d1	Černá-Netrefový
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalá	bývalý	k2eAgFnSc1d1	bývalá
česká	český	k2eAgFnSc1d1	Česká
plavecká	plavecký	k2eAgFnSc1d1	plavecká
reprezentantka	reprezentantka	k1gFnSc1	reprezentantka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Čížková	Čížková	k1gFnSc1	Čížková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Dariusová	Dariusový	k2eAgFnSc1d1	Dariusový
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
reprezentantka	reprezentantka	k1gFnSc1	reprezentantka
ve	v	k7c6	v
veslování	veslování	k1gNnSc6	veslování
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Dohnálková	Dohnálková	k1gFnSc1	Dohnálková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
muzikoložka	muzikoložka	k1gFnSc1	muzikoložka
<g/>
,	,	kIx,	,
houslistka	houslistka	k1gFnSc1	houslistka
a	a	k8xC	a
hudební	hudební	k2eAgFnSc1d1	hudební
pedagožka	pedagožka	k1gFnSc1	pedagožka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Doskočilová	Doskočilová	k1gFnSc1	Doskočilová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Dostalová	Dostalová	k1gFnSc1	Dostalová
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
malířka	malířka	k1gFnSc1	malířka
<g/>
,	,	kIx,	,
ilustrátorka	ilustrátorka	k1gFnSc1	ilustrátorka
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgFnSc1d1	textilní
a	a	k8xC	a
sklářská	sklářský	k2eAgFnSc1d1	sklářská
návrhářka	návrhářka	k1gFnSc1	návrhářka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Doupovcová	Doupovcový	k2eAgFnSc1d1	Doupovcová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Doušová-Jarošová	Doušová-Jarošová	k1gFnSc1	Doušová-Jarošová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československá	československý	k2eAgFnSc1d1	Československá
hráčka	hráčka	k1gFnSc1	hráčka
basketbalu	basketbal	k1gInSc2	basketbal
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Drábková	Drábková	k1gFnSc1	Drábková
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
psycholožka	psycholožka	k1gFnSc1	psycholožka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Dumková	Dumková	k1gFnSc1	Dumková
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
-	-	kIx~	-
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Dýčková	Dýčková	k1gFnSc1	Dýčková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Entlerová	Entlerová	k1gFnSc1	Entlerová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
pedagožka	pedagožka	k1gFnSc1	pedagožka
a	a	k8xC	a
bývalá	bývalý	k2eAgFnSc1d1	bývalá
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Ezrová	Ezrová	k1gFnSc1	Ezrová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československá	československý	k2eAgFnSc1d1	Československá
hráčka	hráčka	k1gFnSc1	hráčka
a	a	k8xC	a
funkcionářka	funkcionářka	k1gFnSc1	funkcionářka
basketbalu	basketbal	k1gInSc2	basketbal
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Fillová	Fillová	k1gFnSc1	Fillová
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
malířka	malířka	k1gFnSc1	malířka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Frejková	Frejková	k1gFnSc1	Frejková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
německo-židovského	německo-židovský	k2eAgInSc2d1	německo-židovský
původu	původ	k1gInSc2	původ
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Gerzanicová	Gerzanicový	k2eAgFnSc1d1	Gerzanicová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
básnířka	básnířka	k1gFnSc1	básnířka
<g/>
,	,	kIx,	,
překladatelka	překladatelka	k1gFnSc1	překladatelka
<g/>
,	,	kIx,	,
pedagožka	pedagožka	k1gFnSc1	pedagožka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Gregorová	Gregorová	k1gFnSc1	Gregorová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československá	československý	k2eAgFnSc1d1	Československá
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
moderátorka	moderátorka	k1gFnSc1	moderátorka
a	a	k8xC	a
divadelní	divadelní	k2eAgFnSc1d1	divadelní
manažerka	manažerka	k1gFnSc1	manažerka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Gregorová	Gregorová	k1gFnSc1	Gregorová
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
-	-	kIx~	-
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovenská	slovenský	k2eAgFnSc1d1	slovenská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
šansoniérka	šansoniérka	k1gFnSc1	šansoniérka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Heřmánková	Heřmánková	k1gFnSc1	Heřmánková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
moderátorka	moderátorka	k1gFnSc1	moderátorka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Holcnerová	Holcnerová	k1gFnSc1	Holcnerová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brněnská	brněnský	k2eAgFnSc1d1	brněnská
aktivistka	aktivistka	k1gFnSc1	aktivistka
<g/>
,	,	kIx,	,
politička	politička	k1gFnSc1	politička
a	a	k8xC	a
publicistka	publicistka	k1gFnSc1	publicistka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Holišová	Holišová	k1gFnSc1	Holišová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Horáková	Horáková	k1gFnSc1	Horáková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
reprezentantka	reprezentantka	k1gFnSc1	reprezentantka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Horecká	Horecká	k1gFnSc1	Horecká
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
country	country	k2eAgFnSc1d1	country
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
kytaristka	kytaristka	k1gFnSc1	kytaristka
<g/>
,	,	kIx,	,
textařka	textařka	k1gFnSc1	textařka
<g/>
,	,	kIx,	,
skladatelka	skladatelka	k1gFnSc1	skladatelka
a	a	k8xC	a
moderátorka	moderátorka	k1gFnSc1	moderátorka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Janků	Janků	k1gFnSc2	Janků
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
operní	operní	k2eAgFnSc1d1	operní
pěvkyně	pěvkyně	k1gFnSc1	pěvkyně
<g/>
–	–	k?	–
<g/>
sopranistka	sopranistka	k1gFnSc1	sopranistka
</s>
</p>
<p>
<s>
Hanka	Hanka	k1gFnSc1	Hanka
Jelínková	Jelínková	k1gFnSc1	Jelínková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
překladatelka	překladatelka	k1gFnSc1	překladatelka
<g/>
,	,	kIx,	,
podnikatelka	podnikatelka	k1gFnSc1	podnikatelka
a	a	k8xC	a
pedagožka	pedagožka	k1gFnSc1	pedagožka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Jiříčková	Jiříčková	k1gFnSc1	Jiříčková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
topmodelka	topmodelka	k1gFnSc1	topmodelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Jonášová	Jonášová	k1gFnSc1	Jonášová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
sopranistka	sopranistka	k1gFnSc1	sopranistka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Kantorová	Kantorová	k1gFnSc1	Kantorová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Klenková	Klenková	k1gFnSc1	Klenková
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autorka	autorka	k1gFnSc1	autorka
próz	próza	k1gFnPc2	próza
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
i	i	k9	i
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
,	,	kIx,	,
překladatelka	překladatelka	k1gFnSc1	překladatelka
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
publicistka	publicistka	k1gFnSc1	publicistka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Kofránková	Kofránková	k1gFnSc1	Kofránková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
režisérka	režisérka	k1gFnSc1	režisérka
a	a	k8xC	a
pedagožka	pedagožka	k1gFnSc1	pedagožka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Veronika	Veronika	k1gFnSc1	Veronika
Konvalinková	Konvalinková	k1gFnSc1	Konvalinková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
politička	politička	k1gFnSc1	politička
a	a	k8xC	a
učitelka	učitelka	k1gFnSc1	učitelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Marie	Marie	k1gFnSc1	Marie
Körnerová	Körnerová	k1gFnSc1	Körnerová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Kotková	Kotková	k1gFnSc1	Kotková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
houslistka	houslistka	k1gFnSc1	houslistka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Kousalová	Kousalová	k1gFnSc1	Kousalová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
moderátorka	moderátorka	k1gFnSc1	moderátorka
<g/>
,	,	kIx,	,
dramaturgyně	dramaturgyně	k1gFnSc1	dramaturgyně
a	a	k8xC	a
scenáristka	scenáristka	k1gFnSc1	scenáristka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Králíčková	Králíčková	k1gFnSc1	Králíčková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Křížková	Křížková	k1gFnSc1	Křížková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Kučerová-Záveská	Kučerová-Záveský	k2eAgFnSc1d1	Kučerová-Záveská
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
architektka	architektka	k1gFnSc1	architektka
<g/>
,	,	kIx,	,
návrhářka	návrhářka	k1gFnSc1	návrhářka
nábytku	nábytek	k1gInSc2	nábytek
a	a	k8xC	a
publicistka	publicistka	k1gFnSc1	publicistka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Kulhánková	Kulhánková	k1gFnSc1	Kulhánková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
odbornice	odbornice	k1gFnSc1	odbornice
<g/>
,	,	kIx,	,
ředitelka	ředitelka	k1gFnSc1	ředitelka
festivalu	festival	k1gInSc2	festival
dokumentárních	dokumentární	k2eAgInPc2d1	dokumentární
filmů	film	k1gInPc2	film
o	o	k7c6	o
lidských	lidský	k2eAgNnPc6d1	lidské
právech	právo	k1gNnPc6	právo
Jeden	jeden	k4xCgInSc1	jeden
svět	svět	k1gInSc1	svět
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Kvapilová	Kvapilová	k1gFnSc1	Kvapilová
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
-	-	kIx~	-
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Lagová	Lagová	k1gFnSc1	Lagová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Librová	librový	k2eAgFnSc1d1	Librová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
bioložka	bioložka	k1gFnSc1	bioložka
<g/>
,	,	kIx,	,
socioložka	socioložka	k1gFnSc1	socioložka
a	a	k8xC	a
environmentalistka	environmentalistka	k1gFnSc1	environmentalistka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Lišková	Lišková	k1gFnSc1	Lišková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československá	československý	k2eAgFnSc1d1	Československá
sportovní	sportovní	k2eAgFnSc1d1	sportovní
gymnastka	gymnastka	k1gFnSc1	gymnastka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Lundiaková	Lundiakový	k2eAgFnSc1d1	Lundiaková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
písničkářka	písničkářka	k1gFnSc1	písničkářka
a	a	k8xC	a
publicistka	publicistka	k1gFnSc1	publicistka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Maciuchová	Maciuchová	k1gFnSc1	Maciuchová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Machková	Machková	k1gFnSc1	Machková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
ekonomka	ekonomka	k1gFnSc1	ekonomka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Mandlíková	Mandlíková	k1gFnSc1	Mandlíková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
tenistka	tenistka	k1gFnSc1	tenistka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Marvanová	Marvanová	k1gFnSc1	Marvanová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
právnička	právnička	k1gFnSc1	právnička
a	a	k8xC	a
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Mašková	Mašková	k1gFnSc1	Mašková
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
-	-	kIx~	-
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
krasobruslařka	krasobruslařka	k1gFnSc1	krasobruslařka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Mašlíková	Mašlíková	k1gFnSc1	Mašlíková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
modelka	modelka	k1gFnSc1	modelka
a	a	k8xC	a
moderátorka	moderátorka	k1gFnSc1	moderátorka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Mejdrová	Mejdrová	k1gFnSc1	Mejdrová
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
historička	historička	k1gFnSc1	historička
a	a	k8xC	a
levicová	levicový	k2eAgFnSc1d1	levicová
intelektuálka	intelektuálka	k1gFnSc1	intelektuálka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Meličková	Meličková	k1gFnSc1	Meličková
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovenská	slovenský	k2eAgFnSc1d1	slovenská
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Michlová	Michlová	k1gFnSc1	Michlová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
poslankyně	poslankyně	k1gFnSc2	poslankyně
Sněmovny	sněmovna	k1gFnSc2	sněmovna
národů	národ	k1gInPc2	národ
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
za	za	k7c2	za
normalizace	normalizace	k1gFnSc2	normalizace
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Monská	Monská	k1gFnSc1	Monská
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
modelka	modelka	k1gFnSc1	modelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Moravcová	Moravcová	k1gFnSc1	Moravcová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
sestra	sestra	k1gFnSc1	sestra
<g/>
,	,	kIx,	,
podnikatelka	podnikatelka	k1gFnSc1	podnikatelka
a	a	k8xC	a
starostka	starostka	k1gFnSc1	starostka
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Praha	Praha	k1gFnSc1	Praha
20	[number]	k4	20
<g/>
-Horní	-Hornět	k5eAaPmIp3nS	-Hornět
Početnice	početnice	k1gFnSc1	početnice
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Moudrá	moudrý	k2eAgFnSc1d1	moudrá
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Mudrová	Mudrová	k1gFnSc1	Mudrová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
novinářka	novinářka	k1gFnSc1	novinářka
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Myslilová-Havlíková	Myslilová-Havlíková	k1gFnSc1	Myslilová-Havlíková
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československá	československý	k2eAgFnSc1d1	Československá
hráčka	hráčka	k1gFnSc1	hráčka
basketbalu	basketbal	k1gInSc2	basketbal
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Návratová	návratový	k2eAgFnSc1d1	návratová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Nosková	Nosková	k1gFnSc1	Nosková
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
malířka	malířka	k1gFnSc1	malířka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Orgoníková	Orgoníková	k1gFnSc1	Orgoníková
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Pinkavová	Pinkavový	k2eAgFnSc1d1	Pinkavová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
režisérka	režisérka	k1gFnSc1	režisérka
a	a	k8xC	a
scénáristka	scénáristka	k1gFnSc1	scénáristka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Pinknerová	Pinknerová	k1gFnSc1	Pinknerová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
křesťansky	křesťansky	k6eAd1	křesťansky
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
publicistka	publicistka	k1gFnSc1	publicistka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Pleskotová	Pleskotová	k1gFnSc1	Pleskotová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Poláková	Poláková	k1gFnSc1	Poláková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
florbalová	florbalový	k2eAgFnSc1d1	florbalová
útočnice	útočnice	k1gFnSc1	útočnice
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Ponická	Ponická	k1gFnSc1	Ponická
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovenská	slovenský	k2eAgFnSc1d1	slovenská
prozaička	prozaička	k1gFnSc1	prozaička
<g/>
,	,	kIx,	,
překladatelka	překladatelka	k1gFnSc1	překladatelka
<g/>
,	,	kIx,	,
publicistka	publicistka	k1gFnSc1	publicistka
a	a	k8xC	a
disidentka	disidentka	k1gFnSc1	disidentka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Pražáková	Pražáková	k1gFnSc1	Pražáková
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Preinhaelterová	Preinhaelterová	k1gFnSc1	Preinhaelterová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
bengalistka	bengalistka	k1gFnSc1	bengalistka
<g/>
,	,	kIx,	,
anglistka	anglistka	k1gFnSc1	anglistka
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
překladatelka	překladatelka	k1gFnSc1	překladatelka
a	a	k8xC	a
vysokoškolská	vysokoškolský	k2eAgFnSc1d1	vysokoškolská
profesorka	profesorka	k1gFnSc1	profesorka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Prošková	Prošková	k1gFnSc1	Prošková
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Purkrábková	Purkrábkový	k2eAgFnSc1d1	Purkrábková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
keramička	keramička	k1gFnSc1	keramička
a	a	k8xC	a
sochařka	sochařka	k1gFnSc1	sochařka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Robinson	Robinson	k1gMnSc1	Robinson
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moravská	moravský	k2eAgFnSc1d1	Moravská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
klavíristka	klavíristka	k1gFnSc1	klavíristka
a	a	k8xC	a
skladatelka	skladatelka	k1gFnSc1	skladatelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Růžičková	Růžičková	k1gFnSc1	Růžičková
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
-	-	kIx~	-
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československá	československý	k2eAgFnSc1d1	Československá
sportovní	sportovní	k2eAgFnSc1d1	sportovní
gymnastka	gymnastka	k1gFnSc1	gymnastka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Rysová	Rysová	k1gFnSc1	Rysová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
fotografka	fotografka	k1gFnSc1	fotografka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Sachsová	Sachsová	k1gFnSc1	Sachsová
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Scharffová	Scharffový	k2eAgFnSc1d1	Scharffová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
televizní	televizní	k2eAgFnSc1d1	televizní
redaktorka	redaktorka	k1gFnSc1	redaktorka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Skalníková	Skalníková	k1gFnSc1	Skalníková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
plážová	plážový	k2eAgFnSc1d1	plážová
volejbalistka	volejbalistka	k1gFnSc1	volejbalistka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Skoumalová	Skoumalová	k1gFnSc1	Skoumalová
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
překladatelka	překladatelka	k1gFnSc1	překladatelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Smekalová	Smekalová	k1gFnSc1	Smekalová
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
-	-	kIx~	-
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezinárodně	mezinárodně	k6eAd1	mezinárodně
proslulá	proslulý	k2eAgFnSc1d1	proslulá
herečka	herečka	k1gFnSc1	herečka
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Smičková-Látalová	Smičková-Látalová	k1gFnSc1	Smičková-Látalová
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
umělecká	umělecký	k2eAgFnSc1d1	umělecká
ředitelka	ředitelka	k1gFnSc1	ředitelka
<g/>
,	,	kIx,	,
choreografka	choreografka	k1gFnSc1	choreografka
a	a	k8xC	a
sólistka	sólistka	k1gFnSc1	sólistka
brněnského	brněnský	k2eAgNnSc2d1	brněnské
Tanečního	taneční	k2eAgNnSc2d1	taneční
divadla	divadlo	k1gNnSc2	divadlo
Mimi	Mimi	k1gFnSc1	Mimi
Fortunae	Fortunae	k1gFnSc1	Fortunae
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Sorrosová	Sorrosová	k1gFnSc1	Sorrosová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
textařka	textařka	k1gFnSc1	textařka
a	a	k8xC	a
překladatelka	překladatelka	k1gFnSc1	překladatelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Soukupová	Soukupová	k1gFnSc1	Soukupová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
modelka	modelka	k1gFnSc1	modelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Svobodová	Svobodová	k1gFnSc1	Svobodová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
modelka	modelka	k1gFnSc1	modelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Sweid	Sweida	k1gFnPc2	Sweida
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
izraelský	izraelský	k2eAgMnSc1d1	izraelský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
poslanec	poslanec	k1gMnSc1	poslanec
Knesetu	Kneset	k1gInSc2	Kneset
za	za	k7c4	za
stranu	strana	k1gFnSc4	strana
Chadaš	Chadaš	k1gInSc1	Chadaš
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Šedivá	šedivý	k2eAgFnSc1d1	šedivá
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
soudkyně	soudkyně	k1gFnSc1	soudkyně
a	a	k8xC	a
bývalá	bývalý	k2eAgFnSc1d1	bývalá
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Ševčíková	Ševčíková	k1gFnSc1	Ševčíková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
dabérka	dabérka	k1gFnSc1	dabérka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Škorpilová	Škorpilová	k1gFnSc1	Škorpilová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
novinářka	novinářka	k1gFnSc1	novinářka
a	a	k8xC	a
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Šromová	Šromová	k1gFnSc1	Šromová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
profesionální	profesionální	k2eAgFnSc1d1	profesionální
tenistka	tenistka	k1gFnSc1	tenistka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Štěpánová	Štěpánová	k1gFnSc1	Štěpánová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Talpová	Talpová	k1gFnSc1	Talpová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Tenglerová	Tenglerová	k1gFnSc1	Tenglerová
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
československá	československý	k2eAgFnSc1d1	Československá
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Tomanová	Tomanová	k1gFnSc1	Tomanová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Ulrychová	Ulrychová	k1gFnSc1	Ulrychová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Vacková	Vacková	k1gFnSc1	Vacková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
středoškolská	středoškolský	k2eAgFnSc1d1	středoškolská
učitelka	učitelka	k1gFnSc1	učitelka
a	a	k8xC	a
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Vagnerová	Vagnerová	k1gFnSc1	Vagnerová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Věrná	věrný	k2eAgFnSc1d1	věrná
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
produkční	produkční	k2eAgFnSc1d1	produkční
<g/>
,	,	kIx,	,
příležitostná	příležitostný	k2eAgFnSc1d1	příležitostná
modelka	modelka	k1gFnSc1	modelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Veselá	veselý	k2eAgFnSc1d1	veselá
<g/>
,	,	kIx,	,
československá	československý	k2eAgFnSc1d1	Československá
krasobruslařka	krasobruslařka	k1gFnSc1	krasobruslařka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Vítová	Vítová	k1gFnSc1	Vítová
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
40	[number]	k4	40
<g/>
.	.	kIx.	.
<g/>
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Vláčilová	Vláčilová	k1gFnSc1	Vláčilová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
sólová	sólový	k2eAgFnSc1d1	sólová
tanečnice	tanečnice	k1gFnSc1	tanečnice
<g/>
,	,	kIx,	,
baletní	baletní	k2eAgFnSc1d1	baletní
mistryně	mistryně	k1gFnSc1	mistryně
<g/>
,	,	kIx,	,
choreografka	choreografka	k1gFnSc1	choreografka
a	a	k8xC	a
pedagožka	pedagožka	k1gFnSc1	pedagožka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Vojtová	Vojtová	k1gFnSc1	Vojtová
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
-	-	kIx~	-
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Volavková	Volavková	k1gFnSc1	Volavková
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
-	-	kIx~	-
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
historička	historička	k1gFnSc1	historička
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
ředitelka	ředitelka	k1gFnSc1	ředitelka
Židovského	židovský	k2eAgNnSc2d1	Židovské
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Vovsová	Vovsová	k1gFnSc1	Vovsová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Vrbová	Vrbová	k1gFnSc1	Vrbová
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
redaktorka	redaktorka	k1gFnSc1	redaktorka
a	a	k8xC	a
překladatelka	překladatelka	k1gFnSc1	překladatelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Whitton	Whitton	k1gInSc1	Whitton
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překladatelka	překladatelka	k1gFnSc1	překladatelka
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
populární	populární	k2eAgFnSc1d1	populární
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Wichterlová	Wichterlová	k1gFnSc1	Wichterlová
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
sochařka	sochařka	k1gFnSc1	sochařka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
textařka	textařka	k1gFnSc1	textařka
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
moderátorka	moderátorka	k1gFnSc1	moderátorka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Zahradníčková	Zahradníčková	k1gFnSc1	Zahradníčková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překladatelka	překladatelka	k1gFnSc1	překladatelka
<g/>
,	,	kIx,	,
redaktorka	redaktorka	k1gFnSc1	redaktorka
<g/>
,	,	kIx,	,
editorka	editorka	k1gFnSc1	editorka
a	a	k8xC	a
učitelka	učitelka	k1gFnSc1	učitelka
jazyků	jazyk	k1gInPc2	jazyk
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Zaňáková	Zaňáková	k1gFnSc1	Zaňáková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Zarevúcká	Zarevúcký	k2eAgFnSc1d1	Zarevúcký
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československá	československý	k2eAgFnSc1d1	Československá
hráčka	hráčka	k1gFnSc1	hráčka
basketbalu	basketbal	k1gInSc2	basketbal
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Zelinová	zelinový	k2eAgFnSc1d1	Zelinová
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovenská	slovenský	k2eAgFnSc1d1	slovenská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
dramatička	dramatička	k1gFnSc1	dramatička
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Zobačová	Zobačová	k1gFnSc1	Zobačová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
pedagožka	pedagožka	k1gFnSc1	pedagožka
<g/>
,	,	kIx,	,
poradce	poradce	k1gMnSc1	poradce
rodinné	rodinný	k2eAgFnSc2d1	rodinná
péče	péče	k1gFnSc2	péče
a	a	k8xC	a
autorka	autorka	k1gFnSc1	autorka
knih	kniha	k1gFnPc2	kniha
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Žantovská	Žantovský	k2eAgFnSc1d1	Žantovská
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
redaktorka	redaktorka	k1gFnSc1	redaktorka
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
především	především	k6eAd1	především
překladatelka	překladatelka	k1gFnSc1	překladatelka
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
němčiny	němčina	k1gFnSc2	němčina
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Želenská	Želenský	k2eAgFnSc1d1	Želenská
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
operetní	operetní	k2eAgFnSc1d1	operetní
subreta	subreta	k1gFnSc1	subreta
</s>
</p>
<p>
<s>
==	==	k?	==
Jiné	jiný	k2eAgFnPc1d1	jiná
Hany	Hana	k1gFnPc1	Hana
==	==	k?	==
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
(	(	kIx(	(
<g/>
Lužické	lužický	k2eAgFnPc1d1	Lužická
hory	hora	k1gFnPc1	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrch	vrch	k1gInSc4	vrch
v	v	k7c6	v
Lužických	lužický	k2eAgFnPc6d1	Lužická
horách	hora	k1gFnPc6	hora
</s>
</p>
<p>
<s>
Hanička	Hanička	k1gFnSc1	Hanička
(	(	kIx(	(
<g/>
dělostřelecká	dělostřelecký	k2eAgFnSc1d1	dělostřelecká
tvrz	tvrz	k1gFnSc1	tvrz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
a	a	k8xC	a
Hana	Hana	k1gFnSc1	Hana
<g/>
,	,	kIx,	,
komiks	komiks	k1gInSc1	komiks
vycházející	vycházející	k2eAgInSc1d1	vycházející
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Reflex	reflex	k1gInSc1	reflex
v	v	k7c6	v
letech	let	k1gInPc6	let
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
bratři	bratr	k1gMnPc1	bratr
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
slovenský	slovenský	k2eAgInSc4d1	slovenský
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
film	film	k1gInSc4	film
s	s	k7c7	s
gay	gay	k1gMnSc1	gay
tematikou	tematika	k1gFnSc7	tematika
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
sestry	sestra	k1gFnPc1	sestra
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnPc1	komedie
scenáristy	scenárista	k1gMnSc2	scenárista
a	a	k8xC	a
režiséra	režisér	k1gMnSc2	režisér
Woodyho	Woody	k1gMnSc2	Woody
Allena	Allen	k1gMnSc2	Allen
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
&	&	k?	&
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
exportní	exportní	k2eAgFnSc1d1	exportní
verze	verze	k1gFnSc1	verze
druhého	druhý	k4xOgNnSc2	druhý
studiového	studiový	k2eAgNnSc2d1	studiové
alba	album	k1gNnSc2	album
Hej	hej	k6eAd1	hej
dámy	dáma	k1gFnPc1	dáma
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
a	a	k8xC	a
páni	pan	k1gMnPc1	pan
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Kytice	kytice	k1gFnSc2	kytice
<g/>
,	,	kIx,	,
kompozice	kompozice	k1gFnSc2	kompozice
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
</s>
</p>
<p>
<s>
Hanička	Hanička	k1gFnSc1	Hanička
Suchá	Suchá	k1gFnSc1	Suchá
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
Ordinace	ordinace	k1gFnSc2	ordinace
v	v	k7c6	v
růžové	růžový	k2eAgFnSc6d1	růžová
zahradě	zahrada	k1gFnSc6	zahrada
2	[number]	k4	2
</s>
</p>
<p>
<s>
hotel	hotel	k1gInSc4	hotel
Hana	Hana	k1gFnSc1	Hana
ve	v	k7c6	v
Špindlerově	Špindlerův	k2eAgInSc6d1	Špindlerův
Mlýně	mlýn	k1gInSc6	mlýn
</s>
</p>
<p>
<s>
hotel	hotel	k1gInSc4	hotel
Hanička	Hanička	k1gFnSc1	Hanička
ve	v	k7c6	v
Špindlerově	Špindlerův	k2eAgInSc6d1	Špindlerův
Mlýně	mlýn	k1gInSc6	mlýn
</s>
</p>
<p>
<s>
Hanka	Hanka	k1gFnSc1	Hanka
z	z	k7c2	z
filmu	film	k1gInSc2	film
Kopretiny	kopretina	k1gFnSc2	kopretina
pro	pro	k7c4	pro
zámeckou	zámecký	k2eAgFnSc4d1	zámecká
paní	paní	k1gFnSc4	paní
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Hana	Hana	k1gFnSc1	Hana
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Hania	Hania	k1gFnSc1	Hania
</s>
</p>
