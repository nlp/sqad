<s>
City	city	k1gNnSc1	city
of	of	k?	of
Lincoln	Lincoln	k1gMnSc1	Lincoln
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgMnSc1d1	hlavní
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Nebraska	Nebrask	k1gInSc2	Nebrask
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
obecní	obecní	k2eAgNnSc1d1	obecní
město	město	k1gNnSc1	město
Lancaster	Lancastra	k1gFnPc2	Lancastra
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
195,2	[number]	k4	195,2
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
193,3	[number]	k4	193,3
km2	km2	k4	km2
tvoří	tvořit	k5eAaImIp3nS	tvořit
pevnina	pevnina	k1gFnSc1	pevnina
a	a	k8xC	a
1,9	[number]	k4	1,9
km2	km2	k4	km2
(	(	kIx(	(
<g/>
0,98	[number]	k4	0,98
%	%	kIx~	%
<g/>
)	)	kIx)	)
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
Nebrasky	Nebraska	k1gFnSc2	Nebraska
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
není	být	k5eNaImIp3nS	být
situováno	situovat	k5eAaBmNgNnS	situovat
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Platte	Platt	k1gInSc5	Platt
nebo	nebo	k8xC	nebo
Missouri	Missouri	k1gNnPc6	Missouri
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vládní	vládní	k2eAgFnSc7d1	vládní
institucí	instituce	k1gFnSc7	instituce
CDC	CDC	kA	CDC
vyhlášené	vyhlášený	k2eAgFnPc1d1	vyhlášená
za	za	k7c4	za
"	"	kIx"	"
<g/>
nejzdravější	zdravý	k2eAgNnSc4d3	nejzdravější
město	město	k1gNnSc4	město
USA	USA	kA	USA
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
258	[number]	k4	258
379	[number]	k4	379
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
byla	být	k5eAaImAgFnS	být
1	[number]	k4	1
324	[number]	k4	324
km2	km2	k4	km2
<g/>
.	.	kIx.	.
86,0	[number]	k4	86,0
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
3,8	[number]	k4	3,8
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,8	[number]	k4	0,8
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
3,8	[number]	k4	3,8
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
2,5	[number]	k4	2,5
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
3,0	[number]	k4	3,0
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
tvořili	tvořit	k5eAaImAgMnP	tvořit
6,3	[number]	k4	6,3
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
<	<	kIx(	<
<g/>
18	[number]	k4	18
let	léto	k1gNnPc2	léto
-	-	kIx~	-
23,0	[number]	k4	23,0
%	%	kIx~	%
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
let	léto	k1gNnPc2	léto
-	-	kIx~	-
16,4	[number]	k4	16,4
%	%	kIx~	%
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
let	léto	k1gNnPc2	léto
-	-	kIx~	-
30,7	[number]	k4	30,7
%	%	kIx~	%
45	[number]	k4	45
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
-	-	kIx~	-
19,5	[number]	k4	19,5
%	%	kIx~	%
>	>	kIx)	>
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
-	-	kIx~	-
10,4	[number]	k4	10,4
%	%	kIx~	%
průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
31	[number]	k4	31
let	let	k1gInSc1	let
Dick	Dick	k1gInSc4	Dick
Cheney	Chenea	k1gFnSc2	Chenea
(	(	kIx(	(
<g/>
*	*	kIx~	*
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obchodník	obchodník	k1gMnSc1	obchodník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
46	[number]	k4	46
<g/>
.	.	kIx.	.
viceprezident	viceprezident	k1gMnSc1	viceprezident
USA	USA	kA	USA
Janine	Janin	k1gInSc5	Janin
Turnerová	Turnerová	k1gFnSc1	Turnerová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
,	,	kIx,	,
Nebraska	Nebraska	k1gFnSc1	Nebraska
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lincoln	Lincoln	k1gMnSc1	Lincoln
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
