<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
plavecká	plavecký	k2eAgFnSc1d1
federace	federace	k1gFnSc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
plavecká	plavecký	k2eAgFnSc1d1
federace	federace	k1gFnSc1
Zkratka	zkratka	k1gFnSc1
</s>
<s>
FINA	Fina	k1gFnSc1
Motto	motto	k1gNnSc1
</s>
<s>
Voda	voda	k1gFnSc1
je	být	k5eAaImIp3nS
náš	náš	k3xOp1gInSc4
svět	svět	k1gInSc4
Vznik	vznik	k1gInSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1908	#num#	k4
Londýn	Londýn	k1gInSc1
Typ	typ	k1gInSc4
</s>
<s>
sportovní	sportovní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
Účel	účel	k1gInSc1
</s>
<s>
plavání	plavání	k1gNnSc1
<g/>
,	,	kIx,
skoky	skok	k1gInPc1
do	do	k7c2
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
synchronizované	synchronizovaný	k2eAgNnSc1d1
plavání	plavání	k1gNnSc1
<g/>
,	,	kIx,
vodní	vodní	k2eAgNnSc1d1
pólo	pólo	k1gNnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
Místo	místo	k7c2
</s>
<s>
Lausanne	Lausanne	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
46	#num#	k4
<g/>
°	°	k?
<g/>
31	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
6	#num#	k4
<g/>
°	°	k?
<g/>
38	#num#	k4
<g/>
′	′	k?
<g/>
33	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Působnost	působnost	k1gFnSc1
</s>
<s>
celosvětová	celosvětový	k2eAgFnSc1d1
Členové	člen	k1gMnPc1
</s>
<s>
202	#num#	k4
Prezident	prezident	k1gMnSc1
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Julio	Julio	k1gMnSc1
Maglione	Maglion	k1gInSc5
Uruguay	Uruguay	k1gFnSc4
Uruguay	Uruguay	k1gFnSc1
Přidružení	přidružení	k1gNnSc2
</s>
<s>
MOV	MOV	kA
<g/>
,	,	kIx,
ASOIF	ASOIF	kA
<g/>
,	,	kIx,
SportAccord	SportAccord	k1gInSc1
<g/>
,	,	kIx,
LEN	len	k1gInSc1
<g/>
,	,	kIx,
ČSPS	ČSPS	kA
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.fina.org	www.fina.org	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
plavecká	plavecký	k2eAgFnSc1d1
federace	federace	k1gFnSc1
(	(	kIx(
<g/>
FINA	FINA	kA
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
Fédération	Fédération	k1gInSc1
Internationale	Internationale	k1gFnSc2
de	de	k?
Natation	Natation	k1gInSc1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
'	'	kIx"
<g/>
International	International	k1gFnSc1
Swimming	Swimming	k1gInSc1
Federation	Federation	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
sportovní	sportovní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
sdružující	sdružující	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
svazy	svaz	k1gInPc1
plaveckých	plavecký	k2eAgInPc2d1
sportů	sport	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastřešuje	zastřešovat	k5eAaImIp3nS
celosvětové	celosvětový	k2eAgFnPc4d1
aktivity	aktivita	k1gFnPc4
bazénového	bazénový	k2eAgNnSc2d1
plavání	plavání	k1gNnSc2
<g/>
,	,	kIx,
dálkového	dálkový	k2eAgNnSc2d1
plavání	plavání	k1gNnSc2
<g/>
,	,	kIx,
skoků	skok	k1gInPc2
do	do	k7c2
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
synchronizovaného	synchronizovaný	k2eAgNnSc2d1
plavání	plavání	k1gNnSc2
a	a	k8xC
vodního	vodní	k2eAgNnSc2d1
póla	pólo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Federace	federace	k1gFnSc1
má	mít	k5eAaImIp3nS
sídlo	sídlo	k1gNnSc4
ve	v	k7c6
švýcarském	švýcarský	k2eAgNnSc6d1
Lausanne	Lausanne	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Češi	Čech	k1gMnPc1
v	v	k7c4
Mezinárodní	mezinárodní	k2eAgNnPc4d1
plavecká	plavecký	k2eAgNnPc4d1
federaci	federace	k1gFnSc3
(	(	kIx(
<g/>
FINA	Fina	k1gFnSc1
<g/>
)	)	kIx)
-	-	kIx~
Ing.	ing.	kA
Ladislav	Ladislav	k1gMnSc1
Hauptmann	Hauptmann	k1gMnSc1
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
uváděn	uvádět	k5eAaImNgInS
s	s	k7c7
jedním	jeden	k4xCgMnSc7
n	n	k0
jako	jako	k8xC,k8xS
Hauptman	Hauptman	k1gMnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
členem	člen	k1gInSc7
FINA	Fin	k1gMnSc2
mezi	mezi	k7c7
roky	rok	k1gInPc7
1936	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
byl	být	k5eAaImAgInS
také	také	k9
mezi	mezi	k7c7
roky	rok	k1gInPc7
1946	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
předsedou	předseda	k1gMnSc7
(	(	kIx(
<g/>
sekretářem	sekretář	k1gMnSc7
<g/>
)	)	kIx)
technické	technický	k2eAgFnSc2d1
komise	komise	k1gFnSc2
skoků	skok	k1gInPc2
do	do	k7c2
vody	voda	k1gFnSc2
a	a	k8xC
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1948-1952	1948-1952	k4
byl	být	k5eAaImAgInS
dokonce	dokonce	k9
viceprezidentem	viceprezident	k1gMnSc7
FINA	Fin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
byl	být	k5eAaImAgInS
jmenován	jmenovat	k5eAaBmNgInS,k5eAaImNgInS
pro	pro	k7c4
velké	velký	k2eAgFnPc4d1
zásluhy	zásluha	k1gFnPc4
čestným	čestný	k2eAgMnSc7d1
členem	člen	k1gInSc7
Mezinárodní	mezinárodní	k2eAgFnSc3d1
plavecké	plavecký	k2eAgFnSc3d1
federaci	federace	k1gFnSc3
(	(	kIx(
<g/>
FINA	Fina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
https://www.sinslavy.czechswimming.cz/subdom/sinslavy/index.php/treneri-funcionari/10-treneri-funcionari/59-ladislav-hauptman	https://www.sinslavy.czechswimming.cz/subdom/sinslavy/index.php/treneri-funcionari/10-treneri-funcionari/59-ladislav-hauptman	k1gMnSc1
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
FINA	Fina	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
19	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1908	#num#	k4
v	v	k7c6
londýnském	londýnský	k2eAgInSc6d1
hotelu	hotel	k1gInSc6
Manchester	Manchester	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
na	na	k7c6
konci	konec	k1gInSc6
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
1908	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakládajícími	zakládající	k2eAgInPc7d1
členy	člen	k1gInPc7
byly	být	k5eAaImAgFnP
svazy	svaz	k1gInPc4
Belgie	Belgie	k1gFnSc2
<g/>
,	,	kIx,
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
Dánska	Dánsko	k1gNnSc2
<g/>
,	,	kIx,
Finska	Finsko	k1gNnSc2
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
Maďarska	Maďarsko	k1gNnSc2
a	a	k8xC
Švédska	Švédsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československo	Československo	k1gNnSc1
zastoupené	zastoupený	k2eAgNnSc1d1
Československým	československý	k2eAgInSc7d1
amatérským	amatérský	k2eAgInSc7d1
plaveckým	plavecký	k2eAgInSc7d1
svazem	svaz	k1gInSc7
bylo	být	k5eAaImAgNnS
do	do	k7c2
FINA	Fin	k1gMnSc2
přijato	přijmout	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
je	být	k5eAaImIp3nS
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
zastoupena	zastoupit	k5eAaPmNgFnS
Českým	český	k2eAgInSc7d1
svazem	svaz	k1gInSc7
plaveckých	plavecký	k2eAgInPc2d1
sportů	sport	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Růst	růst	k1gInSc1
počtu	počet	k1gInSc2
členů	člen	k1gMnPc2
</s>
<s>
1908	#num#	k4
<g/>
:	:	kIx,
8	#num#	k4
</s>
<s>
1928	#num#	k4
<g/>
:	:	kIx,
38	#num#	k4
</s>
<s>
1958	#num#	k4
<g/>
:	:	kIx,
75	#num#	k4
</s>
<s>
1978	#num#	k4
<g/>
:	:	kIx,
106	#num#	k4
</s>
<s>
1988	#num#	k4
<g/>
:	:	kIx,
109	#num#	k4
</s>
<s>
2000	#num#	k4
<g/>
:	:	kIx,
174	#num#	k4
</s>
<s>
2010	#num#	k4
<g/>
:	:	kIx,
202	#num#	k4
</s>
<s>
Seznam	seznam	k1gInSc1
prezidentů	prezident	k1gMnPc2
FINA	Fin	k1gMnSc2
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
byl	být	k5eAaImAgInS
viceprezidentem	viceprezident	k1gMnSc7
Ing.	ing.	kA
Ladislav	Ladislav	k1gMnSc1
Hauptmann	Hauptmann	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
FINA	Fin	k1gMnSc2
mezi	mezi	k7c4
roky	rok	k1gInPc4
1936	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
čestným	čestný	k2eAgMnSc7d1
členem	člen	k1gMnSc7
Ladislav	Ladislav	k1gMnSc1
Hauptmann	Hauptmann	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prezidenti	prezident	k1gMnPc1
FINA	Fin	k1gMnSc2
</s>
<s>
JménoZeměObdobí	JménoZeměObdobit	k5eAaPmIp3nS
</s>
<s>
George	Georgat	k5eAaPmIp3nS
Hearn	Hearn	k1gInSc1
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
<g/>
1908	#num#	k4
<g/>
–	–	k?
<g/>
1924	#num#	k4
</s>
<s>
Erik	Erik	k1gMnSc1
BergvallŠvédsko	BergvallŠvédsko	k1gNnSc4
Švédsko	Švédsko	k1gNnSc1
<g/>
1924	#num#	k4
<g/>
–	–	k?
<g/>
1928	#num#	k4
</s>
<s>
Émile-Georges	Émile-Georges	k1gInSc1
DrignyFrancie	DrignyFrancie	k1gFnSc2
Francie	Francie	k1gFnSc2
<g/>
1928	#num#	k4
<g/>
–	–	k?
<g/>
1932	#num#	k4
</s>
<s>
Walther	Walthra	k1gFnPc2
BinnerNěmecko	BinnerNěmecko	k1gNnSc4
Německo	Německo	k1gNnSc1
<g/>
1932	#num#	k4
<g/>
–	–	k?
<g/>
1936	#num#	k4
</s>
<s>
Harold	Harold	k1gMnSc1
Fern	Fern	k1gMnSc1
Velká	velká	k1gFnSc1
Británie	Británie	k1gFnSc1
<g/>
1936	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
</s>
<s>
Rene	Rene	k1gFnSc1
de	de	k?
RaeveBelgie	RaeveBelgie	k1gFnSc1
Belgie	Belgie	k1gFnSc1
<g/>
1948	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
</s>
<s>
M.	M.	kA
<g/>
L.	L.	kA
NegriArgentina	NegriArgentina	k1gFnSc1
Argentina	Argentina	k1gFnSc1
<g/>
1952	#num#	k4
<g/>
–	–	k?
<g/>
1956	#num#	k4
</s>
<s>
Jan	Jan	k1gMnSc1
de	de	k?
VriesNizozemsko	VriesNizozemsko	k1gNnSc4
Nizozemsko	Nizozemsko	k1gNnSc1
<g/>
1956	#num#	k4
<g/>
–	–	k?
<g/>
1960	#num#	k4
</s>
<s>
Max	Max	k1gMnSc1
RitterNěmecko	RitterNěmecko	k1gNnSc4
Německo	Německo	k1gNnSc1
<g/>
1960	#num#	k4
<g/>
–	–	k?
<g/>
1964	#num#	k4
</s>
<s>
William	William	k6eAd1
Berge	Berge	k1gFnSc1
PhillipsAustrálie	PhillipsAustrálie	k1gFnSc1
Austrálie	Austrálie	k1gFnSc1
<g/>
1964	#num#	k4
<g/>
–	–	k?
<g/>
1968	#num#	k4
</s>
<s>
Javier	Javier	k1gMnSc1
Ostos	Ostos	k1gMnSc1
MoraMexiko	MoraMexika	k1gFnSc5
Mexiko	Mexiko	k1gNnSc1
<g/>
1968	#num#	k4
<g/>
–	–	k?
<g/>
1972	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Harold	Harold	k1gInSc1
Henning	Henning	k1gInSc1
USA	USA	kA
<g/>
1972	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
</s>
<s>
Javier	Javier	k1gMnSc1
Ostos	Ostos	k1gMnSc1
MoraMexiko	MoraMexika	k1gFnSc5
Mexiko	Mexiko	k1gNnSc1
<g/>
1976	#num#	k4
<g/>
–	–	k?
<g/>
1980	#num#	k4
</s>
<s>
Ante	Ant	k1gMnSc5
LambasaJugoslávie	LambasaJugoslávius	k1gMnSc5
Jugoslávie	Jugoslávie	k1gFnSc1
<g/>
1980	#num#	k4
<g/>
–	–	k?
<g/>
1984	#num#	k4
</s>
<s>
Robert	Robert	k1gMnSc1
Helmick	Helmick	k1gMnSc1
USA	USA	kA
<g/>
1984	#num#	k4
<g/>
–	–	k?
<g/>
1988	#num#	k4
</s>
<s>
Mustapha	Mustapha	k1gMnSc1
LarfaouiAlžírsko	LarfaouiAlžírsko	k1gNnSc4
Alžírsko	Alžírsko	k1gNnSc1
<g/>
1988	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Julio	Julio	k1gMnSc1
MaglioneUruguay	MaglioneUruguaa	k1gFnSc2
Uruguay	Uruguay	k1gFnSc1
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
(	(	kIx(
<g/>
mandát	mandát	k1gInSc1
do	do	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Členské	členský	k2eAgInPc1d1
svazy	svaz	k1gInPc1
a	a	k8xC
kontinentální	kontinentální	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2010	#num#	k4
byla	být	k5eAaImAgFnS
přijata	přijat	k2eAgFnSc1d1
Tonga	Tonga	k1gFnSc1
jako	jako	k9
202	#num#	k4
<g/>
.	.	kIx.
členská	členský	k2eAgFnSc1d1
země	země	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
jsou	být	k5eAaImIp3nP
děleni	dělit	k5eAaImNgMnP
podle	podle	k7c2
geografické	geografický	k2eAgFnSc2d1
příslušnosti	příslušnost	k1gFnSc2
(	(	kIx(
<g/>
s	s	k7c7
jistými	jistý	k2eAgFnPc7d1
výjimkami	výjimka	k1gFnPc7
<g/>
)	)	kIx)
do	do	k7c2
pěti	pět	k4xCc2
kontinentálních	kontinentální	k2eAgFnPc2d1
asociací	asociace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Kontinent	kontinent	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
asociace	asociace	k1gFnSc2
</s>
<s>
Zkratka	zkratka	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
založení	založení	k1gNnSc2
</s>
<s>
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Počet	počet	k1gInSc1
členů	člen	k1gMnPc2
</s>
<s>
Evropa	Evropa	k1gFnSc1
</s>
<s>
Ligue	Liguus	k1gMnSc5
Européenne	Européenn	k1gMnSc5
de	de	k?
Natation	Natation	k1gInSc1
</s>
<s>
LEN	len	k1gInSc1
</s>
<s>
1927	#num#	k4
</s>
<s>
Lucemburk	Lucemburk	k1gMnSc1
</s>
<s>
51	#num#	k4
</s>
<s>
Asie	Asie	k1gFnSc1
</s>
<s>
Asian	Asian	k1gMnSc1
Amateur	Amateur	k1gMnSc1
Swimming	Swimming	k1gInSc4
Federation	Federation	k1gInSc4
</s>
<s>
AASF	AASF	kA
</s>
<s>
1978	#num#	k4
</s>
<s>
Maskat	Maskat	k1gInSc1
<g/>
,	,	kIx,
Omán	Omán	k1gInSc1
</s>
<s>
43	#num#	k4
</s>
<s>
Afrika	Afrika	k1gFnSc1
</s>
<s>
Confédération	Confédération	k1gInSc1
Africaine	Africain	k1gInSc5
de	de	k?
Natation	Natation	k1gInSc4
</s>
<s>
CANA	CANA	kA
</s>
<s>
1978	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
51	#num#	k4
</s>
<s>
Amerika	Amerika	k1gFnSc1
</s>
<s>
Amateur	Amateur	k1gMnSc1
Swimming	Swimming	k1gInSc4
Union	union	k1gInSc1
of	of	k?
the	the	k?
Americas	Americas	k1gInSc1
</s>
<s>
ASUA	ASUA	kA
</s>
<s>
1948	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
41	#num#	k4
</s>
<s>
Oceánie	Oceánie	k1gFnSc1
</s>
<s>
Oceania	Oceanium	k1gNnPc4
Swimming	Swimming	k1gInSc1
Association	Association	k1gInSc4
</s>
<s>
OSA	osa	k1gFnSc1
</s>
<s>
1991	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
16	#num#	k4
</s>
<s>
Aktivity	aktivita	k1gFnPc1
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1
závody	závod	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
se	se	k3xPyFc4
konají	konat	k5eAaImIp3nP
pod	pod	k7c7
patronací	patronace	k1gFnSc7
FINA	Fin	k1gMnSc2
jsou	být	k5eAaImIp3nP
mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
zavedeno	zavést	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
a	a	k8xC
mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
v	v	k7c6
krátkém	krátký	k2eAgInSc6d1
bazénu	bazén	k1gInSc6
pořádané	pořádaný	k2eAgFnSc2d1
od	od	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
ve	v	k7c6
dvouletých	dvouletý	k2eAgInPc6d1
intervalech	interval	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgInPc7d1
závody	závod	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
FINA	Fina	k1gFnSc1
pořádá	pořádat	k5eAaImIp3nS
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
Světový	světový	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
plavecké	plavecký	k2eAgInPc4d1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
či	či	k8xC
mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zástupci	zástupce	k1gMnPc1
členských	členský	k2eAgInPc2d1
svazů	svaz	k1gInPc2
se	se	k3xPyFc4
setkávají	setkávat	k5eAaImIp3nP
na	na	k7c6
pravidelných	pravidelný	k2eAgInPc6d1
kongresech	kongres	k1gInPc6
(	(	kIx(
<g/>
ve	v	k7c6
dvouletých	dvouletý	k2eAgInPc6d1
cyklech	cyklus	k1gInPc6
obyčejně	obyčejně	k6eAd1
během	během	k7c2
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
k	k	k7c3
projednání	projednání	k1gNnSc3
některých	některý	k3yIgInPc2
specifických	specifický	k2eAgInPc2d1
problémů	problém	k1gInPc2
jsou	být	k5eAaImIp3nP
svolávány	svolávat	k5eAaImNgInP
i	i	k9
kongresy	kongres	k1gInPc4
mimořádné	mimořádný	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
FINA	Fina	k1gFnSc1
pracuje	pracovat	k5eAaImIp3nS
řada	řada	k1gFnSc1
komisí	komise	k1gFnPc2
a	a	k8xC
výborů	výbor	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
se	se	k3xPyFc4
zabývají	zabývat	k5eAaImIp3nP
technickými	technický	k2eAgInPc7d1
aspekty	aspekt	k1gInPc7
plaveckých	plavecký	k2eAgInPc2d1
sportů	sport	k1gInPc2
<g/>
,	,	kIx,
bojem	boj	k1gInSc7
proti	proti	k7c3
dopingu	doping	k1gInSc3
<g/>
,	,	kIx,
rozvojem	rozvoj	k1gInSc7
plavání	plavání	k1gNnSc2
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
FINA	Fin	k1gMnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Ladislav	Ladislav	k1gMnSc1
Hauptman	Hauptman	k1gMnSc1
<g/>
.	.	kIx.
www.sinslavy.czechswimming.cz	www.sinslavy.czechswimming.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
plaveckých	plavecký	k2eAgInPc2d1
sportů	sport	k1gInPc2
(	(	kIx(
<g/>
ČSPS	ČSPS	kA
<g/>
)	)	kIx)
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
plavecká	plavecký	k2eAgFnSc1d1
liga	liga	k1gFnSc1
(	(	kIx(
<g/>
LEN	len	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Mezinárodní	mezinárodní	k2eAgFnSc1d1
plavecká	plavecký	k2eAgFnSc1d1
federace	federace	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
FINA	Fina	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
plaveckých	plavecký	k2eAgInPc2d1
sportů	sport	k1gInPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mezinárodní	mezinárodní	k2eAgFnSc1d1
sportovní	sportovní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
ASOIF	ASOIF	kA
(	(	kIx(
<g/>
28	#num#	k4
<g/>
)	)	kIx)
<g/>
Federace	federace	k1gFnSc1
letních	letní	k2eAgFnPc2d1
olympiád	olympiáda	k1gFnPc2
</s>
<s>
IAAF	IAAF	kA
(	(	kIx(
<g/>
atletika	atletika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
BWF	BWF	kA
(	(	kIx(
<g/>
badminton	badminton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIBA	FIBA	kA
(	(	kIx(
<g/>
basketbal	basketbal	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
AIBA	AIBA	kA
(	(	kIx(
<g/>
box	box	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
UCI	UCI	kA
(	(	kIx(
<g/>
cyklistika	cyklistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
FIFA	FIFA	kA
(	(	kIx(
<g/>
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IGF	IGF	kA
(	(	kIx(
<g/>
golf	golf	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
FIG	FIG	kA
(	(	kIx(
<g/>
gymnastika	gymnastik	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
IHF	IHF	kA
(	(	kIx(
<g/>
házená	házená	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ISAF	ISAF	kA
(	(	kIx(
<g/>
jachting	jachting	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FEI	FEI	kA
(	(	kIx(
<g/>
jezdectví	jezdectví	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
IJF	IJF	kA
(	(	kIx(
<g/>
judo	judo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICF	ICF	kA
(	(	kIx(
<g/>
kanoistika	kanoistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
WA	WA	kA
(	(	kIx(
<g/>
lukostřelba	lukostřelba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
UIPM	UIPM	kA
(	(	kIx(
<g/>
moderní	moderní	k2eAgInSc4d1
pětiboj	pětiboj	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
FIH	FIH	kA
(	(	kIx(
<g/>
pozemní	pozemní	k2eAgInSc4d1
hokej	hokej	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
WR	WR	kA
(	(	kIx(
<g/>
ragby	ragby	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ISSF	ISSF	kA
(	(	kIx(
<g/>
sportovní	sportovní	k2eAgFnSc1d1
střelba	střelba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ITTF	ITTF	kA
(	(	kIx(
<g/>
stolní	stolní	k2eAgInSc1d1
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIE	FIE	kA
(	(	kIx(
<g/>
šerm	šerm	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WTF	WTF	kA
(	(	kIx(
<g/>
taekwondo	taekwondo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ITF	ITF	kA
(	(	kIx(
<g/>
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ITU	ITU	kA
(	(	kIx(
<g/>
triatlon	triatlon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FISA	FISA	kA
(	(	kIx(
<g/>
veslování	veslování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
FINA	Fina	k1gFnSc1
(	(	kIx(
<g/>
vodní	vodní	k2eAgInPc1d1
sporty	sport	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
FIVB	FIVB	kA
(	(	kIx(
<g/>
volejbal	volejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IWF	IWF	kA
(	(	kIx(
<g/>
vzpírání	vzpírání	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
UWW	UWW	kA
(	(	kIx(
<g/>
zápas	zápas	k1gInSc1
<g/>
)	)	kIx)
AIOWF	AIOWF	kA
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
Federace	federace	k1gFnSc1
zimních	zimní	k2eAgFnPc2d1
olympiád	olympiáda	k1gFnPc2
</s>
<s>
IBU	IBU	kA
(	(	kIx(
<g/>
biatlon	biatlon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IBSF	IBSF	kA
(	(	kIx(
<g/>
boby	bob	k1gInPc1
a	a	k8xC
skeleton	skeleton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISU	ISU	kA
(	(	kIx(
<g/>
bruslení	bruslení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WCF	WCF	kA
(	(	kIx(
<g/>
curling	curling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IIHF	IIHF	kA
(	(	kIx(
<g/>
lední	lední	k2eAgInSc4d1
hokej	hokej	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
FIS	FIS	kA
(	(	kIx(
<g/>
lyžařské	lyžařský	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
FIL	FIL	kA
(	(	kIx(
<g/>
sáňkařský	sáňkařský	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
ARISF	ARISF	kA
(	(	kIx(
<g/>
37	#num#	k4
<g/>
)	)	kIx)
<g/>
Další	další	k2eAgFnSc1d1
federace	federace	k1gFnSc1
uznané	uznaný	k2eAgFnSc2d1
MOV	MOV	kA
</s>
<s>
IFAF	IFAF	kA
(	(	kIx(
<g/>
americký	americký	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIA	FIA	kA
(	(	kIx(
<g/>
automobilový	automobilový	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIB	FIB	kA
(	(	kIx(
<g/>
bandy	bandy	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WBSC	WBSC	kA
(	(	kIx(
<g/>
baseball	baseball	k1gInSc1
a	a	k8xC
softball	softball	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WB	WB	kA
(	(	kIx(
<g/>
bowling	bowling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WBF	WBF	kA
(	(	kIx(
<g/>
bridž	bridž	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFF	IFF	kA
(	(	kIx(
<g/>
florbal	florbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
UIAA	UIAA	kA
(	(	kIx(
<g/>
horolezectví	horolezectví	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICU	ICU	kA
(	(	kIx(
<g/>
cheerleading	cheerleading	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WKF	WKF	kA
(	(	kIx(
<g/>
karate	karate	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICC	ICC	kA
(	(	kIx(
<g/>
kriket	kriket	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIRS	FIRS	kA
(	(	kIx(
<g/>
kolečkové	kolečkový	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
IKF	IKF	kA
(	(	kIx(
<g/>
korfbal	korfbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
CMSB	CMSB	kA
(	(	kIx(
<g/>
koulové	koulový	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
WCBS	WCBS	kA
(	(	kIx(
<g/>
kulečník	kulečník	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FAI	FAI	kA
(	(	kIx(
<g/>
letecký	letecký	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIM	FIM	kA
(	(	kIx(
<g/>
motocyklový	motocyklový	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFMA	IFMA	kA
(	(	kIx(
<g/>
muay	mua	k2eAgFnPc1d1
thai	tha	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
IFNA	IFNA	kA
(	(	kIx(
<g/>
netball	netball	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
IOF	IOF	kA
(	(	kIx(
<g/>
orientační	orientační	k2eAgInSc1d1
běh	běh	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIPV	FIPV	kA
(	(	kIx(
<g/>
pelota	pelota	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CMAS	CMAS	kA
(	(	kIx(
<g/>
podvodní	podvodní	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
FIP	FIP	kA
(	(	kIx(
<g/>
pólo	pólo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
TWIF	TWIF	kA
(	(	kIx(
<g/>
přetahování	přetahování	k1gNnPc2
lanem	lano	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
IRF	IRF	kA
(	(	kIx(
<g/>
raketbal	raketbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISMF	ISMF	kA
(	(	kIx(
<g/>
skialpinismus	skialpinismus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFSC	IFSC	kA
(	(	kIx(
<g/>
sportovní	sportovní	k2eAgNnSc1d1
lezení	lezení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WSF	WSF	kA
(	(	kIx(
<g/>
squash	squash	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFS	IFS	kA
(	(	kIx(
<g/>
sumo	suma	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
ISA	ISA	kA
(	(	kIx(
<g/>
surfing	surfing	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIDE	FIDE	kA
(	(	kIx(
<g/>
šachy	šach	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
WDSF	WDSF	kA
(	(	kIx(
<g/>
taneční	taneční	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WFDF	WFDF	kA
(	(	kIx(
<g/>
ultimate	ultimat	k1gInSc5
frisbee	frisbee	k1gNnPc1
<g/>
)	)	kIx)
</s>
<s>
IWWF	IWWF	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgNnSc1d1
lyžování	lyžování	k1gNnSc1
a	a	k8xC
wakeboarding	wakeboarding	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
UIM	UIM	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgInSc1d1
motorismus	motorismus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IWUF	IWUF	kA
(	(	kIx(
<g/>
wu-šu	wu-sat	k5eAaPmIp1nS
<g/>
)	)	kIx)
</s>
<s>
ILSF	ILSF	kA
(	(	kIx(
<g/>
záchranářský	záchranářský	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
Ostatní	ostatní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
ve	v	k7c6
SportAccordu	SportAccordo	k1gNnSc6
(	(	kIx(
<g/>
21	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
IAF	IAF	kA
(	(	kIx(
<g/>
aikido	aikida	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
FMJD	FMJD	kA
(	(	kIx(
<g/>
dáma	dáma	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
IDBF	IDBF	kA
(	(	kIx(
<g/>
dračí	dračí	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
JJIF	JJIF	kA
(	(	kIx(
<g/>
džú-džucu	džú-džucu	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
IFA	IFA	kA
(	(	kIx(
<g/>
faustball	faustball	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
IGF	IGF	kA
(	(	kIx(
<g/>
go	go	k?
<g/>
)	)	kIx)
</s>
<s>
IFI	IFI	kA
(	(	kIx(
<g/>
ice	ice	k?
stock	stock	k1gInSc1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIK	fik	k0
(	(	kIx(
<g/>
kendó	kendó	k?
<g/>
)	)	kIx)
</s>
<s>
WAKO	WAKO	kA
(	(	kIx(
<g/>
kickboxing	kickboxing	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFBB	IFBB	kA
(	(	kIx(
<g/>
kulturistika	kulturistika	k1gFnSc1
a	a	k8xC
fitness	fitness	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIL	FIL	kA
(	(	kIx(
<g/>
lakros	lakros	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WMF	WMF	kA
(	(	kIx(
<g/>
minigolf	minigolf	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISFF	ISFF	kA
(	(	kIx(
<g/>
psí	psí	k2eAgNnSc1d1
spřežení	spřežení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICSF	ICSF	kA
(	(	kIx(
<g/>
rybolovná	rybolovný	k2eAgFnSc1d1
technika	technika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
FIAS	FIAS	kA
(	(	kIx(
<g/>
sambo	samba	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
FISav	FISav	k1gInSc4
(	(	kIx(
<g/>
savate	savat	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
ISTAF	ISTAF	kA
(	(	kIx(
<g/>
sepak	sepak	k1gMnSc1
takraw	takraw	k?
<g/>
)	)	kIx)
</s>
<s>
IPF	IPF	kA
(	(	kIx(
<g/>
silový	silový	k2eAgInSc1d1
trojboj	trojboj	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISTF	ISTF	kA
(	(	kIx(
<g/>
soft	soft	k?
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
CIPS	CIPS	kA
(	(	kIx(
<g/>
sportovní	sportovní	k2eAgInSc1d1
rybolov	rybolov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WDF	WDF	kA
(	(	kIx(
<g/>
šipky	šipka	k1gFnPc1
<g/>
)	)	kIx)
Jiné	jiný	k2eAgFnPc1d1
federace	federace	k1gFnPc1
(	(	kIx(
<g/>
19	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
WAF	WAF	kA
(	(	kIx(
<g/>
armwrestling	armwrestling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ARI	ARI	kA
(	(	kIx(
<g/>
Australský	australský	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IBA	iba	k6eAd1
(	(	kIx(
<g/>
bodyboarding	bodyboarding	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
PBA	PBA	kA
(	(	kIx(
<g/>
bowls	bowls	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFBA	IFBA	kA
(	(	kIx(
<g/>
broomball	broomball	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
IKAEF	IKAEF	kA
(	(	kIx(
<g/>
eskrima	eskrim	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
WFA	WFA	kA
(	(	kIx(
<g/>
footbag	footbag	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
ISBHF	ISBHF	kA
(	(	kIx(
<g/>
hokejbal	hokejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WCF	WCF	kA
(	(	kIx(
<g/>
kroket	kroket	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IKF	IKF	kA
(	(	kIx(
<g/>
kabaddi	kabaddit	k5eAaPmRp2nS
<g/>
)	)	kIx)
</s>
<s>
IMMAF	IMMAF	kA
(	(	kIx(
<g/>
MMA	MMA	kA
<g/>
)	)	kIx)
</s>
<s>
IFP	IFP	kA
(	(	kIx(
<g/>
poker	poker	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IPSC	IPSC	kA
(	(	kIx(
<g/>
practical	practicat	k5eAaPmAgInS
shooting	shooting	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IQA	IQA	kA
(	(	kIx(
<g/>
mudlovský	mudlovský	k2eAgInSc1d1
famfrpál	famfrpál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFMAR	IFMAR	kA
(	(	kIx(
<g/>
závody	závod	k1gInPc1
automobilových	automobilový	k2eAgInPc2d1
modelů	model	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
FIFTA	FIFTA	kA
(	(	kIx(
<g/>
nohejbal	nohejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IRF	IRF	kA
(	(	kIx(
<g/>
rogaining	rogaining	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
RLIF	RLIF	kA
(	(	kIx(
<g/>
třináctkové	třináctkový	k2eAgNnSc1d1
ragby	ragby	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WSSA	WSSA	kA
(	(	kIx(
<g/>
sport	sport	k1gInSc1
stacking	stacking	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ITPF	ITPF	kA
(	(	kIx(
<g/>
Tent	tent	k1gInSc1
pegging	pegging	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIT	fit	k2eAgInSc1d1
(	(	kIx(
<g/>
touch	touch	k1gInSc1
rugby	rugby	k1gNnSc1
<g/>
)	)	kIx)
Mezinárodní	mezinárodní	k2eAgInSc1d1
olympijský	olympijský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
•	•	k?
Mezinárodní	mezinárodní	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
Světových	světový	k2eAgFnPc2d1
her	hra	k1gFnPc2
•	•	k?
SportAccord	SportAccord	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2011626655	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1456	#num#	k4
7014	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2006035746	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
130554130	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2006035746	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
|	|	kIx~
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
