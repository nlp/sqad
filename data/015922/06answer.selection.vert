<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
plavecká	plavecký	k2eAgFnSc1d1
federace	federace	k1gFnSc1
(	(	kIx(
<g/>
FINA	FINA	kA
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
Fédération	Fédération	k1gInSc1
Internationale	Internationale	k1gFnSc2
de	de	k?
Natation	Natation	k1gInSc1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
'	'	kIx"
<g/>
International	International	k1gFnSc1
Swimming	Swimming	k1gInSc1
Federation	Federation	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
sportovní	sportovní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
sdružující	sdružující	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
svazy	svaz	k1gInPc1
plaveckých	plavecký	k2eAgInPc2d1
sportů	sport	k1gInPc2
<g/>
.	.	kIx.
</s>