<s>
Smíšená	smíšený	k2eAgNnPc1d1
bojová	bojový	k2eAgNnPc1d1
umění	umění	k1gNnPc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Mixed	Mixed	k1gMnSc1
Martial	Martial	k1gMnSc1
Arts	Arts	k1gInSc4
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
MMA	MMA	kA
<g/>
,	,	kIx,
portugalsky	portugalsky	k6eAd1
Artes	Artes	k1gMnSc1
marciais	marciais	k1gFnPc2
mistas	mistas	k1gMnSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
AMM	AMM	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
plnokontaktní	plnokontaktní	k2eAgInSc1d1
bojový	bojový	k2eAgInSc1d1
sport	sport	k1gInSc1
bez	bez	k7c2
využití	využití	k1gNnSc2
zbraní	zbraň	k1gFnPc2
<g/>
,	,	kIx,
umožňující	umožňující	k2eAgInPc4d1
údery	úder	k1gInPc4
i	i	k8xC
chvaty	chvat	k1gInPc4
<g/>
,	,	kIx,
boj	boj	k1gInSc4
ve	v	k7c6
stoje	stoje	k6eAd1
i	i	k8xC
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
spojující	spojující	k2eAgFnPc1d1
různé	různý	k2eAgFnPc1d1
techniky	technika	k1gFnPc1
jiných	jiný	k2eAgInPc2d1
bojových	bojový	k2eAgInPc2d1
sportů	sport	k1gInPc2
i	i	k8xC
bojových	bojový	k2eAgNnPc2d1
umění	umění	k1gNnPc2
<g/>
.	.	kIx.
</s>