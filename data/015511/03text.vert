<s>
Smíšená	smíšený	k2eAgNnPc1d1
bojová	bojový	k2eAgNnPc1d1
umění	umění	k1gNnPc1
</s>
<s>
Smíšená	smíšený	k2eAgNnPc1d1
bojová	bojový	k2eAgNnPc1d1
umění	umění	k1gNnPc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Mixed	Mixed	k1gMnSc1
Martial	Martial	k1gMnSc1
Arts	Arts	k1gInSc4
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
MMA	MMA	kA
<g/>
,	,	kIx,
portugalsky	portugalsky	k6eAd1
Artes	Artes	k1gMnSc1
marciais	marciais	k1gFnPc2
mistas	mistas	k1gMnSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
AMM	AMM	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
plnokontaktní	plnokontaktní	k2eAgInSc1d1
bojový	bojový	k2eAgInSc1d1
sport	sport	k1gInSc1
bez	bez	k7c2
využití	využití	k1gNnSc2
zbraní	zbraň	k1gFnPc2
<g/>
,	,	kIx,
umožňující	umožňující	k2eAgInPc4d1
údery	úder	k1gInPc4
i	i	k8xC
chvaty	chvat	k1gInPc4
<g/>
,	,	kIx,
boj	boj	k1gInSc4
ve	v	k7c6
stoje	stoje	k6eAd1
i	i	k8xC
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
spojující	spojující	k2eAgFnPc1d1
různé	různý	k2eAgFnPc1d1
techniky	technika	k1gFnPc1
jiných	jiný	k2eAgInPc2d1
bojových	bojový	k2eAgInPc2d1
sportů	sport	k1gInPc2
i	i	k8xC
bojových	bojový	k2eAgNnPc2d1
umění	umění	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgMnS
na	na	k7c6
konci	konec	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
ze	z	k7c2
soutěží	soutěž	k1gFnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
nalézt	nalézt	k5eAaBmF,k5eAaPmF
mezi	mezi	k7c7
různými	různý	k2eAgNnPc7d1
bojovými	bojový	k2eAgNnPc7d1
uměními	umění	k1gNnPc7
nejúčinnější	účinný	k2eAgFnSc1d3
způsob	způsob	k1gInSc4
boje	boj	k1gInSc2
ve	v	k7c6
skutečných	skutečný	k2eAgFnPc6d1
situacích	situace	k1gFnPc6
neozbrojeného	ozbrojený	k2eNgInSc2d1
střetu	střet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Počátky	počátek	k1gInPc4
</s>
<s>
Řecký	řecký	k2eAgInSc1d1
pankration	pankration	k1gInSc1
</s>
<s>
Smíšená	smíšený	k2eAgNnPc1d1
bojová	bojový	k2eAgNnPc1d1
umění	umění	k1gNnPc1
(	(	kIx(
<g/>
či	či	k8xC
Vale-tudo	Vale-tudo	k1gNnSc1
<g/>
,	,	kIx,
Free-Fight	Free-Fight	k1gInSc1
<g/>
,	,	kIx,
Ultimate-Fighting	Ultimate-Fighting	k1gInSc1
apod.	apod.	kA
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
fenomén	fenomén	k1gNnSc1
posledních	poslední	k2eAgNnPc2d1
dvou	dva	k4xCgNnPc2
desetiletí	desetiletí	k1gNnPc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
jeho	jeho	k3xOp3gInPc1
počátky	počátek	k1gInPc1
sahají	sahat	k5eAaImIp3nP
až	až	k9
do	do	k7c2
7	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
kdy	kdy	k6eAd1
Řekové	Řek	k1gMnPc1
zavedli	zavést	k5eAaPmAgMnP
na	na	k7c6
Olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
Pankration	Pankration	k1gInSc4
<g/>
,	,	kIx,
drsnou	drsný	k2eAgFnSc4d1
směs	směs	k1gFnSc4
starověkého	starověký	k2eAgInSc2d1
boxu	box	k1gInSc2
a	a	k8xC
zápasu	zápas	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yRgMnPc4,k3yIgMnPc4,k3yQgMnPc4
byly	být	k5eAaImAgInP
povolené	povolený	k2eAgInPc1d1
všechny	všechen	k3xTgInPc1
údery	úder	k1gInPc1
a	a	k8xC
zápasnické	zápasnický	k2eAgFnPc1d1
techniky	technika	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
by	by	kYmCp3nP
vedly	vést	k5eAaImAgInP
k	k	k7c3
poražení	poražení	k1gNnSc3
soupeře	soupeř	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zápasy	zápas	k1gInPc1
trvaly	trvat	k5eAaImAgFnP
často	často	k6eAd1
několik	několik	k4yIc4
hodin	hodina	k1gFnPc2
a	a	k8xC
nezřídka	nezřídka	k6eAd1
končily	končit	k5eAaImAgInP
smrtí	smrt	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Pankratisty	Pankratista	k1gMnPc4
využíval	využívat	k5eAaImAgMnS,k5eAaPmAgMnS
pro	pro	k7c4
jejich	jejich	k3xOp3gFnPc4
legendární	legendární	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
např.	např.	kA
Alexandr	Alexandr	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
při	při	k7c6
svých	svůj	k3xOyFgNnPc6
taženích	tažení	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
způsob	způsob	k1gInSc1
boje	boj	k1gInSc2
přetrval	přetrvat	k5eAaPmAgInS
v	v	k7c6
Řecku	Řecko	k1gNnSc6
dodnes	dodnes	k6eAd1
<g/>
,	,	kIx,
ovšem	ovšem	k9
s	s	k7c7
jasnými	jasný	k2eAgNnPc7d1
a	a	k8xC
bezpečnými	bezpečný	k2eAgNnPc7d1
pravidly	pravidlo	k1gNnPc7
chránícími	chránící	k2eAgFnPc7d1
oba	dva	k4xCgMnPc4
bojovníky	bojovník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Zrod	zrod	k1gInSc1
moderní	moderní	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
smíšených	smíšený	k2eAgNnPc2d1
bojových	bojový	k2eAgNnPc2d1
umění	umění	k1gNnPc2
</s>
<s>
Zrod	zrod	k1gInSc4
moderních	moderní	k2eAgNnPc2d1
smíšených	smíšený	k2eAgNnPc2d1
bojových	bojový	k2eAgNnPc2d1
umění	umění	k1gNnPc2
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
vystopovat	vystopovat	k5eAaPmF
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
první	první	k4xOgFnSc4
pol.	pol.	k?
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Helio	Helio	k1gMnSc1
Gracie	Gracie	k1gFnSc2
(	(	kIx(
<g/>
rodina	rodina	k1gFnSc1
Gracie	Gracie	k1gFnSc2
vytvořila	vytvořit	k5eAaPmAgFnS
Brazilské	brazilský	k2eAgNnSc4d1
Jiu-jitsu	Jiu-jitsa	k1gFnSc4
<g/>
)	)	kIx)
vyzýval	vyzývat	k5eAaImAgMnS
k	k	k7c3
boji	boj	k1gInSc3
zástupce	zástupce	k1gMnPc4
různých	různý	k2eAgInPc2d1
bojových	bojový	k2eAgInPc2d1
stylů	styl	k1gInPc2
podle	podle	k7c2
pravidel	pravidlo	k1gNnPc2
Vale	val	k1gInSc6
tudo	tudo	k1gNnSc1
(	(	kIx(
<g/>
portugalsky	portugalsky	k6eAd1
vše	všechen	k3xTgNnSc1
dovoleno	dovolen	k2eAgNnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
těchto	tento	k3xDgInPc6
bojích	boj	k1gInPc6
většinou	většinou	k6eAd1
vítězil	vítězit	k5eAaImAgMnS
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
proti	proti	k7c3
mnohem	mnohem	k6eAd1
těžším	těžký	k2eAgMnPc3d2
bojovníkům	bojovník	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Učil	učít	k5eAaPmAgMnS,k5eAaImAgMnS
BJJ	BJJ	kA
i	i	k9
své	svůj	k3xOyFgMnPc4
syny	syn	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
poté	poté	k6eAd1
vydali	vydat	k5eAaPmAgMnP
proslulou	proslulý	k2eAgFnSc4d1
Gracieovskou	Gracieovský	k2eAgFnSc4d1
výzvu	výzva	k1gFnSc4
(	(	kIx(
<g/>
odměnu	odměna	k1gFnSc4
100	#num#	k4
000	#num#	k4
dolarů	dolar	k1gInPc2
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
je	on	k3xPp3gMnPc4
dokáže	dokázat	k5eAaPmIp3nS
porazit	porazit	k5eAaPmF
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
zajistili	zajistit	k5eAaPmAgMnP
svému	svůj	k3xOyFgInSc3
rodinnému	rodinný	k2eAgInSc3d1
stylu	styl	k1gInSc3
nehynoucí	hynoucí	k2eNgFnSc1d1
slávu	sláva	k1gFnSc4
a	a	k8xC
přitáhli	přitáhnout	k5eAaPmAgMnP
k	k	k7c3
soutěžím	soutěž	k1gFnPc3
ve	v	k7c4
Vale-Tudo	Vale-Tudo	k1gNnSc4
množství	množství	k1gNnSc2
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Bojovníky	bojovník	k1gMnPc4
vždycky	vždycky	k6eAd1
zajímalo	zajímat	k5eAaImAgNnS
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
styl	styl	k1gInSc1
boje	boj	k1gInSc2
je	být	k5eAaImIp3nS
nejlepší	dobrý	k2eAgNnSc1d3
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
a	a	k8xC
tato	tento	k3xDgFnSc1
zvědavost	zvědavost	k1gFnSc1
vyústila	vyústit	k5eAaPmAgFnS
dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1993	#num#	k4
v	v	k7c6
první	první	k4xOgMnSc1
amatérský	amatérský	k2eAgInSc1d1
šampionát	šampionát	k1gInSc1
UFC	UFC	kA
(	(	kIx(
<g/>
Ultimate	Ultimat	k1gInSc5
Fighting	Fighting	k1gInSc4
Championship	Championship	k1gInSc1
<g/>
)	)	kIx)
pořádaný	pořádaný	k2eAgInSc1d1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Proti	proti	k7c3
sobě	se	k3xPyFc3
tu	tu	k6eAd1
nastoupili	nastoupit	k5eAaPmAgMnP
zástupci	zástupce	k1gMnPc1
různých	různý	k2eAgInPc2d1
stylů	styl	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
Savate	Savat	k1gInSc5
<g/>
,	,	kIx,
Box	box	k1gInSc4
<g/>
,	,	kIx,
Karate	karate	k1gNnSc4
<g/>
,	,	kIx,
Jiu-jitsu	Jiu-jitsa	k1gFnSc4
<g/>
,	,	kIx,
Zápas	zápas	k1gInSc4
apod.	apod.	kA
</s>
<s>
Šampionát	šampionát	k1gInSc1
(	(	kIx(
<g/>
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
několik	několik	k4yIc4
následujících	následující	k2eAgFnPc2d1
<g/>
)	)	kIx)
vyhrál	vyhrát	k5eAaPmAgMnS
zástupce	zástupce	k1gMnSc1
Brazilského	brazilský	k2eAgInSc2d1
Jiu-jitsu	Jiu-jits	k1gInSc2
Royce	Royce	k1gFnSc2
Gracie	Gracie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
ve	v	k7c6
finále	finále	k1gNnSc6
porazil	porazit	k5eAaPmAgMnS
Gerarda	Gerard	k1gMnSc4
Gordeaua	Gordeauus	k1gMnSc4
(	(	kIx(
<g/>
Savate	Savat	k1gInSc5
<g/>
)	)	kIx)
a	a	k8xC
to	ten	k3xDgNnSc1
díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
technice	technika	k1gFnSc3
boje	boj	k1gInSc2
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
ukázal	ukázat	k5eAaPmAgInS
ostatním	ostatní	k2eAgMnPc3d1
stylům	styl	k1gInPc3
boje	boj	k1gInSc2
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
jim	on	k3xPp3gMnPc3
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c4
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
nebyly	být	k5eNaImAgFnP
schopné	schopný	k2eAgFnPc1d1
odpovědět	odpovědět	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Dnešní	dnešní	k2eAgNnPc1d1
smíšená	smíšený	k2eAgNnPc1d1
bojová	bojový	k2eAgNnPc1d1
umění	umění	k1gNnPc1
</s>
<s>
V	v	k7c6
dnešních	dnešní	k2eAgNnPc6d1
smíšených	smíšený	k2eAgNnPc6d1
bojových	bojový	k2eAgNnPc6d1
uměních	umění	k1gNnPc6
jsou	být	k5eAaImIp3nP
bojovníci	bojovník	k1gMnPc1
připravovaní	připravovaný	k2eAgMnPc1d1
ve	v	k7c6
všech	všecek	k3xTgFnPc6
třech	tři	k4xCgFnPc6
úrovních	úroveň	k1gFnPc6
boje	boj	k1gInSc2
—	—	k?
boj	boj	k1gInSc1
v	v	k7c6
postoji	postoj	k1gInSc6
<g/>
,	,	kIx,
zápas	zápas	k1gInSc4
a	a	k8xC
boj	boj	k1gInSc4
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
MMA	MMA	kA
nemá	mít	k5eNaImIp3nS
byť	byť	k8xS
dobrý	dobrý	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
jednodimenzionální	jednodimenzionální	k2eAgMnSc1d1
bojovník	bojovník	k1gMnSc1
šanci	šance	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
elitních	elitní	k2eAgFnPc2d1
profesionálních	profesionální	k2eAgFnPc2d1
soutěží	soutěž	k1gFnPc2
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
například	například	k6eAd1
americké	americký	k2eAgInPc1d1
UFC	UFC	kA
<g/>
,	,	kIx,
Bellator	Bellator	k1gInSc1
a	a	k8xC
PFL	PFL	kA
<g/>
,	,	kIx,
japonská	japonský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Rizin	Rizin	k1gInSc4
Fighting	Fighting	k1gInSc1
Federation	Federation	k1gInSc1
<g/>
,	,	kIx,
ruská	ruský	k2eAgFnSc1d1
M-1	M-1	k1gMnPc4
Global	globat	k5eAaImAgMnS
a	a	k8xC
Absolute	Absolut	k1gInSc5
Championship	Championship	k1gMnSc1
Akhmat	Akhmat	k1gInSc1
nebo	nebo	k8xC
asijská	asijský	k2eAgFnSc1d1
ONE	ONE	kA
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
toho	ten	k3xDgInSc2
je	být	k5eAaImIp3nS
bezpočet	bezpočet	k1gInSc1
menších	malý	k2eAgFnPc2d2
organizací	organizace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
jsou	být	k5eAaImIp3nP
největší	veliký	k2eAgFnPc1d3
organizace	organizace	k1gFnPc1
Oktagon	Oktagona	k1gFnPc2
MMA	MMA	kA
<g/>
,	,	kIx,
IAF	IAF	kA
a	a	k8xC
GCF	GCF	kA
<g/>
.	.	kIx.
</s>
<s>
Bojuje	bojovat	k5eAaImIp3nS
se	se	k3xPyFc4
povětšinou	povětšinou	k6eAd1
v	v	k7c6
oktagonu	oktagon	k1gInSc6
(	(	kIx(
<g/>
osmiúhelník	osmiúhelník	k1gInSc4
vyplněný	vyplněný	k2eAgInSc4d1
pletivem	pletivo	k1gNnSc7
<g/>
,	,	kIx,
často	často	k6eAd1
označovaný	označovaný	k2eAgInSc1d1
jako	jako	k8xC,k8xS
klec	klec	k1gFnSc1
<g/>
)	)	kIx)
anebo	anebo	k8xC
v	v	k7c6
upraveném	upravený	k2eAgInSc6d1
boxerském	boxerský	k2eAgInSc6d1
ringu	ring	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
profesionálních	profesionální	k2eAgInPc6d1
bojích	boj	k1gInPc6
se	se	k3xPyFc4
bojuje	bojovat	k5eAaImIp3nS
na	na	k7c4
omezený	omezený	k2eAgInSc4d1
počet	počet	k1gInSc4
kol	kolo	k1gNnPc2
s	s	k7c7
přesně	přesně	k6eAd1
daným	daný	k2eAgInSc7d1
časovým	časový	k2eAgInSc7d1
limitem	limit	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povinné	povinný	k2eAgInPc1d1
chrániče	chránič	k1gInPc1
tvoří	tvořit	k5eAaImIp3nP
pouze	pouze	k6eAd1
MMA	MMA	kA
rukavice	rukavice	k1gFnSc1
<g/>
,	,	kIx,
chránič	chránič	k1gInSc1
zubů	zub	k1gInPc2
a	a	k8xC
suspenzor	suspenzor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
několik	několik	k4yIc4
způsobů	způsob	k1gInPc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
dosáhnout	dosáhnout	k5eAaPmF
vítězství	vítězství	k1gNnSc4
—	—	k?
knockout	knockout	k1gMnSc1
(	(	kIx(
<g/>
K.O.	K.O.	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vzdáním	vzdání	k1gNnSc7
soupeře	soupeř	k1gMnSc2
(	(	kIx(
<g/>
submission	submission	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zastavení	zastavení	k1gNnSc1
rozhodčím	rozhodčí	k1gMnSc7
–	–	k?
technické	technický	k2eAgFnSc3d1
K.O.	K.O.	k1gFnSc3
(	(	kIx(
<g/>
T.K.O.	T.K.O.	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vážnější	vážní	k2eAgNnSc1d2
zranění	zranění	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
znemožňuje	znemožňovat	k5eAaImIp3nS
pokračování	pokračování	k1gNnSc4
v	v	k7c6
zápasu	zápas	k1gInSc6
(	(	kIx(
<g/>
injury	injura	k1gFnSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
výhra	výhra	k1gFnSc1
na	na	k7c4
body	bod	k1gInPc4
(	(	kIx(
<g/>
unanimous	unanimous	k1gInSc1
decesion	decesion	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
pravidla	pravidlo	k1gNnPc1
od	od	k7c2
každé	každý	k3xTgFnSc2
profesionální	profesionální	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
mění	měnit	k5eAaImIp3nP
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
skoro	skoro	k6eAd1
ve	v	k7c6
všech	všecek	k3xTgNnPc6
pravidlech	pravidlo	k1gNnPc6
zakázány	zakázat	k5eAaPmNgInP
údery	úder	k1gInPc7
do	do	k7c2
genitálií	genitálie	k1gFnPc2
<g/>
,	,	kIx,
píchání	píchání	k1gNnPc2
do	do	k7c2
očí	oko	k1gNnPc2
<g/>
,	,	kIx,
údery	úder	k1gInPc4
do	do	k7c2
temene	temeno	k1gNnSc2
hlavy	hlava	k1gFnSc2
nebo	nebo	k8xC
páky	páka	k1gFnSc2
na	na	k7c4
malé	malý	k2eAgInPc4d1
klouby	kloub	k1gInPc4
(	(	kIx(
<g/>
prsty	prst	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
MMA	MMA	kA
bojovníci	bojovník	k1gMnPc1
například	například	k6eAd1
nesmějí	smát	k5eNaImIp3nP
držet	držet	k5eAaImF
pletiva	pletivo	k1gNnSc2
nebo	nebo	k8xC
soupeřových	soupeřův	k2eAgFnPc2d1
trenek	trenky	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
dodržování	dodržování	k1gNnSc6
pravidel	pravidlo	k1gNnPc2
dohlíží	dohlížet	k5eAaImIp3nS
rozhodčí	rozhodčí	k1gMnPc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
přítomný	přítomný	k2eAgInSc1d1
v	v	k7c6
oktagonu	oktagon	k1gInSc6
po	po	k7c4
celý	celý	k2eAgInSc4d1
zápas	zápas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpoza	zpoza	k7c2
oktagonu	oktagon	k1gInSc2
vše	všechen	k3xTgNnSc4
sledují	sledovat	k5eAaImIp3nP
bodoví	bodový	k2eAgMnPc1d1
rozhodčí	rozhodčí	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
v	v	k7c6
případě	případ	k1gInSc6
ukončení	ukončení	k1gNnSc4
zápasu	zápas	k1gInSc2
po	po	k7c6
uplynutí	uplynutí	k1gNnSc6
limitu	limit	k1gInSc2
<g/>
,	,	kIx,
bodují	bodovat	k5eAaImIp3nP
jednotlivá	jednotlivý	k2eAgNnPc4d1
kola	kolo	k1gNnPc4
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
rozhodnou	rozhodnout	k5eAaPmIp3nP
zápas	zápas	k1gInSc4
ve	v	k7c4
prospěch	prospěch	k1gInSc4
jednoho	jeden	k4xCgMnSc2
z	z	k7c2
bojovníků	bojovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejznámější	známý	k2eAgInPc4d3
světové	světový	k2eAgInPc4d1
MMA	MMA	kA
bojovníky	bojovník	k1gMnPc4
momentálně	momentálně	k6eAd1
patří	patřit	k5eAaImIp3nS
jména	jméno	k1gNnPc4
jako	jako	k8xS,k8xC
například	například	k6eAd1
Conor	Conor	k1gMnSc1
McGregor	McGregor	k1gMnSc1
<g/>
,	,	kIx,
Jon	Jon	k1gMnSc1
„	„	k?
<g/>
Bones	Bones	k1gMnSc1
<g/>
“	“	k?
Jones	Jones	k1gMnSc1
<g/>
,	,	kIx,
Ronda	ronda	k1gFnSc1
Rousey	Rousea	k1gFnSc2
<g/>
,	,	kIx,
Cain	Cain	k1gMnSc1
Velasquez	Velasquez	k1gMnSc1
<g/>
,	,	kIx,
Anderson	Anderson	k1gMnSc1
Silva	Silva	k1gFnSc1
<g/>
,	,	kIx,
Alistair	Alistair	k1gInSc4
Overeem	Overeum	k1gNnSc7
nebo	nebo	k8xC
Robbie	Robbie	k1gFnSc1
Lawler	Lawler	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gInPc1
bojovníků	bojovník	k1gMnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Striker	Striker	k1gMnSc1
—	—	k?
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
brání	bránit	k5eAaImIp3nS
porazům	poraz	k1gInPc3
(	(	kIx(
<g/>
takedownům	takedowno	k1gNnPc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rád	rád	k6eAd1
bojuje	bojovat	k5eAaImIp3nS
v	v	k7c6
postoji	postoj	k1gInSc6
a	a	k8xC
chce	chtít	k5eAaImIp3nS
zápas	zápas	k1gInSc4
ukončit	ukončit	k5eAaPmF
K.O	K.O	k1gFnSc4
nebo	nebo	k8xC
TKO	TKO	kA
(	(	kIx(
<g/>
technical	technicat	k5eAaPmAgMnS
knockout	knockout	k1gMnSc1
<g/>
)	)	kIx)
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejlepší	dobrý	k2eAgNnSc4d3
patří	patřit	k5eAaImIp3nS
např.	např.	kA
Conor	Conor	k1gMnSc1
McGregor	McGregor	k1gMnSc1
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
Hunt	hunt	k1gInSc1
<g/>
,	,	kIx,
Stephen	Stephen	k1gInSc1
Thompson	Thompsona	k1gFnPc2
nebo	nebo	k8xC
Darren	Darrna	k1gFnPc2
Till	Tilla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wrestler	Wrestler	k1gMnSc1
—	—	k?
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
často	často	k6eAd1
používá	používat	k5eAaImIp3nS
porazy	poraz	k1gInPc4
a	a	k8xC
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
soupeře	soupeř	k1gMnSc2
dobít	dobít	k5eAaPmF
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejlepší	dobrý	k2eAgMnPc4d3
patří	patřit	k5eAaImIp3nP
např.	např.	kA
Tyron	Tyron	k1gInSc4
Woodley	Woodlea	k1gFnSc2
Cain	Caina	k1gFnPc2
Velasquez	Velasquez	k1gInSc1
<g/>
,	,	kIx,
Colby	Colba	k1gFnPc1
Covington	Covington	k1gInSc1
<g/>
,	,	kIx,
Demetrious	Demetrious	k1gMnSc1
Johnson	Johnson	k1gMnSc1
<g/>
,	,	kIx,
Karlos	Karlos	k1gMnSc1
Vémola	Vémola	k1gFnSc1
nebo	nebo	k8xC
Henry	henry	k1gInSc1
Cejudo	Cejudo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Groundfighter	Groundfighter	k1gInSc1
/	/	kIx~
Grappler	Grappler	k1gInSc1
/	/	kIx~
Jiu-Jitsu	Jiu-Jits	k1gInSc2
fighter	fighter	k1gMnSc1
—	—	k?
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
boj	boj	k1gInSc4
přenáší	přenášet	k5eAaImIp3nS
na	na	k7c4
zem	zem	k1gFnSc4
a	a	k8xC
tam	tam	k6eAd1
si	se	k3xPyFc3
vynutí	vynutit	k5eAaPmIp3nS
vzdání	vzdání	k1gNnSc4
soupeře	soupeř	k1gMnSc2
pomocí	pomocí	k7c2
různých	různý	k2eAgFnPc2d1
technik	technika	k1gFnPc2
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
pákou	páka	k1gFnSc7
nebo	nebo	k8xC
škrcením	škrcení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejlepší	dobrý	k2eAgNnSc4d3
patří	patřit	k5eAaImIp3nS
např.	např.	kA
Anderson	Anderson	k1gMnSc1
Silva	Silva	k1gFnSc1
<g/>
,	,	kIx,
Royce	Royce	k1gFnSc1
Gracie	Gracie	k1gFnSc1
<g/>
,	,	kIx,
Demian	Demian	k1gMnSc1
Maia	Maia	k1gMnSc1
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
Ortega	Ortega	k1gFnSc1
nebo	nebo	k8xC
Khabib	Khabib	k1gInSc1
Nurmagomedov	Nurmagomedovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Váhové	váhový	k2eAgFnPc1d1
kategorie	kategorie	k1gFnPc1
</s>
<s>
Bojují	bojovat	k5eAaImIp3nP
se	se	k3xPyFc4
v	v	k7c6
několika	několik	k4yIc6
váhových	váhový	k2eAgFnPc6d1
kategoriích	kategorie	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
smíšená	smíšený	k2eAgNnPc4d1
bojová	bojový	k2eAgNnPc4d1
umění	umění	k1gNnSc4
jako	jako	k8xS,k8xC
v	v	k7c6
každém	každý	k3xTgInSc6
jiném	jiný	k2eAgInSc6d1
bojovém	bojový	k2eAgInSc6d1
sportu	sport	k1gInSc6
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
udržování	udržování	k1gNnSc4
si	se	k3xPyFc3
váhu	váha	k1gFnSc4
do	do	k7c2
každé	každý	k3xTgFnSc2
z	z	k7c2
kategorií	kategorie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
kategorie	kategorie	k1gFnPc4
má	mít	k5eAaImIp3nS
svého	svůj	k3xOyFgMnSc2
šampiona	šampion	k1gMnSc2
<g/>
,	,	kIx,
následovaného	následovaný	k2eAgMnSc2d1
zástupem	zástupem	k6eAd1
vyzyvatelů	vyzyvatelů	k?
(	(	kIx(
<g/>
contenders	contenders	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
mezi	mezi	k7c7
sebou	se	k3xPyFc7
bojují	bojovat	k5eAaImIp3nP
o	o	k7c4
možnost	možnost	k1gFnSc4
vyzvat	vyzvat	k5eAaPmF
právě	právě	k9
šampiona	šampion	k1gMnSc4
na	na	k7c4
titulový	titulový	k2eAgInSc4d1
zápas	zápas	k1gInSc4
a	a	k8xC
v	v	k7c6
případě	případ	k1gInSc6
výhry	výhra	k1gFnSc2
se	se	k3xPyFc4
jím	jíst	k5eAaImIp1nS
stát	stát	k5eAaPmF,k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1
mužské	mužský	k2eAgFnPc1d1
kategorie	kategorie	k1gFnPc1
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
muší	muší	k2eAgFnSc1d1
váha	váha	k1gFnSc1
(	(	kIx(
<g/>
flyweight	flyweight	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
do	do	k7c2
56	#num#	k4
kg	kg	kA
</s>
<s>
bantamová	bantamový	k2eAgFnSc1d1
váha	váha	k1gFnSc1
(	(	kIx(
<g/>
bantamweight	bantamweight	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
do	do	k7c2
61	#num#	k4
kg	kg	kA
</s>
<s>
pérová	pérový	k2eAgFnSc1d1
váha	váha	k1gFnSc1
(	(	kIx(
<g/>
featherweight	featherweight	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
do	do	k7c2
66	#num#	k4
kg	kg	kA
</s>
<s>
lehká	lehký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
(	(	kIx(
<g/>
lightweight	lightweight	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
do	do	k7c2
70	#num#	k4
kg	kg	kA
</s>
<s>
welterová	welterový	k2eAgFnSc1d1
váha	váha	k1gFnSc1
(	(	kIx(
<g/>
welterweight	welterweight	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
do	do	k7c2
77	#num#	k4
kg	kg	kA
</s>
<s>
střední	střední	k2eAgFnSc1d1
váha	váha	k1gFnSc1
(	(	kIx(
<g/>
middleweight	middleweight	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
do	do	k7c2
84	#num#	k4
kg	kg	kA
</s>
<s>
polotěžká	polotěžký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
(	(	kIx(
<g/>
light-heavyweight	light-heavyweight	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
do	do	k7c2
93	#num#	k4
kg	kg	kA
</s>
<s>
těžká	těžký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
(	(	kIx(
<g/>
heavyweight	heavyweight	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
do	do	k7c2
120	#num#	k4
kg	kg	kA
</s>
<s>
Lze	lze	k6eAd1
také	také	k9
bojovat	bojovat	k5eAaImF
ve	v	k7c6
smluvní	smluvní	k2eAgFnSc6d1
váze	váha	k1gFnSc6
<g/>
,	,	kIx,
tzv.	tzv.	kA
catchweight	catchweight	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
oba	dva	k4xCgMnPc1
soupeři	soupeř	k1gMnPc1
domluví	domluvit	k5eAaPmIp3nP
na	na	k7c6
požadované	požadovaný	k2eAgFnSc6d1
váze	váha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Ženské	ženská	k1gFnPc1
váhové	váhový	k2eAgFnSc2d1
kategorie	kategorie	k1gFnSc2
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
women	women	k2eAgMnSc1d1
strawweight	strawweight	k1gMnSc1
–	–	k?
do	do	k7c2
52	#num#	k4
kg	kg	kA
</s>
<s>
bantamová	bantamový	k2eAgFnSc1d1
váha	váha	k1gFnSc1
(	(	kIx(
<g/>
women	women	k2eAgInSc1d1
bantamweight	bantamweight	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
do	do	k7c2
61	#num#	k4
kg	kg	kA
</s>
<s>
featherweight	featherweight	k1gInSc1
(	(	kIx(
<g/>
pérová	pérový	k2eAgFnSc1d1
váha	váha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Oblečení	oblečení	k1gNnSc1
a	a	k8xC
vybavení	vybavení	k1gNnSc1
</s>
<s>
MMA	MMA	kA
bojovníci	bojovník	k1gMnPc1
standardně	standardně	k6eAd1
ve	v	k7c6
většině	většina	k1gFnSc6
organizací	organizace	k1gFnPc2
nosí	nosit	k5eAaImIp3nS
šortky	šortky	k1gFnPc4
nebo	nebo	k8xC
trenky	trenky	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
mají	mít	k5eAaImIp3nP
na	na	k7c6
sobě	sebe	k3xPyFc6
i	i	k8xC
kraťasy	kraťas	k1gInPc4
thaiboxerského	thaiboxerský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgFnPc6
menších	malý	k2eAgFnPc6d2
organizacích	organizace	k1gFnPc6
jsou	být	k5eAaImIp3nP
povoleny	povolen	k2eAgInPc1d1
slipy	slip	k1gInPc1
nebo	nebo	k8xC
přiléhavá	přiléhavý	k2eAgNnPc1d1
trička	tričko	k1gNnPc1
tzv.	tzv.	kA
„	„	k?
<g/>
rash	rash	k1gMnSc1
guard	guard	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ženy	žena	k1gFnPc1
navíc	navíc	k6eAd1
nosí	nosit	k5eAaImIp3nP
sportovní	sportovní	k2eAgFnSc4d1
podprsenku	podprsenka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jsou	být	k5eAaImIp3nP
povinné	povinný	k2eAgFnPc4d1
ochranné	ochranný	k2eAgFnPc4d1
pomůcky	pomůcka	k1gFnPc4
jako	jako	k8xC,k8xS
suspenzor	suspenzor	k1gInSc4
a	a	k8xC
chránič	chránič	k1gInSc4
na	na	k7c4
zuby	zub	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
boj	boj	k1gInSc4
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
speciální	speciální	k2eAgFnSc2d1
bezprsté	bezprstý	k2eAgFnSc2d1
MMA	MMA	kA
rukavice	rukavice	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
umožňují	umožňovat	k5eAaImIp3nP
lepší	dobrý	k2eAgInSc4d2
úchop	úchop	k1gInSc4
soupeře	soupeř	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgFnPc6
organizacích	organizace	k1gFnPc6
jsou	být	k5eAaImIp3nP
povolené	povolený	k2eAgInPc1d1
i	i	k8xC
návleky	návlek	k1gInPc1
na	na	k7c4
kotníky	kotník	k1gInPc4
<g/>
,	,	kIx,
</s>
<s>
Česká	český	k2eAgNnPc1d1
smíšená	smíšený	k2eAgNnPc1d1
bojová	bojový	k2eAgNnPc1d1
umění	umění	k1gNnPc1
</s>
<s>
Známí	známit	k5eAaImIp3nP
čeští	český	k2eAgMnPc1d1
MMA	MMA	kA
bojovníci	bojovník	k1gMnPc1
</s>
<s>
Karlos	Karlos	k1gMnSc1
„	„	k?
<g/>
Terminátor	terminátor	k1gMnSc1
<g/>
“	“	k?
Vémola	Vémola	k1gFnSc1
(	(	kIx(
<g/>
London	London	k1gMnSc1
Shootfighters	Shootfightersa	k1gFnPc2
<g/>
)	)	kIx)
–	–	k?
mistr	mistr	k1gMnSc1
tří	tři	k4xCgFnPc2
organizací	organizace	k1gFnPc2
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
váze	váha	k1gFnSc6
(	(	kIx(
<g/>
GCF	GCF	kA
<g/>
,	,	kIx,
MMAA	MMAA	kA
<g/>
,	,	kIx,
UCMMA	UCMMA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
Čech	Čech	k1gMnSc1
v	v	k7c6
UFC	UFC	kA
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
„	„	k?
<g/>
Denisa	Denisa	k1gFnSc1
<g/>
“	“	k?
Procházka	Procházka	k1gMnSc1
(	(	kIx(
<g/>
Jetsaam	Jetsaam	k1gInSc1
Gym	Gym	k1gFnSc2
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
bojovník	bojovník	k1gMnSc1
polotěžké	polotěžký	k2eAgFnSc2d1
váhy	váha	k1gFnSc2
<g/>
,	,	kIx,
šampion	šampion	k1gMnSc1
Rizin	Rizin	k2eAgInSc4d1
Fighting	Fighting	k1gInSc4
Federation	Federation	k1gInSc1
<g/>
,	,	kIx,
bojovník	bojovník	k1gMnSc1
UFC	UFC	kA
<g/>
.	.	kIx.
</s>
<s>
Lucie	Lucie	k1gFnSc1
Pudilová	Pudilová	k1gFnSc1
(	(	kIx(
<g/>
KBC	KBC	kA
Příbram	Příbram	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
první	první	k4xOgFnSc1
Češka	Češka	k1gFnSc1
v	v	k7c6
UFC	UFC	kA
<g/>
.	.	kIx.
</s>
<s>
Magdalena	Magdalena	k1gFnSc1
Šormová	Šormová	k1gFnSc1
(	(	kIx(
<g/>
Penta	Penta	k1gFnSc1
Gym	Gym	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Ohledně	ohledně	k7c2
bezpečnosti	bezpečnost	k1gFnSc2
MMA	MMA	kA
panuje	panovat	k5eAaImIp3nS
stále	stále	k6eAd1
kontroverze	kontroverze	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přispívá	přispívat	k5eAaImIp3nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
i	i	k9
nedostatek	nedostatek	k1gInSc1
sběru	sběr	k1gInSc2
a	a	k8xC
přístupnosti	přístupnost	k1gFnSc2
údajů	údaj	k1gInPc2
o	o	k7c6
zraněních	zranění	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
nynější	nynější	k2eAgFnSc6d1
době	doba	k1gFnSc6
jsou	být	k5eAaImIp3nP
otevřeny	otevřít	k5eAaPmNgFnP
i	i	k9
tzv	tzv	kA
„	„	k?
<g/>
přípravky	přípravka	k1gFnPc4
<g/>
“	“	k?
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
sport	sport	k1gInSc1
(	(	kIx(
a	a	k8xC
to	ten	k3xDgNnSc1
platí	platit	k5eAaImIp3nS
i	i	k9
pro	pro	k7c4
MMA	MMA	kA
<g/>
)	)	kIx)
dělá	dělat	k5eAaImIp3nS
s	s	k7c7
rozumem	rozum	k1gInSc7
a	a	k8xC
dbáme	dbát	k5eAaImIp1nP
na	na	k7c4
rady	rada	k1gFnPc4
trenéra	trenér	k1gMnSc2
<g/>
,	,	kIx,
zranění	zraněný	k2eAgMnPc1d1
jsou	být	k5eAaImIp3nP
minimální	minimální	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Četnost	četnost	k1gFnSc1
zranění	zranění	k1gNnSc2
</s>
<s>
Dle	dle	k7c2
poslední	poslední	k2eAgFnSc2d1
meta-analýzy	meta-analýza	k1gFnSc2
dostupných	dostupný	k2eAgInPc2d1
údajů	údaj	k1gInPc2
o	o	k7c6
zraněních	zranění	k1gNnPc6
v	v	k7c6
MMA	MMA	kA
je	být	k5eAaImIp3nS
četnost	četnost	k1gFnSc4
výskytu	výskyt	k1gInSc2
228.7	228.7	k4
zranění	zranění	k1gNnPc2
na	na	k7c4
1000	#num#	k4
vystoupení	vystoupení	k1gNnPc2
atleta	atlet	k1gMnSc2
(	(	kIx(
<g/>
jedno	jeden	k4xCgNnSc1
vystoupení	vystoupení	k1gNnSc1
atleta	atlet	k1gMnSc2
je	být	k5eAaImIp3nS
definováno	definovat	k5eAaBmNgNnS
jako	jako	k9
jeden	jeden	k4xCgMnSc1
atlet	atlet	k1gMnSc1
vystupující	vystupující	k2eAgMnSc1d1
v	v	k7c6
jednom	jeden	k4xCgInSc6
boji	boj	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Druhy	druh	k1gInPc1
zranění	zranění	k1gNnSc2
</s>
<s>
Obecně	obecně	k6eAd1
jsou	být	k5eAaImIp3nP
druhy	druh	k1gInPc1
zranění	zranění	k1gNnSc1
podobné	podobný	k2eAgFnPc1d1
zraněním	zranění	k1gNnSc7
v	v	k7c6
boxu	box	k1gInSc6
<g/>
,	,	kIx,
kickboxu	kickbox	k1gInSc6
<g/>
,	,	kIx,
karate	karate	k1gNnSc6
<g/>
,	,	kIx,
taekwondu	taekwondo	k1gNnSc6
ale	ale	k8xC
odlišné	odlišný	k2eAgInPc1d1
od	od	k7c2
ostatních	ostatní	k2eAgInPc2d1
bojových	bojový	k2eAgInPc2d1
sportů	sport	k1gInPc2
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
judo	judo	k1gNnSc4
a	a	k8xC
šerm	šerm	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Neoficiální	neoficiální	k2eAgInPc1d1,k2eNgInPc1d1
zápasy	zápas	k1gInPc1
</s>
<s>
Známá	známá	k1gFnSc1
úmrtí	úmrtí	k1gNnSc2
při	při	k7c6
neoficiálních	neoficiální	k2eAgNnPc6d1,k2eNgNnPc6d1
kláních	klání	k1gNnPc6
MMA	MMA	kA
<g/>
,	,	kIx,
tj.	tj.	kA
</s>
<s>
v	v	k7c4
bez	bez	k1gInSc4
vypsaných	vypsaný	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
a	a	k8xC
pauz	pauza	k1gFnPc2
atletů	atlet	k1gMnPc2
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
v	v	k7c6
oficiálním	oficiální	k2eAgNnSc6d1
MMA	MMA	kA
nenajdeme	najít	k5eNaPmIp1nP
<g/>
:	:	kIx,
</s>
<s>
Alfredo	Alfredo	k1gNnSc1
Castro	Castro	k1gNnSc1
Herrera	Herrera	k1gFnSc1
<g/>
,	,	kIx,
15	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
duben	duben	k1gInSc1
1981	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
knock-outu	knock-out	k1gMnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Douglas	Douglas	k1gInSc1
Dedge	Dedg	k1gFnSc2
<g/>
,	,	kIx,
31	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
Američan	Američan	k1gMnSc1
<g/>
,	,	kIx,
březen	březen	k1gInSc1
1998	#num#	k4
<g/>
,	,	kIx,
Kyjev	Kyjev	k1gInSc1
<g/>
,	,	kIx,
vážná	vážná	k1gFnSc1
zranění	zranění	k1gNnSc2
mozku	mozek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lee	Lea	k1gFnSc3
<g/>
,	,	kIx,
35	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
květen	květen	k1gInSc4
2005	#num#	k4
<g/>
,	,	kIx,
Samsong-dong	Samsong-dong	k1gInSc1
(	(	kIx(
<g/>
Korea	Korea	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
infarkt	infarkt	k1gInSc4
myokardu	myokard	k1gInSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mike	Mike	k1gFnSc1
Mittelmeier	Mittelmeira	k1gFnPc2
<g/>
,	,	kIx,
20	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
duben	duben	k1gInSc1
2012	#num#	k4
<g/>
,	,	kIx,
Bolívie	Bolívie	k1gFnSc1
<g/>
,	,	kIx,
krvácení	krvácení	k1gNnSc4
do	do	k7c2
mozku	mozek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dustin	Dustin	k1gMnSc1
Jenson	Jenson	k1gMnSc1
<g/>
,	,	kIx,
26	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
květen	květen	k1gInSc4
2012	#num#	k4
<g/>
,	,	kIx,
Rapid	rapid	k1gInSc1
City	city	k1gNnSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podlebeční	podlebeční	k2eAgNnSc4d1
krvácení	krvácení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Felix	Felix	k1gMnSc1
Pablo	Pablo	k1gNnSc4
Elochukwu	Elochukwus	k1gInSc2
<g/>
,	,	kIx,
35	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
Nigerijec	Nigerijec	k1gMnSc1
<g/>
,	,	kIx,
duben	duben	k1gInSc1
2013	#num#	k4
<g/>
,	,	kIx,
Port	port	k1gInSc1
Huron	Huron	k1gInSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hypoglykemický	hypoglykemický	k2eAgInSc1d1
šok	šok	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ramin	Ramin	k2eAgInSc1d1
Zeynalov	Zeynalov	k1gInSc1
<g/>
,	,	kIx,
27	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
Ázerbájdžánec	Ázerbájdžánec	k1gMnSc1
<g/>
,	,	kIx,
březen	březen	k1gInSc1
2015	#num#	k4
<g/>
,	,	kIx,
krvácení	krvácení	k1gNnSc4
do	do	k7c2
mozku	mozek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jameston	Jameston	k1gInSc1
Lee-Yaw	Lee-Yaw	k1gFnSc2
<g/>
,	,	kIx,
47	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
z	z	k7c2
Trinidadu	Trinidad	k1gInSc2
<g/>
,	,	kIx,
duben	duben	k1gInSc1
2015	#num#	k4
<g/>
,	,	kIx,
Aberdeen	Aberdeen	k1gInSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
selhání	selhání	k1gNnSc1
ledvin	ledvina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Joã	Joã	k1gNnSc1
Carvalho	Carval	k1gMnSc2
<g/>
,	,	kIx,
28	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
Portugalec	Portugalec	k1gMnSc1
<g/>
,	,	kIx,
duben	duben	k1gInSc1
2016	#num#	k4
<g/>
,	,	kIx,
Dublin	Dublin	k1gInSc1
<g/>
,	,	kIx,
úmrtí	úmrtí	k1gNnSc3
2	#num#	k4
dny	den	k1gInPc4
po	po	k7c6
knock-outu	knock-out	k1gMnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
většině	většina	k1gFnSc6
případů	případ	k1gInPc2
nebyla	být	k5eNaImAgFnS
na	na	k7c6
místě	místo	k1gNnSc6
pořadateli	pořadatel	k1gMnSc3
zajištěna	zajistit	k5eAaPmNgFnS
ošetřovatelská	ošetřovatelský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
,	,	kIx,
popř.	popř.	kA
zápasy	zápas	k1gInPc1
neměly	mít	k5eNaImAgInP
striktní	striktní	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
<g/>
,	,	kIx,
např.	např.	kA
týkající	týkající	k2eAgInPc4d1
se	se	k3xPyFc4
povinných	povinný	k2eAgFnPc2d1
pauz	pauza	k1gFnPc2
atletů	atlet	k1gMnPc2
nebo	nebo	k8xC
také	také	k9
byly	být	k5eAaImAgInP
povoleny	povolen	k2eAgInPc1d1
zakázané	zakázaný	k2eAgInPc1d1
údery	úder	k1gInPc1
a	a	k8xC
chování	chování	k1gNnSc1
které	který	k3yRgFnSc3,k3yIgFnSc3,k3yQgFnSc3
v	v	k7c6
oficiálním	oficiální	k2eAgNnSc6d1
MMA	MMA	kA
nenajdeme	najít	k5eNaPmIp1nP
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
LYSTAD	LYSTAD	kA
<g/>
,	,	kIx,
Reidar	Reidar	k1gMnSc1
P.	P.	kA
<g/>
;	;	kIx,
KOBI	KOBI	kA
GREGORY	Gregor	k1gMnPc4
<g/>
;	;	kIx,
JUNO	Juno	k1gFnSc1
WILSON	WILSON	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
epidemiology	epidemiolog	k1gMnPc4
of	of	k?
injuries	injuries	k1gMnSc1
in	in	k?
mixed	mixed	k1gMnSc1
martial	martiat	k5eAaImAgMnS,k5eAaBmAgMnS,k5eAaPmAgMnS
arts	arts	k6eAd1
<g/>
:	:	kIx,
A	a	k8xC
systematic	systematice	k1gFnPc2
review	review	k?
and	and	k?
meta-analysis	meta-analysis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orthopaedic	Orthopaedic	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Sports	Sports	k1gInSc1
Medicine	Medicin	k1gInSc5
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2325967113518492	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.117	10.117	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
2325967113518492	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
ojs	ojs	k?
<g/>
.	.	kIx.
<g/>
sagepub	sagepub	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
FREY	FREY	kA
<g/>
,	,	kIx,
A.	A.	kA
<g/>
;	;	kIx,
D.	D.	kA
ROUSSEAU	Rousseau	k1gMnSc1
<g/>
;	;	kIx,
B.	B.	kA
VESSELLE	VESSELLE	kA
<g/>
;	;	kIx,
Y.	Y.	kA
HERVOUET	HERVOUET	kA
DES	des	k1gNnPc2
FORGES	FORGES	kA
<g/>
;	;	kIx,
M.	M.	kA
EGOUMENDES	EGOUMENDES	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neuf	Neuf	k1gInSc1
saisons	saisons	k1gInSc4
de	de	k?
surveillance	surveillanec	k1gInPc4
médicale	médicale	k6eAd1
de	de	k?
compétitions	compétitions	k1gInSc1
de	de	k?
judo	judo	k1gNnSc1
<g/>
:	:	kIx,
une	une	k?
analyse	analysa	k1gFnSc6
nationale	nationale	k6eAd1
de	de	k?
la	la	k1gNnSc7
traumatologie	traumatologie	k1gFnSc2
du	du	k?
judo	judo	k1gNnSc4
en	en	k?
compétition	compétition	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
J	J	kA
Traumatologie	traumatologie	k1gFnSc1
Sport	sport	k1gInSc1
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
100	#num#	k4
<g/>
–	–	k?
<g/>
109	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
0	#num#	k4
<g/>
762	#num#	k4
<g/>
-	-	kIx~
<g/>
915	#num#	k4
<g/>
x	x	k?
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
97390	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
LYSTAD	LYSTAD	kA
<g/>
,	,	kIx,
Reidar	Reidar	k1gMnSc1
P.	P.	kA
<g/>
;	;	kIx,
HENRY	Henry	k1gMnSc1
POLLAR	POLLAR	kA
<g/>
;	;	kIx,
PETRA	Petr	k1gMnSc2
L.	L.	kA
GRAHAM	graham	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Epidemiology	epidemiolog	k1gMnPc4
of	of	k?
injuries	injuries	k1gInSc1
in	in	k?
competition	competition	k1gInSc1
taekwondo	taekwondo	k1gNnSc1
<g/>
:	:	kIx,
a	a	k8xC
meta-analysis	meta-analysis	k1gInSc1
of	of	k?
observational	observationat	k5eAaPmAgInS,k5eAaImAgInS
studies	studies	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Science	Science	k1gFnSc1
and	and	k?
Medicine	Medicin	k1gInSc5
in	in	k?
Sport	sport	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
614	#num#	k4
<g/>
–	–	k?
<g/>
621	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
jsams	jsams	k1gInSc1
<g/>
.2008	.2008	k4
<g/>
.09	.09	k4
<g/>
.013	.013	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
19054714	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ESTWANIK	ESTWANIK	kA
<g/>
,	,	kIx,
J.J.	J.J.	k1gFnSc1
<g/>
;	;	kIx,
M.	M.	kA
BOITANO	BOITANO	kA
<g/>
;	;	kIx,
N.	N.	kA
ARI	ARI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amateur	Amateura	k1gFnPc2
boxing	boxing	k1gInSc1
injuries	injuries	k1gMnSc1
at	at	k?
the	the	k?
1981	#num#	k4
and	and	k?
1982	#num#	k4
USA	USA	kA
<g/>
/	/	kIx~
<g/>
ABF	ABF	kA
national	nationat	k5eAaPmAgMnS,k5eAaImAgMnS
championships	championships	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Phys	Phys	k1gInSc1
Sportsmed	Sportsmed	k1gInSc4
<g/>
.	.	kIx.
1984	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
123	#num#	k4
<g/>
–	–	k?
<g/>
128	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BLEDSOE	BLEDSOE	kA
<g/>
,	,	kIx,
G.H.	G.H.	k1gFnSc1
<g/>
;	;	kIx,
G.	G.	kA
LI	li	k9
<g/>
;	;	kIx,
F.	F.	kA
LEVY	LEVY	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Injury	Injura	k1gFnPc1
risk	risk	k1gInSc1
in	in	k?
professional	professionat	k5eAaPmAgInS,k5eAaImAgInS
boxing	boxing	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
South	South	k1gInSc1
Med	med	k1gInSc4
J.	J.	kA
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
994	#num#	k4
<g/>
–	–	k?
<g/>
998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
smj	smj	k?
<g/>
.0000182498	.0000182498	k4
<g/>
.19288	.19288	k4
<g/>
.	.	kIx.
<g/>
e	e	k0
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ZAZRYN	ZAZRYN	kA
<g/>
,	,	kIx,
T.	T.	kA
<g/>
R.	R.	kA
<g/>
;	;	kIx,
C.	C.	kA
<g/>
F.	F.	kA
FINCH	FINCH	kA
<g/>
;	;	kIx,
P.	P.	kA
MCCRORY	MCCRORY	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
16	#num#	k4
year	year	k1gInSc1
study	stud	k1gInPc1
of	of	k?
injuries	injuries	k1gInSc1
to	ten	k3xDgNnSc1
professional	professionat	k5eAaImAgInS,k5eAaPmAgInS
boxers	boxers	k1gInSc1
in	in	k?
the	the	k?
state	status	k1gInSc5
of	of	k?
Victoria	Victorium	k1gNnSc2
<g/>
,	,	kIx,
Australia	Australium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Br	br	k0
J	J	kA
Sports	Sports	k1gInSc1
Med	med	k1gInSc1
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
321	#num#	k4
<g/>
–	–	k?
<g/>
324	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.113	10.113	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
bjsm	bjsm	k6eAd1
<g/>
.37	.37	k4
<g/>
.4	.4	k4
<g/>
.321	.321	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ZAZRYN	ZAZRYN	kA
<g/>
,	,	kIx,
T.	T.	kA
<g/>
R.	R.	kA
<g/>
;	;	kIx,
P.	P.	kA
CAMERON	CAMERON	kA
<g/>
;	;	kIx,
P.	P.	kA
MCCRORY	MCCRORY	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
prospective	prospectiv	k1gInSc5
cohort	cohort	k1gInSc1
study	stud	k1gInPc1
of	of	k?
injury	injura	k1gFnSc2
in	in	k?
amateur	amateur	k1gMnSc1
and	and	k?
professional	professionat	k5eAaPmAgMnS,k5eAaImAgMnS
boxing	boxing	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Br	br	k0
J	J	kA
Sports	Sports	k1gInSc1
Med	med	k1gInSc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
670	#num#	k4
<g/>
–	–	k?
<g/>
674	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.113	10.113	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
bjsm	bjsm	k6eAd1
<g/>
.2006	.2006	k4
<g/>
.025924	.025924	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ZAZRYN	ZAZRYN	kA
<g/>
,	,	kIx,
T.	T.	kA
<g/>
R.	R.	kA
<g/>
;	;	kIx,
P.	P.	kA
MCCRORY	MCCRORY	kA
<g/>
;	;	kIx,
P.	P.	kA
CAMERON	CAMERON	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Injury	Injura	k1gFnSc2
rates	ratesa	k1gFnPc2
and	and	k?
risk	risk	k1gInSc1
factors	factors	k1gInSc1
in	in	k?
competitive	competitiv	k1gInSc5
professional	professionat	k5eAaPmAgMnS,k5eAaImAgMnS
boxing	boxing	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Clin	Clin	k1gInSc1
J	J	kA
Sports	Sports	k1gInSc1
Med	med	k1gInSc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
20	#num#	k4
<g/>
–	–	k?
<g/>
25	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Study	stud	k1gInPc1
of	of	k?
Fighters	Fighters	k1gInSc1
Shows	Showsa	k1gFnPc2
Brain	Brain	k1gMnSc1
Changes	Changes	k1gMnSc1
Are	ar	k1gInSc5
Seen	Seena	k1gFnPc2
Before	Befor	k1gInSc5
Symptoms	Symptoms	k1gInSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
TIMOTHY	TIMOTHY	kA
PRATT	PRATT	kA
<g/>
,	,	kIx,
April	April	k1gInSc1
24	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
'	'	kIx"
.	.	kIx.
.	.	kIx.
</s>
<s desamb="1">
This	This	k1gInSc1
is	is	k?
part	part	k1gInSc1
of	of	k?
the	the	k?
Professional	Professional	k1gFnSc3
Fighters	Fighters	k1gInSc4
Brain	Brain	k2eAgInSc4d1
Health	Health	k1gInSc4
Study	stud	k1gInPc7
<g/>
,	,	kIx,
now	now	k?
a	a	k8xC
year	year	k1gInSc1
old	old	k?
.	.	kIx.
.	.	kIx.
.	.	kIx.
.	.	kIx.
</s>
<s desamb="1">
Dr	dr	kA
<g/>
.	.	kIx.
Bernick	Bernick	k1gMnSc1
will	wilnout	k5eAaPmAgMnS
present	present	k1gInSc4
these	these	k1gFnSc2
findings	findings	k6eAd1
on	on	k3xPp3gMnSc1
Wednesday	Wednesda	k1gMnPc4
in	in	k?
New	New	k1gFnSc1
Orleans	Orleans	k1gInSc1
at	at	k?
the	the	k?
American	American	k1gMnSc1
Academy	Academa	k1gFnSc2
of	of	k?
Neurology	neurolog	k1gMnPc7
<g/>
'	'	kIx"
<g/>
s	s	k7c7
annual	annual	k1gInSc1
meeting	meeting	k1gInSc4
.	.	kIx.
.	.	kIx.
.	.	kIx.
.	.	kIx.
</s>
<s desamb="1">
Though	Though	k1gInSc1
Dr	dr	kA
<g/>
.	.	kIx.
Bernick	Bernick	k1gMnSc1
intends	intends	k6eAd1
to	ten	k3xDgNnSc1
continue	continuat	k5eAaPmIp3nS
his	his	k1gNnSc4
study	stud	k1gInPc1
of	of	k?
boxers	boxersa	k1gFnPc2
for	forum	k1gNnPc2
at	at	k?
least	least	k1gMnSc1
five	fivat	k5eAaPmIp3nS
years	years	k6eAd1
<g/>
,	,	kIx,
he	he	k0
said	saido	k1gNnPc2
the	the	k?
preliminary	preliminara	k1gFnSc2
findings	findingsa	k1gFnPc2
were	were	k1gNnSc1
worth	worth	k1gMnSc1
the	the	k?
attention	attention	k1gInSc1
of	of	k?
the	the	k?
neurology	neurolog	k1gMnPc7
association	association	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
annual	annual	k1gInSc1
meeting	meeting	k1gInSc1
<g/>
,	,	kIx,
as	as	k1gInSc1
"	"	kIx"
<g/>
nobody	noboda	k1gFnPc1
has	hasit	k5eAaImRp2nS
the	the	k?
numbers	numbers	k1gInSc4
we	we	k?
do	do	k7c2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
.	.	kIx.
.	.	kIx.
'	'	kIx"
<g/>
↑	↑	k?
MARTINEZ	MARTINEZ	kA
<g/>
,	,	kIx,
Jaime	Jaim	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formal	Formal	k1gInSc1
Autopsy	Autopsa	k1gFnSc2
on	on	k3xPp3gMnSc1
Vasquez	Vasquez	k1gMnSc1
Published	Published	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sherdog	Sherdog	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2008-04-18	2008-04-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
THOMAS	Thomas	k1gMnSc1
<g/>
,	,	kIx,
Luke	Luke	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyrone	Tyron	k1gInSc5
Mims	Mims	k1gInSc1
<g/>
,	,	kIx,
MMA	MMA	kA
Fighter	fighter	k1gMnSc1
<g/>
,	,	kIx,
Dies	Dies	k1gInSc1
Following	Following	k1gInSc1
Amateur	Amateura	k1gFnPc2
Bout	Bout	k1gInSc4
in	in	k?
South	South	k1gInSc1
Carolina	Carolina	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MMA	MMA	kA
Fighting	Fighting	k1gInSc1
<g/>
,	,	kIx,
2012-10-17	2012-10-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Moran	Morana	k1gFnPc2
<g/>
,	,	kIx,
Lee	Lea	k1gFnSc6
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
Pro	pro	k7c4
MMA	MMA	kA
fighter	fighter	k1gMnSc1
Booto	Booto	k1gNnSc4
Guylain	Guylain	k2eAgInSc1d1
dies	dies	k1gInSc1
after	aftra	k1gFnPc2
getting	getting	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
elbowed	elbowed	k1gInSc1
in	in	k?
head	head	k1gInSc1
during	during	k1gInSc1
bout	bout	k1gInSc1
in	in	k?
South	South	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
York	York	k1gInSc4
Daily	Daila	k1gFnSc2
News	News	k1gInSc1
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
retrieved	retrieved	k1gInSc1
2014-04-091	2014-04-091	k4
2	#num#	k4
SVINTH	SVINTH	kA
<g/>
,	,	kIx,
Joseph	Joseph	k1gMnSc1
R.	R.	kA
Death	Death	k1gMnSc1
under	under	k1gMnSc1
the	the	k?
Spotlight	Spotlight	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Velasquez	Velasquez	k1gMnSc1
Boxing	boxing	k1gInSc4
Fatality	fatalita	k1gFnSc2
Collection	Collection	k1gInSc4
The	The	k1gFnSc2
Data	datum	k1gNnSc2
<g/>
:	:	kIx,
MMA	MMA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Combative	Combativ	k1gInSc5
Sport	sport	k1gInSc1
<g/>
,	,	kIx,
July	Jula	k1gFnPc1
2000	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KNAPP	Knapp	k1gMnSc1
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
This	This	k1gInSc1
Day	Day	k1gFnSc2
in	in	k?
MMA	MMA	kA
History	Histor	k1gInPc1
<g/>
:	:	kIx,
Nov	nov	k1gInSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
www.sherdog.com	www.sherdog.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sherdog	Sherdog	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HALL	HALL	kA
<g/>
,	,	kIx,
Joe	Joe	k1gFnSc1
<g/>
;	;	kIx,
UTTERSTROM	UTTERSTROM	kA
<g/>
,	,	kIx,
Brad	brada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Death	Death	k1gMnSc1
of	of	k?
Douglas	Douglas	k1gInSc1
Dedge	Dedge	k1gFnSc1
<g/>
.	.	kIx.
www.sherdog.com	www.sherdog.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sherdog	Sherdog	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GUTIERREZ	GUTIERREZ	kA
<g/>
,	,	kIx,
Guillermo	Guillerma	k1gFnSc5
F.	F.	kA
Tragedia	Tragedium	k1gNnSc2
en	en	k?
la	la	k1gNnSc1
AMM	AMM	kA
<g/>
:	:	kIx,
Mike	Mike	k1gFnSc1
Mitelmeier	Mitelmeira	k1gFnPc2
está	estat	k5eAaPmIp3nS,k5eAaImIp3nS,k5eAaBmIp3nS
en	en	k?
coma	coma	k1gFnSc1
y	y	k?
su	su	k?
cuadro	cuadro	k6eAd1
se	se	k3xPyFc4
complica	complica	k6eAd1
<g/>
.	.	kIx.
www.opinion.com.bo	www.opinion.com.ba	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opinion	Opinion	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
<g/>
bo	bo	k?
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
spanish	spanish	k1gInSc1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GUTIERREZ	GUTIERREZ	kA
<g/>
,	,	kIx,
Guillermo	Guillerma	k1gFnSc5
F.	F.	kA
Una	Una	k1gFnPc1
multitud	multitud	k1gMnSc1
despidió	despidió	k?
con	con	k?
lágrimas	lágrimas	k1gInSc1
a	a	k8xC
Mike	Mike	k1gFnSc1
Mitelmeier	Mitelmeira	k1gFnPc2
<g/>
.	.	kIx.
www.opinion.com.bo	www.opinion.com.ba	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opinion	Opinion	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
<g/>
bo	bo	k?
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
spanish	spanish	k1gInSc1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
LENGERICH	LENGERICH	kA
<g/>
,	,	kIx,
Ryan	Ryan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fighter	fighter	k1gMnSc1
died	died	k1gMnSc1
from	from	k1gMnSc1
blunt	blunt	k1gInSc4
force	force	k1gFnSc2
trauma	trauma	k1gNnSc4
<g/>
,	,	kIx,
autopsy	autops	k1gInPc1
reveals	reveals	k1gInSc1
<g/>
.	.	kIx.
rapidcityjournal	rapidcityjournat	k5eAaPmAgInS,k5eAaImAgInS
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
RapidCityJournal	RapidCityJournal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Police	police	k1gFnSc2
release	release	k6eAd1
autopsy	autopsa	k1gFnSc2
results	results	k6eAd1
for	forum	k1gNnPc2
Sturgis	Sturgis	k1gFnSc1
fighter	fighter	k1gMnSc1
<g/>
.	.	kIx.
www.kotatv.com	www.kotatv.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
KOTAV	KOTAV	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MARROCCO	MARROCCO	kA
<g/>
,	,	kIx,
Steven	Steven	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autopsy	Autopsa	k1gFnSc2
reveals	revealsa	k1gFnPc2
fighter	fighter	k1gMnSc1
Dustin	Dustin	k1gMnSc1
Jenson	Jenson	k1gMnSc1
suffered	suffered	k1gMnSc1
head	head	k1gMnSc1
trauma	trauma	k1gNnSc1
prior	prior	k1gMnSc1
to	ten	k3xDgNnSc4
death	death	k1gMnSc1
<g/>
.	.	kIx.
mmajunkie	mmajunkie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
MMAJunkie	MMAJunkie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
YUEN	YUEN	kA
<g/>
,	,	kIx,
Jenny	Jenn	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
CPR	CPR	kA
performed	performed	k1gMnSc1
on	on	k3xPp3gMnSc1
dead	dead	k6eAd1
Hamilton	Hamilton	k1gInSc1
MMA	MMA	kA
fighter	fighter	k1gMnSc1
for	forum	k1gNnPc2
40	#num#	k4
minutes	minutesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Calgary	Calgary	k1gNnSc7
Sun	suna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Calgary	Calgary	k1gNnSc1
<g/>
:	:	kIx,
11	#num#	k4
April	April	k1gInSc1
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Citation	Citation	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ZIDAN	ZIDAN	kA
<g/>
,	,	kIx,
Karim	Karim	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Report	report	k1gInSc1
<g/>
:	:	kIx,
Fighter	fighter	k1gMnSc1
dies	diesa	k1gFnPc2
in	in	k?
the	the	k?
cage	cagat	k5eAaPmIp3nS
during	during	k1gInSc1
Azerbaijan	Azerbaijany	k1gInPc2
MMA	MMA	kA
tournament	tournament	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BloodyElbow	BloodyElbow	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
29	#num#	k4
March	March	k1gInSc1
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
29	#num#	k4
April	April	k1gInSc1
2015	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SCHILD	SCHILD	kA
<g/>
,	,	kIx,
Jake	Jake	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Medical	Medical	k1gMnSc1
examiner	examiner	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
office	offika	k1gFnSc3
attributes	attributes	k1gMnSc1
MMA	MMA	kA
fighter	fighter	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
death	death	k1gMnSc1
to	ten	k3xDgNnSc1
kidney	kidney	k1gInPc1
failure	failur	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seattletimes	Seattletimes	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
30	#num#	k4
April	April	k1gInSc1
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
25	#num#	k4
June	jun	k1gMnSc5
2015	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BRADLEY	BRADLEY	kA
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
MMA	MMA	kA
fighter	fighter	k1gMnSc1
Joao	Joao	k1gMnSc1
Carvalho	Carval	k1gMnSc2
dies	dies	k1gInSc1
two	two	k?
days	days	k1gInSc1
after	after	k1gMnSc1
TKO	TKO	kA
loss	loss	k6eAd1
to	ten	k3xDgNnSc4
Charlie	Charlie	k1gMnSc1
Ward	Ward	k1gMnSc1
<g/>
,	,	kIx,
Conor	Conor	k1gMnSc1
McGregor	McGregor	k1gMnSc1
tribute	tribut	k1gInSc5
<g/>
.	.	kIx.
news	wsit	k5eNaPmRp2nS
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
<g/>
au	au	k0
<g/>
.	.	kIx.
13	#num#	k4
April	April	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
19	#num#	k4
April	April	k1gInSc1
2016	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
smíšená	smíšený	k2eAgNnPc1d1
bojová	bojový	k2eAgNnPc1d1
umění	umění	k1gNnPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Český	český	k2eAgInSc1d1
web	web	k1gInSc1
o	o	k7c4
MMA	MMA	kA
extraround	extraround	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Český	český	k2eAgInSc1d1
web	web	k1gInSc1
o	o	k7c6
MMA	MMA	kA
fightnews	fightnews	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Český	český	k2eAgInSc1d1
web	web	k1gInSc1
o	o	k7c6
MMA	MMA	kA
mma	mma	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Slovenský	slovenský	k2eAgInSc1d1
web	web	k1gInSc1
o	o	k7c4
Československej	Československej	k?
MMA	MMA	kA
súťaži	súťazat	k5eAaPmIp1nS
sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
aktuality	aktualita	k1gFnPc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7619848-0	7619848-0	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
