<s>
Zde	zde	k6eAd1	zde
Rolland	Rolland	k1gMnSc1	Rolland
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1886	[number]	k4	1886
až	až	k9	až
1889	[number]	k4	1889
historii	historie	k1gFnSc4	historie
na	na	k7c6	na
elitní	elitní	k2eAgFnSc6d1	elitní
škole	škola	k1gFnSc6	škola
Ecole	Ecola	k1gFnSc3	Ecola
Normale	Normala	k1gFnSc3	Normala
Supérieure	Supérieur	k1gMnSc5	Supérieur
a	a	k8xC	a
pak	pak	k9	pak
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k6eAd1	také
zahájil	zahájit	k5eAaPmAgMnS	zahájit
svou	svůj	k3xOyFgFnSc4	svůj
literární	literární	k2eAgFnSc4d1	literární
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
