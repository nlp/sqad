<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
na	na	k7c6	na
Zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1976	[number]	k4	1976
v	v	k7c6	v
Innsbrucku	Innsbrucko	k1gNnSc6	Innsbrucko
reprezentovalo	reprezentovat	k5eAaImAgNnS	reprezentovat
77	[number]	k4	77
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
63	[number]	k4	63
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
14	[number]	k4	14
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
