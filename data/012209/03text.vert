<p>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
na	na	k7c6	na
Zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1976	[number]	k4	1976
v	v	k7c6	v
Innsbrucku	Innsbrucko	k1gNnSc6	Innsbrucko
reprezentovalo	reprezentovat	k5eAaImAgNnS	reprezentovat
77	[number]	k4	77
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
63	[number]	k4	63
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
14	[number]	k4	14
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Nejmladším	mladý	k2eAgMnSc7d3	nejmladší
účastníkem	účastník	k1gMnSc7	účastník
byla	být	k5eAaImAgFnS	být
Claudia	Claudia	k1gFnSc1	Claudia
Kristofics-Binder	Kristofics-Binder	k1gMnSc1	Kristofics-Binder
(	(	kIx(	(
<g/>
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
128	[number]	k4	128
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejstarším	starý	k2eAgNnSc6d3	nejstarší
pak	pak	k6eAd1	pak
Herbert	Herbert	k1gMnSc1	Herbert
Wachter	Wachter	k1gMnSc1	Wachter
(	(	kIx(	(
<g/>
35	[number]	k4	35
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
286	[number]	k4	286
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentanti	reprezentant	k1gMnPc1	reprezentant
vybojovali	vybojovat	k5eAaPmAgMnP	vybojovat
6	[number]	k4	6
medailí	medaile	k1gFnPc2	medaile
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
2	[number]	k4	2
zlaté	zlatý	k2eAgInPc4d1	zlatý
2	[number]	k4	2
stříbrné	stříbrný	k1gInPc4	stříbrný
a	a	k8xC	a
2	[number]	k4	2
bronzové	bronzový	k2eAgInPc1d1	bronzový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Medailisté	medailista	k1gMnPc5	medailista
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Rakousko	Rakousko	k1gNnSc1	Rakousko
na	na	k7c4	na
ZOH	ZOH	kA	ZOH
1976	[number]	k4	1976
</s>
</p>
