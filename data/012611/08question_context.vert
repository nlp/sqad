<s>
Royal	Royal	k1gInSc1	Royal
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc2	force
(	(	kIx(	(
<g/>
RAF	raf	k0	raf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Královské	královský	k2eAgNnSc1d1	královské
letectvo	letectvo	k1gNnSc1	letectvo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vojenské	vojenský	k2eAgNnSc1d1	vojenské
letectvo	letectvo	k1gNnSc1	letectvo
britských	britský	k2eAgFnPc2d1	britská
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
RAF	raf	k0	raf
==	==	k?	==
</s>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejstarší	starý	k2eAgNnSc4d3	nejstarší
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
vojenské	vojenský	k2eAgNnSc4d1	vojenské
letectvo	letectvo	k1gNnSc4	letectvo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1918	[number]	k4	1918
sloučením	sloučení	k1gNnSc7	sloučení
Royal	Royal	k1gInSc1	Royal
Flying	Flying	k1gInSc1	Flying
Corps	corps	k1gInSc1	corps
a	a	k8xC	a
Royal	Royal	k1gInSc1	Royal
Naval	navalit	k5eAaPmRp2nS	navalit
Air	Air	k1gFnPc2	Air
Service	Service	k1gFnPc4	Service
<g/>
.	.	kIx.	.
</s>

