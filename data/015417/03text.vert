<s>
Seznam	seznam	k1gInSc1
farností	farnost	k1gFnPc2
litoměřické	litoměřický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
</s>
<s>
Litoměřická	litoměřický	k2eAgFnSc1d1
diecéze	diecéze	k1gFnSc1
je	být	k5eAaImIp3nS
k	k	k7c3
prosinci	prosinec	k1gInSc3
2014	#num#	k4
územně	územně	k6eAd1
členěna	členit	k5eAaImNgFnS
na	na	k7c4
383	#num#	k4
farností	farnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Farnosti	farnost	k1gFnPc4
litoměřické	litoměřický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
někdy	někdy	k6eAd1
bývají	bývat	k5eAaImIp3nP
sdruženy	sdružen	k2eAgFnPc1d1
po	po	k7c6
několika	několik	k4yIc6
do	do	k7c2
tzv.	tzv.	kA
farních	farní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
(	(	kIx(
<g/>
kolatur	kolatura	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
spravuje	spravovat	k5eAaImIp3nS
jeden	jeden	k4xCgMnSc1
duchovní	duchovní	k2eAgMnSc1d1
správce	správce	k1gMnSc1
–	–	k?
farář	farář	k1gMnSc1
nebo	nebo	k8xC
administrátor	administrátor	k1gMnSc1
farnosti	farnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
farnosti	farnost	k1gFnPc4
připadá	připadat	k5eAaPmIp3nS,k5eAaImIp3nS
do	do	k7c2
jednoho	jeden	k4xCgInSc2
z	z	k7c2
10	#num#	k4
vikariátů	vikariát	k1gInPc2
litoměřického	litoměřický	k2eAgInSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čele	čelo	k1gNnSc6
každého	každý	k3xTgInSc2
vikariátu	vikariát	k1gInSc2
stojí	stát	k5eAaImIp3nS
okrskový	okrskový	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Historický	historický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
</s>
<s>
Do	do	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
měla	mít	k5eAaImAgFnS
litoměřická	litoměřický	k2eAgFnSc1d1
diecéze	diecéze	k1gFnSc1
450	#num#	k4
farnosti	farnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
založení	založení	k1gNnSc3
plzeňské	plzeňský	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
,	,	kIx,
některé	některý	k3yIgFnPc1
farnosti	farnost	k1gFnPc1
byly	být	k5eAaImAgFnP
připojeny	připojit	k5eAaPmNgFnP
k	k	k7c3
této	tento	k3xDgFnSc3
nové	nový	k2eAgFnSc3d1
diecézi	diecéze	k1gFnSc3
a	a	k8xC
některé	některý	k3yIgInPc1
připadly	připadnout	k5eAaPmAgInP
k	k	k7c3
pražské	pražský	k2eAgFnSc3d1
arcidiecézi	arcidiecéze	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
do	do	k7c2
litoměřické	litoměřický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
připadlo	připadnout	k5eAaPmAgNnS
pět	pět	k4xCc1
farností	farnost	k1gFnPc2
z	z	k7c2
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2012	#num#	k4
se	se	k3xPyFc4
litoměřická	litoměřický	k2eAgFnSc1d1
diecéze	diecéze	k1gFnSc1
skládala	skládat	k5eAaImAgFnS
ze	z	k7c2
437	#num#	k4
farností	farnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
litoměřické	litoměřický	k2eAgFnSc3d1
diecézi	diecéze	k1gFnSc3
byla	být	k5eAaImAgFnS
vejprtská	vejprtský	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
přiřazena	přiřazen	k2eAgFnSc1d1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2013	#num#	k4
.	.	kIx.
</s>
<s>
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2013	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
redukci	redukce	k1gFnSc3
počtu	počet	k1gInSc2
farností	farnost	k1gFnPc2
v	v	k7c6
krušnohorském	krušnohorský	k2eAgInSc6d1
vikariátu	vikariát	k1gInSc6
a	a	k8xC
k	k	k7c3
litoměřické	litoměřický	k2eAgFnSc3d1
diecézi	diecéze	k1gFnSc3
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
dekretu	dekret	k1gInSc2
Kongregace	kongregace	k1gFnSc2
pro	pro	k7c4
biskupy	biskup	k1gMnPc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
přiřazena	přiřazen	k2eAgFnSc1d1
vejprtská	vejprtský	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
z	z	k7c2
plzeňské	plzeňský	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
farností	farnost	k1gFnPc2
se	se	k3xPyFc4
v	v	k7c6
litoměřické	litoměřický	k2eAgFnSc6d1
diecézi	diecéze	k1gFnSc6
se	se	k3xPyFc4
ustálil	ustálit	k5eAaPmAgMnS
na	na	k7c6
čísle	číslo	k1gNnSc6
383	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
farností	farnost	k1gFnPc2
litoměřické	litoměřický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
</s>
<s>
FarnostTitulVikariátKanonický	FarnostTitulVikariátKanonický	k2eAgInSc1d1
vznikHistorieZachované	vznikHistorieZachovaný	k2eAgInPc4d1
matrikyOdkaz	matrikyOdkaz	k1gInSc4
na	na	k7c6
katalogMK	katalogMK	k?
ČR	ČR	kA
</s>
<s>
Albrechtice	Albrechtice	k1gFnPc1
v	v	k7c6
Jizerských	jizerský	k2eAgFnPc6d1
horách	hora	k1gFnPc6
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1851	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1784	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1787	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Arnoltice	Arnoltice	k1gFnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1647	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
z	z	k7c2
r.	r.	kA
1240	#num#	k4
</s>
<s>
od	od	k7c2
1785	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Bakov	Bakov	k1gInSc1
nad	nad	k7c7
Jizerou	Jizera	k1gFnSc7
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
velmi	velmi	k6eAd1
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1718	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Bělá	bělat	k5eAaImIp3nS
pod	pod	k7c7
Bezdězem	Bezděz	k1gInSc7
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1336	#num#	k4
</s>
<s>
velmi	velmi	k6eAd1
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1664	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Bělá	bělat	k5eAaImIp3nS
u	u	k7c2
Děčína	Děčín	k1gInSc2
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1852	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1786	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1788	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Benešov	Benešov	k1gInSc1
nad	nad	k7c7
Ploučnicí	Ploučnice	k1gFnSc7
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1384	#num#	k4
</s>
<s>
od	od	k7c2
1581	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Bezdědice	Bezdědice	k1gFnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1855	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1787	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1765	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Bezděz	Bezděz	k1gInSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
konec	konec	k1gInSc1
13	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1658	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Bezno	Bezno	k6eAd1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
1650	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Bílina	Bílina	k1gFnSc1
</s>
<s>
arciděkanství	arciděkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1934	#num#	k4
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
r.	r.	kA
1061	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
v	v	k7c6
r.	r.	kA
1216	#num#	k4
sídlo	sídlo	k1gNnSc1
arcidiakonátu	arcidiakonát	k1gInSc2
</s>
<s>
od	od	k7c2
1591	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Bílý	bílý	k2eAgInSc1d1
Kostel	kostel	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1854	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
již	již	k6eAd1
v	v	k7c6
r.	r.	kA
1400	#num#	k4
</s>
<s>
od	od	k7c2
1643	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Bitozeves	Bitozeves	k1gMnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
před	před	k7c7
r.	r.	kA
1360	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1669	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Blažim	Blažim	k?
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1656	#num#	k4
</s>
<s>
v	v	k7c6
r.	r.	kA
1186	#num#	k4
plebánie	plebánie	k1gFnSc1
řádu	řád	k1gInSc2
johanitů	johanita	k1gMnPc2
</s>
<s>
od	od	k7c2
1651	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Blíževedly	Blíževednout	k5eAaPmAgFnP
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1754	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1657	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Blšany	Blšana	k1gFnPc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
založena	založen	k2eAgFnSc1d1
před	před	k7c7
r.	r.	kA
1300	#num#	k4
</s>
<s>
od	od	k7c2
1649	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Bohosudov	Bohosudov	k1gInSc1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
1852	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1786	#num#	k4
lokálie	lokálie	k1gFnPc1
<g/>
;	;	kIx,
r.	r.	kA
1798	#num#	k4
proboštství	proboštství	k1gNnPc2
</s>
<s>
od	od	k7c2
1787	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Bohušovice	Bohušovice	k1gFnSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
před	před	k7c7
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
inkorporována	inkorporován	k2eAgFnSc1d1
Strahovu	Strahov	k1gInSc3
</s>
<s>
od	od	k7c2
1715	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Bořejov	Bořejov	k1gInSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1725	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1699	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Bořislav	Bořislav	k1gMnSc1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
před	před	k7c7
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1748	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Boseň	Boseň	k1gFnSc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1687	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1687	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Bošín	Bošín	k1gMnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1862	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1788	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Bozkov	Bozkov	k1gInSc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1747	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1344	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1695	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Brenná	Brenný	k2eAgFnSc1d1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1856	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
,	,	kIx,
již	již	k6eAd1
před	před	k7c7
r.	r.	kA
1360	#num#	k4
</s>
<s>
od	od	k7c2
1660	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Brniště	Brniště	k1gNnSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1700	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
z	z	k7c2
r.	r.	kA
1350	#num#	k4
</s>
<s>
od	od	k7c2
1657	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Brozany	Brozana	k1gFnPc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
cca	cca	kA
r.	r.	kA
1100	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1664	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Brtníky	brtník	k1gMnPc4
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1716	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1675	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Březina	Březina	k1gMnSc1
nad	nad	k7c7
Jizerou	Jizera	k1gFnSc7
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1856	#num#	k4
</s>
<s>
r.	r.	kA
1344	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
r.	r.	kA
1787	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1787	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Březno	Březno	k1gNnSc1
u	u	k7c2
Mladé	mladý	k2eAgFnSc2d1
Boleslavi	Boleslaev	k1gFnSc6
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1747	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1710	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Břvany	Břvana	k1gFnPc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1856	#num#	k4
</s>
<s>
kolem	kolem	k7c2
r.	r.	kA
1200	#num#	k4
benediktinské	benediktinský	k2eAgNnSc4d1
proboštství	proboštství	k1gNnSc4
<g/>
;	;	kIx,
r.	r.	kA
1787	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1785	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Bukovno	Bukovna	k1gFnSc5
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1858	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1773	#num#	k4
expozitura	expozitura	k1gFnSc1
</s>
<s>
od	od	k7c2
1775	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Bulovka	Bulovka	k1gFnSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1741	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1741	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Buškovice	Buškovice	k1gFnPc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
farnost	farnost	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
kolem	kolem	k7c2
r.	r.	kA
1200	#num#	k4
</s>
<s>
od	od	k7c2
1636	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Býčkovice	Býčkovice	k1gFnSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
ve	v	k7c6
12	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
komenda	komenda	k1gFnSc1
premonstrátů	premonstrát	k1gMnPc2
z	z	k7c2
Teplé	Teplá	k1gFnSc2
</s>
<s>
od	od	k7c2
1657	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Bystřice	Bystřice	k1gFnSc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1870	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
ze	z	k7c2
14	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Bzí	bzít	k5eAaImIp3nS
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejstarších	starý	k2eAgFnPc2d3
farností	farnost	k1gFnPc2
</s>
<s>
od	od	k7c2
1669	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Cínovec	Cínovec	k1gInSc1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
1728	#num#	k4
</s>
<s>
od	od	k7c2
1827	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Církvice	Církvice	k1gFnSc1
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
1850	#num#	k4
</s>
<s>
od	od	k7c2
1700	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Cítoliby	Cítoliba	k1gFnPc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
1663	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Cvikov	Cvikov	k1gInSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejstarších	starý	k2eAgFnPc2d3
farností	farnost	k1gFnPc2
<g/>
,	,	kIx,
vznikla	vzniknout	k5eAaPmAgFnS
před	před	k7c4
r.	r.	kA
1250	#num#	k4
</s>
<s>
od	od	k7c2
1650	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Čečelice	Čečelice	k1gFnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1369	#num#	k4
</s>
<s>
od	od	k7c2
1729	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Čermná	Čermný	k2eAgFnSc1d1
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
1858	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
již	již	k6eAd1
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
</s>
<s>
od	od	k7c2
1664	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Česká	český	k2eAgFnSc1d1
Kamenice	Kamenice	k1gFnSc1
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1517	#num#	k4
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
1630	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
–	–	k?
in	in	k?
suburbio	suburbio	k6eAd1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1787	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
zřízena	zřízen	k2eAgFnSc1d1
při	při	k7c6
augustiniánském	augustiniánský	k2eAgInSc6d1
klášteře	klášter	k1gInSc6
</s>
<s>
od	od	k7c2
1787	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
–	–	k?
in	in	k?
urbe	urbat	k5eAaPmIp3nS
</s>
<s>
děkanství	děkanství	k1gNnSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
starobylé	starobylý	k2eAgNnSc4d1
děkanství	děkanství	k1gNnSc4
<g/>
,	,	kIx,
zřízené	zřízený	k2eAgNnSc4d1
kolem	kolo	k1gNnSc7
r.	r.	kA
1200	#num#	k4
</s>
<s>
od	od	k7c2
1585	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Český	český	k2eAgInSc1d1
Bukov	Bukov	k1gInSc1
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
1739	#num#	k4
</s>
<s>
podle	podle	k7c2
záznamu	záznam	k1gInSc2
z	z	k7c2
r.	r.	kA
1376	#num#	k4
v	v	k7c6
místě	místo	k1gNnSc6
fara	fara	k1gFnSc1
</s>
<s>
od	od	k7c2
1673	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Český	český	k2eAgInSc1d1
Dub	dub	k1gInSc1
</s>
<s>
děkanství	děkanství	k1gNnSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
1676	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Čížkovice	Čížkovice	k1gFnSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
před	před	k7c7
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1679	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Desná	Desnat	k5eAaPmIp3nS
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1905	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1903	#num#	k4
expozitura	expozitura	k1gFnSc1
</s>
<s>
od	od	k7c2
1903	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Deštná	deštný	k2eAgFnSc1d1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1891	#num#	k4
</s>
<s>
lokálie	lokálie	k1gFnSc1
neznámo	neznámo	k6eAd1
od	od	k7c2
kdy	kdy	k6eAd1
</s>
<s>
od	od	k7c2
1669	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Děčín	Děčín	k1gInSc1
I	i	k9
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1702	#num#	k4
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
existuje	existovat	k5eAaImIp3nS
již	již	k6eAd1
v	v	k7c6
11	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
</s>
<s>
od	od	k7c2
1596	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Děčín-Podmokly	Děčín-Podmokl	k1gInPc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1702	#num#	k4
</s>
<s>
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1596	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Dětřichov	Dětřichov	k1gInSc1
u	u	k7c2
Frýdlantu	Frýdlant	k1gInSc2
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1866	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1786	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Dlažkovice	Dlažkovice	k1gFnSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
do	do	k7c2
r.	r.	kA
1679	#num#	k4
filiálka	filiálka	k1gFnSc1
Třebenic	Třebenice	k1gFnPc2
</s>
<s>
od	od	k7c2
1680	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Dlouhý	dlouhý	k2eAgInSc1d1
Most	most	k1gInSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1849	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1787	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1787	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Dobranov	Dobranov	k1gInSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1392	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1736	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Dobrovice	Dobrovice	k1gFnPc1
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1766	#num#	k4
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
1673	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Doksany	Doksana	k1gFnPc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1782	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
zřízena	zřízen	k2eAgFnSc1d1
po	po	k7c6
zrušení	zrušení	k1gNnSc6
kláštera	klášter	k1gInSc2
v	v	k7c6
r.	r.	kA
1782	#num#	k4
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Doksy	Doksy	k1gInPc1
</s>
<s>
děkanství	děkanství	k1gNnSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1711	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Dolánky	Dolánka	k1gFnPc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
před	před	k7c7
r.	r.	kA
1352	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1657	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Dolní	dolní	k2eAgInSc1d1
Bousov	Bousov	k1gInSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1753	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1753	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1
Krupá	Krupý	k2eAgFnSc1d1
u	u	k7c2
Mnichova	mnichův	k2eAgNnSc2d1
Hradiště	Hradiště	k1gNnSc2
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1767	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Dolní	dolní	k2eAgNnSc1d1
Podluží	Podluží	k1gNnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1860	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
;	;	kIx,
r.	r.	kA
1611	#num#	k4
připojena	připojit	k5eAaPmNgFnS
k	k	k7c3
Jiřetínu	Jiřetín	k1gMnSc3
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1
Poustevna	poustevna	k1gFnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1852	#num#	k4
</s>
<s>
od	od	k7c2
1760	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Dolní	dolní	k2eAgInSc1d1
Ročov	Ročov	k1gInSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1675	#num#	k4
</s>
<s>
předtím	předtím	k6eAd1
plebánie	plebánie	k1gFnSc1
august	august	k1gMnSc1
<g/>
.	.	kIx.
kláštera	klášter	k1gInSc2
</s>
<s>
od	od	k7c2
1654	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Dolní	dolní	k2eAgNnSc1d1
Slivno	Slivno	k1gNnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
1694	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Dolní	dolní	k2eAgInSc1d1
Žleb	žleb	k1gInSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1853	#num#	k4
</s>
<s>
r.	r.	kA
1784	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1780	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Domoušice	Domoušice	k1gFnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
1664	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Doubice	Doubice	k1gFnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1810	#num#	k4
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Držkov	Držkov	k1gInSc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1651	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Dubá	Dubat	k5eAaPmIp3nS,k5eAaBmIp3nS,k5eAaImIp3nS
</s>
<s>
děkanství	děkanství	k1gNnSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1652	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Dubí	dubí	k1gNnSc1
u	u	k7c2
Teplic	Teplice	k1gFnPc2
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
expozitura	expozitura	k1gFnSc1
od	od	k7c2
r.	r.	kA
1940	#num#	k4
k	k	k7c3
farnosti	farnost	k1gFnSc3
Novosedlice	Novosedlice	k1gFnSc2
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Dubnice	Dubnice	k1gFnSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1858	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1363	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
od	od	k7c2
r.	r.	kA
1797	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1656	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Duchcov	Duchcov	k1gInSc1
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1605	#num#	k4
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
r.	r.	kA
1369	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1647	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Frýdlant	Frýdlant	k1gInSc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1624	#num#	k4
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1661	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Fukov	Fukov	k1gInSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1853	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1785	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1787	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Habartice	Habartice	k1gFnSc1
u	u	k7c2
Krupky	krupka	k1gFnSc2
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
do	do	k7c2
r.	r.	kA
1702	#num#	k4
filiální	filiální	k2eAgFnSc2d1
k	k	k7c3
Chlumci	Chlumec	k1gInSc3
</s>
<s>
od	od	k7c2
1677	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Hejnice	Hejnice	k1gFnSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1787	#num#	k4
</s>
<s>
od	od	k7c2
1772	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Hlavice	hlavice	k1gFnSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1705	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
z	z	k7c2
neznámého	známý	k2eNgInSc2d1
roku	rok	k1gInSc2
</s>
<s>
od	od	k7c2
1772	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Hodkovice	Hodkovice	k1gFnSc1
nad	nad	k7c7
Mohelkou	Mohelka	k1gFnSc7
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
z	z	k7c2
neznámého	známý	k2eNgInSc2d1
roku	rok	k1gInSc2
</s>
<s>
od	od	k7c2
1668	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Holany	Holan	k1gMnPc4
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1754	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
z	z	k7c2
neznámého	známý	k2eNgInSc2d1
roku	rok	k1gInSc2
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
<g/>
stol.	stol.	k?
</s>
<s>
od	od	k7c2
1672	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Holedeček	Holedeček	k1gInSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1785	#num#	k4
</s>
<s>
před	před	k7c7
r.	r.	kA
1346	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1785	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Homole	homole	k1gFnSc1
u	u	k7c2
Panny	Panna	k1gFnSc2
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1852	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1786	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1787	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Horky	horka	k1gFnPc1
nad	nad	k7c7
Jizerou	Jizera	k1gFnSc7
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
farnost	farnost	k1gFnSc1
existovala	existovat	k5eAaImAgFnS
již	již	k9
r.	r.	kA
1383	#num#	k4
(	(	kIx(
<g/>
Brodce	Brodka	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
od	od	k7c2
1655	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Horní	horní	k2eAgFnSc1d1
Habartice	Habartice	k1gFnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1858	#num#	k4
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Horní	horní	k2eAgInSc1d1
Jiřetín	Jiřetín	k1gInSc1
</s>
<s>
krušnohorský	krušnohorský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
před	před	k7c7
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1639	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Horní	horní	k2eAgFnSc1d1
Libchava	Libchava	k1gFnSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
připomínaná	připomínaný	k2eAgFnSc1d1
již	již	k6eAd1
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
</s>
<s>
od	od	k7c2
1691	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Horní	horní	k2eAgFnSc1d1
Police	police	k1gFnSc1
</s>
<s>
arciděkanství	arciděkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1723	#num#	k4
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
připomínaná	připomínaný	k2eAgFnSc1d1
již	již	k6eAd1
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
</s>
<s>
od	od	k7c2
1623	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Horní	horní	k2eAgInSc1d1
Prysk	prysk	k1gInSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1852	#num#	k4
</s>
<s>
od	od	k7c2
1671	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Horní	horní	k2eAgInSc1d1
Ročov	Ročov	k1gInSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1898	#num#	k4
</s>
<s>
od	od	k7c2
1760	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Horní	horní	k2eAgFnSc1d1
Vidim	Vidim	k?
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1722	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
před	před	k7c7
r.	r.	kA
1400	#num#	k4
</s>
<s>
od	od	k7c2
1661	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Hořetice	Hořetika	k1gFnSc3
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1858	#num#	k4
</s>
<s>
před	před	k7c7
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1656	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Hoštka	Hoštka	k1gFnSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
od	od	k7c2
17	#num#	k4
<g/>
.	.	kIx.
<g/>
stol.	stol.	k?
opět	opět	k6eAd1
samostatná	samostatný	k2eAgFnSc1d1
</s>
<s>
od	od	k7c2
1666	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Hrádek	hrádek	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1286	#num#	k4
</s>
<s>
od	od	k7c2
1785	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Hradiště	Hradiště	k1gNnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1756	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1380	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
od	od	k7c2
r.	r.	kA
1703	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1656	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Hrob	hrob	k1gInSc1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
1859	#num#	k4
</s>
<s>
po	po	k7c6
r.	r.	kA
1382	#num#	k4
zřízena	zřízen	k2eAgFnSc1d1
plebánie	plebánie	k1gFnSc1
<g/>
;	;	kIx,
od	od	k7c2
r.	r.	kA
1786	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1651	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Hrubá	hrubý	k2eAgFnSc1d1
Skála	skála	k1gFnSc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1854	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1802	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Hrubý	hrubý	k2eAgInSc1d1
Jeseník	Jeseník	k1gInSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1668	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Hrušovany	Hrušovany	k1gInPc1
u	u	k7c2
Litoměřic	Litoměřice	k1gInPc2
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1901	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
z	z	k7c2
r.	r.	kA
1400	#num#	k4
</s>
<s>
od	od	k7c2
1686	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Hřensko	Hřensko	k1gNnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1786	#num#	k4
</s>
<s>
od	od	k7c2
1785	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Hřivice	Hřivice	k1gFnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1858	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Huntířov	Huntířov	k1gInSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1725	#num#	k4
</s>
<s>
od	od	k7c2
1602	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Chabařovice	Chabařovice	k1gFnPc1
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
1714	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1654	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Chcebuz	Chcebuz	k1gMnSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1690	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
po	po	k7c6
reformaci	reformace	k1gFnSc6
k	k	k7c3
farnosti	farnost	k1gFnSc3
Štětí	štětit	k5eAaImIp3nS
</s>
<s>
od	od	k7c2
1690	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Chlumec	Chlumec	k1gInSc1
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1636	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Chomutov	Chomutov	k1gInSc1
</s>
<s>
děkanství	děkanství	k1gNnSc1
</s>
<s>
krušnohorský	krušnohorský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
farnost	farnost	k1gFnSc1
od	od	k7c2
13	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
</s>
<s>
od	od	k7c2
1606	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Chorušice	Chorušice	k1gFnSc1
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1760	#num#	k4
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
1659	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Chotěšov	Chotěšov	k1gInSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1852	#num#	k4
</s>
<s>
od	od	k7c2
1773	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Chotětov	Chotětov	k1gInSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1369	#num#	k4
</s>
<s>
od	od	k7c2
1651	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Chožov	Chožov	k1gInSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1684	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1693	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Chrastava	Chrastava	k1gFnSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1650	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Chřibská	chřibský	k2eAgFnSc1d1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1649	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1892	#num#	k4
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1737	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1356	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1730	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Jablonec	Jablonec	k1gInSc1
u	u	k7c2
Mimoně	mimoň	k1gMnSc2
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1849	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1786	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1735	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Jablonné	Jablonná	k1gFnPc1
v	v	k7c6
Podještědí	Podještědí	k1gNnSc6
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1350	#num#	k4
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1900	#num#	k4
</s>
<s>
od	od	k7c2
1623	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Janov	Janov	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1800	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1732	#num#	k4
administratura	administratura	k1gFnSc1
</s>
<s>
od	od	k7c2
1727	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Javory	javor	k1gInPc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1853	#num#	k4
</s>
<s>
od	od	k7c2
1785	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Jedlka	Jedlka	k1gFnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1675	#num#	k4
</s>
<s>
velmi	velmi	k6eAd1
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
,	,	kIx,
asi	asi	k9
z	z	k7c2
r.	r.	kA
1234	#num#	k4
</s>
<s>
od	od	k7c2
1672	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Jeníkov	Jeníkov	k1gInSc1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
farnost	farnost	k1gFnSc1
založena	založen	k2eAgFnSc1d1
před	před	k7c7
r.	r.	kA
1253	#num#	k4
<g/>
;	;	kIx,
patřila	patřit	k5eAaImAgFnS
Oseku	Osek	k1gInSc6
</s>
<s>
od	od	k7c2
1853	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Jeníšovice	Jeníšovice	k1gFnSc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1728	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1344	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1728	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Jenišův	Jenišův	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
1859	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1808	#num#	k4
lokálie	lokálie	k1gFnSc2
<g/>
;	;	kIx,
patřila	patřit	k5eAaImAgFnS
Oseku	Osek	k1gInSc6
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Jeřmanice	Jeřmanice	k1gFnSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1853	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1787	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1787	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Jestřebí	Jestřebit	k5eAaPmIp3nS
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1786	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
;	;	kIx,
r.	r.	kA
1624	#num#	k4
filiální	filiální	k2eAgInPc1d1
k	k	k7c3
faře	fara	k1gFnSc3
v	v	k7c6
Pavlovicích	Pavlovice	k1gFnPc6
</s>
<s>
od	od	k7c2
1681	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Jetřichovice	Jetřichovice	k1gFnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1787	#num#	k4
</s>
<s>
od	od	k7c2
1752	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Jezvé	Jezvá	k1gFnPc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1650	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
ze	z	k7c2
14	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
</s>
<s>
od	od	k7c2
1683	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Jílové	Jílové	k1gNnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1849	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
<g/>
,	,	kIx,
r.	r.	kA
1832	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1669	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Jindřichovice	Jindřichovice	k1gFnSc1
pod	pod	k7c7
Smrkem	smrk	k1gInSc7
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1755	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1771	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Jirkov	Jirkov	k1gInSc1
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1783	#num#	k4
</s>
<s>
krušnohorský	krušnohorský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1645	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Jiřetín	Jiřetín	k1gInSc1
pod	pod	k7c7
Jedlovou	jedlový	k2eAgFnSc7d1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1611	#num#	k4
</s>
<s>
od	od	k7c2
1650	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Jiříkov	Jiříkov	k1gInSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1664	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1665	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Jítrava	Jítrava	k1gFnSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1778	#num#	k4
</s>
<s>
od	od	k7c2
1734	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Josefův	Josefův	k2eAgInSc1d1
Důl	důl	k1gInSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1865	#num#	k4
</s>
<s>
od	od	k7c2
1866	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Kadaň	Kadaň	k1gFnSc1
</s>
<s>
děkanství	děkanství	k1gNnSc1
</s>
<s>
krušnohorský	krušnohorský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
staré	starý	k2eAgNnSc1d1
děkanství	děkanství	k1gNnSc1
</s>
<s>
od	od	k7c2
1558	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Kadlín	Kadlín	k1gInSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1725	#num#	k4
</s>
<s>
od	od	k7c2
1690	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Kamenický	kamenický	k2eAgInSc1d1
Šenov	Šenov	k1gInSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1725	#num#	k4
</s>
<s>
od	od	k7c2
1715	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Kerhartice	Kerhartice	k1gFnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1853	#num#	k4
</s>
<s>
od	od	k7c2
1707	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Klapý	Klapý	k2eAgInSc1d1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1852	#num#	k4
</s>
<s>
od	od	k7c2
1773	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Klášterec	Klášterec	k1gInSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
</s>
<s>
krušnohorský	krušnohorský	k2eAgInSc1d1
</s>
<s>
1384	#num#	k4
</s>
<s>
od	od	k7c2
1712	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Klokočské	Klokočský	k2eAgFnPc1d1
Loučky	loučka	k1gFnPc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1856	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1793	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1793	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Kněžice	kněžice	k1gFnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1649	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Kněžmost	Kněžmost	k1gFnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1865	#num#	k4
</s>
<s>
od	od	k7c2
1849	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Konojedy	Konojed	k1gMnPc4
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1786	#num#	k4
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Kosmonosy	Kosmonosy	k1gInPc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1652	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Kostomlaty	Kostomle	k1gNnPc7
pod	pod	k7c7
Milešovkou	Milešovka	k1gFnSc7
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
1765	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1352	#num#	k4
plebánie	plebánie	k1gFnSc2
<g/>
,	,	kIx,
r.	r.	kA
1724	#num#	k4
administratura	administratura	k1gFnSc1
</s>
<s>
od	od	k7c2
1664	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Košťany	Košťan	k1gMnPc4
u	u	k7c2
Teplic	Teplice	k1gFnPc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
expozitura	expozitura	k1gFnSc1
<g/>
,	,	kIx,
zřízena	zřízen	k2eAgFnSc1d1
r.	r.	kA
1940	#num#	k4
</s>
<s>
od	od	k7c2
1879	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Koštice	Koštice	k1gFnSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1856	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1787	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1787	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Kováň	Kováň	k1gFnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1723	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
;	;	kIx,
za	za	k7c2
husitských	husitský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
zničena	zničit	k5eAaPmNgFnS
</s>
<s>
od	od	k7c2
1723	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Kozly	Kozel	k1gMnPc4
u	u	k7c2
Loun	Louny	k1gInPc2
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1717	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1707	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Království	království	k1gNnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1848	#num#	k4
</s>
<s>
od	od	k7c2
1848	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Krásná	krásný	k2eAgFnSc1d1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1782	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1760	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1783	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Krásná	krásný	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
u	u	k7c2
Rumburka	Rumburk	k1gInSc2
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1724	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
od	od	k7c2
r.	r.	kA
1361	#num#	k4
</s>
<s>
od	od	k7c2
1651	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Krásný	krásný	k2eAgInSc1d1
Les	les	k1gInSc1
(	(	kIx(
<g/>
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
před	před	k7c7
r.	r.	kA
1360	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1649	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Krásný	krásný	k2eAgInSc1d1
Les	les	k1gInSc1
(	(	kIx(
<g/>
Liberec	Liberec	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1855	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
;	;	kIx,
filiálka	filiálka	k1gFnSc1
fary	fara	k1gFnSc2
frýdlantské	frýdlantský	k2eAgFnSc2d1
<g/>
;	;	kIx,
od	od	k7c2
r.	r.	kA
1781	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1739	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Kravaře	Kravaře	k1gInPc1
u	u	k7c2
České	český	k2eAgFnSc2d1
Lípy	lípa	k1gFnSc2
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1636	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Krnsko	Krnsko	k6eAd1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1764	#num#	k4
</s>
<s>
fara	fara	k1gFnSc1
existovala	existovat	k5eAaImAgFnS
již	již	k9
r.	r.	kA
1361	#num#	k4
</s>
<s>
od	od	k7c2
1715	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Krompach	Krompach	k1gMnSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1852	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1784	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Kruh	kruh	k1gInSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1699	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Krupka	krupka	k1gFnSc1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
velmi	velmi	k6eAd1
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1624	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Kryry	Kryra	k1gFnPc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1740	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
z	z	k7c2
r.	r.	kA
1418	#num#	k4
<g/>
;	;	kIx,
r.	r.	kA
1664	#num#	k4
připojena	připojit	k5eAaPmNgFnS
k	k	k7c3
f.	f.	k?
Nepomyšl	Nepomyšla	k1gFnPc2
</s>
<s>
od	od	k7c2
1664	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Kryštofovo	Kryštofův	k2eAgNnSc1d1
Údolí	údolí	k1gNnSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1768	#num#	k4
</s>
<s>
od	od	k7c2
1673	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Křemýž	Křemýž	k6eAd1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
1706	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1352	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1681	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Křesín	Křesín	k1gMnSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1852	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
<g/>
,	,	kIx,
r.	r.	kA
1781	#num#	k4
expozitura	expozitura	k1gFnSc1
</s>
<s>
od	od	k7c2
1773	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Křešice	Křešice	k1gInPc1
u	u	k7c2
Litoměřic	Litoměřice	k1gInPc2
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1756	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
ze	z	k7c2
14	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
</s>
<s>
od	od	k7c2
1663	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Křinec	Křinec	k1gMnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1758	#num#	k4
</s>
<s>
od	od	k7c2
1660	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Křižany	Křižana	k1gFnPc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1727	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
již	již	k6eAd1
před	před	k7c7
r.	r.	kA
1352	#num#	k4
</s>
<s>
od	od	k7c2
1651	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Kunratice	Kunratice	k1gFnPc1
u	u	k7c2
Cvikova	Cvikov	k1gInSc2
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1851	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1366	#num#	k4
plebánie	plebánie	k1gFnSc2
<g/>
,	,	kIx,
r.	r.	kA
1784	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Kuřívody	Kuřívod	k1gInPc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1724	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
z	z	k7c2
neznámého	známý	k2eNgInSc2d1
roku	rok	k1gInSc2
</s>
<s>
od	od	k7c2
1708	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Kvítkov	Kvítkov	k1gInSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1853	#num#	k4
</s>
<s>
kdysi	kdysi	k6eAd1
plebánie	plebánie	k1gFnSc1
<g/>
,	,	kIx,
od	od	k7c2
r.	r.	kA
1786	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1771	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Kytlice	kytlice	k1gFnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1849	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1786	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1782	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Ledvice	Ledvice	k1gFnSc1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
1898	#num#	k4
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Lenešice	Lenešice	k1gFnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
1670	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Letov	Letov	k1gInSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1772	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
,	,	kIx,
kolem	kolem	k7c2
r.	r.	kA
1600	#num#	k4
</s>
<s>
od	od	k7c2
1714	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Levín	Levín	k1gMnSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1773	#num#	k4
</s>
<s>
1169	#num#	k4
řádu	řád	k1gInSc2
johanitů	johanita	k1gMnPc2
<g/>
;	;	kIx,
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
fil.	fil.	k?
kostel	kostel	k1gInSc1
Liběšic	Liběšice	k1gFnPc2
</s>
<s>
od	od	k7c2
1686	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Libáň	Libáň	k1gFnSc1
</s>
<s>
děkanství	děkanství	k1gNnSc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1370	#num#	k4
</s>
<s>
od	od	k7c2
1665	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Libčeves	Libčeves	k1gMnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
r.	r.	kA
1352	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
pozd	pozd	k1gInSc1
<g/>
.	.	kIx.
fara	fara	k1gFnSc1
husitská	husitský	k2eAgFnSc1d1
<g/>
;	;	kIx,
r.	r.	kA
1661	#num#	k4
katol	katol	k1gInSc4
<g/>
.	.	kIx.
farář	farář	k1gMnSc1
</s>
<s>
od	od	k7c2
1661	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Libědice	Libědice	k1gFnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1628	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
exist	exist	k1gFnSc1
<g/>
.	.	kIx.
již	již	k6eAd1
před	před	k7c7
rokem	rok	k1gInSc7
1226	#num#	k4
</s>
<s>
od	od	k7c2
1594	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Liběchov	Liběchov	k1gInSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1732	#num#	k4
</s>
<s>
od	od	k7c2
1656	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Liberec	Liberec	k1gInSc1
</s>
<s>
arciděkanství	arciděkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1879	#num#	k4
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
farnost	farnost	k1gFnSc1
již	již	k6eAd1
před	před	k7c7
r.	r.	kA
1359	#num#	k4
<g/>
;	;	kIx,
od	od	k7c2
r.	r.	kA
1730	#num#	k4
děkanství	děkanství	k1gNnPc2
</s>
<s>
od	od	k7c2
1625	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Liberec-Rochlice	Liberec-Rochlice	k1gFnSc1
</s>
<s>
děkanství	děkanství	k1gNnSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1652	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
,	,	kIx,
existovala	existovat	k5eAaImAgFnS
již	již	k9
r.	r.	kA
1384	#num#	k4
</s>
<s>
od	od	k7c2
1735	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Liberec-Ruprechtice	Liberec-Ruprechtice	k1gFnSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1911	#num#	k4
</s>
<s>
od	od	k7c2
1799	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Liběšice	Liběšice	k1gInPc1
u	u	k7c2
Litoměřic	Litoměřice	k1gInPc2
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
plebánie	plebánie	k1gFnSc1
1384	#num#	k4
<g/>
;	;	kIx,
pozd	pozd	k1gInSc1
<g/>
.	.	kIx.
filiálka	filiálka	k1gFnSc1
v	v	k7c6
Levíně	Levína	k1gFnSc6
<g/>
;	;	kIx,
po	po	k7c6
r.	r.	kA
1773	#num#	k4
levínská	levínský	k2eAgFnSc1d1
expozitura	expozitura	k1gFnSc1
</s>
<s>
od	od	k7c2
1683	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Liběšice	Liběšice	k1gFnSc1
u	u	k7c2
Žatce	Žatec	k1gInSc2
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
zřízena	zřízen	k2eAgFnSc1d1
před	před	k7c7
r.	r.	kA
1359	#num#	k4
<g/>
;	;	kIx,
od	od	k7c2
hus	husa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
válek	válka	k1gFnPc2
do	do	k7c2
r.	r.	kA
1644	#num#	k4
vývoj	vývoj	k1gInSc1
neznámý	známý	k2eNgInSc1d1
</s>
<s>
od	od	k7c2
1644	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Liblice	Liblice	k1gFnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1670	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
již	již	k6eAd1
před	před	k7c7
rokem	rok	k1gInSc7
1384	#num#	k4
</s>
<s>
od	od	k7c2
1670	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Libočany	Libočan	k1gMnPc4
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1772	#num#	k4
</s>
<s>
plebánie	plebánie	k1gFnSc1
před	před	k7c7
r.	r.	kA
1357	#num#	k4
</s>
<s>
od	od	k7c2
1653	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Libochovany	Libochovan	k1gMnPc4
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1864	#num#	k4
</s>
<s>
od	od	k7c2
1671	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Libochovice	Libochovice	k1gFnPc1
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1892	#num#	k4
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1660	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Libořice	Libořice	k1gFnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1673	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Libošovice	Libošovice	k1gFnSc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1861	#num#	k4
</s>
<s>
stará	stará	k1gFnSc1
far	fara	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
již	již	k6eAd1
před	před	k7c7
r.	r.	kA
1384	#num#	k4
<g/>
;	;	kIx,
plebánie	plebánie	k1gFnSc2
<g/>
;	;	kIx,
po	po	k7c6
reformaci	reformace	k1gFnSc6
k	k	k7c3
Sobotce	Sobotka	k1gFnSc3
<g/>
;	;	kIx,
r.	r.	kA
1787	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1787	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Libotenice	Libotenice	k1gFnSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1787	#num#	k4
</s>
<s>
po	po	k7c4
Bílé	bílý	k2eAgNnSc4d1
Hoře	hoře	k1gNnSc4
filiálka	filiálka	k1gFnSc1
k	k	k7c3
Dolánkám	Dolánka	k1gFnPc3
</s>
<s>
od	od	k7c2
1738	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Libouchec	Libouchec	k1gMnSc1
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
1849	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1357	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
r.	r.	kA
1832	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1654	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Libuň	Libuň	k1gFnSc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
r.	r.	kA
1344	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1680	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Lindava	Lindava	k1gFnSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1722	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1722	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Lipová	lipový	k2eAgFnSc1d1
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1926	#num#	k4
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
farnost	farnost	k1gFnSc1
již	již	k6eAd1
před	před	k7c7
r.	r.	kA
1364	#num#	k4
</s>
<s>
od	od	k7c2
1655	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Litoměřice	Litoměřice	k1gInPc1
</s>
<s>
děkanství	děkanství	k1gNnSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
staré	starý	k2eAgNnSc1d1
děkanství	děkanství	k1gNnSc1
</s>
<s>
od	od	k7c2
1626	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Litoměřice	Litoměřice	k1gInPc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1655	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
zřízena	zřídit	k5eAaPmNgFnS
současně	současně	k6eAd1
s	s	k7c7
biskupstvím	biskupství	k1gNnSc7
</s>
<s>
od	od	k7c2
1655	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Litvínov	Litvínov	k1gInSc1
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1917	#num#	k4
</s>
<s>
krušnohorský	krušnohorský	k2eAgInSc1d1
</s>
<s>
1652	#num#	k4
</s>
<s>
před	před	k7c7
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1684	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Lobendava	Lobendava	k1gFnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
r.	r.	kA
1674	#num#	k4
již	již	k6eAd1
ustavena	ustavit	k5eAaPmNgFnS
</s>
<s>
od	od	k7c2
1760	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Loučeň	Loučeň	k1gFnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1853	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1786	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1786	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Loukov	Loukov	k1gInSc1
u	u	k7c2
Mnichova	mnichův	k2eAgNnSc2d1
Hradiště	Hradiště	k1gNnSc2
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1656	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
připomínaná	připomínaný	k2eAgFnSc1d1
již	již	k6eAd1
r.	r.	kA
1344	#num#	k4
</s>
<s>
od	od	k7c2
1656	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Loukov	Loukov	k1gInSc1
u	u	k7c2
Semil	Semily	k1gInPc2
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1849	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
f.	f.	k?
<g/>
;	;	kIx,
po	po	k7c4
třicet	třicet	k4xCc4
<g/>
.	.	kIx.
<g/>
v.	v.	k?
pod	pod	k7c4
faru	fara	k1gFnSc4
v	v	k7c6
Semilech	Semily	k1gInPc6
<g/>
;	;	kIx,
r.	r.	kA
1759	#num#	k4
residenční	residenční	k2eAgInSc4d1
kaplanství	kaplanství	k1gNnSc1
</s>
<s>
od	od	k7c2
1695	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Loukovec	Loukovec	k1gMnSc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1700	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1662	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Louny	Louny	k1gInPc1
</s>
<s>
děkanství	děkanství	k1gNnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
1687	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Lovosice	Lovosice	k1gInPc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
před	před	k7c7
r.	r.	kA
1248	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1629	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Lučany	Lučan	k1gMnPc4
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1898	#num#	k4
</s>
<s>
od	od	k7c2
1803	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Luštěnice	Luštěnice	k1gFnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1750	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
z	z	k7c2
neznámého	známý	k2eNgInSc2d1
roku	rok	k1gInSc2
</s>
<s>
od	od	k7c2
1698	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Malá	malý	k2eAgFnSc1d1
Bukovina	Bukovina	k1gFnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1722	#num#	k4
</s>
<s>
od	od	k7c2
1723	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Mariánské	mariánský	k2eAgFnPc1d1
Radčice	Radčice	k1gFnPc1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
inkorporována	inkorporován	k2eAgFnSc1d1
oseckému	osecký	k2eAgMnSc3d1
klášteru	klášter	k1gInSc3
</s>
<s>
od	od	k7c2
1606	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Markvartice	Markvartice	k1gFnSc1
u	u	k7c2
Děčína	Děčín	k1gInSc2
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1628	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Markvartice	Markvartice	k1gFnSc1
u	u	k7c2
Sobotky	Sobotka	k1gFnSc2
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1753	#num#	k4
</s>
<s>
stará	stará	k1gFnSc1
f.	f.	k?
z	z	k7c2
r.	r.	kA
1359	#num#	k4
<g/>
;	;	kIx,
od	od	k7c2
r.	r.	kA
1624	#num#	k4
připojena	připojit	k5eAaPmNgFnS
k	k	k7c3
Sobotce	Sobotka	k1gFnSc3
</s>
<s>
od	od	k7c2
1743	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Mařenice	Mařenice	k1gFnSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1708	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
již	již	k6eAd1
před	před	k7c7
r.	r.	kA
1372	#num#	k4
</s>
<s>
od	od	k7c2
1721	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Mašťov	Mašťov	k1gInSc1
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1782	#num#	k4
</s>
<s>
krušnohorský	krušnohorský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
,	,	kIx,
již	již	k6eAd1
před	před	k7c7
r.	r.	kA
1192	#num#	k4
</s>
<s>
od	od	k7c2
1624	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Mcely	Mcela	k1gFnPc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1784	#num#	k4
</s>
<s>
od	od	k7c2
1759	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Medonosy	Medonosa	k1gFnPc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1855	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1801	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Merboltice	Merboltice	k1gFnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1855	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
r.	r.	kA
1787	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1785	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Měcholupy	Měcholupa	k1gFnPc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1690	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1366	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1690	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Mělník	Mělník	k1gInSc1
</s>
<s>
proboštství	proboštství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1896	#num#	k4
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
velmi	velmi	k6eAd1
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
;	;	kIx,
od	od	k7c2
r.	r.	kA
1622	#num#	k4
děkanství	děkanství	k1gNnPc2
</s>
<s>
od	od	k7c2
1623	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Mělník-Pšovka	Mělník-Pšovka	k1gFnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1792	#num#	k4
</s>
<s>
od	od	k7c2
1614	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Měrunice	Měrunice	k1gFnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1661	#num#	k4
</s>
<s>
r.	r.	kA
1352	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1661	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Mikulášovice	Mikulášovice	k1gFnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1780	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Mikulov	Mikulov	k1gInSc1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
1858	#num#	k4
</s>
<s>
fara	fara	k1gFnSc1
před	před	k7c7
r.	r.	kA
1552	#num#	k4
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Milešov	Milešov	k1gInSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1680	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1358	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1680	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Mimoň	mimoň	k1gMnSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
velmi	velmi	k6eAd1
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
,	,	kIx,
pravděpod	pravděpod	k1gInSc1
<g/>
.	.	kIx.
již	již	k6eAd1
ve	v	k7c6
12	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
</s>
<s>
od	od	k7c2
1616	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Minice	Minice	k1gFnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1708	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1361	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1681	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
</s>
<s>
arciděkanství	arciděkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1941	#num#	k4
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
fara	fara	k1gFnSc1
existovala	existovat	k5eAaImAgFnS
již	již	k6eAd1
v	v	k7c6
r.	r.	kA
1369	#num#	k4
</s>
<s>
od	od	k7c2
1616	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Mladějov	Mladějov	k1gInSc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1740	#num#	k4
</s>
<s>
f.	f.	k?
již	již	k6eAd1
před	před	k7c7
r.	r.	kA
1383	#num#	k4
<g/>
;	;	kIx,
poté	poté	k6eAd1
připojena	připojen	k2eAgFnSc1d1
k	k	k7c3
Sobotce	Sobotka	k1gFnSc3
</s>
<s>
od	od	k7c2
1740	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Mnichovo	mnichův	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1691	#num#	k4
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1627	#num#	k4
</s>
<s>
od	od	k7c2
1683	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Mníšek	mníšek	k1gMnSc1
u	u	k7c2
Liberce	Liberec	k1gInSc2
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1767	#num#	k4
</s>
<s>
f.	f.	k?
existovala	existovat	k5eAaImAgFnS
již	již	k6eAd1
před	před	k7c7
reformací	reformace	k1gFnSc7
<g/>
;	;	kIx,
poté	poté	k6eAd1
filiální	filiální	k2eAgFnSc2d1
k	k	k7c3
frýdlantské	frýdlantský	k2eAgFnSc3d1
faře	fara	k1gFnSc3
</s>
<s>
od	od	k7c2
1699	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Modlany	Modlana	k1gFnPc1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
1853	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
r.	r.	kA
1786	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1685	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Mojžíř	Mojžíř	k1gFnSc1
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
1851	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1365	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
od	od	k7c2
r.	r.	kA
1787	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1785	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Moldava	Moldava	k1gFnSc1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
r.	r.	kA
1346	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1667	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Mory	mora	k1gFnPc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1646	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
již	již	k6eAd1
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
</s>
<s>
od	od	k7c2
1669	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Most	most	k1gInSc1
</s>
<s>
děkanství	děkanství	k1gNnSc1
</s>
<s>
krušnohorský	krušnohorský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
fara	fara	k1gFnSc1
založena	založen	k2eAgFnSc1d1
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
r.	r.	kA
1000	#num#	k4
<g/>
;	;	kIx,
děkanství	děkanství	k1gNnSc4
asi	asi	k9
od	od	k7c2
r.	r.	kA
1500	#num#	k4
</s>
<s>
od	od	k7c2
1575	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Mrzlice	Mrzlice	k1gFnSc1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
1725	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1352	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
r.	r.	kA
1676	#num#	k4
administratura	administratura	k1gFnSc1
</s>
<s>
od	od	k7c2
1676	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Mšeno	Mšeno	k6eAd1
u	u	k7c2
Mělníka	Mělník	k1gInSc2
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
velmi	velmi	k6eAd1
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
od	od	k7c2
r.	r.	kA
1080	#num#	k4
</s>
<s>
od	od	k7c2
1635	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Mukařov	Mukařov	k1gInSc1
u	u	k7c2
Mladé	mladý	k2eAgFnSc2d1
Boleslavi	Boleslaev	k1gFnSc6
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1856	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
;	;	kIx,
pozd	pozd	k1gInSc1
<g/>
.	.	kIx.
filiálkou	filiálka	k1gFnSc7
f.	f.	k?
v	v	k7c6
Březně	Březno	k1gNnSc6
<g/>
;	;	kIx,
od	od	k7c2
r.	r.	kA
1795	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1787	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Mukařov	Mukařov	k1gInSc1
u	u	k7c2
Úštěka	Úštěek	k1gInSc2
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1786	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1683	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Nakléřov	Nakléřov	k1gInSc1
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
1852	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
r.	r.	kA
1787	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1772	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Nebočady	Nebočada	k1gFnPc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
,	,	kIx,
založena	založen	k2eAgFnSc1d1
před	před	k7c7
r.	r.	kA
1363	#num#	k4
</s>
<s>
od	od	k7c2
1703	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Nebužely	Nebužet	k5eAaPmAgFnP,k5eAaImAgFnP
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1674	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1674	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Nečemice	Nečemika	k1gFnSc3
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1857	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1355	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
r.	r.	kA
1786	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Nepomyšl	Nepomyšl	k1gMnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1662	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
založena	založit	k5eAaPmNgFnS
již	již	k6eAd1
před	před	k7c7
r.	r.	kA
1346	#num#	k4
</s>
<s>
od	od	k7c2
1742	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
u	u	k7c2
Chrastavy	Chrastavy	k?
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1673	#num#	k4
</s>
<s>
od	od	k7c2
1728	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
pod	pod	k7c7
Smrkem	smrk	k1gInSc7
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1607	#num#	k4
</s>
<s>
od	od	k7c2
1683	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Nové	Nové	k2eAgNnSc1d1
Sedlo	sedlo	k1gNnSc1
u	u	k7c2
Žatce	Žatec	k1gInSc2
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1734	#num#	k4
</s>
<s>
od	od	k7c2
1735	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Novosedlice	Novosedlice	k1gFnSc1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
1787	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1594	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Nový	nový	k2eAgInSc1d1
Bor	bor	k1gInSc1
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1928	#num#	k4
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1786	#num#	k4
</s>
<s>
od	od	k7c2
1786	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Obora	obora	k1gFnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1384	#num#	k4
</s>
<s>
obnovena	obnoven	k2eAgFnSc1d1
r.	r.	kA
1725	#num#	k4
</s>
<s>
od	od	k7c2
1682	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Okna	okno	k1gNnPc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1891	#num#	k4
</s>
<s>
velmi	velmi	k6eAd1
stará	starat	k5eAaImIp3nS
f.	f.	k?
<g/>
;	;	kIx,
pozd	pozd	k1gInSc1
<g/>
.	.	kIx.
filiálkou	filiálka	k1gFnSc7
fary	fara	k1gFnSc2
v	v	k7c6
Bělé	Bělá	k1gFnSc6
p.	p.	k?
<g/>
B.	B.	kA
a	a	k8xC
Doksech	Doksy	k1gInPc6
<g/>
;	;	kIx,
od	od	k7c2
r.	r.	kA
1788	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1685	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Opočno	Opočno	k1gNnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
r.	r.	kA
1361	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1690	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Osečná	Osečný	k2eAgFnSc1d1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1686	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
již	již	k6eAd1
před	před	k7c7
r.	r.	kA
1350	#num#	k4
</s>
<s>
od	od	k7c2
1622	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Osek	Osek	k1gInSc1
u	u	k7c2
Duchcova	Duchcův	k2eAgInSc2d1
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1917	#num#	k4
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
1623	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Osenice	osenice	k1gFnSc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1658	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Paseky	paseka	k1gFnPc1
nad	nad	k7c7
Jizerou	Jizera	k1gFnSc7
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1852	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1789	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1790	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Pavlovice	Pavlovice	k1gFnPc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1650	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1673	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Petrovice	Petrovice	k1gFnSc1
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
1785	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1748	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Plazy	plaz	k1gInPc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1877	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1786	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Počaply	Počapnout	k5eAaPmAgInP
u	u	k7c2
Terezína	Terezín	k1gInSc2
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
před	před	k7c7
r.	r.	kA
1352	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1664	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Podbořany	Podbořany	k1gInPc1
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1892	#num#	k4
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1580	#num#	k4
</s>
<s>
od	od	k7c2
1580	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Polevsko	Polevsko	k6eAd1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1718	#num#	k4
</s>
<s>
od	od	k7c2
1718	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Polubný	Polubný	k2eAgInSc1d1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1793	#num#	k4
</s>
<s>
od	od	k7c2
1793	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Postoloprty	Postoloprta	k1gFnPc1
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1740	#num#	k4
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1630	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1650	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Prackovice	Prackovice	k1gFnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1384	#num#	k4
</s>
<s>
obnovena	obnoven	k2eAgFnSc1d1
r.	r.	kA
1802	#num#	k4
</s>
<s>
od	od	k7c2
1696	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Prácheň	Prácheň	k1gFnSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1802	#num#	k4
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Proboštov	Proboštov	k1gInSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
r.	r.	kA
1363	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1674	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Předlice	Předlice	k1gFnSc1
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
1910	#num#	k4
</s>
<s>
od	od	k7c2
1818	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Přepeře	Přepera	k1gFnSc3
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1861	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1344	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
r.	r.	kA
1789	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1663	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Příchovice	Příchovice	k1gFnSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1738	#num#	k4
</s>
<s>
od	od	k7c2
1716	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Pšov	Pšov	k1gInSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1645	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Radíčeves	Radíčeves	k1gMnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1858	#num#	k4
</s>
<s>
před	před	k7c7
r.	r.	kA
1186	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1679	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Radovesice	Radovesice	k1gFnSc1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
1853	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
již	již	k6eAd1
před	před	k7c7
r.	r.	kA
1384	#num#	k4
<g/>
;	;	kIx,
r.	r.	kA
1787	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1699	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Raná	raný	k2eAgFnSc1d1
u	u	k7c2
Loun	Louny	k1gInPc2
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1857	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
z	z	k7c2
neznámého	známý	k2eNgInSc2d1
roku	rok	k1gInSc2
<g/>
;	;	kIx,
později	pozdě	k6eAd2
lokálie	lokálie	k1gFnSc1
</s>
<s>
od	od	k7c2
1756	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Raspenava	Raspenava	k1gFnSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1726	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1726	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Rejšice	Rejšice	k1gFnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1735	#num#	k4
</s>
<s>
od	od	k7c2
1736	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Robeč	Robeč	k1gMnSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1660	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1678	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Roprachtice	Roprachtice	k1gFnSc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1755	#num#	k4
</s>
<s>
r.	r.	kA
1352-1425	1352-1425	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1755	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Rovensko	Rovensko	k6eAd1
pod	pod	k7c7
Troskami	troska	k1gFnPc7
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1680	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1685	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Roztoky	roztoka	k1gFnPc1
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
1857	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
r.	r.	kA
1786	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1785	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Rožďalovice	Rožďalovice	k1gFnSc1
</s>
<s>
děkanství	děkanství	k1gNnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1682	#num#	k4
</s>
<s>
staré	starý	k2eAgNnSc1d1
děkanství	děkanství	k1gNnSc1
</s>
<s>
od	od	k7c2
1662	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Rumburk	Rumburk	k1gInSc1
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1894	#num#	k4
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
1711	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Růžová	růžový	k2eAgFnSc1d1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1786	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1658	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Rychnov	Rychnov	k1gInSc1
u	u	k7c2
Děčína	Děčín	k1gInSc2
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1787	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
po	po	k7c6
r.	r.	kA
1621	#num#	k4
filiální	filiální	k2eAgMnSc1d1
k	k	k7c3
Verneřicím	Verneřice	k1gFnPc3
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Rychnov	Rychnov	k1gInSc1
u	u	k7c2
Jablonce	Jablonec	k1gInSc2
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1686	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
založená	založený	k2eAgFnSc1d1
před	před	k7c7
r.	r.	kA
1350	#num#	k4
</s>
<s>
od	od	k7c2
1611	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Rynoltice	Rynoltice	k1gFnSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1858	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1364	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
od	od	k7c2
r.	r.	kA
1786	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1809	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Rýnovice	Rýnovice	k1gFnSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1786	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1698	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1724	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Řehlovice	Řehlovice	k1gFnSc1
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
1628	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1672	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Řepín	Řepín	k1gMnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
farnost	farnost	k1gFnSc1
již	již	k6eAd1
před	před	k7c7
r.	r.	kA
1350	#num#	k4
</s>
<s>
od	od	k7c2
1670	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Řitonice	Řitonice	k1gFnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1856	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
již	již	k6eAd1
před	před	k7c7
r.	r.	kA
1362	#num#	k4
</s>
<s>
od	od	k7c2
1787	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Semily	Semily	k1gInPc1
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1911	#num#	k4
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1794	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Skalice	Skalice	k1gFnSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1384	#num#	k4
</s>
<s>
od	od	k7c2
1666	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Skalsko	Skalsko	k6eAd1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
1697	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Skorotice	Skorotice	k1gFnSc1
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
cca	cca	kA
od	od	k7c2
r.	r.	kA
1300	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1652	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Sloup	sloup	k1gInSc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
1640	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Slunečná	slunečný	k2eAgFnSc1d1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1880	#num#	k4
</s>
<s>
od	od	k7c2
1774	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Smržovka	smržovka	k1gFnSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1732	#num#	k4
</s>
<s>
od	od	k7c2
1689	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Sněžná	sněžný	k2eAgFnSc1d1
u	u	k7c2
Krásné	krásný	k2eAgFnSc2d1
Lípy	lípa	k1gFnSc2
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1851	#num#	k4
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Soběchleby	Soběchleba	k1gFnPc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1857	#num#	k4
</s>
<s>
od	od	k7c2
1702	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Soběsuky	Soběsuk	k1gInPc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
zřízena	zřízen	k2eAgFnSc1d1
cca	cca	kA
r.	r.	kA
1180	#num#	k4
z	z	k7c2
kláštera	klášter	k1gInSc2
Waldsassen	Waldsassna	k1gFnPc2
<g/>
;	;	kIx,
asi	asi	k9
od	od	k7c2
r.	r.	kA
1638	#num#	k4
katol	katol	k1gInSc4
<g/>
.	.	kIx.
farář	farář	k1gMnSc1
</s>
<s>
od	od	k7c2
1591	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Sobotka	Sobotka	k1gMnSc1
</s>
<s>
děkanství	děkanství	k1gNnSc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
staré	starý	k2eAgNnSc1d1
děkanství	děkanství	k1gNnSc1
</s>
<s>
od	od	k7c2
1659	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Srbská	srbský	k2eAgFnSc1d1
Kamenice	Kamenice	k1gFnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1856	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
;	;	kIx,
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
filiální	filiální	k2eAgFnSc2d1
k	k	k7c3
faře	fara	k1gFnSc3
českokamenické	českokamenický	k2eAgFnSc2d1
</s>
<s>
od	od	k7c2
1712	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Staňkovice	Staňkovice	k1gFnSc1
u	u	k7c2
Žatce	Žatec	k1gInSc2
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1802	#num#	k4
</s>
<s>
před	před	k7c7
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
r.	r.	kA
1785	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1785	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Staré	Staré	k2eAgMnPc4d1
Křečany	Křečan	k1gMnPc4
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1732	#num#	k4
</s>
<s>
od	od	k7c2
1732	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Stebno	Stebna	k1gFnSc5
u	u	k7c2
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
1850	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
r.	r.	kA
1787	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1663	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Stráž	stráž	k1gFnSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1902	#num#	k4
</s>
<s>
od	od	k7c2
1771	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Stráž	stráž	k1gFnSc1
pod	pod	k7c7
Ralskem	Ralsek	k1gInSc7
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1630	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Strážiště	strážiště	k1gNnSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1786	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
po	po	k7c6
r.	r.	kA
1621	#num#	k4
filiálka	filiálka	k1gFnSc1
k	k	k7c3
Úštěku	Úštěko	k1gNnSc3
</s>
<s>
od	od	k7c2
1653	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Strenice	Strenice	k1gFnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
1640	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Strojetice	Strojetika	k1gFnSc3
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1851	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
;	;	kIx,
kolem	kolem	k7c2
r.	r.	kA
1400	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
r.	r.	kA
1786	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Stružnice	Stružnice	k1gFnSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1866	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1847	#num#	k4
expozitura	expozitura	k1gFnSc1
fary	fara	k1gFnSc2
v	v	k7c6
Jezvé	Jezvá	k1gFnSc6
</s>
<s>
od	od	k7c2
1775	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Studánka	studánka	k1gFnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1874	#num#	k4
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Stvolínky	Stvolínka	k1gFnPc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1723	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1657	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Sutom	Sutom	k1gInSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1630	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
existovala	existovat	k5eAaImAgFnS
již	již	k6eAd1
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
</s>
<s>
od	od	k7c2
1674	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Svádov	Svádov	k1gInSc1
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
1621	#num#	k4
</s>
<s>
r.	r.	kA
1188	#num#	k4
plebánie	plebánie	k1gFnSc1
řádu	řád	k1gInSc2
johanitů	johanita	k1gMnPc2
</s>
<s>
od	od	k7c2
1668	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Svébořice	Svébořice	k1gFnSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
1628	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Světec	světec	k1gMnSc1
u	u	k7c2
Bíliny	Bílina	k1gFnSc2
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
velmi	velmi	k6eAd1
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1647	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Světlá	světlat	k5eAaImIp3nS
pod	pod	k7c7
Ještědem	Ještěd	k1gInSc7
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1763	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1755	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Šemanovice	Šemanovice	k1gFnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1891	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1869	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Široké	Široké	k2eAgFnSc1d1
Třebčice	Třebčice	k1gFnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1857	#num#	k4
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Šluknov	Šluknov	k1gInSc1
</s>
<s>
arciděkanství	arciděkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1922	#num#	k4
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
jako	jako	k8xS,k8xC
farnost	farnost	k1gFnSc1
existovala	existovat	k5eAaImAgFnS
již	již	k9
r.	r.	kA
1346	#num#	k4
<g/>
;	;	kIx,
od	od	k7c2
r.	r.	kA
1722	#num#	k4
děkanství	děkanství	k1gNnPc2
</s>
<s>
od	od	k7c2
1615	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Štětí	štětit	k5eAaImIp3nS
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1650	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1651	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Šumburk	Šumburk	k1gInSc1
nad	nad	k7c7
Desnou	Desna	k1gFnSc7
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1903	#num#	k4
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Tanvald	Tanvald	k6eAd1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1851	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1838	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1838	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Tatobity	Tatobita	k1gFnPc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1858	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1789	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Těchlovice	Těchlovice	k1gFnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1873	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1785	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1674	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Teplice	Teplice	k1gFnPc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
</s>
<s>
děkanství	děkanství	k1gNnSc1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
staré	starý	k2eAgNnSc1d1
děkanství	děkanství	k1gNnSc1
</s>
<s>
od	od	k7c2
1635	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Teplice-Šanov	Teplice-Šanov	k1gInSc1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
1893	#num#	k4
</s>
<s>
od	od	k7c2
1792	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Teplice-Trnovany	Teplice-Trnovan	k1gMnPc4
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
1940	#num#	k4
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Terezín	Terezín	k1gInSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1842	#num#	k4
</s>
<s>
od	od	k7c2
1810	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Tisá	Tisat	k5eAaPmIp3nS,k5eAaImIp3nS,k5eAaBmIp3nS
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
1848	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1786	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Touchořiny	Touchořina	k1gFnPc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1854	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1788	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1788	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Trmice	Trmice	k1gFnSc1
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1641	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Třebenice	Třebenice	k1gFnSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
1671	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Třebívlice	Třebívlice	k1gFnSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1787	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1716	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Třebušín	Třebušín	k1gMnSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1712	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
již	již	k6eAd1
před	před	k7c7
r.	r.	kA
1384	#num#	k4
</s>
<s>
od	od	k7c2
1662	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Tuhaň	Tuhanit	k5eAaPmRp2nS
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1724	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
;	;	kIx,
po	po	k7c6
třicetileté	třicetiletý	k2eAgFnSc6d1
válce	válka	k1gFnSc6
filiálkou	filiálka	k1gFnSc7
fary	fara	k1gFnSc2
v	v	k7c6
Doksech	Doksy	k1gInPc6
</s>
<s>
od	od	k7c2
1669	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Turnov	Turnov	k1gInSc1
</s>
<s>
děkanství	děkanství	k1gNnSc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
staré	starý	k2eAgNnSc4d1
děkanství	děkanství	k1gNnSc4
ze	z	k7c2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
od	od	k7c2
1699	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Újezd	Újezd	k1gInSc1
pod	pod	k7c7
Troskami	troska	k1gFnPc7
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1874	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1344	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
arciděkanství	arciděkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1928	#num#	k4
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
kolem	kolem	k7c2
r.	r.	kA
1000	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1579	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Ústí	ústí	k1gNnSc1
nad	nad	k7c4
Labem-Krásné	Labem-Krásný	k2eAgNnSc4d1
Březno	Březno	k1gNnSc4
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
1897	#num#	k4
</s>
<s>
nová	nový	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
;	;	kIx,
obsazena	obsazen	k2eAgFnSc1d1
r.	r.	kA
1899	#num#	k4
</s>
<s>
od	od	k7c2
1788	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Ústí	ústí	k1gNnSc1
nad	nad	k7c4
Labem-Střekov	Labem-Střekov	k1gInSc4
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
1904	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1904	#num#	k4
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Novosedlice	Novosedlice	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
r.	r.	kA
1921	#num#	k4
Střekov	Střekov	k1gInSc4
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Úštěk	Úštěk	k6eAd1
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1942	#num#	k4
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
před	před	k7c7
r.	r.	kA
1200	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1623	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Václavice	Václavice	k1gFnSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1854	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
již	již	k9
od	od	k7c2
r.	r.	kA
1326	#num#	k4
<g/>
;	;	kIx,
od	od	k7c2
r.	r.	kA
1788	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1771	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Valkeřice	Valkeřice	k1gFnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1610	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Varnsdorf	Varnsdorf	k1gInSc1
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1894	#num#	k4
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
farnost	farnost	k1gFnSc1
exist	exist	k1gFnSc1
<g/>
.	.	kIx.
již	již	k6eAd1
před	před	k7c7
r.	r.	kA
1384	#num#	k4
<g/>
;	;	kIx,
znovuzřízena	znovuzřídit	k5eAaPmNgFnS
před	před	k7c4
r.	r.	kA
1714	#num#	k4
</s>
<s>
od	od	k7c2
1630	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Vejprty	Vejprta	k1gFnPc1
</s>
<s>
krušnohorský	krušnohorský	k2eAgInSc1d1
</s>
<s>
1553	#num#	k4
</s>
<s>
k	k	k7c3
litoměřické	litoměřický	k2eAgFnSc3d1
diecézi	diecéze	k1gFnSc3
byla	být	k5eAaImAgFnS
farnost	farnost	k1gFnSc1
přiřazena	přiřadit	k5eAaPmNgFnS
r.	r.	kA
2013	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Velemín	Velemín	k1gMnSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1849	#num#	k4
</s>
<s>
od	od	k7c2
cca	cca	kA
r.	r.	kA
1230	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
r.	r.	kA
1841	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1702	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Velenice	Velenice	k1gFnPc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1764	#num#	k4
</s>
<s>
od	od	k7c2
1759	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Černoc	Černoc	k1gFnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1805	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
existovala	existovat	k5eAaImAgFnS
již	již	k6eAd1
kolem	kolem	k7c2
r.	r.	kA
1300	#num#	k4
</s>
<s>
od	od	k7c2
1724	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Velké	velký	k2eAgInPc1d1
Hamry	Hamry	k1gInPc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1937	#num#	k4
</s>
<s>
od	od	k7c2
1937	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Velké	velký	k2eAgNnSc1d1
Chvojno	Chvojno	k1gNnSc1
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
1832	#num#	k4
</s>
<s>
před	před	k7c7
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1651	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Velký	velký	k2eAgInSc1d1
Šenov	Šenov	k1gInSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
1782	#num#	k4
</s>
<s>
od	od	k7c2
1785	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Verneřice	Verneřice	k1gFnSc1
</s>
<s>
děčínský	děčínský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
po	po	k7c6
Bílé	bílý	k2eAgFnSc6d1
Hoře	hora	k1gFnSc6
obnovena	obnovit	k5eAaPmNgNnP
</s>
<s>
od	od	k7c2
1669	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Ves	ves	k1gFnSc1
u	u	k7c2
Frýdlantu	Frýdlant	k1gInSc2
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1686	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1773	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Vetlá	Vetlat	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1785	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1699	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Vidhostice	Vidhostika	k1gFnSc3
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1791	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc4
již	již	k6eAd1
r.	r.	kA
1384	#num#	k4
<g/>
;	;	kIx,
v	v	k7c6
době	doba	k1gFnSc6
reformace	reformace	k1gFnSc2
zrušena	zrušen	k2eAgFnSc1d1
<g/>
;	;	kIx,
od	od	k7c2
r.	r.	kA
1652	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Vinařice	Vinařice	k1gFnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1720	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1352	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1724	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Vítkov	Vítkov	k1gInSc1
u	u	k7c2
Chrastavy	Chrastavy	k?
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1676	#num#	k4
</s>
<s>
od	od	k7c2
1676	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Vlastibořice	Vlastibořice	k1gFnSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1855	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1393	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
r.	r.	kA
1789	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1789	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Volfartice	Volfartice	k1gFnSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
1756	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1747	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Vratislavice	Vratislavice	k1gFnSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
1764	#num#	k4
</s>
<s>
od	od	k7c2
1653	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Vroutek	Vroutek	k1gInSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1854	#num#	k4
</s>
<s>
velmi	velmi	k6eAd1
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
od	od	k7c2
r.	r.	kA
1100	#num#	k4
<g/>
;	;	kIx,
od	od	k7c2
r.	r.	kA
1716	#num#	k4
expozitura	expozitura	k1gFnSc1
</s>
<s>
od	od	k7c2
1719	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Všeborsko	Všeborsko	k6eAd1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1857	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
již	již	k9
od	od	k7c2
r.	r.	kA
1363	#num#	k4
<g/>
;	;	kIx,
později	pozdě	k6eAd2
r.	r.	kA
1765	#num#	k4
expozitura	expozitura	k1gFnSc1
</s>
<s>
od	od	k7c2
1765	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Všeň	Všeň	k1gFnSc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1688	#num#	k4
</s>
<s>
od	od	k7c2
1664	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Vtelno	Vtelno	k1gNnSc1
u	u	k7c2
Mostu	most	k1gInSc2
</s>
<s>
krušnohorský	krušnohorský	k2eAgInSc1d1
</s>
<s>
1733	#num#	k4
</s>
<s>
fara	fara	k1gFnSc1
inkorporována	inkorporován	k2eAgFnSc1d1
oseckému	osecký	k2eAgInSc3d1
klášteru	klášter	k1gInSc3
<g/>
;	;	kIx,
od	od	k7c2
r.	r.	kA
1733	#num#	k4
uváděna	uvádět	k5eAaImNgFnS
jako	jako	k9
fara	fara	k1gFnSc1
</s>
<s>
od	od	k7c2
1650	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Vyskeř	Vyskeř	k1gFnSc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1858	#num#	k4
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
;	;	kIx,
později	pozdě	k6eAd2
filiálkou	filiálka	k1gFnSc7
turnovského	turnovský	k2eAgNnSc2d1
děkanství	děkanství	k1gNnSc2
</s>
<s>
od	od	k7c2
1772	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Vysoká	vysoká	k1gFnSc1
u	u	k7c2
Mělníka	Mělník	k1gInSc2
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1725	#num#	k4
</s>
<s>
stará	starý	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
</s>
<s>
od	od	k7c2
1725	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Vysoké	vysoká	k1gFnPc1
nad	nad	k7c7
Jizerou	Jizera	k1gFnSc7
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1700	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1344	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
po	po	k7c6
třicetileté	třicetiletý	k2eAgFnSc6d1
válce	válka	k1gFnSc6
filiálkou	filiálka	k1gFnSc7
fary	fara	k1gFnSc2
v	v	k7c6
Semilech	Semily	k1gInPc6
</s>
<s>
od	od	k7c2
1695	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Záboří	Záboří	k1gNnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1862	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1786	#num#	k4
lokálie	lokálie	k1gFnSc2
</s>
<s>
od	od	k7c2
1784	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Zabrušany	Zabrušana	k1gFnPc1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
1916	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
po	po	k7c4
Bílé	bílý	k2eAgNnSc4d1
Hoře	hoře	k1gNnSc4
filiálka	filiálka	k1gFnSc1
k	k	k7c3
Duchcovu	Duchcův	k2eAgNnSc3d1
</s>
<s>
od	od	k7c2
1670	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Zahořany	Zahořan	k1gMnPc4
u	u	k7c2
Křešic	Křešice	k1gFnPc2
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
1656	#num#	k4
</s>
<s>
od	od	k7c2
1657	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Zákupy	zákup	k1gInPc1
</s>
<s>
děkanství	děkanství	k1gNnSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
1796	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
Olešnice	Olešnice	k1gFnSc1
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1769	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
již	již	k6eAd1
v	v	k7c6
r.	r.	kA
1042	#num#	k4
</s>
<s>
od	od	k7c2
1769	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Zubrnice	Zubrnice	k1gFnSc1
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
1723	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1723	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Žabokliky	Žaboklika	k1gFnPc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1669	#num#	k4
</s>
<s>
od	od	k7c2
cca	cca	kA
r.	r.	kA
1200	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1669	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Žandov	Žandov	k1gInSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
starobylá	starobylý	k2eAgFnSc1d1
f.	f.	k?
z	z	k7c2
doby	doba	k1gFnSc2
předhusitské	předhusitský	k2eAgFnSc2d1
<g/>
;	;	kIx,
od	od	k7c2
r.	r.	kA
1775	#num#	k4
expoziturou	expozitura	k1gFnSc7
děkanství	děkanství	k1gNnSc2
polického	polický	k2eAgNnSc2d1
</s>
<s>
od	od	k7c2
1763	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Žatec	Žatec	k1gInSc1
</s>
<s>
děkanství	děkanství	k1gNnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
zřízena	zřízen	k2eAgFnSc1d1
cca	cca	kA
r.	r.	kA
1000	#num#	k4
<g/>
;	;	kIx,
v	v	k7c6
době	doba	k1gFnSc6
utrakvistické	utrakvistický	k2eAgFnSc6d1
povýšena	povýšen	k2eAgFnSc1d1
na	na	k7c4
děkanství	děkanství	k1gNnSc4
</s>
<s>
od	od	k7c2
1616	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Želeč	Želeč	k1gMnSc1
</s>
<s>
lounský	lounský	k2eAgMnSc1d1
</s>
<s>
1622	#num#	k4
</s>
<s>
před	před	k7c7
r.	r.	kA
1360	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1685	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Želenice	Želenice	k1gFnSc1
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
r.	r.	kA
1352	#num#	k4
plebánie	plebánie	k1gFnPc1
<g/>
;	;	kIx,
po	po	k7c6
Bílé	bílý	k2eAgFnSc6d1
Hoře	hora	k1gFnSc6
obnovena	obnoven	k2eAgFnSc1d1
zřejmě	zřejmě	k6eAd1
až	až	k9
r.	r.	kA
1650	#num#	k4
</s>
<s>
od	od	k7c2
1650	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Železný	železný	k2eAgInSc1d1
Brod	Brod	k1gInSc1
</s>
<s>
děkanství	děkanství	k1gNnSc4
od	od	k7c2
r.	r.	kA
1970	#num#	k4
</s>
<s>
turnovský	turnovský	k2eAgInSc1d1
</s>
<s>
1721	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
již	již	k6eAd1
před	před	k7c7
r.	r.	kA
1456	#num#	k4
</s>
<s>
od	od	k7c2
1721	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Žerčice	Žerčice	k1gFnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1786	#num#	k4
</s>
<s>
farnost	farnost	k1gFnSc1
již	již	k6eAd1
v	v	k7c6
r.	r.	kA
1288	#num#	k4
</s>
<s>
od	od	k7c2
1786	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Žežice	Žežice	k1gFnSc1
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1669	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Žibřidice	Žibřidice	k1gFnSc1
</s>
<s>
liberecký	liberecký	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
farnost	farnost	k1gFnSc1
již	již	k6eAd1
před	před	k7c7
r.	r.	kA
1350	#num#	k4
</s>
<s>
od	od	k7c2
1651	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Žim	Žim	k?
</s>
<s>
teplický	teplický	k2eAgInSc1d1
</s>
<s>
1673	#num#	k4
nebo	nebo	k8xC
1699	#num#	k4
</s>
<s>
od	od	k7c2
r.	r.	kA
1384	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1742	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Žitenice	Žitenice	k1gFnSc1
</s>
<s>
litoměřický	litoměřický	k2eAgInSc1d1
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
od	od	k7c2
cca	cca	kA
r.	r.	kA
1200	#num#	k4
plebánie	plebánie	k1gFnSc2
</s>
<s>
od	od	k7c2
1708	#num#	k4
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Žitovlice	Žitovlice	k1gFnSc1
</s>
<s>
mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
</s>
<s>
1875	#num#	k4
</s>
<s>
neznámo	neznámo	k1gNnSc1
</s>
<s>
katalog	katalog	k1gInSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
CPO	CPO	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MACEK	Macek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katalog	katalog	k1gInSc1
litoměřické	litoměřický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
AD	ad	k7c4
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Litoměřice	Litoměřice	k1gInPc4
<g/>
:	:	kIx,
Biskupství	biskupství	k1gNnSc3
litoměřické	litoměřický	k2eAgInPc1d1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
430	#num#	k4
s.	s.	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
farností	farnost	k1gFnPc2
<g/>
,	,	kIx,
kvazifarností	kvazifarnost	k1gFnPc2
a	a	k8xC
duchovních	duchovní	k2eAgFnPc2d1
správ	správa	k1gFnPc2
v	v	k7c6
Katalogu	katalog	k1gInSc6
biskupství	biskupství	k1gNnSc2
litoměřického	litoměřický	k2eAgInSc2d1
</s>
<s>
Biskupství	biskupství	k1gNnSc1
litoměřické	litoměřický	k2eAgFnSc2d1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Diecéze	diecéze	k1gFnSc1
litoměřická	litoměřický	k2eAgFnSc1d1
</s>
<s>
Seznam	seznam	k1gInSc1
kostelů	kostel	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
v	v	k7c6
litoměřické	litoměřický	k2eAgFnSc6d1
diecézi	diecéze	k1gFnSc6
</s>
<s>
Vikariáty	vikariát	k1gInPc1
litoměřické	litoměřický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
</s>
