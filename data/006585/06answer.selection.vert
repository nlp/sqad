<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
(	(	kIx(	(
<g/>
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Napoleone	Napoleon	k1gMnSc5	Napoleon
di	di	k?	di
Buonaparte	Buonapart	k1gInSc5	Buonapart
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1769	[number]	k4	1769
v	v	k7c6	v
korsickém	korsický	k2eAgNnSc6d1	korsické
městě	město	k1gNnSc6	město
Ajaccio	Ajaccio	k6eAd1	Ajaccio
jako	jako	k8xC	jako
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
nepříliš	příliš	k6eNd1	příliš
zámožného	zámožný	k2eAgMnSc4d1	zámožný
příslušníka	příslušník	k1gMnSc4	příslušník
úřednické	úřednický	k2eAgFnSc2d1	úřednická
šlechty	šlechta	k1gFnSc2	šlechta
advokáta	advokát	k1gMnSc2	advokát
Carla	Carl	k1gMnSc2	Carl
Buonaparta	Buonapart	k1gMnSc2	Buonapart
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
Laetitie	Laetitie	k1gFnSc2	Laetitie
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
