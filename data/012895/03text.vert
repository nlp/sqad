<p>
<s>
Epigramy	epigram	k1gInPc4	epigram
je	být	k5eAaImIp3nS	být
dílo	dílo	k1gNnSc1	dílo
Karla	Karel	k1gMnSc2	Karel
Havlíčka	Havlíček	k1gMnSc2	Havlíček
Borovského	Borovský	k1gMnSc2	Borovský
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1845	[number]	k4	1845
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
-	-	kIx~	-
<g/>
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
vychovatel	vychovatel	k1gMnSc1	vychovatel
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
profesora	profesor	k1gMnSc2	profesor
Pogodina	Pogodin	k2eAgMnSc2d1	Pogodin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
satiricky	satiricky	k6eAd1	satiricky
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
dobové	dobový	k2eAgInPc4d1	dobový
společenské	společenský	k2eAgInPc4d1	společenský
poměry	poměr	k1gInPc4	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Komentuje	komentovat	k5eAaBmIp3nS	komentovat
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
pokrytecké	pokrytecký	k2eAgInPc4d1	pokrytecký
postoje	postoj	k1gInPc4	postoj
a	a	k8xC	a
zažité	zažitý	k2eAgFnPc4d1	zažitá
konvence	konvence	k1gFnPc4	konvence
<g/>
,	,	kIx,	,
otevřeně	otevřeně	k6eAd1	otevřeně
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
rozpory	rozpor	k1gInPc4	rozpor
mezi	mezi	k7c7	mezi
hlásanými	hlásaný	k2eAgFnPc7d1	hlásaná
"	"	kIx"	"
<g/>
pravdami	pravda	k1gFnPc7	pravda
<g/>
"	"	kIx"	"
a	a	k8xC	a
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
podle	podle	k7c2	podle
témat	téma	k1gNnPc2	téma
do	do	k7c2	do
5	[number]	k4	5
oddílů	oddíl	k1gInPc2	oddíl
<g/>
:	:	kIx,	:
Církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
Králi	Král	k1gMnPc7	Král
<g/>
,	,	kIx,	,
Vlasti	vlast	k1gFnPc4	vlast
<g/>
,	,	kIx,	,
Múzám	Múza	k1gFnPc3	Múza
a	a	k8xC	a
Světu	svět	k1gInSc3	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oddílu	oddíl	k1gInSc6	oddíl
Církvi	církev	k1gFnSc6	církev
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
touží	toužit	k5eAaImIp3nP	toužit
po	po	k7c6	po
majetku	majetek	k1gInSc6	majetek
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
jezuity	jezuita	k1gMnPc4	jezuita
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
nemůže	moct	k5eNaImIp3nS	moct
vystát	vystát	k5eAaPmF	vystát
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Z	z	k7c2	z
historie	historie	k1gFnSc2	historie
literatury	literatura	k1gFnSc2	literatura
české	český	k2eAgFnSc2d1	Česká
<g/>
:	:	kIx,	:
Českých	český	k2eAgFnPc2d1	Česká
knížek	knížka	k1gFnPc2	knížka
hubitelé	hubitel	k1gMnPc1	hubitel
lití	lití	k1gNnSc1	lití
<g/>
:	:	kIx,	:
/	/	kIx~	/
plesnivina	plesnivina	k1gFnSc1	plesnivina
<g/>
,	,	kIx,	,
moli	mol	k1gMnPc1	mol
<g/>
,	,	kIx,	,
jezoviti	jezoviti	k?	jezoviti
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oddílu	oddíl	k1gInSc6	oddíl
Králi	Král	k1gMnSc6	Král
si	se	k3xPyFc3	se
bere	brát	k5eAaImIp3nS	brát
na	na	k7c4	na
mušku	muška	k1gFnSc4	muška
státní	státní	k2eAgFnSc4d1	státní
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vlasti	vlast	k1gFnSc6	vlast
ironizuje	ironizovat	k5eAaImIp3nS	ironizovat
vlastenectví	vlastenectví	k1gNnSc1	vlastenectví
a	a	k8xC	a
nekritické	kritický	k2eNgNnSc1d1	nekritické
rusofilství	rusofilství	k1gNnSc1	rusofilství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oddílu	oddíl	k1gInSc6	oddíl
Múzám	Múza	k1gFnPc3	Múza
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
umělcům	umělec	k1gMnPc3	umělec
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
např.	např.	kA	např.
dílo	dílo	k1gNnSc1	dílo
Kusy	kus	k1gInPc7	kus
mého	můj	k3xOp1gInSc2	můj
srdce	srdce	k1gNnPc4	srdce
Josefa	Josef	k1gMnSc2	Josef
Kajetána	Kajetán	k1gMnSc2	Kajetán
Tyla	tyla	k1gFnSc1	tyla
shledává	shledávat	k5eAaImIp3nS	shledávat
vhodným	vhodný	k2eAgInSc7d1	vhodný
pro	pro	k7c4	pro
kuchařky	kuchařka	k1gFnPc4	kuchařka
pod	pod	k7c4	pod
koláč	koláč	k1gInSc4	koláč
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
skupina	skupina	k1gFnSc1	skupina
epigramů	epigram	k1gInPc2	epigram
Světu	svět	k1gInSc3	svět
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
různé	různý	k2eAgInPc4d1	různý
postřehy	postřeh	k1gInPc4	postřeh
o	o	k7c6	o
obecných	obecný	k2eAgFnPc6d1	obecná
lidských	lidský	k2eAgFnPc6d1	lidská
vlastnostech	vlastnost	k1gFnPc6	vlastnost
a	a	k8xC	a
nezáviděníhodném	nezáviděníhodný	k2eAgNnSc6d1	nezáviděníhodné
postavení	postavení	k1gNnSc6	postavení
satirika	satirik	k1gMnSc2	satirik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ukázky	ukázka	k1gFnPc4	ukázka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
==	==	k?	==
</s>
</p>
<p>
<s>
BOROVSKÝ	Borovský	k1gMnSc1	Borovský
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
.	.	kIx.	.
</s>
<s>
Epigramy	epigram	k1gInPc1	epigram
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
E.	E.	kA	E.
Brož	Brož	k1gMnSc1	Brož
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
62	[number]	k4	62
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Úplné	úplný	k2eAgNnSc1d1	úplné
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
dle	dle	k7c2	dle
pův	pův	k?	pův
<g/>
.	.	kIx.	.
rukopisu	rukopis	k1gInSc2	rukopis
<g/>
.	.	kIx.	.
</s>
</p>
