<s>
Jan	Jan	k1gMnSc1	Jan
Svěrák	Svěrák	k1gMnSc1	Svěrák
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1965	[number]	k4	1965
Žatec	Žatec	k1gInSc1	Žatec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Svěráka	Svěrák	k1gMnSc2	Svěrák
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Menzelem	Menzel	k1gMnSc7	Menzel
<g/>
,	,	kIx,	,
Jánem	Ján	k1gMnSc7	Ján
Kadárem	Kadár	k1gMnSc7	Kadár
a	a	k8xC	a
Elmarem	Elmar	k1gMnSc7	Elmar
Klosem	Klos	k1gMnSc7	Klos
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
jediné	jediné	k1gNnSc4	jediné
držitele	držitel	k1gMnSc4	držitel
Oscara	Oscar	k1gMnSc2	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
uděleného	udělený	k2eAgInSc2d1	udělený
snímkům	snímek	k1gInPc3	snímek
československé	československý	k2eAgNnSc4d1	Československé
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
české	český	k2eAgFnSc2d1	Česká
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
pražskou	pražský	k2eAgFnSc7d1	Pražská
FAMU	FAMU	kA	FAMU
<g/>
,	,	kIx,	,
katedru	katedra	k1gFnSc4	katedra
dokumentární	dokumentární	k2eAgFnSc2d1	dokumentární
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
studentských	studentský	k2eAgInPc2d1	studentský
filmů	film	k1gInPc2	film
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
Však	však	k8xC	však
su	su	k?	su
vinař	vinař	k1gMnSc1	vinař
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
odysea	odysea	k1gFnSc1	odysea
II	II	kA	II
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
fiktivní	fiktivní	k2eAgInSc4d1	fiktivní
mystifikačně-ekologický	mystifikačněkologický	k2eAgInSc4d1	mystifikačně-ekologický
dokument	dokument	k1gInSc4	dokument
Ropáci	ropák	k1gMnPc1	ropák
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
o	o	k7c6	o
zvláštním	zvláštní	k2eAgNnSc6d1	zvláštní
zvířeti	zvíře	k1gNnSc6	zvíře
ve	v	k7c6	v
zdevastované	zdevastovaný	k2eAgFnSc6d1	zdevastovaná
krajině	krajina	k1gFnSc6	krajina
Severních	severní	k2eAgFnPc2d1	severní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
dokument	dokument	k1gInSc4	dokument
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
tzv.	tzv.	kA	tzv.
studentského	studentský	k2eAgMnSc4d1	studentský
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
začal	začít	k5eAaPmAgInS	začít
točit	točit	k5eAaImF	točit
celovečerní	celovečerní	k2eAgInPc4d1	celovečerní
hrané	hraný	k2eAgInPc4d1	hraný
filmy	film	k1gInPc4	film
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgMnPc2d3	nejúspěšnější
režisérů	režisér	k1gMnPc2	režisér
své	svůj	k3xOyFgFnSc2	svůj
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
filmy	film	k1gInPc1	film
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nP	těšit
velkému	velký	k2eAgInSc3d1	velký
diváckému	divácký	k2eAgInSc3d1	divácký
úspěchu	úspěch	k1gInSc3	úspěch
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
získávají	získávat	k5eAaImIp3nP	získávat
řadu	řada	k1gFnSc4	řada
cen	cena	k1gFnPc2	cena
na	na	k7c6	na
filmových	filmový	k2eAgInPc6d1	filmový
festivalech	festival	k1gInPc6	festival
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgFnPc4d3	nejdůležitější
ceny	cena	k1gFnPc4	cena
patří	patřit	k5eAaImIp3nP	patřit
tři	tři	k4xCgMnPc1	tři
Čeští	český	k2eAgMnPc1d1	český
lvi	lev	k1gMnPc1	lev
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
<g/>
,	,	kIx,	,
Obecná	obecný	k2eAgFnSc1d1	obecná
škola	škola	k1gFnSc1	škola
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
a	a	k8xC	a
Kolja	Koljus	k1gMnSc4	Koljus
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
získal	získat	k5eAaPmAgMnS	získat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
spolumajitelů	spolumajitel	k1gMnPc2	spolumajitel
produkční	produkční	k2eAgFnSc2d1	produkční
firmy	firma	k1gFnSc2	firma
Luxor	Luxora	k1gFnPc2	Luxora
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
založil	založit	k5eAaPmAgMnS	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
produkční	produkční	k2eAgFnSc4d1	produkční
společnost	společnost	k1gFnSc4	společnost
Biograf	biograf	k1gMnSc1	biograf
Jan	Jan	k1gMnSc1	Jan
Svěrák	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
posléze	posléze	k6eAd1	posléze
produkovala	produkovat	k5eAaImAgFnS	produkovat
nejenom	nejenom	k6eAd1	nejenom
jeho	jeho	k3xOp3gInPc4	jeho
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
také	také	k9	také
filmy	film	k1gInPc1	film
jiných	jiný	k2eAgMnPc2d1	jiný
tvůrců	tvůrce	k1gMnPc2	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
koprodukovala	koprodukovat	k5eAaImAgFnS	koprodukovat
film	film	k1gInSc4	film
Alice	Alice	k1gFnSc2	Alice
Nellis	Nellis	k1gFnSc2	Nellis
Tajnosti	tajnost	k1gFnSc2	tajnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
oscarovém	oscarový	k2eAgInSc6d1	oscarový
úspěchu	úspěch	k1gInSc6	úspěch
filmu	film	k1gInSc2	film
Kolja	Koljum	k1gNnSc2	Koljum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
měl	mít	k5eAaImAgMnS	mít
Jan	Jan	k1gMnSc1	Jan
Svěrák	svěrák	k1gInSc4	svěrák
několik	několik	k4yIc1	několik
nabídek	nabídka	k1gFnPc2	nabídka
od	od	k7c2	od
amerických	americký	k2eAgMnPc2d1	americký
producentů	producent	k1gMnPc2	producent
(	(	kIx(	(
<g/>
např.	např.	kA	např.
i	i	k9	i
na	na	k7c4	na
látky	látka	k1gFnPc4	látka
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
posléze	posléze	k6eAd1	posléze
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
filmy	film	k1gInPc1	film
Pravidla	pravidlo	k1gNnPc1	pravidlo
moštárny	moštárna	k1gFnSc2	moštárna
<g/>
,	,	kIx,	,
Nevěsta	nevěsta	k1gFnSc1	nevěsta
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
a	a	k8xC	a
Čokoláda	čokoláda	k1gFnSc1	čokoláda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc4	všechen
ale	ale	k9	ale
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Plánoval	plánovat	k5eAaImAgMnS	plánovat
natočit	natočit	k5eAaBmF	natočit
anglicky	anglicky	k6eAd1	anglicky
mluvený	mluvený	k2eAgInSc4d1	mluvený
film	film	k1gInSc4	film
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
Saula	Saul	k1gMnSc2	Saul
Bellowa	Bellowum	k1gNnSc2	Bellowum
Henderson	Henderson	k1gMnSc1	Henderson
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
deště	dešť	k1gInSc2	dešť
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgMnS	napsat
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
scénář	scénář	k1gInSc1	scénář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
natáčení	natáčení	k1gNnSc2	natáčení
sešlo	sejít	k5eAaPmAgNnS	sejít
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
natočil	natočit	k5eAaBmAgMnS	natočit
díky	díky	k7c3	díky
spolupráci	spolupráce	k1gFnSc3	spolupráce
s	s	k7c7	s
britským	britský	k2eAgMnSc7d1	britský
producentem	producent	k1gMnSc7	producent
Ericem	Erice	k1gMnSc7	Erice
Abrahamem	Abraham	k1gMnSc7	Abraham
film	film	k1gInSc4	film
Tmavomodrý	tmavomodrý	k2eAgInSc1d1	tmavomodrý
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
Československých	československý	k2eAgInPc6d1	československý
letcích	letek	k1gInPc6	letek
v	v	k7c6	v
britském	britský	k2eAgNnSc6d1	Britské
Královském	královský	k2eAgNnSc6d1	královské
letectvu	letectvo	k1gNnSc6	letectvo
(	(	kIx(	(
<g/>
RAF	raf	k0	raf
<g/>
)	)	kIx)	)
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
tematicky	tematicky	k6eAd1	tematicky
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
české	český	k2eAgFnSc2d1	Česká
společnosti	společnost	k1gFnSc2	společnost
filmem	film	k1gInSc7	film
Vratné	vratný	k2eAgFnSc2d1	vratná
lahve	lahev	k1gFnSc2	lahev
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
zfilmován	zfilmovat	k5eAaPmNgInS	zfilmovat
rovněž	rovněž	k9	rovněž
podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Svěráka	Svěrák	k1gMnSc2	Svěrák
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
také	také	k9	také
ve	v	k7c6	v
filmu	film	k1gInSc6	film
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Danielou	Daniela	k1gFnSc7	Daniela
Kolářovou	Kolářová	k1gFnSc7	Kolářová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hrála	hrát	k5eAaImAgFnS	hrát
jeho	jeho	k3xOp3gFnSc4	jeho
manželku	manželka	k1gFnSc4	manželka
<g/>
,	,	kIx,	,
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
stárnoucího	stárnoucí	k2eAgMnSc2d1	stárnoucí
učitele	učitel	k1gMnSc2	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vznikal	vznikat	k5eAaImAgInS	vznikat
5	[number]	k4	5
let	léto	k1gNnPc2	léto
a	a	k8xC	a
po	po	k7c6	po
vážných	vážný	k2eAgFnPc6d1	vážná
neshodách	neshoda	k1gFnPc6	neshoda
Jana	Jan	k1gMnSc2	Jan
a	a	k8xC	a
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Svěrákových	Svěráková	k1gFnPc2	Svěráková
ohledně	ohledně	k7c2	ohledně
scénáře	scénář	k1gInSc2	scénář
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
i	i	k9	i
životopisný	životopisný	k2eAgInSc4d1	životopisný
dokument	dokument	k1gInSc4	dokument
Tatínek	tatínek	k1gMnSc1	tatínek
aneb	aneb	k?	aneb
Mramorizace	Mramorizace	k1gFnSc2	Mramorizace
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Svěráka	Svěrák	k1gMnSc2	Svěrák
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
Vratné	vratný	k2eAgFnSc2d1	vratná
lahve	lahev	k1gFnSc2	lahev
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
realizován	realizovat	k5eAaBmNgInS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
poté	poté	k6eAd1	poté
zaexperimentoval	zaexperimentovat	k5eAaPmAgInS	zaexperimentovat
loutkovým	loutkový	k2eAgInSc7d1	loutkový
<g/>
/	/	kIx~	/
<g/>
hraným	hraný	k2eAgInSc7d1	hraný
fantasy	fantas	k1gInPc1	fantas
filmem	film	k1gInSc7	film
Kuky	kuka	k1gFnSc2	kuka
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
díky	díky	k7c3	díky
výtvarné	výtvarný	k2eAgFnSc3d1	výtvarná
inspiraci	inspirace	k1gFnSc3	inspirace
Františka	František	k1gMnSc2	František
Skály	Skála	k1gMnSc2	Skála
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
autorům	autor	k1gMnPc3	autor
důležité	důležitý	k2eAgFnPc4d1	důležitá
rady	rada	k1gFnPc4	rada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
filmového	filmový	k2eAgInSc2d1	filmový
projektu	projekt	k1gInSc2	projekt
neúčastnit	účastnit	k5eNaImF	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Svěrák	Svěrák	k1gMnSc1	Svěrák
tedy	tedy	k9	tedy
film	film	k1gInSc4	film
natočil	natočit	k5eAaBmAgMnS	natočit
s	s	k7c7	s
výtvarníkem	výtvarník	k1gMnSc7	výtvarník
Jakubem	Jakub	k1gMnSc7	Jakub
Dvorským	Dvorský	k1gMnSc7	Dvorský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
realizoval	realizovat	k5eAaBmAgMnS	realizovat
hudební	hudební	k2eAgFnSc4d1	hudební
pohádku	pohádka	k1gFnSc4	pohádka
podle	podle	k7c2	podle
minioper	miniopera	k1gFnPc2	miniopera
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Svěráka	Svěrák	k1gMnSc2	Svěrák
a	a	k8xC	a
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Uhlíře	Uhlíř	k1gMnSc2	Uhlíř
(	(	kIx(	(
<g/>
Červená	červený	k2eAgFnSc1d1	červená
Karkulka	Karkulka	k1gFnSc1	Karkulka
<g/>
,	,	kIx,	,
O	o	k7c6	o
dvanácti	dvanáct	k4xCc6	dvanáct
měsíčkách	měsíček	k1gInPc6	měsíček
<g/>
,	,	kIx,	,
Šípková	Šípková	k1gFnSc1	Šípková
Růženka	Růženka	k1gFnSc1	Růženka
<g/>
)	)	kIx)	)
Tři	tři	k4xCgMnPc1	tři
bratři	bratr	k1gMnPc1	bratr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
původně	původně	k6eAd1	původně
plánoval	plánovat	k5eAaImAgMnS	plánovat
zfilmování	zfilmování	k1gNnSc4	zfilmování
knihy	kniha	k1gFnSc2	kniha
irského	irský	k2eAgMnSc2d1	irský
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
novináře	novinář	k1gMnSc2	novinář
Kevina	Kevin	k1gMnSc2	Kevin
Mahera	Mahera	k1gFnSc1	Mahera
Pole	pole	k1gFnSc1	pole
(	(	kIx(	(
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Fields	Fields	k1gInSc1	Fields
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
natáčení	natáčení	k1gNnSc1	natáčení
neuskutečnilo	uskutečnit	k5eNaPmAgNnS	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
filmové	filmový	k2eAgNnSc4d1	filmové
zpracování	zpracování	k1gNnSc4	zpracování
příběhů	příběh	k1gInPc2	příběh
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Svěráka	Svěrák	k1gMnSc2	Svěrák
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
literárně	literárně	k6eAd1	literárně
zpracovány	zpracovat	k5eAaPmNgInP	zpracovat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Po	po	k7c6	po
strništi	strniště	k1gNnSc6	strniště
bos	bos	k2eAgMnSc1d1	bos
<g/>
.	.	kIx.	.
školní	školní	k2eAgInPc4d1	školní
filmy	film	k1gInPc4	film
Sbohem	sbohem	k0	sbohem
<g/>
,	,	kIx,	,
nádražíčko	nádražíčko	k1gNnSc1	nádražíčko
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
Však	však	k8xC	však
su	su	k?	su
vinař	vinař	k1gMnSc1	vinař
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
odysea	odysea	k1gFnSc1	odysea
II	II	kA	II
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Ropáci	ropák	k1gMnPc1	ropák
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
celovečerní	celovečerní	k2eAgInPc4d1	celovečerní
hrané	hraný	k2eAgInPc4d1	hraný
filmy	film	k1gInPc4	film
Obecná	obecný	k2eAgFnSc1d1	obecná
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Jízda	jízda	k1gFnSc1	jízda
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Akumulátor	akumulátor	k1gInSc1	akumulátor
1	[number]	k4	1
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Kolja	Kolj	k1gInSc2	Kolj
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
–	–	k?	–
Český	český	k2eAgInSc4d1	český
lev	lev	k1gInSc4	lev
Tmavomodrý	tmavomodrý	k2eAgInSc1d1	tmavomodrý
svět	svět	k1gInSc1	svět
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
–	–	k?	–
Český	český	k2eAgMnSc1d1	český
lev	lev	k1gMnSc1	lev
Tatínek	tatínek	k1gMnSc1	tatínek
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Vratné	vratný	k2eAgFnSc2d1	vratná
lahve	lahev	k1gFnSc2	lahev
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
–	–	k?	–
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
Kuky	kuka	k1gFnSc2	kuka
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Tři	tři	k4xCgMnPc1	tři
bratři	bratr	k1gMnPc1	bratr
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Po	po	k7c6	po
strništi	strniště	k1gNnSc6	strniště
bos	bos	k1gMnSc1	bos
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
Sbohem	sbohem	k0	sbohem
<g/>
,	,	kIx,	,
nádražíčko	nádražíčko	k1gNnSc1	nádražíčko
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
Však	však	k8xC	však
su	su	k?	su
vinař	vinař	k1gMnSc1	vinař
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
odysea	odysea	k1gFnSc1	odysea
II	II	kA	II
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Jízda	jízda	k1gFnSc1	jízda
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Akumulátor	akumulátor	k1gInSc1	akumulátor
1	[number]	k4	1
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Kulový	kulový	k2eAgInSc1d1	kulový
blesk	blesk	k1gInSc1	blesk
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
Válka	válka	k1gFnSc1	válka
barev	barva	k1gFnPc2	barva
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
filmu	film	k1gInSc2	film
–	–	k?	–
v	v	k7c6	v
teple	teplo	k1gNnSc6	teplo
domova	domov	k1gInSc2	domov
(	(	kIx(	(
<g/>
TV	TV	kA	TV
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Sametová	sametový	k2eAgFnSc1d1	sametová
kocovina	kocovina	k1gFnSc1	kocovina
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Rebelové	rebel	k1gMnPc1	rebel
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Tmavomodrý	tmavomodrý	k2eAgInSc1d1	tmavomodrý
svět	svět	k1gInSc1	svět
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Tatínek	tatínek	k1gMnSc1	tatínek
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Román	román	k1gInSc1	román
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
