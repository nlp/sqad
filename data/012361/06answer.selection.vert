<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
nejseverněji	severně	k6eAd3	severně
žijícím	žijící	k2eAgMnSc7d1	žijící
hadem	had	k1gMnSc7	had
vyznačujícím	vyznačující	k2eAgMnSc7d1	vyznačující
se	se	k3xPyFc4	se
extrémní	extrémní	k2eAgFnSc7d1	extrémní
odolností	odolnost	k1gFnSc7	odolnost
vůči	vůči	k7c3	vůči
chladnému	chladný	k2eAgNnSc3d1	chladné
počasí	počasí	k1gNnSc3	počasí
a	a	k8xC	a
jediným	jediný	k2eAgMnSc7d1	jediný
jedovatým	jedovatý	k2eAgMnSc7d1	jedovatý
hadem	had	k1gMnSc7	had
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgInPc2d1	další
států	stát	k1gInPc2	stát
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
.	.	kIx.	.
</s>
