<s>
Dalejský	Dalejský	k2eAgInSc1d1
profil	profil	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuNárodní	infoboxuNárodní	k2eAgFnSc3d1
přírodní	přírodní	k2eAgFnSc3d1
památkaDalejský	památkaDalejský	k2eAgInSc1d1
profilIUCN	profilIUCN	k?
kategorie	kategorie	k1gFnSc1
III	III	kA
(	(	kIx(
<g/>
Přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
)	)	kIx)
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Vyhlášení	vyhlášení	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1982	#num#	k4
Vyhlásil	vyhlásit	k5eAaPmAgInS
</s>
<s>
NV	NV	kA
hl.	hl.	k?
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
Nadm	Nadm	k1gMnSc1
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
285	#num#	k4
-	-	kIx~
345	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
23,66	23,66	k4
ha	ha	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Praha	Praha	k1gFnSc1
Umístění	umístění	k1gNnSc2
</s>
<s>
Holyně	Holyně	k1gFnSc1
<g/>
,	,	kIx,
Řeporyje	Řeporyje	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
1	#num#	k4
<g/>
′	′	k?
<g/>
56,9	56,9	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
19	#num#	k4
<g/>
′	′	k?
<g/>
56,46	56,46	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Dalejský	Dalejský	k2eAgInSc1d1
profil	profil	k1gInSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc1
</s>
<s>
743	#num#	k4
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Národní	národní	k2eAgFnPc1d1
přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Dalejský	Dalejský	k2eAgInSc1d1
profil	profil	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
vyhlášen	vyhlásit	k5eAaPmNgMnS
přírodní	přírodní	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
(	(	kIx(
<g/>
III	III	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
s	s	k7c7
evidenčním	evidenční	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
743	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
byl	být	k5eAaImAgInS
status	status	k1gInSc1
území	území	k1gNnSc2
změněn	změněn	k2eAgInSc1d1
na	na	k7c4
národní	národní	k2eAgFnSc4d1
přírodní	přírodní	k2eAgFnSc4d1
památku	památka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chráněné	chráněný	k2eAgFnPc4d1
území	území	k1gNnPc1
je	být	k5eAaImIp3nS
ve	v	k7c6
správě	správa	k1gFnSc6
AOPK	AOPK	kA
ČR	ČR	kA
-	-	kIx~
Regionálního	regionální	k2eAgNnSc2d1
pracoviště	pracoviště	k1gNnSc2
Střední	střední	k2eAgFnPc1d1
Čechy	Čechy	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Předmět	předmět	k1gInSc1
ochrany	ochrana	k1gFnSc2
</s>
<s>
Důvodem	důvod	k1gInSc7
ochrany	ochrana	k1gFnSc2
je	být	k5eAaImIp3nS
geologický	geologický	k2eAgInSc4d1
profil	profil	k1gInSc4
<g/>
,	,	kIx,
sedimenty	sediment	k1gInPc4
a	a	k8xC
vulkanity	vulkanita	k1gFnPc4
ordovického	ordovický	k2eAgNnSc2d1
<g/>
,	,	kIx,
silurského	silurský	k2eAgNnSc2d1
a	a	k8xC
spodnodevonského	spodnodevonský	k2eAgNnSc2d1
stáří	stáří	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nalezneme	naleznout	k5eAaPmIp1nP,k5eAaBmIp1nP
zde	zde	k6eAd1
lokality	lokalita	k1gFnSc2
mezinárodního	mezinárodní	k2eAgInSc2d1
významu	význam	k1gInSc2
díky	díky	k7c3
paleontologickému	paleontologický	k2eAgInSc3d1
obsahu	obsah	k1gInSc3
anebo	anebo	k8xC
stratigrafickému	stratigrafický	k2eAgInSc3d1
významu	význam	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
již	již	k9
sama	sám	k3xTgFnSc1
o	o	k7c4
sobě	se	k3xPyFc3
hodnotná	hodnotný	k2eAgFnSc1d1
neživá	živý	k2eNgFnSc1d1
složka	složka	k1gFnSc1
pak	pak	k6eAd1
ovlivňuje	ovlivňovat	k5eAaImIp3nS
složku	složka	k1gFnSc4
živou	živý	k2eAgFnSc4d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
také	také	k9
předmětem	předmět	k1gInSc7
ochrany	ochrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
specifickém	specifický	k2eAgInSc6d1
skalním	skalní	k2eAgInSc6d1
podkladu	podklad	k1gInSc6
nalezneme	nalézt	k5eAaBmIp1nP,k5eAaPmIp1nP
vápnomilné	vápnomilný	k2eAgFnPc1d1
a	a	k8xC
teplomilné	teplomilný	k2eAgFnPc1d1
skalní	skalní	k2eAgFnPc1d1
stepi	step	k1gFnPc1
<g/>
,	,	kIx,
na	na	k7c6
které	který	k3yRgFnSc6,k3yIgFnSc6,k3yQgFnSc6
jsou	být	k5eAaImIp3nP
vázány	vázat	k5eAaImNgFnP
některé	některý	k3yIgFnPc1
chráněné	chráněný	k2eAgFnPc1d1
a	a	k8xC
ohrožené	ohrožený	k2eAgInPc1d1
druhy	druh	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
exkurze	exkurze	k1gFnSc1
z	z	k7c2
Řeporyj	Řeporyj	k1gInSc4
do	do	k7c2
Holyně	Holyně	k1gFnSc2
</s>
<s>
Lokalita	lokalita	k1gFnSc1
</s>
<s>
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Dalejský	Dalejský	k2eAgInSc1d1
profil	profil	k1gInSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
v	v	k7c6
Přírodním	přírodní	k2eAgInSc6d1
parku	park	k1gInSc6
Prokopské	prokopský	k2eAgNnSc1d1
a	a	k8xC
Dalejské	Dalejský	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
na	na	k7c6
jihozápadním	jihozápadní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevelké	velký	k2eNgFnPc4d1
území	území	k1gNnSc6
(	(	kIx(
<g/>
23,68	23,68	k4
ha	ha	kA
<g/>
)	)	kIx)
kopíruje	kopírovat	k5eAaImIp3nS
úsek	úsek	k1gInSc4
Dalejského	Dalejský	k2eAgInSc2d1
potoka	potok	k1gInSc2
mezi	mezi	k7c7
obcemi	obec	k1gFnPc7
Řeporyje	Řeporyje	k1gFnSc2
a	a	k8xC
Holyně	Holyně	k1gFnSc2
a	a	k8xC
má	mít	k5eAaImIp3nS
tak	tak	k6eAd1
výrazně	výrazně	k6eAd1
protažený	protažený	k2eAgInSc4d1
tvar	tvar	k1gInSc4
v	v	k7c6
západovýchodním	západovýchodní	k2eAgInSc6d1
směru	směr	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katastrálně	katastrálně	k6eAd1
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
pod	pod	k7c4
obce	obec	k1gFnPc4
Řeporyje	Řeporyje	k1gFnSc2
<g/>
,	,	kIx,
Stodůlky	stodůlka	k1gFnSc2
a	a	k8xC
Holyně	Holyně	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
stepní	stepní	k2eAgFnPc1d1
lokality	lokalita	k1gFnPc1
a	a	k8xC
skalní	skalní	k2eAgInPc1d1
odkryvy	odkryv	k1gInPc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nP
na	na	k7c6
poměrně	poměrně	k6eAd1
prudkém	prudký	k2eAgInSc6d1
svahu	svah	k1gInSc6
(	(	kIx(
<g/>
celkové	celkový	k2eAgNnSc1d1
převýšení	převýšení	k1gNnSc1
oblasti	oblast	k1gFnSc2
je	být	k5eAaImIp3nS
60	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Vzhledem	vzhledem	k7c3
k	k	k7c3
pozici	pozice	k1gFnSc3
na	na	k7c6
břehu	břeh	k1gInSc6
potoka	potok	k1gInSc2
je	být	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
odvodňována	odvodňovat	k5eAaImNgFnS
Dalejským	Dalejský	k2eAgInSc7d1
potokem	potok	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Územím	území	k1gNnSc7
prochází	procházet	k5eAaImIp3nS
naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
část	část	k1gFnSc1
cesty	cesta	k1gFnSc2
souběžně	souběžně	k6eAd1
vedená	vedený	k2eAgFnSc1d1
se	s	k7c7
zeleným	zelený	k2eAgNnSc7d1
turistickým	turistický	k2eAgNnSc7d1
značením	značení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Holyni	Holyně	k1gFnSc6
a	a	k8xC
Řeporyjích	Řeporyj	k1gInPc6
se	se	k3xPyFc4
napojuje	napojovat	k5eAaImIp3nS
na	na	k7c4
modré	modrý	k2eAgNnSc4d1
turistické	turistický	k2eAgNnSc4d1
značení	značení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Oblast	oblast	k1gFnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
středověku	středověk	k1gInSc2
silně	silně	k6eAd1
ovlivňována	ovlivňovat	k5eAaImNgFnS
člověkem	člověk	k1gMnSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
odlesněna	odlesnit	k5eAaPmNgFnS
a	a	k8xC
využívána	využívat	k5eAaPmNgFnS,k5eAaImNgFnS
pro	pro	k7c4
pastvu	pastva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazné	výrazný	k2eAgInPc1d1
byly	být	k5eAaImAgInP
i	i	k9
zásahy	zásah	k1gInPc1
do	do	k7c2
nivy	niva	k1gFnSc2
Dalejského	Dalejský	k2eAgInSc2d1
potoka	potok	k1gInSc2
(	(	kIx(
<g/>
úpravy	úprava	k1gFnPc1
koryta	koryto	k1gNnSc2
<g/>
,	,	kIx,
stavba	stavba	k1gFnSc1
mlýnů	mlýn	k1gInPc2
-	-	kIx~
např.	např.	kA
Trunečkův	Trunečkův	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ráz	ráz	k1gInSc4
krajiny	krajina	k1gFnSc2
určila	určit	k5eAaPmAgFnS
těžba	těžba	k1gFnSc1
vápenců	vápenec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
těžby	těžba	k1gFnSc2
byla	být	k5eAaImAgFnS
nejrozsáhlejší	rozsáhlý	k2eAgFnSc7d3
terénní	terénní	k2eAgFnSc7d1
úpravou	úprava	k1gFnSc7
stavba	stavba	k1gFnSc1
železnice	železnice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Necitlivým	citlivý	k2eNgNnSc7d1
narušením	narušení	k1gNnSc7
rovnováhy	rovnováha	k1gFnSc2
oblasti	oblast	k1gFnSc2
byla	být	k5eAaImAgFnS
výsadba	výsadba	k1gFnSc1
nepůvodních	původní	k2eNgFnPc2d1
dřevin	dřevina	k1gFnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
tvorby	tvorba	k1gFnSc2
zeleného	zelený	k2eAgInSc2d1
pásu	pás	k1gInSc2
kolem	kolem	k7c2
Prahy	Praha	k1gFnSc2
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
byla	být	k5eAaImAgFnS
oblast	oblast	k1gFnSc1
vyhlášena	vyhlásit	k5eAaPmNgFnS
přírodní	přírodní	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
v	v	k7c6
rámci	rámec	k1gInSc6
projektu	projekt	k1gInSc2
tvorby	tvorba	k1gFnSc2
přírodního	přírodní	k2eAgInSc2d1
parku	park	k1gInSc2
Prokopské	prokopský	k2eAgNnSc1d1
a	a	k8xC
Dalejské	Dalejský	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
byl	být	k5eAaImAgInS
status	status	k1gInSc1
území	území	k1gNnSc2
změněn	změněn	k2eAgInSc1d1
na	na	k7c4
národní	národní	k2eAgFnSc4d1
přírodní	přírodní	k2eAgFnSc4d1
památku	památka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebýt	být	k5eNaImF
intenzivního	intenzivní	k2eAgNnSc2d1
využívání	využívání	k1gNnSc2
člověkem	člověk	k1gMnSc7
<g/>
,	,	kIx,
nevznikly	vzniknout	k5eNaPmAgFnP
by	by	kYmCp3nP
ani	ani	k9
skalní	skalní	k2eAgInPc1d1
výchozy	výchoz	k1gInPc1
ani	ani	k8xC
stepní	stepní	k2eAgNnPc1d1
společenstva	společenstvo	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
nyní	nyní	k6eAd1
chráníme	chránit	k5eAaImIp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
tohoto	tento	k3xDgInSc2
rázu	ráz	k1gInSc2
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
chránit	chránit	k5eAaImF
aktivními	aktivní	k2eAgInPc7d1
zásahy	zásah	k1gInPc7
a	a	k8xC
pravidelným	pravidelný	k2eAgNnSc7d1
udržováním	udržování	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1
poměry	poměr	k1gInPc1
</s>
<s>
Geologie	geologie	k1gFnSc1
</s>
<s>
Význam	význam	k1gInSc1
Dalejského	Dalejský	k2eAgNnSc2d1
údolí	údolí	k1gNnSc2
v	v	k7c6
geologickém	geologický	k2eAgInSc6d1
výzkumu	výzkum	k1gInSc6
</s>
<s>
Názorné	názorný	k2eAgInPc4d1
profily	profil	k1gInPc4
v	v	k7c6
údolí	údolí	k1gNnSc6
Dalejského	Dalejský	k2eAgInSc2d1
potoka	potok	k1gInSc2
byly	být	k5eAaImAgFnP
základem	základ	k1gInSc7
prací	práce	k1gFnSc7
mnoha	mnoho	k4c2
přírodovědců	přírodovědec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
nejvýznamnějších	významný	k2eAgInPc2d3
lze	lze	k6eAd1
uvést	uvést	k5eAaPmF
práce	práce	k1gFnPc4
J.	J.	kA
Barranda	Barranda	k1gFnSc1
(	(	kIx(
<g/>
Défence	Défence	k1gFnSc1
des	des	k1gNnSc2
colonies	coloniesa	k1gFnPc2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Descr	Descr	k1gInSc1
<g/>
.	.	kIx.
de	de	k?
la	la	k1gNnSc4
colonie	colonie	k1gFnSc2
d	d	k?
<g/>
´	´	k?
<g/>
Archiac	Archiac	k1gInSc1
<g/>
,	,	kIx,
1870	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
J.	J.	kA
E.	E.	kA
Maara	Maara	k1gMnSc1
(	(	kIx(
<g/>
On	on	k3xPp3gMnSc1
the	the	k?
predevonians	predevonians	k1gInSc1
rocks	rocks	k1gInSc1
of	of	k?
Bohemia	bohemia	k1gFnSc1
<g/>
,	,	kIx,
1880	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
J.	J.	kA
Woldřicha	Woldřich	k1gMnSc2
(	(	kIx(
<g/>
Das	Das	k1gMnSc1
Prokopital	Prokopital	k1gMnSc1
südlich	südlich	k1gMnSc1
von	von	k1gInSc4
Prag	Praga	k1gFnPc2
<g/>
,	,	kIx,
1918	#num#	k4
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
poznatky	poznatek	k1gInPc4
z	z	k7c2
dalejských	dalejský	k2eAgInPc2d1
odkryvů	odkryv	k1gInPc2
zpracovávali	zpracovávat	k5eAaImAgMnP
F.	F.	kA
Prantl	Prantl	k1gMnPc1
(	(	kIx(
<g/>
O	o	k7c6
novém	nový	k2eAgInSc6d1
nálezu	nález	k1gInSc6
polohy	poloha	k1gFnSc2
s	s	k7c7
Aulacopleura	Aulacopleur	k1gMnSc4
konincki	koninck	k1gFnSc6
<g/>
,	,	kIx,
1936	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
B.	B.	kA
Bouček	bouček	k1gInSc1
(	(	kIx(
<g/>
Stratigrafie	stratigrafie	k1gFnSc1
siluru	silur	k1gInSc2
v	v	k7c6
dalejském	dalejský	k2eAgNnSc6d1
údolí	údolí	k1gNnSc6
u	u	k7c2
Prahy	Praha	k1gFnSc2
a	a	k8xC
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
nejbližším	blízký	k2eAgNnSc6d3
okolí	okolí	k1gNnSc6
<g/>
,	,	kIx,
1937	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
jejich	jejich	k3xOp3gFnSc4
práci	práce	k1gFnSc4
navázal	navázat	k5eAaPmAgMnS
I.	I.	kA
Chlupáč	chlupáč	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
území	území	k1gNnSc4
českého	český	k2eAgInSc2d1
Barrandienu	barrandien	k1gInSc2
proslavil	proslavit	k5eAaPmAgInS
určením	určení	k1gNnSc7
hranice	hranice	k1gFnSc2
siluru	silur	k1gInSc2
a	a	k8xC
devonu	devon	k1gInSc2
a	a	k8xC
stanovením	stanovení	k1gNnSc7
jejího	její	k3xOp3gInSc2
stratotypu	stratotyp	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Stratigrafie	stratigrafie	k1gFnSc1
</s>
<s>
Sedimenty	sediment	k1gInPc1
na	na	k7c6
území	území	k1gNnSc6
NPP	NPP	kA
Dalejský	Dalejský	k2eAgInSc4d1
profil	profil	k1gInSc4
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
období	období	k1gNnSc2
prvohor	prvohory	k1gFnPc2
od	od	k7c2
svrchního	svrchní	k2eAgInSc2d1
ordoviku	ordovik	k1gInSc2
po	po	k7c4
spodní	spodní	k2eAgInSc4d1
devon	devon	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Hranici	hranice	k1gFnSc3
mezi	mezi	k7c7
ordovikem	ordovik	k1gInSc7
a	a	k8xC
silurem	silur	k1gInSc7
vrstevní	vrstevní	k2eAgInSc1d1
sled	sled	k1gInSc1
na	na	k7c6
území	území	k1gNnSc6
Dalejského	Dalejský	k2eAgInSc2d1
profilu	profil	k1gInSc2
nezachycuje	zachycovat	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Hranice	hranice	k1gFnSc1
siluru	silur	k1gInSc2
a	a	k8xC
devonu	devon	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
spodní	spodní	k2eAgFnSc6d1
části	část	k1gFnSc6
stěny	stěna	k1gFnSc2
Černého	Černého	k2eAgInSc2d1
lomu	lom	k1gInSc2
v	v	k7c6
masivních	masivní	k2eAgInPc6d1
krinoidových	krinoidový	k2eAgInPc6d1
vápencích	vápenec	k1gInPc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stratigraficky	Stratigraficky	k6eAd1
významný	významný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
profil	profil	k1gInSc1
v	v	k7c6
lomu	lom	k1gInSc6
Mušlovka	Mušlovka	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
vybrán	vybrán	k2eAgInSc4d1
jako	jako	k8xC,k8xS
opěrný	opěrný	k2eAgInSc4d1
profil	profil	k1gInSc4
k	k	k7c3
mezinárodnímu	mezinárodní	k2eAgInSc3d1
stratotypu	stratotyp	k1gInSc3
hranice	hranice	k1gFnSc2
ludlow-přídolí	ludlow-přídolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
mezinárodní	mezinárodní	k2eAgInSc1d1
stratotyp	stratotyp	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
nedalekém	daleký	k2eNgInSc6d1
lomu	lom	k1gInSc6
Na	na	k7c6
Požárech	požár	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Litologie	litologie	k1gFnSc1
</s>
<s>
Sedimentární	sedimentární	k2eAgInSc1d1
záznam	záznam	k1gInSc1
kosovského	kosovský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
tvoří	tvořit	k5eAaImIp3nP
šedozelené	šedozelený	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
a	a	k8xC
prachovce	prachovka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Vrstevní	vrstevní	k2eAgInSc1d1
sled	sled	k1gInSc1
siluru	silur	k1gInSc2
začíná	začínat	k5eAaImIp3nS
graptolitovými	graptolitový	k2eAgFnPc7d1
břidlicemi	břidlice	k1gFnPc7
motolského	motolský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
silur	silur	k1gInSc1
tvořen	tvořit	k5eAaImNgInS
vápenci	vápenec	k1gInPc7
buď	buď	k8xC
masivními	masivní	k2eAgInPc7d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
jemněji	jemně	k6eAd2
zvrstvenými	zvrstvený	k2eAgFnPc7d1
a	a	k8xC
prokládanými	prokládaný	k2eAgFnPc7d1
jílovitými	jílovitý	k2eAgFnPc7d1
břidlicemi	břidlice	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
dně	dno	k1gNnSc6
silurského	silurský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
probíhala	probíhat	k5eAaImAgFnS
vulkanická	vulkanický	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
<g/>
,	,	kIx,
sedimenty	sediment	k1gInPc1
jsou	být	k5eAaImIp3nP
proloženy	proložen	k2eAgInPc1d1
příkrovy	příkrov	k1gInPc1
zelenavých	zelenavý	k2eAgInPc2d1
bazaltů	bazalt	k1gInPc2
(	(	kIx(
<g/>
diabasů	diabas	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
podmořských	podmořský	k2eAgInPc2d1
výbuchů	výbuch	k1gInPc2
se	se	k3xPyFc4
zde	zde	k6eAd1
také	také	k9
setkáme	setkat	k5eAaPmIp1nP
s	s	k7c7
tufitickými	tufitický	k2eAgInPc7d1
vápenci	vápenec	k1gInPc7
nebo	nebo	k8xC
břidlicemi	břidlice	k1gFnPc7
<g/>
,	,	kIx,
tzn.	tzn.	kA
s	s	k7c7
výrazným	výrazný	k2eAgInSc7d1
podílem	podíl	k1gInSc7
sopečného	sopečný	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Devon	devon	k1gInSc1
začíná	začínat	k5eAaImIp3nS
v	v	k7c6
masivních	masivní	k2eAgInPc6d1
krinoidových	krinoidový	k2eAgInPc6d1
vápencích	vápenec	k1gInPc6
<g/>
,	,	kIx,
dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
různými	různý	k2eAgInPc7d1
druhy	druh	k1gInPc7
vápenců	vápenec	k1gInPc2
(	(	kIx(
<g/>
kotýsské	kotýsská	k1gFnPc1
<g/>
,	,	kIx,
slivenecké	slivenecký	k2eAgFnPc1d1
<g/>
,	,	kIx,
loděnické	loděnický	k2eAgFnPc1d1
<g/>
,	,	kIx,
řeporyjské	řeporyjský	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
vápencích	vápenec	k1gInPc6
leží	ležet	k5eAaImIp3nP
kvartérní	kvartérní	k2eAgFnPc1d1
protorendziny	protorendzina	k1gFnPc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
nevyzrálé	vyzrálý	k2eNgFnSc2d1
vápnité	vápnitý	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
s	s	k7c7
úlomky	úlomek	k1gInPc7
matečné	matečný	k2eAgFnSc2d1
horniny	hornina	k1gFnSc2
<g/>
,	,	kIx,
ovlivněné	ovlivněný	k2eAgFnSc6d1
více	hodně	k6eAd2
podložím	podložit	k5eAaPmIp1nS
než	než	k8xS
typem	typ	k1gInSc7
klimatu	klima	k1gNnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc4
středočeských	středočeský	k2eAgNnPc2d1
prvohorních	prvohorní	k2eAgNnPc2d1
moří	moře	k1gNnPc2
</s>
<s>
Ordovická	ordovický	k2eAgNnPc1d1
moře	moře	k1gNnSc1
byla	být	k5eAaImAgFnS
spíše	spíše	k9
chladná	chladný	k2eAgFnSc1d1
s	s	k7c7
častým	častý	k2eAgNnSc7d1
kolísáním	kolísání	k1gNnSc7
teploty	teplota	k1gFnSc2
způsobeným	způsobený	k2eAgNnSc7d1
zaledněním	zalednění	k1gNnSc7
Gondwany	Gondwana	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
konci	konec	k1gInSc6
ordoviku	ordovik	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
už	už	k6eAd1
máme	mít	k5eAaImIp1nP
v	v	k7c6
oblasti	oblast	k1gFnSc6
Dalejského	Dalejský	k2eAgInSc2d1
profilu	profil	k1gInSc2
zaznamenán	zaznamenat	k5eAaPmNgInS
<g/>
,	,	kIx,
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
ústupu	ústup	k1gInSc3
moře	moře	k1gNnSc2
právě	právě	k6eAd1
v	v	k7c6
důsledku	důsledek	k1gInSc6
zalednění	zalednění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
obnově	obnova	k1gFnSc3
mělkovodních	mělkovodní	k2eAgNnPc2d1
společenstev	společenstvo	k1gNnPc2
dochází	docházet	k5eAaImIp3nS
až	až	k9
na	na	k7c6
hranici	hranice	k1gFnSc6
se	s	k7c7
silurem	silur	k1gInSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ledovce	ledovec	k1gInPc1
tají	tajit	k5eAaImIp3nP
a	a	k8xC
hladina	hladina	k1gFnSc1
opět	opět	k6eAd1
stoupá	stoupat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Začátek	začátek	k1gInSc1
siluru	silur	k1gInSc2
je	být	k5eAaImIp3nS
spojen	spojen	k2eAgInSc1d1
s	s	k7c7
globální	globální	k2eAgFnSc7d1
změnou	změna	k1gFnSc7
klimatu	klima	k1gNnSc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
jeho	jeho	k3xOp3gNnSc7
oteplením	oteplení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klimatické	klimatický	k2eAgFnPc4d1
změny	změna	k1gFnPc4
se	se	k3xPyFc4
projevují	projevovat	k5eAaImIp3nP
celosvětovou	celosvětový	k2eAgFnSc7d1
sedimentací	sedimentace	k1gFnSc7
černých	černý	k2eAgFnPc2d1
graptolitových	graptolitový	k2eAgFnPc2d1
břidlic	břidlice	k1gFnPc2
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
vzniklých	vzniklý	k2eAgInPc2d1
v	v	k7c6
anoxickém	anoxický	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
,	,	kIx,
způsobeném	způsobený	k2eAgInSc6d1
nejspíše	nejspíše	k9
zastíněním	zastínění	k1gNnPc3
řasovým	řasový	k2eAgInSc7d1
porostem	porost	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Oteplení	oteplení	k1gNnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
českého	český	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
způsobeno	způsobit	k5eAaPmNgNnS
také	také	k9
posunem	posun	k1gInSc7
naší	náš	k3xOp1gFnSc2
oblasti	oblast	k1gFnSc2
deskovou	deskový	k2eAgFnSc7d1
tektonikou	tektonika	k1gFnSc7
blíže	blízce	k6eAd2
k	k	k7c3
rovníku	rovník	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
vývoj	vývoj	k1gInSc4
siluru	silur	k1gInSc2
středních	střední	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
je	být	k5eAaImIp3nS
charakteristický	charakteristický	k2eAgInSc1d1
podmořský	podmořský	k2eAgInSc1d1
vulkanismus	vulkanismus	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
tak	tak	k6eAd1
intenzivní	intenzivní	k2eAgInSc1d1
<g/>
,	,	kIx,
že	že	k8xS
způsobil	způsobit	k5eAaPmAgMnS
změlčení	změlčení	k1gNnSc4
moře	moře	k1gNnSc2
a	a	k8xC
rozvoj	rozvoj	k1gInSc1
mělkovodních	mělkovodní	k2eAgNnPc2d1
společenstev	společenstvo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Devon	devon	k1gInSc1
svým	svůj	k3xOyFgInSc7
vývojem	vývoj	k1gInSc7
plynule	plynule	k6eAd1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
silur	silur	k1gInSc4
bez	bez	k7c2
významné	významný	k2eAgFnSc2d1
změny	změna	k1gFnSc2
ve	v	k7c6
způsobu	způsob	k1gInSc6
sedimentace	sedimentace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokračuje	pokračovat	k5eAaImIp3nS
posun	posun	k1gInSc1
směrem	směr	k1gInSc7
k	k	k7c3
rovníku	rovník	k1gInSc3
a	a	k8xC
společenstva	společenstvo	k1gNnPc4
devonu	devon	k1gInSc2
mají	mít	k5eAaImIp3nP
už	už	k6eAd1
tropický	tropický	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
středočeské	středočeský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
se	se	k3xPyFc4
sedimenty	sediment	k1gInPc7
devonu	devon	k1gInSc2
tvořily	tvořit	k5eAaImAgFnP
v	v	k7c6
širomořském	širomořský	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Placatá	placatý	k2eAgFnSc1d1
skála	skála	k1gFnSc1
</s>
<s>
Placatá	placatý	k2eAgFnSc1d1
skála	skála	k1gFnSc1
v	v	k7c6
údolí	údolí	k1gNnSc6
Dalejského	Dalejský	k2eAgInSc2d1
potoka	potok	k1gInSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Placatá	placatý	k2eAgFnSc1d1
skála	skála	k1gFnSc1
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
šedozelenými	šedozelený	k2eAgInPc7d1
pískovci	pískovec	k1gInPc7
a	a	k8xC
prachovci	prachovec	k1gInPc7
náležejícími	náležející	k2eAgInPc7d1
kosovskému	kosovský	k2eAgInSc3d1
souvrství	souvrství	k1gNnSc1
(	(	kIx(
<g/>
svrchní	svrchní	k2eAgInSc1d1
ordovik	ordovik	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
styčných	styčný	k2eAgFnPc6d1
plochách	plocha	k1gFnPc6
jsou	být	k5eAaImIp3nP
díky	dík	k1gInPc1
různé	různý	k2eAgFnSc2d1
zrnitosti	zrnitost	k1gFnSc2
dobře	dobře	k6eAd1
viditelné	viditelný	k2eAgFnSc2d1
stopy	stopa	k1gFnSc2
po	po	k7c6
lezení	lezení	k1gNnSc6
mořských	mořský	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
tzv.	tzv.	kA
ichnofosilie	ichnofosilie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Lokalita	lokalita	k1gFnSc1
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
pro	pro	k7c4
nálezy	nález	k1gInPc4
doupat	doupě	k1gNnPc2
mořských	mořský	k2eAgFnPc2d1
hvězdic	hvězdice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Název	název	k1gInSc1
skály	skála	k1gFnSc2
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
od	od	k7c2
jejího	její	k3xOp3gInSc2
vzhledu	vzhled	k1gInSc2
daného	daný	k2eAgInSc2d1
uložením	uložení	k1gNnSc7
vrstev	vrstva	k1gFnPc2
paralelně	paralelně	k6eAd1
se	s	k7c7
svahem	svah	k1gInSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
zamezuje	zamezovat	k5eAaImIp3nS
uložení	uložení	k1gNnSc4
půd	půda	k1gFnPc2
ve	v	k7c6
větším	veliký	k2eAgNnSc6d2
množství	množství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Mušlovka	Mušlovka	k1gFnSc1
</s>
<s>
lom	lom	k1gInSc1
Mušlovka	Mušlovka	k1gFnSc1
</s>
<s>
Velice	velice	k6eAd1
známou	známý	k2eAgFnSc7d1
lokalitou	lokalita	k1gFnSc7
bohatou	bohatý	k2eAgFnSc7d1
na	na	k7c4
zkameněliny	zkamenělina	k1gFnPc4
je	být	k5eAaImIp3nS
lom	lom	k1gInSc4
Mušlovka	Mušlovka	k1gFnSc1
sloužící	sloužící	k1gFnSc1
jako	jako	k8xS,k8xC
opěrný	opěrný	k2eAgInSc1d1
stratotyp	stratotyp	k1gInSc1
pro	pro	k7c4
hranici	hranice	k1gFnSc4
nejvyšších	vysoký	k2eAgNnPc2d3
silurských	silurský	k2eAgNnPc2d1
oddělení	oddělení	k1gNnPc2
ludlow	ludlow	k?
a	a	k8xC
přídolí	přídolit	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Klasickou	klasický	k2eAgFnSc7d1
paleontologickou	paleontologický	k2eAgFnSc7d1
lokalitou	lokalita	k1gFnSc7
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
postranní	postranní	k2eAgFnSc1d1
část	část	k1gFnSc1
–	–	k?
tzv.	tzv.	kA
Arethusinová	Arethusinový	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
<g/>
,	,	kIx,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1
Woldřichem	Woldřich	k1gInSc7
dle	dle	k7c2
trilobita	trilobit	k1gMnSc2
Aulacopleura	Aulacopleur	k1gMnSc2
–	–	k?
Arethusina	Arethusin	k2eAgMnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
V	v	k7c6
rokli	rokle	k1gFnSc6
vystupují	vystupovat	k5eAaImIp3nP
příkrovy	příkrov	k1gInPc1
podmořských	podmořský	k2eAgInPc2d1
vulkanitů	vulkanit	k1gInPc2
(	(	kIx(
<g/>
diabasů	diabas	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
vyvřelinami	vyvřelina	k1gFnPc7
je	být	k5eAaImIp3nS
mocná	mocný	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
tufitických	tufitický	k2eAgFnPc2d1
břidlic	břidlice	k1gFnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc4
stratigrafické	stratigrafický	k2eAgNnSc4d1
zařazení	zařazení	k1gNnSc4
ke	k	k7c3
svrchní	svrchní	k2eAgFnSc3d1
části	část	k1gFnSc3
liteňského	liteňský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
dokládají	dokládat	k5eAaImIp3nP
trilobit	trilobit	k1gMnSc1
Aulacopleura	Aulacopleur	k1gMnSc2
konincki	koninck	k1gFnSc2
a	a	k8xC
vůdčí	vůdčí	k2eAgMnSc1d1
graptolit	graptolit	k1gMnSc1
Testograptus	Testograptus	k1gMnSc1
testis	testis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c7
roklí	rokle	k1gFnSc7
a	a	k8xC
samotnou	samotný	k2eAgFnSc7d1
lomovou	lomový	k2eAgFnSc7d1
stěnou	stěna	k1gFnSc7
je	být	k5eAaImIp3nS
v	v	k7c6
severozápadně	severozápadně	k6eAd1
orientovaném	orientovaný	k2eAgInSc6d1
svahu	svah	k1gInSc6
lokalita	lokalita	k1gFnSc1
Cromusová	Cromusový	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
nálezy	nález	k1gInPc4
trilobita	trilobit	k1gMnSc4
rodu	rod	k1gInSc2
Cromus	Cromus	k1gInSc1
<g/>
.	.	kIx.
<g/>
Stářím	stáří	k1gNnSc7
náleží	náležet	k5eAaImIp3nS
ke	k	k7c3
spodní	spodní	k2eAgFnSc3d1
části	část	k1gFnSc3
kopaninského	kopaninský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgInSc1d1
vrstevní	vrstevní	k2eAgInSc1d1
sled	sled	k1gInSc1
pokračuje	pokračovat	k5eAaImIp3nS
už	už	k6eAd1
přímo	přímo	k6eAd1
v	v	k7c6
lomové	lomový	k2eAgFnSc6d1
stěně	stěna	k1gFnSc6
Mušlovky	Mušlovka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začíná	začínat	k5eAaImIp3nS
krinoidovými	krinoidový	k2eAgInPc7d1
<g/>
,	,	kIx,
brachiopodovými	brachiopodový	k2eAgInPc7d1
a	a	k8xC
cefalopodovými	cefalopodový	k2eAgInPc7d1
vápenci	vápenec	k1gInPc7
vyšší	vysoký	k2eAgFnSc2d2
části	část	k1gFnSc2
kopaninského	kopaninský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
,	,	kIx,
závěr	závěr	k1gInSc1
kopaninského	kopaninský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
tvoří	tvořit	k5eAaImIp3nP
tenčeji	tenko	k6eAd2
vrstevnaté	vrstevnatý	k2eAgInPc1d1
šedé	šedý	k2eAgInPc1d1
vápence	vápenec	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejmladší	mladý	k2eAgInPc1d3
sedimenty	sediment	k1gInPc1
Mušlovky	Mušlovka	k1gFnSc2
na	na	k7c6
vrcholu	vrchol	k1gInSc6
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
jsou	být	k5eAaImIp3nP
tmavší	tmavý	k2eAgInPc4d2
tence	tenko	k6eAd1
vrstevnaté	vrstevnatý	k2eAgInPc4d1
vápence	vápenec	k1gInPc4
s	s	k7c7
vložkami	vložka	k1gFnPc7
břidlic	břidlice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
nálezů	nález	k1gInPc2
graptolitů	graptolit	k1gMnPc2
Monograptus	Monograptus	k1gMnSc1
ultimus	ultimus	k1gMnSc1
a	a	k8xC
M.	M.	kA
parultimus	parultimus	k1gInSc1
náležejí	náležet	k5eAaImIp3nP
spodní	spodní	k2eAgFnPc1d1
části	část	k1gFnPc1
přídolského	přídolský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lobolitová	Lobolitový	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
</s>
<s>
Za	za	k7c7
ohybem	ohyb	k1gInSc7
cesty	cesta	k1gFnSc2
se	se	k3xPyFc4
ve	v	k7c6
starém	starý	k2eAgInSc6d1
lomu	lom	k1gInSc6
nachází	nacházet	k5eAaImIp3nS
význačné	význačný	k2eAgNnSc4d1
naleziště	naleziště	k1gNnSc4
lilijic	lilijice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
nálezů	nález	k1gInPc2
jejich	jejich	k3xOp3gInPc2
plovacích	plovací	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
zvaných	zvaný	k2eAgInPc2d1
lobolity	lobolit	k1gInPc7
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
Lobolitová	Lobolitový	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
lobolitů	lobolit	k1gInPc2
se	se	k3xPyFc4
ve	v	k7c6
svahu	svah	k1gInSc6
<g/>
,	,	kIx,
obzvláště	obzvláště	k6eAd1
po	po	k7c6
dešti	dešť	k1gInSc6
<g/>
,	,	kIx,
dají	dát	k5eAaPmIp3nP
nalézt	nalézt	k5eAaPmF,k5eAaBmF
vyvětralé	vyvětralý	k2eAgInPc4d1
články	článek	k1gInPc4
stonků	stonek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytky	zbytek	k1gInPc1
kalichů	kalich	k1gInPc2
zde	zde	k6eAd1
hojněji	hojně	k6eAd2
pocházejí	pocházet	k5eAaImIp3nP
od	od	k7c2
čeledi	čeleď	k1gFnSc2
menšího	malý	k2eAgInSc2d2
vzrůstu	vzrůst	k1gInSc2
Pisocrinidae	Pisocrinidae	k1gFnPc2
<g/>
,	,	kIx,
kalichy	kalich	k1gInPc1
větších	veliký	k2eAgInPc2d2
rodů	rod	k1gInPc2
Scyphocrinites	Scyphocrinites	k1gInSc4
a	a	k8xC
Carolicrinus	Carolicrinus	k1gInSc4
se	se	k3xPyFc4
zachovávají	zachovávat	k5eAaImIp3nP
vzácně	vzácně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stratigraficky	Stratigraficky	k1gFnSc1
náleží	náležet	k5eAaImIp3nS
přídolskému	přídolský	k2eAgNnSc3d1
souvrství	souvrství	k1gNnSc3
<g/>
,	,	kIx,
tj.	tj.	kA
nejvyššímu	vysoký	k2eAgInSc3d3
siluru	silur	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Černý	černý	k2eAgInSc1d1
lom	lom	k1gInSc1
</s>
<s>
Na	na	k7c4
dohled	dohled	k1gInSc4
od	od	k7c2
Lobolitové	Lobolitový	k2eAgFnSc2d1
stráně	stráň	k1gFnSc2
je	být	k5eAaImIp3nS
Černý	černý	k2eAgInSc1d1
lom	lom	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
v	v	k7c6
masivní	masivní	k2eAgFnSc6d1
lavici	lavice	k1gFnSc6
krinoidových	krinoidový	k2eAgInPc2d1
vápenců	vápenec	k1gInPc2
odkryta	odkryt	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
mezi	mezi	k7c7
silurem	silur	k1gInSc7
a	a	k8xC
devonem	devon	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgInSc1d3
silur	silur	k1gInSc1
je	být	k5eAaImIp3nS
ohraničen	ohraničit	k5eAaPmNgInS
výskytem	výskyt	k1gInSc7
trilobita	trilobit	k1gMnSc2
Tetinia	Tetinium	k1gNnPc1
minuta	minuta	k1gFnSc1
<g/>
,	,	kIx,
nejnižší	nízký	k2eAgInSc1d3
devon	devon	k1gInSc1
poznáme	poznat	k5eAaPmIp1nP
dle	dle	k7c2
trilobitů	trilobit	k1gMnPc2
ze	z	k7c2
skupiny	skupina	k1gFnSc2
druhu	druh	k1gInSc2
Warburgella	Warburgella	k1gFnSc1
rugulosa	rugulosa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sled	sled	k1gInSc1
devonu	devon	k1gInSc2
zde	zde	k6eAd1
pokračuje	pokračovat	k5eAaImIp3nS
kotýsskými	kotýsský	k2eAgInPc7d1
vápenci	vápenec	k1gInPc7
chudými	chudý	k2eAgInPc7d1
na	na	k7c6
zkameněliny	zkamenělina	k1gFnPc1
náležejícími	náležející	k2eAgFnPc7d1
stářím	stáří	k1gNnSc7
vyššímu	vysoký	k2eAgInSc3d2
lochkovu	lochkov	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
železniční	železniční	k2eAgFnSc3d1
stanici	stanice	k1gFnSc3
Praha-Holyně	Praha-Holyně	k1gFnSc2
už	už	k6eAd1
cesta	cesta	k1gFnSc1
pokračuje	pokračovat	k5eAaImIp3nS
devonskými	devonský	k2eAgInPc7d1
sedimenty	sediment	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Flóra	Flóra	k1gFnSc1
</s>
<s>
Vegetační	vegetační	k2eAgInSc1d1
pokryv	pokryv	k1gInSc1
na	na	k7c6
území	území	k1gNnSc6
Dalejského	Dalejský	k2eAgInSc2d1
profilu	profil	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
kromě	kromě	k7c2
původních	původní	k2eAgFnPc2d1
skalních	skalní	k2eAgFnPc2d1
stepí	step	k1gFnPc2
nepůvodní	původní	k2eNgInPc1d1
porosty	porost	k1gInPc1
tvořené	tvořený	k2eAgFnSc2d1
zejména	zejména	k9
borovicí	borovice	k1gFnSc7
černou	černý	k2eAgFnSc7d1
(	(	kIx(
<g/>
Pinus	Pinus	k1gMnSc1
nigra	nigr	k1gMnSc2
<g/>
)	)	kIx)
a	a	k8xC
trnovníkem	trnovník	k1gInSc7
akátem	akát	k1gInSc7
(	(	kIx(
<g/>
Robinia	Robinium	k1gNnSc2
pseudoacacia	pseudoacacium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
porosty	porost	k1gInPc4
akátu	akát	k1gInSc2
jsou	být	k5eAaImIp3nP
vázány	vázán	k2eAgInPc4d1
nitrofilní	nitrofilní	k2eAgInPc4d1
druhy	druh	k1gInPc4
např.	např.	kA
vlaštovičník	vlaštovičník	k1gInSc4
větší	veliký	k2eAgFnPc1d2
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Chelidonium	Chelidonium	k1gNnSc1
majus	majus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kakost	kakost	k1gInSc1
smrdutý	smrdutý	k2eAgInSc1d1
(	(	kIx(
<g/>
Geranium	Geranium	k1gNnSc1
robertianum	robertianum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kuklík	kuklík	k1gInSc1
městský	městský	k2eAgInSc1d1
(	(	kIx(
<g/>
Geum	Geum	k1gInSc1
urbanum	urbanum	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
netýkavka	netýkavka	k1gFnSc1
malokvětá	malokvětý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Impatiens	Impatiens	k1gInSc1
parviflora	parviflor	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rozrazil	rozrazil	k1gInSc1
břečťanolistý	břečťanolistý	k2eAgInSc1d1
(	(	kIx(
<g/>
Veronica	Veronic	k2eAgFnSc1d1
hederifolia	hederifolia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
14	#num#	k4
<g/>
]	]	kIx)
Lesní	lesní	k2eAgInPc1d1
porosty	porost	k1gInPc1
jsou	být	k5eAaImIp3nP
dále	daleko	k6eAd2
tvořeny	tvořit	k5eAaImNgInP
náletovými	náletový	k2eAgFnPc7d1
dřevinami	dřevina	k1gFnPc7
<g/>
:	:	kIx,
bříza	bříza	k1gFnSc1
(	(	kIx(
<g/>
Betula	Betula	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vrba	vrba	k1gFnSc1
jíva	jíva	k1gFnSc1
(	(	kIx(
<g/>
Salix	Salix	k1gInSc1
caprea	capre	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
topol	topol	k1gInSc1
osika	osika	k1gFnSc1
(	(	kIx(
<g/>
Populus	Populus	k1gInSc1
tremula	tremulum	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
jasan	jasan	k1gInSc1
(	(	kIx(
<g/>
Fraxinus	Fraxinus	k1gInSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sukcesi	sukcese	k1gFnSc4
bylin	bylina	k1gFnPc2
na	na	k7c6
rozhraní	rozhraní	k1gNnSc6
lesních	lesní	k2eAgInPc2d1
porostů	porost	k1gInPc2
a	a	k8xC
stepních	stepní	k2eAgFnPc2d1
enkláv	enkláva	k1gFnPc2
v	v	k7c6
přírodním	přírodní	k2eAgInSc6d1
parku	park	k1gInSc6
Prokopské	prokopský	k2eAgNnSc1d1
a	a	k8xC
Dalejské	Dalejský	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
se	se	k3xPyFc4
věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
Chvátalová	Chvátalová	k1gFnSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nalezneme	nalézt	k5eAaBmIp1nP,k5eAaPmIp1nP
zde	zde	k6eAd1
porosty	porost	k1gInPc1
skalních	skalní	k2eAgFnPc2d1
stepí	step	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
se	se	k3xPyFc4
svou	svůj	k3xOyFgFnSc7
kvalitou	kvalita	k1gFnSc7
vyrovnají	vyrovnat	k5eAaPmIp3nP,k5eAaBmIp3nP
těm	ten	k3xDgMnPc3
v	v	k7c6
CHKO	CHKO	kA
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
podloží	podloží	k1gNnSc4
vulkanitů	vulkanit	k1gInPc2
–	–	k?
diabasů	diabas	k1gInPc2
jsou	být	k5eAaImIp3nP
vázány	vázat	k5eAaImNgFnP
stepi	step	k1gFnPc1
charakterizované	charakterizovaný	k2eAgFnPc1d1
kavylem	kavyl	k1gInSc7
vláskovitým	vláskovitý	k2eAgFnPc3d1
(	(	kIx(
<g/>
Stipa	Stipa	k1gFnSc1
capillata	capille	k1gNnPc1
<g/>
)	)	kIx)
a	a	k8xC
strdivkou	strdivka	k1gFnSc7
sedmihradskou	sedmihradský	k2eAgFnSc7d1
(	(	kIx(
<g/>
Melica	Melic	k2eAgNnPc1d1
transsylvanica	transsylvanicum	k1gNnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vápenci	vápenec	k1gInSc6
nalezneme	naleznout	k5eAaPmIp1nP,k5eAaBmIp1nP
porosty	porost	k1gInPc4
teplomilných	teplomilný	k2eAgInPc2d1
trávníků	trávník	k1gInPc2
s	s	k7c7
pěchavou	pěchava	k1gFnSc7
vápnomilnou	vápnomilný	k2eAgFnSc7d1
(	(	kIx(
<g/>
Sesleria	Seslerium	k1gNnPc4
varia	varia	k1gNnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgInPc1d1
významné	významný	k2eAgInPc1d1
druhy	druh	k1gInPc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
křivatec	křivatec	k1gInSc1
český	český	k2eAgInSc1d1
(	(	kIx(
<g/>
Gagea	Gage	k2eAgFnSc1d1
bohemica	bohemica	k1gFnSc1
<g/>
)	)	kIx)
kriticky	kriticky	k6eAd1
ohrožený	ohrožený	k2eAgInSc1d1
endemit	endemit	k1gInSc1
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
pochybek	pochybek	k1gInSc4
prodloužený	prodloužený	k2eAgInSc4d1
(	(	kIx(
<g/>
Androsace	Androsace	k1gFnSc1
elongata	elongata	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
rozrazil	rozrazil	k1gInSc1
ladní	ladní	k2eAgInSc1d1
(	(	kIx(
<g/>
Veronica	Veronica	k1gFnSc1
dillenii	dillenium	k1gNnPc7
<g/>
)	)	kIx)
</s>
<s>
hvězdovka	hvězdovka	k1gFnSc1
Pouzarova	Pouzarův	k2eAgFnSc1d1
(	(	kIx(
<g/>
Geastrum	Geastrum	k1gNnSc4
pouzarii	pouzarie	k1gFnSc4
<g/>
)	)	kIx)
jediný	jediný	k2eAgInSc1d1
český	český	k2eAgInSc1d1
houbový	houbový	k2eAgInSc1d1
endemit	endemit	k1gInSc1
</s>
<s>
jižní	jižní	k2eAgInPc1d1
svahy	svah	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
devaterníček	devaterníček	k1gInSc1
šedý	šedý	k2eAgInSc1d1
(	(	kIx(
<g/>
Rhodax	Rhodax	k1gInSc1
canus	canus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ostřice	ostřice	k1gFnSc1
nízká	nízký	k2eAgFnSc1d1
(	(	kIx(
<g/>
Carex	Carex	k1gInSc1
humilis	humilis	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
vrcholy	vrchol	k1gInPc1
svahů	svah	k1gInPc2
s	s	k7c7
hlubší	hluboký	k2eAgFnSc7d2
půdou	půda	k1gFnSc7
<g/>
:	:	kIx,
</s>
<s>
trýzel	trýzel	k1gInSc1
škardolistý	škardolistý	k2eAgInSc1d1
(	(	kIx(
<g/>
Erysimum	Erysimum	k1gInSc1
crepidifolium	crepidifolium	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
kostřava	kostřava	k1gFnSc1
walliská	walliská	k1gFnSc1
(	(	kIx(
<g/>
Festuca	Festuc	k2eAgFnSc1d1
valesiaca	valesiaca	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
směrem	směr	k1gInSc7
k	k	k7c3
severu	sever	k1gInSc3
a	a	k8xC
severozápadu	severozápad	k1gInSc3
<g/>
:	:	kIx,
</s>
<s>
prvosenka	prvosenka	k1gFnSc1
jarní	jarní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Primula	Primula	k?
veris	veris	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
pěchava	pěchava	k1gFnSc1
vápnomilná	vápnomilný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Sesleria	Seslerium	k1gNnSc2
varia	varia	k1gNnPc1
<g/>
)	)	kIx)
</s>
<s>
Fauna	fauna	k1gFnSc1
</s>
<s>
Lokalita	lokalita	k1gFnSc1
Dalejský	Dalejský	k2eAgInSc1d1
profil	profil	k1gInSc1
je	být	k5eAaImIp3nS
pozoruhodná	pozoruhodný	k2eAgFnSc1d1
množstvím	množství	k1gNnSc7
druhů	druh	k1gInPc2
motýlů	motýl	k1gMnPc2
(	(	kIx(
<g/>
Lepidoptera	Lepidopter	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
nalezeno	nalézt	k5eAaBmNgNnS,k5eAaPmNgNnS
1054	#num#	k4
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příklady	příklad	k1gInPc1
nejvýznačnějších	význačný	k2eAgInPc2d3
druhů	druh	k1gInPc2
motýlů	motýl	k1gMnPc2
vázaných	vázaný	k2eAgMnPc2d1
na	na	k7c4
stepní	stepní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
:	:	kIx,
Cephimallota	Cephimallota	k1gFnSc1
angusticostella	angusticostella	k1gFnSc1
<g/>
,	,	kIx,
Coleophora	Coleophora	k1gFnSc1
serpylletorum	serpylletorum	k1gInSc1
<g/>
,	,	kIx,
C.	C.	kA
auricella	auricella	k1gFnSc1
<g/>
,	,	kIx,
C.	C.	kA
ditella	ditella	k1gFnSc1
<g/>
,	,	kIx,
C.	C.	kA
ochrea	ochre	k1gInSc2
<g/>
,	,	kIx,
C.	C.	kA
silenella	silenello	k1gNnSc2
<g/>
,	,	kIx,
Heinemannia	Heinemannium	k1gNnSc2
festivella	festivello	k1gNnSc2
<g/>
,	,	kIx,
Vulcaniella	Vulcaniell	k1gMnSc2
pomposella	pomposell	k1gMnSc2
<g/>
,	,	kIx,
Lixodessa	Lixodess	k1gMnSc2
ochrofasciella	ochrofasciell	k1gMnSc2
<g/>
,	,	kIx,
Dyspessa	Dyspess	k1gMnSc2
ulula	ulul	k1gMnSc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Pennisetia	Pennisetia	k1gFnSc1
bohemica	bohemica	k1gFnSc1
<g/>
,	,	kIx,
Adscita	Adscita	k1gMnSc1
geryon	geryon	k1gMnSc1
<g/>
,	,	kIx,
Cochylis	Cochylis	k1gFnSc1
pallidana	pallidana	k1gFnSc1
<g/>
,	,	kIx,
Pelochrista	Pelochrista	k1gMnSc1
caecimaculana	caecimaculana	k1gFnSc1
<g/>
,	,	kIx,
Eurhodope	Eurhodop	k1gInSc5
rosella	rosella	k1gMnSc1
<g/>
,	,	kIx,
E.	E.	kA
cirrigerella	cirrigerell	k1gMnSc2
<g/>
,	,	kIx,
Chazara	Chazar	k1gMnSc2
briseis	briseis	k1gFnSc1
<g/>
,	,	kIx,
Hamearis	Hamearis	k1gFnSc1
lucina	lucina	k1gFnSc1
<g/>
,	,	kIx,
Lomographa	Lomographa	k1gFnSc1
distinctata	distinctata	k1gFnSc1
<g/>
,	,	kIx,
Euchalcia	Euchalcia	k1gFnSc1
consona	consona	k1gFnSc1
<g/>
,	,	kIx,
Auchmis	Auchmis	k1gFnSc1
detersa	detersa	k1gFnSc1
a	a	k8xC
Polia	Polia	k1gFnSc1
serratilinea	serratilinea	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kromě	kromě	k7c2
motýlů	motýl	k1gMnPc2
zde	zde	k6eAd1
nalezneme	nalézt	k5eAaBmIp1nP,k5eAaPmIp1nP
také	také	k9
množství	množství	k1gNnSc4
jiných	jiný	k2eAgNnPc2d1
bezobratlých	bezobratlý	k2eAgNnPc2d1
vázaných	vázané	k1gNnPc2
na	na	k7c4
skalní	skalní	k2eAgFnPc4d1
stepi	step	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
např.	např.	kA
střevlíkovití	střevlíkovitý	k2eAgMnPc1d1
Licinus	Licinus	k1gMnSc1
depressus	depressus	k1gMnSc1
a	a	k8xC
Masoreus	Masoreus	k1gMnSc1
Wetterballii	Wetterballie	k1gFnSc4
<g/>
,	,	kIx,
mandelinka	mandelinka	k1gFnSc1
Chrisolina	Chrisolina	k1gFnSc1
marginata	marginata	k1gFnSc1
<g/>
,	,	kIx,
štítonoš	štítonoš	k1gMnSc1
Cassida	Cassida	k1gFnSc1
rufovirens	rufovirens	k6eAd1
<g/>
,	,	kIx,
nosatčík	nosatčík	k1gMnSc1
Apion	Apion	k1gMnSc1
austriacum	austriacum	k1gNnSc4
a	a	k8xC
krytonosci	krytonosec	k1gMnPc1
Ranunculiphilus	Ranunculiphilus	k1gMnSc1
obsoletus	obsoletus	k1gMnSc1
a	a	k8xC
Ceutorhynchus	Ceutorhynchus	k1gMnSc1
angustur	angustura	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
malé	malý	k2eAgFnSc3d1
rozloze	rozloha	k1gFnSc3
území	území	k1gNnSc2
není	být	k5eNaImIp3nS
lokalita	lokalita	k1gFnSc1
charakteristická	charakteristický	k2eAgFnSc1d1
svými	svůj	k3xOyFgMnPc7
druhy	druh	k1gInPc7
obratlovců	obratlovec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nalezneme	nalézt	k5eAaBmIp1nP,k5eAaPmIp1nP
zde	zde	k6eAd1
druhy	druh	k1gInPc1
vázané	vázaný	k2eAgInPc1d1
buď	buď	k8xC
na	na	k7c4
prosluněné	prosluněný	k2eAgFnPc4d1
stráně	stráň	k1gFnPc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
lesní	lesní	k2eAgInSc4d1
porosty	porost	k1gInPc4
či	či	k8xC
vodní	vodní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
potoka	potok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
obojživelníků	obojživelník	k1gMnPc2
se	se	k3xPyFc4
zde	zde	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
čolek	čolek	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Triturus	Triturus	k1gInSc1
vulgaris	vulgaris	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
skokan	skokan	k1gMnSc1
hnědý	hnědý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Rana	Rana	k1gFnSc1
temporaria	temporarium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
plazů	plaz	k1gMnPc2
pak	pak	k9
ještěrka	ještěrka	k1gFnSc1
obecná	obecná	k1gFnSc1
(	(	kIx(
<g/>
Lacerta	Lacerta	k1gFnSc1
agilis	agilis	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
ještěrka	ještěrka	k1gFnSc1
zelená	zelený	k2eAgFnSc1d1
(	(	kIx(
<g/>
Lacerta	Lacerta	k1gFnSc1
viridis	viridis	k1gFnSc1
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Ptáky	pták	k1gMnPc4
na	na	k7c6
lokalitě	lokalita	k1gFnSc6
zastupuje	zastupovat	k5eAaImIp3nS
například	například	k6eAd1
výr	výr	k1gMnSc1
velký	velký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Bubo	Bubo	k1gMnSc1
bubo	bubo	k1gMnSc1
<g/>
)	)	kIx)
-	-	kIx~
zalétá	zalétat	k5eAaImIp3nS,k5eAaPmIp3nS
sem	sem	k6eAd1
z	z	k7c2
Prokopského	prokopský	k2eAgNnSc2d1
údolí	údolí	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
Praze	Praha	k1gFnSc6
vzácný	vzácný	k2eAgMnSc1d1
bělořit	bělořit	k1gMnSc1
šedý	šedý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Oenanthe	Oenanthe	k1gInSc1
oenanthe	oenanth	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dlask	dlask	k1gMnSc1
tlustozobý	tlustozobý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Coccothraustes	Coccothraustes	k1gMnSc1
coccothraustes	coccothraustes	k1gMnSc1
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
hýl	hýl	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Pyrrhula	Pyrrhul	k1gMnSc4
pyrrhula	pyrrhul	k1gMnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Praze	Praha	k1gFnSc6
méně	málo	k6eAd2
početný	početný	k2eAgMnSc1d1
chocholouš	chocholouš	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Galerida	Galerid	k1gMnSc4
cristata	cristat	k1gMnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
káně	káně	k1gFnSc1
lesní	lesní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Buteo	Buteo	k1gMnSc1
buteo	buteo	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
konipas	konipas	k1gMnSc1
horský	horský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Motacilla	Motacilla	k1gMnSc1
cinerea	cinerea	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vzácnější	vzácný	k2eAgMnSc1d2
šplhavec	šplhavec	k1gMnSc1
krutihlav	krutihlav	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Jynx	Jynx	k1gInSc1
torquilla	torquillo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lejsek	lejsek	k1gMnSc1
bělokrký	bělokrký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Ficedula	Ficedula	k1gFnSc1
albicollis	albicollis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rehek	rehek	k1gMnSc1
zahradní	zahradní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Phoenicurus	Phoenicurus	k1gMnSc1
phoenicurus	phoenicurus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ťuhýk	ťuhýk	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Lanius	Lanius	k1gMnSc1
collurio	collurio	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
žluna	žluna	k1gFnSc1
šedá	šedá	k1gFnSc1
(	(	kIx(
<g/>
Picus	Picus	k1gMnSc1
canus	canus	k1gMnSc1
<g/>
)	)	kIx)
či	či	k8xC
žluva	žluva	k1gFnSc1
hajní	hajní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Oriolus	Oriolus	k1gMnSc1
oriolus	oriolus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ichnofosilie	ichnofosilie	k1gFnSc1
na	na	k7c6
Placaté	placatý	k2eAgFnSc6d1
skále	skála	k1gFnSc6
</s>
<s>
Ze	z	k7c2
savců	savec	k1gMnPc2
byly	být	k5eAaImAgFnP
na	na	k7c6
lokalitě	lokalita	k1gFnSc6
popsány	popsat	k5eAaPmNgFnP
hlavně	hlavně	k9
drobné	drobný	k2eAgFnPc1d1
formy	forma	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poměrně	poměrně	k6eAd1
velký	velký	k2eAgMnSc1d1
hmyzožravec	hmyzožravec	k1gMnSc1
rejsec	rejsec	k1gMnSc1
vodní	vodní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Neomys	Neomys	k1gInSc1
fodiens	fodiens	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vázaný	vázaný	k2eAgMnSc1d1
na	na	k7c6
okolí	okolí	k1gNnSc6
Dalejského	Dalejský	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
drobných	drobný	k2eAgMnPc2d1
savců	savec	k1gMnPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
Dalejského	Dalejský	k2eAgNnSc2d1
údolí	údolí	k1gNnSc2
je	být	k5eAaImIp3nS
nejhojnější	hojný	k2eAgFnSc1d3
myšice	myšice	k1gFnSc1
křovinná	křovinný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Apodemus	Apodemus	k1gMnSc1
sylvaticus	sylvaticus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Šelmy	šelma	k1gFnPc1
jsou	být	k5eAaImIp3nP
zastoupeny	zastoupit	k5eAaPmNgFnP
lasicovitými	lasicovitý	k2eAgFnPc7d1
<g/>
:	:	kIx,
tchoř	tchoř	k1gMnSc1
tmavý	tmavý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Mustela	Mustela	k1gFnSc1
putorius	putorius	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lasice	lasice	k1gFnSc1
kolčava	kolčava	k1gFnSc1
(	(	kIx(
<g/>
Mustela	Mustela	k1gFnSc1
nivalis	nivalis	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
lasice	lasice	k1gFnSc1
hranostaj	hranostaj	k1gMnSc1
(	(	kIx(
<g/>
Mustela	Mustela	k1gFnSc1
erminea	erminea	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ochrana	ochrana	k1gFnSc1
a	a	k8xC
hospodaření	hospodaření	k1gNnSc1
</s>
<s>
Problémem	problém	k1gInSc7
jsou	být	k5eAaImIp3nP
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
uměle	uměle	k6eAd1
vysazené	vysazený	k2eAgFnSc2d1
nepůvodní	původní	k2eNgFnSc2d1
dřeviny	dřevina	k1gFnSc2
<g/>
:	:	kIx,
trnovník	trnovník	k1gInSc1
akát	akát	k1gInSc1
(	(	kIx(
<g/>
Robinia	Robinium	k1gNnSc2
pseudoacacia	pseudoacacium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
borovice	borovice	k1gFnSc1
černá	černý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Pinus	Pinus	k1gMnSc1
nigra	nigr	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
modřín	modřín	k1gInSc1
opadavý	opadavý	k2eAgInSc1d1
(	(	kIx(
<g/>
Larix	Larix	k1gInSc1
decidua	decidu	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
borovice	borovice	k1gFnSc1
lesní	lesní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Pinus	Pinus	k1gMnSc1
sylvestris	sylvestris	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
nyní	nyní	k6eAd1
v	v	k7c6
oblasti	oblast	k1gFnSc6
šíří	šířit	k5eAaImIp3nP
a	a	k8xC
porůstají	porůstat	k5eAaImIp3nP
skalní	skalní	k2eAgInPc1d1
výchozy	výchoz	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zejména	zejména	k9
porosty	porost	k1gInPc1
akátu	akát	k1gInSc2
ničí	ničit	k5eAaImIp3nP
původní	původní	k2eAgInSc4d1
stepní	stepní	k2eAgInSc4d1
porosty	porost	k1gInPc4
a	a	k8xC
umožňují	umožňovat	k5eAaImIp3nP
šíření	šíření	k1gNnSc4
ruderálních	ruderální	k2eAgInPc2d1
a	a	k8xC
nitrofilních	nitrofilní	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Ohrožením	ohrožení	k1gNnSc7
pro	pro	k7c4
stepní	stepní	k2eAgNnPc4d1
společenstva	společenstvo	k1gNnPc4
i	i	k9
pro	pro	k7c4
geologické	geologický	k2eAgInPc4d1
odkryvy	odkryv	k1gInPc4
je	být	k5eAaImIp3nS
zarůstání	zarůstání	k1gNnSc1
náletovými	náletový	k2eAgFnPc7d1
dřevinami	dřevina	k1gFnPc7
<g/>
:	:	kIx,
bříza	bříza	k1gFnSc1
(	(	kIx(
<g/>
Betula	Betula	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vrba	vrba	k1gFnSc1
jíva	jíva	k1gFnSc1
(	(	kIx(
<g/>
Salix	Salix	k1gInSc1
caprea	capre	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
topol	topol	k1gInSc1
osika	osika	k1gFnSc1
(	(	kIx(
<g/>
Populus	Populus	k1gInSc1
tremula	tremulum	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
jasan	jasan	k1gInSc1
(	(	kIx(
<g/>
Fraxinus	Fraxinus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
zatlačují	zatlačovat	k5eAaImIp3nP
původní	původní	k2eAgInPc4d1
druhy	druh	k1gInPc4
a	a	k8xC
mohou	moct	k5eAaImIp3nP
kořeny	kořen	k1gInPc1
narušit	narušit	k5eAaPmF
skalní	skalní	k2eAgFnPc4d1
stěny	stěna	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Potenciální	potenciální	k2eAgFnSc7d1
hrozbou	hrozba	k1gFnSc7
je	být	k5eAaImIp3nS
nadměrný	nadměrný	k2eAgInSc1d1
sběr	sběr	k1gInSc1
zkamenělin	zkamenělina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatím	zatím	k6eAd1
však	však	k9
stav	stav	k1gInSc1
území	území	k1gNnSc2
nevyžaduje	vyžadovat	k5eNaImIp3nS
omezení	omezení	k1gNnSc4
vstupu	vstup	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poškození	poškození	k1gNnPc1
ze	z	k7c2
strany	strana	k1gFnSc2
člověka	člověk	k1gMnSc2
je	být	k5eAaImIp3nS
patrně	patrně	k6eAd1
na	na	k7c6
Placaté	placatý	k2eAgFnSc6d1
skále	skála	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
trpí	trpět	k5eAaImIp3nS
sešlapem	sešlapem	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
negativním	negativní	k2eAgInSc7d1
důsledkem	důsledek	k1gInSc7
lidské	lidský	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
jsou	být	k5eAaImIp3nP
černé	černý	k2eAgFnPc4d1
skládky	skládka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prioritou	priorita	k1gFnSc7
je	být	k5eAaImIp3nS
udržet	udržet	k5eAaPmF
oblast	oblast	k1gFnSc4
použitelnou	použitelný	k2eAgFnSc4d1
a	a	k8xC
esteticky	esteticky	k6eAd1
přijatelnou	přijatelný	k2eAgFnSc4d1
pro	pro	k7c4
další	další	k2eAgInSc4d1
paleontologický	paleontologický	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Garantem	garant	k1gMnSc7
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
geologických	geologický	k2eAgInPc2d1
výchozů	výchoz	k1gInPc2
je	být	k5eAaImIp3nS
Česká	český	k2eAgFnSc1d1
geologická	geologický	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podstatná	podstatný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
i	i	k8xC
ochrana	ochrana	k1gFnSc1
stepních	stepní	k2eAgNnPc2d1
společenstev	společenstvo	k1gNnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
z	z	k7c2
botanického	botanický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
zlepšovat	zlepšovat	k5eAaImF
stav	stav	k1gInSc4
porostů	porost	k1gInPc2
a	a	k8xC
přibližovat	přibližovat	k5eAaImF
je	on	k3xPp3gInPc4
původnímu	původní	k2eAgNnSc3d1
druhovému	druhový	k2eAgNnSc3d1
složení	složení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1999-2002	1999-2002	k4
proběhlo	proběhnout	k5eAaPmAgNnS
mýcení	mýcení	k1gNnSc1
náletových	náletový	k2eAgInPc2d1
a	a	k8xC
uměle	uměle	k6eAd1
vysázených	vysázený	k2eAgFnPc2d1
dřevin	dřevina	k1gFnPc2
na	na	k7c6
Placaté	placatý	k2eAgFnSc6d1
skále	skála	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
Černém	černý	k2eAgInSc6d1
lomu	lom	k1gInSc6
<g/>
,	,	kIx,
lomu	lom	k1gInSc6
Mušlovka	Mušlovka	k1gFnSc1
a	a	k8xC
na	na	k7c6
Cromusové	Cromusový	k2eAgFnSc6d1
stráni	stráň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
v	v	k7c6
Mušlovce	Mušlovka	k1gFnSc6
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
v	v	k7c6
Černém	černý	k2eAgInSc6d1
lomu	lom	k1gInSc6
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
kácení	kácení	k1gNnSc3
už	už	k6eAd1
vzrostlých	vzrostlý	k2eAgInPc2d1
topolů	topol	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
činnosti	činnost	k1gFnSc6
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
pokračovat	pokračovat	k5eAaImF
a	a	k8xC
udržovat	udržovat	k5eAaImF
oblasti	oblast	k1gFnPc4
vymýcené	vymýcený	k2eAgFnPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Turismus	turismus	k1gInSc1
</s>
<s>
kostel	kostel	k1gInSc1
na	na	k7c6
Řeporyjském	Řeporyjský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
</s>
<s>
Skrz	skrz	k7c4
území	území	k1gNnSc4
prochází	procházet	k5eAaImIp3nS
naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
vedoucí	vedoucí	k2eAgFnSc1d1
podél	podél	k7c2
Dalejského	Dalejský	k2eAgInSc2d1
potoka	potok	k1gInSc2
z	z	k7c2
Řeporyj	Řeporyj	k1gMnSc1
do	do	k7c2
Holyně	Holyně	k1gFnSc2
a	a	k8xC
pokračující	pokračující	k2eAgInPc1d1
dále	daleko	k6eAd2
po	po	k7c6
proudu	proud	k1gInSc6
potoka	potok	k1gInSc2
až	až	k9
do	do	k7c2
Hlubočep	Hlubočep	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staré	staré	k1gNnSc1
naučné	naučný	k2eAgFnSc2d1
tabule	tabule	k1gFnSc2
byly	být	k5eAaImAgFnP
na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
2011	#num#	k4
nahrazeny	nahrazen	k2eAgFnPc4d1
novými	nový	k2eAgInPc7d1
většími	veliký	k2eAgInPc7d2
a	a	k8xC
trasa	trasa	k1gFnSc1
bývalé	bývalý	k2eAgFnSc2d1
stezky	stezka	k1gFnSc2
byla	být	k5eAaImAgFnS
mírně	mírně	k6eAd1
upravena	upravit	k5eAaPmNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Tvorba	tvorba	k1gFnSc1
původní	původní	k2eAgFnSc2d1
stezky	stezka	k1gFnSc2
byla	být	k5eAaImAgFnS
inspirována	inspirovat	k5eAaBmNgFnS
popisem	popis	k1gInSc7
prof.	prof.	kA
Iva	Ivo	k1gMnSc2
Chlupáče	chlupáč	k1gMnSc2
v	v	k7c6
knize	kniha	k1gFnSc6
Vycházky	vycházka	k1gFnSc2
za	za	k7c7
geologickou	geologický	k2eAgFnSc7d1
minulostí	minulost	k1gFnSc7
Prahy	Praha	k1gFnSc2
a	a	k8xC
okolí	okolí	k1gNnSc4
vydané	vydaný	k2eAgNnSc4d1
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
pozornost	pozornost	k1gFnSc4
stojí	stát	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgInSc1d1
stratotyp	stratotyp	k1gInSc1
spodní	spodní	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
přídolí	přídolí	k1gNnSc2
–	–	k?
nejvyššího	vysoký	k2eAgNnSc2d3
oddělení	oddělení	k1gNnSc2
siluru	silur	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
lomů	lom	k1gInPc2
Na	na	k7c6
požárech	požár	k1gInPc6
nedaleko	daleko	k6eNd1
Řeporyj	Řeporyj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k1gNnSc1
<g/>
,	,	kIx,
abychom	aby	kYmCp1nP
odbočili	odbočit	k5eAaPmAgMnP
do	do	k7c2
Mládkovy	Mládkův	k2eAgFnSc2d1
ulice	ulice	k1gFnSc2
<g/>
,	,	kIx,
zahneme	zahnout	k5eAaPmIp1nP
po	po	k7c6
hlavní	hlavní	k2eAgFnSc6d1
silnici	silnice	k1gFnSc6
vpravo	vpravo	k6eAd1
<g/>
,	,	kIx,
projdeme	projít	k5eAaPmIp1nP
podjezdem	podjezd	k1gInSc7
pod	pod	k7c7
tratí	trať	k1gFnSc7
a	a	k8xC
hned	hned	k6eAd1
za	za	k7c7
ním	on	k3xPp3gMnSc7
zahneme	zahnout	k5eAaPmIp1nP
vlevo	vlevo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
po	po	k7c4
700	#num#	k4
m	m	kA
jsou	být	k5eAaImIp3nP
po	po	k7c6
pravé	pravý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
cesty	cesta	k1gFnSc2
schůdky	schůdek	k1gInPc1
vedoucí	vedoucí	k1gFnSc2
dolů	dol	k1gInPc2
do	do	k7c2
průkopu	průkop	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projdeme	projít	k5eAaPmIp1nP
tunelem	tunel	k1gInSc7
a	a	k8xC
hned	hned	k6eAd1
vpravo	vpravo	k6eAd1
za	za	k7c7
tunelem	tunel	k1gInSc7
vystoupíme	vystoupit	k5eAaPmIp1nP
po	po	k7c6
železných	železný	k2eAgInPc6d1
schůdcích	schůdek	k1gInPc6
nad	nad	k7c4
tunel	tunel	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
stratotyp	stratotyp	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc7
vrstvou	vrstva	k1gFnSc7
náležející	náležející	k2eAgFnPc1d1
k	k	k7c3
oddělení	oddělení	k1gNnSc3
přídolí	přídolí	k1gNnSc2
je	být	k5eAaImIp3nS
vrstva	vrstva	k1gFnSc1
s	s	k7c7
číslem	číslo	k1gNnSc7
96	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
portál	portál	k1gInSc1
tunelu	tunel	k1gInSc2
ke	k	k7c3
stratotypu	stratotyp	k1gInSc3
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Nejlepším	dobrý	k2eAgNnSc7d3
výchozím	výchozí	k2eAgNnSc7d1
místem	místo	k1gNnSc7
je	být	k5eAaImIp3nS
obec	obec	k1gFnSc1
Řeporyje	Řeporyje	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jezdí	jezdit	k5eAaImIp3nS
sem	sem	k6eAd1
vlak	vlak	k1gInSc1
(	(	kIx(
<g/>
stanice	stanice	k1gFnSc1
Praha-Řeporyje	Praha-Řeporyje	k1gFnSc1
<g/>
)	)	kIx)
ze	z	k7c2
stanice	stanice	k1gFnSc2
Praha-Smíchov	Praha-Smíchov	k1gInSc1
a	a	k8xC
autobus	autobus	k1gInSc1
(	(	kIx(
<g/>
zastávka	zastávka	k1gFnSc1
Řeporyjské	Řeporyjský	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
–	–	k?
linky	linka	k1gFnSc2
230	#num#	k4
<g/>
,	,	kIx,
249	#num#	k4
<g/>
,	,	kIx,
256	#num#	k4
<g/>
,	,	kIx,
301	#num#	k4
<g/>
,	,	kIx,
352	#num#	k4
<g/>
,	,	kIx,
502	#num#	k4
<g/>
)	)	kIx)
ze	z	k7c2
stanice	stanice	k1gFnSc2
metra	metro	k1gNnSc2
B	B	kA
Luka	luka	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
Holyně	Holyně	k1gFnSc2
můžeme	moct	k5eAaImIp1nP
buď	buď	k8xC
jet	jet	k5eAaImF
vlakem	vlak	k1gInSc7
(	(	kIx(
<g/>
stanice	stanice	k1gFnSc1
Praha-Holyně	Praha-Holyně	k1gFnSc2
<g/>
)	)	kIx)
na	na	k7c6
nádraží	nádraží	k1gNnSc6
Praha-Smíchov	Praha-Smíchovo	k1gNnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
po	po	k7c6
modré	modrý	k2eAgFnSc6d1
turistické	turistický	k2eAgFnSc6d1
značce	značka	k1gFnSc6
dojít	dojít	k5eAaPmF
na	na	k7c4
autobus	autobus	k1gInSc4
(	(	kIx(
<g/>
zastávka	zastávka	k1gFnSc1
Holyně	Holyně	k1gFnSc2
–	–	k?
linka	linka	k1gFnSc1
230	#num#	k4
<g/>
)	)	kIx)
ve	v	k7c6
směru	směr	k1gInSc6
na	na	k7c6
stanici	stanice	k1gFnSc6
metra	metro	k1gNnSc2
B	B	kA
Luka	luka	k1gNnPc5
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Dalejský	Dalejský	k2eAgInSc4d1
profil	profil	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Otevřená	otevřený	k2eAgFnSc1d1
data	datum	k1gNnSc2
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
envis	envis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
praha-mesto	praha-mesto	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
http://www.rozhlas.cz/priroda/krasy/_zprava/dalejsky-profil-narodni-prirodni-pamatka--6252471	http://www.rozhlas.cz/priroda/krasy/_zprava/dalejsky-profil-narodni-prirodni-pamatka--6252471	k4
2	#num#	k4
3	#num#	k4
LOŽEK	LOŽEK	kA
<g/>
,	,	kIx,
Vojen	vojna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volba	volba	k1gFnSc1
péče	péče	k1gFnSc2
o	o	k7c4
chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ochrana	ochrana	k1gFnSc1
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
60	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
131	#num#	k4
<g/>
-	-	kIx~
<g/>
134	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NEUHÄUSL	NEUHÄUSL	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přírodní	přírodní	k2eAgInSc1d1
park	park	k1gInSc1
Prokopské	prokopský	k2eAgFnSc2d1
a	a	k8xC
Dalejské	Dalejský	k2eAgNnSc4d1
údolí-Teoretické	údolí-Teoretický	k2eAgNnSc4d1
zdůvodnění	zdůvodnění	k1gNnSc4
koncepce	koncepce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Staletá	staletý	k2eAgFnSc1d1
Praha	Praha	k1gFnSc1
XV	XV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sborník	sborník	k1gInSc1
Pražského	pražský	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
státní	státní	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
a	a	k8xC
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Panorama	panorama	k1gNnSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
293	#num#	k4
<g/>
-	-	kIx~
<g/>
300.1	300.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
NĚMEC	Němec	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prokopské	prokopský	k2eAgInPc1d1
a	a	k8xC
Dalejské	Dalejský	k2eAgInPc4d1
údolí-přírodní	údolí-přírodní	k2eAgInPc4d1
park	park	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Consult	Consult	k1gMnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
142	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Dalejský	Dalejský	k2eAgInSc1d1
profil	profil	k1gInSc4
<g/>
,	,	kIx,
s.	s.	k?
86	#num#	k4
<g/>
-	-	kIx~
<g/>
89	#num#	k4
<g/>
.	.	kIx.
.1	.1	k4
2	#num#	k4
BOUČEK	Bouček	k1gMnSc1
<g/>
,	,	kIx,
Bedřich	Bedřich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stratigrafie	stratigrafie	k1gFnSc1
siluru	silur	k1gInSc2
v	v	k7c6
dalejském	dalejský	k2eAgNnSc6d1
údolí	údolí	k1gNnSc6
u	u	k7c2
Prahy	Praha	k1gFnSc2
a	a	k8xC
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
nejbližším	blízký	k2eAgNnSc6d3
okolí	okolí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnPc2
<g/>
:	:	kIx,
Rozpravy	rozprava	k1gFnPc4
České	český	k2eAgFnSc2d1
Akademie	akademie	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1937	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
27	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
..	..	k?
<g/>
↑	↑	k?
CHLUPÁČ	chlupáč	k1gMnSc1
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geologické	geologický	k2eAgFnPc4d1
zajímavosti	zajímavost	k1gFnPc4
pražského	pražský	k2eAgNnSc2d1
okolí	okolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
252	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Z	Z	kA
Řeporyj	Řeporyj	k1gFnSc1
do	do	k7c2
Holyně	Holyně	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
83	#num#	k4
<g/>
-	-	kIx~
<g/>
86	#num#	k4
<g/>
.	.	kIx.
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
15	#num#	k4
16	#num#	k4
17	#num#	k4
18	#num#	k4
CHLUPÁČ	chlupáč	k1gMnSc1
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vycházky	vycházka	k1gFnPc1
za	za	k7c7
geologickou	geologický	k2eAgFnSc7d1
minulostí	minulost	k1gFnSc7
Prahy	Praha	k1gFnSc2
a	a	k8xC
okolí	okolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
279	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Z	Z	kA
Řeporyj	Řeporyj	k1gFnSc1
do	do	k7c2
Holyně	Holyně	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
96	#num#	k4
<g/>
-	-	kIx~
<g/>
101	#num#	k4
<g/>
.	.	kIx.
.	.	kIx.
<g/>
↑	↑	k?
http://www.geology.cz/aplikace/encyklopedie/term.pl?pyroklasticke_horniny	http://www.geology.cz/aplikace/encyklopedie/term.pl?pyroklasticke_hornina	k1gFnSc2
<g/>
↑	↑	k?
http://www.geology.cz/aplikace/encyklopedie/term.pl?puda1	http://www.geology.cz/aplikace/encyklopedie/term.pl?puda1	k4
2	#num#	k4
3	#num#	k4
TUREK	Turek	k1gMnSc1
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ztracená	ztracený	k2eAgFnSc1d1
moře	moře	k1gNnSc2
uprostřed	uprostřed	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
193	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Ordovik	ordovik	k1gInSc1
<g/>
,	,	kIx,
Silur	silur	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
31	#num#	k4
<g/>
-	-	kIx~
<g/>
120	#num#	k4
<g/>
.	.	kIx.
.1	.1	k4
2	#num#	k4
3	#num#	k4
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.wmap.cz	www.wmap.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
CHVÁTALOVÁ	Chvátalová	k1gFnSc1
<g/>
,	,	kIx,
Lenka	Lenka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylinné	bylinný	k2eAgInPc1d1
patro	patro	k1gNnSc4
v	v	k7c6
lesnických	lesnický	k2eAgFnPc6d1
výsadbách	výsadba	k1gFnPc6
v	v	k7c6
přírodním	přírodní	k2eAgInSc6d1
parku	park	k1gInSc6
Prokopské	prokopský	k2eAgNnSc1d1
a	a	k8xC
Dalejské	Dalejský	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
46	#num#	k4
s.	s.	k?
diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
UK	UK	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
.	.	kIx.
↑	↑	k?
http://botany.cz/cs/gagea-bohemica/	http://botany.cz/cs/gagea-bohemica/	k?
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.wmap.cz	www.wmap.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.wmap.cz	www.wmap.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.nm.cz	www.nm.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.cittadella.cz/europarc/index.php?p=index&	http://www.cittadella.cz/europarc/index.php?p=index&	k1gMnSc1
<g/>
↑	↑	k?
http://www.stezky.info/zrusene-stezky/ns-reporyje-hlubocepy-udolim-dalejskeho-potoka.htm	http://www.stezky.info/zrusene-stezky/ns-reporyje-hlubocepy-udolim-dalejskeho-potoka.htm	k1gMnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
Prahy	Praha	k1gFnSc2
</s>
