Důvodem	důvod	k1gInSc7
ochrany	ochrana	k1gFnSc2
je	být	k5eAaImIp3nS
geologický	geologický	k2eAgInSc4d1
profil	profil	k1gInSc4
<g/>
,	,	kIx,
sedimenty	sediment	k1gInPc4
a	a	k8xC
vulkanity	vulkanita	k1gFnPc4
ordovického	ordovický	k2eAgNnSc2d1
<g/>
,	,	kIx,
silurského	silurský	k2eAgNnSc2d1
a	a	k8xC
spodnodevonského	spodnodevonský	k2eAgNnSc2d1
stáří	stáří	k1gNnSc2
<g/>
.	.	kIx.
