<s>
Romain	Romain	k1gMnSc1	Romain
Rolland	Rolland	k1gMnSc1	Rolland
[	[	kIx(	[
<g/>
rolan	rolan	k1gMnSc1	rolan
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
Clamecy	Clameca	k1gFnPc1	Clameca
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
Vézelay	Vézelay	k1gInPc4	Vézelay
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
esejista	esejista	k1gMnSc1	esejista
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Romain	Romain	k1gMnSc1	Romain
Rolland	Rolland	k1gMnSc1	Rolland
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
v	v	k7c6	v
burgundském	burgundský	k2eAgNnSc6d1	burgundské
městečku	městečko	k1gNnSc6	městečko
Clamecy	Clameca	k1gFnSc2	Clameca
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
notáře	notář	k1gMnSc2	notář
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
již	již	k9	již
v	v	k7c6	v
chlapeckém	chlapecký	k2eAgInSc6d1	chlapecký
věku	věk	k1gInSc6	věk
projevoval	projevovat	k5eAaImAgMnS	projevovat
veliké	veliký	k2eAgNnSc4d1	veliké
nadání	nadání	k1gNnSc4	nadání
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
jeho	jeho	k3xOp3gNnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
matky	matka	k1gFnSc2	matka
<g/>
)	)	kIx)	)
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
získat	získat	k5eAaPmF	získat
co	co	k9	co
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
Rolland	Rolland	k1gMnSc1	Rolland
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1886	[number]	k4	1886
až	až	k9	až
1889	[number]	k4	1889
historii	historie	k1gFnSc4	historie
na	na	k7c6	na
elitní	elitní	k2eAgFnSc6d1	elitní
škole	škola	k1gFnSc6	škola
Ecole	Ecola	k1gFnSc3	Ecola
Normale	Normala	k1gFnSc3	Normala
Supérieure	Supérieur	k1gMnSc5	Supérieur
a	a	k8xC	a
pak	pak	k9	pak
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k6eAd1	také
zahájil	zahájit	k5eAaPmAgMnS	zahájit
svou	svůj	k3xOyFgFnSc4	svůj
literární	literární	k2eAgFnSc4d1	literární
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1893	[number]	k4	1893
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
byl	být	k5eAaImAgInS	být
profesorem	profesor	k1gMnSc7	profesor
dějin	dějiny	k1gFnPc2	dějiny
umění	umění	k1gNnSc4	umění
na	na	k7c6	na
Ecole	Ecola	k1gFnSc6	Ecola
Normale	Normala	k1gFnSc6	Normala
a	a	k8xC	a
přednášel	přednášet	k5eAaImAgMnS	přednášet
dějiny	dějiny	k1gFnPc4	dějiny
hudby	hudba	k1gFnSc2	hudba
na	na	k7c6	na
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
byl	být	k5eAaImAgInS	být
hudebním	hudební	k2eAgMnSc7d1	hudební
kritikem	kritik	k1gMnSc7	kritik
a	a	k8xC	a
psal	psát	k5eAaImAgInS	psát
hudební	hudební	k2eAgFnSc4d1	hudební
monografie	monografie	k1gFnPc4	monografie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
výhradně	výhradně	k6eAd1	výhradně
umělecké	umělecký	k2eAgFnSc3d1	umělecká
činnosti	činnost	k1gFnSc3	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
začal	začít	k5eAaPmAgMnS	začít
Rolland	Rolland	k1gMnSc1	Rolland
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
stěžejním	stěžejní	k2eAgNnSc6d1	stěžejní
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
románu	román	k1gInSc6	román
Jan	Jan	k1gMnSc1	Jan
Kryštof	Kryštof	k1gMnSc1	Kryštof
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyšel	vyjít	k5eAaPmAgInS	vyjít
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
a	a	k8xC	a
za	za	k7c4	za
který	který	k3yIgInSc4	který
byl	být	k5eAaImAgMnS	být
oceněn	oceněn	k2eAgInSc1d1	oceněn
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
"	"	kIx"	"
<g/>
...	...	k?	...
jako	jako	k8xS	jako
projev	projev	k1gInSc1	projev
vysoké	vysoký	k2eAgFnSc2d1	vysoká
pocty	pocta	k1gFnSc2	pocta
za	za	k7c4	za
vznešený	vznešený	k2eAgInSc4d1	vznešený
idealismus	idealismus	k1gInSc4	idealismus
jeho	jeho	k3xOp3gFnPc2	jeho
literárních	literární	k2eAgFnPc2d1	literární
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
za	za	k7c4	za
sympatii	sympatie	k1gFnSc4	sympatie
a	a	k8xC	a
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
pravdě	pravda	k1gFnSc3	pravda
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
líčil	líčit	k5eAaImAgMnS	líčit
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
lidí	člověk	k1gMnPc2	člověk
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgFnSc4d1	finanční
částku	částka	k1gFnSc4	částka
s	s	k7c7	s
cenou	cena	k1gFnSc7	cena
spojenou	spojený	k2eAgFnSc7d1	spojená
ale	ale	k8xC	ale
Rolland	Rolland	k1gMnSc1	Rolland
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
věnoval	věnovat	k5eAaPmAgMnS	věnovat
na	na	k7c4	na
zmírnění	zmírnění	k1gNnSc4	zmírnění
útrap	útrapa	k1gFnPc2	útrapa
lidí	člověk	k1gMnPc2	člověk
postižených	postižený	k2eAgMnPc2d1	postižený
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
humanistické	humanistický	k2eAgInPc4d1	humanistický
a	a	k8xC	a
pacifistické	pacifistický	k2eAgInPc4d1	pacifistický
názory	názor	k1gInPc4	názor
Rolland	Rolland	k1gMnSc1	Rolland
projevoval	projevovat	k5eAaImAgMnS	projevovat
jednak	jednak	k8xC	jednak
prací	práce	k1gFnPc2	práce
v	v	k7c6	v
nově	nově	k6eAd1	nově
vzniklém	vzniklý	k2eAgInSc6d1	vzniklý
Červeném	červený	k2eAgInSc6d1	červený
kříži	kříž	k1gInSc6	kříž
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
ovlivňováním	ovlivňování	k1gNnSc7	ovlivňování
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
četnými	četný	k2eAgFnPc7d1	četná
esejemi	esej	k1gFnPc7	esej
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
,	,	kIx,	,
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejsilnějších	silný	k2eAgMnPc2d3	nejsilnější
duchů	duch	k1gMnPc2	duch
kulturní	kulturní	k2eAgFnSc2d1	kulturní
Evropy	Evropa	k1gFnSc2	Evropa
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Dlouholetá	dlouholetý	k2eAgFnSc1d1	dlouholetá
práce	práce	k1gFnSc1	práce
na	na	k7c6	na
Janu	Jan	k1gMnSc6	Jan
Kryštofovi	Kryštof	k1gMnSc6	Kryštof
Rollanda	Rolland	k1gMnSc2	Rolland
natolik	natolik	k6eAd1	natolik
svazovala	svazovat	k5eAaImAgFnS	svazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
díla	dílo	k1gNnSc2	dílo
toužil	toužit	k5eAaImAgMnS	toužit
napsat	napsat	k5eAaPmF	napsat
něco	něco	k3yInSc4	něco
méně	málo	k6eAd2	málo
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
reakce	reakce	k1gFnSc1	reakce
proti	proti	k7c3	proti
desítiletí	desítiletí	k1gNnSc3	desítiletí
nevolnosti	nevolnost	k1gFnSc2	nevolnost
v	v	k7c6	v
brnění	brnění	k1gNnSc6	brnění
Jana	Jan	k1gMnSc2	Jan
Kryštofa	Kryštof	k1gMnSc2	Kryštof
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
sice	sice	k8xC	sice
bylo	být	k5eAaImAgNnS	být
děláno	dělat	k5eAaImNgNnS	dělat
na	na	k7c4	na
mou	můj	k3xOp1gFnSc4	můj
míru	míra	k1gFnSc4	míra
<g/>
.	.	kIx.	.
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
mi	já	k3xPp1nSc3	já
bylo	být	k5eAaImAgNnS	být
příliš	příliš	k6eAd1	příliš
těsné	těsný	k2eAgNnSc1d1	těsné
<g/>
.	.	kIx.	.
</s>
<s>
Cítil	cítit	k5eAaImAgMnS	cítit
jsem	být	k5eAaImIp1nS	být
nepřekonatelnou	překonatelný	k2eNgFnSc4d1	nepřekonatelná
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
volné	volný	k2eAgFnSc6d1	volná
galské	galský	k2eAgFnSc6d1	galská
veselosti	veselost	k1gFnSc6	veselost
<g/>
,	,	kIx,	,
až	až	k9	až
bezohlednosti	bezohlednost	k1gFnSc2	bezohlednost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Výsledkem	výsledek	k1gInSc7	výsledek
této	tento	k3xDgFnSc2	tento
touhy	touha	k1gFnSc2	touha
byl	být	k5eAaImAgInS	být
román	román	k1gInSc1	román
Colas	Colasa	k1gFnPc2	Colasa
Breugnon	Breugnon	k1gInSc1	Breugnon
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
díky	díky	k7c3	díky
překladu	překlad	k1gInSc6	překlad
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
Zaorálka	Zaorálek	k1gMnSc4	Zaorálek
zdomácněl	zdomácnět	k5eAaPmAgInS	zdomácnět
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
člověk	člověk	k1gMnSc1	člověk
ještě	ještě	k6eAd1	ještě
žije	žít	k5eAaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Rollandově	Rollandův	k2eAgNnSc6d1	Rollandův
rodném	rodný	k2eAgNnSc6d1	rodné
Burgundsku	Burgundsko	k1gNnSc6	Burgundsko
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
zřejmě	zřejmě	k6eAd1	zřejmě
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
nejčtenější	čtený	k2eAgNnSc4d3	nejčtenější
a	a	k8xC	a
nejoblíbenější	oblíbený	k2eAgNnSc4d3	nejoblíbenější
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
napsal	napsat	k5eAaPmAgMnS	napsat
ještě	ještě	k9	ještě
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
méně	málo	k6eAd2	málo
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
práce	práce	k1gFnPc4	práce
-	-	kIx~	-
novelu	novela	k1gFnSc4	novela
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
Lucie	Lucie	k1gFnSc1	Lucie
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
a	a	k8xC	a
román	román	k1gInSc1	román
Clerambault	Clerambaulta	k1gFnPc2	Clerambaulta
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
Rolland	Rolland	k1gMnSc1	Rolland
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
druhém	druhý	k4xOgInSc6	druhý
rozsáhlém	rozsáhlý	k2eAgInSc6d1	rozsáhlý
románu	román	k1gInSc6	román
s	s	k7c7	s
názvem	název	k1gInSc7	název
Okouzlená	okouzlený	k2eAgFnSc1d1	okouzlená
duše	duše	k1gFnSc1	duše
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
známou	známý	k2eAgFnSc7d1	známá
postavou	postava	k1gFnSc7	postava
světové	světový	k2eAgFnSc2d1	světová
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
Francie	Francie	k1gFnSc2	Francie
hitlerovským	hitlerovský	k2eAgNnSc7d1	hitlerovské
Německem	Německo	k1gNnSc7	Německo
se	se	k3xPyFc4	se
nacisté	nacista	k1gMnPc1	nacista
neodvážili	odvážit	k5eNaPmAgMnP	odvážit
Rollanda	Rolland	k1gMnSc4	Rolland
zatknout	zatknout	k5eAaPmF	zatknout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nezpůsobili	způsobit	k5eNaPmAgMnP	způsobit
světovou	světový	k2eAgFnSc4d1	světová
vlnu	vlna	k1gFnSc4	vlna
nevole	nevole	k1gFnSc2	nevole
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rolland	Rolland	k1gMnSc1	Rolland
propagoval	propagovat	k5eAaImAgMnS	propagovat
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
formu	forma	k1gFnSc4	forma
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
fašismus	fašismus	k1gInSc4	fašismus
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc4	jeho
představitele	představitel	k1gMnSc4	představitel
i	i	k9	i
Mnichovskou	mnichovský	k2eAgFnSc4d1	Mnichovská
dohodu	dohoda	k1gFnSc4	dohoda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
Molotovova-Ribbentropova	Molotovova-Ribbentropův	k2eAgInSc2d1	Molotovova-Ribbentropův
paktu	pakt	k1gInSc2	pakt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
kritický	kritický	k2eAgInSc1d1	kritický
postoj	postoj	k1gInSc1	postoj
i	i	k9	i
ke	k	k7c3	k
Stalinovi	Stalin	k1gMnSc3	Stalin
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
prozaických	prozaický	k2eAgFnPc2d1	prozaická
prací	práce	k1gFnPc2	práce
je	být	k5eAaImIp3nS	být
Rolland	Rolland	k1gMnSc1	Rolland
autorem	autor	k1gMnSc7	autor
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
životopisů	životopis	k1gInPc2	životopis
významných	významný	k2eAgFnPc2d1	významná
světových	světový	k2eAgFnPc2d1	světová
osobností	osobnost	k1gFnPc2	osobnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
,	,	kIx,	,
Michelangelo	Michelangela	k1gFnSc5	Michelangela
nebo	nebo	k8xC	nebo
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
mnoha	mnoho	k4c2	mnoho
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
tzv.	tzv.	kA	tzv.
lidové	lidový	k2eAgNnSc1d1	lidové
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
divadla	divadlo	k1gNnSc2	divadlo
pro	pro	k7c4	pro
nejširší	široký	k2eAgFnPc4d3	nejširší
lidové	lidový	k2eAgFnPc4d1	lidová
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
by	by	kYmCp3nS	by
jim	on	k3xPp3gFnPc3	on
zprostředkovalo	zprostředkovat	k5eAaPmAgNnS	zprostředkovat
hluboké	hluboký	k2eAgInPc4d1	hluboký
mravní	mravní	k2eAgInSc4d1	mravní
ideály	ideál	k1gInPc4	ideál
<g/>
,	,	kIx,	,
viděl	vidět	k5eAaImAgMnS	vidět
Rolland	Rolland	k1gMnSc1	Rolland
možnost	možnost	k1gFnSc4	možnost
povznesení	povznesení	k1gNnSc2	povznesení
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Romain	Romain	k1gMnSc1	Romain
Rolland	Rolland	k1gMnSc1	Rolland
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
v	v	k7c6	v
Burgundsku	Burgundsko	k1gNnSc6	Burgundsko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městečku	městečko	k1gNnSc6	městečko
Vézelay	Vézelay	k1gInPc4	Vézelay
poblíž	poblíž	k7c2	poblíž
svého	svůj	k3xOyFgNnSc2	svůj
rodiště	rodiště	k1gNnSc2	rodiště
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
vegetarián	vegetarián	k1gMnSc1	vegetarián
<g/>
,	,	kIx,	,
blízce	blízce	k6eAd1	blízce
se	se	k3xPyFc4	se
přátelil	přátelit	k5eAaImAgMnS	přátelit
s	s	k7c7	s
Gándhím	Gándhí	k1gMnSc7	Gándhí
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgNnSc6	který
také	také	k9	také
napsal	napsat	k5eAaBmAgMnS	napsat
knihu	kniha	k1gFnSc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
indickou	indický	k2eAgFnSc4d1	indická
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
o	o	k7c4	o
mystickou	mystický	k2eAgFnSc4d1	mystická
školu	škola	k1gFnSc4	škola
védánta	védánta	k1gFnSc1	védánta
<g/>
.	.	kIx.	.
</s>
<s>
Jean-Christophe	Jean-Christophe	k1gFnSc1	Jean-Christophe
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kryštof	Kryštof	k1gMnSc1	Kryštof
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
románový	románový	k2eAgInSc1d1	románový
cyklus	cyklus	k1gInSc1	cyklus
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
román-řeka	román-řeka	k1gFnSc1	román-řeka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
koncipovaný	koncipovaný	k2eAgInSc1d1	koncipovaný
jako	jako	k8xS	jako
symfonie	symfonie	k1gFnPc1	symfonie
o	o	k7c6	o
osudech	osud	k1gInPc6	osud
nadaného	nadaný	k2eAgMnSc4d1	nadaný
hudebního	hudební	k2eAgMnSc4d1	hudební
skladatele	skladatel	k1gMnSc4	skladatel
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
Pařížskou	pařížský	k2eAgFnSc7d1	Pařížská
komunou	komuna	k1gFnSc7	komuna
a	a	k8xC	a
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Colas	Colas	k1gMnSc1	Colas
Breugnon	Breugnon	k1gMnSc1	Breugnon
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
jako	jako	k9	jako
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
člověk	člověk	k1gMnSc1	člověk
ještě	ještě	k9	ještě
žije	žít	k5eAaImIp3nS	žít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svérázný	svérázný	k2eAgInSc4d1	svérázný
kratší	krátký	k2eAgInSc4d2	kratší
historický	historický	k2eAgInSc4d1	historický
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
v	v	k7c6	v
autorově	autorův	k2eAgNnSc6d1	autorovo
rodném	rodný	k2eAgNnSc6d1	rodné
Burgundsku	Burgundsko	k1gNnSc6	Burgundsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
ústřední	ústřední	k2eAgFnSc7d1	ústřední
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
prostý	prostý	k2eAgMnSc1d1	prostý
člověk	člověk	k1gMnSc1	člověk
stárnoucí	stárnoucí	k2eAgMnSc1d1	stárnoucí
truhlář	truhlář	k1gMnSc1	truhlář
a	a	k8xC	a
řezbář	řezbář	k1gMnSc1	řezbář
Colas	Colas	k1gMnSc1	Colas
Breugnon	Breugnon	k1gMnSc1	Breugnon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
miluje	milovat	k5eAaImIp3nS	milovat
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
dobré	dobrý	k2eAgNnSc4d1	dobré
jídlo	jídlo	k1gNnSc4	jídlo
a	a	k8xC	a
pití	pití	k1gNnSc4	pití
<g/>
,	,	kIx,	,
rád	rád	k6eAd1	rád
si	se	k3xPyFc3	se
zažertuje	zažertovat	k5eAaPmIp3nS	zažertovat
s	s	k7c7	s
pěknou	pěkný	k2eAgFnSc7d1	pěkná
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
dokáže	dokázat	k5eAaPmIp3nS	dokázat
bránit	bránit	k5eAaImF	bránit
život	život	k1gInSc4	život
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
proti	proti	k7c3	proti
jejich	jejich	k3xOp3gMnPc3	jejich
nepřátelům	nepřítel	k1gMnPc3	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Colas	Colas	k1gInSc1	Colas
prožije	prožít	k5eAaPmIp3nS	prožít
tragický	tragický	k2eAgInSc4d1	tragický
rok	rok	k1gInSc4	rok
<g/>
;	;	kIx,	;
zemře	zemřít	k5eAaPmIp3nS	zemřít
mu	on	k3xPp3gMnSc3	on
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
vyhoří	vyhořet	k5eAaPmIp3nS	vyhořet
a	a	k8xC	a
přijde	přijít	k5eAaPmIp3nS	přijít
o	o	k7c4	o
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgFnPc2	svůj
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
nakazí	nakazit	k5eAaPmIp3nS	nakazit
se	se	k3xPyFc4	se
morem	mor	k1gInSc7	mor
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
si	se	k3xPyFc3	se
zlomí	zlomit	k5eAaPmIp3nP	zlomit
nohu	noha	k1gFnSc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
stále	stále	k6eAd1	stále
miluje	milovat	k5eAaImIp3nS	milovat
po	po	k7c6	po
galsku	galsko	k1gNnSc6	galsko
život	život	k1gInSc4	život
a	a	k8xC	a
také	také	k9	také
jej	on	k3xPp3gMnSc4	on
učí	učit	k5eAaImIp3nP	učit
milovat	milovat	k5eAaImF	milovat
ostatní	ostatní	k2eAgMnPc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
snahou	snaha	k1gFnSc7	snaha
být	být	k5eAaImF	být
prospěšný	prospěšný	k2eAgInSc1d1	prospěšný
svému	svůj	k3xOyFgNnSc3	svůj
okolí	okolí	k1gNnSc3	okolí
dokládá	dokládat	k5eAaImIp3nS	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečně	skutečně	k6eAd1	skutečně
"	"	kIx"	"
<g/>
dobrý	dobrý	k2eAgMnSc1d1	dobrý
člověk	člověk	k1gMnSc1	člověk
ještě	ještě	k9	ještě
žije	žít	k5eAaImIp3nS	žít
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Colas	Colas	k1gInSc1	Colas
má	mít	k5eAaImIp3nS	mít
dar	dar	k1gInSc4	dar
barvité	barvitý	k2eAgFnSc2d1	barvitá
fantazie	fantazie	k1gFnSc2	fantazie
a	a	k8xC	a
živého	živý	k2eAgNnSc2d1	živé
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
vyprávění	vyprávění	k1gNnSc1	vyprávění
je	být	k5eAaImIp3nS	být
proloženo	proložen	k2eAgNnSc1d1	proloženo
výrazy	výraz	k1gInPc7	výraz
burgundského	burgundský	k2eAgInSc2d1	burgundský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
mnoha	mnoho	k4c7	mnoho
příslovími	přísloví	k1gNnPc7	přísloví
a	a	k8xC	a
pořekadly	pořekadlo	k1gNnPc7	pořekadlo
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
příběhy	příběh	k1gInPc1	příběh
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
čerpají	čerpat	k5eAaImIp3nP	čerpat
ze	z	k7c2	z
života	život	k1gInSc2	život
prostých	prostý	k2eAgMnPc2d1	prostý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
oslavují	oslavovat	k5eAaImIp3nP	oslavovat
jejich	jejich	k3xOp3gInSc4	jejich
důvtip	důvtip	k1gInSc4	důvtip
<g/>
,	,	kIx,	,
tvořivost	tvořivost	k1gFnSc4	tvořivost
a	a	k8xC	a
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
humor	humor	k1gInSc4	humor
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
přehlídkou	přehlídka	k1gFnSc7	přehlídka
francouzského	francouzský	k2eAgInSc2d1	francouzský
folklóru	folklór	k1gInSc2	folklór
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
knihou	kniha	k1gFnSc7	kniha
radosti	radost	k1gFnSc2	radost
a	a	k8xC	a
životního	životní	k2eAgInSc2d1	životní
optimismu	optimismus	k1gInSc2	optimismus
všemu	všecek	k3xTgNnSc3	všecek
navzdory	navzdory	k6eAd1	navzdory
<g/>
.	.	kIx.	.
</s>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
et	et	k?	et
Luce	Luce	k1gNnPc1	Luce
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
Lucie	Lucie	k1gFnSc1	Lucie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
milostná	milostný	k2eAgFnSc1d1	milostná
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
jemně	jemně	k6eAd1	jemně
kreslený	kreslený	k2eAgInSc1d1	kreslený
příběh	příběh	k1gInSc1	příběh
tragické	tragický	k2eAgFnSc2d1	tragická
lásky	láska	k1gFnSc2	láska
mladých	mladý	k2eAgMnPc2d1	mladý
milenců	milenec	k1gMnPc2	milenec
zahalený	zahalený	k2eAgInSc4d1	zahalený
stínem	stín	k1gInSc7	stín
válečného	válečný	k2eAgNnSc2d1	válečné
utrpení	utrpení	k1gNnSc2	utrpení
a	a	k8xC	a
hrůzy	hrůza	k1gFnSc2	hrůza
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
žili	žít	k5eAaImAgMnP	žít
svou	svůj	k3xOyFgFnSc4	svůj
bezmeznou	bezmezný	k2eAgFnSc4d1	bezmezná
láskou	láska	k1gFnSc7	láska
a	a	k8xC	a
svými	svůj	k3xOyFgInPc7	svůj
romantickými	romantický	k2eAgInPc7d1	romantický
sny	sen	k1gInPc7	sen
a	a	k8xC	a
ve	v	k7c6	v
velikonočním	velikonoční	k2eAgInSc6d1	velikonoční
týdnu	týden	k1gInSc6	týden
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
zcela	zcela	k6eAd1	zcela
oddat	oddat	k5eAaPmF	oddat
jeden	jeden	k4xCgInSc4	jeden
druhému	druhý	k4xOgInSc3	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
je	být	k5eAaImIp3nS	být
však	však	k9	však
při	při	k7c6	při
varhanním	varhanní	k2eAgInSc6d1	varhanní
koncertu	koncert	k1gInSc6	koncert
na	na	k7c4	na
velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
Saint-Gervais	Saint-Gervais	k1gFnSc2	Saint-Gervais
při	při	k7c6	při
bombardování	bombardování	k1gNnSc6	bombardování
pohřbil	pohřbít	k5eAaPmAgInS	pohřbít
mohutný	mohutný	k2eAgInSc1d1	mohutný
padající	padající	k2eAgInSc1d1	padající
pilíř	pilíř	k1gInSc1	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Clerambault	Clerambault	k1gMnSc1	Clerambault
<g/>
,	,	kIx,	,
histoire	histoir	k1gInSc5	histoir
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
une	une	k?	une
conscience	conscience	k1gFnSc1	conscience
libre	libr	k1gInSc5	libr
pendant	pendant	k1gInSc1	pendant
la	la	k0	la
guerre	guerr	k1gInSc5	guerr
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
Clerambault	Clerambault	k1gInSc1	Clerambault
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc1	historie
svobodného	svobodný	k2eAgNnSc2d1	svobodné
svědomí	svědomí	k1gNnSc2	svědomí
za	za	k7c2	za
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
Rolland	Rolland	k1gMnSc1	Rolland
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
vina	vina	k1gFnSc1	vina
na	na	k7c6	na
válečném	válečný	k2eAgInSc6d1	válečný
konfliktu	konflikt	k1gInSc6	konflikt
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
nebyla	být	k5eNaImAgFnS	být
jen	jen	k9	jen
na	na	k7c4	na
nepříteli	nepřítel	k1gMnSc3	nepřítel
(	(	kIx(	(
<g/>
na	na	k7c6	na
Německu	Německo	k1gNnSc6	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
âme	âme	k?	âme
enchantée	enchantée	k1gInSc1	enchantée
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
Okouzlená	okouzlený	k2eAgFnSc1d1	okouzlená
duše	duše	k1gFnSc1	duše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
autorův	autorův	k2eAgInSc1d1	autorův
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
románový	románový	k2eAgInSc1d1	románový
cyklus	cyklus	k1gInSc1	cyklus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
části	část	k1gFnPc1	část
se	se	k3xPyFc4	se
jmenují	jmenovat	k5eAaBmIp3nP	jmenovat
Annette	Annett	k1gInSc5	Annett
et	et	k?	et
Sylvie	Sylvie	k1gFnPc1	Sylvie
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
Anneta	Anneta	k1gFnSc1	Anneta
a	a	k8xC	a
Sylvie	Sylvie	k1gFnSc1	Sylvie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Été	Été	k1gMnSc1	Été
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
Léto	léto	k1gNnSc1	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mè	Mè	k1gMnSc1	Mè
et	et	k?	et
fils	fils	k1gInSc1	fils
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
Matka	matka	k1gFnSc1	matka
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
<g/>
)	)	kIx)	)
a	a	k8xC	a
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Annonciatrice	Annonciatrika	k1gFnSc3	Annonciatrika
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
Zvěstovatelka	zvěstovatelka	k1gFnSc1	zvěstovatelka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
zrcadlí	zrcadlit	k5eAaImIp3nS	zrcadlit
historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
francouzské	francouzský	k2eAgFnSc2d1	francouzská
a	a	k8xC	a
evropské	evropský	k2eAgFnSc2d1	Evropská
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nástupu	nástup	k1gInSc2	nástup
fašismu	fašismus	k1gInSc2	fašismus
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
hrdinkou	hrdinka	k1gFnSc7	hrdinka
je	být	k5eAaImIp3nS	být
dcera	dcera	k1gFnSc1	dcera
ze	z	k7c2	z
zámožné	zámožný	k2eAgFnSc2d1	zámožná
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pohrdne	pohrdnout	k5eAaPmIp3nS	pohrdnout
pohodlnou	pohodlný	k2eAgFnSc7d1	pohodlná
existencí	existence	k1gFnSc7	existence
a	a	k8xC	a
trpce	trpce	k6eAd1	trpce
a	a	k8xC	a
bolestně	bolestně	k6eAd1	bolestně
si	se	k3xPyFc3	se
vybojovává	vybojovávat	k5eAaImIp3nS	vybojovávat
proti	proti	k7c3	proti
společenským	společenský	k2eAgFnPc3d1	společenská
konvencím	konvence	k1gFnPc3	konvence
své	svůj	k3xOyFgNnSc4	svůj
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
nezávislý	závislý	k2eNgInSc4d1	nezávislý
vlastní	vlastní	k2eAgInSc4d1	vlastní
osud	osud	k1gInSc4	osud
a	a	k8xC	a
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
neprovdána	provdán	k2eNgMnSc4d1	provdán
vychovává	vychovávat	k5eAaImIp3nS	vychovávat
<g/>
,	,	kIx,	,
dospívá	dospívat	k5eAaImIp3nS	dospívat
již	již	k6eAd1	již
k	k	k7c3	k
myšlence	myšlenka	k1gFnSc3	myšlenka
boje	boj	k1gInSc2	boj
za	za	k7c4	za
sociální	sociální	k2eAgFnSc4d1	sociální
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
vyznavačem	vyznavač	k1gMnSc7	vyznavač
Říjnové	říjnový	k2eAgFnSc2d1	říjnová
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
socialismu	socialismus	k1gInSc2	socialismus
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
umírá	umírat	k5eAaImIp3nS	umírat
jako	jako	k9	jako
oběť	oběť	k1gFnSc4	oběť
italského	italský	k2eAgInSc2d1	italský
fašismu	fašismus	k1gInSc2	fašismus
<g/>
.	.	kIx.	.
</s>
<s>
Anneta	Anneta	k1gFnSc1	Anneta
dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
boji	boj	k1gInSc6	boj
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
zamýšlí	zamýšlet	k5eAaImIp3nS	zamýšlet
a	a	k8xC	a
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nežila	žít	k5eNaImAgFnS	žít
nadarmo	nadarmo	k6eAd1	nadarmo
<g/>
.	.	kIx.	.
</s>
<s>
Orsino	Orsino	k1gNnSc1	Orsino
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
baglioni	baglioň	k1gFnSc3	baglioň
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
Baglioniové	Baglioniový	k2eAgInPc1d1	Baglioniový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Caligula	Caligula	k1gFnSc1	Caligula
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Siè	Siè	k1gFnSc2	Siè
de	de	k?	de
Mantoue	Mantou	k1gFnSc2	Mantou
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
Obléhání	obléhání	k1gNnSc1	obléhání
Mantovy	Mantova	k1gFnSc2	Mantova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jeanne	Jeann	k1gInSc5	Jeann
de	de	k?	de
Piennes	Piennes	k1gInSc1	Piennes
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Les	les	k1gInSc4	les
tragédies	tragédiesa	k1gFnPc2	tragédiesa
de	de	k?	de
la	la	k1gNnSc6	la
foi	foi	k?	foi
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
Tragédie	tragédie	k1gFnSc1	tragédie
víry	víra	k1gFnSc2	víra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dramatická	dramatický	k2eAgFnSc1d1	dramatická
trilogie	trilogie	k1gFnSc1	trilogie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
Rolland	Rolland	k1gMnSc1	Rolland
ukázal	ukázat	k5eAaPmAgMnS	ukázat
na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
příkladech	příklad	k1gInPc6	příklad
obrozující	obrozující	k2eAgFnSc4d1	obrozující
sílu	síla	k1gFnSc4	síla
víry	víra	k1gFnSc2	víra
(	(	kIx(	(
<g/>
na	na	k7c6	na
víře	víra	k1gFnSc6	víra
náboženské	náboženský	k2eAgFnSc6d1	náboženská
<g/>
,	,	kIx,	,
vlastenecké	vlastenecký	k2eAgFnSc6d1	vlastenecká
a	a	k8xC	a
revolucionářské	revolucionářský	k2eAgFnSc6d1	revolucionářská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trilogie	trilogie	k1gFnSc1	trilogie
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
Saint	Saint	k1gInSc1	Saint
Luis	Luisa	k1gFnPc2	Luisa
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Aërt	Aërt	k1gMnSc1	Aërt
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
Aertes	Aertes	k1gInSc1	Aertes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Triomphe	Triomphe	k1gNnSc2	Triomphe
de	de	k?	de
la	la	k1gNnSc2	la
raison	raison	k1gFnPc2	raison
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
Vítězství	vítězství	k1gNnSc1	vítězství
rozumu	rozum	k1gInSc2	rozum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Le	Le	k?	Le
Théâtre	Théâtr	k1gMnSc5	Théâtr
de	de	k?	de
la	la	k1gNnPc2	la
Révolution	Révolution	k1gInSc1	Révolution
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cyklus	cyklus	k1gInSc1	cyklus
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
za	za	k7c4	za
Íliadu	Íliada	k1gFnSc4	Íliada
francouzského	francouzský	k2eAgInSc2d1	francouzský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Cyklus	cyklus	k1gInSc1	cyklus
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k9	jako
dramatická	dramatický	k2eAgFnSc1d1	dramatická
trilogie	trilogie	k1gFnSc1	trilogie
her	hra	k1gFnPc2	hra
Vlci	vlk	k1gMnPc1	vlk
<g/>
,	,	kIx,	,
Danton	Danton	k1gInSc4	Danton
a	a	k8xC	a
Čtrnáctý	čtrnáctý	k4xOgInSc4	čtrnáctý
červenec	červenec	k1gInSc4	červenec
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
dostal	dostat	k5eAaPmAgMnS	dostat
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
Rolland	Rolland	k1gMnSc1	Rolland
jej	on	k3xPp3gMnSc4	on
založil	založit	k5eAaPmAgMnS	založit
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
teoretickém	teoretický	k2eAgInSc6d1	teoretický
spise	spis	k1gInSc6	spis
Divadlo	divadlo	k1gNnSc4	divadlo
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
připojoval	připojovat	k5eAaImAgMnS	připojovat
další	další	k2eAgFnSc4d1	další
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
cyklu	cyklus	k1gInSc2	cyklus
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
zařazena	zařazen	k2eAgFnSc1d1	zařazena
hra	hra	k1gFnSc1	hra
Vítězství	vítězství	k1gNnSc2	vítězství
rozumu	rozum	k1gInSc2	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Les	les	k1gInSc1	les
Loups	Loups	k1gInSc1	Loups
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
Vlci	vlk	k1gMnPc1	vlk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
Rollandova	Rollandův	k2eAgFnSc1d1	Rollandova
uvedená	uvedený	k2eAgFnSc1d1	uvedená
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
Le	Le	k1gFnSc1	Le
Triomphe	Triomphe	k1gFnSc1	Triomphe
de	de	k?	de
la	la	k1gNnSc1	la
raison	raison	k1gFnSc1	raison
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
Vítězství	vítězství	k1gNnSc1	vítězství
rozumu	rozum	k1gInSc2	rozum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Danton	Danton	k1gInSc1	Danton
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
quatorze	quatorze	k1gFnSc2	quatorze
jullet	jullet	k1gInSc1	jullet
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Čtrnáctý	čtrnáctý	k4xOgInSc4	čtrnáctý
červenec	červenec	k1gInSc4	červenec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Jeu	Jeu	k1gFnSc2	Jeu
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
amour	amour	k1gMnSc1	amour
et	et	k?	et
de	de	k?	de
la	la	k1gNnSc2	la
mort	morta	k1gFnPc2	morta
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
Hra	hra	k1gFnSc1	hra
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
a	a	k8xC	a
smrti	smrt	k1gFnSc6	smrt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pâques	Pâques	k1gMnSc1	Pâques
fleuries	fleuries	k1gMnSc1	fleuries
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
Květná	květný	k2eAgFnSc1d1	Květná
neděle	neděle	k1gFnSc1	neděle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prolog	prolog	k1gInSc1	prolog
k	k	k7c3	k
cyklu	cyklus	k1gInSc3	cyklus
Les	les	k1gInSc1	les
Léonidés	Léonidés	k1gInSc1	Léonidés
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
Leonidy	Leonida	k1gFnPc1	Leonida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
epilog	epilog	k1gInSc4	epilog
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
Robespierre	Robespierr	k1gMnSc5	Robespierr
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Le	Le	k?	Le
temps	temps	k1gInSc1	temps
viendra	viendra	k1gFnSc1	viendra
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
Přijde	přijít	k5eAaPmIp3nS	přijít
čas	čas	k1gInSc1	čas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Montespan	Montespana	k1gFnPc2	Montespana
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
Montespanová	Montespanová	k1gFnSc1	Montespanová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Les	les	k1gInSc4	les
Trois	Trois	k1gFnPc2	Trois
amoureuses	amoureusesa	k1gFnPc2	amoureusesa
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
Tři	tři	k4xCgFnPc1	tři
zamilované	zamilovaná	k1gFnPc1	zamilovaná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Empédocle	Empédocle	k1gFnSc1	Empédocle
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
Empedokles	Empedokles	k1gInSc4	Empedokles
<g/>
)	)	kIx)	)
Liluli	Lilule	k1gFnSc4	Lilule
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dramatická	dramatický	k2eAgFnSc1d1	dramatická
protiválečná	protiválečný	k2eAgFnSc1d1	protiválečná
groteska	groteska	k1gFnSc1	groteska
<g/>
.	.	kIx.	.
</s>
<s>
Empédocle	Empédocle	k1gFnSc1	Empédocle
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Agrigente	Agrigent	k1gInSc5	Agrigent
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
Empedokles	Empedokles	k1gInSc1	Empedokles
z	z	k7c2	z
Akragantu	Akragant	k1gInSc2	Akragant
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
esej	esej	k1gFnSc4	esej
<g/>
,	,	kIx,	,
popis	popis	k1gInSc4	popis
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
filozofie	filozofie	k1gFnSc2	filozofie
předsokratovského	předsokratovský	k2eAgMnSc2d1	předsokratovský
lékaře	lékař	k1gMnSc2	lékař
<g/>
,	,	kIx,	,
básníka	básník	k1gMnSc2	básník
a	a	k8xC	a
zakladatele	zakladatel	k1gMnSc2	zakladatel
rétoriky	rétorika	k1gFnSc2	rétorika
<g/>
.	.	kIx.	.
</s>
<s>
Histoire	Histoir	k1gMnSc5	Histoir
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
opéra	opér	k1gMnSc2	opér
avant	avant	k1gMnSc1	avant
Lully	Lulla	k1gMnSc2	Lulla
et	et	k?	et
Scarlatti	Scarlatť	k1gFnSc2	Scarlatť
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
před	před	k7c4	před
Lullym	Lullym	k1gInSc4	Lullym
a	a	k8xC	a
Scarlattim	Scarlattim	k1gInSc4	Scarlattim
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
Rollandova	Rollandův	k2eAgFnSc1d1	Rollandova
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
předložena	předložit	k5eAaPmNgFnS	předložit
jako	jako	k8xS	jako
disertační	disertační	k2eAgFnSc1d1	disertační
práce	práce	k1gFnSc1	práce
a	a	k8xC	a
odměněna	odměnit	k5eAaPmNgFnS	odměnit
cenou	cena	k1gFnSc7	cena
Akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
objasňuje	objasňovat	k5eAaImIp3nS	objasňovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
na	na	k7c6	na
italské	italský	k2eAgFnSc6d1	italská
půdě	půda	k1gFnSc6	půda
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
již	již	k6eAd1	již
existujících	existující	k2eAgFnPc2d1	existující
hudebních	hudební	k2eAgFnPc2d1	hudební
forem	forma	k1gFnPc2	forma
nový	nový	k2eAgInSc1d1	nový
scénický	scénický	k2eAgInSc1d1	scénický
útvar	útvar	k1gInSc1	útvar
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgFnSc4d1	spojující
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc4	zpěv
a	a	k8xC	a
popřípadě	popřípadě	k6eAd1	popřípadě
i	i	k9	i
tanec	tanec	k1gInSc4	tanec
v	v	k7c4	v
jednotný	jednotný	k2eAgInSc4d1	jednotný
umělecký	umělecký	k2eAgInSc4d1	umělecký
počin	počin	k1gInSc4	počin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
základem	základ	k1gInSc7	základ
dnešní	dnešní	k2eAgFnSc2d1	dnešní
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
François-Millet	François-Millet	k1gInSc1	François-Millet
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
monografie	monografie	k1gFnSc1	monografie
francouzského	francouzský	k2eAgMnSc2d1	francouzský
malíře	malíř	k1gMnSc2	malíř
Jeana	Jean	k1gMnSc2	Jean
Françoise	Françoise	k1gFnSc1	Françoise
Milleta	Milleta	k1gFnSc1	Milleta
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc5	Le
Théâtre	Théâtr	k1gMnSc5	Théâtr
du	du	k?	du
peuple	peuple	k6eAd1	peuple
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
lidu	lid	k1gInSc2	lid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teoretická	teoretický	k2eAgFnSc1d1	teoretická
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Vie	Vie	k1gFnSc2	Vie
de	de	k?	de
Beethoven	Beethoven	k1gMnSc1	Beethoven
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
Život	život	k1gInSc1	život
Beethovenův	Beethovenův	k2eAgInSc1d1	Beethovenův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
monografie	monografie	k1gFnPc1	monografie
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Vie	Vie	k1gFnSc2	Vie
de	de	k?	de
Michel-Ange	Michel-Ang	k1gFnSc2	Michel-Ang
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
Život	život	k1gInSc1	život
Michelangelův	Michelangelův	k2eAgInSc1d1	Michelangelův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
monografie	monografie	k1gFnSc1	monografie
<g/>
,	,	kIx,	,
Musiciens	Musiciens	k1gInSc1	Musiciens
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
autrefois	autrefois	k1gFnSc1	autrefois
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Hudebníci	hudebník	k1gMnPc1	hudebník
minulosti	minulost	k1gFnSc2	minulost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
studie	studie	k1gFnSc1	studie
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
o	o	k7c6	o
uvedení	uvedení	k1gNnSc6	uvedení
první	první	k4xOgFnSc2	první
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
o	o	k7c6	o
lidském	lidský	k2eAgInSc6d1	lidský
i	i	k8xC	i
uměleckém	umělecký	k2eAgInSc6d1	umělecký
profilu	profil	k1gInSc6	profil
Jeana-Baptisty	Jeana-Baptista	k1gMnSc2	Jeana-Baptista
Lullyho	Lully	k1gMnSc2	Lully
a	a	k8xC	a
o	o	k7c6	o
hudebních	hudební	k2eAgFnPc6d1	hudební
osobnostech	osobnost	k1gFnPc6	osobnost
jako	jako	k8xC	jako
byl	být	k5eAaImAgMnS	být
Christoph	Christoph	k1gMnSc1	Christoph
Willibald	Willibald	k1gMnSc1	Willibald
Gluck	Gluck	k1gMnSc1	Gluck
<g/>
,	,	kIx,	,
André	André	k1gMnSc1	André
Ernest	Ernest	k1gMnSc1	Ernest
Modeste	Modest	k1gInSc5	Modest
Grétry	Grétr	k1gMnPc7	Grétr
nebo	nebo	k8xC	nebo
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
.	.	kIx.	.
</s>
<s>
Musiciens	Musiciens	k1gInSc1	Musiciens
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
aujourd	aujourd	k1gMnSc1	aujourd
<g/>
'	'	kIx"	'
<g/>
hui	hui	k?	hui
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Hudebníci	hudebník	k1gMnPc1	hudebník
nedávné	dávný	k2eNgFnSc2d1	nedávná
doby	doba	k1gFnSc2	doba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
duchaplné	duchaplný	k2eAgFnPc1d1	duchaplná
a	a	k8xC	a
podnětné	podnětný	k2eAgFnPc1d1	podnětná
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
skladatelích	skladatel	k1gMnPc6	skladatel
Hectoru	Hector	k1gMnSc3	Hector
Berliozovi	Berlioz	k1gMnSc3	Berlioz
<g/>
,	,	kIx,	,
Camillovi	Camill	k1gMnSc3	Camill
Saint-Saënsovi	Saint-Saëns	k1gMnSc3	Saint-Saëns
<g/>
,	,	kIx,	,
Richardu	Richard	k1gMnSc3	Richard
Straussovi	Strauss	k1gMnSc3	Strauss
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
o	o	k7c6	o
Wagnerových	Wagnerových	k2eAgFnPc6d1	Wagnerových
operách	opera	k1gFnPc6	opera
Siegfried	Siegfried	k1gInSc1	Siegfried
a	a	k8xC	a
Tristan	Tristan	k1gInSc1	Tristan
a	a	k8xC	a
Isolda	Isolda	k1gFnSc1	Isolda
<g/>
,	,	kIx,	,
o	o	k7c4	o
Debussyho	Debussy	k1gMnSc4	Debussy
Pelleovi	Pelleus	k1gMnSc6	Pelleus
a	a	k8xC	a
Melisandě	Melisanda	k1gFnSc6	Melisanda
i	i	k8xC	i
o	o	k7c6	o
hudebním	hudební	k2eAgInSc6d1	hudební
životě	život	k1gInSc6	život
Francie	Francie	k1gFnSc2	Francie
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Haendel	Haendlo	k1gNnPc2	Haendlo
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
Händel	Händlo	k1gNnPc2	Händlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
monografie	monografie	k1gFnSc1	monografie
německého	německý	k2eAgMnSc2d1	německý
hudebního	hudební	k2eAgMnSc2d1	hudební
skladatele	skladatel	k1gMnSc2	skladatel
Georga	Georg	k1gMnSc2	Georg
Friedricha	Friedrich	k1gMnSc2	Friedrich
Händela	Händel	k1gMnSc2	Händel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rolland	Rolland	k1gMnSc1	Rolland
sugestivně	sugestivně	k6eAd1	sugestivně
líčí	líčit	k5eAaImIp3nS	líčit
Händelův	Händelův	k2eAgInSc4d1	Händelův
boj	boj	k1gInSc4	boj
s	s	k7c7	s
nezájmem	nezájem	k1gInSc7	nezájem
obecenstva	obecenstvo	k1gNnSc2	obecenstvo
a	a	k8xC	a
konečné	konečná	k1gFnSc2	konečná
vítězství	vítězství	k1gNnSc2	vítězství
jeho	jeho	k3xOp3gNnPc2	jeho
oratorií	oratorium	k1gNnPc2	oratorium
<g/>
,	,	kIx,	,
zvláště	zvláště	k9	zvláště
Judy	judo	k1gNnPc7	judo
Makabejského	makabejský	k2eAgNnSc2d1	Makabejské
<g/>
.	.	kIx.	.
</s>
<s>
La	la	k1gNnSc1	la
Vie	Vie	k1gMnSc2	Vie
de	de	k?	de
Tolstoï	Tolstoï	k1gMnSc2	Tolstoï
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
Život	život	k1gInSc4	život
Tolstého	Tolstý	k2eAgNnSc2d1	Tolstý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
monografie	monografie	k1gFnSc1	monografie
<g/>
,	,	kIx,	,
Au-dessus	Auessus	k1gInSc1	Au-dessus
de	de	k?	de
la	la	k1gNnSc7	la
Męlée	Męlé	k1gFnSc2	Męlé
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
Nad	nad	k7c7	nad
vřavou	vřava	k1gFnSc7	vřava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
protiválečných	protiválečný	k2eAgInPc2d1	protiválečný
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
Inde	Ind	k1gMnSc5	Ind
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
deníková	deníkový	k2eAgFnSc1d1	deníková
kniha	kniha	k1gFnSc1	kniha
zachycující	zachycující	k2eAgInPc4d1	zachycující
Rollandovy	Rollandův	k2eAgInPc4d1	Rollandův
styky	styk	k1gInPc4	styk
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
obrozující	obrozující	k2eAgFnSc2d1	obrozující
se	se	k3xPyFc4	se
Indie	Indie	k1gFnSc2	Indie
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
s	s	k7c7	s
Rabíndranáthem	Rabíndranáth	k1gInSc7	Rabíndranáth
Thákurem	Thákur	k1gInSc7	Thákur
a	a	k8xC	a
Mahátmou	Mahátma	k1gMnSc7	Mahátma
Gándhím	Gándhí	k1gMnSc7	Gándhí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Voyage	Voyage	k1gInSc1	Voyage
musical	musical	k1gInSc1	musical
aux	aux	k?	aux
pays	pays	k1gInSc1	pays
du	du	k?	du
passé	passý	k2eAgNnSc1d1	passé
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
Hudebníkova	hudebníkův	k2eAgFnSc1d1	hudebníkova
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
studií	studie	k1gFnPc2	studie
o	o	k7c6	o
hudbě	hudba	k1gFnSc6	hudba
a	a	k8xC	a
hudebnících	hudebník	k1gMnPc6	hudebník
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
La	la	k1gNnSc1	la
Révolte	Révolte	k1gFnPc2	Révolte
des	des	k1gNnSc1	des
machines	machines	k1gMnSc1	machines
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
Vzpoura	vzpoura	k1gFnSc1	vzpoura
strojů	stroj	k1gInPc2	stroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgInSc1d1	filmový
scénář	scénář	k1gInSc1	scénář
<g/>
,	,	kIx,	,
Mahatma	Mahatma	k1gNnSc1	Mahatma
Gandhi	Gandh	k1gFnSc2	Gandh
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
monografie	monografie	k1gFnSc1	monografie
Mahátmy	Mahátma	k1gFnSc2	Mahátma
Gándhího	Gándhí	k1gMnSc2	Gándhí
<g/>
,	,	kIx,	,
Les	les	k1gInSc4	les
Aimés	Aimésa	k1gFnPc2	Aimésa
du	du	k?	du
Beethoven	Beethoven	k1gMnSc1	Beethoven
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
Beethoven	Beethoven	k1gMnSc1	Beethoven
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
studie	studie	k1gFnSc1	studie
sleduje	sledovat	k5eAaImIp3nS	sledovat
Beethovenova	Beethovenův	k2eAgFnSc1d1	Beethovenova
<g/>
,	,	kIx,	,
setkání	setkání	k1gNnSc1	setkání
se	s	k7c7	s
ženami	žena	k1gFnPc7	žena
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
mnohá	mnohé	k1gNnPc1	mnohé
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
marná	marný	k2eAgNnPc1d1	marné
milostná	milostný	k2eAgNnPc1d1	milostné
vzplanutí	vzplanutí	k1gNnPc1	vzplanutí
a	a	k8xC	a
vítězství	vítězství	k1gNnSc1	vítězství
jeho	jeho	k3xOp3gFnSc2	jeho
mohutné	mohutný	k2eAgFnSc2d1	mohutná
tvůrčí	tvůrčí	k2eAgFnSc2d1	tvůrčí
energie	energie	k1gFnSc2	energie
nad	nad	k7c7	nad
neustálým	neustálý	k2eAgNnSc7d1	neustálé
zklamáním	zklamání	k1gNnSc7	zklamání
v	v	k7c6	v
intimním	intimní	k2eAgInSc6d1	intimní
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
.	.	kIx.	.
</s>
<s>
Les	les	k1gInSc1	les
grandes	grandes	k1gMnSc1	grandes
époques	époques	k1gMnSc1	époques
créatrices	créatrices	k1gMnSc1	créatrices
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
tvůrčí	tvůrčí	k2eAgNnPc1d1	tvůrčí
období	období	k1gNnPc1	období
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
pětidílná	pětidílný	k2eAgFnSc1d1	pětidílná
monografie	monografie	k1gFnSc1	monografie
o	o	k7c6	o
životě	život	k1gInSc6	život
geniálního	geniální	k2eAgMnSc2d1	geniální
německého	německý	k2eAgMnSc2d1	německý
skladatele	skladatel	k1gMnSc2	skladatel
Ludwiga	Ludwig	k1gMnSc2	Ludwig
van	vana	k1gFnPc2	vana
Beethovena	Beethoven	k1gMnSc2	Beethoven
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
autor	autor	k1gMnSc1	autor
uložil	uložit	k5eAaPmAgMnS	uložit
výsledky	výsledek	k1gInPc4	výsledek
svého	svůj	k3xOyFgNnSc2	svůj
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
bádání	bádání	k1gNnSc2	bádání
a	a	k8xC	a
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
o	o	k7c6	o
zachycení	zachycení	k1gNnSc6	zachycení
všech	všecek	k3xTgFnPc2	všecek
dostupných	dostupný	k2eAgFnPc2d1	dostupná
podrobností	podrobnost	k1gFnPc2	podrobnost
o	o	k7c6	o
životě	život	k1gInSc6	život
a	a	k8xC	a
díle	dílo	k1gNnSc6	dílo
velkého	velký	k2eAgMnSc2d1	velký
skladatele	skladatel	k1gMnSc2	skladatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c6	o
postižení	postižení	k1gNnSc6	postižení
jeho	jeho	k3xOp3gFnSc2	jeho
tvůrčí	tvůrčí	k2eAgFnSc2d1	tvůrčí
i	i	k8xC	i
lidské	lidský	k2eAgFnSc2d1	lidská
velikosti	velikost	k1gFnSc2	velikost
v	v	k7c6	v
životních	životní	k2eAgInPc6d1	životní
okamžicích	okamžik	k1gInPc6	okamžik
a	a	k8xC	a
dílech	díl	k1gInPc6	díl
zvlášť	zvlášť	k6eAd1	zvlášť
významných	významný	k2eAgInPc2d1	významný
a	a	k8xC	a
typických	typický	k2eAgInPc2d1	typický
<g/>
.	.	kIx.	.
</s>
<s>
Essai	Essai	k6eAd1	Essai
sur	sur	k?	sur
la	la	k1gNnSc7	la
mystique	mystiqu	k1gFnSc2	mystiqu
et	et	k?	et
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
action	action	k1gInSc1	action
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Inde	Ind	k1gMnSc5	Ind
vivante	vivant	k1gMnSc5	vivant
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Mystický	mystický	k2eAgInSc4d1	mystický
a	a	k8xC	a
činný	činný	k2eAgInSc4d1	činný
život	život	k1gInSc4	život
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Indie	Indie	k1gFnSc2	Indie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
monografií	monografie	k1gFnPc2	monografie
<g/>
:	:	kIx,	:
La	la	k1gNnSc1	la
Vie	Vie	k1gMnSc7	Vie
de	de	k?	de
Ramakrishna	Ramakrishna	k1gFnSc1	Ramakrishna
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
Život	život	k1gInSc1	život
Ramakrišnův	Ramakrišnův	k2eAgInSc1d1	Ramakrišnův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Vie	Vie	k1gFnSc2	Vie
de	de	k?	de
Vivekananda	Vivekananda	k1gFnSc1	Vivekananda
et	et	k?	et
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Évangile	Évangila	k1gFnSc3	Évangila
universel	universela	k1gFnPc2	universela
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Vivekanda	Vivekanda	k1gFnSc1	Vivekanda
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
univerzální	univerzální	k2eAgNnSc1d1	univerzální
evangelium	evangelium	k1gNnSc1	evangelium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Goethe	Goethe	k6eAd1	Goethe
et	et	k?	et
Beethoven	Beethoven	k1gMnSc1	Beethoven
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Goethe	Goethe	k1gFnSc1	Goethe
a	a	k8xC	a
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Quinze	Quinze	k1gFnSc1	Quinze
ans	ans	k?	ans
de	de	k?	de
combat	combat	k5eAaPmF	combat
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
Patnáct	patnáct	k4xCc1	patnáct
let	léto	k1gNnPc2	léto
bojů	boj	k1gInPc2	boj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
článků	článek	k1gInPc2	článek
a	a	k8xC	a
korespondence	korespondence	k1gFnSc2	korespondence
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
jako	jako	k8xC	jako
Sbohem	sbohem	k0	sbohem
minulosti	minulost	k1gFnSc6	minulost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Comment	Comment	k1gInSc1	Comment
empécher	empéchra	k1gFnPc2	empéchra
la	la	k1gNnSc2	la
guerre	guerr	k1gInSc5	guerr
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
Zabraňte	zabránit	k5eAaPmRp2nP	zabránit
válce	válka	k1gFnSc3	válka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Compagnos	Compagnos	k1gMnSc1	Compagnos
de	de	k?	de
route	route	k5eAaPmIp2nP	route
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
Společníci	společník	k1gMnPc1	společník
cesty	cesta	k1gFnSc2	cesta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
jako	jako	k8xC	jako
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
pravdě	pravda	k1gFnSc3	pravda
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc4	soubor
esejí	esej	k1gFnPc2	esej
Le	Le	k1gMnSc1	Le
Voyage	Voyag	k1gFnSc2	Voyag
intérieur	intérieur	k1gMnSc1	intérieur
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
hlubin	hlubina	k1gFnPc2	hlubina
nitra	nitro	k1gNnSc2	nitro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Péguy	Pégua	k1gFnSc2	Pégua
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posmrtně	posmrtně	k6eAd1	posmrtně
vydaná	vydaný	k2eAgFnSc1d1	vydaná
monografie	monografie	k1gFnSc1	monografie
francouzského	francouzský	k2eAgMnSc2d1	francouzský
básníka	básník	k1gMnSc2	básník
a	a	k8xC	a
esejisty	esejista	k1gMnSc2	esejista
<g/>
,	,	kIx,	,
Mémoires	Mémoires	k1gInSc1	Mémoires
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
jako	jako	k9	jako
Paměti	paměť	k1gFnPc1	paměť
a	a	k8xC	a
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posmrtně	posmrtně	k6eAd1	posmrtně
vydané	vydaný	k2eAgFnSc2d1	vydaná
Rollandovy	Rollandův	k2eAgFnSc2d1	Rollandova
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
Journal	Journal	k1gFnSc1	Journal
1915-1943	[number]	k4	1915-1943
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
Denik	Denik	k1gInSc1	Denik
1915	[number]	k4	1915
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posmrtně	posmrtně	k6eAd1	posmrtně
vydané	vydaný	k2eAgInPc4d1	vydaný
záznamy	záznam	k1gInPc4	záznam
z	z	k7c2	z
Rollandova	Rollandův	k2eAgInSc2d1	Rollandův
deníku	deník	k1gInSc2	deník
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Kryštof	Kryštof	k1gMnSc1	Kryštof
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Kočí	Kočí	k1gFnSc1	Kočí
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1916	[number]	k4	1916
<g/>
-	-	kIx~	-
<g/>
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Vobrubová-Veselá	Vobrubová-Veselý	k2eAgFnSc1d1	Vobrubová-Veselý
a	a	k8xC	a
František	František	k1gMnSc1	František
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
vydání	vydání	k1gNnSc1	vydání
v	v	k7c6	v
deseti	deset	k4xCc6	deset
samostatných	samostatný	k2eAgInPc6d1	samostatný
svazích	svah	k1gInPc6	svah
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
Michela	Michel	k1gMnSc2	Michel
Angela	angel	k1gMnSc2	angel
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Kočí	Kočí	k1gFnSc1	Kočí
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Marie	Maria	k1gFnSc2	Maria
Kalašová	Kalašová	k1gFnSc1	Kalašová
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
monografii	monografie	k1gFnSc4	monografie
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Kvasnička	kvasnička	k1gFnSc1	kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
Beethovenův	Beethovenův	k2eAgInSc1d1	Beethovenův
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Kočí	Kočí	k1gFnSc1	Kočí
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Marie	Maria	k1gFnSc2	Maria
Kalašová	Kalašová	k1gFnSc1	Kalašová
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
monografii	monografie	k1gFnSc4	monografie
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
Alois	Alois	k1gMnSc1	Alois
Srdce	srdce	k1gNnSc2	srdce
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
<g/>
,	,	kIx,	,
Zátiší	zátiší	k1gNnSc1	zátiší
-	-	kIx~	-
B.	B.	kA	B.
M.	M.	kA	M.
Klika	Klika	k1gMnSc1	Klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Petr	Petr	k1gMnSc1	Petr
Křička	Křička	k1gMnSc1	Křička
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
L.	L.	kA	L.
N.	N.	kA	N.
Tolstého	Tolstý	k2eAgInSc2d1	Tolstý
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Kočí	Kočí	k1gFnSc1	Kočí
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Marie	Maria	k1gFnSc2	Maria
Kalašová	Kalašová	k1gFnSc1	Kalašová
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
monografii	monografie	k1gFnSc4	monografie
ještě	ještě	k9	ještě
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
člověk	člověk	k1gMnSc1	člověk
ještě	ještě	k9	ještě
žije	žít	k5eAaImIp3nS	žít
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Škeřík	Škeřík	k1gInSc1	Škeřík
Praha	Praha	k1gFnSc1	Praha
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Zaorálek	Zaorálek	k1gMnSc1	Zaorálek
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tento	tento	k3xDgInSc4	tento
román	román	k1gInSc4	román
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
a	a	k8xC	a
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
Lidové	lidový	k2eAgNnSc1d1	lidové
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
a	a	k8xC	a
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
a	a	k8xC	a
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Amosium	Amosium	k1gNnSc1	Amosium
servis	servis	k1gInSc1	servis
v	v	k7c6	v
Karviné	Karviná	k1gFnSc6	Karviná
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
Levné	levný	k2eAgFnSc2d1	levná
knihy	kniha	k1gFnSc2	kniha
KMa	KMa	k1gFnSc2	KMa
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
..	..	k?	..
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
Lucie	Lucie	k1gFnSc1	Lucie
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Škeřík	Škeřík	k1gMnSc1	Škeřík
Praha	Praha	k1gFnSc1	Praha
<g />
.	.	kIx.	.
</s>
<s>
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Zaorálek	Zaorálek	k1gMnSc1	Zaorálek
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
novelu	novela	k1gFnSc4	novela
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Družstevní	družstevní	k2eAgFnSc2d1	družstevní
práce	práce	k1gFnSc2	práce
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Svoboda	svoboda	k1gFnSc1	svoboda
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
a	a	k8xC	a
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
SNDK	SNDK	kA	SNDK
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gMnSc1	Melantrich
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
Práce	práce	k1gFnSc1	práce
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Amosium	Amosium	k1gNnSc1	Amosium
servis	servis	k1gInSc1	servis
v	v	k7c6	v
Karviné	Karviná	k1gFnSc6	Karviná
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
a	a	k8xC	a
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
klub	klub	k1gInSc1	klub
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Clerambault	Clerambault	k1gMnSc1	Clerambault
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josefa	Josefa	k1gFnSc1	Josefa
Hrdinová	Hrdinová	k1gFnSc1	Hrdinová
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
a	a	k8xC	a
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Petr	Petr	k1gMnSc1	Petr
Křička	Křička	k1gMnSc1	Křička
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hru	hra	k1gFnSc4	hra
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
toku	tok	k1gInSc2	tok
1949	[number]	k4	1949
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
ČLDJ	ČLDJ	kA	ČLDJ
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
vřavou	vřava	k1gFnSc7	vřava
válečnou	válečná	k1gFnSc7	válečná
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Kočí	Kočí	k1gFnSc1	Kočí
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
František	František	k1gMnSc1	František
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
.	.	kIx.	.
</s>
<s>
Händel	Händlo	k1gNnPc2	Händlo
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Stanislav	Stanislav	k1gMnSc1	Stanislav
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vyšla	vyjít	k5eAaPmAgFnS	vyjít
kniha	kniha	k1gFnSc1	kniha
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
v	v	k7c6	v
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
.	.	kIx.	.
</s>
<s>
Okouzlená	okouzlený	k2eAgFnSc1d1	okouzlená
duše	duše	k1gFnSc1	duše
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Škeřík	Škeřík	k1gMnSc1	Škeřík
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josefa	Josefa	k1gFnSc1	Josefa
Hrdinová	Hrdinová	k1gFnSc1	Hrdinová
<g/>
,	,	kIx,	,
první	první	k4xOgFnPc4	první
čtyři	čtyři	k4xCgFnPc4	čtyři
knihy	kniha	k1gFnPc4	kniha
(	(	kIx(	(
<g/>
další	další	k2eAgInPc4d1	další
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ještě	ještě	k6eAd1	ještě
nevyšly	vyjít	k5eNaPmAgFnP	vyjít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Kryštof	Kryštof	k1gMnSc1	Kryštof
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
-	-	kIx~	-
<g/>
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Kopal	Kopal	k1gMnSc1	Kopal
<g/>
,	,	kIx,	,
vydání	vydání	k1gNnSc1	vydání
v	v	k7c6	v
deseti	deset	k4xCc6	deset
samostatných	samostatný	k2eAgInPc6d1	samostatný
svazích	svah	k1gInPc6	svah
<g/>
.	.	kIx.	.
</s>
<s>
Hudebníci	hudebník	k1gMnPc1	hudebník
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
O.	O.	kA	O.
Půlpán	půlpán	k1gMnSc1	půlpán
<g/>
.	.	kIx.	.
</s>
<s>
Hudebníci	hudebník	k1gMnPc1	hudebník
přítomnosti	přítomnost	k1gFnSc2	přítomnost
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
Štorch-Marien	Štorch-Marien	k1gInSc1	Štorch-Marien
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
O.T.	O.T.	k1gFnSc2	O.T.
Kunstovný	Kunstovný	k2eAgInSc1d1	Kunstovný
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Hudebníci	hudebník	k1gMnPc1	hudebník
nedávné	dávný	k2eNgFnSc2d1	nedávná
doby	doba	k1gFnSc2	doba
ještě	ještě	k6eAd1	ještě
Státní	státní	k2eAgNnSc4d1	státní
hudební	hudební	k2eAgNnSc4d1	hudební
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Mystický	mystický	k2eAgInSc4d1	mystický
a	a	k8xC	a
činný	činný	k2eAgInSc4d1	činný
život	život	k1gInSc4	život
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Škeřík	Škeřík	k1gInSc1	Škeřík
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Zaorálek	Zaorálek	k1gMnSc1	Zaorálek
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
ještě	ještě	k9	ještě
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Eminent	Eminent	k1gMnSc1	Eminent
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Zemři	zemřít	k5eAaPmRp2nS	zemřít
a	a	k8xC	a
buď	budit	k5eAaImRp2nS	budit
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Synek	Synek	k1gMnSc1	Synek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Jan	Jan	k1gMnSc1	Jan
Paulík	Paulík	k1gMnSc1	Paulík
<g/>
.	.	kIx.	.
</s>
<s>
Mahátmá	Mahátmý	k2eAgFnSc1d1	Mahátmá
Gándhí	Gándhí	k1gFnSc1	Gándhí
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Eduard	Eduard	k1gMnSc1	Eduard
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
tvůrčí	tvůrčí	k2eAgNnPc1d1	tvůrčí
období	období	k1gNnPc1	období
<g/>
.	.	kIx.	.
1	[number]	k4	1
-	-	kIx~	-
Od	od	k7c2	od
Eroiky	Eroika	k1gFnSc2	Eroika
k	k	k7c3	k
Appassionatě	Appassionatě	k1gFnSc3	Appassionatě
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Stanislav	Stanislav	k1gMnSc1	Stanislav
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vyšla	vyjít	k5eAaPmAgFnS	vyjít
kniha	kniha	k1gFnSc1	kniha
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
v	v	k7c6	v
SNKLHU	SNKLHU	kA	SNKLHU
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
ve	v	k7c6	v
Státním	státní	k2eAgNnSc6d1	státní
hudebním	hudební	k2eAgNnSc6d1	hudební
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
<g/>
.	.	kIx.	.
</s>
<s>
Goethe	Goethe	k1gFnSc1	Goethe
a	a	k8xC	a
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Škeřík	Škeřík	k1gMnSc1	Škeřík
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josefa	Josefa	k1gFnSc1	Josefa
Hrdinová	Hrdinová	k1gFnSc1	Hrdinová
<g/>
.	.	kIx.	.
</s>
<s>
Okouzlená	okouzlený	k2eAgFnSc1d1	okouzlená
duše	duše	k1gFnSc1	duše
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Škeřík	Škeřík	k1gMnSc1	Škeřík
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josefa	Josef	k1gMnSc2	Josef
Hrdinová	Hrdinová	k1gFnSc1	Hrdinová
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Zaorálek	Zaorálek	k1gMnSc1	Zaorálek
<g/>
,	,	kIx,	,
pátá	pátý	k4xOgFnSc1	pátý
kniha	kniha	k1gFnSc1	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Kryštof	Kryštof	k1gMnSc1	Kryštof
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Kopal	Kopal	k1gMnSc1	Kopal
<g/>
,	,	kIx,	,
vydání	vydání	k1gNnSc3	vydání
ve	v	k7c6	v
čtyřech	čtyři	k4xCgNnPc6	čtyři
dílech	dílo	k1gNnPc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
a	a	k8xC	a
uspořádání	uspořádání	k1gNnSc1	uspořádání
vydalo	vydat	k5eAaPmAgNnS	vydat
román	román	k1gInSc4	román
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
toku	tok	k1gInSc2	tok
1948	[number]	k4	1948
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Dílo	dílo	k1gNnSc1	dílo
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
a	a	k8xC	a
SNKLHU	SNKLHU	kA	SNKLHU
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
Sbohem	sbohem	k0	sbohem
minulosti	minulost	k1gFnSc3	minulost
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Fromek	Fromek	k1gInSc1	Fromek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Zaorálek	Zaorálek	k1gMnSc1	Zaorálek
<g/>
.	.	kIx.	.
</s>
<s>
Zabraňte	zabránit	k5eAaPmRp2nP	zabránit
válce	válec	k1gInPc4	válec
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Prokop	Prokop	k1gMnSc1	Prokop
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Miloslav	Miloslav	k1gMnSc1	Miloslav
Jirda	Jirda	k1gMnSc1	Jirda
<g/>
.	.	kIx.	.
</s>
<s>
Leonidy	Leonida	k1gFnPc1	Leonida
<g/>
,	,	kIx,	,
Nakladatelské	nakladatelský	k2eAgNnSc1d1	nakladatelské
družstvo	družstvo	k1gNnSc1	družstvo
Máje	máje	k1gFnSc1	máje
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Eduard	Eduard	k1gMnSc1	Eduard
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Čtrnáctý	čtrnáctý	k4xOgInSc1	čtrnáctý
červenec	červenec	k1gInSc1	červenec
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Voleský	Voleský	k2eAgMnSc1d1	Voleský
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Karel	Karel	k1gMnSc1	Karel
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vyšla	vyjít	k5eAaPmAgFnS	vyjít
tato	tento	k3xDgFnSc1	tento
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
ve	v	k7c6	v
svazku	svazek	k1gInSc6	svazek
Revoluční	revoluční	k2eAgFnSc2d1	revoluční
dramata	drama	k1gNnPc1	drama
vydaném	vydaný	k2eAgNnSc6d1	vydané
v	v	k7c6	v
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
.	.	kIx.	.
</s>
<s>
Dětství	dětství	k1gNnSc1	dětství
Jana	Jan	k1gMnSc2	Jan
Kryštofa	Kryštof	k1gMnSc2	Kryštof
<g/>
,	,	kIx,	,
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jarmila	Jarmila	k1gFnSc1	Jarmila
Sychrová	Sychrová	k1gFnSc1	Sychrová
<g/>
,	,	kIx,	,
vydání	vydání	k1gNnSc1	vydání
části	část	k1gFnSc2	část
románu	román	k1gInSc2	román
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Beethoven	Beethoven	k1gMnSc1	Beethoven
a	a	k8xC	a
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Popelka	Popelka	k1gMnSc1	Popelka
<g/>
,	,	kIx,	,
Polička	Polička	k1gFnSc1	Polička
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
číslovaná	číslovaný	k2eAgFnSc1d1	číslovaná
bibliofilie	bibliofilie	k1gFnSc1	bibliofilie
<g/>
.	.	kIx.	.
</s>
<s>
Hudebníkova	hudebníkův	k2eAgFnSc1d1	hudebníkova
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Košek	Košek	k1gInSc1	Košek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Bláha-Mikeš	Bláha-Mikeš	k1gMnSc1	Bláha-Mikeš
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
ještě	ještě	k6eAd1	ještě
Státní	státní	k2eAgNnSc1d1	státní
hudební	hudební	k2eAgNnSc1d1	hudební
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
učebné	učebné	k1gNnSc4	učebné
pomůcky	pomůcka	k1gFnSc2	pomůcka
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
a	a	k8xC	a
odborných	odborný	k2eAgFnPc2d1	odborná
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jan	Jan	k1gMnSc1	Jan
Kopecký	Kopecký	k1gMnSc1	Kopecký
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
.	.	kIx.	.
</s>
<s>
Dopisy	dopis	k1gInPc1	dopis
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Marel	Marel	k1gInSc1	Marel
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Lorenc	Lorenc	k1gMnSc1	Lorenc
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
hlubin	hlubina	k1gFnPc2	hlubina
nitra	nitro	k1gNnSc2	nitro
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Škeřík	Škeřík	k1gMnSc1	Škeřík
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jarmila	Jarmila	k1gFnSc1	Jarmila
Votrubová-Koutecká	Votrubová-Koutecký	k2eAgFnSc1d1	Votrubová-Koutecký
<g/>
.	.	kIx.	.
</s>
<s>
Věčný	věčný	k2eAgMnSc1d1	věčný
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Škeřík	Škeřík	k1gMnSc1	Škeřík
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jarmila	Jarmila	k1gFnSc1	Jarmila
Votrubová-Koutecká	Votrubová-Koutecký	k2eAgFnSc1d1	Votrubová-Koutecký
<g/>
.	.	kIx.	.
</s>
<s>
Michelangelo	Michelanget	k5eAaImAgNnS	Michelanget
Buonarroti	Buonarrot	k1gMnPc1	Buonarrot
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Škeřík	Škeřík	k1gMnSc1	Škeřík
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Dagmar	Dagmar	k1gFnSc4	Dagmar
Malá	malý	k2eAgFnSc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Empedokles	Empedokles	k1gInSc1	Empedokles
z	z	k7c2	z
Akragantu	Akragant	k1gInSc2	Akragant
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Škeřík	Škeřík	k1gMnSc1	Škeřík
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Arnošt	Arnošt	k1gMnSc1	Arnošt
Bareš	Bareš	k1gMnSc1	Bareš
<g/>
.	.	kIx.	.
</s>
<s>
Liluly	Lilula	k1gFnPc1	Lilula
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Škeřík	Škeřík	k1gMnSc1	Škeřík
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
N.	N.	kA	N.
Neumannová	Neumannová	k1gFnSc1	Neumannová
a	a	k8xC	a
J.	J.	kA	J.
Kopecký	Kopecký	k1gMnSc1	Kopecký
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
pravdě	pravda	k1gFnSc3	pravda
<g/>
,	,	kIx,	,
Družstvo	družstvo	k1gNnSc4	družstvo
Dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Valter	Valter	k1gMnSc1	Valter
Feldstein	Feldstein	k1gMnSc1	Feldstein
<g/>
.	.	kIx.	.
</s>
<s>
Montespanová	Montespanová	k1gFnSc1	Montespanová
České	český	k2eAgFnSc2d1	Česká
divadelní	divadelní	k2eAgFnSc2d1	divadelní
a	a	k8xC	a
literární	literární	k2eAgNnSc4d1	literární
jednatelství	jednatelství	k1gNnSc4	jednatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
195	[number]	k4	195
<g/>
x	x	k?	x
(	(	kIx(	(
<g/>
přesný	přesný	k2eAgInSc1d1	přesný
rok	rok	k1gInSc1	rok
nezjištěn	zjištěn	k2eNgInSc1d1	nezjištěn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Antonín	Antonín	k1gMnSc1	Antonín
Bernášek	Bernášek	k1gMnSc1	Bernášek
<g/>
.	.	kIx.	.
</s>
<s>
Okouzlená	okouzlený	k2eAgFnSc1d1	okouzlená
duše	duše	k1gFnSc1	duše
<g/>
,	,	kIx,	,
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josefa	Josef	k1gMnSc2	Josef
Hrdinová	Hrdinová	k1gFnSc1	Hrdinová
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Zaorálek	Zaorálek	k1gMnSc1	Zaorálek
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tento	tento	k3xDgInSc4	tento
román	román	k1gInSc4	román
ještě	ještě	k6eAd1	ještě
SNKLHU	SNKLHU	kA	SNKLHU
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
a	a	k8xC	a
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Odeon	odeon	k1gInSc1	odeon
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Svoboda	Svoboda	k1gMnSc1	Svoboda
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Kryštof	Kryštof	k1gMnSc1	Kryštof
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Nina	Nina	k1gFnSc1	Nina
Neklanová	klanový	k2eNgFnSc1d1	Neklanová
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Ruxová	Ruxová	k1gFnSc1	Ruxová
a	a	k8xC	a
Anna	Anna	k1gFnSc1	Anna
Kučerová	Kučerová	k1gFnSc1	Kučerová
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc1	čtyři
díly	díl	k1gInPc1	díl
<g/>
.	.	kIx.	.
</s>
<s>
Revoluční	revoluční	k2eAgNnPc1d1	revoluční
dramata	drama	k1gNnPc1	drama
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
divadelní	divadelní	k2eAgFnPc4d1	divadelní
hry	hra	k1gFnPc4	hra
Čtrnáctý	čtrnáctý	k4xOgInSc4	čtrnáctý
červenec	červenec	k1gInSc4	červenec
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Karla	Karel	k1gMnSc2	Karel
Krause	Kraus	k1gMnSc2	Kraus
a	a	k8xC	a
Danton	Danton	k1gInSc1	Danton
<g/>
,	,	kIx,	,
Vlci	vlk	k1gMnPc1	vlk
a	a	k8xC	a
Robespierre	Robespierr	k1gInSc5	Robespierr
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Niny	Nina	k1gFnSc2	Nina
Neklanové	klanový	k2eNgFnSc2d1	Neklanová
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
Beethovenův	Beethovenův	k2eAgInSc1d1	Beethovenův
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Pavel	Pavel	k1gMnSc1	Pavel
Eisner	Eisner	k1gMnSc1	Eisner
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
monografii	monografie	k1gFnSc4	monografie
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Jean	Jean	k1gMnSc1	Jean
François	François	k1gFnSc2	François
Millet	Millet	k1gInSc1	Millet
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Luděk	Luděk	k1gMnSc1	Luděk
Kult	kult	k1gInSc1	kult
<g/>
.	.	kIx.	.
</s>
<s>
Goethe	Goethe	k1gFnSc1	Goethe
a	a	k8xC	a
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
,	,	kIx,	,
Beethoven	Beethoven	k1gMnSc1	Beethoven
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Pavel	Pavel	k1gMnSc1	Pavel
Eisner	Eisner	k1gMnSc1	Eisner
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
Beethovenův	Beethovenův	k2eAgInSc1d1	Beethovenův
<g/>
,	,	kIx,	,
Život	život	k1gInSc1	život
Michelangelův	Michelangelův	k2eAgInSc1d1	Michelangelův
<g/>
,	,	kIx,	,
Život	život	k1gInSc1	život
Tolstého	Tolstý	k2eAgInSc2d1	Tolstý
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Anna	Anna	k1gFnSc1	Anna
Kučerová	Kučerová	k1gFnSc1	Kučerová
a	a	k8xC	a
Nina	Nina	k1gFnSc1	Nina
Neklanová	klanový	k2eNgFnSc1d1	Neklanová
<g/>
.	.	kIx.	.
</s>
<s>
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
tvůrčí	tvůrčí	k2eAgNnPc1d1	tvůrčí
období	období	k1gNnPc1	období
<g/>
.	.	kIx.	.
2	[number]	k4	2
-	-	kIx~	-
Zpěv	zpěv	k1gInSc1	zpěv
vzříšení	vzříšení	k1gNnSc2	vzříšení
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Oto	Oto	k1gMnSc1	Oto
Albert	Albert	k1gMnSc1	Albert
Tichý	Tichý	k1gMnSc1	Tichý
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vyšla	vyjít	k5eAaPmAgFnS	vyjít
kniha	kniha	k1gFnSc1	kniha
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
ve	v	k7c6	v
Státním	státní	k2eAgNnSc6d1	státní
hudebním	hudební	k2eAgNnSc6d1	hudební
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
<g/>
.	.	kIx.	.
</s>
<s>
Hudebníci	hudebník	k1gMnPc1	hudebník
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Luděk	Luděk	k1gMnSc1	Luděk
Kult	kult	k1gInSc1	kult
<g/>
.	.	kIx.	.
</s>
<s>
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
tvůrčí	tvůrčí	k2eAgNnPc1d1	tvůrčí
období	období	k1gNnPc1	období
<g/>
.	.	kIx.	.
3	[number]	k4	3
-	-	kIx~	-
Nedokončená	dokončený	k2eNgFnSc1d1	nedokončená
katedrála	katedrála	k1gFnSc1	katedrála
-	-	kIx~	-
Devátá	devátý	k4xOgFnSc1	devátý
symfonie	symfonie	k1gFnSc1	symfonie
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Balcar	Balcar	k1gMnSc1	Balcar
<g/>
.	.	kIx.	.
</s>
<s>
Paměti	paměť	k1gFnPc1	paměť
a	a	k8xC	a
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Otakar	Otakar	k1gMnSc1	Otakar
Novák	Novák	k1gMnSc1	Novák
<g/>
.	.	kIx.	.
</s>
<s>
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
tvůrčí	tvůrčí	k2eAgNnPc1d1	tvůrčí
období	období	k1gNnPc1	období
<g/>
.	.	kIx.	.
4	[number]	k4	4
-	-	kIx~	-
Nedokončená	dokončený	k2eNgFnSc1d1	nedokončená
katedrála	katedrála	k1gFnSc1	katedrála
-	-	kIx~	-
Poslední	poslední	k2eAgInPc1d1	poslední
kvartety	kvartet	k1gInPc1	kvartet
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Oto	Oto	k1gMnSc1	Oto
Albert	Albert	k1gMnSc1	Albert
Tichý	Tichý	k1gMnSc1	Tichý
<g/>
.	.	kIx.	.
</s>
<s>
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
tvůrčí	tvůrčí	k2eAgNnPc1d1	tvůrčí
období	období	k1gNnPc1	období
<g/>
.	.	kIx.	.
5	[number]	k4	5
-	-	kIx~	-
Nedokončená	dokončený	k2eNgFnSc1d1	nedokončená
katedrála	katedrála	k1gFnSc1	katedrála
-	-	kIx~	-
Finita	Finita	k1gFnSc1	Finita
comoedia	comoedium	k1gNnSc2	comoedium
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Karel	Karel	k1gMnSc1	Karel
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
.	.	kIx.	.
</s>
<s>
Přijde	přijít	k5eAaPmIp3nS	přijít
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Karel	Karel	k1gMnSc1	Karel
Neubauer	Neubauer	k1gMnSc1	Neubauer
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnSc1	umění
a	a	k8xC	a
čin	čin	k1gInSc1	čin
<g/>
,	,	kIx,	,
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
kolektiv	kolektiv	k1gInSc1	kolektiv
překladatelů	překladatel	k1gMnPc2	překladatel
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
uměleckoteoretického	uměleckoteoretický	k2eAgNnSc2d1	uměleckoteoretický
a	a	k8xC	a
muzikologického	muzikologický	k2eAgNnSc2d1	muzikologické
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
a	a	k8xC	a
smrti	smrt	k1gFnSc3	smrt
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Ida	Ida	k1gFnSc1	Ida
de	de	k?	de
Vries	Vries	k1gInSc1	Vries
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
<g/>
,	,	kIx,	,
DILIA	DILIA	kA	DILIA
Praha	Praha	k1gFnSc1	Praha
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milena	Milena	k1gFnSc1	Milena
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Tomáškovi	Tomášek	k1gMnSc3	Tomášek
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
před	před	k7c4	před
Lullym	Lullym	k1gInSc4	Lullym
a	a	k8xC	a
Scarlattim	Scarlattim	k1gInSc1	Scarlattim
<g/>
,	,	kIx,	,
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Kostohryz	Kostohryz	k1gMnSc1	Kostohryz
<g/>
.	.	kIx.	.
</s>
<s>
Vzpoura	vzpoura	k1gFnSc1	vzpoura
strojů	stroj	k1gInPc2	stroj
aneb	aneb	k?	aneb
Myšlenka	myšlenka	k1gFnSc1	myšlenka
zbavená	zbavený	k2eAgFnSc1d1	zbavená
pout	pout	k1gInSc1	pout
<g/>
,	,	kIx,	,
časopis	časopis	k1gInSc1	časopis
Světová	světový	k2eAgFnSc1d1	světová
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
Amore	Amor	k1gMnSc5	Amor
Pace	Paka	k1gFnSc6	Paka
<g/>
,	,	kIx,	,
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
P.	P.	kA	P.
Sommer	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
autobiografické	autobiografický	k2eAgFnSc2d1	autobiografická
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
korespondence	korespondence	k1gFnSc2	korespondence
<g/>
.	.	kIx.	.
</s>
<s>
Empedokles	Empedokles	k1gInSc1	Empedokles
z	z	k7c2	z
Akragantu	Akragant	k1gInSc2	Akragant
<g/>
,	,	kIx,	,
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Zuzana	Zuzana	k1gFnSc1	Zuzana
Barochová	Barochová	k1gFnSc1	Barochová
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Romain	Romain	k1gMnSc1	Romain
Rolland	Rolland	k1gMnSc1	Rolland
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Autor	autor	k1gMnSc1	autor
Romain	Romain	k1gMnSc1	Romain
Rolland	Rolland	k1gMnSc1	Rolland
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Romain	Romain	k1gMnSc1	Romain
Rolland	Rolland	k1gMnSc1	Rolland
Plné	plný	k2eAgInPc4d1	plný
texty	text	k1gInPc4	text
děl	dít	k5eAaImAgMnS	dít
autora	autor	k1gMnSc2	autor
Romain	Romain	k1gMnSc1	Romain
Rolland	Rolland	k1gMnSc1	Rolland
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Gutenberg	Gutenberg	k1gInSc1	Gutenberg
Osoba	osoba	k1gFnSc1	osoba
Romain	Romain	k1gMnSc1	Romain
Rolland	Rolland	k1gMnSc1	Rolland
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Nobel	Nobel	k1gMnSc1	Nobel
Prize	Prize	k1gFnSc2	Prize
bio	bio	k?	bio
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Association	Association	k1gInSc1	Association
Romain	Romain	k1gMnSc1	Romain
Rolland	Rolland	k1gMnSc1	Rolland
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
http://www.kirjasto.sci.fi/rolland.htm	[url]	k6eAd1	http://www.kirjasto.sci.fi/rolland.htm
</s>
