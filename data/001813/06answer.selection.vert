<s>
Kromě	kromě	k7c2	kromě
prozaických	prozaický	k2eAgFnPc2d1	prozaická
prací	práce	k1gFnPc2	práce
je	být	k5eAaImIp3nS	být
Rolland	Rolland	k1gMnSc1	Rolland
autorem	autor	k1gMnSc7	autor
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
životopisů	životopis	k1gInPc2	životopis
významných	významný	k2eAgFnPc2d1	významná
světových	světový	k2eAgFnPc2d1	světová
osobností	osobnost	k1gFnPc2	osobnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
,	,	kIx,	,
Michelangelo	Michelangela	k1gFnSc5	Michelangela
nebo	nebo	k8xC	nebo
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
mnoha	mnoho	k4c2	mnoho
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
tzv.	tzv.	kA	tzv.
lidové	lidový	k2eAgNnSc1d1	lidové
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
