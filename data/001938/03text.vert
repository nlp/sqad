<s>
Bronx	Bronx	k1gInSc1	Bronx
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
obvodů	obvod	k1gInPc2	obvod
města	město	k1gNnSc2	město
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
okresů	okres	k1gInPc2	okres
amerického	americký	k2eAgInSc2d1	americký
spolkového	spolkový	k2eAgInSc2d1	spolkový
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Bronx	Bronx	k1gInSc1	Bronx
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
městským	městský	k2eAgInSc7d1	městský
obvodem	obvod	k1gInSc7	obvod
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
neleží	ležet	k5eNaImIp3nS	ležet
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Bronx	Bronx	k1gInSc1	Bronx
byl	být	k5eAaImAgInS	být
proslavený	proslavený	k2eAgMnSc1d1	proslavený
zejména	zejména	k9	zejména
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xC	jako
nejbrutálnější	brutální	k2eAgFnSc1d3	nejbrutálnější
část	část	k1gFnSc1	část
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
vyznačoval	vyznačovat	k5eAaImAgMnS	vyznačovat
se	se	k3xPyFc4	se
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
kriminalitou	kriminalita	k1gFnSc7	kriminalita
<g/>
.	.	kIx.	.
</s>
<s>
Přepadení	přepadení	k1gNnSc1	přepadení
<g/>
,	,	kIx,	,
krádeže	krádež	k1gFnPc1	krádež
aut	auto	k1gNnPc2	auto
<g/>
,	,	kIx,	,
drogy	droga	k1gFnPc4	droga
a	a	k8xC	a
dealeři	dealer	k1gMnPc1	dealer
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
každodenní	každodenní	k2eAgFnSc7d1	každodenní
samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velice	velice	k6eAd1	velice
chudou	chudý	k2eAgFnSc4d1	chudá
čtvrť	čtvrť	k1gFnSc4	čtvrť
s	s	k7c7	s
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
černošskou	černošský	k2eAgFnSc7d1	černošská
a	a	k8xC	a
hispánskou	hispánský	k2eAgFnSc7d1	hispánská
komunitou	komunita	k1gFnSc7	komunita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
ale	ale	k9	ale
situace	situace	k1gFnSc1	situace
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
a	a	k8xC	a
zčásti	zčásti	k6eAd1	zčásti
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
gentrifikace	gentrifikace	k1gFnSc1	gentrifikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
přibližně	přibližně	k6eAd1	přibližně
1,3	[number]	k4	1,3
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
48	[number]	k4	48
%	%	kIx~	%
hispánci	hispánek	k1gMnPc1	hispánek
<g/>
,	,	kIx,	,
36	[number]	k4	36
%	%	kIx~	%
černoši	černoch	k1gMnPc1	černoch
<g/>
,	,	kIx,	,
14	[number]	k4	14
%	%	kIx~	%
běloši	běloch	k1gMnPc1	běloch
<g/>
,	,	kIx,	,
2	[number]	k4	2
%	%	kIx~	%
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
nejznámějšího	známý	k2eAgInSc2d3	nejznámější
amerického	americký	k2eAgInSc2d1	americký
baseballového	baseballový	k2eAgInSc2d1	baseballový
týmu	tým	k1gInSc2	tým
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Yankees	Yankees	k1gInSc1	Yankees
<g/>
,	,	kIx,	,
hip	hip	k0	hip
hopu	hopa	k1gMnSc4	hopa
a	a	k8xC	a
breakdance	breakdanec	k1gMnSc4	breakdanec
<g/>
.	.	kIx.	.
</s>
<s>
Bronx	Bronx	k1gInSc1	Bronx
prošel	projít	k5eAaPmAgInS	projít
rychlým	rychlý	k2eAgInSc7d1	rychlý
růstem	růst	k1gInSc7	růst
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
newyorského	newyorský	k2eAgNnSc2d1	newyorské
metra	metro	k1gNnSc2	metro
přispělo	přispět	k5eAaPmAgNnS	přispět
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
když	když	k8xS	když
tisíce	tisíc	k4xCgInPc1	tisíc
imigrantů	imigrant	k1gMnPc2	imigrant
zaplnily	zaplnit	k5eAaPmAgFnP	zaplnit
Bronx	Bronx	k1gInSc4	Bronx
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zapříčinilo	zapříčinit	k5eAaPmAgNnS	zapříčinit
významný	významný	k2eAgInSc4d1	významný
rozmach	rozmach	k1gInSc4	rozmach
výstavby	výstavba	k1gFnSc2	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
imigrantských	imigrantský	k2eAgFnPc2d1	imigrantská
skupin	skupina	k1gFnPc2	skupina
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
usadilo	usadit	k5eAaPmAgNnS	usadit
mnoho	mnoho	k4c1	mnoho
Iro-Američanů	Iro-Američan	k1gMnPc2	Iro-Američan
<g/>
,	,	kIx,	,
Italo-Američanů	Italo-Američan	k1gMnPc2	Italo-Američan
a	a	k8xC	a
zejména	zejména	k9	zejména
Žido-Američanů	Žido-Američan	k1gMnPc2	Žido-Američan
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
do	do	k7c2	do
čtvrti	čtvrt	k1gFnSc2	čtvrt
přistěhovali	přistěhovat	k5eAaPmAgMnP	přistěhovat
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Poláci	Polák	k1gMnPc1	Polák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
Židovská	židovský	k2eAgFnSc1d1	židovská
populace	populace	k1gFnSc1	populace
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
žilo	žít	k5eAaImAgNnS	žít
podle	podle	k7c2	podle
židovských	židovský	k2eAgFnPc2d1	židovská
organizací	organizace	k1gFnPc2	organizace
v	v	k7c6	v
Bronxu	Bronx	k1gInSc6	Bronx
592	[number]	k4	592
185	[number]	k4	185
Židů	Žid	k1gMnPc2	Žid
(	(	kIx(	(
<g/>
43,9	[number]	k4	43,9
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
jen	jen	k9	jen
asi	asi	k9	asi
45000	[number]	k4	45000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bronxu	Bronx	k1gInSc6	Bronx
stále	stále	k6eAd1	stále
stojí	stát	k5eAaImIp3nS	stát
mnoho	mnoho	k4c1	mnoho
synagog	synagoga	k1gFnPc2	synagoga
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
přestavěna	přestavěn	k2eAgFnSc1d1	přestavěna
k	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
prohibice	prohibice	k1gFnSc2	prohibice
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
Bronxu	Bronx	k1gInSc6	Bronx
vymkly	vymknout	k5eAaPmAgInP	vymknout
kontrole	kontrola	k1gFnSc6	kontrola
gangy	gang	k1gInPc4	gang
<g/>
.	.	kIx.	.
</s>
<s>
Irští	irský	k2eAgMnPc1d1	irský
<g/>
,	,	kIx,	,
italští	italský	k2eAgMnPc1d1	italský
a	a	k8xC	a
polští	polský	k2eAgMnPc1d1	polský
imigranti	imigrant	k1gMnPc1	imigrant
pašovali	pašovat	k5eAaImAgMnP	pašovat
většinu	většina	k1gFnSc4	většina
ilegální	ilegální	k2eAgFnSc2d1	ilegální
whisky	whisky	k1gFnSc2	whisky
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
byl	být	k5eAaImAgInS	být
Bronx	Bronx	k1gInSc4	Bronx
znám	znám	k2eAgMnSc1d1	znám
svou	svůj	k3xOyFgFnSc7	svůj
vysokou	vysoký	k2eAgFnSc7d1	vysoká
mírou	míra	k1gFnSc7	míra
kriminality	kriminalita	k1gFnSc2	kriminalita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
populace	populace	k1gFnSc1	populace
irských	irský	k2eAgMnPc2d1	irský
imigrantů	imigrant	k1gMnPc2	imigrant
v	v	k7c6	v
Bronxu	Bronx	k1gInSc6	Bronx
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
populace	populace	k1gFnSc1	populace
toto	tento	k3xDgNnSc4	tento
následovala	následovat	k5eAaImAgFnS	následovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
mnozí	mnohý	k2eAgMnPc1d1	mnohý
Italové	Ital	k1gMnPc1	Ital
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
Bronx	Bronx	k1gInSc1	Bronx
šel	jít	k5eAaImAgInS	jít
do	do	k7c2	do
éry	éra	k1gFnSc2	éra
prudkého	prudký	k2eAgInSc2d1	prudký
poklesu	pokles	k1gInSc2	pokles
kvality	kvalita	k1gFnSc2	kvalita
života	život	k1gInSc2	život
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Historici	historik	k1gMnPc1	historik
a	a	k8xC	a
socilogové	socilog	k1gMnPc1	socilog
toto	tento	k3xDgNnSc4	tento
zdůvodnili	zdůvodnit	k5eAaPmAgMnP	zdůvodnit
mnoha	mnoho	k4c2	mnoho
faktory	faktor	k1gInPc7	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
faktorů	faktor	k1gInPc2	faktor
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rozvoj	rozvoj	k1gInSc4	rozvoj
výstavby	výstavba	k1gFnSc2	výstavba
výškových	výškový	k2eAgInPc2d1	výškový
domů	dům	k1gInPc2	dům
jako	jako	k8xS	jako
budov	budova	k1gFnPc2	budova
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
Bronx	Bronx	k1gInSc1	Bronx
sužován	sužovat	k5eAaImNgInS	sužovat
vlnou	vlna	k1gFnSc7	vlna
žhářství	žhářství	k1gNnSc2	žhářství
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
především	především	k9	především
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Bronxu	Bronx	k1gInSc6	Bronx
<g/>
,	,	kIx,	,
soustředilo	soustředit	k5eAaPmAgNnS	soustředit
se	se	k3xPyFc4	se
kolem	kolem	k6eAd1	kolem
Westchester	Westchester	k1gInSc1	Westchester
Avenue	avenue	k1gFnSc2	avenue
a	a	k8xC	a
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
West	West	k2eAgInSc4d1	West
Farms	Farms	k1gInSc4	Farms
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgNnSc1d3	nejčastější
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
tohoto	tento	k3xDgNnSc2	tento
žhářství	žhářství	k1gNnSc2	žhářství
jsou	být	k5eAaImIp3nP	být
pojistné	pojistný	k2eAgInPc1d1	pojistný
podvody	podvod	k1gInPc1	podvod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zničení	zničení	k1gNnSc6	zničení
mnoha	mnoho	k4c2	mnoho
staveb	stavba	k1gFnPc2	stavba
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Bronxu	Bronx	k1gInSc6	Bronx
se	se	k3xPyFc4	se
žhářství	žhářství	k1gNnSc1	žhářství
výrazně	výrazně	k6eAd1	výrazně
snížilo	snížit	k5eAaPmAgNnS	snížit
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
části	část	k1gFnSc6	část
tohoto	tento	k3xDgNnSc2	tento
desetiletí	desetiletí	k1gNnSc2	desetiletí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
důsledky	důsledek	k1gInPc1	důsledek
byly	být	k5eAaImAgInP	být
cítit	cítit	k5eAaImF	cítit
ještě	ještě	k9	ještě
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
ulic	ulice	k1gFnPc2	ulice
v	v	k7c6	v
Bronxu	Bronx	k1gInSc6	Bronx
je	být	k5eAaImIp3nS	být
nepravidelná	pravidelný	k2eNgFnSc1d1	nepravidelná
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
nejsevernější	severní	k2eAgFnSc1d3	nejsevernější
část	část	k1gFnSc1	část
horního	horní	k2eAgInSc2d1	horní
Manhattanu	Manhattan	k1gInSc2	Manhattan
<g/>
,	,	kIx,	,
kopcovitý	kopcovitý	k2eAgInSc1d1	kopcovitý
terén	terén	k1gInSc1	terén
západního	západní	k2eAgInSc2d1	západní
Bronxu	Bronx	k1gInSc2	Bronx
nechává	nechávat	k5eAaImIp3nS	nechávat
relativně	relativně	k6eAd1	relativně
volný	volný	k2eAgInSc4d1	volný
styl	styl	k1gInSc4	styl
pro	pro	k7c4	pro
síť	síť	k1gFnSc4	síť
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgInSc1d1	východní
Bronx	Bronx	k1gInSc1	Bronx
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
plošší	plochý	k2eAgFnSc1d2	plošší
a	a	k8xC	a
ulice	ulice	k1gFnPc1	ulice
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
být	být	k5eAaImF	být
více	hodně	k6eAd2	hodně
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
tunelů	tunel	k1gInPc2	tunel
a	a	k8xC	a
mostů	most	k1gInPc2	most
pojí	pojíst	k5eAaPmIp3nS	pojíst
Bronx	Bronx	k1gInSc1	Bronx
k	k	k7c3	k
Manhattanu	Manhattan	k1gInSc3	Manhattan
a	a	k8xC	a
Queensu	Queens	k1gMnSc3	Queens
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
patří	patřit	k5eAaImIp3nS	patřit
<g/>
,	,	kIx,	,
od	od	k7c2	od
západu	západ	k1gInSc2	západ
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
:	:	kIx,	:
Na	na	k7c4	na
Manhattan	Manhattan	k1gInSc4	Manhattan
<g/>
:	:	kIx,	:
Spuyten	Spuyten	k2eAgMnSc1d1	Spuyten
Duyvil	Duyvil	k1gMnSc1	Duyvil
Bridge	Bridge	k1gNnSc2	Bridge
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
Hudson	Hudson	k1gMnSc1	Hudson
Bridge	Bridg	k1gMnSc2	Bridg
<g/>
,	,	kIx,	,
Broadway	Broadwaa	k1gMnSc2	Broadwaa
Bridge	Bridg	k1gMnSc2	Bridg
<g/>
,	,	kIx,	,
University	universita	k1gFnSc2	universita
Heights	Heightsa	k1gFnPc2	Heightsa
Bridge	Bridge	k1gInSc1	Bridge
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
Bridge	Bridge	k1gFnPc2	Bridge
<g/>
,	,	kIx,	,
Alexander	Alexandra	k1gFnPc2	Alexandra
Hamilton	Hamilton	k1gInSc1	Hamilton
Bridge	Bridg	k1gInSc2	Bridg
<g/>
,	,	kIx,	,
High	High	k1gMnSc1	High
Bridge	Bridge	k1gFnSc1	Bridge
<g/>
,	,	kIx,	,
Concourse	Concourse	k1gFnSc1	Concourse
Tunnel	Tunnel	k1gMnSc1	Tunnel
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Macombs	Macombs	k1gInSc4	Macombs
Dam	dáma	k1gFnPc2	dáma
Bridge	Bridg	k1gInSc2	Bridg
<g/>
,	,	kIx,	,
145	[number]	k4	145
<g/>
th	th	k?	th
Street	Street	k1gMnSc1	Street
Bridge	Bridge	k1gNnSc1	Bridge
<g/>
,	,	kIx,	,
149	[number]	k4	149
<g/>
th	th	k?	th
Street	Street	k1gMnSc1	Street
Tunnel	Tunnel	k1gMnSc1	Tunnel
,	,	kIx,	,
the	the	k?	the
Madison	Madison	k1gInSc1	Madison
Avenue	avenue	k1gFnSc1	avenue
Bridge	Bridge	k1gFnSc1	Bridge
<g/>
,	,	kIx,	,
Park	park	k1gInSc1	park
Avenue	avenue	k1gFnSc2	avenue
Bridge	Bridg	k1gFnSc2	Bridg
<g/>
,	,	kIx,	,
the	the	k?	the
Lexington	Lexington	k1gInSc1	Lexington
Avenue	avenue	k1gFnSc1	avenue
Tunnel	Tunnel	k1gMnSc1	Tunnel
<g/>
,	,	kIx,	,
Third	Third	k1gMnSc1	Third
Avenue	avenue	k1gFnSc2	avenue
Bridge	Bridg	k1gFnSc2	Bridg
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
provoz	provoz	k1gInSc4	provoz
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
)	)	kIx)	)
a	a	k8xC	a
Willis	Willis	k1gFnSc1	Willis
Avenue	avenue	k1gFnSc1	avenue
Bridge	Bridge	k1gFnSc1	Bridge
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
provoz	provoz	k1gInSc4	provoz
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Manhattan	Manhattan	k1gInSc4	Manhattan
a	a	k8xC	a
Queens	Queens	k1gInSc4	Queens
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
F.	F.	kA	F.
Kennedy	Kenneda	k1gMnSc2	Kenneda
Bridge	Bridg	k1gMnSc2	Bridg
Na	na	k7c4	na
Queens	Queens	k1gInSc4	Queens
<g/>
:	:	kIx,	:
Bronx	Bronx	k1gInSc1	Bronx
Whitestone	Whiteston	k1gInSc5	Whiteston
Bridge	Bridge	k1gInSc1	Bridge
a	a	k8xC	a
Throgs	Throgs	k1gInSc1	Throgs
Neck	Necka	k1gFnPc2	Necka
Bridge	Bridg	k1gInSc2	Bridg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bronxu	Bronx	k1gInSc6	Bronx
je	být	k5eAaImIp3nS	být
šest	šest	k4xCc1	šest
tratí	trať	k1gFnPc2	trať
newyorského	newyorský	k2eAgNnSc2d1	newyorské
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
1	[number]	k4	1
332	[number]	k4	332
650	[number]	k4	650
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zde	zde	k6eAd1	zde
463	[number]	k4	463
212	[number]	k4	212
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
314	[number]	k4	314
984	[number]	k4	984
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
činila	činit	k5eAaImAgFnS	činit
12	[number]	k4	12
242,2	[number]	k4	242,2
lidí	člověk	k1gMnPc2	člověk
na	na	k7c4	na
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
100	[number]	k4	100
žen	žena	k1gFnPc2	žena
tam	tam	k6eAd1	tam
případalo	případat	k5eAaImAgNnS	případat
87	[number]	k4	87
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
činil	činit	k5eAaImAgInS	činit
střední	střední	k2eAgInSc1d1	střední
příjem	příjem	k1gInSc1	příjem
jedné	jeden	k4xCgFnSc2	jeden
domácnosti	domácnost	k1gFnSc2	domácnost
27	[number]	k4	27
611	[number]	k4	611
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
příjem	příjem	k1gInSc4	příjem
pro	pro	k7c4	pro
rodinu	rodina	k1gFnSc4	rodina
byl	být	k5eAaImAgMnS	být
30	[number]	k4	30
682	[number]	k4	682
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
měli	mít	k5eAaImAgMnP	mít
střední	střední	k2eAgInSc4d1	střední
příjem	příjem	k1gInSc4	příjem
31	[number]	k4	31
178	[number]	k4	178
dolarů	dolar	k1gInPc2	dolar
oproti	oproti	k7c3	oproti
29	[number]	k4	29
429	[number]	k4	429
dolarům	dolar	k1gInPc3	dolar
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Příjem	příjem	k1gInSc1	příjem
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
byl	být	k5eAaImAgMnS	být
13	[number]	k4	13
959	[number]	k4	959
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
28,0	[number]	k4	28,0
%	%	kIx~	%
rodin	rodina	k1gFnPc2	rodina
a	a	k8xC	a
30,7	[number]	k4	30,7
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
žilo	žít	k5eAaImAgNnS	žít
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
The	The	k1gFnSc2	The
Bronx	Bronx	k1gInSc1	Bronx
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
