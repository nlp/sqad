<s>
Hromosvod	hromosvod	k1gInSc1	hromosvod
<g/>
,	,	kIx,	,
vzácněji	vzácně	k6eAd2	vzácně
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
bleskosvod	bleskosvod	k1gInSc1	bleskosvod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
umělou	umělý	k2eAgFnSc4d1	umělá
vodivou	vodivý	k2eAgFnSc4d1	vodivá
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
a	a	k8xC	a
svedení	svedení	k1gNnSc3	svedení
bleskového	bleskový	k2eAgInSc2d1	bleskový
výboje	výboj	k1gInSc2	výboj
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jakožto	jakožto	k8xS	jakožto
ochrana	ochrana	k1gFnSc1	ochrana
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
objektů	objekt	k1gInPc2	objekt
před	před	k7c7	před
poškozením	poškození	k1gNnSc7	poškození
tepelnými	tepelný	k2eAgInPc7d1	tepelný
a	a	k8xC	a
mechanickými	mechanický	k2eAgInPc7d1	mechanický
účinky	účinek	k1gInPc7	účinek
blesku	blesk	k1gInSc2	blesk
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
před	před	k7c7	před
jejich	jejich	k3xOp3gInSc7	jejich
požárem	požár	k1gInSc7	požár
nebo	nebo	k8xC	nebo
mechanickým	mechanický	k2eAgNnSc7d1	mechanické
poškozením	poškození	k1gNnSc7	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Hromosvody	hromosvod	k1gInPc4	hromosvod
řadíme	řadit	k5eAaImIp1nP	řadit
do	do	k7c2	do
systémů	systém	k1gInPc2	systém
vnější	vnější	k2eAgFnSc2d1	vnější
ochrany	ochrana	k1gFnSc2	ochrana
před	před	k7c7	před
bleskem	blesk	k1gInSc7	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Hromosvod	hromosvod	k1gInSc1	hromosvod
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
premonstrát	premonstrát	k1gMnSc1	premonstrát
Prokop	Prokop	k1gMnSc1	Prokop
Diviš	Diviš	k1gMnSc1	Diviš
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
farní	farní	k2eAgFnSc6d1	farní
zahradě	zahrada	k1gFnSc6	zahrada
v	v	k7c6	v
Příměticích	Přímětice	k1gFnPc6	Přímětice
blízko	blízko	k7c2	blízko
Znojma	Znojmo	k1gNnSc2	Znojmo
umístil	umístit	k5eAaPmAgMnS	umístit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1754	[number]	k4	1754
první	první	k4xOgInSc1	první
hromosvod	hromosvod	k1gInSc1	hromosvod
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
platí	platit	k5eAaImIp3nS	platit
za	za	k7c4	za
vynálezce	vynálezce	k1gMnPc4	vynálezce
hromosvodu	hromosvod	k1gInSc2	hromosvod
také	také	k9	také
americký	americký	k2eAgMnSc1d1	americký
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Benjamin	Benjamin	k1gMnSc1	Benjamin
Franklin	Franklin	k1gInSc4	Franklin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1750	[number]	k4	1750
prováděl	provádět	k5eAaImAgInS	provádět
podobné	podobný	k2eAgInPc4d1	podobný
experimenty	experiment	k1gInPc4	experiment
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
hromosvod	hromosvod	k1gInSc1	hromosvod
na	na	k7c6	na
území	území	k1gNnSc6	území
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
instalován	instalovat	k5eAaBmNgInS	instalovat
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Měšicích	Měšice	k1gFnPc6	Měšice
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
<g/>
.	.	kIx.	.
</s>
<s>
Hromosvod	hromosvod	k1gInSc1	hromosvod
se	se	k3xPyFc4	se
zřizuje	zřizovat	k5eAaImIp3nS	zřizovat
zejména	zejména	k9	zejména
na	na	k7c6	na
objektech	objekt	k1gInPc6	objekt
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
výboj	výboj	k1gInSc1	výboj
blesku	blesk	k1gInSc2	blesk
<g/>
:	:	kIx,	:
ohrozit	ohrozit	k5eAaPmF	ohrozit
zdraví	zdraví	k1gNnSc4	zdraví
nebo	nebo	k8xC	nebo
životy	život	k1gInPc4	život
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
bytové	bytový	k2eAgInPc4d1	bytový
domy	dům	k1gInPc4	dům
<g/>
,	,	kIx,	,
nemocnice	nemocnice	k1gFnPc4	nemocnice
<g/>
,	,	kIx,	,
školy	škola	k1gFnPc4	škola
<g/>
)	)	kIx)	)
způsobit	způsobit	k5eAaPmF	způsobit
poruchu	porucha	k1gFnSc4	porucha
(	(	kIx(	(
<g/>
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
,	,	kIx,	,
plynárny	plynárna	k1gFnSc2	plynárna
<g/>
,	,	kIx,	,
vodárny	vodárna	k1gFnSc2	vodárna
<g/>
,	,	kIx,	,
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
)	)	kIx)	)
způsobit	způsobit	k5eAaPmF	způsobit
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
či	či	k8xC	či
kulturní	kulturní	k2eAgFnPc4d1	kulturní
škody	škoda	k1gFnPc4	škoda
(	(	kIx(	(
<g/>
výrobní	výrobní	k2eAgFnPc1d1	výrobní
haly	hala	k1gFnPc1	hala
<g/>
,	,	kIx,	,
muzea	muzeum	k1gNnPc1	muzeum
<g/>
,	,	kIx,	,
archivy	archiv	k1gInPc1	archiv
<g/>
)	)	kIx)	)
na	na	k7c6	na
objektech	objekt	k1gInPc6	objekt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
sousedí	sousedit	k5eAaImIp3nP	sousedit
s	s	k7c7	s
objekty	objekt	k1gInPc7	objekt
významnými	významný	k2eAgInPc7d1	významný
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zásahu	zásah	k1gInSc2	zásah
bleskem	blesk	k1gInSc7	blesk
by	by	kYmCp3nP	by
je	on	k3xPp3gMnPc4	on
mohly	moct	k5eAaImAgFnP	moct
ohrozit	ohrozit	k5eAaPmF	ohrozit
požárem	požár	k1gInSc7	požár
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
bouřkovým	bouřkový	k2eAgInSc7d1	bouřkový
mrakem	mrak	k1gInSc7	mrak
může	moct	k5eAaImIp3nS	moct
během	během	k7c2	během
bouřky	bouřka	k1gFnSc2	bouřka
vzniknout	vzniknout	k5eAaPmF	vzniknout
rozdíl	rozdíl	k1gInSc4	rozdíl
elektrických	elektrický	k2eAgInPc2d1	elektrický
potenciálů	potenciál	k1gInPc2	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
Překročí	překročit	k5eAaPmIp3nS	překročit
<g/>
-li	i	k?	-li
rozdíl	rozdíl	k1gInSc1	rozdíl
těchto	tento	k3xDgInPc2	tento
potenciálů	potenciál	k1gInPc2	potenciál
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
pevnost	pevnost	k1gFnSc4	pevnost
vzduchové	vzduchový	k2eAgFnSc2d1	vzduchová
vrstvy	vrstva	k1gFnSc2	vrstva
mezi	mezi	k7c7	mezi
takovýmto	takovýto	k3xDgInSc7	takovýto
mrakem	mrak	k1gInSc7	mrak
a	a	k8xC	a
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
elektrickému	elektrický	k2eAgInSc3d1	elektrický
bleskovému	bleskový	k2eAgInSc3d1	bleskový
výboji	výboj	k1gInSc3	výboj
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
výboji	výboj	k1gInSc3	výboj
dojde	dojít	k5eAaPmIp3nS	dojít
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vzduchová	vzduchový	k2eAgFnSc1d1	vzduchová
vrstva	vrstva	k1gFnSc1	vrstva
mezi	mezi	k7c7	mezi
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
mrakem	mrak	k1gInSc7	mrak
je	být	k5eAaImIp3nS	být
nejtenčí	tenký	k2eAgInSc1d3	nejtenčí
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bleskem	blesk	k1gInSc7	blesk
bývají	bývat	k5eAaImIp3nP	bývat
zasažena	zasáhnout	k5eAaPmNgNnP	zasáhnout
nejčastěji	často	k6eAd3	často
různá	různý	k2eAgNnPc1d1	různé
vyvýšená	vyvýšený	k2eAgNnPc1d1	vyvýšené
místa	místo	k1gNnPc1	místo
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
stromy	strom	k1gInPc4	strom
nebo	nebo	k8xC	nebo
budovy	budova	k1gFnPc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Hromosvod	hromosvod	k1gInSc1	hromosvod
využívá	využívat	k5eAaImIp3nS	využívat
této	tento	k3xDgFnSc2	tento
zákonitosti	zákonitost	k1gFnSc2	zákonitost
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
obecně	obecně	k6eAd1	obecně
popsat	popsat	k5eAaPmF	popsat
jako	jako	k8xC	jako
elektricky	elektricky	k6eAd1	elektricky
vodivý	vodivý	k2eAgInSc4d1	vodivý
předmět	předmět	k1gInSc4	předmět
umístěný	umístěný	k2eAgInSc4d1	umístěný
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
nad	nad	k7c7	nad
chráněným	chráněný	k2eAgInSc7d1	chráněný
objektem	objekt	k1gInSc7	objekt
a	a	k8xC	a
vodivě	vodivě	k6eAd1	vodivě
spojený	spojený	k2eAgInSc1d1	spojený
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
hromosvod	hromosvod	k1gInSc1	hromosvod
umístěn	umístit	k5eAaPmNgInS	umístit
nad	nad	k7c4	nad
chráněný	chráněný	k2eAgInSc4d1	chráněný
předmět	předmět	k1gInSc4	předmět
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
se	se	k3xPyFc4	se
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
blesk	blesk	k1gInSc1	blesk
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
hromosvod	hromosvod	k1gInSc4	hromosvod
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
bezpečně	bezpečně	k6eAd1	bezpečně
svede	svést	k5eAaPmIp3nS	svést
potenciál	potenciál	k1gInSc4	potenciál
blesku	blesk	k1gInSc2	blesk
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
objekt	objekt	k1gInSc1	objekt
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
hromosvodem	hromosvod	k1gInSc7	hromosvod
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
hromosvod	hromosvod	k1gInSc1	hromosvod
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
hlavní	hlavní	k2eAgFnPc4d1	hlavní
části	část	k1gFnPc4	část
–	–	k?	–
jímací	jímací	k2eAgNnSc4d1	jímací
zařízení	zařízení	k1gNnSc4	zařízení
(	(	kIx(	(
<g/>
jímač	jímač	k1gInSc1	jímač
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svod	svod	k1gInSc1	svod
a	a	k8xC	a
uzemnění	uzemnění	k1gNnSc1	uzemnění
<g/>
.	.	kIx.	.
</s>
<s>
Jímač	jímač	k1gInSc1	jímač
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
zachycení	zachycení	k1gNnSc4	zachycení
blesku	blesk	k1gInSc2	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
umístěn	umístit	k5eAaPmNgInS	umístit
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
chráněného	chráněný	k2eAgInSc2d1	chráněný
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
výše	vysoce	k6eAd2	vysoce
než	než	k8xS	než
nejvýše	vysoce	k6eAd3	vysoce
umístěné	umístěný	k2eAgFnPc4d1	umístěná
části	část	k1gFnPc4	část
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
blesk	blesk	k1gInSc1	blesk
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
nejvýše	vysoce	k6eAd3	vysoce
umístěné	umístěný	k2eAgInPc1d1	umístěný
předměty	předmět	k1gInPc1	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Svod	svod	k1gInSc1	svod
je	být	k5eAaImIp3nS	být
elektrický	elektrický	k2eAgInSc4d1	elektrický
vodič	vodič	k1gInSc4	vodič
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vodivě	vodivě	k6eAd1	vodivě
spojuje	spojovat	k5eAaImIp3nS	spojovat
jímač	jímač	k1gInSc1	jímač
blesku	blesk	k1gInSc2	blesk
s	s	k7c7	s
uzemněním	uzemnění	k1gNnSc7	uzemnění
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
proveden	provést	k5eAaPmNgInS	provést
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
charakteristických	charakteristický	k2eAgNnPc2d1	charakteristické
ocelových	ocelový	k2eAgNnPc2d1	ocelové
lan	lano	k1gNnPc2	lano
vedených	vedený	k2eAgFnPc2d1	vedená
svisle	svisel	k1gFnPc4	svisel
na	na	k7c6	na
vnějších	vnější	k2eAgFnPc6d1	vnější
stěnách	stěna	k1gFnPc6	stěna
objektu	objekt	k1gInSc2	objekt
od	od	k7c2	od
střechy	střecha	k1gFnSc2	střecha
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Uzemnění	uzemnění	k1gNnPc1	uzemnění
tvoří	tvořit	k5eAaImIp3nP	tvořit
nejčastěji	často	k6eAd3	často
pásy	pás	k1gInPc1	pás
ze	z	k7c2	z
zinkované	zinkovaný	k2eAgFnSc2d1	zinkovaná
oceli	ocel	k1gFnSc2	ocel
zakopané	zakopaný	k2eAgFnSc2d1	zakopaná
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
podél	podél	k7c2	podél
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Jímače	jímač	k1gInPc1	jímač
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
provedeny	provést	k5eAaPmNgInP	provést
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
jímací	jímací	k2eAgFnSc2d1	jímací
tyče	tyč	k1gFnSc2	tyč
<g/>
,	,	kIx,	,
jímacího	jímací	k2eAgNnSc2d1	jímací
vedení	vedení	k1gNnSc2	vedení
nebo	nebo	k8xC	nebo
mřížového	mřížový	k2eAgInSc2d1	mřížový
jímače	jímač	k1gInSc2	jímač
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
jímače	jímač	k1gInPc4	jímač
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
strojené	strojený	k2eAgNnSc4d1	strojené
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jímače	jímač	k1gInPc1	jímač
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
využity	využít	k5eAaPmNgInP	využít
též	též	k9	též
i	i	k9	i
vhodné	vhodný	k2eAgInPc4d1	vhodný
vodivé	vodivý	k2eAgInPc4d1	vodivý
konstrukční	konstrukční	k2eAgInPc4d1	konstrukční
prvky	prvek	k1gInPc4	prvek
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
např.	např.	kA	např.
plechová	plechový	k2eAgFnSc1d1	plechová
krytina	krytina	k1gFnSc1	krytina
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc4	ten
pak	pak	k6eAd1	pak
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
náhodné	náhodný	k2eAgInPc4d1	náhodný
jímače	jímač	k1gInPc4	jímač
<g/>
.	.	kIx.	.
</s>
<s>
Jímací	jímací	k2eAgFnSc1d1	jímací
tyč	tyč	k1gFnSc1	tyč
je	být	k5eAaImIp3nS	být
plná	plný	k2eAgFnSc1d1	plná
ocelová	ocelový	k2eAgFnSc1d1	ocelová
tyč	tyč	k1gFnSc1	tyč
kolmo	kolmo	k6eAd1	kolmo
vztyčená	vztyčený	k2eAgFnSc1d1	vztyčená
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
jímače	jímač	k1gInSc2	jímač
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
pro	pro	k7c4	pro
hromosvody	hromosvod	k1gInPc4	hromosvod
mnoha	mnoho	k4c2	mnoho
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
těch	ten	k3xDgMnPc2	ten
starších	starší	k1gMnPc2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Ochranné	ochranný	k2eAgNnSc1d1	ochranné
pásmo	pásmo	k1gNnSc1	pásmo
tyčového	tyčový	k2eAgInSc2d1	tyčový
jímače	jímač	k1gInSc2	jímač
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc1	tvar
kužele	kužel	k1gInSc2	kužel
s	s	k7c7	s
vrcholem	vrchol	k1gInSc7	vrchol
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
konci	konec	k1gInSc6	konec
tyče	tyč	k1gFnSc2	tyč
a	a	k8xC	a
sklonem	sklon	k1gInSc7	sklon
pláště	plášť	k1gInSc2	plášť
45	[number]	k4	45
<g/>
°	°	k?	°
(	(	kIx(	(
<g/>
vrcholový	vrcholový	k2eAgInSc1d1	vrcholový
úhel	úhel	k1gInSc1	úhel
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jímací	jímací	k2eAgFnSc1d1	jímací
tyč	tyč	k1gFnSc1	tyč
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
tedy	tedy	k9	tedy
takovou	takový	k3xDgFnSc4	takový
výšku	výška	k1gFnSc4	výška
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
její	její	k3xOp3gNnSc1	její
ochranné	ochranný	k2eAgNnSc1d1	ochranné
pásmo	pásmo	k1gNnSc1	pásmo
spolehlivě	spolehlivě	k6eAd1	spolehlivě
pokrylo	pokrýt	k5eAaPmAgNnS	pokrýt
všechny	všechen	k3xTgFnPc4	všechen
části	část	k1gFnPc4	část
chráněného	chráněný	k2eAgInSc2d1	chráněný
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
objekt	objekt	k1gInSc4	objekt
větší	veliký	k2eAgInSc4d2	veliký
půdorysnou	půdorysný	k2eAgFnSc4d1	půdorysná
plochu	plocha	k1gFnSc4	plocha
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
rozmístit	rozmístit	k5eAaPmF	rozmístit
více	hodně	k6eAd2	hodně
jímacích	jímací	k2eAgFnPc2d1	jímací
tyčí	tyč	k1gFnPc2	tyč
<g/>
.	.	kIx.	.
</s>
<s>
Jímací	jímací	k2eAgNnSc1d1	jímací
vedení	vedení	k1gNnSc1	vedení
tvoří	tvořit	k5eAaImIp3nS	tvořit
ocelový	ocelový	k2eAgInSc4d1	ocelový
pozinkovaný	pozinkovaný	k2eAgInSc4d1	pozinkovaný
drát	drát	k1gInSc4	drát
průměru	průměr	k1gInSc2	průměr
8	[number]	k4	8
nebo	nebo	k8xC	nebo
10	[number]	k4	10
mm	mm	kA	mm
vedený	vedený	k2eAgInSc1d1	vedený
po	po	k7c6	po
střeše	střecha	k1gFnSc6	střecha
objektu	objekt	k1gInSc2	objekt
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
s	s	k7c7	s
jejím	její	k3xOp3gInSc7	její
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Ochranné	ochranný	k2eAgNnSc1d1	ochranné
pásmo	pásmo	k1gNnSc1	pásmo
jímacího	jímací	k2eAgNnSc2d1	jímací
vedení	vedení	k1gNnSc2	vedení
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
se	s	k7c7	s
sklonem	sklon	k1gInSc7	sklon
45	[number]	k4	45
<g/>
°	°	k?	°
na	na	k7c4	na
obě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
a	a	k8xC	a
vrcholem	vrchol	k1gInSc7	vrchol
90	[number]	k4	90
<g/>
°	°	k?	°
na	na	k7c6	na
jímacím	jímací	k2eAgNnSc6d1	jímací
vedení	vedení	k1gNnSc6	vedení
<g/>
,	,	kIx,	,
sunutého	sunutý	k2eAgInSc2d1	sunutý
po	po	k7c6	po
ose	osa	k1gFnSc6	osa
jímacího	jímací	k2eAgNnSc2d1	jímací
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
tedy	tedy	k9	tedy
jímací	jímací	k2eAgNnSc4d1	jímací
vedení	vedení	k1gNnSc4	vedení
taženo	tažen	k2eAgNnSc4d1	taženo
po	po	k7c6	po
hřebenu	hřeben	k1gInSc6	hřeben
sedlové	sedlový	k2eAgFnSc2d1	sedlová
střechy	střecha	k1gFnSc2	střecha
se	s	k7c7	s
sklonem	sklon	k1gInSc7	sklon
větším	veliký	k2eAgInSc7d2	veliký
nebo	nebo	k8xC	nebo
rovným	rovný	k2eAgInSc7d1	rovný
45	[number]	k4	45
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
bleskem	blesk	k1gInSc7	blesk
dostačující	dostačující	k2eAgMnPc1d1	dostačující
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
sklon	sklon	k1gInSc4	sklon
střechy	střecha	k1gFnSc2	střecha
menší	malý	k2eAgFnSc2d2	menší
než	než	k8xS	než
45	[number]	k4	45
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
jímací	jímací	k2eAgNnSc1d1	jímací
vedení	vedení	k1gNnSc1	vedení
na	na	k7c6	na
hřebenu	hřeben	k1gInSc6	hřeben
doplněno	doplnit	k5eAaPmNgNnS	doplnit
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
vzdálenostech	vzdálenost	k1gFnPc6	vzdálenost
příčnými	příčný	k2eAgNnPc7d1	příčné
vedeními	vedení	k1gNnPc7	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
mřížová	mřížový	k2eAgFnSc1d1	mřížová
jímací	jímací	k2eAgFnSc1d1	jímací
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Rozteč	rozteč	k1gFnSc1	rozteč
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
příčnými	příčný	k2eAgInPc7d1	příčný
dráty	drát	k1gInPc7	drát
nesmí	smět	k5eNaImIp3nS	smět
překročit	překročit	k5eAaPmF	překročit
20	[number]	k4	20
m	m	kA	m
a	a	k8xC	a
rozteč	rozteč	k1gFnSc1	rozteč
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
podélnými	podélný	k2eAgInPc7d1	podélný
dráty	drát	k1gInPc7	drát
nesmí	smět	k5eNaImIp3nS	smět
překročit	překročit	k5eAaPmF	překročit
60	[number]	k4	60
m	m	kA	m
(	(	kIx(	(
<g/>
15	[number]	k4	15
m	m	kA	m
u	u	k7c2	u
zesíleného	zesílený	k2eAgInSc2d1	zesílený
hromosvodu	hromosvod	k1gInSc2	hromosvod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příčná	příčný	k2eAgNnPc1d1	příčné
a	a	k8xC	a
podélná	podélný	k2eAgNnPc1d1	podélné
vedení	vedení	k1gNnPc1	vedení
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
bodech	bod	k1gInPc6	bod
propojena	propojen	k2eAgFnSc1d1	propojena
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tvoří	tvořit	k5eAaImIp3nP	tvořit
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
objektu	objekt	k1gInSc2	objekt
vyvýšená	vyvýšený	k2eAgFnSc1d1	vyvýšená
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
např.	např.	kA	např.
komín	komín	k1gInSc1	komín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nepokrývá	pokrývat	k5eNaImIp3nS	pokrývat
ochranné	ochranný	k2eAgNnSc1d1	ochranné
pásmo	pásmo	k1gNnSc1	pásmo
jímací	jímací	k2eAgFnSc2d1	jímací
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
vybavena	vybavit	k5eAaPmNgFnS	vybavit
vlastní	vlastní	k2eAgFnSc7d1	vlastní
jímací	jímací	k2eAgFnSc7d1	jímací
tyčí	tyč	k1gFnSc7	tyč
<g/>
.	.	kIx.	.
</s>
<s>
Svod	svod	k1gInSc1	svod
spojuje	spojovat	k5eAaImIp3nS	spojovat
jímací	jímací	k2eAgFnSc4d1	jímací
soustavu	soustava	k1gFnSc4	soustava
s	s	k7c7	s
uzemněním	uzemnění	k1gNnSc7	uzemnění
(	(	kIx(	(
<g/>
zemniči	zemnič	k1gInSc6	zemnič
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svody	svod	k1gInPc1	svod
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
strojené	strojený	k2eAgInPc1d1	strojený
<g/>
,	,	kIx,	,
vodiči	vodič	k1gInPc7	vodič
vedenými	vedený	k2eAgInPc7d1	vedený
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
objektu	objekt	k1gInSc2	objekt
či	či	k8xC	či
v	v	k7c6	v
omítce	omítka	k1gFnSc6	omítka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
náhodné	náhodný	k2eAgNnSc4d1	náhodné
(	(	kIx(	(
<g/>
využití	využití	k1gNnSc4	využití
stávajících	stávající	k2eAgInPc2d1	stávající
prvků	prvek	k1gInPc2	prvek
konstrukce	konstrukce	k1gFnSc2	konstrukce
objektu	objekt	k1gInSc2	objekt
-	-	kIx~	-
ocelových	ocelový	k2eAgInPc2d1	ocelový
sloupů	sloup	k1gInPc2	sloup
<g/>
,	,	kIx,	,
výztuže	výztuž	k1gFnPc1	výztuž
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Počet	počet	k1gInSc1	počet
svodů	svod	k1gInPc2	svod
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
obvodem	obvod	k1gInSc7	obvod
půdorysu	půdorys	k1gInSc2	půdorys
chráněného	chráněný	k2eAgInSc2d1	chráněný
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každých	každý	k3xTgFnPc6	každý
započatých	započatý	k2eAgFnPc6d1	započatá
30	[number]	k4	30
m	m	kA	m
obvodu	obvod	k1gInSc2	obvod
půdorysu	půdorys	k1gInSc2	půdorys
objektu	objekt	k1gInSc2	objekt
náleží	náležet	k5eAaImIp3nS	náležet
jeden	jeden	k4xCgInSc4	jeden
svod	svod	k1gInSc4	svod
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
půdorysný	půdorysný	k2eAgInSc1d1	půdorysný
obvod	obvod	k1gInSc1	obvod
menší	malý	k2eAgInSc1d2	menší
než	než	k8xS	než
30	[number]	k4	30
m	m	kA	m
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
objekt	objekt	k1gInSc1	objekt
vybaven	vybavit	k5eAaPmNgInS	vybavit
alespoň	alespoň	k9	alespoň
dvěma	dva	k4xCgInPc7	dva
svody	svod	k1gInPc7	svod
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
objekt	objekt	k1gInSc1	objekt
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
30	[number]	k4	30
m	m	kA	m
<g/>
,	,	kIx,	,
umísťuje	umísťovat	k5eAaImIp3nS	umísťovat
se	se	k3xPyFc4	se
jeden	jeden	k4xCgInSc1	jeden
svod	svod	k1gInSc1	svod
na	na	k7c4	na
každých	každý	k3xTgInPc2	každý
15	[number]	k4	15
započatých	započatý	k2eAgInPc2d1	započatý
metrů	metr	k1gInPc2	metr
jeho	on	k3xPp3gInSc2	on
půdorysného	půdorysný	k2eAgInSc2d1	půdorysný
obvodu	obvod	k1gInSc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Svod	svod	k1gInSc1	svod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
veden	vést	k5eAaImNgInS	vést
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
střechy	střecha	k1gFnSc2	střecha
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
také	také	k9	také
jako	jako	k9	jako
jímač	jímač	k1gInSc4	jímač
<g/>
.	.	kIx.	.
</s>
<s>
Uzemnění	uzemnění	k1gNnSc1	uzemnění
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
provedeno	provést	k5eAaPmNgNnS	provést
zemnícími	zemnící	k2eAgFnPc7d1	zemnící
tyčemi	tyč	k1gFnPc7	tyč
<g/>
,	,	kIx,	,
deskami	deska	k1gFnPc7	deska
<g/>
,	,	kIx,	,
dráty	drát	k1gInPc7	drát
<g/>
,	,	kIx,	,
či	či	k8xC	či
pásky	pásek	k1gInPc1	pásek
<g/>
,	,	kIx,	,
uloženo	uložit	k5eAaPmNgNnS	uložit
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
základovém	základový	k2eAgInSc6d1	základový
betonu	beton	k1gInSc6	beton
<g/>
.	.	kIx.	.
</s>
<s>
Svodové	svodový	k2eAgInPc1d1	svodový
vodiče	vodič	k1gInPc1	vodič
jsou	být	k5eAaImIp3nP	být
se	s	k7c7	s
zemniči	zemnič	k1gInPc7	zemnič
spojeny	spojit	k5eAaPmNgInP	spojit
rozpojitelnými	rozpojitelný	k2eAgFnPc7d1	rozpojitelná
šroubovacími	šroubovací	k2eAgFnPc7d1	šroubovací
svorkami	svorka	k1gFnPc7	svorka
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
hromosvod	hromosvod	k1gInSc1	hromosvod
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
konstrukcí	konstrukce	k1gFnSc7	konstrukce
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
izolovaný	izolovaný	k2eAgMnSc1d1	izolovaný
od	od	k7c2	od
chráněné	chráněný	k2eAgFnSc2d1	chráněná
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
"	"	kIx"	"
<g/>
klasických	klasický	k2eAgInPc2d1	klasický
<g/>
"	"	kIx"	"
hromosvodů	hromosvod	k1gInPc2	hromosvod
(	(	kIx(	(
<g/>
Franklinova	Franklinův	k2eAgInSc2d1	Franklinův
typu	typ	k1gInSc2	typ
–	–	k?	–
hřebenové	hřebenový	k2eAgFnPc1d1	hřebenová
<g/>
,	,	kIx,	,
mřížové	mřížový	k2eAgFnPc1d1	mřížová
<g/>
,	,	kIx,	,
tyčové	tyčový	k2eAgFnPc1d1	tyčová
<g/>
,	,	kIx,	,
oddálené	oddálený	k2eAgFnPc1d1	oddálená
<g/>
,	,	kIx,	,
stožárové	stožárový	k2eAgFnPc1d1	stožárová
<g/>
,	,	kIx,	,
závěsové	závěsový	k2eAgFnPc1d1	závěsová
<g/>
,	,	kIx,	,
klecové	klecový	k2eAgFnPc1d1	klecová
<g/>
)	)	kIx)	)
existují	existovat	k5eAaImIp3nP	existovat
tzv.	tzv.	kA	tzv.
hromosvody	hromosvod	k1gInPc4	hromosvod
aktivní	aktivní	k2eAgMnSc1d1	aktivní
(	(	kIx(	(
<g/>
zařízení	zařízení	k1gNnSc1	zařízení
se	s	k7c7	s
včasnou	včasný	k2eAgFnSc7d1	včasná
emisí	emise	k1gFnSc7	emise
výboje	výboj	k1gInSc2	výboj
<g/>
,	,	kIx,	,
PDA	PDA	kA	PDA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
PDA	PDA	kA	PDA
jsou	být	k5eAaImIp3nP	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
aktivním	aktivní	k2eAgInSc7d1	aktivní
jímačem	jímač	k1gInSc7	jímač
a	a	k8xC	a
svodem	svod	k1gInSc7	svod
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
Franklinova	Franklinův	k2eAgInSc2d1	Franklinův
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgInSc1d1	aktivní
jímač	jímač	k1gInSc1	jímač
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
elektrický	elektrický	k2eAgInSc4d1	elektrický
potenciál	potenciál	k1gInSc4	potenciál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
při	při	k7c6	při
včasné	včasný	k2eAgFnSc6d1	včasná
emisi	emise	k1gFnSc6	emise
stáhne	stáhnout	k5eAaPmIp3nS	stáhnout
výboj	výboj	k1gInSc4	výboj
bouřkového	bouřkový	k2eAgInSc2d1	bouřkový
mraku	mrak	k1gInSc2	mrak
do	do	k7c2	do
aktivního	aktivní	k2eAgInSc2d1	aktivní
jímače	jímač	k1gInSc2	jímač
na	na	k7c6	na
základě	základ	k1gInSc6	základ
ochranného	ochranný	k2eAgInSc2d1	ochranný
poloměru	poloměr	k1gInSc2	poloměr
jímače	jímač	k1gInSc2	jímač
<g/>
.	.	kIx.	.
</s>
<s>
Takovýto	takovýto	k3xDgInSc1	takovýto
typ	typ	k1gInSc1	typ
hromosvodu	hromosvod	k1gInSc3	hromosvod
bývá	bývat	k5eAaImIp3nS	bývat
použit	použít	k5eAaPmNgInS	použít
u	u	k7c2	u
otevřených	otevřený	k2eAgInPc2d1	otevřený
prostorů	prostor	k1gInPc2	prostor
a	a	k8xC	a
velkých	velký	k2eAgInPc2d1	velký
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
jímačů	jímač	k1gInPc2	jímač
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
chránit	chránit	k5eAaImF	chránit
různě	různě	k6eAd1	různě
velké	velký	k2eAgInPc4d1	velký
objekty	objekt	k1gInPc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
PDA	PDA	kA	PDA
byla	být	k5eAaImAgFnS	být
testována	testovat	k5eAaImNgFnS	testovat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
laboratořích	laboratoř	k1gFnPc6	laboratoř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prokázali	prokázat	k5eAaPmAgMnP	prokázat
jejich	jejich	k3xOp3gFnSc4	jejich
účinnost	účinnost	k1gFnSc4	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgInPc1	veškerý
funkční	funkční	k2eAgInPc1d1	funkční
aktivní	aktivní	k2eAgInPc1d1	aktivní
hromosvody	hromosvod	k1gInPc1	hromosvod
jsou	být	k5eAaImIp3nP	být
testovány	testovat	k5eAaImNgInP	testovat
a	a	k8xC	a
certifikovány	certifikovat	k5eAaImNgInP	certifikovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
existuje	existovat	k5eAaImIp3nS	existovat
už	už	k6eAd1	už
mnoho	mnoho	k4c1	mnoho
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
aktivní	aktivní	k2eAgInPc4d1	aktivní
jímače	jímač	k1gInPc4	jímač
PDA	PDA	kA	PDA
<g/>
.	.	kIx.	.
</s>
<s>
PDA	PDA	kA	PDA
je	být	k5eAaImIp3nS	být
schopný	schopný	k2eAgInSc1d1	schopný
svést	svést	k5eAaPmF	svést
mnoho	mnoho	k4c1	mnoho
výbojů	výboj	k1gInPc2	výboj
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
použit	použít	k5eAaPmNgInS	použít
na	na	k7c6	na
památných	památný	k2eAgInPc6d1	památný
objektech	objekt	k1gInPc6	objekt
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
použít	použít	k5eAaPmF	použít
mnoho	mnoho	k4c4	mnoho
drátových	drátový	k2eAgInPc2d1	drátový
svodů	svod	k1gInPc2	svod
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
PDA	PDA	kA	PDA
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
pouze	pouze	k6eAd1	pouze
Franklinův	Franklinův	k2eAgInSc4d1	Franklinův
typ	typ	k1gInSc4	typ
<g/>
,	,	kIx,	,
považují	považovat	k5eAaImIp3nP	považovat
aktivní	aktivní	k2eAgInPc1d1	aktivní
jímače	jímač	k1gInPc1	jímač
za	za	k7c4	za
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
komerční	komerční	k2eAgInSc4d1	komerční
trik	trik	k1gInSc4	trik
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
podvod	podvod	k1gInSc1	podvod
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
platná	platný	k2eAgFnSc1d1	platná
norma	norma	k1gFnSc1	norma
použití	použití	k1gNnSc2	použití
aktivních	aktivní	k2eAgInPc2d1	aktivní
hromosvodů	hromosvod	k1gInPc2	hromosvod
nezakazuje	zakazovat	k5eNaImIp3nS	zakazovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
stanovení	stanovení	k1gNnSc4	stanovení
ochranného	ochranný	k2eAgInSc2d1	ochranný
prostoru	prostor	k1gInSc2	prostor
předepisuje	předepisovat	k5eAaImIp3nS	předepisovat
uvažovat	uvažovat	k5eAaImF	uvažovat
pouze	pouze	k6eAd1	pouze
mechanické	mechanický	k2eAgInPc1d1	mechanický
rozměry	rozměr	k1gInPc1	rozměr
aktivního	aktivní	k2eAgInSc2d1	aktivní
jímače	jímač	k1gInSc2	jímač
<g/>
.	.	kIx.	.
</s>
<s>
Materiály	materiál	k1gInPc4	materiál
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
nejvíce	nejvíce	k6eAd1	nejvíce
používané	používaný	k2eAgInPc1d1	používaný
na	na	k7c4	na
jímací	jímací	k2eAgNnSc4d1	jímací
vedení	vedení	k1gNnSc4	vedení
<g/>
,	,	kIx,	,
svody	svod	k1gInPc4	svod
a	a	k8xC	a
uzemnění	uzemnění	k1gNnSc4	uzemnění
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
žárově	žárově	k6eAd1	žárově
zinkovaná	zinkovaný	k2eAgFnSc1d1	zinkovaná
ocel	ocel	k1gFnSc1	ocel
(	(	kIx(	(
<g/>
železo	železo	k1gNnSc1	železo
a	a	k8xC	a
zinek	zinek	k1gInSc1	zinek
<g/>
)	)	kIx)	)
měď	měď	k1gFnSc1	měď
slitiny	slitina	k1gFnSc2	slitina
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
např.	např.	kA	např.
dural	dural	k1gInSc1	dural
(	(	kIx(	(
<g/>
hliník	hliník	k1gInSc1	hliník
<g/>
,	,	kIx,	,
hořčík	hořčík	k1gInSc1	hořčík
<g/>
,	,	kIx,	,
křemík	křemík	k1gInSc1	křemík
<g/>
)	)	kIx)	)
nerezavějící	rezavějící	k2eNgFnSc1d1	nerezavějící
ocel	ocel	k1gFnSc1	ocel
blesk	blesk	k1gInSc1	blesk
bouřka	bouřka	k1gFnSc1	bouřka
bleskojistka	bleskojistka	k1gFnSc1	bleskojistka
uzemnění	uzemnění	k1gNnSc2	uzemnění
TKOTZ	TKOTZ	kA	TKOTZ
<g/>
,	,	kIx,	,
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Příručka	příručka	k1gFnSc1	příručka
pro	pro	k7c4	pro
elektrotechnika	elektrotechnik	k1gMnSc4	elektrotechnik
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Europa	Europa	k1gFnSc1	Europa
-	-	kIx~	-
Sobotáles	sobotáles	k1gInSc1	sobotáles
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
561	[number]	k4	561
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86706	[number]	k4	86706
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Ochrana	ochrana	k1gFnSc1	ochrana
budov	budova	k1gFnPc2	budova
před	před	k7c7	před
bleskem	blesk	k1gInSc7	blesk
<g/>
,	,	kIx,	,
s.	s.	k?	s.
488	[number]	k4	488
-	-	kIx~	-
490	[number]	k4	490
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hromosvod	hromosvod	k1gInSc4	hromosvod
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
jsou	být	k5eAaImIp3nP	být
bleskosvody	bleskosvod	k1gInPc1	bleskosvod
</s>
