<s>
Hromosvod	hromosvod	k1gInSc1	hromosvod
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
premonstrát	premonstrát	k1gMnSc1	premonstrát
Prokop	Prokop	k1gMnSc1	Prokop
Diviš	Diviš	k1gMnSc1	Diviš
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
farní	farní	k2eAgFnSc6d1	farní
zahradě	zahrada	k1gFnSc6	zahrada
v	v	k7c6	v
Příměticích	Přímětice	k1gFnPc6	Přímětice
blízko	blízko	k7c2	blízko
Znojma	Znojmo	k1gNnSc2	Znojmo
umístil	umístit	k5eAaPmAgMnS	umístit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1754	[number]	k4	1754
první	první	k4xOgInSc1	první
hromosvod	hromosvod	k1gInSc1	hromosvod
<g/>
.	.	kIx.	.
</s>
