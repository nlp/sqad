<p>
<s>
Uganda	Uganda	k1gFnSc1	Uganda
je	být	k5eAaImIp3nS	být
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
Keňou	Keňa	k1gFnSc7	Keňa
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
s	s	k7c7	s
Jižním	jižní	k2eAgInSc7d1	jižní
Súdánem	Súdán	k1gInSc7	Súdán
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
s	s	k7c7	s
Demokratickou	demokratický	k2eAgFnSc7d1	demokratická
republikou	republika	k1gFnSc7	republika
Kongo	Kongo	k1gNnSc4	Kongo
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Rwandou	Rwanda	k1gFnSc7	Rwanda
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
a	a	k8xC	a
s	s	k7c7	s
Tanzanií	Tanzanie	k1gFnSc7	Tanzanie
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
Ugandy	Uganda	k1gFnSc2	Uganda
je	být	k5eAaImIp3nS	být
i	i	k9	i
nezanedbatelná	zanedbatelný	k2eNgFnSc1d1	nezanedbatelná
část	část	k1gFnSc1	část
Viktoriina	Viktoriin	k2eAgNnSc2d1	Viktoriino
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
také	také	k6eAd1	také
procházejí	procházet	k5eAaImIp3nP	procházet
hranice	hranice	k1gFnPc4	hranice
s	s	k7c7	s
Keňou	Keňa	k1gFnSc7	Keňa
a	a	k8xC	a
Tanzanií	Tanzanie	k1gFnSc7	Tanzanie
<g/>
.	.	kIx.	.
</s>
<s>
Uganda	Uganda	k1gFnSc1	Uganda
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
241	[number]	k4	241
548	[number]	k4	548
km2	km2	k4	km2
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejhustěji	husto	k6eAd3	husto
zalidněné	zalidněný	k2eAgFnPc4d1	zalidněná
země	zem	k1gFnPc4	zem
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
nadále	nadále	k6eAd1	nadále
rychle	rychle	k6eAd1	rychle
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
názvu	název	k1gInSc2	název
tradičního	tradiční	k2eAgNnSc2d1	tradiční
království	království	k1gNnSc2	království
Buganda	Bugando	k1gNnSc2	Bugando
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
země	zem	k1gFnSc2	zem
včetně	včetně	k7c2	včetně
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Kampaly	Kampal	k1gMnPc4	Kampal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
Ugandy	Uganda	k1gFnSc2	Uganda
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Arabů	Arab	k1gMnPc2	Arab
a	a	k8xC	a
Evropanů	Evropan	k1gMnPc2	Evropan
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
toho	ten	k3xDgNnSc2	ten
ví	vědět	k5eAaImIp3nS	vědět
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jisté	jistý	k2eAgNnSc1d1	jisté
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
lidé	člověk	k1gMnPc1	člověk
usadili	usadit	k5eAaPmAgMnP	usadit
nejpozději	pozdě	k6eAd3	pozdě
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
př.n.l.	př.n.l.	k?	př.n.l.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
sem	sem	k6eAd1	sem
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dorazili	dorazit	k5eAaPmAgMnP	dorazit
Arabové	Arab	k1gMnPc1	Arab
a	a	k8xC	a
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
,	,	kIx,	,
narazili	narazit	k5eAaPmAgMnP	narazit
na	na	k7c4	na
několik	několik	k4yIc4	několik
nezávislých	závislý	k2eNgNnPc2d1	nezávislé
království	království	k1gNnPc2	království
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
byly	být	k5eAaImAgFnP	být
především	především	k9	především
Buganda	Buganda	k1gFnSc1	Buganda
<g/>
,	,	kIx,	,
Buňoro-Kitara	Buňoro-Kitara	k1gFnSc1	Buňoro-Kitara
<g/>
,	,	kIx,	,
Nkore	Nkor	k1gInSc5	Nkor
a	a	k8xC	a
Toro	Toro	k?	Toro
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
království	království	k1gNnPc2	království
byla	být	k5eAaImAgFnS	být
Buganda	Buganda	k1gFnSc1	Buganda
<g/>
,	,	kIx,	,
existující	existující	k2eAgInSc1d1	existující
dnes	dnes	k6eAd1	dnes
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
Ugandy	Uganda	k1gFnSc2	Uganda
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
existovala	existovat	k5eAaImAgFnS	existovat
ještě	ještě	k9	ještě
busogská	busogská	k1gFnSc1	busogská
knížectví	knížectví	k1gNnSc2	knížectví
a	a	k8xC	a
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
menších	malý	k2eAgInPc2d2	menší
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Evropany	Evropan	k1gMnPc7	Evropan
a	a	k8xC	a
Araby	Arab	k1gMnPc7	Arab
sem	sem	k6eAd1	sem
přišel	přijít	k5eAaPmAgMnS	přijít
Islám	islám	k1gInSc4	islám
a	a	k8xC	a
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uganda	Uganda	k1gFnSc1	Uganda
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
Britské	britský	k2eAgFnSc2d1	britská
Východoafrické	východoafrický	k2eAgFnSc2d1	východoafrická
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
byla	být	k5eAaImAgFnS	být
britským	britský	k2eAgInSc7d1	britský
protektorátem	protektorát	k1gInSc7	protektorát
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1896	[number]	k4	1896
byl	být	k5eAaImAgInS	být
protektorát	protektorát	k1gInSc1	protektorát
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
na	na	k7c4	na
Buňoro-Kitaru	Buňoro-Kitara	k1gFnSc4	Buňoro-Kitara
<g/>
,	,	kIx,	,
Busogu	Busoga	k1gFnSc4	Busoga
<g/>
,	,	kIx,	,
Nkore	Nkor	k1gMnSc5	Nkor
<g/>
,	,	kIx,	,
Toro	Toro	k?	Toro
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
protektorát	protektorát	k1gInSc4	protektorát
Uganda	Uganda	k1gFnSc1	Uganda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
rámci	rámec	k1gInSc6	rámec
Nkore	Nkor	k1gInSc5	Nkor
a	a	k8xC	a
několik	několik	k4yIc1	několik
menších	malý	k2eAgInPc2d2	menší
států	stát	k1gInPc2	stát
sjednotilo	sjednotit	k5eAaPmAgNnS	sjednotit
do	do	k7c2	do
království	království	k1gNnSc2	království
Ankole	Ankole	k1gFnSc2	Ankole
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1902	[number]	k4	1902
byly	být	k5eAaImAgFnP	být
východní	východní	k2eAgFnPc1d1	východní
oblasti	oblast	k1gFnPc1	oblast
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
Ugandského	ugandský	k2eAgInSc2d1	ugandský
protektorátu	protektorát	k1gInSc2	protektorát
začleněny	začlenit	k5eAaPmNgFnP	začlenit
do	do	k7c2	do
pozdější	pozdní	k2eAgFnSc2d2	pozdější
Keni	Keňa	k1gFnSc2	Keňa
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	Hranice	k1gFnPc1	Hranice
protektorátu	protektorát	k1gInSc2	protektorát
doznaly	doznat	k5eAaPmAgFnP	doznat
během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
ještě	ještě	k6eAd1	ještě
několika	několik	k4yIc2	několik
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1906	[number]	k4	1906
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Ugandy	Uganda	k1gFnSc2	Uganda
ke	k	k7c3	k
sjednocení	sjednocení	k1gNnSc3	sjednocení
busogských	busogských	k2eAgNnSc2d1	busogských
knížectví	knížectví	k1gNnSc2	knížectví
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
útvaru	útvar	k1gInSc2	útvar
dnes	dnes	k6eAd1	dnes
známého	známý	k2eAgNnSc2d1	známé
jako	jako	k8xS	jako
království	království	k1gNnSc2	království
Busoga	Busog	k1gMnSc2	Busog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
pak	pak	k6eAd1	pak
získala	získat	k5eAaPmAgFnS	získat
Uganda	Uganda	k1gFnSc1	Uganda
nezávislost	nezávislost	k1gFnSc1	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
nová	nový	k2eAgFnSc1d1	nová
ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
Ugandě	Uganda	k1gFnSc6	Uganda
zavedla	zavést	k5eAaPmAgFnS	zavést
státní	státní	k2eAgNnSc4d1	státní
zřízení	zřízení	k1gNnSc4	zřízení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
bylo	být	k5eAaImAgNnS	být
mixem	mix	k1gInSc7	mix
federace	federace	k1gFnSc2	federace
a	a	k8xC	a
unitárního	unitární	k2eAgInSc2d1	unitární
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
základě	základ	k1gInSc6	základ
mělo	mít	k5eAaImAgNnS	mít
království	království	k1gNnSc1	království
Buganda	Bugando	k1gNnSc2	Bugando
(	(	kIx(	(
<g/>
současně	současně	k6eAd1	současně
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
4	[number]	k4	4
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
formálních	formální	k2eAgFnPc2d1	formální
oblastí	oblast	k1gFnPc2	oblast
-	-	kIx~	-
vedle	vedle	k7c2	vedle
Severní	severní	k2eAgFnSc2d1	severní
<g/>
,	,	kIx,	,
Západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
Východní	východní	k2eAgFnSc2d1	východní
oblasti	oblast	k1gFnSc2	oblast
<g/>
)	)	kIx)	)
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
4	[number]	k4	4
celky	celek	k1gInPc4	celek
(	(	kIx(	(
<g/>
království	království	k1gNnSc3	království
Ankole	Ankole	k1gFnSc2	Ankole
<g/>
,	,	kIx,	,
království	království	k1gNnSc1	království
Buňoro-Kitara	Buňoro-Kitara	k1gFnSc1	Buňoro-Kitara
<g/>
,	,	kIx,	,
království	království	k1gNnSc1	království
Toro	Toro	k?	Toro
a	a	k8xC	a
území	území	k1gNnSc6	území
Busoga	Busog	k1gMnSc2	Busog
<g/>
)	)	kIx)	)
omezenou	omezený	k2eAgFnSc4d1	omezená
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
10	[number]	k4	10
distriktů	distrikt	k1gInPc2	distrikt
(	(	kIx(	(
<g/>
Acholi	Achole	k1gFnSc4	Achole
<g/>
,	,	kIx,	,
Bukedi	Buked	k1gMnPc1	Buked
<g/>
,	,	kIx,	,
Bugisu	Bugis	k1gInSc2	Bugis
<g/>
,	,	kIx,	,
Karamoja	Karamoj	k1gInSc2	Karamoj
<g/>
,	,	kIx,	,
Kigezi	Kigeze	k1gFnSc4	Kigeze
<g/>
,	,	kIx,	,
Lango	Langa	k1gFnSc5	Langa
<g/>
,	,	kIx,	,
Madi	Mad	k1gFnSc5	Mad
<g/>
,	,	kIx,	,
Sebei	Sebe	k1gFnSc5	Sebe
<g/>
,	,	kIx,	,
Teso	Teso	k1gMnSc1	Teso
<g/>
,	,	kIx,	,
Západní	západní	k2eAgInSc1d1	západní
Nil	Nil	k1gInSc1	Nil
<g/>
)	)	kIx)	)
a	a	k8xC	a
teritorium	teritorium	k1gNnSc4	teritorium
Mbale	Mbale	k1gFnSc2	Mbale
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
autonomii	autonomie	k1gFnSc4	autonomie
neměly	mít	k5eNaImAgInP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
první	první	k4xOgInSc1	první
ugandský	ugandský	k2eAgMnSc1d1	ugandský
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Apollo	Apollo	k1gMnSc1	Apollo
Milton	Milton	k1gInSc1	Milton
Obote	Obot	k1gInSc5	Obot
provedl	provést	k5eAaPmAgInS	provést
státní	státní	k2eAgInSc4d1	státní
převrat	převrat	k1gInSc4	převrat
<g/>
,	,	kIx,	,
zrušil	zrušit	k5eAaPmAgMnS	zrušit
ústavu	ústava	k1gFnSc4	ústava
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
se	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
převratů	převrat	k1gInPc2	převrat
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
probíhaly	probíhat	k5eAaImAgFnP	probíhat
až	až	k8xS	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1966	[number]	k4	1966
pak	pak	k6eAd1	pak
tato	tento	k3xDgFnSc1	tento
krize	krize	k1gFnSc1	krize
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
krvavým	krvavý	k2eAgInSc7d1	krvavý
útokem	útok	k1gInSc7	útok
ugandské	ugandský	k2eAgFnSc2d1	ugandská
armády	armáda	k1gFnSc2	armáda
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
generála	generál	k1gMnSc2	generál
Idiho	Idi	k1gMnSc2	Idi
Amina	Amin	k1gMnSc2	Amin
na	na	k7c4	na
palác	palác	k1gInSc4	palác
bugandského	bugandský	k2eAgMnSc2d1	bugandský
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
se	se	k3xPyFc4	se
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
uniknout	uniknout	k5eAaPmF	uniknout
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
za	za	k7c2	za
podezřelých	podezřelý	k2eAgFnPc2d1	podezřelá
okolností	okolnost	k1gFnPc2	okolnost
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
útoku	útok	k1gInSc3	útok
dal	dát	k5eAaPmAgMnS	dát
pokyn	pokyn	k1gInSc4	pokyn
sám	sám	k3xTgMnSc1	sám
Obote	Obot	k1gInSc5	Obot
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zbavit	zbavit	k5eAaPmF	zbavit
svých	svůj	k3xOyFgMnPc2	svůj
politických	politický	k2eAgMnPc2d1	politický
soupeřů	soupeř	k1gMnPc2	soupeř
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
byl	být	k5eAaImAgInS	být
i	i	k9	i
bugandský	bugandský	k2eAgMnSc1d1	bugandský
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1967	[number]	k4	1967
pak	pak	k9	pak
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
nová	nový	k2eAgFnSc1d1	nová
ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
rušila	rušit	k5eAaImAgFnS	rušit
království	království	k1gNnSc4	království
a	a	k8xC	a
Ugandu	Uganda	k1gFnSc4	Uganda
přeměnila	přeměnit	k5eAaPmAgFnS	přeměnit
v	v	k7c4	v
centralisticky	centralisticky	k6eAd1	centralisticky
řízený	řízený	k2eAgInSc4d1	řízený
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Bývalá	bývalý	k2eAgNnPc1d1	bývalé
království	království	k1gNnPc1	království
byla	být	k5eAaImAgNnP	být
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Bugandy	Buganda	k1gFnSc2	Buganda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
území	území	k1gNnSc4	území
i	i	k9	i
nadále	nadále	k6eAd1	nadále
tvořilo	tvořit	k5eAaImAgNnS	tvořit
provincii	provincie	k1gFnSc4	provincie
Bugandu	Bugand	k1gInSc2	Bugand
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
distrikty	distrikt	k1gInPc7	distrikt
<g/>
)	)	kIx)	)
přeměněna	přeměnit	k5eAaPmNgFnS	přeměnit
v	v	k7c4	v
pouhé	pouhý	k2eAgInPc4d1	pouhý
distrikty	distrikt	k1gInPc4	distrikt
centralizované	centralizovaný	k2eAgFnSc2d1	centralizovaná
Ugandy	Uganda	k1gFnSc2	Uganda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1971	[number]	k4	1971
převzal	převzít	k5eAaPmAgInS	převzít
Idi	Idi	k1gFnSc2	Idi
Amin	amin	k1gInSc1	amin
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
vládl	vládnout	k5eAaImAgInS	vládnout
následující	následující	k2eAgNnSc4d1	následující
desetiletí	desetiletí	k1gNnSc4	desetiletí
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Idiho	Idi	k1gMnSc2	Idi
Amina	Amin	k1gMnSc2	Amin
bylo	být	k5eAaImAgNnS	být
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
300	[number]	k4	300
000	[number]	k4	000
Uganďanů	Uganďan	k1gMnPc2	Uganďan
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vyhnání	vyhnání	k1gNnSc3	vyhnání
podnikavé	podnikavý	k2eAgFnSc2d1	podnikavá
indické	indický	k2eAgFnSc2d1	indická
menšiny	menšina	k1gFnSc2	menšina
a	a	k8xC	a
rozvratu	rozvrat	k1gInSc2	rozvrat
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
skončila	skončit	k5eAaPmAgFnS	skončit
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1979	[number]	k4	1979
po	po	k7c6	po
invazi	invaze	k1gFnSc6	invaze
tanzanských	tanzanský	k2eAgNnPc2d1	tanzanské
vojsk	vojsko	k1gNnPc2	vojsko
podporovaných	podporovaný	k2eAgInPc2d1	podporovaný
ugandským	ugandský	k2eAgInSc7d1	ugandský
exilem	exil	k1gInSc7	exil
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
mírně	mírně	k6eAd1	mírně
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
Apolla	Apollo	k1gNnSc2	Apollo
Miltona	Milton	k1gMnSc2	Milton
Oboteho	Obote	k1gMnSc2	Obote
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
opětovně	opětovně	k6eAd1	opětovně
svržen	svrhnout	k5eAaPmNgMnS	svrhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současný	současný	k2eAgMnSc1d1	současný
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
Yoweri	Yower	k1gMnPc1	Yower
Museveni	Museven	k2eAgMnPc1d1	Museven
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
moci	moc	k1gFnSc2	moc
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
generaci	generace	k1gFnSc3	generace
afrických	africký	k2eAgMnPc2d1	africký
vůdců	vůdce	k1gMnPc2	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
kritizován	kritizován	k2eAgInSc1d1	kritizován
za	za	k7c4	za
návrh	návrh	k1gInSc4	návrh
ústavní	ústavní	k2eAgFnSc2d1	ústavní
změny	změna	k1gFnSc2	změna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
umožnila	umožnit	k5eAaPmAgFnS	umožnit
ucházet	ucházet	k5eAaImF	ucházet
se	se	k3xPyFc4	se
potřetí	potřetí	k4xO	potřetí
o	o	k7c4	o
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
post	post	k1gInSc4	post
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
došlo	dojít	k5eAaPmAgNnS	dojít
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
k	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
nové	nový	k2eAgFnSc2d1	nová
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožnila	umožnit	k5eAaPmAgFnS	umožnit
znovunastolení	znovunastolení	k1gNnSc3	znovunastolení
tradičních	tradiční	k2eAgMnPc2d1	tradiční
vůdců	vůdce	k1gMnPc2	vůdce
a	a	k8xC	a
obnovu	obnova	k1gFnSc4	obnova
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obnově	obnova	k1gFnSc3	obnova
království	království	k1gNnSc2	království
Ankole	Ankole	k1gFnSc2	Ankole
<g/>
,	,	kIx,	,
Buganda	Buganda	k1gFnSc1	Buganda
<g/>
,	,	kIx,	,
Buňoro-Kitara	Buňoro-Kitara	k1gFnSc1	Buňoro-Kitara
<g/>
,	,	kIx,	,
Busoga	Busoga	k1gFnSc1	Busoga
a	a	k8xC	a
Toro	Toro	k?	Toro
<g/>
.	.	kIx.	.
</s>
<s>
Obnovená	obnovený	k2eAgNnPc4d1	obnovené
království	království	k1gNnPc4	království
však	však	k9	však
nyní	nyní	k6eAd1	nyní
mají	mít	k5eAaImIp3nP	mít
už	už	k6eAd1	už
jen	jen	k9	jen
spíše	spíše	k9	spíše
obřadní	obřadní	k2eAgFnSc4d1	obřadní
funkci	funkce	k1gFnSc4	funkce
bez	bez	k7c2	bez
skutečné	skutečný	k2eAgFnSc2d1	skutečná
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
pak	pak	k6eAd1	pak
ugandská	ugandský	k2eAgFnSc1d1	ugandská
vláda	vláda	k1gFnSc1	vláda
uznala	uznat	k5eAaPmAgFnS	uznat
existenci	existence	k1gFnSc4	existence
dalšího	další	k2eAgNnSc2d1	další
království	království	k1gNnSc2	království
-	-	kIx~	-
Rwenzururu	Rwenzurur	k1gInSc2	Rwenzurur
<g/>
,	,	kIx,	,
vyhlášeného	vyhlášený	k2eAgInSc2d1	vyhlášený
na	na	k7c6	na
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
tvořilo	tvořit	k5eAaImAgNnS	tvořit
západní	západní	k2eAgFnSc3d1	západní
části	část	k1gFnSc3	část
Tora	Tor	k1gMnSc2	Tor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
řada	řada	k1gFnSc1	řada
organizací	organizace	k1gFnPc2	organizace
požaduje	požadovat	k5eAaImIp3nS	požadovat
přeměnu	přeměna	k1gFnSc4	přeměna
Ugandy	Uganda	k1gFnSc2	Uganda
ve	v	k7c6	v
federaci	federace	k1gFnSc6	federace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
tato	tento	k3xDgNnPc4	tento
království	království	k1gNnPc4	království
i	i	k8xC	i
ostatní	ostatní	k2eAgInPc4d1	ostatní
regiony	region	k1gInPc4	region
(	(	kIx(	(
<g/>
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
historické	historický	k2eAgInPc4d1	historický
distrikty	distrikt	k1gInPc4	distrikt
z	z	k7c2	z
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
postavení	postavení	k1gNnSc2	postavení
spolkových	spolkový	k2eAgInPc2d1	spolkový
států	stát	k1gInPc2	stát
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
stupněm	stupeň	k1gInSc7	stupeň
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
autonomie	autonomie	k1gFnSc2	autonomie
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
v	v	k7c6	v
letech	let	k1gInPc6	let
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1967	[number]	k4	1967
nefungovalo	fungovat	k5eNaImAgNnS	fungovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
probíhal	probíhat	k5eAaImAgInS	probíhat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
občanský	občanský	k2eAgInSc1d1	občanský
konflikt	konflikt	k1gInSc1	konflikt
vedený	vedený	k2eAgInSc1d1	vedený
Boží	boží	k2eAgFnSc7d1	boží
armádou	armáda	k1gFnSc7	armáda
odporu	odpor	k1gInSc2	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
konfliktu	konflikt	k1gInSc2	konflikt
bylo	být	k5eAaImAgNnS	být
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
domovů	domov	k1gInPc2	domov
uneseno	unést	k5eAaPmNgNnS	unést
mnoho	mnoho	k4c1	mnoho
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
násilím	násilí	k1gNnSc7	násilí
nuceny	nucen	k2eAgFnPc1d1	nucena
bojovat	bojovat	k5eAaImF	bojovat
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Armády	armáda	k1gFnSc2	armáda
Božího	boží	k2eAgInSc2d1	boží
Odporu	odpor	k1gInSc2	odpor
<g/>
,	,	kIx,	,
vedené	vedený	k2eAgFnSc2d1	vedená
Josephem	Joseph	k1gInSc7	Joseph
Konym	Konym	k1gInSc1	Konym
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
probíhají	probíhat	k5eAaImIp3nP	probíhat
mírová	mírový	k2eAgNnPc4d1	Mírové
jednání	jednání	k1gNnPc4	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
konflikt	konflikt	k1gInSc1	konflikt
si	se	k3xPyFc3	se
vyžádal	vyžádat	k5eAaPmAgInS	vyžádat
mnoho	mnoho	k4c4	mnoho
lidských	lidský	k2eAgInPc2d1	lidský
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
Uganda	Uganda	k1gFnSc1	Uganda
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
několika	několik	k4yIc3	několik
velkým	velký	k2eAgFnPc3d1	velká
vodním	vodní	k2eAgFnPc3d1	vodní
plochám	plocha	k1gFnPc3	plocha
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Viktoriino	Viktoriin	k2eAgNnSc1d1	Viktoriino
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
Albertovo	Albertův	k2eAgNnSc1d1	Albertovo
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
jezero	jezero	k1gNnSc1	jezero
Kyoga	Kyog	k1gMnSc2	Kyog
a	a	k8xC	a
Edwardovo	Edwardův	k2eAgNnSc1d1	Edwardovo
jezero	jezero	k1gNnSc1	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Uganda	Uganda	k1gFnSc1	Uganda
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
plošině	plošina	k1gFnSc6	plošina
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
okolo	okolo	k7c2	okolo
900	[number]	k4	900
m.	m.	k?	m.
Převažuje	převažovat	k5eAaImIp3nS	převažovat
zde	zde	k6eAd1	zde
tropické	tropický	k2eAgNnSc4d1	tropické
podnebí	podnebí	k1gNnSc4	podnebí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
země	zem	k1gFnSc2	zem
poněkud	poněkud	k6eAd1	poněkud
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
Ugandy	Uganda	k1gFnSc2	Uganda
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
některé	některý	k3yIgInPc1	některý
pobřežní	pobřežní	k2eAgInPc1d1	pobřežní
ostrovy	ostrov	k1gInPc1	ostrov
ve	v	k7c6	v
Viktoriině	Viktoriin	k2eAgNnSc6d1	Viktoriino
jezeře	jezero	k1gNnSc6	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgNnSc1d3	nejdůležitější
města	město	k1gNnPc1	město
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
u	u	k7c2	u
Viktoriina	Viktoriin	k2eAgNnSc2d1	Viktoriino
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Kampala	Kampal	k1gMnSc4	Kampal
i	i	k9	i
blízké	blízký	k2eAgNnSc1d1	blízké
město	město	k1gNnSc1	město
Entebbe	Entebb	k1gMnSc5	Entebb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
<s>
Uganda	Uganda	k1gFnSc1	Uganda
má	mít	k5eAaImIp3nS	mít
tropické	tropický	k2eAgNnSc4d1	tropické
podnebí	podnebí	k1gNnSc4	podnebí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
zmírňováno	zmírňovat	k5eAaImNgNnS	zmírňovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
většiny	většina	k1gFnSc2	většina
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
1000	[number]	k4	1000
–	–	k?	–
1500	[number]	k4	1500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uganda	Uganda	k1gFnSc1	Uganda
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
perla	perla	k1gFnSc1	perla
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Srážky	srážka	k1gFnPc1	srážka
postupně	postupně	k6eAd1	postupně
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
k	k	k7c3	k
severu	sever	k1gInSc3	sever
ubývají	ubývat	k5eAaImIp3nP	ubývat
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
kolem	kolem	k7c2	kolem
Viktoriina	Viktoriin	k2eAgNnSc2d1	Viktoriino
jezera	jezero	k1gNnSc2	jezero
má	mít	k5eAaImIp3nS	mít
klima	klima	k1gNnSc4	klima
tropického	tropický	k2eAgInSc2d1	tropický
deštného	deštný	k2eAgInSc2d1	deštný
pralesa	prales	k1gInSc2	prales
s	s	k7c7	s
celoročními	celoroční	k2eAgFnPc7d1	celoroční
srážkami	srážka	k1gFnPc7	srážka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
při	při	k7c6	při
postupu	postup	k1gInSc6	postup
k	k	k7c3	k
severu	sever	k1gInSc3	sever
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
objevuje	objevovat	k5eAaImIp3nS	objevovat
období	období	k1gNnSc4	období
sucha	sucho	k1gNnSc2	sucho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
má	mít	k5eAaImIp3nS	mít
země	země	k1gFnSc1	země
novou	nový	k2eAgFnSc4d1	nová
ústavu	ústava	k1gFnSc4	ústava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
je	být	k5eAaImIp3nS	být
zakotvena	zakotven	k2eAgFnSc1d1	zakotvena
existence	existence	k1gFnSc1	existence
jednokomorového	jednokomorový	k2eAgInSc2d1	jednokomorový
Národního	národní	k2eAgInSc2d1	národní
parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
National	National	k1gMnSc1	National
Parliament	Parliament	k1gMnSc1	Parliament
<g/>
)	)	kIx)	)
s	s	k7c7	s
303	[number]	k4	303
poslanci	poslanec	k1gMnPc1	poslanec
s	s	k7c7	s
pětiletým	pětiletý	k2eAgNnSc7d1	pětileté
funkčním	funkční	k2eAgNnSc7d1	funkční
obdobím	období	k1gNnSc7	období
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
81	[number]	k4	81
nominují	nominovat	k5eAaBmIp3nP	nominovat
různé	různý	k2eAgFnPc1d1	různá
zájmové	zájmový	k2eAgFnPc1d1	zájmová
skupiny	skupina	k1gFnPc1	skupina
včetně	včetně	k7c2	včetně
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
ugandské	ugandský	k2eAgFnSc2d1	ugandská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
(	(	kIx(	(
<g/>
214	[number]	k4	214
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
voleno	volit	k5eAaImNgNnS	volit
přímo	přímo	k6eAd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
ústavy	ústava	k1gFnSc2	ústava
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
omezen	omezit	k5eAaPmNgInS	omezit
vliv	vliv	k1gInSc4	vliv
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
posílen	posílit	k5eAaPmNgInS	posílit
vliv	vliv	k1gInSc1	vliv
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
i	i	k8xC	i
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
předsedu	předseda	k1gMnSc4	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
plnění	plnění	k1gNnSc6	plnění
jeho	jeho	k3xOp3gInPc2	jeho
úkolů	úkol	k1gInPc2	úkol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
omezit	omezit	k5eAaPmF	omezit
sektářské	sektářský	k2eAgNnSc4d1	sektářské
násilí	násilí	k1gNnSc4	násilí
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
zavedl	zavést	k5eAaPmAgMnS	zavést
současný	současný	k2eAgMnSc1d1	současný
prezident	prezident	k1gMnSc1	prezident
Yoweri	Yower	k1gFnSc2	Yower
Museveni	Museven	k2eAgMnPc1d1	Museven
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
nástupu	nástup	k1gInSc6	nástup
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
zákaz	zákaz	k1gInSc1	zákaz
jejich	jejich	k3xOp3gFnPc2	jejich
politických	politický	k2eAgFnPc2d1	politická
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
sice	sice	k8xC	sice
i	i	k9	i
nadále	nadále	k6eAd1	nadále
existovaly	existovat	k5eAaImAgFnP	existovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nesměly	smět	k5eNaImAgInP	smět
kandidovat	kandidovat	k5eAaImF	kandidovat
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
volily	volit	k5eAaImAgFnP	volit
pouze	pouze	k6eAd1	pouze
osobnosti	osobnost	k1gFnPc4	osobnost
(	(	kIx(	(
<g/>
i	i	k8xC	i
když	když	k8xS	když
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
členy	člen	k1gInPc7	člen
některé	některý	k3yIgFnSc2	některý
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
konaném	konaný	k2eAgNnSc6d1	konané
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
se	se	k3xPyFc4	se
však	však	k9	však
hlasující	hlasující	k1gMnPc1	hlasující
vyslovili	vyslovit	k5eAaPmAgMnP	vyslovit
pro	pro	k7c4	pro
zrušení	zrušení	k1gNnSc4	zrušení
tohoto	tento	k3xDgInSc2	tento
19	[number]	k4	19
<g/>
letého	letý	k2eAgInSc2d1	letý
zákazu	zákaz	k1gInSc2	zákaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řada	řada	k1gFnSc1	řada
organizací	organizace	k1gFnPc2	organizace
požaduje	požadovat	k5eAaImIp3nS	požadovat
přeměnu	přeměna	k1gFnSc4	přeměna
Ugandy	Uganda	k1gFnSc2	Uganda
ve	v	k7c6	v
federaci	federace	k1gFnSc6	federace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc1	přehled
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
představitelů	představitel	k1gMnPc2	představitel
===	===	k?	===
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1962	[number]	k4	1962
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1963	[number]	k4	1963
–	–	k?	–
Sir	sir	k1gMnSc1	sir
Walter	Walter	k1gMnSc1	Walter
Fleming	Fleming	k1gInSc1	Fleming
Coutts	Coutts	k1gInSc4	Coutts
–	–	k?	–
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
reprezentující	reprezentující	k2eAgFnSc4d1	reprezentující
britskou	britský	k2eAgFnSc4d1	britská
monarchii	monarchie	k1gFnSc4	monarchie
jako	jako	k8xC	jako
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1963	[number]	k4	1963
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1966	[number]	k4	1966
–	–	k?	–
Sir	sir	k1gMnSc1	sir
Edward	Edward	k1gMnSc1	Edward
Mutebi	Muteb	k1gFnSc2	Muteb
Mutesa	Mutesa	k1gFnSc1	Mutesa
II	II	kA	II
<g/>
.	.	kIx.	.
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
KY	KY	kA	KY
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1966	[number]	k4	1966
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1971	[number]	k4	1971
–	–	k?	–
Apolo	Apolo	k1gNnSc1	Apolo
Milton	Milton	k1gInSc1	Milton
Obote	Obot	k1gInSc5	Obot
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
UPC	UPC	kA	UPC
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1971	[number]	k4	1971
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1971	[number]	k4	1971
–	–	k?	–
Idi	Idi	k1gMnSc1	Idi
Amin	amin	k1gInSc4	amin
Dada	dada	k1gNnSc2	dada
Oumee	Oume	k1gInSc2	Oume
–	–	k?	–
vojenská	vojenský	k2eAgFnSc1d1	vojenská
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1971	[number]	k4	1971
-	-	kIx~	-
13	[number]	k4	13
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
–	–	k?	–
Idi	Idi	k1gMnSc1	Idi
Amin	amin	k1gInSc4	amin
Dada	dada	k1gNnSc2	dada
Oumee	Oume	k1gFnSc2	Oume
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
–	–	k?	–
Yusufu	Yusuf	k1gInSc2	Yusuf
Kironde	Kirond	k1gInSc5	Kirond
Lule	lula	k1gFnSc3	lula
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
UNLF	UNLF	kA	UNLF
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
-	-	kIx~	-
11	[number]	k4	11
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
–	–	k?	–
Godfrey	Godfrea	k1gMnSc2	Godfrea
Lukongwa	Lukongwus	k1gMnSc2	Lukongwus
Binaisa	Binais	k1gMnSc2	Binais
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
UNLF	UNLF	kA	UNLF
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
–	–	k?	–
Paulo	Paula	k1gFnSc5	Paula
Muwanga	Muwang	k1gMnSc4	Muwang
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
vojenské	vojenský	k2eAgFnSc2d1	vojenská
komise	komise	k1gFnSc2	komise
<g/>
;	;	kIx,	;
UNLF	UNLF	kA	UNLF
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
–	–	k?	–
Saulo	Saulo	k1gNnSc1	Saulo
Musoke	Musoke	k1gFnSc1	Musoke
<g/>
,	,	kIx,	,
Polycarp	Polycarp	k1gInSc1	Polycarp
Nyamuchoncho	Nyamuchoncha	k1gFnSc5	Nyamuchoncha
<g/>
,	,	kIx,	,
Yoweri	Yoweri	k1gNnSc1	Yoweri
Hunter	Huntra	k1gFnPc2	Huntra
Wacha-Olwol	Wacha-Olwola	k1gFnPc2	Wacha-Olwola
–	–	k?	–
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
komise	komise	k1gFnSc1	komise
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
-	-	kIx~	-
27	[number]	k4	27
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1985	[number]	k4	1985
–	–	k?	–
Apolo	Apolo	k1gNnSc1	Apolo
Milton	Milton	k1gInSc1	Milton
Obote	Obot	k1gInSc5	Obot
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
UPC	UPC	kA	UPC
</s>
</p>
<p>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1985	[number]	k4	1985
-	-	kIx~	-
29	[number]	k4	29
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1985	[number]	k4	1985
–	–	k?	–
Basilio	Basilio	k1gNnSc1	Basilio
Olara	Olara	k1gFnSc1	Olara
Okello	Okello	k1gNnSc1	Okello
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Vojenské	vojenský	k2eAgFnSc2d1	vojenská
rady	rada	k1gFnSc2	rada
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1985	[number]	k4	1985
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1986	[number]	k4	1986
–	–	k?	–
Tito	tento	k3xDgMnPc1	tento
Lutwa	Lutwa	k1gMnSc1	Lutwa
Okello	Okello	k1gNnSc1	Okello
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Vojenské	vojenský	k2eAgFnSc2d1	vojenská
rady	rada	k1gFnSc2	rada
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
</s>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1986	[number]	k4	1986
-	-	kIx~	-
29	[number]	k4	29
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1986	[number]	k4	1986
–	–	k?	–
Yoweri	Yoweri	k1gNnSc2	Yoweri
Kaguta	Kagut	k1gMnSc2	Kagut
Museveni	Museven	k2eAgMnPc1d1	Museven
–	–	k?	–
velitel	velitel	k1gMnSc1	velitel
Národní	národní	k2eAgFnSc2d1	národní
armády	armáda	k1gFnSc2	armáda
odporu	odpor	k1gInSc2	odpor
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
<g/>
/	/	kIx~	/
<g/>
NMR	NMR	kA	NMR
</s>
</p>
<p>
<s>
od	od	k7c2	od
29	[number]	k4	29
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1986	[number]	k4	1986
–	–	k?	–
Yoweri	Yoweri	k1gNnSc2	Yoweri
Kaguta	Kagut	k1gMnSc2	Kagut
Museveni	Museven	k2eAgMnPc1d1	Museven
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
NMR	NMR	kA	NMR
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc4d1	administrativní
členění	členění	k1gNnSc4	členění
===	===	k?	===
</s>
</p>
<p>
<s>
Uganda	Uganda	k1gFnSc1	Uganda
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
127	[number]	k4	127
distriktů	distrikt	k1gInPc2	distrikt
<g/>
,	,	kIx,	,
formálně	formálně	k6eAd1	formálně
seskupených	seskupený	k2eAgInPc2d1	seskupený
do	do	k7c2	do
4	[number]	k4	4
oblasti	oblast	k1gFnSc2	oblast
<g/>
:	:	kIx,	:
Centrální	centrální	k2eAgFnSc2d1	centrální
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc2d1	severní
<g/>
,	,	kIx,	,
Východní	východní	k2eAgFnSc2d1	východní
a	a	k8xC	a
Západní	západní	k2eAgFnSc2d1	západní
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
statut	statut	k1gInSc4	statut
<g/>
.	.	kIx.	.
</s>
<s>
Distrikty	distrikt	k1gInPc1	distrikt
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
pojmenované	pojmenovaný	k2eAgInPc1d1	pojmenovaný
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
správního	správní	k2eAgNnSc2d1	správní
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Významná	významný	k2eAgNnPc1d1	významné
města	město	k1gNnPc1	město
===	===	k?	===
</s>
</p>
<p>
<s>
Kampala	Kampat	k5eAaImAgFnS	Kampat
-	-	kIx~	-
hlavní	hlavní	k2eAgFnSc1d1	hlavní
-	-	kIx~	-
954	[number]	k4	954
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
Džindža	Džindža	k1gFnSc1	Džindža
-	-	kIx~	-
61	[number]	k4	61
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
Mbale	Mbale	k1gFnSc1	Mbale
-	-	kIx~	-
54	[number]	k4	54
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
Masaka	Masak	k1gMnSc4	Masak
-	-	kIx~	-
49	[number]	k4	49
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
HDP	HDP	kA	HDP
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
mírně	mírně	k6eAd1	mírně
rostl	růst	k5eAaImAgInS	růst
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
k	k	k7c3	k
400	[number]	k4	400
USD	USD	kA	USD
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
však	však	k9	však
země	země	k1gFnSc1	země
velice	velice	k6eAd1	velice
chudá	chudý	k2eAgFnSc1d1	chudá
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomice	ekonomika	k1gFnSc3	ekonomika
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
stabilní	stabilní	k2eAgFnSc1d1	stabilní
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
a	a	k8xC	a
Uganďané	Uganďaná	k1gFnPc1	Uganďaná
žijící	žijící	k2eAgFnPc1d1	žijící
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
příbuzným	příbuzný	k1gMnSc7	příbuzný
posílají	posílat	k5eAaImIp3nP	posílat
peníze	peníz	k1gInPc4	peníz
(	(	kIx(	(
<g/>
remitence	remitence	k1gFnPc4	remitence
<g/>
)	)	kIx)	)
a	a	k8xC	a
investují	investovat	k5eAaBmIp3nP	investovat
do	do	k7c2	do
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
však	však	k9	však
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
epidemie	epidemie	k1gFnSc1	epidemie
AIDS	AIDS	kA	AIDS
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
Ugandě	Uganda	k1gFnSc6	Uganda
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
nakaženo	nakazit	k5eAaPmNgNnS	nakazit
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
právě	právě	k9	právě
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
vyváží	vyvážet	k5eAaImIp3nS	vyvážet
kávu	káva	k1gFnSc4	káva
a	a	k8xC	a
dováží	dovázat	k5eAaPmIp3nP	dovázat
potraviny	potravina	k1gFnPc4	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc7d1	místní
měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
ugandský	ugandský	k2eAgInSc1d1	ugandský
šilink	šilink	k1gInSc1	šilink
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Uganda	Uganda	k1gFnSc1	Uganda
je	být	k5eAaImIp3nS	být
vlastí	vlast	k1gFnSc7	vlast
mnoha	mnoho	k4c2	mnoho
různých	různý	k2eAgFnPc2d1	různá
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
žádná	žádný	k3yNgFnSc1	žádný
nemá	mít	k5eNaImIp3nS	mít
většinu	většina	k1gFnSc4	většina
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnější	početní	k2eAgMnSc1d3	nejpočetnější
jsou	být	k5eAaImIp3nP	být
kmeny	kmen	k1gInPc1	kmen
Ganda	Gando	k1gNnSc2	Gando
a	a	k8xC	a
Karamodžong	Karamodžonga	k1gFnPc2	Karamodžonga
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
různými	různý	k2eAgInPc7d1	různý
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osamostatnění	osamostatnění	k1gNnSc6	osamostatnění
Ugandy	Uganda	k1gFnSc2	Uganda
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
angličtina	angličtina	k1gFnSc1	angličtina
úředním	úřední	k2eAgMnSc7d1	úřední
jazykem	jazyk	k1gMnSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jazykem	jazyk	k1gInSc7	jazyk
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
mluvčích	mluvčí	k1gMnPc2	mluvčí
je	být	k5eAaImIp3nS	být
luganda	luganda	k1gFnSc1	luganda
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
v	v	k7c6	v
Bugandě	Buganda	k1gFnSc6	Buganda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
oblastech	oblast	k1gFnPc6	oblast
Ugandy	Uganda	k1gFnSc2	Uganda
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
místními	místní	k2eAgInPc7d1	místní
kmenovými	kmenový	k2eAgInPc7d1	kmenový
jazyky	jazyk	k1gInPc7	jazyk
(	(	kIx(	(
<g/>
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Ugandy	Uganda	k1gFnSc2	Uganda
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
např.	např.	kA	např.
jazyk	jazyk	k1gInSc1	jazyk
Lacholi	Lachole	k1gFnSc3	Lachole
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
ke	k	k7c3	k
kmenu	kmen	k1gInSc6	kmen
Acholi	Achole	k1gFnSc3	Achole
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
africké	africký	k2eAgInPc4d1	africký
poměry	poměr	k1gInPc4	poměr
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
gramotnost	gramotnost	k1gFnSc1	gramotnost
poměrně	poměrně	k6eAd1	poměrně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
-	-	kIx~	-
69,9	[number]	k4	69,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ugandě	Uganda	k1gFnSc6	Uganda
je	být	k5eAaImIp3nS	být
povinné	povinný	k2eAgNnSc4d1	povinné
školné	školné	k1gNnSc4	školné
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
chudým	chudý	k2eAgMnPc3d1	chudý
Uganďanům	Uganďan	k1gMnPc3	Uganďan
zamezuje	zamezovat	k5eAaImIp3nS	zamezovat
v	v	k7c6	v
přístupu	přístup	k1gInSc6	přístup
ke	k	k7c3	k
vzdělání	vzdělání	k1gNnSc3	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgInSc1d1	střední
věk	věk	k1gInSc1	věk
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
14	[number]	k4	14
let	léto	k1gNnPc2	léto
až	až	k6eAd1	až
11	[number]	k4	11
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
Uganda	Uganda	k1gFnSc1	Uganda
je	být	k5eAaImIp3nS	být
země	zem	k1gFnPc4	zem
s	s	k7c7	s
nejmladší	mladý	k2eAgFnSc7d3	nejmladší
populací	populace	k1gFnSc7	populace
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
49,4	[number]	k4	49,4
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
mladší	mladý	k2eAgMnSc1d2	mladší
čtrnácti	čtrnáct	k4xCc2	čtrnáct
let	léto	k1gNnPc2	léto
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
kategorii	kategorie	k1gFnSc6	kategorie
je	být	k5eAaImIp3nS	být
Uganda	Uganda	k1gFnSc1	Uganda
první	první	k4xOgFnSc1	první
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgNnSc4d3	nejčastější
onemocnění	onemocnění	k1gNnSc4	onemocnění
vyskytující	vyskytující	k2eAgFnSc2d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
Ugandě	Uganda	k1gFnSc6	Uganda
patří	patřit	k5eAaImIp3nS	patřit
malárie	malárie	k1gFnSc1	malárie
<g/>
,	,	kIx,	,
HIV	HIV	kA	HIV
<g/>
/	/	kIx~	/
<g/>
AIDS	AIDS	kA	AIDS
<g/>
,	,	kIx,	,
TBC	TBC	kA	TBC
<g/>
,	,	kIx,	,
hepatitida	hepatitida	k1gFnSc1	hepatitida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
a	a	k8xC	a
Islám	islám	k1gInSc1	islám
se	se	k3xPyFc4	se
v	v	k7c6	v
Ugandě	Uganda	k1gFnSc6	Uganda
začaly	začít	k5eAaPmAgFnP	začít
šířit	šířit	k5eAaImF	šířit
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sem	sem	k6eAd1	sem
poprvé	poprvé	k6eAd1	poprvé
dorazili	dorazit	k5eAaPmAgMnP	dorazit
misionáři	misionář	k1gMnPc1	misionář
obou	dva	k4xCgNnPc2	dva
vyznání	vyznání	k1gNnPc2	vyznání
<g/>
,	,	kIx,	,
snažící	snažící	k2eAgFnSc4d1	snažící
se	se	k3xPyFc4	se
vnutit	vnutit	k5eAaPmF	vnutit
své	svůj	k3xOyFgNnSc4	svůj
náboženství	náboženství	k1gNnSc4	náboženství
bugandskému	bugandský	k2eAgMnSc3d1	bugandský
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
vypadá	vypadat	k5eAaImIp3nS	vypadat
náboženské	náboženský	k2eAgNnSc4d1	náboženské
složení	složení	k1gNnSc4	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
33	[number]	k4	33
<g/>
%	%	kIx~	%
katolíci	katolík	k1gMnPc1	katolík
<g/>
,	,	kIx,	,
a	a	k8xC	a
33	[number]	k4	33
<g/>
%	%	kIx~	%
protestanti	protestant	k1gMnPc1	protestant
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
%	%	kIx~	%
muslimové	muslim	k1gMnPc1	muslim
a	a	k8xC	a
18	[number]	k4	18
<g/>
%	%	kIx~	%
různé	různý	k2eAgInPc1d1	různý
domorodé	domorodý	k2eAgInPc1d1	domorodý
víry	vír	k1gInPc1	vír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ugandě	Uganda	k1gFnSc6	Uganda
<g/>
,	,	kIx,	,
i	i	k9	i
přes	přes	k7c4	přes
různé	různý	k2eAgNnSc4d1	různé
vyznání	vyznání	k1gNnSc4	vyznání
jejích	její	k3xOp3gMnPc2	její
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
nedochází	docházet	k5eNaImIp3nS	docházet
ke	k	k7c3	k
konfliktům	konflikt	k1gInPc3	konflikt
s	s	k7c7	s
náboženským	náboženský	k2eAgInSc7d1	náboženský
kontextem	kontext	k1gInSc7	kontext
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Uganda	Uganda	k1gFnSc1	Uganda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Uganda	Uganda	k1gFnSc1	Uganda
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Uganda	Uganda	k1gFnSc1	Uganda
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Archivní	archivní	k2eAgFnPc1d1	archivní
mapy	mapa	k1gFnPc1	mapa
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Ugandy	Uganda	k1gFnSc2	Uganda
z	z	k7c2	z
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c6	na
webu	web	k1gInSc6	web
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Kinji	Kinje	k1gFnSc4	Kinje
Imanishi	Imanish	k1gFnSc2	Imanish
Digital	Digital	kA	Digital
Archive	archiv	k1gInSc5	archiv
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Country	country	k2eAgInSc1d1	country
of	of	k?	of
Origin	Origin	k2eAgInSc1d1	Origin
Information	Information	k1gInSc1	Information
Report	report	k1gInSc1	report
-	-	kIx~	-
Uganda	Uganda	k1gFnSc1	Uganda
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
UK	UK	kA	UK
Border	Border	k1gMnSc1	Border
Agency	Agenca	k1gMnSc2	Agenca
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
rev	rev	k?	rev
<g/>
.	.	kIx.	.
2011-04-26	[number]	k4	2011-04-26
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Uganda	Uganda	k1gFnSc1	Uganda
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Uganda	Uganda	k1gFnSc1	Uganda
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Uganda	Uganda	k1gFnSc1	Uganda
Country	country	k2eAgFnSc1d1	country
Report	report	k1gInSc1	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
African	African	k1gInSc1	African
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Uganda	Uganda	k1gFnSc1	Uganda
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-08-10	[number]	k4	2011-08-10
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Uganda	Uganda	k1gFnSc1	Uganda
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Nairobi	Nairobi	k1gNnSc6	Nairobi
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Uganda	Uganda	k1gFnSc1	Uganda
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2009-09-13	[number]	k4	2009-09-13
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
INGHAM	INGHAM	kA	INGHAM
<g/>
,	,	kIx,	,
Kenneth	Kenneth	k1gInSc1	Kenneth
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Uganda	Uganda	k1gFnSc1	Uganda
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
