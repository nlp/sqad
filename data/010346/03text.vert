<p>
<s>
Georg	Georg	k1gMnSc1	Georg
Maximilian	Maximilian	k1gMnSc1	Maximilian
kardinál	kardinál	k1gMnSc1	kardinál
Sterzinsky	Sterzinsky	k1gMnSc1	Sterzinsky
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1936	[number]	k4	1936
Warlack	Warlacka	k1gFnPc2	Warlacka
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Worławki	Worławki	k1gNnSc1	Worławki
<g/>
)	)	kIx)	)
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
římskokatolický	římskokatolický	k2eAgMnSc1d1	římskokatolický
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kněz	kněz	k1gMnSc1	kněz
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
na	na	k7c4	na
území	území	k1gNnSc4	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
jako	jako	k8xS	jako
devítiletý	devítiletý	k2eAgMnSc1d1	devítiletý
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
odsunut	odsunout	k5eAaPmNgMnS	odsunout
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
v	v	k7c6	v
semináři	seminář	k1gInSc6	seminář
v	v	k7c6	v
Erfurtu	Erfurt	k1gInSc6	Erfurt
<g/>
,	,	kIx,	,
kněžské	kněžský	k2eAgNnSc4d1	kněžské
svěcení	svěcení	k1gNnSc4	svěcení
přijal	přijmout	k5eAaPmAgInS	přijmout
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
inkardinován	inkardinovat	k5eAaPmNgMnS	inkardinovat
do	do	k7c2	do
diecéze	diecéze	k1gFnSc2	diecéze
Erfurt-Meiningen	Erfurt-Meiningen	k1gInSc1	Erfurt-Meiningen
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
zde	zde	k6eAd1	zde
jako	jako	k8xS	jako
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
přednášel	přednášet	k5eAaImAgMnS	přednášet
v	v	k7c6	v
semináři	seminář	k1gInSc6	seminář
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
prefektem	prefekt	k1gMnSc7	prefekt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1981	[number]	k4	1981
až	až	k9	až
1989	[number]	k4	1989
vykonával	vykonávat	k5eAaImAgInS	vykonávat
funkci	funkce	k1gFnSc4	funkce
generálního	generální	k2eAgMnSc2d1	generální
vikáře	vikář	k1gMnSc2	vikář
<g/>
.	.	kIx.	.
</s>
<s>
Vedl	vést	k5eAaImAgMnS	vést
mj.	mj.	kA	mj.
diecézní	diecézní	k2eAgFnSc4d1	diecézní
ekumenickou	ekumenický	k2eAgFnSc4d1	ekumenická
komisi	komise	k1gFnSc4	komise
a	a	k8xC	a
staral	starat	k5eAaImAgMnS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
diecézní	diecézní	k2eAgNnSc4d1	diecézní
vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biskup	biskup	k1gMnSc1	biskup
a	a	k8xC	a
kardinál	kardinál	k1gMnSc1	kardinál
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1989	[number]	k4	1989
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
pomocným	pomocný	k2eAgMnSc7d1	pomocný
berlínským	berlínský	k2eAgMnSc7d1	berlínský
biskupem	biskup	k1gMnSc7	biskup
<g/>
,	,	kIx,	,
biskupské	biskupský	k2eAgNnSc1d1	Biskupské
svěcení	svěcení	k1gNnSc1	svěcení
mu	on	k3xPp3gMnSc3	on
udělil	udělit	k5eAaPmAgMnS	udělit
biskup	biskup	k1gMnSc1	biskup
Joachim	Joachim	k1gMnSc1	Joachim
Wanke	Wanke	k1gFnSc1	Wanke
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1989	[number]	k4	1989
a	a	k8xC	a
1990	[number]	k4	1990
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
biskupské	biskupský	k2eAgFnSc2d1	biskupská
konference	konference	k1gFnSc2	konference
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
konzistoři	konzistoř	k1gFnSc6	konzistoř
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1991	[number]	k4	1991
ho	on	k3xPp3gMnSc4	on
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
kardinálem	kardinál	k1gMnSc7	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
povýšení	povýšení	k1gNnSc6	povýšení
Berlína	Berlín	k1gInSc2	Berlín
na	na	k7c6	na
metropoli	metropol	k1gFnSc6	metropol
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1994	[number]	k4	1994
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dovršení	dovršení	k1gNnSc6	dovršení
kanonického	kanonický	k2eAgInSc2d1	kanonický
věku	věk	k1gInSc2	věk
podal	podat	k5eAaPmAgMnS	podat
rezignaci	rezignace	k1gFnSc4	rezignace
na	na	k7c4	na
funkci	funkce	k1gFnSc4	funkce
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
přijal	přijmout	k5eAaPmAgInS	přijmout
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
hospitalizován	hospitalizován	k2eAgInSc1d1	hospitalizován
se	s	k7c7	s
zápalem	zápal	k1gInSc7	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
kardinál	kardinál	k1gMnSc1	kardinál
Rainer	Rainer	k1gMnSc1	Rainer
Maria	Maria	k1gFnSc1	Maria
Woelki	Woelk	k1gFnSc2	Woelk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Georg	Georg	k1gMnSc1	Georg
Sterzinsky	Sterzinsky	k1gFnSc7	Sterzinsky
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
v	v	k7c6	v
biografickém	biografický	k2eAgInSc6d1	biografický
slovníku	slovník	k1gInSc6	slovník
kardinálů	kardinál	k1gMnPc2	kardinál
Salvadora	Salvador	k1gMnSc4	Salvador
Mirandy	Miranda	k1gFnSc2	Miranda
</s>
</p>
<p>
<s>
Biografické	biografický	k2eAgNnSc1d1	biografické
heslo	heslo	k1gNnSc1	heslo
na	na	k7c4	na
catholic-hierarchy	catholicierarcha	k1gFnPc4	catholic-hierarcha
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
kardinálů	kardinál	k1gMnPc2	kardinál
jmenovaných	jmenovaný	k1gMnPc2	jmenovaný
Janem	Jan	k1gMnSc7	Jan
Pavlem	Pavel	k1gMnSc7	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
