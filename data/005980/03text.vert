<s>
Etiopské	etiopský	k2eAgNnSc1d1	etiopské
písmo	písmo	k1gNnSc1	písmo
(	(	kIx(	(
<g/>
ግ	ግ	k?	ግ
<g/>
,	,	kIx,	,
Ge	Ge	k1gFnSc1	Ge
<g/>
'	'	kIx"	'
<g/>
ez	ez	k?	ez
<g/>
)	)	kIx)	)
sloužilo	sloužit	k5eAaImAgNnS	sloužit
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
jihosemitského	jihosemitský	k2eAgInSc2d1	jihosemitský
jazyka	jazyk	k1gInSc2	jazyk
ge	ge	k?	ge
<g/>
'	'	kIx"	'
<g/>
ez	ez	k?	ez
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
jazykem	jazyk	k1gInSc7	jazyk
se	se	k3xPyFc4	se
mluvilo	mluvit	k5eAaImAgNnS	mluvit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
severní	severní	k2eAgFnSc2d1	severní
Etiopie	Etiopie	k1gFnSc2	Etiopie
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
království	království	k1gNnSc2	království
Aksum	Aksum	k1gNnSc4	Aksum
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
ge	ge	k?	ge
<g/>
'	'	kIx"	'
<g/>
ez	ez	k?	ez
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
liturgii	liturgie	k1gFnSc6	liturgie
některých	některý	k3yIgFnPc2	některý
etiopských	etiopský	k2eAgFnPc2d1	etiopská
a	a	k8xC	a
eritrejských	eritrejský	k2eAgFnPc2d1	eritrejská
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Etiopské	etiopský	k2eAgNnSc4d1	etiopské
písmo	písmo	k1gNnSc4	písmo
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
používá	používat	k5eAaImIp3nS	používat
hlavně	hlavně	k9	hlavně
jihosemitská	jihosemitský	k2eAgFnSc1d1	jihosemitský
amharština	amharština	k1gFnSc1	amharština
a	a	k8xC	a
tygrajština	tygrajština	k1gFnSc1	tygrajština
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ostatní	ostatní	k2eAgInPc1d1	ostatní
jazyky	jazyk	k1gInPc1	jazyk
etiopské	etiopský	k2eAgFnSc2d1	etiopská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
jazyky	jazyk	k1gInPc1	jazyk
však	však	k9	však
přecházejí	přecházet	k5eAaImIp3nP	přecházet
na	na	k7c4	na
latinku	latinka	k1gFnSc4	latinka
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
například	například	k6eAd1	například
oromština	oromština	k1gFnSc1	oromština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Písmo	písmo	k1gNnSc1	písmo
ge	ge	k?	ge
<g/>
'	'	kIx"	'
<g/>
ez	ez	k?	ez
je	být	k5eAaImIp3nS	být
odvozené	odvozený	k2eAgNnSc1d1	odvozené
z	z	k7c2	z
jihoarabské	jihoarabský	k2eAgFnSc2d1	jihoarabský
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
protosinajského	protosinajský	k2eAgNnSc2d1	protosinajský
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Jihoarabská	Jihoarabský	k2eAgFnSc1d1	Jihoarabská
abeceda	abeceda	k1gFnSc1	abeceda
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
způsob	způsob	k1gInSc1	způsob
zápisu	zápis	k1gInSc2	zápis
abdžád	abdžáda	k1gFnPc2	abdžáda
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jihoarabská	jihoarabský	k2eAgFnSc1d1	jihoarabský
abeceda	abeceda	k1gFnSc1	abeceda
nezaznamenával	zaznamenávat	k5eNaImAgMnS	zaznamenávat
samohlásky	samohláska	k1gFnSc2	samohláska
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
oproti	oproti	k7c3	oproti
ní	on	k3xPp3gFnSc3	on
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
(	(	kIx(	(
<g/>
p	p	k?	p
<g/>
̣	̣	k?	̣
<g/>
,	,	kIx,	,
p	p	k?	p
<g/>
)	)	kIx)	)
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
26	[number]	k4	26
souhlásek	souhláska	k1gFnPc2	souhláska
:	:	kIx,	:
h	h	k?	h
<g/>
,	,	kIx,	,
l	l	kA	l
<g/>
,	,	kIx,	,
ḥ	ḥ	k?	ḥ
<g/>
,	,	kIx,	,
m	m	kA	m
<g/>
,	,	kIx,	,
ś	ś	k?	ś
<g/>
,	,	kIx,	,
r	r	kA	r
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
s	s	k7c7	s
<g/>
,	,	kIx,	,
ḳ	ḳ	k?	ḳ
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
,	,	kIx,	,
ḫ	ḫ	k?	ḫ
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
,	,	kIx,	,
ʾ	ʾ	k?	ʾ
<g/>
,	,	kIx,	,
k	k	k7c3	k
<g/>
,	,	kIx,	,
w	w	k?	w
<g/>
,	,	kIx,	,
ʿ	ʿ	k?	ʿ
<g/>
,	,	kIx,	,
z	z	k0	z
<g/>
,	,	kIx,	,
j	j	k?	j
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
,	,	kIx,	,
ṭ	ṭ	k?	ṭ
<g/>
,	,	kIx,	,
p	p	k?	p
<g/>
̣	̣	k?	̣
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ṣ	ṣ	k?	ṣ
<g/>
,	,	kIx,	,
ṣ	ṣ	k?	ṣ
<g/>
́	́	k?	́
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
,	,	kIx,	,
p	p	k?	p
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
písmo	písmo	k1gNnSc1	písmo
psalo	psát	k5eAaImAgNnS	psát
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
známý	známý	k2eAgInSc1d1	známý
nápis	nápis	k1gInSc1	nápis
etiopským	etiopský	k2eAgNnSc7d1	etiopské
písmem	písmo	k1gNnSc7	písmo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
před	před	k7c7	před
našim	náš	k3xOp1gFnPc3	náš
letopočtem	letopočet	k1gInSc7	letopočet
z	z	k7c2	z
města	město	k1gNnSc2	město
Matara	Matara	k1gFnSc1	Matara
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Eritrea	Eritrea	k1gFnSc1	Eritrea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vokalizaci	vokalizace	k1gFnSc3	vokalizace
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
základní	základní	k2eAgInSc1d1	základní
znak	znak	k1gInSc1	znak
zůstal	zůstat	k5eAaPmAgInS	zůstat
vyhrazen	vyhradit	k5eAaPmNgInS	vyhradit
pro	pro	k7c4	pro
slabiku	slabika	k1gFnSc4	slabika
se	s	k7c7	s
souhláskou	souhláska	k1gFnSc7	souhláska
ä	ä	k?	ä
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zvuk	zvuk	k1gInSc4	zvuk
obvykle	obvykle	k6eAd1	obvykle
kolísající	kolísající	k2eAgMnSc1d1	kolísající
mezi	mezi	k7c7	mezi
a	a	k8xC	a
a	a	k8xC	a
e.	e.	k?	e.
O	o	k7c6	o
příčinách	příčina	k1gFnPc6	příčina
vokalizace	vokalizace	k1gFnSc2	vokalizace
nepanuje	panovat	k5eNaImIp3nS	panovat
shoda	shoda	k1gFnSc1	shoda
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
se	se	k3xPyFc4	se
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
lépe	dobře	k6eAd2	dobře
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
výslovnost	výslovnost	k1gFnSc1	výslovnost
již	již	k6eAd1	již
zanikajícího	zanikající	k2eAgInSc2d1	zanikající
jazyka	jazyk	k1gInSc2	jazyk
ge	ge	k?	ge
<g/>
'	'	kIx"	'
<g/>
ez	ez	k?	ez
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
se	se	k3xPyFc4	se
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
vlivem	vliv	k1gInSc7	vliv
indického	indický	k2eAgNnSc2d1	indické
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Znaky	znak	k1gInPc1	znak
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgFnPc4d1	ostatní
slabiky	slabika	k1gFnPc4	slabika
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
odvozeny	odvodit	k5eAaPmNgFnP	odvodit
ze	z	k7c2	z
základního	základní	k2eAgInSc2d1	základní
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
každá	každý	k3xTgFnSc1	každý
souhláska	souhláska	k1gFnSc1	souhláska
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
7	[number]	k4	7
samohlásek	samohláska	k1gFnPc2	samohláska
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
i	i	k8xC	i
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
á	á	k0	á
<g/>
,	,	kIx,	,
é	é	k0	é
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Písmo	písmo	k1gNnSc1	písmo
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
celkem	celkem	k6eAd1	celkem
přes	přes	k7c4	přes
200	[number]	k4	200
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgNnSc1d1	současné
etiopské	etiopský	k2eAgNnSc1d1	etiopské
písmo	písmo	k1gNnSc1	písmo
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
zleva	zleva	k6eAd1	zleva
doprava	doprava	k1gFnSc1	doprava
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
znaky	znak	k1gInPc4	znak
i	i	k9	i
pro	pro	k7c4	pro
souhlásky	souhláska	k1gFnPc4	souhláska
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
původně	původně	k6eAd1	původně
neexistovaly	existovat	k5eNaImAgFnP	existovat
a	a	k8xC	a
které	který	k3yQgMnPc4	který
podchycují	podchycovat	k5eAaImIp3nP	podchycovat
i	i	k9	i
ostatní	ostatní	k2eAgInPc1d1	ostatní
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
<g/>
.	.	kIx.	.
</s>
<s>
Amharština	amharština	k1gFnSc1	amharština
používá	používat	k5eAaImIp3nS	používat
všechny	všechen	k3xTgInPc4	všechen
základní	základní	k2eAgInPc4d1	základní
znaky	znak	k1gInPc4	znak
plus	plus	k6eAd1	plus
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
varianty	varianta	k1gFnPc1	varianta
labiovelár	labiovelára	k1gFnPc2	labiovelára
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
používají	používat	k5eAaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Tigriňština	Tigriňština	k1gFnSc1	Tigriňština
používá	používat	k5eAaImIp3nS	používat
všechny	všechen	k3xTgInPc4	všechen
základní	základní	k2eAgInPc4d1	základní
znaky	znak	k1gInPc4	znak
a	a	k8xC	a
labiovelární	labiovelární	k2eAgFnPc4d1	labiovelární
varianty	varianta	k1gFnPc4	varianta
kromě	kromě	k7c2	kromě
ḫ	ḫ	k?	ḫ
(	(	kIx(	(
<g/>
ኈ	ኈ	k?	ኈ
<g/>
)	)	kIx)	)
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
ty	ten	k3xDgFnPc1	ten
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Tigrejština	Tigrejština	k1gFnSc1	Tigrejština
používá	používat	k5eAaImIp3nS	používat
všechny	všechen	k3xTgInPc4	všechen
základní	základní	k2eAgInPc4d1	základní
znaky	znak	k1gInPc4	znak
kromě	kromě	k7c2	kromě
ś	ś	k?	ś
(	(	kIx(	(
<g/>
ሠ	ሠ	k?	ሠ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ḫ	ḫ	k?	ḫ
(	(	kIx(	(
<g/>
ኀ	ኀ	k?	ኀ
<g/>
)	)	kIx)	)
et	et	k?	et
ḍ	ḍ	k?	ḍ
(	(	kIx(	(
<g/>
ፀ	ፀ	k?	ፀ
<g/>
)	)	kIx)	)
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
ty	ten	k3xDgFnPc1	ten
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Nepoužívá	používat	k5eNaImIp3nS	používat
labiovelární	labiovelární	k2eAgFnPc4d1	labiovelární
varianty	varianta	k1gFnPc4	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Bilenština	Bilenština	k1gFnSc1	Bilenština
používá	používat	k5eAaImIp3nS	používat
všechny	všechen	k3xTgInPc4	všechen
základní	základní	k2eAgInPc4d1	základní
znaky	znak	k1gInPc4	znak
kromě	kromě	k7c2	kromě
ś	ś	k?	ś
(	(	kIx(	(
<g/>
ሠ	ሠ	k?	ሠ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ḫ	ḫ	k?	ḫ
(	(	kIx(	(
<g/>
ኀ	ኀ	k?	ኀ
<g/>
)	)	kIx)	)
et	et	k?	et
ḍ	ḍ	k?	ḍ
(	(	kIx(	(
<g/>
ፀ	ፀ	k?	ፀ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
používá	používat	k5eAaImIp3nS	používat
labiovelární	labiovelární	k2eAgFnPc4d1	labiovelární
varianty	varianta	k1gFnPc4	varianta
a	a	k8xC	a
znaky	znak	k1gInPc4	znak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Ge	Ge	k?	Ge
<g/>
'	'	kIx"	'
<g/>
ez	ez	k?	ez
používá	používat	k5eAaImIp3nS	používat
systém	systém	k1gInSc4	systém
jedniček	jednička	k1gFnPc2	jednička
a	a	k8xC	a
desítek	desítka	k1gFnPc2	desítka
srovnatelých	srovnatelý	k2eAgFnPc2d1	srovnatelá
s	s	k7c7	s
hebrejskými	hebrejský	k2eAgFnPc7d1	hebrejská
číslicemi	číslice	k1gFnPc7	číslice
<g/>
,	,	kIx,	,
arabskými	arabský	k2eAgFnPc7d1	arabská
číslicemi	číslice	k1gFnPc7	číslice
abdžad	abdžad	k6eAd1	abdžad
a	a	k8xC	a
řeckými	řecký	k2eAgFnPc7d1	řecká
číslicemi	číslice	k1gFnPc7	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Etiopské	etiopský	k2eAgFnPc4d1	etiopská
číslice	číslice	k1gFnPc4	číslice
byli	být	k5eAaImAgMnP	být
odvozeny	odvozen	k2eAgMnPc4d1	odvozen
ze	z	k7c2	z
řeckých	řecký	k2eAgFnPc2d1	řecká
číslic	číslice	k1gFnPc2	číslice
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
koptského	koptský	k2eAgNnSc2d1	Koptské
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ge	Ge	k1gFnSc2	Ge
<g/>
'	'	kIx"	'
<g/>
ez	ez	k?	ez
script	script	k1gMnSc1	script
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
písem	písmo	k1gNnPc2	písmo
Ge	Ge	k1gFnSc2	Ge
<g/>
'	'	kIx"	'
<g/>
ez	ez	k?	ez
</s>
