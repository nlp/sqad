<s>
Dita	Dita	k1gFnSc1	Dita
je	být	k5eAaImIp3nS	být
ženské	ženský	k2eAgNnSc4d1	ženské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
zkrácením	zkrácení	k1gNnSc7	zkrácení
jména	jméno	k1gNnSc2	jméno
Judita	Judita	k1gFnSc1	Judita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
občanském	občanský	k2eAgInSc6d1	občanský
kalendáři	kalendář	k1gInSc6	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
ženskými	ženský	k2eAgNnPc7d1	ženské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaPmF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgFnPc7d1	žijící
ženami	žena	k1gFnPc7	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
+1,8	+1,8	k4	+1,8
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k1gFnSc1	domácí
podoby	podoba	k1gFnSc2	podoba
Ditka	Ditka	k1gFnSc1	Ditka
<g/>
,	,	kIx,	,
Ditunka	Ditunka	k1gFnSc1	Ditunka
<g/>
,	,	kIx,	,
Díťa	Díťa	k1gMnSc1	Díťa
<g/>
,	,	kIx,	,
Dituška	Dituška	k1gMnSc1	Dituška
<g/>
,	,	kIx,	,
Ditulínek	Ditulínek	k1gMnSc1	Ditulínek
<g/>
,	,	kIx,	,
Dituna	Dituna	k1gFnSc1	Dituna
<g/>
,	,	kIx,	,
Tulínek	Tulínek	k1gInSc1	Tulínek
Dita	Dita	k1gFnSc1	Dita
Petrusová	Petrusový	k2eAgFnSc1d1	Petrusová
Filipová	Filipová	k1gFnSc1	Filipová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
módní	módní	k2eAgFnSc1d1	módní
návrhářka	návrhářka	k1gFnSc1	návrhářka
Dita	Dita	k1gFnSc1	Dita
Tuháčková	Tuháčková	k1gFnSc1	Tuháčková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
tanečnice	tanečnice	k1gFnSc1	tanečnice
latinskoamerických	latinskoamerický	k2eAgInPc2d1	latinskoamerický
tanců	tanec	k1gInPc2	tanec
DITA	Dita	k1gFnSc1	Dita
–	–	k?	–
Darwin	Darwin	k1gMnSc1	Darwin
Information	Information	k1gInSc1	Information
Typing	Typing	k1gInSc1	Typing
Architecture	Architectur	k1gMnSc5	Architectur
–	–	k?	–
architektura	architektura	k1gFnSc1	architektura
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
technickými	technický	k2eAgInPc7d1	technický
texty	text	k1gInPc7	text
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c4	na
XML	XML	kA	XML
</s>
