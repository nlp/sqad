<p>
<s>
386BSD	[number]	k4	386BSD
(	(	kIx(	(
<g/>
též	též	k9	též
Jolix	Jolix	k1gInSc1	Jolix
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
svobodný	svobodný	k2eAgInSc4d1	svobodný
unixový	unixový	k2eAgInSc4d1	unixový
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
vycházející	vycházející	k2eAgInPc4d1	vycházející
z	z	k7c2	z
BSD	BSD	kA	BSD
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Běžel	běžet	k5eAaImAgInS	běžet
na	na	k7c6	na
IBM	IBM	kA	IBM
PC	PC	kA	PC
kompatibilních	kompatibilní	k2eAgInPc6d1	kompatibilní
počítačích	počítač	k1gInPc6	počítač
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
procesoru	procesor	k1gInSc6	procesor
Intel	Intel	kA	Intel
80386	[number]	k4	80386
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
386BSD	[number]	k4	386BSD
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
napsali	napsat	k5eAaBmAgMnP	napsat
dva	dva	k4xCgMnPc1	dva
absolventi	absolvent	k1gMnPc1	absolvent
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c4	v
Berkeley	Berkelea	k1gFnPc4	Berkelea
<g/>
:	:	kIx,	:
Lynne	Lynn	k1gInSc5	Lynn
a	a	k8xC	a
Williame	William	k1gInSc5	William
Jolitz	Jolitz	k1gInSc1	Jolitz
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k6eAd1	William
Jolitz	Jolitz	k1gInSc1	Jolitz
měl	mít	k5eAaImAgInS	mít
již	již	k6eAd1	již
velké	velký	k2eAgFnPc4d1	velká
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
předchozími	předchozí	k2eAgFnPc7d1	předchozí
verzemi	verze	k1gFnPc7	verze
BSD	BSD	kA	BSD
(	(	kIx(	(
<g/>
2.8	[number]	k4	2.8
a	a	k8xC	a
2.9	[number]	k4	2.9
<g/>
BSD	BSD	kA	BSD
<g/>
)	)	kIx)	)
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
přispívali	přispívat	k5eAaImAgMnP	přispívat
kódem	kód	k1gInSc7	kód
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
v	v	k7c6	v
Symmetric	Symmetrice	k1gFnPc2	Symmetrice
Computer	computer	k1gInSc1	computer
Systems	Systems	k1gInSc1	Systems
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
1980	[number]	k4	1980
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k6eAd1	William
Jolitz	Jolitz	k1gMnSc1	Jolitz
pracoval	pracovat	k5eAaImAgMnS	pracovat
také	také	k9	také
na	na	k7c4	na
portování	portování	k1gNnSc4	portování
4.3	[number]	k4	4.3
<g/>
BSD-Reno	BSD-Ren	k2eAgNnSc1d1	BSD-Ren
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
4.3	[number]	k4	4.3
<g/>
BSD	BSD	kA	BSD
Net	Net	k1gFnPc2	Net
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
na	na	k7c4	na
Intel	Intel	kA	Intel
80386	[number]	k4	80386
<g/>
.	.	kIx.	.
4.3	[number]	k4	4.3
<g/>
BSD	BSD	kA	BSD
Net	Net	k1gFnPc2	Net
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
byl	být	k5eAaImAgInS	být
neúplný	úplný	k2eNgInSc1d1	neúplný
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
nefunkční	funkční	k2eNgInSc4d1	nefunkční
systém	systém	k1gInSc4	systém
s	s	k7c7	s
částmi	část	k1gFnPc7	část
kódu	kód	k1gInSc2	kód
nepatřícími	patřící	k2eNgFnPc7d1	nepatřící
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
(	(	kIx(	(
<g/>
vlastněných	vlastněný	k2eAgFnPc2d1	vlastněná
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T	T	kA	T
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uvolnění	uvolnění	k1gNnSc1	uvolnění
386BSD	[number]	k4	386BSD
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
částech	část	k1gFnPc6	část
4.3	[number]	k4	4.3
<g/>
BSD	BSD	kA	BSD
Net	Net	k1gFnPc2	Net
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalším	další	k2eAgInSc7d1	další
doplňujícím	doplňující	k2eAgInSc7d1	doplňující
kódem	kód	k1gInSc7	kód
napsaném	napsaný	k2eAgInSc6d1	napsaný
Williamem	William	k1gInSc7	William
a	a	k8xC	a
Lynne	Lynn	k1gInSc5	Lynn
Jolitzovými	Jolitzův	k2eAgFnPc7d1	Jolitzův
nutným	nutný	k2eAgMnSc7d1	nutný
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
kompletní	kompletní	k2eAgFnSc1d1	kompletní
a	a	k8xC	a
funkční	funkční	k2eAgFnSc1d1	funkční
verze	verze	k1gFnSc1	verze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Portování	Portování	k1gNnSc1	Portování
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
a	a	k8xC	a
první	první	k4xOgFnPc1	první
<g/>
,	,	kIx,	,
neúplné	úplný	k2eNgFnPc1d1	neúplná
stopy	stopa	k1gFnPc1	stopa
portu	port	k1gInSc2	port
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
4.3	[number]	k4	4.3
<g/>
BSD	BSD	kA	BSD
Net	Net	k1gFnPc2	Net
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1992	[number]	k4	1992
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc1	verze
0.0	[number]	k4	0.0
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
použitelnější	použitelný	k2eAgFnSc6d2	použitelnější
verzi	verze	k1gFnSc6	verze
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1992	[number]	k4	1992
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc1	verze
0.1	[number]	k4	0.1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
portování	portování	k1gNnSc2	portování
společně	společně	k6eAd1	společně
s	s	k7c7	s
kódem	kód	k1gInSc7	kód
byl	být	k5eAaImAgInS	být
bohatě	bohatě	k6eAd1	bohatě
dokumentován	dokumentovat	k5eAaBmNgInS	dokumentovat
v	v	k7c6	v
18	[number]	k4	18
<g/>
dílném	dílný	k2eAgInSc6d1	dílný
seriálu	seriál	k1gInSc6	seriál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
napsali	napsat	k5eAaBmAgMnP	napsat
Lynne	Lynn	k1gInSc5	Lynn
Jolitz	Jolitz	k1gInSc4	Jolitz
a	a	k8xC	a
Williamem	William	k1gInSc7	William
Jolitz	Jolitza	k1gFnPc2	Jolitza
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dobb	Dobb	k1gMnSc1	Dobb
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Journal	Journal	k1gFnSc7	Journal
uveřejňovaném	uveřejňovaný	k2eAgInSc6d1	uveřejňovaný
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
FreeBSD	FreeBSD	k1gMnPc3	FreeBSD
a	a	k8xC	a
NetBSD	NetBSD	k1gMnPc3	NetBSD
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
386BSD	[number]	k4	386BSD
verze	verze	k1gFnSc1	verze
0.1	[number]	k4	0.1
začala	začít	k5eAaPmAgFnS	začít
skupina	skupina	k1gFnSc1	skupina
uživatelů	uživatel	k1gMnPc2	uživatel
sbírat	sbírat	k5eAaImF	sbírat
opravy	oprava	k1gFnPc4	oprava
chyb	chyba	k1gFnPc2	chyba
a	a	k8xC	a
různá	různý	k2eAgNnPc4d1	různé
vylepšení	vylepšení	k1gNnSc4	vylepšení
a	a	k8xC	a
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
je	být	k5eAaImIp3nS	být
jako	jako	k8xC	jako
neoficiální	oficiální	k2eNgFnSc1d1	neoficiální
aktualizace	aktualizace	k1gFnSc1	aktualizace
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozdílům	rozdíl	k1gInPc3	rozdíl
v	v	k7c6	v
názorech	názor	k1gInPc6	názor
s	s	k7c7	s
autory	autor	k1gMnPc7	autor
386BSD	[number]	k4	386BSD
založili	založit	k5eAaPmAgMnP	založit
správci	správce	k1gMnPc1	správce
aktualizací	aktualizace	k1gFnPc2	aktualizace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
projekt	projekt	k1gInSc4	projekt
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
také	také	k9	také
projekt	projekt	k1gInSc1	projekt
NetBSD	NetBSD	k1gFnSc2	NetBSD
jinou	jiný	k2eAgFnSc7d1	jiná
skupinou	skupina	k1gFnSc7	skupina
uživatelů	uživatel	k1gMnPc2	uživatel
386	[number]	k4	386
<g/>
BSD	BSD	kA	BSD
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
sjednotit	sjednotit	k5eAaPmF	sjednotit
386BSD	[number]	k4	386BSD
s	s	k7c7	s
jinými	jiný	k2eAgNnPc7d1	jiné
vlákny	vlákno	k1gNnPc7	vlákno
vývoje	vývoj	k1gInSc2	vývoj
BSD	BSD	kA	BSD
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
multiplatformního	multiplatformní	k2eAgInSc2d1	multiplatformní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
tyto	tento	k3xDgInPc1	tento
projekty	projekt	k1gInPc1	projekt
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Verze	verze	k1gFnSc1	verze
1.0	[number]	k4	1.0
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
byl	být	k5eAaImAgMnS	být
dokončený	dokončený	k2eAgMnSc1d1	dokončený
386BSD	[number]	k4	386BSD
verze	verze	k1gFnSc1	verze
1.0	[number]	k4	1.0
distribuován	distribuovat	k5eAaBmNgMnS	distribuovat
na	na	k7c4	na
CD-ROM	CD-ROM	k1gFnSc4	CD-ROM
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
obrovské	obrovský	k2eAgFnSc3d1	obrovská
velikosti	velikost	k1gFnSc3	velikost
600	[number]	k4	600
MB	MB	kA	MB
<g/>
)	)	kIx)	)
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
časopisem	časopis	k1gInSc7	časopis
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dobb	Dobb	k1gMnSc1	Dobb
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Journal	Journal	k1gFnSc7	Journal
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
nejprodávanější	prodávaný	k2eAgNnSc1d3	nejprodávanější
CD-ROM	CD-ROM	k1gFnSc2	CD-ROM
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
386BSD	[number]	k4	386BSD
verze	verze	k1gFnSc1	verze
1.0	[number]	k4	1.0
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zcela	zcela	k6eAd1	zcela
nový	nový	k2eAgInSc4d1	nový
návrh	návrh	k1gInSc4	návrh
a	a	k8xC	a
implementaci	implementace	k1gFnSc4	implementace
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
a	a	k8xC	a
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
proces	proces	k1gInSc1	proces
začleňování	začleňování	k1gNnSc2	začleňování
dřívějších	dřívější	k2eAgNnPc2d1	dřívější
doporučení	doporučení	k1gNnPc2	doporučení
vývojářů	vývojář	k1gMnPc2	vývojář
z	z	k7c2	z
Berkeley	Berkelea	k1gFnSc2	Berkelea
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neobjevila	objevit	k5eNaPmAgFnS	objevit
v	v	k7c6	v
BSD	BSD	kA	BSD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
BSD	BSD	kA	BSD
<g/>
/	/	kIx~	/
<g/>
386	[number]	k4	386
==	==	k?	==
</s>
</p>
<p>
<s>
Systém	systém	k1gInSc1	systém
386BSD	[number]	k4	386BSD
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
zaměňován	zaměňován	k2eAgInSc1d1	zaměňován
s	s	k7c7	s
BSD	BSD	kA	BSD
<g/>
/	/	kIx~	/
<g/>
386	[number]	k4	386
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
společností	společnost	k1gFnSc7	společnost
BSDi	BSDi	k1gNnSc4	BSDi
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
BSD	BSD	kA	BSD
<g/>
/	/	kIx~	/
<g/>
386	[number]	k4	386
používal	používat	k5eAaImAgMnS	používat
ten	ten	k3xDgMnSc1	ten
samý	samý	k3xTgMnSc1	samý
kód	kód	k1gInSc4	kód
jako	jako	k9	jako
386BSD	[number]	k4	386BSD
se	s	k7c7	s
základem	základ	k1gInSc7	základ
v	v	k7c6	v
4.3	[number]	k4	4.3
<g/>
BSD	BSD	kA	BSD
NET	NET	kA	NET
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
William	William	k1gInSc1	William
Jolitz	Jolitza	k1gFnPc2	Jolitza
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
krátce	krátce	k6eAd1	krátce
pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
UUNET	UUNET	kA	UUNET
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
oddělila	oddělit	k5eAaPmAgFnS	oddělit
BSDi	BSD	k1gInSc3	BSD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
se	se	k3xPyFc4	se
však	však	k9	však
lišila	lišit	k5eAaImAgFnS	lišit
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
přispíval	přispívat	k5eAaImAgInS	přispívat
univerzitě	univerzita	k1gFnSc3	univerzita
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
neobjevila	objevit	k5eNaPmAgFnS	objevit
v	v	k7c6	v
kódu	kód	k1gInSc6	kód
386	[number]	k4	386
<g/>
BSD	BSD	kA	BSD
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
dával	dávat	k5eAaImAgInS	dávat
William	William	k1gInSc1	William
Jolitz	Jolitza	k1gFnPc2	Jolitza
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
aktualizace	aktualizace	k1gFnSc2	aktualizace
kódu	kód	k1gInSc2	kód
Donnu	donna	k1gFnSc4	donna
Seeleymu	Seeleym	k1gInSc2	Seeleym
z	z	k7c2	z
BSDi	BSD	k1gFnSc2	BSD
na	na	k7c4	na
otestování	otestování	k1gNnSc4	otestování
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc4	všechen
materiály	materiál	k1gInPc4	materiál
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgInP	být
vráceny	vrátit	k5eAaPmNgInP	vrátit
<g/>
,	,	kIx,	,
když	když	k8xS	když
opustil	opustit	k5eAaPmAgMnS	opustit
společnost	společnost	k1gFnSc4	společnost
pro	pro	k7c4	pro
zásadní	zásadní	k2eAgFnPc4d1	zásadní
neshody	neshoda	k1gFnPc4	neshoda
ohledně	ohledně	k7c2	ohledně
cílů	cíl	k1gInPc2	cíl
a	a	k8xC	a
směřování	směřování	k1gNnSc2	směřování
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Autorská	autorský	k2eAgNnPc1d1	autorské
práva	právo	k1gNnPc1	právo
a	a	k8xC	a
použití	použití	k1gNnSc1	použití
kódu	kód	k1gInSc2	kód
==	==	k?	==
</s>
</p>
<p>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
autorská	autorský	k2eAgNnPc1d1	autorské
práva	právo	k1gNnPc1	právo
k	k	k7c3	k
386BSD	[number]	k4	386BSD
a	a	k8xC	a
JOLIXu	JOLIXa	k1gMnSc4	JOLIXa
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
drženy	držet	k5eAaImNgFnP	držet
výhradně	výhradně	k6eAd1	výhradně
Williamem	William	k1gInSc7	William
a	a	k8xC	a
Lynne	Lynn	k1gInSc5	Lynn
Jolitzovými	Jolitzův	k2eAgMnPc7d1	Jolitzův
<g/>
.	.	kIx.	.
</s>
<s>
Vydávání	vydávání	k1gNnSc1	vydávání
386BSD	[number]	k4	386BSD
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Kód	kód	k1gInSc1	kód
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
odvozených	odvozený	k2eAgInPc6d1	odvozený
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
několika	několik	k4yIc7	několik
jejich	jejich	k3xOp3gInPc7	jejich
deriváty	derivát	k1gInPc7	derivát
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Darwin	Darwin	k1gMnSc1	Darwin
Apple	Apple	kA	Apple
a	a	k8xC	a
OpenBSD	OpenBSD	k1gFnSc2	OpenBSD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
386BSD	[number]	k4	386BSD
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
otevřených	otevřený	k2eAgInPc6d1	otevřený
systémech	systém	k1gInPc6	systém
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
OpenSolaris	OpenSolaris	k1gFnSc1	OpenSolaris
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
386BSD	[number]	k4	386BSD
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
