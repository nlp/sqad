<s>
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
je	být	k5eAaImIp3nS	být
australská	australský	k2eAgFnSc1d1	australská
hard	hard	k6eAd1	hard
rocková	rockový	k2eAgFnSc1d1	rocková
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
založili	založit	k5eAaPmAgMnP	založit
bratři	bratr	k1gMnPc1	bratr
Angus	Angus	k1gMnSc1	Angus
Young	Young	k1gMnSc1	Young
a	a	k8xC	a
Malcolm	Malcolm	k1gMnSc1	Malcolm
Young	Young	k1gMnSc1	Young
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
<g/>
,	,	kIx,	,
Deep	Deep	k1gInSc1	Deep
Purple	Purple	k1gFnPc2	Purple
nebo	nebo	k8xC	nebo
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
průkopníky	průkopník	k1gMnPc4	průkopník
hard	hard	k6eAd1	hard
rocku	rock	k1gInSc2	rock
a	a	k8xC	a
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
však	však	k9	však
styl	styl	k1gInSc4	styl
kapely	kapela	k1gFnSc2	kapela
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k8xS	jako
rock	rock	k1gInSc4	rock
and	and	k?	and
roll	roll	k1gInSc1	roll
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
prošla	projít	k5eAaPmAgFnS	projít
několika	několik	k4yIc2	několik
změnami	změna	k1gFnPc7	změna
sestavy	sestava	k1gFnPc4	sestava
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
High	Higha	k1gFnPc2	Higha
Voltage	Voltag	k1gInSc2	Voltag
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
další	další	k2eAgFnSc3d1	další
změně	změna	k1gFnSc3	změna
sestavy	sestava	k1gFnSc2	sestava
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
baskytaristy	baskytarista	k1gMnSc2	baskytarista
<g/>
,	,	kIx,	,
když	když	k8xS	když
Cliff	Cliff	k1gMnSc1	Cliff
Williams	Williams	k1gInSc4	Williams
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Marka	Marek	k1gMnSc4	Marek
Evanse	Evans	k1gMnSc4	Evans
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
vydala	vydat	k5eAaPmAgFnS	vydat
kapela	kapela	k1gFnSc1	kapela
velice	velice	k6eAd1	velice
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
album	album	k1gNnSc4	album
Highway	Highwaa	k1gFnSc2	Highwaa
to	ten	k3xDgNnSc4	ten
Hell	Hell	k1gInSc1	Hell
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
a	a	k8xC	a
spoluautor	spoluautor	k1gMnSc1	spoluautor
písní	píseň	k1gFnPc2	píseň
Bon	bon	k1gInSc4	bon
Scott	Scott	k1gInSc4	Scott
však	však	k9	však
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1980	[number]	k4	1980
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
otravu	otrava	k1gFnSc4	otrava
alkoholem	alkohol	k1gInSc7	alkohol
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nalezen	nalezen	k2eAgMnSc1d1	nalezen
mrtev	mrtev	k2eAgMnSc1d1	mrtev
na	na	k7c6	na
zadním	zadní	k2eAgNnSc6d1	zadní
sedadle	sedadlo	k1gNnSc6	sedadlo
automobilu	automobil	k1gInSc2	automobil
jeho	on	k3xPp3gMnSc2	on
kamaráda	kamarád	k1gMnSc2	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
uvažovala	uvažovat	k5eAaImAgFnS	uvažovat
o	o	k7c6	o
ukončení	ukončení	k1gNnSc6	ukončení
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
byl	být	k5eAaImAgMnS	být
jako	jako	k8xC	jako
nový	nový	k2eAgMnSc1d1	nový
zpěvák	zpěvák	k1gMnSc1	zpěvák
vybrán	vybrán	k2eAgMnSc1d1	vybrán
Brian	Brian	k1gMnSc1	Brian
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Geordie	Geordie	k1gFnSc2	Geordie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
vydala	vydat	k5eAaPmAgFnS	vydat
kapela	kapela	k1gFnSc1	kapela
své	svůj	k3xOyFgNnSc4	svůj
nejúspěšnější	úspěšný	k2eAgNnSc4d3	nejúspěšnější
album	album	k1gNnSc4	album
Back	Back	k1gMnSc1	Back
in	in	k?	in
Black	Black	k1gMnSc1	Black
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnSc4d1	následující
album	album	k1gNnSc4	album
For	forum	k1gNnPc2	forum
Those	Those	k1gFnSc2	Those
About	About	k2eAgInSc1d1	About
to	ten	k3xDgNnSc1	ten
Rock	rock	k1gInSc4	rock
We	We	k1gMnSc2	We
Salute	salut	k1gInSc5	salut
You	You	k1gFnPc3	You
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
velice	velice	k6eAd1	velice
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
prvním	první	k4xOgInPc3	první
hard	hard	k6eAd1	hard
rockovým	rockový	k2eAgNnSc7d1	rockové
albem	album	k1gNnSc7	album
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
amerického	americký	k2eAgInSc2d1	americký
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
bubeníka	bubeník	k1gMnSc2	bubeník
Phila	Phil	k1gMnSc2	Phil
Rudda	Rudd	k1gMnSc2	Rudd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
však	však	k9	však
začala	začít	k5eAaPmAgFnS	začít
popularita	popularita	k1gFnSc1	popularita
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
prodejnost	prodejnost	k1gFnSc1	prodejnost
jejích	její	k3xOp3gFnPc2	její
desek	deska	k1gFnPc2	deska
klesat	klesat	k5eAaImF	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Nepříznivý	příznivý	k2eNgInSc1d1	nepříznivý
trend	trend	k1gInSc1	trend
zvrátilo	zvrátit	k5eAaPmAgNnS	zvrátit
až	až	k9	až
album	album	k1gNnSc1	album
The	The	k1gFnSc2	The
Razors	Razorsa	k1gFnPc2	Razorsa
Edge	Edg	k1gFnSc2	Edg
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Phil	Phil	k1gMnSc1	Phil
Rudd	Rudd	k1gMnSc1	Rudd
se	se	k3xPyFc4	se
do	do	k7c2	do
kapely	kapela	k1gFnSc2	kapela
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
a	a	k8xC	a
podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
albu	album	k1gNnSc6	album
Ballbreaker	Ballbreakra	k1gFnPc2	Ballbreakra
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
album	album	k1gNnSc1	album
Stiff	Stiff	k1gInSc1	Stiff
Upper	Upper	k1gMnSc1	Upper
Lip	lípa	k1gFnPc2	lípa
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
a	a	k8xC	a
kritikou	kritika	k1gFnSc7	kritika
bylo	být	k5eAaImAgNnS	být
pozitivně	pozitivně	k6eAd1	pozitivně
přijato	přijmout	k5eAaPmNgNnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
posledním	poslední	k2eAgNnSc7d1	poslední
albem	album	k1gNnSc7	album
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
je	být	k5eAaImIp3nS	být
Rock	rock	k1gInSc1	rock
or	or	k?	or
Bust	busta	k1gFnPc2	busta
z	z	k7c2	z
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kapela	kapela	k1gFnSc1	kapela
prodala	prodat	k5eAaPmAgFnS	prodat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
asi	asi	k9	asi
160	[number]	k4	160
milionů	milion	k4xCgInPc2	milion
alb	alba	k1gFnPc2	alba
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
68	[number]	k4	68
milionů	milion	k4xCgInPc2	milion
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Alba	alba	k1gFnSc1	alba
Back	Backa	k1gFnPc2	Backa
in	in	k?	in
Black	Black	k1gMnSc1	Black
se	se	k3xPyFc4	se
celosvětově	celosvětově	k6eAd1	celosvětově
prodalo	prodat	k5eAaPmAgNnS	prodat
52	[number]	k4	52
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
21	[number]	k4	21
milionů	milion	k4xCgInPc2	milion
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Back	Back	k1gMnSc1	Back
in	in	k?	in
Black	Black	k1gMnSc1	Black
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
druhým	druhý	k4xOgNnSc7	druhý
celosvětově	celosvětově	k6eAd1	celosvětově
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
úspěšnější	úspěšný	k2eAgFnSc2d2	úspěšnější
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
album	album	k1gNnSc1	album
Michaela	Michael	k1gMnSc2	Michael
Jacksona	Jackson	k1gMnSc2	Jackson
–	–	k?	–
Thriller	thriller	k1gInSc1	thriller
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
Kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
100	[number]	k4	100
Greatest	Greatest	k1gInSc1	Greatest
Artists	Artists	k1gInSc4	Artists
of	of	k?	of
Hard	Hard	k1gInSc1	Hard
Rock	rock	k1gInSc1	rock
televize	televize	k1gFnSc2	televize
VH1	VH1	k1gFnSc2	VH1
a	a	k8xC	a
televizí	televize	k1gFnSc7	televize
MTV	MTV	kA	MTV
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
"	"	kIx"	"
<g/>
sedmou	sedmý	k4xOgFnSc4	sedmý
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
heavy	heav	k1gInPc4	heav
metalovou	metalový	k2eAgFnSc7d1	metalová
kapelou	kapela	k1gFnSc7	kapela
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Angus	Angus	k1gMnSc1	Angus
a	a	k8xC	a
Malcolm	Malcolm	k1gMnSc1	Malcolm
Youngovi	Youngův	k2eAgMnPc1d1	Youngův
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
kapely	kapela	k1gFnSc2	kapela
napadl	napadnout	k5eAaPmAgMnS	napadnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
viděli	vidět	k5eAaImAgMnP	vidět
zkratku	zkratka	k1gFnSc4	zkratka
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
na	na	k7c6	na
šicím	šicí	k2eAgInSc6d1	šicí
stroji	stroj	k1gInSc6	stroj
svojí	svůj	k3xOyFgFnSc2	svůj
sestry	sestra	k1gFnSc2	sestra
Margaret	Margareta	k1gFnPc2	Margareta
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
vysavač	vysavač	k1gInSc4	vysavač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
je	být	k5eAaImIp3nS	být
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
zkratkou	zkratka	k1gFnSc7	zkratka
termínu	termín	k1gInSc2	termín
"	"	kIx"	"
<g/>
alternating	alternating	k1gInSc1	alternating
current	current	k1gInSc1	current
<g/>
/	/	kIx~	/
<g/>
direct	direct	k2eAgInSc1d1	direct
current	current	k1gInSc1	current
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
<g/>
/	/	kIx~	/
<g/>
stejnosměrný	stejnosměrný	k2eAgInSc1d1	stejnosměrný
proud	proud	k1gInSc1	proud
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
měli	mít	k5eAaImAgMnP	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jméno	jméno	k1gNnSc1	jméno
dobře	dobře	k6eAd1	dobře
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
nefalšovanou	falšovaný	k2eNgFnSc4d1	nefalšovaná
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
energická	energický	k2eAgNnPc4d1	energické
vystoupení	vystoupení	k1gNnPc4	vystoupení
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
kulturách	kultura	k1gFnPc6	kultura
však	však	k9	však
zkratka	zkratka	k1gFnSc1	zkratka
slangově	slangově	k6eAd1	slangově
"	"	kIx"	"
<g/>
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
"	"	kIx"	"
označuje	označovat	k5eAaImIp3nS	označovat
bisexuály	bisexuál	k1gMnPc4	bisexuál
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
toho	ten	k3xDgNnSc2	ten
nebyla	být	k5eNaImAgFnS	být
vědoma	vědom	k2eAgFnSc1d1	vědoma
až	až	k9	až
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
jejich	jejich	k3xOp3gInPc2	jejich
prvních	první	k4xOgInPc2	první
koncertů	koncert	k1gInPc2	koncert
řekl	říct	k5eAaPmAgMnS	říct
taxikář	taxikář	k1gMnSc1	taxikář
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
vezl	vézt	k5eAaImAgMnS	vézt
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
náboženští	náboženský	k2eAgMnPc1d1	náboženský
představitelé	představitel	k1gMnPc1	představitel
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zkratka	zkratka	k1gFnSc1	zkratka
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Anti-Christ	Anti-Christ	k1gInSc1	Anti-Christ
<g/>
/	/	kIx~	/
<g/>
Devil	Devil	k1gInSc1	Devil
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Child	Childa	k1gFnPc2	Childa
<g/>
(	(	kIx(	(
<g/>
ren	ren	k?	ren
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Anti-Christ	Anti-Christ	k1gMnSc1	Anti-Christ
<g/>
/	/	kIx~	/
<g/>
Devil	Devil	k1gMnSc1	Devil
Christ	Christ	k1gMnSc1	Christ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
má	mít	k5eAaImIp3nS	mít
ještě	ještě	k6eAd1	ještě
jiné	jiný	k2eAgInPc4d1	jiný
významy	význam	k1gInPc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
spekulace	spekulace	k1gFnPc1	spekulace
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
kritiky	kritik	k1gMnPc7	kritik
udržely	udržet	k5eAaPmAgFnP	udržet
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
kapelu	kapela	k1gFnSc4	kapela
prezentovat	prezentovat	k5eAaBmF	prezentovat
jako	jako	k9	jako
satanisty	satanista	k1gMnSc2	satanista
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
tyto	tento	k3xDgFnPc1	tento
interpretace	interpretace	k1gFnPc1	interpretace
svého	svůj	k3xOyFgNnSc2	svůj
jména	jméno	k1gNnSc2	jméno
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
úmyslně	úmyslně	k6eAd1	úmyslně
vykonstruované	vykonstruovaný	k2eAgInPc4d1	vykonstruovaný
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
poškození	poškození	k1gNnSc2	poškození
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
kapel	kapela	k1gFnPc2	kapela
hrajících	hrající	k2eAgMnPc2d1	hrající
coververze	coververze	k1gFnPc4	coververze
písní	píseň	k1gFnPc2	píseň
od	od	k7c2	od
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
jméno	jméno	k1gNnSc4	jméno
kapely	kapela	k1gFnPc1	kapela
napodobily	napodobit	k5eAaPmAgFnP	napodobit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
BC	BC	kA	BC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
z	z	k7c2	z
Britské	britský	k2eAgFnSc2d1	britská
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DShee	DShee	k1gInSc1	DShee
kterou	který	k3yQgFnSc4	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
samé	samý	k3xTgFnPc4	samý
ženy	žena	k1gFnPc4	žena
ze	z	k7c2	z
San	San	k1gFnSc2	San
Francisca	Francisc	k1gInSc2	Francisc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
kapele	kapela	k1gFnSc3	kapela
někdy	někdy	k6eAd1	někdy
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
Praha	Praha	k1gFnSc1	Praha
<g/>
/	/	kIx~	/
<g/>
Děčín	Děčín	k1gInSc1	Děčín
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
SPZ	SPZ	kA	SPZ
těchto	tento	k3xDgInPc6	tento
dvou	dva	k4xCgNnPc2	dva
měst	město	k1gNnPc2	město
tvoří	tvořit	k5eAaImIp3nP	tvořit
zkratku	zkratka	k1gFnSc4	zkratka
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
.	.	kIx.	.
<g/>
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
Angus	Angus	k1gMnSc1	Angus
Young	Young	k1gMnSc1	Young
<g/>
,	,	kIx,	,
Malcolm	Malcolm	k1gMnSc1	Malcolm
Young	Young	k1gMnSc1	Young
a	a	k8xC	a
George	George	k1gFnSc1	George
Young	Younga	k1gFnPc2	Younga
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
v	v	k7c6	v
Glasgow	Glasgow	k1gNnSc6	Glasgow
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
a	a	k8xC	a
do	do	k7c2	do
Austrálie	Austrálie	k1gFnSc2	Austrálie
se	se	k3xPyFc4	se
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
George	Georgat	k5eAaPmIp3nS	Georgat
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
naučil	naučit	k5eAaPmAgMnS	naučit
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
The	The	k1gFnSc2	The
Easybeats	Easybeatsa	k1gFnPc2	Easybeatsa
<g/>
,	,	kIx,	,
nejúspěšnější	úspěšný	k2eAgFnPc1d3	nejúspěšnější
australské	australský	k2eAgFnPc1d1	australská
kapely	kapela	k1gFnPc1	kapela
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
pak	pak	k6eAd1	pak
vydali	vydat	k5eAaPmAgMnP	vydat
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
hit	hit	k1gInSc4	hit
"	"	kIx"	"
<g/>
Friday	Fridaa	k1gMnSc2	Fridaa
On	on	k3xPp3gMnSc1	on
My	my	k3xPp1nPc1	my
Mind	Mindo	k1gNnPc2	Mindo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Malcolm	Malcolm	k1gInSc1	Malcolm
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
stopách	stopa	k1gFnPc6	stopa
svého	své	k1gNnSc2	své
bratra	bratr	k1gMnSc2	bratr
a	a	k8xC	a
v	v	k7c6	v
Newcastlu	Newcastlo	k1gNnSc6	Newcastlo
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jižním	jižní	k2eAgInSc6d1	jižní
Walesu	Wales	k1gInSc6	Wales
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc1	underground
(	(	kIx(	(
<g/>
nezaměňovat	zaměňovat	k5eNaImF	zaměňovat
se	se	k3xPyFc4	se
slavnější	slavný	k2eAgFnSc7d2	slavnější
stejnojmennou	stejnojmenný	k2eAgFnSc7d1	stejnojmenná
kapelou	kapela	k1gFnSc7	kapela
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Malcolm	Malcolm	k1gMnSc1	Malcolm
a	a	k8xC	a
Angus	Angus	k1gMnSc1	Angus
Youngovi	Youngův	k2eAgMnPc1d1	Youngův
založili	založit	k5eAaPmAgMnP	založit
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
členy	člen	k1gMnPc7	člen
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
baskytarista	baskytarista	k1gMnSc1	baskytarista
Larry	Larra	k1gFnSc2	Larra
Van	vana	k1gFnPc2	vana
Kriedt	Kriedt	k1gMnSc1	Kriedt
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
Dave	Dav	k1gInSc5	Dav
Evans	Evans	k1gInSc1	Evans
a	a	k8xC	a
Colin	Colin	k2eAgInSc1d1	Colin
Burgess	Burgess	k1gInSc1	Burgess
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
bubeník	bubeník	k1gMnSc1	bubeník
kapely	kapela	k1gFnSc2	kapela
The	The	k1gMnSc1	The
Master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Apprentices	Apprenticesa	k1gFnPc2	Apprenticesa
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
odehrála	odehrát	k5eAaPmAgFnS	odehrát
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Chequers	Chequersa	k1gFnPc2	Chequersa
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
podepsali	podepsat	k5eAaPmAgMnP	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
labelem	label	k1gInSc7	label
Albert	Alberta	k1gFnPc2	Alberta
Productions	Productionsa	k1gFnPc2	Productionsa
<g/>
.	.	kIx.	.
</s>
<s>
Sestava	sestava	k1gFnSc1	sestava
kapely	kapela	k1gFnSc2	kapela
se	se	k3xPyFc4	se
často	často	k6eAd1	často
měnila	měnit	k5eAaImAgFnS	měnit
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
byl	být	k5eAaImAgInS	být
vyhozen	vyhozen	k2eAgInSc1d1	vyhozen
Colin	Colin	k2eAgInSc1d1	Colin
Burgess	Burgess	k1gInSc1	Burgess
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
několik	několik	k4yIc1	několik
baskytaristů	baskytarista	k1gMnPc2	baskytarista
a	a	k8xC	a
bubeníků	bubeník	k1gMnPc2	bubeník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
začal	začít	k5eAaPmAgMnS	začít
Angus	Angus	k1gMnSc1	Angus
Young	Young	k1gMnSc1	Young
při	při	k7c6	při
koncertech	koncert	k1gInPc6	koncert
nosit	nosit	k5eAaImF	nosit
svou	svůj	k3xOyFgFnSc4	svůj
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
školní	školní	k2eAgFnSc4d1	školní
uniformu	uniforma	k1gFnSc4	uniforma
<g/>
.	.	kIx.	.
</s>
<s>
Uniforma	uniforma	k1gFnSc1	uniforma
údajně	údajně	k6eAd1	údajně
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
Ashfield	Ashfielda	k1gFnPc2	Ashfielda
Boys	boy	k1gMnPc2	boy
High	High	k1gInSc4	High
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
nápadem	nápad	k1gInSc7	nápad
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ji	on	k3xPp3gFnSc4	on
při	při	k7c6	při
koncertech	koncert	k1gInPc6	koncert
nosil	nosit	k5eAaImAgInS	nosit
<g/>
,	,	kIx,	,
přišla	přijít	k5eAaPmAgFnS	přijít
jeho	jeho	k3xOp3gNnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
Margaret	Margareta	k1gFnPc2	Margareta
<g/>
.	.	kIx.	.
</s>
<s>
Angus	Angus	k1gInSc1	Angus
zkoušel	zkoušet	k5eAaImAgInS	zkoušet
také	také	k9	také
další	další	k2eAgInPc4d1	další
kostýmy	kostým	k1gInPc4	kostým
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Spider-Mana	Spider-Man	k1gMnSc4	Spider-Man
<g/>
,	,	kIx,	,
Zorra	Zorr	k1gMnSc4	Zorr
<g/>
,	,	kIx,	,
gorilu	gorila	k1gFnSc4	gorila
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
parodii	parodie	k1gFnSc4	parodie
na	na	k7c4	na
Supermana	superman	k1gMnSc4	superman
jménem	jméno	k1gNnSc7	jméno
Super-Ang	Super-Ang	k1gMnSc1	Super-Ang
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úplných	úplný	k2eAgInPc6d1	úplný
začátcích	začátek	k1gInPc6	začátek
také	také	k9	také
kapela	kapela	k1gFnSc1	kapela
experimentovala	experimentovat	k5eAaImAgFnS	experimentovat
se	s	k7c7	s
saténovým	saténový	k2eAgNnSc7d1	saténové
oblečením	oblečení	k1gNnSc7	oblečení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
této	tento	k3xDgFnSc2	tento
myšlenky	myšlenka	k1gFnSc2	myšlenka
se	se	k3xPyFc4	se
členové	člen	k1gMnPc1	člen
vzdali	vzdát	k5eAaPmAgMnP	vzdát
<g/>
,	,	kIx,	,
když	když	k8xS	když
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
stejně	stejně	k6eAd1	stejně
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
kapela	kapela	k1gFnSc1	kapela
Skyhooks	Skyhooksa	k1gFnPc2	Skyhooksa
z	z	k7c2	z
Melbourne	Melbourne	k1gNnSc2	Melbourne
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
Youngovi	Youngův	k2eAgMnPc1d1	Youngův
dospěli	dochvít	k5eAaPmAgMnP	dochvít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Evans	Evans	k1gInSc1	Evans
není	být	k5eNaImIp3nS	být
dobrým	dobrý	k2eAgInSc7d1	dobrý
frontmanem	frontman	k1gInSc7	frontman
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jim	on	k3xPp3gFnPc3	on
připadal	připadat	k5eAaImAgMnS	připadat
spíše	spíše	k9	spíše
jako	jako	k9	jako
glam	glam	k1gMnSc1	glam
rocker	rocker	k1gMnSc1	rocker
jako	jako	k8xS	jako
například	například	k6eAd1	například
Gary	Gary	k1gInPc4	Gary
Glitter	Glittrum	k1gNnPc2	Glittrum
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
koncertech	koncert	k1gInPc6	koncert
často	často	k6eAd1	často
místo	místo	k7c2	místo
Evanse	Evanse	k1gFnSc2	Evanse
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
první	první	k4xOgMnSc1	první
manažer	manažer	k1gMnSc1	manažer
kapely	kapela	k1gFnSc2	kapela
Dennis	Dennis	k1gFnSc2	Dennis
Laughlin	Laughlina	k1gFnPc2	Laughlina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
zpěvákem	zpěvák	k1gMnSc7	zpěvák
kapely	kapela	k1gFnSc2	kapela
Sherbet	Sherbeta	k1gFnPc2	Sherbeta
<g/>
.	.	kIx.	.
</s>
<s>
Evans	Evans	k1gInSc1	Evans
měl	mít	k5eAaImAgInS	mít
také	také	k9	také
s	s	k7c7	s
Laughlinem	Laughlin	k1gInSc7	Laughlin
osobní	osobní	k2eAgInPc4d1	osobní
spory	spor	k1gInPc4	spor
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
negativnímu	negativní	k2eAgInSc3d1	negativní
pohledu	pohled	k1gInSc3	pohled
ostatních	ostatní	k2eAgMnPc2d1	ostatní
členů	člen	k1gMnPc2	člen
kapely	kapela	k1gFnSc2	kapela
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
těchto	tento	k3xDgMnPc2	tento
sporů	spor	k1gInPc2	spor
byl	být	k5eAaImAgInS	být
Evans	Evans	k1gInSc1	Evans
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
vyhozen	vyhozen	k2eAgInSc4d1	vyhozen
a	a	k8xC	a
frontmanem	frontman	k1gMnSc7	frontman
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zkušený	zkušený	k2eAgMnSc1d1	zkušený
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
přítel	přítel	k1gMnSc1	přítel
George	Georg	k1gMnSc2	Georg
Younga	Young	k1gMnSc2	Young
Ronald	Ronald	k1gMnSc1	Ronald
Belford	Belford	k1gMnSc1	Belford
"	"	kIx"	"
<g/>
Bon	bon	k1gInSc1	bon
<g/>
"	"	kIx"	"
Scott	Scott	k1gMnSc1	Scott
<g/>
.	.	kIx.	.
</s>
<s>
Bon	bon	k1gInSc1	bon
Scott	Scotta	k1gFnPc2	Scotta
nahradil	nahradit	k5eAaPmAgInS	nahradit
Evanse	Evanse	k1gFnSc2	Evanse
v	v	k7c6	v
září	září	k1gNnSc6	září
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Scott	Scott	k1gMnSc1	Scott
předtím	předtím	k6eAd1	předtím
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
kapelách	kapela	k1gFnPc6	kapela
The	The	k1gFnSc2	The
Spektors	Spektors	k1gInSc1	Spektors
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
–	–	k?	–
<g/>
66	[number]	k4	66
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Valentines	Valentines	k1gMnSc1	Valentines
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
<g/>
)	)	kIx)	)
a	a	k8xC	a
Fraternity	fraternita	k1gFnSc2	fraternita
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
73	[number]	k4	73
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
nahrála	nahrát	k5eAaBmAgFnS	nahrát
s	s	k7c7	s
Evansem	Evans	k1gMnSc7	Evans
jen	jen	k6eAd1	jen
jeden	jeden	k4xCgInSc4	jeden
singl	singl	k1gInSc4	singl
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
"	"	kIx"	"
<g/>
Can	Can	k1gMnSc1	Can
I	i	k8xC	i
Sit	Sit	k1gMnSc1	Sit
Next	Next	k1gMnSc1	Next
to	ten	k3xDgNnSc4	ten
You	You	k1gFnSc1	You
<g/>
"	"	kIx"	"
<g/>
/	/	kIx~	/
<g/>
"	"	kIx"	"
<g/>
Rockin	Rockin	k1gInSc1	Rockin
<g/>
'	'	kIx"	'
in	in	k?	in
the	the	k?	the
Parlour	Parloura	k1gFnPc2	Parloura
<g/>
"	"	kIx"	"
a	a	k8xC	a
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Can	Can	k1gMnSc1	Can
I	i	k8xC	i
Sit	Sit	k1gMnSc1	Sit
Next	Next	k1gMnSc1	Next
to	ten	k3xDgNnSc4	ten
You	You	k1gFnSc1	You
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
nahrána	nahrát	k5eAaPmNgFnS	nahrát
znovu	znovu	k6eAd1	znovu
se	s	k7c7	s
Scottem	Scott	k1gInSc7	Scott
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Can	Can	k1gMnSc1	Can
I	i	k8xC	i
Sit	Sit	k1gMnSc1	Sit
Next	Next	k1gMnSc1	Next
to	ten	k3xDgNnSc4	ten
You	You	k1gFnSc7	You
Girl	girl	k1gFnSc2	girl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
High	High	k1gInSc1	High
Voltage	Voltag	k1gInSc2	Voltag
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
jen	jen	k6eAd1	jen
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
nahrávání	nahrávání	k1gNnSc1	nahrávání
trvalo	trvat	k5eAaImAgNnS	trvat
jen	jen	k9	jen
deset	deset	k4xCc1	deset
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
složili	složit	k5eAaPmAgMnP	složit
bratři	bratr	k1gMnPc1	bratr
Youngovi	Youngův	k2eAgMnPc1d1	Youngův
a	a	k8xC	a
texty	text	k1gInPc4	text
napsal	napsat	k5eAaBmAgMnS	napsat
Scott	Scott	k1gMnSc1	Scott
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
měsíců	měsíc	k1gInPc2	měsíc
se	se	k3xPyFc4	se
stabilizovala	stabilizovat	k5eAaBmAgFnS	stabilizovat
sestava	sestava	k1gFnSc1	sestava
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
tvořil	tvořit	k5eAaImAgInS	tvořit
Scott	Scott	k1gInSc1	Scott
<g/>
,	,	kIx,	,
bratři	bratr	k1gMnPc1	bratr
Youngovi	Youngův	k2eAgMnPc1d1	Youngův
<g/>
,	,	kIx,	,
baskytarista	baskytarista	k1gMnSc1	baskytarista
Mark	Mark	k1gMnSc1	Mark
Evans	Evans	k1gInSc1	Evans
a	a	k8xC	a
bubeník	bubeník	k1gMnSc1	bubeník
Phil	Phil	k1gMnSc1	Phil
Rudd	Rudd	k1gMnSc1	Rudd
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
také	také	k9	také
kapela	kapela	k1gFnSc1	kapela
vydala	vydat	k5eAaPmAgFnS	vydat
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
It	It	k1gFnSc4	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
a	a	k8xC	a
Long	Long	k1gMnSc1	Long
Way	Way	k1gMnSc1	Way
to	ten	k3xDgNnSc4	ten
the	the	k?	the
Top	topit	k5eAaImRp2nS	topit
(	(	kIx(	(
<g/>
If	If	k1gFnSc6	If
You	You	k1gFnPc2	You
Wanna	Wanna	k1gFnSc1	Wanna
Rock	rock	k1gInSc1	rock
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
Roll	Roll	k1gInSc4	Roll
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
populární	populární	k2eAgFnSc7d1	populární
rockovou	rockový	k2eAgFnSc7d1	rocková
hymnou	hymna	k1gFnSc7	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
byla	být	k5eAaImAgFnS	být
zařazena	zařadit	k5eAaPmNgFnS	zařadit
na	na	k7c4	na
T.	T.	kA	T.
<g/>
N.	N.	kA	N.
<g/>
T.	T.	kA	T.
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
také	také	k9	také
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
další	další	k2eAgInSc4d1	další
velký	velký	k2eAgInSc4d1	velký
hit	hit	k1gInSc4	hit
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
High	High	k1gInSc1	High
Voltage	Voltag	k1gInSc2	Voltag
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1974	[number]	k4	1974
a	a	k8xC	a
1977	[number]	k4	1977
kapela	kapela	k1gFnSc1	kapela
pravidelně	pravidelně	k6eAd1	pravidelně
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Molly	Molla	k1gFnSc2	Molla
Meldrum	Meldrum	k1gInSc1	Meldrum
"	"	kIx"	"
<g/>
Countdown	Countdown	k1gInSc1	Countdown
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
celostátním	celostátní	k2eAgInSc6d1	celostátní
pořadu	pořad	k1gInSc6	pořad
o	o	k7c6	o
populární	populární	k2eAgFnSc6d1	populární
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
a	a	k8xC	a
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stávali	stávat	k5eAaImAgMnP	stávat
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgFnPc2d3	nejpopulárnější
australských	australský	k2eAgFnPc2d1	australská
kapel	kapela	k1gFnPc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Vystoupení	vystoupení	k1gNnSc1	vystoupení
ze	z	k7c2	z
dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1977	[number]	k4	1977
bylo	být	k5eAaImAgNnS	být
jejich	jejich	k3xOp3gNnSc7	jejich
posledním	poslední	k2eAgNnSc7d1	poslední
televizním	televizní	k2eAgNnSc7d1	televizní
vystoupením	vystoupení	k1gNnSc7	vystoupení
na	na	k7c6	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
podepsala	podepsat	k5eAaPmAgFnS	podepsat
kapela	kapela	k1gFnSc1	kapela
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Atlantic	Atlantice	k1gFnPc2	Atlantice
Records	Records	k1gInSc4	Records
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Získali	získat	k5eAaPmAgMnP	získat
neocenitelné	ocenitelný	k2eNgFnPc4d1	neocenitelná
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
vystupováním	vystupování	k1gNnSc7	vystupování
na	na	k7c6	na
stadionech	stadion	k1gInPc6	stadion
<g/>
,	,	kIx,	,
když	když	k8xS	když
dělali	dělat	k5eAaImAgMnP	dělat
předkapely	předkapela	k1gFnPc4	předkapela
skupinám	skupina	k1gFnPc3	skupina
Kiss	Kiss	k1gInSc4	Kiss
<g/>
,	,	kIx,	,
Aerosmith	Aerosmith	k1gInSc4	Aerosmith
<g/>
,	,	kIx,	,
Styx	Styx	k1gInSc4	Styx
a	a	k8xC	a
Blue	Blue	k1gInSc4	Blue
Öyster	Öystra	k1gFnPc2	Öystra
Cult	Culta	k1gFnPc2	Culta
a	a	k8xC	a
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
jako	jako	k9	jako
hlavní	hlavní	k2eAgFnPc4d1	hlavní
hvězdy	hvězda	k1gFnPc4	hvězda
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
Cheap	Cheap	k1gMnSc1	Cheap
Trick	Trick	k1gMnSc1	Trick
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
mezinárodně	mezinárodně	k6eAd1	mezinárodně
distribuované	distribuovaný	k2eAgNnSc4d1	distribuované
album	album	k1gNnSc4	album
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
byla	být	k5eAaImAgFnS	být
kompilace	kompilace	k1gFnSc1	kompilace
z	z	k7c2	z
alb	album	k1gNnPc2	album
High	Higha	k1gFnPc2	Higha
Voltage	Voltag	k1gInSc2	Voltag
a	a	k8xC	a
T.	T.	kA	T.
<g/>
N.	N.	kA	N.
<g/>
T.	T.	kA	T.
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
High	High	k1gInSc4	High
Voltage	Voltag	k1gInSc2	Voltag
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
pod	pod	k7c7	pod
labelem	label	k1gInSc7	label
Atlantic	Atlantice	k1gFnPc2	Atlantice
Records	Recordsa	k1gFnPc2	Recordsa
a	a	k8xC	a
prodaly	prodat	k5eAaPmAgFnP	prodat
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
tři	tři	k4xCgNnPc4	tři
miliony	milion	k4xCgInPc4	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
zaměřeno	zaměřit	k5eAaPmNgNnS	zaměřit
především	především	k9	především
na	na	k7c4	na
T.	T.	kA	T.
<g/>
N.	N.	kA	N.
<g/>
T.	T.	kA	T.
a	a	k8xC	a
z	z	k7c2	z
předešlého	předešlý	k2eAgNnSc2d1	předešlé
alba	album	k1gNnSc2	album
High	High	k1gInSc1	High
Voltage	Voltage	k1gFnSc4	Voltage
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
byly	být	k5eAaImAgInP	být
obsaženy	obsáhnout	k5eAaPmNgInP	obsáhnout
jen	jen	k9	jen
dvě	dva	k4xCgFnPc4	dva
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
Dirty	Dirta	k1gFnSc2	Dirta
Deeds	Deedsa	k1gFnPc2	Deedsa
Done	Don	k1gInSc5	Don
Dirt	Dirta	k1gFnPc2	Dirta
Cheap	Cheap	k1gInSc1	Cheap
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
jak	jak	k8xC	jak
v	v	k7c4	v
australské	australský	k2eAgNnSc4d1	Australské
<g/>
,	,	kIx,	,
tak	tak	k9	tak
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
verzi	verze	k1gFnSc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
dvou	dva	k4xCgFnPc6	dva
verzích	verze	k1gFnPc6	verze
se	se	k3xPyFc4	se
lišil	lišit	k5eAaImAgMnS	lišit
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
verze	verze	k1gFnSc1	verze
například	například	k6eAd1	například
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Rocker	rocker	k1gMnSc1	rocker
<g/>
"	"	kIx"	"
z	z	k7c2	z
alba	album	k1gNnSc2	album
T.	T.	kA	T.
<g/>
N.	N.	kA	N.
<g/>
T.	T.	kA	T.
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
australská	australský	k2eAgFnSc1d1	australská
verze	verze	k1gFnSc1	verze
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
například	například	k6eAd1	například
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Jailbreak	Jailbreak	k1gInSc1	Jailbreak
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Dirty	Dirta	k1gFnSc2	Dirta
Deeds	Deedsa	k1gFnPc2	Deedsa
nevyšlo	vyjít	k5eNaPmAgNnS	vyjít
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
kapela	kapela	k1gFnSc1	kapela
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
své	svůj	k3xOyFgFnSc2	svůj
popularity	popularita	k1gFnSc2	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
Let	léto	k1gNnPc2	léto
There	Ther	k1gInSc5	Ther
Be	Be	k1gMnSc4	Be
Rock	rock	k1gInSc1	rock
byl	být	k5eAaImAgInS	být
vyhozen	vyhozen	k2eAgMnSc1d1	vyhozen
baskytarista	baskytarista	k1gMnSc1	baskytarista
Mark	Mark	k1gMnSc1	Mark
Evans	Evansa	k1gFnPc2	Evansa
pro	pro	k7c4	pro
osobní	osobní	k2eAgInPc4d1	osobní
spory	spor	k1gInPc4	spor
s	s	k7c7	s
Angusem	Angus	k1gMnSc7	Angus
Youngem	Young	k1gInSc7	Young
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gInSc4	on
Cliff	Cliff	k1gInSc4	Cliff
Williams	Williamsa	k1gFnPc2	Williamsa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Malcolmem	Malcolmo	k1gNnSc7	Malcolmo
Youngem	Young	k1gInSc7	Young
později	pozdě	k6eAd2	pozdě
zpíval	zpívat	k5eAaImAgMnS	zpívat
doprovodné	doprovodný	k2eAgInPc4d1	doprovodný
vokály	vokál	k1gInPc4	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
Youngovi	Youngův	k2eAgMnPc1d1	Youngův
Evansův	Evansův	k2eAgInSc4d1	Evansův
odchod	odchod	k1gInSc4	odchod
nijak	nijak	k6eAd1	nijak
nerozváděli	rozvádět	k5eNaImAgMnP	rozvádět
a	a	k8xC	a
nevyjadřovali	vyjadřovat	k5eNaImAgMnP	vyjadřovat
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Richard	Richard	k1gMnSc1	Richard
Griffiths	Griffiths	k1gInSc1	Griffiths
<g/>
,	,	kIx,	,
šéf	šéf	k1gMnSc1	šéf
Epic	Epic	k1gFnSc4	Epic
Records	Recordsa	k1gFnPc2	Recordsa
a	a	k8xC	a
agent	agent	k1gMnSc1	agent
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Věděli	vědět	k5eAaImAgMnP	vědět
jste	být	k5eAaImIp2nP	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mark	Mark	k1gMnSc1	Mark
nevydrží	vydržet	k5eNaPmIp3nS	vydržet
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
prostě	prostě	k9	prostě
moc	moc	k6eAd1	moc
hodný	hodný	k2eAgMnSc1d1	hodný
kluk	kluk	k1gMnSc1	kluk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
měli	mít	k5eAaImAgMnP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
formování	formování	k1gNnSc4	formování
NWOBHM	NWOBHM	kA	NWOBHM
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kapel	kapela	k1gFnPc2	kapela
Saxon	Saxon	k1gInSc1	Saxon
a	a	k8xC	a
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
úpadek	úpadek	k1gInSc4	úpadek
tradičních	tradiční	k2eAgFnPc2d1	tradiční
heavy	heav	k1gInPc7	heav
metalových	metalový	k2eAgFnPc2d1	metalová
kapel	kapela	k1gFnPc2	kapela
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
označili	označit	k5eAaPmAgMnP	označit
kritici	kritik	k1gMnPc1	kritik
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Thin	Thin	k1gInSc1	Thin
Lizzy	Lizza	k1gFnPc1	Lizza
<g/>
,	,	kIx,	,
UFO	UFO	kA	UFO
<g/>
,	,	kIx,	,
Scorpions	Scorpionsa	k1gFnPc2	Scorpionsa
a	a	k8xC	a
Judas	Judasa	k1gFnPc2	Judasa
Priest	Priest	k1gFnSc1	Priest
za	za	k7c4	za
"	"	kIx"	"
<g/>
druhou	druhý	k4xOgFnSc4	druhý
generaci	generace	k1gFnSc4	generace
stoupajících	stoupající	k2eAgFnPc2d1	stoupající
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
připravených	připravený	k2eAgFnPc2d1	připravená
prorazit	prorazit	k5eAaPmF	prorazit
<g/>
,	,	kIx,	,
když	když	k8xS	když
stará	starý	k2eAgFnSc1d1	stará
garda	garda	k1gFnSc1	garda
začala	začít	k5eAaPmAgFnS	začít
upadat	upadat	k5eAaImF	upadat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
První	první	k4xOgNnPc4	první
vystoupení	vystoupení	k1gNnPc4	vystoupení
kapely	kapela	k1gFnSc2	kapela
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
zařídilo	zařídit	k5eAaPmAgNnS	zařídit
michiganské	michiganský	k2eAgNnSc4d1	Michiganské
rádio	rádio	k1gNnSc4	rádio
AM	AM	kA	AM
600	[number]	k4	600
WTAC	WTAC	kA	WTAC
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Manažer	manažer	k1gMnSc1	manažer
rádia	rádius	k1gInSc2	rádius
Peter	Peter	k1gMnSc1	Peter
C.	C.	kA	C.
Cavanaugh	Cavanaugh	k1gMnSc1	Cavanaugh
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
vystoupení	vystoupení	k1gNnSc4	vystoupení
v	v	k7c4	v
Capitol	Capitol	k1gInSc4	Capitol
Theater	Theatra	k1gFnPc2	Theatra
ve	v	k7c6	v
Flintu	flint	k1gInSc6	flint
<g/>
.	.	kIx.	.
</s>
<s>
Předkapelou	předkapela	k1gFnSc7	předkapela
byli	být	k5eAaImAgMnP	být
MC	MC	kA	MC
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
příležitost	příležitost	k1gFnSc4	příležitost
opět	opět	k6eAd1	opět
sjednotili	sjednotit	k5eAaPmAgMnP	sjednotit
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
začal	začít	k5eAaPmAgInS	začít
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
Live	Live	k1gInSc1	Live
Wire	Wir	k1gInSc2	Wir
<g/>
"	"	kIx"	"
a	a	k8xC	a
skončil	skončit	k5eAaPmAgMnS	skončit
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
a	a	k8xC	a
Long	Long	k1gMnSc1	Long
Way	Way	k1gMnSc1	Way
to	ten	k3xDgNnSc4	ten
the	the	k?	the
Top	topit	k5eAaImRp2nS	topit
(	(	kIx(	(
<g/>
If	If	k1gFnSc6	If
You	You	k1gFnPc2	You
Wanna	Wanna	k1gFnSc1	Wanna
Rock	rock	k1gInSc1	rock
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
Roll	Roll	k1gInSc4	Roll
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
začali	začít	k5eAaPmAgMnP	začít
být	být	k5eAaImF	být
britskými	britský	k2eAgMnPc7d1	britský
médií	médium	k1gNnPc2	médium
ztotožňováni	ztotožňovat	k5eAaImNgMnP	ztotožňovat
s	s	k7c7	s
punkovým	punkový	k2eAgNnSc7d1	punkové
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
popularitě	popularita	k1gFnSc3	popularita
však	však	k9	však
úspěšně	úspěšně	k6eAd1	úspěšně
přečkali	přečkat	k5eAaPmAgMnP	přečkat
pozdvižení	pozdvižení	k1gNnSc4	pozdvižení
okolo	okolo	k7c2	okolo
punku	punk	k1gInSc2	punk
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
začali	začít	k5eAaPmAgMnP	začít
získávat	získávat	k5eAaImF	získávat
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
status	status	k1gInSc4	status
kultovní	kultovní	k2eAgFnSc2d1	kultovní
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
Powerage	Powerag	k1gInSc2	Powerag
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgNnSc6	který
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
podílel	podílet	k5eAaImAgMnS	podílet
baskytarista	baskytarista	k1gMnSc1	baskytarista
Cliff	Cliff	k1gMnSc1	Cliff
Williams	Williams	k1gInSc1	Williams
a	a	k8xC	a
které	který	k3yQgInPc4	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
ostřejší	ostrý	k2eAgInPc4d2	ostřejší
riffy	riff	k1gInPc4	riff
než	než	k8xS	než
předchozí	předchozí	k2eAgNnSc4d1	předchozí
album	album	k1gNnSc4	album
Let	léto	k1gNnPc2	léto
There	Ther	k1gInSc5	Ther
Be	Be	k1gMnSc4	Be
Rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
alba	album	k1gNnSc2	album
Powerage	Powerag	k1gFnSc2	Powerag
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc1	jeden
singl	singl	k1gInSc1	singl
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
"	"	kIx"	"
<g/>
Rock	rock	k1gInSc1	rock
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
Roll	Rolla	k1gFnPc2	Rolla
Damnation	Damnation	k1gInSc1	Damnation
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
zatím	zatím	k6eAd1	zatím
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
singlem	singl	k1gInSc7	singl
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
24	[number]	k4	24
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
v	v	k7c4	v
Apollo	Apollo	k1gNnSc4	Apollo
Theatre	Theatr	k1gInSc5	Theatr
v	v	k7c6	v
Glasgow	Glasgow	k1gNnSc6	Glasgow
byl	být	k5eAaImAgInS	být
nahrán	nahrát	k5eAaBmNgInS	nahrát
a	a	k8xC	a
vydán	vydat	k5eAaPmNgInS	vydat
jako	jako	k9	jako
album	album	k1gNnSc1	album
If	If	k1gFnPc2	If
You	You	k1gFnPc2	You
Want	Want	k2eAgInSc4d1	Want
Blood	Blood	k1gInSc4	Blood
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
jak	jak	k6eAd1	jak
klasické	klasický	k2eAgFnPc4d1	klasická
písně	píseň	k1gFnPc4	píseň
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Whole	Whole	k1gNnSc1	Whole
Lotta	Lott	k1gInSc2	Lott
Rosie	Rosi	k1gMnPc4	Rosi
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Problem	Probl	k1gMnSc7	Probl
Child	Child	k1gMnSc1	Child
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
"	"	kIx"	"
<g/>
Let	let	k1gInSc1	let
There	Ther	k1gInSc5	Ther
Be	Be	k1gMnSc1	Be
Rock	rock	k1gInSc1	rock
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
méně	málo	k6eAd2	málo
známé	známý	k2eAgFnPc4d1	známá
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Riff	Riff	k1gMnSc1	Riff
Raff	Raff	k1gMnSc1	Raff
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
poslední	poslední	k2eAgNnSc4d1	poslední
album	album	k1gNnSc4	album
kapely	kapela	k1gFnSc2	kapela
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
se	s	k7c7	s
Scottem	Scotto	k1gNnSc7	Scotto
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
produkovali	produkovat	k5eAaImAgMnP	produkovat
Harry	Harr	k1gInPc4	Harr
Vanda	Vanda	k1gFnSc1	Vanda
a	a	k8xC	a
George	George	k1gFnSc1	George
Young	Younga	k1gFnPc2	Younga
<g/>
.	.	kIx.	.
</s>
<s>
Šesté	šestý	k4xOgNnSc1	šestý
album	album	k1gNnSc1	album
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
Highway	Highwa	k2eAgMnPc4d1	Highwa
to	ten	k3xDgNnSc1	ten
Hell	Hell	k1gInSc4	Hell
<g/>
,	,	kIx,	,
produkoval	produkovat	k5eAaImAgMnS	produkovat
Mutt	Mutt	k1gMnSc1	Mutt
Lange	Lang	k1gFnSc2	Lang
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
mezi	mezi	k7c4	mezi
100	[number]	k4	100
nejprodávanějších	prodávaný	k2eAgNnPc2d3	nejprodávanější
alb	album	k1gNnPc2	album
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
17	[number]	k4	17
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
posunulo	posunout	k5eAaPmAgNnS	posunout
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
mezi	mezi	k7c4	mezi
nejpopulárnější	populární	k2eAgInSc4d3	nejpopulárnější
hard	hard	k1gInSc4	hard
rockové	rockový	k2eAgFnSc2d1	rocková
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Highway	Highwa	k1gMnPc4	Highwa
to	ten	k3xDgNnSc1	ten
Hell	Hell	k1gInSc4	Hell
klade	klást	k5eAaImIp3nS	klást
větší	veliký	k2eAgInSc4d2	veliký
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
doprovodné	doprovodný	k2eAgInPc4d1	doprovodný
vokály	vokál	k1gInPc4	vokál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
má	mít	k5eAaImIp3nS	mít
zvuk	zvuk	k1gInSc4	zvuk
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
pro	pro	k7c4	pro
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
rychlé	rychlý	k2eAgInPc4d1	rychlý
<g/>
,	,	kIx,	,
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
a	a	k8xC	a
úderné	úderný	k2eAgInPc4d1	úderný
riffy	riff	k1gInPc4	riff
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1980	[number]	k4	1980
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
Bon	bon	k1gInSc1	bon
Scott	Scott	k1gInSc1	Scott
po	po	k7c6	po
zkonzumování	zkonzumování	k1gNnSc6	zkonzumování
značného	značný	k2eAgNnSc2d1	značné
množství	množství	k1gNnSc2	množství
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
známý	známý	k2eAgMnSc1d1	známý
Alistair	Alistair	k1gMnSc1	Alistair
Kinnear	Kinnear	k1gMnSc1	Kinnear
jej	on	k3xPp3gNnSc4	on
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
nechal	nechat	k5eAaPmAgMnS	nechat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
autě	auto	k1gNnSc6	auto
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
také	také	k9	také
ráno	ráno	k6eAd1	ráno
nalezl	nalézt	k5eAaBmAgInS	nalézt
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
odvezl	odvézt	k5eAaPmAgMnS	odvézt
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gNnSc7	College
v	v	k7c6	v
Camberwellu	Camberwell	k1gInSc6	Camberwell
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
Scott	Scott	k1gMnSc1	Scott
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
často	často	k6eAd1	často
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
udušení	udušení	k1gNnSc1	udušení
vdechnutými	vdechnutý	k2eAgInPc7d1	vdechnutý
zvratky	zvratek	k1gInPc7	zvratek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
oficiální	oficiální	k2eAgInSc4d1	oficiální
důvod	důvod	k1gInSc4	důvod
úmrtí	úmrtí	k1gNnSc2	úmrtí
byla	být	k5eAaImAgFnS	být
uvedena	uveden	k2eAgFnSc1d1	uvedena
otrava	otrava	k1gFnSc1	otrava
alkoholem	alkohol	k1gInSc7	alkohol
a	a	k8xC	a
nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
náhoda	náhoda	k1gFnSc1	náhoda
<g/>
.	.	kIx.	.
</s>
<s>
Scott	Scott	k1gInSc1	Scott
byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgInS	pohřbít
ve	v	k7c4	v
Fremantle	Fremantle	k1gFnPc4	Fremantle
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
nejasnosti	nejasnost	k1gFnPc1	nejasnost
oficiální	oficiální	k2eAgFnSc2d1	oficiální
verze	verze	k1gFnSc2	verze
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
terčem	terč	k1gInSc7	terč
stoupenců	stoupenec	k1gMnPc2	stoupenec
konspiračních	konspirační	k2eAgFnPc2d1	konspirační
teorií	teorie	k1gFnPc2	teorie
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Scott	Scott	k1gMnSc1	Scott
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
předávkování	předávkování	k1gNnPc4	předávkování
heroinem	heroin	k1gInSc7	heroin
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zabit	zabít	k5eAaPmNgInS	zabít
výfukovými	výfukový	k2eAgInPc7d1	výfukový
plyny	plyn	k1gInPc7	plyn
přiváděnými	přiváděný	k2eAgInPc7d1	přiváděný
do	do	k7c2	do
auta	auto	k1gNnSc2	auto
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
že	že	k8xS	že
Kinnear	Kinnear	k1gInSc1	Kinnear
neexistoval	existovat	k5eNaImAgInS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Scott	Scott	k1gMnSc1	Scott
byl	být	k5eAaImAgMnS	být
navíc	navíc	k6eAd1	navíc
astmatik	astmatik	k1gMnSc1	astmatik
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
byla	být	k5eAaImAgFnS	být
v	v	k7c4	v
den	den	k1gInSc4	den
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
členy	člen	k1gMnPc7	člen
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Malcom	Malcom	k1gInSc4	Malcom
Young	Young	k1gMnSc1	Young
a	a	k8xC	a
Phil	Phil	k1gMnSc1	Phil
Rudd	Rudd	k1gMnSc1	Rudd
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
z	z	k7c2	z
alkoholismu	alkoholismus	k1gInSc2	alkoholismus
léčit	léčit	k5eAaImF	léčit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Scottově	Scottův	k2eAgNnSc6d1	Scottovo
úmrtí	úmrtí	k1gNnSc6	úmrtí
krátce	krátce	k6eAd1	krátce
kapela	kapela	k1gFnSc1	kapela
zvažovala	zvažovat	k5eAaImAgFnS	zvažovat
ukončení	ukončení	k1gNnSc4	ukončení
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
nakonec	nakonec	k6eAd1	nakonec
dospěli	dochvít	k5eAaPmAgMnP	dochvít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Scott	Scott	k1gMnSc1	Scott
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
se	se	k3xPyFc4	se
o	o	k7c6	o
několika	několik	k4yIc6	několik
možných	možný	k2eAgMnPc6d1	možný
kandidátech	kandidát	k1gMnPc6	kandidát
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
by	by	kYmCp3nP	by
jej	on	k3xPp3gMnSc4	on
mohli	moct	k5eAaImAgMnP	moct
nahradit	nahradit	k5eAaPmF	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgMnS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
zpěvák	zpěvák	k1gMnSc1	zpěvák
kapely	kapela	k1gFnSc2	kapela
Back	Back	k1gMnSc1	Back
Street	Street	k1gMnSc1	Street
Crawler	Crawler	k1gMnSc1	Crawler
Terry	Terra	k1gFnSc2	Terra
Slesser	Slesser	k1gMnSc1	Slesser
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
nevstoupit	vstoupit	k5eNaPmF	vstoupit
do	do	k7c2	do
již	již	k6eAd1	již
zavedené	zavedený	k2eAgFnSc2d1	zavedená
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgInS	zahájit
svou	svůj	k3xOyFgFnSc4	svůj
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
sólovou	sólový	k2eAgFnSc4d1	sólová
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
,	,	kIx,	,
Buzz	Buzz	k1gMnSc1	Buzz
Sherman	Sherman	k1gMnSc1	Sherman
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
kapely	kapela	k1gFnSc2	kapela
Moxy	Moxa	k1gFnSc2	Moxa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
připojit	připojit	k5eAaPmF	připojit
kvůli	kvůli	k7c3	kvůli
problémů	problém	k1gInPc2	problém
s	s	k7c7	s
hlasem	hlas	k1gInSc7	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgMnPc1d1	zbývající
členové	člen	k1gMnPc1	člen
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pro	pro	k7c4	pro
bývalého	bývalý	k2eAgMnSc4d1	bývalý
člena	člen	k1gMnSc4	člen
skupiny	skupina	k1gFnSc2	skupina
Geordie	Geordie	k1gFnSc1	Geordie
<g/>
,	,	kIx,	,
Briana	Briana	k1gFnSc1	Briana
Johnsona	Johnsona	k1gFnSc1	Johnsona
<g/>
.	.	kIx.	.
</s>
<s>
Angus	Angus	k1gMnSc1	Angus
Young	Young	k1gMnSc1	Young
později	pozdě	k6eAd2	pozdě
vzpomínal	vzpomínat	k5eAaImAgMnS	vzpomínat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pamatuju	pamatovat	k5eAaImIp1nS	pamatovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mi	já	k3xPp1nSc3	já
Bon	bon	k1gInSc1	bon
pouštěl	pouštět	k5eAaImAgInS	pouštět
písně	píseň	k1gFnPc4	píseň
od	od	k7c2	od
Little	Little	k1gFnSc2	Little
Richarda	Richard	k1gMnSc2	Richard
a	a	k8xC	a
poté	poté	k6eAd1	poté
mi	já	k3xPp1nSc3	já
popisoval	popisovat	k5eAaImAgMnS	popisovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
viděl	vidět	k5eAaImAgInS	vidět
Briana	Brian	k1gMnSc4	Brian
zpívat	zpívat	k5eAaImF	zpívat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
koncertě	koncert	k1gInSc6	koncert
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ten	ten	k3xDgInSc1	ten
týpek	týpek	k1gInSc1	týpek
řval	řvát	k5eAaImAgInS	řvát
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jen	jen	k9	jen
jeho	jeho	k3xOp3gFnPc1	jeho
plíce	plíce	k1gFnPc1	plíce
dovolovaly	dovolovat	k5eAaImAgFnP	dovolovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
si	se	k3xPyFc3	se
pamatuju	pamatovat	k5eAaImIp1nS	pamatovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
sebou	se	k3xPyFc7	se
švihl	švihnout	k5eAaPmAgMnS	švihnout
o	o	k7c4	o
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
válel	válet	k5eAaImAgMnS	válet
se	se	k3xPyFc4	se
a	a	k8xC	a
křičel	křičet	k5eAaImAgMnS	křičet
<g/>
.	.	kIx.	.
</s>
<s>
Přišlo	přijít	k5eAaPmAgNnS	přijít
mi	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc1	ten
skvělé	skvělý	k2eAgNnSc1d1	skvělé
a	a	k8xC	a
jako	jako	k9	jako
vrchol	vrchol	k1gInSc4	vrchol
všeho	všecek	k3xTgNnSc2	všecek
–	–	k?	–
nemohlo	moct	k5eNaImAgNnS	moct
to	ten	k3xDgNnSc1	ten
mít	mít	k5eAaImF	mít
lepší	dobrý	k2eAgInSc4d2	lepší
přídavek	přídavek	k1gInSc4	přídavek
–	–	k?	–
toho	ten	k3xDgNnSc2	ten
týpka	týpko	k1gNnSc2	týpko
odvezli	odvézt	k5eAaPmAgMnP	odvézt
na	na	k7c6	na
vozíku	vozík	k1gInSc6	vozík
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Později	pozdě	k6eAd2	pozdě
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
byl	být	k5eAaImAgInS	být
Johnsonovi	Johnson	k1gMnSc3	Johnson
diagnostikován	diagnostikován	k2eAgInSc4d1	diagnostikován
zánět	zánět	k1gInSc4	zánět
slepého	slepý	k2eAgNnSc2d1	slepé
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
příčinou	příčina	k1gFnSc7	příčina
jeho	jeho	k3xOp3gNnSc2	jeho
svíjení	svíjení	k1gNnSc2	svíjení
se	se	k3xPyFc4	se
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konkurzu	konkurz	k1gInSc6	konkurz
zazpíval	zazpívat	k5eAaPmAgInS	zazpívat
Johnson	Johnson	k1gInSc1	Johnson
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Whole	Whole	k1gInSc1	Whole
Lotta	Lott	k1gInSc2	Lott
Rosie	Rosi	k1gMnPc4	Rosi
<g/>
"	"	kIx"	"
z	z	k7c2	z
alba	album	k1gNnSc2	album
Let	léto	k1gNnPc2	léto
There	Ther	k1gInSc5	Ther
Be	Be	k1gFnPc6	Be
Rock	rock	k1gInSc1	rock
a	a	k8xC	a
"	"	kIx"	"
<g/>
Nutbush	Nutbush	k1gInSc1	Nutbush
City	city	k1gNnSc1	city
Limits	Limitsa	k1gFnPc2	Limitsa
<g/>
"	"	kIx"	"
od	od	k7c2	od
Ikea	Ike	k1gInSc2	Ike
a	a	k8xC	a
Tiny	Tina	k1gFnSc2	Tina
Turnerových	turnerův	k2eAgMnPc2d1	turnerův
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
konkurzu	konkurz	k1gInSc6	konkurz
byl	být	k5eAaImAgInS	být
Johnson	Johnson	k1gInSc1	Johnson
přijat	přijmout	k5eAaPmNgInS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Johnsonem	Johnson	k1gInSc7	Johnson
dokončila	dokončit	k5eAaPmAgFnS	dokončit
kapela	kapela	k1gFnSc1	kapela
písně	píseň	k1gFnSc2	píseň
pro	pro	k7c4	pro
album	album	k1gNnSc4	album
Back	Back	k1gMnSc1	Back
in	in	k?	in
Black	Black	k1gMnSc1	Black
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
začala	začít	k5eAaPmAgFnS	začít
psát	psát	k5eAaImF	psát
ještě	ještě	k9	ještě
za	za	k7c2	za
Scottova	Scottův	k2eAgInSc2d1	Scottův
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávání	nahrávání	k1gNnSc1	nahrávání
probíhalo	probíhat	k5eAaImAgNnS	probíhat
v	v	k7c4	v
Compass	Compass	k1gInSc4	Compass
Point	pointa	k1gFnPc2	pointa
Studios	Studios	k?	Studios
na	na	k7c6	na
Bahamách	Bahamy	k1gFnPc6	Bahamy
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
Scottově	Scottův	k2eAgFnSc6d1	Scottova
smrti	smrt	k1gFnSc6	smrt
a	a	k8xC	a
produkoval	produkovat	k5eAaImAgMnS	produkovat
jej	on	k3xPp3gInSc4	on
Mutt	Mutt	k2eAgInSc4d1	Mutt
Lange	Lange	k1gInSc4	Lange
<g/>
.	.	kIx.	.
</s>
<s>
Back	Back	k6eAd1	Back
in	in	k?	in
Black	Blacka	k1gFnPc2	Blacka
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejúspěšnějším	úspěšný	k2eAgNnPc3d3	nejúspěšnější
albem	album	k1gNnSc7	album
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
a	a	k8xC	a
také	také	k9	také
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
hard	harda	k1gFnPc2	harda
rockových	rockový	k2eAgFnPc2d1	rocková
alb	alba	k1gFnPc2	alba
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
několik	několik	k4yIc1	několik
z	z	k7c2	z
nejpopulárnjších	jpopulárnjší	k2eNgFnPc2d1	jpopulárnjší
hard	harda	k1gFnPc2	harda
rockových	rockový	k2eAgFnPc2d1	rocková
písní	píseň	k1gFnPc2	píseň
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Hells	Hells	k1gInSc1	Hells
Bells	Bells	k1gInSc1	Bells
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
You	You	k1gFnSc1	You
Shook	Shook	k1gInSc1	Shook
Me	Me	k1gMnSc1	Me
All	All	k1gMnSc1	All
Night	Night	k1gMnSc1	Night
Long	Long	k1gMnSc1	Long
<g/>
"	"	kIx"	"
a	a	k8xC	a
titulní	titulní	k2eAgFnSc4d1	titulní
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Back	Back	k1gInSc1	Back
in	in	k?	in
Black	Black	k1gInSc1	Black
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
získalo	získat	k5eAaPmAgNnS	získat
platinovou	platinový	k2eAgFnSc4d1	platinová
desku	deska	k1gFnSc4	deska
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc4	on
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgMnPc2d1	americký
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
21	[number]	k4	21
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
<g/>
Celkově	celkově	k6eAd1	celkově
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
kopí	kopit	k5eAaImIp3nS	kopit
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
britském	britský	k2eAgInSc6d1	britský
žebříčku	žebříček	k1gInSc6	žebříček
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
album	album	k1gNnSc1	album
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnSc1d1	následující
album	album	k1gNnSc1	album
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
For	forum	k1gNnPc2	forum
Those	Thos	k1gInSc5	Thos
About	About	k2eAgInSc1d1	About
to	ten	k3xDgNnSc1	ten
Rock	rock	k1gInSc4	rock
We	We	k1gMnSc2	We
Salute	salut	k1gInSc5	salut
You	You	k1gMnSc6	You
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
také	také	k9	také
prodávalo	prodávat	k5eAaImAgNnS	prodávat
dobře	dobře	k6eAd1	dobře
a	a	k8xC	a
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
se	se	k3xPyFc4	se
i	i	k9	i
pozitivních	pozitivní	k2eAgInPc2d1	pozitivní
ohlasů	ohlas	k1gInPc2	ohlas
u	u	k7c2	u
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
alba	album	k1gNnSc2	album
byly	být	k5eAaImAgFnP	být
vybrány	vybrat	k5eAaPmNgFnP	vybrat
dvě	dva	k4xCgFnPc1	dva
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyšly	vyjít	k5eAaPmAgFnP	vyjít
jako	jako	k9	jako
singly	singl	k1gInPc4	singl
a	a	k8xC	a
staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
jedněmi	jeden	k4xCgFnPc7	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgMnPc2d3	nejpopulárnější
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
"	"	kIx"	"
<g/>
Let	let	k1gInSc1	let
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Get	Get	k1gMnSc7	Get
It	It	k1gMnSc7	It
Up	Up	k1gMnSc7	Up
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
For	forum	k1gNnPc2	forum
Those	Thos	k1gInSc5	Thos
About	About	k2eAgInSc1d1	About
to	ten	k3xDgNnSc1	ten
Rock	rock	k1gInSc1	rock
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
umístily	umístit	k5eAaPmAgFnP	umístit
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
15	[number]	k4	15
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
britské	britský	k2eAgFnSc2d1	britská
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
nahrávání	nahrávání	k1gNnSc4	nahrávání
svého	svůj	k3xOyFgNnSc2	svůj
dalšího	další	k2eAgNnSc2d1	další
alba	album	k1gNnSc2	album
Flick	Flick	k1gMnSc1	Flick
of	of	k?	of
the	the	k?	the
Switch	Switch	k1gMnSc1	Switch
rozešla	rozejít	k5eAaPmAgFnS	rozejít
s	s	k7c7	s
Langem	Lang	k1gMnSc7	Lang
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
chtěla	chtít	k5eAaImAgFnS	chtít
vrátit	vrátit	k5eAaPmF	vrátit
k	k	k7c3	k
jednoduchosti	jednoduchost	k1gFnSc3	jednoduchost
a	a	k8xC	a
syrovosti	syrovost	k1gFnSc6	syrovost
svých	svůj	k3xOyFgFnPc2	svůj
prvních	první	k4xOgFnPc2	první
alb	alba	k1gFnPc2	alba
<g/>
.	.	kIx.	.
</s>
<s>
Phill	Phill	k1gMnSc1	Phill
Rudd	Rudd	k1gMnSc1	Rudd
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
konzumace	konzumace	k1gFnSc2	konzumace
drog	droga	k1gFnPc2	droga
mít	mít	k5eAaImF	mít
psychické	psychický	k2eAgInPc4d1	psychický
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
trpěl	trpět	k5eAaImAgMnS	trpět
halucinacemi	halucinace	k1gFnPc7	halucinace
<g/>
.	.	kIx.	.
</s>
<s>
Přátelství	přátelství	k1gNnSc1	přátelství
mezi	mezi	k7c7	mezi
Ruddem	Rudd	k1gMnSc7	Rudd
a	a	k8xC	a
Malcolmem	Malcolm	k1gMnSc7	Malcolm
Youngem	Young	k1gInSc7	Young
začalo	začít	k5eAaPmAgNnS	začít
upadat	upadat	k5eAaImF	upadat
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
vztahy	vztah	k1gInPc1	vztah
se	se	k3xPyFc4	se
zhoršily	zhoršit	k5eAaPmAgInP	zhoršit
až	až	k9	až
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
antipatií	antipatie	k1gFnPc2	antipatie
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
poprali	poprat	k5eAaPmAgMnP	poprat
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
hodiny	hodina	k1gFnPc1	hodina
po	po	k7c6	po
rvačce	rvačka	k1gFnSc6	rvačka
byl	být	k5eAaImAgInS	být
Rudd	Rudd	k1gInSc1	Rudd
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
vyhozen	vyhozen	k2eAgMnSc1d1	vyhozen
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Rudd	Rudd	k1gMnSc1	Rudd
již	již	k6eAd1	již
dokončil	dokončit	k5eAaPmAgMnS	dokončit
většinu	většina	k1gFnSc4	většina
své	svůj	k3xOyFgFnSc2	svůj
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
<g/>
,	,	kIx,	,
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
jej	on	k3xPp3gMnSc4	on
po	po	k7c6	po
konkurzu	konkurz	k1gInSc6	konkurz
nahradil	nahradit	k5eAaPmAgMnS	nahradit
bubeník	bubeník	k1gMnSc1	bubeník
Simon	Simon	k1gMnSc1	Simon
Wright	Wright	k1gMnSc1	Wright
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
sestavě	sestava	k1gFnSc6	sestava
vydala	vydat	k5eAaPmAgFnS	vydat
kapela	kapela	k1gFnSc1	kapela
album	album	k1gNnSc4	album
Flick	Flick	k1gMnSc1	Flick
of	of	k?	of
the	the	k?	the
Switch	Switcha	k1gFnPc2	Switcha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
málo	málo	k6eAd1	málo
propracované	propracovaný	k2eAgInPc4d1	propracovaný
a	a	k8xC	a
snáze	snadno	k6eAd2	snadno
zapomenutelné	zapomenutelný	k2eAgNnSc1d1	zapomenutelné
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
kritik	kritik	k1gMnSc1	kritik
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kapela	kapela	k1gFnSc1	kapela
"	"	kIx"	"
<g/>
udělala	udělat	k5eAaPmAgFnS	udělat
devětkrát	devětkrát	k6eAd1	devětkrát
stejné	stejný	k2eAgNnSc4d1	stejné
album	album	k1gNnSc4	album
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
čtenářské	čtenářský	k2eAgFnSc6d1	čtenářská
anketě	anketa	k1gFnSc6	anketa
časopisu	časopis	k1gInSc2	časopis
Kerrang	Kerranga	k1gFnPc2	Kerranga
<g/>
!	!	kIx.	!
</s>
<s>
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
osmým	osmý	k4xOgFnPc3	osmý
největším	veliký	k2eAgNnSc7d3	veliký
zklamáním	zklamání	k1gNnSc7	zklamání
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
album	album	k1gNnSc1	album
dostalo	dostat	k5eAaPmAgNnS	dostat
až	až	k9	až
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
britského	britský	k2eAgInSc2d1	britský
žebříčku	žebříček	k1gInSc2	žebříček
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
menší	malý	k2eAgInSc4d2	menší
úspěch	úspěch	k1gInSc4	úspěch
i	i	k9	i
se	s	k7c7	s
singly	singl	k1gInPc7	singl
"	"	kIx"	"
<g/>
Nervous	Nervous	k1gMnSc1	Nervous
Shakedown	Shakedown	k1gMnSc1	Shakedown
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Flick	Flick	k1gInSc1	Flick
of	of	k?	of
the	the	k?	the
Swich	Swich	k1gInSc1	Swich
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Fly	Fly	k1gFnSc2	Fly
on	on	k3xPp3gInSc1	on
the	the	k?	the
Wall	Wall	k1gInSc1	Wall
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
produkovali	produkovat	k5eAaImAgMnP	produkovat
bratři	bratr	k1gMnPc1	bratr
Youngovi	Youngův	k2eAgMnPc1d1	Youngův
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
jej	on	k3xPp3gMnSc4	on
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
dílo	dílo	k1gNnSc4	dílo
bez	bez	k7c2	bez
inspirace	inspirace	k1gFnSc2	inspirace
a	a	k8xC	a
bez	bez	k7c2	bez
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
také	také	k9	také
vydáno	vydán	k2eAgNnSc1d1	vydáno
video	video	k1gNnSc1	video
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
kapela	kapela	k1gFnSc1	kapela
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
baru	bar	k1gInSc6	bar
pět	pět	k4xCc1	pět
z	z	k7c2	z
desíti	deset	k4xCc2	deset
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
albu	album	k1gNnSc6	album
obsaženy	obsažen	k2eAgInPc1d1	obsažen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
úspěchu	úspěch	k1gInSc3	úspěch
píseň	píseň	k1gFnSc1	píseň
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
rádia	rádio	k1gNnPc4	rádio
"	"	kIx"	"
<g/>
Who	Who	k1gFnSc1	Who
Made	Made	k1gFnSc1	Made
Who	Who	k1gFnSc1	Who
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Who	Who	k1gFnPc2	Who
Made	Mad	k1gFnSc2	Mad
Who	Who	k1gFnSc7	Who
bylo	být	k5eAaImAgNnS	být
soundtrackem	soundtrack	k1gInSc7	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
Stephena	Stephen	k2eAgFnSc1d1	Stephena
Kinga	Kinga	k1gFnSc1	Kinga
Maximum	maximum	k1gNnSc1	maximum
Overdrive	Overdriev	k1gFnSc2	Overdriev
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
kompilaci	kompilace	k1gFnSc4	kompilace
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
totiž	totiž	k9	totiž
byly	být	k5eAaImAgFnP	být
obsaženy	obsáhnout	k5eAaPmNgInP	obsáhnout
jak	jak	k8xC	jak
staré	starý	k2eAgInPc1d1	starý
hity	hit	k1gInPc1	hit
jako	jako	k8xS	jako
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Hells	Hells	k1gInSc1	Hells
Bells	Bells	k1gInSc1	Bells
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Ride	Ride	k1gFnSc1	Ride
On	on	k3xPp3gMnSc1	on
<g/>
"	"	kIx"	"
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
novějšími	nový	k2eAgInPc7d2	novější
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Sink	Sink	k1gInSc1	Sink
the	the	k?	the
Pink	pink	k6eAd1	pink
<g/>
"	"	kIx"	"
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
instrumentálními	instrumentální	k2eAgFnPc7d1	instrumentální
písněmi	píseň	k1gFnPc7	píseň
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
D.T	D.T	k1gMnPc5	D.T
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Chase	chasa	k1gFnSc3	chasa
the	the	k?	the
Ace	Ace	k1gFnSc1	Ace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1988	[number]	k4	1988
byli	být	k5eAaImAgMnP	být
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
uvedeni	uvést	k5eAaPmNgMnP	uvést
do	do	k7c2	do
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
Australské	australský	k2eAgFnSc2d1	australská
asociace	asociace	k1gFnSc2	asociace
nahrávacího	nahrávací	k2eAgInSc2d1	nahrávací
průmyslu	průmysl	k1gInSc2	průmysl
(	(	kIx(	(
<g/>
Australian	Australian	k1gMnSc1	Australian
Recording	Recording	k1gInSc4	Recording
Industry	Industra	k1gFnSc2	Industra
Association	Association	k1gInSc1	Association
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
Blow	Blow	k1gFnSc1	Blow
Up	Up	k1gFnSc2	Up
Your	Youra	k1gFnPc2	Youra
Video	video	k1gNnSc1	video
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nahráváno	nahrávat	k5eAaImNgNnS	nahrávat
v	v	k7c6	v
Miraval	Miraval	k1gFnSc6	Miraval
Studio	studio	k1gNnSc1	studio
v	v	k7c6	v
Le	Le	k1gFnSc6	Le
Val	val	k1gInSc4	val
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
podíleli	podílet	k5eAaImAgMnP	podílet
původní	původní	k2eAgMnPc1d1	původní
producenti	producent	k1gMnPc1	producent
Harry	Harra	k1gFnSc2	Harra
Vanda	Vanda	k1gFnSc1	Vanda
a	a	k8xC	a
George	George	k1gFnSc1	George
Young	Younga	k1gFnPc2	Younga
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
nahrála	nahrát	k5eAaBmAgFnS	nahrát
devatenáct	devatenáct	k4xCc4	devatenáct
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
na	na	k7c4	na
album	album	k1gNnSc4	album
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
deset	deset	k4xCc1	deset
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc1	album
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k6eAd1	mnoho
"	"	kIx"	"
<g/>
výplně	výplň	k1gFnPc1	výplň
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Blow	Blow	k?	Blow
up	up	k?	up
Your	Your	k1gInSc4	Your
Video	video	k1gNnSc1	video
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
značného	značný	k2eAgInSc2d1	značný
komerčního	komerční	k2eAgInSc2d1	komerční
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
,	,	kIx,	,
prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
více	hodně	k6eAd2	hodně
kopií	kopie	k1gFnPc2	kopie
než	než	k8xS	než
předchozích	předchozí	k2eAgFnPc2d1	předchozí
dvou	dva	k4xCgFnPc2	dva
alb	alba	k1gFnPc2	alba
dohromady	dohromady	k6eAd1	dohromady
a	a	k8xC	a
na	na	k7c6	na
britském	britský	k2eAgInSc6d1	britský
žebříčku	žebříček	k1gInSc6	žebříček
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
největší	veliký	k2eAgInSc4d3	veliký
úspěch	úspěch	k1gInSc4	úspěch
od	od	k7c2	od
alba	album	k1gNnSc2	album
Back	Back	k1gMnSc1	Back
in	in	k?	in
Black	Black	k1gMnSc1	Black
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Heatseeker	Heatseeker	k1gInSc1	Heatseeker
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
první	první	k4xOgFnSc2	první
dvacítky	dvacítka	k1gFnSc2	dvacítka
singlového	singlový	k2eAgInSc2d1	singlový
žebříčku	žebříček	k1gInSc2	žebříček
a	a	k8xC	a
album	album	k1gNnSc1	album
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
i	i	k8xC	i
další	další	k2eAgFnPc1d1	další
populární	populární	k2eAgFnPc1d1	populární
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
That	That	k1gInSc1	That
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
The	The	k1gFnSc7	The
Way	Way	k1gMnSc2	Way
I	i	k8xC	i
Wanna	Wann	k1gMnSc2	Wann
Rock	rock	k1gInSc1	rock
And	Anda	k1gFnPc2	Anda
Roll	Rolla	k1gFnPc2	Rolla
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Světové	světový	k2eAgNnSc4d1	světové
turné	turné	k1gNnSc4	turné
k	k	k7c3	k
albu	album	k1gNnSc3	album
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1988	[number]	k4	1988
v	v	k7c6	v
Perthu	Perth	k1gInSc6	Perth
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1988	[number]	k4	1988
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Malcolm	Malcolm	k1gMnSc1	Malcolm
Young	Young	k1gMnSc1	Young
<g/>
,	,	kIx,	,
že	že	k8xS	že
turné	turné	k1gNnSc4	turné
na	na	k7c4	na
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
opustí	opustit	k5eAaPmIp3nS	opustit
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
kvůli	kvůli	k7c3	kvůli
léčbě	léčba	k1gFnSc3	léčba
z	z	k7c2	z
alkoholismu	alkoholismus	k1gInSc2	alkoholismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
jej	on	k3xPp3gNnSc2	on
zatím	zatím	k6eAd1	zatím
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jeho	jeho	k3xOp3gMnSc1	jeho
příbuzný	příbuzný	k1gMnSc1	příbuzný
Stevie	Stevie	k1gFnSc2	Stevie
Young	Young	k1gMnSc1	Young
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
turné	turné	k1gNnSc2	turné
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
odešel	odejít	k5eAaPmAgMnS	odejít
Simon	Simon	k1gMnSc1	Simon
Wright	Wright	k1gMnSc1	Wright
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
na	na	k7c6	na
albu	album	k1gNnSc6	album
Ronnieho	Ronnie	k1gMnSc2	Ronnie
Jamese	Jamese	k1gFnSc2	Jamese
Dia	Dia	k1gMnSc1	Dia
Lock	Lock	k1gMnSc1	Lock
up	up	k?	up
the	the	k?	the
Wolves	Wolves	k1gInSc1	Wolves
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
jej	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
studiový	studiový	k2eAgMnSc1d1	studiový
hudebník	hudebník	k1gMnSc1	hudebník
Chris	Chris	k1gFnSc2	Chris
Slade	slad	k1gInSc5	slad
<g/>
.	.	kIx.	.
</s>
<s>
Johnson	Johnson	k1gMnSc1	Johnson
se	se	k3xPyFc4	se
dění	dění	k1gNnSc4	dění
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
také	také	k9	také
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
neúčastnil	účastnit	k5eNaImAgMnS	účastnit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
rozváděl	rozvádět	k5eAaImAgMnS	rozvádět
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
bratři	bratr	k1gMnPc1	bratr
Youngovi	Youngův	k2eAgMnPc1d1	Youngův
napsali	napsat	k5eAaBmAgMnP	napsat
všechny	všechen	k3xTgFnPc4	všechen
písně	píseň	k1gFnPc4	píseň
pro	pro	k7c4	pro
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
v	v	k7c6	v
čemž	což	k3yQnSc6	což
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
i	i	k9	i
u	u	k7c2	u
následujících	následující	k2eAgNnPc2d1	následující
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
album	album	k1gNnSc1	album
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Razors	Razorsa	k1gFnPc2	Razorsa
Edge	Edg	k1gInSc2	Edg
<g/>
,	,	kIx,	,
produkoval	produkovat	k5eAaImAgMnS	produkovat
Bruce	Bruce	k1gMnSc1	Bruce
Fairbairn	Fairbairn	k1gMnSc1	Fairbairn
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dříve	dříve	k6eAd2	dříve
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Aerosmith	Aerosmith	k1gInSc4	Aerosmith
a	a	k8xC	a
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
znamenalo	znamenat	k5eAaImAgNnS	znamenat
pro	pro	k7c4	pro
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
úpsěšný	úpsěšný	k2eAgInSc1d1	úpsěšný
návrat	návrat	k1gInSc1	návrat
mezi	mezi	k7c4	mezi
nejpopulárnější	populární	k2eAgFnPc4d3	nejpopulárnější
kapely	kapela	k1gFnPc4	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Hity	hit	k1gInPc1	hit
"	"	kIx"	"
<g/>
Thunderstruck	Thunderstruck	k1gInSc1	Thunderstruck
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Are	ar	k1gInSc5	ar
You	You	k1gMnSc1	You
Ready	ready	k0	ready
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
umístily	umístit	k5eAaPmAgFnP	umístit
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
16	[number]	k4	16
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
Mainstream	Mainstream	k1gInSc1	Mainstream
Rock	rock	k1gInSc1	rock
Tracks	Tracks	k1gInSc1	Tracks
a	a	k8xC	a
"	"	kIx"	"
<g/>
Moneytalks	Moneytalks	k1gInSc1	Moneytalks
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
23	[number]	k4	23
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
amerického	americký	k2eAgInSc2d1	americký
singlového	singlový	k2eAgInSc2d1	singlový
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc4	album
získalo	získat	k5eAaPmAgNnS	získat
několik	několik	k4yIc1	několik
platinových	platinový	k2eAgFnPc2d1	platinová
desek	deska	k1gFnPc2	deska
a	a	k8xC	a
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgFnPc2d1	americká
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
do	do	k7c2	do
nejlepší	dobrý	k2eAgFnSc2d3	nejlepší
desítky	desítka	k1gFnSc2	desítka
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
koncertů	koncert	k1gInPc2	koncert
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
k	k	k7c3	k
albu	album	k1gNnSc3	album
Razors	Razorsa	k1gFnPc2	Razorsa
Edge	Edg	k1gInSc2	Edg
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaBmNgNnS	nahrát
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
posloužily	posloužit	k5eAaPmAgInP	posloužit
jako	jako	k8xS	jako
základ	základ	k1gInSc1	základ
živého	živý	k2eAgNnSc2d1	živé
alba	album	k1gNnSc2	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Live	Liv	k1gInSc2	Liv
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc4	album
opět	opět	k6eAd1	opět
produkoval	produkovat	k5eAaImAgMnS	produkovat
Fairbairn	Fairbairn	k1gInSc4	Fairbairn
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
živých	živý	k2eAgNnPc2d1	živé
alb	album	k1gNnPc2	album
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
nahrála	nahrát	k5eAaBmAgFnS	nahrát
kapela	kapela	k1gFnSc1	kapela
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Big	Big	k1gFnSc1	Big
Gun	Gun	k1gFnSc2	Gun
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
soundtrack	soundtrack	k1gInSc4	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
Arnolda	Arnold	k1gMnSc2	Arnold
Schwarzeneggera	Schwarzenegger	k1gMnSc2	Schwarzenegger
Poslední	poslední	k2eAgMnSc1d1	poslední
akční	akční	k2eAgMnSc1d1	akční
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Mainstream	Mainstream	k1gInSc1	Mainstream
Rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
kapele	kapela	k1gFnSc3	kapela
povedlo	povést	k5eAaPmAgNnS	povést
vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
pozvali	pozvat	k5eAaPmAgMnP	pozvat
Youngovi	Youngův	k2eAgMnPc1d1	Youngův
Phila	Phila	k1gMnSc1	Phila
Rudda	Rudda	k1gMnSc1	Rudda
na	na	k7c4	na
několik	několik	k4yIc4	několik
jam	jáma	k1gFnPc2	jáma
session	session	k1gInSc1	session
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
stal	stát	k5eAaPmAgMnS	stát
bubeníkem	bubeník	k1gMnSc7	bubeník
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgInS	nahradit
Sladea	Sladeus	k1gMnSc4	Sladeus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
rozešel	rozejít	k5eAaPmAgMnS	rozejít
v	v	k7c6	v
dobrém	dobrý	k2eAgNnSc6d1	dobré
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
chápal	chápat	k5eAaImAgInS	chápat
chuť	chuť	k1gFnSc4	chuť
členů	člen	k1gInPc2	člen
hrát	hrát	k5eAaImF	hrát
opět	opět	k6eAd1	opět
s	s	k7c7	s
Ruddem	Rudd	k1gInSc7	Rudd
<g/>
.	.	kIx.	.
</s>
<s>
Sestava	sestava	k1gFnSc1	sestava
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
sjednotila	sjednotit	k5eAaPmAgFnS	sjednotit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
vydala	vydat	k5eAaPmAgFnS	vydat
album	album	k1gNnSc4	album
Ballbreaker	Ballbreakero	k1gNnPc2	Ballbreakero
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
nahrála	nahrát	k5eAaPmAgFnS	nahrát
v	v	k7c4	v
Ocean	Ocean	k1gInSc4	Ocean
Way	Way	k1gFnSc1	Way
Studios	Studios	k?	Studios
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gMnSc1	Angeles
a	a	k8xC	a
produkoval	produkovat	k5eAaImAgMnS	produkovat
jej	on	k3xPp3gMnSc4	on
Rick	Rick	k1gMnSc1	Rick
Rubin	Rubin	k1gMnSc1	Rubin
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Hard	Hard	k1gInSc1	Hard
as	as	k1gInSc1	as
a	a	k8xC	a
Rock	rock	k1gInSc1	rock
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
amerických	americký	k2eAgInPc2d1	americký
žebříčků	žebříček	k1gInPc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
i	i	k8xC	i
další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
singly	singl	k1gInPc1	singl
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
"	"	kIx"	"
<g/>
Hail	Hail	k1gInSc1	Hail
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Cover	Cover	k1gInSc1	Cover
You	You	k1gFnSc2	You
in	in	k?	in
Oil	Oil	k1gFnSc2	Oil
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
vyšel	vyjít	k5eAaPmAgInS	vyjít
box	box	k1gInSc1	box
set	sto	k4xCgNnPc2	sto
Bonfire	Bonfir	k1gMnSc5	Bonfir
<g/>
.	.	kIx.	.
</s>
<s>
Obsahoval	obsahovat	k5eAaImAgMnS	obsahovat
čtyři	čtyři	k4xCgNnPc4	čtyři
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
remaseterovanou	remaseterovaný	k2eAgFnSc4d1	remaseterovaný
verzi	verze	k1gFnSc4	verze
Back	Backa	k1gFnPc2	Backa
in	in	k?	in
Black	Black	k1gInSc1	Black
<g/>
,	,	kIx,	,
Volts	Volts	k1gInSc1	Volts
(	(	kIx(	(
<g/>
disk	disk	k1gInSc1	disk
s	s	k7c7	s
alternativními	alternativní	k2eAgFnPc7d1	alternativní
verzemi	verze	k1gFnPc7	verze
<g/>
,	,	kIx,	,
nevydanými	vydaný	k2eNgFnPc7d1	nevydaná
písněmi	píseň	k1gFnPc7	píseň
a	a	k8xC	a
živými	živý	k2eAgFnPc7d1	živá
nahrávkami	nahrávka	k1gFnPc7	nahrávka
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
živá	živý	k2eAgNnPc4d1	živé
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
Live	Liv	k1gFnPc4	Liv
from	froma	k1gFnPc2	froma
the	the	k?	the
Atlantic	Atlantice	k1gFnPc2	Atlantice
Studios	Studios	k?	Studios
a	a	k8xC	a
Let	léto	k1gNnPc2	léto
There	Ther	k1gInSc5	Ther
Be	Be	k1gMnSc1	Be
Rock	rock	k1gInSc1	rock
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Movie	Movie	k1gFnSc1	Movie
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
verze	verze	k1gFnSc1	verze
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
barevný	barevný	k2eAgInSc4d1	barevný
booklet	booklet	k1gInSc4	booklet
<g/>
,	,	kIx,	,
oboustranný	oboustranný	k2eAgInSc4d1	oboustranný
plakát	plakát	k1gInSc4	plakát
<g/>
,	,	kIx,	,
nálepku	nálepka	k1gFnSc4	nálepka
<g/>
,	,	kIx,	,
smývatelné	smývatelný	k2eAgNnSc4d1	smývatelné
tetování	tetování	k1gNnSc4	tetování
<g/>
,	,	kIx,	,
otvírák	otvírák	k1gInSc4	otvírák
a	a	k8xC	a
trsátko	trsátko	k1gNnSc4	trsátko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
vydali	vydat	k5eAaPmAgMnP	vydat
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
své	svůj	k3xOyFgNnSc4	svůj
šestnácté	šestnáctý	k4xOgNnSc4	šestnáctý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
Stiff	Stiff	k1gInSc4	Stiff
Upper	Uppra	k1gFnPc2	Uppra
Lip	lípa	k1gFnPc2	lípa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
produkoval	produkovat	k5eAaImAgInS	produkovat
George	George	k1gFnSc4	George
Young	Younga	k1gFnPc2	Younga
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
kritikou	kritika	k1gFnSc7	kritika
přijato	přijmout	k5eAaPmNgNnS	přijmout
pozitivněji	pozitivně	k6eAd2	pozitivně
než	než	k8xS	než
předchozí	předchozí	k2eAgInSc4d1	předchozí
Ballbreaker	Ballbreaker	k1gInSc4	Ballbreaker
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gMnSc3	on
vyčítán	vyčítán	k2eAgInSc4d1	vyčítán
nedostatek	nedostatek	k1gInSc4	nedostatek
nových	nový	k2eAgInPc2d1	nový
nápadů	nápad	k1gInPc2	nápad
<g/>
.	.	kIx.	.
</s>
<s>
Australská	australský	k2eAgFnSc1d1	australská
verze	verze	k1gFnSc1	verze
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
bonusový	bonusový	k2eAgInSc4d1	bonusový
disk	disk	k1gInSc4	disk
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
propagačními	propagační	k2eAgInPc7d1	propagační
videi	vide	k1gInPc7	vide
a	a	k8xC	a
několika	několik	k4yIc7	několik
živými	živý	k2eAgFnPc7d1	živá
nahrávkami	nahrávka	k1gFnPc7	nahrávka
pořízenými	pořízený	k2eAgFnPc7d1	pořízená
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Stiff	Stiff	k1gInSc1	Stiff
Upper	Upper	k1gMnSc1	Upper
Lip	lípa	k1gFnPc2	lípa
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
v	v	k7c6	v
pěti	pět	k4xCc6	pět
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
album	album	k1gNnSc1	album
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
sedmém	sedmý	k4xOgInSc6	sedmý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Stiff	Stiff	k1gInSc1	Stiff
Upper	Uppra	k1gFnPc2	Uppra
Lip	lípa	k1gFnPc2	lípa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
amerického	americký	k2eAgInSc2d1	americký
žebříčku	žebříček	k1gInSc2	žebříček
Mainstream	Mainstream	k1gInSc1	Mainstream
Rock	rock	k1gInSc1	rock
Tracks	Tracks	k1gInSc4	Tracks
udržel	udržet	k5eAaPmAgInS	udržet
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
singly	singl	k1gInPc1	singl
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
také	také	k6eAd1	také
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Safe	safe	k1gInSc1	safe
in	in	k?	in
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
City	City	k1gFnSc1	City
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
31	[number]	k4	31
<g/>
.	.	kIx.	.
a	a	k8xC	a
"	"	kIx"	"
<g/>
Sattelite	Sattelit	k1gInSc5	Sattelit
Blues	blues	k1gNnSc1	blues
<g/>
"	"	kIx"	"
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
podepsali	podepsat	k5eAaPmAgMnP	podepsat
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
smlouvu	smlouva	k1gFnSc4	smlouva
se	s	k7c7	s
Sony	son	k1gInPc7	son
Music	Musice	k1gInPc2	Musice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
poté	poté	k6eAd1	poté
vydala	vydat	k5eAaPmAgFnS	vydat
sérií	série	k1gFnSc7	série
remasterovaných	remasterovaný	k2eAgFnPc2d1	remasterovaná
alb	alba	k1gFnPc2	alba
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
série	série	k1gFnSc2	série
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
Remasters	Remasters	k1gInSc1	Remasters
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
album	album	k1gNnSc1	album
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
booklet	booklet	k1gInSc4	booklet
<g/>
,	,	kIx,	,
unikátní	unikátní	k2eAgFnPc1d1	unikátní
fotografie	fotografia	k1gFnPc1	fotografia
<g/>
,	,	kIx,	,
memorabilia	memorabilia	k1gNnPc1	memorabilia
a	a	k8xC	a
poznámky	poznámka	k1gFnPc1	poznámka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
celý	celý	k2eAgInSc1d1	celý
katalog	katalog	k1gInSc1	katalog
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
alb	album	k1gNnPc2	album
Ballbreaker	Ballbreakra	k1gFnPc2	Ballbreakra
a	a	k8xC	a
Stiff	Stiff	k1gInSc1	Stiff
Upper	Uppra	k1gFnPc2	Uppra
Lip	lípa	k1gFnPc2	lípa
<g/>
)	)	kIx)	)
remasterován	remasterován	k2eAgInSc1d1	remasterován
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
vydán	vydat	k5eAaPmNgInS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Ballbreaker	Ballbreaker	k1gInSc1	Ballbreaker
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
a	a	k8xC	a
Stiff	Stiff	k1gInSc1	Stiff
Upper	Uppra	k1gFnPc2	Uppra
Lip	lípa	k1gFnPc2	lípa
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2003	[number]	k4	2003
přijal	přijmout	k5eAaPmAgMnS	přijmout
Malcolm	Malcolm	k1gMnSc1	Malcolm
Young	Young	k1gMnSc1	Young
cenu	cena	k1gFnSc4	cena
Teda	Ted	k1gMnSc2	Ted
Edwarda	Edward	k1gMnSc2	Edward
za	za	k7c4	za
skvělou	skvělý	k2eAgFnSc4d1	skvělá
službu	služba	k1gFnSc4	služba
australské	australský	k2eAgFnSc3d1	australská
hudbě	hudba	k1gFnSc3	hudba
(	(	kIx(	(
<g/>
Ted	Ted	k1gMnSc1	Ted
Albert	Albert	k1gMnSc1	Albert
Award	Award	k1gMnSc1	Award
for	forum	k1gNnPc2	forum
Outstanding	Outstanding	k1gInSc1	Outstanding
Service	Service	k1gFnPc1	Service
to	ten	k3xDgNnSc4	ten
Australian	Australian	k1gMnSc1	Australian
Music	Music	k1gMnSc1	Music
<g/>
)	)	kIx)	)
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
Music	Musice	k1gFnPc2	Musice
Winner	Winnra	k1gFnPc2	Winnra
Awards	Awards	k1gInSc1	Awards
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
současně	současně	k6eAd1	současně
vzdal	vzdát	k5eAaPmAgMnS	vzdát
úctu	úcta	k1gFnSc4	úcta
Bonu	bon	k1gInSc2	bon
Scottovi	Scott	k1gMnSc3	Scott
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
RIAA	RIAA	kA	RIAA
aktualizovala	aktualizovat	k5eAaBmAgFnS	aktualizovat
počet	počet	k1gInSc4	počet
prodaných	prodaný	k2eAgNnPc2d1	prodané
alb	album	k1gNnPc2	album
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
z	z	k7c2	z
46,5	[number]	k4	46,5
milionu	milion	k4xCgInSc2	milion
na	na	k7c4	na
63	[number]	k4	63
miliony	milion	k4xCgInPc7	milion
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
stali	stát	k5eAaPmAgMnP	stát
pátou	pátá	k1gFnSc4	pátá
nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
kapelou	kapela	k1gFnSc7	kapela
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnPc1d2	vyšší
prodejnosti	prodejnost	k1gFnPc1	prodejnost
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
jen	jen	k9	jen
kapely	kapela	k1gFnPc1	kapela
The	The	k1gMnSc2	The
Beatles	beatles	k1gMnSc2	beatles
<g/>
,	,	kIx,	,
Led	led	k1gInSc4	led
Zeppelin	Zeppelin	k2eAgInSc4d1	Zeppelin
<g/>
,	,	kIx,	,
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
a	a	k8xC	a
Eagles	Eagles	k1gInSc4	Eagles
<g/>
.	.	kIx.	.
</s>
<s>
RIAA	RIAA	kA	RIAA
také	také	k9	také
ocenila	ocenit	k5eAaPmAgFnS	ocenit
album	album	k1gNnSc4	album
Back	Backa	k1gFnPc2	Backa
in	in	k?	in
Black	Blacka	k1gFnPc2	Blacka
dvakrát	dvakrát	k6eAd1	dvakrát
diamantovou	diamantový	k2eAgFnSc7d1	Diamantová
deskou	deska	k1gFnSc7	deska
za	za	k7c4	za
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
prodaných	prodaný	k2eAgFnPc2d1	prodaná
kopií	kopie	k1gFnPc2	kopie
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
album	album	k1gNnSc1	album
stalo	stát	k5eAaPmAgNnS	stát
šestým	šestý	k4xOgMnSc7	šestý
nejúspěšnějším	úspěšný	k2eAgNnSc7d3	nejúspěšnější
albem	album	k1gNnSc7	album
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
s	s	k7c7	s
21	[number]	k4	21
miliony	milion	k4xCgInPc7	milion
prodaných	prodaný	k2eAgFnPc2d1	prodaná
kopií	kopie	k1gFnPc2	kopie
posunulo	posunout	k5eAaPmAgNnS	posunout
na	na	k7c4	na
páté	pátý	k4xOgNnSc4	pátý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2003	[number]	k4	2003
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
před	před	k7c4	před
půl	půl	k1xP	půl
milionem	milion	k4xCgInSc7	milion
diváků	divák	k1gMnPc2	divák
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Rush	Rusha	k1gFnPc2	Rusha
a	a	k8xC	a
The	The	k1gMnPc2	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
na	na	k7c4	na
Molson	Molson	k1gInSc4	Molson
Canadian	Canadian	k1gInSc1	Canadian
Rocks	Rocksa	k1gFnPc2	Rocksa
for	forum	k1gNnPc2	forum
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
koncert	koncert	k1gInSc1	koncert
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
městu	město	k1gNnSc3	město
pomoci	pomoct	k5eAaPmF	pomoct
překonat	překonat	k5eAaPmF	překonat
následky	následek	k1gInPc4	následek
epidemie	epidemie	k1gFnSc2	epidemie
SARS	SARS	kA	SARS
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
největší	veliký	k2eAgInSc4d3	veliký
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
vybíralo	vybírat	k5eAaImAgNnS	vybírat
vstupné	vstupný	k2eAgNnSc1d1	vstupné
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
dopravní	dopravní	k2eAgFnSc1d1	dopravní
tepna	tepna	k1gFnSc1	tepna
v	v	k7c6	v
Melbourne	Melbourne	k1gNnSc6	Melbourne
<g/>
,	,	kIx,	,
Corporation	Corporation	k1gInSc4	Corporation
Lane	Lan	k1gFnSc2	Lan
<g/>
,	,	kIx,	,
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Představitelé	představitel	k1gMnPc1	představitel
města	město	k1gNnSc2	město
však	však	k9	však
zamítli	zamítnout	k5eAaPmAgMnP	zamítnout
použití	použití	k1gNnSc4	použití
lomítka	lomítko	k1gNnSc2	lomítko
v	v	k7c6	v
názvu	název	k1gInSc6	název
ulice	ulice	k1gFnSc2	ulice
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
ACDC	ACDC	kA	ACDC
Lane	Lane	k1gInSc1	Lane
<g/>
.	.	kIx.	.
</s>
<s>
Ulice	ulice	k1gFnSc1	ulice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
poblíž	poblíž	k6eAd1	poblíž
Swanston	Swanston	k1gInSc1	Swanston
Street	Streeta	k1gFnPc2	Streeta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
natočen	natočit	k5eAaBmNgInS	natočit
videoklip	videoklip	k1gInSc1	videoklip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
"	"	kIx"	"
<g/>
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
a	a	k8xC	a
Long	Long	k1gMnSc1	Long
Way	Way	k1gMnSc1	Way
to	ten	k3xDgNnSc4	ten
the	the	k?	the
Top	topit	k5eAaImRp2nS	topit
(	(	kIx(	(
<g/>
If	If	k1gFnSc6	If
You	You	k1gFnPc2	You
Wanna	Wanna	k1gFnSc1	Wanna
Rock	rock	k1gInSc1	rock
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
Roll	Roll	k1gInSc4	Roll
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2000	[number]	k4	2000
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
ulice	ulice	k1gFnSc1	ulice
v	v	k7c4	v
Leganés	Leganés	k1gInSc4	Leganés
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
"	"	kIx"	"
<g/>
Calle	Calle	k1gFnSc1	Calle
de	de	k?	de
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
s	s	k7c7	s
příjmy	příjem	k1gInPc7	příjem
25	[number]	k4	25
miliony	milion	k4xCgInPc1	milion
australských	australský	k2eAgMnPc2d1	australský
dolarů	dolar	k1gInPc2	dolar
druhou	druhý	k4xOgFnSc4	druhý
nejvíce	nejvíce	k6eAd1	nejvíce
vydělávající	vydělávající	k2eAgFnSc7d1	vydělávající
australskou	australský	k2eAgFnSc7d1	australská
kapelou	kapela	k1gFnSc7	kapela
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
nevydali	vydat	k5eNaPmAgMnP	vydat
žádné	žádný	k3yNgNnSc4	žádný
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
nekoncertovali	koncertovat	k5eNaImAgMnP	koncertovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
(	(	kIx(	(
<g/>
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
aus	aus	k?	aus
<g/>
$	$	kIx~	$
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
vydělala	vydělat	k5eAaPmAgFnS	vydělat
kapela	kapela	k1gFnSc1	kapela
The	The	k1gMnSc1	The
Wiggles	Wiggles	k1gMnSc1	Wiggles
<g/>
.	.	kIx.	.
</s>
<s>
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fam	k1gFnSc2	Fam
uvedeni	uveden	k2eAgMnPc1d1	uveden
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
zahráli	zahrát	k5eAaPmAgMnP	zahrát
písně	píseň	k1gFnPc4	píseň
"	"	kIx"	"
<g/>
Highway	Highwa	k2eAgFnPc4d1	Highwa
to	ten	k3xDgNnSc4	ten
Hell	Hell	k1gMnSc1	Hell
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
You	You	k1gFnSc1	You
Shook	Shook	k1gInSc1	Shook
Me	Me	k1gMnSc1	Me
All	All	k1gMnSc1	All
Night	Night	k1gMnSc1	Night
Long	Long	k1gMnSc1	Long
<g/>
"	"	kIx"	"
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Stevenem	Steven	k1gMnSc7	Steven
Tylerem	Tyler	k1gMnSc7	Tyler
z	z	k7c2	z
Aerosmith	Aerosmitha	k1gFnPc2	Aerosmitha
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
popsal	popsat	k5eAaPmAgMnS	popsat
jejich	jejich	k3xOp3gInSc4	jejich
power	power	k1gInSc4	power
chords	chords	k6eAd1	chords
jako	jako	k9	jako
"	"	kIx"	"
<g/>
blesk	blesk	k1gInSc1	blesk
zespodu	zespodu	k6eAd1	zespodu
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vám	vy	k3xPp2nPc3	vy
dá	dát	k5eAaPmIp3nS	dát
druhý	druhý	k4xOgInSc1	druhý
nejsilnější	silný	k2eAgInSc1d3	nejsilnější
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
může	moct	k5eAaImIp3nS	moct
projít	projít	k5eAaPmF	projít
vašim	váš	k3xOp2gMnPc3	váš
tělem	tělo	k1gNnSc7	tělo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
citoval	citovat	k5eAaBmAgMnS	citovat
Brian	Brian	k1gMnSc1	Brian
Johnson	Johnson	k1gMnSc1	Johnson
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Let	let	k1gInSc1	let
There	Ther	k1gInSc5	Ther
Be	Be	k1gMnSc1	Be
Rock	rock	k1gInSc1	rock
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
<g/>
:	:	kIx,	:
Odkaz	odkaz	k1gInSc1	odkaz
kapely	kapela	k1gFnSc2	kapela
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
aktuální	aktuální	k2eAgFnSc1d1	aktuální
<g/>
.	.	kIx.	.
</s>
<s>
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
se	se	k3xPyFc4	se
dočkali	dočkat	k5eAaPmAgMnP	dočkat
mnoha	mnoho	k4c2	mnoho
hudebních	hudební	k2eAgNnPc2d1	hudební
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
od	od	k7c2	od
kritiků	kritik	k1gMnPc2	kritik
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
od	od	k7c2	od
podobně	podobně	k6eAd1	podobně
slavných	slavný	k2eAgMnPc2d1	slavný
a	a	k8xC	a
respektovaných	respektovaný	k2eAgMnPc2d1	respektovaný
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Linda	Linda	k1gFnSc1	Linda
Ronstadt	Ronstadt	k1gInSc1	Ronstadt
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
nevěnovala	věnovat	k5eNaImAgFnS	věnovat
pozornost	pozornost	k1gFnSc1	pozornost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
"	"	kIx"	"
<g/>
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
jsou	být	k5eAaImIp3nP	být
opravdou	opravda	k1gFnSc7	opravda
dobrou	dobrý	k2eAgFnSc7d1	dobrá
kapelou	kapela	k1gFnSc7	kapela
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c6	po
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
legendární	legendární	k2eAgFnSc1d1	legendární
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
zavřela	zavřít	k5eAaPmAgFnS	zavřít
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
řadovém	řadový	k2eAgNnSc6d1	řadové
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
si	se	k3xPyFc3	se
zavolala	zavolat	k5eAaPmAgFnS	zavolat
producenta	producent	k1gMnSc4	producent
Brendana	Brendana	k1gFnSc1	Brendana
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Briena	Brien	k1gMnSc4	Brien
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
také	také	k9	také
například	například	k6eAd1	například
s	s	k7c7	s
Pearl	Pearla	k1gFnPc2	Pearla
Jam	jáma	k1gFnPc2	jáma
<g/>
,	,	kIx,	,
Soundgarden	Soundgardna	k1gFnPc2	Soundgardna
<g/>
,	,	kIx,	,
Incubus	Incubus	k1gInSc1	Incubus
či	či	k8xC	či
Audioslave	Audioslav	k1gMnSc5	Audioslav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
řekl	říct	k5eAaPmAgMnS	říct
Brian	Brian	k1gMnSc1	Brian
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
,	,	kIx,	,
že	že	k8xS	že
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
alba	album	k1gNnSc2	album
Blow	Blow	k1gFnSc2	Blow
Up	Up	k1gFnSc2	Up
Your	Youra	k1gFnPc2	Youra
Video	video	k1gNnSc1	video
bude	být	k5eAaImBp3nS	být
zase	zase	k9	zase
psát	psát	k5eAaImF	psát
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgMnS	říct
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
Angus	Angus	k1gMnSc1	Angus
Young	Young	k1gMnSc1	Young
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
tvrdšími	tvrdý	k2eAgInPc7d2	tvrdší
riffy	riff	k1gInPc7	riff
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgFnPc4	ten
ve	v	k7c4	v
Stiff	Stiff	k1gInSc4	Stiff
Upper	Upper	k1gInSc4	Upper
Lip	lípa	k1gFnPc2	lípa
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Black	Black	k1gInSc1	Black
Ice	Ice	k1gFnSc2	Ice
a	a	k8xC	a
znamenalo	znamenat	k5eAaImAgNnS	znamenat
hudebně	hudebně	k6eAd1	hudebně
návrat	návrat	k1gInSc4	návrat
rockovým	rockový	k2eAgMnPc3d1	rockový
kořenům	kořen	k1gInPc3	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Najisto	najisto	k9	najisto
se	se	k3xPyFc4	se
však	však	k9	však
počítalo	počítat	k5eAaImAgNnS	počítat
s	s	k7c7	s
turné	turné	k1gNnSc7	turné
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
následovat	následovat	k5eAaImF	následovat
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
a	a	k8xC	a
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
patnáctá	patnáctý	k4xOgFnSc1	patnáctý
studiovka	studiovka	k1gFnSc1	studiovka
"	"	kIx"	"
<g/>
Stiff	Stiff	k1gInSc1	Stiff
Upper	Uppra	k1gFnPc2	Uppra
Lip	lípa	k1gFnPc2	lípa
<g/>
"	"	kIx"	"
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávala	nahrávat	k5eAaImAgFnS	nahrávat
se	se	k3xPyFc4	se
v	v	k7c6	v
The	The	k1gFnSc6	The
Warehouse	Warehouse	k1gFnSc2	Warehouse
Studio	studio	k1gNnSc1	studio
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
a	a	k8xC	a
produkci	produkce	k1gFnSc3	produkce
měl	mít	k5eAaImAgInS	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
George	George	k1gNnSc2	George
Young	Younga	k1gFnPc2	Younga
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
verzi	verze	k1gFnSc6	verze
"	"	kIx"	"
<g/>
Stiff	Stiff	k1gInSc1	Stiff
Upper	Upper	k1gMnSc1	Upper
Lip	lípa	k1gFnPc2	lípa
Tour	Toura	k1gFnPc2	Toura
Edition	Edition	k1gInSc1	Edition
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
kromě	kromě	k7c2	kromě
CD	CD	kA	CD
s	s	k7c7	s
klasickým	klasický	k2eAgInSc7d1	klasický
tracklistem	tracklist	k1gInSc7	tracklist
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
navíc	navíc	k6eAd1	navíc
i	i	k9	i
několik	několik	k4yIc4	několik
bonusů	bonus	k1gInPc2	bonus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2008	[number]	k4	2008
skupina	skupina	k1gFnSc1	skupina
zahájila	zahájit	k5eAaPmAgFnS	zahájit
své	svůj	k3xOyFgNnSc4	svůj
nové	nový	k2eAgNnSc4d1	nové
turné	turné	k1gNnSc4	turné
Black	Black	k1gMnSc1	Black
Ice	Ice	k1gMnSc1	Ice
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
tohoto	tento	k3xDgNnSc2	tento
turné	turné	k1gNnSc2	turné
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
vystoupení	vystoupení	k1gNnSc4	vystoupení
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
O2	O2	k1gFnSc6	O2
areně	areeň	k1gFnSc2	areeň
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Vstupenky	vstupenka	k1gFnPc4	vstupenka
na	na	k7c4	na
koncert	koncert	k1gInSc4	koncert
byly	být	k5eAaImAgFnP	být
během	během	k7c2	během
jediného	jediný	k2eAgInSc2d1	jediný
dne	den	k1gInSc2	den
vyprodány	vyprodán	k2eAgFnPc1d1	vyprodána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
odešel	odejít	k5eAaPmAgMnS	odejít
Malcolm	Malcolm	k1gMnSc1	Malcolm
Young	Young	k1gMnSc1	Young
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jeho	jeho	k3xOp3gMnSc1	jeho
synovec	synovec	k1gMnSc1	synovec
Stevie	Stevie	k1gFnSc2	Stevie
Young	Young	k1gMnSc1	Young
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
prosince	prosinec	k1gInSc2	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Rock	rock	k1gInSc4	rock
or	or	k?	or
Bust	busta	k1gFnPc2	busta
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
skupinou	skupina	k1gFnSc7	skupina
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
přestal	přestat	k5eAaPmAgMnS	přestat
vystupovat	vystupovat	k5eAaImF	vystupovat
Phil	Phil	k1gMnSc1	Phil
Rudd	Rudd	k1gMnSc1	Rudd
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
předávání	předávání	k1gNnSc2	předávání
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2015	[number]	k4	2015
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
Chris	Chris	k1gInSc1	Chris
Slade	slad	k1gInSc5	slad
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
působil	působit	k5eAaImAgMnS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
rovněž	rovněž	k9	rovněž
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skupinou	skupina	k1gFnSc7	skupina
odehraje	odehrát	k5eAaPmIp3nS	odehrát
i	i	k9	i
nadcházející	nadcházející	k2eAgNnSc4d1	nadcházející
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
turné	turné	k1gNnSc1	turné
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
vystoupení	vystoupení	k1gNnSc1	vystoupení
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
v	v	k7c6	v
Letňanech	Letňan	k1gMnPc6	Letňan
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Vstupenky	vstupenka	k1gFnPc4	vstupenka
na	na	k7c4	na
koncert	koncert	k1gInSc4	koncert
byly	být	k5eAaImAgFnP	být
rychle	rychle	k6eAd1	rychle
vyprodány	vyprodán	k2eAgFnPc1d1	vyprodána
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
Brian	Brian	k1gMnSc1	Brian
Johnson	Johnson	k1gMnSc1	Johnson
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
diagnostikována	diagnostikován	k2eAgFnSc1d1	diagnostikována
hrozba	hrozba	k1gFnSc1	hrozba
úplné	úplný	k2eAgFnSc2d1	úplná
ztráty	ztráta	k1gFnSc2	ztráta
sluchu	sluch	k1gInSc2	sluch
a	a	k8xC	a
ošetřující	ošetřující	k2eAgMnSc1d1	ošetřující
lékaři	lékař	k1gMnPc1	lékař
mu	on	k3xPp3gMnSc3	on
doporučili	doporučit	k5eAaPmAgMnP	doporučit
ukončit	ukončit	k5eAaPmF	ukončit
vystupování	vystupování	k1gNnSc4	vystupování
na	na	k7c6	na
velkých	velký	k2eAgInPc6d1	velký
koncertech	koncert	k1gInPc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
vyjádření	vyjádření	k1gNnSc6	vyjádření
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Naši	náš	k3xOp1gMnPc1	náš
fanoušci	fanoušek	k1gMnPc1	fanoušek
si	se	k3xPyFc3	se
zalouží	zaloužit	k5eAaPmIp3nP	zaloužit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
slyšeli	slyšet	k5eAaImAgMnP	slyšet
má	můj	k3xOp1gNnPc4	můj
vystoupení	vystoupení	k1gNnPc4	vystoupení
v	v	k7c6	v
perfektní	perfektní	k2eAgFnSc6d1	perfektní
kvalitě	kvalita	k1gFnSc6	kvalita
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
je	on	k3xPp3gInPc4	on
nemohu	moct	k5eNaImIp1nS	moct
zaručit	zaručit	k5eAaPmF	zaručit
<g/>
,	,	kIx,	,
nechci	chtít	k5eNaImIp1nS	chtít
zklamat	zklamat	k5eAaPmF	zklamat
diváky	divák	k1gMnPc4	divák
a	a	k8xC	a
potopit	potopit	k5eAaPmF	potopit
ostatní	ostatní	k2eAgMnPc4d1	ostatní
členy	člen	k1gMnPc4	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Nejsem	být	k5eNaImIp1nS	být
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
nedotahují	dotahovat	k5eNaImIp3nP	dotahovat
věci	věc	k1gFnPc4	věc
do	do	k7c2	do
konce	konec	k1gInSc2	konec
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
podle	podle	k7c2	podle
lékařů	lékař	k1gMnPc2	lékař
nemám	mít	k5eNaImIp1nS	mít
jinou	jiný	k2eAgFnSc4d1	jiná
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
než	než	k8xS	než
přestat	přestat	k5eAaPmF	přestat
vystupovat	vystupovat	k5eAaImF	vystupovat
na	na	k7c6	na
zbývajících	zbývající	k2eAgInPc6d1	zbývající
koncertech	koncert	k1gInPc6	koncert
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Diagnóza	diagnóza	k1gFnSc1	diagnóza
totiž	totiž	k9	totiž
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
v	v	k7c6	v
právě	právě	k6eAd1	právě
probíhajícím	probíhající	k2eAgNnSc6d1	probíhající
turné	turné	k1gNnSc6	turné
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
zbylých	zbylý	k2eAgInPc2d1	zbylý
10	[number]	k4	10
koncertů	koncert	k1gInPc2	koncert
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
odloženo	odložit	k5eAaPmNgNnS	odložit
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
začala	začít	k5eAaPmAgFnS	začít
hledat	hledat	k5eAaImF	hledat
náhradníka	náhradník	k1gMnSc4	náhradník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
by	by	kYmCp3nS	by
turné	turné	k1gNnSc4	turné
jako	jako	k8xC	jako
hostující	hostující	k2eAgMnSc1d1	hostující
zpěvák	zpěvák	k1gMnSc1	zpěvák
dokončil	dokončit	k5eAaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Johnson	Johnson	k1gMnSc1	Johnson
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
odejít	odejít	k5eAaPmF	odejít
z	z	k7c2	z
hudební	hudební	k2eAgFnSc2d1	hudební
scény	scéna	k1gFnSc2	scéna
natrvalo	natrvalo	k6eAd1	natrvalo
a	a	k8xC	a
plánuje	plánovat	k5eAaImIp3nS	plánovat
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
ve	v	k7c6	v
studiích	studio	k1gNnPc6	studio
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vystoupení	vystoupení	k1gNnSc6	vystoupení
v	v	k7c6	v
The	The	k1gFnSc6	The
Howard	Howard	k1gInSc1	Howard
Stern	sternum	k1gNnPc2	sternum
Show	show	k1gFnSc2	show
navíc	navíc	k6eAd1	navíc
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc4	jeho
hluchotu	hluchota	k1gFnSc4	hluchota
nezpůsobilo	způsobit	k5eNaPmAgNnS	způsobit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třicetileté	třicetiletý	k2eAgNnSc4d1	třicetileté
angažmá	angažmá	k1gNnSc4	angažmá
v	v	k7c6	v
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jeho	jeho	k3xOp3gFnSc1	jeho
záliba	záliba	k1gFnSc1	záliba
v	v	k7c6	v
rychlých	rychlý	k2eAgNnPc6d1	rychlé
kolech	kolo	k1gNnPc6	kolo
–	–	k?	–
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
závodě	závod	k1gInSc6	závod
si	se	k3xPyFc3	se
prý	prý	k9	prý
zapomněl	zapomnět	k5eAaImAgMnS	zapomnět
nasadit	nasadit	k5eAaPmF	nasadit
ušní	ušní	k2eAgFnSc2d1	ušní
klapky	klapka	k1gFnSc2	klapka
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
si	se	k3xPyFc3	se
protrhl	protrhnout	k5eAaPmAgInS	protrhnout
levý	levý	k2eAgInSc4d1	levý
ušní	ušní	k2eAgInSc4d1	ušní
bubínek	bubínek	k1gInSc4	bubínek
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
ovšem	ovšem	k9	ovšem
Jim	on	k3xPp3gMnPc3	on
Breuer	Breuer	k1gMnSc1	Breuer
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
bavič	bavič	k1gMnSc1	bavič
a	a	k8xC	a
blízký	blízký	k2eAgMnSc1d1	blízký
přítel	přítel	k1gMnSc1	přítel
Johnsona	Johnson	k1gMnSc2	Johnson
<g/>
,	,	kIx,	,
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zpěvák	zpěvák	k1gMnSc1	zpěvák
svěřil	svěřit	k5eAaPmAgInS	svěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
druhý	druhý	k4xOgInSc4	druhý
posudek	posudek	k1gInSc4	posudek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jeho	jeho	k3xOp3gInSc1	jeho
problém	problém	k1gInSc1	problém
se	se	k3xPyFc4	se
sluchem	sluch	k1gInSc7	sluch
nehodnotí	hodnotit	k5eNaImIp3nS	hodnotit
tak	tak	k9	tak
tragicky	tragicky	k6eAd1	tragicky
jako	jako	k9	jako
ten	ten	k3xDgInSc1	ten
původní	původní	k2eAgInSc1d1	původní
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgMnS	být
Johnson	Johnson	k1gInSc4	Johnson
podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
prakticky	prakticky	k6eAd1	prakticky
vyhozen	vyhodit	k5eAaPmNgInS	vyhodit
z	z	k7c2	z
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
a	a	k8xC	a
od	od	k7c2	od
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kapela	kapela	k1gFnSc1	kapela
odložila	odložit	k5eAaPmAgFnS	odložit
zbývající	zbývající	k2eAgInPc4d1	zbývající
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nikdo	nikdo	k3yNnSc1	nikdo
neozval	ozvat	k5eNaPmAgMnS	ozvat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Breuera	Breuero	k1gNnSc2	Breuero
Brian	Brian	k1gMnSc1	Brian
Johnson	Johnson	k1gMnSc1	Johnson
cítil	cítit	k5eAaImAgMnS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
zbytek	zbytek	k1gInSc4	zbytek
skupiny	skupina	k1gFnSc2	skupina
hodil	hodit	k5eAaImAgMnS	hodit
přes	přes	k7c4	přes
palubu	paluba	k1gFnSc4	paluba
a	a	k8xC	a
již	již	k6eAd1	již
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
hledá	hledat	k5eAaImIp3nS	hledat
trvalou	trvalý	k2eAgFnSc4d1	trvalá
náhradu	náhrada	k1gFnSc4	náhrada
<g/>
.	.	kIx.	.
</s>
<s>
Budoucnost	budoucnost	k1gFnSc1	budoucnost
Briana	Brian	k1gMnSc2	Brian
Johnsona	Johnson	k1gMnSc2	Johnson
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
tak	tak	k6eAd1	tak
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
nejistá	jistý	k2eNgFnSc1d1	nejistá
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
vydal	vydat	k5eAaPmAgInS	vydat
Johnson	Johnson	k1gNnSc4	Johnson
další	další	k2eAgNnSc4d1	další
oficiální	oficiální	k2eAgNnSc4d1	oficiální
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
opět	opět	k6eAd1	opět
zmínil	zmínit	k5eAaPmAgMnS	zmínit
své	svůj	k3xOyFgFnPc4	svůj
zdravotní	zdravotní	k2eAgFnPc4d1	zdravotní
potíže	potíž	k1gFnPc4	potíž
a	a	k8xC	a
neschopnost	neschopnost	k1gFnSc4	neschopnost
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
turné	turné	k1gNnSc6	turné
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
zmínil	zmínit	k5eAaPmAgMnS	zmínit
své	svůj	k3xOyFgInPc4	svůj
plány	plán	k1gInPc4	plán
podílet	podílet	k5eAaImF	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
studiové	studiový	k2eAgFnSc6d1	studiová
tvorbě	tvorba	k1gFnSc6	tvorba
a	a	k8xC	a
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
jeviště	jeviště	k1gNnSc4	jeviště
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnPc1	jeho
sluchové	sluchový	k2eAgFnPc1d1	sluchová
obtíže	obtíž	k1gFnPc1	obtíž
vyřeší	vyřešit	k5eAaPmIp3nP	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
poděkoval	poděkovat	k5eAaPmAgMnS	poděkovat
Angusi	Anguse	k1gFnSc4	Anguse
Youngovi	Young	k1gMnSc3	Young
a	a	k8xC	a
Cliffu	Cliff	k1gMnSc3	Cliff
Williamsovi	Williams	k1gMnSc3	Williams
za	za	k7c4	za
spolupráci	spolupráce	k1gFnSc4	spolupráce
v	v	k7c6	v
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
však	však	k9	však
nechtěla	chtít	k5eNaImAgFnS	chtít
přerušit	přerušit	k5eAaPmF	přerušit
rozjeté	rozjetý	k2eAgNnSc4d1	rozjeté
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
frontmanem	frontman	k1gMnSc7	frontman
nakonec	nakonec	k6eAd1	nakonec
definitivně	definitivně	k6eAd1	definitivně
rozloučila	rozloučit	k5eAaPmAgFnS	rozloučit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
oznámila	oznámit	k5eAaPmAgFnS	oznámit
v	v	k7c6	v
prohlášení	prohlášení	k1gNnSc6	prohlášení
již	již	k6eAd1	již
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
náhradník	náhradník	k1gMnSc1	náhradník
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
turné	turné	k1gNnSc4	turné
Rock	rock	k1gInSc1	rock
or	or	k?	or
Bust	busta	k1gFnPc2	busta
vybrán	vybrán	k2eAgMnSc1d1	vybrán
Axl	Axl	k1gMnSc1	Axl
Rose	Rose	k1gMnSc1	Rose
z	z	k7c2	z
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
<g/>
.	.	kIx.	.
</s>
<s>
Přerušené	přerušený	k2eAgNnSc1d1	přerušené
turné	turné	k1gNnSc1	turné
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
odložené	odložený	k2eAgInPc1d1	odložený
koncerty	koncert	k1gInPc1	koncert
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
přijdou	přijít	k5eAaPmIp3nP	přijít
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
fanoušci	fanoušek	k1gMnPc1	fanoušek
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
byli	být	k5eAaImAgMnP	být
s	s	k7c7	s
výběrem	výběr	k1gInSc7	výběr
zpěváka	zpěvák	k1gMnSc2	zpěvák
spokojeni	spokojen	k2eAgMnPc1d1	spokojen
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
dokonce	dokonce	k9	dokonce
požadovali	požadovat	k5eAaImAgMnP	požadovat
vrácení	vrácení	k1gNnSc3	vrácení
peněz	peníze	k1gInPc2	peníze
za	za	k7c4	za
vstupenky	vstupenka	k1gFnPc4	vstupenka
na	na	k7c4	na
koncerty	koncert	k1gInPc4	koncert
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
o	o	k7c4	o
koncerty	koncert	k1gInPc4	koncert
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Rosem	Ros	k1gMnSc7	Ros
neměli	mít	k5eNaImAgMnP	mít
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
recenzí	recenze	k1gFnPc2	recenze
však	však	k9	však
Axl	Axl	k1gMnSc1	Axl
Rose	Rose	k1gMnSc1	Rose
i	i	k9	i
přes	přes	k7c4	přes
nevíru	nevíra	k1gFnSc4	nevíra
skalních	skalní	k2eAgMnPc2d1	skalní
fanoušků	fanoušek	k1gMnPc2	fanoušek
svoji	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
zvládl	zvládnout	k5eAaPmAgMnS	zvládnout
a	a	k8xC	a
překvapil	překvapit	k5eAaPmAgInS	překvapit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Pavlína	Pavlína	k1gFnSc1	Pavlína
Kindlová	Kindlová	k1gFnSc1	Kindlová
pro	pro	k7c4	pro
Lidovky	Lidovky	k1gFnPc4	Lidovky
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
napsala	napsat	k5eAaPmAgFnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Axl	Axl	k1gMnSc1	Axl
Rose	Rose	k1gMnSc1	Rose
"	"	kIx"	"
<g/>
vdechl	vdechnout	k5eAaPmAgInS	vdechnout
život	život	k1gInSc1	život
starnoucí	starnoucí	k2eAgFnSc2d1	starnoucí
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
a	a	k8xC	a
předvedl	předvést	k5eAaPmAgInS	předvést
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
výkon	výkon	k1gInSc4	výkon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
interview	interview	k1gNnSc6	interview
Rose	Rose	k1gMnSc1	Rose
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
zájem	zájem	k1gInSc4	zájem
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
plnohodnotným	plnohodnotný	k2eAgMnSc7d1	plnohodnotný
členem	člen	k1gMnSc7	člen
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
prozatím	prozatím	k6eAd1	prozatím
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
jeho	on	k3xPp3gNnSc2	on
angažmá	angažmá	k1gNnSc2	angažmá
pro	pro	k7c4	pro
zbývající	zbývající	k2eAgInPc4d1	zbývající
koncerty	koncert	k1gInPc4	koncert
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
Cliff	Cliff	k1gInSc1	Cliff
Williams	Williams	k1gInSc1	Williams
<g/>
,	,	kIx,	,
baskytarista	baskytarista	k1gMnSc1	baskytarista
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
možný	možný	k2eAgInSc4d1	možný
odchod	odchod	k1gInSc4	odchod
do	do	k7c2	do
výslužby	výslužba	k1gFnSc2	výslužba
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
naplánoval	naplánovat	k5eAaBmAgInS	naplánovat
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
probíhajícího	probíhající	k2eAgNnSc2d1	probíhající
turné	turné	k1gNnSc2	turné
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
Angus	Angus	k1gMnSc1	Angus
Young	Young	k1gMnSc1	Young
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
posledním	poslední	k2eAgInSc7d1	poslední
původním	původní	k2eAgInSc7d1	původní
členem	člen	k1gInSc7	člen
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
by	by	kYmCp3nS	by
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
zůstal	zůstat	k5eAaPmAgMnS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
členů	člen	k1gMnPc2	člen
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
.	.	kIx.	.
</s>
<s>
Angus	Angus	k1gMnSc1	Angus
Young	Young	k1gMnSc1	Young
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc1	současnost
<g/>
)	)	kIx)	)
Stevie	Stevie	k1gFnSc1	Stevie
Young	Young	k1gMnSc1	Young
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc1	současnost
<g/>
)	)	kIx)	)
Cliff	Cliff	k1gInSc1	Cliff
Williams	Williams	k1gInSc1	Williams
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
Chris	Chris	k1gFnSc4	Chris
Slade	slad	k1gInSc5	slad
–	–	k?	–
bicí	bicí	k2eAgInSc1d1	bicí
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc1	současnost
<g/>
)	)	kIx)	)
Axl	Axl	k1gMnSc1	Axl
Rose	Rose	k1gMnSc1	Rose
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc1	současnost
<g/>
)	)	kIx)	)
Brian	Brian	k1gMnSc1	Brian
Johnson	Johnson	k1gMnSc1	Johnson
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
[	[	kIx(	[
<g/>
nucená	nucený	k2eAgFnSc1d1	nucená
pauza	pauza	k1gFnSc1	pauza
do	do	k7c2	do
odvolání	odvolání	k1gNnSc2	odvolání
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Malcolm	Malcolm	k1gMnSc1	Malcolm
Young	Young	k1gMnSc1	Young
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Bon	bon	k1gInSc1	bon
Scott	Scott	k1gMnSc1	Scott
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
]	]	kIx)	]
Phil	Phil	k1gInSc1	Phil
Rudd	Rudd	k1gMnSc1	Rudd
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Simon	Simon	k1gMnSc1	Simon
Wright	Wright	k1gMnSc1	Wright
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Mark	Mark	k1gMnSc1	Mark
Evans	Evans	k1gInSc1	Evans
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
–	–	k?	–
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
Dave	Dav	k1gInSc5	Dav
Evans	Evans	k1gInSc4	Evans
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Larry	Larra	k1gFnSc2	Larra
Van	van	k1gInSc1	van
Kriedt	Kriedt	k1gMnSc1	Kriedt
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Colin	Colin	k2eAgInSc1d1	Colin
Burgess	Burgess	k1gInSc1	Burgess
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Neil	Neil	k1gMnSc1	Neil
Smith	Smith	k1gMnSc1	Smith
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Russel	Russel	k1gMnSc1	Russel
Coleman	Coleman	k1gMnSc1	Coleman
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Noel	Noel	k1gMnSc1	Noel
Taylor	Taylor	k1gMnSc1	Taylor
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Peter	Peter	k1gMnSc1	Peter
Clack	Clack	k1gMnSc1	Clack
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Rob	robit	k5eAaImRp2nS	robit
Bailey	Bailea	k1gFnSc2	Bailea
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
George	Georg	k1gInSc2	Georg
Young	Young	k1gInSc1	Young
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
.	.	kIx.	.
</s>
