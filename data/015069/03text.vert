<s>
Fridolin	Fridolin	k2eAgMnSc1d1
Wilhelm	Wilhelm	k1gMnSc1
Volkmann	Volkmann	k1gMnSc1
</s>
<s>
Fridolin	Fridolin	k2eAgMnSc1d1
Wilhelm	Wilhelm	k1gMnSc1
Volkmann	Volkmann	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1822	#num#	k4
Praha-Malá	Praha-Malý	k2eAgFnSc1d1
StranaRakouské	StranaRakouský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1877	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
54	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Praha-Malá	Praha-Malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
Povolání	povolání	k1gNnSc3
</s>
<s>
psycholog	psycholog	k1gMnSc1
a	a	k8xC
filozof	filozof	k1gMnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgInSc2d1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Grundriß	Grundriß	k?
der	drát	k5eAaImRp2nS
Psychologie	psychologie	k1gFnSc2
<g/>
,	,	kIx,
kniha	kniha	k1gFnSc1
od	od	k7c2
rytíře	rytíř	k1gMnSc2
Fridolina	Fridolina	k1gFnSc1
Wilhelma	Wilhelma	k1gFnSc1
Volkmanna	Volkmanna	k1gFnSc1
vydaná	vydaný	k2eAgFnSc1d1
v	v	k7c6
Halle	Halla	k1gFnSc6
roku	rok	k1gInSc2
1856	#num#	k4
</s>
<s>
Fridolin	Fridolin	k2eAgMnSc1d1
Wilhelm	Wilhelm	k1gMnSc1
Volkmann	Volkmann	k1gMnSc1
rytíř	rytíř	k1gMnSc1
z	z	k7c2
Volkmarů	Volkmar	k1gInPc2
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1822	#num#	k4
<g/>
,	,	kIx,
Praha-Malá	Praha-Malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
13	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1877	#num#	k4
<g/>
,	,	kIx,
Praha-Malá	Praha-Malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
filozof	filozof	k1gMnSc1
a	a	k8xC
psycholog	psycholog	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Studoval	studovat	k5eAaImAgMnS
práva	právo	k1gNnPc4
a	a	k8xC
filozofii	filozofie	k1gFnSc4
na	na	k7c6
Karlo-Ferdinandově	Karlo-Ferdinandův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
1845	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1846	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
soukromým	soukromý	k2eAgMnSc7d1
docentem	docent	k1gMnSc7
estetiky	estetika	k1gFnSc2
a	a	k8xC
současně	současně	k6eAd1
suplujícím	suplující	k2eAgMnSc7d1
profesorem	profesor	k1gMnSc7
německé	německý	k2eAgFnSc2d1
jazykovědy	jazykověda	k1gFnSc2
na	na	k7c6
malostranském	malostranský	k2eAgNnSc6d1
a	a	k8xC
staroměstském	staroměstský	k2eAgNnSc6d1
gymnáziu	gymnázium	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
pražské	pražský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1856	#num#	k4
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
mimořádným	mimořádný	k2eAgNnPc3d1
a	a	k8xC
1861	#num#	k4
řádným	řádný	k2eAgMnSc7d1
profesorem	profesor	k1gMnSc7
filozofie	filozofie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1874	#num#	k4
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
korespondujícím	korespondující	k2eAgMnSc7d1
členem	člen	k1gMnSc7
filozoficko-historické	filozoficko-historický	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
císařské	císařský	k2eAgFnSc2d1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
také	také	k9
rytířem	rytíř	k1gMnSc7
rakouského	rakouský	k2eAgInSc2d1
Řádu	řád	k1gInSc2
železné	železný	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
(	(	kIx(
<g/>
zval	zvát	k5eAaImAgMnS
se	se	k3xPyFc4
pak	pak	k6eAd1
také	také	k9
rytířem	rytíř	k1gMnSc7
z	z	k7c2
Volkmaru	Volkmar	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Die	Die	k?
Lehre	Lehr	k1gMnSc5
von	von	k1gInSc4
den	den	k1gInSc4
Elementen	Elementno	k1gNnPc2
der	drát	k5eAaImRp2nS
Psychologie	psychologie	k1gFnSc2
als	als	k?
Wissenschaft	Wissenschaft	k1gMnSc1
<g/>
,	,	kIx,
1850	#num#	k4
</s>
<s>
Grundrisse	Grundrisse	k6eAd1
der	drát	k5eAaImRp2nS
Psychologie	psychologie	k1gFnSc2
vom	vom	k?
Standpunkte	Standpunkt	k1gInSc5
des	des	k1gNnSc7
philosophischen	philosophischen	k2eAgInSc1d1
Realismus	realismus	k1gInSc1
und	und	k?
nach	nach	k1gInSc1
genetischer	genetischra	k1gFnPc2
Methode	Method	k1gInSc5
,	,	kIx,
Halle	Halle	k1gFnSc1
1856	#num#	k4
v	v	k7c4
pozdějších	pozdní	k2eAgMnPc2d2
(	(	kIx(
<g/>
doplňovaných	doplňovaný	k2eAgMnPc2d1
<g/>
)	)	kIx)
vyd	vyd	k?
<g/>
.	.	kIx.
jako	jako	k8xC,k8xS
Lehrbuch	Lehrbuch	k1gMnSc1
der	drát	k5eAaImRp2nS
Psychologie	psychologie	k1gFnSc2
<g/>
,	,	kIx,
Köthen	Köthen	k1gInSc4
1894	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
farnost	farnost	k1gFnSc1
při	při	k7c6
kostele	kostel	k1gInSc6
sv.	sv.	kA
<g/>
Tomáše	Tomáš	k1gMnSc2
na	na	k7c6
Malé	Malé	k2eAgFnSc6d1
Straně	strana	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
úmrtí	úmrtí	k1gNnSc6
a	a	k8xC
pohřbu	pohřeb	k1gInSc6
farnost	farnost	k1gFnSc1
při	při	k7c6
kostele	kostel	k1gInSc6
sv.	sv.	kA
<g/>
Tomáše	Tomáš	k1gMnSc2
na	na	k7c6
Malé	Malé	k2eAgFnSc6d1
Straně	strana	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Fridolin	Fridolin	k2eAgMnSc1d1
Wilhelm	Wilhelm	k1gMnSc1
Volkmann	Volkmann	k1gMnSc1
</s>
<s>
Soupis	soupis	k1gInSc1
pražských	pražský	k2eAgMnPc2d1
domovských	domovský	k2eAgMnPc2d1
příslušníků	příslušník	k1gMnPc2
1830	#num#	k4
<g/>
-	-	kIx~
<g/>
1910	#num#	k4
<g/>
,	,	kIx,
Volkmann	Volkmann	k1gInSc1
<g/>
,	,	kIx,
Wilhelm	Wilhelm	k1gInSc1
1822	#num#	k4
</s>
<s>
Soupis	soupis	k1gInSc1
pražských	pražský	k2eAgMnPc2d1
domovských	domovský	k2eAgMnPc2d1
příslušníků	příslušník	k1gMnPc2
1830	#num#	k4
<g/>
-	-	kIx~
<g/>
1910	#num#	k4
<g/>
,	,	kIx,
Volkmann	Volkmann	k1gMnSc1
<g/>
,	,	kIx,
Wilhelm	Wilhelm	k1gMnSc1
Franz	Franz	k1gInSc4
1794	#num#	k4
otec	otec	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Wilhelm	Wilhelm	k1gMnSc1
Fridolin	Fridolina	k1gFnPc2
Volkmann	Volkmann	k1gMnSc1
v	v	k7c6
Allgemeine	Allgemein	k1gMnSc5
Deutsche	Deutschus	k1gMnSc5
Biographie	Biographius	k1gMnSc5
(	(	kIx(
<g/>
1896	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
mzk	mzk	k?
<g/>
2003205180	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
129576956	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8232	#num#	k4
4586	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
52772087	#num#	k4
</s>
