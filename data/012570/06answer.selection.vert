<s>
Olympus	Olympus	k1gInSc1	Olympus
Mons	Mons	k1gInSc1	Mons
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Hora	hora	k1gFnSc1	hora
Olymp	Olymp	k1gInSc1	Olymp
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
známá	známý	k2eAgFnSc1d1	známá
hora	hora	k1gFnSc1	hora
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
oblasti	oblast	k1gFnSc2	oblast
Tharsis	Tharsis	k1gFnSc2	Tharsis
<g/>
.	.	kIx.	.
</s>
