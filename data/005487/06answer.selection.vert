<s>
Vorvaně	vorvaň	k1gMnSc4	vorvaň
poprvé	poprvé	k6eAd1	poprvé
kategorizoval	kategorizovat	k5eAaBmAgMnS	kategorizovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1758	[number]	k4	1758
Linné	Linná	k1gFnSc2	Linná
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zařadil	zařadit	k5eAaPmAgMnS	zařadit
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Physeter	Physeter	k1gInSc4	Physeter
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgInPc4	čtyři
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
