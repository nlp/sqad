<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Oswalt	Oswalt	k2eAgMnSc1d1	Oswalt
Covey	Covea	k1gFnPc4	Covea
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1946	[number]	k4	1946
ve	v	k7c6	v
Fayetteville	Fayettevilla	k1gFnSc6	Fayettevilla
<g/>
,	,	kIx,	,
Arkansas	Arkansas	k1gInSc1	Arkansas
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
americký	americký	k2eAgMnSc1d1	americký
astronaut	astronaut	k1gMnSc1	astronaut
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
letů	let	k1gInPc2	let
s	s	k7c7	s
raketoplány	raketoplán	k1gInPc7	raketoplán
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
řídící	řídící	k2eAgMnSc1d1	řídící
pracovník	pracovník	k1gMnSc1	pracovník
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
a	a	k8xC	a
výcvik	výcvik	k1gInSc1	výcvik
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
ukončil	ukončit	k5eAaPmAgInS	ukončit
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
(	(	kIx(	(
<g/>
Choctawhatchee	Choctawhatchee	k1gFnSc1	Choctawhatchee
High	High	k1gMnSc1	High
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
Salimar	Salimar	k1gInSc1	Salimar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
Purdueovu	Purdueův	k2eAgFnSc4d1	Purdueův
univerzitu	univerzita	k1gFnSc4	univerzita
(	(	kIx(	(
<g/>
Purdue	Purdue	k1gFnSc4	Purdue
University	universita	k1gFnSc2	universita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
zde	zde	k6eAd1	zde
získal	získat	k5eAaPmAgInS	získat
hodnost	hodnost	k1gFnSc4	hodnost
mistra	mistr	k1gMnSc2	mistr
leteckých	letecký	k2eAgFnPc2d1	letecká
a	a	k8xC	a
astronautických	astronautický	k2eAgFnPc2d1	astronautická
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
se	se	k3xPyFc4	se
po	po	k7c6	po
zaškolení	zaškolení	k1gNnSc6	zaškolení
v	v	k7c6	v
akademii	akademie	k1gFnSc6	akademie
a	a	k8xC	a
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Edwards	Edwardsa	k1gFnPc2	Edwardsa
stal	stát	k5eAaPmAgMnS	stát
velitelem	velitel	k1gMnSc7	velitel
zkušební	zkušební	k2eAgFnSc2d1	zkušební
letky	letka	k1gFnSc2	letka
v	v	k7c4	v
Air	Air	k1gFnSc4	Air
Force	force	k1gFnSc2	force
Center	centrum	k1gNnPc2	centrum
Detachment	Detachment	k1gMnSc1	Detachment
2	[number]	k4	2
Raglin	Raglina	k1gFnPc2	Raglina
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
(	(	kIx(	(
<g/>
manželka	manželka	k1gFnSc1	manželka
Covey	Covea	k1gFnSc2	Covea
<g/>
,	,	kIx,	,
Kathleen	Kathleen	k1gInSc4	Kathleen
rozená	rozený	k2eAgFnSc1d1	rozená
Allbaughová	Allbaughová	k1gFnSc1	Allbaughová
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
mezi	mezi	k7c7	mezi
astronauty	astronaut	k1gMnPc7	astronaut
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
majora	major	k1gMnSc2	major
vojenského	vojenský	k2eAgNnSc2d1	vojenské
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgMnS	používat
přezdívku	přezdívka	k1gFnSc4	přezdívka
Dick	Dick	k1gMnSc1	Dick
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
===	===	k?	===
Lety	léto	k1gNnPc7	léto
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
===	===	k?	===
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
ve	v	k7c4	v
svých	svůj	k3xOyFgNnPc2	svůj
39	[number]	k4	39
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
Kennedyho	Kennedy	k1gMnSc2	Kennedy
vesmírného	vesmírný	k2eAgNnSc2d1	vesmírné
střediska	středisko	k1gNnSc2	středisko
na	na	k7c6	na
mysu	mys	k1gInSc6	mys
Canaveral	Canaveral	k1gFnSc2	Canaveral
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
raketoplánu	raketoplán	k1gInSc2	raketoplán
Discovery	Discovera	k1gFnSc2	Discovera
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
<g/>
:	:	kIx,	:
velitel	velitel	k1gMnSc1	velitel
plk.	plk.	kA	plk.
Joseph	Joseph	k1gMnSc1	Joseph
Engle	Engle	k1gFnSc2	Engle
<g/>
,	,	kIx,	,
pilot	pilot	k1gMnSc1	pilot
pplk.	pplk.	kA	pplk.
Richard	Richard	k1gMnSc1	Richard
Covey	Covea	k1gFnSc2	Covea
a	a	k8xC	a
trojice	trojice	k1gFnSc2	trojice
letových	letový	k2eAgMnPc2d1	letový
specialistů	specialista	k1gMnPc2	specialista
<g/>
:	:	kIx,	:
James	James	k1gInSc1	James
van	van	k1gInSc1	van
Hoften	Hoften	k2eAgInSc1d1	Hoften
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Lounge	Lounge	k1gInSc1	Lounge
a	a	k8xC	a
William	William	k1gInSc1	William
Fisher	Fishra	k1gFnPc2	Fishra
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
letu	let	k1gInSc2	let
STS-51-I	STS-51-I	k1gFnPc2	STS-51-I
vypustili	vypustit	k5eAaPmAgMnP	vypustit
tři	tři	k4xCgFnPc4	tři
družice	družice	k1gFnPc4	družice
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ASC-	ASC-	k1gFnSc1	ASC-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
Aussat	Aussat	k1gFnSc1	Aussat
1	[number]	k4	1
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
Leasat	Leasat	k1gFnSc4	Leasat
4	[number]	k4	4
<g/>
,	,	kIx,	,
známou	známý	k2eAgFnSc4d1	známá
též	též	k9	též
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Syncom	Syncom	k1gInSc1	Syncom
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
zachytili	zachytit	k5eAaPmAgMnP	zachytit
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
družici	družice	k1gFnSc6	družice
Leasat	Leasat	k1gFnSc1	Leasat
3	[number]	k4	3
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
ji	on	k3xPp3gFnSc4	on
opravit	opravit	k5eAaPmF	opravit
a	a	k8xC	a
vrátit	vrátit	k5eAaPmF	vrátit
na	na	k7c4	na
orbitu	orbita	k1gFnSc4	orbita
<g/>
.	.	kIx.	.
</s>
<s>
Přistáli	přistát	k5eAaPmAgMnP	přistát
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Edwards	Edwardsa	k1gFnPc2	Edwardsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
letěl	letět	k5eAaImAgMnS	letět
podruhé	podruhé	k6eAd1	podruhé
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
raketoplán	raketoplán	k1gInSc1	raketoplán
Discovery	Discovera	k1gFnSc2	Discovera
z	z	k7c2	z
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
<g/>
:	:	kIx,	:
Frederick	Frederick	k1gMnSc1	Frederick
Hauck	Hauck	k1gMnSc1	Hauck
<g/>
,	,	kIx,	,
pilot	pilot	k1gMnSc1	pilot
plk.	plk.	kA	plk.
USAF	USAF	kA	USAF
Richard	Richard	k1gMnSc1	Richard
Covey	Covea	k1gFnSc2	Covea
a	a	k8xC	a
tři	tři	k4xCgMnPc1	tři
letoví	letový	k2eAgMnPc1d1	letový
specialisté	specialista	k1gMnPc1	specialista
<g/>
:	:	kIx,	:
pplk.	pplk.	kA	pplk.
David	David	k1gMnSc1	David
Hilmers	Hilmers	k1gInSc1	Hilmers
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Lounge	Loung	k1gFnSc2	Loung
a	a	k8xC	a
George	Georg	k1gInSc2	Georg
Nelson	Nelson	k1gMnSc1	Nelson
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
čtyřdenního	čtyřdenní	k2eAgInSc2d1	čtyřdenní
letu	let	k1gInSc2	let
STS-26	STS-26	k1gFnSc2	STS-26
vypustili	vypustit	k5eAaPmAgMnP	vypustit
družici	družice	k1gFnSc4	družice
typu	typ	k1gInSc2	typ
TDRS	TDRS	kA	TDRS
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tentokrát	tentokrát	k6eAd1	tentokrát
přistáli	přistát	k5eAaImAgMnP	přistát
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Edwards	Edwardsa	k1gFnPc2	Edwardsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
letěl	letět	k5eAaImAgMnS	letět
potřetí	potřetí	k4xO	potřetí
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
v	v	k7c6	v
raketoplánu	raketoplán	k1gInSc6	raketoplán
Atlantis	Atlantis	k1gFnSc1	Atlantis
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc7	jeho
velitelem	velitel	k1gMnSc7	velitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posádce	posádka	k1gFnSc6	posádka
vojenské	vojenský	k2eAgFnSc2d1	vojenská
mise	mise	k1gFnSc2	mise
STS-38	STS-38	k1gMnPc2	STS-38
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
byli	být	k5eAaImAgMnP	být
důstojníci	důstojník	k1gMnPc1	důstojník
Frank	Frank	k1gMnSc1	Frank
Culbertson	Culbertson	k1gMnSc1	Culbertson
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Gemar	Gemar	k1gMnSc1	Gemar
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Meade	Mead	k1gInSc5	Mead
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
Springer	Springer	k1gMnSc1	Springer
<g/>
.	.	kIx.	.
</s>
<s>
Vynesli	vynést	k5eAaPmAgMnP	vynést
a	a	k8xC	a
vypustili	vypustit	k5eAaPmAgMnP	vypustit
desetitunovou	desetitunový	k2eAgFnSc4d1	desetitunová
tajnou	tajný	k2eAgFnSc4d1	tajná
družici	družice	k1gFnSc4	družice
typu	typ	k1gInSc2	typ
AFP-	AFP-	k1gFnSc2	AFP-
<g/>
658	[number]	k4	658
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nevlídnému	vlídný	k2eNgNnSc3d1	nevlídné
počasí	počasí	k1gNnSc3	počasí
u	u	k7c2	u
základny	základna	k1gFnSc2	základna
Edwards	Edwards	k1gInSc1	Edwards
byl	být	k5eAaImAgInS	být
čtyřdenní	čtyřdenní	k2eAgInSc4d1	čtyřdenní
let	let	k1gInSc4	let
ukončen	ukončit	k5eAaPmNgInS	ukončit
na	na	k7c6	na
mysu	mys	k1gInSc6	mys
Canaveral	Canaveral	k1gFnSc2	Canaveral
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počtvrté	počtvrté	k4xO	počtvrté
letěl	letět	k5eAaImAgMnS	letět
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1993	[number]	k4	1993
v	v	k7c6	v
nejnovějším	nový	k2eAgInSc6d3	nejnovější
raketoplánu	raketoplán	k1gInSc6	raketoplán
Endeavour	Endeavour	k1gInSc1	Endeavour
v	v	k7c6	v
misi	mise	k1gFnSc6	mise
STS-	STS-	k1gFnSc1	STS-
<g/>
61	[number]	k4	61
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gNnSc7	jejíž
hlavním	hlavní	k2eAgMnSc7d1	hlavní
(	(	kIx(	(
<g/>
a	a	k8xC	a
splněným	splněný	k2eAgInSc7d1	splněný
úkolem	úkol	k1gInSc7	úkol
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
opravit	opravit	k5eAaPmF	opravit
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
kosmický	kosmický	k2eAgInSc1d1	kosmický
dalekohled	dalekohled	k1gInSc1	dalekohled
HST	HST	kA	HST
(	(	kIx(	(
<g/>
Hubble	Hubble	k1gMnSc5	Hubble
Space	Space	k1gMnSc5	Space
Telescope	Telescop	k1gMnSc5	Telescop
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
<g/>
:	:	kIx,	:
plk.	plk.	kA	plk.
Richard	Richard	k1gMnSc1	Richard
Covey	Covea	k1gMnSc2	Covea
<g/>
,	,	kIx,	,
kpt.	kpt.	k?	kpt.
Kenneth	Kenneth	k1gInSc1	Kenneth
Bowersox	Bowersox	k1gInSc1	Bowersox
<g/>
,	,	kIx,	,
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Franklin	Franklin	k1gInSc1	Franklin
Musgrave	Musgrav	k1gInSc5	Musgrav
<g/>
,	,	kIx,	,
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Kathryn	Kathryn	k1gInSc1	Kathryn
Thorntonová	Thorntonový	k2eAgFnSc1d1	Thorntonová
<g/>
,	,	kIx,	,
západoevropský	západoevropský	k2eAgMnSc1d1	západoevropský
astronaut	astronaut	k1gMnSc1	astronaut
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
původu	původ	k1gInSc2	původ
Claude	Claud	k1gInSc5	Claud
Nicollier	Nicollier	k1gMnSc1	Nicollier
<g/>
,	,	kIx,	,
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jeffrey	Jeffrea	k1gMnSc2	Jeffrea
Hoffman	Hoffman	k1gMnSc1	Hoffman
a	a	k8xC	a
Thomas	Thomas	k1gMnSc1	Thomas
Akers	Akersa	k1gFnPc2	Akersa
<g/>
.	.	kIx.	.
</s>
<s>
Startovali	startovat	k5eAaBmAgMnP	startovat
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
,	,	kIx,	,
let	let	k1gInSc1	let
trval	trvat	k5eAaImAgInS	trvat
10	[number]	k4	10
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
přistáli	přistát	k5eAaPmAgMnP	přistát
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Edwards	Edwardsa	k1gFnPc2	Edwardsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
čtyř	čtyři	k4xCgMnPc2	čtyři
svých	svůj	k3xOyFgMnPc2	svůj
letů	let	k1gInPc2	let
strávil	strávit	k5eAaPmAgInS	strávit
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
26	[number]	k4	26
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zapsán	zapsat	k5eAaPmNgMnS	zapsat
jako	jako	k9	jako
179	[number]	k4	179
<g/>
.	.	kIx.	.
člověk	člověk	k1gMnSc1	člověk
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
STS-51-I	STS-51-I	k4	STS-51-I
Discovery	Discovera	k1gFnPc1	Discovera
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1985	[number]	k4	1985
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
STS-26	STS-26	k4	STS-26
Discovery	Discovera	k1gFnPc1	Discovera
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1988	[number]	k4	1988
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
STS-38	STS-38	k4	STS-38
Atlantis	Atlantis	k1gFnSc1	Atlantis
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
STS-61	STS-61	k4	STS-61
Endeavour	Endeavour	k1gInSc1	Endeavour
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1993	[number]	k4	1993
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
letů	let	k1gInPc2	let
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
z	z	k7c2	z
týmu	tým	k1gInSc2	tým
astronautů	astronaut	k1gMnPc2	astronaut
NASA	NASA	kA	NASA
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
velitelských	velitelský	k2eAgInPc6d1	velitelský
postech	post	k1gInPc6	post
zůstal	zůstat	k5eAaPmAgMnS	zůstat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
období	období	k1gNnSc2	období
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
u	u	k7c2	u
firmy	firma	k1gFnSc2	firma
Boeing	boeing	k1gInSc1	boeing
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
doposud	doposud	k6eAd1	doposud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Richard	Richarda	k1gFnPc2	Richarda
Covey	Covea	k1gFnSc2	Covea
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Richard	Richard	k1gMnSc1	Richard
Covey	Covea	k1gFnSc2	Covea
v	v	k7c6	v
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
SPACE-40	SPACE-40	k1gFnSc2	SPACE-40
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Covey	Covey	k1gInPc1	Covey
<g/>
,	,	kIx,	,
R.	R.	kA	R.
<g/>
O.	O.	kA	O.
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
kosmonautiky	kosmonautika	k1gFnSc2	kosmonautika
</s>
</p>
