<p>
<s>
Dobrovolná	dobrovolný	k2eAgFnSc1d1	dobrovolná
sportovní	sportovní	k2eAgFnSc1d1	sportovní
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
DSO	DSO	kA	DSO
<g/>
,	,	kIx,	,
v	v	k7c6	v
dobovém	dobový	k2eAgInSc6d1	dobový
pravopisu	pravopis	k1gInSc6	pravopis
Dobrovolná	dobrovolný	k2eAgFnSc1d1	dobrovolná
sportovní	sportovní	k2eAgFnSc1d1	sportovní
organisace	organisace	k1gFnSc1	organisace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
Dobrovoľná	Dobrovoľný	k2eAgFnSc1d1	Dobrovoľný
športová	športový	k2eAgFnSc1d1	športový
organizácia	organizácia	k1gFnSc1	organizácia
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
DŠO	DŠO	kA	DŠO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
sportovní	sportovní	k2eAgInSc4d1	sportovní
klub	klub	k1gInSc4	klub
používané	používaný	k2eAgNnSc1d1	používané
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sjednocení	sjednocení	k1gNnSc3	sjednocení
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
důsledku	důsledek	k1gInSc6	důsledek
musely	muset	k5eAaImAgInP	muset
všechny	všechen	k3xTgInPc1	všechen
dosavadní	dosavadní	k2eAgInPc1d1	dosavadní
tělovýchovné	tělovýchovný	k2eAgInPc1d1	tělovýchovný
spolky	spolek	k1gInPc1	spolek
s	s	k7c7	s
veškerým	veškerý	k3xTgInSc7	veškerý
majetkem	majetek	k1gInSc7	majetek
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
Sokola	Sokol	k1gMnSc2	Sokol
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
oficiálně	oficiálně	k6eAd1	oficiálně
zakázán	zakázat	k5eAaPmNgInS	zakázat
profesionální	profesionální	k2eAgInSc1d1	profesionální
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
několik	několik	k4yIc4	několik
výjimek	výjimka	k1gFnPc2	výjimka
jako	jako	k8xS	jako
dostihy	dostih	k1gInPc7	dostih
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
reorganizaci	reorganizace	k1gFnSc3	reorganizace
<g/>
:	:	kIx,	:
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
byl	být	k5eAaImAgInS	být
přijat	přijat	k2eAgInSc1d1	přijat
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
71	[number]	k4	71
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
O	o	k7c6	o
organizaci	organizace	k1gFnSc6	organizace
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgMnSc2	jenž
byl	být	k5eAaImAgInS	být
zřízen	zřízen	k2eAgInSc1d1	zřízen
Státní	státní	k2eAgInSc1d1	státní
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
výchovu	výchova	k1gFnSc4	výchova
a	a	k8xC	a
sport	sport	k1gInSc4	sport
(	(	kIx(	(
<g/>
SVTVS	SVTVS	kA	SVTVS
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
nástroj	nástroj	k1gInSc1	nástroj
centrálního	centrální	k2eAgNnSc2d1	centrální
řízení	řízení	k1gNnSc2	řízení
veškerých	veškerý	k3xTgFnPc2	veškerý
sportovních	sportovní	k2eAgFnPc2d1	sportovní
aktivit	aktivita	k1gFnPc2	aktivita
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
existující	existující	k2eAgInPc1d1	existující
oddíly	oddíl	k1gInPc1	oddíl
byly	být	k5eAaImAgInP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
do	do	k7c2	do
dobrovolných	dobrovolný	k2eAgFnPc2d1	dobrovolná
sportovních	sportovní	k2eAgFnPc2d1	sportovní
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
vytvořených	vytvořený	k2eAgFnPc2d1	vytvořená
podle	podle	k7c2	podle
sovětského	sovětský	k2eAgInSc2d1	sovětský
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
DSO	DSO	kA	DSO
spadal	spadat	k5eAaPmAgInS	spadat
pod	pod	k7c4	pod
Revoluční	revoluční	k2eAgNnSc4d1	revoluční
odborové	odborový	k2eAgNnSc4d1	odborové
hnutí	hnutí	k1gNnSc4	hnutí
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
sportovec	sportovec	k1gMnSc1	sportovec
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
občanského	občanský	k2eAgNnSc2d1	občanské
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
musel	muset	k5eAaImAgMnS	muset
stát	stát	k5eAaPmF	stát
členem	člen	k1gMnSc7	člen
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
devíti	devět	k4xCc2	devět
organizací	organizace	k1gFnPc2	organizace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Spartak	Spartak	k1gInSc1	Spartak
<g/>
:	:	kIx,	:
strojírenství	strojírenství	k1gNnSc2	strojírenství
</s>
</p>
<p>
<s>
Baník	Baník	k1gInSc1	Baník
<g/>
:	:	kIx,	:
doly	dol	k1gInPc1	dol
</s>
</p>
<p>
<s>
Dynamo	dynamo	k1gNnSc1	dynamo
<g/>
:	:	kIx,	:
energetika	energetika	k1gFnSc1	energetika
<g/>
,	,	kIx,	,
spoje	spoj	k1gInPc1	spoj
</s>
</p>
<p>
<s>
Tatran	Tatran	k1gInSc1	Tatran
<g/>
:	:	kIx,	:
těžba	těžba	k1gFnSc1	těžba
a	a	k8xC	a
zpracování	zpracování	k1gNnSc1	zpracování
dřeva	dřevo	k1gNnSc2	dřevo
</s>
</p>
<p>
<s>
Jiskra	jiskra	k1gFnSc1	jiskra
(	(	kIx(	(
<g/>
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
Iskra	Iskr	k1gInSc2	Iskr
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
tzv.	tzv.	kA	tzv.
lehký	lehký	k2eAgInSc1d1	lehký
průmysl	průmysl	k1gInSc1	průmysl
(	(	kIx(	(
<g/>
textilní	textilní	k2eAgInSc4d1	textilní
<g/>
,	,	kIx,	,
sklářský	sklářský	k2eAgInSc4d1	sklářský
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc4d1	chemický
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Slavoj	Slavoj	k1gInSc1	Slavoj
<g/>
:	:	kIx,	:
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
průmysl	průmysl	k1gInSc1	průmysl
</s>
</p>
<p>
<s>
Lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
(	(	kIx(	(
<g/>
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
Lokomotíva	Lokomotívo	k1gNnSc2	Lokomotívo
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
železnice	železnice	k1gFnSc1	železnice
</s>
</p>
<p>
<s>
Slovan	Slovan	k1gMnSc1	Slovan
<g/>
:	:	kIx,	:
státní	státní	k2eAgFnSc1d1	státní
správa	správa	k1gFnSc1	správa
<g/>
,	,	kIx,	,
justice	justice	k1gFnSc1	justice
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc1	zdravotnictví
</s>
</p>
<p>
<s>
Slavia	Slavia	k1gFnSc1	Slavia
(	(	kIx(	(
<g/>
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
Slávia	Slávia	k1gFnSc1	Slávia
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
školství	školství	k1gNnSc1	školství
<g/>
,	,	kIx,	,
věda	věda	k1gFnSc1	věda
</s>
<s>
Označení	Označení	k1gNnSc1	Označení
Sokol	Sokol	k1gMnSc1	Sokol
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
používalo	používat	k5eAaImAgNnS	používat
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
oddíly	oddíl	k1gInPc4	oddíl
venkovské	venkovský	k2eAgFnSc2d1	venkovská
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
také	také	k9	také
DSO	DSO	kA	DSO
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
pro	pro	k7c4	pro
příslušníky	příslušník	k1gMnPc4	příslušník
jednotek	jednotka	k1gFnPc2	jednotka
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
a	a	k8xC	a
ATK	ATK	kA	ATK
(	(	kIx(	(
<g/>
Armádní	armádní	k2eAgInSc1d1	armádní
tělovýchovný	tělovýchovný	k2eAgInSc1d1	tělovýchovný
klub	klub	k1gInSc1	klub
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Dukla	Dukla	k1gFnSc1	Dukla
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
armádní	armádní	k2eAgMnPc4d1	armádní
sportovce	sportovec	k1gMnPc4	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
Učňovské	učňovský	k2eAgFnPc4d1	učňovská
sportovní	sportovní	k2eAgFnPc4d1	sportovní
aktivity	aktivita	k1gFnPc4	aktivita
zastřešovala	zastřešovat	k5eAaImAgFnS	zastřešovat
DSO	DSO	kA	DSO
Pracovní	pracovní	k2eAgFnSc2d1	pracovní
zálohy	záloha	k1gFnSc2	záloha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zúčastňovala	zúčastňovat	k5eAaImAgFnS	zúčastňovat
pouze	pouze	k6eAd1	pouze
soutěží	soutěž	k1gFnPc2	soutěž
v	v	k7c6	v
mládežnických	mládežnický	k2eAgFnPc6d1	mládežnická
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Systém	systém	k1gInSc1	systém
DSO	DSO	kA	DSO
se	se	k3xPyFc4	se
odrazil	odrazit	k5eAaPmAgInS	odrazit
také	také	k9	také
na	na	k7c6	na
podobě	podoba	k1gFnSc6	podoba
ligových	ligový	k2eAgFnPc2d1	ligová
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
z	z	k7c2	z
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
míst	místo	k1gNnPc2	místo
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
soutěže	soutěž	k1gFnSc2	soutěž
Přebor	přebor	k1gInSc1	přebor
československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
1953	[number]	k4	1953
nasazeny	nasazen	k2eAgInPc4d1	nasazen
týmy	tým	k1gInPc4	tým
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
každá	každý	k3xTgFnSc1	každý
DSO	DSO	kA	DSO
měla	mít	k5eAaImAgFnS	mít
svého	svůj	k3xOyFgMnSc4	svůj
zástupce	zástupce	k1gMnSc4	zástupce
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
výsledky	výsledek	k1gInPc4	výsledek
minulého	minulý	k2eAgInSc2d1	minulý
ročníku	ročník	k1gInSc2	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
republiky	republika	k1gFnSc2	republika
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
a	a	k8xC	a
do	do	k7c2	do
I.	I.	kA	I.
ligy	liga	k1gFnSc2	liga
instalován	instalován	k2eAgInSc4d1	instalován
klub	klub	k1gInSc4	klub
Slávia	Slávia	k1gFnSc1	Slávia
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Komplikovaný	komplikovaný	k2eAgInSc1d1	komplikovaný
a	a	k8xC	a
nepřirozený	přirozený	k2eNgInSc1d1	nepřirozený
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
neosvědčil	osvědčit	k5eNaPmAgInS	osvědčit
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
bylo	být	k5eAaImAgNnS	být
všech	všecek	k3xTgMnPc2	všecek
devět	devět	k4xCc4	devět
DSO	DSO	kA	DSO
sloučeno	sloučen	k2eAgNnSc1d1	sloučeno
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
organizace	organizace	k1gFnSc2	organizace
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1957	[number]	k4	1957
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
Československý	československý	k2eAgInSc1d1	československý
svaz	svaz	k1gInSc1	svaz
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
sportu	sport	k1gInSc2	sport
jako	jako	k8xS	jako
řídící	řídící	k2eAgInSc1d1	řídící
sportovní	sportovní	k2eAgInSc1d1	sportovní
orgán	orgán	k1gInSc1	orgán
<g/>
,	,	kIx,	,
kluby	klub	k1gInPc1	klub
působící	působící	k2eAgInPc1d1	působící
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
rámci	rámec	k1gInSc6	rámec
se	se	k3xPyFc4	se
jmenovaly	jmenovat	k5eAaBmAgInP	jmenovat
TJ	tj	kA	tj
(	(	kIx(	(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nesly	nést	k5eAaImAgInP	nést
však	však	k9	však
nadále	nadále	k6eAd1	nadále
název	název	k1gInSc1	název
původní	původní	k2eAgInSc1d1	původní
DSO	DSO	kA	DSO
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
době	doba	k1gFnSc6	doba
politického	politický	k2eAgNnSc2d1	politické
uvolnění	uvolnění	k1gNnSc2	uvolnění
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
některé	některý	k3yIgInPc1	některý
oddíly	oddíl	k1gInPc1	oddíl
vrátily	vrátit	k5eAaPmAgInP	vrátit
k	k	k7c3	k
tradičním	tradiční	k2eAgInPc3d1	tradiční
prvorepublikovým	prvorepublikový	k2eAgInPc3d1	prvorepublikový
názvům	název	k1gInPc3	název
(	(	kIx(	(
<g/>
z	z	k7c2	z
Dynama	dynamo	k1gNnSc2	dynamo
Praha	Praha	k1gFnSc1	Praha
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
SK	Sk	kA	Sk
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
přijaly	přijmout	k5eAaPmAgInP	přijmout
název	název	k1gInSc4	název
patronátního	patronátní	k2eAgInSc2d1	patronátní
podniku	podnik	k1gInSc2	podnik
(	(	kIx(	(
<g/>
Škoda	škoda	k1gFnSc1	škoda
Plzeň	Plzeň	k1gFnSc1	Plzeň
místo	místo	k6eAd1	místo
Spartak	Spartak	k1gInSc1	Spartak
ZVIL	zvít	k5eAaPmAgInS	zvít
Plzeň	Plzeň	k1gFnSc4	Plzeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
městech	město	k1gNnPc6	město
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
malých	malý	k2eAgNnPc2d1	malé
DSO	DSO	kA	DSO
do	do	k7c2	do
jednotného	jednotný	k2eAgInSc2d1	jednotný
týmu	tým	k1gInSc2	tým
(	(	kIx(	(
<g/>
TJ	tj	kA	tj
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
klubů	klub	k1gInPc2	klub
si	se	k3xPyFc3	se
však	však	k9	však
název	název	k1gInSc4	název
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
DSO	DSO	kA	DSO
ponechala	ponechat	k5eAaPmAgFnS	ponechat
i	i	k9	i
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
kvůli	kvůli	k7c3	kvůli
oblibě	obliba	k1gFnSc3	obliba
u	u	k7c2	u
fanoušků	fanoušek	k1gMnPc2	fanoušek
dokonce	dokonce	k9	dokonce
vrátily	vrátit	k5eAaPmAgInP	vrátit
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
působení	působení	k1gNnSc2	působení
pod	pod	k7c7	pod
jiným	jiný	k2eAgNnSc7d1	jiné
jménem	jméno	k1gNnSc7	jméno
(	(	kIx(	(
<g/>
SK	Sk	kA	Sk
Dynamo	dynamo	k1gNnSc1	dynamo
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Horák	Horák	k1gMnSc1	Horák
<g/>
,	,	kIx,	,
Lubomír	Lubomír	k1gMnSc1	Lubomír
Král	Král	k1gMnSc1	Král
<g/>
:	:	kIx,	:
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
našeho	náš	k3xOp1gInSc2	náš
fotbalu	fotbal	k1gInSc2	fotbal
(	(	kIx(	(
<g/>
str	str	kA	str
<g/>
.	.	kIx.	.
255	[number]	k4	255
<g/>
)	)	kIx)	)
−	−	k?	−
Libri	Libri	k1gNnSc1	Libri
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
http://eamos.pf.jcu.cz/amos/kat_tv/modules/low/kurz_text.php?id_kap=33&	[url]	k?	http://eamos.pf.jcu.cz/amos/kat_tv/modules/low/kurz_text.php?id_kap=33&
</s>
</p>
<p>
<s>
https://books.google.cz/books?id=1FhaAgAAQBAJ&	[url]	k?	https://books.google.cz/books?id=1FhaAgAAQBAJ&
</s>
</p>
<p>
<s>
http://odznaky.wz.cz/katalog/tabulky/1952.htm	[url]	k6eAd1	http://odznaky.wz.cz/katalog/tabulky/1952.htm
</s>
</p>
<p>
<s>
Dobrovoľná	Dobrovoľný	k2eAgFnSc1d1	Dobrovoľný
športová	športový	k2eAgFnSc1d1	športový
organizácia	organizácia	k1gFnSc1	organizácia
–	–	k?	–
história	histórium	k1gNnSc2	histórium
vzniku	vznik	k1gInSc3	vznik
</s>
</p>
