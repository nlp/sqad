<s>
Submisivita	Submisivita	k1gFnSc1
</s>
<s>
Submisivita	Submisivita	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
lat.	lat.	k?
sub-mittere	sub-mitter	k1gMnSc5
<g/>
,	,	kIx,
poddávat	poddávat	k5eAaImF
se	se	k3xPyFc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sklon	sklon	k1gInSc1
zaujímat	zaujímat	k5eAaImF
podřízenou	podřízený	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
v	v	k7c6
sociální	sociální	k2eAgFnSc6d1
hiearchii	hiearchie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Být	být	k5eAaImF
submisivní	submisivní	k2eAgInSc4d1
znamená	znamenat	k5eAaImIp3nS
být	být	k5eAaImF
ochotný	ochotný	k2eAgMnSc1d1
se	se	k3xPyFc4
podřizovat	podřizovat	k5eAaImF
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
dominantní	dominantní	k2eAgNnSc1d1
<g/>
,	,	kIx,
poslouchat	poslouchat	k5eAaImF
jeho	jeho	k3xOp3gInPc4
příkazy	příkaz	k1gInPc4
<g/>
,	,	kIx,
nechávat	nechávat	k5eAaImF
rozhodnutí	rozhodnutí	k1gNnPc4
na	na	k7c6
něm	on	k3xPp3gNnSc6
a	a	k8xC
snaha	snaha	k1gFnSc1
ve	v	k7c6
všem	všecek	k3xTgNnSc6
mu	on	k3xPp3gMnSc3
vyhovět	vyhovět	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Submisivní	submisivní	k2eAgInPc1d1
bývají	bývat	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
jsou	být	k5eAaImIp3nP
málo	málo	k4c4
sebevědomí	sebevědomí	k1gNnSc2
<g/>
,	,	kIx,
nevěří	věřit	k5eNaImIp3nP
si	se	k3xPyFc3
a	a	k8xC
spoléhají	spoléhat	k5eAaImIp3nP
se	se	k3xPyFc4
na	na	k7c4
svého	svůj	k3xOyFgMnSc4
dominantního	dominantní	k2eAgMnSc4d1
partnera	partner	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
sice	sice	k8xC
má	mít	k5eAaImIp3nS
hlavní	hlavní	k2eAgNnSc4d1
slovo	slovo	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
něm	on	k3xPp3gInSc6
také	také	k9
leží	ležet	k5eAaImIp3nS
zodpovědnost	zodpovědnost	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
staral	starat	k5eAaImAgMnS
o	o	k7c4
submisivního	submisivní	k2eAgMnSc4d1
partnera	partner	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
submisivity	submisivita	k1gFnSc2
</s>
<s>
K	k	k7c3
úplným	úplný	k2eAgInPc3d1
počátkům	počátek	k1gInPc3
submisivity	submisivita	k1gFnSc2
jako	jako	k8xS,k8xC
takové	takový	k3xDgNnSc1
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
nemožné	možný	k2eNgNnSc1d1
docílit	docílit	k5eAaPmF
<g/>
,	,	kIx,
ovšem	ovšem	k9
tento	tento	k3xDgInSc4
pojem	pojem	k1gInSc4
prokazatelně	prokazatelně	k6eAd1
můžeme	moct	k5eAaImIp1nP
najít	najít	k5eAaPmF
ve	v	k7c6
spojitosti	spojitost	k1gFnSc6
s	s	k7c7
náboženstvím	náboženství	k1gNnSc7
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
u	u	k7c2
Islámu	islám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Definici	definice	k1gFnSc6
Islámu	islám	k1gInSc2
si	se	k3xPyFc3
můžeme	moct	k5eAaImIp1nP
také	také	k9
vyložit	vyložit	k5eAaPmF
jako	jako	k9
dobrovolná	dobrovolný	k2eAgFnSc1d1
submisivita	submisivita	k1gFnSc1
k	k	k7c3
vůli	vůle	k1gFnSc3
boží	božit	k5eAaImIp3nP
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
najít	najít	k5eAaPmF
i	i	k9
ve	v	k7c6
spoustě	spousta	k1gFnSc6
dalších	další	k2eAgNnPc2d1
náboženství	náboženství	k1gNnPc2
<g/>
,	,	kIx,
u	u	k7c2
kterých	který	k3yQgFnPc2,k3yRgFnPc2,k3yIgFnPc2
je	být	k5eAaImIp3nS
submisivita	submisivita	k1gFnSc1
pouze	pouze	k6eAd1
dobrovolná	dobrovolný	k2eAgFnSc1d1
a	a	k8xC
je	být	k5eAaImIp3nS
vnímána	vnímat	k5eAaImNgFnS
vůči	vůči	k7c3
nezpochybnitelnému	zpochybnitelný	k2eNgMnSc3d1
bohu	bůh	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Submisivita	Submisivita	k1gFnSc1
v	v	k7c6
náboženství	náboženství	k1gNnSc6
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
tak	tak	k6eAd1
pokládat	pokládat	k5eAaImF
za	za	k7c4
první	první	k4xOgInSc4
dochovaný	dochovaný	k2eAgInSc4d1
důkaz	důkaz	k1gInSc4
submisivity	submisivita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývoj	vývoj	k1gInSc1
submisivity	submisivita	k1gFnSc2
ale	ale	k8xC
dále	daleko	k6eAd2
pokračuje	pokračovat	k5eAaImIp3nS
hlavně	hlavně	k9
ve	v	k7c6
spojitosti	spojitost	k1gFnSc6
se	s	k7c7
sexuálním	sexuální	k2eAgInSc7d1
životem	život	k1gInSc7
a	a	k8xC
za	za	k7c4
největší	veliký	k2eAgInSc4d3
důkaz	důkaz	k1gInSc4
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
BDSM	BDSM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
BDSM	BDSM	kA
jako	jako	k8xS,k8xC
takové	takový	k3xDgInPc4
už	už	k6eAd1
ve	v	k7c6
společnosti	společnost	k1gFnSc6
funguje	fungovat	k5eAaImIp3nS
od	od	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
možná	možná	k9
ještě	ještě	k9
dříve	dříve	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úplné	úplný	k2eAgInPc1d1
počátky	počátek	k1gInPc1
BDSM	BDSM	kA
(	(	kIx(
<g/>
submisivity	submisivit	k1gInPc1
v	v	k7c6
sexuálních	sexuální	k2eAgInPc6d1
vztazích	vztah	k1gInPc6
<g/>
)	)	kIx)
můžeme	moct	k5eAaImIp1nP
ovšem	ovšem	k9
najít	najít	k5eAaPmF
v	v	k7c6
Kamasutře	Kamasutra	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
knize	kniha	k1gFnSc6
sexu	sex	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Submisivitu	Submisivit	k1gInSc2
můžeme	moct	k5eAaImIp1nP
hojně	hojně	k6eAd1
hledat	hledat	k5eAaImF
i	i	k9
v	v	k7c6
chování	chování	k1gNnSc6
starověkých	starověký	k2eAgMnPc2d1
vládců	vládce	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
sexuálním	sexuální	k2eAgNnPc3d1
mučením	mučení	k1gNnPc3
a	a	k8xC
podobnými	podobný	k2eAgMnPc7d1
praktiky	praktik	k1gMnPc7
dokazovali	dokazovat	k5eAaImAgMnP
jejich	jejich	k3xOp3gFnSc4
dominaci	dominace	k1gFnSc4
ve	v	k7c6
společnosti	společnost	k1gFnSc6
i	i	k8xC
sexuálním	sexuální	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědci	vědec	k1gMnPc1
ale	ale	k9
také	také	k9
mylně	mylně	k6eAd1
tyto	tento	k3xDgFnPc4
sexuální	sexuální	k2eAgFnPc4d1
praktiky	praktika	k1gFnPc4
považovali	považovat	k5eAaImAgMnP
za	za	k7c4
psychopatologii	psychopatologie	k1gFnSc4
a	a	k8xC
domnívali	domnívat	k5eAaImAgMnP
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
neprospívají	prospívat	k5eNaImIp3nP
stabilitě	stabilita	k1gFnSc3
vztahu	vztah	k1gInSc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
ovšem	ovšem	k9
nikdy	nikdy	k6eAd1
nebylo	být	k5eNaImAgNnS
dokázano	dokázana	k1gFnSc5
a	a	k8xC
spíše	spíše	k9
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
žádná	žádný	k3yNgFnSc1
spojitost	spojitost	k1gFnSc1
tam	tam	k6eAd1
není	být	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Submisivita	Submisivita	k1gFnSc1
v	v	k7c6
partnerských	partnerský	k2eAgInPc6d1
vztazích	vztah	k1gInPc6
</s>
<s>
Pohled	pohled	k1gInSc4
biologickým	biologický	k2eAgInSc7d1
esencialismem	esencialismus	k1gInSc7
</s>
<s>
Submisivita	Submisivita	k1gFnSc1
a	a	k8xC
dominance	dominance	k1gFnSc1
v	v	k7c6
partnerských	partnerský	k2eAgInPc6d1
vztazích	vztah	k1gInPc6
značí	značit	k5eAaImIp3nS
míru	míra	k1gFnSc4
nerovnosti	nerovnost	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
panuje	panovat	k5eAaImIp3nS
mezi	mezi	k7c4
partnery	partner	k1gMnPc4
<g/>
,	,	kIx,
tato	tento	k3xDgFnSc1
nerovnost	nerovnost	k1gFnSc1
nadále	nadále	k6eAd1
determinuje	determinovat	k5eAaBmIp3nS
rozložení	rozložení	k1gNnSc4
moci	moc	k1gFnSc2
mezi	mezi	k7c7
partnery	partner	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavení	postavení	k1gNnSc1
partnerů	partner	k1gMnPc2
lze	lze	k6eAd1
vysvětlit	vysvětlit	k5eAaPmF
tzv.	tzv.	kA
biologickým	biologický	k2eAgInSc7d1
esencialismem	esencialismus	k1gInSc7
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
pohled	pohled	k1gInSc1
nahlíží	nahlížet	k5eAaImIp3nS
na	na	k7c4
partnery	partner	k1gMnPc4
z	z	k7c2
biologického	biologický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
a	a	k8xC
přikládá	přikládat	k5eAaImIp3nS
submisivní	submisivní	k2eAgFnSc3d1
a	a	k8xC
dominantní	dominantní	k2eAgFnSc3d1
roli	role	k1gFnSc3
dle	dle	k7c2
pohlaví	pohlaví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ženě	žena	k1gFnSc6
jsou	být	k5eAaImIp3nP
přisuzovány	přisuzován	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
něžnosti	něžnost	k1gFnSc2
<g/>
,	,	kIx,
emocionality	emocionalita	k1gFnSc2
a	a	k8xC
soucitnosti	soucitnost	k1gFnSc2
což	což	k3yRnSc4,k3yQnSc4
lze	lze	k6eAd1
označit	označit	k5eAaPmF
za	za	k7c4
roli	role	k1gFnSc4
ženské	ženský	k2eAgFnSc2d1
submisivity	submisivita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
u	u	k7c2
mužů	muž	k1gMnPc2
pozorujeme	pozorovat	k5eAaImIp1nP
charakterové	charakterový	k2eAgInPc4d1
rysy	rys	k1gInPc4
jako	jako	k8xS,k8xC
odolnost	odolnost	k1gFnSc4
<g/>
,	,	kIx,
sílu	síla	k1gFnSc4
a	a	k8xC
rozhodnost	rozhodnost	k1gFnSc4
neboli	neboli	k8xC
rysy	rys	k1gInPc4
mužské	mužský	k2eAgFnSc2d1
dominance	dominance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
role	role	k1gFnSc1
přímo	přímo	k6eAd1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
následnými	následný	k2eAgFnPc7d1
potřebami	potřeba	k1gFnPc7
novorozeného	novorozený	k2eAgNnSc2d1
dítěte	dítě	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
vlastnosti	vlastnost	k1gFnPc1
ženy	žena	k1gFnPc1
promítají	promítat	k5eAaImIp3nP
do	do	k7c2
nezbytných	zbytný	k2eNgFnPc2d1,k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
pečujícího	pečující	k2eAgMnSc2d1
partnera	partner	k1gMnSc2
–	–	k?
matky	matka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muž	muž	k1gMnSc1
–	–	k?
otec	otec	k1gMnSc1
následně	následně	k6eAd1
zajišťuje	zajišťovat	k5eAaImIp3nS
potřeby	potřeba	k1gFnPc1
dítěte	dítě	k1gNnSc2
již	již	k9
více	hodně	k6eAd2
samostatného	samostatný	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Pohled	pohled	k1gInSc1
individiuální	individiuální	k2eAgFnSc2d1
determinace	determinace	k1gFnSc2
</s>
<s>
Na	na	k7c4
submisivní	submisivní	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
partnerských	partnerský	k2eAgInPc6d1
vztazích	vztah	k1gInPc6
lze	lze	k6eAd1
ovšem	ovšem	k9
nahlížet	nahlížet	k5eAaImF
také	také	k9
z	z	k7c2
pohledu	pohled	k1gInSc2
individuální	individuální	k2eAgFnSc2d1
determinace	determinace	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
role	role	k1gFnPc1
partnerů	partner	k1gMnPc2
nejsou	být	k5eNaImIp3nP
určeny	určit	k5eAaPmNgInP
biologickými	biologický	k2eAgInPc7d1
či	či	k8xC
genderovými	genderův	k2eAgInPc7d1
předpoklady	předpoklad	k1gInPc7
ale	ale	k8xC
individuální	individuální	k2eAgFnSc7d1
povahou	povaha	k1gFnSc7
jedince	jedinec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
může	moct	k5eAaImIp3nS
submisivní	submisivní	k2eAgFnSc4d1
roli	role	k1gFnSc4
zaujmout	zaujmout	k5eAaPmF
muž	muž	k1gMnSc1
a	a	k8xC
dominantním	dominantní	k2eAgMnSc7d1
partnerem	partner	k1gMnSc7
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
stát	stát	k5eAaImF,k5eAaPmF
žena	žena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přijetím	přijetí	k1gNnSc7
takto	takto	k6eAd1
„	„	k?
<g/>
opačných	opačný	k2eAgInPc2d1
<g/>
“	“	k?
rolí	role	k1gFnPc2
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
odklonu	odklon	k1gInSc3
od	od	k7c2
tradičního	tradiční	k2eAgNnSc2d1
partnerského	partnerský	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Sexuální	sexuální	k2eAgFnSc1d1
submisivita	submisivita	k1gFnSc1
<g/>
,	,	kIx,
dominance	dominance	k1gFnSc1
</s>
<s>
Sexuální	sexuální	k2eAgFnSc1d1
dominance	dominance	k1gFnSc1
je	být	k5eAaImIp3nS
projev	projev	k1gInSc4
nadvlády	nadvláda	k1gFnSc2
<g/>
,	,	kIx,
převahy	převaha	k1gFnSc2
jednoho	jeden	k4xCgMnSc4
partnera	partner	k1gMnSc4
nad	nad	k7c4
druhým	druhý	k4xOgNnSc7
při	při	k7c6
pohlavním	pohlavní	k2eAgInSc6d1
styku	styk	k1gInSc6
či	či	k8xC
sexuálních	sexuální	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protějškem	protějšek	k1gInSc7
je	být	k5eAaImIp3nS
submisivita	submisivita	k1gFnSc1
<g/>
,	,	kIx,
neboli	neboli	k8xC
projevy	projev	k1gInPc1
poslušnosti	poslušnost	k1gFnSc2
a	a	k8xC
podřízenosti	podřízenost	k1gFnSc2
partnera	partner	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Osoby	osoba	k1gFnPc1
zaujímají	zaujímat	k5eAaImIp3nP
roli	role	k1gFnSc4
dominy	domino	k1gNnPc7
<g/>
/	/	kIx~
<g/>
dominanta	dominanta	k1gFnSc1
či	či	k8xC
submisiva	submisiva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tzv.	tzv.	kA
„	„	k?
<g/>
switchující	switchující	k2eAgInSc4d1
<g/>
“	“	k?
jedinci	jedinec	k1gMnPc1
pak	pak	k6eAd1
zaujímají	zaujímat	k5eAaImIp3nP
střídavě	střídavě	k6eAd1
obě	dva	k4xCgFnPc1
role	role	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
D	D	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
(	(	kIx(
<g/>
dominance	dominance	k1gFnSc1
a	a	k8xC
submisivita	submisivita	k1gFnSc1
v	v	k7c6
sexu	sex	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
Osoby	osoba	k1gFnPc1
s	s	k7c7
„	„	k?
<g/>
D	D	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
“	“	k?
zaměřením	zaměření	k1gNnSc7
pociťují	pociťovat	k5eAaImIp3nP
silné	silný	k2eAgNnSc4d1
sexuální	sexuální	k2eAgNnSc4d1
vzrušení	vzrušení	k1gNnSc4
když	když	k8xS
prožívají	prožívat	k5eAaImIp3nP
pocit	pocit	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
vůči	vůči	k7c3
svému	svůj	k3xOyFgInSc3
sexuálnímu	sexuální	k2eAgInSc3d1
protějšku	protějšek	k1gInSc3
velmi	velmi	k6eAd1
nadřazeni	nadřazen	k2eAgMnPc1d1
či	či	k8xC
podřízeni	podřízen	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Hraní	hraní	k1gNnSc1
rolí	role	k1gFnPc2
</s>
<s>
Jeden	jeden	k4xCgInSc1
ze	z	k7c2
znaků	znak	k1gInPc2
D	D	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
jedinců	jedinec	k1gMnPc2
<g/>
,	,	kIx,
partneři	partner	k1gMnPc1
zvolí	zvolit	k5eAaPmIp3nP
konkrétní	konkrétní	k2eAgFnSc4d1
submisivní	submisivní	k2eAgFnSc4d1
nebo	nebo	k8xC
dominantní	dominantní	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
,	,	kIx,
mezi	mezi	k7c4
nejběžnější	běžný	k2eAgFnSc4d3
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
role	role	k1gFnSc1
„	„	k?
<g/>
paní	paní	k1gFnSc2
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
otrok	otrok	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
nebo	nebo	k8xC
„	„	k?
<g/>
učitel	učitel	k1gMnSc1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
studentka	studentka	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
své	svůj	k3xOyFgFnSc2
role	role	k1gFnSc2
osoby	osoba	k1gFnSc2
přehánějí	přehánět	k5eAaImIp3nP
a	a	k8xC
zdůrazňují	zdůrazňovat	k5eAaImIp3nP
určité	určitý	k2eAgFnPc4d1
situace	situace	k1gFnPc4
a	a	k8xC
děje	děj	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
souvisejí	souviset	k5eAaImIp3nP
se	s	k7c7
zvolenými	zvolený	k2eAgFnPc7d1
rolemi	role	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
„	„	k?
<g/>
učitel	učitel	k1gMnSc1
kontroluje	kontrolovat	k5eAaImIp3nS
domácí	domácí	k2eAgInPc4d1
úkoly	úkol	k1gInPc4
studentky	studentka	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
paní	paní	k1gFnSc2
trestá	trestat	k5eAaImIp3nS
provinilého	provinilý	k2eAgMnSc4d1
otroka	otrok	k1gMnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Konsenzualita	Konsenzualita	k1gFnSc1
</s>
<s>
Dobrovolný	dobrovolný	k2eAgInSc1d1
souhlas	souhlas	k1gInSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
vstup	vstup	k1gInSc4
do	do	k7c2
dominantně-submisivní	dominantně-submisivní	k2eAgFnSc2d1
hry	hra	k1gFnSc2
nezbytný	nezbytný	k2eAgInSc1d1,k2eNgInSc1d1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
dodržování	dodržování	k1gNnSc4
určitých	určitý	k2eAgInPc2d1
limitů	limit	k1gInPc2
a	a	k8xC
předem	předem	k6eAd1
stanovených	stanovený	k2eAgFnPc2d1
dohod	dohoda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obojí	oboj	k1gFnPc2
omezuje	omezovat	k5eAaImIp3nS
nebezpečí	nebezpečí	k1gNnSc1
nechtěného	chtěný	k2eNgNnSc2d1
přehnaného	přehnaný	k2eAgNnSc2d1
násilí	násilí	k1gNnSc2
či	či	k8xC
ublížení	ublížení	k1gNnSc2
na	na	k7c6
zdraví	zdraví	k1gNnSc6
a	a	k8xC
na	na	k7c6
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Kašáková	Kašáková	k1gFnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Výkladový	výkladový	k2eAgInSc1d1
slovník	slovník	k1gInSc1
pro	pro	k7c4
zdravotní	zdravotní	k2eAgFnPc4d1
sestry	sestra	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc1
Submisivní	submisivní	k2eAgFnSc2d1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
368	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Petrusek	Petrusek	k1gInSc1
<g/>
,	,	kIx,
Velký	velký	k2eAgInSc1d1
sociologický	sociologický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Submise	Submise	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1250	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WEINBERG	WEINBERG	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
S.	S.	kA
<g/>
;	;	kIx,
WILLIAMS	WILLIAMS	kA
<g/>
,	,	kIx,
Colin	Colin	k1gMnSc1
J.	J.	kA
<g/>
;	;	kIx,
MOSER	MOSER	kA
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Social	Social	k1gMnSc1
Constituents	Constituentsa	k1gFnPc2
of	of	k?
Sadomasochism	Sadomasochisma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Social	Social	k1gInSc1
Problems	Problems	k1gInSc4
<g/>
.	.	kIx.
1984	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
31	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
379	#num#	k4
<g/>
–	–	k?
<g/>
389	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
37	#num#	k4
<g/>
-	-	kIx~
<g/>
7791	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.152	10.152	k4
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
sp	sp	k?
<g/>
.1	.1	k4
<g/>
984.31.4.03	984.31.4.03	k4
<g/>
a	a	k8xC
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
20	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KOLMES	KOLMES	kA
<g/>
,	,	kIx,
Keely	Keela	k1gFnSc2
<g/>
;	;	kIx,
STOCK	STOCK	kA
<g/>
,	,	kIx,
Wendy	Wenda	k1gFnSc2
<g/>
;	;	kIx,
MOSER	MOSER	kA
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Investigating	Investigating	k1gInSc1
Bias	Bias	k1gInSc4
in	in	k?
Psychotherapy	Psychotherapa	k1gFnSc2
with	with	k1gMnSc1
BDSM	BDSM	kA
Clients	Clients	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Homosexuality	homosexualita	k1gFnSc2
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
50	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
301	#num#	k4
<g/>
–	–	k?
<g/>
324	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
91	#num#	k4
<g/>
-	-	kIx~
<g/>
8369	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.130	10.130	k4
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
j	j	k?
<g/>
0	#num#	k4
<g/>
82	#num#	k4
<g/>
v	v	k7c4
<g/>
50	#num#	k4
<g/>
n	n	k0
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
_	_	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
E.	E.	kA
Kašáková	Kašáková	k1gFnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Výkladový	výkladový	k2eAgInSc1d1
slovník	slovník	k1gInSc1
pro	pro	k7c4
zdravotní	zdravotní	k2eAgFnPc4d1
sestry	sestra	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Maxdorf	Maxdorf	k1gInSc1
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
M.	M.	kA
Petrusek	Petrusek	k1gMnSc1
(	(	kIx(
<g/>
red	red	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Velký	velký	k2eAgInSc1d1
sociologický	sociologický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Karolinum	Karolinum	k1gNnSc1
1998	#num#	k4
</s>
<s>
M.	M.	kA
Macenauer	Macenauer	k1gMnSc1
<g/>
,	,	kIx,
Dominance	dominance	k1gFnSc1
a	a	k8xC
submisivata	submisivata	k1gFnSc1
jako	jako	k8xC,k8xS
indikátor	indikátor	k1gInSc1
nerovnosti	nerovnost	k1gFnSc2
v	v	k7c6
partnerských	partnerský	k2eAgInPc6d1
vztazích	vztah	k1gInPc6
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
2013	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Psychologie	psychologie	k1gFnSc1
|	|	kIx~
Sociologie	sociologie	k1gFnSc1
</s>
