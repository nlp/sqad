<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
produkt	produkt	k1gInSc1	produkt
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
nebo	nebo	k8xC	nebo
živočišného	živočišný	k2eAgInSc2d1	živočišný
původu	původ	k1gInSc2	původ
a	a	k8xC	a
produkty	produkt	k1gInPc1	produkt
jejich	jejich	k3xOp3gNnSc2	jejich
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
pro	pro	k7c4	pro
výživu	výživa	k1gFnSc4	výživa
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
?	?	kIx.	?
</s>
