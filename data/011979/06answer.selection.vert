<s>
Krmivo	krmivo	k1gNnSc1	krmivo
je	být	k5eAaImIp3nS	být
produkt	produkt	k1gInSc4	produkt
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
nebo	nebo	k8xC	nebo
živočišného	živočišný	k2eAgInSc2d1	živočišný
původu	původ	k1gInSc2	původ
a	a	k8xC	a
produkty	produkt	k1gInPc1	produkt
jejich	jejich	k3xOp3gNnSc2	jejich
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
organické	organický	k2eAgFnPc1d1	organická
nebo	nebo	k8xC	nebo
anorganické	anorganický	k2eAgFnPc1d1	anorganická
látky	látka	k1gFnPc1	látka
jednotlivě	jednotlivě	k6eAd1	jednotlivě
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
směsích	směs	k1gFnPc6	směs
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgFnP	určit
pro	pro	k7c4	pro
výživu	výživa	k1gFnSc4	výživa
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
