<p>
<s>
Krmivo	krmivo	k1gNnSc1	krmivo
je	být	k5eAaImIp3nS	být
produkt	produkt	k1gInSc4	produkt
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
nebo	nebo	k8xC	nebo
živočišného	živočišný	k2eAgInSc2d1	živočišný
původu	původ	k1gInSc2	původ
a	a	k8xC	a
produkty	produkt	k1gInPc1	produkt
jejich	jejich	k3xOp3gNnSc2	jejich
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
organické	organický	k2eAgFnPc1d1	organická
nebo	nebo	k8xC	nebo
anorganické	anorganický	k2eAgFnPc1d1	anorganická
látky	látka	k1gFnPc1	látka
jednotlivě	jednotlivě	k6eAd1	jednotlivě
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
směsích	směs	k1gFnPc6	směs
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgFnP	určit
pro	pro	k7c4	pro
výživu	výživa	k1gFnSc4	výživa
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vysokoprodukční	Vysokoprodukční	k2eAgNnPc1d1	Vysokoprodukční
hospodářská	hospodářský	k2eAgNnPc1d1	hospodářské
zvířata	zvíře	k1gNnPc1	zvíře
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
zdraví	zdraví	k1gNnSc3	zdraví
a	a	k8xC	a
užitkovosti	užitkovost	k1gFnSc3	užitkovost
krmiva	krmivo	k1gNnSc2	krmivo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jim	on	k3xPp3gMnPc3	on
budou	být	k5eAaImBp3nP	být
poskytovat	poskytovat	k5eAaImF	poskytovat
všechny	všechen	k3xTgFnPc4	všechen
potřebné	potřebný	k2eAgFnPc4d1	potřebná
živiny	živina	k1gFnPc4	živina
a	a	k8xC	a
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
chovaná	chovaný	k2eAgNnPc1d1	chované
jako	jako	k8xC	jako
společníci	společník	k1gMnPc1	společník
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
také	také	k9	také
krmena	krmen	k2eAgFnSc1d1	krmena
průmyslově	průmyslově	k6eAd1	průmyslově
vyráběnými	vyráběný	k2eAgNnPc7d1	vyráběné
krmivy	krmivo	k1gNnPc7	krmivo
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
granulí	granule	k1gFnPc2	granule
nebo	nebo	k8xC	nebo
konzerv	konzerva	k1gFnPc2	konzerva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krmivo	krmivo	k1gNnSc1	krmivo
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
mít	mít	k5eAaImF	mít
složení	složení	k1gNnSc1	složení
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
druhu	druh	k1gMnSc6	druh
a	a	k8xC	a
kategorii	kategorie	k1gFnSc6	kategorie
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
;	;	kIx,	;
často	často	k6eAd1	často
se	se	k3xPyFc4	se
ale	ale	k9	ale
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
levnějších	levný	k2eAgNnPc6d2	levnější
krmivech	krmivo	k1gNnPc6	krmivo
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
masožravce	masožravec	k1gMnPc4	masožravec
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
dávku	dávka	k1gFnSc4	dávka
bílkovin	bílkovina	k1gFnPc2	bílkovina
sójový	sójový	k2eAgMnSc1d1	sójový
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
protein	protein	k1gInSc1	protein
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
šelmy	šelma	k1gFnPc1	šelma
špatně	špatně	k6eAd1	špatně
tráví	trávit	k5eAaImIp3nP	trávit
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
do	do	k7c2	do
krmných	krmný	k2eAgFnPc2d1	krmná
dávek	dávka	k1gFnPc2	dávka
pro	pro	k7c4	pro
přežvýkavce	přežvýkavec	k1gMnPc4	přežvýkavec
se	se	k3xPyFc4	se
přidávala	přidávat	k5eAaImAgFnS	přidávat
bílkovina	bílkovina	k1gFnSc1	bílkovina
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
masokostní	masokostní	k2eAgFnSc2d1	masokostní
moučky	moučka	k1gFnSc2	moučka
z	z	k7c2	z
jiných	jiný	k2eAgMnPc2d1	jiný
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
praktiky	praktika	k1gFnPc1	praktika
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zakázány	zakázán	k2eAgInPc1d1	zakázán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
krmiv	krmivo	k1gNnPc2	krmivo
==	==	k?	==
</s>
</p>
<p>
<s>
Krmiva	krmivo	k1gNnSc2	krmivo
pro	pro	k7c4	pro
hospodářská	hospodářský	k2eAgNnPc4d1	hospodářské
zvířata	zvíře	k1gNnPc4	zvíře
můžeme	moct	k5eAaImIp1nP	moct
dělit	dělit	k5eAaImF	dělit
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
hledisek	hledisko	k1gNnPc2	hledisko
<g/>
:	:	kIx,	:
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
obsažených	obsažený	k2eAgFnPc2d1	obsažená
živin	živina	k1gFnPc2	živina
na	na	k7c4	na
sacharidová	sacharidový	k2eAgNnPc4d1	sacharidové
<g/>
,	,	kIx,	,
bílkovinná	bílkovinný	k2eAgNnPc4d1	bílkovinné
a	a	k8xC	a
polobílkovinná	polobílkovinný	k2eAgNnPc4d1	polobílkovinný
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
podílu	podíl	k1gInSc2	podíl
vlákniny	vláknina	k1gFnSc2	vláknina
na	na	k7c4	na
jadrná	jadrný	k2eAgNnPc4d1	jadrné
(	(	kIx(	(
<g/>
málo	málo	k4c1	málo
vlákniny	vláknina	k1gFnSc2	vláknina
<g/>
,	,	kIx,	,
hodně	hodně	k6eAd1	hodně
energie	energie	k1gFnSc2	energie
<g/>
)	)	kIx)	)
a	a	k8xC	a
objemná	objemný	k2eAgFnSc1d1	objemná
(	(	kIx(	(
<g/>
hodně	hodně	k6eAd1	hodně
vlákniny	vláknina	k1gFnPc4	vláknina
<g/>
,	,	kIx,	,
málo	málo	k1gNnSc4	málo
energie	energie	k1gFnSc2	energie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krmiva	krmivo	k1gNnPc1	krmivo
můžeme	moct	k5eAaImIp1nP	moct
hodnotit	hodnotit	k5eAaImF	hodnotit
i	i	k9	i
podle	podle	k7c2	podle
obsahu	obsah	k1gInSc2	obsah
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
suchá	suchý	k2eAgNnPc4d1	suché
<g/>
,	,	kIx,	,
šťavnatá	šťavnatý	k2eAgNnPc1d1	šťavnaté
a	a	k8xC	a
vodnatá	vodnatý	k2eAgNnPc1d1	vodnaté
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
jadrná	jadrný	k2eAgFnSc1d1	jadrná
-	-	kIx~	-
zrniny	zrnina	k1gFnPc1	zrnina
<g/>
,	,	kIx,	,
pokrutiny	pokrutiny	k1gFnPc1	pokrutiny
<g/>
,	,	kIx,	,
extrahované	extrahovaný	k2eAgInPc1d1	extrahovaný
šroty	šrot	k1gInPc1	šrot
<g/>
,	,	kIx,	,
mlýnské	mlýnský	k2eAgInPc1d1	mlýnský
krmné	krmný	k2eAgInPc1d1	krmný
odpady	odpad	k1gInPc1	odpad
<g/>
,	,	kIx,	,
živočišné	živočišný	k2eAgFnPc1d1	živočišná
moučky	moučka	k1gFnPc1	moučka
</s>
</p>
<p>
<s>
objemná	objemný	k2eAgFnSc1d1	objemná
-	-	kIx~	-
pícePodle	pícePodle	k6eAd1	pícePodle
původu	původ	k1gInSc2	původ
se	se	k3xPyFc4	se
krmiva	krmivo	k1gNnPc1	krmivo
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
statková	statkový	k2eAgNnPc4d1	statkové
a	a	k8xC	a
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
<g/>
.	.	kIx.	.
</s>
<s>
Statková	statkový	k2eAgNnPc1d1	statkové
krmiva	krmivo	k1gNnPc1	krmivo
jsou	být	k5eAaImIp3nP	být
vyprodukovány	vyprodukovat	k5eAaPmNgFnP	vyprodukovat
rovnou	rovnou	k6eAd1	rovnou
na	na	k7c6	na
statku	statek	k1gInSc6	statek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
ekonomicky	ekonomicky	k6eAd1	ekonomicky
výhodné	výhodný	k2eAgNnSc1d1	výhodné
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
takových	takový	k3xDgNnPc2	takový
krmiv	krmivo	k1gNnPc2	krmivo
je	být	k5eAaImIp3nS	být
siláž	siláž	k1gFnSc1	siláž
nebo	nebo	k8xC	nebo
senáž	senáž	k1gFnSc1	senáž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
krmiva	krmivo	k1gNnPc1	krmivo
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnPc4	označení
pro	pro	k7c4	pro
zbytky	zbytek	k1gInPc4	zbytek
po	po	k7c6	po
zpracování	zpracování	k1gNnSc6	zpracování
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
krmivo	krmivo	k1gNnSc4	krmivo
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
pokrutiny	pokrutiny	k1gFnPc4	pokrutiny
po	po	k7c6	po
zpracování	zpracování	k1gNnSc6	zpracování
olejnin	olejnina	k1gFnPc2	olejnina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
dělit	dělit	k5eAaImF	dělit
i	i	k9	i
na	na	k7c4	na
rostlinná	rostlinný	k2eAgNnPc4d1	rostlinné
<g/>
,	,	kIx,	,
živočišná	živočišný	k2eAgFnSc1d1	živočišná
a	a	k8xC	a
syntetická	syntetický	k2eAgFnSc1d1	syntetická
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
<g/>
:	:	kIx,	:
píce	píce	k1gFnSc2	píce
<g/>
,	,	kIx,	,
chrásty	chrásta	k1gFnSc2	chrásta
a	a	k8xC	a
natě	nať	k1gFnSc2	nať
<g/>
,	,	kIx,	,
okopaniny	okopanina	k1gFnSc2	okopanina
<g/>
,	,	kIx,	,
krmné	krmný	k2eAgInPc1d1	krmný
stromy	strom	k1gInPc1	strom
a	a	k8xC	a
keře	keř	k1gInPc1	keř
<g/>
,	,	kIx,	,
seno	seno	k1gNnSc1	seno
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Do	do	k7c2	do
živočišných	živočišný	k2eAgMnPc2d1	živočišný
řadíme	řadit	k5eAaImIp1nP	řadit
např.	např.	kA	např.
<g/>
:	:	kIx,	:
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
živočišné	živočišný	k2eAgFnPc1d1	živočišná
moučky	moučka	k1gFnPc1	moučka
<g/>
,	,	kIx,	,
jatečné	jatečný	k2eAgInPc1d1	jatečný
odpady	odpad	k1gInPc1	odpad
<g/>
,	,	kIx,	,
či	či	k8xC	či
exkrementy	exkrement	k1gInPc4	exkrement
<g/>
.	.	kIx.	.
</s>
<s>
Syntetická	syntetický	k2eAgNnPc1d1	syntetické
krmiva	krmivo	k1gNnPc1	krmivo
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
<g/>
:	:	kIx,	:
kompletní	kompletní	k2eAgFnSc6d1	kompletní
krmné	krmný	k2eAgFnSc6d1	krmná
směsi	směs	k1gFnSc6	směs
<g/>
,	,	kIx,	,
doplňkové	doplňkový	k2eAgNnSc4d1	doplňkové
krmné	krmný	k2eAgFnPc4d1	krmná
směsi	směs	k1gFnPc4	směs
<g/>
,	,	kIx,	,
bílkovinné	bílkovinný	k2eAgInPc4d1	bílkovinný
koncentráty	koncentrát	k1gInPc4	koncentrát
<g/>
,	,	kIx,	,
doplňky	doplněk	k1gInPc4	doplněk
biofaktorů	biofaktor	k1gInPc2	biofaktor
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
krmných	krmný	k2eAgFnPc2d1	krmná
dávek	dávka	k1gFnPc2	dávka
se	se	k3xPyFc4	se
přidávají	přidávat	k5eAaImIp3nP	přidávat
i	i	k9	i
anorganické	anorganický	k2eAgFnPc1d1	anorganická
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
soli	sůl	k1gFnPc4	sůl
a	a	k8xC	a
stopové	stopový	k2eAgInPc4d1	stopový
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
premixy	premix	k1gInPc1	premix
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
doplňkové	doplňkový	k2eAgFnPc1d1	doplňková
látky	látka	k1gFnPc1	látka
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
hormony	hormon	k1gInPc1	hormon
nebo	nebo	k8xC	nebo
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Setkáváme	setkávat	k5eAaImIp1nP	setkávat
se	se	k3xPyFc4	se
i	i	k9	i
se	s	k7c7	s
zchutňovadly	zchutňovadlo	k1gNnPc7	zchutňovadlo
nebo	nebo	k8xC	nebo
s	s	k7c7	s
barvivy	barvivo	k1gNnPc7	barvivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
případem	případ	k1gInSc7	případ
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc1	použití
močoviny	močovina	k1gFnSc2	močovina
jako	jako	k8xC	jako
krmiva	krmivo	k1gNnSc2	krmivo
pro	pro	k7c4	pro
polygastrická	polygastrický	k2eAgNnPc4d1	polygastrický
zvířata	zvíře	k1gNnPc4	zvíře
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
přežvýkavce	přežvýkavec	k1gMnSc2	přežvýkavec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Močovinu	močovina	k1gFnSc4	močovina
využijí	využít	k5eAaPmIp3nP	využít
symbiotické	symbiotický	k2eAgFnSc2d1	symbiotická
bakterie	bakterie	k1gFnSc2	bakterie
bachoru	bachor	k1gInSc2	bachor
k	k	k7c3	k
syntéze	syntéza	k1gFnSc3	syntéza
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
následně	následně	k6eAd1	následně
využity	využít	k5eAaPmNgInP	využít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pícniny	pícnina	k1gFnSc2	pícnina
==	==	k?	==
</s>
</p>
<p>
<s>
Plodiny	plodina	k1gFnPc1	plodina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
výživu	výživa	k1gFnSc4	výživa
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
pícniny	pícnina	k1gFnPc1	pícnina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Legislativní	legislativní	k2eAgFnSc1d1	legislativní
úprava	úprava	k1gFnSc1	úprava
==	==	k?	==
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
91	[number]	k4	91
<g/>
/	/	kIx~	/
<g/>
1996	[number]	k4	1996
Sb	sb	kA	sb
O	o	k7c6	o
krmivech	krmivo	k1gNnPc6	krmivo
+	+	kIx~	+
prováděcí	prováděcí	k2eAgFnSc2d1	prováděcí
vyhlášky	vyhláška	k1gFnSc2	vyhláška
</s>
</p>
