<s>
Europium	europium	k1gNnSc1
</s>
<s>
Europium	europium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Xe	Xe	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
f	f	k?
<g/>
7	#num#	k4
6	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Eu	Eu	kA
</s>
<s>
63	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Europium	europium	k1gNnSc1
<g/>
,	,	kIx,
Eu	Eu	k?
<g/>
,	,	kIx,
63	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Europium	europium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
f	f	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-53-1	7440-53-1	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
151,964	151,964	k4
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Xe	Xe	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
f	f	k?
<g/>
7	#num#	k4
6	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,2	1,2	k4
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
5,264	5,264	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
<g/>
;	;	kIx,
<g/>
Hustota	hustota	k1gFnSc1
při	při	k7c6
teplotě	teplota	k1gFnSc6
tání	tání	k1gNnSc2
<g/>
:	:	kIx,
<g/>
5,13	5,13	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
Pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
826	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
0	#num#	k4
<g/>
99,15	99,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
1529	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
802,15	802,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Samarium	samarium	k1gNnSc1
≺	≺	k?
<g/>
Eu	Eu	k?
<g/>
≻	≻	k?
Gadolinium	gadolinium	k1gNnSc4
</s>
<s>
⋎	⋎	k?
<g/>
Am	Am	k1gFnSc1
</s>
<s>
Europium	europium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Eu	Eu	k?
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Europium	europium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
měkký	měkký	k2eAgMnSc1d1
<g/>
,	,	kIx,
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
vnitřně	vnitřně	k6eAd1
přechodný	přechodný	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
člen	člen	k1gInSc1
skupiny	skupina	k1gFnSc2
lanthanoidů	lanthanoid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
této	tento	k3xDgFnSc2
skupiny	skupina	k1gFnSc2
je	být	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
nejžádanějším	žádaný	k2eAgInSc7d3
prvkem	prvek	k1gInSc7
díky	díky	k7c3
svému	svůj	k3xOyFgNnSc3
uplatnění	uplatnění	k1gNnSc3
při	při	k7c6
výrobě	výroba	k1gFnSc6
barevných	barevný	k2eAgFnPc2d1
televizních	televizní	k2eAgFnPc2d1
obrazovek	obrazovka	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
funguje	fungovat	k5eAaImIp3nS
jako	jako	k9
luminofor	luminofor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Europium	europium	k1gNnSc1
je	být	k5eAaImIp3nS
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
měkký	měkký	k2eAgInSc1d1
přechodný	přechodný	k2eAgInSc1d1
kov	kov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
ostatními	ostatní	k2eAgInPc7d1
lanthanoidy	lanthanoid	k1gInPc7
má	mít	k5eAaImIp3nS
největší	veliký	k2eAgInSc1d3
kovový	kovový	k2eAgInSc1d1
poloměr	poloměr	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
je	být	k5eAaImIp3nS
i	i	k9
nejreaktivnější	reaktivní	k2eAgNnSc1d3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
S	s	k7c7
vodou	voda	k1gFnSc7
reaguje	reagovat	k5eAaBmIp3nS
podobně	podobně	k6eAd1
jako	jako	k9
vápník	vápník	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
vzduchu	vzduch	k1gInSc6
hoří	hoře	k1gNnPc2
při	při	k7c6
teplotách	teplota	k1gFnPc6
nad	nad	k7c7
150	#num#	k4
°	°	k?
<g/>
C	C	kA
za	za	k7c2
vzniku	vznik	k1gInSc2
oxidu	oxid	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
2	#num#	k4
Eu	Eu	k?
+	+	kIx~
6	#num#	k4
H2O	H2O	k1gFnSc2
→	→	k?
2	#num#	k4
Eu	Eu	k?
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
3	#num#	k4
+	+	kIx~
3	#num#	k4
H2	H2	k1gFnPc2
</s>
<s>
4	#num#	k4
Eu	Eu	k?
+	+	kIx~
3	#num#	k4
O2	O2	k1gFnSc2
→	→	k?
2	#num#	k4
Eu	Eu	k?
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
</s>
<s>
Od	od	k7c2
ostatních	ostatní	k2eAgInPc2d1
prvků	prvek	k1gInPc2
skupiny	skupina	k1gFnSc2
lanthanoidů	lanthanoid	k1gInPc2
se	se	k3xPyFc4
europium	europium	k1gNnSc1
značně	značně	k6eAd1
odlišuje	odlišovat	k5eAaImIp3nS
nižší	nízký	k2eAgFnSc7d2
teplotou	teplota	k1gFnSc7
tání	tání	k1gNnSc2
i	i	k8xC
varu	var	k1gInSc2
a	a	k8xC
nižší	nízký	k2eAgFnSc7d2
hustotou	hustota	k1gFnSc7
<g/>
,	,	kIx,
než	než	k8xS
jaká	jaký	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
mu	on	k3xPp3gMnSc3
příslušela	příslušet	k5eAaImAgFnS
na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
místě	místo	k1gNnSc6
v	v	k7c6
periodické	periodický	k2eAgFnSc6d1
tabulce	tabulka	k1gFnSc6
prvků	prvek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýraznějším	výrazný	k2eAgInSc7d3
rozdílem	rozdíl	k1gInSc7
je	být	k5eAaImIp3nS
ale	ale	k9
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
kromě	kromě	k7c2
stabilního	stabilní	k2eAgNnSc2d1
mocenství	mocenství	k1gNnSc2
Eu	Eu	k?
<g/>
3	#num#	k4
<g/>
+	+	kIx~
jsou	být	k5eAaImIp3nP
značně	značně	k6eAd1
stálé	stálý	k2eAgFnPc1d1
i	i	k8xC
sloučeniny	sloučenina	k1gFnPc1
dvojmocného	dvojmocný	k2eAgNnSc2d1
europia	europium	k1gNnSc2
Eu	Eu	k?
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
jeho	jeho	k3xOp3gFnPc2
solí	sůl	k1gFnPc2
v	v	k7c6
mocenství	mocenství	k1gNnSc6
Eu	Eu	k?
<g/>
3	#num#	k4
<g/>
+	+	kIx~
jsou	být	k5eAaImIp3nP
značně	značně	k6eAd1
podobné	podobný	k2eAgInPc1d1
sloučeninám	sloučenina	k1gFnPc3
ostatních	ostatní	k2eAgInPc2d1
lanthanoidů	lanthanoid	k1gInPc2
a	a	k8xC
hliníku	hliník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
tyto	tento	k3xDgInPc4
prvky	prvek	k1gInPc4
tvoří	tvořit	k5eAaImIp3nS
například	například	k6eAd1
vysoce	vysoce	k6eAd1
stabilní	stabilní	k2eAgInPc1d1
oxidy	oxid	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
nereagují	reagovat	k5eNaBmIp3nP
s	s	k7c7
vodou	voda	k1gFnSc7
a	a	k8xC
jen	jen	k9
velmi	velmi	k6eAd1
obtížně	obtížně	k6eAd1
se	se	k3xPyFc4
redukují	redukovat	k5eAaBmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
solí	sůl	k1gFnPc2
anorganických	anorganický	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
jsou	být	k5eAaImIp3nP
důležité	důležitý	k2eAgInPc1d1
především	především	k9
fluoridy	fluorid	k1gInPc1
a	a	k8xC
fosforečnany	fosforečnan	k1gInPc1
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnSc1
nerozpustnost	nerozpustnost	k1gFnSc1
ve	v	k7c6
vodě	voda	k1gFnSc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
k	k	k7c3
separaci	separace	k1gFnSc3
lanthanoidů	lanthanoid	k1gInPc2
od	od	k7c2
jiných	jiný	k2eAgInPc2d1
kovových	kovový	k2eAgInPc2d1
iontů	ion	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Europité	Europitý	k2eAgFnPc4d1
soli	sůl	k1gFnPc4
mají	mít	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
narůžovělou	narůžovělý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
jeho	jeho	k3xOp3gFnPc2
solí	sůl	k1gFnPc2
v	v	k7c6
mocenství	mocenství	k1gNnSc6
Eu	Eu	k?
<g/>
2	#num#	k4
<g/>
+	+	kIx~
jsou	být	k5eAaImIp3nP
značně	značně	k6eAd1
podobné	podobný	k2eAgInPc1d1
sloučeninám	sloučenina	k1gFnPc3
vápníku	vápník	k1gInSc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
kovů	kov	k1gInPc2
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitou	důležitý	k2eAgFnSc7d1
vlastností	vlastnost	k1gFnSc7
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
rozpustnost	rozpustnost	k1gFnSc4
hydroxidu	hydroxid	k1gInSc2
Eu	Eu	k?
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
,	,	kIx,
čehož	což	k3yRnSc2,k3yQnSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
využít	využít	k5eAaPmF
k	k	k7c3
oddělení	oddělení	k1gNnSc3
od	od	k7c2
zbylých	zbylý	k2eAgInPc2d1
lanthanoidů	lanthanoid	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
hydroxidy	hydroxid	k1gInPc1
jsou	být	k5eAaImIp3nP
ve	v	k7c6
vodě	voda	k1gFnSc6
prakticky	prakticky	k6eAd1
nerozpustné	rozpustný	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soli	sůl	k1gFnSc2
Eu	Eu	k?
<g/>
2	#num#	k4
<g/>
+	+	kIx~
jsou	být	k5eAaImIp3nP
bezbarvé	bezbarvý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
objevu	objev	k1gInSc2
</s>
<s>
Roku	rok	k1gInSc2
1890	#num#	k4
objevil	objevit	k5eAaPmAgMnS
Paul-Émile	Paul-Émila	k1gFnSc3
Lecoq	Lecoq	k1gMnSc1
de	de	k?
Boisbaudran	Boisbaudran	k1gInSc1
neznámé	neznámá	k1gFnSc2
emisní	emisní	k2eAgFnSc2d1
čáry	čára	k1gFnSc2
ve	v	k7c6
spektru	spektrum	k1gNnSc6
frakce	frakce	k1gFnSc2
ze	z	k7c2
separace	separace	k1gFnSc2
prvků	prvek	k1gInPc2
vzácných	vzácný	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
s	s	k7c7
převahou	převaha	k1gFnSc7
samaria	samarium	k1gNnSc2
a	a	k8xC
gadolinia	gadolinium	k1gNnSc2
a	a	k8xC
přiřadil	přiřadit	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
doposud	doposud	k6eAd1
neznámému	známý	k2eNgInSc3d1
prvku	prvek	k1gInSc3
z	z	k7c2
řady	řada	k1gFnSc2
lanthanoidů	lanthanoid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Izolaci	izolace	k1gFnSc4
čistého	čistý	k2eAgInSc2d1
prvku	prvek	k1gInSc2
provedl	provést	k5eAaPmAgMnS
teprve	teprve	k6eAd1
roku	rok	k1gInSc2
1901	#num#	k4
francouzský	francouzský	k2eAgInSc1d1
chemik	chemik	k1gMnSc1
Eugè	Eugè	k1gFnSc3
Demarçay	Demarça	k1gMnPc7
a	a	k8xC
pojmenoval	pojmenovat	k5eAaPmAgMnS
jej	on	k3xPp3gNnSc4
po	po	k7c6
kontinentu	kontinent	k1gInSc6
Evropa	Evropa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
a	a	k8xC
výroba	výroba	k1gFnSc1
</s>
<s>
Europium	europium	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
obsaženo	obsáhnout	k5eAaPmNgNnS
pouze	pouze	k6eAd1
v	v	k7c6
koncentraci	koncentrace	k1gFnSc6
asi	asi	k9
1,2	1,2	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
<g/>
,	,	kIx,
o	o	k7c6
jeho	jeho	k3xOp3gInSc6
obsahu	obsah	k1gInSc6
v	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
údaje	údaj	k1gInPc1
chybí	chybět	k5eAaImIp3nP,k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vesmíru	vesmír	k1gInSc6
připadá	připadat	k5eAaPmIp3nS,k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
atom	atom	k1gInSc4
europia	europium	k1gNnSc2
na	na	k7c4
400	#num#	k4
miliard	miliarda	k4xCgFnPc2
atomů	atom	k1gInPc2
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
europium	europium	k1gNnSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
ve	v	k7c6
formě	forma	k1gFnSc6
sloučenin	sloučenina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neexistují	existovat	k5eNaImIp3nP
však	však	k9
ani	ani	k9
minerály	minerál	k1gInPc1
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
by	by	kYmCp3nS
se	se	k3xPyFc4
některé	některý	k3yIgFnPc1
lanthanoidy	lanthanoida	k1gFnPc1
(	(	kIx(
<g/>
prvky	prvek	k1gInPc1
vzácných	vzácný	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
<g/>
)	)	kIx)
vyskytovaly	vyskytovat	k5eAaImAgInP
samostatně	samostatně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
vždy	vždy	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
minerály	minerál	k1gInPc4
směsné	směsný	k2eAgInPc4d1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
obsahují	obsahovat	k5eAaImIp3nP
prakticky	prakticky	k6eAd1
všechny	všechen	k3xTgInPc1
prvky	prvek	k1gInPc1
této	tento	k3xDgFnSc2
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejznámější	známý	k2eAgInPc4d3
patří	patřit	k5eAaImIp3nS
monazity	monazit	k1gInPc4
(	(	kIx(
<g/>
Ce	Ce	k1gFnSc1
<g/>
,	,	kIx,
La	la	k1gNnSc1
<g/>
,	,	kIx,
Th	Th	k1gFnSc1
<g/>
,	,	kIx,
Nd	Nd	k1gFnSc1
<g/>
,	,	kIx,
Y	Y	kA
<g/>
)	)	kIx)
<g/>
PO	Po	kA
<g/>
4	#num#	k4
a	a	k8xC
xenotim	xenotim	k1gInSc1
<g/>
,	,	kIx,
chemicky	chemicky	k6eAd1
fosforečnany	fosforečnan	k1gInPc1
lanthanoidů	lanthanoid	k1gInPc2
,	,	kIx,
dále	daleko	k6eAd2
bastnäsity	bastnäsit	k1gInPc1
(	(	kIx(
<g/>
Ce	Ce	k1gFnSc1
<g/>
,	,	kIx,
La	la	k1gNnSc1
<g/>
,	,	kIx,
Y	Y	kA
<g/>
)	)	kIx)
<g/>
CO	co	k8xS
<g/>
3	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
F	F	kA
<g/>
–	–	k?
směsné	směsný	k2eAgInPc4d1
flourouhličitany	flourouhličitan	k1gInPc4
prvků	prvek	k1gInPc2
vzácných	vzácný	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
a	a	k8xC
např.	např.	kA
minerál	minerál	k1gInSc1
samarskit	samarskit	k1gInSc1
((	((	k?
<g/>
Y	Y	kA
<g/>
,	,	kIx,
<g/>
Ce	Ce	k1gMnSc1
<g/>
,	,	kIx,
<g/>
U	U	kA
<g/>
,	,	kIx,
<g/>
Fe	Fe	k1gFnSc1
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
(	(	kIx(
<g/>
Nb	Nb	k1gFnSc1
<g/>
,	,	kIx,
<g/>
Ta	ten	k3xDgFnSc1
<g/>
,	,	kIx,
<g/>
Ti	ty	k3xPp2nSc3
<g/>
)	)	kIx)
<g/>
5	#num#	k4
<g/>
O	o	k7c4
<g/>
16	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Velká	velký	k2eAgNnPc1d1
ložiska	ložisko	k1gNnPc1
těchto	tento	k3xDgFnPc2
rud	ruda	k1gFnPc2
se	se	k3xPyFc4
nalézají	nalézat	k5eAaImIp3nP
ve	v	k7c4
Skandinávii	Skandinávie	k1gFnSc4
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
Číně	Čína	k1gFnSc6
a	a	k8xC
Vietnamu	Vietnam	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významným	významný	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
jsou	být	k5eAaImIp3nP
i	i	k9
fosfátové	fosfátový	k2eAgFnPc1d1
suroviny	surovina	k1gFnPc1
–	–	k?
apatity	apatit	k1gInPc1
z	z	k7c2
poloostrova	poloostrov	k1gInSc2
Kola	kolo	k1gNnSc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
byl	být	k5eAaImAgInS
ohlášen	ohlášet	k5eAaImNgInS,k5eAaPmNgInS
nález	nález	k1gInSc1
ložiska	ložisko	k1gNnSc2
bohatého	bohatý	k2eAgNnSc2d1
na	na	k7c6
yttrium	yttrium	k1gNnSc4
<g/>
,	,	kIx,
dysprosium	dysprosium	k1gNnSc4
<g/>
,	,	kIx,
europium	europium	k1gNnSc4
a	a	k8xC
terbium	terbium	k1gNnSc4
poblíž	poblíž	k7c2
japonského	japonský	k2eAgInSc2d1
ostrůvku	ostrůvek	k1gInSc2
Minamitori	Minamitor	k1gFnSc2
(	(	kIx(
<g/>
asi	asi	k9
1	#num#	k4
850	#num#	k4
km	km	kA
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
Tokia	Tokio	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
omezené	omezený	k2eAgFnSc3d1
dostupnosti	dostupnost	k1gFnSc3
hrozí	hrozit	k5eAaImIp3nS
v	v	k7c6
nejbližších	blízký	k2eAgInPc6d3
letech	let	k1gInPc6
kritický	kritický	k2eAgInSc1d1
nedostatek	nedostatek	k1gInSc1
zdrojů	zdroj	k1gInPc2
prvku	prvek	k1gInSc2
pro	pro	k7c4
technologické	technologický	k2eAgNnSc4d1
využití	využití	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Výše	vysoce	k6eAd2
uvedený	uvedený	k2eAgInSc1d1
nález	nález	k1gInSc1
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
tuto	tento	k3xDgFnSc4
situaci	situace	k1gFnSc4
změnit	změnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
průmyslové	průmyslový	k2eAgFnSc6d1
výrobě	výroba	k1gFnSc6
prvků	prvek	k1gInPc2
vzácných	vzácný	k2eAgInPc2d1
se	se	k3xPyFc4
jejich	jejich	k3xOp3gFnPc1
rudy	ruda	k1gFnPc1
nejprve	nejprve	k6eAd1
louží	loužit	k5eAaImIp3nP
směsí	směs	k1gFnSc7
kyseliny	kyselina	k1gFnSc2
sírové	sírový	k2eAgFnSc2d1
a	a	k8xC
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1
a	a	k8xC
ze	z	k7c2
vzniklého	vzniklý	k2eAgInSc2d1
roztoku	roztok	k1gInSc2
solí	solit	k5eAaImIp3nS
se	s	k7c7
přídavkem	přídavek	k1gInSc7
hydroxidu	hydroxid	k1gInSc2
sodného	sodný	k2eAgInSc2d1
vysráží	vysrážet	k5eAaPmIp3nS
hydroxidy	hydroxid	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Separace	separace	k1gFnSc1
jednotlivých	jednotlivý	k2eAgInPc2d1
prvků	prvek	k1gInPc2
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
řadou	řada	k1gFnSc7
různých	různý	k2eAgInPc2d1
postupů	postup	k1gInPc2
–	–	k?
kapalinovou	kapalinový	k2eAgFnSc7d1
extrakcí	extrakce	k1gFnSc7
<g/>
,	,	kIx,
za	za	k7c4
použití	použití	k1gNnSc4
ionexových	ionexový	k2eAgFnPc2d1
kolon	kolona	k1gFnPc2
nebo	nebo	k8xC
selektivním	selektivní	k2eAgNnSc7d1
srážením	srážení	k1gNnSc7
nerozpustných	rozpustný	k2eNgFnPc2d1
komplexních	komplexní	k2eAgFnPc2d1
solí	sůl	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Příprava	příprava	k1gFnSc1
čistého	čistý	k2eAgInSc2d1
kovu	kov	k1gInSc2
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
provádí	provádět	k5eAaImIp3nS
elektrolýzou	elektrolýza	k1gFnSc7
směsi	směs	k1gFnSc2
roztavených	roztavený	k2eAgInPc2d1
chloridů	chlorid	k1gInPc2
europitého	europitý	k2eAgInSc2d1
EuCl	EuCla	k1gFnPc2
<g/>
3	#num#	k4
<g/>
,	,	kIx,
vápenatého	vápenatý	k2eAgInSc2d1
CaCl	CaCl	k1gMnSc1
<g/>
2	#num#	k4
a	a	k8xC
sodného	sodný	k2eAgInSc2d1
NaCl	NaCl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
postupech	postup	k1gInPc6
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
i	i	k9
redukce	redukce	k1gFnSc1
oxidu	oxid	k1gInSc2
europia	europium	k1gNnSc2
Eu	Eu	k?
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
elementárním	elementární	k2eAgInSc7d1
lanthanem	lanthan	k1gInSc7
<g/>
:	:	kIx,
</s>
<s>
Eu	Eu	k?
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
+	+	kIx~
2	#num#	k4
La	la	k1gNnPc2
→	→	k?
2	#num#	k4
Eu	Eu	k?
+	+	kIx~
La	la	k1gNnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
</s>
<s>
Použití	použití	k1gNnSc1
a	a	k8xC
sloučeniny	sloučenina	k1gFnPc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
většina	většina	k1gFnSc1
vyrobeného	vyrobený	k2eAgNnSc2d1
europia	europium	k1gNnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
zpracovávána	zpracováván	k2eAgFnSc1d1
při	při	k7c6
výrobě	výroba	k1gFnSc6
barevných	barevný	k2eAgFnPc2d1
televizních	televizní	k2eAgFnPc2d1
obrazovek	obrazovka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
se	se	k3xPyFc4
na	na	k7c4
vnitřní	vnitřní	k2eAgFnSc4d1
stěnu	stěna	k1gFnSc4
obrazovky	obrazovka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
vlastně	vlastně	k9
katodovou	katodový	k2eAgFnSc7d1
trubicí	trubice	k1gFnSc7
<g/>
,	,	kIx,
nanášejí	nanášet	k5eAaImIp3nP
látky	látka	k1gFnPc4
–	–	k?
luminofory	luminofor	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
po	po	k7c6
dopadu	dopad	k1gInSc6
urychleného	urychlený	k2eAgInSc2d1
elektronu	elektron	k1gInSc2
vydávají	vydávat	k5eAaImIp3nP,k5eAaPmIp3nP
elektromagnetické	elektromagnetický	k2eAgNnSc4d1
záření	záření	k1gNnSc4
ve	v	k7c6
viditelné	viditelný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
spektra	spektrum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
luminoforů	luminofor	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
vzájemně	vzájemně	k6eAd1
liší	lišit	k5eAaImIp3nS
intenzitou	intenzita	k1gFnSc7
a	a	k8xC
barvou	barva	k1gFnSc7
emitovaného	emitovaný	k2eAgNnSc2d1
záření	záření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
trojmocné	trojmocný	k2eAgNnSc4d1
europium	europium	k1gNnSc4
Eu	Eu	k?
<g/>
3	#num#	k4
<g/>
+	+	kIx~
se	se	k3xPyFc4
nejčastěji	často	k6eAd3
jako	jako	k8xC,k8xS
nosič	nosič	k1gInSc1
používá	používat	k5eAaImIp3nS
směsný	směsný	k2eAgInSc1d1
oxid	oxid	k1gInSc1
a	a	k8xC
sulfid	sulfid	k1gInSc1
yttria	yttrium	k1gNnSc2
Y	Y	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
2	#num#	k4
<g/>
S.	S.	kA
Jako	jako	k8xS,k8xC
nosič	nosič	k1gInSc1
dvojmocného	dvojmocný	k2eAgNnSc2d1
europia	europium	k1gNnSc2
Eu	Eu	k?
<g/>
2	#num#	k4
<g/>
+	+	kIx~
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
sloučenina	sloučenina	k1gFnSc1
BaFBr	BaFBra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Luminofory	luminofor	k1gInPc4
na	na	k7c6
bázi	báze	k1gFnSc6
Eu	Eu	k?
<g/>
3	#num#	k4
<g/>
+	+	kIx~
vydávají	vydávat	k5eAaPmIp3nP,k5eAaImIp3nP
červené	červený	k2eAgNnSc1d1
záření	záření	k1gNnSc1
<g/>
,	,	kIx,
luminofory	luminofor	k1gInPc1
obsahující	obsahující	k2eAgInSc1d1
Eu	Eu	k?
<g/>
2	#num#	k4
<g/>
+	+	kIx~
emitují	emitovat	k5eAaBmIp3nP
světlo	světlo	k1gNnSc4
modro-fialové	modro-fialové	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ze	z	k7c2
sloučenin	sloučenina	k1gFnPc2
europia	europium	k1gNnSc2
se	se	k3xPyFc4
také	také	k9
vyrábějí	vyrábět	k5eAaImIp3nP
luminiscenční	luminiscenční	k2eAgNnPc1d1
barviva	barvivo	k1gNnPc1
a	a	k8xC
hmoty	hmota	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
schopny	schopen	k2eAgFnPc1d1
světélkovat	světélkovat	k5eAaImF
až	až	k9
po	po	k7c4
dobu	doba	k1gFnSc4
několika	několik	k4yIc2
dnů	den	k1gInPc2
po	po	k7c6
několikaminutovém	několikaminutový	k2eAgNnSc6d1
ozáření	ozáření	k1gNnSc6
světlem	světlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
GREENWOOD	GREENWOOD	kA
<g/>
,	,	kIx,
Norman	Norman	k1gMnSc1
Neill	Neill	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
1	#num#	k4
<g/>
..	..	k?
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Informatorium	Informatorium	k1gNnSc1
793	#num#	k4
s.	s.	k?
<g/>
,	,	kIx,
1	#num#	k4
příl	příl	k1gInSc1
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85427	#num#	k4
<g/>
-	-	kIx~
<g/>
38	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85427	#num#	k4
<g/>
-	-	kIx~
<g/>
38	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
320245801	#num#	k4
S.	S.	kA
1531	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
The	The	k1gMnSc1
tremendous	tremendous	k1gMnSc1
potential	potential	k1gMnSc1
of	of	k?
deep-sea	deep-sea	k1gMnSc1
mud	mud	k?
as	as	k1gInSc1
a	a	k8xC
source	source	k1gMnSc1
of	of	k?
rare-earth	rare-earth	k1gInSc1
elements	elementsa	k1gFnPc2
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2018	#num#	k4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Energy	Energ	k1gInPc4
Department	department	k1gInSc1
Releases	Releases	k1gMnSc1
New	New	k1gMnSc1
Critical	Critical	k1gMnSc1
Materials	Materials	k1gInSc4
Strategy	Stratega	k1gFnSc2
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2010	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
<g/>
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1993	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
europium	europium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
europium	europium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4153210-7	4153210-7	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85045863	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85045863	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
