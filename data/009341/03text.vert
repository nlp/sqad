<p>
<s>
Monoteismus	monoteismus	k1gInSc1	monoteismus
(	(	kIx(	(
<g/>
možno	možno	k6eAd1	možno
též	též	k9	též
psát	psát	k5eAaImF	psát
i	i	k9	i
jako	jako	k9	jako
monotheismus	monotheismus	k1gInSc4	monotheismus
či	či	k8xC	či
monoteizmus	monoteizmus	k1gInSc4	monoteizmus
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
jednobožství	jednobožství	k1gNnSc1	jednobožství
je	být	k5eAaImIp3nS	být
forma	forma	k1gFnSc1	forma
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
lidmi	člověk	k1gMnPc7	člověk
uctíván	uctívat	k5eAaImNgMnS	uctívat
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgMnSc1d1	jediný
Bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrazuje	vyhrazovat	k5eAaImIp3nS	vyhrazovat
toto	tento	k3xDgNnSc4	tento
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
monopolizuje	monopolizovat	k5eAaBmIp3nS	monopolizovat
ho	on	k3xPp3gInSc4	on
a	a	k8xC	a
odmítá	odmítat	k5eAaImIp3nS	odmítat
pluralitu	pluralita	k1gFnSc4	pluralita
božských	božský	k2eAgFnPc2d1	božská
bytostí	bytost	k1gFnPc2	bytost
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
případnou	případný	k2eAgFnSc4d1	případná
hierarchii	hierarchie	k1gFnSc4	hierarchie
typickou	typický	k2eAgFnSc4d1	typická
pro	pro	k7c4	pro
náboženství	náboženství	k1gNnSc4	náboženství
polyteistická	polyteistický	k2eAgFnSc1d1	polyteistická
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
proti	proti	k7c3	proti
nim	on	k3xPp3gInPc3	on
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
záměrné	záměrný	k2eAgFnSc6d1	záměrná
aktivní	aktivní	k2eAgFnSc6d1	aktivní
opozici	opozice	k1gFnSc6	opozice
(	(	kIx(	(
<g/>
fundamentalismus	fundamentalismus	k1gInSc1	fundamentalismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Složitější	složitý	k2eAgFnPc1d2	složitější
společnosti	společnost	k1gFnPc1	společnost
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
příčinou	příčina	k1gFnSc7	příčina
vzniku	vznik	k1gInSc2	vznik
větších	veliký	k2eAgMnPc2d2	veliký
a	a	k8xC	a
moralističtějších	moralistický	k2eAgMnPc2d2	moralistický
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgFnPc7d3	nejvýznamnější
monoteistickými	monoteistický	k2eAgFnPc7d1	monoteistická
náboženstvími	náboženství	k1gNnPc7	náboženství
jsou	být	k5eAaImIp3nP	být
judaismus	judaismus	k1gInSc1	judaismus
<g/>
,	,	kIx,	,
křesťanství	křesťanství	k1gNnSc1	křesťanství
<g/>
,	,	kIx,	,
islám	islám	k1gInSc1	islám
a	a	k8xC	a
bahaismus	bahaismus	k1gInSc1	bahaismus
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
známý	známý	k2eAgInSc4d1	známý
případ	případ	k1gInSc4	případ
monoteistického	monoteistický	k2eAgNnSc2d1	monoteistické
náboženství	náboženství	k1gNnSc2	náboženství
je	být	k5eAaImIp3nS	být
pokládán	pokládán	k2eAgInSc1d1	pokládán
kult	kult	k1gInSc1	kult
slunečního	sluneční	k2eAgMnSc2d1	sluneční
boha	bůh	k1gMnSc2	bůh
Atona	Aton	k1gMnSc2	Aton
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
byl	být	k5eAaImAgInS	být
prosazován	prosazovat	k5eAaImNgInS	prosazovat
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
staroegyptského	staroegyptský	k2eAgMnSc2d1	staroegyptský
panovníka	panovník	k1gMnSc2	panovník
Achnatona	Achnaton	k1gMnSc2	Achnaton
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Teje	Tej	k1gInSc2	Tej
<g/>
)	)	kIx)	)
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jeho	jeho	k3xOp3gFnSc2	jeho
náboženské	náboženský	k2eAgFnSc2d1	náboženská
reformy	reforma	k1gFnSc2	reforma
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nakonec	nakonec	k6eAd1	nakonec
vyústil	vyústit	k5eAaPmAgInS	vyústit
ve	v	k7c4	v
specifickou	specifický	k2eAgFnSc4d1	specifická
formu	forma	k1gFnSc4	forma
monoteismu	monoteismus	k1gInSc2	monoteismus
<g/>
.	.	kIx.	.
</s>
<s>
Monoteistické	monoteistický	k2eAgInPc1d1	monoteistický
prvky	prvek	k1gInPc1	prvek
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
stále	stále	k6eAd1	stále
sílily	sílit	k5eAaImAgFnP	sílit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
ovšem	ovšem	k9	ovšem
přítomny	přítomen	k2eAgFnPc1d1	přítomna
spíše	spíše	k9	spíše
implicitně	implicitně	k6eAd1	implicitně
a	a	k8xC	a
dávají	dávat	k5eAaImIp3nP	dávat
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
interpretace	interpretace	k1gFnPc4	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Motivace	motivace	k1gFnSc1	motivace
Achnatonova	Achnatonův	k2eAgInSc2d1	Achnatonův
odporu	odpor	k1gInSc2	odpor
vůči	vůči	k7c3	vůči
tradičnímu	tradiční	k2eAgNnSc3d1	tradiční
egyptskému	egyptský	k2eAgNnSc3d1	egyptské
náboženství	náboženství	k1gNnSc3	náboženství
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
bohy	bůh	k1gMnPc7	bůh
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasná	jasný	k2eAgFnSc1d1	jasná
<g/>
;	;	kIx,	;
dříve	dříve	k6eAd2	dříve
prosazovaná	prosazovaný	k2eAgFnSc1d1	prosazovaná
domněnka	domněnka	k1gFnSc1	domněnka
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
politickém	politický	k2eAgNnSc6d1	politické
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
královým	králův	k2eAgInSc7d1	králův
cílem	cíl	k1gInSc7	cíl
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
především	především	k9	především
potvrzení	potvrzení	k1gNnSc4	potvrzení
vlastní	vlastní	k2eAgFnSc2d1	vlastní
jedinečnosti	jedinečnost	k1gFnSc2	jedinečnost
a	a	k8xC	a
omezení	omezení	k1gNnSc4	omezení
moci	moc	k1gFnSc2	moc
kněžstva	kněžstvo	k1gNnSc2	kněžstvo
boha	bůh	k1gMnSc2	bůh
Amona	Amon	k1gMnSc2	Amon
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
pokládána	pokládat	k5eAaImNgFnS	pokládat
za	za	k7c4	za
příliš	příliš	k6eAd1	příliš
jednostrannou	jednostranný	k2eAgFnSc4d1	jednostranná
<g/>
.	.	kIx.	.
<g/>
Pokusy	pokus	k1gInPc7	pokus
hledat	hledat	k5eAaImF	hledat
v	v	k7c6	v
Atonismu	Atonismus	k1gInSc6	Atonismus
počátky	počátek	k1gInPc4	počátek
monoteistických	monoteistický	k2eAgInPc2d1	monoteistický
světových	světový	k2eAgInPc2d1	světový
náboženství	náboženství	k1gNnPc1	náboženství
někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
zpochybňují	zpochybňovat	k5eAaImIp3nP	zpochybňovat
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
zároveň	zároveň	k6eAd1	zároveň
stopy	stopa	k1gFnPc1	stopa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
napovídat	napovídat	k5eAaBmF	napovídat
o	o	k7c6	o
opaku	opak	k1gInSc6	opak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
problematiku	problematika	k1gFnSc4	problematika
však	však	k9	však
dosud	dosud	k6eAd1	dosud
neexistuje	existovat	k5eNaImIp3nS	existovat
jednotný	jednotný	k2eAgInSc4d1	jednotný
názor	názor	k1gInSc4	názor
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
zatím	zatím	k6eAd1	zatím
ani	ani	k8xC	ani
známy	znám	k2eAgInPc4d1	znám
přímé	přímý	k2eAgInPc4d1	přímý
historické	historický	k2eAgInPc4d1	historický
důkazy	důkaz	k1gInPc4	důkaz
pro	pro	k7c4	pro
nebo	nebo	k8xC	nebo
proti	proti	k7c3	proti
existenci	existence	k1gFnSc3	existence
vazby	vazba	k1gFnSc2	vazba
mezi	mezi	k7c7	mezi
Atonismem	Atonismus	k1gInSc7	Atonismus
a	a	k8xC	a
monoteistickými	monoteistický	k2eAgNnPc7d1	monoteistické
světovými	světový	k2eAgNnPc7d1	světové
náboženstvími	náboženství	k1gNnPc7	náboženství
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
následující	následující	k2eAgFnSc1d1	následující
podkapitola	podkapitola	k1gFnSc1	podkapitola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnešní	dnešní	k2eAgNnPc1d1	dnešní
čtyři	čtyři	k4xCgNnPc1	čtyři
nejvýznačnější	význačný	k2eAgNnPc1d3	nejvýznačnější
monoteistická	monoteistický	k2eAgNnPc1d1	monoteistické
náboženství	náboženství	k1gNnPc1	náboženství
-	-	kIx~	-
judaismus	judaismus	k1gInSc1	judaismus
<g/>
,	,	kIx,	,
bahaismus	bahaismus	k1gInSc1	bahaismus
,	,	kIx,	,
<g/>
křesťanství	křesťanství	k1gNnSc1	křesťanství
a	a	k8xC	a
islám	islám	k1gInSc1	islám
-	-	kIx~	-
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
základu	základ	k1gInSc2	základ
<g/>
,	,	kIx,	,
z	z	k7c2	z
kázání	kázání	k1gNnSc2	kázání
(	(	kIx(	(
<g/>
učení	učení	k1gNnSc4	učení
<g/>
)	)	kIx)	)
proroků	prorok	k1gMnPc2	prorok
<g/>
,	,	kIx,	,
kterými	který	k3yRgMnPc7	který
byl	být	k5eAaImAgInS	být
definován	definován	k2eAgInSc4d1	definován
judaismu	judaismus	k1gInSc3	judaismus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
letopočtu	letopočet	k1gInSc2	letopočet
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
křesťanství	křesťanství	k1gNnSc1	křesťanství
kázáním	kázání	k1gNnSc7	kázání
Ježíše	Ježíš	k1gMnSc2	Ježíš
Nazaretského	nazaretský	k2eAgMnSc2d1	nazaretský
(	(	kIx(	(
<g/>
Kristus	Kristus	k1gMnSc1	Kristus
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
pomazaný	pomazaný	k2eAgMnSc1d1	pomazaný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
pozdějších	pozdní	k2eAgMnPc2d2	pozdější
následovníků	následovník	k1gMnPc2	následovník
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
křesťané	křesťan	k1gMnPc1	křesťan
byli	být	k5eAaImAgMnP	být
židé	žid	k1gMnPc1	žid
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
je	být	k5eAaImIp3nS	být
prorokem	prorok	k1gMnSc7	prorok
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
Božím	boží	k2eAgMnSc7d1	boží
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
částí	část	k1gFnSc7	část
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
podle	podle	k7c2	podle
kázání	kázání	k1gNnSc2	kázání
proroka	prorok	k1gMnSc2	prorok
Mohameda	Mohamed	k1gMnSc2	Mohamed
Korán	korán	k1gInSc4	korán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
hlásá	hlásat	k5eAaImIp3nS	hlásat
pokračování	pokračování	k1gNnSc4	pokračování
prorocké	prorocký	k2eAgFnSc2d1	prorocká
řady	řada	k1gFnSc2	řada
přes	přes	k7c4	přes
Abraháma	Abrahám	k1gMnSc4	Abrahám
<g/>
,	,	kIx,	,
Ježíše	Ježíš	k1gMnSc4	Ježíš
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
až	až	k6eAd1	až
k	k	k7c3	k
Mohamedovi	Mohamed	k1gMnSc3	Mohamed
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
informace	informace	k1gFnSc1	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
proroci	prorok	k1gMnPc1	prorok
hlásali	hlásat	k5eAaImAgMnP	hlásat
stejnou	stejný	k2eAgFnSc4d1	stejná
myšlenku	myšlenka	k1gFnSc4	myšlenka
o	o	k7c6	o
jedinečnosti	jedinečnost	k1gFnSc6	jedinečnost
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
monoteismu	monoteismus	k1gInSc2	monoteismus
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
za	za	k7c4	za
nejstarší	starý	k2eAgInSc4d3	nejstarší
doložený	doložený	k2eAgInSc4d1	doložený
monoteismus	monoteismus	k1gInSc4	monoteismus
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
lidstva	lidstvo	k1gNnSc2	lidstvo
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
již	již	k6eAd1	již
zmiňovaný	zmiňovaný	k2eAgInSc1d1	zmiňovaný
Atonismus	Atonismus	k1gInSc1	Atonismus
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
původ	původ	k1gInSc4	původ
monoteismu	monoteismus	k1gInSc2	monoteismus
jako	jako	k8xC	jako
takového	takový	k3xDgMnSc4	takový
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
Judaismus	judaismus	k1gInSc1	judaismus
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
druhý	druhý	k4xOgInSc1	druhý
nejstarší	starý	k2eAgInSc1d3	nejstarší
monoteistismus	monoteistismus	k1gInSc1	monoteistismus
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
zas	zas	k6eAd1	zas
o	o	k7c4	o
tolik	tolik	k6eAd1	tolik
mladší	mladý	k2eAgMnSc1d2	mladší
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
i	i	k9	i
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Atonismu	Atonismus	k1gInSc2	Atonismus
předcházel	předcházet	k5eAaImAgInS	předcházet
<g/>
.	.	kIx.	.
</s>
<s>
Mojžíšův	Mojžíšův	k2eAgInSc1d1	Mojžíšův
odchod	odchod	k1gInSc1	odchod
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
nejčastěji	často	k6eAd3	často
pokládán	pokládat	k5eAaImNgInS	pokládat
do	do	k7c2	do
období	období	k1gNnSc2	období
Ramesse	Ramesse	k1gFnSc2	Ramesse
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
po	po	k7c6	po
Achnatonovi	Achnaton	k1gMnSc6	Achnaton
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
interval	interval	k1gInSc4	interval
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
je	být	k5eAaImIp3nS	být
Exodus	Exodus	k1gInSc1	Exodus
pokládán	pokládat	k5eAaImNgInS	pokládat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
širší	široký	k2eAgFnSc1d2	širší
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
vlády	vláda	k1gFnSc2	vláda
Ramesseova	Ramesseův	k2eAgMnSc4d1	Ramesseův
syna	syn	k1gMnSc4	syn
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
celou	celý	k2eAgFnSc4d1	celá
předcházející	předcházející	k2eAgFnSc4d1	předcházející
dynastii	dynastie	k1gFnSc4	dynastie
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
současníkem	současník	k1gMnSc7	současník
oné	onen	k3xDgFnSc2	onen
náboženské	náboženský	k2eAgFnSc2d1	náboženská
reformy	reforma	k1gFnSc2	reforma
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jí	jíst	k5eAaImIp3nS	jíst
i	i	k9	i
předcházet	předcházet	k5eAaImF	předcházet
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
část	část	k1gFnSc1	část
historiků	historik	k1gMnPc2	historik
zpochybňuje	zpochybňovat	k5eAaImIp3nS	zpochybňovat
jak	jak	k8xC	jak
samotného	samotný	k2eAgMnSc4d1	samotný
Mojžíše	Mojžíš	k1gMnSc4	Mojžíš
<g/>
,	,	kIx,	,
coby	coby	k?	coby
historickou	historický	k2eAgFnSc4d1	historická
osobnost	osobnost	k1gFnSc4	osobnost
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
samotný	samotný	k2eAgInSc1d1	samotný
Exodus	Exodus	k1gInSc1	Exodus
jako	jako	k8xC	jako
historickou	historický	k2eAgFnSc4d1	historická
událost	událost	k1gFnSc4	událost
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Snahy	snaha	k1gFnSc2	snaha
o	o	k7c6	o
vědecké	vědecký	k2eAgNnSc4d1	vědecké
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
událostí	událost	k1gFnSc7	událost
exodu	exodus	k1gInSc2	exodus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přinejmenším	přinejmenším	k6eAd1	přinejmenším
tu	tu	k6eAd1	tu
však	však	k9	však
máme	mít	k5eAaImIp1nP	mít
nejstarší	starý	k2eAgInSc4d3	nejstarší
doložený	doložený	k2eAgInSc4d1	doložený
záznam	záznam	k1gInSc4	záznam
o	o	k7c6	o
izraelském	izraelský	k2eAgInSc6d1	izraelský
národě	národ	k1gInSc6	národ
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Kanaánu	Kanaán	k1gInSc2	Kanaán
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
Merenptahově	Merenptahův	k2eAgFnSc6d1	Merenptahův
stéle	stéla	k1gFnSc6	stéla
(	(	kIx(	(
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1209	[number]	k4	1209
<g/>
/	/	kIx~	/
<g/>
1208	[number]	k4	1208
př.n.l.	př.n.l.	k?	př.n.l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
po	po	k7c6	po
předpokládaném	předpokládaný	k2eAgInSc6d1	předpokládaný
exodu	exodus	k1gInSc6	exodus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
atonistického	atonistický	k2eAgNnSc2d1	atonistický
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
1355	[number]	k4	1355
<g/>
-	-	kIx~	-
<g/>
1335	[number]	k4	1335
<g/>
)	)	kIx)	)
<g/>
ji	on	k3xPp3gFnSc4	on
dělí	dělit	k5eAaImIp3nP	dělit
zhruba	zhruba	k6eAd1	zhruba
130	[number]	k4	130
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doložené	doložený	k2eAgInPc1d1	doložený
záznamy	záznam	k1gInPc1	záznam
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
dokládají	dokládat	k5eAaImIp3nP	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejstarší	starý	k2eAgInSc1d3	nejstarší
je	být	k5eAaImIp3nS	být
Atonismus	Atonismus	k1gInSc1	Atonismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
náznaky	náznak	k1gInPc4	náznak
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
Judaismem	judaismus	k1gInSc7	judaismus
mohly	moct	k5eAaImAgFnP	moct
existovat	existovat	k5eAaImF	existovat
jisté	jistý	k2eAgFnPc4d1	jistá
vazby	vazba	k1gFnPc4	vazba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
znamenat	znamenat	k5eAaImF	znamenat
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Judaismus	judaismus	k1gInSc1	judaismus
mohl	moct	k5eAaImAgInS	moct
Atonismu	Atonismus	k1gInSc2	Atonismus
předcházet	předcházet	k5eAaImF	předcházet
a	a	k8xC	a
Atonismus	Atonismus	k1gInSc1	Atonismus
se	se	k3xPyFc4	se
jím	jíst	k5eAaImIp1nS	jíst
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
nechal	nechat	k5eAaPmAgMnS	nechat
<g />
.	.	kIx.	.
</s>
<s>
inspirovat	inspirovat	k5eAaBmF	inspirovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
i	i	k9	i
že	že	k8xS	že
Atonismus	Atonismus	k1gInSc1	Atonismus
mohl	moct	k5eAaImAgInS	moct
naopak	naopak	k6eAd1	naopak
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
náboženský	náboženský	k2eAgInSc1d1	náboženský
vývoj	vývoj	k1gInSc1	vývoj
Židů	Žid	k1gMnPc2	Žid
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
například	například	k6eAd1	například
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Muž	muž	k1gMnSc1	muž
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
a	a	k8xC	a
monoteistické	monoteistický	k2eAgNnSc1d1	monoteistické
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
obě	dva	k4xCgNnPc1	dva
náboženství	náboženství	k1gNnPc1	náboženství
mohla	moct	k5eAaImAgNnP	moct
být	být	k5eAaImF	být
různými	různý	k2eAgInPc7d1	různý
odkazy	odkaz	k1gInPc7	odkaz
jiné	jiná	k1gFnSc2	jiná
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
neznáme	znát	k5eNaImIp1nP	znát
monoteistické	monoteistický	k2eAgFnPc4d1	monoteistická
tradice	tradice	k1gFnPc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Nápadná	nápadný	k2eAgFnSc1d1	nápadná
podobnost	podobnost	k1gFnSc1	podobnost
Achnatonova	Achnatonův	k2eAgInSc2d1	Achnatonův
Hymnu	hymnus	k1gInSc2	hymnus
na	na	k7c4	na
Slunce	slunce	k1gNnSc4	slunce
se	s	k7c7	s
104	[number]	k4	104
<g/>
.	.	kIx.	.
žalmem	žalm	k1gInSc7	žalm
není	být	k5eNaImIp3nS	být
jen	jen	k9	jen
teorií	teorie	k1gFnSc7	teorie
záhadologů	záhadolog	k1gMnPc2	záhadolog
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
neexistuje	existovat	k5eNaImIp3nS	existovat
jednoznačný	jednoznačný	k2eAgInSc4d1	jednoznačný
názor	názor	k1gInSc4	názor
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
text	text	k1gInSc4	text
si	se	k3xPyFc3	se
vypůjčil	vypůjčit	k5eAaPmAgMnS	vypůjčit
z	z	k7c2	z
kterého	který	k3yQgNnSc2	který
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
podobnost	podobnost	k1gFnSc4	podobnost
pouze	pouze	k6eAd1	pouze
náhodnou	náhodný	k2eAgFnSc4d1	náhodná
<g/>
.	.	kIx.	.
</s>
<s>
Monoteismus	monoteismus	k1gInSc1	monoteismus
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
začal	začít	k5eAaPmAgInS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
až	až	k9	až
v	v	k7c6	v
7	[number]	k4	7
století	století	k1gNnSc6	století
př.n.l.	př.n.l.	k?	př.n.l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Jóšijáš	Jóšijáš	k1gFnSc1	Jóšijáš
legalizuje	legalizovat	k5eAaBmIp3nS	legalizovat
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgInSc1d1	jediný
kult	kult	k1gInSc1	kult
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
později	pozdě	k6eAd2	pozdě
podobně	podobně	k6eAd1	podobně
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
moci	moc	k1gFnSc2	moc
prosadí	prosadit	k5eAaPmIp3nS	prosadit
monoteismus	monoteismus	k1gInSc1	monoteismus
Konstantin	Konstantin	k1gMnSc1	Konstantin
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
a	a	k8xC	a
Mohamed	Mohamed	k1gMnSc1	Mohamed
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
monoteismu	monoteismus	k1gInSc2	monoteismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
ve	v	k7c6	v
světovém	světový	k2eAgNnSc6d1	světové
měřítku	měřítko	k1gNnSc6	měřítko
dominantní	dominantní	k2eAgFnSc2d1	dominantní
formou	forma	k1gFnSc7	forma
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
zahalen	zahalit	k5eAaPmNgInS	zahalit
hávem	háv	k1gInSc7	háv
nejasností	nejasnost	k1gFnPc2	nejasnost
a	a	k8xC	a
nezodpovězených	zodpovězený	k2eNgFnPc2d1	nezodpovězená
otázek	otázka	k1gFnPc2	otázka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Srovnání	srovnání	k1gNnSc1	srovnání
monoteismu	monoteismus	k1gInSc2	monoteismus
s	s	k7c7	s
polyteismem	polyteismus	k1gInSc7	polyteismus
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
protichůdných	protichůdný	k2eAgFnPc2d1	protichůdná
náboženských	náboženský	k2eAgFnPc2d1	náboženská
doktrín	doktrína	k1gFnPc2	doktrína
-	-	kIx~	-
polyteismu	polyteismus	k1gInSc2	polyteismus
a	a	k8xC	a
monoteismu	monoteismus	k1gInSc2	monoteismus
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
mnoho	mnoho	k4c1	mnoho
podobností	podobnost	k1gFnPc2	podobnost
až	až	k8xS	až
shod	shoda	k1gFnPc2	shoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zmírňují	zmírňovat	k5eAaImIp3nP	zmírňovat
antagonismus	antagonismus	k1gInSc4	antagonismus
přísného	přísný	k2eAgNnSc2d1	přísné
oddělování	oddělování	k1gNnSc2	oddělování
těchto	tento	k3xDgFnPc2	tento
ideologií	ideologie	k1gFnPc2	ideologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
jako	jako	k8xS	jako
příklad	příklad	k1gInSc1	příklad
monoteismu	monoteismus	k1gInSc2	monoteismus
(	(	kIx(	(
<g/>
obdobně	obdobně	k6eAd1	obdobně
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
monoteismů	monoteismus	k1gInPc2	monoteismus
<g/>
)	)	kIx)	)
zná	znát	k5eAaImIp3nS	znát
celý	celý	k2eAgInSc4d1	celý
systém	systém	k1gInSc4	systém
bytostí	bytost	k1gFnPc2	bytost
kromě	kromě	k7c2	kromě
Boha	bůh	k1gMnSc2	bůh
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
<g/>
:	:	kIx,	:
Ďábel	ďábel	k1gMnSc1	ďábel
(	(	kIx(	(
<g/>
Satan	Satan	k1gMnSc1	Satan
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
andělé	anděl	k1gMnPc1	anděl
a	a	k8xC	a
čerti	čert	k1gMnPc1	čert
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
"	"	kIx"	"
<g/>
mocnosti	mocnost	k1gFnSc3	mocnost
nebeské	nebeský	k2eAgFnSc2d1	nebeská
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
mocnosti	mocnost	k1gFnPc1	mocnost
pekelné	pekelný	k2eAgFnPc1d1	pekelná
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejichž	jejichž	k3xOyRp3gInPc6	jejichž
počtech	počet	k1gInPc6	počet
mnoho	mnoho	k6eAd1	mnoho
nevíme	vědět	k5eNaImIp1nP	vědět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
nauky	nauka	k1gFnSc2	nauka
vnitřně	vnitřně	k6eAd1	vnitřně
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
ducha	duch	k1gMnSc4	duch
resp.	resp.	kA	resp.
duši	duše	k1gFnSc4	duše
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
nesmrtelná	smrtelný	k2eNgFnSc1d1	nesmrtelná
<g/>
.	.	kIx.	.
</s>
<s>
Kdesi	kdesi	k6eAd1	kdesi
"	"	kIx"	"
<g/>
na	na	k7c6	na
věčnosti	věčnost	k1gFnSc6	věčnost
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
daleko	daleko	k6eAd1	daleko
víc	hodně	k6eAd2	hodně
bytostí	bytost	k1gFnPc2	bytost
(	(	kIx(	(
<g/>
duší	duše	k1gFnPc2	duše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
než	než	k8xS	než
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
na	na	k7c4	na
ty	ten	k3xDgFnPc4	ten
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
mocnější	mocný	k2eAgFnPc1d2	mocnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
je	on	k3xPp3gInPc4	on
pak	pak	k6eAd1	pak
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
svaté	svatý	k2eAgInPc4d1	svatý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
zbožštělé	zbožštělý	k2eAgFnPc1d1	zbožštělá
duše	duše	k1gFnPc1	duše
konají	konat	k5eAaImIp3nP	konat
zázraky	zázrak	k1gInPc4	zázrak
a	a	k8xC	a
také	také	k9	také
mají	mít	k5eAaImIp3nP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
Boha	bůh	k1gMnSc4	bůh
a	a	k8xC	a
křesťané	křesťan	k1gMnPc1	křesťan
je	on	k3xPp3gMnPc4	on
uctívají	uctívat	k5eAaImIp3nP	uctívat
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
pohané	pohan	k1gMnPc1	pohan
své	svůj	k3xOyFgMnPc4	svůj
bohy	bůh	k1gMnPc4	bůh
a	a	k8xC	a
bůžky	bůžek	k1gMnPc4	bůžek
nebo	nebo	k8xC	nebo
duchy	duch	k1gMnPc4	duch
a	a	k8xC	a
duše	duše	k1gFnPc4	duše
zemřelých	zemřelý	k2eAgMnPc2d1	zemřelý
předků	předek	k1gMnPc2	předek
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
polyteismu	polyteismus	k1gInSc3	polyteismus
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
tyto	tento	k3xDgFnPc1	tento
nadpřirozené	nadpřirozený	k2eAgFnPc1d1	nadpřirozená
schopnosti	schopnost	k1gFnPc1	schopnost
spojují	spojovat	k5eAaImIp3nP	spojovat
či	či	k8xC	či
přímo	přímo	k6eAd1	přímo
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
právě	právě	k9	právě
od	od	k7c2	od
jediného	jediný	k2eAgMnSc2d1	jediný
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
možné	možný	k2eAgNnSc1d1	možné
pojímat	pojímat	k5eAaImF	pojímat
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
monoteismem	monoteismus	k1gInSc7	monoteismus
a	a	k8xC	a
polyteismem	polyteismus	k1gInSc7	polyteismus
jako	jako	k8xS	jako
rozdíl	rozdíl	k1gInSc4	rozdíl
v	v	k7c6	v
míře	míra	k1gFnSc6	míra
povýšení	povýšení	k1gNnSc4	povýšení
jediného	jediný	k2eAgMnSc2d1	jediný
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hlavní	hlavní	k2eAgFnSc7d1	hlavní
součástí	součást	k1gFnSc7	součást
tohoto	tento	k3xDgNnSc2	tento
povýšení	povýšení	k1gNnSc2	povýšení
je	být	k5eAaImIp3nS	být
monopolizace	monopolizace	k1gFnSc1	monopolizace
znaku	znak	k1gInSc2	znak
-	-	kIx~	-
slova	slovo	k1gNnSc2	slovo
Bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Idolatrie	idolatrie	k1gFnSc1	idolatrie
</s>
</p>
<p>
<s>
Pohanství	pohanství	k1gNnSc1	pohanství
<g/>
,	,	kIx,	,
Novopohanství	novopohanství	k1gNnSc1	novopohanství
</s>
</p>
<p>
<s>
Polyteismus	polyteismus	k1gInSc1	polyteismus
<g/>
,	,	kIx,	,
Henoteismus	Henoteismus	k1gInSc1	Henoteismus
</s>
</p>
<p>
<s>
Proselytismus	proselytismus	k1gInSc1	proselytismus
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Monoteismus	monoteismus	k1gInSc1	monoteismus
a	a	k8xC	a
starověký	starověký	k2eAgInSc1d1	starověký
Egypt	Egypt	k1gInSc1	Egypt
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Pětidílné	pětidílný	k2eAgNnSc1d1	pětidílné
pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c6	o
Jediném	jediné	k1gNnSc6	jediné
Bohu	bůh	k1gMnSc3	bůh
z	z	k7c2	z
tradičního	tradiční	k2eAgInSc2d1	tradiční
pohledu	pohled	k1gInSc2	pohled
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
</s>
</p>
