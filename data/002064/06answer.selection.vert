<s>
Díky	díky	k7c3	díky
poloze	poloha	k1gFnSc3	poloha
města	město	k1gNnSc2	město
na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
posledním	poslední	k2eAgNnSc7d1	poslední
městem	město	k1gNnSc7	město
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Kanadské	kanadský	k2eAgFnSc2d1	kanadská
transkontinentální	transkontinentální	k2eAgFnSc2d1	transkontinentální
dálnice	dálnice	k1gFnSc2	dálnice
a	a	k8xC	a
železnice	železnice	k1gFnSc2	železnice
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Vancouver	Vancouver	k1gInSc1	Vancouver
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgNnPc4d3	veliký
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
centra	centrum	k1gNnPc4	centrum
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
