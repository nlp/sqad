<s>
Gaboltov	Gaboltov	k1gInSc1
</s>
<s>
Gaboltov	Gaboltov	k1gInSc1
Kostel	kostel	k1gInSc1
sv.	sv.	kA
VojtěchaPoloha	VojtěchaPoloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
21	#num#	k4
<g/>
′	′	k?
<g/>
54	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
21	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
<g/>
33	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
421	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Prešovský	prešovský	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
Bardejov	Bardejov	k1gInSc1
Tradiční	tradiční	k2eAgInSc4d1
region	region	k1gInSc4
</s>
<s>
Šariš	Šariš	k1gInSc1
</s>
<s>
Gaboltov	Gaboltov	k1gInSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
12,7	12,7	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
494	#num#	k4
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
38,8	38,8	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
obec	obec	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Martin	Martin	k1gMnSc1
Ceľuch	Ceľucha	k1gFnPc2
Vznik	vznik	k1gInSc1
</s>
<s>
1247	#num#	k4
(	(	kIx(
<g/>
první	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.gaboltov.sk	www.gaboltov.sk	k1gInSc1
E-mail	e-mail	k1gInSc1
</s>
<s>
obec_gaboltov@mail.t-com.sk	obec_gaboltov@mail.t-com.sk	k1gInSc1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
054	#num#	k4
PSČ	PSČ	kA
</s>
<s>
086	#num#	k4
02	#num#	k4
Označení	označení	k1gNnPc2
vozidel	vozidlo	k1gNnPc2
</s>
<s>
BJ	BJ	kA
NUTS	NUTS	kA
</s>
<s>
519171	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Gaboltov	Gaboltov	k1gInSc1
je	být	k5eAaImIp3nS
obec	obec	k1gFnSc4
a	a	k8xC
poutní	poutní	k2eAgNnSc4d1
místo	místo	k1gNnSc4
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
v	v	k7c6
okrese	okres	k1gInSc6
Bardejov	Bardejov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Gaboltov	Gaboltov	k1gInSc1
leží	ležet	k5eAaImIp3nS
asi	asi	k9
15	#num#	k4
kilometrů	kilometr	k1gInPc2
severozápadně	severozápadně	k6eAd1
od	od	k7c2
Bardejova	Bardejov	k1gInSc2
v	v	k7c6
údolí	údolí	k1gNnSc6
potoka	potok	k1gInSc2
Kamenec	Kamenec	k1gInSc4
pod	pod	k7c7
vrchem	vrch	k1gInSc7
Busov	Busov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
3	#num#	k4
km	km	kA
od	od	k7c2
obce	obec	k1gFnSc2
probíhá	probíhat	k5eAaImIp3nS
slovensko	slovensko	k6eAd1
<g/>
–	–	k?
<g/>
polská	polský	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
494	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1247	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
římskokatolický	římskokatolický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Vojtěcha	Vojtěch	k1gMnSc2
ze	z	k7c2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
s	s	k7c7
milostným	milostný	k2eAgInSc7d1
obrazem	obraz	k1gInSc7
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgInSc3
se	se	k3xPyFc4
každoročně	každoročně	k6eAd1
koná	konat	k5eAaImIp3nS
pouť	pouť	k1gFnSc1
(	(	kIx(
<g/>
Gaboltovský	Gaboltovský	k2eAgInSc1d1
odpust	odpust	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
SR	SR	kA
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
.	.	kIx.
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Gaboltov	Gaboltovo	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Města	město	k1gNnSc2
a	a	k8xC
obce	obec	k1gFnSc2
okresu	okres	k1gInSc2
Bardejov	Bardejov	k1gInSc1
</s>
<s>
Abrahámovce	Abrahámovec	k1gInSc2
•	•	k?
Andrejová	Andrejový	k2eAgFnSc1d1
•	•	k?
Bardejov	Bardejov	k1gInSc1
•	•	k?
Bartošovce	Bartošovec	k1gInSc2
•	•	k?
Becherov	Becherov	k1gInSc1
•	•	k?
Beloveža	Beloveža	k1gFnSc1
•	•	k?
Bogliarka	Bogliarka	k1gFnSc1
•	•	k?
Brezov	Brezov	k1gInSc1
•	•	k?
Brezovka	Brezovka	k1gFnSc1
•	•	k?
Buclovany	Buclovan	k1gMnPc7
•	•	k?
Cigeľka	Cigeľek	k1gInSc2
•	•	k?
Dubinné	Dubinný	k2eAgNnSc1d1
•	•	k?
Frička	Friček	k1gMnSc2
•	•	k?
Fričkovce	Fričkovec	k1gMnSc2
•	•	k?
Gaboltov	Gaboltov	k1gInSc1
•	•	k?
Gerlachov	Gerlachov	k1gInSc1
•	•	k?
Hankovce	Hankovec	k1gInSc2
•	•	k?
Harhaj	Harhaj	k1gMnSc1
•	•	k?
Hažlín	Hažlín	k1gInSc1
•	•	k?
Hertník	Hertník	k1gInSc1
•	•	k?
Hervartov	Hervartov	k1gInSc1
•	•	k?
Hrabovec	Hrabovec	k1gInSc4
•	•	k?
Hrabské	hrabský	k2eAgFnSc2d1
•	•	k?
Hutka	Hutka	k1gMnSc1
•	•	k?
Chmeľová	Chmeľová	k1gFnSc1
•	•	k?
Janovce	janovec	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Jedlinka	Jedlinka	k1gFnSc1
•	•	k?
Kľušov	Kľušov	k1gInSc1
•	•	k?
Kobyly	kobyla	k1gFnSc2
•	•	k?
Kochanovce	Kochanovec	k1gMnSc2
•	•	k?
Komárov	Komárov	k1gInSc1
•	•	k?
Koprivnica	Koprivnica	k1gFnSc1
•	•	k?
Kožany	Kožana	k1gFnSc2
•	•	k?
Krivé	Krivá	k1gFnSc2
•	•	k?
Kríže	Kríž	k1gFnSc2
•	•	k?
Kružlov	Kružlov	k1gInSc1
•	•	k?
Kučín	Kučín	k1gInSc1
•	•	k?
Kurima	Kurima	k1gFnSc1
•	•	k?
Kurov	Kurov	k1gInSc1
•	•	k?
Lascov	Lascov	k1gInSc1
•	•	k?
Lenartov	Lenartov	k1gInSc1
•	•	k?
Lipová	lipový	k2eAgFnSc1d1
•	•	k?
Livov	Livov	k1gInSc1
•	•	k?
Livovská	Livovská	k1gFnSc1
Huta	Huta	k1gFnSc1
•	•	k?
Lopúchov	Lopúchov	k1gInSc1
•	•	k?
Lukavica	Lukavic	k1gInSc2
•	•	k?
Lukov	Lukov	k1gInSc1
•	•	k?
Malcov	Malcov	k1gInSc1
•	•	k?
Marhaň	Marhaň	k1gFnSc2
•	•	k?
Mikulášová	Mikulášová	k1gFnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Mokroluh	Mokroluh	k1gInSc1
•	•	k?
Nemcovce	Nemcovec	k1gInSc2
•	•	k?
Nižná	nižný	k2eAgFnSc1d1
Polianka	Polianka	k1gFnSc1
•	•	k?
Nižná	nižný	k2eAgFnSc1d1
Voľa	Voľa	k1gFnSc1
•	•	k?
Nižný	nižný	k2eAgMnSc1d1
Tvarožec	Tvarožec	k1gMnSc1
•	•	k?
Oľšavce	Oľšavka	k1gFnSc6
•	•	k?
Ondavka	Ondavka	k1gFnSc1
•	•	k?
Ortuťová	Ortuťový	k2eAgFnSc1d1
•	•	k?
Osikov	Osikov	k1gInSc1
•	•	k?
Petrová	Petrová	k1gFnSc1
•	•	k?
Poliakovce	Poliakovec	k1gMnSc2
•	•	k?
Porúbka	Porúbek	k1gMnSc2
•	•	k?
Raslavice	Raslavice	k1gFnSc2
•	•	k?
Regetovka	Regetovka	k1gFnSc1
•	•	k?
Rešov	Rešov	k1gInSc1
•	•	k?
Richvald	Richvald	k1gInSc1
•	•	k?
Rokytov	Rokytov	k1gInSc1
•	•	k?
Smilno	Smilno	k1gNnSc4
•	•	k?
Snakov	Snakov	k1gInSc1
•	•	k?
Stebnícka	Stebnícka	k1gFnSc1
Huta	Hut	k1gInSc2
•	•	k?
Stebník	Stebník	k1gMnSc1
•	•	k?
Stuľany	Stuľan	k1gInPc1
•	•	k?
Sveržov	Sveržov	k1gInSc4
•	•	k?
Šarišské	šarišský	k2eAgFnSc2d1
Čierne	Čiern	k1gInSc5
•	•	k?
Šašová	Šašová	k1gFnSc1
•	•	k?
Šiba	Šiba	k?
•	•	k?
Tarnov	Tarnov	k1gInSc1
•	•	k?
Tročany	Tročan	k1gMnPc7
•	•	k?
Vaniškovce	Vaniškovec	k1gInSc2
•	•	k?
Varadka	Varadka	k1gFnSc1
•	•	k?
Vyšná	vyšný	k2eAgFnSc1d1
Polianka	Polianka	k1gFnSc1
•	•	k?
Vyšná	vyšný	k2eAgFnSc1d1
Voľa	Voľa	k1gFnSc1
•	•	k?
Vyšný	vyšný	k2eAgInSc4d1
Kručov	Kručov	k1gInSc4
•	•	k?
Vyšný	vyšný	k2eAgMnSc1d1
Tvarožec	Tvarožec	k1gMnSc1
•	•	k?
Zborov	Zborov	k1gInSc1
•	•	k?
Zlaté	zlatá	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
944271	#num#	k4
</s>
