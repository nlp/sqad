<s>
Listed	Listed	k1gInSc1
building	building	k1gInSc1
</s>
<s>
Buckinghamský	buckinghamský	k2eAgInSc1d1
palác	palác	k1gInSc1
je	být	k5eAaImIp3nS
památka	památka	k1gFnSc1
stupně	stupeň	k1gInSc2
I	i	k9
</s>
<s>
National	Nationat	k5eAaImAgMnS,k5eAaPmAgMnS
Gallery	Galler	k1gMnPc4
of	of	k?
Scotland	Scotland	k1gInSc1
je	být	k5eAaImIp3nS
památkou	památka	k1gFnSc7
kategorie	kategorie	k1gFnSc2
A	a	k8xC
</s>
<s>
Listed	Listed	k1gInSc1
building	building	k1gInSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
budova	budova	k1gFnSc1
na	na	k7c6
seznamu	seznam	k1gInSc6
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ve	v	k7c6
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
označení	označení	k1gNnSc1
pro	pro	k7c4
stavbu	stavba	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
Zákonném	zákonný	k2eAgInSc6d1
seznamu	seznam	k1gInSc6
budov	budova	k1gFnPc2
zvláštního	zvláštní	k2eAgInSc2d1
architektonického	architektonický	k2eAgInSc2d1
nebo	nebo	k8xC
historického	historický	k2eAgInSc2d1
významu	význam	k1gInSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Statutory	Statutor	k1gInPc1
List	lista	k1gFnPc2
of	of	k?
Buildings	Buildingsa	k1gFnPc2
of	of	k?
Special	Special	k1gMnSc1
Architectural	Architectural	k1gMnSc1
or	or	k?
Historic	Historic	k1gMnSc1
Interest	Interest	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
status	status	k1gInSc4
má	mít	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
půl	půl	k1xP
milionu	milion	k4xCgInSc2
staveb	stavba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Budova	budova	k1gFnSc1
uvedená	uvedený	k2eAgFnSc1d1
na	na	k7c6
seznamu	seznam	k1gInSc6
nesmí	smět	k5eNaImIp3nS
být	být	k5eAaImF
zbořena	zbořit	k5eAaPmNgFnS
<g/>
,	,	kIx,
rozšířena	rozšířit	k5eAaPmNgFnS
či	či	k8xC
upravena	upravit	k5eAaPmNgFnS
bez	bez	k7c2
zvláštního	zvláštní	k2eAgNnSc2d1
povolení	povolení	k1gNnSc2
místního	místní	k2eAgInSc2d1
plánovacího	plánovací	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastníci	vlastník	k1gMnPc1
těchto	tento	k3xDgFnPc2
budov	budova	k1gFnPc2
jsou	být	k5eAaImIp3nP
za	za	k7c2
jistých	jistý	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
povinni	povinen	k2eAgMnPc1d1
je	on	k3xPp3gNnSc4
opravovat	opravovat	k5eAaImF
a	a	k8xC
udržovat	udržovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
tak	tak	k6eAd1
nečiní	činit	k5eNaImIp3nS
nebo	nebo	k8xC
provádí	provádět	k5eAaImIp3nS
nepovolené	povolený	k2eNgFnPc4d1
úpravy	úprava	k1gFnPc4
<g/>
,	,	kIx,
hrozí	hrozit	k5eAaImIp3nS
jim	on	k3xPp3gMnPc3
trestní	trestní	k2eAgNnSc1d1
stíhání	stíhání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Navzdory	navzdory	k7c3
pojmu	pojem	k1gInSc3
listed	listed	k1gInSc1
building	building	k1gInSc4
(	(	kIx(
<g/>
česky	česky	k6eAd1
budova	budova	k1gFnSc1
nebo	nebo	k8xC
stavba	stavba	k1gFnSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
na	na	k7c6
seznamu	seznam	k1gInSc6
kromě	kromě	k7c2
budov	budova	k1gFnPc2
také	také	k9
mosty	most	k1gInPc1
<g/>
,	,	kIx,
monumenty	monument	k1gInPc1
<g/>
,	,	kIx,
sochy	socha	k1gFnPc1
<g/>
,	,	kIx,
památníky	památník	k1gInPc1
a	a	k8xC
dokonce	dokonce	k9
i	i	k9
přechod	přechod	k1gInSc4
pro	pro	k7c4
chodce	chodec	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Starověké	starověký	k2eAgInPc4d1
<g/>
,	,	kIx,
válečné	válečný	k2eAgInPc4d1
a	a	k8xC
neobydlené	obydlený	k2eNgInPc4d1
objekty	objekt	k1gInPc4
(	(	kIx(
<g/>
například	například	k6eAd1
Stonehenge	Stoneheng	k1gMnSc2
<g/>
)	)	kIx)
mají	mít	k5eAaImIp3nP
někdy	někdy	k6eAd1
namísto	namísto	k7c2
tohoto	tento	k3xDgInSc2
statusu	status	k1gInSc2
označení	označení	k1gNnSc1
scheduled	scheduled	k1gMnSc1
monument	monument	k1gInSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
doslova	doslova	k6eAd1
plánovaná	plánovaný	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
jsou	být	k5eAaImIp3nP
chráněny	chránit	k5eAaImNgFnP
starší	starý	k2eAgFnSc7d2
legislativou	legislativa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odlišný	odlišný	k2eAgInSc1d1
způsob	způsob	k1gInSc1
ochrany	ochrana	k1gFnSc2
funguje	fungovat	k5eAaImIp3nS
pro	pro	k7c4
kulturní	kulturní	k2eAgFnPc4d1
krajiny	krajina	k1gFnPc4
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
parky	park	k1gInPc4
či	či	k8xC
zahrady	zahrada	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Systémy	systém	k1gInPc1
se	se	k3xPyFc4
v	v	k7c6
různých	různý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
regionech	region	k1gInPc6
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
mírně	mírně	k6eAd1
liší	lišit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Anglie	Anglie	k1gFnSc1
a	a	k8xC
Wales	Wales	k1gInSc1
</s>
<s>
V	v	k7c6
Anglii	Anglie	k1gFnSc6
a	a	k8xC
Walesu	Wales	k1gInSc2
jsou	být	k5eAaImIp3nP
tři	tři	k4xCgInPc1
různé	různý	k2eAgInPc1d1
statusy	status	k1gInPc1
chráněných	chráněný	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
stupeň	stupeň	k1gInSc1
I	i	k8xC
–	–	k?
stavby	stavba	k1gFnPc4
výjimečného	výjimečný	k2eAgInSc2d1
zájmu	zájem	k1gInSc2
</s>
<s>
stupeň	stupeň	k1gInSc1
II	II	kA
<g/>
*	*	kIx~
–	–	k?
významné	významný	k2eAgFnPc4d1
stavby	stavba	k1gFnPc4
více	hodně	k6eAd2
než	než	k8xS
zvláštního	zvláštní	k2eAgInSc2d1
zájmu	zájem	k1gInSc2
</s>
<s>
stupeň	stupeň	k1gInSc1
II	II	kA
–	–	k?
stavby	stavba	k1gFnPc4
zvláštního	zvláštní	k2eAgInSc2d1
zájmu	zájem	k1gInSc2
</s>
<s>
V	v	k7c6
březnu	březen	k1gInSc6
2010	#num#	k4
bylo	být	k5eAaImAgNnS
na	na	k7c6
seznamu	seznam	k1gInSc6
cca	cca	kA
374	#num#	k4
000	#num#	k4
anglických	anglický	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
z	z	k7c2
nichž	jenž	k3xRgNnPc2
92	#num#	k4
%	%	kIx~
tvořil	tvořit	k5eAaImAgInS
stupeň	stupeň	k1gInSc1
II	II	kA
<g/>
,	,	kIx,
5,5	5,5	k4
%	%	kIx~
stupeň	stupeň	k1gInSc1
II	II	kA
<g/>
*	*	kIx~
a	a	k8xC
2,5	2,5	k4
%	%	kIx~
stupeň	stupeň	k1gInSc1
I.	I.	kA
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Dříve	dříve	k6eAd2
existoval	existovat	k5eAaImAgInS
také	také	k9
neuzákoněný	uzákoněný	k2eNgInSc1d1
stupeň	stupeň	k1gInSc1
III	III	kA
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
byl	být	k5eAaImAgInS
ale	ale	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
definitivně	definitivně	k6eAd1
zrušen	zrušit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1977	#num#	k4
se	se	k3xPyFc4
pro	pro	k7c4
anglikánské	anglikánský	k2eAgInPc4d1
kostely	kostel	k1gInPc4
používaly	používat	k5eAaImAgInP
stupně	stupeň	k1gInPc1
A	A	kA
<g/>
,	,	kIx,
B	B	kA
a	a	k8xC
C	C	kA
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
přibližně	přibližně	k6eAd1
odpovídaly	odpovídat	k5eAaImAgFnP
běžným	běžný	k2eAgNnSc7d1
I	I	kA
<g/>
,	,	kIx,
II	II	kA
<g/>
*	*	kIx~
a	a	k8xC
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
Walesu	Wales	k1gInSc6
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
asi	asi	k9
30	#num#	k4
000	#num#	k4
chráněných	chráněný	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Radnice	radnice	k1gFnSc1
v	v	k7c4
Leeds	Leeds	k1gInSc4
(	(	kIx(
<g/>
I	i	k9
<g/>
)	)	kIx)
</s>
<s>
Oxwich	Oxwich	k1gInSc1
Castle	Castle	k1gFnSc2
ve	v	k7c6
Walesu	Wales	k1gInSc6
(	(	kIx(
<g/>
I	i	k9
<g/>
)	)	kIx)
</s>
<s>
Divadlo	divadlo	k1gNnSc1
London	London	k1gMnSc1
Palladium	palladium	k1gNnSc4
(	(	kIx(
<g/>
II	II	kA
<g/>
*	*	kIx~
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
v	v	k7c6
Bristolu	Bristol	k1gInSc6
(	(	kIx(
<g/>
II	II	kA
<g/>
*	*	kIx~
<g/>
)	)	kIx)
</s>
<s>
Mansion	Mansion	k1gInSc4
House	house	k1gNnSc1
v	v	k7c6
Cardiffu	Cardiff	k1gInSc6
(	(	kIx(
<g/>
II	II	kA
<g/>
)	)	kIx)
</s>
<s>
Skotsko	Skotsko	k1gNnSc1
</s>
<s>
Ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
je	být	k5eAaImIp3nS
schéma	schéma	k1gNnSc1
ochrany	ochrana	k1gFnSc2
následovné	následovný	k2eAgNnSc1d1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
kategorie	kategorie	k1gFnSc1
A	A	kA
–	–	k?
stavby	stavba	k1gFnPc4
národního	národní	k2eAgInSc2d1
významu	význam	k1gInSc2
<g/>
,	,	kIx,
architektonického	architektonický	k2eAgNnSc2d1
nebo	nebo	k8xC
historického	historický	k2eAgNnSc2d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
dobře	dobře	k6eAd1
zachovalé	zachovalý	k2eAgInPc4d1
příklady	příklad	k1gInPc4
určitého	určitý	k2eAgNnSc2d1
období	období	k1gNnSc2
či	či	k8xC
stylu	styl	k1gInSc2
</s>
<s>
kategorie	kategorie	k1gFnSc1
B	B	kA
–	–	k?
stavby	stavba	k1gFnPc4
regionálního	regionální	k2eAgInSc2d1
významu	význam	k1gInSc2
nebo	nebo	k8xC
hlavní	hlavní	k2eAgInPc4d1
příklady	příklad	k1gInPc4
určitého	určitý	k2eAgNnSc2d1
období	období	k1gNnSc2
či	či	k8xC
stylu	styl	k1gInSc2
<g/>
,	,	kIx,
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
příliš	příliš	k6eAd1
zachovalé	zachovalý	k2eAgFnPc1d1
</s>
<s>
kategorie	kategorie	k1gFnSc1
C	C	kA
<g/>
(	(	kIx(
<g/>
s	s	k7c7
<g/>
)	)	kIx)
–	–	k?
stavby	stavba	k1gFnPc4
místního	místní	k2eAgInSc2d1
významu	význam	k1gInSc2
a	a	k8xC
jednoduché	jednoduchý	k2eAgFnSc2d1
tradiční	tradiční	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
<g/>
,	,	kIx,
dobře	dobře	k6eAd1
seskupené	seskupený	k2eAgNnSc1d1
s	s	k7c7
těmi	ten	k3xDgMnPc7
z	z	k7c2
kategorií	kategorie	k1gFnPc2
A	A	kA
a	a	k8xC
B	B	kA
</s>
<s>
Roku	rok	k1gInSc2
2009	#num#	k4
bylo	být	k5eAaImAgNnS
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
okolo	okolo	k7c2
47	#num#	k4
400	#num#	k4
památek	památka	k1gFnPc2
rozdělených	rozdělená	k1gFnPc2
takto	takto	k6eAd1
<g/>
:	:	kIx,
cca	cca	kA
8	#num#	k4
%	%	kIx~
kategorie	kategorie	k1gFnSc2
A	A	kA
<g/>
,	,	kIx,
51	#num#	k4
%	%	kIx~
kategorie	kategorie	k1gFnSc2
B	B	kA
a	a	k8xC
zbytek	zbytek	k1gInSc1
kategorie	kategorie	k1gFnSc2
C	C	kA
<g/>
(	(	kIx(
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dalmeny	Dalmen	k1gInPc1
House	house	k1gNnSc1
u	u	k7c2
Edinburghu	Edinburgh	k1gInSc2
(	(	kIx(
<g/>
A	A	kA
<g/>
)	)	kIx)
</s>
<s>
Akvadukt	akvadukt	k1gInSc1
přes	přes	k7c4
řeku	řeka	k1gFnSc4
Avon	Avona	k1gFnPc2
(	(	kIx(
<g/>
A	a	k9
<g/>
)	)	kIx)
</s>
<s>
Edinburský	Edinburský	k2eAgInSc1d1
Lochend	Lochend	k1gInSc1
Castle	Castle	k1gFnSc1
(	(	kIx(
<g/>
B	B	kA
<g/>
)	)	kIx)
</s>
<s>
Muzeum	muzeum	k1gNnSc1
v	v	k7c6
Montrose	Montrosa	k1gFnSc6
(	(	kIx(
<g/>
B	B	kA
<g/>
)	)	kIx)
</s>
<s>
Cookney	Cookney	k1gInPc1
Church	Churcha	k1gFnPc2
v	v	k7c6
Aberdeenshiru	Aberdeenshir	k1gInSc6
(	(	kIx(
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Severní	severní	k2eAgNnSc1d1
Irsko	Irsko	k1gNnSc1
</s>
<s>
Stejně	stejně	k6eAd1
jako	jako	k9
v	v	k7c6
Anglii	Anglie	k1gFnSc6
a	a	k8xC
Walesu	Wales	k1gInSc2
existují	existovat	k5eAaImIp3nP
i	i	k9
v	v	k7c6
Severním	severní	k2eAgNnSc6d1
Irsku	Irsko	k1gNnSc6
tři	tři	k4xCgInPc1
stupně	stupeň	k1gInPc1
ochrany	ochrana	k1gFnSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
stupeň	stupeň	k1gInSc1
A	A	kA
–	–	k?
stavby	stavba	k1gFnPc4
národního	národní	k2eAgInSc2d1
významu	význam	k1gInSc2
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
vynikající	vynikající	k2eAgNnPc1d1
architektonická	architektonický	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
nebo	nebo	k8xC
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
zachovalé	zachovalý	k2eAgInPc4d1
příklady	příklad	k1gInPc4
určitého	určitý	k2eAgNnSc2d1
období	období	k1gNnSc2
a	a	k8xC
stylu	styl	k1gInSc2
</s>
<s>
stupeň	stupeň	k1gInSc1
B	B	kA
<g/>
+	+	kIx~
–	–	k?
stavby	stavba	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
kvůli	kvůli	k7c3
některým	některý	k3yIgMnPc3
nedostatkům	nedostatek	k1gInPc3
nedosahují	dosahovat	k5eNaImIp3nP
stupně	stupeň	k1gInPc1
A	a	k9
<g/>
,	,	kIx,
ale	ale	k8xC
současně	současně	k6eAd1
obsahují	obsahovat	k5eAaImIp3nP
rysy	rys	k1gInPc4
(	(	kIx(
<g/>
případně	případně	k6eAd1
historický	historický	k2eAgInSc1d1
význam	význam	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
jsou	být	k5eAaImIp3nP
nad	nad	k7c7
standardem	standard	k1gInSc7
stupně	stupeň	k1gInSc2
B	B	kA
</s>
<s>
stupeň	stupeň	k1gInSc1
B	B	kA
–	–	k?
stavby	stavba	k1gFnPc4
místního	místní	k2eAgInSc2d1
významu	význam	k1gInSc2
a	a	k8xC
zachovalé	zachovalý	k2eAgInPc4d1
příklady	příklad	k1gInPc4
určitého	určitý	k2eAgNnSc2d1
období	období	k1gNnSc2
či	či	k8xC
stylu	styl	k1gInSc2
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1987	#num#	k4
je	být	k5eAaImIp3nS
stupeň	stupeň	k1gInSc1
B	B	kA
rozdělen	rozdělit	k5eAaPmNgInS
na	na	k7c6
B1	B1	k1gFnSc6
u	u	k7c2
budov	budova	k1gFnPc2
jež	jenž	k3xRgFnPc4
byly	být	k5eAaImAgFnP
zařazeny	zařadit	k5eAaPmNgFnP
na	na	k7c6
základě	základ	k1gInSc6
většího	veliký	k2eAgNnSc2d2
množství	množství	k1gNnSc2
znaků	znak	k1gInPc2
a	a	k8xC
naopak	naopak	k6eAd1
B	B	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
Severním	severní	k2eAgNnSc6d1
Irsku	Irsko	k1gNnSc6
8	#num#	k4
424	#num#	k4
památek	památka	k1gFnPc2
označených	označený	k2eAgFnPc2d1
jako	jako	k8xC,k8xS
listed	listed	k1gMnSc1
building	building	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
byla	být	k5eAaImAgFnS
stupně	stupeň	k1gInSc2
B.	B.	kA
</s>
<s>
Bangor	Bangor	k1gInSc1
Abbey	Abbea	k1gFnSc2
v	v	k7c6
hrabství	hrabství	k1gNnSc6
Down	Downa	k1gFnPc2
(	(	kIx(
<g/>
A	a	k9
<g/>
)	)	kIx)
</s>
<s>
Grand	grand	k1gMnSc1
Opera	opera	k1gFnSc1
House	house	k1gNnSc1
v	v	k7c6
Belfastu	Belfast	k1gInSc6
(	(	kIx(
<g/>
A	A	kA
<g/>
)	)	kIx)
</s>
<s>
Hrad	hrad	k1gInSc1
Necarne	Necarn	k1gInSc5
v	v	k7c6
hrabství	hrabství	k1gNnSc6
Fermanagh	Fermanagha	k1gFnPc2
(	(	kIx(
<g/>
B	B	kA
<g/>
+	+	kIx~
<g/>
)	)	kIx)
</s>
<s>
Queen	Queen	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Bridge	Bridge	k1gNnSc7
v	v	k7c6
Belfastu	Belfast	k1gInSc6
(	(	kIx(
<g/>
B	B	kA
<g/>
+	+	kIx~
<g/>
)	)	kIx)
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
Carrickfergus	Carrickfergus	k1gMnSc1
(	(	kIx(
<g/>
B	B	kA
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Listed	Listed	k1gInSc1
building	building	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
unusual	unusuat	k5eAaBmAgInS,k5eAaPmAgInS,k5eAaImAgInS
buildings	buildings	k6eAd1
granted	granted	k1gInSc1
listed	listed	k1gInSc1
status	status	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daily	Dailo	k1gNnPc7
Telegraph	Telegrapha	k1gFnPc2
<g/>
,	,	kIx,
2011-06-08	2011-06-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Principles	Principles	k1gInSc1
of	of	k?
Selection	Selection	k1gInSc1
for	forum	k1gNnPc2
Listing	Listing	k1gInSc4
Buildings	Buildings	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Department	department	k1gInSc1
for	forum	k1gNnPc2
culture	cultur	k1gMnSc5
<g/>
,	,	kIx,
media	medium	k1gNnPc4
and	and	k?
sport	sport	k1gInSc1
<g/>
,	,	kIx,
březen	březen	k1gInSc1
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Listed	Listed	k1gInSc1
buildings	buildings	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Department	department	k1gInSc1
for	forum	k1gNnPc2
culture	cultur	k1gMnSc5
<g/>
,	,	kIx,
media	medium	k1gNnPc4
and	and	k?
sport	sport	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Listed	Listed	k1gInSc1
buildings	buildings	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
English	English	k1gInSc1
Heritage	Heritage	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Buildings	Buildings	k1gInSc1
&	&	k?
Conservation	Conservation	k1gInSc1
Areas	Areas	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cadw	Cadw	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
What	What	k1gInSc1
is	is	k?
listing	listing	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historic	Historice	k1gInPc2
Scotland	Scotlanda	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc1
the	the	k?
Protection	Protection	k1gInSc1
of	of	k?
Scotland	Scotland	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Listed	Listed	k1gMnSc1
Buildings	Buildings	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Edinburgh	Edinburgh	k1gMnSc1
<g/>
:	:	kIx,
Historic	Historic	k1gMnSc1
Scotland	Scotlanda	k1gFnPc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
84917	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
13	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Criteria	Criterium	k1gNnSc2
for	forum	k1gNnPc2
Listing	Listing	k1gInSc1
Review	Review	k1gFnPc1
<g/>
,	,	kIx,
strany	strana	k1gFnPc1
10	#num#	k4
<g/>
–	–	k?
<g/>
11	#num#	k4
a	a	k8xC
16	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Northern	Northern	k1gMnSc1
Ireland	Irelanda	k1gFnPc2
Environment	Environment	k1gMnSc1
Agency	Agenca	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Statistics	Statistics	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Northern	Northern	k1gMnSc1
Ireland	Irelanda	k1gFnPc2
Environment	Environment	k1gMnSc1
Agency	Agenca	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Site	Site	k6eAd1
of	of	k?
Special	Special	k1gMnSc1
Scientific	Scientific	k1gMnSc1
Interest	Interest	k1gMnSc1
</s>
<s>
Scheduled	Scheduled	k1gInSc1
monument	monument	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
listed	listed	k1gInSc1
building	building	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
English	English	k1gInSc1
Heritage	Heritag	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Cadw	Cadw	k1gFnSc1
–	–	k?
Welsh	Welsh	k1gInSc1
Government	Government	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
historic	historic	k1gMnSc1
environment	environment	k1gMnSc1
service	service	k1gFnSc2
Archivováno	archivován	k2eAgNnSc1d1
6	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Historic	Historice	k1gFnPc2
Scotland	Scotlando	k1gNnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Northern	Northern	k1gInSc1
Ireland	Ireland	k1gInSc1
Environment	Environment	k1gInSc4
Agency	Agenca	k1gFnSc2
</s>
