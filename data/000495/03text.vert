<s>
Juno	Juno	k1gFnSc1	Juno
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Iuno	Iuno	k1gFnSc1	Iuno
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
bohyně	bohyně	k1gFnSc1	bohyně
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
(	(	kIx(	(
<g/>
její	její	k3xOp3gNnSc4	její
jméno	jméno	k1gNnSc4	jméno
ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
bylo	být	k5eAaImAgNnS	být
Héra	Héra	k1gFnSc1	Héra
<g/>
,	,	kIx,	,
Etruskové	Etrusk	k1gMnPc1	Etrusk
ji	on	k3xPp3gFnSc4	on
nazývali	nazývat	k5eAaImAgMnP	nazývat
Uni	Uni	k1gMnPc1	Uni
<g/>
)	)	kIx)	)
-	-	kIx~	-
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc4	ten
královna	královna	k1gFnSc1	královna
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Juno	Juno	k1gFnSc1	Juno
byla	být	k5eAaImAgFnS	být
sestrou	sestra	k1gFnSc7	sestra
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k8xC	i
manželkou	manželka	k1gFnSc7	manželka
Jova	Jova	k1gMnSc1	Jova
<g/>
,	,	kIx,	,
vládce	vládce	k1gMnSc1	vládce
všech	všecek	k3xTgMnPc2	všecek
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
a	a	k8xC	a
matkou	matka	k1gFnSc7	matka
Hébé	Hébé	k1gFnSc2	Hébé
<g/>
,	,	kIx,	,
Vulkána	Vulkán	k2eAgFnSc1d1	Vulkána
a	a	k8xC	a
Marta	Marta	k1gFnSc1	Marta
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
postav	postava	k1gFnPc2	postava
božstva	božstvo	k1gNnSc2	božstvo
starořímského	starořímský	k2eAgNnSc2d1	starořímské
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jupiterem	Jupiter	k1gMnSc7	Jupiter
a	a	k8xC	a
Minervou	Minerva	k1gFnSc7	Minerva
byla	být	k5eAaImAgFnS	být
členem	člen	k1gMnSc7	člen
Triády	triáda	k1gFnSc2	triáda
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Jupiter	Jupiter	k1gInSc4	Jupiter
měla	mít	k5eAaImAgFnS	mít
Juno	Juno	k1gFnSc1	Juno
schopnost	schopnost	k1gFnSc4	schopnost
házet	házet	k5eAaImF	házet
blesky	blesk	k1gInPc4	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
vyobrazena	vyobrazen	k2eAgFnSc1d1	vyobrazena
se	s	k7c7	s
zbraní	zbraň	k1gFnSc7	zbraň
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
a	a	k8xC	a
v	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
z	z	k7c2	z
kozí	kozí	k2eAgFnSc2d1	kozí
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
část	část	k1gFnSc1	část
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
Římští	římský	k2eAgMnPc1d1	římský
vojáci	voják	k1gMnPc1	voják
oblékali	oblékat	k5eAaImAgMnP	oblékat
při	při	k7c6	při
tažení	tažení	k1gNnSc6	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Junina	Junin	k2eAgFnSc1d1	Junin
doména	doména	k1gFnSc1	doména
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
živá	živý	k2eAgFnSc1d1	živá
síla	síla	k1gFnSc1	síla
nebo	nebo	k8xC	nebo
mládí	mládí	k1gNnSc1	mládí
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
slábnutím	slábnutí	k1gNnSc7	slábnutí
významu	význam	k1gInSc2	význam
Starověkého	starověký	k2eAgNnSc2d1	starověké
Řecka	Řecko	k1gNnSc2	Řecko
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
význam	význam	k1gInSc1	význam
Juno	Juno	k1gFnSc2	Juno
omezil	omezit	k5eAaPmAgInS	omezit
na	na	k7c4	na
choť	choť	k1gFnSc4	choť
Jupitera	Jupiter	k1gMnSc2	Jupiter
a	a	k8xC	a
královnu	královna	k1gFnSc4	královna
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Přídomky	přídomek	k1gInPc1	přídomek
Interduca	Interducum	k1gNnSc2	Interducum
a	a	k8xC	a
Domiduca	Domiducum	k1gNnSc2	Domiducum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jí	on	k3xPp3gFnSc7	on
Římané	Říman	k1gMnPc1	Říman
dávali	dávat	k5eAaImAgMnP	dávat
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
nevěstu	nevěsta	k1gFnSc4	nevěsta
k	k	k7c3	k
manželství	manželství	k1gNnSc3	manželství
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
nevěstu	nevěsta	k1gFnSc4	nevěsta
do	do	k7c2	do
jejího	její	k3xOp3gInSc2	její
nového	nový	k2eAgInSc2d1	nový
domova	domov	k1gInSc2	domov
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bohyni	bohyně	k1gFnSc4	bohyně
Juno	Juno	k1gFnSc2	Juno
přisuzují	přisuzovat	k5eAaImIp3nP	přisuzovat
funkci	funkce	k1gFnSc4	funkce
jakési	jakýsi	k3yIgFnSc2	jakýsi
patronky	patronka	k1gFnSc2	patronka
nevěst	nevěsta	k1gFnPc2	nevěsta
nebo	nebo	k8xC	nebo
obecně	obecně	k6eAd1	obecně
manželství	manželství	k1gNnSc1	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Věří	věřit	k5eAaImIp3nS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byla	být	k5eAaImAgFnS	být
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
konána	konán	k2eAgFnSc1d1	konána
slavnost	slavnost	k1gFnSc1	slavnost
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
uctívání	uctívání	k1gNnSc4	uctívání
(	(	kIx(	(
<g/>
připadající	připadající	k2eAgMnSc1d1	připadající
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
July	Jula	k1gMnSc2	Jula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
Juno	Juno	k1gFnSc2	Juno
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
přídomky	přídomek	k1gInPc1	přídomek
Juno	Juno	k1gFnSc2	Juno
vyzdvihují	vyzdvihovat	k5eAaImIp3nP	vyzdvihovat
jako	jako	k9	jako
královnu	královna	k1gFnSc4	královna
<g/>
,	,	kIx,	,
ochránkyni	ochránkyně	k1gFnSc4	ochránkyně
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
bohyni	bohyně	k1gFnSc4	bohyně
osudu	osud	k1gInSc2	osud
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
tu	tu	k6eAd1	tu
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
děti	dítě	k1gFnPc4	dítě
ke	k	k7c3	k
světlu	světlo	k1gNnSc3	světlo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tato	tento	k3xDgFnSc1	tento
označení	označení	k1gNnSc4	označení
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
poetickou	poetický	k2eAgFnSc4d1	poetická
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
ne	ne	k9	ne
takový	takový	k3xDgInSc4	takový
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
