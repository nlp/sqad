<s>
Juno	Juno	k1gFnSc1	Juno
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Iuno	Iuno	k1gFnSc1	Iuno
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
bohyně	bohyně	k1gFnSc1	bohyně
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
(	(	kIx(	(
<g/>
její	její	k3xOp3gNnSc4	její
jméno	jméno	k1gNnSc4	jméno
ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
bylo	být	k5eAaImAgNnS	být
Héra	Héra	k1gFnSc1	Héra
<g/>
,	,	kIx,	,
Etruskové	Etrusk	k1gMnPc1	Etrusk
ji	on	k3xPp3gFnSc4	on
nazývali	nazývat	k5eAaImAgMnP	nazývat
Uni	Uni	k1gMnPc1	Uni
<g/>
)	)	kIx)	)
-	-	kIx~	-
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc4	ten
královna	královna	k1gFnSc1	královna
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
