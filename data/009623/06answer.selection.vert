<s>
Vestfálský	vestfálský	k2eAgInSc1d1	vestfálský
mír	mír	k1gInSc1	mír
roku	rok	k1gInSc2	rok
1648	[number]	k4	1648
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
osmdesátiletou	osmdesátiletý	k2eAgFnSc4d1	osmdesátiletá
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Republika	republika	k1gFnSc1	republika
spojených	spojený	k2eAgFnPc2d1	spojená
nizozemských	nizozemský	k2eAgFnPc2d1	nizozemská
provincií	provincie	k1gFnPc2	provincie
obdržela	obdržet	k5eAaPmAgFnS	obdržet
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
