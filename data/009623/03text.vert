<p>
<s>
Osmdesátiletá	osmdesátiletý	k2eAgFnSc1d1	osmdesátiletá
válka	válka	k1gFnSc1	válka
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
období	období	k1gNnSc2	období
nizozemských	nizozemský	k2eAgNnPc2d1	Nizozemské
povstání	povstání	k1gNnPc2	povstání
a	a	k8xC	a
bojů	boj	k1gInPc2	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
proti	proti	k7c3	proti
španělské	španělský	k2eAgFnSc3d1	španělská
nadvládě	nadvláda	k1gFnSc3	nadvláda
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1568	[number]	k4	1568
a	a	k8xC	a
1648	[number]	k4	1648
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
datace	datace	k1gFnSc1	datace
však	však	k9	však
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jednotná	jednotný	k2eAgFnSc1d1	jednotná
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
historik	historik	k1gMnSc1	historik
Pieter	Pieter	k1gMnSc1	Pieter
Geyl	Geyl	k1gMnSc1	Geyl
vyčleňuje	vyčleňovat	k5eAaImIp3nS	vyčleňovat
z	z	k7c2	z
osmdesátileté	osmdesátiletý	k2eAgFnSc2d1	osmdesátiletá
války	válka	k1gFnSc2	válka
pojem	pojem	k1gInSc4	pojem
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
počátek	počátek	k1gInSc4	počátek
posouvá	posouvat	k5eAaImIp3nS	posouvat
až	až	k9	až
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1555	[number]	k4	1555
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
končil	končit	k5eAaImAgInS	končit
svoji	svůj	k3xOyFgFnSc4	svůj
vládu	vláda	k1gFnSc4	vláda
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
<g/>
,	,	kIx,	,
a	a	k8xC	a
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
ho	on	k3xPp3gNnSc2	on
rokem	rok	k1gInSc7	rok
1609	[number]	k4	1609
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
podepsáno	podepsat	k5eAaPmNgNnS	podepsat
dvanáctileté	dvanáctiletý	k2eAgNnSc1d1	dvanáctileté
příměří	příměří	k1gNnSc1	příměří
<g/>
.	.	kIx.	.
</s>
<s>
Vestfálský	vestfálský	k2eAgInSc1d1	vestfálský
mír	mír	k1gInSc1	mír
roku	rok	k1gInSc2	rok
1648	[number]	k4	1648
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
osmdesátiletou	osmdesátiletý	k2eAgFnSc4d1	osmdesátiletá
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Republika	republika	k1gFnSc1	republika
spojených	spojený	k2eAgFnPc2d1	spojená
nizozemských	nizozemský	k2eAgFnPc2d1	nizozemská
provincií	provincie	k1gFnPc2	provincie
obdržela	obdržet	k5eAaPmAgFnS	obdržet
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
revoluce	revoluce	k1gFnSc1	revoluce
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
