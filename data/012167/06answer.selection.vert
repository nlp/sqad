<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Xaverius	Xaverius	k1gMnSc1	Xaverius
je	být	k5eAaImIp3nS	být
romaneto	romaneto	k1gNnSc4	romaneto
Jakuba	Jakub	k1gMnSc2	Jakub
Arbese	Arbes	k1gMnSc2	Arbes
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgInPc4d1	vydaný
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
<g/>
,	,	kIx,	,
nejspíš	nejspíš	k9	nejspíš
nejznámější	známý	k2eAgInPc4d3	nejznámější
a	a	k8xC	a
nejslavnější	slavný	k2eAgNnSc4d3	nejslavnější
Arbesovo	Arbesův	k2eAgNnSc4d1	Arbesovo
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
