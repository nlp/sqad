<s>
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Conrad	Conrada	k1gFnPc2	Conrada
Röntgen	Röntgen	k1gInSc1	Röntgen
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1845	[number]	k4	1845
Lennep	Lennep	k1gInSc1	Lennep
<g/>
,	,	kIx,	,
Prusko	Prusko	k1gNnSc1	Prusko
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1923	[number]	k4	1923
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
<g/>
.	.	kIx.	.
</s>
