<s>
Apačský	apačský	k2eAgMnSc1d1	apačský
náčelník	náčelník	k1gMnSc1	náčelník
Vinnetou	Vinneta	k1gFnSc7	Vinneta
je	být	k5eAaImIp3nS	být
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
a	a	k8xC	a
pokrevním	pokrevní	k2eAgMnSc7d1	pokrevní
bratrem	bratr	k1gMnSc7	bratr
Old	Olda	k1gFnPc2	Olda
Shatterhandem	Shatterhand	k1gInSc7	Shatterhand
nejznámější	známý	k2eAgFnPc1d3	nejznámější
literární	literární	k2eAgFnPc1d1	literární
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
německý	německý	k2eAgMnSc1d1	německý
spisovatel	spisovatel	k1gMnSc1	spisovatel
Karl	Karl	k1gMnSc1	Karl
May	May	k1gMnSc1	May
<g/>
.	.	kIx.	.
</s>
<s>
Vinnetouovo	Vinnetouův	k2eAgNnSc1d1	Vinnetouův
jméno	jméno	k1gNnSc1	jméno
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
Mayově	Mayův	k2eAgFnSc6d1	Mayova
novele	novela	k1gFnSc6	novela
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
Old	Olda	k1gFnPc2	Olda
Firehand	Firehanda	k1gFnPc2	Firehanda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
novela	novela	k1gFnSc1	novela
se	se	k3xPyFc4	se
v	v	k7c6	v
dosti	dosti	k6eAd1	dosti
přepracované	přepracovaný	k2eAgFnSc6d1	přepracovaná
podobě	podoba	k1gFnSc6	podoba
stala	stát	k5eAaPmAgFnS	stát
podstatnou	podstatný	k2eAgFnSc7d1	podstatná
částí	část	k1gFnSc7	část
druhého	druhý	k4xOgInSc2	druhý
dílu	díl	k1gInSc2	díl
románu	román	k1gInSc2	román
Vinnetou	Vinneta	k1gFnSc7	Vinneta
(	(	kIx(	(
<g/>
tři	tři	k4xCgInPc4	tři
díly	díl	k1gInPc1	díl
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
díl	díl	k1gInSc1	díl
1909	[number]	k4	1909
<g/>
–	–	k?	–
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
jsou	být	k5eAaImIp3nP	být
vylíčeny	vylíčen	k2eAgInPc4d1	vylíčen
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
prožil	prožít	k5eAaPmAgMnS	prožít
Vinnetou	Vinnetý	k2eAgFnSc4d1	Vinnetý
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
pokrevní	pokrevní	k2eAgMnSc1d1	pokrevní
bratr	bratr	k1gMnSc1	bratr
Old	Olda	k1gFnPc2	Olda
Shatterhand	Shatterhanda	k1gFnPc2	Shatterhanda
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
jejich	jejich	k3xOp3gNnSc2	jejich
prvního	první	k4xOgNnSc2	první
setkání	setkání	k1gNnSc2	setkání
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
apačský	apačský	k2eAgMnSc1d1	apačský
náčelník	náčelník	k1gMnSc1	náčelník
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
dílu	díl	k1gInSc6	díl
Vinnetoua	Vinnetouum	k1gNnSc2	Vinnetouum
apačský	apačský	k2eAgMnSc1d1	apačský
náčelník	náčelník	k1gMnSc1	náčelník
již	již	k6eAd1	již
nevystupuje	vystupovat	k5eNaImIp3nS	vystupovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zhruba	zhruba	k6eAd1	zhruba
dvacet	dvacet	k4xCc4	dvacet
až	až	k9	až
třicet	třicet	k4xCc4	třicet
let	léto	k1gNnPc2	léto
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Vinnetou	Vinnést	k5eAaPmIp3nP	Vinnést
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
stává	stávat	k5eAaImIp3nS	stávat
symbolem	symbol	k1gInSc7	symbol
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
bratrství	bratrství	k1gNnSc2	bratrství
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
společná	společný	k2eAgNnPc1d1	společné
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
Old	Olda	k1gFnPc2	Olda
Shatterhanda	Shatterhanda	k1gFnSc1	Shatterhanda
a	a	k8xC	a
Vinnetoua	Vinnetoua	k1gFnSc1	Vinnetoua
jsou	být	k5eAaImIp3nP	být
pochopitelně	pochopitelně	k6eAd1	pochopitelně
obsahem	obsah	k1gInSc7	obsah
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
autorových	autorův	k2eAgFnPc2d1	autorova
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Syn	syn	k1gMnSc1	syn
lovce	lovec	k1gMnSc2	lovec
medvědů	medvěd	k1gMnPc2	medvěd
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Duch	duch	k1gMnSc1	duch
Llana	Llano	k1gNnSc2	Llano
Estacada	Estacada	k1gFnSc1	Estacada
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Old	Olda	k1gFnPc2	Olda
Surehand	Surehanda	k1gFnPc2	Surehanda
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
–	–	k?	–
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Satan	Satan	k1gMnSc1	Satan
a	a	k8xC	a
Jidáš	Jidáš	k1gMnSc1	Jidáš
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
–	–	k?	–
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
románu	román	k1gInSc6	román
se	se	k3xPyFc4	se
Vinnetou	Vinneta	k1gFnSc7	Vinneta
dokonce	dokonce	k9	dokonce
vydává	vydávat	k5eAaPmIp3nS	vydávat
s	s	k7c7	s
Old	Olda	k1gFnPc2	Olda
Shatterhandem	Shatterhand	k1gInSc7	Shatterhand
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
na	na	k7c6	na
zpáteční	zpáteční	k2eAgFnSc6d1	zpáteční
cestě	cesta	k1gFnSc6	cesta
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vinnetou	Vinneta	k1gFnSc7	Vinneta
je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
náčelníka	náčelník	k1gMnSc2	náčelník
mescalerských	mescalerský	k2eAgMnPc2d1	mescalerský
Apačů	Apač	k1gMnPc2	Apač
Inču-čuny	Inču-čuna	k1gFnSc2	Inču-čuna
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Dobré	dobrý	k2eAgNnSc4d1	dobré
Slunce	slunce	k1gNnSc4	slunce
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
sestru	sestra	k1gFnSc4	sestra
Nšo-či	Nšo-č	k1gFnSc3	Nšo-č
(	(	kIx(	(
<g/>
Krásný	krásný	k2eAgInSc1d1	krásný
Den	den	k1gInSc1	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
mládí	mládí	k1gNnSc4	mládí
jej	on	k3xPp3gInSc2	on
kromě	kromě	k7c2	kromě
otce	otec	k1gMnSc2	otec
vychovával	vychovávat	k5eAaImAgMnS	vychovávat
také	také	k6eAd1	také
Klekí-petra	Klekíetr	k1gMnSc4	Klekí-petr
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
bílý	bílý	k1gMnSc1	bílý
otec	otec	k1gMnSc1	otec
<g/>
)	)	kIx)	)
Apačů	Apač	k1gMnPc2	Apač
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
základě	základ	k1gInSc6	základ
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
lásky	láska	k1gFnSc2	láska
k	k	k7c3	k
bližním	bližní	k1gMnPc3	bližní
(	(	kIx(	(
<g/>
Klekí-petra	Klekíetra	k1gFnSc1	Klekí-petra
byl	být	k5eAaImAgInS	být
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
Němec	Němec	k1gMnSc1	Němec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
dlouho	dlouho	k6eAd1	dlouho
s	s	k7c7	s
Indiány	Indián	k1gMnPc7	Indián
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
své	své	k1gNnSc4	své
dobré	dobrý	k2eAgFnSc2d1	dobrá
vlastnosti	vlastnost	k1gFnSc2	vlastnost
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
kmene	kmen	k1gInSc2	kmen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vinnetou	Vinneta	k1gFnSc7	Vinneta
byl	být	k5eAaImAgInS	být
zamilován	zamilován	k2eAgInSc1d1	zamilován
do	do	k7c2	do
Ribanny	Ribanna	k1gFnSc2	Ribanna
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Assiniboinů	Assiniboin	k1gInPc2	Assiniboin
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
si	se	k3xPyFc3	se
však	však	k9	však
vyvolila	vyvolit	k5eAaPmAgFnS	vyvolit
za	za	k7c4	za
muže	muž	k1gMnSc4	muž
Vinnetouova	Vinnetouův	k2eAgMnSc4d1	Vinnetouův
přítele	přítel	k1gMnSc4	přítel
<g/>
,	,	kIx,	,
zálesáka	zálesák	k1gMnSc4	zálesák
Old	Olda	k1gFnPc2	Olda
Firehanda	Firehando	k1gNnSc2	Firehando
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
Ribanna	Ribanna	k1gFnSc1	Ribanna
zavražděna	zavraždit	k5eAaPmNgFnS	zavraždit
bílým	bílý	k2eAgInSc7d1	bílý
padoušským	padoušský	k2eAgInSc7d1	padoušský
náčelníkem	náčelník	k1gInSc7	náčelník
indiánského	indiánský	k2eAgInSc2d1	indiánský
kmene	kmen	k1gInSc2	kmen
Ponků	ponk	k1gInPc2	ponk
Parranohem	Parranoh	k1gMnSc7	Parranoh
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
smrt	smrt	k1gFnSc1	smrt
je	být	k5eAaImIp3nS	být
pomstěna	pomstit	k5eAaImNgFnS	pomstit
již	již	k6eAd1	již
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Old	Olda	k1gFnPc2	Olda
Shatterhanda	Shatterhando	k1gNnSc2	Shatterhando
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Old	Olda	k1gFnPc2	Olda
Shatterhandem	Shatterhand	k1gInSc7	Shatterhand
se	se	k3xPyFc4	se
Vinnetou	Vinnetý	k2eAgFnSc4d1	Vinnetý
přes	přes	k7c4	přes
počáteční	počáteční	k2eAgInSc4d1	počáteční
konflikt	konflikt	k1gInSc4	konflikt
tak	tak	k6eAd1	tak
spřátelí	spřátelit	k5eAaPmIp3nS	spřátelit
<g/>
,	,	kIx,	,
že	že	k8xS	že
uzavřou	uzavřít	k5eAaPmIp3nP	uzavřít
pokrevní	pokrevní	k2eAgNnSc4d1	pokrevní
bratrství	bratrství	k1gNnSc4	bratrství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
své	svůj	k3xOyFgFnSc2	svůj
sestry	sestra	k1gFnSc2	sestra
(	(	kIx(	(
<g/>
zavraždil	zavraždit	k5eAaPmAgMnS	zavraždit
je	on	k3xPp3gNnSc4	on
běloch	běloch	k1gMnSc1	běloch
Santer	Santer	k1gMnSc1	Santer
kvůli	kvůli	k7c3	kvůli
zlatu	zlato	k1gNnSc3	zlato
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Nugget-tsil	Nuggetsil	k1gFnSc2	Nugget-tsil
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Vinnetou	Vinneta	k1gFnSc7	Vinneta
stává	stávat	k5eAaImIp3nS	stávat
apačským	apačský	k2eAgMnSc7d1	apačský
náčelníkem	náčelník	k1gMnSc7	náčelník
a	a	k8xC	a
dědí	dědit	k5eAaImIp3nS	dědit
slavnou	slavný	k2eAgFnSc4d1	slavná
Inču-čunovu	Inču-čunův	k2eAgFnSc4d1	Inču-čunův
"	"	kIx"	"
<g/>
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
pušku	puška	k1gFnSc4	puška
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Silberbüchse	Silberbüchse	k1gFnSc1	Silberbüchse
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dvouhlavňová	dvouhlavňový	k2eAgFnSc1d1	dvouhlavňová
ručnice	ručnice	k1gFnSc1	ručnice
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
pažba	pažba	k1gFnSc1	pažba
je	být	k5eAaImIp3nS	být
zdobena	zdobit	k5eAaImNgFnS	zdobit
stříbrem	stříbro	k1gNnSc7	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Vinnetou	Vinneta	k1gFnSc7	Vinneta
jezdí	jezdit	k5eAaImIp3nS	jezdit
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
jménem	jméno	k1gNnSc7	jméno
Ilčí	Ilčí	k2eAgMnSc1d1	Ilčí
(	(	kIx(	(
<g/>
Vítr	vítr	k1gInSc1	vítr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
bratra	bratr	k1gMnSc4	bratr
<g/>
,	,	kIx,	,
Hatátitlu	Hatátitla	k1gFnSc4	Hatátitla
(	(	kIx(	(
<g/>
Blesk	blesk	k1gInSc4	blesk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
daruje	darovat	k5eAaPmIp3nS	darovat
Old	Olda	k1gFnPc2	Olda
Shatterhandovi	Shatterhandův	k2eAgMnPc5d1	Shatterhandův
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
koně	kůň	k1gMnPc1	kůň
Vinnetou	Vinneta	k1gFnSc7	Vinneta
sám	sám	k3xTgMnSc1	sám
vychoval	vychovat	k5eAaPmAgInS	vychovat
<g/>
.	.	kIx.	.
</s>
<s>
Vinnetou	Vinnést	k5eAaPmIp3nP	Vinnést
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
marně	marně	k6eAd1	marně
snaží	snažit	k5eAaImIp3nS	snažit
dopadnout	dopadnout	k5eAaPmF	dopadnout
vraha	vrah	k1gMnSc4	vrah
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
a	a	k8xC	a
své	svůj	k3xOyFgFnSc2	svůj
sestry	sestra	k1gFnSc2	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
pomstě	pomsta	k1gFnSc6	pomsta
zabrání	zabránit	k5eAaPmIp3nS	zabránit
předčasná	předčasný	k2eAgFnSc1d1	předčasná
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Santer	Santer	k1gInSc1	Santer
zahyne	zahynout	k5eAaPmIp3nS	zahynout
až	až	k9	až
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
přispěním	přispění	k1gNnSc7	přispění
Old	Olda	k1gFnPc2	Olda
Shatterhanda	Shatterhando	k1gNnSc2	Shatterhando
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Karla	Karel	k1gMnSc2	Karel
Maye	May	k1gMnSc2	May
zemřel	zemřít	k5eAaPmAgInS	zemřít
Vinnetou	Vinneta	k1gFnSc7	Vinneta
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
datum	datum	k1gNnSc1	datum
však	však	k9	však
příliš	příliš	k6eAd1	příliš
nevyhovuje	vyhovovat	k5eNaImIp3nS	vyhovovat
logice	logika	k1gFnSc3	logika
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
May	May	k1gMnSc1	May
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
Old	Olda	k1gFnPc2	Olda
Shatterhand	Shatterhanda	k1gFnPc2	Shatterhanda
seznámil	seznámit	k5eAaPmAgInS	seznámit
s	s	k7c7	s
Vinnetouem	Vinnetou	k1gInSc7	Vinnetou
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnPc1	jejich
společná	společný	k2eAgNnPc1d1	společné
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
musela	muset	k5eAaImAgNnP	muset
odehrát	odehrát	k5eAaPmF	odehrát
během	během	k7c2	během
pouhých	pouhý	k2eAgNnPc2d1	pouhé
čtrnácti	čtrnáct	k4xCc2	čtrnáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
přičteme	přičíst	k5eAaPmIp1nP	přičíst
Karlova	Karlův	k2eAgNnPc1d1	Karlovo
další	další	k2eAgNnPc1d1	další
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
časově	časově	k6eAd1	časově
naprosto	naprosto	k6eAd1	naprosto
nereálné	reálný	k2eNgNnSc1d1	nereálné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Chronik	chronik	k1gMnSc1	chronik
eines	einesa	k1gFnPc2	einesa
Weltläufers	Weltläufers	k1gInSc1	Weltläufers
<g/>
:	:	kIx,	:
Die	Die	k1gFnSc1	Die
Reisen	Reisen	k1gInSc4	Reisen
von	von	k1gInSc1	von
Old	Olda	k1gFnPc2	Olda
Shatterhand	Shatterhando	k1gNnPc2	Shatterhando
alias	alias	k9	alias
Kara	kara	k1gFnSc1	kara
Ben	Ben	k1gInSc1	Ben
Nemsi	Nemse	k1gFnSc4	Nemse
(	(	kIx(	(
<g/>
vydala	vydat	k5eAaPmAgFnS	vydat
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Karl-May-Verlag	Karl-May-Verlaga	k1gFnPc2	Karl-May-Verlaga
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
její	její	k3xOp3gMnSc1	její
autor	autor	k1gMnSc1	autor
<g/>
,	,	kIx,	,
Němec	Němec	k1gMnSc1	Němec
Hans	Hans	k1gMnSc1	Hans
Imgram	Imgram	k1gInSc4	Imgram
<g/>
,	,	kIx,	,
snaží	snažit	k5eAaImIp3nP	snažit
tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc4	problém
vyřešit	vyřešit	k5eAaPmF	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
pečlivého	pečlivý	k2eAgNnSc2d1	pečlivé
bádání	bádání	k1gNnSc2	bádání
a	a	k8xC	a
porovnávání	porovnávání	k1gNnSc6	porovnávání
románových	románový	k2eAgInPc2d1	románový
a	a	k8xC	a
povídkových	povídkový	k2eAgInPc2d1	povídkový
obsahů	obsah	k1gInPc2	obsah
navzájem	navzájem	k6eAd1	navzájem
sestavil	sestavit	k5eAaPmAgInS	sestavit
důvěryhodnou	důvěryhodný	k2eAgFnSc4d1	důvěryhodná
časovou	časový	k2eAgFnSc4d1	časová
osu	osa	k1gFnSc4	osa
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
Vinnetou	Vinneta	k1gFnSc7	Vinneta
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
1962	[number]	k4	1962
Poklad	poklad	k1gInSc1	poklad
na	na	k7c6	na
Stříbrném	stříbrný	k1gInSc6	stříbrný
jezeře-hlavní	jezeřelavní	k2eAgFnSc2d1	jezeře-hlavní
role	role	k1gFnSc2	role
Pierre	Pierr	k1gInSc5	Pierr
Brice	Brice	k1gMnSc4	Brice
a	a	k8xC	a
Lex	Lex	k1gMnSc4	Lex
Barker	Barker	k1gInSc4	Barker
1963	[number]	k4	1963
Vinnetou	Vinneta	k1gFnSc7	Vinneta
-hlavní	lavní	k2eAgFnSc2d1	-hlavní
role	role	k1gFnSc2	role
Pierre	Pierr	k1gInSc5	Pierr
Brice	Brice	k1gMnSc1	Brice
a	a	k8xC	a
Lex	Lex	k1gMnSc1	Lex
Barker	Barker	k1gMnSc1	Barker
<g />
.	.	kIx.	.
</s>
<s>
1964	[number]	k4	1964
Mezi	mezi	k7c4	mezi
supy	sup	k1gMnPc4	sup
-hlavní	lavní	k2eAgFnSc2d1	-hlavní
role	role	k1gFnSc2	role
Pierre	Pierr	k1gInSc5	Pierr
Brice	Brice	k1gFnPc4	Brice
a	a	k8xC	a
Stuart	Stuart	k1gInSc4	Stuart
Grenger	Grengra	k1gFnPc2	Grengra
1964	[number]	k4	1964
Vinnetou	Vinneta	k1gFnSc7	Vinneta
-	-	kIx~	-
Rudý	rudý	k2eAgMnSc1d1	rudý
gentleman	gentleman	k1gMnSc1	gentleman
-hlavní	lavní	k2eAgFnSc2d1	-hlavní
role	role	k1gFnSc2	role
Pierre	Pierr	k1gInSc5	Pierr
Brice	Brice	k1gMnSc4	Brice
a	a	k8xC	a
Lex	Lex	k1gMnSc4	Lex
Barker	Barker	k1gInSc4	Barker
1964	[number]	k4	1964
Old	Olda	k1gFnPc2	Olda
Shatterhand	Shatterhand	k1gInSc4	Shatterhand
-hlavní	lavní	k2eAgFnSc2d1	-hlavní
role	role	k1gFnSc2	role
Pierre	Pierr	k1gInSc5	Pierr
Brice	Brice	k1gMnSc4	Brice
a	a	k8xC	a
Lex	Lex	k1gMnSc4	Lex
Barker	Barker	k1gInSc4	Barker
1965	[number]	k4	1965
Petrolejový	petrolejový	k2eAgInSc4d1	petrolejový
princ	princ	k1gMnSc1	princ
hlavní	hlavní	k2eAgFnPc4d1	hlavní
role	role	k1gFnPc4	role
Pierre	Pierr	k1gInSc5	Pierr
Brice	Brice	k1gFnPc4	Brice
a	a	k8xC	a
Stuart	Stuart	k1gInSc4	Stuart
Grenger	Grenger	k1gInSc1	Grenger
1965	[number]	k4	1965
Vinnetou	Vinnetý	k2eAgFnSc4d1	Vinnetý
-	-	kIx~	-
poslední	poslední	k2eAgInSc1d1	poslední
výstřel	výstřel	k1gInSc1	výstřel
-hlavní	lavní	k2eAgFnSc2d1	-hlavní
role	role	k1gFnSc2	role
Pierre	Pierr	k1gInSc5	Pierr
<g />
.	.	kIx.	.
</s>
<s>
Brice	Brice	k1gMnSc1	Brice
a	a	k8xC	a
Lex	Lex	k1gMnSc1	Lex
Barker	Barker	k1gMnSc1	Barker
1966	[number]	k4	1966
Vinnetou	Vinnetý	k2eAgFnSc7d1	Vinnetý
a	a	k8xC	a
míšenka	míšenka	k1gFnSc1	míšenka
Apanači	Apanač	k1gInSc6	Apanač
-hlavní	lavní	k2eAgFnPc4d1	-hlavní
role	role	k1gFnPc4	role
Pierre	Pierr	k1gInSc5	Pierr
Brice	Brice	k1gMnSc4	Brice
a	a	k8xC	a
Lex	Lex	k1gMnSc4	Lex
Barker	Barker	k1gInSc4	Barker
1966	[number]	k4	1966
Old	Olda	k1gFnPc2	Olda
Firehand-	Firehand-	k1gFnSc2	Firehand-
hlavní	hlavní	k2eAgFnSc2d1	hlavní
role	role	k1gFnSc2	role
Pierre	Pierr	k1gInSc5	Pierr
Brice	Brice	k1gFnPc4	Brice
a	a	k8xC	a
Rod	rod	k1gInSc4	rod
Cameron	Cameron	k1gInSc1	Cameron
1968	[number]	k4	1968
Vinnetou	Vinnetý	k2eAgFnSc4d1	Vinnetý
a	a	k8xC	a
Old	Olda	k1gFnPc2	Olda
Shatterhand	Shatterhanda	k1gFnPc2	Shatterhanda
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
smrti	smrt	k1gFnSc2	smrt
-hlavní	lavní	k2eAgFnSc2d1	-hlavní
role	role	k1gFnSc2	role
Pierre	Pierr	k1gInSc5	Pierr
Brice	Brice	k1gMnSc4	Brice
a	a	k8xC	a
Lex	Lex	k1gMnSc4	Lex
Barker	Barker	k1gInSc4	Barker
1998	[number]	k4	1998
Vinnetou	Vinneta	k1gFnSc7	Vinneta
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
(	(	kIx(	(
<g/>
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
-hlavní	lavní	k2eAgFnSc2d1	-hlavní
role	role	k1gFnSc2	role
Pierre	Pierr	k1gInSc5	Pierr
Brice	Briec	k1gInSc2	Briec
2016	[number]	k4	2016
Vinnetou	Vinneta	k1gFnSc7	Vinneta
(	(	kIx(	(
<g/>
minisérie	minisérie	k1gFnSc1	minisérie
<g/>
)	)	kIx)	)
hlavní	hlavní	k2eAgFnSc1d1	hlavní
role	role	k1gFnSc1	role
Nik	nika	k1gFnPc2	nika
Xhelilaj	Xhelilaj	k1gInSc4	Xhelilaj
a	a	k8xC	a
Wotan	Wotan	k1gInSc4	Wotan
Wilke	Wilk	k1gInSc2	Wilk
Möhring	Möhring	k1gInSc1	Möhring
Postava	postava	k1gFnSc1	postava
Vinnetoua	Vinnetoua	k1gFnSc1	Vinnetoua
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
znovu	znovu	k6eAd1	znovu
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc1d1	populární
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgInPc2d3	nejznámější
filmů	film	k1gInPc2	film
inspirovaných	inspirovaný	k2eAgFnPc2d1	inspirovaná
dílem	díl	k1gInSc7	díl
Karla	Karel	k1gMnSc2	Karel
Maye	May	k1gMnSc2	May
Poklad	poklad	k1gInSc1	poklad
na	na	k7c6	na
Stříbrném	stříbrný	k2eAgNnSc6d1	stříbrné
jezeře	jezero	k1gNnSc6	jezero
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
a	a	k8xC	a
několika	několik	k4yIc2	několik
následujících	následující	k2eAgFnPc2d1	následující
filmových	filmový	k2eAgFnPc2d1	filmová
zpracováních	zpracování	k1gNnPc6	zpracování
mu	on	k3xPp3gMnSc3	on
propůjčil	propůjčit	k5eAaPmAgMnS	propůjčit
tvář	tvář	k1gFnSc4	tvář
francouzský	francouzský	k2eAgMnSc1d1	francouzský
herec	herec	k1gMnSc1	herec
Pierre	Pierr	k1gMnSc5	Pierr
Brice	Brice	k1gMnSc5	Brice
<g/>
.	.	kIx.	.
</s>
<s>
Filmový	filmový	k2eAgMnSc1d1	filmový
Vinnetou	Vinneta	k1gFnSc7	Vinneta
se	se	k3xPyFc4	se
ale	ale	k9	ale
od	od	k7c2	od
románového	románový	k2eAgNnSc2d1	románové
dosti	dosti	k6eAd1	dosti
liší	lišit	k5eAaImIp3nS	lišit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
uplatněna	uplatněn	k2eAgFnSc1d1	uplatněna
silná	silný	k2eAgFnSc1d1	silná
licentia	licentia	k1gFnSc1	licentia
poetica	poetica	k1gFnSc1	poetica
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc3d1	původní
knižní	knižní	k2eAgFnSc3d1	knižní
podobě	podoba	k1gFnSc3	podoba
je	být	k5eAaImIp3nS	být
nejpodobnější	podobný	k2eAgInSc1d3	nejpodobnější
Poklad	poklad	k1gInSc1	poklad
na	na	k7c6	na
Stříbrném	stříbrný	k2eAgNnSc6d1	stříbrné
jezeře	jezero	k1gNnSc6	jezero
a	a	k8xC	a
první	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
Vinnetoua	Vinnetouum	k1gNnSc2	Vinnetouum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
(	(	kIx(	(
<g/>
pomineme	pominout	k5eAaPmIp1nP	pominout
<g/>
-li	i	k?	-li
<g/>
,	,	kIx,	,
že	že	k8xS	že
Klekí-petru	Klekíetr	k1gInSc2	Klekí-petr
zastřelil	zastřelit	k5eAaPmAgInS	zastřelit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Santer	Santer	k1gMnSc1	Santer
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c6	na
konci	konec	k1gInSc6	konec
filmu	film	k1gInSc2	film
zahyne	zahynout	k5eAaPmIp3nS	zahynout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
filmová	filmový	k2eAgNnPc1d1	filmové
zpracování	zpracování	k1gNnPc1	zpracování
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
již	již	k6eAd1	již
pouze	pouze	k6eAd1	pouze
motivy	motiv	k1gInPc1	motiv
Mayových	Mayových	k2eAgNnPc2d1	Mayových
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
na	na	k7c4	na
příklad	příklad	k1gInSc4	příklad
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
dílu	díl	k1gInSc6	díl
Vinnetoua	Vinnetou	k2eAgFnSc1d1	Vinnetoua
Vinnetou	Vinneta	k1gFnSc7	Vinneta
–	–	k?	–
Rudý	rudý	k1gMnSc1	rudý
gentleman	gentleman	k1gMnSc1	gentleman
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
Ribanna	Ribann	k1gInSc2	Ribann
žije	žít	k5eAaImIp3nS	žít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
Vinnetoua	Vinnetou	k1gInSc2	Vinnetou
zamilovaná	zamilovaná	k1gFnSc1	zamilovaná
a	a	k8xC	a
Old	Olda	k1gFnPc2	Olda
Firehand	Firehanda	k1gFnPc2	Firehanda
se	se	k3xPyFc4	se
ve	v	k7c6	v
filmu	film	k1gInSc6	film
vůbec	vůbec	k9	vůbec
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
díle	dílo	k1gNnSc6	dílo
Vinnetou	Vinneta	k1gFnSc7	Vinneta
–	–	k?	–
Poslední	poslední	k2eAgInSc4d1	poslední
výstřel	výstřel	k1gInSc4	výstřel
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
hyne	hynout	k5eAaImIp3nS	hynout
Vinnetou	Vinnetý	k2eAgFnSc7d1	Vinnetý
kulkou	kulka	k1gFnSc7	kulka
bídáka	bídák	k1gMnSc2	bídák
Rollinse	Rollins	k1gMnSc2	Rollins
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
epizodní	epizodní	k2eAgFnSc1d1	epizodní
postava	postava	k1gFnSc1	postava
druhého	druhý	k4xOgInSc2	druhý
dílu	díl	k1gInSc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
rána	rána	k1gFnSc1	rána
nebyla	být	k5eNaImAgFnS	být
smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
volné	volný	k2eAgNnSc1d1	volné
pokračování	pokračování	k1gNnSc1	pokračování
Vinnetou	Vinneta	k1gFnSc7	Vinneta
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
také	také	k6eAd1	také
pod	pod	k7c7	pod
českým	český	k2eAgInSc7d1	český
názvem	název	k1gInSc7	název
Návrat	návrat	k1gInSc1	návrat
Vinnetoua	Vinnetou	k1gInSc2	Vinnetou
<g/>
.	.	kIx.	.
</s>
<s>
Plánovaný	plánovaný	k2eAgInSc4d1	plánovaný
film	film	k1gInSc4	film
Vinnetou	Vinnetý	k2eAgFnSc4d1	Vinnetý
v	v	k7c6	v
cizích	cizí	k2eAgFnPc6d1	cizí
stopách	stopa	k1gFnPc6	stopa
nebyl	být	k5eNaImAgInS	být
vůbec	vůbec	k9	vůbec
realizován	realizovat	k5eAaBmNgInS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
seriál	seriál	k1gInSc1	seriál
o	o	k7c6	o
Vinnetouovi	Vinnetou	k1gMnSc6	Vinnetou
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Vinnetoua	Vinnetoua	k1gMnSc1	Vinnetoua
opět	opět	k6eAd1	opět
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Pierre	Pierr	k1gMnSc5	Pierr
Brice	Brice	k1gMnSc5	Brice
<g/>
,	,	kIx,	,
u	u	k7c2	u
fanoušků	fanoušek	k1gMnPc2	fanoušek
Vinnetoua	Vinnetou	k1gInSc2	Vinnetou
ale	ale	k8xC	ale
seriál	seriál	k1gInSc4	seriál
pozitivní	pozitivní	k2eAgNnSc4d1	pozitivní
hodnocení	hodnocení	k1gNnSc4	hodnocení
nezískal	získat	k5eNaPmAgMnS	získat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
televizní	televizní	k2eAgFnSc1d1	televizní
společnost	společnost	k1gFnSc1	společnost
RTL	RTL	kA	RTL
plánovala	plánovat	k5eAaImAgFnS	plánovat
natočit	natočit	k5eAaBmF	natočit
nové	nový	k2eAgInPc4d1	nový
tři	tři	k4xCgInPc4	tři
filmy	film	k1gInPc4	film
o	o	k7c6	o
Vinnetouovi	Vinnetou	k1gMnSc6	Vinnetou
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
se	se	k3xPyFc4	se
o	o	k7c6	o
Pierru	Pierr	k1gInSc6	Pierr
Briceovi	Briceus	k1gMnSc3	Briceus
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
jako	jako	k8xS	jako
o	o	k7c6	o
možném	možný	k2eAgMnSc6d1	možný
představiteli	představitel	k1gMnSc6	představitel
Inču-čuny	Inču-čuna	k1gFnSc2	Inču-čuna
<g/>
.	.	kIx.	.
</s>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Brice	Brice	k1gMnSc1	Brice
ovšem	ovšem	k9	ovšem
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
roli	role	k1gFnSc4	role
Inču-čuny	Inču-čuna	k1gFnSc2	Inču-čuna
zahrál	zahrát	k5eAaPmAgMnS	zahrát
Gojko	Gojko	k1gNnSc4	Gojko
Mitič	Mitič	k1gInSc1	Mitič
<g/>
.	.	kIx.	.
<g/>
Natočeny	natočen	k2eAgFnPc1d1	natočena
byly	být	k5eAaImAgInP	být
tři	tři	k4xCgInPc1	tři
filmy	film	k1gInPc1	film
<g/>
:	:	kIx,	:
<g/>
Vinnetou-Nový	Vinnetou-Nový	k2eAgInSc1d1	Vinnetou-Nový
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
Vinnetou-Tajemství	Vinnetou-Tajemství	k1gNnSc1	Vinnetou-Tajemství
stříbrného	stříbrný	k2eAgNnSc2d1	stříbrné
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
Vinnetou-Poslední	Vinnetou-Posledný	k2eAgMnPc1d1	Vinnetou-Posledný
Bitva	bitva	k1gFnSc1	bitva
<g/>
.	.	kIx.	.
<g/>
Celá	celý	k2eAgFnSc1d1	celá
triologie	triologie	k1gFnSc1	triologie
měla	mít	k5eAaImAgFnS	mít
název	název	k1gInSc4	název
Vinnetou	Vinneta	k1gFnSc7	Vinneta
(	(	kIx(	(
<g/>
minisérie	minisérie	k1gFnSc1	minisérie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Na	na	k7c4	na
filmy	film	k1gInPc4	film
o	o	k7c6	o
Vinnetouovi	Vinnetou	k1gMnSc6	Vinnetou
byla	být	k5eAaImAgFnS	být
natočena	natočen	k2eAgFnSc1d1	natočena
parodie	parodie	k1gFnSc1	parodie
Manitouova	Manitouův	k2eAgFnSc1d1	Manitouův
bota	bota	k1gFnSc1	bota
<g/>
.	.	kIx.	.
</s>
<s>
Vinnetou	Vinneta	k1gFnSc7	Vinneta
je	být	k5eAaImIp3nS	být
smyšlená	smyšlený	k2eAgFnSc1d1	smyšlená
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
předlohou	předloha	k1gFnSc7	předloha
však	však	k9	však
byl	být	k5eAaImAgMnS	být
skutečný	skutečný	k2eAgMnSc1d1	skutečný
apačský	apačský	k2eAgMnSc1d1	apačský
náčelník	náčelník	k1gMnSc1	náčelník
Cochise	Cochise	k1gFnSc2	Cochise
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnSc1d1	žijící
v	v	k7c6	v
letech	let	k1gInPc6	let
1805	[number]	k4	1805
<g/>
–	–	k?	–
<g/>
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
však	však	k9	však
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
i	i	k9	i
jinými	jiný	k2eAgInPc7d1	jiný
apačskými	apačský	k2eAgInPc7d1	apačský
náčelníky	náčelník	k1gInPc7	náčelník
<g/>
,	,	kIx,	,
především	především	k9	především
Geronimem	Geronimo	k1gNnSc7	Geronimo
a	a	k8xC	a
Vittoriem	Vittorium	k1gNnSc7	Vittorium
<g/>
.	.	kIx.	.
</s>
<s>
Náčelník	náčelník	k1gInSc1	náčelník
Cochise	Cochise	k1gFnSc2	Cochise
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Vinnetou	Vinneta	k1gFnSc7	Vinneta
<g/>
,	,	kIx,	,
přátelil	přátelit	k5eAaImAgMnS	přátelit
s	s	k7c7	s
bělochy	běloch	k1gMnPc7	běloch
a	a	k8xC	a
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
armádním	armádní	k2eAgMnSc7d1	armádní
stopařem	stopař	k1gMnSc7	stopař
Tomem	Tom	k1gMnSc7	Tom
Jeffordsem	Jeffords	k1gMnSc7	Jeffords
<g/>
,	,	kIx,	,
jej	on	k3xPp3gMnSc4	on
pojilo	pojit	k5eAaImAgNnS	pojit
hluboké	hluboký	k2eAgNnSc1d1	hluboké
přátelství	přátelství	k1gNnSc1	přátelství
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgInPc1d1	podobný
vztahu	vztah	k1gInSc2	vztah
Vinnetoua	Vinnetoua	k1gFnSc1	Vinnetoua
s	s	k7c7	s
Old	Olda	k1gFnPc2	Olda
Shatterhandem	Shatterhand	k1gInSc7	Shatterhand
<g/>
.	.	kIx.	.
</s>
<s>
Cochise	Cochise	k1gFnPc4	Cochise
měl	mít	k5eAaImAgInS	mít
sestru	sestra	k1gFnSc4	sestra
jménem	jméno	k1gNnSc7	jméno
Sonseeahray	Sonseeahraa	k1gMnSc2	Sonseeahraa
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Jitřenka	Jitřenka	k1gFnSc1	Jitřenka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
byl	být	k5eAaImAgInS	být
Jeffords	Jeffords	k1gInSc1	Jeffords
zamilován	zamilován	k2eAgInSc1d1	zamilován
<g/>
.	.	kIx.	.
</s>
<s>
Dívka	dívka	k1gFnSc1	dívka
však	však	k9	však
byla	být	k5eAaImAgFnS	být
záhy	záhy	k6eAd1	záhy
zavražděna	zavražděn	k2eAgFnSc1d1	zavražděna
bělochy	běloch	k1gMnPc7	běloch
(	(	kIx(	(
<g/>
tak	tak	k6eAd1	tak
jako	jako	k9	jako
literární	literární	k2eAgFnSc3d1	literární
Nšo-či	Nšo-č	k1gFnSc3	Nšo-č
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
literárního	literární	k2eAgNnSc2d1	literární
Vinnetou	Vinneta	k1gFnSc7	Vinneta
však	však	k9	však
byl	být	k5eAaImAgMnS	být
Chochise	Chochise	k1gFnPc4	Chochise
náčelníkem	náčelník	k1gInSc7	náčelník
Chirikahuú	Chirikahuú	k1gMnSc7	Chirikahuú
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
Mescalerů	Mescaler	k1gMnPc2	Mescaler
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
smrtí	smrt	k1gFnSc7	smrt
v	v	k7c6	v
rezervaci	rezervace	k1gFnSc6	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
puška	puška	k1gFnSc1	puška
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
dvouhlavňové	dvouhlavňový	k2eAgFnSc2d1	dvouhlavňová
ručnice	ručnice	k1gFnSc2	ručnice
s	s	k7c7	s
pažbou	pažba	k1gFnSc7	pažba
zdobenou	zdobený	k2eAgFnSc4d1	zdobená
stříbrem	stříbro	k1gNnSc7	stříbro
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
Vinnetou	Vinneta	k1gFnSc7	Vinneta
používal	používat	k5eAaImAgInS	používat
v	v	k7c6	v
dobrodružných	dobrodružný	k2eAgInPc6d1	dobrodružný
románech	román	k1gInPc6	román
Karla	Karel	k1gMnSc4	Karel
Maye	May	k1gMnSc4	May
<g/>
.	.	kIx.	.
</s>
<s>
Zdědil	zdědit	k5eAaPmAgMnS	zdědit
ji	on	k3xPp3gFnSc4	on
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
otci	otec	k1gMnSc6	otec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmovém	filmový	k2eAgNnSc6d1	filmové
zpracování	zpracování	k1gNnSc6	zpracování
příběhů	příběh	k1gInPc2	příběh
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
Vinnetouově	Vinnetouov	k1gInSc6	Vinnetouov
smrti	smrt	k1gFnSc2	smrt
odnášena	odnášet	k5eAaImNgFnS	odnášet
proudem	proud	k1gInSc7	proud
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
tělem	tělo	k1gNnSc7	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
Vinnetoua	Vinnetou	k1gInSc2	Vinnetou
se	se	k3xPyFc4	se
již	již	k6eAd1	již
několikrát	několikrát	k6eAd1	několikrát
představila	představit	k5eAaPmAgFnS	představit
i	i	k9	i
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
uvedlo	uvést	k5eAaPmAgNnS	uvést
Divadlo	divadlo	k1gNnSc1	divadlo
Minor	minor	k2eAgInPc2d1	minor
částečně	částečně	k6eAd1	částečně
loutkovou	loutkový	k2eAgFnSc4d1	loutková
hru	hra	k1gFnSc4	hra
Vinnetou	Vinnetý	k2eAgFnSc4d1	Vinnetý
aneb	aneb	k?	aneb
Věčná	věčné	k1gNnPc1	věčné
loviště	loviště	k1gNnSc4	loviště
for	forum	k1gNnPc2	forum
everybody	everyboda	k1gFnSc2	everyboda
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Jana	Jan	k1gMnSc2	Jan
Jirků	Jirků	k1gMnSc2	Jirků
<g/>
.	.	kIx.	.
</s>
<s>
Vinnetoua	Vinnetoua	k1gFnSc1	Vinnetoua
s	s	k7c7	s
Old	Olda	k1gFnPc2	Olda
Shatterhandem	Shatterhando	k1gNnSc7	Shatterhando
hráli	hrát	k5eAaImAgMnP	hrát
Jiří	Jiří	k1gMnPc1	Jiří
Laštovka	Laštovka	k1gMnSc1	Laštovka
a	a	k8xC	a
Filip	Filip	k1gMnSc1	Filip
Jevič	Jevič	k1gMnSc1	Jevič
<g/>
,	,	kIx,	,
Sama	sám	k3xTgFnSc1	sám
Hawkense	Hawkense	k1gFnSc1	Hawkense
Petr	Petr	k1gMnSc1	Petr
Stach	Stach	k1gMnSc1	Stach
<g/>
,	,	kIx,	,
Santera	Santer	k1gMnSc2	Santer
Ondřej	Ondřej	k1gMnSc1	Ondřej
Nosálek	nosálka	k1gFnPc2	nosálka
<g/>
,	,	kIx,	,
Nšo-či	Nšo-č	k1gFnSc6	Nšo-č
Kristýna	Kristýna	k1gFnSc1	Kristýna
Franková	Franková	k1gFnSc1	Franková
<g/>
,	,	kIx,	,
Klekí-petru	Klekíetr	k1gMnSc6	Klekí-petr
Zuzana	Zuzana	k1gFnSc1	Zuzana
Skalníková	Skalníková	k1gFnSc1	Skalníková
a	a	k8xC	a
Inču-čunu	Inču-čun	k1gMnSc3	Inču-čun
a	a	k8xC	a
Tanguu	Tangu	k1gMnSc3	Tangu
Hynek	Hynek	k1gMnSc1	Hynek
Chmelař	Chmelař	k1gMnSc1	Chmelař
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
hru	hra	k1gFnSc4	hra
s	s	k7c7	s
názvem	název	k1gInSc7	název
Vinnetou	Vinneta	k1gFnSc7	Vinneta
uvedlo	uvést	k5eAaPmAgNnS	uvést
Strašnické	strašnický	k2eAgNnSc1d1	Strašnické
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Štěpánka	Štěpánek	k1gMnSc2	Štěpánek
<g/>
,	,	kIx,	,
titulní	titulní	k2eAgFnSc4d1	titulní
roli	role	k1gFnSc4	role
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Michal	Michal	k1gMnSc1	Michal
Čeliš	Čeliš	k1gMnSc1	Čeliš
<g/>
,	,	kIx,	,
Old	Olda	k1gFnPc2	Olda
Shatterhanda	Shatterhanda	k1gFnSc1	Shatterhanda
hrál	hrát	k5eAaImAgMnS	hrát
Jiří	Jiří	k1gMnSc1	Jiří
Racek	racek	k1gMnSc1	racek
a	a	k8xC	a
Ribannu	Ribanno	k1gNnSc3	Ribanno
Malvína	Malvína	k1gFnSc1	Malvína
Pachlová	Pachlový	k2eAgFnSc1d1	Pachlový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
Městském	městský	k2eAgNnSc6d1	Městské
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
premiéru	premiéra	k1gFnSc4	premiéra
hra	hra	k1gFnSc1	hra
Vinnetou	Vinneta	k1gFnSc7	Vinneta
dramatika	dramatik	k1gMnSc2	dramatik
Petra	Petr	k1gMnSc2	Petr
Kolečka	kolečko	k1gNnSc2	kolečko
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Pavelky	Pavelka	k1gMnSc2	Pavelka
<g/>
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgFnSc4d1	titulní
roli	role	k1gFnSc4	role
hrál	hrát	k5eAaImAgMnS	hrát
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Kotek	Kotek	k1gMnSc1	Kotek
<g/>
,	,	kIx,	,
Old	Olda	k1gFnPc2	Olda
Shatterhanda	Shatterhando	k1gNnSc2	Shatterhando
Jakub	Jakub	k1gMnSc1	Jakub
Prachař	Prachař	k1gMnSc1	Prachař
<g/>
,	,	kIx,	,
Santera	Santer	k1gMnSc2	Santer
Tomáš	Tomáš	k1gMnSc1	Tomáš
Petřík	Petřík	k1gMnSc1	Petřík
<g/>
,	,	kIx,	,
Sama	sám	k3xTgFnSc1	sám
Hawkense	Hawkense	k1gFnSc1	Hawkense
Daniel	Daniel	k1gMnSc1	Daniel
Čámský	Čámský	k1gMnSc1	Čámský
<g/>
,	,	kIx,	,
Klekí-petru	Klekíetr	k1gInSc2	Klekí-petr
Marie	Maria	k1gFnSc2	Maria
Štípková	Štípková	k1gFnSc1	Štípková
<g/>
,	,	kIx,	,
Nšo-či	Nšo-č	k1gFnSc6	Nšo-č
Šárka	Šárka	k1gFnSc1	Šárka
Opršálová	Opršálový	k2eAgFnSc1d1	Opršálová
<g/>
,	,	kIx,	,
Ribannu	Ribanen	k2eAgFnSc4d1	Ribanen
Lenka	lenka	k1gFnSc4	lenka
Zahradnická	zahradnický	k2eAgFnSc1d1	zahradnická
a	a	k8xC	a
Inču-čunu	Inču-čuna	k1gFnSc4	Inču-čuna
Zdeněk	Zdeňka	k1gFnPc2	Zdeňka
Velen	velen	k2eAgMnSc1d1	velen
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgNnPc2d1	české
vydání	vydání	k1gNnSc2	vydání
díla	dílo	k1gNnSc2	dílo
Karla	Karel	k1gMnSc2	Karel
Maye	May	k1gMnSc2	May
Postavy	postava	k1gFnSc2	postava
z	z	k7c2	z
děl	dělo	k1gNnPc2	dělo
Karla	Karel	k1gMnSc2	Karel
Maye	May	k1gMnSc2	May
Klaus	Klaus	k1gMnSc1	Klaus
Farin	Farin	k1gMnSc1	Farin
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
May	May	k1gMnSc1	May
–	–	k?	–
první	první	k4xOgFnSc1	první
německá	německý	k2eAgFnSc1d1	německá
pop-hvězda	popvězda	k1gFnSc1	pop-hvězda
<g/>
,	,	kIx,	,
Arcadia	Arcadium	k1gNnPc1	Arcadium
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
May	May	k1gMnSc1	May
<g/>
:	:	kIx,	:
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
náčelník	náčelník	k1gMnSc1	náčelník
Apačů	Apač	k1gMnPc2	Apač
<g/>
,	,	kIx,	,
Olympia	Olympia	k1gFnSc1	Olympia
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
http://karel-may.majerco.net/	[url]	k?	http://karel-may.majerco.net/
Karel	Karel	k1gMnSc1	Karel
May	May	k1gMnSc1	May
-	-	kIx~	-
Mayovky	mayovka	k1gFnPc1	mayovka
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
</s>
