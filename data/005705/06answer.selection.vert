<s>
Apačský	apačský	k2eAgMnSc1d1	apačský
náčelník	náčelník	k1gMnSc1	náčelník
Vinnetou	Vinneta	k1gFnSc7	Vinneta
je	být	k5eAaImIp3nS	být
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
a	a	k8xC	a
pokrevním	pokrevní	k2eAgMnSc7d1	pokrevní
bratrem	bratr	k1gMnSc7	bratr
Old	Olda	k1gFnPc2	Olda
Shatterhandem	Shatterhand	k1gInSc7	Shatterhand
nejznámější	známý	k2eAgFnPc1d3	nejznámější
literární	literární	k2eAgFnPc1d1	literární
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
německý	německý	k2eAgMnSc1d1	německý
spisovatel	spisovatel	k1gMnSc1	spisovatel
Karl	Karl	k1gMnSc1	Karl
May	May	k1gMnSc1	May
<g/>
.	.	kIx.	.
</s>
