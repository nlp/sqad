<p>
<s>
Malý	malý	k2eAgMnSc1d1	malý
svědek	svědek	k1gMnSc1	svědek
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Natočil	natočit	k5eAaBmAgMnS	natočit
jej	on	k3xPp3gMnSc4	on
režisér	režisér	k1gMnSc1	režisér
Harold	Harold	k1gMnSc1	Harold
Becker	Becker	k1gMnSc1	Becker
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gInSc4	jeho
poslední	poslední	k2eAgInSc4d1	poslední
film	film	k1gInSc4	film
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
Lewise	Lewise	k1gFnSc1	Lewise
Colicka	Colicka	k1gFnSc1	Colicka
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
stavitele	stavitel	k1gMnSc2	stavitel
lodí	loď	k1gFnPc2	loď
Franka	Frank	k1gMnSc2	Frank
Morrisona	Morrison	k1gMnSc2	Morrison
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
nedávno	nedávno	k6eAd1	nedávno
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Susan	Susana	k1gFnPc2	Susana
(	(	kIx(	(
<g/>
Teri	Ter	k1gFnPc4	Ter
Polo	polo	k6eAd1	polo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
John	John	k1gMnSc1	John
Travolta	Travolta	k1gMnSc1	Travolta
<g/>
.	.	kIx.	.
</s>
<s>
Susan	Susan	k1gMnSc1	Susan
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
provdala	provdat	k5eAaPmAgFnS	provdat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
Ricka	Ricek	k1gMnSc4	Ricek
Barnese	Barnese	k1gFnSc1	Barnese
(	(	kIx(	(
<g/>
Vince	Vince	k?	Vince
Vaughn	Vaughn	k1gInSc1	Vaughn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
těžce	těžce	k6eAd1	těžce
nese	nést	k5eAaImIp3nS	nést
syn	syn	k1gMnSc1	syn
Franka	Frank	k1gMnSc2	Frank
a	a	k8xC	a
Susan	Susan	k1gInSc4	Susan
<g/>
,	,	kIx,	,
dvanáctiletý	dvanáctiletý	k2eAgInSc4d1	dvanáctiletý
Danny	Dann	k1gInPc4	Dann
(	(	kIx(	(
<g/>
Matt	Matt	k1gMnSc1	Matt
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Leary	Lear	k1gInPc4	Lear
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
hudby	hudba	k1gFnSc2	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
je	být	k5eAaImIp3nS	být
Mark	Mark	k1gMnSc1	Mark
Mancina	Mancina	k1gMnSc1	Mancina
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
Steve	Steve	k1gMnSc1	Steve
Buscemi	Busce	k1gFnPc7	Busce
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
připletl	připlést	k5eAaPmAgMnS	připlést
do	do	k7c2	do
rvačky	rvačka	k1gFnSc2	rvačka
v	v	k7c6	v
baru	bar	k1gInSc6	bar
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
Vincem	Vince	k1gMnSc7	Vince
Vaughnem	Vaughn	k1gMnSc7	Vaughn
<g/>
,	,	kIx,	,
scenáristou	scenárista	k1gMnSc7	scenárista
Scottem	Scott	k1gMnSc7	Scott
Rosenbergem	Rosenberg	k1gMnSc7	Rosenberg
a	a	k8xC	a
místním	místní	k2eAgMnSc7d1	místní
mužem	muž	k1gMnSc7	muž
Timothym	Timothym	k1gInSc1	Timothym
Fogertym	Fogertym	k1gInSc4	Fogertym
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
roztržku	roztržka	k1gFnSc4	roztržka
údajně	údajně	k6eAd1	údajně
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyvázl	vyváznout	k5eAaPmAgMnS	vyváznout
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
s	s	k7c7	s
roztrženým	roztržený	k2eAgInSc7d1	roztržený
obličejem	obličej	k1gInSc7	obličej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Malý	Malý	k1gMnSc1	Malý
svědek	svědek	k1gMnSc1	svědek
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
