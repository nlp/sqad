<s>
TSV	TSV	kA
(	(	kIx(
<g/>
datový	datový	k2eAgInSc4d1
formát	formát	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
TSV	TSV	kA
(	(	kIx(
<g/>
Tab-Separated	Tab-Separated	k1gMnSc1
Values	Values	k1gMnSc1
<g/>
,	,	kIx,
Tabulátorem	tabulátor	k1gInSc7
oddělené	oddělený	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednoduchý	jednoduchý	k2eAgInSc4d1
formát	formát	k1gInSc4
ukládání	ukládání	k1gNnSc2
dat	datum	k1gNnPc2
používaný	používaný	k2eAgInSc4d1
v	v	k7c6
informačních	informační	k2eAgFnPc6d1
technologiích	technologie	k1gFnPc6
k	k	k7c3
ukládání	ukládání	k1gNnSc3
a	a	k8xC
čtení	čtení	k1gNnSc3
málo	málo	k6eAd1
strukturovaných	strukturovaný	k2eAgNnPc2d1
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Příkladem	příklad	k1gInSc7
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
logy	log	k1gInPc1
programů	program	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
každá	každý	k3xTgFnSc1
řádka	řádka	k1gFnSc1
představuje	představovat	k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
záznam	záznam	k1gInSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc4
struktura	struktura	k1gFnSc1
je	být	k5eAaImIp3nS
určena	určit	k5eAaPmNgFnS
oddělovacím	oddělovací	k2eAgInSc7d1
znakem	znak	k1gInSc7
tabulátor	tabulátor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
o	o	k7c4
formu	forma	k1gFnSc4
obecnějšího	obecní	k2eAgInSc2d2
formátu	formát	k1gInSc2
DSV	DSV	kA
(	(	kIx(
<g/>
Delimiter	Delimiter	k1gMnSc1
Separated	Separated	k1gMnSc1
Values	Values	k1gMnSc1
<g/>
,	,	kIx,
Oddělovačem	oddělovač	k1gInSc7
Oddělené	oddělený	k2eAgFnSc2d1
Hodnoty	hodnota	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
TSV	TSV	kA
je	být	k5eAaImIp3nS
alternativou	alternativa	k1gFnSc7
častěji	často	k6eAd2
používaného	používaný	k2eAgInSc2d1
formátu	formát	k1gInSc2
CSV	CSV	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
široce	široko	k6eAd1
používá	používat	k5eAaImIp3nS
k	k	k7c3
výměně	výměna	k1gFnSc3
strukturovaných	strukturovaný	k2eAgFnPc2d1
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ukázka	ukázka	k1gFnSc1
</s>
<s>
Zde	zde	k6eAd1
je	být	k5eAaImIp3nS
ukázka	ukázka	k1gFnSc1
jednoduše	jednoduše	k6eAd1
strukturovaných	strukturovaný	k2eAgNnPc2d1
dat	datum	k1gNnPc2
obsahujících	obsahující	k2eAgFnPc2d1
informace	informace	k1gFnPc4
o	o	k7c6
Jménech	jméno	k1gNnPc6
a	a	k8xC
Příjmeních	příjmení	k1gNnPc6
konkrétních	konkrétní	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Jméno	jméno	k1gNnSc1
<g/>
<TAB>
Příjmení	příjmení	k1gNnPc2
</s>
<s>
František	František	k1gMnSc1
<g/>
<TAB>
Putšálek	Putšálek	k1gMnSc1
</s>
<s>
Tonda	Tonda	k1gFnSc1
<g/>
<TAB>
Hřebenka	Hřebenka	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
<g/>
<TAB>
Putšálková	Putšálková	k1gFnSc1
</s>
<s>
V	v	k7c6
reprezentaci	reprezentace	k1gFnSc6
zástupného	zástupný	k2eAgInSc2d1
<TAB>
skutečným	skutečný	k2eAgInSc7d1
znakem	znak	k1gInSc7
tabulátoru	tabulátor	k1gInSc2
pak	pak	k6eAd1
data	datum	k1gNnPc1
vypadají	vypadat	k5eAaPmIp3nP,k5eAaImIp3nP
takto	takto	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
JménoPříjmení	JménoPříjmenit	k5eAaPmIp3nS
</s>
<s>
FrantišekPutšálek	FrantišekPutšálek	k1gMnSc1
</s>
<s>
TondaHřebenka	TondaHřebenka	k1gFnSc1
</s>
<s>
MariePutšálková	MariePutšálková	k1gFnSc1
</s>
<s>
Relevantní	relevantní	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Tabulka	tabulka	k1gFnSc1
(	(	kIx(
<g/>
informace	informace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
DSV	DSV	kA
(	(	kIx(
<g/>
datový	datový	k2eAgInSc4d1
formát	formát	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
CSV	CSV	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Tab	tab	kA
Separated	Separated	k1gInSc1
Value	Value	k1gNnSc2
File	Fil	k1gFnSc2
Format	Format	k1gInSc1
<g/>
,	,	kIx,
Gnumeric	Gnumeric	k1gMnSc1
manual	manual	k1gMnSc1
</s>
<s>
Work	Work	k1gMnSc1
with	with	k1gMnSc1
tab-delimited	tab-delimited	k1gMnSc1
(	(	kIx(
<g/>
tsv	tsv	k?
<g/>
)	)	kIx)
format	format	k1gInSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Google	Google	k1gInSc1
drive	drive	k1gInSc1
help	help	k1gInSc4
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Tab-separated	Tab-separated	k1gMnSc1
values	values	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
