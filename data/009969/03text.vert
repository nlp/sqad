<p>
<s>
Armén	Armén	k1gMnSc1	Armén
Antonian	Antonian	k1gMnSc1	Antonian
je	být	k5eAaImIp3nS	být
arménský	arménský	k2eAgMnSc1d1	arménský
violoncellista	violoncellista	k1gMnSc1	violoncellista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
Jerevanskou	Jerevanský	k2eAgFnSc4d1	Jerevanská
hudební	hudební	k2eAgFnSc4d1	hudební
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
"	"	kIx"	"
<g/>
Romanos	Romanos	k1gInSc1	Romanos
Melikiana	Melikian	k1gMnSc2	Melikian
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgMnS	získat
vysokoškolský	vysokoškolský	k2eAgInSc4d1	vysokoškolský
titul	titul	k1gInSc4	titul
v	v	k7c6	v
Konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
"	"	kIx"	"
<g/>
Komitasa	Komitasa	k1gFnSc1	Komitasa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
proslulým	proslulý	k2eAgMnSc7d1	proslulý
violoncellistou	violoncellista	k1gMnSc7	violoncellista
a	a	k8xC	a
profesorem	profesor	k1gMnSc7	profesor
Geronti	geront	k1gMnPc1	geront
Talalyanem	Talalyan	k1gMnSc7	Talalyan
<g/>
,	,	kIx,	,
nejprestižnějším	prestižní	k2eAgMnSc7d3	nejprestižnější
učitele	učitel	k1gMnSc4	učitel
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
violoncello	violoncello	k1gNnSc4	violoncello
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
a	a	k8xC	a
také	také	k9	také
spolužákem	spolužák	k1gMnSc7	spolužák
Mstislava	Mstislava	k1gFnSc1	Mstislava
Rostropoviče	Rostropovič	k1gMnSc2	Rostropovič
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tituly	titul	k1gInPc4	titul
violoncellový	violoncellový	k2eAgMnSc1d1	violoncellový
sólista	sólista	k1gMnSc1	sólista
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
violoncellové	violoncellový	k2eAgFnSc2d1	violoncellová
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
klavírní	klavírní	k2eAgMnSc1d1	klavírní
sólista	sólista	k1gMnSc1	sólista
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
klavírní	klavírní	k2eAgFnSc2d1	klavírní
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
orchestrální	orchestrální	k2eAgMnSc1d1	orchestrální
umělec	umělec	k1gMnSc1	umělec
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
orchestrální	orchestrální	k2eAgFnSc2d1	orchestrální
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
Symfonického	symfonický	k2eAgInSc2d1	symfonický
orchestru	orchestr	k1gInSc2	orchestr
Radiotelevision	Radiotelevision	k1gInSc1	Radiotelevision
v	v	k7c6	v
Arménii	Arménie	k1gFnSc6	Arménie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
sólista	sólista	k1gMnSc1	sólista
do	do	k7c2	do
arménského	arménský	k2eAgInSc2d1	arménský
filharmonického	filharmonický	k2eAgInSc2d1	filharmonický
orchestru	orchestr	k1gInSc2	orchestr
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgMnSc7	který
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k8xC	i
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
hostujícího	hostující	k2eAgMnSc2d1	hostující
sólisty	sólista	k1gMnSc2	sólista
hrál	hrát	k5eAaImAgMnS	hrát
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
orchestry	orchestr	k1gInPc7	orchestr
Baltských	baltský	k2eAgInPc2d1	baltský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1987	[number]	k4	1987
až	až	k9	až
1992	[number]	k4	1992
pracoval	pracovat	k5eAaImAgInS	pracovat
na	na	k7c6	na
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
v	v	k7c6	v
Jerevanu	Jerevan	k1gMnSc6	Jerevan
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
v	v	k7c6	v
nejvýznamnějších	významný	k2eAgNnPc6d3	nejvýznamnější
městech	město	k1gNnPc6	město
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
jako	jako	k8xS	jako
sólista	sólista	k1gMnSc1	sólista
a	a	k8xC	a
také	také	k9	také
s	s	k7c7	s
Ensemble	ensemble	k1gInSc1	ensemble
de	de	k?	de
virtuosos	virtuosos	k1gMnSc1	virtuosos
violonchelistas	violonchelistas	k1gMnSc1	violonchelistas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Evropském	evropský	k2eAgInSc6d1	evropský
parlamentu	parlament	k1gInSc6	parlament
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
Arménského	arménský	k2eAgNnSc2d1	arménské
fóra	fórum	k1gNnSc2	fórum
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
propagovat	propagovat	k5eAaImF	propagovat
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
kulturu	kultura	k1gFnSc4	kultura
Arménie	Arménie	k1gFnSc2	Arménie
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
příjezdu	příjezd	k1gInSc2	příjezd
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
mnoha	mnoho	k4c2	mnoho
festivalů	festival	k1gInPc2	festival
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Clásicos	Clásicos	k1gInSc4	Clásicos
de	de	k?	de
Verano	Verana	k1gFnSc5	Verana
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
nebo	nebo	k8xC	nebo
Caprichos	Caprichos	k1gMnSc1	Caprichos
Musicales	Musicales	k1gMnSc1	Musicales
de	de	k?	de
Comillas	Comillas	k1gMnSc1	Comillas
v	v	k7c6	v
Kantábrii	Kantábrie	k1gFnSc6	Kantábrie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
s	s	k7c7	s
houslistou	houslista	k1gMnSc7	houslista
Arou	ara	k1gMnSc7	ara
Malikianem	Malikian	k1gMnSc7	Malikian
a	a	k8xC	a
violoncellistou	violoncellista	k1gMnSc7	violoncellista
Sergueiem	Sergueius	k1gMnSc7	Sergueius
Mesropianem	Mesropian	k1gMnSc7	Mesropian
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
sólista	sólista	k1gMnSc1	sólista
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
hraje	hrát	k5eAaImIp3nS	hrát
hlavně	hlavně	k9	hlavně
dueta	dueto	k1gNnSc2	dueto
pro	pro	k7c4	pro
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
konkrétněji	konkrétně	k6eAd2	konkrétně
např.	např.	kA	např.
na	na	k7c6	na
benefičním	benefiční	k2eAgInSc6d1	benefiční
koncertu	koncert	k1gInSc6	koncert
Voces	Vocesa	k1gFnPc2	Vocesa
para	para	k2eAgNnSc2d1	para
la	la	k1gNnSc2	la
Paz	Paz	k1gFnSc2	Paz
nebo	nebo	k8xC	nebo
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Festival	festival	k1gInSc1	festival
de	de	k?	de
Música	Músicus	k1gMnSc2	Músicus
de	de	k?	de
Titulcia	Titulcius	k1gMnSc2	Titulcius
(	(	kIx(	(
<g/>
obojí	obojí	k4xRgFnPc1	obojí
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
propagovat	propagovat	k5eAaImF	propagovat
arménskou	arménský	k2eAgFnSc4d1	arménská
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
některou	některý	k3yIgFnSc4	některý
nahrál	nahrát	k5eAaBmAgMnS	nahrát
s	s	k7c7	s
Radio	radio	k1gNnSc1	radio
Nacional	Nacional	k1gMnSc7	Nacional
de	de	k?	de
Españ	Españ	k1gMnSc7	Españ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Violoncello	violoncello	k1gNnSc1	violoncello
==	==	k?	==
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
violoncello	violoncello	k1gNnSc4	violoncello
mu	on	k3xPp3gMnSc3	on
věnoval	věnovat	k5eAaPmAgMnS	věnovat
ruský	ruský	k2eAgMnSc1d1	ruský
violoncellista	violoncellista	k1gMnSc1	violoncellista
Alexandr	Alexandr	k1gMnSc1	Alexandr
Vajnrot	Vajnrot	k1gMnSc1	Vajnrot
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Poslal	poslat	k5eAaPmAgMnS	poslat
mu	on	k3xPp3gMnSc3	on
ho	on	k3xPp3gInSc4	on
poštou	pošta	k1gFnSc7	pošta
do	do	k7c2	do
Jerevanu	Jerevan	k1gMnSc3	Jerevan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Antonian	Antonian	k1gMnSc1	Antonian
aktuálně	aktuálně	k6eAd1	aktuálně
pobýval	pobývat	k5eAaImAgMnS	pobývat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
cena	cena	k1gFnSc1	cena
sólisty	sólista	k1gMnSc2	sólista
<g/>
,	,	kIx,	,
Jerevanská	Jerevanský	k2eAgFnSc1d1	Jerevanská
státní	státní	k2eAgFnSc1d1	státní
konzervatoř	konzervatoř	k1gFnSc1	konzervatoř
</s>
</p>
<p>
<s>
Cena	cena	k1gFnSc1	cena
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
interpreta	interpret	k1gMnSc4	interpret
(	(	kIx(	(
<g/>
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Concierto	Concierta	k1gFnSc5	Concierta
de	de	k?	de
conmemoración	conmemoración	k1gInSc1	conmemoración
de	de	k?	de
la	la	k1gNnSc1	la
fundación	fundación	k1gMnSc1	fundación
de	de	k?	de
la	la	k1gNnPc2	la
ciudad	ciudad	k1gInSc1	ciudad
de	de	k?	de
Kiev	Kiev	k1gInSc1	Kiev
(	(	kIx(	(
<g/>
koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cena	cena	k1gFnSc1	cena
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
interpreta	interpret	k1gMnSc4	interpret
(	(	kIx(	(
<g/>
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Encuentros	Encuentrosa	k1gFnPc2	Encuentrosa
de	de	k?	de
amistad	amistad	k1gInSc1	amistad
de	de	k?	de
las	laso	k1gNnPc2	laso
Repúblicas	Repúblicas	k1gMnSc1	Repúblicas
Soviéticas	Soviéticas	k1gMnSc1	Soviéticas
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
známější	známý	k2eAgFnPc4d2	známější
nahrávky	nahrávka	k1gFnPc4	nahrávka
patří	patřit	k5eAaImIp3nS	patřit
Requiem	Requium	k1gNnSc7	Requium
od	od	k7c2	od
Davida	David	k1gMnSc2	David
Poppera	Popper	k1gMnSc2	Popper
<g/>
,	,	kIx,	,
Adagio	adagio	k6eAd1	adagio
od	od	k7c2	od
Antonia	Antonio	k1gMnSc2	Antonio
Vivaldiho	Vivaldi	k1gMnSc2	Vivaldi
<g/>
,	,	kIx,	,
Labuť	labuť	k1gFnSc1	labuť
od	od	k7c2	od
Camille	Camille	k1gFnSc2	Camille
Saint-Saënse	Saint-Saënse	k1gFnSc2	Saint-Saënse
nebo	nebo	k8xC	nebo
Šavlový	šavlový	k2eAgInSc1d1	šavlový
tanec	tanec	k1gInSc1	tanec
od	od	k7c2	od
Arama	Aram	k1gMnSc2	Aram
Chačaturjana	Chačaturjan	k1gMnSc2	Chačaturjan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
</s>
</p>
<p>
<s>
Interview	interview	k1gNnSc1	interview
</s>
</p>
<p>
<s>
Juan	Juan	k1gMnSc1	Juan
March	March	k1gMnSc1	March
</s>
</p>
