<s>
Opilost	opilost	k1gFnSc1	opilost
neboli	neboli	k8xC	neboli
opojenost	opojenost	k1gFnSc1	opojenost
či	či	k8xC	či
též	též	k9	též
ebrieta	ebrieta	k1gFnSc1	ebrieta
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
po	po	k7c6	po
intoxikaci	intoxikace	k1gFnSc6	intoxikace
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
nadnesená	nadnesený	k2eAgFnSc1d1	nadnesená
nálada	nálada	k1gFnSc1	nálada
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
zvýšené	zvýšený	k2eAgNnSc1d1	zvýšené
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
<g/>
,	,	kIx,	,
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
hovornost	hovornost	k1gFnSc1	hovornost
<g/>
,	,	kIx,	,
podnikavost	podnikavost	k1gFnSc1	podnikavost
<g/>
,	,	kIx,	,
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
družnost	družnost	k1gFnSc1	družnost
<g/>
,	,	kIx,	,
zmenšují	zmenšovat	k5eAaImIp3nP	zmenšovat
se	se	k3xPyFc4	se
sociální	sociální	k2eAgFnPc1d1	sociální
zábrany	zábrana	k1gFnPc1	zábrana
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
ale	ale	k8xC	ale
porucha	porucha	k1gFnSc1	porucha
pozornosti	pozornost	k1gFnSc2	pozornost
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
v	v	k7c6	v
opilosti	opilost	k1gFnSc6	opilost
častěji	často	k6eAd2	často
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
dopravním	dopravní	k2eAgFnPc3d1	dopravní
nehodám	nehoda	k1gFnPc3	nehoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
objevit	objevit	k5eAaPmF	objevit
i	i	k9	i
agresivní	agresivní	k2eAgNnSc4d1	agresivní
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
větší	veliký	k2eAgFnSc6d2	veliký
opilosti	opilost	k1gFnSc6	opilost
bývají	bývat	k5eAaImIp3nP	bývat
poruchy	poruch	k1gInPc1	poruch
chůze	chůze	k1gFnSc2	chůze
-	-	kIx~	-
chůze	chůze	k1gFnSc1	chůze
o	o	k7c6	o
široké	široký	k2eAgFnSc6d1	široká
bázi	báze	k1gFnSc6	báze
<g/>
,	,	kIx,	,
pády	pád	k1gInPc1	pád
<g/>
,	,	kIx,	,
zarudlé	zarudlý	k2eAgFnPc1d1	zarudlá
spojivky	spojivka	k1gFnPc1	spojivka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
intoxikaci	intoxikace	k1gFnSc6	intoxikace
alkoholem	alkohol	k1gInSc7	alkohol
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přímému	přímý	k2eAgNnSc3d1	přímé
ovlivnění	ovlivnění	k1gNnSc3	ovlivnění
neurotransmiterů	neurotransmiter	k1gInPc2	neurotransmiter
(	(	kIx(	(
<g/>
útlum	útlum	k1gInSc1	útlum
dopaminergní	dopaminergeň	k1gFnPc2	dopaminergeň
<g/>
,	,	kIx,	,
serotoninergní	serotoninergní	k2eAgFnPc4d1	serotoninergní
a	a	k8xC	a
taurinergní	taurinergní	k2eAgFnPc4d1	taurinergní
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
,	,	kIx,	,
potlačení	potlačení	k1gNnSc3	potlačení
inhibičního	inhibiční	k2eAgInSc2d1	inhibiční
vlivu	vliv	k1gInSc2	vliv
GABA	GABA	kA	GABA
<g/>
,	,	kIx,	,
stoupá	stoupat	k5eAaImIp3nS	stoupat
aktivita	aktivita	k1gFnSc1	aktivita
glutamátu	glutamát	k1gInSc2	glutamát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
mechanismus	mechanismus	k1gInSc1	mechanismus
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
tyto	tento	k3xDgFnPc4	tento
interakce	interakce	k1gFnPc1	interakce
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
pocitu	pocit	k1gInSc3	pocit
uspokojení	uspokojení	k1gNnSc2	uspokojení
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
.	.	kIx.	.
</s>
<s>
Vyvolání	vyvolání	k1gNnSc1	vyvolání
tendence	tendence	k1gFnSc2	tendence
pro	pro	k7c4	pro
opakování	opakování	k1gNnSc4	opakování
příjmu	příjem	k1gInSc2	příjem
alkoholu	alkohol	k1gInSc2	alkohol
lze	lze	k6eAd1	lze
teoreticky	teoreticky	k6eAd1	teoreticky
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
účinky	účinek	k1gInPc4	účinek
změněné	změněný	k2eAgFnSc2d1	změněná
aktivity	aktivita	k1gFnSc2	aktivita
GABAergního	GABAergní	k2eAgInSc2d1	GABAergní
a	a	k8xC	a
glutamátergního	glutamátergní	k2eAgInSc2d1	glutamátergní
systému	systém	k1gInSc2	systém
na	na	k7c4	na
uvolňování	uvolňování	k1gNnSc4	uvolňování
dopaminu	dopamin	k1gInSc2	dopamin
v	v	k7c6	v
mezolimbické	mezolimbický	k2eAgFnSc6d1	mezolimbický
dopaminové	dopaminový	k2eAgFnSc6d1	dopaminová
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
somatické	somatický	k2eAgNnSc4d1	somatické
poškození	poškození	k1gNnSc4	poškození
neuronů	neuron	k1gInPc2	neuron
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
zodpovědné	zodpovědný	k2eAgNnSc4d1	zodpovědné
hromadění	hromadění	k1gNnSc4	hromadění
metabolitů	metabolit	k1gInPc2	metabolit
alkoholu	alkohol	k1gInSc2	alkohol
-	-	kIx~	-
zejména	zejména	k9	zejména
acetaldehydu	acetaldehyd	k1gInSc2	acetaldehyd
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
dle	dle	k7c2	dle
množství	množství	k1gNnSc2	množství
alkoholu	alkohol	k1gInSc2	alkohol
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
opilost	opilost	k1gFnSc4	opilost
do	do	k7c2	do
několika	několik	k4yIc2	několik
stádií	stádium	k1gNnPc2	stádium
<g/>
:	:	kIx,	:
excitační	excitační	k2eAgFnSc1d1	excitační
opilost	opilost	k1gFnSc1	opilost
(	(	kIx(	(
<g/>
do	do	k7c2	do
0,99	[number]	k4	0,99
‰	‰	k?	‰
<g/>
)	)	kIx)	)
–	–	k?	–
subjekt	subjekt	k1gInSc1	subjekt
hovorný	hovorný	k2eAgInSc1d1	hovorný
<g/>
,	,	kIx,	,
euforie	euforie	k1gFnSc1	euforie
mírně	mírně	k6eAd1	mírně
elevována	elevován	k2eAgFnSc1d1	elevován
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
intoxikovaný	intoxikovaný	k2eAgInSc1d1	intoxikovaný
má	mít	k5eAaImIp3nS	mít
lepší	dobrý	k2eAgFnSc4d2	lepší
<g/>
,	,	kIx,	,
povznesenější	povznesený	k2eAgFnSc4d2	povznesenější
náladu	nálada	k1gFnSc4	nálada
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
však	však	k9	však
v	v	k7c6	v
hranici	hranice	k1gFnSc6	hranice
normy	norma	k1gFnSc2	norma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
0,80	[number]	k4	0,80
‰	‰	k?	‰
výše	vysoce	k6eAd2	vysoce
není	být	k5eNaImIp3nS	být
řidič	řidič	k1gMnSc1	řidič
schopen	schopen	k2eAgMnSc1d1	schopen
bezpečně	bezpečně	k6eAd1	bezpečně
řídit	řídit	k5eAaImF	řídit
vozidlo	vozidlo	k1gNnSc4	vozidlo
<g/>
.	.	kIx.	.
mírná	mírný	k2eAgFnSc1d1	mírná
opilost	opilost	k1gFnSc1	opilost
(	(	kIx(	(
<g/>
1,00	[number]	k4	1,00
<g/>
-	-	kIx~	-
<g/>
1,49	[number]	k4	1,49
‰	‰	k?	‰
<g/>
)	)	kIx)	)
–	–	k?	–
subjekt	subjekt	k1gInSc1	subjekt
hovorný	hovorný	k2eAgInSc1d1	hovorný
<g/>
,	,	kIx,	,
euforie	euforie	k1gFnSc1	euforie
<g/>
,	,	kIx,	,
místy	místo	k1gNnPc7	místo
planě	planě	k6eAd1	planě
vtipkuje	vtipkovat	k5eAaImIp3nS	vtipkovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zvýšené	zvýšený	k2eAgNnSc1d1	zvýšené
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
a	a	k8xC	a
sebedůvěra	sebedůvěra	k1gFnSc1	sebedůvěra
<g/>
.	.	kIx.	.
</s>
<s>
Psychomotorika	psychomotorika	k1gFnSc1	psychomotorika
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
alk	alk	k?	alk
<g/>
.	.	kIx.	.
intoxikací	intoxikace	k1gFnPc2	intoxikace
–	–	k?	–
reakční	reakční	k2eAgInSc4d1	reakční
čas	čas	k1gInSc4	čas
je	být	k5eAaImIp3nS	být
prodloužen	prodloužit	k5eAaPmNgMnS	prodloužit
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
1	[number]	k4	1
‰	‰	k?	‰
pak	pak	k6eAd1	pak
ataxie	ataxie	k1gFnSc2	ataxie
<g/>
,	,	kIx,	,
pohledový	pohledový	k2eAgInSc4d1	pohledový
nystagmus	nystagmus	k1gInSc4	nystagmus
(	(	kIx(	(
<g/>
mimovolní	mimovolní	k2eAgInPc4d1	mimovolní
pohyby	pohyb	k1gInPc4	pohyb
očí	oko	k1gNnPc2	oko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
střední	střední	k2eAgFnSc1d1	střední
opilost	opilost	k1gFnSc1	opilost
(	(	kIx(	(
<g/>
1,50	[number]	k4	1,50
<g/>
-	-	kIx~	-
<g/>
1,99	[number]	k4	1,99
‰	‰	k?	‰
<g/>
)	)	kIx)	)
–	–	k?	–
psychomotorika	psychomotorika	k1gFnSc1	psychomotorika
již	již	k6eAd1	již
výrazněji	výrazně	k6eAd2	výrazně
narušena	narušit	k5eAaPmNgFnS	narušit
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
přítomny	přítomen	k2eAgFnPc1d1	přítomna
poruchy	porucha	k1gFnPc1	porucha
koordinace	koordinace	k1gFnSc1	koordinace
a	a	k8xC	a
zpomalení	zpomalení	k1gNnSc1	zpomalení
tělesných	tělesný	k2eAgInPc2d1	tělesný
pohybů	pohyb	k1gInPc2	pohyb
<g/>
,	,	kIx,	,
snížena	snížen	k2eAgFnSc1d1	snížena
pozornost	pozornost	k1gFnSc1	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Afektivita	Afektivita	k1gFnSc1	Afektivita
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
přibližně	přibližně	k6eAd1	přibližně
emoční	emoční	k2eAgNnSc4d1	emoční
vyladění	vyladění	k1gNnSc4	vyladění
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
labilní	labilní	k2eAgFnSc1d1	labilní
–	–	k?	–
nálada	nálada	k1gFnSc1	nálada
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
buď	buď	k8xC	buď
zlepšit	zlepšit	k5eAaPmF	zlepšit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
zhoršit	zhoršit	k5eAaPmF	zhoršit
a	a	k8xC	a
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
celkový	celkový	k2eAgInSc1d1	celkový
útlum	útlum	k1gInSc1	útlum
(	(	kIx(	(
<g/>
možno	možno	k6eAd1	možno
až	až	k9	až
k	k	k7c3	k
usnutí	usnutí	k1gNnSc3	usnutí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
možné	možný	k2eAgNnSc4d1	možné
i	i	k8xC	i
kolísání	kolísání	k1gNnSc4	kolísání
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
krajních	krajní	k2eAgFnPc2d1	krajní
poloh	poloha	k1gFnPc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Deliberace	Deliberace	k1gFnSc1	Deliberace
jednání	jednání	k1gNnSc2	jednání
(	(	kIx(	(
<g/>
snížení	snížení	k1gNnSc1	snížení
zábran	zábrana	k1gFnPc2	zábrana
<g/>
)	)	kIx)	)
–	–	k?	–
opilecká	opilecký	k2eAgFnSc1d1	opilecká
sebevražednost	sebevražednost	k1gFnSc1	sebevražednost
<g/>
,	,	kIx,	,
agresivita	agresivita	k1gFnSc1	agresivita
<g/>
.	.	kIx.	.
těžká	těžký	k2eAgFnSc1d1	těžká
opilost	opilost	k1gFnSc1	opilost
(	(	kIx(	(
<g/>
2,00	[number]	k4	2,00
<g/>
-	-	kIx~	-
<g/>
2,99	[number]	k4	2,99
‰	‰	k?	‰
<g/>
)	)	kIx)	)
–	–	k?	–
blábolivá	blábolivý	k2eAgFnSc1d1	blábolivá
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
neschopnost	neschopnost	k1gFnSc1	neschopnost
samostatné	samostatný	k2eAgFnSc2d1	samostatná
chůze	chůze	k1gFnSc2	chůze
<g/>
,	,	kIx,	,
psychické	psychický	k2eAgFnSc2d1	psychická
poruchy	porucha	k1gFnSc2	porucha
<g/>
,	,	kIx,	,
výraznější	výrazný	k2eAgFnPc1d2	výraznější
poruchy	porucha	k1gFnPc1	porucha
chování	chování	k1gNnSc2	chování
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
výše	výše	k1gFnSc1	výše
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
opilecká	opilecký	k2eAgNnPc1d1	opilecké
suicidia	suicidium	k1gNnPc1	suicidium
<g/>
,	,	kIx,	,
agresivita	agresivita	k1gFnSc1	agresivita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvracení	zvracení	k1gNnSc1	zvracení
<g/>
.	.	kIx.	.
vážná	vážný	k2eAgFnSc1d1	vážná
otrava	otrava	k1gFnSc1	otrava
alkoholem	alkohol	k1gInSc7	alkohol
(	(	kIx(	(
<g/>
3,00	[number]	k4	3,00
<g/>
-	-	kIx~	-
<g/>
3,99	[number]	k4	3,99
‰	‰	k?	‰
<g/>
)	)	kIx)	)
–	–	k?	–
těžké	těžký	k2eAgFnSc2d1	těžká
poruchy	porucha	k1gFnSc2	porucha
vědomí	vědomí	k1gNnSc2	vědomí
(	(	kIx(	(
<g/>
stupor	stupor	k1gInSc1	stupor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obluzení	obluzení	k1gNnSc4	obluzení
<g/>
,	,	kIx,	,
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
na	na	k7c4	na
podněty	podnět	k1gInPc4	podnět
<g/>
,	,	kIx,	,
dýchání	dýchání	k1gNnSc1	dýchání
je	být	k5eAaImIp3nS	být
pomalé	pomalý	k2eAgNnSc1d1	pomalé
a	a	k8xC	a
mělké	mělký	k2eAgNnSc1d1	mělké
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
poruchy	porucha	k1gFnPc1	porucha
srdečního	srdeční	k2eAgInSc2d1	srdeční
rytmu	rytmus	k1gInSc2	rytmus
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
zástavy	zástava	k1gFnSc2	zástava
dechu	dech	k1gInSc2	dech
a	a	k8xC	a
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
riziko	riziko	k1gNnSc4	riziko
smrtelné	smrtelný	k2eAgFnSc2d1	smrtelná
otravy	otrava	k1gFnSc2	otrava
alkoholem	alkohol	k1gInSc7	alkohol
(	(	kIx(	(
<g/>
nad	nad	k7c7	nad
4,00	[number]	k4	4,00
‰	‰	k?	‰
<g/>
)	)	kIx)	)
–	–	k?	–
komatózní	komatózní	k2eAgInSc4d1	komatózní
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
letální	letální	k2eAgFnSc1d1	letální
dávka	dávka	k1gFnSc1	dávka
(	(	kIx(	(
<g/>
u	u	k7c2	u
50	[number]	k4	50
%	%	kIx~	%
pacientů	pacient	k1gMnPc2	pacient
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
‰	‰	k?	‰
alkoholu	alkohol	k1gInSc2	alkohol
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
ovšem	ovšem	k9	ovšem
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
individuální	individuální	k2eAgFnSc1d1	individuální
snášenlivost	snášenlivost	k1gFnSc1	snášenlivost
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
široká	široký	k2eAgFnSc1d1	široká
<g/>
.	.	kIx.	.
</s>
<s>
Trénovaní	trénovaný	k2eAgMnPc1d1	trénovaný
jedinci	jedinec	k1gMnPc1	jedinec
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
jedinci	jedinec	k1gMnPc1	jedinec
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
tolerance	tolerance	k1gFnSc2	tolerance
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
i	i	k8xC	i
s	s	k7c7	s
hladinou	hladina	k1gFnSc7	hladina
alkoholu	alkohol	k1gInSc2	alkohol
blížící	blížící	k2eAgFnSc7d1	blížící
se	s	k7c7	s
3	[number]	k4	3
‰	‰	k?	‰
působit	působit	k5eAaImF	působit
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
zcela	zcela	k6eAd1	zcela
nenápadně	nápadně	k6eNd1	nápadně
a	a	k8xC	a
z	z	k7c2	z
opilosti	opilost	k1gFnSc2	opilost
je	on	k3xPp3gFnPc4	on
usvědčí	usvědčit	k5eAaPmIp3nS	usvědčit
až	až	k9	až
dechová	dechový	k2eAgFnSc1d1	dechová
zkouška	zkouška	k1gFnSc1	zkouška
či	či	k8xC	či
toxikologické	toxikologický	k2eAgNnSc1d1	Toxikologické
vyšetření	vyšetření	k1gNnSc1	vyšetření
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Patická	Patický	k2eAgFnSc1d1	Patická
opilost	opilost	k1gFnSc1	opilost
(	(	kIx(	(
<g/>
patická	patický	k2eAgFnSc1d1	patický
ebrieta	ebrieta	k1gFnSc1	ebrieta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
abnormální	abnormální	k2eAgFnSc1d1	abnormální
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
požitý	požitý	k2eAgInSc4d1	požitý
alkohol	alkohol	k1gInSc4	alkohol
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
výrazné	výrazný	k2eAgFnSc2d1	výrazná
psychopatologické	psychopatologický	k2eAgFnSc2d1	psychopatologická
symptomatiky	symptomatika	k1gFnSc2	symptomatika
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
kardinální	kardinální	k2eAgFnSc6d1	kardinální
roli	role	k1gFnSc6	role
hraje	hrát	k5eAaImIp3nS	hrát
kvalitativní	kvalitativní	k2eAgFnSc1d1	kvalitativní
porucha	porucha	k1gFnSc1	porucha
vědomí	vědomí	k1gNnSc2	vědomí
pod	pod	k7c7	pod
obrazem	obraz	k1gInSc7	obraz
mrákotného	mrákotný	k2eAgInSc2d1	mrákotný
stavu	stav	k1gInSc2	stav
(	(	kIx(	(
<g/>
obnubilace	obnubilace	k1gFnSc1	obnubilace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
bludy	blud	k1gInPc1	blud
a	a	k8xC	a
halucinace	halucinace	k1gFnPc1	halucinace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
dalšího	další	k2eAgNnSc2d1	další
chorobného	chorobný	k2eAgNnSc2d1	chorobné
jednání	jednání	k1gNnSc2	jednání
s	s	k7c7	s
psychotickou	psychotický	k2eAgFnSc7d1	psychotická
motivací	motivace	k1gFnSc7	motivace
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
premorbidní	premorbidní	k2eAgFnSc7d1	premorbidní
osobností	osobnost	k1gFnSc7	osobnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
agresí	agrese	k1gFnSc7	agrese
zaměřenou	zaměřený	k2eAgFnSc7d1	zaměřená
na	na	k7c4	na
okolí	okolí	k1gNnSc4	okolí
(	(	kIx(	(
<g/>
heteroagrese	heteroagrese	k1gFnSc2	heteroagrese
<g/>
)	)	kIx)	)
i	i	k9	i
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
(	(	kIx(	(
<g/>
autoagrese	autoagrese	k1gFnSc2	autoagrese
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc1	stav
odeznívá	odeznívat	k5eAaImIp3nS	odeznívat
po	po	k7c6	po
minutách	minuta	k1gFnPc6	minuta
až	až	k8xS	až
hodinách	hodina	k1gFnPc6	hodina
<g/>
,	,	kIx,	,
končí	končit	k5eAaImIp3nS	končit
spánkem	spánek	k1gInSc7	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
stav	stav	k1gInSc4	stav
si	se	k3xPyFc3	se
postižený	postižený	k1gMnSc1	postižený
obvykle	obvykle	k6eAd1	obvykle
nepamatuje	pamatovat	k5eNaImIp3nS	pamatovat
(	(	kIx(	(
<g/>
amnézie	amnézie	k1gFnSc1	amnézie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
trestné	trestný	k2eAgFnSc2d1	trestná
činnosti	činnost	k1gFnSc2	činnost
páchané	páchaný	k2eAgFnSc2d1	páchaná
v	v	k7c6	v
patické	patický	k2eAgFnSc6d1	patický
ebrietě	ebrieta	k1gFnSc6	ebrieta
je	být	k5eAaImIp3nS	být
snížená	snížený	k2eAgFnSc1d1	snížená
trestní	trestní	k2eAgFnSc1d1	trestní
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
postiženého	postižený	k1gMnSc2	postižený
<g/>
,	,	kIx,	,
patická	patický	k2eAgFnSc1d1	patický
ebrieta	ebrieta	k1gFnSc1	ebrieta
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
důvodem	důvod	k1gInSc7	důvod
ke	k	k7c3	k
zproštění	zproštění	k1gNnSc3	zproštění
viny	vina	k1gFnSc2	vina
(	(	kIx(	(
<g/>
exkulpaci	exkulpace	k1gFnSc4	exkulpace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
