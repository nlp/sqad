<s>
Anthrax	Anthrax	k1gInSc1	Anthrax
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
též	též	k9	též
sněť	sněť	k1gFnSc1	sněť
slezinná	slezinný	k2eAgFnSc1d1	slezinná
nebo	nebo	k8xC	nebo
uhlák	uhlák	k1gInSc1	uhlák
je	být	k5eAaImIp3nS	být
onemocnění	onemocnění	k1gNnSc1	onemocnění
způsobované	způsobovaný	k2eAgNnSc1d1	způsobované
baktérií	baktérie	k1gFnSc7	baktérie
Bacillus	Bacillus	k1gMnSc1	Bacillus
anthracis	anthracis	k1gFnSc2	anthracis
<g/>
.	.	kIx.	.
</s>
<s>
Primárně	primárně	k6eAd1	primárně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
onemocnění	onemocnění	k1gNnPc4	onemocnění
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
hovězího	hovězí	k2eAgInSc2d1	hovězí
dobytka	dobytek	k1gInSc2	dobytek
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
může	moct	k5eAaImIp3nS	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
těžké	těžký	k2eAgNnSc1d1	těžké
onemocnění	onemocnění	k1gNnSc1	onemocnění
i	i	k9	i
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
častou	častý	k2eAgFnSc4d1	častá
náplň	náplň	k1gFnSc4	náplň
biologických	biologický	k2eAgFnPc2d1	biologická
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
původcem	původce	k1gMnSc7	původce
anthraxu	anthrax	k1gInSc2	anthrax
je	být	k5eAaImIp3nS	být
Bacillus	Bacillus	k1gInSc1	Bacillus
anthracis	anthracis	k1gFnSc2	anthracis
<g/>
,	,	kIx,	,
grampozitivní	grampozitivní	k2eAgFnSc1d1	grampozitivní
tyčinkovitá	tyčinkovitý	k2eAgFnSc1d1	tyčinkovitá
bakterie	bakterie	k1gFnSc1	bakterie
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
vytvářet	vytvářet	k5eAaImF	vytvářet
endospory	endospora	k1gFnPc4	endospora
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
zjištění	zjištění	k1gNnSc3	zjištění
však	však	k8xC	však
vedla	vést	k5eAaImAgFnS	vést
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1758	[number]	k4	1758
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
švédský	švédský	k2eAgMnSc1d1	švédský
vědec	vědec	k1gMnSc1	vědec
Carl	Carl	k1gMnSc1	Carl
Linné	Linné	k2eAgFnSc4d1	Linné
domněnku	domněnka	k1gFnSc4	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
furia	furia	k1gFnSc1	furia
infernalis	infernalis	k1gFnSc1	infernalis
neboli	neboli	k8xC	neboli
ďábelská	ďábelský	k2eAgFnSc1d1	ďábelská
bestie	bestie	k1gFnSc1	bestie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
nitkovitý	nitkovitý	k2eAgMnSc1d1	nitkovitý
červ	červ	k1gMnSc1	červ
se	s	k7c7	s
zakřivenými	zakřivený	k2eAgInPc7d1	zakřivený
ostny	osten	k1gInPc7	osten
po	po	k7c6	po
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
domněnka	domněnka	k1gFnSc1	domněnka
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nepotvrdila	potvrdit	k5eNaPmAgFnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Wipperfürthu	Wipperfürth	k1gInSc2	Wipperfürth
německý	německý	k2eAgMnSc1d1	německý
doktor	doktor	k1gMnSc1	doktor
Aloys	Aloysa	k1gFnPc2	Aloysa
Pollender	Pollender	k1gInSc4	Pollender
pod	pod	k7c7	pod
mikroskopem	mikroskop	k1gInSc7	mikroskop
krev	krev	k1gFnSc4	krev
krav	kráva	k1gFnPc2	kráva
zasažených	zasažený	k2eAgFnPc2d1	zasažená
snětí	sněť	k1gFnPc2	sněť
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgMnS	objevit
tak	tak	k6eAd1	tak
tyčinkovitá	tyčinkovitý	k2eAgNnPc4d1	tyčinkovitý
tělíska	tělísko	k1gNnPc4	tělísko
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
5	[number]	k4	5
až	až	k9	až
11	[number]	k4	11
mikrometrů	mikrometr	k1gInPc2	mikrometr
a	a	k8xC	a
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
asi	asi	k9	asi
0,7	[number]	k4	0,7
mikrometru	mikrometr	k1gInSc2	mikrometr
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgInPc4	první
cíleně	cíleně	k6eAd1	cíleně
objevené	objevený	k2eAgInPc4d1	objevený
mikroby	mikrob	k1gInPc4	mikrob
<g/>
.	.	kIx.	.
</s>
<s>
Spojitost	spojitost	k1gFnSc1	spojitost
tělísek	tělísko	k1gNnPc2	tělísko
s	s	k7c7	s
anthraxem	anthrax	k1gInSc7	anthrax
byla	být	k5eAaImAgFnS	být
však	však	k9	však
v	v	k7c6	v
následných	následný	k2eAgFnPc6d1	následná
diskusích	diskuse	k1gFnPc6	diskuse
částečně	částečně	k6eAd1	částečně
zpochybňována	zpochybňován	k2eAgFnSc1d1	zpochybňována
<g/>
.	.	kIx.	.
</s>
<s>
Tělíska	tělísko	k1gNnPc4	tělísko
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
pařížský	pařížský	k2eAgMnSc1d1	pařížský
lékař	lékař	k1gMnSc1	lékař
Casimir	Casimir	k1gMnSc1	Casimir
Davaine	Davain	k1gInSc5	Davain
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
nazval	nazvat	k5eAaBmAgMnS	nazvat
"	"	kIx"	"
<g/>
bacteridies	bacteridies	k1gInSc4	bacteridies
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
obecný	obecný	k2eAgInSc1d1	obecný
pojem	pojem	k1gInSc1	pojem
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
anthraxem	anthrax	k1gInSc7	anthrax
začal	začít	k5eAaPmAgInS	začít
zabývat	zabývat	k5eAaImF	zabývat
německý	německý	k2eAgMnSc1d1	německý
lékař	lékař	k1gMnSc1	lékař
Robert	Robert	k1gMnSc1	Robert
Koch	Koch	k1gMnSc1	Koch
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tehdy	tehdy	k6eAd1	tehdy
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
pruském	pruský	k2eAgNnSc6d1	pruské
Poznaňsku	Poznaňsko	k1gNnSc6	Poznaňsko
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1876	[number]	k4	1876
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
vypěstovat	vypěstovat	k5eAaPmF	vypěstovat
původce	původce	k1gMnSc4	původce
v	v	k7c6	v
moku	mok	k1gInSc6	mok
z	z	k7c2	z
volského	volský	k2eAgNnSc2d1	volské
oka	oko	k1gNnSc2	oko
a	a	k8xC	a
poté	poté	k6eAd1	poté
detailně	detailně	k6eAd1	detailně
popsal	popsat	k5eAaPmAgMnS	popsat
životní	životní	k2eAgInSc4d1	životní
cyklus	cyklus	k1gInSc4	cyklus
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výzkumu	výzkum	k1gInSc6	výzkum
použil	použít	k5eAaPmAgInS	použít
průkopnicky	průkopnicky	k6eAd1	průkopnicky
mj.	mj.	kA	mj.
mikrofotografii	mikrofotografie	k1gFnSc4	mikrofotografie
<g/>
.	.	kIx.	.
</s>
<s>
Zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mikroby	mikrob	k1gInPc4	mikrob
v	v	k7c6	v
nepříznivých	příznivý	k2eNgFnPc6d1	nepříznivá
podmínkách	podmínka	k1gFnPc6	podmínka
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
spóry	spóra	k1gFnPc4	spóra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
odolné	odolný	k2eAgFnPc1d1	odolná
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
přečkat	přečkat	k5eAaPmF	přečkat
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
nepříznivých	příznivý	k2eNgFnPc6d1	nepříznivá
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výsledky	výsledek	k1gInPc4	výsledek
Roberta	Robert	k1gMnSc4	Robert
Kocha	Koch	k1gMnSc4	Koch
navázal	navázat	k5eAaPmAgMnS	navázat
Louis	Louis	k1gMnSc1	Louis
Pasteur	Pasteur	k1gMnSc1	Pasteur
<g/>
,	,	kIx,	,
když	když	k8xS	když
náhodou	náhodou	k6eAd1	náhodou
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nákaza	nákaza	k1gFnSc1	nákaza
oslabeným	oslabený	k2eAgInSc7d1	oslabený
bacilem	bacil	k1gInSc7	bacil
chrání	chránit	k5eAaImIp3nP	chránit
kuřata	kuře	k1gNnPc1	kuře
před	před	k7c7	před
další	další	k2eAgFnSc7d1	další
nákazou	nákaza	k1gFnSc7	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Opatřil	opatřit	k5eAaPmAgMnS	opatřit
si	se	k3xPyFc3	se
od	od	k7c2	od
Kocha	Koch	k1gMnSc2	Koch
kulturu	kultura	k1gFnSc4	kultura
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
pak	pak	k6eAd1	pak
prováděl	provádět	k5eAaImAgMnS	provádět
pokusy	pokus	k1gInPc4	pokus
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
výzkumu	výzkum	k1gInSc2	výzkum
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
prokázat	prokázat	k5eAaPmF	prokázat
účinnost	účinnost	k1gFnSc4	účinnost
očkování	očkování	k1gNnSc2	očkování
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
při	při	k7c6	při
veřejném	veřejný	k2eAgInSc6d1	veřejný
pokusu	pokus	k1gInSc6	pokus
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1881	[number]	k4	1881
<g/>
.	.	kIx.	.
</s>
<s>
Anthrax	Anthrax	k1gInSc1	Anthrax
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
první	první	k4xOgFnSc7	první
chorobou	choroba	k1gFnSc7	choroba
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
které	který	k3yRgFnSc3	který
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
očkovat	očkovat	k5eAaImF	očkovat
uměle	uměle	k6eAd1	uměle
oslabenou	oslabený	k2eAgFnSc7d1	oslabená
kulturou	kultura	k1gFnSc7	kultura
původce	původce	k1gMnSc2	původce
samého	samý	k3xTgNnSc2	samý
<g/>
,	,	kIx,	,
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
chorobou	choroba	k1gFnSc7	choroba
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
které	který	k3yIgFnSc3	který
bylo	být	k5eAaImAgNnS	být
vynalezeno	vynalezen	k2eAgNnSc4d1	vynalezeno
očkování	očkování	k1gNnSc4	očkování
(	(	kIx(	(
<g/>
první	první	k4xOgFnPc1	první
byly	být	k5eAaImAgFnP	být
pravé	pravý	k2eAgFnPc1d1	pravá
neštovice	neštovice	k1gFnPc1	neštovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anthrax	Anthrax	k1gInSc1	Anthrax
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
infekční	infekční	k2eAgFnSc1d1	infekční
a	a	k8xC	a
přenáší	přenášet	k5eAaImIp3nS	přenášet
se	se	k3xPyFc4	se
alimentární	alimentární	k2eAgFnSc7d1	alimentární
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
kůží	kůže	k1gFnSc7	kůže
přes	přes	k7c4	přes
oděrky	oděrka	k1gFnPc4	oděrka
nebo	nebo	k8xC	nebo
skrze	skrze	k?	skrze
dýchací	dýchací	k2eAgFnSc4d1	dýchací
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Prevenci	prevence	k1gFnSc4	prevence
představuje	představovat	k5eAaImIp3nS	představovat
očkování	očkování	k1gNnSc1	očkování
<g/>
,	,	kIx,	,
omezování	omezování	k1gNnSc2	omezování
rozšíření	rozšíření	k1gNnSc2	rozšíření
této	tento	k3xDgFnSc2	tento
nemoci	nemoc	k1gFnSc2	nemoc
mezi	mezi	k7c7	mezi
dobytkem	dobytek	k1gInSc7	dobytek
<g/>
,	,	kIx,	,
vyhýbání	vyhýbání	k1gNnPc1	vyhýbání
se	se	k3xPyFc4	se
podezřelému	podezřelý	k2eAgInSc3d1	podezřelý
dobytku	dobytek	k1gInSc3	dobytek
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
Choroba	choroba	k1gFnSc1	choroba
se	se	k3xPyFc4	se
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
způsobem	způsob	k1gInSc7	způsob
vstupu	vstup	k1gInSc2	vstup
jejího	její	k3xOp3gMnSc4	její
původce	původce	k1gMnSc4	původce
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
může	moct	k5eAaImIp3nS	moct
rozvinout	rozvinout	k5eAaPmF	rozvinout
ve	v	k7c4	v
tři	tři	k4xCgFnPc4	tři
možné	možný	k2eAgFnPc4d1	možná
varianty	varianta	k1gFnPc4	varianta
<g/>
:	:	kIx,	:
Plicní	plicní	k2eAgInSc1d1	plicní
anthrax	anthrax	k1gInSc1	anthrax
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
úmrtností	úmrtnost	k1gFnSc7	úmrtnost
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
lékařského	lékařský	k2eAgInSc2d1	lékařský
zákroku	zákrok	k1gInSc2	zákrok
končí	končit	k5eAaImIp3nS	končit
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
lékařský	lékařský	k2eAgInSc1d1	lékařský
zásah	zásah	k1gInSc1	zásah
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
,	,	kIx,	,
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
včasnou	včasný	k2eAgFnSc4d1	včasná
diagnózu	diagnóza	k1gFnSc4	diagnóza
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
nejdříve	dříve	k6eAd3	dříve
silné	silný	k2eAgFnSc3d1	silná
chřipce	chřipka	k1gFnSc3	chřipka
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
silnému	silný	k2eAgInSc3d1	silný
zápalu	zápal	k1gInSc3	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
probíhá	probíhat	k5eAaImIp3nS	probíhat
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
silných	silný	k2eAgFnPc2d1	silná
dávek	dávka	k1gFnPc2	dávka
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Letální	letální	k2eAgFnSc4d1	letální
dávku	dávka	k1gFnSc4	dávka
představuje	představovat	k5eAaImIp3nS	představovat
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
spor	spora	k1gFnPc2	spora
<g/>
,	,	kIx,	,
inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
jedním	jeden	k4xCgInSc7	jeden
a	a	k8xC	a
několika	několik	k4yIc7	několik
dny	den	k1gInPc7	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
nemoc	nemoc	k1gFnSc1	nemoc
může	moct	k5eAaImIp3nS	moct
někdy	někdy	k6eAd1	někdy
nazývat	nazývat	k5eAaImF	nazývat
Wool-sorters	Woolorters	k1gInSc4	Wool-sorters
<g/>
'	'	kIx"	'
disease	disease	k6eAd1	disease
(	(	kIx(	(
<g/>
nemoc	nemoc	k1gFnSc1	nemoc
třídičů	třídič	k1gInPc2	třídič
vlny	vlna	k1gFnSc2	vlna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
hadrářská	hadrářský	k2eAgFnSc1d1	hadrářský
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Střevní	střevní	k2eAgInSc1d1	střevní
anthrax	anthrax	k1gInSc1	anthrax
vzniká	vznikat	k5eAaImIp3nS	vznikat
průnikem	průnik	k1gInSc7	průnik
choroby	choroba	k1gFnSc2	choroba
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
skrze	skrze	k?	skrze
trávicí	trávicí	k2eAgFnSc4d1	trávicí
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vážnou	vážný	k2eAgFnSc4d1	vážná
infekci	infekce	k1gFnSc4	infekce
žaludku	žaludek	k1gInSc2	žaludek
a	a	k8xC	a
střev	střevo	k1gNnPc2	střevo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
lékařské	lékařský	k2eAgFnSc2d1	lékařská
pomoci	pomoc	k1gFnSc2	pomoc
smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
ve	v	k7c6	v
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
krví	krvit	k5eAaImIp3nP	krvit
ve	v	k7c6	v
stolici	stolice	k1gFnSc6	stolice
a	a	k8xC	a
zvratcích	zvratek	k1gInPc6	zvratek
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
pro	pro	k7c4	pro
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
léčbu	léčba	k1gFnSc4	léčba
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
včasná	včasný	k2eAgFnSc1d1	včasná
diagnóza	diagnóza	k1gFnSc1	diagnóza
<g/>
.	.	kIx.	.
</s>
<s>
Kožní	kožní	k2eAgInSc1d1	kožní
anthrax	anthrax	k1gInSc1	anthrax
(	(	kIx(	(
<g/>
dal	dát	k5eAaPmAgInS	dát
nemoci	moct	k5eNaImF	moct
jméno	jméno	k1gNnSc4	jméno
uhlák	uhlák	k1gInSc1	uhlák
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
<g/>
-li	i	k?	-li
vstupní	vstupní	k2eAgFnSc7d1	vstupní
bránou	brána	k1gFnSc7	brána
infekce	infekce	k1gFnSc2	infekce
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
projevem	projev	k1gInSc7	projev
bývá	bývat	k5eAaImIp3nS	bývat
svědivé	svědivý	k2eAgNnSc4d1	svědivé
místo	místo	k1gNnSc4	místo
nebo	nebo	k8xC	nebo
puchýř	puchýř	k1gInSc1	puchýř
tmavší	tmavý	k2eAgFnSc2d2	tmavší
až	až	k8xS	až
černé	černý	k2eAgFnSc2d1	černá
barvy	barva	k1gFnSc2	barva
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
infekce	infekce	k1gFnSc2	infekce
(	(	kIx(	(
<g/>
inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
týdny	týden	k1gInPc4	týden
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
pustula	pustula	k1gFnSc1	pustula
maligna	maligna	k1gFnSc1	maligna
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
velké	velký	k2eAgInPc1d1	velký
<g/>
,	,	kIx,	,
nebolestivé	bolestivý	k2eNgInPc1d1	nebolestivý
a	a	k8xC	a
nekrotické	nekrotický	k2eAgInPc1d1	nekrotický
vředy	vřed	k1gInPc1	vřed
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
lékařského	lékařský	k2eAgInSc2d1	lékařský
zásahu	zásah	k1gInSc2	zásah
končí	končit	k5eAaImIp3nS	končit
nemoc	nemoc	k1gFnSc1	nemoc
ve	v	k7c6	v
20	[number]	k4	20
%	%	kIx~	%
případů	případ	k1gInPc2	případ
smrtí	smrt	k1gFnPc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
prodlevy	prodleva	k1gFnPc1	prodleva
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k9	jako
v	v	k7c6	v
předchozích	předchozí	k2eAgInPc6d1	předchozí
případech	případ	k1gInPc6	případ
-	-	kIx~	-
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Anthrax	Anthrax	k1gInSc1	Anthrax
dříve	dříve	k6eAd2	dříve
představoval	představovat	k5eAaImAgInS	představovat
obrovský	obrovský	k2eAgInSc1d1	obrovský
problém	problém	k1gInSc1	problém
z	z	k7c2	z
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
i	i	k8xC	i
lékařského	lékařský	k2eAgNnSc2d1	lékařské
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
Přinášel	přinášet	k5eAaImAgMnS	přinášet
obrovské	obrovský	k2eAgFnPc4d1	obrovská
ztráty	ztráta	k1gFnPc4	ztráta
zemědělcům	zemědělec	k1gMnPc3	zemědělec
na	na	k7c6	na
dobytku	dobytek	k1gInSc6	dobytek
a	a	k8xC	a
půdě	půda	k1gFnSc6	půda
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
prokleté	prokletý	k2eAgFnPc4d1	prokletá
louky	louka	k1gFnPc4	louka
na	na	k7c6	na
desetiletí	desetiletí	k1gNnSc6	desetiletí
zamořené	zamořený	k2eAgNnSc4d1	zamořené
anthraxem	anthrax	k1gInSc7	anthrax
<g/>
)	)	kIx)	)
a	a	k8xC	a
tisíce	tisíc	k4xCgInSc2	tisíc
úmrtí	úmrť	k1gFnPc2	úmrť
pro	pro	k7c4	pro
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
dobytkářských	dobytkářský	k2eAgFnPc2d1	dobytkářská
farem	farma	k1gFnPc2	farma
a	a	k8xC	a
jatek	jatka	k1gFnPc2	jatka
<g/>
.	.	kIx.	.
</s>
<s>
Systematickým	systematický	k2eAgInSc7d1	systematický
bojem	boj	k1gInSc7	boj
proti	proti	k7c3	proti
této	tento	k3xDgFnSc3	tento
nemoci	nemoc	k1gFnSc3	nemoc
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
podařilo	podařit	k5eAaPmAgNnS	podařit
potlačit	potlačit	k5eAaPmF	potlačit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
jako	jako	k9	jako
endemitické	endemitický	k2eAgNnSc1d1	endemitický
onemocnění	onemocnění	k1gNnSc1	onemocnění
divoce	divoce	k6eAd1	divoce
žijící	žijící	k2eAgFnSc2d1	žijící
zvěře	zvěř	k1gFnSc2	zvěř
na	na	k7c6	na
několika	několik	k4yIc6	několik
málo	málo	k6eAd1	málo
místech	místo	k1gNnPc6	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
či	či	k8xC	či
Ugandě	Uganda	k1gFnSc6	Uganda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
ale	ale	k9	ale
anthrax	anthrax	k1gInSc1	anthrax
stal	stát	k5eAaPmAgInS	stát
vděčnou	vděčný	k2eAgFnSc7d1	vděčná
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
biologických	biologický	k2eAgFnPc2d1	biologická
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Anthrax	Anthrax	k1gInSc1	Anthrax
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
biologických	biologický	k2eAgFnPc6d1	biologická
zbraních	zbraň	k1gFnPc6	zbraň
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
relativně	relativně	k6eAd1	relativně
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
kultivovatelnost	kultivovatelnost	k1gFnSc4	kultivovatelnost
a	a	k8xC	a
výdrž	výdrž	k1gFnSc4	výdrž
jeho	jeho	k3xOp3gFnPc2	jeho
spor	spora	k1gFnPc2	spora
<g/>
.	.	kIx.	.
</s>
<s>
Biologickou	biologický	k2eAgFnSc4d1	biologická
zbraň	zbraň	k1gFnSc4	zbraň
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
anthraxu	anthrax	k1gInSc2	anthrax
použila	použít	k5eAaPmAgFnS	použít
japonská	japonský	k2eAgFnSc1d1	japonská
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
Jednotka	jednotka	k1gFnSc1	jednotka
731	[number]	k4	731
<g/>
)	)	kIx)	)
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
čínsko-japonské	čínskoaponský	k2eAgFnSc2d1	čínsko-japonská
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Považuje	považovat	k5eAaImIp3nS	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
prokázané	prokázaný	k2eAgNnSc4d1	prokázané
<g/>
,	,	kIx,	,
že	že	k8xS	že
biologické	biologický	k2eAgFnPc1d1	biologická
zbraně	zbraň	k1gFnPc1	zbraň
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
anthraxu	anthrax	k1gInSc2	anthrax
hromadně	hromadně	k6eAd1	hromadně
vyráběl	vyrábět	k5eAaImAgMnS	vyrábět
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
zbraní	zbraň	k1gFnPc2	zbraň
ve	v	k7c6	v
Sverdlovsku	Sverdlovsk	k1gInSc6	Sverdlovsk
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Jekatěrinburg	Jekatěrinburg	k1gInSc1	Jekatěrinburg
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
vyráběných	vyráběný	k2eAgFnPc2d1	vyráběná
spor	spora	k1gFnPc2	spora
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
následkem	následek	k1gInSc7	následek
byla	být	k5eAaImAgFnS	být
epidemie	epidemie	k1gFnSc1	epidemie
anthraxu	anthrax	k1gInSc2	anthrax
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Případ	případ	k1gInSc1	případ
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
jako	jako	k8xS	jako
Sverdlovský	sverdlovský	k2eAgInSc1d1	sverdlovský
incident	incident	k1gInSc1	incident
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
mrtvých	mrtvý	k1gMnPc2	mrtvý
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
mezi	mezi	k7c7	mezi
60	[number]	k4	60
<g/>
-	-	kIx~	-
<g/>
600	[number]	k4	600
lidmi	člověk	k1gMnPc7	člověk
(	(	kIx(	(
<g/>
SSSR	SSSR	kA	SSSR
celou	celý	k2eAgFnSc4d1	celá
záležitost	záležitost	k1gFnSc4	záležitost
ututlal	ututlat	k5eAaPmAgMnS	ututlat
a	a	k8xC	a
zamaskoval	zamaskovat	k5eAaPmAgMnS	zamaskovat
jako	jako	k9	jako
nákazu	nákaza	k1gFnSc4	nákaza
z	z	k7c2	z
jídla	jídlo	k1gNnSc2	jídlo
<g/>
,	,	kIx,	,
materiály	materiál	k1gInPc4	materiál
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
případu	případ	k1gInSc3	případ
buď	buď	k8xC	buď
nikdy	nikdy	k6eAd1	nikdy
neexistovaly	existovat	k5eNaImAgFnP	existovat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
ztratily	ztratit	k5eAaPmAgInP	ztratit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každopádně	každopádně	k6eAd1	každopádně
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
prokázané	prokázaný	k2eAgNnSc4d1	prokázané
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
únik	únik	k1gInSc4	únik
spor	spora	k1gFnPc2	spora
z	z	k7c2	z
výrobny	výrobna	k1gFnSc2	výrobna
biologických	biologický	k2eAgFnPc2d1	biologická
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
později	pozdě	k6eAd2	pozdě
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
i	i	k9	i
výpověď	výpověď	k1gFnSc1	výpověď
Kanatžana	Kanatžan	k1gMnSc2	Kanatžan
Alibekova	Alibekov	k1gInSc2	Alibekov
<g/>
,	,	kIx,	,
osoby	osoba	k1gFnSc2	osoba
č.	č.	k?	č.
2	[number]	k4	2
ruského	ruský	k2eAgInSc2d1	ruský
biologického	biologický	k2eAgInSc2d1	biologický
zbrojního	zbrojní	k2eAgInSc2d1	zbrojní
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
spor	spora	k1gFnPc2	spora
anthraxu	anthrax	k1gInSc2	anthrax
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ruských	ruský	k2eAgInPc2d1	ruský
zbrojních	zbrojní	k2eAgInPc2d1	zbrojní
programů	program	k1gInPc2	program
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
stovek	stovka	k1gFnPc2	stovka
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
výrobu	výroba	k1gFnSc4	výroba
zbraní	zbraň	k1gFnPc2	zbraň
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
anthraxu	anthrax	k1gInSc2	anthrax
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
také	také	k9	také
Irák	Irák	k1gInSc1	Irák
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
daleko	daleko	k6eAd1	daleko
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
snahách	snaha	k1gFnPc6	snaha
postoupil	postoupit	k5eAaPmAgMnS	postoupit
.	.	kIx.	.
</s>
<s>
Anthrax	Anthrax	k1gInSc1	Anthrax
jako	jako	k8xS	jako
biologická	biologický	k2eAgFnSc1d1	biologická
zbraň	zbraň	k1gFnSc1	zbraň
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
používán	používat	k5eAaImNgInS	používat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
spor	spora	k1gFnPc2	spora
vhodných	vhodný	k2eAgFnPc2d1	vhodná
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
biologických	biologický	k2eAgFnPc2d1	biologická
zbraní	zbraň	k1gFnPc2	zbraň
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
buďto	buďto	k8xC	buďto
běžné	běžný	k2eAgInPc1d1	běžný
kmeny	kmen	k1gInPc1	kmen
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
kmeny	kmen	k1gInPc1	kmen
speciálně	speciálně	k6eAd1	speciálně
vyšlechtěné	vyšlechtěný	k2eAgInPc1d1	vyšlechtěný
či	či	k8xC	či
geneticky	geneticky	k6eAd1	geneticky
upravené	upravený	k2eAgFnPc1d1	upravená
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
druhé	druhý	k4xOgFnSc2	druhý
skupiny	skupina	k1gFnSc2	skupina
lze	lze	k6eAd1	lze
očekávat	očekávat	k5eAaImF	očekávat
neúčinnost	neúčinnost	k1gFnSc4	neúčinnost
očkování	očkování	k1gNnPc2	očkování
proti	proti	k7c3	proti
běžným	běžný	k2eAgInPc3d1	běžný
kmenům	kmen	k1gInPc3	kmen
<g/>
,	,	kIx,	,
vážnější	vážní	k2eAgInSc1d2	vážnější
průběh	průběh	k1gInSc4	průběh
onemocnění	onemocnění	k1gNnPc2	onemocnění
a	a	k8xC	a
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
antibiotikům	antibiotikum	k1gNnPc3	antibiotikum
obvykle	obvykle	k6eAd1	obvykle
nasazovaným	nasazovaný	k2eAgFnPc3d1	nasazovaná
proti	proti	k7c3	proti
anthraxu	anthrax	k1gInSc2	anthrax
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
WTC	WTC	kA	WTC
a	a	k8xC	a
Pentagon	Pentagon	k1gInSc1	Pentagon
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
USA	USA	kA	USA
použit	použít	k5eAaPmNgInS	použít
při	při	k7c6	při
rozesílání	rozesílání	k1gNnSc6	rozesílání
tzv.	tzv.	kA	tzv.
anthraxových	anthraxový	k2eAgInPc2d1	anthraxový
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
způsobily	způsobit	k5eAaPmAgFnP	způsobit
17	[number]	k4	17
infekcí	infekce	k1gFnPc2	infekce
a	a	k8xC	a
vyžádaly	vyžádat	k5eAaPmAgFnP	vyžádat
si	se	k3xPyFc3	se
5	[number]	k4	5
lidských	lidský	k2eAgMnPc2d1	lidský
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
příjemci	příjemce	k1gMnPc7	příjemce
tzv.	tzv.	kA	tzv.
anthraxových	anthraxový	k2eAgFnPc2d1	anthraxový
obálek	obálka	k1gFnPc2	obálka
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
patřili	patřit	k5eAaImAgMnP	patřit
například	například	k6eAd1	například
politici	politik	k1gMnPc1	politik
či	či	k8xC	či
novináři	novinář	k1gMnPc1	novinář
(	(	kIx(	(
<g/>
první	první	k4xOgInSc4	první
obětí	oběť	k1gFnPc2	oběť
byl	být	k5eAaImAgMnS	být
redaktor	redaktor	k1gMnSc1	redaktor
the	the	k?	the
Sun	Sun	kA	Sun
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Stevens	Stevens	k1gInSc1	Stevens
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Antraxový	Antraxový	k2eAgInSc4d1	Antraxový
poplach	poplach	k1gInSc4	poplach
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
října	říjen	k1gInSc2	říjen
2001	[number]	k4	2001
opanoval	opanovat	k5eAaPmAgInS	opanovat
i	i	k9	i
budovu	budova	k1gFnSc4	budova
Kapitolu	Kapitol	k1gInSc2	Kapitol
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
důležitého	důležitý	k2eAgNnSc2d1	důležité
hlasování	hlasování	k1gNnSc2	hlasování
o	o	k7c6	o
výnosu	výnos	k1gInSc6	výnos
PATRIOT	patriot	k1gMnSc1	patriot
Act	Act	k1gFnSc2	Act
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
budov	budova	k1gFnPc2	budova
mimo	mimo	k7c4	mimo
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
District	District	k1gMnSc1	District
of	of	k?	of
Columbia	Columbia	k1gFnSc1	Columbia
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
též	též	k9	též
případy	případ	k1gInPc4	případ
ve	v	k7c6	v
státu	stát	k1gInSc6	stát
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
a	a	k8xC	a
Florida	Florida	k1gFnSc1	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Použitý	použitý	k2eAgInSc1d1	použitý
anthrax	anthrax	k1gInSc1	anthrax
byl	být	k5eAaImAgInS	být
americké	americký	k2eAgFnPc4d1	americká
výroby	výroba	k1gFnPc4	výroba
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Aimes	Aimes	k1gInSc4	Aimes
Stray	Straa	k1gFnSc2	Straa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
vojenských	vojenský	k2eAgFnPc2d1	vojenská
laboratoří	laboratoř	k1gFnPc2	laboratoř
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
<g/>
Army	Arma	k1gMnSc2	Arma
Medical	Medical	k1gMnSc1	Medical
Research	Research	k1gMnSc1	Research
Institute	institut	k1gInSc5	institut
of	of	k?	of
Infectious	Infectious	k1gMnSc1	Infectious
Disease	Diseasa	k1gFnSc3	Diseasa
ve	v	k7c6	v
Fort	Fort	k?	Fort
Detrick	Detrick	k1gMnSc1	Detrick
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Maryland	Maryland	k1gInSc1	Maryland
<g/>
.	.	kIx.	.
</s>
<s>
Deník	deník	k1gInSc1	deník
The	The	k1gMnPc2	The
Washington	Washington	k1gInSc1	Washington
Post	posta	k1gFnPc2	posta
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
toto	tento	k3xDgNnSc4	tento
konstatoval	konstatovat	k5eAaBmAgInS	konstatovat
<g/>
,	,	kIx,	,
též	též	k9	též
dodal	dodat	k5eAaPmAgMnS	dodat
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
citovala	citovat	k5eAaBmAgFnS	citovat
z	z	k7c2	z
vládních	vládní	k2eAgInPc2d1	vládní
zdrojů	zdroj	k1gInPc2	zdroj
a	a	k8xC	a
rozvědných	rozvědný	k2eAgFnPc2d1	rozvědná
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
oněch	onen	k3xDgNnPc2	onen
60	[number]	k4	60
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
hrozeb	hrozba	k1gFnPc2	hrozba
nedokazovala	dokazovat	k5eNaImAgFnS	dokazovat
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
teroristickými	teroristický	k2eAgFnPc7d1	teroristická
organizacemi	organizace	k1gFnPc7	organizace
formátu	formát	k1gInSc2	formát
Al-Káidy	Al-Káid	k1gMnPc7	Al-Káid
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2001	[number]	k4	2001
vydala	vydat	k5eAaPmAgFnS	vydat
ABC	ABC	kA	ABC
News	News	k1gInSc1	News
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
použitý	použitý	k2eAgInSc1d1	použitý
anthrax	anthrax	k1gInSc1	anthrax
spojoval	spojovat	k5eAaImAgInS	spojovat
s	s	k7c7	s
Irákem	Irák	k1gInSc7	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
původ	původ	k1gInSc1	původ
a	a	k8xC	a
výjimečnou	výjimečný	k2eAgFnSc4d1	výjimečná
kvalitu	kvalita	k1gFnSc4	kvalita
antraxu	antrax	k1gInSc2	antrax
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
i	i	k9	i
národní	národní	k2eAgFnSc1d1	národní
laboratoř	laboratoř	k1gFnSc1	laboratoř
Sandia	Sandium	k1gNnSc2	Sandium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gMnPc1	její
vědci	vědec	k1gMnPc1	vědec
měli	mít	k5eAaImAgMnP	mít
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
tento	tento	k3xDgInSc4	tento
závěr	závěr	k1gInSc4	závěr
zveřejnit	zveřejnit	k5eAaPmF	zveřejnit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Podezřelým	podezřelý	k2eAgMnSc7d1	podezřelý
z	z	k7c2	z
útoků	útok	k1gInPc2	útok
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
stal	stát	k5eAaPmAgMnS	stát
americký	americký	k2eAgMnSc1d1	americký
mikrobiolog	mikrobiolog	k1gMnSc1	mikrobiolog
Bruce	Bruce	k1gMnSc1	Bruce
Edwards	Edwardsa	k1gFnPc2	Edwardsa
Ivins	Ivinsa	k1gFnPc2	Ivinsa
<g/>
.	.	kIx.	.
</s>
