<s>
Jako	jako	k8xC	jako
temné	temný	k2eAgNnSc1d1	temné
století	století	k1gNnSc1	století
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
saeculum	saeculum	k1gInSc1	saeculum
obscurum	obscurum	k1gInSc1	obscurum
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
etapa	etapa	k1gFnSc1	etapa
papežství	papežství	k1gNnSc2	papežství
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
vymezená	vymezený	k2eAgFnSc1d1	vymezená
pontifikáty	pontifikát	k1gInPc4	pontifikát
Sergia	Sergium	k1gNnSc2	Sergium
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
XII	XII	kA	XII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
904	[number]	k4	904
–	–	k?	–
964	[number]	k4	964
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
