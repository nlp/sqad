<p>
<s>
Vila	vila	k1gFnSc1	vila
Košťál	Košťál	k1gMnSc1	Košťál
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
staveb	stavba	k1gFnPc2	stavba
Osady	osada	k1gFnSc2	osada
Baba	baba	k1gFnSc1	baba
<g/>
.	.	kIx.	.
</s>
<s>
Nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
po	po	k7c6	po
profesorovi	profesor	k1gMnSc3	profesor
ČVUT	ČVUT	kA	ČVUT
Ing.	ing.	kA	ing.
Janu	Jan	k1gMnSc3	Jan
Košťálovi	Košťál	k1gMnSc3	Košťál
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nechal	nechat	k5eAaPmAgMnS	nechat
dům	dům	k1gInSc4	dům
vybudovat	vybudovat	k5eAaPmF	vybudovat
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
čtyřčlennou	čtyřčlenný	k2eAgFnSc4d1	čtyřčlenná
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
stál	stát	k5eAaImAgInS	stát
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
základních	základní	k2eAgInPc2d1	základní
nákresů	nákres	k1gInPc2	nákres
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gMnSc7	jeho
architektem	architekt	k1gMnSc7	architekt
byl	být	k5eAaImAgMnS	být
František	František	k1gMnSc1	František
Kerhart	Kerharta	k1gFnPc2	Kerharta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vila	vila	k1gFnSc1	vila
renovuje	renovovat	k5eAaBmIp3nS	renovovat
a	a	k8xC	a
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
dostavbě	dostavba	k1gFnSc3	dostavba
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
vily	vila	k1gFnSc2	vila
==	==	k?	==
</s>
</p>
<p>
<s>
Vila	vila	k1gFnSc1	vila
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Pražských	pražský	k2eAgFnPc6d1	Pražská
Dejvicích	Dejvice	k1gFnPc6	Dejvice
v	v	k7c6	v
Osadě	osada	k1gFnSc6	osada
Baba	baba	k1gFnSc1	baba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
ve	v	k7c6	v
funkcionalistickém	funkcionalistický	k2eAgInSc6d1	funkcionalistický
stylu	styl	k1gInSc6	styl
První	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
domů	dům	k1gInPc2	dům
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
řadě	řada	k1gFnSc6	řada
je	být	k5eAaImIp3nS	být
vstup	vstup	k1gInSc1	vstup
orientován	orientován	k2eAgInSc1d1	orientován
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
skutečnosti	skutečnost	k1gFnSc3	skutečnost
se	se	k3xPyFc4	se
druhá	druhý	k4xOgFnSc1	druhý
strana	strana	k1gFnSc1	strana
budovy	budova	k1gFnSc2	budova
(	(	kIx(	(
<g/>
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
)	)	kIx)	)
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
velikými	veliký	k2eAgNnPc7d1	veliké
okny	okno	k1gNnPc7	okno
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
je	být	k5eAaImIp3nS	být
prostor	prostor	k1gInSc4	prostor
domu	dům	k1gInSc2	dům
velice	velice	k6eAd1	velice
krásně	krásně	k6eAd1	krásně
osvětlen	osvětlit	k5eAaPmNgInS	osvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dvěma	dva	k4xCgNnPc7	dva
patry	patro	k1gNnPc7	patro
s	s	k7c7	s
velikou	veliký	k2eAgFnSc7d1	veliká
střešní	střešní	k2eAgFnSc7d1	střešní
terasou	terasa	k1gFnSc7	terasa
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
je	být	k5eAaImIp3nS	být
nádherný	nádherný	k2eAgInSc1d1	nádherný
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
celé	celý	k2eAgNnSc4d1	celé
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Kerhart	Kerhart	k1gInSc4	Kerhart
kombinoval	kombinovat	k5eAaImAgMnS	kombinovat
železobetonovou	železobetonový	k2eAgFnSc4d1	železobetonová
konstrukci	konstrukce	k1gFnSc4	konstrukce
s	s	k7c7	s
obvodovým	obvodový	k2eAgInSc7d1	obvodový
cihelným	cihelný	k2eAgInSc7d1	cihelný
nosným	nosný	k2eAgInSc7d1	nosný
zdvihem	zdvih	k1gInSc7	zdvih
o	o	k7c6	o
tloušťce	tloušťka	k1gFnSc6	tloušťka
45	[number]	k4	45
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Uvolnil	uvolnit	k5eAaPmAgMnS	uvolnit
tak	tak	k9	tak
vnitřní	vnitřní	k2eAgFnSc3d1	vnitřní
dispozici	dispozice	k1gFnSc3	dispozice
pro	pro	k7c4	pro
standardní	standardní	k2eAgNnSc4d1	standardní
funkcionalistické	funkcionalistický	k2eAgNnSc4d1	funkcionalistické
řešení	řešení	k1gNnSc4	řešení
<g/>
,	,	kIx,	,
s	s	k7c7	s
obytnými	obytný	k2eAgFnPc7d1	obytná
prostorami	prostora	k1gFnPc7	prostora
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
a	a	k8xC	a
ložnicemi	ložnice	k1gFnPc7	ložnice
v	v	k7c6	v
patře	patro	k1gNnSc6	patro
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
původnímu	původní	k2eAgInSc3d1	původní
projektu	projekt	k1gInSc3	projekt
autor	autor	k1gMnSc1	autor
zvětšil	zvětšit	k5eAaPmAgMnS	zvětšit
obytné	obytný	k2eAgInPc4d1	obytný
pokoje	pokoj	k1gInPc4	pokoj
(	(	kIx(	(
<g/>
přízemí	přízemí	k1gNnSc2	přízemí
<g/>
)	)	kIx)	)
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
předsadil	předsadit	k5eAaPmAgInS	předsadit
mohutný	mohutný	k2eAgInSc1d1	mohutný
podélný	podélný	k2eAgInSc1d1	podélný
arkýř	arkýř	k1gInSc1	arkýř
před	před	k7c4	před
konstrukci	konstrukce	k1gFnSc4	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
zároveň	zároveň	k6eAd1	zároveň
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
úzká	úzký	k2eAgFnSc1d1	úzká
balkonová	balkonový	k2eAgFnSc1d1	balkonová
terasa	terasa	k1gFnSc1	terasa
před	před	k7c7	před
ložnicemi	ložnice	k1gFnPc7	ložnice
(	(	kIx(	(
<g/>
patro	patro	k1gNnSc1	patro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
svažitosti	svažitost	k1gFnSc2	svažitost
terénu	terén	k1gInSc2	terén
se	se	k3xPyFc4	se
přízemí	přízemí	k1gNnSc1	přízemí
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
patro	patro	k1gNnSc1	patro
(	(	kIx(	(
<g/>
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stavebník	stavebník	k1gMnSc1	stavebník
Jan	Jan	k1gMnSc1	Jan
Košťál	Košťál	k1gMnSc1	Košťál
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
umístit	umístit	k5eAaPmF	umístit
garáž	garáž	k1gFnSc4	garáž
v	v	k7c6	v
suterénu	suterén	k1gInSc6	suterén
a	a	k8xC	a
právě	právě	k9	právě
díky	díky	k7c3	díky
spádu	spád	k1gInSc3	spád
terénu	terén	k1gInSc2	terén
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc4	jeho
požadavek	požadavek	k1gInSc4	požadavek
stal	stát	k5eAaPmAgInS	stát
řešitelným	řešitelný	k2eAgMnPc3d1	řešitelný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výstavba	výstavba	k1gFnSc1	výstavba
vily	vila	k1gFnSc2	vila
==	==	k?	==
</s>
</p>
<p>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
dům	dům	k1gInSc1	dům
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
až	až	k9	až
po	po	k7c6	po
výstavbě	výstavba	k1gFnSc6	výstavba
okolní	okolní	k2eAgFnSc2d1	okolní
Osady	osada	k1gFnSc2	osada
Baba	baba	k1gFnSc1	baba
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
skutečnosti	skutečnost	k1gFnSc3	skutečnost
byla	být	k5eAaImAgFnS	být
dodatečně	dodatečně	k6eAd1	dodatečně
publikována	publikovat	k5eAaBmNgFnS	publikovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
vilou	vila	k1gFnSc7	vila
č.	č.	k?	č.
30	[number]	k4	30
Jana	Jana	k1gFnSc1	Jana
Bělohrádka	Bělohrádka	k1gFnSc1	Bělohrádka
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Architektura	architektura	k1gFnSc1	architektura
a	a	k8xC	a
následně	následně	k6eAd1	následně
doprovázely	doprovázet	k5eAaImAgInP	doprovázet
některé	některý	k3yIgInPc1	některý
půdorysy	půdorys	k1gInPc1	půdorys
publikovanou	publikovaný	k2eAgFnSc4d1	publikovaná
přednášku	přednáška	k1gFnSc4	přednáška
Oldřicha	Oldřich	k1gMnSc2	Oldřich
Stefana	Stefan	k1gMnSc2	Stefan
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
výstavy	výstava	k1gFnSc2	výstava
"	"	kIx"	"
<g/>
Za	za	k7c4	za
novou	nový	k2eAgFnSc4d1	nová
architekturu	architektura	k1gFnSc4	architektura
<g/>
"	"	kIx"	"
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Osada	osada	k1gFnSc1	osada
Baba	baba	k1gFnSc1	baba
plány	plán	k1gInPc1	plán
a	a	k8xC	a
modely	model	k1gInPc1	model
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Tomáš	Tomáš	k1gMnSc1	Tomáš
Šenberger	Šenberger	k1gMnSc1	Šenberger
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šlapeta	Šlapeto	k1gNnSc2	Šlapeto
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Urlich	Urlich	k1gMnSc1	Urlich
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Slavné	slavný	k2eAgFnPc4d1	slavná
vily	vila	k1gFnPc4	vila
Prahy	Praha	k1gFnSc2	Praha
6	[number]	k4	6
-	-	kIx~	-
Osada	osada	k1gFnSc1	osada
Baba	baba	k1gFnSc1	baba
1932-1936	[number]	k4	1932-1936
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Petr	Petr	k1gMnSc1	Petr
Urlich	Urlich	k1gMnSc1	Urlich
<g/>
,	,	kIx,	,
Vlaimír	Vlaimír	k1gMnSc1	Vlaimír
Šlapeta	Šlapeta	k1gFnSc1	Šlapeta
<g/>
,	,	kIx,	,
Alena	Alena	k1gFnSc1	Alena
Křížková	Křížková	k1gFnSc1	Křížková
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
