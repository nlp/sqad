<p>
<s>
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc4	oblast
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
s	s	k7c7	s
přesně	přesně	k6eAd1	přesně
vymezenými	vymezený	k2eAgFnPc7d1	vymezená
hranicemi	hranice	k1gFnPc7	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
říká	říkat	k5eAaImIp3nS	říkat
i	i	k8xC	i
zdánlivému	zdánlivý	k2eAgInSc3d1	zdánlivý
útvaru	útvar	k1gInSc3	útvar
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
(	(	kIx(	(
<g/>
alignementu	alignement	k1gInSc6	alignement
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
spojnicemi	spojnice	k1gFnPc7	spojnice
několika	několik	k4yIc2	několik
nejjasnějších	jasný	k2eAgFnPc2d3	nejjasnější
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
v	v	k7c6	v
dávných	dávný	k2eAgFnPc6d1	dávná
dobách	doba	k1gFnPc6	doba
lidem	člověk	k1gMnPc3	člověk
připomínaly	připomínat	k5eAaImAgInP	připomínat
různé	různý	k2eAgMnPc4d1	různý
bohy	bůh	k1gMnPc4	bůh
<g/>
,	,	kIx,	,
zvířata	zvíře	k1gNnPc4	zvíře
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yRgInPc2	který
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
civilizaci	civilizace	k1gFnSc6	civilizace
byl	být	k5eAaImAgInS	být
systém	systém	k1gInSc1	systém
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
jiný	jiný	k2eAgInSc1d1	jiný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
nebi	nebe	k1gNnSc6	nebe
bylo	být	k5eAaImAgNnS	být
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
astronomickou	astronomický	k2eAgFnSc7d1	astronomická
unií	unie	k1gFnSc7	unie
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
nakonec	nakonec	k6eAd1	nakonec
ustaveno	ustavit	k5eAaPmNgNnS	ustavit
právě	právě	k9	právě
88	[number]	k4	88
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
48	[number]	k4	48
nese	nést	k5eAaImIp3nS	nést
pojmenování	pojmenování	k1gNnSc4	pojmenování
ještě	ještě	k9	ještě
z	z	k7c2	z
antických	antický	k2eAgFnPc2d1	antická
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
v	v	k7c6	v
Ptolemaiově	Ptolemaiův	k2eAgInSc6d1	Ptolemaiův
Almagestu	Almagest	k1gInSc6	Almagest
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
těchto	tento	k3xDgNnPc2	tento
48	[number]	k4	48
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
se	se	k3xPyFc4	se
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
k	k	k7c3	k
řeckým	řecký	k2eAgInPc3d1	řecký
mýtům	mýtus	k1gInPc3	mýtus
<g/>
.	.	kIx.	.
</s>
<s>
Pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
především	především	k9	především
severní	severní	k2eAgFnSc4d1	severní
nebeskou	nebeský	k2eAgFnSc4d1	nebeská
klenbu	klenba	k1gFnSc4	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
třeba	třeba	k6eAd1	třeba
Býk	býk	k1gMnSc1	býk
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
medvědice	medvědice	k1gFnSc1	medvědice
<g/>
,	,	kIx,	,
Orion	orion	k1gInSc1	orion
<g/>
,	,	kIx,	,
Andromeda	Andromeda	k1gMnSc1	Andromeda
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
civilizace	civilizace	k1gFnSc1	civilizace
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tato	tento	k3xDgNnPc1	tento
souhvězdí	souhvězdí	k1gNnPc1	souhvězdí
převzala	převzít	k5eAaPmAgNnP	převzít
z	z	k7c2	z
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
nebo	nebo	k8xC	nebo
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgNnSc1d1	tradiční
souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
té	ten	k3xDgFnSc6	ten
části	část	k1gFnSc6	část
oblohy	obloha	k1gFnSc2	obloha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
z	z	k7c2	z
35	[number]	k4	35
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
5	[number]	k4	5
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
tvary	tvar	k1gInPc1	tvar
však	však	k9	však
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
známy	znám	k2eAgInPc1d1	znám
už	už	k9	už
mnohem	mnohem	k6eAd1	mnohem
dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
obraz	obraz	k1gInSc1	obraz
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
Oriona	Oriona	k1gFnSc1	Oriona
byl	být	k5eAaImAgInS	být
nalezen	nalezen	k2eAgMnSc1d1	nalezen
i	i	k9	i
na	na	k7c4	na
kosti	kost	k1gFnPc4	kost
pocházející	pocházející	k2eAgFnPc4d1	pocházející
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
jižní	jižní	k2eAgFnSc2d1	jižní
oblohy	obloha	k1gFnSc2	obloha
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Autory	autor	k1gMnPc7	autor
jejich	jejich	k3xOp3gFnPc2	jejich
názvů	název	k1gInPc2	název
i	i	k9	i
tvarů	tvar	k1gInPc2	tvar
jsou	být	k5eAaImIp3nP	být
mořeplavci	mořeplavec	k1gMnPc1	mořeplavec
a	a	k8xC	a
vědci	vědec	k1gMnPc1	vědec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
zámořských	zámořský	k2eAgInPc2d1	zámořský
objevů	objev	k1gInPc2	objev
dostali	dostat	k5eAaPmAgMnP	dostat
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohli	moct	k5eAaImAgMnP	moct
poprvé	poprvé	k6eAd1	poprvé
tato	tento	k3xDgNnPc4	tento
souhvězdí	souhvězdí	k1gNnPc4	souhvězdí
pozorovat	pozorovat	k5eAaImF	pozorovat
a	a	k8xC	a
kteří	který	k3yQgMnPc1	který
je	on	k3xPp3gNnSc4	on
potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
k	k	k7c3	k
orientaci	orientace	k1gFnSc3	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Popis	popis	k1gInSc1	popis
jižní	jižní	k2eAgFnSc2d1	jižní
oblohy	obloha	k1gFnSc2	obloha
dokončil	dokončit	k5eAaPmAgInS	dokončit
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
francouzský	francouzský	k2eAgMnSc1d1	francouzský
astronom	astronom	k1gMnSc1	astronom
Nicolas	Nicolas	k1gMnSc1	Nicolas
Louis	Louis	k1gMnSc1	Louis
de	de	k?	de
Lacaille	Lacaille	k1gInSc1	Lacaille
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
zavedl	zavést	k5eAaPmAgInS	zavést
například	například	k6eAd1	například
souhvězdí	souhvězdí	k1gNnSc4	souhvězdí
<g/>
:	:	kIx,	:
Trojúhelník	trojúhelník	k1gInSc1	trojúhelník
<g/>
,	,	kIx,	,
Mikroskop	mikroskop	k1gInSc1	mikroskop
<g/>
,	,	kIx,	,
Vývěva	vývěva	k1gFnSc1	vývěva
a	a	k8xC	a
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozorování	pozorování	k1gNnSc2	pozorování
==	==	k?	==
</s>
</p>
<p>
<s>
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
i	i	k9	i
dnes	dnes	k6eAd1	dnes
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
při	při	k7c6	při
orientaci	orientace	k1gFnSc6	orientace
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Žijeme	žít	k5eAaImIp1nP	žít
<g/>
-li	i	k?	-li
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
můžeme	moct	k5eAaImIp1nP	moct
během	během	k7c2	během
roku	rok	k1gInSc2	rok
pozorovat	pozorovat	k5eAaImF	pozorovat
souhvězdí	souhvězdí	k1gNnSc4	souhvězdí
severní	severní	k2eAgFnSc2d1	severní
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jižní	jižní	k2eAgInSc1d1	jižní
nám	my	k3xPp1nPc3	my
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
skryta	skrýt	k5eAaPmNgFnS	skrýt
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dáno	dát	k5eAaPmNgNnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemská	zemský	k2eAgFnSc1d1	zemská
osa	osa	k1gFnSc1	osa
se	se	k3xPyFc4	se
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
téměř	téměř	k6eAd1	téměř
nepohybuje	pohybovat	k5eNaImIp3nS	pohybovat
a	a	k8xC	a
vůči	vůči	k7c3	vůči
vzdáleným	vzdálený	k2eAgFnPc3d1	vzdálená
hvězdám	hvězda	k1gFnPc3	hvězda
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
(	(	kIx(	(
<g/>
míří	mířit	k5eAaImIp3nS	mířit
stále	stále	k6eAd1	stále
k	k	k7c3	k
Polárce	Polárka	k1gFnSc3	Polárka
neboli	neboli	k8xC	neboli
Severce	Severka	k1gFnSc3	Severka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgNnPc1d1	jižní
souhvězdí	souhvězdí	k1gNnPc1	souhvězdí
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
bez	bez	k7c2	bez
ustání	ustání	k1gNnSc2	ustání
zakryta	zakrýt	k5eAaPmNgFnS	zakrýt
zemským	zemský	k2eAgNnSc7d1	zemské
tělesem	těleso	k1gNnSc7	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
kolem	kolem	k7c2	kolem
světového	světový	k2eAgInSc2d1	světový
pólu	pól	k1gInSc2	pól
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
cirkumpolární	cirkumpolární	k2eAgNnSc1d1	cirkumpolární
souhvězdí	souhvězdí	k1gNnPc1	souhvězdí
jsou	být	k5eAaImIp3nP	být
vidět	vidět	k5eAaImF	vidět
z	z	k7c2	z
daného	daný	k2eAgNnSc2d1	dané
místa	místo	k1gNnSc2	místo
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Poloměr	poloměr	k1gInSc1	poloměr
tohoto	tento	k3xDgInSc2	tento
okruhu	okruh	k1gInSc2	okruh
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xS	jako
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
šířka	šířka	k1gFnSc1	šířka
místa	místo	k1gNnSc2	místo
pozorování	pozorování	k1gNnSc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
na	na	k7c6	na
pólu	pól	k1gInSc6	pól
vidí	vidět	k5eAaImIp3nS	vidět
neustále	neustále	k6eAd1	neustále
tutéž	týž	k3xTgFnSc4	týž
část	část	k1gFnSc4	část
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
stojící	stojící	k2eAgFnSc2d1	stojící
blízko	blízko	k7c2	blízko
rovníku	rovník	k1gInSc2	rovník
by	by	kYmCp3nP	by
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
mohl	moct	k5eAaImAgInS	moct
teoreticky	teoreticky	k6eAd1	teoreticky
spatřit	spatřit	k5eAaPmF	spatřit
celé	celý	k2eAgNnSc4d1	celé
nebe	nebe	k1gNnSc4	nebe
i	i	k9	i
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Polárku	Polárka	k1gFnSc4	Polárka
by	by	kYmCp3nP	by
neustále	neustále	k6eAd1	neustále
viděl	vidět	k5eAaImAgInS	vidět
těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
obzorem	obzor	k1gInSc7	obzor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prakticky	prakticky	k6eAd1	prakticky
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nedojde	dojít	k5eNaPmIp3nS	dojít
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ve	v	k7c6	v
dne	den	k1gInSc2	den
hvězdy	hvězda	k1gFnSc2	hvězda
pozorovat	pozorovat	k5eAaImF	pozorovat
nemůžeme	moct	k5eNaImIp1nP	moct
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
jedna	jeden	k4xCgFnSc1	jeden
polovina	polovina	k1gFnSc1	polovina
nebe	nebe	k1gNnSc2	nebe
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
skryta	skrýt	k5eAaPmNgFnS	skrýt
v	v	k7c4	v
jeho	jeho	k3xOp3gFnSc4	jeho
záři	záře	k1gFnSc4	záře
<g/>
.	.	kIx.	.
</s>
<s>
Musíme	muset	k5eAaImIp1nP	muset
si	se	k3xPyFc3	se
počkat	počkat	k5eAaPmF	počkat
mnoho	mnoho	k4c4	mnoho
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
Země	země	k1gFnSc1	země
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
dráze	dráha	k1gFnSc6	dráha
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
posune	posunout	k5eAaPmIp3nS	posunout
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
otevře	otevřít	k5eAaPmIp3nS	otevřít
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
souhvězdí	souhvězdí	k1gNnSc4	souhvězdí
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
skrytá	skrytý	k2eAgFnSc1d1	skrytá
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
často	často	k6eAd1	často
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
jarní	jarní	k2eAgFnSc6d1	jarní
<g/>
,	,	kIx,	,
letní	letní	k2eAgFnSc6d1	letní
<g/>
,	,	kIx,	,
podzimní	podzimní	k2eAgFnSc6d1	podzimní
a	a	k8xC	a
zimní	zimní	k2eAgFnSc6d1	zimní
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
==	==	k?	==
</s>
</p>
<p>
<s>
Souhvězdí	souhvězdí	k1gNnPc1	souhvězdí
jsou	být	k5eAaImIp3nP	být
dělena	dělit	k5eAaImNgNnP	dělit
do	do	k7c2	do
rodin	rodina	k1gFnPc2	rodina
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Rodina	rodina	k1gFnSc1	rodina
Velké	velký	k2eAgFnSc2d1	velká
medvědice	medvědice	k1gFnSc2	medvědice
</s>
</p>
<p>
<s>
Perseova	Perseův	k2eAgFnSc1d1	Perseova
rodina	rodina	k1gFnSc1	rodina
</s>
</p>
<p>
<s>
Herkulova	Herkulův	k2eAgFnSc1d1	Herkulova
rodina	rodina	k1gFnSc1	rodina
</s>
</p>
<p>
<s>
Zvířetníková	zvířetníkový	k2eAgNnPc4d1	zvířetníkové
souhvězdí	souhvězdí	k1gNnPc4	souhvězdí
–	–	k?	–
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
ekliptice	ekliptika	k1gFnSc6	ekliptika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Orionova	Orionův	k2eAgFnSc1d1	Orionova
rodina	rodina	k1gFnSc1	rodina
</s>
</p>
<p>
<s>
Nebeské	nebeský	k2eAgFnPc1d1	nebeská
vody	voda	k1gFnPc1	voda
</s>
</p>
<p>
<s>
Bayerova	Bayerův	k2eAgFnSc1d1	Bayerova
rodina	rodina	k1gFnSc1	rodina
–	–	k?	–
poprvé	poprvé	k6eAd1	poprvé
je	být	k5eAaImIp3nS	být
popsal	popsat	k5eAaPmAgMnS	popsat
německý	německý	k2eAgMnSc1d1	německý
astronom	astronom	k1gMnSc1	astronom
Johann	Johann	k1gMnSc1	Johann
Bayer	Bayer	k1gMnSc1	Bayer
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1603	[number]	k4	1603
</s>
</p>
<p>
<s>
La	la	k0	la
Caillova	Caillův	k2eAgFnSc1d1	Caillův
rodina	rodina	k1gFnSc1	rodina
</s>
</p>
<p>
<s>
==	==	k?	==
Tvary	tvar	k1gInPc1	tvar
souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
==	==	k?	==
</s>
</p>
<p>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
hvězdy	hvězda	k1gFnPc4	hvězda
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
spojuje	spojovat	k5eAaImIp3nS	spojovat
pouze	pouze	k6eAd1	pouze
lidská	lidský	k2eAgFnSc1d1	lidská
představivost	představivost	k1gFnSc1	představivost
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vidíme	vidět	k5eAaImIp1nP	vidět
prostým	prostý	k2eAgNnSc7d1	prosté
okem	oke	k1gNnSc7	oke
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
naší	náš	k3xOp1gFnSc2	náš
Galaxie	galaxie	k1gFnSc2	galaxie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jinak	jinak	k6eAd1	jinak
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
vůbec	vůbec	k9	vůbec
nepodobají	podobat	k5eNaImIp3nP	podobat
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dělí	dělit	k5eAaImIp3nP	dělit
propastné	propastný	k2eAgInPc1d1	propastný
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
a	a	k8xC	a
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
různými	různý	k2eAgInPc7d1	různý
směry	směr	k1gInPc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Pomíjivost	pomíjivost	k1gFnSc4	pomíjivost
tvarů	tvar	k1gInPc2	tvar
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
"	"	kIx"	"
<g/>
svižná	svižný	k2eAgFnSc1d1	svižná
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
kolem	kolem	k7c2	kolem
30	[number]	k4	30
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
hvězdy	hvězda	k1gFnPc1	hvězda
od	od	k7c2	od
sebe	se	k3xPyFc2	se
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
vzdálí	vzdálit	k5eAaPmIp3nS	vzdálit
a	a	k8xC	a
obrazec	obrazec	k1gInSc1	obrazec
se	se	k3xPyFc4	se
rozpadne	rozpadnout	k5eAaPmIp3nS	rozpadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
otevřené	otevřený	k2eAgFnPc1d1	otevřená
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
a	a	k8xC	a
pohybové	pohybový	k2eAgFnPc1d1	pohybová
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
dostatečně	dostatečně	k6eAd1	dostatečně
blízko	blízko	k6eAd1	blízko
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
tvořit	tvořit	k5eAaImF	tvořit
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
i	i	k9	i
tvoří	tvořit	k5eAaImIp3nP	tvořit
převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
celého	celý	k2eAgNnSc2d1	celé
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
<g/>
.	.	kIx.	.
</s>
<s>
Hyády	Hyády	k6eAd1	Hyády
v	v	k7c6	v
Býku	býk	k1gMnSc6	býk
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
hvězd	hvězda	k1gFnPc2	hvězda
Velkého	velký	k2eAgInSc2d1	velký
vozu	vůz	k1gInSc2	vůz
takovými	takový	k3xDgFnPc7	takový
hvězdokupami	hvězdokupa	k1gFnPc7	hvězdokupa
jsou	být	k5eAaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
už	už	k9	už
označení	označení	k1gNnSc1	označení
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
neznamená	znamenat	k5eNaImIp3nS	znamenat
jen	jen	k9	jen
tvar	tvar	k1gInSc1	tvar
a	a	k8xC	a
několik	několik	k4yIc1	několik
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
tvar	tvar	k1gInSc4	tvar
spojený	spojený	k2eAgInSc4d1	spojený
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
jedno	jeden	k4xCgNnSc1	jeden
souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
má	mít	k5eAaImIp3nS	mít
přesné	přesný	k2eAgFnPc4d1	přesná
hranice	hranice	k1gFnPc4	hranice
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jakýmsi	jakýsi	k3yIgNnSc7	jakýsi
územím	území	k1gNnSc7	území
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
spadá	spadat	k5eAaImIp3nS	spadat
všechno	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
území	území	k1gNnSc6	území
můžeme	moct	k5eAaImIp1nP	moct
v	v	k7c6	v
dalekém	daleký	k2eAgInSc6d1	daleký
vesmíru	vesmír	k1gInSc6	vesmír
pozorovat	pozorovat	k5eAaImF	pozorovat
(	(	kIx(	(
<g/>
tělesa	těleso	k1gNnPc1	těleso
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
se	se	k3xPyFc4	se
při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
mohou	moct	k5eAaImIp3nP	moct
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
promítat	promítat	k5eAaImF	promítat
do	do	k7c2	do
různých	různý	k2eAgNnPc2d1	různé
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
o	o	k7c6	o
souhvězdích	souhvězdí	k1gNnPc6	souhvězdí
==	==	k?	==
</s>
</p>
<p>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
souhvězdím	souhvězdí	k1gNnSc7	souhvězdí
celé	celý	k2eAgFnSc2d1	celá
oblohy	obloha	k1gFnSc2	obloha
bývala	bývat	k5eAaImAgNnP	bývat
loď	loď	k1gFnSc4	loď
Argo	Argo	k6eAd1	Argo
(	(	kIx(	(
<g/>
legendárních	legendární	k2eAgMnPc2d1	legendární
Argonautů	argonaut	k1gMnPc2	argonaut
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
veliké	veliký	k2eAgNnSc1d1	veliké
souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
Lodní	lodní	k2eAgInSc4d1	lodní
kýl	kýl	k1gInSc4	kýl
<g/>
,	,	kIx,	,
Lodní	lodní	k2eAgFnSc4d1	lodní
záď	záď	k1gFnSc4	záď
<g/>
,	,	kIx,	,
Kompas	kompas	k1gInSc4	kompas
<g/>
,	,	kIx,	,
Holubici	holubice	k1gFnSc4	holubice
<g/>
,	,	kIx,	,
Létající	létající	k2eAgFnSc4d1	létající
rybu	ryba	k1gFnSc4	ryba
a	a	k8xC	a
Plachty	plachta	k1gFnPc4	plachta
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
souhvězdím	souhvězdí	k1gNnSc7	souhvězdí
Hydra	hydra	k1gFnSc1	hydra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jen	jen	k9	jen
jedno	jeden	k4xCgNnSc1	jeden
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
je	být	k5eAaImIp3nS	být
půlené	půlený	k2eAgNnSc1d1	půlené
<g/>
.	.	kIx.	.
</s>
<s>
Had	had	k1gMnSc1	had
byl	být	k5eAaImAgMnS	být
dříve	dříve	k6eAd2	dříve
součástí	součást	k1gFnSc7	součást
Hadonoše	hadonoš	k1gMnSc2	hadonoš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
oddělení	oddělení	k1gNnSc6	oddělení
Hadonoš	hadonoš	k1gMnSc1	hadonoš
Hada	had	k1gMnSc4	had
přeťal	přetít	k5eAaPmAgMnS	přetít
a	a	k8xC	a
Had	had	k1gMnSc1	had
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
dvoudílným	dvoudílný	k2eAgNnSc7d1	dvoudílné
souhvězdím	souhvězdí	k1gNnSc7	souhvězdí
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
jeho	jeho	k3xOp3gFnPc1	jeho
části	část	k1gFnPc1	část
můžeme	moct	k5eAaImIp1nP	moct
rozlišit	rozlišit	k5eAaPmF	rozlišit
jako	jako	k9	jako
Hlavu	hlava	k1gFnSc4	hlava
hada	had	k1gMnSc2	had
a	a	k8xC	a
Ocas	ocas	k1gInSc4	ocas
hada	had	k1gMnSc2	had
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
ale	ale	k9	ale
o	o	k7c4	o
jediné	jediný	k2eAgNnSc4d1	jediné
souhvězdí	souhvězdí	k1gNnSc4	souhvězdí
Hada	had	k1gMnSc2	had
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
vůz	vůz	k1gInSc1	vůz
není	být	k5eNaImIp3nS	být
souhvězdí	souhvězdí	k1gNnSc4	souhvězdí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tzv.	tzv.	kA	tzv.
asterismus	asterismus	k1gInSc1	asterismus
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
skupinu	skupina	k1gFnSc4	skupina
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
souhvězdí	souhvězdí	k1gNnSc3	souhvězdí
Velké	velký	k2eAgFnSc2d1	velká
medvědice	medvědice	k1gFnSc2	medvědice
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
vůz	vůz	k1gInSc1	vůz
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
česky	česky	k6eAd1	česky
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Malý	Malý	k1gMnSc1	Malý
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
časových	časový	k2eAgInPc2d1	časový
úseků	úsek	k1gInPc2	úsek
svoji	svůj	k3xOyFgFnSc4	svůj
podobu	podoba	k1gFnSc4	podoba
a	a	k8xC	a
například	například	k6eAd1	například
v	v	k7c6	v
době	doba	k1gFnSc6	doba
druhohor	druhohory	k1gFnPc2	druhohory
bychom	by	kYmCp1nP	by
žádný	žádný	k3yNgInSc4	žádný
ze	z	k7c2	z
současných	současný	k2eAgInPc2d1	současný
obrazců	obrazec	k1gInPc2	obrazec
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
nenašli	najít	k5eNaPmAgMnP	najít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
/	/	kIx~	/
A.	A.	kA	A.
Rükl	Rükl	k1gMnSc1	Rükl
<g/>
.	.	kIx.	.
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
-	-	kIx~	-
223	[number]	k4	223
str	str	kA	str
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
</s>
</p>
<p>
<s>
Stellarium	Stellarium	k1gNnSc1	Stellarium
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
