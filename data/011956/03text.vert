<p>
<s>
Luminofor	luminofor	k1gInSc1	luminofor
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
schopná	schopný	k2eAgFnSc1d1	schopná
pohlcovat	pohlcovat	k5eAaImF	pohlcovat
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
následně	následně	k6eAd1	následně
ji	on	k3xPp3gFnSc4	on
vyzařovat	vyzařovat	k5eAaImF	vyzařovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
luminiscence	luminiscence	k1gFnSc2	luminiscence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Luminoforem	luminofor	k1gInSc7	luminofor
je	být	k5eAaImIp3nS	být
pokrytý	pokrytý	k2eAgInSc4d1	pokrytý
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
povrch	povrch	k1gInSc4	povrch
všech	všecek	k3xTgFnPc2	všecek
zářivek	zářivka	k1gFnPc2	zářivka
a	a	k8xC	a
většiny	většina	k1gFnSc2	většina
výbojek	výbojka	k1gFnPc2	výbojka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
mění	měnit	k5eAaImIp3nS	měnit
energii	energie	k1gFnSc4	energie
elektrického	elektrický	k2eAgInSc2d1	elektrický
výboje	výboj	k1gInSc2	výboj
na	na	k7c4	na
viditelné	viditelný	k2eAgNnSc4d1	viditelné
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
složení	složení	k1gNnSc1	složení
luminoforu	luminofor	k1gInSc2	luminofor
určuje	určovat	k5eAaImIp3nS	určovat
barvu	barva	k1gFnSc4	barva
světla	světlo	k1gNnSc2	světlo
u	u	k7c2	u
zářivek	zářivka	k1gFnPc2	zářivka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
se	se	k3xPyFc4	se
využíval	využívat	k5eAaImAgInS	využívat
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
obrazu	obraz	k1gInSc2	obraz
v	v	k7c6	v
monitorech	monitor	k1gInPc6	monitor
počítačů	počítač	k1gMnPc2	počítač
nebo	nebo	k8xC	nebo
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
starších	starý	k2eAgNnPc6d2	starší
zařízeních	zařízení	k1gNnPc6	zařízení
vybavených	vybavený	k2eAgInPc2d1	vybavený
složitější	složitý	k2eAgFnSc7d2	složitější
zobrazovací	zobrazovací	k2eAgFnSc7d1	zobrazovací
jednotkou	jednotka	k1gFnSc7	jednotka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
televizní	televizní	k2eAgFnSc1d1	televizní
obrazovka	obrazovka	k1gFnSc1	obrazovka
CRT	CRT	kA	CRT
<g/>
,	,	kIx,	,
osciloskop	osciloskop	k1gInSc1	osciloskop
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
luminofory	luminofor	k1gInPc1	luminofor
mohou	moct	k5eAaImIp3nP	moct
zářit	zářit	k5eAaImF	zářit
každý	každý	k3xTgInSc4	každý
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
barvě	barva	k1gFnSc6	barva
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnPc4d3	veliký
využití	využití	k1gNnPc4	využití
našel	najít	k5eAaPmAgInS	najít
žlutozelený	žlutozelený	k2eAgInSc1d1	žlutozelený
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
vysokou	vysoký	k2eAgFnSc4d1	vysoká
svítivost	svítivost	k1gFnSc4	svítivost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Recyklace	recyklace	k1gFnSc2	recyklace
==	==	k?	==
</s>
</p>
<p>
<s>
Součástí	součást	k1gFnSc7	součást
recyklace	recyklace	k1gFnSc2	recyklace
počítačových	počítačový	k2eAgFnPc2d1	počítačová
a	a	k8xC	a
televizních	televizní	k2eAgFnPc2d1	televizní
obrazovek	obrazovka	k1gFnPc2	obrazovka
je	být	k5eAaImIp3nS	být
i	i	k9	i
recyklace	recyklace	k1gFnSc1	recyklace
luminoforů	luminofor	k1gInPc2	luminofor
<g/>
.	.	kIx.	.
</s>
<s>
Luminofory	luminofor	k1gInPc1	luminofor
jsou	být	k5eAaImIp3nP	být
cenné	cenný	k2eAgInPc1d1	cenný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
kovy	kov	k1gInPc1	kov
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
toxické	toxický	k2eAgFnPc4d1	toxická
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
je	on	k3xPp3gInPc4	on
odstranit	odstranit	k5eAaPmF	odstranit
především	především	k9	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
při	při	k7c6	při
další	další	k2eAgFnSc6d1	další
recyklaci	recyklace	k1gFnSc6	recyklace
skleněných	skleněný	k2eAgFnPc2d1	skleněná
obrazovek	obrazovka	k1gFnPc2	obrazovka
neměnily	měnit	k5eNaImAgInP	měnit
nežádoucím	žádoucí	k2eNgInSc7d1	nežádoucí
způsobem	způsob	k1gInSc7	způsob
optické	optický	k2eAgFnPc4d1	optická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Luminofor	luminofor	k1gInSc1	luminofor
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
oddělit	oddělit	k5eAaPmF	oddělit
z	z	k7c2	z
rozřezané	rozřezaný	k2eAgFnSc2d1	rozřezaná
obrazovky	obrazovka	k1gFnSc2	obrazovka
odsátím	odsátí	k1gNnSc7	odsátí
<g/>
,	,	kIx,	,
kartáčováním	kartáčování	k1gNnSc7	kartáčování
<g/>
,	,	kIx,	,
pískováním	pískování	k1gNnSc7	pískování
nebo	nebo	k8xC	nebo
ostříkáním	ostříkání	k1gNnSc7	ostříkání
tlakovou	tlakový	k2eAgFnSc7d1	tlaková
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
klasické	klasický	k2eAgFnSc6d1	klasická
recyklaci	recyklace	k1gFnSc6	recyklace
obrazovek	obrazovka	k1gFnPc2	obrazovka
jejich	jejich	k3xOp3gNnSc7	jejich
rozdrcením	rozdrcení	k1gNnSc7	rozdrcení
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
vibrační	vibrační	k2eAgNnSc4d1	vibrační
síto	síto	k1gNnSc4	síto
a	a	k8xC	a
oplachování	oplachování	k1gNnSc1	oplachování
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
skelný	skelný	k2eAgInSc1d1	skelný
prach	prach	k1gInSc1	prach
se	se	k3xPyFc4	se
od	od	k7c2	od
luminoforů	luminofor	k1gInPc2	luminofor
částečně	částečně	k6eAd1	částečně
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
i	i	k9	i
flotací	flotace	k1gFnSc7	flotace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
oddělených	oddělený	k2eAgInPc2d1	oddělený
kalů	kal	k1gInPc2	kal
luminoforu	luminofor	k1gInSc2	luminofor
lze	lze	k6eAd1	lze
kapalinovou	kapalinový	k2eAgFnSc7d1	kapalinová
extrakcí	extrakce	k1gFnSc7	extrakce
separovat	separovat	k5eAaBmF	separovat
yttrium	yttrium	k1gNnSc4	yttrium
a	a	k8xC	a
europium	europium	k1gNnSc4	europium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
