<s>
Luminofory	luminofor	k1gInPc1	luminofor
jsou	být	k5eAaImIp3nP	být
cenné	cenný	k2eAgInPc1d1	cenný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
kovy	kov	k1gInPc1	kov
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
toxické	toxický	k2eAgFnPc4d1	toxická
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
