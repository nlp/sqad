<s>
Nadváha	nadváha	k1gFnSc1	nadváha
je	být	k5eAaImIp3nS	být
předstupeň	předstupeň	k1gInSc4	předstupeň
obezity	obezita	k1gFnSc2	obezita
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
první	první	k4xOgNnSc1	první
stádium	stádium	k1gNnSc1	stádium
obezity	obezita	k1gFnSc2	obezita
<g/>
.	.	kIx.	.
</s>
