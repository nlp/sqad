<s>
Nadváha	nadváha	k1gFnSc1	nadváha
je	být	k5eAaImIp3nS	být
předstupeň	předstupeň	k1gInSc4	předstupeň
obezity	obezita	k1gFnSc2	obezita
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
první	první	k4xOgNnSc1	první
stádium	stádium	k1gNnSc1	stádium
obezity	obezita	k1gFnSc2	obezita
<g/>
.	.	kIx.	.
</s>
<s>
Přináší	přinášet	k5eAaImIp3nS	přinášet
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
riziko	riziko	k1gNnSc4	riziko
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
komplikací	komplikace	k1gFnPc2	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
evropskou	evropský	k2eAgFnSc4d1	Evropská
populaci	populace	k1gFnSc4	populace
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
kritérií	kritérion	k1gNnPc2	kritérion
Světové	světový	k2eAgFnSc2d1	světová
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
definována	definovat	k5eAaBmNgFnS	definovat
indexem	index	k1gInSc7	index
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
(	(	kIx(	(
<g/>
body-mass	bodyass	k6eAd1	body-mass
index	index	k1gInSc1	index
<g/>
,	,	kIx,	,
BMI	BMI	kA	BMI
<g/>
)	)	kIx)	)
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
25	[number]	k4	25
až	až	k9	až
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
asijskou	asijský	k2eAgFnSc4d1	asijská
a	a	k8xC	a
pacifickou	pacifický	k2eAgFnSc4d1	Pacifická
populaci	populace	k1gFnSc4	populace
je	být	k5eAaImIp3nS	být
nadváha	nadváha	k1gFnSc1	nadváha
definována	definován	k2eAgFnSc1d1	definována
rozmezím	rozmezit	k5eAaPmIp1nS	rozmezit
BMI	BMI	kA	BMI
23	[number]	k4	23
až	až	k9	až
25	[number]	k4	25
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
23	[number]	k4	23
až	až	k9	až
27	[number]	k4	27
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
BMI	BMI	kA	BMI
nad	nad	k7c7	nad
30	[number]	k4	30
u	u	k7c2	u
bělošské	bělošský	k2eAgFnSc2d1	bělošská
evropské	evropský	k2eAgFnSc2d1	Evropská
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
obezita	obezita	k1gFnSc1	obezita
<g/>
.	.	kIx.	.
</s>
<s>
Orientačním	orientační	k2eAgInSc7d1	orientační
ukazatelem	ukazatel	k1gInSc7	ukazatel
nadváhy	nadváha	k1gFnSc2	nadváha
je	být	k5eAaImIp3nS	být
také	také	k9	také
obvod	obvod	k1gInSc1	obvod
pasu	pas	k1gInSc2	pas
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obvodu	obvod	k1gInSc6	obvod
nad	nad	k7c7	nad
94	[number]	k4	94
cm	cm	kA	cm
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
nad	nad	k7c7	nad
80	[number]	k4	80
cm	cm	kA	cm
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nadváhu	nadváha	k1gFnSc4	nadváha
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
obezitu	obezita	k1gFnSc4	obezita
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
orientační	orientační	k2eAgInSc1d1	orientační
obvod	obvod	k1gInSc1	obvod
pasu	pást	k5eAaImIp1nS	pást
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
102	[number]	k4	102
cm	cm	kA	cm
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
88	[number]	k4	88
cm	cm	kA	cm
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
další	další	k2eAgInPc1d1	další
ukazatele	ukazatel	k1gInPc1	ukazatel
(	(	kIx(	(
<g/>
poměr	poměr	k1gInSc1	poměr
obvodu	obvod	k1gInSc2	obvod
pasu	pást	k5eAaImIp1nS	pást
k	k	k7c3	k
obvodu	obvod	k1gInSc3	obvod
hýždí	hýždě	k1gFnPc2	hýždě
<g/>
,	,	kIx,	,
měření	měření	k1gNnSc1	měření
tloušťky	tloušťka	k1gFnSc2	tloušťka
kožní	kožní	k2eAgFnSc2d1	kožní
řasy	řasa	k1gFnSc2	řasa
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příčiny	příčina	k1gFnPc1	příčina
nadváhy	nadváha	k1gFnSc2	nadváha
lze	lze	k6eAd1	lze
obecně	obecně	k6eAd1	obecně
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
vyšší	vysoký	k2eAgInSc1d2	vyšší
příjem	příjem	k1gInSc1	příjem
energie	energie	k1gFnSc2	energie
než	než	k8xS	než
její	její	k3xOp3gInSc4	její
výdej	výdej	k1gInSc4	výdej
genetika	genetika	k1gFnSc1	genetika
a	a	k8xC	a
vrozené	vrozený	k2eAgFnPc1d1	vrozená
dispozice	dispozice	k1gFnPc1	dispozice
poruchy	porucha	k1gFnSc2	porucha
metabolismu	metabolismus	k1gInSc2	metabolismus
užívání	užívání	k1gNnSc2	užívání
některých	některý	k3yIgInPc2	některý
léků	lék	k1gInPc2	lék
psychické	psychický	k2eAgInPc4d1	psychický
faktory	faktor	k1gInPc4	faktor
nevhodné	vhodný	k2eNgInPc4d1	nevhodný
jídelní	jídelní	k2eAgInPc4d1	jídelní
návyky	návyk	k1gInPc4	návyk
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
hormonální	hormonální	k2eAgInPc1d1	hormonální
vlivy	vliv	k1gInPc1	vliv
stres	stres	k1gInSc4	stres
přejídání	přejídání	k1gNnSc1	přejídání
ve	v	k7c6	v
večerních	večerní	k2eAgFnPc6d1	večerní
hodinách	hodina	k1gFnPc6	hodina
příjem	příjem	k1gInSc4	příjem
málo	málo	k4c1	málo
tekutin	tekutina	k1gFnPc2	tekutina
</s>
