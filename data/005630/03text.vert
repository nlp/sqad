<s>
Den	den	k1gInSc1	den
matek	matka	k1gFnPc2	matka
je	být	k5eAaImIp3nS	být
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
pocta	pocta	k1gFnSc1	pocta
matkám	matka	k1gFnPc3	matka
a	a	k8xC	a
mateřství	mateřství	k1gNnPc2	mateřství
<g/>
.	.	kIx.	.
</s>
<s>
Slaví	slavit	k5eAaImIp3nS	slavit
se	se	k3xPyFc4	se
v	v	k7c4	v
různé	různý	k2eAgInPc4d1	různý
dny	den	k1gInPc4	den
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
tradic	tradice	k1gFnPc2	tradice
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jinak	jinak	k6eAd1	jinak
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
(	(	kIx(	(
<g/>
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
postní	postní	k2eAgFnSc4d1	postní
neděli	neděle	k1gFnSc4	neděle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
v	v	k7c6	v
arabských	arabský	k2eAgFnPc6d1	arabská
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
dávají	dávat	k5eAaImIp3nP	dávat
děti	dítě	k1gFnPc4	dítě
svým	svůj	k3xOyFgNnSc7	svůj
matkám	matka	k1gFnPc3	matka
dárky	dárek	k1gInPc4	dárek
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
vlastnoručně	vlastnoručně	k6eAd1	vlastnoručně
vyrobené	vyrobený	k2eAgFnPc1d1	vyrobená
<g/>
.	.	kIx.	.
</s>
<s>
Obdobné	obdobný	k2eAgInPc4d1	obdobný
svátky	svátek	k1gInPc4	svátek
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
existovaly	existovat	k5eAaImAgInP	existovat
už	už	k6eAd1	už
dávno	dávno	k6eAd1	dávno
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
se	se	k3xPyFc4	se
slavil	slavit	k5eAaImAgInS	slavit
svátek	svátek	k1gInSc1	svátek
plodnosti	plodnost	k1gFnSc2	plodnost
a	a	k8xC	a
matek	matka	k1gFnPc2	matka
<g/>
,	,	kIx,	,
spojený	spojený	k2eAgInSc4d1	spojený
s	s	k7c7	s
uctíváním	uctívání	k1gNnSc7	uctívání
pohanské	pohanský	k2eAgFnSc2d1	pohanská
bohyně	bohyně	k1gFnSc2	bohyně
Rhey	Rhea	k1gFnSc2	Rhea
=	=	kIx~	=
Kybelé	Kybelý	k2eAgFnSc2d1	Kybelý
<g/>
,	,	kIx,	,
matky	matka	k1gFnSc2	matka
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
a	a	k8xC	a
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
oslav	oslava	k1gFnPc2	oslava
tohoto	tento	k3xDgInSc2	tento
svátku	svátek	k1gInSc2	svátek
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Anny	Anna	k1gFnSc2	Anna
Reeves	Reeves	k1gMnSc1	Reeves
Jarvisové	Jarvisový	k2eAgFnSc2d1	Jarvisová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bojovala	bojovat	k5eAaImAgFnS	bojovat
za	za	k7c4	za
práva	právo	k1gNnPc4	právo
matek	matka	k1gFnPc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
Woodrow	Woodrow	k1gMnSc1	Woodrow
Wilson	Wilson	k1gMnSc1	Wilson
první	první	k4xOgFnSc4	první
oficiální	oficiální	k2eAgFnSc4d1	oficiální
oslavu	oslava	k1gFnSc4	oslava
Dne	den	k1gInSc2	den
matek	matka	k1gFnPc2	matka
<g/>
,	,	kIx,	,
konající	konající	k2eAgFnPc1d1	konající
se	se	k3xPyFc4	se
druhou	druhý	k4xOgFnSc4	druhý
květnovou	květnový	k2eAgFnSc4d1	květnová
neděli	neděle	k1gFnSc4	neděle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
podle	podle	k7c2	podle
amerického	americký	k2eAgInSc2d1	americký
vzoru	vzor	k1gInSc2	vzor
rovněž	rovněž	k9	rovněž
druhou	druhý	k4xOgFnSc4	druhý
květnovou	květnový	k2eAgFnSc4d1	květnová
neděli	neděle	k1gFnSc4	neděle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
slavit	slavit	k5eAaImF	slavit
tento	tento	k3xDgInSc1	tento
svátek	svátek	k1gInSc1	svátek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
propagátorkou	propagátorka	k1gFnSc7	propagátorka
byla	být	k5eAaImAgFnS	být
Alice	Alice	k1gFnSc1	Alice
Masaryková	Masaryková	k1gFnSc1	Masaryková
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
postupně	postupně	k6eAd1	postupně
zatlačován	zatlačovat	k5eAaImNgInS	zatlačovat
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
oslavami	oslava	k1gFnPc7	oslava
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
dne	den	k1gInSc2	den
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
slaveného	slavený	k2eAgInSc2d1	slavený
vždy	vždy	k6eAd1	vždy
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
dál	daleko	k6eAd2	daleko
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
rodinách	rodina	k1gFnPc6	rodina
připomínal	připomínat	k5eAaImAgMnS	připomínat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
začal	začít	k5eAaPmAgInS	začít
slavit	slavit	k5eAaImF	slavit
veřejně	veřejně	k6eAd1	veřejně
<g/>
.	.	kIx.	.
</s>
<s>
MDŽ	MDŽ	kA	MDŽ
se	se	k3xPyFc4	se
ale	ale	k9	ale
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
také	také	k9	také
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc1	dva
svátky	svátek	k1gInPc1	svátek
sloučily	sloučit	k5eAaPmAgInP	sloučit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
Den	den	k1gInSc1	den
otců	otec	k1gMnPc2	otec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
otce	otec	k1gMnSc4	otec
a	a	k8xC	a
slaví	slavit	k5eAaImIp3nS	slavit
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
třetí	třetí	k4xOgFnSc4	třetí
neděli	neděle	k1gFnSc4	neděle
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
oslavoval	oslavovat	k5eAaImAgMnS	oslavovat
svátek	svátek	k1gInSc4	svátek
matek	matka	k1gFnPc2	matka
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
neděle	neděle	k1gFnSc1	neděle
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Neděle	neděle	k1gFnSc1	neděle
matek	matka	k1gFnPc2	matka
<g/>
)	)	kIx)	)
a	a	k8xC	a
připadal	připadat	k5eAaImAgMnS	připadat
na	na	k7c4	na
postní	postní	k2eAgFnSc4d1	postní
dobu	doba	k1gFnSc4	doba
před	před	k7c7	před
Velikonocemi	Velikonoce	k1gFnPc7	Velikonoce
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
chudých	chudý	k2eAgMnPc2d1	chudý
lidí	člověk	k1gMnPc2	člověk
tehdy	tehdy	k6eAd1	tehdy
sloužilo	sloužit	k5eAaImAgNnS	sloužit
v	v	k7c6	v
bohatých	bohatý	k2eAgFnPc6d1	bohatá
domácnostech	domácnost	k1gFnPc6	domácnost
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
domova	domov	k1gInSc2	domov
a	a	k8xC	a
nemohlo	moct	k5eNaImAgNnS	moct
trávit	trávit	k5eAaImF	trávit
čas	čas	k1gInSc4	čas
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
matkami	matka	k1gFnPc7	matka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
sloužící	sloužící	k1gMnPc1	sloužící
dostávali	dostávat	k5eAaImAgMnP	dostávat
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
volno	volno	k1gNnSc4	volno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
vrátit	vrátit	k5eAaPmF	vrátit
domů	dům	k1gInPc2	dům
nebo	nebo	k8xC	nebo
navštívit	navštívit	k5eAaPmF	navštívit
kostel	kostel	k1gInSc4	kostel
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žily	žít	k5eAaImAgFnP	žít
jejich	jejich	k3xOp3gFnPc1	jejich
matky	matka	k1gFnPc1	matka
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
příbuzní	příbuzný	k1gMnPc1	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Den	den	k1gInSc1	den
matek	matka	k1gFnPc2	matka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
