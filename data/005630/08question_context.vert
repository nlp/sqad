<s>
Den	den	k1gInSc1	den
matek	matka	k1gFnPc2	matka
je	být	k5eAaImIp3nS	být
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
pocta	pocta	k1gFnSc1	pocta
matkám	matka	k1gFnPc3	matka
a	a	k8xC	a
mateřství	mateřství	k1gNnPc2	mateřství
<g/>
.	.	kIx.	.
</s>
<s>
Slaví	slavit	k5eAaImIp3nS	slavit
se	se	k3xPyFc4	se
v	v	k7c4	v
různé	různý	k2eAgInPc4d1	různý
dny	den	k1gInPc4	den
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
tradic	tradice	k1gFnPc2	tradice
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jinak	jinak	k6eAd1	jinak
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
(	(	kIx(	(
<g/>
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
postní	postní	k2eAgFnSc4d1	postní
neděli	neděle	k1gFnSc4	neděle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
v	v	k7c6	v
arabských	arabský	k2eAgFnPc6d1	arabská
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
dávají	dávat	k5eAaImIp3nP	dávat
děti	dítě	k1gFnPc4	dítě
svým	svůj	k3xOyFgNnSc7	svůj
matkám	matka	k1gFnPc3	matka
dárky	dárek	k1gInPc4	dárek
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
vlastnoručně	vlastnoručně	k6eAd1	vlastnoručně
vyrobené	vyrobený	k2eAgFnPc1d1	vyrobená
<g/>
.	.	kIx.	.
</s>
<s>
Obdobné	obdobný	k2eAgInPc4d1	obdobný
svátky	svátek	k1gInPc4	svátek
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
existovaly	existovat	k5eAaImAgInP	existovat
už	už	k6eAd1	už
dávno	dávno	k6eAd1	dávno
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
se	se	k3xPyFc4	se
slavil	slavit	k5eAaImAgInS	slavit
svátek	svátek	k1gInSc1	svátek
plodnosti	plodnost	k1gFnSc2	plodnost
a	a	k8xC	a
matek	matka	k1gFnPc2	matka
<g/>
,	,	kIx,	,
spojený	spojený	k2eAgInSc4d1	spojený
s	s	k7c7	s
uctíváním	uctívání	k1gNnSc7	uctívání
pohanské	pohanský	k2eAgFnSc2d1	pohanská
bohyně	bohyně	k1gFnSc2	bohyně
Rhey	Rhea	k1gFnSc2	Rhea
=	=	kIx~	=
Kybelé	Kybelý	k2eAgFnSc2d1	Kybelý
<g/>
,	,	kIx,	,
matky	matka	k1gFnSc2	matka
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
a	a	k8xC	a
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
oslav	oslava	k1gFnPc2	oslava
tohoto	tento	k3xDgInSc2	tento
svátku	svátek	k1gInSc2	svátek
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Anny	Anna	k1gFnSc2	Anna
Reeves	Reeves	k1gMnSc1	Reeves
Jarvisové	Jarvisový	k2eAgFnSc2d1	Jarvisová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bojovala	bojovat	k5eAaImAgFnS	bojovat
za	za	k7c4	za
práva	právo	k1gNnPc4	právo
matek	matka	k1gFnPc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
Woodrow	Woodrow	k1gMnSc1	Woodrow
Wilson	Wilson	k1gMnSc1	Wilson
první	první	k4xOgFnSc4	první
oficiální	oficiální	k2eAgFnSc4d1	oficiální
oslavu	oslava	k1gFnSc4	oslava
Dne	den	k1gInSc2	den
matek	matka	k1gFnPc2	matka
<g/>
,	,	kIx,	,
konající	konající	k2eAgFnPc1d1	konající
se	se	k3xPyFc4	se
druhou	druhý	k4xOgFnSc4	druhý
květnovou	květnový	k2eAgFnSc4d1	květnová
neděli	neděle	k1gFnSc4	neděle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
podle	podle	k7c2	podle
amerického	americký	k2eAgInSc2d1	americký
vzoru	vzor	k1gInSc2	vzor
rovněž	rovněž	k9	rovněž
druhou	druhý	k4xOgFnSc4	druhý
květnovou	květnový	k2eAgFnSc4d1	květnová
neděli	neděle	k1gFnSc4	neděle
<g/>
.	.	kIx.	.
</s>
