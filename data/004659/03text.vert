<s>
Teorie	teorie	k1gFnSc1	teorie
velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
The	The	k1gMnSc1	The
Big	Big	k1gFnSc2	Big
Bang	Bang	k1gMnSc1	Bang
Theory	Theora	k1gFnSc2	Theora
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
situační	situační	k2eAgFnSc1d1	situační
komedie	komedie	k1gFnSc1	komedie
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
mladých	mladý	k2eAgMnPc2d1	mladý
fyziků	fyzik	k1gMnPc2	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
Dělá	dělat	k5eAaImIp3nS	dělat
si	se	k3xPyFc3	se
legraci	legrace	k1gFnSc4	legrace
z	z	k7c2	z
jejich	jejich	k3xOp3gInSc2	jejich
osobního	osobní	k2eAgInSc2d1	osobní
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
z	z	k7c2	z
nerdovské	rdovský	k2eNgFnSc2d1	rdovský
subkultury	subkultura	k1gFnSc2	subkultura
současné	současný	k2eAgFnSc2d1	současná
americké	americký	k2eAgFnSc2d1	americká
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
a	a	k8xC	a
staví	stavit	k5eAaImIp3nS	stavit
je	on	k3xPp3gInPc4	on
do	do	k7c2	do
opozice	opozice	k1gFnSc2	opozice
k	k	k7c3	k
obyčejným	obyčejný	k2eAgMnPc3d1	obyčejný
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
vykreslováni	vykreslován	k2eAgMnPc1d1	vykreslován
jako	jako	k8xC	jako
hloupí	hloupý	k2eAgMnPc1d1	hloupý
<g/>
,	,	kIx,	,
leč	leč	k8xS	leč
šťastní	šťastný	k2eAgMnPc1d1	šťastný
a	a	k8xC	a
sociálně	sociálně	k6eAd1	sociálně
naplnění	naplněný	k2eAgMnPc1d1	naplněný
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc4	seriál
tvoří	tvořit	k5eAaImIp3nP	tvořit
a	a	k8xC	a
produkují	produkovat	k5eAaImIp3nP	produkovat
Chuck	Chuck	k1gInSc4	Chuck
Lorre	Lorr	k1gInSc5	Lorr
a	a	k8xC	a
Bill	Bill	k1gMnSc1	Bill
Prady	Prada	k1gFnSc2	Prada
<g/>
,	,	kIx,	,
pilotní	pilotní	k2eAgInSc4d1	pilotní
díl	díl	k1gInSc4	díl
režíroval	režírovat	k5eAaImAgMnS	režírovat
James	James	k1gMnSc1	James
Burrows	Burrowsa	k1gFnPc2	Burrowsa
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2007	[number]	k4	2007
na	na	k7c6	na
CBS	CBS	kA	CBS
<g/>
,	,	kIx,	,
vysílá	vysílat	k5eAaImIp3nS	vysílat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
CTV	CTV	kA	CTV
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc4	Indie
na	na	k7c4	na
Zee	Zee	k1gFnSc4	Zee
Cafe	Caf	k1gInSc2	Caf
<g/>
,	,	kIx,	,
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
na	na	k7c4	na
Warner	Warner	k1gInSc4	Warner
Channel	Channela	k1gFnPc2	Channela
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
na	na	k7c4	na
Channel	Channel	k1gInSc4	Channel
4	[number]	k4	4
<g/>
,	,	kIx,	,
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
na	na	k7c4	na
Nine	Nine	k1gFnSc4	Nine
Network	network	k1gInSc2	network
a	a	k8xC	a
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jej	on	k3xPp3gInSc4	on
vysílají	vysílat	k5eAaImIp3nP	vysílat
na	na	k7c6	na
programu	program	k1gInSc6	program
Prima	prima	k2eAgFnPc2d1	prima
Cool	Coola	k1gFnPc2	Coola
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgFnSc7d3	nejnovější
epizodou	epizoda	k1gFnSc7	epizoda
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
první	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Conjugal	Conjugal	k1gMnSc1	Conjugal
Conjecture	Conjectur	k1gMnSc5	Conjectur
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
pořadu	pořad	k1gInSc2	pořad
byla	být	k5eAaImAgFnS	být
přerušena	přerušen	k2eAgFnSc1d1	přerušena
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
stávky	stávka	k1gFnSc2	stávka
scenáristů	scenárista	k1gMnPc2	scenárista
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
se	se	k3xPyFc4	se
natáčení	natáčení	k1gNnSc2	natáčení
obnovilo	obnovit	k5eAaPmAgNnS	obnovit
a	a	k8xC	a
dokončila	dokončit	k5eAaPmAgFnS	dokončit
se	se	k3xPyFc4	se
první	první	k4xOgFnSc1	první
série	série	k1gFnSc1	série
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
série	série	k1gFnSc1	série
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2008	[number]	k4	2008
epizodou	epizoda	k1gFnSc7	epizoda
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Bad	Bad	k1gMnSc1	Bad
Fish	Fish	k1gMnSc1	Fish
Paradigm	Paradigm	k1gMnSc1	Paradigm
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
také	také	k9	také
vyšla	vyjít	k5eAaPmAgFnS	vyjít
první	první	k4xOgFnSc1	první
série	série	k1gFnSc1	série
seriálu	seriál	k1gInSc2	seriál
na	na	k7c6	na
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
vysílat	vysílat	k5eAaImF	vysílat
třetí	třetí	k4xOgFnSc1	třetí
sezóna	sezóna	k1gFnSc1	sezóna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
posledním	poslední	k2eAgNnSc7d1	poslední
dílem	dílo	k1gNnSc7	dílo
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Lunar	Lunar	k1gMnSc1	Lunar
Excitation	Excitation	k1gInSc1	Excitation
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
obrazovkách	obrazovka	k1gFnPc6	obrazovka
objevil	objevit	k5eAaPmAgInS	objevit
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2010	[number]	k4	2010
se	se	k3xPyFc4	se
natáčela	natáčet	k5eAaImAgFnS	natáčet
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
série	série	k1gFnSc1	série
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
zakončena	zakončit	k5eAaPmNgFnS	zakončit
epizodou	epizoda	k1gFnSc7	epizoda
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Roommate	Roommat	k1gInSc5	Roommat
Transmogrification	Transmogrification	k1gInSc4	Transmogrification
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2011	[number]	k4	2011
se	se	k3xPyFc4	se
na	na	k7c6	na
obrazovkách	obrazovka	k1gFnPc6	obrazovka
objevila	objevit	k5eAaPmAgFnS	objevit
první	první	k4xOgFnSc1	první
epizoda	epizoda	k1gFnSc1	epizoda
páté	pátý	k4xOgFnSc2	pátý
série	série	k1gFnSc2	série
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
odvysílána	odvysílán	k2eAgFnSc1d1	odvysílána
poslední	poslední	k2eAgFnSc1d1	poslední
epizoda	epizoda	k1gFnSc1	epizoda
páté	pátý	k4xOgFnSc2	pátý
sezóny	sezóna	k1gFnSc2	sezóna
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Countdown	Countdown	k1gMnSc1	Countdown
Reflection	Reflection	k1gInSc1	Reflection
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
následovala	následovat	k5eAaImAgFnS	následovat
přestávka	přestávka	k1gFnSc1	přestávka
ve	v	k7c6	v
vysílání	vysílání	k1gNnSc6	vysílání
způsobená	způsobený	k2eAgFnSc1d1	způsobená
letními	letní	k2eAgFnPc7d1	letní
prázdninami	prázdniny	k1gFnPc7	prázdniny
<g/>
,	,	kIx,	,
seriál	seriál	k1gInSc1	seriál
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
začal	začít	k5eAaPmAgInS	začít
vysílat	vysílat	k5eAaImF	vysílat
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
bylo	být	k5eAaImAgNnS	být
odvysíláno	odvysílat	k5eAaPmNgNnS	odvysílat
24	[number]	k4	24
epizod	epizoda	k1gFnPc2	epizoda
šesté	šestý	k4xOgFnSc2	šestý
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
sedmá	sedmý	k4xOgFnSc1	sedmý
série	série	k1gFnSc1	série
se	se	k3xPyFc4	se
vysílala	vysílat	k5eAaImAgFnS	vysílat
od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Osmá	osmý	k4xOgFnSc1	osmý
série	série	k1gFnSc1	série
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
vysílat	vysílat	k5eAaImF	vysílat
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Podoba	podoba	k1gFnSc1	podoba
pilotního	pilotní	k2eAgInSc2d1	pilotní
dílu	díl	k1gInSc2	díl
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgNnSc7	který
bylo	být	k5eAaImAgNnS	být
počítáno	počítat	k5eAaImNgNnS	počítat
na	na	k7c4	na
televizní	televizní	k2eAgFnSc4d1	televizní
sezónu	sezóna	k1gFnSc4	sezóna
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
lehce	lehko	k6eAd1	lehko
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
současné	současný	k2eAgFnSc2d1	současná
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgFnPc1d1	jediná
postavy	postava	k1gFnPc1	postava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
z	z	k7c2	z
úvodního	úvodní	k2eAgInSc2d1	úvodní
dílu	díl	k1gInSc2	díl
v	v	k7c6	v
kompletním	kompletní	k2eAgInSc6d1	kompletní
seriálu	seriál	k1gInSc6	seriál
byly	být	k5eAaImAgInP	být
Leonard	Leonard	k1gInSc4	Leonard
a	a	k8xC	a
Sheldon	Sheldon	k1gInSc4	Sheldon
(	(	kIx(	(
<g/>
ztvárněni	ztvárněn	k2eAgMnPc1d1	ztvárněn
Johnnym	Johnnymum	k1gNnPc2	Johnnymum
Galeckim	Galeckim	k1gInSc4	Galeckim
a	a	k8xC	a
Jimem	Jimem	k1gInSc4	Jimem
Parsonsem	Parsons	k1gInSc7	Parsons
<g/>
,	,	kIx,	,
postavy	postava	k1gFnPc1	postava
byly	být	k5eAaImAgFnP	být
pojmenovány	pojmenovat	k5eAaPmNgFnP	pojmenovat
po	po	k7c6	po
Sheldonu	Sheldon	k1gMnSc6	Sheldon
Leonardovi	Leonardo	k1gMnSc6	Leonardo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
se	se	k3xPyFc4	se
také	také	k9	také
měly	mít	k5eAaImAgFnP	mít
objevit	objevit	k5eAaPmF	objevit
postavy	postava	k1gFnPc1	postava
dvou	dva	k4xCgInPc2	dva
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
Katie	Katie	k1gFnSc1	Katie
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
holka	holka	k1gFnSc1	holka
vychována	vychován	k2eAgFnSc1d1	vychována
ulicí	ulice	k1gFnSc7	ulice
<g/>
,	,	kIx,	,
drsná	drsný	k2eAgFnSc1d1	drsná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
citlivou	citlivý	k2eAgFnSc7d1	citlivá
duší	duše	k1gFnSc7	duše
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
po	po	k7c6	po
rozchodu	rozchod	k1gInSc6	rozchod
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
poznali	poznat	k5eAaPmAgMnP	poznat
hlavní	hlavní	k2eAgMnPc1d1	hlavní
hrdinové	hrdina	k1gMnPc1	hrdina
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
ji	on	k3xPp3gFnSc4	on
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
spolubydlení	spolubydlení	k1gNnSc4	spolubydlení
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
hrdinů	hrdina	k1gMnPc2	hrdina
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Amanda	Amanda	k1gFnSc1	Amanda
Walsh	Walsh	k1gMnSc1	Walsh
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
pilotním	pilotní	k2eAgInSc6d1	pilotní
díle	díl	k1gInSc6	díl
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
postava	postava	k1gFnSc1	postava
obměněna	obměnit	k5eAaPmNgFnS	obměnit
za	za	k7c4	za
Penny	penny	k1gFnPc4	penny
(	(	kIx(	(
<g/>
Kaley	Kalea	k1gFnPc4	Kalea
Cuoco	Cuoco	k6eAd1	Cuoco
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Druhou	druhý	k4xOgFnSc7	druhý
ženskou	ženský	k2eAgFnSc7d1	ženská
postavou	postava	k1gFnSc7	postava
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
Gilda	gilda	k1gFnSc1	gilda
(	(	kIx(	(
<g/>
Iris	iris	k1gFnSc1	iris
Bahr	Bahr	k1gMnSc1	Bahr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
vědkyně	vědkyně	k1gFnSc1	vědkyně
<g/>
,	,	kIx,	,
kolegyně	kolegyně	k1gFnSc1	kolegyně
a	a	k8xC	a
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
hlavních	hlavní	k2eAgMnPc2d1	hlavní
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
ohrožena	ohrozit	k5eAaPmNgFnS	ohrozit
přítomností	přítomnost	k1gFnSc7	přítomnost
Katie	Katie	k1gFnSc2	Katie
<g/>
.	.	kIx.	.
</s>
<s>
Pilotní	pilotní	k2eAgInSc1d1	pilotní
díl	díl	k1gInSc1	díl
používal	používat	k5eAaImAgInS	používat
jako	jako	k9	jako
titulní	titulní	k2eAgFnSc4d1	titulní
znělku	znělka	k1gFnSc4	znělka
"	"	kIx"	"
<g/>
She	She	k1gMnSc1	She
Blinded	Blinded	k1gMnSc1	Blinded
Me	Me	k1gMnSc1	Me
with	with	k1gMnSc1	with
Science	Science	k1gFnSc1	Science
<g/>
"	"	kIx"	"
od	od	k7c2	od
Thomase	Thomas	k1gMnSc2	Thomas
Dolbyho	Dolby	k1gMnSc2	Dolby
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
nebyl	být	k5eNaImAgMnS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
do	do	k7c2	do
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
televizní	televizní	k2eAgFnSc1d1	televizní
společnost	společnost	k1gFnSc1	společnost
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
tvůrcům	tvůrce	k1gMnPc3	tvůrce
možnost	možnost	k1gFnSc4	možnost
přepracovat	přepracovat	k5eAaPmF	přepracovat
námět	námět	k1gInSc4	námět
seriálu	seriál	k1gInSc2	seriál
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
druhý	druhý	k4xOgInSc4	druhý
pilotní	pilotní	k2eAgInSc4d1	pilotní
díl	díl	k1gInSc4	díl
<g/>
.	.	kIx.	.
</s>
<s>
Zajistili	zajistit	k5eAaPmAgMnP	zajistit
další	další	k2eAgFnPc4d1	další
postavy	postava	k1gFnPc4	postava
a	a	k8xC	a
přepracovali	přepracovat	k5eAaPmAgMnP	přepracovat
seriál	seriál	k1gInSc4	seriál
do	do	k7c2	do
konečné	konečný	k2eAgFnSc2d1	konečná
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
pilotní	pilotní	k2eAgInSc1d1	pilotní
díl	díl	k1gInSc1	díl
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
odvysílán	odvysílán	k2eAgInSc1d1	odvysílán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
přípravy	příprava	k1gFnSc2	příprava
seriálu	seriál	k1gInSc2	seriál
Chuck	Chucko	k1gNnPc2	Chucko
Lorre	Lorr	k1gInSc5	Lorr
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
"	"	kIx"	"
<g/>
Na	na	k7c6	na
pilotu	pilot	k1gMnSc6	pilot
Big	Big	k1gMnSc6	Big
Bangu	Bang	k1gMnSc6	Bang
jsme	být	k5eAaImIp1nP	být
dělali	dělat	k5eAaImAgMnP	dělat
asi	asi	k9	asi
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
a	a	k8xC	a
úplně	úplně	k6eAd1	úplně
propadl	propadnout	k5eAaPmAgMnS	propadnout
<g/>
...	...	k?	...
ale	ale	k8xC	ale
dvě	dva	k4xCgFnPc1	dva
věci	věc	k1gFnPc1	věc
fungovaly	fungovat	k5eAaImAgFnP	fungovat
perfektně	perfektně	k6eAd1	perfektně
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Johnny	Johnna	k1gFnPc4	Johnna
a	a	k8xC	a
Jim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Přepsali	přepsat	k5eAaPmAgMnP	přepsat
jsme	být	k5eAaImIp1nP	být
další	další	k2eAgFnPc4d1	další
věci	věc	k1gFnPc4	věc
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
jsme	být	k5eAaImIp1nP	být
oslepeni	oslepen	k2eAgMnPc1d1	oslepen
Kaley	Kalea	k1gFnSc2	Kalea
<g/>
,	,	kIx,	,
Simonem	Simon	k1gMnSc7	Simon
a	a	k8xC	a
Kunalem	Kunal	k1gMnSc7	Kunal
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
původní	původní	k2eAgInSc1d1	původní
pilotní	pilotní	k2eAgInSc1d1	pilotní
díl	díl	k1gInSc1	díl
možná	možná	k9	možná
vyjde	vyjít	k5eAaPmIp3nS	vyjít
jako	jako	k9	jako
bonus	bonus	k1gInSc1	bonus
na	na	k7c6	na
DVD	DVD	kA	DVD
k	k	k7c3	k
seriálu	seriál	k1gInSc3	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Lorre	Lorr	k1gMnSc5	Lorr
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ty	ten	k3xDgFnPc1	ten
jó	jó	k0	jó
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
bych	by	kYmCp1nS	by
chtěl	chtít	k5eAaImAgMnS	chtít
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
vidět	vidět	k5eAaImF	vidět
svoje	svůj	k3xOyFgFnPc4	svůj
chyby	chyba	k1gFnPc4	chyba
<g/>
...	...	k?	...
uvidíme	uvidět	k5eAaPmIp1nP	uvidět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
První	první	k4xOgMnSc1	první
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
pilotní	pilotní	k2eAgInSc1d1	pilotní
díl	díl	k1gInSc1	díl
seriálu	seriál	k1gInSc2	seriál
The	The	k1gFnSc2	The
Big	Big	k1gFnPc2	Big
Bang	Banga	k1gFnPc2	Banga
Theory	Theora	k1gFnSc2	Theora
byly	být	k5eAaImAgInP	být
režírovány	režírovat	k5eAaImNgInP	režírovat
Jamesem	James	k1gInSc7	James
Burrowsem	Burrows	k1gMnSc7	Burrows
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
však	však	k9	však
další	další	k2eAgInPc4d1	další
díly	díl	k1gInPc4	díl
nerežíroval	režírovat	k5eNaImAgMnS	režírovat
<g/>
.	.	kIx.	.
</s>
<s>
Přepracovaný	přepracovaný	k2eAgInSc4d1	přepracovaný
druhý	druhý	k4xOgInSc4	druhý
pilotní	pilotní	k2eAgInSc4d1	pilotní
díl	díl	k1gInSc4	díl
vedl	vést	k5eAaImAgMnS	vést
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
k	k	k7c3	k
objednávce	objednávka	k1gFnSc3	objednávka
13	[number]	k4	13
epizod	epizoda	k1gFnPc2	epizoda
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
CBS	CBS	kA	CBS
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vysíláním	vysílání	k1gNnSc7	vysílání
pilotního	pilotní	k2eAgInSc2d1	pilotní
dílu	díl	k1gInSc2	díl
na	na	k7c6	na
CBS	CBS	kA	CBS
byla	být	k5eAaImAgFnS	být
epizoda	epizoda	k1gFnSc1	epizoda
uvolněna	uvolnit	k5eAaPmNgFnS	uvolnit
zdarma	zdarma	k6eAd1	zdarma
na	na	k7c4	na
iTunes	iTunes	k1gInSc4	iTunes
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
USA	USA	kA	USA
vysílat	vysílat	k5eAaImF	vysílat
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2007	[number]	k4	2007
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
na	na	k7c4	na
celkových	celkový	k2eAgInPc2d1	celkový
22	[number]	k4	22
dílů	díl	k1gInPc2	díl
první	první	k4xOgFnSc2	první
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
produkce	produkce	k1gFnSc1	produkce
seriálu	seriál	k1gInSc2	seriál
byla	být	k5eAaImAgFnS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
zastavena	zastavit	k5eAaPmNgFnS	zastavit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
stávky	stávka	k1gFnSc2	stávka
scenáristů	scenárista	k1gMnPc2	scenárista
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
téměř	téměř	k6eAd1	téměř
3	[number]	k4	3
měsíce	měsíc	k1gInSc2	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
seriál	seriál	k1gInSc1	seriál
přechodně	přechodně	k6eAd1	přechodně
nahrazen	nahradit	k5eAaPmNgInS	nahradit
krátkým	krátký	k2eAgInSc7d1	krátký
sitcomem	sitcom	k1gInSc7	sitcom
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Captain	Captain	k1gInSc1	Captain
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Vítejte	vítat	k5eAaImRp2nP	vítat
u	u	k7c2	u
kapitána	kapitán	k1gMnSc2	kapitán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
seriál	seriál	k1gInSc1	seriál
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
v	v	k7c6	v
lepším	dobrý	k2eAgInSc6d2	lepší
vysílacím	vysílací	k2eAgInSc6d1	vysílací
čase	čas	k1gInSc6	čas
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
první	první	k4xOgFnSc6	první
sezóně	sezóna	k1gFnSc6	sezóna
odvysíláno	odvysílat	k5eAaPmNgNnS	odvysílat
pouze	pouze	k6eAd1	pouze
17	[number]	k4	17
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
stávky	stávka	k1gFnSc2	stávka
byla	být	k5eAaImAgFnS	být
společností	společnost	k1gFnSc7	společnost
CBS	CBS	kA	CBS
zakoupena	zakoupen	k2eAgFnSc1d1	zakoupena
druhá	druhý	k4xOgFnSc1	druhý
série	série	k1gFnSc1	série
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
odvysílána	odvysílat	k5eAaPmNgFnS	odvysílat
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2008	[number]	k4	2008
a	a	k8xC	a
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
vysílána	vysílán	k2eAgFnSc1d1	vysílána
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
vysílacím	vysílací	k2eAgInSc6d1	vysílací
čase	čas	k1gInSc6	čas
a	a	k8xC	a
premiérový	premiérový	k2eAgInSc1d1	premiérový
díl	díl	k1gInSc1	díl
byl	být	k5eAaImAgInS	být
odvysílán	odvysílat	k5eAaPmNgInS	odvysílat
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
lepšícím	lepšící	k2eAgMnSc7d1	lepšící
se	se	k3xPyFc4	se
hodnocením	hodnocení	k1gNnSc7	hodnocení
byl	být	k5eAaImAgInS	být
seriál	seriál	k1gInSc1	seriál
prodloužen	prodloužit	k5eAaPmNgInS	prodloužit
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
sezóny	sezóna	k1gFnPc4	sezóna
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
seriál	seriál	k1gInSc1	seriál
prodloužen	prodloužit	k5eAaPmNgInS	prodloužit
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
tři	tři	k4xCgFnPc4	tři
sezóny	sezóna	k1gFnPc4	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
je	být	k5eAaImIp3nS	být
natáčen	natáčet	k5eAaImNgInS	natáčet
před	před	k7c7	před
živým	živý	k2eAgNnSc7d1	živé
publikem	publikum	k1gNnSc7	publikum
a	a	k8xC	a
produkován	produkovat	k5eAaImNgInS	produkovat
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
společností	společnost	k1gFnPc2	společnost
Warner	Warnra	k1gFnPc2	Warnra
Bros	Bros	k1gInSc1	Bros
<g/>
.	.	kIx.	.
</s>
<s>
Television	Television	k1gInSc1	Television
a	a	k8xC	a
Chuck	Chuck	k1gInSc1	Chuck
Lorre	Lorr	k1gInSc5	Lorr
Productions	Productionsa	k1gFnPc2	Productionsa
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
astronomie	astronomie	k1gFnSc2	astronomie
Kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gMnSc1	Angeles
David	David	k1gMnSc1	David
Saltzberg	Saltzberg	k1gMnSc1	Saltzberg
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
scénáře	scénář	k1gInPc4	scénář
a	a	k8xC	a
upravuje	upravovat	k5eAaImIp3nS	upravovat
dialogy	dialog	k1gInPc1	dialog
<g/>
,	,	kIx,	,
matematické	matematický	k2eAgInPc1d1	matematický
vzorce	vzorec	k1gInPc1	vzorec
a	a	k8xC	a
diagramy	diagram	k1gInPc1	diagram
používané	používaný	k2eAgInPc1d1	používaný
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výkonného	výkonný	k2eAgMnSc2d1	výkonný
producenta	producent	k1gMnSc2	producent
<g/>
/	/	kIx~	/
<g/>
spolutvůrce	spolutvůrce	k1gMnSc2	spolutvůrce
Billa	Bill	k1gMnSc2	Bill
Pradyho	Prady	k1gMnSc2	Prady
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Pracujeme	pracovat	k5eAaImIp1nP	pracovat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Sheldon	Sheldon	k1gInSc1	Sheldon
řešil	řešit	k5eAaImAgInS	řešit
opravdové	opravdový	k2eAgInPc4d1	opravdový
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
probíhaly	probíhat	k5eAaImAgInP	probíhat
celou	celý	k2eAgFnSc4d1	celá
první	první	k4xOgFnSc4	první
sezónu	sezóna	k1gFnSc4	sezóna
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
aktuální	aktuální	k2eAgInSc4d1	aktuální
průběh	průběh	k1gInSc4	průběh
byl	být	k5eAaImAgInS	být
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
na	na	k7c6	na
tabulích	tabule	k1gFnPc6	tabule
...	...	k?	...
těžce	těžce	k6eAd1	těžce
jsme	být	k5eAaImIp1nP	být
pracovali	pracovat	k5eAaImAgMnP	pracovat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgFnPc4	všechen
vědecké	vědecký	k2eAgFnPc4d1	vědecká
věci	věc	k1gFnPc4	věc
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
herců	herec	k1gMnPc2	herec
v	v	k7c6	v
Teorii	teorie	k1gFnSc6	teorie
velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
spolu	spolu	k6eAd1	spolu
pracovali	pracovat	k5eAaImAgMnP	pracovat
na	na	k7c6	na
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
Roseanne	Roseann	k1gInSc5	Roseann
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
Johnnyho	Johnny	k1gMnSc2	Johnny
Galeckiho	Galecki	k1gMnSc2	Galecki
<g/>
,	,	kIx,	,
Sary	Sara	k1gFnSc2	Sara
Gilbertové	Gilbert	k1gMnPc1	Gilbert
a	a	k8xC	a
Laurie	Laurie	k1gFnPc1	Laurie
Metcalfové	Metcalfový	k2eAgFnPc1d1	Metcalfová
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
Sheldonovu	Sheldonův	k2eAgFnSc4d1	Sheldonova
matku	matka	k1gFnSc4	matka
Mary	Mary	k1gFnSc1	Mary
Cooperovou	Cooperová	k1gFnSc7	Cooperová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
byl	být	k5eAaImAgInS	být
Chuck	Chuck	k1gInSc1	Chuck
Lorre	Lorr	k1gInSc5	Lorr
scenáristou	scenárista	k1gMnSc7	scenárista
tohoto	tento	k3xDgInSc2	tento
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
alternativního	alternativní	k2eAgInSc2d1	alternativní
rocku	rock	k1gInSc2	rock
Barenaked	Barenaked	k1gMnSc1	Barenaked
Ladies	Ladiesa	k1gFnPc2	Ladiesa
napsala	napsat	k5eAaBmAgFnS	napsat
a	a	k8xC	a
nahrála	nahrát	k5eAaPmAgFnS	nahrát
titulní	titulní	k2eAgFnSc4d1	titulní
píseň	píseň	k1gFnSc4	píseň
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umělecky	umělecky	k6eAd1	umělecky
popisuje	popisovat	k5eAaImIp3nS	popisovat
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
stvoření	stvoření	k1gNnSc4	stvoření
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
seriálu	seriál	k1gInSc2	seriál
Chuck	Chuck	k1gInSc1	Chuck
Lorre	Lorr	k1gInSc5	Lorr
a	a	k8xC	a
Bill	Bill	k1gMnSc1	Bill
Prady	Prada	k1gFnSc2	Prada
oslovili	oslovit	k5eAaPmAgMnP	oslovit
hlavního	hlavní	k2eAgMnSc4d1	hlavní
zpěváka	zpěvák	k1gMnSc4	zpěvák
a	a	k8xC	a
kytaristu	kytarista	k1gMnSc4	kytarista
skupiny	skupina	k1gFnSc2	skupina
Eda	Eda	k1gMnSc1	Eda
Robertsona	Robertsona	k1gFnSc1	Robertsona
a	a	k8xC	a
požádali	požádat	k5eAaPmAgMnP	požádat
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
napsal	napsat	k5eAaPmAgMnS	napsat
titulní	titulní	k2eAgFnSc4d1	titulní
píseň	píseň	k1gFnSc4	píseň
pro	pro	k7c4	pro
seriál	seriál	k1gInSc4	seriál
po	po	k7c6	po
té	ten	k3xDgFnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
navštívili	navštívit	k5eAaPmAgMnP	navštívit
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
koncertů	koncert	k1gInPc2	koncert
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
Ed	Ed	k1gMnSc1	Ed
Robertson	Robertson	k1gMnSc1	Robertson
dříve	dříve	k6eAd2	dříve
přečetl	přečíst	k5eAaPmAgMnS	přečíst
knihu	kniha	k1gFnSc4	kniha
Simona	Simona	k1gFnSc1	Simona
Singha	Singha	k1gMnSc1	Singha
Big	Big	k1gMnSc1	Big
Bang	Bang	k1gMnSc1	Bang
a	a	k8xC	a
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
předvedl	předvést	k5eAaPmAgMnS	předvést
improvizaci	improvizace	k1gFnSc4	improvizace
pomocí	pomocí	k7c2	pomocí
freestyle	freestyl	k1gInSc5	freestyl
rapu	rap	k1gMnSc6	rap
o	o	k7c6	o
původu	původ	k1gInSc6	původ
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Lorre	Lorr	k1gMnSc5	Lorr
a	a	k8xC	a
Prady	Prada	k1gFnPc4	Prada
později	pozdě	k6eAd2	pozdě
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
zavolali	zavolat	k5eAaPmAgMnP	zavolat
Robertsonovi	Robertsonův	k2eAgMnPc1d1	Robertsonův
a	a	k8xC	a
zeptali	zeptat	k5eAaPmAgMnP	zeptat
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc2	on
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
zájem	zájem	k1gInSc4	zájem
napsat	napsat	k5eAaBmF	napsat
titulní	titulní	k2eAgFnSc4d1	titulní
píseň	píseň	k1gFnSc4	píseň
k	k	k7c3	k
seriálu	seriál	k1gInSc3	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Robertson	Robertson	k1gMnSc1	Robertson
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
pouze	pouze	k6eAd1	pouze
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
a	a	k8xC	a
po	po	k7c4	po
ujištění	ujištění	k1gNnSc4	ujištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lorre	Lorr	k1gInSc5	Lorr
a	a	k8xC	a
Prady	Prada	k1gFnPc1	Prada
nebudou	být	k5eNaImBp3nP	být
již	již	k6eAd1	již
shánět	shánět	k5eAaImF	shánět
žádné	žádný	k3yNgMnPc4	žádný
další	další	k2eAgMnPc4d1	další
autory	autor	k1gMnPc4	autor
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
komerčně	komerčně	k6eAd1	komerčně
vydána	vydán	k2eAgFnSc1d1	vydána
plná	plný	k2eAgFnSc1d1	plná
verze	verze	k1gFnSc1	verze
(	(	kIx(	(
<g/>
1	[number]	k4	1
minuta	minut	k2eAgFnSc1d1	minuta
a	a	k8xC	a
45	[number]	k4	45
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
píseň	píseň	k1gFnSc4	píseň
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
History	Histor	k1gInPc1	Histor
of	of	k?	of
Everything	Everything	k1gInSc1	Everything
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c6	na
obalu	obal	k1gInSc6	obal
singlu	singl	k1gInSc2	singl
je	být	k5eAaImIp3nS	být
píseň	píseň	k1gFnSc1	píseň
uvedena	uvést	k5eAaPmNgFnS	uvést
jako	jako	k8xS	jako
titulní	titulní	k2eAgFnSc1d1	titulní
píseň	píseň	k1gFnSc1	píseň
Teorie	teorie	k1gFnSc1	teorie
Velkého	velký	k2eAgInSc2d1	velký
Třesku	třesk	k1gInSc2	třesk
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgInSc1d1	hudební
videoklip	videoklip	k1gInSc1	videoklip
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
a	a	k8xC	a
přiložen	přiložit	k5eAaPmNgInS	přiložit
jako	jako	k9	jako
bonus	bonus	k1gInSc1	bonus
při	při	k7c6	při
vydání	vydání	k1gNnSc6	vydání
DVD	DVD	kA	DVD
a	a	k8xC	a
Blu-ray	Bluaa	k1gFnSc2	Blu-raa
Kompletní	kompletní	k2eAgFnSc2d1	kompletní
4	[number]	k4	4
<g/>
.	.	kIx.	.
sezóny	sezóna	k1gFnSc2	sezóna
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
The	The	k1gMnSc1	The
Complete	Comple	k1gNnSc2	Comple
Fourth	Fourth	k1gMnSc1	Fourth
Season	Season	k1gMnSc1	Season
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
byla	být	k5eAaImAgFnS	být
také	také	k9	také
uvolněna	uvolnit	k5eAaPmNgFnS	uvolnit
pro	pro	k7c4	pro
nejznámější	známý	k2eAgNnSc4d3	nejznámější
album	album	k1gNnSc4	album
skupiny	skupina	k1gFnSc2	skupina
Barenaked	Barenaked	k1gMnSc1	Barenaked
Ladies	Ladies	k1gMnSc1	Ladies
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Hits	Hits	k1gInSc1	Hits
from	froma	k1gFnPc2	froma
Yesterday	Yesterdaa	k1gFnSc2	Yesterdaa
&	&	k?	&
the	the	k?	the
Day	Day	k1gMnSc5	Day
Before	Befor	k1gMnSc5	Befor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
alba	album	k1gNnSc2	album
došlo	dojít	k5eAaPmAgNnS	dojít
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvních	první	k4xOgInPc2	první
tří	tři	k4xCgInPc2	tři
sérií	série	k1gFnPc2	série
tři	tři	k4xCgMnPc4	tři
hlavní	hlavní	k2eAgMnPc1d1	hlavní
představitelé	představitel	k1gMnPc1	představitel
(	(	kIx(	(
<g/>
Galecki	Galecki	k1gNnSc1	Galecki
<g/>
,	,	kIx,	,
Parsons	Parsons	k1gInSc1	Parsons
a	a	k8xC	a
Cuoco	Cuoco	k6eAd1	Cuoco
<g/>
)	)	kIx)	)
vydělávali	vydělávat	k5eAaImAgMnP	vydělávat
maximálně	maximálně	k6eAd1	maximálně
60	[number]	k4	60
000	[number]	k4	000
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
epizodu	epizoda	k1gFnSc4	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Výdělky	výdělek	k1gInPc1	výdělek
těchto	tento	k3xDgInPc2	tento
tří	tři	k4xCgFnPc2	tři
vyrostly	vyrůst	k5eAaPmAgFnP	vyrůst
ve	v	k7c6	v
4	[number]	k4	4
sérii	série	k1gFnSc6	série
na	na	k7c4	na
200	[number]	k4	200
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
epizodu	epizoda	k1gFnSc4	epizoda
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
smlouvy	smlouva	k1gFnSc2	smlouva
bylo	být	k5eAaImAgNnS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
třech	tři	k4xCgFnPc6	tři
sériích	série	k1gFnPc6	série
porostou	růst	k5eAaImIp3nP	růst
jejich	jejich	k3xOp3gInPc4	jejich
platy	plat	k1gInPc4	plat
o	o	k7c4	o
50	[number]	k4	50
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
sérii	série	k1gFnSc6	série
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
v	v	k7c6	v
sedmé	sedmý	k4xOgFnSc6	sedmý
sérii	série	k1gFnSc6	série
tito	tento	k3xDgMnPc1	tento
herci	herec	k1gMnPc1	herec
vydělali	vydělat	k5eAaPmAgMnP	vydělat
350	[number]	k4	350
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
epizodu	epizoda	k1gFnSc4	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
pak	pak	k6eAd1	pak
ústřední	ústřední	k2eAgFnSc1d1	ústřední
trojice	trojice	k1gFnSc1	trojice
podepsala	podepsat	k5eAaPmAgFnS	podepsat
kontrakt	kontrakt	k1gInSc4	kontrakt
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
tři	tři	k4xCgFnPc4	tři
série	série	k1gFnPc4	série
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
honorář	honorář	k1gInSc1	honorář
má	mít	k5eAaImIp3nS	mít
činit	činit	k5eAaImF	činit
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
milion	milion	k4xCgInSc1	milion
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
epizodu	epizoda	k1gFnSc4	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Leonard	Leonard	k1gMnSc1	Leonard
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
experimentální	experimentální	k2eAgFnSc7d1	experimentální
fyzikou	fyzika	k1gFnSc7	fyzika
<g/>
,	,	kIx,	,
a	a	k8xC	a
geniální	geniální	k2eAgMnSc1d1	geniální
teoretický	teoretický	k2eAgMnSc1d1	teoretický
fyzik	fyzik	k1gMnSc1	fyzik
Sheldon	Sheldon	k1gMnSc1	Sheldon
s	s	k7c7	s
IQ	iq	kA	iq
187	[number]	k4	187
spolu	spolu	k6eAd1	spolu
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
bytě	byt	k1gInSc6	byt
<g/>
.	.	kIx.	.
</s>
<s>
Zajímají	zajímat	k5eAaImIp3nP	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
kvantovou	kvantový	k2eAgFnSc4d1	kvantová
mechaniku	mechanika	k1gFnSc4	mechanika
<g/>
,	,	kIx,	,
teorii	teorie	k1gFnSc4	teorie
superstrun	superstruna	k1gFnPc2	superstruna
<g/>
,	,	kIx,	,
sci-fi	scii	k1gNnPc2	sci-fi
a	a	k8xC	a
fantasy	fantas	k1gInPc1	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
výpočtů	výpočet	k1gInPc2	výpočet
nových	nový	k2eAgFnPc2d1	nová
vlastností	vlastnost	k1gFnPc2	vlastnost
elementárních	elementární	k2eAgFnPc2d1	elementární
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
hraní	hraní	k1gNnSc2	hraní
her	hra	k1gFnPc2	hra
jako	jako	k8xC	jako
například	například	k6eAd1	například
Halo	halo	k1gNnSc4	halo
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
dvěma	dva	k4xCgInPc7	dva
vědci	vědec	k1gMnPc7	vědec
<g/>
,	,	kIx,	,
Howardem	Howard	k1gInSc7	Howard
a	a	k8xC	a
Rajeshem	Rajesh	k1gInSc7	Rajesh
zabývajícím	zabývající	k2eAgInSc7d1	zabývající
se	se	k3xPyFc4	se
astrofyzikou	astrofyzika	k1gFnSc7	astrofyzika
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
díle	díl	k1gInSc6	díl
do	do	k7c2	do
jejich	jejich	k3xOp3gInSc2	jejich
života	život	k1gInSc2	život
vtrhne	vtrhnout	k5eAaPmIp3nS	vtrhnout
Penny	penny	k1gFnSc1	penny
<g/>
,	,	kIx,	,
přitažlivá	přitažlivý	k2eAgFnSc1d1	přitažlivá
blondýnka	blondýnka	k1gFnSc1	blondýnka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přestože	přestože	k8xS	přestože
neví	vědět	k5eNaImIp3nS	vědět
nic	nic	k3yNnSc1	nic
o	o	k7c6	o
Schrödingerově	Schrödingerův	k2eAgFnSc6d1	Schrödingerova
kočce	kočka	k1gFnSc6	kočka
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
neumí	umět	k5eNaImIp3nS	umět
klingonsky	klingonsky	k6eAd1	klingonsky
<g/>
,	,	kIx,	,
Leonarda	Leonardo	k1gMnSc2	Leonardo
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
především	především	k9	především
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Leonardova	Leonardův	k2eAgFnSc1d1	Leonardova
náklonnost	náklonnost	k1gFnSc1	náklonnost
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
diváky	divák	k1gMnPc7	divák
provází	provázet	k5eAaImIp3nS	provázet
nejen	nejen	k6eAd1	nejen
celou	celý	k2eAgFnSc4d1	celá
první	první	k4xOgNnSc4	první
sérií	série	k1gFnPc2	série
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
studnic	studnice	k1gFnPc2	studnice
situačního	situační	k2eAgInSc2d1	situační
humoru	humor	k1gInSc2	humor
<g/>
.	.	kIx.	.
</s>
<s>
Johnny	Johnen	k2eAgFnPc1d1	Johnna
Galecki	Galeck	k1gFnPc1	Galeck
jako	jako	k8xS	jako
Leonard	Leonard	k1gMnSc1	Leonard
Hofstadter	Hofstadter	k1gMnSc1	Hofstadter
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
(	(	kIx(	(
<g/>
český	český	k2eAgInSc4d1	český
dabing	dabing	k1gInSc4	dabing
Aleš	Aleš	k1gMnSc1	Aleš
Háma	Háma	k1gMnSc1	Háma
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
experimentální	experimentální	k2eAgMnSc1d1	experimentální
fyzik	fyzik	k1gMnSc1	fyzik
s	s	k7c7	s
IQ	iq	kA	iq
173	[number]	k4	173
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
získal	získat	k5eAaPmAgInS	získat
ve	v	k7c6	v
24	[number]	k4	24
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
férovým	férový	k2eAgMnSc7d1	férový
mužem	muž	k1gMnSc7	muž
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
<g/>
,	,	kIx,	,
bydlí	bydlet	k5eAaImIp3nS	bydlet
s	s	k7c7	s
kolegou	kolega	k1gMnSc7	kolega
a	a	k8xC	a
přítelem	přítel	k1gMnSc7	přítel
Sheldonem	Sheldon	k1gMnSc7	Sheldon
Cooperem	Cooper	k1gMnSc7	Cooper
v	v	k7c6	v
Pasadeně	Pasadena	k1gFnSc6	Pasadena
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
seriálu	seriál	k1gInSc2	seriál
předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
potenciální	potenciální	k2eAgInSc4d1	potenciální
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
Hofstadterem	Hofstadter	k1gInSc7	Hofstadter
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
sousedkou	sousedka	k1gFnSc7	sousedka
Penny	penny	k1gFnSc7	penny
<g/>
,	,	kIx,	,
a	a	k8xC	a
sexuální	sexuální	k2eAgNnSc4d1	sexuální
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
občasným	občasný	k2eAgFnPc3d1	občasná
schůzkám	schůzka	k1gFnPc3	schůzka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
sérii	série	k1gFnSc6	série
spolu	spolu	k6eAd1	spolu
tito	tento	k3xDgMnPc1	tento
dva	dva	k4xCgMnPc1	dva
začínají	začínat	k5eAaImIp3nP	začínat
chodit	chodit	k5eAaImF	chodit
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
stále	stále	k6eAd1	stále
žijí	žít	k5eAaImIp3nP	žít
odděleně	odděleně	k6eAd1	odděleně
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
rozchází	rozcházet	k5eAaImIp3nS	rozcházet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
páté	pátý	k4xOgFnSc6	pátý
sérii	série	k1gFnSc6	série
spolu	spolu	k6eAd1	spolu
začínají	začínat	k5eAaImIp3nP	začínat
chodit	chodit	k5eAaImF	chodit
opět	opět	k6eAd1	opět
a	a	k8xC	a
zkoušejí	zkoušet	k5eAaImIp3nP	zkoušet
experiment	experiment	k1gInSc4	experiment
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
betatest	betatest	k1gInSc1	betatest
vztahu	vztah	k1gInSc2	vztah
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
mohou	moct	k5eAaImIp3nP	moct
oba	dva	k4xCgMnPc1	dva
reportovat	reportovat	k5eAaImF	reportovat
chyby	chyba	k1gFnPc1	chyba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmé	sedmý	k4xOgFnSc6	sedmý
sérii	série	k1gFnSc6	série
se	se	k3xPyFc4	se
zasnoubí	zasnoubit	k5eAaPmIp3nS	zasnoubit
a	a	k8xC	a
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
vezmou	vzít	k5eAaPmIp3nP	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Jim	on	k3xPp3gMnPc3	on
Parsons	Parsonsa	k1gFnPc2	Parsonsa
<g/>
[	[	kIx(	[
<g/>
35	[number]	k4	35
<g/>
]	]	kIx)	]
jako	jako	k8xS	jako
Sheldon	Sheldon	k1gMnSc1	Sheldon
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
[	[	kIx(	[
<g/>
36	[number]	k4	36
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
český	český	k2eAgInSc4d1	český
dabing	dabing	k1gInSc4	dabing
Jakub	Jakub	k1gMnSc1	Jakub
Wehrenberg	Wehrenberg	k1gMnSc1	Wehrenberg
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Galvestonu	Galveston	k1gInSc2	Galveston
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
[	[	kIx(	[
<g/>
37	[number]	k4	37
<g/>
]	]	kIx)	]
byl	být	k5eAaImAgMnS	být
zázračným	zázračný	k2eAgMnSc7d1	zázračný
<g />
.	.	kIx.	.
</s>
<s>
dítětem	dítě	k1gNnSc7	dítě
s	s	k7c7	s
eidetickou	eidetický	k2eAgFnSc7d1	eidetická
pamětí	paměť	k1gFnSc7	paměť
a	a	k8xC	a
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
11	[number]	k4	11
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
páté	pátý	k4xOgFnSc2	pátý
třídy	třída	k1gFnSc2	třída
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgInSc1d2	vyšší
stupeň	stupeň	k1gInSc1	stupeň
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
ve	v	k7c6	v
14	[number]	k4	14
letech	let	k1gInPc6	let
a	a	k8xC	a
v	v	k7c6	v
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
Je	být	k5eAaImIp3nS	být
teoretickým	teoretický	k2eAgMnSc7d1	teoretický
fyzikem	fyzik	k1gMnSc7	fyzik
zkoumajícím	zkoumající	k2eAgFnPc3d1	zkoumající
kvantovou	kvantový	k2eAgFnSc4d1	kvantová
mechaniku	mechanika	k1gFnSc4	mechanika
a	a	k8xC	a
teorii	teorie	k1gFnSc4	teorie
strun	struna	k1gFnPc2	struna
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
tituly	titul	k1gInPc4	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
a	a	k8xC	a
Sc	Sc	k1gMnSc1	Sc
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
a	a	k8xC	a
také	také	k9	také
má	mít	k5eAaImIp3nS	mít
IQ	iq	kA	iq
187	[number]	k4	187
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
předvádí	předvádět	k5eAaImIp3nS	předvádět
typické	typický	k2eAgNnSc1d1	typické
dodržování	dodržování	k1gNnSc1	dodržování
rutiny	rutina	k1gFnSc2	rutina
a	a	k8xC	a
zvyků	zvyk	k1gInPc2	zvyk
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
problém	problém	k1gInSc1	problém
s	s	k7c7	s
pochopením	pochopení	k1gNnSc7	pochopení
sarkasmu	sarkasmus	k1gInSc2	sarkasmus
a	a	k8xC	a
ironie	ironie	k1gFnSc2	ironie
<g/>
,	,	kIx,	,
také	také	k9	také
má	mít	k5eAaImIp3nS	mít
problém	problém	k1gInSc4	problém
pochopit	pochopit	k5eAaPmF	pochopit
romantické	romantický	k2eAgInPc4d1	romantický
vztahy	vztah	k1gInPc4	vztah
svých	svůj	k3xOyFgMnPc2	svůj
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Sheldon	Sheldon	k1gInSc1	Sheldon
sdílí	sdílet	k5eAaImIp3nS	sdílet
byt	byt	k1gInSc4	byt
s	s	k7c7	s
Leonardem	Leonardo	k1gMnSc7	Leonardo
Hofstadterem	Hofstadter	k1gMnSc7	Hofstadter
<g/>
,	,	kIx,	,
bydlí	bydlet	k5eAaImIp3nS	bydlet
naproti	naproti	k6eAd1	naproti
přes	přes	k7c4	přes
chodbu	chodba	k1gFnSc4	chodba
od	od	k7c2	od
Penny	penny	k1gInSc2	penny
a	a	k8xC	a
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
spoléhá	spoléhat	k5eAaImIp3nS	spoléhat
při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
společenských	společenský	k2eAgFnPc2d1	společenská
situací	situace	k1gFnPc2	situace
a	a	k8xC	a
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Sheldon	Sheldon	k1gInSc1	Sheldon
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
sobecký	sobecký	k2eAgMnSc1d1	sobecký
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
často	často	k6eAd1	často
chlubí	chlubit	k5eAaImIp3nS	chlubit
svojí	svůj	k3xOyFgFnSc7	svůj
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc7d1	vysoká
inteligencí	inteligence	k1gFnSc7	inteligence
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
podceňuje	podceňovat	k5eAaImIp3nS	podceňovat
některé	některý	k3yIgInPc4	některý
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Sheldon	Sheldon	k1gInSc1	Sheldon
spoléhá	spoléhat	k5eAaImIp3nS	spoléhat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
některý	některý	k3yIgMnSc1	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
přátel	přítel	k1gMnPc2	přítel
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
Leonard	Leonard	k1gInSc1	Leonard
<g/>
)	)	kIx)	)
jej	on	k3xPp3gNnSc4	on
odveze	odvézt	k5eAaPmIp3nS	odvézt
někam	někam	k6eAd1	někam
autem	auto	k1gNnSc7	auto
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
sérii	série	k1gFnSc6	série
zkusil	zkusit	k5eAaPmAgMnS	zkusit
neúspěšně	úspěšně	k6eNd1	úspěšně
získat	získat	k5eAaPmF	získat
řidičský	řidičský	k2eAgInSc4d1	řidičský
průkaz	průkaz	k1gInSc4	průkaz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
sérii	série	k1gFnSc6	série
poznal	poznat	k5eAaPmAgMnS	poznat
Amy	Amy	k1gMnSc1	Amy
Farrah	Farrah	k1gMnSc1	Farrah
Fowlerovou	Fowlerová	k1gFnSc4	Fowlerová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
později	pozdě	k6eAd2	pozdě
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
vztah	vztah	k1gInSc4	vztah
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
strachem	strach	k1gInSc7	strach
z	z	k7c2	z
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
z	z	k7c2	z
fyzického	fyzický	k2eAgInSc2d1	fyzický
kontaktu	kontakt	k1gInSc2	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
introvertní	introvertní	k2eAgMnSc1d1	introvertní
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
jiných	jiný	k2eAgFnPc2d1	jiná
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
malé	malý	k2eAgNnSc1d1	malé
dítě	dítě	k1gNnSc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
ritualizovaný	ritualizovaný	k2eAgMnSc1d1	ritualizovaný
a	a	k8xC	a
trpí	trpět	k5eAaImIp3nS	trpět
obsesí	obsese	k1gFnSc7	obsese
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
vidět	vidět	k5eAaImF	vidět
věci	věc	k1gFnPc1	věc
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
sedět	sedět	k5eAaImF	sedět
stále	stále	k6eAd1	stále
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
pohovce	pohovka	k1gFnSc6	pohovka
či	či	k8xC	či
klepat	klepat	k5eAaImF	klepat
na	na	k7c4	na
dveře	dveře	k1gFnPc4	dveře
třikrát	třikrát	k6eAd1	třikrát
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
říkat	říkat	k5eAaImF	říkat
jméno	jméno	k1gNnSc4	jméno
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
klepe	klepat	k5eAaImIp3nS	klepat
<g/>
,	,	kIx,	,
než	než	k8xS	než
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
vlastně	vlastně	k9	vlastně
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Kaley	Kalea	k1gFnPc1	Kalea
Cuoco	Cuoco	k6eAd1	Cuoco
jako	jako	k8xS	jako
Penny	penny	k1gInSc4	penny
(	(	kIx(	(
<g/>
český	český	k2eAgInSc4d1	český
dabing	dabing	k1gInSc4	dabing
Jitka	Jitka	k1gFnSc1	Jitka
Moučková	Moučková	k1gFnSc1	Moučková
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
města	město	k1gNnSc2	město
nedaleko	nedaleko	k7c2	nedaleko
Omahy	Omaha	k1gFnSc2	Omaha
v	v	k7c6	v
Nebrasce	Nebraska	k1gFnSc6	Nebraska
<g/>
.	.	kIx.	.
</s>
<s>
Penny	penny	k1gFnSc1	penny
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
být	být	k5eAaImF	být
herečkou	herečka	k1gFnSc7	herečka
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
naproti	naproti	k7c3	naproti
bytu	byt	k1gInSc3	byt
Sheldona	Sheldon	k1gMnSc2	Sheldon
a	a	k8xC	a
Leonarda	Leonardo	k1gMnSc2	Leonardo
<g/>
.	.	kIx.	.
</s>
<s>
Penny	penny	k1gFnSc1	penny
zkoušela	zkoušet	k5eAaImAgFnS	zkoušet
získat	získat	k5eAaPmF	získat
několik	několik	k4yIc4	několik
televizních	televizní	k2eAgFnPc2d1	televizní
rolí	role	k1gFnPc2	role
pomocí	pomocí	k7c2	pomocí
kastingů	kasting	k1gInPc2	kasting
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
prozatím	prozatím	k6eAd1	prozatím
nebyla	být	k5eNaImAgFnS	být
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
snaze	snaha	k1gFnSc6	snaha
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
první	první	k4xOgFnSc2	první
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
sedmé	sedmý	k4xOgFnSc2	sedmý
série	série	k1gFnSc2	série
se	se	k3xPyFc4	se
živila	živit	k5eAaImAgFnS	živit
jako	jako	k9	jako
číšnice	číšnice	k1gFnSc1	číšnice
a	a	k8xC	a
občasná	občasný	k2eAgFnSc1d1	občasná
barmanka	barmanka	k1gFnSc1	barmanka
v	v	k7c6	v
Cheesecake	Cheesecake	k1gFnSc6	Cheesecake
Factory	Factor	k1gInPc4	Factor
<g/>
.	.	kIx.	.
</s>
<s>
Nezískala	získat	k5eNaPmAgFnS	získat
univerzitní	univerzitní	k2eAgInSc4d1	univerzitní
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
mnohem	mnohem	k6eAd1	mnohem
lepší	dobrý	k2eAgInSc4d2	lepší
cit	cit	k1gInSc4	cit
pro	pro	k7c4	pro
situaci	situace	k1gFnSc4	situace
a	a	k8xC	a
sociální	sociální	k2eAgFnSc4d1	sociální
inteligenci	inteligence	k1gFnSc4	inteligence
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc1d1	ostatní
hlavní	hlavní	k2eAgMnPc1d1	hlavní
hrdinové	hrdina	k1gMnPc1	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Prozatím	prozatím	k6eAd1	prozatím
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
její	její	k3xOp3gNnSc4	její
příjmení	příjmení	k1gNnSc4	příjmení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
první	první	k4xOgFnSc2	první
série	série	k1gFnSc2	série
a	a	k8xC	a
během	během	k7c2	během
třetí	třetí	k4xOgFnSc2	třetí
<g/>
,	,	kIx,	,
páté	pátý	k4xOgNnSc4	pátý
a	a	k8xC	a
šesté	šestý	k4xOgFnSc2	šestý
série	série	k1gFnSc2	série
chodila	chodit	k5eAaImAgFnS	chodit
s	s	k7c7	s
Leonardem	Leonardo	k1gMnSc7	Leonardo
Hofstadterem	Hofstadter	k1gMnSc7	Hofstadter
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
zasnoubila	zasnoubit	k5eAaPmAgFnS	zasnoubit
a	a	k8xC	a
vdala	vdát	k5eAaPmAgFnS	vdát
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
sezóny	sezóna	k1gFnSc2	sezóna
se	se	k3xPyFc4	se
spřátelila	spřátelit	k5eAaPmAgFnS	spřátelit
s	s	k7c7	s
Bernadette	Bernadett	k1gInSc5	Bernadett
a	a	k8xC	a
Amy	Amy	k1gFnPc7	Amy
a	a	k8xC	a
scházejí	scházet	k5eAaImIp3nP	scházet
se	se	k3xPyFc4	se
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
či	či	k8xC	či
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
prostorách	prostora	k1gFnPc6	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Simon	Simon	k1gMnSc1	Simon
Helberg	Helberg	k1gMnSc1	Helberg
jako	jako	k8xC	jako
Howard	Howard	k1gMnSc1	Howard
Joel	Joel	k1gMnSc1	Joel
Wolowitz	Wolowitz	k1gMnSc1	Wolowitz
<g/>
,	,	kIx,	,
<g/>
M.	M.	kA	M.
<g/>
Eng	Eng	k1gFnSc1	Eng
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
český	český	k2eAgInSc4d1	český
dabing	dabing	k1gInSc4	dabing
Matěj	Matěj	k1gMnSc1	Matěj
Hádek	hádek	k1gMnSc1	hádek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
letecký	letecký	k2eAgMnSc1d1	letecký
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
židovského	židovský	k2eAgNnSc2d1	Židovské
vyznání	vyznání	k1gNnSc2	vyznání
a	a	k8xC	a
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
žil	žít	k5eAaImAgMnS	žít
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jediný	jediný	k2eAgMnSc1d1	jediný
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
hrdinů	hrdina	k1gMnPc2	hrdina
nezískal	získat	k5eNaPmAgInS	získat
doktorský	doktorský	k2eAgInSc1d1	doktorský
titul	titul	k1gInSc1	titul
<g/>
,	,	kIx,	,
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
obranu	obrana	k1gFnSc4	obrana
často	často	k6eAd1	často
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
získal	získat	k5eAaPmAgMnS	získat
magisterský	magisterský	k2eAgInSc4d1	magisterský
titul	titul	k1gInSc4	titul
z	z	k7c2	z
prestižní	prestižní	k2eAgFnSc2d1	prestižní
univerzity	univerzita	k1gFnSc2	univerzita
MIT	MIT	kA	MIT
a	a	k8xC	a
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
vysílány	vysílat	k5eAaImNgInP	vysílat
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
abstraktní	abstraktní	k2eAgFnSc3d1	abstraktní
práci	práce	k1gFnSc3	práce
ostatních	ostatní	k2eAgMnPc2d1	ostatní
hrdinů	hrdina	k1gMnPc2	hrdina
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
díle	dílo	k1gNnSc6	dílo
5	[number]	k4	5
<g/>
.	.	kIx.	.
série	série	k1gFnSc2	série
letěl	letět	k5eAaImAgMnS	letět
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
předvádí	předvádět	k5eAaImIp3nS	předvádět
židovský	židovský	k2eAgInSc4d1	židovský
humor	humor	k1gInSc4	humor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Leonard	Leonard	k1gInSc1	Leonard
a	a	k8xC	a
Raj	Raj	k1gFnSc1	Raj
shledávají	shledávat	k5eAaImIp3nP	shledávat
přehnaný	přehnaný	k2eAgInSc4d1	přehnaný
či	či	k8xC	či
neopodstatněný	opodstatněný	k2eNgInSc4d1	neopodstatněný
<g/>
.	.	kIx.	.
</s>
<s>
Prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
polyglot	polyglot	k1gMnSc1	polyglot
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
mluví	mluvit	k5eAaImIp3nS	mluvit
šesti	šest	k4xCc7	šest
jazyky	jazyk	k1gInPc7	jazyk
<g/>
:	:	kIx,	:
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
,	,	kIx,	,
mandarínsky	mandarínsky	k6eAd1	mandarínsky
<g/>
,	,	kIx,	,
persky	persky	k6eAd1	persky
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
a	a	k8xC	a
klingonsky	klingonsky	k6eAd1	klingonsky
<g/>
.	.	kIx.	.
</s>
<s>
Chodil	chodit	k5eAaImAgMnS	chodit
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
Bernadette	Bernadett	k1gInSc5	Bernadett
Rostenkowski	Rostenkowsk	k1gMnSc6	Rostenkowsk
a	a	k8xC	a
v	v	k7c6	v
šesté	šestý	k4xOgFnSc6	šestý
sérii	série	k1gFnSc6	série
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
vlastního	vlastní	k2eAgInSc2d1	vlastní
bytu	byt	k1gInSc2	byt
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
po	po	k7c6	po
nějakém	nějaký	k3yIgInSc6	nějaký
čase	čas	k1gInSc6	čas
Bernadette	Bernadett	k1gInSc5	Bernadett
otěhotní	otěhotnět	k5eAaPmIp3nS	otěhotnět
<g/>
.	.	kIx.	.
</s>
<s>
Kunal	Kunal	k1gMnSc1	Kunal
Nayyar	Nayyar	k1gMnSc1	Nayyar
jako	jako	k8xS	jako
Rajesh	Rajesh	k1gMnSc1	Rajesh
Ramajan	Ramajan	k1gInSc4	Ramajan
Koothrappali	Koothrappali	k1gFnSc2	Koothrappali
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
(	(	kIx(	(
<g/>
český	český	k2eAgInSc4d1	český
dabing	dabing	k1gInSc4	dabing
Petr	Petr	k1gMnSc1	Petr
Burian	Burian	k1gMnSc1	Burian
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Nového	Nového	k2eAgNnSc2d1	Nového
Dillí	Dillí	k1gNnSc2	Dillí
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
částicový	částicový	k2eAgMnSc1d1	částicový
astrofyzik	astrofyzik	k1gMnSc1	astrofyzik
na	na	k7c6	na
Caltechu	Caltech	k1gInSc6	Caltech
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
je	být	k5eAaImIp3nS	být
gynekolog	gynekolog	k1gMnSc1	gynekolog
a	a	k8xC	a
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
,,	,,	k?	,,
<g/>
ve	v	k7c6	v
vatě	vata	k1gFnSc6	vata
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
stydlivý	stydlivý	k2eAgInSc1d1	stydlivý
v	v	k7c6	v
komunikaci	komunikace	k1gFnSc6	komunikace
s	s	k7c7	s
ženami	žena	k1gFnPc7	žena
a	a	k8xC	a
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
šesti	šest	k4xCc6	šest
sériích	série	k1gFnPc6	série
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
promluvit	promluvit	k5eAaPmF	promluvit
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
své	svůj	k3xOyFgFnSc2	svůj
sestry	sestra	k1gFnSc2	sestra
a	a	k8xC	a
matky	matka	k1gFnSc2	matka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mluvit	mluvit	k5eAaImF	mluvit
se	s	k7c7	s
ženami	žena	k1gFnPc7	žena
dokázal	dokázat	k5eAaPmAgMnS	dokázat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
pil	pít	k5eAaImAgMnS	pít
alkohol	alkohol	k1gInSc4	alkohol
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
aspoň	aspoň	k9	aspoň
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
pil	pít	k5eAaImAgMnS	pít
alkohol	alkohol	k1gInSc4	alkohol
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
užíval	užívat	k5eAaImAgInS	užívat
experimentální	experimentální	k2eAgInPc4d1	experimentální
medikamenty	medikament	k1gInPc4	medikament
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
však	však	k9	však
měl	mít	k5eAaImAgMnS	mít
větší	veliký	k2eAgInPc4d2	veliký
úspěchy	úspěch	k1gInPc4	úspěch
s	s	k7c7	s
ženami	žena	k1gFnPc7	žena
než	než	k8xS	než
jeho	jeho	k3xOp3gMnSc1	jeho
přespříliš	přespříliš	k6eAd1	přespříliš
sebevědomý	sebevědomý	k2eAgMnSc1d1	sebevědomý
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
přítel	přítel	k1gMnSc1	přítel
Howard	Howard	k1gMnSc1	Howard
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
epizodě	epizoda	k1gFnSc6	epizoda
šesté	šestý	k4xOgFnSc2	šestý
série	série	k1gFnSc2	série
překonal	překonat	k5eAaPmAgMnS	překonat
svůj	svůj	k3xOyFgInSc4	svůj
ostych	ostych	k1gInSc4	ostych
v	v	k7c6	v
komunikaci	komunikace	k1gFnSc6	komunikace
s	s	k7c7	s
ženami	žena	k1gFnPc7	žena
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
dokáže	dokázat	k5eAaPmIp3nS	dokázat
mluvit	mluvit	k5eAaImF	mluvit
se	s	k7c7	s
ženami	žena	k1gFnPc7	žena
bez	bez	k7c2	bez
užívání	užívání	k1gNnSc2	užívání
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
zženštilý	zženštilý	k2eAgInSc1d1	zženštilý
a	a	k8xC	a
často	často	k6eAd1	často
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
bere	brát	k5eAaImIp3nS	brát
typicky	typicky	k6eAd1	typicky
ženskou	ženský	k2eAgFnSc4d1	ženská
roli	role	k1gFnSc4	role
v	v	k7c6	v
přátelství	přátelství	k1gNnSc6	přátelství
s	s	k7c7	s
Howardem	Howard	k1gInSc7	Howard
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
si	se	k3xPyFc3	se
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
homosexuál	homosexuál	k1gMnSc1	homosexuál
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
sezóny	sezóna	k1gFnSc2	sezóna
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
sdílela	sdílet	k5eAaImAgFnS	sdílet
byt	byt	k1gInSc4	byt
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
Priya	Priyum	k1gNnSc2	Priyum
(	(	kIx(	(
<g/>
Aarti	Aarť	k1gFnSc2	Aarť
Mann	Mann	k1gMnSc1	Mann
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sérii	série	k1gFnSc6	série
se	se	k3xPyFc4	se
Priya	Priya	k1gFnSc1	Priya
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
Leonardem	Leonardo	k1gMnSc7	Leonardo
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc7	jeho
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
(	(	kIx(	(
<g/>
ač	ač	k8xS	ač
to	ten	k3xDgNnSc1	ten
Rajovi	Raja	k1gMnSc3	Raja
vadilo	vadit	k5eAaImAgNnS	vadit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
šesté	šestý	k4xOgFnSc2	šestý
série	série	k1gFnSc2	série
"	"	kIx"	"
<g/>
Nesporný	sporný	k2eNgInSc1d1	nesporný
důkaz	důkaz	k1gInSc1	důkaz
náklonnosti	náklonnost	k1gFnSc2	náklonnost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Tangible	Tangible	k1gFnSc1	Tangible
Affection	Affection	k1gInSc4	Affection
Proof	Proof	k1gInSc1	Proof
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Raj	Raj	k1gMnSc1	Raj
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
s	s	k7c7	s
komiksy	komiks	k1gInPc7	komiks
potkal	potkat	k5eAaPmAgInS	potkat
Lucy	Lucy	k1gInPc4	Lucy
(	(	kIx(	(
<g/>
Kate	kat	k1gMnSc5	kat
Micucci	Micucc	k1gMnSc5	Micucc
<g/>
)	)	kIx)	)
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
Bon	bon	k1gInSc4	bon
Voyage	Voyag	k1gFnSc2	Voyag
<g/>
"	"	kIx"	"
Lucy	Luca	k1gFnSc2	Luca
ukončila	ukončit	k5eAaPmAgFnS	ukončit
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Rajem	Raj	k1gInSc7	Raj
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
příliš	příliš	k6eAd1	příliš
stresována	stresovat	k5eAaImNgFnS	stresovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
chce	chtít	k5eAaImIp3nS	chtít
představit	představit	k5eAaPmF	představit
svým	svůj	k3xOyFgMnPc3	svůj
přátelům	přítel	k1gMnPc3	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
této	tento	k3xDgFnSc2	tento
epizody	epizoda	k1gFnSc2	epizoda
Raj	Raj	k1gFnSc4	Raj
promluvil	promluvit	k5eAaPmAgMnS	promluvit
s	s	k7c7	s
Penny	penny	k1gInSc7	penny
bez	bez	k7c2	bez
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Sara	Sar	k2eAgFnSc1d1	Sara
Gilbertová	Gilbertová	k1gFnSc1	Gilbertová
jako	jako	k8xS	jako
Leslie	Leslie	k1gFnSc1	Leslie
Winkle	Winkl	k1gInSc5	Winkl
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
(	(	kIx(	(
<g/>
český	český	k2eAgInSc4d1	český
dabing	dabing	k1gInSc4	dabing
Lucie	Lucie	k1gFnSc1	Lucie
Juřičková	Juřičková	k1gFnSc1	Juřičková
<g/>
,	,	kIx,	,
vracející	vracející	k2eAgFnSc1d1	vracející
se	se	k3xPyFc4	se
postava	postava	k1gFnSc1	postava
v	v	k7c6	v
sérii	série	k1gFnSc6	série
1	[number]	k4	1
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
v	v	k7c6	v
sérii	série	k1gFnSc6	série
2	[number]	k4	2
<g/>
,	,	kIx,	,
vracející	vracející	k2eAgMnSc1d1	vracející
se	se	k3xPyFc4	se
v	v	k7c6	v
sérii	série	k1gFnSc6	série
3	[number]	k4	3
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fyzikem	fyzik	k1gMnSc7	fyzik
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
laboratoři	laboratoř	k1gFnSc6	laboratoř
jako	jako	k9	jako
Leonard	Leonard	k1gMnSc1	Leonard
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
hraje	hrát	k5eAaImIp3nS	hrát
Leonardův	Leonardův	k2eAgInSc1d1	Leonardův
ženský	ženský	k2eAgInSc1d1	ženský
protějšek	protějšek	k1gInSc1	protějšek
<g/>
,	,	kIx,	,
nosí	nosit	k5eAaImIp3nS	nosit
brýle	brýle	k1gFnPc4	brýle
s	s	k7c7	s
černými	černý	k2eAgFnPc7d1	černá
obroučkami	obroučka	k1gFnPc7	obroučka
a	a	k8xC	a
pletené	pletený	k2eAgInPc4d1	pletený
kostýmky	kostýmek	k1gInPc4	kostýmek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ostře	ostro	k6eAd1	ostro
vyhraněna	vyhraněn	k2eAgFnSc1d1	vyhraněna
vůči	vůči	k7c3	vůči
Sheldonovi	Sheldon	k1gMnSc3	Sheldon
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
jiného	jiný	k2eAgInSc2d1	jiný
náhledu	náhled	k1gInSc2	náhled
na	na	k7c4	na
vědecké	vědecký	k2eAgInPc4d1	vědecký
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Navzájem	navzájem	k6eAd1	navzájem
si	se	k3xPyFc3	se
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
intelektuálně	intelektuálně	k6eAd1	intelektuálně
výš	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
než	než	k8xS	než
ten	ten	k3xDgInSc1	ten
druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
Leslie	Leslie	k1gFnSc1	Leslie
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
mnohem	mnohem	k6eAd1	mnohem
vtipnější	vtipný	k2eAgMnSc1d2	vtipnější
a	a	k8xC	a
pohotovější	pohotový	k2eAgMnSc1d2	pohotovější
než	než	k8xS	než
Sheldon	Sheldon	k1gMnSc1	Sheldon
a	a	k8xC	a
často	často	k6eAd1	často
ho	on	k3xPp3gMnSc4	on
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k8xC	jako
blbce	blbec	k1gMnPc4	blbec
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
dumbass	dumbass	k6eAd1	dumbass
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Leslie	Leslie	k1gFnSc1	Leslie
měla	mít	k5eAaImAgFnS	mít
přátelský	přátelský	k2eAgInSc4d1	přátelský
a	a	k8xC	a
i	i	k9	i
sexuální	sexuální	k2eAgInSc1d1	sexuální
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
Leonardem	Leonardo	k1gMnSc7	Leonardo
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
Howardem	Howard	k1gInSc7	Howard
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vztahu	vztah	k1gInSc2	vztah
s	s	k7c7	s
Leonardem	Leonardo	k1gMnSc7	Leonardo
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
vztahu	vztah	k1gInSc2	vztah
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
spolu	spolu	k6eAd1	spolu
měli	mít	k5eAaImAgMnP	mít
jejich	jejich	k3xOp3gFnPc4	jejich
postavy	postava	k1gFnPc4	postava
Darlene	Darlen	k1gInSc5	Darlen
Connorové	Connorové	k2eAgMnSc2d1	Connorové
a	a	k8xC	a
Davida	David	k1gMnSc2	David
Healyho	Healy	k1gMnSc2	Healy
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Roseanne	Roseann	k1gInSc5	Roseann
<g/>
.	.	kIx.	.
</s>
<s>
Gilbertová	Gilbertová	k1gFnSc1	Gilbertová
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
sérii	série	k1gFnSc6	série
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
zpět	zpět	k6eAd1	zpět
mezi	mezi	k7c4	mezi
postavy	postava	k1gFnPc4	postava
vracející	vracející	k2eAgFnPc4d1	vracející
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
autoři	autor	k1gMnPc1	autor
seriálu	seriál	k1gInSc2	seriál
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
generovat	generovat	k5eAaImF	generovat
dostatek	dostatek	k1gInSc4	dostatek
obsahu	obsah	k1gInSc2	obsah
pro	pro	k7c4	pro
její	její	k3xOp3gFnSc4	její
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Gilbertová	Gilbertová	k1gFnSc1	Gilbertová
odešla	odejít	k5eAaPmAgFnS	odejít
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
po	po	k7c4	po
třetí	třetí	k4xOgFnSc4	třetí
sérii	série	k1gFnSc4	série
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
větší	veliký	k2eAgFnSc2d2	veliký
zainteresovanosti	zainteresovanost	k1gFnSc2	zainteresovanost
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
The	The	k1gMnSc1	The
Talk	Talk	k1gMnSc1	Talk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
výkonný	výkonný	k2eAgMnSc1d1	výkonný
producent	producent	k1gMnSc1	producent
pro	pro	k7c4	pro
CBS	CBS	kA	CBS
<g/>
.	.	kIx.	.
</s>
<s>
Melissa	Melissa	k1gFnSc1	Melissa
Rauchová	Rauchová	k1gFnSc1	Rauchová
jako	jako	k8xS	jako
Bernadette	Bernadett	k1gInSc5	Bernadett
Rostenkowski-Wolowitz	Rostenkowski-Wolowitz	k1gMnSc1	Rostenkowski-Wolowitz
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
(	(	kIx(	(
<g/>
český	český	k2eAgInSc4d1	český
dabing	dabing	k1gInSc4	dabing
Terezie	Terezie	k1gFnSc2	Terezie
Taberyová	Taberyová	k1gFnSc1	Taberyová
<g/>
,	,	kIx,	,
vracející	vracející	k2eAgFnSc1d1	vracející
se	se	k3xPyFc4	se
postava	postava	k1gFnSc1	postava
v	v	k7c6	v
sérii	série	k1gFnSc6	série
3	[number]	k4	3
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
od	od	k7c2	od
série	série	k1gFnSc2	série
4	[number]	k4	4
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mladá	mladý	k2eAgFnSc1d1	mladá
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zpočátku	zpočátku	k6eAd1	zpočátku
působila	působit	k5eAaImAgFnS	působit
jako	jako	k9	jako
servírka	servírka	k1gFnSc1	servírka
a	a	k8xC	a
kolegyně	kolegyně	k1gFnSc1	kolegyně
Penny	penny	k1gFnSc1	penny
<g/>
,	,	kIx,	,
tímto	tento	k3xDgNnSc7	tento
zaměstnáním	zaměstnání	k1gNnSc7	zaměstnání
si	se	k3xPyFc3	se
přivydělávala	přivydělávat	k5eAaImAgFnS	přivydělávat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
doktorského	doktorský	k2eAgNnSc2d1	doktorské
studia	studio	k1gNnSc2	studio
mikrobiologie	mikrobiologie	k1gFnSc2	mikrobiologie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
série	série	k1gFnSc2	série
obhájila	obhájit	k5eAaPmAgFnS	obhájit
svoji	svůj	k3xOyFgFnSc4	svůj
dizertaci	dizertace	k1gFnSc4	dizertace
a	a	k8xC	a
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
na	na	k7c4	na
vysoce	vysoce	k6eAd1	vysoce
placené	placený	k2eAgNnSc4d1	placené
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Penny	penny	k1gFnSc1	penny
představila	představit	k5eAaPmAgFnS	představit
Bernadette	Bernadett	k1gInSc5	Bernadett
Howardovi	Howard	k1gMnSc3	Howard
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
neměla	mít	k5eNaImAgFnS	mít
o	o	k7c4	o
Howarda	Howard	k1gMnSc4	Howard
vůbec	vůbec	k9	vůbec
žádný	žádný	k3yNgInSc4	žádný
zájem	zájem	k1gInSc4	zájem
a	a	k8xC	a
prohlašovala	prohlašovat	k5eAaImAgFnS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
nemají	mít	k5eNaImIp3nP	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zjištění	zjištění	k1gNnSc6	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
měli	mít	k5eAaImAgMnP	mít
podobné	podobný	k2eAgInPc4d1	podobný
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
se	se	k3xPyFc4	se
sblížili	sblížit	k5eAaPmAgMnP	sblížit
<g/>
,	,	kIx,	,
během	během	k7c2	během
třetí	třetí	k4xOgFnSc2	třetí
série	série	k1gFnSc2	série
spolu	spolu	k6eAd1	spolu
začali	začít	k5eAaPmAgMnP	začít
chodit	chodit	k5eAaImF	chodit
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
rozešli	rozejít	k5eAaPmAgMnP	rozejít
<g/>
,	,	kIx,	,
ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
sérii	série	k1gFnSc6	série
se	se	k3xPyFc4	se
k	k	k7c3	k
sobě	se	k3xPyFc3	se
ovšem	ovšem	k9	ovšem
vrátili	vrátit	k5eAaPmAgMnP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
série	série	k1gFnSc2	série
se	se	k3xPyFc4	se
zasnoubili	zasnoubit	k5eAaPmAgMnP	zasnoubit
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
páté	pátý	k4xOgFnSc2	pátý
série	série	k1gFnSc2	série
se	se	k3xPyFc4	se
vzali	vzít	k5eAaPmAgMnP	vzít
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
zanedlouho	zanedlouho	k6eAd1	zanedlouho
Bernadette	Bernadett	k1gInSc5	Bernadett
otěhotněla	otěhotnět	k5eAaPmAgFnS	otěhotnět
<g/>
.	.	kIx.	.
</s>
<s>
Stuarta	Stuarta	k1gFnSc1	Stuarta
Blooma	Bloomum	k1gNnSc2	Bloomum
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
hraje	hrát	k5eAaImIp3nS	hrát
Kevin	Kevin	k1gMnSc1	Kevin
Sussman	Sussman	k1gMnSc1	Sussman
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
nadaboval	nadabovat	k5eAaPmAgMnS	nadabovat
Pavel	Pavel	k1gMnSc1	Pavel
Vondrák	Vondrák	k1gMnSc1	Vondrák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stuart	Stuart	k1gInSc1	Stuart
provozuje	provozovat	k5eAaImIp3nS	provozovat
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
komiksy	komiks	k1gInPc7	komiks
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Pasadeny	Pasaden	k2eAgInPc1d1	Pasaden
<g/>
.	.	kIx.	.
</s>
<s>
Stuart	Stuart	k1gInSc1	Stuart
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
svým	svůj	k3xOyFgNnSc7	svůj
nízkým	nízký	k2eAgNnSc7d1	nízké
sebevědomím	sebevědomí	k1gNnSc7	sebevědomí
a	a	k8xC	a
osamělostí	osamělost	k1gFnSc7	osamělost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
ubohé	ubohý	k2eAgInPc1d1	ubohý
pokusy	pokus	k1gInPc1	pokus
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
zapojit	zapojit	k5eAaPmF	zapojit
do	do	k7c2	do
rozhovoru	rozhovor	k1gInSc2	rozhovor
s	s	k7c7	s
ženami	žena	k1gFnPc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
je	být	k5eAaImIp3nS	být
talentovaný	talentovaný	k2eAgMnSc1d1	talentovaný
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
chodil	chodit	k5eAaImAgMnS	chodit
na	na	k7c6	na
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
School	School	k1gInSc1	School
of	of	k?	of
Design	design	k1gInSc1	design
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
má	mít	k5eAaImIp3nS	mít
téměř	téměř	k6eAd1	téměř
encyklopedické	encyklopedický	k2eAgFnPc4d1	encyklopedická
znalosti	znalost	k1gFnPc4	znalost
o	o	k7c6	o
komiksech	komiks	k1gInPc6	komiks
a	a	k8xC	a
superhrdinech	superhrdin	k1gInPc6	superhrdin
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
<g/>
,	,	kIx,	,
přátelé	přítel	k1gMnPc1	přítel
přivedli	přivést	k5eAaPmAgMnP	přivést
Penny	penny	k1gFnSc4	penny
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
ji	on	k3xPp3gFnSc4	on
pozval	pozvat	k5eAaPmAgMnS	pozvat
na	na	k7c4	na
rande	rande	k1gNnSc4	rande
a	a	k8xC	a
Penny	penny	k1gNnSc4	penny
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Rande	rande	k1gNnSc1	rande
by	by	kYmCp3nS	by
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
Stuart	Stuart	k1gInSc1	Stuart
nepožádal	požádat	k5eNaPmAgInS	požádat
o	o	k7c4	o
radu	rada	k1gMnSc4	rada
Sheldona	Sheldon	k1gMnSc4	Sheldon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
poradil	poradit	k5eAaPmAgMnS	poradit
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
Penny	penny	k1gFnPc1	penny
nudí	nudit	k5eAaImIp3nP	nudit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
druhém	druhý	k4xOgInSc6	druhý
rande	rande	k1gNnSc6	rande
Stuart	Stuarta	k1gFnPc2	Stuarta
požádal	požádat	k5eAaPmAgInS	požádat
o	o	k7c4	o
radu	rada	k1gFnSc4	rada
Leonarda	Leonardo	k1gMnSc2	Leonardo
jakožto	jakožto	k8xS	jakožto
bývalého	bývalý	k2eAgMnSc2d1	bývalý
přítele	přítel	k1gMnSc2	přítel
Penny	penny	k1gNnSc2	penny
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
mu	on	k3xPp3gMnSc3	on
záměrně	záměrně	k6eAd1	záměrně
poradil	poradit	k5eAaPmAgMnS	poradit
špatně	špatně	k6eAd1	špatně
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
Stuart	Stuart	k1gInSc1	Stuart
zachoval	zachovat	k5eAaPmAgInS	zachovat
přesně	přesně	k6eAd1	přesně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
neměl	mít	k5eNaImAgMnS	mít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
páté	pátý	k4xOgFnSc6	pátý
řadě	řada	k1gFnSc6	řada
seriálu	seriál	k1gInSc2	seriál
pak	pak	k6eAd1	pak
šel	jít	k5eAaImAgMnS	jít
na	na	k7c6	na
rande	rande	k1gNnSc6	rande
s	s	k7c7	s
Amy	Amy	k1gFnSc7	Amy
(	(	kIx(	(
<g/>
Sheldonovou	Sheldonový	k2eAgFnSc7d1	Sheldonový
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
jim	on	k3xPp3gMnPc3	on
překazil	překazit	k5eAaPmAgInS	překazit
Sheldon	Sheldon	k1gInSc4	Sheldon
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
upevnil	upevnit	k5eAaPmAgInS	upevnit
svůj	svůj	k3xOyFgInSc4	svůj
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Amy	Amy	k1gFnSc7	Amy
<g/>
.	.	kIx.	.
</s>
<s>
Stuart	Stuart	k1gInSc1	Stuart
byl	být	k5eAaImAgInS	být
také	také	k9	také
spoluhráčem	spoluhráč	k1gMnSc7	spoluhráč
Willa	Will	k1gMnSc2	Will
Wheatona	Wheaton	k1gMnSc2	Wheaton
v	v	k7c6	v
karetním	karetní	k2eAgInSc6d1	karetní
turnaji	turnaj	k1gInSc6	turnaj
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
porazili	porazit	k5eAaPmAgMnP	porazit
dvojici	dvojice	k1gFnSc4	dvojice
Raje	Raj	k1gMnSc2	Raj
a	a	k8xC	a
Sheldona	Sheldon	k1gMnSc2	Sheldon
<g/>
.	.	kIx.	.
</s>
<s>
Amy	Amy	k?	Amy
Farrah	Farrah	k1gMnSc1	Farrah
Fowler	Fowler	k1gMnSc1	Fowler
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
zastoupila	zastoupit	k5eAaPmAgFnS	zastoupit
Mayim	Mayim	k1gInSc4	Mayim
Bialik	Bialika	k1gFnPc2	Bialika
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
dabovala	dabovat	k5eAaBmAgFnS	dabovat
Nikola	Nikola	k1gFnSc1	Nikola
Votočková	Votočková	k1gFnSc1	Votočková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Amy	Amy	k?	Amy
je	být	k5eAaImIp3nS	být
neurobioložka	neurobioložka	k1gFnSc1	neurobioložka
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
a	a	k8xC	a
studovala	studovat	k5eAaImAgFnS	studovat
na	na	k7c6	na
Harvardu	Harvard	k1gInSc6	Harvard
<g/>
.	.	kIx.	.
</s>
<s>
Nosí	nosit	k5eAaImIp3nP	nosit
brýle	brýle	k1gFnPc4	brýle
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
rozpuštěné	rozpuštěný	k2eAgInPc4d1	rozpuštěný
vlasy	vlas	k1gInPc4	vlas
a	a	k8xC	a
hlídá	hlídat	k5eAaImIp3nS	hlídat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
aby	aby	k9	aby
svým	svůj	k3xOyFgNnSc7	svůj
oblékáním	oblékání	k1gNnSc7	oblékání
nevyšla	vyjít	k5eNaPmAgFnS	vyjít
z	z	k7c2	z
"	"	kIx"	"
<g/>
komfortní	komfortní	k2eAgFnSc2d1	komfortní
zóny	zóna	k1gFnSc2	zóna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
povahově	povahově	k6eAd1	povahově
a	a	k8xC	a
intelektuálně	intelektuálně	k6eAd1	intelektuálně
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
Sheldonovi	Sheldon	k1gMnSc3	Sheldon
<g/>
,	,	kIx,	,
nesdílí	sdílet	k5eNaImIp3nS	sdílet
některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
zájmů	zájem	k1gInPc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
především	především	k9	především
o	o	k7c4	o
zálibu	záliba	k1gFnSc4	záliba
v	v	k7c6	v
superhrdinech	superhrdin	k1gInPc6	superhrdin
a	a	k8xC	a
vláčcích	vláček	k1gInPc6	vláček
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
pro	pro	k7c4	pro
Sheldona	Sheldon	k1gMnSc4	Sheldon
činit	činit	k5eAaImF	činit
kompromisy	kompromis	k1gInPc4	kompromis
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
úchylkách	úchylka	k1gFnPc6	úchylka
naopak	naopak	k6eAd1	naopak
nachází	nacházet	k5eAaImIp3nS	nacházet
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
sblížit	sblížit	k5eAaPmF	sblížit
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
dohodu	dohoda	k1gFnSc4	dohoda
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
že	že	k8xS	že
půjde	jít	k5eAaImIp3nS	jít
alespoň	alespoň	k9	alespoň
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
rok	rok	k1gInSc4	rok
na	na	k7c4	na
rande	rande	k1gNnSc4	rande
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hledání	hledání	k1gNnSc3	hledání
obětí	oběť	k1gFnPc2	oběť
využívala	využívat	k5eAaImAgFnS	využívat
online	onlinout	k5eAaPmIp3nS	onlinout
seznamku	seznamka	k1gFnSc4	seznamka
<g/>
.	.	kIx.	.
</s>
<s>
Osud	osud	k1gInSc1	osud
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
Raj	Raj	k1gFnSc4	Raj
a	a	k8xC	a
Howard	Howard	k1gInSc4	Howard
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
seznamce	seznamka	k1gFnSc6	seznamka
účet	účet	k1gInSc4	účet
Sheldonovi	Sheldonův	k2eAgMnPc1d1	Sheldonův
a	a	k8xC	a
perfektní	perfektní	k2eAgFnSc1d1	perfektní
volba	volba	k1gFnSc1	volba
protějšku	protějšek	k1gInSc2	protějšek
padla	padnout	k5eAaImAgFnS	padnout
na	na	k7c4	na
Amy	Amy	k1gFnSc4	Amy
<g/>
.	.	kIx.	.
</s>
<s>
Vysokému	vysoký	k2eAgMnSc3d1	vysoký
hubenému	hubený	k2eAgMnSc3d1	hubený
doktoru	doktor	k1gMnSc3	doktor
Cooperovi	Cooper	k1gMnSc3	Cooper
dala	dát	k5eAaPmAgFnS	dát
ihned	ihned	k6eAd1	ihned
jasně	jasně	k6eAd1	jasně
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
fyzický	fyzický	k2eAgInSc4d1	fyzický
kontakt	kontakt	k1gInSc4	kontakt
vyloučen	vyloučen	k2eAgInSc1d1	vyloučen
a	a	k8xC	a
ejhle	ejhle	k0	ejhle
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
vztah	vztah	k1gInSc1	vztah
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
již	již	k6eAd1	již
několikátým	několikátý	k4xOyIgInSc7	několikátý
rokem	rok	k1gInSc7	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
se	se	k3xPyFc4	se
také	také	k9	také
objevilo	objevit	k5eAaPmAgNnS	objevit
v	v	k7c6	v
menších	malý	k2eAgFnPc6d2	menší
rolích	role	k1gFnPc6	role
několik	několik	k4yIc4	několik
známějších	známý	k2eAgFnPc2d2	známější
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
sehrály	sehrát	k5eAaPmAgFnP	sehrát
samy	sám	k3xTgFnPc1	sám
sebe	sebe	k3xPyFc4	sebe
(	(	kIx(	(
<g/>
cameo	cameo	k6eAd1	cameo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
Charlie	Charlie	k1gMnSc1	Charlie
Sheen	Sheen	k2eAgMnSc1d1	Sheen
ve	v	k7c4	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
epizodě	epizoda	k1gFnSc6	epizoda
(	(	kIx(	(
<g/>
Griffinova	Griffinův	k2eAgFnSc1d1	Griffinova
ekvivalence	ekvivalence	k1gFnSc1	ekvivalence
<g/>
)	)	kIx)	)
druhé	druhý	k4xOgFnSc2	druhý
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
epizodě	epizoda	k1gFnSc6	epizoda
(	(	kIx(	(
<g/>
Setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
terminátorem	terminátor	k1gInSc7	terminátor
<g/>
)	)	kIx)	)
téže	týž	k3xTgFnSc2	týž
série	série	k1gFnSc2	série
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
Summer	Summer	k1gInSc4	Summer
Glau	Glaus	k1gInSc2	Glaus
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
F.	F.	kA	F.
Smoot	Smoot	k1gInSc1	Smoot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
epizodě	epizoda	k1gFnSc6	epizoda
(	(	kIx(	(
<g/>
Korolár	Korolár	k1gMnSc1	Korolár
hrůzostrašné	hrůzostrašný	k2eAgFnPc4d1	hrůzostrašná
polevy	poleva	k1gFnPc4	poleva
<g/>
)	)	kIx)	)
třetí	třetí	k4xOgFnSc1	třetí
série	série	k1gFnSc1	série
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
Wil	Wil	k1gFnSc3	Wil
Wheaton	Wheaton	k1gInSc4	Wheaton
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
úhlavní	úhlavní	k2eAgMnSc1d1	úhlavní
nepřítel	nepřítel	k1gMnSc1	nepřítel
Sheldona	Sheldon	k1gMnSc2	Sheldon
Coopera	Cooper	k1gMnSc2	Cooper
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
poté	poté	k6eAd1	poté
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
jako	jako	k8xS	jako
vedlejší	vedlejší	k2eAgFnSc1d1	vedlejší
postava	postava	k1gFnSc1	postava
<g/>
.	.	kIx.	.
</s>
<s>
Stan	stan	k1gInSc1	stan
Lee	Lea	k1gFnSc6	Lea
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
epizodě	epizoda	k1gFnSc6	epizoda
(	(	kIx(	(
<g/>
Kauza	kauza	k1gFnSc1	kauza
Excelsior	Excelsiora	k1gFnPc2	Excelsiora
<g/>
)	)	kIx)	)
třetí	třetí	k4xOgFnSc2	třetí
série	série	k1gFnSc2	série
<g/>
.	.	kIx.	.
</s>
<s>
Steve	Steve	k1gMnSc1	Steve
Wozniak	Wozniak	k1gMnSc1	Wozniak
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
epizodě	epizoda	k1gFnSc6	epizoda
(	(	kIx(	(
<g/>
Obohacení	obohacení	k1gNnSc1	obohacení
koštálovou	koštálový	k2eAgFnSc7d1	koštálový
zeleninou	zelenina	k1gFnSc7	zelenina
<g/>
)	)	kIx)	)
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
série	série	k1gFnSc2	série
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
epizodě	epizoda	k1gFnSc6	epizoda
(	(	kIx(	(
<g/>
Archimedův	Archimedův	k2eAgInSc1d1	Archimedův
princip	princip	k1gInSc1	princip
<g/>
)	)	kIx)	)
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
série	série	k1gFnSc2	série
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
epizodě	epizoda	k1gFnSc6	epizoda
(	(	kIx(	(
<g/>
Párové	párový	k2eAgNnSc1d1	párové
dilema	dilema	k1gNnSc1	dilema
<g/>
)	)	kIx)	)
šesté	šestý	k4xOgFnSc2	šestý
série	série	k1gFnSc2	série
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
i	i	k9	i
LeVar	LeVar	k1gInSc1	LeVar
Burton	Burton	k1gInSc1	Burton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
epizodě	epizoda	k1gFnSc3	epizoda
(	(	kIx(	(
<g/>
Následky	následek	k1gInPc4	následek
hypotetické	hypotetický	k2eAgFnSc2d1	hypotetická
katastrofy	katastrofa	k1gFnSc2	katastrofa
<g/>
)	)	kIx)	)
páté	pátá	k1gFnSc2	pátá
série	série	k1gFnSc2	série
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
astronaut	astronaut	k1gMnSc1	astronaut
Michael	Michael	k1gMnSc1	Michael
Massimino	Massimin	k2eAgNnSc4d1	Massimino
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
epizodě	epizoda	k1gFnSc6	epizoda
(	(	kIx(	(
<g/>
Hawkingova	Hawkingův	k2eAgFnSc1d1	Hawkingova
excitace	excitace	k1gFnSc1	excitace
<g/>
)	)	kIx)	)
páté	pátý	k4xOgFnSc2	pátý
série	série	k1gFnSc2	série
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
Stephen	Stephen	k2eAgInSc4d1	Stephen
Hawking	Hawking	k1gInSc4	Hawking
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
poté	poté	k6eAd1	poté
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Wil	Wil	k1gFnSc1	Wil
Wheaton	Wheaton	k1gInSc1	Wheaton
nebo	nebo	k8xC	nebo
Michael	Michael	k1gMnSc1	Michael
Massimino	Massimin	k2eAgNnSc5d1	Massimino
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
epizodě	epizoda	k1gFnSc6	epizoda
(	(	kIx(	(
<g/>
Minimalizace	minimalizace	k1gFnSc1	minimalizace
dopadu	dopad	k1gInSc2	dopad
návratu	návrat	k1gInSc2	návrat
<g/>
)	)	kIx)	)
šesté	šestý	k4xOgFnSc2	šestý
série	série	k1gFnSc2	série
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
Howie	Howie	k1gFnSc2	Howie
Mandel	mandel	k1gInSc1	mandel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
epizodě	epizoda	k1gFnSc6	epizoda
(	(	kIx(	(
<g/>
Holografická	holografický	k2eAgFnSc1d1	holografická
stimulace	stimulace	k1gFnSc1	stimulace
<g/>
)	)	kIx)	)
šesté	šestý	k4xOgFnSc2	šestý
série	série	k1gFnSc2	série
se	se	k3xPyFc4	se
ve	v	k7c6	v
videu	video	k1gNnSc6	video
jakoby	jakoby	k8xS	jakoby
na	na	k7c6	na
YouTube	YouTub	k1gInSc5	YouTub
objeví	objevit	k5eAaPmIp3nS	objevit
druhý	druhý	k4xOgMnSc1	druhý
astronaut	astronaut	k1gMnSc1	astronaut
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
Buzz	Buzz	k1gInSc1	Buzz
Aldrin	aldrin	k1gInSc1	aldrin
<g/>
.	.	kIx.	.
</s>
<s>
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
epizodě	epizoda	k1gFnSc6	epizoda
(	(	kIx(	(
<g/>
Vystřídání	vystřídání	k1gNnSc1	vystřídání
partnerských	partnerský	k2eAgFnPc2d1	partnerská
rolí	role	k1gFnPc2	role
<g/>
)	)	kIx)	)
deváté	devátý	k4xOgFnSc2	devátý
série	série	k1gFnSc2	série
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
9	[number]	k4	9
<g/>
.	.	kIx.	.
epizody	epizoda	k1gFnPc4	epizoda
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Geology	geolog	k1gMnPc7	geolog
Elevation	Elevation	k1gInSc1	Elevation
<g/>
)	)	kIx)	)
desáté	desátá	k1gFnPc1	desátá
série	série	k1gFnSc2	série
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
Ellen	Ellna	k1gFnPc2	Ellna
DeGeneresová	DeGeneresový	k2eAgFnSc1d1	DeGeneresový
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
moderátorka	moderátorka	k1gFnSc1	moderátorka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
dílů	díl	k1gInPc2	díl
seriálu	seriál	k1gInSc2	seriál
Teorie	teorie	k1gFnSc2	teorie
velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Teorie	teorie	k1gFnSc2	teorie
velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
seriálu	seriál	k1gInSc2	seriál
Teorie	teorie	k1gFnSc2	teorie
velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
na	na	k7c4	na
SerialZone	SerialZon	k1gInSc5	SerialZon
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Big	Big	k1gMnSc4	Big
Bang	Bang	k1gMnSc1	Bang
Theory	Theora	k1gFnSc2	Theora
na	na	k7c6	na
edna	edna	k1gFnSc1	edna
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Big	Big	k1gMnSc4	Big
Bang	Bang	k1gMnSc1	Bang
Theory	Theora	k1gFnSc2	Theora
v	v	k7c6	v
československé	československý	k2eAgFnSc3d1	Československá
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
</s>
