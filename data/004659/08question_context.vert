<s>
Teorie	teorie	k1gFnSc1	teorie
velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
The	The	k1gMnSc1	The
Big	Big	k1gFnSc2	Big
Bang	Bang	k1gMnSc1	Bang
Theory	Theora	k1gFnSc2	Theora
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
situační	situační	k2eAgFnSc1d1	situační
komedie	komedie	k1gFnSc1	komedie
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
mladých	mladý	k2eAgMnPc2d1	mladý
fyziků	fyzik	k1gMnPc2	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
Dělá	dělat	k5eAaImIp3nS	dělat
si	se	k3xPyFc3	se
legraci	legrace	k1gFnSc4	legrace
z	z	k7c2	z
jejich	jejich	k3xOp3gInSc2	jejich
osobního	osobní	k2eAgInSc2d1	osobní
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
z	z	k7c2	z
nerdovské	rdovský	k2eNgFnSc2d1	rdovský
subkultury	subkultura	k1gFnSc2	subkultura
současné	současný	k2eAgFnSc2d1	současná
americké	americký	k2eAgFnSc2d1	americká
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
a	a	k8xC	a
staví	stavit	k5eAaImIp3nS	stavit
je	on	k3xPp3gInPc4	on
do	do	k7c2	do
opozice	opozice	k1gFnSc2	opozice
k	k	k7c3	k
obyčejným	obyčejný	k2eAgMnPc3d1	obyčejný
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
vykreslováni	vykreslován	k2eAgMnPc1d1	vykreslován
jako	jako	k8xC	jako
hloupí	hloupý	k2eAgMnPc1d1	hloupý
<g/>
,	,	kIx,	,
leč	leč	k8xS	leč
šťastní	šťastný	k2eAgMnPc1d1	šťastný
a	a	k8xC	a
sociálně	sociálně	k6eAd1	sociálně
naplnění	naplněný	k2eAgMnPc1d1	naplněný
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc4	seriál
tvoří	tvořit	k5eAaImIp3nP	tvořit
a	a	k8xC	a
produkují	produkovat	k5eAaImIp3nP	produkovat
Chuck	Chuck	k1gInSc4	Chuck
Lorre	Lorr	k1gInSc5	Lorr
a	a	k8xC	a
Bill	Bill	k1gMnSc1	Bill
Prady	Prada	k1gFnSc2	Prada
<g/>
,	,	kIx,	,
pilotní	pilotní	k2eAgInSc4d1	pilotní
díl	díl	k1gInSc4	díl
režíroval	režírovat	k5eAaImAgMnS	režírovat
James	James	k1gMnSc1	James
Burrows	Burrowsa	k1gFnPc2	Burrowsa
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2007	[number]	k4	2007
na	na	k7c6	na
CBS	CBS	kA	CBS
<g/>
,	,	kIx,	,
vysílá	vysílat	k5eAaImIp3nS	vysílat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
CTV	CTV	kA	CTV
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc4	Indie
na	na	k7c4	na
Zee	Zee	k1gFnSc4	Zee
Cafe	Caf	k1gInSc2	Caf
<g/>
,	,	kIx,	,
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
na	na	k7c4	na
Warner	Warner	k1gInSc4	Warner
Channel	Channela	k1gFnPc2	Channela
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
na	na	k7c4	na
Channel	Channel	k1gInSc4	Channel
4	[number]	k4	4
<g/>
,	,	kIx,	,
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
na	na	k7c4	na
Nine	Nine	k1gFnSc4	Nine
Network	network	k1gInSc2	network
a	a	k8xC	a
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jej	on	k3xPp3gInSc4	on
vysílají	vysílat	k5eAaImIp3nP	vysílat
na	na	k7c6	na
programu	program	k1gInSc6	program
Prima	prima	k2eAgFnPc2d1	prima
Cool	Coola	k1gFnPc2	Coola
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>

