<s>
Měl	mít	k5eAaImAgMnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2007	[number]	k4	2007
na	na	k7c6	na
CBS	CBS	kA	CBS
<g/>
,	,	kIx,	,
vysílá	vysílat	k5eAaImIp3nS	vysílat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
CTV	CTV	kA	CTV
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc4	Indie
na	na	k7c4	na
Zee	Zee	k1gFnSc4	Zee
Cafe	Caf	k1gInSc2	Caf
<g/>
,	,	kIx,	,
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
na	na	k7c4	na
Warner	Warner	k1gInSc4	Warner
Channel	Channela	k1gFnPc2	Channela
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
na	na	k7c4	na
Channel	Channel	k1gInSc4	Channel
4	[number]	k4	4
<g/>
,	,	kIx,	,
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
na	na	k7c4	na
Nine	Nine	k1gFnSc4	Nine
Network	network	k1gInSc2	network
a	a	k8xC	a
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jej	on	k3xPp3gInSc4	on
vysílají	vysílat	k5eAaImIp3nP	vysílat
na	na	k7c6	na
programu	program	k1gInSc6	program
Prima	prima	k2eAgFnPc2d1	prima
Cool	Coola	k1gFnPc2	Coola
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
