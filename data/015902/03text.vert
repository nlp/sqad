<s>
Východotimorská	Východotimorský	k2eAgFnSc1d1
hymna	hymna	k1gFnSc1
</s>
<s>
Pátria	Pátrium	k1gNnPc1
</s>
<s>
Vlast	vlast	k1gFnSc1
Hymna	hymna	k1gFnSc1
</s>
<s>
Východní	východní	k2eAgMnSc1d1
Timor	Timor	k1gMnSc1
Východní	východní	k2eAgInSc4d1
Timor	Timor	k1gInSc4
Slova	slovo	k1gNnSc2
</s>
<s>
Afonso	Afonsa	k1gFnSc5
de	de	k?
Araujo	Araujo	k1gNnSc1
<g/>
,	,	kIx,
1975	#num#	k4
Hudba	hudba	k1gFnSc1
</s>
<s>
Francisco	Francisco	k1gMnSc1
Borja	Borja	k1gMnSc1
da	da	k?
Costa	Costa	k1gMnSc1
<g/>
,	,	kIx,
1975	#num#	k4
Přijata	přijat	k2eAgFnSc1d1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2002	#num#	k4
</s>
<s>
Hymna	hymna	k1gFnSc1
Východního	východní	k2eAgInSc2d1
Timoru	Timor	k1gInSc2
je	být	k5eAaImIp3nS
píseň	píseň	k1gFnSc4
Pátria	Pátria	k1gFnSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
Vlast	vlast	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
byla	být	k5eAaImAgFnS
hrána	hrát	k5eAaImNgFnS
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1975	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
deklaroval	deklarovat	k5eAaBmAgInS
svoji	svůj	k3xOyFgFnSc4
nezávislost	nezávislost	k1gFnSc4
na	na	k7c6
Portugalsku	Portugalsko	k1gNnSc6
jehož	jenž	k3xRgNnSc2,k3xOyRp3gNnSc2
bylo	být	k5eAaImAgNnS
území	území	k1gNnSc1
Východního	východní	k2eAgInSc2d1
Timoru	Timor	k1gInSc2
kolonií	kolonie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
hymna	hymna	k1gFnSc1
byla	být	k5eAaImAgFnS
po	po	k7c6
indonéské	indonéský	k2eAgFnSc6d1
invazi	invaze	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
krátce	krátce	k6eAd1
po	po	k7c6
získání	získání	k1gNnSc6
nezávislosti	nezávislost	k1gFnSc2
<g/>
,	,	kIx,
zakázána	zakázat	k5eAaPmNgFnS
a	a	k8xC
opět	opět	k6eAd1
přijata	přijmout	k5eAaPmNgFnS
a	a	k8xC
používána	používat	k5eAaImNgFnS
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
vyhlásil	vyhlásit	k5eAaPmAgInS
nezávislost	nezávislost	k1gFnSc4
tentokrát	tentokrát	k6eAd1
na	na	k7c6
Indonésii	Indonésie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hudbu	hudba	k1gFnSc4
složil	složit	k5eAaPmAgMnS
Afonso	Afonso	k1gMnSc1
de	de	k?
Araujo	Araujo	k1gMnSc1
<g/>
,	,	kIx,
slova	slovo	k1gNnSc2
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
básník	básník	k1gMnSc1
Francisco	Francisco	k1gMnSc1
Borja	Borja	k1gMnSc1
da	da	k?
Costa	Costa	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgInS
zabit	zabít	k5eAaPmNgMnS
v	v	k7c4
den	den	k1gInSc4
indonéské	indonéský	k2eAgFnSc2d1
invaze	invaze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hymna	hymna	k1gFnSc1
je	být	k5eAaImIp3nS
zpívána	zpívat	k5eAaImNgFnS
v	v	k7c6
portugalštině	portugalština	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
úředním	úřední	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
Východního	východní	k2eAgInSc2d1
Timoru	Timor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
též	též	k9
verze	verze	k1gFnSc1
v	v	k7c6
jazyce	jazyk	k1gInSc6
Tetum	Tetum	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
také	také	k9
dalším	další	k2eAgInPc3d1
úředním	úřední	k2eAgInPc3d1
a	a	k8xC
také	také	k9
národním	národní	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
většiny	většina	k1gFnSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
</s>
<s>
Originální	originální	k2eAgInSc1d1
portugalský	portugalský	k2eAgInSc1d1
textPátria	textPátrium	k1gNnSc2
<g/>
,	,	kIx,
Pátria	Pátrium	k1gNnSc2
<g/>
,	,	kIx,
Timor-Leste	Timor-Lest	k1gInSc5
<g/>
,	,	kIx,
nossa	nossa	k1gFnSc1
Naçã	Naçã	k1gMnSc1
<g/>
.	.	kIx.
<g/>
Glória	Glórium	k1gNnSc2
ao	ao	k?
povo	povo	k1gMnSc1
e	e	k0
aos	aos	k?
heróis	heróis	k1gFnSc6
da	da	k?
nossa	noss	k1gMnSc4
libertaçã	libertaçã	k1gMnSc1
<g/>
.	.	kIx.
<g/>
Pátria	Pátrium	k1gNnSc2
<g/>
,	,	kIx,
Pátria	Pátrium	k1gNnSc2
<g/>
,	,	kIx,
Timor-Leste	Timor-Lest	k1gInSc5
<g/>
,	,	kIx,
nossa	nossa	k1gFnSc1
Naçã	Naçã	k1gMnSc1
<g/>
.	.	kIx.
<g/>
Glória	Glórium	k1gNnSc2
ao	ao	k?
povo	povo	k1gMnSc1
e	e	k0
aos	aos	k?
heróis	heróis	k1gFnSc6
da	da	k?
nossa	nossa	k1gFnSc1
libertaçã	libertaçã	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
Vencemos	Vencemosa	k1gFnPc2
o	o	k7c4
colonialismo	colonialismo	k6eAd1
<g/>
,	,	kIx,
gritamos	gritamos	k1gMnSc1
<g/>
:	:	kIx,
<g/>
abaixo	abaixo	k1gMnSc1
o	o	k7c6
imperialismo	imperialismo	k6eAd1
<g/>
.	.	kIx.
<g/>
Terra	Terra	k1gFnSc1
livre	livr	k1gInSc5
<g/>
,	,	kIx,
povo	pova	k1gFnSc5
livre	livr	k1gInSc5
<g/>
,	,	kIx,
<g/>
nã	nã	k1gFnSc5
<g/>
,	,	kIx,
nã	nã	k1gFnSc5
<g/>
,	,	kIx,
nã	nã	k1gMnSc1
à	à	k?
exploraçã	exploraçã	k1gMnSc1
<g/>
.	.	kIx.
<g/>
Avante	Avant	k1gInSc5
unidos	unidosa	k1gFnPc2
firmes	firmes	k1gMnSc1
e	e	k0
decididos	decididos	k1gInSc4
<g/>
.	.	kIx.
<g/>
Na	na	k7c4
luta	lut	k2eAgMnSc4d1
contra	contr	k1gMnSc4
o	o	k7c4
imperialismoo	imperialismoo	k1gNnSc4
inimigo	inimigo	k1gMnSc1
dos	dos	k?
povos	povos	k1gMnSc1
<g/>
,	,	kIx,
até	até	k?
à	à	k?
vitória	vitórium	k1gNnSc2
final	final	k1gInSc4
<g/>
.	.	kIx.
<g/>
Pelo	Pelo	k6eAd1
caminho	camin	k1gMnSc2
da	da	k?
revoluçã	revoluçã	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Český	český	k2eAgInSc1d1
překladVlast	překladVlast	k1gInSc1
<g/>
,	,	kIx,
vlast	vlast	k1gFnSc1
<g/>
,	,	kIx,
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
<g/>
,	,	kIx,
náš	náš	k3xOp1gInSc1
národ	národ	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
Sláva	Sláva	k1gFnSc1
lidu	lid	k1gInSc2
a	a	k8xC
hrdinům	hrdina	k1gMnPc3
našeho	náš	k3xOp1gNnSc2
osvobození	osvobození	k1gNnSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
Vlast	vlast	k1gFnSc1
<g/>
,	,	kIx,
vlast	vlast	k1gFnSc1
<g/>
,	,	kIx,
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
náš	náš	k3xOp1gInSc4
národ	národ	k1gInSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
Sláva	Sláva	k1gFnSc1
lidu	lid	k1gInSc2
a	a	k8xC
hrdinům	hrdina	k1gMnPc3
našeho	náš	k3xOp1gNnSc2
osvobození	osvobození	k1gNnSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
My	my	k3xPp1nPc1
přemáháme	přemáhat	k5eAaImIp1nP
kolonialismus	kolonialismus	k1gInSc4
<g/>
,	,	kIx,
my	my	k3xPp1nPc1
křičíme	křičet	k5eAaImIp1nP
<g/>
:	:	kIx,
pryč	pryč	k6eAd1
s	s	k7c7
imperialismem	imperialismus	k1gInSc7
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
Svobodná	svobodný	k2eAgFnSc1d1
země	země	k1gFnSc1
<g/>
,	,	kIx,
svobodný	svobodný	k2eAgInSc1d1
národ	národ	k1gInSc1
<g/>
,	,	kIx,
ne	ne	k9
<g/>
,	,	kIx,
ne	ne	k9
<g/>
,	,	kIx,
ne	ne	k9
vykořisťování	vykořisťování	k1gNnSc1
<g/>
.	.	kIx.
<g/>
Vpřed	vpřed	k6eAd1
<g/>
,	,	kIx,
spojení	spojení	k1gNnSc2
<g/>
,	,	kIx,
pevní	pevný	k2eAgMnPc1d1
a	a	k8xC
odhodlaní	odhodlaný	k2eAgMnPc1d1
<g/>
,	,	kIx,
<g/>
do	do	k7c2
boje	boj	k1gInSc2
proti	proti	k7c3
imperialismu	imperialismus	k1gInSc3
<g/>
,	,	kIx,
nepříteli	nepřítel	k1gMnSc3
lidu	lid	k1gInSc2
<g/>
,	,	kIx,
až	až	k9
do	do	k7c2
konečného	konečný	k2eAgInSc2d1
vítězstvícestou	vítězstvícesta	k1gFnSc7
revoluce	revoluce	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Východního	východní	k2eAgInSc2d1
Timoru	Timor	k1gInSc2
</s>
<s>
Státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
Východního	východní	k2eAgInSc2d1
Timoru	Timor	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Východotimorská	Východotimorský	k2eAgFnSc1d1
hymna	hymna	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Hymny	hymnus	k1gInPc1
asijských	asijský	k2eAgInPc2d1
států	stát	k1gInPc2
Nezávislé	závislý	k2eNgInPc4d1
státy	stát	k1gInPc4
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Bahrajn	Bahrajn	k1gInSc1
•	•	k?
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
Bhútán	Bhútán	k1gInSc1
•	•	k?
Brunej	Brunej	k1gFnSc2
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Egypt	Egypt	k1gInSc1
•	•	k?
Filipíny	Filipíny	k1gFnPc4
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Irák	Irák	k1gInSc1
•	•	k?
Írán	Írán	k1gInSc1
•	•	k?
Izrael	Izrael	k1gInSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc4
•	•	k?
Jemen	Jemen	k1gInSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Jordánsko	Jordánsko	k1gNnSc1
•	•	k?
Kambodža	Kambodža	k1gFnSc1
•	•	k?
Katar	katar	k1gInSc1
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
Kuvajt	Kuvajt	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Kyrgyzstán	Kyrgyzstán	k2eAgInSc1d1
•	•	k?
Laos	Laos	k1gInSc1
•	•	k?
Libanon	Libanon	k1gInSc1
•	•	k?
Malajsie	Malajsie	k1gFnSc2
•	•	k?
Maledivy	Maledivy	k1gFnPc4
•	•	k?
Mongolsko	Mongolsko	k1gNnSc1
•	•	k?
Myanmar	Myanmar	k1gMnSc1
•	•	k?
Nepál	Nepál	k1gInSc1
•	•	k?
Omán	Omán	k1gInSc1
•	•	k?
Pákistán	Pákistán	k1gInSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Singapur	Singapur	k1gInSc4
•	•	k?
Spojené	spojený	k2eAgInPc4d1
arabské	arabský	k2eAgInPc4d1
emiráty	emirát	k1gInPc4
•	•	k?
Sýrie	Sýrie	k1gFnSc2
•	•	k?
Srí	Srí	k1gFnSc2
Lanka	lanko	k1gNnSc2
•	•	k?
Východní	východní	k2eAgFnSc1d1
Timor	Timor	k1gInSc1
•	•	k?
Tádžikistán	Tádžikistán	k1gInSc1
•	•	k?
Thajsko	Thajsko	k1gNnSc4
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
•	•	k?
Turecko	Turecko	k1gNnSc4
•	•	k?
Turkmenistán	Turkmenistán	k1gInSc1
•	•	k?
Uzbekistán	Uzbekistán	k1gInSc1
•	•	k?
Vietnam	Vietnam	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
a	a	k8xC
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Britské	britský	k2eAgNnSc1d1
indickooceánské	indickooceánský	k2eAgNnSc1d1
území	území	k1gNnSc1
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Hongkong	Hongkong	k1gInSc1
(	(	kIx(
<g/>
CHN	CHN	kA
<g/>
)	)	kIx)
•	•	k?
Kokosové	kokosový	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
AUS	AUS	kA
<g/>
)	)	kIx)
•	•	k?
Macao	Macao	k1gNnSc4
(	(	kIx(
<g/>
CHN	CHN	kA
<g/>
)	)	kIx)
•	•	k?
Tibet	Tibet	k1gInSc1
(	(	kIx(
<g/>
CHN	CHN	kA
<g/>
)	)	kIx)
•	•	k?
Vánoční	vánoční	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
(	(	kIx(
<g/>
AUS	AUS	kA
<g/>
)	)	kIx)
Státy	stát	k1gInPc7
bez	bez	k7c2
plného	plný	k2eAgNnSc2d1
uznání	uznání	k1gNnSc2
</s>
<s>
Abcházie	Abcházie	k1gFnSc1
•	•	k?
Arcach	Arcacha	k1gFnPc2
•	•	k?
Jižní	jižní	k2eAgFnSc2d1
Osetie	Osetie	k1gFnSc2
•	•	k?
Palestina	Palestina	k1gFnSc1
•	•	k?
Severní	severní	k2eAgInSc1d1
Kypr	Kypr	k1gInSc1
</s>
<s>
Hymny	hymna	k1gFnSc2
států	stát	k1gInPc2
Austrálie	Austrálie	k1gFnSc2
a	a	k8xC
Oceánie	Oceánie	k1gFnSc2
Nezávislé	závislý	k2eNgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
•	•	k?
Federativní	federativní	k2eAgInPc1d1
státy	stát	k1gInPc1
Mikronésie	Mikronésie	k1gFnSc2
•	•	k?
Fidži	Fidž	k1gFnSc6
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Kiribati	Kiribati	k1gFnSc2
•	•	k?
Marshallovy	Marshallův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Nauru	Naur	k1gInSc2
•	•	k?
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
•	•	k?
Palau	Palaus	k1gInSc2
•	•	k?
Papua	Papu	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
•	•	k?
Samoa	Samoa	k1gFnSc1
•	•	k?
Šalomounovy	Šalomounův	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Tonga	Tonga	k1gFnSc1
•	•	k?
Tuvalu	Tuval	k1gInSc2
•	•	k?
Vanuatu	Vanuat	k1gInSc2
•	•	k?
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
a	a	k8xC
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Americká	americký	k2eAgFnSc1d1
Samoa	Samoa	k1gFnSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
•	•	k?
Cookovy	Cookův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
NZL	NZL	kA
<g/>
)	)	kIx)
•	•	k?
Francouzská	francouzský	k2eAgFnSc1d1
Polynésie	Polynésie	k1gFnSc1
(	(	kIx(
<g/>
FRA	FRA	kA
<g/>
)	)	kIx)
•	•	k?
Guam	Guam	k1gMnSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
•	•	k?
Niue	Niue	k1gInSc1
(	(	kIx(
<g/>
NZL	NZL	kA
<g/>
)	)	kIx)
•	•	k?
Norfolk	Norfolk	k1gMnSc1
(	(	kIx(
<g/>
AUS	AUS	kA
<g/>
)	)	kIx)
•	•	k?
Nová	nový	k2eAgFnSc1d1
Kaledonie	Kaledonie	k1gFnSc1
(	(	kIx(
<g/>
FRA	FRA	kA
<g/>
)	)	kIx)
•	•	k?
Pitcairnovy	Pitcairnův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Severní	severní	k2eAgFnSc2d1
Mariany	Mariana	k1gFnSc2
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
•	•	k?
Tokelau	Tokelaus	k1gInSc2
(	(	kIx(
<g/>
NZL	NZL	kA
<g/>
)	)	kIx)
•	•	k?
Velikonoční	velikonoční	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
(	(	kIx(
<g/>
CHL	CHL	kA
<g/>
)	)	kIx)
•	•	k?
Wallis	Wallis	k1gInSc1
a	a	k8xC
Futuna	Futuna	k1gFnSc1
(	(	kIx(
<g/>
FRA	FRA	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
