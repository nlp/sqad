<s>
Polygrafie	polygrafie	k1gFnSc1	polygrafie
je	být	k5eAaImIp3nS	být
výrobní	výrobní	k2eAgInSc4d1	výrobní
obor	obor	k1gInSc4	obor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
a	a	k8xC	a
tiskem	tisk	k1gInSc7	tisk
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
textové	textový	k2eAgFnPc4d1	textová
a	a	k8xC	a
obrazové	obrazový	k2eAgFnPc4d1	obrazová
předlohy	předloha	k1gFnPc4	předloha
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tyto	tento	k3xDgFnPc4	tento
základní	základní	k2eAgFnPc4d1	základní
výrobní	výrobní	k2eAgFnPc4d1	výrobní
fáze	fáze	k1gFnPc4	fáze
<g/>
:	:	kIx,	:
předtiskovou	předtiskový	k2eAgFnSc4d1	předtisková
přípravu	příprava	k1gFnSc4	příprava
(	(	kIx(	(
<g/>
pre	pre	k?	pre
press	press	k1gInSc1	press
<g/>
)	)	kIx)	)
,	,	kIx,	,
tisk	tisk	k1gInSc1	tisk
(	(	kIx(	(
<g/>
press	press	k1gInSc1	press
<g/>
)	)	kIx)	)
a	a	k8xC	a
dokončovací	dokončovací	k2eAgNnSc1d1	dokončovací
zpracování	zpracování	k1gNnSc1	zpracování
(	(	kIx(	(
<g/>
post	post	k1gInSc1	post
press	press	k1gInSc1	press
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předtiskové	předtiskový	k2eAgFnSc6d1	předtisková
přípravě	příprava	k1gFnSc6	příprava
vzniká	vznikat	k5eAaImIp3nS	vznikat
předloha	předloha	k1gFnSc1	předloha
pro	pro	k7c4	pro
tisk	tisk	k1gInSc4	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Předlohu	předloha	k1gFnSc4	předloha
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
grafik	grafik	k1gMnSc1	grafik
z	z	k7c2	z
připravených	připravený	k2eAgInPc2d1	připravený
podkladů	podklad	k1gInPc2	podklad
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
zákazník	zákazník	k1gMnSc1	zákazník
dodá	dodat	k5eAaPmIp3nS	dodat
<g/>
.	.	kIx.	.
</s>
<s>
Podklady	podklad	k1gInPc4	podklad
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
programech	program	k1gInPc6	program
od	od	k7c2	od
společnostech	společnost	k1gFnPc6	společnost
zabývajících	zabývající	k2eAgMnPc2d1	zabývající
se	se	k3xPyFc4	se
grafikou	grafika	k1gFnSc7	grafika
(	(	kIx(	(
<g/>
Adobe	Adobe	kA	Adobe
<g/>
,	,	kIx,	,
Corel	Corel	kA	Corel
<g/>
,	,	kIx,	,
Zoner	Zoner	k1gMnSc1	Zoner
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Výstupním	výstupní	k2eAgInSc7d1	výstupní
výrobkem	výrobek	k1gInSc7	výrobek
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
fáze	fáze	k1gFnSc2	fáze
jsou	být	k5eAaImIp3nP	být
tiskové	tiskový	k2eAgFnPc1d1	tisková
desky	deska	k1gFnPc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
obrazová	obrazový	k2eAgFnSc1d1	obrazová
předloha	předloha	k1gFnSc1	předloha
na	na	k7c4	na
předem	předem	k6eAd1	předem
určený	určený	k2eAgInSc4d1	určený
materiál	materiál	k1gInSc4	materiál
pomocí	pomocí	k7c2	pomocí
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
tisku	tisk	k1gInSc2	tisk
můžeme	moct	k5eAaImIp1nP	moct
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
základní	základní	k2eAgFnPc4d1	základní
kategorie	kategorie	k1gFnPc4	kategorie
<g/>
:	:	kIx,	:
Konvenční	konvenční	k2eAgFnSc2d1	konvenční
techniky	technika	k1gFnSc2	technika
ofsetový	ofsetový	k2eAgInSc4d1	ofsetový
tisk	tisk	k1gInSc4	tisk
sítotisk	sítotisk	k1gInSc1	sítotisk
flexotisk	flexotisk	k1gInSc1	flexotisk
hlubotisk	hlubotisk	k1gInSc1	hlubotisk
knihtisk	knihtisk	k1gInSc4	knihtisk
(	(	kIx(	(
<g/>
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
)	)	kIx)	)
Digitální	digitální	k2eAgInSc1d1	digitální
tisk	tisk	k1gInSc1	tisk
Konvenční	konvenční	k2eAgInSc1d1	konvenční
(	(	kIx(	(
<g/>
klasické	klasický	k2eAgFnPc1d1	klasická
<g/>
)	)	kIx)	)
tiskové	tiskový	k2eAgFnPc1d1	tisková
techniky	technika	k1gFnPc1	technika
jsou	být	k5eAaImIp3nP	být
specifická	specifický	k2eAgFnSc1d1	specifická
základními	základní	k2eAgInPc7d1	základní
čtyřmi	čtyři	k4xCgInPc7	čtyři
činiteli	činitel	k1gInPc7	činitel
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
kterými	který	k3yRgMnPc7	který
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
tisková	tiskový	k2eAgFnSc1d1	tisková
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
tisková	tiskový	k2eAgFnSc1d1	tisková
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
tlak	tlak	k1gInSc1	tlak
a	a	k8xC	a
potiskovaný	potiskovaný	k2eAgInSc1d1	potiskovaný
materiál	materiál	k1gInSc1	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
konvenčních	konvenční	k2eAgFnPc2d1	konvenční
technik	technika	k1gFnPc2	technika
digitální	digitální	k2eAgInSc4d1	digitální
tisk	tisk	k1gInSc4	tisk
má	mít	k5eAaImIp3nS	mít
jenom	jenom	k9	jenom
dva	dva	k4xCgMnPc4	dva
činitele	činitel	k1gMnPc4	činitel
tisku	tisk	k1gInSc2	tisk
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
tiskovou	tiskový	k2eAgFnSc4d1	tisková
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
potiskovaný	potiskovaný	k2eAgInSc4d1	potiskovaný
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
je	být	k5eAaImIp3nS	být
konečný	konečný	k2eAgInSc1d1	konečný
výrobek	výrobek	k1gInSc1	výrobek
předán	předat	k5eAaPmNgInS	předat
zákazníkovi	zákazník	k1gMnSc3	zákazník
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
vytištěné	vytištěný	k2eAgInPc4d1	vytištěný
archy	arch	k1gInPc4	arch
projít	projít	k5eAaPmF	projít
knihárnou	knihárna	k1gFnSc7	knihárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knihárně	knihárna	k1gFnSc6	knihárna
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
tyto	tento	k3xDgInPc1	tento
úkony	úkon	k1gInPc1	úkon
<g/>
:	:	kIx,	:
řezání	řezání	k1gNnSc1	řezání
<g/>
,	,	kIx,	,
skládání	skládání	k1gNnSc1	skládání
<g/>
,	,	kIx,	,
vysekávání	vysekávání	k1gNnSc1	vysekávání
<g/>
,	,	kIx,	,
šití	šití	k1gNnSc1	šití
<g/>
,	,	kIx,	,
lepení	lepení	k1gNnSc1	lepení
atd.	atd.	kA	atd.
V	v	k7c6	v
polygrafické	polygrafický	k2eAgFnSc6d1	polygrafická
výrobě	výroba	k1gFnSc6	výroba
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
kvalita	kvalita	k1gFnSc1	kvalita
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
výslednou	výsledný	k2eAgFnSc4d1	výsledná
kvalitu	kvalita	k1gFnSc4	kvalita
polygrafického	polygrafický	k2eAgInSc2d1	polygrafický
produktu	produkt	k1gInSc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ty	ten	k3xDgInPc4	ten
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
patří	patřit	k5eAaImIp3nS	patřit
lepidla	lepidlo	k1gNnSc2	lepidlo
<g/>
,	,	kIx,	,
papír	papír	k1gInSc1	papír
<g/>
,	,	kIx,	,
tiskové	tiskový	k2eAgFnPc1d1	tisková
barvy	barva	k1gFnPc1	barva
a	a	k8xC	a
laky	lak	k1gInPc1	lak
<g/>
.	.	kIx.	.
</s>
<s>
Polygrafie	polygrafie	k1gFnSc1	polygrafie
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
terminologické	terminologický	k2eAgFnSc6d1	terminologická
databázi	databáze	k1gFnSc6	databáze
knihovnictví	knihovnictví	k1gNnSc2	knihovnictví
a	a	k8xC	a
informační	informační	k2eAgFnSc2d1	informační
vědy	věda	k1gFnSc2	věda
(	(	kIx(	(
<g/>
TDKIV	TDKIV	kA	TDKIV
<g/>
)	)	kIx)	)
Nejčastější	častý	k2eAgNnSc1d3	nejčastější
povolání	povolání	k1gNnSc1	povolání
v	v	k7c6	v
polygrafii	polygrafia	k1gFnSc6	polygrafia
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
práce	práce	k1gFnSc2	práce
ČR	ČR	kA	ČR
</s>
