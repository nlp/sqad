<s>
Norsko	Norsko	k1gNnSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
na	na	k7c6
západní	západní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
Skandinávského	skandinávský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
a	a	k8xC
na	na	k7c6
mnoha	mnoho	k4c6
ostrůvcích	ostrůvek	k1gInPc6
v	v	k7c6
Severním	severní	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
,	,	kIx,
Norském	norský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
,	,	kIx,
Barentsově	barentsově	k6eAd1
moři	moře	k1gNnSc3
a	a	k8xC
Severním	severní	k2eAgInSc6d1
ledovém	ledový	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
<g/>
.	.	kIx.
</s>