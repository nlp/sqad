<s>
Dále	daleko	k6eAd2	daleko
existuje	existovat	k5eAaImIp3nS	existovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
zápalek	zápalka	k1gFnPc2	zápalka
<g/>
,	,	kIx,	,
vyznačujících	vyznačující	k2eAgFnPc2d1	vyznačující
se	s	k7c7	s
zvláštními	zvláštní	k2eAgInPc7d1	zvláštní
rozměry	rozměr	k1gInPc7	rozměr
nebo	nebo	k8xC	nebo
balením	balení	k1gNnSc7	balení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
často	často	k6eAd1	často
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
reklamním	reklamní	k2eAgInPc3d1	reklamní
účelům	účel	k1gInPc3	účel
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
upomínkové	upomínkový	k2eAgInPc4d1	upomínkový
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
