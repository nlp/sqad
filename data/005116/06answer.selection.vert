<s>
Translatologie	Translatologie	k1gFnSc1	Translatologie
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
překladu	překlad	k1gInSc6	překlad
a	a	k8xC	a
tlumočení	tlumočení	k1gNnSc6	tlumočení
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
interdisciplinární	interdisciplinární	k2eAgInSc1d1	interdisciplinární
vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
zabývající	zabývající	k2eAgInSc1d1	zabývající
se	s	k7c7	s
převodem	převod	k1gInSc7	převod
textů	text	k1gInPc2	text
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
jazyka	jazyk	k1gInSc2	jazyk
do	do	k7c2	do
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
sémiotického	sémiotický	k2eAgInSc2d1	sémiotický
systému	systém	k1gInSc2	systém
do	do	k7c2	do
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
překladatelem	překladatel	k1gMnSc7	překladatel
lidským	lidský	k2eAgMnSc7d1	lidský
nebo	nebo	k8xC	nebo
překladačem	překladač	k1gInSc7	překladač
strojovým	strojový	k2eAgInSc7d1	strojový
<g/>
,	,	kIx,	,
písemně	písemně	k6eAd1	písemně
(	(	kIx(	(
<g/>
překlad	překlad	k1gInSc1	překlad
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ústně	ústně	k6eAd1	ústně
(	(	kIx(	(
<g/>
tlumočení	tlumočení	k1gNnSc1	tlumočení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
