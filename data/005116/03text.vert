<s>
Translatologie	Translatologie	k1gFnSc1	Translatologie
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
překladu	překlad	k1gInSc6	překlad
a	a	k8xC	a
tlumočení	tlumočení	k1gNnSc6	tlumočení
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
interdisciplinární	interdisciplinární	k2eAgInSc1d1	interdisciplinární
vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
zabývající	zabývající	k2eAgInSc1d1	zabývající
se	s	k7c7	s
převodem	převod	k1gInSc7	převod
textů	text	k1gInPc2	text
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
jazyka	jazyk	k1gInSc2	jazyk
do	do	k7c2	do
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
sémiotického	sémiotický	k2eAgInSc2d1	sémiotický
systému	systém	k1gInSc2	systém
do	do	k7c2	do
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
překladatelem	překladatel	k1gMnSc7	překladatel
lidským	lidský	k2eAgMnSc7d1	lidský
nebo	nebo	k8xC	nebo
překladačem	překladač	k1gInSc7	překladač
strojovým	strojový	k2eAgInSc7d1	strojový
<g/>
,	,	kIx,	,
písemně	písemně	k6eAd1	písemně
(	(	kIx(	(
<g/>
překlad	překlad	k1gInSc1	překlad
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ústně	ústně	k6eAd1	ústně
(	(	kIx(	(
<g/>
tlumočení	tlumočení	k1gNnSc1	tlumočení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zajímá	zajímat	k5eAaImIp3nS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
otázky	otázka	k1gFnPc4	otázka
přesnosti	přesnost	k1gFnSc2	přesnost
(	(	kIx(	(
<g/>
věrnosti	věrnost	k1gFnSc2	věrnost
<g/>
)	)	kIx)	)
překladu	překlad	k1gInSc2	překlad
<g/>
,	,	kIx,	,
převodu	převod	k1gInSc2	převod
textů	text	k1gInPc2	text
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
kulturní	kulturní	k2eAgFnSc2d1	kulturní
oblasti	oblast	k1gFnSc2	oblast
nebo	nebo	k8xC	nebo
dějinné	dějinný	k2eAgFnSc2d1	dějinná
doby	doba	k1gFnSc2	doba
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
<g/>
,	,	kIx,	,
o	o	k7c4	o
otázku	otázka	k1gFnSc4	otázka
možnosti	možnost	k1gFnSc2	možnost
překladu	překlad	k1gInSc2	překlad
(	(	kIx(	(
<g/>
přeložitelnosti	přeložitelnost	k1gFnSc2	přeložitelnost
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
lze	lze	k6eAd1	lze
translatologii	translatologie	k1gFnSc4	translatologie
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
deskriptivní	deskriptivní	k2eAgFnSc4d1	deskriptivní
a	a	k8xC	a
aplikovanou	aplikovaný	k2eAgFnSc4d1	aplikovaná
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
deskriptivní	deskriptivní	k2eAgFnSc2d1	deskriptivní
odnože	odnož	k1gFnSc2	odnož
spadá	spadat	k5eAaImIp3nS	spadat
například	například	k6eAd1	například
kritika	kritika	k1gFnSc1	kritika
překladu	překlad	k1gInSc2	překlad
či	či	k8xC	či
dějiny	dějiny	k1gFnPc4	dějiny
překladu	překlad	k1gInSc2	překlad
<g/>
.	.	kIx.	.
</s>
<s>
Aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
translatologie	translatologie	k1gFnSc1	translatologie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
překladatelskou	překladatelský	k2eAgFnSc7d1	překladatelská
a	a	k8xC	a
tlumočnickou	tlumočnický	k2eAgFnSc7d1	tlumočnická
praxí	praxe	k1gFnSc7	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
interlingvální	interlingvální	k2eAgFnSc2d1	interlingvální
(	(	kIx(	(
<g/>
mezijazyčné	mezijazyčný	k2eAgFnSc2d1	mezijazyčný
<g/>
)	)	kIx)	)
komunikace	komunikace	k1gFnSc2	komunikace
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
monolingvální	monolingvální	k2eAgFnSc2d1	monolingvální
(	(	kIx(	(
<g/>
jednojazyčné	jednojazyčný	k2eAgFnSc2d1	jednojazyčná
<g/>
)	)	kIx)	)
komunikace	komunikace	k1gFnSc2	komunikace
objevuje	objevovat	k5eAaImIp3nS	objevovat
jazyková	jazykový	k2eAgFnSc1d1	jazyková
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc1d1	kulturní
bariéra	bariéra	k1gFnSc1	bariéra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
dorozumění	dorozumění	k1gNnSc4	dorozumění
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
komunikačního	komunikační	k2eAgInSc2d1	komunikační
procesu	proces	k1gInSc2	proces
tak	tak	k6eAd1	tak
nutně	nutně	k6eAd1	nutně
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
zprostředkovatel	zprostředkovatel	k1gMnSc1	zprostředkovatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ovládá	ovládat	k5eAaImIp3nS	ovládat
oba	dva	k4xCgMnPc4	dva
jazyky	jazyk	k1gMnPc4	jazyk
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gInPc2	jejich
kulturních	kulturní	k2eAgInPc2d1	kulturní
vzorců	vzorec	k1gInPc2	vzorec
i	i	k8xC	i
způsoby	způsob	k1gInPc1	způsob
transferu	transfer	k1gInSc2	transfer
(	(	kIx(	(
<g/>
přenosu	přenos	k1gInSc2	přenos
<g/>
)	)	kIx)	)
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
jazykového	jazykový	k2eAgInSc2d1	jazykový
kódu	kód	k1gInSc2	kód
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Odborník	odborník	k1gMnSc1	odborník
na	na	k7c4	na
ústní	ústní	k2eAgNnSc4d1	ústní
jazykově-kulturní	jazykověulturní	k2eAgNnSc4d1	jazykově-kulturní
zprostředkování	zprostředkování	k1gNnSc4	zprostředkování
je	být	k5eAaImIp3nS	být
označován	označován	k2eAgMnSc1d1	označován
jako	jako	k8xS	jako
tlumočník	tlumočník	k1gMnSc1	tlumočník
<g/>
,	,	kIx,	,
odborník	odborník	k1gMnSc1	odborník
na	na	k7c4	na
písemný	písemný	k2eAgInSc4d1	písemný
jazykově-kulturní	jazykověulturní	k2eAgInSc4d1	jazykově-kulturní
zprostředkování	zprostředkování	k1gNnSc6	zprostředkování
pak	pak	k6eAd1	pak
jako	jako	k9	jako
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
.	.	kIx.	.
</s>
<s>
Zastřešujícím	zastřešující	k2eAgInSc7d1	zastřešující
pojmem	pojem	k1gInSc7	pojem
pro	pro	k7c4	pro
překladatele	překladatel	k1gMnSc4	překladatel
a	a	k8xC	a
tlumočníka	tlumočník	k1gMnSc4	tlumočník
je	být	k5eAaImIp3nS	být
translátor	translátor	k1gMnSc1	translátor
<g/>
.	.	kIx.	.
</s>
<s>
Tlumočení	tlumočení	k1gNnSc1	tlumočení
a	a	k8xC	a
překladatelství	překladatelství	k1gNnSc1	překladatelství
je	být	k5eAaImIp3nS	být
souhrnně	souhrnně	k6eAd1	souhrnně
nazýváno	nazývat	k5eAaImNgNnS	nazývat
jako	jako	k8xC	jako
translační	translační	k2eAgFnSc1d1	translační
činnost	činnost	k1gFnSc1	činnost
či	či	k8xC	či
translace	translace	k1gFnSc1	translace
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
o	o	k7c6	o
překladu	překlad	k1gInSc6	překlad
a	a	k8xC	a
tlumočení	tlumočení	k1gNnSc1	tlumočení
je	být	k5eAaImIp3nS	být
translatologie	translatologie	k1gFnSc1	translatologie
<g/>
.	.	kIx.	.
</s>
<s>
Překládaný	překládaný	k2eAgInSc1d1	překládaný
či	či	k8xC	či
tlumočený	tlumočený	k2eAgInSc1d1	tlumočený
text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
výchozí	výchozí	k2eAgInSc1d1	výchozí
text	text	k1gInSc1	text
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
výchozího	výchozí	k2eAgInSc2d1	výchozí
jazyka	jazyk	k1gInSc2	jazyk
převáděn	převádět	k5eAaImNgInS	převádět
do	do	k7c2	do
cílového	cílový	k2eAgInSc2d1	cílový
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Produktem	produkt	k1gInSc7	produkt
této	tento	k3xDgFnSc2	tento
translace	translace	k1gFnSc2	translace
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
cílový	cílový	k2eAgInSc4d1	cílový
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
překladu	překlad	k1gInSc6	překlad
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
již	již	k9	již
v	v	k7c6	v
antických	antický	k2eAgInPc6d1	antický
zdrojích	zdroj	k1gInPc6	zdroj
<g/>
,	,	kIx,	,
věda	věda	k1gFnSc1	věda
translatologie	translatologie	k1gFnSc1	translatologie
se	se	k3xPyFc4	se
konstituovala	konstituovat	k5eAaBmAgFnS	konstituovat
až	až	k9	až
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
tomu	ten	k3xDgMnSc3	ten
předcházel	předcházet	k5eAaImAgMnS	předcházet
bouřlivý	bouřlivý	k2eAgInSc4d1	bouřlivý
rozvoj	rozvoj	k1gInSc4	rozvoj
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
fáze	fáze	k1gFnSc1	fáze
translatologie	translatologie	k1gFnSc2	translatologie
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
výrazně	výrazně	k6eAd1	výrazně
lingvisticky	lingvisticky	k6eAd1	lingvisticky
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
však	však	k9	však
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
podstatnému	podstatný	k2eAgNnSc3d1	podstatné
rozšíření	rozšíření	k1gNnSc3	rozšíření
zájmů	zájem	k1gInPc2	zájem
translatologie	translatologie	k1gFnSc2	translatologie
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
kulturnímu	kulturní	k2eAgInSc3d1	kulturní
obratu	obrat	k1gInSc3	obrat
<g/>
.	.	kIx.	.
</s>
<s>
Lingvistické	lingvistický	k2eAgInPc1d1	lingvistický
zájmy	zájem	k1gInPc1	zájem
ustupují	ustupovat	k5eAaImIp3nP	ustupovat
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nS	soustředit
na	na	k7c4	na
fungování	fungování	k1gNnSc4	fungování
literárních	literární	k2eAgInPc2d1	literární
systémů	systém	k1gInPc2	systém
jako	jako	k8xC	jako
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgFnPc7	jenž
překlad	překlad	k1gInSc1	překlad
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
zprostředkující	zprostředkující	k2eAgInSc1d1	zprostředkující
prvek	prvek	k1gInSc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
se	s	k7c7	s
kritikou	kritika	k1gFnSc7	kritika
předchozího	předchozí	k2eAgInSc2d1	předchozí
odosobněného	odosobněný	k2eAgInSc2d1	odosobněný
přístupu	přístup	k1gInSc2	přístup
dospívá	dospívat	k5eAaImIp3nS	dospívat
k	k	k7c3	k
širokému	široký	k2eAgNnSc3d1	široké
bádání	bádání	k1gNnSc3	bádání
o	o	k7c6	o
překladatelích	překladatel	k1gMnPc6	překladatel
jako	jako	k8xC	jako
o	o	k7c6	o
aktivních	aktivní	k2eAgMnPc6d1	aktivní
jedincích	jedinec	k1gMnPc6	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Levý	levý	k2eAgMnSc1d1	levý
Anton	Anton	k1gMnSc1	Anton
Popovič	Popovič	k1gMnSc1	Popovič
Otakar	Otakar	k1gMnSc1	Otakar
Levý	levý	k2eAgMnSc1d1	levý
Jiří	Jiří	k1gMnSc1	Jiří
Pechar	Pechar	k1gMnSc1	Pechar
Gideon	Gideon	k1gMnSc1	Gideon
Toury	Toura	k1gFnSc2	Toura
Andrew	Andrew	k1gMnSc1	Andrew
Chesterman	Chesterman	k1gMnSc1	Chesterman
Peter	Peter	k1gMnSc1	Peter
Newmark	Newmark	k1gInSc4	Newmark
Eugene	Eugen	k1gInSc5	Eugen
Nida	Nidus	k1gMnSc2	Nidus
Anthony	Anthona	k1gFnSc2	Anthona
Pym	Pym	k1gMnSc2	Pym
Danica	Danicus	k1gMnSc2	Danicus
Seleskovitchová	Seleskovitchová	k1gFnSc1	Seleskovitchová
Daniel	Daniel	k1gMnSc1	Daniel
Gile	Gil	k1gInSc2	Gil
Překladatel	překladatel	k1gMnSc1	překladatel
Překlad	překlad	k1gInSc4	překlad
Tlumočení	tlumočení	k1gNnSc2	tlumočení
Konferenční	konferenční	k2eAgNnSc1d1	konferenční
tlumočení	tlumočení	k1gNnSc1	tlumočení
Ústav	ústava	k1gFnPc2	ústava
translatologie	translatologie	k1gFnSc2	translatologie
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Teorie	teorie	k1gFnSc1	teorie
skoposu	skopos	k1gInSc2	skopos
Překlady	překlad	k1gInPc4	překlad
Bible	bible	k1gFnPc1	bible
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
translatologie	translatologie	k1gFnSc2	translatologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Ústav	ústava	k1gFnPc2	ústava
translatologie	translatologie	k1gFnSc2	translatologie
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
Jednota	jednota	k1gFnSc1	jednota
tlumočníků	tlumočník	k1gMnPc2	tlumočník
a	a	k8xC	a
překladatelů	překladatel	k1gMnPc2	překladatel
Obec	obec	k1gFnSc1	obec
překladatelů	překladatel	k1gMnPc2	překladatel
</s>
