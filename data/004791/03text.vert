<s>
SARS	SARS	kA	SARS
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
Severe	sever	k1gInSc5	sever
Acute	Acut	k1gInSc5	Acut
Respiratory	Respirator	k1gMnPc4	Respirator
Syndrome	syndrom	k1gInSc5	syndrom
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
těžký	těžký	k2eAgInSc1d1	těžký
akutní	akutní	k2eAgInSc1d1	akutní
respirační	respirační	k2eAgInSc1d1	respirační
syndrom	syndrom	k1gInSc1	syndrom
<g/>
,	,	kIx,	,
či	či	k8xC	či
také	také	k9	také
syndrom	syndrom	k1gInSc1	syndrom
náhlého	náhlý	k2eAgNnSc2d1	náhlé
selhání	selhání	k1gNnSc2	selhání
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
virové	virový	k2eAgNnSc1d1	virové
onemocnění	onemocnění	k1gNnSc1	onemocnění
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
způsobené	způsobený	k2eAgFnSc2d1	způsobená
koronavirem	koronavir	k1gMnSc7	koronavir
SARS-CoV	SARS-CoV	k1gMnSc7	SARS-CoV
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
hlášen	hlásit	k5eAaImNgInS	hlásit
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2002	[number]	k4	2002
z	z	k7c2	z
čínské	čínský	k2eAgFnSc2d1	čínská
provincie	provincie	k1gFnSc2	provincie
Kuang-tung	Kuangunga	k1gFnPc2	Kuang-tunga
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
dalších	další	k2eAgInPc2d1	další
měsíců	měsíc	k1gInPc2	měsíc
se	se	k3xPyFc4	se
nákaza	nákaza	k1gFnSc1	nákaza
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
nakazila	nakazit	k5eAaPmAgFnS	nakazit
přes	přes	k7c4	přes
8000	[number]	k4	8000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
způsobila	způsobit	k5eAaPmAgFnS	způsobit
smrt	smrt	k1gFnSc4	smrt
774	[number]	k4	774
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
pomocí	pomocí	k7c2	pomocí
efektivních	efektivní	k2eAgNnPc2d1	efektivní
protiepidemických	protiepidemický	k2eAgNnPc2d1	protiepidemické
opatření	opatření	k1gNnPc2	opatření
zastavit	zastavit	k5eAaPmF	zastavit
šíření	šíření	k1gNnSc2	šíření
infekce	infekce	k1gFnSc2	infekce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k9	ještě
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
byly	být	k5eAaImAgInP	být
nové	nový	k2eAgInPc1d1	nový
případy	případ	k1gInPc1	případ
hlášeny	hlášen	k2eAgInPc1d1	hlášen
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
několik	několik	k4yIc4	několik
případů	případ	k1gInPc2	případ
infekce	infekce	k1gFnSc2	infekce
v	v	k7c6	v
laboratořích	laboratoř	k1gFnPc6	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
nebyly	být	k5eNaImAgInP	být
potvrzeny	potvrzen	k2eAgInPc1d1	potvrzen
případy	případ	k1gInPc1	případ
nakažení	nakažení	k1gNnSc2	nakažení
SARS	SARS	kA	SARS
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
smrtelného	smrtelný	k2eAgInSc2d1	smrtelný
koronaviru	koronavir	k1gInSc2	koronavir
SARS	SARS	kA	SARS
(	(	kIx(	(
<g/>
SARS-CoV	SARS-CoV	k1gMnSc1	SARS-CoV
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
populaci	populace	k1gFnSc6	populace
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
příbuzné	příbuzný	k1gMnPc4	příbuzný
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
virulentní	virulentní	k2eAgInPc1d1	virulentní
koronaviry	koronavir	k1gInPc1	koronavir
NL63	NL63	k1gFnSc2	NL63
a	a	k8xC	a
OC43	OC43	k1gFnSc2	OC43
způsobující	způsobující	k2eAgNnSc4d1	způsobující
nachlazení	nachlazení	k1gNnSc4	nachlazení
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
nedávno	nedávno	k6eAd1	nedávno
objevené	objevený	k2eAgInPc1d1	objevený
koronaviry	koronavir	k1gInPc1	koronavir
NL63	NL63	k1gFnSc2	NL63
a	a	k8xC	a
HKU	HKU	kA	HKU
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
virus	virus	k1gInSc1	virus
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
koronavirů	koronavir	k1gInPc2	koronavir
nazvaný	nazvaný	k2eAgMnSc1d1	nazvaný
MERS-CoV	MERS-CoV	k1gMnSc1	MERS-CoV
způsobující	způsobující	k2eAgInSc4d1	způsobující
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
provázený	provázený	k2eAgInSc1d1	provázený
selháním	selhání	k1gNnSc7	selhání
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
;	;	kIx,	;
WHO	WHO	kA	WHO
se	se	k3xPyFc4	se
obává	obávat	k5eAaImIp3nS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
nemoc	nemoc	k1gFnSc1	nemoc
má	mít	k5eAaImIp3nS	mít
potenciál	potenciál	k1gInSc4	potenciál
rozvinout	rozvinout	k5eAaPmF	rozvinout
se	se	k3xPyFc4	se
v	v	k7c6	v
epidemii	epidemie	k1gFnSc6	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
SARS	SARS	kA	SARS
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
časový	časový	k2eAgInSc1d1	časový
interval	interval	k1gInSc1	interval
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
uplyne	uplynout	k5eAaPmIp3nS	uplynout
od	od	k7c2	od
nákazy	nákaza	k1gFnSc2	nákaza
virem	vir	k1gInSc7	vir
k	k	k7c3	k
prvním	první	k4xOgNnPc3	první
projevům	projev	k1gInPc3	projev
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
až	až	k9	až
10	[number]	k4	10
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
začíná	začínat	k5eAaImIp3nS	začínat
obvykle	obvykle	k6eAd1	obvykle
vysokou	vysoký	k2eAgFnSc7d1	vysoká
teplotou	teplota	k1gFnSc7	teplota
(	(	kIx(	(
<g/>
nad	nad	k7c4	nad
38	[number]	k4	38
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bolestmi	bolest	k1gFnPc7	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
celkovou	celkový	k2eAgFnSc7d1	celková
únavou	únava	k1gFnSc7	únava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
dnech	den	k1gInPc6	den
se	se	k3xPyFc4	se
přidružuje	přidružovat	k5eAaImIp3nS	přidružovat
suchý	suchý	k2eAgInSc4d1	suchý
kašel	kašel	k1gInSc4	kašel
a	a	k8xC	a
dýchací	dýchací	k2eAgFnPc4d1	dýchací
obtíže	obtíž	k1gFnPc4	obtíž
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
postižených	postižený	k1gMnPc2	postižený
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
10	[number]	k4	10
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přičítána	přičítán	k2eAgFnSc1d1	přičítána
především	především	k9	především
příliš	příliš	k6eAd1	příliš
silné	silný	k2eAgInPc1d1	silný
imunitní	imunitní	k2eAgFnSc4d1	imunitní
reakci	reakce	k1gFnSc4	reakce
vyvolané	vyvolaný	k2eAgFnSc2d1	vyvolaná
cytokiny	cytokina	k1gFnSc2	cytokina
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
cytokinová	cytokinová	k1gFnSc1	cytokinová
bouře	bouře	k1gFnSc1	bouře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
má	mít	k5eAaImIp3nS	mít
přílišná	přílišný	k2eAgFnSc1d1	přílišná
produkce	produkce	k1gFnSc1	produkce
interferonu	interferon	k1gInSc2	interferon
gama	gama	k1gNnSc1	gama
lidskými	lidský	k2eAgFnPc7d1	lidská
buňkami	buňka	k1gFnPc7	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Původce	původce	k1gMnSc1	původce
nákazy	nákaza	k1gFnSc2	nákaza
–	–	k?	–
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
koronavirů	koronavir	k1gMnPc2	koronavir
–	–	k?	–
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
zejména	zejména	k9	zejména
kapénkovou	kapénkový	k2eAgFnSc7d1	kapénková
infekcí	infekce	k1gFnSc7	infekce
při	při	k7c6	při
úzkém	úzký	k2eAgInSc6d1	úzký
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
nemocnou	mocný	k2eNgFnSc7d1	mocný
osobou	osoba	k1gFnSc7	osoba
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
různých	různý	k2eAgInPc2d1	různý
předmětů	předmět	k1gInPc2	předmět
potřísněných	potřísněný	k2eAgInPc2d1	potřísněný
sekrety	sekret	k1gInPc7	sekret
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
jinými	jiný	k2eAgFnPc7d1	jiná
tělesnými	tělesný	k2eAgFnPc7d1	tělesná
tekutinami	tekutina	k1gFnPc7	tekutina
nebo	nebo	k8xC	nebo
stolicí	stolice	k1gFnSc7	stolice
<g/>
.	.	kIx.	.
</s>
<s>
Předběžné	předběžný	k2eAgFnPc1d1	předběžná
zkušenosti	zkušenost	k1gFnPc1	zkušenost
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
virus	virus	k1gInSc1	virus
přežívá	přežívat	k5eAaImIp3nS	přežívat
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
délce	délka	k1gFnSc6	délka
přežívání	přežívání	k1gNnSc2	přežívání
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
množství	množství	k1gNnSc1	množství
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiná	k1gFnPc4	jiná
charakter	charakter	k1gInSc4	charakter
potřísněného	potřísněný	k2eAgInSc2d1	potřísněný
povrchu	povrch	k1gInSc2	povrch
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
druh	druh	k1gInSc1	druh
tělesné	tělesný	k2eAgFnSc2d1	tělesná
tekutiny	tekutina	k1gFnSc2	tekutina
obsahující	obsahující	k2eAgInSc1d1	obsahující
virus	virus	k1gInSc1	virus
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
vlhkost	vlhkost	k1gFnSc1	vlhkost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nemoc	nemoc	k1gFnSc1	nemoc
je	být	k5eAaImIp3nS	být
zoonóza	zoonóza	k1gFnSc1	zoonóza
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
rezervoár	rezervoár	k1gInSc1	rezervoár
jí	on	k3xPp3gFnSc2	on
slouží	sloužit	k5eAaImIp3nS	sloužit
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
netopýrů	netopýr	k1gMnPc2	netopýr
(	(	kIx(	(
<g/>
např.	např.	kA	např.
čínský	čínský	k2eAgInSc1d1	čínský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
infekce	infekce	k1gFnSc1	infekce
lidí	člověk	k1gMnPc2	člověk
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
nepřímo	přímo	k6eNd1	přímo
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
cibetky	cibetka	k1gFnPc4	cibetka
nebo	nebo	k8xC	nebo
psíky	psík	k1gMnPc4	psík
mývalovité	mývalovitý	k2eAgNnSc1d1	mývalovitý
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
SARS	SARS	kA	SARS
je	být	k5eAaImIp3nS	být
především	především	k6eAd1	především
symptomatická	symptomatický	k2eAgFnSc1d1	symptomatická
<g/>
,	,	kIx,	,
provádí	provádět	k5eAaImIp3nS	provádět
se	se	k3xPyFc4	se
srážení	srážení	k1gNnSc1	srážení
horečky	horečka	k1gFnSc2	horečka
pomocí	pomocí	k7c2	pomocí
antipyretik	antipyretikum	k1gNnPc2	antipyretikum
<g/>
,	,	kIx,	,
dodává	dodávat	k5eAaImIp3nS	dodávat
se	se	k3xPyFc4	se
kyslík	kyslík	k1gInSc1	kyslík
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
mechanicky	mechanicky	k6eAd1	mechanicky
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
dýchání	dýchání	k1gNnSc4	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Pacienti	pacient	k1gMnPc1	pacient
infikovaní	infikovaný	k2eAgMnPc1d1	infikovaný
koronavirem	koronavirma	k1gFnPc2	koronavirma
SARS	SARS	kA	SARS
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
izolováni	izolován	k2eAgMnPc1d1	izolován
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejlépe	dobře	k6eAd3	dobře
drženi	držen	k2eAgMnPc1d1	držen
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
s	s	k7c7	s
podtlakem	podtlak	k1gInSc7	podtlak
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
šíření	šíření	k1gNnSc4	šíření
viru	vir	k1gInSc2	vir
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
neexistuje	existovat	k5eNaImIp3nS	existovat
účinný	účinný	k2eAgInSc4d1	účinný
lék	lék	k1gInSc4	lék
proti	proti	k7c3	proti
SARS	SARS	kA	SARS
testovaný	testovaný	k2eAgInSc4d1	testovaný
na	na	k7c6	na
lidech	lid	k1gInPc6	lid
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
monoklonální	monoklonální	k2eAgFnPc1d1	monoklonální
protilátky	protilátka	k1gFnPc1	protilátka
a	a	k8xC	a
léky	lék	k1gInPc1	lék
blokující	blokující	k2eAgFnSc4d1	blokující
činnost	činnost	k1gFnSc4	činnost
virové	virový	k2eAgFnSc2d1	virová
helikázy	helikáza	k1gFnSc2	helikáza
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
replikaci	replikace	k1gFnSc3	replikace
viru	vir	k1gInSc2	vir
<g/>
;	;	kIx,	;
tyto	tento	k3xDgInPc1	tento
léky	lék	k1gInPc1	lék
ovšem	ovšem	k9	ovšem
zatím	zatím	k6eAd1	zatím
nebyly	být	k5eNaImAgInP	být
schváleny	schválit	k5eAaPmNgInP	schválit
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
na	na	k7c6	na
lidech	člověk	k1gMnPc6	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Individuální	individuální	k2eAgFnSc1d1	individuální
ochrana	ochrana	k1gFnSc1	ochrana
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c4	v
dodržování	dodržování	k1gNnSc4	dodržování
běžných	běžný	k2eAgFnPc2d1	běžná
hygienických	hygienický	k2eAgFnPc2d1	hygienická
zásad	zásada	k1gFnPc2	zásada
<g/>
,	,	kIx,	,
uplatňovaných	uplatňovaný	k2eAgFnPc2d1	uplatňovaná
zejména	zejména	k9	zejména
při	při	k7c6	při
epidemiích	epidemie	k1gFnPc6	epidemie
akutních	akutní	k2eAgNnPc2d1	akutní
onemocnění	onemocnění	k1gNnPc2	onemocnění
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
důležité	důležitý	k2eAgNnSc1d1	důležité
je	on	k3xPp3gNnSc4	on
pečlivé	pečlivý	k2eAgNnSc4d1	pečlivé
mytí	mytí	k1gNnSc4	mytí
rukou	ruka	k1gFnSc7	ruka
teplou	teplý	k2eAgFnSc7d1	teplá
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
mýdlem	mýdlo	k1gNnSc7	mýdlo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
šíření	šíření	k1gNnSc2	šíření
nákazy	nákaza	k1gFnSc2	nákaza
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zamezit	zamezit	k5eAaPmF	zamezit
vzniku	vznik	k1gInSc3	vznik
infekčního	infekční	k2eAgInSc2d1	infekční
aerosolu	aerosol	k1gInSc2	aerosol
při	při	k7c6	při
kašli	kašel	k1gInSc6	kašel
a	a	k8xC	a
kýchání	kýchání	k1gNnSc6	kýchání
a	a	k8xC	a
vyhýbat	vyhýbat	k5eAaImF	vyhýbat
se	se	k3xPyFc4	se
dotyku	dotyk	k1gInSc6	dotyk
úst	ústa	k1gNnPc2	ústa
<g/>
,	,	kIx,	,
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
očí	oko	k1gNnPc2	oko
nemytýma	mytý	k2eNgFnPc7d1	nemytá
rukama	ruka	k1gFnPc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
FERENČÍK	FERENČÍK	kA	FERENČÍK
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
;	;	kIx,	;
ROVENSKÝ	ROVENSKÝ	kA	ROVENSKÝ
<g/>
,	,	kIx,	,
J	J	kA	J
<g/>
;	;	kIx,	;
SHOENFELD	SHOENFELD	kA	SHOENFELD
<g/>
,	,	kIx,	,
Y	Y	kA	Y
<g/>
;	;	kIx,	;
MAŤHA	MAŤHA	kA	MAŤHA
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Imunitní	imunitní	k2eAgInSc1d1	imunitní
systém	systém	k1gInSc1	systém
<g/>
;	;	kIx,	;
informace	informace	k1gFnSc1	informace
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
SARS	SARS	kA	SARS
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
