<p>
<s>
Ráz	ráz	k1gInSc1	ráz
je	být	k5eAaImIp3nS	být
neznělá	znělý	k2eNgFnSc1d1	neznělá
glotální	glotální	k2eAgFnSc1d1	glotální
ploziva	ploziva	k1gFnSc1	ploziva
<g/>
,	,	kIx,	,
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
souhláska	souhláska	k1gFnSc1	souhláska
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
fonetické	fonetický	k2eAgFnSc6d1	fonetická
abecedě	abeceda	k1gFnSc6	abeceda
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
symbolem	symbol	k1gInSc7	symbol
ʔ	ʔ	k?	ʔ
<g/>
,	,	kIx,	,
číselné	číselný	k2eAgNnSc1d1	číselné
označení	označení	k1gNnSc1	označení
IPA	IPA	kA	IPA
je	být	k5eAaImIp3nS	být
113	[number]	k4	113
<g/>
,	,	kIx,	,
ekvivalentním	ekvivalentní	k2eAgInSc7d1	ekvivalentní
symbolem	symbol	k1gInSc7	symbol
v	v	k7c6	v
SAMPA	SAMPA	kA	SAMPA
je	být	k5eAaImIp3nS	být
?	?	kIx.	?
</s>
<s>
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Způsob	způsob	k1gInSc1	způsob
artikulace	artikulace	k1gFnSc2	artikulace
<g/>
:	:	kIx,	:
ražená	ražený	k2eAgFnSc1d1	ražená
souhláska	souhláska	k1gFnSc1	souhláska
(	(	kIx(	(
<g/>
ploziva	ploziva	k1gFnSc1	ploziva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářet	k5eAaImIp3nS	vytvářet
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
uzávěry	uzávěra	k1gFnSc2	uzávěra
(	(	kIx(	(
<g/>
okluze	okluze	k1gFnSc1	okluze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
brání	bránit	k5eAaImIp3nS	bránit
proudění	proudění	k1gNnSc4	proudění
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
posléze	posléze	k6eAd1	posléze
uvolněna	uvolněn	k2eAgFnSc1d1	uvolněna
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
zvukový	zvukový	k2eAgInSc4d1	zvukový
dojem	dojem	k1gInSc4	dojem
–	–	k?	–
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
též	též	k6eAd1	též
označení	označení	k1gNnSc4	označení
závěrová	závěrový	k2eAgFnSc1d1	závěrová
souhláska	souhláska	k1gFnSc1	souhláska
(	(	kIx(	(
<g/>
okluziva	okluziva	k1gFnSc1	okluziva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Místo	místo	k7c2	místo
artikulace	artikulace	k1gFnSc2	artikulace
<g/>
:	:	kIx,	:
hlasivková	hlasivkový	k2eAgFnSc1d1	hlasivková
souhláska	souhláska	k1gFnSc1	souhláska
(	(	kIx(	(
<g/>
glotála	glotála	k1gFnSc1	glotála
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uzávěra	uzávěra	k1gFnSc1	uzávěra
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
v	v	k7c6	v
hlasivkách	hlasivka	k1gFnPc6	hlasivka
<g/>
,	,	kIx,	,
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
není	být	k5eNaImIp3nS	být
možná	možná	k9	možná
znělá	znělý	k2eAgFnSc1d1	znělá
realizace	realizace	k1gFnSc1	realizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znělost	znělost	k1gFnSc1	znělost
<g/>
:	:	kIx,	:
neznělá	znělý	k2eNgFnSc1d1	neznělá
souhláska	souhláska	k1gFnSc1	souhláska
–	–	k?	–
při	při	k7c6	při
artikulaci	artikulace	k1gFnSc6	artikulace
jsou	být	k5eAaImIp3nP	být
hlasivky	hlasivka	k1gFnPc4	hlasivka
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
pulmická	pulmický	k2eAgFnSc1d1	pulmický
hláska	hláska	k1gFnSc1	hláska
nemá	mít	k5eNaImIp3nS	mít
znělý	znělý	k2eAgInSc4d1	znělý
protějšek	protějšek	k1gInSc4	protějšek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ústní	ústní	k2eAgFnSc1d1	ústní
souhláska	souhláska	k1gFnSc1	souhláska
–	–	k?	–
vzduch	vzduch	k1gInSc1	vzduch
prochází	procházet	k5eAaImIp3nS	procházet
při	při	k7c6	při
artikulaci	artikulace	k1gFnSc6	artikulace
ústní	ústní	k2eAgFnSc7d1	ústní
dutinou	dutina	k1gFnSc7	dutina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Středová	středový	k2eAgFnSc1d1	středová
souhláska	souhláska	k1gFnSc1	souhláska
–	–	k?	–
vzduch	vzduch	k1gInSc1	vzduch
proudí	proudit	k5eAaPmIp3nS	proudit
převážně	převážně	k6eAd1	převážně
přes	přes	k7c4	přes
střed	střed	k1gInSc4	střed
jazyka	jazyk	k1gInSc2	jazyk
spíše	spíše	k9	spíše
než	než	k8xS	než
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gInPc4	jeho
boky	bok	k1gInPc4	bok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pulmonická	Pulmonický	k2eAgFnSc1d1	Pulmonický
egresivní	egresivní	k2eAgFnSc1d1	egresivní
hláska	hláska	k1gFnSc1	hláska
–	–	k?	–
vzduch	vzduch	k1gInSc1	vzduch
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
artikulaci	artikulace	k1gFnSc6	artikulace
vytlačován	vytlačován	k2eAgInSc4d1	vytlačován
z	z	k7c2	z
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
bývá	bývat	k5eAaImIp3nS	bývat
ráz	ráz	k1gInSc4	ráz
(	(	kIx(	(
<g/>
předraz	předrazit	k5eAaPmRp2nS	předrazit
<g/>
)	)	kIx)	)
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
hlasový	hlasový	k2eAgInSc4d1	hlasový
začátek	začátek	k1gInSc4	začátek
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
samostatným	samostatný	k2eAgInSc7d1	samostatný
fonémem	foném	k1gInSc7	foném
a	a	k8xC	a
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
se	se	k3xPyFc4	se
nezaznamenává	zaznamenávat	k5eNaImIp3nS	zaznamenávat
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
rázu	ráz	k1gInSc2	ráz
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
je	být	k5eAaImIp3nS	být
fakultativní	fakultativní	k2eAgMnSc1d1	fakultativní
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jen	jen	k9	jen
někdy	někdy	k6eAd1	někdy
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
artikulaci	artikulace	k1gFnSc6	artikulace
samohlásky	samohláska	k1gFnSc2	samohláska
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
morfému	morfém	k1gInSc2	morfém
<g/>
)	)	kIx)	)
plní	plnit	k5eAaImIp3nS	plnit
ráz	ráz	k1gInSc1	ráz
funkci	funkce	k1gFnSc4	funkce
protetické	protetický	k2eAgInPc1d1	protetický
hlásky	hlásek	k1gInPc1	hlásek
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
tak	tak	k6eAd1	tak
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vyznačení	vyznačení	k1gNnSc3	vyznačení
předělu	předěl	k1gInSc2	předěl
mezi	mezi	k7c7	mezi
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
uvnitř	uvnitř	k7c2	uvnitř
složenin	složenina	k1gFnPc2	složenina
<g/>
.	.	kIx.	.
</s>
<s>
Vkládá	vkládat	k5eAaImIp3nS	vkládat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
mezi	mezi	k7c4	mezi
dvě	dva	k4xCgFnPc4	dva
samohlásky	samohláska	k1gFnPc4	samohláska
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
spolu	spolu	k6eAd1	spolu
netvoří	tvořit	k5eNaImIp3nP	tvořit
dvojhlásku	dvojhláska	k1gFnSc4	dvojhláska
<g/>
,	,	kIx,	,
např.	např.	kA	např.
používat	používat	k5eAaImF	používat
[	[	kIx(	[
<g/>
po	po	k7c4	po
<g/>
.	.	kIx.	.
<g/>
ʔ	ʔ	k?	ʔ
<g/>
:	:	kIx,	:
<g/>
vat	vat	k1gInSc1	vat
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
táta	táta	k1gMnSc1	táta
a	a	k8xC	a
máma	máma	k1gFnSc1	máma
[	[	kIx(	[
<g/>
ta	ten	k3xDgFnSc1	ten
<g/>
:	:	kIx,	:
<g/>
ta	ten	k3xDgFnSc1	ten
ʔ	ʔ	k?	ʔ
ma	ma	k?	ma
<g/>
:	:	kIx,	:
<g/>
ma	ma	k?	ma
<g/>
]	]	kIx)	]
<g/>
<g />
.	.	kIx.	.
</s>
<s>
;	;	kIx,	;
u	u	k7c2	u
slov	slovo	k1gNnPc2	slovo
začínajících	začínající	k2eAgMnPc2d1	začínající
samohláskou	samohláska	k1gFnSc7	samohláska
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
odlišení	odlišení	k1gNnSc3	odlišení
předložky	předložka	k1gFnSc2	předložka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
[	[	kIx(	[
<g/>
s	s	k7c7	s
ʔ	ʔ	k?	ʔ
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
vkládá	vkládat	k5eAaImIp3nS	vkládat
se	se	k3xPyFc4	se
před	před	k7c4	před
samohlásku	samohláska	k1gFnSc4	samohláska
u	u	k7c2	u
druhé	druhý	k4xOgFnSc2	druhý
části	část	k1gFnSc2	část
složeniny	složenina	k1gFnSc2	složenina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
[	[	kIx(	[
<g/>
troj	trojit	k5eAaImRp2nS	trojit
<g/>
.	.	kIx.	.
<g/>
ʔ	ʔ	k?	ʔ
<g/>
:	:	kIx,	:
<g/>
ɦ	ɦ	k?	ɦ
<g/>
:	:	kIx,	:
<g/>
k	k	k7c3	k
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ráz	ráz	k1gInSc1	ráz
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
více	hodně	k6eAd2	hodně
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
v	v	k7c6	v
moravských	moravský	k2eAgNnPc6d1	Moravské
nářečích	nářečí	k1gNnPc6	nářečí
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
bez	bez	k7c2	bez
rázu	ráz	k1gInSc2	ráz
<g/>
,	,	kIx,	,
např.	např.	kA	např.
[	[	kIx(	[
<g/>
troju	troju	k5eAaPmIp1nS	troju
<g/>
:	:	kIx,	:
<g/>
ɦ	ɦ	k?	ɦ
<g/>
:	:	kIx,	:
<g/>
k	k	k7c3	k
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
zokna	zokna	k6eAd1	zokna
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současné	současný	k2eAgFnSc2d1	současná
ortoepické	ortoepický	k2eAgFnSc2d1	ortoepická
normy	norma	k1gFnSc2	norma
je	být	k5eAaImIp3nS	být
ráz	ráz	k1gInSc4	ráz
povinný	povinný	k2eAgInSc4d1	povinný
po	po	k7c6	po
neslabičných	slabičný	k2eNgFnPc6d1	neslabičná
předložkách	předložka	k1gFnPc6	předložka
k	k	k7c3	k
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
,	,	kIx,	,
z	z	k7c2	z
<g/>
,	,	kIx,	,
v	v	k7c6	v
<g/>
,	,	kIx,	,
v	v	k7c6	v
profesionálních	profesionální	k2eAgInPc6d1	profesionální
projevech	projev	k1gInPc6	projev
se	se	k3xPyFc4	se
ale	ale	k9	ale
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
výslovnost	výslovnost	k1gFnSc1	výslovnost
s	s	k7c7	s
rázem	ráz	k1gInSc7	ráz
i	i	k9	i
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
případech	případ	k1gInPc6	případ
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spisovné	spisovný	k2eAgFnSc6d1	spisovná
výslovnosti	výslovnost	k1gFnSc6	výslovnost
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neklade	klást	k5eNaImIp3nS	klást
ráz	ráz	k1gInSc4	ráz
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
cizího	cizí	k2eAgInSc2d1	cizí
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
setkání	setkání	k1gNnSc1	setkání
dvou	dva	k4xCgFnPc2	dva
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
koala	koala	k1gFnSc1	koala
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
(	(	kIx(	(
<g/>
ء	ء	k?	ء
hamza	hamza	k1gFnSc1	hamza
<g/>
)	)	kIx)	)
a	a	k8xC	a
hebrejštině	hebrejština	k1gFnSc6	hebrejština
(	(	kIx(	(
<g/>
ע	ע	k?	ע
<g/>
'	'	kIx"	'
<g/>
ajn	ajn	k?	ajn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k6eAd1	uprostřed
nebo	nebo	k8xC	nebo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přepisu	přepis	k1gInSc6	přepis
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
apostrofem	apostrof	k1gInSc7	apostrof
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ráz	ráz	k1gInSc1	ráz
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
v	v	k7c6	v
polynéských	polynéský	k2eAgInPc6d1	polynéský
jazycích	jazyk	k1gInPc6	jazyk
jako	jako	k8xS	jako
wallisština	wallisština	k1gFnSc1	wallisština
<g/>
,	,	kIx,	,
futunština	futunština	k1gFnSc1	futunština
nebo	nebo	k8xC	nebo
havajština	havajština	k1gFnSc1	havajština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
psané	psaný	k2eAgFnSc6d1	psaná
formě	forma	k1gFnSc6	forma
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
pomocí	pomocí	k7c2	pomocí
znaku	znak	k1gInSc2	znak
'	'	kIx"	'
jako	jako	k8xS	jako
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
ma	ma	k?	ma
<g/>
'	'	kIx"	'
<g/>
uli	uli	k?	uli
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
aho	aho	k?	aho
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
nebo	nebo	k8xC	nebo
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
ahu	ahu	k?	ahu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
ajmarština	ajmarština	k1gFnSc1	ajmarština
<g/>
,	,	kIx,	,
kečuánština	kečuánština	k1gFnSc1	kečuánština
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
také	také	k9	také
označuje	označovat	k5eAaImIp3nS	označovat
apostrofem	apostrof	k1gInSc7	apostrof
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
fonologický	fonologický	k2eAgInSc4d1	fonologický
význam	význam	k1gInSc4	význam
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ajm	ajm	k?	ajm
<g/>
.	.	kIx.	.
t	t	k?	t
<g/>
'	'	kIx"	'
<g/>
ant	ant	k?	ant
<g/>
'	'	kIx"	'
<g/>
a	a	k8xC	a
"	"	kIx"	"
<g/>
chléb	chléb	k1gInSc1	chléb
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
grónštině	grónština	k1gFnSc6	grónština
se	se	k3xPyFc4	se
zřídka	zřídka	k6eAd1	zřídka
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
také	také	k9	také
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
cizího	cizí	k2eAgInSc2d1	cizí
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Encyklopedický	encyklopedický	k2eAgInSc1d1	encyklopedický
slovník	slovník	k1gInSc1	slovník
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Petr	Petr	k1gMnSc1	Petr
Karlík	Karlík	k1gMnSc1	Karlík
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
Nekula	Nekula	k1gMnSc1	Nekula
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Pleskalová	Pleskalová	k1gFnSc1	Pleskalová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
604	[number]	k4	604
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
484	[number]	k4	484
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
S.	S.	kA	S.
368	[number]	k4	368
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
