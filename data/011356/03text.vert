<p>
<s>
Mika	Mik	k1gMnSc2	Mik
Pauli	Paule	k1gFnSc3	Paule
Häkkinen	Häkkinen	k2eAgInSc1d1	Häkkinen
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Vantaa	Vantaa	k1gFnSc1	Vantaa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
finský	finský	k2eAgMnSc1d1	finský
automobilový	automobilový	k2eAgMnSc1d1	automobilový
závodník	závodník	k1gMnSc1	závodník
a	a	k8xC	a
pilot	pilot	k1gMnSc1	pilot
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
Létající	létající	k2eAgMnSc1d1	létající
Fin.	Fin.	k1gMnSc1	Fin.
Je	být	k5eAaImIp3nS	být
dvojnásobným	dvojnásobný	k2eAgMnSc7d1	dvojnásobný
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
úspěších	úspěch	k1gInPc6	úspěch
na	na	k7c6	na
motokárách	motokára	k1gFnPc6	motokára
a	a	k8xC	a
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Macau	Macao	k1gNnSc6	Macao
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gNnSc3	on
těsně	těsně	k6eAd1	těsně
uniklo	uniknout	k5eAaPmAgNnS	uniknout
vítězství	vítězství	k1gNnSc1	vítězství
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Häkkinen	Häkkinen	k2eAgInSc4d1	Häkkinen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
týmu	tým	k1gInSc3	tým
Lotus	Lotus	kA	Lotus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
strávil	strávit	k5eAaPmAgMnS	strávit
dvě	dva	k4xCgFnPc4	dva
sezóny	sezóna	k1gFnPc4	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
přešel	přejít	k5eAaPmAgInS	přejít
k	k	k7c3	k
týmu	tým	k1gInSc3	tým
McLaren	McLarna	k1gFnPc2	McLarna
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
jako	jako	k8xS	jako
testovací	testovací	k2eAgInSc1d1	testovací
a	a	k8xC	a
náhradní	náhradní	k2eAgMnSc1d1	náhradní
jezdec	jezdec	k1gMnSc1	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kokpitu	kokpit	k1gInSc2	kokpit
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
McLaren	McLarna	k1gFnPc2	McLarna
rozešel	rozejít	k5eAaPmAgInS	rozejít
s	s	k7c7	s
Michaelem	Michael	k1gMnSc7	Michael
Andrettim	Andrettima	k1gFnPc2	Andrettima
po	po	k7c6	po
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Ayrtona	Ayrtona	k1gFnSc1	Ayrtona
Senny	Senny	k?	Senny
do	do	k7c2	do
Williamsu	Williams	k1gInSc2	Williams
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
týmovým	týmový	k2eAgMnSc7d1	týmový
lídrem	lídr	k1gMnSc7	lídr
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Austrálie	Austrálie	k1gFnSc2	Austrálie
1995	[number]	k4	1995
měl	mít	k5eAaImAgInS	mít
Mika	Mik	k1gMnSc2	Mik
těžkou	těžký	k2eAgFnSc4d1	těžká
havárii	havárie	k1gFnSc4	havárie
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
málem	málem	k6eAd1	málem
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
a	a	k8xC	a
první	první	k4xOgNnSc4	první
vítězství	vítězství	k1gNnSc4	vítězství
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
při	při	k7c6	při
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Evropy	Evropa	k1gFnSc2	Evropa
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
a	a	k8xC	a
1999	[number]	k4	1999
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
připsal	připsat	k5eAaPmAgMnS	připsat
dva	dva	k4xCgInPc4	dva
tituly	titul	k1gInPc4	titul
mistra	mistr	k1gMnSc4	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
tisicíletí	tisicíletí	k1gNnSc2	tisicíletí
stále	stále	k6eAd1	stále
bojoval	bojovat	k5eAaImAgInS	bojovat
o	o	k7c4	o
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
ale	ale	k9	ale
získal	získat	k5eAaPmAgMnS	získat
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgInS	připsat
poslední	poslední	k2eAgInSc4d1	poslední
2	[number]	k4	2
výhry	výhra	k1gFnPc1	výhra
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
dává	dávat	k5eAaImIp3nS	dávat
od	od	k7c2	od
závodění	závodění	k1gNnSc2	závodění
přestávku	přestávka	k1gFnSc4	přestávka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
změnila	změnit	k5eAaPmAgFnS	změnit
v	v	k7c4	v
konec	konec	k1gInSc4	konec
kariéry	kariéra	k1gFnSc2	kariéra
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
závodění	závodění	k1gNnSc3	závodění
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
sérii	série	k1gFnSc6	série
DTM	DTM	kA	DTM
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
třikrát	třikrát	k6eAd1	třikrát
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
ve	v	k7c6	v
voze	vůz	k1gInSc6	vůz
Mercedes	mercedes	k1gInSc1	mercedes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
oznámil	oznámit	k5eAaPmAgMnS	oznámit
úplné	úplný	k2eAgNnSc4d1	úplné
ukončení	ukončení	k1gNnSc4	ukončení
závodní	závodní	k2eAgFnSc2d1	závodní
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Počátky	počátek	k1gInPc1	počátek
kariéry	kariéra	k1gFnSc2	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Mika	Mik	k1gMnSc2	Mik
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
městě	město	k1gNnSc6	město
Vantta	Vantt	k1gInSc2	Vantt
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Harri	Harr	k1gFnSc2	Harr
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
krátkovlnným	krátkovlnný	k2eAgInSc7d1	krátkovlnný
radiovým	radiový	k2eAgInSc7d1	radiový
operátorem	operátor	k1gInSc7	operátor
a	a	k8xC	a
příležitostným	příležitostný	k2eAgMnSc7d1	příležitostný
taxikářem	taxikář	k1gMnSc7	taxikář
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
matkou	matka	k1gFnSc7	matka
byla	být	k5eAaImAgFnS	být
Aila	Aila	k1gFnSc1	Aila
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
sekretářka	sekretářka	k1gFnSc1	sekretářka
<g/>
.	.	kIx.	.
</s>
<s>
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sestrou	sestra	k1gFnSc7	sestra
Ninou	Nina	k1gFnSc7	Nina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
starala	starat	k5eAaImAgFnS	starat
O	o	k7c4	o
Mikovu	Mikův	k2eAgFnSc4d1	Mikova
fan	fana	k1gFnPc2	fana
stránku	stránka	k1gFnSc4	stránka
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
dítě	dítě	k1gNnSc4	dítě
žil	žít	k5eAaImAgMnS	žít
Mika	Mik	k1gMnSc4	Mik
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
ulici	ulice	k1gFnSc6	ulice
jako	jako	k8xS	jako
další	další	k2eAgMnSc1d1	další
jezdec	jezdec	k1gMnSc1	jezdec
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
Mika	Mik	k1gMnSc2	Mik
Salo	Salo	k1gMnSc1	Salo
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stali	stát	k5eAaPmAgMnP	stát
dobrými	dobrý	k2eAgMnPc7d1	dobrý
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Häkkinen	Häkkinen	k2eAgMnSc1d1	Häkkinen
jako	jako	k8xC	jako
malý	malý	k2eAgMnSc1d1	malý
také	také	k9	také
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
v	v	k7c6	v
akrobatické	akrobatický	k2eAgFnSc6d1	akrobatická
show	show	k1gFnSc6	show
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
též	též	k6eAd1	též
věnoval	věnovat	k5eAaImAgMnS	věnovat
akrobatické	akrobatický	k2eAgFnSc3d1	akrobatická
gymnastice	gymnastika	k1gFnSc3	gymnastika
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
akrobacii	akrobacie	k1gFnSc3	akrobacie
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
.	.	kIx.	.
</s>
<s>
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
musel	muset	k5eAaImAgMnS	muset
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
věnovat	věnovat	k5eAaImF	věnovat
akrobacii	akrobacie	k1gFnSc3	akrobacie
nebo	nebo	k8xC	nebo
závodění	závodění	k1gNnSc3	závodění
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
závodním	závodní	k2eAgMnSc7d1	závodní
jezdcem	jezdec	k1gMnSc7	jezdec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
před	před	k7c7	před
Formulí	formule	k1gFnSc7	formule
1	[number]	k4	1
==	==	k?	==
</s>
</p>
<p>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Mikovi	Mikův	k2eAgMnPc1d1	Mikův
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
pronajali	pronajmout	k5eAaPmAgMnP	pronajmout
motokáru	motokára	k1gFnSc4	motokára
a	a	k8xC	a
brali	brát	k5eAaImAgMnP	brát
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
nedaleko	nedaleko	k7c2	nedaleko
jejich	jejich	k3xOp3gInSc2	jejich
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
se	se	k3xPyFc4	se
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
havárie	havárie	k1gFnSc2	havárie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nic	nic	k3yNnSc1	nic
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
počáteční	počáteční	k2eAgInSc4d1	počáteční
neúspěch	neúspěch	k1gInSc4	neúspěch
chtěl	chtít	k5eAaImAgMnS	chtít
Häkkinen	Häkkinen	k1gInSc4	Häkkinen
se	s	k7c7	s
závoděním	závodění	k1gNnSc7	závodění
pokračovat	pokračovat	k5eAaImF	pokračovat
a	a	k8xC	a
poté	poté	k6eAd1	poté
co	co	k3yInSc1	co
neustále	neustále	k6eAd1	neustále
přemlouval	přemlouvat	k5eAaImAgMnS	přemlouvat
své	svůj	k3xOyFgMnPc4	svůj
rodiče	rodič	k1gMnPc4	rodič
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
jeho	jeho	k3xOp3gNnSc1	jeho
přání	přání	k1gNnSc1	přání
splnilo	splnit	k5eAaPmAgNnS	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Mikův	Mikův	k2eAgMnSc1d1	Mikův
otec	otec	k1gMnSc1	otec
mu	on	k3xPp3gMnSc3	on
koupil	koupit	k5eAaPmAgMnS	koupit
první	první	k4xOgFnSc4	první
motokáru	motokára	k1gFnSc4	motokára
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
motokára	motokára	k1gFnSc1	motokára
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
dříve	dříve	k6eAd2	dříve
závodil	závodit	k5eAaImAgMnS	závodit
Henri	Henre	k1gFnSc4	Henre
Toivonen	Toivonen	k2eAgMnSc1d1	Toivonen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
5	[number]	k4	5
titulů	titul	k1gInPc2	titul
na	na	k7c6	na
motokárách	motokára	k1gFnPc6	motokára
<g/>
.	.	kIx.	.
</s>
<s>
Mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
Keke	Kek	k1gInSc2	Kek
Rosberg	Rosberg	k1gInSc1	Rosberg
pomohl	pomoct	k5eAaPmAgInS	pomoct
Häkkinenovi	Häkkinen	k1gMnSc3	Häkkinen
sehnat	sehnat	k5eAaPmF	sehnat
sponzory	sponzor	k1gMnPc4	sponzor
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mu	on	k3xPp3gMnSc3	on
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
v	v	k7c6	v
juniorských	juniorský	k2eAgFnPc6d1	juniorská
kategoriích	kategorie	k1gFnPc6	kategorie
otevřených	otevřený	k2eAgInPc2d1	otevřený
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nový	nový	k2eAgMnSc1d1	nový
létající	létající	k2eAgMnSc1d1	létající
Fin	Fin	k1gMnSc1	Fin
<g/>
"	"	kIx"	"
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
třikrát	třikrát	k6eAd1	třikrát
Skandinávský	skandinávský	k2eAgInSc4d1	skandinávský
šampionát	šampionát	k1gInSc4	šampionát
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
šampionát	šampionát	k1gInSc1	šampionát
Opel	opel	k1gInSc4	opel
Lotus	Lotus	kA	Lotus
Euroseries	Euroseries	k1gInSc1	Euroseries
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
Britskou	britský	k2eAgFnSc4d1	britská
Formuli	formule	k1gFnSc4	formule
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
v	v	k7c6	v
domě	dům	k1gInSc6	dům
společně	společně	k6eAd1	společně
s	s	k7c7	s
týmovým	týmový	k2eAgMnSc7d1	týmový
kolegou	kolega	k1gMnSc7	kolega
ze	z	k7c2	z
stáje	stáj	k1gFnSc2	stáj
West	Westa	k1gFnPc2	Westa
Surrey	Surrea	k1gFnSc2	Surrea
Racing	Racing	k1gInSc1	Racing
a	a	k8xC	a
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
jezdcem	jezdec	k1gMnSc7	jezdec
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
,	,	kIx,	,
Allanem	Allan	k1gMnSc7	Allan
McNishem	McNish	k1gInSc7	McNish
<g/>
.	.	kIx.	.
</s>
<s>
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
byl	být	k5eAaImAgInS	být
blízko	blízko	k7c2	blízko
vítězství	vítězství	k1gNnSc2	vítězství
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
Macau	Macao	k1gNnSc6	Macao
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
havaroval	havarovat	k5eAaPmAgMnS	havarovat
s	s	k7c7	s
Michaelem	Michael	k1gMnSc7	Michael
Schumacherem	Schumacher	k1gMnSc7	Schumacher
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
výkon	výkon	k1gInSc1	výkon
ale	ale	k9	ale
znamenal	znamenat	k5eAaImAgInS	znamenat
posun	posun	k1gInSc1	posun
do	do	k7c2	do
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
k	k	k7c3	k
týmu	tým	k1gInSc3	tým
Lotus	Lotus	kA	Lotus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
Lotus	Lotus	kA	Lotus
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
1991	[number]	k4	1991
====	====	k?	====
</s>
</p>
<p>
<s>
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
týmu	tým	k1gInSc3	tým
Lotus	Lotus	kA	Lotus
<g/>
.	.	kIx.	.
</s>
<s>
Debutoval	debutovat	k5eAaBmAgMnS	debutovat
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ve	v	k7c6	v
Phoenixu	Phoenix	k1gInSc6	Phoenix
<g/>
,	,	kIx,	,
kvalifikoval	kvalifikovat	k5eAaBmAgMnS	kvalifikovat
se	se	k3xPyFc4	se
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
pozici	pozice	k1gFnSc6	pozice
by	by	kYmCp3nP	by
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
dojel	dojet	k5eAaPmAgMnS	dojet
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
neselhal	selhat	k5eNaPmAgInS	selhat
motor	motor	k1gInSc1	motor
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
první	první	k4xOgInPc4	první
body	bod	k1gInPc4	bod
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
závody	závod	k1gInPc4	závod
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Imole	Imola	k1gFnSc6	Imola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
startu	start	k1gInSc6	start
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
místa	místo	k1gNnSc2	místo
dojel	dojet	k5eAaPmAgMnS	dojet
pátý	pátý	k4xOgMnSc1	pátý
<g/>
,	,	kIx,	,
na	na	k7c4	na
vítěze	vítěz	k1gMnSc4	vítěz
<g/>
,	,	kIx,	,
Ayrtona	Ayrton	k1gMnSc4	Ayrton
Sennu	Senna	k1gMnSc4	Senna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ztratil	ztratit	k5eAaPmAgMnS	ztratit
3	[number]	k4	3
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
body	bod	k1gInPc1	bod
už	už	k6eAd1	už
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
nepřidal	přidat	k5eNaPmAgMnS	přidat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1992	[number]	k4	1992
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
další	další	k2eAgFnSc6d1	další
sezóně	sezóna	k1gFnSc6	sezóna
byl	být	k5eAaImAgMnS	být
Mikův	Mikův	k2eAgMnSc1d1	Mikův
týmový	týmový	k2eAgMnSc1d1	týmový
kolega	kolega	k1gMnSc1	kolega
Johnny	Johnna	k1gFnSc2	Johnna
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
.	.	kIx.	.
</s>
<s>
Häkkinen	Häkkinen	k2eAgMnSc1d1	Häkkinen
se	se	k3xPyFc4	se
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
a	a	k8xC	a
bodoval	bodovat	k5eAaImAgMnS	bodovat
celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
6	[number]	k4	6
závodech	závod	k1gInPc6	závod
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
dojel	dojet	k5eAaPmAgMnS	dojet
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
poháru	pohár	k1gInSc6	pohár
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
McLaren	McLarna	k1gFnPc2	McLarna
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
1993	[number]	k4	1993
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
přešel	přejít	k5eAaPmAgInS	přejít
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
do	do	k7c2	do
týmu	tým	k1gInSc2	tým
McLaren	McLarna	k1gFnPc2	McLarna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
stát	stát	k5eAaImF	stát
nejprve	nejprve	k6eAd1	nejprve
testovacím	testovací	k2eAgMnSc7d1	testovací
jezdcem	jezdec	k1gMnSc7	jezdec
a	a	k8xC	a
až	až	k9	až
přijde	přijít	k5eAaPmIp3nS	přijít
jeho	on	k3xPp3gInSc4	on
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
se	se	k3xPyFc4	se
posunout	posunout	k5eAaPmF	posunout
do	do	k7c2	do
role	role	k1gFnSc2	role
závodního	závodní	k1gMnSc4	závodní
pilota	pilot	k1gMnSc4	pilot
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
hostující	hostující	k2eAgMnSc1d1	hostující
jezdec	jezdec	k1gMnSc1	jezdec
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
v	v	k7c6	v
sérii	série	k1gFnSc6	série
Porsche	Porsche	k1gNnSc2	Porsche
Supercup	Supercup	k1gInSc1	Supercup
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
závod	závod	k1gInSc1	závod
opanoval	opanovat	k5eAaPmAgInS	opanovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
sen	sen	k1gInSc1	sen
usednout	usednout	k5eAaPmF	usednout
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
opět	opět	k6eAd1	opět
do	do	k7c2	do
kokpitu	kokpit	k1gInSc2	kokpit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vyplnil	vyplnit	k5eAaPmAgInS	vyplnit
po	po	k7c6	po
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
Monze	Monz	k1gInSc6	Monz
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
po	po	k7c6	po
špatných	špatný	k2eAgInPc6d1	špatný
výsledcích	výsledek	k1gInPc6	výsledek
tým	tým	k1gInSc1	tým
opustil	opustit	k5eAaPmAgMnS	opustit
Michael	Michael	k1gMnSc1	Michael
Andretti	Andretť	k1gFnSc2	Andretť
<g/>
.	.	kIx.	.
</s>
<s>
Debut	debut	k1gInSc1	debut
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
stáji	stáj	k1gFnSc6	stáj
si	se	k3xPyFc3	se
odbyl	odbýt	k5eAaPmAgMnS	odbýt
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
a	a	k8xC	a
hned	hned	k6eAd1	hned
všechny	všechen	k3xTgFnPc4	všechen
ohromil	ohromit	k5eAaPmAgMnS	ohromit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
porazil	porazit	k5eAaPmAgMnS	porazit
svého	svůj	k3xOyFgMnSc4	svůj
týmového	týmový	k2eAgMnSc4d1	týmový
kolegu	kolega	k1gMnSc4	kolega
Ayrtona	Ayrton	k1gMnSc4	Ayrton
Sennu	Senna	k1gMnSc4	Senna
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
<g/>
,	,	kIx,	,
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
zatáčce	zatáčka	k1gFnSc6	zatáčka
okruhu	okruh	k1gInSc2	okruh
přehnal	přehnat	k5eAaPmAgInS	přehnat
svou	svůj	k3xOyFgFnSc4	svůj
snahu	snaha	k1gFnSc4	snaha
<g/>
,	,	kIx,	,
vyjel	vyjet	k5eAaPmAgInS	vyjet
z	z	k7c2	z
trati	trať	k1gFnSc2	trať
a	a	k8xC	a
narazil	narazit	k5eAaPmAgMnS	narazit
do	do	k7c2	do
boxové	boxový	k2eAgFnSc2d1	boxová
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
jezdil	jezdit	k5eAaImAgMnS	jezdit
na	na	k7c6	na
bodovaných	bodovaný	k2eAgFnPc6d1	bodovaná
příčkách	příčka	k1gFnPc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Suzuka	Suzuk	k1gMnSc2	Suzuk
poprvé	poprvé	k6eAd1	poprvé
vystoupal	vystoupat	k5eAaPmAgMnS	vystoupat
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dojel	dojet	k5eAaPmAgMnS	dojet
15	[number]	k4	15
sekund	sekunda	k1gFnPc2	sekunda
za	za	k7c4	za
Sennou	senný	k2eAgFnSc4d1	senná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
sezóny	sezóna	k1gFnSc2	sezóna
testoval	testovat	k5eAaImAgInS	testovat
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
i	i	k8xC	i
Senna	Senna	k1gFnSc1	Senna
motor	motor	k1gInSc4	motor
Lamborghini	Lamborghin	k2eAgMnPc1d1	Lamborghin
V12	V12	k1gMnPc1	V12
v	v	k7c6	v
modifikovaném	modifikovaný	k2eAgInSc6d1	modifikovaný
voze	vůz	k1gInSc6	vůz
McLaren	McLarna	k1gFnPc2	McLarna
MP	MP	kA	MP
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
"	"	kIx"	"
<g/>
MP	MP	kA	MP
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
<g/>
B	B	kA	B
<g/>
"	"	kIx"	"
na	na	k7c6	na
okruzích	okruh	k1gInPc6	okruh
Estoril	Estorila	k1gFnPc2	Estorila
a	a	k8xC	a
Silverstone	Silverston	k1gInSc5	Silverston
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
jezdci	jezdec	k1gMnPc1	jezdec
byli	být	k5eAaImAgMnP	být
motorem	motor	k1gInSc7	motor
zaujati	zaujmout	k5eAaPmNgMnP	zaujmout
<g/>
,	,	kIx,	,
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
jezdil	jezdit	k5eAaImAgInS	jezdit
v	v	k7c6	v
Silverstone	Silverston	k1gInSc5	Silverston
o	o	k7c4	o
1,4	[number]	k4	1,4
sekundy	sekunda	k1gFnSc2	sekunda
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
s	s	k7c7	s
normálním	normální	k2eAgInSc7d1	normální
závodním	závodní	k2eAgInSc7d1	závodní
motorem	motor	k1gInSc7	motor
Ford	ford	k1gInSc1	ford
V	v	k7c6	v
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1994	[number]	k4	1994
====	====	k?	====
</s>
</p>
<p>
<s>
Senna	Senna	k6eAd1	Senna
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
týmu	tým	k1gInSc2	tým
Williams	Williamsa	k1gFnPc2	Williamsa
a	a	k8xC	a
Häkkinen	Häkkinna	k1gFnPc2	Häkkinna
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
lídrem	lídr	k1gMnSc7	lídr
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
kolegou	kolega	k1gMnSc7	kolega
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Martin	Martin	k1gMnSc1	Martin
Brundle	Brundle	k1gMnSc1	Brundle
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
McLaren	McLarna	k1gFnPc2	McLarna
také	také	k9	také
změnil	změnit	k5eAaPmAgInS	změnit
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
od	od	k7c2	od
motoru	motor	k1gInSc2	motor
Ford	ford	k1gInSc1	ford
V8	V8	k1gFnSc2	V8
přešel	přejít	k5eAaPmAgInS	přejít
k	k	k7c3	k
motoru	motor	k1gInSc3	motor
Peugeot	peugeot	k1gInSc4	peugeot
V10	V10	k1gMnSc2	V10
(	(	kIx(	(
<g/>
tým	tým	k1gInSc1	tým
chtěl	chtít	k5eAaImAgInS	chtít
údajně	údajně	k6eAd1	údajně
původně	původně	k6eAd1	původně
použít	použít	k5eAaPmF	použít
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
roce	rok	k1gInSc6	rok
testovaný	testovaný	k2eAgInSc4d1	testovaný
motor	motor	k1gInSc4	motor
Lamborghini	Lamborghin	k1gMnPc1	Lamborghin
V	V	kA	V
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
šéf	šéf	k1gMnSc1	šéf
týmu	tým	k1gInSc2	tým
Ron	Ron	k1gMnSc1	Ron
Dennis	Dennis	k1gFnSc2	Dennis
se	se	k3xPyFc4	se
ale	ale	k9	ale
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
pro	pro	k7c4	pro
Peugeot	peugeot	k1gInSc4	peugeot
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
si	se	k3xPyFc3	se
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
připsal	připsat	k5eAaPmAgInS	připsat
6	[number]	k4	6
pódií	pódium	k1gNnPc2	pódium
a	a	k8xC	a
v	v	k7c6	v
šampionátu	šampionát	k1gInSc6	šampionát
skončil	skončit	k5eAaPmAgMnS	skončit
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
26	[number]	k4	26
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1995	[number]	k4	1995
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
začalo	začít	k5eAaPmAgNnS	začít
dlouhotrvající	dlouhotrvající	k2eAgNnSc1d1	dlouhotrvající
spojení	spojení	k1gNnSc1	spojení
mezi	mezi	k7c7	mezi
týmem	tým	k1gInSc7	tým
McLaren	McLarna	k1gFnPc2	McLarna
a	a	k8xC	a
německou	německý	k2eAgFnSc7d1	německá
automobilkou	automobilka	k1gFnSc7	automobilka
Mercedes-Benz	Mercedes-Benza	k1gFnPc2	Mercedes-Benza
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
sezóny	sezóna	k1gFnSc2	sezóna
si	se	k3xPyFc3	se
Häkkinen	Häkkinen	k1gInSc4	Häkkinen
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
týmový	týmový	k2eAgMnSc1d1	týmový
kolega	kolega	k1gMnSc1	kolega
Nigel	Nigel	k1gMnSc1	Nigel
Mansell	Mansell	k1gMnSc1	Mansell
stěžovali	stěžovat	k5eAaImAgMnP	stěžovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vůz	vůz	k1gInSc1	vůz
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
malý	malý	k2eAgInSc1d1	malý
a	a	k8xC	a
oni	onen	k3xDgMnPc1	onen
trpí	trpět	k5eAaImIp3nP	trpět
údery	úder	k1gInPc4	úder
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
loktů	loket	k1gInPc2	loket
do	do	k7c2	do
boků	bok	k1gInPc2	bok
kokpitu	kokpit	k1gInSc2	kokpit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvě	dva	k4xCgNnPc4	dva
pódiová	pódiový	k2eAgNnPc4d1	pódiové
umístění	umístění	k1gNnPc4	umístění
<g/>
,	,	kIx,	,
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
znamenala	znamenat	k5eAaImAgFnS	znamenat
pro	pro	k7c4	pro
Miku	Mik	k1gMnSc3	Mik
již	již	k9	již
deváté	devátý	k4xOgNnSc4	devátý
umístění	umístění	k1gNnSc4	umístění
mezi	mezi	k7c7	mezi
nejlepšími	dobrý	k2eAgInPc7d3	nejlepší
třemi	tři	k4xCgInPc7	tři
<g/>
.	.	kIx.	.
</s>
<s>
Häkkinen	Häkkinen	k1gInSc4	Häkkinen
zmeškal	zmeškat	k5eAaPmAgMnS	zmeškat
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Pacifiku	Pacifik	k1gInSc2	Pacifik
kvůli	kvůli	k7c3	kvůli
operaci	operace	k1gFnSc3	operace
slepého	slepý	k2eAgNnSc2d1	slepé
střeva	střevo	k1gNnSc2	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
při	při	k7c6	při
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Austrálie	Austrálie	k1gFnSc2	Austrálie
1995	[number]	k4	1995
v	v	k7c6	v
Adelaide	Adelaid	k1gInSc5	Adelaid
<g/>
,	,	kIx,	,
mu	on	k3xPp3gNnSc3	on
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
první	první	k4xOgFnSc2	první
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
selhala	selhat	k5eAaPmAgFnS	selhat
pneumatika	pneumatika	k1gFnSc1	pneumatika
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tvrdému	tvrdý	k2eAgInSc3d1	tvrdý
nárazu	náraz	k1gInSc3	náraz
do	do	k7c2	do
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
utrpěl	utrpět	k5eAaPmAgInS	utrpět
vážná	vážný	k2eAgNnPc4d1	vážné
zranění	zranění	k1gNnPc4	zranění
(	(	kIx(	(
<g/>
prasknutí	prasknutí	k1gNnSc3	prasknutí
lebky	lebka	k1gFnSc2	lebka
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
krvácení	krvácení	k1gNnSc4	krvácení
<g/>
,	,	kIx,	,
zapadlý	zapadlý	k2eAgInSc1d1	zapadlý
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
ucpal	ucpat	k5eAaPmAgInS	ucpat
dýchací	dýchací	k2eAgFnPc4d1	dýchací
cesty	cesta	k1gFnPc4	cesta
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zachráněn	zachránit	k5eAaPmNgInS	zachránit
jen	jen	k6eAd1	jen
díky	díky	k7c3	díky
koniotomii	koniotomie	k1gFnSc3	koniotomie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
hned	hned	k6eAd1	hned
vedle	vedle	k7c2	vedle
trati	trať	k1gFnSc2	trať
provedl	provést	k5eAaPmAgMnS	provést
lékař	lékař	k1gMnSc1	lékař
Sid	Sid	k1gFnSc2	Sid
Watkins	Watkins	k1gInSc1	Watkins
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
incident	incident	k1gInSc1	incident
upevnil	upevnit	k5eAaPmAgInS	upevnit
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
Häkkinenem	Häkkinen	k1gMnSc7	Häkkinen
a	a	k8xC	a
týmovým	týmový	k2eAgMnSc7d1	týmový
šéfem	šéf	k1gMnSc7	šéf
Ronem	ron	k1gInSc7	ron
Dennisem	Dennis	k1gInSc7	Dennis
a	a	k8xC	a
také	také	k9	také
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
posun	posun	k1gInSc1	posun
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
se	se	k3xPyFc4	se
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
plně	plně	k6eAd1	plně
zotavil	zotavit	k5eAaPmAgInS	zotavit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
mohl	moct	k5eAaImAgInS	moct
opět	opět	k6eAd1	opět
závodit	závodit	k5eAaImF	závodit
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
zmeškal	zmeškat	k5eAaPmAgMnS	zmeškat
pouhý	pouhý	k2eAgInSc4d1	pouhý
jeden	jeden	k4xCgInSc4	jeden
závod	závod	k1gInSc4	závod
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kokpitu	kokpit	k1gInSc2	kokpit
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
usedl	usednout	k5eAaPmAgMnS	usednout
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Paul	Paul	k1gMnSc1	Paul
Ricard	Ricard	k1gMnSc1	Ricard
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
poháru	pohár	k1gInSc6	pohár
jezdců	jezdec	k1gMnPc2	jezdec
sedmý	sedmý	k4xOgMnSc1	sedmý
se	se	k3xPyFc4	se
ziskem	zisk	k1gInSc7	zisk
17	[number]	k4	17
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1996	[number]	k4	1996
====	====	k?	====
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
1996	[number]	k4	1996
znamenal	znamenat	k5eAaImAgInS	znamenat
pro	pro	k7c4	pro
McLaren	McLarno	k1gNnPc2	McLarno
zlepšení	zlepšení	k1gNnSc2	zlepšení
<g/>
;	;	kIx,	;
Mercedes-Benz	Mercedes-Benz	k1gInSc1	Mercedes-Benz
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
sezóny	sezóna	k1gFnSc2	sezóna
jako	jako	k9	jako
dodavatel	dodavatel	k1gMnSc1	dodavatel
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
Häkkinen	Häkkinna	k1gFnPc2	Häkkinna
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c4	na
pódiová	pódiový	k2eAgNnPc4d1	pódiové
umístění	umístění	k1gNnSc4	umístění
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
první	první	k4xOgFnSc1	první
vítězství	vítězství	k1gNnSc2	vítězství
mu	on	k3xPp3gMnSc3	on
stále	stále	k6eAd1	stále
unikalo	unikat	k5eAaImAgNnS	unikat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
k	k	k7c3	k
týmu	tým	k1gInSc3	tým
připojil	připojit	k5eAaPmAgMnS	připojit
David	David	k1gMnSc1	David
Coulthard	Coulthard	k1gMnSc1	Coulthard
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
přišel	přijít	k5eAaPmAgInS	přijít
z	z	k7c2	z
týmu	tým	k1gInSc2	tým
Williams	Williamsa	k1gFnPc2	Williamsa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Spa	Spa	k1gFnPc2	Spa
si	se	k3xPyFc3	se
Fin	Fin	k1gMnSc1	Fin
málen	málit	k5eAaImNgMnS	málit
připsal	připsat	k5eAaPmAgMnS	připsat
výhru	výhra	k1gFnSc4	výhra
<g/>
,	,	kIx,	,
když	když	k8xS	když
zvolil	zvolit	k5eAaPmAgMnS	zvolit
jednu	jeden	k4xCgFnSc4	jeden
zastávku	zastávka	k1gFnSc4	zastávka
v	v	k7c6	v
boxech	box	k1gInPc6	box
<g/>
.	.	kIx.	.
</s>
<s>
Havárie	havárie	k1gFnSc1	havárie
Jose	Jose	k1gFnSc1	Jose
Verstappena	Verstappen	k2eAgFnSc1d1	Verstappena
ale	ale	k9	ale
znamenala	znamenat	k5eAaImAgFnS	znamenat
vyjetí	vyjetí	k1gNnSc2	vyjetí
safety	safeta	k1gFnSc2	safeta
caru	car	k1gMnSc3	car
a	a	k8xC	a
zborcení	zborcený	k2eAgMnPc1d1	zborcený
celé	celý	k2eAgFnSc2d1	celá
strategie	strategie	k1gFnPc1	strategie
<g/>
.	.	kIx.	.
</s>
<s>
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
by	by	kYmCp3nS	by
jinak	jinak	k6eAd1	jinak
závod	závod	k1gInSc1	závod
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
o	o	k7c4	o
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
10	[number]	k4	10
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šampionátu	šampionát	k1gInSc6	šampionát
skončil	skončit	k5eAaPmAgInS	skončit
celkově	celkově	k6eAd1	celkově
pátý	pátý	k4xOgMnSc1	pátý
<g/>
,	,	kIx,	,
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
31	[number]	k4	31
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1997	[number]	k4	1997
====	====	k?	====
</s>
</p>
<p>
<s>
McLaren	McLarna	k1gFnPc2	McLarna
vstupoval	vstupovat	k5eAaImAgMnS	vstupovat
do	do	k7c2	do
ročníku	ročník	k1gInSc2	ročník
1997	[number]	k4	1997
se	se	k3xPyFc4	se
sebevědomím	sebevědomí	k1gNnSc7	sebevědomí
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
sponzora	sponzor	k1gMnSc2	sponzor
Marlboro	Marlboro	k1gNnSc6	Marlboro
zmizela	zmizet	k5eAaPmAgFnS	zmizet
a	a	k8xC	a
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
ji	on	k3xPp3gFnSc4	on
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
a	a	k8xC	a
černá	černý	k2eAgFnSc1d1	černá
barva	barva	k1gFnSc1	barva
jiné	jiný	k2eAgFnSc2d1	jiná
tabákové	tabákový	k2eAgFnSc2d1	tabáková
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
West	West	k2eAgInSc1d1	West
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
McLaren	McLarna	k1gFnPc2	McLarna
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c4	na
vítěznou	vítězný	k2eAgFnSc4d1	vítězná
vlnu	vlna	k1gFnSc4	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Coulthard	Coulthard	k1gInSc1	Coulthard
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
hned	hned	k6eAd1	hned
v	v	k7c6	v
úvodní	úvodní	k2eAgFnSc6d1	úvodní
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Austrálie	Austrálie	k1gFnSc2	Austrálie
vítězství	vítězství	k1gNnSc2	vítězství
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3	[number]	k4	3
sezónách	sezóna	k1gFnPc6	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
sezóny	sezóna	k1gFnSc2	sezóna
se	se	k3xPyFc4	se
McLaren	McLarna	k1gFnPc2	McLarna
začal	začít	k5eAaPmAgInS	začít
pravidelně	pravidelně	k6eAd1	pravidelně
umísťovat	umísťovat	k5eAaImF	umísťovat
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
pozicích	pozice	k1gFnPc6	pozice
<g/>
,	,	kIx,	,
v	v	k7c6	v
poháru	pohár	k1gInSc6	pohár
jezdců	jezdec	k1gMnPc2	jezdec
byl	být	k5eAaImAgMnS	být
ale	ale	k8xC	ale
úspěšnější	úspěšný	k2eAgMnSc1d2	úspěšnější
Coulthard	Coulthard	k1gMnSc1	Coulthard
<g/>
.	.	kIx.	.
</s>
<s>
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
měl	mít	k5eAaImAgInS	mít
několik	několik	k4yIc4	několik
možností	možnost	k1gFnPc2	možnost
připsat	připsat	k5eAaPmF	připsat
si	se	k3xPyFc3	se
první	první	k4xOgFnSc4	první
výhru	výhra	k1gFnSc4	výhra
<g/>
,	,	kIx,	,
na	na	k7c6	na
okruzích	okruh	k1gInPc6	okruh
Silverstone	Silverston	k1gInSc5	Silverston
<g/>
,	,	kIx,	,
A	a	k9	a
<g/>
1	[number]	k4	1
<g/>
-Ring	-Ringa	k1gFnPc2	-Ringa
a	a	k8xC	a
Nürburgring	Nürburgring	k1gInSc1	Nürburgring
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jerezu	Jerez	k1gInSc6	Jerez
konečně	konečně	k6eAd1	konečně
protnul	protnout	k5eAaPmAgMnS	protnout
cílovou	cílový	k2eAgFnSc4d1	cílová
čáru	čára	k1gFnSc4	čára
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
ale	ale	k9	ale
poté	poté	k6eAd1	poté
co	co	k3yRnSc4	co
byl	být	k5eAaImAgInS	být
Coulthard	Coulthard	k1gInSc1	Coulthard
požádán	požádán	k2eAgInSc1d1	požádán
aby	aby	kYmCp3nS	aby
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
cestu	cesta	k1gFnSc4	cesta
jemu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
Jacquesovi	Jacques	k1gMnSc6	Jacques
Villeneuvovi	Villeneuva	k1gMnSc6	Villeneuva
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
poškozeným	poškozený	k2eAgInSc7d1	poškozený
vozem	vůz	k1gInSc7	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
bodování	bodování	k1gNnSc6	bodování
šestý	šestý	k4xOgInSc1	šestý
za	za	k7c4	za
27	[number]	k4	27
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1998	[number]	k4	1998
====	====	k?	====
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
zlomovým	zlomový	k2eAgInSc7d1	zlomový
pro	pro	k7c4	pro
kariéru	kariéra	k1gFnSc4	kariéra
Häkkinena	Häkkineno	k1gNnSc2	Häkkineno
<g/>
.	.	kIx.	.
</s>
<s>
Vůz	vůz	k1gInSc1	vůz
McLaren	McLarna	k1gFnPc2	McLarna
MP	MP	kA	MP
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgInSc6	první
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
designoval	designovat	k5eAaPmAgMnS	designovat
Adrian	Adrian	k1gMnSc1	Adrian
Newey	Newea	k1gFnSc2	Newea
<g/>
,	,	kIx,	,
aerodynamik	aerodynamika	k1gFnPc2	aerodynamika
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
minulých	minulý	k2eAgNnPc6d1	Minulé
letech	léto	k1gNnPc6	léto
projektoval	projektovat	k5eAaBmAgMnS	projektovat
i	i	k9	i
výborné	výborný	k2eAgInPc1d1	výborný
vozy	vůz	k1gInPc1	vůz
Williams	Williamsa	k1gFnPc2	Williamsa
<g/>
.	.	kIx.	.
</s>
<s>
Mika	Mik	k1gMnSc4	Mik
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
celkem	celek	k1gInSc7	celek
8	[number]	k4	8
závodů	závod	k1gInPc2	závod
a	a	k8xC	a
ve	v	k7c6	v
13	[number]	k4	13
ze	z	k7c2	z
16	[number]	k4	16
závodů	závod	k1gInPc2	závod
bodoval	bodovat	k5eAaImAgMnS	bodovat
<g/>
.	.	kIx.	.
</s>
<s>
Zajistil	zajistit	k5eAaPmAgInS	zajistit
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
když	když	k8xS	když
získal	získat	k5eAaPmAgMnS	získat
rovných	rovný	k2eAgNnPc2d1	rovné
100	[number]	k4	100
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
skončil	skončit	k5eAaPmAgMnS	skončit
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
s	s	k7c7	s
vozem	vůz	k1gInSc7	vůz
Ferrari	Ferrari	k1gMnSc2	Ferrari
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
86	[number]	k4	86
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
rovněž	rovněž	k9	rovněž
devětkrát	devětkrát	k6eAd1	devětkrát
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
startoval	startovat	k5eAaBmAgMnS	startovat
z	z	k7c2	z
pole	pole	k1gNnSc2	pole
position	position	k1gInSc1	position
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
začala	začít	k5eAaPmAgFnS	začít
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
4	[number]	k4	4
ze	z	k7c2	z
6	[number]	k4	6
prvních	první	k4xOgInPc2	první
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
poté	poté	k6eAd1	poté
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
a	a	k8xC	a
v	v	k7c6	v
bodování	bodování	k1gNnSc6	bodování
se	se	k3xPyFc4	se
Finovi	Fin	k1gMnSc3	Fin
výrazně	výrazně	k6eAd1	výrazně
přiblížil	přiblížit	k5eAaPmAgMnS	přiblížit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
výhry	výhra	k1gFnPc1	výhra
pro	pro	k7c4	pro
Fina	Fin	k1gMnSc2	Fin
přišly	přijít	k5eAaPmAgInP	přijít
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
a	a	k8xC	a
Belgii	Belgie	k1gFnSc6	Belgie
ale	ale	k9	ale
získal	získat	k5eAaPmAgInS	získat
pouhý	pouhý	k2eAgInSc1d1	pouhý
bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
Schumacher	Schumachra	k1gFnPc2	Schumachra
toho	ten	k3xDgInSc2	ten
využil	využít	k5eAaPmAgMnS	využít
ale	ale	k9	ale
jen	jen	k9	jen
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Hungaroring	Hungaroring	k1gInSc1	Hungaroring
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
deštivé	deštivý	k2eAgFnSc6d1	deštivá
a	a	k8xC	a
chaotické	chaotický	k2eAgFnSc6d1	chaotická
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Belgie	Belgie	k1gFnSc2	Belgie
po	po	k7c6	po
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
Davidem	David	k1gMnSc7	David
Coulthardem	Coulthard	k1gMnSc7	Coulthard
nedojel	dojet	k5eNaPmAgMnS	dojet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
závodě	závod	k1gInSc6	závod
na	na	k7c6	na
Monze	Monza	k1gFnSc6	Monza
se	se	k3xPyFc4	se
bodový	bodový	k2eAgInSc1d1	bodový
stav	stav	k1gInSc1	stav
u	u	k7c2	u
obou	dva	k4xCgNnPc2	dva
srovnal	srovnat	k5eAaPmAgMnS	srovnat
na	na	k7c6	na
80	[number]	k4	80
bodech	bod	k1gInPc6	bod
<g/>
.	.	kIx.	.
</s>
<s>
Zbývaly	zbývat	k5eAaImAgFnP	zbývat
pouhé	pouhý	k2eAgInPc4d1	pouhý
2	[number]	k4	2
závody	závod	k1gInPc4	závod
<g/>
.	.	kIx.	.
</s>
<s>
Schumacher	Schumachra	k1gFnPc2	Schumachra
startoval	startovat	k5eAaBmAgInS	startovat
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Nürburgring	Nürburgring	k1gInSc4	Nürburgring
z	z	k7c2	z
pole	pole	k1gNnSc2	pole
position	position	k1gInSc1	position
<g/>
,	,	kIx,	,
závod	závod	k1gInSc1	závod
ale	ale	k8xC	ale
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
mohli	moct	k5eAaImAgMnP	moct
oba	dva	k4xCgMnPc1	dva
bojovat	bojovat	k5eAaImF	bojovat
o	o	k7c4	o
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
favoritem	favorit	k1gMnSc7	favorit
byl	být	k5eAaImAgMnS	být
ale	ale	k9	ale
Häkkinen	Häkkinen	k2eAgMnSc1d1	Häkkinen
<g/>
.	.	kIx.	.
</s>
<s>
Schumacher	Schumachra	k1gFnPc2	Schumachra
sice	sice	k8xC	sice
opět	opět	k6eAd1	opět
získal	získat	k5eAaPmAgMnS	získat
pole	pole	k1gNnSc4	pole
position	position	k1gInSc1	position
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgInS	muset
ale	ale	k9	ale
startovat	startovat	k5eAaBmF	startovat
z	z	k7c2	z
konce	konec	k1gInSc2	konec
startovního	startovní	k2eAgNnSc2d1	startovní
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
při	při	k7c6	při
zahřívacím	zahřívací	k2eAgNnSc6d1	zahřívací
kole	kolo	k1gNnSc6	kolo
postihl	postihnout	k5eAaPmAgInS	postihnout
problém	problém	k1gInSc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Němec	Němec	k1gMnSc1	Němec
ze	z	k7c2	z
závodu	závod	k1gInSc2	závod
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
po	po	k7c6	po
defektu	defekt	k1gInSc6	defekt
pneumatiky	pneumatika	k1gFnSc2	pneumatika
<g/>
.	.	kIx.	.
</s>
<s>
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
závod	závod	k1gInSc4	závod
i	i	k9	i
celý	celý	k2eAgInSc4d1	celý
šampionát	šampionát	k1gInSc4	šampionát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1999	[number]	k4	1999
====	====	k?	====
</s>
</p>
<p>
<s>
Vůz	vůz	k1gInSc1	vůz
MP	MP	kA	MP
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
tužky	tužka	k1gFnSc2	tužka
Adriana	Adrian	k1gMnSc2	Adrian
Neweyho	Newey	k1gMnSc2	Newey
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
minulou	minulý	k2eAgFnSc4d1	minulá
sezónu	sezóna	k1gFnSc4	sezóna
konkurenceschopný	konkurenceschopný	k2eAgMnSc1d1	konkurenceschopný
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
loňského	loňský	k2eAgMnSc2d1	loňský
předchůdce	předchůdce	k1gMnSc2	předchůdce
poruchovější	poruchový	k2eAgNnSc4d2	poruchovější
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
za	za	k7c7	za
titulem	titul	k1gInSc7	titul
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
Ferrari	Ferrari	k1gMnSc2	Ferrari
opět	opět	k6eAd1	opět
vsázel	vsázet	k5eAaImAgMnS	vsázet
na	na	k7c4	na
Michaela	Michael	k1gMnSc4	Michael
Schumachera	Schumacher	k1gMnSc4	Schumacher
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
ale	ale	k8xC	ale
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
sezóny	sezóna	k1gFnSc2	sezóna
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
když	když	k8xS	když
vážně	vážně	k6eAd1	vážně
havaroval	havarovat	k5eAaPmAgMnS	havarovat
při	pře	k1gFnSc3	pře
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Velké	velká	k1gFnSc2	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Zastoupil	zastoupit	k5eAaPmAgMnS	zastoupit
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
jeho	jeho	k3xOp3gMnSc1	jeho
týmový	týmový	k2eAgMnSc1d1	týmový
kolega	kolega	k1gMnSc1	kolega
Eddie	Eddius	k1gMnSc5	Eddius
Irvine	Irvin	k1gMnSc5	Irvin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
s	s	k7c7	s
Häkkinenem	Häkkinen	k1gMnSc7	Häkkinen
bojoval	bojovat	k5eAaImAgMnS	bojovat
o	o	k7c4	o
titul	titul	k1gInSc4	titul
až	až	k9	až
do	do	k7c2	do
posledního	poslední	k2eAgInSc2d1	poslední
závodu	závod	k1gInSc2	závod
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
Mika	Mik	k1gMnSc4	Mik
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
před	před	k7c7	před
Michaelem	Michael	k1gMnSc7	Michael
Schumacherem	Schumacher	k1gMnSc7	Schumacher
a	a	k8xC	a
právě	právě	k9	právě
Irvinem	Irvino	k1gNnSc7	Irvino
a	a	k8xC	a
s	s	k7c7	s
celkovým	celkový	k2eAgInSc7d1	celkový
ziskem	zisk	k1gInSc7	zisk
76	[number]	k4	76
bodů	bod	k1gInPc2	bod
obhájil	obhájit	k5eAaPmAgInS	obhájit
titul	titul	k1gInSc1	titul
mistra	mistr	k1gMnSc4	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Irvine	Irvinout	k5eAaPmIp3nS	Irvinout
skončil	skončit	k5eAaPmAgInS	skončit
se	s	k7c7	s
74	[number]	k4	74
body	bod	k1gInPc7	bod
jen	jen	k6eAd1	jen
o	o	k7c4	o
2	[number]	k4	2
body	bod	k1gInPc4	bod
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Začátek	začátek	k1gInSc1	začátek
nebyl	být	k5eNaImAgInS	být
pro	pro	k7c4	pro
Miku	Mik	k1gMnSc6	Mik
příliš	příliš	k6eAd1	příliš
dobrý	dobrý	k2eAgMnSc1d1	dobrý
<g/>
,	,	kIx,	,
když	když	k8xS	když
musel	muset	k5eAaImAgMnS	muset
odstoupit	odstoupit	k5eAaPmF	odstoupit
z	z	k7c2	z
úvodní	úvodní	k2eAgFnSc2d1	úvodní
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgFnSc4	první
výhru	výhra	k1gFnSc4	výhra
ale	ale	k8xC	ale
nečekal	čekat	k5eNaImAgMnS	čekat
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
připsal	připsat	k5eAaPmAgMnS	připsat
hned	hned	k6eAd1	hned
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
Imole	Imola	k1gFnSc6	Imola
ale	ale	k8xC	ale
opět	opět	k6eAd1	opět
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
byl	být	k5eAaImAgInS	být
třetí	třetí	k4xOgInSc4	třetí
a	a	k8xC	a
poté	poté	k6eAd1	poté
dvakrát	dvakrát	k6eAd1	dvakrát
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
pódium	pódium	k1gNnSc4	pódium
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Silverstone	Silverston	k1gInSc5	Silverston
poté	poté	k6eAd1	poté
nedojel	dojet	k5eNaPmAgInS	dojet
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
závodě	závod	k1gInSc6	závod
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
zranění	zranění	k1gNnSc1	zranění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ho	on	k3xPp3gMnSc4	on
vyřadilo	vyřadit	k5eAaPmAgNnS	vyřadit
z	z	k7c2	z
boje	boj	k1gInSc2	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
výhru	výhra	k1gFnSc4	výhra
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
a	a	k8xC	a
ve	v	k7c6	v
Spa	Spa	k1gFnSc6	Spa
dojel	dojet	k5eAaPmAgMnS	dojet
druhý	druhý	k4xOgMnSc1	druhý
za	za	k7c7	za
Coulthardem	Coulthard	k1gInSc7	Coulthard
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Monze	Monza	k1gFnSc6	Monza
Häkkinen	Häkkinna	k1gFnPc2	Häkkinna
havaroval	havarovat	k5eAaPmAgMnS	havarovat
a	a	k8xC	a
kamery	kamera	k1gFnSc2	kamera
ho	on	k3xPp3gMnSc2	on
zachytily	zachytit	k5eAaPmAgInP	zachytit
nedaleko	nedaleko	k7c2	nedaleko
trati	trať	k1gFnSc2	trať
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
pláče	plakat	k5eAaImIp3nS	plakat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Evropy	Evropa	k1gFnSc2	Evropa
skončil	skončit	k5eAaPmAgMnS	skončit
pátý	pátý	k4xOgInSc4	pátý
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
jediný	jediný	k2eAgInSc1d1	jediný
závod	závod	k1gInSc1	závod
který	který	k3yIgInSc4	který
dojel	dojet	k5eAaPmAgMnS	dojet
a	a	k8xC	a
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
zároveň	zároveň	k6eAd1	zároveň
nebyl	být	k5eNaImAgMnS	být
na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Malajsii	Malajsie	k1gFnSc6	Malajsie
dojel	dojet	k5eAaPmAgMnS	dojet
třetí	třetí	k4xOgMnSc1	třetí
a	a	k8xC	a
v	v	k7c6	v
již	již	k6eAd1	již
zmíněném	zmíněný	k2eAgNnSc6d1	zmíněné
Japonsku	Japonsko	k1gNnSc6	Japonsko
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
si	se	k3xPyFc3	se
zajistil	zajistit	k5eAaPmAgMnS	zajistit
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Irvineovi	Irvineus	k1gMnSc3	Irvineus
málem	málem	k6eAd1	málem
nadělil	nadělit	k5eAaPmAgMnS	nadělit
celé	celý	k2eAgNnSc4d1	celé
kolo	kolo	k1gNnSc4	kolo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
2000	[number]	k4	2000
====	====	k?	====
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
elitní	elitní	k2eAgFnSc3d1	elitní
skupině	skupina	k1gFnSc3	skupina
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2000	[number]	k4	2000
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
zkompletovat	zkompletovat	k5eAaPmF	zkompletovat
hattrick	hattrick	k1gInSc4	hattrick
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
nepovedlo	povést	k5eNaPmAgNnS	povést
<g/>
.	.	kIx.	.
</s>
<s>
Häkkinen	Häkkinen	k1gInSc4	Häkkinen
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
vítězství	vítězství	k1gNnSc4	vítězství
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
(	(	kIx(	(
<g/>
stylem	styl	k1gInSc7	styl
start-cíl	startílum	k1gNnPc2	start-cílum
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
převzal	převzít	k5eAaPmAgInS	převzít
vedení	vedení	k1gNnSc4	vedení
v	v	k7c6	v
šampionátu	šampionát	k1gInSc6	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
i	i	k9	i
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
provedl	provést	k5eAaPmAgInS	provést
dechberoucí	dechberoucí	k2eAgInSc1d1	dechberoucí
manévr	manévr	k1gInSc1	manévr
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
rovince	rovinka	k1gFnSc6	rovinka
Kemmel	Kemmel	k1gMnSc1	Kemmel
straight	straight	k1gMnSc1	straight
předjel	předjet	k5eAaPmAgMnS	předjet
zároveň	zároveň	k6eAd1	zároveň
Michaela	Michael	k1gMnSc4	Michael
Schumachera	Schumacher	k1gMnSc4	Schumacher
i	i	k8xC	i
Ricarda	Ricard	k1gMnSc4	Ricard
Zontu	Zonta	k1gMnSc4	Zonta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
další	další	k2eAgFnSc6d1	další
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
převzal	převzít	k5eAaPmAgMnS	převzít
celkové	celkový	k2eAgNnSc4d1	celkové
vedení	vedení	k1gNnSc4	vedení
opět	opět	k6eAd1	opět
Schumacher	Schumachra	k1gFnPc2	Schumachra
a	a	k8xC	a
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
popsal	popsat	k5eAaPmAgMnS	popsat
svůj	svůj	k3xOyFgInSc4	svůj
boj	boj	k1gInSc4	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
s	s	k7c7	s
Häkkinenem	Häkkinen	k1gInSc7	Häkkinen
jako	jako	k8xS	jako
nejvíce	nejvíce	k6eAd1	nejvíce
uspokojující	uspokojující	k2eAgMnSc1d1	uspokojující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
2001	[number]	k4	2001
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k9	jako
jeho	jeho	k3xOp3gFnSc1	jeho
poslední	poslední	k2eAgFnSc1d1	poslední
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
nahrazen	nahradit	k5eAaPmNgMnS	nahradit
svým	svůj	k3xOyFgMnSc7	svůj
mladým	mladý	k2eAgMnSc7d1	mladý
krajanem	krajan	k1gMnSc7	krajan
a	a	k8xC	a
chráněncem	chráněnec	k1gMnSc7	chráněnec
Kimim	Kimim	k1gMnSc1	Kimim
Räikkönenem	Räikkönen	k1gMnSc7	Räikkönen
<g/>
.	.	kIx.	.
</s>
<s>
Vůz	vůz	k1gInSc1	vůz
MP4-16	MP4-16	k1gFnSc2	MP4-16
již	již	k6eAd1	již
nebyl	být	k5eNaImAgInS	být
tak	tak	k6eAd1	tak
dobrý	dobrý	k2eAgInSc1d1	dobrý
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
spíše	spíše	k9	spíše
Coulthard	Coulthard	k1gMnSc1	Coulthard
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
potrápil	potrápit	k5eAaPmAgMnS	potrápit
Michaela	Michael	k1gMnSc4	Michael
Schumachera	Schumacher	k1gMnSc4	Schumacher
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
utrpěl	utrpět	k5eAaPmAgInS	utrpět
větší	veliký	k2eAgFnSc4d2	veliký
nehodu	nehoda	k1gFnSc4	nehoda
kvůli	kvůli	k7c3	kvůli
selhání	selhání	k1gNnSc3	selhání
předního	přední	k2eAgNnSc2d1	přední
zavěšení	zavěšení	k1gNnSc2	zavěšení
v	v	k7c6	v
úvodním	úvodní	k2eAgInSc6d1	úvodní
podniku	podnik	k1gInSc6	podnik
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
když	když	k8xS	když
jel	jet	k5eAaImAgMnS	jet
druhý	druhý	k4xOgMnSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
havárie	havárie	k1gFnSc1	havárie
jakoby	jakoby	k8xS	jakoby
způsobila	způsobit	k5eAaPmAgFnS	způsobit
pokles	pokles	k1gInSc4	pokles
jeho	jeho	k3xOp3gFnSc2	jeho
motivace	motivace	k1gFnSc2	motivace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
se	se	k3xPyFc4	se
zdržel	zdržet	k5eAaPmAgInS	zdržet
na	na	k7c6	na
startu	start	k1gInSc6	start
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
málem	málem	k6eAd1	málem
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
narazil	narazit	k5eAaPmAgMnS	narazit
Olivier	Olivier	k1gMnSc1	Olivier
Panis	Panis	k1gFnSc2	Panis
<g/>
.	.	kIx.	.
</s>
<s>
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
jednání	jednání	k1gNnSc4	jednání
dostal	dostat	k5eAaPmAgInS	dostat
pokutu	pokuta	k1gFnSc4	pokuta
$	$	kIx~	$
<g/>
5	[number]	k4	5
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
vedl	vést	k5eAaImAgInS	vést
až	až	k6eAd1	až
do	do	k7c2	do
posledního	poslední	k2eAgNnSc2d1	poslední
kola	kolo	k1gNnSc2	kolo
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
si	se	k3xPyFc3	se
připsat	připsat	k5eAaPmF	připsat
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
výhru	výhra	k1gFnSc4	výhra
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
okruhu	okruh	k1gInSc6	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Porucha	porucha	k1gFnSc1	porucha
spojky	spojka	k1gFnSc2	spojka
ho	on	k3xPp3gNnSc4	on
nakonec	nakonec	k6eAd1	nakonec
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
ze	z	k7c2	z
závodu	závod	k1gInSc2	závod
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
kole	kolo	k1gNnSc6	kolo
pouhých	pouhý	k2eAgFnPc2d1	pouhá
5	[number]	k4	5
zatáček	zatáčka	k1gFnPc2	zatáčka
před	před	k7c7	před
cílem	cíl	k1gInSc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Häkkinena	Häkkinena	k1gFnSc1	Häkkinena
poté	poté	k6eAd1	poté
do	do	k7c2	do
boxů	box	k1gInPc2	box
na	na	k7c6	na
boku	bok	k1gInSc6	bok
svého	svůj	k3xOyFgInSc2	svůj
vozu	vůz	k1gInSc2	vůz
přivezl	přivézt	k5eAaPmAgMnS	přivézt
David	David	k1gMnSc1	David
Coulthard	Coulthard	k1gMnSc1	Coulthard
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
smutný	smutný	k2eAgInSc1d1	smutný
ale	ale	k8xC	ale
nezapomenutelný	zapomenutelný	k2eNgInSc1d1	nezapomenutelný
moment	moment	k1gInSc1	moment
<g/>
,	,	kIx,	,
zdůrazňující	zdůrazňující	k2eAgNnSc1d1	zdůrazňující
silné	silný	k2eAgNnSc1d1	silné
pouto	pouto	k1gNnSc1	pouto
mezi	mezi	k7c7	mezi
Coulthardem	Coulthard	k1gMnSc7	Coulthard
a	a	k8xC	a
Häkkinenem	Häkkinen	k1gMnSc7	Häkkinen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
dominoval	dominovat	k5eAaImAgInS	dominovat
celému	celý	k2eAgInSc3d1	celý
závodu	závod	k1gInSc3	závod
a	a	k8xC	a
připsal	připsat	k5eAaPmAgMnS	připsat
si	se	k3xPyFc3	se
první	první	k4xOgFnSc4	první
výhru	výhra	k1gFnSc4	výhra
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Monze	Monz	k1gInSc6	Monz
oznámil	oznámit	k5eAaPmAgInS	oznámit
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
médiím	médium	k1gNnPc3	médium
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
si	se	k3xPyFc3	se
dá	dát	k5eAaPmIp3nS	dát
od	od	k7c2	od
závodění	závodění	k1gNnSc2	závodění
pauzu	pauza	k1gFnSc4	pauza
a	a	k8xC	a
mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Indianapolis	Indianapolis	k1gFnPc2	Indianapolis
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
své	svůj	k3xOyFgNnSc4	svůj
poslední	poslední	k2eAgNnSc4d1	poslední
vítězství	vítězství	k1gNnSc4	vítězství
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
skončil	skončit	k5eAaPmAgInS	skončit
s	s	k7c7	s
37	[number]	k4	37
body	bod	k1gInPc7	bod
pátý	pátý	k4xOgInSc1	pátý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
oznámil	oznámit	k5eAaPmAgMnS	oznámit
úplné	úplný	k2eAgNnSc4d1	úplné
ukončení	ukončení	k1gNnSc4	ukončení
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
DTM	DTM	kA	DTM
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
oznámil	oznámit	k5eAaPmAgInS	oznámit
své	svůj	k3xOyFgInPc4	svůj
plány	plán	k1gInPc4	plán
na	na	k7c4	na
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
závodění	závodění	k1gNnSc3	závodění
a	a	k8xC	a
hovořil	hovořit	k5eAaImAgInS	hovořit
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
Williams	Williamsa	k1gFnPc2	Williamsa
o	o	k7c4	o
angažmá	angažmá	k1gNnSc4	angažmá
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
nedošlo	dojít	k5eNaPmAgNnS	dojít
a	a	k8xC	a
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
provedl	provést	k5eAaPmAgInS	provést
svůj	svůj	k3xOyFgInSc4	svůj
comeback	comeback	k1gInSc4	comeback
k	k	k7c3	k
závodění	závodění	k1gNnSc3	závodění
v	v	k7c6	v
sérii	série	k1gFnSc6	série
DTM	DTM	kA	DTM
ve	v	k7c6	v
voze	vůz	k1gInSc6	vůz
Mercedes-Benz	Mercedes-Benz	k1gInSc1	Mercedes-Benz
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
první	první	k4xOgFnSc1	první
sezóna	sezóna	k1gFnSc1	sezóna
<g/>
,	,	kIx,	,
okořeněná	okořeněný	k2eAgFnSc1d1	okořeněná
jedním	jeden	k4xCgNnSc7	jeden
vítězstvím	vítězství	k1gNnSc7	vítězství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
šampionátu	šampionát	k1gInSc6	šampionát
DTM	DTM	kA	DTM
s	s	k7c7	s
Mercedesem	mercedes	k1gInSc7	mercedes
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sezóna	sezóna	k1gFnSc1	sezóna
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
obtížnější	obtížný	k2eAgFnSc1d2	obtížnější
a	a	k8xC	a
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
druhých	druhý	k4xOgInPc2	druhý
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Magazín	magazín	k1gInSc1	magazín
Autosport	autosport	k1gInSc1	autosport
spekuloval	spekulovat	k5eAaImAgInS	spekulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Häkkinenův	Häkkinenův	k2eAgInSc1d1	Häkkinenův
styl	styl	k1gInSc1	styl
jízdy	jízda	k1gFnSc2	jízda
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
nehodí	hodit	k5eNaPmIp3nS	hodit
do	do	k7c2	do
vozu	vůz	k1gInSc2	vůz
DTM	DTM	kA	DTM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
akci	akce	k1gFnSc6	akce
Goodwood	Goodwooda	k1gFnPc2	Goodwooda
Festival	festival	k1gInSc1	festival
of	of	k?	of
Speed	Speed	k1gInSc1	Speed
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
řídil	řídit	k5eAaImAgInS	řídit
Häkkinen	Häkkinen	k2eAgInSc1d1	Häkkinen
vůz	vůz	k1gInSc1	vůz
McLaren-Mercedes	McLaren-Mercedes	k1gMnSc1	McLaren-Mercedes
MP4-20	MP4-20	k1gMnSc1	MP4-20
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
lidé	člověk	k1gMnPc1	člověk
říkali	říkat	k5eAaImAgMnP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
skvělé	skvělý	k2eAgNnSc1d1	skvělé
vidět	vidět	k5eAaImF	vidět
Häkkinena	Häkkinen	k1gMnSc4	Häkkinen
opět	opět	k6eAd1	opět
v	v	k7c6	v
McLarenu	McLaren	k1gInSc6	McLaren
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgMnS	být
tenkrát	tenkrát	k6eAd1	tenkrát
posledním	poslední	k2eAgMnSc7d1	poslední
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
týmu	tým	k1gInSc6	tým
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
zůstal	zůstat	k5eAaPmAgInS	zůstat
u	u	k7c2	u
Mercedesu	mercedes	k1gInSc2	mercedes
i	i	k9	i
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
sezóně	sezóna	k1gFnSc6	sezóna
a	a	k8xC	a
přestože	přestože	k8xS	přestože
celkový	celkový	k2eAgInSc4d1	celkový
dojem	dojem	k1gInSc4	dojem
kazí	kazit	k5eAaImIp3nP	kazit
několik	několik	k4yIc4	několik
smolných	smolný	k2eAgInPc2d1	smolný
závodů	závod	k1gInPc2	závod
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
rok	rok	k1gInSc4	rok
2007	[number]	k4	2007
jeho	jeho	k3xOp3gFnSc4	jeho
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
kvalifikoval	kvalifikovat	k5eAaBmAgMnS	kvalifikovat
do	do	k7c2	do
přední	přední	k2eAgFnSc2d1	přední
řady	řada	k1gFnSc2	řada
a	a	k8xC	a
dvakrát	dvakrát	k6eAd1	dvakrát
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
(	(	kIx(	(
<g/>
na	na	k7c6	na
okruzích	okruh	k1gInPc6	okruh
Lausitz	Lausitza	k1gFnPc2	Lausitza
a	a	k8xC	a
Mugello	Mugello	k1gNnSc4	Mugello
<g/>
)	)	kIx)	)
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
hodnocení	hodnocení	k1gNnSc6	hodnocení
osmý	osmý	k4xOgMnSc1	osmý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Možné	možný	k2eAgInPc1d1	možný
návraty	návrat	k1gInPc1	návrat
do	do	k7c2	do
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Häkkinenově	Häkkinenův	k2eAgFnSc6d1	Häkkinenův
roční	roční	k2eAgFnSc6d1	roční
přestávce	přestávka	k1gFnSc6	přestávka
a	a	k8xC	a
následném	následný	k2eAgNnSc6d1	následné
ukončení	ukončení	k1gNnSc6	ukončení
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mnohokrát	mnohokrát	k6eAd1	mnohokrát
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
možném	možný	k2eAgInSc6d1	možný
návratu	návrat	k1gInSc6	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Mika	Mik	k1gMnSc2	Mik
byl	být	k5eAaImAgInS	být
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
Williams	Williamsa	k1gFnPc2	Williamsa
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Juan	Juan	k1gMnSc1	Juan
Pablo	Pablo	k1gNnSc4	Pablo
Montoya	Montoyus	k1gMnSc2	Montoyus
měl	mít	k5eAaImAgMnS	mít
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
přestoupit	přestoupit	k5eAaPmF	přestoupit
k	k	k7c3	k
McLarenu	McLaren	k1gInSc3	McLaren
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
ale	ale	k8xC	ale
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
opět	opět	k6eAd1	opět
vyvstaly	vyvstat	k5eAaPmAgFnP	vyvstat
spekulace	spekulace	k1gFnPc1	spekulace
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
o	o	k7c6	o
možném	možný	k2eAgInSc6d1	možný
návratu	návrat	k1gInSc6	návrat
k	k	k7c3	k
týmu	tým	k1gInSc3	tým
McLaren	McLarno	k1gNnPc2	McLarno
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Magazín	magazín	k1gInSc1	magazín
Autosport	autosport	k1gInSc1	autosport
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
dvakrát	dvakrát	k6eAd1	dvakrát
testoval	testovat	k5eAaImAgMnS	testovat
v	v	k7c6	v
simulátoru	simulátor	k1gInSc6	simulátor
McLarenu	McLaren	k1gInSc2	McLaren
a	a	k8xC	a
diskutoval	diskutovat	k5eAaImAgMnS	diskutovat
o	o	k7c6	o
možném	možný	k2eAgInSc6d1	možný
návratu	návrat	k1gInSc6	návrat
k	k	k7c3	k
týmu	tým	k1gInSc3	tým
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
McLaren	McLarna	k1gFnPc2	McLarna
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lewis	Lewis	k1gFnSc1	Lewis
Hamilton	Hamilton	k1gInSc1	Hamilton
usedne	usednout	k5eAaPmIp3nS	usednout
do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
kokpitu	kokpit	k1gInSc2	kokpit
McLarenu	McLaren	k1gInSc2	McLaren
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
a	a	k8xC	a
ukončil	ukončit	k5eAaPmAgInS	ukončit
tak	tak	k6eAd1	tak
veškeré	veškerý	k3xTgFnPc4	veškerý
spekulace	spekulace	k1gFnPc4	spekulace
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
psalo	psát	k5eAaImAgNnS	psát
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
působil	působit	k5eAaImAgInS	působit
v	v	k7c6	v
roli	role	k1gFnSc6	role
poradce	poradce	k1gMnSc2	poradce
<g/>
,	,	kIx,	,
když	když	k8xS	když
Ron	Ron	k1gMnSc1	Ron
Dennis	Dennis	k1gFnSc4	Dennis
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
osobnost	osobnost	k1gFnSc1	osobnost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
přivádíme	přivádět	k5eAaImIp1nP	přivádět
a	a	k8xC	a
která	který	k3yRgNnPc1	který
zhodnotí	zhodnotit	k5eAaPmIp3nP	zhodnotit
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyvíjíme	vyvíjet	k5eAaImIp1nP	vyvíjet
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
Häkkinen	Häkkinen	k2eAgInSc4d1	Häkkinen
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
testoval	testovat	k5eAaImAgMnS	testovat
McLaren-Mercedes	McLaren-Mercedes	k1gMnSc1	McLaren-Mercedes
MP4-21	MP4-21	k1gMnSc1	MP4-21
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Circuit	Circuit	k1gMnSc1	Circuit
de	de	k?	de
Catalunya	Catalunya	k1gMnSc1	Catalunya
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
79	[number]	k4	79
kol	kola	k1gFnPc2	kola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnSc4	jeho
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
kolo	kolo	k1gNnSc4	kolo
bylo	být	k5eAaImAgNnS	být
o	o	k7c4	o
3	[number]	k4	3
sekundy	sekunda	k1gFnPc4	sekunda
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
než	než	k8xS	než
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
McLaren	McLarna	k1gFnPc2	McLarna
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
využili	využít	k5eAaPmAgMnP	využít
jeho	jeho	k3xOp3gFnPc2	jeho
odborných	odborný	k2eAgFnPc2d1	odborná
znalostí	znalost	k1gFnPc2	znalost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
poznali	poznat	k5eAaPmAgMnP	poznat
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
od	od	k7c2	od
jeho	jeho	k3xOp3gInSc2	jeho
odchodu	odchod	k1gInSc2	odchod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Häkkinen	Häkkinen	k1gInSc4	Häkkinen
byla	být	k5eAaImAgFnS	být
také	také	k9	také
jmenován	jmenovat	k5eAaImNgInS	jmenovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
pijící	pijící	k2eAgMnSc1d1	pijící
ambasador	ambasador	k1gMnSc1	ambasador
<g/>
"	"	kIx"	"
sponzorem	sponzor	k1gMnSc7	sponzor
McLarenu	McLaren	k1gInSc2	McLaren
firmou	firma	k1gFnSc7	firma
Johnnie	Johnnie	k1gFnSc2	Johnnie
Walker	Walkra	k1gFnPc2	Walkra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
má	mít	k5eAaImIp3nS	mít
ulici	ulice	k1gFnSc4	ulice
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenovanou	pojmenovaný	k2eAgFnSc4d1	pojmenovaná
v	v	k7c6	v
australském	australský	k2eAgNnSc6d1	Australské
městě	město	k1gNnSc6	město
Adelaide	Adelaid	k1gMnSc5	Adelaid
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
málem	málem	k6eAd1	málem
tragické	tragický	k2eAgFnSc3d1	tragická
nehodě	nehoda	k1gFnSc3	nehoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Intercontinental	Intercontinental	k1gMnSc1	Intercontinental
Le	Le	k1gFnSc2	Le
Mans	Mans	k1gInSc1	Mans
Cup	cup	k1gInSc1	cup
2011	[number]	k4	2011
==	==	k?	==
</s>
</p>
<p>
<s>
Tým	tým	k1gInSc1	tým
AMG	AMG	kA	AMG
China	China	k1gFnSc1	China
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
s	s	k7c7	s
vozem	vůz	k1gInSc7	vůz
Mercedes-Benz	Mercedes-Benz	k1gMnSc1	Mercedes-Benz
SLS	SLS	kA	SLS
AMG	AMG	kA	AMG
do	do	k7c2	do
série	série	k1gFnSc2	série
Intercontinental	Intercontinental	k1gMnSc1	Intercontinental
Le	Le	k1gMnSc1	Le
Mans	Mansa	k1gFnPc2	Mansa
Cup	cup	k1gInSc4	cup
–	–	k?	–
6	[number]	k4	6
hodinového	hodinový	k2eAgInSc2d1	hodinový
závodu	závod	k1gInSc2	závod
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Zhuhau	Zhuhaus	k1gInSc2	Zhuhaus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
odbyl	odbýt	k5eAaPmAgInS	odbýt
debut	debut	k1gInSc4	debut
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
GT	GT	kA	GT
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konec	konec	k1gInSc1	konec
kariéry	kariéra	k1gFnSc2	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
oznámil	oznámit	k5eAaPmAgInS	oznámit
konec	konec	k1gInSc4	konec
závodní	závodní	k2eAgFnSc2d1	závodní
kariéry	kariéra	k1gFnSc2	kariéra
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
<g/>
,	,	kIx,	,
4	[number]	k4	4
.	.	kIx.	.
<g/>
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
prý	prý	k9	prý
"	"	kIx"	"
<g/>
nebylo	být	k5eNaImAgNnS	být
snadné	snadný	k2eAgNnSc1d1	snadné
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
závodění	závodění	k1gNnSc1	závodění
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
v	v	k7c6	v
mé	můj	k3xOp1gFnSc6	můj
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mi	já	k3xPp1nSc3	já
bude	být	k5eAaImBp3nS	být
bránit	bránit	k5eAaImF	bránit
závodit	závodit	k5eAaImF	závodit
pro	pro	k7c4	pro
radost	radost	k1gFnSc4	radost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2008	[number]	k4	2008
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
začne	začít	k5eAaPmIp3nS	začít
novou	nový	k2eAgFnSc4d1	nová
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
managementu	management	k1gInSc2	management
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Helma	helma	k1gFnSc1	helma
==	==	k?	==
</s>
</p>
<p>
<s>
Helma	helma	k1gFnSc1	helma
Miky	Mik	k1gMnPc4	Mik
Häkkinena	Häkkinena	k1gFnSc1	Häkkinena
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
s	s	k7c7	s
tmavě	tmavě	k6eAd1	tmavě
modrými	modrý	k2eAgInPc7d1	modrý
pruhy	pruh	k1gInPc7	pruh
<g/>
,	,	kIx,	,
královsky	královsky	k6eAd1	královsky
modrými	modrý	k2eAgInPc7d1	modrý
pruhy	pruh	k1gInPc7	pruh
a	a	k8xC	a
nebesky	nebesky	k6eAd1	nebesky
modrými	modrý	k2eAgInPc7d1	modrý
pruhy	pruh	k1gInPc7	pruh
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
<g/>
,	,	kIx,	,
modrým	modrý	k2eAgInSc7d1	modrý
kruhem	kruh	k1gInSc7	kruh
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
helmy	helma	k1gFnSc2	helma
a	a	k8xC	a
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
jménem	jméno	k1gNnSc7	jméno
napsaným	napsaný	k2eAgInSc7d1	napsaný
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
brady	brada	k1gFnSc2	brada
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
zvoleny	zvolit	k5eAaPmNgFnP	zvolit
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gMnSc2	jeho
sponzora	sponzor	k1gMnSc2	sponzor
z	z	k7c2	z
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
společnosti	společnost	k1gFnSc2	společnost
Oy	Oy	k1gFnSc2	Oy
G.W.	G.W.	k1gMnSc1	G.W.
Sohlberg	Sohlberg	k1gMnSc1	Sohlberg
Ab	Ab	k1gMnSc1	Ab
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
záliby	záliba	k1gFnPc1	záliba
==	==	k?	==
</s>
</p>
<p>
<s>
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
umí	umět	k5eAaImIp3nS	umět
jezdit	jezdit	k5eAaImF	jezdit
na	na	k7c4	na
jednokolce	jednokolec	k1gInPc4	jednokolec
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
lekce	lekce	k1gFnPc4	lekce
řízení	řízení	k1gNnSc2	řízení
v	v	k7c4	v
Mercedes-Benz	Mercedes-Benz	k1gInSc4	Mercedes-Benz
World	Worlda	k1gFnPc2	Worlda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vydělal	vydělat	k5eAaPmAgInS	vydělat
peníze	peníz	k1gInPc4	peníz
pro	pro	k7c4	pro
dětskou	dětský	k2eAgFnSc4d1	dětská
charitu	charita	k1gFnSc4	charita
pro	pro	k7c4	pro
nemocnici	nemocnice	k1gFnSc4	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Akci	akce	k1gFnSc4	akce
sponzoroval	sponzorovat	k5eAaImAgInS	sponzorovat
myHermes	myHermes	k1gInSc1	myHermes
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
který	který	k3yIgInSc4	který
je	být	k5eAaImIp3nS	být
Häkkinen	Häkkinen	k2eAgInSc4d1	Häkkinen
ambasadorem	ambasador	k1gMnSc7	ambasador
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
se	se	k3xPyFc4	se
také	také	k9	také
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Top	topit	k5eAaImRp2nS	topit
Gear	Gear	k1gInSc4	Gear
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
učil	učít	k5eAaPmAgMnS	učít
moderátora	moderátor	k1gMnSc2	moderátor
Jamese	Jamese	k1gFnSc2	Jamese
Maye	May	k1gMnSc2	May
základy	základ	k1gInPc1	základ
řízení	řízení	k1gNnSc4	řízení
rally	ralla	k1gFnSc2	ralla
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
test	test	k1gInSc4	test
s	s	k7c7	s
Pagani	Pagan	k1gMnPc1	Pagan
Zonda	Zond	k1gMnSc4	Zond
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
usídlil	usídlit	k5eAaPmAgInS	usídlit
v	v	k7c6	v
Monte	Mont	k1gInSc5	Mont
Carlu	Carl	k1gInSc3	Carl
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
vlastní	vlastní	k2eAgInPc4d1	vlastní
domy	dům	k1gInPc4	dům
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
shořela	shořet	k5eAaPmAgFnS	shořet
jeho	jeho	k3xOp3gFnSc1	jeho
nově	nově	k6eAd1	nově
postavená	postavený	k2eAgFnSc1d1	postavená
vila	vila	k1gFnSc1	vila
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
po	po	k7c6	po
zkratu	zkrat	k1gInSc6	zkrat
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
skříni	skříň	k1gFnSc6	skříň
s	s	k7c7	s
trofejemi	trofej	k1gFnPc7	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
nebyl	být	k5eNaImAgMnS	být
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zahynula	zahynout	k5eAaPmAgFnS	zahynout
jeho	jeho	k3xOp3gFnSc1	jeho
želva	želva	k1gFnSc1	želva
a	a	k8xC	a
také	také	k9	také
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
jeho	jeho	k3xOp3gFnSc1	jeho
kolekce	kolekce	k1gFnSc1	kolekce
trofejí	trofej	k1gFnPc2	trofej
z	z	k7c2	z
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Häkkinen	Häkkinen	k2eAgMnSc1d1	Häkkinen
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
s	s	k7c7	s
Erjou	Erja	k1gFnSc7	Erja
Honkanen	Honkanna	k1gFnPc2	Honkanna
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
mají	mít	k5eAaImIp3nP	mít
syna	syn	k1gMnSc4	syn
Huga	Hugo	k1gMnSc4	Hugo
Ronana	Ronan	k1gMnSc4	Ronan
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgInSc1d1	narozen
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Ainu	Ain	k2eAgFnSc4d1	Aina
Julii	Julie	k1gFnSc4	Julie
(	(	kIx(	(
<g/>
narozena	narozen	k2eAgFnSc1d1	narozena
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
rozešli	rozejít	k5eAaPmAgMnP	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Häkkinenovo	Häkkinenův	k2eAgNnSc4d1	Häkkinenův
třetí	třetí	k4xOgNnSc4	třetí
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Ella	Ella	k1gFnSc1	Ella
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
jeho	jeho	k3xOp3gFnSc4	jeho
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
Markétě	Markéta	k1gFnSc6	Markéta
Remešové	Remešové	k2eAgFnSc6d1	Remešové
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Statistiky	statistika	k1gFnPc1	statistika
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Souhrn	souhrn	k1gInSc1	souhrn
kariéry	kariéra	k1gFnSc2	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Kompletní	kompletní	k2eAgInPc4d1	kompletní
výsledky	výsledek	k1gInPc4	výsledek
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
===	===	k?	===
</s>
</p>
<p>
<s>
tučně	tučně	k6eAd1	tučně
vyznačené	vyznačený	k2eAgInPc1d1	vyznačený
závody	závod	k1gInPc1	závod
znamenají	znamenat	k5eAaImIp3nP	znamenat
zisk	zisk	k1gInSc4	zisk
pole	pole	k1gNnSc2	pole
position	position	k1gInSc1	position
<g/>
,	,	kIx,	,
závody	závod	k1gInPc1	závod
vyznačené	vyznačený	k2eAgInPc1d1	vyznačený
kurzívou	kurzíva	k1gFnSc7	kurzíva
označují	označovat	k5eAaImIp3nP	označovat
zajetí	zajetí	k1gNnPc1	zajetí
nejrychlejšího	rychlý	k2eAgNnSc2d3	nejrychlejší
kola	kolo	k1gNnSc2	kolo
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
</s>
</p>
<p>
<s>
===	===	k?	===
Kompletní	kompletní	k2eAgInPc4d1	kompletní
výsledky	výsledek	k1gInPc4	výsledek
v	v	k7c6	v
DTM	DTM	kA	DTM
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
tučné	tučný	k2eAgNnSc4d1	tučné
písmo	písmo	k1gNnSc4	písmo
v	v	k7c6	v
označení	označení	k1gNnSc6	označení
závodu	závod	k1gInSc2	závod
indikuje	indikovat	k5eAaBmIp3nS	indikovat
pole	pole	k1gNnSc4	pole
position	position	k1gInSc1	position
<g/>
,	,	kIx,	,
kurzíva	kurzíva	k1gFnSc1	kurzíva
indikuje	indikovat	k5eAaBmIp3nS	indikovat
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
kolo	kolo	k1gNnSc4	kolo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
*	*	kIx~	*
<g/>
"	"	kIx"	"
V	v	k7c6	v
závodě	závod	k1gInSc6	závod
byly	být	k5eAaImAgFnP	být
uděleny	udělit	k5eAaPmNgFnP	udělit
pouze	pouze	k6eAd1	pouze
poloviční	poloviční	k2eAgInPc4d1	poloviční
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
několika	několik	k4yIc2	několik
chybám	chyba	k1gFnPc3	chyba
stewardů	steward	k1gInPc2	steward
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mika	Mik	k1gMnSc2	Mik
Häkkinen	Häkkinen	k2eAgMnSc1d1	Häkkinen
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Mika	Mik	k1gMnSc2	Mik
Häkkinen	Häkkinen	k2eAgMnSc1d1	Häkkinen
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
