<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
fotograficky	fotograficky	k6eAd1	fotograficky
dokumentoval	dokumentovat	k5eAaBmAgMnS	dokumentovat
zranění	zranění	k1gNnSc4	zranění
a	a	k8xC	a
choroby	choroba	k1gFnSc2	choroba
vojáků	voják	k1gMnPc2	voják
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
pro	pro	k7c4	pro
armádní	armádní	k2eAgNnSc4d1	armádní
muzeum	muzeum	k1gNnSc4	muzeum
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
mnohé	mnohý	k2eAgFnPc1d1	mnohá
byly	být	k5eAaImAgInP	být
publikovány	publikovat	k5eAaBmNgInP	publikovat
v	v	k7c6	v
lékařské	lékařský	k2eAgFnSc6d1	lékařská
knize	kniha	k1gFnSc6	kniha
Medical	Medical	k1gFnSc2	Medical
and	and	k?	and
Surgical	Surgical	k1gFnSc2	Surgical
History	Histor	k1gInPc4	Histor
of	of	k?	of
the	the	k?	the
War	War	k1gFnSc2	War
of	of	k?	of
the	the	k?	the
Rebellion	Rebellion	k?	Rebellion
(	(	kIx(	(
<g/>
Historie	historie	k1gFnSc1	historie
medicíny	medicína	k1gFnSc2	medicína
a	a	k8xC	a
chirurgie	chirurgie	k1gFnSc2	chirurgie
válečné	válečný	k2eAgFnSc2d1	válečná
vzpoury	vzpoura	k1gFnSc2	vzpoura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
