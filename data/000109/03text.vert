<s>
Staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
neboli	neboli	k8xC	neboli
stará	starý	k2eAgFnSc1d1	stará
církevní	církevní	k2eAgFnSc1d1	církevní
slovanština	slovanština	k1gFnSc1	slovanština
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
slovanský	slovanský	k2eAgInSc1d1	slovanský
spisovný	spisovný	k2eAgInSc1d1	spisovný
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Spisovná	spisovný	k2eAgFnSc1d1	spisovná
norma	norma	k1gFnSc1	norma
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nářečí	nářečí	k1gNnSc2	nářečí
používaného	používaný	k2eAgNnSc2d1	používané
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Soluně	Soluň	k1gFnSc2	Soluň
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
určitými	určitý	k2eAgFnPc7d1	určitá
změnami	změna	k1gFnPc7	změna
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
církevní	církevní	k2eAgFnSc1d1	církevní
slovanština	slovanština	k1gFnSc1	slovanština
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
ne	ne	k9	ne
už	už	k9	už
však	však	k9	však
jako	jako	k8xS	jako
živý	živý	k2eAgInSc4d1	živý
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jako	jako	k9	jako
bohoslužebný	bohoslužebný	k2eAgInSc1d1	bohoslužebný
jazyk	jazyk	k1gInSc1	jazyk
církví	církev	k1gFnPc2	církev
byzantského	byzantský	k2eAgInSc2d1	byzantský
obřadu	obřad	k1gInSc2	obřad
(	(	kIx(	(
<g/>
řeckokatolická	řeckokatolický	k2eAgFnSc1d1	řeckokatolická
<g/>
,	,	kIx,	,
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Staroslověnským	staroslověnský	k2eAgInSc7d1	staroslověnský
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
písemnictvím	písemnictví	k1gNnSc7	písemnictví
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
paleoslovenistika	paleoslovenistika	k1gFnSc1	paleoslovenistika
<g/>
.	.	kIx.	.
</s>
<s>
Staroslověnštinu	staroslověnština	k1gFnSc4	staroslověnština
nelze	lze	k6eNd1	lze
zaměňovat	zaměňovat	k5eAaImF	zaměňovat
s	s	k7c7	s
praslovanštinou	praslovanština	k1gFnSc7	praslovanština
(	(	kIx(	(
<g/>
třebaže	třebaže	k8xS	třebaže
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
jako	jako	k8xS	jako
jediném	jediný	k2eAgInSc6d1	jediný
slovanském	slovanský	k2eAgInSc6d1	slovanský
jazyce	jazyk	k1gInSc6	jazyk
mnohé	mnohý	k2eAgInPc4d1	mnohý
praslovanské	praslovanský	k2eAgInPc4d1	praslovanský
jevy	jev	k1gInPc1	jev
doloženy	doložit	k5eAaPmNgInP	doložit
přímo	přímo	k6eAd1	přímo
a	a	k8xC	a
staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dá	dát	k5eAaPmIp3nS	dát
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
zachycení	zachycení	k1gNnSc4	zachycení
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
fáze	fáze	k1gFnSc2	fáze
existence	existence	k1gFnSc2	existence
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
z	z	k7c2	z
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
-	-	kIx~	-
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
hypotetická	hypotetický	k2eAgFnSc1d1	hypotetická
podoba	podoba	k1gFnSc1	podoba
se	se	k3xPyFc4	se
rekonstruuje	rekonstruovat	k5eAaBmIp3nS	rekonstruovat
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
srovnáváním	srovnávání	k1gNnSc7	srovnávání
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
(	(	kIx(	(
<g/>
i	i	k8xC	i
neslovanských	slovanský	k2eNgInPc2d1	neslovanský
<g/>
)	)	kIx)	)
jazyků	jazyk	k1gInPc2	jazyk
-	-	kIx~	-
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
všechny	všechen	k3xTgInPc1	všechen
slovanské	slovanský	k2eAgInPc1d1	slovanský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
už	už	k6eAd1	už
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
vývojových	vývojový	k2eAgFnPc2d1	vývojová
větví	větev	k1gFnPc2	větev
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
z	z	k7c2	z
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
vycházejí	vycházet	k5eAaImIp3nP	vycházet
<g/>
:	:	kIx,	:
na	na	k7c6	na
větvi	větev	k1gFnSc6	větev
jihoslovanské	jihoslovanský	k2eAgFnSc6d1	Jihoslovanská
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
prvních	první	k4xOgFnPc2	první
staroslověnských	staroslověnský	k2eAgFnPc2d1	staroslověnská
písemných	písemný	k2eAgFnPc2d1	písemná
památek	památka	k1gFnPc2	památka
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
ještě	ještě	k6eAd1	ještě
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
větve	větev	k1gFnPc1	větev
lišily	lišit	k5eAaImAgFnP	lišit
jen	jen	k9	jen
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
starobulharština	starobulharština	k1gFnSc1	starobulharština
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyjadřovalo	vyjadřovat	k5eAaImAgNnS	vyjadřovat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
původní	původní	k2eAgFnSc4d1	původní
variantu	varianta	k1gFnSc4	varianta
bulharštiny	bulharština	k1gFnSc2	bulharština
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
nesouhlasil	souhlasit	k5eNaImAgInS	souhlasit
jiný	jiný	k2eAgInSc4d1	jiný
směr	směr	k1gInSc4	směr
bádání	bádání	k1gNnSc2	bádání
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
staroslověnštinu	staroslověnština	k1gFnSc4	staroslověnština
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
původní	původní	k2eAgFnSc4d1	původní
variantu	varianta	k1gFnSc4	varianta
slovinštiny	slovinština	k1gFnSc2	slovinština
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
odmítnuta	odmítnut	k2eAgFnSc1d1	odmítnuta
i	i	k8xC	i
tato	tento	k3xDgFnSc1	tento
teze	teze	k1gFnSc1	teze
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
neutrální	neutrální	k2eAgInSc1d1	neutrální
název	název	k1gInSc1	název
církevní	církevní	k2eAgFnSc1d1	církevní
slovanština	slovanština	k1gFnSc1	slovanština
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
však	však	k9	však
více	hodně	k6eAd2	hodně
rozšířen	rozšířen	k2eAgInSc1d1	rozšířen
název	název	k1gInSc1	název
staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
už	už	k6eAd1	už
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
nemíní	mínit	k5eNaImIp3nS	mínit
nejstarší	starý	k2eAgFnSc1d3	nejstarší
slovinština	slovinština	k1gFnSc1	slovinština
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
pouze	pouze	k6eAd1	pouze
ke	k	k7c3	k
tvaru	tvar	k1gInSc3	tvar
jazyka	jazyk	k1gInSc2	jazyk
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
zúženě	zúženě	k6eAd1	zúženě
dokonce	dokonce	k9	dokonce
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
památkám	památka	k1gFnPc3	památka
z	z	k7c2	z
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
církevní	církevní	k2eAgFnSc1d1	církevní
slovanština	slovanština	k1gFnSc1	slovanština
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
označuje	označovat	k5eAaImIp3nS	označovat
pozdější	pozdní	k2eAgFnPc4d2	pozdější
vývojové	vývojový	k2eAgFnPc4d1	vývojová
fáze	fáze	k1gFnPc4	fáze
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
již	již	k6eAd1	již
odlišoval	odlišovat	k5eAaImAgInS	odlišovat
od	od	k7c2	od
živých	živý	k2eAgInPc2d1	živý
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
existoval	existovat	k5eAaImAgInS	existovat
jako	jako	k9	jako
jejich	jejich	k3xOp3gInSc1	jejich
spisovný	spisovný	k2eAgInSc1d1	spisovný
jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
národních	národní	k2eAgInPc2d1	národní
spisovných	spisovný	k2eAgInPc2d1	spisovný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
existoval	existovat	k5eAaImAgInS	existovat
dále	daleko	k6eAd2	daleko
za	za	k7c7	za
zdmi	zeď	k1gFnPc7	zeď
klášterů	klášter	k1gInPc2	klášter
jako	jako	k8xC	jako
jazyk	jazyk	k1gInSc1	jazyk
bohoslužebný	bohoslužebný	k2eAgInSc1d1	bohoslužebný
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
"	"	kIx"	"
<g/>
hlavní	hlavní	k2eAgInSc1d1	hlavní
<g/>
"	"	kIx"	"
název	název	k1gInSc1	název
tohoto	tento	k3xDgInSc2	tento
článku	článek	k1gInSc2	článek
je	být	k5eAaImIp3nS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pojem	pojem	k1gInSc4	pojem
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
běžnější	běžný	k2eAgFnSc6d2	běžnější
a	a	k8xC	a
vžitější	vžitý	k2eAgFnSc6d2	vžitý
<g/>
.	.	kIx.	.
</s>
<s>
Staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
má	mít	k5eAaImIp3nS	mít
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
nejstarší	starý	k2eAgFnSc2d3	nejstarší
písemné	písemný	k2eAgFnSc2d1	písemná
památky	památka	k1gFnSc2	památka
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
věrozvěstové	věrozvěst	k1gMnPc1	věrozvěst
Cyril	Cyril	k1gMnSc1	Cyril
a	a	k8xC	a
Metoděj	Metoděj	k1gMnSc1	Metoděj
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přeložili	přeložit	k5eAaPmAgMnP	přeložit
do	do	k7c2	do
staroslověnštiny	staroslověnština	k1gFnSc2	staroslověnština
řadu	řada	k1gFnSc4	řada
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
připravovali	připravovat	k5eAaImAgMnP	připravovat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
misi	mise	k1gFnSc4	mise
na	na	k7c4	na
Velkou	velký	k2eAgFnSc4d1	velká
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Slovanské	slovanský	k2eAgInPc1d1	slovanský
jazyky	jazyk	k1gInPc1	jazyk
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
nebyly	být	k5eNaImAgFnP	být
příliš	příliš	k6eAd1	příliš
diferencované	diferencovaný	k2eAgFnPc1d1	diferencovaná
-	-	kIx~	-
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
existoval	existovat	k5eAaImAgInS	existovat
pouze	pouze	k6eAd1	pouze
jazyk	jazyk	k1gInSc1	jazyk
západoslovanský	západoslovanský	k2eAgInSc1d1	západoslovanský
<g/>
,	,	kIx,	,
východoslovanský	východoslovanský	k2eAgInSc1d1	východoslovanský
a	a	k8xC	a
jihoslovanský	jihoslovanský	k2eAgInSc1d1	jihoslovanský
<g/>
.	.	kIx.	.
</s>
<s>
Staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
posledně	posledně	k6eAd1	posledně
jmenovanému	jmenovaný	k2eAgMnSc3d1	jmenovaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
západní	západní	k2eAgMnPc1d1	západní
Slované	Slovan	k1gMnPc1	Slovan
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Moravě	Morava	k1gFnSc6	Morava
jí	jíst	k5eAaImIp3nS	jíst
nejspíš	nejspíš	k9	nejspíš
dobře	dobře	k6eAd1	dobře
rozuměli	rozumět	k5eAaImAgMnP	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
staroslověnštinu	staroslověnština	k1gFnSc4	staroslověnština
jako	jako	k8xC	jako
liturgický	liturgický	k2eAgInSc4d1	liturgický
jazyk	jazyk	k1gInSc4	jazyk
z	z	k7c2	z
moravského	moravský	k2eAgNnSc2d1	Moravské
území	území	k1gNnSc2	území
latina	latina	k1gFnSc1	latina
<g/>
.	.	kIx.	.
</s>
<s>
Staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
se	se	k3xPyFc4	se
však	však	k9	však
udržela	udržet	k5eAaPmAgFnS	udržet
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
díky	díky	k7c3	díky
žákům	žák	k1gMnPc3	žák
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
<g/>
.	.	kIx.	.
</s>
<s>
Kliment	Kliment	k1gMnSc1	Kliment
Ochridský	Ochridský	k2eAgMnSc1d1	Ochridský
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
zavedení	zavedení	k1gNnSc6	zavedení
jako	jako	k8xC	jako
liturgického	liturgický	k2eAgInSc2d1	liturgický
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
nově	nově	k6eAd1	nově
osamostatněném	osamostatněný	k2eAgNnSc6d1	osamostatněné
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nahradila	nahradit	k5eAaPmAgFnS	nahradit
řečtinu	řečtina	k1gFnSc4	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pravoslavím	pravoslaví	k1gNnSc7	pravoslaví
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
šířila	šířit	k5eAaImAgFnS	šířit
do	do	k7c2	do
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
,	,	kIx,	,
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
a	a	k8xC	a
Kyjevské	kyjevský	k2eAgFnSc2d1	Kyjevská
Rusi	Rus	k1gFnSc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Kyjev	Kyjev	k1gInSc1	Kyjev
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
novým	nový	k2eAgMnSc7d1	nový
centrem	centr	k1gMnSc7	centr
církevněslovanského	církevněslovanský	k2eAgNnSc2d1	církevněslovanský
písemnictví	písemnictví	k1gNnSc2	písemnictví
<g/>
,	,	kIx,	,
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
šířila	šířit	k5eAaImAgFnS	šířit
dále	daleko	k6eAd2	daleko
po	po	k7c6	po
Kyjevské	kyjevský	k2eAgFnSc6d1	Kyjevská
Rusi	Rus	k1gFnSc6	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zmíněných	zmíněný	k2eAgNnPc6d1	zmíněné
územích	území	k1gNnPc6	území
fungovala	fungovat	k5eAaImAgFnS	fungovat
staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
nejen	nejen	k6eAd1	nejen
jako	jako	k9	jako
jazyk	jazyk	k1gInSc1	jazyk
liturgický	liturgický	k2eAgInSc1d1	liturgický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zpočátku	zpočátku	k6eAd1	zpočátku
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
i	i	k9	i
jako	jako	k8xC	jako
jazyk	jazyk	k1gInSc4	jazyk
úřední	úřední	k2eAgInSc4d1	úřední
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
latina	latina	k1gFnSc1	latina
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInPc1d1	národní
jazyky	jazyk	k1gInPc1	jazyk
ještě	ještě	k6eAd1	ještě
neměly	mít	k5eNaImAgInP	mít
své	svůj	k3xOyFgFnPc4	svůj
spisovné	spisovný	k2eAgFnPc4d1	spisovná
normy	norma	k1gFnPc4	norma
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jim	on	k3xPp3gMnPc3	on
spisovným	spisovný	k2eAgMnPc3d1	spisovný
jazykem	jazyk	k1gInSc7	jazyk
byla	být	k5eAaImAgFnS	být
staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
běžný	běžný	k2eAgInSc4d1	běžný
lidový	lidový	k2eAgInSc4d1	lidový
dorozumívací	dorozumívací	k2eAgInSc4d1	dorozumívací
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
podléhala	podléhat	k5eAaImAgFnS	podléhat
vlivům	vliv	k1gInPc3	vliv
místních	místní	k2eAgMnPc2d1	místní
(	(	kIx(	(
<g/>
vesměs	vesměs	k6eAd1	vesměs
slovanských	slovanský	k2eAgInPc2d1	slovanský
<g/>
)	)	kIx)	)
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
měnila	měnit	k5eAaImAgFnS	měnit
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc1	její
výslovnost	výslovnost	k1gFnSc1	výslovnost
i	i	k8xC	i
pravopis	pravopis	k1gInSc1	pravopis
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
nazální	nazální	k2eAgInPc1d1	nazální
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
existovaly	existovat	k5eAaImAgInP	existovat
v	v	k7c6	v
praslovanském	praslovanský	k2eAgInSc6d1	praslovanský
jazyce	jazyk	k1gInSc6	jazyk
i	i	k8xC	i
původní	původní	k2eAgFnSc3d1	původní
staroslověnštině	staroslověnština	k1gFnSc3	staroslověnština
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
uchovaly	uchovat	k5eAaPmAgFnP	uchovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
staroslověnské	staroslověnský	k2eAgFnSc6d1	staroslověnská
abecedě	abeceda	k1gFnSc6	abeceda
jsou	být	k5eAaImIp3nP	být
jim	on	k3xPp3gFnPc3	on
věnovány	věnován	k2eAgInPc1d1	věnován
znaky	znak	k1gInPc1	znak
Ѧ	Ѧ	k?	Ѧ
(	(	kIx(	(
<g/>
ę	ę	k?	ę
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ѫ	Ѫ	k?	Ѫ
(	(	kIx(	(
<g/>
ǫ	ǫ	k?	ǫ
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
malý	malý	k2eAgInSc1d1	malý
a	a	k8xC	a
velký	velký	k2eAgInSc1d1	velký
jus	jus	k?	jus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
církevní	církevní	k2eAgFnSc6d1	církevní
slovanštině	slovanština	k1gFnSc6	slovanština
se	se	k3xPyFc4	se
malý	malý	k2eAgInSc1d1	malý
jus	jus	k?	jus
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
"	"	kIx"	"
<g/>
ja	ja	k?	ja
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
analogie	analogie	k1gFnSc1	analogie
k	k	k7c3	k
hláskovým	hláskový	k2eAgFnPc3d1	hlásková
změnám	změna	k1gFnPc3	změna
u	u	k7c2	u
podobných	podobný	k2eAgNnPc2d1	podobné
slov	slovo	k1gNnPc2	slovo
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
jus	jus	k?	jus
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
staroslověnských	staroslověnský	k2eAgFnPc2d1	staroslověnská
písemných	písemný	k2eAgFnPc2d1	písemná
památek	památka	k1gFnPc2	památka
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pocházejí	pocházet	k5eAaImIp3nP	pocházet
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
původní	původní	k2eAgFnSc2d1	původní
staroslověnštiny	staroslověnština	k1gFnSc2	staroslověnština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
staletích	staletí	k1gNnPc6	staletí
pěstována	pěstovat	k5eAaImNgFnS	pěstovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
jazykové	jazykový	k2eAgNnSc4d1	jazykové
zabarvení	zabarvení	k1gNnSc4	zabarvení
šest	šest	k4xCc1	šest
tzv.	tzv.	kA	tzv.
redakcí	redakce	k1gFnSc7	redakce
<g/>
:	:	kIx,	:
československou	československý	k2eAgFnSc7d1	Československá
<g/>
,	,	kIx,	,
panonsko-slovinskou	panonskolovinský	k2eAgFnSc7d1	panonsko-slovinský
(	(	kIx(	(
<g/>
zastoupena	zastoupen	k2eAgNnPc4d1	zastoupeno
pouze	pouze	k6eAd1	pouze
jedinou	jediný	k2eAgFnSc7d1	jediná
dochovanou	dochovaný	k2eAgFnSc7d1	dochovaná
písemnou	písemný	k2eAgFnSc7d1	písemná
památkou	památka	k1gFnSc7	památka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bulharsko-makedonskou	bulharskoakedonský	k2eAgFnSc7d1	bulharsko-makedonský
srbsko-chorvatskou	srbskohorvatský	k2eAgFnSc7d1	srbsko-chorvatský
<g/>
,	,	kIx,	,
ruskou	ruský	k2eAgFnSc7d1	ruská
<g/>
,	,	kIx,	,
rumunskou	rumunský	k2eAgFnSc7d1	rumunská
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rostoucímu	rostoucí	k2eAgInSc3d1	rostoucí
politickému	politický	k2eAgInSc3d1	politický
vlivu	vliv	k1gInSc3	vliv
Ruska	Ruska	k1gFnSc1	Ruska
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dnes	dnes	k6eAd1	dnes
největší	veliký	k2eAgFnSc2d3	veliký
prestiže	prestiž	k1gFnSc2	prestiž
požívá	požívat	k5eAaImIp3nS	požívat
redakce	redakce	k1gFnSc1	redakce
ruská	ruský	k2eAgFnSc1d1	ruská
a	a	k8xC	a
zpětně	zpětně	k6eAd1	zpětně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
i	i	k9	i
současné	současný	k2eAgNnSc1d1	současné
užívání	užívání	k1gNnSc1	užívání
církevní	církevní	k2eAgFnSc2d1	církevní
slovanštiny	slovanština	k1gFnSc2	slovanština
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Církevní	církevní	k2eAgFnSc1d1	církevní
slovanština	slovanština	k1gFnSc1	slovanština
již	již	k6eAd1	již
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
nefunguje	fungovat	k5eNaImIp3nS	fungovat
jako	jako	k9	jako
mateřský	mateřský	k2eAgInSc4d1	mateřský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
uživatelé	uživatel	k1gMnPc1	uživatel
ji	on	k3xPp3gFnSc4	on
mají	mít	k5eAaImIp3nP	mít
jako	jako	k8xS	jako
druhý	druhý	k4xOgInSc1	druhý
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
navíc	navíc	k6eAd1	navíc
již	již	k6eAd1	již
ztratil	ztratit	k5eAaPmAgMnS	ztratit
funkci	funkce	k1gFnSc4	funkce
jazyka	jazyk	k1gInSc2	jazyk
dorozumívacího	dorozumívací	k2eAgInSc2d1	dorozumívací
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
jen	jen	k9	jen
při	při	k7c6	při
bohoslužbách	bohoslužba	k1gFnPc6	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
statistiky	statistika	k1gFnPc1	statistika
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
odstupňovat	odstupňovat	k5eAaPmF	odstupňovat
míru	míra	k1gFnSc4	míra
zvládnutí	zvládnutí	k1gNnSc2	zvládnutí
jazyka	jazyk	k1gInSc2	jazyk
u	u	k7c2	u
jeho	jeho	k3xOp3gMnPc2	jeho
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
většinou	většina	k1gFnSc7	většina
o	o	k7c4	o
znalost	znalost	k1gFnSc4	znalost
pasivní	pasivní	k2eAgFnSc4d1	pasivní
a	a	k8xC	a
omezenou	omezený	k2eAgFnSc4d1	omezená
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
samotné	samotný	k2eAgInPc1d1	samotný
odhady	odhad	k1gInPc1	odhad
počtu	počet	k1gInSc2	počet
uživatelů	uživatel	k1gMnPc2	uživatel
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
hrubé	hrubý	k2eAgFnPc1d1	hrubá
a	a	k8xC	a
nespolehlivé	spolehlivý	k2eNgFnPc1d1	nespolehlivá
<g/>
,	,	kIx,	,
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
odhadů	odhad	k1gInPc2	odhad
počtu	počet	k1gInSc2	počet
pravoslavných	pravoslavný	k2eAgFnPc2d1	pravoslavná
věřících	věřící	k1gFnPc2	věřící
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c6	o
80	[number]	k4	80
miliónech	milión	k4xCgInPc6	milión
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
žijících	žijící	k2eAgFnPc2d1	žijící
zejména	zejména	k9	zejména
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
plus	plus	k6eAd1	plus
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
připočítat	připočítat	k5eAaPmF	připočítat
řeckokatolíky	řeckokatolík	k1gMnPc4	řeckokatolík
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
používají	používat	k5eAaImIp3nP	používat
církevní	církevní	k2eAgFnSc4d1	církevní
slovanštinu	slovanština	k1gFnSc4	slovanština
na	na	k7c6	na
bohoslužbách	bohoslužba	k1gFnPc6	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
diachronní	diachronní	k2eAgFnSc2d1	diachronní
gramatiky	gramatika	k1gFnSc2	gramatika
řady	řada	k1gFnSc2	řada
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
;	;	kIx,	;
neboť	neboť	k8xC	neboť
jenom	jenom	k9	jenom
prostřednictví	prostřednictví	k1gNnSc1	prostřednictví
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
objasnit	objasnit	k5eAaPmF	objasnit
řada	řada	k1gFnSc1	řada
změn	změna	k1gFnPc2	změna
a	a	k8xC	a
postupů	postup	k1gInPc2	postup
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yIgFnPc3	který
u	u	k7c2	u
řady	řada	k1gFnSc2	řada
slovanských	slovanský	k2eAgMnPc2d1	slovanský
jazyků	jazyk	k1gMnPc2	jazyk
došlo	dojít	k5eAaPmAgNnS	dojít
a	a	k8xC	a
poukázat	poukázat	k5eAaPmF	poukázat
<g/>
,	,	kIx,	,
v	v	k7c6	v
jaké	jaký	k3yIgFnSc6	jaký
míře	míra	k1gFnSc6	míra
a	a	k8xC	a
jakým	jaký	k3yQgNnSc7	jaký
tempem	tempo	k1gNnSc7	tempo
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vzdalovaly	vzdalovat	k5eAaImAgFnP	vzdalovat
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
také	také	k9	také
i	i	k9	i
vliv	vliv	k1gInSc4	vliv
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
terminologie	terminologie	k1gFnSc2	terminologie
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
jazyka	jazyk	k1gInSc2	jazyk
jako	jako	k8xC	jako
takového	takový	k3xDgNnSc2	takový
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
filologických	filologický	k2eAgInPc2d1	filologický
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hlaholice	hlaholice	k1gFnSc2	hlaholice
<g/>
.	.	kIx.	.
</s>
<s>
Cyril	Cyril	k1gMnSc1	Cyril
pro	pro	k7c4	pro
staroslověnštinu	staroslověnština	k1gFnSc4	staroslověnština
nejprve	nejprve	k6eAd1	nejprve
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
nové	nový	k2eAgNnSc4d1	nové
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
hlaholici	hlaholice	k1gFnSc4	hlaholice
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
epizodně	epizodně	k6eAd1	epizodně
ještě	ještě	k9	ještě
ve	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
Sázavském	sázavský	k2eAgInSc6d1	sázavský
klášteře	klášter	k1gInSc6	klášter
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
nejdéle	dlouho	k6eAd3	dlouho
se	se	k3xPyFc4	se
však	však	k9	však
udržela	udržet	k5eAaPmAgFnS	udržet
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
po	po	k7c6	po
rozkolu	rozkol	k1gInSc6	rozkol
církve	církev	k1gFnSc2	církev
přiklonili	přiklonit	k5eAaPmAgMnP	přiklonit
k	k	k7c3	k
římskokatolickému	římskokatolický	k2eAgInSc3d1	římskokatolický
směru	směr	k1gInSc3	směr
a	a	k8xC	a
přijali	přijmout	k5eAaPmAgMnP	přijmout
latinu	latina	k1gFnSc4	latina
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Cyrilice	cyrilice	k1gFnSc2	cyrilice
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
půl	půl	k1xP	půl
století	století	k1gNnSc2	století
později	pozdě	k6eAd2	pozdě
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc3	Bulharsko
cyrilice	cyrilice	k1gFnSc2	cyrilice
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
sestavili	sestavit	k5eAaPmAgMnP	sestavit
Cyrilovi	Cyrilův	k2eAgMnPc1d1	Cyrilův
žáci	žák	k1gMnPc1	žák
<g/>
,	,	kIx,	,
snad	snad	k9	snad
Kliment	Kliment	k1gMnSc1	Kliment
Ochridský	Ochridský	k2eAgMnSc1d1	Ochridský
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
praktičtější	praktický	k2eAgFnSc1d2	praktičtější
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
její	její	k3xOp3gNnPc1	její
písmena	písmeno	k1gNnPc1	písmeno
byla	být	k5eAaImAgNnP	být
jednodušší	jednoduchý	k2eAgNnSc4d2	jednodušší
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
podobala	podobat	k5eAaImAgFnS	podobat
svým	svůj	k3xOyFgInPc3	svůj
řeckým	řecký	k2eAgInPc3d1	řecký
vzorům	vzor	k1gInPc3	vzor
<g/>
,	,	kIx,	,
a	a	k8xC	a
řeckou	řecký	k2eAgFnSc4d1	řecká
abecedu	abeceda	k1gFnSc4	abeceda
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
gramotná	gramotný	k2eAgFnSc1d1	gramotná
vrstva	vrstva	k1gFnSc1	vrstva
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
duchovní	duchovní	k2eAgInSc1d1	duchovní
<g/>
)	)	kIx)	)
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
ovládala	ovládat	k5eAaImAgFnS	ovládat
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Církevní	církevní	k2eAgFnSc1d1	církevní
slovanština	slovanština	k1gFnSc1	slovanština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
šířila	šířit	k5eAaImAgFnS	šířit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
pravoslavím	pravoslaví	k1gNnSc7	pravoslaví
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
byla	být	k5eAaImAgFnS	být
psaná	psaný	k2eAgFnSc1d1	psaná
výhradně	výhradně	k6eAd1	výhradně
cyrilicí	cyrilice	k1gFnSc7	cyrilice
<g/>
;	;	kIx,	;
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
cyrilice	cyrilice	k1gFnSc1	cyrilice
prosadila	prosadit	k5eAaPmAgFnS	prosadit
u	u	k7c2	u
řady	řada	k1gFnSc2	řada
pozdějších	pozdní	k2eAgInPc2d2	pozdější
spisovných	spisovný	k2eAgInPc2d1	spisovný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Staroslověnské	staroslověnský	k2eAgFnPc1d1	staroslověnská
samohlásky	samohláska	k1gFnPc1	samohláska
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
samohlásky	samohláska	k1gFnPc4	samohláska
ústní	ústní	k2eAgFnPc4d1	ústní
(	(	kIx(	(
<g/>
orály	orála	k1gFnPc4	orála
<g/>
)	)	kIx)	)
a	a	k8xC	a
samohlásky	samohláska	k1gFnSc2	samohláska
nosové	nosový	k2eAgFnSc2d1	nosová
(	(	kIx(	(
<g/>
nazály	nazála	k1gFnSc2	nazála
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samohlásky	samohláska	k1gFnPc1	samohláska
ústní	ústní	k2eAgFnSc2d1	ústní
<g/>
:	:	kIx,	:
a	a	k8xC	a
<g/>
,	,	kIx,	,
ě	ě	k?	ě
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
i	i	k9	i
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
ь	ь	k?	ь
<g/>
,	,	kIx,	,
ъ	ъ	k?	ъ
Samohlásky	samohláska	k1gFnSc2	samohláska
ь	ь	k?	ь
a	a	k8xC	a
ъ	ъ	k?	ъ
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
jery	jer	k1gInPc1	jer
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
ultrakrátké	ultrakrátký	k2eAgFnPc4d1	ultrakrátká
samohlásky	samohláska	k1gFnPc4	samohláska
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
vyslovované	vyslovovaný	k2eAgFnPc1d1	vyslovovaná
velmi	velmi	k6eAd1	velmi
krátce	krátce	k6eAd1	krátce
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
kratčeji	krátce	k6eAd2	krátce
než	než	k8xS	než
samohlásky	samohláska	k1gFnPc1	samohláska
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
ь	ь	k?	ь
jako	jako	k8xS	jako
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgNnSc4d1	krátké
i	i	k9	i
<g/>
,	,	kIx,	,
ъ	ъ	k?	ъ
jako	jako	k8xC	jako
velmi	velmi	k6eAd1	velmi
krátké	krátká	k1gFnPc1	krátká
u	u	k7c2	u
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgFnSc2d1	krátká
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samohláska	samohláska	k1gFnSc1	samohláska
ě	ě	k?	ě
se	se	k3xPyFc4	se
vyslovovala	vyslovovat	k5eAaImAgFnS	vyslovovat
v	v	k7c6	v
počáteční	počáteční	k2eAgFnSc6d1	počáteční
fázi	fáze	k1gFnSc6	fáze
staroslověnštiny	staroslověnština	k1gFnSc2	staroslověnština
jako	jako	k8xS	jako
ä	ä	k?	ä
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
dobách	doba	k1gFnPc6	doba
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gNnSc4	její
výslovnost	výslovnost	k1gFnSc1	výslovnost
ja	ja	k?	ja
<g/>
.	.	kIx.	.
</s>
<s>
Samohlásky	samohláska	k1gFnSc2	samohláska
ústní	ústní	k2eAgFnSc2d1	ústní
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
jerů	jer	k1gInPc2	jer
<g/>
)	)	kIx)	)
vždy	vždy	k6eAd1	vždy
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
samohlásky	samohláska	k1gFnPc1	samohláska
ve	v	k7c6	v
staroslověnštině	staroslověnština	k1gFnSc6	staroslověnština
neexistovaly	existovat	k5eNaImAgInP	existovat
ve	v	k7c6	v
fonologickém	fonologický	k2eAgInSc6d1	fonologický
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
případná	případný	k2eAgFnSc1d1	případná
délka	délka	k1gFnSc1	délka
nerozlišovala	rozlišovat	k5eNaImAgFnS	rozlišovat
význam	význam	k1gInSc4	význam
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
ruštině	ruština	k1gFnSc6	ruština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samohlásky	samohláska	k1gFnPc1	samohláska
nosové	nosový	k2eAgFnPc1d1	nosová
<g/>
:	:	kIx,	:
ę	ę	k?	ę
<g/>
,	,	kIx,	,
<g/>
ǫ	ǫ	k?	ǫ
Staroslověnské	staroslověnský	k2eAgFnSc2d1	staroslověnská
souhlásky	souhláska	k1gFnSc2	souhláska
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
roztříděny	roztřídit	k5eAaPmNgInP	roztřídit
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
tvoření	tvoření	k1gNnSc2	tvoření
<g/>
.	.	kIx.	.
</s>
<s>
Souhlásky	souhláska	k1gFnPc1	souhláska
retné	retný	k2eAgFnPc1d1	retný
<g/>
:	:	kIx,	:
b	b	k?	b
<g/>
,	,	kIx,	,
m	m	kA	m
<g/>
,	,	kIx,	,
p	p	k?	p
<g/>
,	,	kIx,	,
v.	v.	k?	v.
Souhlásky	souhláska	k1gFnSc2	souhláska
zubodásňové	zubodásňový	k2eAgNnSc1d1	zubodásňový
<g/>
:	:	kIx,	:
c	c	k0	c
<g/>
,	,	kIx,	,
č	č	k0	č
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
dz	dz	k?	dz
<g/>
,	,	kIx,	,
l	l	kA	l
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
,	,	kIx,	,
r	r	kA	r
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
,	,	kIx,	,
š	š	k?	š
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
,	,	kIx,	,
z	z	k0	z
<g/>
,	,	kIx,	,
ž.	ž.	k?	ž.
Souhlásky	souhláska	k1gFnSc2	souhláska
předopatrové	předopatrová	k1gFnSc2	předopatrová
<g/>
:	:	kIx,	:
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
r	r	kA	r
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Souhlásky	souhláska	k1gFnPc1	souhláska
zadopatrové	zadopatrová	k1gFnSc2	zadopatrová
<g/>
:	:	kIx,	:
g	g	kA	g
<g/>
,	,	kIx,	,
ch	ch	k0	ch
<g/>
,	,	kIx,	,
k.	k.	k?	k.
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
pro	pro	k7c4	pro
hlásku	hláska	k1gFnSc4	hláska
j	j	k?	j
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
hlaholici	hlaholice	k1gFnSc6	hlaholice
samostatný	samostatný	k2eAgInSc4d1	samostatný
znak	znak	k1gInSc4	znak
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
v	v	k7c6	v
nejstarší	starý	k2eAgFnSc6d3	nejstarší
fázi	fáze	k1gFnSc6	fáze
staroslověnštiny	staroslověnština	k1gFnSc2	staroslověnština
hláska	hláska	k1gFnSc1	hláska
j	j	k?	j
byla	být	k5eAaImAgFnS	být
samostatným	samostatný	k2eAgInSc7d1	samostatný
fonémem	foném	k1gInSc7	foném
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Skloňování	skloňování	k1gNnSc2	skloňování
ve	v	k7c6	v
staroslověnštině	staroslověnština	k1gFnSc6	staroslověnština
<g/>
.	.	kIx.	.
</s>
<s>
Staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
rozeznává	rozeznávat	k5eAaImIp3nS	rozeznávat
několik	několik	k4yIc4	několik
způsobů	způsob	k1gInPc2	způsob
skloňování	skloňování	k1gNnSc2	skloňování
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
druhu	druh	k1gInSc2	druh
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
zná	znát	k5eAaImIp3nS	znát
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
čeština	čeština	k1gFnSc1	čeština
sedm	sedm	k4xCc4	sedm
pádů	pád	k1gInPc2	pád
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jednotného	jednotný	k2eAgNnSc2d1	jednotné
a	a	k8xC	a
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
ještě	ještě	k9	ještě
číslo	číslo	k1gNnSc1	číslo
dvojné	dvojný	k2eAgNnSc1d1	dvojné
(	(	kIx(	(
<g/>
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
však	však	k9	však
pádové	pádový	k2eAgInPc1d1	pádový
tvary	tvar	k1gInPc1	tvar
splývají	splývat	k5eAaImIp3nP	splývat
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
tvarů	tvar	k1gInPc2	tvar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
dělíme	dělit	k5eAaImIp1nP	dělit
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
skloňování	skloňování	k1gNnSc2	skloňování
především	především	k9	především
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
skupiny	skupina	k1gFnPc4	skupina
podle	podle	k7c2	podle
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
ve	v	k7c6	v
staroslověnštině	staroslověnština	k1gFnSc6	staroslověnština
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
také	také	k9	také
hledisko	hledisko	k1gNnSc1	hledisko
zakončení	zakončení	k1gNnSc2	zakončení
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Časování	časování	k1gNnSc2	časování
ve	v	k7c6	v
staroslověnštině	staroslověnština	k1gFnSc6	staroslověnština
<g/>
.	.	kIx.	.
</s>
<s>
Staroslověnská	staroslověnský	k2eAgNnPc1d1	staroslověnské
slovesa	sloveso	k1gNnPc1	sloveso
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
kategorie	kategorie	k1gFnPc1	kategorie
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
vidu	vid	k1gInSc2	vid
<g/>
,	,	kIx,	,
způsobu	způsob	k1gInSc2	způsob
a	a	k8xC	a
slovesného	slovesný	k2eAgInSc2d1	slovesný
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Osoby	osoba	k1gFnPc1	osoba
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgNnPc4	tři
<g/>
,	,	kIx,	,
čísla	číslo	k1gNnPc1	číslo
jako	jako	k8xS	jako
u	u	k7c2	u
jmen	jméno	k1gNnPc2	jméno
také	také	k9	také
tři	tři	k4xCgMnPc4	tři
(	(	kIx(	(
<g/>
jednotné	jednotný	k2eAgMnPc4d1	jednotný
<g/>
,	,	kIx,	,
dvojné	dvojný	k2eAgMnPc4d1	dvojný
a	a	k8xC	a
množné	množný	k2eAgMnPc4d1	množný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
slovesných	slovesný	k2eAgInPc2d1	slovesný
časů	čas	k1gInPc2	čas
-	-	kIx~	-
staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
zná	znát	k5eAaImIp3nS	znát
dva	dva	k4xCgInPc4	dva
budoucí	budoucí	k2eAgInPc4d1	budoucí
časy	čas	k1gInPc4	čas
a	a	k8xC	a
několik	několik	k4yIc4	několik
časů	čas	k1gInPc2	čas
minulých	minulý	k2eAgInPc2d1	minulý
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tvary	tvar	k1gInPc1	tvar
tvořené	tvořený	k2eAgInPc1d1	tvořený
od	od	k7c2	od
sloves	sloveso	k1gNnPc2	sloveso
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
uvedené	uvedený	k2eAgFnPc1d1	uvedená
kategorie	kategorie	k1gFnPc1	kategorie
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
podle	podle	k7c2	podle
funkce	funkce	k1gFnSc2	funkce
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
na	na	k7c6	na
určité	určitý	k2eAgFnSc6d1	určitá
a	a	k8xC	a
neurčité	určitý	k2eNgFnSc6d1	neurčitá
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Skladba	skladba	k1gFnSc1	skladba
staroslověnštiny	staroslověnština	k1gFnSc2	staroslověnština
<g/>
.	.	kIx.	.
</s>
<s>
Slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
staroslověnštiny	staroslověnština	k1gFnSc2	staroslověnština
v	v	k7c6	v
převážné	převážný	k2eAgFnSc6d1	převážná
míře	míra	k1gFnSc6	míra
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
všeslovanskou	všeslovanský	k2eAgFnSc4d1	všeslovanská
slovní	slovní	k2eAgFnSc4d1	slovní
zásobu	zásoba	k1gFnSc4	zásoba
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
je	být	k5eAaImIp3nS	být
obohacena	obohacen	k2eAgFnSc1d1	obohacena
novými	nový	k2eAgNnPc7d1	nové
slovy	slovo	k1gNnPc7	slovo
z	z	k7c2	z
domácích	domácí	k2eAgInPc2d1	domácí
základů	základ	k1gInPc2	základ
a	a	k8xC	a
hojnými	hojný	k2eAgFnPc7d1	hojná
přejímkami	přejímka	k1gFnPc7	přejímka
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1	domácí
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
zděděná	zděděný	k2eAgFnSc1d1	zděděná
z	z	k7c2	z
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
především	především	k9	především
slova	slovo	k1gNnSc2	slovo
pro	pro	k7c4	pro
výrazy	výraz	k1gInPc4	výraz
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
souvisely	souviset	k5eAaImAgFnP	souviset
s	s	k7c7	s
běžným	běžný	k2eAgInSc7d1	běžný
životem	život	k1gInSc7	život
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
slova	slovo	k1gNnPc1	slovo
z	z	k7c2	z
okruhu	okruh	k1gInSc2	okruh
příbuzenského	příbuzenský	k2eAgInSc2d1	příbuzenský
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
popisující	popisující	k2eAgFnSc1d1	popisující
společnost	společnost	k1gFnSc1	společnost
a	a	k8xC	a
společenské	společenský	k2eAgInPc1d1	společenský
vztahy	vztah	k1gInPc1	vztah
v	v	k7c6	v
předstátní	předstátní	k2eAgFnSc6d1	předstátní
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
slova	slovo	k1gNnSc2	slovo
popisující	popisující	k2eAgNnSc4d1	popisující
lidské	lidský	k2eAgNnSc4d1	lidské
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
živou	živý	k2eAgFnSc4d1	živá
i	i	k8xC	i
neživou	živý	k2eNgFnSc4d1	neživá
přírodu	příroda	k1gFnSc4	příroda
<g/>
,	,	kIx,	,
hospodaření	hospodaření	k1gNnSc2	hospodaření
a	a	k8xC	a
slova	slovo	k1gNnSc2	slovo
popisující	popisující	k2eAgFnSc4d1	popisující
většinu	většina	k1gFnSc4	většina
dějů	děj	k1gInPc2	děj
souvisejících	související	k2eAgInPc2d1	související
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
oblastmi	oblast	k1gFnPc7	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
zachovala	zachovat	k5eAaPmAgFnS	zachovat
slovanština	slovanština	k1gFnSc1	slovanština
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
možných	možný	k2eAgInPc2d1	možný
starobylých	starobylý	k2eAgInPc2d1	starobylý
(	(	kIx(	(
<g/>
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
<g/>
)	)	kIx)	)
výrazů	výraz	k1gInPc2	výraz
pro	pro	k7c4	pro
jeden	jeden	k4xCgInSc4	jeden
pojem	pojem	k1gInSc4	pojem
jiný	jiný	k2eAgInSc4d1	jiný
než	než	k8xS	než
ostatní	ostatní	k2eAgInPc4d1	ostatní
indoevropské	indoevropský	k2eAgInPc4d1	indoevropský
jazyky	jazyk	k1gInPc4	jazyk
-	-	kIx~	-
např.	např.	kA	např.
pro	pro	k7c4	pro
slovo	slovo	k1gNnSc4	slovo
otec	otec	k1gMnSc1	otec
má	mít	k5eAaImIp3nS	mít
jako	jako	k9	jako
základní	základní	k2eAgInSc1d1	základní
pojem	pojem	k1gInSc1	pojem
otь	otь	k?	otь
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
silnější	silný	k2eAgFnSc1d2	silnější
citová	citový	k2eAgFnSc1d1	citová
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
latinskému	latinský	k2eAgNnSc3d1	latinské
pater	patro	k1gNnPc2	patro
nebo	nebo	k8xC	nebo
německému	německý	k2eAgNnSc3d1	německé
Vater	vatra	k1gFnPc2	vatra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
slovanštině	slovanština	k1gFnSc6	slovanština
nezachovalo	zachovat	k5eNaPmAgNnS	zachovat
<g/>
;	;	kIx,	;
jde	jít	k5eAaImIp3nS	jít
původně	původně	k6eAd1	původně
o	o	k7c4	o
zdrobnělinu	zdrobnělina	k1gFnSc4	zdrobnělina
od	od	k7c2	od
nedoloženého	doložený	k2eNgMnSc2d1	nedoložený
otъ	otъ	k?	otъ
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
latinskému	latinský	k2eAgNnSc3d1	latinské
a	a	k8xC	a
gótskému	gótský	k2eAgNnSc3d1	gótské
atta	atta	k1gMnSc1	atta
(	(	kIx(	(
<g/>
tatíček	tatíček	k1gMnSc1	tatíček
<g/>
,	,	kIx,	,
tatínek	tatínek	k1gMnSc1	tatínek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
u	u	k7c2	u
termínu	termín	k1gInSc2	termín
muž	muž	k1gMnSc1	muž
není	být	k5eNaImIp3nS	být
zachován	zachovat	k5eAaPmNgInS	zachovat
indoevropský	indoevropský	k2eAgInSc1d1	indoevropský
základ	základ	k1gInSc1	základ
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc7	jehož
potomky	potomek	k1gMnPc7	potomek
jsou	být	k5eAaImIp3nP	být
latinské	latinský	k2eAgInPc4d1	latinský
vir	vir	k1gInSc4	vir
<g/>
,	,	kIx,	,
staroindické	staroindický	k2eAgFnPc4d1	staroindická
vī	vī	k?	vī
nebo	nebo	k8xC	nebo
litevské	litevský	k2eAgInPc1d1	litevský
vyras	vyras	k1gInSc1	vyras
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
něho	on	k3xPp3gMnSc2	on
má	mít	k5eAaImIp3nS	mít
slovanština	slovanština	k1gFnSc1	slovanština
slovo	slovo	k1gNnSc1	slovo
mǫ	mǫ	k?	mǫ
<g/>
,	,	kIx,	,
odvozené	odvozený	k2eAgInPc1d1	odvozený
od	od	k7c2	od
jiného	jiný	k2eAgInSc2d1	jiný
základu	základ	k1gInSc2	základ
man-	man-	k?	man-
(	(	kIx(	(
<g/>
shodně	shodně	k6eAd1	shodně
germánské	germánský	k2eAgInPc1d1	germánský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
praslovanština	praslovanština	k1gFnSc1	praslovanština
i	i	k9	i
slova	slovo	k1gNnPc4	slovo
přejatá	přejatý	k2eAgNnPc4d1	přejaté
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
z	z	k7c2	z
předindoevropských	předindoevropský	k2eAgInPc2d1	předindoevropský
jazyků	jazyk	k1gInPc2	jazyk
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
slova	slovo	k1gNnSc2	slovo
pro	pro	k7c4	pro
rostliny	rostlina	k1gFnPc4	rostlina
a	a	k8xC	a
živočichy	živočich	k1gMnPc4	živočich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
v	v	k7c6	v
předstaroslověnském	předstaroslověnský	k2eAgNnSc6d1	předstaroslověnský
období	období	k1gNnSc6	období
pak	pak	k6eAd1	pak
přijímala	přijímat	k5eAaImAgNnP	přijímat
i	i	k9	i
slova	slovo	k1gNnPc1	slovo
z	z	k7c2	z
jiných	jiný	k2eAgMnPc2d1	jiný
indoevropských	indoevropský	k2eAgMnPc2d1	indoevropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
byla	být	k5eAaImAgFnS	být
převážně	převážně	k6eAd1	převážně
jazyk	jazyk	k1gInSc4	jazyk
náboženský	náboženský	k2eAgInSc4d1	náboženský
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
specifika	specifika	k1gFnSc1	specifika
její	její	k3xOp3gFnSc2	její
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
odrážejí	odrážet	k5eAaImIp3nP	odrážet
především	především	k9	především
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
mytologických	mytologický	k2eAgInPc2d1	mytologický
a	a	k8xC	a
básnických	básnický	k2eAgInPc2d1	básnický
výtvorů	výtvor	k1gInPc2	výtvor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
ústní	ústní	k2eAgFnSc6d1	ústní
podobě	podoba	k1gFnSc6	podoba
u	u	k7c2	u
Slovanů	Slovan	k1gInPc2	Slovan
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
<g/>
,	,	kIx,	,
nestačil	stačit	k5eNaBmAgInS	stačit
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
pojmů	pojem	k1gInPc2	pojem
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přinášelo	přinášet	k5eAaImAgNnS	přinášet
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
staroslověnštinu	staroslověnština	k1gFnSc4	staroslověnština
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
doplněním	doplnění	k1gNnSc7	doplnění
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
dělo	dít	k5eAaBmAgNnS	dít
jednak	jednak	k8xC	jednak
přehodnocováním	přehodnocování	k1gNnSc7	přehodnocování
významu	význam	k1gInSc2	význam
již	již	k6eAd1	již
existujících	existující	k2eAgNnPc2d1	existující
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
mnohem	mnohem	k6eAd1	mnohem
častěji	často	k6eAd2	často
<g/>
)	)	kIx)	)
tvorbou	tvorba	k1gFnSc7	tvorba
nových	nový	k2eAgNnPc2d1	nové
slov	slovo	k1gNnPc2	slovo
podle	podle	k7c2	podle
cizích	cizí	k2eAgInPc2d1	cizí
vzorů	vzor	k1gInPc2	vzor
nebo	nebo	k8xC	nebo
rovnou	rovnou	k6eAd1	rovnou
jejich	jejich	k3xOp3gNnSc7	jejich
přejímáním	přejímání	k1gNnSc7	přejímání
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
<g/>
,	,	kIx,	,
náboženský	náboženský	k2eAgInSc1d1	náboženský
význam	význam	k1gInSc1	význam
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
slovu	slovo	k1gNnSc3	slovo
grěchъ	grěchъ	k?	grěchъ
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
hřích	hřích	k1gInSc4	hřích
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
původně	původně	k6eAd1	původně
mělo	mít	k5eAaImAgNnS	mít
obecný	obecný	k2eAgInSc4d1	obecný
význam	význam	k1gInSc4	význam
chyba	chyba	k1gFnSc1	chyba
<g/>
,	,	kIx,	,
omyl	omyl	k1gInSc1	omyl
<g/>
,	,	kIx,	,
nedopatření	nedopatření	k1gNnSc1	nedopatření
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
slovu	slovo	k1gNnSc3	slovo
pь	pь	k?	pь
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
původně	původně	k6eAd1	původně
znamenalo	znamenat	k5eAaImAgNnS	znamenat
smůla	smůla	k1gFnSc1	smůla
(	(	kIx(	(
<g/>
nový	nový	k2eAgInSc1d1	nový
význam	význam	k1gInSc1	význam
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
užívání	užívání	k1gNnSc2	užívání
smůly	smůla	k1gFnSc2	smůla
v	v	k7c6	v
boji	boj	k1gInSc6	boj
a	a	k8xC	a
při	při	k7c6	při
trestání	trestání	k1gNnSc6	trestání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgNnPc1d1	nové
slova	slovo	k1gNnPc1	slovo
z	z	k7c2	z
domácích	domácí	k2eAgMnPc2d1	domácí
zdrojů	zdroj	k1gInPc2	zdroj
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nejčastěji	často	k6eAd3	často
napodobením	napodobení	k1gNnSc7	napodobení
řečtiny	řečtina	k1gFnSc2	řečtina
(	(	kIx(	(
<g/>
blagověstie	blagověstie	k1gFnSc1	blagověstie
-	-	kIx~	-
evangelium	evangelium	k1gNnSc1	evangelium
<g/>
,	,	kIx,	,
zakonoučitelь	zakonoučitelь	k?	zakonoučitelь
-	-	kIx~	-
učitel	učitel	k1gMnSc1	učitel
zákona	zákon	k1gInSc2	zákon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
však	však	k9	však
samostatně	samostatně	k6eAd1	samostatně
(	(	kIx(	(
<g/>
učenikъ	učenikъ	k?	učenikъ
-	-	kIx~	-
učedník	učedník	k1gMnSc1	učedník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
přejímkami	přejímka	k1gFnPc7	přejímka
rovněž	rovněž	k9	rovněž
převládají	převládat	k5eAaImIp3nP	převládat
slova	slovo	k1gNnSc2	slovo
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
:	:	kIx,	:
adъ	adъ	k?	adъ
(	(	kIx(	(
<g/>
podsvětí	podsvětí	k1gNnSc1	podsvětí
<g/>
,	,	kIx,	,
z	z	k7c2	z
řec.	řec.	k?	řec.
adis	adisa	k1gFnPc2	adisa
<g/>
,	,	kIx,	,
starořecky	starořecky	k6eAd1	starořecky
(	(	kIx(	(
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
ádés	ádés	k1gInSc1	ádés
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trapeza	trapeza	k1gFnSc1	trapeza
(	(	kIx(	(
<g/>
stůl	stůl	k1gInSc1	stůl
<g/>
,	,	kIx,	,
z	z	k7c2	z
řec.	řec.	k?	řec.
trapeza	trapeza	k1gFnSc1	trapeza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architektonъ	architektonъ	k?	architektonъ
(	(	kIx(	(
<g/>
stavitel	stavitel	k1gMnSc1	stavitel
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
z	z	k7c2	z
řec.	řec.	k?	řec.
architektos	architektos	k1gInSc1	architektos
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
Některá	některý	k3yIgNnPc1	některý
slova	slovo	k1gNnPc1	slovo
byla	být	k5eAaImAgNnP	být
přejata	přejmout	k5eAaPmNgNnP	přejmout
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
nebo	nebo	k8xC	nebo
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
livra	livra	k1gFnSc1	livra
<g/>
,	,	kIx,	,
olъ	olъ	k?	olъ
<g/>
,	,	kIx,	,
olěi	olě	k1gFnPc1	olě
<g/>
,	,	kIx,	,
mь	mь	k?	mь
<g/>
,	,	kIx,	,
križь	križь	k?	križь
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
germánským	germánský	k2eAgNnSc7d1	germánské
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
(	(	kIx(	(
<g/>
např.	např.	kA	např.
postъ	postъ	k?	postъ
<g/>
,	,	kIx,	,
mъ	mъ	k?	mъ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Latinské	latinský	k2eAgFnPc1d1	Latinská
a	a	k8xC	a
románské	románský	k2eAgFnPc1d1	románská
přejímky	přejímka	k1gFnPc1	přejímka
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
především	především	k9	především
v	v	k7c6	v
textech	text	k1gInPc6	text
moravského	moravský	k2eAgMnSc2d1	moravský
a	a	k8xC	a
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
v	v	k7c6	v
mladších	mladý	k2eAgInPc6d2	mladší
textech	text	k1gInPc6	text
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
bulharské	bulharský	k2eAgFnSc2d1	bulharská
redakce	redakce	k1gFnSc2	redakce
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
domácími	domácí	k2eAgNnPc7d1	domácí
slovy	slovo	k1gNnPc7	slovo
nebo	nebo	k8xC	nebo
výpůjčkami	výpůjčka	k1gFnPc7	výpůjčka
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
.	.	kIx.	.
heslo	heslo	k1gNnSc4	heslo
Staroslověnský	staroslověnský	k2eAgInSc1d1	staroslověnský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
XXIII	XXIII	kA	XXIII
<g/>
,	,	kIx,	,
p.	p.	k?	p.
1062	[number]	k4	1062
Alexandr	Alexandr	k1gMnSc1	Alexandr
Stich	Stich	k1gMnSc1	Stich
<g/>
,	,	kIx,	,
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Janáčková	Janáčková	k1gFnSc1	Janáčková
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Lehár	Lehár	k1gMnSc1	Lehár
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Holý	Holý	k1gMnSc1	Holý
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
k	k	k7c3	k
dnešku	dnešek	k1gInSc3	dnešek
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
308	[number]	k4	308
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Vašica	Vašic	k1gInSc2	Vašic
1940	[number]	k4	1940
Jan	Jan	k1gMnSc1	Jan
Máchal	Máchal	k1gMnSc1	Máchal
<g/>
:	:	kIx,	:
Slovanské	slovanský	k2eAgFnSc2d1	Slovanská
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
I	I	kA	I
<g/>
-	-	kIx~	-
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Glanville	Glanville	k1gFnSc1	Glanville
Price	Priec	k1gInSc2	Priec
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
jazyků	jazyk	k1gInPc2	jazyk
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
(	(	kIx(	(
<g/>
originál	originál	k1gInSc1	originál
Blackwell	Blackwell	k1gInSc1	Blackwell
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
450	[number]	k4	450
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Kurz	Kurz	k1gMnSc1	Kurz
<g/>
:	:	kIx,	:
Učebnice	učebnice	k1gFnSc1	učebnice
jazyka	jazyk	k1gInSc2	jazyk
staroslověnského	staroslověnský	k2eAgInSc2d1	staroslověnský
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Radoslav	Radoslav	k1gMnSc1	Radoslav
Večerka	Večerka	k1gMnSc1	Večerka
<g/>
:	:	kIx,	:
Staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2047	[number]	k4	2047
Seznam	seznam	k1gInSc4	seznam
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
jazykových	jazykový	k2eAgFnPc2d1	jazyková
rodin	rodina	k1gFnPc2	rodina
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Old	Olda	k1gFnPc2	Olda
Church	Church	k1gInSc4	Church
Slavonic	Slavonice	k1gFnPc2	Slavonice
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Texas	Texas	k1gInSc1	Texas
Staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
na	na	k7c4	na
Omniglotu	Omniglota	k1gFnSc4	Omniglota
Church	Church	k1gMnSc1	Church
Slavonic	Slavonice	k1gFnPc2	Slavonice
E-Tutor	E-Tutor	k1gMnSc1	E-Tutor
Staroslavenski	Staroslavensk	k1gFnSc2	Staroslavensk
institut	institut	k1gInSc1	institut
<g/>
,	,	kIx,	,
Zagreb	Zagreb	k1gInSc1	Zagreb
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
tabulek	tabulka	k1gFnPc2	tabulka
pro	pro	k7c4	pro
hlaholici	hlaholice	k1gFnSc4	hlaholice
<g/>
)	)	kIx)	)
</s>
