<s>
Afanasij	Afanasít	k5eAaPmRp2nS	Afanasít
Afanasjevič	Afanasjevič	k1gMnSc1	Afanasjevič
Fet	Fet	k1gMnSc1	Fet
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
А	А	k?	А
А	А	k?	А
Ф	Ф	k?	Ф
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1820	[number]	k4	1820
poblíž	poblíž	k7c2	poblíž
Mcenska	Mcensko	k1gNnSc2	Mcensko
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1892	[number]	k4	1892
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
změnil	změnit	k5eAaPmAgMnS	změnit
jméno	jméno	k1gNnSc4	jméno
na	na	k7c4	na
Šenšin	Šenšin	k2eAgInSc4d1	Šenšin
(	(	kIx(	(
<g/>
Ш	Ш	k?	Ш
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
postav	postava	k1gFnPc2	postava
ruské	ruský	k2eAgFnSc2d1	ruská
poezie	poezie	k1gFnSc2	poezie
poslední	poslední	k2eAgFnSc2d1	poslední
čtvrtiny	čtvrtina	k1gFnSc2	čtvrtina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
