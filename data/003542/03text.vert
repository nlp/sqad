<s>
Bojnický	bojnický	k2eAgInSc1d1	bojnický
zámek	zámek	k1gInSc1	zámek
je	být	k5eAaImIp3nS	být
romantický	romantický	k2eAgInSc1d1	romantický
zámek	zámek	k1gInSc1	zámek
s	s	k7c7	s
částečně	částečně	k6eAd1	částečně
ponechaným	ponechaný	k2eAgInSc7d1	ponechaný
původně	původně	k6eAd1	původně
gotickým	gotický	k2eAgInSc7d1	gotický
a	a	k8xC	a
renesančním	renesanční	k2eAgInSc7d1	renesanční
základem	základ	k1gInSc7	základ
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
Trenčínském	trenčínský	k2eAgInSc6d1	trenčínský
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
Bojnice	Bojnice	k1gFnPc1	Bojnice
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
je	být	k5eAaImIp3nS	být
viditelný	viditelný	k2eAgInSc1d1	viditelný
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
horního	horní	k2eAgMnSc2d1	horní
Ponitří	Ponitří	k1gMnSc2	Ponitří
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
travertinové	travertinový	k2eAgFnSc6d1	travertinová
skále	skála	k1gFnSc6	skála
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
ho	on	k3xPp3gInSc4	on
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
a	a	k8xC	a
venkovní	venkovní	k2eAgInSc4d1	venkovní
hrad	hrad	k1gInSc4	hrad
s	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
nádvořími	nádvoří	k1gNnPc7	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
Národní	národní	k2eAgFnSc4d1	národní
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Archivní	archivní	k2eAgFnPc1d1	archivní
doložené	doložený	k2eAgFnPc1d1	doložená
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
hradu	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
o	o	k7c6	o
hradišti	hradiště	k1gNnSc6	hradiště
nacházejícím	nacházející	k2eAgNnSc6d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
hradu	hrad	k1gInSc6	hrad
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
zoborského	zoborský	k2eAgNnSc2d1	zoborský
opatství	opatství	k1gNnSc2	opatství
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1113	[number]	k4	1113
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
hradiště	hradiště	k1gNnSc1	hradiště
bylo	být	k5eAaImAgNnS	být
dřevěné	dřevěný	k2eAgNnSc1d1	dřevěné
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hrad	hrad	k1gInSc1	hrad
patřil	patřit	k5eAaImAgInS	patřit
rodu	rod	k1gInSc3	rod
Poznanů	Poznan	k1gInPc2	Poznan
(	(	kIx(	(
<g/>
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
jméno	jméno	k1gNnSc4	jméno
rodu	rod	k1gInSc2	rod
jako	jako	k8xS	jako
Hont-Poznanů	Hont-Poznan	k1gInPc2	Hont-Poznan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
přestavěn	přestavět	k5eAaPmNgInS	přestavět
na	na	k7c4	na
kamenný	kamenný	k2eAgInSc4d1	kamenný
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgMnPc7	první
feudálními	feudální	k2eAgMnPc7d1	feudální
majiteli	majitel	k1gMnPc7	majitel
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
jména	jméno	k1gNnPc1	jméno
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgNnP	dochovat
v	v	k7c6	v
historických	historický	k2eAgInPc6d1	historický
pramenech	pramen	k1gInPc6	pramen
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
právě	právě	k9	právě
synové	syn	k1gMnPc1	syn
velmože	velmož	k1gMnSc2	velmož
Kazimíra	Kazimír	k1gMnSc2	Kazimír
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Poznanů	Poznan	k1gInPc2	Poznan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
hrad	hrad	k1gInSc1	hrad
získal	získat	k5eAaPmAgInS	získat
Matúš	Matúš	k1gInSc1	Matúš
Čák	čáko	k1gNnPc2	čáko
Trenčanský	Trenčanský	k2eAgMnSc1d1	Trenčanský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc7	jeho
majitelem	majitel	k1gMnSc7	majitel
až	až	k8xS	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1321	[number]	k4	1321
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Matúše	Matúše	k1gFnSc2	Matúše
Čáka	čáko	k1gNnSc2	čáko
Trenčínského	trenčínský	k2eAgNnSc2d1	trenčínské
se	se	k3xPyFc4	se
hrad	hrad	k1gInSc1	hrad
stal	stát	k5eAaPmAgInS	stát
královským	královský	k2eAgInSc7d1	královský
majetkem	majetek	k1gInSc7	majetek
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
několikrát	několikrát	k6eAd1	několikrát
změnil	změnit	k5eAaPmAgInS	změnit
majitele	majitel	k1gMnSc4	majitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
patřil	patřit	k5eAaImAgInS	patřit
hrad	hrad	k1gInSc4	hrad
rodu	rod	k1gInSc2	rod
Gilethů	Gileth	k1gMnPc2	Gileth
<g/>
,	,	kIx,	,
Ladislavu	Ladislava	k1gFnSc4	Ladislava
z	z	k7c2	z
Opole	Opole	k1gFnSc2	Opole
<g/>
,	,	kIx,	,
Leustachům	Leustach	k1gMnPc3	Leustach
<g/>
,	,	kIx,	,
pánům	pan	k1gMnPc3	pan
z	z	k7c2	z
Jelšavy	Jelšava	k1gFnSc2	Jelšava
a	a	k8xC	a
Noffryům	Noffry	k1gMnPc3	Noffry
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Noffryové	Noffryus	k1gMnPc1	Noffryus
se	se	k3xPyFc4	se
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
o	o	k7c4	o
rozšíření	rozšíření	k1gNnSc4	rozšíření
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
i	i	k9	i
opevnění	opevněný	k2eAgMnPc1d1	opevněný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
vymření	vymření	k1gNnSc4	vymření
jejich	jejich	k3xOp3gInSc2	jejich
rodu	rod	k1gInSc2	rod
přešel	přejít	k5eAaPmAgInS	přejít
hrad	hrad	k1gInSc1	hrad
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
Jánoše	Jánoš	k1gMnSc2	Jánoš
Korvína	Korvín	k1gMnSc2	Korvín
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
nemanželským	manželský	k2eNgMnSc7d1	nemanželský
synem	syn	k1gMnSc7	syn
Matyáše	Matyáš	k1gMnSc2	Matyáš
Korvína	Korvín	k1gMnSc2	Korvín
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověstí	pověst	k1gFnPc2	pověst
král	král	k1gMnSc1	král
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
často	často	k6eAd1	často
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
Bojnice	Bojnice	k1gFnPc4	Bojnice
<g/>
.	.	kIx.	.
</s>
<s>
Úřední	úřední	k2eAgMnSc1d1	úřední
listiny	listina	k1gFnPc4	listina
diktoval	diktovat	k5eAaImAgMnS	diktovat
a	a	k8xC	a
podepisoval	podepisovat	k5eAaImAgMnS	podepisovat
pod	pod	k7c7	pod
lípou	lípa	k1gFnSc7	lípa
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Dokumenty	dokument	k1gInPc4	dokument
končil	končit	k5eAaImAgInS	končit
vždy	vždy	k6eAd1	vždy
větou	věta	k1gFnSc7	věta
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Sub	sub	k7c4	sub
Nostra	Nostr	k1gMnSc4	Nostr
dilectis	dilectis	k1gFnSc4	dilectis
Tillis	Tillis	k1gFnSc1	Tillis
bojniciensibus	bojniciensibus	k1gMnSc1	bojniciensibus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
"	"	kIx"	"
<g/>
Pod	pod	k7c7	pod
našimi	náš	k3xOp1gFnPc7	náš
milými	milý	k2eAgFnPc7d1	Milá
lipami	lípa	k1gFnPc7	lípa
Bojnickými	bojnický	k2eAgFnPc7d1	Bojnická
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Matyáše	Matyáš	k1gMnSc2	Matyáš
Korvína	Korvín	k1gInSc2	Korvín
hrad	hrad	k1gInSc4	hrad
získali	získat	k5eAaPmAgMnP	získat
Zápolští	Zápolský	k2eAgMnPc1d1	Zápolský
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
jeho	jeho	k3xOp3gMnPc7	jeho
majiteli	majitel	k1gMnPc7	majitel
od	od	k7c2	od
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
získali	získat	k5eAaPmAgMnP	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1527	[number]	k4	1527
hrad	hrad	k1gInSc4	hrad
Turzové	Turzová	k1gFnSc2	Turzová
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Turzo	Turza	k1gFnSc5	Turza
hrad	hrad	k1gInSc1	hrad
značně	značně	k6eAd1	značně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
a	a	k8xC	a
nechal	nechat	k5eAaPmAgInS	nechat
zpevnit	zpevnit	k5eAaPmF	zpevnit
opevnění	opevnění	k1gNnSc4	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Změnil	změnit	k5eAaPmAgInS	změnit
i	i	k9	i
charakter	charakter	k1gInSc1	charakter
stavby	stavba	k1gFnSc2	stavba
-	-	kIx~	-
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
gotický	gotický	k2eAgInSc1d1	gotický
hrad	hrad	k1gInSc1	hrad
nechal	nechat	k5eAaPmAgInS	nechat
přestavět	přestavět	k5eAaPmF	přestavět
na	na	k7c4	na
renesanční	renesanční	k2eAgInSc4d1	renesanční
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
rod	rod	k1gInSc1	rod
Turzů	Turz	k1gInPc2	Turz
vymřel	vymřít	k5eAaPmAgInS	vymřít
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
královským	královský	k2eAgInSc7d1	královský
majetkem	majetek	k1gInSc7	majetek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1644	[number]	k4	1644
ho	on	k3xPp3gMnSc4	on
král	král	k1gMnSc1	král
daroval	darovat	k5eAaPmAgMnS	darovat
rodině	rodina	k1gFnSc3	rodina
Pálfyů	Pálfy	k1gMnPc2	Pálfy
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přestavěli	přestavět	k5eAaPmAgMnP	přestavět
do	do	k7c2	do
barokního	barokní	k2eAgInSc2d1	barokní
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
přestavbu	přestavba	k1gFnSc4	přestavba
respektovali	respektovat	k5eAaImAgMnP	respektovat
pro	pro	k7c4	pro
vysokou	vysoký	k2eAgFnSc4d1	vysoká
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
hodnotu	hodnota	k1gFnSc4	hodnota
i	i	k9	i
při	při	k7c6	při
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
stavebních	stavební	k2eAgFnPc6d1	stavební
úpravách	úprava	k1gFnPc6	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
přestavbě	přestavba	k1gFnSc6	přestavba
zámku	zámek	k1gInSc2	zámek
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1662	[number]	k4	1662
barokní	barokní	k2eAgFnSc1d1	barokní
kaple	kaple	k1gFnSc1	kaple
s	s	k7c7	s
jedinečnou	jedinečný	k2eAgFnSc7d1	jedinečná
štukovou	štukový	k2eAgFnSc7d1	štuková
klenbou	klenba	k1gFnSc7	klenba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c4	o
figurální	figurální	k2eAgFnSc4d1	figurální
fresku	freska	k1gFnSc4	freska
s	s	k7c7	s
náboženskými	náboženský	k2eAgInPc7d1	náboženský
motivy	motiv	k1gInPc7	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Vybudovaly	vybudovat	k5eAaPmAgInP	vybudovat
se	se	k3xPyFc4	se
i	i	k9	i
nové	nový	k2eAgFnPc1d1	nová
obytné	obytný	k2eAgFnPc1d1	obytná
budovy	budova	k1gFnPc1	budova
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
situovaly	situovat	k5eAaBmAgFnP	situovat
do	do	k7c2	do
někdejšího	někdejší	k2eAgNnSc2d1	někdejší
předhradí	předhradí	k1gNnSc2	předhradí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1889	[number]	k4	1889
až	až	k9	až
1910	[number]	k4	1910
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
poslední	poslední	k2eAgMnSc1d1	poslední
feudální	feudální	k2eAgMnSc1d1	feudální
majitel	majitel	k1gMnSc1	majitel
Ján	Ján	k1gMnSc1	Ján
František	František	k1gMnSc1	František
Pálfi	Pálf	k1gFnSc2	Pálf
poslední	poslední	k2eAgFnSc4d1	poslední
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
přestavbu	přestavba	k1gFnSc4	přestavba
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
neogotickém	ogotický	k2eNgInSc6d1	neogotický
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
přestavbě	přestavba	k1gFnSc3	přestavba
zámek	zámek	k1gInSc4	zámek
nabyl	nabýt	k5eAaPmAgInS	nabýt
své	svůj	k3xOyFgInPc4	svůj
charakteristické	charakteristický	k2eAgInPc4d1	charakteristický
rysy	rys	k1gInPc4	rys
a	a	k8xC	a
současný	současný	k2eAgInSc4d1	současný
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Ján	Ján	k1gMnSc1	Ján
František	František	k1gMnSc1	František
Pálfi	Pálf	k1gMnSc3	Pálf
se	se	k3xPyFc4	se
osobně	osobně	k6eAd1	osobně
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
účastnil	účastnit	k5eAaImAgMnS	účastnit
přestavby	přestavba	k1gFnPc4	přestavba
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vzor	vzor	k1gInSc4	vzor
mu	on	k3xPp3gMnSc3	on
posloužily	posloužit	k5eAaPmAgFnP	posloužit
zejména	zejména	k9	zejména
monumentální	monumentální	k2eAgFnPc1d1	monumentální
středověké	středověký	k2eAgFnPc1d1	středověká
evropské	evropský	k2eAgFnPc1d1	Evropská
architektury	architektura	k1gFnPc1	architektura
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
zámek	zámek	k1gInSc4	zámek
a	a	k8xC	a
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
patřící	patřící	k2eAgFnPc4d1	patřící
pozemky	pozemka	k1gFnPc4	pozemka
koupila	koupit	k5eAaPmAgFnS	koupit
firma	firma	k1gFnSc1	firma
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
Benešových	Benešových	k2eAgInPc2d1	Benešových
dekretů	dekret	k1gInPc2	dekret
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
připadl	připadnout	k5eAaPmAgInS	připadnout
její	její	k3xOp3gInSc1	její
majetek	majetek	k1gInSc1	majetek
státu	stát	k1gInSc2	stát
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1950	[number]	k4	1950
zámek	zámek	k1gInSc1	zámek
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
náklady	náklad	k1gInPc4	náklad
státu	stát	k1gInSc2	stát
byly	být	k5eAaImAgFnP	být
odstraněny	odstranit	k5eAaPmNgInP	odstranit
následky	následek	k1gInPc1	následek
požáru	požár	k1gInSc2	požár
a	a	k8xC	a
převedla	převést	k5eAaPmAgFnS	převést
se	se	k3xPyFc4	se
celková	celkový	k2eAgFnSc1d1	celková
obnova	obnova	k1gFnSc1	obnova
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
bude	být	k5eAaImBp3nS	být
zřízeno	zřízen	k2eAgNnSc1d1	zřízeno
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
Národní	národní	k2eAgFnSc4d1	národní
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
<g/>
.	.	kIx.	.
42	[number]	k4	42
let	léto	k1gNnPc2	léto
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Jana	Jan	k1gMnSc2	Jan
Františka	František	k1gMnSc2	František
Pálfiho	Pálfi	k1gMnSc2	Pálfi
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
splnilo	splnit	k5eAaPmAgNnS	splnit
jeho	jeho	k3xOp3gNnSc1	jeho
přání	přání	k1gNnSc1	přání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
ze	z	k7c2	z
zámku	zámek	k1gInSc2	zámek
stalo	stát	k5eAaPmAgNnS	stát
muzeum	muzeum	k1gNnSc1	muzeum
přístupné	přístupný	k2eAgFnSc2d1	přístupná
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Bojnický	bojnický	k2eAgInSc1d1	bojnický
zámek	zámek	k1gInSc1	zámek
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
romantický	romantický	k2eAgInSc1d1	romantický
charakter	charakter	k1gInSc1	charakter
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vidět	vidět	k5eAaImF	vidět
i	i	k8xC	i
na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
obytné	obytný	k2eAgFnSc2d1	obytná
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
členěná	členěný	k2eAgFnSc1d1	členěná
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
otvory	otvor	k1gInPc1	otvor
v	v	k7c6	v
hradbách	hradba	k1gFnPc6	hradba
<g/>
,	,	kIx,	,
portály	portál	k1gInPc4	portál
<g/>
,	,	kIx,	,
stěny	stěna	k1gFnPc4	stěna
<g/>
,	,	kIx,	,
atiky	atika	k1gFnPc4	atika
a	a	k8xC	a
různé	různý	k2eAgInPc4d1	různý
kované	kovaný	k2eAgInPc4d1	kovaný
detaily	detail	k1gInPc4	detail
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc1d1	typický
pro	pro	k7c4	pro
romantickou	romantický	k2eAgFnSc4d1	romantická
architekturu	architektura	k1gFnSc4	architektura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc4	zámek
součástí	součást	k1gFnPc2	součást
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
Národní	národní	k2eAgFnSc4d1	národní
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
veřejnosti	veřejnost	k1gFnSc3	veřejnost
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
pod	pod	k7c7	pod
zámkem	zámek	k1gInSc7	zámek
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Bojnickému	bojnický	k2eAgInSc3d1	bojnický
zámku	zámek	k1gInSc3	zámek
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
přilehlý	přilehlý	k2eAgInSc1d1	přilehlý
zámecký	zámecký	k2eAgInSc1d1	zámecký
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
roste	růst	k5eAaImIp3nS	růst
mnoho	mnoho	k4c4	mnoho
různých	různý	k2eAgInPc2d1	různý
exemplářů	exemplář	k1gInPc2	exemplář
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
sedmisetletá	sedmisetletý	k2eAgFnSc1d1	sedmisetletá
<g/>
,	,	kIx,	,
Lípa	lípa	k1gFnSc1	lípa
krále	král	k1gMnSc2	král
Matyáše	Matyáš	k1gMnSc2	Matyáš
před	před	k7c7	před
vchodem	vchod	k1gInSc7	vchod
do	do	k7c2	do
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
lípa	lípa	k1gFnSc1	lípa
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejstarší	starý	k2eAgInSc1d3	nejstarší
strom	strom	k1gInSc1	strom
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
zámeckého	zámecký	k2eAgInSc2d1	zámecký
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
i	i	k9	i
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
vzácné	vzácný	k2eAgInPc1d1	vzácný
exempláře	exemplář	k1gInPc1	exemplář
cizokrajných	cizokrajný	k2eAgMnPc2d1	cizokrajný
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dále	daleko	k6eAd2	daleko
lesoparkem	lesopark	k1gInSc7	lesopark
v	v	k7c6	v
Strážovských	Strážovský	k2eAgInPc6d1	Strážovský
vrších	vrch	k1gInPc6	vrch
<g/>
.	.	kIx.	.
</s>
