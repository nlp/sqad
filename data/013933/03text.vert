<s>
Henrik	Henrik	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
Porthan	Porthan	k1gMnSc1
</s>
<s>
Henrik	Henrik	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
Porthan	Porthan	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1739	#num#	k4
Viitasaari	Viitasaari	k1gNnPc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1804	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
64	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Turku	Turek	k1gMnSc6
Povolání	povolání	k1gNnSc1
</s>
<s>
historik	historik	k1gMnSc1
<g/>
,	,	kIx,
pedagog	pedagog	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Královská	královský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
v	v	k7c6
Turku	turek	k1gInSc6
Rodiče	rodič	k1gMnPc1
</s>
<s>
Sigfrid	Sigfrid	k1gInSc1
Porthan	Porthan	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
galerie	galerie	k1gFnSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Henrik	Henrik	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
Porthan	Porthan	k1gMnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1739	#num#	k4
<g/>
,	,	kIx,
Viitasaari	Viitasaare	k1gFnSc4
–	–	k?
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1804	#num#	k4
<g/>
,	,	kIx,
Turku	Turek	k1gMnSc5
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
profesor	profesor	k1gMnSc1
a	a	k8xC
knihovník	knihovník	k1gMnSc1
Královské	královský	k2eAgFnSc2d1
Akademie	akademie	k1gFnSc2
v	v	k7c6
Turku	turek	k1gInSc6
a	a	k8xC
významná	významný	k2eAgFnSc1d1
postava	postava	k1gFnSc1
finského	finský	k2eAgInSc2d1
humanismu	humanismus	k1gInSc2
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
přezdívaný	přezdívaný	k2eAgMnSc1d1
také	také	k9
jako	jako	k8xC,k8xS
Otec	otec	k1gMnSc1
finského	finský	k2eAgNnSc2d1
dějepisectví	dějepisectví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Psal	psát	k5eAaImAgMnS
studie	studie	k1gFnPc4
o	o	k7c6
finské	finský	k2eAgFnSc6d1
historii	historie	k1gFnSc6
<g/>
,	,	kIx,
mytologie	mytologie	k1gFnSc1
<g/>
,	,	kIx,
poezii	poezie	k1gFnSc6
a	a	k8xC
jiných	jiný	k2eAgFnPc6d1
populárních	populární	k2eAgFnPc6d1
humanitních	humanitní	k2eAgFnPc6d1
vědách	věda	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1776	#num#	k4
<g/>
–	–	k?
<g/>
1778	#num#	k4
vydal	vydat	k5eAaPmAgInS
v	v	k7c6
pěti	pět	k4xCc6
dílech	dílo	k1gNnPc6
studii	studie	k1gFnSc4
o	o	k7c6
finském	finský	k2eAgInSc6d1
folklóru	folklór	k1gInSc6
a	a	k8xC
poezii	poezie	k1gFnSc3
<g/>
,	,	kIx,
De	De	k?
Poësi	Poëse	k1gFnSc4
Fennica	Fennica	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
velký	velký	k2eAgInSc4d1
význam	význam	k1gInSc4
v	v	k7c6
probuzení	probuzení	k1gNnSc6
zájmu	zájem	k1gInSc2
veřejnosti	veřejnost	k1gFnSc2
o	o	k7c4
finskou	finský	k2eAgFnSc4d1
mytologii	mytologie	k1gFnSc4
a	a	k8xC
Kalevalu	Kalevala	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sloužila	sloužit	k5eAaImAgFnS
také	také	k9
jako	jako	k9
základ	základ	k1gInSc4
pro	pro	k7c4
pozdější	pozdní	k2eAgFnPc4d2
studie	studie	k1gFnPc4
poezie	poezie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
také	také	k9
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
zakladatelů	zakladatel	k1gMnPc2
společnosti	společnost	k1gFnSc2
Aurora	Aurora	k1gFnSc1
a	a	k8xC
redaktorem	redaktor	k1gMnSc7
prvních	první	k4xOgFnPc6
finských	finský	k2eAgFnPc2d1
novin	novina	k1gFnPc2
<g/>
,	,	kIx,
Tidningar	Tidningar	k1gInSc1
ugifne	ugifnout	k5eAaPmIp3nS
af	af	k?
et	et	k?
sällskap	sällskap	k1gInSc1
i	i	k8xC
Å	Å	k1gFnSc5
<g/>
,	,	kIx,
založených	založený	k2eAgInPc2d1
v	v	k7c6
roce	rok	k1gInSc6
1771	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Byl	být	k5eAaImAgInS
studentem	student	k1gMnSc7
Daniela	Daniel	k1gMnSc2
Juslenia	Juslenium	k1gNnSc2
<g/>
,	,	kIx,
učitelem	učitel	k1gMnSc7
Franse	Frans	k1gMnSc2
Mikaela	Mikael	k1gMnSc2
Franzéna	Franzén	k1gMnSc2
a	a	k8xC
inspiroval	inspirovat	k5eAaBmAgMnS
následující	následující	k2eAgFnSc4d1
generaci	generace	k1gFnSc4
finských	finský	k2eAgMnPc2d1
autorů	autor	k1gMnPc2
<g/>
,	,	kIx,
básníků	básník	k1gMnPc2
a	a	k8xC
badatelů	badatel	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
většina	většina	k1gFnSc1
patřila	patřit	k5eAaImAgFnS
mezi	mezi	k7c4
zakladatele	zakladatel	k1gMnPc4
Společnosti	společnost	k1gFnSc2
finské	finský	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1831	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Svenskt	Svenskt	k1gInSc1
biografiskt	biografiskt	k1gInSc1
lexikon	lexikon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
118595865	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0856	#num#	k4
583X	583X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
82132055	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
36908769	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
82132055	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Finsko	Finsko	k1gNnSc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
</s>
