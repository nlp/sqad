<s>
Henrik	Henrik	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
Porthan	Porthan	k1gMnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1739	#num#	k4
<g/>
,	,	kIx,
Viitasaari	Viitasaare	k1gFnSc4
–	–	k?
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1804	#num#	k4
<g/>
,	,	kIx,
Turku	Turek	k1gMnSc5
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
profesor	profesor	k1gMnSc1
a	a	k8xC
knihovník	knihovník	k1gMnSc1
Královské	královský	k2eAgFnSc2d1
Akademie	akademie	k1gFnSc2
v	v	k7c6
Turku	turek	k1gInSc6
a	a	k8xC
významná	významný	k2eAgFnSc1d1
postava	postava	k1gFnSc1
finského	finský	k2eAgInSc2d1
humanismu	humanismus	k1gInSc2
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
přezdívaný	přezdívaný	k2eAgMnSc1d1
také	také	k9
jako	jako	k8xC,k8xS
Otec	otec	k1gMnSc1
finského	finský	k2eAgNnSc2d1
dějepisectví	dějepisectví	k1gNnSc2
<g/>
.	.	kIx.
</s>