<p>
<s>
Nikolaus	Nikolaus	k1gInSc1	Nikolaus
von	von	k1gInSc1	von
Wassilko	Wassilka	k1gFnSc5	Wassilka
<g/>
,	,	kIx,	,
rumunsky	rumunsky	k6eAd1	rumunsky
Nicolae	Nicolae	k1gInSc1	Nicolae
de	de	k?	de
Wassilko	Wassilka	k1gFnSc5	Wassilka
<g/>
,	,	kIx,	,
ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
cyrilicí	cyrilice	k1gFnSc7	cyrilice
М	М	k?	М
В	В	k?	В
<g/>
,	,	kIx,	,
Mykola	Mykola	k1gFnSc1	Mykola
Vasylko	Vasylko	k1gNnSc4	Vasylko
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1868	[number]	k4	1868
Lukavci	Lukavec	k1gInPc7	Lukavec
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1924	[number]	k4	1924
Bad	Bad	k1gMnSc1	Bad
Gleichenberg	Gleichenberg	k1gMnSc1	Gleichenberg
nebo	nebo	k8xC	nebo
Bad	Bad	k1gMnSc1	Bad
Reichenhall	Reichenhall	k1gMnSc1	Reichenhall
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
šlechtic	šlechtic	k1gMnSc1	šlechtic
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
rusínské	rusínský	k2eAgFnSc2d1	rusínská
(	(	kIx(	(
<g/>
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
<g/>
)	)	kIx)	)
národnosti	národnost	k1gFnSc2	národnost
z	z	k7c2	z
Bukoviny	Bukovina	k1gFnSc2	Bukovina
<g/>
,	,	kIx,	,
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
poslanec	poslanec	k1gMnSc1	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
vyslanec	vyslanec	k1gMnSc1	vyslanec
Západoukrajinské	Západoukrajinský	k2eAgFnSc2d1	Západoukrajinská
republiky	republika	k1gFnSc2	republika
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Lukavci	Lukavec	k1gMnPc1	Lukavec
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
starosta	starosta	k1gMnSc1	starosta
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
získal	získat	k5eAaPmAgInS	získat
i	i	k9	i
mandát	mandát	k1gInSc1	mandát
poslance	poslanec	k1gMnSc2	poslanec
Bukovinského	bukovinský	k2eAgInSc2d1	bukovinský
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
<g/>
.	.	kIx.	.
<g/>
Byl	být	k5eAaImAgMnS	být
rusínské	rusínský	k2eAgFnPc4d1	rusínská
národnosti	národnost	k1gFnPc4	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Odmítal	odmítat	k5eAaImAgMnS	odmítat
promoskevské	promoskevský	k2eAgNnSc4d1	promoskevské
(	(	kIx(	(
<g/>
rusofilské	rusofilský	k2eAgFnPc4d1	rusofilská
<g/>
)	)	kIx)	)
tendence	tendence	k1gFnPc4	tendence
mezi	mezi	k7c7	mezi
Rusíny	rusín	k1gInPc7	rusín
v	v	k7c6	v
Rakousku-Uhersku	Rakousku-Uhersko	k1gNnSc6	Rakousku-Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
umělkyně	umělkyně	k1gFnSc1	umělkyně
Gerda	Gerdo	k1gNnSc2	Gerdo
Walden	Waldna	k1gFnPc2	Waldna
<g/>
.	.	kIx.	.
<g/>
Byl	být	k5eAaImAgInS	být
i	i	k9	i
poslancem	poslanec	k1gMnSc7	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
celostátního	celostátní	k2eAgInSc2d1	celostátní
parlamentu	parlament	k1gInSc2	parlament
Předlitavska	Předlitavsko	k1gNnSc2	Předlitavsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
usedl	usednout	k5eAaPmAgMnS	usednout
v	v	k7c6	v
doplňovacích	doplňovací	k2eAgFnPc6d1	doplňovací
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
za	za	k7c4	za
kurii	kurie	k1gFnSc4	kurie
venkovských	venkovský	k2eAgFnPc2d1	venkovská
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
Bukovině	Bukovina	k1gFnSc6	Bukovina
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc1	obvod
Vyžnycja	Vyžnycjum	k1gNnSc2	Vyžnycjum
<g/>
,	,	kIx,	,
Kicmaň	Kicmaň	k1gFnSc2	Kicmaň
atd.	atd.	kA	atd.
Nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1899	[number]	k4	1899
místo	místo	k7c2	místo
Vasyla	Vasyla	k1gMnSc2	Vasyla
Voljana	Voljan	k1gMnSc2	Voljan
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
zde	zde	k6eAd1	zde
obhájil	obhájit	k5eAaPmAgInS	obhájit
v	v	k7c6	v
řádných	řádný	k2eAgFnPc6d1	řádná
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Uspěl	uspět	k5eAaPmAgMnS	uspět
také	také	k9	také
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
konaných	konaný	k2eAgMnPc2d1	konaný
podle	podle	k7c2	podle
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
a	a	k8xC	a
rovného	rovný	k2eAgNnSc2d1	rovné
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
Bukovina	Bukovina	k1gFnSc1	Bukovina
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
zde	zde	k6eAd1	zde
obhájil	obhájit	k5eAaPmAgInS	obhájit
i	i	k9	i
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1901	[number]	k4	1901
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
rytíř	rytíř	k1gMnSc1	rytíř
Nikolai	Nikola	k1gFnSc2	Nikola
von	von	k1gInSc1	von
Wassilkó	Wassilkó	k1gFnSc2	Wassilkó
<g/>
,	,	kIx,	,
velkostatkář	velkostatkář	k1gMnSc1	velkostatkář
<g/>
,	,	kIx,	,
zemský	zemský	k2eAgMnSc1d1	zemský
poslanec	poslanec	k1gMnSc1	poslanec
a	a	k8xC	a
obecní	obecní	k2eAgMnSc1d1	obecní
starosta	starosta	k1gMnSc1	starosta
<g/>
,	,	kIx,	,
bytem	byt	k1gInSc7	byt
Černovice	Černovice	k1gFnSc2	Černovice
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k8xS	jako
starorusínský	starorusínský	k2eAgMnSc1d1	starorusínský
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Usedl	usednout	k5eAaPmAgMnS	usednout
pak	pak	k6eAd1	pak
do	do	k7c2	do
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
Rusínského	rusínský	k2eAgInSc2d1	rusínský
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1906	[number]	k4	1906
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
osmi	osm	k4xCc2	osm
členů	člen	k1gInPc2	člen
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
Rusínského	rusínský	k2eAgInSc2d1	rusínský
klubu	klub	k1gInSc2	klub
na	na	k7c6	na
Říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
zasedl	zasednout	k5eAaPmAgMnS	zasednout
do	do	k7c2	do
poslaneckého	poslanecký	k2eAgNnSc2d1	poslanecké
uskupení	uskupení	k1gNnSc2	uskupení
Klub	klub	k1gInSc1	klub
bukovinských	bukovinský	k2eAgInPc2d1	bukovinský
Rusínů	rusín	k1gInPc2	rusín
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
monarchie	monarchie	k1gFnSc2	monarchie
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vyslancem	vyslanec	k1gMnSc7	vyslanec
krátkodobě	krátkodobě	k6eAd1	krátkodobě
existující	existující	k2eAgFnSc2d1	existující
Západoukrajinské	Západoukrajinský	k2eAgFnSc2d1	Západoukrajinská
republiky	republika	k1gFnSc2	republika
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Bernu	Bern	k1gInSc6	Bern
a	a	k8xC	a
pak	pak	k6eAd1	pak
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
<g/>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1924	[number]	k4	1924
v	v	k7c6	v
Bad	Bad	k1gFnSc6	Bad
Gleichenberg	Gleichenberg	k1gInSc4	Gleichenberg
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
zdrojích	zdroj	k1gInPc6	zdroj
uváděno	uvádět	k5eAaImNgNnS	uvádět
v	v	k7c6	v
Bad	Bad	k1gFnSc6	Bad
Reichenhall	Reichenhalla	k1gFnPc2	Reichenhalla
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
smrti	smrt	k1gFnSc2	smrt
byla	být	k5eAaImAgFnS	být
mrtvice	mrtvice	k1gFnSc1	mrtvice
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgMnSc1d1	pohřben
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
