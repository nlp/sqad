<s>
Nikolaus	Nikolaus	k1gInSc1	Nikolaus
von	von	k1gInSc1	von
Wassilko	Wassilka	k1gFnSc5	Wassilka
<g/>
,	,	kIx,	,
rumunsky	rumunsky	k6eAd1	rumunsky
Nicolae	Nicolae	k1gInSc1	Nicolae
de	de	k?	de
Wassilko	Wassilka	k1gFnSc5	Wassilka
<g/>
,	,	kIx,	,
ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
cyrilicí	cyrilice	k1gFnSc7	cyrilice
М	М	k?	М
В	В	k?	В
<g/>
,	,	kIx,	,
Mykola	Mykola	k1gFnSc1	Mykola
Vasylko	Vasylko	k1gNnSc4	Vasylko
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1868	[number]	k4	1868
Lukavci	Lukavec	k1gInPc7	Lukavec
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1924	[number]	k4	1924
Bad	Bad	k1gMnSc1	Bad
Gleichenberg	Gleichenberg	k1gMnSc1	Gleichenberg
nebo	nebo	k8xC	nebo
Bad	Bad	k1gMnSc1	Bad
Reichenhall	Reichenhall	k1gMnSc1	Reichenhall
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
šlechtic	šlechtic	k1gMnSc1	šlechtic
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
rusínské	rusínský	k2eAgFnSc2d1	rusínská
(	(	kIx(	(
<g/>
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
<g/>
)	)	kIx)	)
národnosti	národnost	k1gFnSc2	národnost
z	z	k7c2	z
Bukoviny	Bukovina	k1gFnSc2	Bukovina
<g/>
,	,	kIx,	,
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
poslanec	poslanec	k1gMnSc1	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
vyslanec	vyslanec	k1gMnSc1	vyslanec
Západoukrajinské	Západoukrajinský	k2eAgFnSc2d1	Západoukrajinská
republiky	republika	k1gFnSc2	republika
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
