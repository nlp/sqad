<s>
Okno	okno	k1gNnSc1	okno
je	být	k5eAaImIp3nS	být
výplň	výplň	k1gFnSc4	výplň
stavebního	stavební	k2eAgInSc2d1	stavební
otvoru	otvor	k1gInSc2	otvor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
až	až	k9	až
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
především	především	k9	především
k	k	k7c3	k
prosvětlování	prosvětlování	k1gNnSc3	prosvětlování
místností	místnost	k1gFnPc2	místnost
a	a	k8xC	a
pro	pro	k7c4	pro
přirozený	přirozený	k2eAgInSc4d1	přirozený
kontakt	kontakt	k1gInSc4	kontakt
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
<g/>
.	.	kIx.	.
</s>
