<s>
Okno	okno	k1gNnSc1	okno
je	být	k5eAaImIp3nS	být
výplň	výplň	k1gFnSc4	výplň
stavebního	stavební	k2eAgInSc2d1	stavební
otvoru	otvor	k1gInSc2	otvor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
až	až	k9	až
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
především	především	k9	především
k	k	k7c3	k
prosvětlování	prosvětlování	k1gNnSc3	prosvětlování
místností	místnost	k1gFnPc2	místnost
a	a	k8xC	a
pro	pro	k7c4	pro
přirozený	přirozený	k2eAgInSc4d1	přirozený
kontakt	kontakt	k1gInSc4	kontakt
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Okno	okno	k1gNnSc1	okno
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
skleněné	skleněný	k2eAgFnSc2d1	skleněná
výplně	výplň	k1gFnSc2	výplň
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
z	z	k7c2	z
rámu	rám	k1gInSc2	rám
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
hliníkových	hliníkový	k2eAgFnPc2d1	hliníková
slitin	slitina	k1gFnPc2	slitina
<g/>
,	,	kIx,	,
z	z	k7c2	z
plastů	plast	k1gInPc2	plast
nebo	nebo	k8xC	nebo
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
z	z	k7c2	z
kompozit	kompozitum	k1gNnPc2	kompozitum
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
jsou	být	k5eAaImIp3nP	být
okna	okno	k1gNnPc4	okno
jednoduchá	jednoduchý	k2eAgNnPc4d1	jednoduché
s	s	k7c7	s
dvojitým	dvojitý	k2eAgNnSc7d1	dvojité
zasklením	zasklení	k1gNnSc7	zasklení
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rámu	rám	k1gInSc6	rám
uloženy	uložit	k5eAaPmNgFnP	uložit
dvě	dva	k4xCgFnPc1	dva
vrstvy	vrstva	k1gFnPc1	vrstva
skleněné	skleněný	k2eAgFnPc4d1	skleněná
výplně	výplň	k1gFnPc4	výplň
spojené	spojený	k2eAgFnPc4d1	spojená
rámečkem	rámeček	k1gInSc7	rámeček
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
je	být	k5eAaImIp3nS	být
izolační	izolační	k2eAgFnSc1d1	izolační
vrstva	vrstva	k1gFnSc1	vrstva
inertního	inertní	k2eAgInSc2d1	inertní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
argonu	argon	k1gInSc2	argon
<g/>
,	,	kIx,	,
kryptonu	krypton	k1gInSc2	krypton
nebo	nebo	k8xC	nebo
xenonu	xenon	k1gInSc2	xenon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
výplň	výplň	k1gFnSc1	výplň
okna	okno	k1gNnSc2	okno
tvořena	tvořit	k5eAaImNgFnS	tvořit
jen	jen	k6eAd1	jen
pomaštěným	pomaštěný	k2eAgInSc7d1	pomaštěný
papírem	papír	k1gInSc7	papír
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
okno	okno	k1gNnSc4	okno
výplň	výplň	k1gFnSc1	výplň
vůbec	vůbec	k9	vůbec
nemělo	mít	k5eNaImAgNnS	mít
(	(	kIx(	(
<g/>
mělo	mít	k5eAaImAgNnS	mít
pouze	pouze	k6eAd1	pouze
okenici	okenice	k1gFnSc3	okenice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
okna	okno	k1gNnPc1	okno
katedrál	katedrála	k1gFnPc2	katedrála
byla	být	k5eAaImAgNnP	být
bohatě	bohatě	k6eAd1	bohatě
vykládaná	vykládaný	k2eAgNnPc1d1	vykládané
(	(	kIx(	(
<g/>
vitrážovaná	vitrážovaný	k2eAgFnSc1d1	vitrážovaný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okna	okno	k1gNnPc1	okno
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
celistvá	celistvý	k2eAgFnSc1d1	celistvá
(	(	kIx(	(
<g/>
obdélníkový	obdélníkový	k2eAgInSc4d1	obdélníkový
rám	rám	k1gInSc4	rám
s	s	k7c7	s
jedním	jeden	k4xCgNnSc7	jeden
souvislým	souvislý	k2eAgNnSc7d1	souvislé
sklem	sklo	k1gNnSc7	sklo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dělená	dělený	k2eAgFnSc1d1	dělená
na	na	k7c6	na
části	část	k1gFnSc6	část
(	(	kIx(	(
<g/>
okenní	okenní	k2eAgFnPc1d1	okenní
tabule	tabule	k1gFnPc1	tabule
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sdružené	sdružený	k2eAgNnSc1d1	sdružené
okno	okno	k1gNnSc1	okno
je	být	k5eAaImIp3nS	být
okno	okno	k1gNnSc1	okno
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
otvory	otvor	k1gInPc1	otvor
s	s	k7c7	s
mezilehlými	mezilehlý	k2eAgInPc7d1	mezilehlý
sloupky	sloupek	k1gInPc7	sloupek
a	a	k8xC	a
stojkami	stojka	k1gFnPc7	stojka
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
případem	případ	k1gInSc7	případ
okna	okno	k1gNnSc2	okno
jsou	být	k5eAaImIp3nP	být
střešní	střešní	k2eAgNnPc4d1	střešní
okna	okno	k1gNnPc4	okno
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgMnS	spojit
dánský	dánský	k2eAgMnSc1d1	dánský
inženýr	inženýr	k1gMnSc1	inženýr
Villum	Villum	k1gNnSc1	Villum
Kann	Kanny	k1gFnPc2	Kanny
Rasmussen	Rasmussna	k1gFnPc2	Rasmussna
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
a	a	k8xC	a
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
první	první	k4xOgFnSc4	první
moderní	moderní	k2eAgFnSc4d1	moderní
střešní	střešní	k2eAgNnSc4d1	střešní
okno	okno	k1gNnSc4	okno
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
umožnil	umožnit	k5eAaPmAgMnS	umožnit
budoucí	budoucí	k2eAgMnSc1d1	budoucí
optimální	optimální	k2eAgNnSc4d1	optimální
využití	využití	k1gNnSc4	využití
prostoru	prostor	k1gInSc2	prostor
pod	pod	k7c7	pod
šikmou	šikmý	k2eAgFnSc7d1	šikmá
střechou	střecha	k1gFnSc7	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zakladatelem	zakladatel	k1gMnSc7	zakladatel
společnosti	společnost	k1gFnSc2	společnost
Velux	Velux	k1gInSc1	Velux
<g/>
.	.	kIx.	.
</s>
<s>
Okno	okno	k1gNnSc1	okno
do	do	k7c2	do
ploché	plochý	k2eAgFnSc2d1	plochá
střechy	střecha	k1gFnSc2	střecha
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
prosvětlení	prosvětlení	k1gNnSc1	prosvětlení
interiéru	interiér	k1gInSc2	interiér
pod	pod	k7c7	pod
rovnou	rovnou	k6eAd1	rovnou
(	(	kIx(	(
<g/>
ne	ne	k9	ne
sedlovou	sedlový	k2eAgFnSc7d1	sedlová
<g/>
)	)	kIx)	)
střechou	střecha	k1gFnSc7	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Okno	okno	k1gNnSc1	okno
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
otevíravé	otevíravý	k2eAgNnSc1d1	otevíravé
(	(	kIx(	(
<g/>
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
použít	použít	k5eAaPmF	použít
dálkové	dálkový	k2eAgNnSc4d1	dálkové
ovládání	ovládání	k1gNnSc4	ovládání
na	na	k7c4	na
elektrický	elektrický	k2eAgInSc4d1	elektrický
pohon	pohon	k1gInSc4	pohon
pro	pro	k7c4	pro
nedostupnost	nedostupnost	k1gFnSc4	nedostupnost
okna	okno	k1gNnSc2	okno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
neotvíravé	otvíravý	k2eNgFnPc1d1	neotvíravá
<g/>
.	.	kIx.	.
</s>
<s>
Okno	okno	k1gNnSc1	okno
do	do	k7c2	do
ploché	plochý	k2eAgFnSc2d1	plochá
střechy	střecha	k1gFnSc2	střecha
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zastínit	zastínit	k5eAaPmF	zastínit
roletkou	roletka	k1gFnSc7	roletka
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ostrého	ostrý	k2eAgNnSc2d1	ostré
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
plastová	plastový	k2eAgFnSc1d1	plastová
dřevo-hliníková	dřevoliníková	k1gFnSc1	dřevo-hliníková
ocelová	ocelový	k2eAgFnSc1d1	ocelová
hliníková	hliníkový	k2eAgFnSc1d1	hliníková
bezrámová	bezrámová	k1gFnSc1	bezrámová
otevíravá	otevíravý	k2eAgFnSc1d1	otevíravá
sklápěcí	sklápěcí	k2eAgFnSc1d1	sklápěcí
otevíravá	otevíravý	k2eAgFnSc1d1	otevíravá
a	a	k8xC	a
sklápěcí	sklápěcí	k2eAgFnSc1d1	sklápěcí
vyklápěcí	vyklápěcí	k2eAgFnSc1d1	vyklápěcí
posuvná	posuvný	k2eAgFnSc1d1	posuvná
výsuvná	výsuvný	k2eAgFnSc1d1	výsuvná
kyvná	kyvný	k2eAgFnSc1d1	kyvná
pevná	pevný	k2eAgFnSc1d1	pevná
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
dvojitě	dvojitě	k6eAd1	dvojitě
zasklená	zasklený	k2eAgNnPc1d1	zasklené
okna	okno	k1gNnPc1	okno
se	s	k7c7	s
sdruženými	sdružený	k2eAgNnPc7d1	sdružené
křídly	křídlo	k1gNnPc7	křídlo
zdvojená	zdvojený	k2eAgFnSc1d1	zdvojená
dvojitá	dvojitý	k2eAgFnSc1d1	dvojitá
-	-	kIx~	-
špaletová	špaletový	k2eAgFnSc1d1	špaletová
či	či	k8xC	či
kastlová	kastlový	k2eAgFnSc1d1	kastlová
Součástí	součást	k1gFnSc7	součást
okna	okno	k1gNnSc2	okno
bývají	bývat	k5eAaImIp3nP	bývat
rovněž	rovněž	k9	rovněž
okenice	okenice	k1gFnPc1	okenice
(	(	kIx(	(
<g/>
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
či	či	k8xC	či
kovové	kovový	k2eAgFnPc1d1	kovová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatahovací	zatahovací	k2eAgFnSc2d1	zatahovací
žaluzie	žaluzie	k1gFnSc2	žaluzie
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgInSc4d1	funkční
celek	celek	k1gInSc4	celek
mohou	moct	k5eAaImIp3nP	moct
dále	daleko	k6eAd2	daleko
tvořit	tvořit	k5eAaImF	tvořit
v	v	k7c6	v
exteriéru	exteriér	k1gInSc6	exteriér
markýza	markýza	k1gFnSc1	markýza
<g/>
,	,	kIx,	,
slunolam	slunolam	k1gInSc1	slunolam
<g/>
,	,	kIx,	,
bezpečnostní	bezpečnostní	k2eAgFnPc1d1	bezpečnostní
mříže	mříž	k1gFnPc1	mříž
<g/>
,	,	kIx,	,
parapet	parapet	k1gInSc1	parapet
<g/>
,	,	kIx,	,
fasádní	fasádní	k2eAgFnPc1d1	fasádní
dekorace	dekorace	k1gFnPc1	dekorace
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
zábradlí	zábradlí	k1gNnSc1	zábradlí
nebo	nebo	k8xC	nebo
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
závěsy	závěsa	k1gFnSc2	závěsa
<g/>
,	,	kIx,	,
parapet	parapet	k1gInSc1	parapet
<g/>
,	,	kIx,	,
otopné	otopný	k2eAgNnSc1d1	otopné
těleso	těleso	k1gNnSc1	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
základní	základní	k2eAgFnPc4d1	základní
charakteristiky	charakteristika	k1gFnPc4	charakteristika
okna	okno	k1gNnSc2	okno
patří	patřit	k5eAaImIp3nP	patřit
součinitel	součinitel	k1gInSc4	součinitel
prostupu	prostup	k1gInSc2	prostup
tepla	teplo	k1gNnSc2	teplo
(	(	kIx(	(
<g/>
Uw	Uw	k1gFnSc1	Uw
<g/>
,	,	kIx,	,
Uwindow	Uwindow	k1gFnSc1	Uwindow
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
w	w	k?	w
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
⋅	⋅	k?	⋅
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
I	i	k9	i
g	g	kA	g
⋅	⋅	k?	⋅
Ψ	Ψ	k?	Ψ
g	g	kA	g
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
[	[	kIx(	[
W	W	kA	W
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
∗	∗	k?	∗
K	K	kA	K
)	)	kIx)	)
]	]	kIx)	]
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
U_	U_	k1gFnSc6	U_
<g/>
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
A_	A_	k1gMnSc1	A_
<g/>
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
U_	U_	k1gMnSc1	U_
<g/>
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
A_	A_	k1gFnSc1	A_
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
U_	U_	k1gMnSc1	U_
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
Ig	Ig	k1gFnSc1	Ig
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
g	g	kA	g
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
A_	A_	k1gMnSc1	A_
<g/>
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
A_	A_	k1gFnSc1	A_
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
\	\	kIx~	\
\	\	kIx~	\
[	[	kIx(	[
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
W	W	kA	W
<g/>
/	/	kIx~	/
<g/>
(	(	kIx(	(
<g/>
m	m	kA	m
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
*	*	kIx~	*
<g/>
K	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
<g/>
,	,	kIx,	,
Ag	Ag	k1gFnSc1	Ag
[	[	kIx(	[
<g/>
m2	m2	k4	m2
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
zasklená	zasklený	k2eAgFnSc1d1	zasklená
plocha	plocha	k1gFnSc1	plocha
<g/>
,	,	kIx,	,
Af	Af	k1gFnSc1	Af
[	[	kIx(	[
<g/>
m2	m2	k4	m2
<g/>
]	]	kIx)	]
plocha	plocha	k1gFnSc1	plocha
rámu	rám	k1gInSc2	rám
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
Ig	Ig	k1gFnSc1	Ig
obvod	obvod	k1gInSc1	obvod
viditelného	viditelný	k2eAgNnSc2d1	viditelné
zasklení	zasklení	k1gNnSc2	zasklení
[	[	kIx(	[
<g/>
m	m	kA	m
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Ug	Ug	k1gFnPc7	Ug
a	a	k8xC	a
Uf	uf	k0	uf
[	[	kIx(	[
<g/>
W	W	kA	W
<g/>
/	/	kIx~	/
<g/>
(	(	kIx(	(
<g/>
m2	m2	k4	m2
<g/>
×	×	k?	×
<g/>
K	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
součinitel	součinitel	k1gInSc1	součinitel
prostupu	prostup	k1gInSc2	prostup
tepla	teplo	k1gNnSc2	teplo
zasklením	zasklenit	k5eAaPmIp1nS	zasklenit
a	a	k8xC	a
rámem	rám	k1gInSc7	rám
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Ψ	Ψ	k?	Ψ
g	g	kA	g
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Psi	pes	k1gMnPc1	pes
g	g	kA	g
<g/>
}	}	kIx)	}
[	[	kIx(	[
<g/>
W	W	kA	W
<g/>
/	/	kIx~	/
<g/>
(	(	kIx(	(
<g/>
m	m	kA	m
<g/>
×	×	k?	×
<g/>
K	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
lineární	lineární	k2eAgMnSc1d1	lineární
činitel	činitel	k1gMnSc1	činitel
prostupu	prostup	k1gInSc2	prostup
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušený	zjednodušený	k2eAgInSc1d1	zjednodušený
výpočet	výpočet	k1gInSc1	výpočet
Uw	Uw	k1gFnSc2	Uw
lze	lze	k6eAd1	lze
provést	provést	k5eAaPmF	provést
pro	pro	k7c4	pro
novodobé	novodobý	k2eAgNnSc4d1	novodobé
běžné	běžný	k2eAgNnSc4d1	běžné
okno	okno	k1gNnSc4	okno
1	[number]	k4	1
<g/>
m	m	kA	m
x	x	k?	x
1	[number]	k4	1
<g/>
m	m	kA	m
jako	jako	k8xS	jako
Uf	uf	k0	uf
z	z	k7c2	z
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Ug	Ug	k1gFnSc1	Ug
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
sklo	sklo	k1gNnSc1	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Přirážka	přirážka	k1gFnSc1	přirážka
5	[number]	k4	5
<g/>
%	%	kIx~	%
za	za	k7c4	za
tepelný	tepelný	k2eAgInSc4d1	tepelný
most	most	k1gInSc4	most
mezi	mezi	k7c7	mezi
sklem	sklo	k1gNnSc7	sklo
a	a	k8xC	a
rámečkem	rámeček	k1gInSc7	rámeček
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
w	w	k?	w
,	,	kIx,	,
o	o	k7c6	o
d	d	k?	d
h	h	k?	h
a	a	k8xC	a
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
⋅	⋅	k?	⋅
1	[number]	k4	1
,	,	kIx,	,
05	[number]	k4	05
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
U_	U_	k1gFnSc6	U_
<g/>
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
,	,	kIx,	,
<g/>
odhad	odhad	k1gInSc1	odhad
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
U_	U_	k1gFnPc2	U_
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
U_	U_	k1gFnPc2	U_
<g/>
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
1,05	[number]	k4	1,05
<g/>
}	}	kIx)	}
:	:	kIx,	:
Dalším	další	k2eAgInSc7d1	další
důležitou	důležitý	k2eAgFnSc7d1	důležitá
hodnotou	hodnota	k1gFnSc7	hodnota
je	být	k5eAaImIp3nS	být
g	g	kA	g
[	[	kIx(	[
<g/>
%	%	kIx~	%
<g/>
]	]	kIx)	]
vyjadřující	vyjadřující	k2eAgNnSc1d1	vyjadřující
množství	množství	k1gNnSc1	množství
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
(	(	kIx(	(
<g/>
viditelné	viditelný	k2eAgFnSc2d1	viditelná
<g/>
,	,	kIx,	,
infračervené	infračervený	k2eAgFnSc2d1	infračervená
<g/>
,	,	kIx,	,
tepelné	tepelný	k2eAgFnSc2d1	tepelná
<g/>
)	)	kIx)	)
procházející	procházející	k2eAgNnSc1d1	procházející
zasklením	zasklení	k1gNnSc7	zasklení
<g/>
.	.	kIx.	.
</s>
<s>
Pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
0,5	[number]	k4	0,5
<g/>
-	-	kIx~	-
<g/>
0,8	[number]	k4	0,8
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
klesá	klesat	k5eAaImIp3nS	klesat
se	s	k7c7	s
snižujícím	snižující	k2eAgMnSc7d1	snižující
se	se	k3xPyFc4	se
hodnotou	hodnota	k1gFnSc7	hodnota
Uw	Uw	k1gFnPc1	Uw
<g/>
.	.	kIx.	.
</s>
