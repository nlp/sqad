<s>
Viktor	Viktor	k1gMnSc1
Frankl	Frankl	k1gMnSc1
</s>
<s>
Viktor	Viktor	k1gMnSc1
Frankl	Frankl	k1gMnSc2
Narození	narození	k1gNnSc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1905	#num#	k4
<g/>
Vídeň	Vídeň	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1997	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
92	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Vídeň	Vídeň	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Starý	starý	k2eAgInSc4d1
židovský	židovský	k2eAgInSc4d1
hřbitov	hřbitov	k1gInSc4
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
chirurg	chirurg	k1gMnSc1
<g/>
,	,	kIx,
psychoterapeut	psychoterapeut	k1gMnSc1
<g/>
,	,	kIx,
psycholog	psycholog	k1gMnSc1
<g/>
,	,	kIx,
psychiatr	psychiatr	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
<g/>
,	,	kIx,
neurolog	neurolog	k1gMnSc1
<g/>
,	,	kIx,
existential	existential	k1gMnSc1
therapist	therapist	k1gMnSc1
a	a	k8xC
spisovatel	spisovatel	k1gMnSc1
Zaměstnavatel	zaměstnavatel	k1gMnSc1
</s>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
City	City	k1gFnSc1
of	of	k?
Vienna	Vienen	k2eAgFnSc1d1
Prize	Prize	k1gFnSc1
for	forum	k1gNnPc2
science	science	k1gFnSc2
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
Čestný	čestný	k2eAgInSc1d1
odznak	odznak	k1gInSc1
Za	za	k7c4
vědu	věda	k1gFnSc4
a	a	k8xC
umění	umění	k1gNnSc4
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
<g/>
The	The	k1gMnSc1
Oskar	Oskar	k1gMnSc1
Pfister	Pfister	k1gMnSc1
Award	Award	k1gMnSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
<g/>
Zlatá	zlatý	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
MU	MU	kA
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
Čestné	čestný	k2eAgNnSc1d1
občanství	občanství	k1gNnSc1
města	město	k1gNnSc2
Vídně	Vídeň	k1gFnSc2
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Nábož	Nábož	k1gFnSc4
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc4
</s>
<s>
judaismus	judaismus	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Viktor	Viktor	k1gMnSc1
Emil	Emil	k1gMnSc1
Frankl	Frankl	k1gMnSc1
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1905	#num#	k4
Vídeň	Vídeň	k1gFnSc1
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1997	#num#	k4
Vídeň	Vídeň	k1gFnSc4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
rakouský	rakouský	k2eAgMnSc1d1
neurolog	neurolog	k1gMnSc1
a	a	k8xC
psychiatr	psychiatr	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
existenciální	existenciální	k2eAgFnSc2d1
analýzy	analýza	k1gFnSc2
a	a	k8xC
logoterapie	logoterapie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Viktor	Viktor	k1gMnSc1
Frankl	Frankl	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
v	v	k7c6
židovské	židovský	k2eAgFnSc6d1
rodině	rodina	k1gFnSc6
krátce	krátce	k6eAd1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
rodiče	rodič	k1gMnSc2
do	do	k7c2
Vídně	Vídeň	k1gFnSc2
přistěhovali	přistěhovat	k5eAaPmAgMnP
z	z	k7c2
Pohořelic	Pohořelice	k1gFnPc2
u	u	k7c2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc4
otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
státní	státní	k2eAgMnSc1d1
úředník	úředník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS
medicínu	medicína	k1gFnSc4
na	na	k7c6
Vídeňské	vídeňský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
,	,	kIx,
během	během	k7c2
studia	studio	k1gNnSc2
se	se	k3xPyFc4
specializoval	specializovat	k5eAaBmAgMnS
na	na	k7c6
neurologii	neurologie	k1gFnSc6
a	a	k8xC
psychiatrii	psychiatrie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
nadšení	nadšení	k1gNnSc2
ze	z	k7c2
Sigmunda	Sigmund	k1gMnSc2
Freuda	Freud	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnSc3
redukce	redukce	k1gFnSc1
člověka	člověk	k1gMnSc2
na	na	k7c4
bytost	bytost	k1gFnSc4
ovlivňovanou	ovlivňovaný	k2eAgFnSc4d1
více	hodně	k6eAd2
či	či	k8xC
méně	málo	k6eAd2
skrytými	skrytý	k2eAgInPc7d1
sexuálními	sexuální	k2eAgInPc7d1
impulzy	impulz	k1gInPc7
„	„	k?
<g/>
přešel	přejít	k5eAaPmAgMnS
<g/>
“	“	k?
ke	k	k7c3
škole	škola	k1gFnSc3
Freudova	Freudův	k2eAgMnSc2d1
oponenta	oponent	k1gMnSc2
Alfreda	Alfred	k1gMnSc2
Adlera	Adler	k1gMnSc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
ani	ani	k8xC
s	s	k7c7
jeho	jeho	k3xOp3gInSc7
výkladem	výklad	k1gInSc7
člověka	člověk	k1gMnSc2
nebyl	být	k5eNaImAgMnS
spokojen	spokojen	k2eAgMnSc1d1
dlouho	dlouho	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
začal	začít	k5eAaPmAgInS
po	po	k7c6
skončení	skončení	k1gNnSc6
studia	studio	k1gNnSc2
pracovat	pracovat	k5eAaImF
jako	jako	k9
psychiatr	psychiatr	k1gMnSc1
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
a	a	k8xC
pomáhal	pomáhat	k5eAaImAgMnS
budovat	budovat	k5eAaImF
síť	síť	k1gFnSc4
poraden	poradna	k1gFnPc2
po	po	k7c6
celém	celý	k2eAgNnSc6d1
Rakousku	Rakousko	k1gNnSc6
zachváceném	zachvácený	k2eAgNnSc6d1
krizí	krize	k1gFnSc7
<g/>
,	,	kIx,
začal	začít	k5eAaPmAgInS
přicházet	přicházet	k5eAaImF
k	k	k7c3
názoru	názor	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
terapii	terapie	k1gFnSc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
vést	vést	k5eAaImF
do	do	k7c2
hlubších	hluboký	k2eAgFnPc2d2
a	a	k8xC
duchovnějších	duchovní	k2eAgFnPc2d2
sfér	sféra	k1gFnPc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
<g/>
-li	-li	k?
být	být	k5eAaImF
běžnému	běžný	k2eAgMnSc3d1
člověku	člověk	k1gMnSc3
se	s	k7c7
všemi	všecek	k3xTgInPc7
jeho	jeho	k3xOp3gInPc7
starostmi	starost	k1gFnPc7
užitečná	užitečný	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
přestal	přestat	k5eAaPmAgInS
poslouchat	poslouchat	k5eAaImF
své	svůj	k3xOyFgMnPc4
učitele	učitel	k1gMnPc4
a	a	k8xC
naučil	naučit	k5eAaPmAgMnS
se	se	k3xPyFc4
naslouchat	naslouchat	k5eAaImF
svým	svůj	k3xOyFgMnPc3
pacientům	pacient	k1gMnPc3
a	a	k8xC
učit	učit	k5eAaImF
se	se	k3xPyFc4
od	od	k7c2
nich	on	k3xPp3gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Pracoval	pracovat	k5eAaImAgInS
také	také	k9
v	v	k7c6
poradně	poradna	k1gFnSc6
pro	pro	k7c4
mládež	mládež	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
mj.	mj.	kA
zabýval	zabývat	k5eAaImAgInS
problémem	problém	k1gInSc7
sebevražd	sebevražda	k1gFnPc2
při	při	k7c6
vydávání	vydávání	k1gNnSc6
vysvědčení	vysvědčení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1937	#num#	k4
si	se	k3xPyFc3
založil	založit	k5eAaPmAgMnS
soukromou	soukromý	k2eAgFnSc4d1
praxi	praxe	k1gFnSc4
<g/>
,	,	kIx,
specializovanou	specializovaný	k2eAgFnSc4d1
na	na	k7c6
neurologii	neurologie	k1gFnSc6
a	a	k8xC
psychiatrii	psychiatrie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
koncentrační	koncentrační	k2eAgInPc4d1
tábory	tábor	k1gInPc4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
Rakousko	Rakousko	k1gNnSc4
obsadili	obsadit	k5eAaPmAgMnP
Němci	Němec	k1gMnPc1
<g/>
,	,	kIx,
musel	muset	k5eAaImAgMnS
kvůli	kvůli	k7c3
židovskému	židovský	k2eAgInSc3d1
původu	původ	k1gInSc3
přestat	přestat	k5eAaPmF
léčit	léčit	k5eAaImF
„	„	k?
<g/>
árijské	árijský	k2eAgNnSc1d1
<g/>
"	"	kIx"
pacienty	pacient	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
mohl	moct	k5eAaImAgMnS
mladý	mladý	k2eAgMnSc1d1
Frankl	Frankl	k1gMnSc1
uprchnout	uprchnout	k5eAaPmF
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
,	,	kIx,
zůstal	zůstat	k5eAaPmAgMnS
kvůli	kvůli	k7c3
svým	svůj	k3xOyFgMnPc3
blízkým	blízký	k2eAgMnPc3d1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
povolení	povolení	k1gNnSc4
vycestovat	vycestovat	k5eAaPmF
nedostali	dostat	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
začal	začít	k5eAaPmAgMnS
pracovat	pracovat	k5eAaImF
v	v	k7c6
Rothschild	Rothschild	k1gMnSc1
Hospital	Hospital	k1gMnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
vedl	vést	k5eAaImAgMnS
neurologické	urologický	k2eNgNnSc4d1,k2eAgNnSc4d1
oddělení	oddělení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
jedinou	jediný	k2eAgFnSc4d1
nemocnici	nemocnice	k1gFnSc4
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yIgFnSc6,k3yQgFnSc6
tehdy	tehdy	k6eAd1
byli	být	k5eAaImAgMnP
ještě	ještě	k6eAd1
přijímání	přijímání	k1gNnSc4
i	i	k8xC
Židé	Žid	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
lékařské	lékařský	k2eAgFnSc2d1
diagnózy	diagnóza	k1gFnSc2
v	v	k7c6
několika	několik	k4yIc6
případech	případ	k1gInPc6
zachránily	zachránit	k5eAaPmAgFnP
nemocné	nemocná	k1gFnPc1
před	před	k7c7
eutanazií	eutanazie	k1gFnSc7
podle	podle	k7c2
nacistického	nacistický	k2eAgInSc2d1
programu	program	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
1941	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Tilly	Tillo	k1gNnPc7
Grosser	Grosser	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
byl	být	k5eAaImAgInS
odeslán	odeslat	k5eAaPmNgInS
do	do	k7c2
koncentračního	koncentrační	k2eAgInSc2d1
tábora	tábor	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
průběhu	průběh	k1gInSc6
války	válka	k1gFnSc2
byl	být	k5eAaImAgInS
vězněn	věznit	k5eAaImNgMnS
v	v	k7c6
Terezíně	Terezín	k1gInSc6
<g/>
,	,	kIx,
Osvětimi	Osvětim	k1gFnSc6
a	a	k8xC
Türkheimu	Türkheimo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c2
pobytu	pobyt	k1gInSc2
v	v	k7c6
koncentračních	koncentrační	k2eAgInPc6d1
táborech	tábor	k1gInPc6
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
začaly	začít	k5eAaPmAgFnP
rýsovat	rýsovat	k5eAaImF
hlavní	hlavní	k2eAgFnPc4d1
myšlenky	myšlenka	k1gFnPc4
logoterapie	logoterapie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organizoval	organizovat	k5eAaBmAgInS
tajná	tajný	k2eAgNnPc4d1
setkání	setkání	k1gNnPc4
a	a	k8xC
přesvědčoval	přesvědčovat	k5eAaImAgMnS
spoluvězně	spoluvězeň	k1gMnSc4
<g/>
,	,	kIx,
že	že	k8xS
budou	být	k5eAaImBp3nP
<g/>
-li	-li	k?
mít	mít	k5eAaImF
pro	pro	k7c4
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
žít	žít	k5eAaImF
<g/>
,	,	kIx,
přežijí	přežít	k5eAaPmIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
On	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
myslíval	myslívat	k5eAaImAgMnS
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
odbornou	odborný	k2eAgFnSc4d1
práci	práce	k1gFnSc4
a	a	k8xC
promýšlel	promýšlet	k5eAaImAgMnS
rukopis	rukopis	k1gInSc4
své	svůj	k3xOyFgFnSc2
knihy	kniha	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
po	po	k7c4
osvobození	osvobození	k1gNnSc4
</s>
<s>
Když	když	k8xS
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
z	z	k7c2
koncentračních	koncentrační	k2eAgInPc2d1
táborů	tábor	k1gInPc2
<g/>
,	,	kIx,
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nejbližší	blízký	k2eAgMnPc1d3
členové	člen	k1gMnPc1
jeho	jeho	k3xOp3gFnSc2
rodiny	rodina	k1gFnSc2
válku	válka	k1gFnSc4
nepřežili	přežít	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc4
otec	otec	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
Terezíně	Terezín	k1gInSc6
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
matka	matka	k1gFnSc1
a	a	k8xC
bratr	bratr	k1gMnSc1
v	v	k7c6
Osvětimi	Osvětim	k1gFnSc6
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
žena	žena	k1gFnSc1
zahynula	zahynout	k5eAaPmAgFnS
v	v	k7c6
táboře	tábor	k1gInSc6
Bergen-Belsen	Bergen-Belsen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Holokaust	holokaust	k1gInSc4
tak	tak	k9
z	z	k7c2
jeho	jeho	k3xOp3gMnPc2
nejbližších	blízký	k2eAgMnPc2d3
přežila	přežít	k5eAaPmAgFnS
jen	jen	k6eAd1
jeho	jeho	k3xOp3gFnSc1
sestra	sestra	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zachránila	zachránit	k5eAaPmAgFnS
emigrací	emigrace	k1gFnSc7
do	do	k7c2
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ještě	ještě	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
sepsal	sepsat	k5eAaPmAgMnS
knihu	kniha	k1gFnSc4
A	a	k9
přesto	přesto	k6eAd1
říci	říct	k5eAaPmF
životu	život	k1gInSc3
ano	ano	k9
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
začal	začít	k5eAaPmAgMnS
vést	vést	k5eAaImF
neurologickou	urologický	k2eNgFnSc4d1,k2eAgFnSc4d1
kliniku	klinika	k1gFnSc4
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1971	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
si	se	k3xPyFc3
vzal	vzít	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
druhou	druhý	k4xOgFnSc4
ženu	žena	k1gFnSc4
<g/>
,	,	kIx,
Eleonoru	Eleonora	k1gFnSc4
Katharinu	Katharin	k1gInSc2
Schwindt	Schwindtum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
katolička	katolička	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
manželé	manžel	k1gMnPc1
chodili	chodit	k5eAaImAgMnP
jak	jak	k6eAd1
do	do	k7c2
kostela	kostel	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
do	do	k7c2
synagogy	synagoga	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
slavili	slavit	k5eAaImAgMnP
jak	jak	k8xS,k8xC
Vánoce	Vánoce	k1gFnPc1
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
chanuku	chanuka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měli	mít	k5eAaImAgMnP
spolu	spolu	k6eAd1
dceru	dcera	k1gFnSc4
Gabrielu	Gabriela	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
dětskou	dětský	k2eAgFnSc7d1
psycholožkou	psycholožka	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1955	#num#	k4
získal	získat	k5eAaPmAgMnS
titul	titul	k1gInSc4
profesora	profesor	k1gMnSc2
neurologie	neurologie	k1gFnSc2
a	a	k8xC
psychiatrie	psychiatrie	k1gFnSc2
na	na	k7c6
vídeňské	vídeňský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založil	založit	k5eAaPmAgMnS
také	také	k9
Rakouskou	rakouský	k2eAgFnSc4d1
lékařskou	lékařský	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
pro	pro	k7c4
psychoterapii	psychoterapie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
hostujícím	hostující	k2eAgMnSc7d1
profesorem	profesor	k1gMnSc7
na	na	k7c6
Harvardově	Harvardův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
přednášel	přednášet	k5eAaImAgMnS
a	a	k8xC
vedl	vést	k5eAaImAgMnS
semináře	seminář	k1gInPc4
na	na	k7c6
řadě	řada	k1gFnSc6
dalších	další	k2eAgFnPc2d1
univerzit	univerzita	k1gFnPc2
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdržel	obdržet	k5eAaPmAgMnS
29	#num#	k4
čestných	čestný	k2eAgMnPc2d1
doktorátů	doktorát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Publikoval	publikovat	k5eAaBmAgMnS
39	#num#	k4
knih	kniha	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
přeloženy	přeložit	k5eAaPmNgFnP
do	do	k7c2
40	#num#	k4
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc1
američtí	americký	k2eAgMnPc1d1
příznivci	příznivec	k1gMnPc1
koncem	koncem	k7c2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
založili	založit	k5eAaPmAgMnP
v	v	k7c4
Berkeley	Berkelea	k1gFnPc4
Logoterapeutický	Logoterapeutický	k2eAgInSc1d1
institut	institut	k1gInSc1
(	(	kIx(
<g/>
Viktor	Viktor	k1gMnSc1
Frankl	Frankl	k1gMnSc1
Institute	institut	k1gInSc5
of	of	k?
Logotherapy	Logotherap	k1gMnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c4
srdeční	srdeční	k2eAgNnPc4d1
selhání	selhání	k1gNnPc4
ve	v	k7c6
věku	věk	k1gInSc6
92	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Dát	dát	k5eAaPmF
životu	život	k1gInSc3
smysl	smysl	k1gInSc4
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Logoterapie	logoterapie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Viktor	Viktor	k1gMnSc1
Frankl	Frankl	k1gMnSc1
vystihl	vystihnout	k5eAaPmAgMnS
lidskost	lidskost	k1gFnSc4
člověka	člověk	k1gMnSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
logoterapie	logoterapie	k1gFnSc1
vidí	vidět	k5eAaImIp3nS
v	v	k7c6
člověku	člověk	k1gMnSc6
bytost	bytost	k1gFnSc4
hledající	hledající	k2eAgInSc4d1
smysl	smysl	k1gInSc4
a	a	k8xC
reaguje	reagovat	k5eAaBmIp3nS
na	na	k7c4
volání	volání	k1gNnSc4
po	po	k7c6
smyslu	smysl	k1gInSc6
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
dnes	dnes	k6eAd1
nezaslechnuto	zaslechnut	k2eNgNnSc1d1
zanikne	zaniknout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Jsou	být	k5eAaImIp3nP
tři	tři	k4xCgFnPc4
možnosti	možnost	k1gFnPc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
dát	dát	k5eAaPmF
životu	život	k1gInSc3
smysl	smysl	k1gInSc4
<g/>
:	:	kIx,
</s>
<s>
vykonáním	vykonání	k1gNnSc7
činu	čin	k1gInSc2
-	-	kIx~
smysluplné	smysluplný	k2eAgNnSc1d1
lidské	lidský	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
je	být	k5eAaImIp3nS
zacíleno	zacílit	k5eAaPmNgNnS
za	za	k7c2
hranice	hranice	k1gFnSc2
vlastního	vlastní	k2eAgNnSc2d1
já	já	k3xPp1nSc1
<g/>
;	;	kIx,
nevyžaduje	vyžadovat	k5eNaImIp3nS
přitom	přitom	k6eAd1
hrdinské	hrdinský	k2eAgInPc1d1
činy	čin	k1gInPc1
-	-	kIx~
každý	každý	k3xTgInSc1
čin	čin	k1gInSc1
vykonaný	vykonaný	k2eAgInSc1d1
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c6
druhé	druhý	k4xOgFnSc6
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
sebepřesahujícím	sebepřesahující	k2eAgMnPc3d1
<g/>
,	,	kIx,
lidským	lidský	k2eAgMnPc3d1
a	a	k8xC
smysluplným	smysluplný	k2eAgMnPc3d1
</s>
<s>
prožitím	prožití	k1gNnSc7
zážitku	zážitek	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
obohacuje	obohacovat	k5eAaImIp3nS
a	a	k8xC
povznáší	povznášet	k5eAaImIp3nS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
nejvyšší	vysoký	k2eAgFnSc7d3
hodnotou	hodnota	k1gFnSc7
je	být	k5eAaImIp3nS
láska	láska	k1gFnSc1
</s>
<s>
utrpením	utrpení	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
každého	každý	k3xTgInSc2
života	život	k1gInSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
v	v	k7c6
beznadějné	beznadějný	k2eAgFnSc6d1
situaci	situace	k1gFnSc6
(	(	kIx(
<g/>
např.	např.	kA
neoperativní	operativní	k2eNgInSc1d1
karcinom	karcinom	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
není	být	k5eNaImIp3nS
úniku	únik	k1gInSc2
<g/>
,	,	kIx,
vidí	vidět	k5eAaImIp3nS
za	za	k7c2
jistých	jistý	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
smysl	smysl	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Franklovi	Frankl	k1gMnSc3
je	být	k5eAaImIp3nS
dále	daleko	k6eAd2
připisován	připisován	k2eAgInSc1d1
termín	termín	k1gInSc1
nedělní	nedělní	k2eAgFnSc1d1
neuróza	neuróza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
formu	forma	k1gFnSc4
úzkosti	úzkost	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
výsledkem	výsledek	k1gInSc7
vědomí	vědomí	k1gNnSc2
životní	životní	k2eAgFnSc2d1
prázdnoty	prázdnota	k1gFnSc2
po	po	k7c6
skončení	skončení	k1gNnSc6
pracovního	pracovní	k2eAgInSc2d1
týdne	týden	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Demonstruje	demonstrovat	k5eAaBmIp3nS
se	se	k3xPyFc4
pocitem	pocit	k1gInSc7
nespokojenosti	nespokojenost	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
pramení	pramenit	k5eAaImIp3nS
z	z	k7c2
existenčního	existenční	k2eAgNnSc2d1
vakua	vakuum	k1gNnSc2
a	a	k8xC
absence	absence	k1gFnSc2
smyslu	smysl	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
ústí	ústit	k5eAaImIp3nS
do	do	k7c2
stavu	stav	k1gInSc2
nudy	nuda	k1gFnSc2
<g/>
,	,	kIx,
apatie	apatie	k1gFnSc2
a	a	k8xC
prázdnoty	prázdnota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
tomto	tento	k3xDgInSc6
stavu	stav	k1gInSc6
cynický	cynický	k2eAgMnSc1d1
<g/>
,	,	kIx,
postrádá	postrádat	k5eAaImIp3nS
směr	směr	k1gInSc4
a	a	k8xC
pochybuje	pochybovat	k5eAaImIp3nS
o	o	k7c6
smyslu	smysl	k1gInSc6
většiny	většina	k1gFnSc2
svých	svůj	k3xOyFgFnPc2
životních	životní	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Frankl	Franknout	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
duchovním	duchovní	k2eAgMnSc7d1
otcem	otec	k1gMnSc7
myšlenky	myšlenka	k1gFnSc2
<g/>
,	,	kIx,
podle	podle	k7c2
níž	jenž	k3xRgFnSc2
by	by	kYmCp3nS
socha	socha	k1gFnSc1
Svobody	svoboda	k1gFnSc2
na	na	k7c6
východním	východní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
doplněna	doplnit	k5eAaPmNgFnS
sochou	socha	k1gFnSc7
Zodpovědnosti	zodpovědnost	k1gFnSc2
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
západním	západní	k2eAgNnSc6d1
</s>
<s>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Svoboda	Svoboda	k1gMnSc1
nicméně	nicméně	k8xC
není	být	k5eNaImIp3nS
posledním	poslední	k2eAgNnSc7d1
slovem	slovo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svoboda	Svoboda	k1gMnSc1
je	být	k5eAaImIp3nS
jen	jen	k9
částí	část	k1gFnSc7
příběhu	příběh	k1gInSc2
a	a	k8xC
polovinou	polovina	k1gFnSc7
pravdy	pravda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
negativním	negativní	k2eAgInSc7d1
aspektem	aspekt	k1gInSc7
celého	celý	k2eAgInSc2d1
fenoménu	fenomén	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
pozitivním	pozitivní	k2eAgInSc7d1
aspektem	aspekt	k1gInSc7
je	být	k5eAaImIp3nS
odpovědnost	odpovědnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svoboda	Svoboda	k1gMnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
ohrožena	ohrozit	k5eAaPmNgFnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
zdegeneruje	zdegenerovat	k5eAaPmIp3nS
do	do	k7c2
pouhé	pouhý	k2eAgFnSc2d1
libovůle	libovůle	k1gFnSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
není	být	k5eNaImIp3nS
prožívána	prožívat	k5eAaImNgFnS
zodpovědně	zodpovědně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
proto	proto	k8xC
doporučuji	doporučovat	k5eAaImIp1nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
socha	socha	k1gFnSc1
Svobody	svoboda	k1gFnSc2
na	na	k7c6
východním	východní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
byla	být	k5eAaImAgFnS
doplněna	doplnit	k5eAaPmNgFnS
sochou	socha	k1gFnSc7
Zodpovědnosti	zodpovědnost	k1gFnSc2
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
západním	západní	k2eAgNnSc6d1
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Viktor	Viktor	k1gMnSc1
E.	E.	kA
Frankl	Frankl	k1gMnSc1
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Teorie	teorie	k1gFnSc1
a	a	k8xC
terapie	terapie	k1gFnSc1
neuróz	neuróza	k1gFnPc2
<g/>
,	,	kIx,
Grada	Grada	k1gFnSc1
1999	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7169-779-6	80-7169-779-6	k4
a	a	k8xC
Portál	portál	k1gInSc1
2019	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-262-1478-6	978-80-262-1478-6	k4
</s>
<s>
Utrpení	utrpení	k1gNnSc1
z	z	k7c2
nesmyslnosti	nesmyslnost	k1gFnSc2
života	život	k1gInSc2
<g/>
,	,	kIx,
Portál	portál	k1gInSc1
2016	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-262-1038-2	978-80-262-1038-2	k4
</s>
<s>
Psychoterapie	psychoterapie	k1gFnSc1
a	a	k8xC
náboženství	náboženství	k1gNnSc1
<g/>
,	,	kIx,
Cesta	cesta	k1gFnSc1
2007	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7295-088-6	80-7295-088-6	k4
</s>
<s>
Vůle	vůle	k1gFnSc1
ke	k	k7c3
smyslu	smysl	k1gInSc3
<g/>
,	,	kIx,
Cesta	cesta	k1gFnSc1
2006	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7295-084-3	80-7295-084-3	k4
</s>
<s>
Lékařská	lékařský	k2eAgFnSc1d1
péče	péče	k1gFnSc1
o	o	k7c6
duši	duše	k1gFnSc6
<g/>
,	,	kIx,
Cesta	cesta	k1gFnSc1
2006	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7295-085-1	80-7295-085-1	k4
</s>
<s>
A	a	k9
přesto	přesto	k8xC
říci	říct	k5eAaPmF
životu	život	k1gInSc3
ano	ano	k9
<g/>
,	,	kIx,
Karmelitánské	karmelitánský	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
2006	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7192-848-8	80-7192-848-8	k4
</s>
<s>
Psychoterapie	psychoterapie	k1gFnSc1
pro	pro	k7c4
laiky	laik	k1gMnPc4
<g/>
,	,	kIx,
Cesta	cesta	k1gFnSc1
1998	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-85319-80-2	80-85319-80-2	k4
</s>
<s>
Co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
v	v	k7c6
mých	můj	k3xOp1gFnPc6
knihách	kniha	k1gFnPc6
není	být	k5eNaImIp3nS
<g/>
,	,	kIx,
Cesta	cesta	k1gFnSc1
1997	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-85319-66-7	80-85319-66-7	k4
</s>
<s>
Člověk	člověk	k1gMnSc1
hledá	hledat	k5eAaImIp3nS
smysl	smysl	k1gInSc4
<g/>
,	,	kIx,
Psychoanalytické	psychoanalytický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
1994	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-901601-4-X	80-901601-4-X	k4
</s>
<s>
Prožitek	prožitek	k1gInSc1
hor	hora	k1gFnPc2
a	a	k8xC
zkušenost	zkušenost	k1gFnSc1
smyslu	smysl	k1gInSc2
<g/>
,	,	kIx,
Cesta	cesta	k1gFnSc1
(	(	kIx(
<g/>
nakladatelství	nakladatelství	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-7295-185-7	978-80-7295-185-7	k4
</s>
<s>
Bůh	bůh	k1gMnSc1
a	a	k8xC
člověk	člověk	k1gMnSc1
hledající	hledající	k2eAgInSc4d1
smysl	smysl	k1gInSc4
<g/>
,	,	kIx,
Cesta	cesta	k1gFnSc1
(	(	kIx(
<g/>
nakladatelství	nakladatelství	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-7295-137-6	978-80-7295-137-6	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Viktor	Viktor	k1gMnSc1
E.	E.	kA
Frankl	Frankl	k1gMnSc1
<g/>
:	:	kIx,
Logoterapie	logoterapie	k1gFnSc1
<g/>
↑	↑	k?
Jolana	Jolana	k1gFnSc1
Poláková	Poláková	k1gFnSc1
<g/>
.	.	kIx.
...	...	k?
<g/>
vždyť	vždyť	k9
se	s	k7c7
mnou	já	k3xPp1nSc7
jsi	být	k5eAaImIp2nS
Ty	ty	k3xPp2nSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teologické	teologický	k2eAgInPc1d1
texty	text	k1gInPc1
<g/>
.	.	kIx.
1995	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
108	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
.	.	kIx.
<g/>
jpg	jpg	k?
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
19	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
<g/>
)	)	kIx)
↑	↑	k?
Viktor	Viktor	k1gMnSc1
E.	E.	kA
Frankl	Frankl	k1gMnSc1
(	(	kIx(
<g/>
na	na	k7c6
stránkách	stránka	k1gFnPc6
Společnosti	společnost	k1gFnSc2
pro	pro	k7c4
logoterapii	logoterapie	k1gFnSc4
a	a	k8xC
existenciální	existenciální	k2eAgFnSc4d1
analýzu	analýza	k1gFnSc4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Anotace	anotace	k1gFnPc4
ke	k	k7c3
knize	kniha	k1gFnSc3
When	Whena	k1gFnPc2
life	lifat	k5eAaPmIp3nS
calls	calls	k6eAd1
out	out	k?
to	ten	k3xDgNnSc1
us	us	k?
<g/>
,	,	kIx,
books	books	k1gInSc1
<g/>
.	.	kIx.
<g/>
google	google	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Viktor	Viktor	k1gMnSc1
Frankl	Frankl	k1gMnSc1
<g/>
:	:	kIx,
Život	život	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
dává	dávat	k5eAaImIp3nS
smysl	smysl	k1gInSc4
<g/>
.	.	kIx.
www.dreamlife.cz	www.dreamlife.cz	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
REDSAND	REDSAND	kA
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
S.	S.	kA
Viktor	Viktor	k1gMnSc1
Frankl	Frankl	k1gMnSc1
<g/>
:	:	kIx,
A	A	kA
life	life	k1gInSc1
worth	worth	k1gInSc1
living	living	k1gInSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Clarion	Clarion	k1gInSc4
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
↑	↑	k?
Dr	dr	kA
<g/>
.	.	kIx.
Kazimier	Kazimier	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Popielski	Popielski	k1gNnSc1
<g/>
,	,	kIx,
předmluva	předmluva	k1gFnSc1
knihy	kniha	k1gFnSc2
<g/>
:	:	kIx,
Elisabeth	Elisabeth	k1gMnSc1
Lukas	Lukas	k1gMnSc1
I	i	k9
tvoje	tvůj	k3xOp2gNnSc4
utrpení	utrpení	k1gNnSc4
má	mít	k5eAaImIp3nS
smysl	smysl	k1gInSc1
<g/>
,	,	kIx,
Cesta	cesta	k1gFnSc1
1998	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85319	#num#	k4
<g/>
-	-	kIx~
<g/>
79	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
↑	↑	k?
Viktor	Viktor	k1gMnSc1
E.	E.	kA
Frankl	Frankl	k1gFnSc1
Vůle	vůle	k1gFnSc1
ke	k	k7c3
smyslu	smysl	k1gInSc3
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Cesta	cesta	k1gFnSc1
1997	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
19	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85319	#num#	k4
<g/>
-	-	kIx~
<g/>
63	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
↑	↑	k?
V.	V.	kA
E.	E.	kA
Frankl	Frankl	k1gMnSc1
-	-	kIx~
cíl	cíl	k1gInSc1
a	a	k8xC
smysl	smysl	k1gInSc1
života	život	k1gInSc2
<g/>
.	.	kIx.
www.psychoweb.cz	www.psychoweb.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Statue	statue	k1gFnSc2
of	of	k?
Responsibility	Responsibilita	k1gFnSc2
<g/>
,	,	kIx,
Daily	Daila	k1gFnSc2
Herald	Heralda	k1gFnPc2
<g/>
,	,	kIx,
8	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2005	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
TAVEL	TAVEL	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
:	:	kIx,
Smysl	smysl	k1gInSc1
života	život	k1gInSc2
podle	podle	k7c2
Viktora	Viktor	k1gMnSc2
Emanuela	Emanuel	k1gMnSc2
Frankla	Frankla	k1gMnSc2
<g/>
,	,	kIx,
Triton	triton	k1gInSc1
2007	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7254-915-4	80-7254-915-4	k4
</s>
<s>
FRANKL	FRANKL	kA
<g/>
,	,	kIx,
Viktor	Viktor	k1gMnSc1
E.	E.	kA
<g/>
:	:	kIx,
Utrpení	utrpení	k1gNnSc1
z	z	k7c2
nesmyslnosti	nesmyslnost	k1gFnSc2
života	život	k1gInSc2
<g/>
,	,	kIx,
Portál	portál	k1gInSc1
2016	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-262-1038-2	978-80-262-1038-2	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
A	a	k9
přesto	přesto	k8xC
říci	říct	k5eAaPmF
životu	život	k1gInSc3
ano	ano	k9
</s>
<s>
Logoterapie	logoterapie	k1gFnSc1
</s>
<s>
Existenciální	existenciální	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
</s>
<s>
Elisabeth	Elisabeth	k1gMnSc1
Lukas	Lukas	k1gMnSc1
</s>
<s>
Alfried	Alfried	k1gInSc1
Längle	Längle	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Viktor	Viktor	k1gMnSc1
Frankl	Frankl	k1gFnSc7
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Viktor	Viktor	k1gMnSc1
Frankl	Frankl	k1gMnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Viktor	Viktor	k1gMnSc1
Frankl	Frankl	k1gMnSc1
</s>
<s>
Viktor	Viktor	k1gMnSc1
Frankl	Frankl	k1gFnSc2
at	at	k?
Ninety	Nineta	k1gFnSc2
<g/>
:	:	kIx,
An	An	k1gFnSc1
Interview	interview	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
duben	duben	k1gInSc1
1995	#num#	k4
</s>
<s>
Institut	institut	k1gInSc1
Viktora	Viktor	k1gMnSc2
Frankla	Frankla	k1gMnSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
,	,	kIx,
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Viktor	Viktor	k1gMnSc1
Frankl	Frankl	k1gMnSc1
Institute	institut	k1gInSc5
of	of	k?
Logotherapy	Logotherap	k1gMnPc7
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19990009561	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118534904	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0859	#num#	k4
4462	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79122444	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
95178598	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79122444	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Medicína	medicína	k1gFnSc1
|	|	kIx~
Rakousko	Rakousko	k1gNnSc1
</s>
