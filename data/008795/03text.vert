<p>
<s>
Střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
též	též	k9	též
AC	AC	kA	AC
(	(	kIx(	(
<g/>
alternating	alternating	k1gInSc1	alternating
current	current	k1gInSc1	current
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
označující	označující	k2eAgInSc1d1	označující
elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
směr	směr	k1gInSc1	směr
se	se	k3xPyFc4	se
v	v	k7c6	v
čase	čas	k1gInSc6	čas
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
proudu	proud	k1gInSc2	proud
DC	DC	kA	DC
(	(	kIx(	(
<g/>
direct	direct	k1gMnSc1	direct
current	current	k1gMnSc1	current
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
obvodem	obvod	k1gInSc7	obvod
stále	stále	k6eAd1	stále
stejným	stejný	k2eAgInSc7d1	stejný
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc1	jeho
velikost	velikost	k1gFnSc1	velikost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
proměnná	proměnná	k1gFnSc1	proměnná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
==	==	k?	==
</s>
</p>
<p>
<s>
Střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
periodický	periodický	k2eAgInSc4d1	periodický
nebo	nebo	k8xC	nebo
neperiodický	periodický	k2eNgInSc4d1	neperiodický
průběh	průběh	k1gInSc4	průběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
periodický	periodický	k2eAgInSc1d1	periodický
průběh	průběh	k1gInSc1	průběh
-	-	kIx~	-
okamžité	okamžitý	k2eAgFnSc2d1	okamžitá
hodnoty	hodnota	k1gFnSc2	hodnota
proudu	proud	k1gInSc2	proud
se	se	k3xPyFc4	se
po	po	k7c6	po
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnSc6d1	označovaná
jako	jako	k8xC	jako
perioda	perioda	k1gFnSc1	perioda
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
opakují	opakovat	k5eAaImIp3nP	opakovat
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
proud	proud	k1gInSc1	proud
v	v	k7c6	v
rozvodné	rozvodný	k2eAgFnSc6d1	rozvodná
síti	síť	k1gFnSc6	síť
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
neperiodický	periodický	k2eNgInSc1d1	neperiodický
průběh	průběh	k1gInSc1	průběh
-	-	kIx~	-
hodnoty	hodnota	k1gFnSc2	hodnota
okamžitého	okamžitý	k2eAgInSc2d1	okamžitý
proudu	proud	k1gInSc2	proud
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
čase	čas	k1gInSc6	čas
náhodné	náhodný	k2eAgNnSc1d1	náhodné
(	(	kIx(	(
<g/>
takový	takový	k3xDgInSc4	takový
charakter	charakter	k1gInSc4	charakter
má	mít	k5eAaImIp3nS	mít
například	například	k6eAd1	například
šum	šum	k1gInSc4	šum
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
záznam	záznam	k1gInSc1	záznam
lidské	lidský	k2eAgFnSc2d1	lidská
řeči	řeč	k1gFnSc2	řeč
<g/>
)	)	kIx)	)
<g/>
Speciálním	speciální	k2eAgInSc7d1	speciální
periodickým	periodický	k2eAgInSc7d1	periodický
průběhem	průběh	k1gInSc7	průběh
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
harmonický	harmonický	k2eAgInSc1d1	harmonický
střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitá	okamžitý	k2eAgFnSc1d1	okamžitá
hodnota	hodnota	k1gFnSc1	hodnota
harmonického	harmonický	k2eAgInSc2d1	harmonický
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
se	se	k3xPyFc4	se
v	v	k7c6	v
čase	čas	k1gInSc6	čas
mění	měnit	k5eAaImIp3nS	měnit
podle	podle	k7c2	podle
funkce	funkce	k1gFnSc2	funkce
sinus	sinus	k1gInSc1	sinus
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
φ	φ	k?	φ
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
i	i	k9	i
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
I_	I_	k1gMnSc1	I_
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
sin	sin	kA	sin
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
t	t	k?	t
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
Harmonický	harmonický	k2eAgInSc1d1	harmonický
střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
prochází	procházet	k5eAaImIp3nS	procházet
obvodem	obvod	k1gInSc7	obvod
s	s	k7c7	s
lineární	lineární	k2eAgFnSc7d1	lineární
zátěží	zátěž	k1gFnSc7	zátěž
(	(	kIx(	(
<g/>
odpor	odpor	k1gInSc1	odpor
<g/>
,	,	kIx,	,
indukčnost	indukčnost	k1gFnSc1	indukčnost
<g/>
,	,	kIx,	,
kapacita	kapacita	k1gFnSc1	kapacita
<g/>
)	)	kIx)	)
při	při	k7c6	při
napájení	napájení	k1gNnSc6	napájení
ze	z	k7c2	z
zdroje	zdroj	k1gInSc2	zdroj
harmonického	harmonický	k2eAgNnSc2d1	harmonické
střídavého	střídavý	k2eAgNnSc2d1	střídavé
napětí	napětí	k1gNnSc2	napětí
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
u	u	k7c2	u
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
φ	φ	k?	φ
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
φ	φ	k?	φ
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
u	u	k7c2	u
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
U_	U_	k1gMnSc1	U_
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
sin	sin	kA	sin
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
t	t	k?	t
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
Im	Im	k1gFnSc1	Im
je	být	k5eAaImIp3nS	být
amplituda	amplituda	k1gFnSc1	amplituda
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
Um	um	k1gInSc1	um
amplituda	amplituda	k1gFnSc1	amplituda
střídavého	střídavý	k2eAgNnSc2d1	střídavé
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
ω	ω	k?	ω
je	být	k5eAaImIp3nS	být
úhlová	úhlový	k2eAgFnSc1d1	úhlová
frekvence	frekvence	k1gFnSc1	frekvence
<g/>
,	,	kIx,	,
φ	φ	k?	φ
<g/>
0	[number]	k4	0
je	být	k5eAaImIp3nS	být
počáteční	počáteční	k2eAgFnSc1d1	počáteční
fáze	fáze	k1gFnSc1	fáze
střídavého	střídavý	k2eAgNnSc2d1	střídavé
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
φ	φ	k?	φ
je	být	k5eAaImIp3nS	být
fázový	fázový	k2eAgInSc1d1	fázový
posuv	posuv	k1gInSc1	posuv
mezi	mezi	k7c7	mezi
napětím	napětí	k1gNnSc7	napětí
a	a	k8xC	a
proudem	proud	k1gInSc7	proud
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
se	se	k3xPyFc4	se
zkráceně	zkráceně	k6eAd1	zkráceně
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
fázi	fáze	k1gFnSc6	fáze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
libovolný	libovolný	k2eAgInSc4d1	libovolný
obecný	obecný	k2eAgInSc4d1	obecný
průběh	průběh	k1gInSc4	průběh
např.	např.	kA	např.
trojúhelníkový	trojúhelníkový	k2eAgMnSc1d1	trojúhelníkový
<g/>
,	,	kIx,	,
pilový	pilový	k2eAgMnSc1d1	pilový
<g/>
,	,	kIx,	,
obdélníkový	obdélníkový	k2eAgMnSc1d1	obdélníkový
<g/>
,	,	kIx,	,
impulsní	impulsní	k2eAgMnSc1d1	impulsní
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
každý	každý	k3xTgInSc4	každý
periodický	periodický	k2eAgInSc4d1	periodický
střídavý	střídavý	k2eAgInSc4d1	střídavý
signál	signál	k1gInSc4	signál
můžeme	moct	k5eAaImIp1nP	moct
pomocí	pomocí	k7c2	pomocí
Fourierovy	Fourierův	k2eAgFnSc2d1	Fourierova
transformace	transformace	k1gFnSc2	transformace
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
harmonických	harmonický	k2eAgInPc2d1	harmonický
sinusových	sinusový	k2eAgInPc2d1	sinusový
signálů	signál	k1gInPc2	signál
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
že	že	k8xS	že
můžeme	moct	k5eAaImIp1nP	moct
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
průběh	průběh	k1gInSc4	průběh
pohlížet	pohlížet	k5eAaImF	pohlížet
jako	jako	k9	jako
na	na	k7c4	na
součet	součet	k1gInSc4	součet
sinusových	sinusový	k2eAgInPc2d1	sinusový
průběhů	průběh	k1gInPc2	průběh
o	o	k7c6	o
různých	různý	k2eAgFnPc6d1	různá
frekvencích	frekvence	k1gFnPc6	frekvence
a	a	k8xC	a
amplitudách	amplituda	k1gFnPc6	amplituda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obvody	obvod	k1gInPc4	obvod
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
napětí	napětí	k1gNnSc2	napětí
===	===	k?	===
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
v	v	k7c6	v
konstantním	konstantní	k2eAgNnSc6d1	konstantní
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
otáčíme	otáčet	k5eAaImIp1nP	otáčet
cívkou	cívka	k1gFnSc7	cívka
(	(	kIx(	(
<g/>
osa	osa	k1gFnSc1	osa
otáčení	otáčení	k1gNnSc2	otáčení
je	být	k5eAaImIp3nS	být
kolmá	kolmý	k2eAgFnSc1d1	kolmá
na	na	k7c4	na
osu	osa	k1gFnSc4	osa
cívky	cívka	k1gFnSc2	cívka
i	i	k9	i
na	na	k7c4	na
siločáry	siločára	k1gFnPc4	siločára
pole	pole	k1gNnSc2	pole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
na	na	k7c6	na
jejích	její	k3xOp3gInPc6	její
vývodech	vývod	k1gInPc6	vývod
indukuje	indukovat	k5eAaBmIp3nS	indukovat
harmonické	harmonický	k2eAgNnSc1d1	harmonické
střídavé	střídavý	k2eAgNnSc1d1	střídavé
napětí	napětí	k1gNnSc1	napětí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
předpokládáme	předpokládat	k5eAaImIp1nP	předpokládat
pasivní	pasivní	k2eAgFnSc4d1	pasivní
lineární	lineární	k2eAgFnSc4d1	lineární
zátěž	zátěž	k1gFnSc4	zátěž
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
a	a	k8xC	a
střídavé	střídavý	k2eAgNnSc1d1	střídavé
napětí	napětí	k1gNnSc1	napětí
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
obvodu	obvod	k1gInSc6	obvod
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
frekvenci	frekvence	k1gFnSc4	frekvence
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
charakteru	charakter	k1gInSc2	charakter
zátěže	zátěž	k1gFnSc2	zátěž
má	mít	k5eAaImIp3nS	mít
proud	proud	k1gInSc1	proud
vůči	vůči	k7c3	vůči
napětí	napětí	k1gNnSc3	napětí
různý	různý	k2eAgInSc4d1	různý
fázový	fázový	k2eAgInSc4d1	fázový
posuv	posuv	k1gInSc4	posuv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
součástkách	součástka	k1gFnPc6	součástka
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
(	(	kIx(	(
<g/>
kondenzátor	kondenzátor	k1gInSc1	kondenzátor
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
proud	proud	k1gInSc1	proud
proti	proti	k7c3	proti
napětí	napětí	k1gNnSc3	napětí
fázový	fázový	k2eAgInSc4d1	fázový
předstih	předstih	k1gInSc4	předstih
<g/>
,	,	kIx,	,
na	na	k7c6	na
součástkách	součástka	k1gFnPc6	součástka
s	s	k7c7	s
indukčností	indukčnost	k1gFnSc7	indukčnost
(	(	kIx(	(
<g/>
cívka	cívka	k1gFnSc1	cívka
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
proud	proud	k1gInSc1	proud
proti	proti	k7c3	proti
napětí	napětí	k1gNnSc4	napětí
fázové	fázový	k2eAgNnSc1d1	fázové
zpoždění	zpoždění	k1gNnSc1	zpoždění
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k6eAd1	též
RLC	RLC	kA	RLC
obvody	obvod	k1gInPc1	obvod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Součástky	součástka	k1gFnPc1	součástka
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
elektrickým	elektrický	k2eAgInSc7d1	elektrický
odporem	odpor	k1gInSc7	odpor
(	(	kIx(	(
<g/>
rezistor	rezistor	k1gInSc1	rezistor
<g/>
)	)	kIx)	)
nevytvářejí	vytvářet	k5eNaImIp3nP	vytvářet
žádný	žádný	k3yNgInSc4	žádný
fázový	fázový	k2eAgInSc4d1	fázový
posuv	posuv	k1gInSc4	posuv
–	–	k?	–
napětí	napětí	k1gNnSc2	napětí
a	a	k8xC	a
proud	proud	k1gInSc1	proud
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lineární	lineární	k2eAgFnSc1d1	lineární
zátěž	zátěž	k1gFnSc1	zátěž
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
střídavém	střídavý	k2eAgInSc6d1	střídavý
obvodu	obvod	k1gInSc6	obvod
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
svou	svůj	k3xOyFgFnSc7	svůj
impedancí	impedance	k1gFnSc7	impedance
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
impedance	impedance	k1gFnSc2	impedance
závisí	záviset	k5eAaImIp3nS	záviset
vedle	vedle	k7c2	vedle
rezistence	rezistence	k1gFnSc2	rezistence
též	též	k9	též
na	na	k7c6	na
zdánlivých	zdánlivý	k2eAgInPc6d1	zdánlivý
odporech	odpor	k1gInPc6	odpor
(	(	kIx(	(
<g/>
induktivní	induktivní	k2eAgFnPc4d1	induktivní
reaktanci	reaktance	k1gFnSc4	reaktance
<g/>
,	,	kIx,	,
kapacitní	kapacitní	k2eAgFnSc4d1	kapacitní
reaktanci	reaktance	k1gFnSc4	reaktance
<g/>
)	)	kIx)	)
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
součástek	součástka	k1gFnPc2	součástka
proti	proti	k7c3	proti
průchodu	průchod	k1gInSc3	průchod
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Velikost	velikost	k1gFnSc1	velikost
střídavých	střídavý	k2eAgFnPc2d1	střídavá
veličin	veličina	k1gFnPc2	veličina
===	===	k?	===
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jediným	jediný	k2eAgNnSc7d1	jediné
číslem	číslo	k1gNnSc7	číslo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
hodnota	hodnota	k1gFnSc1	hodnota
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
zavádíme	zavádět	k5eAaImIp1nP	zavádět
několik	několik	k4yIc4	několik
různých	různý	k2eAgInPc2d1	různý
odlišně	odlišně	k6eAd1	odlišně
definovaných	definovaný	k2eAgFnPc2d1	definovaná
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Imax	Imax	k1gInSc1	Imax
je	být	k5eAaImIp3nS	být
špičkový	špičkový	k2eAgInSc4d1	špičkový
proud	proud	k1gInSc4	proud
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hodnota	hodnota	k1gFnSc1	hodnota
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
průběhu	průběh	k1gInSc6	průběh
vyskytujeI	vyskytujeI	k?	vyskytujeI
<g/>
'	'	kIx"	'
<g/>
ef	ef	k?	ef
<g/>
'	'	kIx"	'
je	být	k5eAaImIp3nS	být
efektivní	efektivní	k2eAgFnSc1d1	efektivní
hodnota	hodnota	k1gFnSc1	hodnota
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
v	v	k7c6	v
silových	silový	k2eAgInPc6d1	silový
střídavých	střídavý	k2eAgInPc6d1	střídavý
obvodech	obvod	k1gInPc6	obvod
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejčastěji	často	k6eAd3	často
udávanou	udávaný	k2eAgFnSc4d1	udávaná
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
specifikováno	specifikovat	k5eAaBmNgNnS	specifikovat
o	o	k7c4	o
jakou	jaký	k3yIgFnSc4	jaký
hodnotu	hodnota	k1gFnSc4	hodnota
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
myšlena	myšlen	k2eAgFnSc1d1	myšlena
hodnota	hodnota	k1gFnSc1	hodnota
efektivní	efektivní	k2eAgFnSc1d1	efektivní
<g/>
.	.	kIx.	.
</s>
<s>
Efektivní	efektivní	k2eAgFnSc1d1	efektivní
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
velikost	velikost	k1gFnSc1	velikost
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
rezistorem	rezistor	k1gInSc7	rezistor
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
stejný	stejný	k2eAgInSc1d1	stejný
tepelný	tepelný	k2eAgInSc1d1	tepelný
účinek	účinek	k1gInSc1	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
matematického	matematický	k2eAgNnSc2d1	matematické
hlediska	hledisko	k1gNnSc2	hledisko
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kvadratický	kvadratický	k2eAgInSc4d1	kvadratický
průměr	průměr	k1gInSc4	průměr
hodnot	hodnota	k1gFnPc2	hodnota
uceleného	ucelený	k2eAgInSc2d1	ucelený
počtu	počet	k1gInSc2	počet
period	perioda	k1gFnPc2	perioda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
sinusový	sinusový	k2eAgInSc4d1	sinusový
průběh	průběh	k1gInSc4	průběh
platí	platit	k5eAaImIp3nS	platit
tyto	tento	k3xDgInPc4	tento
vztahy	vztah	k1gInPc4	vztah
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ief	Ief	k?	Ief
=	=	kIx~	=
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
√	√	k?	√
<g/>
2	[number]	k4	2
.	.	kIx.	.
</s>
<s>
Imax	Imax	k1gInSc1	Imax
≈	≈	k?	≈
0,7072	[number]	k4	0,7072
<g/>
.	.	kIx.	.
</s>
<s>
ImaxImax	ImaxImax	k1gInSc1	ImaxImax
=	=	kIx~	=
√	√	k?	√
<g/>
2	[number]	k4	2
.	.	kIx.	.
</s>
<s>
Ief	Ief	k?	Ief
≈	≈	k?	≈
1,414	[number]	k4	1,414
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Ief	Ief	k?	Ief
</s>
</p>
<p>
<s>
Istř	Istř	k1gFnSc1	Istř
–	–	k?	–
střední	střední	k2eAgFnSc1d1	střední
absolutní	absolutní	k2eAgFnSc1d1	absolutní
hodnota	hodnota	k1gFnSc1	hodnota
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
střední	střední	k2eAgFnSc1d1	střední
hodnota	hodnota	k1gFnSc1	hodnota
absolutních	absolutní	k2eAgFnPc2d1	absolutní
hodnot	hodnota	k1gFnPc2	hodnota
proudu	proud	k1gInSc2	proud
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∫	∫	k?	∫
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
τ	τ	k?	τ
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
τ	τ	k?	τ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
I_	I_	k1gMnSc1	I_
<g/>
{	{	kIx(	{
<g/>
str	str	kA	str
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
+	+	kIx~	+
<g/>
T	T	kA	T
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
i	i	k9	i
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc1	tau
)	)	kIx)	)
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc1	tau
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
hodnotu	hodnota	k1gFnSc4	hodnota
ustáleného	ustálený	k2eAgInSc2d1	ustálený
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
projde	projít	k5eAaPmIp3nS	projít
vodičem	vodič	k1gInSc7	vodič
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
periodu	perioda	k1gFnSc4	perioda
stejný	stejný	k2eAgInSc4d1	stejný
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
jako	jako	k8xC	jako
u	u	k7c2	u
proudu	proud	k1gInSc2	proud
střídavého	střídavý	k2eAgInSc2d1	střídavý
(	(	kIx(	(
<g/>
uvažují	uvažovat	k5eAaImIp3nP	uvažovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ovšem	ovšem	k9	ovšem
absolutní	absolutní	k2eAgFnSc2d1	absolutní
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
proud	proud	k1gInSc1	proud
naměřený	naměřený	k2eAgInSc1d1	naměřený
stejnosměrným	stejnosměrný	k2eAgInSc7d1	stejnosměrný
ampérmetrem	ampérmetr	k1gInSc7	ampérmetr
<g/>
,	,	kIx,	,
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
dvoucestného	dvoucestný	k2eAgNnSc2d1	dvoucestné
usměrnění	usměrnění	k1gNnSc2	usměrnění
např.	např.	kA	např.
Grätzovým	Grätzův	k2eAgInSc7d1	Grätzův
můstkem	můstek	k1gInSc7	můstek
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
levnější	levný	k2eAgInPc1d2	levnější
měřicí	měřicí	k2eAgInPc1d1	měřicí
přístroje	přístroj	k1gInPc1	přístroj
proto	proto	k8xC	proto
měří	měřit	k5eAaImIp3nP	měřit
místo	místo	k7c2	místo
Ief	Ief	k1gFnSc2	Ief
hodnotu	hodnota	k1gFnSc4	hodnota
Istř	Istř	k1gFnSc4	Istř
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
jednodušší	jednoduchý	k2eAgNnSc1d2	jednodušší
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
ji	on	k3xPp3gFnSc4	on
násobí	násobit	k5eAaImIp3nP	násobit
konstantou	konstanta	k1gFnSc7	konstanta
1,110	[number]	k4	1,110
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zdrojem	zdroj	k1gInSc7	zdroj
systematických	systematický	k2eAgFnPc2d1	systematická
chyb	chyba	k1gFnPc2	chyba
při	při	k7c6	při
měření	měření	k1gNnSc6	měření
neharmonických	harmonický	k2eNgInPc2d1	neharmonický
průběhů	průběh	k1gInPc2	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Přístroje	přístroj	k1gInPc1	přístroj
umožňující	umožňující	k2eAgInPc1d1	umožňující
měřit	měřit	k5eAaImF	měřit
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
efektivní	efektivní	k2eAgFnSc4d1	efektivní
hodnotu	hodnota	k1gFnSc4	hodnota
libovolných	libovolný	k2eAgInPc2d1	libovolný
střídavých	střídavý	k2eAgInPc2d1	střídavý
průběhů	průběh	k1gInPc2	průběh
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
označuji	označovat	k5eAaImIp1nS	označovat
true	true	k1gInSc1	true
RMS	RMS	kA	RMS
<g/>
.	.	kIx.	.
<g/>
Vztah	vztah	k1gInSc1	vztah
Istř	Istř	k1gInSc1	Istř
k	k	k7c3	k
ostatním	ostatní	k2eAgNnPc3d1	ostatní
napětím	napětí	k1gNnPc3	napětí
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Istř	Istř	k1gFnSc1	Istř
=	=	kIx~	=
2	[number]	k4	2
Imax	Imax	k1gInSc1	Imax
/	/	kIx~	/
π	π	k?	π
=	=	kIx~	=
0,6366	[number]	k4	0,6366
ImaxImax	ImaxImax	k1gInSc1	ImaxImax
=	=	kIx~	=
π	π	k?	π
.	.	kIx.	.
</s>
<s>
Istř	Istř	k1gFnSc1	Istř
/	/	kIx~	/
2	[number]	k4	2
=	=	kIx~	=
1,570	[number]	k4	1,570
<g/>
8	[number]	k4	8
IstřIstř	IstřIstř	k1gFnSc1	IstřIstř
=	=	kIx~	=
√	√	k?	√
<g/>
8	[number]	k4	8
Ief	Ief	k1gFnSc1	Ief
/	/	kIx~	/
π	π	k?	π
=	=	kIx~	=
0,9003	[number]	k4	0,9003
IefIef	IefIef	k1gInSc1	IefIef
=	=	kIx~	=
π	π	k?	π
.	.	kIx.	.
</s>
<s>
Istř	Istř	k1gFnSc1	Istř
/	/	kIx~	/
√	√	k?	√
<g/>
8	[number]	k4	8
=	=	kIx~	=
1,110	[number]	k4	1,110
<g/>
7	[number]	k4	7
Istř	Istř	k1gFnSc1	Istř
</s>
</p>
<p>
<s>
===	===	k?	===
Výkon	výkon	k1gInSc1	výkon
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
===	===	k?	===
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
neustále	neustále	k6eAd1	neustále
se	se	k3xPyFc4	se
měnící	měnící	k2eAgFnSc3d1	měnící
okamžité	okamžitý	k2eAgFnSc3d1	okamžitá
hodnotě	hodnota	k1gFnSc3	hodnota
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
napětí	napětí	k1gNnSc2	napětí
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
také	také	k9	také
elektrický	elektrický	k2eAgInSc4d1	elektrický
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
elektrický	elektrický	k2eAgInSc1d1	elektrický
výkon	výkon	k1gInSc1	výkon
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
lze	lze	k6eAd1	lze
vypočítat	vypočítat	k5eAaPmF	vypočítat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
<s>
φ	φ	k?	φ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
=	=	kIx~	=
<g/>
U	u	k7c2	u
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
I	I	kA	I
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
U	u	k7c2	u
a	a	k8xC	a
I	i	k8xC	i
jsou	být	k5eAaImIp3nP	být
efektivní	efektivní	k2eAgFnPc1d1	efektivní
hodnoty	hodnota	k1gFnPc1	hodnota
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
φ	φ	k?	φ
je	být	k5eAaImIp3nS	být
fázový	fázový	k2eAgInSc1d1	fázový
posuv	posuv	k1gInSc1	posuv
mezi	mezi	k7c7	mezi
proudem	proud	k1gInSc7	proud
a	a	k8xC	a
napětím	napětí	k1gNnSc7	napětí
<g/>
,	,	kIx,	,
člen	člen	k1gInSc1	člen
cos	cos	kA	cos
φ	φ	k?	φ
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
účiník	účiník	k1gInSc1	účiník
<g/>
.	.	kIx.	.
</s>
<s>
Pozor	pozor	k1gInSc1	pozor
platí	platit	k5eAaImIp3nS	platit
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
sinových	sinový	k2eAgInPc2d1	sinový
průběhů	průběh	k1gInPc2	průběh
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Efektivní	efektivní	k2eAgFnPc1d1	efektivní
hodnoty	hodnota	k1gFnPc1	hodnota
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
napětí	napětí	k1gNnSc2	napětí
jsou	být	k5eAaImIp3nP	být
hodnoty	hodnota	k1gFnPc4	hodnota
takového	takový	k3xDgInSc2	takový
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
výkon	výkon	k1gInSc1	výkon
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
výkon	výkon	k1gInSc4	výkon
daného	daný	k2eAgInSc2d1	daný
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
napětí	napětí	k1gNnSc2	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
efektivní	efektivní	k2eAgFnSc2d1	efektivní
hodnoty	hodnota	k1gFnSc2	hodnota
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
napětí	napětí	k1gNnSc2	napětí
s	s	k7c7	s
harmonickým	harmonický	k2eAgInSc7d1	harmonický
průběhem	průběh	k1gInSc7	průběh
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
≈	≈	k?	≈
</s>
</p>
<p>
<s>
0,707	[number]	k4	0,707
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
I_	I_	k1gFnSc6	I_
<g/>
{	{	kIx(	{
<g/>
ef	ef	k?	ef
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
I_	I_	k1gMnSc1	I_
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
approx	approx	k1gInSc1	approx
0	[number]	k4	0
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
707	[number]	k4	707
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
I_	I_	k1gMnSc1	I_
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
≈	≈	k?	≈
</s>
</p>
<p>
<s>
0,707	[number]	k4	0,707
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
U_	U_	k1gFnSc6	U_
<g/>
{	{	kIx(	{
<g/>
ef	ef	k?	ef
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
U_	U_	k1gMnSc1	U_
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
approx	approx	k1gInSc1	approx
0	[number]	k4	0
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
707	[number]	k4	707
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
U_	U_	k1gMnSc1	U_
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
Im	Im	k1gFnSc1	Im
je	být	k5eAaImIp3nS	být
amplituda	amplituda	k1gFnSc1	amplituda
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
Um	uma	k1gFnPc2	uma
je	být	k5eAaImIp3nS	být
amplituda	amplituda	k1gFnSc1	amplituda
střídavého	střídavý	k2eAgNnSc2d1	střídavé
napětí	napětí	k1gNnSc2	napětí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
==	==	k?	==
</s>
</p>
<p>
<s>
Střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
vzniká	vznikat	k5eAaImIp3nS	vznikat
elektromagnetickou	elektromagnetický	k2eAgFnSc7d1	elektromagnetická
indukcí	indukce	k1gFnSc7	indukce
v	v	k7c6	v
generátoru	generátor	k1gInSc6	generátor
<g/>
,	,	kIx,	,
nazývaném	nazývaný	k2eAgInSc6d1	nazývaný
alternátor	alternátor	k1gInSc1	alternátor
<g/>
.	.	kIx.	.
</s>
<s>
Frekvence	frekvence	k1gFnSc1	frekvence
otáčení	otáčení	k1gNnSc2	otáčení
rotoru	rotor	k1gInSc2	rotor
v	v	k7c6	v
generátoru	generátor	k1gInSc6	generátor
určuje	určovat	k5eAaImIp3nS	určovat
frekvenci	frekvence	k1gFnSc4	frekvence
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
otáčení	otáčení	k1gNnSc1	otáčení
rotoru	rotor	k1gInSc2	rotor
děje	dít	k5eAaImIp3nS	dít
se	s	k7c7	s
stálou	stálý	k2eAgFnSc7d1	stálá
úhlovou	úhlový	k2eAgFnSc7d1	úhlová
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
má	mít	k5eAaImIp3nS	mít
harmonický	harmonický	k2eAgInSc4d1	harmonický
průběh	průběh	k1gInSc4	průběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dvoufázová	dvoufázový	k2eAgFnSc1d1	dvoufázová
soustava	soustava	k1gFnSc1	soustava
===	===	k?	===
</s>
</p>
<p>
<s>
Předchůdce	předchůdce	k1gMnSc1	předchůdce
třífázové	třífázový	k2eAgFnSc2d1	třífázová
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
ve	v	k7c6	v
Philadelphii	Philadelphia	k1gFnSc6	Philadelphia
</s>
</p>
<p>
<s>
===	===	k?	===
Trojfázová	trojfázový	k2eAgFnSc1d1	trojfázová
soustava	soustava	k1gFnSc1	soustava
===	===	k?	===
</s>
</p>
<p>
<s>
Střídavý	střídavý	k2eAgInSc1d1	střídavý
elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
pomocí	pomocí	k7c2	pomocí
synchronních	synchronní	k2eAgInPc2d1	synchronní
generátorů	generátor	k1gInPc2	generátor
(	(	kIx(	(
<g/>
alternátorů	alternátor	k1gInPc2	alternátor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
u	u	k7c2	u
dvoupólových	dvoupólový	k2eAgInPc2d1	dvoupólový
strojů	stroj	k1gInPc2	stroj
tři	tři	k4xCgFnPc1	tři
cívky	cívka	k1gFnPc1	cívka
navzájem	navzájem	k6eAd1	navzájem
otočené	otočený	k2eAgFnPc1d1	otočená
o	o	k7c4	o
120	[number]	k4	120
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Střídavé	střídavý	k2eAgNnSc1d1	střídavé
napětí	napětí	k1gNnSc1	napětí
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgNnSc1d1	vznikající
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
generátoru	generátor	k1gInSc6	generátor
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
trojfázové	trojfázový	k2eAgNnSc1d1	trojfázové
napětí	napětí	k1gNnSc1	napětí
nebo	nebo	k8xC	nebo
také	také	k9	také
třífázové	třífázový	k2eAgNnSc4d1	třífázové
napětí	napětí	k1gNnSc4	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Cívky	cívka	k1gFnPc1	cívka
generátoru	generátor	k1gInSc2	generátor
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zapojeny	zapojit	k5eAaPmNgFnP	zapojit
do	do	k7c2	do
hvězdy	hvězda	k1gFnSc2	hvězda
nebo	nebo	k8xC	nebo
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
generátor	generátor	k1gInSc1	generátor
má	mít	k5eAaImIp3nS	mít
pak	pak	k6eAd1	pak
tři	tři	k4xCgInPc4	tři
vývody	vývod	k1gInPc1	vývod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
fáze	fáze	k1gFnPc4	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
fází	fáze	k1gFnPc2	fáze
má	mít	k5eAaImIp3nS	mít
průběh	průběh	k1gInSc1	průběh
napětí	napětí	k1gNnSc2	napětí
proti	proti	k7c3	proti
sousedním	sousední	k2eAgFnPc3d1	sousední
fázím	fáze	k1gFnPc3	fáze
fázově	fázově	k6eAd1	fázově
posunut	posunut	k2eAgInSc4d1	posunut
o	o	k7c4	o
120	[number]	k4	120
stupňů	stupeň	k1gInPc2	stupeň
elektrických	elektrický	k2eAgInPc2d1	elektrický
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
alternátory	alternátor	k1gInPc1	alternátor
veřejné	veřejný	k2eAgFnSc2d1	veřejná
elektrické	elektrický	k2eAgFnSc2d1	elektrická
sítě	síť	k1gFnSc2	síť
pracují	pracovat	k5eAaImIp3nP	pracovat
navzájem	navzájem	k6eAd1	navzájem
synchronně	synchronně	k6eAd1	synchronně
s	s	k7c7	s
jmenovitou	jmenovitý	k2eAgFnSc7d1	jmenovitá
frekvencí	frekvence	k1gFnSc7	frekvence
50	[number]	k4	50
Hz	Hz	kA	Hz
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
nebo	nebo	k8xC	nebo
60	[number]	k4	60
Hz	Hz	kA	Hz
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
zapojení	zapojení	k1gNnSc6	zapojení
cívek	cívka	k1gFnPc2	cívka
do	do	k7c2	do
hvězdy	hvězda	k1gFnSc2	hvězda
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
kromě	kromě	k7c2	kromě
tří	tři	k4xCgFnPc2	tři
fází	fáze	k1gFnPc2	fáze
ještě	ještě	k9	ještě
vodič	vodič	k1gInSc4	vodič
s	s	k7c7	s
nulovým	nulový	k2eAgInSc7d1	nulový
elektrickým	elektrický	k2eAgInSc7d1	elektrický
potenciálem	potenciál	k1gInSc7	potenciál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
střední	střední	k2eAgInSc1d1	střední
pracovní	pracovní	k2eAgInSc1d1	pracovní
vodič	vodič	k1gInSc1	vodič
(	(	kIx(	(
<g/>
v	v	k7c6	v
žargonu	žargon	k1gInSc6	žargon
nulák	nulák	k1gInSc1	nulák
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
střed	střed	k1gInSc4	střed
sítě	síť	k1gFnSc2	síť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Elektrické	elektrický	k2eAgNnSc1d1	elektrické
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
středním	střední	k2eAgInSc7d1	střední
pracovním	pracovní	k2eAgInSc7d1	pracovní
vodičem	vodič	k1gInSc7	vodič
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
svorkou	svorka	k1gFnSc7	svorka
cívky	cívka	k1gFnSc2	cívka
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
fázové	fázový	k2eAgNnSc1d1	fázové
napětí	napětí	k1gNnSc1	napětí
<g/>
,	,	kIx,	,
napětí	napětí	k1gNnSc1	napětí
měřené	měřený	k2eAgNnSc1d1	měřené
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
libovolnými	libovolný	k2eAgFnPc7d1	libovolná
fázemi	fáze	k1gFnPc7	fáze
(	(	kIx(	(
<g/>
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
díky	díky	k7c3	díky
vzájemnému	vzájemný	k2eAgInSc3d1	vzájemný
fázovému	fázový	k2eAgInSc3d1	fázový
posunu	posun	k1gInSc3	posun
o	o	k7c4	o
120	[number]	k4	120
stupňů	stupeň	k1gInPc2	stupeň
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
sdružené	sdružený	k2eAgNnSc1d1	sdružené
napětí	napětí	k1gNnSc1	napětí
případně	případně	k6eAd1	případně
mezifázové	mezifázové	k2eAgNnSc2d1	mezifázové
napětí	napětí	k1gNnSc2	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
harmonický	harmonický	k2eAgInSc4d1	harmonický
průběh	průběh	k1gInSc4	průběh
platí	platit	k5eAaImIp3nS	platit
podle	podle	k7c2	podle
cosinové	cosinový	k2eAgFnSc2d1	cosinový
věty	věta	k1gFnSc2	věta
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
≈	≈	k?	≈
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
73	[number]	k4	73
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
U_	U_	k1gFnPc1	U_
<g/>
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
U_	U_	k1gMnSc1	U_
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
U_	U_	k1gMnSc1	U_
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
approx	approx	k1gInSc1	approx
1	[number]	k4	1
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
73	[number]	k4	73
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
U_	U_	k1gMnSc1	U_
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
<g/>
Velkou	velký	k2eAgFnSc7d1	velká
výhodou	výhoda	k1gFnSc7	výhoda
třífázové	třífázový	k2eAgFnSc2d1	třífázová
soustavy	soustava	k1gFnSc2	soustava
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
lineární	lineární	k2eAgFnSc6d1	lineární
a	a	k8xC	a
symetrické	symetrický	k2eAgFnSc6d1	symetrická
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
třech	tři	k4xCgFnPc6	tři
fázích	fáze	k1gFnPc6	fáze
stejné	stejná	k1gFnSc2	stejná
<g/>
)	)	kIx)	)
zátěži	zátěž	k1gFnSc3	zátěž
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
generátoru	generátor	k1gInSc2	generátor
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
okamžiku	okamžik	k1gInSc6	okamžik
periody	perioda	k1gFnSc2	perioda
odebírán	odebírán	k2eAgInSc4d1	odebírán
konstantní	konstantní	k2eAgInSc4d1	konstantní
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
nevznikají	vznikat	k5eNaImIp3nP	vznikat
momentové	momentový	k2eAgInPc4d1	momentový
rázy	ráz	k1gInPc4	ráz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výhody	výhoda	k1gFnPc1	výhoda
a	a	k8xC	a
nevýhody	nevýhoda	k1gFnPc1	nevýhoda
použití	použití	k1gNnSc2	použití
harmonických	harmonický	k2eAgNnPc2d1	harmonické
střídavých	střídavý	k2eAgNnPc2d1	střídavé
napětí	napětí	k1gNnPc2	napětí
a	a	k8xC	a
proudů	proud	k1gInPc2	proud
===	===	k?	===
</s>
</p>
<p>
<s>
Střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
kvůli	kvůli	k7c3	kvůli
snadnější	snadný	k2eAgFnSc3d2	snazší
výrobě	výroba	k1gFnSc3	výroba
v	v	k7c6	v
elektrárnách	elektrárna	k1gFnPc6	elektrárna
<g/>
,	,	kIx,	,
dálkovému	dálkový	k2eAgInSc3d1	dálkový
přenosu	přenos	k1gInSc3	přenos
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
kvůli	kvůli	k7c3	kvůli
snadnějšímu	snadný	k2eAgNnSc3d2	snazší
vypínání	vypínání	k1gNnSc3	vypínání
<g/>
.	.	kIx.	.
</s>
<s>
Snížení	snížení	k1gNnSc1	snížení
přenosových	přenosový	k2eAgFnPc2d1	přenosová
ztrát	ztráta	k1gFnPc2	ztráta
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
především	především	k6eAd1	především
transformací	transformace	k1gFnSc7	transformace
elektrického	elektrický	k2eAgNnSc2d1	elektrické
napětí	napětí	k1gNnSc2	napětí
na	na	k7c4	na
vysoké	vysoký	k2eAgNnSc4d1	vysoké
napětí	napětí	k1gNnSc4	napětí
nebo	nebo	k8xC	nebo
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgNnSc4d1	vysoké
napětí	napětí	k1gNnSc4	napětí
(	(	kIx(	(
<g/>
vyšší	vysoký	k2eAgFnSc1d2	vyšší
intenzita	intenzita	k1gFnSc1	intenzita
elektrického	elektrický	k2eAgNnSc2d1	elektrické
pole	pole	k1gNnSc2	pole
<g/>
)	)	kIx)	)
a	a	k8xC	a
nízký	nízký	k2eAgInSc1d1	nízký
proud	proud	k1gInSc1	proud
(	(	kIx(	(
<g/>
nižší	nízký	k2eAgFnSc1d2	nižší
magnetická	magnetický	k2eAgFnSc1d1	magnetická
indukce	indukce	k1gFnSc1	indukce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
snižují	snižovat	k5eAaImIp3nP	snižovat
provozní	provozní	k2eAgFnPc1d1	provozní
ztráty	ztráta	k1gFnPc1	ztráta
vzniklé	vzniklý	k2eAgFnPc1d1	vzniklá
zahříváním	zahřívání	k1gNnSc7	zahřívání
elektrického	elektrický	k2eAgNnSc2d1	elektrické
vedení	vedení	k1gNnSc2	vedení
vlivem	vlivem	k7c2	vlivem
elektromagnetické	elektromagnetický	k2eAgFnSc2d1	elektromagnetická
indukce	indukce	k1gFnSc2	indukce
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgNnSc1d2	vyšší
střídavé	střídavý	k2eAgNnSc1d1	střídavé
napětí	napětí	k1gNnSc1	napětí
přináší	přinášet	k5eAaImIp3nS	přinášet
také	také	k9	také
pořizovací	pořizovací	k2eAgFnPc4d1	pořizovací
úspory	úspora	k1gFnPc4	úspora
na	na	k7c6	na
průřezech	průřez	k1gInPc6	průřez
vodičů	vodič	k1gInPc2	vodič
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
především	především	k9	především
na	na	k7c6	na
mědi	měď	k1gFnSc6	měď
<g/>
,	,	kIx,	,
a	a	k8xC	a
následných	následný	k2eAgFnPc6d1	následná
lehčích	lehký	k2eAgFnPc6d2	lehčí
nosných	nosný	k2eAgFnPc6d1	nosná
konstrukcích	konstrukce	k1gFnPc6	konstrukce
vedení	vedení	k1gNnSc2	vedení
i	i	k8xC	i
dalších	další	k2eAgNnPc2d1	další
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Transformace	transformace	k1gFnSc1	transformace
napětí	napětí	k1gNnSc2	napětí
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
pomocí	pomocí	k7c2	pomocí
elektrických	elektrický	k2eAgInPc2d1	elektrický
transformátorů	transformátor	k1gInPc2	transformátor
napětí	napětí	k1gNnSc2	napětí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Výhody	výhoda	k1gFnPc4	výhoda
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
====	====	k?	====
</s>
</p>
<p>
<s>
Zásadní	zásadní	k2eAgFnSc7d1	zásadní
výhodou	výhoda	k1gFnSc7	výhoda
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
stejnosměrným	stejnosměrný	k2eAgInSc7d1	stejnosměrný
proudem	proud	k1gInSc7	proud
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
mnohem	mnohem	k6eAd1	mnohem
jednodušší	jednoduchý	k2eAgFnSc1d2	jednodušší
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
distribuce	distribuce	k1gFnSc1	distribuce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
(	(	kIx(	(
<g/>
skokově	skokově	k6eAd1	skokově
přepínané	přepínaný	k2eAgNnSc1d1	přepínané
<g/>
)	)	kIx)	)
zvyšování	zvyšování	k1gNnSc1	zvyšování
a	a	k8xC	a
snižování	snižování	k1gNnSc1	snižování
napětí	napětí	k1gNnSc2	napětí
pomocí	pomocí	k7c2	pomocí
transformátorů	transformátor	k1gInPc2	transformátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
proud	proud	k1gInSc1	proud
prochází	procházet	k5eAaImIp3nS	procházet
nulou	nula	k1gFnSc7	nula
každou	každý	k3xTgFnSc4	každý
půlperiodu	půlperioda	k1gFnSc4	půlperioda
<g/>
,	,	kIx,	,
vycházejí	vycházet	k5eAaImIp3nP	vycházet
přístroje	přístroj	k1gInPc1	přístroj
určené	určený	k2eAgInPc1d1	určený
k	k	k7c3	k
vypínání	vypínání	k1gNnSc3	vypínání
(	(	kIx(	(
<g/>
vypínače	vypínač	k1gInSc2	vypínač
a	a	k8xC	a
stykače	stykač	k1gInSc2	stykač
<g/>
)	)	kIx)	)
a	a	k8xC	a
ochraně	ochrana	k1gFnSc6	ochrana
(	(	kIx(	(
<g/>
pojistky	pojistka	k1gFnSc2	pojistka
<g/>
,	,	kIx,	,
jističe	jistič	k1gInSc2	jistič
a	a	k8xC	a
chrániče	chránič	k1gInSc2	chránič
<g/>
)	)	kIx)	)
silových	silový	k2eAgInPc2d1	silový
obvodů	obvod	k1gInPc2	obvod
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
konstrukčně	konstrukčně	k6eAd1	konstrukčně
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnPc1d2	menší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
generátory	generátor	k1gInPc1	generátor
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
stejnosměrných	stejnosměrný	k2eAgNnPc2d1	stejnosměrné
dynam	dynamo	k1gNnPc2	dynamo
<g/>
,	,	kIx,	,
nepoužívají	používat	k5eNaImIp3nP	používat
komutátory	komutátor	k1gInPc4	komutátor
(	(	kIx(	(
<g/>
mechanické	mechanický	k2eAgMnPc4d1	mechanický
střídače	střídač	k1gMnPc4	střídač
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jsou	být	k5eAaImIp3nP	být
jednodušší	jednoduchý	k2eAgInPc1d2	jednodušší
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
výroby	výroba	k1gFnSc2	výroba
i	i	k8xC	i
údržby	údržba	k1gFnSc2	údržba
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
distribuce	distribuce	k1gFnSc2	distribuce
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
stejnosměrný	stejnosměrný	k2eAgInSc1d1	stejnosměrný
proud	proud	k1gInSc1	proud
výjimečně	výjimečně	k6eAd1	výjimečně
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
pro	pro	k7c4	pro
masivní	masivní	k2eAgNnSc4d1	masivní
dálkové	dálkový	k2eAgNnSc4d1	dálkové
vedení	vedení	k1gNnSc4	vedení
750	[number]	k4	750
kV	kV	k?	kV
odlehlými	odlehlý	k2eAgFnPc7d1	odlehlá
a	a	k8xC	a
nepřístupnými	přístupný	k2eNgFnPc7d1	nepřístupná
oblastmi	oblast	k1gFnPc7	oblast
divočiny	divočina	k1gFnSc2	divočina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
potřeba	potřeba	k6eAd1	potřeba
jen	jen	k9	jen
jediný	jediný	k2eAgInSc1d1	jediný
vodič	vodič	k1gInSc1	vodič
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
v	v	k7c6	v
supravodivém	supravodivý	k2eAgNnSc6d1	supravodivé
provedení	provedení	k1gNnSc6	provedení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Nevýhody	nevýhoda	k1gFnPc4	nevýhoda
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
====	====	k?	====
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
nevýhody	nevýhoda	k1gFnPc1	nevýhoda
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
stejnosměrným	stejnosměrný	k2eAgInSc7d1	stejnosměrný
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
složitější	složitý	k2eAgFnSc1d2	složitější
rekuperace	rekuperace	k1gFnSc1	rekuperace
(	(	kIx(	(
<g/>
vracení	vracení	k1gNnSc1	vracení
energie	energie	k1gFnSc2	energie
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
<g/>
)	)	kIx)	)
–	–	k?	–
tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc4	problém
je	být	k5eAaImIp3nS	být
zmírněn	zmírnit	k5eAaPmNgInS	zmírnit
nástupem	nástup	k1gInSc7	nástup
pokročilé	pokročilý	k2eAgFnPc1d1	pokročilá
polovodičové	polovodičový	k2eAgFnPc1d1	polovodičová
techniky	technika	k1gFnPc1	technika
(	(	kIx(	(
<g/>
aplikace	aplikace	k1gFnSc1	aplikace
elektronických	elektronický	k2eAgMnPc2d1	elektronický
střídačů	střídač	k1gMnPc2	střídač
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nutnost	nutnost	k1gFnSc4	nutnost
plně	plně	k6eAd1	plně
synchronizovat	synchronizovat	k5eAaBmF	synchronizovat
všechny	všechen	k3xTgInPc4	všechen
elektrické	elektrický	k2eAgInPc4d1	elektrický
generátory	generátor	k1gInPc4	generátor
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
síti	síť	k1gFnSc6	síť
</s>
</p>
<p>
<s>
nutnost	nutnost	k1gFnSc4	nutnost
vyvažovat	vyvažovat	k5eAaImF	vyvažovat
nejen	nejen	k6eAd1	nejen
toky	tok	k1gInPc4	tok
samotného	samotný	k2eAgInSc2d1	samotný
činného	činný	k2eAgInSc2d1	činný
výkonu	výkon	k1gInSc2	výkon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jalového	jalový	k2eAgNnSc2d1	jalové
</s>
</p>
<p>
<s>
podstatný	podstatný	k2eAgInSc4d1	podstatný
vliv	vliv	k1gInSc4	vliv
rozložených	rozložený	k2eAgInPc2d1	rozložený
liniových	liniový	k2eAgInPc2d1	liniový
parametrů	parametr	k1gInPc2	parametr
vedení	vedení	k1gNnSc2	vedení
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
parazitní	parazitní	k2eAgFnSc1d1	parazitní
indukčnost	indukčnost	k1gFnSc1	indukčnost
a	a	k8xC	a
kapacita	kapacita	k1gFnSc1	kapacita
</s>
</p>
<p>
<s>
==	==	k?	==
Důvody	důvod	k1gInPc7	důvod
střídavého	střídavý	k2eAgNnSc2d1	střídavé
elektrického	elektrický	k2eAgNnSc2d1	elektrické
napájení	napájení	k1gNnSc2	napájení
==	==	k?	==
</s>
</p>
<p>
<s>
Střídavá	střídavý	k2eAgFnSc1d1	střídavá
forma	forma	k1gFnSc1	forma
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
její	její	k3xOp3gInSc4	její
přenos	přenos	k1gInSc4	přenos
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
podstatného	podstatný	k2eAgNnSc2d1	podstatné
snížení	snížení	k1gNnSc2	snížení
ztrát	ztráta	k1gFnPc2	ztráta
na	na	k7c6	na
vedení	vedení	k1gNnSc6	vedení
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
podstatného	podstatný	k2eAgNnSc2d1	podstatné
snížení	snížení	k1gNnSc2	snížení
pořizovacích	pořizovací	k2eAgInPc2d1	pořizovací
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
dálkové	dálkový	k2eAgInPc4d1	dálkový
rozvody	rozvod	k1gInPc4	rozvod
<g/>
,	,	kIx,	,
<g/>
obojí	obojí	k4xRgNnPc4	obojí
snížením	snížení	k1gNnSc7	snížení
velikosti	velikost	k1gFnSc2	velikost
přenášeného	přenášený	k2eAgInSc2d1	přenášený
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Transformace	transformace	k1gFnSc1	transformace
při	při	k7c6	při
rozvodu	rozvod	k1gInSc6	rozvod
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zvýšení	zvýšení	k1gNnSc6	zvýšení
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
přenosu	přenos	k1gInSc2	přenos
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgNnSc1d1	vysoké
napětí	napětí	k1gNnSc1	napětí
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
nadřazená	nadřazený	k2eAgFnSc1d1	nadřazená
soutava	soutava	k1gFnSc1	soutava
(	(	kIx(	(
<g/>
VVN	VVN	kA	VVN
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
o	o	k7c6	o
napětí	napětí	k1gNnSc6	napětí
110	[number]	k4	110
kV	kV	k?	kV
<g/>
,	,	kIx,	,
220	[number]	k4	220
kV	kV	k?	kV
a	a	k8xC	a
400	[number]	k4	400
kV	kV	k?	kV
<g/>
)	)	kIx)	)
opět	opět	k6eAd1	opět
snižuje	snižovat	k5eAaImIp3nS	snižovat
v	v	k7c6	v
rozvodnách	rozvodna	k1gFnPc6	rozvodna
VVN	VVN	kA	VVN
<g/>
/	/	kIx~	/
<g/>
VN	VN	kA	VN
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
transformovnách	transformovna	k1gFnPc6	transformovna
na	na	k7c4	na
vysoké	vysoký	k2eAgNnSc4d1	vysoké
napětí	napětí	k1gNnSc4	napětí
(	(	kIx(	(
<g/>
VN	VN	kA	VN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
snižuje	snižovat	k5eAaImIp3nS	snižovat
v	v	k7c6	v
distribučních	distribuční	k2eAgInPc6d1	distribuční
transformátorech	transformátor	k1gInPc6	transformátor
na	na	k7c4	na
nízké	nízký	k2eAgNnSc4d1	nízké
napětí	napětí	k1gNnSc4	napětí
(	(	kIx(	(
<g/>
NN	NN	kA	NN
<g/>
)	)	kIx)	)
s	s	k7c7	s
efektivní	efektivní	k2eAgFnSc7d1	efektivní
hodnotou	hodnota	k1gFnSc7	hodnota
mezifázového	mezifázový	k2eAgNnSc2d1	mezifázový
sdruženého	sdružený	k2eAgNnSc2d1	sdružené
napětí	napětí	k1gNnSc2	napětí
definovanou	definovaný	k2eAgFnSc7d1	definovaná
na	na	k7c6	na
přesně	přesně	k6eAd1	přesně
400	[number]	k4	400
V	V	kA	V
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
0,4	[number]	k4	0,4
kV	kV	k?	kV
<g/>
,	,	kIx,	,
s	s	k7c7	s
mezními	mezní	k2eAgFnPc7d1	mezní
tolerancemi	tolerance	k1gFnPc7	tolerance
+5	+5	k4	+5
%	%	kIx~	%
−	−	k?	−
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zápis	zápis	k1gInSc1	zápis
na	na	k7c6	na
transformovně	transformovna	k1gFnSc6	transformovna
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
VN	VN	kA	VN
<g/>
/	/	kIx~	/
<g/>
NN	NN	kA	NN
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
×	×	k?	×
22	[number]	k4	22
<g/>
/	/	kIx~	/
<g/>
0,4	[number]	k4	0,4
kV	kV	k?	kV
<g/>
,	,	kIx,	,
50	[number]	k4	50
HzDíky	HzDík	k1gInPc1	HzDík
n-násobnému	násobný	k2eAgNnSc3d1	n-násobný
snížení	snížení	k1gNnSc3	snížení
napětí	napětí	k1gNnSc2	napětí
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
NN	NN	kA	NN
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
stejného	stejný	k2eAgInSc2d1	stejný
výkonu	výkon	k1gInSc2	výkon
nutno	nutno	k6eAd1	nutno
přenášet	přenášet	k5eAaImF	přenášet
adekvátně	adekvátně	k6eAd1	adekvátně
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
proud	proud	k1gInSc4	proud
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
při	při	k7c6	při
dodržení	dodržení	k1gNnSc6	dodržení
stejné	stejný	k2eAgFnSc2d1	stejná
proudové	proudový	k2eAgFnSc2d1	proudová
hustoty	hustota	k1gFnSc2	hustota
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
n	n	k0	n
<g/>
×	×	k?	×
krát	krát	k6eAd1	krát
větší	veliký	k2eAgInSc1d2	veliký
průřez	průřez	k1gInSc1	průřez
vodičů	vodič	k1gInPc2	vodič
a	a	k8xC	a
jemu	on	k3xPp3gInSc3	on
odpovídající	odpovídající	k2eAgFnPc4d1	odpovídající
vyšší	vysoký	k2eAgNnPc4d2	vyšší
množství	množství	k1gNnPc4	množství
vodivého	vodivý	k2eAgInSc2d1	vodivý
materiálu	materiál	k1gInSc2	materiál
v	v	k7c6	v
kabelech	kabel	k1gInPc6	kabel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Trojfázový	trojfázový	k2eAgInSc1d1	trojfázový
rozvod	rozvod	k1gInSc1	rozvod
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
elektromotory	elektromotor	k1gInPc4	elektromotor
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
točivé	točivý	k2eAgInPc4d1	točivý
elektrické	elektrický	k2eAgInPc4d1	elektrický
stroje	stroj	k1gInPc4	stroj
obecně	obecně	k6eAd1	obecně
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přirozené	přirozený	k2eAgNnSc1d1	přirozené
používat	používat	k5eAaImF	používat
trojfázový	trojfázový	k2eAgInSc4d1	trojfázový
rozvod	rozvod	k1gInSc4	rozvod
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yQgInSc3	který
pak	pak	k6eAd1	pak
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
může	moct	k5eAaImIp3nS	moct
vznikat	vznikat	k5eAaImF	vznikat
točivé	točivý	k2eAgNnSc4d1	točivé
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Trojfázový	trojfázový	k2eAgInSc1d1	trojfázový
stroj	stroj	k1gInSc1	stroj
totiž	totiž	k9	totiž
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
točivého	točivý	k2eAgNnSc2d1	točivé
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
"	"	kIx"	"
<g/>
nulák	nulák	k1gInSc1	nulák
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
navíc	navíc	k6eAd1	navíc
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
nemusí	muset	k5eNaImIp3nP	muset
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Výroba	výroba	k1gFnSc1	výroba
====	====	k?	====
</s>
</p>
<p>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
elektrárnách	elektrárna	k1gFnPc6	elektrárna
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
generátory	generátor	k1gInPc4	generátor
(	(	kIx(	(
<g/>
alternátory	alternátor	k1gInPc4	alternátor
<g/>
)	)	kIx)	)
svá	svůj	k3xOyFgNnPc4	svůj
vlastní	vlastní	k2eAgNnPc4d1	vlastní
jmenovitá	jmenovitý	k2eAgNnPc4d1	jmenovité
napětí	napětí	k1gNnSc4	napětí
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
3	[number]	k4	3
kV	kV	k?	kV
<g/>
,	,	kIx,	,
a	a	k8xC	a
do	do	k7c2	do
standardizované	standardizovaný	k2eAgFnSc2d1	standardizovaná
sítě	síť	k1gFnSc2	síť
VN	VN	kA	VN
se	se	k3xPyFc4	se
připojují	připojovat	k5eAaImIp3nP	připojovat
přifázované	přifázovaný	k2eAgInPc1d1	přifázovaný
a	a	k8xC	a
přes	přes	k7c4	přes
přesně	přesně	k6eAd1	přesně
nastavené	nastavený	k2eAgFnPc4d1	nastavená
odbočky	odbočka	k1gFnPc4	odbočka
transformátorů	transformátor	k1gInPc2	transformátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zvýšení	zvýšení	k1gNnSc6	zvýšení
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
přenosu	přenos	k1gInSc2	přenos
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgNnSc1d1	vysoké
napětí	napětí	k1gNnSc1	napětí
(	(	kIx(	(
<g/>
VVN	VVN	kA	VVN
<g/>
)	)	kIx)	)
opět	opět	k6eAd1	opět
snižuje	snižovat	k5eAaImIp3nS	snižovat
v	v	k7c6	v
rozvodnách	rozvodna	k1gFnPc6	rozvodna
a	a	k8xC	a
transformovnách	transformovna	k1gFnPc6	transformovna
na	na	k7c6	na
vysoké	vysoká	k1gFnSc6	vysoká
napětí	napětí	k1gNnSc2	napětí
(	(	kIx(	(
<g/>
VN	VN	kA	VN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rozvody	rozvod	k1gInPc1	rozvod
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
rozvodná	rozvodný	k2eAgFnSc1d1	rozvodná
síť	síť	k1gFnSc1	síť
o	o	k7c6	o
frekvenci	frekvence	k1gFnSc6	frekvence
50	[number]	k4	50
Hz	Hz	kA	Hz
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
například	například	k6eAd1	například
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
používá	používat	k5eAaImIp3nS	používat
elektrický	elektrický	k2eAgInSc1d1	elektrický
rozvod	rozvod	k1gInSc1	rozvod
o	o	k7c6	o
fázovém	fázový	k2eAgNnSc6d1	fázové
napětí	napětí	k1gNnSc6	napětí
120	[number]	k4	120
V	V	kA	V
při	při	k7c6	při
frekvenci	frekvence	k1gFnSc6	frekvence
60	[number]	k4	60
Hz	Hz	kA	Hz
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
s	s	k7c7	s
jinak	jinak	k6eAd1	jinak
definovanou	definovaný	k2eAgFnSc7d1	definovaná
neutrální	neutrální	k2eAgFnSc7d1	neutrální
zemí	zem	k1gFnSc7	zem
<g/>
:	:	kIx,	:
Mimo	mimo	k7c4	mimo
střední	střední	k2eAgInSc4d1	střední
vodič	vodič	k1gInSc4	vodič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jednofázový	jednofázový	k2eAgInSc1d1	jednofázový
rozvod	rozvod	k1gInSc1	rozvod
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
se	se	k3xPyFc4	se
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
spíše	spíše	k9	spíše
jednofázové	jednofázový	k2eAgNnSc1d1	jednofázové
napájení	napájení	k1gNnSc1	napájení
s	s	k7c7	s
efektivní	efektivní	k2eAgFnSc7d1	efektivní
hodnotou	hodnota	k1gFnSc7	hodnota
fázového	fázový	k2eAgNnSc2d1	fázové
napětí	napětí	k1gNnSc2	napětí
cca	cca	kA	cca
230	[number]	k4	230
V	V	kA	V
proti	proti	k7c3	proti
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hodnota	hodnota	k1gFnSc1	hodnota
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
právě	právě	k9	právě
z	z	k7c2	z
hodnoty	hodnota	k1gFnSc2	hodnota
400	[number]	k4	400
V	V	kA	V
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sdružené	sdružený	k2eAgNnSc1d1	sdružené
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
fázory	fázor	k1gInPc7	fázor
rotujícími	rotující	k2eAgInPc7d1	rotující
v	v	k7c6	v
rovnostranném	rovnostranný	k2eAgInSc6d1	rovnostranný
trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
jednoznačně	jednoznačně	k6eAd1	jednoznačně
určuje	určovat	k5eAaImIp3nS	určovat
hodnotu	hodnota	k1gFnSc4	hodnota
mezi	mezi	k7c7	mezi
vrcholem	vrchol	k1gInSc7	vrchol
a	a	k8xC	a
středem	střed	k1gInSc7	střed
umělé	umělý	k2eAgFnSc2d1	umělá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Fázové	fázový	k2eAgNnSc1d1	fázové
napětí	napětí	k1gNnSc1	napětí
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
vypočítat	vypočítat	k5eAaPmF	vypočítat
z	z	k7c2	z
kosinové	kosinový	k2eAgFnSc2d1	kosinová
věty	věta	k1gFnSc2	věta
<g/>
:	:	kIx,	:
Fázové	fázový	k2eAgNnSc1d1	fázové
napětí	napětí	k1gNnSc1	napětí
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
ze	z	k7c2	z
sdruženého	sdružený	k2eAgNnSc2d1	sdružené
zmenšeného	zmenšený	k2eAgNnSc2d1	zmenšené
odmocninou	odmocnina	k1gFnSc7	odmocnina
čísla	číslo	k1gNnSc2	číslo
3	[number]	k4	3
<g/>
,	,	kIx,	,
cca	cca	kA	cca
1,73	[number]	k4	1,73
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rozvody	rozvod	k1gInPc1	rozvod
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
bytovém	bytový	k2eAgInSc6d1	bytový
rozvodu	rozvod	k1gInSc6	rozvod
je	být	k5eAaImIp3nS	být
předepsáno	předepsán	k2eAgNnSc1d1	předepsáno
vytvořit	vytvořit	k5eAaPmF	vytvořit
několik	několik	k4yIc4	několik
navzájem	navzájem	k6eAd1	navzájem
nezávislých	závislý	k2eNgInPc2d1	nezávislý
napájecích	napájecí	k2eAgInPc2d1	napájecí
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
zásuvky	zásuvka	k1gFnSc2	zásuvka
<g/>
,	,	kIx,	,
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
elektrický	elektrický	k2eAgInSc4d1	elektrický
sporák	sporák	k1gInSc4	sporák
<g/>
,	,	kIx,	,
automatická	automatický	k2eAgFnSc1d1	automatická
pračka	pračka	k1gFnSc1	pračka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
připojovat	připojovat	k5eAaImF	připojovat
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
rovnoměrnějšího	rovnoměrný	k2eAgNnSc2d2	rovnoměrnější
zatížení	zatížení	k1gNnSc2	zatížení
každý	každý	k3xTgInSc1	každý
k	k	k7c3	k
jiné	jiný	k2eAgFnSc3d1	jiná
fázi	fáze	k1gFnSc3	fáze
rozvodné	rozvodný	k2eAgFnSc2d1	rozvodná
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Napájení	napájení	k1gNnSc4	napájení
drobné	drobný	k2eAgFnSc2d1	drobná
elektroniky	elektronika	k1gFnSc2	elektronika
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
převážné	převážný	k2eAgFnSc6d1	převážná
většině	většina	k1gFnSc6	většina
spotřební	spotřební	k2eAgFnSc2d1	spotřební
elektroniky	elektronika	k1gFnSc2	elektronika
(	(	kIx(	(
<g/>
počítač	počítač	k1gInSc1	počítač
<g/>
,	,	kIx,	,
rozhlas	rozhlas	k1gInSc1	rozhlas
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
bezpečné	bezpečný	k2eAgNnSc1d1	bezpečné
malé	malé	k1gNnSc1	malé
(	(	kIx(	(
<g/>
MN	MN	kA	MN
<g/>
)	)	kIx)	)
stejnosměrné	stejnosměrný	k2eAgNnSc1d1	stejnosměrné
napětí	napětí	k1gNnSc1	napětí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
typicky	typicky	k6eAd1	typicky
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
buď	buď	k8xC	buď
pomocí	pomocí	k7c2	pomocí
domácích	domácí	k2eAgInPc2d1	domácí
transformátorků	transformátorek	k1gInPc2	transformátorek
(	(	kIx(	(
<g/>
adaptérů	adaptér	k1gMnPc2	adaptér
<g/>
)	)	kIx)	)
s	s	k7c7	s
usměrňovači	usměrňovač	k1gInPc7	usměrňovač
nebo	nebo	k8xC	nebo
pomocí	pomoc	k1gFnSc7	pomoc
spínaných	spínaný	k2eAgInPc2d1	spínaný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
spotřebiče	spotřebič	k1gInPc1	spotřebič
tedy	tedy	k9	tedy
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
napájeny	napájet	k5eAaImNgInP	napájet
i	i	k9	i
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
stejnosměrného	stejnosměrný	k2eAgNnSc2d1	stejnosměrné
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
auto	auto	k1gNnSc1	auto
<g/>
)	)	kIx)	)
<g/>
baterie	baterie	k1gFnPc1	baterie
<g/>
,	,	kIx,	,
suché	suchý	k2eAgInPc1d1	suchý
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
dynama	dynamo	k1gNnPc1	dynamo
<g/>
,	,	kIx,	,
fotovoltaické	fotovoltaický	k2eAgInPc1d1	fotovoltaický
články	článek	k1gInPc1	článek
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc1	použití
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
==	==	k?	==
</s>
</p>
<p>
<s>
Střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
běžných	běžný	k2eAgInPc6d1	běžný
domácích	domácí	k2eAgInPc6d1	domácí
elektrických	elektrický	k2eAgInPc6d1	elektrický
spotřebičích	spotřebič	k1gInPc6	spotřebič
(	(	kIx(	(
<g/>
žárovka	žárovka	k1gFnSc1	žárovka
<g/>
,	,	kIx,	,
zářivka	zářivka	k1gFnSc1	zářivka
<g/>
,	,	kIx,	,
spotřebiče	spotřebič	k1gInPc1	spotřebič
používající	používající	k2eAgInSc1d1	používající
elektromotor	elektromotor	k1gInSc4	elektromotor
<g/>
,	,	kIx,	,
elektrická	elektrický	k2eAgNnPc1d1	elektrické
topidla	topidlo	k1gNnPc1	topidlo
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
trakci	trakce	k1gFnSc6	trakce
===	===	k?	===
</s>
</p>
<p>
<s>
Zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
kapitolu	kapitola	k1gFnSc4	kapitola
tvoří	tvořit	k5eAaImIp3nS	tvořit
speciální	speciální	k2eAgInPc4d1	speciální
typy	typ	k1gInPc4	typ
trakčního	trakční	k2eAgNnSc2d1	trakční
napájení	napájení	k1gNnSc2	napájení
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
stejnosměrného	stejnosměrný	k2eAgNnSc2d1	stejnosměrné
napájení	napájení	k1gNnSc2	napájení
trakčních	trakční	k2eAgNnPc2d1	trakční
vozidel	vozidlo	k1gNnPc2	vozidlo
(	(	kIx(	(
<g/>
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
<g/>
,	,	kIx,	,
trolejbusů	trolejbus	k1gInPc2	trolejbus
<g/>
,	,	kIx,	,
vozů	vůz	k1gInPc2	vůz
metra	metro	k1gNnSc2	metro
či	či	k8xC	či
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
běžná	běžný	k2eAgFnSc1d1	běžná
frekvence	frekvence	k1gFnSc1	frekvence
50	[number]	k4	50
Hz	Hz	kA	Hz
při	při	k7c6	při
speciálním	speciální	k2eAgNnSc6d1	speciální
zapojení	zapojení	k1gNnSc6	zapojení
fází	fáze	k1gFnPc2	fáze
střídavého	střídavý	k2eAgNnSc2d1	střídavé
napájení	napájení	k1gNnSc2	napájení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
železnice	železnice	k1gFnSc1	železnice
používá	používat	k5eAaImIp3nS	používat
třetinová	třetinový	k2eAgFnSc1d1	třetinová
frekvence	frekvence	k1gFnSc1	frekvence
16	[number]	k4	16
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tfrac	tfrac	k1gInSc1	tfrac
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
Hz	Hz	kA	Hz
(	(	kIx(	(
<g/>
16,6	[number]	k4	16,6
<g/>
Hz	Hz	kA	Hz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
frekvence	frekvence	k1gFnSc1	frekvence
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
pro	pro	k7c4	pro
možnost	možnost	k1gFnSc4	možnost
jednoduchého	jednoduchý	k2eAgNnSc2d1	jednoduché
odvození	odvození	k1gNnSc2	odvození
od	od	k7c2	od
frekvence	frekvence	k1gFnSc2	frekvence
50	[number]	k4	50
Hz	Hz	kA	Hz
v	v	k7c6	v
rotačních	rotační	k2eAgInPc6d1	rotační
měničích	měnič	k1gInPc6	měnič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
napájecí	napájecí	k2eAgFnPc4d1	napájecí
sítě	síť	k1gFnPc4	síť
normována	normován	k2eAgFnSc1d1	normována
frekvence	frekvence	k1gFnSc1	frekvence
16,7	[number]	k4	16,7
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
===	===	k?	===
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
typy	typ	k1gInPc1	typ
speciálního	speciální	k2eAgNnSc2d1	speciální
použití	použití	k1gNnSc2	použití
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
:	:	kIx,	:
Kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
v	v	k7c6	v
hutnictví	hutnictví	k1gNnSc6	hutnictví
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používají	používat	k5eAaImIp3nP	používat
obloukové	obloukový	k2eAgFnPc1d1	oblouková
pece	pec	k1gFnPc1	pec
s	s	k7c7	s
běžnou	běžný	k2eAgFnSc7d1	běžná
frekvencí	frekvence	k1gFnSc7	frekvence
50	[number]	k4	50
Hz	Hz	kA	Hz
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jsou	být	k5eAaImIp3nP	být
jedny	jeden	k4xCgInPc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
spotřebičů	spotřebič	k1gInPc2	spotřebič
zapojených	zapojený	k2eAgInPc2d1	zapojený
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Početně	početně	k6eAd1	početně
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
a	a	k8xC	a
nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
skupinu	skupina	k1gFnSc4	skupina
elektrospotřebičů	elektrospotřebič	k1gInPc2	elektrospotřebič
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
tvoří	tvořit	k5eAaImIp3nP	tvořit
třífázové	třífázový	k2eAgInPc1d1	třífázový
asynchronní	asynchronní	k2eAgInPc1d1	asynchronní
motory	motor	k1gInPc1	motor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pohání	pohánět	k5eAaImIp3nP	pohánět
naprostou	naprostý	k2eAgFnSc4d1	naprostá
většinu	většina	k1gFnSc4	většina
běžných	běžný	k2eAgInPc2d1	běžný
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
strojních	strojní	k2eAgInPc2d1	strojní
mechanismů	mechanismus	k1gInPc2	mechanismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Palubní	palubní	k2eAgFnSc2d1	palubní
sítě	síť	k1gFnSc2	síť
===	===	k?	===
</s>
</p>
<p>
<s>
Střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
i	i	k9	i
v	v	k7c6	v
palubních	palubní	k2eAgFnPc6d1	palubní
sítích	síť	k1gFnPc6	síť
některých	některý	k3yIgMnPc2	některý
dopravních	dopravní	k2eAgMnPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
(	(	kIx(	(
<g/>
letadla	letadlo	k1gNnPc1	letadlo
<g/>
,	,	kIx,	,
lodě	loď	k1gFnPc1	loď
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
používáno	používán	k2eAgNnSc1d1	používáno
střídavé	střídavý	k2eAgNnSc1d1	střídavé
napájení	napájení	k1gNnSc1	napájení
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
frekvencí	frekvence	k1gFnSc7	frekvence
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
400	[number]	k4	400
Hz	Hz	kA	Hz
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
snížení	snížení	k1gNnSc4	snížení
ztrát	ztráta	k1gFnPc2	ztráta
a	a	k8xC	a
zmenšení	zmenšení	k1gNnSc2	zmenšení
rozměrů	rozměr	k1gInPc2	rozměr
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
transformátorů	transformátor	k1gInPc2	transformátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Označení	označení	k1gNnSc1	označení
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
schématech	schéma	k1gNnPc6	schéma
elektrických	elektrický	k2eAgInPc2d1	elektrický
obvodů	obvod	k1gInPc2	obvod
se	se	k3xPyFc4	se
střídavý	střídavý	k2eAgInSc1d1	střídavý
proud	proud	k1gInSc1	proud
označuje	označovat	k5eAaImIp3nS	označovat
vlnovkou	vlnovka	k1gFnSc7	vlnovka
~	~	kIx~	~
<g/>
.	.	kIx.	.
</s>
<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
zkratka	zkratka	k1gFnSc1	zkratka
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
je	být	k5eAaImIp3nS	být
AC	AC	kA	AC
(	(	kIx(	(
<g/>
alternating	alternating	k1gInSc1	alternating
current	current	k1gInSc1	current
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Stejnosměrný	stejnosměrný	k2eAgInSc1d1	stejnosměrný
proud	proud	k1gInSc1	proud
</s>
</p>
<p>
<s>
Elektřina	elektřina	k1gFnSc1	elektřina
</s>
</p>
<p>
<s>
Transformátor	transformátor	k1gMnSc1	transformátor
</s>
</p>
<p>
<s>
Alternátor	alternátor	k1gInSc1	alternátor
</s>
</p>
<p>
<s>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
</s>
</p>
<p>
<s>
Telegrafní	telegrafní	k2eAgFnPc1d1	telegrafní
rovnice	rovnice	k1gFnPc1	rovnice
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
střídavý	střídavý	k2eAgInSc4d1	střídavý
proud	proud	k1gInSc4	proud
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Trojfázová	trojfázový	k2eAgFnSc1d1	trojfázová
soustava	soustava	k1gFnSc1	soustava
(	(	kIx(	(
<g/>
krynicky	krynicky	k6eAd1	krynicky
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
</s>
</p>
