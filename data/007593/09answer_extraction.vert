velký	velký	k2eAgInSc4d1	velký
a	a	k8xC	a
malý	malý	k2eAgInSc4d1	malý
státní	státní	k2eAgInSc4d1	státní
znak	znak	k1gInSc4	znak
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
(	(	kIx(	(
<g/>
republika	republika	k1gFnSc1	republika
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
federace	federace	k1gFnSc2	federace
převzala	převzít	k5eAaPmAgFnS	převzít
i	i	k9	i
původní	původní	k2eAgFnSc4d1	původní
vlajku	vlajka	k1gFnSc4	vlajka
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Slovensko	Slovensko	k1gNnSc1	Slovensko
nemělo	mít	k5eNaImAgNnS	mít
zájem	zájem	k1gInSc4	zájem
tuto	tento	k3xDgFnSc4	tento
vlajku	vlajka	k1gFnSc4	vlajka
použít	použít	k5eAaPmF	použít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
standarta	standarta	k1gFnSc1	standarta
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc4d1	státní
pečeť	pečeť	k1gFnSc4	pečeť
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnPc4d1	státní
barvy	barva	k1gFnPc4	barva
a	a	k8xC	a
státní	státní	k2eAgFnSc1d1	státní
hymna	hymna	k1gFnSc1	hymna
Kde	kde	k6eAd1	kde
domov	domov	k1gInSc1	domov
můj	můj	k3xOp1gInSc1	můj
