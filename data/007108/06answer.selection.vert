<s>
Plazma	plazma	k1gFnSc1	plazma
je	být	k5eAaImIp3nS	být
čtvrté	čtvrtý	k4xOgNnSc1	čtvrtý
skupenství	skupenství	k1gNnSc1	skupenství
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
také	také	k9	také
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
forma	forma	k1gFnSc1	forma
látky	látka	k1gFnSc2	látka
–	–	k?	–
tvoří	tvořit	k5eAaImIp3nS	tvořit
až	až	k9	až
99	[number]	k4	99
%	%	kIx~	%
pozorované	pozorovaný	k2eAgFnSc2d1	pozorovaná
atomární	atomární	k2eAgFnSc2d1	atomární
hmoty	hmota	k1gFnSc2	hmota
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
