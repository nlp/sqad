<s>
V	v	k7c6
letectví	letectví	k1gNnSc6
je	být	k5eAaImIp3nS
maximální	maximální	k2eAgFnSc1d1
vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
(	(	kIx(
<g/>
angl.	angl.	k?
zkratka	zkratka	k1gFnSc1
MTOW	MTOW	kA
<g/>
,	,	kIx,
též	též	k9
nepřesně	přesně	k6eNd1
vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
)	)	kIx)
definována	definován	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
maximální	maximální	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
letadla	letadlo	k1gNnSc2
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
je	být	k5eAaImIp3nS
schopno	schopen	k2eAgNnSc1d1
uskutečnit	uskutečnit	k5eAaPmF
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>