<s>
Maximální	maximální	k2eAgFnSc1d1
vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
Součásti	součást	k1gFnPc1
vzletové	vzletový	k2eAgFnSc2d1
hmotnosti	hmotnost	k1gFnSc2
</s>
<s>
V	v	k7c6
letectví	letectví	k1gNnSc6
je	být	k5eAaImIp3nS
maximální	maximální	k2eAgFnSc1d1
vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
(	(	kIx(
<g/>
angl.	angl.	k?
zkratka	zkratka	k1gFnSc1
MTOW	MTOW	kA
<g/>
,	,	kIx,
též	též	k9
nepřesně	přesně	k6eNd1
vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
)	)	kIx)
definována	definován	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
maximální	maximální	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
letadla	letadlo	k1gNnSc2
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
je	být	k5eAaImIp3nS
schopno	schopen	k2eAgNnSc1d1
uskutečnit	uskutečnit	k5eAaPmF
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecně	obecně	k6eAd1
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
součtem	součet	k1gInSc7
hmotností	hmotnost	k1gFnSc7
prázdného	prázdný	k2eAgNnSc2d1
letadla	letadlo	k1gNnSc2
včetně	včetně	k7c2
provozních	provozní	k2eAgFnPc2d1
náplní	náplň	k1gFnPc2
<g/>
,	,	kIx,
posádky	posádka	k1gFnSc2
<g/>
,	,	kIx,
nákladu	náklad	k1gInSc2
a	a	k8xC
paliva	palivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
letadel	letadlo	k1gNnPc2
z	z	k7c2
počátků	počátek	k1gInPc2
letectví	letectví	k1gNnSc2
byla	být	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
hmotnost	hmotnost	k1gFnSc1
limitována	limitovat	k5eAaBmNgFnS
pouze	pouze	k6eAd1
výkony	výkon	k1gInPc1
letadla	letadlo	k1gNnSc2
<g/>
;	;	kIx,
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
maximální	maximální	k2eAgFnSc1d1
vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
omezena	omezit	k5eAaPmNgFnS
leteckými	letecký	k2eAgInPc7d1
předpisy	předpis	k1gInPc7
jako	jako	k8xC,k8xS
maximální	maximální	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
,	,	kIx,
při	pře	k1gFnSc4
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
letadlo	letadlo	k1gNnSc1
splňuje	splňovat	k5eAaImIp3nS
požadavky	požadavek	k1gInPc4
na	na	k7c4
bezpečný	bezpečný	k2eAgInSc4d1
let	let	k1gInSc4
definované	definovaný	k2eAgFnPc4d1
předpisem	předpis	k1gInSc7
pro	pro	k7c4
určenou	určený	k2eAgFnSc4d1
kategorii	kategorie	k1gFnSc4
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
