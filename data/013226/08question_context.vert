<s>
Fitz	Fitz	k1gInSc1	Fitz
and	and	k?	and
the	the	k?	the
Tantrums	Tantrumsa	k1gFnPc2	Tantrumsa
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
založil	založit	k5eAaPmAgMnS	založit
zpěvák	zpěvák	k1gMnSc1	zpěvák
Michael	Michael	k1gMnSc1	Michael
Fitzpatrick	Fitzpatrick	k1gMnSc1	Fitzpatrick
a	a	k8xC	a
přidali	přidat	k5eAaPmAgMnP	přidat
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Noelle	Noelle	k1gFnSc2	Noelle
Scaggs	Scaggsa	k1gFnPc2	Scaggsa
a	a	k8xC	a
bubeník	bubeník	k1gMnSc1	bubeník
John	John	k1gMnSc1	John
Wicks	Wicksa	k1gFnPc2	Wicksa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mu	on	k3xPp3gMnSc3	on
doporučil	doporučit	k5eAaPmAgMnS	doporučit
jeho	jeho	k3xOp3gMnSc1	jeho
kamarád	kamarád	k1gMnSc1	kamarád
<g/>
,	,	kIx,	,
saxofonista	saxofonista	k1gMnSc1	saxofonista
James	James	k1gMnSc1	James
King	King	k1gMnSc1	King
<g/>
.	.	kIx.	.
</s>

