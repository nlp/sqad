<p>
<s>
Swiss	Swiss	k1gInSc1	Swiss
Cottage	Cottag	k1gFnSc2	Cottag
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
nepoužívané	používaný	k2eNgFnPc4d1	nepoužívaná
a	a	k8xC	a
zrušené	zrušený	k2eAgFnPc4d1	zrušená
stanice	stanice	k1gFnPc4	stanice
londýnského	londýnský	k2eAgNnSc2d1	Londýnské
metra	metro	k1gNnSc2	metro
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
Metropolitan	metropolitan	k1gInSc1	metropolitan
line	linout	k5eAaImIp3nS	linout
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Swiss	Swissa	k1gFnPc2	Swissa
Cottage	Cottag	k1gInSc2	Cottag
v	v	k7c6	v
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
stanice	stanice	k1gFnSc1	stanice
původně	původně	k6eAd1	původně
otevřena	otevřít	k5eAaPmNgFnS	otevřít
jako	jako	k8xC	jako
severní	severní	k2eAgFnSc1d1	severní
konečná	konečný	k2eAgFnSc1d1	konečná
stanice	stanice	k1gFnSc1	stanice
železnice	železnice	k1gFnSc2	železnice
Metropolitan	metropolitan	k1gInSc1	metropolitan
and	and	k?	and
St.	st.	kA	st.
John	John	k1gMnSc1	John
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Wood	Wood	k1gInSc1	Wood
Railway	Railwaa	k1gFnSc2	Railwaa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
prvním	první	k4xOgInSc7	první
severním	severní	k2eAgInSc7d1	severní
prodloužením	prodloužení	k1gNnSc7	prodloužení
železnice	železnice	k1gFnPc1	železnice
Metropolitan	metropolitan	k1gInSc4	metropolitan
Railway	Railwaa	k1gFnSc2	Railwaa
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
linka	linka	k1gFnSc1	linka
Metropolitan	metropolitan	k1gInSc1	metropolitan
line	linout	k5eAaImIp3nS	linout
<g/>
)	)	kIx)	)
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
Baker	Baker	k1gMnSc1	Baker
Street	Street	k1gMnSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
byla	být	k5eAaImAgFnS	být
linka	linka	k1gFnSc1	linka
odtud	odtud	k6eAd1	odtud
postupně	postupně	k6eAd1	postupně
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
do	do	k7c2	do
konečných	konečný	k2eAgFnPc2d1	konečná
stanic	stanice	k1gFnPc2	stanice
Watford	Watford	k1gInSc1	Watford
<g/>
,	,	kIx,	,
Amersham	Amersham	k1gInSc1	Amersham
<g/>
,	,	kIx,	,
Chesham	Chesham	k1gInSc1	Chesham
<g/>
,	,	kIx,	,
Uxbridge	Uxbridge	k1gInSc1	Uxbridge
a	a	k8xC	a
Stanmore	Stanmor	k1gMnSc5	Stanmor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
nové	nový	k2eAgFnSc2d1	nová
sousední	sousední	k2eAgFnSc2d1	sousední
ražené	ražený	k2eAgFnSc2d1	ražená
stanice	stanice	k1gFnSc2	stanice
Swiss	Swissa	k1gFnPc2	Swissa
Cottage	Cottag	k1gFnSc2	Cottag
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
obsluhována	obsluhovat	k5eAaImNgFnS	obsluhovat
linkou	linka	k1gFnSc7	linka
Bakerloo	Bakerloo	k1gNnSc4	Bakerloo
line	linout	k5eAaImIp3nS	linout
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
Jubilee	Jubile	k1gInSc2	Jubile
line	linout	k5eAaImIp3nS	linout
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
podpovrchová	podpovrchový	k2eAgFnSc1d1	podpovrchová
stanice	stanice	k1gFnSc1	stanice
metra	metro	k1gNnSc2	metro
Swiss	Swissa	k1gFnPc2	Swissa
Cottage	Cottage	k1gFnPc7	Cottage
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
Metropolitan	metropolitan	k1gInSc4	metropolitan
line	linout	k5eAaImIp3nS	linout
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Železnice	železnice	k1gFnSc1	železnice
Metropolitan	metropolitan	k1gInSc4	metropolitan
Railway	Railwaa	k1gFnSc2	Railwaa
otevřela	otevřít	k5eAaPmAgFnS	otevřít
stanici	stanice	k1gFnSc4	stanice
Swiss	Swissa	k1gFnPc2	Swissa
Cottage	Cottage	k1gNnPc2	Cottage
</s>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1868	[number]	k4	1868
jako	jako	k8xC	jako
severní	severní	k2eAgFnSc4d1	severní
konečnou	konečná	k1gFnSc4	konečná
své	svůj	k3xOyFgFnSc2	svůj
nové	nový	k2eAgFnSc2d1	nová
trati	trať	k1gFnSc2	trať
<g/>
,	,	kIx,	,
Metropolitan	metropolitan	k1gInSc1	metropolitan
and	and	k?	and
St.	st.	kA	st.
John	John	k1gMnSc1	John
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Wood	Wood	k1gInSc1	Wood
Railway	Railwaa	k1gFnSc2	Railwaa
<g/>
.	.	kIx.	.
</s>
<s>
26	26	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1868	[number]	k4	1868
se	se	k3xPyFc4	se
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
čelně	čelně	k6eAd1	čelně
srazily	srazit	k5eAaPmAgInP	srazit
dva	dva	k4xCgInPc1	dva
vlaky	vlak	k1gInPc1	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vině	vina	k1gFnSc6	vina
byla	být	k5eAaImAgFnS	být
chyba	chyba	k1gFnSc1	chyba
signalisty	signalista	k1gMnSc2	signalista
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přijíždějící	přijíždějící	k2eAgInSc1d1	přijíždějící
vlak	vlak	k1gInSc1	vlak
byl	být	k5eAaImAgInS	být
odkloněn	odklonit	k5eAaPmNgInS	odklonit
k	k	k7c3	k
nástupišti	nástupiště	k1gNnSc3	nástupiště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
jiný	jiný	k2eAgInSc4d1	jiný
vlak	vlak	k1gInSc4	vlak
připravený	připravený	k2eAgInSc4d1	připravený
k	k	k7c3	k
odjezdu	odjezd	k1gInSc3	odjezd
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
zraněni	zranit	k5eAaPmNgMnP	zranit
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
společnost	společnost	k1gFnSc1	společnost
Metropolitan	metropolitan	k1gInSc4	metropolitan
Railway	Railwaa	k1gFnSc2	Railwaa
zbourala	zbourat	k5eAaPmAgFnS	zbourat
staniční	staniční	k2eAgFnSc4d1	staniční
budovu	budova	k1gFnSc4	budova
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
ulice	ulice	k1gFnSc2	ulice
Finchley	Finchlea	k1gFnSc2	Finchlea
Road	Roada	k1gFnPc2	Roada
a	a	k8xC	a
nahradila	nahradit	k5eAaPmAgFnS	nahradit
ji	on	k3xPp3gFnSc4	on
nákupní	nákupní	k2eAgFnSc4d1	nákupní
pasáží	pasáž	k1gFnSc7	pasáž
a	a	k8xC	a
třemi	tři	k4xCgInPc7	tři
vchody	vchod	k1gInPc7	vchod
dolů	dolů	k6eAd1	dolů
k	k	k7c3	k
nástupištím	nástupiště	k1gNnPc3	nástupiště
<g/>
.	.	kIx.	.
</s>
<s>
Komplex	komplex	k1gInSc1	komplex
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
podle	podle	k7c2	podle
návrhů	návrh	k1gInPc2	návrh
Charlese	Charles	k1gMnSc2	Charles
Waltera	Walter	k1gMnSc2	Walter
Clarka	Clarek	k1gMnSc2	Clarek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začala	začít	k5eAaPmAgFnS	začít
linka	linka	k1gFnSc1	linka
Metropolitan	metropolitan	k1gInSc1	metropolitan
line	linout	k5eAaImIp3nS	linout
trpět	trpět	k5eAaImF	trpět
zahlcením	zahlcení	k1gNnSc7	zahlcení
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
konci	konec	k1gInSc6	konec
své	svůj	k3xOyFgFnSc2	svůj
hlavní	hlavní	k2eAgFnSc2d1	hlavní
trasy	trasa	k1gFnSc2	trasa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vlaky	vlak	k1gInPc1	vlak
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
jejích	její	k3xOp3gFnPc2	její
větví	větev	k1gFnPc2	větev
(	(	kIx(	(
<g/>
větve	větev	k1gFnPc1	větev
Watford	Watfordo	k1gNnPc2	Watfordo
<g/>
,	,	kIx,	,
Amersham	Amersham	k1gInSc1	Amersham
<g/>
,	,	kIx,	,
Chesham	Chesham	k1gInSc1	Chesham
<g/>
,	,	kIx,	,
Uxbridge	Uxbridge	k1gInSc1	Uxbridge
a	a	k8xC	a
Stanmore	Stanmor	k1gInSc5	Stanmor
<g/>
)	)	kIx)	)
snažily	snažit	k5eAaImAgFnP	snažit
sdílet	sdílet	k5eAaImF	sdílet
omezené	omezený	k2eAgFnPc4d1	omezená
kapacity	kapacita	k1gFnPc4	kapacita
úseku	úsek	k1gInSc2	úsek
tratě	trať	k1gFnSc2	trať
mezi	mezi	k7c7	mezi
stanicemi	stanice	k1gFnPc7	stanice
Finchley	Finchleum	k1gNnPc7	Finchleum
Road	Road	k1gMnSc1	Road
a	a	k8xC	a
Baker	Baker	k1gMnSc1	Baker
Street	Street	k1gMnSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zmírnění	zmírnění	k1gNnSc4	zmírnění
této	tento	k3xDgFnSc2	tento
kongesce	kongesce	k1gFnSc2	kongesce
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
nový	nový	k2eAgInSc1d1	nový
ražený	ražený	k2eAgInSc1d1	ražený
tunel	tunel	k1gInSc1	tunel
mezi	mezi	k7c7	mezi
stanicí	stanice	k1gFnSc7	stanice
Finchley	Finchlea	k1gFnSc2	Finchlea
Road	Road	k1gInSc1	Road
a	a	k8xC	a
tunely	tunel	k1gInPc1	tunel
linky	linka	k1gFnSc2	linka
Bakerloo	Bakerloo	k6eAd1	Bakerloo
line	linout	k5eAaImIp3nS	linout
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Baker	Baker	k1gMnSc1	Baker
Street	Street	k1gMnSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
byly	být	k5eAaImAgInP	být
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
stanice	stanice	k1gFnSc2	stanice
větve	větev	k1gFnSc2	větev
Stanmore	Stanmor	k1gInSc5	Stanmor
linky	linka	k1gFnPc4	linka
Metropolitan	metropolitan	k1gInSc1	metropolitan
line	linout	k5eAaImIp3nS	linout
převedeny	převést	k5eAaPmNgInP	převést
na	na	k7c4	na
linku	linka	k1gFnSc4	linka
Bakerloo	Bakerloo	k6eAd1	Bakerloo
line	linout	k5eAaImIp3nS	linout
a	a	k8xC	a
přesměrovány	přesměrován	k2eAgFnPc4d1	přesměrována
do	do	k7c2	do
nových	nový	k2eAgInPc2d1	nový
ražených	ražený	k2eAgInPc2d1	ražený
tunelů	tunel	k1gInPc2	tunel
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Baker	Baker	k1gMnSc1	Baker
Street	Street	k1gMnSc1	Street
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
snížil	snížit	k5eAaPmAgInS	snížit
počet	počet	k1gInSc1	počet
vlaků	vlak	k1gInPc2	vlak
používajících	používající	k2eAgInPc2d1	používající
tratě	trať	k1gFnPc4	trať
linky	linka	k1gFnPc4	linka
Metropolitan	metropolitan	k1gInSc1	metropolitan
line	linout	k5eAaImIp3nS	linout
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
úseku	úsek	k1gInSc6	úsek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
raženou	ražený	k2eAgFnSc7d1	ražená
trasou	trasa	k1gFnSc7	trasa
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
nová	nový	k2eAgFnSc1d1	nová
stanice	stanice	k1gFnSc1	stanice
linky	linka	k1gFnSc2	linka
Bakerloo	Bakerloo	k6eAd1	Bakerloo
line	linout	k5eAaImIp3nS	linout
<g/>
,	,	kIx,	,
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Swiss	Swiss	k1gInSc4	Swiss
Cottage	Cottag	k1gInSc2	Cottag
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
stávající	stávající	k2eAgFnSc2d1	stávající
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
stanice	stanice	k1gFnSc2	stanice
linky	linka	k1gFnSc2	linka
Metropolitan	metropolitan	k1gInSc1	metropolitan
line	linout	k5eAaImIp3nS	linout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
fungovaly	fungovat	k5eAaImAgFnP	fungovat
obě	dva	k4xCgFnPc1	dva
stanice	stanice	k1gFnPc1	stanice
jako	jako	k8xS	jako
stanice	stanice	k1gFnSc1	stanice
jedna	jeden	k4xCgFnSc1	jeden
(	(	kIx(	(
<g/>
nástupiště	nástupiště	k1gNnSc4	nástupiště
linky	linka	k1gFnSc2	linka
Metropolitan	metropolitan	k1gInSc4	metropolitan
line	linout	k5eAaImIp3nS	linout
zůstala	zůstat	k5eAaPmAgFnS	zůstat
označena	označit	k5eAaPmNgFnS	označit
jako	jako	k8xC	jako
nástupiště	nástupiště	k1gNnSc1	nástupiště
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
a	a	k8xC	a
nástupiště	nástupiště	k1gNnSc4	nástupiště
linky	linka	k1gFnSc2	linka
Bakerloo	Bakerloo	k6eAd1	Bakerloo
line	linout	k5eAaImIp3nS	linout
byla	být	k5eAaImAgFnS	být
označena	označit	k5eAaPmNgFnS	označit
jako	jako	k8xC	jako
nástupiště	nástupiště	k1gNnSc1	nástupiště
3	[number]	k4	3
a	a	k8xC	a
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
uspořádání	uspořádání	k1gNnSc1	uspořádání
však	však	k9	však
dlouho	dlouho	k6eAd1	dlouho
nevydrželo	vydržet	k5eNaPmAgNnS	vydržet
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
úsporných	úsporný	k2eAgNnPc2d1	úsporné
opatření	opatření	k1gNnPc2	opatření
v	v	k7c6	v
době	doba	k1gFnSc6	doba
války	válka	k1gFnSc2	válka
zastavil	zastavit	k5eAaPmAgMnS	zastavit
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
metra	metro	k1gNnSc2	metro
Swiss	Swiss	k1gInSc4	Swiss
Cottage	Cottage	k1gNnSc2	Cottage
linky	linka	k1gFnSc2	linka
Metropolitní	metropolitní	k2eAgNnSc1d1	metropolitní
line	linout	k5eAaImIp3nS	linout
poslední	poslední	k2eAgInSc4d1	poslední
vlak	vlak	k1gInSc4	vlak
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
linky	linka	k1gFnSc2	linka
Jubilee	Jubile	k1gInSc2	Jubile
line	linout	k5eAaImIp3nS	linout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
byla	být	k5eAaImAgFnS	být
větev	větev	k1gFnSc1	větev
Stanmore	Stanmor	k1gInSc5	Stanmor
linky	linka	k1gFnPc4	linka
Bakerloo	Bakerloo	k6eAd1	Bakerloo
line	linout	k5eAaImIp3nS	linout
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nové	nový	k2eAgFnSc2d1	nová
stanice	stanice	k1gFnSc2	stanice
Swiss	Swissa	k1gFnPc2	Swissa
Cottage	Cottag	k1gFnSc2	Cottag
<g/>
,	,	kIx,	,
převedena	převést	k5eAaPmNgFnS	převést
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
linku	linka	k1gFnSc4	linka
Jubilee	Jubile	k1gInSc2	Jubile
line	linout	k5eAaImIp3nS	linout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
původní	původní	k2eAgFnSc2d1	původní
stanice	stanice	k1gFnSc2	stanice
stržena	stržen	k2eAgFnSc1d1	stržena
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
rozšířením	rozšíření	k1gNnSc7	rozšíření
ulice	ulice	k1gFnSc2	ulice
Finchley	Finchlea	k1gFnSc2	Finchlea
Road	Roada	k1gFnPc2	Roada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gNnPc4	její
bývalá	bývalý	k2eAgNnPc4d1	bývalé
nástupiště	nástupiště	k1gNnPc4	nástupiště
stále	stále	k6eAd1	stále
částečně	částečně	k6eAd1	částečně
existují	existovat	k5eAaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nepostavená	postavený	k2eNgFnSc1d1	nepostavená
trať	trať	k1gFnSc1	trať
na	na	k7c4	na
Hampstead	Hampstead	k1gInSc4	Hampstead
==	==	k?	==
</s>
</p>
<p>
<s>
Původní	původní	k2eAgMnPc1d1	původní
záměrem	záměr	k1gInSc7	záměr
železniční	železniční	k2eAgFnSc2d1	železniční
společnosti	společnost	k1gFnSc2	společnost
Metropolitan	metropolitan	k1gInSc1	metropolitan
&	&	k?	&
St.	st.	kA	st.
John	John	k1gMnSc1	John
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Wood	Wood	k1gInSc1	Wood
Railway	Railwaa	k1gMnSc2	Railwaa
bylo	být	k5eAaImAgNnS	být
stočit	stočit	k5eAaPmF	stočit
se	se	k3xPyFc4	se
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
podzemní	podzemní	k2eAgFnSc7d1	podzemní
tratí	trať	k1gFnSc7	trať
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
na	na	k7c4	na
Hampstead	Hampstead	k1gInSc4	Hampstead
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
objevilo	objevit	k5eAaPmAgNnS	objevit
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
mapách	mapa	k1gFnPc6	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
tento	tento	k3xDgInSc4	tento
projekt	projekt	k1gInSc4	projekt
nepokračoval	pokračovat	k5eNaImAgMnS	pokračovat
a	a	k8xC	a
trať	trať	k1gFnSc1	trať
byla	být	k5eAaImAgFnS	být
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
vedena	veden	k2eAgFnSc1d1	vedena
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
krátký	krátký	k2eAgInSc1d1	krátký
úsek	úsek	k1gInSc1	úsek
tunelu	tunel	k1gInSc2	tunel
severně	severně	k6eAd1	severně
od	od	k7c2	od
stanice	stanice	k1gFnSc2	stanice
Swiss	Swiss	k1gInSc1	Swiss
Cottage	Cottage	k1gFnSc4	Cottage
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
z	z	k7c2	z
vlaku	vlak	k1gInSc2	vlak
linky	linka	k1gFnSc2	linka
Metropolitan	metropolitan	k1gInSc1	metropolitan
line	linout	k5eAaImIp3nS	linout
směřujícího	směřující	k2eAgMnSc4d1	směřující
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
uzavřené	uzavřený	k2eAgFnPc1d1	uzavřená
stanice	stanice	k1gFnPc1	stanice
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
úseku	úsek	k1gInSc6	úsek
==	==	k?	==
</s>
</p>
<p>
<s>
Dalšími	další	k2eAgFnPc7d1	další
stanicemi	stanice	k1gFnPc7	stanice
linky	linka	k1gFnSc2	linka
Metropolitan	metropolitan	k1gInSc4	metropolitan
line	linout	k5eAaImIp3nS	linout
uzavřenými	uzavřený	k2eAgInPc7d1	uzavřený
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
otevřením	otevření	k1gNnSc7	otevření
raženého	ražený	k2eAgInSc2d1	ražený
tunelu	tunel	k1gInSc2	tunel
linky	linka	k1gFnSc2	linka
Bakerloo	Bakerloo	k6eAd1	Bakerloo
line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
linka	linka	k1gFnSc1	linka
Jubilee	Jubile	k1gFnSc2	Jubile
line	linout	k5eAaImIp3nS	linout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
vlaky	vlak	k1gInPc7	vlak
linky	linka	k1gFnPc4	linka
Metropolitan	metropolitan	k1gInSc1	metropolitan
line	linout	k5eAaImIp3nS	linout
pouze	pouze	k6eAd1	pouze
projíždějí	projíždět	k5eAaImIp3nP	projíždět
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lord	lord	k1gMnSc1	lord
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
</s>
</p>
<p>
<s>
Marlborough	Marlborough	k1gMnSc1	Marlborough
Road	Road	k1gMnSc1	Road
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Swiss	Swissa	k1gFnPc2	Swissa
Cottage	Cottag	k1gFnSc2	Cottag
tube	tubus	k1gInSc5	tubus
station	station	k1gInSc1	station
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
London	London	k1gMnSc1	London
Transport	transporta	k1gFnPc2	transporta
Museum	museum	k1gNnSc1	museum
Photographic	Photographic	k1gMnSc1	Photographic
Archive	archiv	k1gInSc5	archiv
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
stanice	stanice	k1gFnSc2	stanice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
po	po	k7c6	po
přestavbě	přestavba	k1gFnSc6	přestavba
(	(	kIx(	(
<g/>
archiv	archiv	k1gInSc1	archiv
London	London	k1gMnSc1	London
Transport	transport	k1gInSc1	transport
Museum	museum	k1gNnSc1	museum
<g/>
)	)	kIx)	)
</s>
</p>
