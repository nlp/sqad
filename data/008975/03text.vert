<p>
<s>
Turečtina	turečtina	k1gFnSc1	turečtina
je	být	k5eAaImIp3nS	být
turkický	turkický	k2eAgInSc1d1	turkický
jazyk	jazyk	k1gInSc1	jazyk
používaný	používaný	k2eAgInSc1d1	používaný
zejména	zejména	k9	zejména
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
,	,	kIx,	,
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
jazykem	jazyk	k1gInSc7	jazyk
několika	několik	k4yIc2	několik
milionů	milion	k4xCgInPc2	milion
imigrantů	imigrant	k1gMnPc2	imigrant
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
turečtina	turečtina	k1gFnSc1	turečtina
používala	používat	k5eAaImAgFnS	používat
výhradně	výhradně	k6eAd1	výhradně
turecké	turecký	k2eAgNnSc4d1	turecké
osmanské	osmanský	k2eAgNnSc4d1	osmanské
písmo	písmo	k1gNnSc4	písmo
(	(	kIx(	(
<g/>
variantu	varianta	k1gFnSc4	varianta
arabského	arabský	k2eAgNnSc2d1	arabské
písma	písmo	k1gNnSc2	písmo
<g/>
)	)	kIx)	)
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
popud	popud	k1gInSc4	popud
Kemala	Kemal	k1gMnSc2	Kemal
Atatürka	Atatürek	k1gMnSc2	Atatürek
zavedena	zaveden	k2eAgFnSc1d1	zavedena
latinka	latinka	k1gFnSc1	latinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
turečtinou	turečtina	k1gFnSc7	turečtina
a	a	k8xC	a
jinými	jiný	k2eAgInPc7d1	jiný
oghuzskými	oghuzský	k2eAgInPc7d1	oghuzský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
azerštinou	azerština	k1gFnSc7	azerština
<g/>
,	,	kIx,	,
turkmenštinou	turkmenština	k1gFnSc7	turkmenština
či	či	k8xC	či
kazaštinou	kazaština	k1gFnSc7	kazaština
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
úroveň	úroveň	k1gFnSc1	úroveň
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
srozumitelnosti	srozumitelnost	k1gFnSc2	srozumitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
rodilých	rodilý	k2eAgMnPc2d1	rodilý
mluvčích	mluvčí	k1gMnPc2	mluvčí
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podskupině	podskupina	k1gFnSc6	podskupina
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
a	a	k8xC	a
celkový	celkový	k2eAgInSc4d1	celkový
počet	počet	k1gInSc4	počet
mluvčích	mluvčí	k1gFnPc2	mluvčí
125	[number]	k4	125
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
jazyka	jazyk	k1gInSc2	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
a	a	k8xC	a
turecké	turecký	k2eAgFnSc3d1	turecká
části	část	k1gFnSc3	část
Kypru	Kypr	k1gInSc2	Kypr
je	být	k5eAaImIp3nS	být
turečtina	turečtina	k1gFnSc1	turečtina
úředním	úřední	k2eAgMnSc7d1	úřední
jazykem	jazyk	k1gMnSc7	jazyk
<g/>
,	,	kIx,	,
turecky	turecky	k6eAd1	turecky
však	však	k9	však
mluví	mluvit	k5eAaImIp3nS	mluvit
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
menšin	menšina	k1gFnPc2	menšina
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Abeceda	abeceda	k1gFnSc1	abeceda
==	==	k?	==
</s>
</p>
<p>
<s>
Turečtina	turečtina	k1gFnSc1	turečtina
používá	používat	k5eAaImIp3nS	používat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
latinku	latinka	k1gFnSc4	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Turecká	turecký	k2eAgFnSc1d1	turecká
abeceda	abeceda	k1gFnSc1	abeceda
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
29	[number]	k4	29
písmen	písmeno	k1gNnPc2	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
písmena	písmeno	k1gNnPc1	písmeno
(	(	kIx(	(
<g/>
Â	Â	kA	Â
<g/>
,	,	kIx,	,
Ç	Ç	kA	Ç
<g/>
,	,	kIx,	,
Ğ	Ğ	k?	Ğ
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
,	,	kIx,	,
İ	İ	k?	İ
<g/>
,	,	kIx,	,
Î	Î	kA	Î
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ö	Ö	kA	Ö
<g/>
,	,	kIx,	,
Ş	Ş	kA	Ş
<g/>
,	,	kIx,	,
Û	Û	k?	Û
a	a	k8xC	a
Ü	Ü	kA	Ü
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
upravena	upravit	k5eAaPmNgFnS	upravit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
fonetickým	fonetický	k2eAgFnPc3d1	fonetická
potřebám	potřeba	k1gFnPc3	potřeba
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velká	velký	k2eAgNnPc1d1	velké
písmena	písmeno	k1gNnPc1	písmeno
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
A	A	kA	A
<g/>
,	,	kIx,	,
Â	Â	kA	Â
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
Ç	Ç	kA	Ç
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
,	,	kIx,	,
Ğ	Ğ	k?	Ğ
<g/>
,	,	kIx,	,
H	H	kA	H
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
,	,	kIx,	,
İ	İ	k?	İ
<g/>
,	,	kIx,	,
Î	Î	kA	Î
J	J	kA	J
<g/>
,	,	kIx,	,
K	K	kA	K
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
,	,	kIx,	,
O	O	kA	O
<g/>
,	,	kIx,	,
Ö	Ö	kA	Ö
<g/>
,	,	kIx,	,
P	P	kA	P
<g/>
,	,	kIx,	,
R	R	kA	R
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
,	,	kIx,	,
Ş	Ş	kA	Ş
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
,	,	kIx,	,
U	U	kA	U
<g/>
,	,	kIx,	,
Û	Û	k?	Û
<g/>
,	,	kIx,	,
Ü	Ü	kA	Ü
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
,	,	kIx,	,
Y	Y	kA	Y
<g/>
,	,	kIx,	,
ZMalá	ZMalý	k2eAgNnPc4d1	ZMalý
písmena	písmeno	k1gNnPc4	písmeno
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
a	a	k9	a
<g/>
,	,	kIx,	,
â	â	k?	â
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
,	,	kIx,	,
ç	ç	k?	ç
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
,	,	kIx,	,
ğ	ğ	k?	ğ
<g/>
,	,	kIx,	,
h	h	k?	h
<g/>
,	,	kIx,	,
ı	ı	k?	ı
<g/>
,	,	kIx,	,
i	i	k9	i
<g/>
,	,	kIx,	,
î	î	k?	î
j	j	k?	j
<g/>
,	,	kIx,	,
k	k	k7c3	k
<g/>
,	,	kIx,	,
l	l	kA	l
<g/>
,	,	kIx,	,
m	m	kA	m
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
ö	ö	k?	ö
<g/>
,	,	kIx,	,
p	p	k?	p
<g/>
,	,	kIx,	,
r	r	kA	r
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
,	,	kIx,	,
ş	ş	k?	ş
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
û	û	k?	û
<g/>
,	,	kIx,	,
ü	ü	k?	ü
<g/>
,	,	kIx,	,
v	v	k7c6	v
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
,	,	kIx,	,
z.	z.	k?	z.
</s>
</p>
<p>
<s>
Turečtina	turečtina	k1gFnSc1	turečtina
má	mít	k5eAaImIp3nS	mít
8	[number]	k4	8
samohlásek	samohláska	k1gFnPc2	samohláska
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
,	,	kIx,	,
İ	İ	k?	İ
<g/>
,	,	kIx,	,
O	O	kA	O
<g/>
,	,	kIx,	,
Ö	Ö	kA	Ö
<g/>
,	,	kIx,	,
U	U	kA	U
<g/>
,	,	kIx,	,
Ü	Ü	kA	Ü
<g/>
)	)	kIx)	)
a	a	k8xC	a
21	[number]	k4	21
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Názvy	název	k1gInPc1	název
písmen	písmeno	k1gNnPc2	písmeno
===	===	k?	===
</s>
</p>
<p>
<s>
Názvy	název	k1gInPc1	název
písmen	písmeno	k1gNnPc2	písmeno
představujících	představující	k2eAgNnPc2d1	představující
samohlásku	samohláska	k1gFnSc4	samohláska
jsou	být	k5eAaImIp3nP	být
samohlásky	samohláska	k1gFnPc1	samohláska
samy	sám	k3xTgFnPc1	sám
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
názvy	název	k1gInPc7	název
souhlásek	souhláska	k1gFnPc2	souhláska
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
souhláska	souhláska	k1gFnSc1	souhláska
+	+	kIx~	+
e.	e.	k?	e.
Jediná	jediný	k2eAgFnSc1d1	jediná
výjimka	výjimka	k1gFnSc1	výjimka
je	být	k5eAaImIp3nS	být
písmeno	písmeno	k1gNnSc4	písmeno
ğ	ğ	k?	ğ
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
yumuşak	yumuşak	k1gInSc1	yumuşak
ge	ge	k?	ge
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
měkké	měkký	k2eAgNnSc1d1	měkké
gé	gé	k?	gé
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
a	a	k9	a
<g/>
,	,	kIx,	,
be	be	k?	be
<g/>
,	,	kIx,	,
ce	ce	k?	ce
<g/>
,	,	kIx,	,
çe	çe	k?	çe
<g/>
,	,	kIx,	,
de	de	k?	de
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
fe	fe	k?	fe
<g/>
,	,	kIx,	,
ge	ge	k?	ge
<g/>
,	,	kIx,	,
yumuşak	yumuşak	k1gMnSc1	yumuşak
ge	ge	k?	ge
<g/>
,	,	kIx,	,
he	he	k0	he
<g/>
,	,	kIx,	,
ı	ı	k?	ı
<g/>
,	,	kIx,	,
i	i	k9	i
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
ke	k	k7c3	k
<g/>
,	,	kIx,	,
le	le	k?	le
<g/>
,	,	kIx,	,
me	me	k?	me
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
ö	ö	k?	ö
<g/>
,	,	kIx,	,
pe	pe	k?	pe
<g/>
,	,	kIx,	,
re	re	k9	re
<g/>
,	,	kIx,	,
se	s	k7c7	s
<g/>
,	,	kIx,	,
şe	şe	k?	şe
<g/>
,	,	kIx,	,
te	te	k?	te
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
ü	ü	k?	ü
<g/>
,	,	kIx,	,
ve	v	k7c6	v
<g/>
,	,	kIx,	,
ye	ye	k?	ye
<g/>
,	,	kIx,	,
ze	z	k7c2	z
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výslovnost	výslovnost	k1gFnSc1	výslovnost
===	===	k?	===
</s>
</p>
<p>
<s>
Turecký	turecký	k2eAgInSc1d1	turecký
pravopis	pravopis	k1gInSc1	pravopis
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
fonetický	fonetický	k2eAgMnSc1d1	fonetický
a	a	k8xC	a
zápis	zápis	k1gInSc1	zápis
každého	každý	k3xTgNnSc2	každý
slova	slovo	k1gNnSc2	slovo
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jeho	jeho	k3xOp3gFnPc4	jeho
výslovnosti	výslovnost	k1gFnPc4	výslovnost
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
turecká	turecký	k2eAgNnPc4d1	turecké
písmena	písmeno	k1gNnPc4	písmeno
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
ekvivalenty	ekvivalent	k1gInPc1	ekvivalent
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
fonetické	fonetický	k2eAgFnSc6d1	fonetická
abecedě	abeceda	k1gFnSc6	abeceda
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
přibližná	přibližný	k2eAgFnSc1d1	přibližná
výslovnost	výslovnost	k1gFnSc1	výslovnost
podle	podle	k7c2	podle
češtiny	čeština	k1gFnSc2	čeština
</s>
</p>
<p>
<s>
==	==	k?	==
Přízvuk	přízvuk	k1gInSc4	přízvuk
==	==	k?	==
</s>
</p>
<p>
<s>
Obecně	obecně	k6eAd1	obecně
přízvuk	přízvuk	k1gInSc1	přízvuk
v	v	k7c6	v
turečtině	turečtina	k1gFnSc6	turečtina
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
slabice	slabika	k1gFnSc6	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
příslovce	příslovce	k1gNnPc4	příslovce
<g/>
,	,	kIx,	,
citoslovce	citoslovce	k1gNnPc4	citoslovce
<g/>
,	,	kIx,	,
spojky	spojka	k1gFnPc4	spojka
<g/>
,	,	kIx,	,
oslovení	oslovení	k1gNnPc4	oslovení
<g/>
,	,	kIx,	,
zeměpisné	zeměpisný	k2eAgInPc4d1	zeměpisný
názvy	název	k1gInPc4	název
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
přípony	přípona	k1gFnPc4	přípona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
přízvuk	přízvuk	k1gInSc1	přízvuk
určuje	určovat	k5eAaImIp3nS	určovat
význam	význam	k1gInSc4	význam
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ártı	ártı	k?	ártı
<g/>
/	/	kIx~	/
<g/>
arı	arı	k?	arı
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzorový	vzorový	k2eAgInSc1d1	vzorový
text	text	k1gInSc1	text
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KUČERA	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Podrobná	podrobný	k2eAgFnSc1d1	podrobná
gramatika	gramatika	k1gFnSc1	gramatika
turečtiny	turečtina	k1gFnSc2	turečtina
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Lingea	Lingea	k1gFnSc1	Lingea
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
494	[number]	k4	494
S.	S.	kA	S.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87471	[number]	k4	87471
<g/>
-	-	kIx~	-
<g/>
79	[number]	k4	79
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HRISTOVA	HRISTOVA	kA	HRISTOVA
<g/>
,	,	kIx,	,
Radka	Radka	k1gFnSc1	Radka
<g/>
.	.	kIx.	.
</s>
<s>
Stručná	stručný	k2eAgFnSc1d1	stručná
mluvnice	mluvnice	k1gFnSc1	mluvnice
tureckého	turecký	k2eAgInSc2d1	turecký
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
147	[number]	k4	147
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7184	[number]	k4	7184
<g/>
-	-	kIx~	-
<g/>
736	[number]	k4	736
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
turečtina	turečtina	k1gFnSc1	turečtina
</s>
</p>
<p>
<s>
Turecké	turecký	k2eAgNnSc1d1	turecké
osmanské	osmanský	k2eAgNnSc1d1	osmanské
písmo	písmo	k1gNnSc1	písmo
</s>
</p>
<p>
<s>
Turkologie	turkologie	k1gFnSc1	turkologie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
turečtina	turečtina	k1gFnSc1	turečtina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
