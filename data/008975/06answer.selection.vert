<s>
Turečtina	turečtina	k1gFnSc1	turečtina
je	být	k5eAaImIp3nS	být
turkický	turkický	k2eAgInSc1d1	turkický
jazyk	jazyk	k1gInSc1	jazyk
používaný	používaný	k2eAgInSc1d1	používaný
zejména	zejména	k9	zejména
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
,	,	kIx,	,
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
jazykem	jazyk	k1gInSc7	jazyk
několika	několik	k4yIc2	několik
milionů	milion	k4xCgInPc2	milion
imigrantů	imigrant	k1gMnPc2	imigrant
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
