<s>
Honduras	Honduras	k1gInSc1	Honduras
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
v	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
sousedy	soused	k1gMnPc7	soused
jsou	být	k5eAaImIp3nP	být
Guatemala	Guatemala	k1gFnSc1	Guatemala
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
,	,	kIx,	,
Salvador	Salvador	k1gInSc1	Salvador
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
a	a	k8xC	a
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Jihozápadní	jihozápadní	k2eAgInSc1d1	jihozápadní
výběžek	výběžek	k1gInSc1	výběžek
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
omýván	omývat	k5eAaImNgInS	omývat
vodami	voda	k1gFnPc7	voda
Pacifiku	Pacifik	k1gInSc2	Pacifik
<g/>
,	,	kIx,	,
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
strany	strana	k1gFnSc2	strana
má	mít	k5eAaImIp3nS	mít
Honduras	Honduras	k1gInSc4	Honduras
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
pobřeží	pobřeží	k1gNnSc4	pobřeží
s	s	k7c7	s
Atlantickým	atlantický	k2eAgInSc7d1	atlantický
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Administrativně	administrativně	k6eAd1	administrativně
se	se	k3xPyFc4	se
stát	stát	k1gInSc1	stát
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
18	[number]	k4	18
departementů	departement	k1gInPc2	departement
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hondurasu	Honduras	k1gInSc6	Honduras
stále	stále	k6eAd1	stále
panuje	panovat	k5eAaImIp3nS	panovat
neklidná	klidný	k2eNgFnSc1d1	neklidná
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
průmysl	průmysl	k1gInSc4	průmysl
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
důležité	důležitý	k2eAgNnSc4d1	důležité
zpracování	zpracování	k1gNnSc4	zpracování
mahagonového	mahagonový	k2eAgNnSc2d1	mahagonové
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Honduras	Honduras	k1gInSc1	Honduras
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zemi	zem	k1gFnSc4	zem
dali	dát	k5eAaPmAgMnP	dát
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
hluboký	hluboký	k2eAgInSc4d1	hluboký
záliv	záliv	k1gInSc4	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
podoby	podoba	k1gFnSc2	podoba
zálivu	záliv	k1gInSc2	záliv
u	u	k7c2	u
města	město	k1gNnSc2	město
Trujillo	Trujillo	k1gNnSc1	Trujillo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
prvním	první	k4xOgMnSc6	první
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
v	v	k7c6	v
Hondurasu	Honduras	k1gInSc6	Honduras
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
citátu	citát	k1gInSc2	citát
Kryštofa	Kryštof	k1gMnSc2	Kryštof
Kolumba	Kolumbus	k1gMnSc2	Kolumbus
"	"	kIx"	"
<g/>
Gracias	Gracias	k1gInSc1	Gracias
a	a	k8xC	a
Dios	Dios	k1gInSc1	Dios
que	que	k?	que
hemos	hemos	k1gInSc1	hemos
salido	salida	k1gFnSc5	salida
de	de	k?	de
estas	estas	k1gMnSc1	estas
honduras	honduras	k1gMnSc1	honduras
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Díky	díky	k7c3	díky
bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
unikli	uniknout	k5eAaPmAgMnP	uniknout
z	z	k7c2	z
těch	ten	k3xDgFnPc2	ten
zrádných	zrádný	k2eAgFnPc2d1	zrádná
hlubin	hlubina	k1gFnPc2	hlubina
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
pronesl	pronést	k5eAaPmAgMnS	pronést
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
přistál	přistát	k5eAaImAgInS	přistát
během	během	k7c2	během
tropické	tropický	k2eAgFnSc2d1	tropická
bouře	bouř	k1gFnSc2	bouř
na	na	k7c6	na
výběžku	výběžek	k1gInSc6	výběžek
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
pak	pak	k6eAd1	pak
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
Gracias	Gracias	k1gInSc1	Gracias
a	a	k8xC	a
Dios	Dios	k1gInSc1	Dios
<g/>
.	.	kIx.	.
</s>
<s>
Aztékové	Azték	k1gMnPc1	Azték
jej	on	k3xPp3gMnSc4	on
ovšem	ovšem	k9	ovšem
nazývali	nazývat	k5eAaImAgMnP	nazývat
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Nahuatl	Nahuatl	k1gMnSc1	Nahuatl
Zollan	Zollan	k1gMnSc1	Zollan
-	-	kIx~	-
Lugar	Lugar	k1gMnSc1	Lugar
de	de	k?	de
cordonices	cordonices	k1gMnSc1	cordonices
-	-	kIx~	-
Místo	místo	k7c2	místo
křepelek	křepelka	k1gFnPc2	křepelka
a	a	k8xC	a
v	v	k7c6	v
nahuatlu	nahuatlo	k1gNnSc6	nahuatlo
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
dodnes	dodnes	k6eAd1	dodnes
používá	používat	k5eAaImIp3nS	používat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jméno	jméno	k1gNnSc1	jméno
pro	pro	k7c4	pro
křepelku	křepelka	k1gFnSc4	křepelka
-	-	kIx~	-
cordoniz	cordoniz	k1gInSc1	cordoniz
-	-	kIx~	-
převzali	převzít	k5eAaPmAgMnP	převzít
Aztékové	Azték	k1gMnPc1	Azték
z	z	k7c2	z
jazyku	jazyk	k1gInSc3	jazyk
Pipil	Pipil	k1gInSc1	Pipil
<g/>
,	,	kIx,	,
příbuznému	příbuzný	k1gMnSc3	příbuzný
Nahuatlu	Nahuatl	k1gMnSc3	Nahuatl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
sula	sout	k5eAaImAgFnS	sout
(	(	kIx(	(
<g/>
v	v	k7c4	v
Pipil	Pipil	k1gInSc4	Pipil
<g/>
)	)	kIx)	)
a	a	k8xC	a
zolli	zolle	k1gFnSc4	zolle
<g/>
(	(	kIx(	(
<g/>
v	v	k7c6	v
Nahuatlu	Nahuatl	k1gInSc6	Nahuatl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jazykem	jazyk	k1gInSc7	jazyk
Pipil	Pipil	k1gInSc1	Pipil
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
El	Ela	k1gFnPc2	Ela
Salvadoru	Salvador	k1gInSc2	Salvador
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
s	s	k7c7	s
Hondurasem	Honduras	k1gInSc7	Honduras
sousedí	sousedit	k5eAaImIp3nS	sousedit
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Hondurasu	Honduras	k1gInSc2	Honduras
nedaleko	nedaleko	k7c2	nedaleko
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Guatemalou	Guatemala	k1gFnSc7	Guatemala
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
v	v	k7c6	v
předkolumbovské	předkolumbovský	k2eAgFnSc6d1	předkolumbovská
době	doba	k1gFnSc6	doba
město	město	k1gNnSc1	město
Copán	Copána	k1gFnPc2	Copána
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
center	centrum	k1gNnPc2	centrum
Mayské	mayský	k2eAgFnSc2d1	mayská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
pocházelo	pocházet	k5eAaImAgNnS	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
přelomu	přelom	k1gInSc2	přelom
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
staré	starý	k2eAgNnSc4d1	staré
osídlení	osídlení	k1gNnSc4	osídlení
oblasti	oblast	k1gFnSc2	oblast
a	a	k8xC	a
na	na	k7c4	na
vysoký	vysoký	k2eAgInSc4d1	vysoký
stupeň	stupeň	k1gInSc4	stupeň
civilizace	civilizace	k1gFnSc2	civilizace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
západní	západní	k2eAgFnSc4d1	západní
civilizaci	civilizace	k1gFnSc4	civilizace
byla	být	k5eAaImAgFnS	být
země	zem	k1gFnSc2	zem
objevena	objevit	k5eAaPmNgFnS	objevit
Kryštofem	Kryštof	k1gMnSc7	Kryštof
Kolumbem	Kolumbus	k1gMnSc7	Kolumbus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spatřil	spatřit	k5eAaPmAgInS	spatřit
břehy	břeh	k1gInPc4	břeh
Hondurasu	Honduras	k1gInSc2	Honduras
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1502	[number]	k4	1502
během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
objevitelských	objevitelský	k2eAgFnPc2d1	objevitelská
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
španělském	španělský	k2eAgNnSc6d1	španělské
objevení	objevení	k1gNnSc6	objevení
oblasti	oblast	k1gFnSc2	oblast
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
brzy	brzy	k6eAd1	brzy
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
španělského	španělský	k2eAgNnSc2d1	španělské
impéria	impérium	k1gNnSc2	impérium
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
generálního	generální	k2eAgInSc2d1	generální
kapitanátu	kapitanát	k1gInSc2	kapitanát
Guatemala	Guatemala	k1gFnSc1	Guatemala
<g/>
.	.	kIx.	.
</s>
<s>
Honduras	Honduras	k1gInSc1	Honduras
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Španělsku	Španělsko	k1gNnSc6	Španělsko
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1821	[number]	k4	1821
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
stát	stát	k1gInSc1	stát
podroben	podrobit	k5eAaPmNgInS	podrobit
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
roku	rok	k1gInSc2	rok
1823	[number]	k4	1823
a	a	k8xC	a
1839	[number]	k4	1839
byl	být	k5eAaImAgInS	být
součástí	součást	k1gFnSc7	součást
Spojených	spojený	k2eAgFnPc2d1	spojená
středoamerických	středoamerický	k2eAgFnPc2d1	středoamerická
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
nepříliš	příliš	k6eNd1	příliš
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
pokusů	pokus	k1gInPc2	pokus
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
federace	federace	k1gFnSc2	federace
středoamerických	středoamerický	k2eAgInPc2d1	středoamerický
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
se	se	k3xPyFc4	se
Honduras	Honduras	k1gInSc1	Honduras
dostal	dostat	k5eAaPmAgInS	dostat
pod	pod	k7c4	pod
vliv	vliv	k1gInSc4	vliv
amerických	americký	k2eAgFnPc2d1	americká
společností	společnost	k1gFnPc2	společnost
jako	jako	k8xS	jako
United	United	k1gInSc1	United
Fruit	Fruita	k1gFnPc2	Fruita
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
řazen	řadit	k5eAaImNgMnS	řadit
mezi	mezi	k7c4	mezi
"	"	kIx"	"
<g/>
banánové	banánový	k2eAgFnPc4d1	banánová
republiky	republika	k1gFnPc4	republika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
Hondurasem	Honduras	k1gInSc7	Honduras
a	a	k8xC	a
Salvadorem	Salvador	k1gInSc7	Salvador
v	v	k7c6	v
několikadenní	několikadenní	k2eAgFnSc6d1	několikadenní
Fotbalové	fotbalový	k2eAgFnSc6d1	fotbalová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
do	do	k7c2	do
země	zem	k1gFnSc2	zem
uprchlo	uprchnout	k5eAaPmAgNnS	uprchnout
mnoho	mnoho	k4c1	mnoho
válečných	válečný	k2eAgMnPc2d1	válečný
uprchlíků	uprchlík	k1gMnPc2	uprchlík
ze	z	k7c2	z
Salvadoru	Salvador	k1gInSc2	Salvador
a	a	k8xC	a
Nikaragui	Nikaragua	k1gFnSc6	Nikaragua
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
utíkali	utíkat	k5eAaImAgMnP	utíkat
před	před	k7c7	před
salvadorskou	salvadorský	k2eAgFnSc7d1	Salvadorská
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
resp.	resp.	kA	resp.
nikaragujskou	nikaragujský	k2eAgFnSc7d1	Nikaragujská
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
zemi	zem	k1gFnSc4	zem
těžce	těžce	k6eAd1	těžce
postihl	postihnout	k5eAaPmAgInS	postihnout
hurikán	hurikán	k1gInSc1	hurikán
Mitch	Mitch	k1gInSc1	Mitch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
převratu	převrat	k1gInSc3	převrat
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
byl	být	k5eAaImAgMnS	být
armádou	armáda	k1gFnSc7	armáda
svržen	svržen	k2eAgMnSc1d1	svržen
prezident	prezident	k1gMnSc1	prezident
Manuel	Manuel	k1gMnSc1	Manuel
Zelaya	Zelaya	k1gMnSc1	Zelaya
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
prosadit	prosadit	k5eAaPmF	prosadit
referendum	referendum	k1gNnSc4	referendum
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
povolující	povolující	k2eAgFnSc1d1	povolující
mu	on	k3xPp3gMnSc3	on
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
ucházet	ucházet	k5eAaImF	ucházet
o	o	k7c4	o
post	post	k1gInSc4	post
prezidenta	prezident	k1gMnSc2	prezident
v	v	k7c6	v
nadcházejících	nadcházející	k2eAgFnPc6d1	nadcházející
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
de	de	k?	de
facto	facto	k1gNnSc1	facto
stal	stát	k5eAaPmAgMnS	stát
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
předseda	předseda	k1gMnSc1	předseda
parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
prezident	prezident	k1gMnSc1	prezident
Národního	národní	k2eAgInSc2d1	národní
Kongresu	kongres	k1gInSc2	kongres
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Roberto	Roberta	k1gFnSc5	Roberta
Micheletti	Micheletť	k1gFnSc3	Micheletť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
kandidát	kandidát	k1gMnSc1	kandidát
opoziční	opoziční	k2eAgFnSc2d1	opoziční
Národní	národní	k2eAgFnSc2d1	národní
strany	strana	k1gFnSc2	strana
Hondurasu	Honduras	k1gInSc2	Honduras
Porfirio	Porfirio	k1gNnSc1	Porfirio
Lobo	Lobo	k1gMnSc1	Lobo
Sosa	Sosa	k1gMnSc1	Sosa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgInS	získat
více	hodně	k6eAd2	hodně
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
56	[number]	k4	56
%	%	kIx~	%
<g/>
)	)	kIx)	)
než	než	k8xS	než
vládní	vládní	k2eAgMnSc1d1	vládní
kandidát	kandidát	k1gMnSc1	kandidát
Liberální	liberální	k2eAgFnSc2d1	liberální
strany	strana	k1gFnSc2	strana
Hondurasu	Honduras	k1gInSc2	Honduras
Elvin	Elvin	k1gInSc1	Elvin
Ernesto	Ernesta	k1gMnSc5	Ernesta
Santos	Santosa	k1gFnPc2	Santosa
Ordóñ	Ordóñ	k1gMnPc3	Ordóñ
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
svržený	svržený	k2eAgMnSc1d1	svržený
prezident	prezident	k1gMnSc1	prezident
José	José	k1gNnSc2	José
Manuel	Manuel	k1gMnSc1	Manuel
Zelaya	Zelaya	k1gMnSc1	Zelaya
Rosales	Rosales	k1gMnSc1	Rosales
výsledky	výsledek	k1gInPc4	výsledek
voleb	volba	k1gFnPc2	volba
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
výsledky	výsledek	k1gInPc1	výsledek
voleb	volba	k1gFnPc2	volba
též	též	k9	též
neuznala	uznat	k5eNaPmAgFnS	uznat
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
a	a	k8xC	a
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
65	[number]	k4	65
<g/>
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
horského	horský	k2eAgInSc2d1	horský
charakteru	charakter	k1gInSc2	charakter
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
1000	[number]	k4	1000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Toto	tento	k3xDgNnSc4	tento
pohoří	pohoří	k1gNnSc4	pohoří
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
karibskému	karibský	k2eAgNnSc3d1	Karibské
pobřeží	pobřeží	k1gNnSc3	pobřeží
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
snižuje	snižovat	k5eAaImIp3nS	snižovat
a	a	k8xC	a
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
nížinných	nížinný	k2eAgNnPc2d1	nížinné
údolí	údolí	k1gNnPc2	údolí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Valle	Valle	k1gFnSc1	Valle
de	de	k?	de
Sula	sout	k5eAaImAgFnS	sout
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
město	město	k1gNnSc1	město
San	San	k1gFnSc2	San
Pedro	Pedro	k1gNnSc4	Pedro
Sula	sout	k5eAaImAgFnS	sout
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spolu	spolu	k6eAd1	spolu
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
důležitou	důležitý	k2eAgFnSc4d1	důležitá
aglomeraci	aglomerace	k1gFnSc4	aglomerace
a	a	k8xC	a
představují	představovat	k5eAaImIp3nP	představovat
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
centrum	centrum	k1gNnSc4	centrum
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Hondurasu	Honduras	k1gInSc2	Honduras
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
46	[number]	k4	46
%	%	kIx~	%
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
lesními	lesní	k2eAgInPc7d1	lesní
porosty	porost	k1gInPc7	porost
<g/>
,	,	kIx,	,
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
půda	půda	k1gFnSc1	půda
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
15	[number]	k4	15
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
91	[number]	k4	91
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
o	o	k7c6	o
souhrnné	souhrnný	k2eAgFnSc6d1	souhrnná
rozloze	rozloha	k1gFnSc6	rozloha
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
40	[number]	k4	40
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Honduras	Honduras	k1gInSc1	Honduras
má	mít	k5eAaImIp3nS	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
dvěma	dva	k4xCgInPc3	dva
světovým	světový	k2eAgInPc3d1	světový
oceánům	oceán	k1gInPc3	oceán
-	-	kIx~	-
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
se	se	k3xPyFc4	se
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
honduraského	honduraský	k2eAgNnSc2d1	honduraské
pobřeží	pobřeží	k1gNnSc2	pobřeží
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
svým	svůj	k3xOyFgInSc7	svůj
zálivem	záliv	k1gInSc7	záliv
Fonseca	Fonseca	k1gFnSc1	Fonseca
<g/>
,	,	kIx,	,
Atlantik	Atlantik	k1gInSc1	Atlantik
u	u	k7c2	u
honduraského	honduraský	k2eAgNnSc2d1	honduraské
pobřeží	pobřeží	k1gNnSc2	pobřeží
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
do	do	k7c2	do
Honduraského	honduraský	k2eAgInSc2d1	honduraský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Významnější	významný	k2eAgFnPc1d2	významnější
řeky	řeka	k1gFnPc1	řeka
odvádějící	odvádějící	k2eAgFnSc4d1	odvádějící
vodu	voda	k1gFnSc4	voda
do	do	k7c2	do
Atlantiku	Atlantik	k1gInSc2	Atlantik
jsou	být	k5eAaImIp3nP	být
Coco	Coco	k6eAd1	Coco
<g/>
,	,	kIx,	,
Patuca	Patuca	k1gFnSc1	Patuca
a	a	k8xC	a
Ulúa	Ulúa	k1gFnSc1	Ulúa
<g/>
,	,	kIx,	,
do	do	k7c2	do
Pacifiku	Pacifik	k1gInSc2	Pacifik
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
např.	např.	kA	např.
řeka	řeka	k1gFnSc1	řeka
Choluteca	Choluteca	k1gFnSc1	Choluteca
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
přírodní	přírodní	k2eAgNnSc1d1	přírodní
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
Yojoa	Yojoa	k1gFnSc1	Yojoa
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Hondurasu	Honduras	k1gInSc3	Honduras
patří	patřit	k5eAaImIp3nP	patřit
několik	několik	k4yIc1	několik
menších	malý	k2eAgInPc2d2	menší
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
o	o	k7c6	o
souostroví	souostroví	k1gNnSc6	souostroví
Islas	Islasa	k1gFnPc2	Islasa
de	de	k?	de
la	la	k1gNnSc6	la
Bahía	Bahí	k1gInSc2	Bahí
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Ostrovy	ostrov	k1gInPc1	ostrov
zálivu	záliv	k1gInSc2	záliv
<g/>
)	)	kIx)	)
s	s	k7c7	s
ostrovy	ostrov	k1gInPc4	ostrov
Roatán	Roatán	k2eAgInSc1d1	Roatán
<g/>
,	,	kIx,	,
Utila	Utila	k1gMnSc1	Utila
a	a	k8xC	a
Guanaja	Guanaja	k1gMnSc1	Guanaja
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Roatán	Roatán	k1gMnSc1	Roatán
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
brněnského	brněnský	k2eAgMnSc2d1	brněnský
podnikatele	podnikatel	k1gMnSc2	podnikatel
Jiřího	Jiří	k1gMnSc2	Jiří
Černého	Černý	k1gMnSc2	Černý
malá	malý	k2eAgFnSc1d1	malá
československá	československý	k2eAgFnSc1d1	Československá
komunita	komunita	k1gFnSc1	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Honduras	Honduras	k1gInSc1	Honduras
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
chudším	chudý	k2eAgInPc3d2	chudší
státům	stát	k1gInPc3	stát
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
hodnoty	hodnota	k1gFnSc2	hodnota
4	[number]	k4	4
344	[number]	k4	344
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Honduras	Honduras	k1gInSc1	Honduras
je	být	k5eAaImIp3nS	být
členským	členský	k2eAgInSc7d1	členský
státem	stát	k1gInSc7	stát
DR-CAFTA	DR-CAFTA	k1gFnSc2	DR-CAFTA
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
honduraského	honduraský	k2eAgInSc2d1	honduraský
exportu	export	k1gInSc2	export
tvoří	tvořit	k5eAaImIp3nP	tvořit
zemědělské	zemědělský	k2eAgInPc1d1	zemědělský
produkty	produkt	k1gInPc1	produkt
-	-	kIx~	-
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
banány	banán	k1gInPc1	banán
<g/>
,	,	kIx,	,
palmový	palmový	k2eAgInSc1d1	palmový
olej	olej	k1gInSc1	olej
<g/>
,	,	kIx,	,
mořské	mořský	k2eAgInPc1d1	mořský
plody	plod	k1gInPc1	plod
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
vývozním	vývozní	k2eAgInSc7d1	vývozní
artiklem	artikl	k1gInSc7	artikl
jsou	být	k5eAaImIp3nP	být
nepoužité	použitý	k2eNgFnPc4d1	nepoužitá
poštovní	poštovní	k2eAgFnPc4d1	poštovní
známky	známka	k1gFnPc4	známka
<g/>
,	,	kIx,	,
bankovky	bankovka	k1gFnPc4	bankovka
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
dokumenty	dokument	k1gInPc4	dokument
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
a	a	k8xC	a
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Latinskoamerická	latinskoamerický	k2eAgFnSc1d1	latinskoamerická
integrace	integrace	k1gFnSc1	integrace
<g/>
.	.	kIx.	.
</s>
<s>
Honduras	Honduras	k1gInSc1	Honduras
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
několika	několik	k4yIc2	několik
globálních	globální	k2eAgFnPc2d1	globální
organizací	organizace	k1gFnPc2	organizace
např.	např.	kA	např.
Organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
výživu	výživa	k1gFnSc4	výživa
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
,	,	kIx,	,
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
UNESCO	Unesco	k1gNnSc1	Unesco
nebo	nebo	k8xC	nebo
Světová	světový	k2eAgFnSc1d1	světová
obchodní	obchodní	k2eAgFnSc1d1	obchodní
organizace	organizace	k1gFnSc1	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
regionálních	regionální	k2eAgFnPc2d1	regionální
organizací	organizace	k1gFnPc2	organizace
působících	působící	k2eAgFnPc2d1	působící
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentu	kontinent	k1gInSc6	kontinent
jako	jako	k8xS	jako
např.	např.	kA	např.
Společenství	společenství	k1gNnSc1	společenství
latinskoamerických	latinskoamerický	k2eAgInPc2d1	latinskoamerický
a	a	k8xC	a
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Latinskoamerický	latinskoamerický	k2eAgInSc1d1	latinskoamerický
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
Sdružení	sdružení	k1gNnSc1	sdružení
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Středoamerický	středoamerický	k2eAgInSc1d1	středoamerický
integrační	integrační	k2eAgInSc1d1	integrační
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
Organizace	organizace	k1gFnSc1	organizace
amerických	americký	k2eAgInPc2d1	americký
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Petrocaribe	Petrocarib	k1gMnSc5	Petrocarib
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
zejména	zejména	k9	zejména
z	z	k7c2	z
mesticů	mestic	k1gMnPc2	mestic
a	a	k8xC	a
indiánů	indián	k1gMnPc2	indián
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
složení	složení	k1gNnSc1	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
výrazně	výrazně	k6eAd1	výrazně
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
politické	politický	k2eAgFnSc3d1	politická
nestabilitě	nestabilita	k1gFnSc3	nestabilita
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
