<s>
Hertz	hertz	k1gInSc1
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Hertz	hertz	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hertz	hertz	k1gInSc1
(	(	kIx(
<g/>
značka	značka	k1gFnSc1
Hz	Hz	kA
<g/>
;	;	kIx,
celým	celý	k2eAgNnSc7d1
slovem	slovo	k1gNnSc7
hertz	hertz	k1gInSc1
<g/>
,	,	kIx,
s	s	k7c7
malým	malý	k2eAgInSc7d1
h	h	k?
<g/>
;	;	kIx,
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
herc	herc	k1gInSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednotkou	jednotka	k1gFnSc7
frekvence	frekvence	k1gFnSc2
(	(	kIx(
<g/>
kmitočtu	kmitočet	k1gInSc2
<g/>
)	)	kIx)
v	v	k7c6
soustavě	soustava	k1gFnSc6
SI	SI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
odvozenou	odvozený	k2eAgFnSc4d1
jednotku	jednotka	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
<g/>
,	,	kIx,
kolik	kolik	k4yRc4,k4yIc4,k4yQc4
cyklických	cyklický	k2eAgFnPc2d1
(	(	kIx(
<g/>
pravidelně	pravidelně	k6eAd1
se	se	k3xPyFc4
opakujících	opakující	k2eAgInPc2d1
<g/>
)	)	kIx)
dějů	děj	k1gInPc2
se	se	k3xPyFc4
odehraje	odehrát	k5eAaPmIp3nS
za	za	k7c4
jednu	jeden	k4xCgFnSc4
sekundu	sekunda	k1gFnSc4
<g/>
;	;	kIx,
vyjádření	vyjádření	k1gNnSc4
v	v	k7c6
základních	základní	k2eAgFnPc6d1
jednotkách	jednotka	k1gFnPc6
je	být	k5eAaImIp3nS
tedy	tedy	k9
s	s	k7c7
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jednotka	jednotka	k1gFnSc1
je	být	k5eAaImIp3nS
pojmenována	pojmenován	k2eAgFnSc1d1
podle	podle	k7c2
německého	německý	k2eAgMnSc2d1
fyzika	fyzik	k1gMnSc2
Heinricha	Heinrich	k1gMnSc2
Hertze	hertz	k1gInSc5
<g/>
,	,	kIx,
badatele	badatel	k1gMnPc4
a	a	k8xC
objevitele	objevitel	k1gMnPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
elektromagnetických	elektromagnetický	k2eAgFnPc2d1
vln	vlna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Příklady	příklad	k1gInPc1
</s>
<s>
U	u	k7c2
střídavého	střídavý	k2eAgInSc2d1
proudu	proud	k1gInSc2
se	se	k3xPyFc4
v	v	k7c4
běžné	běžný	k2eAgNnSc4d1
elektrické	elektrický	k2eAgNnSc4d1
sítí	sítí	k1gNnSc4
používají	používat	k5eAaImIp3nP
frekvence	frekvence	k1gFnSc1
50	#num#	k4
Hz	Hz	kA
(	(	kIx(
<g/>
Evropa	Evropa	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
60	#num#	k4
Hz	Hz	kA
(	(	kIx(
<g/>
Amerika	Amerika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektrické	elektrický	k2eAgInPc1d1
rozvody	rozvod	k1gInPc1
lodí	loď	k1gFnPc2
a	a	k8xC
letadel	letadlo	k1gNnPc2
často	často	k6eAd1
používají	používat	k5eAaImIp3nP
zvýšenou	zvýšený	k2eAgFnSc4d1
frekvenci	frekvence	k1gFnSc4
400	#num#	k4
Hz	Hz	kA
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
zvuku	zvuk	k1gInSc2
je	být	k5eAaImIp3nS
rozsah	rozsah	k1gInSc1
člověkem	člověk	k1gMnSc7
slyšitelných	slyšitelný	k2eAgFnPc2d1
frekvencí	frekvence	k1gFnPc2
zhruba	zhruba	k6eAd1
20	#num#	k4
Hz	Hz	kA
až	až	k9
20	#num#	k4
kHz	khz	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komorní	komorní	k2eAgInSc4d1
A	a	k8xC
(	(	kIx(
<g/>
standardně	standardně	k6eAd1
užívaný	užívaný	k2eAgInSc1d1
referenční	referenční	k2eAgInSc1d1
tón	tón	k1gInSc1
pro	pro	k7c4
ladění	ladění	k1gNnSc4
hudebních	hudební	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
frekvenci	frekvence	k1gFnSc4
440	#num#	k4
Hz	Hz	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každé	každý	k3xTgNnSc1
zdvojnásobení	zdvojnásobení	k1gNnSc1
frekvence	frekvence	k1gFnSc2
znamená	znamenat	k5eAaImIp3nS
zvýšení	zvýšení	k1gNnSc1
tónu	tón	k1gInSc2
o	o	k7c6
oktávu	oktáv	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Násobné	násobný	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
</s>
<s>
milihertz	milihertz	k1gInSc1
(	(	kIx(
<g/>
značka	značka	k1gFnSc1
mHz	mHz	k?
<g/>
)	)	kIx)
se	se	k3xPyFc4
rovná	rovnat	k5eAaImIp3nS
10	#num#	k4
<g/>
−	−	k?
<g/>
3	#num#	k4
Hz	Hz	kA
(	(	kIx(
<g/>
0,001	0,001	k4
Hz	Hz	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Této	tento	k3xDgFnSc3
frekvenci	frekvence	k1gFnSc3
odpovídá	odpovídat	k5eAaImIp3nS
perioda	perioda	k1gFnSc1
1000	#num#	k4
sekund	sekunda	k1gFnPc2
(	(	kIx(
<g/>
tj.	tj.	kA
16	#num#	k4
<g/>
⅔	⅔	k?
minuty	minuta	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
kilohertz	kilohertz	k1gInSc1
(	(	kIx(
<g/>
značka	značka	k1gFnSc1
kHz	khz	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
rovná	rovnat	k5eAaImIp3nS
103	#num#	k4
Hz	Hz	kA
(	(	kIx(
<g/>
1	#num#	k4
000	#num#	k4
Hz	Hz	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Této	tento	k3xDgFnSc3
frekvenci	frekvence	k1gFnSc3
odpovídá	odpovídat	k5eAaImIp3nS
perioda	perioda	k1gFnSc1
1	#num#	k4
ms.	ms.	k?
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
např.	např.	kA
pro	pro	k7c4
udávání	udávání	k1gNnSc4
frekvence	frekvence	k1gFnSc2
elektroakustického	elektroakustický	k2eAgInSc2d1
signálu	signál	k1gInSc2
či	či	k8xC
v	v	k7c6
akustice	akustika	k1gFnSc6
u	u	k7c2
vyšších	vysoký	k2eAgFnPc2d2
frekvencí	frekvence	k1gFnPc2
zvuku	zvuk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
megahertz	megahertz	k1gInSc1
(	(	kIx(
<g/>
značka	značka	k1gFnSc1
MHz	Mhz	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
rovná	rovnat	k5eAaImIp3nS
106	#num#	k4
Hz	Hz	kA
(	(	kIx(
<g/>
1	#num#	k4
milion	milion	k4xCgInSc4
Hz	Hz	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Této	tento	k3xDgFnSc3
frekvenci	frekvence	k1gFnSc3
odpovídá	odpovídat	k5eAaImIp3nS
perioda	perioda	k1gFnSc1
1	#num#	k4
µ	µ	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
např.	např.	kA
pro	pro	k7c4
udávání	udávání	k1gNnSc4
frekvence	frekvence	k1gFnSc2
radiotelevizního	radiotelevizní	k2eAgInSc2d1
či	či	k8xC
telekomunikačního	telekomunikační	k2eAgInSc2d1
signálu	signál	k1gInSc2
nebo	nebo	k8xC
pro	pro	k7c4
údaj	údaj	k1gInSc4
o	o	k7c4
taktovací	taktovací	k2eAgFnSc4d1
frekvenci	frekvence	k1gFnSc4
procesoru	procesor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
gigahertz	gigahertz	k1gInSc1
(	(	kIx(
<g/>
značka	značka	k1gFnSc1
GHz	GHz	k1gFnSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
rovná	rovnat	k5eAaImIp3nS
109	#num#	k4
Hz	Hz	kA
(	(	kIx(
<g/>
1	#num#	k4
miliarda	miliarda	k4xCgFnSc1
Hz	Hz	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Této	tento	k3xDgFnSc3
frekvenci	frekvence	k1gFnSc3
odpovídá	odpovídat	k5eAaImIp3nS
perioda	perioda	k1gFnSc1
1	#num#	k4
ns	ns	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
např.	např.	kA
pro	pro	k7c4
udávání	udávání	k1gNnSc4
frekvence	frekvence	k1gFnSc2
signálu	signál	k1gInSc2
a	a	k8xC
pro	pro	k7c4
popis	popis	k1gInSc4
taktovací	taktovací	k2eAgFnSc2d1
frekvence	frekvence	k1gFnSc2
rychlých	rychlý	k2eAgInPc2d1
mikroprocesorů	mikroprocesor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektromagnetické	elektromagnetický	k2eAgNnSc4d1
vlnění	vlnění	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
řádově	řádově	k6eAd1
pohybuje	pohybovat	k5eAaImIp3nS
v	v	k7c6
těchto	tento	k3xDgFnPc6
a	a	k8xC
vyšších	vysoký	k2eAgFnPc6d2
frekvencích	frekvence	k1gFnPc6
(	(	kIx(
<g/>
centimetrové	centimetrový	k2eAgFnSc2d1
a	a	k8xC
milimetrové	milimetrový	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	se	k3xPyFc4
šíří	šířit	k5eAaImIp3nS
převážně	převážně	k6eAd1
po	po	k7c6
přímce	přímka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
pro	pro	k7c4
mobilní	mobilní	k2eAgInPc4d1
telefony	telefon	k1gInPc4
<g/>
,	,	kIx,
mikrovlnné	mikrovlnný	k2eAgFnPc4d1
trouby	trouba	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
radarech	radar	k1gInPc6
aj.	aj.	kA
</s>
<s>
terahertz	terahertz	k1gInSc1
(	(	kIx(
<g/>
značka	značka	k1gFnSc1
THz	THz	k1gFnSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
rovná	rovnat	k5eAaImIp3nS
1012	#num#	k4
Hz	Hz	kA
(	(	kIx(
<g/>
1	#num#	k4
bilion	bilion	k4xCgInSc4
Hz	Hz	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
touto	tento	k3xDgFnSc7
jednotkou	jednotka	k1gFnSc7
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
se	se	k3xPyFc4
setkat	setkat	k5eAaPmF
např.	např.	kA
v	v	k7c6
oblasti	oblast	k1gFnSc6
fyziky	fyzika	k1gFnSc2
záření	záření	k1gNnSc2
(	(	kIx(
<g/>
viditelné	viditelný	k2eAgNnSc1d1
světlo	světlo	k1gNnSc1
<g/>
,	,	kIx,
gama	gama	k1gNnSc1
záření	záření	k1gNnSc2
<g/>
,	,	kIx,
Rentgenové	rentgenový	k2eAgNnSc1d1
záření	záření	k1gNnSc1
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
petahertz	petahertz	k1gInSc1
(	(	kIx(
<g/>
značka	značka	k1gFnSc1
PHz	PHz	k1gFnSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
rovná	rovnat	k5eAaImIp3nS
1015	#num#	k4
Hz	Hz	kA
(	(	kIx(
<g/>
1	#num#	k4
biliarda	biliarda	k4xCgFnSc1
Hz	Hz	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
exahertz	exahertz	k1gInSc1
(	(	kIx(
<g/>
značka	značka	k1gFnSc1
EHz	EHz	k1gFnSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
rovná	rovnat	k5eAaImIp3nS
1018	#num#	k4
Hz	Hz	kA
(	(	kIx(
<g/>
1	#num#	k4
trilion	trilion	k4xCgInSc4
Hz	Hz	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
častěji	často	k6eAd2
v	v	k7c6
kvantové	kvantový	k2eAgFnSc6d1
fyzice	fyzika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Název	název	k1gInSc1
jednotky	jednotka	k1gFnSc2
hertz	hertz	k1gInSc1
byl	být	k5eAaImAgInS
přijat	přijmout	k5eAaPmNgInS
organizací	organizace	k1gFnSc7
CGPM	CGPM	kA
(	(	kIx(
<g/>
Conférence	Conférenec	k1gMnSc2
générale	générale	k6eAd1
des	des	k1gNnSc4
poids	poids	k6eAd1
et	et	k?
mesures	mesures	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
a	a	k8xC
nahradil	nahradit	k5eAaPmAgInS
předchozí	předchozí	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
„	„	k?
<g/>
cykly	cyklus	k1gInPc4
za	za	k7c4
sekundu	sekunda	k1gFnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
značeno	značen	k2eAgNnSc1d1
c	c	k0
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
,	,	kIx,
cykl	cyklit	k5eAaPmRp2nS
<g/>
/	/	kIx~
<g/>
s	s	k7c7
nebo	nebo	k8xC
cps	cps	k?
(	(	kIx(
<g/>
cycles	cyclesa	k1gFnPc2
per	pero	k1gNnPc2
second	second	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
příslušnými	příslušný	k2eAgInPc7d1
násobky	násobek	k1gInPc7
(	(	kIx(
<g/>
kilocyklů	kilocykl	k1gInPc2
<g/>
,	,	kIx,
megacykly	megacyknout	k5eAaPmAgFnP
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Frekvence	frekvence	k1gFnSc1
</s>
<s>
Frekvenční	frekvenční	k2eAgNnSc1d1
spektrum	spektrum	k1gNnSc1
</s>
<s>
Otáčky	otáčka	k1gFnPc1
za	za	k7c4
minutu	minuta	k1gFnSc4
</s>
<s>
Úhlová	úhlový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
hertz	hertz	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Soustava	soustava	k1gFnSc1
SI	si	k1gNnSc2
základní	základní	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
</s>
<s>
ampér	ampér	k1gInSc1
</s>
<s>
kandela	kandela	k1gFnSc1
</s>
<s>
kelvin	kelvin	k1gInSc1
</s>
<s>
kilogram	kilogram	k1gInSc1
</s>
<s>
metr	metr	k1gInSc1
</s>
<s>
mol	mol	k1gMnSc1
</s>
<s>
sekunda	sekunda	k1gFnSc1
odvozené	odvozený	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
</s>
<s>
becquerel	becquerel	k1gInSc1
</s>
<s>
coulomb	coulomb	k1gInSc1
</s>
<s>
farad	farad	k1gInSc1
</s>
<s>
gray	graa	k1gFnPc1
</s>
<s>
henry	henry	k1gInSc1
</s>
<s>
hertz	hertz	k1gInSc1
</s>
<s>
joule	joule	k1gInSc1
</s>
<s>
katal	katal	k1gMnSc1
</s>
<s>
lumen	lumen	k1gMnSc1
</s>
<s>
lux	lux	k1gInSc1
</s>
<s>
newton	newton	k1gInSc1
</s>
<s>
ohm	ohm	k1gInSc1
</s>
<s>
pascal	pascal	k1gInSc1
</s>
<s>
radián	radián	k1gInSc1
</s>
<s>
siemens	siemens	k1gInSc1
</s>
<s>
sievert	sievert	k1gMnSc1
</s>
<s>
steradián	steradián	k1gInSc1
</s>
<s>
stupeň	stupeň	k1gInSc1
Celsia	Celsius	k1gMnSc2
</s>
<s>
tesla	tesla	k1gFnSc1
</s>
<s>
volt	volt	k1gInSc1
</s>
<s>
watt	watt	k1gInSc1
</s>
<s>
weber	weber	k1gInSc1
další	další	k2eAgFnSc2d1
</s>
<s>
předpony	předpona	k1gFnPc1
soustavy	soustava	k1gFnSc2
SI	si	k1gNnSc2
</s>
<s>
systémy	systém	k1gInPc1
měření	měření	k1gNnSc2
</s>
<s>
převody	převod	k1gInPc1
jednotek	jednotka	k1gFnPc2
</s>
<s>
nové	nový	k2eAgFnPc1d1
definice	definice	k1gFnPc1
SI	se	k3xPyFc3
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
míry	míra	k1gFnPc4
a	a	k8xC
váhy	váha	k1gFnPc4
</s>
