<s>
Hertz	hertz	k1gInSc1
(	(	kIx(
<g/>
značka	značka	k1gFnSc1
Hz	Hz	kA
<g/>
;	;	kIx,
celým	celý	k2eAgNnSc7d1
slovem	slovo	k1gNnSc7
hertz	hertz	k1gInSc1
<g/>
,	,	kIx,
s	s	k7c7
malým	malý	k2eAgInSc7d1
h	h	k?
<g/>
;	;	kIx,
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
herc	herc	k1gInSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednotkou	jednotka	k1gFnSc7
frekvence	frekvence	k1gFnSc2
(	(	kIx(
<g/>
kmitočtu	kmitočet	k1gInSc2
<g/>
)	)	kIx)
v	v	k7c6
soustavě	soustava	k1gFnSc6
SI	SI	kA
<g/>
.	.	kIx.
</s>