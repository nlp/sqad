<s>
Mabel	Mabel	k1gInSc1
Gayová	Gayová	k1gFnSc1
</s>
<s>
Mabel	Mabet	k5eAaPmAgInS,k5eAaBmAgInS,k5eAaImAgInS
Gayová	Gayový	k2eAgNnPc4d1
Narození	narození	k1gNnPc4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1983	#num#	k4
(	(	kIx(
<g/>
38	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Santiago	Santiago	k1gNnSc1
de	de	k?
Cuba	Cub	k1gInSc2
Povolání	povolání	k1gNnPc2
</s>
<s>
atletka	atletka	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
2009	#num#	k4
Berlín	Berlín	k1gInSc1
</s>
<s>
trojskok	trojskok	k1gInSc1
</s>
<s>
Halové	halový	k2eAgFnPc4d1
MS	MS	kA
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
2012	#num#	k4
Istanbul	Istanbul	k1gInSc1
</s>
<s>
trojskok	trojskok	k1gInSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
1999	#num#	k4
Bydhošť	Bydhošť	k1gFnSc1
</s>
<s>
trojskok	trojskok	k1gInSc1
</s>
<s>
Mabel	Mabel	k1gMnSc1
Gay	gay	k1gMnSc1
Tamayo	Tamayo	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
5	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1983	#num#	k4
<g/>
,	,	kIx,
Santiago	Santiago	k1gNnSc1
de	de	k?
Cuba	Cub	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kubánská	kubánský	k2eAgFnSc1d1
atletka	atletka	k1gFnSc1
<g/>
,	,	kIx,
reprezentantka	reprezentantka	k1gFnSc1
v	v	k7c6
trojskoku	trojskok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Kariéra	kariéra	k1gFnSc1
</s>
<s>
První	první	k4xOgInSc4
úspěch	úspěch	k1gInSc4
zaznamenala	zaznamenat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
v	v	k7c6
Bydhošti	Bydhošti	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
získala	získat	k5eAaPmAgFnS
zlatou	zlatý	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
skončila	skončit	k5eAaPmAgFnS
čtvrtá	čtvrtá	k1gFnSc1
na	na	k7c6
juniorském	juniorský	k2eAgNnSc6d1
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
v	v	k7c4
Santiago	Santiago	k1gNnSc4
de	de	k?
Chile	Chile	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
v	v	k7c6
Kingstonu	Kingston	k1gInSc6
již	již	k6eAd1
získala	získat	k5eAaPmAgFnS
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
zlatou	zlatý	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
světovém	světový	k2eAgInSc6d1
šampionátu	šampionát	k1gInSc6
v	v	k7c6
Paříži	Paříž	k1gFnSc6
2003	#num#	k4
se	se	k3xPyFc4
umístila	umístit	k5eAaPmAgFnS
ve	v	k7c6
finále	finále	k1gNnSc6
na	na	k7c6
pátém	pátý	k4xOgNnSc6
místě	místo	k1gNnSc6
(	(	kIx(
<g/>
14,52	14,52	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
témž	týž	k3xTgInSc6
roce	rok	k1gInSc6
získala	získat	k5eAaPmAgFnS
zlato	zlato	k1gNnSc4
na	na	k7c6
Panamerických	panamerický	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c4
Santo	Santo	k1gNnSc4
Domingu	Doming	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
halovém	halový	k2eAgInSc6d1
MS	MS	kA
2004	#num#	k4
v	v	k7c6
Budapešti	Budapešť	k1gFnSc6
skončila	skončit	k5eAaPmAgFnS
devátá	devátý	k4xOgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
vybojovala	vybojovat	k5eAaPmAgFnS
na	na	k7c6
Panamerických	panamerický	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
brazilském	brazilský	k2eAgMnSc6d1
Rio	Rio	k1gMnSc6
de	de	k?
Janeiru	Janeira	k1gMnSc4
bronz	bronz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Reprezentovala	reprezentovat	k5eAaImAgFnS
na	na	k7c6
letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
Pekingu	Peking	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
kvalifikační	kvalifikační	k2eAgInSc1d1
skok	skok	k1gInSc1
14,09	14,09	k4
m	m	kA
však	však	k9
na	na	k7c4
postup	postup	k1gInSc4
do	do	k7c2
finále	finále	k1gNnSc2
nestačil	stačit	k5eNaBmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
získala	získat	k5eAaPmAgFnS
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
v	v	k7c6
Berlíně	Berlín	k1gInSc6
stříbrnou	stříbrný	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
(	(	kIx(
<g/>
14,61	14,61	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finále	finále	k1gNnSc6
prohrála	prohrát	k5eAaPmAgFnS
jen	jen	k9
s	s	k7c7
krajankou	krajanka	k1gFnSc7
Yargelis	Yargelis	k1gFnSc7
Savigneovou	Savigneův	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
zvítězila	zvítězit	k5eAaPmAgFnS
výkonem	výkon	k1gInSc7
14,62	14,62	k4
m	m	kA
na	na	k7c6
světovém	světový	k2eAgNnSc6d1
atletickém	atletický	k2eAgNnSc6d1
finále	finále	k1gNnSc6
v	v	k7c6
Soluni	Soluň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
skončila	skončit	k5eAaPmAgFnS
na	na	k7c6
halovém	halový	k2eAgNnSc6d1
MS	MS	kA
v	v	k7c6
katarském	katarský	k2eAgInSc6d1
Dauhá	Dauhý	k2eAgFnSc1d1
na	na	k7c6
pátém	pátý	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Osobní	osobní	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
</s>
<s>
hala	hala	k1gFnSc1
–	–	k?
14,57	14,57	k4
m	m	kA
–	–	k?
5	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2004	#num#	k4
<g/>
,	,	kIx,
Budapešť	Budapešť	k1gFnSc1
</s>
<s>
venku	venku	k6eAd1
–	–	k?
14,66	14,66	k4
m	m	kA
–	–	k?
8	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2007	#num#	k4
<g/>
,	,	kIx,
Havana	Havana	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Mabel	Mabela	k1gFnPc2
Gayová	Gayová	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Mabel	Mabel	k1gMnSc1
Gayová	Gayová	k1gFnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
IAAF	IAAF	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Sport	sport	k1gInSc1
|	|	kIx~
Střední	střední	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
a	a	k8xC
Karibik	Karibik	k1gInSc1
</s>
