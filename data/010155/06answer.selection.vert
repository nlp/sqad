<s>
Chirurgie	chirurgie	k1gFnSc1	chirurgie
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
krku	krk	k1gInSc2	krk
je	být	k5eAaImIp3nS	být
souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
lékařský	lékařský	k2eAgInSc1d1	lékařský
termín	termín	k1gInSc1	termín
<g/>
,	,	kIx,	,
vyplývající	vyplývající	k2eAgInPc1d1	vyplývající
z	z	k7c2	z
anatomické	anatomický	k2eAgFnSc2d1	anatomická
lokalizace	lokalizace	k1gFnSc2	lokalizace
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgNnSc6	který
daný	daný	k2eAgInSc1d1	daný
lékařský	lékařský	k2eAgInSc1d1	lékařský
obor	obor	k1gInSc1	obor
provádí	provádět	k5eAaImIp3nS	provádět
svou	svůj	k3xOyFgFnSc4	svůj
léčbu	léčba	k1gFnSc4	léčba
<g/>
.	.	kIx.	.
</s>
