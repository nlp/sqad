<p>
<s>
Chirurgie	chirurgie	k1gFnSc1	chirurgie
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
krku	krk	k1gInSc2	krk
je	být	k5eAaImIp3nS	být
souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
lékařský	lékařský	k2eAgInSc1d1	lékařský
termín	termín	k1gInSc1	termín
<g/>
,	,	kIx,	,
vyplývající	vyplývající	k2eAgInPc1d1	vyplývající
z	z	k7c2	z
anatomické	anatomický	k2eAgFnSc2d1	anatomická
lokalizace	lokalizace	k1gFnSc2	lokalizace
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgNnSc6	který
daný	daný	k2eAgInSc1d1	daný
lékařský	lékařský	k2eAgInSc1d1	lékařský
obor	obor	k1gInSc1	obor
provádí	provádět	k5eAaImIp3nS	provádět
svou	svůj	k3xOyFgFnSc4	svůj
léčbu	léčba	k1gFnSc4	léčba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Majoritními	majoritní	k2eAgFnPc7d1	majoritní
odbornostmi	odbornost	k1gFnPc7	odbornost
zabývajícími	zabývající	k2eAgFnPc7d1	zabývající
se	se	k3xPyFc4	se
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
chirurgií	chirurgie	k1gFnPc2	chirurgie
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
krku	krk	k1gInSc2	krk
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Maxilofaciální	Maxilofaciální	k2eAgFnSc1d1	Maxilofaciální
chirurgie	chirurgie	k1gFnSc1	chirurgie
a	a	k8xC	a
orální	orální	k2eAgFnSc1d1	orální
a	a	k8xC	a
maxilofaciální	maxilofaciální	k2eAgFnSc1d1	maxilofaciální
chirurgie	chirurgie	k1gFnSc1	chirurgie
</s>
</p>
<p>
<s>
Otorinolaryngologie	otorinolaryngologie	k1gFnSc1	otorinolaryngologie
a	a	k8xC	a
chirurgie	chirurgie	k1gFnSc1	chirurgie
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
krkuV	krkuV	k?	krkuV
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnPc1	slovo
smyslu	smysl	k1gInSc2	smysl
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
krku	krk	k1gInSc6	krk
provádí	provádět	k5eAaImIp3nP	provádět
chirugické	chirugický	k2eAgInPc4d1	chirugický
výkony	výkon	k1gInPc4	výkon
též	též	k9	též
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Neurochirurgie	neurochirurgie	k1gFnSc1	neurochirurgie
</s>
</p>
<p>
<s>
Oftalmologie	oftalmologie	k1gFnSc1	oftalmologie
</s>
</p>
<p>
<s>
Plastická	plastický	k2eAgFnSc1d1	plastická
chirurgie	chirurgie	k1gFnSc1	chirurgie
</s>
</p>
<p>
<s>
Spondylochirurgie	Spondylochirurgie	k1gFnSc1	Spondylochirurgie
</s>
</p>
<p>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
chirurgie	chirurgie	k1gFnSc1	chirurgie
</s>
</p>
