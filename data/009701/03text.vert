<p>
<s>
Jordanovská	Jordanovský	k2eAgFnSc1d1	Jordanovský
kultura	kultura	k1gFnSc1	kultura
či	či	k8xC	či
Jordanovská	Jordanovský	k2eAgFnSc1d1	Jordanovský
skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
pravěká	pravěký	k2eAgFnSc1d1	pravěká
kultura	kultura	k1gFnSc1	kultura
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
koncem	koncem	k7c2	koncem
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
na	na	k7c6	na
konci	konec	k1gInSc6	konec
neolitu	neolit	k1gInSc2	neolit
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
v	v	k7c6	v
polském	polský	k2eAgNnSc6d1	polské
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
starším	starý	k2eAgInSc7d2	starší
eneolitem	eneolit	k1gInSc7	eneolit
mizí	mizet	k5eAaImIp3nS	mizet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
polské	polský	k2eAgFnSc2d1	polská
obce	obec	k1gFnSc2	obec
Jordanów	Jordanów	k1gFnSc2	Jordanów
Šląski	Šląsk	k1gFnSc2	Šląsk
<g/>
.	.	kIx.	.
</s>
<s>
Rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
se	se	k3xPyFc4	se
z	z	k7c2	z
pozdní	pozdní	k2eAgFnSc2d1	pozdní
lengyelské	lengyelský	k2eAgFnSc2d1	lengyelská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
kulturu	kultura	k1gFnSc4	kultura
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc1d1	typický
kostrové	kostrový	k2eAgInPc1d1	kostrový
a	a	k8xC	a
žárové	žárový	k2eAgInPc1d1	žárový
hroby	hrob	k1gInPc1	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Produkovala	produkovat	k5eAaImAgFnS	produkovat
také	také	k9	také
keramiku	keramika	k1gFnSc4	keramika
<g/>
,	,	kIx,	,
džbánky	džbánek	k1gInPc1	džbánek
prosté	prostý	k2eAgInPc1d1	prostý
i	i	k8xC	i
dvouuché	dvouuchý	k2eAgInPc4d1	dvouuchý
<g/>
,	,	kIx,	,
široké	široký	k2eAgInPc4d1	široký
poháry	pohár	k1gInPc4	pohár
a	a	k8xC	a
mísy	mísa	k1gFnPc4	mísa
(	(	kIx(	(
<g/>
i	i	k9	i
na	na	k7c6	na
nožce	nožka	k1gFnSc6	nožka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výzdoba	výzdoba	k1gFnSc1	výzdoba
keramiky	keramika	k1gFnSc2	keramika
je	být	k5eAaImIp3nS	být
rytá	rytý	k2eAgFnSc1d1	rytá
a	a	k8xC	a
žlábkovaná	žlábkovaný	k2eAgFnSc1d1	žlábkovaná
<g/>
,	,	kIx,	,
převládají	převládat	k5eAaImIp3nP	převládat
klikatkové	klikatkový	k2eAgInPc1d1	klikatkový
vzory	vzor	k1gInPc1	vzor
a	a	k8xC	a
brázděný	brázděný	k2eAgInSc1d1	brázděný
vpich	vpich	k1gInSc1	vpich
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
patrně	patrně	k6eAd1	patrně
o	o	k7c4	o
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c4	na
naše	náš	k3xOp1gNnSc4	náš
území	území	k1gNnSc4	území
zavedla	zavést	k5eAaPmAgFnS	zavést
užívání	užívání	k1gNnSc4	užívání
kovu	kov	k1gInSc2	kov
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejspíše	nejspíše	k9	nejspíše
z	z	k7c2	z
importu	import	k1gInSc2	import
<g/>
,	,	kIx,	,
archeologové	archeolog	k1gMnPc1	archeolog
nalezli	nalézt	k5eAaBmAgMnP	nalézt
v	v	k7c6	v
jordanovských	jordanovský	k2eAgNnPc6d1	jordanovský
sídlištích	sídliště	k1gNnPc6	sídliště
drobné	drobný	k2eAgFnSc2d1	drobná
ozdoby	ozdoba	k1gFnSc2	ozdoba
z	z	k7c2	z
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
spirálky	spirálka	k1gFnPc1	spirálka
do	do	k7c2	do
náhrdelníků	náhrdelník	k1gInPc2	náhrdelník
či	či	k8xC	či
závěsky	závěska	k1gFnSc2	závěska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Sklenář	Sklenář	k1gMnSc1	Sklenář
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
,	,	kIx,	,
Sklenářová	Sklenářová	k1gFnSc1	Sklenářová
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
<g/>
,	,	kIx,	,
Slabina	slabina	k1gFnSc1	slabina
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
pravěku	pravěk	k1gInSc2	pravěk
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
M.	M.	kA	M.
Zápotocký	Zápotocký	k1gMnSc1	Zápotocký
<g/>
,	,	kIx,	,
Jordanovská	Jordanovský	k2eAgFnSc1d1	Jordanovský
kultura	kultura	k1gFnSc1	kultura
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Archeologický	archeologický	k2eAgInSc1d1	archeologický
ústav	ústav	k1gInSc1	ústav
</s>
</p>
