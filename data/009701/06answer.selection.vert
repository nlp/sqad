<s>
Jordanovská	Jordanovský	k2eAgFnSc1d1	Jordanovský
kultura	kultura	k1gFnSc1	kultura
či	či	k8xC	či
Jordanovská	Jordanovský	k2eAgFnSc1d1	Jordanovský
skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
pravěká	pravěký	k2eAgFnSc1d1	pravěká
kultura	kultura	k1gFnSc1	kultura
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
koncem	koncem	k7c2	koncem
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
na	na	k7c6	na
konci	konec	k1gInSc6	konec
neolitu	neolit	k1gInSc2	neolit
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
v	v	k7c6	v
polském	polský	k2eAgNnSc6d1	polské
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
</s>
