<s>
Identifikátory	identifikátor	k1gInPc1	identifikátor
uživatelů	uživatel	k1gMnPc2	uživatel
(	(	kIx(	(
<g/>
takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
Jabber	Jabber	k1gInSc4	Jabber
ID	Ida	k1gFnPc2	Ida
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
JID	JID	kA	JID
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
základním	základní	k2eAgInSc6d1	základní
tvaru	tvar	k1gInSc6	tvar
syntakticky	syntakticky	k6eAd1	syntakticky
i	i	k9	i
sémanticky	sémanticky	k6eAd1	sémanticky
podobné	podobný	k2eAgFnPc4d1	podobná
e-mailovým	eailův	k2eAgFnPc3d1	e-mailova
adresám	adresa	k1gFnPc3	adresa
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
uzivatel	uzivatel	k1gMnSc1	uzivatel
<g/>
@	@	kIx~	@
<g/>
server	server	k1gInSc1	server
<g/>
.	.	kIx.	.
</s>
