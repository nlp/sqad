<s>
Rostlinné	rostlinný	k2eAgNnSc1d1	rostlinné
společenstvo	společenstvo	k1gNnSc1	společenstvo
(	(	kIx(	(
<g/>
též	též	k9	též
fytocenóza	fytocenóza	k1gFnSc1	fytocenóza
-	-	kIx~	-
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
fytos	fytos	k1gInSc1	fytos
-	-	kIx~	-
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
cenos	cenos	k1gInSc1	cenos
-	-	kIx~	-
společenstvo	společenstvo	k1gNnSc1	společenstvo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
abstraktní	abstraktní	k2eAgInSc1d1	abstraktní
pojem	pojem	k1gInSc1	pojem
pro	pro	k7c4	pro
soubor	soubor	k1gInSc4	soubor
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
druhů	druh	k1gInPc2	druh
společně	společně	k6eAd1	společně
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
ve	v	k7c6	v
stejných	stejný	k2eAgInPc6d1	stejný
typech	typ	k1gInPc6	typ
abiotického	abiotický	k2eAgNnSc2d1	abiotické
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
stanoviště	stanoviště	k1gNnSc2	stanoviště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
