<s>
Rostlinné	rostlinný	k2eAgNnSc1d1	rostlinné
společenstvo	společenstvo	k1gNnSc1	společenstvo
(	(	kIx(	(
<g/>
též	též	k9	též
fytocenóza	fytocenóza	k1gFnSc1	fytocenóza
-	-	kIx~	-
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
fytos	fytos	k1gInSc1	fytos
-	-	kIx~	-
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
cenos	cenos	k1gInSc1	cenos
-	-	kIx~	-
společenstvo	společenstvo	k1gNnSc1	společenstvo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
abstraktní	abstraktní	k2eAgInSc1d1	abstraktní
pojem	pojem	k1gInSc1	pojem
pro	pro	k7c4	pro
soubor	soubor	k1gInSc4	soubor
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
druhů	druh	k1gInPc2	druh
společně	společně	k6eAd1	společně
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
ve	v	k7c6	v
stejných	stejný	k2eAgInPc6d1	stejný
typech	typ	k1gInPc6	typ
abiotického	abiotický	k2eAgNnSc2d1	abiotické
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
stanoviště	stanoviště	k1gNnSc2	stanoviště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Souhrn	souhrn	k1gInSc1	souhrn
rostlinných	rostlinný	k2eAgNnPc2d1	rostlinné
společenstev	společenstvo	k1gNnPc2	společenstvo
na	na	k7c6	na
určitém	určitý	k2eAgNnSc6d1	určité
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vegetace	vegetace	k1gFnSc1	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Klasifikací	klasifikace	k1gFnSc7	klasifikace
a	a	k8xC	a
tříděním	třídění	k1gNnSc7	třídění
rostlinných	rostlinný	k2eAgNnPc2d1	rostlinné
společenstev	společenstvo	k1gNnPc2	společenstvo
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
fytocenologie	fytocenologie	k1gFnSc1	fytocenologie
<g/>
.	.	kIx.	.
</s>
