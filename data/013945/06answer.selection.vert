<s>
Historicky	historicky	k6eAd1
největší	veliký	k2eAgFnSc1d3
návštěva	návštěva	k1gFnSc1
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
byla	být	k5eAaImAgFnS
na	na	k7c6
turnaji	turnaj	k1gInSc6
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
návštěvnost	návštěvnost	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
hodnoty	hodnota	k1gFnPc4
741	#num#	k4
690	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
odpovídá	odpovídat	k5eAaImIp3nS
v	v	k7c6
průměru	průměr	k1gInSc6
11	#num#	k4
589	#num#	k4
diváků	divák	k1gMnPc2
na	na	k7c4
zápas	zápas	k1gInSc4
<g/>
.	.	kIx.
</s>