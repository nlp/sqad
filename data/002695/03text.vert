<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Orlík	Orlík	k1gInSc1	Orlík
je	být	k5eAaImIp3nS	být
součást	součást	k1gFnSc4	součást
Vltavské	vltavský	k2eAgFnSc2d1	Vltavská
kaskády	kaskáda	k1gFnSc2	kaskáda
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
a	a	k8xC	a
středních	střední	k2eAgFnPc6d1	střední
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1954	[number]	k4	1954
<g/>
-	-	kIx~	-
<g/>
1961	[number]	k4	1961
a	a	k8xC	a
přehradila	přehradit	k5eAaPmAgFnS	přehradit
tok	tok	k1gInSc4	tok
řeky	řeka	k1gFnSc2	řeka
Vltavy	Vltava	k1gFnSc2	Vltava
u	u	k7c2	u
Solenice	Solenice	k1gFnSc2	Solenice
na	na	k7c6	na
Příbramsku	Příbramsko	k1gNnSc6	Příbramsko
<g/>
.	.	kIx.	.
</s>
<s>
Nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
zámku	zámek	k1gInSc2	zámek
Orlík	orlík	k1gMnSc1	orlík
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
kdysi	kdysi	k6eAd1	kdysi
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
na	na	k7c6	na
skále	skála	k1gFnSc6	skála
nad	nad	k7c7	nad
hlubokým	hluboký	k2eAgNnSc7d1	hluboké
údolím	údolí	k1gNnSc7	údolí
Vltavy	Vltava	k1gFnSc2	Vltava
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jen	jen	k9	jen
pár	pár	k4xCyI	pár
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
přehradního	přehradní	k2eAgNnSc2d1	přehradní
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Přípravné	přípravný	k2eAgFnPc1d1	přípravná
stavební	stavební	k2eAgFnPc1d1	stavební
práce	práce	k1gFnPc1	práce
budoucího	budoucí	k2eAgNnSc2d1	budoucí
vodního	vodní	k2eAgNnSc2d1	vodní
díla	dílo	k1gNnSc2	dílo
Orlík	Orlík	k1gInSc1	Orlík
začaly	začít	k5eAaPmAgInP	začít
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
projekt	projekt	k1gInSc1	projekt
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
schválen	schválit	k5eAaPmNgInS	schválit
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
stavby	stavba	k1gFnSc2	stavba
se	se	k3xPyFc4	se
denně	denně	k6eAd1	denně
střídalo	střídat	k5eAaImAgNnS	střídat
přes	přes	k7c4	přes
1	[number]	k4	1
500	[number]	k4	500
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
samotná	samotný	k2eAgFnSc1d1	samotná
stavba	stavba	k1gFnSc1	stavba
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
2	[number]	k4	2
oběti	oběť	k1gFnSc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklému	vzniklý	k2eAgNnSc3d1	vzniklé
umělému	umělý	k2eAgNnSc3d1	umělé
jezeru	jezero	k1gNnSc3	jezero
muselo	muset	k5eAaImAgNnS	muset
ustoupit	ustoupit	k5eAaPmF	ustoupit
14	[number]	k4	14
mlýnů	mlýn	k1gInPc2	mlýn
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
pil	pila	k1gFnPc2	pila
a	a	k8xC	a
650	[number]	k4	650
obytných	obytný	k2eAgFnPc2d1	obytná
a	a	k8xC	a
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
přehrady	přehrada	k1gFnSc2	přehrada
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
železniční	železniční	k2eAgFnSc1d1	železniční
vlečka	vlečka	k1gFnSc1	vlečka
z	z	k7c2	z
Tochovic	Tochovice	k1gFnPc2	Tochovice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stavební	stavební	k2eAgFnSc6d1	stavební
stránce	stránka	k1gFnSc6	stránka
byla	být	k5eAaImAgFnS	být
přehrada	přehrada	k1gFnSc1	přehrada
dokončena	dokončit	k5eAaPmNgFnS	dokončit
a	a	k8xC	a
slavnostně	slavnostně	k6eAd1	slavnostně
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1961	[number]	k4	1961
,	,	kIx,	,
8	[number]	k4	8
měsíců	měsíc	k1gInPc2	měsíc
před	před	k7c7	před
stanoveným	stanovený	k2eAgInSc7d1	stanovený
termínem	termín	k1gInSc7	termín
dokončení	dokončení	k1gNnSc2	dokončení
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
,	,	kIx,	,
IV	IV	kA	IV
<g/>
.	.	kIx.	.
turbína	turbína	k1gFnSc1	turbína
vodní	vodní	k2eAgFnSc2d1	vodní
elektrárny	elektrárna	k1gFnSc2	elektrárna
byla	být	k5eAaImAgFnS	být
spuštěna	spuštěn	k2eAgFnSc1d1	spuštěna
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
výstavby	výstavba	k1gFnSc2	výstavba
vodního	vodní	k2eAgNnSc2d1	vodní
díla	dílo	k1gNnSc2	dílo
Orlík	Orlík	k1gInSc4	Orlík
byly	být	k5eAaImAgFnP	být
zatopeny	zatopen	k2eAgFnPc1d1	zatopena
následující	následující	k2eAgFnPc1d1	následující
vesnice	vesnice	k1gFnPc1	vesnice
a	a	k8xC	a
osady	osada	k1gFnPc1	osada
<g/>
:	:	kIx,	:
</s>
<s>
Těleso	těleso	k1gNnSc1	těleso
Orlické	orlický	k2eAgFnSc2d1	Orlická
přehrady	přehrada	k1gFnSc2	přehrada
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
řetězu	řetěz	k1gInSc6	řetěz
vltavských	vltavský	k2eAgFnPc2d1	Vltavská
přehrad	přehrada	k1gFnPc2	přehrada
největší	veliký	k2eAgFnSc4d3	veliký
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
a	a	k8xC	a
nejmohutnější	mohutný	k2eAgFnSc4d3	nejmohutnější
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
450	[number]	k4	450
m	m	kA	m
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
betonová	betonový	k2eAgFnSc1d1	betonová
tížní	tížný	k2eAgMnPc1d1	tížný
hráz	hráz	k1gFnSc1	hráz
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
koruně	koruna	k1gFnSc6	koruna
výšky	výška	k1gFnSc2	výška
91	[number]	k4	91
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
<s>
Vzdutí	vzdutí	k1gNnSc1	vzdutí
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
na	na	k7c6	na
Vltavě	Vltava	k1gFnSc6	Vltava
68	[number]	k4	68
km	km	kA	km
<g/>
,	,	kIx,	,
na	na	k7c6	na
Otavě	Otava	k1gFnSc6	Otava
23	[number]	k4	23
km	km	kA	km
a	a	k8xC	a
na	na	k7c6	na
Lužnici	Lužnice	k1gFnSc6	Lužnice
7	[number]	k4	7
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
hloubka	hloubka	k1gFnSc1	hloubka
je	být	k5eAaImIp3nS	být
74	[number]	k4	74
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
<s>
Objemem	objem	k1gInSc7	objem
zadržené	zadržený	k2eAgFnSc2d1	zadržená
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
nádrž	nádrž	k1gFnSc1	nádrž
Orlík	Orlík	k1gInSc1	Orlík
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Stálý	stálý	k2eAgInSc1d1	stálý
objem	objem	k1gInSc1	objem
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
,	,	kIx,	,
určený	určený	k2eAgInSc4d1	určený
kótou	kóta	k1gFnSc7	kóta
330,0	[number]	k4	330,0
m	m	kA	m
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
280	[number]	k4	280
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
<s>
Zásobní	zásobní	k2eAgInSc1d1	zásobní
objem	objem	k1gInSc1	objem
je	být	k5eAaImIp3nS	být
374,5	[number]	k4	374,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
m3	m3	k4	m3
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
využitelný	využitelný	k2eAgInSc1d1	využitelný
zásobní	zásobní	k2eAgInSc1d1	zásobní
objem	objem	k1gInSc1	objem
po	po	k7c4	po
kótu	kóta	k1gFnSc4	kóta
351,6	[number]	k4	351,6
m	m	kA	m
představuje	představovat	k5eAaImIp3nS	představovat
85	[number]	k4	85
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
<s>
Ochranný	ochranný	k2eAgInSc1d1	ochranný
objem	objem	k1gInSc1	objem
nad	nad	k7c7	nad
touto	tento	k3xDgFnSc7	tento
úrovní	úroveň	k1gFnSc7	úroveň
činí	činit	k5eAaImIp3nS	činit
62,1	[number]	k4	62,1
mil	míle	k1gFnPc2	míle
m3	m3	k4	m3
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
hladinou	hladina	k1gFnSc7	hladina
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
354,0	[number]	k4	354,0
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
maximální	maximální	k2eAgInSc1d1	maximální
teoretický	teoretický	k2eAgInSc1d1	teoretický
objem	objem	k1gInSc1	objem
vody	voda	k1gFnSc2	voda
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
716,6	[number]	k4	716,6
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Orlík	Orlík	k1gInSc1	Orlík
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
části	část	k1gFnSc6	část
řeky	řeka	k1gFnSc2	řeka
u	u	k7c2	u
paty	pata	k1gFnSc2	pata
betonové	betonový	k2eAgFnSc2d1	betonová
hráze	hráz	k1gFnSc2	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1960-61	[number]	k4	1960-61
a	a	k8xC	a
celkový	celkový	k2eAgInSc1d1	celkový
instalovaný	instalovaný	k2eAgInSc1d1	instalovaný
výkon	výkon	k1gInSc1	výkon
činí	činit	k5eAaImIp3nS	činit
364	[number]	k4	364
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
<s>
Plného	plný	k2eAgInSc2d1	plný
výkonu	výkon	k1gInSc2	výkon
je	být	k5eAaImIp3nS	být
elektrárna	elektrárna	k1gFnSc1	elektrárna
schopna	schopen	k2eAgFnSc1d1	schopna
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
za	za	k7c4	za
2	[number]	k4	2
minuty	minuta	k1gFnSc2	minuta
a	a	k8xC	a
plní	plnit	k5eAaImIp3nS	plnit
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
při	při	k7c6	při
stabilizaci	stabilizace	k1gFnSc6	stabilizace
elektrické	elektrický	k2eAgFnSc2d1	elektrická
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Popularitu	popularita	k1gFnSc4	popularita
Orlické	orlický	k2eAgFnSc2d1	Orlická
přehrady	přehrada	k1gFnSc2	přehrada
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
tzv.	tzv.	kA	tzv.
Orlické	orlický	k2eAgFnSc2d1	Orlická
vraždy	vražda	k1gFnSc2	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
pěti	pět	k4xCc2	pět
sériových	sériový	k2eAgMnPc2d1	sériový
vrahů	vrah	k1gMnPc2	vrah
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
peněžního	peněžní	k2eAgInSc2d1	peněžní
zisku	zisk	k1gInSc2	zisk
zavraždila	zavraždit	k5eAaPmAgFnS	zavraždit
pět	pět	k4xCc4	pět
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
vesměs	vesměs	k6eAd1	vesměs
podnikatelů	podnikatel	k1gMnPc2	podnikatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
těla	tělo	k1gNnSc2	tělo
obětí	oběť	k1gFnPc2	oběť
většinou	většinou	k6eAd1	většinou
ukrývali	ukrývat	k5eAaImAgMnP	ukrývat
do	do	k7c2	do
sudů	sud	k1gInPc2	sud
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgFnPc2	jenž
nalili	nalít	k5eAaBmAgMnP	nalít
louh	louh	k1gInSc4	louh
a	a	k8xC	a
shodili	shodit	k5eAaPmAgMnP	shodit
je	on	k3xPp3gNnPc4	on
do	do	k7c2	do
Orlické	orlický	k2eAgFnSc2d1	Orlická
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
tohoto	tento	k3xDgInSc2	tento
nápadu	nápad	k1gInSc2	nápad
byl	být	k5eAaImAgMnS	být
Vladimír	Vladimír	k1gMnSc1	Vladimír
Kuna	Kuna	k1gMnSc1	Kuna
<g/>
.	.	kIx.	.
</s>
<s>
Prozkoumávání	prozkoumávání	k1gNnSc1	prozkoumávání
dna	dno	k1gNnSc2	dno
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
krycí	krycí	k2eAgFnSc1d1	krycí
historka	historka	k1gFnSc1	historka
o	o	k7c6	o
čištění	čištění	k1gNnSc6	čištění
dna	dno	k1gNnSc2	dno
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
dva	dva	k4xCgInPc1	dva
sudy	sud	k1gInPc1	sud
s	s	k7c7	s
lidskými	lidský	k2eAgInPc7d1	lidský
ostatky	ostatek	k1gInPc7	ostatek
a	a	k8xC	a
tělo	tělo	k1gNnSc1	tělo
zabalené	zabalený	k2eAgNnSc1d1	zabalené
v	v	k7c6	v
pletivu	pletivo	k1gNnSc6	pletivo
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
policejními	policejní	k2eAgInPc7d1	policejní
potápěči	potápěč	k1gInPc7	potápěč
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
přehrady	přehrada	k1gFnSc2	přehrada
zabalené	zabalený	k2eAgFnSc2d1	zabalená
do	do	k7c2	do
role	role	k1gFnSc2	role
pletiva	pletivo	k1gNnSc2	pletivo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Jiří	Jiří	k1gMnSc1	Jiří
Svoboda	Svoboda	k1gMnSc1	Svoboda
na	na	k7c4	na
náměty	námět	k1gInPc4	námět
těchto	tento	k3xDgFnPc2	tento
událostí	událost	k1gFnPc2	událost
film	film	k1gInSc4	film
Sametoví	sametový	k2eAgMnPc1d1	sametový
vrazi	vrah	k1gMnPc1	vrah
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2006	[number]	k4	2006
objevili	objevit	k5eAaPmAgMnP	objevit
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
přehradě	přehrada	k1gFnSc6	přehrada
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Chrást	chrást	k1gInSc1	chrást
mrtvolu	mrtvola	k1gFnSc4	mrtvola
jednačtyřicetiletého	jednačtyřicetiletý	k2eAgMnSc4d1	jednačtyřicetiletý
zavražděného	zavražděný	k2eAgMnSc4d1	zavražděný
muže	muž	k1gMnSc4	muž
z	z	k7c2	z
Českobudějovicka	Českobudějovicko	k1gNnSc2	Českobudějovicko
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
měl	mít	k5eAaImAgMnS	mít
ruce	ruka	k1gFnPc4	ruka
i	i	k9	i
nohy	noha	k1gFnPc4	noha
svázané	svázaný	k2eAgFnPc4d1	svázaná
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
nalezli	naleznout	k5eAaPmAgMnP	naleznout
lidé	člověk	k1gMnPc1	člověk
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Vlastec	Vlastec	k1gMnSc1	Vlastec
vyplavené	vyplavený	k2eAgNnSc4d1	vyplavené
tělo	tělo	k1gNnSc4	tělo
53	[number]	k4	53
<g/>
letého	letý	k2eAgMnSc4d1	letý
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
předtím	předtím	k6eAd1	předtím
manželka	manželka	k1gFnSc1	manželka
namíchala	namíchat	k5eAaPmAgFnS	namíchat
do	do	k7c2	do
jídla	jídlo	k1gNnSc2	jídlo
uspávací	uspávací	k2eAgInPc4d1	uspávací
prostředky	prostředek	k1gInPc4	prostředek
a	a	k8xC	a
s	s	k7c7	s
neznámým	známý	k2eNgMnSc7d1	neznámý
komplicem	komplic	k1gMnSc7	komplic
ho	on	k3xPp3gInSc2	on
zabila	zabít	k5eAaPmAgFnS	zabít
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
objevili	objevit	k5eAaPmAgMnP	objevit
rybáři	rybář	k1gMnPc1	rybář
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
nádrže	nádrž	k1gFnSc2	nádrž
u	u	k7c2	u
Červené	Červená	k1gFnSc2	Červená
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
ostatky	ostatek	k1gInPc7	ostatek
ženy	žena	k1gFnPc1	žena
v	v	k7c6	v
plastové	plastový	k2eAgFnSc6d1	plastová
truhle	truhla	k1gFnSc6	truhla
<g/>
.	.	kIx.	.
</s>
<s>
Mrtvola	mrtvola	k1gFnSc1	mrtvola
byla	být	k5eAaImAgFnS	být
zabalená	zabalený	k2eAgFnSc1d1	zabalená
v	v	k7c6	v
igelitu	igelit	k1gInSc6	igelit
<g/>
,	,	kIx,	,
ruce	ruka	k1gFnSc6	ruka
měla	mít	k5eAaImAgNnP	mít
svázané	svázaný	k2eAgFnSc2d1	svázaná
drátem	drát	k1gInSc7	drát
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mělo	mít	k5eAaImAgNnS	mít
jít	jít	k5eAaImF	jít
o	o	k7c4	o
tělo	tělo	k1gNnSc4	tělo
Marcely	Marcela	k1gFnSc2	Marcela
Sekáčové	Sekáčové	k?	Sekáčové
<g/>
,	,	kIx,	,
za	za	k7c4	za
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
smrt	smrt	k1gFnSc4	smrt
byl	být	k5eAaImAgInS	být
dosud	dosud	k6eAd1	dosud
nepravomocně	pravomocně	k6eNd1	pravomocně
na	na	k7c4	na
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
její	její	k3xOp3gMnSc1	její
milenec	milenec	k1gMnSc1	milenec
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mikuš	Mikuš	k1gMnSc1	Mikuš
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
v	v	k7c6	v
době	doba	k1gFnSc6	doba
rozsudku	rozsudek	k1gInSc2	rozsudek
nebylo	být	k5eNaImAgNnS	být
tělo	tělo	k1gNnSc1	tělo
ještě	ještě	k6eAd1	ještě
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
<g/>
.	.	kIx.	.
</s>
<s>
Trest	trest	k1gInSc1	trest
pro	pro	k7c4	pro
pachatele	pachatel	k1gMnSc4	pachatel
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
odvolání	odvolání	k1gNnSc6	odvolání
zvýšen	zvýšen	k2eAgMnSc1d1	zvýšen
na	na	k7c4	na
14	[number]	k4	14
let	léto	k1gNnPc2	léto
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
mrtvolu	mrtvola	k1gFnSc4	mrtvola
našli	najít	k5eAaPmAgMnP	najít
policisté	policista	k1gMnPc1	policista
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
při	při	k7c6	při
pátrání	pátrání	k1gNnSc6	pátrání
po	po	k7c6	po
jiné	jiný	k2eAgFnSc6d1	jiná
pohřešované	pohřešovaný	k2eAgFnSc6d1	pohřešovaná
osobě	osoba	k1gFnSc6	osoba
nedaleko	nedaleko	k7c2	nedaleko
hradu	hrad	k1gInSc2	hrad
Zvíkov	Zvíkov	k1gInSc1	Zvíkov
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
muže	muž	k1gMnSc2	muž
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nejméně	málo	k6eAd3	málo
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
omotáno	omotat	k5eAaPmNgNnS	omotat
řetězem	řetěz	k1gInSc7	řetěz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
dna	dno	k1gNnSc2	dno
ukotven	ukotven	k2eAgInSc4d1	ukotven
podstavcem	podstavec	k1gInSc7	podstavec
přenosné	přenosný	k2eAgFnSc2d1	přenosná
dopravní	dopravní	k2eAgFnSc2d1	dopravní
značky	značka	k1gFnSc2	značka
<g/>
.	.	kIx.	.
</s>
<s>
Identifikace	identifikace	k1gFnSc1	identifikace
však	však	k9	však
byla	být	k5eAaImAgFnS	být
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
pohřešovaných	pohřešovaný	k2eAgMnPc2d1	pohřešovaný
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
přes	přes	k7c4	přes
1000	[number]	k4	1000
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
věku	věk	k1gInSc6	věk
a	a	k8xC	a
výšce	výška	k1gFnSc6	výška
nalezeného	nalezený	k2eAgNnSc2d1	nalezené
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
50	[number]	k4	50
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
přehrady	přehrada	k1gFnSc2	přehrada
ji	on	k3xPp3gFnSc4	on
iDnes	iDnesa	k1gFnPc2	iDnesa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
nazvala	nazvat	k5eAaPmAgFnS	nazvat
"	"	kIx"	"
<g/>
přehradou	přehrada	k1gFnSc7	přehrada
s	s	k7c7	s
krvavou	krvavý	k2eAgFnSc7d1	krvavá
pověstí	pověst	k1gFnSc7	pověst
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
"	"	kIx"	"
<g/>
s	s	k7c7	s
nejhorší	zlý	k2eAgFnSc7d3	nejhorší
pověstí	pověst	k1gFnSc7	pověst
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2014	[number]	k4	2014
ČTK	ČTK	kA	ČTK
publikovala	publikovat	k5eAaBmAgFnS	publikovat
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Stavební	stavební	k2eAgFnSc1d1	stavební
fakulta	fakulta	k1gFnSc1	fakulta
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
pro	pro	k7c4	pro
Povodí	povodí	k1gNnSc4	povodí
Vltavy	Vltava	k1gFnSc2	Vltava
studii	studie	k1gFnSc4	studie
o	o	k7c6	o
možnostech	možnost	k1gFnPc6	možnost
prevence	prevence	k1gFnSc2	prevence
povodňových	povodňový	k2eAgFnPc2d1	povodňová
škod	škoda	k1gFnPc2	škoda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Povodí	povodí	k1gNnSc1	povodí
Vltavy	Vltava	k1gFnSc2	Vltava
s.	s.	k?	s.
p.	p.	k?	p.
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
základě	základ	k1gInSc6	základ
přineslo	přinést	k5eAaPmAgNnS	přinést
možné	možný	k2eAgFnPc4d1	možná
varianty	varianta	k1gFnPc4	varianta
do	do	k7c2	do
veřejné	veřejný	k2eAgFnSc2d1	veřejná
diskuse	diskuse	k1gFnSc2	diskuse
a	a	k8xC	a
navrhlo	navrhnout	k5eAaPmAgNnS	navrhnout
nějaké	nějaký	k3yIgNnSc1	nějaký
řešení	řešení	k1gNnSc1	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
hotova	hotov	k2eAgFnSc1d1	hotova
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
krajních	krajní	k2eAgFnPc2d1	krajní
teoretických	teoretický	k2eAgFnPc2d1	teoretická
variant	varianta	k1gFnPc2	varianta
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
orlická	orlický	k2eAgFnSc1d1	Orlická
nádrž	nádrž	k1gFnSc1	nádrž
vypuštěna	vypustit	k5eAaPmNgFnS	vypustit
a	a	k8xC	a
sloužila	sloužit	k5eAaImAgFnS	sloužit
by	by	kYmCp3nS	by
jako	jako	k8xS	jako
suchý	suchý	k2eAgInSc1d1	suchý
poldr	poldr	k1gInSc1	poldr
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
vltavská	vltavský	k2eAgFnSc1d1	Vltavská
kaskáda	kaskáda	k1gFnSc1	kaskáda
může	moct	k5eAaImIp3nS	moct
trvale	trvale	k6eAd1	trvale
snížit	snížit	k5eAaPmF	snížit
hladinu	hladina	k1gFnSc4	hladina
některých	některý	k3yIgFnPc2	některý
přehrad	přehrada	k1gFnPc2	přehrada
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
ohroženy	ohrozit	k5eAaPmNgFnP	ohrozit
ostatní	ostatní	k2eAgFnPc1d1	ostatní
funkce	funkce	k1gFnPc1	funkce
kaskády	kaskáda	k1gFnSc2	kaskáda
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
udržování	udržování	k1gNnSc1	udržování
splavnosti	splavnost	k1gFnSc2	splavnost
řeky	řeka	k1gFnSc2	řeka
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
<g/>
,	,	kIx,	,
zásobování	zásobování	k1gNnSc6	zásobování
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
podniků	podnik	k1gInPc2	podnik
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc1	zemědělství
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
udržení	udržení	k1gNnSc4	udržení
kvality	kvalita	k1gFnSc2	kvalita
vody	voda	k1gFnSc2	voda
ve	v	k7c6	v
Vltavě	Vltava	k1gFnSc6	Vltava
a	a	k8xC	a
dodržení	dodržení	k1gNnSc6	dodržení
požadavků	požadavek	k1gInPc2	požadavek
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
největší	veliký	k2eAgFnSc3d3	veliký
rozloze	rozloha	k1gFnSc3	rozloha
byla	být	k5eAaImAgFnS	být
Orlická	orlický	k2eAgFnSc1d1	Orlická
přehrada	přehrada	k1gFnSc1	přehrada
vybrána	vybrat	k5eAaPmNgFnS	vybrat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
největších	veliký	k2eAgFnPc2d3	veliký
nádrží	nádrž	k1gFnPc2	nádrž
jako	jako	k8xS	jako
nejvhodnější	vhodný	k2eAgInSc4d3	nejvhodnější
pro	pro	k7c4	pro
vypuštění	vypuštění	k1gNnSc4	vypuštění
<g/>
.	.	kIx.	.
</s>
<s>
Starosta	Starosta	k1gMnSc1	Starosta
Orlíku	Orlík	k1gInSc2	Orlík
Jiří	Jiří	k1gMnSc1	Jiří
Štrajt	Štrajt	k1gMnSc1	Štrajt
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
obavu	obava	k1gFnSc4	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
dno	dno	k1gNnSc1	dno
bylo	být	k5eAaImAgNnS	být
jednou	jeden	k4xCgFnSc7	jeden
velkou	velký	k2eAgFnSc7d1	velká
žumpou	žumpa	k1gFnSc7	žumpa
a	a	k8xC	a
"	"	kIx"	"
<g/>
kdoví	kdoví	k0	kdoví
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
všechno	všechen	k3xTgNnSc1	všechen
našlo	najít	k5eAaPmAgNnS	najít
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rekultivace	rekultivace	k1gFnPc1	rekultivace
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
obrovské	obrovský	k2eAgInPc4d1	obrovský
náklady	náklad	k1gInPc4	náklad
a	a	k8xC	a
chataři	chatař	k1gMnPc1	chatař
<g/>
,	,	kIx,	,
rybáři	rybář	k1gMnPc1	rybář
<g/>
,	,	kIx,	,
turisté	turist	k1gMnPc1	turist
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jsou	být	k5eAaImIp3nP	být
živi	živ	k2eAgMnPc1d1	živ
místní	místní	k2eAgMnPc1d1	místní
obchodníci	obchodník	k1gMnPc1	obchodník
a	a	k8xC	a
ubytovatelé	ubytovatel	k1gMnPc1	ubytovatel
a	a	k8xC	a
potažmo	potažmo	k6eAd1	potažmo
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
krajinu	krajina	k1gFnSc4	krajina
opustili	opustit	k5eAaPmAgMnP	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Provozovatel	provozovatel	k1gMnSc1	provozovatel
lodní	lodní	k2eAgFnSc2d1	lodní
společnosti	společnost	k1gFnSc2	společnost
Quarter	quarter	k1gInSc1	quarter
Jindřich	Jindřich	k1gMnSc1	Jindřich
Hospaska	Hospaska	k1gFnSc1	Hospaska
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
vypuštěnou	vypuštěný	k2eAgFnSc4d1	vypuštěná
přehradu	přehrada	k1gFnSc4	přehrada
vlastně	vlastně	k9	vlastně
umí	umět	k5eAaImIp3nS	umět
představit	představit	k5eAaPmF	představit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
v	v	k7c6	v
tomhle	tenhle	k3xDgInSc6	tenhle
státě	stát	k1gInSc6	stát
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
všechno	všechen	k3xTgNnSc4	všechen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
silnice	silnice	k1gFnPc1	silnice
z	z	k7c2	z
Tábora	tábor	k1gMnSc2	tábor
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Písek	Písek	k1gInSc4	Písek
a	a	k8xC	a
Plzeň	Plzeň	k1gFnSc4	Plzeň
překonávají	překonávat	k5eAaImIp3nP	překonávat
jezero	jezero	k1gNnSc4	jezero
přes	přes	k7c4	přes
Podolský	podolský	k2eAgInSc4d1	podolský
a	a	k8xC	a
Žďákovský	Žďákovský	k2eAgInSc4d1	Žďákovský
most	most	k1gInSc4	most
<g/>
.	.	kIx.	.
</s>
<s>
Silnice	silnice	k1gFnPc1	silnice
z	z	k7c2	z
Milevska	Milevsko	k1gNnSc2	Milevsko
do	do	k7c2	do
Mirotic	Mirotice	k1gFnPc2	Mirotice
překonává	překonávat	k5eAaImIp3nS	překonávat
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
u	u	k7c2	u
Zvíkova	Zvíkov	k1gInSc2	Zvíkov
dvojicí	dvojice	k1gFnSc7	dvojice
mostů	most	k1gInPc2	most
přes	přes	k7c4	přes
Vltavu	Vltava	k1gFnSc4	Vltava
a	a	k8xC	a
Otavu	Otava	k1gFnSc4	Otava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
koruně	koruna	k1gFnSc6	koruna
hráze	hráz	k1gFnSc2	hráz
vede	vést	k5eAaImIp3nS	vést
silnice	silnice	k1gFnSc1	silnice
spojující	spojující	k2eAgFnSc2d1	spojující
Milešov	Milešov	k1gInSc4	Milešov
s	s	k7c7	s
oblastí	oblast	k1gFnSc7	oblast
Příbramska	Příbramsko	k1gNnSc2	Příbramsko
<g/>
.	.	kIx.	.
</s>
<s>
Údolí	údolí	k1gNnSc1	údolí
také	také	k9	také
překonává	překonávat	k5eAaImIp3nS	překonávat
železniční	železniční	k2eAgInSc4d1	železniční
most	most	k1gInSc4	most
trati	trať	k1gFnSc2	trať
Tábor	Tábor	k1gInSc1	Tábor
-	-	kIx~	-
Ražice	Ražice	k1gFnSc1	Ražice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přehradním	přehradní	k2eAgNnSc6d1	přehradní
jezeře	jezero	k1gNnSc6	jezero
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
provozována	provozovat	k5eAaImNgFnS	provozovat
lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
turisticky	turisticky	k6eAd1	turisticky
zajímavá	zajímavý	k2eAgNnPc1d1	zajímavé
místa	místo	k1gNnPc1	místo
umístěná	umístěný	k2eAgNnPc1d1	umístěné
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
stavby	stavba	k1gFnSc2	stavba
přehrady	přehrada	k1gFnSc2	přehrada
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
lodní	lodní	k2eAgNnSc1d1	lodní
zdvihadlo	zdvihadlo	k1gNnSc1	zdvihadlo
pro	pro	k7c4	pro
lodě	loď	k1gFnPc4	loď
o	o	k7c6	o
výtlaku	výtlak	k1gInSc6	výtlak
do	do	k7c2	do
300	[number]	k4	300
t	t	k?	t
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Povoděn	povoděn	k2eAgMnSc1d1	povoděn
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2002	[number]	k4	2002
znamenala	znamenat	k5eAaImAgFnS	znamenat
pro	pro	k7c4	pro
VD	VD	kA	VD
Orlík	orlík	k1gMnSc1	orlík
velkou	velký	k2eAgFnSc4d1	velká
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
relativně	relativně	k6eAd1	relativně
bez	bez	k7c2	bez
problému	problém	k1gInSc2	problém
vydržel	vydržet	k5eAaPmAgMnS	vydržet
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
elektrárny	elektrárna	k1gFnSc2	elektrárna
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyplavena	vyplavit	k5eAaPmNgFnS	vyplavit
<g/>
.	.	kIx.	.
</s>
<s>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
byla	být	k5eAaImAgFnS	být
vyplavena	vyplavit	k5eAaPmNgFnS	vyplavit
kvůli	kvůli	k7c3	kvůli
vlnám	vlna	k1gFnPc3	vlna
vznikajícím	vznikající	k2eAgFnPc3d1	vznikající
pří	přít	k5eAaImIp3nP	přít
spádu	spád	k1gInSc3	spád
z	z	k7c2	z
přelivů	přeliv	k1gInPc2	přeliv
a	a	k8xC	a
také	také	k9	také
kvůli	kvůli	k7c3	kvůli
překročení	překročení	k1gNnSc3	překročení
max	max	kA	max
<g/>
.	.	kIx.	.
hladiny	hladina	k1gFnSc2	hladina
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
o	o	k7c6	o
1,57	[number]	k4	1,57
metru	metro	k1gNnSc6	metro
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
přehrady	přehrada	k1gFnSc2	přehrada
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
něco	něco	k6eAd1	něco
kolem	kolem	k7c2	kolem
950	[number]	k4	950
milionů	milion	k4xCgInPc2	milion
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
<s>
Přítok	přítok	k1gInSc1	přítok
v	v	k7c6	v
maximech	maxim	k1gInPc6	maxim
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
něco	něco	k6eAd1	něco
kolem	kolem	k7c2	kolem
4400	[number]	k4	4400
m3	m3	k4	m3
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
a	a	k8xC	a
maximální	maximální	k2eAgInSc4d1	maximální
odtok	odtok	k1gInSc4	odtok
kolem	kolem	k7c2	kolem
3000	[number]	k4	3000
m3	m3	k4	m3
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
o	o	k7c4	o
17	[number]	k4	17
hodin	hodina	k1gFnPc2	hodina
zpomalil	zpomalit	k5eAaPmAgInS	zpomalit
povodňovou	povodňový	k2eAgFnSc4d1	povodňová
vlnu	vlna	k1gFnSc4	vlna
kterou	který	k3yRgFnSc4	který
zčásti	zčásti	k6eAd1	zčásti
zmírnil	zmírnit	k5eAaPmAgMnS	zmírnit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
nebýt	být	k5eNaImF	být
tohoto	tento	k3xDgInSc2	tento
vodní	vodní	k2eAgNnSc4d1	vodní
díla	dílo	k1gNnPc4	dílo
byl	být	k5eAaImAgInS	být
by	by	kYmCp3nS	by
průtok	průtok	k1gInSc1	průtok
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
o	o	k7c4	o
1500-2000	[number]	k4	1500-2000
m3	m3	k4	m3
vyšší	vysoký	k2eAgInPc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
zachytilo	zachytit	k5eAaPmAgNnS	zachytit
jako	jako	k9	jako
i	i	k9	i
další	další	k2eAgFnPc4d1	další
hráze	hráz	k1gFnPc4	hráz
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
trosek	troska	k1gFnPc2	troska
které	který	k3yQgFnPc1	který
by	by	kYmCp3nS	by
mohly	moct	k5eAaImAgFnP	moct
ucpat	ucpat	k5eAaPmF	ucpat
a	a	k8xC	a
zničit	zničit	k5eAaPmF	zničit
mosty	most	k1gInPc4	most
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Osobní	osobní	k2eAgFnSc1d1	osobní
vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
Vltavě	Vltava	k1gFnSc6	Vltava
<g/>
#	#	kIx~	#
<g/>
Orlická	orlický	k2eAgFnSc1d1	Orlická
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
<g/>
.	.	kIx.	.
</s>
