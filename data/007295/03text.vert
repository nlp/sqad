<s>
Za	za	k7c4	za
velkoměsto	velkoměsto	k1gNnSc4	velkoměsto
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
byl	být	k5eAaImAgInS	být
takto	takto	k6eAd1	takto
definován	definovat	k5eAaBmNgInS	definovat
na	na	k7c6	na
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
statistické	statistický	k2eAgFnSc6d1	statistická
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Velkoměsta	velkoměsto	k1gNnPc1	velkoměsto
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
kulturními	kulturní	k2eAgFnPc7d1	kulturní
<g/>
,	,	kIx,	,
ekonomickými	ekonomický	k2eAgInPc7d1	ekonomický
i	i	k8xC	i
politickými	politický	k2eAgInPc7d1	politický
centry	centr	k1gInPc7	centr
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgNnPc6d1	české
velkoměstech	velkoměsto	k1gNnPc6	velkoměsto
žije	žít	k5eAaImIp3nS	žít
jedna	jeden	k4xCgFnSc1	jeden
pětina	pětina	k1gFnSc1	pětina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
největším	veliký	k2eAgNnSc7d3	veliký
českým	český	k2eAgNnSc7d1	české
velkoměstem	velkoměsto	k1gNnSc7	velkoměsto
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
má	můj	k3xOp1gFnSc1	můj
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1,2	[number]	k4	1,2
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
následuje	následovat	k5eAaImIp3nS	následovat
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
380	[number]	k4	380
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ostrava	Ostrava	k1gFnSc1	Ostrava
(	(	kIx(	(
<g/>
300	[number]	k4	300
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
Plzeň	Plzeň	k1gFnSc1	Plzeň
(	(	kIx(	(
<g/>
170	[number]	k4	170
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
stotisícové	stotisícový	k2eAgFnSc2d1	stotisícová
hranice	hranice	k1gFnSc2	hranice
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
Liberec	Liberec	k1gInSc1	Liberec
a	a	k8xC	a
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
ji	on	k3xPp3gFnSc4	on
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
i	i	k8xC	i
Ústí	ústit	k5eAaImIp3nP	ústit
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
a	a	k8xC	a
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
přiblížily	přiblížit	k5eAaPmAgInP	přiblížit
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
a	a	k8xC	a
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
věkové	věkový	k2eAgFnSc2d1	věková
struktury	struktura	k1gFnSc2	struktura
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgNnSc7d3	nejstarší
velkoměstem	velkoměsto	k1gNnSc7	velkoměsto
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
nejmladším	mladý	k2eAgMnPc3d3	nejmladší
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgNnPc1d1	Evropské
velkoměsta	velkoměsto	k1gNnPc1	velkoměsto
vznikala	vznikat	k5eAaImAgNnP	vznikat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozrůstala	rozrůstat	k5eAaImAgFnS	rozrůstat
až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
století	století	k1gNnSc2	století
minulého	minulý	k2eAgNnSc2d1	Minulé
<g/>
,	,	kIx,	,
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
již	již	k9	již
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
nedochází	docházet	k5eNaImIp3nS	docházet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
městech	město	k1gNnPc6	město
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
existují	existovat	k5eAaImIp3nP	existovat
národnostní	národnostní	k2eAgFnPc1d1	národnostní
menšiny	menšina	k1gFnPc1	menšina
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
z	z	k7c2	z
rozvojových	rozvojový	k2eAgFnPc2d1	rozvojová
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
existuje	existovat	k5eAaImIp3nS	existovat
asi	asi	k9	asi
15	[number]	k4	15
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
přes	přes	k7c4	přes
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
samy	sám	k3xTgFnPc1	sám
nejsou	být	k5eNaImIp3nP	být
ani	ani	k8xC	ani
hlavními	hlavní	k2eAgNnPc7d1	hlavní
městy	město	k1gNnPc7	město
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yIgInPc3	který
náležejí	náležet	k5eAaImIp3nP	náležet
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Evropy	Evropa	k1gFnSc2	Evropa
tam	tam	k6eAd1	tam
populace	populace	k1gFnSc2	populace
přibývá	přibývat	k5eAaImIp3nS	přibývat
<g/>
,	,	kIx,	,
tato	tento	k3xDgNnPc1	tento
města	město	k1gNnPc1	město
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
neustále	neustále	k6eAd1	neustále
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
a	a	k8xC	a
propojují	propojovat	k5eAaImIp3nP	propojovat
se	se	k3xPyFc4	se
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k6eAd1	tak
souvislá	souvislý	k2eAgFnSc1d1	souvislá
městská	městský	k2eAgFnSc1d1	městská
krajina	krajina	k1gFnSc1	krajina
(	(	kIx(	(
<g/>
východní	východní	k2eAgNnSc1d1	východní
pobřeží	pobřeží	k1gNnSc1	pobřeží
USA	USA	kA	USA
<g/>
,	,	kIx,	,
okolí	okolí	k1gNnSc1	okolí
Los	los	k1gInSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obdobná	obdobný	k2eAgFnSc1d1	obdobná
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
města	město	k1gNnPc1	město
rozrůstají	rozrůstat	k5eAaImIp3nP	rozrůstat
ale	ale	k9	ale
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
přistěhovalectví	přistěhovalectví	k1gNnSc3	přistěhovalectví
venkovanů	venkovan	k1gMnPc2	venkovan
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
tak	tak	k6eAd1	tak
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgInPc1d2	veliký
celky	celek	k1gInPc1	celek
než	než	k8xS	než
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
dochází	docházet	k5eAaImIp3nS	docházet
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
k	k	k7c3	k
ohromnému	ohromný	k2eAgInSc3d1	ohromný
rozvoji	rozvoj	k1gInSc3	rozvoj
velkoměst	velkoměsto	k1gNnPc2	velkoměsto
<g/>
,	,	kIx,	,
města	město	k1gNnSc2	město
jako	jako	k8xC	jako
např.	např.	kA	např.
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
nebo	nebo	k8xC	nebo
Tokio	Tokio	k1gNnSc1	Tokio
mají	mít	k5eAaImIp3nP	mít
desítky	desítka	k1gFnPc4	desítka
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
nových	nový	k2eAgFnPc2d1	nová
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
staví	stavit	k5eAaBmIp3nS	stavit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
mrakodrapy	mrakodrap	k1gInPc4	mrakodrap
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
Taipei	Taipei	k1gNnSc1	Taipei
101	[number]	k4	101
<g/>
,	,	kIx,	,
Petronas	Petronas	k1gInSc1	Petronas
Towers	Towers	k1gInSc1	Towers
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přistěhovalectví	přistěhovalectví	k1gNnSc1	přistěhovalectví
z	z	k7c2	z
venkova	venkov	k1gInSc2	venkov
zde	zde	k6eAd1	zde
hraje	hrát	k5eAaImIp3nS	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
pak	pak	k6eAd1	pak
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
Indonésii	Indonésie	k1gFnSc6	Indonésie
a	a	k8xC	a
Malajsii	Malajsie	k1gFnSc6	Malajsie
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
Íránu	Írán	k1gInSc2	Írán
je	být	k5eAaImIp3nS	být
životní	životní	k2eAgFnSc1d1	životní
úroveň	úroveň	k1gFnSc1	úroveň
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
města	město	k1gNnSc2	město
trpí	trpět	k5eAaImIp3nS	trpět
velkým	velký	k2eAgNnSc7d1	velké
přelidněním	přelidnění	k1gNnSc7	přelidnění
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gNnPc2	on
vznikají	vznikat	k5eAaImIp3nP	vznikat
chudá	chudý	k2eAgNnPc1d1	chudé
předměstí	předměstí	k1gNnPc1	předměstí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
hlavně	hlavně	k9	hlavně
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
z	z	k7c2	z
venkova	venkov	k1gInSc2	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
města	město	k1gNnPc1	město
nedostávají	dostávat	k5eNaImIp3nP	dostávat
tolik	tolik	k4xDc4	tolik
investic	investice	k1gFnPc2	investice
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ta	ten	k3xDgFnSc1	ten
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
díky	díky	k7c3	díky
vysokému	vysoký	k2eAgInSc3d1	vysoký
přírůstku	přírůstek	k1gInSc3	přírůstek
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
vidinou	vidina	k1gFnSc7	vidina
práce	práce	k1gFnSc2	práce
u	u	k7c2	u
venkovanů	venkovan	k1gMnPc2	venkovan
se	se	k3xPyFc4	se
enormně	enormně	k6eAd1	enormně
rozrůstají	rozrůstat	k5eAaImIp3nP	rozrůstat
(	(	kIx(	(
<g/>
Bombaj	Bombaj	k1gFnSc1	Bombaj
<g/>
,	,	kIx,	,
Dillí	Dillí	k1gNnSc1	Dillí
<g/>
,	,	kIx,	,
Isfahán	Isfahán	k1gInSc1	Isfahán
<g/>
,	,	kIx,	,
Teherán	Teherán	k1gInSc1	Teherán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
špatné	špatný	k2eAgFnPc1d1	špatná
podmínky	podmínka	k1gFnPc1	podmínka
ještě	ještě	k6eAd1	ještě
zhoršují	zhoršovat	k5eAaImIp3nP	zhoršovat
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
ropy	ropa	k1gFnSc2	ropa
v	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
založení	založení	k1gNnSc2	založení
OPECu	OPECus	k1gInSc2	OPECus
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
zbohatnutí	zbohatnutí	k1gNnSc4	zbohatnutí
zemí	zem	k1gFnPc2	zem
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Dubaje	Dubaj	k1gInSc2	Dubaj
či	či	k8xC	či
Rijádu	Rijád	k1gInSc2	Rijád
v	v	k7c4	v
moderní	moderní	k2eAgNnPc4d1	moderní
milionová	milionový	k2eAgNnPc4d1	milionové
velkoměsta	velkoměsto	k1gNnPc4	velkoměsto
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
životní	životní	k2eAgFnSc7d1	životní
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc7d1	kulturní
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
blíže	blízce	k6eAd2	blízce
ke	k	k7c3	k
Středozemnímu	středozemní	k2eAgNnSc3d1	středozemní
moři	moře	k1gNnSc3	moře
existují	existovat	k5eAaImIp3nP	existovat
velkoměsta	velkoměsto	k1gNnPc1	velkoměsto
chudší	chudý	k2eAgNnPc1d2	chudší
(	(	kIx(	(
<g/>
Káhira	Káhira	k1gFnSc1	Káhira
<g/>
,	,	kIx,	,
Gíza	Gíza	k1gFnSc1	Gíza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mají	mít	k5eAaImIp3nP	mít
ohromné	ohromný	k2eAgInPc4d1	ohromný
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
znečištěním	znečištění	k1gNnSc7	znečištění
a	a	k8xC	a
hlukem	hluk	k1gInSc7	hluk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
4000	[number]	k4	4000
velkoměst	velkoměsto	k1gNnPc2	velkoměsto
<g/>
.	.	kIx.	.
</s>
