<s>
Durham	Durham	k1gInSc1	Durham
Wasps	Waspsa	k1gFnPc2	Waspsa
byl	být	k5eAaImAgInS	být
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
z	z	k7c2	z
Durhamu	Durham	k1gInSc2	Durham
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
hrál	hrát	k5eAaImAgInS	hrát
Britskou	britský	k2eAgFnSc4d1	britská
hokejovou	hokejový	k2eAgFnSc4d1	hokejová
ligu	liga	k1gFnSc4	liga
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Zanikl	zaniknout	k5eAaPmAgMnS	zaniknout
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
domovským	domovský	k2eAgInSc7d1	domovský
stadionem	stadion	k1gInSc7	stadion
byl	být	k5eAaImAgInS	být
Durham	Durham	k1gInSc1	Durham
Ice	Ice	k1gFnPc2	Ice
Rink	Rink	k1gInSc4	Rink
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
2860	[number]	k4	2860
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Heineken	Heineken	k2eAgInSc1d1	Heineken
Premier	Premier	k1gInSc1	Premier
League	Leagu	k1gFnSc2	Leagu
Champions	Championsa	k1gFnPc2	Championsa
-	-	kIx~	-
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
Autumn	Autumn	k1gInSc1	Autumn
Cup	cup	k1gInSc4	cup
-	-	kIx~	-
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
</s>
