<s>
Óda	óda	k1gFnSc1
na	na	k7c4
radost	radost	k1gFnSc4
</s>
<s>
Óda	óda	k1gFnSc1
na	na	k7c4
radost	radost	k1gFnSc4
Friedrich	Friedrich	k1gMnSc1
SchillerAutor	SchillerAutor	k1gMnSc1
</s>
<s>
Friedrich	Friedrich	k1gMnSc1
Schiller	Schiller	k1gMnSc1
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
[	[	kIx(
<g/>
Ode	ode	k7c2
<g/>
]	]	kIx)
an	an	k?
die	die	k?
Freude	Freud	k1gInSc5
Země	zem	k1gFnSc2
</s>
<s>
Německo	Německo	k1gNnSc1
Jazyk	jazyk	k1gInSc1
</s>
<s>
němčina	němčina	k1gFnSc1
Žánr	žánr	k1gInSc1
</s>
<s>
óda	óda	k1gFnSc1
Datum	datum	k1gNnSc1
vydání	vydání	k1gNnSc6
</s>
<s>
1786	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Óda	óda	k1gFnSc1
na	na	k7c4
radost	radost	k1gFnSc4
(	(	kIx(
<g/>
v	v	k7c6
němčině	němčina	k1gFnSc6
Ode	ode	k7c2
an	an	k?
die	die	k?
Freude	Freud	k1gMnSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
původně	původně	k6eAd1
báseň	báseň	k1gFnSc1
napsaná	napsaný	k2eAgFnSc1d1
Friedrichem	Friedrich	k1gMnSc7
Schillerem	Schiller	k1gMnSc7
v	v	k7c6
létě	léto	k1gNnSc6
1785	#num#	k4
oslavující	oslavující	k2eAgInSc1d1
přátelství	přátelství	k1gNnSc2
mezi	mezi	k7c7
lidmi	člověk	k1gMnPc7
(	(	kIx(
<g/>
vznikla	vzniknout	k5eAaPmAgFnS
jako	jako	k9
hold	hold	k1gInSc4
Schillerovu	Schillerův	k2eAgMnSc3d1
příteli	přítel	k1gMnSc3
a	a	k8xC
nakladateli	nakladatel	k1gMnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc1
zřejmě	zřejmě	k6eAd1
známější	známý	k2eAgFnSc1d2
forma	forma	k1gFnSc1
je	být	k5eAaImIp3nS
ale	ale	k8xC
upravená	upravený	k2eAgFnSc1d1
verze	verze	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
zhudebněna	zhudebnit	k5eAaPmNgFnS
Ludwigem	Ludwig	k1gMnSc7
van	vana	k1gFnPc2
Beethovenem	Beethoven	k1gMnSc7
roku	rok	k1gInSc2
1824	#num#	k4
jako	jako	k9
čtvrtá	čtvrtý	k4xOgFnSc1
a	a	k8xC
závěrečná	závěrečný	k2eAgFnSc1d1
věta	věta	k1gFnSc1
jeho	jeho	k3xOp3gFnPc4
Deváté	devátý	k4xOgFnPc4
symfonie	symfonie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Mnohem	mnohem	k6eAd1
méně	málo	k6eAd2
známé	známý	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
zhudebněné	zhudebněný	k2eAgFnPc1d1
verze	verze	k1gFnPc1
Franze	Franze	k1gFnSc2
Schuberta	Schubert	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1815	#num#	k4
nebo	nebo	k8xC
Petra	Petra	k1gFnSc1
Iljiče	Iljič	k1gInSc2
Čajkovského	Čajkovský	k2eAgInSc2d1
z	z	k7c2
roku	rok	k1gInSc2
1865	#num#	k4
(	(	kIx(
<g/>
a	a	k8xC
dalších	další	k2eAgFnPc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
hymna	hymna	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Hymna	hymna	k1gFnSc1
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1955	#num#	k4
navrhl	navrhnout	k5eAaPmAgMnS
Radě	rada	k1gFnSc6
Evropy	Evropa	k1gFnSc2
Richard	Richard	k1gMnSc1
Nikolaus	Nikolaus	k1gMnSc1
Coudenhove-Kalergi	Coudenhove-Kalergi	k1gMnSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
Óda	óda	k1gFnSc1
na	na	k7c4
radost	radost	k1gFnSc4
stala	stát	k5eAaPmAgFnS
evropskou	evropský	k2eAgFnSc7d1
hymnou	hymna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
byla	být	k5eAaImAgFnS
skladba	skladba	k1gFnSc1
oficiálně	oficiálně	k6eAd1
uznána	uznat	k5eAaPmNgFnS
za	za	k7c4
hymnu	hymna	k1gFnSc4
Rady	rada	k1gFnSc2
Evropy	Evropa	k1gFnSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
oficiální	oficiální	k2eAgFnSc7d1
hymnou	hymna	k1gFnSc7
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
hymna	hymna	k1gFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
pouze	pouze	k6eAd1
instrumentální	instrumentální	k2eAgFnSc1d1
verze	verze	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Instrumentální	instrumentální	k2eAgFnSc1d1
kompozice	kompozice	k1gFnSc1
nové	nový	k2eAgFnSc2d1
evropské	evropský	k2eAgFnSc2d1
hymny	hymna	k1gFnSc2
se	se	k3xPyFc4
ujal	ujmout	k5eAaPmAgInS
Herbert	Herbert	k1gInSc1
von	von	k1gInSc1
Karajan	Karajana	k1gFnPc2
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejvýznamnějších	významný	k2eAgMnPc2d3
dirigentů	dirigent	k1gMnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navrhl	navrhnout	k5eAaPmAgMnS
tři	tři	k4xCgFnPc4
instrumentální	instrumentální	k2eAgFnPc4d1
verze	verze	k1gFnPc4
-	-	kIx~
pro	pro	k7c4
klavír	klavír	k1gInSc4
<g/>
,	,	kIx,
pro	pro	k7c4
dechové	dechový	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
a	a	k8xC
pro	pro	k7c4
orchestr	orchestr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Hymna	hymna	k1gFnSc1
Společného	společný	k2eAgNnSc2d1
německého	německý	k2eAgNnSc2d1
družstva	družstvo	k1gNnSc2
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1956	#num#	k4
až	až	k9
1968	#num#	k4
startovalo	startovat	k5eAaBmAgNnS
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
Společné	společný	k2eAgNnSc1d1
německé	německý	k2eAgNnSc1d1
družstvo	družstvo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
jako	jako	k8xC,k8xS
hymnu	hymnus	k1gInSc6
používalo	používat	k5eAaImAgNnS
Ódu	óda	k1gFnSc4
na	na	k7c4
radost	radost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Text	text	k1gInSc1
</s>
<s>
Německý	německý	k2eAgInSc1d1
originál	originál	k1gInSc1
</s>
<s>
An	An	k?
die	die	k?
Freude	Freud	k1gInSc5
</s>
<s>
Freude	Freude	k6eAd1
<g/>
,	,	kIx,
schöner	schöner	k1gInSc4
Götterfunken	Götterfunkna	k1gFnPc2
<g/>
,	,	kIx,
</s>
<s>
Tochter	Tochter	k1gMnSc1
aus	aus	k?
Elysium	elysium	k1gNnSc4
<g/>
!	!	kIx.
</s>
<s>
Wir	Wir	k?
betreten	betreten	k2eAgInSc4d1
feuertrunken	feuertrunken	k1gInSc4
<g/>
,	,	kIx,
</s>
<s>
Himmlische	Himmlische	k1gNnSc1
<g/>
,	,	kIx,
Dein	Dein	k1gNnSc1
Heiligtum	Heiligtum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Deine	Deinout	k5eAaImIp3nS,k5eAaPmIp3nS
Zauber	Zauber	k1gInSc1
binden	bindna	k1gFnPc2
wieder	wiedra	k1gFnPc2
<g/>
,	,	kIx,
</s>
<s>
Was	Was	k?
die	die	k?
Mode	modus	k1gInSc5
streng	strenga	k1gFnPc2
geteilt	geteilt	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Alle	Alle	k6eAd1
Menschen	Menschen	k1gInSc1
werden	werdna	k1gFnPc2
Brüder	Brüdra	k1gFnPc2
<g/>
,	,	kIx,
</s>
<s>
Wo	Wo	k?
Dein	Dein	k1gMnSc1
sanfter	sanfter	k1gMnSc1
Flügel	Flügel	k1gMnSc1
weilt	weilt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Wem	Wem	k?
der	drát	k5eAaImRp2nS
große	große	k1gNnSc4
Wurf	Wurf	k1gInSc1
gelungen	gelungen	k1gInSc4
<g/>
,	,	kIx,
</s>
<s>
Eines	Eines	k1gMnSc1
Freundes	Freundes	k1gMnSc1
Freund	Freund	k1gMnSc1
zu	zu	k?
sein	sein	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Wer	Wer	k?
ein	ein	k?
holdes	holdes	k1gInSc1
Weib	Weib	k1gMnSc1
errungen	errungen	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Mische	Mische	k6eAd1
seinen	seinen	k2eAgInSc1d1
Jubel	Jubel	k1gInSc1
ein	ein	k?
<g/>
!	!	kIx.
</s>
<s>
Ja	Ja	k?
<g/>
,	,	kIx,
wer	wer	k?
auch	auch	k1gInSc1
nur	nur	k?
eine	einat	k5eAaPmIp3nS
Seele	Seele	k1gFnSc1
</s>
<s>
Sein	Seina	k1gFnPc2
nennt	nennta	k1gFnPc2
auf	auf	k?
dem	dem	k?
Erdenrund	Erdenrund	k1gInSc1
<g/>
!	!	kIx.
</s>
<s>
Und	Und	k?
wer	wer	k?
<g/>
'	'	kIx"
<g/>
s	s	k7c7
nie	nie	k?
gekonnt	gekonnt	k1gMnSc1
<g/>
,	,	kIx,
der	drát	k5eAaImRp2nS
stehle	stehle	k1gNnSc4
</s>
<s>
Weinend	Weinend	k1gInSc1
sich	sich	k1gInSc1
aus	aus	k?
diesem	dieso	k1gNnSc7
Bund	bunda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Freude	Freude	k6eAd1
trinken	trinken	k2eAgInSc1d1
alle	alle	k1gInSc1
Wesen	Wesen	k2eAgInSc1d1
</s>
<s>
An	An	k?
den	den	k1gInSc4
Brüsten	Brüsten	k2eAgInSc4d1
der	drát	k5eAaImRp2nS
Natur	Natur	k1gMnSc1
<g/>
;	;	kIx,
</s>
<s>
Alle	Alle	k6eAd1
Guten	Guten	k2eAgInSc1d1
<g/>
,	,	kIx,
alle	allat	k5eAaPmIp3nS
Bösen	Bösen	k1gInSc1
</s>
<s>
Folgen	Folgen	k1gInSc1
ihrer	ihrer	k1gMnSc1
Rosenspur	Rosenspur	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Küße	Küße	k1gFnSc1
gab	gab	k?
sie	sie	k?
uns	uns	k?
und	und	k?
Reben	Reben	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Einen	Einen	k2eAgMnSc1d1
Freund	Freund	k1gMnSc1
<g/>
,	,	kIx,
geprüft	geprüft	k1gMnSc1
im	im	k?
Tod	Tod	k1gMnSc1
<g/>
;	;	kIx,
</s>
<s>
Wollust	Wollust	k1gMnSc1
ward	ward	k1gMnSc1
dem	dem	k?
Wurm	Wurm	k1gMnSc1
gegeben	gegeben	k2eAgMnSc1d1
<g/>
,	,	kIx,
</s>
<s>
Und	Und	k?
der	drát	k5eAaImRp2nS
Cherub	cherub	k1gMnSc1
steht	steht	k1gMnSc1
vor	vor	k1gInSc4
Gott	Gott	k2eAgInSc4d1
<g/>
!	!	kIx.
</s>
<s>
Froh	Froh	k1gMnSc1
<g/>
,	,	kIx,
wie	wie	k?
seine	seinout	k5eAaPmIp3nS,k5eAaImIp3nS
Sonnen	Sonnen	k2eAgInSc1d1
Fliegen	Fliegen	k1gInSc1
</s>
<s>
Durch	durch	k1gInSc1
des	des	k1gNnSc1
Himmels	Himmels	k1gInSc1
prächt	prächt	k1gMnSc1
<g/>
'	'	kIx"
<g/>
gen	gen	k1gInSc1
Plan	plan	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Laufet	Laufet	k1gMnSc1
<g/>
,	,	kIx,
Brüder	Brüder	k1gMnSc1
<g/>
,	,	kIx,
eure	eurat	k5eAaPmIp3nS
Bahn	Bahn	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Freudig	Freudig	k1gMnSc1
<g/>
,	,	kIx,
wie	wie	k?
ein	ein	k?
Held	Held	k1gInSc1
zum	zum	k?
Siegen	Siegen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Seid	Seid	k6eAd1
umschlungen	umschlungen	k1gInSc1
<g/>
,	,	kIx,
Millionen	Millionen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Diesen	Diesen	k2eAgMnSc1d1
Kuß	Kuß	k1gMnSc1
der	drát	k5eAaImRp2nS
ganzen	ganzna	k1gFnPc2
Welt	Welt	k2eAgMnSc1d1
<g/>
!	!	kIx.
</s>
<s>
Brüder	Brüder	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Über	Über	k1gInSc1
<g/>
'	'	kIx"
<g/>
m	m	kA
Sternenzelt	Sternenzelt	k1gMnSc1
</s>
<s>
Muß	Muß	k?
ein	ein	k?
lieber	lieber	k1gInSc1
Vater	vatra	k1gFnPc2
wohnen	wohnna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ihr	Ihr	k?
stürzt	stürzt	k1gMnSc1
nieder	nieder	k1gMnSc1
<g/>
,	,	kIx,
Millionen	Millionen	k2eAgMnSc1d1
<g/>
?	?	kIx.
</s>
<s>
Ahnest	Ahnest	k1gFnSc1
du	du	k?
den	den	k1gInSc4
Schöpfer	Schöpfra	k1gFnPc2
<g/>
,	,	kIx,
Welt	Welta	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s>
Such	sucho	k1gNnPc2
<g/>
'	'	kIx"
ihn	ihn	k?
über	über	k1gMnSc1
<g/>
'	'	kIx"
<g/>
m	m	kA
Sternenzelt	Sternenzelt	k1gInSc1
<g/>
!	!	kIx.
</s>
<s>
über	über	k1gInSc1
Sternen	Sternen	k1gInSc1
muß	muß	k?
er	er	k?
wohnen	wohnen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
Friedrich	Friedrich	k1gMnSc1
Schiller	Schiller	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
</s>
<s>
Óda	óda	k1gFnSc1
na	na	k7c4
radost	radost	k1gFnSc4
</s>
<s>
Radosti	radost	k1gFnPc1
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
jiskro	jiskra	k1gFnSc5
boží	boží	k2eAgFnSc3d1
<g/>
,	,	kIx,
</s>
<s>
dcero	dcera	k1gFnSc5
<g/>
,	,	kIx,
již	již	k6eAd1
nám	my	k3xPp1nPc3
ráj	ráj	k1gInSc1
dal	dát	k5eAaPmAgInS
sám	sám	k3xTgInSc4
<g/>
!	!	kIx.
</s>
<s>
Srdce	srdce	k1gNnSc1
vzňaté	vzňatý	k2eAgNnSc1d1
žárem	žár	k1gInSc7
touží	toužit	k5eAaImIp3nP
<g/>
,	,	kIx,
</s>
<s>
nebeský	nebeský	k2eAgMnSc1d1
tvůj	tvůj	k1gMnSc1
krásy	krása	k1gFnSc2
chrám	chrám	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Kouzlo	kouzlo	k1gNnSc1
tvé	tvůj	k3xOp2gFnSc2
teď	teď	k6eAd1
opět	opět	k6eAd1
váže	vázat	k5eAaImIp3nS
<g/>
,	,	kIx,
</s>
<s>
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
kdy	kdy	k6eAd1
čas	čas	k1gInSc1
tak	tak	k6eAd1
dělil	dělit	k5eAaImAgInS
rád	rád	k2eAgMnSc1d1
<g/>
,	,	kIx,
</s>
<s>
zástup	zástup	k1gInSc1
lidstva	lidstvo	k1gNnSc2
sbratřen	sbratřen	k2eAgInSc1d1
blíže	blízce	k6eAd2
</s>
<s>
cítí	cítit	k5eAaImIp3nP
van	van	k1gInSc4
tvých	tvůj	k3xOp2gNnPc2
křídel	křídlo	k1gNnPc2
vát	vát	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Komu	kdo	k3yRnSc3,k3yQnSc3,k3yInSc3
štěstí	štěstit	k5eAaImIp3nS
v	v	k7c6
žití	žití	k1gNnSc6
přálo	přát	k5eAaImAgNnS
<g/>
,	,	kIx,
</s>
<s>
v	v	k7c6
příteli	přítel	k1gMnSc6
že	že	k8xS
štít	štít	k1gInSc4
svůj	svůj	k3xOyFgInSc4
máš	mít	k5eAaImIp2nS
<g/>
,	,	kIx,
</s>
<s>
komu	kdo	k3yRnSc3,k3yQnSc3,k3yInSc3
věrnou	věrný	k2eAgFnSc4d1
ženu	žena	k1gFnSc4
dalo	dát	k5eAaPmAgNnS
<g/>
,	,	kIx,
</s>
<s>
mísit	mísit	k5eAaImF
pojď	jít	k5eAaImRp2nS
se	se	k3xPyFc4
v	v	k7c4
jásot	jásot	k1gInSc4
náš	náš	k3xOp1gInSc1
<g/>
!	!	kIx.
</s>
<s>
Sám	sám	k3xTgMnSc1
byť	byť	k8xS
jenom	jenom	k6eAd1
jednu	jeden	k4xCgFnSc4
duši	duše	k1gFnSc4
</s>
<s>
svojí	svojit	k5eAaImIp3nS
na	na	k7c6
tom	ten	k3xDgInSc6
světě	svět	k1gInSc6
zval	zvát	k5eAaImAgMnS
<g/>
!	!	kIx.
</s>
<s>
V	v	k7c6
díle	dílo	k1gNnSc6
tom	ten	k3xDgNnSc6
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
nezdar	nezdar	k1gInSc4
tuší	tušit	k5eAaImIp3nS
<g/>
,	,	kIx,
</s>
<s>
od	od	k7c2
nás	my	k3xPp1nPc2
ber	brát	k5eAaImRp2nS
se	se	k3xPyFc4
s	s	k7c7
pláčem	pláč	k1gInSc7
dál	daleko	k6eAd2
<g/>
!	!	kIx.
</s>
<s>
Radost	radost	k1gFnSc1
každá	každý	k3xTgFnSc1
bytost	bytost	k1gFnSc1
sáti	sát	k5eAaImF
<g/>
,	,	kIx,
</s>
<s>
přírodo	příroda	k1gFnSc5
<g/>
,	,	kIx,
chce	chtít	k5eAaImIp3nS
z	z	k7c2
ňader	ňadro	k1gNnPc2
tvých	tvůj	k3xOp2gMnPc2
<g/>
;	;	kIx,
</s>
<s>
zlý	zlý	k2eAgMnSc1d1
a	a	k8xC
dobrý	dobrý	k2eAgMnSc1d1
chce	chtít	k5eAaImIp3nS
se	se	k3xPyFc4
bráti	brát	k5eAaImF
</s>
<s>
v	v	k7c6
jejích	její	k3xOp3gFnPc6
stopách	stopa	k1gFnPc6
růžových	růžový	k2eAgFnPc6d1
<g/>
.	.	kIx.
</s>
<s>
Révy	réva	k1gFnPc1
žár	žár	k1gInSc1
nám	my	k3xPp1nPc3
<g/>
,	,	kIx,
pocel	pocel	k1gInSc4
smavý	smavý	k2eAgInSc4d1
<g/>
,	,	kIx,
</s>
<s>
na	na	k7c4
smrt	smrt	k1gFnSc4
věrný	věrný	k2eAgMnSc1d1
přítel	přítel	k1gMnSc1
dán	dát	k5eAaPmNgMnS
<g/>
;	;	kIx,
</s>
<s>
vášně	vášeň	k1gFnPc4
plam	plama	k1gFnPc2
dán	dát	k5eAaPmNgInS
červu	červ	k1gMnSc3
žhavý	žhavý	k2eAgMnSc1d1
<g/>
,	,	kIx,
</s>
<s>
světlý	světlý	k2eAgMnSc1d1
cherub	cherub	k1gMnSc1
dlí	dlít	k5eAaImIp3nS
<g/>
,	,	kIx,
kde	kde	k6eAd1
Pán	pán	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Dál	daleko	k6eAd2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
jeho	jeho	k3xOp3gNnSc3
slunci	slunce	k1gNnSc3
roje	roj	k1gInSc2
</s>
<s>
nebes	nebesa	k1gNnPc2
modrou	modrý	k2eAgFnSc7d1
nádherou	nádhera	k1gFnSc7
<g/>
,	,	kIx,
</s>
<s>
spějte	spět	k5eAaImRp2nP
<g/>
,	,	kIx,
bratří	bratřit	k5eAaImIp3nP
<g/>
,	,	kIx,
drahou	draha	k1gFnSc7
svou	svůj	k3xOyFgFnSc7
</s>
<s>
směle	směle	k6eAd1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
by	by	kYmCp3nS
rek	rek	k1gMnSc1
šel	jít	k5eAaImAgMnS
v	v	k7c4
boje	boj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c4
náruč	náruč	k1gFnSc4
spějte	spět	k5eAaImRp2nP
<g/>
,	,	kIx,
milióny	milión	k4xCgInPc7
<g/>
!	!	kIx.
</s>
<s>
Zlíbat	zlíbat	k5eAaPmF
svět	svět	k1gInSc4
kéž	kéž	k9
dáno	dán	k2eAgNnSc1d1
nám	my	k3xPp1nPc3
<g/>
!	!	kIx.
</s>
<s>
Bratři	bratr	k1gMnPc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Hvězd	hvězda	k1gFnPc2
kde	kde	k6eAd1
žárný	žárný	k2eAgInSc1d1
stan	stan	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
dobrý	dobrý	k2eAgMnSc1d1
Otec	otec	k1gMnSc1
zří	zřít	k5eAaImIp3nS
z	z	k7c2
mlh	mlha	k1gFnPc2
clony	clona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dlíte	dlít	k5eAaImIp2nP
v	v	k7c6
prachu	prach	k1gInSc6
<g/>
,	,	kIx,
milióny	milión	k4xCgInPc7
<g/>
?	?	kIx.
</s>
<s>
V	v	k7c6
bázni	bázeň	k1gFnSc6
Tvůrce	tvůrce	k1gMnSc1
tuší	tušit	k5eAaImIp3nS
svět	svět	k1gInSc4
<g/>
?	?	kIx.
</s>
<s>
Pátrej	pátrat	k5eAaImRp2nS
<g/>
,	,	kIx,
hvězd	hvězda	k1gFnPc2
kde	kde	k6eAd1
bezpočet	bezpočet	k1gInSc4
<g/>
!	!	kIx.
</s>
<s>
V	v	k7c6
záři	zář	k1gFnSc6
ční	čnět	k5eAaImIp3nP
tam	tam	k6eAd1
Božské	božský	k2eAgInPc1d1
trůny	trůn	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
Pavel	Pavel	k1gMnSc1
Eisner	Eisner	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Hymna	hymna	k1gFnSc1
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
</s>
<s>
Symfonie	symfonie	k1gFnSc1
č.	č.	k?
9	#num#	k4
(	(	kIx(
<g/>
Beethoven	Beethoven	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Schillerův	Schillerův	k2eAgInSc1d1
dům	dům	k1gInSc1
(	(	kIx(
<g/>
Lipsko	Lipsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Óda	óda	k1gFnSc1
na	na	k7c4
radost	radost	k1gFnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Dílo	dílo	k1gNnSc1
Sebrané	sebraný	k2eAgFnSc2d1
básně	báseň	k1gFnSc2
<g/>
/	/	kIx~
<g/>
Píseň	píseň	k1gFnSc1
na	na	k7c4
radost	radost	k1gFnSc4
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Dílo	dílo	k1gNnSc1
An	An	k1gFnSc2
die	die	k?
Freude	Freud	k1gInSc5
(	(	kIx(
<g/>
Schiller	Schiller	k1gMnSc1
<g/>
)	)	kIx)
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Německý	německý	k2eAgInSc1d1
text	text	k1gInSc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
v	v	k7c6
Beethovenově	Beethovenův	k2eAgNnSc6d1
zhudebnění	zhudebnění	k1gNnSc6
na	na	k7c6
Wikisource	Wikisourka	k1gFnSc6
</s>
<s>
Původní	původní	k2eAgInSc1d1
Schillerův	Schillerův	k2eAgInSc1d1
text	text	k1gInSc1
Wikisource	Wikisourka	k1gFnSc3
</s>
<s>
Óda	óda	k1gFnSc1
na	na	k7c4
radost	radost	k1gFnSc4
(	(	kIx(
<g/>
info	info	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
instrumentální	instrumentální	k2eAgFnSc1d1
verze	verze	k1gFnSc1
skladby	skladba	k1gFnSc2
Ludwiga	Ludwig	k1gMnSc2
van	vana	k1gFnPc2
Beethovena	Beethoven	k1gMnSc2
<g/>
,	,	kIx,
část	část	k1gFnSc4
jeho	jeho	k3xOp3gFnSc2
Deváté	devátý	k4xOgFnSc2
symfonie	symfonie	k1gFnSc2
</s>
<s>
Máte	mít	k5eAaImIp2nP
problémy	problém	k1gInPc4
s	s	k7c7
přehráváním	přehrávání	k1gNnSc7
<g/>
?	?	kIx.
</s>
<s desamb="1">
Vizte	vidět	k5eAaImRp2nP
nápovědu	nápověda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4590404-2	4590404-2	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
86113990	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
187059293	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
86113990	#num#	k4
</s>
