<s>
Tristan	Tristan	k1gInSc1	Tristan
Tzara	Tzar	k1gMnSc2	Tzar
[	[	kIx(	[
<g/>
Cara	car	k1gMnSc2	car
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Samuel	Samuel	k1gMnSc1	Samuel
Rosenstock	Rosenstock	k1gMnSc1	Rosenstock
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
Moineș	Moineș	k1gFnSc6	Moineș
<g/>
,	,	kIx,	,
Bacău	Bacăum	k1gNnSc6	Bacăum
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
rumunsko-židovského	rumunsko-židovský	k2eAgInSc2d1	rumunsko-židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
