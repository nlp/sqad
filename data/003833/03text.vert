<s>
Jelena	Jelena	k1gFnSc1	Jelena
Vesninová	Vesninový	k2eAgFnSc1d1	Vesninová
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
Е	Е	k?	Е
С	С	k?	С
В	В	k?	В
<g/>
,	,	kIx,	,
Jelena	Jelena	k1gFnSc1	Jelena
Sergejevna	Sergejevna	k1gFnSc1	Sergejevna
Vesnina	Vesnin	k2eAgFnSc1d1	Vesnina
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1986	[number]	k4	1986
Lvov	Lvov	k1gInSc1	Lvov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ruská	ruský	k2eAgFnSc1d1	ruská
profesionální	profesionální	k2eAgFnSc1d1	profesionální
tenistka	tenistka	k1gFnSc1	tenistka
a	a	k8xC	a
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vítězka	vítězka	k1gFnSc1	vítězka
z	z	k7c2	z
ženské	ženský	k2eAgFnSc2d1	ženská
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
Riodejaneirské	riodejaneirský	k2eAgFnSc2d1	riodejaneirský
olympiády	olympiáda	k1gFnSc2	olympiáda
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
odehrála	odehrát	k5eAaPmAgFnS	odehrát
s	s	k7c7	s
Jekatěrinou	Jekatěrin	k2eAgFnSc7d1	Jekatěrina
Makarovovou	Makarovová	k1gFnSc7	Makarovová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
dosavadní	dosavadní	k2eAgFnSc6d1	dosavadní
kariéře	kariéra	k1gFnSc6	kariéra
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
Tour	Tour	k1gMnSc1	Tour
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
tři	tři	k4xCgInPc4	tři
singlové	singlový	k2eAgInPc4d1	singlový
a	a	k8xC	a
osmnáct	osmnáct	k4xCc4	osmnáct
deblových	deblový	k2eAgInPc2d1	deblový
turnajů	turnaj	k1gInPc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
okruhu	okruh	k1gInSc2	okruh
ITF	ITF	kA	ITF
získala	získat	k5eAaPmAgFnS	získat
dva	dva	k4xCgInPc4	dva
tituly	titul	k1gInPc4	titul
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
šest	šest	k4xCc4	šest
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
WTA	WTA	kA	WTA
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
nejvýše	vysoce	k6eAd3	vysoce
klasifikována	klasifikovat	k5eAaImNgFnS	klasifikovat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2017	[number]	k4	2017
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
pak	pak	k6eAd1	pak
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2015	[number]	k4	2015
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Trénuje	trénovat	k5eAaImIp3nS	trénovat
ji	on	k3xPp3gFnSc4	on
bývalý	bývalý	k2eAgMnSc1d1	bývalý
sovětský	sovětský	k2eAgMnSc1d1	sovětský
tenista	tenista	k1gMnSc1	tenista
Andrej	Andrej	k1gMnSc1	Andrej
Česnokov	Česnokov	k1gInSc1	Česnokov
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
Sergej	Sergej	k1gMnSc1	Sergej
Vesnin	Vesnin	k2eAgMnSc1d1	Vesnin
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
koučem	kouč	k1gMnSc7	kouč
byl	být	k5eAaImAgMnS	být
Jurij	Jurij	k1gMnSc1	Jurij
Judkin	Judkin	k1gMnSc1	Judkin
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
grandslamu	grandslam	k1gInSc6	grandslam
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
se	s	k7c7	s
stabilní	stabilní	k2eAgFnSc7d1	stabilní
spoluhráčkou	spoluhráčka	k1gFnSc7	spoluhráčka
Jekatěrinou	Jekatěrin	k2eAgFnSc7d1	Jekatěrina
Makarovovou	Makarovová	k1gFnSc7	Makarovová
ženskou	ženský	k2eAgFnSc4d1	ženská
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
na	na	k7c4	na
French	French	k1gInSc4	French
Open	Open	k1gInSc1	Open
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
US	US	kA	US
Open	Open	k1gInSc4	Open
2014	[number]	k4	2014
a	a	k8xC	a
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
finále	finále	k1gNnSc6	finále
zdolaly	zdolat	k5eAaPmAgFnP	zdolat
italskou	italský	k2eAgFnSc4d1	italská
dvojici	dvojice	k1gFnSc4	dvojice
Sara	Sar	k1gInSc2	Sar
Erraniová	Erraniový	k2eAgFnSc1d1	Erraniová
a	a	k8xC	a
Roberta	Roberta	k1gFnSc1	Roberta
Vinciová	Vinciový	k2eAgFnSc1d1	Vinciová
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
švýcarsko-italský	švýcarskotalský	k2eAgInSc1d1	švýcarsko-italský
pár	pár	k4xCyI	pár
Martina	Martin	k1gInSc2	Martin
Hingisová	Hingisový	k2eAgFnSc1d1	Hingisová
a	a	k8xC	a
Flavia	Flavia	k1gFnSc1	Flavia
Pennettaová	Pennettaová	k1gFnSc1	Pennettaová
a	a	k8xC	a
ve	v	k7c6	v
třetím	třetí	k4xOgMnSc6	třetí
pak	pak	k6eAd1	pak
dvojici	dvojice	k1gFnSc4	dvojice
Čan	Čan	k1gFnSc2	Čan
Chao-čching	Chao-čching	k1gInSc1	Chao-čching
a	a	k8xC	a
Monica	Monica	k1gFnSc1	Monica
Niculescuová	Niculescuová	k1gFnSc1	Niculescuová
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
brazilským	brazilský	k2eAgMnSc7d1	brazilský
tenistou	tenista	k1gMnSc7	tenista
Brunem	Bruno	k1gMnSc7	Bruno
Soaresem	Soares	k1gMnSc7	Soares
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc1	Open
2016	[number]	k4	2016
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
smíšené	smíšený	k2eAgFnSc2d1	smíšená
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
devětkrát	devětkrát	k6eAd1	devětkrát
odešla	odejít	k5eAaPmAgFnS	odejít
z	z	k7c2	z
grandslamového	grandslamový	k2eAgNnSc2d1	grandslamové
finále	finále	k1gNnSc2	finále
poražena	poražen	k2eAgFnSc1d1	poražena
–	–	k?	–
z	z	k7c2	z
ženského	ženský	k2eAgNnSc2d1	ženské
debla	deblo	k1gNnSc2	deblo
na	na	k7c4	na
French	French	k1gInSc4	French
Open	Open	k1gInSc1	Open
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
i	i	k8xC	i
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
z	z	k7c2	z
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
a	a	k8xC	a
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gInSc4	Open
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Třikrát	třikrát	k6eAd1	třikrát
pak	pak	k6eAd1	pak
neuspěla	uspět	k5eNaPmAgNnP	uspět
s	s	k7c7	s
indickými	indický	k2eAgMnPc7d1	indický
partnery	partner	k1gMnPc7	partner
Bhupathim	Bhupathimo	k1gNnPc2	Bhupathimo
a	a	k8xC	a
Paesem	Paes	k1gInSc7	Paes
ve	v	k7c6	v
smíšené	smíšený	k2eAgFnSc6d1	smíšená
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Finálové	finálový	k2eAgFnPc1d1	finálová
porážky	porážka	k1gFnPc1	porážka
přišly	přijít	k5eAaPmAgFnP	přijít
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
2011	[number]	k4	2011
a	a	k8xC	a
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc4	Open
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
získala	získat	k5eAaPmAgFnS	získat
s	s	k7c7	s
Makarovovou	Makarovová	k1gFnSc7	Makarovová
titul	titul	k1gInSc1	titul
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
Turnaje	turnaj	k1gInSc2	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zdolaly	zdolat	k5eAaPmAgFnP	zdolat
pár	pár	k4xCyI	pár
Bethanie	Bethanie	k1gFnPc1	Bethanie
Matteková-Sandsová	Matteková-Sandsová	k1gFnSc1	Matteková-Sandsová
a	a	k8xC	a
Lucie	Lucie	k1gFnSc1	Lucie
Šafářová	Šafářová	k1gFnSc1	Šafářová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
fedcupovém	fedcupový	k2eAgInSc6d1	fedcupový
týmu	tým	k1gInSc6	tým
debutovala	debutovat	k5eAaBmAgFnS	debutovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
umagskou	umagská	k1gFnSc4	umagská
baráží	baráž	k1gFnPc2	baráž
Světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
proti	proti	k7c3	proti
Chorvatsku	Chorvatsko	k1gNnSc3	Chorvatsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
prohrála	prohrát	k5eAaPmAgFnS	prohrát
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Čakvetadzeovou	Čakvetadzeův	k2eAgFnSc7d1	Čakvetadzeův
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Rusky	Ruska	k1gFnPc1	Ruska
přesto	přesto	k6eAd1	přesto
triumfovaly	triumfovat	k5eAaBmAgFnP	triumfovat
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
a	a	k8xC	a
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
družstva	družstvo	k1gNnSc2	družstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
celou	celá	k1gFnSc4	celá
soutěž	soutěž	k1gFnSc4	soutěž
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
2007	[number]	k4	2007
proti	proti	k7c3	proti
Italkám	Italka	k1gFnPc3	Italka
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
výsledku	výsledek	k1gInSc3	výsledek
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
bodem	bod	k1gInSc7	bod
z	z	k7c2	z
dvouhry	dvouhra	k1gFnSc2	dvouhra
<g/>
,	,	kIx,	,
když	když	k8xS	když
porazila	porazit	k5eAaPmAgFnS	porazit
Maru	Maru	k1gFnSc4	Maru
Santangelovou	Santangelový	k2eAgFnSc4d1	Santangelový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
proti	proti	k7c3	proti
Španělsku	Španělsko	k1gNnSc3	Španělsko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
Rusky	Ruska	k1gFnPc1	Ruska
zvládly	zvládnout	k5eAaPmAgFnP	zvládnout
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
s	s	k7c7	s
Jekatěrinou	Jekatěrin	k2eAgFnSc7d1	Jekatěrina
Makarovovou	Makarovová	k1gFnSc7	Makarovová
bod	bod	k1gInSc1	bod
ze	z	k7c2	z
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
ke	k	k7c3	k
patnácti	patnáct	k4xCc3	patnáct
mezistátním	mezistátní	k2eAgNnPc3d1	mezistátní
utkáním	utkání	k1gNnSc7	utkání
s	s	k7c7	s
bilancí	bilance	k1gFnSc7	bilance
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2008	[number]	k4	2008
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
sedmou	sedmý	k4xOgFnSc4	sedmý
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
dvojici	dvojice	k1gFnSc4	dvojice
s	s	k7c7	s
Věrou	Věra	k1gFnSc7	Věra
Zvonarevovou	Zvonarevová	k1gFnSc7	Zvonarevová
<g/>
.	.	kIx.	.
</s>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
opustily	opustit	k5eAaPmAgInP	opustit
po	po	k7c6	po
čtvrtfinálové	čtvrtfinálový	k2eAgFnSc6d1	čtvrtfinálová
prohře	prohra	k1gFnSc6	prohra
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
od	od	k7c2	od
pozdějších	pozdní	k2eAgFnPc2d2	pozdější
olympijských	olympijský	k2eAgFnPc2d1	olympijská
vítězek	vítězka	k1gFnPc2	vítězka
Sereny	Serena	k1gFnSc2	Serena
a	a	k8xC	a
Venus	Venus	k1gInSc1	Venus
Williamsových	Williamsových	k2eAgInSc1d1	Williamsových
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
se	se	k3xPyFc4	se
také	také	k9	také
londýnských	londýnský	k2eAgFnPc2d1	londýnská
Her	hra	k1gFnPc2	hra
XXX	XXX	kA	XXX
<g/>
.	.	kIx.	.
olympiády	olympiáda	k1gFnSc2	olympiáda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
ženském	ženský	k2eAgInSc6d1	ženský
deblu	debl	k1gInSc6	debl
startovala	startovat	k5eAaBmAgFnS	startovat
s	s	k7c7	s
Jekatěrinou	Jekatěrin	k2eAgFnSc7d1	Jekatěrina
Makarovovou	Makarovová	k1gFnSc7	Makarovová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
turnajových	turnajový	k2eAgFnPc2d1	turnajová
šestek	šestka	k1gFnPc2	šestka
dohrály	dohrát	k5eAaPmAgFnP	dohrát
mezi	mezi	k7c7	mezi
poslední	poslední	k2eAgFnSc7d1	poslední
osmičkou	osmička	k1gFnSc7	osmička
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
když	když	k8xS	když
podlehly	podlehnout	k5eAaPmAgFnP	podlehnout
nejvýše	vysoce	k6eAd3	vysoce
nasazeným	nasazený	k2eAgFnPc3d1	nasazená
Američankám	Američanka	k1gFnPc3	Američanka
Liezel	Liezel	k1gFnSc4	Liezel
Huberové	Huberové	k2eAgFnSc4d1	Huberové
s	s	k7c7	s
Lisou	Lisa	k1gFnSc7	Lisa
Raymondovou	Raymondový	k2eAgFnSc7d1	Raymondová
po	po	k7c6	po
setech	set	k1gInPc6	set
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
a	a	k8xC	a
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Michailem	Michail	k1gMnSc7	Michail
Južným	Južný	k1gMnSc7	Južný
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
na	na	k7c4	na
divokou	divoký	k2eAgFnSc4d1	divoká
kartu	karta	k1gFnSc4	karta
i	i	k9	i
do	do	k7c2	do
smíšené	smíšený	k2eAgFnSc2d1	smíšená
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
je	být	k5eAaImIp3nS	být
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
argentinská	argentinský	k2eAgFnSc1d1	Argentinská
dvojice	dvojice	k1gFnSc1	dvojice
Gisela	Gisela	k1gFnSc1	Gisela
Dulková	Dulková	k1gFnSc1	Dulková
a	a	k8xC	a
Juan	Juan	k1gMnSc1	Juan
Martín	Martína	k1gFnPc2	Martína
del	del	k?	del
Potro	Potro	k1gNnSc4	Potro
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Riodejaneirské	riodejaneirský	k2eAgFnSc6d1	riodejaneirský
olympiádě	olympiáda	k1gFnSc6	olympiáda
2016	[number]	k4	2016
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
s	s	k7c7	s
Makarovovou	Makarovová	k1gFnSc7	Makarovová
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zdolaly	zdolat	k5eAaPmAgFnP	zdolat
za	za	k7c4	za
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
39	[number]	k4	39
hodin	hodina	k1gFnPc2	hodina
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
turnajové	turnajový	k2eAgFnSc2d1	turnajová
pětky	pětka	k1gFnSc2	pětka
Timeu	Timea	k1gFnSc4	Timea
Bacsinszkou	Bacsinszka	k1gFnSc7	Bacsinszka
s	s	k7c7	s
Martinou	Martina	k1gFnSc7	Martina
Hingisovou	Hingisový	k2eAgFnSc7d1	Hingisová
po	po	k7c6	po
dvousetovém	dvousetový	k2eAgInSc6d1	dvousetový
průběhu	průběh	k1gInSc6	průběh
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Letní	letní	k2eAgFnSc6d1	letní
univerziádě	univerziáda	k1gFnSc6	univerziáda
2013	[number]	k4	2013
v	v	k7c6	v
Kazani	Kazaň	k1gFnSc6	Kazaň
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
z	z	k7c2	z
ženské	ženský	k2eAgFnSc2d1	ženská
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
<g/>
,	,	kIx,	,
v	v	k7c6	v
páru	pár	k1gInSc6	pár
s	s	k7c7	s
Anastasií	Anastasie	k1gFnSc7	Anastasie
Pavljučenkovovou	Pavljučenkovový	k2eAgFnSc7d1	Pavljučenkovový
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
ze	z	k7c2	z
smíšené	smíšený	k2eAgFnSc2d1	smíšená
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Andreje	Andrej	k1gMnSc2	Andrej
Kuzněcova	Kuzněcův	k2eAgFnSc1d1	Kuzněcova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2002	[number]	k4	2002
–	–	k?	–
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
šestnácti	šestnáct	k4xCc2	šestnáct
let	léto	k1gNnPc2	léto
a	a	k8xC	a
dvou	dva	k4xCgInPc2	dva
měsíců	měsíc	k1gInPc2	měsíc
–	–	k?	–
poprvé	poprvé	k6eAd1	poprvé
startovala	startovat	k5eAaBmAgFnS	startovat
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
turnaje	turnaj	k1gInSc2	turnaj
okruhu	okruh	k1gInSc2	okruh
ITF	ITF	kA	ITF
<g/>
,	,	kIx,	,
když	když	k8xS	když
obdržela	obdržet	k5eAaPmAgFnS	obdržet
divokou	divoký	k2eAgFnSc4d1	divoká
kartu	karta	k1gFnSc4	karta
na	na	k7c4	na
10	[number]	k4	10
<g/>
tisícovou	tisícový	k2eAgFnSc4d1	tisícová
událost	událost	k1gFnSc4	událost
v	v	k7c6	v
egyptské	egyptský	k2eAgFnSc6d1	egyptská
Gíze	Gíze	k1gFnSc6	Gíze
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zvládnutém	zvládnutý	k2eAgInSc6d1	zvládnutý
kvalifikačním	kvalifikační	k2eAgInSc6d1	kvalifikační
turnaji	turnaj	k1gInSc6	turnaj
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Premiérovou	premiérový	k2eAgFnSc7d1	premiérová
událostí	událost	k1gFnSc7	událost
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
Tour	Tour	k1gInSc4	Tour
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
říjnového	říjnový	k2eAgInSc2d1	říjnový
Kremlin	Kremlin	k2eAgInSc1d1	Kremlin
Cupu	cup	k1gInSc2	cup
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
Taškent	Taškent	k1gInSc4	Taškent
Open	Open	k1gInSc1	Open
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
soutěže	soutěž	k1gFnSc2	soutěž
nepostoupila	postoupit	k5eNaPmAgFnS	postoupit
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
až	až	k6eAd1	až
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
když	když	k8xS	když
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
s	s	k7c7	s
Argentinkou	Argentinka	k1gFnSc7	Argentinka
Marianou	Mariana	k1gFnSc7	Mariana
Díazovou	Díazová	k1gFnSc7	Díazová
Olivovou	Olivová	k1gFnSc7	Olivová
v	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
listopadového	listopadový	k2eAgInSc2d1	listopadový
Challenge	Challeng	k1gInSc2	Challeng
Bell	bell	k1gInSc1	bell
v	v	k7c6	v
Québecu	Québecus	k1gInSc6	Québecus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
kanadském	kanadský	k2eAgInSc6d1	kanadský
turnaji	turnaj	k1gInSc6	turnaj
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
první	první	k4xOgInSc1	první
titul	titul	k1gInSc1	titul
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
deblové	deblový	k2eAgFnSc2d1	deblová
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
krajankou	krajanka	k1gFnSc7	krajanka
Anastasií	Anastasie	k1gFnSc7	Anastasie
Rodionovovou	Rodionovová	k1gFnSc7	Rodionovová
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zdolaly	zdolat	k5eAaPmAgFnP	zdolat
lotyšsko-americký	lotyšskomerický	k2eAgInSc4d1	lotyšsko-americký
pár	pár	k1gInSc4	pár
Lī	Lī	k1gFnSc1	Lī
Dekmeijereová	Dekmeijereová	k1gFnSc1	Dekmeijereová
a	a	k8xC	a
Ashley	Ashle	k2eAgFnPc4d1	Ashle
Harkleroadová	Harkleroadový	k2eAgNnPc4d1	Harkleroadový
<g/>
.	.	kIx.	.
</s>
<s>
Debutový	debutový	k2eAgInSc1d1	debutový
průlom	průlom	k1gInSc1	průlom
do	do	k7c2	do
elitní	elitní	k2eAgFnSc2d1	elitní
stovky	stovka	k1gFnSc2	stovka
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
přišel	přijít	k5eAaPmAgInS	přijít
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
posunula	posunout	k5eAaPmAgFnS	posunout
ze	z	k7c2	z
100	[number]	k4	100
<g/>
.	.	kIx.	.
na	na	k7c4	na
100	[number]	k4	100
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
pronikla	proniknout	k5eAaPmAgFnS	proniknout
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
do	do	k7c2	do
první	první	k4xOgFnSc2	první
padesátky	padesátka	k1gFnSc2	padesátka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
51	[number]	k4	51
<g/>
.	.	kIx.	.
na	na	k7c4	na
50	[number]	k4	50
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
prvních	první	k4xOgInPc2	první
deset	deset	k4xCc4	deset
deblistek	deblistka	k1gFnPc2	deblistka
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
premiérově	premiérově	k6eAd1	premiérově
podívala	podívat	k5eAaPmAgFnS	podívat
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
figurovala	figurovat	k5eAaImAgNnP	figurovat
na	na	k7c6	na
10	[number]	k4	10
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc4	pozice
světové	světový	k2eAgFnSc2d1	světová
klasifikace	klasifikace	k1gFnSc2	klasifikace
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
elitní	elitní	k2eAgFnSc2d1	elitní
pětky	pětka	k1gFnSc2	pětka
pak	pak	k6eAd1	pak
vystoupala	vystoupat	k5eAaPmAgFnS	vystoupat
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
sérii	série	k1gFnSc4	série
šesti	šest	k4xCc2	šest
prohraných	prohraná	k1gFnPc2	prohraná
finále	finále	k1gNnSc2	finále
dvouhry	dvouhra	k1gFnSc2	dvouhra
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
Tour	Tour	k1gMnSc1	Tour
–	–	k?	–
trvající	trvající	k2eAgMnSc1d1	trvající
od	od	k7c2	od
ASB	ASB	kA	ASB
Classic	Classice	k1gFnPc2	Classice
2009	[number]	k4	2009
s	s	k7c7	s
Dementěvovou	Dementěvová	k1gFnSc7	Dementěvová
až	až	k6eAd1	až
do	do	k7c2	do
porážky	porážka	k1gFnSc2	porážka
s	s	k7c7	s
Erraniovou	Erraniový	k2eAgFnSc7d1	Erraniová
na	na	k7c4	na
Budapest	Budapest	k1gFnSc4	Budapest
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
2012	[number]	k4	2012
–	–	k?	–
<g/>
,	,	kIx,	,
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
premiérový	premiérový	k2eAgInSc4d1	premiérový
titul	titul	k1gInSc4	titul
na	na	k7c6	na
lednovém	lednový	k2eAgInSc6d1	lednový
Moorilla	Moorillo	k1gNnSc2	Moorillo
Hobart	Hobart	k1gInSc1	Hobart
International	International	k1gFnSc4	International
2013	[number]	k4	2013
po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Barthelovou	Barthelová	k1gFnSc7	Barthelová
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
singlovou	singlový	k2eAgFnSc4d1	singlová
trofej	trofej	k1gFnSc4	trofej
přidala	přidat	k5eAaPmAgFnS	přidat
na	na	k7c6	na
travnatém	travnatý	k2eAgInSc6d1	travnatý
AEGON	AEGON	kA	AEGON
International	International	k1gFnSc1	International
2013	[number]	k4	2013
v	v	k7c6	v
Eastbourne	Eastbourn	k1gInSc5	Eastbourn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hladce	hladko	k6eAd1	hladko
přehrála	přehrát	k5eAaPmAgFnS	přehrát
Američanku	Američanka	k1gFnSc4	Američanka
Hamptonovou	Hamptonový	k2eAgFnSc4d1	Hamptonová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2013	[number]	k4	2013
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
s	s	k7c7	s
Jekatěrinou	Jekatěrin	k2eAgFnSc7d1	Jekatěrina
Makarovovou	Makarovová	k1gFnSc7	Makarovová
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
na	na	k7c6	na
březnovém	březnový	k2eAgNnSc6d1	březnové
BNP	BNP	kA	BNP
Paribas	Paribas	k1gMnSc1	Paribas
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
zdolaly	zdolat	k5eAaPmAgFnP	zdolat
Petrovovou	Petrovový	k2eAgFnSc4d1	Petrovová
se	se	k3xPyFc4	se
Srebotnikovou	Srebotnikový	k2eAgFnSc4d1	Srebotniková
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
a	a	k8xC	a
[	[	kIx(	[
<g/>
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Premiérový	premiérový	k2eAgInSc4d1	premiérový
grandslamový	grandslamový	k2eAgInSc4d1	grandslamový
triumf	triumf	k1gInSc4	triumf
si	se	k3xPyFc3	se
připsaly	připsat	k5eAaPmAgFnP	připsat
na	na	k7c6	na
antukovém	antukový	k2eAgInSc6d1	antukový
French	French	k1gInSc1	French
Open	Opena	k1gFnPc2	Opena
a	a	k8xC	a
také	také	k9	také
semifinálová	semifinálový	k2eAgFnSc1d1	semifinálová
účast	účast	k1gFnSc1	účast
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc4	Open
jim	on	k3xPp3gMnPc3	on
zajistila	zajistit	k5eAaPmAgFnS	zajistit
start	start	k1gInSc4	start
ve	v	k7c6	v
deblové	deblový	k2eAgFnSc6d1	deblová
soutěži	soutěž	k1gFnSc6	soutěž
istanbulského	istanbulský	k2eAgNnSc2d1	istanbulské
Tunraje	Tunraje	k1gFnSc4	Tunraje
mistryň	mistryně	k1gFnPc2	mistryně
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Kvalifikovaly	kvalifikovat	k5eAaBmAgInP	kvalifikovat
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
jako	jako	k8xC	jako
poslední	poslední	k2eAgInSc4d1	poslední
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
pár	pár	k1gInSc4	pár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
přesto	přesto	k8xC	přesto
vyřadily	vyřadit	k5eAaPmAgFnP	vyřadit
světové	světový	k2eAgFnPc4d1	světová
jedničky	jednička	k1gFnPc4	jednička
Erraniovou	Erraniový	k2eAgFnSc4d1	Erraniová
s	s	k7c7	s
Vinciovou	Vinciový	k2eAgFnSc7d1	Vinciová
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnSc4	on
ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
zastavila	zastavit	k5eAaPmAgFnS	zastavit
čínsko-tchajwanská	čínskochajwanský	k2eAgFnSc1d1	čínsko-tchajwanský
dvojice	dvojice	k1gFnSc1	dvojice
Pcheng	Pcheng	k1gMnSc1	Pcheng
Šuaj	Šuaj	k1gMnSc1	Šuaj
a	a	k8xC	a
Sie	Sie	k1gMnSc1	Sie
Su-wej	Suej	k1gInSc1	Su-wej
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
březnový	březnový	k2eAgInSc4d1	březnový
BNP	BNP	kA	BNP
Paribas	Paribas	k1gInSc1	Paribas
Open	Open	k1gNnSc1	Open
2017	[number]	k4	2017
v	v	k7c6	v
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wells	k1gInSc1	Wells
<g/>
,	,	kIx,	,
hraný	hraný	k2eAgInSc1d1	hraný
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
WTA	WTA	kA	WTA
Premier	Premier	k1gMnSc1	Premier
Mandatory	Mandator	k1gInPc4	Mandator
<g/>
,	,	kIx,	,
přijížděla	přijíždět	k5eAaImAgFnS	přijíždět
jako	jako	k9	jako
patnáctá	patnáctý	k4xOgFnSc1	patnáctý
hráčka	hráčka	k1gFnSc1	hráčka
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
dvě	dva	k4xCgFnPc4	dva
bývalé	bývalý	k2eAgFnPc4d1	bývalá
světové	světový	k2eAgFnPc4d1	světová
jedničky	jednička	k1gFnPc4	jednička
<g/>
,	,	kIx,	,
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
kole	kolo	k1gNnSc6	kolo
druhou	druhý	k4xOgFnSc4	druhý
ženu	žena	k1gFnSc4	žena
pořadí	pořadí	k1gNnSc2	pořadí
Angelique	Angeliqu	k1gFnSc2	Angeliqu
Kerberovou	Kerberův	k2eAgFnSc7d1	Kerberova
a	a	k8xC	a
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
Američanku	Američanka	k1gFnSc4	Američanka
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
desítky	desítka	k1gFnSc2	desítka
Venus	Venus	k1gInSc1	Venus
Williamsovou	Williamsová	k1gFnSc4	Williamsová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Francouzkou	Francouzka	k1gFnSc7	Francouzka
Kristinou	Kristina	k1gFnSc7	Kristina
Mladenovicovou	Mladenovicový	k2eAgFnSc7d1	Mladenovicová
pak	pak	k6eAd1	pak
pronikla	proniknout	k5eAaPmAgFnS	proniknout
do	do	k7c2	do
desátého	desátý	k4xOgNnSc2	desátý
kariérního	kariérní	k2eAgNnSc2d1	kariérní
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
za	za	k7c4	za
3.01	[number]	k4	3.01
hodin	hodina	k1gFnPc2	hodina
zdolala	zdolat	k5eAaPmAgFnS	zdolat
turnajovou	turnajový	k2eAgFnSc4d1	turnajová
osmičku	osmička	k1gFnSc4	osmička
Světlanu	Světlana	k1gFnSc4	Světlana
Kuzněcovovou	Kuzněcovová	k1gFnSc4	Kuzněcovová
po	po	k7c6	po
třísetovém	třísetový	k2eAgInSc6d1	třísetový
průběhu	průběh	k1gInSc6	průběh
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
duelu	duel	k1gInSc6	duel
dokázala	dokázat	k5eAaPmAgFnS	dokázat
otočit	otočit	k5eAaPmF	otočit
nepříznivý	příznivý	k2eNgInSc4d1	nepříznivý
vývoj	vývoj	k1gInSc4	vývoj
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
sadě	sada	k1gFnSc6	sada
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Krajanku	krajanka	k1gFnSc4	krajanka
zdolala	zdolat	k5eAaPmAgFnS	zdolat
podruhé	podruhé	k6eAd1	podruhé
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
vzájemných	vzájemný	k2eAgNnPc2d1	vzájemné
střetnutí	střetnutí	k1gNnPc2	střetnutí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
přitom	přitom	k6eAd1	přitom
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
již	již	k6eAd1	již
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
tehdy	tehdy	k6eAd1	tehdy
patřila	patřit	k5eAaImAgFnS	patřit
86	[number]	k4	86
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
třetí	třetí	k4xOgFnSc7	třetí
singlovou	singlový	k2eAgFnSc7d1	singlová
trofejí	trofej	k1gFnSc7	trofej
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
Tour	Tour	k1gInSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následném	následný	k2eAgNnSc6d1	následné
vydání	vydání	k1gNnSc6	vydání
žebříčku	žebříček	k1gInSc2	žebříček
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
se	se	k3xPyFc4	se
posunula	posunout	k5eAaPmAgFnS	posunout
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
maximum	maximum	k1gNnSc4	maximum
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
patřilo	patřit	k5eAaImAgNnS	patřit
13	[number]	k4	13
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
semifinále	semifinále	k1gNnSc2	semifinále
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
2016	[number]	k4	2016
přitom	přitom	k6eAd1	přitom
na	na	k7c6	na
žádném	žádný	k3yNgNnSc6	žádný
z	z	k7c2	z
třinácti	třináct	k4xCc2	třináct
turnajů	turnaj	k1gInPc2	turnaj
nevyhrála	vyhrát	k5eNaPmAgFnS	vyhrát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgNnPc1	dva
utkání	utkání	k1gNnPc1	utkání
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
šestkrát	šestkrát	k6eAd1	šestkrát
odešla	odejít	k5eAaPmAgFnS	odejít
poražena	porazit	k5eAaPmNgFnS	porazit
již	již	k6eAd1	již
v	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
ve	v	k7c6	v
Lvově	Lvův	k2eAgFnSc6d1	Lvův
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgFnSc3d1	tehdejší
Ukrajinské	ukrajinský	k2eAgFnSc3d1	ukrajinská
socialistické	socialistický	k2eAgFnSc3d1	socialistická
republice	republika	k1gFnSc3	republika
<g/>
,	,	kIx,	,
součásti	součást	k1gFnPc1	součást
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Sergej	Sergej	k1gMnSc1	Sergej
Vesnin	Vesnin	k2eAgMnSc1d1	Vesnin
ji	on	k3xPp3gFnSc4	on
trénuje	trénovat	k5eAaImIp3nS	trénovat
a	a	k8xC	a
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Irina	Irina	k1gFnSc1	Irina
Vesninová	Vesninový	k2eAgFnSc1d1	Vesninová
je	být	k5eAaImIp3nS	být
učitelka	učitelka	k1gFnSc1	učitelka
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
Dmitr	Dmitr	k1gMnSc1	Dmitr
Vesnin	Vesnin	k2eAgMnSc1d1	Vesnin
také	také	k9	také
hrál	hrát	k5eAaImAgMnS	hrát
profesionálně	profesionálně	k6eAd1	profesionálně
tenis	tenis	k1gInSc4	tenis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2015	[number]	k4	2015
se	se	k3xPyFc4	se
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c2	za
podnikatele	podnikatel	k1gMnSc2	podnikatel
Pavla	Pavel	k1gMnSc2	Pavel
Tabuncova	Tabuncův	k2eAgMnSc2d1	Tabuncův
<g/>
.	.	kIx.	.
</s>
