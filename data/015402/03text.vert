<s>
Stabyhoun	Stabyhoun	k1gMnSc1
</s>
<s>
Stabyhoun	Stabyhoun	k1gInSc1
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Země	zem	k1gFnSc2
původu	původ	k1gInSc2
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
Nizozemsko	Nizozemsko	k1gNnSc1
Využití	využití	k1gNnSc1
</s>
<s>
Lovecký	lovecký	k2eAgMnSc1d1
pes	pes	k1gMnSc1
<g/>
,	,	kIx,
společník	společník	k1gMnSc1
Tělesná	tělesný	k2eAgFnSc1d1
charakteristika	charakteristika	k1gFnSc1
Hmotnost	hmotnost	k1gFnSc1
</s>
<s>
15	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
kg	kg	kA
(	(	kIx(
<g/>
standard	standard	k1gInSc1
váhu	váha	k1gFnSc4
neuvádí	uvádět	k5eNaImIp3nS
<g/>
)	)	kIx)
Výška	výška	k1gFnSc1
†	†	k?
</s>
<s>
fena	fena	k1gFnSc1
50	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
<g/>
pes	pes	k1gMnSc1
53	#num#	k4
cm	cm	kA
Barva	barva	k1gFnSc1
</s>
<s>
Černá	černý	k2eAgFnSc1d1
<g/>
,	,	kIx,
hnědá	hnědý	k2eAgFnSc1d1
nebo	nebo	k8xC
oranžová	oranžový	k2eAgFnSc1d1
s	s	k7c7
bílými	bílý	k2eAgInPc7d1
znaky	znak	k1gInPc7
Klasifikace	klasifikace	k1gFnSc2
a	a	k8xC
standard	standard	k1gInSc1
Skupina	skupina	k1gFnSc1
FCI	FCI	kA
</s>
<s>
Ohaři	ohař	k1gMnPc1
Sekce	sekce	k1gFnSc2
FCI	FCI	kA
</s>
<s>
Kontinentální	kontinentální	k2eAgMnPc1d1
ohaři	ohař	k1gMnPc1
Podsekce	Podsekce	k1gFnSc2
FCI	FCI	kA
</s>
<s>
Typu	typ	k1gInSc2
španěl	španěl	k1gMnSc1
(	(	kIx(
<g/>
dlouhosrstí	dlouhosrstý	k2eAgMnPc1d1
<g/>
)	)	kIx)
</s>
<s>
†	†	k?
výška	výška	k1gFnSc1
uváděna	uváděn	k2eAgFnSc1d1
v	v	k7c6
kohoutku	kohoutek	k1gInSc6
</s>
<s>
Stabyhoun	Stabyhoun	k1gMnSc1
nebo	nebo	k8xC
Stabij	Stabij	k1gMnSc1
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
také	také	k9
jako	jako	k9
Fríský	fríský	k2eAgMnSc1d1
ohař	ohař	k1gMnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nizozemské	nizozemský	k2eAgNnSc1d1
psí	psí	k2eAgNnSc1d1
plemeno	plemeno	k1gNnSc1
vyšlechtěné	vyšlechtěný	k2eAgNnSc1d1
na	na	k7c6
začátku	začátek	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Stabyhoun	Stabyhoun	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
Nizozemska	Nizozemsko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
vyšlechtěn	vyšlechtit	k5eAaPmNgInS
v	v	k7c6
provincii	provincie	k1gFnSc6
Frísko	Frísko	k1gNnSc4
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yRgFnSc2,k3yIgFnSc2,k3yQgFnSc2
je	být	k5eAaImIp3nS
pojmenován	pojmenovat	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
verzí	verze	k1gFnPc2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
vzniku	vznik	k1gInSc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
na	na	k7c6
začátku	začátek	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
křížili	křížit	k5eAaImAgMnP
drentští	drentský	k2eAgMnPc1d1
koroptváři	koroptvář	k1gMnPc1
a	a	k8xC
francouzskými	francouzský	k2eAgMnPc7d1
a	a	k8xC
německými	německý	k2eAgMnPc7d1
španěly	španěl	k1gMnPc7
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhou	druhý	k4xOgFnSc7
verzí	verze	k1gFnSc7
je	být	k5eAaImIp3nS
ta	ten	k3xDgFnSc1
<g/>
,	,	kIx,
že	že	k8xS
byli	být	k5eAaImAgMnP
do	do	k7c2
Nizozemska	Nizozemsko	k1gNnSc2
tito	tento	k3xDgMnPc1
psi	pes	k1gMnPc1
přivezeni	přivezen	k2eAgMnPc1d1
při	při	k7c6
vpádu	vpád	k1gInSc6
Španělů	Španěl	k1gMnPc2
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc7
původním	původní	k2eAgNnSc7d1
zaměřením	zaměření	k1gNnSc7
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
nahánění	nahánění	k1gNnSc4
a	a	k8xC
lov	lov	k1gInSc4
zvěře	zvěř	k1gFnSc2
v	v	k7c6
těžko	těžko	k6eAd1
dostupném	dostupný	k2eAgInSc6d1
terénu	terén	k1gInSc6
<g/>
,	,	kIx,
zde	zde	k6eAd1
se	se	k3xPyFc4
ale	ale	k9
neosvědčil	osvědčit	k5eNaPmAgMnS
a	a	k8xC
populace	populace	k1gFnSc1
těchto	tento	k3xDgMnPc2
psů	pes	k1gMnPc2
se	se	k3xPyFc4
velmi	velmi	k6eAd1
snížila	snížit	k5eAaPmAgFnS
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadále	nadále	k6eAd1
byli	být	k5eAaImAgMnP
využívání	využívání	k1gNnSc4
jen	jen	k6eAd1
jako	jako	k8xC,k8xS
lovci	lovec	k1gMnPc1
krys	krysa	k1gFnPc2
a	a	k8xC
jiné	jiný	k2eAgFnSc2d1
škodné	škodná	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
chován	chovat	k5eAaImNgInS
již	již	k6eAd1
jen	jen	k6eAd1
jako	jako	k8xC,k8xS
společník	společník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
dva	dva	k4xCgMnPc1
Fríští	fríský	k2eAgMnPc1d1
ohaři	ohař	k1gMnPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
používaná	používaný	k2eAgFnSc1d1
v	v	k7c6
Česku	Česko	k1gNnSc6
je	být	k5eAaImIp3nS
STA	sto	k4xCgNnPc1
a	a	k8xC
zatím	zatím	k6eAd1
jej	on	k3xPp3gMnSc4
zde	zde	k6eAd1
nezastřešuje	zastřešovat	k5eNaImIp3nS
žádný	žádný	k3yNgInSc1
chovatelský	chovatelský	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
světě	svět	k1gInSc6
je	být	k5eAaImIp3nS
toto	tento	k3xDgNnSc1
plemeno	plemeno	k1gNnSc1
vzácné	vzácný	k2eAgNnSc1d1
a	a	k8xC
vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
hlavně	hlavně	k9
v	v	k7c6
zemi	zem	k1gFnSc6
svého	svůj	k3xOyFgInSc2
původu	původ	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
Nizozemsku	Nizozemsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
dospělý	dospělý	k2eAgMnSc1d1
pes	pes	k1gMnSc1
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
silný	silný	k2eAgMnSc1d1
pes	pes	k1gMnSc1
obdélníkového	obdélníkový	k2eAgInSc2d1
rámce	rámec	k1gInSc2
spíše	spíše	k9
s	s	k7c7
jemnou	jemný	k2eAgFnSc7d1
konstrukcí	konstrukce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlava	hlava	k1gFnSc1
je	být	k5eAaImIp3nS
suchá	suchý	k2eAgFnSc1d1
a	a	k8xC
dlouhá	dlouhý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mozkovna	mozkovna	k1gFnSc1
je	být	k5eAaImIp3nS
stejně	stejně	k6eAd1
dlouhá	dlouhý	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
čenichová	čenichový	k2eAgFnSc1d1
partie	partie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stop	stop	k1gInSc1
mírný	mírný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pysky	pysk	k1gInPc4
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
přiléhavé	přiléhavý	k2eAgNnSc4d1
<g/>
,	,	kIx,
ne	ne	k9
plandavé	plandavý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zuby	zub	k1gInPc7
jsou	být	k5eAaImIp3nP
silné	silný	k2eAgInPc1d1
a	a	k8xC
mají	mít	k5eAaImIp3nP
pravidelný	pravidelný	k2eAgInSc4d1
nůžkovitý	nůžkovitý	k2eAgInSc4d1
skus	skus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oči	oko	k1gNnPc1
jsou	být	k5eAaImIp3nP
středně	středně	k6eAd1
velké	velký	k2eAgInPc1d1
a	a	k8xC
kulaté	kulatý	k2eAgInPc1d1
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
tmavě	tmavě	k6eAd1
hnědé	hnědý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uši	ucho	k1gNnPc1
jsou	být	k5eAaImIp3nP
zasazené	zasazený	k2eAgFnPc4d1
nízko	nízko	k6eAd1
a	a	k8xC
srst	srst	k1gFnSc4
na	na	k7c6
nich	on	k3xPp3gNnPc6
tvoří	tvořit	k5eAaImIp3nP
krátké	krátký	k2eAgInPc1d1
volánky	volánek	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hřbet	hřbet	k1gInSc1
rovný	rovný	k2eAgInSc1d1
a	a	k8xC
dlouhý	dlouhý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ocas	ocas	k1gInSc1
je	být	k5eAaImIp3nS
dlouhý	dlouhý	k2eAgInSc1d1
a	a	k8xC
nasazený	nasazený	k2eAgMnSc1d1
nízko	nízko	k6eAd1
a	a	k8xC
srst	srst	k1gFnSc4
netvoří	tvořit	k5eNaImIp3nP
praporky	praporek	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Možná	možná	k9
záměna	záměna	k1gFnSc1
</s>
<s>
Stabyhoun	Stabyhoun	k1gInSc1
je	být	k5eAaImIp3nS
zbarvením	zbarvení	k1gNnSc7
i	i	k8xC
velikostí	velikost	k1gFnSc7
podobný	podobný	k2eAgInSc4d1
malému	malý	k2eAgMnSc3d1
münsterlandskému	münsterlandský	k2eAgMnSc3d1
ohaři	ohař	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbarvením	zbarvení	k1gNnSc7
je	být	k5eAaImIp3nS
podobný	podobný	k2eAgInSc1d1
i	i	k9
jednomu	jeden	k4xCgMnSc3
ze	z	k7c2
svých	svůj	k3xOyFgMnPc2
předků	předek	k1gMnPc2
–	–	k?
drentskému	drentský	k2eAgMnSc3d1
koroptváři	koroptvář	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Povaha	povaha	k1gFnSc1
</s>
<s>
Stabyhoun	Stabyhoun	k1gInSc4
je	být	k5eAaImIp3nS
milé	milý	k2eAgNnSc1d1
<g/>
,	,	kIx,
přátelské	přátelský	k2eAgNnSc1d1
a	a	k8xC
citlivé	citlivý	k2eAgNnSc1d1
psí	psí	k2eAgNnSc1d1
plemeno	plemeno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
oddaný	oddaný	k2eAgMnSc1d1
své	svůj	k3xOyFgFnSc3
rodině	rodina	k1gFnSc3
a	a	k8xC
chrání	chránit	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
hravý	hravý	k2eAgMnSc1d1
a	a	k8xC
aktivní	aktivní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
je	být	k5eAaImIp3nS
inteligentní	inteligentní	k2eAgMnSc1d1
a	a	k8xC
rychle	rychle	k6eAd1
se	se	k3xPyFc4
učí	učit	k5eAaImIp3nS
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
dobře	dobře	k6eAd1
vycvičit	vycvičit	k5eAaPmF
a	a	k8xC
je	být	k5eAaImIp3nS
vhodný	vhodný	k2eAgInSc1d1
i	i	k9
pro	pro	k7c4
začínající	začínající	k2eAgMnPc4d1
chovatele	chovatel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
klidný	klidný	k2eAgInSc1d1
a	a	k8xC
nereaguje	reagovat	k5eNaBmIp3nS
agresivně	agresivně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rád	rád	k6eAd1
aportuje	aportovat	k5eAaImIp3nS
z	z	k7c2
vody	voda	k1gFnSc2
i	i	k9
na	na	k7c6
souši	souš	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Děti	dítě	k1gFnPc4
má	mít	k5eAaImIp3nS
rád	rád	k2eAgMnSc1d1
a	a	k8xC
dětské	dětský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
mu	on	k3xPp3gMnSc3
nevadí	vadit	k5eNaImIp3nP
<g/>
,	,	kIx,
proto	proto	k8xC
je	být	k5eAaImIp3nS
vhodným	vhodný	k2eAgMnSc7d1
společníkem	společník	k1gMnSc7
i	i	k9
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stabyhoun	Stabyhoun	k1gInSc1
není	být	k5eNaImIp3nS
zvyklý	zvyklý	k2eAgInSc1d1
spolupracovat	spolupracovat	k5eAaImF
s	s	k7c7
jinými	jiný	k2eAgMnPc7d1
psy	pes	k1gMnPc7
ve	v	k7c6
smečce	smečka	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
umí	umět	k5eAaImIp3nS
si	se	k3xPyFc3
na	na	k7c4
ně	on	k3xPp3gMnPc4
zvyknout	zvyknout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiná	jiná	k1gFnSc1
zvířata	zvíře	k1gNnPc4
snese	snést	k5eAaPmIp3nS
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
ve	v	k7c6
štěněcím	štěněcí	k2eAgInSc6d1
věku	věk	k1gInSc6
s	s	k7c7
nimi	on	k3xPp3gInPc7
seznámen	seznámen	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s>
Péče	péče	k1gFnSc1
</s>
<s>
Srst	srst	k1gFnSc1
stabyhouna	stabyhoun	k1gMnSc2
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
často	často	k6eAd1
pročesávat	pročesávat	k5eAaImF
a	a	k8xC
kartáčovat	kartáčovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zacuchanou	zacuchaný	k2eAgFnSc4d1
srst	srst	k1gFnSc4
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
odstřihnout	odstřihnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
srst	srst	k1gFnSc1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c6
uších	ucho	k1gNnPc6
volánky	volánek	k1gInPc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
dbát	dbát	k5eAaImF
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
na	na	k7c6
nich	on	k3xPp3gMnPc6
nezůstávala	zůstávat	k5eNaImAgFnS
voda	voda	k1gFnSc1
nebo	nebo	k8xC
špína	špína	k1gFnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
způsobit	způsobit	k5eAaPmF
zánět	zánět	k1gInSc4
zevního	zevní	k2eAgInSc2d1
zvukovodu	zvukovod	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
toto	tento	k3xDgNnSc4
plemeno	plemeno	k1gNnSc4
trpí	trpět	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potřebují	potřebovat	k5eAaImIp3nP
důrazný	důrazný	k2eAgInSc4d1
výcvik	výcvik	k1gInSc4
i	i	k8xC
výchovu	výchova	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cvičí	cvičit	k5eAaImIp3nS
se	se	k3xPyFc4
dobře	dobře	k6eAd1
a	a	k8xC
rychle	rychle	k6eAd1
<g/>
,	,	kIx,
proto	proto	k8xC
jsou	být	k5eAaImIp3nP
vhodní	vhodný	k2eAgMnPc1d1
i	i	k8xC
pro	pro	k7c4
začátečníky	začátečník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potřebují	potřebovat	k5eAaImIp3nP
pohyb	pohyb	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
vhodné	vhodný	k2eAgInPc1d1
veškeré	veškerý	k3xTgInPc1
typy	typ	k1gInPc1
pohybu	pohyb	k1gInSc2
<g/>
,	,	kIx,
od	od	k7c2
běhání	běhání	k1gNnSc2
při	při	k7c6
kole	kolo	k1gNnSc6
až	až	k9
po	po	k7c4
psí	psí	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://cmku.cz/soubory/plemena/222.doc	http://cmku.cz/soubory/plemena/222.doc	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
FOGLE	FOGLE	kA
<g/>
,	,	kIx,
Bruce	Bruce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
psů	pes	k1gMnPc2
<g/>
.	.	kIx.
6	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Slovart	Slovart	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
409	#num#	k4
s.	s.	k?
S.	S.	kA
7	#num#	k4
<g/>
,	,	kIx,
229	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Odstavec	odstavec	k1gInSc1
"	"	kIx"
<g/>
Vzhled	vzhled	k1gInSc1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
výtahem	výtah	k1gInSc7
z	z	k7c2
oficiálního	oficiální	k2eAgInSc2d1
platného	platný	k2eAgInSc2d1
standardu	standard	k1gInSc2
plemene	plemeno	k1gNnSc2
stabyhoun	stabyhoun	k1gInSc1
dle	dle	k7c2
FCI	FCI	kA
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Stabyhoun	Stabyhoun	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Pes	pes	k1gMnSc1
domácí	domácí	k1gMnSc1
</s>
