<s>
Stabyhoun	Stabyhoun	k1gMnSc1
nebo	nebo	k8xC
Stabij	Stabij	k1gMnSc1
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
také	také	k9
jako	jako	k9
Fríský	fríský	k2eAgMnSc1d1
ohař	ohař	k1gMnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nizozemské	nizozemský	k2eAgNnSc1d1
psí	psí	k2eAgNnSc1d1
plemeno	plemeno	k1gNnSc1
vyšlechtěné	vyšlechtěný	k2eAgNnSc1d1
na	na	k7c6
začátku	začátek	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>