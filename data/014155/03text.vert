<s>
Ludovico	Ludovico	k6eAd1
Antonio	Antonio	k1gMnSc1
Muratori	Murator	k1gFnSc2
</s>
<s>
Ludovico	Ludovico	k1gMnSc1
Antonio	Antonio	k1gMnSc1
Muratori	Muratore	k1gFnSc4
Narození	narození	k1gNnSc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1672	#num#	k4
<g/>
Vignola	Vignola	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1750	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
77	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Modena	Modena	k1gFnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Modenská	modenský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
historik	historik	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
kněz	kněz	k1gMnSc1
<g/>
,	,	kIx,
knihovník	knihovník	k1gMnSc1
<g/>
,	,	kIx,
archivář	archivář	k1gMnSc1
<g/>
,	,	kIx,
filozof	filozof	k1gMnSc1
<g/>
,	,	kIx,
numismatik	numismatik	k1gMnSc1
a	a	k8xC
presbyter	presbyter	k1gMnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
člen	člen	k1gMnSc1
Královské	královský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
katolická	katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lodovico	Lodovico	k6eAd1
Antonio	Antonio	k1gMnSc1
Muratori	Murator	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1662	#num#	k4
<g/>
,	,	kIx,
Vignola	Vignola	k1gFnSc1
–	–	k?
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1750	#num#	k4
<g/>
,	,	kIx,
Modena	Modena	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
italský	italský	k2eAgMnSc1d1
katolický	katolický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
<g/>
,	,	kIx,
osvícenský	osvícenský	k2eAgMnSc1d1
vzdělanec	vzdělanec	k1gMnSc1
a	a	k8xC
otec	otec	k1gMnSc1
italského	italský	k2eAgNnSc2d1
dějepisectví	dějepisectví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
spisy	spis	k1gInPc7
měly	mít	k5eAaImAgFnP
velký	velký	k2eAgInSc4d1
vliv	vliv	k1gInSc4
i	i	k9
na	na	k7c4
osvícenskou	osvícenský	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
v	v	k7c6
habsburské	habsburský	k2eAgFnSc6d1
monarchii	monarchie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Muratoriho	Muratorize	k6eAd1
myšlenky	myšlenka	k1gFnSc2
šířila	šířit	k5eAaImAgFnS
první	první	k4xOgFnSc1
učená	učený	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
na	na	k7c6
území	území	k1gNnSc6
ovládaném	ovládaný	k2eAgNnSc6d1
rakouskými	rakouský	k2eAgMnPc7d1
Habsburky	Habsburk	k1gMnPc7
Societas	Societas	k1gInSc4
eruditorum	eruditorum	k1gInSc1
incognitorum	incognitorum	k1gInSc1
in	in	k?
terris	terris	k1gFnSc2
Austriacis	Austriacis	k1gFnSc2
prostřednictvím	prostřednictvím	k7c2
prvního	první	k4xOgInSc2
vědeckého	vědecký	k2eAgInSc2d1
časopisu	časopis	k1gInSc2
v	v	k7c6
monarchii	monarchie	k1gFnSc6
Monatliche	Monatlich	k1gMnSc2
Auszüge	Auszüg	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Výběr	výběr	k1gInSc1
z	z	k7c2
díla	dílo	k1gNnSc2
</s>
<s>
Della	Della	k1gMnSc1
pubblica	pubblica	k1gMnSc1
felicità	felicità	k?
<g/>
,	,	kIx,
1749	#num#	k4
</s>
<s>
Anecdota	Anecdota	k1gFnSc1
<g/>
,	,	kIx,
quae	quaat	k5eAaPmIp3nS
ex	ex	k6eAd1
ambrosianae	ambrosianae	k6eAd1
bibliothecae	bibliothecaat	k5eAaPmIp3nS
codicibus	codicibus	k1gInSc1
<g/>
,	,	kIx,
I-IV	I-IV	k1gFnSc5
<g/>
,	,	kIx,
Milano	Milana	k1gFnSc5
1697	#num#	k4
<g/>
,	,	kIx,
1698	#num#	k4
<g/>
,	,	kIx,
Padua	Padua	k1gFnSc1
1713	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Primi	Pri	k1gFnPc7
disegni	disegni	k5eAaImNgMnP
della	della	k1gMnSc1
Repubblica	Repubblicus	k1gMnSc2
letteraria	letterarium	k1gNnSc2
d	d	k?
<g/>
'	'	kIx"
<g/>
Italia	Italius	k1gMnSc2
<g/>
,	,	kIx,
Venezia	Venezius	k1gMnSc2
<g/>
,	,	kIx,
1703	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Della	Della	k1gFnSc1
perfetta	perfetta	k1gFnSc1
poesia	poesia	k1gFnSc1
italiana	italiana	k1gFnSc1
<g/>
,	,	kIx,
Modena	Modena	k1gFnSc1
1706	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Riflessioni	Riflessioň	k1gFnSc3
sopra	sopra	k1gMnSc1
il	il	k?
buon	buon	k1gMnSc1
gusto	gusto	k1gNnSc1
<g/>
,	,	kIx,
Venezia	Venezia	k1gFnSc1
1708	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Anecdota	Anecdota	k1gFnSc1
Graeca	Graeca	k1gFnSc1
<g/>
,	,	kIx,
1709	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Piena	Piena	k6eAd1
esposizione	esposizion	k1gInSc5
dei	dei	k?
diritti	diritt	k1gMnPc1
imperiali	imperiat	k5eAaImAgMnP,k5eAaPmAgMnP,k5eAaBmAgMnP
ed	ed	k?
estensi	estense	k1gFnSc6
sopra	sopra	k6eAd1
la	la	k1gNnPc2
città	città	k?
di	di	k?
Comacchio	Comacchio	k1gMnSc1
<g/>
,	,	kIx,
Modena	Modena	k1gFnSc1
1712	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
De	De	k?
ingeniorum	ingeniorum	k1gInSc1
moderatione	moderation	k1gInSc5
in	in	k?
religionis	religionis	k1gFnSc3
negotio	negotio	k1gMnSc1
<g/>
,	,	kIx,
Paris	Paris	k1gMnSc1
1714	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rerum	Rerum	k1gNnSc1
italicarum	italicarum	k1gInSc1
scriptores	scriptores	k1gMnSc1
I-XXVII	I-XXVII	k1gMnSc1
<g/>
,	,	kIx,
Milano	Milana	k1gFnSc5
1723	#num#	k4
<g/>
-	-	kIx~
<g/>
1738	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Antiquitates	Antiquitates	k1gInSc4
italicae	italicae	k6eAd1
Medii	medium	k1gNnPc7
Aevi	Aev	k1gFnSc2
<g/>
,	,	kIx,
I-VI	I-VI	k1gFnSc5
<g/>
,	,	kIx,
Milano	Milana	k1gFnSc5
1738	#num#	k4
<g/>
-	-	kIx~
<g/>
1743	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Della	Della	k1gFnSc1
carità	carità	k?
cristiana	cristiana	k1gFnSc1
<g/>
,	,	kIx,
Modena	Modena	k1gFnSc1
1723	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
De	De	k?
regolata	regole	k1gNnPc1
devotione	devotion	k1gInSc5
de	de	k?
<g/>
´	´	k?
<g/>
christiani	christian	k1gMnPc1
<g/>
,	,	kIx,
1723	#num#	k4
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
<g/>
:	:	kIx,
O	o	k7c6
pravé	pravý	k2eAgFnSc6d1
křestianské	křestianský	k2eAgFnSc6d1
pobožnosti	pobožnost	k1gFnSc6
<g/>
,	,	kIx,
kniha	kniha	k1gFnSc1
Laminda	Laminda	k1gFnSc1
Pryntanya	Pryntanya	k1gFnSc1
aneb	aneb	k?
slovutného	slovutný	k2eAgMnSc4d1
muže	muž	k1gMnSc4
Ludvíka	Ludvík	k1gMnSc4
Antonè	Antonè	k1gMnSc4
Muratorya	Muratoryus	k1gMnSc4
<g/>
,	,	kIx,
we	we	k?
wlaským	wlaský	k2eAgFnPc3d1
jazyku	jazyk	k1gInSc6
sepsaná	sepsaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
pak	pak	k6eAd1
do	do	k7c2
cžeštiny	cžeština	k1gFnSc2
přenesená	přenesený	k2eAgFnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1778	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
De	De	k?
superstitione	superstition	k1gInSc5
vitanda	vitanda	k1gFnSc1
<g/>
,	,	kIx,
Venezia	Venezia	k1gFnSc1
1743	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dei	Dei	k?
difetti	difetti	k1gNnSc1
della	della	k1gFnSc1
giurisprudenza	giurisprudenza	k1gFnSc1
<g/>
,	,	kIx,
Venezia	Venezia	k1gFnSc1
1742	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Delle	Delle	k6eAd1
forze	forze	k6eAd1
dell	dell	k1gInSc1
<g/>
'	'	kIx"
<g/>
intendimento	intendimento	k1gNnSc1
umano	umano	k1gNnSc1
<g/>
,	,	kIx,
Venezia	Venezia	k1gFnSc1
1745	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Della	Della	k1gFnSc1
forza	forza	k1gFnSc1
della	della	k1gFnSc1
fantasia	fantasia	k1gFnSc1
umana	umana	k1gFnSc1
<g/>
,	,	kIx,
Venezia	Venezia	k1gFnSc1
1745	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Annali	Annat	k5eAaBmAgMnP,k5eAaImAgMnP,k5eAaPmAgMnP
d	d	k?
<g/>
'	'	kIx"
<g/>
Italia	Italia	k1gFnSc1
<g/>
,	,	kIx,
Venezia	Venezia	k1gFnSc1
1744	#num#	k4
<g/>
-	-	kIx~
<g/>
1749	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Cristianesimo	Cristianesimo	k6eAd1
felice	felice	k1gFnSc1
nelle	nelle	k1gFnSc2
missioni	mission	k1gMnPc1
de	de	k?
<g/>
'	'	kIx"
padri	padr	k1gFnSc2
della	dell	k1gMnSc2
Compagnia	Compagnium	k1gNnSc2
di	di	k?
Gesù	Gesù	k1gMnSc1
nel	nel	k?
Paraguay	Paraguay	k1gFnSc1
<g/>
,	,	kIx,
1743	#num#	k4
<g/>
-	-	kIx~
<g/>
1749	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
De	De	k?
Jurisprudentiae	Jurisprudentiae	k1gFnSc1
Naevis	Naevis	k1gFnSc2
Dissertatio	Dissertatio	k1gNnSc1
<g/>
,	,	kIx,
1753	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Lodovico	Lodovico	k1gMnSc1
Antonio	Antonio	k1gMnSc1
Muratori-Annali	Muratori-Annali	k1gMnSc1
Italia-Roma	Italia-Roma	k1gFnSc1
<g/>
,	,	kIx,
1752	#num#	k4
<g/>
,	,	kIx,
t.	t.	k?
I	I	kA
<g/>
,	,	kIx,
p.	p.	k?
I	i	k8xC
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Winter	Winter	k1gMnSc1
Eduard	Eduard	k1gMnSc1
<g/>
,	,	kIx,
Josefinismus	josefinismus	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
dějiny	dějiny	k1gFnPc1
<g/>
:	:	kIx,
Příspěvek	příspěvek	k1gInSc1
k	k	k7c3
duchovním	duchovní	k2eAgFnPc3d1
dějinám	dějiny	k1gFnPc3
Čech	Čechy	k1gFnPc2
a	a	k8xC
Moravy	Morava	k1gFnSc2
1740	#num#	k4
<g/>
-	-	kIx~
<g/>
1848	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Jelínek	Jelínek	k1gMnSc1
<g/>
,	,	kIx,
1945	#num#	k4
<g/>
,	,	kIx,
14	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ludovico	Ludovico	k6eAd1
Antonio	Antonio	k1gMnSc1
Muratori	Murator	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Ludovico	Ludovico	k6eAd1
Antonio	Antonio	k1gMnSc1
Muratori	Murator	k1gFnSc2
</s>
<s>
http://www.letteraturaitaliana.net/autori/muratori_ludovico_antonio.html	http://www.letteraturaitaliana.net/autori/muratori_ludovico_antonio.html	k1gMnSc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Ludovico	Ludovico	k1gMnSc1
Antonio	Antonio	k1gMnSc1
Muratori	Muratori	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
wissen	wissen	k1gInSc1
<g/>
.	.	kIx.
<g/>
spiegel	spiegel	k1gInSc1
<g/>
.	.	kIx.
<g/>
de	de	k?
</s>
<s>
76	#num#	k4
Dissertazioni	Dissertazioň	k1gFnSc6
sopra	sopr	k1gInSc2
le	le	k?
antiquità	antiquità	k?
italiane	italian	k1gMnSc5
<g/>
,	,	kIx,
Bände	Bänd	k1gMnSc5
II	II	kA
<g/>
–	–	k?
<g/>
V	v	k7c6
der	drát	k5eAaImRp2nS
kritischen	kritischen	k1gInSc4
Neuauflage	Neuauflag	k1gMnSc2
<g/>
,	,	kIx,
Mailand	Mailand	k1gInSc4
1837	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20011024186	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118844520	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2137	#num#	k4
9855	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79018678	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
68933348	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79018678	#num#	k4
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Historie	historie	k1gFnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Itálie	Itálie	k1gFnSc1
</s>
