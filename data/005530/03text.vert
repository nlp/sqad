<p>
<s>
AZ	AZ	kA	AZ
Tower	Tower	k1gInSc1	Tower
je	být	k5eAaImIp3nS	být
výšková	výškový	k2eAgFnSc1d1	výšková
budova	budova	k1gFnSc1	budova
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
budova	budova	k1gFnSc1	budova
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2011	[number]	k4	2011
až	až	k9	až
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c4	v
ulici	ulice	k1gFnSc4	ulice
Pražákova	Pražákův	k2eAgFnSc1d1	Pražákova
<g/>
,	,	kIx,	,
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Štýřice	Štýřice	k1gFnSc2	Štýřice
<g/>
,	,	kIx,	,
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Brno-střed	Brnotřed	k1gInSc4	Brno-střed
<g/>
,	,	kIx,	,
poblíž	poblíž	k6eAd1	poblíž
M-Paláce	M-Paláce	k1gFnSc2	M-Paláce
a	a	k8xC	a
objektu	objekt	k1gInSc2	objekt
Spielberk	Spielberk	k1gInSc4	Spielberk
Tower	Towero	k1gNnPc2	Towero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
popis	popis	k1gInSc1	popis
budovy	budova	k1gFnSc2	budova
==	==	k?	==
</s>
</p>
<p>
<s>
Stavba	stavba	k1gFnSc1	stavba
mrakodrapu	mrakodrap	k1gInSc2	mrakodrap
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
zahájena	zahájen	k2eAgFnSc1d1	zahájena
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
dodavatelem	dodavatel	k1gMnSc7	dodavatel
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
PSJ	PSJ	kA	PSJ
<g/>
,	,	kIx,	,
investorem	investor	k1gMnSc7	investor
společnost	společnost	k1gFnSc1	společnost
Properity	Properit	k1gInPc4	Properit
<g/>
,	,	kIx,	,
celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
měly	mít	k5eAaImAgInP	mít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
částky	částka	k1gFnPc4	částka
800	[number]	k4	800
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Budovu	budova	k1gFnSc4	budova
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
Architektonická	architektonický	k2eAgFnSc1d1	architektonická
kancelář	kancelář	k1gFnSc1	kancelář
Burian	Burian	k1gMnSc1	Burian
–	–	k?	–
Křivinka	Křivinka	k1gFnSc1	Křivinka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
30	[number]	k4	30
podlaží	podlaží	k1gNnPc2	podlaží
a	a	k8xC	a
výšky	výška	k1gFnSc2	výška
111	[number]	k4	111
m	m	kA	m
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
výškovou	výškový	k2eAgFnSc7d1	výšková
budovou	budova	k1gFnSc7	budova
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
<g/>
Budova	budova	k1gFnSc1	budova
AZ	AZ	kA	AZ
Tower	Towra	k1gFnPc2	Towra
je	být	k5eAaImIp3nS	být
komplexem	komplex	k1gInSc7	komplex
třípodlažní	třípodlažní	k2eAgFnSc2d1	třípodlažní
víceúčelové	víceúčelový	k2eAgFnSc2d1	víceúčelová
podnože	podnož	k1gFnSc2	podnož
(	(	kIx(	(
<g/>
obchodní	obchodní	k2eAgFnSc1d1	obchodní
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
fitness	fitness	k1gInSc1	fitness
<g/>
,	,	kIx,	,
autoservis	autoservis	k1gInSc1	autoservis
<g/>
)	)	kIx)	)
s	s	k7c7	s
dvoupodlažním	dvoupodlažní	k2eAgNnSc7d1	dvoupodlažní
podzemním	podzemní	k2eAgNnSc7d1	podzemní
parkováním	parkování	k1gNnSc7	parkování
a	a	k8xC	a
výškové	výškový	k2eAgFnPc4d1	výšková
budovy	budova	k1gFnPc4	budova
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
nad	nad	k7c7	nad
částí	část	k1gFnSc7	část
půdorysu	půdorys	k1gInSc2	půdorys
stavby	stavba	k1gFnSc2	stavba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Železobetonová	železobetonový	k2eAgFnSc1d1	železobetonová
konstrukce	konstrukce	k1gFnSc1	konstrukce
věže	věž	k1gFnSc2	věž
s	s	k7c7	s
charakteristicky	charakteristicky	k6eAd1	charakteristicky
prolomenou	prolomený	k2eAgFnSc7d1	prolomená
fasádou	fasáda	k1gFnSc7	fasáda
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc1d1	obsahující
kancelářské	kancelářský	k2eAgFnPc4d1	kancelářská
prostory	prostora	k1gFnPc4	prostora
a	a	k8xC	a
apartmány	apartmán	k1gInPc4	apartmán
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
původně	původně	k6eAd1	původně
dosahovat	dosahovat	k5eAaImF	dosahovat
výšky	výška	k1gFnSc2	výška
85	[number]	k4	85
m.	m.	k?	m.
Po	po	k7c6	po
změně	změna	k1gFnSc6	změna
projektu	projekt	k1gInSc2	projekt
byla	být	k5eAaImAgFnS	být
realizována	realizovat	k5eAaBmNgFnS	realizovat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
109,5	[number]	k4	109,5
m	m	kA	m
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
ocelové	ocelový	k2eAgFnSc3d1	ocelová
konstrukci	konstrukce	k1gFnSc3	konstrukce
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
výšky	výška	k1gFnSc2	výška
111	[number]	k4	111
m.	m.	k?	m.
Anténa	anténa	k1gFnSc1	anténa
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
116	[number]	k4	116
m.	m.	k?	m.
<g/>
Objekt	objekt	k1gInSc1	objekt
byl	být	k5eAaImAgInS	být
projektován	projektovat	k5eAaBmNgInS	projektovat
se	s	k7c7	s
zřetelem	zřetel	k1gInSc7	zřetel
na	na	k7c4	na
ohleduplnost	ohleduplnost	k1gFnSc4	ohleduplnost
k	k	k7c3	k
životnímu	životní	k2eAgNnSc3d1	životní
prostředí	prostředí	k1gNnSc3	prostředí
(	(	kIx(	(
<g/>
využití	využití	k1gNnSc3	využití
geotermálního	geotermální	k2eAgNnSc2d1	geotermální
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
akumulace	akumulace	k1gFnSc1	akumulace
hlubokými	hluboký	k2eAgInPc7d1	hluboký
pilotami	pilota	k1gFnPc7	pilota
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
železobetonové	železobetonový	k2eAgFnSc6d1	železobetonová
věži	věž	k1gFnSc6	věž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kanceláře	kancelář	k1gFnSc2	kancelář
a	a	k8xC	a
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
sedmi	sedm	k4xCc6	sedm
podlažích	podlaží	k1gNnPc6	podlaží
apartmány	apartmán	k1gInPc1	apartmán
<g/>
.	.	kIx.	.
</s>
<s>
Včetně	včetně	k7c2	včetně
třípodlažní	třípodlažní	k2eAgFnSc2d1	třípodlažní
podnože	podnož	k1gFnSc2	podnož
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
celkově	celkově	k6eAd1	celkově
17	[number]	k4	17
000	[number]	k4	000
metrů	metr	k1gInPc2	metr
čtverečních	čtvereční	k2eAgFnPc2d1	čtvereční
obchodních	obchodní	k2eAgFnPc2d1	obchodní
<g/>
,	,	kIx,	,
kancelářských	kancelářský	k2eAgFnPc2d1	kancelářská
a	a	k8xC	a
rezidenčních	rezidenční	k2eAgFnPc2d1	rezidenční
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
záměru	záměr	k1gInSc2	záměr
stavba	stavba	k1gFnSc1	stavba
budovy	budova	k1gFnSc2	budova
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
25	[number]	k4	25
měsíců	měsíc	k1gInPc2	měsíc
od	od	k7c2	od
zahájení	zahájení	k1gNnSc2	zahájení
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámen	k2eAgNnSc1d1	oznámeno
dokončení	dokončení	k1gNnSc1	dokončení
samotné	samotný	k2eAgFnSc2d1	samotná
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
s	s	k7c7	s
předpokladem	předpoklad	k1gInSc7	předpoklad
pokračování	pokračování	k1gNnSc2	pokračování
dokončovacích	dokončovací	k2eAgFnPc2d1	dokončovací
prací	práce	k1gFnPc2	práce
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
<g/>
,	,	kIx,	,
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
a	a	k8xC	a
květnu	květen	k1gInSc3	květen
následovala	následovat	k5eAaImAgFnS	následovat
kolaudace	kolaudace	k1gFnSc1	kolaudace
<g/>
,	,	kIx,	,
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
den	den	k1gInSc1	den
otevřených	otevřený	k2eAgFnPc2d1	otevřená
dveří	dveře	k1gFnPc2	dveře
<g/>
.	.	kIx.	.
</s>
<s>
AZ	AZ	kA	AZ
Tower	Tower	k1gInSc1	Tower
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřít	k5eAaPmNgInS	otevřít
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
výškou	výška	k1gFnSc7	výška
překonal	překonat	k5eAaPmAgMnS	překonat
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
budovu	budova	k1gFnSc4	budova
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
Spilberk	Spilberk	k1gInSc1	Spilberk
Tower	Tower	k1gInSc1	Tower
B.	B.	kA	B.
</s>
</p>
<p>
<s>
==	==	k?	==
Hodnocení	hodnocení	k1gNnSc1	hodnocení
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
Staveb	stavba	k1gFnPc2	stavba
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
stavba	stavba	k1gFnSc1	stavba
občanské	občanský	k2eAgFnSc2d1	občanská
vybavenosti	vybavenost	k1gFnSc2	vybavenost
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2014	[number]	k4	2014
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
soutěži	soutěž	k1gFnSc6	soutěž
Emporis	Emporis	k1gFnPc2	Emporis
Skyscraper	Skyscraper	k1gMnSc1	Skyscraper
Award	Award	k1gMnSc1	Award
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
vyhlašovaná	vyhlašovaný	k2eAgFnSc1d1	vyhlašovaná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
hamburskou	hamburský	k2eAgFnSc7d1	hamburská
společností	společnost	k1gFnSc7	společnost
Emporis	Emporis	k1gFnSc2	Emporis
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
funkčnost	funkčnost	k1gFnSc4	funkčnost
a	a	k8xC	a
vzhled	vzhled	k1gInSc4	vzhled
objektů	objekt	k1gInPc2	objekt
vyšších	vysoký	k2eAgInPc2d2	vyšší
než	než	k8xS	než
sto	sto	k4xCgNnSc1	sto
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
AZ	AZ	kA	AZ
Tower	Towero	k1gNnPc2	Towero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
