<s>
Pojezdová	pojezdový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Dvě	dva	k4xCgFnPc4
protínající	protínající	k2eAgFnPc4d1
se	se	k3xPyFc4
vzletové	vzletový	k2eAgFnSc2d1
a	a	k8xC
přistávací	přistávací	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
(	(	kIx(
<g/>
tmavě	tmavě	k6eAd1
šedá	šedý	k2eAgFnSc1d1
<g/>
)	)	kIx)
s	s	k7c7
přilehlými	přilehlý	k2eAgFnPc7d1
pojezdovými	pojezdový	k2eAgFnPc7d1
drahami	draha	k1gFnPc7
(	(	kIx(
<g/>
modrá	modrý	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Normální	normální	k2eAgFnSc1d1
pojezdová	pojezdový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
a	a	k8xC
pojezdová	pojezdový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
pro	pro	k7c4
rychlé	rychlý	k2eAgNnSc4d1
odbočení	odbočení	k1gNnSc4
</s>
<s>
Pojezdová	pojezdový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
taxiway	taxiwa	k2eAgFnPc1d1
<g/>
,	,	kIx,
zkracováno	zkracován	k2eAgNnSc1d1
TWY	TWY	kA
<g/>
)	)	kIx)
patří	patřit	k5eAaImIp3nS
podobně	podobně	k6eAd1
jako	jako	k9
vzletová	vzletový	k2eAgFnSc1d1
a	a	k8xC
přistávací	přistávací	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
mezi	mezi	k7c4
provozní	provozní	k2eAgFnPc4d1
plochy	plocha	k1gFnPc4
letiště	letiště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
vymezená	vymezený	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
na	na	k7c6
letišti	letiště	k1gNnSc6
<g/>
,	,	kIx,
sloužící	sloužící	k2eAgMnSc1d1
k	k	k7c3
pojíždění	pojíždění	k1gNnSc3
letadel	letadlo	k1gNnPc2
mezi	mezi	k7c7
odbavovací	odbavovací	k2eAgFnSc7d1
plochou	plocha	k1gFnSc7
a	a	k8xC
vzletovou	vzletový	k2eAgFnSc7d1
a	a	k8xC
přistávací	přistávací	k2eAgFnSc7d1
drahou	draha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letadla	letadlo	k1gNnPc1
se	se	k3xPyFc4
po	po	k7c6
ní	on	k3xPp3gFnSc6
pohybují	pohybovat	k5eAaImIp3nP
rychlostí	rychlost	k1gFnSc7
20	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
37	#num#	k4
<g/>
–	–	k?
<g/>
56	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vytížená	vytížený	k2eAgNnPc1d1
letiště	letiště	k1gNnPc1
vytvářejí	vytvářet	k5eAaImIp3nP
vysokorychlostní	vysokorychlostní	k2eAgFnPc1d1
pojezdové	pojezdový	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc5
umožňují	umožňovat	k5eAaImIp3nP
letadlům	letadlo	k1gNnPc3
rychleji	rychle	k6eAd2
uvolnit	uvolnit	k5eAaPmF
přistávací	přistávací	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
a	a	k8xC
umožnit	umožnit	k5eAaPmF
jinému	jiný	k2eAgNnSc3d1
letadlu	letadlo	k1gNnSc3
přistání	přistání	k1gNnSc2
nebo	nebo	k8xC
vzlet	vzlet	k1gInSc1
v	v	k7c6
kratším	krátký	k2eAgInSc6d2
časovém	časový	k2eAgInSc6d1
intervalu	interval	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgMnSc2
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
dosahuje	dosahovat	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
pojezdová	pojezdový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
sloužící	sloužící	k2eAgFnSc1d1
k	k	k7c3
opuštění	opuštění	k1gNnSc3
přistávací	přistávací	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
je	být	k5eAaImIp3nS
delší	dlouhý	k2eAgFnSc1d2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
je	být	k5eAaImIp3nS
letadlu	letadlo	k1gNnSc3
poskytnut	poskytnut	k2eAgInSc4d1
větší	veliký	k2eAgInSc4d2
prostor	prostor	k1gInSc4
<g/>
,	,	kIx,
než	než	k8xS
zpomalí	zpomalet	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Značení	značení	k1gNnSc1
a	a	k8xC
pojmenování	pojmenování	k1gNnSc1
pojezdových	pojezdový	k2eAgFnPc2d1
drah	draha	k1gFnPc2
</s>
<s>
Pojezdová	pojezdový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
v	v	k7c6
noci	noc	k1gFnSc6
</s>
<s>
Značení	značení	k1gNnSc2
a	a	k8xC
návěstidla	návěstidlo	k1gNnSc2
</s>
<s>
Pojezdové	pojezdový	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
jsou	být	k5eAaImIp3nP
značeny	značit	k5eAaImNgFnP
žlutou	žlutý	k2eAgFnSc7d1
barvou	barva	k1gFnSc7
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
vzletových	vzletový	k2eAgFnPc2d1
a	a	k8xC
přistávacích	přistávací	k2eAgFnPc2d1
drah	draha	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
značeny	značit	k5eAaImNgFnP
bíle	bíle	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
k	k	k7c3
vyznačení	vyznačení	k1gNnSc3
pojezdové	pojezdový	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
v	v	k7c6
noci	noc	k1gFnSc6
užita	užit	k2eAgNnPc1d1
světelná	světelný	k2eAgNnPc1d1
návěstidla	návěstidlo	k1gNnPc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použita	použit	k2eAgNnPc1d1
zelená	zelené	k1gNnPc1
(	(	kIx(
<g/>
středová	středový	k2eAgFnSc1d1
čára	čára	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
modrá	modrý	k2eAgFnSc1d1
(	(	kIx(
<g/>
okraje	okraj	k1gInPc1
pojezdové	pojezdový	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojezdové	pojezdový	k2eAgInPc1d1
pro	pro	k7c4
rychlé	rychlý	k2eAgNnSc4d1
odbočení	odbočení	k1gNnSc4
mají	mít	k5eAaImIp3nP
středovou	středový	k2eAgFnSc4d1
čáru	čára	k1gFnSc4
žlutou	žlutý	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Pojezdové	pojezdový	k2eAgFnPc4d1
dráhy	dráha	k1gFnPc4
od	od	k7c2
vzletových	vzletový	k2eAgMnPc2d1
oddělují	oddělovat	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
přerušované	přerušovaný	k2eAgFnPc1d1
čáry	čára	k1gFnPc1
následované	následovaný	k2eAgFnPc1d1
dvěma	dva	k4xCgFnPc7
plnými	plný	k2eAgFnPc7d1
čárami	čára	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Oddělení	oddělení	k1gNnSc1
drah	draha	k1gFnPc2
</s>
<s>
Pojmenování	pojmenování	k1gNnPc1
</s>
<s>
Pojezdové	pojezdový	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
jsou	být	k5eAaImIp3nP
většinou	většina	k1gFnSc7
pojmenovány	pojmenovat	k5eAaPmNgInP
písmeny	písmeno	k1gNnPc7
nebo	nebo	k8xC
číslicemi	číslice	k1gFnPc7
<g/>
,	,	kIx,
při	při	k7c6
jejich	jejich	k3xOp3gNnSc6
označování	označování	k1gNnSc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
hláskovací	hláskovací	k2eAgFnSc1d1
abeceda	abeceda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
některých	některý	k3yIgNnPc6
letištích	letiště	k1gNnPc6
se	se	k3xPyFc4
pak	pak	k6eAd1
používá	používat	k5eAaImIp3nS
speciálních	speciální	k2eAgInPc2d1
názvů	název	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
na	na	k7c6
letišti	letiště	k1gNnSc6
Šeremetěvo	Šeremetěvo	k1gNnSc4
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
pojezdových	pojezdový	k2eAgFnPc2d1
označena	označit	k5eAaPmNgFnS
jako	jako	k8xS,k8xC
hlavní	hlavní	k2eAgFnSc1d1
pojezdová	pojezdový	k2eAgFnSc1d1
Magistralnaja	Magistralnaja	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Main	Main	k1gNnSc1
taxiway	taxiwaa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
na	na	k7c6
letišti	letiště	k1gNnSc6
London	London	k1gMnSc1
Heathrow	Heathrow	k1gMnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
dvojici	dvojice	k1gFnSc4
paralelních	paralelní	k2eAgInPc2d1
pojezdových	pojezdový	k2eAgInPc2d1
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
vedou	vést	k5eAaImIp3nP
kolem	kolem	k7c2
dokola	dokola	k6eAd1
celého	celý	k2eAgInSc2d1
letištního	letištní	k2eAgInSc2d1
terminálu	terminál	k1gInSc2
označení	označení	k1gNnSc2
Outer	Outra	k1gFnPc2
a	a	k8xC
Inner	Innra	k1gFnPc2
<g/>
,	,	kIx,
tedy	tedy	k9
vnější	vnější	k2eAgFnSc4d1
a	a	k8xC
vnitřní	vnitřní	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Znaky	znak	k1gInPc1
a	a	k8xC
značky	značka	k1gFnPc1
</s>
<s>
Na	na	k7c6
pojezdových	pojezdový	k2eAgFnPc6d1
drahách	draha	k1gFnPc6
se	se	k3xPyFc4
rozlišují	rozlišovat	k5eAaImIp3nP
tři	tři	k4xCgInPc1
typy	typ	k1gInPc1
značek	značka	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Příkazové	příkazový	k2eAgInPc1d1
znaky	znak	k1gInPc1
–	–	k?
bílé	bílý	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
na	na	k7c6
červeném	červený	k2eAgInSc6d1
podkladě	podklad	k1gInSc6
–	–	k?
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
označení	označení	k1gNnSc3
místa	místo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
nesmí	smět	k5eNaImIp3nS
letadlo	letadlo	k1gNnSc4
přejet	přejet	k5eAaPmF
pokud	pokud	k8xS
nedostane	dostat	k5eNaPmIp3nS
příkaz	příkaz	k1gInSc4
od	od	k7c2
řídícího	řídící	k2eAgInSc2d1
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
</s>
<s>
Informační	informační	k2eAgInPc1d1
znaky	znak	k1gInPc1
místa	místo	k1gNnSc2
–	–	k?
žluté	žlutý	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
na	na	k7c6
černém	černý	k2eAgInSc6d1
podkladu	podklad	k1gInSc6
–	–	k?
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
označení	označení	k1gNnSc3
místa	místo	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
se	se	k3xPyFc4
letadlo	letadlo	k1gNnSc1
nachází	nacházet	k5eAaImIp3nS
</s>
<s>
Informační	informační	k2eAgInPc4d1
znaky	znak	k1gInPc4
směrové	směrový	k2eAgNnSc4d1
–	–	k?
černé	černý	k2eAgNnSc1d1
písmo	písmo	k1gNnSc4
na	na	k7c6
žlutém	žlutý	k2eAgInSc6d1
podkladu	podklad	k1gInSc6
–	–	k?
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
označení	označení	k1gNnSc3
směru	směr	k1gInSc2
a	a	k8xC
pojmenování	pojmenování	k1gNnSc4
dalších	další	k2eAgFnPc2d1
pojezdových	pojezdový	k2eAgFnPc2d1
drah	draha	k1gFnPc2
na	na	k7c6
křižovatkách	křižovatka	k1gFnPc6
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Letiště	letiště	k1gNnSc1
</s>
<s>
Heliport	heliport	k1gInSc1
</s>
<s>
Vzletová	vzletový	k2eAgFnSc1d1
a	a	k8xC
přistávací	přistávací	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Pojezdová	pojezdový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
