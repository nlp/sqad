<s>
Pojezdová	pojezdový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
taxiway	taxiwa	k2eAgFnPc1d1
<g/>
,	,	kIx,
zkracováno	zkracován	k2eAgNnSc1d1
TWY	TWY	kA
<g/>
)	)	kIx)
patří	patřit	k5eAaImIp3nS
podobně	podobně	k6eAd1
jako	jako	k9
vzletová	vzletový	k2eAgFnSc1d1
a	a	k8xC
přistávací	přistávací	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
mezi	mezi	k7c4
provozní	provozní	k2eAgFnPc4d1
plochy	plocha	k1gFnPc4
letiště	letiště	k1gNnSc2
<g/>
.	.	kIx.
</s>