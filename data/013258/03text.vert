<p>
<s>
Mr	Mr	k?	Mr
<g/>
.	.	kIx.	.
Bean	Bean	k1gInSc1	Bean
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
britský	britský	k2eAgInSc1d1	britský
sitcom	sitcom	k1gInSc1	sitcom
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
proslavil	proslavit	k5eAaPmAgMnS	proslavit
komik	komik	k1gMnSc1	komik
Rowan	Rowan	k1gMnSc1	Rowan
Atkinson	Atkinson	k1gMnSc1	Atkinson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Seriál	seriál	k1gInSc1	seriál
Mr	Mr	k1gFnSc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gInSc1	Bean
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1989	[number]	k4	1989
až	až	k6eAd1	až
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
se	se	k3xPyFc4	se
moc	moc	k6eAd1	moc
nemluví	mluvit	k5eNaImIp3nS	mluvit
<g/>
,	,	kIx,	,
komické	komický	k2eAgFnPc1d1	komická
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
spíše	spíše	k9	spíše
situace	situace	k1gFnSc1	situace
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gMnSc1	Bean
je	být	k5eAaImIp3nS	být
smolař	smolař	k1gMnSc1	smolař
<g/>
,	,	kIx,	,
chovající	chovající	k2eAgMnSc1d1	chovající
se	se	k3xPyFc4	se
extrémně	extrémně	k6eAd1	extrémně
přitrouble	přitrouble	k6eAd1	přitrouble
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
nic	nic	k3yNnSc1	nic
nevyjde	vyjít	k5eNaPmIp3nS	vyjít
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
naplánuje	naplánovat	k5eAaBmIp3nS	naplánovat
a	a	k8xC	a
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
představuje	představovat	k5eAaImIp3nS	představovat
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgNnSc1d1	známé
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jezdí	jezdit	k5eAaImIp3nS	jezdit
žlutozeleným	žlutozelený	k2eAgFnPc3d1	žlutozelená
Mini	mini	k2eAgFnPc3d1	mini
(	(	kIx(	(
<g/>
a	a	k8xC	a
bytostně	bytostně	k6eAd1	bytostně
nesnáší	snášet	k5eNaImIp3nP	snášet
tříkolky	tříkolka	k1gFnSc2	tříkolka
Reliant	Relianta	k1gFnPc2	Relianta
Regal	Regal	k1gInSc1	Regal
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
plyšového	plyšový	k2eAgMnSc4d1	plyšový
medvídka	medvídek	k1gMnSc4	medvídek
jménem	jméno	k1gNnSc7	jméno
Teddy	Tedda	k1gFnSc2	Tedda
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
schází	scházet	k5eAaImIp3nS	scházet
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
milou	milá	k1gFnSc7	milá
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vždy	vždy	k6eAd1	vždy
zklame	zklamat	k5eAaPmIp3nS	zklamat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Seriál	seriál	k1gInSc1	seriál
měl	mít	k5eAaImAgInS	mít
úspěch	úspěch	k1gInSc4	úspěch
už	už	k6eAd1	už
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
promítání	promítání	k1gNnSc6	promítání
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
natočeny	natočit	k5eAaBmNgInP	natočit
i	i	k9	i
dva	dva	k4xCgInPc1	dva
celovečerní	celovečerní	k2eAgInPc1d1	celovečerní
filmy	film	k1gInPc1	film
(	(	kIx(	(
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gNnSc1	Bean
<g/>
:	:	kIx,	:
Největší	veliký	k2eAgFnSc1d3	veliký
filmová	filmový	k2eAgFnSc1d1	filmová
katastrofa	katastrofa	k1gFnSc1	katastrofa
a	a	k8xC	a
Prázdniny	prázdniny	k1gFnPc1	prázdniny
pana	pan	k1gMnSc2	pan
Beana	Bean	k1gMnSc2	Bean
<g/>
)	)	kIx)	)
a	a	k8xC	a
animovaný	animovaný	k2eAgInSc1d1	animovaný
seriál	seriál	k1gInSc1	seriál
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gNnSc1	Bean
<g/>
:	:	kIx,	:
Animované	animovaný	k2eAgInPc1d1	animovaný
příběhy	příběh	k1gInPc1	příběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	s	k7c7	s
Mr	Mr	k1gFnSc7	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gMnSc1	Bean
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
několika	několik	k4yIc6	několik
speciálech	speciál	k1gInPc6	speciál
a	a	k8xC	a
samostatných	samostatný	k2eAgFnPc6d1	samostatná
scénkách	scénka	k1gFnPc6	scénka
<g/>
,	,	kIx,	,
v	v	k7c6	v
televizních	televizní	k2eAgInPc6d1	televizní
rozhovorech	rozhovor	k1gInPc6	rozhovor
<g/>
,	,	kIx,	,
reklamách	reklama	k1gFnPc6	reklama
a	a	k8xC	a
videoklipech	videoklip	k1gInPc6	videoklip
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Postavy	postava	k1gFnPc1	postava
a	a	k8xC	a
opakující	opakující	k2eAgFnPc1d1	opakující
se	se	k3xPyFc4	se
rekvizity	rekvizita	k1gFnSc2	rekvizita
==	==	k?	==
</s>
</p>
<p>
<s>
Mr	Mr	k?	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gNnSc1	Bean
–	–	k?	–
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
seriálu	seriál	k1gInSc2	seriál
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
ho	on	k3xPp3gMnSc4	on
Rowan	Rowan	k1gMnSc1	Rowan
Atkinson	Atkinson	k1gMnSc1	Atkinson
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dětinský	dětinský	k2eAgMnSc1d1	dětinský
kašpar	kašpar	k?	kašpar
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vymýšlí	vymýšlet	k5eAaImIp3nS	vymýšlet
různá	různý	k2eAgNnPc4d1	různé
neobvyklá	obvyklý	k2eNgNnPc4d1	neobvyklé
řešení	řešení	k1gNnPc4	řešení
pro	pro	k7c4	pro
každodenní	každodenní	k2eAgInPc4d1	každodenní
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
oblečení	oblečení	k1gNnSc1	oblečení
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
obvykle	obvykle	k6eAd1	obvykle
z	z	k7c2	z
tvídového	tvídový	k2eAgNnSc2d1	tvídové
saka	sako	k1gNnSc2	sako
a	a	k8xC	a
tenké	tenký	k2eAgFnSc2d1	tenká
červené	červený	k2eAgFnSc2d1	červená
kravaty	kravata	k1gFnSc2	kravata
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Bean	Bean	k1gMnSc1	Bean
málokdy	málokdy	k6eAd1	málokdy
promluví	promluvit	k5eAaPmIp3nS	promluvit
a	a	k8xC	a
pokud	pokud	k8xS	pokud
ano	ano	k9	ano
<g/>
,	,	kIx,	,
zamumlá	zamumlat	k5eAaPmIp3nS	zamumlat
pouze	pouze	k6eAd1	pouze
pár	pár	k4xCyI	pár
slov	slovo	k1gNnPc2	slovo
komicky	komicky	k6eAd1	komicky
nízkým	nízký	k2eAgInSc7d1	nízký
hlasem	hlas	k1gInSc7	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
je	být	k5eAaImIp3nS	být
neznámé	známý	k2eNgNnSc1d1	neznámé
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
Bean	Bean	k1gMnSc1	Bean
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jeho	jeho	k3xOp3gNnSc4	jeho
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
neznáme	znát	k5eNaImIp1nP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
odhalila	odhalit	k5eAaPmAgFnS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
hlídač	hlídač	k1gInSc1	hlídač
v	v	k7c6	v
londýnské	londýnský	k2eAgFnSc6d1	londýnská
Národní	národní	k2eAgFnSc6d1	národní
galerii	galerie	k1gFnSc6	galerie
a	a	k8xC	a
v	v	k7c6	v
pasu	pas	k1gInSc6	pas
má	mít	k5eAaImIp3nS	mít
jako	jako	k9	jako
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
vyplněno	vyplněn	k2eAgNnSc4d1	vyplněno
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc4	Mr
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pan	pan	k1gMnSc1	pan
Bean	Bean	k1gMnSc1	Bean
neví	vědět	k5eNaImIp3nS	vědět
jak	jak	k6eAd1	jak
okolní	okolní	k2eAgInSc4d1	okolní
svět	svět	k1gInSc4	svět
funguje	fungovat	k5eAaImIp3nS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
vyřešit	vyřešit	k5eAaPmF	vyřešit
každodenní	každodenní	k2eAgFnSc2d1	každodenní
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
činnosti	činnost	k1gFnSc2	činnost
neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
složitým	složitý	k2eAgInSc7d1	složitý
způsobem	způsob	k1gInSc7	způsob
(	(	kIx(	(
<g/>
návštěva	návštěva	k1gFnSc1	návštěva
bazénu	bazén	k1gInSc2	bazén
<g/>
,	,	kIx,	,
zapojení	zapojení	k1gNnSc4	zapojení
televizoru	televizor	k1gInSc2	televizor
<g/>
,	,	kIx,	,
výzdoba	výzdoba	k1gFnSc1	výzdoba
bytu	byt	k1gInSc2	byt
<g/>
,	,	kIx,	,
návštěva	návštěva	k1gFnSc1	návštěva
kostela	kostel	k1gInSc2	kostel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Humorné	humorný	k2eAgFnPc1d1	humorná
situace	situace	k1gFnPc1	situace
nastávají	nastávat	k5eAaImIp3nP	nastávat
právě	právě	k9	právě
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
originálním	originální	k2eAgInSc6d1	originální
přístupu	přístup	k1gInSc6	přístup
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgNnSc6	který
nehledí	hledět	k5eNaImIp3nS	hledět
na	na	k7c4	na
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
také	také	k9	také
malicherný	malicherný	k2eAgMnSc1d1	malicherný
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
až	až	k9	až
zlomyslný	zlomyslný	k2eAgMnSc1d1	zlomyslný
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
humor	humor	k1gInSc1	humor
plyne	plynout	k5eAaImIp3nS	plynout
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
smolař	smolař	k1gMnSc1	smolař
a	a	k8xC	a
nešika	nešika	k1gMnSc1	nešika
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
vynalézavý	vynalézavý	k2eAgMnSc1d1	vynalézavý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Irma	Irma	k1gFnSc1	Irma
Gobbová	Gobbová	k1gFnSc1	Gobbová
–	–	k?	–
Přítelkyně	přítelkyně	k1gFnSc2	přítelkyně
pana	pan	k1gMnSc2	pan
Beana	Bean	k1gMnSc2	Bean
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
dílech	dílo	k1gNnPc6	dílo
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
ji	on	k3xPp3gFnSc4	on
Matilda	Matilda	k1gFnSc1	Matilda
Zieglerová	Zieglerová	k1gFnSc1	Zieglerová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Bean	Bean	k1gMnSc1	Bean
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
nechová	chovat	k5eNaImIp3nS	chovat
příliš	příliš	k6eAd1	příliš
ohleduplně	ohleduplně	k6eAd1	ohleduplně
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
spíš	spíš	k9	spíš
kamarádka	kamarádka	k1gFnSc1	kamarádka
než	než	k8xS	než
milostná	milostný	k2eAgFnSc1d1	milostná
záležitost	záležitost	k1gFnSc1	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Bean	Bean	k1gMnSc1	Bean
žárlí	žárlit	k5eAaImIp3nS	žárlit
<g/>
,	,	kIx,	,
když	když	k8xS	když
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
tancuje	tancovat	k5eAaImIp3nS	tancovat
jiný	jiný	k2eAgMnSc1d1	jiný
muž	muž	k1gMnSc1	muž
na	na	k7c6	na
diskotéce	diskotéka	k1gFnSc6	diskotéka
<g/>
.	.	kIx.	.
</s>
<s>
Irma	Irma	k1gFnSc1	Irma
zjevně	zjevně	k6eAd1	zjevně
očekává	očekávat	k5eAaImIp3nS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
Bean	Bean	k1gMnSc1	Bean
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
požádá	požádat	k5eAaPmIp3nS	požádat
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Bean	Bean	k1gMnSc1	Bean
nepochopí	pochopit	k5eNaPmIp3nS	pochopit
náznak	náznak	k1gInSc4	náznak
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
všelijak	všelijak	k6eAd1	všelijak
oslavu	oslava	k1gFnSc4	oslava
Vánoc	Vánoce	k1gFnPc2	Vánoce
pokazí	pokazit	k5eAaPmIp3nS	pokazit
<g/>
,	,	kIx,	,
opouští	opouštět	k5eAaImIp3nS	opouštět
Irma	Irma	k1gFnSc1	Irma
Beana	Bean	k1gInSc2	Bean
nadobro	nadobro	k6eAd1	nadobro
<g/>
.	.	kIx.	.
</s>
<s>
Stejná	stejný	k2eAgFnSc1d1	stejná
herečka	herečka	k1gFnSc1	herečka
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
nesouvisející	související	k2eNgFnPc4d1	nesouvisející
role	role	k1gFnPc4	role
(	(	kIx(	(
<g/>
servírka	servírka	k1gFnSc1	servírka
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
policistka	policistka	k1gFnSc1	policistka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teddy	Teddy	k6eAd1	Teddy
–	–	k?	–
Beanův	Beanův	k2eAgMnSc1d1	Beanův
medvídek	medvídek	k1gMnSc1	medvídek
a	a	k8xC	a
zjevně	zjevně	k6eAd1	zjevně
i	i	k9	i
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
přítel	přítel	k1gMnSc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Teddy	Teddy	k6eAd1	Teddy
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgMnSc1d1	malý
hnědý	hnědý	k2eAgMnSc1d1	hnědý
pletený	pletený	k2eAgMnSc1d1	pletený
medvídek	medvídek	k1gMnSc1	medvídek
<g/>
,	,	kIx,	,
pan	pan	k1gMnSc1	pan
Bean	Bean	k1gMnSc1	Bean
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
ale	ale	k8xC	ale
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
živý	živý	k2eAgMnSc1d1	živý
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
uspává	uspávat	k5eAaImIp3nS	uspávat
pomocí	pomocí	k7c2	pomocí
hypnózy	hypnóza	k1gFnSc2	hypnóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gInSc1	Bean
také	také	k9	také
kupuje	kupovat	k5eAaImIp3nS	kupovat
medvídkovi	medvídkův	k2eAgMnPc1d1	medvídkův
dárky	dárek	k1gInPc4	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Umí	umět	k5eAaImIp3nS	umět
se	se	k3xPyFc4	se
k	k	k7c3	k
medvídkovi	medvídek	k1gMnSc3	medvídek
ale	ale	k8xC	ale
chovat	chovat	k5eAaImF	chovat
i	i	k9	i
ošklivě	ošklivě	k6eAd1	ošklivě
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
když	když	k8xS	když
mu	on	k3xPp3gNnSc3	on
nešťastně	šťastně	k6eNd1	šťastně
usekne	useknout	k5eAaPmIp3nS	useknout
hlavu	hlava	k1gFnSc4	hlava
nebo	nebo	k8xC	nebo
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
použije	použít	k5eAaPmIp3nS	použít
jako	jako	k9	jako
náhradní	náhradní	k2eAgInSc4d1	náhradní
štětec	štětec	k1gInSc4	štětec
k	k	k7c3	k
vymalování	vymalování	k1gNnSc3	vymalování
pokoje	pokoj	k1gInSc2	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Bean	Bean	k1gMnSc1	Bean
zřejmě	zřejmě	k6eAd1	zřejmě
považuje	považovat	k5eAaImIp3nS	považovat
Teddyho	Teddy	k1gMnSc4	Teddy
i	i	k9	i
jako	jako	k9	jako
domácího	domácí	k2eAgMnSc2d1	domácí
mazlíčka	mazlíček	k1gMnSc2	mazlíček
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
závodí	závodit	k5eAaImIp3nP	závodit
na	na	k7c6	na
dětské	dětský	k2eAgFnSc6d1	dětská
psí	psí	k2eAgFnSc6d1	psí
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
seriálu	seriál	k1gInSc2	seriál
se	se	k3xPyFc4	se
Teddyho	Teddyha	k1gFnSc5	Teddyha
podoba	podoba	k1gFnSc1	podoba
několikrát	několikrát	k6eAd1	několikrát
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mini	mini	k2eAgInSc1d1	mini
–	–	k?	–
Beanovým	Beanův	k2eAgNnSc7d1	Beanův
vozidlem	vozidlo	k1gNnSc7	vozidlo
je	být	k5eAaImIp3nS	být
žlutozelené	žlutozelený	k2eAgNnSc1d1	žlutozelené
British	British	k1gInSc4	British
Leyland	Leyland	k1gInSc1	Leyland
Mini	mini	k2eAgInSc1d1	mini
1000	[number]	k4	1000
Mark	Mark	k1gMnSc1	Mark
4	[number]	k4	4
ročník	ročník	k1gInSc1	ročník
1977	[number]	k4	1977
s	s	k7c7	s
černou	černý	k2eAgFnSc7d1	černá
kapotou	kapota	k1gFnSc7	kapota
a	a	k8xC	a
espézetkou	espézetka	k1gFnSc7	espézetka
SLW	SLW	kA	SLW
287	[number]	k4	287
<g/>
R.	R.	kA	R.
Centrem	centr	k1gInSc7	centr
děje	děj	k1gInSc2	děj
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
například	například	k6eAd1	například
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
Bean	Bean	k1gMnSc1	Bean
převlékal	převlékat	k5eAaImAgMnS	převlékat
během	během	k7c2	během
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
řídil	řídit	k5eAaImAgMnS	řídit
z	z	k7c2	z
křesla	křeslo	k1gNnSc2	křeslo
přivázaného	přivázaný	k2eAgNnSc2d1	přivázané
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gInSc4	Bean
si	se	k3xPyFc3	se
auto	auto	k1gNnSc1	auto
zamyká	zamykat	k5eAaImIp3nS	zamykat
na	na	k7c4	na
visací	visací	k2eAgInSc4d1	visací
zámek	zámek	k1gInSc4	zámek
s	s	k7c7	s
petlicí	petlice	k1gFnSc7	petlice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zaparkování	zaparkování	k1gNnSc6	zaparkování
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
taky	taky	k6eAd1	taky
někdy	někdy	k6eAd1	někdy
bere	brát	k5eAaImIp3nS	brát
volant	volant	k1gInSc1	volant
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
jako	jako	k9	jako
bezpečnostní	bezpečnostní	k2eAgNnPc1d1	bezpečnostní
opatření	opatření	k1gNnPc1	opatření
proti	proti	k7c3	proti
zlodějům	zloděj	k1gMnPc3	zloděj
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
epizod	epizoda	k1gFnPc2	epizoda
osvědčilo	osvědčit	k5eAaPmAgNnS	osvědčit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
dílu	díl	k1gInSc6	díl
rozdrtí	rozdrtit	k5eAaPmIp3nS	rozdrtit
Beanovo	Beanův	k2eAgNnSc1d1	Beanův
auto	auto	k1gNnSc1	auto
tank	tank	k1gInSc4	tank
<g/>
,	,	kIx,	,
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
dílech	dílo	k1gNnPc6	dílo
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
opět	opět	k6eAd1	opět
stejné	stejný	k2eAgNnSc4d1	stejné
auto	auto	k1gNnSc4	auto
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
SPZ	SPZ	kA	SPZ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reliant	Reliant	k1gInSc1	Reliant
–	–	k?	–
Už	už	k6eAd1	už
od	od	k7c2	od
prvního	první	k4xOgInSc2	první
dílu	díl	k1gInSc2	díl
se	se	k3xPyFc4	se
pan	pan	k1gMnSc1	pan
Bean	Bean	k1gMnSc1	Bean
přetahuje	přetahovat	k5eAaImIp3nS	přetahovat
s	s	k7c7	s
řidičem	řidič	k1gMnSc7	řidič
světlemodrého	světlemodrý	k2eAgInSc2d1	světlemodrý
automobilu	automobil	k1gInSc2	automobil
Reliant	Reliant	k1gMnSc1	Reliant
Regal	Regal	k1gMnSc1	Regal
Supervan	Supervan	k1gMnSc1	Supervan
III	III	kA	III
ročník	ročník	k1gInSc1	ročník
1972	[number]	k4	1972
(	(	kIx(	(
<g/>
státní	státní	k2eAgFnSc2d1	státní
poznávací	poznávací	k2eAgFnSc2d1	poznávací
značky	značka	k1gFnSc2	značka
GRA	GRA	kA	GRA
26	[number]	k4	26
<g/>
K	K	kA	K
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řidiče	řidič	k1gInSc2	řidič
vozu	vůz	k1gInSc2	vůz
nikdy	nikdy	k6eAd1	nikdy
nevidíme	vidět	k5eNaImIp1nP	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Bean	Bean	k1gMnSc1	Bean
svým	svůj	k3xOyFgNnSc7	svůj
Mini	mini	k2eAgInPc1d1	mini
buď	buď	k8xC	buď
převrhne	převrhnout	k5eAaPmIp3nS	převrhnout
protivníkovo	protivníkův	k2eAgNnSc4d1	protivníkovo
auto	auto	k1gNnSc4	auto
na	na	k7c4	na
bok	bok	k1gInSc4	bok
nebo	nebo	k8xC	nebo
ho	on	k3xPp3gMnSc4	on
vystrčí	vystrčit	k5eAaPmIp3nS	vystrčit
z	z	k7c2	z
parkovacího	parkovací	k2eAgNnSc2d1	parkovací
místa	místo	k1gNnSc2	místo
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
nehledí	hledět	k5eNaImIp3nP	hledět
na	na	k7c4	na
následky	následek	k1gInPc4	následek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dílu	díl	k1gInSc6	díl
Odpal	odpal	k1gInSc1	odpal
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
Beane	Bean	k1gMnSc5	Bean
pan	pan	k1gMnSc1	pan
Bean	Bean	k1gNnSc4	Bean
stopuje	stopovat	k5eAaImIp3nS	stopovat
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
a	a	k8xC	a
Reliant	Reliant	k1gMnSc1	Reliant
mu	on	k3xPp3gMnSc3	on
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
Bean	Bean	k1gInSc1	Bean
dělá	dělat	k5eAaImIp3nS	dělat
jakože	jakože	k8xS	jakože
nic	nic	k3yNnSc1	nic
a	a	k8xC	a
stopuje	stopovat	k5eAaImIp3nS	stopovat
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úvodní	úvodní	k2eAgFnSc1d1	úvodní
a	a	k8xC	a
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
znělka	znělka	k1gFnSc1	znělka
==	==	k?	==
</s>
</p>
<p>
<s>
Úvodní	úvodní	k2eAgFnSc1d1	úvodní
znělka	znělka	k1gFnSc1	znělka
seriálu	seriál	k1gInSc2	seriál
začíná	začínat	k5eAaImIp3nS	začínat
kuželem	kužel	k1gInSc7	kužel
světla	světlo	k1gNnSc2	světlo
dopadajícím	dopadající	k2eAgInSc7d1	dopadající
z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
na	na	k7c4	na
chodník	chodník	k1gInSc4	chodník
před	před	k7c7	před
londýnskou	londýnský	k2eAgFnSc7d1	londýnská
Katedrálou	katedrála	k1gFnSc7	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
pak	pak	k6eAd1	pak
spadne	spadnout	k5eAaPmIp3nS	spadnout
pan	pan	k1gMnSc1	pan
Bean	Bean	k1gMnSc1	Bean
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
oklepe	oklepat	k5eAaPmIp3nS	oklepat
a	a	k8xC	a
odchází	odcházet	k5eAaImIp3nS	odcházet
ze	z	k7c2	z
záběru	záběr	k1gInSc2	záběr
<g/>
.	.	kIx.	.
</s>
<s>
Scéna	scéna	k1gFnSc1	scéna
je	být	k5eAaImIp3nS	být
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
zpěvem	zpěv	k1gInSc7	zpěv
dětského	dětský	k2eAgInSc2d1	dětský
církevního	církevní	k2eAgInSc2d1	církevní
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Latinský	latinský	k2eAgInSc1d1	latinský
text	text	k1gInSc1	text
skladby	skladba	k1gFnPc1	skladba
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
hudbu	hudba	k1gFnSc4	hudba
napsal	napsat	k5eAaBmAgMnS	napsat
Howard	Howard	k1gMnSc1	Howard
Goodall	Goodall	k1gMnSc1	Goodall
<g/>
,	,	kIx,	,
zní	znět	k5eAaImIp3nS	znět
Ecce	Ecce	k1gFnSc4	Ecce
homo	homo	k6eAd1	homo
qui	qui	k?	qui
est	est	k?	est
faba	fab	k1gInSc2	fab
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Pohleďte	pohledět	k5eAaPmRp2nP	pohledět
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
fazole	fazole	k1gFnPc4	fazole
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
znělka	znělka	k1gFnSc1	znělka
objevovala	objevovat	k5eAaImAgFnS	objevovat
až	až	k6eAd1	až
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
dílu	díl	k1gInSc2	díl
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
a	a	k8xC	a
třetím	třetí	k4xOgInSc6	třetí
díle	díl	k1gInSc6	díl
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
černobílými	černobílý	k2eAgInPc7d1	černobílý
záběry	záběr	k1gInPc7	záběr
Beana	Beano	k1gNnSc2	Beano
padajícího	padající	k2eAgNnSc2d1	padající
z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
remasterovaná	remasterovaný	k2eAgFnSc1d1	remasterovaná
edice	edice	k1gFnSc1	edice
seriálu	seriál	k1gInSc2	seriál
úvodní	úvodní	k2eAgFnSc3d1	úvodní
sekvenci	sekvence	k1gFnSc3	sekvence
sjednocuje	sjednocovat	k5eAaImIp3nS	sjednocovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
znělka	znělka	k1gFnSc1	znělka
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
Beana	Beana	k1gFnSc1	Beana
vcucne	vcucnout	k5eAaPmIp3nS	vcucnout
kužel	kužel	k1gInSc4	kužel
světla	světlo	k1gNnSc2	světlo
opět	opět	k6eAd1	opět
na	na	k7c4	na
nebe	nebe	k1gNnSc4	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
ale	ale	k8xC	ale
zní	znět	k5eAaImIp3nS	znět
závěčná	závěčný	k2eAgFnSc1d1	závěčný
znělka	znělka	k1gFnSc1	znělka
přes	přes	k7c4	přes
závěrečné	závěrečný	k2eAgInPc4d1	závěrečný
titulky	titulek	k1gInPc4	titulek
<g/>
.	.	kIx.	.
</s>
<s>
Melodie	melodie	k1gFnSc1	melodie
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k9	jako
u	u	k7c2	u
úvodní	úvodní	k2eAgFnSc2d1	úvodní
znělky	znělka	k1gFnSc2	znělka
<g/>
,	,	kIx,	,
latinský	latinský	k2eAgInSc1d1	latinský
text	text	k1gInSc1	text
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
–	–	k?	–
Vale	val	k1gInSc6	val
homo	homo	k1gMnSc1	homo
qui	qui	k?	qui
est	est	k?	est
faba	fab	k1gInSc2	fab
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Sbohem	sbohem	k0	sbohem
člověku	člověk	k1gMnSc6	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
fazole	fazole	k1gFnPc4	fazole
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
dílů	díl	k1gInPc2	díl
==	==	k?	==
</s>
</p>
<p>
<s>
Seriál	seriál	k1gInSc1	seriál
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
uváděn	uvádět	k5eAaImNgInS	uvádět
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
znění	znění	k1gNnSc6	znění
s	s	k7c7	s
titulky	titulek	k1gInPc7	titulek
<g/>
,	,	kIx,	,
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
moc	moc	k6eAd1	moc
nemluví	mluvit	k5eNaImIp3nS	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
různými	různý	k2eAgNnPc7d1	různé
uvedeními	uvedení	k1gNnPc7	uvedení
existují	existovat	k5eAaImIp3nP	existovat
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
dialogů	dialog	k1gInPc2	dialog
i	i	k9	i
v	v	k7c6	v
názvech	název	k1gInPc6	název
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc4d1	poslední
15	[number]	k4	15
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
uveden	uvést	k5eAaPmNgInS	uvést
nebyl	být	k5eNaImAgInS	být
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
střihový	střihový	k2eAgInSc4d1	střihový
pořad	pořad	k1gInSc4	pořad
s	s	k7c7	s
dotočenými	dotočený	k2eAgFnPc7d1	dotočená
propojovacími	propojovací	k2eAgFnPc7d1	propojovací
scénami	scéna	k1gFnPc7	scéna
(	(	kIx(	(
<g/>
pan	pan	k1gMnSc1	pan
Bean	Bean	k1gMnSc1	Bean
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
různé	různý	k2eAgInPc4d1	různý
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mu	on	k3xPp3gInSc3	on
připomenou	připomenout	k5eAaPmIp3nP	připomenout
dřívější	dřívější	k2eAgFnPc4d1	dřívější
příhody	příhoda	k1gFnPc4	příhoda
a	a	k8xC	a
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Speciály	speciál	k1gInPc4	speciál
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Skeče	skeč	k1gInSc2	skeč
Comic	Comic	k1gMnSc1	Comic
Relief	Relief	k1gMnSc1	Relief
==	==	k?	==
</s>
</p>
<p>
<s>
Scénky	scénka	k1gFnSc2	scénka
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
charitativní	charitativní	k2eAgFnSc2d1	charitativní
akce	akce	k1gFnSc2	akce
Comic	Comic	k1gMnSc1	Comic
Relief	Relief	k1gMnSc1	Relief
</s>
</p>
<p>
<s>
==	==	k?	==
Vydání	vydání	k1gNnSc1	vydání
na	na	k7c6	na
VHS	VHS	kA	VHS
a	a	k8xC	a
DVD	DVD	kA	DVD
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
VHS	VHS	kA	VHS
===	===	k?	===
</s>
</p>
<p>
<s>
Bean	Bean	k1gInSc1	Bean
1	[number]	k4	1
<g/>
:	:	kIx,	:
Úžasná	úžasný	k2eAgNnPc1d1	úžasné
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
pana	pan	k1gMnSc2	pan
Beana	Bean	k1gMnSc2	Bean
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Amazing	Amazing	k1gInSc1	Amazing
Adventures	Adventures	k1gMnSc1	Adventures
Of	Of	k1gMnSc1	Of
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gNnSc1	Bean
<g/>
)	)	kIx)	)
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díly	dílo	k1gNnPc7	dílo
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
a	a	k8xC	a
bonus	bonus	k1gInSc1	bonus
Knihovna	knihovna	k1gFnSc1	knihovna
</s>
</p>
<p>
<s>
Bean	Bean	k1gInSc1	Bean
2	[number]	k4	2
<g/>
:	:	kIx,	:
Vzrušující	vzrušující	k2eAgInPc1d1	vzrušující
kousky	kousek	k1gInPc1	kousek
pana	pan	k1gMnSc2	pan
Beana	Bean	k1gMnSc2	Bean
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Exciting	Exciting	k1gInSc1	Exciting
Escapades	Escapades	k1gMnSc1	Escapades
of	of	k?	of
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gNnSc1	Bean
<g/>
)	)	kIx)	)
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díly	dílo	k1gNnPc7	dílo
3	[number]	k4	3
a	a	k8xC	a
4	[number]	k4	4
a	a	k8xC	a
bonus	bonus	k1gInSc1	bonus
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
zastávka	zastávka	k1gFnSc1	zastávka
</s>
</p>
<p>
<s>
Bean	Bean	k1gInSc1	Bean
3	[number]	k4	3
<g/>
:	:	kIx,	:
Příšerné	příšerný	k2eAgInPc1d1	příšerný
příběhy	příběh	k1gInPc1	příběh
pana	pan	k1gMnSc2	pan
Beana	Bean	k1gMnSc2	Bean
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Terrible	Terrible	k1gMnSc1	Terrible
Tales	Tales	k1gMnSc1	Tales
Of	Of	k1gMnSc1	Of
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gNnSc1	Bean
<g/>
)	)	kIx)	)
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díly	dílo	k1gNnPc7	dílo
5	[number]	k4	5
a	a	k8xC	a
6	[number]	k4	6
</s>
</p>
<p>
<s>
Bean	Bean	k1gInSc1	Bean
4	[number]	k4	4
<g/>
:	:	kIx,	:
Šťastné	Šťastné	k2eAgFnSc2d1	Šťastné
nehody	nehoda	k1gFnSc2	nehoda
pana	pan	k1gMnSc2	pan
Beana	Bean	k1gMnSc2	Bean
(	(	kIx(	(
<g/>
The	The	k1gMnSc2	The
Merry	Merra	k1gMnSc2	Merra
Mishaps	Mishaps	k1gInSc1	Mishaps
of	of	k?	of
Mr	Mr	k1gFnSc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gNnSc1	Bean
<g/>
)	)	kIx)	)
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díly	dílo	k1gNnPc7	dílo
7	[number]	k4	7
a	a	k8xC	a
8	[number]	k4	8
</s>
</p>
<p>
<s>
Bean	Bean	k1gInSc1	Bean
5	[number]	k4	5
<g/>
:	:	kIx,	:
Nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
honičky	honička	k1gFnSc2	honička
pana	pan	k1gMnSc2	pan
Beana	Bean	k1gMnSc2	Bean
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Perilous	Perilous	k1gMnSc1	Perilous
Persuits	Persuitsa	k1gFnPc2	Persuitsa
of	of	k?	of
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gNnSc1	Bean
<g/>
)	)	kIx)	)
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díly	dílo	k1gNnPc7	dílo
9	[number]	k4	9
a	a	k8xC	a
10	[number]	k4	10
</s>
</p>
<p>
<s>
Bean	Bean	k1gInSc1	Bean
6	[number]	k4	6
<g/>
:	:	kIx,	:
Neviděný	viděný	k2eNgInSc1d1	neviděný
Bean	Bean	k1gInSc1	Bean
(	(	kIx(	(
<g/>
Unseen	Unseen	k2eAgInSc1d1	Unseen
Bean	Bean	k1gInSc1	Bean
<g/>
)	)	kIx)	)
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díly	dílo	k1gNnPc7	dílo
14	[number]	k4	14
a	a	k8xC	a
11	[number]	k4	11
</s>
</p>
<p>
<s>
Bean	Bean	k1gInSc1	Bean
7	[number]	k4	7
<g/>
:	:	kIx,	:
Poslední	poslední	k2eAgInPc1d1	poslední
žerty	žert	k1gInPc1	žert
pana	pan	k1gMnSc2	pan
Beana	Bean	k1gMnSc2	Bean
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Final	Final	k1gMnSc1	Final
Frolics	Frolicsa	k1gFnPc2	Frolicsa
of	of	k?	of
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gNnSc1	Bean
<g/>
)	)	kIx)	)
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díly	dílo	k1gNnPc7	dílo
12	[number]	k4	12
a	a	k8xC	a
13	[number]	k4	13
</s>
</p>
<p>
<s>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
nevyšlo	vyjít	k5eNaPmAgNnS	vyjít
Bean	Bean	k1gNnSc1	Bean
8	[number]	k4	8
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Best	Best	k1gMnSc1	Best
Bits	Bits	k1gInSc1	Bits
of	of	k?	of
Mr	Mr	k1gFnSc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gInSc1	Bean
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díl	díl	k1gInSc4	díl
15	[number]	k4	15
(	(	kIx(	(
<g/>
72	[number]	k4	72
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
DVD	DVD	kA	DVD
===	===	k?	===
</s>
</p>
<p>
<s>
Mr	Mr	k?	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gInSc1	Bean
1	[number]	k4	1
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díly	díl	k1gInPc4	díl
1	[number]	k4	1
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
13	[number]	k4	13
a	a	k8xC	a
15	[number]	k4	15
(	(	kIx(	(
<g/>
72	[number]	k4	72
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mr	Mr	k?	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gInSc1	Bean
2	[number]	k4	2
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díly	díl	k1gInPc4	díl
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
,	,	kIx,	,
14	[number]	k4	14
a	a	k8xC	a
dokument	dokument	k1gInSc1	dokument
Příběh	příběh	k1gInSc1	příběh
pana	pan	k1gMnSc2	pan
Beana	Bean	k1gMnSc2	Bean
</s>
</p>
<p>
<s>
Mr	Mr	k?	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gInSc1	Bean
3	[number]	k4	3
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díly	díl	k1gInPc4	díl
2	[number]	k4	2
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
,	,	kIx,	,
12	[number]	k4	12
a	a	k8xC	a
bonusy	bonus	k1gInPc4	bonus
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
zastávka	zastávka	k1gFnSc1	zastávka
/	/	kIx~	/
Zastávka	zastávka	k1gFnSc1	zastávka
<g/>
,	,	kIx,	,
Knihovna	knihovna	k1gFnSc1	knihovna
</s>
</p>
<p>
<s>
===	===	k?	===
DVD	DVD	kA	DVD
remasterovaná	remasterovaný	k2eAgFnSc1d1	remasterovaná
verze	verze	k1gFnSc1	verze
===	===	k?	===
</s>
</p>
<p>
<s>
Mr	Mr	k?	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gInSc1	Bean
1	[number]	k4	1
(	(	kIx(	(
<g/>
digitálně	digitálně	k6eAd1	digitálně
remasterovaná	remasterovaný	k2eAgFnSc1d1	remasterovaná
edice	edice	k1gFnSc1	edice
<g/>
)	)	kIx)	)
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díly	dílo	k1gNnPc7	dílo
1	[number]	k4	1
až	až	k8xS	až
5	[number]	k4	5
</s>
</p>
<p>
<s>
Mr	Mr	k?	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gInSc1	Bean
2	[number]	k4	2
(	(	kIx(	(
<g/>
digitálně	digitálně	k6eAd1	digitálně
remasterovaná	remasterovaný	k2eAgFnSc1d1	remasterovaná
edice	edice	k1gFnSc1	edice
<g/>
)	)	kIx)	)
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díly	dílo	k1gNnPc7	dílo
6	[number]	k4	6
až	až	k8xS	až
10	[number]	k4	10
</s>
</p>
<p>
<s>
Mr	Mr	k?	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gInSc1	Bean
3	[number]	k4	3
(	(	kIx(	(
<g/>
digitálně	digitálně	k6eAd1	digitálně
remasterovaná	remasterovaný	k2eAgFnSc1d1	remasterovaná
edice	edice	k1gFnSc1	edice
<g/>
)	)	kIx)	)
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díly	dílo	k1gNnPc7	dílo
11	[number]	k4	11
až	až	k8xS	až
14	[number]	k4	14
a	a	k8xC	a
bonusy	bonus	k1gInPc1	bonus
Knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
zastávka	zastávka	k1gFnSc1	zastávka
</s>
</p>
<p>
<s>
Mr	Mr	k?	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gInSc1	Bean
4	[number]	k4	4
(	(	kIx(	(
<g/>
digitálně	digitálně	k6eAd1	digitálně
remasterovaná	remasterovaný	k2eAgFnSc1d1	remasterovaná
edice	edice	k1gFnSc1	edice
<g/>
)	)	kIx)	)
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díl	díl	k1gInSc4	díl
15	[number]	k4	15
(	(	kIx(	(
<g/>
52	[number]	k4	52
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
dokument	dokument	k1gInSc4	dokument
Beanův	Beanův	k2eAgInSc1d1	Beanův
příběh	příběh	k1gInSc1	příběh
</s>
</p>
<p>
<s>
==	==	k?	==
Filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
Mr	Mr	k?	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gNnSc1	Bean
<g/>
:	:	kIx,	:
Největší	veliký	k2eAgFnSc1d3	veliký
filmová	filmový	k2eAgFnSc1d1	filmová
katastrofa	katastrofa	k1gFnSc1	katastrofa
–	–	k?	–
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gInSc1	Bean
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
Britské	britský	k2eAgFnSc6d1	britská
národní	národní	k2eAgFnSc6d1	národní
galerii	galerie	k1gFnSc6	galerie
jako	jako	k8xS	jako
hlídač	hlídač	k1gInSc4	hlídač
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gNnSc4	on
nikdo	nikdo	k3yNnSc1	nikdo
nemá	mít	k5eNaImIp3nS	mít
rád	rád	k2eAgMnSc1d1	rád
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
Beana	Bean	k1gInSc2	Bean
pošle	poslat	k5eAaPmIp3nS	poslat
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
vydávaje	vydávat	k5eAaImSgInS	vydávat
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
znalce	znalec	k1gMnSc4	znalec
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
zbavil	zbavit	k5eAaPmAgMnS	zbavit
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
Bean	Bean	k1gInSc4	Bean
odhalit	odhalit	k5eAaPmF	odhalit
slavnou	slavný	k2eAgFnSc4d1	slavná
Whistlerovu	Whistlerův	k2eAgFnSc4d1	Whistlerova
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
Bean	Bean	k1gMnSc1	Bean
seznámí	seznámit	k5eAaPmIp3nS	seznámit
s	s	k7c7	s
kurátorem	kurátor	k1gMnSc7	kurátor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gMnSc4	on
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
ubytuje	ubytovat	k5eAaPmIp3nS	ubytovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
ho	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
taky	taky	k6eAd1	taky
nemá	mít	k5eNaImIp3nS	mít
příliš	příliš	k6eAd1	příliš
v	v	k7c6	v
lásce	láska	k1gFnSc6	láska
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gInSc1	Bean
jde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
nové	nový	k2eAgFnSc2d1	nová
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
slavný	slavný	k2eAgInSc1d1	slavný
obraz	obraz	k1gInSc1	obraz
ihned	ihned	k6eAd1	ihned
nedopatřením	nedopatření	k1gNnSc7	nedopatření
zničí	zničit	k5eAaPmIp3nP	zničit
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
před	před	k7c7	před
slavnostním	slavnostní	k2eAgNnSc7d1	slavnostní
odhalením	odhalení	k1gNnSc7	odhalení
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
Bean	Beana	k1gFnPc2	Beana
situaci	situace	k1gFnSc3	situace
zachrání	zachránit	k5eAaPmIp3nS	zachránit
<g/>
:	:	kIx,	:
místo	místo	k7c2	místo
pravého	pravý	k2eAgInSc2d1	pravý
obrazu	obraz	k1gInSc2	obraz
vystaví	vystavit	k5eAaPmIp3nS	vystavit
jeho	jeho	k3xOp3gFnSc4	jeho
kopii	kopie	k1gFnSc4	kopie
(	(	kIx(	(
<g/>
plakát	plakát	k1gInSc1	plakát
<g/>
)	)	kIx)	)
a	a	k8xC	a
skutečný	skutečný	k2eAgInSc1d1	skutečný
obraz	obraz	k1gInSc1	obraz
si	se	k3xPyFc3	se
ponechá	ponechat	k5eAaPmIp3nS	ponechat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
nakonec	nakonec	k6eAd1	nakonec
zachrání	zachránit	k5eAaPmIp3nP	zachránit
i	i	k9	i
kurátorovu	kurátorův	k2eAgFnSc4d1	kurátorův
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prázdniny	prázdniny	k1gFnPc1	prázdniny
pana	pan	k1gMnSc2	pan
Beana	Bean	k1gMnSc2	Bean
–	–	k?	–
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
filmu	film	k1gInSc6	film
hraje	hrát	k5eAaImIp3nS	hrát
i	i	k9	i
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Karel	Karel	k1gMnSc1	Karel
Roden	Rodno	k1gNnPc2	Rodno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seriál	seriál	k1gInSc1	seriál
==	==	k?	==
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
Rowan	Rowan	k1gMnSc1	Rowan
Atkinson	Atkinson	k1gMnSc1	Atkinson
skončit	skončit	k5eAaPmF	skončit
s	s	k7c7	s
rolí	role	k1gFnSc7	role
Mr	Mr	k1gFnPc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Beana	Beana	k1gFnSc1	Beana
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tvůrci	tvůrce	k1gMnPc1	tvůrce
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c4	na
natočení	natočení	k1gNnSc4	natočení
animovaného	animovaný	k2eAgInSc2d1	animovaný
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c4	mnoho
řad	řada	k1gFnPc2	řada
a	a	k8xC	a
těší	těšit	k5eAaImIp3nS	těšit
se	se	k3xPyFc4	se
velkého	velký	k2eAgInSc2d1	velký
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hraný	hraný	k2eAgInSc1d1	hraný
seriál	seriál	k1gInSc1	seriál
byl	být	k5eAaImAgInS	být
úspěšnější	úspěšný	k2eAgMnSc1d2	úspěšnější
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Mr	Mr	k1gFnSc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gInSc1	Bean
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
,	,	kIx,	,
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gMnSc1	Bean
(	(	kIx(	(
<g/>
character	character	k1gMnSc1	character
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
List	list	k1gInSc1	list
of	of	k?	of
Mr	Mr	k1gFnSc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gMnSc1	Bean
episodes	episodes	k1gMnSc1	episodes
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mr	Mr	k1gFnSc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gInSc1	Bean
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Mr	Mr	k?	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gInSc1	Bean
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mr	Mr	k?	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gInSc1	Bean
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
