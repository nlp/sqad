<s>
Nový	nový	k2eAgInSc1d1	nový
Hollywood	Hollywood	k1gInSc1	Hollywood
<g/>
,	,	kIx,	,
také	také	k9	také
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
"	"	kIx"	"
<g/>
post-klasický	postlasický	k2eAgInSc1d1	post-klasický
Hollywood	Hollywood	k1gInSc1	Hollywood
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
"	"	kIx"	"
<g/>
Americká	americký	k2eAgFnSc1d1	americká
nová	nový	k2eAgFnSc1d1	nová
vlna	vlna	k1gFnSc1	vlna
<g/>
"	"	kIx"	"
označuje	označovat	k5eAaImIp3nS	označovat
období	období	k1gNnSc4	období
americké	americký	k2eAgFnSc2d1	americká
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
mezi	mezi	k7c7	mezi
pozdními	pozdní	k2eAgNnPc7d1	pozdní
60	[number]	k4	60
<g/>
.	.	kIx.	.
lety	léto	k1gNnPc7	léto
(	(	kIx(	(
<g/>
Bonnie	Bonnie	k1gFnSc1	Bonnie
a	a	k8xC	a
Clyde	Clyd	k1gInSc5	Clyd
<g/>
)	)	kIx)	)
a	a	k8xC	a
ranými	raný	k2eAgNnPc7d1	rané
80	[number]	k4	80
<g/>
.	.	kIx.	.
lety	léto	k1gNnPc7	léto
(	(	kIx(	(
<g/>
Nebeská	nebeský	k2eAgFnSc1d1	nebeská
brána	brána	k1gFnSc1	brána
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
stala	stát	k5eAaPmAgFnS	stát
prominentní	prominentní	k2eAgFnSc1d1	prominentní
nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
mladých	mladý	k2eAgMnPc2d1	mladý
filmařů	filmař	k1gMnPc2	filmař
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
tvůrci	tvůrce	k1gMnPc1	tvůrce
Nového	Nového	k2eAgInSc2d1	Nového
Hollywoodu	Hollywood	k1gInSc2	Hollywood
změnili	změnit	k5eAaPmAgMnP	změnit
nejen	nejen	k6eAd1	nejen
typ	typ	k1gInSc4	typ
natáčených	natáčený	k2eAgInPc2d1	natáčený
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jejich	jejich	k3xOp3gFnSc4	jejich
produkci	produkce	k1gFnSc4	produkce
a	a	k8xC	a
marketing	marketing	k1gInSc4	marketing
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
tedy	tedy	k9	tedy
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
studia	studio	k1gNnSc2	studio
přistupovala	přistupovat	k5eAaImAgFnS	přistupovat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
a	a	k8xC	a
uvádění	uvádění	k1gNnSc3	uvádění
hraných	hraný	k2eAgInPc2d1	hraný
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
novovlnní	novovlnní	k2eAgMnPc1d1	novovlnní
tvůrci	tvůrce	k1gMnPc1	tvůrce
natáčeli	natáčet	k5eAaImAgMnP	natáčet
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
produktem	produkt	k1gInSc7	produkt
studiového	studiový	k2eAgInSc2d1	studiový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
individualističtí	individualistický	k2eAgMnPc1d1	individualistický
režiséři	režisér	k1gMnPc1	režisér
nebyli	být	k5eNaImAgMnP	být
nezávislými	závislý	k2eNgMnPc7d1	nezávislý
filmaři	filmař	k1gMnPc7	filmař
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pracovali	pracovat	k5eAaImAgMnP	pracovat
pod	pod	k7c7	pod
hollywoodskými	hollywoodský	k2eAgNnPc7d1	hollywoodské
studii	studio	k1gNnPc7	studio
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
vnesli	vnést	k5eAaPmAgMnP	vnést
do	do	k7c2	do
amerického	americký	k2eAgInSc2d1	americký
filmu	film	k1gInSc2	film
osobité	osobitý	k2eAgNnSc1d1	osobité
témata	téma	k1gNnPc4	téma
a	a	k8xC	a
styly	styl	k1gInPc4	styl
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
radikálně	radikálně	k6eAd1	radikálně
změnili	změnit	k5eAaPmAgMnP	změnit
studiovou	studiový	k2eAgFnSc4d1	studiová
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ustanovila	ustanovit	k5eAaPmAgFnS	ustanovit
dřívější	dřívější	k2eAgFnSc1d1	dřívější
generace	generace	k1gFnSc1	generace
filmových	filmový	k2eAgMnPc2d1	filmový
tvůrců	tvůrce	k1gMnPc2	tvůrce
mezi	mezi	k7c7	mezi
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
zpětně	zpětně	k6eAd1	zpětně
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
klasický	klasický	k2eAgInSc4d1	klasický
Hollywood	Hollywood	k1gInSc4	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgInPc1d1	soudní
spory	spor	k1gInPc1	spor
s	s	k7c7	s
majoritními	majoritní	k2eAgNnPc7d1	majoritní
hollywoodskými	hollywoodský	k2eAgNnPc7d1	hollywoodské
studii	studio	k1gNnPc7	studio
na	na	k7c6	na
konci	konec	k1gInSc6	konec
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
postupný	postupný	k2eAgInSc1d1	postupný
nástup	nástup	k1gInSc1	nástup
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
vážně	vážně	k6eAd1	vážně
oslabily	oslabit	k5eAaPmAgFnP	oslabit
tradiční	tradiční	k2eAgInSc4d1	tradiční
studiový	studiový	k2eAgInSc4d1	studiový
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Hollywoodská	hollywoodský	k2eAgNnPc1d1	hollywoodské
studia	studio	k1gNnPc1	studio
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
nepříznivé	příznivý	k2eNgInPc4d1	nepříznivý
vlivy	vliv	k1gInPc4	vliv
reagovala	reagovat	k5eAaBmAgFnS	reagovat
zintenzivněním	zintenzivnění	k1gNnSc7	zintenzivnění
spektakulárních	spektakulární	k2eAgFnPc2d1	spektakulární
kvalit	kvalita	k1gFnPc2	kvalita
svých	svůj	k3xOyFgInPc2	svůj
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
navrátilo	navrátit	k5eAaPmAgNnS	navrátit
ubývající	ubývající	k2eAgNnSc4d1	ubývající
publikum	publikum	k1gNnSc4	publikum
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
ziskovost	ziskovost	k1gFnSc4	ziskovost
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
častěji	často	k6eAd2	často
než	než	k8xS	než
dosud	dosud	k6eAd1	dosud
byl	být	k5eAaImAgInS	být
užíván	užíván	k2eAgInSc1d1	užíván
barevný	barevný	k2eAgInSc1d1	barevný
systém	systém	k1gInSc1	systém
Technicolor	Technicolora	k1gFnPc2	Technicolora
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
širokoúhlý	širokoúhlý	k2eAgInSc4d1	širokoúhlý
formát	formát	k1gInSc4	formát
Cinemascope	Cinemascop	k1gInSc5	Cinemascop
<g/>
,	,	kIx,	,
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
byly	být	k5eAaImAgInP	být
zavedeny	zaveden	k2eAgInPc1d1	zaveden
stereo	stereo	k2eAgInPc1d1	stereo
reproduktory	reproduktor	k1gInPc1	reproduktor
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
natáčeny	natáčet	k5eAaImNgInP	natáčet
3D	[number]	k4	3D
filmy	film	k1gInPc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
všechny	všechen	k3xTgFnPc1	všechen
inovace	inovace	k1gFnPc1	inovace
byly	být	k5eAaImAgFnP	být
tedy	tedy	k9	tedy
zavedeny	zavést	k5eAaPmNgFnP	zavést
do	do	k7c2	do
americké	americký	k2eAgFnSc2d1	americká
kinematografie	kinematografie	k1gFnSc2	kinematografie
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
navrátilo	navrátit	k5eAaPmAgNnS	navrátit
ubývající	ubývající	k2eAgNnSc1d1	ubývající
publikum	publikum	k1gNnSc1	publikum
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
obecně	obecně	k6eAd1	obecně
tyto	tento	k3xDgFnPc1	tento
snahy	snaha	k1gFnPc1	snaha
nebyly	být	k5eNaImAgFnP	být
z	z	k7c2	z
finančního	finanční	k2eAgNnSc2d1	finanční
hlediska	hledisko	k1gNnSc2	hledisko
efektní	efektní	k2eAgNnSc1d1	efektní
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
nezvýšily	zvýšit	k5eNaPmAgInP	zvýšit
profit	profit	k1gInSc4	profit
hollywoodským	hollywoodský	k2eAgNnPc3d1	hollywoodské
studiím	studio	k1gNnPc3	studio
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
označil	označit	k5eAaPmAgInS	označit
časopis	časopis	k1gInSc1	časopis
Life	Lif	k1gFnSc2	Lif
předešlých	předešlý	k2eAgInPc2d1	předešlý
deset	deset	k4xCc4	deset
let	let	k1gInSc4	let
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
strašlivou	strašlivý	k2eAgFnSc7d1	strašlivá
dekádou	dekáda	k1gFnSc7	dekáda
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
Hollywood	Hollywood	k1gInSc4	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
amerických	americký	k2eAgNnPc6d1	americké
kinech	kino	k1gNnPc6	kino
dominantní	dominantní	k2eAgInSc4d1	dominantní
muzikály	muzikál	k1gInPc4	muzikál
<g/>
,	,	kIx,	,
historické	historický	k2eAgInPc4d1	historický
eposy	epos	k1gInPc4	epos
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
efektivně	efektivně	k6eAd1	efektivně
využívajících	využívající	k2eAgNnPc2d1	využívající
velkých	velký	k2eAgNnPc2d1	velké
pláten	plátno	k1gNnPc2	plátno
a	a	k8xC	a
stereofonního	stereofonní	k2eAgInSc2d1	stereofonní
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
ubývání	ubývání	k1gNnSc1	ubývání
diváků	divák	k1gMnPc2	divák
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
a	a	k8xC	a
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vykazovala	vykazovat	k5eAaImAgNnP	vykazovat
kina	kino	k1gNnPc1	kino
alarmující	alarmující	k2eAgNnPc1d1	alarmující
nízká	nízký	k2eAgNnPc1d1	nízké
čísla	číslo	k1gNnPc1	číslo
návštěvnosti	návštěvnost	k1gFnSc2	návštěvnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
poválečná	poválečný	k2eAgFnSc1d1	poválečná
generace	generace	k1gFnSc1	generace
pomalu	pomalu	k6eAd1	pomalu
dospívá	dospívat	k5eAaImIp3nS	dospívat
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
starý	starý	k2eAgInSc1d1	starý
<g/>
"	"	kIx"	"
Hollywood	Hollywood	k1gInSc1	Hollywood
rychle	rychle	k6eAd1	rychle
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
peníze	peníz	k1gInPc1	peníz
<g/>
,	,	kIx,	,
představitelé	představitel	k1gMnPc1	představitel
studií	studio	k1gNnPc2	studio
jsou	být	k5eAaImIp3nP	být
nejistí	jistý	k2eNgMnPc1d1	nejistý
jak	jak	k6eAd1	jak
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
radikální	radikální	k2eAgFnPc4d1	radikální
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
demografickém	demografický	k2eAgNnSc6d1	demografické
složení	složení	k1gNnSc6	složení
publika	publikum	k1gNnSc2	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Výrazná	výrazný	k2eAgFnSc1d1	výrazná
změna	změna	k1gFnSc1	změna
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
většinové	většinový	k2eAgNnSc1d1	většinové
publikum	publikum	k1gNnSc1	publikum
hollywoodských	hollywoodský	k2eAgInPc2d1	hollywoodský
filmů	film	k1gInPc2	film
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
ze	z	k7c2	z
středoškolsky	středoškolsky	k6eAd1	středoškolsky
vzdělaných	vzdělaný	k2eAgMnPc2d1	vzdělaný
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
středních	střední	k2eAgNnPc6d1	střední
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
k	k	k7c3	k
výrazně	výrazně	k6eAd1	výrazně
mladším	mladý	k2eAgMnSc6d2	mladší
<g/>
,	,	kIx,	,
vysokoškolsky	vysokoškolsky	k6eAd1	vysokoškolsky
vzdělaným	vzdělaný	k2eAgMnPc3d1	vzdělaný
divákům	divák	k1gMnPc3	divák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
76	[number]	k4	76
<g/>
%	%	kIx~	%
pravidelných	pravidelný	k2eAgMnPc2d1	pravidelný
návštěvníků	návštěvník	k1gMnPc2	návštěvník
kin	kino	k1gNnPc2	kino
mladší	mladý	k2eAgMnSc1d2	mladší
než	než	k8xS	než
30	[number]	k4	30
let	léto	k1gNnPc2	léto
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byli	být	k5eAaImAgMnP	být
vysokoškolsky	vysokoškolsky	k6eAd1	vysokoškolsky
vzdělaní	vzdělaný	k2eAgMnPc1d1	vzdělaný
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
publikum	publikum	k1gNnSc1	publikum
mělo	mít	k5eAaImAgNnS	mít
spíše	spíše	k9	spíše
než	než	k8xS	než
americké	americký	k2eAgInPc1d1	americký
filmy	film	k1gInPc1	film
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
evropské	evropský	k2eAgFnSc6d1	Evropská
"	"	kIx"	"
<g/>
umělecké	umělecký	k2eAgFnSc6d1	umělecká
<g/>
"	"	kIx"	"
snímky	snímek	k1gInPc1	snímek
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k9	jako
díla	dílo	k1gNnPc4	dílo
japonské	japonský	k2eAgFnSc2d1	japonská
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Zoufalství	zoufalství	k1gNnSc1	zoufalství
vrcholných	vrcholný	k2eAgMnPc2d1	vrcholný
představitelů	představitel	k1gMnPc2	představitel
studií	studie	k1gFnPc2	studie
během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
poklesu	pokles	k1gInSc2	pokles
<g/>
,	,	kIx,	,
podpořené	podpořený	k2eAgFnPc1d1	podpořená
několika	několik	k4yIc7	několik
nákladnými	nákladný	k2eAgInPc7d1	nákladný
filmovými	filmový	k2eAgInPc7d1	filmový
propadáky	propadák	k1gInPc7	propadák
<g/>
,	,	kIx,	,
vedlo	vést	k5eAaImAgNnS	vést
majoritní	majoritní	k2eAgNnSc1d1	majoritní
studia	studio	k1gNnPc1	studio
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
umožnila	umožnit	k5eAaPmAgFnS	umožnit
větší	veliký	k2eAgFnSc4d2	veliký
produkční	produkční	k2eAgFnSc4d1	produkční
kontrolu	kontrola	k1gFnSc4	kontrola
mladým	mladý	k2eAgMnPc3d1	mladý
kreativním	kreativní	k2eAgMnPc3d1	kreativní
režisérům	režisér	k1gMnPc3	režisér
a	a	k8xC	a
producentům	producent	k1gMnPc3	producent
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
nalákání	nalákání	k1gNnSc4	nalákání
početné	početný	k2eAgFnSc2d1	početná
divácké	divácký	k2eAgFnSc2d1	divácká
obce	obec	k1gFnSc2	obec
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
studia	studio	k1gNnSc2	studio
najala	najmout	k5eAaPmAgFnS	najmout
hostující	hostující	k2eAgMnPc4d1	hostující
mladé	mladý	k2eAgMnPc4d1	mladý
filmaře	filmař	k1gMnPc4	filmař
(	(	kIx(	(
<g/>
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
začínalo	začínat	k5eAaImAgNnS	začínat
u	u	k7c2	u
Rogera	Roger	k1gMnSc2	Roger
Cormana	Corman	k1gMnSc2	Corman
<g/>
)	)	kIx)	)
a	a	k8xC	a
dovolila	dovolit	k5eAaPmAgFnS	dovolit
jim	on	k3xPp3gMnPc3	on
natáčet	natáčet	k5eAaImF	natáčet
netradiční	tradiční	k2eNgInPc4d1	netradiční
filmy	film	k1gInPc4	film
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
malým	malý	k2eAgNnSc7d1	malé
vměšováním	vměšování	k1gNnSc7	vměšování
samotného	samotný	k2eAgNnSc2d1	samotné
studia	studio	k1gNnSc2	studio
do	do	k7c2	do
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
najímání	najímání	k1gNnSc1	najímání
mladých	mladý	k2eAgMnPc2d1	mladý
inovativních	inovativní	k2eAgMnPc2d1	inovativní
tvůrců	tvůrce	k1gMnPc2	tvůrce
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
zhroucením	zhroucení	k1gNnSc7	zhroucení
Produkčního	produkční	k2eAgInSc2d1	produkční
kódu	kód	k1gInSc2	kód
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
a	a	k8xC	a
zavedením	zavedení	k1gNnSc7	zavedení
nového	nový	k2eAgInSc2d1	nový
ratingového	ratingový	k2eAgInSc2d1	ratingový
systému	systém	k1gInSc2	systém
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
připravilo	připravit	k5eAaPmAgNnS	připravit
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
Nový	nový	k2eAgInSc4d1	nový
Hollywood	Hollywood	k1gInSc4	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
mladých	mladý	k2eAgMnPc2d1	mladý
hollywoodských	hollywoodský	k2eAgMnPc2d1	hollywoodský
filmařů	filmař	k1gMnPc2	filmař
<g/>
,	,	kIx,	,
vzešlá	vzešlý	k2eAgNnPc4d1	vzešlé
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
filmových	filmový	k2eAgFnPc2d1	filmová
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
studiím	studie	k1gFnPc3	studie
pomoci	pomoc	k1gFnSc2	pomoc
navrátit	navrátit	k5eAaPmF	navrátit
mladé	mladý	k2eAgNnSc4d1	mladé
publikum	publikum	k1gNnSc4	publikum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
studia	studio	k1gNnSc2	studio
rapidně	rapidně	k6eAd1	rapidně
ztrácela	ztrácet	k5eAaImAgFnS	ztrácet
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
mladých	mladý	k2eAgMnPc2d1	mladý
filmařů	filmař	k1gMnPc2	filmař
změnila	změnit	k5eAaPmAgFnS	změnit
producenty	producent	k1gMnPc4	producent
řízený	řízený	k2eAgInSc4d1	řízený
hollywoodský	hollywoodský	k2eAgInSc4d1	hollywoodský
systém	systém	k1gInSc4	systém
minulosti	minulost	k1gFnSc2	minulost
a	a	k8xC	a
natáčela	natáčet	k5eAaImAgFnS	natáčet
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
plné	plný	k2eAgFnPc4d1	plná
radosti	radost	k1gFnPc4	radost
<g/>
,	,	kIx,	,
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
sexu	sex	k1gInSc2	sex
i	i	k8xC	i
zápalu	zápal	k1gInSc2	zápal
pro	pro	k7c4	pro
umělecké	umělecký	k2eAgFnPc4d1	umělecká
hodnoty	hodnota	k1gFnPc4	hodnota
samotného	samotný	k2eAgInSc2d1	samotný
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
facto	facto	k1gNnSc4	facto
nejviditelnější	viditelný	k2eAgFnSc1d3	nejviditelnější
změna	změna	k1gFnSc1	změna
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
novohollywoodští	novohollywoodský	k2eAgMnPc1d1	novohollywoodský
filmaři	filmař	k1gMnPc1	filmař
vnesli	vnést	k5eAaPmAgMnP	vnést
do	do	k7c2	do
poetiky	poetika	k1gFnSc2	poetika
amerického	americký	k2eAgInSc2d1	americký
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
příklon	příklon	k1gInSc1	příklon
k	k	k7c3	k
realismu	realismus	k1gInSc3	realismus
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožnil	umožnit	k5eAaPmAgInS	umožnit
zejména	zejména	k9	zejména
technologický	technologický	k2eAgInSc1d1	technologický
průlom	průlom	k1gInSc1	průlom
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pomocí	pomocí	k7c2	pomocí
zdokonalených	zdokonalený	k2eAgFnPc2d1	zdokonalená
lehkých	lehký	k2eAgFnPc2d1	lehká
kamer	kamera	k1gFnPc2	kamera
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
mnohem	mnohem	k6eAd1	mnohem
snadněji	snadno	k6eAd2	snadno
natáčet	natáčet	k5eAaImF	natáčet
v	v	k7c6	v
exteriérech	exteriér	k1gInPc6	exteriér
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
natáčení	natáčení	k1gNnSc1	natáčení
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
lokacích	lokace	k1gFnPc6	lokace
stalo	stát	k5eAaPmAgNnS	stát
levnější	levný	k2eAgNnSc1d2	levnější
(	(	kIx(	(
<g/>
nebylo	být	k5eNaImAgNnS	být
třeba	třeba	k6eAd1	třeba
stavět	stavět	k5eAaImF	stavět
kulisy	kulisa	k1gFnPc4	kulisa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mladí	mladý	k2eAgMnPc1d1	mladý
filmaři	filmař	k1gMnPc1	filmař
rychle	rychle	k6eAd1	rychle
nalezli	naleznout	k5eAaPmAgMnP	naleznout
zálibu	záliba	k1gFnSc4	záliba
v	v	k7c6	v
převážně	převážně	k6eAd1	převážně
exteriérovém	exteriérový	k2eAgNnSc6d1	exteriérové
natáčení	natáčení	k1gNnSc6	natáčení
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zesílili	zesílit	k5eAaPmAgMnP	zesílit
efekt	efekt	k1gInSc4	efekt
realismu	realismus	k1gInSc2	realismus
a	a	k8xC	a
pohroužení	pohroužení	k1gNnSc2	pohroužení
se	se	k3xPyFc4	se
do	do	k7c2	do
těchto	tento	k3xDgInPc2	tento
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
umělostí	umělost	k1gFnSc7	umělost
mizanscény	mizanscéna	k1gFnSc2	mizanscéna
prostředí	prostředí	k1gNnSc2	prostředí
předchozích	předchozí	k2eAgInPc2d1	předchozí
muzikálů	muzikál	k1gInPc2	muzikál
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
studiových	studiový	k2eAgInPc2d1	studiový
spektáklů	spektákl	k1gInPc2	spektákl
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
stylistickým	stylistický	k2eAgInSc7d1	stylistický
faktorem	faktor	k1gInSc7	faktor
filmů	film	k1gInPc2	film
Nového	Nového	k2eAgInSc2d1	Nového
Hollywoodu	Hollywood	k1gInSc2	Hollywood
bylo	být	k5eAaImAgNnS	být
kreativní	kreativní	k2eAgNnSc1d1	kreativní
užití	užití	k1gNnSc1	užití
střihu	střih	k1gInSc2	střih
a	a	k8xC	a
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
realismu	realismus	k1gInSc2	realismus
filmy	film	k1gInPc4	film
Nového	Nového	k2eAgInSc2d1	Nového
Hollywoodu	Hollywood	k1gInSc2	Hollywood
často	často	k6eAd1	často
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
kontroverzní	kontroverzní	k2eAgFnPc4d1	kontroverzní
politická	politický	k2eAgNnPc4d1	politické
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
používaly	používat	k5eAaImAgInP	používat
rockovou	rockový	k2eAgFnSc4d1	rocková
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
tematizovaly	tematizovat	k5eAaImAgInP	tematizovat
sexuální	sexuální	k2eAgFnSc4d1	sexuální
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přesně	přesně	k6eAd1	přesně
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
představám	představa	k1gFnPc3	představa
studií	studio	k1gNnPc2	studio
o	o	k7c6	o
kontra-kultuře	kontraultura	k1gFnSc6	kontra-kultura
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gNnSc7	jejíž
zobrazováním	zobrazování	k1gNnSc7	zobrazování
chtěla	chtít	k5eAaImAgFnS	chtít
přilákat	přilákat	k5eAaPmF	přilákat
mladé	mladý	k2eAgNnSc4d1	mladé
publikum	publikum	k1gNnSc4	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgFnPc2d1	významná
novohollywoodských	novohollywoodský	k2eAgFnPc2d1	novohollywoodský
figur	figura	k1gFnPc2	figura
navíc	navíc	k6eAd1	navíc
otevřeně	otevřeně	k6eAd1	otevřeně
přiznalo	přiznat	k5eAaPmAgNnS	přiznat
užívání	užívání	k1gNnSc1	užívání
drog	droga	k1gFnPc2	droga
jako	jako	k8xC	jako
například	například	k6eAd1	například
LSD	LSD	kA	LSD
a	a	k8xC	a
marihuany	marihuana	k1gFnSc2	marihuana
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
filmem	film	k1gInSc7	film
pro	pro	k7c4	pro
nastupující	nastupující	k2eAgFnSc4d1	nastupující
generaci	generace	k1gFnSc4	generace
Nového	Nového	k2eAgInSc2d1	Nového
Hollywoodu	Hollywood	k1gInSc2	Hollywood
byl	být	k5eAaImAgInS	být
snímek	snímek	k1gInSc1	snímek
Bonnie	Bonnie	k1gFnSc2	Bonnie
a	a	k8xC	a
Clyde	Clyd	k1gInSc5	Clyd
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
produkoval	produkovat	k5eAaImAgMnS	produkovat
Warren	Warrna	k1gFnPc2	Warrna
Beatty	Beatta	k1gFnSc2	Beatta
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
i	i	k9	i
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
do	do	k7c2	do
filmové	filmový	k2eAgFnSc2d1	filmová
historie	historie	k1gFnSc2	historie
kombinací	kombinace	k1gFnPc2	kombinace
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
sexu	sex	k1gInSc2	sex
i	i	k8xC	i
humoru	humor	k1gInSc2	humor
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
téma	téma	k1gNnSc4	téma
odpadlého	odpadlý	k2eAgNnSc2d1	odpadlé
mládí	mládí	k1gNnSc2	mládí
bylo	být	k5eAaImAgNnS	být
přitažlivé	přitažlivý	k2eAgNnSc1d1	přitažlivé
pro	pro	k7c4	pro
mladé	mladý	k2eAgNnSc4d1	mladé
publikum	publikum	k1gNnSc4	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
explicitně	explicitně	k6eAd1	explicitně
portrétuje	portrétovat	k5eAaImIp3nS	portrétovat
násilí	násilí	k1gNnSc4	násilí
a	a	k8xC	a
nejednoznačnost	nejednoznačnost	k1gFnSc4	nejednoznačnost
v	v	k7c6	v
nahlížení	nahlížení	k1gNnSc6	nahlížení
na	na	k7c4	na
tradiční	tradiční	k2eAgFnPc4d1	tradiční
morální	morální	k2eAgFnPc4d1	morální
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
se	s	k7c7	s
šokujícím	šokující	k2eAgInSc7d1	šokující
závěrem	závěr	k1gInSc7	závěr
<g/>
,	,	kIx,	,
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
jedni	jeden	k4xCgMnPc1	jeden
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
násilí	násilí	k1gNnSc4	násilí
ve	v	k7c6	v
filmu	film	k1gInSc6	film
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
možný	možný	k2eAgInSc1d1	možný
negativní	negativní	k2eAgInSc1d1	negativní
vliv	vliv	k1gInSc1	vliv
a	a	k8xC	a
dopad	dopad	k1gInSc1	dopad
na	na	k7c4	na
obecenstvo	obecenstvo	k1gNnSc4	obecenstvo
<g/>
,	,	kIx,	,
druzí	druhý	k4xOgMnPc1	druhý
oceňovali	oceňovat	k5eAaImAgMnP	oceňovat
umělecké	umělecký	k2eAgNnSc4d1	umělecké
stylistické	stylistický	k2eAgNnSc4d1	stylistické
provedení	provedení	k1gNnSc4	provedení
a	a	k8xC	a
tematizování	tematizování	k1gNnSc4	tematizování
násilí	násilí	k1gNnSc2	násilí
jakožto	jakožto	k8xS	jakožto
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
charakterových	charakterový	k2eAgInPc2d1	charakterový
rysů	rys	k1gInPc2	rys
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
otiskl	otisknout	k5eAaPmAgInS	otisknout
časopis	časopis	k1gInSc1	časopis
Time	Time	k1gFnSc4	Time
oslavný	oslavný	k2eAgInSc1d1	oslavný
článek	článek	k1gInSc1	článek
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
psalo	psát	k5eAaImAgNnS	psát
se	se	k3xPyFc4	se
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
americké	americký	k2eAgFnSc3d1	americká
nové	nový	k2eAgFnSc3d1	nová
vlně	vlna	k1gFnSc3	vlna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vlivný	vlivný	k2eAgInSc1d1	vlivný
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
napsal	napsat	k5eAaPmAgMnS	napsat
Stefan	Stefan	k1gMnSc1	Stefan
Kanfer	Kanfer	k1gMnSc1	Kanfer
<g/>
,	,	kIx,	,
označoval	označovat	k5eAaImAgInS	označovat
snímek	snímek	k1gInSc1	snímek
Bonnie	Bonnie	k1gFnSc2	Bonnie
a	a	k8xC	a
Clyde	Clyd	k1gInSc5	Clyd
za	za	k7c4	za
revoluční	revoluční	k2eAgInSc4d1	revoluční
a	a	k8xC	a
přelomový	přelomový	k2eAgInSc4d1	přelomový
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
znejasňuje	znejasňovat	k5eAaImIp3nS	znejasňovat
žánrové	žánrový	k2eAgNnSc4d1	žánrové
ukotvení	ukotvení	k1gNnSc4	ukotvení
a	a	k8xC	a
nedbá	nedbat	k5eAaImIp3nS	nedbat
zavedených	zavedený	k2eAgInPc2d1	zavedený
a	a	k8xC	a
osvědčených	osvědčený	k2eAgInPc2d1	osvědčený
aspektů	aspekt	k1gInPc2	aspekt
vyprávění	vyprávění	k1gNnSc2	vyprávění
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
motivací	motivace	k1gFnSc7	motivace
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
tedy	tedy	k9	tedy
snímek	snímek	k1gInSc1	snímek
signalizuje	signalizovat	k5eAaImIp3nS	signalizovat
nový	nový	k2eAgInSc1d1	nový
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
nový	nový	k2eAgInSc1d1	nový
trend	trend	k1gInSc1	trend
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
pozitivní	pozitivní	k2eAgNnSc4d1	pozitivní
hodnocení	hodnocení	k1gNnSc4	hodnocení
od	od	k7c2	od
mnohých	mnohý	k2eAgMnPc2d1	mnohý
kritiků	kritik	k1gMnPc2	kritik
<g/>
,	,	kIx,	,
umožnil	umožnit	k5eAaPmAgInS	umožnit
filmu	film	k1gInSc3	film
znovu-uvedení	znovuvedení	k1gNnSc2	znovu-uvedení
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
a	a	k8xC	a
zajistil	zajistit	k5eAaPmAgInS	zajistit
tak	tak	k9	tak
jeho	jeho	k3xOp3gInSc1	jeho
značný	značný	k2eAgInSc1d1	značný
komerční	komerční	k2eAgInSc1d1	komerční
úspěch	úspěch	k1gInSc1	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Enormní	enormní	k2eAgInSc4d1	enormní
vliv	vliv	k1gInSc4	vliv
tohoto	tento	k3xDgInSc2	tento
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
pro	pro	k7c4	pro
pochopení	pochopení	k1gNnSc4	pochopení
celého	celý	k2eAgNnSc2d1	celé
následujícího	následující	k2eAgNnSc2d1	následující
období	období	k1gNnSc2	období
Nového	Nového	k2eAgInSc2d1	Nového
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
počáteční	počáteční	k2eAgInSc1d1	počáteční
úspěch	úspěch	k1gInSc1	úspěch
vydláždil	vydláždit	k5eAaPmAgInS	vydláždit
cestu	cesta	k1gFnSc4	cesta
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
studia	studio	k1gNnSc2	studio
nakonec	nakonec	k6eAd1	nakonec
přenechala	přenechat	k5eAaPmAgFnS	přenechat
téměř	téměř	k6eAd1	téměř
veškerou	veškerý	k3xTgFnSc4	veškerý
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
kontrolu	kontrola	k1gFnSc4	kontrola
inovativním	inovativní	k2eAgNnSc7d1	inovativní
a	a	k8xC	a
progresivním	progresivní	k2eAgMnPc3d1	progresivní
mladým	mladý	k2eAgMnPc3d1	mladý
filmařům	filmař	k1gMnPc3	filmař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
velmi	velmi	k6eAd1	velmi
osobité	osobitý	k2eAgInPc1d1	osobitý
a	a	k8xC	a
udivující	udivující	k2eAgInPc1d1	udivující
filmy	film	k1gInPc1	film
jako	jako	k8xC	jako
například	například	k6eAd1	například
Psí	psí	k2eAgNnSc1d1	psí
odpoledne	odpoledne	k1gNnSc1	odpoledne
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
čtvrť	čtvrť	k1gFnSc1	čtvrť
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Taxikář	taxikář	k1gMnSc1	taxikář
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
zaznamenaly	zaznamenat	k5eAaPmAgInP	zaznamenat
enormní	enormní	k2eAgInSc4d1	enormní
kritický	kritický	k2eAgInSc4d1	kritický
i	i	k8xC	i
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úspěch	úspěch	k1gInSc1	úspěch
prominentních	prominentní	k2eAgMnPc2d1	prominentní
filmařů	filmař	k1gMnPc2	filmař
Nového	Nového	k2eAgInSc2d1	Nového
Hollywoodu	Hollywood	k1gInSc2	Hollywood
vedl	vést	k5eAaImAgMnS	vést
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
k	k	k7c3	k
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
více	hodně	k6eAd2	hodně
a	a	k8xC	a
více	hodně	k6eAd2	hodně
extravagantním	extravagantní	k2eAgInPc3d1	extravagantní
a	a	k8xC	a
přehnaným	přehnaný	k2eAgInPc3d1	přehnaný
požadavkům	požadavek	k1gInPc3	požadavek
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
vůči	vůči	k7c3	vůči
studiím	studie	k1gFnPc3	studie
a	a	k8xC	a
jednak	jednak	k8xC	jednak
vůči	vůči	k7c3	vůči
divákům	divák	k1gMnPc3	divák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
retrospektivě	retrospektiva	k1gFnSc6	retrospektiva
snímky	snímek	k1gInPc4	snímek
Čelisti	čelist	k1gFnSc2	čelist
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
a	a	k8xC	a
Star	Star	kA	Star
Wars	Wars	k1gInSc1	Wars
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
zřetelně	zřetelně	k6eAd1	zřetelně
naznačovaly	naznačovat	k5eAaImAgFnP	naznačovat
začátek	začátek	k1gInSc4	začátek
konce	konec	k1gInSc2	konec
éry	éra	k1gFnSc2	éra
Nového	Nového	k2eAgInSc2d1	Nového
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
bezprecedentním	bezprecedentní	k2eAgInSc7d1	bezprecedentní
finančním	finanční	k2eAgInSc7d1	finanční
úspěchem	úspěch	k1gInSc7	úspěch
blockbustery	blockbuster	k1gInPc4	blockbuster
Stevena	Steven	k2eAgFnSc1d1	Stevena
Spielberga	Spielberga	k1gFnSc1	Spielberga
a	a	k8xC	a
George	George	k1gInSc4	George
Lucase	Lucasa	k1gFnSc3	Lucasa
změnily	změnit	k5eAaPmAgInP	změnit
paradigma	paradigma	k1gNnSc4	paradigma
Hollywoodské	hollywoodský	k2eAgFnSc2d1	hollywoodská
továrny	továrna	k1gFnSc2	továrna
na	na	k7c4	na
sny	sen	k1gInPc4	sen
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
filmy	film	k1gInPc1	film
daly	dát	k5eAaPmAgInP	dát
studiím	studie	k1gFnPc3	studie
návod	návod	k1gInSc4	návod
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
opět	opět	k6eAd1	opět
vydělávat	vydělávat	k5eAaImF	vydělávat
peníze	peníz	k1gInPc4	peníz
ve	v	k7c6	v
změněném	změněný	k2eAgMnSc6d1	změněný
kulturním	kulturní	k2eAgMnSc6d1	kulturní
a	a	k8xC	a
obchodním	obchodní	k2eAgNnSc6d1	obchodní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInSc1d2	veliký
důraz	důraz	k1gInSc1	důraz
Hollywoodských	hollywoodský	k2eAgNnPc2d1	hollywoodské
studií	studio	k1gNnPc2	studio
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
merchandising	merchandising	k1gInSc4	merchandising
(	(	kIx(	(
<g/>
např.	např.	kA	např.
napojení	napojení	k1gNnSc4	napojení
na	na	k7c6	na
hračkářství	hračkářství	k1gNnSc6	hračkářství
<g/>
)	)	kIx)	)
než	než	k8xS	než
dosud	dosud	k6eAd1	dosud
<g/>
,	,	kIx,	,
pronikání	pronikání	k1gNnSc1	pronikání
do	do	k7c2	do
dalších	další	k2eAgNnPc2d1	další
médií	médium	k1gNnPc2	médium
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
vydávání	vydávání	k1gNnSc1	vydávání
soundtracků	soundtrack	k1gInPc2	soundtrack
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
sequelů	sequel	k1gInPc2	sequel
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pokračování	pokračování	k1gNnSc1	pokračování
filmů	film	k1gInPc2	film
a	a	k8xC	a
vytváření	vytváření	k1gNnSc2	vytváření
úspěšných	úspěšný	k2eAgFnPc2d1	úspěšná
sérií	série	k1gFnPc2	série
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
všechno	všechen	k3xTgNnSc1	všechen
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
studiím	studie	k1gFnPc3	studie
opětovně	opětovně	k6eAd1	opětovně
vydělávat	vydělávat	k5eAaImF	vydělávat
na	na	k7c6	na
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
vrcholní	vrcholný	k2eAgMnPc1d1	vrcholný
představitelé	představitel	k1gMnPc1	představitel
globálních	globální	k2eAgFnPc2d1	globální
korporací	korporace	k1gFnPc2	korporace
uvědomili	uvědomit	k5eAaPmAgMnP	uvědomit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
moc	moc	k6eAd1	moc
peněz	peníze	k1gInPc2	peníze
lze	lze	k6eAd1	lze
potenciálně	potenciálně	k6eAd1	potenciálně
vydělat	vydělat	k5eAaPmF	vydělat
na	na	k7c6	na
výrobě	výroba	k1gFnSc6	výroba
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
tak	tak	k9	tak
tyto	tento	k3xDgFnPc1	tento
globální	globální	k2eAgFnPc1d1	globální
společnosti	společnost	k1gFnPc1	společnost
začaly	začít	k5eAaPmAgFnP	začít
postupně	postupně	k6eAd1	postupně
skupovat	skupovat	k5eAaImF	skupovat
hollywoodská	hollywoodský	k2eAgNnPc4d1	hollywoodské
studia	studio	k1gNnPc4	studio
<g/>
.	.	kIx.	.
</s>
<s>
Korporátní	Korporátní	k2eAgFnSc1d1	Korporátní
mentalita	mentalita	k1gFnSc1	mentalita
těchto	tento	k3xDgFnPc2	tento
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vnesla	vnést	k5eAaPmAgFnS	vnést
do	do	k7c2	do
amerického	americký	k2eAgInSc2d1	americký
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
idiosynkratické	idiosynkratický	k2eAgInPc4d1	idiosynkratický
filmy	film	k1gInPc4	film
odvážných	odvážný	k2eAgMnPc2d1	odvážný
filmařů	filmař	k1gMnPc2	filmař
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
posílila	posílit	k5eAaPmAgFnS	posílit
výrobu	výroba	k1gFnSc4	výroba
ideologicky	ideologicky	k6eAd1	ideologicky
méně	málo	k6eAd2	málo
konfliktních	konfliktní	k2eAgFnPc2d1	konfliktní
a	a	k8xC	a
finančně	finančně	k6eAd1	finančně
méně	málo	k6eAd2	málo
riskantnějších	riskantní	k2eAgInPc2d2	riskantnější
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Konečná	konečný	k2eAgFnSc1d1	konečná
demise	demise	k1gFnSc1	demise
Nového	Nového	k2eAgInSc2d1	Nového
Hollywoodu	Hollywood	k1gInSc2	Hollywood
přišla	přijít	k5eAaPmAgFnS	přijít
po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
finančních	finanční	k2eAgInPc2d1	finanční
propadáků	propadák	k1gInPc2	propadák
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mnoho	mnoho	k6eAd1	mnoho
kritiků	kritik	k1gMnPc2	kritik
označovalo	označovat	k5eAaImAgNnS	označovat
jako	jako	k8xS	jako
sebestředné	sebestředný	k2eAgInPc4d1	sebestředný
a	a	k8xC	a
excesivní	excesivní	k2eAgInPc4d1	excesivní
snímky	snímek	k1gInPc4	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Významní	významný	k2eAgMnPc1d1	významný
tvůrci	tvůrce	k1gMnPc1	tvůrce
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
těšili	těšit	k5eAaImAgMnP	těšit
bezprecedentní	bezprecedentní	k2eAgFnSc3d1	bezprecedentní
umělecké	umělecký	k2eAgFnSc3d1	umělecká
kontrole	kontrola	k1gFnSc3	kontrola
svých	svůj	k3xOyFgInPc2	svůj
filmů	film	k1gInPc2	film
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
neomezenému	omezený	k2eNgInSc3d1	neomezený
rozpočtu	rozpočet	k1gInSc3	rozpočet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
takové	takový	k3xDgInPc4	takový
finanční	finanční	k2eAgInPc4d1	finanční
propadáky	propadák	k1gInPc4	propadák
jako	jako	k8xS	jako
například	například	k6eAd1	například
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Nebeská	nebeský	k2eAgFnSc1d1	nebeská
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
způsobily	způsobit	k5eAaPmAgInP	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
studia	studio	k1gNnSc2	studio
opětovně	opětovně	k6eAd1	opětovně
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
svojí	svojit	k5eAaImIp3nP	svojit
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
produkcí	produkce	k1gFnSc7	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Výstřednosti	výstřednost	k1gFnPc1	výstřednost
tvůrců	tvůrce	k1gMnPc2	tvůrce
Nového	Nového	k2eAgInSc2d1	Nového
Hollywoodu	Hollywood	k1gInSc2	Hollywood
kulminovaly	kulminovat	k5eAaImAgInP	kulminovat
ve	v	k7c4	v
dvě	dva	k4xCgFnPc4	dva
naprosté	naprostý	k2eAgFnPc4d1	naprostá
finanční	finanční	k2eAgFnPc4d1	finanční
katastrofy	katastrofa	k1gFnPc4	katastrofa
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
byly	být	k5eAaImAgFnP	být
filmy	film	k1gInPc4	film
Nebeská	nebeský	k2eAgFnSc1d1	nebeská
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
Michaela	Michael	k1gMnSc4	Michael
Cimina	Cimin	k1gMnSc4	Cimin
a	a	k8xC	a
One	One	k1gMnSc4	One
from	from	k6eAd1	from
the	the	k?	the
Heart	Heart	k1gInSc1	Heart
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Francise	Francise	k1gFnSc2	Francise
Forda	ford	k1gMnSc4	ford
Coppoly	Coppola	k1gFnSc2	Coppola
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
astronomicky	astronomicky	k6eAd1	astronomicky
překročil	překročit	k5eAaPmAgMnS	překročit
rozpočet	rozpočet	k1gInSc4	rozpočet
kvůli	kvůli	k7c3	kvůli
přehnaným	přehnaný	k2eAgInPc3d1	přehnaný
požadavkům	požadavek	k1gInPc3	požadavek
režiséra	režisér	k1gMnSc2	režisér
<g/>
,	,	kIx,	,
snímek	snímek	k1gInSc4	snímek
Nebeská	nebeský	k2eAgFnSc1d1	nebeská
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
celá	celý	k2eAgFnSc1d1	celá
produkce	produkce	k1gFnSc1	produkce
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
44	[number]	k4	44
miliónů	milión	k4xCgInPc2	milión
USD	USD	kA	USD
<g/>
,	,	kIx,	,
vydělal	vydělat	k5eAaPmAgInS	vydělat
v	v	k7c6	v
pokladnách	pokladna	k1gFnPc6	pokladna
kin	kino	k1gNnPc2	kino
pouze	pouze	k6eAd1	pouze
3,5	[number]	k4	3,5
miliónu	milión	k4xCgInSc2	milión
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
finanční	finanční	k2eAgFnSc1d1	finanční
ztráta	ztráta	k1gFnSc1	ztráta
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
studio	studio	k1gNnSc4	studio
United	United	k1gMnSc1	United
Artists	Artistsa	k1gFnPc2	Artistsa
tak	tak	k9	tak
obrovská	obrovský	k2eAgFnSc1d1	obrovská
<g/>
,	,	kIx,	,
že	že	k8xS	že
přivedla	přivést	k5eAaPmAgFnS	přivést
studio	studio	k1gNnSc4	studio
k	k	k7c3	k
bankrotu	bankrot	k1gInSc3	bankrot
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
společnost	společnost	k1gFnSc1	společnost
koupilo	koupit	k5eAaPmAgNnS	koupit
MGM	MGM	kA	MGM
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
filmu	film	k1gInSc2	film
Michael	Michael	k1gMnSc1	Michael
Cimino	Cimino	k1gNnSc4	Cimino
natočil	natočit	k5eAaBmAgMnS	natočit
další	další	k2eAgInSc4d1	další
film	film	k1gInSc4	film
až	až	k9	až
po	po	k7c6	po
dlouhých	dlouhý	k2eAgNnPc6d1	dlouhé
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Coppola	Coppola	k1gFnSc1	Coppola
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
filmu	film	k1gInSc2	film
Apocalypsa	Apocalypsa	k1gFnSc1	Apocalypsa
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
finančně	finančně	k6eAd1	finančně
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
veškerý	veškerý	k3xTgInSc1	veškerý
kapitál	kapitál	k1gInSc1	kapitál
vydělaný	vydělaný	k2eAgInSc1d1	vydělaný
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
filmu	film	k1gInSc6	film
dal	dát	k5eAaPmAgInS	dát
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
filmové	filmový	k2eAgFnSc2d1	filmová
společnosti	společnost	k1gFnSc2	společnost
American	American	k1gMnSc1	American
Zoetrope	Zoetrop	k1gInSc5	Zoetrop
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
vsadil	vsadit	k5eAaPmAgMnS	vsadit
všechno	všechen	k3xTgNnSc4	všechen
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
další	další	k2eAgInSc4d1	další
velký	velký	k2eAgInSc4d1	velký
projekt	projekt	k1gInSc4	projekt
One	One	k1gMnSc1	One
from	from	k1gMnSc1	from
the	the	k?	the
Heart	Heart	k1gInSc1	Heart
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
však	však	k9	však
posléze	posléze	k6eAd1	posléze
utržil	utržit	k5eAaPmAgInS	utržit
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
pouhých	pouhý	k2eAgFnPc2d1	pouhá
636	[number]	k4	636
tisíc	tisíc	k4xCgInPc2	tisíc
USD	USD	kA	USD
<g/>
,	,	kIx,	,
nepatrný	nepatrný	k2eAgInSc4d1	nepatrný
zlomek	zlomek	k1gInSc4	zlomek
produkčních	produkční	k2eAgInPc2d1	produkční
nákladů	náklad	k1gInPc2	náklad
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
26	[number]	k4	26
miliónů	milión	k4xCgInPc2	milión
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
udržel	udržet	k5eAaPmAgInS	udržet
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
pouhý	pouhý	k2eAgInSc4d1	pouhý
týden	týden	k1gInSc4	týden
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
přivedl	přivést	k5eAaPmAgMnS	přivést
k	k	k7c3	k
bankrotu	bankrot	k1gInSc3	bankrot
jak	jak	k8xS	jak
Coppolu	Coppol	k1gInSc3	Coppol
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jeho	jeho	k3xOp3gNnSc1	jeho
začínající	začínající	k2eAgNnSc1d1	začínající
studio	studio	k1gNnSc1	studio
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
finanční	finanční	k2eAgFnSc6d1	finanční
katastrofě	katastrofa	k1gFnSc6	katastrofa
se	se	k3xPyFc4	se
filmu	film	k1gInSc6	film
začalo	začít	k5eAaPmAgNnS	začít
vtipně	vtipně	k6eAd1	vtipně
přezdívat	přezdívat	k5eAaImF	přezdívat
"	"	kIx"	"
<g/>
One	One	k1gFnSc1	One
Through	Through	k1gInSc1	Through
the	the	k?	the
Heart	Heart	k1gInSc1	Heart
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
zářivé	zářivý	k2eAgInPc1d1	zářivý
příklady	příklad	k1gInPc1	příklad
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
finančními	finanční	k2eAgInPc7d1	finanční
propadáky	propadák	k1gInPc7	propadák
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
invenční	invenční	k2eAgFnSc1d1	invenční
a	a	k8xC	a
riskantní	riskantní	k2eAgFnSc1d1	riskantní
strategie	strategie	k1gFnSc1	strategie
postoupení	postoupení	k1gNnSc2	postoupení
veškeré	veškerý	k3xTgFnSc2	veškerý
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
filmovými	filmový	k2eAgInPc7d1	filmový
projekty	projekt	k1gInPc7	projekt
režisérům	režisér	k1gMnPc3	režisér
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
éra	éra	k1gFnSc1	éra
Nového	Nového	k2eAgInSc2d1	Nového
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
New	New	k1gFnSc2	New
Hollywood	Hollywood	k1gInSc1	Hollywood
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
