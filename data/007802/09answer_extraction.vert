<s>
období	období	k1gNnSc4	období
americké	americký	k2eAgFnSc2d1	americká
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
mezi	mezi	k7c7	mezi
pozdními	pozdní	k2eAgNnPc7d1	pozdní
60	[number]	k4	60
<g/>
.	.	kIx.	.
lety	léto	k1gNnPc7	léto
(	(	kIx(	(
<g/>
Bonnie	Bonnie	k1gFnSc1	Bonnie
a	a	k8xC	a
Clyde	Clyd	k1gInSc5	Clyd
<g/>
)	)	kIx)	)
a	a	k8xC	a
ranými	raný	k2eAgNnPc7d1	rané
80	[number]	k4	80
<g/>
.	.	kIx.	.
lety	léto	k1gNnPc7	léto
(	(	kIx(	(
<g/>
Nebeská	nebeský	k2eAgFnSc1d1	nebeská
brána	brána	k1gFnSc1	brána
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
stala	stát	k5eAaPmAgFnS	stát
prominentní	prominentní	k2eAgFnSc1d1	prominentní
nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
mladých	mladý	k2eAgMnPc2d1	mladý
filmařů	filmař	k1gMnPc2	filmař
</s>
