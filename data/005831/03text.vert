<s>
Patrick	Patrick	k1gMnSc1	Patrick
Wayne	Wayn	k1gInSc5	Wayn
Swayze	Swayza	k1gFnSc6	Swayza
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1952	[number]	k4	1952
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
tanečník	tanečník	k1gMnSc1	tanečník
a	a	k8xC	a
kovboj	kovboj	k1gMnSc1	kovboj
<g/>
,	,	kIx,	,
taneční	taneční	k2eAgFnSc1d1	taneční
hvězda	hvězda	k1gFnSc1	hvězda
filmového	filmový	k2eAgNnSc2d1	filmové
nebe	nebe	k1gNnSc2	nebe
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
umělecké	umělecký	k2eAgFnSc2d1	umělecká
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Patsy	Patsa	k1gFnSc2	Patsa
Swayze	Swayze	k1gFnSc2	Swayze
byla	být	k5eAaImAgFnS	být
profesionální	profesionální	k2eAgFnSc1d1	profesionální
tanečnice	tanečnice	k1gFnSc1	tanečnice
<g/>
,	,	kIx,	,
choreografka	choreografka	k1gFnSc1	choreografka
a	a	k8xC	a
taneční	taneční	k2eAgFnSc1d1	taneční
pedagožka	pedagožka	k1gFnSc1	pedagožka
<g/>
.	.	kIx.	.
</s>
<s>
Českým	český	k2eAgMnPc3d1	český
filmovým	filmový	k2eAgMnPc3d1	filmový
divákům	divák	k1gMnPc3	divák
je	být	k5eAaImIp3nS	být
Patrick	Patrick	k1gMnSc1	Patrick
Swayze	Swayze	k1gFnSc2	Swayze
patrně	patrně	k6eAd1	patrně
ponejvíce	ponejvíce	k6eAd1	ponejvíce
znám	znát	k5eAaImIp1nS	znát
ze	z	k7c2	z
snímku	snímek	k1gInSc2	snímek
Hříšný	hříšný	k2eAgInSc4d1	hříšný
tanec	tanec	k1gInSc4	tanec
a	a	k8xC	a
z	z	k7c2	z
filmu	film	k1gInSc2	film
Duch	duch	k1gMnSc1	duch
<g/>
.	.	kIx.	.
</s>
<s>
Sourozenci	sourozenec	k1gMnPc1	sourozenec
Swayzovi	Swayzův	k2eAgMnPc1d1	Swayzův
tančili	tančit	k5eAaImAgMnP	tančit
spolu	spolu	k6eAd1	spolu
v	v	k7c6	v
taneční	taneční	k2eAgFnSc6d1	taneční
škole	škola	k1gFnSc6	škola
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
(	(	kIx(	(
<g/>
Patricie	Patricie	k1gFnSc1	Patricie
Swayzové	Swayzová	k1gFnSc2	Swayzová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
matce	matka	k1gFnSc3	matka
tancoval	tancovat	k5eAaImAgInS	tancovat
od	od	k7c2	od
útlého	útlý	k2eAgNnSc2d1	útlé
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
tanec	tanec	k1gInSc1	tanec
posléze	posléze	k6eAd1	posléze
studoval	studovat	k5eAaImAgInS	studovat
i	i	k9	i
na	na	k7c6	na
baletních	baletní	k2eAgFnPc6d1	baletní
školách	škola	k1gFnPc6	škola
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Profesionálně	profesionálně	k6eAd1	profesionálně
tančit	tančit	k5eAaImF	tančit
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
zájezdovém	zájezdový	k2eAgNnSc6d1	zájezdové
divadle	divadlo	k1gNnSc6	divadlo
Disney	Disnea	k1gFnSc2	Disnea
on	on	k3xPp3gMnSc1	on
Parade	Parad	k1gInSc5	Parad
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
tak	tak	k9	tak
jako	jako	k9	jako
mnoho	mnoho	k4c1	mnoho
jiných	jiný	k2eAgMnPc2d1	jiný
amerických	americký	k2eAgMnPc2d1	americký
herců	herec	k1gMnPc2	herec
a	a	k8xC	a
tanečníků	tanečník	k1gMnPc2	tanečník
posléze	posléze	k6eAd1	posléze
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
newyorské	newyorský	k2eAgFnSc6d1	newyorská
Broadwayi	Broadway	k1gFnSc6	Broadway
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
vystupovat	vystupovat	k5eAaImF	vystupovat
v	v	k7c6	v
amerických	americký	k2eAgInPc6d1	americký
televizních	televizní	k2eAgInPc6d1	televizní
seriálech	seriál	k1gInPc6	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
americké	americký	k2eAgFnPc4d1	americká
herecké	herecký	k2eAgFnPc4d1	herecká
hvězdy	hvězda	k1gFnPc4	hvězda
jej	on	k3xPp3gMnSc4	on
vynesl	vynést	k5eAaPmAgInS	vynést
právě	právě	k9	právě
film	film	k1gInSc1	film
Hříšný	hříšný	k2eAgInSc1d1	hříšný
tanec	tanec	k1gInSc4	tanec
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
diagnostikována	diagnostikován	k2eAgFnSc1d1	diagnostikována
rakovina	rakovina	k1gFnSc1	rakovina
slinivky	slinivka	k1gFnSc2	slinivka
břišní	břišní	k2eAgFnSc2d1	břišní
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
chemoterapii	chemoterapie	k1gFnSc4	chemoterapie
a	a	k8xC	a
nové	nový	k2eAgInPc4d1	nový
terapeutické	terapeutický	k2eAgInPc4d1	terapeutický
postupy	postup	k1gInPc4	postup
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2009	[number]	k4	2009
v	v	k7c6	v
losangelském	losangelský	k2eAgNnSc6d1	losangelské
rodinném	rodinný	k2eAgNnSc6d1	rodinné
sídle	sídlo	k1gNnSc6	sídlo
na	na	k7c4	na
selhání	selhání	k1gNnSc4	selhání
základních	základní	k2eAgFnPc2d1	základní
životních	životní	k2eAgFnPc2d1	životní
funkcí	funkce	k1gFnPc2	funkce
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Patsy	Patsa	k1gFnPc1	Patsa
Helen	Helena	k1gFnPc2	Helena
Swayze	Swayze	k1gFnSc2	Swayze
-	-	kIx~	-
matka	matka	k1gFnSc1	matka
Jessie	Jessie	k1gFnSc2	Jessie
Wayne	Wayn	k1gInSc5	Wayn
Swayze	Swayze	k1gFnSc1	Swayze
-	-	kIx~	-
otec	otec	k1gMnSc1	otec
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
57	[number]	k4	57
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
Vicky	Vicka	k1gFnSc2	Vicka
Lynn	Lynn	k1gInSc1	Lynn
-	-	kIx~	-
sestra	sestra	k1gFnSc1	sestra
(	(	kIx(	(
<g/>
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
)	)	kIx)	)
Donald	Donald	k1gMnSc1	Donald
Carl	Carl	k1gMnSc1	Carl
-	-	kIx~	-
bratr	bratr	k1gMnSc1	bratr
Sean	Sean	k1gMnSc1	Sean
Kyle	Kyle	k1gFnSc1	Kyle
-	-	kIx~	-
bratr	bratr	k1gMnSc1	bratr
Bambi	Bamb	k1gFnSc2	Bamb
(	(	kIx(	(
<g/>
Bo	Bo	k?	Bo
Ra	ra	k0	ra
Song	song	k1gInSc4	song
<g/>
)	)	kIx)	)
-	-	kIx~	-
sestra	sestra	k1gFnSc1	sestra
(	(	kIx(	(
<g/>
Korejka	Korejka	k1gFnSc1	Korejka
<g/>
,	,	kIx,	,
adoptovaná	adoptovaný	k2eAgFnSc1d1	adoptovaná
<g/>
)	)	kIx)	)
Patrick	Patrick	k1gMnSc1	Patrick
nebyl	být	k5eNaImAgMnS	být
podle	podle	k7c2	podle
dostupných	dostupný	k2eAgFnPc2d1	dostupná
ani	ani	k8xC	ani
reálných	reálný	k2eAgFnPc2d1	reálná
informací	informace	k1gFnPc2	informace
podobný	podobný	k2eAgInSc1d1	podobný
někomu	někdo	k3yInSc3	někdo
z	z	k7c2	z
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Povahově	povahově	k6eAd1	povahově
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
<g/>
.	.	kIx.	.
</s>
<s>
Patrick	Patrick	k1gMnSc1	Patrick
Swayze	Swayze	k1gFnSc2	Swayze
byl	být	k5eAaImAgMnS	být
nadaný	nadaný	k2eAgMnSc1d1	nadaný
člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
jeho	jeho	k3xOp3gFnSc2	jeho
slávy	sláva	k1gFnSc2	sláva
i	i	k9	i
nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
hercem	herec	k1gMnSc7	herec
<g/>
/	/	kIx~	/
<g/>
tanečníkem	tanečník	k1gMnSc7	tanečník
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
The	The	k1gMnSc1	The
Beast	Beast	k1gMnSc1	Beast
(	(	kIx(	(
<g/>
Bestie	bestie	k1gFnSc1	bestie
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Charles	Charles	k1gMnSc1	Charles
Barker	Barker	k1gMnSc1	Barker
2008	[number]	k4	2008
Powder	Powdero	k1gNnPc2	Powdero
Blue	Blue	k1gFnPc2	Blue
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Velvet	Velvet	k1gInSc1	Velvet
Larry	Larra	k1gFnSc2	Larra
2007	[number]	k4	2007
Christmas	Christmas	k1gMnSc1	Christmas
in	in	k?	in
Wonderland	Wonderland	k1gInSc1	Wonderland
-	-	kIx~	-
role	role	k1gFnSc1	role
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Wayne	Wayn	k1gInSc5	Wayn
Saunders	Saunders	k1gInSc4	Saunders
2007	[number]	k4	2007
Jump	Jump	k1gInSc1	Jump
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Richard	Richard	k1gMnSc1	Richard
Pressburger	Pressburger	k1gMnSc1	Pressburger
2006	[number]	k4	2006
The	The	k1gFnPc2	The
Fox	fox	k1gInSc4	fox
and	and	k?	and
the	the	k?	the
Hound	Hound	k1gInSc1	Hound
2	[number]	k4	2
-	-	kIx~	-
dabing	dabing	k1gInSc4	dabing
2005	[number]	k4	2005
Ikona	ikona	k1gFnSc1	ikona
(	(	kIx(	(
<g/>
Icon	Icon	k1gInSc1	Icon
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Jason	Jason	k1gMnSc1	Jason
Monk	Monk	k1gInSc4	Monk
2005	[number]	k4	2005
Univerzální	univerzální	k2eAgFnSc1d1	univerzální
uklízečka	uklízečka	k1gFnSc1	uklízečka
(	(	kIx(	(
<g/>
Keeping	Keeping	k1gInSc1	Keeping
Mum	Mum	k1gFnSc2	Mum
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Lance	lance	k1gNnSc1	lance
2004	[number]	k4	2004
<g />
.	.	kIx.	.
</s>
<s>
Hříšný	hříšný	k2eAgInSc1d1	hříšný
tanec	tanec	k1gInSc1	tanec
2	[number]	k4	2
<g/>
:	:	kIx,	:
Havana	Havana	k1gFnSc1	Havana
night	night	k1gMnSc1	night
-	-	kIx~	-
taneční	taneční	k2eAgMnSc1d1	taneční
instruktor	instruktor	k1gMnSc1	instruktor
2004	[number]	k4	2004
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
Jiřím	Jiří	k1gMnSc6	Jiří
a	a	k8xC	a
drakovi	drak	k1gMnSc6	drak
(	(	kIx(	(
<g/>
George	Georg	k1gFnSc2	Georg
and	and	k?	and
the	the	k?	the
Dragon	Dragon	k1gMnSc1	Dragon
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Garth	Garth	k1gInSc1	Garth
2003	[number]	k4	2003
Poslední	poslední	k2eAgInSc1d1	poslední
tanec	tanec	k1gInSc1	tanec
(	(	kIx(	(
<g/>
One	One	k1gFnSc1	One
Last	Last	k1gInSc1	Last
Dance	Danka	k1gFnSc3	Danka
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Travis	Travis	k1gFnSc1	Travis
MacPhearson	MacPhearsona	k1gFnPc2	MacPhearsona
2003	[number]	k4	2003
Zkurvená	Zkurvený	k2eAgFnSc1d1	Zkurvená
noc	noc	k1gFnSc1	noc
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Frank	Frank	k1gMnSc1	Frank
2002	[number]	k4	2002
Probuzení	probuzení	k1gNnSc1	probuzení
v	v	k7c4	v
Reno	Reno	k1gNnSc4	Reno
(	(	kIx(	(
<g/>
Waking	Waking	k1gInSc1	Waking
Up	Up	k1gMnSc1	Up
In	In	k1gMnSc1	In
Reno	Reno	k1gMnSc1	Reno
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Roy	Roy	k1gMnSc1	Roy
Kirkendall	Kirkendall	k1gMnSc1	Kirkendall
2003	[number]	k4	2003
Doly	dol	k1gInPc7	dol
krále	král	k1gMnSc2	král
Šalamouna	Šalamoun	k1gMnSc2	Šalamoun
(	(	kIx(	(
<g/>
King	King	k1gMnSc1	King
Solomon	Solomon	k1gMnSc1	Solomon
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Mines	Minesa	k1gFnPc2	Minesa
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Allan	Allan	k1gMnSc1	Allan
<g />
.	.	kIx.	.
</s>
<s>
Quatermain	Quatermain	k1gInSc1	Quatermain
2001	[number]	k4	2001
Donnie	Donnie	k1gFnSc2	Donnie
Darko	Darko	k1gNnSc1	Darko
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Jim	on	k3xPp3gMnPc3	on
Cunningham	Cunningham	k1gInSc1	Cunningham
2000	[number]	k4	2000
Lulu	lula	k1gFnSc4	lula
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
(	(	kIx(	(
<g/>
Forever	Forever	k1gInSc4	Forever
Lulu	lula	k1gFnSc4	lula
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Ben	Ben	k1gInSc1	Ben
Clifton	Clifton	k1gInSc1	Clifton
1998	[number]	k4	1998
Dopisy	dopis	k1gInPc7	dopis
od	od	k7c2	od
vraha	vrah	k1gMnSc2	vrah
(	(	kIx(	(
<g/>
Letters	Letters	k1gInSc1	Letters
From	From	k1gMnSc1	From
a	a	k8xC	a
Killer	Killer	k1gMnSc1	Killer
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Race	Race	k1gFnSc1	Race
Darnell	Darnell	k1gInSc1	Darnell
1998	[number]	k4	1998
Černý	Černý	k1gMnSc1	Černý
pes	pes	k1gMnSc1	pes
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
<g />
.	.	kIx.	.
</s>
<s>
Dog	doga	k1gFnPc2	doga
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Jack	Jack	k1gMnSc1	Jack
Crews	Crews	k1gInSc4	Crews
1993	[number]	k4	1993
Táta	táta	k1gMnSc1	táta
lump	lump	k1gMnSc1	lump
(	(	kIx(	(
<g/>
Father	Fathra	k1gFnPc2	Fathra
Hood	Hood	k1gInSc1	Hood
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Jack	Jack	k1gMnSc1	Jack
Charles	Charles	k1gMnSc1	Charles
1995	[number]	k4	1995
Tři	tři	k4xCgMnPc4	tři
muži	muž	k1gMnPc1	muž
v	v	k7c6	v
negližé	negližé	k1gNnSc6	negližé
(	(	kIx(	(
<g/>
To	to	k9	to
Wong	Wong	k1gMnSc1	Wong
Foo	Foo	k1gMnSc1	Foo
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Vida	Vida	k?	Vida
Boheme	Bohem	k1gInSc5	Bohem
1995	[number]	k4	1995
Tři	tři	k4xCgNnPc1	tři
přání	přání	k1gNnPc1	přání
(	(	kIx(	(
<g/>
Three	Three	k1gFnSc1	Three
Wishes	Wishesa	k1gFnPc2	Wishesa
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Jack	Jack	k1gMnSc1	Jack
McCloud	McCloud	k1gMnSc1	McCloud
1994	[number]	k4	1994
Machři	Machři	k?	Machři
(	(	kIx(	(
<g/>
Tall	Tall	k1gInSc1	Tall
Tale	Tal	k1gInSc2	Tal
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Pecos	Pecos	k1gMnSc1	Pecos
Bill	Bill	k1gMnSc1	Bill
1992	[number]	k4	1992
Město	město	k1gNnSc1	město
radosti	radost	k1gFnSc2	radost
(	(	kIx(	(
<g/>
City	city	k1gNnSc1	city
Of	Of	k1gFnSc2	Of
Joy	Joy	k1gFnSc2	Joy
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Max	max	kA	max
Lowe	Lowe	k1gFnSc1	Lowe
1991	[number]	k4	1991
Bod	bod	k1gInSc1	bod
zlomu	zlom	k1gInSc2	zlom
(	(	kIx(	(
<g/>
Point	pointa	k1gFnPc2	pointa
Break	break	k1gInSc1	break
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Bodhi	Bodh	k1gFnSc2	Bodh
1990	[number]	k4	1990
Duch	duch	k1gMnSc1	duch
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
Ghost	Ghost	k1gFnSc1	Ghost
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Sam	Sam	k1gMnSc1	Sam
Wheat	Wheat	k1gInSc4	Wheat
1989	[number]	k4	1989
Hrozba	hrozba	k1gFnSc1	hrozba
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
Road	Road	k1gInSc1	Road
House	house	k1gNnSc1	house
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnPc4	role
<g/>
:	:	kIx,	:
Dalton	Dalton	k1gInSc4	Dalton
1989	[number]	k4	1989
Nejbližší	blízký	k2eAgInSc4d3	Nejbližší
příbuzenstvo	příbuzenstvo	k1gNnSc1	příbuzenstvo
(	(	kIx(	(
<g/>
Next	Next	k2eAgInSc1d1	Next
of	of	k?	of
Kin	kino	k1gNnPc2	kino
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Truman	Truman	k1gMnSc1	Truman
Gates	Gates	k1gMnSc1	Gates
1988	[number]	k4	1988
<g />
.	.	kIx.	.
</s>
<s>
Tiger	Tiger	k1gMnSc1	Tiger
Warsaw	Warsaw	k1gMnSc1	Warsaw
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Chuck	Chuck	k1gInSc1	Chuck
<g/>
(	(	kIx(	(
<g/>
Tiger	Tiger	k1gInSc1	Tiger
<g/>
)	)	kIx)	)
Warsaw	Warsaw	k1gFnSc1	Warsaw
1987	[number]	k4	1987
Hříšný	hříšný	k2eAgInSc4d1	hříšný
tanec	tanec	k1gInSc4	tanec
(	(	kIx(	(
<g/>
Dirty	Dirt	k1gInPc4	Dirt
Dancing	dancing	k1gInSc1	dancing
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnPc1	role
<g/>
:	:	kIx,	:
Johnny	Johnen	k2eAgFnPc1d1	Johnna
Castle	Castle	k1gFnPc1	Castle
1987	[number]	k4	1987
Ocelový	ocelový	k2eAgInSc1d1	ocelový
meč	meč	k1gInSc1	meč
(	(	kIx(	(
<g/>
Steel	Steel	k1gMnSc1	Steel
Dawn	Dawn	k1gMnSc1	Dawn
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Nomad	Nomad	k1gInSc1	Nomad
1988	[number]	k4	1988
Swayze	Swayze	k1gFnSc2	Swayze
Dancing	dancing	k1gInSc1	dancing
-	-	kIx~	-
hraje	hrát	k5eAaImIp3nS	hrát
sám	sám	k3xTgMnSc1	sám
<g />
.	.	kIx.	.
</s>
<s>
sebe	sebe	k3xPyFc4	sebe
1986	[number]	k4	1986
Youngblood	Youngblooda	k1gFnPc2	Youngblooda
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Derek	Derek	k1gMnSc1	Derek
Sutton	Sutton	k1gInSc4	Sutton
1986	[number]	k4	1986
Sever	sever	k1gInSc4	sever
a	a	k8xC	a
Jih	jih	k1gInSc4	jih
2	[number]	k4	2
(	(	kIx(	(
<g/>
North	North	k1gInSc1	North
and	and	k?	and
South	South	k1gInSc1	South
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Orry	Orry	k1gInPc1	Orry
Main	Main	k1gNnSc1	Main
1985	[number]	k4	1985
Sever	sever	k1gInSc4	sever
a	a	k8xC	a
Jih	jih	k1gInSc4	jih
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Orry	Orry	k1gInPc1	Orry
Main	Main	k1gNnSc1	Main
1985	[number]	k4	1985
Neuvěřitelné	uvěřitelný	k2eNgInPc4d1	neuvěřitelný
příběhy	příběh	k1gInPc4	příběh
(	(	kIx(	(
<g/>
Amazing	Amazing	k1gInSc1	Amazing
Stories	Stories	k1gInSc1	Stories
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Eric	Eric	k1gInSc4	Eric
Peterson	Petersona	k1gFnPc2	Petersona
1984	[number]	k4	1984
Grandview	Grandview	k1gFnPc2	Grandview
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Ernie	Ernie	k1gFnSc1	Ernie
(	(	kIx(	(
<g/>
Slam	slam	k1gInSc1	slam
<g/>
)	)	kIx)	)
Webster	Webster	k1gInSc1	Webster
1984	[number]	k4	1984
Rudý	rudý	k2eAgInSc1d1	rudý
úsvit	úsvit	k1gInSc1	úsvit
(	(	kIx(	(
<g/>
Red	Red	k1gMnSc1	Red
Dawn	Dawn	k1gMnSc1	Dawn
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Jed	jed	k1gInSc1	jed
Eckert	Eckert	k1gInSc1	Eckert
1984	[number]	k4	1984
Offsides	Offsidesa	k1gFnPc2	Offsidesa
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Doug	Doug	k1gInSc1	Doug
Zimmer	Zimmer	k1gInSc1	Zimmer
1983	[number]	k4	1983
Sedm	sedm	k4xCc1	sedm
neohrožených	ohrožený	k2eNgFnPc2d1	neohrožená
(	(	kIx(	(
<g/>
Uncommon	Uncommona	k1gFnPc2	Uncommona
Valor	valor	k1gInSc1	valor
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Kevin	Kevin	k1gMnSc1	Kevin
Scott	Scott	k1gMnSc1	Scott
1983	[number]	k4	1983
The	The	k1gFnSc2	The
Outsiders	Outsidersa	k1gFnPc2	Outsidersa
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Darrel	Darrel	k1gInSc1	Darrel
Curtis	Curtis	k1gFnSc2	Curtis
1982	[number]	k4	1982
M	M	kA	M
<g/>
*	*	kIx~	*
<g/>
A	A	kA	A
<g/>
*	*	kIx~	*
<g/>
S	s	k7c7	s
<g/>
*	*	kIx~	*
<g/>
H	H	kA	H
-	-	kIx~	-
role	role	k1gFnPc1	role
<g/>
:	:	kIx,	:
Gary	Gar	k2eAgFnPc1d1	Gara
Sturgis	Sturgis	k1gFnPc1	Sturgis
1982	[number]	k4	1982
Odpadlíci	odpadlík	k1gMnPc1	odpadlík
(	(	kIx(	(
<g/>
Renegades	Renegades	k1gMnSc1	Renegades
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
bandita	bandita	k1gMnSc1	bandita
1982	[number]	k4	1982
The	The	k1gFnSc2	The
Comeback	Comebacka	k1gFnPc2	Comebacka
Kid	Kid	k1gMnSc1	Kid
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
role	role	k1gFnPc4	role
<g/>
:	:	kIx,	:
Chuck	Chuck	k1gInSc4	Chuck
1981	[number]	k4	1981
Návrat	návrat	k1gInSc4	návrat
rebelů	rebel	k1gMnPc2	rebel
(	(	kIx(	(
<g/>
Return	Return	k1gMnSc1	Return
of	of	k?	of
the	the	k?	the
Rebels	Rebels	k1gInSc1	Rebels
<g/>
)	)	kIx)	)
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
K.C.	K.C.	k1gMnSc1	K.C.
Barnes	Barnes	k1gMnSc1	Barnes
1979	[number]	k4	1979
Skatetown	Skatetown	k1gInSc1	Skatetown
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
<g/>
A.	A.	kA	A.
-	-	kIx~	-
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Ace	Ace	k1gMnSc1	Ace
Johnson	Johnson	k1gMnSc1	Johnson
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Patrick	Patricka	k1gFnPc2	Patricka
Swayze	Swayze	k1gFnSc2	Swayze
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
http://www.patrickswayze.net/	[url]	k?	http://www.patrickswayze.net/
http://www.donswayze.net/	[url]	k?	http://www.donswayze.net/
Osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
Patrick	Patrick	k1gMnSc1	Patrick
Swayze	Swayze	k1gFnSc2	Swayze
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Patrick	Patrick	k1gInSc1	Patrick
Swayze	Swayze	k1gFnSc2	Swayze
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
