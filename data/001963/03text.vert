<s>
Jan	Jan	k1gMnSc1	Jan
Palach	palach	k1gInSc1	palach
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1948	[number]	k4	1948
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
student	student	k1gMnSc1	student
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
politické	politický	k2eAgFnSc2d1	politická
ekonomie	ekonomie	k1gFnSc2	ekonomie
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
obětoval	obětovat	k5eAaBmAgMnS	obětovat
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
potlačování	potlačování	k1gNnSc3	potlačování
svobod	svoboda	k1gFnPc2	svoboda
a	a	k8xC	a
pasivnímu	pasivní	k2eAgInSc3d1	pasivní
přístupu	přístup	k1gInSc3	přístup
veřejnosti	veřejnost	k1gFnSc2	veřejnost
po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
Československa	Československo	k1gNnSc2	Československo
armádami	armáda	k1gFnPc7	armáda
států	stát	k1gInPc2	stát
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
se	se	k3xPyFc4	se
zapálil	zapálit	k5eAaPmAgMnS	zapálit
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
Václavského	václavský	k2eAgNnSc2d1	Václavské
náměstí	náměstí	k1gNnSc2	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozsáhlými	rozsáhlý	k2eAgFnPc7d1	rozsáhlá
popáleninami	popálenina	k1gFnPc7	popálenina
byl	být	k5eAaImAgInS	být
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
dnech	den	k1gInPc6	den
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Palach	palach	k1gInSc4	palach
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
Všetat	Všetat	k1gFnSc2	Všetat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Gymnázium	gymnázium	k1gNnSc1	gymnázium
v	v	k7c6	v
Mělníku	Mělník	k1gInSc6	Mělník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Filozofickou	filozofický	k2eAgFnSc4d1	filozofická
fakultu	fakulta	k1gFnSc4	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
nebyl	být	k5eNaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
přijat	přijmout	k5eAaPmNgMnS	přijmout
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
"	"	kIx"	"
<g/>
živnostenský	živnostenský	k2eAgInSc4d1	živnostenský
původ	původ	k1gInSc4	původ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
uspěl	uspět	k5eAaPmAgMnS	uspět
při	při	k7c6	při
přijímacích	přijímací	k2eAgFnPc6d1	přijímací
zkouškách	zkouška	k1gFnPc6	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
proto	proto	k8xC	proto
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
svůj	svůj	k3xOyFgInSc4	svůj
pokus	pokus	k1gInSc4	pokus
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
Filozofickou	filozofický	k2eAgFnSc4d1	filozofická
fakultu	fakulta	k1gFnSc4	fakulta
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1968	[number]	k4	1968
dostal	dostat	k5eAaPmAgMnS	dostat
povolení	povolení	k1gNnSc4	povolení
odcestovat	odcestovat	k5eAaPmF	odcestovat
na	na	k7c4	na
dovolenou	dovolená	k1gFnSc4	dovolená
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gMnSc4	on
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
mnohých	mnohý	k2eAgFnPc2d1	mnohá
protestních	protestní	k2eAgFnPc2d1	protestní
akcí	akce	k1gFnPc2	akce
proti	proti	k7c3	proti
okupaci	okupace	k1gFnSc3	okupace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
studentské	studentský	k2eAgFnSc2d1	studentská
stávky	stávka	k1gFnSc2	stávka
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
neviděl	vidět	k5eNaImAgMnS	vidět
žádný	žádný	k3yNgInSc4	žádný
významný	významný	k2eAgInSc4d1	významný
pokrok	pokrok	k1gInSc4	pokrok
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
začal	začít	k5eAaPmAgInS	začít
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c4	o
radikálnější	radikální	k2eAgFnSc4d2	radikálnější
akci	akce	k1gFnSc4	akce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
probrala	probrat	k5eAaPmAgFnS	probrat
společnost	společnost	k1gFnSc1	společnost
z	z	k7c2	z
rezignace	rezignace	k1gFnSc2	rezignace
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
propadala	propadat	k5eAaImAgFnS	propadat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dokumentu	dokument	k1gInSc2	dokument
Poselství	poselství	k1gNnSc2	poselství
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
byl	být	k5eAaImAgInS	být
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
velmi	velmi	k6eAd1	velmi
znepokojený	znepokojený	k2eAgInSc1d1	znepokojený
politickou	politický	k2eAgFnSc7d1	politická
situací	situace	k1gFnSc7	situace
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rozhovoru	rozhovor	k1gInSc2	rozhovor
s	s	k7c7	s
starším	starý	k2eAgMnSc7d2	starší
Palachovým	palachový	k2eAgMnSc7d1	palachový
bratrem	bratr	k1gMnSc7	bratr
Jiřím	Jiří	k1gMnSc7	Jiří
Jan	Jan	k1gMnSc1	Jan
před	před	k7c7	před
svým	svůj	k3xOyFgInSc7	svůj
činem	čin	k1gInSc7	čin
nic	nic	k6eAd1	nic
nenaznačil	naznačit	k5eNaPmAgMnS	naznačit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
domluvený	domluvený	k2eAgInSc1d1	domluvený
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
že	že	k8xS	že
půjdou	jít	k5eAaImIp3nP	jít
nakupovat	nakupovat	k5eAaBmF	nakupovat
nové	nový	k2eAgNnSc4d1	nové
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
okolo	okolo	k7c2	okolo
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
se	se	k3xPyFc4	se
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
Václavského	václavský	k2eAgNnSc2d1	Václavské
náměstí	náměstí	k1gNnSc2	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
polil	polít	k5eAaPmAgMnS	polít
hořlavinou	hořlavina	k1gFnSc7	hořlavina
a	a	k8xC	a
zapálil	zapálit	k5eAaPmAgInS	zapálit
<g/>
.	.	kIx.	.
</s>
<s>
Hořící	hořící	k2eAgInSc1d1	hořící
běžel	běžet	k5eAaImAgInS	běžet
přes	přes	k7c4	přes
křižovatku	křižovatka	k1gFnSc4	křižovatka
od	od	k7c2	od
kašny	kašna	k1gFnSc2	kašna
pod	pod	k7c7	pod
muzeem	muzeum	k1gNnSc7	muzeum
k	k	k7c3	k
Washingtonově	Washingtonův	k2eAgFnSc3d1	Washingtonova
ulici	ulice	k1gFnSc3	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
pokusil	pokusit	k5eAaPmAgMnS	pokusit
uhasit	uhasit	k5eAaPmF	uhasit
výhybkář	výhybkář	k1gMnSc1	výhybkář
Dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
svým	svůj	k3xOyFgInSc7	svůj
kabátem	kabát	k1gInSc7	kabát
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozsáhlými	rozsáhlý	k2eAgFnPc7d1	rozsáhlá
popáleninami	popálenina	k1gFnPc7	popálenina
byl	být	k5eAaImAgInS	být
převezen	převézt	k5eAaPmNgMnS	převézt
na	na	k7c6	na
Kliniku	klinik	k1gMnSc6	klinik
popálenin	popálenina	k1gFnPc2	popálenina
FNKV	FNKV	kA	FNKV
a	a	k8xC	a
LFH	LFH	kA	LFH
UK	UK	kA	UK
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Legerově	Legerův	k2eAgFnSc6d1	Legerova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
magnetofonový	magnetofonový	k2eAgInSc4d1	magnetofonový
pásek	pásek	k1gInSc4	pásek
natočen	natočen	k2eAgInSc4d1	natočen
krátký	krátký	k2eAgInSc4d1	krátký
rozhovor	rozhovor	k1gInSc4	rozhovor
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Palach	palach	k1gInSc4	palach
hovořil	hovořit	k5eAaImAgMnS	hovořit
o	o	k7c6	o
příčině	příčina	k1gFnSc6	příčina
svého	svůj	k3xOyFgInSc2	svůj
činu	čin	k1gInSc2	čin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
záznamu	záznam	k1gInSc6	záznam
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
o	o	k7c6	o
pasivitě	pasivita	k1gFnSc6	pasivita
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
proti	proti	k7c3	proti
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
Zpráv	zpráva	k1gFnPc2	zpráva
-	-	kIx~	-
deníku	deník	k1gInSc2	deník
vydávaného	vydávaný	k2eAgInSc2d1	vydávaný
sovětskými	sovětský	k2eAgFnPc7d1	sovětská
okupanty	okupant	k1gMnPc4	okupant
<g/>
,	,	kIx,	,
a	a	k8xC	a
požaduje	požadovat	k5eAaImIp3nS	požadovat
generální	generální	k2eAgFnSc4d1	generální
stávku	stávka	k1gFnSc4	stávka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rozhovoru	rozhovor	k1gInSc2	rozhovor
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
pod	pod	k7c7	pod
utišujícími	utišující	k2eAgInPc7d1	utišující
léky	lék	k1gInPc7	lék
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
značné	značný	k2eAgFnPc4d1	značná
bolesti	bolest	k1gFnPc4	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
čin	čin	k1gInSc1	čin
nenazval	nazvat	k5eNaPmAgInS	nazvat
sebevraždou	sebevražda	k1gFnSc7	sebevražda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
akcí	akce	k1gFnSc7	akce
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Palachova	Palachův	k2eAgFnSc1d1	Palachova
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
o	o	k7c6	o
činu	čin	k1gInSc6	čin
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
až	až	k6eAd1	až
novin	novina	k1gFnPc2	novina
spolucestujícího	spolucestující	k2eAgInSc2d1	spolucestující
ve	v	k7c6	v
vlaku	vlak	k1gInSc6	vlak
a	a	k8xC	a
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Jiří	Jiří	k1gMnSc1	Jiří
se	se	k3xPyFc4	se
o	o	k7c6	o
Janově	Janův	k2eAgNnSc6d1	Janovo
popálení	popálení	k1gNnSc6	popálení
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
z	z	k7c2	z
telefonátu	telefonát	k1gInSc2	telefonát
z	z	k7c2	z
Pražské	pražský	k2eAgFnSc2d1	Pražská
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedozvěděl	dozvědět	k5eNaPmAgMnS	dozvědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
byl	být	k5eAaImAgInS	být
popálen	popálen	k2eAgInSc1d1	popálen
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
chvíli	chvíle	k1gFnSc6	chvíle
později	pozdě	k6eAd2	pozdě
však	však	k9	však
z	z	k7c2	z
rozhlasu	rozhlas	k1gInSc2	rozhlas
slyšel	slyšet	k5eAaImAgMnS	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
upálit	upálit	k5eAaPmF	upálit
jednadvacetiletý	jednadvacetiletý	k2eAgMnSc1d1	jednadvacetiletý
student	student	k1gMnSc1	student
filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
J.	J.	kA	J.
P.	P.	kA	P.
a	a	k8xC	a
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
spojilo	spojit	k5eAaPmAgNnS	spojit
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jan	Jan	k1gMnSc1	Jan
Palach	palach	k1gInSc4	palach
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
v	v	k7c4	v
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
činu	čin	k1gInSc6	čin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
zveřejněno	zveřejnit	k5eAaPmNgNnS	zveřejnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
historici	historik	k1gMnPc1	historik
objevili	objevit	k5eAaPmAgMnP	objevit
dokument	dokument	k1gInSc4	dokument
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgMnSc4	který
Palach	palach	k1gInSc4	palach
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1969	[number]	k4	1969
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
studenti	student	k1gMnPc1	student
obsadili	obsadit	k5eAaPmAgMnP	obsadit
budovu	budova	k1gFnSc4	budova
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Palach	palach	k1gInSc4	palach
svůj	svůj	k3xOyFgInSc4	svůj
návrh	návrh	k1gInSc4	návrh
odeslal	odeslat	k5eAaPmAgMnS	odeslat
10	[number]	k4	10
dnů	den	k1gInPc2	den
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
sebevraždou	sebevražda	k1gFnSc7	sebevražda
v	v	k7c6	v
dopisu	dopis	k1gInSc6	dopis
adresovaném	adresovaný	k2eAgInSc6d1	adresovaný
tehdejšímu	tehdejší	k2eAgMnSc3d1	tehdejší
studentskému	studentský	k2eAgMnSc3d1	studentský
vůdci	vůdce	k1gMnSc3	vůdce
Luboši	Luboš	k1gMnSc3	Luboš
Holečkovi	Holeček	k1gMnSc3	Holeček
<g/>
.	.	kIx.	.
</s>
<s>
Dokument	dokument	k1gInSc4	dokument
objevil	objevit	k5eAaPmAgMnS	objevit
historik	historik	k1gMnSc1	historik
Petr	Petr	k1gMnSc1	Petr
Blažek	Blažek	k1gMnSc1	Blažek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
sestavit	sestavit	k5eAaPmF	sestavit
co	co	k9	co
nejpřesnější	přesný	k2eAgFnSc4d3	nejpřesnější
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
předcházely	předcházet	k5eAaImAgFnP	předcházet
Palachovu	Palachův	k2eAgFnSc4d1	Palachova
sebeupálení	sebeupálení	k1gNnSc3	sebeupálení
<g/>
,	,	kIx,	,
samotný	samotný	k2eAgInSc1d1	samotný
čin	čin	k1gInSc1	čin
i	i	k8xC	i
následné	následný	k2eAgFnPc1d1	následná
události	událost	k1gFnPc1	událost
včetně	včetně	k7c2	včetně
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
příslušníky	příslušník	k1gMnPc7	příslušník
Veřejné	veřejný	k2eAgFnSc2d1	veřejná
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
posledním	poslední	k2eAgInSc6d1	poslední
činu	čin	k1gInSc6	čin
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
vykonáním	vykonání	k1gNnSc7	vykonání
nikomu	nikdo	k3yNnSc3	nikdo
nezmínil	zmínit	k5eNaPmAgMnS	zmínit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Inspirací	inspirace	k1gFnPc2	inspirace
Palachovi	Palachův	k2eAgMnPc1d1	Palachův
bylo	být	k5eAaImAgNnS	být
sebeupálení	sebeupálení	k1gNnSc3	sebeupálení
vietnamských	vietnamský	k2eAgMnPc2d1	vietnamský
buddhistických	buddhistický	k2eAgMnPc2d1	buddhistický
mnichů	mnich	k1gMnPc2	mnich
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
perzekuci	perzekuce	k1gFnSc3	perzekuce
buddhistů	buddhista	k1gMnPc2	buddhista
v	v	k7c6	v
Jižním	jižní	k2eAgInSc6d1	jižní
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
čin	čin	k1gInSc1	čin
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
nebyl	být	k5eNaImAgMnS	být
krokem	krokem	k6eAd1	krokem
zoufalého	zoufalý	k2eAgMnSc4d1	zoufalý
a	a	k8xC	a
nešťastného	šťastný	k2eNgMnSc4d1	nešťastný
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
svědčí	svědčit	k5eAaImIp3nS	svědčit
přepis	přepis	k1gInSc1	přepis
rozhovoru	rozhovor	k1gInSc2	rozhovor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
s	s	k7c7	s
Palachem	palach	k1gInSc7	palach
před	před	k7c7	před
jeho	jeho	k3xOp3gFnSc7	jeho
smrtí	smrt	k1gFnSc7	smrt
natočila	natočit	k5eAaBmAgFnS	natočit
ošetřovatelka	ošetřovatelka	k1gFnSc1	ošetřovatelka
Zdenka	Zdenka	k1gFnSc1	Zdenka
Kmuníčková	Kmuníčková	k1gFnSc1	Kmuníčková
<g/>
.	.	kIx.	.
</s>
<s>
Palach	palach	k1gInSc4	palach
jí	jíst	k5eAaImIp3nS	jíst
sdělil	sdělit	k5eAaPmAgMnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zapálil	zapálit	k5eAaPmAgMnS	zapálit
<g/>
,	,	kIx,	,
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
vztek	vztek	k1gInSc4	vztek
<g/>
.	.	kIx.	.
</s>
<s>
Pohřeb	pohřeb	k1gInSc1	pohřeb
Jana	Jana	k1gFnSc1	Jana
Palacha	Palacha	k1gMnSc1	Palacha
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
velkým	velký	k2eAgInSc7d1	velký
protestem	protest	k1gInSc7	protest
proti	proti	k7c3	proti
pokračující	pokračující	k2eAgFnSc3d1	pokračující
okupaci	okupace	k1gFnSc3	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Smuteční	smuteční	k2eAgInSc1d1	smuteční
průvod	průvod	k1gInSc1	průvod
<g/>
,	,	kIx,	,
čítající	čítající	k2eAgNnPc1d1	čítající
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
šel	jít	k5eAaImAgMnS	jít
z	z	k7c2	z
Václavského	václavský	k2eAgNnSc2d1	Václavské
náměstí	náměstí	k1gNnSc2	náměstí
na	na	k7c4	na
tehdejší	tehdejší	k2eAgNnSc4d1	tehdejší
náměstí	náměstí	k1gNnSc4	náměstí
Krasnoarmějců	Krasnoarmějce	k1gMnPc2	Krasnoarmějce
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1	[number]	k4	1
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Náměstí	náměstí	k1gNnSc1	náměstí
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hovořilo	hovořit	k5eAaImAgNnS	hovořit
několik	několik	k4yIc1	několik
řečníků	řečník	k1gMnPc2	řečník
včetně	včetně	k7c2	včetně
Luboše	Luboš	k1gMnSc2	Luboš
Holečka	Holeček	k1gMnSc2	Holeček
<g/>
.	.	kIx.	.
</s>
<s>
Palach	palach	k1gInSc1	palach
byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgInS	pohřbít
na	na	k7c6	na
Olšanských	olšanský	k2eAgInPc6d1	olšanský
hřbitovech	hřbitov	k1gInPc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Pohřební	pohřební	k2eAgNnSc1d1	pohřební
rozloučení	rozloučení	k1gNnSc1	rozloučení
vedl	vést	k5eAaImAgMnS	vést
evangelický	evangelický	k2eAgMnSc1d1	evangelický
farář	farář	k1gMnSc1	farář
Jakub	Jakub	k1gMnSc1	Jakub
Schwarz	Schwarz	k1gMnSc1	Schwarz
Trojan	Trojan	k1gMnSc1	Trojan
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gNnSc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
pozůstalých	pozůstalý	k1gMnPc2	pozůstalý
zpopelněny	zpopelněn	k2eAgInPc1d1	zpopelněn
a	a	k8xC	a
popel	popel	k1gInSc1	popel
uložen	uložen	k2eAgInSc1d1	uložen
v	v	k7c6	v
rodných	rodný	k2eAgInPc6d1	rodný
Všetatech	Všetat	k1gInPc6	Všetat
<g/>
.	.	kIx.	.
</s>
<s>
Převoz	převoz	k1gInSc1	převoz
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
tajně	tajně	k6eAd1	tajně
a	a	k8xC	a
narychlo	narychlo	k6eAd1	narychlo
<g/>
.	.	kIx.	.
</s>
<s>
Prázdný	prázdný	k2eAgInSc1d1	prázdný
hrob	hrob	k1gInSc1	hrob
byl	být	k5eAaImAgInS	být
však	však	k9	však
hojně	hojně	k6eAd1	hojně
navštěvován	navštěvován	k2eAgInSc1d1	navštěvován
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
urna	urna	k1gFnSc1	urna
s	s	k7c7	s
popelem	popel	k1gInSc7	popel
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
Olšanské	olšanský	k2eAgInPc4d1	olšanský
hřbitovy	hřbitov	k1gInPc4	hřbitov
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jan	Jan	k1gMnSc1	Jan
Palach	palach	k1gInSc4	palach
je	být	k5eAaImIp3nS	být
in	in	k?	in
memoriam	memoriam	k6eAd1	memoriam
nositelem	nositel	k1gMnSc7	nositel
Čestné	čestný	k2eAgFnSc2d1	čestná
medaile	medaile	k1gFnSc2	medaile
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
za	za	k7c4	za
věrnost	věrnost	k1gFnSc4	věrnost
jeho	jeho	k3xOp3gInSc2	jeho
odkazu	odkaz	k1gInSc2	odkaz
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mu	on	k3xPp3gMnSc3	on
udělilo	udělit	k5eAaPmAgNnS	udělit
jako	jako	k9	jako
prvnímu	první	k4xOgNnSc3	první
in	in	k?	in
memoriam	memoriam	k6eAd1	memoriam
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
demokratické	demokratický	k2eAgNnSc4d1	demokratické
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigua	Garriguus	k1gMnSc2	Garriguus
Masaryka	Masaryk	k1gMnSc2	Masaryk
1	[number]	k4	1
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
převzal	převzít	k5eAaPmAgMnS	převzít
za	za	k7c2	za
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
od	od	k7c2	od
prezidenta	prezident	k1gMnSc2	prezident
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Jiří	Jiří	k1gMnSc1	Jiří
Palach	palach	k1gInSc4	palach
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Janem	Jan	k1gMnSc7	Jan
Palachem	palach	k1gInSc7	palach
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
v	v	k7c6	v
září	září	k1gNnSc6	září
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
provedl	provést	k5eAaPmAgInS	provést
stejný	stejný	k2eAgInSc4d1	stejný
čin	čin	k1gInSc4	čin
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
Ryszard	Ryszard	k1gMnSc1	Ryszard
Siwiec	Siwiec	k1gMnSc1	Siwiec
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
sta	sto	k4xCgNnSc2	sto
tisíc	tisíc	k4xCgInSc4	tisíc
lidí	člověk	k1gMnPc2	člověk
včetně	včetně	k7c2	včetně
vedení	vedení	k1gNnSc2	vedení
polské	polský	k2eAgFnSc2d1	polská
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
protest	protest	k1gInSc1	protest
byl	být	k5eAaImAgInS	být
všemi	všecek	k3xTgNnPc7	všecek
oficiálními	oficiální	k2eAgNnPc7d1	oficiální
médii	médium	k1gNnPc7	médium
zamlčen	zamlčet	k5eAaPmNgInS	zamlčet
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
známý	známý	k2eAgMnSc1d1	známý
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1968	[number]	k4	1968
se	se	k3xPyFc4	se
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
náměstí	náměstí	k1gNnSc6	náměstí
Chreščatyk	Chreščatyka	k1gFnPc2	Chreščatyka
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
upálil	upálit	k5eAaPmAgMnS	upálit
jednačtyřicetiletý	jednačtyřicetiletý	k2eAgMnSc1d1	jednačtyřicetiletý
disident	disident	k1gMnSc1	disident
Vasyľ	Vasyľ	k1gMnSc1	Vasyľ
Makuch	Makuch	k1gMnSc1	Makuch
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
komunistickému	komunistický	k2eAgInSc3d1	komunistický
totalitnímu	totalitní	k2eAgInSc3d1	totalitní
systému	systém	k1gInSc3	systém
<g/>
,	,	kIx,	,
koloniálnímu	koloniální	k2eAgInSc3d1	koloniální
stavu	stav	k1gInSc3	stav
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
politice	politika	k1gFnSc3	politika
rusifikace	rusifikace	k1gFnSc2	rusifikace
a	a	k8xC	a
agresi	agrese	k1gFnSc4	agrese
SSSR	SSSR	kA	SSSR
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
den	den	k1gInSc4	den
nato	nato	k6eAd1	nato
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
čin	čin	k1gInSc1	čin
byl	být	k5eAaImAgInS	být
také	také	k9	také
dlouho	dlouho	k6eAd1	dlouho
zamlčován	zamlčován	k2eAgMnSc1d1	zamlčován
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Palach	palach	k1gInSc4	palach
si	se	k3xPyFc3	se
sám	sám	k3xTgInSc4	sám
údajně	údajně	k6eAd1	údajně
nepřál	přát	k5eNaImAgMnS	přát
další	další	k2eAgMnPc4d1	další
mrtvé	mrtvý	k1gMnPc4	mrtvý
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
písemné	písemný	k2eAgFnPc4d1	písemná
poznámky	poznámka	k1gFnPc4	poznámka
a	a	k8xC	a
výpověď	výpověď	k1gFnSc4	výpověď
jeho	jeho	k3xOp3gMnSc2	jeho
spolužáka	spolužák	k1gMnSc2	spolužák
Luboše	Luboš	k1gMnSc2	Luboš
Holečka	Holeček	k1gMnSc2	Holeček
o	o	k7c6	o
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Palachem	palach	k1gInSc7	palach
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
:	:	kIx,	:
Podle	podle	k7c2	podle
historika	historik	k1gMnSc2	historik
Petra	Petr	k1gMnSc2	Petr
Blažka	Blažka	k1gFnSc1	Blažka
ovšem	ovšem	k9	ovšem
umírající	umírající	k2eAgInSc4d1	umírající
Palach	palach	k1gInSc4	palach
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
souvisle	souvisle	k6eAd1	souvisle
mluvit	mluvit	k5eAaImF	mluvit
a	a	k8xC	a
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
Holečkův	Holečkův	k2eAgInSc4d1	Holečkův
výmysl	výmysl	k1gInSc4	výmysl
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
dalšímu	další	k2eAgNnSc3d1	další
upalování	upalování	k1gNnSc3	upalování
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
i	i	k8xC	i
básník	básník	k1gMnSc1	básník
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
činu	čin	k1gInSc6	čin
Josefa	Josef	k1gMnSc2	Josef
Hlavatého	Hlavatý	k1gMnSc2	Hlavatý
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tyto	tento	k3xDgFnPc4	tento
žádosti	žádost	k1gFnPc4	žádost
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
měsících	měsíc	k1gInPc6	měsíc
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
k	k	k7c3	k
vlně	vlna	k1gFnSc3	vlna
sebevražedných	sebevražedný	k2eAgInPc2d1	sebevražedný
pokusů	pokus	k1gInPc2	pokus
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
následovali	následovat	k5eAaImAgMnP	následovat
Palacha	Palach	k1gMnSc4	Palach
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Palachovi	Palach	k1gMnSc6	Palach
se	se	k3xPyFc4	se
v	v	k7c4	v
období	období	k1gNnSc4	období
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
se	se	k3xPyFc4	se
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
upálit	upálit	k5eAaPmF	upálit
10	[number]	k4	10
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
do	do	k7c2	do
konce	konec	k1gInSc2	konec
dubna	duben	k1gInSc2	duben
1969	[number]	k4	1969
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
26	[number]	k4	26
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
7	[number]	k4	7
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnPc1d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Zajíc	Zajíc	k1gMnSc1	Zajíc
(	(	kIx(	(
<g/>
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
podepsal	podepsat	k5eAaPmAgMnS	podepsat
jako	jako	k9	jako
pochodeň	pochodeň	k1gFnSc4	pochodeň
č.	č.	k?	č.
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
upálil	upálit	k5eAaPmAgInS	upálit
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
v	v	k7c6	v
průchodu	průchod	k1gInSc6	průchod
domu	dům	k1gInSc2	dům
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
dalších	další	k2eAgMnPc2d1	další
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
upálili	upálit	k5eAaPmAgMnP	upálit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ale	ale	k9	ale
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Palachem	palach	k1gInSc7	palach
takřka	takřka	k6eAd1	takřka
jistě	jistě	k6eAd1	jistě
neznali	znát	k5eNaImAgMnP	znát
a	a	k8xC	a
nebyli	být	k5eNaImAgMnP	být
členy	člen	k1gInPc4	člen
žádné	žádný	k3yNgFnSc2	žádný
organizované	organizovaný	k2eAgFnSc2d1	organizovaná
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc1	jméno
dalších	další	k2eAgMnPc2d1	další
jsou	být	k5eAaImIp3nP	být
Josef	Josef	k1gMnSc1	Josef
Hlavatý	Hlavatý	k1gMnSc1	Hlavatý
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
na	na	k7c6	na
pomníku	pomník	k1gInSc6	pomník
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigue	Garrigu	k1gMnSc2	Garrigu
Masaryka	Masaryk	k1gMnSc2	Masaryk
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
dověděl	dovědět	k5eAaPmAgMnS	dovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jan	Jan	k1gMnSc1	Jan
Palach	palach	k1gInSc4	palach
na	na	k7c6	na
klinice	klinika	k1gFnSc6	klinika
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Evžen	Evžen	k1gMnSc1	Evžen
Plocek	Plocka	k1gFnPc2	Plocka
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1969	[number]	k4	1969
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
Michal	Michal	k1gMnSc1	Michal
Lefčík	Lefčík	k1gMnSc1	Lefčík
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
upálil	upálit	k5eAaPmAgMnS	upálit
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Josefa	Josef	k1gMnSc2	Josef
Hlavatého	Hlavatý	k1gMnSc2	Hlavatý
se	se	k3xPyFc4	se
ale	ale	k9	ale
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
jejich	jejich	k3xOp3gInPc6	jejich
činech	čin	k1gInPc6	čin
na	na	k7c6	na
celostátní	celostátní	k2eAgFnSc6d1	celostátní
úrovni	úroveň	k1gFnSc6	úroveň
omezila	omezit	k5eAaPmAgFnS	omezit
jen	jen	k9	jen
na	na	k7c4	na
několikařádkovou	několikařádkový	k2eAgFnSc4d1	několikařádková
zmínku	zmínka	k1gFnSc4	zmínka
v	v	k7c6	v
černé	černý	k2eAgFnSc6d1	černá
kronice	kronika	k1gFnSc6	kronika
Rudého	rudý	k2eAgNnSc2d1	Rudé
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
sebeupálení	sebeupálení	k1gNnSc6	sebeupálení
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
též	též	k9	též
dělník	dělník	k1gMnSc1	dělník
Miroslav	Miroslav	k1gMnSc1	Miroslav
Malinka	malinka	k1gFnSc1	malinka
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
)	)	kIx)	)
a	a	k8xC	a
šestnáctiletý	šestnáctiletý	k2eAgMnSc1d1	šestnáctiletý
učeň	učeň	k1gMnSc1	učeň
Jan	Jan	k1gMnSc1	Jan
Bereš	brát	k5eAaImIp2nS	brát
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
se	se	k3xPyFc4	se
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
Palachovu	Palachův	k2eAgFnSc4d1	Palachova
smrt	smrt	k1gFnSc4	smrt
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
polil	polít	k5eAaPmAgMnS	polít
benzínem	benzín	k1gInSc7	benzín
a	a	k8xC	a
zapálil	zapálit	k5eAaPmAgMnS	zapálit
Sándor	Sándor	k1gMnSc1	Sándor
Bauer	Bauer	k1gMnSc1	Bauer
<g/>
,	,	kIx,	,
šestnáctiletý	šestnáctiletý	k2eAgMnSc1d1	šestnáctiletý
student	student	k1gMnSc1	student
průmyslovky	průmyslovka	k1gFnSc2	průmyslovka
<g/>
;	;	kIx,	;
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
upálit	upálit	k5eAaPmF	upálit
lotyšský	lotyšský	k2eAgMnSc1d1	lotyšský
student	student	k1gMnSc1	student
Elijahu	Elijah	k1gInSc2	Elijah
Rips	rips	k1gInSc1	rips
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
čin	čin	k1gInSc1	čin
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
rychlé	rychlý	k2eAgFnSc3d1	rychlá
pomoci	pomoc	k1gFnSc3	pomoc
kolemjdoucích	kolemjdoucí	k1gMnPc2	kolemjdoucí
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Vysoce	vysoce	k6eAd1	vysoce
postavený	postavený	k2eAgMnSc1d1	postavený
komunistický	komunistický	k2eAgMnSc1d1	komunistický
funkcionář	funkcionář	k1gMnSc1	funkcionář
Vilém	Vilém	k1gMnSc1	Vilém
Nový	Nový	k1gMnSc1	Nový
pronášel	pronášet	k5eAaImAgMnS	pronášet
lživé	lživý	k2eAgFnPc4d1	lživá
teorie	teorie	k1gFnPc4	teorie
o	o	k7c6	o
Palachově	palachově	k6eAd1	palachově
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
motivaci	motivace	k1gFnSc4	motivace
jeho	jeho	k3xOp3gInSc2	jeho
činu	čin	k1gInSc2	čin
<g/>
.	.	kIx.	.
</s>
<s>
Palach	palach	k1gInSc1	palach
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
Nového	Nový	k1gMnSc2	Nový
vylosován	vylosován	k2eAgMnSc1d1	vylosován
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
ze	z	k7c2	z
studentské	studentská	k1gFnSc2	studentská
skupiny	skupina	k1gFnSc2	skupina
"	"	kIx"	"
<g/>
Pětice	pětice	k1gFnSc1	pětice
smrti	smrt	k1gFnSc2	smrt
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nechtěl	chtít	k5eNaImAgMnS	chtít
se	se	k3xPyFc4	se
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pouze	pouze	k6eAd1	pouze
polít	polít	k5eAaPmF	polít
chemikálií	chemikálie	k1gFnSc7	chemikálie
způsobující	způsobující	k2eAgInSc4d1	způsobující
"	"	kIx"	"
<g/>
studený	studený	k2eAgInSc4d1	studený
plamen	plamen	k1gInSc4	plamen
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
oheň	oheň	k1gInSc1	oheň
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nepálí	pálit	k5eNaImIp3nS	pálit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tímto	tento	k3xDgInSc7	tento
protestem	protest	k1gInSc7	protest
destabilizovat	destabilizovat	k5eAaBmF	destabilizovat
situaci	situace	k1gFnSc4	situace
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Chemikálie	chemikálie	k1gFnSc1	chemikálie
mu	on	k3xPp3gInSc3	on
však	však	k9	však
přimíchali	přimíchat	k5eAaPmAgMnP	přimíchat
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
do	do	k7c2	do
hořlaviny	hořlavina	k1gFnSc2	hořlavina
příliš	příliš	k6eAd1	příliš
malé	malý	k2eAgNnSc4d1	malé
množství	množství	k1gNnSc4	množství
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
smrtelné	smrtelný	k2eAgFnPc4d1	smrtelná
popáleniny	popálenina	k1gFnPc4	popálenina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
formě	forma	k1gFnSc3	forma
protestu	protest	k1gInSc2	protest
měli	mít	k5eAaImAgMnP	mít
studenty	student	k1gMnPc4	student
navést	navést	k5eAaPmF	navést
Luděk	Luděk	k1gMnSc1	Luděk
Pachman	Pachman	k1gMnSc1	Pachman
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Škutina	Škutina	k1gFnSc1	Škutina
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Kohout	Kohout	k1gMnSc1	Kohout
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Zátopek	Zátopek	k1gInSc1	Zátopek
a	a	k8xC	a
Lubomír	Lubomír	k1gMnSc1	Lubomír
Holeček	Holeček	k1gMnSc1	Holeček
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
na	na	k7c6	na
Nového	Nového	k2eAgFnSc6d1	Nového
podali	podat	k5eAaPmAgMnP	podat
žalobu	žaloba	k1gFnSc4	žaloba
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
osobnosti	osobnost	k1gFnSc2	osobnost
(	(	kIx(	(
<g/>
Zátopek	Zátopek	k1gInSc1	Zátopek
ji	on	k3xPp3gFnSc4	on
později	pozdě	k6eAd2	pozdě
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
žalobu	žaloba	k1gFnSc4	žaloba
podala	podat	k5eAaPmAgFnS	podat
Palachova	Palachův	k2eAgFnSc1d1	Palachova
matka	matka	k1gFnSc1	matka
Libuše	Libuše	k1gFnSc2	Libuše
Palachová	palachový	k2eAgFnSc1d1	Palachová
<g/>
,	,	kIx,	,
zastupovaná	zastupovaný	k2eAgFnSc1d1	zastupovaná
advokátkou	advokátka	k1gFnSc7	advokátka
Dagmar	Dagmar	k1gFnSc7	Dagmar
Burešovou	Burešová	k1gFnSc7	Burešová
<g/>
.	.	kIx.	.
</s>
<s>
Soudkyně	soudkyně	k1gFnSc1	soudkyně
Jarmila	Jarmila	k1gFnSc1	Jarmila
Ortová	Ortová	k1gFnSc1	Ortová
obě	dva	k4xCgFnPc1	dva
žaloby	žaloba	k1gFnSc2	žaloba
zamítla	zamítnout	k5eAaPmAgFnS	zamítnout
se	s	k7c7	s
zdůvodněním	zdůvodnění	k1gNnSc7	zdůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nový	nový	k2eAgInSc1d1	nový
měl	mít	k5eAaImAgInS	mít
nejen	nejen	k6eAd1	nejen
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokonce	dokonce	k9	dokonce
povinnost	povinnost	k1gFnSc1	povinnost
kritizovat	kritizovat	k5eAaImF	kritizovat
Palachův	Palachův	k2eAgInSc4d1	Palachův
čin	čin	k1gInSc4	čin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tzv.	tzv.	kA	tzv.
Palachově	palachově	k6eAd1	palachově
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
probíhaly	probíhat	k5eAaImAgFnP	probíhat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
protestní	protestní	k2eAgFnSc2d1	protestní
akce	akce	k1gFnSc2	akce
zaměřené	zaměřený	k2eAgNnSc1d1	zaměřené
proti	proti	k7c3	proti
komunistickému	komunistický	k2eAgInSc3d1	komunistický
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
předstupeň	předstupeň	k1gInSc4	předstupeň
Sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
a	a	k8xC	a
Lidové	lidový	k2eAgFnPc1d1	lidová
milice	milice	k1gFnSc2	milice
tyto	tento	k3xDgInPc1	tento
protesty	protest	k1gInPc1	protest
rozháněly	rozhánět	k5eAaImAgInP	rozhánět
obušky	obušek	k1gInPc4	obušek
<g/>
,	,	kIx,	,
slzným	slzný	k2eAgInSc7d1	slzný
plynem	plyn	k1gInSc7	plyn
a	a	k8xC	a
vodními	vodní	k2eAgNnPc7d1	vodní
děly	dělo	k1gNnPc7	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
představitelů	představitel	k1gMnPc2	představitel
nezávislých	závislý	k2eNgFnPc2d1	nezávislá
iniciativ	iniciativa	k1gFnPc2	iniciativa
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Penc	Penc	k1gFnSc1	Penc
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
Vondra	Vondra	k1gMnSc1	Vondra
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
Němcová	Němcová	k1gFnSc1	Němcová
či	či	k8xC	či
Petr	Petr	k1gMnSc1	Petr
Placák	placák	k1gInSc4	placák
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
kvůli	kvůli	k7c3	kvůli
účasti	účast	k1gFnSc3	účast
na	na	k7c6	na
demonstraci	demonstrace	k1gFnSc6	demonstrace
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
a	a	k8xC	a
vzato	vzat	k2eAgNnSc1d1	vzato
do	do	k7c2	do
vazby	vazba	k1gFnSc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
hřbitov	hřbitov	k1gInSc4	hřbitov
ve	v	k7c6	v
Všetatech	Všetat	k1gInPc6	Všetat
byl	být	k5eAaImAgMnS	být
Veřejnou	veřejný	k2eAgFnSc7d1	veřejná
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
zcela	zcela	k6eAd1	zcela
obležen	obležen	k2eAgMnSc1d1	obležen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
pokládat	pokládat	k5eAaImF	pokládat
květiny	květina	k1gFnPc4	květina
a	a	k8xC	a
rozsvěcet	rozsvěcet	k5eAaImF	rozsvěcet
svíce	svíce	k1gFnPc4	svíce
u	u	k7c2	u
Palachova	Palachův	k2eAgInSc2d1	Palachův
hrobu	hrob	k1gInSc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
existuje	existovat	k5eAaImIp3nS	existovat
ve	v	k7c6	v
Všetatech	Všetat	k1gInPc6	Všetat
nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
Společnost	společnost	k1gFnSc1	společnost
Jana	Jana	k1gFnSc1	Jana
Palacha	Palacha	k1gFnSc1	Palacha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pořádá	pořádat	k5eAaImIp3nS	pořádat
každoroční	každoroční	k2eAgFnSc1d1	každoroční
pietní	pietní	k2eAgFnSc1d1	pietní
akce	akce	k1gFnSc1	akce
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
obnovením	obnovení	k1gNnSc7	obnovení
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
spontánně	spontánně	k6eAd1	spontánně
založené	založený	k2eAgInPc1d1	založený
hned	hned	k6eAd1	hned
po	po	k7c6	po
pohřbu	pohřeb	k1gInSc6	pohřeb
(	(	kIx(	(
<g/>
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
vybudovat	vybudovat	k5eAaPmF	vybudovat
Janu	Jan	k1gMnSc3	Jan
Palachovi	Palach	k1gMnSc3	Palach
pomník	pomník	k1gInSc4	pomník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
ve	v	k7c6	v
Všetatech	Všetat	k1gInPc6	Všetat
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Jan	Jan	k1gMnSc1	Jan
Palach	palach	k1gInSc4	palach
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
<g/>
,	,	kIx,	,
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
nachází	nacházet	k5eAaImIp3nS	nacházet
Palachova	Palachův	k2eAgFnSc1d1	Palachova
busta	busta	k1gFnSc1	busta
a	a	k8xC	a
pamětní	pamětní	k2eAgFnSc1d1	pamětní
expozice	expozice	k1gFnSc1	expozice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Janu	Jan	k1gMnSc6	Jan
Palachovi	Palach	k1gMnSc6	Palach
je	být	k5eAaImIp3nS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
také	také	k9	také
planetka	planetka	k1gFnSc1	planetka
(	(	kIx(	(
<g/>
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
Palach	palach	k1gInSc1	palach
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1969	[number]	k4	1969
objevil	objevit	k5eAaPmAgMnS	objevit
astronom	astronom	k1gMnSc1	astronom
Luboš	Luboš	k1gMnSc1	Luboš
Kohoutek	Kohoutek	k1gMnSc1	Kohoutek
na	na	k7c6	na
hvězdárně	hvězdárna	k1gFnSc6	hvězdárna
Bergedorf	Bergedorf	k1gInSc4	Bergedorf
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
nese	nést	k5eAaImIp3nS	nést
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1990	[number]	k4	1990
gymnázium	gymnázium	k1gNnSc1	gymnázium
v	v	k7c6	v
Mělníce	Mělník	k1gInSc6	Mělník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
maturoval	maturovat	k5eAaBmAgInS	maturovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
gymnázium	gymnázium	k1gNnSc1	gymnázium
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
jménem	jméno	k1gNnSc7	jméno
jsou	být	k5eAaImIp3nP	být
pojmenována	pojmenovat	k5eAaPmNgNnP	pojmenovat
také	také	k9	také
náměstí	náměstí	k1gNnSc3	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Varně	Varna	k1gFnSc6	Varna
a	a	k8xC	a
Lucemburku	Lucemburk	k1gInSc6	Lucemburk
a	a	k8xC	a
ulice	ulice	k1gFnPc1	ulice
v	v	k7c6	v
desítkách	desítka	k1gFnPc6	desítka
měst	město	k1gNnPc2	město
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
i	i	k8xC	i
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byly	být	k5eAaImAgFnP	být
schváleny	schválit	k5eAaPmNgInP	schválit
nové	nový	k2eAgInPc1d1	nový
památné	památný	k2eAgInPc1d1	památný
dny	den	k1gInPc1	den
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
i	i	k9	i
den	den	k1gInSc4	den
narození	narození	k1gNnSc2	narození
T.G.	T.G.	k1gMnSc2	T.G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
a	a	k8xC	a
úmrtí	úmrtí	k1gNnSc2	úmrtí
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Byly	být	k5eAaImAgInP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
kalendářích	kalendář	k1gInPc6	kalendář
pro	pro	k7c4	pro
léta	léto	k1gNnPc4	léto
1991	[number]	k4	1991
až	až	k8xS	až
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
s	s	k7c7	s
rozpadem	rozpad	k1gInSc7	rozpad
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednových	lednový	k2eAgInPc6d1	lednový
dnech	den	k1gInPc6	den
pořádá	pořádat	k5eAaImIp3nS	pořádat
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
každoroční	každoroční	k2eAgFnSc1d1	každoroční
vzpomínková	vzpomínkový	k2eAgFnSc1d1	vzpomínková
shromáždění	shromáždění	k1gNnSc1	shromáždění
u	u	k7c2	u
Palachova	Palachův	k2eAgInSc2d1	Palachův
hrobu	hrob	k1gInSc2	hrob
na	na	k7c6	na
Olšanských	olšanský	k2eAgInPc6d1	olšanský
hřbitovech	hřbitov	k1gInPc6	hřbitov
společnost	společnost	k1gFnSc1	společnost
JANUA	JANUA	kA	JANUA
<g/>
,	,	kIx,	,
starající	starající	k2eAgMnSc1d1	starající
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
o	o	k7c4	o
odkaz	odkaz	k1gInSc4	odkaz
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
česká	český	k2eAgFnSc1d1	Česká
vláda	vláda	k1gFnSc1	vláda
souhlas	souhlas	k1gInSc1	souhlas
s	s	k7c7	s
poslaneckým	poslanecký	k2eAgInSc7d1	poslanecký
návrhem	návrh	k1gInSc7	návrh
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
16	[number]	k4	16
<g/>
.	.	kIx.	.
leden	leden	k1gInSc4	leden
(	(	kIx(	(
<g/>
den	den	k1gInSc4	den
Palachova	Palachův	k2eAgNnSc2d1	Palachovo
sebeobětování	sebeobětování	k1gNnSc2	sebeobětování
<g/>
)	)	kIx)	)
stal	stát	k5eAaPmAgInS	stát
významným	významný	k2eAgInSc7d1	významný
dnem	den	k1gInSc7	den
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Den	den	k1gInSc1	den
památky	památka	k1gFnSc2	památka
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
65	[number]	k4	65
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
narození	narození	k1gNnSc2	narození
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
<g/>
.	.	kIx.	.
</s>
<s>
Palachovu	Palachův	k2eAgFnSc4d1	Palachova
posmrtnou	posmrtný	k2eAgFnSc4d1	posmrtná
masku	maska	k1gFnSc4	maska
odlil	odlít	k5eAaPmAgMnS	odlít
sochař	sochař	k1gMnSc1	sochař
Olbram	Olbram	k1gInSc4	Olbram
Zoubek	zoubek	k1gInSc1	zoubek
Kniha	kniha	k1gFnSc1	kniha
Slunce	slunce	k1gNnSc2	slunce
v	v	k7c6	v
úplňku	úplněk	k1gInSc6	úplněk
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Lenky	Lenka	k1gFnSc2	Lenka
Procházkové	Procházková	k1gFnSc2	Procházková
je	být	k5eAaImIp3nS	být
románové	románový	k2eAgNnSc1d1	románové
zpracování	zpracování	k1gNnSc1	zpracování
posledního	poslední	k2eAgInSc2d1	poslední
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgInSc4d1	hudební
klip	klip	k1gInSc4	klip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
"	"	kIx"	"
<g/>
Club	club	k1gInSc1	club
Foot	Foot	k1gInSc1	Foot
<g/>
"	"	kIx"	"
skupiny	skupina	k1gFnSc2	skupina
Kasabian	Kasabian	k1gInSc1	Kasabian
je	být	k5eAaImIp3nS	být
věnován	věnovat	k5eAaPmNgInS	věnovat
Janu	Jan	k1gMnSc3	Jan
Palachovi	Palach	k1gMnSc3	Palach
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Ticho	ticho	k1gNnSc1	ticho
<g/>
"	"	kIx"	"
Bohdana	Bohdana	k1gFnSc1	Bohdana
Mikoláška	Mikolášek	k1gMnSc2	Mikolášek
je	být	k5eAaImIp3nS	být
věnována	věnovat	k5eAaImNgFnS	věnovat
smrti	smrt	k1gFnSc2	smrt
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
"	"	kIx"	"
<g/>
Dědicům	dědic	k1gMnPc3	dědic
Palachovým	palachový	k2eAgMnPc3d1	palachový
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Marat	Marat	k1gInSc1	Marat
ve	v	k7c6	v
vaně	vana	k1gFnSc6	vana
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Atlantis	Atlantis	k1gFnSc1	Atlantis
<g/>
"	"	kIx"	"
Karla	Karel	k1gMnSc4	Karel
Kryla	Kryl	k1gMnSc4	Kryl
Americký	americký	k2eAgMnSc1d1	americký
básník	básník	k1gMnSc1	básník
David	David	k1gMnSc1	David
Shapiro	Shapiro	k1gNnSc4	Shapiro
napsal	napsat	k5eAaBmAgMnS	napsat
báseň	báseň	k1gFnSc4	báseň
"	"	kIx"	"
<g/>
Pohřeb	pohřeb	k1gInSc1	pohřeb
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Funeral	Funeral	k1gMnSc1	Funeral
of	of	k?	of
Jan	Jan	k1gMnSc1	Jan
Palach	palach	k1gInSc1	palach
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
báseň	báseň	k1gFnSc1	báseň
byla	být	k5eAaImAgFnS	být
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
amerického	americký	k2eAgMnSc4d1	americký
architekta	architekt	k1gMnSc4	architekt
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
Johna	John	k1gMnSc2	John
Hejduka	Hejduk	k1gMnSc2	Hejduk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
sochy	socha	k1gFnPc4	socha
s	s	k7c7	s
názvem	název	k1gInSc7	název
Dům	dům	k1gInSc1	dům
pro	pro	k7c4	pro
sebevraha	sebevrah	k1gMnSc4	sebevrah
a	a	k8xC	a
Dům	dům	k1gInSc1	dům
pro	pro	k7c4	pro
matku	matka	k1gFnSc4	matka
sebevraha	sebevrah	k1gMnSc2	sebevrah
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
House	house	k1gNnSc1	house
of	of	k?	of
the	the	k?	the
Suicide	Suicid	k1gMnSc5	Suicid
<g/>
,	,	kIx,	,
The	The	k1gFnPc1	The
House	house	k1gNnSc1	house
of	of	k?	of
the	the	k?	the
Mother	Mothra	k1gFnPc2	Mothra
of	of	k?	of
the	the	k?	the
Suicide	Suicid	k1gInSc5	Suicid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sochy	socha	k1gFnPc1	socha
byly	být	k5eAaImAgFnP	být
poprvé	poprvé	k6eAd1	poprvé
realizovány	realizovat	k5eAaBmNgFnP	realizovat
skupinou	skupina	k1gFnSc7	skupina
studentů	student	k1gMnPc2	student
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
profesora	profesor	k1gMnSc2	profesor
Jamese	Jamese	k1gFnSc2	Jamese
Williamsona	Williamson	k1gMnSc2	Williamson
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
nechal	nechat	k5eAaPmAgMnS	nechat
John	John	k1gMnSc1	John
Hejduk	Hejduk	k1gMnSc1	Hejduk
sochy	socha	k1gFnSc2	socha
znovu	znovu	k6eAd1	znovu
zbudovat	zbudovat	k5eAaPmF	zbudovat
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
výstavy	výstava	k1gFnSc2	výstava
John	John	k1gMnSc1	John
Hejduk	Hejduk	k1gMnSc1	Hejduk
-	-	kIx~	-
Práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Sochy	Socha	k1gMnPc4	Socha
věnoval	věnovat	k5eAaImAgInS	věnovat
Václavu	Václav	k1gMnSc3	Václav
Havlovi	Havel	k1gMnSc3	Havel
a	a	k8xC	a
českému	český	k2eAgInSc3d1	český
lidu	lid	k1gInSc3	lid
<g/>
.	.	kIx.	.
</s>
<s>
Dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
sochy	socha	k1gFnPc1	socha
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
v	v	k7c6	v
zahradách	zahrada	k1gFnPc6	zahrada
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
nevydržely	vydržet	k5eNaPmAgInP	vydržet
ale	ale	k8xC	ale
vliv	vliv	k1gInSc1	vliv
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
byly	být	k5eAaImAgFnP	být
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
schválil	schválit	k5eAaPmAgInS	schválit
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
kulturu	kultura	k1gFnSc4	kultura
pražského	pražský	k2eAgInSc2d1	pražský
Magistrátu	magistrát	k1gInSc2	magistrát
záměr	záměr	k1gInSc1	záměr
výstavby	výstavba	k1gFnSc2	výstavba
pomníku	pomník	k1gInSc2	pomník
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
podle	podle	k7c2	podle
Hejdukovy	Hejdukův	k2eAgFnSc2d1	Hejdukova
dokumentace	dokumentace	k1gFnSc2	dokumentace
na	na	k7c6	na
Alšově	Alšův	k2eAgNnSc6d1	Alšovo
nábřeží	nábřeží	k1gNnSc6	nábřeží
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1	[number]	k4	1
vedle	vedle	k7c2	vedle
budovy	budova	k1gFnSc2	budova
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc2d1	uměleckoprůmyslová
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
pomník	pomník	k1gInSc1	pomník
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
budou	být	k5eAaImBp3nP	být
tvořit	tvořit	k5eAaImF	tvořit
dvě	dva	k4xCgFnPc4	dva
přibližně	přibližně	k6eAd1	přibližně
šestimetrové	šestimetrový	k2eAgFnPc4d1	šestimetrová
geometrické	geometrický	k2eAgFnPc4d1	geometrická
plastiky	plastika	k1gFnPc4	plastika
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgFnPc6	jenž
z	z	k7c2	z
hranatého	hranatý	k2eAgInSc2d1	hranatý
soklu	sokl	k1gInSc2	sokl
budou	být	k5eAaImBp3nP	být
vycházet	vycházet	k5eAaImF	vycházet
plameny	plamen	k1gInPc1	plamen
<g/>
.	.	kIx.	.
</s>
<s>
Světlá	světlý	k2eAgFnSc1d1	světlá
plastika	plastika	k1gFnSc1	plastika
bude	být	k5eAaImBp3nS	být
představovat	představovat	k5eAaImF	představovat
syna	syn	k1gMnSc4	syn
-	-	kIx~	-
světlonoše	světlonoš	k1gMnSc4	světlonoš
<g/>
,	,	kIx,	,
tmavá	tmavý	k2eAgNnPc4d1	tmavé
bude	být	k5eAaImBp3nS	být
znázorňovat	znázorňovat	k5eAaImF	znázorňovat
trpící	trpící	k2eAgFnSc4d1	trpící
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Primavera	Primavera	k1gFnSc1	Primavera
di	di	k?	di
Praga	Praga	k1gFnSc1	Praga
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Pražské	pražský	k2eAgNnSc4d1	Pražské
jaro	jaro	k1gNnSc4	jaro
<g/>
)	)	kIx)	)
italského	italský	k2eAgMnSc2d1	italský
zpěváka	zpěvák	k1gMnSc2	zpěvák
Francesca	Francescus	k1gMnSc2	Francescus
Gucciniho	Guccini	k1gMnSc2	Guccini
upomíná	upomínat	k5eAaImIp3nS	upomínat
i	i	k9	i
na	na	k7c4	na
Palachovu	Palachův	k2eAgFnSc4d1	Palachova
smrt	smrt	k1gFnSc4	smrt
veršem	verš	k1gInSc7	verš
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
znovu	znovu	k6eAd1	znovu
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
hořel	hořet	k5eAaImAgInS	hořet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Mourir	Mourir	k1gInSc1	Mourir
dans	dans	k6eAd1	dans
tes	tes	k1gInSc1	tes
bras	brasa	k1gFnPc2	brasa
<g/>
"	"	kIx"	"
italského	italský	k2eAgMnSc2d1	italský
zpěváka	zpěvák	k1gMnSc2	zpěvák
Salvatore	Salvator	k1gMnSc5	Salvator
Adamo	Adama	k1gFnSc5	Adama
upomíná	upomínat	k5eAaImIp3nS	upomínat
na	na	k7c4	na
čin	čin	k1gInSc4	čin
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Soud	soud	k1gInSc1	soud
<g/>
"	"	kIx"	"
českého	český	k2eAgMnSc2d1	český
zpěváka	zpěvák	k1gMnSc2	zpěvák
a	a	k8xC	a
herce	herec	k1gMnSc2	herec
Václava	Václav	k1gMnSc2	Václav
Neckáře	Neckář	k1gMnSc2	Neckář
byla	být	k5eAaImAgFnS	být
natočena	natočit	k5eAaBmNgFnS	natočit
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
černobílý	černobílý	k2eAgInSc1d1	černobílý
klip	klip	k1gInSc1	klip
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgInS	natáčet
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
kolem	kolem	k7c2	kolem
pražského	pražský	k2eAgInSc2d1	pražský
pohřbu	pohřeb	k1gInSc2	pohřeb
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
Palachem	palach	k1gInSc7	palach
nicméně	nicméně	k8xC	nicméně
text	text	k1gInSc1	text
nesouvisí	souviset	k5eNaImIp3nS	souviset
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgInSc1d1	hudební
cyklus	cyklus	k1gInSc1	cyklus
Plamenný	plamenný	k2eAgInSc1d1	plamenný
meč	meč	k1gInSc1	meč
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
Jan	Jan	k1gMnSc1	Jan
Bedřich	Bedřich	k1gMnSc1	Bedřich
<g/>
,	,	kIx,	,
slova	slovo	k1gNnSc2	slovo
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Nohelová	Nohelová	k1gFnSc1	Nohelová
<g/>
)	)	kIx)	)
od	od	k7c2	od
českého	český	k2eAgMnSc2d1	český
operního	operní	k2eAgMnSc2d1	operní
pěvce	pěvec	k1gMnSc2	pěvec
Hynka	Hynek	k1gMnSc2	Hynek
Maxy	Maxa	k1gMnSc2	Maxa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
osobní	osobní	k2eAgFnSc2d1	osobní
iniciativy	iniciativa	k1gFnSc2	iniciativa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
okamžitou	okamžitý	k2eAgFnSc7d1	okamžitá
hudební	hudební	k2eAgFnSc7d1	hudební
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
Palachovu	Palachův	k2eAgFnSc4d1	Palachova
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Jan	Jan	k1gMnSc1	Jan
Palach	palach	k1gInSc1	palach
<g/>
"	"	kIx"	"
italské	italský	k2eAgFnSc2d1	italská
nacionalistické	nacionalistický	k2eAgFnSc2d1	nacionalistická
kapely	kapela	k1gFnSc2	kapela
Compagnia	Compagnium	k1gNnSc2	Compagnium
dell	dell	k1gInSc1	dell
<g/>
'	'	kIx"	'
<g/>
Anello	Anello	k1gNnSc1	Anello
(	(	kIx(	(
<g/>
Společenstvo	společenstvo	k1gNnSc1	společenstvo
prstenu	prsten	k1gInSc2	prsten
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Imagine	Imagin	k1gInSc5	Imagin
<g/>
"	"	kIx"	"
od	od	k7c2	od
Deža	Dežus	k1gMnSc2	Dežus
Ursinyho	Ursiny	k1gMnSc2	Ursiny
je	být	k5eAaImIp3nS	být
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
památce	památka	k1gFnSc3	památka
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
je	být	k5eAaImIp3nS	být
remakem	remak	k1gInSc7	remak
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
skladby	skladba	k1gFnSc2	skladba
od	od	k7c2	od
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
2	[number]	k4	2
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
původní	původní	k2eAgInSc4d1	původní
anglicky	anglicky	k6eAd1	anglicky
zpívaný	zpívaný	k2eAgInSc4d1	zpívaný
text	text	k1gInSc4	text
a	a	k8xC	a
remakový	remakový	k2eAgInSc4d1	remakový
slovensky	slovensky	k6eAd1	slovensky
zpívaný	zpívaný	k2eAgInSc4d1	zpívaný
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Motiv	motiv	k1gInSc1	motiv
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
Deža	Dežus	k1gMnSc2	Dežus
Ursinyho	Ursiny	k1gMnSc2	Ursiny
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
textu	text	k1gInSc6	text
skladby	skladba	k1gFnSc2	skladba
Kedy	Keda	k1gFnSc2	Keda
ak	ak	k?	ak
nie	nie	k?	nie
teraz	teraz	k1gInSc1	teraz
Upálení	upálení	k1gNnSc1	upálení
je	být	k5eAaImIp3nS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
v	v	k7c6	v
jednom	jeden	k4xCgMnSc6	jeden
z	z	k7c2	z
dílů	díl	k1gInPc2	díl
seriálu	seriál	k1gInSc2	seriál
Vyprávěj	vyprávět	k5eAaImRp2nS	vyprávět
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
činu	čin	k1gInSc6	čin
a	a	k8xC	a
následujících	následující	k2eAgFnPc6d1	následující
událostech	událost	k1gFnPc6	událost
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
blízkými	blízký	k2eAgMnPc7d1	blízký
byla	být	k5eAaImAgFnS	být
natočena	natočen	k2eAgFnSc1d1	natočena
filmová	filmový	k2eAgFnSc1d1	filmová
trilogie	trilogie	k1gFnSc1	trilogie
Hořící	hořící	k2eAgFnSc1d1	hořící
keř	keř	k1gInSc1	keř
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc1	pomník
upomínající	upomínající	k2eAgFnSc1d1	upomínající
smrt	smrt	k1gFnSc1	smrt
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
na	na	k7c6	na
budově	budova	k1gFnSc6	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
polikliniky	poliklinika	k1gFnSc2	poliklinika
popálenin	popálenina	k1gFnPc2	popálenina
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Legerově	Legerův	k2eAgFnSc6d1	Legerova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Pochodeň	pochodeň	k1gFnSc1	pochodeň
<g/>
"	"	kIx"	"
z	z	k7c2	z
alba	album	k1gNnSc2	album
Kotlina	kotlina	k1gFnSc1	kotlina
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Epydemye	Epydemy	k1gFnSc2	Epydemy
<g/>
.	.	kIx.	.
</s>
