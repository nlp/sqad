<s>
Bambi	Bambi	k6eAd1	Bambi
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
<g/>
:	:	kIx,	:
kniha	kniha	k1gFnSc1	kniha
Bambi	Bamb	k1gFnSc2	Bamb
(	(	kIx(	(
<g/>
postava	postava	k1gFnSc1	postava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvířecí	zvířecí	k2eAgFnSc1d1	zvířecí
postava	postava	k1gFnSc1	postava
jelena	jelen	k1gMnSc2	jelen
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
rakouského	rakouský	k2eAgNnSc2d1	rakouské
spisovatele	spisovatel	k1gMnPc4	spisovatel
Felixe	Felix	k1gMnSc4	Felix
Saltena	Salten	k2eAgMnSc4d1	Salten
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
Bambi	Bamb	k1gFnSc2	Bamb
<g/>
.	.	kIx.	.
</s>
<s>
Eine	Einus	k1gMnSc5	Einus
Lebensgeschichte	Lebensgeschicht	k1gMnSc5	Lebensgeschicht
aus	aus	k?	aus
dem	dem	k?	dem
Walde	Wald	k1gInSc5	Wald
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
přeloženo	přeložit	k5eAaPmNgNnS	přeložit
<g/>
:	:	kIx,	:
<g/>
Bambi	Bambe	k1gFnSc4	Bambe
<g/>
,	,	kIx,	,
život	život	k1gInSc4	život
v	v	k7c6	v
lese	les	k1gInSc6	les
nebo	nebo	k8xC	nebo
Bambi	Bamb	k1gInSc6	Bamb
<g/>
,	,	kIx,	,
životní	životní	k2eAgInSc1d1	životní
příběh	příběh	k1gInSc1	příběh
z	z	k7c2	z
lesů	les	k1gInPc2	les
<g/>
)	)	kIx)	)
film	film	k1gInSc1	film
Bambi	Bambi	k1gNnSc1	Bambi
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
dětská	dětský	k2eAgFnSc1d1	dětská
pohádka	pohádka	k1gFnSc1	pohádka
<g />
.	.	kIx.	.
</s>
<s>
studia	studio	k1gNnPc1	studio
Walta	Walto	k1gNnSc2	Walto
Disneye	Disneye	k1gFnPc2	Disneye
natočená	natočený	k2eAgFnSc1d1	natočená
podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
Felixe	Felix	k1gMnSc2	Felix
Saltena	Salten	k2eAgNnPc4d1	Salten
Bambi	Bambi	k1gNnPc4	Bambi
Meets	Meetsa	k1gFnPc2	Meetsa
Godzilla	Godzilla	k1gFnSc1	Godzilla
<g/>
,	,	kIx,	,
kanadský	kanadský	k2eAgInSc1d1	kanadský
krátkometrážní	krátkometrážní	k2eAgInSc1d1	krátkometrážní
animovaný	animovaný	k2eAgInSc1d1	animovaný
snímek	snímek	k1gInSc1	snímek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
Bambi	Bambi	k1gNnSc2	Bambi
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sovětský	sovětský	k2eAgInSc4d1	sovětský
hraný	hraný	k2eAgInSc4d1	hraný
fantasy	fantas	k1gInPc4	fantas
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Natalie	Natalie	k1gFnSc1	Natalie
Bondarčuková	Bondarčukový	k2eAgFnSc1d1	Bondarčuková
Bambiho	Bambi	k1gMnSc2	Bambi
dospívání	dospívání	k1gNnSc4	dospívání
<g/>
,	,	kIx,	,
sovětský	sovětský	k2eAgInSc4d1	sovětský
hraný	hraný	k2eAgInSc4d1	hraný
fantasy	fantas	k1gInPc4	fantas
<g />
.	.	kIx.	.
</s>
<s>
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Natalie	Natalie	k1gFnSc2	Natalie
Bondarčuková	Bondarčukový	k2eAgFnSc1d1	Bondarčuková
Bambi	Bambi	k1gNnSc7	Bambi
2	[number]	k4	2
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
filmová	filmový	k2eAgFnSc1d1	filmová
pohádka	pohádka	k1gFnSc1	pohádka
studia	studio	k1gNnSc2	studio
Walta	Walt	k1gMnSc2	Walt
Disneye	Disney	k1gMnSc2	Disney
<g/>
,	,	kIx,	,
doplněk	doplněk	k1gInSc1	doplněk
a	a	k8xC	a
pokračování	pokračování	k1gNnSc1	pokračování
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
filmu	film	k1gInSc2	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
Bambi	Bambi	k1gNnSc2	Bambi
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgInSc1d1	francouzský
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
životopisný	životopisný	k2eAgInSc4d1	životopisný
<g />
.	.	kIx.	.
</s>
<s>
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
Hello	Hello	k1gNnSc4	Hello
Bambi	Bamb	k1gFnSc2	Bamb
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
krátký	krátký	k2eAgInSc1d1	krátký
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
jiný	jiný	k2eAgInSc1d1	jiný
význam	význam	k1gInSc1	význam
Bambi	Bambi	k1gNnSc2	Bambi
(	(	kIx(	(
<g/>
cena	cena	k1gFnSc1	cena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německá	německý	k2eAgFnSc1d1	německá
umělecká	umělecký	k2eAgFnSc1d1	umělecká
mediální	mediální	k2eAgFnSc1d1	mediální
a	a	k8xC	a
televizní	televizní	k2eAgFnSc1d1	televizní
cena	cena	k1gFnSc1	cena
Bambi	Bamb	k1gFnSc2	Bamb
(	(	kIx(	(
<g/>
Angola	Angola	k1gFnSc1	Angola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
Angole	Angola	k1gFnSc6	Angola
Bambi	Bamb	k1gFnSc2	Bamb
(	(	kIx(	(
<g/>
firma	firma	k1gFnSc1	firma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srbská	srbský	k2eAgFnSc1d1	Srbská
potravinářská	potravinářský	k2eAgFnSc1d1	potravinářská
společnost	společnost	k1gFnSc1	společnost
Bambieffekten	Bambieffektno	k1gNnPc2	Bambieffektno
<g/>
,	,	kIx,	,
norský	norský	k2eAgInSc1d1	norský
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
</s>
<s>
Filmu	film	k1gInSc3	film
se	se	k3xPyFc4	se
režisérsky	režisérsky	k6eAd1	režisérsky
ujal	ujmout	k5eAaPmAgInS	ujmout
Davidem	David	k1gMnSc7	David
Handem	Hand	k1gMnSc7	Hand
a	a	k8xC	a
producentem	producent	k1gMnSc7	producent
byl	být	k5eAaImAgMnS	být
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
<g/>
.	.	kIx.	.
</s>
