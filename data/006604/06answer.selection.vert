<s>
Buňky	buňka	k1gFnPc1	buňka
sinic	sinice	k1gFnPc2	sinice
jsou	být	k5eAaImIp3nP	být
jednobuněčné	jednobuněčný	k2eAgFnPc1d1	jednobuněčná
či	či	k8xC	či
vláknité	vláknitý	k2eAgFnPc1d1	vláknitá
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
modrozeleně	modrozeleně	k6eAd1	modrozeleně
zbarvené	zbarvený	k2eAgFnPc1d1	zbarvená
a	a	k8xC	a
v	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
ohledech	ohled	k1gInPc6	ohled
typicky	typicky	k6eAd1	typicky
prokaryotické	prokaryotický	k2eAgFnPc1d1	prokaryotická
<g/>
:	:	kIx,	:
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
kruhovou	kruhový	k2eAgFnSc4d1	kruhová
molekulu	molekula	k1gFnSc4	molekula
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
bakteriální	bakteriální	k2eAgInSc1d1	bakteriální
typ	typ	k1gInSc1	typ
ribozomů	ribozom	k1gInPc2	ribozom
a	a	k8xC	a
chybí	chybit	k5eAaPmIp3nS	chybit
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
složitější	složitý	k2eAgFnSc1d2	složitější
membránové	membránový	k2eAgFnPc4d1	membránová
struktury	struktura	k1gFnPc4	struktura
<g/>
.	.	kIx.	.
</s>
