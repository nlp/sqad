<s>
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
:	:	kIx,
大	大	k?
–	–	k?
Dai	Dai	k1gMnPc2
Nippon	Nippon	k1gMnSc1
Teikoku	Teikok	k1gInSc2
<g/>
,	,	kIx,
doslova	doslova	k6eAd1
Velké	velký	k2eAgNnSc1d1
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
státní	státní	k2eAgInSc4d1
útvar	útvar	k1gInSc4
existující	existující	k2eAgInSc4d1
v	v	k7c6
letech	let	k1gInPc6
1868	#num#	k4
<g/>
–	–	k?
<g/>
1947	#num#	k4
na	na	k7c6
území	území	k1gNnSc6
nástupnického	nástupnický	k2eAgNnSc2d1
Japonska	Japonsko	k1gNnSc2
<g/>
,	,	kIx,
Kurilských	kurilský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
,	,	kIx,
Severních	severní	k2eAgFnPc2d1
Marian	Mariana	k1gFnPc2
<g/>
,	,	kIx,
Tchaj-wanu	Tchaj-wan	k1gInSc2
a	a	k8xC
po	po	k7c6
okupaci	okupace	k1gFnSc6
roku	rok	k1gInSc2
1910	#num#	k4
i	i	k9
Koreje	Korea	k1gFnPc1
<g/>
.	.	kIx.
</s>