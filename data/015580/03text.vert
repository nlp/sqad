<s>
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
Veliké	veliký	k2eAgNnSc1d1
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
大	大	k?
Dai	Dai	k1gMnSc1
Nippon	Nippon	k1gMnSc1
Teikoku	Teikok	k1gInSc2
</s>
<s>
↓	↓	k?
</s>
<s>
1868	#num#	k4
<g/>
–	–	k?
<g/>
1947	#num#	k4
</s>
<s>
↓	↓	k?
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Hymna	hymna	k1gFnSc1
<g/>
:	:	kIx,
君	君	k?
(	(	kIx(
<g/>
Kimigajo	Kimigajo	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Motto	motto	k1gNnSc1
<g/>
:	:	kIx,
五	五	k?
<g/>
,	,	kIx,
八	八	k?
</s>
<s>
geografie	geografie	k1gFnSc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
(	(	kIx(
<g/>
tmavě	tmavě	k6eAd1
zelená	zelený	k2eAgFnSc1d1
<g/>
)	)	kIx)
kolem	kolem	k7c2
roku	rok	k1gInSc2
1942	#num#	k4
se	se	k3xPyFc4
svými	svůj	k3xOyFgFnPc7
koloniemi	kolonie	k1gFnPc7
<g/>
,	,	kIx,
protektoráty	protektorát	k1gInPc7
a	a	k8xC
loutkovými	loutkový	k2eAgInPc7d1
státy	stát	k1gInPc7
</s>
<s>
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Kjóto	Kjóto	k1gNnSc1
(	(	kIx(
<g/>
1868	#num#	k4
<g/>
–	–	k?
<g/>
1869	#num#	k4
<g/>
)	)	kIx)
<g/>
Tokio-ši	Tokio-še	k1gFnSc6
(	(	kIx(
<g/>
1869	#num#	k4
<g/>
–	–	k?
<g/>
1943	#num#	k4
<g/>
)	)	kIx)
<g/>
Tokio	Tokio	k1gNnSc4
(	(	kIx(
<g/>
1943	#num#	k4
<g/>
–	–	k?
<g/>
1947	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
rozloha	rozloha	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
984	#num#	k4
000	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
rok	rok	k1gInSc1
1938	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
:	:	kIx,
</s>
<s>
105	#num#	k4
200	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
rok	rok	k1gInSc1
1940	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
národnostní	národnostní	k2eAgNnSc1d1
složení	složení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Japonci	Japonec	k1gMnPc1
<g/>
,	,	kIx,
Ainuové	Ainuus	k1gMnPc1
<g/>
,	,	kIx,
Mandžuové	Mandžu	k1gMnPc1
<g/>
,	,	kIx,
Korejci	Korejec	k1gMnPc1
<g/>
,	,	kIx,
Číňané	Číňan	k1gMnPc1
</s>
<s>
jazyky	jazyk	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
japonština	japonština	k1gFnSc1
</s>
<s>
náboženství	náboženství	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
de	de	k?
iure	iurat	k5eAaPmIp3nS
žádnéde	žádnéde	k6eAd1
facto	fact	k2eAgNnSc4d1
státní	státní	k2eAgInSc4d1
šintoismus	šintoismus	k1gInSc4
<g/>
,	,	kIx,
<g/>
lidově	lidově	k6eAd1
také	také	k9
buddhismus	buddhismus	k1gInSc1
</s>
<s>
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
</s>
<s>
státní	státní	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
feudální	feudální	k2eAgFnSc1d1
absolutní	absolutní	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
(	(	kIx(
<g/>
1868	#num#	k4
<g/>
–	–	k?
<g/>
1885	#num#	k4
<g/>
)	)	kIx)
<g/>
konstituční	konstituční	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
(	(	kIx(
<g/>
1885	#num#	k4
<g/>
–	–	k?
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
<g/>
vojenská	vojenský	k2eAgFnSc1d1
diktatura	diktatura	k1gFnSc1
s	s	k7c7
konstituční	konstituční	k2eAgFnSc7d1
monarchií	monarchie	k1gFnSc7
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
–	–	k?
<g/>
1940	#num#	k4
<g/>
)	)	kIx)
<g/>
totalitární	totalitární	k2eAgFnSc1d1
diktatura	diktatura	k1gFnSc1
jedné	jeden	k4xCgFnSc2
strany	strana	k1gFnSc2
s	s	k7c7
konstituční	konstituční	k2eAgFnSc7d1
monarchií	monarchie	k1gFnSc7
(	(	kIx(
<g/>
1940	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
okupační	okupační	k2eAgFnSc1d1
správa	správa	k1gFnSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1947	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
měna	měna	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
jen	jen	k9
</s>
<s>
vznik	vznik	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
1868	#num#	k4
</s>
<s>
zánik	zánik	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
1947	#num#	k4
</s>
<s>
státní	státní	k2eAgInPc4d1
útvary	útvar	k1gInPc4
a	a	k8xC
území	území	k1gNnSc4
</s>
<s>
předcházející	předcházející	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
Šógunát	Šógunát	k1gInSc1
Tokugawa	Tokugaw	k1gInSc2
</s>
<s>
Republika	republika	k1gFnSc1
Ezo	Ezo	k1gFnSc2
</s>
<s>
Království	království	k1gNnSc1
Rjúkjú	Rjúkjú	k1gNnSc2
</s>
<s>
Republika	republika	k1gFnSc1
Tchaj-wan	Tchaj-wan	k1gInSc1
</s>
<s>
Říše	říše	k1gFnSc1
Čching	Čching	k1gInSc1
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
</s>
<s>
Korejské	korejský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
Německá	německý	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
</s>
<s>
Nizozemská	nizozemský	k2eAgFnSc1d1
východní	východní	k2eAgFnSc1d1
Indie	Indie	k1gFnSc1
</s>
<s>
následující	následující	k2eAgInPc4d1
<g/>
:	:	kIx,
</s>
<s>
Okupace	okupace	k1gFnSc1
Japonska	Japonsko	k1gNnSc2
</s>
<s>
Americká	americký	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
správa	správa	k1gFnSc1
souostroví	souostroví	k1gNnSc2
Rjúkjú	Rjúkjú	k1gNnSc2
</s>
<s>
Americká	americký	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
Koreje	Korea	k1gFnSc2
</s>
<s>
Poručenské	poručenský	k2eAgNnSc4d1
území	území	k1gNnSc4
Tichomořské	tichomořský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1
civilní	civilní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
</s>
<s>
Sachalinská	sachalinský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Indonésie	Indonésie	k1gFnSc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
:	:	kIx,
大	大	k?
–	–	k?
Dai	Dai	k1gMnPc2
Nippon	Nippon	k1gMnSc1
Teikoku	Teikok	k1gInSc2
<g/>
,	,	kIx,
doslova	doslova	k6eAd1
Velké	velký	k2eAgNnSc1d1
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
státní	státní	k2eAgInSc4d1
útvar	útvar	k1gInSc4
existující	existující	k2eAgInSc4d1
v	v	k7c6
letech	let	k1gInPc6
1868	#num#	k4
<g/>
–	–	k?
<g/>
1947	#num#	k4
na	na	k7c6
území	území	k1gNnSc6
nástupnického	nástupnický	k2eAgNnSc2d1
Japonska	Japonsko	k1gNnSc2
<g/>
,	,	kIx,
Kurilských	kurilský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
,	,	kIx,
Severních	severní	k2eAgFnPc2d1
Marian	Mariana	k1gFnPc2
<g/>
,	,	kIx,
Tchaj-wanu	Tchaj-wan	k1gInSc2
a	a	k8xC
po	po	k7c6
okupaci	okupace	k1gFnSc6
roku	rok	k1gInSc2
1910	#num#	k4
i	i	k9
Koreje	Korea	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládli	vládnout	k5eAaImAgMnP
mu	on	k3xPp3gMnSc3
císařové	císařová	k1gFnPc4
z	z	k7c2
dynastie	dynastie	k1gFnSc2
Jamato	Jamat	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Císařství	císařství	k1gNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
po	po	k7c6
pádu	pád	k1gInSc6
šógunátu	šógunát	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1868	#num#	k4
a	a	k8xC
zaniklo	zaniknout	k5eAaPmAgNnS
po	po	k7c6
prohrané	prohraná	k1gFnSc6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Dějiny	dějiny	k1gFnPc1
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Historická	historický	k2eAgNnPc1d1
období	období	k1gNnPc1
</s>
<s>
Džómon	Džómon	k1gNnSc1
(	(	kIx(
<g/>
縄	縄	k?
10	#num#	k4
000	#num#	k4
–	–	k?
300	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
</s>
<s>
Jajoi	Jajoi	k1gNnSc1
(	(	kIx(
<g/>
弥	弥	k?
300	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
–	–	k?
710	#num#	k4
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
</s>
<s>
Nara	Nara	k1gFnSc1
(	(	kIx(
<g/>
奈	奈	k?
710	#num#	k4
<g/>
–	–	k?
<g/>
794	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Heian	Heian	k1gInSc1
(	(	kIx(
<g/>
平	平	k?
794	#num#	k4
<g/>
–	–	k?
<g/>
1185	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kamakura	Kamakura	k1gFnSc1
(	(	kIx(
<g/>
鎌	鎌	k?
1185	#num#	k4
<g/>
–	–	k?
<g/>
1333	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Muromači	Muromač	k1gMnPc1
(	(	kIx(
<g/>
室	室	k?
1333	#num#	k4
<g/>
–	–	k?
<g/>
1568	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Azuči-Momojama	Azuči-Momojama	k1gFnSc1
(	(	kIx(
<g/>
安	安	k?
1568	#num#	k4
<g/>
–	–	k?
<g/>
1600	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tokugawa	Tokugawa	k6eAd1
<g/>
,	,	kIx,
také	také	k9
Edo	Eda	k1gMnSc5
(	(	kIx(
<g/>
江	江	k?
1600	#num#	k4
<g/>
–	–	k?
<g/>
1868	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Meidži	Meidzat	k5eAaPmIp1nS
(	(	kIx(
<g/>
明	明	k?
1868	#num#	k4
<g/>
–	–	k?
<g/>
1912	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Taišó	Taišó	k?
(	(	kIx(
<g/>
大	大	k?
1912	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Šówa	Šówa	k1gFnSc1
(	(	kIx(
<g/>
昭	昭	k?
1926	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Heisei	Heisei	k1gNnSc1
(	(	kIx(
<g/>
平	平	k?
1989	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Reiwa	Reiwa	k1gFnSc1
(	(	kIx(
<g/>
令	令	k?
od	od	k7c2
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Od	od	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
přijížděli	přijíždět	k5eAaImAgMnP
do	do	k7c2
Japonska	Japonsko	k1gNnSc2
obchodníci	obchodník	k1gMnPc1
a	a	k8xC
křesťanští	křesťanský	k2eAgMnPc1d1
misionáři	misionář	k1gMnPc1
z	z	k7c2
Portugalska	Portugalsko	k1gNnSc2
<g/>
,	,	kIx,
Španělska	Španělsko	k1gNnSc2
<g/>
,	,	kIx,
Nizozemí	Nizozemí	k1gNnSc2
a	a	k8xC
Anglie	Anglie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
podezříval	podezřívat	k5eAaImAgMnS
japonský	japonský	k2eAgInSc4d1
šógunát	šógunát	k1gInSc4
katolické	katolický	k2eAgMnPc4d1
misionáře	misionář	k1gMnPc4
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
předvojem	předvoj	k1gInSc7
ozbrojené	ozbrojený	k2eAgFnSc2d1
iberské	iberský	k2eAgFnSc2d1
invaze	invaze	k1gFnSc2
a	a	k8xC
okamžitě	okamžitě	k6eAd1
zakázal	zakázat	k5eAaPmAgInS
veškeré	veškerý	k3xTgInPc4
styky	styk	k1gInPc4
s	s	k7c7
Evropany	Evropan	k1gMnPc7
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
významně	významně	k6eAd1
omezených	omezený	k2eAgInPc2d1
kontaktů	kontakt	k1gInPc2
s	s	k7c7
protestantskými	protestantský	k2eAgMnPc7d1
nizozemskými	nizozemský	k2eAgMnPc7d1
obchodníky	obchodník	k1gMnPc7
na	na	k7c6
umělém	umělý	k2eAgInSc6d1
ostrůvku	ostrůvek	k1gInSc6
Dedžima	Dedžima	k1gFnSc1
(	(	kIx(
<g/>
také	také	k9
Dešima	Dešimum	k1gNnSc2
<g/>
)	)	kIx)
u	u	k7c2
Nagasaki	Nagasaki	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čínským	čínský	k2eAgNnSc7d1
lodím	loď	k1gFnPc3
bylo	být	k5eAaImAgNnS
nadále	nadále	k6eAd1
povoleno	povolit	k5eAaPmNgNnS
vjíždět	vjíždět	k5eAaImF
do	do	k7c2
Nagasaki	Nagasaki	k1gNnSc2
a	a	k8xC
korejští	korejský	k2eAgMnPc1d1
vyslanci	vyslanec	k1gMnPc1
měli	mít	k5eAaImAgMnP
přístup	přístup	k1gInSc4
do	do	k7c2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
izolace	izolace	k1gFnSc1
trvala	trvat	k5eAaImAgFnS
251	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
dokud	dokud	k8xS
si	se	k3xPyFc3
komodor	komodor	k1gMnSc1
Matthew	Matthew	k1gMnSc1
Perry	Perra	k1gFnSc2
nevynutil	vynutit	k5eNaPmAgMnS
otevření	otevření	k1gNnSc3
japonských	japonský	k2eAgMnPc2d1
přístavů	přístav	k1gInPc2
pro	pro	k7c4
americké	americký	k2eAgMnPc4d1
obchodníky	obchodník	k1gMnPc4
v	v	k7c6
roce	rok	k1gInSc6
1854	#num#	k4
na	na	k7c6
Konferenci	konference	k1gFnSc6
v	v	k7c6
Kanagawě	Kanagawa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
podpisu	podpis	k1gInSc3
obdobných	obdobný	k2eAgFnPc2d1
smluv	smlouva	k1gFnPc2
(	(	kIx(
<g/>
Ansejské	Ansejský	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
<g/>
)	)	kIx)
i	i	k9
s	s	k7c7
evropskými	evropský	k2eAgFnPc7d1
mocnostmi	mocnost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
</s>
<s>
Během	během	k7c2
několika	několik	k4yIc2
let	léto	k1gNnPc2
obnovený	obnovený	k2eAgInSc1d1
kontakt	kontakt	k1gInSc1
se	s	k7c7
Západem	západ	k1gInSc7
zásadně	zásadně	k6eAd1
změnil	změnit	k5eAaPmAgInS
japonskou	japonský	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
Bošin	Bošina	k1gFnPc2
v	v	k7c6
letech	let	k1gInPc6
1867	#num#	k4
<g/>
–	–	k?
<g/>
1868	#num#	k4
byl	být	k5eAaImAgInS
šógunát	šógunát	k1gInSc1
zrušen	zrušit	k5eAaPmNgInS
a	a	k8xC
byla	být	k5eAaImAgFnS
znovuobnovena	znovuobnoven	k2eAgFnSc1d1
moc	moc	k1gFnSc1
císaře	císař	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1867	#num#	k4
nastoupil	nastoupit	k5eAaPmAgInS
na	na	k7c4
Chryzantémový	chryzantémový	k2eAgInSc4d1
trůn	trůn	k1gInSc4
nový	nový	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Mucuhito	Mucuhit	k2eAgNnSc1d1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
známý	známý	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
císař	císař	k1gMnSc1
éry	éra	k1gFnSc2
Meidži	Meidž	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
během	během	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
45	#num#	k4
<g/>
leté	letý	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
se	se	k3xPyFc4
uskutečnilo	uskutečnit	k5eAaPmAgNnS
mnoho	mnoho	k4c1
reforem	reforma	k1gFnPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
reformy	reforma	k1gFnSc2
Meidži	Meidž	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Feudální	feudální	k2eAgInSc1d1
systém	systém	k1gInSc1
byl	být	k5eAaImAgInS
zrušen	zrušit	k5eAaPmNgInS
a	a	k8xC
byly	být	k5eAaImAgFnP
převzaty	převzít	k5eAaPmNgFnP
četné	četný	k2eAgFnPc1d1
západní	západní	k2eAgFnPc1d1
instituce	instituce	k1gFnPc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
západního	západní	k2eAgInSc2d1
právního	právní	k2eAgInSc2d1
řádu	řád	k1gInSc2
a	a	k8xC
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgFnPc7d1
ekonomickými	ekonomický	k2eAgFnPc7d1
<g/>
,	,	kIx,
sociálními	sociální	k2eAgFnPc7d1
a	a	k8xC
vojenskými	vojenský	k2eAgFnPc7d1
reformami	reforma	k1gFnPc7
vyústily	vyústit	k5eAaPmAgFnP
tyto	tento	k3xDgFnPc1
změny	změna	k1gFnPc1
k	k	k7c3
přerodu	přerod	k1gInSc2
Japonska	Japonsko	k1gNnSc2
do	do	k7c2
moderní	moderní	k2eAgFnSc2d1
světové	světový	k2eAgFnSc2d1
mocnosti	mocnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
výsledek	výsledek	k1gInSc4
první	první	k4xOgFnSc2
čínsko-japonské	čínsko-japonský	k2eAgFnSc2d1
a	a	k8xC
rusko-japonské	rusko-japonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
získalo	získat	k5eAaPmAgNnS
Japonsko	Japonsko	k1gNnSc1
Tchaj-wan	Tchaj-wan	k1gInSc1
a	a	k8xC
Sachalin	Sachalin	k1gInSc1
a	a	k8xC
později	pozdě	k6eAd2
v	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
okupovalo	okupovat	k5eAaBmAgNnS
Koreu	Korea	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Japonský	japonský	k2eAgInSc1d1
fašismus	fašismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
zaznamenalo	zaznamenat	k5eAaPmAgNnS
Japonsko	Japonsko	k1gNnSc1
vzrůstající	vzrůstající	k2eAgNnSc1d1
vliv	vliv	k1gInSc4
expanzivního	expanzivní	k2eAgInSc2d1
militarismu	militarismus	k1gInSc2
<g/>
,	,	kIx,
vedoucímu	vedoucí	k1gMnSc3
k	k	k7c3
invazi	invaze	k1gFnSc3
do	do	k7c2
Mandžuska	Mandžusko	k1gNnSc2
a	a	k8xC
druhé	druhý	k4xOgFnSc3
čínsko-japonské	čínsko-japonský	k2eAgFnSc3d1
válce	válka	k1gFnSc3
(	(	kIx(
<g/>
1937	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonsko	Japonsko	k1gNnSc1
se	se	k3xPyFc4
spojilo	spojit	k5eAaPmAgNnS
s	s	k7c7
Německem	Německo	k1gNnSc7
a	a	k8xC
Itálií	Itálie	k1gFnSc7
a	a	k8xC
zformovalo	zformovat	k5eAaPmAgNnS
Osu	osa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonští	japonský	k2eAgMnPc1d1
vůdci	vůdce	k1gMnPc1
považovali	považovat	k5eAaImAgMnP
za	za	k7c4
nezbytné	nezbytný	k2eAgNnSc4d1,k2eNgNnSc4d1
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
americkou	americký	k2eAgFnSc4d1
námořní	námořní	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
v	v	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbor	k1gInSc1
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byla	být	k5eAaImAgFnS
zajištěna	zajištěn	k2eAgFnSc1d1
japonská	japonský	k2eAgFnSc1d1
nadvláda	nadvláda	k1gFnSc1
v	v	k7c6
Pacifiku	Pacifik	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
vstup	vstup	k1gInSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
do	do	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
postupně	postupně	k6eAd1
změnil	změnit	k5eAaPmAgInS
rovnováhu	rovnováha	k1gFnSc4
sil	síla	k1gFnPc2
v	v	k7c6
Pacifiku	Pacifik	k1gInSc6
v	v	k7c4
neprospěch	neprospěch	k1gInSc4
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dlouhém	dlouhý	k2eAgNnSc6d1
pacifickém	pacifický	k2eAgNnSc6d1
tažení	tažení	k1gNnSc6
Japonsko	Japonsko	k1gNnSc1
ztratilo	ztratit	k5eAaPmAgNnS
Okinawu	Okinawa	k1gFnSc4
v	v	k7c6
souostroví	souostroví	k1gNnSc6
Rjúkjú	Rjúkjú	k1gNnSc2
a	a	k8xC
bylo	být	k5eAaImAgNnS
zatlačeno	zatlačit	k5eAaPmNgNnS
na	na	k7c4
čtyři	čtyři	k4xCgInPc4
hlavní	hlavní	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
mohutně	mohutně	k6eAd1
zaútočily	zaútočit	k5eAaPmAgInP
na	na	k7c4
Tokio	Tokio	k1gNnSc4
<g/>
,	,	kIx,
Ósaku	Ósaka	k1gFnSc4
a	a	k8xC
další	další	k2eAgNnPc4d1
města	město	k1gNnPc4
strategickým	strategický	k2eAgNnSc7d1
bombardováním	bombardování	k1gNnSc7
a	a	k8xC
na	na	k7c4
Hirošimu	Hirošima	k1gFnSc4
a	a	k8xC
Nagasaki	Nagasaki	k1gNnSc1
atomovými	atomový	k2eAgFnPc7d1
bombami	bomba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
vypracován	vypracovat	k5eAaPmNgInS
i	i	k9
plán	plán	k1gInSc1
invaze	invaze	k1gFnSc2
na	na	k7c4
japonské	japonský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
Japonsko	Japonsko	k1gNnSc1
oznámilo	oznámit	k5eAaPmAgNnS
bezpodmínečnou	bezpodmínečný	k2eAgFnSc4d1
kapitulaci	kapitulace	k1gFnSc4
15	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1945	#num#	k4
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
byla	být	k5eAaImAgFnS
kapitulece	kapituleko	k6eAd1
podepsána	podepsán	k2eAgFnSc1d1
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
definitivně	definitivně	k6eAd1
skončila	skončit	k5eAaPmAgFnS
druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
a	a	k8xC
i	i	k9
samotné	samotný	k2eAgNnSc1d1
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
přešlo	přejít	k5eAaPmAgNnS
pod	pod	k7c4
okupační	okupační	k2eAgFnSc4d1
správu	správa	k1gFnSc4
spojenců	spojenec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Systém	systém	k1gInSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Základním	základní	k2eAgInSc7d1
zákonem	zákon	k1gInSc7
Japonského	japonský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
byla	být	k5eAaImAgFnS
ústava	ústava	k1gFnSc1
známá	známý	k2eAgFnSc1d1
též	též	k9
jako	jako	k8xC,k8xS
Ústava	ústava	k1gFnSc1
Meidži	Meidž	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
vyhlášena	vyhlášen	k2eAgFnSc1d1
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1890	#num#	k4
a	a	k8xC
platila	platit	k5eAaImAgFnS
až	až	k6eAd1
do	do	k7c2
2	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1947	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prohlásila	prohlásit	k5eAaPmAgFnS
Japonsko	Japonsko	k1gNnSc4
konstituční	konstituční	k2eAgFnSc7d1
monarchií	monarchie	k1gFnSc7
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
císařem	císař	k1gMnSc7
<g/>
,	,	kIx,
zajišťovala	zajišťovat	k5eAaImAgFnS
oddělení	oddělení	k1gNnSc4
zákonodárné	zákonodárný	k2eAgFnSc2d1
<g/>
,	,	kIx,
výkonné	výkonný	k2eAgFnSc2d1
a	a	k8xC
soudní	soudní	k2eAgFnSc2d1
moci	moc	k1gFnSc2
a	a	k8xC
základní	základní	k2eAgNnPc4d1
lidská	lidský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Císař	Císař	k1gMnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Japonský	japonský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Císař	Císař	k1gMnSc1
Šówa	Šówa	k1gMnSc1
(	(	kIx(
<g/>
Hirohito	Hirohit	k2eAgNnSc1d1
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
</s>
<s>
I	i	k9
přes	přes	k7c4
demokratické	demokratický	k2eAgNnSc4d1
zřízení	zřízení	k1gNnSc4
<g/>
,	,	kIx,
zůstávala	zůstávat	k5eAaImAgFnS
většina	většina	k1gFnSc1
moci	moct	k5eAaImF
v	v	k7c6
rukou	ruka	k1gFnPc6
císaře	císař	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc1
titul	titul	k1gInSc4
zněl	znět	k5eAaImAgMnS
Vládce	vládce	k1gMnSc1
nebes	nebesa	k1gNnPc2
(	(	kIx(
<g/>
天	天	k?
-	-	kIx~
Tennó	tennó	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
byl	být	k5eAaImAgMnS
nadále	nadále	k6eAd1
považován	považován	k2eAgMnSc1d1
za	za	k7c7
„	„	k?
<g/>
posvátného	posvátný	k2eAgNnSc2d1
a	a	k8xC
nedotknutelného	nedotknutelného	k?
<g/>
“	“	k?
vládce	vládce	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
rukou	ruka	k1gFnPc6
držel	držet	k5eAaImAgMnS
zákonodárnou	zákonodárný	k2eAgFnSc4d1
<g/>
,	,	kIx,
výkonnou	výkonný	k2eAgFnSc4d1
i	i	k8xC
soudní	soudní	k2eAgFnSc4d1
moc	moc	k1gFnSc4
<g/>
,	,	kIx,
veškeré	veškerý	k3xTgInPc1
zákony	zákon	k1gInPc1
<g/>
,	,	kIx,
vyhlášky	vyhláška	k1gFnPc1
a	a	k8xC
rozsudky	rozsudek	k1gInPc1
byly	být	k5eAaImAgInP
vydávány	vydávat	k5eAaImNgInP,k5eAaPmNgInP
„	„	k?
<g/>
Ve	v	k7c6
jménu	jméno	k1gNnSc6
Císaře	Císař	k1gMnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výkonnou	výkonný	k2eAgFnSc4d1
moc	moc	k1gFnSc4
vykonával	vykonávat	k5eAaImAgInS
nepřímo	přímo	k6eNd1
<g/>
,	,	kIx,
prostřednictvím	prostřednictvím	k7c2
Tajné	tajný	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
členy	člen	k1gMnPc4
a	a	k8xC
všechny	všechen	k3xTgMnPc4
vládní	vládní	k2eAgMnPc4d1
úředníky	úředník	k1gMnPc4
libovolně	libovolně	k6eAd1
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
a	a	k8xC
odvolával	odvolávat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
císařskou	císařský	k2eAgFnSc7d1
výsadou	výsada	k1gFnSc7
bylo	být	k5eAaImAgNnS
velení	velení	k1gNnSc1
japonské	japonský	k2eAgFnSc2d1
armádě	armáda	k1gFnSc3
a	a	k8xC
námořnictvu	námořnictvo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1868	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
Japonském	japonský	k2eAgInSc6d1
trůně	trůn	k1gInSc6
vystřídali	vystřídat	k5eAaPmAgMnP
tři	tři	k4xCgMnPc1
císaři	císař	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Meidži	Meidzat	k5eAaPmIp1nS
(	(	kIx(
<g/>
Mucuhito	Mucuhit	k2eAgNnSc1d1
<g/>
)	)	kIx)
1867	#num#	k4
<g/>
–	–	k?
<g/>
1912	#num#	k4
</s>
<s>
Taišó	Taišó	k?
(	(	kIx(
<g/>
Jošihito	Jošihit	k2eAgNnSc1d1
<g/>
)	)	kIx)
1912	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
</s>
<s>
Šówa	Šówa	k1gFnSc1
(	(	kIx(
<g/>
Hirohito	Hirohit	k2eAgNnSc1d1
<g/>
)	)	kIx)
1926	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
</s>
<s>
Vláda	vláda	k1gFnSc1
a	a	k8xC
ministerstva	ministerstvo	k1gNnSc2
</s>
<s>
Premiér	premiér	k1gMnSc1
Hideki	Hidek	k1gFnSc2
Tódžó	Tódžó	k1gMnSc1
a	a	k8xC
ministr	ministr	k1gMnSc1
Nobusuke	Nobusuk	k1gFnSc2
Kiši	Kiš	k1gFnSc2
(	(	kIx(
<g/>
pozdější	pozdní	k2eAgMnSc1d2
premiér	premiér	k1gMnSc1
Japonska	Japonsko	k1gNnSc2
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1943	#num#	k4
</s>
<s>
Výkonnou	výkonný	k2eAgFnSc4d1
moc	moc	k1gFnSc4
vykonával	vykonávat	k5eAaImAgMnS
císař	císař	k1gMnSc1
prostřednictvím	prostřednictvím	k7c2
takzvané	takzvaný	k2eAgFnSc2d1
Tajné	tajný	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
de	de	k?
facto	facta	k1gFnSc5
vládou	vláda	k1gFnSc7
a	a	k8xC
poradním	poradní	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
císaře	císař	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gMnSc1
členové	člen	k1gMnPc1
jednali	jednat	k5eAaImAgMnP
společně	společně	k6eAd1
s	s	k7c7
císařem	císař	k1gMnSc7
o	o	k7c6
všech	všecek	k3xTgFnPc6
důležitých	důležitý	k2eAgFnPc6d1
státních	státní	k2eAgFnPc6d1
otázkách	otázka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tajnou	tajný	k2eAgFnSc4d1
radu	rada	k1gFnSc4
tvořili	tvořit	k5eAaImAgMnP
premiér	premiér	k1gMnSc1
a	a	k8xC
ministři	ministr	k1gMnPc1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
každý	každý	k3xTgMnSc1
směl	smět	k5eAaImAgMnS
sdělit	sdělit	k5eAaPmF
císaři	císař	k1gMnSc3
doporučení	doporučení	k1gNnPc4
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
oboru	obor	k1gInSc2
<g/>
,	,	kIx,
za	za	k7c4
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
byl	být	k5eAaImAgMnS
zodpovědný	zodpovědný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
z	z	k7c2
nichž	jenž	k3xRgMnPc2
každý	každý	k3xTgMnSc1
spravoval	spravovat	k5eAaImAgMnS
jedno	jeden	k4xCgNnSc4
z	z	k7c2
osmi	osm	k4xCc2
ministerstev	ministerstvo	k1gNnPc2
<g/>
:	:	kIx,
</s>
<s>
ministerstvo	ministerstvo	k1gNnSc1
občanských	občanský	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1873	#num#	k4
ministerstvo	ministerstvo	k1gNnSc4
domácích	domácí	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
ministerstvo	ministerstvo	k1gNnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
</s>
<s>
ministerstvo	ministerstvo	k1gNnSc1
financí	finance	k1gFnPc2
</s>
<s>
ministerstvo	ministerstvo	k1gNnSc1
armády	armáda	k1gFnSc2
</s>
<s>
ministerstvo	ministerstvo	k1gNnSc1
námořnictva	námořnictvo	k1gNnSc2
</s>
<s>
ministerstvo	ministerstvo	k1gNnSc1
císařského	císařský	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
</s>
<s>
ministerstvo	ministerstvo	k1gNnSc1
spravedlnosti	spravedlnost	k1gFnSc2
</s>
<s>
ministerstvo	ministerstvo	k1gNnSc1
veřejných	veřejný	k2eAgFnPc2d1
prací	práce	k1gFnPc2
</s>
<s>
ministerstvo	ministerstvo	k1gNnSc1
školství	školství	k1gNnSc2
</s>
<s>
(	(	kIx(
<g/>
Do	do	k7c2
roku	rok	k1gInSc2
1872	#num#	k4
tvořily	tvořit	k5eAaImAgFnP
ministerstvo	ministerstvo	k1gNnSc4
armády	armáda	k1gFnSc2
a	a	k8xC
ministerstvo	ministerstvo	k1gNnSc4
námořnictva	námořnictvo	k1gNnSc2
jednotné	jednotný	k2eAgNnSc4d1
Vojenské	vojenský	k2eAgNnSc4d1
ministerstvo	ministerstvo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
Parlament	parlament	k1gInSc1
</s>
<s>
Zákonodárnou	zákonodárný	k2eAgFnSc4d1
moc	moc	k1gFnSc4
do	do	k7c2
roku	rok	k1gInSc2
1890	#num#	k4
vykonával	vykonávat	k5eAaImAgMnS
císař	císař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
přijetím	přijetí	k1gNnSc7
Mucuhitovy	Mucuhitův	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
první	první	k4xOgInSc1
parlament	parlament	k1gInSc1
<g/>
,	,	kIx,
zvaný	zvaný	k2eAgInSc1d1
Říšský	říšský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
(	(	kIx(
<g/>
帝	帝	k?
-	-	kIx~
Teikoku-gikai	Teikoku-gika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládal	skládat	k5eAaImAgMnS
se	se	k3xPyFc4
ze	z	k7c2
dvou	dva	k4xCgFnPc2
komor	komora	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horní	horní	k2eAgFnSc4d1
komoru	komora	k1gFnSc4
<g/>
,	,	kIx,
takzvanou	takzvaný	k2eAgFnSc4d1
Šlechtickou	šlechtický	k2eAgFnSc4d1
sněmovnu	sněmovna	k1gFnSc4
(	(	kIx(
<g/>
貴	貴	k?
-	-	kIx~
Kizoku-in	Kizoku-in	k1gMnSc1
<g/>
)	)	kIx)
tvořili	tvořit	k5eAaImAgMnP
členové	člen	k1gMnPc1
Císařské	císařský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
osoby	osoba	k1gFnPc1
s	s	k7c7
dědičným	dědičný	k2eAgInSc7d1
šlechtickým	šlechtický	k2eAgInSc7d1
titulem	titul	k1gInSc7
a	a	k8xC
další	další	k2eAgMnPc1d1
členové	člen	k1gMnPc1
jmenovaní	jmenovaný	k2eAgMnPc1d1
císařem	císař	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslaneckou	poslanecký	k2eAgFnSc4d1
sněmovnu	sněmovna	k1gFnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
též	též	k9
Sněmovnu	sněmovna	k1gFnSc4
reprezentantů	reprezentant	k1gMnPc2
(	(	kIx(
<g/>
衆	衆	k?
-	-	kIx~
Šúgi-in	Šúgi-in	k1gMnSc1
<g/>
)	)	kIx)
volili	volit	k5eAaImAgMnP
občané	občan	k1gMnPc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
volební	volební	k2eAgNnSc4d1
právo	právo	k1gNnSc4
se	se	k3xPyFc4
vztahovalo	vztahovat	k5eAaImAgNnS
pouze	pouze	k6eAd1
na	na	k7c4
muže	muž	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
dosáhli	dosáhnout	k5eAaPmAgMnP
věku	věk	k1gInSc2
25	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
zaplatili	zaplatit	k5eAaPmAgMnP
na	na	k7c6
daních	daň	k1gFnPc6
více	hodně	k6eAd2
než	než	k8xS
15	#num#	k4
jenů	jen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oprávnění	oprávněný	k2eAgMnPc1d1
voliči	volič	k1gMnPc1
tak	tak	k6eAd1
tvořili	tvořit	k5eAaImAgMnP
pouze	pouze	k6eAd1
mizivé	mizivý	k2eAgNnSc4d1
procento	procento	k1gNnSc4
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
REISCHAUER	REISCHAUER	kA
<g/>
,	,	kIx,
Edwin	Edwin	k2eAgMnSc1d1
O	O	kA
<g/>
;	;	kIx,
CRAIG	CRAIG	kA
<g/>
,	,	kIx,
Albert	Albert	k1gMnSc1
M.	M.	kA
Dějiny	dějiny	k1gFnPc1
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
doplněné	doplněný	k2eAgFnPc4d1
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Dějiny	dějiny	k1gFnPc4
států	stát	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
843	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
japonských	japonský	k2eAgMnPc2d1
císařů	císař	k1gMnPc2
</s>
<s>
Japonské	japonský	k2eAgFnSc2d1
kolonie	kolonie	k1gFnSc2
</s>
<s>
Japonský	japonský	k2eAgInSc1d1
fašismus	fašismus	k1gInSc1
</s>
<s>
Japonské	japonský	k2eAgInPc1d1
válečné	válečný	k2eAgInPc1d1
zločiny	zločin	k1gInPc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Japonska	Japonsko	k1gNnSc2
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1
císařská	císařský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
císařské	císařský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
císařství	císařství	k1gNnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Japonské	japonský	k2eAgFnSc2d1
císařství	císařství	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Infoweb	Infowba	k1gFnPc2
o	o	k7c6
Japonsku	Japonsko	k1gNnSc6
</s>
<s>
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Úřad	úřad	k1gInSc1
císařské	císařský	k2eAgFnSc2d1
domácnosti	domácnost	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Hirohito	Hirohit	k2eAgNnSc1d1
a	a	k8xC
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Japonsko	Japonsko	k1gNnSc1
</s>
