<p>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
I.	I.	kA	I.
zvaný	zvaný	k2eAgInSc1d1	zvaný
Ukrutný	ukrutný	k2eAgInSc1d1	ukrutný
(	(	kIx(	(
<g/>
asi	asi	k9	asi
915	[number]	k4	915
–	–	k?	–
967	[number]	k4	967
či	či	k8xC	či
972	[number]	k4	972
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
v	v	k7c6	v
letech	let	k1gInPc6	let
935	[number]	k4	935
<g/>
–	–	k?	–
<g/>
967	[number]	k4	967
<g/>
/	/	kIx~	/
<g/>
972	[number]	k4	972
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Vratislava	Vratislav	k1gMnSc2	Vratislav
I.	I.	kA	I.
a	a	k8xC	a
Drahomíry	Drahomíra	k1gFnSc2	Drahomíra
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
byla	být	k5eAaImAgFnS	být
matkou	matka	k1gFnSc7	matka
obou	dva	k4xCgMnPc2	dva
Vratislavových	Vratislavův	k2eAgMnPc2d1	Vratislavův
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
jistě	jistě	k6eAd1	jistě
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
matkou	matka	k1gFnSc7	matka
Boleslavovou	Boleslavův	k2eAgFnSc7d1	Boleslavova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
knížetem	kníže	k1gMnSc7	kníže
Václavem	Václav	k1gMnSc7	Václav
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
Boleslavem	Boleslav	k1gMnSc7	Boleslav
jistě	jistě	k9	jistě
probíhaly	probíhat	k5eAaImAgInP	probíhat
názorové	názorový	k2eAgInPc4d1	názorový
spory	spor	k1gInPc4	spor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgInPc1d1	různý
pohledy	pohled	k1gInPc1	pohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
týkaly	týkat	k5eAaImAgInP	týkat
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Častější	častý	k2eAgFnSc1d2	častější
interpretace	interpretace	k1gFnSc1	interpretace
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
důslednou	důsledný	k2eAgFnSc4d1	důsledná
Václavovu	Václavův	k2eAgFnSc4d1	Václavova
politiku	politika	k1gFnSc4	politika
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
christianizace	christianizace	k1gFnSc2	christianizace
a	a	k8xC	a
upevňování	upevňování	k1gNnSc3	upevňování
křesťanství	křesťanství	k1gNnSc2	křesťanství
v	v	k7c6	v
knížectví	knížectví	k1gNnSc6	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
skutečnosti	skutečnost	k1gFnPc1	skutečnost
tak	tak	k9	tak
mohly	moct	k5eAaImAgFnP	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
nesouhlasnou	souhlasný	k2eNgFnSc4d1	nesouhlasná
reakci	reakce	k1gFnSc4	reakce
u	u	k7c2	u
části	část	k1gFnSc2	část
českých	český	k2eAgMnPc2d1	český
předáků	předák	k1gMnPc2	předák
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
odklon	odklon	k1gInSc4	odklon
od	od	k7c2	od
dosud	dosud	k6eAd1	dosud
tradičního	tradiční	k2eAgNnSc2d1	tradiční
uspořádání	uspořádání	k1gNnSc2	uspořádání
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c4	na
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
pohanském	pohanský	k2eAgInSc6d1	pohanský
základě	základ	k1gInSc6	základ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
interpretace	interpretace	k1gFnSc1	interpretace
přihlíží	přihlížet	k5eAaImIp3nS	přihlížet
k	k	k7c3	k
vnitřnímu	vnitřní	k2eAgNnSc3d1	vnitřní
uspořádání	uspořádání	k1gNnSc3	uspořádání
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
též	též	k9	též
k	k	k7c3	k
Václavovu	Václavův	k2eAgInSc3d1	Václavův
poměru	poměr	k1gInSc3	poměr
k	k	k7c3	k
Sasku	Sasko	k1gNnSc3	Sasko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dva	dva	k4xCgInPc1	dva
možné	možný	k2eAgInPc1d1	možný
výklady	výklad	k1gInPc1	výklad
==	==	k?	==
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
legendy	legenda	k1gFnPc1	legenda
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
ne	ne	k9	ne
se	s	k7c7	s
zcela	zcela	k6eAd1	zcela
jasným	jasný	k2eAgInSc7d1	jasný
motivem	motiv	k1gInSc7	motiv
<g/>
)	)	kIx)	)
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
935	[number]	k4	935
(	(	kIx(	(
<g/>
spíše	spíše	k9	spíše
než	než	k8xS	než
929	[number]	k4	929
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
k	k	k7c3	k
Václavově	Václavův	k2eAgFnSc3d1	Václavova
úkladné	úkladný	k2eAgFnSc3d1	úkladná
vraždě	vražda	k1gFnSc3	vražda
<g/>
,	,	kIx,	,
zosnované	zosnovaný	k2eAgNnSc1d1	zosnované
Boleslavem	Boleslav	k1gMnSc7	Boleslav
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
ponoukli	ponouknout	k5eAaPmAgMnP	ponouknout
"	"	kIx"	"
<g/>
čeští	český	k2eAgMnPc1d1	český
mužové	muž	k1gMnPc1	muž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
byli	být	k5eAaImAgMnP	být
zavražděni	zavraždit	k5eAaPmNgMnP	zavraždit
i	i	k9	i
členové	člen	k1gMnPc1	člen
Václavovy	Václavův	k2eAgFnSc2d1	Václavova
družiny	družina	k1gFnSc2	družina
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
přikazovala	přikazovat	k5eAaImAgFnS	přikazovat
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
zvyklost	zvyklost	k1gFnSc1	zvyklost
krevní	krevní	k2eAgFnSc2d1	krevní
msty	msta	k1gFnSc2	msta
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vyvozují	vyvozovat	k5eAaImIp3nP	vyvozovat
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgInPc1d1	jiný
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
legendisté	legendista	k1gMnPc1	legendista
příběh	příběh	k1gInSc4	příběh
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohlo	moct	k5eAaImAgNnS	moct
jít	jít	k5eAaImF	jít
spíš	spíš	k9	spíš
o	o	k7c4	o
nešťastnou	šťastný	k2eNgFnSc4d1	nešťastná
náhodu	náhoda	k1gFnSc4	náhoda
a	a	k8xC	a
osudné	osudný	k2eAgNnSc1d1	osudné
nedorozumění	nedorozumění	k1gNnSc1	nedorozumění
s	s	k7c7	s
tragickým	tragický	k2eAgInSc7d1	tragický
koncem	konec	k1gInSc7	konec
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
o	o	k7c4	o
vyvrcholení	vyvrcholení	k1gNnSc4	vyvrcholení
mocenského	mocenský	k2eAgInSc2d1	mocenský
boje	boj	k1gInSc2	boj
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
tábory	tábor	k1gInPc7	tábor
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
možných	možný	k2eAgInPc2d1	možný
nových	nový	k2eAgInPc2d1	nový
výkladů	výklad	k1gInPc2	výklad
události	událost	k1gFnSc2	událost
může	moct	k5eAaImIp3nS	moct
znít	znít	k5eAaImF	znít
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Onoho	onen	k3xDgNnSc2	onen
rána	ráno	k1gNnSc2	ráno
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
potkal	potkat	k5eAaPmAgMnS	potkat
Václav	Václav	k1gMnSc1	Václav
svého	své	k1gNnSc2	své
bratra	bratr	k1gMnSc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zastává	zastávat	k5eAaImIp3nS	zastávat
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
Dušan	Dušan	k1gMnSc1	Dušan
Třeštík	Třeštík	k1gMnSc1	Třeštík
<g/>
,	,	kIx,	,
rozlítila	rozlítit	k5eAaPmAgFnS	rozlítit
mladšího	mladý	k2eAgMnSc4d2	mladší
Boleslava	Boleslav	k1gMnSc4	Boleslav
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
neustálé	neustálý	k2eAgFnSc2d1	neustálá
spory	spora	k1gFnSc2	spora
<g/>
,	,	kIx,	,
povýšenost	povýšenost	k1gFnSc1	povýšenost
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
ho	on	k3xPp3gInSc4	on
kníže	kníže	k1gMnSc1	kníže
oslovil	oslovit	k5eAaPmAgMnS	oslovit
<g/>
.	.	kIx.	.
</s>
<s>
Tasil	tasit	k5eAaPmAgInS	tasit
meč	meč	k1gInSc1	meč
a	a	k8xC	a
vrhl	vrhnout	k5eAaPmAgMnS	vrhnout
se	se	k3xPyFc4	se
na	na	k7c4	na
Václava	Václav	k1gMnSc4	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Kníže	kníže	k1gMnSc1	kníže
ho	on	k3xPp3gNnSc4	on
však	však	k9	však
odzbrojil	odzbrojit	k5eAaPmAgMnS	odzbrojit
a	a	k8xC	a
povalil	povalit	k5eAaPmAgMnS	povalit
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Boleslavovo	Boleslavův	k2eAgNnSc4d1	Boleslavovo
volání	volání	k1gNnSc4	volání
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
přiběhli	přiběhnout	k5eAaPmAgMnP	přiběhnout
jeho	jeho	k3xOp3gMnPc1	jeho
družiníci	družiník	k1gMnPc1	družiník
<g/>
.	.	kIx.	.
</s>
<s>
Viděli	vidět	k5eAaImAgMnP	vidět
Václava	Václav	k1gMnSc4	Václav
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
s	s	k7c7	s
mečem	meč	k1gInSc7	meč
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
stojí	stát	k5eAaImIp3nS	stát
nad	nad	k7c7	nad
ležícím	ležící	k2eAgMnSc7d1	ležící
bratrem	bratr	k1gMnSc7	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Vrhli	vrhnout	k5eAaImAgMnP	vrhnout
se	se	k3xPyFc4	se
svému	svůj	k3xOyFgMnSc3	svůj
pánovi	pán	k1gMnSc3	pán
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
knížete	kníže	k1gMnSc4	kníže
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nestačil	stačit	k5eNaBmAgInS	stačit
prchnout	prchnout	k5eAaPmF	prchnout
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
ubili	ubít	k5eAaPmAgMnP	ubít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
obvykle	obvykle	k6eAd1	obvykle
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
otázky	otázka	k1gFnPc4	otázka
po	po	k7c6	po
smyslu	smysl	k1gInSc6	smysl
Boleslavovy	Boleslavův	k2eAgFnSc2d1	Boleslavova
cesty	cesta	k1gFnSc2	cesta
za	za	k7c7	za
bratrem	bratr	k1gMnSc7	bratr
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zabít	zabít	k5eAaPmF	zabít
ho	on	k3xPp3gMnSc4	on
vlastníma	vlastní	k2eAgFnPc7d1	vlastní
rukama	ruka	k1gFnPc7	ruka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
připomínají	připomínat	k5eAaImIp3nP	připomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnPc1	jeho
bojovníci	bojovník	k1gMnPc1	bojovník
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
skrývali	skrývat	k5eAaImAgMnP	skrývat
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
interpretoval	interpretovat	k5eAaBmAgMnS	interpretovat
Václavovu	Václavův	k2eAgFnSc4d1	Václavova
smrt	smrt	k1gFnSc4	smrt
i	i	k8xC	i
historik	historik	k1gMnSc1	historik
František	František	k1gMnSc1	František
Dvorník	dvorník	k1gMnSc1	dvorník
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
ale	ale	k9	ale
nechodilo	chodit	k5eNaImAgNnS	chodit
s	s	k7c7	s
mečem	meč	k1gInSc7	meč
<g/>
,	,	kIx,	,
Boleslav	Boleslav	k1gMnSc1	Boleslav
s	s	k7c7	s
ukrytými	ukrytý	k2eAgFnPc7d1	ukrytá
bojovníky	bojovník	k1gMnPc4	bojovník
čekali	čekat	k5eAaImAgMnP	čekat
na	na	k7c4	na
neozbrojeného	ozbrojený	k2eNgMnSc4d1	neozbrojený
Václava	Václav	k1gMnSc4	Václav
s	s	k7c7	s
jasným	jasný	k2eAgInSc7d1	jasný
úmyslem	úmysl	k1gInSc7	úmysl
zavraždit	zavraždit	k5eAaPmF	zavraždit
jej	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
představovala	představovat	k5eAaImAgFnS	představovat
Václavova	Václavův	k2eAgFnSc1d1	Václavova
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
byla	být	k5eAaImAgFnS	být
výsledkem	výsledek	k1gInSc7	výsledek
náhody	náhoda	k1gFnSc2	náhoda
či	či	k8xC	či
zlého	zlý	k2eAgInSc2d1	zlý
úmyslu	úmysl	k1gInSc2	úmysl
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Boleslava	Boleslav	k1gMnSc4	Boleslav
I.	I.	kA	I.
možnost	možnost	k1gFnSc4	možnost
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
vlastní	vlastní	k2eAgInPc4d1	vlastní
záměry	záměr	k1gInPc4	záměr
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
však	však	k9	však
byly	být	k5eAaImAgFnP	být
dříve	dříve	k6eAd2	dříve
mylně	mylně	k6eAd1	mylně
vykládány	vykládán	k2eAgInPc1d1	vykládán
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
slovanské	slovanský	k2eAgFnPc4d1	Slovanská
<g/>
"	"	kIx"	"
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
"	"	kIx"	"
<g/>
národní	národní	k2eAgInSc4d1	národní
<g/>
"	"	kIx"	"
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
údajnou	údajný	k2eAgFnSc7d1	údajná
Václavovou	Václavův	k2eAgFnSc7d1	Václavova
podřízeností	podřízenost	k1gFnSc7	podřízenost
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
938	[number]	k4	938
nechal	nechat	k5eAaPmAgMnS	nechat
Boleslav	Boleslav	k1gMnSc1	Boleslav
I.	I.	kA	I.
převézt	převézt	k5eAaPmF	převézt
a	a	k8xC	a
pohřbít	pohřbít	k5eAaPmF	pohřbít
ostatky	ostatek	k1gInPc4	ostatek
knížete	kníže	k1gMnSc2	kníže
Václava	Václav	k1gMnSc2	Václav
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
apsid	apsida	k1gFnPc2	apsida
rotundy	rotunda	k1gFnSc2	rotunda
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
(	(	kIx(	(
<g/>
postavené	postavený	k2eAgFnPc1d1	postavená
samotným	samotný	k2eAgMnPc3d1	samotný
sv.	sv.	kA	sv.
Václavem	Václav	k1gMnSc7	Václav
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
apsida	apsida	k1gFnSc1	apsida
směřovala	směřovat	k5eAaImAgFnS	směřovat
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
světovou	světový	k2eAgFnSc4d1	světová
stranu	strana	k1gFnSc4	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
místě	místo	k1gNnSc6	místo
dnes	dnes	k6eAd1	dnes
stojí	stát	k5eAaImIp3nS	stát
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Saská	saský	k2eAgFnSc1d1	saská
válka	válka	k1gFnSc1	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
I.	I.	kA	I.
je	být	k5eAaImIp3nS	být
českými	český	k2eAgMnPc7d1	český
historiky	historik	k1gMnPc7	historik
i	i	k8xC	i
přes	přes	k7c4	přes
bratrovraždu	bratrovražda	k1gFnSc4	bratrovražda
respektován	respektován	k2eAgMnSc1d1	respektován
jako	jako	k8xC	jako
energický	energický	k2eAgMnSc1d1	energický
vládce	vládce	k1gMnSc1	vládce
a	a	k8xC	a
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgMnPc2d3	nejúspěšnější
českých	český	k2eAgMnPc2d1	český
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
české	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
povznesly	povznést	k5eAaPmAgFnP	povznést
politicky	politicky	k6eAd1	politicky
i	i	k8xC	i
ekonomicky	ekonomicky	k6eAd1	ekonomicky
a	a	k8xC	a
Boleslav	Boleslav	k1gMnSc1	Boleslav
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
skutečného	skutečný	k2eAgMnSc4d1	skutečný
zakladatele	zakladatel	k1gMnSc4	zakladatel
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Záhy	záhy	k6eAd1	záhy
po	po	k7c6	po
převzetí	převzetí	k1gNnSc6	převzetí
moci	moc	k1gFnSc2	moc
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
bratru	bratr	k1gMnSc6	bratr
knížeti	kníže	k1gMnSc6	kníže
Václavovi	Václav	k1gMnSc6	Václav
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
Boleslav	Boleslav	k1gMnSc1	Boleslav
I.	I.	kA	I.
odvádět	odvádět	k5eAaImF	odvádět
tribut	tribut	k1gInSc4	tribut
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
byla	být	k5eAaImAgFnS	být
čtrnáctiletá	čtrnáctiletý	k2eAgFnSc1d1	čtrnáctiletá
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
936	[number]	k4	936
<g/>
–	–	k?	–
<g/>
950	[number]	k4	950
<g/>
)	)	kIx)	)
s	s	k7c7	s
králem	král	k1gMnSc7	král
Otou	Ota	k1gMnSc7	Ota
I.	I.	kA	I.
<g/>
,	,	kIx,	,
nástupcem	nástupce	k1gMnSc7	nástupce
Jindřicha	Jindřich	k1gMnSc2	Jindřich
I.	I.	kA	I.
Ptáčníka	Ptáčník	k1gMnSc2	Ptáčník
<g/>
.	.	kIx.	.
</s>
<s>
Ota	Ota	k1gMnSc1	Ota
I.	I.	kA	I.
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
také	také	k9	také
potlačoval	potlačovat	k5eAaImAgMnS	potlačovat
povstání	povstání	k1gNnSc4	povstání
Polabských	polabský	k2eAgInPc2d1	polabský
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hned	hned	k6eAd1	hned
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
počátku	počátek	k1gInSc6	počátek
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
936	[number]	k4	936
<g/>
,	,	kIx,	,
Boleslav	Boleslav	k1gMnSc1	Boleslav
napadl	napadnout	k5eAaPmAgMnS	napadnout
vicina	vicin	k1gMnSc4	vicin
subregula	subregul	k1gMnSc4	subregul
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
vedlejšího	vedlejší	k2eAgMnSc2d1	vedlejší
podkrálíčka	podkrálíček	k1gMnSc2	podkrálíček
<g/>
"	"	kIx"	"
–	–	k?	–
termín	termín	k1gInSc1	termín
užívaný	užívaný	k2eAgInSc1d1	užívaný
Widukindem	Widukind	k1gInSc7	Widukind
z	z	k7c2	z
Corvey	Corvea	k1gFnSc2	Corvea
<g/>
)	)	kIx)	)
ovládajícího	ovládající	k2eAgNnSc2d1	ovládající
území	území	k1gNnSc2	území
na	na	k7c4	na
SZ	SZ	kA	SZ
Čech	Čech	k1gMnSc1	Čech
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
zároveň	zároveň	k6eAd1	zároveň
spojencem	spojenec	k1gMnSc7	spojenec
Sasů	Sas	k1gMnPc2	Sas
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
Ota	Ota	k1gMnSc1	Ota
I.	I.	kA	I.
vyslal	vyslat	k5eAaPmAgMnS	vyslat
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
dvě	dva	k4xCgFnPc4	dva
armády	armáda	k1gFnPc4	armáda
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
Durynska	Durynsko	k1gNnSc2	Durynsko
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
z	z	k7c2	z
Merseburku	Merseburk	k1gInSc2	Merseburk
<g/>
,	,	kIx,	,
sebrané	sebraný	k2eAgNnSc1d1	sebrané
prý	prý	k9	prý
z	z	k7c2	z
nejhorších	zlý	k2eAgMnPc2d3	nejhorší
hrdlořezů	hrdlořez	k1gMnPc2	hrdlořez
a	a	k8xC	a
lupičů	lupič	k1gMnPc2	lupič
<g/>
.	.	kIx.	.
</s>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
se	se	k3xPyFc4	se
o	o	k7c6	o
dvou	dva	k4xCgFnPc6	dva
armádách	armáda	k1gFnPc6	armáda
postupujících	postupující	k2eAgFnPc6d1	postupující
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
směrů	směr	k1gInPc2	směr
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
a	a	k8xC	a
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
tedy	tedy	k9	tedy
i	i	k9	i
svoje	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
porazit	porazit	k5eAaPmF	porazit
Durynky	Durynk	k1gMnPc4	Durynk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
následně	následně	k6eAd1	následně
utekli	utéct	k5eAaPmAgMnP	utéct
<g/>
;	;	kIx,	;
druhá	druhý	k4xOgFnSc1	druhý
Boleslavova	Boleslavův	k2eAgFnSc1d1	Boleslavova
armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
sice	sice	k8xC	sice
poražena	porazit	k5eAaPmNgFnS	porazit
Sasy	Sas	k1gMnPc7	Sas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gInPc1	její
zbytky	zbytek	k1gInPc1	zbytek
se	se	k3xPyFc4	se
spojily	spojit	k5eAaPmAgInP	spojit
s	s	k7c7	s
vítězivší	vítězivší	k2eAgFnSc7d1	vítězivší
částí	část	k1gFnSc7	část
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
spojené	spojený	k2eAgFnPc1d1	spojená
Boleslavovy	Boleslavův	k2eAgFnPc1d1	Boleslavova
síly	síla	k1gFnPc1	síla
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
napadly	napadnout	k5eAaPmAgInP	napadnout
nic	nic	k6eAd1	nic
nečekající	čekající	k2eNgMnPc4d1	nečekající
Sasy	Sas	k1gMnPc4	Sas
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
Boleslav	Boleslav	k1gMnSc1	Boleslav
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
,	,	kIx,	,
Sasové	Sas	k1gMnPc1	Sas
bitvu	bitva	k1gFnSc4	bitva
prohráli	prohrát	k5eAaPmAgMnP	prohrát
a	a	k8xC	a
Boleslav	Boleslav	k1gMnSc1	Boleslav
potom	potom	k6eAd1	potom
okamžitě	okamžitě	k6eAd1	okamžitě
oblehl	oblehnout	k5eAaPmAgMnS	oblehnout
<g/>
,	,	kIx,	,
dobyl	dobýt	k5eAaPmAgMnS	dobýt
a	a	k8xC	a
vypálil	vypálit	k5eAaPmAgMnS	vypálit
i	i	k9	i
hrad	hrad	k1gInSc4	hrad
zmíněného	zmíněný	k2eAgMnSc2d1	zmíněný
sousedního	sousední	k2eAgMnSc2d1	sousední
vládce	vládce	k1gMnSc2	vládce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
950	[number]	k4	950
<g/>
.	.	kIx.	.
</s>
<s>
Ota	Ota	k1gMnSc1	Ota
oblehl	oblehnout	k5eAaPmAgMnS	oblehnout
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
Nový	nový	k2eAgInSc4d1	nový
hrad	hrad	k1gInSc4	hrad
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nově	nově	k6eAd1	nově
postavený	postavený	k2eAgInSc4d1	postavený
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zatím	zatím	k6eAd1	zatím
nebyl	být	k5eNaImAgMnS	být
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
spravoval	spravovat	k5eAaImAgMnS	spravovat
Boleslavův	Boleslavův	k2eAgMnSc1d1	Boleslavův
syn	syn	k1gMnSc1	syn
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
ovšem	ovšem	k9	ovšem
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Boleslav	Boleslav	k1gMnSc1	Boleslav
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
nebojovat	bojovat	k5eNaImF	bojovat
a	a	k8xC	a
jednat	jednat	k5eAaImF	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
osobní	osobní	k2eAgNnPc1d1	osobní
jednání	jednání	k1gNnPc1	jednání
nakonec	nakonec	k6eAd1	nakonec
skončila	skončit	k5eAaPmAgNnP	skončit
smírem	smír	k1gInSc7	smír
obou	dva	k4xCgInPc2	dva
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
objevují	objevovat	k5eAaImIp3nP	objevovat
hypotézy	hypotéza	k1gFnPc1	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
Boleslav	Boleslav	k1gMnSc1	Boleslav
Otovi	Otův	k2eAgMnPc1d1	Otův
složil	složit	k5eAaPmAgMnS	složit
lenní	lenní	k2eAgFnSc4d1	lenní
přísahu	přísaha	k1gFnSc4	přísaha
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
českých	český	k2eAgMnPc2d1	český
historiků	historik	k1gMnPc2	historik
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
myšlenkou	myšlenka	k1gFnSc7	myšlenka
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Římskoněmečtí	římskoněmecký	k2eAgMnPc1d1	římskoněmecký
panovníci	panovník	k1gMnPc1	panovník
by	by	kYmCp3nP	by
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
udělovali	udělovat	k5eAaImAgMnP	udělovat
českým	český	k2eAgMnPc3d1	český
knížatům	kníže	k1gMnPc3wR	kníže
jejich	jejich	k3xOp3gNnSc4	jejich
území	území	k1gNnSc4	území
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
během	během	k7c2	během
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nikdy	nikdy	k6eAd1	nikdy
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
jazykem	jazyk	k1gInSc7	jazyk
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
panovníci	panovník	k1gMnPc1	panovník
spolu	spolu	k6eAd1	spolu
dorozumívali	dorozumívat	k5eAaImAgMnP	dorozumívat
<g/>
,	,	kIx,	,
či	či	k8xC	či
zda	zda	k8xS	zda
užívali	užívat	k5eAaImAgMnP	užívat
tlumočníků	tlumočník	k1gMnPc2	tlumočník
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
o	o	k7c6	o
formě	forma	k1gFnSc6	forma
jejich	jejich	k3xOp3gFnSc2	jejich
osobní	osobní	k2eAgFnSc2d1	osobní
komunikace	komunikace	k1gFnSc2	komunikace
prameny	pramen	k1gInPc1	pramen
(	(	kIx(	(
<g/>
Widukindova	Widukindův	k2eAgFnSc1d1	Widukindova
Kronika	kronika	k1gFnSc1	kronika
Saská	saský	k2eAgFnSc1d1	saská
<g/>
)	)	kIx)	)
mlčí	mlčet	k5eAaImIp3nS	mlčet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Otově	Otův	k2eAgFnSc6d1	Otova
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
armáda	armáda	k1gFnSc1	armáda
Boleslava	Boleslav	k1gMnSc2	Boleslav
I.	I.	kA	I.
roku	rok	k1gInSc2	rok
955	[number]	k4	955
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
bitvy	bitva	k1gFnPc4	bitva
na	na	k7c6	na
Lechu	lech	k1gInSc6	lech
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
poraženy	porazit	k5eAaPmNgInP	porazit
kočovné	kočovný	k2eAgInPc1d1	kočovný
maďarské	maďarský	k2eAgInPc1d1	maďarský
kmeny	kmen	k1gInPc1	kmen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
usadily	usadit	k5eAaPmAgInP	usadit
v	v	k7c6	v
podunajské	podunajský	k2eAgFnSc6d1	Podunajská
nížině	nížina	k1gFnSc6	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
ale	ale	k9	ale
neúčastnil	účastnit	k5eNaImAgInS	účastnit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
musel	muset	k5eAaImAgMnS	muset
odrazit	odrazit	k5eAaPmF	odrazit
vedlejší	vedlejší	k2eAgFnSc3d1	vedlejší
invazi	invaze	k1gFnSc3	invaze
Maďarů	Maďar	k1gMnPc2	Maďar
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
sbor	sbor	k1gInSc1	sbor
vyslaný	vyslaný	k2eAgInSc1d1	vyslaný
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
čítal	čítat	k5eAaImAgInS	čítat
prý	prý	k9	prý
asi	asi	k9	asi
1000	[number]	k4	1000
(	(	kIx(	(
<g/>
Dušan	Dušan	k1gMnSc1	Dušan
Třeštík	Třeštík	k1gMnSc1	Třeštík
tento	tento	k3xDgInSc4	tento
počet	počet	k1gInSc4	počet
zásadně	zásadně	k6eAd1	zásadně
zpochybňuje	zpochybňovat	k5eAaImIp3nS	zpochybňovat
<g/>
)	)	kIx)	)
vyzbrojených	vyzbrojený	k2eAgMnPc2d1	vyzbrojený
a	a	k8xC	a
vycvičených	vycvičený	k2eAgMnPc2d1	vycvičený
družiníků	družiník	k1gMnPc2	družiník
a	a	k8xC	a
přesto	přesto	k8xC	přesto
Boleslavovi	Boleslav	k1gMnSc3	Boleslav
zůstala	zůstat	k5eAaPmAgFnS	zůstat
armáda	armáda	k1gFnSc1	armáda
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
boje	boj	k1gInPc4	boj
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
jeho	jeho	k3xOp3gFnSc1	jeho
celková	celkový	k2eAgFnSc1d1	celková
armáda	armáda	k1gFnSc1	armáda
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
ještě	ještě	k9	ještě
podstatně	podstatně	k6eAd1	podstatně
silnější	silný	k2eAgMnSc1d2	silnější
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
obrovský	obrovský	k2eAgInSc4d1	obrovský
nárůst	nárůst	k1gInSc4	nárůst
oproti	oproti	k7c3	oproti
družinám	družina	k1gFnPc3	družina
čítajícím	čítající	k2eAgFnPc3d1	čítající
desítky	desítka	k1gFnPc1	desítka
jedinců	jedinec	k1gMnPc2	jedinec
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Václavově	Václavův	k2eAgFnSc6d1	Václavova
a	a	k8xC	a
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Porážka	porážka	k1gFnSc1	porážka
Maďarů	Maďar	k1gMnPc2	Maďar
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Lechu	lech	k1gInSc6	lech
znamenala	znamenat	k5eAaImAgFnS	znamenat
konec	konec	k1gInSc4	konec
dalších	další	k2eAgInPc2d1	další
maďarských	maďarský	k2eAgInPc2d1	maďarský
nájezdů	nájezd	k1gInPc2	nájezd
na	na	k7c4	na
západní	západní	k2eAgMnPc4d1	západní
sousedy	soused	k1gMnPc4	soused
a	a	k8xC	a
Maďaři	Maďar	k1gMnPc1	Maďar
postupně	postupně	k6eAd1	postupně
přešli	přejít	k5eAaPmAgMnP	přejít
k	k	k7c3	k
usedlému	usedlý	k2eAgInSc3d1	usedlý
způsobu	způsob	k1gInSc3	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
hradské	hradský	k2eAgFnSc2d1	Hradská
soustavy	soustava	k1gFnSc2	soustava
==	==	k?	==
</s>
</p>
<p>
<s>
Postavení	postavení	k1gNnSc1	postavení
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
Čechách	Čechy	k1gFnPc6	Čechy
se	se	k3xPyFc4	se
za	za	k7c2	za
Boleslavova	Boleslavův	k2eAgNnSc2d1	Boleslavovo
třicetiletého	třicetiletý	k2eAgNnSc2d1	třicetileté
panování	panování	k1gNnSc2	panování
natolik	natolik	k6eAd1	natolik
upevnilo	upevnit	k5eAaPmAgNnS	upevnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
dovolit	dovolit	k5eAaPmF	dovolit
vybírat	vybírat	k5eAaImF	vybírat
od	od	k7c2	od
zdejších	zdejší	k2eAgMnPc2d1	zdejší
obyvatel	obyvatel	k1gMnPc2	obyvatel
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
daně	daň	k1gFnSc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Daň	daň	k1gFnSc1	daň
představovala	představovat	k5eAaImAgFnS	představovat
poplatek	poplatek	k1gInSc4	poplatek
za	za	k7c4	za
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
kníže	kníže	k1gMnSc1	kníže
svému	svůj	k3xOyFgNnSc3	svůj
lidu	lido	k1gNnSc3	lido
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
získaných	získaný	k2eAgInPc2d1	získaný
prostředků	prostředek	k1gInPc2	prostředek
zřídil	zřídit	k5eAaPmAgInS	zřídit
a	a	k8xC	a
vydržoval	vydržovat	k5eAaImAgInS	vydržovat
několikatisícovou	několikatisícový	k2eAgFnSc4d1	několikatisícová
dobře	dobře	k6eAd1	dobře
vyzbrojenou	vyzbrojený	k2eAgFnSc4d1	vyzbrojená
družinu	družina	k1gFnSc4	družina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vojenská	vojenský	k2eAgFnSc1d1	vojenská
moc	moc	k1gFnSc1	moc
mu	on	k3xPp3gMnSc3	on
umožnila	umožnit	k5eAaPmAgFnS	umožnit
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zbavit	zbavit	k5eAaPmF	zbavit
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dosud	dosud	k6eAd1	dosud
vládly	vládnout	k5eAaImAgInP	vládnout
na	na	k7c6	na
hradištích	hradiště	k1gNnPc6	hradiště
české	český	k2eAgFnSc2d1	Česká
kotliny	kotlina	k1gFnSc2	kotlina
a	a	k8xC	a
jen	jen	k9	jen
volně	volně	k6eAd1	volně
se	se	k3xPyFc4	se
podřizovaly	podřizovat	k5eAaImAgFnP	podřizovat
pražským	pražský	k2eAgMnSc7d1	pražský
Přemyslovcům	Přemyslovec	k1gMnPc3	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
"	"	kIx"	"
<g/>
subreguli	subregout	k5eAaPmAgMnP	subregout
<g/>
"	"	kIx"	"
byli	být	k5eAaImAgMnP	být
zničeni	zničen	k2eAgMnPc1d1	zničen
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
s	s	k7c7	s
celými	celý	k2eAgInPc7d1	celý
rody	rod	k1gInPc7	rod
<g/>
)	)	kIx)	)
a	a	k8xC	a
nahrazeni	nahradit	k5eAaPmNgMnP	nahradit
kastelány	kastelán	k1gMnPc7	kastelán
plně	plně	k6eAd1	plně
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
úplně	úplně	k6eAd1	úplně
jiná	jiný	k2eAgFnSc1d1	jiná
koncepce	koncepce	k1gFnSc1	koncepce
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
uspořádání	uspořádání	k1gNnSc2	uspořádání
státu	stát	k1gInSc2	stát
než	než	k8xS	než
za	za	k7c4	za
Boleslavova	Boleslavův	k2eAgMnSc4d1	Boleslavův
předchůdce	předchůdce	k1gMnSc4	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
kníže	kníže	k1gMnSc1	kníže
Václav	Václav	k1gMnSc1	Václav
vojensky	vojensky	k6eAd1	vojensky
porazil	porazit	k5eAaPmAgMnS	porazit
některého	některý	k3yIgMnSc4	některý
z	z	k7c2	z
dalších	další	k2eAgNnPc2d1	další
českých	český	k2eAgNnPc2d1	české
knížat	kníže	k1gNnPc2	kníže
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
území	území	k1gNnSc4	území
neobsadil	obsadit	k5eNaPmAgMnS	obsadit
a	a	k8xC	a
spokojil	spokojit	k5eAaPmAgMnS	spokojit
se	se	k3xPyFc4	se
s	s	k7c7	s
formálním	formální	k2eAgInSc7d1	formální
slibem	slib	k1gInSc7	slib
závislosti	závislost	k1gFnSc2	závislost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
často	často	k6eAd1	často
nebyl	být	k5eNaImAgInS	být
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
knížecí	knížecí	k2eAgFnSc2d1	knížecí
družiny	družina	k1gFnSc2	družina
realizován	realizovat	k5eAaBmNgInS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
pevně	pevně	k6eAd1	pevně
ovládnout	ovládnout	k5eAaPmF	ovládnout
celé	celý	k2eAgFnPc4d1	celá
Čechy	Čechy	k1gFnPc4	Čechy
<g/>
,	,	kIx,	,
zbavit	zbavit	k5eAaPmF	zbavit
ostatní	ostatní	k2eAgMnPc4d1	ostatní
knížata	kníže	k1gMnPc4wR	kníže
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
dosadit	dosadit	k5eAaPmF	dosadit
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
území	území	k1gNnSc6	území
knížecí	knížecí	k2eAgMnSc1d1	knížecí
správce	správce	k1gMnSc1	správce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vybrané	vybraný	k2eAgFnPc4d1	vybraná
daně	daň	k1gFnPc4	daň
by	by	kYmCp3nS	by
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vybudovat	vybudovat	k5eAaPmF	vybudovat
vojsko	vojsko	k1gNnSc4	vojsko
a	a	k8xC	a
zbavit	zbavit	k5eAaPmF	zbavit
se	se	k3xPyFc4	se
poplatné	poplatný	k2eAgFnSc3d1	poplatná
závislosti	závislost	k1gFnSc3	závislost
na	na	k7c4	na
Sasku	Saska	k1gFnSc4	Saska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hradiště	Hradiště	k1gNnSc1	Hradiště
podřízených	podřízený	k2eAgNnPc2d1	podřízené
knížat	kníže	k1gNnPc2	kníže
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
a	a	k8xC	a
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
blízkosti	blízkost	k1gFnSc6	blízkost
vyrostla	vyrůst	k5eAaPmAgNnP	vyrůst
nová	nový	k2eAgNnPc1d1	nové
správní	správní	k2eAgNnPc1d1	správní
centra	centrum	k1gNnPc1	centrum
(	(	kIx(	(
<g/>
Žatec	Žatec	k1gInSc1	Žatec
<g/>
,	,	kIx,	,
Starý	starý	k2eAgInSc1d1	starý
Plzenec	Plzenec	k1gInSc1	Plzenec
<g/>
,	,	kIx,	,
Sedlec	Sedlec	k1gInSc1	Sedlec
<g/>
,	,	kIx,	,
Bílina	Bílina	k1gFnSc1	Bílina
<g/>
,	,	kIx,	,
Litoměřice	Litoměřice	k1gInPc1	Litoměřice
<g/>
,	,	kIx,	,
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
Kouřim-	Kouřim-	k1gMnSc1	Kouřim-
<g/>
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
Prácheň	Prácheň	k1gFnSc1	Prácheň
<g/>
,	,	kIx,	,
Doudleby	Doudleby	k1gInPc1	Doudleby
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
této	tento	k3xDgFnSc2	tento
hradské	hradský	k2eAgFnSc2d1	Hradská
správy	správa	k1gFnSc2	správa
Boleslav	Boleslav	k1gMnSc1	Boleslav
pevně	pevně	k6eAd1	pevně
ovládal	ovládat	k5eAaImAgMnS	ovládat
a	a	k8xC	a
řídil	řídit	k5eAaImAgMnS	řídit
celou	celý	k2eAgFnSc4d1	celá
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
postup	postup	k1gInSc4	postup
vůči	vůči	k7c3	vůči
zbývajícím	zbývající	k2eAgMnPc3d1	zbývající
nezávislým	závislý	k2eNgMnPc3d1	nezávislý
českým	český	k2eAgMnPc3d1	český
knížatům	kníže	k1gMnPc3wR	kníže
a	a	k8xC	a
snad	snad	k9	snad
i	i	k9	i
za	za	k7c4	za
bratrovraždu	bratrovražda	k1gFnSc4	bratrovražda
si	se	k3xPyFc3	se
Boleslav	Boleslav	k1gMnSc1	Boleslav
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
přízvisko	přízvisko	k1gNnSc4	přízvisko
Ukrutný	ukrutný	k2eAgMnSc1d1	ukrutný
<g/>
.	.	kIx.	.
</s>
<s>
Hradská	hradská	k1gFnSc1	hradská
soustava	soustava	k1gFnSc1	soustava
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
postupně	postupně	k6eAd1	postupně
během	během	k7c2	během
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
zániku	zánik	k1gInSc3	zánik
přispěla	přispět	k5eAaPmAgFnS	přispět
především	především	k9	především
snaha	snaha	k1gFnSc1	snaha
kastelánů	kastelán	k1gMnPc2	kastelán
a	a	k8xC	a
šlechty	šlechta	k1gFnSc2	šlechta
navázané	navázaný	k2eAgFnSc2d1	navázaná
na	na	k7c4	na
tato	tento	k3xDgNnPc4	tento
správní	správní	k2eAgNnPc4d1	správní
centra	centrum	k1gNnPc4	centrum
dědičně	dědičně	k6eAd1	dědičně
získat	získat	k5eAaPmF	získat
soukromé	soukromý	k2eAgNnSc4d1	soukromé
pozemkové	pozemkový	k2eAgNnSc4d1	pozemkové
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Určitá	určitý	k2eAgFnSc1d1	určitá
skupina	skupina	k1gFnSc1	skupina
přemyslovských	přemyslovský	k2eAgInPc2d1	přemyslovský
hradů	hrad	k1gInPc2	hrad
a	a	k8xC	a
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gFnPc2	on
vytvořených	vytvořený	k2eAgFnPc2d1	vytvořená
aglomerací	aglomerace	k1gFnPc2	aglomerace
si	se	k3xPyFc3	se
i	i	k9	i
ve	v	k7c6	v
vrcholném	vrcholný	k2eAgInSc6d1	vrcholný
středověku	středověk	k1gInSc6	středověk
uchovala	uchovat	k5eAaPmAgFnS	uchovat
centrální	centrální	k2eAgNnSc4d1	centrální
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
přerodila	přerodit	k5eAaPmAgFnS	přerodit
se	se	k3xPyFc4	se
ve	v	k7c4	v
vrcholně	vrcholně	k6eAd1	vrcholně
středověká	středověký	k2eAgNnPc4d1	středověké
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pražské	pražský	k2eAgInPc1d1	pražský
denáry	denár	k1gInPc1	denár
a	a	k8xC	a
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
růst	růst	k1gInSc1	růst
státu	stát	k1gInSc2	stát
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c2	za
Boleslavovy	Boleslavův	k2eAgFnSc2d1	Boleslavova
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
začínají	začínat	k5eAaImIp3nP	začínat
razit	razit	k5eAaImF	razit
první	první	k4xOgFnPc4	první
české	český	k2eAgFnPc4d1	Česká
drobné	drobný	k2eAgFnPc4d1	drobná
mince	mince	k1gFnPc4	mince
<g/>
,	,	kIx,	,
pražské	pražský	k2eAgInPc4d1	pražský
denáry	denár	k1gInPc4	denár
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
straně	strana	k1gFnSc6	strana
s	s	k7c7	s
kaplicí	kaplicí	k?	kaplicí
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
Boleslavovo	Boleslavův	k2eAgNnSc1d1	Boleslavovo
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
nápis	nápis	k1gInSc1	nápis
BIAGOTACOIIIIA	BIAGOTACOIIIIA	kA	BIAGOTACOIIIIA
nebo	nebo	k8xC	nebo
BIAGOTACOVIIX	BIAGOTACOVIIX	kA	BIAGOTACOVIIX
(	(	kIx(	(
<g/>
Biagota	Biagota	k1gFnSc1	Biagota
manželka	manželka	k1gFnSc1	manželka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
manželkou	manželka	k1gFnSc7	manželka
knížete	kníže	k1gMnSc2	kníže
byla	být	k5eAaImAgFnS	být
právě	právě	k6eAd1	právě
jistá	jistý	k2eAgFnSc1d1	jistá
Biagota	Biagota	k1gFnSc1	Biagota
<g/>
.	.	kIx.	.
</s>
<s>
Uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
se	se	k3xPyFc4	se
i	i	k9	i
o	o	k7c4	o
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
denáry	denár	k1gInPc1	denár
byly	být	k5eAaImAgInP	být
raženy	razit	k5eAaImNgInP	razit
jako	jako	k8xS	jako
svatební	svatební	k2eAgInPc1d1	svatební
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
hypotézu	hypotéza	k1gFnSc4	hypotéza
neexistují	existovat	k5eNaImIp3nP	existovat
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
době	doba	k1gFnSc6	doba
doma	doma	k6eAd1	doma
ani	ani	k8xC	ani
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
žádné	žádný	k3yNgFnSc2	žádný
analogie	analogie	k1gFnSc2	analogie
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
kvalitou	kvalita	k1gFnSc7	kvalita
a	a	k8xC	a
hodnotou	hodnota	k1gFnSc7	hodnota
se	se	k3xPyFc4	se
Boleslavovy	Boleslavův	k2eAgInPc1d1	Boleslavův
denáry	denár	k1gInPc1	denár
rovnaly	rovnat	k5eAaImAgFnP	rovnat
ostatním	ostatní	k2eAgFnPc3d1	ostatní
mincím	mince	k1gFnPc3	mince
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
ekonomickém	ekonomický	k2eAgInSc6d1	ekonomický
vzrůstu	vzrůst	k1gInSc6	vzrůst
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k8xC	i
zápisy	zápis	k1gInPc1	zápis
židovského	židovský	k2eAgMnSc2d1	židovský
obchodníka	obchodník	k1gMnSc2	obchodník
a	a	k8xC	a
cestovatele	cestovatel	k1gMnSc2	cestovatel
Ibrahima	Ibrahim	k1gMnSc2	Ibrahim
ibn	ibn	k?	ibn
Jakuba	Jakub	k1gMnSc2	Jakub
z	z	k7c2	z
Iberského	iberský	k2eAgInSc2d1	iberský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
965	[number]	k4	965
-	-	kIx~	-
966	[number]	k4	966
navštívil	navštívit	k5eAaPmAgMnS	navštívit
knížectví	knížectví	k1gNnSc4	knížectví
(	(	kIx(	(
<g/>
Boleslava	Boleslav	k1gMnSc4	Boleslav
nazývá	nazývat	k5eAaImIp3nS	nazývat
Bújislav	Bújislav	k1gMnSc1	Bújislav
či	či	k8xC	či
Bújim	Bújim	k1gMnSc1	Bújim
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ibrahim	Ibrahim	k6eAd1	Ibrahim
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
Město	město	k1gNnSc1	město
Frága	Frága	k1gFnSc1	Frága
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zbudováno	zbudovat	k5eAaPmNgNnS	zbudovat
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
a	a	k8xC	a
vápna	vápno	k1gNnSc2	vápno
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
obchodem	obchod	k1gInSc7	obchod
město	město	k1gNnSc1	město
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
nejbohatší	bohatý	k2eAgInSc1d3	nejbohatší
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
severu	sever	k1gInSc2	sever
(	(	kIx(	(
<g/>
míněno	mínit	k5eAaImNgNnS	mínit
Evropa	Evropa	k1gFnSc1	Evropa
severně	severně	k6eAd1	severně
od	od	k7c2	od
Alp	Alpy	k1gFnPc2	Alpy
<g/>
)	)	kIx)	)
a	a	k8xC	a
potravinami	potravina	k1gFnPc7	potravina
nejbohatší	bohatý	k2eAgFnPc1d3	nejbohatší
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
budováním	budování	k1gNnSc7	budování
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
a	a	k8xC	a
vápna	vápno	k1gNnSc2	vápno
míní	mínit	k5eAaImIp3nS	mínit
Pražský	pražský	k2eAgInSc1d1	pražský
hrad	hrad	k1gInSc1	hrad
-	-	kIx~	-
několik	několik	k4yIc4	několik
málo	málo	k6eAd1	málo
církevních	církevní	k2eAgFnPc2d1	církevní
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
při	při	k7c6	při
jejichž	jejichž	k3xOyRp3gFnSc6	jejichž
výstavbě	výstavba	k1gFnSc6	výstavba
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
bílá	bílý	k2eAgFnSc1d1	bílá
opuka	opuka	k1gFnSc1	opuka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jinak	jinak	k6eAd1	jinak
stavělo	stavět	k5eAaImAgNnS	stavět
především	především	k9	především
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výboje	výboj	k1gInPc4	výboj
==	==	k?	==
</s>
</p>
<p>
<s>
Upevnění	upevnění	k1gNnSc1	upevnění
moci	moc	k1gFnSc2	moc
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
Boleslavovi	Boleslav	k1gMnSc3	Boleslav
I.	I.	kA	I.
zahájit	zahájit	k5eAaPmF	zahájit
výboje	výboj	k1gInPc4	výboj
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgInPc2	jenž
připojil	připojit	k5eAaPmAgInS	připojit
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
státu	stát	k1gInSc3	stát
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
,	,	kIx,	,
Krakovsko	Krakovsko	k1gNnSc4	Krakovsko
<g/>
,	,	kIx,	,
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
Červené	Červené	k2eAgInPc4d1	Červené
hrady	hrad	k1gInPc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
náhodou	náhoda	k1gFnSc7	náhoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
expanze	expanze	k1gFnSc1	expanze
směřovala	směřovat	k5eAaImAgFnS	směřovat
na	na	k7c4	na
východ	východ	k1gInSc4	východ
podél	podél	k7c2	podél
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
obchodní	obchodní	k2eAgFnSc2d1	obchodní
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
Kyjeva	Kyjev	k1gInSc2	Kyjev
(	(	kIx(	(
<g/>
byla	být	k5eAaImAgFnS	být
sem	sem	k6eAd1	sem
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Maďaři	Maďar	k1gMnPc1	Maďar
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Podunají	Podunají	k1gNnSc4	Podunají
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
vedla	vést	k5eAaImAgFnS	vést
původně	původně	k6eAd1	původně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
Boleslavovi	Boleslav	k1gMnSc3	Boleslav
I.	I.	kA	I.
podařilo	podařit	k5eAaPmAgNnS	podařit
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
a	a	k8xC	a
z	z	k7c2	z
obchodu	obchod	k1gInSc2	obchod
především	především	k9	především
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
vzácnými	vzácný	k2eAgFnPc7d1	vzácná
látkami	látka	k1gFnPc7	látka
<g/>
,	,	kIx,	,
koňmi	kůň	k1gMnPc7	kůň
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
ekonomicky	ekonomicky	k6eAd1	ekonomicky
těžit	těžit	k5eAaImF	těžit
<g/>
.	.	kIx.	.
</s>
<s>
Kupec	kupec	k1gMnSc1	kupec
Ibráhím	Ibráhí	k1gMnSc7	Ibráhí
ibn	ibn	k?	ibn
Jákúb	Jákúb	k1gInSc1	Jákúb
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
nejstarší	starý	k2eAgInSc1d3	nejstarší
popis	popis	k1gInSc1	popis
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Boleslava	Boleslav	k1gMnSc4	Boleslav
jakožto	jakožto	k8xS	jakožto
vládce	vládce	k1gMnSc1	vládce
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Krakova	Krakov	k1gInSc2	Krakov
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
"	"	kIx"	"
<g/>
krále	král	k1gMnSc2	král
severu	sever	k1gInSc2	sever
<g/>
"	"	kIx"	"
Měška	Měška	k1gFnSc1	Měška
I.	I.	kA	I.
<g/>
Území	území	k1gNnSc4	území
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
Krakovska	Krakovsko	k1gNnSc2	Krakovsko
<g/>
,	,	kIx,	,
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Červených	Červených	k2eAgInPc2d1	Červených
hradů	hrad	k1gInPc2	hrad
tedy	tedy	k9	tedy
plně	plně	k6eAd1	plně
neovládal	ovládat	k5eNaImAgMnS	ovládat
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
zde	zde	k6eAd1	zde
stála	stát	k5eAaImAgNnP	stát
hradiště	hradiště	k1gNnPc1	hradiště
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
kontrolovala	kontrolovat	k5eAaImAgFnS	kontrolovat
provoz	provoz	k1gInSc4	provoz
na	na	k7c6	na
obchodní	obchodní	k2eAgFnSc6d1	obchodní
magistrále	magistrála	k1gFnSc6	magistrála
<g/>
,	,	kIx,	,
a	a	k8xC	a
využíval	využívat	k5eAaPmAgInS	využívat
těchto	tento	k3xDgInPc2	tento
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
kořistních	kořistní	k2eAgNnPc2d1	kořistní
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Církevní	církevní	k2eAgFnSc1d1	církevní
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k9	již
Boleslav	Boleslav	k1gMnSc1	Boleslav
I.	I.	kA	I.
začal	začít	k5eAaPmAgMnS	začít
směřovat	směřovat	k5eAaImF	směřovat
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
významnému	významný	k2eAgInSc3d1	významný
politickému	politický	k2eAgInSc3d1	politický
cíli	cíl	k1gInSc3	cíl
<g/>
,	,	kIx,	,
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
jeho	jeho	k3xOp3gMnSc2	jeho
syna	syn	k1gMnSc2	syn
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
bylo	být	k5eAaImAgNnS	být
vlastní	vlastní	k2eAgNnSc4d1	vlastní
biskupství	biskupství	k1gNnSc4	biskupství
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
podporoval	podporovat	k5eAaImAgInS	podporovat
vznik	vznik	k1gInSc4	vznik
kultu	kult	k1gInSc2	kult
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Čechy	Čechy	k1gFnPc1	Čechy
měly	mít	k5eAaImAgFnP	mít
získat	získat	k5eAaPmF	získat
vlastního	vlastní	k2eAgNnSc2d1	vlastní
svatého	svatý	k2eAgNnSc2d1	svaté
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zvětšit	zvětšit	k5eAaPmF	zvětšit
možnost	možnost	k1gFnSc4	možnost
získání	získání	k1gNnSc4	získání
biskupství	biskupství	k1gNnSc2	biskupství
<g/>
.	.	kIx.	.
</s>
<s>
Pražské	pražský	k2eAgNnSc4d1	Pražské
biskupství	biskupství	k1gNnSc4	biskupství
pro	pro	k7c4	pro
českého	český	k2eAgMnSc4d1	český
panovníka	panovník	k1gMnSc4	panovník
znamenalo	znamenat	k5eAaImAgNnS	znamenat
větší	veliký	k2eAgFnSc4d2	veliký
váhu	váha	k1gFnSc4	váha
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
politickém	politický	k2eAgNnSc6d1	politické
poli	pole	k1gNnSc6	pole
a	a	k8xC	a
také	také	k9	také
větší	veliký	k2eAgFnSc4d2	veliký
politickou	politický	k2eAgFnSc4d1	politická
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
církevní	církevní	k2eAgMnSc1d1	církevní
<g/>
)	)	kIx)	)
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
Boleslavovy	Boleslavův	k2eAgFnSc2d1	Boleslavova
vlády	vláda	k1gFnSc2	vláda
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
prvního	první	k4xOgInSc2	první
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
ženský	ženský	k2eAgInSc4d1	ženský
benediktinský	benediktinský	k2eAgInSc4d1	benediktinský
klášter	klášter	k1gInSc4	klášter
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Abatyší	abatyše	k1gFnSc7	abatyše
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Boleslavova	Boleslavův	k2eAgFnSc1d1	Boleslavova
dcera	dcera	k1gFnSc1	dcera
Mlada	Mlada	k1gFnSc1	Mlada
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
dceru	dcera	k1gFnSc4	dcera
Dobravu	Dobrava	k1gFnSc4	Dobrava
(	(	kIx(	(
<g/>
v	v	k7c6	v
českých	český	k2eAgInPc6d1	český
historických	historický	k2eAgInPc6d1	historický
textech	text	k1gInPc6	text
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
Doubravka	Doubravka	k1gFnSc1	Doubravka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sám	sám	k3xTgMnSc1	sám
Kosmas	Kosmas	k1gMnSc1	Kosmas
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
Dobrava	Dobrava	k1gFnSc1	Dobrava
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
Bona	bona	k1gFnSc1	bona
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
provdal	provdat	k5eAaPmAgMnS	provdat
za	za	k7c2	za
prvního	první	k4xOgNnSc2	první
historicky	historicky	k6eAd1	historicky
doloženého	doložený	k2eAgNnSc2d1	doložené
polského	polský	k2eAgNnSc2d1	polské
knížete	kníže	k1gNnSc2wR	kníže
Měška	Měško	k1gNnSc2	Měško
I.	I.	kA	I.
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
významně	významně	k6eAd1	významně
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
christianizaci	christianizace	k1gFnSc3	christianizace
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Měšek	Měšek	k1gMnSc1	Měšek
byl	být	k5eAaImAgMnS	být
totiž	totiž	k9	totiž
pohan	pohan	k1gMnSc1	pohan
a	a	k8xC	a
Boleslav	Boleslav	k1gMnSc1	Boleslav
mu	on	k3xPp3gMnSc3	on
svoji	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
dal	dát	k5eAaPmAgMnS	dát
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
pod	pod	k7c7	pod
příslibem	příslib	k1gInSc7	příslib
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
pokřtít	pokřtít	k5eAaPmF	pokřtít
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
Dobravě	Dobrava	k1gFnSc3	Dobrava
umožnil	umožnit	k5eAaPmAgInS	umožnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
vzala	vzít	k5eAaPmAgFnS	vzít
skupinu	skupina	k1gFnSc4	skupina
kněží	kněz	k1gMnPc2	kněz
a	a	k8xC	a
mnichů	mnich	k1gMnPc2	mnich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
vyjednával	vyjednávat	k5eAaImAgInS	vyjednávat
také	také	k9	také
o	o	k7c4	o
založení	založení	k1gNnSc4	založení
pražského	pražský	k2eAgNnSc2d1	Pražské
biskupství	biskupství	k1gNnSc2	biskupství
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgNnSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
ostatky	ostatek	k1gInPc1	ostatek
Boleslava	Boleslav	k1gMnSc2	Boleslav
I.	I.	kA	I.
==	==	k?	==
</s>
</p>
<p>
<s>
Kosmas	Kosmas	k1gMnSc1	Kosmas
k	k	k7c3	k
roku	rok	k1gInSc3	rok
967	[number]	k4	967
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
kníže	kníže	k1gMnSc1	kníže
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
mající	mající	k2eAgMnSc1d1	mající
příjmení	příjmení	k1gNnPc2	příjmení
Ukrutný	ukrutný	k2eAgMnSc1d1	ukrutný
<g/>
,	,	kIx,	,
pozbyl	pozbýt	k5eAaPmAgMnS	pozbýt
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
,	,	kIx,	,
bratrovou	bratrův	k2eAgFnSc7d1	bratrova
krví	krev	k1gFnSc7	krev
zle	zle	k6eAd1	zle
získaného	získaný	k2eAgNnSc2d1	získané
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Kronikář	kronikář	k1gMnSc1	kronikář
Widukind	Widukind	k1gMnSc1	Widukind
z	z	k7c2	z
Corvey	Corvea	k1gFnSc2	Corvea
naopak	naopak	k6eAd1	naopak
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
ještě	ještě	k9	ještě
v	v	k7c6	v
září	září	k1gNnSc6	září
967	[number]	k4	967
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
Boleslav	Boleslav	k1gMnSc1	Boleslav
svému	svůj	k1gMnSc3	svůj
zeti	zeť	k1gMnSc3	zeť
<g/>
,	,	kIx,	,
polskému	polský	k2eAgMnSc3d1	polský
knížeti	kníže	k1gMnSc3	kníže
Měškovi	Měšek	k1gMnSc3	Měšek
<g/>
,	,	kIx,	,
v	v	k7c6	v
boji	boj	k1gInSc6	boj
se	s	k7c7	s
saským	saský	k2eAgMnSc7d1	saský
hrabětem	hrabě	k1gMnSc7	hrabě
Wichmannem	Wichmann	k1gMnSc7	Wichmann
<g/>
.	.	kIx.	.
</s>
<s>
Widukind	Widukind	k1gMnSc1	Widukind
sice	sice	k8xC	sice
používá	používat	k5eAaImIp3nS	používat
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
gener	gener	k1gInSc1	gener
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
znamenat	znamenat	k5eAaImF	znamenat
i	i	k9	i
švagr	švagr	k1gMnSc1	švagr
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
knížete	kníže	k1gMnSc2	kníže
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
knížetem	kníže	k1gMnSc7	kníže
Boleslavem	Boleslav	k1gMnSc7	Boleslav
vždy	vždy	k6eAd1	vždy
myslel	myslet	k5eAaImAgMnS	myslet
Boleslava	Boleslav	k1gMnSc4	Boleslav
I.	I.	kA	I.
</s>
</p>
<p>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
data	datum	k1gNnPc1	datum
Boleslavova	Boleslavův	k2eAgInSc2d1	Boleslavův
skonu	skon	k1gInSc2	skon
<g/>
,	,	kIx,	,
kloní	klonit	k5eAaImIp3nP	klonit
se	se	k3xPyFc4	se
dnešní	dnešní	k2eAgMnPc1d1	dnešní
historici	historik	k1gMnPc1	historik
spíše	spíše	k9	spíše
k	k	k7c3	k
roku	rok	k1gInSc3	rok
972	[number]	k4	972
<g/>
.	.	kIx.	.
</s>
<s>
Naznačují	naznačovat	k5eAaImIp3nP	naznačovat
to	ten	k3xDgNnSc1	ten
jiné	jiný	k2eAgInPc1d1	jiný
historické	historický	k2eAgInPc1d1	historický
prameny	pramen	k1gInPc1	pramen
i	i	k8xC	i
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
<g/>
.	.	kIx.	.
</s>
<s>
Kosmas	Kosmas	k1gMnSc1	Kosmas
se	se	k3xPyFc4	se
ostatně	ostatně	k6eAd1	ostatně
vesměs	vesměs	k6eAd1	vesměs
mýlil	mýlit	k5eAaImAgMnS	mýlit
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
důležitých	důležitý	k2eAgNnPc2d1	důležité
dat	datum	k1gNnPc2	datum
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Historik	historik	k1gMnSc1	historik
Petr	Petr	k1gMnSc1	Petr
Kubín	Kubín	k1gMnSc1	Kubín
dokonce	dokonce	k9	dokonce
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Kosmas	Kosmas	k1gMnSc1	Kosmas
rok	rok	k1gInSc4	rok
967	[number]	k4	967
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
aby	aby	k9	aby
nemusel	muset	k5eNaImAgInS	muset
bratrovrahovi	bratrovrah	k1gMnSc3	bratrovrah
přiznat	přiznat	k5eAaPmF	přiznat
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c4	na
zřízení	zřízení	k1gNnSc4	zřízení
pražského	pražský	k2eAgNnSc2d1	Pražské
biskupství	biskupství	k1gNnSc2	biskupství
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pražské	pražský	k2eAgNnSc1d1	Pražské
biskupství	biskupství	k1gNnSc1	biskupství
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
i	i	k9	i
přes	přes	k7c4	přes
úpornou	úporný	k2eAgFnSc4d1	úporná
snahu	snaha	k1gFnSc4	snaha
Boleslavovu	Boleslavův	k2eAgFnSc4d1	Boleslavova
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
dřívější	dřívější	k2eAgNnSc4d1	dřívější
založení	založení	k1gNnSc4	založení
(	(	kIx(	(
<g/>
ještě	ještě	k9	ještě
za	za	k7c2	za
Boleslavova	Boleslavův	k2eAgInSc2d1	Boleslavův
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
zřízeno	zřízen	k2eAgNnSc1d1	zřízeno
až	až	k9	až
rok	rok	k1gInSc4	rok
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
roku	rok	k1gInSc2	rok
973	[number]	k4	973
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neví	vědět	k5eNaImIp3nS	vědět
se	se	k3xPyFc4	se
přesně	přesně	k6eAd1	přesně
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
Boleslav	Boleslav	k1gMnSc1	Boleslav
I.	I.	kA	I.
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
mohlo	moct	k5eAaImAgNnS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
přemyslovské	přemyslovský	k2eAgNnSc4d1	přemyslovské
pohřebiště	pohřebiště	k1gNnSc4	pohřebiště
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
dal	dát	k5eAaPmAgMnS	dát
vystavět	vystavět	k5eAaPmF	vystavět
Boleslavův	Boleslavův	k2eAgMnSc1d1	Boleslavův
otec	otec	k1gMnSc1	otec
Vratislav	Vratislav	k1gMnSc1	Vratislav
I.	I.	kA	I.
Snad	snad	k9	snad
i	i	k9	i
o	o	k7c4	o
hrob	hrob	k1gInSc4	hrob
označovaný	označovaný	k2eAgInSc4d1	označovaný
K	k	k7c3	k
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
objevený	objevený	k2eAgInSc1d1	objevený
při	při	k7c6	při
dostavbě	dostavba	k1gFnSc6	dostavba
katedrály	katedrála	k1gFnSc2	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jistě	jistě	k6eAd1	jistě
patřil	patřit	k5eAaImAgMnS	patřit
význačné	význačný	k2eAgFnSc3d1	význačná
osobě	osoba	k1gFnSc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Ostatky	ostatek	k1gInPc1	ostatek
muže	muž	k1gMnSc2	muž
objeveného	objevený	k2eAgMnSc2d1	objevený
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
hrobě	hrob	k1gInSc6	hrob
Emanuel	Emanuel	k1gMnSc1	Emanuel
Vlček	Vlček	k1gMnSc1	Vlček
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
knížete	kníže	k1gMnSc4	kníže
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
I.	I.	kA	I.
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnPc4	teorie
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
zpochybňována	zpochybňovat	k5eAaImNgFnS	zpochybňovat
<g/>
.	.	kIx.	.
</s>
<s>
Archeologové	archeolog	k1gMnPc1	archeolog
Jan	Jan	k1gMnSc1	Jan
Frolík	Frolík	k1gMnSc1	Frolík
a	a	k8xC	a
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Smetánka	smetánka	k1gFnSc1	smetánka
své	svůj	k3xOyFgInPc4	svůj
poznatky	poznatek	k1gInPc4	poznatek
shrnuli	shrnout	k5eAaPmAgMnP	shrnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrobka	hrobka	k1gFnSc1	hrobka
K1	K1	k1gFnSc2	K1
(	(	kIx(	(
<g/>
a	a	k8xC	a
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
snad	snad	k9	snad
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
pohřbena	pohřben	k2eAgFnSc1d1	pohřbena
Boleslavova	Boleslavův	k2eAgFnSc1d1	Boleslavova
manželka	manželka	k1gFnSc1	manželka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
po	po	k7c6	po
roce	rok	k1gInSc6	rok
938	[number]	k4	938
<g/>
,	,	kIx,	,
dlouho	dlouho	k6eAd1	dlouho
po	po	k7c6	po
Bořivojově	Bořivojův	k2eAgFnSc6d1	Bořivojova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potomci	potomek	k1gMnPc5	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zřejmě	zřejmě	k6eAd1	zřejmě
byla	být	k5eAaImAgFnS	být
Biagota	Biagota	k1gFnSc1	Biagota
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
Boleslav	Boleslav	k1gMnSc1	Boleslav
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dožily	dožít	k5eAaPmAgFnP	dožít
dospělosti	dospělost	k1gFnPc1	dospělost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
932	[number]	k4	932
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
999	[number]	k4	999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
<g/>
∞	∞	k?	∞
EmmaStrachkvas	EmmaStrachkvas	k1gMnSc1	EmmaStrachkvas
(	(	kIx(	(
<g/>
935	[number]	k4	935
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
996	[number]	k4	996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Doubravka	Doubravka	k1gFnSc1	Doubravka
(	(	kIx(	(
<g/>
†	†	k?	†
977	[number]	k4	977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polská	polský	k2eAgFnSc1d1	polská
kněžna	kněžna	k1gFnSc1	kněžna
<g/>
∞	∞	k?	∞
965	[number]	k4	965
Měšek	Měšek	k1gInSc1	Měšek
I.	I.	kA	I.
<g/>
Mlada	Mlada	k1gFnSc1	Mlada
(	(	kIx(	(
<g/>
†	†	k?	†
994	[number]	k4	994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
abatyše	abatyše	k1gFnSc1	abatyše
u	u	k7c2	u
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc5	Jiří
</s>
</p>
<p>
<s>
==	==	k?	==
Genealogie	genealogie	k1gFnSc2	genealogie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BLÁHOVÁ	Bláhová	k1gFnSc1	Bláhová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
FROLÍK	FROLÍK	kA	FROLÍK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
PROFANTOVÁ	PROFANTOVÁ	kA	PROFANTOVÁ
<g/>
,	,	kIx,	,
Naďa	Naďa	k1gFnSc1	Naďa
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
I.	I.	kA	I.
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
800	[number]	k4	800
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
265	[number]	k4	265
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHARVÁT	Charvát	k1gMnSc1	Charvát
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Zrod	zrod	k1gInSc1	zrod
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
568	[number]	k4	568
<g/>
-	-	kIx~	-
<g/>
1055	[number]	k4	1055
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
263	[number]	k4	263
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
845	[number]	k4	845
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LUTOVSKÝ	LUTOVSKÝ	kA	LUTOVSKÝ
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Bratrovrah	bratrovrah	k1gMnSc1	bratrovrah
a	a	k8xC	a
tvůrce	tvůrce	k1gMnSc1	tvůrce
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
knížete	kníže	k1gMnSc2	kníže
Boleslava	Boleslav	k1gMnSc2	Boleslav
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Set	set	k1gInSc1	set
out	out	k?	out
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
162	[number]	k4	162
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902058	[number]	k4	902058
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LUTOVSKÝ	LUTOVSKÝ	kA	LUTOVSKÝ
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
prvních	první	k4xOgInPc2	první
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
I.	I.	kA	I.
Zrození	zrození	k1gNnSc1	zrození
státu	stát	k1gInSc2	stát
872	[number]	k4	872
<g/>
-	-	kIx~	-
<g/>
972	[number]	k4	972
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
I.	I.	kA	I.
po	po	k7c4	po
Boleslava	Boleslav	k1gMnSc4	Boleslav
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
267	[number]	k4	267
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
308	[number]	k4	308
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MATLA-KOZŁOWSKA	MATLA-KOZŁOWSKA	k?	MATLA-KOZŁOWSKA
<g/>
,	,	kIx,	,
Marzena	Marzen	k2eAgFnSc1d1	Marzena
<g/>
.	.	kIx.	.
</s>
<s>
Pierwsi	Pierwse	k1gFnSc4	Pierwse
Przemyślidzi	Przemyślidze	k1gFnSc4	Przemyślidze
i	i	k9	i
ich	ich	k?	ich
państwo	państwo	k6eAd1	państwo
(	(	kIx(	(
<g/>
od	od	k7c2	od
X	X	kA	X
do	do	k7c2	do
połowy	połowa	k1gFnSc2	połowa
XI	XI	kA	XI
wieku	wieku	k6eAd1	wieku
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ekspansja	Ekspansj	k2eAgFnSc1d1	Ekspansj
terytorialna	terytorialna	k1gFnSc1	terytorialna
i	i	k8xC	i
jej	on	k3xPp3gMnSc4	on
polityczne	politycznout	k5eAaPmIp3nS	politycznout
uwarunkowania	uwarunkowanium	k1gNnPc4	uwarunkowanium
<g/>
.	.	kIx.	.
</s>
<s>
Poznań	Poznań	k?	Poznań
<g/>
:	:	kIx,	:
Wydawnictwo	Wydawnictwo	k1gMnSc1	Wydawnictwo
Poznańskie	Poznańskie	k1gFnSc2	Poznańskie
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
576	[number]	k4	576
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
7177	[number]	k4	7177
<g/>
-	-	kIx~	-
<g/>
547	[number]	k4	547
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
I.	I.	kA	I.
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
Od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
knížete	kníže	k1gMnSc2	kníže
Oldřicha	Oldřich	k1gMnSc2	Oldřich
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
782	[number]	k4	782
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
61	[number]	k4	61
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SOMMER	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
;	;	kIx,	;
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc4	budování
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
779	[number]	k4	779
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
352	[number]	k4	352
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
:	:	kIx,	:
vstup	vstup	k1gInSc1	vstup
Čechů	Čech	k1gMnPc2	Čech
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
(	(	kIx(	(
<g/>
530	[number]	k4	530
<g/>
–	–	k?	–
<g/>
935	[number]	k4	935
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
658	[number]	k4	658
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
138	[number]	k4	138
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
6	[number]	k4	6
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
Boh	Boh	k1gFnSc1	Boh
<g/>
–	–	k?	–
<g/>
Bož	Boža	k1gFnPc2	Boža
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
109	[number]	k4	109
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
239	[number]	k4	239
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
36	[number]	k4	36
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Morava	Morava	k1gFnSc1	Morava
v	v	k7c6	v
době	doba	k1gFnSc6	doba
knížecí	knížecí	k2eAgFnSc1d1	knížecí
906	[number]	k4	906
<g/>
–	–	k?	–
<g/>
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
464	[number]	k4	464
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
563	[number]	k4	563
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Boleslav	Boleslav	k1gMnSc1	Boleslav
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Děje	dít	k5eAaImIp3nS	dít
království	království	k1gNnSc4	království
českého	český	k2eAgInSc2d1	český
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
-	-	kIx~	-
Boleslav	Boleslav	k1gMnSc1	Boleslav
I.	I.	kA	I.
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Lutovský	Lutovský	k1gMnSc1	Lutovský
<g/>
:	:	kIx,	:
BOLESLAV	Boleslav	k1gMnSc1	Boleslav
I.	I.	kA	I.
–	–	k?	–
SMRT	smrt	k1gFnSc1	smrt
A	a	k8xC	a
HROB	hrob	k1gInSc1	hrob
</s>
</p>
<p>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
I.	I.	kA	I.
v	v	k7c6	v
Kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yRnSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
</s>
</p>
<p>
<s>
Říše	říše	k1gFnSc1	říše
českých	český	k2eAgMnPc2d1	český
Boleslavů	Boleslav	k1gMnPc2	Boleslav
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
