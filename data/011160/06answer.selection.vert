<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
I.	I.	kA	I.
zvaný	zvaný	k2eAgInSc1d1	zvaný
Ukrutný	ukrutný	k2eAgInSc1d1	ukrutný
(	(	kIx(	(
<g/>
asi	asi	k9	asi
915	[number]	k4	915
–	–	k?	–
967	[number]	k4	967
či	či	k8xC	či
972	[number]	k4	972
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
v	v	k7c6	v
letech	let	k1gInPc6	let
935	[number]	k4	935
<g/>
–	–	k?	–
<g/>
967	[number]	k4	967
<g/>
/	/	kIx~	/
<g/>
972	[number]	k4	972
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Vratislava	Vratislav	k1gMnSc2	Vratislav
I.	I.	kA	I.
a	a	k8xC	a
Drahomíry	Drahomíra	k1gFnSc2	Drahomíra
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
