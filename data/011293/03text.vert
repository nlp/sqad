<p>
<s>
Algicidy	Algicid	k1gInPc1	Algicid
jsou	být	k5eAaImIp3nP	být
skupinou	skupina	k1gFnSc7	skupina
pesticidů	pesticid	k1gInPc2	pesticid
určených	určený	k2eAgInPc2d1	určený
k	k	k7c3	k
hubení	hubení	k1gNnSc3	hubení
řas	řasa	k1gFnPc2	řasa
(	(	kIx(	(
<g/>
Algae	Algae	k1gInSc1	Algae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
klasifikovány	klasifikován	k2eAgInPc1d1	klasifikován
jako	jako	k8xS	jako
herbicidy	herbicid	k1gInPc1	herbicid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
algicid	algicid	k1gInSc1	algicid
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
bazénech	bazén	k1gInPc6	bazén
či	či	k8xC	či
ve	v	k7c6	v
vodárenství	vodárenství	k1gNnSc6	vodárenství
užíván	užíván	k2eAgInSc1d1	užíván
plynný	plynný	k2eAgInSc1d1	plynný
chlór	chlór	k1gInSc1	chlór
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
užíván	užíván	k2eAgInSc4d1	užíván
atrazin	atrazin	k1gInSc4	atrazin
<g/>
,	,	kIx,	,
diuron	diuron	k1gInSc4	diuron
<g/>
,	,	kIx,	,
síran	síran	k1gInSc4	síran
měďnatý	měďnatý	k2eAgInSc4d1	měďnatý
<g/>
,	,	kIx,	,
simazin	simazin	k2eAgInSc4d1	simazin
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
byla	být	k5eAaImAgFnS	být
užívána	užívat	k5eAaImNgFnS	užívat
pro	pro	k7c4	pro
hubení	hubení	k1gNnSc4	hubení
řas	řasa	k1gFnPc2	řasa
tradičně	tradičně	k6eAd1	tradičně
sláma	sláma	k1gFnSc1	sláma
ječmene	ječmen	k1gInSc2	ječmen
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
poškozování	poškozování	k1gNnSc3	poškozování
jiných	jiný	k2eAgFnPc2d1	jiná
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
