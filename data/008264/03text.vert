<p>
<s>
Georges	Georges	k1gMnSc1	Georges
Jacques	Jacques	k1gMnSc1	Jacques
Danton	Danton	k1gInSc1	Danton
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1759	[number]	k4	1759
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1794	[number]	k4	1794
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
osobností	osobnost	k1gFnSc7	osobnost
rané	raný	k2eAgFnSc2d1	raná
fáze	fáze	k1gFnSc2	fáze
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Danton	Danton	k1gInSc1	Danton
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1759	[number]	k4	1759
v	v	k7c6	v
Arcis-sur-Aube	Arcisur-Aub	k1gInSc5	Arcis-sur-Aub
jako	jako	k9	jako
syn	syn	k1gMnSc1	syn
Jacquese	Jacques	k1gMnSc2	Jacques
Dantona	Danton	k1gMnSc2	Danton
a	a	k8xC	a
Mary	Mary	k1gFnSc2	Mary
Camusové	Camusový	k2eAgFnSc2d1	Camusový
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiu	studio	k1gNnSc6	studio
práv	právo	k1gNnPc2	právo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
advokátem	advokát	k1gMnSc7	advokát
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1787	[number]	k4	1787
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Antonietou	Antonietý	k2eAgFnSc7d1	Antonietý
Gabrielou	Gabriela	k1gFnSc7	Gabriela
Charpentier	Charpentira	k1gFnPc2	Charpentira
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1785	[number]	k4	1785
<g/>
–	–	k?	–
<g/>
1791	[number]	k4	1791
byl	být	k5eAaImAgMnS	být
advokátem	advokát	k1gMnSc7	advokát
královské	královský	k2eAgFnSc2d1	královská
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1790	[number]	k4	1790
založil	založit	k5eAaPmAgInS	založit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Camillem	Camill	k1gMnSc7	Camill
Desmoulinsem	Desmoulins	k1gMnSc7	Desmoulins
(	(	kIx(	(
<g/>
novinářem	novinář	k1gMnSc7	novinář
a	a	k8xC	a
přítelem	přítel	k1gMnSc7	přítel
z	z	k7c2	z
mládí	mládí	k1gNnSc2	mládí
<g/>
)	)	kIx)	)
a	a	k8xC	a
J.	J.	kA	J.
<g/>
-	-	kIx~	-
<g/>
P.	P.	kA	P.
Maratem	Marat	k1gInSc7	Marat
klub	klub	k1gInSc1	klub
kordelierů	kordelier	k1gMnPc2	kordelier
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1791	[number]	k4	1791
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
komuny	komuna	k1gFnSc2	komuna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1792	[number]	k4	1792
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
svržení	svržení	k1gNnSc4	svržení
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
také	také	k9	také
po	po	k7c6	po
ustanovení	ustanovení	k1gNnSc6	ustanovení
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
získal	získat	k5eAaPmAgMnS	získat
post	post	k1gInSc4	post
ministra	ministr	k1gMnSc2	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
zářijových	zářijový	k2eAgInPc6d1	zářijový
masakrech	masakr	k1gInPc6	masakr
je	být	k5eAaImIp3nS	být
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
,	,	kIx,	,
ví	vědět	k5eAaImIp3nS	vědět
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
schválil	schválit	k5eAaPmAgMnS	schválit
a	a	k8xC	a
nesnažil	snažit	k5eNaImAgMnS	snažit
se	se	k3xPyFc4	se
jim	on	k3xPp3gNnPc3	on
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
do	do	k7c2	do
národního	národní	k2eAgInSc2d1	národní
konventu	konvent	k1gInSc2	konvent
<g/>
,	,	kIx,	,
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
francouzských	francouzský	k2eAgFnPc2d1	francouzská
armád	armáda	k1gFnPc2	armáda
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
ministra	ministr	k1gMnSc2	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1793	[number]	k4	1793
hlasoval	hlasovat	k5eAaImAgMnS	hlasovat
pro	pro	k7c4	pro
smrt	smrt	k1gFnSc4	smrt
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
revolučního	revoluční	k2eAgInSc2d1	revoluční
tribunálu	tribunál	k1gInSc2	tribunál
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
nového	nový	k2eAgInSc2d1	nový
rozhodovacího	rozhodovací	k2eAgInSc2d1	rozhodovací
orgánu	orgán	k1gInSc2	orgán
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
veřejné	veřejný	k2eAgNnSc4d1	veřejné
blaho	blaho	k1gNnSc4	blaho
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
francouzské	francouzský	k2eAgMnPc4d1	francouzský
vojáky	voják	k1gMnPc4	voják
a	a	k8xC	a
podporoval	podporovat	k5eAaImAgMnS	podporovat
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
nový	nový	k2eAgInSc4d1	nový
systém	systém	k1gInSc4	systém
vzdělávaní	vzdělávaný	k2eAgMnPc1d1	vzdělávaný
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
legislativního	legislativní	k2eAgInSc2d1	legislativní
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
vytváření	vytváření	k1gNnSc4	vytváření
nového	nový	k2eAgInSc2d1	nový
systému	systém	k1gInSc2	systém
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
se	se	k3xPyFc4	se
svržení	svržení	k1gNnSc2	svržení
Girondistů	girondista	k1gMnPc2	girondista
a	a	k8xC	a
chopil	chopit	k5eAaPmAgMnS	chopit
se	se	k3xPyFc4	se
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
kordeliéry	kordeliér	k1gMnPc7	kordeliér
absolutní	absolutní	k2eAgFnSc2d1	absolutní
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
z	z	k7c2	z
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
veřejné	veřejný	k2eAgNnSc4d1	veřejné
blaho	blaho	k1gNnSc4	blaho
diktátorský	diktátorský	k2eAgInSc1d1	diktátorský
orgán	orgán	k1gInSc1	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
však	však	k9	však
byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
jeho	jeho	k3xOp3gMnSc1	jeho
spojenec	spojenec	k1gMnSc1	spojenec
Marat	Marat	k2eAgMnSc1d1	Marat
a	a	k8xC	a
Danton	Danton	k1gInSc1	Danton
byl	být	k5eAaImAgInS	být
donucen	donucen	k2eAgMnSc1d1	donucen
opustit	opustit	k5eAaPmF	opustit
křeslo	křeslo	k1gNnSc4	křeslo
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
veřejné	veřejný	k2eAgNnSc4d1	veřejné
blaho	blaho	k1gNnSc4	blaho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zabránit	zabránit	k5eAaPmF	zabránit
teroru	teror	k1gInSc3	teror
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgInS	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
boje	boj	k1gInSc2	boj
s	s	k7c7	s
radikálními	radikální	k2eAgMnPc7d1	radikální
jakobíny	jakobín	k1gMnPc7	jakobín
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Robespierra	Robespierr	k1gInSc2	Robespierr
<g/>
.	.	kIx.	.
</s>
<s>
Teror	teror	k1gInSc1	teror
zastavit	zastavit	k5eAaPmF	zastavit
však	však	k9	však
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1794	[number]	k4	1794
po	po	k7c6	po
popravě	poprava	k1gFnSc6	poprava
Hébertistů	Hébertista	k1gMnPc2	Hébertista
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
kordeliéry	kordeliér	k1gMnPc7	kordeliér
Robespierrem	Robespierro	k1gNnSc7	Robespierro
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
korupce	korupce	k1gFnSc2	korupce
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1794	[number]	k4	1794
popraven	popraven	k2eAgInSc1d1	popraven
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
poprava	poprava	k1gFnSc1	poprava
otevřela	otevřít	k5eAaPmAgFnS	otevřít
dveře	dveře	k1gFnPc4	dveře
extremní	extremní	k2eAgFnSc2d1	extremní
části	část	k1gFnSc2	část
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FURET	FURET	kA	FURET
<g/>
,	,	kIx,	,
François	François	k1gFnSc1	François
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Turgota	Turgot	k1gMnSc2	Turgot
k	k	k7c3	k
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
1770	[number]	k4	1770
<g/>
-	-	kIx~	-
<g/>
1814	[number]	k4	1814
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
450	[number]	k4	450
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
452	[number]	k4	452
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MAUROIS	MAUROIS	kA	MAUROIS
<g/>
,	,	kIx,	,
André	André	k1gMnSc1	André
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dějiny	dějiny	k1gFnPc1	dějiny
Francie	Francie	k1gFnSc2	Francie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TINKOVÁ	TINKOVÁ	kA	TINKOVÁ
<g/>
,	,	kIx,	,
Daniela	Daniela	k1gFnSc1	Daniela
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Revoluční	revoluční	k2eAgFnSc1d1	revoluční
Francie	Francie	k1gFnSc1	Francie
1787	[number]	k4	1787
<g/>
-	-	kIx~	-
<g/>
1799	[number]	k4	1799
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Triton	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
426	[number]	k4	426
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7387	[number]	k4	7387
<g/>
-	-	kIx~	-
<g/>
211	[number]	k4	211
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Georges	Georges	k1gInSc1	Georges
Danton	Danton	k1gInSc4	Danton
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Georges	Georges	k1gMnSc1	Georges
Jacques	Jacques	k1gMnSc1	Jacques
Danton	Danton	k1gInSc4	Danton
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Danton	Danton	k1gInSc1	Danton
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
