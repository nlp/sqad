<p>
<s>
Maso	maso	k1gNnSc1	maso
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
kosterní	kosterní	k2eAgFnSc4d1	kosterní
svalovinu	svalovina	k1gFnSc4	svalovina
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
související	související	k2eAgFnPc4d1	související
tkáně	tkáň	k1gFnPc4	tkáň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
a	a	k8xC	a
méně	málo	k6eAd2	málo
běžném	běžný	k2eAgInSc6d1	běžný
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
veškeré	veškerý	k3xTgFnSc6	veškerý
poživatelné	poživatelný	k2eAgFnSc6d1	poživatelná
části	část	k1gFnSc6	část
těl	tělo	k1gNnPc2	tělo
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
<g/>
které	který	k3yQgInPc4	který
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
určili	určit	k5eAaPmAgMnP	určit
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
výživu	výživa	k1gFnSc4	výživa
(	(	kIx(	(
<g/>
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
střeva	střevo	k1gNnPc4	střevo
a	a	k8xC	a
vnitřnosti	vnitřnost	k1gFnPc4	vnitřnost
nevyjímaje	nevyjímaje	k7c4	nevyjímaje
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Maso	maso	k1gNnSc1	maso
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejoblíbenější	oblíbený	k2eAgFnPc4d3	nejoblíbenější
a	a	k8xC	a
nejvyhledávanější	vyhledávaný	k2eAgFnPc4d3	nejvyhledávanější
potraviny	potravina	k1gFnPc4	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
ceněno	cenit	k5eAaImNgNnS	cenit
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
obsah	obsah	k1gInSc4	obsah
biologicky	biologicky	k6eAd1	biologicky
hodnotných	hodnotný	k2eAgFnPc2d1	hodnotná
bílkovin	bílkovina	k1gFnPc2	bílkovina
a	a	k8xC	a
látek	látka	k1gFnPc2	látka
budujících	budující	k2eAgFnPc2d1	budující
organismus	organismus	k1gInSc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
plnohodnotných	plnohodnotný	k2eAgFnPc2d1	plnohodnotná
bílkovin	bílkovina	k1gFnPc2	bílkovina
kolísá	kolísat	k5eAaImIp3nS	kolísat
od	od	k7c2	od
16	[number]	k4	16
do	do	k7c2	do
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vitamínů	vitamín	k1gInPc2	vitamín
maso	maso	k1gNnSc1	maso
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zejména	zejména	k9	zejména
vitaminy	vitamin	k1gInPc4	vitamin
skupiny	skupina	k1gFnSc2	skupina
B.	B.	kA	B.
Dodává	dodávat	k5eAaImIp3nS	dodávat
organismu	organismus	k1gInSc6	organismus
železo	železo	k1gNnSc4	železo
a	a	k8xC	a
fosfor	fosfor	k1gInSc4	fosfor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tučná	tučný	k2eAgFnSc1d1	tučná
masa	masa	k1gFnSc1	masa
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
vepřové	vepřový	k2eAgNnSc1d1	vepřové
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
energetickou	energetický	k2eAgFnSc4d1	energetická
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřnosti	vnitřnost	k1gFnPc1	vnitřnost
mají	mít	k5eAaImIp3nP	mít
zas	zas	k6eAd1	zas
vyšší	vysoký	k2eAgFnSc4d2	vyšší
biologickou	biologický	k2eAgFnSc4d1	biologická
hodnotu	hodnota	k1gFnSc4	hodnota
než	než	k8xS	než
svalovina	svalovina	k1gFnSc1	svalovina
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yQnSc7	co
je	být	k5eAaImIp3nS	být
maso	maso	k1gNnSc4	maso
tučnější	tučný	k2eAgNnSc1d2	tučnější
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
hůře	zle	k6eAd2	zle
stravitelné	stravitelný	k2eAgNnSc1d1	stravitelné
<g/>
.	.	kIx.	.
</s>
<s>
Maso	maso	k1gNnSc1	maso
mladých	mladý	k2eAgNnPc2d1	mladé
zvířat	zvíře	k1gNnPc2	zvíře
je	být	k5eAaImIp3nS	být
jemnější	jemný	k2eAgInSc1d2	jemnější
<g/>
,	,	kIx,	,
maso	maso	k1gNnSc1	maso
starších	starý	k2eAgNnPc2d2	starší
zvířat	zvíře	k1gNnPc2	zvíře
bývá	bývat	k5eAaImIp3nS	bývat
tvrdší	tvrdý	k2eAgInSc1d2	tvrdší
a	a	k8xC	a
hůře	zle	k6eAd2	zle
stravitelné	stravitelný	k2eAgNnSc1d1	stravitelné
<g/>
.	.	kIx.	.
</s>
<s>
Lehká	lehký	k2eAgFnSc1d1	lehká
nebo	nebo	k8xC	nebo
těžší	těžký	k2eAgFnSc1d2	těžší
stravitelnost	stravitelnost	k1gFnSc1	stravitelnost
nezávisí	záviset	k5eNaImIp3nS	záviset
jen	jen	k9	jen
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
a	a	k8xC	a
stáří	stáří	k1gNnSc6	stáří
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
od	od	k7c2	od
"	"	kIx"	"
<g/>
odleženosti	odleženost	k1gFnSc2	odleženost
<g/>
"	"	kIx"	"
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
od	od	k7c2	od
způsobu	způsob	k1gInSc2	způsob
úpravy	úprava	k1gFnSc2	úprava
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgNnSc1	žádný
maso	maso	k1gNnSc1	maso
–	–	k?	–
kromě	kromě	k7c2	kromě
masa	maso	k1gNnSc2	maso
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
vnitřností	vnitřnost	k1gFnPc2	vnitřnost
–	–	k?	–
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nemělo	mít	k5eNaImAgNnS	mít
použít	použít	k5eAaPmF	použít
hned	hned	k6eAd1	hned
po	po	k7c4	po
zabití	zabití	k1gNnSc4	zabití
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Maso	maso	k1gNnSc1	maso
z	z	k7c2	z
čerstvě	čerstvě	k6eAd1	čerstvě
zabitých	zabitý	k2eAgNnPc2d1	zabité
zvířat	zvíře	k1gNnPc2	zvíře
totiž	totiž	k9	totiž
bývá	bývat	k5eAaImIp3nS	bývat
tuhé	tuhý	k2eAgNnSc1d1	tuhé
<g/>
,	,	kIx,	,
hůře	zle	k6eAd2	zle
stravitelné	stravitelný	k2eAgFnPc1d1	stravitelná
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
když	když	k8xS	když
odleží	odležet	k5eAaPmIp3nS	odležet
<g/>
,	,	kIx,	,
vyzraje	vyzrát	k5eAaPmIp3nS	vyzrát
<g/>
,	,	kIx,	,
změkne	změknout	k5eAaPmIp3nS	změknout
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
snadněji	snadno	k6eAd2	snadno
stravitelným	stravitelný	k2eAgInSc7d1	stravitelný
<g/>
.	.	kIx.	.
</s>
<s>
Doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
uskladnit	uskladnit	k5eAaPmF	uskladnit
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
podchlazeném	podchlazený	k2eAgNnSc6d1	podchlazené
místě	místo	k1gNnSc6	místo
na	na	k7c4	na
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
teplotou	teplota	k1gFnSc7	teplota
a	a	k8xC	a
vlhkostí	vlhkost	k1gFnSc7	vlhkost
se	se	k3xPyFc4	se
maso	maso	k1gNnSc1	maso
rychle	rychle	k6eAd1	rychle
kazí	kazit	k5eAaImIp3nS	kazit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
ho	on	k3xPp3gMnSc4	on
uskladňujeme	uskladňovat	k5eAaImIp1nP	uskladňovat
v	v	k7c6	v
chladu	chlad	k1gInSc6	chlad
a	a	k8xC	a
chráníme	chránit	k5eAaImIp1nP	chránit
před	před	k7c7	před
mouchami	moucha	k1gFnPc7	moucha
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nemáme	mít	k5eNaImIp1nP	mít
možnost	možnost	k1gFnSc4	možnost
uschovat	uschovat	k5eAaPmF	uschovat
syrové	syrový	k2eAgNnSc4d1	syrové
maso	maso	k1gNnSc4	maso
v	v	k7c6	v
chladu	chlad	k1gInSc6	chlad
(	(	kIx(	(
<g/>
v	v	k7c6	v
lednici	lednice	k1gFnSc6	lednice
nebo	nebo	k8xC	nebo
mrazáku	mrazák	k1gInSc6	mrazák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zabalíme	zabalit	k5eAaPmIp1nP	zabalit
ho	on	k3xPp3gNnSc4	on
do	do	k7c2	do
utěrky	utěrka	k1gFnSc2	utěrka
navlhčené	navlhčený	k2eAgFnSc2d1	navlhčená
octem	ocet	k1gInSc7	ocet
a	a	k8xC	a
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
jej	on	k3xPp3gMnSc4	on
použijeme	použít	k5eAaPmIp1nP	použít
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hovězí	hovězí	k2eAgNnSc1d1	hovězí
maso	maso	k1gNnSc1	maso
má	mít	k5eAaImIp3nS	mít
světle	světle	k6eAd1	světle
červenou	červená	k1gFnSc4	červená
až	až	k9	až
tmavě	tmavě	k6eAd1	tmavě
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
každé	každý	k3xTgNnSc1	každý
maso	maso	k1gNnSc1	maso
ze	z	k7c2	z
skotu	skot	k1gInSc2	skot
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
kvalitní	kvalitní	k2eAgFnSc1d1	kvalitní
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nemalý	malý	k2eNgInSc4d1	nemalý
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
masem	maso	k1gNnSc7	maso
mladých	mladý	k2eAgMnPc2d1	mladý
býčků	býček	k1gMnPc2	býček
a	a	k8xC	a
starších	starý	k2eAgFnPc2d2	starší
krav	kráva	k1gFnPc2	kráva
<g/>
.	.	kIx.	.
</s>
<s>
Maso	maso	k1gNnSc1	maso
mladých	mladý	k2eAgMnPc2d1	mladý
býčků	býček	k1gMnPc2	býček
má	mít	k5eAaImIp3nS	mít
světle	světle	k6eAd1	světle
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
skoro	skoro	k6eAd1	skoro
čistě	čistě	k6eAd1	čistě
bílý	bílý	k2eAgInSc1d1	bílý
tuk	tuk	k1gInSc1	tuk
<g/>
,	,	kIx,	,
maso	maso	k1gNnSc1	maso
ze	z	k7c2	z
starších	starý	k2eAgInPc2d2	starší
kusů	kus	k1gInPc2	kus
skotu	skot	k1gInSc2	skot
má	mít	k5eAaImIp3nS	mít
tmavě	tmavě	k6eAd1	tmavě
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
tuk	tuk	k1gInSc4	tuk
žlutý	žlutý	k2eAgInSc4d1	žlutý
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
hovězí	hovězí	k2eAgNnSc1d1	hovězí
maso	maso	k1gNnSc1	maso
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
nechat	nechat	k5eAaPmF	nechat
odležet	odležet	k5eAaPmF	odležet
alespoň	alespoň	k9	alespoň
na	na	k7c4	na
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zamezí	zamezit	k5eAaPmIp3nS	zamezit
jeho	jeho	k3xOp3gFnSc2	jeho
tuhosti	tuhost	k1gFnSc2	tuhost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
chceme	chtít	k5eAaImIp1nP	chtít
použít	použít	k5eAaPmF	použít
čerstvé	čerstvý	k2eAgNnSc1d1	čerstvé
hovězí	hovězí	k2eAgNnSc1d1	hovězí
maso	maso	k1gNnSc1	maso
a	a	k8xC	a
chceme	chtít	k5eAaImIp1nP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
vláčnější	vláčný	k2eAgInSc1d2	vláčnější
<g/>
,	,	kIx,	,
měkčí	měkký	k2eAgInSc1d2	měkčí
<g/>
,	,	kIx,	,
naložíme	naložit	k5eAaPmIp1nP	naložit
ho	on	k3xPp3gMnSc4	on
na	na	k7c6	na
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
dny	den	k1gInPc7	den
do	do	k7c2	do
nálevu	nálev	k1gInSc2	nálev
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
ho	on	k3xPp3gMnSc4	on
marinujeme	marinovat	k5eAaBmIp1nP	marinovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
morfologického	morfologický	k2eAgNnSc2d1	morfologické
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
maso	maso	k1gNnSc1	maso
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
tkáně	tkáň	k1gFnSc2	tkáň
svalové	svalový	k2eAgFnSc2d1	svalová
<g/>
,	,	kIx,	,
kostní	kostní	k2eAgFnSc2d1	kostní
<g/>
,	,	kIx,	,
vazivové	vazivový	k2eAgFnSc2d1	vazivová
<g/>
,	,	kIx,	,
chrupavkové	chrupavkový	k2eAgFnSc2d1	chrupavková
<g/>
,	,	kIx,	,
tukové	tukový	k2eAgFnSc2d1	tuková
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
sem	sem	k6eAd1	sem
řadíme	řadit	k5eAaImIp1nP	řadit
i	i	k9	i
tkáň	tkáň	k1gFnSc4	tkáň
nervovou	nervový	k2eAgFnSc4d1	nervová
a	a	k8xC	a
část	část	k1gFnSc4	část
soustavy	soustava	k1gFnSc2	soustava
oběhové	oběhový	k2eAgFnSc2d1	oběhová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Orientační	orientační	k2eAgNnSc1d1	orientační
složení	složení	k1gNnSc1	složení
masa	maso	k1gNnSc2	maso
===	===	k?	===
</s>
</p>
<p>
<s>
voda	voda	k1gFnSc1	voda
zhruba	zhruba	k6eAd1	zhruba
70	[number]	k4	70
%	%	kIx~	%
(	(	kIx(	(
<g/>
tepelnou	tepelný	k2eAgFnSc7d1	tepelná
úpravou	úprava	k1gFnSc7	úprava
se	se	k3xPyFc4	se
až	až	k9	až
30	[number]	k4	30
%	%	kIx~	%
ztratí	ztratit	k5eAaPmIp3nS	ztratit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
bílkoviny	bílkovina	k1gFnPc1	bílkovina
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
%	%	kIx~	%
</s>
</p>
<p>
<s>
tuky	tuk	k1gInPc1	tuk
(	(	kIx(	(
<g/>
vnitrobuněčné	vnitrobuněčný	k2eAgNnSc1d1	vnitrobuněčné
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
%	%	kIx~	%
(	(	kIx(	(
<g/>
vepřové	vepřový	k2eAgNnSc1d1	vepřové
maso	maso	k1gNnSc1	maso
libové	libový	k2eAgFnSc2d1	libová
20	[number]	k4	20
%	%	kIx~	%
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jinak	jinak	k6eAd1	jinak
až	až	k9	až
40	[number]	k4	40
%	%	kIx~	%
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
dusíkaté	dusíkatý	k2eAgFnSc2d1	dusíkatá
látky	látka	k1gFnSc2	látka
nebílkovinné	bílkovinný	k2eNgFnSc2d1	nebílkovinná
2	[number]	k4	2
%	%	kIx~	%
</s>
</p>
<p>
<s>
bezdusíkaté	bezdusíkatý	k2eAgFnPc4d1	bezdusíkatý
(	(	kIx(	(
<g/>
extraktivní	extraktivní	k2eAgFnPc4d1	extraktivní
<g/>
)	)	kIx)	)
látky	látka	k1gFnPc4	látka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sacharidy	sacharid	k1gInPc1	sacharid
<g/>
)	)	kIx)	)
1	[number]	k4	1
%	%	kIx~	%
(	(	kIx(	(
<g/>
hovězí	hovězí	k2eAgNnSc1d1	hovězí
maso	maso	k1gNnSc1	maso
i	i	k8xC	i
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
anorganické	anorganický	k2eAgFnPc1d1	anorganická
látky	látka	k1gFnPc1	látka
1	[number]	k4	1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vedlejší	vedlejší	k2eAgInPc1d1	vedlejší
produkty	produkt	k1gInPc1	produkt
masa	maso	k1gNnSc2	maso
===	===	k?	===
</s>
</p>
<p>
<s>
tuk	tuk	k1gInSc1	tuk
</s>
</p>
<p>
<s>
krev	krev	k1gFnSc1	krev
</s>
</p>
<p>
<s>
droby	droba	k1gFnPc1	droba
</s>
</p>
<p>
<s>
kůže	kůže	k1gFnSc1	kůže
</s>
</p>
<p>
<s>
kosti	kost	k1gFnPc1	kost
</s>
</p>
<p>
<s>
peří	peří	k1gNnSc1	peří
</s>
</p>
<p>
<s>
štětiny	štětina	k1gFnPc1	štětina
</s>
</p>
<p>
<s>
===	===	k?	===
Barva	barva	k1gFnSc1	barva
masa	maso	k1gNnSc2	maso
===	===	k?	===
</s>
</p>
<p>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
masa	maso	k1gNnSc2	maso
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
hemovými	hemův	k2eAgInPc7d1	hemův
barvivy	barvivo	k1gNnPc7	barvivo
myoglobinem	myoglobino	k1gNnSc7	myoglobino
(	(	kIx(	(
<g/>
svalové	svalový	k2eAgNnSc1d1	svalové
barvivo	barvivo	k1gNnSc1	barvivo
<g/>
)	)	kIx)	)
a	a	k8xC	a
hemoglobinem	hemoglobin	k1gInSc7	hemoglobin
(	(	kIx(	(
<g/>
krevní	krevní	k2eAgNnSc1d1	krevní
barvivo	barvivo	k1gNnSc1	barvivo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zdroje	zdroj	k1gInPc1	zdroj
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgInPc4d3	nejčastější
zdroje	zdroj	k1gInPc4	zdroj
masa	maso	k1gNnSc2	maso
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
jatečná	jatečný	k2eAgNnPc1d1	jatečné
zvířata	zvíře	k1gNnPc1	zvíře
(	(	kIx(	(
<g/>
prasata	prase	k1gNnPc1	prase
<g/>
,	,	kIx,	,
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
ovce	ovce	k1gFnSc1	ovce
<g/>
,	,	kIx,	,
koně	kůň	k1gMnPc1	kůň
<g/>
,	,	kIx,	,
králíci	králík	k1gMnPc1	králík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jatečná	jatečný	k2eAgFnSc1d1	jatečná
drůbež	drůbež	k1gFnSc1	drůbež
(	(	kIx(	(
<g/>
hrabavá	hrabavý	k2eAgFnSc1d1	hrabavá
i	i	k8xC	i
vodní	vodní	k2eAgFnSc1d1	vodní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lovná	lovný	k2eAgFnSc1d1	lovná
zvěř	zvěř	k1gFnSc1	zvěř
(	(	kIx(	(
<g/>
jelen	jelen	k1gMnSc1	jelen
<g/>
,	,	kIx,	,
srnec	srnec	k1gMnSc1	srnec
<g/>
,	,	kIx,	,
daněk	daněk	k1gMnSc1	daněk
<g/>
,	,	kIx,	,
muflon	muflon	k1gMnSc1	muflon
<g/>
,	,	kIx,	,
zajíc	zajíc	k1gMnSc1	zajíc
<g/>
,	,	kIx,	,
divočák	divočák	k1gMnSc1	divočák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
exotické	exotický	k2eAgInPc1d1	exotický
druhy	druh	k1gInPc1	druh
zvěře	zvěř	k1gFnSc2	zvěř
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
výskytu	výskyt	k1gInSc2	výskyt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
bezobratlí	bezobratlý	k2eAgMnPc1d1	bezobratlý
živočichové	živočich	k1gMnPc1	živočich
(	(	kIx(	(
<g/>
entomofagie	entomofagie	k1gFnSc1	entomofagie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jateční	jateční	k2eAgNnSc4d1	jateční
opracování	opracování	k1gNnSc4	opracování
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
prasat	prase	k1gNnPc2	prase
==	==	k?	==
</s>
</p>
<p>
<s>
Nejprve	nejprve	k6eAd1	nejprve
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
omráčení	omráčení	k1gNnSc3	omráčení
<g/>
.	.	kIx.	.
</s>
<s>
Provádí	provádět	k5eAaImIp3nS	provádět
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
mechanicky	mechanicky	k6eAd1	mechanicky
(	(	kIx(	(
<g/>
tupý	tupý	k2eAgInSc1d1	tupý
úder	úder	k1gInSc1	úder
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
průstřel	průstřel	k1gInSc4	průstřel
čelní	čelní	k2eAgFnSc1d1	čelní
kosti	kost	k1gFnPc1	kost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
elektricky	elektricky	k6eAd1	elektricky
(	(	kIx(	(
<g/>
napětím	napětí	k1gNnSc7	napětí
vyšším	vysoký	k2eAgNnSc7d2	vyšší
400	[number]	k4	400
V	V	kA	V
<g/>
)	)	kIx)	)
a	a	k8xC	a
chemicky	chemicky	k6eAd1	chemicky
(	(	kIx(	(
<g/>
pomocí	pomocí	k7c2	pomocí
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Omráčení	omráčení	k1gNnSc1	omráčení
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
ochrany	ochrana	k1gFnSc2	ochrana
pracovníků	pracovník	k1gMnPc2	pracovník
a	a	k8xC	a
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
usnadnění	usnadnění	k1gNnSc4	usnadnění
manipulace	manipulace	k1gFnSc2	manipulace
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
,	,	kIx,	,
vykrvení	vykrvení	k1gNnSc1	vykrvení
zvěře	zvěř	k1gFnSc2	zvěř
či	či	k8xC	či
zachování	zachování	k1gNnSc4	zachování
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
omráčení	omráčení	k1gNnSc6	omráčení
zvířat	zvíře	k1gNnPc2	zvíře
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
vlastnímu	vlastní	k2eAgNnSc3d1	vlastní
usmrcení	usmrcení	k1gNnSc3	usmrcení
–	–	k?	–
vykrvácení	vykrvácení	k1gNnSc2	vykrvácení
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
buď	buď	k8xC	buď
ve	v	k7c6	v
visu	vis	k1gInSc6	vis
či	či	k8xC	či
v	v	k7c6	v
leže	leže	k6eAd1	leže
<g/>
,	,	kIx,	,
dutým	dutý	k2eAgInSc7d1	dutý
nožem	nůž	k1gInSc7	nůž
nebo	nebo	k8xC	nebo
přeříznutím	přeříznutí	k1gNnSc7	přeříznutí
krční	krční	k2eAgFnSc2d1	krční
tepny	tepna	k1gFnSc2	tepna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
napaření	napaření	k1gNnSc6	napaření
prasete	prase	k1gNnSc2	prase
se	se	k3xPyFc4	se
odstraňují	odstraňovat	k5eAaImIp3nP	odstraňovat
štětiny	štětina	k1gFnPc1	štětina
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
skotu	skot	k1gInSc2	skot
se	se	k3xPyFc4	se
kůže	kůže	k1gFnSc1	kůže
stahuje	stahovat	k5eAaImIp3nS	stahovat
v	v	k7c6	v
celku	celek	k1gInSc6	celek
bez	bez	k7c2	bez
napaření	napaření	k1gNnSc2	napaření
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
skot	skot	k1gInSc1	skot
zbavován	zbavován	k2eAgInSc1d1	zbavován
vnitřností	vnitřnost	k1gFnSc7	vnitřnost
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
půlen	půlen	k2eAgMnSc1d1	půlen
a	a	k8xC	a
čtvrcen	čtvrcen	k2eAgMnSc1d1	čtvrcen
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
veterinární	veterinární	k2eAgFnSc1d1	veterinární
prohlídka	prohlídka	k1gFnSc1	prohlídka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
další	další	k2eAgNnSc4d1	další
zpracování	zpracování	k1gNnSc4	zpracování
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
u	u	k7c2	u
každého	každý	k3xTgInSc2	každý
kusu	kus	k1gInSc2	kus
určeného	určený	k2eAgMnSc2d1	určený
pro	pro	k7c4	pro
lidskou	lidský	k2eAgFnSc4d1	lidská
výživu	výživa	k1gFnSc4	výživa
<g/>
.	.	kIx.	.
</s>
<s>
Kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
se	se	k3xPyFc4	se
též	též	k9	též
výskyt	výskyt	k1gInSc1	výskyt
parazitů	parazit	k1gMnPc2	parazit
nebo	nebo	k8xC	nebo
mikrobů	mikrob	k1gInPc2	mikrob
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
maso	maso	k1gNnSc1	maso
bourá	bourat	k5eAaImIp3nS	bourat
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
bourání	bourání	k1gNnSc2	bourání
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
dělení	dělení	k1gNnSc2	dělení
na	na	k7c6	na
části	část	k1gFnSc6	část
i	i	k9	i
vykostění	vykostění	k1gNnSc1	vykostění
<g/>
,	,	kIx,	,
odstranění	odstranění	k1gNnSc1	odstranění
tuků	tuk	k1gInPc2	tuk
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
úpravy	úprava	k1gFnPc4	úprava
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
se	se	k3xPyFc4	se
3	[number]	k4	3
druhy	druh	k1gInPc7	druh
bourání	bourání	k1gNnSc1	bourání
<g/>
:	:	kIx,	:
pro	pro	k7c4	pro
výsek	výsek	k1gInSc4	výsek
(	(	kIx(	(
<g/>
maso	maso	k1gNnSc1	maso
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
anatomické	anatomický	k2eAgInPc4d1	anatomický
celky	celek	k1gInPc4	celek
–	–	k?	–
kýta	kýta	k1gFnSc1	kýta
<g/>
,	,	kIx,	,
bůček	bůček	k1gInSc1	bůček
<g/>
,	,	kIx,	,
krkovice	krkovice	k1gFnSc1	krkovice
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
maso	maso	k1gNnSc1	maso
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
pro	pro	k7c4	pro
obchodní	obchodní	k2eAgFnPc4d1	obchodní
sítě	síť	k1gFnPc4	síť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
masnou	masný	k2eAgFnSc4d1	Masná
výrobu	výroba	k1gFnSc4	výroba
(	(	kIx(	(
<g/>
maso	maso	k1gNnSc1	maso
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
podle	podle	k7c2	podle
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hovězí	hovězí	k1gNnSc4	hovězí
přední	přední	k2eAgNnSc4d1	přední
a	a	k8xC	a
zadní	zadní	k2eAgNnSc4d1	zadní
<g/>
,	,	kIx,	,
vepřové	vepřový	k2eAgNnSc4d1	vepřové
maso	maso	k1gNnSc4	maso
libové	libový	k2eAgFnSc2d1	libová
<g/>
,	,	kIx,	,
výrobní	výrobní	k2eAgFnSc2d1	výrobní
s	s	k7c7	s
kůží	kůže	k1gFnSc7	kůže
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
kůže	kůže	k1gFnSc2	kůže
<g/>
)	)	kIx)	)
a	a	k8xC	a
pro	pro	k7c4	pro
mrazírny	mrazírna	k1gFnPc4	mrazírna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Masná	masný	k2eAgFnSc1d1	Masná
výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
výrobu	výroba	k1gFnSc4	výroba
masných	masný	k2eAgInPc2d1	masný
výrobků	výrobek	k1gInPc2	výrobek
(	(	kIx(	(
<g/>
salámy	salám	k1gInPc1	salám
<g/>
,	,	kIx,	,
párky	párek	k1gInPc1	párek
<g/>
,	,	kIx,	,
klobásy	klobás	k1gInPc1	klobás
<g/>
,	,	kIx,	,
uzené	uzený	k2eAgNnSc1d1	uzené
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
fází	fáze	k1gFnSc7	fáze
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
mělnění	mělnění	k1gNnSc1	mělnění
a	a	k8xC	a
míchání	míchání	k1gNnSc1	míchání
<g/>
.	.	kIx.	.
tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
homogenní	homogenní	k2eAgFnSc1d1	homogenní
hmota	hmota	k1gFnSc1	hmota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
složek	složka	k1gFnPc2	složka
<g/>
:	:	kIx,	:
první	první	k4xOgFnSc1	první
je	být	k5eAaImIp3nS	být
homogenní	homogenní	k2eAgFnSc1d1	homogenní
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
spojka	spojka	k1gFnSc1	spojka
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
je	být	k5eAaImIp3nS	být
makroskopická	makroskopický	k2eAgFnSc1d1	makroskopická
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
vložka	vložka	k1gFnSc1	vložka
(	(	kIx(	(
<g/>
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
tuk	tuk	k1gInSc1	tuk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
pevné	pevný	k2eAgFnSc2d1	pevná
struktury	struktura	k1gFnSc2	struktura
salámu	salám	k1gInSc2	salám
(	(	kIx(	(
<g/>
síť	síť	k1gFnSc1	síť
příčných	příčný	k2eAgFnPc2d1	příčná
vazeb	vazba	k1gFnPc2	vazba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
pomocí	pomocí	k7c2	pomocí
řezaček	řezačka	k1gFnPc2	řezačka
nebo	nebo	k8xC	nebo
kutrů	kutr	k1gInPc2	kutr
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
přidání	přidání	k1gNnSc4	přidání
šupinek	šupinka	k1gFnPc2	šupinka
ledu	led	k1gInSc2	led
nebo	nebo	k8xC	nebo
mělnění	mělnění	k1gNnSc2	mělnění
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
hmota	hmota	k1gFnSc1	hmota
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Maso	maso	k1gNnSc1	maso
se	se	k3xPyFc4	se
naloží	naložit	k5eAaPmIp3nS	naložit
do	do	k7c2	do
láku	lák	k1gInSc2	lák
(	(	kIx(	(
<g/>
až	až	k9	až
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
sůl	sůl	k1gFnSc1	sůl
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
%	%	kIx~	%
roztok	roztok	k1gInSc1	roztok
NaCl	NaCl	k1gInSc1	NaCl
<g/>
)	)	kIx)	)
vstřikuje	vstřikovat	k5eAaImIp3nS	vstřikovat
do	do	k7c2	do
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Solení	solení	k1gNnSc1	solení
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
pro	pro	k7c4	pro
chuť	chuť	k1gFnSc4	chuť
a	a	k8xC	a
trvanlivost	trvanlivost	k1gFnSc4	trvanlivost
masných	masný	k2eAgInPc2d1	masný
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
NaCl	NaCl	k1gMnSc1	NaCl
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
"	"	kIx"	"
<g/>
rychlosůl	rychlosůl	k1gInSc4	rychlosůl
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dusitan	dusitan	k1gInSc1	dusitan
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sanitr	sanitr	k1gInSc1	sanitr
(	(	kIx(	(
<g/>
dusičnan	dusičnan	k1gInSc1	dusičnan
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polyfosfáty	polyfosfát	k1gInPc1	polyfosfát
<g/>
,	,	kIx,	,
koření	koření	k1gNnSc1	koření
<g/>
,	,	kIx,	,
kyselina	kyselina	k1gFnSc1	kyselina
askorbová	askorbový	k2eAgFnSc1d1	askorbová
nebo	nebo	k8xC	nebo
sacharidy	sacharid	k1gInPc7	sacharid
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
solení	solení	k1gNnSc6	solení
přichází	přicházet	k5eAaImIp3nS	přicházet
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
narážení	narážení	k1gNnSc2	narážení
a	a	k8xC	a
tvarování	tvarování	k1gNnSc2	tvarování
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
plnění	plnění	k1gNnSc4	plnění
hmoty	hmota	k1gFnSc2	hmota
do	do	k7c2	do
obalů	obal	k1gInPc2	obal
(	(	kIx(	(
<g/>
střeva	střevo	k1gNnSc2	střevo
–	–	k?	–
přírodní	přírodní	k2eAgFnSc1d1	přírodní
<g/>
,	,	kIx,	,
umělá	umělý	k2eAgFnSc1d1	umělá
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
narážeček	narážečka	k1gFnPc2	narážečka
<g/>
.	.	kIx.	.
poslední	poslední	k2eAgFnSc7d1	poslední
fází	fáze	k1gFnSc7	fáze
je	být	k5eAaImIp3nS	být
uzení	uzení	k1gNnSc1	uzení
<g/>
.	.	kIx.	.
</s>
<s>
Uzení	uzení	k1gNnSc1	uzení
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
trvanlivost	trvanlivost	k1gFnSc1	trvanlivost
a	a	k8xC	a
dochucuje	dochucovat	k5eAaImIp3nS	dochucovat
masné	masný	k2eAgInPc4d1	masný
výrobky	výrobek	k1gInPc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Udí	udit	k5eAaImIp3nS	udit
se	se	k3xPyFc4	se
kouřem	kouřmo	k1gNnPc2	kouřmo
z	z	k7c2	z
tvrdého	tvrdé	k1gNnSc2	tvrdé
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
bukového	bukový	k2eAgNnSc2d1	bukové
<g/>
)	)	kIx)	)
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Měkké	měkký	k2eAgNnSc1d1	měkké
dřevo	dřevo	k1gNnSc1	dřevo
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
uzení	uzení	k1gNnSc3	uzení
nevhodné	vhodný	k2eNgFnPc1d1	nevhodná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
pryskyřic	pryskyřice	k1gFnPc2	pryskyřice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
konečné	konečný	k2eAgFnSc6d1	konečná
fázi	fáze	k1gFnSc6	fáze
zdraví	zdraví	k1gNnSc4	zdraví
škodlivé	škodlivý	k2eAgNnSc4d1	škodlivé
<g/>
.	.	kIx.	.
</s>
<s>
Drobné	drobný	k2eAgInPc4d1	drobný
masné	masný	k2eAgInPc4d1	masný
výrobky	výrobek	k1gInPc4	výrobek
(	(	kIx(	(
<g/>
měkké	měkký	k2eAgInPc4d1	měkký
salámy	salám	k1gInPc4	salám
<g/>
,	,	kIx,	,
klobásy	klobás	k1gInPc4	klobás
<g/>
,	,	kIx,	,
párky	párek	k1gInPc4	párek
<g/>
,	,	kIx,	,
uzená	uzený	k2eAgNnPc4d1	uzené
masa	maso	k1gNnPc4	maso
<g/>
,	,	kIx,	,
uzené	uzený	k2eAgFnPc4d1	uzená
boky	boka	k1gFnPc4	boka
a	a	k8xC	a
slaniny	slanina	k1gFnPc4	slanina
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
udí	udit	k5eAaImIp3nS	udit
horkým	horký	k2eAgInSc7d1	horký
kouřem	kouř	k1gInSc7	kouř
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
°	°	k?	°
<g/>
C.	C.	kA	C.
Trvanlivé	trvanlivý	k2eAgInPc1d1	trvanlivý
masné	masný	k2eAgInPc1d1	masný
výrobky	výrobek	k1gInPc1	výrobek
–	–	k?	–
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
salámy	salám	k1gInPc1	salám
(	(	kIx(	(
<g/>
např.	např.	kA	např.
lovecký	lovecký	k2eAgInSc1d1	lovecký
salám	salám	k1gInSc1	salám
<g/>
,	,	kIx,	,
Herkules	Herkules	k1gMnSc1	Herkules
<g/>
,	,	kIx,	,
Paprikáš	paprikáš	k1gInSc1	paprikáš
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
udí	udit	k5eAaImIp3nS	udit
teplým	teplý	k2eAgInSc7d1	teplý
kouřem	kouř	k1gInSc7	kouř
(	(	kIx(	(
<g/>
60	[number]	k4	60
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
a	a	k8xC	a
suší	sušit	k5eAaImIp3nS	sušit
se	se	k3xPyFc4	se
kouřem	kouř	k1gInSc7	kouř
studeným	studený	k2eAgInSc7d1	studený
(	(	kIx(	(
<g/>
18	[number]	k4	18
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
uzení	uzení	k1gNnSc6	uzení
teplým	teplý	k2eAgInSc7d1	teplý
kouřem	kouř	k1gInSc7	kouř
a	a	k8xC	a
následném	následný	k2eAgNnSc6d1	následné
sušení	sušení	k1gNnSc6	sušení
studeným	studený	k2eAgInSc7d1	studený
kouřem	kouř	k1gInSc7	kouř
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
dodržovat	dodržovat	k5eAaImF	dodržovat
stejnou	stejný	k2eAgFnSc4d1	stejná
teplotu	teplota	k1gFnSc4	teplota
a	a	k8xC	a
daný	daný	k2eAgInSc4d1	daný
technologický	technologický	k2eAgInSc4d1	technologický
postup	postup	k1gInSc4	postup
(	(	kIx(	(
<g/>
nesmí	smět	k5eNaImIp3nS	smět
kolísat	kolísat	k5eAaImF	kolísat
teplota	teplota	k1gFnSc1	teplota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nS	by
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
výskytu	výskyt	k1gInSc3	výskyt
botulotoxinu	botulotoxin	k1gInSc2	botulotoxin
(	(	kIx(	(
<g/>
klobásového	klobásový	k2eAgInSc2d1	klobásový
jedu	jed	k1gInSc2	jed
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
pro	pro	k7c4	pro
lidský	lidský	k2eAgInSc4d1	lidský
organizmus	organizmus	k1gInSc4	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
uzení	uzení	k1gNnSc2	uzení
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
specializované	specializovaný	k2eAgFnPc1d1	specializovaná
udírny	udírna	k1gFnPc1	udírna
a	a	k8xC	a
sušící	sušící	k2eAgFnPc1d1	sušící
komory	komora	k1gFnPc1	komora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
masných	masný	k2eAgInPc2d1	masný
výrobků	výrobek	k1gInPc2	výrobek
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
zpracování	zpracování	k1gNnSc2	zpracování
rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
tyto	tento	k3xDgInPc4	tento
druhy	druh	k1gInPc4	druh
<g/>
:	:	kIx,	:
drobné	drobný	k2eAgInPc4d1	drobný
masné	masný	k2eAgInPc4d1	masný
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
měkké	měkký	k2eAgInPc4d1	měkký
salámy	salám	k1gInPc4	salám
<g/>
,	,	kIx,	,
trvanlivé	trvanlivý	k2eAgInPc4d1	trvanlivý
masné	masný	k2eAgInPc4d1	masný
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgInPc4d1	speciální
masné	masný	k2eAgInPc4d1	masný
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
vařené	vařený	k2eAgInPc4d1	vařený
masné	masný	k2eAgInPc4d1	masný
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
pečené	pečený	k2eAgInPc4d1	pečený
masné	masný	k2eAgInPc4d1	masný
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
uzená	uzený	k2eAgNnPc4d1	uzené
masa	maso	k1gNnPc4	maso
<g/>
,	,	kIx,	,
fermentované	fermentovaný	k2eAgInPc4d1	fermentovaný
salámy	salám	k1gInPc4	salám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
drobné	drobný	k2eAgInPc1d1	drobný
masné	masný	k2eAgInPc1d1	masný
výrobky	výrobek	k1gInPc1	výrobek
</s>
</p>
<p>
<s>
hmotnost	hmotnost	k1gFnSc1	hmotnost
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
g	g	kA	g
</s>
</p>
<p>
<s>
narážené	narážený	k2eAgMnPc4d1	narážený
do	do	k7c2	do
střev	střevo	k1gNnPc2	střevo
<g/>
,	,	kIx,	,
oddělené	oddělený	k2eAgInPc4d1	oddělený
</s>
</p>
<p>
<s>
nakládání	nakládání	k1gNnSc1	nakládání
pomocí	pomocí	k7c2	pomocí
dusitanové	dusitanový	k2eAgFnSc2d1	dusitanová
směsi	směs	k1gFnSc2	směs
</s>
</p>
<p>
<s>
tepelné	tepelný	k2eAgFnPc1d1	tepelná
opracované	opracovaný	k2eAgFnPc1d1	opracovaná
<g/>
,	,	kIx,	,
uzené	uzený	k2eAgFnPc1d1	uzená
</s>
</p>
<p>
<s>
s	s	k7c7	s
vložkou	vložka	k1gFnSc7	vložka
(	(	kIx(	(
<g/>
špekáčky	špekáček	k1gInPc4	špekáček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
vložky	vložka	k1gFnSc2	vložka
(	(	kIx(	(
<g/>
párky	párek	k1gInPc1	párek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
obsahem	obsah	k1gInSc7	obsah
spojkyměkké	spojkyměkký	k2eAgInPc4d1	spojkyměkký
salámy	salám	k1gInPc4	salám
</s>
</p>
<p>
<s>
stejná	stejný	k2eAgFnSc1d1	stejná
výroba	výroba	k1gFnSc1	výroba
jako	jako	k8xC	jako
u	u	k7c2	u
drobných	drobný	k2eAgInPc2d1	drobný
masných	masný	k2eAgInPc2d1	masný
výrobků	výrobek	k1gInPc2	výrobek
</s>
</p>
<p>
<s>
jemně	jemně	k6eAd1	jemně
mělněné	mělněný	k2eAgFnPc1d1	mělněný
(	(	kIx(	(
<g/>
junior	junior	k1gMnSc1	junior
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jemně	jemně	k6eAd1	jemně
mělněné	mělněný	k2eAgFnSc2d1	mělněný
s	s	k7c7	s
vložkou	vložka	k1gFnSc7	vložka
tukové	tukový	k2eAgFnSc2d1	tuková
tkáně	tkáň	k1gFnSc2	tkáň
(	(	kIx(	(
<g/>
gothaj	gothaj	k1gInSc1	gothaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jemně	jemně	k6eAd1	jemně
mělněné	mělněný	k2eAgFnSc2d1	mělněný
s	s	k7c7	s
vložkou	vložka	k1gFnSc7	vložka
libového	libový	k2eAgNnSc2d1	libové
masa	maso	k1gNnSc2	maso
(	(	kIx(	(
<g/>
šunkový	šunkový	k2eAgInSc1d1	šunkový
s.	s.	k?	s.
<g/>
)	)	kIx)	)
<g/>
trvanlivé	trvanlivý	k2eAgInPc1d1	trvanlivý
masné	masný	k2eAgInPc1d1	masný
výrobky	výrobek	k1gInPc1	výrobek
</s>
</p>
<p>
<s>
hrubě	hrubě	k6eAd1	hrubě
zrněné	zrněný	k2eAgInPc1d1	zrněný
s	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
dusitanové	dusitanový	k2eAgFnSc2d1	dusitanová
směsi	směs	k1gFnSc2	směs
</s>
</p>
<p>
<s>
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
údržnost	údržnost	k1gFnSc1	údržnost
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
</s>
</p>
<p>
<s>
uzené	uzené	k1gNnSc1	uzené
horkým	horký	k2eAgInSc7d1	horký
kouřem	kouř	k1gInSc7	kouř
<g/>
,	,	kIx,	,
sušenéspeciální	sušenéspeciální	k2eAgInPc4d1	sušenéspeciální
masné	masný	k2eAgInPc4d1	masný
výrobky	výrobek	k1gInPc4	výrobek
</s>
</p>
<p>
<s>
různé	různý	k2eAgFnPc1d1	různá
technologie	technologie	k1gFnPc1	technologie
výroby	výroba	k1gFnSc2	výroba
</s>
</p>
<p>
<s>
např.	např.	kA	např.
debrecínská	debrecínský	k2eAgFnSc1d1	Debrecínská
pečeně	pečeně	k1gFnSc1	pečeně
<g/>
,	,	kIx,	,
anglická	anglický	k2eAgFnSc1d1	anglická
slaninavařené	slaninavařený	k2eAgInPc4d1	slaninavařený
masné	masný	k2eAgInPc4d1	masný
výrobky	výrobek	k1gInPc4	výrobek
</s>
</p>
<p>
<s>
určené	určený	k2eAgInPc1d1	určený
k	k	k7c3	k
rychlé	rychlý	k2eAgFnSc3d1	rychlá
spotřebě	spotřeba	k1gFnSc3	spotřeba
</s>
</p>
<p>
<s>
výroba	výroba	k1gFnSc1	výroba
z	z	k7c2	z
drobů	drob	k1gInPc2	drob
<g/>
,	,	kIx,	,
vepřové	vepřový	k2eAgFnSc2d1	vepřová
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
hlavy	hlava	k1gFnSc2	hlava
</s>
</p>
<p>
<s>
dováření	dováření	k1gNnSc1	dováření
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
či	či	k8xC	či
páře	pára	k1gFnSc6	pára
</s>
</p>
<p>
<s>
jitrnice	jitrnice	k1gFnPc1	jitrnice
<g/>
,	,	kIx,	,
tlačenkapečené	tlačenkapečený	k2eAgInPc1d1	tlačenkapečený
masné	masný	k2eAgInPc1d1	masný
výrobky	výrobek	k1gInPc1	výrobek
</s>
</p>
<p>
<s>
mělněné	mělněný	k2eAgInPc1d1	mělněný
masné	masný	k2eAgInPc1d1	masný
výrobky	výrobek	k1gInPc1	výrobek
s	s	k7c7	s
vložkou	vložka	k1gFnSc7	vložka
</s>
</p>
<p>
<s>
výroba	výroba	k1gFnSc1	výroba
bez	bez	k7c2	bez
obalů	obal	k1gInPc2	obal
ve	v	k7c6	v
formách	forma	k1gFnPc6	forma
</s>
</p>
<p>
<s>
dusitanová	dusitanový	k2eAgFnSc1d1	dusitanová
směs	směs	k1gFnSc1	směs
</s>
</p>
<p>
<s>
dovářejí	dovářet	k5eAaImIp3nP	dovářet
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
sekaná	sekaný	k2eAgFnSc1d1	sekaná
<g/>
)	)	kIx)	)
<g/>
uzená	uzený	k2eAgFnSc1d1	uzená
masa	masa	k1gFnSc1	masa
</s>
</p>
<p>
<s>
výrobky	výrobek	k1gInPc1	výrobek
z	z	k7c2	z
kusů	kus	k1gInPc2	kus
masa	maso	k1gNnSc2	maso
</s>
</p>
<p>
<s>
naložené	naložený	k2eAgNnSc4d1	naložené
do	do	k7c2	do
dusitanové	dusitanový	k2eAgFnSc2d1	dusitanová
směsi	směs	k1gFnSc2	směs
</s>
</p>
<p>
<s>
udí	udit	k5eAaImIp3nS	udit
se	se	k3xPyFc4	se
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
hodin	hodina	k1gFnPc2	hodina
horkým	horký	k2eAgInSc7d1	horký
kouřem	kouř	k1gInSc7	kouř
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
<s>
udí	udit	k5eAaImIp3nS	udit
se	se	k3xPyFc4	se
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
hodiny	hodina	k1gFnSc2	hodina
teplým	teplý	k2eAgInSc7d1	teplý
kouřem	kouř	k1gInSc7	kouř
s	s	k7c7	s
následným	následný	k2eAgNnSc7d1	následné
dovařením	dovaření	k1gNnSc7	dovaření
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nebo	nebo	k8xC	nebo
páře	pára	k1gFnSc6	pára
(	(	kIx(	(
<g/>
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
výrobku	výrobek	k1gInSc2	výrobek
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
70	[number]	k4	70
°	°	k?	°
<g/>
C	C	kA	C
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
<g/>
fermentované	fermentovaný	k2eAgInPc1d1	fermentovaný
salámy	salám	k1gInPc1	salám
</s>
</p>
<p>
<s>
tepelně	tepelně	k6eAd1	tepelně
neopracované	opracovaný	k2eNgInPc4d1	neopracovaný
výrobky	výrobek	k1gInPc4	výrobek
</s>
</p>
<p>
<s>
konzervace	konzervace	k1gFnSc1	konzervace
působením	působení	k1gNnSc7	působení
laktobacilů	laktobacil	k1gMnPc2	laktobacil
a	a	k8xC	a
streptokoků	streptokok	k1gMnPc2	streptokok
</s>
</p>
<p>
<s>
zvýšení	zvýšení	k1gNnSc4	zvýšení
údržnosti	údržnost	k1gFnSc2	údržnost
snížením	snížení	k1gNnSc7	snížení
pH	ph	k0wR	ph
</s>
</p>
<p>
<s>
snížení	snížení	k1gNnSc1	snížení
aktivity	aktivita	k1gFnSc2	aktivita
vody	voda	k1gFnSc2	voda
</s>
</p>
<p>
<s>
paštiky	paštika	k1gFnPc1	paštika
<g/>
,	,	kIx,	,
lovecký	lovecký	k2eAgInSc1d1	lovecký
salám	salám	k1gInSc1	salám
</s>
</p>
<p>
<s>
==	==	k?	==
Konzumace	konzumace	k1gFnPc1	konzumace
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
zdraví	zdraví	k1gNnSc2	zdraví
==	==	k?	==
</s>
</p>
<p>
<s>
Statisticky	statisticky	k6eAd1	statisticky
významný	významný	k2eAgInSc1d1	významný
vztah	vztah	k1gInSc1	vztah
existuje	existovat	k5eAaImIp3nS	existovat
mezi	mezi	k7c7	mezi
množstvím	množství	k1gNnSc7	množství
snědeného	snědený	k2eAgNnSc2d1	snědené
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
rizikem	riziko	k1gNnSc7	riziko
vzniku	vznik	k1gInSc2	vznik
rakoviny	rakovina	k1gFnSc2	rakovina
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
každých	každý	k3xTgInPc2	každý
100	[number]	k4	100
gramů	gram	k1gInPc2	gram
denně	denně	k6eAd1	denně
zkonzumovaného	zkonzumovaný	k2eAgNnSc2d1	zkonzumované
červeného	červený	k2eAgNnSc2d1	červené
masa	maso	k1gNnSc2	maso
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc4	riziko
onemocnění	onemocnění	k1gNnSc2	onemocnění
rakovinou	rakovina	k1gFnSc7	rakovina
o	o	k7c4	o
17	[number]	k4	17
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Takzvané	takzvaný	k2eAgNnSc1d1	takzvané
bílé	bílý	k2eAgNnSc1d1	bílé
a	a	k8xC	a
červené	červený	k2eAgNnSc1d1	červené
maso	maso	k1gNnSc1	maso
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
cholesterol	cholesterol	k1gInSc4	cholesterol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kontroverze	kontroverze	k1gFnSc2	kontroverze
==	==	k?	==
</s>
</p>
<p>
<s>
Maso	maso	k1gNnSc1	maso
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
nejkontroverznější	kontroverzní	k2eAgFnSc7d3	nejkontroverznější
potravinou	potravina	k1gFnSc7	potravina
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vhodnost	vhodnost	k1gFnSc4	vhodnost
masa	maso	k1gNnSc2	maso
pro	pro	k7c4	pro
lidskou	lidský	k2eAgFnSc4d1	lidská
výživu	výživa	k1gFnSc4	výživa
není	být	k5eNaImIp3nS	být
jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
názor	názor	k1gInSc1	názor
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nejedí	jíst	k5eNaImIp3nP	jíst
buď	buď	k8xC	buď
veškeré	veškerý	k3xTgNnSc4	veškerý
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
některý	některý	k3yIgInSc4	některý
druh	druh	k1gInSc4	druh
ať	ať	k8xS	ať
už	už	k6eAd1	už
z	z	k7c2	z
etického	etický	k2eAgNnSc2d1	etické
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
<g/>
,	,	kIx,	,
náboženského	náboženský	k2eAgNnSc2d1	náboženské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
nebo	nebo	k8xC	nebo
dalších	další	k2eAgInPc2d1	další
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nejí	jíst	k5eNaImIp3nS	jíst
žádný	žádný	k3yNgInSc4	žádný
druh	druh	k1gInSc4	druh
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
vegetarián	vegetarián	k1gMnSc1	vegetarián
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
druhy	druh	k1gInPc7	druh
částečného	částečný	k2eAgNnSc2d1	částečné
vegetariánství	vegetariánství	k1gNnSc2	vegetariánství
jako	jako	k8xC	jako
pesco-pollo	pescoollo	k1gNnSc4	pesco-pollo
vegetariánství	vegetariánství	k1gNnSc2	vegetariánství
nebo	nebo	k8xC	nebo
pescetariánství	pescetariánství	k1gNnSc2	pescetariánství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
konzumaci	konzumace	k1gFnSc4	konzumace
rybího	rybí	k2eAgNnSc2d1	rybí
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
mořských	mořský	k2eAgInPc2d1	mořský
plodů	plod	k1gInPc2	plod
a	a	k8xC	a
kuřecího	kuřecí	k2eAgNnSc2d1	kuřecí
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpracované	zpracovaný	k2eAgNnSc4d1	zpracované
maso	maso	k1gNnSc4	maso
považuje	považovat	k5eAaImIp3nS	považovat
IARC	IARC	kA	IARC
za	za	k7c4	za
karcinogen	karcinogen	k1gInSc4	karcinogen
<g/>
.	.	kIx.	.
<g/>
Některá	některý	k3yIgFnSc1	některý
červená	červený	k2eAgFnSc1d1	červená
masa	masa	k1gFnSc1	masa
od	od	k7c2	od
přírody	příroda	k1gFnSc2	příroda
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
karcinogenní	karcinogenní	k2eAgInSc1d1	karcinogenní
sacharid	sacharid	k1gInSc1	sacharid
Neu	Neu	k1gFnSc2	Neu
<g/>
5	[number]	k4	5
<g/>
Gc	Gc	k1gMnSc1	Gc
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
imunitní	imunitní	k2eAgFnPc4d1	imunitní
reakce	reakce	k1gFnPc4	reakce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
při	při	k7c6	při
nadměrné	nadměrný	k2eAgFnSc6d1	nadměrná
konzumaci	konzumace	k1gFnSc6	konzumace
těchto	tento	k3xDgFnPc2	tento
mas	masa	k1gFnPc2	masa
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
rakovině	rakovina	k1gFnSc3	rakovina
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
<g/>
.	.	kIx.	.
<g/>
Maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
rybího	rybí	k2eAgNnSc2d1	rybí
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
polutanty	polutant	k1gInPc4	polutant
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
náboženství	náboženství	k1gNnPc4	náboženství
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
zakazují	zakazovat	k5eAaImIp3nP	zakazovat
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
masa	maso	k1gNnSc2	maso
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
judaismus	judaismus	k1gInSc1	judaismus
–	–	k?	–
podle	podle	k7c2	podle
kašrutu	kašrut	k1gInSc2	kašrut
nesmí	smět	k5eNaImIp3nP	smět
židé	žid	k1gMnPc1	žid
konzumovat	konzumovat	k5eAaBmF	konzumovat
žádné	žádný	k3yNgNnSc1	žádný
maso	maso	k1gNnSc1	maso
savců	savec	k1gMnPc2	savec
kromě	kromě	k7c2	kromě
sudokopytné	sudokopytný	k2eAgFnSc2d1	sudokopytný
přežvýkavé	přežvýkavý	k2eAgFnSc2d1	přežvýkavý
zvěře	zvěř	k1gFnSc2	zvěř
(	(	kIx(	(
<g/>
krávy	kráva	k1gFnPc1	kráva
<g/>
,	,	kIx,	,
ovce	ovce	k1gFnPc1	ovce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ptáků	pták	k1gMnPc2	pták
kromě	kromě	k7c2	kromě
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
holubů	holub	k1gMnPc2	holub
(	(	kIx(	(
<g/>
zákaz	zákaz	k1gInSc1	zákaz
konzumace	konzumace	k1gFnSc2	konzumace
masa	maso	k1gNnSc2	maso
dravých	dravý	k2eAgMnPc2d1	dravý
ptáků	pták	k1gMnPc2	pták
<g/>
)	)	kIx)	)
a	a	k8xC	a
bezšupinatých	bezšupinatý	k2eAgFnPc2d1	bezšupinatý
a	a	k8xC	a
bezploutvých	bezploutvý	k2eAgFnPc2d1	bezploutvý
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
mořských	mořský	k2eAgInPc2d1	mořský
plodů	plod	k1gInPc2	plod
a	a	k8xC	a
žádnou	žádný	k3yNgFnSc4	žádný
krev	krev	k1gFnSc4	krev
</s>
</p>
<p>
<s>
islám	islám	k1gInSc1	islám
–	–	k?	–
zákaz	zákaz	k1gInSc4	zákaz
konzumace	konzumace	k1gFnSc2	konzumace
vepřového	vepřový	k2eAgNnSc2d1	vepřové
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
masa	maso	k1gNnSc2	maso
nešupinatých	šupinatý	k2eNgFnPc2d1	šupinatý
ryb	ryba	k1gFnPc2	ryba
(	(	kIx(	(
<g/>
halal	halal	k1gInSc1	halal
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
hinduismus	hinduismus	k1gInSc1	hinduismus
a	a	k8xC	a
buddhismus	buddhismus	k1gInSc1	buddhismus
preferují	preferovat	k5eAaImIp3nP	preferovat
vegetariánství	vegetariánství	k1gNnPc1	vegetariánství
</s>
</p>
<p>
<s>
==	==	k?	==
Náhražka	náhražka	k1gFnSc1	náhražka
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
laboratorně	laboratorně	k6eAd1	laboratorně
vypěstované	vypěstovaný	k2eAgNnSc4d1	vypěstované
maso	maso	k1gNnSc4	maso
==	==	k?	==
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
vegetariánskému	vegetariánský	k2eAgInSc3d1	vegetariánský
způsobu	způsob	k1gInSc3	způsob
stravování	stravování	k1gNnSc2	stravování
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
náhražek	náhražka	k1gFnPc2	náhražka
masa	maso	k1gNnSc2	maso
(	(	kIx(	(
<g/>
sojové	sojový	k2eAgNnSc1d1	sojové
maso	maso	k1gNnSc1	maso
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
vypadat	vypadat	k5eAaPmF	vypadat
a	a	k8xC	a
chutnat	chutnat	k5eAaImF	chutnat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
skutečné	skutečný	k2eAgNnSc4d1	skutečné
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Probíhají	probíhat	k5eAaImIp3nP	probíhat
také	také	k9	také
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
masa	maso	k1gNnSc2	maso
ze	z	k7c2	z
živočišné	živočišný	k2eAgFnSc2d1	živočišná
tkáně	tkáň	k1gFnSc2	tkáň
v	v	k7c6	v
laboratorních	laboratorní	k2eAgFnPc6d1	laboratorní
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Domestikace	domestikace	k1gFnSc1	domestikace
</s>
</p>
<p>
<s>
Chrupavka	chrupavka	k1gFnSc1	chrupavka
</s>
</p>
<p>
<s>
Želatina	želatina	k1gFnSc1	želatina
</s>
</p>
<p>
<s>
Křehčené	Křehčený	k2eAgNnSc1d1	Křehčené
maso	maso	k1gNnSc1	maso
</s>
</p>
<p>
<s>
Dopad	dopad	k1gInSc1	dopad
produkce	produkce	k1gFnSc2	produkce
masa	maso	k1gNnSc2	maso
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
</s>
</p>
<p>
<s>
Kultivované	kultivovaný	k2eAgNnSc4d1	kultivované
(	(	kIx(	(
<g/>
umělé	umělý	k2eAgNnSc4d1	umělé
<g/>
)	)	kIx)	)
maso	maso	k1gNnSc4	maso
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
maso	maso	k1gNnSc4	maso
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
maso	maso	k1gNnSc4	maso
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
MASO	maso	k1gNnSc1	maso
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
vše	všechen	k3xTgNnSc1	všechen
o	o	k7c6	o
výrobě	výroba	k1gFnSc6	výroba
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc4	zpracování
a	a	k8xC	a
distribuci	distribuce	k1gFnSc4	distribuce
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
masných	masný	k2eAgInPc2d1	masný
produktů	produkt	k1gInPc2	produkt
</s>
</p>
