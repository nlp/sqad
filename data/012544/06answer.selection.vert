<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
masa	maso	k1gNnSc2	maso
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
hemovými	hemův	k2eAgInPc7d1	hemův
barvivy	barvivo	k1gNnPc7	barvivo
myoglobinem	myoglobino	k1gNnSc7	myoglobino
(	(	kIx(	(
<g/>
svalové	svalový	k2eAgNnSc1d1	svalové
barvivo	barvivo	k1gNnSc1	barvivo
<g/>
)	)	kIx)	)
a	a	k8xC	a
hemoglobinem	hemoglobin	k1gInSc7	hemoglobin
(	(	kIx(	(
<g/>
krevní	krevní	k2eAgNnSc1d1	krevní
barvivo	barvivo	k1gNnSc1	barvivo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
