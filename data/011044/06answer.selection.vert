<s>
Gravlax	Gravlax	k1gInSc1	Gravlax
je	být	k5eAaImIp3nS	být
pochoutka	pochoutka	k1gFnSc1	pochoutka
skandinávské	skandinávský	k2eAgFnSc2d1	skandinávská
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
ze	z	k7c2	z
syrového	syrový	k2eAgNnSc2d1	syrové
lososího	lososí	k2eAgNnSc2d1	lososí
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
