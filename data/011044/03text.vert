<p>
<s>
Gravlax	Gravlax	k1gInSc1	Gravlax
je	být	k5eAaImIp3nS	být
pochoutka	pochoutka	k1gFnSc1	pochoutka
skandinávské	skandinávský	k2eAgFnSc2d1	skandinávská
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
ze	z	k7c2	z
syrového	syrový	k2eAgNnSc2d1	syrové
lososího	lososí	k2eAgNnSc2d1	lososí
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Vykostěné	vykostěný	k2eAgInPc1d1	vykostěný
filety	filet	k1gInPc1	filet
(	(	kIx(	(
<g/>
kůže	kůže	k1gFnSc1	kůže
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
ponechává	ponechávat	k5eAaImIp3nS	ponechávat
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
obalí	obalit	k5eAaPmIp3nP	obalit
v	v	k7c6	v
hrubozrnné	hrubozrnný	k2eAgFnSc6d1	hrubozrnná
mořské	mořský	k2eAgFnSc6d1	mořská
soli	sůl	k1gFnSc6	sůl
<g/>
,	,	kIx,	,
pískovém	pískový	k2eAgInSc6d1	pískový
cukru	cukr	k1gInSc6	cukr
a	a	k8xC	a
čerstvém	čerstvý	k2eAgInSc6d1	čerstvý
kopru	kopr	k1gInSc6	kopr
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
i	i	k9	i
alkohol	alkohol	k1gInSc1	alkohol
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vodka	vodka	k1gFnSc1	vodka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ryba	ryba	k1gFnSc1	ryba
zkřehla	zkřehnout	k5eAaPmAgFnS	zkřehnout
<g/>
)	)	kIx)	)
a	a	k8xC	a
nechají	nechat	k5eAaPmIp3nP	nechat
se	se	k3xPyFc4	se
nejméně	málo	k6eAd3	málo
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
marinovat	marinovat	k5eAaBmF	marinovat
v	v	k7c6	v
chladu	chlad	k1gInSc6	chlad
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
maso	maso	k1gNnSc1	maso
očistí	očistit	k5eAaPmIp3nS	očistit
a	a	k8xC	a
krájí	krájet	k5eAaImIp3nS	krájet
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
tenké	tenký	k2eAgInPc4d1	tenký
plátky	plátek	k1gInPc4	plátek
<g/>
.	.	kIx.	.
</s>
<s>
Podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
s	s	k7c7	s
tmavým	tmavý	k2eAgInSc7d1	tmavý
chlebem	chléb	k1gInSc7	chléb
a	a	k8xC	a
omáčkou	omáčka	k1gFnSc7	omáčka
hovmästarså	hovmästarså	k?	hovmästarså
složenou	složený	k2eAgFnSc4d1	složená
z	z	k7c2	z
hořčice	hořčice	k1gFnSc2	hořčice
<g/>
,	,	kIx,	,
oleje	olej	k1gInSc2	olej
a	a	k8xC	a
sekaného	sekaný	k2eAgInSc2d1	sekaný
kopru	kopr	k1gInSc2	kopr
jako	jako	k8xS	jako
předkrm	předkrm	k1gInSc4	předkrm
nebo	nebo	k8xC	nebo
součást	součást	k1gFnSc4	součást
švédských	švédský	k2eAgInPc2d1	švédský
stolů	stol	k1gInPc2	stol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
švédského	švédský	k2eAgInSc2d1	švédský
výrazu	výraz	k1gInSc2	výraz
gravad	gravad	k1gInSc1	gravad
lax	lax	k?	lax
(	(	kIx(	(
<g/>
pohřbený	pohřbený	k2eAgMnSc1d1	pohřbený
losos	losos	k1gMnSc1	losos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
porce	porce	k1gFnSc1	porce
masa	maso	k1gNnSc2	maso
pro	pro	k7c4	pro
usnadnění	usnadnění	k1gNnSc4	usnadnění
fermentace	fermentace	k1gFnSc2	fermentace
zahrabávaly	zahrabávat	k5eAaImAgInP	zahrabávat
do	do	k7c2	do
písku	písek	k1gInSc2	písek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
gravlax	gravlax	k1gInSc1	gravlax
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.dlc.fi/~marianna/gourmet/5_2.htm	[url]	k6eAd1	http://www.dlc.fi/~marianna/gourmet/5_2.htm
</s>
</p>
<p>
<s>
http://www.cookingforengineers.com/recipe/132/Gravlax	[url]	k1gInSc1	http://www.cookingforengineers.com/recipe/132/Gravlax
</s>
</p>
<p>
<s>
http://sweden.se/culture/gravad-lax/	[url]	k?	http://sweden.se/culture/gravad-lax/
</s>
</p>
