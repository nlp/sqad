<s>
Spejbl	Spejbl	k1gMnSc1	Spejbl
a	a	k8xC	a
Hurvínek	Hurvínek	k1gMnSc1	Hurvínek
je	být	k5eAaImIp3nS	být
legendární	legendární	k2eAgFnSc1d1	legendární
dvojice	dvojice	k1gFnSc1	dvojice
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
loutek	loutka	k1gFnPc2	loutka
dle	dle	k7c2	dle
návrhu	návrh	k1gInSc2	návrh
českého	český	k2eAgMnSc2d1	český
loutkáře	loutkář	k1gMnSc2	loutkář
Josefa	Josef	k1gMnSc2	Josef
Skupy	skupa	k1gFnSc2	skupa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
mezi	mezi	k7c7	mezi
dětmi	dítě	k1gFnPc7	dítě
i	i	k8xC	i
dospělými	dospělí	k1gMnPc7	dospělí
<g/>
.	.	kIx.	.
</s>
