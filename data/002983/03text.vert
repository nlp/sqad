<s>
Spejbl	Spejbl	k1gMnSc1	Spejbl
a	a	k8xC	a
Hurvínek	Hurvínek	k1gMnSc1	Hurvínek
je	být	k5eAaImIp3nS	být
legendární	legendární	k2eAgFnSc1d1	legendární
dvojice	dvojice	k1gFnSc1	dvojice
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
loutek	loutka	k1gFnPc2	loutka
dle	dle	k7c2	dle
návrhu	návrh	k1gInSc2	návrh
českého	český	k2eAgMnSc2d1	český
loutkáře	loutkář	k1gMnSc2	loutkář
Josefa	Josef	k1gMnSc2	Josef
Skupy	skupa	k1gFnSc2	skupa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
mezi	mezi	k7c7	mezi
dětmi	dítě	k1gFnPc7	dítě
i	i	k8xC	i
dospělými	dospělí	k1gMnPc7	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
loutky	loutka	k1gFnPc1	loutka
jsou	být	k5eAaImIp3nP	být
aktéry	aktér	k1gMnPc4	aktér
mnoha	mnoho	k4c2	mnoho
dětských	dětský	k2eAgFnPc2d1	dětská
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
na	na	k7c6	na
CD	CD	kA	CD
<g/>
,	,	kIx,	,
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
večerníčku	večerníček	k1gInSc6	večerníček
nebo	nebo	k8xC	nebo
knihách	kniha	k1gFnPc6	kniha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc7	jejich
hlavní	hlavní	k2eAgFnSc7d1	hlavní
doménou	doména	k1gFnSc7	doména
jsou	být	k5eAaImIp3nP	být
divadelní	divadelní	k2eAgNnSc4d1	divadelní
představení	představení	k1gNnSc4	představení
Divadla	divadlo	k1gNnSc2	divadlo
Spejbla	Spejbl	k1gMnSc2	Spejbl
a	a	k8xC	a
Hurvínka	Hurvínek	k1gMnSc2	Hurvínek
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
hrají	hrát	k5eAaImIp3nP	hrát
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
a	a	k8xC	a
díky	díky	k7c3	díky
nimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
Hurvínek	Hurvínek	k1gMnSc1	Hurvínek
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Představení	představení	k1gNnPc1	představení
byla	být	k5eAaImAgNnP	být
přeložena	přeložit	k5eAaPmNgNnP	přeložit
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvaceti	dvacet	k4xCc2	dvacet
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
čínštiny	čínština	k1gFnSc2	čínština
a	a	k8xC	a
japonštiny	japonština	k1gFnSc2	japonština
<g/>
.	.	kIx.	.
</s>
<s>
Proslulé	proslulý	k2eAgFnPc1d1	proslulá
loutky	loutka	k1gFnPc1	loutka
Spejbla	Spejbl	k1gMnSc2	Spejbl
a	a	k8xC	a
Hurvínka	Hurvínek	k1gMnSc2	Hurvínek
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
(	(	kIx(	(
<g/>
Spejbl	Spejbl	k1gMnSc1	Spejbl
<g/>
)	)	kIx)	)
a	a	k8xC	a
1926	[number]	k4	1926
(	(	kIx(	(
<g/>
Hurvínek	Hurvínek	k1gMnSc1	Hurvínek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
jim	on	k3xPp3gMnPc3	on
vdechl	vdechnout	k5eAaPmAgMnS	vdechnout
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
charakter	charakter	k1gInSc4	charakter
určil	určit	k5eAaPmAgMnS	určit
divadelní	divadelní	k2eAgMnSc1d1	divadelní
nadšenec	nadšenec	k1gMnSc1	nadšenec
Josef	Josef	k1gMnSc1	Josef
Skupa	skupa	k1gFnSc1	skupa
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
-	-	kIx~	-
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spejblovi	Spejbl	k1gMnSc3	Spejbl
přiřkl	přiřknout	k5eAaPmAgMnS	přiřknout
roli	role	k1gFnSc4	role
nespokojeného	spokojený	k2eNgMnSc2d1	nespokojený
bručouna	bručoun	k1gMnSc2	bručoun
a	a	k8xC	a
popleteného	popletený	k2eAgMnSc2d1	popletený
poučovatele	poučovatel	k1gMnSc2	poučovatel
svého	svůj	k3xOyFgMnSc2	svůj
prostořekého	prostořeký	k2eAgMnSc2d1	prostořeký
syna	syn	k1gMnSc2	syn
Hurvínka	Hurvínek	k1gMnSc2	Hurvínek
<g/>
.	.	kIx.	.
</s>
<s>
Mluvil	mluvit	k5eAaImAgMnS	mluvit
obě	dva	k4xCgFnPc4	dva
postavy	postava	k1gFnPc4	postava
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
plynule	plynule	k6eAd1	plynule
přecházel	přecházet	k5eAaImAgMnS	přecházet
z	z	k7c2	z
basu	bas	k1gInSc2	bas
otcova	otcův	k2eAgInSc2d1	otcův
do	do	k7c2	do
fistulky	fistulka	k1gFnSc2	fistulka
synka	synek	k1gMnSc2	synek
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
dvojice	dvojice	k1gFnSc1	dvojice
Spejbla	Spejbl	k1gMnSc2	Spejbl
a	a	k8xC	a
Hurvínka	Hurvínek	k1gMnSc2	Hurvínek
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c6	o
postavu	postav	k1gInSc6	postav
Máničky	Mánička	k1gFnSc2	Mánička
<g/>
,	,	kIx,	,
paní	paní	k1gFnSc2	paní
Kateřiny	Kateřina	k1gFnSc2	Kateřina
a	a	k8xC	a
psa	pes	k1gMnSc4	pes
Žeryka	Žeryek	k1gMnSc4	Žeryek
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
r.	r.	kA	r.
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
svět	svět	k1gInSc4	svět
paní	paní	k1gFnSc1	paní
Drbálková	Drbálková	k1gFnSc1	Drbálková
<g/>
,	,	kIx,	,
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
a	a	k8xC	a
mluvená	mluvený	k2eAgFnSc1d1	mluvená
Janem	Jan	k1gMnSc7	Jan
Vavříkem-Rýzem	Vavříkem-Rýz	k1gMnSc7	Vavříkem-Rýz
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
1955	[number]	k4	1955
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
paní	paní	k1gFnSc1	paní
Švitorková	Švitorková	k1gFnSc1	Švitorková
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Radko	Radka	k1gFnSc5	Radka
Haken	Haken	k1gMnSc1	Haken
a	a	k8xC	a
vyřezal	vyřezat	k5eAaPmAgMnS	vyřezat
Bohumil	Bohumil	k1gMnSc1	Bohumil
Rubeš	Rubeš	k1gMnSc1	Rubeš
a	a	k8xC	a
kterou	který	k3yRgFnSc4	který
do	do	k7c2	do
r.	r.	kA	r.
1961	[number]	k4	1961
mluvila	mluvit	k5eAaImAgFnS	mluvit
Míla	Míla	k1gFnSc1	Míla
Mellanová	Mellanová	k1gFnSc1	Mellanová
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
postavy	postava	k1gFnPc1	postava
zatím	zatím	k6eAd1	zatím
asi	asi	k9	asi
přibývat	přibývat	k5eAaImF	přibývat
nebudou	být	k5eNaImBp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Dialogy	dialog	k1gInPc1	dialog
obou	dva	k4xCgMnPc2	dva
protagonistů	protagonista	k1gMnPc2	protagonista
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c6	na
principu	princip	k1gInSc6	princip
zaskočeného	zaskočený	k2eAgMnSc2d1	zaskočený
učitele	učitel	k1gMnSc2	učitel
<g/>
,	,	kIx,	,
maskujícího	maskující	k2eAgMnSc2d1	maskující
svou	svůj	k3xOyFgFnSc4	svůj
nedotknutelnost	nedotknutelnost	k1gFnSc4	nedotknutelnost
rodičovskou	rodičovský	k2eAgFnSc7d1	rodičovská
autoritou	autorita	k1gFnSc7	autorita
a	a	k8xC	a
důsledně	důsledně	k6eAd1	důsledně
zvídavého	zvídavý	k2eAgMnSc2d1	zvídavý
žáka	žák	k1gMnSc2	žák
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
loutkové	loutkový	k2eAgFnPc1d1	loutková
postavičky	postavička	k1gFnPc1	postavička
si	se	k3xPyFc3	se
záhy	záhy	k6eAd1	záhy
získaly	získat	k5eAaPmAgFnP	získat
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
popularitu	popularita	k1gFnSc4	popularita
a	a	k8xC	a
rozhovory	rozhovor	k1gInPc1	rozhovor
obou	dva	k4xCgFnPc2	dva
hrdinů	hrdina	k1gMnPc2	hrdina
byly	být	k5eAaImAgInP	být
nahrávány	nahrávat	k5eAaImNgInP	nahrávat
na	na	k7c4	na
gramofonové	gramofonový	k2eAgFnPc4d1	gramofonová
desky	deska	k1gFnPc4	deska
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jejich	jejich	k3xOp3gFnSc4	jejich
slávu	sláva	k1gFnSc4	sláva
dále	daleko	k6eAd2	daleko
šířily	šířit	k5eAaImAgInP	šířit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Josefa	Josef	k1gMnSc2	Josef
Skupy	skupa	k1gFnSc2	skupa
propůjčil	propůjčit	k5eAaPmAgMnS	propůjčit
svůj	svůj	k3xOyFgInSc4	svůj
hlas	hlas	k1gInSc4	hlas
loutkám	loutka	k1gFnPc3	loutka
Spejbla	Spejbl	k1gMnSc4	Spejbl
a	a	k8xC	a
Hurvínka	Hurvínek	k1gMnSc4	Hurvínek
Miloš	Miloš	k1gMnSc1	Miloš
Kirschner	Kirschner	k1gMnSc1	Kirschner
<g/>
.	.	kIx.	.
</s>
<s>
Máničku	Mánička	k1gFnSc4	Mánička
mluvila	mluvit	k5eAaImAgFnS	mluvit
Anna	Anna	k1gFnSc1	Anna
Kreuzmannová	Kreuzmannová	k1gFnSc1	Kreuzmannová
<g/>
,	,	kIx,	,
od	od	k7c2	od
r.	r.	kA	r.
1945	[number]	k4	1945
Božena	Božena	k1gFnSc1	Božena
Weleková	Weleková	k1gFnSc1	Weleková
a	a	k8xC	a
od	od	k7c2	od
r.	r.	kA	r.
1969	[number]	k4	1969
mluví	mluvit	k5eAaImIp3nS	mluvit
Helena	Helena	k1gFnSc1	Helena
Štáchová	Štáchový	k2eAgFnSc1d1	Štáchový
Máničku	Mánička	k1gFnSc4	Mánička
i	i	k8xC	i
Paní	paní	k1gFnSc4	paní
Kateřinu	Kateřina	k1gFnSc4	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
M.	M.	kA	M.
Kirschnera	Kirschner	k1gMnSc2	Kirschner
se	s	k7c7	s
třetím	třetí	k4xOgMnSc7	třetí
interpretem	interpret	k1gMnSc7	interpret
Spejbla	Spejbl	k1gMnSc4	Spejbl
a	a	k8xC	a
Hurvínka	Hurvínek	k1gMnSc4	Hurvínek
stal	stát	k5eAaPmAgMnS	stát
Martin	Martin	k1gMnSc1	Martin
Klásek	klásek	k1gInSc4	klásek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
návštěvě	návštěva	k1gFnSc6	návštěva
u	u	k7c2	u
Spejbla	Spejbl	k1gMnSc2	Spejbl
a	a	k8xC	a
Hurvínka	Hurvínek	k1gMnSc2	Hurvínek
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Znovu	znovu	k6eAd1	znovu
u	u	k7c2	u
Spejbla	Spejbl	k1gMnSc2	Spejbl
a	a	k8xC	a
Hurvínka	Hurvínek	k1gMnSc2	Hurvínek
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
-	-	kIx~	-
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Hurvínek	Hurvínek	k1gMnSc1	Hurvínek
vzduchoplavcem	vzduchoplavec	k1gMnSc7	vzduchoplavec
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Hurvínkův	Hurvínkův	k2eAgInSc1d1	Hurvínkův
rok	rok	k1gInSc1	rok
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Divadlo	divadlo	k1gNnSc1	divadlo
Spejbla	Spejbl	k1gMnSc2	Spejbl
a	a	k8xC	a
Hurvínka	Hurvínek	k1gMnSc2	Hurvínek
Spejbl	Spejbl	k1gMnSc1	Spejbl
Hurvínek	Hurvínek	k1gMnSc1	Hurvínek
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Spejbl	Spejbl	k1gMnSc1	Spejbl
a	a	k8xC	a
Hurvínek	Hurvínek	k1gMnSc1	Hurvínek
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc1	Commons
Divadlo	divadlo	k1gNnSc1	divadlo
Spejbla	Spejbl	k1gMnSc2	Spejbl
a	a	k8xC	a
Hurvínka	Hurvínek	k1gMnSc2	Hurvínek
Hurvínkova	Hurvínkův	k2eAgFnSc1d1	Hurvínkova
planetka	planetka	k1gFnSc1	planetka
(	(	kIx(	(
<g/>
CZ	CZ	kA	CZ
Fan	Fana	k1gFnPc2	Fana
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
Loutka	loutka	k1gFnSc1	loutka
Spejbl	Spejbl	k1gMnSc1	Spejbl
</s>
