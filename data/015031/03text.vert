<s>
Turistická	turistický	k2eAgFnSc1d1
značená	značený	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
4202	#num#	k4
</s>
<s>
Turistická	turistický	k2eAgFnSc1d1
značená	značený	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
4202	#num#	k4
Trasa	trasa	k1gFnSc1
4202	#num#	k4
klesá	klesat	k5eAaImIp3nS
od	od	k7c2
Špindlerovky	Špindlerovka	k1gFnSc2
k	k	k7c3
Lužické	lužický	k2eAgFnSc3d1
bouděZákladní	bouděZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Typ	typ	k1gInSc1
</s>
<s>
turistická	turistický	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Délka	délka	k1gFnSc1
</s>
<s>
3,5	3,5	k4
km	km	kA
Provozovatel	provozovatel	k1gMnSc1
</s>
<s>
Klub	klub	k1gInSc1
českých	český	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
Značení	značení	k1gNnSc2
</s>
<s>
zelená	zelený	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Sezóna	sezóna	k1gFnSc1
</s>
<s>
celoroční	celoroční	k2eAgFnSc1d1
Lokalizace	lokalizace	k1gFnSc1
Poloha	poloha	k1gFnSc1
</s>
<s>
KrkonošeČesko	KrkonošeČesko	k6eAd1
Česko	Česko	k1gNnSc1
<g/>
,	,	kIx,
Okres	okres	k1gInSc1
Trutnov	Trutnov	k1gInSc1
Start	start	k1gInSc1
</s>
<s>
U	u	k7c2
Dívčí	dívčí	k2eAgFnSc2d1
lávky	lávka	k1gFnSc2
(	(	kIx(
<g/>
bus	bus	k1gInSc1
<g/>
)	)	kIx)
Cíl	cíl	k1gInSc1
</s>
<s>
Špindlerova	Špindlerův	k2eAgFnSc1d1
bouda	bouda	k1gFnSc1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
/	/	kIx~
<g/>
PL	PL	kA
<g/>
,	,	kIx,
bus	bus	k1gInSc1
<g/>
)	)	kIx)
Výškový	výškový	k2eAgInSc1d1
rozdíl	rozdíl	k1gInSc1
</s>
<s>
419	#num#	k4
m	m	kA
Nejvyšší	vysoký	k2eAgInSc4d3
bod	bod	k1gInSc4
</s>
<s>
koncové	koncový	k2eAgNnSc1d1
rozcestí	rozcestí	k1gNnSc1
1200	#num#	k4
m	m	kA
Nejnižší	nízký	k2eAgInSc4d3
bod	bod	k1gInSc4
</s>
<s>
počáteční	počáteční	k2eAgNnSc4d1
rozcestí	rozcestí	k1gNnSc4
781	#num#	k4
m	m	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Turistická	turistický	k2eAgFnSc1d1
značená	značený	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
4202	#num#	k4
je	být	k5eAaImIp3nS
3,5	3,5	k4
km	km	kA
dlouhá	dlouhý	k2eAgFnSc1d1
zeleně	zeleň	k1gFnSc2
značená	značený	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
Klubu	klub	k1gInSc2
českých	český	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
okrese	okres	k1gInSc6
Trutnov	Trutnov	k1gInSc1
spojující	spojující	k2eAgFnSc2d1
Dívčí	dívčí	k2eAgFnSc2d1
lávky	lávka	k1gFnSc2
a	a	k8xC
Špindlerovu	Špindlerův	k2eAgFnSc4d1
boudu	bouda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
převažující	převažující	k2eAgInSc1d1
směr	směr	k1gInSc1
je	být	k5eAaImIp3nS
severovýchodní	severovýchodní	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
trasy	trasa	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
Krkonošského	krkonošský	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
trasy	trasa	k1gFnSc2
</s>
<s>
Turistická	turistický	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
má	mít	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
počátek	počátek	k1gInSc4
nedaleko	nedaleko	k7c2
autobusové	autobusový	k2eAgFnSc2d1
zastávky	zastávka	k1gFnSc2
na	na	k7c6
Dívčích	dívčí	k2eAgFnPc6d1
lávkách	lávka	k1gFnPc6
na	na	k7c6
rozcestí	rozcestí	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
přímo	přímo	k6eAd1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
stejně	stejně	k6eAd1
značenou	značený	k2eAgFnSc4d1
trasu	trasa	k1gFnSc4
4201	#num#	k4
přicházející	přicházející	k2eAgFnSc1d1
od	od	k7c2
pramene	pramen	k1gInSc2
Labe	Labe	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
tudy	tudy	k6eAd1
prochází	procházet	k5eAaImIp3nS
modře	modro	k6eAd1
značená	značený	k2eAgFnSc1d1
Weberova	Weberův	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
ze	z	k7c2
Špindlerova	Špindlerův	k2eAgInSc2d1
Mlýna	mlýn	k1gInSc2
do	do	k7c2
Obřího	obří	k2eAgNnSc2d1
sedla	sedlo	k1gNnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
tu	tu	k6eAd1
výchozí	výchozí	k2eAgFnSc1d1
žlutě	žlutě	k6eAd1
značená	značený	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
7362	#num#	k4
na	na	k7c4
Petrovu	Petrův	k2eAgFnSc4d1
boudu	bouda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
posledními	poslední	k2eAgMnPc7d1
dvěma	dva	k4xCgFnPc7
trasami	trasa	k1gFnPc7
vede	vést	k5eAaImIp3nS
zpočátku	zpočátku	k6eAd1
trasa	trasa	k1gFnSc1
4202	#num#	k4
po	po	k7c6
asfaltové	asfaltový	k2eAgFnSc6d1
komunikaci	komunikace	k1gFnSc6
proti	proti	k7c3
proudu	proud	k1gInSc3
Bílého	bílý	k2eAgNnSc2d1
Labe	Labe	k1gNnSc2
v	v	k7c6
souběhu	souběh	k1gInSc6
<g/>
,	,	kIx,
s	s	k7c7
trasou	trasa	k1gFnSc7
7362	#num#	k4
k	k	k7c3
oboře	obora	k1gFnSc3
Dívčí	dívčí	k2eAgFnSc2d1
lávky	lávka	k1gFnSc2
a	a	k8xC
s	s	k7c7
Weberovou	Weberův	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
k	k	k7c3
Červenému	červený	k2eAgInSc3d1
potoku	potok	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
trasa	trasa	k1gFnSc1
4202	#num#	k4
opouští	opouštět	k5eAaImIp3nS
silnici	silnice	k1gFnSc4
a	a	k8xC
stoupá	stoupat	k5eAaImIp3nS
po	po	k7c6
lesních	lesní	k2eAgFnPc6d1
pěšinách	pěšina	k1gFnPc6
k	k	k7c3
severovýchodu	severovýchod	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zleva	zleva	k6eAd1
míjí	míjet	k5eAaImIp3nS
okraj	okraj	k1gInSc4
Jeleních	jelení	k2eAgFnPc2d1
Bud	bouda	k1gFnPc2
a	a	k8xC
poté	poté	k6eAd1
třikrát	třikrát	k6eAd1
kříží	křížit	k5eAaImIp3nS
silnici	silnice	k1gFnSc3
Špindlerův	Špindlerův	k2eAgInSc4d1
Mlýn	mlýn	k1gInSc4
–	–	k?
Špindlerova	Špindlerův	k2eAgFnSc1d1
bouda	bouda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
Lužickou	lužický	k2eAgFnSc7d1
boudou	bouda	k1gFnSc7
přechází	přecházet	k5eAaImIp3nS
Hřímavou	hřímavý	k2eAgFnSc4d1
bystřinu	bystřina	k1gFnSc4
<g/>
,	,	kIx,
u	u	k7c2
boudy	bouda	k1gFnSc2
samotné	samotný	k2eAgFnPc4d1
pak	pak	k6eAd1
vstupuje	vstupovat	k5eAaImIp3nS
do	do	k7c2
souběhu	souběh	k1gInSc2
se	s	k7c7
žlutě	žlutě	k6eAd1
značenou	značený	k2eAgFnSc7d1
trasou	trasa	k1gFnSc7
7204	#num#	k4
od	od	k7c2
Boudy	bouda	k1gFnSc2
u	u	k7c2
Bílého	bílý	k2eAgNnSc2d1
Labe	Labe	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
pak	pak	k6eAd1
stoupají	stoupat	k5eAaImIp3nP
loukou	louka	k1gFnSc7
ke	k	k7c3
Špindlerově	Špindlerův	k2eAgFnSc3d1
boudě	bouda	k1gFnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
končí	končit	k5eAaImIp3nS
na	na	k7c6
rozcestí	rozcestí	k1gNnSc6
s	s	k7c7
červeně	červeně	k6eAd1
značenou	značený	k2eAgFnSc7d1
Cestou	cesta	k1gFnSc7
česko-polského	česko-polský	k2eAgNnSc2d1
přátelství	přátelství	k1gNnSc2
a	a	k8xC
rovněž	rovněž	k9
zde	zde	k6eAd1
končící	končící	k2eAgInSc1d1
modře	modro	k6eAd1
značenou	značený	k2eAgFnSc7d1
trasou	trasa	k1gFnSc7
1882	#num#	k4
od	od	k7c2
Medvědí	medvědí	k2eAgFnSc2d1
boudy	bouda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
blízkosti	blízkost	k1gFnSc3
státní	státní	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
možné	možný	k2eAgNnSc1d1
navázat	navázat	k5eAaPmF
rovněž	rovněž	k9
na	na	k7c4
polské	polský	k2eAgFnPc4d1
turistické	turistický	k2eAgFnPc4d1
trasy	trasa	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Turistická	turistický	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
měla	mít	k5eAaImAgFnS
dříve	dříve	k6eAd2
svůj	svůj	k3xOyFgInSc4
počátek	počátek	k1gInSc4
v	v	k7c6
centru	centrum	k1gNnSc6
Špindlerova	Špindlerův	k2eAgInSc2d1
Mlýna	mlýn	k1gInSc2
a	a	k8xC
k	k	k7c3
Dívčím	dívčí	k2eAgFnPc3d1
Lávkám	lávka	k1gFnPc3
sledovala	sledovat	k5eAaImAgFnS
levobřežní	levobřežní	k2eAgFnSc4d1
silnici	silnice	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Plánovač	plánovač	k1gMnSc1
tras	trasa	k1gFnPc2
KČT	KČT	kA
<g/>
↑	↑	k?
Trasa	trasa	k1gFnSc1
4202	#num#	k4
na	na	k7c6
Mapách	mapa	k1gFnPc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Starší	starý	k2eAgNnSc4d2
vydání	vydání	k1gNnSc4
map	mapa	k1gFnPc2
KČT	KČT	kA
</s>
