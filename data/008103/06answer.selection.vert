<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
podoba	podoba	k1gFnSc1	podoba
výrazu	výraz	k1gInSc2	výraz
(	(	kIx(	(
<g/>
skyscraper	skyscraper	k1gInSc1	skyscraper
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
také	také	k6eAd1	také
první	první	k4xOgInPc1	první
mrakodrapy	mrakodrap	k1gInPc1	mrakodrap
postaveny	postaven	k2eAgInPc1d1	postaven
<g/>
.	.	kIx.	.
</s>
