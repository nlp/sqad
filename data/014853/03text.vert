<s>
These	these	k1gFnSc1
Are	ar	k1gInSc5
the	the	k?
Days	Daysa	k1gFnPc2
of	of	k?
Our	Our	k1gMnSc1
Lives	Lives	k1gMnSc1
</s>
<s>
These	these	k1gFnSc1
Are	ar	k1gInSc5
the	the	k?
Days	Daysa	k1gFnPc2
of	of	k?
Our	Our	k1gMnSc1
Lives	Lives	k1gMnSc1
</s>
<s>
Singl	singl	k1gInSc1
od	od	k7c2
Queen	Quena	k1gFnPc2
</s>
<s>
z	z	k7c2
alba	album	k1gNnSc2
Innuendo	Innuendo	k6eAd1
</s>
<s>
Strana	strana	k1gFnSc1
B	B	kA
</s>
<s>
Bohemian	bohemian	k1gInSc1
Rhapsody	Rhapsoda	k1gFnSc2
</s>
<s>
Vydáno	vydán	k2eAgNnSc1d1
</s>
<s>
1991	#num#	k4
</s>
<s>
Formát	formát	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
"	"	kIx"
singl	singl	k1gInSc1
<g/>
,	,	kIx,
CD	CD	kA
singl	singl	k1gInSc1
</s>
<s>
Nahráno	nahrát	k5eAaBmNgNnS,k5eAaPmNgNnS
</s>
<s>
1989	#num#	k4
-	-	kIx~
1990	#num#	k4
</s>
<s>
Žánr	žánr	k1gInSc1
</s>
<s>
rock	rock	k1gInSc1
</s>
<s>
Délka	délka	k1gFnSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
</s>
<s>
Autor	autor	k1gMnSc1
</s>
<s>
Queen	Queen	k1gInSc1
(	(	kIx(
<g/>
Roger	Roger	k1gMnSc1
Taylor	Taylor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Producent	producent	k1gMnSc1
</s>
<s>
Queen	Queen	k1gInSc1
a	a	k8xC
David	David	k1gMnSc1
Richards	Richardsa	k1gFnPc2
</s>
<s>
Chronologie	chronologie	k1gFnSc1
singlů	singl	k1gInPc2
Queen	Quena	k1gFnPc2
</s>
<s>
„	„	k?
<g/>
I	i	k9
Can	Can	k1gFnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Live	Live	k1gInSc1
With	With	k1gMnSc1
You	You	k1gMnSc1
<g/>
“	“	k?
<g/>
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
These	these	k1gFnSc1
Are	ar	k1gInSc5
The	The	k1gMnSc1
Days	Daysa	k1gFnPc2
Of	Of	k1gMnSc1
Our	Our	k1gMnSc1
Lives	Lives	k1gMnSc1
<g/>
“	“	k?
<g/>
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
The	The	k1gMnSc1
Show	show	k1gFnSc2
Must	Must	k1gMnSc1
Go	Go	k1gMnSc1
On	on	k3xPp3gMnSc1
<g/>
“	“	k?
<g/>
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Chronologie	chronologie	k1gFnSc1
singlů	singl	k1gInPc2
Queen	Quena	k1gFnPc2
</s>
<s>
„	„	k?
<g/>
Stone	ston	k1gInSc5
Cold	Cold	k1gInSc1
Crazy	Craz	k1gInPc1
<g/>
“	“	k?
<g/>
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bohemian	bohemian	k1gInSc1
Rhapsody	Rhapsoda	k1gFnSc2
/	/	kIx~
These	these	k1gFnSc1
Are	ar	k1gInSc5
the	the	k?
Days	Daysa	k1gFnPc2
of	of	k?
Our	Our	k1gMnSc1
Lives	Lives	k1gMnSc1
<g/>
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Delilah	Delilah	k1gInSc1
<g/>
“	“	k?
<g/>
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
These	these	k1gFnSc1
Are	ar	k1gInSc5
the	the	k?
Days	Daysa	k1gFnPc2
of	of	k?
Our	Our	k1gMnSc1
Lives	Lives	k1gMnSc1
je	být	k5eAaImIp3nS
píseň	píseň	k1gFnSc4
britské	britský	k2eAgFnSc2d1
rockové	rockový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Queen	Quena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
je	být	k5eAaImIp3nS
autorství	autorství	k1gNnSc1
připisováno	připisován	k2eAgNnSc1d1
celému	celý	k2eAgInSc3d1
souboru	soubor	k1gInSc3
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
skladby	skladba	k1gFnSc2
je	být	k5eAaImIp3nS
dílem	dílo	k1gNnSc7
Rogera	Roger	k1gMnSc2
Taylora	Taylor	k1gMnSc2
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Píseň	píseň	k1gFnSc1
vyšla	vyjít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
na	na	k7c6
albu	album	k1gNnSc6
Innuendo	Innuendo	k6eAd1
a	a	k8xC
následně	následně	k6eAd1
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
také	také	k9
jako	jako	k8xC,k8xS
singl	singl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Mercuryho	Mercury	k1gMnSc2
smrti	smrt	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
vyšla	vyjít	k5eAaPmAgFnS
píseň	píseň	k1gFnSc1
znovu	znovu	k6eAd1
jako	jako	k8xC,k8xS
singl	singl	k1gInSc4
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
jako	jako	k9
„	„	k?
<g/>
double	double	k1gInSc1
A-side	A-sid	k1gInSc5
<g/>
“	“	k?
spolu	spolu	k6eAd1
s	s	k7c7
písní	píseň	k1gFnSc7
„	„	k?
<g/>
Bohemian	bohemian	k1gInSc4
Rhapsody	Rhapsoda	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
psaní	psaní	k1gNnSc6
textu	text	k1gInSc2
se	se	k3xPyFc4
Roger	Roger	k1gMnSc1
Taylor	Taylor	k1gMnSc1
jakoby	jakoby	k8xS
vcítil	vcítit	k5eAaPmAgMnS
do	do	k7c2
frontmana	frontman	k1gMnSc2
Freddieho	Freddie	k1gMnSc2
Mercuryho	Mercury	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
v	v	k7c6
době	doba	k1gFnSc6
vzniku	vznik	k1gInSc2
písně	píseň	k1gFnSc2
trpěl	trpět	k5eAaImAgInS
velmi	velmi	k6eAd1
pokročilým	pokročilý	k2eAgNnSc7d1
stadiem	stadion	k1gNnSc7
nemoci	nemoc	k1gFnSc2
AIDS	AIDS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
refrénu	refrén	k1gInSc6
se	se	k3xPyFc4
zpívá	zpívat	k5eAaImIp3nS
„	„	k?
<g/>
Those	Those	k1gFnSc1
were	wer	k1gFnSc2
the	the	k?
days	days	k1gInSc1
of	of	k?
our	our	k?
lives	lives	k1gInSc1
<g/>
“	“	k?
v	v	k7c6
překladu	překlad	k1gInSc6
„	„	k?
<g/>
To	ten	k3xDgNnSc1
byly	být	k5eAaImAgInP
dny	den	k1gInPc1
našich	naši	k1gMnPc2
životů	život	k1gInPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Freddie	Freddie	k1gFnSc1
Mercury	Mercura	k1gFnSc2
se	s	k7c7
zpíváním	zpívání	k1gNnSc7
písně	píseň	k1gFnSc2
ohlíží	ohlížet	k5eAaImIp3nS
zpět	zpět	k6eAd1
za	za	k7c7
svým	svůj	k3xOyFgInSc7
životem	život	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roger	Roger	k1gMnSc1
Taylor	Taylor	k1gMnSc1
kromě	kromě	k7c2
narážek	narážka	k1gFnPc2
na	na	k7c4
Mercuryho	Mercury	k1gMnSc4
stav	stav	k1gInSc4
píseň	píseň	k1gFnSc1
napsal	napsat	k5eAaPmAgInS,k5eAaBmAgInS
o	o	k7c6
dětech	dítě	k1gFnPc6
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
skrz	skrz	k7c4
ně	on	k3xPp3gInPc4
lze	lze	k6eAd1
užívat	užívat	k5eAaImF
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
sami	sám	k3xTgMnPc1
byli	být	k5eAaImAgMnP
mladí	mladý	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Videoklip	videoklip	k1gInSc1
</s>
<s>
K	k	k7c3
písni	píseň	k1gFnSc3
byl	být	k5eAaImAgInS
začátkem	začátkem	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
natočen	natočen	k2eAgInSc1d1
videoklip	videoklip	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jeden	k4xCgFnSc7
věcí	věc	k1gFnSc7
se	se	k3xPyFc4
ale	ale	k9
od	od	k7c2
ostatních	ostatní	k2eAgInPc2d1
videoklipů	videoklip	k1gInPc2
skupiny	skupina	k1gFnSc2
Queen	Quena	k1gFnPc2
liší	lišit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k1gNnSc1
energických	energický	k2eAgInPc2d1
pohybů	pohyb	k1gInPc2
Freddie	Freddie	k1gFnSc2
Mercury	Mercura	k1gFnSc2
na	na	k7c6
videu	video	k1gNnSc6
pouze	pouze	k6eAd1
stojí	stát	k5eAaImIp3nS
a	a	k8xC
zpívá	zpívat	k5eAaImIp3nS
o	o	k7c6
svém	svůj	k3xOyFgInSc6
životě	život	k1gInSc6
<g/>
.	.	kIx.
„	„	k?
<g/>
These	these	k1gFnSc1
Are	ar	k1gInSc5
the	the	k?
Days	Days	k1gInSc1
of	of	k?
Our	Our	k1gFnSc1
Lives	Lives	k1gInSc1
<g/>
“	“	k?
byla	být	k5eAaImAgFnS
totiž	totiž	k9
poslední	poslední	k2eAgFnSc4d1
píseň	píseň	k1gFnSc4
<g/>
,	,	kIx,
ke	k	k7c3
které	který	k3yRgFnSc3,k3yQgFnSc3,k3yIgFnSc3
stihl	stihnout	k5eAaPmAgMnS
natočit	natočit	k5eAaBmF
klip	klip	k1gInSc4
před	před	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
než	než	k8xS
podlehl	podlehnout	k5eAaPmAgMnS
nemoci	nemoc	k1gFnSc3
AIDS	aids	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
albu	album	k1gNnSc6
Innuendo	Innuendo	k6eAd1
je	být	k5eAaImIp3nS
většina	většina	k1gFnSc1
videoklipů	videoklip	k1gInPc2
k	k	k7c3
písním	píseň	k1gFnPc3
černobílá	černobílý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
tak	tak	k6eAd1
učiněno	učinit	k5eAaImNgNnS,k5eAaPmNgNnS
na	na	k7c4
přání	přání	k1gNnSc4
Freddieho	Freddie	k1gMnSc2
Mercuryho	Mercury	k1gMnSc2
<g/>
,	,	kIx,
na	na	k7c6
jehož	jehož	k3xOyRp3gInSc6
vzhledu	vzhled	k1gInSc6
se	se	k3xPyFc4
čím	co	k3yQnSc7,k3yRnSc7,k3yInSc7
dál	daleko	k6eAd2
tím	ten	k3xDgNnSc7
víc	hodně	k6eAd2
projevovaly	projevovat	k5eAaImAgInP
příznaky	příznak	k1gInPc1
AIDS	AIDS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
značně	značně	k6eAd1
bledý	bledý	k2eAgInSc1d1
a	a	k8xC
pohublý	pohublý	k2eAgInSc1d1
<g/>
,	,	kIx,
bez	bez	k7c2
energie	energie	k1gFnSc2
a	a	k8xC
nechtěl	chtít	k5eNaImAgMnS
aby	aby	kYmCp3nS
se	se	k3xPyFc4
o	o	k7c6
jeho	jeho	k3xOp3gInSc6
zdravotním	zdravotní	k2eAgInSc6d1
stavu	stav	k1gInSc6
dozvěděla	dozvědět	k5eAaPmAgFnS
veřejnost	veřejnost	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
“	“	k?
<g/>
Innuendo	Innuendo	k1gNnSc1
-	-	kIx~
These	these	k1gFnSc1
Are	ar	k1gInSc5
The	The	k1gMnSc1
Days	Daysa	k1gFnPc2
Of	Of	k1gMnSc1
Our	Our	k1gMnSc1
Lives	Lives	k1gMnSc1
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
UltimateQueen	UltimateQuena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
25	#num#	k4
June	jun	k1gMnSc5
2011	#num#	k4
<g/>
↑	↑	k?
Zvládl	zvládnout	k5eAaPmAgInS
to	ten	k3xDgNnSc1
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
<g/>
:	:	kIx,
víte	vědět	k5eAaImIp2nP
<g/>
,	,	kIx,
proč	proč	k6eAd1
jsou	být	k5eAaImIp3nP
poslední	poslední	k2eAgInPc4d1
klipy	klip	k1gInPc4
Queen	Queen	k2eAgMnSc1d1
s	s	k7c7
Freddiem	Freddium	k1gNnSc7
Mercurym	Mercurymum	k1gNnPc2
černobílé	černobílý	k2eAgFnPc1d1
<g/>
?	?	kIx.
</s>
<s desamb="1">
–	–	k?
G.	G.	kA
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
G.	G.	kA
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc4d1
videoklip	videoklip	k1gInSc4
na	na	k7c4
YouTube	YouTub	k1gInSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Singly	singl	k1gInPc1
<g/>
[	[	kIx(
<g/>
P	P	kA
1	#num#	k4
<g/>
]	]	kIx)
skupiny	skupina	k1gFnPc1
Queen	Queen	k1gInSc4
Diskografie	diskografie	k1gFnSc2
Queen	Queen	k2eAgInSc4d1
</s>
<s>
„	„	k?
<g/>
Keep	Keep	k1gMnSc1
Yourself	Yourself	k1gMnSc1
Alive	Aliev	k1gFnSc2
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Son	son	k1gInSc1
and	and	k?
Daughter	Daughter	k1gInSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Liar	Liar	k1gInSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Doing	Doing	k1gMnSc1
All	All	k1gMnSc1
Right	Right	k1gMnSc1
<g/>
“	“	k?
Queen	Queno	k1gNnPc2
II	II	kA
</s>
<s>
„	„	k?
<g/>
Seven	Seven	k1gInSc1
Seas	Seas	k1gInSc1
of	of	k?
Rhye	Rhye	k1gInSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
See	See	k1gMnSc1
What	What	k1gMnSc1
a	a	k8xC
Fool	Fool	k1gMnSc1
I	i	k9
<g/>
'	'	kIx"
<g/>
ve	v	k7c4
Been	Been	k1gInSc4
<g/>
“	“	k?
(	(	kIx(
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
„	„	k?
<g/>
The	The	k1gMnSc1
Loser	Loser	k1gMnSc1
in	in	k?
the	the	k?
End	End	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
Japonsko	Japonsko	k1gNnSc1
<g/>
)	)	kIx)
Sheer	Sheer	k1gInSc1
Heart	Hearta	k1gFnPc2
Attack	Attacka	k1gFnPc2
</s>
<s>
„	„	k?
<g/>
Killer	Killer	k1gInSc1
Queen	Queen	k1gInSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Flick	Flick	k1gInSc1
of	of	k?
the	the	k?
Wrist	Wrist	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
double	double	k1gInSc1
A	a	k8xC
strana	strana	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Now	Now	k1gMnSc1
I	I	kA
<g/>
'	'	kIx"
<g/>
m	m	kA
Here	Here	k1gInSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Lily	lít	k5eAaImAgInP
of	of	k?
the	the	k?
Valley	Vallea	k1gFnSc2
<g/>
“	“	k?
A	a	k9
Night	Night	k1gMnSc1
at	at	k?
the	the	k?
Opera	opera	k1gFnSc1
</s>
<s>
„	„	k?
<g/>
Bohemian	bohemian	k1gInSc1
Rhapsody	Rhapsoda	k1gFnSc2
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
I	I	kA
<g/>
'	'	kIx"
<g/>
m	m	kA
in	in	k?
Love	lov	k1gInSc5
with	with	k1gInSc1
My	my	k3xPp1nPc1
Car	car	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
You	You	k1gFnSc1
<g/>
'	'	kIx"
<g/>
re	re	k9
My	my	k3xPp1nPc1
Best	Best	k1gMnSc1
Friend	Frienda	k1gFnPc2
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
’	’	k?
<g/>
39	#num#	k4
<g/>
“	“	k?
A	A	kA
Day	Day	k1gMnSc3
at	at	k?
the	the	k?
Races	Races	k1gInSc1
</s>
<s>
„	„	k?
<g/>
Somebody	Someboda	k1gFnSc2
to	ten	k3xDgNnSc1
Love	lov	k1gInSc5
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
White	Whit	k1gInSc5
Man	Man	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
„	„	k?
<g/>
Long	Long	k1gMnSc1
Away	Awaa	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Tie	Tie	k1gMnSc1
Your	Your	k1gMnSc1
Mother	Mothra	k1gFnPc2
Down	Down	k1gMnSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
You	You	k1gFnSc2
And	Anda	k1gFnPc2
I	i	k9
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Teo	Tea	k1gFnSc5
Torriatte	Torriatt	k1gInSc5
(	(	kIx(
<g/>
Let	let	k1gInSc4
Us	Us	k1gMnPc2
Cling	Cling	k1gInSc1
Together	Togethra	k1gFnPc2
<g/>
)	)	kIx)
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Good	Good	k1gMnSc1
Old-Fashioned	Old-Fashioned	k1gMnSc1
Lover	Lover	k1gMnSc1
Boy	boy	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Long	Long	k1gMnSc1
Away	Awaa	k1gFnSc2
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
You	You	k1gFnSc2
And	Anda	k1gFnPc2
I	i	k9
<g/>
“	“	k?
News	News	k1gInSc1
of	of	k?
the	the	k?
World	World	k1gInSc1
</s>
<s>
„	„	k?
<g/>
We	We	k1gFnSc1
Are	ar	k1gInSc5
the	the	k?
Champions	Champions	k1gInSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
We	We	k1gFnSc2
Will	Willa	k1gFnPc2
Rock	rock	k1gInSc1
You	You	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
double	double	k1gInSc1
A	a	k8xC
strana	strana	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Spread	Spread	k1gInSc1
Your	Your	k1gMnSc1
Wings	Wingsa	k1gFnPc2
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Sheer	Sheer	k1gInSc1
Heart	Heart	k1gInSc1
Attack	Attack	k1gInSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
It	It	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Late	lat	k1gInSc5
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Sheer	Sheer	k1gInSc1
Heart	Heart	k1gInSc1
Attack	Attack	k1gInSc1
<g/>
“	“	k?
Jazz	jazz	k1gInSc1
</s>
<s>
„	„	k?
<g/>
Bicycle	Bicycle	k1gInSc1
Race	Race	k1gInSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Fat	fatum	k1gNnPc2
Bottomed	Bottomed	k1gInSc1
Girls	girl	k1gFnPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
double	double	k1gInSc1
A	a	k8xC
strana	strana	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Stop	stop	k1gInSc1
Me	Me	k1gMnSc2
Now	Now	k1gMnSc2
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
In	In	k1gMnSc2
Only	Onla	k1gMnSc2
Seven	Seven	k2eAgInSc1d1
Days	Days	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
„	„	k?
<g/>
More	mor	k1gInSc5
of	of	k?
That	That	k2eAgInSc1d1
Jazz	jazz	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Mustapha	Mustapha	k1gFnSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Dead	Dead	k1gInSc1
On	on	k3xPp3gInSc1
Time	Time	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
„	„	k?
<g/>
In	In	k1gMnSc2
Only	Onla	k1gMnSc2
Seven	Seven	k2eAgInSc1d1
Days	Days	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Jugoslávie	Jugoslávie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Jealousy	Jealous	k1gInPc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Fun	Fun	k1gMnSc1
It	It	k1gMnSc1
<g/>
“	“	k?
<g/>
;	;	kIx,
„	„	k?
<g/>
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Stop	stop	k1gInSc1
Me	Me	k1gMnSc2
Now	Now	k1gMnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
SSSR	SSSR	kA
<g/>
)	)	kIx)
Live	Liv	k1gMnSc2
Killers	Killers	k1gInSc1
</s>
<s>
„	„	k?
<g/>
Love	lov	k1gInSc5
of	of	k?
My	my	k3xPp1nPc1
Life	Life	k1gNnPc6
(	(	kIx(
<g/>
živě	živě	k6eAd1
<g/>
)	)	kIx)
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Now	Now	k1gFnPc4
I	I	kA
<g/>
'	'	kIx"
<g/>
m	m	kA
Here	Here	k1gFnSc6
(	(	kIx(
<g/>
živě	živě	k6eAd1
<g/>
)	)	kIx)
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
We	We	k1gMnSc1
Will	Will	k1gMnSc1
Rock	rock	k1gInSc4
You	You	k1gFnSc2
(	(	kIx(
<g/>
živě	živě	k6eAd1
<g/>
)	)	kIx)
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Let	let	k1gInSc1
Me	Me	k1gMnSc1
Entertain	Entertain	k1gMnSc1
You	You	k1gMnSc1
(	(	kIx(
<g/>
živě	živě	k6eAd1
<g/>
)	)	kIx)
<g/>
“	“	k?
The	The	k1gFnSc1
Game	game	k1gInSc1
</s>
<s>
„	„	k?
<g/>
Crazy	Craz	k1gInPc1
Little	Little	k1gFnSc1
Thing	Thing	k1gInSc1
Called	Called	k1gInSc1
Love	lov	k1gInSc5
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
We	We	k1gFnSc2
Will	Willa	k1gFnPc2
Rock	rock	k1gInSc1
You	You	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
„	„	k?
<g/>
Spread	Spread	k1gInSc1
Your	Your	k1gMnSc1
Wings	Wingsa	k1gFnPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Save	Save	k1gNnSc1
Me	Me	k1gMnSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Let	let	k1gInSc1
Me	Me	k1gMnSc1
Entertain	Entertain	k1gMnSc1
You	You	k1gMnSc1
(	(	kIx(
<g/>
živě	živě	k6eAd1
<g/>
)	)	kIx)
<g/>
“	“	k?
(	(	kIx(
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
„	„	k?
<g/>
Sheer	Sheer	k1gInSc1
Heart	Heart	k1gInSc1
Attack	Attack	k1gInSc4
(	(	kIx(
<g/>
živě	živě	k6eAd1
<g/>
)	)	kIx)
<g/>
“	“	k?
(	(	kIx(
<g/>
Japonsko	Japonsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Play	play	k0
the	the	k?
Game	game	k1gInSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
A	a	k8xC
Human	Human	k1gInSc1
Body	bod	k1gInPc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Another	Anothra	k1gFnPc2
One	One	k1gMnSc1
Bites	Bites	k1gMnSc1
the	the	k?
Dust	Dust	k1gMnSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Dragon	Dragon	k1gMnSc1
Attack	Attack	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
„	„	k?
<g/>
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Try	Try	k1gMnSc5
Suicide	Suicid	k1gMnSc5
<g/>
“	“	k?
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Need	Need	k1gInSc1
Your	Youra	k1gFnPc2
Loving	Loving	k1gInSc1
Tonight	Tonight	k1gMnSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Rock	rock	k1gInSc1
It	It	k1gFnSc1
(	(	kIx(
<g/>
Prime	prim	k1gInSc5
Jive	jive	k1gInSc4
<g/>
)	)	kIx)
<g/>
“	“	k?
Flash	Flash	k1gMnSc1
Gordon	Gordon	k1gMnSc1
</s>
<s>
„	„	k?
<g/>
Flash	Flash	k1gInSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Football	Football	k1gMnSc1
Fight	Fight	k2eAgMnSc1d1
<g/>
“	“	k?
Hot	hot	k0
Space	Space	k1gFnSc1
</s>
<s>
„	„	k?
<g/>
Under	Under	k1gInSc1
Pressure	Pressur	k1gMnSc5
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Soul	Soul	k1gInSc1
Brother	Brother	kA
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Body	bod	k1gInPc1
Language	language	k1gFnPc2
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Life	Life	k1gInSc1
is	is	k?
Real	Real	k1gInSc1
(	(	kIx(
<g/>
Song	song	k1gInSc1
for	forum	k1gNnPc2
Lennon	Lennon	k1gInSc1
<g/>
)	)	kIx)
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Las	laso	k1gNnPc2
Palabras	Palabras	k1gInSc1
de	de	k?
Amor	Amor	k1gMnSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Cool	Cool	k1gMnSc1
Cat	Cat	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Calling	Calling	k1gInSc1
All	All	k1gMnSc1
Girls	girl	k1gFnPc2
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Put	puta	k1gFnPc2
Out	Out	k1gFnSc1
The	The	k1gFnSc1
Fire	Fire	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Staying	Staying	k1gInSc1
Power	Power	k1gInSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Calling	Calling	k1gInSc1
All	All	k1gMnSc1
Girls	girl	k1gFnPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
Japonsko	Japonsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
„	„	k?
<g/>
Back	Back	k1gInSc1
Chat	chata	k1gFnPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Back	Back	k1gInSc1
Chat	chata	k1gFnPc2
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Staying	Staying	k1gInSc1
Power	Power	k1gMnSc1
<g/>
“	“	k?
The	The	k1gMnSc1
Works	Works	kA
</s>
<s>
„	„	k?
<g/>
Radio	radio	k1gNnSc1
Ga	Ga	k1gMnSc1
Ga	Ga	k1gMnSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
I	i	k8xC
Go	Go	k1gMnSc1
Crazy	Craza	k1gFnSc2
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
I	i	k9
Want	Want	k1gMnSc1
to	ten	k3xDgNnSc1
Break	break	k1gInSc4
Free	Fre	k1gMnSc2
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Machines	Machines	k1gInSc1
(	(	kIx(
<g/>
or	or	k?
'	'	kIx"
<g/>
Back	Back	k1gMnSc1
to	ten	k3xDgNnSc4
Humans	Humans	k1gInSc4
<g/>
'	'	kIx"
<g/>
)	)	kIx)
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
It	It	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
a	a	k8xC
Hard	Hardo	k1gNnPc2
Life	Life	k1gNnPc2
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Is	Is	k1gFnSc2
This	Thisa	k1gFnPc2
the	the	k?
World	World	k1gMnSc1
We	We	k1gMnSc1
Created	Created	k1gMnSc1
<g/>
…	…	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Hammer	Hammer	k1gInSc1
to	ten	k3xDgNnSc1
Fall	Fall	k1gInSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Tear	Tear	k1gMnSc1
It	It	k1gMnSc1
Up	Up	k1gMnSc1
<g/>
“	“	k?
A	a	k9
Kind	Kind	k1gMnSc1
of	of	k?
Magic	Magic	k1gMnSc1
</s>
<s>
„	„	k?
<g/>
One	One	k1gFnSc1
Vision	vision	k1gInSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Blurred	Blurred	k1gInSc1
Vision	vision	k1gInSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
A	a	k9
Kind	Kind	k1gMnSc1
of	of	k?
Magic	Magic	k1gMnSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
A	a	k8xC
Dozen	Dozen	k2eAgMnSc1d1
Red	Red	k1gMnSc1
Roses	Roses	k1gMnSc1
for	forum	k1gNnPc2
My	my	k3xPp1nPc1
Darling	Darling	k1gInSc1
<g/>
“	“	k?
<g/>
;	;	kIx,
„	„	k?
<g/>
"	"	kIx"
<g/>
Gimme	Gimm	k1gInSc5
the	the	k?
Prize	Prize	k1gFnSc1
(	(	kIx(
<g/>
Kurgan	kurgan	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Theme	Them	k1gInSc5
<g/>
)	)	kIx)
<g/>
“	“	k?
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Princes	princes	k1gInSc1
of	of	k?
the	the	k?
Universe	Universe	k1gFnSc2
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
A	a	k8xC
Dozen	Dozen	k2eAgMnSc1d1
Red	Red	k1gMnSc1
Roses	Roses	k1gMnSc1
for	forum	k1gNnPc2
My	my	k3xPp1nPc1
Darling	Darling	k1gInSc1
<g/>
“	“	k?
&	&	k?
„	„	k?
<g/>
Who	Who	k1gFnSc2
Wants	Wantsa	k1gFnPc2
to	ten	k3xDgNnSc1
Live	Live	k1gNnSc7
Forever	Forevra	k1gFnPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
Japonsko	Japonsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Gimme	Gimm	k1gMnSc5
the	the	k?
Prize	Prize	k1gFnSc1
(	(	kIx(
<g/>
Kurgan	kurgan	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Theme	Them	k1gInSc5
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Austrálie	Austrálie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Was	Was	k1gMnSc1
It	It	k1gMnSc1
All	All	k1gMnSc1
Worth	Worth	k1gMnSc1
It	It	k1gMnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
,	,	kIx,
Nizozemsko	Nizozemsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
One	One	k1gMnSc1
Year	Year	k1gMnSc1
of	of	k?
Love	lov	k1gInSc5
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Gimme	Gimm	k1gInSc5
the	the	k?
Prize	Prize	k1gFnSc1
(	(	kIx(
<g/>
Kurgan	kurgan	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Theme	Them	k1gInSc5
<g/>
)	)	kIx)
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Friends	Friends	k1gInSc1
Will	Will	k1gMnSc1
Be	Be	k1gFnPc2
Friends	Friends	k1gInSc4
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Seven	Seven	k2eAgInSc4d1
Seas	Seas	k1gInSc4
of	of	k?
Rhye	Rhye	k1gInSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Pain	Pain	k1gMnSc1
Is	Is	k1gMnSc2
So	So	kA
Close	Clos	k1gMnSc2
to	ten	k3xDgNnSc1
Pleasure	Pleasur	k1gMnSc5
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Lose	los	k1gInSc6
Your	Your	k1gInSc1
Head	Head	k1gInSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Who	Who	k1gFnSc1
Wants	Wants	k1gInSc1
to	ten	k3xDgNnSc4
Live	Live	k1gNnSc4
Forever	Forever	k1gMnSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Killer	Killer	k1gInSc1
Queen	Queen	k1gInSc1
<g/>
“	“	k?
The	The	k1gMnSc2
Miracle	Miracl	k1gMnSc2
</s>
<s>
„	„	k?
<g/>
I	i	k9
Want	Want	k1gMnSc1
It	It	k1gMnSc1
All	All	k1gMnSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Hang	Hang	k1gInSc1
on	on	k3xPp3gMnSc1
in	in	k?
There	Ther	k1gMnSc5
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Breakthru	Breakthra	k1gFnSc4
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Stealin	Stealin	k1gInSc1
<g/>
'	'	kIx"
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
The	The	k1gMnSc1
Invisible	Invisible	k1gMnSc1
Man	Man	k1gMnSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Hijack	Hijack	k1gInSc1
My	my	k3xPp1nPc1
Heart	Heart	k1gInSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Scandal	Scandal	k1gFnSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
My	my	k3xPp1nPc1
Life	Lifus	k1gInSc5
Has	hasit	k5eAaImRp2nS
Been	Been	k1gMnSc1
Saved	Saved	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
The	The	k1gFnSc1
Miracle	Miracle	k1gFnSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Stone	ston	k1gInSc5
Cold	Cold	k1gInSc4
Crazy	Craza	k1gFnPc4
(	(	kIx(
<g/>
živě	živě	k6eAd1
<g/>
)	)	kIx)
<g/>
“	“	k?
Innuendo	Innuendo	k6eAd1
</s>
<s>
„	„	k?
<g/>
Innuendo	Innuendo	k6eAd1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Bijou	bijou	k1gNnSc2
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
I	I	kA
<g/>
'	'	kIx"
<g/>
m	m	kA
Going	Going	k1gMnSc1
Slightly	Slightly	k1gMnSc1
Mad	Mad	k1gMnSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Lost	Lost	k1gInSc1
Opportunity	Opportunita	k1gFnSc2
<g/>
;	;	kIx,
The	The	k1gMnSc1
Hitman	Hitman	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Headlong	Headlong	k1gInSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Mad	Mad	k1gFnSc1
the	the	k?
Swine	Swin	k1gInSc5
<g/>
“	“	k?
<g/>
;	;	kIx,
„	„	k?
<g/>
All	All	k1gMnSc1
God	God	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
People	People	k1gFnSc7
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
I	i	k9
Can	Can	k1gFnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Live	Live	k1gInSc1
with	with	k1gMnSc1
You	You	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
These	these	k1gFnSc1
Are	ar	k1gInSc5
the	the	k?
Days	Daysa	k1gFnPc2
of	of	k?
Our	Our	k1gMnSc1
Lives	Lives	k1gMnSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Bijou	bijou	k1gNnSc2
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
The	The	k1gMnSc1
Show	show	k1gFnSc2
Must	Must	k1gMnSc1
Go	Go	k1gMnSc1
On	on	k3xPp3gMnSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Keep	Keep	k1gMnSc1
Yourself	Yourself	k1gMnSc1
Alive	Aliev	k1gFnSc2
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Stone	ston	k1gInSc5
Cold	Cold	k1gInSc1
Crazy	Craz	k1gInPc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
We	We	k1gFnSc1
Are	ar	k1gInSc5
the	the	k?
Champions	Champions	k1gInSc1
<g/>
“	“	k?
<g/>
;	;	kIx,
„	„	k?
<g/>
We	We	k1gFnSc2
Will	Willa	k1gFnPc2
Rock	rock	k1gInSc1
You	You	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Bohemian	bohemian	k1gInSc1
Rhapsody	Rhapsoda	k1gFnSc2
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
These	these	k1gFnSc1
Are	ar	k1gInSc5
the	the	k?
Days	Daysa	k1gFnPc2
of	of	k?
Our	Our	k1gMnSc1
Lives	Lives	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
double	double	k1gInSc1
A	a	k8xC
strana	strana	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Delilah	Delilah	k1gInSc1
(	(	kIx(
<g/>
píseň	píseň	k1gFnSc1
<g/>
,	,	kIx,
Queen	Queen	k1gInSc1
<g/>
)	)	kIx)
<g/>
“	“	k?
Made	Made	k1gInSc1
in	in	k?
Heaven	Heaven	k2eAgInSc1d1
</s>
<s>
„	„	k?
<g/>
Made	Made	k1gInSc1
in	in	k?
Heaven	Heaven	k2eAgInSc1d1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Heaven	Heavno	k1gNnPc2
for	forum	k1gNnPc2
Everyone	Everyon	k1gInSc5
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
It	It	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
a	a	k8xC
Beautiful	Beautiful	k1gInSc1
Day	Day	k1gFnSc2
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
A	a	k9
Winter	Winter	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Tale	Tale	k1gInSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Thank	Thank	k1gMnSc1
God	God	k1gMnSc1
It	It	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Christmas	Christmas	k1gInSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
I	i	k9
Was	Was	k1gMnSc1
Born	Born	k1gMnSc1
to	ten	k3xDgNnSc4
Love	lov	k1gInSc5
You	You	k1gMnPc2
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Too	Too	k1gFnSc1
Much	moucha	k1gFnPc2
Love	lov	k1gInSc5
Will	Will	k1gMnSc1
Kill	Killa	k1gFnPc2
You	You	k1gMnSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
I	I	kA
<g/>
'	'	kIx"
<g/>
m	m	kA
Scared	Scared	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Let	let	k1gInSc1
Me	Me	k1gMnSc2
Live	Liv	k1gMnSc2
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Fat	fatum	k1gNnPc2
Bottomed	Bottomed	k1gInSc1
Girls	girl	k1gFnPc2
<g/>
“	“	k?
<g/>
;	;	kIx,
„	„	k?
<g/>
Bicycle	Bicycle	k1gInSc1
Race	Race	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
You	You	k1gMnSc1
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Fool	Fool	k1gMnSc1
Me	Me	k1gMnSc1
<g/>
“	“	k?
/	/	kIx~
Queen	Queen	k2eAgInSc1d1
Rocks	Rocks	k1gInSc1
</s>
<s>
„	„	k?
<g/>
No-One	No-On	k1gMnSc5
but	but	k?
You	You	k1gFnPc1
(	(	kIx(
<g/>
Only	Onla	k1gFnPc1
the	the	k?
Good	Good	k1gMnSc1
Die	Die	k1gMnSc1
Young	Young	k1gMnSc1
<g/>
)	)	kIx)
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Tie	Tie	k1gMnSc1
Your	Your	k1gMnSc1
Mother	Mothra	k1gFnPc2
Down	Down	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
double	double	k1gInSc1
A	a	k8xC
strana	strana	k1gFnSc1
<g/>
)	)	kIx)
Queen	Queen	k2eAgInSc1d1
Forever	Forever	k1gInSc1
</s>
<s>
„	„	k?
<g/>
Let	let	k1gInSc1
Me	Me	k1gMnSc1
in	in	k?
Your	Your	k1gInSc1
Heart	Heart	k1gInSc1
Again	Again	k1gInSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Love	lov	k1gInSc5
Kills	Kills	k1gInSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
There	Ther	k1gMnSc5
Must	Must	k1gInSc1
Be	Be	k1gMnSc1
More	mor	k1gInSc5
to	ten	k3xDgNnSc1
Life	Life	k1gNnSc7
Than	Thana	k1gFnPc2
This	This	k1gInSc4
<g/>
“	“	k?
Ostatní	ostatní	k2eAgInPc4d1
singly	singl	k1gInPc4
</s>
<s>
„	„	k?
<g/>
Thank	Thank	k1gMnSc1
God	God	k1gMnSc1
It	It	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Christmas	Christmas	k1gInSc1
<g/>
“	“	k?
/	/	kIx~
„	„	k?
<g/>
Man	Man	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
Prowl	Prowl	k1gInSc1
<g/>
“	“	k?
<g/>
;	;	kIx,
„	„	k?
<g/>
Keep	Keep	k1gInSc1
Passing	Passing	k1gInSc1
the	the	k?
Open	Open	k1gInSc1
Windows	Windows	kA
<g/>
“	“	k?
↑	↑	k?
za	za	k7c7
lomítkem	lomítko	k1gNnSc7
jsem	být	k5eAaImIp1nS
uvedeny	uveden	k2eAgFnPc4d1
i	i	k8xC
B	B	kA
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
případně	případně	k6eAd1
double	double	k2eAgInSc4d1
A	A	kA
strany	strana	k1gFnSc2
<g/>
)	)	kIx)
singlů	singl	k1gInPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Queen	Queen	k1gInSc1
</s>
