<p>
<s>
KTM	KTM	kA	KTM
Sportmotorcykl	Sportmotorcykl	k1gMnSc1	Sportmotorcykl
AG	AG	kA	AG
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
KTM	KTM	kA	KTM
je	být	k5eAaImIp3nS	být
rakouská	rakouský	k2eAgFnSc1d1	rakouská
motocyklová	motocyklový	k2eAgFnSc1d1	motocyklová
firma	firma	k1gFnSc1	firma
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
profiluje	profilovat	k5eAaImIp3nS	profilovat
především	především	k9	především
jako	jako	k8xC	jako
výrobce	výrobce	k1gMnPc4	výrobce
offroadových	offroadový	k2eAgInPc2d1	offroadový
(	(	kIx(	(
<g/>
terénních	terénní	k2eAgInPc2d1	terénní
<g/>
)	)	kIx)	)
motocyklů	motocykl	k1gInPc2	motocykl
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
či	či	k8xC	či
stále	stále	k6eAd1	stále
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
i	i	k9	i
silniční	silniční	k2eAgInPc1d1	silniční
motocykly	motocykl	k1gInPc1	motocykl
<g/>
,	,	kIx,	,
skútry	skútr	k1gInPc1	skútr
<g/>
,	,	kIx,	,
mopedy	moped	k1gInPc1	moped
<g/>
,	,	kIx,	,
jízdní	jízdní	k2eAgNnPc1d1	jízdní
kola	kolo	k1gNnPc1	kolo
<g/>
,	,	kIx,	,
čtyřkolky	čtyřkolka	k1gFnPc1	čtyřkolka
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgNnSc1d1	sportovní
auto	auto	k1gNnSc1	auto
a	a	k8xC	a
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
terénní	terénní	k2eAgNnPc4d1	terénní
kola	kolo	k1gNnPc4	kolo
na	na	k7c4	na
elektrický	elektrický	k2eAgInSc4d1	elektrický
pohon	pohon	k1gInSc4	pohon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
motocrosu	motocros	k1gInSc6	motocros
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
motocykly	motocykl	k1gInPc1	motocykl
hlavně	hlavně	k9	hlavně
této	tento	k3xDgFnSc2	tento
značky	značka	k1gFnSc2	značka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Firma	firma	k1gFnSc1	firma
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
motocykly	motocykl	k1gInPc4	motocykl
od	od	k7c2	od
objemu	objem	k1gInSc2	objem
50	[number]	k4	50
cm3	cm3	k4	cm3
až	až	k9	až
do	do	k7c2	do
1301	[number]	k4	1301
cm3	cm3	k4	cm3
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gNnSc7	její
mottem	motto	k1gNnSc7	motto
je	být	k5eAaImIp3nS	být
READY	ready	k0	ready
TO	ten	k3xDgNnSc4	ten
RACE	RACE	kA	RACE
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gInPc1	její
počátky	počátek	k1gInPc1	počátek
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
vystopovat	vystopovat	k5eAaPmF	vystopovat
až	až	k9	až
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Firma	firma	k1gFnSc1	firma
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
jako	jako	k8xC	jako
opravárenská	opravárenský	k2eAgFnSc1d1	opravárenská
dílna	dílna	k1gFnSc1	dílna
inženýrem	inženýr	k1gMnSc7	inženýr
Hansem	Hans	k1gMnSc7	Hans
Trunkenpolzem	Trunkenpolz	k1gMnSc7	Trunkenpolz
v	v	k7c6	v
hornorakouském	hornorakouský	k2eAgInSc6d1	hornorakouský
Mattighofenu	Mattighofen	k1gInSc6	Mattighofen
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
začala	začít	k5eAaPmAgFnS	začít
s	s	k7c7	s
prodejem	prodej	k1gInSc7	prodej
motocyklů	motocykl	k1gInPc2	motocykl
DKW	DKW	kA	DKW
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
prototypu	prototyp	k1gInSc6	prototyp
vlastního	vlastní	k2eAgInSc2d1	vlastní
motocyklu	motocykl	k1gInSc2	motocykl
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
zahájila	zahájit	k5eAaPmAgFnS	zahájit
výrobu	výroba	k1gFnSc4	výroba
série	série	k1gFnSc2	série
KTM	KTM	kA	KTM
R	R	kA	R
100	[number]	k4	100
a	a	k8xC	a
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
se	se	k3xPyFc4	se
na	na	k7c4	na
Kraftfahrzeug	Kraftfahrzeug	k1gInSc4	Kraftfahrzeug
Trunkenpolz	Trunkenpolz	k1gInSc4	Trunkenpolz
Mattighofen	Mattighofna	k1gFnPc2	Mattighofna
<g/>
.	.	kIx.	.
</s>
<s>
Jejích	její	k3xOp3gMnPc2	její
20	[number]	k4	20
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
sestavovalo	sestavovat	k5eAaImAgNnS	sestavovat
tři	tři	k4xCgInPc4	tři
motocykly	motocykl	k1gInPc4	motocykl
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
významným	významný	k2eAgMnSc7d1	významný
akcionářem	akcionář	k1gMnSc7	akcionář
společnosti	společnost	k1gFnSc2	společnost
podnikatel	podnikatel	k1gMnSc1	podnikatel
Ernest	Ernest	k1gMnSc1	Ernest
Kronreif	Kronreif	k1gMnSc1	Kronreif
a	a	k8xC	a
firma	firma	k1gFnSc1	firma
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Kronreif	Kronreif	k1gInSc4	Kronreif
&	&	k?	&
Trunkenpolz	Trunkenpolz	k1gInSc1	Trunkenpolz
Mattighofen	Mattighofen	k1gInSc1	Mattighofen
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
KTM	KTM	kA	KTM
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
první	první	k4xOgInSc4	první
sportovní	sportovní	k2eAgInSc4d1	sportovní
motocykl	motocykl	k1gInSc4	motocykl
<g/>
,	,	kIx,	,
Trophy	Tropha	k1gFnPc1	Tropha
125	[number]	k4	125
<g/>
cc	cc	k?	cc
<g/>
,,	,,	k?	,,
se	s	k7c7	s
kterým	který	k3yIgMnSc7	který
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
na	na	k7c4	na
americký	americký	k2eAgInSc4d1	americký
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
zahájila	zahájit	k5eAaPmAgFnS	zahájit
výrobu	výroba	k1gFnSc4	výroba
skútrů	skútr	k1gInPc2	skútr
a	a	k8xC	a
mopedů	moped	k1gInPc2	moped
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
terénní	terénní	k2eAgInPc4d1	terénní
motocykly	motocykl	k1gInPc4	motocykl
společnost	společnost	k1gFnSc1	společnost
využívala	využívat	k5eAaImAgFnS	využívat
motory	motor	k1gInPc4	motor
Sachs	Sachsa	k1gFnPc2	Sachsa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
produkuje	produkovat	k5eAaImIp3nS	produkovat
motory	motor	k1gInPc4	motor
vlastní	vlastní	k2eAgFnSc2d1	vlastní
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Ernsta	Ernst	k1gMnSc2	Ernst
Kronreifa	Kronreif	k1gMnSc2	Kronreif
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
i	i	k8xC	i
zakladatel	zakladatel	k1gMnSc1	zakladatel
Trunkenpolz	Trunkenpolz	k1gMnSc1	Trunkenpolz
<g/>
,	,	kIx,	,
firmu	firma	k1gFnSc4	firma
převzal	převzít	k5eAaPmAgMnS	převzít
jeho	jeho	k3xOp3gFnSc4	jeho
syn	syn	k1gMnSc1	syn
Erich	Erich	k1gMnSc1	Erich
Trunkenpolz	Trunkenpolz	k1gMnSc1	Trunkenpolz
a	a	k8xC	a
jméno	jméno	k1gNnSc4	jméno
firmy	firma	k1gFnSc2	firma
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c4	na
původní	původní	k2eAgInSc4d1	původní
Kraftfahrzeug	Kraftfahrzeug	k1gInSc4	Kraftfahrzeug
Trunkenpolz	Trunkenpolza	k1gFnPc2	Trunkenpolza
Mattighofen	Mattighofna	k1gFnPc2	Mattighofna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
krize	krize	k1gFnSc2	krize
motocyklového	motocyklový	k2eAgInSc2d1	motocyklový
průmyslu	průmysl	k1gInSc2	průmysl
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
KTM	KTM	kA	KTM
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
bankrot	bankrot	k1gInSc4	bankrot
a	a	k8xC	a
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
se	se	k3xPyFc4	se
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
nezávislé	závislý	k2eNgFnPc4d1	nezávislá
firmy	firma	k1gFnPc4	firma
zabývající	zabývající	k2eAgFnPc4d1	zabývající
se	se	k3xPyFc4	se
radiátory	radiátor	k1gInPc1	radiátor
<g/>
,	,	kIx,	,
motocykly	motocykl	k1gInPc1	motocykl
<g/>
,	,	kIx,	,
jízdními	jízdní	k2eAgNnPc7d1	jízdní
koly	kolo	k1gNnPc7	kolo
a	a	k8xC	a
nástroji	nástroj	k1gInPc7	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
tedy	tedy	k9	tedy
zahájila	zahájit	k5eAaPmAgFnS	zahájit
činnost	činnost	k1gFnSc1	činnost
společnost	společnost	k1gFnSc1	společnost
KTM	KTM	kA	KTM
Sportmotorcycle	Sportmotorcycle	k1gFnSc1	Sportmotorcycle
GmbH	GmbH	k1gFnSc1	GmbH
<g/>
,	,	kIx,	,
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
</s>
</p>
<p>
<s>
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c6	na
KTM-Sportsmotorcycle	KTM-Sportsmotorcycla	k1gFnSc6	KTM-Sportsmotorcycla
AG	AG	kA	AG
<g/>
,	,	kIx,	,
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
koupila	koupit	k5eAaPmAgFnS	koupit
firmy	firma	k1gFnPc1	firma
Husaberg	Husaberg	k1gMnSc1	Husaberg
<g/>
,	,	kIx,	,
výrobce	výrobce	k1gMnSc1	výrobce
sportovních	sportovní	k2eAgInPc2d1	sportovní
motocyklů	motocykl	k1gInPc2	motocykl
<g/>
,	,	kIx,	,
a	a	k8xC	a
holandskou	holandský	k2eAgFnSc7d1	holandská
White	Whit	k1gInSc5	Whit
Power	Power	k1gMnSc1	Power
Suspension	Suspension	k1gInSc1	Suspension
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
Shana	Shan	k1gMnSc2	Shan
Kinga	King	k1gMnSc2	King
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
motokrosu	motokros	k1gInSc6	motokros
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
500	[number]	k4	500
<g/>
cc	cc	k?	cc
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
úspěchů	úspěch	k1gInPc2	úspěch
pomohly	pomoct	k5eAaPmAgFnP	pomoct
společnosti	společnost	k1gFnPc1	společnost
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
nově	nově	k6eAd1	nově
vystavěné	vystavěný	k2eAgFnSc2d1	vystavěná
továrny	továrna	k1gFnSc2	továrna
v	v	k7c6	v
Mattighofenu	Mattighofen	k1gInSc6	Mattighofen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KTM	KTM	kA	KTM
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
i	i	k9	i
do	do	k7c2	do
nového	nový	k2eAgNnSc2d1	nové
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
Moto	moto	k1gNnSc4	moto
<g/>
3	[number]	k4	3
se	s	k7c7	s
soutěžním	soutěžní	k2eAgInSc7d1	soutěžní
motocyklem	motocykl	k1gInSc7	motocykl
vyrobeným	vyrobený	k2eAgInSc7d1	vyrobený
čistě	čistě	k6eAd1	čistě
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
hned	hned	k6eAd1	hned
poprvé	poprvé	k6eAd1	poprvé
Sandro	Sandra	k1gFnSc5	Sandra
Cortese	Cortesa	k1gFnSc6	Cortesa
firmě	firma	k1gFnSc3	firma
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
titul	titul	k1gInSc1	titul
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
to	ten	k3xDgNnSc4	ten
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
KTM	KTM	kA	KTM
koupila	koupit	k5eAaPmAgFnS	koupit
motocyklovou	motocyklový	k2eAgFnSc4d1	motocyklová
značku	značka	k1gFnSc4	značka
Husqvarna	Husqvarno	k1gNnSc2	Husqvarno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
KTM	KTM	kA	KTM
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
