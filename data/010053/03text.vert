<p>
<s>
Práčov	Práčov	k1gInSc1	Práčov
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
městyse	městys	k1gInSc2	městys
Přídolí	Přídolí	k1gNnSc2	Přídolí
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
asi	asi	k9	asi
3	[number]	k4	3
km	km	kA	km
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Přídolí	Přídolí	k1gNnSc2	Přídolí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
evidováno	evidovat	k5eAaImNgNnS	evidovat
17	[number]	k4	17
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Práčov	Práčov	k1gInSc1	Práčov
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Zátes	zátes	k1gInSc1	zátes
o	o	k7c6	o
výměře	výměra	k1gFnSc6	výměra
7,35	[number]	k4	7,35
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
vesnici	vesnice	k1gFnSc6	vesnice
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1375	[number]	k4	1375
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Boží	boží	k2eAgFnSc1d1	boží
muka	muka	k1gFnSc1	muka
stávala	stávat	k5eAaImAgFnS	stávat
u	u	k7c2	u
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
dvorů	dvůr	k1gInPc2	dvůr
v	v	k7c6	v
Drahoslavicích-Práčově	Drahoslavicích-Práčův	k2eAgFnSc6d1	Drahoslavicích-Práčův
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
přenesena	přenesen	k2eAgFnSc1d1	přenesena
k	k	k7c3	k
jezuitskému	jezuitský	k2eAgInSc3d1	jezuitský
semináři	seminář	k1gInSc3	seminář
v	v	k7c6	v
Horní	horní	k2eAgFnSc6d1	horní
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
katastr	katastr	k1gInSc1	katastr
<g/>
:	:	kIx,	:
<g/>
735850	[number]	k4	735850
<g/>
)	)	kIx)	)
</s>
</p>
