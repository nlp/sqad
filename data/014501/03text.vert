<s>
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
</s>
<s>
Australian	Australian	k1gMnSc1
OpenThe	OpenTh	k1gFnSc2
Grand	grand	k1gMnSc1
Slam	sláma	k1gFnPc2
of	of	k?
Asia	Asia	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Pacific	Pacific	k1gMnSc1
logo	logo	k1gNnSc4
Rod	rod	k1gInSc4
Laver	lavra	k1gFnPc2
Arena	Aren	k1gInSc2
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Založeno	založen	k2eAgNnSc1d1
</s>
<s>
1905	#num#	k4
Místo	místo	k7c2
</s>
<s>
Melbourne	Melbourne	k1gNnSc1
<g/>
,	,	kIx,
Victoria	Victorium	k1gNnSc2
Austrálie	Austrálie	k1gFnSc2
Austrálie	Austrálie	k1gFnSc2
Dějiště	dějiště	k1gNnSc2
</s>
<s>
Melbourne	Melbourne	k1gNnSc1
Park	park	k1gInSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
37	#num#	k4
<g/>
°	°	k?
<g/>
49	#num#	k4
<g/>
′	′	k?
<g/>
18	#num#	k4
<g/>
″	″	k?
j.	j.	k?
š.	š.	k?
<g/>
,	,	kIx,
144	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
<g/>
42	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Povrch	povrch	k1gInSc1
</s>
<s>
tráva	tráva	k1gFnSc1
/	/	kIx~
venku	venku	k6eAd1
(	(	kIx(
<g/>
1905	#num#	k4
<g/>
–	–	k?
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
<g/>
tvrdý	tvrdý	k2eAgMnSc1d1
–	–	k?
Rebound	Rebound	k1gMnSc1
Ace	Ace	k1gMnSc1
/	/	kIx~
venku	venku	k6eAd1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
tvrdý	tvrdý	k2eAgInSc4d1
–	–	k?
Plexicushion	Plexicushion	k1gInSc4
/	/	kIx~
venku	venku	k6eAd1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
tvrdý	tvrdý	k2eAgInSc4d1
–	–	k?
GreenSet	GreenSet	k1gInSc4
/	/	kIx~
venku	venku	k6eAd1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
–	–	k?
<g/>
)	)	kIx)
<g/>
zatahovací	zatahovací	k2eAgFnSc1d1
střecha	střecha	k1gFnSc1
ve	v	k7c6
třech	tři	k4xCgFnPc6
arénách	aréna	k1gFnPc6
Dotace	dotace	k1gFnSc2
</s>
<s>
80	#num#	k4
000	#num#	k4
000	#num#	k4
AU	au	k0
<g/>
$	$	kIx~
Období	období	k1gNnSc1
</s>
<s>
leden	leden	k1gInSc1
Poznámka	poznámka	k1gFnSc1
</s>
<s>
jediný	jediný	k2eAgInSc1d1
grandslam	grandslam	k1gInSc1
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
polokouli	polokoule	k1gFnSc6
<g/>
,	,	kIx,
<g/>
nejnavštěvovanější	navštěvovaný	k2eAgNnSc1d3
z	z	k7c2
grandslamů	grandslam	k1gInPc2
<g/>
,	,	kIx,
<g/>
první	první	k4xOgInSc1
grandslam	grandslam	k1gInSc1
hraný	hraný	k2eAgInSc1d1
pod	pod	k7c7
střechou	střecha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Soutěže	soutěž	k1gFnPc1
mužů	muž	k1gMnPc2
</s>
<s>
128	#num#	k4
dvouhra	dvouhra	k1gFnSc1
(	(	kIx(
<g/>
128	#num#	k4
kval	kvala	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
/	/	kIx~
64	#num#	k4
čtyřhra	čtyřhra	k1gFnSc1
Soutěže	soutěž	k1gFnSc2
žen	žena	k1gFnPc2
</s>
<s>
128	#num#	k4
dvouhra	dvouhra	k1gFnSc1
(	(	kIx(
<g/>
128	#num#	k4
kval	kvala	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
/	/	kIx~
64	#num#	k4
čtyřhra	čtyřhra	k1gFnSc1
Smíšená	smíšený	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
</s>
<s>
64	#num#	k4
hráčů	hráč	k1gMnPc2
(	(	kIx(
<g/>
32	#num#	k4
párů	pár	k1gInPc2
<g/>
)	)	kIx)
Vítězové	vítěz	k1gMnPc1
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
2021	#num#	k4
Dvouhra	dvouhra	k1gFnSc1
mužů	muž	k1gMnPc2
</s>
<s>
Novak	Novak	k6eAd1
Djoković	Djoković	k1gFnSc4
Dvouhra	dvouhra	k1gFnSc1
žen	žena	k1gFnPc2
</s>
<s>
Naomi	Nao	k1gFnPc7
Ósakaová	Ósakaový	k2eAgFnSc1d1
Čtyřhra	čtyřhra	k1gFnSc1
mužů	muž	k1gMnPc2
</s>
<s>
Ivan	Ivan	k1gMnSc1
Dodig	Dodig	k1gMnSc1
Filip	Filip	k1gMnSc1
Polášek	Polášek	k1gMnSc1
Čtyřhra	čtyřhra	k1gFnSc1
žen	žena	k1gFnPc2
</s>
<s>
Elise	elise	k1gFnSc1
Mertensová	Mertensový	k2eAgFnSc1d1
Aryna	Aryen	k2eAgFnSc1d1
Sabalenková	Sabalenkový	k2eAgFnSc1d1
Smíšená	smíšený	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
Barbora	Barbora	k1gFnSc1
Krejčíková	Krejčíková	k1gFnSc1
Rajeev	Rajeev	k1gFnSc1
Ram	Ram	k1gFnSc1
Grand	grand	k1gMnSc1
Slam	slam	k1gInSc1
</s>
<s>
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
•	•	k?
French	French	k1gInSc1
Open	Open	k1gMnSc1
Wimbledon	Wimbledon	k1gInSc1
•	•	k?
US	US	kA
Open	Open	k1gInSc1
www.australianopen.com	www.australianopen.com	k1gInSc1
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Serena	Seren	k2eAgFnSc1d1
Williamsová	Williamsová	k1gFnSc1
vyhrála	vyhrát	k5eAaPmAgFnS
sedm	sedm	k4xCc4
titulů	titul	k1gInPc2
</s>
<s>
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Grand	grand	k1gMnSc1
Slam	sláma	k1gFnPc2
of	of	k?
Asia	Asia	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Pacific	Pacific	k1gMnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
ze	z	k7c2
čtyř	čtyři	k4xCgInPc2
tenisových	tenisový	k2eAgInPc2d1
turnajů	turnaj	k1gInPc2
nejvyšší	vysoký	k2eAgFnSc2d3
kategorie	kategorie	k1gFnSc2
–	–	k?
Grand	grand	k1gMnSc1
Slamu	slam	k1gInSc2
<g/>
,	,	kIx,
každoročně	každoročně	k6eAd1
hraný	hraný	k2eAgInSc1d1
v	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
ledna	leden	k1gInSc2
a	a	k8xC
jediný	jediný	k2eAgMnSc1d1
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
polokouli	polokoule	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úvodní	úvodní	k2eAgInSc1d1
ročník	ročník	k1gInSc1
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
v	v	k7c6
listopadu	listopad	k1gInSc6
1905	#num#	k4
jako	jako	k8xS,k8xC
amatérské	amatérský	k2eAgNnSc4d1
mezinárodní	mezinárodní	k2eAgNnSc4d1
mistrovství	mistrovství	k1gNnSc4
Australásie	Australásie	k1gFnSc2
a	a	k8xC
do	do	k7c2
roku	rok	k1gInSc2
1927	#num#	k4
nesl	nést	k5eAaImAgInS
název	název	k1gInSc1
Australasian	Australasiany	k1gInPc2
Championships	Championshipsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
až	až	k9
do	do	k7c2
počátku	počátek	k1gInSc2
otevřené	otevřený	k2eAgFnSc2d1
éry	éra	k1gFnSc2
tenisu	tenis	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
se	se	k3xPyFc4
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
Australian	Australian	k1gMnSc1
Championships	Championshipsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1969	#num#	k4
byl	být	k5eAaImAgInS
turnaj	turnaj	k1gInSc1
poprvé	poprvé	k6eAd1
otevřen	otevřít	k5eAaPmNgInS
profesionálním	profesionální	k2eAgMnPc3d1
tenistům	tenista	k1gMnPc3
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
odráží	odrážet	k5eAaImIp3nS
část	část	k1gFnSc1
„	„	k?
<g/>
Open	Open	k1gInSc1
<g/>
“	“	k?
v	v	k7c6
názvu	název	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1972	#num#	k4
je	být	k5eAaImIp3nS
dějištěm	dějiště	k1gNnSc7
grandslamu	grandslam	k1gInSc2
opět	opět	k6eAd1
Melbourne	Melbourne	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
původního	původní	k2eAgInSc2d1
areálu	areál	k1gInSc2
Kooyong	Kooyong	k1gMnSc1
Lawn	Lawn	k1gMnSc1
Tennis	Tennis	k1gFnPc2
Club	club	k1gInSc4
s	s	k7c7
travnatými	travnatý	k2eAgInPc7d1
dvorci	dvorec	k1gInPc7
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
událost	událost	k1gFnSc1
přemístila	přemístit	k5eAaPmAgFnS
do	do	k7c2
dnešního	dnešní	k2eAgNnSc2d1
Melbourne	Melbourne	k1gNnSc2
Parku	park	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgMnSc6
jsou	být	k5eAaImIp3nP
kurty	kurta	k1gFnPc4
se	s	k7c7
středně	středně	k6eAd1
tvrdým	tvrdý	k2eAgInSc7d1
akrylátovým	akrylátový	k2eAgInSc7d1
povrchem	povrch	k1gInSc7
s	s	k7c7
lepší	dobrý	k2eAgFnSc7d2
konzistencí	konzistence	k1gFnSc7
a	a	k8xC
nižším	nízký	k2eAgNnSc7d2
zadržováním	zadržování	k1gNnSc7
tepla	teplo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
roky	rok	k1gInPc4
1988	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
se	se	k3xPyFc4
hrálo	hrát	k5eAaImAgNnS
na	na	k7c6
povrchu	povrch	k1gInSc6
Rebound	Rebound	k1gMnSc1
Ace	Ace	k1gMnSc1
<g/>
,	,	kIx,
poté	poté	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
letech	let	k1gInPc6
2008	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
používal	používat	k5eAaImAgInS
Plexicushion	Plexicushion	k1gInSc4
Prestige	Prestig	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2020	#num#	k4
se	se	k3xPyFc4
dodavatelem	dodavatel	k1gMnSc7
povrchu	povrch	k1gInSc2
na	na	k7c6
australských	australský	k2eAgInPc6d1
turnajích	turnaj	k1gInPc6
včetně	včetně	k7c2
Australian	Australiany	k1gInPc2
Open	Open	k1gInSc4
Series	Series	k1gInSc4
stala	stát	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
GreenSet	GreenSeta	k1gFnPc2
Worldwide	Worldwid	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
a	a	k8xC
Serena	Serena	k1gFnSc1
Williamsová	Williamsová	k1gFnSc1
jsou	být	k5eAaImIp3nP
jedinými	jediný	k2eAgMnPc7d1
hráči	hráč	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
zvítězili	zvítězit	k5eAaPmAgMnP
na	na	k7c6
površích	povrch	k1gInPc6
Rebound	Rebound	k1gMnSc1
Ace	Ace	k1gMnSc1
i	i	k9
Plexicushion	Plexicushion	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Tři	tři	k4xCgInPc1
hlavní	hlavní	k2eAgInPc1d1
dvorce	dvorec	k1gInPc1
disponují	disponovat	k5eAaBmIp3nP
zatahovací	zatahovací	k2eAgFnSc7d1
střechou	střecha	k1gFnSc7
a	a	k8xC
lze	lze	k6eAd1
je	být	k5eAaImIp3nS
využít	využít	k5eAaPmF
při	při	k7c6
dešti	dešť	k1gInSc6
či	či	k8xC
vysokých	vysoký	k2eAgFnPc6d1
teplotách	teplota	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
centrálnímu	centrální	k2eAgInSc3d1
kurtu	kurt	k1gInSc3
Rod	rod	k1gInSc4
Laver	lavra	k1gFnPc2
Arena	Aren	k1gInSc2
a	a	k8xC
druhému	druhý	k4xOgMnSc3
největšímu	veliký	k2eAgInSc3d3
John	John	k1gMnSc1
Cain	Cain	k1gInSc4
Arena	Areno	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
přiřadila	přiřadit	k5eAaPmAgFnS
i	i	k9
Margaret	Margareta	k1gFnPc2
Court	Court	k1gInSc1
Arena	Aren	k1gInSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
australský	australský	k2eAgMnSc1d1
grandslam	grandslam	k6eAd1
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
z	z	k7c2
„	„	k?
<g/>
velké	velký	k2eAgFnSc2d1
čtyřky	čtyřka	k1gFnSc2
<g/>
“	“	k?
se	s	k7c7
třemi	tři	k4xCgInPc7
takto	takto	k6eAd1
upravenými	upravený	k2eAgInPc7d1
stadiony	stadion	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
pojízdnou	pojízdný	k2eAgFnSc4d1
střechu	střecha	k1gFnSc4
získal	získat	k5eAaPmAgInS
centrkurt	centrkurt	k1gInSc1
ve	v	k7c6
Wimbledonu	Wimbledon	k1gInSc6
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
představoval	představovat	k5eAaImAgMnS
jediný	jediný	k2eAgMnSc1d1
major	major	k1gMnSc1
s	s	k7c7
možností	možnost	k1gFnSc7
halových	halový	k2eAgInPc2d1
zápasů	zápas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
na	na	k7c6
dalších	další	k2eAgInPc6d1
grandslamech	grandslam	k1gInPc6
zahrnuje	zahrnovat	k5eAaImIp3nS
program	program	k1gInSc1
soutěže	soutěž	k1gFnSc2
mužské	mužský	k2eAgFnSc2d1
i	i	k8xC
ženské	ženský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
<g/>
,	,	kIx,
mužskou	mužský	k2eAgFnSc4d1
<g/>
,	,	kIx,
ženskou	ženský	k2eAgFnSc4d1
a	a	k8xC
smíšenou	smíšený	k2eAgFnSc4d1
čtyřhru	čtyřhra	k1gFnSc4
<g/>
,	,	kIx,
soutěže	soutěž	k1gFnPc1
juniorů	junior	k1gMnPc2
<g/>
,	,	kIx,
legend	legenda	k1gFnPc2
a	a	k8xC
vozíčkářů	vozíčkář	k1gMnPc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k6eAd1
i	i	k8xC
exhibici	exhibice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
počest	počest	k1gFnSc4
australského	australský	k2eAgMnSc2d1
tenisty	tenista	k1gMnSc2
a	a	k8xC
funkcionáře	funkcionář	k1gMnSc2
Normana	Norman	k1gMnSc2
Brookese	Brookese	k1gFnSc2
byl	být	k5eAaImAgInS
po	po	k7c6
něm	on	k3xPp3gInSc6
pojmenován	pojmenovat	k5eAaPmNgInS
pohár	pohár	k1gInSc1
pro	pro	k7c4
vítěze	vítěz	k1gMnPc4
mužské	mužský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
–	–	k?
„	„	k?
<g/>
Norman	Norman	k1gMnSc1
Brookes	Brookes	k1gMnSc1
Challenge	Challenge	k1gFnPc2
Cup	cup	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australian	Australiany	k1gInPc2
Open	Open	k1gInSc1
2019	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
prvním	první	k4xOgInSc7
grandslamem	grandslam	k1gInSc7
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
hrál	hrát	k5eAaImAgMnS
supertiebreak	supertiebreak	k6eAd1
v	v	k7c6
závěru	závěr	k1gInSc6
rozhodujících	rozhodující	k2eAgInPc2d1
setů	set	k1gInPc2
všech	všecek	k3xTgFnPc2
soutěží	soutěž	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgInPc1d1
míče	míč	k1gInPc1
grandslamu	grandslam	k1gInSc2
dodává	dodávat	k5eAaImIp3nS
díky	díky	k7c3
pětileté	pětiletý	k2eAgFnSc3d1
smlouvě	smlouva	k1gFnSc3
od	od	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
firma	firma	k1gFnSc1
Dunlop	Dunlop	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
nahradila	nahradit	k5eAaPmAgFnS
míče	míč	k1gInPc4
Wilson	Wilsona	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Struny	struna	k1gFnSc2
zajišťuje	zajišťovat	k5eAaImIp3nS
společnost	společnost	k1gFnSc1
Yonex	Yonex	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generálním	generální	k2eAgMnSc7d1
sponzorem	sponzor	k1gMnSc7
je	být	k5eAaImIp3nS
jihokorejská	jihokorejský	k2eAgFnSc1d1
automobilka	automobilka	k1gFnSc1
Kia	Kia	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
v	v	k7c6
lednu	leden	k1gInSc6
2018	#num#	k4
prodloužila	prodloužit	k5eAaPmAgFnS
kontrakt	kontrakt	k1gInSc4
do	do	k7c2
roku	rok	k1gInSc2
2024	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Hlavními	hlavní	k2eAgMnPc7d1
partnery	partner	k1gMnPc7
pak	pak	k6eAd1
jsou	být	k5eAaImIp3nP
firmy	firma	k1gFnPc1
ANZ	ANZ	kA
<g/>
,	,	kIx,
Luzhou	Luzha	k1gMnSc7
Laojiao	Laojiao	k1gNnSc4
a	a	k8xC
Rolex	Rolex	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technologie	technologie	k1gFnSc1
pro	pro	k7c4
dopad	dopad	k1gInSc4
míčů	míč	k1gInPc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
jestřábí	jestřábí	k2eAgNnSc1d1
oko	oko	k1gNnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
všech	všecek	k3xTgInPc2
16	#num#	k4
soutěžních	soutěžní	k2eAgInPc2d1
dvorců	dvorec	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Každoroční	každoroční	k2eAgFnSc1d1
návštěvnost	návštěvnost	k1gFnSc1
turnaje	turnaj	k1gInSc2
je	být	k5eAaImIp3nS
vysoká	vysoký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ročník	ročník	k1gInSc1
2020	#num#	k4
zaznamenal	zaznamenat	k5eAaPmAgInS
divácký	divácký	k2eAgInSc1d1
rekord	rekord	k1gInSc1
v	v	k7c6
celé	celý	k2eAgFnSc6d1
historii	historie	k1gFnSc6
grandslamu	grandslam	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
do	do	k7c2
areálu	areál	k1gInSc2
zavítalo	zavítat	k5eAaPmAgNnS
812	#num#	k4
174	#num#	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Australian	Australian	k1gInSc1
Open	Open	k1gInSc1
je	být	k5eAaImIp3nS
organizován	organizovat	k5eAaBmNgInS
národním	národní	k2eAgInSc7d1
tenisovým	tenisový	k2eAgInSc7d1
svazem	svaz	k1gInSc7
Tennis	Tennis	k1gFnSc2
Australia	Australium	k1gNnSc2
a	a	k8xC
Mezinárodní	mezinárodní	k2eAgFnSc7d1
tenisovou	tenisový	k2eAgFnSc7d1
federací	federace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc4
ročník	ročník	k1gInSc4
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
v	v	k7c6
melbournském	melbournský	k2eAgInSc6d1
areálu	areál	k1gInSc6
Warehouseman	Warehouseman	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Cricket	Cricket	k1gInSc1
Ground	Grounda	k1gFnPc2
roku	rok	k1gInSc2
1905	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
současný	současný	k2eAgInSc1d1
název	název	k1gInSc1
zní	znět	k5eAaImIp3nS
Albert	Albert	k1gMnSc1
Reserve	Reserev	k1gFnSc2
Tennis	Tennis	k1gFnSc2
Centre	centr	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Název	název	k1gInSc1
a	a	k8xC
dějiště	dějiště	k1gNnSc1
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1
turnaje	turnaj	k1gInSc2
se	se	k3xPyFc4
vyvíjelo	vyvíjet	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdříve	dříve	k6eAd3
se	se	k3xPyFc4
v	v	k7c6
letech	let	k1gInPc6
1905	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
událost	událost	k1gFnSc1
hrála	hrát	k5eAaImAgFnS
jako	jako	k9
„	„	k?
<g/>
Australasian	Australasian	k1gInSc1
Championships	Championships	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
poté	poté	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1968	#num#	k4
nesla	nést	k5eAaImAgFnS
název	název	k1gInSc4
„	„	k?
<g/>
Australian	Australian	k1gInSc1
Championships	Championships	k1gInSc1
<g/>
“	“	k?
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1969	#num#	k4
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
„	„	k?
<g/>
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Šestinásobný	šestinásobný	k2eAgMnSc1d1
vítěz	vítěz	k1gMnSc1
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
Postupně	postupně	k6eAd1
se	se	k3xPyFc4
dějištěm	dějiště	k1gNnSc7
stalo	stát	k5eAaPmAgNnS
pět	pět	k4xCc1
australských	australský	k2eAgMnPc2d1
a	a	k8xC
dvě	dva	k4xCgNnPc4
novozélandská	novozélandský	k2eAgNnPc4d1
města	město	k1gNnPc4
<g/>
:	:	kIx,
Melbourne	Melbourne	k1gNnSc6
(	(	kIx(
<g/>
65	#num#	k4
<g/>
krát	krát	k6eAd1
k	k	k7c3
roku	rok	k1gInSc3
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sydney	Sydney	k1gNnSc4
(	(	kIx(
<g/>
17	#num#	k4
<g/>
krát	krát	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Adelaide	Adelaid	k1gMnSc5
(	(	kIx(
<g/>
14	#num#	k4
<g/>
krát	krát	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Brisbane	Brisban	k1gMnSc5
(	(	kIx(
<g/>
7	#num#	k4
<g/>
krát	krát	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Perth	Perth	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
krát	krát	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Christchurch	Christchurch	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
krát	krát	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1906	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Hastings	Hastings	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
krát	krát	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1912	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1972	#num#	k4
bylo	být	k5eAaImAgNnS
přijato	přijmout	k5eAaPmNgNnS
rozhodnutí	rozhodnutí	k1gNnSc1
o	o	k7c6
stálém	stálý	k2eAgInSc6d1
místu	místo	k1gNnSc3
určeném	určený	k2eAgInSc6d1
ke	k	k7c3
konání	konání	k1gNnSc3
grandslamu	grandslam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vybrán	vybrán	k2eAgInSc1d1
byl	být	k5eAaImAgInS
areál	areál	k1gInSc1
Kooyong	Kooyong	k1gInSc1
Lawn	Lawn	k1gInSc1
Tennis	Tennis	k1gInSc1
Club	club	k1gInSc1
v	v	k7c6
Melbourne	Melbourne	k1gNnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
díky	díky	k7c3
atraktivitě	atraktivita	k1gFnSc3
a	a	k8xC
příslibu	příslib	k1gInSc3
nejlepšího	dobrý	k2eAgNnSc2d3
zajištění	zajištění	k1gNnSc2
zázemí	zázemí	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ačkoli	ačkoli	k8xS
úvodní	úvodní	k2eAgInSc1d1
ročník	ročník	k1gInSc1
proběhl	proběhnout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1905	#num#	k4
<g/>
,	,	kIx,
International	International	k1gFnSc1
Lawn	Lawna	k1gFnPc2
Tennis	Tennis	k1gFnSc1
Federation	Federation	k1gInSc1
(	(	kIx(
<g/>
ILTF	ILTF	kA
<g/>
)	)	kIx)
turnaj	turnaj	k1gInSc1
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1924	#num#	k4
neklasifikovala	klasifikovat	k5eNaImAgFnS
mezi	mezi	k7c4
tzv.	tzv.	kA
majory	major	k1gMnPc4
<g/>
,	,	kIx,
velké	velký	k2eAgFnPc1d1
události	událost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodnutí	rozhodnutí	k1gNnSc1
tak	tak	k6eAd1
učinit	učinit	k5eAaPmF,k5eAaImF
padlo	padnout	k5eAaImAgNnS,k5eAaPmAgNnS
na	na	k7c6
jejím	její	k3xOp3gNnSc6
zasedání	zasedání	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1923	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tím	ten	k3xDgNnSc7
souvisela	souviset	k5eAaImAgFnS
i	i	k9
implementace	implementace	k1gFnSc1
nového	nový	k2eAgMnSc2d1
tzv.	tzv.	kA
amerického	americký	k2eAgInSc2d1
způsobu	způsob	k1gInSc2
nasazování	nasazování	k1gNnSc1
hráčů	hráč	k1gMnPc2
v	v	k7c6
soutěžích	soutěž	k1gFnPc6
turnaje	turnaj	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
nedostačující	dostačující	k2eNgFnSc4d1
kapacitu	kapacita	k1gFnSc4
diváků	divák	k1gMnPc2
a	a	k8xC
rozlohu	rozloha	k1gFnSc4
areálu	areál	k1gInSc2
v	v	k7c6
Kooyongu	Kooyong	k1gInSc6
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
dostavěno	dostavěn	k2eAgNnSc1d1
–	–	k?
vedle	vedle	k7c2
železničního	železniční	k2eAgNnSc2d1
překladiště	překladiště	k1gNnSc2
a	a	k8xC
železničních	železniční	k2eAgFnPc2d1
tratí	trať	k1gFnPc2
Jolimont	Jolimonta	k1gFnPc2
Yards	Yards	k1gInSc1
–	–	k?
<g/>
,	,	kIx,
nové	nový	k2eAgNnSc1d1
sportoviště	sportoviště	k1gNnSc1
Melbourne	Melbourne	k1gNnSc2
Park	park	k1gInSc4
<g/>
,	,	kIx,
nejdříve	dříve	k6eAd3
známý	známý	k2eAgMnSc1d1
pod	pod	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Flinders	Flinders	k1gInSc1
Park	park	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změna	změna	k1gFnSc1
dějiště	dějiště	k1gNnSc2
znamenala	znamenat	k5eAaImAgFnS
okamžitý	okamžitý	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
s	s	k7c7
90	#num#	k4
%	%	kIx~
nárůstem	nárůst	k1gInSc7
návštěvnosti	návštěvnost	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
grandslam	grandslam	k6eAd1
v	v	k7c6
Melbourne	Melbourne	k1gNnSc6
Parku	park	k1gInSc6
zhlédlo	zhlédnout	k5eAaPmAgNnS
266	#num#	k4
436	#num#	k4
platících	platící	k2eAgMnPc2d1
diváků	divák	k1gMnPc2
<g/>
,	,	kIx,
oproti	oproti	k7c3
roku	rok	k1gInSc3
1987	#num#	k4
v	v	k7c6
Kooyongu	Kooyong	k1gInSc6
s	s	k7c7
účastí	účast	k1gFnSc7
140	#num#	k4
000	#num#	k4
návštěvníků	návštěvník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Návrh	návrh	k1gInSc1
změny	změna	k1gFnSc2
dějiště	dějiště	k1gNnSc2
</s>
<s>
Guillaume	Guillaum	k1gInSc5
Rufin	Rufin	k1gMnSc1
podává	podávat	k5eAaImIp3nS
proti	proti	k7c3
Tomáši	Tomáš	k1gMnSc3
Berdychovi	Berdych	k1gMnSc3
v	v	k7c6
Laverově	Laverův	k2eAgFnSc6d1
aréně	aréna	k1gFnSc6
během	během	k7c2
2	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
ročníku	ročník	k1gInSc2
2013	#num#	k4
</s>
<s>
S	s	k7c7
návrhem	návrh	k1gInSc7
změny	změna	k1gFnSc2
dějiště	dějiště	k1gNnSc2
Australian	Australiana	k1gFnPc2
Open	Openo	k1gNnPc2
přišly	přijít	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
správní	správní	k2eAgInPc4d1
orgány	orgán	k1gInPc4
Nového	Nového	k2eAgInSc2d1
Jižního	jižní	k2eAgInSc2d1
Walesu	Wales	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
usilují	usilovat	k5eAaImIp3nP
o	o	k7c6
získání	získání	k1gNnSc6
turnajových	turnajový	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
,	,	kIx,
jakmile	jakmile	k8xS
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
vyprší	vypršet	k5eAaPmIp3nS
současná	současný	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
s	s	k7c7
Melbourne	Melbourne	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změna	změna	k1gFnSc1
předpokládá	předpokládat	k5eAaImIp3nS
přemístění	přemístění	k1gNnSc4
dějiště	dějiště	k1gNnSc2
na	na	k7c4
Glebe	Gleb	k1gInSc5
Island	Island	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
takovou	takový	k3xDgFnSc4
snahu	snaha	k1gFnSc4
prezident	prezident	k1gMnSc1
Victorian	Victoriana	k1gFnPc2
Events	Eventsa	k1gFnPc2
Industry	Industra	k1gFnSc2
Council	Council	k1gMnSc1
Wayne	Wayn	k1gInSc5
Kayler-Thomson	Kayler-Thomson	k1gInSc4
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
grandslam	grandslam	k1gInSc1
měl	mít	k5eAaImAgInS
zůstat	zůstat	k5eAaPmF
v	v	k7c6
Melbourne	Melbourne	k1gNnSc6
a	a	k8xC
snahu	snaha	k1gFnSc4
samosprávy	samospráva	k1gFnSc2
vůči	vůči	k7c3
městu	město	k1gNnSc3
označil	označit	k5eAaPmAgInS
za	za	k7c4
zničující	zničující	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
zveřejnění	zveřejnění	k1gNnSc2
návrhu	návrh	k1gInSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
oznámení	oznámení	k1gNnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
do	do	k7c2
přestavby	přestavba	k1gFnSc2
Melbourne	Melbourne	k1gNnSc2
Parku	park	k1gInSc2
investují	investovat	k5eAaBmIp3nP
desítky	desítka	k1gFnPc4
až	až	k8xS
stovky	stovka	k1gFnPc4
miliónů	milión	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proběhne	proběhnout	k5eAaPmIp3nS
jeho	jeho	k3xOp3gFnSc2
modernizace	modernizace	k1gFnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
zvětšení	zvětšení	k1gNnSc2
kapacity	kapacita	k1gFnSc2
sedících	sedící	k2eAgMnPc2d1
diváků	divák	k1gMnPc2
na	na	k7c6
velkých	velký	k2eAgInPc6d1
dvorcích	dvorec	k1gInPc6
<g/>
,	,	kIx,
Margaret	Margareta	k1gFnPc2
Court	Courta	k1gFnPc2
Arena	Areno	k1gNnSc2
získá	získat	k5eAaPmIp3nS
zatahovací	zatahovací	k2eAgFnSc4d1
střechu	střecha	k1gFnSc4
<g/>
,	,	kIx,
zlepší	zlepšit	k5eAaPmIp3nS
se	se	k3xPyFc4
zázemí	zázemí	k1gNnSc1
hráčů	hráč	k1gMnPc2
a	a	k8xC
bude	být	k5eAaImBp3nS
vybudováno	vybudovat	k5eAaPmNgNnS
nové	nový	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
vedení	vedení	k1gNnSc2
australského	australský	k2eAgInSc2d1
tenisového	tenisový	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Následně	následně	k6eAd1
tyto	tento	k3xDgInPc4
plány	plán	k1gInPc4
obnovy	obnova	k1gFnSc2
našly	najít	k5eAaPmAgFnP
kladnou	kladný	k2eAgFnSc4d1
odezvu	odezva	k1gFnSc4
u	u	k7c2
úřadujícího	úřadující	k2eAgMnSc2d1
victorijského	victorijský	k2eAgMnSc2d1
premiéra	premiér	k1gMnSc2
Johna	John	k1gMnSc2
Brumbyho	Brumby	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
potvrdil	potvrdit	k5eAaPmAgMnS
ochotou	ochota	k1gFnSc7
místní	místní	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
investovat	investovat	k5eAaBmF
363	#num#	k4
miliónů	milión	k4xCgInPc2
australských	australský	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
bude	být	k5eAaImBp3nS
na	na	k7c6
tomto	tento	k3xDgNnSc6
sportovišti	sportoviště	k1gNnSc6
garantována	garantován	k2eAgFnSc1d1
přítomnost	přítomnost	k1gFnSc1
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
alespoň	alespoň	k9
do	do	k7c2
roku	rok	k1gInSc2
2036	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Championships	Championships	k6eAd1
éra	éra	k1gFnSc1
</s>
<s>
Pro	pro	k7c4
geografickou	geografický	k2eAgFnSc4d1
odlehlost	odlehlost	k1gFnSc4
Austrálie	Austrálie	k1gFnSc2
se	se	k3xPyFc4
turnaje	turnaj	k1gInPc1
v	v	k7c6
jeho	jeho	k3xOp3gInPc6
počátcích	počátek	k1gInPc6
zúčastňoval	zúčastňovat	k5eAaImAgInS
velmi	velmi	k6eAd1
omezený	omezený	k2eAgInSc1d1
počet	počet	k1gInSc1
tenistů	tenista	k1gMnPc2
ze	z	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
trvala	trvat	k5eAaImAgFnS
oceánská	oceánský	k2eAgFnSc1d1
plavba	plavba	k1gFnSc1
z	z	k7c2
Evropy	Evropa	k1gFnSc2
přibližně	přibližně	k6eAd1
45	#num#	k4
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnPc1
hráči	hráč	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
na	na	k7c4
kontinent	kontinent	k1gInSc4
přicestovali	přicestovat	k5eAaPmAgMnP
letadlem	letadlo	k1gNnSc7
byli	být	k5eAaImAgMnP
až	až	k9
členové	člen	k1gMnPc1
daviscupového	daviscupový	k2eAgInSc2d1
týmu	tým	k1gInSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
v	v	k7c6
listopadu	listopad	k1gInSc6
1946	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Také	také	k9
Australané	Australan	k1gMnPc1
žijící	žijící	k2eAgMnPc1d1
v	v	k7c6
odlehlejších	odlehlý	k2eAgFnPc6d2
částech	část	k1gFnPc6
měli	mít	k5eAaImAgMnP
problémy	problém	k1gInPc4
s	s	k7c7
dopravou	doprava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
dějištěm	dějiště	k1gNnSc7
stal	stát	k5eAaPmAgInS
jihozápadní	jihozápadní	k2eAgInSc1d1
Perth	Perth	k1gInSc1
<g/>
,	,	kIx,
žádný	žádný	k3yNgMnSc1
tenista	tenista	k1gMnSc1
z	z	k7c2
Victorie	Victorie	k1gFnSc2
a	a	k8xC
Nového	Nového	k2eAgInSc2d1
Jižního	jižní	k2eAgInSc2d1
Walesu	Wales	k1gInSc2
na	na	k7c4
turnaj	turnaj	k1gInSc4
železniční	železniční	k2eAgFnSc7d1
dopravou	doprava	k1gFnSc7
nedorazil	dorazit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdálenost	vzdálenost	k1gFnSc1
čítající	čítající	k2eAgFnSc1d1
přibližně	přibližně	k6eAd1
3	#num#	k4
300	#num#	k4
km	km	kA
mezi	mezi	k7c7
východním	východní	k2eAgNnSc7d1
a	a	k8xC
západním	západní	k2eAgNnSc7d1
pobřežím	pobřeží	k1gNnSc7
byla	být	k5eAaImAgFnS
příliš	příliš	k6eAd1
značná	značný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1906	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
událost	událost	k1gFnSc1
konala	konat	k5eAaImAgFnS
v	v	k7c6
Christchurch	Christchur	k1gFnPc6
<g/>
,	,	kIx,
nastoupilo	nastoupit	k5eAaPmAgNnS
pouze	pouze	k6eAd1
10	#num#	k4
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgMnSc2
jen	jen	k9
dva	dva	k4xCgMnPc1
Australané	Australan	k1gMnPc1
a	a	k8xC
druhý	druhý	k4xOgInSc1
ročník	ročník	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgMnS
Novozélanďan	Novozélanďan	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Almagro	Almagro	k6eAd1
podává	podávat	k5eAaImIp3nS
v	v	k7c6
zápase	zápas	k1gInSc6
s	s	k7c7
Fallou	Falla	k1gFnSc7
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
Úvodní	úvodní	k2eAgInSc1d1
ročník	ročník	k1gInSc1
Australasian	Australasian	k1gMnSc1
Championships	Championshipsa	k1gFnPc2
utrpěl	utrpět	k5eAaPmAgMnS
také	také	k9
v	v	k7c6
důsledku	důsledek	k1gInSc6
již	již	k6eAd1
existujících	existující	k2eAgInPc2d1
australasijských	australasijský	k2eAgInPc2d1
turnajů	turnaj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
rokem	rok	k1gInSc7
1905	#num#	k4
pořádaly	pořádat	k5eAaImAgInP
všechny	všechen	k3xTgInPc1
australské	australský	k2eAgInPc1d1
státy	stát	k1gInPc1
i	i	k8xC
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
vlastní	vlastnit	k5eAaImIp3nS
tenisová	tenisový	k2eAgNnPc4d1
mistrovství	mistrovství	k1gNnPc4
<g/>
,	,	kIx,
první	první	k4xOgInPc4
organizované	organizovaný	k2eAgInPc4d1
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1880	#num#	k4
v	v	k7c6
Melbourne	Melbourne	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
název	název	k1gInSc4
zněl	znět	k5eAaImAgInS
„	„	k?
<g/>
Championship	Championship	k1gInSc1
of	of	k?
the	the	k?
Colony	colon	k1gNnPc7
of	of	k?
Victoria	Victorium	k1gNnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
přejmenován	přejmenován	k2eAgInSc1d1
na	na	k7c4
„	„	k?
<g/>
Championship	Championship	k1gInSc4
of	of	k?
Victoria	Victorium	k1gNnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc6
se	se	k3xPyFc4
dva	dva	k4xCgMnPc1
nejlepší	dobrý	k2eAgMnPc1d3
hráči	hráč	k1gMnPc1
<g/>
,	,	kIx,
Australan	Australan	k1gMnSc1
Norman	Norman	k1gMnSc1
Brookes	Brookes	k1gMnSc1
a	a	k8xC
Novozélanďan	Novozélanďan	k1gMnSc1
Tony	Tony	k1gMnSc1
Wilding	Wilding	k1gInSc4
<g/>
,	,	kIx,
až	až	k9
na	na	k7c4
výjimky	výjimka	k1gFnPc4
australasijského	australasijský	k2eAgInSc2d1
grandslamu	grandslam	k1gInSc2
nezúčatnili	zúčatnit	k5eNaImAgMnP,k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brookes	Brookes	k1gInSc4
na	na	k7c6
něm	on	k3xPp3gMnSc6
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
startoval	startovat	k5eAaBmAgInS
pouze	pouze	k6eAd1
jednou	jeden	k4xCgFnSc7
v	v	k7c6
roce	rok	k1gInSc6
1911	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
také	také	k9
získal	získat	k5eAaPmAgMnS
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wilding	Wilding	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
oba	dva	k4xCgInPc4
ročníky	ročník	k1gInPc4
při	při	k7c6
dvou	dva	k4xCgFnPc6
účastech	účast	k1gFnPc6
v	v	k7c6
letech	léto	k1gNnPc6
1906	#num#	k4
a	a	k8xC
1909	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
preferovali	preferovat	k5eAaImAgMnP
jiné	jiný	k2eAgInPc4d1
turnaje	turnaj	k1gInPc4
<g/>
,	,	kIx,
střetávali	střetávat	k5eAaImAgMnP
se	se	k3xPyFc4
na	na	k7c4
Victorian	Victorian	k1gInSc4
Championships	Championshipsa	k1gFnPc2
a	a	k8xC
ve	v	k7c6
Wimbledonu	Wimbledon	k1gInSc6
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
kvalitními	kvalitní	k2eAgMnPc7d1
australasijskými	australasijský	k2eAgMnPc7d1
tenisty	tenista	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokonce	dokonce	k9
<g/>
,	,	kIx,
ani	ani	k8xC
když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1912	#num#	k4
Australasian	Australasian	k1gInSc1
Championships	Championships	k1gInSc4
konal	konat	k5eAaImAgInS
v	v	k7c6
novozélandském	novozélandský	k2eAgInSc6d1
Hastingsu	Hastings	k1gInSc6
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
již	již	k6eAd1
trojnásobný	trojnásobný	k2eAgMnSc1d1
wimbledonský	wimbledonský	k2eAgMnSc1d1
vítěz	vítěz	k1gMnSc1
Wilding	Wilding	k1gInSc4
<g/>
,	,	kIx,
nepřicestoval	přicestovat	k5eNaPmAgMnS
do	do	k7c2
svého	svůj	k3xOyFgNnSc2
rodného	rodný	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
na	na	k7c6
něm	on	k3xPp3gInSc6
nastoupil	nastoupit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1
a	a	k8xC
nedostatečný	dostatečný	k2eNgInSc1d1
věhlas	věhlas	k1gInSc1
turnaje	turnaj	k1gInSc2
tak	tak	k6eAd1
znamenaly	znamenat	k5eAaImAgFnP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jej	on	k3xPp3gNnSc2
řada	řada	k1gFnSc1
světových	světový	k2eAgMnPc2d1
tenistů	tenista	k1gMnPc2
nikdy	nikdy	k6eAd1
nezúčastnila	zúčastnit	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
takovými	takový	k3xDgInPc7
byli	být	k5eAaImAgMnP
také	také	k9
bratři	bratr	k1gMnPc1
William	William	k1gInSc4
a	a	k8xC
Ernest	Ernest	k1gMnSc1
Renshawovi	Renshawův	k2eAgMnPc1d1
<g/>
,	,	kIx,
bratři	bratr	k1gMnPc1
Lawrence	Lawrence	k1gFnSc2
a	a	k8xC
Reginald	Reginald	k1gMnSc1
Dohertyovi	Dohertya	k1gMnSc3
<g/>
,	,	kIx,
William	William	k1gInSc1
Larned	Larned	k1gInSc4
<g/>
,	,	kIx,
Maurice	Maurika	k1gFnSc3
McLoughlin	McLoughlina	k1gFnPc2
<g/>
,	,	kIx,
Beals	Bealsa	k1gFnPc2
Wright	Wrighta	k1gFnPc2
<g/>
,	,	kIx,
Bill	Bill	k1gMnSc1
Johnston	Johnston	k1gInSc1
<g/>
,	,	kIx,
Bill	Bill	k1gMnSc1
Tilden	Tildna	k1gFnPc2
<g/>
,	,	kIx,
René	René	k1gFnPc2
Lacoste	Lacost	k1gInSc5
<g/>
,	,	kIx,
Henri	Henri	k1gNnPc7
Cochet	Cochet	k1gInSc1
<g/>
,	,	kIx,
Bobby	Bobba	k1gFnPc1
Riggs	Riggs	k1gInSc1
<g/>
,	,	kIx,
Jack	Jack	k1gMnSc1
Kramer	Kramer	k1gMnSc1
<g/>
,	,	kIx,
Ted	Ted	k1gMnSc1
Schroeder	Schroeder	k1gMnSc1
<g/>
,	,	kIx,
Pancho	Pancha	k1gFnSc5
Gonzales	Gonzales	k1gInSc1
<g/>
,	,	kIx,
Budge	Budge	k1gInSc1
Patty	Patta	k1gFnSc2
<g/>
,	,	kIx,
Manuel	Manuel	k1gMnSc1
Santana	Santana	k1gFnSc1
nebo	nebo	k8xC
Jan	Jan	k1gMnSc1
Kodeš	Kodeš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgMnPc1d1
světoví	světový	k2eAgMnPc1d1
tenisté	tenista	k1gMnPc1
na	na	k7c4
grandslam	grandslam	k1gInSc4
přijeli	přijet	k5eAaPmAgMnP
pouze	pouze	k6eAd1
jednou	jednou	k6eAd1
<g/>
,	,	kIx,
včetně	včetně	k7c2
hráčů	hráč	k1gMnPc2
jakými	jaký	k3yQgInPc7,k3yIgInPc7,k3yRgInPc7
byli	být	k5eAaImAgMnP
Brookes	Brookes	k1gMnSc1
<g/>
,	,	kIx,
Ellsworth	Ellsworth	k1gMnSc1
Vines	Vines	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Drobný	drobný	k2eAgMnSc1d1
<g/>
,	,	kIx,
Manuel	Manuel	k1gMnSc1
Orantes	Orantes	k1gMnSc1
<g/>
,	,	kIx,
třicetipětiletý	třicetipětiletý	k2eAgInSc4d1
Ilie	Ilie	k1gInSc4
Năstase	Năstasa	k1gFnSc3
a	a	k8xC
Björn	Björna	k1gFnPc2
Borg	Borg	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Open	Open	k1gInSc1
éra	éra	k1gFnSc1
</s>
<s>
Japonka	Japonka	k1gFnSc1
Naomi	Nao	k1gFnPc7
Ósakaová	Ósakaová	k1gFnSc1
vyhrála	vyhrát	k5eAaPmAgFnS
ročníky	ročník	k1gInPc4
2019	#num#	k4
a	a	k8xC
2021	#num#	k4
</s>
<s>
Poprvé	poprvé	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
hrálo	hrát	k5eAaImAgNnS
v	v	k7c6
brisbaneském	brisbaneský	k2eAgInSc6d1
areálu	areál	k1gInSc6
Milton	Milton	k1gInSc1
Courts	Courts	k1gInSc1
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
otevření	otevření	k1gNnSc3
turnaje	turnaj	k1gInSc2
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
hráče	hráč	k1gMnPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
profesionálů	profesionál	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Vyjma	vyjma	k7c2
let	léto	k1gNnPc2
1969	#num#	k4
a	a	k8xC
1971	#num#	k4
<g/>
,	,	kIx,
řada	řada	k1gFnSc1
z	z	k7c2
nejlepších	dobrý	k2eAgMnPc2d3
tenistů	tenista	k1gMnPc2
absentovala	absentovat	k5eAaImAgFnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
důvody	důvod	k1gInPc4
stále	stále	k6eAd1
patřily	patřit	k5eAaImAgInP
značná	značný	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
<g/>
,	,	kIx,
nevyhovující	vyhovující	k2eNgNnSc1d1
datum	datum	k1gNnSc1
v	v	k7c6
období	období	k1gNnSc6
Vánoc	Vánoce	k1gFnPc2
a	a	k8xC
Nového	Nového	k2eAgInSc2d1
roku	rok	k1gInSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k9
i	i	k9
nízké	nízký	k2eAgFnPc4d1
finanční	finanční	k2eAgFnPc4d1
odměny	odměna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
si	se	k3xPyFc3
americká	americký	k2eAgFnSc1d1
tenisová	tenisový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
(	(	kIx(
<g/>
National	National	k1gFnSc1
Tennis	Tennis	k1gFnSc1
League	League	k1gFnSc1
<g/>
)	)	kIx)
zavázala	zavázat	k5eAaPmAgFnS
garancemi	garance	k1gFnPc7
své	svůj	k3xOyFgMnPc4
hráče	hráč	k1gMnPc4
před	před	k7c7
startem	start	k1gInSc7
na	na	k7c4
Australian	Australian	k1gInSc4
Open	Opena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řadili	řadit	k5eAaImAgMnP
se	se	k3xPyFc4
k	k	k7c3
nim	on	k3xPp3gFnPc3
Rod	rod	k1gInSc1
Laver	lavra	k1gFnPc2
<g/>
,	,	kIx,
Ken	Ken	k1gFnPc2
Rosewall	Rosewalla	k1gFnPc2
<g/>
,	,	kIx,
Andrés	Andrésa	k1gFnPc2
Gimeno	Gimen	k2eAgNnSc1d1
<g/>
,	,	kIx,
Pancho	Pancha	k1gMnSc5
Gonzales	Gonzales	k1gMnSc1
<g/>
,	,	kIx,
Roy	Roy	k1gMnSc1
Emerson	Emerson	k1gMnSc1
a	a	k8xC
Fred	Fred	k1gMnSc1
Stolle	Stolle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australský	australský	k2eAgInSc4d1
turnaj	turnaj	k1gInSc4
pak	pak	k6eAd1
vyhrál	vyhrát	k5eAaPmAgMnS
Arthur	Arthur	k1gMnSc1
Ashe	Ash	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
na	na	k7c4
grandslam	grandslam	k1gInSc4
poprvé	poprvé	k6eAd1
přijeli	přijet	k5eAaPmAgMnP
Ivan	Ivan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
McEnroe	McEnro	k1gFnSc2
a	a	k8xC
Mats	Matsa	k1gFnPc2
Wilander	Wilander	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
následně	následně	k6eAd1
získal	získat	k5eAaPmAgMnS
singlový	singlový	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
krátce	krátce	k6eAd1
poté	poté	k6eAd1
ještě	ještě	k6eAd1
na	na	k7c6
konci	konec	k1gInSc6
prosince	prosinec	k1gInSc2
<g/>
,	,	kIx,
vyhrál	vyhrát	k5eAaPmAgMnS
obě	dva	k4xCgFnPc4
dvouhry	dvouhra	k1gFnPc4
v	v	k7c6
daviscupovém	daviscupový	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
mezi	mezi	k7c7
Austrálií	Austrálie	k1gFnSc7
a	a	k8xC
Švédskem	Švédsko	k1gNnSc7
konaném	konaný	k2eAgNnSc6d1
ve	v	k7c6
stejném	stejné	k1gNnSc6
areálu	areál	k1gInSc2
Kooyong	Kooyonga	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
skončení	skončení	k1gNnSc6
grandslamu	grandslam	k1gInSc2
1983	#num#	k4
pobídla	pobídnout	k5eAaPmAgFnS
Mezinárodní	mezinárodní	k2eAgFnSc1d1
tenisová	tenisový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
australský	australský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
ke	k	k7c3
změně	změna	k1gFnSc3
dějiště	dějiště	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
pro	pro	k7c4
tak	tak	k6eAd1
velkou	velký	k2eAgFnSc4d1
událost	událost	k1gFnSc4
nesplňovalo	splňovat	k5eNaImAgNnS
podmínky	podmínka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
se	se	k3xPyFc4
Australian	Australian	k1gInSc1
Open	Opena	k1gFnPc2
poprvé	poprvé	k6eAd1
představilo	představit	k5eAaPmAgNnS
v	v	k7c6
novém	nový	k2eAgInSc6d1
Flinders	Flinders	k1gInSc1
Parku	park	k1gInSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
přejmenovaném	přejmenovaný	k2eAgNnSc6d1
na	na	k7c6
Melbourne	Melbourne	k1gNnSc6
Park	park	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
také	také	k6eAd1
ke	k	k7c3
změně	změna	k1gFnSc3
povrchu	povrch	k1gInSc2
<g/>
,	,	kIx,
travnaté	travnatý	k2eAgInPc4d1
dvorce	dvorec	k1gInPc4
nahradil	nahradit	k5eAaPmAgInS
tvrdý	tvrdý	k2eAgInSc1d1
povrch	povrch	k1gInSc1
Rebound	Rebound	k1gMnSc1
Ace	Ace	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Mats	Mats	k1gInSc1
Wilander	Wilander	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
jediným	jediný	k2eAgMnSc7d1
tenistou	tenista	k1gMnSc7
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
dokázal	dokázat	k5eAaPmAgMnS
triumfovat	triumfovat	k5eAaBmF
na	na	k7c6
obou	dva	k4xCgNnPc6
z	z	k7c2
nich	on	k3xPp3gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Čtyři	čtyři	k4xCgInPc4
singlové	singlový	k2eAgInPc4d1
tituly	titul	k1gInPc4
si	se	k3xPyFc3
připsal	připsat	k5eAaPmAgMnS
Andre	Andr	k1gMnSc5
Agassi	Agass	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Serena	Seren	k2eAgFnSc1d1
Williamsová	Williamsová	k1gFnSc1
vyhrála	vyhrát	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
šestý	šestý	k4xOgInSc1
titul	titul	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamenalo	znamenat	k5eAaImAgNnS
rekord	rekord	k1gInSc4
otevřené	otevřený	k2eAgFnSc2d1
éry	éra	k1gFnSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
se	se	k3xPyFc4
pro	pro	k7c4
toto	tento	k3xDgNnSc4
období	období	k1gNnSc4
stala	stát	k5eAaPmAgFnS
–	–	k?
ve	v	k7c6
33	#num#	k4
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
nejstarší	starý	k2eAgFnSc1d3
vítězkou	vítězka	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
přidala	přidat	k5eAaPmAgFnS
Serena	Seren	k2eAgFnSc1d1
Williamsová	Williamsová	k1gFnSc1
do	do	k7c2
své	svůj	k3xOyFgFnSc2
sbírky	sbírka	k1gFnSc2
sedmý	sedmý	k4xOgInSc4
titul	titul	k1gInSc4
a	a	k8xC
v	v	k7c6
35	#num#	k4
letech	léto	k1gNnPc6
tak	tak	k6eAd1
vylepšila	vylepšit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
vlastní	vlastní	k2eAgInSc4d1
rekord	rekord	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srb	Srb	k1gMnSc1
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
dosáhl	dosáhnout	k5eAaPmAgMnS
výhrou	výhra	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
na	na	k7c4
šestý	šestý	k4xOgInSc4
titul	titul	k1gInSc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
vyrovnal	vyrovnat	k5eAaBmAgMnS,k5eAaPmAgMnS
rekord	rekord	k1gInSc4
Roye	Roy	k1gMnSc2
Emersona	Emerson	k1gMnSc2
ze	z	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc4d1
dva	dva	k4xCgInPc4
ročníky	ročník	k1gInPc4
2017	#num#	k4
a	a	k8xC
2018	#num#	k4
patřily	patřit	k5eAaImAgFnP
Rogeru	Rogera	k1gFnSc4
Federerovi	Federer	k1gMnSc3
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
debutový	debutový	k2eAgInSc1d1
vavřín	vavřín	k1gInSc1
vybojoval	vybojovat	k5eAaPmAgInS
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2018	#num#	k4
vytvořil	vytvořit	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
rekord	rekord	k1gInSc1
open	opena	k1gFnPc2
éry	éra	k1gFnSc2
v	v	k7c6
počtu	počet	k1gInSc6
sedmi	sedm	k4xCc2
finále	finále	k1gNnPc2
a	a	k8xC
šestým	šestý	k4xOgInSc7
titulem	titul	k1gInSc7
se	se	k3xPyFc4
dotáhl	dotáhnout	k5eAaPmAgMnS
na	na	k7c4
vedoucí	vedoucí	k2eAgFnSc4d1
dvojici	dvojice	k1gFnSc4
hráčů	hráč	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
první	první	k4xOgMnSc1
muž	muž	k1gMnSc1
historie	historie	k1gFnSc2
Švýcar	Švýcar	k1gMnSc1
zvítězil	zvítězit	k5eAaPmAgMnS
na	na	k7c6
dvou	dva	k4xCgInPc6
grandslamech	grandslam	k1gInPc6
šestkrát	šestkrát	k6eAd1
a	a	k8xC
ve	v	k7c6
36	#num#	k4
letech	léto	k1gNnPc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nejstarším	starý	k2eAgMnSc7d3
šampionem	šampion	k1gMnSc7
grandslamu	grandslam	k1gInSc2
od	od	k7c2
Australian	Australiana	k1gFnPc2
Open	Openo	k1gNnPc2
1972	#num#	k4
a	a	k8xC
Kena	Ken	k2eAgFnSc1d1
Rosewalla	Rosewalla	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Djoković	Djoković	k1gMnSc1
pak	pak	k6eAd1
získal	získat	k5eAaPmAgMnS
v	v	k7c6
letech	let	k1gInPc6
2019	#num#	k4
<g/>
–	–	k?
<g/>
2021	#num#	k4
další	další	k2eAgInSc4d1
tři	tři	k4xCgInPc4
trofeje	trofej	k1gInPc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamenalo	znamenat	k5eAaImAgNnS
rekord	rekord	k1gInSc4
v	v	k7c6
počtu	počet	k1gInSc6
devíti	devět	k4xCc2
singlových	singlový	k2eAgInPc2d1
triumfů	triumf	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
semifinále	semifinále	k1gNnSc6
a	a	k8xC
finále	finále	k1gNnSc6
udržel	udržet	k5eAaPmAgInS
neporazitelnost	neporazitelnost	k1gFnSc4
s	s	k7c7
bilancí	bilance	k1gFnSc7
18	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Melbourne	Melbourne	k1gNnSc1
Park	park	k1gInSc1
</s>
<s>
Dějiště	dějiště	k1gNnSc1
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
vrátilo	vrátit	k5eAaPmAgNnS
do	do	k7c2
Melbourne	Melbourne	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1988	#num#	k4
se	se	k3xPyFc4
přesunulo	přesunout	k5eAaPmAgNnS
z	z	k7c2
melbournského	melbournský	k2eAgMnSc2d1
Kooyong	Kooyong	k1gInSc4
Clubu	club	k1gInSc2
do	do	k7c2
nově	nově	k6eAd1
postaveného	postavený	k2eAgInSc2d1
areálu	areál	k1gInSc2
ve	v	k7c4
Flinders	Flinders	k1gInSc4
Parku	park	k1gInSc2
<g/>
,	,	kIx,
součásti	součást	k1gFnPc1
Melbourne	Melbourne	k1gNnSc2
Sports	Sportsa	k1gFnPc2
and	and	k?
Entertainment	Entertainment	k1gMnSc1
Precinct	Precinct	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australský	australský	k2eAgInSc1d1
major	major	k1gMnSc1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgInSc7
grandslamem	grandslam	k1gInSc7
s	s	k7c7
třemi	tři	k4xCgFnPc7
arénami	aréna	k1gFnPc7
opatřenými	opatřený	k2eAgFnPc7d1
zataženou	zatažený	k2eAgFnSc7d1
střechou	střecha	k1gFnSc7
v	v	k7c6
případě	případ	k1gInSc6
deště	dešť	k1gInSc2
a	a	k8xC
vysokých	vysoký	k2eAgFnPc2d1
teplot	teplota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
celém	celý	k2eAgInSc6d1
areálu	areál	k1gInSc6
je	být	k5eAaImIp3nS
39	#num#	k4
kurtů	kurt	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
33	#num#	k4
s	s	k7c7
tvrdým	tvrdý	k2eAgInSc7d1
povrchem	povrch	k1gInSc7
a	a	k8xC
6	#num#	k4
antukových	antukový	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
soutěžní	soutěžní	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
grandslamu	grandslam	k1gInSc2
je	být	k5eAaImIp3nS
využíváno	využívat	k5eAaImNgNnS,k5eAaPmNgNnS
16	#num#	k4
<g/>
–	–	k?
<g/>
17	#num#	k4
z	z	k7c2
nich	on	k3xPp3gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Všechny	všechen	k3xTgInPc1
postranní	postranní	k2eAgInPc1d1
dvorce	dvorec	k1gInPc1
mají	mít	k5eAaImIp3nP
během	během	k7c2
turnaje	turnaj	k1gInSc2
dočasně	dočasně	k6eAd1
zvýšenou	zvýšený	k2eAgFnSc4d1
kapacitu	kapacita	k1gFnSc4
50	#num#	k4
až	až	k9
2	#num#	k4
500	#num#	k4
sedících	sedící	k2eAgMnPc2d1
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
rokem	rok	k1gInSc7
1988	#num#	k4
se	se	k3xPyFc4
hrálo	hrát	k5eAaImAgNnS
na	na	k7c6
trávě	tráva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1988	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
grandslam	grandslam	k1gInSc1
probíhal	probíhat	k5eAaImAgInS
na	na	k7c6
dvorcích	dvorec	k1gInPc6
s	s	k7c7
tvrdým	tvrdý	k2eAgInSc7d1
povrchem	povrch	k1gInSc7
Rebound	Rebound	k1gMnSc1
Ace	Ace	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
období	období	k1gNnSc6
2008	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
nahrazen	nahrazen	k2eAgInSc1d1
Plexicushion	Plexicushion	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2020	#num#	k4
se	se	k3xPyFc4
hraje	hrát	k5eAaImIp3nS
na	na	k7c6
povrchu	povrch	k1gInSc6
GreenSet	GreenSeta	k1gFnPc2
<g/>
,	,	kIx,
s	s	k7c7
ponecháním	ponechání	k1gNnSc7
sytého	sytý	k2eAgNnSc2d1
tónování	tónování	k1gNnSc2
pro	pro	k7c4
dosažení	dosažení	k1gNnSc4
vyššího	vysoký	k2eAgInSc2d2
kontrastu	kontrast	k1gInSc2
míčku	míček	k1gInSc2
vůči	vůči	k7c3
podkladu	podklad	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Dvorec	dvorec	k1gInSc1
</s>
<s>
otevřen	otevřít	k5eAaPmNgInS
</s>
<s>
kapacita	kapacita	k1gFnSc1
</s>
<s>
střecha	střecha	k1gFnSc1
</s>
<s>
zdroj	zdroj	k1gInSc1
</s>
<s>
Rod	rod	k1gInSc1
Laver	lavra	k1gFnPc2
Arena	Aren	k1gInSc2
</s>
<s>
Rod	rod	k1gInSc1
Laver	lavra	k1gFnPc2
Arena	Aren	k1gInSc2
</s>
<s>
1988	#num#	k4
</s>
<s>
14	#num#	k4
820	#num#	k4
</s>
<s>
zatahovací	zatahovací	k2eAgFnSc1d1
</s>
<s>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
John	John	k1gMnSc1
Cain	Cain	k1gMnSc1
Arena	Arena	k1gFnSc1
</s>
<s>
John	John	k1gMnSc1
Cain	Cain	k1gMnSc1
Arena	Arena	k1gFnSc1
</s>
<s>
2000	#num#	k4
</s>
<s>
10	#num#	k4
500	#num#	k4
</s>
<s>
zatahovací	zatahovací	k2eAgFnSc1d1
</s>
<s>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Margaret	Margareta	k1gFnPc2
Court	Courta	k1gFnPc2
Arena	Aren	k1gMnSc2
<g/>
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Show	show	k1gFnSc1
Court	Court	k1gInSc1
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Margaret	Margareta	k1gFnPc2
Court	Courta	k1gFnPc2
Arena	Areen	k2eAgFnSc1d1
</s>
<s>
1988	#num#	k4
</s>
<s>
7	#num#	k4
500	#num#	k4
</s>
<s>
zatahovací	zatahovací	k2eAgFnSc1d1
</s>
<s>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1573	#num#	k4
Arena	Areno	k1gNnPc1
<g/>
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Show	show	k1gFnSc1
Court	Court	k1gInSc1
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1573	#num#	k4
Arena	Aren	k1gInSc2
</s>
<s>
1988	#num#	k4
</s>
<s>
3	#num#	k4
000	#num#	k4
</s>
<s>
není	být	k5eNaImIp3nS
</s>
<s>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Show	show	k1gFnSc1
Court	Court	k1gInSc1
3	#num#	k4
</s>
<s>
Show	show	k1gFnSc1
Court	Court	k1gInSc1
3	#num#	k4
</s>
<s>
1988	#num#	k4
</s>
<s>
3	#num#	k4
000	#num#	k4
</s>
<s>
není	být	k5eNaImIp3nS
</s>
<s>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Období	období	k1gNnSc1
konání	konání	k1gNnSc2
</s>
<s>
Do	do	k7c2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
se	s	k7c7
stálým	stálý	k2eAgNnSc7d1
dějištěm	dějiště	k1gNnSc7
stal	stát	k5eAaPmAgMnS
Melbourne	Melbourne	k1gNnSc4
Park	park	k1gInSc1
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
také	také	k9
proměnlivé	proměnlivý	k2eAgNnSc1d1
datum	datum	k1gNnSc1
turnaje	turnaj	k1gInSc2
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
už	už	k9
pro	pro	k7c4
specifické	specifický	k2eAgNnSc4d1
podnebí	podnebí	k1gNnSc4
rozdílných	rozdílný	k2eAgFnPc2d1
lokalit	lokalita	k1gFnPc2
nebo	nebo	k8xC
specifičnost	specifičnost	k1gFnSc4
konání	konání	k1gNnSc2
události	událost	k1gFnSc2
v	v	k7c6
daném	daný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Například	například	k6eAd1
pro	pro	k7c4
ročník	ročník	k1gInSc4
1919	#num#	k4
se	se	k3xPyFc4
turnaj	turnaj	k1gInSc1
konal	konat	k5eAaImAgInS
až	až	k9
v	v	k7c6
lednu	leden	k1gInSc6
1920	#num#	k4
a	a	k8xC
následující	následující	k2eAgInSc1d1
ročník	ročník	k1gInSc1
již	již	k6eAd1
v	v	k7c6
březnu	březen	k1gInSc6
1920	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1923	#num#	k4
se	se	k3xPyFc4
pak	pak	k6eAd1
v	v	k7c6
Brisbane	Brisban	k1gMnSc5
uskutečnil	uskutečnit	k5eAaPmAgInS
v	v	k7c6
období	období	k1gNnSc6
menšího	malý	k2eAgInSc2d2
srážkového	srážkový	k2eAgInSc2d1
úhrnu	úhrn	k1gInSc2
a	a	k8xC
nižší	nízký	k2eAgFnSc2d2
teploty	teplota	k1gFnSc2
v	v	k7c6
srpnu	srpen	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
odehrání	odehrání	k1gNnSc6
ročníku	ročník	k1gInSc2
1976	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
skončil	skončit	k5eAaPmAgInS
až	až	k6eAd1
4	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1977	#num#	k4
se	se	k3xPyFc4
organizátoři	organizátor	k1gMnPc1
rozhodli	rozhodnout	k5eAaPmAgMnP
pro	pro	k7c4
posunutí	posunutí	k1gNnSc4
data	datum	k1gNnSc2
do	do	k7c2
konce	konec	k1gInSc2
kalendářního	kalendářní	k2eAgInSc2d1
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
bylo	být	k5eAaImAgNnS
zvýšení	zvýšení	k1gNnSc1
stále	stále	k6eAd1
nízké	nízký	k2eAgFnSc2d1
prestiže	prestiž	k1gFnSc2
mezi	mezi	k7c7
událostmi	událost	k1gFnPc7
velké	velký	k2eAgFnSc2d1
čtyřky	čtyřka	k1gFnSc2
<g/>
,	,	kIx,
malé	malý	k2eAgFnPc1d1
odměny	odměna	k1gFnPc1
a	a	k8xC
snaha	snaha	k1gFnSc1
dohrát	dohrát	k5eAaPmF
turnaj	turnaj	k1gInSc4
ještě	ještě	k9
v	v	k7c6
prosinci	prosinec	k1gInSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
by	by	kYmCp3nS
znamenalo	znamenat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
kalendářní	kalendářní	k2eAgMnSc1d1
Grand	grand	k1gMnSc1
Slam	sláma	k1gFnPc2
bude	být	k5eAaImBp3nS
rozhodovat	rozhodovat	k5eAaImF
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
a	a	k8xC
nikoli	nikoli	k9
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
během	během	k7c2
US	US	kA
Open	Open	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
pořadatelé	pořadatel	k1gMnPc1
následující	následující	k2eAgFnSc2d1
ročník	ročník	k1gInSc1
zahájili	zahájit	k5eAaPmAgMnP
již	již	k6eAd1
3	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1977	#num#	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
ale	ale	k9
setkalo	setkat	k5eAaPmAgNnS
s	s	k7c7
negativní	negativní	k2eAgFnSc7d1
kritikou	kritika	k1gFnSc7
a	a	k8xC
absencí	absence	k1gFnSc7
nejlepších	dobrý	k2eAgMnPc2d3
tenistů	tenista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1982	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
se	se	k3xPyFc4
hrálo	hrát	k5eAaImAgNnS
uprostřed	uprostřed	k7c2
prosince	prosinec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
konci	konec	k1gInSc6
grandslamu	grandslam	k1gInSc2
1985	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozhodnutí	rozhodnutí	k1gNnSc3
<g/>
,	,	kIx,
že	že	k8xS
termín	termín	k1gInSc1
následujícího	následující	k2eAgInSc2d1
ročníku	ročník	k1gInSc2
se	se	k3xPyFc4
posune	posunout	k5eAaPmIp3nS
do	do	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
ledna	leden	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamenalo	znamenat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
nekonal	konat	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
sezóny	sezóna	k1gFnSc2
1987	#num#	k4
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
lednové	lednový	k2eAgNnSc1d1
datum	datum	k1gNnSc1
stálé	stálý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	se	k3xPyFc4
někteří	některý	k3yIgMnPc1
tenisté	tenista	k1gMnPc1
jako	jako	k9
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
a	a	k8xC
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
vyjádřili	vyjádřit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
lednový	lednový	k2eAgInSc1d1
termín	termín	k1gInSc1
je	být	k5eAaImIp3nS
po	po	k7c6
době	doba	k1gFnSc6
Vánoc	Vánoce	k1gFnPc2
a	a	k8xC
Nového	Nového	k2eAgInSc2d1
roku	rok	k1gInSc2
příliš	příliš	k6eAd1
brzký	brzký	k2eAgInSc1d1
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
pro	pro	k7c4
lepší	dobrý	k2eAgFnSc4d2
přípravu	příprava	k1gFnSc4
a	a	k8xC
získání	získání	k1gNnSc4
nejlepší	dobrý	k2eAgFnSc2d3
formy	forma	k1gFnSc2
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
vhodný	vhodný	k2eAgInSc1d1
posun	posun	k1gInSc1
na	na	k7c4
únor	únor	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2021	#num#	k4
průběh	průběh	k1gInSc4
ovlivnila	ovlivnit	k5eAaPmAgFnS
pandemie	pandemie	k1gFnSc1
covidu-	covidu-	k?
<g/>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvalifikace	kvalifikace	k1gFnSc1
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
grandslamu	grandslam	k1gInSc2
odehrály	odehrát	k5eAaPmAgInP
mimo	mimo	k7c4
vlastní	vlastní	k2eAgNnSc4d1
dějiště	dějiště	k1gNnSc4
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
konaly	konat	k5eAaImAgFnP
s	s	k7c7
třítýdenním	třítýdenní	k2eAgInSc7d1
předstihem	předstih	k1gInSc7
v	v	k7c4
Dauhá	Dauhý	k2eAgNnPc4d1
a	a	k8xC
Dubaji	Dubaj	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Kvůli	kvůli	k7c3
povinné	povinný	k2eAgFnSc3d1
14	#num#	k4
<g/>
denní	denní	k2eAgFnSc6d1
karanténě	karanténa	k1gFnSc6
osob	osoba	k1gFnPc2
přjíždějících	přjíždějící	k2eAgFnPc2d1
do	do	k7c2
Austrálie	Austrálie	k1gFnSc2
začal	začít	k5eAaPmAgMnS
australský	australský	k2eAgMnSc1d1
major	major	k1gMnSc1
poprvé	poprvé	k6eAd1
až	až	k9
v	v	k7c6
únoru	únor	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
rovněž	rovněž	k9
o	o	k7c4
první	první	k4xOgInSc4
grandslam	grandslam	k1gInSc4
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
,	,	kIx,
s	s	k7c7
využitím	využití	k1gNnSc7
systému	systém	k1gInSc2
elektronických	elektronický	k2eAgMnPc2d1
čárových	čárový	k2eAgMnPc2d1
rozhodčích	rozhodčí	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Panoráma	panoráma	k1gFnSc1
ještě	ještě	k6eAd1
nezastřešeného	zastřešený	k2eNgMnSc2d1
3	#num#	k4
<g/>
.	.	kIx.
největšího	veliký	k2eAgInSc2d3
dvorce	dvorec	k1gInSc2
Margaret	Margareta	k1gFnPc2
Court	Court	k1gInSc1
Arena	Aren	k1gInSc2
dříve	dříve	k6eAd2
pro	pro	k7c4
6	#num#	k4
000	#num#	k4
diváků	divák	k1gMnPc2
<g/>
,	,	kIx,
2008	#num#	k4
</s>
<s>
Mediální	mediální	k2eAgNnSc1d1
pokrytí	pokrytí	k1gNnSc1
</s>
<s>
Televizní	televizní	k2eAgInPc1d1
přenosy	přenos	k1gInPc1
byly	být	k5eAaImAgInP
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
zprostředkovány	zprostředkován	k2eAgInPc4d1
ze	z	k7c2
všech	všecek	k3xTgInPc2
17	#num#	k4
soutěžních	soutěžní	k2eAgInPc2d1
kurtů	kurt	k1gInPc2
a	a	k8xC
podruhé	podruhé	k6eAd1
také	také	k9
z	z	k7c2
8	#num#	k4
tréninkových	tréninkový	k2eAgInPc2d1
dvorců	dvorec	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Akreditováno	akreditovat	k5eAaBmNgNnS
bylo	být	k5eAaImAgNnS
413	#num#	k4
novinářů	novinář	k1gMnPc2
a	a	k8xC
173	#num#	k4
fotografů	fotograf	k1gMnPc2
ze	z	k7c2
43	#num#	k4
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
dvou	dva	k4xCgInPc2
týdnů	týden	k1gInPc2
se	se	k3xPyFc4
uskutečnilo	uskutečnit	k5eAaPmAgNnS
přes	přes	k7c4
tisíc	tisíc	k4xCgInSc4
tiskových	tiskový	k2eAgFnPc2d1
konferencí	konference	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přenosy	přenos	k1gInPc1
byly	být	k5eAaImAgInP
zprostředkovány	zprostředkovat	k5eAaPmNgInP
do	do	k7c2
více	hodně	k6eAd2
než	než	k8xS
220	#num#	k4
teritorií	teritorium	k1gNnPc2
skrze	skrze	k?
65	#num#	k4
televizních	televizní	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Austrálii	Austrálie	k1gFnSc6
byla	být	k5eAaImAgFnS
k	k	k7c3
roku	rok	k1gInSc3
2018	#num#	k4
držitelem	držitel	k1gMnSc7
práv	právo	k1gNnPc2
na	na	k7c4
živé	živý	k2eAgNnSc4d1
vysílání	vysílání	k1gNnSc4
stanice	stanice	k1gFnSc2
Seven	Seven	k2eAgInSc1d1
Network	network	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
prvním	první	k4xOgInSc6
programu	program	k1gInSc6
Channel	Channela	k1gFnPc2
Seven	Seven	k1gInSc4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
přenosu	přenos	k1gInSc3
většiny	většina	k1gFnSc2
utkání	utkání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
asijsko-pacifickém	asijsko-pacifický	k2eAgInSc6d1
regionu	region	k1gInSc6
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
televizní	televizní	k2eAgInPc4d1
kanály	kanál	k1gInPc4
CCTV	CCTV	kA
<g/>
,	,	kIx,
iQiyi	iQiy	k1gInPc7
<g/>
,	,	kIx,
Shanghai	Shangha	k1gInPc7
TV	TV	kA
(	(	kIx(
<g/>
ČLR	ČLR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sony	Sony	kA
SIX	SIX	kA
(	(	kIx(
<g/>
Indie	Indie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sky	Sky	k1gMnSc1
TV	TV	kA
(	(	kIx(
<g/>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Fox	fox	k1gInSc1
Sports	Sportsa	k1gFnPc2
Asia	Asi	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
evropském	evropský	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
byl	být	k5eAaImAgInS
signál	signál	k1gInSc1
šířen	šířit	k5eAaImNgInS
v	v	k7c6
síti	síť	k1gFnSc6
Eurosport	eurosport	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
Středním	střední	k2eAgInSc6d1
východu	východ	k1gInSc6
pak	pak	k6eAd1
turnaj	turnaj	k1gInSc1
k	k	k7c3
roku	rok	k1gInSc3
2017	#num#	k4
přenášely	přenášet	k5eAaImAgFnP
beIN	beIN	k?
Sports	Sports	k1gInSc1
a	a	k8xC
v	v	k7c6
subsaharské	subsaharský	k2eAgFnSc6d1
frankofonní	frankofonní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
stanice	stanice	k1gFnSc2
SuperSport	supersport	k1gInSc1
a	a	k8xC
Eurosport	eurosport	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokrytí	pokrytí	k1gNnPc1
na	na	k7c6
americkém	americký	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
zajišťovala	zajišťovat	k5eAaImAgFnS
ESPN	ESPN	kA
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInPc1
programy	program	k1gInPc1
ESPN2	ESPN2	k1gFnPc2
a	a	k8xC
ESPN3	ESPN3	k1gFnSc4
zprostředkovávaly	zprostředkovávat	k5eAaImAgInP
zápasy	zápas	k1gInPc1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
,	,	kIx,
ESPN	ESPN	kA
International	International	k1gFnSc1
vysílala	vysílat	k5eAaImAgFnS
v	v	k7c6
Latinské	latinský	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
a	a	k8xC
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
pak	pak	k6eAd1
kanál	kanál	k1gInSc4
TSN	TSN	kA
<g/>
/	/	kIx~
<g/>
RDS	RDS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
byly	být	k5eAaImAgFnP
zprostředkovány	zprostředkovat	k5eAaPmNgInP
živé	živý	k2eAgInPc1d1
přenosy	přenos	k1gInPc1
ze	z	k7c2
sedmi	sedm	k4xCc2
dvorců	dvorec	k1gInPc2
–	–	k?
Rod	rod	k1gInSc1
Laver	lavra	k1gFnPc2
Arena	Aren	k1gInSc2
<g/>
,	,	kIx,
Hisense	Hisense	k1gFnSc1
Arena	Areno	k1gNnSc2
<g/>
,	,	kIx,
Margaret	Margareta	k1gFnPc2
Court	Courta	k1gFnPc2
Arena	Areno	k1gNnSc2
a	a	k8xC
kurtů	kurt	k1gInPc2
č.	č.	k?
2	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
a	a	k8xC
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvalifikace	kvalifikace	k1gFnSc1
<g/>
,	,	kIx,
rozlosování	rozlosování	k1gNnSc4
a	a	k8xC
dětský	dětský	k2eAgInSc4d1
den	den	k1gInSc4
přenášela	přenášet	k5eAaImAgFnS
oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
AusOpen	AusOpen	k2eAgInSc4d1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poprvé	poprvé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
byl	být	k5eAaImAgInS
průběh	průběh	k1gInSc1
kvalifikací	kvalifikace	k1gFnPc2
hlavních	hlavní	k2eAgFnPc2d1
soutěží	soutěž	k1gFnPc2
přenášen	přenášen	k2eAgInSc1d1
živě	živě	k6eAd1
po	po	k7c6
internetu	internet	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Návštěvnost	návštěvnost	k1gFnSc1
</s>
<s>
Uvedena	uveden	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
celková	celkový	k2eAgFnSc1d1
návštěvnost	návštěvnost	k1gFnSc1
ročníků	ročník	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Sběrači	sběrač	k1gMnPc1
míčů	míč	k1gInPc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Australian	Australian	k1gInSc4
Open	Opena	k1gFnPc2
2020	#num#	k4
bylo	být	k5eAaImAgNnS
z	z	k7c2
více	hodně	k6eAd2
než	než	k8xS
2	#num#	k4
500	#num#	k4
zájemců	zájemce	k1gMnPc2
náborem	nábor	k1gInSc7
vybráno	vybrat	k5eAaPmNgNnS
360	#num#	k4
sběračů	sběrač	k1gMnPc2
<g/>
,	,	kIx,
chlapců	chlapec	k1gMnPc2
a	a	k8xC
dívek	dívka	k1gFnPc2
ve	v	k7c6
věku	věk	k1gInSc6
12	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
dvaceti	dvacet	k4xCc2
Korejců	Korejec	k1gMnPc2
<g/>
,	,	kIx,
deseti	deset	k4xCc2
Indů	Ind	k1gMnPc2
<g/>
,	,	kIx,
šesti	šest	k4xCc2
Číňanů	Číňan	k1gMnPc2
a	a	k8xC
dvou	dva	k4xCgMnPc2
Francouzů	Francouz	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1987	#num#	k4
–	–	k?
140	#num#	k4
089	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1988	#num#	k4
–	–	k?
244	#num#	k4
859	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1989	#num#	k4
–	–	k?
289	#num#	k4
023	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1990	#num#	k4
–	–	k?
312	#num#	k4
000	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1991	#num#	k4
–	–	k?
305	#num#	k4
048	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1992	#num#	k4
–	–	k?
329	#num#	k4
034	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1993	#num#	k4
–	–	k?
322	#num#	k4
074	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1994	#num#	k4
–	–	k?
332	#num#	k4
926	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1995	#num#	k4
–	–	k?
311	#num#	k4
678	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1996	#num#	k4
–	–	k?
389	#num#	k4
598	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1997	#num#	k4
–	–	k?
391	#num#	k4
504	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1998	#num#	k4
–	–	k?
434	#num#	k4
807	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1999	#num#	k4
–	–	k?
473	#num#	k4
296	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2000	#num#	k4
–	–	k?
501	#num#	k4
251	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2001	#num#	k4
–	–	k?
543	#num#	k4
834	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2002	#num#	k4
–	–	k?
518	#num#	k4
248	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2003	#num#	k4
–	–	k?
512	#num#	k4
225	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2004	#num#	k4
–	–	k?
521	#num#	k4
691	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2004	#num#	k4
–	–	k?
521	#num#	k4
691	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2005	#num#	k4
–	–	k?
543	#num#	k4
873	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2006	#num#	k4
–	–	k?
550	#num#	k4
550	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2007	#num#	k4
–	–	k?
554	#num#	k4
858	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2008	#num#	k4
–	–	k?
605	#num#	k4
735	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2009	#num#	k4
–	–	k?
603	#num#	k4
160	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2010	#num#	k4
–	–	k?
653	#num#	k4
860	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2011	#num#	k4
–	–	k?
651	#num#	k4
127	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2012	#num#	k4
–	–	k?
686	#num#	k4
006	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2013	#num#	k4
–	–	k?
684	#num#	k4
457	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2014	#num#	k4
–	–	k?
643	#num#	k4
280	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2015	#num#	k4
–	–	k?
703	#num#	k4
899	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2016	#num#	k4
–	–	k?
720	#num#	k4
363	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2017	#num#	k4
–	–	k?
728	#num#	k4
763	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2018	#num#	k4
–	–	k?
743	#num#	k4
667	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2019	#num#	k4
–	–	k?
780	#num#	k4
000	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2020	#num#	k4
–	–	k?
812	#num#	k4
174	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2021	#num#	k4
–	–	k?
130	#num#	k4
374	#num#	k4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
P	P	kA
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poháry	pohár	k1gInPc4
pro	pro	k7c4
vítěze	vítěz	k1gMnPc4
</s>
<s>
Poháry	pohár	k1gInPc4
pro	pro	k7c4
vítěze	vítěz	k1gMnPc4
</s>
<s>
Jména	jméno	k1gNnSc2
vítězů	vítěz	k1gMnPc2
příslušných	příslušný	k2eAgFnPc2d1
soutěží	soutěž	k1gFnPc2
jsou	být	k5eAaImIp3nP
vyryta	vyrýt	k5eAaPmNgNnP
na	na	k7c4
poháry	pohár	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
vítězce	vítězka	k1gFnSc3
ženské	ženský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
náleží	náležet	k5eAaImIp3nS
pohár	pohár	k1gInSc4
„	„	k?
<g/>
Daphne	Daphne	k1gFnSc2
Akhurst	Akhurst	k1gFnSc1
Memorial	Memorial	k1gMnSc1
Cup	cup	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
pojmenovaný	pojmenovaný	k2eAgMnSc1d1
po	po	k7c6
pětinásobné	pětinásobný	k2eAgFnSc6d1
šampiónce	šampiónka	k1gFnSc6
Daphne	Daphne	k1gFnSc2
Akhurstové	Akhurstová	k1gFnSc2
<g/>
,	,	kIx,
</s>
<s>
vítězi	vítěz	k1gMnPc7
mužské	mužský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
náleží	náležet	k5eAaImIp3nS
pohár	pohár	k1gInSc1
„	„	k?
<g/>
Norman	Norman	k1gMnSc1
Brookes	Brookes	k1gMnSc1
Challenge	Challenge	k1gFnPc2
Cup	cup	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
pojmenovaný	pojmenovaný	k2eAgMnSc1d1
po	po	k7c6
šampiónu	šampión	k1gMnSc6
Normanu	Norman	k1gMnSc6
Brookesovi	Brookes	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Daphne	Daphnout	k5eAaPmIp3nS
Akhurst	Akhurst	k1gMnSc1
Memorial	Memorial	k1gMnSc1
Cup	cup	k1gInSc4
</s>
<s>
Li	li	k9
Na	na	k7c6
a	a	k8xC
Stan	stan	k1gInSc4
Wawrinkavítězové	Wawrinkavítězové	k2eAgFnPc2d1
dvouher	dvouhra	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
</s>
<s>
Norman	Norman	k1gMnSc1
Brookes	Brookes	k1gMnSc1
Challenge	Challenge	k1gFnPc2
Cup	cup	k1gInSc4
</s>
<s>
Finanční	finanční	k2eAgFnPc1d1
odměny	odměna	k1gFnPc1
</s>
<s>
Celkový	celkový	k2eAgInSc1d1
rozpočet	rozpočet	k1gInSc1
Australian	Australiany	k1gInPc2
Open	Open	k1gInSc1
2021	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
výše	vysoce	k6eAd2
80	#num#	k4
miliónů	milión	k4xCgInPc2
australských	australský	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamenalo	znamenat	k5eAaImAgNnS
meziroční	meziroční	k2eAgInSc4d1
nárůst	nárůst	k1gInSc4
o	o	k7c4
12,7	12,7	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
vítězové	vítěz	k1gMnPc1
</s>
<s>
finalisté	finalista	k1gMnPc1
</s>
<s>
semifinalisté	semifinalista	k1gMnPc1
</s>
<s>
čtvrtfinalisté	čtvrtfinalista	k1gMnPc1
</s>
<s>
16	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
32	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
64	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
128	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
Q3	Q3	k4
</s>
<s>
Q2	Q2	k4
</s>
<s>
Q1	Q1	k4
</s>
<s>
dvouhry	dvouhra	k1gFnPc1
</s>
<s>
2	#num#	k4
750	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
1	#num#	k4
500	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
850	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
525	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
320	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
215	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
150	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
100	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
52	#num#	k4
500	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
35	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
25	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
čtyřhry	čtyřhra	k1gFnPc1
</s>
<s>
600	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
340	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
200	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
110	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
65	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
45	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
30	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
mix	mix	k1gInSc1
</s>
<s>
150	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
85	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
45	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
24	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
12	#num#	k4
000	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
6	#num#	k4
250	#num#	k4
A	A	kA
<g/>
$	$	kIx~
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
částky	částka	k1gFnPc1
ve	v	k7c6
čtyřhře	čtyřhra	k1gFnSc6
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgFnP
na	na	k7c4
pár	pár	k4xCyI
</s>
<s>
Panoráma	panoráma	k1gFnSc1
Rod	rod	k1gInSc1
Laver	lavra	k1gFnPc2
Arena	Aren	k1gInSc2
při	při	k7c6
ceremoniálu	ceremoniál	k1gInSc6
po	po	k7c6
finále	finále	k1gNnSc6
ženské	ženský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
<g/>
,	,	kIx,
2009	#num#	k4
</s>
<s>
Bodové	bodový	k2eAgNnSc1d1
hodnocení	hodnocení	k1gNnSc1
</s>
<s>
Tabulka	tabulka	k1gFnSc1
uvádí	uvádět	k5eAaImIp3nS
zisk	zisk	k1gInSc4
bodů	bod	k1gInPc2
do	do	k7c2
žebříčku	žebříček	k1gInSc2
ATP	atp	kA
a	a	k8xC
WTA	WTA	kA
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
kole	kolo	k1gNnSc6
turnaje	turnaj	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
tenista	tenista	k1gMnSc1
vypadl	vypadnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Dospělí	dospělí	k1gMnPc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
vítězové	vítěz	k1gMnPc1
</s>
<s>
finalisté	finalista	k1gMnPc1
</s>
<s>
semifinalisté	semifinalista	k1gMnPc1
</s>
<s>
čtvrtfinalisté	čtvrtfinalista	k1gMnPc1
</s>
<s>
16	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
32	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
64	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
128	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
Q	Q	kA
</s>
<s>
Q3	Q3	k4
</s>
<s>
Q2	Q2	k4
</s>
<s>
Q1	Q1	k4
</s>
<s>
dvouhra	dvouhra	k1gFnSc1
mužů	muž	k1gMnPc2
</s>
<s>
2000	#num#	k4
</s>
<s>
1200	#num#	k4
</s>
<s>
720	#num#	k4
</s>
<s>
360	#num#	k4
</s>
<s>
180	#num#	k4
</s>
<s>
90	#num#	k4
</s>
<s>
45	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
čtyřhra	čtyřhra	k1gFnSc1
mužů	muž	k1gMnPc2
</s>
<s>
0	#num#	k4
</s>
<s>
N	N	kA
<g/>
/	/	kIx~
<g/>
A	A	kA
</s>
<s>
dvouhra	dvouhra	k1gFnSc1
žen	žena	k1gFnPc2
</s>
<s>
1300	#num#	k4
</s>
<s>
780	#num#	k4
</s>
<s>
430	#num#	k4
</s>
<s>
240	#num#	k4
</s>
<s>
130	#num#	k4
</s>
<s>
70	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
40	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
čtyřhra	čtyřhra	k1gFnSc1
žen	žena	k1gFnPc2
</s>
<s>
10	#num#	k4
</s>
<s>
N	N	kA
<g/>
/	/	kIx~
<g/>
A	A	kA
</s>
<s>
Junioři	junior	k1gMnPc1
</s>
<s>
vítězové	vítěz	k1gMnPc1
</s>
<s>
finalisté	finalista	k1gMnPc1
</s>
<s>
semifinalisté	semifinalista	k1gMnPc1
</s>
<s>
čtvrtfinalisté	čtvrtfinalista	k1gMnPc1
</s>
<s>
16	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
32	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
Q	Q	kA
</s>
<s>
Q3	Q3	k4
</s>
<s>
N	N	kA
<g/>
/	/	kIx~
<g/>
A	A	kA
</s>
<s>
dvouhra	dvouhra	k1gFnSc1
juniorů	junior	k1gMnPc2
</s>
<s>
1000	#num#	k4
</s>
<s>
600	#num#	k4
</s>
<s>
370	#num#	k4
</s>
<s>
200	#num#	k4
</s>
<s>
100	#num#	k4
</s>
<s>
45	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
dvouhra	dvouhra	k1gFnSc1
juniorek	juniorka	k1gFnPc2
</s>
<s>
čtyřhra	čtyřhra	k1gFnSc1
juniorů	junior	k1gMnPc2
</s>
<s>
750	#num#	k4
</s>
<s>
450	#num#	k4
</s>
<s>
275	#num#	k4
</s>
<s>
150	#num#	k4
</s>
<s>
75	#num#	k4
</s>
<s>
N	N	kA
<g/>
/	/	kIx~
<g/>
A	A	kA
</s>
<s>
čtyřhra	čtyřhra	k1gFnSc1
juniorek	juniorka	k1gFnPc2
</s>
<s>
Vozíčkáři	vozíčkář	k1gMnPc1
</s>
<s>
vítězové	vítěz	k1gMnPc1
</s>
<s>
finalisté	finalista	k1gMnPc1
</s>
<s>
semifinalisté	semifinalista	k1gMnPc1
</s>
<s>
čtvrtfinalisté	čtvrtfinalista	k1gMnPc1
</s>
<s>
N	N	kA
<g/>
/	/	kIx~
<g/>
A	A	kA
</s>
<s>
dvouhra	dvouhra	k1gFnSc1
</s>
<s>
800	#num#	k4
</s>
<s>
500	#num#	k4
</s>
<s>
375	#num#	k4
</s>
<s>
100	#num#	k4
</s>
<s>
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
800	#num#	k4
</s>
<s>
500	#num#	k4
</s>
<s>
100	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
dvouhra	dvouhra	k1gFnSc1
kvadroplegiků	kvadroplegik	k1gMnPc2
</s>
<s>
800	#num#	k4
</s>
<s>
500	#num#	k4
</s>
<s>
100	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
čtyřhra	čtyřhra	k1gFnSc1
kvadroplegiků	kvadroplegik	k1gMnPc2
</s>
<s>
800	#num#	k4
</s>
<s>
100	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
Vítězové	vítěz	k1gMnPc1
a	a	k8xC
finalisté	finalista	k1gMnPc1
roku	rok	k1gInSc2
2021	#num#	k4
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Australian	Australiana	k1gFnPc2
Open	Open	k1gNnSc1
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vítězové	vítěz	k1gMnPc1
a	a	k8xC
finalisté	finalista	k1gMnPc1
Australian	Australiana	k1gFnPc2
Open	Openo	k1gNnPc2
2021	#num#	k4
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Vítězové	vítěz	k1gMnPc1
</s>
<s>
Finalisté	finalista	k1gMnPc1
</s>
<s>
Výsledek	výsledek	k1gInSc1
</s>
<s>
Mužská	mužský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
Daniil	Daniil	k1gMnSc1
Medveděv	Medveděv	k1gMnSc1
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Ženská	ženský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
Naomi	Nao	k1gFnPc7
Ósakaová	Ósakaová	k1gFnSc1
Jennifer	Jennifer	k1gInSc1
Bradyová	Bradyová	k1gFnSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Mužská	mužský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
Ivan	Ivan	k1gMnSc1
Dodig	Dodig	k1gMnSc1
Filip	Filip	k1gMnSc1
Polášek	Polášek	k1gMnSc1
Rajeev	Rajeva	k1gFnPc2
Ram	Ram	k1gMnSc1
Joe	Joe	k1gMnSc1
Salisbury	Salisbura	k1gFnSc2
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Ženská	ženský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
Elise	elise	k1gFnSc2
Mertensová	Mertensový	k2eAgFnSc1d1
Aryna	Aryen	k2eAgFnSc1d1
Sabalenková	Sabalenkový	k2eAgFnSc1d1
Barbora	Barbora	k1gFnSc1
Krejčíková	Krejčíková	k1gFnSc1
Kateřina	Kateřina	k1gFnSc1
Siniaková	Siniakový	k2eAgFnSc1d1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
Smíšená	smíšený	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
Barbora	Barbora	k1gFnSc1
Krejčíková	Krejčíková	k1gFnSc1
Rajeev	Rajeev	k1gFnSc1
Ram	Ram	k1gFnSc1
Samantha	Samantha	k1gFnSc1
Stosurová	stosurový	k2eAgFnSc1d1
Matthew	Matthew	k1gFnSc1
Ebden	Ebden	k1gInSc1
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Galerie	galerie	k1gFnSc1
vítězů	vítěz	k1gMnPc2
</s>
<s>
Novak	Novak	k6eAd1
Djokovićmužská	Djokovićmužský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
</s>
<s>
Naomi	Nao	k1gFnPc7
Ósakaováženská	Ósakaováženský	k2eAgNnPc4d1
dvouhra	dvouhra	k1gFnSc1
</s>
<s>
Ivan	Ivan	k1gMnSc1
Dodigmužská	Dodigmužský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
Polášekmužská	Polášekmužský	k2eAgNnPc5d1
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
Elise	elise	k1gFnSc1
Mertensováženská	Mertensováženský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
Aryna	Aryen	k2eAgFnSc1d1
Sabalenkováženská	Sabalenkováženský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
Barbora	Barbora	k1gFnSc1
Krejčíkovásmíšená	Krejčíkovásmíšená	k1gFnSc1
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
Rajeev	Rajeev	k1gFnSc1
Ramsmíšená	Ramsmíšená	k1gFnSc1
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
Přehled	přehled	k1gInSc1
rekordů	rekord	k1gInPc2
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
tří	tři	k4xCgInPc2
zbývajících	zbývající	k2eAgInPc2d1
grandslamů	grandslam	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
umožnily	umožnit	k5eAaPmAgFnP
přístup	přístup	k1gInSc4
profesionálům	profesionál	k1gMnPc3
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
<g/>
,	,	kIx,
australský	australský	k2eAgInSc1d1
grandslam	grandslam	k1gInSc1
se	s	k7c7
profesionálním	profesionální	k2eAgNnSc7d1
tenistům	tenista	k1gMnPc3
otevřel	otevřít	k5eAaPmAgInS
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přehled	přehled	k1gInSc1
rekordů	rekord	k1gInPc2
podle	podle	k7c2
titulů	titul	k1gInPc2
na	na	k7c4
Australian	Australian	k1gInSc4
Open	Opena	k1gFnPc2
</s>
<s>
Rekord	rekord	k1gInSc1
</s>
<s>
Éra	éra	k1gFnSc1
</s>
<s>
Tenista	tenista	k1gMnSc1
</s>
<s>
Počet	počet	k1gInSc1
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Muži	muž	k1gMnPc1
od	od	k7c2
roku	rok	k1gInSc2
1905	#num#	k4
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
titulů	titul	k1gInPc2
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
</s>
<s>
do	do	k7c2
1968	#num#	k4
</s>
<s>
Roy	Roy	k1gMnSc1
Emerson	Emerson	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
1961	#num#	k4
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
,	,	kIx,
1966	#num#	k4
<g/>
,	,	kIx,
1967	#num#	k4
</s>
<s>
od	od	k7c2
1969	#num#	k4
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
9	#num#	k4
</s>
<s>
2008	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
,	,	kIx,
2021	#num#	k4
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
titulů	titul	k1gInPc2
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
v	v	k7c6
řadě	řada	k1gFnSc6
</s>
<s>
do	do	k7c2
1968	#num#	k4
</s>
<s>
Roy	Roy	k1gMnSc1
Emerson	Emerson	k1gMnSc1
</s>
<s>
5	#num#	k4
</s>
<s>
1963	#num#	k4
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
,	,	kIx,
1966	#num#	k4
<g/>
,	,	kIx,
1967	#num#	k4
</s>
<s>
od	od	k7c2
1969	#num#	k4
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
2011	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
,	,	kIx,
20132019	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
,	,	kIx,
2021	#num#	k4
</s>
<s>
Nejvíce	nejvíce	k6eAd1,k6eAd3
titulů	titul	k1gInPc2
ve	v	k7c6
čtyřhře	čtyřhra	k1gFnSc6
</s>
<s>
do	do	k7c2
1968	#num#	k4
</s>
<s>
Adrian	Adrian	k1gMnSc1
Quist	Quist	k1gMnSc1
</s>
<s>
10	#num#	k4
</s>
<s>
1936	#num#	k4
<g/>
,	,	kIx,
1937	#num#	k4
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
,	,	kIx,
1939	#num#	k4
<g/>
,	,	kIx,
1940	#num#	k4
<g/>
,	,	kIx,
1946	#num#	k4
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
,	,	kIx,
1948	#num#	k4
<g/>
,	,	kIx,
1949	#num#	k4
<g/>
,	,	kIx,
1950	#num#	k4
</s>
<s>
od	od	k7c2
1969	#num#	k4
</s>
<s>
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
Mike	Mike	k1gFnPc2
Bryan	Bryan	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
2006	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
2006	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
titulů	titul	k1gInPc2
ve	v	k7c6
čtyřhře	čtyřhra	k1gFnSc6
v	v	k7c6
řadě	řada	k1gFnSc6
</s>
<s>
do	do	k7c2
1968	#num#	k4
</s>
<s>
Adrian	Adrian	k1gMnSc1
Quist	Quist	k1gMnSc1
</s>
<s>
10	#num#	k4
</s>
<s>
1936	#num#	k4
<g/>
,	,	kIx,
1937	#num#	k4
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
,	,	kIx,
1939	#num#	k4
<g/>
,	,	kIx,
1940	#num#	k4
<g/>
,	,	kIx,
1946	#num#	k4
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
,	,	kIx,
1948	#num#	k4
<g/>
,	,	kIx,
1949	#num#	k4
<g/>
,	,	kIx,
1950	#num#	k4
<g/>
[	[	kIx(
<g/>
P	P	kA
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
od	od	k7c2
1969	#num#	k4
</s>
<s>
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
Mike	Mike	k1gFnPc2
Bryan	Bryan	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
2009	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
2009	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
titulů	titul	k1gInPc2
v	v	k7c6
mixu	mix	k1gInSc6
mezi	mezi	k7c7
muži	muž	k1gMnPc7
</s>
<s>
do	do	k7c2
1968	#num#	k4
</s>
<s>
Harry	Harra	k1gMnSc2
Hopman	Hopman	k1gMnSc1
Colin	Colin	k1gMnSc1
Long	Long	k1gMnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
1930	#num#	k4
<g/>
,	,	kIx,
1936	#num#	k4
<g/>
,	,	kIx,
1937	#num#	k4
<g/>
,	,	kIx,
19391940	#num#	k4
<g/>
,	,	kIx,
1946	#num#	k4
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
,	,	kIx,
1948	#num#	k4
</s>
<s>
od	od	k7c2
1969	#num#	k4
</s>
<s>
Jim	on	k3xPp3gMnPc3
Pugh	Pugh	k1gInSc1
Leander	Leander	k1gMnSc1
Paes	Paesa	k1gFnPc2
Daniel	Daniel	k1gMnSc1
Nestor	Nestor	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
1988	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
,	,	kIx,
19902003	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
20152007	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
titulů	titul	k1gInPc2
celkově	celkově	k6eAd1
mezi	mezi	k7c7
muži	muž	k1gMnPc7
<g/>
(	(	kIx(
<g/>
dvouhra	dvouhra	k1gFnSc1
<g/>
,	,	kIx,
čtyřhra	čtyřhra	k1gFnSc1
<g/>
,	,	kIx,
mix	mix	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
do	do	k7c2
1968	#num#	k4
</s>
<s>
Adrian	Adrian	k1gMnSc1
Quist	Quist	k1gMnSc1
</s>
<s>
13	#num#	k4
</s>
<s>
1936	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
dvouhra	dvouhra	k1gFnSc1
<g/>
,	,	kIx,
10	#num#	k4
mužská	mužský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
<g/>
,	,	kIx,
0	#num#	k4
mix	mix	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
od	od	k7c2
1969	#num#	k4
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
9	#num#	k4
</s>
<s>
2008	#num#	k4
<g/>
–	–	k?
<g/>
2021	#num#	k4
(	(	kIx(
<g/>
mužská	mužský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Ženy	žena	k1gFnPc1
od	od	k7c2
roku	rok	k1gInSc2
1922	#num#	k4
</s>
<s>
Nejvíce	nejvíce	k6eAd1,k6eAd3
titulů	titul	k1gInPc2
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
</s>
<s>
do	do	k7c2
1968	#num#	k4
</s>
<s>
Margaret	Margareta	k1gFnPc2
Courtová	Courtová	k1gFnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
1960	#num#	k4
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
,	,	kIx,
1962	#num#	k4
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
,	,	kIx,
1966	#num#	k4
</s>
<s>
od	od	k7c2
1969	#num#	k4
</s>
<s>
Serena	Seren	k2eAgFnSc1d1
Williamsová	Williamsová	k1gFnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
2003	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
titulů	titul	k1gInPc2
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
v	v	k7c6
řadě	řada	k1gFnSc6
</s>
<s>
do	do	k7c2
1968	#num#	k4
</s>
<s>
Margaret	Margareta	k1gFnPc2
Courtová	Courtová	k1gFnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
1960	#num#	k4
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
,	,	kIx,
1962	#num#	k4
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
,	,	kIx,
1966	#num#	k4
</s>
<s>
od	od	k7c2
1969	#num#	k4
</s>
<s>
Margaret	Margareta	k1gFnPc2
Courtová	Courtová	k1gFnSc1
Evonne	Evonn	k1gInSc5
Goolagongová	Goolagongový	k2eAgFnSc1d1
Steffi	Steffi	k1gFnSc2
Grafová	grafový	k2eAgFnSc1d1
<g/>
/	/	kIx~
Monika	Monika	k1gFnSc1
Selešová	Selešová	k1gFnSc1
Martina	Martina	k1gFnSc1
Hingisová	Hingisový	k2eAgFnSc1d1
</s>
<s>
3	#num#	k4
</s>
<s>
1969	#num#	k4
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
,	,	kIx,
1971	#num#	k4
1974	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
1988	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
1991	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
1997	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
titulů	titul	k1gInPc2
ve	v	k7c6
čtyřhře	čtyřhra	k1gFnSc6
</s>
<s>
do	do	k7c2
1968	#num#	k4
</s>
<s>
Thelma	Thelma	k1gFnSc1
Coyne	Coyn	k1gMnSc5
Longová	Longový	k2eAgFnSc5d1
</s>
<s>
12	#num#	k4
</s>
<s>
1936	#num#	k4
<g/>
,	,	kIx,
1937	#num#	k4
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
,	,	kIx,
1939	#num#	k4
<g/>
,	,	kIx,
1940	#num#	k4
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
,	,	kIx,
1948	#num#	k4
<g/>
,	,	kIx,
1949	#num#	k4
<g/>
,	,	kIx,
1951	#num#	k4
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
,	,	kIx,
1958	#num#	k4
</s>
<s>
od	od	k7c2
1969	#num#	k4
</s>
<s>
Martina	Martina	k1gFnSc1
Navrátilová	Navrátilová	k1gFnSc1
</s>
<s>
8	#num#	k4
</s>
<s>
1980	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
</s>
<s>
Nejvíce	nejvíce	k6eAd1,k6eAd3
titulů	titul	k1gInPc2
ve	v	k7c6
čtyřhře	čtyřhra	k1gFnSc6
v	v	k7c6
řadě	řada	k1gFnSc6
</s>
<s>
do	do	k7c2
1968	#num#	k4
</s>
<s>
Thelma	Thelma	k1gFnSc1
Coyne	Coyn	k1gInSc5
Longová	Longový	k2eAgFnSc1d1
Nancye	Nancyus	k1gInSc5
Wynne	Wynn	k1gInSc5
Boltonová	Boltonový	k2eAgNnPc4d1
</s>
<s>
5	#num#	k4
</s>
<s>
1936	#num#	k4
<g/>
,	,	kIx,
1937	#num#	k4
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
,	,	kIx,
1939	#num#	k4
<g/>
,	,	kIx,
19401936	#num#	k4
<g/>
,	,	kIx,
1937	#num#	k4
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
,	,	kIx,
1939	#num#	k4
<g/>
,	,	kIx,
1940	#num#	k4
</s>
<s>
od	od	k7c2
1969	#num#	k4
</s>
<s>
Martina	Martina	k1gFnSc1
Navrátilová	Navrátilová	k1gFnSc1
Pam	Pam	k1gFnSc1
Shriverová	Shriverová	k1gFnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
1982	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
1982	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
</s>
<s>
Nejvíce	hodně	k6eAd3,k6eAd1
titulů	titul	k1gInPc2
v	v	k7c6
mixu	mix	k1gInSc6
mezi	mezi	k7c7
ženami	žena	k1gFnPc7
</s>
<s>
do	do	k7c2
1968	#num#	k4
</s>
<s>
Daphne	Daphnout	k5eAaPmIp3nS
Akhurst	Akhurst	k1gFnSc1
Cozensová	Cozensová	k1gFnSc1
Nell	Nell	k1gInSc4
Hall	Hall	k1gInSc1
Hopmanová	Hopmanová	k1gFnSc1
Nancye	Nancy	k1gMnSc2
Wynne	Wynn	k1gInSc5
Boltonová	Boltonový	k2eAgFnSc1d1
Thelma	Thelma	k1gFnSc1
Coyne	Coyn	k1gInSc5
Longová	Longový	k2eAgFnSc5d1
</s>
<s>
4	#num#	k4
</s>
<s>
1924	#num#	k4
<g/>
,	,	kIx,
1925	#num#	k4
<g/>
,	,	kIx,
1928	#num#	k4
<g/>
,	,	kIx,
19291930	#num#	k4
<g/>
,	,	kIx,
1936	#num#	k4
<g/>
,	,	kIx,
1937	#num#	k4
<g/>
,	,	kIx,
19391940	#num#	k4
<g/>
,	,	kIx,
1946	#num#	k4
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
,	,	kIx,
1948	#num#	k4
1951	#num#	k4
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
,	,	kIx,
1954	#num#	k4
<g/>
,	,	kIx,
1955	#num#	k4
</s>
<s>
od	od	k7c2
1969	#num#	k4
</s>
<s>
Barbora	Barbora	k1gFnSc1
Krejčíková	Krejčíková	k1gFnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
2019	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
,	,	kIx,
2021	#num#	k4
</s>
<s>
Nejvíce	nejvíce	k6eAd1,k6eAd3
titulů	titul	k1gInPc2
celkově	celkově	k6eAd1
mezi	mezi	k7c7
ženami	žena	k1gFnPc7
<g/>
(	(	kIx(
<g/>
dvouhra	dvouhra	k1gFnSc1
<g/>
,	,	kIx,
čtyřhra	čtyřhra	k1gFnSc1
<g/>
,	,	kIx,
mix	mix	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
do	do	k7c2
1968	#num#	k4
</s>
<s>
Nancye	Nancye	k6eAd1
Wynneová	Wynneový	k2eAgFnSc1d1
</s>
<s>
20	#num#	k4
</s>
<s>
1936	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
(	(	kIx(
<g/>
6	#num#	k4
dvouhra	dvouhra	k1gFnSc1
<g/>
,	,	kIx,
10	#num#	k4
ženská	ženský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
<g/>
,	,	kIx,
4	#num#	k4
mix	mix	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
od	od	k7c2
1969	#num#	k4
</s>
<s>
Martina	Martina	k1gFnSc1
Navrátilová	Navrátilová	k1gFnSc1
</s>
<s>
12	#num#	k4
</s>
<s>
1980	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
dvouhra	dvouhra	k1gFnSc1
<g/>
,	,	kIx,
8	#num#	k4
ženská	ženský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
<g/>
,	,	kIx,
1	#num#	k4
mix	mix	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
na	na	k7c4
Australian	Australian	k1gInSc4
Open	Opena	k1gFnPc2
</s>
<s>
Rekord	rekord	k1gInSc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Tenista	tenista	k1gMnSc1
</s>
<s>
Popis	popis	k1gInSc1
rekordu	rekord	k1gInSc2
</s>
<s>
Nejmladší	mladý	k2eAgMnPc1d3
vítězové	vítěz	k1gMnPc1
</s>
<s>
mužská	mužský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
</s>
<s>
Ken	Ken	k?
Rosewall	Rosewall	k1gInSc1
</s>
<s>
v	v	k7c6
18	#num#	k4
letech	let	k1gInPc6
a	a	k8xC
2	#num#	k4
měsících	měsíc	k1gInPc6
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
mužská	mužský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
Lew	Lew	k?
Hoad	Hoad	k1gInSc1
</s>
<s>
v	v	k7c6
18	#num#	k4
letech	let	k1gInPc6
a	a	k8xC
2	#num#	k4
měsících	měsíc	k1gInPc6
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ženská	ženský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
</s>
<s>
Martina	Martina	k1gFnSc1
Hingisová	Hingisový	k2eAgFnSc1d1
</s>
<s>
v	v	k7c6
16	#num#	k4
letech	let	k1gInPc6
a	a	k8xC
4	#num#	k4
měsících	měsíc	k1gInPc6
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ženská	ženský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
Mirjana	Mirjana	k1gFnSc1
Lučićová	Lučićová	k1gFnSc1
</s>
<s>
v	v	k7c6
15	#num#	k4
letech	let	k1gInPc6
a	a	k8xC
10	#num#	k4
měsících	měsíc	k1gInPc6
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejstarší	starý	k2eAgMnPc1d3
vítězové	vítěz	k1gMnPc1
</s>
<s>
mužská	mužský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
</s>
<s>
Ken	Ken	k?
Rosewall	Rosewall	k1gInSc1
</s>
<s>
v	v	k7c6
37	#num#	k4
letech	let	k1gInPc6
a	a	k8xC
8	#num#	k4
měsících	měsíc	k1gInPc6
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
mužská	mužský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
Norman	Norman	k1gMnSc1
Brookes	Brookes	k1gMnSc1
</s>
<s>
ve	v	k7c6
46	#num#	k4
letech	let	k1gInPc6
a	a	k8xC
2	#num#	k4
měsících	měsíc	k1gInPc6
(	(	kIx(
<g/>
1924	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ženská	ženský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
</s>
<s>
Thelma	Thelma	k1gFnSc1
Coyne	Coyn	k1gMnSc5
Longová	Longový	k2eAgFnSc5d1
</s>
<s>
v	v	k7c6
35	#num#	k4
letech	let	k1gInPc6
a	a	k8xC
8	#num#	k4
měsících	měsíc	k1gInPc6
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ženská	ženský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
Thelma	Thelma	k1gFnSc1
Coyne	Coyn	k1gInSc5
Longová	Longový	k2eAgFnSc5d1
</s>
<s>
v	v	k7c6
39	#num#	k4
letech	let	k1gInPc6
a	a	k8xC
3	#num#	k4
měsících	měsíc	k1gInPc6
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
smíšená	smíšený	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
(	(	kIx(
<g/>
muži	muž	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
Horace	Horace	k1gFnSc1
Rice	Ric	k1gFnSc2
</s>
<s>
v	v	k7c6
52	#num#	k4
letech	léto	k1gNnPc6
(	(	kIx(
<g/>
1923	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
smíšená	smíšený	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
(	(	kIx(
<g/>
ženy	žena	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Martina	Martina	k1gFnSc1
Navrátilová	Navrátilová	k1gFnSc1
</s>
<s>
ve	v	k7c6
46	#num#	k4
letech	let	k1gInPc6
a	a	k8xC
3	#num#	k4
měsících	měsíc	k1gInPc6
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejdelší	dlouhý	k2eAgInSc1d3
zápas	zápas	k1gInSc1
</s>
<s>
bez	bez	k7c2
rozdílu	rozdíl	k1gInSc2
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
</s>
<s>
5	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
53	#num#	k4
minutfinále	minutfinále	k1gNnPc2
mužské	mužský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
2012	#num#	k4
</s>
<s>
Nejpozdněji	pozdně	k6eAd3
rozehraný	rozehraný	k2eAgInSc1d1
zápas	zápas	k1gInSc1
</s>
<s>
bez	bez	k7c2
rozdílu	rozdíl	k1gInSc2
</s>
<s>
Grigor	Grigora	k1gFnPc2
Dimitrov	Dimitrovo	k1gNnPc2
Richard	Richard	k1gMnSc1
Gasquet	Gasquet	k1gMnSc1
</s>
<s>
ve	v	k7c6
23.58	23.58	k4
hodin	hodina	k1gFnPc2
<g/>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
mužské	mužský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přehled	přehled	k1gInSc1
singlových	singlový	k2eAgInPc2d1
titulů	titul	k1gInPc2
v	v	k7c6
otevřené	otevřený	k2eAgFnSc6d1
éře	éra	k1gFnSc6
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
je	být	k5eAaImIp3nS
držitelem	držitel	k1gMnSc7
rekordních	rekordní	k2eAgFnPc2d1
devíti	devět	k4xCc2
trofejí	trofej	k1gFnPc2
mezi	mezi	k7c7
muži	muž	k1gMnPc7
</s>
<s>
Mužská	mužský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
</s>
<s>
Tenista	tenista	k1gMnSc1
</s>
<s>
stát	stát	k1gInSc1
</s>
<s>
titulů	titul	k1gInPc2
</s>
<s>
roky	rok	k1gInPc4
</s>
<s>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
9	#num#	k4
</s>
<s>
2008	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
,	,	kIx,
2021	#num#	k4
</s>
<s>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
2004	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
</s>
<s>
Andre	Andr	k1gMnSc5
Agassi	Agass	k1gMnSc5
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
4	#num#	k4
</s>
<s>
1995	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
</s>
<s>
Mats	Mats	k6eAd1
Wilander	Wilander	k1gInSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
1983	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
</s>
<s>
Ženská	ženský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
</s>
<s>
Tenistka	tenistka	k1gFnSc1
</s>
<s>
stát	stát	k1gInSc1
</s>
<s>
titulů	titul	k1gInPc2
</s>
<s>
roky	rok	k1gInPc4
</s>
<s>
Serena	Seren	k2eAgFnSc1d1
Williamsová	Williamsová	k1gFnSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
7	#num#	k4
</s>
<s>
2003	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
</s>
<s>
Evonne	Evonnout	k5eAaImIp3nS,k5eAaPmIp3nS
Goolagongová	Goolagongový	k2eAgFnSc1d1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
1974	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
,	,	kIx,
1977	#num#	k4
</s>
<s>
Margaret	Margareta	k1gFnPc2
Smithová	Smithová	k1gFnSc1
Courtová	Courtová	k1gFnSc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
1969	#num#	k4
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
,	,	kIx,
1973	#num#	k4
</s>
<s>
Monika	Monika	k1gFnSc1
Selešová	Selešová	k1gFnSc1
</s>
<s>
Jugoslávie	Jugoslávie	k1gFnSc1
/	/	kIx~
USA	USA	kA
</s>
<s>
4	#num#	k4
</s>
<s>
1991	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
</s>
<s>
Steffi	Steffi	k1gFnSc1
Grafová	Grafová	k1gFnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
1988	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
</s>
<s>
Martina	Martina	k1gFnSc1
Navrátilová	Navrátilová	k1gFnSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
3	#num#	k4
</s>
<s>
1981	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
</s>
<s>
Martina	Martina	k1gFnSc1
Hingisová	Hingisový	k2eAgFnSc1d1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
1997	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
</s>
<s>
Československá	československý	k2eAgFnSc1d1
a	a	k8xC
česká	český	k2eAgFnSc1d1
stopa	stopa	k1gFnSc1
</s>
<s>
Československé	československý	k2eAgNnSc1d1
finále	finále	k1gNnSc1
mužské	mužský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
Melbourne	Melbourne	k1gNnSc6
Parku	park	k1gInSc6
uskutečnilo	uskutečnit	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Ivan	Ivan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
porazil	porazit	k5eAaPmAgMnS
Miloše	Miloš	k1gMnSc4
Mečíře	mečíř	k1gMnSc4
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petr	Petr	k1gMnSc1
Korda	Korda	k1gMnSc1
vyhrál	vyhrát	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
mužskou	mužský	k2eAgFnSc4d1
čtyřhru	čtyřhra	k1gFnSc4
se	s	k7c7
Švédem	Švéd	k1gMnSc7
Edbergem	Edberg	k1gMnSc7
a	a	k8xC
poté	poté	k6eAd1
roku	rok	k1gInSc2
1998	#num#	k4
triumfoval	triumfovat	k5eAaBmAgMnS
také	také	k9
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Hana	Hana	k1gFnSc1
Mandlíková	Mandlíková	k1gFnSc1
získala	získat	k5eAaPmAgFnS
dva	dva	k4xCgInPc4
singlové	singlový	k2eAgInPc4d1
tituly	titul	k1gInPc4
<g/>
,	,	kIx,
když	když	k8xS
ve	v	k7c6
finále	finále	k1gNnSc6
roku	rok	k1gInSc2
1980	#num#	k4
zdolala	zdolat	k5eAaPmAgFnS
Australanku	Australanka	k1gFnSc4
Wendy	Wenda	k1gFnSc2
Turnbullovou	Turnbullová	k1gFnSc7
a	a	k8xC
o	o	k7c4
sedm	sedm	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
také	také	k9
Martinu	Martin	k2eAgFnSc4d1
Navrátilovou	Navrátilová	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jana	Jana	k1gFnSc1
Novotná	Novotná	k1gFnSc1
si	se	k3xPyFc3
připsala	připsat	k5eAaPmAgFnS
celkem	celkem	k6eAd1
tři	tři	k4xCgInPc4
trofeje	trofej	k1gInPc4
(	(	kIx(
<g/>
1995	#num#	k4
–	–	k?
ženská	ženský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
<g/>
;	;	kIx,
1988	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
–	–	k?
smíšená	smíšený	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Helena	Helena	k1gFnSc1
Suková	Suková	k1gFnSc1
dvě	dva	k4xCgNnPc1
vítězství	vítězství	k1gNnPc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
–	–	k?
ženská	ženský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Radek	Radek	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
triumfoval	triumfovat	k5eAaBmAgMnS
s	s	k7c7
Indem	Ind	k1gMnSc7
Paesem	Paes	k1gMnSc7
v	v	k7c6
mužské	mužský	k2eAgFnSc6d1
čtyřhře	čtyřhra	k1gFnSc6
roku	rok	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lucie	Lucie	k1gFnSc1
Šafářová	Šafářová	k1gFnSc1
ovládla	ovládnout	k5eAaPmAgFnS
ženskou	ženský	k2eAgFnSc4d1
čtyřhru	čtyřhra	k1gFnSc4
v	v	k7c6
letech	léto	k1gNnPc6
2015	#num#	k4
a	a	k8xC
2017	#num#	k4
v	v	k7c6
páru	pár	k1gInSc6
s	s	k7c7
Američankou	Američanka	k1gFnSc7
Bethanií	Bethanie	k1gFnSc7
Mattekovou-Sandsovou	Mattekovou-Sandsová	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
smíšené	smíšený	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
vybojovala	vybojovat	k5eAaPmAgFnS
Barbora	Barbora	k1gFnSc1
Krejčíková	Krejčíková	k1gFnSc1
tři	tři	k4xCgInPc4
tituly	titul	k1gInPc4
za	za	k7c7
sebou	se	k3xPyFc7
v	v	k7c6
letech	léto	k1gNnPc6
2019	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
a	a	k8xC
2021	#num#	k4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
první	první	k4xOgFnSc7
ženou	žena	k1gFnSc7
od	od	k7c2
Margaret	Margareta	k1gFnPc2
Courtové	Courtová	k1gFnSc2
z	z	k7c2
let	léto	k1gNnPc2
1963	#num#	k4
<g/>
–	–	k?
<g/>
1965	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
výkon	výkon	k1gInSc1
podařil	podařit	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
tenistka	tenistka	k1gFnSc1
Iveta	Iveta	k1gFnSc1
Benešová	Benešová	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
</s>
<s>
Mužská	mužský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
vítěz	vítěz	k1gMnSc1
</s>
<s>
finalista	finalista	k1gMnSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
</s>
<s>
1983	#num#	k4
</s>
<s>
Mats	Mats	k6eAd1
Wilander	Wilander	k1gInSc1
</s>
<s>
Ivan	Ivan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
1989	#num#	k4
</s>
<s>
Ivan	Ivan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
</s>
<s>
Miloslav	Miloslav	k1gMnSc1
Mečíř	mečíř	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
1990	#num#	k4
</s>
<s>
Ivan	Ivan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
</s>
<s>
Stefan	Stefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
1991	#num#	k4
</s>
<s>
Boris	Boris	k1gMnSc1
Becker	Becker	k1gMnSc1
</s>
<s>
Ivan	Ivan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
1998	#num#	k4
</s>
<s>
Petr	Petr	k1gMnSc1
Korda	Korda	k1gMnSc1
</s>
<s>
Marcelo	Marcela	k1gFnSc5
Ríos	Ríos	k1gInSc4
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
Ženská	ženský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
vítězka	vítězka	k1gFnSc1
</s>
<s>
finalistka	finalistka	k1gFnSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
</s>
<s>
1976	#num#	k4
</s>
<s>
Evonne	Evonnout	k5eAaPmIp3nS,k5eAaImIp3nS
Goolagongová	Goolagongový	k2eAgFnSc1d1
</s>
<s>
Renáta	Renáta	k1gFnSc1
Tomanová	Tomanová	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
1980	#num#	k4
</s>
<s>
Hana	Hana	k1gFnSc1
Mandlíková	Mandlíková	k1gFnSc1
</s>
<s>
Wendy	Wendy	k6eAd1
Turnbullová	Turnbullový	k2eAgFnSc1d1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
1984	#num#	k4
</s>
<s>
Chris	Chris	k1gFnSc1
Evertová	Evertová	k1gFnSc1
</s>
<s>
Helena	Helena	k1gFnSc1
Suková	Suková	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
1987	#num#	k4
</s>
<s>
Hana	Hana	k1gFnSc1
Mandlíková	Mandlíková	k1gFnSc1
</s>
<s>
Martina	Martina	k1gFnSc1
Navrátilová	Navrátilová	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1989	#num#	k4
</s>
<s>
Steffi	Steffi	k1gFnSc1
Grafová	Grafová	k1gFnSc1
</s>
<s>
Helena	Helena	k1gFnSc1
Suková	Suková	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
1991	#num#	k4
</s>
<s>
Monika	Monika	k1gFnSc1
Selešová	Selešová	k1gFnSc1
</s>
<s>
Jana	Jana	k1gFnSc1
Novotná	Novotná	k1gFnSc1
</s>
<s>
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
</s>
<s>
2019	#num#	k4
</s>
<s>
Naomi	Nao	k1gFnPc7
Ósakaová	Ósakaová	k1gFnSc1
</s>
<s>
Petra	Petra	k1gFnSc1
Kvitová	Kvitová	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Počet	počet	k1gInSc1
diváků	divák	k1gMnPc2
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
2021	#num#	k4
omezen	omezit	k5eAaPmNgMnS
pro	pro	k7c4
koronavirovou	koronavirový	k2eAgFnSc4d1
pandemii	pandemie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náštěvnost	Náštěvnost	k1gFnSc1
byla	být	k5eAaImAgFnS
snížena	snížit	k5eAaPmNgFnS
na	na	k7c4
30	#num#	k4
<g/>
–	–	k?
<g/>
50	#num#	k4
%	%	kIx~
<g/>
;	;	kIx,
diváci	divák	k1gMnPc1
měli	mít	k5eAaImAgMnP
do	do	k7c2
areálu	areál	k1gInSc2
přístup	přístup	k1gInSc4
devět	devět	k4xCc1
ze	z	k7c2
čtrnácti	čtrnáct	k4xCc2
dní	den	k1gInPc2
grandslamu	grandslam	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
V	v	k7c6
letech	let	k1gInPc6
1941	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
se	se	k3xPyFc4
Australian	Australian	k1gInSc1
Championships	Championshipsa	k1gFnPc2
nekonal	konat	k5eNaImAgInS
pro	pro	k7c4
druhou	druhý	k4xOgFnSc4
světovou	světový	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
ILANBEY	ILANBEY	kA
<g/>
,	,	kIx,
Sumeyya	Sumeyyum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
courts	courtsa	k1gFnPc2
rolled	rolled	k1gMnSc1
out	out	k?
ahead	ahead	k6eAd1
of	of	k?
major	major	k1gMnSc1
tennis	tennis	k1gFnSc2
tournaments	tournaments	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Age	Age	k1gMnSc1
<g/>
,	,	kIx,
2019-07-26	2019-07-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Australian	Australian	k1gInSc1
Open	Open	k1gNnSc1
2015	#num#	k4
to	ten	k3xDgNnSc1
stage	stagat	k5eAaPmIp3nS
revamped	revamped	k1gInSc1
Margaret	Margareta	k1gFnPc2
Court	Court	k1gInSc1
Arena	Aren	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GiveMeSport	GiveMeSport	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
:	:	kIx,
Final-set	Final-set	k1gMnSc1
tie-breaks	tie-breaks	k6eAd1
to	ten	k3xDgNnSc1
be	be	k?
used	used	k1gInSc1
in	in	k?
2019	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
21	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
John	John	k1gMnSc1
Millman	Millman	k1gMnSc1
joins	joinsa	k1gFnPc2
Bernard	Bernard	k1gMnSc1
Tomic	Tomic	k1gMnSc1
in	in	k?
deriding	deriding	k1gInSc1
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
balls	balls	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
News	News	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
<g/>
au	au	k0
<g/>
,	,	kIx,
2019-01-11	2019-01-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
AO	AO	kA
Press	Press	k1gInSc1
Release	Releas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
2018	#num#	k4
–	–	k?
By	by	k9
the	the	k?
numbers	numbers	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnSc1
Tourtalk	Tourtalka	k1gFnPc2
<g/>
,	,	kIx,
2018-01-28	2018-01-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Event	Eventa	k1gFnPc2
Guide	Guid	k1gInSc5
<g/>
:	:	kIx,
Essentials	Essentials	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Increased	Increased	k1gInSc1
prize	prize	k1gFnSc2
money	monea	k1gFnSc2
for	forum	k1gNnPc2
AO	AO	kA
2019	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnSc1
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Djokovic	Djokovice	k1gFnPc2
Wins	Winsa	k1gFnPc2
Eighth	Eighth	k1gInSc1
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
Crown	Crown	k1gMnSc1
<g/>
,	,	kIx,
Returns	Returns	k1gInSc1
To	to	k9
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2020-02-02	2020-02-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Australian	Australiany	k1gInPc2
Tennis	Tennis	k1gFnSc1
Open	Open	k1gInSc1
History	Histor	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jazzsports	Jazzsports	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
30	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Tristan	Tristan	k1gInSc1
Foenander	Foenander	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
History	Histor	k1gInPc7
of	of	k?
the	the	k?
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
–	–	k?
the	the	k?
Grand	grand	k1gMnSc1
Slam	sláma	k1gFnPc2
of	of	k?
Asia	Asia	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Pacific	Pacific	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Australasian	Australasian	k1gInSc1
Championships	Championships	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Sydney	Sydney	k1gNnSc2
Morning	Morning	k1gInSc1
Herald	Heraldo	k1gNnPc2
<g/>
,	,	kIx,
9	#num#	k4
November	November	k1gInSc1
1923	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Frank	Frank	k1gMnSc1
Cook	Cook	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Open	Open	k1gInSc1
began	began	k1gInSc4
as	as	k9
Aussie	Aussie	k1gFnSc1
closed	closed	k1gMnSc1
shop	shop	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Daily	Daila	k1gFnSc2
Telegraph	Telegraph	k1gInSc1
<g/>
.	.	kIx.
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
<g/>
au	au	k0
<g/>
,	,	kIx,
14	#num#	k4
February	Februara	k1gFnSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
6	#num#	k4
December	December	k1gInSc1
2012	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Brumby	Brumba	k1gFnSc2
Government	Government	k1gMnSc1
announces	announces	k1gInSc1
Melbourne	Melbourne	k1gNnSc6
Park	park	k1gInSc1
redevelopment	redevelopment	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Herald	Herald	k1gInSc1
Sun	Sun	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australia	Australium	k1gNnPc1
<g/>
:	:	kIx,
26	#num#	k4
January	Januara	k1gFnSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
29	#num#	k4
May	May	k1gMnSc1
2012	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://www.australianopen.com/en_AU/news/articles/2010-01-19/201001191263860753359.html?fpos=r2	http://www.australianopen.com/en_AU/news/articles/2010-01-19/201001191263860753359.html?fpos=r2	k4
<g/>
↑	↑	k?
Anthony	Anthona	k1gFnSc2
Frederick	Frederick	k1gMnSc1
Wilding	Wilding	k1gInSc1
"	"	kIx"
<g/>
Tony	Tony	k1gMnSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gFnSc1
Tennis	Tennis	k1gFnSc2
Hall	Hall	k1gInSc1
of	of	k?
Fame	Fame	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
History	Histor	k1gInPc1
of	of	k?
Tennis	Tennis	k1gInSc1
–	–	k?
From	From	k1gInSc1
humble	humble	k6eAd1
beginnings	beginnings	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnSc1
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Milton	Milton	k1gInSc1
Tennis	Tennis	k1gInSc1
Centre	centr	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australian	Australian	k1gInSc1
Stadiums	Stadiums	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nikki	Nikki	k1gNnSc2
Tugwell	Tugwella	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hewitt	Hewitt	k2eAgInSc1d1
chases	chases	k1gInSc1
amazing	amazing	k1gInSc1
slam	slam	k1gInSc4
win	win	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Daily	Daila	k1gFnSc2
Telegraph	Telegraph	k1gInSc1
<g/>
.	.	kIx.
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
<g/>
au	au	k0
<g/>
,	,	kIx,
14	#num#	k4
January	Januara	k1gFnSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
29	#num#	k4
May	May	k1gMnSc1
2012	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alan	Alan	k1gMnSc1
Trengove	Trengov	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
1983	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
wilandertribute	wilandertribut	k1gMnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
World	World	k1gMnSc1
Group	Group	k1gMnSc1
1983	#num#	k4
Final	Final	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Davis	Davis	k1gInSc1
Cup	cup	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Rebound	Rebound	k1gMnSc1
Ace	Ace	k1gMnSc1
under	under	k1gMnSc1
review	review	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Daily	Daila	k1gFnSc2
Telegraph	Telegraph	k1gInSc1
<g/>
.	.	kIx.
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
<g/>
au	au	k0
<g/>
,	,	kIx,
29	#num#	k4
January	Januara	k1gFnSc2
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
29	#num#	k4
May	May	k1gMnSc1
2012	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Serena	Serena	k1gFnSc1
je	být	k5eAaImIp3nS
nejstarší	starý	k2eAgFnSc7d3
šampionkou	šampionka	k1gFnSc7
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
a	a	k8xC
slaví	slavit	k5eAaImIp3nS
už	už	k6eAd1
19	#num#	k4
<g/>
.	.	kIx.
grandslamový	grandslamový	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tenisportal	Tenisportal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-01-31	2015-01-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Serena	Sereno	k1gNnSc2
Wins	Winsa	k1gFnPc2
Historic	Historice	k1gInPc2
19	#num#	k4
<g/>
th	th	k?
Major	major	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Melbourne	Melbourne	k1gNnSc1
<g/>
:	:	kIx,
WTA	WTA	kA
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2015-02-01	2015-02-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Matt	Matt	k1gMnSc1
Trollope	Trollop	k1gInSc5
<g/>
.	.	kIx.
#	#	kIx~
<g/>
RF	RF	kA
<g/>
20	#num#	k4
<g/>
:	:	kIx,
The	The	k1gFnPc2
road	road	k6eAd1
to	ten	k3xDgNnSc4
Roger	Roger	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
mystical	mysticat	k5eAaPmAgMnS
tally	tall	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnSc1
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Federer	Federer	k1gInSc1
wins	wins	k1gInSc1
record	record	k1gInSc1
20	#num#	k4
<g/>
th	th	k?
Grand	grand	k1gMnSc1
Slam	slam	k1gInSc4
title	titla	k1gFnSc3
with	with	k1gInSc1
five-set	five-set	k1gMnSc1
win	win	k?
over	over	k1gMnSc1
Cilic	Cilic	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
28-01-2018	28-01-2018	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dominant	dominanta	k1gFnPc2
Novak	Novak	k1gMnSc1
Djokovic	Djokovice	k1gFnPc2
Seals	Seals	k1gInSc1
Historic	Historic	k1gMnSc1
Ninth	Ninth	k1gMnSc1
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
Crown	Crown	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2021-02-21	2021-02-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Venue	Venue	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Rod	rod	k1gInSc1
Laver	lavra	k1gFnPc2
Arena	Aren	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austadiums	Austadiums	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
John	John	k1gMnSc1
Cain	Cain	k1gMnSc1
Arena	Arena	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austadiums	Austadiums	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Margaret	Margareta	k1gFnPc2
Court	Court	k1gInSc1
Arena	Aren	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austadiums	Austadiums	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SCHLINK	SCHLINK	kA
<g/>
,	,	kIx,
Leo	Leo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
keen	keen	k1gMnSc1
to	ten	k3xDgNnSc4
call	calnout	k5eAaPmAgMnS
time	time	k6eAd1
on	on	k3xPp3gMnSc1
early	earl	k1gMnPc4
slam	slam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Herald	Herald	k1gInSc1
Sun	Sun	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australia	Australium	k1gNnPc1
<g/>
:	:	kIx,
17	#num#	k4
January	Januara	k1gFnSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
29	#num#	k4
May	May	k1gMnSc1
2012	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Doha	Doha	k1gFnSc1
to	ten	k3xDgNnSc4
host	host	k1gMnSc1
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
2021	#num#	k4
men	men	k?
<g/>
’	’	k?
<g/>
s	s	k7c7
qualifying	qualifying	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
ausopen	ausopen	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dubai	Duba	k1gFnSc2
to	ten	k3xDgNnSc1
host	host	k1gMnSc1
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
2021	#num#	k4
women	women	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
qualifying	qualifying	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
ausopen	ausopen	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Australian	Australiany	k1gInPc2
Open	Open	k1gInSc1
Set	sto	k4xCgNnPc2
For	forum	k1gNnPc2
Historic	Historic	k1gMnSc1
Start	start	k1gInSc1
<g/>
,	,	kIx,
Total	totat	k5eAaImAgMnS
Prize	Priza	k1gFnSc3
Pool	Poolum	k1gNnPc2
Revealed	Revealed	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2020-12-19	2020-12-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GOODWIN	GOODWIN	kA
<g/>
,	,	kIx,
Sam	Sam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
makes	makes	k1gMnSc1
world-first	world-first	k1gMnSc1
change	change	k1gFnSc2
after	after	k1gInSc1
Novak	Novak	k1gInSc1
Djokovic	Djokovice	k1gFnPc2
drama	dramo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Yahoo	Yahoo	k1gNnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Sport	sport	k1gInSc1
<g/>
,	,	kIx,
2021-02-04	2021-02-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Seven	Seven	k1gInSc1
Tennis	Tennis	k1gFnSc1
2016	#num#	k4
<g/>
:	:	kIx,
summer	summrat	k5eAaPmRp2nS
guide	guid	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
17-12-2015	17-12-2015	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Video	video	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AusOpen	AusOpen	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
25	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
2014	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Watch	Watch	k1gMnSc1
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
qualifying	qualifying	k1gInSc4
live	liv	k1gInSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
www.australianopen.com	www.australianopen.com	k1gInSc4
<g/>
.	.	kIx.
australianopen	australianopen	k2eAgInSc4d1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
11	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tournament	Tournament	k1gMnSc1
Info	Info	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AusOpen	AusOpen	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lawn	Lawn	k1gInSc1
Tennis	Tennis	k1gInSc1
Association	Association	k1gInSc1
of	of	k?
Australia	Australia	k1gFnSc1
Annual	Annual	k1gInSc1
Report	report	k1gInSc1
1987	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lawn	Lawn	k1gInSc4
Tennis	Tennis	k1gFnSc2
Association	Association	k1gInSc1
of	of	k?
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lawn	Lawn	k1gInSc1
Tennis	Tennis	k1gInSc1
Association	Association	k1gInSc1
of	of	k?
Australia	Australia	k1gFnSc1
Annual	Annual	k1gInSc1
Report	report	k1gInSc1
1988	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lawn	Lawn	k1gInSc4
Tennis	Tennis	k1gFnSc2
Association	Association	k1gInSc1
of	of	k?
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lawn	Lawn	k1gInSc1
Tennis	Tennis	k1gInSc1
Association	Association	k1gInSc1
of	of	k?
Australia	Australia	k1gFnSc1
Annual	Annual	k1gInSc1
Report	report	k1gInSc1
1989	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lawn	Lawna	k1gFnPc2
Tennis	Tennis	k1gFnPc2
Association	Association	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tennis	Tennis	k1gFnSc1
Australia	Australia	k1gFnSc1
Annual	Annual	k1gInSc4
Report	report	k1gInSc1
1990	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnSc1
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
26	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2017	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tennis	Tennis	k1gInSc1
Australia	Australia	k1gFnSc1
1991	#num#	k4
Annual	Annual	k1gInSc1
Report	report	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnSc1
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tennis	Tennis	k1gInSc1
Australia	Australia	k1gFnSc1
1992	#num#	k4
Annual	Annual	k1gInSc1
Report	report	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnSc1
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tennis	Tennis	k1gInSc1
Australia	Australia	k1gFnSc1
1993	#num#	k4
Annual	Annual	k1gInSc1
Report	report	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnSc1
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tennis	Tennis	k1gInSc1
Australia	Australia	k1gFnSc1
1994	#num#	k4
Annual	Annual	k1gInSc1
Report	report	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnSc1
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tennis	Tennis	k1gInSc1
Australia	Australia	k1gFnSc1
1995	#num#	k4
Annual	Annual	k1gInSc1
Report	report	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnSc1
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tennis	Tennis	k1gInSc1
Australia	Australia	k1gFnSc1
1996	#num#	k4
Annual	Annual	k1gInSc1
Report	report	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnSc1
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tennis	Tennis	k1gInSc1
Australia	Australia	k1gFnSc1
Annual	Annual	k1gMnSc1
Report	report	k1gInSc1
<g/>
,	,	kIx,
1997	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnSc1
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Tennis	Tennis	k1gFnPc2
Australia	Australia	k1gFnSc1
Annual	Annual	k1gInSc1
Report	report	k1gInSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnSc1
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tennis	Tennis	k1gInSc1
Australia	Australia	k1gFnSc1
Annual	Annual	k1gMnSc1
Report	report	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnSc1
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tennis	Tennis	k1gInSc1
Australia	Australia	k1gFnSc1
Annual	Annual	k1gMnSc1
Report	report	k1gInSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnSc1
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tennis	Tennis	k1gInSc1
Australia	Australia	k1gFnSc1
Annual	Annual	k1gMnSc1
Report	report	k1gInSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnSc1
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
26	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2017	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tennis	Tennis	k1gInSc1
Australia	Australia	k1gFnSc1
Annual	Annual	k1gMnSc1
Report	report	k1gInSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnSc1
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
Australian	Australiana	k1gFnPc2
Open	Opena	k1gFnPc2
Tennis	Tennis	k1gInSc1
Attendance	Attendanec	k1gInSc2
History	Histor	k1gInPc4
Archivováno	archivován	k2eAgNnSc4d1
16	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
–	–	k?
Altius	Altius	k1gInSc1
Directory	Director	k1gInPc1
<g/>
↑	↑	k?
Safin	Safin	k1gInSc1
credits	credits	k1gInSc1
Lundgren	Lundgrna	k1gFnPc2
for	forum	k1gNnPc2
resurgence	resurgenec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sports	Sports	k1gInSc1
Illustrated	Illustrated	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
<g/>
,	,	kIx,
30	#num#	k4
January	Januara	k1gFnSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
25	#num#	k4
January	Januara	k1gFnSc2
2008	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
AO	AO	kA
2007	#num#	k4
<g/>
:	:	kIx,
The	The	k1gFnSc1
Final	Final	k1gMnSc1
Word	Word	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnSc1
Australia	Australia	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
18	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gMnSc1
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
–	–	k?
History	Histor	k1gInPc1
of	of	k?
Attendance	Attendanec	k1gInSc2
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Australian	Australian	k1gInSc1
Open	Open	k1gNnSc1
2009	#num#	k4
–	–	k?
the	the	k?
final	final	k1gMnSc1
word	word	k1gMnSc1
<g/>
↑	↑	k?
Federer	Federer	k1gMnSc1
wins	wins	k6eAd1
fourth	fourth	k1gMnSc1
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
<g/>
,	,	kIx,
16	#num#	k4
<g/>
th	th	k?
major	major	k1gMnSc1
singles	singles	k1gMnSc1
title	titla	k1gFnSc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
31	#num#	k4
January	Januara	k1gFnSc2
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Closing	Closing	k1gInSc1
notes	notes	k1gInSc1
<g/>
:	:	kIx,
Australian	Australian	k1gInSc1
Open	Open	k1gNnSc1
2011	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
30	#num#	k4
January	Januara	k1gFnSc2
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Top	topit	k5eAaImRp2nS
10	#num#	k4
<g/>
:	:	kIx,
Memorable	Memorable	k1gFnSc1
AO2012	AO2012	k1gMnSc1
moments	moments	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
29	#num#	k4
January	Januara	k1gFnSc2
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Australian	Australian	k1gInSc1
Open	Open	k1gNnSc1
2013	#num#	k4
–	–	k?
The	The	k1gMnSc1
Final	Final	k1gMnSc1
Word	Word	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
28	#num#	k4
January	Januara	k1gFnSc2
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
AO	AO	kA
2014	#num#	k4
–	–	k?
The	The	k1gMnSc1
Final	Final	k1gMnSc1
Word	Word	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
27	#num#	k4
January	Januara	k1gFnSc2
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Australian	Australian	k1gInSc1
Open	Open	k1gNnSc1
2015	#num#	k4
–	–	k?
The	The	k1gMnSc1
final	final	k1gMnSc1
word	wordo	k1gNnPc2
from	fro	k1gNnSc7
Tennis	Tennis	k1gFnSc1
Australia	Australium	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
February	Februara	k1gFnSc2
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
What	What	k1gMnSc1
We	We	k1gMnSc1
Learned	Learned	k1gMnSc1
from	from	k1gMnSc1
the	the	k?
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
1	#num#	k4
February	Februara	k1gFnSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
Glance	Glance	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
30	#num#	k4
January	Januara	k1gFnSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
smashes	smashes	k1gMnSc1
attendance	attendance	k1gFnSc2
records	records	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
28	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Total	totat	k5eAaImAgMnS
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
Attendance	Attendance	k1gFnSc1
<g/>
:	:	kIx,
130,374	130,374	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Twitter	Twitter	k1gInSc1
[	[	kIx(
<g/>
account	account	k1gInSc1
Austadiums	Austadiums	k1gInSc1
Sport	sport	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2021-02-21	2021-02-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Australian	Australian	k1gInSc1
Open	Open	k1gMnSc1
Prize	Prize	k1gFnSc1
Money	Monea	k1gFnSc2
2021	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.perfect-tennis.com	www.perfect-tennis.com	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Australian	Australian	k1gInSc1
History	Histor	k1gInPc1
and	and	k?
Records	Records	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
TennisTours	TennisTours	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
21	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Australian	Australian	k1gInSc1
Open	Open	k1gNnSc1
2017	#num#	k4
<g/>
:	:	kIx,
Start	start	k1gInSc1
time	timat	k5eAaPmIp3nS
record	record	k6eAd1
broken	broken	k2eAgInSc1d1
as	as	k1gInSc1
Grigor	Grigor	k1gInSc1
Dimitrov	Dimitrov	k1gInSc1
defeats	defeatsa	k1gFnPc2
Richard	Richard	k1gMnSc1
Gasquet	Gasquet	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Age	Age	k1gMnSc1
<g/>
,	,	kIx,
22-01-2017	22-01-2017	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Luboš	Luboš	k1gMnSc1
Zabloudil	Zabloudil	k1gMnSc1
<g/>
,	,	kIx,
TenisPortal	TenisPortal	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krejčíková	Krejčíková	k1gFnSc1
vyhrála	vyhrát	k5eAaPmAgFnS
třetí	třetí	k4xOgInSc4
rok	rok	k1gInSc4
po	po	k7c6
sobě	se	k3xPyFc3
smíšenou	smíšený	k2eAgFnSc4d1
čtyřhru	čtyřhra	k1gFnSc4
na	na	k7c4
Australian	Australian	k1gInSc4
Open	Opena	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
www.tenisportal.cz	www.tenisportal.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-02-20	2021-02-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Australian	Australiany	k1gInPc2
Open	Opena	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
–	–	k?
oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Tenis	tenis	k1gInSc1
Organizace	organizace	k1gFnSc2
</s>
<s>
ITF	ITF	kA
</s>
<s>
ATP	atp	kA
</s>
<s>
WTA	WTA	kA
</s>
<s>
ČTS	ČTS	kA
</s>
<s>
FFT	fft	k0
</s>
<s>
TA	ten	k3xDgFnSc1
</s>
<s>
USTA	USTA	kA
Okruhy	okruh	k1gInPc1
</s>
<s>
mužské	mužský	k2eAgNnSc1d1
</s>
<s>
ATP	atp	kA
Tour	Tour	k1gMnSc1
</s>
<s>
ATP	atp	kA
Challenger	Challenger	k1gMnSc1
Tour	Tour	k1gMnSc1
</s>
<s>
ITF	ITF	kA
Men	Men	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
World	World	k1gInSc1
Tennis	Tennis	k1gFnSc2
Tour	Toura	k1gFnPc2
ženské	ženský	k2eAgFnSc2d1
</s>
<s>
WTA	WTA	kA
Tour	Tour	k1gMnSc1
</s>
<s>
WTA	WTA	kA
125	#num#	k4
<g/>
s	s	k7c7
</s>
<s>
ITF	ITF	kA
Women	Women	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
World	World	k1gInSc1
Tennis	Tennis	k1gFnSc1
Tour	Tour	k1gMnSc1
</s>
<s>
Grand	grand	k1gMnSc1
Slam	sláma	k1gFnPc2
</s>
<s>
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
</s>
<s>
French	French	k1gMnSc1
Open	Open	k1gMnSc1
</s>
<s>
Wimbledon	Wimbledon	k1gInSc1
</s>
<s>
US	US	kA
Open	Open	k1gMnSc1
Údery	úder	k1gInPc1
</s>
<s>
bekhend	bekhend	k1gInSc1
</s>
<s>
čop	čop	k?
</s>
<s>
forhend	forhend	k1gInSc1
</s>
<s>
halfvolej	halfvolej	k1gInSc1
</s>
<s>
lob	lob	k1gInSc1
</s>
<s>
podání	podání	k1gNnSc1
</s>
<s>
smeč	smeč	k1gFnSc1
</s>
<s>
topspin	topspin	k1gInSc1
</s>
<s>
volej	volej	k1gInSc1
</s>
<s>
zkrácení	zkrácení	k1gNnSc4
hry	hra	k1gFnSc2
Dvorce	dvorec	k1gInPc4
a	a	k8xC
povrchy	povrch	k1gInPc4
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
tráva	tráva	k1gFnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgMnSc1d1
(	(	kIx(
<g/>
DecoTurf	DecoTurf	k1gMnSc1
•	•	k?
GreenSet	GreenSet	k1gMnSc1
•	•	k?
Laykold	Laykold	k1gMnSc1
•	•	k?
Plexicushion	Plexicushion	k1gInSc1
•	•	k?
Rebound	Rebound	k1gInSc1
Ace	Ace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
hala	hala	k1gFnSc1
Vybavení	vybavení	k1gNnSc2
</s>
<s>
míč	míč	k1gInSc1
(	(	kIx(
<g/>
melton	melton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
raketa	raketa	k1gFnSc1
</s>
<s>
výplet	výplet	k1gInSc1
Týmové	týmový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
</s>
<s>
Davis	Davis	k1gInSc1
Cup	cup	k1gInSc1
</s>
<s>
Billie	Billie	k1gFnSc1
Jean	Jean	k1gMnSc1
King	King	k1gMnSc1
Cup	cup	k1gInSc4
</s>
<s>
Hopmanův	Hopmanův	k2eAgInSc4d1
pohár	pohár	k1gInSc4
</s>
<s>
ATP	atp	kA
Cup	cup	k1gInSc1
</s>
<s>
Laver	lavra	k1gFnPc2
Cup	cup	k1gInSc1
</s>
<s>
Světový	světový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
družstev	družstvo	k1gNnPc2
</s>
<s>
Galeův	Galeův	k2eAgInSc4d1
pohár	pohár	k1gInSc4
Ostatní	ostatní	k2eAgFnSc2d1
</s>
<s>
Letní	letní	k2eAgFnPc1d1
olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
<s>
terminologie	terminologie	k1gFnSc1
</s>
<s>
rozhodčí	rozhodčí	k1gMnSc1
</s>
<s>
jestřábí	jestřábí	k2eAgNnSc4d1
oko	oko	k1gNnSc4
</s>
<s>
žebříček	žebříček	k1gInSc4
ATP	atp	kA
a	a	k8xC
WTA	WTA	kA
</s>
<s>
historie	historie	k1gFnSc1
tenisu	tenis	k1gInSc2
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
tenisová	tenisový	k2eAgFnSc1d1
síň	síň	k1gFnSc1
slávy	sláva	k1gFnSc2
</s>
<s>
Australian	Australian	k1gInSc1
Open	Open	k1gNnSc1
Championships	Championships	k1gInSc1
éra	éra	k1gFnSc1
</s>
<s>
1905	#num#	k4
•	•	k?
1906	#num#	k4
•	•	k?
1907	#num#	k4
•	•	k?
1908	#num#	k4
•	•	k?
1909	#num#	k4
•	•	k?
1910	#num#	k4
•	•	k?
1911	#num#	k4
•	•	k?
1912	#num#	k4
•	•	k?
1913	#num#	k4
•	•	k?
1914	#num#	k4
•	•	k?
1915	#num#	k4
•	•	k?
1916	#num#	k4
•	•	k?
1917	#num#	k4
•	•	k?
1918	#num#	k4
•	•	k?
1919	#num#	k4
•	•	k?
1920	#num#	k4
•	•	k?
1921	#num#	k4
•	•	k?
1922	#num#	k4
•	•	k?
1923	#num#	k4
•	•	k?
1924	#num#	k4
•	•	k?
1925	#num#	k4
•	•	k?
1926	#num#	k4
•	•	k?
1927	#num#	k4
•	•	k?
1928	#num#	k4
•	•	k?
1929	#num#	k4
•	•	k?
1930	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1931	#num#	k4
•	•	k?
1932	#num#	k4
•	•	k?
1933	#num#	k4
•	•	k?
1934	#num#	k4
•	•	k?
1935	#num#	k4
•	•	k?
1936	#num#	k4
•	•	k?
1937	#num#	k4
•	•	k?
1938	#num#	k4
•	•	k?
1939	#num#	k4
•	•	k?
1940	#num#	k4
•	•	k?
1941	#num#	k4
•	•	k?
1942	#num#	k4
•	•	k?
1943	#num#	k4
•	•	k?
1944	#num#	k4
•	•	k?
1945	#num#	k4
•	•	k?
1946	#num#	k4
•	•	k?
1947	#num#	k4
•	•	k?
1948	#num#	k4
•	•	k?
1949	#num#	k4
•	•	k?
1950	#num#	k4
•	•	k?
1951	#num#	k4
•	•	k?
1952	#num#	k4
•	•	k?
1953	#num#	k4
•	•	k?
1954	#num#	k4
•	•	k?
1955	#num#	k4
•	•	k?
1956	#num#	k4
•	•	k?
1957	#num#	k4
•	•	k?
1958	#num#	k4
•	•	k?
1959	#num#	k4
•	•	k?
1960	#num#	k4
•	•	k?
1961	#num#	k4
•	•	k?
1962	#num#	k4
•	•	k?
1963	#num#	k4
•	•	k?
1964	#num#	k4
•	•	k?
1965	#num#	k4
•	•	k?
1966	#num#	k4
•	•	k?
1967	#num#	k4
•	•	k?
1968	#num#	k4
Open	Openo	k1gNnPc2
éra	éra	k1gFnSc1
</s>
<s>
1969	#num#	k4
•	•	k?
1970	#num#	k4
•	•	k?
1971	#num#	k4
•	•	k?
1972	#num#	k4
•	•	k?
1973	#num#	k4
•	•	k?
1974	#num#	k4
•	•	k?
1975	#num#	k4
•	•	k?
1976	#num#	k4
•	•	k?
I	I	kA
<g/>
/	/	kIx~
<g/>
1977	#num#	k4
•	•	k?
XII	XII	kA
<g/>
/	/	kIx~
<g/>
1977	#num#	k4
•	•	k?
1978	#num#	k4
•	•	k?
1979	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1981	#num#	k4
•	•	k?
1982	#num#	k4
•	•	k?
1983	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
1985	#num#	k4
•	•	k?
1986	#num#	k4
•	•	k?
1987	#num#	k4
•	•	k?
1988	#num#	k4
•	•	k?
1989	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1990	#num#	k4
•	•	k?
1991	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1993	#num#	k4
•	•	k?
1994	#num#	k4
•	•	k?
1995	#num#	k4
•	•	k?
1996	#num#	k4
•	•	k?
1997	#num#	k4
•	•	k?
1998	#num#	k4
•	•	k?
1999	#num#	k4
•	•	k?
2000	#num#	k4
•	•	k?
2001	#num#	k4
•	•	k?
2002	#num#	k4
•	•	k?
2003	#num#	k4
•	•	k?
2004	#num#	k4
•	•	k?
2005	#num#	k4
•	•	k?
2006	#num#	k4
•	•	k?
2007	#num#	k4
•	•	k?
2008	#num#	k4
•	•	k?
2009	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2011	#num#	k4
•	•	k?
2012	#num#	k4
•	•	k?
2013	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2015	#num#	k4
•	•	k?
2016	#num#	k4
•	•	k?
2017	#num#	k4
•	•	k?
2018	#num#	k4
•	•	k?
2019	#num#	k4
•	•	k?
2020	#num#	k4
•	•	k?
2021	#num#	k4
Vítězové	vítěz	k1gMnPc1
</s>
<s>
mužská	mužský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
•	•	k?
</s>
<s>
ženská	ženský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
•	•	k?
</s>
<s>
mužská	mužský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
•	•	k?
</s>
<s>
ženská	ženský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
•	•	k?
</s>
<s>
smíšená	smíšený	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
•	•	k?
</s>
<s>
junioři	junior	k1gMnPc1
</s>
<s>
Australian	Australian	k1gMnSc1
Open	Opena	k1gFnPc2
–	–	k?
Vítězové	vítěz	k1gMnPc1
mužské	mužský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
v	v	k7c6
otevřené	otevřený	k2eAgFnSc6d1
éře	éra	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
Laver	lavra	k1gFnPc2
•	•	k?
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
Arthur	Arthura	k1gFnPc2
Ashe	Ashe	k1gNnPc2
•	•	k?
(	(	kIx(
<g/>
1971	#num#	k4
–	–	k?
1972	#num#	k4
<g/>
)	)	kIx)
Ken	Ken	k1gMnSc1
Rosewall	Rosewall	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
John	John	k1gMnSc1
Newcombe	Newcomb	k1gInSc5
•	•	k?
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
Jimmy	Jimma	k1gFnSc2
Connors	Connors	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
John	John	k1gMnSc1
Newcombe	Newcomb	k1gInSc5
•	•	k?
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
Mark	Mark	k1gMnSc1
Edmondson	Edmondson	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1977	#num#	k4
leden	leden	k1gInSc1
<g/>
)	)	kIx)
Roscoe	Roscoe	k1gInSc1
Tanner	Tanner	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
1977	#num#	k4
prosinec	prosinec	k1gInSc1
<g/>
)	)	kIx)
Vitas	Vitas	k1gInSc1
Gerulaitis	Gerulaitis	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
Guillermo	Guillerma	k1gFnSc5
Vilas	Vilas	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
Brian	Brian	k1gMnSc1
Teacher	Teachra	k1gFnPc2
•	•	k?
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
–	–	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
Johan	Johan	k1gMnSc1
Kriek	Kriek	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
–	–	k?
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
Mats	Matsa	k1gFnPc2
Wilander	Wilandero	k1gNnPc2
•	•	k?
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
Stefan	Stefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
nekonalo	konat	k5eNaImAgNnS
se	se	k3xPyFc4
•	•	k?
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
Stefan	Stefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
Mats	Matsa	k1gFnPc2
Wilander	Wilandero	k1gNnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
Ivan	Ivan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
Boris	Boris	k1gMnSc1
Becker	Becker	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
Jim	on	k3xPp3gFnPc3
Courier	Courier	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
Pete	Pet	k1gInSc2
Sampras	Sampras	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
Andre	Andr	k1gMnSc5
Agassi	Agass	k1gMnSc5
•	•	k?
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
Boris	Boris	k1gMnSc1
Becker	Becker	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
Pete	Pet	k1gInSc2
Sampras	Sampras	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
Petr	Petr	k1gMnSc1
Korda	Korda	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
Jevgenij	Jevgenij	k1gFnSc1
Kafelnikov	Kafelnikov	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
Andre	Andr	k1gMnSc5
Agassi	Agass	k1gMnSc5
•	•	k?
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
Thomas	Thomas	k1gMnSc1
Johansson	Johansson	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
Andre	Andr	k1gMnSc5
Agassi	Agass	k1gMnSc5
•	•	k?
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
Marat	Marat	k2eAgInSc1d1
Safin	Safin	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
•	•	k?
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
Stan	stan	k1gInSc1
Wawrinka	Wawrinka	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
•	•	k?
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
–	–	k?
Vítězky	vítězka	k1gFnSc2
ženské	ženský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
v	v	k7c6
otevřené	otevřený	k2eAgFnSc6d1
éře	éra	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
Margaret	Margareta	k1gFnPc2
Courtová	Courtová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
Virginia	Virginium	k1gNnSc2
Wadeová	Wadeová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
Margaret	Margareta	k1gFnPc2
Courtová	Courtová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
Evonne	Evonn	k1gInSc5
Goolagongová	Goolagongový	k2eAgNnPc1d1
•	•	k?
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
I.	I.	kA
<g/>
)	)	kIx)
Kerry	Kerra	k1gMnSc2
Reidová	Reidový	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
XII	XII	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Evonne	Evonn	k1gMnSc5
Goolagongová	Goolagongový	k2eAgNnPc1d1
•	•	k?
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
Chris	Chris	k1gFnSc1
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Neilová	Neilová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
Barbara	Barbara	k1gFnSc1
Jordanová	Jordanová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
Hana	Hana	k1gFnSc1
Mandlíková	Mandlíková	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
Martina	Martina	k1gFnSc1
Navrátilová	Navrátilová	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
Chris	Chris	k1gFnSc1
Evertová	Evertová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
Martina	Martina	k1gFnSc1
Navrátilová	Navrátilová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
Chris	Chris	k1gFnSc1
Evertová	Evertová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
Martina	Martina	k1gFnSc1
Navrátilová	Navrátilová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
Hana	Hana	k1gFnSc1
Mandlíková	Mandlíková	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Steffi	Steffi	k1gFnSc1
Grafová	Grafová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
Monika	Monika	k1gFnSc1
Selešová	Selešová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
Steffi	Steffi	k1gFnSc1
Grafová	Grafová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
Mary	Mary	k1gFnSc1
Pierceová	Pierceová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
Monika	Monika	k1gFnSc1
Selešová	Selešová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
Martina	Martina	k1gFnSc1
Hingisová	Hingisový	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
Lindsay	Lindsaa	k1gFnSc2
Davenportová	Davenportová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
Jennifer	Jennifra	k1gFnPc2
Capriatiová	Capriatiový	k2eAgFnSc1d1
•	•	k?
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
Serena	Seren	k2eAgFnSc1d1
Williamsová	Williamsová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
Justine	Justin	k1gMnSc5
Heninová	Heninový	k2eAgNnPc1d1
•	•	k?
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
Serena	Seren	k2eAgFnSc1d1
Williamsová	Williamsová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Amélie	Amélie	k1gFnSc1
Mauresmová	Mauresmová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
Serena	Seren	k2eAgFnSc1d1
Williamsová	Williamsová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
Maria	Maria	k1gFnSc1
Šarapovová	Šarapovová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
Serena	Seren	k2eAgFnSc1d1
Williamsová	Williamsová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
Kim	Kim	k1gFnSc1
Clijstersová	Clijstersová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
Viktoria	Viktoria	k1gFnSc1
Azarenková	Azarenkový	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
Li	li	k8xS
Na	na	k7c4
•	•	k?
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
Serena	Seren	k2eAgFnSc1d1
Williamsová	Williamsová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
Angelique	Angeliqu	k1gInSc2
Kerberová	Kerberová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Serena	Seren	k2eAgFnSc1d1
Williamsová	Williamsová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
Caroline	Carolin	k1gInSc5
Wozniacká	Wozniacký	k2eAgNnPc1d1
•	•	k?
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
Naomi	Nao	k1gFnPc7
Ósakaová	Ósakaový	k2eAgNnPc1d1
•	•	k?
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
Sofia	Sofia	k1gFnSc1
Keninová	Keninová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
Naomi	Nao	k1gFnPc7
Ósakaová	Ósakaový	k2eAgNnPc5d1
</s>
<s>
Grand	grand	k1gMnSc1
Slam	slam	k1gInSc1
<g/>
:	:	kIx,
mužští	mužský	k2eAgMnPc1d1
vítězové	vítěz	k1gMnPc1
a	a	k8xC
ženské	ženská	k1gFnPc1
vítězky	vítězka	k1gFnSc2
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
</s>
<s>
mužská	mužský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
•	•	k?
ženská	ženský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
•	•	k?
mužská	mužský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
•	•	k?
ženská	ženský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
•	•	k?
smíšená	smíšený	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
French	French	k1gMnSc1
Open	Open	k1gMnSc1
</s>
<s>
mužská	mužský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
•	•	k?
ženská	ženský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
•	•	k?
mužská	mužský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
•	•	k?
ženská	ženský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
•	•	k?
smíšená	smíšený	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
Wimbledon	Wimbledon	k1gInSc1
</s>
<s>
mužská	mužský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
•	•	k?
ženská	ženský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
•	•	k?
mužská	mužský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
•	•	k?
ženská	ženský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
•	•	k?
smíšená	smíšený	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
US	US	kA
Open	Open	k1gNnSc1
</s>
<s>
mužská	mužský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
•	•	k?
ženská	ženský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
•	•	k?
mužská	mužský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
•	•	k?
ženská	ženský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
•	•	k?
smíšená	smíšený	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2015039971	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
316549312	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2015039971	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Tenis	tenis	k1gInSc1
</s>
