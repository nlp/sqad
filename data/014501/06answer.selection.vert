<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2020	#num#	k4
se	se	k3xPyFc4
dodavatelem	dodavatel	k1gMnSc7
povrchu	povrch	k1gInSc2
na	na	k7c6
australských	australský	k2eAgInPc6d1
turnajích	turnaj	k1gInPc6
včetně	včetně	k7c2
Australian	Australiany	k1gInPc2
Open	Open	k1gInSc4
Series	Series	k1gInSc4
stala	stát	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
GreenSet	GreenSeta	k1gFnPc2
Worldwide	Worldwid	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
a	a	k8xC
Serena	Serena	k1gFnSc1
Williamsová	Williamsová	k1gFnSc1
jsou	být	k5eAaImIp3nP
jedinými	jediný	k2eAgMnPc7d1
hráči	hráč	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
zvítězili	zvítězit	k5eAaPmAgMnP
na	na	k7c6
površích	povrch	k1gInPc6
Rebound	Rebound	k1gMnSc1
Ace	Ace	k1gMnSc1
i	i	k9
Plexicushion	Plexicushion	k1gInSc4
<g/>
.	.	kIx.
</s>