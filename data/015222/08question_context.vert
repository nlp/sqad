<s>
Založená	založený	k2eAgFnSc1d1
na	na	k7c6
dělbě	dělba	k1gFnSc6
práce	práce	k1gFnSc2
(	(	kIx(
<g/>
převládá	převládat	k5eAaImIp3nS
ve	v	k7c6
vyspělejších	vyspělý	k2eAgFnPc6d2
společnostech	společnost	k1gFnPc6
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
moderní	moderní	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
již	již	k6eAd1
dělba	dělba	k1gFnSc1
práce	práce	k1gFnSc2
rozvinutá	rozvinutý	k2eAgFnSc1d1
<g/>
,	,	kIx,
lidé	člověk	k1gMnPc1
zaujímají	zaujímat	k5eAaImIp3nP
nejen	nejen	k6eAd1
odlišné	odlišný	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
plní	plnit	k5eAaImIp3nP
odlišné	odlišný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
vzájemně	vzájemně	k6eAd1
se	se	k3xPyFc4
doplňující	doplňující	k2eAgFnSc1d1
<g/>
,	,	kIx,
existuje	existovat	k5eAaImIp3nS
solidarita	solidarita	k1gFnSc1
organická	organický	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
postavena	postaven	k2eAgFnSc1d1
na	na	k7c6
dělbě	dělba	k1gFnSc6
práce	práce	k1gFnSc2
a	a	k8xC
kooperaci	kooperace	k1gFnSc6
<g/>
.	.	kIx.
</s>