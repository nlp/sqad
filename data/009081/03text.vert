<p>
<s>
Veleobr	veleobr	k1gMnSc1	veleobr
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
hmotná	hmotný	k2eAgFnSc1d1	hmotná
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
zářivá	zářivý	k2eAgFnSc1d1	zářivá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
absolutní	absolutní	k2eAgFnSc1d1	absolutní
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
-3	-3	k4	-3
až	až	k9	až
-8	-8	k4	-8
<g/>
,	,	kIx,	,
povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
od	od	k7c2	od
3	[number]	k4	3
500	[number]	k4	500
do	do	k7c2	do
25	[number]	k4	25
000	[number]	k4	000
stupňů	stupeň	k1gInPc2	stupeň
Kelvina	Kelvin	k2eAgFnSc1d1	Kelvina
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc1	hmotnost
10	[number]	k4	10
až	až	k8xS	až
70	[number]	k4	70
hmotností	hmotnost	k1gFnSc7	hmotnost
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
velikostí	velikost	k1gFnPc2	velikost
se	se	k3xPyFc4	se
rovnají	rovnat	k5eAaImIp3nP	rovnat
1	[number]	k4	1
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
krát	krát	k6eAd1	krát
průměru	průměr	k1gInSc3	průměr
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
extrémní	extrémní	k2eAgFnSc3d1	extrémní
hmotnosti	hmotnost	k1gFnSc3	hmotnost
mají	mít	k5eAaImIp3nP	mít
velice	velice	k6eAd1	velice
krátký	krátký	k2eAgInSc4d1	krátký
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
pohybující	pohybující	k2eAgInPc4d1	pohybující
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
mezi	mezi	k7c7	mezi
10	[number]	k4	10
až	až	k8xS	až
50	[number]	k4	50
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hertzsprung-Russellově	Hertzsprung-Russellův	k2eAgInSc6d1	Hertzsprung-Russellův
diagramu	diagram	k1gInSc6	diagram
zabírají	zabírat	k5eAaImIp3nP	zabírat
celou	celý	k2eAgFnSc4d1	celá
horní	horní	k2eAgFnSc4d1	horní
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
mladých	mladý	k2eAgFnPc6d1	mladá
kosmických	kosmický	k2eAgFnPc6d1	kosmická
strukturách	struktura	k1gFnPc6	struktura
<g/>
,	,	kIx,	,
jakými	jaký	k3yQgFnPc7	jaký
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
otevřené	otevřený	k2eAgFnPc4d1	otevřená
hvězdokupy	hvězdokupa	k1gFnPc4	hvězdokupa
v	v	k7c6	v
ramenech	rameno	k1gNnPc6	rameno
spirálních	spirální	k2eAgFnPc2d1	spirální
a	a	k8xC	a
nepravidelných	pravidelný	k2eNgFnPc2d1	nepravidelná
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
eliptických	eliptický	k2eAgFnPc6d1	eliptická
galaxiích	galaxie	k1gFnPc6	galaxie
a	a	k8xC	a
kulových	kulový	k2eAgFnPc6d1	kulová
hvězdokupách	hvězdokupa	k1gFnPc6	hvězdokupa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
složené	složený	k2eAgFnPc1d1	složená
převážně	převážně	k6eAd1	převážně
ze	z	k7c2	z
starších	starý	k2eAgFnPc2d2	starší
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
veleobry	veleobr	k1gMnPc4	veleobr
patří	patřit	k5eAaImIp3nP	patřit
hvězdy	hvězda	k1gFnPc1	hvězda
Polárka	Polárka	k1gFnSc1	Polárka
<g/>
,	,	kIx,	,
Betelgeuze	Betelgeuze	k1gFnSc1	Betelgeuze
<g/>
,	,	kIx,	,
VV	VV	kA	VV
Cephei	Cephei	k1gNnSc2	Cephei
<g/>
,	,	kIx,	,
Deneb	Denba	k1gFnPc2	Denba
<g/>
,	,	kIx,	,
Rigel	Rigela	k1gFnPc2	Rigela
nebo	nebo	k8xC	nebo
V354	V354	k1gFnPc2	V354
Cephei	Cephe	k1gFnSc2	Cephe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
třídy	třída	k1gFnSc2	třída
O	O	kA	O
a	a	k8xC	a
nejhmotnější	hmotný	k2eAgFnSc2d3	nejhmotnější
modrobílé	modrobílý	k2eAgFnSc2d1	modrobílá
hvězdy	hvězda	k1gFnSc2	hvězda
třídy	třída	k1gFnSc2	třída
B	B	kA	B
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
veleobry	veleobr	k1gMnPc7	veleobr
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vysoké	vysoký	k2eAgFnSc3d1	vysoká
hmotnosti	hmotnost	k1gFnSc3	hmotnost
mají	mít	k5eAaImIp3nP	mít
extrémně	extrémně	k6eAd1	extrémně
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
tisíc	tisíc	k4xCgInSc4	tisíc
až	až	k8xS	až
maximálně	maximálně	k6eAd1	maximálně
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
ke	k	k7c3	k
spatření	spatření	k1gNnSc3	spatření
především	především	k6eAd1	především
v	v	k7c6	v
mladých	mladý	k2eAgFnPc6d1	mladá
galaktických	galaktický	k2eAgFnPc6d1	Galaktická
strukturách	struktura	k1gFnPc6	struktura
jako	jako	k9	jako
otevřené	otevřený	k2eAgFnPc1d1	otevřená
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
<g/>
,	,	kIx,	,
ramena	rameno	k1gNnPc1	rameno
spirálních	spirální	k2eAgFnPc2d1	spirální
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
a	a	k8xC	a
nepravidelné	pravidelný	k2eNgFnPc1d1	nepravidelná
galaxie	galaxie	k1gFnPc1	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
často	často	k6eAd1	často
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
spirálních	spirální	k2eAgFnPc6d1	spirální
galaxiích	galaxie	k1gFnPc6	galaxie
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
jsou	být	k5eAaImIp3nP	být
pozorované	pozorovaný	k2eAgMnPc4d1	pozorovaný
v	v	k7c6	v
eliptických	eliptický	k2eAgFnPc6d1	eliptická
galaxiích	galaxie	k1gFnPc6	galaxie
nebo	nebo	k8xC	nebo
kulových	kulový	k2eAgFnPc6d1	kulová
hvězdokupách	hvězdokupa	k1gFnPc6	hvězdokupa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
převážně	převážně	k6eAd1	převážně
ze	z	k7c2	z
starých	starý	k2eAgFnPc2d1	stará
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Veleobr	veleobr	k1gMnSc1	veleobr
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
<g/>
,	,	kIx,	,
když	když	k8xS	když
masivní	masivní	k2eAgFnSc1d1	masivní
hvězda	hvězda	k1gFnSc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
všechen	všechen	k3xTgInSc1	všechen
vodík	vodík	k1gInSc1	vodík
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
začne	začít	k5eAaPmIp3nS	začít
expandovat	expandovat	k5eAaImF	expandovat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
hvězdy	hvězda	k1gFnPc1	hvězda
nižších	nízký	k2eAgFnPc2d2	nižší
hmotností	hmotnost	k1gFnPc2	hmotnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
nich	on	k3xPp3gFnPc2	on
má	mít	k5eAaImIp3nS	mít
veleobr	veleobr	k1gMnSc1	veleobr
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
teplota	teplota	k1gFnSc1	teplota
potřebná	potřebný	k2eAgFnSc1d1	potřebná
pro	pro	k7c4	pro
nastartování	nastartování	k1gNnSc4	nastartování
fúze	fúze	k1gFnSc2	fúze
hélia	hélium	k1gNnSc2	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
méně	málo	k6eAd2	málo
hmotných	hmotný	k2eAgFnPc2d1	hmotná
hvězd	hvězda	k1gFnPc2	hvězda
červení	červený	k2eAgMnPc1d1	červený
veleobři	veleobr	k1gMnPc1	veleobr
neodhodí	odhodit	k5eNaPmIp3nP	odhodit
svou	svůj	k3xOyFgFnSc4	svůj
atmosféru	atmosféra	k1gFnSc4	atmosféra
jako	jako	k8xS	jako
planetární	planetární	k2eAgFnSc4d1	planetární
mlhovinu	mlhovina	k1gFnSc4	mlhovina
<g/>
,	,	kIx,	,
když	když	k8xS	když
spotřebují	spotřebovat	k5eAaPmIp3nP	spotřebovat
vodík	vodík	k1gInSc4	vodík
a	a	k8xC	a
hélium	hélium	k1gNnSc4	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
i	i	k9	i
nadále	nadále	k6eAd1	nadále
spalují	spalovat	k5eAaImIp3nP	spalovat
těžší	těžký	k2eAgInPc4d2	těžší
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
nezhroutí	zhroutit	k5eNaPmIp3nS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
nepřijdou	přijít	k5eNaPmIp3nP	přijít
o	o	k7c4	o
dostatek	dostatek	k1gInSc4	dostatek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
stal	stát	k5eAaPmAgMnS	stát
bílý	bílý	k1gMnSc1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
na	na	k7c4	na
neutronovou	neutronový	k2eAgFnSc4d1	neutronová
hvězdu	hvězda	k1gFnSc4	hvězda
nebo	nebo	k8xC	nebo
černou	černý	k2eAgFnSc4d1	černá
díru	díra	k1gFnSc4	díra
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
vybuchnou	vybuchnout	k5eAaPmIp3nP	vybuchnout
jako	jako	k8xS	jako
supernova	supernova	k1gFnSc1	supernova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
40	[number]	k4	40
hmotností	hmotnost	k1gFnSc7	hmotnost
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
nemohou	moct	k5eNaImIp3nP	moct
stát	stát	k5eAaImF	stát
červenými	červený	k2eAgMnPc7d1	červený
veleobry	veleobr	k1gMnPc7	veleobr
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
spalují	spalovat	k5eAaImIp3nP	spalovat
vodík	vodík	k1gInSc4	vodík
příliš	příliš	k6eAd1	příliš
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
příliš	příliš	k6eAd1	příliš
rychle	rychle	k6eAd1	rychle
také	také	k6eAd1	také
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
své	svůj	k3xOyFgFnPc4	svůj
vnější	vnější	k2eAgFnPc4d1	vnější
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Stanou	stanout	k5eAaPmIp3nP	stanout
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
tedy	tedy	k9	tedy
modří	modrý	k2eAgMnPc1d1	modrý
nebo	nebo	k8xC	nebo
žlutí	žlutý	k2eAgMnPc1d1	žlutý
hyperobři	hyperobr	k1gMnPc1	hyperobr
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
velmi	velmi	k6eAd1	velmi
žhavé	žhavý	k2eAgFnPc1d1	žhavá
a	a	k8xC	a
svítivé	svítivý	k2eAgFnPc1d1	svítivá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
stokrát	stokrát	k6eAd1	stokrát
větší	veliký	k2eAgFnSc7d2	veliký
než	než	k8xS	než
hmotnost	hmotnost	k1gFnSc4	hmotnost
Slunce	slunce	k1gNnSc2	slunce
spalují	spalovat	k5eAaImIp3nP	spalovat
tak	tak	k9	tak
účinně	účinně	k6eAd1	účinně
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokáží	dokázat	k5eAaPmIp3nP	dokázat
přivést	přivést	k5eAaPmF	přivést
vodík	vodík	k1gInSc4	vodík
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
do	do	k7c2	do
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
zde	zde	k6eAd1	zde
ho	on	k3xPp3gMnSc4	on
spotřebovat	spotřebovat	k5eAaPmF	spotřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
kompletním	kompletní	k2eAgNnSc6d1	kompletní
spotřebování	spotřebování	k1gNnSc6	spotřebování
vodíku	vodík	k1gInSc2	vodík
projde	projít	k5eAaPmIp3nS	projít
tato	tento	k3xDgFnSc1	tento
hvězda	hvězda	k1gFnSc1	hvězda
zrychleným	zrychlený	k2eAgInSc7d1	zrychlený
vývojem	vývoj	k1gInSc7	vývoj
a	a	k8xC	a
v	v	k7c6	v
sérii	série	k1gFnSc6	série
rychlých	rychlý	k2eAgFnPc2d1	rychlá
změn	změna	k1gFnPc2	změna
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
stane	stanout	k5eAaPmIp3nS	stanout
různými	různý	k2eAgFnPc7d1	různá
hvězdami	hvězda	k1gFnPc7	hvězda
typu	typ	k1gInSc2	typ
WNh	WNh	k1gFnPc2	WNh
<g/>
,	,	kIx,	,
WN	WN	kA	WN
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
WC	WC	kA	WC
nebo	nebo	k8xC	nebo
WO	WO	kA	WO
<g/>
.	.	kIx.	.
</s>
<s>
Očekává	očekávat	k5eAaImIp3nS	očekávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
stane	stanout	k5eAaPmIp3nS	stanout
supernovou	supernova	k1gFnSc7	supernova
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dosud	dosud	k6eAd1	dosud
není	být	k5eNaImIp3nS	být
jasný	jasný	k2eAgInSc1d1	jasný
princip	princip	k1gInSc1	princip
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
děje	dít	k5eAaImIp3nS	dít
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
těchto	tento	k3xDgMnPc2	tento
veleobrů	veleobr	k1gMnPc2	veleobr
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
spalují	spalovat	k5eAaImIp3nP	spalovat
vodík	vodík	k1gInSc4	vodík
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
už	už	k6eAd1	už
tvoří	tvořit	k5eAaImIp3nP	tvořit
těžší	těžký	k2eAgInPc4d2	těžší
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
vyžadovat	vyžadovat	k5eAaImF	vyžadovat
poněkud	poněkud	k6eAd1	poněkud
komplexnější	komplexní	k2eAgFnSc4d2	komplexnější
definici	definice	k1gFnSc4	definice
veleobrů	veleobr	k1gMnPc2	veleobr
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgFnPc1	první
hvězdy	hvězda	k1gFnPc1	hvězda
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
byly	být	k5eAaImAgInP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
značně	značně	k6eAd1	značně
jasnější	jasný	k2eAgFnSc1d2	jasnější
a	a	k8xC	a
masivnější	masivní	k2eAgFnSc1d2	masivnější
než	než	k8xS	než
hvězdy	hvězda	k1gFnPc1	hvězda
v	v	k7c6	v
současném	současný	k2eAgInSc6d1	současný
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
existence	existence	k1gFnSc1	existence
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
pro	pro	k7c4	pro
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
některých	některý	k3yIgInPc2	některý
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
hélia	hélium	k1gNnSc2	hélium
v	v	k7c6	v
kvasarech	kvasar	k1gInPc6	kvasar
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
větší	veliký	k2eAgFnSc4d2	veliký
a	a	k8xC	a
zářivější	zářivý	k2eAgFnSc4d2	zářivější
než	než	k8xS	než
jakýkoli	jakýkoli	k3yIgMnSc1	jakýkoli
dnes	dnes	k6eAd1	dnes
známý	známý	k2eAgMnSc1d1	známý
veleobr	veleobr	k1gMnSc1	veleobr
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
struktura	struktura	k1gFnSc1	struktura
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sníženou	snížený	k2eAgFnSc7d1	snížená
konvekcí	konvekce	k1gFnSc7	konvekce
a	a	k8xC	a
menšími	malý	k2eAgFnPc7d2	menší
ztrátami	ztráta	k1gFnPc7	ztráta
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc4	jejich
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgInPc4d1	krátký
životy	život	k1gInPc4	život
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
skončily	skončit	k5eAaPmAgFnP	skončit
násilnou	násilný	k2eAgFnSc7d1	násilná
fototransmutací	fototransmutace	k1gFnSc7	fototransmutace
nebo	nebo	k8xC	nebo
dvojicí	dvojice	k1gFnSc7	dvojice
nestabilních	stabilní	k2eNgFnPc2d1	nestabilní
supernov	supernova	k1gFnPc2	supernova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Definice	definice	k1gFnSc2	definice
==	==	k?	==
</s>
</p>
<p>
<s>
Termín	termín	k1gInSc1	termín
veleobr	veleobr	k1gMnSc1	veleobr
<g/>
,	,	kIx,	,
aplikovaný	aplikovaný	k2eAgMnSc1d1	aplikovaný
na	na	k7c4	na
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
definici	definice	k1gFnSc4	definice
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
obří	obří	k2eAgFnSc1d1	obří
hvězda	hvězda	k1gFnSc1	hvězda
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
Ejnar	Ejnar	k1gMnSc1	Ejnar
Hertzsprung	Hertzsprung	k1gMnSc1	Hertzsprung
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
hvězd	hvězda	k1gFnPc2	hvězda
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
odlišných	odlišný	k2eAgFnPc2d1	odlišná
oblastí	oblast	k1gFnPc2	oblast
Hertzsprung-Russellova	Hertzsprung-Russellův	k2eAgInSc2d1	Hertzsprung-Russellův
diagramu	diagram	k1gInSc2	diagram
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
oblast	oblast	k1gFnSc1	oblast
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
větší	veliký	k2eAgFnSc1d2	veliký
a	a	k8xC	a
více	hodně	k6eAd2	hodně
svítící	svítící	k2eAgFnPc4d1	svítící
hvězdy	hvězda	k1gFnPc4	hvězda
spektrální	spektrální	k2eAgFnSc1d1	spektrální
typů	typ	k1gInPc2	typ
A	a	k8xC	a
až	až	k9	až
M	M	kA	M
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dostaly	dostat	k5eAaPmAgInP	dostat
název	název	k1gInSc1	název
obři	obr	k1gMnPc1	obr
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dříve	dříve	k6eAd2	dříve
neexistovala	existovat	k5eNaImAgNnP	existovat
měřitelná	měřitelný	k2eAgNnPc1d1	měřitelné
srovnání	srovnání	k1gNnPc1	srovnání
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
hvězd	hvězda	k1gFnPc2	hvězda
jsou	být	k5eAaImIp3nP	být
podstatně	podstatně	k6eAd1	podstatně
větší	veliký	k2eAgFnPc1d2	veliký
a	a	k8xC	a
zářivější	zářivý	k2eAgFnPc1d2	zářivější
než	než	k8xS	než
ostatní	ostatní	k2eAgFnPc1d1	ostatní
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
přijat	přijat	k2eAgInSc1d1	přijat
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
velmi	velmi	k6eAd1	velmi
obří	obří	k2eAgInSc1d1	obří
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
veleobři	veleobr	k1gMnPc1	veleobr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spektrální	spektrální	k2eAgFnSc1d1	spektrální
třída	třída	k1gFnSc1	třída
svítivosti	svítivost	k1gFnSc2	svítivost
===	===	k?	===
</s>
</p>
<p>
<s>
Veleobři	veleobr	k1gMnPc1	veleobr
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
identifikováni	identifikován	k2eAgMnPc1d1	identifikován
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jejich	jejich	k3xOp3gFnPc2	jejich
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
čar	čára	k1gFnPc2	čára
<g/>
,	,	kIx,	,
s	s	k7c7	s
výraznými	výrazný	k2eAgFnPc7d1	výrazná
liniemi	linie	k1gFnPc7	linie
citlivými	citlivý	k2eAgFnPc7d1	citlivá
na	na	k7c4	na
vysokou	vysoká	k1gFnSc4	vysoká
svítivost	svítivost	k1gFnSc4	svítivost
a	a	k8xC	a
nízkou	nízký	k2eAgFnSc4d1	nízká
gravitaci	gravitace	k1gFnSc4	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
americká	americký	k2eAgFnSc1d1	americká
astronomka	astronomka	k1gFnSc1	astronomka
Antonia	Antonio	k1gMnSc2	Antonio
Mauryová	Mauryová	k1gFnSc1	Mauryová
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
hvězdy	hvězda	k1gFnPc4	hvězda
na	na	k7c6	na
základě	základ	k1gInSc6	základ
šířek	šířka	k1gFnPc2	šířka
jejich	jejich	k3xOp3gFnPc2	jejich
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
čar	čára	k1gFnPc2	čára
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
třída	třída	k1gFnSc1	třída
C	C	kA	C
připadla	připadnout	k5eAaPmAgFnS	připadnout
hvězdám	hvězda	k1gFnPc3	hvězda
s	s	k7c7	s
nejužšími	úzký	k2eAgFnPc7d3	nejužší
linkami	linka	k1gFnPc7	linka
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
to	ten	k3xDgNnSc1	ten
nebylo	být	k5eNaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
to	ten	k3xDgNnSc1	ten
nejzářivější	zářivý	k2eAgFnPc1d3	nejzářivější
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
astrofyzici	astrofyzik	k1gMnPc1	astrofyzik
Morgan	morgan	k1gMnSc1	morgan
a	a	k8xC	a
Keenan	Keenan	k1gMnSc1	Keenan
stanovili	stanovit	k5eAaPmAgMnP	stanovit
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
spektrální	spektrální	k2eAgFnSc4d1	spektrální
klasifikaci	klasifikace	k1gFnSc4	klasifikace
tříd	třída	k1gFnPc2	třída
zářivosti	zářivost	k1gFnSc2	zářivost
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
třída	třída	k1gFnSc1	třída
I	i	k9	i
odkazovala	odkazovat	k5eAaImAgFnS	odkazovat
na	na	k7c4	na
veleobry	veleobr	k1gMnPc4	veleobr
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
klasifikace	klasifikace	k1gFnSc1	klasifikace
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
úpravy	úprava	k1gFnPc1	úprava
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vyššího	vysoký	k2eAgNnSc2d2	vyšší
rozlišení	rozlišení	k1gNnSc2	rozlišení
moderních	moderní	k2eAgInPc2d1	moderní
spektrometrů	spektrometr	k1gInPc2	spektrometr
<g/>
.	.	kIx.	.
</s>
<s>
Veleobři	veleobr	k1gMnPc1	veleobr
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
spektrální	spektrální	k2eAgFnSc6d1	spektrální
třídě	třída	k1gFnSc6	třída
<g/>
,	,	kIx,	,
od	od	k7c2	od
mladé	mladý	k2eAgFnSc2d1	mladá
modré	modrý	k2eAgFnSc2d1	modrá
hvězdy	hvězda	k1gFnSc2	hvězda
třídy	třída	k1gFnSc2	třída
O	O	kA	O
až	až	k9	až
po	po	k7c4	po
velmi	velmi	k6eAd1	velmi
vyvinuté	vyvinutý	k2eAgFnPc4d1	vyvinutá
červené	červený	k2eAgFnPc4d1	červená
hvězdy	hvězda	k1gFnPc4	hvězda
třídy	třída	k1gFnSc2	třída
M.	M.	kA	M.
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgMnPc1d2	veliký
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
hvězdami	hvězda	k1gFnPc7	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
stejného	stejný	k2eAgInSc2d1	stejný
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
nižší	nízký	k2eAgFnSc4d2	nižší
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
gravitaci	gravitace	k1gFnSc4	gravitace
a	a	k8xC	a
tyto	tento	k3xDgFnPc4	tento
změny	změna	k1gFnPc4	změna
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pozorovány	pozorován	k2eAgInPc1d1	pozorován
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
profilových	profilový	k2eAgFnPc6d1	profilová
liniích	linie	k1gFnPc6	linie
<g/>
.	.	kIx.	.
</s>
<s>
Veleobři	veleobr	k1gMnPc1	veleobr
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
vyvinuté	vyvinutý	k2eAgFnPc4d1	vyvinutá
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vysoké	vysoký	k2eAgNnSc4d1	vysoké
procento	procento	k1gNnSc4	procento
těžších	těžký	k2eAgInPc2d2	těžší
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
mají	mít	k5eAaImIp3nP	mít
hvězdy	hvězda	k1gFnPc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
změn	změna	k1gFnPc2	změna
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
čar	čára	k1gFnPc2	čára
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nízké	nízký	k2eAgFnSc2d1	nízká
povrchové	povrchový	k2eAgFnSc2d1	povrchová
gravitace	gravitace	k1gFnSc2	gravitace
lze	lze	k6eAd1	lze
veleobry	veleobr	k1gMnPc4	veleobr
identifikovat	identifikovat	k5eAaBmF	identifikovat
také	také	k9	také
podle	podle	k7c2	podle
velmi	velmi	k6eAd1	velmi
vysokých	vysoký	k2eAgFnPc2d1	vysoká
ztrát	ztráta	k1gFnPc2	ztráta
hmotnosti	hmotnost	k1gFnSc2	hmotnost
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plynoucích	plynoucí	k2eAgInPc2d1	plynoucí
oblaků	oblak	k1gInPc2	oblak
materiálu	materiál	k1gInSc2	materiál
obklopující	obklopující	k2eAgFnSc4d1	obklopující
danou	daný	k2eAgFnSc4d1	daná
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
produkují	produkovat	k5eAaImIp3nP	produkovat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
odlišné	odlišný	k2eAgFnPc4d1	odlišná
spektrální	spektrální	k2eAgFnPc4d1	spektrální
čáry	čára	k1gFnPc4	čára
<g/>
.	.	kIx.	.
</s>
<s>
Morgan-Keenanova	Morgan-Keenanův	k2eAgFnSc1d1	Morgan-Keenanův
klasifikace	klasifikace	k1gFnSc1	klasifikace
přiřazuje	přiřazovat	k5eAaImIp3nS	přiřazovat
těmto	tento	k3xDgFnPc3	tento
hvězdám	hvězda	k1gFnPc3	hvězda
tyto	tento	k3xDgFnPc1	tento
třídy	třída	k1gFnPc1	třída
<g/>
:	:	kIx,	:
Ib	Ib	k1gFnPc1	Ib
pro	pro	k7c4	pro
veleobry	veleobr	k1gMnPc4	veleobr
<g/>
,	,	kIx,	,
Ia	ia	k0	ia
pro	pro	k7c4	pro
zářivé	zářivý	k2eAgMnPc4d1	zářivý
veleobry	veleobr	k1gMnPc4	veleobr
<g/>
,	,	kIx,	,
0	[number]	k4	0
nebo	nebo	k8xC	nebo
Ia	ia	k0	ia
<g/>
+	+	kIx~	+
pro	pro	k7c4	pro
hyperobry	hyperobr	k1gMnPc4	hyperobr
a	a	k8xC	a
Iab	Iab	k1gMnPc4	Iab
je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
pro	pro	k7c4	pro
středně	středně	k6eAd1	středně
pokročilé	pokročilý	k2eAgMnPc4d1	pokročilý
zářivé	zářivý	k2eAgMnPc4d1	zářivý
veleobry	veleobr	k1gMnPc4	veleobr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
obtížně	obtížně	k6eAd1	obtížně
definovaná	definovaný	k2eAgNnPc4d1	definované
pásma	pásmo	k1gNnPc4	pásmo
spíš	spíš	k9	spíš
než	než	k8xS	než
pevně	pevně	k6eAd1	pevně
dané	daný	k2eAgFnPc4d1	daná
třídy	třída	k1gFnPc4	třída
s	s	k7c7	s
přesně	přesně	k6eAd1	přesně
vymezenými	vymezený	k2eAgFnPc7d1	vymezená
hodnotami	hodnota	k1gFnPc7	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Spektra	spektrum	k1gNnSc2	spektrum
veleobrů	veleobr	k1gMnPc2	veleobr
tak	tak	k9	tak
často	často	k6eAd1	často
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vysvětlivky	vysvětlivka	k1gFnPc1	vysvětlivka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dále	daleko	k6eAd2	daleko
určují	určovat	k5eAaImIp3nP	určovat
spektrální	spektrální	k2eAgFnPc4d1	spektrální
zvláštnosti	zvláštnost	k1gFnPc4	zvláštnost
dané	daný	k2eAgFnSc2d1	daná
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Rigel	Rigel	k1gMnSc1	Rigel
má	mít	k5eAaImIp3nS	mít
označení	označení	k1gNnPc4	označení
B2	B2	k1gMnSc2	B2
Iae	Iae	k1gMnSc2	Iae
nebo	nebo	k8xC	nebo
W	W	kA	W
Mensae	Mensae	k1gFnSc1	Mensae
F5	F5	k1gFnSc1	F5
Ipec	Ipec	k1gFnSc1	Ipec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vyvinutí	vyvinutý	k2eAgMnPc1d1	vyvinutý
veleobři	veleobr	k1gMnPc1	veleobr
===	===	k?	===
</s>
</p>
<p>
<s>
Veleobry	veleobr	k1gMnPc4	veleobr
lze	lze	k6eAd1	lze
také	také	k9	také
definovat	definovat	k5eAaBmF	definovat
jako	jako	k9	jako
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
fáze	fáze	k1gFnSc1	fáze
v	v	k7c6	v
evoluční	evoluční	k2eAgFnSc6d1	evoluční
historii	historie	k1gFnSc6	historie
některých	některý	k3yIgFnPc2	některý
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
počáteční	počáteční	k2eAgFnSc7d1	počáteční
hmotností	hmotnost	k1gFnSc7	hmotnost
nad	nad	k7c7	nad
10	[number]	k4	10
hmotností	hmotnost	k1gFnSc7	hmotnost
Slunce	slunce	k1gNnSc2	slunce
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
hladce	hladko	k6eAd1	hladko
nastartují	nastartovat	k5eAaPmIp3nP	nastartovat
fúzi	fúze	k1gFnSc4	fúze
jader	jádro	k1gNnPc2	jádro
hélia	hélium	k1gNnSc2	hélium
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
vyčerpaly	vyčerpat	k5eAaPmAgInP	vyčerpat
vodík	vodík	k1gInSc4	vodík
a	a	k8xC	a
následně	následně	k6eAd1	následně
po	po	k7c6	po
spotřebování	spotřebování	k1gNnSc6	spotřebování
helia	helium	k1gNnSc2	helium
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
fúzi	fúze	k1gFnSc4	fúze
dalších	další	k2eAgInPc2d1	další
těžkých	těžký	k2eAgInPc2d1	těžký
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
hvězda	hvězda	k1gFnSc1	hvězda
začne	začít	k5eAaPmIp3nS	začít
produkovat	produkovat	k5eAaImF	produkovat
železo	železo	k1gNnSc4	železo
<g/>
,	,	kIx,	,
následkem	následkem	k7c2	následkem
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
jádro	jádro	k1gNnSc1	jádro
zhroutí	zhroutit	k5eAaPmIp3nS	zhroutit
a	a	k8xC	a
hvězda	hvězda	k1gFnSc1	hvězda
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
supernovou	supernova	k1gFnSc7	supernova
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
tato	tento	k3xDgFnSc1	tento
hmotná	hmotný	k2eAgFnSc1d1	hmotná
hvězda	hvězda	k1gFnSc1	hvězda
opustí	opustit	k5eAaPmIp3nS	opustit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
posloupnost	posloupnost	k1gFnSc4	posloupnost
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
atmosféra	atmosféra	k1gFnSc1	atmosféra
se	se	k3xPyFc4	se
nafoukne	nafouknout	k5eAaPmIp3nS	nafouknout
a	a	k8xC	a
změní	změnit	k5eAaPmIp3nS	změnit
se	se	k3xPyFc4	se
na	na	k7c4	na
veleobra	veleobr	k1gMnSc4	veleobr
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
počáteční	počáteční	k2eAgFnSc7d1	počáteční
hmotností	hmotnost	k1gFnSc7	hmotnost
menší	malý	k2eAgFnSc7d2	menší
než	než	k8xS	než
10	[number]	k4	10
hmotností	hmotnost	k1gFnSc7	hmotnost
Slunce	slunce	k1gNnSc2	slunce
nikdy	nikdy	k6eAd1	nikdy
nevytvoří	vytvořit	k5eNaPmIp3nS	vytvořit
železné	železný	k2eAgNnSc1d1	železné
jádro	jádro	k1gNnSc1	jádro
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nikdy	nikdy	k6eAd1	nikdy
nestanou	stanout	k5eNaPmIp3nP	stanout
veleobři	veleobr	k1gMnPc1	veleobr
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
zářivého	zářivý	k2eAgInSc2d1	zářivý
výkonu	výkon	k1gInSc2	výkon
tisíckrát	tisíckrát	k6eAd1	tisíckrát
většího	veliký	k2eAgInSc2d2	veliký
než	než	k8xS	než
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Nemají	mít	k5eNaImIp3nP	mít
totiž	totiž	k9	totiž
dostatečně	dostatečně	k6eAd1	dostatečně
velkou	velký	k2eAgFnSc4d1	velká
gravitaci	gravitace	k1gFnSc4	gravitace
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
jádru	jádro	k1gNnSc6	jádro
zažehli	zažehnout	k5eAaPmAgMnP	zažehnout
fúzi	fúze	k1gFnSc4	fúze
uhlíku	uhlík	k1gInSc2	uhlík
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
těžších	těžký	k2eAgInPc2d2	těžší
prvků	prvek	k1gInPc2	prvek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
krajních	krajní	k2eAgFnPc2d1	krajní
vrstev	vrstva	k1gFnPc2	vrstva
stanou	stanout	k5eAaPmIp3nP	stanout
bílým	bílé	k1gNnSc7	bílé
trpaslíkem	trpaslík	k1gMnSc7	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Fáze	fáze	k1gFnSc1	fáze
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tato	tento	k3xDgFnSc1	tento
hvězda	hvězda	k1gFnSc1	hvězda
má	mít	k5eAaImIp3nS	mít
vodík	vodík	k1gInSc4	vodík
i	i	k8xC	i
hélium	hélium	k1gNnSc4	hélium
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
je	být	k5eAaImIp3nS	být
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xC	jako
asymptotická	asymptotický	k2eAgFnSc1d1	asymptotická
větev	větev	k1gFnSc1	větev
obrů	obr	k1gMnPc2	obr
(	(	kIx(	(
<g/>
AGB	AGB	kA	AGB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
stává	stávat	k5eAaImIp3nS	stávat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
a	a	k8xC	a
více	hodně	k6eAd2	hodně
zářivější	zářivý	k2eAgFnSc1d2	zářivější
hvězda	hvězda	k1gFnSc1	hvězda
třídy	třída	k1gFnSc2	třída
M.	M.	kA	M.
Hvězdy	Hvězda	k1gMnSc2	Hvězda
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
přesně	přesně	k6eAd1	přesně
8	[number]	k4	8
až	až	k9	až
10	[number]	k4	10
hmotností	hmotnost	k1gFnPc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
mohou	moct	k5eAaImIp3nP	moct
roztavit	roztavit	k5eAaPmF	roztavit
dostatek	dostatek	k1gInSc4	dostatek
uhlíku	uhlík	k1gInSc2	uhlík
na	na	k7c6	na
AGB	AGB	kA	AGB
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
začnou	začít	k5eAaPmIp3nP	začít
vytvářet	vytvářet	k5eAaImF	vytvářet
kyslíkovo-neonové	kyslíkovoeonový	k2eAgNnSc4d1	kyslíkovo-neonový
jádro	jádro	k1gNnSc4	jádro
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
stát	stát	k5eAaPmF	stát
supernovou	supernový	k2eAgFnSc7d1	supernová
díky	díky	k7c3	díky
vzniku	vznik	k1gInSc3	vznik
degenerovaného	degenerovaný	k2eAgInSc2d1	degenerovaný
elektronového	elektronový	k2eAgInSc2d1	elektronový
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
astrofyzici	astrofyzik	k1gMnPc1	astrofyzik
tyto	tento	k3xDgFnPc4	tento
hvězdy	hvězda	k1gFnPc4	hvězda
ovšem	ovšem	k9	ovšem
označují	označovat	k5eAaImIp3nP	označovat
spíše	spíše	k9	spíše
jako	jako	k9	jako
Asymptotické	asymptotický	k2eAgMnPc4d1	asymptotický
veleobry	veleobr	k1gMnPc4	veleobr
než	než	k8xS	než
veleobry	veleobr	k1gMnPc4	veleobr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kategorizace	kategorizace	k1gFnSc1	kategorizace
vyvinutých	vyvinutý	k2eAgMnPc2d1	vyvinutý
veleobrů	veleobr	k1gMnPc2	veleobr
===	===	k?	===
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
kategorií	kategorie	k1gFnPc2	kategorie
vyvinutých	vyvinutý	k2eAgFnPc2d1	vyvinutá
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
veleobry	veleobr	k1gMnPc4	veleobr
z	z	k7c2	z
evolučního	evoluční	k2eAgNnSc2d1	evoluční
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
vykazovat	vykazovat	k5eAaImF	vykazovat
spektrální	spektrální	k2eAgFnPc4d1	spektrální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
veleobrů	veleobr	k1gMnPc2	veleobr
nebo	nebo	k8xC	nebo
mají	mít	k5eAaImIp3nP	mít
srovnatelný	srovnatelný	k2eAgInSc4d1	srovnatelný
zářivý	zářivý	k2eAgInSc4d1	zářivý
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
hvězdy	hvězda	k1gFnPc1	hvězda
z	z	k7c2	z
asymptotické	asymptotický	k2eAgFnSc2d1	asymptotická
větve	větev	k1gFnSc2	větev
obrů	obr	k1gMnPc2	obr
(	(	kIx(	(
<g/>
AGB	AGB	kA	AGB
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
vyvinutí	vyvinutý	k2eAgMnPc1d1	vyvinutý
červení	červený	k2eAgMnPc1d1	červený
obři	obr	k1gMnPc1	obr
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
zářivý	zářivý	k2eAgInSc4d1	zářivý
výkon	výkon	k1gInSc4	výkon
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
s	s	k7c7	s
více	hodně	k6eAd2	hodně
hmotnými	hmotný	k2eAgMnPc7d1	hmotný
červenými	červený	k2eAgMnPc7d1	červený
veleobry	veleobr	k1gMnPc7	veleobr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
nízké	nízký	k2eAgFnSc3d1	nízká
hmotnost	hmotnost	k1gFnSc4	hmotnost
končí	končit	k5eAaImIp3nS	končit
jejich	jejich	k3xOp3gInPc4	jejich
životy	život	k1gInPc4	život
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
(	(	kIx(	(
<g/>
planetární	planetární	k2eAgFnSc1d1	planetární
mlhovina	mlhovina	k1gFnSc1	mlhovina
a	a	k8xC	a
bílý	bílý	k1gMnSc1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
spíše	spíše	k9	spíše
než	než	k8xS	než
supernova	supernova	k1gFnSc1	supernova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
astrofyzici	astrofyzik	k1gMnPc1	astrofyzik
je	on	k3xPp3gMnPc4	on
řadí	řadit	k5eAaImIp3nP	řadit
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
mnoho	mnoho	k4c4	mnoho
hvězd	hvězda	k1gFnPc2	hvězda
zbylých	zbylý	k2eAgFnPc2d1	zbylá
po	po	k7c6	po
asymptotických	asymptotický	k2eAgMnPc6d1	asymptotický
veleobrech	veleobr	k1gMnPc6	veleobr
dostane	dostat	k5eAaPmIp3nS	dostat
spektrální	spektrální	k2eAgInSc4d1	spektrální
typ	typ	k1gInSc4	typ
náležící	náležící	k2eAgInSc4d1	náležící
svítivým	svítivý	k2eAgMnSc7d1	svítivý
veleobrům	veleobr	k1gMnPc3	veleobr
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
hvězd	hvězda	k1gFnPc2	hvězda
RV	RV	kA	RV
Tauri	Taur	k1gFnSc2	Taur
spadá	spadat	k5eAaImIp3nS	spadat
to	ten	k3xDgNnSc1	ten
třídy	třída	k1gFnPc4	třída
Ia	ia	k0	ia
(	(	kIx(	(
<g/>
zářivý	zářivý	k2eAgMnSc1d1	zářivý
veleobr	veleobr	k1gMnSc1	veleobr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
hmotná	hmotný	k2eAgFnSc1d1	hmotná
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
i	i	k9	i
některých	některý	k3yIgFnPc2	některý
hvězd	hvězda	k1gFnPc2	hvězda
z	z	k7c2	z
AGB	AGB	kA	AGB
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
proměnné	proměnný	k2eAgFnPc4d1	proměnná
hvězdy	hvězda	k1gFnPc4	hvězda
W	W	kA	W
Virginis	Virginis	k1gFnSc2	Virginis
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
svítivost	svítivost	k1gFnSc1	svítivost
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
termální	termální	k2eAgNnSc4d1	termální
pulzování	pulzování	k1gNnSc4	pulzování
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
Mirid	Mirid	k1gInSc1	Mirid
má	mít	k5eAaImIp3nS	mít
spektrální	spektrální	k2eAgInPc4d1	spektrální
typy	typ	k1gInPc4	typ
náležící	náležící	k2eAgInPc4d1	náležící
svítivým	svítivý	k2eAgMnPc3d1	svítivý
veleobrům	veleobr	k1gMnPc3	veleobr
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Alfa	alfa	k1gFnSc1	alfa
Herculis	Herculis	k1gFnSc1	Herculis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proměnné	proměnný	k2eAgFnPc1d1	proměnná
hvězdy	hvězda	k1gFnPc1	hvězda
Cefeidy	Cefeida	k1gFnSc2	Cefeida
obvykle	obvykle	k6eAd1	obvykle
mají	mít	k5eAaImIp3nP	mít
spektrální	spektrální	k2eAgFnPc1d1	spektrální
typy	typa	k1gFnPc1	typa
náležící	náležící	k2eAgFnPc1d1	náležící
svítivým	svítivý	k2eAgMnPc3d1	svítivý
veleobrům	veleobr	k1gMnPc3	veleobr
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
pouze	pouze	k6eAd1	pouze
nejzářivější	zářivý	k2eAgFnPc1d3	nejzářivější
a	a	k8xC	a
nejhmotnější	hmotný	k2eAgFnPc1d3	nejhmotnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
dokážou	dokázat	k5eAaPmIp3nP	dokázat
vytvořit	vytvořit	k5eAaPmF	vytvořit
železné	železný	k2eAgNnSc1d1	železné
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jich	on	k3xPp3gFnPc2	on
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
průměrně	průměrně	k6eAd1	průměrně
hmotné	hmotný	k2eAgFnPc4d1	hmotná
hvězdy	hvězda	k1gFnPc4	hvězda
spalující	spalující	k2eAgNnSc1d1	spalující
helium	helium	k1gNnSc1	helium
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
vyvinou	vyvinout	k5eAaPmIp3nP	vyvinout
v	v	k7c4	v
asymptotické	asymptotický	k2eAgMnPc4d1	asymptotický
obry	obr	k1gMnPc4	obr
<g/>
.	.	kIx.	.
</s>
<s>
Delta	delta	k1gFnSc1	delta
Cephei	Cephe	k1gFnSc2	Cephe
je	být	k5eAaImIp3nS	být
takovým	takový	k3xDgInSc7	takový
příkladem	příklad	k1gInSc7	příklad
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
krát	krát	k6eAd1	krát
svítivější	svítivý	k2eAgFnSc4d2	svítivější
a	a	k8xC	a
4,5	[number]	k4	4,5
<g/>
krát	krát	k6eAd1	krát
hmotnější	hmotný	k2eAgFnSc1d2	hmotnější
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Wolf	Wolf	k1gMnSc1	Wolf
<g/>
–	–	k?	–
<g/>
Rayetovy	Rayetův	k2eAgFnPc1d1	Rayetův
hvězdy	hvězda	k1gFnPc1	hvězda
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
hmotné	hmotný	k2eAgFnPc1d1	hmotná
zářivé	zářivý	k2eAgFnPc1d1	zářivá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
žhavější	žhavý	k2eAgFnSc1d2	žhavější
než	než	k8xS	než
většina	většina	k1gFnSc1	většina
veleobrů	veleobr	k1gMnPc2	veleobr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
jejich	jejich	k3xOp3gFnPc7	jejich
vysokými	vysoký	k2eAgFnPc7d1	vysoká
povrchovými	povrchový	k2eAgFnPc7d1	povrchová
teplotami	teplota	k1gFnPc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
spektru	spektrum	k1gNnSc6	spektrum
dominují	dominovat	k5eAaImIp3nP	dominovat
helium	helium	k1gNnSc4	helium
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
těžké	těžký	k2eAgInPc4d1	těžký
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
mají	mít	k5eAaImIp3nP	mít
málo	málo	k6eAd1	málo
nebo	nebo	k8xC	nebo
žádný	žádný	k3yNgInSc4	žádný
vodík	vodík	k1gInSc4	vodík
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
vyvinuté	vyvinutý	k2eAgFnPc4d1	vyvinutá
hvězdy	hvězda	k1gFnPc4	hvězda
než	než	k8xS	než
veleobři	veleobr	k1gMnPc1	veleobr
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
hvězdy	hvězda	k1gFnPc4	hvězda
z	z	k7c2	z
AGB	AGB	kA	AGB
se	se	k3xPyFc4	se
Wolf-Rayetovy	Wolf-Rayetův	k2eAgFnPc1d1	Wolf-Rayetova
hvězdy	hvězda	k1gFnPc1	hvězda
mohou	moct	k5eAaImIp3nP	moct
nacházet	nacházet	k5eAaImF	nacházet
jak	jak	k6eAd1	jak
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
oblasti	oblast	k1gFnSc6	oblast
HR	hr	k2eAgInSc2d1	hr
diagramu	diagram	k1gInSc2	diagram
jako	jako	k9	jako
červení	červený	k2eAgMnPc1d1	červený
veleobři	veleobr	k1gMnPc1	veleobr
<g/>
,	,	kIx,	,
tak	tak	k9	tak
také	také	k9	také
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
patřících	patřící	k2eAgFnPc2d1	patřící
hvězdám	hvězda	k1gFnPc3	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
i	i	k9	i
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
patřících	patřící	k2eAgFnPc2d1	patřící
nejžhavějším	žhavý	k2eAgMnPc3d3	nejžhavější
modrým	modrý	k2eAgMnPc3d1	modrý
veleobrům	veleobr	k1gMnPc3	veleobr
<g/>
.	.	kIx.	.
</s>
<s>
Nejhmotnější	hmotný	k2eAgFnPc1d3	nejhmotnější
a	a	k8xC	a
nejzářivější	zářivý	k2eAgFnPc1d3	nejzářivější
hvězdy	hvězda	k1gFnPc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
k	k	k7c3	k
nerozeznání	nerozeznání	k1gNnSc3	nerozeznání
od	od	k7c2	od
veleobrů	veleobr	k1gMnPc2	veleobr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
promění	proměnit	k5eAaPmIp3nS	proměnit
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
téměř	téměř	k6eAd1	téměř
stejnou	stejný	k2eAgFnSc4d1	stejná
teplotu	teplota	k1gFnSc4	teplota
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc4d1	podobný
zářivý	zářivý	k2eAgInSc4d1	zářivý
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
a	a	k8xC	a
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
detailní	detailní	k2eAgFnSc1d1	detailní
analýza	analýza	k1gFnSc1	analýza
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
vlastností	vlastnost	k1gFnPc2	vlastnost
dokáže	dokázat	k5eAaPmIp3nS	dokázat
rozlišit	rozlišit	k5eAaPmF	rozlišit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
hvězdu	hvězda	k1gFnSc4	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
třídy	třída	k1gFnSc2	třída
O	O	kA	O
a	a	k8xC	a
ne	ne	k9	ne
raného	raný	k2eAgMnSc4d1	raný
veleobra	veleobr	k1gMnSc4	veleobr
třídy	třída	k1gFnSc2	třída
O.	O.	kA	O.
</s>
</p>
<p>
<s>
Zářivé	zářivý	k2eAgFnPc1d1	zářivá
proměnné	proměnná	k1gFnPc1	proměnná
modré	modrý	k2eAgFnSc2d1	modrá
hvězdy	hvězda	k1gFnSc2	hvězda
(	(	kIx(	(
<g/>
LBV	LBV	kA	LBV
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
typem	typ	k1gInSc7	typ
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
oblasti	oblast	k1gFnSc6	oblast
HR	hr	k2eAgInSc2d1	hr
diagramu	diagram	k1gInSc2	diagram
jako	jako	k9	jako
modří	modrý	k2eAgMnPc1d1	modrý
veleobři	veleobr	k1gMnPc1	veleobr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
zařazeni	zařadit	k5eAaPmNgMnP	zařadit
do	do	k7c2	do
samostatné	samostatný	k2eAgFnSc2d1	samostatná
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
<g/>
,	,	kIx,	,
expandované	expandovaný	k2eAgInPc1d1	expandovaný
<g/>
,	,	kIx,	,
masivní	masivní	k2eAgFnSc1d1	masivní
a	a	k8xC	a
zářivé	zářivý	k2eAgFnPc1d1	zářivá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
hyperobři	hyperobr	k1gMnPc1	hyperobr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
specifickou	specifický	k2eAgFnSc4d1	specifická
spektrální	spektrální	k2eAgFnSc4d1	spektrální
variabilitu	variabilita	k1gFnSc4	variabilita
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
LBV	LBV	kA	LBV
pozorované	pozorovaný	k2eAgInPc1d1	pozorovaný
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
stabilní	stabilní	k2eAgFnPc1d1	stabilní
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
chybně	chybně	k6eAd1	chybně
zařazeny	zařadit	k5eAaPmNgInP	zařadit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
veleobrů	veleobr	k1gMnPc2	veleobr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hyperobři	Hyperobr	k1gMnPc1	Hyperobr
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
kategorii	kategorie	k1gFnSc4	kategorie
hvězd	hvězda	k1gFnPc2	hvězda
než	než	k8xS	než
veleobři	veleobr	k1gMnPc1	veleobr
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
jen	jen	k9	jen
o	o	k7c4	o
jasnější	jasný	k2eAgFnPc4d2	jasnější
verze	verze	k1gFnPc4	verze
veleobrů	veleobr	k1gMnPc2	veleobr
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vyvinutí	vyvinutý	k2eAgMnPc1d1	vyvinutý
<g/>
,	,	kIx,	,
expandovaní	expandovaný	k2eAgMnPc1d1	expandovaný
<g/>
,	,	kIx,	,
masivní	masivní	k2eAgMnPc1d1	masivní
a	a	k8xC	a
zářiví	zářivý	k2eAgMnPc1d1	zářivý
jako	jako	k8xS	jako
veleobři	veleobr	k1gMnPc1	veleobr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tyto	tento	k3xDgFnPc1	tento
charakteristiky	charakteristika	k1gFnPc1	charakteristika
jsou	být	k5eAaImIp3nP	být
dotažené	dotažený	k2eAgFnPc1d1	dotažená
do	do	k7c2	do
extrému	extrém	k1gInSc2	extrém
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
vlastnosti	vlastnost	k1gFnPc1	vlastnost
částečně	částečně	k6eAd1	částečně
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
velké	velký	k2eAgFnPc4d1	velká
ztráty	ztráta	k1gFnPc4	ztráta
hmoty	hmota	k1gFnSc2	hmota
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
jejich	jejich	k3xOp3gInSc2	jejich
extrémního	extrémní	k2eAgInSc2d1	extrémní
zářivého	zářivý	k2eAgInSc2d1	zářivý
výkonu	výkon	k1gInSc2	výkon
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojené	spojený	k2eAgFnPc1d1	spojená
nestability	nestabilita	k1gFnSc2	nestabilita
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
velmi	velmi	k6eAd1	velmi
vyvinutí	vyvinutý	k2eAgMnPc1d1	vyvinutý
veleobři	veleobr	k1gMnPc1	veleobr
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
známky	známka	k1gFnPc4	známka
hyperobrů	hyperobr	k1gInPc2	hyperobr
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
ztratí	ztratit	k5eAaPmIp3nS	ztratit
mnoho	mnoho	k6eAd1	mnoho
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc4	některý
hvězdy	hvězda	k1gFnPc4	hvězda
třídy	třída	k1gFnSc2	třída
B	B	kA	B
<g/>
[	[	kIx(	[
<g/>
e	e	k0	e
<g/>
]	]	kIx)	]
jsou	být	k5eAaImIp3nP	být
veleobry	veleobr	k1gMnPc4	veleobr
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
jiné	jiný	k2eAgFnPc1d1	jiná
hvězdy	hvězda	k1gFnPc1	hvězda
jimi	on	k3xPp3gFnPc7	on
zjevně	zjevně	k6eAd1	zjevně
nejsou	být	k5eNaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
řadí	řadit	k5eAaImIp3nP	řadit
B	B	kA	B
<g/>
[	[	kIx(	[
<g/>
e	e	k0	e
<g/>
]	]	kIx)	]
objekty	objekt	k1gInPc4	objekt
odděleně	odděleně	k6eAd1	odděleně
od	od	k7c2	od
veleobrů	veleobr	k1gMnPc2	veleobr
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiní	jiný	k2eAgMnPc1d1	jiný
definují	definovat	k5eAaBmIp3nP	definovat
masivní	masivní	k2eAgNnSc4d1	masivní
vyvinuté	vyvinutý	k2eAgNnSc4d1	vyvinuté
B	B	kA	B
<g/>
[	[	kIx(	[
<g/>
e	e	k0	e
<g/>
]	]	kIx)	]
hvězdy	hvězda	k1gFnPc4	hvězda
jako	jako	k8xC	jako
podskupinu	podskupina	k1gFnSc4	podskupina
veleobrů	veleobr	k1gMnPc2	veleobr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Veleobři	veleobr	k1gMnPc1	veleobr
mají	mít	k5eAaImIp3nP	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
od	od	k7c2	od
8	[number]	k4	8
až	až	k9	až
12	[number]	k4	12
hmotností	hmotnost	k1gFnPc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
výš	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
a	a	k8xC	a
zářivý	zářivý	k2eAgInSc1d1	zářivý
výkon	výkon	k1gInSc1	výkon
tisíckrát	tisíckrát	k6eAd1	tisíckrát
až	až	k9	až
milionkrát	milionkrát	k6eAd1	milionkrát
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Značně	značně	k6eAd1	značně
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
poloměrem	poloměr	k1gInSc7	poloměr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
30	[number]	k4	30
<g/>
krát	krát	k6eAd1	krát
až	až	k9	až
500	[number]	k4	500
<g/>
krát	krát	k6eAd1	krát
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
dokonce	dokonce	k9	dokonce
1000	[number]	k4	1000
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
poloměr	poloměr	k1gInSc4	poloměr
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
masivní	masivní	k2eAgNnPc4d1	masivní
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
zažehnout	zažehnout	k5eAaPmF	zažehnout
fúzi	fúze	k1gFnSc3	fúze
hélia	hélium	k1gNnSc2	hélium
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnSc1	jejich
jádro	jádro	k1gNnSc1	jádro
zhroutí	zhroutit	k5eAaPmIp3nS	zhroutit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
bez	bez	k7c2	bez
dodatečných	dodatečný	k2eAgInPc2d1	dodatečný
výbojů	výboj	k1gInPc2	výboj
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
hvězdy	hvězda	k1gFnPc4	hvězda
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
poté	poté	k6eAd1	poté
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
ve	v	k7c6	v
spalování	spalování	k1gNnSc6	spalování
těžších	těžký	k2eAgInPc2d2	těžší
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
jádru	jádro	k1gNnSc6	jádro
začne	začít	k5eAaPmIp3nS	začít
tvořit	tvořit	k5eAaImF	tvořit
železo	železo	k1gNnSc4	železo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hmotností	hmotnost	k1gFnSc7	hmotnost
vyústí	vyústit	k5eAaPmIp3nS	vyústit
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
explozi	exploze	k1gFnSc6	exploze
jako	jako	k9	jako
supernova	supernova	k1gFnSc1	supernova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stefanův-Boltzmannův	Stefanův-Boltzmannův	k2eAgInSc1d1	Stefanův-Boltzmannův
zákon	zákon	k1gInSc1	zákon
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
relativně	relativně	k6eAd1	relativně
chladné	chladný	k2eAgInPc4d1	chladný
povrchy	povrch	k1gInPc4	povrch
červených	červený	k2eAgMnPc2d1	červený
veleobrů	veleobr	k1gMnPc2	veleobr
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
energie	energie	k1gFnSc2	energie
než	než	k8xS	než
modří	modrý	k2eAgMnPc1d1	modrý
veleobři	veleobr	k1gMnPc1	veleobr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
při	při	k7c6	při
stejné	stejný	k2eAgFnSc6d1	stejná
svítivosti	svítivost	k1gFnSc6	svítivost
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
červení	červený	k2eAgMnPc1d1	červený
veleobři	veleobr	k1gMnPc1	veleobr
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
jejich	jejich	k3xOp3gMnPc1	jejich
modří	modrý	k2eAgMnPc1d1	modrý
protějšci	protějšek	k1gMnPc1	protějšek
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
udržuje	udržovat	k5eAaImIp3nS	udržovat
největší	veliký	k2eAgMnPc4d3	veliký
chladné	chladný	k2eAgMnPc4d1	chladný
veleobry	veleobr	k1gMnPc4	veleobr
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
až	až	k9	až
kolem	kolem	k7c2	kolem
1	[number]	k4	1
500	[number]	k4	500
poloměrů	poloměr	k1gInPc2	poloměr
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
nejmohutnější	mohutný	k2eAgMnPc4d3	nejmohutnější
horké	horký	k2eAgMnPc4d1	horký
veleobry	veleobr	k1gMnPc4	veleobr
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
milionkrát	milionkrát	k6eAd1	milionkrát
větší	veliký	k2eAgFnPc4d2	veliký
zářivosti	zářivost	k1gFnPc4	zářivost
<g/>
,	,	kIx,	,
než	než	k8xS	než
má	mít	k5eAaImIp3nS	mít
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
blízké	blízký	k2eAgFnPc1d1	blízká
těmto	tento	k3xDgFnPc3	tento
hodnotám	hodnota	k1gFnPc3	hodnota
jsou	být	k5eAaImIp3nP	být
nestabilní	stabilní	k2eNgFnSc4d1	nestabilní
<g/>
,	,	kIx,	,
pulzují	pulzovat	k5eAaImIp3nP	pulzovat
a	a	k8xC	a
zažívají	zažívat	k5eAaImIp3nP	zažívat
rychlé	rychlý	k2eAgFnPc4d1	rychlá
ztráty	ztráta	k1gFnPc4	ztráta
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Povrchová	povrchový	k2eAgFnSc1d1	povrchová
gravitace	gravitace	k1gFnSc1	gravitace
===	===	k?	===
</s>
</p>
<p>
<s>
Třídy	třída	k1gFnPc1	třída
svítivosti	svítivost	k1gFnSc2	svítivost
jsou	být	k5eAaImIp3nP	být
veleobrům	veleobr	k1gMnPc3	veleobr
přiřazeny	přiřazen	k2eAgFnPc4d1	přiřazena
na	na	k7c6	na
základě	základ	k1gInSc6	základ
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
odrazem	odraz	k1gInSc7	odraz
povrchové	povrchový	k2eAgFnSc2d1	povrchová
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
také	také	k6eAd1	také
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
dalšími	další	k2eAgFnPc7d1	další
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
mikroturbulencemi	mikroturbulence	k1gFnPc7	mikroturbulence
<g/>
.	.	kIx.	.
</s>
<s>
Veleobři	veleobr	k1gMnPc1	veleobr
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
gravitaci	gravitace	k1gFnSc4	gravitace
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
přibližně	přibližně	k6eAd1	přibližně
log	log	kA	log
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
2,0	[number]	k4	2,0
cgs	cgs	k?	cgs
a	a	k8xC	a
nižší	nízký	k2eAgMnSc1d2	nižší
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
svítiví	svítivý	k2eAgMnPc1d1	svítivý
obři	obr	k1gMnPc1	obr
(	(	kIx(	(
<g/>
třída	třída	k1gFnSc1	třída
svítivosti	svítivost	k1gFnSc2	svítivost
II	II	kA	II
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
statisticky	statisticky	k6eAd1	statisticky
velmi	velmi	k6eAd1	velmi
podobnou	podobný	k2eAgFnSc4d1	podobná
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
jako	jako	k8xC	jako
veleobři	veleobr	k1gMnPc1	veleobr
třídy	třída	k1gFnSc2	třída
Ib	Ib	k1gFnSc2	Ib
<g/>
.	.	kIx.	.
</s>
<s>
Chladní	chladný	k2eAgMnPc1d1	chladný
zářiví	zářivý	k2eAgMnPc1d1	zářivý
červení	červený	k2eAgMnPc1d1	červený
veleobři	veleobr	k1gMnPc1	veleobr
mají	mít	k5eAaImIp3nP	mít
nižší	nízký	k2eAgFnSc4d2	nižší
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
gravitaci	gravitace	k1gFnSc4	gravitace
<g/>
,	,	kIx,	,
u	u	k7c2	u
nejzářivějších	zářivý	k2eAgFnPc2d3	nejzářivější
nestabilních	stabilní	k2eNgFnPc2d1	nestabilní
hvězd	hvězda	k1gFnPc2	hvězda
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
log	log	kA	log
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
okolo	okolo	k7c2	okolo
nuly	nula	k1gFnSc2	nula
<g/>
.	.	kIx.	.
</s>
<s>
Žhavější	žhavý	k2eAgMnPc1d2	žhavější
veleobři	veleobr	k1gMnPc1	veleobr
<g/>
,	,	kIx,	,
i	i	k8xC	i
ti	ten	k3xDgMnPc1	ten
nejzářivější	zářivý	k2eAgMnPc1d3	nejzářivější
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
gravitaci	gravitace	k1gFnSc4	gravitace
kolem	kolem	k7c2	kolem
jedné	jeden	k4xCgFnSc2	jeden
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
jejich	jejich	k3xOp3gFnSc3	jejich
vyšší	vysoký	k2eAgFnSc3d2	vyšší
hmotnosti	hmotnost	k1gFnSc3	hmotnost
a	a	k8xC	a
menšímu	malý	k2eAgInSc3d2	menší
poloměru	poloměr	k1gInSc3	poloměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Teplota	teplota	k1gFnSc1	teplota
===	===	k?	===
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
veleobři	veleobr	k1gMnPc1	veleobr
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
spektrálních	spektrální	k2eAgFnPc6d1	spektrální
třídách	třída	k1gFnPc6	třída
a	a	k8xC	a
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
rozsahu	rozsah	k1gInSc6	rozsah
teplot	teplota	k1gFnPc2	teplota
<g/>
,	,	kIx,	,
od	od	k7c2	od
hvězd	hvězda	k1gFnPc2	hvězda
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
třídy	třída	k1gFnSc2	třída
M	M	kA	M
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
3500	[number]	k4	3500
K	K	kA	K
po	po	k7c4	po
nejžhavější	žhavý	k2eAgFnPc4d3	nejžhavější
hvězdy	hvězda	k1gFnPc4	hvězda
třídy	třída	k1gFnSc2	třída
O	O	kA	O
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
40	[number]	k4	40
000	[number]	k4	000
tisíc	tisíc	k4xCgInPc2	tisíc
Kelvinů	kelvin	k1gInPc2	kelvin
<g/>
.	.	kIx.	.
</s>
<s>
Veleobři	veleobr	k1gMnPc1	veleobr
obecně	obecně	k6eAd1	obecně
nejsou	být	k5eNaImIp3nP	být
chladnější	chladný	k2eAgFnPc1d2	chladnější
než	než	k8xS	než
střední	střední	k2eAgFnPc1d1	střední
hvězdy	hvězda	k1gFnSc2	hvězda
třídy	třída	k1gFnSc2	třída
M	M	kA	M
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
taková	takový	k3xDgNnPc4	takový
tělesa	těleso	k1gNnPc4	těleso
by	by	kYmCp3nS	by
teoreticky	teoreticky	k6eAd1	teoreticky
byla	být	k5eAaImAgFnS	být
katastroficky	katastroficky	k6eAd1	katastroficky
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
existují	existovat	k5eAaImIp3nP	existovat
potenciální	potenciální	k2eAgFnPc4d1	potenciální
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
VX	VX	kA	VX
Sagittarii	Sagittarie	k1gFnSc6	Sagittarie
<g/>
.	.	kIx.	.
<g/>
Ačkoli	ačkoli	k8xS	ačkoli
existují	existovat	k5eAaImIp3nP	existovat
příklady	příklad	k1gInPc4	příklad
veleobrů	veleobr	k1gMnPc2	veleobr
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
třídě	třída	k1gFnSc6	třída
z	z	k7c2	z
O	O	kA	O
do	do	k7c2	do
M	M	kA	M
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
jsou	být	k5eAaImIp3nP	být
spektrální	spektrální	k2eAgInSc4d1	spektrální
typ	typ	k1gInSc4	typ
B	B	kA	B
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
ostatních	ostatní	k2eAgFnPc6d1	ostatní
spektrálních	spektrální	k2eAgFnPc6d1	spektrální
třídách	třída	k1gFnPc6	třída
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc1d2	menší
skupina	skupina	k1gFnSc1	skupina
málo	málo	k6eAd1	málo
svítivých	svítivý	k2eAgMnPc2d1	svítivý
veleobrů	veleobr	k1gMnPc2	veleobr
existuje	existovat	k5eAaImIp3nS	existovat
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
G	G	kA	G
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
hvězdy	hvězda	k1gFnPc1	hvězda
střední	střední	k2eAgFnSc2d1	střední
velikosti	velikost	k1gFnSc2	velikost
spalují	spalovat	k5eAaImIp3nP	spalovat
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
helium	helium	k1gNnSc4	helium
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
přesunou	přesunout	k5eAaPmIp3nP	přesunout
do	do	k7c2	do
asymptotické	asymptotický	k2eAgFnSc2d1	asymptotická
větev	větev	k1gFnSc4	větev
obrů	obr	k1gMnPc2	obr
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
svítiví	svítivý	k2eAgMnPc1d1	svítivý
veleobři	veleobr	k1gMnPc1	veleobr
nízké	nízký	k2eAgFnSc2d1	nízká
třídy	třída	k1gFnSc2	třída
B	B	kA	B
(	(	kIx(	(
<g/>
B	B	kA	B
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnPc4d1	vysoká
třídy	třída	k1gFnPc4	třída
O	o	k7c4	o
(	(	kIx(	(
<g/>
O	o	k7c6	o
<g/>
9,5	[number]	k4	9,5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k6eAd1	ještě
běžnější	běžný	k2eAgFnPc1d2	běžnější
než	než	k8xS	než
hvězdy	hvězda	k1gFnPc1	hvězda
hlavní	hlavní	k2eAgFnPc4d1	hlavní
posloupnosti	posloupnost	k1gFnPc4	posloupnost
těchto	tento	k3xDgInPc2	tento
spektrálních	spektrální	k2eAgInPc2d1	spektrální
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
<g/>
Relativní	relativní	k2eAgInSc1d1	relativní
počet	počet	k1gInSc1	počet
modrých	modrý	k2eAgFnPc2d1	modrá
<g/>
,	,	kIx,	,
žlutých	žlutý	k2eAgFnPc2d1	žlutá
a	a	k8xC	a
červených	červený	k2eAgMnPc2d1	červený
veleobrů	veleobr	k1gMnPc2	veleobr
je	být	k5eAaImIp3nS	být
ukazatelem	ukazatel	k1gInSc7	ukazatel
rychlosti	rychlost	k1gFnSc2	rychlost
vývoje	vývoj	k1gInSc2	vývoj
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
silný	silný	k2eAgInSc1d1	silný
testovací	testovací	k2eAgInSc1d1	testovací
model	model	k1gInSc1	model
vývoje	vývoj	k1gInSc2	vývoj
hmotných	hmotný	k2eAgFnPc2d1	hmotná
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Svítivost	svítivost	k1gFnSc4	svítivost
===	===	k?	===
</s>
</p>
<p>
<s>
Veleobři	veleobr	k1gMnPc1	veleobr
leží	ležet	k5eAaImIp3nP	ležet
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
horizontálním	horizontální	k2eAgNnSc6d1	horizontální
pásmu	pásmo	k1gNnSc6	pásmo
zabírajícím	zabírající	k2eAgNnSc6d1	zabírající
celou	celý	k2eAgFnSc4d1	celá
horní	horní	k2eAgFnSc4d1	horní
část	část	k1gFnSc4	část
HR	hr	k2eAgInSc2d1	hr
diagramu	diagram	k1gInSc2	diagram
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
existují	existovat	k5eAaImIp3nP	existovat
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
spektrálních	spektrální	k2eAgInPc6d1	spektrální
typech	typ	k1gInPc6	typ
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
rozdíly	rozdíl	k1gInPc1	rozdíl
jsou	být	k5eAaImIp3nP	být
částečně	částečně	k6eAd1	částečně
důsledkem	důsledek	k1gInSc7	důsledek
různých	různý	k2eAgFnPc2d1	různá
metod	metoda	k1gFnPc2	metoda
pro	pro	k7c4	pro
přiřazení	přiřazení	k1gNnSc4	přiřazení
třídy	třída	k1gFnSc2	třída
svítivosti	svítivost	k1gFnPc4	svítivost
různým	různý	k2eAgInPc3d1	různý
spektrálním	spektrální	k2eAgInPc3d1	spektrální
typům	typ	k1gInPc3	typ
<g/>
,	,	kIx,	,
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
odráží	odrážet	k5eAaImIp3nS	odrážet
skutečné	skutečný	k2eAgInPc4d1	skutečný
fyzické	fyzický	k2eAgInPc4d1	fyzický
rozdíly	rozdíl	k1gInPc4	rozdíl
ve	v	k7c6	v
hvězdách	hvězda	k1gFnPc6	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Bolometrická	bolometrický	k2eAgFnSc1d1	bolometrická
svítivost	svítivost	k1gFnSc1	svítivost
hvězdy	hvězda	k1gFnSc2	hvězda
odráží	odrážet	k5eAaImIp3nS	odrážet
celkové	celkový	k2eAgNnSc1d1	celkové
výstupní	výstupní	k2eAgNnSc1d1	výstupní
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
velmi	velmi	k6eAd1	velmi
horké	horký	k2eAgFnPc4d1	horká
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
chladné	chladný	k2eAgFnSc2d1	chladná
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
bolometrická	bolometrický	k2eAgFnSc1d1	bolometrická
svítivost	svítivost	k1gFnSc1	svítivost
výrazně	výrazně	k6eAd1	výrazně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
svítivost	svítivost	k1gFnSc1	svítivost
vizuální	vizuální	k2eAgFnSc1d1	vizuální
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
několika	několik	k4yIc2	několik
magnitud	magnitudo	k1gNnPc2	magnitudo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
bolometrická	bolometrický	k2eAgFnSc1d1	bolometrická
korekce	korekce	k1gFnSc1	korekce
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
přibližně	přibližně	k6eAd1	přibližně
jedné	jeden	k4xCgFnSc2	jeden
magnitudě	magnitudě	k6eAd1	magnitudě
pro	pro	k7c4	pro
střední	střední	k2eAgFnPc4d1	střední
hvězdy	hvězda	k1gFnPc4	hvězda
třídy	třída	k1gFnSc2	třída
B	B	kA	B
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgFnPc4d1	vysoká
hvězdy	hvězda	k1gFnPc4	hvězda
třídy	třída	k1gFnSc2	třída
K	k	k7c3	k
<g/>
,	,	kIx,	,
a	a	k8xC	a
nízké	nízký	k2eAgFnPc4d1	nízká
hvězdy	hvězda	k1gFnPc4	hvězda
třídy	třída	k1gFnSc2	třída
M	M	kA	M
<g/>
,	,	kIx,	,
a	a	k8xC	a
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
se	se	k3xPyFc4	se
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
magnitudy	magnitud	k1gInPc4	magnitud
pro	pro	k7c4	pro
třídu	třída	k1gFnSc4	třída
O	O	kA	O
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
hvězdy	hvězda	k1gFnSc2	hvězda
třídy	třída	k1gFnSc2	třída
M.	M.	kA	M.
</s>
</p>
<p>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
veleobři	veleobr	k1gMnPc1	veleobr
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgMnPc1d2	veliký
a	a	k8xC	a
zářivější	zářivý	k2eAgMnPc1d2	zářivější
<g/>
,	,	kIx,	,
než	než	k8xS	než
hvězdy	hvězda	k1gFnPc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
teplotou	teplota	k1gFnSc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
hvězda	hvězda	k1gFnSc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
třídy	třída	k1gFnSc2	třída
B0	B0	k1gFnSc2	B0
má	mít	k5eAaImIp3nS	mít
absolutní	absolutní	k2eAgFnSc4d1	absolutní
magnitudu	magnituda	k1gFnSc4	magnituda
-5	-5	k4	-5
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
veleobři	veleobr	k1gMnPc1	veleobr
třídy	třída	k1gFnSc2	třída
B0	B0	k1gFnSc2	B0
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
zářivější	zářivý	k2eAgFnPc1d2	zářivější
než	než	k8xS	než
absolutní	absolutní	k2eAgFnSc1d1	absolutní
magnituda	magnituda	k1gFnSc1	magnituda
-5	-5	k4	-5
<g/>
.	.	kIx.	.
</s>
<s>
Bolometrická	bolometrický	k2eAgFnSc1d1	bolometrická
svítivost	svítivost	k1gFnSc1	svítivost
i	i	k9	i
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
nejslabší	slabý	k2eAgMnPc4d3	nejslabší
modré	modrý	k2eAgMnPc4d1	modrý
veleobry	veleobr	k1gMnPc4	veleobr
je	být	k5eAaImIp3nS	být
desettisíckrát	desettisíckrát	k6eAd1	desettisíckrát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
svítivost	svítivost	k1gFnSc1	svítivost
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
nejsilnější	silný	k2eAgFnPc1d3	nejsilnější
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
milionkrát	milionkrát	k6eAd1	milionkrát
silnější	silný	k2eAgMnSc1d2	silnější
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
hvězdy	hvězda	k1gFnPc1	hvězda
typu	typ	k1gInSc2	typ
Alfa	alfa	k1gFnSc1	alfa
Cygni	Cygeň	k1gFnSc3	Cygeň
nebo	nebo	k8xC	nebo
zářivé	zářivý	k2eAgFnSc3d1	zářivá
proměnné	proměnná	k1gFnSc3	proměnná
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Nejžhavější	žhavý	k2eAgMnPc1d3	nejžhavější
veleobři	veleobr	k1gMnPc1	veleobr
nízkého	nízký	k2eAgNnSc2d1	nízké
spektrálních	spektrální	k2eAgFnPc6d1	spektrální
typu	typa	k1gFnSc4	typa
O	o	k7c4	o
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
úzkém	úzký	k2eAgNnSc6d1	úzké
rozmezí	rozmezí	k1gNnSc6	rozmezí
svítivosti	svítivost	k1gFnSc2	svítivost
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
vysoce	vysoce	k6eAd1	vysoce
svítivými	svítivý	k2eAgFnPc7d1	svítivá
nízkými	nízký	k2eAgFnPc7d1	nízká
hvězdami	hvězda	k1gFnPc7	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
třídy	třída	k1gFnSc2	třída
O	O	kA	O
a	a	k8xC	a
obřími	obří	k2eAgFnPc7d1	obří
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
řazeni	řazen	k2eAgMnPc1d1	řazen
samostatně	samostatně	k6eAd1	samostatně
do	do	k7c2	do
třídy	třída	k1gFnSc2	třída
normálních	normální	k2eAgFnPc2d1	normální
(	(	kIx(	(
<g/>
Ib	Ib	k1gFnPc2	Ib
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
zářivých	zářivý	k2eAgInPc2d1	zářivý
(	(	kIx(	(
<g/>
Ia	ia	k0	ia
<g/>
)	)	kIx)	)
veleobrů	veleobr	k1gMnPc2	veleobr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
obyčejně	obyčejně	k6eAd1	obyčejně
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
spektrální	spektrální	k2eAgInSc4d1	spektrální
modifikátor	modifikátor	k1gInSc4	modifikátor
"	"	kIx"	"
<g/>
f	f	k?	f
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
emise	emise	k1gFnPc4	emise
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
helia	helium	k1gNnSc2	helium
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hvězda	hvězda	k1gFnSc1	hvězda
HD	HD	kA	HD
93129A	[number]	k4	93129A
je	být	k5eAaImIp3nS	být
spektrální	spektrální	k2eAgInSc1d1	spektrální
typ	typ	k1gInSc1	typ
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
If	If	k1gFnPc2	If
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Žlutí	žlutý	k2eAgMnPc1d1	žlutý
veleobři	veleobr	k1gMnPc1	veleobr
jsou	být	k5eAaImIp3nP	být
podstatně	podstatně	k6eAd1	podstatně
méně	málo	k6eAd2	málo
zářivější	zářivý	k2eAgFnSc1d2	zářivější
než	než	k8xS	než
absolutní	absolutní	k2eAgFnSc1d1	absolutní
magnituda	magnituda	k1gFnSc1	magnituda
-5	-5	k4	-5
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
okolo	okolo	k7c2	okolo
hodnoty	hodnota	k1gFnSc2	hodnota
-2	-2	k4	-2
jako	jako	k8xS	jako
například	například	k6eAd1	například
hvězda	hvězda	k1gFnSc1	hvězda
14	[number]	k4	14
Persei	Persei	k1gNnSc2	Persei
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
bolometrickou	bolometrický	k2eAgFnSc7d1	bolometrická
korekcí	korekce	k1gFnSc7	korekce
okolo	okolo	k7c2	okolo
nuly	nula	k1gFnSc2	nula
tak	tak	k6eAd1	tak
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
stokrát	stokrát	k6eAd1	stokrát
svítivější	svítivý	k2eAgFnSc1d2	svítivější
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
hmotné	hmotný	k2eAgFnPc4d1	hmotná
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
mají	mít	k5eAaImIp3nP	mít
obzvlášť	obzvlášť	k6eAd1	obzvlášť
nízkou	nízký	k2eAgFnSc4d1	nízká
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
gravitaci	gravitace	k1gFnSc4	gravitace
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
kvůli	kvůli	k7c3	kvůli
nestabilitě	nestabilita	k1gFnSc3	nestabilita
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
pulzující	pulzující	k2eAgInPc4d1	pulzující
proměnné	proměnný	k2eAgInPc4d1	proměnný
Cepheidy	Cepheid	k1gInPc4	Cepheid
<g/>
.	.	kIx.	.
</s>
<s>
Nejzářivější	zářivý	k2eAgFnPc1d3	nejzářivější
žluté	žlutý	k2eAgFnPc1d1	žlutá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
žlutí	žlutý	k2eAgMnPc1d1	žlutý
hyperobři	hyperobr	k1gMnPc1	hyperobr
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
zrakově	zrakově	k6eAd1	zrakově
nejjasnějších	jasný	k2eAgInPc2d3	nejjasnější
hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
absolutní	absolutní	k2eAgFnSc7d1	absolutní
magnitudou	magnitudý	k2eAgFnSc7d1	magnitudý
-9	-9	k4	-9
a	a	k8xC	a
zářivým	zářivý	k2eAgMnSc7d1	zářivý
výkonem	výkon	k1gInSc7	výkon
milionu	milion	k4xCgInSc2	milion
Sluncí	slunce	k1gNnPc2	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Horní	horní	k2eAgFnSc1d1	horní
hranice	hranice	k1gFnSc1	hranice
svítivosti	svítivost	k1gFnSc2	svítivost
červených	červený	k2eAgMnPc2d1	červený
veleobrů	veleobr	k1gMnPc2	veleobr
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
zářivého	zářivý	k2eAgInSc2d1	zářivý
výkonu	výkon	k1gInSc2	výkon
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
jasnější	jasný	k2eAgFnSc1d2	jasnější
než	než	k8xS	než
tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
odhazují	odhazovat	k5eAaImIp3nP	odhazovat
vnější	vnější	k2eAgFnPc4d1	vnější
vrstvy	vrstva	k1gFnPc4	vrstva
své	svůj	k3xOyFgFnSc2	svůj
obálky	obálka	k1gFnSc2	obálka
a	a	k8xC	a
stávají	stávat	k5eAaImIp3nP	stávat
se	se	k3xPyFc4	se
horkými	horký	k2eAgMnPc7d1	horký
modrými	modrý	k2eAgMnPc7d1	modrý
veleobry	veleobr	k1gMnPc7	veleobr
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
červených	červený	k2eAgMnPc2d1	červený
veleobrů	veleobr	k1gMnPc2	veleobr
<g/>
,	,	kIx,	,
když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
hvězdami	hvězda	k1gFnPc7	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
hmotnost	hmotnost	k1gFnSc1	hmotnost
10	[number]	k4	10
až	až	k8xS	až
15	[number]	k4	15
hmotností	hmotnost	k1gFnSc7	hmotnost
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
mají	mít	k5eAaImIp3nP	mít
zářivý	zářivý	k2eAgInSc4d1	zářivý
výkon	výkon	k1gInSc4	výkon
nižší	nízký	k2eAgInSc4d2	nižší
jak	jak	k6eAd1	jak
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
Sluncí	slunce	k1gNnPc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
svítivých	svítivý	k2eAgMnPc2d1	svítivý
veleobrů	veleobr	k1gMnPc2	veleobr
Ia	ia	k0	ia
třídy	třída	k1gFnSc2	třída
M.	M.	kA	M.
Nejméně	málo	k6eAd3	málo
svítivými	svítivý	k2eAgFnPc7d1	svítivá
hvězdami	hvězda	k1gFnPc7	hvězda
klasifikovanými	klasifikovaný	k2eAgFnPc7d1	klasifikovaná
jako	jako	k8xS	jako
červení	červený	k2eAgMnPc1d1	červený
veleobři	veleobr	k1gMnPc1	veleobr
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
nejjasnějších	jasný	k2eAgFnPc2d3	nejjasnější
hvězd	hvězda	k1gFnPc2	hvězda
AGB	AGB	kA	AGB
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
expandované	expandovaný	k2eAgFnPc1d1	expandovaná
a	a	k8xC	a
nestabilní	stabilní	k2eNgFnPc1d1	nestabilní
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
proměnné	proměnný	k2eAgFnPc4d1	proměnná
hvězdy	hvězda	k1gFnPc4	hvězda
RV	RV	kA	RV
Tauri	Taur	k1gFnSc2	Taur
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
AGB	AGB	kA	AGB
hvězd	hvězda	k1gFnPc2	hvězda
náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
obrů	obr	k1gMnPc2	obr
nebo	nebo	k8xC	nebo
zářivých	zářivý	k2eAgMnPc2d1	zářivý
obrů	obr	k1gMnPc2	obr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
nestabilní	stabilní	k2eNgFnPc4d1	nestabilní
proměnné	proměnný	k2eAgFnPc4d1	proměnná
hvězdy	hvězda	k1gFnPc4	hvězda
jako	jako	k8xC	jako
W	W	kA	W
Virginis	Virginis	k1gInSc1	Virginis
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
mezi	mezi	k7c7	mezi
veleobry	veleobr	k1gMnPc7	veleobr
<g/>
.	.	kIx.	.
</s>
<s>
Nejslabší	slabý	k2eAgMnPc1d3	nejslabší
červení	červený	k2eAgMnPc1d1	červený
veleobři	veleobr	k1gMnPc1	veleobr
mají	mít	k5eAaImIp3nP	mít
absolutní	absolutní	k2eAgFnSc4d1	absolutní
magnitudu	magnituda	k1gFnSc4	magnituda
okolo	okolo	k7c2	okolo
-3	-3	k4	-3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Proměnnost	proměnnost	k1gFnSc4	proměnnost
===	===	k?	===
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
většina	většina	k1gFnSc1	většina
veleobrů	veleobr	k1gMnPc2	veleobr
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
určitý	určitý	k2eAgInSc4d1	určitý
stupeň	stupeň	k1gInSc4	stupeň
fotometrické	fotometrický	k2eAgFnSc2d1	fotometrická
variability	variabilita	k1gFnSc2	variabilita
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
hvězda	hvězda	k1gFnSc1	hvězda
typu	typ	k1gInSc2	typ
Alfa	alfa	k1gNnSc2	alfa
Cygni	Cygen	k2eAgMnPc1d1	Cygen
<g/>
,	,	kIx,	,
polopravidelná	polopravidelný	k2eAgFnSc1d1	polopravidelná
proměnná	proměnný	k2eAgFnSc1d1	proměnná
hvězda	hvězda	k1gFnSc1	hvězda
a	a	k8xC	a
nepravidelná	pravidelný	k2eNgFnSc1d1	nepravidelná
proměnná	proměnný	k2eAgFnSc1d1	proměnná
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
určité	určitý	k2eAgFnPc1d1	určitá
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
definované	definovaný	k2eAgInPc1d1	definovaný
typy	typ	k1gInPc1	typ
proměnných	proměnný	k2eAgFnPc2d1	proměnná
hvězd	hvězda	k1gFnPc2	hvězda
i	i	k9	i
mezi	mezi	k7c7	mezi
veleobry	veleobr	k1gMnPc7	veleobr
<g/>
.	.	kIx.	.
</s>
<s>
Pás	pás	k1gInSc1	pás
nestability	nestabilita	k1gFnSc2	nestabilita
protíná	protínat	k5eAaImIp3nS	protínat
i	i	k9	i
oblast	oblast	k1gFnSc1	oblast
veleobrů	veleobr	k1gMnPc2	veleobr
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mnoho	mnoho	k4c1	mnoho
žlutých	žlutý	k2eAgMnPc2d1	žlutý
veleobrů	veleobr	k1gMnPc2	veleobr
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
proměnných	proměnný	k2eAgFnPc2d1	proměnná
Cepheid	Cepheida	k1gFnPc2	Cepheida
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
ještě	ještě	k6eAd1	ještě
zářivější	zářivý	k2eAgFnPc4d2	zářivější
žluté	žlutý	k2eAgFnPc4d1	žlutá
hyperobry	hyperobra	k1gFnPc4	hyperobra
<g/>
,	,	kIx,	,
extrémně	extrémně	k6eAd1	extrémně
vzácné	vzácný	k2eAgFnPc4d1	vzácná
hvězdy	hvězda	k1gFnPc4	hvězda
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
krátkou	krátký	k2eAgFnSc7d1	krátká
dobou	doba	k1gFnSc7	doba
životnosti	životnost	k1gFnSc2	životnost
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
proměnných	proměnný	k2eAgFnPc2d1	proměnná
hvězd	hvězda	k1gFnPc2	hvězda
R	R	kA	R
Coronae	Coronae	k1gFnSc1	Coronae
Borealis	Borealis	k1gFnSc1	Borealis
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ne	ne	k9	ne
všechny	všechen	k3xTgInPc4	všechen
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
žlutými	žlutý	k2eAgMnPc7d1	žlutý
veleobry	veleobr	k1gMnPc7	veleobr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
proměnnost	proměnnost	k1gFnSc1	proměnnost
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
neobvyklým	obvyklý	k2eNgNnSc7d1	neobvyklé
chemickým	chemický	k2eAgNnSc7d1	chemické
složením	složení	k1gNnSc7	složení
<g/>
,	,	kIx,	,
spíš	spát	k5eAaImIp2nS	spát
než	než	k8xS	než
fyzikální	fyzikální	k2eAgFnSc7d1	fyzikální
nestabilitou	nestabilita	k1gFnSc7	nestabilita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
další	další	k2eAgInPc1d1	další
typy	typ	k1gInPc1	typ
proměnných	proměnný	k2eAgFnPc2d1	proměnná
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
RV	RV	kA	RV
Tauri	Tauri	k1gNnSc1	Tauri
nebo	nebo	k8xC	nebo
PV	PV	kA	PV
Telescopii	Telescopie	k1gFnSc4	Telescopie
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
popisovány	popisován	k2eAgFnPc1d1	popisována
jako	jako	k8xC	jako
veleobři	veleobr	k1gMnPc1	veleobr
<g/>
.	.	kIx.	.
</s>
<s>
Proměnným	proměnný	k2eAgFnPc3d1	proměnná
hvězdám	hvězda	k1gFnPc3	hvězda
typu	typ	k1gInSc2	typ
RV	RV	kA	RV
Tauri	Taur	k1gFnPc1	Taur
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
přiřazeny	přiřazen	k2eAgFnPc1d1	přiřazena
spektrální	spektrální	k2eAgFnPc1d1	spektrální
charakteristiky	charakteristika	k1gFnPc1	charakteristika
zářivých	zářivý	k2eAgMnPc2d1	zářivý
veleobrů	veleobr	k1gMnPc2	veleobr
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jejich	jejich	k3xOp3gFnSc2	jejich
nízké	nízký	k2eAgFnSc2d1	nízká
povrchové	povrchový	k2eAgFnSc2d1	povrchová
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
;	;	kIx,	;
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvíce	nejvíce	k6eAd1	nejvíce
zářivé	zářivý	k2eAgFnPc4d1	zářivá
hvězdy	hvězda	k1gFnPc4	hvězda
AGB	AGB	kA	AGB
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
podobnou	podobný	k2eAgFnSc4d1	podobná
Slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
vzácnější	vzácný	k2eAgFnPc1d2	vzácnější
proměnné	proměnný	k2eAgFnPc1d1	proměnná
hvězdy	hvězda	k1gFnPc1	hvězda
typu	typ	k1gInSc2	typ
PV	PV	kA	PV
Telescopii	Telescopie	k1gFnSc4	Telescopie
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
často	často	k6eAd1	často
řazeny	řadit	k5eAaImNgFnP	řadit
mezi	mezi	k7c7	mezi
veleobry	veleobr	k1gMnPc7	veleobr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
nižší	nízký	k2eAgInSc4d2	nižší
zářivý	zářivý	k2eAgInSc4d1	zářivý
výkon	výkon	k1gInSc4	výkon
než	než	k8xS	než
veleobři	veleobr	k1gMnPc1	veleobr
a	a	k8xC	a
unikátní	unikátní	k2eAgNnSc1d1	unikátní
B	B	kA	B
<g/>
[	[	kIx(	[
<g/>
e	e	k0	e
<g/>
]	]	kIx)	]
spektrum	spektrum	k1gNnSc4	spektrum
s	s	k7c7	s
extrémním	extrémní	k2eAgInSc7d1	extrémní
deficitem	deficit	k1gInSc7	deficit
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LBV	LBV	kA	LBV
jsou	být	k5eAaImIp3nP	být
proměnné	proměnná	k1gFnPc1	proměnná
s	s	k7c7	s
četnými	četný	k2eAgFnPc7d1	četná
polopravidelnými	polopravidelný	k2eAgFnPc7d1	polopravidelná
periodami	perioda	k1gFnPc7	perioda
<g/>
,	,	kIx,	,
špatně	špatně	k6eAd1	špatně
předpovídatelnými	předpovídatelný	k2eAgFnPc7d1	předpovídatelná
erupcemi	erupce	k1gFnPc7	erupce
a	a	k8xC	a
obřími	obří	k2eAgInPc7d1	obří
výbuchy	výbuch	k1gInPc7	výbuch
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
veleobry	veleobr	k1gMnPc4	veleobr
nebo	nebo	k8xC	nebo
hyperobry	hyperobr	k1gMnPc4	hyperobr
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
s	s	k7c7	s
charakteristikami	charakteristika	k1gFnPc7	charakteristika
Wolf-Rayetových	Wolf-Rayetový	k2eAgFnPc2d1	Wolf-Rayetový
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
extrémně	extrémně	k6eAd1	extrémně
zářivé	zářivý	k2eAgFnPc1d1	zářivá
<g/>
,	,	kIx,	,
masivní	masivní	k2eAgFnPc1d1	masivní
<g/>
,	,	kIx,	,
vyvinuté	vyvinutý	k2eAgFnPc1d1	vyvinutá
hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
expandovanými	expandovaný	k2eAgFnPc7d1	expandovaná
vnějšími	vnější	k2eAgFnPc7d1	vnější
vrstvami	vrstva	k1gFnPc7	vrstva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
výrazné	výrazný	k2eAgInPc4d1	výrazný
a	a	k8xC	a
neobvyklé	obvyklý	k2eNgInPc4d1	neobvyklý
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
kategorie	kategorie	k1gFnPc4	kategorie
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
byly	být	k5eAaImAgInP	být
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xS	jako
veleobři	veleobr	k1gMnPc1	veleobr
daného	daný	k2eAgInSc2d1	daný
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
spektrální	spektrální	k2eAgInSc1d1	spektrální
typ	typ	k1gInSc1	typ
označuje	označovat	k5eAaImIp3nS	označovat
prostě	prostě	k9	prostě
"	"	kIx"	"
<g/>
LBV	LBV	kA	LBV
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mají	mít	k5eAaImIp3nP	mít
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
proměnlivé	proměnlivý	k2eAgFnPc1d1	proměnlivá
spektrální	spektrální	k2eAgFnPc1d1	spektrální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
,	,	kIx,	,
s	s	k7c7	s
teplotami	teplota	k1gFnPc7	teplota
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
8000	[number]	k4	8000
až	až	k9	až
20	[number]	k4	20
000	[number]	k4	000
stupňů	stupeň	k1gInPc2	stupeň
Kelvina	Kelvin	k2eAgFnSc1d1	Kelvina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chemické	chemický	k2eAgNnSc4d1	chemické
složení	složení	k1gNnSc4	složení
===	===	k?	===
</s>
</p>
<p>
<s>
Hojnost	hojnost	k1gFnSc1	hojnost
různých	různý	k2eAgInPc2d1	různý
prvků	prvek	k1gInPc2	prvek
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
veleobrů	veleobr	k1gMnPc2	veleobr
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
méně	málo	k6eAd2	málo
svítivých	svítivý	k2eAgFnPc2d1	svítivá
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
veleobři	veleobr	k1gMnPc1	veleobr
jako	jako	k8xC	jako
vyvinuté	vyvinutý	k2eAgFnPc1d1	vyvinutá
hvězdy	hvězda	k1gFnPc1	hvězda
mohou	moct	k5eAaImIp3nP	moct
produkovat	produkovat	k5eAaImF	produkovat
těžší	těžký	k2eAgInPc4d2	těžší
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chladní	chladný	k2eAgMnPc1d1	chladný
veleobři	veleobr	k1gMnPc1	veleobr
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
zvýšené	zvýšený	k2eAgFnPc4d1	zvýšená
hodnoty	hodnota	k1gFnPc4	hodnota
hélia	hélium	k1gNnSc2	hélium
a	a	k8xC	a
dusíku	dusík	k1gInSc2	dusík
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
konvekce	konvekce	k1gFnSc2	konvekce
těchto	tento	k3xDgInPc2	tento
produktů	produkt	k1gInPc2	produkt
jaderné	jaderný	k2eAgFnSc2d1	jaderná
fúze	fúze	k1gFnSc2	fúze
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
<g/>
,	,	kIx,	,
když	když	k8xS	když
velmi	velmi	k6eAd1	velmi
hmotná	hmotný	k2eAgFnSc1d1	hmotná
hvězda	hvězda	k1gFnSc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
spaluje	spalovat	k5eAaImIp3nS	spalovat
vodík	vodík	k1gInSc1	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Helium	helium	k1gNnSc1	helium
se	se	k3xPyFc4	se
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
i	i	k8xC	i
obálce	obálka	k1gFnSc6	obálka
fúzí	fúze	k1gFnPc2	fúze
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
dusík	dusík	k1gInSc1	dusík
vzniká	vznikat	k5eAaImIp3nS	vznikat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
uhlíkem	uhlík	k1gInSc7	uhlík
a	a	k8xC	a
kyslíkem	kyslík	k1gInSc7	kyslík
při	při	k7c6	při
CNO	CNO	kA	CNO
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jsou	být	k5eAaImIp3nP	být
uhlík	uhlík	k1gInSc4	uhlík
a	a	k8xC	a
kyslík	kyslík	k1gInSc4	kyslík
zredukovány	zredukován	k2eAgInPc1d1	zredukován
<g/>
.	.	kIx.	.
</s>
<s>
Červení	červený	k2eAgMnPc1d1	červený
veleobři	veleobr	k1gMnPc1	veleobr
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
zářivých	zářivý	k2eAgInPc2d1	zářivý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
méně	málo	k6eAd2	málo
hmotných	hmotný	k2eAgFnPc2d1	hmotná
hvězd	hvězda	k1gFnPc2	hvězda
AGB	AGB	kA	AGB
neobvyklým	obvyklý	k2eNgNnSc7d1	neobvyklé
chemickým	chemický	k2eAgNnSc7d1	chemické
složením	složení	k1gNnSc7	složení
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
totiž	totiž	k9	totiž
navíc	navíc	k6eAd1	navíc
uhlík	uhlík	k1gInSc4	uhlík
a	a	k8xC	a
lithium	lithium	k1gNnSc4	lithium
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
AGB	AGB	kA	AGB
v	v	k7c6	v
pozdních	pozdní	k2eAgFnPc6d1	pozdní
fázích	fáze	k1gFnPc6	fáze
vývoje	vývoj	k1gInSc2	vývoj
zase	zase	k9	zase
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vysoce	vysoce	k6eAd1	vysoce
obohacený	obohacený	k2eAgInSc4d1	obohacený
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
produkují	produkovat	k5eAaImIp3nP	produkovat
hydroxidy	hydroxid	k1gInPc4	hydroxid
<g/>
.	.	kIx.	.
<g/>
Žhavější	žhavý	k2eAgMnPc1d2	žhavější
veleobři	veleobr	k1gMnPc1	veleobr
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
rozdílné	rozdílný	k2eAgFnPc4d1	rozdílná
úrovně	úroveň	k1gFnPc4	úroveň
obohaceného	obohacený	k2eAgInSc2d1	obohacený
dusíku	dusík	k1gInSc2	dusík
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
důsledkem	důsledek	k1gInSc7	důsledek
různých	různý	k2eAgInPc2d1	různý
mixů	mix	k1gInPc2	mix
vlastností	vlastnost	k1gFnPc2	vlastnost
původních	původní	k2eAgFnPc2d1	původní
hvězd	hvězda	k1gFnPc2	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
rotací	rotace	k1gFnPc2	rotace
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
někteří	některý	k3yIgMnPc1	některý
modří	modrý	k2eAgMnPc1d1	modrý
veleobři	veleobr	k1gMnPc1	veleobr
jsou	být	k5eAaImIp3nP	být
čerstvě	čerstvě	k6eAd1	čerstvě
vyvinutí	vyvinutí	k1gNnSc4	vyvinutí
z	z	k7c2	z
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiní	jiný	k1gMnPc1	jiný
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
prošli	projít	k5eAaPmAgMnP	projít
fází	fáze	k1gFnSc7	fáze
červeného	červený	k2eAgMnSc4d1	červený
veleobra	veleobr	k1gMnSc4	veleobr
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc4	hvězda
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
z	z	k7c2	z
červeného	červený	k2eAgMnSc2d1	červený
obra	obr	k1gMnSc2	obr
mají	mít	k5eAaImIp3nP	mít
obecně	obecně	k6eAd1	obecně
vyšší	vysoký	k2eAgFnSc2d2	vyšší
úrovně	úroveň	k1gFnSc2	úroveň
dusíku	dusík	k1gInSc2	dusík
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
uhlíkem	uhlík	k1gInSc7	uhlík
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
konvekce	konvekce	k1gFnSc2	konvekce
materiálu	materiál	k1gInSc2	materiál
prošlého	prošlý	k2eAgInSc2d1	prošlý
cyklem	cyklus	k1gInSc7	cyklus
CNO	CNO	kA	CNO
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
úplně	úplně	k6eAd1	úplně
ztratili	ztratit	k5eAaPmAgMnP	ztratit
vnější	vnější	k2eAgFnSc4d1	vnější
obálku	obálka	k1gFnSc4	obálka
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgMnSc1d1	typický
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
povrch	povrch	k6eAd1wR	povrch
obohacený	obohacený	k2eAgInSc1d1	obohacený
héliem	hélium	k1gNnSc7	hélium
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třetinu	třetina	k1gFnSc4	třetina
jejich	jejich	k3xOp3gFnSc2	jejich
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
veleobrů	veleobr	k1gMnPc2	veleobr
==	==	k?	==
</s>
</p>
<p>
<s>
Veleobři	veleobr	k1gMnPc1	veleobr
jsou	být	k5eAaImIp3nP	být
vzácné	vzácný	k2eAgFnPc4d1	vzácná
hvězdy	hvězda	k1gFnPc4	hvězda
s	s	k7c7	s
krátkou	krátký	k2eAgFnSc7d1	krátká
životností	životnost	k1gFnSc7	životnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
jejich	jejich	k3xOp3gFnSc3	jejich
vysoké	vysoký	k2eAgFnSc3d1	vysoká
zářivosti	zářivost	k1gFnSc3	zářivost
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
spatřit	spatřit	k5eAaPmF	spatřit
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
prostým	prostý	k2eAgNnSc7d1	prosté
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Rigel	Rigel	k1gInSc1	Rigel
je	být	k5eAaImIp3nS	být
nejjasnější	jasný	k2eAgFnSc7d3	nejjasnější
hvězdou	hvězda	k1gFnSc7	hvězda
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Orion	orion	k1gInSc4	orion
a	a	k8xC	a
typický	typický	k2eAgMnSc1d1	typický
modrý	modrý	k2eAgMnSc1d1	modrý
veleobr	veleobr	k1gMnSc1	veleobr
<g/>
,	,	kIx,	,
Deneb	Denba	k1gFnPc2	Denba
je	být	k5eAaImIp3nS	být
nejjasnější	jasný	k2eAgFnSc7d3	nejjasnější
hvězdou	hvězda	k1gFnSc7	hvězda
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Labuť	labuť	k1gFnSc1	labuť
a	a	k8xC	a
bílý	bílý	k1gMnSc1	bílý
veleobr	veleobr	k1gMnSc1	veleobr
<g/>
,	,	kIx,	,
Delta	delta	k1gFnSc1	delta
Cephei	Cephe	k1gFnSc2	Cephe
je	být	k5eAaImIp3nS	být
klasickým	klasický	k2eAgInSc7d1	klasický
prototypem	prototyp	k1gInSc7	prototyp
proměnné	proměnný	k2eAgFnSc2d1	proměnná
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
žlutý	žlutý	k2eAgMnSc1d1	žlutý
veleobr	veleobr	k1gMnSc1	veleobr
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Betelgeuse	Betelgeuse	k1gFnSc1	Betelgeuse
<g/>
,	,	kIx,	,
Antares	Antares	k1gInSc1	Antares
a	a	k8xC	a
UY	UY	kA	UY
Scuti	Scut	k1gMnPc1	Scut
jsou	být	k5eAaImIp3nP	být
červení	červený	k2eAgMnPc1d1	červený
veleobři	veleobr	k1gMnPc1	veleobr
<g/>
.	.	kIx.	.
</s>
<s>
Mý	Mý	k?	Mý
Cephei	Cephei	k1gNnSc1	Cephei
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejčervenějších	červený	k2eAgFnPc2d3	nejčervenější
hvězd	hvězda	k1gFnPc2	hvězda
viditelných	viditelný	k2eAgInPc2d1	viditelný
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
Galaxii	galaxie	k1gFnSc6	galaxie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Fyzika	fyzika	k1gFnSc1	fyzika
</s>
</p>
<p>
<s>
Astrofyzika	astrofyzika	k1gFnSc1	astrofyzika
</s>
</p>
<p>
<s>
Vesmír	vesmír	k1gInSc1	vesmír
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Supergiant	Supergiant	k1gInSc1	Supergiant
star	star	k1gFnSc2	star
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
