<s>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
</s>
<s>
FC	FC	kA
BarcelonaNázev	BarcelonaNázev	k1gFnSc1
</s>
<s>
Futbol	Futbol	k1gInSc1
Club	club	k1gInSc1
Barcelona	Barcelona	k1gFnSc1
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
Blaugranas	Blaugranas	k1gMnSc1
<g/>
,	,	kIx,
Barça	Barça	k1gMnSc1
Země	zem	k1gFnSc2
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
Město	město	k1gNnSc1
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
Založen	založen	k2eAgInSc1d1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1899	#num#	k4
Asociace	asociace	k1gFnSc2
</s>
<s>
RFEF	RFEF	kA
Barvy	barva	k1gFnPc1
</s>
<s>
modrá	modrý	k2eAgFnSc1d1
a	a	k8xC
granátová	granátový	k2eAgFnSc1d1
(	(	kIx(
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_fcbarcelona	_fcbarcelona	k1gFnSc1
<g/>
2021	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Domácí	domácí	k2eAgInSc1d1
dres	dres	k1gInSc1
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_fcbarcelona	_fcbarcelona	k1gFnSc1
<g/>
2021	#num#	k4
<g/>
a	a	k8xC
<g/>
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Venkovní	venkovní	k2eAgInSc4d1
dres	dres	k1gInSc4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_fcbarcelona	_fcbarcelona	k1gFnSc1
<g/>
2021	#num#	k4
<g/>
t	t	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Alternativní	alternativní	k2eAgInPc1d1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
španělská	španělský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
Stadion	stadion	k1gNnSc1
</s>
<s>
Camp	camp	k1gInSc1
Nou	Nou	k1gFnSc2
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
41	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
51	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
21	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Kapacita	kapacita	k1gFnSc1
</s>
<s>
99	#num#	k4
354	#num#	k4
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Vedení	vedení	k1gNnSc1
Vlastník	vlastník	k1gMnSc1
</s>
<s>
associate	associat	k1gMnSc5
membersupporters	membersupportersit	k5eAaPmRp2nS
<g/>
'	'	kIx"
group	group	k1gMnSc1
Předseda	předseda	k1gMnSc1
</s>
<s>
Joan	Joan	k1gInSc1
Laporta	Laporta	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
2021	#num#	k4
<g/>
)	)	kIx)
Trenér	trenér	k1gMnSc1
</s>
<s>
Ronald	Ronald	k1gMnSc1
Koeman	Koeman	k1gMnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Největší	veliký	k2eAgInPc1d3
úspěchy	úspěch	k1gInPc1
Ligové	ligový	k2eAgInPc1d1
tituly	titul	k1gInPc1
</s>
<s>
26	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
Španělska	Španělsko	k1gNnSc2
(	(	kIx(
<g/>
1929	#num#	k4
<g/>
,	,	kIx,
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
<g/>
,	,	kIx,
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
<g/>
,	,	kIx,
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
<g/>
,	,	kIx,
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
Domácí	domácí	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
</s>
<s>
30	#num#	k4
<g/>
×	×	k?
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
2	#num#	k4
<g/>
×	×	k?
Copa	Cop	k1gInSc2
de	de	k?
la	la	k1gNnSc2
Liga	liga	k1gFnSc1
<g/>
13	#num#	k4
<g/>
×	×	k?
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gNnSc2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
</s>
<s>
5	#num#	k4
<g/>
×	×	k?
PMEZ	PMEZ	kA
/	/	kIx~
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
<g/>
3	#num#	k4
<g/>
×	×	k?
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
<g/>
4	#num#	k4
<g/>
×	×	k?
Pohár	pohár	k1gInSc4
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
5	#num#	k4
<g/>
×	×	k?
Superpohár	superpohár	k1gInSc4
UEFA	UEFA	kA
<g/>
3	#num#	k4
<g/>
×	×	k?
MS	MS	kA
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
<g/>
2	#num#	k4
<g/>
×	×	k?
Latinský	latinský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
(	(	kIx(
<g/>
celým	celý	k2eAgInSc7d1
názvem	název	k1gInSc7
<g/>
:	:	kIx,
Futbol	Futbol	k1gInSc1
Club	club	k1gInSc1
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
Barcelona	Barcelona	k1gFnSc1
či	či	k8xC
Barça	Barça	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
profesionální	profesionální	k2eAgInSc4d1
španělský	španělský	k2eAgInSc4d1
fotbalový	fotbalový	k2eAgInSc4d1
klub	klub	k1gInSc4
sídlící	sídlící	k2eAgInSc4d1
ve	v	k7c6
městě	město	k1gNnSc6
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
autonomním	autonomní	k2eAgNnSc6d1
společenství	společenství	k1gNnSc6
Katalánsko	Katalánsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
sezóny	sezóna	k1gFnSc2
1929	#num#	k4
hraje	hrát	k5eAaImIp3nS
v	v	k7c6
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
<g/>
,	,	kIx,
španělské	španělský	k2eAgFnSc6d1
nejvyšší	vysoký	k2eAgFnSc6d3
fotbalové	fotbalový	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domácím	domácí	k2eAgInSc7d1
fotbalovým	fotbalový	k2eAgInSc7d1
stadionem	stadion	k1gInSc7
je	být	k5eAaImIp3nS
Nou	Nou	k1gMnSc1
Estadi	Estad	k1gMnPc1
del	del	k?
Futbol	Futbol	k1gInSc1
Club	club	k1gInSc1
Barcelona	Barcelona	k1gFnSc1
známý	známý	k1gMnSc1
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Camp	camp	k1gInSc1
Nou	Nou	k1gFnPc4
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
téměř	téměř	k6eAd1
100	#num#	k4
tisíc	tisíc	k4xCgInSc4
diváků	divák	k1gMnPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klubové	klubový	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
jsou	být	k5eAaImIp3nP
modrá	modrý	k2eAgNnPc1d1
a	a	k8xC
granátová	granátový	k2eAgNnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
kontě	konto	k1gNnSc6
mnohé	mnohý	k2eAgInPc4d1
úspěchy	úspěch	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
je	být	k5eAaImIp3nS
šestadvacetinásobným	šestadvacetinásobný	k2eAgMnSc7d1
mistrem	mistr	k1gMnSc7
Španělska	Španělsko	k1gNnSc2
a	a	k8xC
pětinásobným	pětinásobný	k2eAgMnSc7d1
vítězem	vítěz	k1gMnSc7
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
(	(	kIx(
<g/>
v	v	k7c6
letech	léto	k1gNnPc6
1992	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíckrát	Nejvíckrát	k1gFnPc2
vyhrála	vyhrát	k5eAaPmAgFnS
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
(	(	kIx(
<g/>
30	#num#	k4
<g/>
×	×	k?
<g/>
)	)	kIx)
a	a	k8xC
španělský	španělský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
Supercopa	Supercop	k1gMnSc2
de	de	k?
Españ	Españ	k1gMnSc2
(	(	kIx(
<g/>
13	#num#	k4
<g/>
×	×	k?
<g/>
,	,	kIx,
naposledy	naposledy	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
a	a	k8xC
2011	#num#	k4
získala	získat	k5eAaPmAgFnS
trofej	trofej	k1gFnSc1
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
a	a	k8xC
mnohé	mnohé	k1gNnSc1
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kromě	kromě	k7c2
fotbalového	fotbalový	k2eAgNnSc2d1
jsou	být	k5eAaImIp3nP
v	v	k7c6
klubu	klub	k1gInSc6
i	i	k9
další	další	k2eAgInPc4d1
týmy	tým	k1gInPc4
basketbalový	basketbalový	k2eAgInSc1d1
<g/>
,	,	kIx,
házenkářský	házenkářský	k2eAgInSc1d1
<g/>
,	,	kIx,
futsalový	futsalový	k2eAgMnSc1d1
a	a	k8xC
hokeje	hokej	k1gInPc1
na	na	k7c6
kolečkových	kolečkový	k2eAgFnPc6d1
bruslích	brusle	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
svého	svůj	k3xOyFgNnSc2
založení	založení	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1899	#num#	k4
má	mít	k5eAaImIp3nS
klub	klub	k1gInSc1
heslo	heslo	k1gNnSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Més	Més	k1gFnSc1
que	que	k?
un	un	k?
club	club	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
v	v	k7c6
překladu	překlad	k1gInSc6
z	z	k7c2
katalánštiny	katalánština	k1gFnSc2
<g/>
:	:	kIx,
Víc	hodně	k6eAd2
než	než	k8xS
klub	klub	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Sestava	sestava	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1910	#num#	k4
</s>
<s>
Football	Football	k1gInSc1
Club	club	k1gInSc1
Barcelona	Barcelona	k1gFnSc1
založil	založit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1899	#num#	k4
Joan	Joan	k1gMnSc1
(	(	kIx(
<g/>
Hans	Hans	k1gMnSc1
<g/>
)	)	kIx)
Gamper	Gamper	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gamper	Gamper	k1gMnSc1
se	se	k3xPyFc4
do	do	k7c2
Barcelony	Barcelona	k1gFnSc2
přestěhoval	přestěhovat	k5eAaPmAgMnS
kvůli	kvůli	k7c3
svým	svůj	k3xOyFgFnPc3
podnikatelským	podnikatelský	k2eAgFnPc3d1
aktivitám	aktivita	k1gFnPc3
v	v	k7c6
roce	rok	k1gInSc6
1898	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
21	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švýcarský	švýcarský	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
zvolil	zvolit	k5eAaPmAgMnS
za	za	k7c2
klubové	klubový	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
červenou	červený	k2eAgFnSc7d1
a	a	k8xC
modrou	modrý	k2eAgFnSc7d1
<g/>
,	,	kIx,
inspirován	inspirovat	k5eAaBmNgInS
svým	svůj	k3xOyFgInSc7
oblíbeným	oblíbený	k2eAgInSc7d1
švýcarským	švýcarský	k2eAgInSc7d1
fotbalovým	fotbalový	k2eAgInSc7d1
klubem	klub	k1gInSc7
FC	FC	kA
Basel	Basel	k1gInSc4
1893	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgMnPc7d1
spoluzakladateli	spoluzakladatel	k1gMnPc7
byli	být	k5eAaImAgMnP
Walter	Walter	k1gMnSc1
(	(	kIx(
<g/>
Gualteri	Gualteri	k1gNnSc1
<g/>
)	)	kIx)
Wild	Wild	k1gInSc1
<g/>
,	,	kIx,
Lluis	Lluis	k1gInSc1
d	d	k?
<g/>
’	’	k?
<g/>
Ossó	Ossó	k1gFnSc2
<g/>
,	,	kIx,
přítomni	přítomen	k2eAgMnPc1d1
byli	být	k5eAaImAgMnP
také	také	k9
Bartomeu	Bartomea	k1gFnSc4
Terrados	Terradosa	k1gFnPc2
<g/>
,	,	kIx,
Otto	Otto	k1gMnSc1
Kunzle	Kunzle	k1gMnSc1
<g/>
,	,	kIx,
Otto	Otto	k1gMnSc1
Maier	Maier	k1gMnSc1
<g/>
,	,	kIx,
Enric	Enric	k1gMnSc1
Ducal	ducat	k5eAaImAgMnS
<g/>
,	,	kIx,
Pere	prát	k5eAaImIp3nS
Cabot	Cabot	k1gMnSc1
<g/>
,	,	kIx,
Josep	Josep	k1gMnSc1
Llobet	Llobet	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Parsons	Parsons	k1gInSc1
a	a	k8xC
William	William	k1gInSc1
Parsons	Parsonsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Walter	Walter	k1gMnSc1
Wild	Wild	k1gMnSc1
<g/>
,	,	kIx,
rodák	rodák	k1gMnSc1
z	z	k7c2
Anglie	Anglie	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
prezidentem	prezident	k1gMnSc7
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
jeho	jeho	k3xOp3gFnPc7
hráčskými	hráčský	k2eAgFnPc7d1
schopnostmi	schopnost	k1gFnPc7
ho	on	k3xPp3gInSc2
můžeme	moct	k5eAaImIp1nP
považovat	považovat	k5eAaImF
za	za	k7c4
první	první	k4xOgFnSc4
klubovou	klubový	k2eAgFnSc4d1
legendu	legenda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc1
dres	dres	k1gInSc1
klubu	klub	k1gInSc2
byl	být	k5eAaImAgInS
napůl	napůl	k6eAd1
sešitý	sešitý	k2eAgMnSc1d1
z	z	k7c2
barev	barva	k1gFnPc2
purpurové	purpurový	k2eAgFnSc2d1
a	a	k8xC
modré	modrý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trenýrky	trenýrky	k1gFnPc1
byly	být	k5eAaImAgFnP
bílé	bílý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
existence	existence	k1gFnSc2
používala	používat	k5eAaImAgFnS
Barcelona	Barcelona	k1gFnSc1
jako	jako	k9
svůj	svůj	k3xOyFgInSc4
znak	znak	k1gInSc4
oficiální	oficiální	k2eAgInSc1d1
městský	městský	k2eAgInSc1d1
znak	znak	k1gInSc1
Barcelony	Barcelona	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
demonstrovala	demonstrovat	k5eAaBmAgFnS
své	svůj	k3xOyFgNnSc4
spojení	spojení	k1gNnSc4
s	s	k7c7
městem	město	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
usoudilo	usoudit	k5eAaPmAgNnS
vedení	vedení	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
klub	klub	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
vlastní	vlastní	k2eAgInSc1d1
znak	znak	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
vypsalo	vypsat	k5eAaPmAgNnS
výběrovou	výběrový	k2eAgFnSc4d1
soutěž	soutěž	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tu	tu	k6eAd1
vyhrálo	vyhrát	k5eAaPmAgNnS
logo	logo	k1gNnSc1
anonymního	anonymní	k2eAgMnSc2d1
přispěvatele	přispěvatel	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
s	s	k7c7
malými	malý	k2eAgFnPc7d1
úpravami	úprava	k1gFnPc7
zachovalo	zachovat	k5eAaPmAgNnS
dodnes	dodnes	k6eAd1
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
kapitolu	kapitola	k1gFnSc4
Znak	znak	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historicky	historicky	k6eAd1
první	první	k4xOgNnSc4
utkání	utkání	k1gNnSc4
sehrála	sehrát	k5eAaPmAgFnS
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
proti	proti	k7c3
skupině	skupina	k1gFnSc3
anglických	anglický	k2eAgMnPc2d1
přistěhovalců	přistěhovalec	k1gMnPc2
<g/>
,	,	kIx,
prohrála	prohrát	k5eAaPmAgFnS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1909	#num#	k4
získal	získat	k5eAaPmAgInS
klub	klub	k1gInSc1
první	první	k4xOgNnSc4
oficiální	oficiální	k2eAgNnSc4d1
hřiště	hřiště	k1gNnSc4
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
6000	#num#	k4
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
počáteční	počáteční	k2eAgFnSc6d1
éře	éra	k1gFnSc6
vyhrál	vyhrát	k5eAaPmAgInS
klub	klub	k1gInSc1
několikrát	několikrát	k6eAd1
katalánský	katalánský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
první	první	k4xOgFnPc1
v	v	k7c6
roce	rok	k1gInSc6
1901	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
čtyřikrát	čtyřikrát	k6eAd1
dokázal	dokázat	k5eAaPmAgMnS
zvítězit	zvítězit	k5eAaPmF
ve	v	k7c6
španělském	španělský	k2eAgNnSc6d1
mistrovství	mistrovství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc4
trofej	trofej	k1gFnSc4
získal	získat	k5eAaPmAgMnS
roku	rok	k1gInSc2
1901	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vyhrál	vyhrát	k5eAaPmAgMnS
Copa	Copa	k1gMnSc1
Macaya	Macaya	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
hrál	hrát	k5eAaImAgMnS
hned	hned	k6eAd1
v	v	k7c6
prvním	první	k4xOgInSc6
ročníku	ročník	k1gInSc6
finále	finále	k1gNnSc2
Copa	Copus	k1gMnSc2
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
podlehl	podlehnout	k5eAaPmAgMnS
Clubu	club	k1gInSc3
Vizcaya	Vizcayus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
Les	les	k1gInSc4
Corts	Cortsa	k1gFnPc2
na	na	k7c4
Camp	camp	k1gInSc4
Nou	Nou	k1gFnSc2
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
–	–	k?
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Zlatá	zlatý	k2eAgFnSc1d1
éra	éra	k1gFnSc1
<g/>
“	“	k?
klubu	klub	k1gInSc2
trvala	trvat	k5eAaImAgFnS
od	od	k7c2
roku	rok	k1gInSc2
1919	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1929	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
zůstávala	zůstávat	k5eAaImAgFnS
Barcelona	Barcelona	k1gFnSc1
prakticky	prakticky	k6eAd1
neporažena	porazit	k5eNaPmNgFnS
na	na	k7c6
domácí	domácí	k2eAgFnSc6d1
půdě	půda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1922	#num#	k4
byl	být	k5eAaImAgInS
slavnostně	slavnostně	k6eAd1
otevřen	otevřít	k5eAaPmNgInS
stadion	stadion	k1gInSc1
Les	les	k1gInSc1
Corts	Corts	k1gInSc1
a	a	k8xC
klub	klub	k1gInSc1
na	na	k7c6
něm	on	k3xPp3gNnSc6
zvítězil	zvítězit	k5eAaPmAgMnS
ve	v	k7c6
vůbec	vůbec	k9
prvním	první	k4xOgInSc6
ročníku	ročník	k1gInSc6
La	la	k1gNnSc2
ligy	liga	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stadion	stadion	k1gInSc1
se	se	k3xPyFc4
otevíral	otevírat	k5eAaImAgInS
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
20	#num#	k4
000	#num#	k4
diváků	divák	k1gMnPc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
rozšířena	rozšířit	k5eAaPmNgFnS
na	na	k7c4
60	#num#	k4
000	#num#	k4
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přezdívalo	přezdívat	k5eAaImAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
Katedrála	katedrála	k1gFnSc1
fotbalu	fotbal	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
hrály	hrát	k5eAaImAgInP
v	v	k7c6
klubu	klub	k1gInSc6
hvězdy	hvězda	k1gFnSc2
jako	jako	k8xC,k8xS
Samitier	Samitier	k1gInSc1
<g/>
,	,	kIx,
Alcántara	Alcántara	k1gFnSc1
<g/>
,	,	kIx,
Zamora	Zamora	k1gFnSc1
<g/>
,	,	kIx,
Sagi	Sagi	k1gNnSc1
<g/>
,	,	kIx,
Piera	Piera	k1gFnSc1
či	či	k8xC
Sancho	Sancha	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
však	však	k9
přišly	přijít	k5eAaPmAgInP
těžké	těžký	k2eAgInPc1d1
časy	čas	k1gInPc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
klub	klub	k1gInSc1
přišel	přijít	k5eAaPmAgInS
o	o	k7c4
svou	svůj	k3xOyFgFnSc4
chloubu	chlouba	k1gFnSc4
–	–	k?
stadion	stadion	k1gInSc1
Les	les	k1gInSc1
Corts	Corts	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
obsadil	obsadit	k5eAaPmAgMnS
fašistický	fašistický	k2eAgMnSc1d1
diktátor	diktátor	k1gMnSc1
Primo	primo	k1gNnSc1
de	de	k?
Rivera	River	k1gMnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
si	se	k3xPyFc3
ze	z	k7c2
stadionu	stadion	k1gInSc2
udělal	udělat	k5eAaPmAgInS
tábor	tábor	k1gInSc1
pro	pro	k7c4
občanskou	občanský	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
tak	tak	k6eAd1
projevila	projevit	k5eAaPmAgFnS
zášť	zášť	k1gFnSc4
politické	politický	k2eAgFnSc2d1
moci	moc	k1gFnSc2
vůči	vůči	k7c3
Barceloně	Barcelona	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
důvodem	důvod	k1gInSc7
byla	být	k5eAaImAgFnS
příslušnost	příslušnost	k1gFnSc1
klubu	klub	k1gInSc2
ke	k	k7c3
Katalánsku	Katalánsko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
členů	člen	k1gInPc2
klubu	klub	k1gInSc2
bylo	být	k5eAaImAgNnS
pronásledováno	pronásledovat	k5eAaImNgNnS
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
byli	být	k5eAaImAgMnP
zatčeni	zatknout	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konce	konec	k1gInSc2
největšího	veliký	k2eAgInSc2d3
teroru	teror	k1gInSc2
se	se	k3xPyFc4
členové	člen	k1gMnPc1
klubu	klub	k1gInSc2
dočkali	dočkat	k5eAaPmAgMnP
až	až	k9
po	po	k7c6
konci	konec	k1gInSc6
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
počet	počet	k1gInSc1
členů	člen	k1gMnPc2
klesl	klesnout	k5eAaPmAgInS
o	o	k7c4
75	#num#	k4
%	%	kIx~
a	a	k8xC
zázemí	zázemí	k1gNnSc1
klubu	klub	k1gInSc2
dokonce	dokonce	k9
postihlo	postihnout	k5eAaPmAgNnS
ostřelování	ostřelování	k1gNnSc1
bombami	bomba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Klubu	klub	k1gInSc3
se	se	k3xPyFc4
však	však	k9
podařilo	podařit	k5eAaPmAgNnS
vstát	vstát	k5eAaPmF
z	z	k7c2
popela	popel	k1gInSc2
a	a	k8xC
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
získal	získat	k5eAaPmAgInS
svůj	svůj	k3xOyFgInSc4
druhý	druhý	k4xOgInSc4
titul	titul	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
záhy	záhy	k6eAd1
následován	následovat	k5eAaImNgInS
dalšími	další	k2eAgInPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
členů	člen	k1gMnPc2
se	se	k3xPyFc4
oproti	oproti	k7c3
předválečnému	předválečný	k2eAgInSc3d1
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1957	#num#	k4
měla	mít	k5eAaImAgFnS
Barcelona	Barcelona	k1gFnSc1
na	na	k7c6
kontě	konto	k1gNnSc6
šest	šest	k4xCc4
mistrovských	mistrovský	k2eAgInPc2d1
titulů	titul	k1gInPc2
<g/>
,	,	kIx,
více	hodně	k6eAd2
než	než	k8xS
dvacet	dvacet	k4xCc4
titulů	titul	k1gInPc2
katalánských	katalánský	k2eAgFnPc2d1
a	a	k8xC
13	#num#	k4
vítězství	vítězství	k1gNnSc2
v	v	k7c6
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
přišel	přijít	k5eAaPmAgMnS
do	do	k7c2
týmu	tým	k1gInSc2
Ladislav	Ladislav	k1gMnSc1
Kubala	Kubala	k1gMnSc1
a	a	k8xC
tým	tým	k1gInSc1
i	i	k9
s	s	k7c7
jeho	jeho	k3xOp3gFnSc7
podporou	podpora	k1gFnSc7
získal	získat	k5eAaPmAgMnS
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
první	první	k4xOgInSc4
mistrovský	mistrovský	k2eAgInSc4d1
„	„	k?
<g/>
double	double	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Víc	hodně	k6eAd2
než	než	k8xS
klub	klub	k1gInSc1
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
–	–	k?
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Víc	hodně	k6eAd2
než	než	k8xS
klub	klub	k1gInSc1
<g/>
“	“	k?
je	být	k5eAaImIp3nS
napsáno	napsat	k5eAaPmNgNnS,k5eAaBmNgNnS
na	na	k7c6
tribuně	tribuna	k1gFnSc6
</s>
<s>
Ve	v	k7c6
40	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgFnP
myšlenky	myšlenka	k1gFnPc1
<g/>
,	,	kIx,
že	že	k8xS
stadion	stadion	k1gInSc1
v	v	k7c4
Les	les	k1gInSc4
Corts	Corts	k1gInSc1
nebude	být	k5eNaImBp3nS
dostačující	dostačující	k2eAgInSc1d1
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
tak	tak	k6eAd1
velkého	velký	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
Barcelona	Barcelona	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
tak	tak	k8xC,k8xS
tehdejší	tehdejší	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Francesc	Francesc	k1gFnSc4
Miró-Sans	Miró-Sansa	k1gFnPc2
nechal	nechat	k5eAaPmAgMnS
zahájit	zahájit	k5eAaPmF
stavbu	stavba	k1gFnSc4
nového	nový	k2eAgInSc2d1
stadionu	stadion	k1gInSc2
s	s	k7c7
názvem	název	k1gInSc7
Camp	camp	k1gInSc1
Nou	Nou	k1gFnSc2
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
přes	přes	k7c4
98	#num#	k4
000	#num#	k4
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stadion	stadion	k1gInSc1
byl	být	k5eAaImAgInS
slavnostně	slavnostně	k6eAd1
otevřen	otevřít	k5eAaPmNgInS
24	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1957	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otevření	otevření	k1gNnSc1
účinkovalo	účinkovat	k5eAaImAgNnS
jako	jako	k9
pokropení	pokropení	k1gNnSc1
živou	živý	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
a	a	k8xC
úspěchy	úspěch	k1gInPc1
se	se	k3xPyFc4
dostavily	dostavit	k5eAaPmAgInP
velmi	velmi	k6eAd1
brzy	brzy	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
v	v	k7c6
následujících	následující	k2eAgFnPc6d1
dvou	dva	k4xCgFnPc6
sezónách	sezóna	k1gFnPc6
získal	získat	k5eAaPmAgInS
klub	klub	k1gInSc1
titul	titul	k1gInSc1
a	a	k8xC
na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
vyhrála	vyhrát	k5eAaPmAgFnS
Barcelona	Barcelona	k1gFnSc1
taktéž	taktéž	k?
dvakrát	dvakrát	k6eAd1
po	po	k7c6
sobě	se	k3xPyFc3
<g/>
,	,	kIx,
v	v	k7c6
letech	léto	k1gNnPc6
1958	#num#	k4
a	a	k8xC
1960	#num#	k4
<g/>
,	,	kIx,
Pohár	pohár	k1gInSc1
veletržních	veletržní	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
pak	pak	k6eAd1
vyhrála	vyhrát	k5eAaPmAgFnS
ještě	ještě	k6eAd1
roku	rok	k1gInSc2
1966	#num#	k4
a	a	k8xC
se	s	k7c7
třemi	tři	k4xCgNnPc7
vítězstvími	vítězství	k1gNnPc7
ji	on	k3xPp3gFnSc4
v	v	k7c6
dnes	dnes	k6eAd1
již	již	k6eAd1
neexistující	existující	k2eNgFnSc6d1
soutěži	soutěž	k1gFnSc6
nikdo	nikdo	k3yNnSc1
nepředstihne	předstihnout	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
u	u	k7c2
fanoušků	fanoušek	k1gMnPc2
začala	začít	k5eAaPmAgFnS
stále	stále	k6eAd1
více	hodně	k6eAd2
projevovat	projevovat	k5eAaImF
nejen	nejen	k6eAd1
sportovní	sportovní	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
sociální	sociální	k2eAgFnSc4d1
příslušnost	příslušnost	k1gFnSc4
ke	k	k7c3
klubu	klub	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
národním	národní	k2eAgInSc7d1
symbolem	symbol	k1gInSc7
„	„	k?
<g/>
utlačovaného	utlačovaný	k2eAgMnSc4d1
<g/>
“	“	k?
katalánského	katalánský	k2eAgInSc2d1
lidu	lid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
prohlásil	prohlásit	k5eAaPmAgMnS
tehdejší	tehdejší	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
klubu	klub	k1gInSc2
Narcís	Narcísa	k1gFnPc2
de	de	k?
Carreras	Carreras	k1gMnSc1
při	při	k7c6
svém	svůj	k3xOyFgNnSc6
vřelém	vřelý	k2eAgNnSc6d1
přivítání	přivítání	k1gNnSc6
po	po	k7c6
příchodu	příchod	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
Barca	Barca	k1gFnSc1
je	být	k5eAaImIp3nS
víc	hodně	k6eAd2
než	než	k8xS
jen	jen	k6eAd1
klub	klub	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
přesně	přesně	k6eAd1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
její	její	k3xOp3gFnSc1
spojitost	spojitost	k1gFnSc1
s	s	k7c7
lidmi	člověk	k1gMnPc7
v	v	k7c6
Katalánsku	Katalánsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
heslo	heslo	k1gNnSc1
se	se	k3xPyFc4
ujalo	ujmout	k5eAaPmAgNnS
jako	jako	k9
oficiální	oficiální	k2eAgFnSc4d1
a	a	k8xC
dnes	dnes	k6eAd1
jej	on	k3xPp3gMnSc4
lze	lze	k6eAd1
vidět	vidět	k5eAaImF
na	na	k7c6
hlavní	hlavní	k2eAgFnSc6d1
tribuně	tribuna	k1gFnSc6
stadionu	stadion	k1gInSc2
Camp	camp	k1gInSc4
Nou	Nou	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
sportovní	sportovní	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
se	se	k3xPyFc4
však	však	k9
klub	klub	k1gInSc4
potýkal	potýkat	k5eAaImAgMnS
s	s	k7c7
neúspěchy	neúspěch	k1gInPc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nedokázal	dokázat	k5eNaPmAgMnS
sesadit	sesadit	k5eAaPmF
tehdy	tehdy	k6eAd1
fenomenální	fenomenální	k2eAgInSc4d1
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
z	z	k7c2
mistrovského	mistrovský	k2eAgInSc2d1
trůnu	trůn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dařilo	dařit	k5eAaImAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gNnSc3
alespoň	alespoň	k9
v	v	k7c6
domácím	domácí	k2eAgInSc6d1
poháru	pohár	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
připsal	připsat	k5eAaPmAgInS
dalších	další	k2eAgInPc2d1
pět	pět	k4xCc4
vítězství	vítězství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
byl	být	k5eAaImAgMnS
do	do	k7c2
klubu	klub	k1gInSc2
z	z	k7c2
Ajaxu	Ajax	k1gInSc2
Amsterdam	Amsterdam	k1gInSc4
přiveden	přiveden	k2eAgMnSc1d1
nizozemský	nizozemský	k2eAgMnSc1d1
ostrostřelec	ostrostřelec	k1gMnSc1
Johan	Johan	k1gMnSc1
Cruijff	Cruijff	k1gMnSc1
<g/>
,	,	kIx,
díky	díky	k7c3
němuž	jenž	k3xRgNnSc3
získala	získat	k5eAaPmAgFnS
Barcelona	Barcelona	k1gFnSc1
po	po	k7c6
dlouhých	dlouhý	k2eAgNnPc6d1
čtrnácti	čtrnáct	k4xCc6
letech	léto	k1gNnPc6
znovu	znovu	k6eAd1
mistrovský	mistrovský	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
příchodem	příchod	k1gInSc7
Cruijffa	Cruijff	k1gMnSc2
se	se	k3xPyFc4
změnila	změnit	k5eAaPmAgFnS
hra	hra	k1gFnSc1
klubu	klub	k1gInSc2
k	k	k7c3
nepoznání	nepoznání	k1gNnSc3
<g/>
,	,	kIx,
tým	tým	k1gInSc1
předváděl	předvádět	k5eAaImAgInS
ofenzivní	ofenzivní	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
označován	označovat	k5eAaImNgInS
za	za	k7c4
nejhezčí	hezký	k2eAgInSc4d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
1974	#num#	k4
proběhly	proběhnout	k5eAaPmAgFnP
oslavy	oslava	k1gFnPc1
75	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc3
založení	založení	k1gNnSc2
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
ukázal	ukázat	k5eAaPmAgInS
sociální	sociální	k2eAgInSc1d1
význam	význam	k1gInSc1
Barcelony	Barcelona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
oslavy	oslava	k1gFnPc4
dorazilo	dorazit	k5eAaPmAgNnS
množství	množství	k1gNnSc1
umělců	umělec	k1gMnPc2
<g/>
,	,	kIx,
vznikaly	vznikat	k5eAaImAgInP
chorály	chorál	k1gInPc4
<g/>
,	,	kIx,
plakáty	plakát	k1gInPc4
a	a	k8xC
odehrála	odehrát	k5eAaPmAgFnS
se	se	k3xPyFc4
různá	různý	k2eAgNnPc1d1
setkání	setkání	k1gNnPc1
hráčů	hráč	k1gMnPc2
s	s	k7c7
diváky	divák	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgInS
také	také	k9
oficiální	oficiální	k2eAgInSc1d1
chorál	chorál	k1gInSc1
klubu	klub	k1gInSc2
Cant	canto	k1gNnPc2
del	del	k?
Barça	Barça	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Nová	nový	k2eAgFnSc1d1
strategie	strategie	k1gFnSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
příchodem	příchod	k1gInSc7
nového	nový	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
Josep	Josep	k1gMnSc1
Lluís	Lluísa	k1gFnPc2
Núñ	Núñ	k1gMnSc1
i	i	k9
Clemente	Clement	k1gInSc5
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
strategie	strategie	k1gFnSc1
a	a	k8xC
politika	politika	k1gFnSc1
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Núñ	Núñ	k1gInSc1
přišel	přijít	k5eAaPmAgInS
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1978	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k8xC
po	po	k7c6
odchodu	odchod	k1gInSc6
hvězdy	hvězda	k1gFnSc2
Johana	Johan	k1gMnSc2
Cruijffa	Cruijff	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
nyní	nyní	k6eAd1
stabilizace	stabilizace	k1gFnSc1
klubu	klub	k1gInSc2
a	a	k8xC
práce	práce	k1gFnSc2
s	s	k7c7
vlastními	vlastní	k2eAgMnPc7d1
odchovanci	odchovanec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniká	vznikat	k5eAaImIp3nS
nová	nový	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
členů	člen	k1gMnPc2
klubu	klub	k1gInSc2
se	se	k3xPyFc4
vyšplhal	vyšplhat	k5eAaPmAgMnS
až	až	k9
na	na	k7c4
700	#num#	k4
tisíc	tisíc	k4xCgInSc4
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
sportovním	sportovní	k2eAgNnSc6d1
poli	pole	k1gNnSc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
lehkému	lehký	k2eAgInSc3d1
útlumu	útlum	k1gInSc3
<g/>
,	,	kIx,
zejména	zejména	k9
na	na	k7c6
domácí	domácí	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
klub	klub	k1gInSc1
získal	získat	k5eAaPmAgInS
pouze	pouze	k6eAd1
jeden	jeden	k4xCgInSc4
mistrovský	mistrovský	k2eAgInSc4d1
titul	titul	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
domácím	domácí	k2eAgInSc6d1
poháru	pohár	k1gInSc6
Copa	Copa	k1gMnSc1
del	del	k?
Rey	Rea	k1gFnSc2
však	však	k9
získává	získávat	k5eAaImIp3nS
hned	hned	k6eAd1
pět	pět	k4xCc4
trofejí	trofej	k1gFnPc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
jich	on	k3xPp3gMnPc2
na	na	k7c6
kontě	konto	k1gNnSc6
má	mít	k5eAaImIp3nS
už	už	k9
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
se	se	k3xPyFc4
daří	dařit	k5eAaImIp3nS
o	o	k7c4
mnoho	mnoho	k4c4
lépe	dobře	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
se	se	k3xPyFc4
kvůli	kvůli	k7c3
neúspěchům	neúspěch	k1gInPc3
nedostává	dostávat	k5eNaImIp3nS
do	do	k7c2
Poháru	pohár	k1gInSc6
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
hned	hned	k6eAd1
třikrát	třikrát	k6eAd1
vítězí	vítězit	k5eAaImIp3nS
v	v	k7c6
Poháru	pohár	k1gInSc6
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
strategii	strategie	k1gFnSc6
však	však	k9
Núñ	Núñ	k1gInSc1
jednou	jednou	k6eAd1
poleví	polevit	k5eAaPmIp3nS
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
když	když	k8xS
na	na	k7c4
nátlak	nátlak	k1gInSc4
fanoušků	fanoušek	k1gMnPc2
za	za	k7c4
miliardu	miliarda	k4xCgFnSc4
pessos	pessosa	k1gFnPc2
přivádí	přivádět	k5eAaImIp3nS
v	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
argentinskou	argentinský	k2eAgFnSc4d1
superhvězdu	superhvězda	k1gFnSc4
Diega	Diega	k1gFnSc1
Maradonu	Maradon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
však	však	k9
vydrží	vydržet	k5eAaPmIp3nS
v	v	k7c6
klubu	klub	k1gInSc6
pouze	pouze	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1984	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
odchází	odcházet	k5eAaImIp3nS
do	do	k7c2
italského	italský	k2eAgNnSc2d1
SSC	SSC	kA
Neapol	Neapol	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Dream	Dream	k1gInSc1
Team	team	k1gInSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
José	Josý	k2eAgNnSc1d1
Mari	Mari	k1gNnSc1
Bakero	Bakero	k1gNnSc1
</s>
<s>
Když	když	k8xS
prezident	prezident	k1gMnSc1
Josep	Josep	k1gMnSc1
Bugladin	Bugladin	k2eAgInSc4d1
Núñ	Núñ	k1gInSc4
i	i	k8xC
Clemente	Clement	k1gInSc5
měnil	měnit	k5eAaImAgMnS
sportovní	sportovní	k2eAgFnSc4d1
strategii	strategie	k1gFnSc4
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
bude	být	k5eAaImBp3nS
velmi	velmi	k6eAd1
bolestné	bolestný	k2eAgNnSc1d1
<g/>
.	.	kIx.
„	„	k?
<g/>
Je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
deset	deset	k4xCc4
let	léto	k1gNnPc2
nevyhrajeme	vyhrát	k5eNaPmIp1nP
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
sice	sice	k8xC
tvrdá	tvrdý	k2eAgFnSc1d1
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
konci	konec	k1gInSc6
bude	být	k5eAaImBp3nS
stát	stát	k5eAaPmF,k5eAaImF
vysněný	vysněný	k2eAgInSc1d1
tým	tým	k1gInSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
Nemýlil	mýlit	k5eNaImAgMnS
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
sice	sice	k8xC
vyhrála	vyhrát	k5eAaPmAgFnS
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1974	#num#	k4
a	a	k8xC
1990	#num#	k4
pouze	pouze	k6eAd1
jeden	jeden	k4xCgInSc4
titul	titul	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
počátku	počátek	k1gInSc6
90	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
povstal	povstat	k5eAaPmAgInS
tým	tým	k1gInSc1
snů	sen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
prohrála	prohrát	k5eAaPmAgFnS
Barcelona	Barcelona	k1gFnSc1
ve	v	k7c6
finále	finále	k1gNnSc6
Poháru	pohár	k1gInSc2
UEFA	UEFA	kA
a	a	k8xC
nedůvěra	nedůvěra	k1gFnSc1
fanoušků	fanoušek	k1gMnPc2
rostla	růst	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
bylo	být	k5eAaImAgNnS
znatelné	znatelný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
výkony	výkon	k1gInPc1
klubu	klub	k1gInSc2
jdou	jít	k5eAaImIp3nP
vzhůru	vzhůru	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trenérského	trenérský	k2eAgNnSc2d1
kormidla	kormidlo	k1gNnSc2
se	se	k3xPyFc4
chopila	chopit	k5eAaPmAgFnS
bývalá	bývalý	k2eAgFnSc1d1
superhvězda	superhvězda	k1gFnSc1
Johan	Johan	k1gMnSc1
Cruijff	Cruijff	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
dokázala	dokázat	k5eAaPmAgFnS
čtyřikrát	čtyřikrát	k6eAd1
v	v	k7c6
řadě	řada	k1gFnSc6
zvítězit	zvítězit	k5eAaPmF
v	v	k7c6
Primera	primera	k1gFnSc1
División	División	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
klubu	klub	k1gInSc6
dominovaly	dominovat	k5eAaImAgFnP
hvězdy	hvězda	k1gFnPc1
jako	jako	k8xS,k8xC
Josep	Josep	k1gMnSc1
Guardiola	Guardiola	k1gFnSc1
<g/>
,	,	kIx,
José	José	k1gNnSc1
Mari	Mar	k1gFnSc2
Bakero	Bakero	k1gNnSc1
<g/>
,	,	kIx,
Ronald	Ronald	k1gMnSc1
Koeman	Koeman	k1gMnSc1
či	či	k8xC
Hristo	Hrista	k1gMnSc5
Stoičkov	Stoičkov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
hráči	hráč	k1gMnPc1
pak	pak	k6eAd1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
získali	získat	k5eAaPmAgMnP
tři	tři	k4xCgFnPc4
velevýznamné	velevýznamný	k2eAgFnPc4d1
trofeje	trofej	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
mistrovském	mistrovský	k2eAgInSc6d1
titulu	titul	k1gInSc6
v	v	k7c6
Primera	primera	k1gFnSc1
División	División	k1gInSc4
následovala	následovat	k5eAaImAgFnS
vyřazovací	vyřazovací	k2eAgFnSc1d1
část	část	k1gFnSc1
nultého	nultý	k4xOgInSc2
ročníku	ročník	k1gInSc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
semifinálové	semifinálový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
(	(	kIx(
<g/>
odkud	odkud	k6eAd1
postupoval	postupovat	k5eAaImAgMnS
pouze	pouze	k6eAd1
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
přešla	přejít	k5eAaPmAgFnS
s	s	k7c7
přehledem	přehled	k1gInSc7
přes	přes	k7c4
trojici	trojice	k1gFnSc4
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Benfica	Benfica	k1gFnSc1
Lisabon	Lisabon	k1gInSc1
a	a	k8xC
Dynamo	dynamo	k1gNnSc1
Kyjev	Kyjev	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finále	finále	k1gNnSc6
pak	pak	k6eAd1
potkala	potkat	k5eAaPmAgFnS
italskou	italský	k2eAgFnSc4d1
Sampdorii	Sampdorie	k1gFnSc4
Janov	Janov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
základní	základní	k2eAgFnSc6d1
hrací	hrací	k2eAgFnSc6d1
době	doba	k1gFnSc6
skončil	skončit	k5eAaPmAgMnS
zápas	zápas	k1gInSc4
bez	bez	k7c2
gólů	gól	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prodloužení	prodloužení	k1gNnSc6
pak	pak	k6eAd1
rozhodl	rozhodnout	k5eAaPmAgMnS
Ronald	Ronald	k1gMnSc1
Koeman	Koeman	k1gMnSc1
gólem	gól	k1gInSc7
ve	v	k7c4
112	#num#	k4
<g/>
.	.	kIx.
minutě	minuta	k1gFnSc6
o	o	k7c6
premiérovém	premiérový	k2eAgNnSc6d1
vítězství	vítězství	k1gNnSc6
Barcelony	Barcelona	k1gFnSc2
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězstvím	vítězství	k1gNnSc7
se	se	k3xPyFc4
zároveň	zároveň	k6eAd1
kvalifikovala	kvalifikovat	k5eAaBmAgNnP
do	do	k7c2
souboje	souboj	k1gInSc2
o	o	k7c4
další	další	k2eAgFnSc4d1
významnou	významný	k2eAgFnSc4d1
trofej	trofej	k1gFnSc4
–	–	k?
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
se	se	k3xPyFc4
potkala	potkat	k5eAaPmAgFnS
s	s	k7c7
vítězem	vítěz	k1gMnSc7
Poháru	pohár	k1gInSc2
UEFA	UEFA	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
ve	v	k7c6
dvou	dva	k4xCgInPc6
zápasech	zápas	k1gInPc6
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
porazila	porazit	k5eAaPmAgFnS
německý	německý	k2eAgInSc4d1
Werder	Werder	k1gInSc4
Brémy	Brémy	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získala	získat	k5eAaPmAgFnS
tak	tak	k9
už	už	k6eAd1
třetí	třetí	k4xOgFnSc4
trofej	trofej	k1gFnSc4
v	v	k7c6
sezóně	sezóna	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
jí	jíst	k5eAaImIp3nS
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nepodařilo	podařit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
Nizozemce	Nizozemec	k1gMnSc2
k	k	k7c3
Nizozemci	Nizozemec	k1gMnSc3
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ronaldinho	Ronaldinze	k6eAd1
</s>
<s>
Úspěch	úspěch	k1gInSc1
„	„	k?
<g/>
týmu	tým	k1gInSc2
snů	sen	k1gInPc2
<g/>
“	“	k?
na	na	k7c6
počátku	počátek	k1gInSc6
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
byl	být	k5eAaImAgInS
obrovský	obrovský	k2eAgInSc1d1
a	a	k8xC
přilákal	přilákat	k5eAaPmAgInS
Barceloně	Barcelona	k1gFnSc3
po	po	k7c6
světě	svět	k1gInSc6
miliony	milion	k4xCgInPc4
fanoušků	fanoušek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prohrané	prohraná	k1gFnSc2
finále	finále	k1gNnSc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
v	v	k7c6
řeckých	řecký	k2eAgFnPc6d1
Aténách	Atény	k1gFnPc6
však	však	k9
znamenalo	znamenat	k5eAaImAgNnS
konec	konec	k1gInSc4
této	tento	k3xDgFnSc2
druhé	druhý	k4xOgFnSc2
zlaté	zlatý	k2eAgFnSc2d1
éry	éra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
klubu	klub	k1gInSc6
byl	být	k5eAaImAgInS
znát	znát	k5eAaImF
přetlak	přetlak	k1gInSc1
hvězd	hvězda	k1gFnPc2
a	a	k8xC
napětí	napětí	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
vyústilo	vyústit	k5eAaPmAgNnS
odchodem	odchod	k1gInSc7
trenéra	trenér	k1gMnSc2
Cruijffa	Cruijff	k1gMnSc2
během	během	k7c2
sezóny	sezóna	k1gFnSc2
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
něm	on	k3xPp3gInSc6
se	se	k3xPyFc4
v	v	k7c6
klubu	klub	k1gInSc6
vystřídalo	vystřídat	k5eAaPmAgNnS
během	během	k7c2
sedmi	sedm	k4xCc2
let	léto	k1gNnPc2
hned	hned	k6eAd1
šest	šest	k4xCc4
trenérů	trenér	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
získala	získat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
ještě	ještě	k6eAd1
jednou	jeden	k4xCgFnSc7
a	a	k8xC
naposledy	naposledy	k6eAd1
Pohár	pohár	k1gInSc4
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgInS
znát	znát	k5eAaImF
její	její	k3xOp3gInSc1
celkový	celkový	k2eAgInSc1d1
ústup	ústup	k1gInSc1
z	z	k7c2
absolutní	absolutní	k2eAgFnSc2d1
špičky	špička	k1gFnSc2
evropského	evropský	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Klub	klub	k1gInSc1
však	však	k9
dokázal	dokázat	k5eAaPmAgInS
těžit	těžit	k5eAaImF
ze	z	k7c2
svého	svůj	k3xOyFgNnSc2
postavení	postavení	k1gNnSc2
a	a	k8xC
na	na	k7c6
domácí	domácí	k2eAgFnSc6d1
půdě	půda	k1gFnSc6
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
posbírat	posbírat	k5eAaPmF
po	po	k7c6
dvou	dva	k4xCgInPc6
titulech	titul	k1gInPc6
mistra	mistr	k1gMnSc2
v	v	k7c6
letech	léto	k1gNnPc6
1998	#num#	k4
a	a	k8xC
1999	#num#	k4
i	i	k8xC
stejný	stejný	k2eAgInSc1d1
počet	počet	k1gInSc1
vítězství	vítězství	k1gNnSc2
ve	v	k7c6
španělském	španělský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
Copa	Copa	k1gMnSc1
del	del	k?
Rey	Rea	k1gFnSc2
v	v	k7c6
letech	léto	k1gNnPc6
1997	#num#	k4
a	a	k8xC
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
odešel	odejít	k5eAaPmAgMnS
nejdéle	dlouho	k6eAd3
sloužící	sloužící	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
v	v	k7c6
historii	historie	k1gFnSc6
(	(	kIx(
<g/>
22	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Josep	Josep	k1gInSc1
Lluís	Lluís	k1gInSc1
Núñ	Núñ	k1gInSc4
i	i	k9
Clemente	Clement	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přespříští	přespříští	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
klubu	klub	k1gInSc2
Joan	Joan	k1gMnSc1
Laporta	Laporta	k1gFnSc1
pak	pak	k6eAd1
znovu	znovu	k6eAd1
mění	měnit	k5eAaImIp3nS
strategii	strategie	k1gFnSc4
a	a	k8xC
místo	místo	k1gNnSc4
starších	starý	k2eAgFnPc2d2
hvězd	hvězda	k1gFnPc2
typu	typ	k1gInSc2
Luise	Luisa	k1gFnSc3
Figa	Figa	k1gMnSc1
či	či	k8xC
Patricka	Patricka	k1gFnSc1
Kluiverta	Kluivert	k1gMnSc2
přivádí	přivádět	k5eAaImIp3nS
Ronaldinha	Ronaldinha	k1gFnSc1
<g/>
,	,	kIx,
Samuela	Samuela	k1gFnSc1
Eto	Eto	k1gFnSc1
<g/>
’	’	k?
<g/>
a	a	k8xC
a	a	k8xC
Deca	Deca	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
tým	tým	k1gInSc1
již	již	k6eAd1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
nizozemského	nizozemský	k2eAgMnSc2d1
kouče	kouč	k1gMnSc2
Franka	Frank	k1gMnSc2
Rijkaarda	Rijkaard	k1gMnSc2
nečeká	čekat	k5eNaImIp3nS
na	na	k7c4
úspěch	úspěch	k1gInSc4
dlouho	dlouho	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
vítězí	vítězit	k5eAaImIp3nP
v	v	k7c6
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
a	a	k8xC
připisuje	připisovat	k5eAaImIp3nS
si	se	k3xPyFc3
šesté	šestý	k4xOgNnSc1
vítězství	vítězství	k1gNnSc1
v	v	k7c6
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následující	následující	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
dokonce	dokonce	k9
podruhé	podruhé	k6eAd1
triumfuje	triumfovat	k5eAaBmIp3nS
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
a	a	k8xC
s	s	k7c7
obhajobou	obhajoba	k1gFnSc7
trofejí	trofej	k1gFnSc7
za	za	k7c4
Primera	primera	k1gFnSc1
División	División	k1gInSc1
a	a	k8xC
Španělský	španělský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
získává	získávat	k5eAaImIp3nS
„	„	k?
<g/>
treble	treble	k6eAd1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
doplnit	doplnit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
finále	finále	k1gNnSc6
ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
ve	v	k7c6
francouzském	francouzský	k2eAgInSc6d1
Stade	Stad	k1gInSc5
de	de	k?
France	Franc	k1gMnSc2
porazila	porazit	k5eAaPmAgNnP
anglický	anglický	k2eAgInSc4d1
Arsenal	Arsenal	k1gFnSc7
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
pak	pak	k6eAd1
Barcelona	Barcelona	k1gFnSc1
podepisuje	podepisovat	k5eAaImIp3nS
sponzorský	sponzorský	k2eAgInSc4d1
kontrakt	kontrakt	k1gInSc4
s	s	k7c7
neziskovou	ziskový	k2eNgFnSc7d1
organizací	organizace	k1gFnSc7
UNICEF	UNICEF	kA
a	a	k8xC
jako	jako	k9
první	první	k4xOgInSc1
klub	klub	k1gInSc1
světa	svět	k1gInSc2
za	za	k7c4
hlavní	hlavní	k2eAgFnSc4d1
reklamu	reklama	k1gFnSc4
na	na	k7c6
dresu	dres	k1gInSc6
neinkasuje	inkasovat	k5eNaBmIp3nS
peníze	peníz	k1gInPc4
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
přispívá	přispívat	k5eAaImIp3nS
částkou	částka	k1gFnSc7
20	#num#	k4
milionů	milion	k4xCgInPc2
eur	euro	k1gNnPc2
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Zlaté	zlatý	k2eAgNnSc1d1
dítě	dítě	k1gNnSc1
Pep	Pepa	k1gFnPc2
Guardiola	Guardiola	k1gFnSc1
a	a	k8xC
fantom	fantom	k1gInSc1
Messi	Messe	k1gFnSc3
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pep	Pepa	k1gFnPc2
Guardiola	Guardiola	k1gFnSc1
</s>
<s>
Se	s	k7c7
závěrem	závěr	k1gInSc7
sezóny	sezóna	k1gFnSc2
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
poměrně	poměrně	k6eAd1
nečekaně	nečekaně	k6eAd1
skončil	skončit	k5eAaPmAgMnS
v	v	k7c6
klubu	klub	k1gInSc6
kouč	kouč	k1gMnSc1
Frank	Frank	k1gMnSc1
Rijkaard	Rijkaard	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
v	v	k7c6
klubu	klub	k1gInSc6
působil	působit	k5eAaImAgMnS
pět	pět	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
ním	on	k3xPp3gMnSc7
odešly	odejít	k5eAaPmAgInP
i	i	k9
pilíře	pilíř	k1gInPc1
útočné	útočný	k2eAgFnSc2d1
hry	hra	k1gFnSc2
týmu	tým	k1gInSc2
–	–	k?
Ronaldinho	Ronaldin	k1gMnSc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Deco	Deco	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
nedlouho	dlouho	k6eNd1
na	na	k7c4
to	ten	k3xDgNnSc4
následoval	následovat	k5eAaImAgInS
i	i	k8xC
kamerunský	kamerunský	k2eAgMnSc1d1
ostrostřelec	ostrostřelec	k1gMnSc1
Eto	Eto	k1gFnSc2
<g/>
’	’	k?
<g/>
o.	o.	k?
Prezident	prezident	k1gMnSc1
Joan	Joan	k1gMnSc1
Laporta	Laport	k1gMnSc2
se	se	k3xPyFc4
odhodlal	odhodlat	k5eAaPmAgMnS
k	k	k7c3
riskantnímu	riskantní	k2eAgInSc3d1
tahu	tah	k1gInSc3
a	a	k8xC
angažoval	angažovat	k5eAaBmAgInS
trenéra	trenér	k1gMnSc2
B-týmu	B-týma	k1gFnSc4
Barcelony	Barcelona	k1gFnSc2
Josepa	Josep	k1gMnSc2
„	„	k?
<g/>
Pepeho	Pepe	k1gMnSc2
<g/>
“	“	k?
Guardiolu	Guardiol	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgMnSc1
kouč	kouč	k1gMnSc1
byl	být	k5eAaImAgMnS
známý	známý	k2eAgMnSc1d1
svým	svůj	k3xOyFgInSc7
dosti	dosti	k6eAd1
benevoletním	benevoletní	k2eAgInSc7d1
přístupem	přístup	k1gInSc7
k	k	k7c3
hráčům	hráč	k1gMnPc3
(	(	kIx(
<g/>
což	což	k3yQnSc1,k3yRnSc1
byl	být	k5eAaImAgMnS
důvod	důvod	k1gInSc4
<g/>
,	,	kIx,
proč	proč	k6eAd1
někteří	některý	k3yIgMnPc1
tento	tento	k3xDgInSc4
tah	tah	k1gInSc4
kritizovali	kritizovat	k5eAaImAgMnP
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
pozice	pozice	k1gFnSc2
tahouna	tahoun	k1gMnSc2
týmu	tým	k1gInSc2
dosadil	dosadit	k5eAaPmAgInS
velmi	velmi	k6eAd1
nadaného	nadaný	k2eAgMnSc2d1
argentinského	argentinský	k2eAgMnSc2d1
technika	technik	k1gMnSc2
Lionela	Lionel	k1gMnSc2
Messiho	Messi	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pilíře	pilíř	k1gInSc2
nově	nově	k6eAd1
vzniklého	vzniklý	k2eAgInSc2d1
týmu	tým	k1gInSc2
tvořili	tvořit	k5eAaImAgMnP
Andrés	Andrés	k1gInSc4
Iniesta	Iniest	k1gMnSc2
<g/>
,	,	kIx,
Xavi	Xav	k1gFnSc2
<g/>
,	,	kIx,
obránce	obránce	k1gMnSc1
Carles	Carles	k1gMnSc1
Puyol	Puyol	k1gInSc4
<g/>
,	,	kIx,
Dani	daň	k1gFnSc3
Alves	Alvesa	k1gFnPc2
a	a	k8xC
brankář	brankář	k1gMnSc1
Víctor	Víctor	k1gMnSc1
Valdés	Valdés	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
vzniklý	vzniklý	k2eAgInSc1d1
tým	tým	k1gInSc1
se	se	k3xPyFc4
brzy	brzy	k6eAd1
ukázal	ukázat	k5eAaPmAgInS
jako	jako	k9
„	„	k?
<g/>
fotbalová	fotbalový	k2eAgFnSc1d1
symfonie	symfonie	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
rok	rok	k1gInSc4
v	v	k7c6
klubové	klubový	k2eAgFnSc6d1
historii	historie	k1gFnSc6
–	–	k?
2009	#num#	k4
</s>
<s>
Příchod	příchod	k1gInSc1
Josepa	Josep	k1gMnSc2
Guardioly	Guardiola	k1gFnSc2
na	na	k7c4
post	post	k1gInSc4
prvního	první	k4xOgMnSc2
kouče	kouč	k1gMnSc2
na	na	k7c6
počátku	počátek	k1gInSc6
sezóny	sezóna	k1gFnSc2
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
účinkoval	účinkovat	k5eAaImAgMnS
jako	jako	k9
pokropení	pokropení	k1gNnSc4
živou	živý	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgMnSc1
energický	energický	k2eAgMnSc1d1
kouč	kouč	k1gMnSc1
dovedl	dovést	k5eAaPmAgMnS
tým	tým	k1gInSc4
k	k	k7c3
nejúspěšnější	úspěšný	k2eAgFnSc3d3
sezóně	sezóna	k1gFnSc3
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
nejprve	nejprve	k6eAd1
získat	získat	k5eAaPmF
španělský	španělský	k2eAgInSc4d1
„	„	k?
<g/>
double	double	k1gInSc4
<g/>
“	“	k?
(	(	kIx(
<g/>
vítěz	vítěz	k1gMnSc1
ligy	liga	k1gFnSc2
i	i	k8xC
poháru	pohár	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
poté	poté	k6eAd1
potřetí	potřetí	k4xO
triumfovat	triumfovat	k5eAaBmF
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
následně	následně	k6eAd1
zvítězit	zvítězit	k5eAaPmF
jak	jak	k8xC,k8xS
v	v	k7c6
evropském	evropský	k2eAgInSc6d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
španělském	španělský	k2eAgInSc6d1
Superpoháru	superpohár	k1gInSc6
a	a	k8xC
nakonec	nakonec	k6eAd1
dovršit	dovršit	k5eAaPmF
sezónu	sezóna	k1gFnSc4
snů	sen	k1gInPc2
ziskem	zisk	k1gInSc7
titulu	titul	k1gInSc2
Mistra	mistr	k1gMnSc2
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zisk	zisk	k1gInSc1
šesti	šest	k4xCc2
trofejí	trofej	k1gFnPc2
znamenal	znamenat	k5eAaImAgInS
pro	pro	k7c4
Barcelonu	Barcelona	k1gFnSc4
obrovský	obrovský	k2eAgInSc1d1
příval	příval	k1gInSc1
podpory	podpora	k1gFnSc2
fanoušků	fanoušek	k1gMnPc2
i	i	k8xC
mezinárodního	mezinárodní	k2eAgNnSc2d1
uznání	uznání	k1gNnSc2
<g/>
,	,	kIx,
respektu	respekt	k1gInSc2
a	a	k8xC
autority	autorita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
se	se	k3xPyFc4
nyní	nyní	k6eAd1
již	již	k9
světovým	světový	k2eAgFnPc3d1
superhvězdám	superhvězda	k1gFnPc3
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Messim	Messi	k1gNnSc7
<g/>
,	,	kIx,
Xavim	Xavi	k1gNnSc7
a	a	k8xC
Iniestou	Iniesta	k1gFnSc7
podařilo	podařit	k5eAaPmAgNnS
zvítězit	zvítězit	k5eAaPmF
na	na	k7c6
hřišti	hřiště	k1gNnSc6
svého	svůj	k3xOyFgMnSc2
největšího	veliký	k2eAgMnSc2d3
rivala	rival	k1gMnSc2
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
6	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
což	což	k3yRnSc1,k3yQnSc1
byla	být	k5eAaImAgFnS
pro	pro	k7c4
„	„	k?
<g/>
Bílý	bílý	k2eAgInSc1d1
balet	balet	k1gInSc1
<g/>
“	“	k?
dosud	dosud	k6eAd1
největší	veliký	k2eAgFnSc1d3
domácí	domácí	k2eAgFnSc1d1
porážka	porážka	k1gFnSc1
v	v	k7c6
El	Ela	k1gFnPc2
Clásico	Clásico	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězství	vítězství	k1gNnSc1
nad	nad	k7c7
Manchesterem	Manchester	k1gInSc7
United	United	k1gInSc1
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
a	a	k8xC
Estudiantes	Estudiantesa	k1gFnPc2
de	de	k?
La	la	k1gNnSc1
Plata	plato	k1gNnSc2
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
se	se	k3xPyFc4
odrazily	odrazit	k5eAaPmAgInP
v	v	k7c6
přílivu	příliv	k1gInSc6
financí	finance	k1gFnPc2
i	i	k8xC
nových	nový	k2eAgInPc2d1
členů	člen	k1gInPc2
(	(	kIx(
<g/>
za	za	k7c4
rok	rok	k1gInSc4
2009	#num#	k4
jich	on	k3xPp3gMnPc2
přibylo	přibýt	k5eAaPmAgNnS
170	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
tak	tak	k9
v	v	k7c6
tomto	tento	k3xDgInSc6
roce	rok	k1gInSc6
vyhrála	vyhrát	k5eAaPmAgFnS
všechny	všechen	k3xTgFnPc4
soutěže	soutěž	k1gFnPc4
<g/>
,	,	kIx,
kterých	který	k3yIgFnPc2,k3yQgFnPc2,k3yRgFnPc2
se	se	k3xPyFc4
účastnila	účastnit	k5eAaImAgFnS
–	–	k?
španělský	španělský	k2eAgInSc1d1
klub	klub	k1gInSc1
nemůže	moct	k5eNaImIp3nS
momentálně	momentálně	k6eAd1
získat	získat	k5eAaPmF
více	hodně	k6eAd2
než	než	k8xS
šest	šest	k4xCc4
trofejí	trofej	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
Lionel	Lionel	k1gMnSc1
Messi	Messe	k1gFnSc4
byl	být	k5eAaImAgMnS
za	za	k7c4
rok	rok	k1gInSc4
2009	#num#	k4
oceněn	ocenit	k5eAaPmNgInS
ziskem	zisk	k1gInSc7
Zlatého	zlatý	k2eAgInSc2d1
míče	míč	k1gInSc2
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
fotbalistou	fotbalista	k1gMnSc7
světa	svět	k1gInSc2
podle	podle	k7c2
FIFA	FIFA	kA
<g/>
.	.	kIx.
</s>
<s>
Nová	nový	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
</s>
<s>
Toho	ten	k3xDgMnSc4
<g/>
,	,	kIx,
čeho	co	k3yQnSc2,k3yRnSc2,k3yInSc2
jsme	být	k5eAaImIp1nP
v	v	k7c6
minulosti	minulost	k1gFnSc6
byli	být	k5eAaImAgMnP
svědky	svědek	k1gMnPc7
třeba	třeba	k6eAd1
v	v	k7c6
podání	podání	k1gNnSc6
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
nebo	nebo	k8xC
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
nyní	nyní	k6eAd1
odehrává	odehrávat	k5eAaImIp3nS
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
přepisuje	přepisovat	k5eAaImIp3nS
historii	historie	k1gFnSc4
pod	pod	k7c7
koučem	kouč	k1gMnSc7
Guardiolou	Guardiola	k1gMnSc7
a	a	k8xC
s	s	k7c7
fenomenální	fenomenální	k2eAgFnSc7d1
trojicí	trojice	k1gFnSc7
Messi	Messe	k1gFnSc4
<g/>
,	,	kIx,
Xavi	Xave	k1gFnSc4
<g/>
,	,	kIx,
Iniesta	Iniesta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdatně	zdatně	k6eAd1
jim	on	k3xPp3gMnPc3
sekunduje	sekundovat	k5eAaImIp3nS
zbytek	zbytek	k1gInSc1
týmu	tým	k1gInSc2
–	–	k?
Valdés	Valdés	k1gInSc1
<g/>
,	,	kIx,
Puyol	Puyol	k1gInSc1
<g/>
,	,	kIx,
Abidal	Abidal	k1gFnSc1
<g/>
,	,	kIx,
Piqué	Piqué	k1gNnSc1
<g/>
,	,	kIx,
Pedro	Pedro	k1gNnSc1
<g/>
,	,	kIx,
Villa	Villa	k1gFnSc1
a	a	k8xC
mnozí	mnohý	k2eAgMnPc1d1
další	další	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
dnes	dnes	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
slyšíte	slyšet	k5eAaImIp2nP
tato	tento	k3xDgNnPc1
jména	jméno	k1gNnPc1
<g/>
,	,	kIx,
víte	vědět	k5eAaImIp2nP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
legendy	legenda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Lionel	Lionet	k5eAaPmAgMnS,k5eAaImAgMnS,k5eAaBmAgMnS
Messi	Messe	k1gFnSc4
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
dokráčel	dokráčet	k5eAaPmAgInS
klub	klub	k1gInSc1
až	až	k6eAd1
de	de	k?
semifinále	semifinále	k1gNnSc7
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
vyřazen	vyřadit	k5eAaPmNgMnS
pozdějším	pozdní	k2eAgMnSc7d2
vítězem	vítěz	k1gMnSc7
Interem	Inter	k1gInSc7
Milán	Milán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
domácí	domácí	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
obhájit	obhájit	k5eAaPmF
titul	titul	k1gInSc4
v	v	k7c6
Primera	primera	k1gFnSc1
División	División	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
pořadí	pořadí	k1gNnSc6
už	už	k9
dvacátý	dvacátý	k4xOgInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Lionel	Lionel	k1gInSc1
Messi	Messe	k1gFnSc4
byl	být	k5eAaImAgInS
opět	opět	k6eAd1
oceněn	ocenit	k5eAaPmNgInS
jako	jako	k8xC,k8xS
nejlepší	dobrý	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
jako	jako	k9
první	první	k4xOgMnSc1
vítěz	vítěz	k1gMnSc1
v	v	k7c6
anketě	anketa	k1gFnSc6
nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgNnSc1d1
sloučením	sloučení	k1gNnSc7
dvou	dva	k4xCgNnPc6
dosud	dosud	k6eAd1
nejvýznamnějších	významný	k2eAgNnPc6d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
však	však	k9
není	být	k5eNaImIp3nS
všechno	všechen	k3xTgNnSc4
<g/>
:	:	kIx,
na	na	k7c6
prvních	první	k4xOgNnPc6
třech	tři	k4xCgNnPc6
místech	místo	k1gNnPc6
se	se	k3xPyFc4
umístili	umístit	k5eAaPmAgMnP
jen	jen	k9
hráči	hráč	k1gMnPc1
Barcelony	Barcelona	k1gFnSc2
–	–	k?
Messi	Messe	k1gFnSc4
<g/>
,	,	kIx,
Xavi	Xave	k1gFnSc4
<g/>
,	,	kIx,
Iniesta	Iniesta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Následující	následující	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
dopadla	dopadnout	k5eAaPmAgFnS
pro	pro	k7c4
klub	klub	k1gInSc4
opět	opět	k6eAd1
skvěle	skvěle	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
ve	v	k7c6
třech	tři	k4xCgFnPc6
soutěžích	soutěž	k1gFnPc6
potýkal	potýkat	k5eAaImAgInS
s	s	k7c7
velmi	velmi	k6eAd1
silným	silný	k2eAgMnSc7d1
rivalem	rival	k1gMnSc7
Realem	Real	k1gInSc7
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Primera	primera	k1gFnSc1
División	División	k1gInSc4
dokázala	dokázat	k5eAaPmAgFnS
Barcelona	Barcelona	k1gFnSc1
zvítězit	zvítězit	k5eAaPmF
rozdílem	rozdíl	k1gInSc7
čtyř	čtyři	k4xCgInPc2
bodů	bod	k1gInPc2
již	již	k6eAd1
několik	několik	k4yIc1
kol	kolo	k1gNnPc2
před	před	k7c7
koncem	konec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
na	na	k7c6
sebe	sebe	k3xPyFc4
tito	tento	k3xDgMnPc1
soupeři	soupeř	k1gMnPc1
narazili	narazit	k5eAaPmAgMnP
v	v	k7c6
semifinále	semifinále	k1gNnSc6
<g/>
,	,	kIx,
i	i	k9
tady	tady	k6eAd1
dokázala	dokázat	k5eAaPmAgFnS
Barcelona	Barcelona	k1gFnSc1
svého	svůj	k3xOyFgMnSc2
rivala	rival	k1gMnSc2
přetlačit	přetlačit	k5eAaPmF
celkovým	celkový	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finále	finále	k1gNnSc6
pak	pak	k6eAd1
pohodlně	pohodlně	k6eAd1
porazila	porazit	k5eAaPmAgFnS
Manchester	Manchester	k1gInSc4
United	United	k1gMnSc1
a	a	k8xC
dokráčela	dokráčet	k5eAaPmAgFnS
si	se	k3xPyFc3
pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
historicky	historicky	k6eAd1
čtvrtý	čtvrtý	k4xOgInSc1
triumf	triumf	k1gInSc1
–	–	k?
třetí	třetí	k4xOgFnSc4
za	za	k7c2
posledních	poslední	k2eAgNnPc2d1
šest	šest	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediné	jediné	k1gNnSc1
<g/>
,	,	kIx,
čím	co	k3yRnSc7,k3yInSc7,k3yQnSc7
se	se	k3xPyFc4
doposud	doposud	k6eAd1
rok	rok	k1gInSc4
2011	#num#	k4
liší	lišit	k5eAaImIp3nP
od	od	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
prohra	prohra	k1gFnSc1
ve	v	k7c6
finále	finále	k1gNnSc6
domácí	domácí	k1gFnSc2
poháru	pohár	k1gInSc2
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
právě	právě	k6eAd1
s	s	k7c7
Realem	Real	k1gInSc7
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Real	Real	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
zápas	zápas	k1gInSc4
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
<g/>
,	,	kIx,
když	když	k8xS
ve	v	k7c6
103	#num#	k4
<g/>
.	.	kIx.
minutě	minuta	k1gFnSc6
rozhodl	rozhodnout	k5eAaPmAgMnS
svým	svůj	k3xOyFgInSc7
gólem	gól	k1gInSc7
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
si	se	k3xPyFc3
tak	tak	k6eAd1
na	na	k7c4
svůj	svůj	k3xOyFgInSc4
26	#num#	k4
<g/>
.	.	kIx.
domácí	domácí	k2eAgInSc1d1
pohár	pohár	k1gInSc1
musí	muset	k5eAaImIp3nS
ještě	ještě	k6eAd1
počkat	počkat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Sezona	sezona	k1gFnSc1
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Oslava	oslava	k1gFnSc1
gólu	gól	k1gInSc2
</s>
<s>
Ročník	ročník	k1gInSc1
začal	začít	k5eAaPmAgInS
pro	pro	k7c4
Barcelonu	Barcelona	k1gFnSc4
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letním	letní	k2eAgNnSc6d1
období	období	k1gNnSc6
získala	získat	k5eAaPmAgFnS
dvě	dva	k4xCgFnPc4
velké	velký	k2eAgFnPc4d1
posily	posila	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francesc	Francesc	k1gInSc1
Fà	Fà	k1gInSc4
přišel	přijít	k5eAaPmAgInS
po	po	k7c6
dlouhém	dlouhý	k2eAgNnSc6d1
jednání	jednání	k1gNnSc6
z	z	k7c2
Arsenal	Arsenal	k1gFnSc2
FC	FC	kA
za	za	k7c4
29	#num#	k4
milionů	milion	k4xCgInPc2
€	€	k?
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
klubu	klub	k1gInSc6
se	se	k3xPyFc4
uvedl	uvést	k5eAaPmAgMnS
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
prvních	první	k4xOgNnPc6
čtyřech	čtyři	k4xCgNnPc6
kolech	kolo	k1gNnPc6
Primera	primera	k1gFnSc1
División	División	k1gInSc1
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
dal	dát	k5eAaPmAgInS
vždy	vždy	k6eAd1
gól	gól	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhou	druhý	k4xOgFnSc7
posilou	posila	k1gFnSc7
byl	být	k5eAaImAgInS
chilský	chilský	k2eAgMnSc1d1
útočník	útočník	k1gMnSc1
Alexis	Alexis	k1gFnPc2
Sánchez	Sánchez	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
přišel	přijít	k5eAaPmAgMnS
posílit	posílit	k5eAaPmF
útok	útok	k1gInSc4
z	z	k7c2
italského	italský	k2eAgInSc2d1
Udinese	Udinese	k1gFnPc1
Calcio	Calcio	k1gNnSc4
za	za	k7c4
26	#num#	k4
milionů	milion	k4xCgInPc2
€	€	k?
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
s	s	k7c7
těmito	tento	k3xDgMnPc7
hráči	hráč	k1gMnPc7
vstoupil	vstoupit	k5eAaPmAgMnS
klub	klub	k1gInSc4
do	do	k7c2
sezony	sezona	k1gFnSc2
dvojzápasem	dvojzápas	k1gInSc7
o	o	k7c6
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gInSc2
2011	#num#	k4
<g/>
,	,	kIx,
čili	čili	k8xC
španělský	španělský	k2eAgInSc4d1
Superpohár	superpohár	k1gInSc4
s	s	k7c7
Realem	Real	k1gInSc7
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
úvodní	úvodní	k2eAgFnSc6d1
remíze	remíza	k1gFnSc6
2-2	2-2	k4
na	na	k7c6
hřišti	hřiště	k1gNnSc6
Realu	Real	k1gInSc2
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
přišla	přijít	k5eAaPmAgFnS
domácí	domácí	k2eAgFnSc1d1
výhra	výhra	k1gFnSc1
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
dvěma	dva	k4xCgFnPc7
góly	gól	k1gInPc4
počvtrté	počvtrtý	k2eAgInPc1d1
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Den	den	k1gInSc1
před	před	k7c7
utkáním	utkání	k1gNnSc7
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
los	los	k1gInSc1
nového	nový	k2eAgInSc2d1
ročníku	ročník	k1gInSc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
Barceloně	Barcelona	k1gFnSc3
nalosován	nalosován	k2eAgMnSc1d1
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
i	i	k9
český	český	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
FC	FC	kA
Viktoria	Viktoria	k1gFnSc1
Plzeň	Plzeň	k1gFnSc1
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
této	tento	k3xDgFnSc6
příležitosti	příležitost	k1gFnSc6
byl	být	k5eAaImAgInS
nejlepším	dobrý	k2eAgMnSc7d3
hráčem	hráč	k1gMnSc7
Evropy	Evropa	k1gFnSc2
za	za	k7c4
sezonu	sezona	k1gFnSc4
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
vyhlášen	vyhlásit	k5eAaPmNgInS
Lionel	Lionel	k1gInSc1
Messi	Messe	k1gFnSc3
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Týmové	týmový	k2eAgNnSc1d1
foto	foto	k1gNnSc1
po	po	k7c6
vítězství	vítězství	k1gNnSc6
ve	v	k7c4
finále	finále	k1gNnSc4
MS	MS	kA
klubů	klub	k1gInPc2
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vítězství	vítězství	k1gNnSc1
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
přineslo	přinést	k5eAaPmAgNnS
Barçe	Barçe	k1gNnSc1
kromě	kromě	k7c2
účasti	účast	k1gFnSc2
v	v	k7c6
Superpoháru	superpohár	k1gInSc6
UEFA	UEFA	kA
i	i	k8xC
účast	účast	k1gFnSc1
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
2011	#num#	k4
<g/>
,	,	kIx,
do	do	k7c2
jehož	jenž	k3xRgNnSc2,k3xOyRp3gNnSc2
semifinále	semifinále	k1gNnSc2
byla	být	k5eAaImAgNnP
nasazena	nasadit	k5eAaPmNgNnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vítězství	vítězství	k1gNnSc6
4-0	4-0	k4
nad	nad	k7c7
Al-Sadd	Al-Sadd	k1gInSc1
<g/>
,	,	kIx,
vítězem	vítěz	k1gMnSc7
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
AFC	AFC	kA
<g/>
,	,	kIx,
si	se	k3xPyFc3
stejným	stejný	k2eAgInSc7d1
rozdílem	rozdíl	k1gInSc7
poradila	poradit	k5eAaPmAgFnS
se	s	k7c7
Santos	Santosa	k1gFnPc2
FC	FC	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
vyhrál	vyhrát	k5eAaPmAgMnS
Copa	Copa	k1gMnSc1
Libertadores	Libertadores	k1gMnSc1
2011	#num#	k4
a	a	k8xC
získala	získat	k5eAaPmAgFnS
podruhé	podruhé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
titul	titul	k1gInSc4
z	z	k7c2
tohoto	tento	k3xDgInSc2
prestižního	prestižní	k2eAgInSc2d1
podniku	podnik	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
semifinálovém	semifinálový	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
se	se	k3xPyFc4
zranil	zranit	k5eAaPmAgMnS
David	David	k1gMnSc1
Villa	Villa	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
do	do	k7c2
zbytku	zbytek	k1gInSc2
sezony	sezona	k1gFnSc2
nenastoupil	nastoupit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Sezona	sezona	k1gFnSc1
2011-12	2011-12	k4
naznačila	naznačit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Barcelona	Barcelona	k1gFnSc1
není	být	k5eNaImIp3nS
neporazitelným	porazitelný	k2eNgInSc7d1
tým	tým	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
semifinále	semifinále	k1gNnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
podlehla	podlehnout	k5eAaPmAgFnS
přes	přes	k7c4
lepší	dobrý	k2eAgInSc4d2
výkon	výkon	k1gInSc4
dobře	dobře	k6eAd1
bránící	bránící	k2eAgFnSc1d1
Chelsea	Chelsea	k1gFnSc1
a	a	k8xC
nestala	stát	k5eNaPmAgFnS
se	s	k7c7
prvním	první	k4xOgInSc7
klubem	klub	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
dokázal	dokázat	k5eAaPmAgInS
trofej	trofej	k1gFnSc4
obhájit	obhájit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Lionel	Lionel	k1gInSc4
Messi	Messe	k1gFnSc4
tak	tak	k6eAd1
alespoň	alespoň	k9
překonal	překonat	k5eAaPmAgInS
střelecký	střelecký	k2eAgInSc1d1
rekord	rekord	k1gInSc1
soutěže	soutěž	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
trefil	trefit	k5eAaPmAgMnS
celkem	celkem	k6eAd1
čtrnáctkrát	čtrnáctkrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
titul	titul	k1gInSc4
v	v	k7c6
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
a	a	k8xC
o	o	k7c6
sérii	série	k1gFnSc6
tří	tři	k4xCgNnPc2
vítězství	vítězství	k1gNnPc2
jí	on	k3xPp3gFnSc3
zase	zase	k9
připravil	připravit	k5eAaPmAgInS
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věčný	věčný	k2eAgMnSc1d1
rival	rival	k1gMnSc1
zvítězil	zvítězit	k5eAaPmAgMnS
při	při	k7c6
derby	derby	k1gNnSc6
El-Clásico	El-Clásico	k6eAd1
na	na	k7c6
Nou	Nou	k1gFnSc6
Campu	camp	k1gInSc2
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
vytvořil	vytvořit	k5eAaPmAgMnS
si	se	k3xPyFc3
rozhodující	rozhodující	k2eAgInSc4d1
náskok	náskok	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
Barca	Barca	k1gFnSc1
nedokázala	dokázat	k5eNaPmAgFnS
stáhnout	stáhnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čest	čest	k1gFnSc1
klubu	klub	k1gInSc2
tak	tak	k6eAd1
opět	opět	k6eAd1
zachraňoval	zachraňovat	k5eAaImAgInS
svými	svůj	k3xOyFgInPc7
góly	gól	k1gInPc7
Lionel	Lionel	k1gMnSc1
Messi	Mess	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
po	po	k7c6
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
získal	získat	k5eAaPmAgMnS
Pichichi	Pichichi	k1gNnSc1
Trophy	Tropha	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
s	s	k7c7
rekordními	rekordní	k2eAgFnPc7d1
50	#num#	k4
brankami	branka	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Nikomu	nikdo	k3yNnSc3
před	před	k7c7
ním	on	k3xPp3gMnSc7
se	se	k3xPyFc4
v	v	k7c6
předních	přední	k2eAgFnPc6d1
světových	světový	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
nepodařilo	podařit	k5eNaPmAgNnS
této	tento	k3xDgFnSc2
mety	meta	k1gFnSc2
dosáhnout	dosáhnout	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
všech	všecek	k3xTgFnPc6
soutěžích	soutěž	k1gFnPc6
<g/>
,	,	kIx,
kterých	který	k3yIgFnPc2,k3yRgFnPc2,k3yQgFnPc2
se	se	k3xPyFc4
lotos	lotos	k1gInSc4
Barcelona	Barcelona	k1gFnSc1
účastnila	účastnit	k5eAaImAgFnS
pak	pak	k6eAd1
vstřelil	vstřelit	k5eAaPmAgMnS
73	#num#	k4
branek	branka	k1gFnPc2
a	a	k8xC
překonal	překonat	k5eAaPmAgMnS
letitý	letitý	k2eAgInSc4d1
rekord	rekord	k1gInSc4
Gerda	Gerd	k1gMnSc2
Müllera	Müller	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
v	v	k7c6
sezoně	sezona	k1gFnSc6
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
vstřelil	vstřelit	k5eAaPmAgMnS
68	#num#	k4
branek	branka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
Lionel	Lionel	k1gInSc1
Messi	Messe	k1gFnSc4
je	být	k5eAaImIp3nS
nejlepší	dobrý	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
historie	historie	k1gFnSc2
fotbalu	fotbal	k1gInSc2
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ten	ten	k3xDgInSc1
poslední	poslední	k2eAgMnSc1d1
přidal	přidat	k5eAaPmAgInS
ve	v	k7c6
finále	finále	k1gNnSc6
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
Barça	Barça	k1gFnSc1
porazila	porazit	k5eAaPmAgFnS
Athletic	Athletice	k1gFnPc2
Bilbao	Bilbao	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
souboji	souboj	k1gInSc6
dvou	dva	k4xCgInPc2
nejúspěšnějších	úspěšný	k2eAgInPc2d3
klubů	klub	k1gInPc2
soutěže	soutěž	k1gFnSc2
zvítězila	zvítězit	k5eAaPmAgFnS
3-0	3-0	k4
a	a	k8xC
získala	získat	k5eAaPmAgFnS
v	v	k7c6
ní	on	k3xPp3gFnSc6
26	#num#	k4
<g/>
.	.	kIx.
vítězství	vítězství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
zápas	zápas	k1gInSc1
byl	být	k5eAaImAgInS
zároveň	zároveň	k6eAd1
posledním	poslední	k2eAgNnSc7d1
pro	pro	k7c4
Josepa	Josep	k1gMnSc4
Guardiolu	Guardiol	k1gInSc2
<g/>
,	,	kIx,
nejúspěšnějšího	úspěšný	k2eAgMnSc2d3
trenéra	trenér	k1gMnSc2
v	v	k7c6
historii	historie	k1gFnSc6
klubového	klubový	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
během	během	k7c2
4	#num#	k4
sezon	sezona	k1gFnPc2
na	na	k7c6
lavičce	lavička	k1gFnSc6
katalánského	katalánský	k2eAgInSc2d1
klubu	klub	k1gInSc2
získal	získat	k5eAaPmAgMnS
neuvěřitelných	uvěřitelný	k2eNgFnPc2d1
14	#num#	k4
trofejí	trofej	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
S	s	k7c7
jeho	jeho	k3xOp3gInSc7
odchodem	odchod	k1gInSc7
končí	končit	k5eAaImIp3nS
i	i	k9
slavná	slavný	k2eAgFnSc1d1
éra	éra	k1gFnSc1
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
navázání	navázání	k1gNnSc4
na	na	k7c6
ní	on	k3xPp3gFnSc6
se	se	k3xPyFc4
od	od	k7c2
sezony	sezona	k1gFnSc2
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
pokoušel	pokoušet	k5eAaImAgMnS
nový	nový	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
Tito	tento	k3xDgMnPc1
Vilanova	Vilanův	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s>
zařídil	zařídit	k5eAaPmAgMnS
především	především	k9
Lionel	Lionel	k1gMnSc1
Messi	Messe	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Barcu	Barca	k1gFnSc4
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
jubilejní	jubilejní	k2eAgNnSc4d1
10	#num#	k4
<g/>
.	.	kIx.
vítězství	vítězství	k1gNnSc3
v	v	k7c6
této	tento	k3xDgFnSc6
soutěži	soutěž	k1gFnSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
této	tento	k3xDgFnSc2
mety	meta	k1gFnSc2
dosáhl	dosáhnout	k5eAaPmAgInS
jako	jako	k9
první	první	k4xOgInSc1
klub	klub	k1gInSc1
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neuběhly	uběhnout	k5eNaPmAgInP
ani	ani	k8xC
dva	dva	k4xCgInPc1
týdny	týden	k1gInPc1
a	a	k8xC
Barcelona	Barcelona	k1gFnSc1
slavila	slavit	k5eAaImAgFnS
další	další	k2eAgInSc4d1
triumf	triumf	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
utkání	utkání	k1gNnSc6
o	o	k7c4
Superpohár	superpohár	k1gInSc4
UEFA	UEFA	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
střetla	střetnout	k5eAaPmAgFnS
s	s	k7c7
portugalským	portugalský	k2eAgMnSc7d1
vítězem	vítěz	k1gMnSc7
Evropské	evropský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
<g/>
,	,	kIx,
zvítězila	zvítězit	k5eAaPmAgFnS
pohodlně	pohodlně	k6eAd1
2-0	2-0	k4
a	a	k8xC
v	v	k7c4
soutěž	soutěž	k1gFnSc4
vyhrála	vyhrát	k5eAaPmAgFnS
</s>
<s>
Roky	rok	k1gInPc1
po	po	k7c6
Guardiolovi	Guardiol	k1gMnSc6
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Lionel	Lionet	k5eAaBmAgMnS,k5eAaPmAgMnS,k5eAaImAgMnS
Messi	Messe	k1gFnSc4
<g/>
,	,	kIx,
čtyřnásobný	čtyřnásobný	k2eAgMnSc1d1
vítěz	vítěz	k1gMnSc1
Zlatého	zlatý	k2eAgInSc2d1
míče	míč	k1gInSc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
historicky	historicky	k6eAd1
nejlepší	dobrý	k2eAgMnSc1d3
střelec	střelec	k1gMnSc1
Barcelony	Barcelona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
všech	všecek	k3xTgFnPc2
soutěží	soutěž	k1gFnPc2
vstřelil	vstřelit	k5eAaPmAgMnS
již	již	k6eAd1
420	#num#	k4
gólů	gól	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nový	nový	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
Tito	tento	k3xDgMnPc1
Vilanova	Vilanův	k2eAgMnSc4d1
do	do	k7c2
sezony	sezona	k1gFnSc2
nevstoupil	vstoupit	k5eNaPmAgMnS
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
nepodařilo	podařit	k5eNaPmAgNnS
získat	získat	k5eAaPmF
španělský	španělský	k2eAgInSc4d1
Superpohár	superpohár	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
Barcelona	Barcelona	k1gFnSc1
podlehla	podlehnout	k5eAaPmAgFnS
Realu	Real	k1gInSc6
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
tato	tento	k3xDgFnSc1
sezona	sezona	k1gFnSc1
považovat	považovat	k5eAaImF
za	za	k7c4
velice	velice	k6eAd1
rozporuplnou	rozporuplný	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Messi	Messe	k1gFnSc6
sice	sice	k8xC
na	na	k7c6
začátku	začátek	k1gInSc6
nového	nový	k2eAgInSc2d1
roku	rok	k1gInSc2
počtvrté	počtvrté	k4xO
uspěl	uspět	k5eAaPmAgMnS
v	v	k7c6
anketě	anketa	k1gFnSc6
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gFnSc1
forma	forma	k1gFnSc1
pomalu	pomalu	k6eAd1
upadala	upadat	k5eAaPmAgFnS,k5eAaImAgFnS
a	a	k8xC
už	už	k6eAd1
se	se	k3xPyFc4
netěšil	těšit	k5eNaImAgMnS
takovým	takový	k3xDgInPc3
úspěchům	úspěch	k1gInPc3
jako	jako	k8xC,k8xS
pod	pod	k7c7
Guardiolou	Guardiola	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
samotná	samotný	k2eAgFnSc1d1
hra	hra	k1gFnSc1
už	už	k6eAd1
nebyla	být	k5eNaImAgFnS
zdaleka	zdaleka	k6eAd1
tak	tak	k6eAd1
efektivní	efektivní	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
Barca	Barca	k1gFnSc1
byla	být	k5eAaImAgFnS
stále	stále	k6eAd1
velice	velice	k6eAd1
obávaným	obávaný	k2eAgInSc7d1
klubem	klub	k1gInSc7
a	a	k8xC
dokráčela	dokráčet	k5eAaPmAgNnP
až	až	k6eAd1
do	do	k7c2
semifinále	semifinále	k1gNnSc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
po	po	k7c6
porážkách	porážka	k1gFnPc6
0	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
na	na	k7c6
stadionu	stadion	k1gInSc6
Alianz	Alianza	k1gFnPc2
Area	area	k1gFnSc1
a	a	k8xC
0	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
na	na	k7c4
Camp	camp	k1gInSc4
Nou	Nou	k1gFnSc1
vypadla	vypadnout	k5eAaPmAgFnS
s	s	k7c7
Bayernem	Bayern	k1gInSc7
Mnichov	Mnichov	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
následně	následně	k6eAd1
celou	celý	k2eAgFnSc4d1
soutěž	soutěž	k1gFnSc4
vyhrál	vyhrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
Barcelona	Barcelona	k1gFnSc1
vypadla	vypadnout	k5eAaPmAgFnS
také	také	k9
v	v	k7c6
semifinále	semifinále	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
ji	on	k3xPp3gFnSc4
ve	v	k7c6
vyrovnaném	vyrovnaný	k2eAgInSc6d1
dvojzápase	dvojzápas	k1gInSc6
porazil	porazit	k5eAaPmAgInS
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
pak	pak	k6eAd1
ve	v	k7c6
finále	finále	k1gNnSc6
podlehl	podlehnout	k5eAaPmAgInS
Atléticu	Atlética	k1gFnSc4
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc4
trofej	trofej	k1gFnSc4
za	za	k7c2
Tita	Tit	k2eAgFnSc1d1
Barcelona	Barcelona	k1gFnSc1
nakonec	nakonec	k6eAd1
získala	získat	k5eAaPmAgFnS
<g/>
,	,	kIx,
když	když	k8xS
vyrovnala	vyrovnat	k5eAaPmAgFnS,k5eAaBmAgFnS
rekord	rekord	k1gInSc4
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
(	(	kIx(
<g/>
100	#num#	k4
bodů	bod	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
zvítězila	zvítězit	k5eAaPmAgFnS
v	v	k7c6
tomto	tento	k3xDgInSc6
ročníku	ročník	k1gInSc6
La	la	k1gNnSc2
Ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
pohár	pohár	k1gInSc4
byl	být	k5eAaImAgMnS
nakonec	nakonec	k6eAd1
Titův	Titův	k2eAgMnSc1d1
poslední	poslední	k2eAgMnSc1d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
kvůli	kvůli	k7c3
zdravotním	zdravotní	k2eAgFnPc3d1
potížím	potíž	k1gFnPc3
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
funkce	funkce	k1gFnSc2
odstoupil	odstoupit	k5eAaPmAgInS
a	a	k8xC
nahradil	nahradit	k5eAaPmAgInS
ho	on	k3xPp3gNnSc4
Gerardo	Gerardo	k1gNnSc4
Martino	Martin	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Klub	klub	k1gInSc1
na	na	k7c4
sebe	sebe	k3xPyFc4
v	v	k7c6
létě	léto	k1gNnSc6
upoutal	upoutat	k5eAaPmAgInS
pozornost	pozornost	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
za	za	k7c4
minimální	minimální	k2eAgFnSc4d1
mzdu	mzda	k1gFnSc4
poslal	poslat	k5eAaPmAgMnS
do	do	k7c2
Atlética	Atlétic	k1gInSc2
Madrid	Madrid	k1gInSc1
svého	svůj	k3xOyFgMnSc4
útočníka	útočník	k1gMnSc4
Davida	David	k1gMnSc4
Villu	Villa	k1gMnSc4
a	a	k8xC
za	za	k7c4
obrovskou	obrovský	k2eAgFnSc4d1
sumu	suma	k1gFnSc4
přivedl	přivést	k5eAaPmAgInS
mladého	mladý	k2eAgMnSc4d1
a	a	k8xC
talentovaného	talentovaný	k2eAgMnSc4d1
Neymara	Neymar	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
díky	díky	k7c3
gólu	gól	k1gInSc2
tohoto	tento	k3xDgMnSc2
Brazilce	Brazilec	k1gMnSc2
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
podařilo	podařit	k5eAaPmAgNnS
zvítězit	zvítězit	k5eAaPmF
v	v	k7c6
Superpoháru	superpohár	k1gInSc6
nad	nad	k7c7
Atléticem	Atlétic	k1gMnSc7
Madrid	Madrid	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavé	zajímavý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c4
Atlético	Atlético	k1gNnSc4
se	se	k3xPyFc4
prosadil	prosadit	k5eAaPmAgMnS
právě	právě	k9
David	David	k1gMnSc1
Villa	Villa	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
po	po	k7c6
výhře	výhra	k1gFnSc6
v	v	k7c6
Superpoháru	superpohár	k1gInSc6
začala	začít	k5eAaPmAgFnS
dělat	dělat	k5eAaImF
Barcelona	Barcelona	k1gFnSc1
svým	svůj	k3xOyFgMnPc3
fanouškům	fanoušek	k1gMnPc3
radost	radost	k6eAd1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
zažila	zažít	k5eAaPmAgFnS
nejlepší	dobrý	k2eAgInSc4d3
start	start	k1gInSc4
v	v	k7c6
historii	historie	k1gFnSc6
La	la	k1gNnSc2
Ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
první	první	k4xOgFnSc7
polovinou	polovina	k1gFnSc7
sezony	sezona	k1gFnSc2
prošla	projít	k5eAaPmAgFnS
pouze	pouze	k6eAd1
s	s	k7c7
jednou	jeden	k4xCgFnSc7
porážkou	porážka	k1gFnSc7
a	a	k8xC
dvěma	dva	k4xCgFnPc7
remízami	remíza	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
přes	přes	k7c4
skvělé	skvělý	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
byla	být	k5eAaImAgFnS
však	však	k9
hra	hra	k1gFnSc1
velice	velice	k6eAd1
kritizovaná	kritizovaný	k2eAgFnSc1d1
a	a	k8xC
ani	ani	k8xC
trápící	trápící	k2eAgMnSc1d1
se	se	k3xPyFc4
Messi	Messe	k1gFnSc4
s	s	k7c7
tím	ten	k3xDgNnSc7
nedokázal	dokázat	k5eNaPmAgMnS
nic	nic	k3yNnSc1
udělat	udělat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Upadnutí	upadnutí	k1gNnSc1
jeho	jeho	k3xOp3gFnSc2
formy	forma	k1gFnSc2
se	se	k3xPyFc4
projevilo	projevit	k5eAaPmAgNnS
<g/>
,	,	kIx,
když	když	k8xS
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
2013	#num#	k4
vyhrál	vyhrát	k5eAaPmAgMnS
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
a	a	k8xC
přerušil	přerušit	k5eAaPmAgMnS
tak	tak	k6eAd1
Messiho	Messi	k1gMnSc4
čtyřletou	čtyřletý	k2eAgFnSc4d1
dominanci	dominance	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhou	druhý	k4xOgFnSc4
polovinu	polovina	k1gFnSc4
sezony	sezona	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
na	na	k7c4
slova	slovo	k1gNnPc4
kritiků	kritik	k1gMnPc2
a	a	k8xC
Barceloně	Barcelona	k1gFnSc3
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
všechno	všechen	k3xTgNnSc1
hroutit	hroutit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
sice	sice	k8xC
prošla	projít	k5eAaPmAgFnS
přes	přes	k7c4
Manchester	Manchester	k1gInSc4
City	City	k1gFnSc2
FC	FC	kA
<g/>
,	,	kIx,
ale	ale	k8xC
už	už	k6eAd1
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
po	po	k7c6
výsledcích	výsledek	k1gInPc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
vypadla	vypadnout	k5eAaPmAgFnS
s	s	k7c7
Atléticem	Atlétic	k1gMnSc7
Madrid	Madrid	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lize	liga	k1gFnSc6
jejich	jejich	k3xOp3gInPc1
výsledky	výsledek	k1gInPc1
taky	taky	k6eAd1
upadaly	upadat	k5eAaPmAgInP,k5eAaImAgInP
<g/>
,	,	kIx,
avšak	avšak	k8xC
o	o	k7c4
titul	titul	k1gInSc4
bojovali	bojovat	k5eAaImAgMnP
až	až	k9
do	do	k7c2
posledního	poslední	k2eAgInSc2d1
zápasu	zápas	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
však	však	k9
nepodařilo	podařit	k5eNaPmAgNnS
porazit	porazit	k5eAaPmF
Atlético	Atlético	k6eAd1
a	a	k8xC
skončili	skončit	k5eAaPmAgMnP
tak	tak	k6eAd1
na	na	k7c6
druhém	druhý	k4xOgInSc6
místě	místo	k1gNnSc6
právě	právě	k9
po	po	k7c6
méně	málo	k6eAd2
slavném	slavný	k2eAgInSc6d1
madridském	madridský	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
v	v	k7c6
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
bojovala	bojovat	k5eAaImAgFnS
Barcelona	Barcelona	k1gFnSc1
doslova	doslova	k6eAd1
do	do	k7c2
posledních	poslední	k2eAgFnPc2d1
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostali	dostat	k5eAaPmAgMnP
se	se	k3xPyFc4
totiž	totiž	k9
až	až	k9
do	do	k7c2
finále	finále	k1gNnSc2
<g/>
,	,	kIx,
jenže	jenže	k8xC
vyrovnaný	vyrovnaný	k2eAgInSc4d1
zápas	zápas	k1gInSc4
pár	pár	k4xCyI
minut	minuta	k1gFnPc2
před	před	k7c7
koncem	konec	k1gInSc7
rozhodl	rozhodnout	k5eAaPmAgMnS
Gareth	Gareth	k1gMnSc1
Bale	bal	k1gInSc5
a	a	k8xC
pohár	pohár	k1gInSc1
tak	tak	k6eAd1
získal	získat	k5eAaPmAgInS
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
tedy	tedy	k9
po	po	k7c6
dlouhé	dlouhý	k2eAgFnSc6d1
době	doba	k1gFnSc6
zažila	zažít	k5eAaPmAgFnS
sezonu	sezona	k1gFnSc4
bez	bez	k7c2
trofeje	trofej	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
okamžitému	okamžitý	k2eAgNnSc3d1
odvolání	odvolání	k1gNnSc3
Gerarda	Gerard	k1gMnSc2
Martina	Martin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k6eAd1
něj	on	k3xPp3gMnSc4
z	z	k7c2
Celty	celta	k1gFnSc2
Vigo	Vigo	k6eAd1
přivedlo	přivést	k5eAaPmAgNnS
vedení	vedení	k1gNnSc4
Luise	Luisa	k1gFnSc3
Enriqueho	Enrique	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Éra	éra	k1gFnSc1
Luise	Luisa	k1gFnSc3
Enriqueho	Enriqueha	k1gMnSc5
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Po	po	k7c6
neúspěšné	úspěšný	k2eNgFnSc6d1
sezoně	sezona	k1gFnSc6
bylo	být	k5eAaImAgNnS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
klub	klub	k1gInSc1
čeká	čekat	k5eAaImIp3nS
přestavba	přestavba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Luis	Luisa	k1gFnPc2
Enrique	Enriqu	k1gFnSc2
Martínez	Martínez	k1gMnSc1
García	García	k1gMnSc1
se	se	k3xPyFc4
zbavil	zbavit	k5eAaPmAgMnS
nejdřív	dříve	k6eAd3
náhradníka	náhradník	k1gMnSc4
Alexise	Alexise	k1gFnSc2
Sáncheze	Sáncheze	k1gFnSc2
a	a	k8xC
poté	poté	k6eAd1
i	i	k9
Cesca	Cesc	k2eAgFnSc1d1
Fabregase	Fabregasa	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
navíc	navíc	k6eAd1
opustili	opustit	k5eAaPmAgMnP
i	i	k8xC
Víctor	Víctor	k1gInSc4
Valdés	Valdésa	k1gFnPc2
<g/>
,	,	kIx,
Manuel	Manuela	k1gFnPc2
Pinto	pinta	k1gFnSc5
a	a	k8xC
Carles	Carles	k1gInSc4
Puyol	Puyola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
nich	on	k3xPp3gMnPc2
klub	klub	k1gInSc4
za	za	k7c4
rekordní	rekordní	k2eAgFnSc4d1
částku	částka	k1gFnSc4
přivedl	přivést	k5eAaPmAgInS
Luise	Luisa	k1gFnSc6
Suáreze	Suáreze	k1gFnSc2
<g/>
,	,	kIx,
Ivana	Ivan	k1gMnSc2
Rakitiče	Rakitič	k1gMnSc2
<g/>
,	,	kIx,
Claudia	Claudia	k1gFnSc1
Brava	bravo	k1gMnSc2
<g/>
,	,	kIx,
Marc-Andrého	Marc-Andrý	k2eAgMnSc4d1
ter	ter	k?
Stegena	Stegen	k1gMnSc4
<g/>
,	,	kIx,
Thomase	Thomas	k1gMnSc4
Vermaelena	Vermaelen	k2eAgMnSc4d1
a	a	k8xC
Jérémy	Jérém	k1gInPc7
Mathieua	Mathieuum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
prošla	projít	k5eAaPmAgFnS
přestavbou	přestavba	k1gFnSc7
a	a	k8xC
do	do	k7c2
sezony	sezona	k1gFnSc2
vstoupila	vstoupit	k5eAaPmAgFnS
velice	velice	k6eAd1
dobře	dobře	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Hned	hned	k6eAd1
v	v	k7c6
prvních	první	k4xOgInPc6
měsících	měsíc	k1gInPc6
velice	velice	k6eAd1
zářil	zářit	k5eAaImAgInS
právě	právě	k9
Neymar	Neymar	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
přestup	přestup	k1gInSc1
byl	být	k5eAaImAgInS
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
spíše	spíše	k9
kritizován	kritizován	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
Lionel	Lionel	k1gMnSc1
Messi	Messe	k1gFnSc4
po	po	k7c6
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
nalezl	nalézt	k5eAaBmAgInS,k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
ztracenou	ztracený	k2eAgFnSc4d1
formu	forma	k1gFnSc4
a	a	k8xC
všechno	všechen	k3xTgNnSc1
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
vyvíjet	vyvíjet	k5eAaImF
v	v	k7c6
prostě	prostě	k6eAd1
katalánského	katalánský	k2eAgInSc2d1
velkoklubu	velkoklub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trenér	trenér	k1gMnSc1
navíc	navíc	k6eAd1
dával	dávat	k5eAaImAgMnS
spoustu	spousta	k1gFnSc4
šancí	šance	k1gFnPc2
mladým	mladý	k2eAgMnPc3d1
hráčům	hráč	k1gMnPc3
z	z	k7c2
B-týmu	B-tým	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
ale	ale	k9
mnohým	mnohý	k2eAgMnPc3d1
fanouškům	fanoušek	k1gMnPc3
nelíbilo	líbit	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
však	však	k9
klub	klub	k1gInSc1
ke	k	k7c3
konci	konec	k1gInSc3
října	říjen	k1gInSc2
podlehl	podlehnout	k5eAaPmAgInS
Realu	Real	k1gInSc3
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
forma	forma	k1gFnSc1
značně	značně	k6eAd1
upadla	upadnout	k5eAaPmAgFnS
a	a	k8xC
fanoušci	fanoušek	k1gMnPc1
už	už	k6eAd1
opět	opět	k6eAd1
očekávali	očekávat	k5eAaImAgMnP
sezonu	sezona	k1gFnSc4
bez	bez	k7c2
trofejí	trofej	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
potupné	potupný	k2eAgFnSc6d1
prohře	prohra	k1gFnSc6
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
s	s	k7c7
Realem	Real	k1gInSc7
Sociedad	Sociedad	k1gInSc1
se	se	k3xPyFc4
však	však	k9
situace	situace	k1gFnSc1
začala	začít	k5eAaPmAgFnS
měnit	měnit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
se	se	k3xPyFc4
zlepšila	zlepšit	k5eAaPmAgFnS
<g/>
,	,	kIx,
rozstřílel	rozstřílet	k5eAaPmAgMnS
se	se	k3xPyFc4
i	i	k9
klíčový	klíčový	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
Lionel	Lionel	k1gMnSc1
Messi	Messe	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
2015	#num#	k4
klub	klub	k1gInSc1
navíc	navíc	k6eAd1
díky	díky	k7c3
brankám	branka	k1gFnPc3
Jéremyho	Jéremy	k1gMnSc4
Mathieua	Mathieuum	k1gNnSc2
a	a	k8xC
Luise	Luisa	k1gFnSc6
Suáreze	Suáreze	k1gFnSc2
dokázal	dokázat	k5eAaPmAgMnS
porazit	porazit	k5eAaPmF
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
a	a	k8xC
se	s	k7c7
čtyřbodovým	čtyřbodový	k2eAgInSc7d1
náskokem	náskok	k1gInSc7
nad	nad	k7c7
odvěkým	odvěký	k2eAgMnSc7d1
rivalem	rival	k1gMnSc7
byl	být	k5eAaImAgInS
na	na	k7c6
dobré	dobrý	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
k	k	k7c3
vítězství	vítězství	k1gNnSc3
v	v	k7c6
La	la	k1gNnSc1
Lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titul	titul	k1gInSc1
byl	být	k5eAaImAgInS
zajištěn	zajistit	k5eAaPmNgInS
v	v	k7c6
předposledním	předposlední	k2eAgNnSc6d1
kole	kolo	k1gNnSc6
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2015	#num#	k4
při	při	k7c6
vítězství	vítězství	k1gNnSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
proti	proti	k7c3
Atléticu	Atléticum	k1gNnSc3
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězný	vítězný	k2eAgInSc4d1
gól	gól	k1gInSc4
vstřelil	vstřelit	k5eAaPmAgMnS
Lionel	Lionel	k1gMnSc1
Messi	Messe	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
Ve	v	k7c6
finále	finále	k1gNnSc6
španělského	španělský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
se	se	k3xPyFc4
po	po	k7c6
úžasné	úžasný	k2eAgFnSc6d1
sólové	sólový	k2eAgFnSc6d1
akci	akce	k1gFnSc6
prosadil	prosadit	k5eAaPmAgMnS
Lionel	Lionel	k1gMnSc1
Messi	Messe	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c4
nějž	jenž	k3xRgMnSc4
navázal	navázat	k5eAaPmAgInS
Neymar	Neymar	k1gInSc1
a	a	k8xC
v	v	k7c6
závěru	závěr	k1gInSc6
přidal	přidat	k5eAaPmAgMnS
další	další	k2eAgInSc4d1
gól	gól	k1gInSc4
opět	opět	k6eAd1
Messi	Messe	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katalánci	Katalánec	k1gMnSc6
tedy	tedy	k9
ve	v	k7c6
finále	finále	k1gNnSc6
zvítězili	zvítězit	k5eAaPmAgMnP
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barceloně	Barcelona	k1gFnSc6
se	se	k3xPyFc4
také	také	k9
podařilo	podařit	k5eAaPmAgNnS
vyhrát	vyhrát	k5eAaPmF
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
finále	finále	k1gNnSc6
porazila	porazit	k5eAaPmAgFnS
Juventus	Juventus	k1gInSc4
FC	FC	kA
díky	díky	k7c3
gólům	gól	k1gInPc3
Ivana	Ivan	k1gMnSc4
Rakitiće	Rakitiće	k1gFnSc6
<g/>
,	,	kIx,
Luise	Luisa	k1gFnSc6
Suáreze	Suáreze	k1gFnSc1
a	a	k8xC
Neymara	Neymara	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zápas	zápas	k1gInSc1
skončil	skončit	k5eAaPmAgInS
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
pro	pro	k7c4
Barcelonu	Barcelona	k1gFnSc4
a	a	k8xC
díky	díky	k7c3
tomu	ten	k3xDgMnSc3
znovu	znovu	k6eAd1
vyhrála	vyhrát	k5eAaPmAgFnS
treble	treble	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
tak	tak	k6eAd1
prvním	první	k4xOgInSc7
klubem	klub	k1gInSc7
historie	historie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
tohoto	tento	k3xDgInSc2
úspěchu	úspěch	k1gInSc2
dosáhl	dosáhnout	k5eAaPmAgInS
dvakrát	dvakrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Messi	Messe	k1gFnSc4
<g/>
,	,	kIx,
Neymar	Neymar	k1gInSc4
a	a	k8xC
Suárez	Suárez	k1gInSc4
navíc	navíc	k6eAd1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
společně	společně	k6eAd1
nastříleli	nastřílet	k5eAaPmAgMnP
122	#num#	k4
branek	branka	k1gFnPc2
a	a	k8xC
pokořili	pokořit	k5eAaPmAgMnP
tak	tak	k6eAd1
španělský	španělský	k2eAgInSc4d1
rekord	rekord	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konec	konec	k1gInSc1
sezony	sezona	k1gFnSc2
se	se	k3xPyFc4
i	i	k9
přes	přes	k7c4
velké	velký	k2eAgInPc4d1
soutěžní	soutěžní	k2eAgInPc4d1
úspěchy	úspěch	k1gInPc4
nesl	nést	k5eAaImAgMnS
ve	v	k7c6
smutném	smutný	k2eAgMnSc6d1
duchu	duch	k1gMnSc6
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
se	se	k3xPyFc4
naplnil	naplnit	k5eAaPmAgInS
dlouho	dlouho	k6eAd1
očekávaný	očekávaný	k2eAgInSc4d1
přestup	přestup	k1gInSc4
a	a	k8xC
klub	klub	k1gInSc4
opustila	opustit	k5eAaPmAgFnS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
jeho	jeho	k3xOp3gNnPc2
největších	veliký	k2eAgNnPc2d3
legend-	legend-	k?
Xavi	Xavi	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nutno	nutno	k6eAd1
ale	ale	k8xC
podotknout	podotknout	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
mu	on	k3xPp3gMnSc3
klub	klub	k1gInSc1
během	během	k7c2
posledního	poslední	k2eAgInSc2d1
ligového	ligový	k2eAgInSc2d1
zápasu	zápas	k1gInSc2
připravil	připravit	k5eAaPmAgMnS
ohromnou	ohromný	k2eAgFnSc4d1
rozlučku	rozlučka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Klub	klub	k1gInSc1
nevstoupil	vstoupit	k5eNaPmAgInS
do	do	k7c2
sezóny	sezóna	k1gFnSc2
vůbec	vůbec	k9
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
přípravných	přípravný	k2eAgInPc6d1
přátelských	přátelský	k2eAgInPc6d1
zápasech	zápas	k1gInPc6
prohrál	prohrát	k5eAaPmAgInS
s	s	k7c7
nejdříve	dříve	k6eAd3
s	s	k7c7
Manchesterem	Manchester	k1gInSc7
United	United	k1gMnSc1
<g/>
,	,	kIx,
Chelsea	Chelsea	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
poté	poté	k6eAd1
i	i	k9
Fiorentinou	Fiorentina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nutno	nutno	k6eAd1
ale	ale	k8xC
podotknout	podotknout	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
těchto	tento	k3xDgInPc2
zápasů	zápas	k1gInPc2
nezasáhly	zasáhnout	k5eNaPmAgFnP
hvězdy	hvězda	k1gFnPc1
jako	jako	k9
Messi	Messe	k1gFnSc4
<g/>
,	,	kIx,
Neymar	Neymar	k1gInSc4
<g/>
,	,	kIx,
Mascherano	Mascherana	k1gFnSc5
<g/>
,	,	kIx,
Bravo	bravo	k1gMnSc1
nebo	nebo	k8xC
Alves	Alves	k1gMnSc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
si	se	k3xPyFc3
prodloužili	prodloužit	k5eAaPmAgMnP
dovolenou	dovolená	k1gFnSc4
díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
účasti	účast	k1gFnSc3
na	na	k7c4
Copa	Cop	k2eAgNnPc4d1
América	Américum	k1gNnPc4
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgInSc7
oficiálním	oficiální	k2eAgInSc7d1
zápasem	zápas	k1gInSc7
sezóny	sezóna	k1gFnSc2
byl	být	k5eAaImAgInS
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
2015	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
Barcelona	Barcelona	k1gFnSc1
vyzvala	vyzvat	k5eAaPmAgFnS
Sevillu	Sevilla	k1gFnSc4
a	a	k8xC
když	když	k8xS
už	už	k6eAd1
se	se	k3xPyFc4
zdálo	zdát	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
Sevilla	Sevilla	k1gFnSc1
opravdu	opravdu	k6eAd1
otočí	otočit	k5eAaPmIp3nS
náskok	náskok	k1gInSc4
Barcelony	Barcelona	k1gFnSc2
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
rozhodl	rozhodnout	k5eAaPmAgMnS
svým	svůj	k3xOyFgInSc7
gólem	gól	k1gInSc7
Pedro	Pedro	k1gNnSc1
a	a	k8xC
zápas	zápas	k1gInSc1
skončil	skončit	k5eAaPmAgInS
5	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
pro	pro	k7c4
katalánský	katalánský	k2eAgInSc4d1
celek	celek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
poté	poté	k6eAd1
Barcelona	Barcelona	k1gFnSc1
nastoupila	nastoupit	k5eAaPmAgFnS
do	do	k7c2
dvojzápasu	dvojzápas	k1gInSc2
o	o	k7c4
španělský	španělský	k2eAgInSc4d1
Superpohár	superpohár	k1gInSc4
proti	proti	k7c3
Athletic	Athletice	k1gFnPc2
Bilbau	Bilbaus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc4
zápas	zápas	k1gInSc4
se	se	k3xPyFc4
hrál	hrát	k5eAaImAgMnS
venku	venku	k6eAd1
a	a	k8xC
k	k	k7c3
překvapení	překvapení	k1gNnSc3
všech	všecek	k3xTgInPc2
skončil	skončit	k5eAaPmAgMnS
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
pro	pro	k7c4
Athletic	Athletice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
odvetě	odveta	k1gFnSc6
Barcelona	Barcelona	k1gFnSc1
nedokázala	dokázat	k5eNaPmAgFnS
náskok	náskok	k1gInSc4
soupeře	soupeř	k1gMnSc4
stáhnout	stáhnout	k5eAaPmF
<g/>
,	,	kIx,
zápas	zápas	k1gInSc1
skončil	skončit	k5eAaPmAgInS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Krátce	krátce	k6eAd1
poté	poté	k6eAd1
klub	klub	k1gInSc1
opustil	opustit	k5eAaPmAgInS
Pedro	Pedro	k1gNnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
zamířil	zamířit	k5eAaPmAgMnS
do	do	k7c2
Chelsea	Chelse	k1gInSc2
FC	FC	kA
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
toužil	toužit	k5eAaImAgMnS
po	po	k7c6
větším	veliký	k2eAgInSc6d2
prostoru	prostor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
přes	přes	k7c4
léto	léto	k1gNnSc4
naopak	naopak	k6eAd1
získala	získat	k5eAaPmAgFnS
Ardu	Arda	k1gMnSc4
Turana	Turan	k1gMnSc4
z	z	k7c2
Atletica	Atleticum	k1gNnSc2
Madrid	Madrid	k1gInSc1
a	a	k8xC
Aleixe	Aleixe	k1gFnSc1
Vidala	Vidala	k1gFnSc1
ze	z	k7c2
Sevilly	Sevilla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgMnPc1
hráči	hráč	k1gMnPc1
na	na	k7c4
svůj	svůj	k3xOyFgInSc4
start	start	k1gInSc4
ale	ale	k8xC
museli	muset	k5eAaImAgMnP
počkat	počkat	k5eAaPmF
až	až	k9
do	do	k7c2
příštího	příští	k2eAgNnSc2d1
přestupového	přestupový	k2eAgNnSc2d1
období	období	k1gNnSc2
díky	díky	k7c3
zákazu	zákaz	k1gInSc3
přestupů	přestup	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
Barceloně	Barcelona	k1gFnSc3
udělen	udělen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
B-týmu	B-tým	k1gInSc2
Luis	Luisa	k1gFnPc2
Enrique	Enrique	k1gFnSc1
povolal	povolat	k5eAaPmAgMnS
útočníky	útočník	k1gMnPc4
Munira	Muniro	k1gNnSc2
a	a	k8xC
Sandra	Sandra	k1gFnSc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
mu	on	k3xPp3gMnSc3
sloužili	sloužit	k5eAaImAgMnP
jako	jako	k9
náhradníci	náhradník	k1gMnPc1
a	a	k8xC
Alena	Alena	k1gFnSc1
Halilovice	Halilovice	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
ale	ale	k8xC
obratem	obratem	k6eAd1
poslán	poslat	k5eAaPmNgMnS
na	na	k7c4
hostování	hostování	k1gNnSc4
do	do	k7c2
Sportingu	Sporting	k1gInSc2
Gijón	Gijón	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
v	v	k7c6
domácí	domácí	k2eAgFnSc6d1
lize	liga	k1gFnSc6
Barcelona	Barcelona	k1gFnSc1
zažila	zažít	k5eAaPmAgFnS
velice	velice	k6eAd1
pomalý	pomalý	k2eAgInSc4d1
start	start	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhráli	vyhrát	k5eAaPmAgMnP
sice	sice	k8xC
první	první	k4xOgInPc4
čtyři	čtyři	k4xCgInPc4
zápasy	zápas	k1gInPc4
v	v	k7c6
řadě	řada	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gInPc1
výkony	výkon	k1gInPc1
byly	být	k5eAaImAgInP
kritizovány	kritizovat	k5eAaImNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
první	první	k4xOgFnSc4
Barcelonu	Barcelona	k1gFnSc4
potrestala	potrestat	k5eAaPmAgFnS
Celta	celta	k1gFnSc1
Vigo	Vigo	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
Barcelonu	Barcelona	k1gFnSc4
ztrapnila	ztrapnit	k5eAaPmAgFnS
výsledkem	výsledek	k1gInSc7
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
nato	nato	k6eAd1
se	se	k3xPyFc4
při	při	k7c6
zápasu	zápas	k1gInSc6
s	s	k7c7
Las	laso	k1gNnPc2
Palmas	Palmas	k1gInSc1
zranil	zranit	k5eAaPmAgInS
Lionel	Lionel	k1gInSc4
Messi	Messe	k1gFnSc4
na	na	k7c4
dva	dva	k4xCgInPc4
měsíce	měsíc	k1gInPc4
a	a	k8xC
Barcelona	Barcelona	k1gFnSc1
utrpěla	utrpět	k5eAaPmAgFnS
další	další	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
od	od	k7c2
Sevilly	Sevilla	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
zranění	zranění	k1gNnPc2
Lionela	Lionel	k1gMnSc2
Messiho	Messi	k1gMnSc2
se	se	k3xPyFc4
Neymar	Neymar	k1gMnSc1
a	a	k8xC
Suárez	Suárez	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
třeba	třeba	k6eAd1
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nepříliš	příliš	k6eNd1
známý	známý	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
Sergi	Serg	k1gFnSc2
Roberto	Roberta	k1gFnSc5
<g/>
,	,	kIx,
dostali	dostat	k5eAaPmAgMnP
do	do	k7c2
obrovské	obrovský	k2eAgFnSc2d1
formy	forma	k1gFnSc2
a	a	k8xC
odstartovali	odstartovat	k5eAaPmAgMnP
dlouhou	dlouhý	k2eAgFnSc4d1
vlnu	vlna	k1gFnSc4
skvělých	skvělý	k2eAgInPc2d1
výkonů	výkon	k1gInPc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
završili	završit	k5eAaPmAgMnP
vítězstvím	vítězství	k1gNnSc7
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
nad	nad	k7c7
Realem	Real	k1gInSc7
Madrid	Madrid	k1gInSc1
na	na	k7c6
Santiagu	Santiago	k1gNnSc6
Bernabeu	Bernabeus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvakrát	dvakrát	k6eAd1
se	se	k3xPyFc4
do	do	k7c2
sítě	síť	k1gFnSc2
konkurenta	konkurent	k1gMnSc2
trefil	trefit	k5eAaPmAgMnS
Luis	Luisa	k1gFnPc2
Suárez	Suárez	k1gInSc1
a	a	k8xC
po	po	k7c6
jednom	jeden	k4xCgInSc6
gólu	gól	k1gInSc6
přidali	přidat	k5eAaPmAgMnP
Andrés	Andrés	k1gInSc4
Iniesta	Iniest	k1gMnSc2
a	a	k8xC
Neymar	Neymar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
si	se	k3xPyFc3
tak	tak	k6eAd1
upevnila	upevnit	k5eAaPmAgFnS
průběžnou	průběžný	k2eAgFnSc4d1
první	první	k4xOgFnSc4
příčku	příčka	k1gFnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
navíc	navíc	k6eAd1
Barcelona	Barcelona	k1gFnSc1
již	již	k6eAd1
potřetí	potřetí	k4xO
vyhrála	vyhrát	k5eAaPmAgFnS
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
tak	tak	k6eAd1
nejúspěšnějším	úspěšný	k2eAgInSc7d3
klubem	klub	k1gInSc7
v	v	k7c6
dějinách	dějiny	k1gFnPc6
této	tento	k3xDgFnSc2
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Luis	Luisa	k1gFnPc2
Suárez	Suáreza	k1gFnPc2
na	na	k7c6
turnaji	turnaj	k1gInSc6
nastřílel	nastřílet	k5eAaPmAgMnS
pět	pět	k4xCc1
gólů	gól	k1gInPc2
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
i	i	k8xC
hráčem	hráč	k1gMnSc7
turnaje	turnaj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
tak	tak	k6eAd1
zakončila	zakončit	k5eAaPmAgFnS
rok	rok	k1gInSc4
2015	#num#	k4
s	s	k7c7
pěti	pět	k4xCc2
ze	z	k7c2
šesti	šest	k4xCc2
možných	možný	k2eAgFnPc2d1
trofejí	trofej	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
úspěchem	úspěch	k1gInSc7
byl	být	k5eAaImAgInS
Zlatý	zlatý	k2eAgInSc1d1
míč	míč	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
v	v	k7c6
lednu	leden	k1gInSc6
2016	#num#	k4
vyhrál	vyhrát	k5eAaPmAgInS
Lionel	Lionel	k1gInSc1
Messi	Messe	k1gFnSc4
a	a	k8xC
vylepšil	vylepšit	k5eAaPmAgInS
tak	tak	k9
svůj	svůj	k3xOyFgInSc4
vlastní	vlastní	k2eAgInSc4d1
rekord	rekord	k1gInSc4
v	v	k7c6
počtu	počet	k1gInSc6
vyhraných	vyhraný	k2eAgInPc2d1
Zlatých	zlatý	k2eAgInPc2d1
míčů	míč	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Sezóny	sezóna	k1gFnPc1
2017	#num#	k4
-	-	kIx~
2020	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2017	#num#	k4
Barcelona	Barcelona	k1gFnSc1
zaznamenala	zaznamenat	k5eAaPmAgFnS
největší	veliký	k2eAgInSc4d3
návrat	návrat	k1gInSc4
v	v	k7c6
historii	historie	k1gFnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
v	v	k7c6
osmifinálovém	osmifinálový	k2eAgMnSc6d1
16	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
porazila	porazit	k5eAaPmAgFnS
Paříž	Paříž	k1gFnSc4
Saint-Germain	Saint-Germain	k1gInSc1
6	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
první	první	k4xOgInSc1
duel	duel	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
prohrála	prohrát	k5eAaPmAgFnS
ve	v	k7c6
Francii	Francie	k1gFnSc6
se	s	k7c7
skóre	skóre	k1gNnSc7
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
(	(	kIx(
<g/>
celkové	celkový	k2eAgNnSc4d1
skóre	skóre	k1gNnSc4
6	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2017	#num#	k4
byl	být	k5eAaImAgMnS
bývalý	bývalý	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
Ernesto	Ernesta	k1gMnSc5
Valverde	Valverd	k1gInSc5
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
nástupcem	nástupce	k1gMnSc7
Luisa	Luisa	k1gFnSc1
Enriqueho	Enrique	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
podepsal	podepsat	k5eAaPmAgMnS
dvouletý	dvouletý	k2eAgInSc4d1
kontrakt	kontrakt	k1gInSc4
s	s	k7c7
opcí	opce	k1gFnSc7
na	na	k7c4
další	další	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2017	#num#	k4
Barcelona	Barcelona	k1gFnSc1
vydala	vydat	k5eAaPmAgFnS
prohlášení	prohlášení	k1gNnSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
zaujala	zaujmout	k5eAaPmAgFnS
stanovisko	stanovisko	k1gNnSc4
ke	k	k7c3
katalánskému	katalánský	k2eAgNnSc3d1
referendu	referendum	k1gNnSc3
z	z	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
a	a	k8xC
uvedla	uvést	k5eAaPmAgFnS
<g/>
:	:	kIx,
"	"	kIx"
<g/>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
bude	být	k5eAaImBp3nS
při	při	k7c6
maximálním	maximální	k2eAgNnSc6d1
respektování	respektování	k1gNnSc6
svého	svůj	k3xOyFgInSc2
rozmanitého	rozmanitý	k2eAgInSc2d1
souboru	soubor	k1gInSc2
členů	člen	k1gMnPc2
nadále	nadále	k6eAd1
podporovat	podporovat	k5eAaImF
vůli	vůle	k1gFnSc4
většiny	většina	k1gFnSc2
obyvatel	obyvatel	k1gMnPc2
Katalánska	Katalánsko	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
bude	být	k5eAaImBp3nS
to	ten	k3xDgNnSc1
dělat	dělat	k5eAaImF
občanským	občanský	k2eAgInSc7d1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
mírovým	mírový	k2eAgInPc3d1
a	a	k8xC
příkladným	příkladný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
V	V	kA
den	den	k1gInSc1
referenda	referendum	k1gNnSc2
požádal	požádat	k5eAaPmAgInS
barcelonský	barcelonský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
o	o	k7c4
odložení	odložení	k1gNnSc4
zápasu	zápas	k1gInSc2
proti	proti	k7c3
UD	UD	kA
Las	laso	k1gNnPc2
Palmas	Palmas	k1gInSc1
z	z	k7c2
důvodu	důvod	k1gInSc2
násilí	násilí	k1gNnSc2
v	v	k7c6
Katalánsku	Katalánsko	k1gNnSc6
<g/>
,	,	kIx,
avšak	avšak	k8xC
žádost	žádost	k1gFnSc4
La	la	k1gNnSc2
Liga	liga	k1gFnSc1
odmítla	odmítnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
byl	být	k5eAaImAgInS
hrán	hrát	k5eAaImNgInS
za	za	k7c7
zavřenými	zavřený	k2eAgFnPc7d1
branami	brána	k1gFnPc7
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Vítězem	vítěz	k1gMnSc7
La	la	k1gNnSc2
Ligy	liga	k1gFnSc2
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2017-18	2017-18	k4
byla	být	k5eAaImAgFnS
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2018	#num#	k4
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
zvítězila	zvítězit	k5eAaPmAgFnS
nad	nad	k7c7
Villarrealem	Villarreal	k1gInSc7
5	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
stanovila	stanovit	k5eAaPmAgFnS
tak	tak	k6eAd1
nejdelší	dlouhý	k2eAgFnSc4d3
sérii	série	k1gFnSc4
bez	bez	k7c2
prohry	prohra	k1gFnSc2
(	(	kIx(
<g/>
43	#num#	k4
zápasů	zápas	k1gInPc2
<g/>
)	)	kIx)
v	v	k7c6
historii	historie	k1gFnSc6
La	la	k1gNnSc2
Ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2019	#num#	k4
získala	získat	k5eAaPmAgFnS
Barcelona	Barcelona	k1gFnSc1
26	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
v	v	k7c6
La	la	k1gNnSc1
Lize	liga	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Titul	titul	k1gInSc1
v	v	k7c4
La	la	k1gNnSc4
Liga	liga	k1gFnSc1
však	však	k9
zastínilo	zastínit	k5eAaPmAgNnS
potupné	potupný	k2eAgNnSc1d1
vypadnutí	vypadnutí	k1gNnSc1
z	z	k7c2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
v	v	k7c6
semifinále	semifinále	k1gNnSc6
s	s	k7c7
jejím	její	k3xOp3gMnSc7
pozdějším	pozdní	k2eAgMnSc7d2
vítězem	vítěz	k1gMnSc7
Liverpoolem	Liverpool	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barca	Barca	k1gFnSc1
prohrála	prohrát	k5eAaPmAgFnS
druhý	druhý	k4xOgInSc4
zápas	zápas	k1gInSc4
0	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
po	po	k7c6
domácím	domácí	k2eAgNnSc6d1
vítězství	vítězství	k1gNnSc6
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
měla	mít	k5eAaImAgFnS
postup	postup	k1gInSc4
na	na	k7c4
dosah	dosah	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
13	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2020	#num#	k4
bývalý	bývalý	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
Realu	Real	k1gInSc2
Betis	Betis	k1gFnSc2
Quique	Quique	k1gInSc1
Setién	Setién	k1gInSc1
po	po	k7c6
prohře	prohra	k1gFnSc6
s	s	k7c7
Atlétikem	Atlétikum	k1gNnSc7
Madrid	Madrid	k1gInSc1
ve	v	k7c6
španělském	španělský	k2eAgInSc6d1
Superpoháru	superpohár	k1gInSc6
nahradil	nahradit	k5eAaPmAgInS
Ernesta	Ernest	k1gMnSc4
Valverdeho	Valverde	k1gMnSc4
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
novým	nový	k2eAgMnSc7d1
hlavním	hlavní	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
Barcelony	Barcelona	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Barcelona	Barcelona	k1gFnSc1
byla	být	k5eAaImAgFnS
na	na	k7c6
čele	čelo	k1gNnSc6
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
vypuknutí	vypuknutí	k1gNnSc1
pandemie	pandemie	k1gFnSc2
koronavirus	koronavirus	k1gInSc1
COVID-19	COVID-19	k1gFnSc4
zastavilo	zastavit	k5eAaPmAgNnS
celou	celý	k2eAgFnSc4d1
soutěž	soutěž	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledním	poslední	k2eAgMnSc7d1
odehraných	odehraný	k2eAgMnPc2d1
zápasů	zápas	k1gInPc2
před	před	k7c7
nuceným	nucený	k2eAgNnSc7d1
přerušením	přerušení	k1gNnSc7
bylo	být	k5eAaImAgNnS
domácí	domácí	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
nad	nad	k7c4
Real	Real	k1gInSc4
Sociedad	Sociedad	k1gInSc1
7	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Liga	liga	k1gFnSc1
se	se	k3xPyFc4
obnovila	obnovit	k5eAaPmAgFnS
až	až	k9
v	v	k7c6
polovině	polovina	k1gFnSc6
června	červen	k1gInSc2
a	a	k8xC
po	po	k7c6
více	hodně	k6eAd2
než	než	k8xS
tříměsíční	tříměsíční	k2eAgFnSc6d1
přestávce	přestávka	k1gFnSc6
odehrála	odehrát	k5eAaPmAgFnS
Barcelona	Barcelona	k1gFnSc1
první	první	k4xOgInSc1
zápas	zápas	k1gInSc1
až	až	k9
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
proti	proti	k7c3
Mallorce	Mallorka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návratu	návrat	k1gInSc6
na	na	k7c4
hřiště	hřiště	k1gNnSc4
ale	ale	k8xC
jejich	jejich	k3xOp3gFnSc4
výkonnost	výkonnost	k1gFnSc4
poklesla	poklesnout	k5eAaPmAgFnS
a	a	k8xC
nakonec	nakonec	k6eAd1
16	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
prohrála	prohrát	k5eAaPmAgFnS
ligový	ligový	k2eAgInSc4d1
titul	titul	k1gInSc4
s	s	k7c7
Realem	Real	k1gInSc7
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
I	i	k9
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
Katalánci	Katalánec	k1gMnPc1
prohráli	prohrát	k5eAaPmAgMnP
boj	boj	k1gInSc4
o	o	k7c4
ligový	ligový	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
měli	mít	k5eAaImAgMnP
stále	stále	k6eAd1
naději	naděje	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
zvítězili	zvítězit	k5eAaPmAgMnP
nad	nad	k7c7
FC	FC	kA
Neapol	Neapol	k1gFnSc1
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamenalo	znamenat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
nastoupí	nastoupit	k5eAaPmIp3nS
proti	proti	k7c3
Bayernu	Bayern	k1gInSc3
Mnichov	Mnichov	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
v	v	k7c6
sezoně	sezona	k1gFnSc6
zdolal	zdolat	k5eAaPmAgMnS
Chelsea	Chelsea	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barca	Barc	k1gInSc2
však	však	k9
uhrála	uhrát	k5eAaPmAgFnS
jednu	jeden	k4xCgFnSc4
z	z	k7c2
najpotupnejších	najpotupný	k2eAgFnPc2d2,k2eAgFnPc2d1
proher	prohra	k1gFnPc2
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
historii	historie	k1gFnSc6
s	s	k7c7
výsledkem	výsledek	k1gInSc7
2	#num#	k4
<g/>
:	:	kIx,
<g/>
8	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Výsledek	výsledek	k1gInSc1
znamenal	znamenat	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
Barca	Barca	k1gFnSc1
se	se	k3xPyFc4
popáté	popáté	k4xO
za	za	k7c2
sebou	se	k3xPyFc7
nedostala	dostat	k5eNaPmAgFnS
do	do	k7c2
finále	finále	k1gNnSc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
když	když	k8xS
jednou	jeden	k4xCgFnSc7
postoupila	postoupit	k5eAaPmAgFnS
do	do	k7c2
semifinále	semifinále	k1gNnSc2
a	a	k8xC
čtyřikrát	čtyřikrát	k6eAd1
vypadla	vypadnout	k5eAaPmAgFnS
ve	v	k7c6
čtvrtfinálové	čtvrtfinálový	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
klub	klub	k1gInSc1
potvrdil	potvrdit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Setién	Setién	k1gInSc1
byl	být	k5eAaImAgInS
odvolán	odvolat	k5eAaPmNgInS
z	z	k7c2
funkce	funkce	k1gFnSc2
trenéra	trenér	k1gMnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
přičemž	přičemž	k6eAd1
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
funkce	funkce	k1gFnSc2
byl	být	k5eAaImAgInS
odvolán	odvolat	k5eAaPmNgMnS
i	i	k9
fotbalový	fotbalový	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Eric	Eric	k1gFnSc4
Abidal	Abidal	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
dva	dva	k4xCgInPc4
dny	den	k1gInPc4
později	pozdě	k6eAd2
byl	být	k5eAaImAgMnS
Ronald	Ronald	k1gMnSc1
Koeman	Koeman	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
nového	nový	k2eAgMnSc2d1
hlavního	hlavní	k2eAgMnSc2d1
trenéra	trenér	k1gMnSc2
Barcelony	Barcelona	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
však	však	k9
změny	změna	k1gFnPc1
neskončily	skončit	k5eNaPmAgFnP
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
oznámil	oznámit	k5eAaPmAgMnS
Josep	Josep	k1gMnSc1
Maria	Mario	k1gMnSc2
Bartomeu	Bartomeus	k1gInSc2
(	(	kIx(
<g/>
vůči	vůči	k7c3
jeho	jeho	k3xOp3gFnSc3
osobě	osoba	k1gFnSc3
se	se	k3xPyFc4
veřejně	veřejně	k6eAd1
ohradil	ohradit	k5eAaPmAgMnS
i	i	k9
Leo	Leo	k1gMnSc1
Messi	Messe	k1gFnSc4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
svou	svůj	k3xOyFgFnSc4
rezignaci	rezignace	k1gFnSc4
na	na	k7c6
funkci	funkce	k1gFnSc6
prezidenta	prezident	k1gMnSc2
spolu	spolu	k6eAd1
se	s	k7c7
zbytkem	zbytek	k1gInSc7
představenstva	představenstvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kronika	kronika	k1gFnSc1
Futbol	Futbol	k1gInSc1
Club	club	k1gInSc1
Barcelona	Barcelona	k1gFnSc1
</s>
<s>
Kronika	kronika	k1gFnSc1
historie	historie	k1gFnSc1
Futbol	Futbol	k1gInSc4
Club	club	k1gInSc1
Barcelona	Barcelona	k1gFnSc1
</s>
<s>
1899	#num#	k4
-	-	kIx~
Založení	založení	k1gNnSc1
klubu	klub	k1gInSc2
Foot-Ball	Foot-Ball	k1gInSc1
Club	club	k1gInSc1
Barcelona	Barcelona	k1gFnSc1
Joanem	Joan	k1gMnSc7
Gamperem	Gamper	k1gMnSc7
22	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1902	#num#	k4
-	-	kIx~
Barcelona	Barcelona	k1gFnSc1
získává	získávat	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
první	první	k4xOgFnSc4
trofej	trofej	k1gFnSc4
<g/>
,	,	kIx,
vyhrává	vyhrávat	k5eAaImIp3nS
Copa	Copa	k1gMnSc1
Macaya	Macaya	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založena	založen	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1905	#num#	k4
-	-	kIx~
První	první	k4xOgNnPc4
vítězství	vítězství	k1gNnPc4
v	v	k7c6
Campionat	Campionat	k1gFnSc6
de	de	k?
Catalunya	Cataluny	k1gInSc2
<g/>
,	,	kIx,
do	do	k7c2
zániku	zánik	k1gInSc2
soutěže	soutěž	k1gFnSc2
ji	on	k3xPp3gFnSc4
FCB	FCB	kA
vyhraje	vyhrát	k5eAaPmIp3nS
23	#num#	k4
<g/>
×	×	k?
</s>
<s>
1909	#num#	k4
-	-	kIx~
Premiérová	premiérový	k2eAgFnSc1d1
účast	účast	k1gFnSc1
v	v	k7c6
celostátní	celostátní	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
královský	královský	k2eAgInSc4d1
pohár	pohár	k1gInSc4
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vypadnutí	vypadnutí	k1gNnSc1
v	v	k7c6
semifinále	semifinále	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
1910	#num#	k4
-	-	kIx~
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
vyhrává	vyhrávat	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
skupinu	skupina	k1gFnSc4
a	a	k8xC
o	o	k7c4
pohár	pohár	k1gInSc4
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
s	s	k7c7
Athletic	Athletice	k1gFnPc2
Bilbao	Bilbao	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězství	vítězství	k1gNnSc1
v	v	k7c6
Copa	Cop	k1gInSc2
dels	delsa	k1gFnPc2
Pirineus	Pirineus	k1gMnSc1
</s>
<s>
1911	#num#	k4
-	-	kIx~
Vyloučení	vyloučení	k1gNnSc1
z	z	k7c2
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězství	vítězství	k1gNnSc1
v	v	k7c6
Copa	Cop	k1gInSc2
dels	delsa	k1gFnPc2
Pirineus	Pirineus	k1gMnSc1
</s>
<s>
1912	#num#	k4
-	-	kIx~
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězství	vítězství	k1gNnSc1
v	v	k7c6
Copa	Cop	k1gInSc2
dels	delsa	k1gFnPc2
Pirineus	Pirineus	k1gMnSc1
</s>
<s>
1913	#num#	k4
-	-	kIx~
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
vyhrává	vyhrávat	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
skupinu	skupina	k1gFnSc4
a	a	k8xC
o	o	k7c4
pohár	pohár	k1gInSc4
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
s	s	k7c7
Real	Real	k1gInSc1
Unión	Unión	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězství	vítězství	k1gNnSc1
v	v	k7c6
Copa	Cop	k1gInSc2
dels	delsa	k1gFnPc2
Pirineus	Pirineus	k1gMnSc1
</s>
<s>
1914	#num#	k4
a	a	k8xC
1915	#num#	k4
-	-	kIx~
neúčast	neúčast	k1gFnSc1
v	v	k7c6
kterékoliv	kterýkoliv	k3yIgFnSc6
soutěži	soutěž	k1gFnSc6
</s>
<s>
1916	#num#	k4
-	-	kIx~
Semifinále	semifinále	k1gNnSc6
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
</s>
<s>
1919	#num#	k4
-	-	kIx~
Finále	finále	k1gNnSc1
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
</s>
<s>
1920	#num#	k4
-	-	kIx~
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1922	#num#	k4
-	-	kIx~
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavnostně	slavnostně	k6eAd1
otevřen	otevřít	k5eAaPmNgInS
stadion	stadion	k1gInSc1
Les	les	k1gInSc1
Corts	Corts	k1gInSc1
</s>
<s>
1924	#num#	k4
-	-	kIx~
Semifinále	semifinále	k1gNnSc6
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
</s>
<s>
1925	#num#	k4
-	-	kIx~
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1926	#num#	k4
-	-	kIx~
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1927	#num#	k4
-	-	kIx~
Semifinále	semifinále	k1gNnSc6
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
</s>
<s>
1928	#num#	k4
-	-	kIx~
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
třetím	třetí	k4xOgInSc6
opakovaném	opakovaný	k2eAgNnSc6d1
finále	finále	k1gNnSc6
s	s	k7c7
Real	Real	k1gInSc4
Sociedad	Sociedad	k1gInSc1
</s>
<s>
Založena	založen	k2eAgFnSc1d1
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1928-29	1928-29	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1929-30	1929-30	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1930-31	1930-31	k4
-	-	kIx~
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1931-32	1931-32	k4
-	-	kIx~
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Copa	Copa	k1gMnSc1
della	della	k1gMnSc1
Repubblica	Repubblica	k1gMnSc1
(	(	kIx(
<g/>
Copa	Copa	k1gMnSc1
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
1932-33	1932-33	k4
-	-	kIx~
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1933-34	1933-34	k4
-	-	kIx~
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1934-35	1934-35	k4
-	-	kIx~
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1935-36	1935-36	k4
-	-	kIx~
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Copa	Copa	k1gMnSc1
della	della	k1gMnSc1
Repubblica	Repubblica	k1gMnSc1
(	(	kIx(
<g/>
Copa	Copa	k1gMnSc1
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
1939-40	1939-40	k4
-	-	kIx~
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1940-41	1940-41	k4
-	-	kIx~
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1941-42	1941-42	k4
-	-	kIx~
12	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
del	del	k?
Generalísimo	Generalísima	k1gFnSc5
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1942-43	1942-43	k4
-	-	kIx~
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1943-44	1943-44	k4
-	-	kIx~
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1944-45	1944-45	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1945-46	1945-46	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítěz	vítěz	k1gMnSc1
Copa	Copa	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Or	Or	k1gFnSc1
Argentina	Argentina	k1gFnSc1
</s>
<s>
1946-47	1946-47	k4
-	-	kIx~
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1947-48	1947-48	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1948-49	1948-49	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítěz	vítěz	k1gMnSc1
Copa	Copa	k1gMnSc1
Eva	Eva	k1gFnSc1
Duarte	Duart	k1gInSc5
</s>
<s>
1949-50	1949-50	k4
-	-	kIx~
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1950-51	1950-51	k4
-	-	kIx~
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
del	del	k?
Generalísimo	Generalísima	k1gFnSc5
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1951-52	1951-52	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
del	del	k?
Generalísimo	Generalísima	k1gFnSc5
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítěz	vítěz	k1gMnSc1
Copa	Copa	k1gMnSc1
Eva	Eva	k1gFnSc1
Duarte	Duart	k1gInSc5
</s>
<s>
1952-53	1952-53	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
del	del	k?
Generalísimo	Generalísima	k1gFnSc5
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítěz	vítěz	k1gMnSc1
Copa	Copa	k1gMnSc1
Eva	Eva	k1gFnSc1
Duarte	Duart	k1gMnSc5
</s>
<s>
1953-54	1953-54	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Copa	Copa	k1gMnSc1
del	del	k?
Generalísimo	Generalísima	k1gFnSc5
</s>
<s>
1954-55	1954-55	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1955-56	1955-56	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1956-57	1956-57	k4
-	-	kIx~
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
del	del	k?
Generalísimo	Generalísima	k1gFnSc5
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1957-58	1957-58	k4
-	-	kIx~
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítěz	vítěz	k1gMnSc1
Pohár	pohár	k1gInSc4
veletržních	veletržní	k2eAgNnPc2d1
měst	město	k1gNnPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
vítězství	vítězství	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
1958-59	1958-59	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
del	del	k?
Generalísimo	Generalísima	k1gFnSc5
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1959-60	1959-60	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítěz	vítěz	k1gMnSc1
Pohár	pohár	k1gInSc4
veletržních	veletržní	k2eAgNnPc2d1
měst	město	k1gNnPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
vítězství	vítězství	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
1960-61	1960-61	k4
-	-	kIx~
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Poháru	pohár	k1gInSc2
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1961-62	1961-62	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Pohár	pohár	k1gInSc4
veletržních	veletržní	k2eAgNnPc2d1
měst	město	k1gNnPc2
</s>
<s>
1962-63	1962-63	k4
-	-	kIx~
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
del	del	k?
Generalísimo	Generalísima	k1gFnSc5
(	(	kIx(
<g/>
15	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1963-64	1963-64	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1964-65	1964-65	k4
-	-	kIx~
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1965-66	1965-66	k4
-	-	kIx~
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítěz	vítěz	k1gMnSc1
Pohár	pohár	k1gInSc4
veletržních	veletržní	k2eAgNnPc2d1
měst	město	k1gNnPc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
vítězství	vítězství	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
1966-67	1966-67	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1967-68	1967-68	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
del	del	k?
Generalísimo	Generalísima	k1gFnSc5
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1968-69	1968-69	k4
-	-	kIx~
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Poháru	pohár	k1gInSc2
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1969-70	1969-70	k4
-	-	kIx~
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1970-71	1970-71	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
del	del	k?
Generalísimo	Generalísima	k1gFnSc5
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1971-72	1971-72	k4
-	-	kIx~
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1972-73	1972-73	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1973-74	1973-74	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Copa	Copa	k1gMnSc1
del	del	k?
Generalísimo	Generalísima	k1gFnSc5
</s>
<s>
1974-75	1974-75	k4
-	-	kIx~
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1975-76	1975-76	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1976-77	1976-77	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1977-78	1977-78	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1978-79	1978-79	k4
-	-	kIx~
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítěz	vítěz	k1gMnSc1
Pohár	pohár	k1gInSc4
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
vítězství	vítězství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Superpoháru	superpohár	k1gInSc2
UEFA	UEFA	kA
</s>
<s>
1979-80	1979-80	k4
-	-	kIx~
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1980-81	1980-81	k4
-	-	kIx~
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
(	(	kIx(
<g/>
19	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1981-82	1981-82	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítěz	vítěz	k1gMnSc1
Pohár	pohár	k1gInSc4
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
vítězství	vítězství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Superpoháru	superpohár	k1gInSc2
UEFA	UEFA	kA
</s>
<s>
1982-83	1982-83	k4
-	-	kIx~
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
de	de	k?
la	la	k1gNnSc1
Liga	liga	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1983-84	1983-84	k4
-	-	kIx~
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Copa	Copa	k1gMnSc1
del	del	k?
Rey	Rea	k1gFnSc2
</s>
<s>
1984-85	1984-85	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Supercopa	Supercop	k1gMnSc2
de	de	k?
Españ	Españ	k1gMnSc2
</s>
<s>
1985-86	1985-86	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
de	de	k?
la	la	k1gNnSc1
Liga	liga	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Poháru	pohár	k1gInSc2
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Copa	Copa	k1gMnSc1
del	del	k?
Rey	Rea	k1gFnSc2
</s>
<s>
1986-87	1986-87	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1987-88	1987-88	k4
-	-	kIx~
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Supercopa	Supercop	k1gMnSc2
de	de	k?
Españ	Españ	k1gMnSc2
</s>
<s>
1988-89	1988-89	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítěz	vítěz	k1gMnSc1
Pohár	pohár	k1gInSc4
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
vítězství	vítězství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Superpoháru	superpohár	k1gInSc2
UEFA	UEFA	kA
</s>
<s>
1989-90	1989-90	k4
-	-	kIx~
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Supercopa	Supercop	k1gMnSc2
de	de	k?
Españ	Españ	k1gMnSc2
</s>
<s>
1990-91	1990-91	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Poháru	pohár	k1gInSc2
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
1991-92	1991-92	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gFnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítěz	vítěz	k1gMnSc1
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
vítězství	vítězství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Interkontinentálního	interkontinentální	k2eAgInSc2d1
poháru	pohár	k1gInSc2
</s>
<s>
1992-93	1992-93	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Supercopa	Supercop	k1gMnSc2
de	de	k?
Españ	Españ	k1gMnSc2
</s>
<s>
1993-94	1993-94	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gFnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
1994-95	1994-95	k4
-	-	kIx~
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
1995-96	1995-96	k4
-	-	kIx~
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gFnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Copa	Copa	k1gMnSc1
del	del	k?
Rey	Rea	k1gFnSc2
</s>
<s>
1996-97	1996-97	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítěz	vítěz	k1gMnSc1
Pohár	pohár	k1gInSc4
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
vítězství	vítězství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
vítězství	vítězství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Supercopa	Supercop	k1gMnSc2
de	de	k?
Españ	Españ	k1gMnSc2
</s>
<s>
1997-98	1997-98	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
15	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
(	(	kIx(
<g/>
24	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Supercopa	Supercop	k1gMnSc2
de	de	k?
Españ	Españ	k1gMnSc2
</s>
<s>
1998-99	1998-99	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Supercopa	Supercop	k1gMnSc2
de	de	k?
Españ	Españ	k1gMnSc2
</s>
<s>
1999	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
2000-01	2000-01	k4
-	-	kIx~
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
2001-02	2001-02	k4
-	-	kIx~
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
2002-03	2002-03	k4
-	-	kIx~
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
2003-04	2003-04	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
2004-05	2004-05	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gFnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
2005-06	2005-06	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gFnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítěz	vítěz	k1gMnSc1
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Superpoháru	superpohár	k1gInSc2
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
</s>
<s>
2006-07	2006-07	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
2007-08	2007-08	k4
-	-	kIx~
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
</s>
<s>
2008-09	2008-09	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
19	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gFnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítěz	vítěz	k1gMnSc1
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
vítězství	vítězství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
vítězství	vítězství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
2009-10	2009-10	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gFnSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
2010-11	2010-11	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítěz	vítěz	k1gMnSc1
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finalista	finalista	k1gMnSc1
Copa	Copa	k1gMnSc1
del	del	k?
Rey	Rea	k1gFnSc2
</s>
<s>
2011-12	2011-12	k4
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gFnSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
vítězství	vítězství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
vítězství	vítězství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
2012-13	2012-13	k4
-	-	kIx~
Mistr	mistr	k1gMnSc1
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gFnSc1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
Úspěchy	úspěch	k1gInPc1
A	a	k8xC
<g/>
–	–	k?
<g/>
týmu	tým	k1gInSc2
</s>
<s>
Kontinentální	kontinentální	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
/	/	kIx~
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
evropská	evropský	k2eAgFnSc1d1
výkonnostní	výkonnostní	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
<g/>
;	;	kIx,
5	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
evropská	evropský	k2eAgFnSc1d1
výkonnostní	výkonnostní	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
<g/>
;	;	kIx,
4	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
evropská	evropský	k2eAgFnSc1d1
výkonnostní	výkonnostní	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
<g/>
;	;	kIx,
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
<g/>
,	,	kIx,
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
</s>
<s>
Latinský	latinský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
elitní	elitní	k2eAgFnSc1d1
poválečná	poválečný	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
<g/>
;	;	kIx,
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1949	#num#	k4
<g/>
,	,	kIx,
1952	#num#	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
(	(	kIx(
<g/>
5	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1979	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
</s>
<s>
Interkontinentální	interkontinentální	k2eAgInSc1d1
pohár	pohár	k1gInSc1
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1992	#num#	k4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
2006	#num#	k4
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
fotbalová	fotbalový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
(	(	kIx(
<g/>
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
<g/>
;	;	kIx,
26	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1929	#num#	k4
<g/>
,	,	kIx,
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
<g/>
,	,	kIx,
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
<g/>
,	,	kIx,
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
<g/>
,	,	kIx,
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
<g/>
,	,	kIx,
1959	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
60	#num#	k4
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
98	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
<g/>
,	,	kIx,
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
,	,	kIx,
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
<g/>
,	,	kIx,
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
<g/>
,	,	kIx,
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
64	#num#	k4
<g/>
,	,	kIx,
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
<g/>
,	,	kIx,
1942	#num#	k4
<g/>
/	/	kIx~
<g/>
43	#num#	k4
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
<g/>
,	,	kIx,
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Španělský	španělský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
Copa	Copa	k1gFnSc1
del	del	k?
Generalísimo	Generalísima	k1gFnSc5
<g/>
,	,	kIx,
Copa	Copa	k1gMnSc1
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
;	;	kIx,
30	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1910	#num#	k4
<g/>
,	,	kIx,
1912	#num#	k4
<g/>
,	,	kIx,
1913	#num#	k4
<g/>
,	,	kIx,
1920	#num#	k4
<g/>
,	,	kIx,
1922	#num#	k4
<g/>
,	,	kIx,
1925	#num#	k4
<g/>
,	,	kIx,
1926	#num#	k4
<g/>
,	,	kIx,
1928	#num#	k4
<g/>
,	,	kIx,
1942	#num#	k4
<g/>
,	,	kIx,
1951	#num#	k4
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
<g/>
,	,	kIx,
1957	#num#	k4
<g/>
,	,	kIx,
1958	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
59	#num#	k4
<g/>
,	,	kIx,
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
88	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
Španělský	španělský	k2eAgInSc1d1
ligový	ligový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
Copa	Copa	k1gFnSc1
de	de	k?
la	la	k1gNnSc1
Liga	liga	k1gFnSc1
<g/>
;	;	kIx,
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
</s>
<s>
Španělský	španělský	k2eAgInSc1d1
Superpohár	superpohár	k1gInSc1
(	(	kIx(
<g/>
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gFnSc1
<g/>
;	;	kIx,
13	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1983	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
</s>
<s>
Menší	malý	k2eAgInPc1d2
úspěchy	úspěch	k1gInPc1
</s>
<s>
Pyrenees	Pyrenees	k1gInSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1910	#num#	k4
<g/>
,	,	kIx,
1911	#num#	k4
<g/>
,	,	kIx,
1912	#num#	k4
<g/>
,	,	kIx,
1913	#num#	k4
</s>
<s>
Copa	Cop	k2eAgFnSc1d1
de	de	k?
Oro	Oro	k1gFnSc1
Argentina	Argentina	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1945	#num#	k4
</s>
<s>
Copa	Cop	k2eAgFnSc1d1
Eva	Eva	k1gFnSc1
Duarte	Duart	k1gInSc5
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1948	#num#	k4
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
,	,	kIx,
1953	#num#	k4
</s>
<s>
Pequeñ	Pequeñ	k2eAgFnSc1d1
Copa	Copa	k1gFnSc1
del	del	k?
Mundo	Mundo	k1gNnSc1
de	de	k?
Clubes	Clubes	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1957	#num#	k4
</s>
<s>
Campionat	Campionat	k5eAaPmF,k5eAaImF
de	de	k?
Catalunya	Catalunyum	k1gNnPc4
(	(	kIx(
<g/>
23	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1902	#num#	k4
<g/>
,	,	kIx,
1903	#num#	k4
<g/>
,	,	kIx,
1904	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
1908	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
1909	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
1910	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
1912	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
,	,	kIx,
1915	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
,	,	kIx,
1918	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
,	,	kIx,
1919	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
,	,	kIx,
1920	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
<g/>
,	,	kIx,
1921	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
<g/>
,	,	kIx,
1923	#num#	k4
<g/>
/	/	kIx~
<g/>
24	#num#	k4
<g/>
,	,	kIx,
1924	#num#	k4
<g/>
/	/	kIx~
<g/>
25	#num#	k4
<g/>
,	,	kIx,
1925	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
26	#num#	k4
<g/>
,	,	kIx,
1926	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
<g/>
,	,	kIx,
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
<g/>
,	,	kIx,
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
<g/>
,	,	kIx,
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
<g/>
,	,	kIx,
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
<g/>
,	,	kIx,
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
<g/>
,	,	kIx,
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
,	,	kIx,
1937	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1901	#num#	k4
<g/>
,	,	kIx,
1907	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
1911	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
<g/>
,	,	kIx,
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
</s>
<s>
Mediterranean	Mediterranean	k1gInSc1
League	Leagu	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1937	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
</s>
<s>
Copa	Cop	k2eAgFnSc1d1
Catalunya	Catalunya	k1gFnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
(	(	kIx(
<g/>
kombinovaně	kombinovaně	k6eAd1
s	s	k7c7
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
„	„	k?
<g/>
B	B	kA
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Catalunya	Catalunya	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Trofeo	Trofeo	k6eAd1
Teresa	Teresa	k1gFnSc1
Herrera	Herrera	k1gFnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1948	#num#	k4
<g/>
,	,	kIx,
1951	#num#	k4
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
</s>
<s>
Copa	Copa	k1gMnSc1
Martini	martini	k1gNnSc2
&	&	k?
Rossi	Ross	k1gMnPc7
(	(	kIx(
<g/>
6	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1948	#num#	k4
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1949	#num#	k4
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1950	#num#	k4
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Červen	červen	k1gInSc1
1952	#num#	k4
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Prosinec	prosinec	k1gInSc1
1952	#num#	k4
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1953	#num#	k4
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Trofeo	Trofeo	k6eAd1
Ramón	Ramón	k1gInSc1
de	de	k?
Carranza	Carranz	k1gMnSc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1961	#num#	k4
<g/>
,	,	kIx,
1962	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
</s>
<s>
Trofeu	Trofeu	k6eAd1
Joan	Joan	k1gMnSc1
Gamper	Gamper	k1gMnSc1
(	(	kIx(
<g/>
42	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1966	#num#	k4
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
,	,	kIx,
1986	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
</s>
<s>
Coupe	coup	k1gInSc5
Mohammed	Mohammed	k1gMnSc1
V	V	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1969	#num#	k4
</s>
<s>
Trofeo	Trofeo	k1gMnSc1
Festa	Festa	k?
d	d	k?
<g/>
'	'	kIx"
<g/>
Elx	Elx	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1970	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
</s>
<s>
Trofeo	Trofeo	k1gMnSc1
Costa	Costa	k1gMnSc1
del	del	k?
Sol	sol	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1977	#num#	k4
</s>
<s>
Trofeo	Trofeo	k6eAd1
Ciudad	Ciudad	k1gInSc1
de	de	k?
Alicante	Alicant	k1gMnSc5
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1983	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
</s>
<s>
Trofeo	Trofeo	k6eAd1
Ciudad	Ciudad	k1gInSc1
de	de	k?
Zaragoza	Zaragoza	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1985	#num#	k4
</s>
<s>
Trofeo	Trofeo	k1gMnSc1
Naranja	Naranja	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1987	#num#	k4
</s>
<s>
Torneig	Torneig	k1gMnSc1
Ciutat	Ciutat	k1gMnSc1
de	de	k?
Barcelona	Barcelona	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1989	#num#	k4
</s>
<s>
Trofeo	Trofeo	k6eAd1
Ciudad	Ciudad	k1gInSc1
de	de	k?
Marbella	Marbella	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1993	#num#	k4
</s>
<s>
Trofeo	Trofeo	k1gMnSc1
Ciutat	Ciutat	k1gMnSc1
de	de	k?
Lleida	Lleida	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1998	#num#	k4
</s>
<s>
Amsterdam	Amsterdam	k1gInSc1
Tournament	Tournament	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
2000	#num#	k4
</s>
<s>
Franz	Franz	k1gMnSc1
Beckenbauer	Beckenbauer	k1gMnSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
2007	#num#	k4
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Audi	Audi	k1gNnSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
2011	#num#	k4
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tournoi	Tournoi	k6eAd1
de	de	k?
Paris	Paris	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
2012	#num#	k4
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Trofeo	Trofeo	k6eAd1
Colombino	Colombin	k2eAgNnSc1d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
2014	#num#	k4
</s>
<s>
International	Internationat	k5eAaPmAgInS,k5eAaImAgInS
Champions	Champions	k1gInSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
2017	#num#	k4
USA	USA	kA
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
La	la	k1gNnSc1
Liga-Serie	Liga-Serie	k1gFnSc2
A	a	k9
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
2019	#num#	k4
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úspěchy	úspěch	k1gInPc1
mládeže	mládež	k1gFnSc2
</s>
<s>
Kontinentální	kontinentální	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Juniorská	juniorský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
evropská	evropský	k2eAgFnSc1d1
výkonnostní	výkonnostní	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
<g/>
;	;	kIx,
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
fotbalová	fotbalový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
do	do	k7c2
19	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
ligová	ligový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
/	/	kIx~
od	od	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
jako	jako	k8xS,k8xC
turnaj	turnaj	k1gInSc1
Copa	Cop	k1gInSc2
de	de	k?
Campeones	Campeones	k1gMnSc1
<g/>
;	;	kIx,
4	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
ligová	ligový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
do	do	k7c2
19	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
kvalifikace	kvalifikace	k1gFnSc2
do	do	k7c2
soutěže	soutěž	k1gFnSc2
Copa	Copa	k1gMnSc1
de	de	k?
Campeones	Campeones	k1gMnSc1
<g/>
;	;	kIx,
11	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
3	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
(	(	kIx(
<g/>
sk	sk	k?
<g/>
.	.	kIx.
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Španělský	španělský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
do	do	k7c2
19	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
Juvenil	Juvenil	k1gFnSc2
<g/>
;	;	kIx,
18	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
1951	#num#	k4
<g/>
,	,	kIx,
1959	#num#	k4
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
</s>
<s>
Hráči	hráč	k1gMnPc1
</s>
<s>
Všechny	všechen	k3xTgMnPc4
hráče	hráč	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
kdy	kdy	k6eAd1
hráli	hrát	k5eAaImAgMnP
za	za	k7c2
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
a	a	k8xC
na	na	k7c6
české	český	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
Wikipedie	Wikipedie	k1gFnSc2
mají	mít	k5eAaImIp3nP
existující	existující	k2eAgInSc4d1
článek	článek	k1gInSc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
Kategorii	kategorie	k1gFnSc6
<g/>
:	:	kIx,
<g/>
Fotbalisté	fotbalista	k1gMnPc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
</s>
<s>
Současná	současný	k2eAgFnSc1d1
soupiska	soupiska	k1gFnSc1
</s>
<s>
Španělské	španělský	k2eAgInPc1d1
kluby	klub	k1gInPc1
jsou	být	k5eAaImIp3nP
limitovány	limitovat	k5eAaBmNgInP
pravidly	pravidlo	k1gNnPc7
nařizující	nařizující	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
každý	každý	k3xTgInSc1
klub	klub	k1gInSc1
smí	smět	k5eAaImIp3nS
mít	mít	k5eAaImF
na	na	k7c6
soupisce	soupiska	k1gFnSc6
pouze	pouze	k6eAd1
tři	tři	k4xCgMnPc4
hráče	hráč	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
mají	mít	k5eAaImIp3nP
občanství	občanství	k1gNnSc4
ve	v	k7c6
státech	stát	k1gInPc6
mimo	mimo	k7c4
Evropskou	evropský	k2eAgFnSc4d1
unii	unie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
pravidlo	pravidlo	k1gNnSc1
se	se	k3xPyFc4
nevztahuje	vztahovat	k5eNaImIp3nS
na	na	k7c4
hráče	hráč	k1gMnPc4
s	s	k7c7
dvojím	dvojí	k4xRgInSc7
občanstvím	občanství	k1gNnSc7
a	a	k8xC
na	na	k7c4
hráče	hráč	k1gMnPc4
z	z	k7c2
tzv.	tzv.	kA
AKT	akt	k1gInSc1
(	(	kIx(
<g/>
Africká	africký	k2eAgFnSc1d1
<g/>
,	,	kIx,
Karibská	karibský	k2eAgFnSc1d1
a	a	k8xC
Pacifická	pacifický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
států	stát	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Marc-André	Marc-Andrý	k2eAgNnSc4d1
ter	ter	k?
Stegen	Stegen	k1gInSc4
</s>
<s>
2	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Sergiñ	Sergiñ	k1gMnSc1
Dest	Dest	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Gerard	Gerard	k1gInSc1
Piqué	Piquá	k1gFnSc2
(	(	kIx(
<g/>
třetí	třetí	k4xOgMnSc1
kapitán	kapitán	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Ronald	Ronald	k1gMnSc1
Araújo	Araújo	k1gMnSc1
</s>
<s>
5	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Sergio	Sergio	k1gNnSc1
Busquets	Busquetsa	k1gFnPc2
(	(	kIx(
<g/>
zástupce	zástupce	k1gMnSc2
kapitána	kapitán	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
7	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Antoine	Antoinout	k5eAaPmIp3nS
Griezmann	Griezmann	k1gInSc1
</s>
<s>
8	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Miralem	Miral	k1gInSc7
Pjanić	Pjanić	k1gFnSc2
</s>
<s>
9	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Martin	Martin	k1gMnSc1
Braithwaite	Braithwait	k1gMnSc5
</s>
<s>
10	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Lionel	Lionet	k5eAaImAgMnS,k5eAaPmAgMnS,k5eAaBmAgMnS
Messi	Messe	k1gFnSc4
(	(	kIx(
<g/>
kapitán	kapitán	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
11	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Ousmane	Ousman	k1gMnSc5
Dembélé	Dembélý	k2eAgMnPc4d1
</s>
<s>
12	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Riqui	Riqui	k6eAd1
Puig	Puig	k1gInSc1
</s>
<s>
13	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Neto	Neto	k1gMnSc1
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
14	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Philippe	Philipp	k1gMnSc5
Coutinho	Coutinha	k1gMnSc5
</s>
<s>
15	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Clément	Clément	k1gInSc1
Lenglet	Lenglet	k1gInSc1
</s>
<s>
16	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Pedri	Pedri	k6eAd1
</s>
<s>
17	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Francisco	Francisco	k1gMnSc1
Trincã	Trincã	k1gMnSc1
</s>
<s>
18	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Jordi	Jord	k1gMnPc1
Alba	album	k1gNnSc2
</s>
<s>
19	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Matheus	Matheus	k1gMnSc1
Fernandes	Fernandes	k1gMnSc1
</s>
<s>
20	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Sergi	Serg	k1gFnSc5
Roberto	Roberta	k1gFnSc5
(	(	kIx(
<g/>
čtvrtý	čtvrtý	k4xOgMnSc1
kapitán	kapitán	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
21	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Frenkie	Frenkie	k1gFnSc1
de	de	k?
Jong	Jong	k1gMnSc1
</s>
<s>
22	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Ansu	Ans	k2eAgFnSc4d1
Fati	Fate	k1gFnSc4
</s>
<s>
23	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Samuel	Samuel	k1gMnSc1
Umtiti	Umtit	k1gMnPc1
</s>
<s>
24	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Junior	junior	k1gMnSc1
Firpo	Firpa	k1gFnSc5
</s>
<s>
Hráči	hráč	k1gMnPc1
na	na	k7c6
hostování	hostování	k1gNnSc6
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
—	—	k?
</s>
<s>
B	B	kA
</s>
<s>
Álex	Álex	k1gInSc1
Ruiz	Ruiz	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
L	L	kA
<g/>
'	'	kIx"
<g/>
Hospitalet	Hospitalet	k1gInSc1
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
—	—	k?
</s>
<s>
O	o	k7c6
</s>
<s>
Sergio	Sergio	k1gMnSc1
Akieme	Akiem	k1gInSc5
(	(	kIx(
<g/>
v	v	k7c6
Almeríi	Almerí	k1gFnSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
—	—	k?
</s>
<s>
O	o	k7c6
</s>
<s>
Juan	Juan	k1gMnSc1
Miranda	Mirando	k1gNnSc2
(	(	kIx(
<g/>
v	v	k7c6
Realu	Real	k1gInSc6
Betis	Betis	k1gFnSc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
—	—	k?
</s>
<s>
O	o	k7c6
</s>
<s>
Jean-Clair	Jean-Clair	k1gMnSc1
Todibo	Todiba	k1gFnSc5
(	(	kIx(
<g/>
v	v	k7c6
Benfice	Benfika	k1gFnSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2022	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
—	—	k?
</s>
<s>
O	o	k7c6
</s>
<s>
Emerson	Emerson	k1gNnSc1
(	(	kIx(
<g/>
v	v	k7c6
Realu	Real	k1gInSc6
Betis	Betis	k1gFnSc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
—	—	k?
</s>
<s>
O	o	k7c6
</s>
<s>
Josep	Josepit	k5eAaPmRp2nS
Jaume	Jaum	k1gMnSc5
(	(	kIx(
<g/>
v	v	k7c6
Badaloně	Badalona	k1gFnSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
—	—	k?
</s>
<s>
O	o	k7c6
</s>
<s>
Moussa	Moussa	k1gFnSc1
Wagué	Waguá	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
PAOKu	PAOKum	k1gNnSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
—	—	k?
</s>
<s>
Z	z	k7c2
</s>
<s>
Monchu	Moncha	k1gFnSc4
(	(	kIx(
<g/>
v	v	k7c6
Gironě	Girona	k1gFnSc6
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
—	—	k?
</s>
<s>
Z	z	k7c2
</s>
<s>
Ludovit	Ludovit	k5eAaPmF
Reis	Reis	k1gInSc4
(	(	kIx(
<g/>
v	v	k7c6
VfL	VfL	k1gFnSc6
Osnabrücku	Osnabrück	k1gInSc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
—	—	k?
</s>
<s>
Z	z	k7c2
</s>
<s>
Carles	Carles	k1gInSc1
Aleñ	Aleñ	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
Getafe	Getaf	k1gInSc5
CF	CF	kA
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Realizační	realizační	k2eAgInSc1d1
tým	tým	k1gInSc1
</s>
<s>
Trenérský	trenérský	k2eAgInSc1d1
tým	tým	k1gInSc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Ronald	Ronald	k1gMnSc1
Koeman	Koeman	k1gMnSc1
</s>
<s>
Asistent	asistent	k1gMnSc1
trenéra	trenér	k1gMnSc2
</s>
<s>
Alfred	Alfred	k1gMnSc1
Schreuder	Schreuder	k1gMnSc1
</s>
<s>
Henrik	Henrik	k1gMnSc1
Larsson	Larsson	k1gMnSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
brankářů	brankář	k1gMnPc2
</s>
<s>
José	José	k6eAd1
Ramón	Ramón	k1gInSc1
de	de	k?
la	la	k1gNnSc7
Fuente	Fuent	k1gInSc5
</s>
<s>
Kondiční	kondiční	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Albert	Albert	k1gMnSc1
Roca	Roca	k1gMnSc1
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1
mládeže	mládež	k1gFnSc2
</s>
<s>
Patrick	Patrick	k1gMnSc1
Kluivert	Kluivert	k1gMnSc1
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
sezonách	sezona	k1gFnPc6
</s>
<s>
Stručný	stručný	k2eAgInSc1d1
přehled	přehled	k1gInSc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1929	#num#	k4
<g/>
–	–	k?
:	:	kIx,
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1
ročníky	ročník	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
Z	z	k7c2
–	–	k?
zápasy	zápas	k1gInPc4
<g/>
,	,	kIx,
V	v	k7c6
–	–	k?
výhry	výhra	k1gFnPc1
<g/>
,	,	kIx,
R	R	kA
–	–	k?
remízy	remíza	k1gFnPc4
<g/>
,	,	kIx,
P	P	kA
–	–	k?
porážky	porážka	k1gFnSc2
<g/>
,	,	kIx,
VG	VG	kA
–	–	k?
vstřelené	vstřelený	k2eAgInPc1d1
góly	gól	k1gInPc1
<g/>
,	,	kIx,
OG	OG	kA
–	–	k?
obdržené	obdržený	k2eAgInPc4d1
góly	gól	k1gInPc4
<g/>
,	,	kIx,
+	+	kIx~
<g/>
/	/	kIx~
<g/>
−	−	k?
–	–	k?
rozdíl	rozdíl	k1gInSc1
skóre	skóre	k1gNnSc2
<g/>
,	,	kIx,
B	B	kA
–	–	k?
body	bod	k1gInPc1
<g/>
,	,	kIx,
zlaté	zlatý	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
<g/>
,	,	kIx,
stříbrné	stříbrný	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
<g/>
,	,	kIx,
bronzové	bronzový	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
červené	červený	k2eAgNnSc4d1
podbarvení	podbarvení	k1gNnSc4
–	–	k?
sestup	sestup	k1gInSc1
<g/>
,	,	kIx,
zelené	zelený	k2eAgNnSc4d1
podbarvení	podbarvení	k1gNnSc4
–	–	k?
postup	postup	k1gInSc1
<g/>
,	,	kIx,
fialové	fialový	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
reorganizace	reorganizace	k1gFnSc1
<g/>
,	,	kIx,
změna	změna	k1gFnSc1
skupiny	skupina	k1gFnSc2
či	či	k8xC
soutěže	soutěž	k1gFnSc2
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
(	(	kIx(
<g/>
1929	#num#	k4
–	–	k?
)	)	kIx)
</s>
<s>
Sezóny	sezóna	k1gFnPc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
1929	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
11811343723	#num#	k4
<g/>
+	+	kIx~
<g/>
1425	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
11811164636	#num#	k4
<g/>
+	+	kIx~
<g/>
1023	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1187744043-321	1187744043-321	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
11810444026	#num#	k4
<g/>
+	+	kIx~
<g/>
1424	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1187564234	#num#	k4
<g/>
+	+	kIx~
<g/>
819	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
11880104240	#num#	k4
<g/>
+	+	kIx~
<g/>
216	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1229675544	#num#	k4
<g/>
+	+	kIx~
<g/>
1124	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12211293932	#num#	k4
<g/>
+	+	kIx~
<g/>
724	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Španělské	španělský	k2eAgFnPc1d1
soutěže	soutěž	k1gFnPc1
se	se	k3xPyFc4
v	v	k7c6
letech	let	k1gInPc6
1936	#num#	k4
<g/>
–	–	k?
<g/>
39	#num#	k4
nehrály	hrát	k5eNaImAgInP
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
kvůli	kvůli	k7c3
právě	právě	k9
probíhající	probíhající	k2eAgFnSc3d1
občanské	občanský	k2eAgFnSc3d1
válce	válka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12283113238-619	12283113238-619	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12213185545	#num#	k4
<g/>
+	+	kIx~
<g/>
1027	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12683155766-919	12683155766-919	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1942	#num#	k4
<g/>
/	/	kIx~
<g/>
43	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12614487750	#num#	k4
<g/>
+	+	kIx~
<g/>
2732	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12610885946	#num#	k4
<g/>
+	+	kIx~
<g/>
1328	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12617545030	#num#	k4
<g/>
+	+	kIx~
<g/>
2039	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12614754831	#num#	k4
<g/>
+	+	kIx~
<g/>
1735	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12614395942	#num#	k4
<g/>
+	+	kIx~
<g/>
1731	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12615746531	#num#	k4
<g/>
+	+	kIx~
<g/>
3437	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
12616556636	#num#	k4
<g/>
+	+	kIx~
<g/>
3037	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
126133106747	#num#	k4
<g/>
+	+	kIx~
<g/>
2029	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
130163118361	#num#	k4
<g/>
+	+	kIx~
<g/>
2235	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13019569243	#num#	k4
<g/>
+	+	kIx~
<g/>
4943	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13019478243	#num#	k4
<g/>
+	+	kIx~
<g/>
3942	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
130164107439	#num#	k4
<g/>
+	+	kIx~
<g/>
3536	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13017767539	#num#	k4
<g/>
+	+	kIx~
<g/>
3641	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13022356726	#num#	k4
<g/>
+	+	kIx~
<g/>
4147	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13016777037	#num#	k4
<g/>
+	+	kIx~
<g/>
3339	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13017496938	#num#	k4
<g/>
+	+	kIx~
<g/>
3138	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13024339626	#num#	k4
<g/>
+	+	kIx~
<g/>
7051	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13022268828	#num#	k4
<g/>
+	+	kIx~
<g/>
6046	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
130136116247	#num#	k4
<g/>
+	+	kIx~
<g/>
1532	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13018488146	#num#	k4
<g/>
+	+	kIx~
<g/>
3540	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
130119104536	#num#	k4
<g/>
+	+	kIx~
<g/>
931	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13019477438	#num#	k4
<g/>
+	+	kIx~
<g/>
3642	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
130144125941	#num#	k4
<g/>
+	+	kIx~
<g/>
1832	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13016685127	#num#	k4
<g/>
+	+	kIx~
<g/>
2438	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13020285829	#num#	k4
<g/>
+	+	kIx~
<g/>
2942	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13015964829	#num#	k4
<g/>
+	+	kIx~
<g/>
1939	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
130131074018	#num#	k4
<g/>
+	+	kIx~
<g/>
2236	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13013984031	#num#	k4
<g/>
+	+	kIx~
<g/>
935	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13019565022	#num#	k4
<g/>
+	+	kIx~
<g/>
2843	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13417984026	#num#	k4
<g/>
+	+	kIx~
<g/>
1443	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
134181064121	#num#	k4
<g/>
+	+	kIx~
<g/>
2046	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13421857524	#num#	k4
<g/>
+	+	kIx~
<g/>
5150	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
134157125736	#num#	k4
<g/>
+	+	kIx~
<g/>
2137	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13418796141	#num#	k4
<g/>
+	+	kIx~
<g/>
2043	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13418976934	#num#	k4
<g/>
+	+	kIx~
<g/>
3545	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13416994929	#num#	k4
<g/>
+	+	kIx~
<g/>
2041	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
134166126937	#num#	k4
<g/>
+	+	kIx~
<g/>
3238	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
134131294233	#num#	k4
<g/>
+	+	kIx~
<g/>
938	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
134185116641	#num#	k4
<g/>
+	+	kIx~
<g/>
2541	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13419787540	#num#	k4
<g/>
+	+	kIx~
<g/>
3545	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
134171076029	#num#	k4
<g/>
+	+	kIx~
<g/>
3144	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13420866228	#num#	k4
<g/>
+	+	kIx~
<g/>
3448	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
134211126925	#num#	k4
<g/>
+	+	kIx~
<g/>
4453	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13418976136	#num#	k4
<g/>
+	+	kIx~
<g/>
2545	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
144241556329	#num#	k4
<g/>
+	+	kIx~
<g/>
3463	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138159144944	#num#	k4
<g/>
+	+	kIx~
<g/>
539	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138231148026	#num#	k4
<g/>
+	+	kIx~
<g/>
5457	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138235108339	#num#	k4
<g/>
+	+	kIx~
<g/>
4451	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13825767433	#num#	k4
<g/>
+	+	kIx~
<g/>
4157	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13823968737	#num#	k4
<g/>
+	+	kIx~
<g/>
5055	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13825858734	#num#	k4
<g/>
+	+	kIx~
<g/>
5358	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13825679142	#num#	k4
<g/>
+	+	kIx~
<g/>
4956	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1381810106045	#num#	k4
<g/>
+	+	kIx~
<g/>
1546	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
142221467239	#num#	k4
<g/>
+	+	kIx~
<g/>
3380	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
142286810248	#num#	k4
<g/>
+	+	kIx~
<g/>
5490	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138235107856	#num#	k4
<g/>
+	+	kIx~
<g/>
2274	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13824778743	#num#	k4
<g/>
+	+	kIx~
<g/>
4479	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138197127046	#num#	k4
<g/>
+	+	kIx~
<g/>
2464	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138171298057	#num#	k4
<g/>
+	+	kIx~
<g/>
2363	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1381810106537	#num#	k4
<g/>
+	+	kIx~
<g/>
2864	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
1381511126347	#num#	k4
<g/>
+	+	kIx~
<g/>
1656	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13821986339	#num#	k4
<g/>
+	+	kIx~
<g/>
2472	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13825947329	#num#	k4
<g/>
+	+	kIx~
<g/>
4484	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13825768035	#num#	k4
<g/>
+	+	kIx~
<g/>
4582	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138221067833	#num#	k4
<g/>
+	+	kIx~
<g/>
4576	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138191097643	#num#	k4
<g/>
+	+	kIx~
<g/>
3367	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138276510535	#num#	k4
<g/>
+	+	kIx~
<g/>
7087	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13831619824	#num#	k4
<g/>
+	+	kIx~
<g/>
7499	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13830629521	#num#	k4
<g/>
+	+	kIx~
<g/>
7496	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138287311429	#num#	k4
<g/>
+	+	kIx~
<g/>
8591	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138324211540	#num#	k4
<g/>
+	+	kIx~
<g/>
75100	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138276510033	#num#	k4
<g/>
+	+	kIx~
<g/>
6787	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138304411021	#num#	k4
<g/>
+	+	kIx~
<g/>
8994	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138294511229	#num#	k4
<g/>
+	+	kIx~
<g/>
8391	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138286411637	#num#	k4
<g/>
+	+	kIx~
<g/>
7990	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13828919929	#num#	k4
<g/>
+	+	kIx~
<g/>
7093	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13826939036	#num#	k4
<g/>
+	+	kIx~
<g/>
5487	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
13825768638	#num#	k4
<g/>
+	+	kIx~
<g/>
4882	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
</s>
<s>
138	#num#	k4
</s>
<s>
Účast	účast	k1gFnSc1
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ročník	ročník	k1gInSc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Kolo	kolo	k1gNnSc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Doma	doma	k6eAd1
</s>
<s>
Venku	venku	k6eAd1
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
1949	#num#	k4
</s>
<s>
Latinský	latinský	k2eAgInSc4d1
pohár	pohár	k1gInSc4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Stade	Stad	k1gInSc5
de	de	k?
Reims	Reims	k1gInSc4
</s>
<s>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
(	(	kIx(
<g/>
Barcelona	Barcelona	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Finále	finále	k1gNnSc1
Sporting	Sporting	k1gInSc1
CP	CP	kA
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
(	(	kIx(
<g/>
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1952	#num#	k4
</s>
<s>
Latinský	latinský	k2eAgInSc4d1
pohár	pohár	k1gInSc4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Juventus	Juventus	k1gInSc1
FC	FC	kA
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Finále	finále	k1gNnSc1
OGC	OGC	kA
Nice	Nice	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
(	(	kIx(
<g/>
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
Kodaň	Kodaň	k1gFnSc1
XI	XI	kA
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Birmingham	Birmingham	k1gInSc1
City	City	k1gFnSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
44	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc1
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Finále	finále	k1gNnSc1
Londýn	Londýn	k1gInSc1
XI	XI	kA
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
28	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Basilej	Basilej	k1gFnSc1
XI	XI	kA
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
28	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Bělehrad	Bělehrad	k1gInSc1
XI	XI	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Birmingham	Birmingham	k1gInSc1
City	City	k1gFnSc1
FC	FC	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
PFK	PFK	kA
CSKA	CSKA	kA
Sofia	Sofia	k1gFnSc1
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
28	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
AC	AC	kA
Milán	Milán	k1gInSc4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Wolverhampton	Wolverhampton	k1gInSc1
Wanderers	Wanderers	k1gInSc4
FC	FC	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
29	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc7
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
</s>
<s>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Záhřeb	Záhřeb	k1gInSc1
XI	XI	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Hibernian	Hiberniana	k1gFnPc2
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
47	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
</s>
<s>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
Předkolo	předkolo	k1gNnSc1
Lierse	Lierse	k1gFnSc2
SK	Sk	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Spartak	Spartak	k1gInSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Hamburger	hamburger	k1gInSc1
SV	sv	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc1
<g/>
:	:	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Finále	finále	k1gNnSc1
SL	SL	kA
Benfica	Benfic	k1gInSc2
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
Západní	západní	k2eAgInSc1d1
Berlín	Berlín	k1gInSc1
XI	XI	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
GNK	GNK	kA
Dinamo	Dinama	k1gFnSc5
Zagreb	Zagreb	k1gInSc1
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
27	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Sheffield	Sheffielda	k1gFnPc2
Wednesday	Wednesdaa	k1gFnSc2
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
FK	FK	kA
Crvena	Crven	k1gMnSc2
zvezda	zvezd	k1gMnSc2
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Valencia	Valencium	k1gNnSc2
CF	CF	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
63	#num#	k4
<g/>
:	:	kIx,
<g/>
7	#num#	k4
</s>
<s>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
CF	CF	kA
Os	osa	k1gFnPc2
Belenenses	Belenenses	k1gMnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc1
<g/>
:	:	kIx,
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FK	FK	kA
Crvena	Crven	k1gMnSc2
zvezda	zvezd	k1gMnSc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc1
<g/>
:	:	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Shelbourne	Shelbourn	k1gInSc5
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Hamburger	hamburger	k1gInSc1
SV	sv	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc1
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
ACF	ACF	kA
Fiorentina	Fiorentina	k1gFnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Celtic	Celtice	k1gFnPc2
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
RC	RC	kA
Strasbourg	Strasbourg	k1gInSc1
Alsace	Alsace	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc1
<g/>
:	:	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
HM	HM	k?
<g/>
)	)	kIx)
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
VV	VV	kA
DOS	DOS	kA
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Royal	Royal	k1gInSc1
Antwerp	Antwerp	k1gMnSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
Hannover	Hannover	k1gInSc1
961	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc1
<g/>
:	:	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
HM	HM	k?
<g/>
)	)	kIx)
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
RCD	RCD	kA
Espanyol	Espanyol	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Chelsea	Chelse	k1gInSc2
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc1
<g/>
:	:	kIx,
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Finále	finále	k1gNnSc1
Real	Real	k1gInSc1
Zaragoza	Zaragoza	k1gFnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Dundee	Dunde	k1gInSc2
United	United	k1gInSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Zürich	Zürich	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Lugano	Lugana	k1gFnSc5
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
Volný	volný	k2eAgInSc1d1
los	los	k1gInSc1
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FK	FK	kA
Lyn	Lyn	k1gFnSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Köln	Köln	k1gInSc4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
26	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Slovan	Slovan	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
Boldklubben	Boldklubbna	k1gFnPc2
19134	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Győri	Győri	k1gNnPc2
ETO	ETO	kA
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Inter	Inter	k1gInSc4
Milán	Milán	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
</s>
<s>
Veletržní	veletržní	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
GKS	GKS	kA
Katowice	Katowice	k1gFnSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Inter	Inter	k1gInSc4
Milán	Milán	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
Superfinále	superfinále	k1gNnSc1
Leeds	Leeds	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Lisburn	Lisburna	k1gFnPc2
Distillery	Distiller	k1gMnPc4
FC	FC	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Steaua	Steau	k1gInSc2
Bucureș	Bucureș	k1gFnSc2
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
FC	FC	kA
Porto	porto	k1gNnSc4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
OGC	OGC	kA
Nice	Nice	k1gFnSc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Linz	Linz	k1gInSc4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
Feyenoord	Feyenoorda	k1gFnPc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Å	Å	k1gFnPc2
FF	ff	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Leeds	Leeds	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
PAOK	PAOK	kA
FC	FC	kA
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
SS	SS	kA
Lazio	Lazio	k6eAd1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Vasas	Vasasa	k1gFnPc2
SC	SC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
PFK	PFK	kA
Levski	Levske	k1gFnSc4
Sofia	Sofia	k1gFnSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
58	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
CF	CF	kA
Os	osa	k1gFnPc2
Belenenses	Belenenses	k1gMnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
KSC	KSC	kA
Lokeren	Lokerna	k1gFnPc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Östers	Östersa	k1gFnPc2
IF	IF	kA
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Athletic	Athletice	k1gFnPc2
Bilbao	Bilbao	k1gNnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Steaua	Steau	k1gInSc2
Bucureș	Bucureș	k1gFnSc2
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
18	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
AZ	AZ	kA
Alkmaar	Alkmaar	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Ipswich	Ipswicha	k1gFnPc2
Town	Towna	k1gFnPc2
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Aston	Astona	k1gFnPc2
Villa	Villo	k1gNnSc2
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
PSV	PSV	kA
Eindhoven	Eindhoven	k2eAgMnSc1d1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FK	FK	kA
Šachtar	Šachtar	k1gInSc1
Doněck	Doněck	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
RSC	RSC	kA
Anderlecht	Anderlechtum	k1gNnPc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Ipswich	Ipswicha	k1gFnPc2
Town	Towna	k1gFnPc2
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
Semifinále	semifinále	k1gNnSc1
KSK	KSK	kA
Beveren	Beverna	k1gFnPc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Finále	finála	k1gFnSc3
Fortuna	Fortuna	k1gFnSc1
Düsseldorf	Düsseldorf	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
po	po	k7c6
prodl	prodnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
1979	#num#	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
Finále	finále	k1gNnSc1
Nottingham	Nottingham	k1gInSc1
Forest	Forest	k1gMnSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Íþ	Íþ	k1gFnPc2
Akraness	Akranessa	k1gFnPc2
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Aris	Aris	k1gInSc1
Bonnevoie	Bonnevoie	k1gFnSc1
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
111	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Valencia	Valencium	k1gNnSc2
CF	CF	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
43	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Sliema	Sliemum	k1gNnSc2
Wanderers	Wanderersa	k1gFnPc2
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Köln	Köln	k1gInSc4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
41	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
PFK	PFK	kA
Botev	Botev	k1gFnSc1
Plovdiv	Plovdiv	k1gInSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
ASVS	ASVS	kA
Dukla	Dukla	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Lokomotive	Lokomotiv	k1gInSc5
Leipzig	Leipzig	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Tottenham	Tottenham	k1gInSc1
Hotspur	Hotspur	k1gMnSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Standard	standard	k1gInSc1
Liè	Liè	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1982	#num#	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
Finále	finále	k1gNnSc1
Aston	Astona	k1gFnPc2
Villa	Villo	k1gNnSc2
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Apollon	Apollon	k1gMnSc1
Limassol	Limassol	k1gInSc4
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FK	FK	kA
Crvena	Crven	k1gMnSc2
zvezda	zvezd	k1gMnSc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
26	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FK	FK	kA
Austria	Austrium	k1gNnSc2
Wien	Wien	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Magdeburg	Magdeburg	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
NEC	NEC	kA
Nijmegen	Nijmegen	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Metz	Metz	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
44	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
TJ	tj	kA
Sparta	Sparta	k1gFnSc1
ČKD	ČKD	kA
Praha	Praha	k1gFnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
FC	FC	kA
Porto	porto	k1gNnSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
IFK	IFK	kA
Göteborg	Göteborg	k1gInSc4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Finále	finále	k1gNnSc1
FC	FC	kA
Steaua	Steau	k1gInSc2
Bucureș	Bucureș	k1gFnSc2
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
KS	ks	kA
Flamurtari	Flamurtar	k1gFnSc2
Vlorë	Vlorë	k1gFnSc2
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Sporting	Sporting	k1gInSc4
CP	CP	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Bayer	Bayer	k1gMnSc1
05	#num#	k4
Uerdingen	Uerdingen	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Dundee	Dunde	k1gInSc2
United	United	k1gInSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
CF	CF	kA
Os	osa	k1gFnPc2
Belenenses	Belenenses	k1gMnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
FK	FK	kA
Dynamo	dynamo	k1gNnSc4
Moskva	Moskva	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
KS	ks	kA
Flamurtari	Flamurtar	k1gFnSc2
Vlorë	Vlorë	k1gFnSc2
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Bayer	Bayer	k1gMnSc1
04	#num#	k4
Leverkusen	Leverkusen	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Fram	Fram	k1gMnSc1
Reykjavík	Reykjavík	k1gMnSc1
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Lech	Lech	k1gMnSc1
Poznań	Poznań	k1gMnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Aarhus	Aarhus	k1gInSc1
GF	GF	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
CFKA	CFKA	kA
Sredec	Sredec	k1gMnSc1
Sofia	Sofia	k1gFnSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
UC	UC	kA
Sampdoria	Sampdorium	k1gNnPc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1989	#num#	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
Finále	finále	k1gNnSc1
AC	AC	kA
Milán	Milán	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Legia	Legia	k1gFnSc1
Warszawa	Warszawa	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
RSC	RSC	kA
Anderlecht	Anderlechtum	k1gNnPc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
Trabzonspor	Trabzonspora	k1gFnPc2
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Fram	Fram	k1gMnSc1
Reykjavík	Reykjavík	k1gMnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc4
FK	FK	kA
Dynamo	dynamo	k1gNnSc4
Kyjev	Kyjev	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FC	FC	kA
Hansa	hansa	k1gFnSc1
Rostock	Rostock	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Kaiserslautern	Kaiserslautern	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
SL	SL	kA
Benfica	Benfica	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
FK	FK	kA
Dynamo	dynamo	k1gNnSc4
Kyjev	Kyjev	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
UC	UC	kA
Sampdoria	Sampdorium	k1gNnPc5
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
po	po	k7c6
prodl	prodnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
1992	#num#	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
Finále	finále	k1gNnSc1
Werder	Werder	k1gMnSc1
Bremen	Bremen	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1992	#num#	k4
</s>
<s>
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
</s>
<s>
Finále	finále	k1gNnSc1
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Viking	Viking	k1gMnSc1
FK	FK	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
PFK	PFK	kA
CSKA	CSKA	kA
Moskva	Moskva	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
FK	FK	kA
Dynamo	dynamo	k1gNnSc4
Kyjev	Kyjev	k1gInSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FK	FK	kA
Austria	Austrium	k1gNnSc2
Wien	Wien	k1gInSc4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
Galatasaray	Galatasaray	k1gInPc1
SK	Sk	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
AS	as	k9
Monaco	Monaco	k6eAd1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
FK	FK	kA
Spartak	Spartak	k1gInSc1
Moskva	Moskva	k1gFnSc1
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc4
FC	FC	kA
Porto	porto	k1gNnSc4
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
AC	AC	kA
Milán	Milán	k1gInSc1
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
Galatasaray	Galatasaray	k1gInPc1
SK	Sk	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
IFK	IFK	kA
Göteborg	Göteborg	k1gInSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
FC	FC	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Hapoel	Hapoela	k1gFnPc2
Beerševa	Beerševo	k1gNnSc2
FC	FC	kA
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Vitória	Vitórium	k1gNnSc2
SC	SC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
PSV	PSV	kA
Eindhoven	Eindhoven	k2eAgMnSc1d1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
AEK	AEK	kA
Larnaka	Larnak	k1gMnSc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FK	FK	kA
Crvena	Crven	k1gMnSc2
zvezda	zvezd	k1gMnSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
AIK	AIK	kA
Stockholm	Stockholm	k1gInSc4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
ACF	ACF	kA
Fiorentina	Fiorentina	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1997	#num#	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
Finále	finále	k1gNnSc1
Borussia	Borussium	k1gNnSc2
Dortmund	Dortmund	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
Skonto	skonto	k1gNnSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
C	C	kA
</s>
<s>
Newcastle	Newcastle	k6eAd1
United	United	k1gInSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
PSV	PSV	kA
Eindhoven	Eindhoven	k2eAgInSc4d1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
FK	FK	kA
Dynamo	dynamo	k1gNnSc4
Kyjev	Kyjev	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
D	D	kA
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Brø	Brø	k1gInPc1
IF	IF	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
AIK	AIK	kA
Stockholm	Stockholm	k1gInSc1
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
ACF	ACF	kA
Fiorentina	Fiorentina	k1gFnSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Arsenal	Arsenat	k5eAaImAgMnS,k5eAaPmAgMnS
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Osmifinálová	osmifinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
Hertha	Hertha	k1gFnSc1
BSC	BSC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
FC	FC	kA
Porto	porto	k1gNnSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Chelsea	Chelse	k1gInSc2
FC	FC	kA
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
36	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Valencia	Valencium	k1gNnSc2
CF	CF	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
43	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
H	H	kA
</s>
<s>
Leeds	Leeds	k6eAd1
United	United	k1gInSc1
FC	FC	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Beşiktaş	Beşiktaş	k?
JK	JK	kA
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Club	club	k1gInSc4
Brugge	Brugg	k1gFnSc2
KV	KV	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
AEK	AEK	kA
Athény	Athéna	k1gFnSc2
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Celta	celta	k1gFnSc1
de	de	k?
Vigo	Vigo	k1gNnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
Wisła	Wisł	k1gInSc2
Kraków	Kraków	k1gFnSc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
F	F	kA
</s>
<s>
Fenerbahçe	Fenerbahçe	k1gFnSc1
SK	Sk	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Bayer	Bayer	k1gMnSc1
04	#num#	k4
Leverkusen	Leverkusen	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Olympique	Olympique	k1gInSc1
Lyon	Lyon	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Osmifinálová	osmifinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Galatasaray	Galatasaray	k1gInPc1
SK	Sk	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
AS	as	k1gNnSc1
Roma	Rom	k1gMnSc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Panathinaikos	Panathinaikosa	k1gFnPc2
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc7
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
Legia	Legia	k1gFnSc1
Warszawa	Warszawa	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
H	H	kA
</s>
<s>
Club	club	k1gInSc1
Brugge	Brugg	k1gInSc2
KV	KV	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Galatasaray	Galatasaray	k1gInPc1
SK	Sk	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
FK	FK	kA
Lokomotiv	lokomotiva	k1gFnPc2
Moskva	Moskva	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinálová	osmifinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
Bayer	Bayer	k1gMnSc1
04	#num#	k4
Leverkusen	Leverkusen	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Newcastle	Newcastle	k6eAd1
United	United	k1gInSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
FK	FK	kA
Matador	matador	k1gMnSc1
Púchov	Púchov	k1gInSc4
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
19	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Panionios	Panioniosa	k1gFnPc2
GSS	GSS	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
Brø	Brø	k1gFnSc2
IF	IF	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
Celtic	Celtice	k1gFnPc2
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
F	F	kA
</s>
<s>
Celtic	Celtice	k1gFnPc2
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FK	FK	kA
Šachtar	Šachtar	k1gInSc1
Doněck	Doněck	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Chelsea	Chelse	k1gInSc2
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
44	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
C	C	kA
</s>
<s>
Werder	Werder	k1gInSc1
Bremen	Bremen	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Udinese	Udinést	k5eAaPmIp3nS
Calcio	Calcio	k6eAd1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Panathinaikos	Panathinaikos	k1gInSc1
FC	FC	kA
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Chelsea	Chelse	k1gInSc2
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
SL	SL	kA
Benfica	Benfic	k1gInSc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
AC	AC	kA
Milán	Milán	k1gInSc4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Arsenal	Arsenal	k1gFnPc2
FC	FC	kA
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
Finále	finále	k1gNnSc1
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
MS	MS	kA
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
</s>
<s>
Semifinále	semifinále	k1gNnPc4
Club	club	k1gInSc1
América	Améric	k1gInSc2
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Sport	sport	k1gInSc1
Club	club	k1gInSc4
Internacional	Internacional	k1gFnSc2
</s>
<s>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
PFK	PFK	kA
Levski	Levske	k1gFnSc4
Sofia	Sofia	k1gFnSc1
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Werder	Werder	k1gInSc1
Bremen	Bremen	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Chelsea	Chelsea	k1gFnSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
E	E	kA
</s>
<s>
Olympique	Olympique	k1gInSc1
Lyon	Lyon	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
VfB	VfB	k?
Stuttgart	Stuttgart	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Rangers	Rangers	k6eAd1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Celtic	Celtice	k1gFnPc2
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FC	FC	kA
Schalke	Schalk	k1gFnSc2
0	#num#	k4
<g/>
41	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
Wisła	Wisł	k1gInSc2
Kraków	Kraków	k1gFnSc2
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
C	C	kA
</s>
<s>
Sporting	Sporting	k1gInSc1
CP	CP	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
</s>
<s>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FK	FK	kA
Šachtar	Šachtar	k1gInSc1
Doněck	Doněck	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
FC	FC	kA
Basel	Basel	k1gInSc4
18931	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Olympique	Olympique	k1gNnSc2
Lyon	Lyon	k1gInSc4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Chelsea	Chelse	k1gInSc2
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
Finále	finále	k1gNnSc1
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2009	#num#	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
Finále	finále	k1gNnSc1
FK	FK	kA
Šachtar	Šachtar	k1gInSc1
Doněck	Doněck	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2009	#num#	k4
</s>
<s>
MS	MS	kA
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Atlante	Atlant	k1gMnSc5
FC	FC	kA
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Estudiantes	Estudiantesa	k1gFnPc2
de	de	k?
La	la	k1gNnSc1
Plata	plato	k1gNnSc2
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
po	po	k7c6
prodl	prodnout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
F	F	kA
</s>
<s>
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FK	FK	kA
Dynamo	dynamo	k1gNnSc4
Kyjev	Kyjev	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
FK	FK	kA
Rubin	Rubin	k1gInSc1
Kazaň	Kazaň	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
VfB	VfB	k1gFnSc2
Stuttgart	Stuttgart	k1gInSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Arsenal	Arsenal	k1gFnPc2
FC	FC	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
26	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc7
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
D	D	kA
</s>
<s>
Panathinaikos	Panathinaikos	k1gInSc1
FC	FC	kA
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FK	FK	kA
Rubin	Rubin	k1gInSc1
Kazaň	Kazaň	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
FC	FC	kA
Kø	Kø	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Arsenal	Arsenal	k1gFnPc2
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
FK	FK	kA
Šachtar	Šachtar	k1gInSc1
Doněck	Doněck	k1gInSc1
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc7
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2011	#num#	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
Finále	finále	k1gNnSc4
FC	FC	kA
Porto	porto	k1gNnSc4
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2011	#num#	k4
</s>
<s>
MS	MS	kA
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Al-Sadd	Al-Sadda	k1gFnPc2
SC	SC	kA
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Santos	Santosa	k1gFnPc2
FC	FC	kA
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
H	H	kA
</s>
<s>
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FK	FK	kA
BATE	BATE	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
FC	FC	kA
Viktoria	Viktoria	k1gFnSc1
Plzeň	Plzeň	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Bayer	Bayer	k1gMnSc1
04	#num#	k4
Leverkusen	Leverkusen	k1gInSc1
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
110	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
AC	AC	kA
Milán	Milán	k1gInSc4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Chelsea	Chelse	k1gInSc2
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
G	G	kA
</s>
<s>
FK	FK	kA
Spartak	Spartak	k1gInSc1
Moskva	Moskva	k1gFnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
SL	SL	kA
Benfica	Benfica	k1gFnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Celtic	Celtice	k1gFnPc2
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
AC	AC	kA
Milán	Milán	k1gInSc4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
Semifinále	semifinále	k1gNnSc1
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
<g/>
:	:	kIx,
<g/>
7	#num#	k4
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
H	H	kA
</s>
<s>
AFC	AFC	kA
Ajax	Ajax	k1gInSc4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Celtic	Celtice	k1gFnPc2
FC	FC	kA
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Manchester	Manchester	k1gInSc1
City	City	k1gFnSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
F	F	kA
</s>
<s>
APOEL	APOEL	kA
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
AFC	AFC	kA
Ajax	Ajax	k1gInSc4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Manchester	Manchester	k1gInSc1
City	City	k1gFnSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc1
Juventus	Juventus	k1gInSc1
FC	FC	kA
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
2015	#num#	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
Finále	finále	k1gNnSc1
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
</s>
<s>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
2015	#num#	k4
</s>
<s>
MS	MS	kA
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Kuang-čou	Kuang-ča	k1gMnSc7
Evergrande	Evergrand	k1gInSc5
Tchao-pao	Tchao-paa	k1gMnSc5
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Finále	finále	k1gNnSc4
CA	ca	kA
River	Rivero	k1gNnPc2
Plate	plat	k1gInSc5
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
E	E	kA
</s>
<s>
AS	as	k1gNnSc1
Roma	Rom	k1gMnSc2
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Bayer	Bayer	k1gMnSc1
04	#num#	k4
Leverkusen	Leverkusen	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
FK	FK	kA
BATE	BATE	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Arsenal	Arsenal	k1gFnPc2
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
C	C	kA
</s>
<s>
Celtic	Celtice	k1gFnPc2
FC	FC	kA
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Borussia	Borussia	k1gFnSc1
Mönchengladbach	Mönchengladbach	k1gInSc1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Manchester	Manchester	k1gInSc1
City	City	k1gFnSc2
FC	FC	kA
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
46	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
D	D	kA
</s>
<s>
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Sporting	Sporting	k1gInSc1
CP	CP	kA
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Olympiacos	Olympiacos	k1gInSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Chelsea	Chelse	k1gInSc2
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
AS	as	k1gNnSc2
Roma	Rom	k1gMnSc2
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
PSV	PSV	kA
Eindhoven	Eindhoven	k2eAgInSc4d1
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Tottenham	Tottenham	k6eAd1
Hotspur	Hotspur	k1gMnSc1
FC	FC	kA
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
FC	FC	kA
Internazionale	Internazionale	k1gFnSc1
Milano	Milana	k1gFnSc5
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
Olympique	Olympique	k1gNnSc2
Lyon	Lyon	k1gInSc4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Semifinále	semifinále	k1gNnSc1
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
43	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
F	F	kA
</s>
<s>
Borussia	Borussia	k1gFnSc1
Dortmund	Dortmund	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FC	FC	kA
Internazionale	Internazionale	k1gFnSc1
Milano	Milana	k1gFnSc5
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
SK	Sk	kA
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
SSC	SSC	kA
Neapol	Neapol	k1gFnSc1
</s>
<s>
Rivalita	rivalita	k1gFnSc1
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
kluby	klub	k1gInPc7
</s>
<s>
Rivalita	rivalita	k1gFnSc1
mezi	mezi	k7c7
kluby	klub	k1gInPc7
je	být	k5eAaImIp3nS
dvojího	dvojí	k4xRgInSc2
charakteru	charakter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
můžeme	moct	k5eAaImIp1nP
nazvat	nazvat	k5eAaBmF,k5eAaPmF
geografický	geografický	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
častější	častý	k2eAgNnSc1d2
a	a	k8xC
vzniká	vznikat	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
jednoduše	jednoduše	k6eAd1
mezi	mezi	k7c4
kluby	klub	k1gInPc4
s	s	k7c7
nepříliš	příliš	k6eNd1
od	od	k7c2
sebe	se	k3xPyFc2
vzdálenými	vzdálený	k2eAgFnPc7d1
domácími	domácí	k2eAgMnPc7d1
hřišti	hřiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
kluby	klub	k1gInPc4
ze	z	k7c2
stejného	stejný	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
regionu	region	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
FC	FC	kA
Barcelony	Barcelona	k1gFnSc2
jde	jít	k5eAaImIp3nS
o	o	k7c4
městského	městský	k2eAgMnSc4d1
rivala	rival	k1gMnSc4
RCD	RCD	kA
Espanyol	Espanyol	k1gInSc1
a	a	k8xC
vzájemný	vzájemný	k2eAgInSc1d1
duel	duel	k1gInSc1
těchto	tento	k3xDgFnPc2
dvou	dva	k4xCgNnPc2
mužstev	mužstvo	k1gNnPc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
El	Ela	k1gFnPc2
derbi	derbi	k6eAd1
Barceloní	Barceloň	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
překladu	překlad	k1gInSc6
Barcelonské	barcelonský	k2eAgNnSc1d1
derby	derby	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc4
charakter	charakter	k1gInSc4
by	by	kYmCp3nS
se	se	k3xPyFc4
dal	dát	k5eAaPmAgInS
nazvat	nazvat	k5eAaPmF,k5eAaBmF
historický	historický	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rivalita	rivalita	k1gFnSc1
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
vzniká	vznikat	k5eAaImIp3nS
velmi	velmi	k6eAd1
dlouho	dlouho	k6eAd1
<g/>
,	,	kIx,
zejména	zejména	k9
mezi	mezi	k7c7
úspěšnými	úspěšný	k2eAgInPc7d1
kluby	klub	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
často	často	k6eAd1
mezi	mezi	k7c7
sebou	se	k3xPyFc7
bojují	bojovat	k5eAaImIp3nP
o	o	k7c4
přední	přední	k2eAgNnPc4d1
místa	místo	k1gNnPc4
v	v	k7c6
domácích	domácí	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typický	typický	k2eAgInSc1d1
s	s	k7c7
světově	světově	k6eAd1
proslulý	proslulý	k2eAgInSc1d1
příklad	příklad	k1gInSc1
najdeme	najít	k5eAaPmIp1nP
právě	právě	k9
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
a	a	k8xC
jedním	jeden	k4xCgInSc7
z	z	k7c2
akterů	akter	k1gInPc2
je	být	k5eAaImIp3nS
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gMnSc7
největším	veliký	k2eAgMnSc7d3
rivalem	rival	k1gMnSc7
je	být	k5eAaImIp3nS
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
a	a	k8xC
vzájemný	vzájemný	k2eAgInSc1d1
souboj	souboj	k1gInSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
El	Ela	k1gFnPc2
Clásico	Clásico	k6eAd1
(	(	kIx(
<g/>
v	v	k7c6
překladu	překlad	k1gInSc6
Klasika	klasika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
El	Ela	k1gFnPc2
Clásico	Clásico	k1gMnSc1
(	(	kIx(
<g/>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Souboj	souboj	k1gInSc1
v	v	k7c6
semifinále	semifinále	k1gNnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
El	Ela	k1gFnPc2
Clásico	Clásico	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Jeden	jeden	k4xCgInSc1
z	z	k7c2
největších	veliký	k2eAgInPc2d3
a	a	k8xC
nejsledovanějších	sledovaný	k2eAgInPc2d3
duelů	duel	k1gInPc2
na	na	k7c6
světě	svět	k1gInSc6
mezi	mezi	k7c7
nejslavnějšími	slavný	k2eAgInPc7d3
kluby	klub	k1gInPc7
světa	svět	k1gInSc2
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souboj	souboj	k1gInSc1
mezi	mezi	k7c7
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
a	a	k8xC
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
proti	proti	k7c3
Barceloně	Barcelona	k1gFnSc3
stojí	stát	k5eAaImIp3nS
33	#num#	k4
titulů	titul	k1gInPc2
z	z	k7c2
Primera	primera	k1gFnSc1
División	División	k1gInSc4
a	a	k8xC
13	#num#	k4
vítězství	vítězství	k1gNnPc2
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
Realu	Real	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rivalita	rivalita	k1gFnSc1
mezi	mezi	k7c7
těmito	tento	k3xDgInPc7
kluby	klub	k1gInPc7
je	být	k5eAaImIp3nS
tak	tak	k9
velmi	velmi	k6eAd1
pochopitelná	pochopitelný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
podpořena	podpořit	k5eAaPmNgFnS
politicko-historickými	politicko-historický	k2eAgInPc7d1
důvody	důvod	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
je	být	k5eAaImIp3nS
centrum	centrum	k1gNnSc4
Katalánska	Katalánsko	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
cítí	cítit	k5eAaImIp3nS
utlačováno	utlačován	k2eAgNnSc1d1
pod	pod	k7c7
nadvládou	nadvláda	k1gFnSc7
Španělského	španělský	k2eAgNnSc2d1
království	království	k1gNnSc2
a	a	k8xC
léta	léto	k1gNnSc2
žádá	žádat	k5eAaImIp3nS
o	o	k7c4
autonomii	autonomie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Katalánce	Katalánec	k1gMnPc4
je	být	k5eAaImIp3nS
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
národním	národní	k2eAgNnSc7d1
mužstvem	mužstvo	k1gNnSc7
a	a	k8xC
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
symbolem	symbol	k1gInSc7
zla	zlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soupeření	soupeření	k1gNnSc1
tak	tak	k6eAd1
má	mít	k5eAaImIp3nS
nejen	nejen	k6eAd1
sportovní	sportovní	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
z	z	k7c2
něj	on	k3xPp3gNnSc2
cítit	cítit	k5eAaImF
politické	politický	k2eAgNnSc4d1
a	a	k8xC
kulturní	kulturní	k2eAgNnSc4d1
napětí	napětí	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
El	Ela	k1gFnPc2
derbi	derbi	k1gNnSc2
Barceloní	Barceloň	k1gFnPc2
(	(	kIx(
<g/>
RCD	RCD	kA
Espanyol	Espanyol	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Camp	camp	k1gInSc1
Nou	Nou	k1gFnSc2
při	při	k7c6
zápase	zápas	k1gInSc6
s	s	k7c7
RCD	RCD	kA
Espanyol	Espanyol	k1gInSc1
</s>
<s>
Aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
červenci	červenec	k1gInSc3
2019	#num#	k4
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Zápasy	zápas	k1gInPc1
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
Góly	gól	k1gInPc1
</s>
<s>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
</s>
<s>
210	#num#	k4
</s>
<s>
123	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
44	#num#	k4
</s>
<s>
404	#num#	k4
</s>
<s>
RCD	RCD	kA
Espanyol	Espanyol	k1gInSc1
</s>
<s>
210	#num#	k4
</s>
<s>
44	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
123	#num#	k4
</s>
<s>
223	#num#	k4
</s>
<s>
Největším	veliký	k2eAgMnSc7d3
lokálním	lokální	k2eAgMnSc7d1
rivalem	rival	k1gMnSc7
byl	být	k5eAaImAgMnS
od	od	k7c2
založení	založení	k1gNnSc2
obou	dva	k4xCgInPc2
klubů	klub	k1gInPc2
RCD	RCD	kA
Espanyol	Espanyol	k1gInSc1
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc1
slavný	slavný	k2eAgInSc1d1
klub	klub	k1gInSc1
z	z	k7c2
Barcelony	Barcelona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílo-modří	Bílo-modrý	k2eAgMnPc5d1
podle	podle	k7c2
mnohých	mnohé	k1gNnPc6
zradili	zradit	k5eAaPmAgMnP
katalánský	katalánský	k2eAgInSc4d1
lid	lid	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
přijali	přijmout	k5eAaPmAgMnP
královský	královský	k2eAgInSc4d1
patronát	patronát	k1gInSc4
a	a	k8xC
přidali	přidat	k5eAaPmAgMnP
korunu	koruna	k1gFnSc4
do	do	k7c2
loga	logo	k1gNnSc2
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tak	tak	k6eAd1
i	i	k9
zde	zde	k6eAd1
můžeme	moct	k5eAaImIp1nP
vidět	vidět	k5eAaImF
politický	politický	k2eAgInSc4d1
podtext	podtext	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
nadvlády	nadvláda	k1gFnSc2
generála	generál	k1gMnSc2
Francisco	Francisco	k6eAd1
Francoa	Francoa	k1gMnSc1
byl	být	k5eAaImAgMnS
Espanyol	Espanyol	k1gInSc4
symbolem	symbol	k1gInSc7
zrady	zrada	k1gFnSc2
katalánského	katalánský	k2eAgInSc2d1
lidu	lid	k1gInSc2
a	a	k8xC
přisluhovačem	přisluhovač	k1gMnSc7
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
tomu	ten	k3xDgMnSc3
stál	stát	k5eAaImAgMnS
revolucionářský	revolucionářský	k2eAgMnSc1d1
duch	duch	k1gMnSc1
příznivců	příznivec	k1gMnPc2
Barcy	Barca	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rivalitu	rivalita	k1gFnSc4
s	s	k7c7
RCD	RCD	kA
Espanyol	Espanyol	k1gInSc1
můžeme	moct	k5eAaImIp1nP
pozorovat	pozorovat	k5eAaImF
především	především	k9
mezi	mezi	k7c7
fanoušky	fanoušek	k1gMnPc7
obou	dva	k4xCgInPc2
klubů	klub	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
herního	herní	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
není	být	k5eNaImIp3nS
Espanyol	Espanyol	k1gInSc1
tak	tak	k6eAd1
úspěšný	úspěšný	k2eAgInSc1d1
jako	jako	k8xC,k8xS
Barcelona	Barcelona	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
kontě	konto	k1gNnSc6
nemá	mít	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
mistrovský	mistrovský	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
,	,	kIx,
„	„	k?
<g/>
pouze	pouze	k6eAd1
<g/>
“	“	k?
4	#num#	k4
vítězství	vítězství	k1gNnSc2
v	v	k7c6
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
při	při	k7c6
prvních	první	k4xOgNnPc6
dvou	dva	k4xCgFnPc2
(	(	kIx(
<g/>
1929	#num#	k4
<g/>
,	,	kIx,
1940	#num#	k4
<g/>
)	)	kIx)
musel	muset	k5eAaImAgInS
přejít	přejít	k5eAaPmF
právě	právě	k9
přes	přes	k7c4
Barcelonu	Barcelona	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
trofej	trofej	k1gFnSc4
získal	získat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
těmito	tento	k3xDgInPc7
kluby	klub	k1gInPc7
bylo	být	k5eAaImAgNnS
odehráno	odehrát	k5eAaPmNgNnS
nejvíce	hodně	k6eAd3,k6eAd1
zápasů	zápas	k1gInPc2
ze	z	k7c2
všech	všecek	k3xTgNnPc2
regionálních	regionální	k2eAgNnPc2d1
derby	derby	k1gNnPc2
(	(	kIx(
<g/>
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
Sevilla	Sevilla	k1gFnSc1
<g/>
,	,	kIx,
...	...	k?
<g/>
)	)	kIx)
na	na	k7c6
území	území	k1gNnSc6
Španělska	Španělsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezoně	sezona	k1gFnSc6
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Barcelona	Barcelona	k1gFnSc1
získal	získat	k5eAaPmAgInS
„	„	k?
<g/>
treble	treble	k6eAd1
<g/>
“	“	k?
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
Espanyol	Espanyol	k1gInSc1
prvním	první	k4xOgInSc7
klubem	klub	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
porazil	porazit	k5eAaPmAgInS
Barcu	Barca	k1gFnSc4
na	na	k7c6
domácím	domácí	k2eAgNnSc6d1
hřišti	hřiště	k1gNnSc6
Camp	camp	k1gInSc4
Nou	Nou	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Znak	znak	k1gInSc1
</s>
<s>
Původní	původní	k2eAgInSc1d1
znak	znak	k1gInSc1
</s>
<s>
Luis	Luisa	k1gFnPc2
Suarez	Suarez	k1gMnSc1
v	v	k7c6
dresu	dres	k1gInSc6
Barcelony	Barcelona	k1gFnSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgInSc1d1
znak	znak	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1899	#num#	k4
byl	být	k5eAaImAgInS
totožný	totožný	k2eAgInSc1d1
s	s	k7c7
městským	městský	k2eAgInSc7d1
znakem	znak	k1gInSc7
Barcelony	Barcelona	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
jako	jako	k8xC,k8xS
důkaz	důkaz	k1gInSc4
soudržnosti	soudržnost	k1gFnSc2
klubu	klub	k1gInSc2
a	a	k8xC
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
byla	být	k5eAaImAgFnS
vypsána	vypsán	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
na	na	k7c4
nový	nový	k2eAgInSc4d1
znak	znak	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soutěž	soutěž	k1gFnSc1
vyhrál	vyhrát	k5eAaPmAgInS
příspěvek	příspěvek	k1gInSc1
neznámého	známý	k2eNgMnSc2d1
autora	autor	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
slouží	sloužit	k5eAaImIp3nS
s	s	k7c7
drobnými	drobný	k2eAgFnPc7d1
úpravami	úprava	k1gFnPc7
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úpravy	úprava	k1gFnPc1
proběhly	proběhnout	k5eAaPmAgFnP
v	v	k7c6
letech	léto	k1gNnPc6
1910	#num#	k4
(	(	kIx(
<g/>
po	po	k7c6
zavedení	zavedení	k1gNnSc6
znaku	znak	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1920	#num#	k4
<g/>
,	,	kIx,
1936	#num#	k4
<g/>
,	,	kIx,
1941	#num#	k4
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
,	,	kIx,
1975	#num#	k4
a	a	k8xC
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
schválena	schválit	k5eAaPmNgFnS
další	další	k2eAgFnSc1d1
změna	změna	k1gFnSc1
<g/>
,	,	kIx,
vypuštění	vypuštění	k1gNnSc1
písmen	písmeno	k1gNnPc2
FCB	FCB	kA
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
platit	platit	k5eAaImF
od	od	k7c2
nové	nový	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Znak	znak	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
(	(	kIx(
<g/>
desátý	desátý	k4xOgMnSc1
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
<g/>
)	)	kIx)
tvoří	tvořit	k5eAaImIp3nP
štít	štít	k1gInSc4
se	s	k7c7
zlatými	zlatý	k2eAgInPc7d1
okraji	okraj	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horní	horní	k2eAgFnSc1d1
část	část	k1gFnSc1
štítu	štít	k1gInSc2
je	být	k5eAaImIp3nS
vertikálně	vertikálně	k6eAd1
dělena	dělit	k5eAaImNgFnS
na	na	k7c4
dvě	dva	k4xCgFnPc4
poloviny	polovina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
levé	levý	k2eAgFnSc6d1
je	být	k5eAaImIp3nS
červený	červený	k2eAgInSc1d1
kříž	kříž	k1gInSc1
na	na	k7c6
bílém	bílý	k2eAgNnSc6d1
poli	pole	k1gNnSc6
(	(	kIx(
<g/>
Creu	Cre	k2eAgFnSc4d1
de	de	k?
Sant	Santa	k1gFnPc2
Jordi	Jord	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravou	pravá	k1gFnSc7
tvoří	tvořit	k5eAaImIp3nS
červené	červený	k2eAgInPc4d1
a	a	k8xC
žluté	žlutý	k2eAgInPc4d1
svislé	svislý	k2eAgInPc4d1
pruhy	pruh	k1gInPc4
(	(	kIx(
<g/>
barvy	barva	k1gFnPc4
Katalánska	Katalánsko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spodní	spodní	k2eAgFnSc1d1
část	část	k1gFnSc1
tvoří	tvořit	k5eAaImIp3nS
rudé	rudý	k2eAgInPc4d1
a	a	k8xC
modré	modrý	k2eAgInPc4d1
svislé	svislý	k2eAgInPc4d1
pruhy	pruh	k1gInPc4
(	(	kIx(
<g/>
barvy	barva	k1gFnPc4
klubu	klub	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
nichž	jenž	k3xRgNnPc6
leží	ležet	k5eAaImIp3nS
zlatý	zlatý	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
míč	míč	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
horní	horní	k2eAgFnSc7d1
a	a	k8xC
spodní	spodní	k2eAgFnSc7d1
částí	část	k1gFnSc7
je	být	k5eAaImIp3nS
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
zlatý	zlatý	k2eAgInSc1d1
pruh	pruh	k1gInSc1
s	s	k7c7
písmeny	písmeno	k1gNnPc7
FCB	FCB	kA
(	(	kIx(
<g/>
počáteční	počáteční	k2eAgNnPc1d1
písmena	písmeno	k1gNnPc1
názvu	název	k1gInSc2
klubu	klub	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Užívá	užívat	k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k9
černobílá	černobílý	k2eAgFnSc1d1
varianta	varianta	k1gFnSc1
znaku	znak	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dres	dres	k1gInSc1
</s>
<s>
Dres	dres	k1gInSc1
Barcelony	Barcelona	k1gFnSc2
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
sešit	sešít	k5eAaPmNgInS
ze	z	k7c2
dvou	dva	k4xCgInPc2
dresů	dres	k1gInPc2
různých	různý	k2eAgFnPc2d1
barev	barva	k1gFnPc2
napůl	napůl	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trenýrky	trenýrky	k1gFnPc4
byly	být	k5eAaImAgFnP
původně	původně	k6eAd1
bílé	bílý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barvy	barva	k1gFnPc1
jsou	být	k5eAaImIp3nP
symbolem	symbol	k1gInSc7
Katalánska	Katalánsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
se	se	k3xPyFc4
dres	dres	k1gInSc4
vyvíjel	vyvíjet	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
dresu	dres	k1gInSc6
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgFnP
pruhy	pruh	k1gInPc4
<g/>
,	,	kIx,
kterých	který	k3yIgInPc2,k3yQgInPc2,k3yRgInPc2
časem	časem	k6eAd1
přibývalo	přibývat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
prakticky	prakticky	k6eAd1
před	před	k7c7
každou	každý	k3xTgFnSc7
sezonou	sezona	k1gFnSc7
domácí	domácí	k2eAgInSc4d1
dres	dres	k1gInSc4
lehce	lehko	k6eAd1
upravuje	upravovat	k5eAaImIp3nS
a	a	k8xC
venkovní	venkovní	k2eAgInSc4d1
dres	dres	k1gInSc4
téměř	téměř	k6eAd1
zcela	zcela	k6eAd1
mění	měnit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
v	v	k7c6
sezoně	sezona	k1gFnSc6
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
byl	být	k5eAaImAgInS
venkovní	venkovní	k2eAgInSc1d1
dres	dres	k1gInSc1
tyrkysový	tyrkysový	k2eAgInSc1d1
<g/>
,	,	kIx,
v	v	k7c6
sezoně	sezona	k1gFnSc6
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
je	být	k5eAaImIp3nS
venkovní	venkovní	k2eAgInSc4d1
dres	dres	k1gInSc4
poprvé	poprvé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
černý	černý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
hrudi	hruď	k1gFnSc6
měl	mít	k5eAaImAgInS
dres	dres	k1gInSc4
Barcelony	Barcelona	k1gFnSc2
až	až	k9
do	do	k7c2
sezony	sezona	k1gFnSc2
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
logo	logo	k1gNnSc1
a	a	k8xC
nápis	nápis	k1gInSc1
UNICEF	UNICEF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
sezony	sezona	k1gFnSc2
2011-12	2011-12	k4
nosil	nosit	k5eAaImAgInS
na	na	k7c6
hrudi	hruď	k1gFnSc6
logo	logo	k1gNnSc1
neziskové	ziskový	k2eNgFnSc2d1
charitativní	charitativní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
Qatar	Qatar	k1gMnSc1
Foundation	Foundation	k1gInSc1
a	a	k8xC
od	od	k7c2
sezony	sezona	k1gFnSc2
2013-14	2013-14	k4
již	již	k9
plně	plně	k6eAd1
komerční	komerční	k2eAgNnSc4d1
logo	logo	k1gNnSc4
sponzora	sponzor	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dres	dres	k1gInSc1
pro	pro	k7c4
sezonu	sezona	k1gFnSc4
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
představili	představit	k5eAaPmAgMnP
činovnící	činovnící	k2eAgMnPc1d1
klubu	klub	k1gInSc2
spolu	spolu	k6eAd1
se	s	k7c7
společností	společnost	k1gFnSc7
Nike	Nike	k1gFnSc7
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Design	design	k1gInSc1
se	se	k3xPyFc4
opět	opět	k6eAd1
vrací	vracet	k5eAaImIp3nS
ke	k	k7c3
třem	tři	k4xCgInPc3
pruhům	pruh	k1gInPc3
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
však	však	k9
v	v	k7c6
moderní	moderní	k2eAgFnSc6d1
"	"	kIx"
<g/>
rozpité	rozpitý	k2eAgFnSc6d1
<g/>
"	"	kIx"
podobě	podoba	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
1899	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_blue_stripes	_blue_stripes	k1gMnSc1
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
1910	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_fcbarcelona	_fcbarcelona	k1gFnSc1
<g/>
0	#num#	k4
<g/>
910	#num#	k4
<g/>
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
1913	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_redshoulders	_redshoulders	k1gInSc1
<g/>
|	|	kIx~
<g/>
link	link	k1gInSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
1935	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_red_stripes	_red_stripes	k1gMnSc1
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
1950	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
1955	#num#	k4
(	(	kIx(
<g/>
pro	pro	k7c4
Evropské	evropský	k2eAgInPc4d1
poháry	pohár	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_fcbarcelona	_fcbarcelona	k1gFnSc1
<g/>
0	#num#	k4
<g/>
910	#num#	k4
<g/>
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
1984	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_fcbarcelona	_fcbarcelona	k1gFnSc1
<g/>
9192	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
1992	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_bluelower	_bluelower	k1gMnSc1
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
1995	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_fcbarcelona	_fcbarcelona	k1gFnSc1
<g/>
0	#num#	k4
<g/>
708	#num#	k4
<g/>
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
1997	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
1998	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_thinblacklower	_thinblacklower	k1gMnSc1
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
2001	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_fcbarcelona	_fcbarcelona	k1gFnSc1
<g/>
0	#num#	k4
<g/>
304	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
2003	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_fcbarcelona	_fcbarcelona	k1gFnSc1
<g/>
0	#num#	k4
<g/>
405	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
2004	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_fcbarcelona	_fcbarcelona	k1gFnSc1
<g/>
0	#num#	k4
<g/>
506	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
2005	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
2006	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_fcbarcelona	_fcbarcelona	k1gFnSc1
<g/>
0	#num#	k4
<g/>
708	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
2007	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
2008	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_fcbarcelona	_fcbarcelona	k1gFnSc1
<g/>
0	#num#	k4
<g/>
910	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
2009	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_fcbarcelona	_fcbarcelona	k1gFnSc1
<g/>
1011	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
2010	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_fcbarcelona	_fcbarcelona	k1gFnSc1
<g/>
1112	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
2011	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_fcbarcelona	_fcbarcelona	k1gFnSc1
<g/>
1213	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
2012	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_fcbarcelona	_fcbarcelona	k1gFnSc1
<g/>
1314	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
2013	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
„	„	k?
<g/>
B	B	kA
<g/>
“	“	k?
</s>
<s>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
„	„	k?
<g/>
C	C	kA
<g/>
“	“	k?
</s>
<s>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
Femení	Femení	k1gNnPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
PŮVOD	původ	k1gInSc1
KLUBOVÝCH	klubový	k2eAgFnPc2d1
BAREV	barva	k1gFnPc2
FC	FC	kA
BARCELONY	Barcelona	k1gFnSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fcbarcelona	fcbarcelona	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
30.02	30.02	k4
<g/>
.2016	.2016	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
Information	Information	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
barcelona	barcelona	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cat	cat	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NOVÁK	Novák	k1gMnSc1
<g/>
,	,	kIx,
Miloslav	Miloslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legenda	legenda	k1gFnSc1
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
do	do	k7c2
Barcelony	Barcelona	k1gFnSc2
jako	jako	k8xS,k8xC
kouč	kouč	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koeman	Koeman	k1gMnSc1
podepsal	podepsat	k5eAaPmAgMnS
na	na	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-08-19	2020-08-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
A	a	k8xC
Five	Fiv	k1gMnSc4
Star	Star	kA
Stadium	stadium	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
barcelona	barcelona	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cat	cat	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
Trophy	Tropha	k1gFnSc2
Room	Rooma	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
barcelona	barcelona	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cat	cat	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
24	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ronaldinho	Ronaldin	k1gMnSc2
přestupuje	přestupovat	k5eAaImIp3nS
za	za	k7c4
21	#num#	k4
milionů	milion	k4xCgInPc2
€	€	k?
do	do	k7c2
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
↑	↑	k?
Guardiola	Guardiola	k1gFnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
stát	stát	k5eAaImF,k5eAaPmF
novým	nový	k2eAgMnSc7d1
koučem	kouč	k1gMnSc7
Barcelony	Barcelona	k1gFnSc2
<g/>
,	,	kIx,
sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
6	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
↑	↑	k?
Barcelona	Barcelona	k1gFnSc1
porazila	porazit	k5eAaPmAgFnS
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc4
na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
hřišti	hřiště	k1gNnSc6
6	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
↑	↑	k?
Barcelona	Barcelona	k1gFnSc1
získala	získat	k5eAaPmAgFnS
jubilejní	jubilejní	k2eAgFnSc1d1
20	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc1
<g/>
,	,	kIx,
čas	čas	k1gInSc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
17.05	17.05	k4
<g/>
.2010	.2010	k4
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Překvapení	překvapení	k1gNnSc1
<g/>
:	:	kIx,
Zlatý	zlatý	k2eAgInSc1d1
míč	míč	k1gInSc1
získal	získat	k5eAaPmAgInS
opět	opět	k6eAd1
Lionel	Lionel	k1gInSc1
Messi	Messe	k1gFnSc4
<g/>
,	,	kIx,
isport	isport	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
10.01	10.01	k4
<g/>
.2011	.2011	k4
<g/>
↑	↑	k?
'	'	kIx"
<g/>
Vítej	vítat	k5eAaImRp2nS
doma	doma	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
'	'	kIx"
Fabregas	Fabregas	k1gMnSc1
přestoupil	přestoupit	k5eAaPmAgMnS
do	do	k7c2
Barcelony	Barcelona	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
lidovky	lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Super	super	k2eAgInSc4d1
úlovek	úlovek	k1gInSc4
Barcy	Barca	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získala	získat	k5eAaPmAgFnS
střelce	střelec	k1gMnSc4
Sáncheze	Sáncheze	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
isport	isport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Superpohár	superpohár	k1gInSc4
rozhodne	rozhodnout	k5eAaPmIp3nS
odveta	odveta	k1gFnSc1
<g/>
,	,	kIx,
Real	Real	k1gInSc1
remizoval	remizovat	k5eAaPmAgInS
s	s	k7c7
Barcelonou	Barcelona	k1gFnSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
isport	isport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Barcelona	Barcelona	k1gFnSc1
má	mít	k5eAaImIp3nS
další	další	k2eAgFnSc4d1
trofej	trofej	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
Superpoháru	superpohár	k1gInSc6
přehrála	přehrát	k5eAaPmAgFnS
Porto	porto	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Los	los	k1gInSc1
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
:	:	kIx,
Plzeň	Plzeň	k1gFnSc1
vyfasovala	vyfasovat	k5eAaPmAgFnS
Barcelonu	Barcelona	k1gFnSc4
<g/>
,	,	kIx,
AC	AC	kA
Milán	Milán	k1gInSc1
a	a	k8xC
Borisov	Borisov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
isport	isport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nejlepším	dobrý	k2eAgMnSc7d3
fotbalistou	fotbalista	k1gMnSc7
v	v	k7c6
Evropě	Evropa	k1gFnSc6
je	být	k5eAaImIp3nS
Messi	Messe	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČTK	ČTK	kA
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Barcelona	Barcelona	k1gFnSc1
vyhrála	vyhrát	k5eAaPmAgFnS
MS	MS	kA
klubů	klub	k1gInPc2
<g/>
,	,	kIx,
ve	v	k7c6
finále	finále	k1gNnSc6
rozdrtila	rozdrtit	k5eAaPmAgFnS
brazilský	brazilský	k2eAgInSc4d1
Santos	Santos	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
Sport	sport	k1gInSc4
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Chelsea	Chelsea	k1gFnSc1
vyřadila	vyřadit	k5eAaPmAgFnS
Barcelonu	Barcelona	k1gFnSc4
a	a	k8xC
míří	mířit	k5eAaImIp3nS
do	do	k7c2
finále	finále	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Real	Real	k1gInSc1
porazil	porazit	k5eAaPmAgInS
Barcelonu	Barcelona	k1gFnSc4
a	a	k8xC
má	mít	k5eAaImIp3nS
už	už	k6eAd1
sedm	sedm	k4xCc1
bodů	bod	k1gInPc2
náskok	náskok	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
21.04	21.04	k4
<g/>
.2011	.2011	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Messi	Messe	k1gFnSc4
zničil	zničit	k5eAaPmAgInS
Espaňol	Espaňol	k1gInSc1
čtyřmi	čtyři	k4xCgInPc7
góly	gól	k1gInPc7
<g/>
!	!	kIx.
</s>
<s desamb="1">
V	v	k7c6
lize	liga	k1gFnSc6
jich	on	k3xPp3gFnPc2
nasázel	nasázet	k5eAaPmAgMnS
už	už	k6eAd1
50	#num#	k4
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
isport	isport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
0	#num#	k4
<g/>
5.05	5.05	k4
<g/>
.2011	.2011	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Four-goal	Four-goal	k1gMnSc1
Lionel	Lionel	k1gMnSc1
Messi	Messe	k1gFnSc4
gives	givesa	k1gFnPc2
Pep	Pepa	k1gFnPc2
Guardiola	Guardiola	k1gFnSc1
perfect	perfect	k1gInSc1
Barcelona	Barcelona	k1gFnSc1
send-off	send-off	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
<g/>
,	,	kIx,
0	#num#	k4
<g/>
5.05	5.05	k4
<g/>
.2011	.2011	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Slastné	slastný	k2eAgNnSc4d1
loučení	loučení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guardiola	Guardiola	k1gFnSc1
získal	získat	k5eAaPmAgInS
s	s	k7c7
Barcelonou	Barcelona	k1gFnSc7
španělský	španělský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
Sport	sport	k1gInSc4
<g/>
,	,	kIx,
25.05	25.05	k4
<g/>
.2012	.2012	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Guardiola	Guardiola	k1gFnSc1
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
končí	končit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
neprodloužit	prodloužit	k5eNaPmF
smlouvu	smlouva	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
27.04	27.04	k4
<g/>
.2012	.2012	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Messi	Messe	k1gFnSc4
vystřílel	vystřílet	k5eAaPmAgInS
Barceloně	Barcelona	k1gFnSc6
španělský	španělský	k2eAgInSc4d1
Superpohár	superpohár	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
lidovky	lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://www.fcbarcelona.com/club/the-honours/detail/card/fc-barcelona-individual-records	http://www.fcbarcelona.com/club/the-honours/detail/card/fc-barcelona-individual-records	k1gInSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
oficial	oficial	k1gMnSc1
rekords	rekords	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Barcelona	Barcelona	k1gFnSc1
slaví	slavit	k5eAaImIp3nS
mistrovský	mistrovský	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
,	,	kIx,
zařídil	zařídit	k5eAaPmAgMnS
ho	on	k3xPp3gInSc4
gólem	gól	k1gInSc7
božský	božský	k2eAgInSc1d1
Messi	Messe	k1gFnSc3
<g/>
,	,	kIx,
iSport	iSport	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
17	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
2015	#num#	k4
<g/>
↑	↑	k?
Barcelona	Barcelona	k1gFnSc1
vs	vs	k?
Paris	Paris	k1gMnSc1
Saint	Saint	k1gMnSc1
Germain	Germain	k1gMnSc1
6	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
0	#num#	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
<g/>
.	.	kIx.
rowdie	rowdie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Paris	Paris	k1gMnSc1
Saint	Saint	k1gMnSc1
Germain	Germain	k1gMnSc1
vs	vs	k?
Barcelona	Barcelona	k1gFnSc1
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
14	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
<g/>
.	.	kIx.
rowdie	rowdie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ernesto	Ernesta	k1gMnSc5
Valverde	Valverd	k1gMnSc5
is	is	k?
the	the	k?
new	new	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
coach	coach	k1gMnSc1
<g/>
.	.	kIx.
www.fcbarcelona.com	www.fcbarcelona.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Official	Official	k1gMnSc1
communique	communiqu	k1gFnSc2
from	from	k1gMnSc1
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
.	.	kIx.
www.fcbarcelona.com	www.fcbarcelona.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
INNES	INNES	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
7	#num#	k4
most	most	k1gInSc1
bizarre	bizarr	k1gInSc5
images	images	k1gInSc4
from	from	k1gInSc1
Barcelona	Barcelona	k1gFnSc1
vs	vs	k?
Las	laso	k1gNnPc2
Palmas	Palmas	k1gInSc1
<g/>
…	…	k?
played	played	k1gInSc1
behind	behind	k1gInSc1
closed	closed	k1gInSc1
doors	doors	k1gInSc1
<g/>
.	.	kIx.
mirror	mirror	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-10-01	2017-10-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Barcelona	Barcelona	k1gFnSc1
vs	vs	k?
Villarreal	Villarreal	k1gInSc1
5	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
0	#num#	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
<g/>
.	.	kIx.
rowdie	rowdie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
REUTERS	REUTERS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
win	win	k?
La	la	k1gNnSc1
Liga	liga	k1gFnSc1
title	titla	k1gFnSc6
as	as	k9
Lionel	Lionel	k1gMnSc1
Messi	Messe	k1gFnSc4
goal	goanout	k5eAaPmAgMnS
secures	secures	k1gMnSc1
victory	victor	k1gMnPc4
over	over	k1gInSc1
Levante	Levant	k1gMnSc5
<g/>
.	.	kIx.
the	the	k?
Guardian	Guardian	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-04-27	2019-04-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Liverpool	Liverpool	k1gInSc1
vs	vs	k?
Barcelona	Barcelona	k1gFnSc1
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
0	#num#	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
<g/>
.	.	kIx.
rowdie	rowdie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Barcelona	Barcelona	k1gFnSc1
vs	vs	k?
Liverpool	Liverpool	k1gInSc1
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
0	#num#	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
<g/>
.	.	kIx.
rowdie	rowdie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BURT	BURT	kA
<g/>
,	,	kIx,
Jason	Jason	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
appoint	appoint	k1gInSc4
former	former	k1gInSc1
Real	Real	k1gInSc1
Betis	Betis	k1gInSc1
coach	coach	k1gInSc4
Quique	Quiqu	k1gFnSc2
Setien	Setien	k2eAgInSc4d1
after	after	k1gInSc4
Ernesto	Ernesta	k1gFnSc5
Valverde	Valverd	k1gInSc5
sacked	sacked	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Telegraph	Telegraph	k1gMnSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
307	#num#	k4
<g/>
-	-	kIx~
<g/>
1235	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Barcelona	Barcelona	k1gFnSc1
vs	vs	k?
Real	Real	k1gInSc1
Sociedad	Sociedad	k1gInSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
Prediction	Prediction	k1gInSc1
0	#num#	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
<g/>
.	.	kIx.
rowdie	rowdie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
beat	beat	k1gInSc1
Villarreal	Villarreal	k1gInSc1
to	ten	k3xDgNnSc1
win	win	k?
La	la	k1gNnSc1
Liga	liga	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
Sport	sport	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Barcelona	Barcelona	k1gFnSc1
vs	vs	k?
Napoli	Napole	k1gFnSc4
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
Prediction	Prediction	k1gInSc1
0	#num#	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
<g/>
.	.	kIx.
rowdie	rowdie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Barcelona	Barcelona	k1gFnSc1
vs	vs	k?
Bayern	Bayern	k1gInSc1
München	München	k1gInSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
8	#num#	k4
Prediction	Prediction	k1gInSc1
14	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
<g/>
.	.	kIx.
rowdie	rowdie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Quique	Quique	k1gFnSc1
Setién	Setién	k1gInSc1
no	no	k9
longer	longer	k1gInSc1
first	first	k5eAaPmF
team	team	k1gInSc4
coach	coach	k1gInSc1
<g/>
.	.	kIx.
www.fcbarcelona.com	www.fcbarcelona.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Agreement	Agreement	k1gMnSc1
for	forum	k1gNnPc2
the	the	k?
ending	ending	k1gInSc1
of	of	k?
Éric	Éric	k1gInSc1
Abidal	Abidal	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
contract	contracta	k1gFnPc2
<g/>
.	.	kIx.
www.fcbarcelona.com	www.fcbarcelona.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ronald	Ronald	k1gMnSc1
Koeman	Koeman	k1gMnSc1
<g/>
,	,	kIx,
the	the	k?
return	return	k1gInSc1
of	of	k?
an	an	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
legend	legenda	k1gFnPc2
<g/>
.	.	kIx.
www.fcbarcelona.com	www.fcbarcelona.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bartomeu	Bartomeus	k1gInSc2
breaks	breaksa	k1gFnPc2
the	the	k?
silence	silenka	k1gFnSc3
on	on	k3xPp3gMnSc1
his	his	k1gNnSc2
conflict	conflict	k2eAgMnSc1d1
with	with	k1gMnSc1
Leo	Leo	k1gMnSc1
Messi	Messe	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FCBN	FCBN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Josep	Josep	k1gInSc4
Maria	Mario	k1gMnSc2
Bartomeu	Bartomeus	k1gInSc2
announces	announces	k1gMnSc1
the	the	k?
resignation	resignation	k1gInSc1
of	of	k?
the	the	k?
Board	Board	k1gInSc1
of	of	k?
Directors	Directors	k1gInSc1
<g/>
.	.	kIx.
www.fcbarcelona.com	www.fcbarcelona.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
"	"	kIx"
<g/>
European	European	k1gInSc1
Cups	Cupsa	k1gFnPc2
Archive	archiv	k1gInSc5
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Latin	latina	k1gFnPc2
Cup	cup	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Intercontinental	Intercontinental	k1gMnSc1
Club	club	k1gInSc4
Cup	cup	k1gInSc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
FIFA	FIFA	kA
Club	club	k1gInSc1
World	World	k1gInSc1
Cup	cup	k1gInSc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Spain	Spain	k1gMnSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Champions	Champions	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Real	Real	k1gInSc1
zaváhal	zaváhat	k5eAaPmAgInS
na	na	k7c4
Espaňolu	Espaňola	k1gFnSc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
Barceloně	Barcelona	k1gFnSc6
posvětil	posvětit	k5eAaPmAgInS
22	#num#	k4
<g/>
.	.	kIx.
španělský	španělský	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-05-12	2013-05-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
"	"	kIx"
<g/>
Spain	Spain	k1gMnSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Cup	cup	k1gInSc1
Finals	Finals	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Spain	Spain	k1gMnSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
League	League	k1gInSc1
Cup	cup	k1gInSc1
Finals	Finals	k1gInSc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Spain	Spain	k1gInSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Super	super	k2eAgInSc1d1
Cup	cup	k1gInSc1
Finals	Finalsa	k1gFnPc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Challenge	Challeng	k1gMnSc2
International	International	k1gMnSc2
du	du	k?
Sud	sud	k1gInSc1
de	de	k?
la	la	k1gNnSc2
France	Franc	k1gMnSc2
(	(	kIx(
<g/>
"	"	kIx"
<g/>
Coupe	coup	k1gInSc5
des	des	k1gNnSc1
Pyrenées	Pyrenées	k1gMnSc1
-	-	kIx~
Copa	Copa	k1gMnSc1
Pirineos	Pirineos	k1gMnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Antoni	Anton	k1gMnPc1
Closa	Closa	k1gFnSc1
i	i	k8xC
Garcia	Garcia	k1gFnSc1
<g/>
,	,	kIx,
Jaume	Jaum	k1gInSc5
Rius	Riusa	k1gFnPc2
i	i	k9
Solé	Solé	k1gNnSc1
<g/>
,	,	kIx,
Joan	Joan	k1gMnSc1
Vidal	Vidal	k1gMnSc1
i	i	k8xC
Urpí	Urpí	k1gMnSc1
<g/>
,	,	kIx,
eds	eds	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Un	Un	k1gFnSc1
Segle	Segle	k1gFnSc1
de	de	k?
futbol	futbol	k1gInSc1
català	català	k?
<g/>
:	:	kIx,
1900-2000	1900-2000	k4
(	(	kIx(
<g/>
katalánsky	katalánsky	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
Federació	Federació	k1gFnSc1
Catalana	Catalana	k1gFnSc1
de	de	k?
Futbol	Futbol	k1gInSc1
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
62	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Historia	Historium	k1gNnSc2
de	de	k?
la	la	k1gNnSc2
Supercopa	Supercop	k1gMnSc2
de	de	k?
Españ	Españ	k1gMnSc2
<g/>
:	:	kIx,
¿	¿	k?
<g/>
Sabías	Sabías	k1gInSc1
que	que	k?
durante	durant	k1gMnSc5
unos	unosit	k5eAaPmRp2nS
añ	añ	k1gFnPc2
se	se	k3xPyFc4
llamó	llamó	k?
Copa	Copa	k1gFnSc1
Eva	Eva	k1gFnSc1
de	de	k?
Duarte	Duart	k1gInSc5
y	y	k?
Perón	perón	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rfef	rfef	k1gInSc1
<g/>
.	.	kIx.
<g/>
es	es	k1gNnSc1
<g/>
,	,	kIx,
0	#num#	k4
<g/>
3.08	3.08	k4
<g/>
.2015	.2015	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Pequeñ	Pequeñ	k1gMnSc1
Copa	Copa	k1gMnSc1
del	del	k?
Mundo	Mundo	k1gNnSc4
and	and	k?
Other	Othra	k1gFnPc2
International	International	k1gFnSc1
Club	club	k1gInSc1
Tournaments	Tournaments	k1gInSc1
in	in	k?
Caracas	Caracas	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Spain	Spain	k1gMnSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Champions	Champions	k1gInSc1
of	of	k?
Catalonia	Catalonium	k1gNnSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Spain	Spain	k1gMnSc1
-	-	kIx~
Mediterranean	Mediterranean	k1gMnSc1
League	Leagu	k1gFnSc2
Libre	Libr	k1gInSc5
1937	#num#	k4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Spain	Spain	k1gMnSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Cup	cup	k1gInSc1
Winners	Winners	k1gInSc1
of	of	k?
Catalonia	Catalonium	k1gNnSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
El	Ela	k1gFnPc2
Barcelona	Barcelona	k1gFnSc1
gana	ganum	k1gNnSc2
la	la	k1gNnSc2
Supercopa	Supercop	k1gMnSc2
de	de	k?
Catalunya	Cataluny	k1gInSc2
al	ala	k1gFnPc2
Espanyol	Espanyola	k1gFnPc2
en	en	k?
la	la	k1gNnSc1
tanda	tanda	k1gMnSc1
de	de	k?
penaltis	penaltis	k1gFnSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
elpais	elpais	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
30.10	30.10	k4
<g/>
.2014	.2014	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
El	Ela	k1gFnPc2
Barça	Barçum	k1gNnSc2
se	se	k3xPyFc4
lleva	lleva	k6eAd1
la	la	k1gNnSc1
Supercopa	Supercop	k1gMnSc2
de	de	k?
Catalunya	Catalunyus	k1gMnSc2
en	en	k?
los	los	k1gInSc1
penaltis	penaltis	k1gFnSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
marca	marca	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
0	#num#	k4
<g/>
7.03	7.03	k4
<g/>
.2018	.2018	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Teresa	Teresa	k1gFnSc1
Herrera	Herrera	k1gFnSc1
(	(	kIx(
<g/>
La	la	k1gNnSc1
Coruñ	Coruñ	k1gFnPc2
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
El	Ela	k1gFnPc2
Mundo	Munda	k1gFnSc5
Deportivo	Deportiva	k1gFnSc5
<g/>
,	,	kIx,
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1	#num#	k4
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1948	#num#	k4
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
mundodeportivo	mundodeportiva	k1gFnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
El	Ela	k1gFnPc2
Mundo	Munda	k1gFnSc5
Deportivo	Deportiva	k1gFnSc5
<g/>
,	,	kIx,
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
3	#num#	k4
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1949	#num#	k4
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
mundodeportivo	mundodeportiva	k1gFnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
El	Ela	k1gFnPc2
Mundo	Munda	k1gFnSc5
Deportivo	Deportiva	k1gFnSc5
<g/>
,	,	kIx,
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1950	#num#	k4
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
mundodeportivo	mundodeportiva	k1gFnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
El	Ela	k1gFnPc2
Mundo	Munda	k1gFnSc5
Deportivo	Deportiva	k1gFnSc5
<g/>
,	,	kIx,
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1	#num#	k4
(	(	kIx(
<g/>
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1952	#num#	k4
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
mundodeportivo	mundodeportiva	k1gFnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
El	Ela	k1gFnPc2
Mundo	Munda	k1gFnSc5
Deportivo	Deportiva	k1gFnSc5
<g/>
,	,	kIx,
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1	#num#	k4
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1952	#num#	k4
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
mundodeportivo	mundodeportiva	k1gFnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
El	Ela	k1gFnPc2
Mundo	Munda	k1gFnSc5
Deportivo	Deportiva	k1gFnSc5
<g/>
,	,	kIx,
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1	#num#	k4
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1953	#num#	k4
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
mundodeportivo	mundodeportiva	k1gFnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Ramón	Ramón	k1gMnSc1
de	de	k?
Carranza	Carranza	k1gFnSc1
(	(	kIx(
<g/>
Cádiz-Spain	Cádiz-Spain	k1gInSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Joan	Joan	k1gMnSc1
Gamper	Gamper	k1gMnSc1
(	(	kIx(
<g/>
Barcelona-Spain	Barcelona-Spain	k1gMnSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Coupe	coup	k1gInSc5
Mohamed	Mohamed	k1gMnSc1
V	V	kA
(	(	kIx(
<g/>
Casablanca	Casablanca	k1gFnSc1
-	-	kIx~
Morocco	Morocco	k1gMnSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Festa	Festa	k?
d	d	k?
<g/>
'	'	kIx"
<g/>
Elx	Elx	k1gMnSc1
(	(	kIx(
<g/>
Elche	Elche	k1gInSc1
<g/>
,	,	kIx,
Alicante-Spain	Alicante-Spain	k1gInSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Costa	Costa	k1gMnSc1
del	del	k?
Sol	sol	k1gInSc1
-	-	kIx~
Ciudad	Ciudad	k1gInSc1
de	de	k?
Málaga	Málaga	k1gFnSc1
(	(	kIx(
<g/>
Málaga-Spain	Málaga-Spain	k1gInSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Alicante-Costa	Alicante-Costa	k1gMnSc1
Blanca-Ciudad	Blanca-Ciudad	k1gInSc1
de	de	k?
Alicante-Ayuntamiento	Alicante-Ayuntamiento	k1gNnSc1
(	(	kIx(
<g/>
Alicante-Spain	Alicante-Spain	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k6eAd1
Ciudad	Ciudad	k1gInSc1
de	de	k?
Zaragoza	Zaragoza	k1gFnSc1
"	"	kIx"
Memorial	Memorial	k1gInSc1
Carlos	Carlos	k1gMnSc1
Lapetra	Lapetra	k1gFnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Zaragoza-Spain	Zaragoza-Spain	k1gInSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Naranja	Naranja	k1gMnSc1
(	(	kIx(
<g/>
Valencia-Spain	Valencia-Spain	k1gMnSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Ciutat	Ciutat	k1gMnSc1
de	de	k?
Barcelona	Barcelona	k1gFnSc1
(	(	kIx(
<g/>
Barcelona-Spain	Barcelona-Spain	k1gInSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Semana	Seman	k1gMnSc2
del	del	k?
Sol-Ciudad	Sol-Ciudad	k1gInSc1
de	de	k?
Marbella	Marbella	k1gFnSc1
(	(	kIx(
<g/>
Marbella	Marbella	k1gMnSc1
<g/>
,	,	kIx,
Málaga-Spain	Málaga-Spain	k1gMnSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k1gMnSc1
Ciutat	Ciutat	k1gMnSc1
de	de	k?
Lleida	Lleida	k1gFnSc1
(	(	kIx(
<g/>
Lleida-Spain	Lleida-Spain	k1gInSc1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Amsterdam	Amsterdam	k1gInSc1
Tournament	Tournament	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Bayern	Bayern	k1gInSc1
gegen	gegen	k1gInSc1
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
Scholl	Scholl	k1gMnSc1
feiert	feiert	k1gMnSc1
<g/>
,	,	kIx,
Messi	Messe	k1gFnSc4
trifft	trifft	k5eAaPmF
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
spiegel	spiegel	k1gInSc1
<g/>
.	.	kIx.
<g/>
de	de	k?
<g/>
,	,	kIx,
15.08	15.08	k4
<g/>
.2007	.2007	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Barca	Barca	k1gFnSc1
win	win	k?
Audi	Audi	k1gNnSc1
Cup	cup	k1gInSc1
<g/>
,	,	kIx,
Hernandez	Hernandez	k1gMnSc1
out	out	k?
for	forum	k1gNnPc2
Saturday	Saturda	k2eAgFnPc1d1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
barcablaugranes	barcablaugranes	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
28.07	28.07	k4
<g/>
.2011	.2011	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
PSG-Barcelone	PSG-Barcelon	k1gInSc5
2-2	2-2	k4
:	:	kIx,
une	une	k?
soirée	soirée	k1gFnSc1
pleine	pleinout	k5eAaPmIp3nS
de	de	k?
promesses	promesses	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
leparisien	leparisien	k1gInSc1
<g/>
.	.	kIx.
<g/>
fr	fr	k0
<g/>
,	,	kIx,
0	#num#	k4
<g/>
4.08	4.08	k4
<g/>
.2012	.2012	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Trofeo	Trofeo	k6eAd1
Colombino	Colombin	k2eAgNnSc1d1
(	(	kIx(
<g/>
Huelva-Spain	Huelva-Spain	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Barcelona	Barcelona	k1gFnSc1
claimed	claimed	k1gInSc1
the	the	k?
International	International	k1gFnSc1
Champions	Champions	k1gInSc1
Cup	cup	k1gInSc1
after	after	k1gInSc1
prevailing	prevailing	k1gInSc1
3-2	3-2	k4
in	in	k?
a	a	k8xC
thrilling	thrilling	k1gInSc1
Clasico	Clasico	k6eAd1
encounter	encounter	k1gInSc1
against	against	k5eAaPmF
bitter	bitter	k1gInSc4
rivals	rivals	k1gInSc1
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
internationalchampionscup	internationalchampionscup	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
31.07	31.07	k4
<g/>
.2017	.2017	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Barcelona	Barcelona	k1gFnSc1
are	ar	k1gInSc5
winners	winnersa	k1gFnPc2
of	of	k?
the	the	k?
inaugural	inaugurat	k5eAaPmAgInS,k5eAaImAgInS
La	la	k1gNnSc4
Liga-Serie	Liga-Serie	k1gFnSc2
A	a	k9
Cup	cup	k1gInSc1
today	todaa	k1gMnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
4-0	4-0	k4
thrashing	thrashing	k1gInSc1
of	of	k?
Napoli	Napole	k1gFnSc4
<g/>
,	,	kIx,
winning	winning	k1gInSc4
6-1	6-1	k4
on	on	k3xPp3gMnSc1
aggregate	aggregat	k1gInSc5
<g/>
.	.	kIx.
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
theguardian	theguardian	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
11.08	11.08	k4
<g/>
.2019	.2019	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Youth	Youth	k1gMnSc1
gets	getsa	k1gFnPc2
its	its	k?
chance	chanec	k1gInSc2
to	ten	k3xDgNnSc1
shine	shinout	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
uefa	uefa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Spain	Spain	k1gMnSc1
-	-	kIx~
U-19	U-19	k1gMnSc1
League	Leagu	k1gFnSc2
Champions	Champions	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Spain	Spain	k1gMnSc1
-	-	kIx~
U-19	U-19	k1gFnSc1
Cup	cup	k1gInSc1
History	Histor	k1gInPc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Ligas	Ligas	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
bdfutbol	bdfutbol	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Žebříček	žebříček	k1gInSc1
<g/>
:	:	kIx,
Nejbohatší	bohatý	k2eAgInSc1d3
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
světa	svět	k1gInSc2
je	být	k5eAaImIp3nS
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
iHned	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
El	Ela	k1gFnPc2
clásico	clásico	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
barcelona	barcelona	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
El	Ela	k1gFnPc2
Derbi	Derb	k1gMnSc6
Barceloní	Barceloň	k1gFnSc7
–	–	k?
The	The	k1gFnSc1
History	Histor	k1gInPc4
Behind	Behind	k1gMnSc1
the	the	k?
Barcelona	Barcelona	k1gFnSc1
Derby	derby	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
barca	barca	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
History	Histor	k1gInPc1
of	of	k?
the	the	k?
crest	crest	k1gInSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
https://sport.ihned.cz/fotbal/zahranici/fotbalova-barcelona-zmeni-znak-zmizi-z-nej-i-jeden-z-nejtrad/r~47ab8406c29911e89f96ac1f6b220ee8/	https://sport.ihned.cz/fotbal/zahranici/fotbalova-barcelona-zmeni-znak-zmizi-z-nej-i-jeden-z-nejtrad/r~47ab8406c29911e89f96ac1f6b220ee8/	k4
Fotbalová	fotbalový	k2eAgFnSc1d1
Barcelona	Barcelona	k1gFnSc1
změní	změnit	k5eAaPmIp3nS
znak	znak	k1gInSc4
<g/>
,	,	kIx,
zmizí	zmizet	k5eAaPmIp3nS
z	z	k7c2
něj	on	k3xPp3gMnSc2
i	i	k9
jeden	jeden	k4xCgInSc4
z	z	k7c2
nejtradičnějších	tradiční	k2eAgInPc2d3
prvků	prvek	k1gInPc2
<g/>
↑	↑	k?
An	An	k1gMnSc2
artful	artfout	k5eAaPmAgMnS
presentation	presentation	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
22.05	22.05	k4
<g/>
.2012	.2012	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
(	(	kIx(
<g/>
katalánsky	katalánsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
arabsky	arabsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
indonésky	indonésky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
portugalsky	portugalsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
turecky	turecky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
stadionu	stadion	k1gInSc6
-	-	kIx~
FotbaloveStadiony	FotbaloveStadion	k1gInPc7
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Fanouškovské	fanouškovský	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Profil	profil	k1gInSc1
klubu	klub	k1gInSc2
na	na	k7c6
fotbalportal	fotbalportat	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Primera	primera	k1gFnSc1
División	División	k1gInSc1
–	–	k?
španělská	španělský	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
ve	v	k7c6
fotbale	fotbal	k1gInSc6
(	(	kIx(
<g/>
ročníky	ročník	k1gInPc1
<g/>
,	,	kIx,
týmy	tým	k1gInPc1
<g/>
,	,	kIx,
ocenění	ocenění	k1gNnSc1
<g/>
,	,	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
Kluby	klub	k1gInPc7
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
(	(	kIx(
<g/>
20	#num#	k4
mužstev	mužstvo	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Deportivo	Deportiva	k1gFnSc5
Alavés	Alavésa	k1gFnPc2
•	•	k?
Athletic	Athletice	k1gFnPc2
Bilbao	Bilbao	k1gMnSc1
•	•	k?
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
Cádiz	Cádiz	k1gInSc1
CF	CF	kA
•	•	k?
Celta	celta	k1gFnSc1
de	de	k?
Vigo	Vigo	k1gMnSc1
•	•	k?
CA	ca	kA
Osasuna	Osasuna	k1gFnSc1
•	•	k?
SD	SD	kA
Eibar	Eibar	k1gMnSc1
•	•	k?
Elche	Elche	k1gNnPc2
CF	CF	kA
•	•	k?
Getafe	Getaf	k1gInSc5
CF	CF	kA
•	•	k?
Granada	Granada	k1gFnSc1
CF	CF	kA
•	•	k?
SD	SD	kA
Huesca	Huesc	k2eAgMnSc4d1
•	•	k?
Levante	Levant	k1gMnSc5
UD	UD	kA
•	•	k?
Real	Real	k1gInSc1
Valladolid	Valladolid	k1gInSc1
•	•	k?
Betis	Betis	k1gInSc1
Sevilla	Sevilla	k1gFnSc1
•	•	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
•	•	k?
Real	Real	k1gInSc1
Sociedad	Sociedad	k1gInSc1
•	•	k?
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
•	•	k?
Valencia	Valencia	k1gFnSc1
CF	CF	kA
•	•	k?
Villarreal	Villarreal	k1gInSc1
CF	CF	kA
Sezóny	sezóna	k1gFnSc2
</s>
<s>
1929	#num#	k4
•	•	k?
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
•	•	k?
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
•	•	k?
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
•	•	k?
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
•	•	k?
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
•	•	k?
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
•	•	k?
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
•	•	k?
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
•	•	k?
1937	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
38	#num#	k4
•	•	k?
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
•	•	k?
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
•	•	k?
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
•	•	k?
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
•	•	k?
1942	#num#	k4
<g/>
/	/	kIx~
<g/>
43	#num#	k4
•	•	k?
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
•	•	k?
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
•	•	k?
1945	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
46	#num#	k4
•	•	k?
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
•	•	k?
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
•	•	k?
1948	#num#	k4
<g/>
/	/	kIx~
<g/>
49	#num#	k4
•	•	k?
1949	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
•	•	k?
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
•	•	k?
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
•	•	k?
1952	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
•	•	k?
1953	#num#	k4
<g/>
/	/	kIx~
<g/>
54	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
•	•	k?
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
•	•	k?
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
•	•	k?
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
•	•	k?
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
•	•	k?
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
•	•	k?
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
•	•	k?
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
•	•	k?
1962	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
63	#num#	k4
•	•	k?
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
•	•	k?
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
•	•	k?
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
•	•	k?
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
•	•	k?
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
•	•	k?
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
•	•	k?
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
•	•	k?
1970	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
71	#num#	k4
•	•	k?
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
•	•	k?
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
•	•	k?
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
•	•	k?
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
•	•	k?
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
•	•	k?
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
•	•	k?
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
•	•	k?
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
•	•	k?
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
•	•	k?
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
•	•	k?
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
•	•	k?
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
•	•	k?
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
•	•	k?
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
•	•	k?
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
•	•	k?
1987	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
88	#num#	k4
•	•	k?
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
•	•	k?
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
•	•	k?
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
•	•	k?
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
•	•	k?
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
•	•	k?
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
•	•	k?
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
96	#num#	k4
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
3	#num#	k4
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
Dřívější	dřívější	k2eAgMnPc1d1
účastníci	účastník	k1gMnPc1
(	(	kIx(
<g/>
poslední	poslední	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CE	CE	kA
Europa	Europa	k1gFnSc1
(	(	kIx(
<g/>
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Real	Real	k1gInSc1
Unión	Unión	k1gInSc1
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Arenas	Arenas	k1gInSc1
Club	club	k1gInSc1
de	de	k?
Getxo	Getxo	k1gMnSc1
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CD	CD	kA
Alcoyano	Alcoyana	k1gFnSc5
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Atlético	Atlético	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Tetuán	Tetuán	k1gInSc1
(	(	kIx(
<g/>
1951	#num#	k4
<g/>
/	/	kIx~
<g/>
52	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cultural	Cultural	k1gFnSc1
y	y	k?
Deportiva	Deportiva	k1gFnSc1
Leonesa	Leonesa	k1gFnSc1
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CD	CD	kA
Condal	Condal	k1gFnSc2
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Real	Real	k1gInSc1
Jaén	Jaén	k1gInSc1
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pontevedra	Pontevedra	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
CF	CF	kA
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Burgos	Burgos	k1gInSc1
CF	CF	kA
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
)	)	kIx)
•	•	k?
AD	ad	k7c4
Almería	Almería	k1gMnSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CE	CE	kA
Sabadell	Sabadell	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CD	CD	kA
Castellón	Castellón	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Real	Real	k1gInSc1
Burgos	Burgos	k1gMnSc1
CF	CF	kA
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
)	)	kIx)
•	•	k?
UE	UE	kA
Lleida	Lleida	k1gFnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CD	CD	kA
Logroñ	Logroñ	k1gInSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SD	SD	kA
Compostela	Compostela	k1gFnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CP	CP	kA
Mérida	Mérida	k1gFnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CF	CF	kA
Extremadura	Extremadura	k1gFnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
•	•	k?
UD	UD	kA
Salamanca	Salamanca	k1gMnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Real	Real	k1gInSc1
Oviedo	Oviedo	k1gNnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Albacete	Albace	k1gNnSc2
Balompié	Balompiá	k1gFnSc2
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cádiz	Cádiz	k1gInSc1
CF	CF	kA
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gimnà	Gimnà	k1gFnPc2
de	de	k?
Tarragona	Tarragon	k1gMnSc2
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Real	Real	k1gInSc1
Murcia	Murcia	k1gFnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CD	CD	kA
Numancia	Numancium	k1gNnSc2
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Recreativo	Recreativa	k1gFnSc5
de	de	k?
Huelva	Huelvo	k1gNnPc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
•	•	k?
CD	CD	kA
Tenerife	Tenerif	k1gInSc5
(	(	kIx(
<g/>
2009	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Xerez	Xerez	k1gInSc1
CD	CD	kA
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hércules	Hércules	k1gInSc1
CF	CF	kA
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Racing	Racing	k1gInSc1
de	de	k?
Santander	Santander	k1gInSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Real	Real	k1gInSc1
Zaragoza	Zaragoza	k1gFnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
13	#num#	k4
<g/>
)	)	kIx)
•	•	k?
UD	UD	kA
Almería	Almería	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Córdoba	Córdoba	k1gFnSc1
CF	CF	kA
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Elche	Elche	k1gInSc1
CF	CF	kA
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sporting	Sporting	k1gInSc1
de	de	k?
Gijón	Gijón	k1gInSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Deportivo	Deportiva	k1gFnSc5
de	de	k?
La	la	k1gNnSc1
Coruñ	Coruñ	k1gMnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
UD	UD	kA
Las	laso	k1gNnPc2
Palmas	Palmas	k1gInSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Málaga	Málaga	k1gFnSc1
CF	CF	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Girona	Girona	k1gFnSc1
FC	FC	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rayo	Rayo	k6eAd1
Vallecano	Vallecana	k1gFnSc5
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SD	SD	kA
Huesca	Huesca	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
Trofeje	trofej	k1gInSc2
a	a	k8xC
ocenění	ocenění	k1gNnSc2
</s>
<s>
Premios	Premios	k1gMnSc1
LFP	LFP	kA
•	•	k?
Trofeo	Trofeo	k1gMnSc1
Pichichi	Pichich	k1gFnSc2
•	•	k?
Premio	Premio	k1gMnSc1
Don	Don	k1gMnSc1
Balón	balón	k1gInSc1
•	•	k?
Premios	Premios	k1gInSc1
Santander	Santander	k1gMnSc1
•	•	k?
Trofeo	Trofeo	k1gMnSc1
Zarra	Zarra	k1gMnSc1
•	•	k?
Trofeo	Trofeo	k6eAd1
Ricardo	Ricardo	k1gNnSc1
Zamora	Zamor	k1gMnSc2
•	•	k?
Trofeo	Trofeo	k1gMnSc1
Miguel	Miguel	k1gMnSc1
Muñ	Muñ	k1gMnSc1
•	•	k?
Trofeo	Trofeo	k1gMnSc1
Guruceta	Guruceto	k1gNnSc2
•	•	k?
Trofeo	Trofeo	k1gMnSc1
Guruceta	Gurucet	k1gMnSc2
Jiné	jiný	k2eAgInPc1d1
odkazy	odkaz	k1gInPc7
</s>
<s>
Španělský	španělský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
•	•	k?
Španělský	španělský	k2eAgInSc1d1
ligový	ligový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
•	•	k?
Španělský	španělský	k2eAgInSc4d1
superpohár	superpohár	k1gInSc4
Fotbal	fotbal	k1gInSc1
ženy	žena	k1gFnSc2
<g/>
:	:	kIx,
Primera	primera	k1gFnSc1
División	División	k1gInSc1
Femenina	Femenina	k1gFnSc1
de	de	k?
Españ	Españ	k2eAgFnSc1d1
•	•	k?
Copa	Copa	k1gFnSc1
de	de	k?
la	la	k1gNnSc1
Reina	Rein	k2eAgNnSc2d1
de	de	k?
Fútbol	Fútbol	k1gInSc1
•	•	k?
Španělská	španělský	k2eAgFnSc1d1
ženská	ženský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Vítězové	vítěz	k1gMnPc1
-	-	kIx~
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1972	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1973	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1975	#num#	k4
FK	FK	kA
Dynamo	dynamo	k1gNnSc1
Kyjev	Kyjev	k1gInSc1
•	•	k?
1976	#num#	k4
RSC	RSC	kA
Anderlecht	Anderlechta	k1gFnPc2
•	•	k?
1977	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
1978	#num#	k4
RSC	RSC	kA
Anderlecht	Anderlechta	k1gFnPc2
•	•	k?
1979	#num#	k4
Nottingham	Nottingham	k1gInSc1
Forest	Forest	k1gInSc4
FC	FC	kA
•	•	k?
1980	#num#	k4
Valencia	Valencius	k1gMnSc2
CF	CF	kA
•	•	k?
1982	#num#	k4
Aston	Aston	k1gNnSc4
Villa	Villo	k1gNnSc2
FC	FC	kA
•	•	k?
1983	#num#	k4
Aberdeen	Aberdeen	k1gInSc1
FC	FC	kA
•	•	k?
1984	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1986	#num#	k4
FC	FC	kA
Steaua	Steau	k1gInSc2
Bucureș	Bucureș	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1987	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
•	•	k?
1988	#num#	k4
KV	KV	kA
Mechelen	Mechelna	k1gFnPc2
•	•	k?
1989	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1990	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1991	#num#	k4
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
•	•	k?
1992	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
1993	#num#	k4
AC	AC	kA
Parma	Parma	k1gFnSc1
•	•	k?
1994	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1995	#num#	k4
Ajax	Ajax	k1gInSc1
Amsterdam	Amsterdam	k1gInSc4
•	•	k?
1996	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1997	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
1998	#num#	k4
Chelsea	Chelseus	k1gMnSc2
FC	FC	kA
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1999	#num#	k4
Lazio	Lazio	k6eAd1
Řím	Řím	k1gInSc1
•	•	k?
2000	#num#	k4
Galatasaray	Galatasaraa	k1gMnSc2
SK	Sk	kA
•	•	k?
2001	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2002	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2003	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2004	#num#	k4
Valencia	Valencius	k1gMnSc2
CF	CF	kA
•	•	k?
2005	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2006	#num#	k4
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
•	•	k?
2007	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2008	#num#	k4
Zenit	zenit	k1gInSc1
Petrohrad	Petrohrad	k1gInSc4
•	•	k?
2009	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2010	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2011	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2012	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2013	#num#	k4
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
•	•	k?
2014	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2015	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2016	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2017	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2018	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
2019	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2020	#num#	k4
FC	FC	kA
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
•	•	k?
2021	#num#	k4
•	•	k?
2022	#num#	k4
•	•	k?
2023	#num#	k4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
-	-	kIx~
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
-	-	kIx~
předchůdce	předchůdce	k1gMnSc2
</s>
<s>
1960	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
1961	#num#	k4
CA	ca	kA
Peñ	Peñ	k1gFnPc2
•	•	k?
1962	#num#	k4
Santos	Santos	k1gInSc1
FC	FC	kA
•	•	k?
1963	#num#	k4
Santos	Santos	k1gInSc1
FC	FC	kA
•	•	k?
1964	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
1965	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
1966	#num#	k4
CA	ca	kA
Peñ	Peñ	k1gFnPc2
•	•	k?
1967	#num#	k4
Racing	Racing	k1gInSc1
Club	club	k1gInSc4
•	•	k?
1968	#num#	k4
Estudiantes	Estudiantes	k1gInSc1
de	de	k?
La	la	k1gNnSc1
Plata	plato	k1gNnSc2
•	•	k?
1969	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1970	#num#	k4
Feyenoord	Feyenoord	k1gInSc1
•	•	k?
1971	#num#	k4
Nacional	Nacional	k1gFnSc6
Montevideo	Montevideo	k1gNnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1972	#num#	k4
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
•	•	k?
1973	#num#	k4
CA	ca	kA
Independiente	Independient	k1gInSc5
•	•	k?
1974	#num#	k4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
•	•	k?
1975	#num#	k4
Ročník	ročník	k1gInSc1
neodehrán	odehrát	k5eNaPmNgInS
•	•	k?
1976	#num#	k4
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc4
•	•	k?
1977	#num#	k4
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
•	•	k?
1978	#num#	k4
Ročník	ročník	k1gInSc1
neodehrán	odehrát	k5eNaPmNgInS
•	•	k?
1979	#num#	k4
Club	club	k1gInSc1
Olimpia	Olimpium	k1gNnSc2
•	•	k?
1980	#num#	k4
Nacional	Nacional	k1gFnSc6
Montevideo	Montevideo	k1gNnSc1
•	•	k?
1981	#num#	k4
Flamengo	flamengo	k1gNnSc4
•	•	k?
1982	#num#	k4
CA	ca	kA
Peñ	Peñ	k1gFnPc2
•	•	k?
1983	#num#	k4
Grê	Grê	k1gNnSc4
•	•	k?
1984	#num#	k4
CA	ca	kA
Independiente	Independient	k1gInSc5
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1985	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1986	#num#	k4
CA	ca	kA
River	Rivra	k1gFnPc2
Plate	plat	k1gInSc5
•	•	k?
1987	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
•	•	k?
1988	#num#	k4
Nacional	Nacional	k1gFnSc6
Montevideo	Montevideo	k1gNnSc1
•	•	k?
1989	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1990	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
1991	#num#	k4
FK	FK	kA
Crvena	Crven	k2eAgFnSc1d1
zvezda	zvezda	k1gFnSc1
•	•	k?
1992	#num#	k4
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
•	•	k?
1993	#num#	k4
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
•	•	k?
1994	#num#	k4
CA	ca	kA
Vélez	Vélez	k1gInSc1
Sarsfield	Sarsfield	k1gInSc1
•	•	k?
1995	#num#	k4
AFC	AFC	kA
Ajax	Ajax	k1gInSc1
•	•	k?
1996	#num#	k4
Juventus	Juventus	k1gInSc1
FC	FC	kA
•	•	k?
1997	#num#	k4
Borussia	Borussium	k1gNnSc2
Dortmund	Dortmund	k1gInSc1
•	•	k?
1998	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
1999	#num#	k4
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
•	•	k?
2000	#num#	k4
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
•	•	k?
2001	#num#	k4
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc4
•	•	k?
2002	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2003	#num#	k4
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
•	•	k?
2004	#num#	k4
FC	FC	kA
Porto	porto	k1gNnSc1
Mistrovství	mistrovství	k1gNnPc2
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
</s>
<s>
2000	#num#	k4
SC	SC	kA
Corinthians	Corinthiansa	k1gFnPc2
Paulista	Paulista	k1gMnSc1
•	•	k?
2001-2004	2001-2004	k4
•	•	k?
2005	#num#	k4
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
FC	FC	kA
•	•	k?
2006	#num#	k4
Sport	sport	k1gInSc1
Club	club	k1gInSc4
Internacional	Internacional	k1gFnSc2
•	•	k?
2007	#num#	k4
AC	AC	kA
Milán	Milán	k1gInSc1
•	•	k?
2008	#num#	k4
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
FC	FC	kA
•	•	k?
2009	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2010	#num#	k4
FC	FC	kA
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
•	•	k?
2011	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2012	#num#	k4
SC	SC	kA
Corinthians	Corinthiansa	k1gFnPc2
Paulista	Paulista	k1gMnSc1
•	•	k?
2013	#num#	k4
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc4
•	•	k?
2014	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2015	#num#	k4
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
•	•	k?
2016	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2017	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2018	#num#	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
•	•	k?
2019	#num#	k4
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
•	•	k?
2020	#num#	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
118149	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
2102831-X	2102831-X	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0805	#num#	k4
9654	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2007075876	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
130104837	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2007075876	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
|	|	kIx~
Španělsko	Španělsko	k1gNnSc1
</s>
