<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Španělska	Španělsko	k1gNnSc2	Španělsko
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
červené	červená	k1gFnSc2	červená
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgFnSc2d1	žlutá
a	a	k8xC	a
červené	červený	k2eAgFnSc2d1	červená
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
odvozená	odvozený	k2eAgFnSc1d1	odvozená
ze	z	k7c2	z
středověkého	středověký	k2eAgInSc2d1	středověký
aragonského	aragonský	k2eAgInSc2d1	aragonský
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nepravdivé	pravdivý	k2eNgFnSc2d1	nepravdivá
legendy	legenda	k1gFnSc2	legenda
západofranský	západofranský	k2eAgMnSc1d1	západofranský
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
Lysý	Lysý	k1gMnSc1	Lysý
(	(	kIx(	(
<g/>
848	[number]	k4	848
<g/>
–	–	k?	–
<g/>
877	[number]	k4	877
<g/>
)	)	kIx)	)
po	po	k7c6	po
boji	boj	k1gInSc6	boj
s	s	k7c7	s
Maury	Maur	k1gMnPc7	Maur
ponořil	ponořit	k5eAaPmAgMnS	ponořit
prsty	prst	k1gInPc4	prst
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
svého	svůj	k3xOyFgMnSc2	svůj
zraněného	zraněný	k1gMnSc2	zraněný
spojence	spojenec	k1gMnSc4	spojenec
<g/>
,	,	kIx,	,
aragonského	aragonský	k2eAgMnSc4d1	aragonský
hraběte	hrabě	k1gMnSc4	hrabě
Wilfreda	Wilfred	k1gMnSc2	Wilfred
I.	I.	kA	I.
<g/>
,	,	kIx,	,
a	a	k8xC	a
otřel	otřít	k5eAaPmAgMnS	otřít
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
doposud	doposud	k6eAd1	doposud
prázdný	prázdný	k2eAgInSc1d1	prázdný
štít	štít	k1gInSc1	štít
<g/>
;	;	kIx,	;
tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Červeno-žluto-červená	Červeno-žluto-červený	k2eAgFnSc1d1	Červeno-žluto-červený
vlajka	vlajka	k1gFnSc1	vlajka
pro	pro	k7c4	pro
válečné	válečný	k2eAgNnSc4d1	válečné
loďstvo	loďstvo	k1gNnSc4	loďstvo
byla	být	k5eAaImAgFnS	být
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
roku	rok	k1gInSc2	rok
1785	[number]	k4	1785
(	(	kIx(	(
<g/>
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
Kastilie	Kastilie	k1gFnSc2	Kastilie
a	a	k8xC	a
Leónu	León	k1gInSc2	León
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
1793	[number]	k4	1793
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vlajkou	vlajka	k1gFnSc7	vlajka
státní	státní	k2eAgFnPc1d1	státní
<g/>
.	.	kIx.	.
</s>
<s>
Revoluce	revoluce	k1gFnSc1	revoluce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
nahradila	nahradit	k5eAaPmAgFnS	nahradit
dolní	dolní	k2eAgInSc4d1	dolní
červený	červený	k2eAgInSc4d1	červený
pruh	pruh	k1gInSc4	pruh
barvou	barvý	k2eAgFnSc4d1	barvá
fialovou	fialový	k2eAgFnSc4d1	fialová
a	a	k8xC	a
královský	královský	k2eAgInSc4d1	královský
znak	znak	k1gInSc4	znak
znakem	znak	k1gInSc7	znak
republikánským	republikánský	k2eAgInSc7d1	republikánský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
znovu	znovu	k6eAd1	znovu
používala	používat	k5eAaImAgFnS	používat
královská	královský	k2eAgFnSc1d1	královská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
republika	republika	k1gFnSc1	republika
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1931	[number]	k4	1931
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
k	k	k7c3	k
trikolóře	trikolóra	k1gFnSc3	trikolóra
složené	složený	k2eAgFnSc3d1	složená
ze	z	k7c2	z
třech	tři	k4xCgInPc2	tři
stejně	stejně	k6eAd1	stejně
širokých	široký	k2eAgInPc2d1	široký
pruhů	pruh	k1gInPc2	pruh
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
červené	červená	k1gFnSc2	červená
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgFnPc4d1	žlutá
a	a	k8xC	a
fialové	fialový	k2eAgFnPc4d1	fialová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
diktátor	diktátor	k1gMnSc1	diktátor
Franco	Franco	k1gMnSc1	Franco
obnovil	obnovit	k5eAaPmAgMnS	obnovit
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
červeno-žluto-červené	červeno-žluto-červený	k2eAgInPc4d1	červeno-žluto-červený
pruhy	pruh	k1gInPc4	pruh
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
doplněné	doplněný	k2eAgInPc1d1	doplněný
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
dosavadním	dosavadní	k2eAgInSc7d1	dosavadní
republikánským	republikánský	k2eAgInSc7d1	republikánský
znakem	znak	k1gInSc7	znak
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
znakem	znak	k1gInSc7	znak
frankistickým	frankistický	k2eAgInSc7d1	frankistický
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
znak	znak	k1gInSc1	znak
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
má	mít	k5eAaImIp3nS	mít
čtvrcený	čtvrcený	k2eAgInSc1d1	čtvrcený
štít	štít	k1gInSc1	štít
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
jednotlivá	jednotlivý	k2eAgFnSc1d1	jednotlivá
pole	pole	k1gFnSc1	pole
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
Kastilii	Kastilie	k1gFnSc4	Kastilie
(	(	kIx(	(
<g/>
v	v	k7c6	v
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
zlatý	zlatý	k2eAgInSc4d1	zlatý
hrad	hrad	k1gInSc4	hrad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
León	León	k1gMnSc1	León
(	(	kIx(	(
<g/>
ve	v	k7c6	v
stříbrném	stříbrný	k2eAgNnSc6d1	stříbrné
poli	pole	k1gNnSc6	pole
červený	červený	k2eAgInSc4d1	červený
<g/>
,	,	kIx,	,
zlatě	zlatě	k6eAd1	zlatě
lemovaný	lemovaný	k2eAgInSc4d1	lemovaný
lev	lev	k1gInSc4	lev
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Aragonii	Aragonie	k1gFnSc4	Aragonie
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zlatém	zlatý	k2eAgNnSc6d1	Zlaté
poli	pole	k1gNnSc6	pole
čtyři	čtyři	k4xCgInPc4	čtyři
červené	červený	k2eAgInPc4d1	červený
pruhy	pruh	k1gInPc4	pruh
<g/>
)	)	kIx)	)
a	a	k8xC	a
Navarru	Navarra	k1gFnSc4	Navarra
(	(	kIx(	(
<g/>
v	v	k7c6	v
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
zlatý	zlatý	k2eAgInSc4d1	zlatý
řetěz	řetěz	k1gInSc4	řetěz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zespodu	zespodu	k7c2	zespodu
vsunuté	vsunutý	k2eAgFnSc2d1	vsunutá
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
pole	pole	k1gFnSc2	pole
s	s	k7c7	s
granátovým	granátový	k2eAgNnSc7d1	granátové
jablkem	jablko	k1gNnSc7	jablko
v	v	k7c6	v
přirozených	přirozený	k2eAgFnPc6d1	přirozená
barvách	barva	k1gFnPc6	barva
patří	patřit	k5eAaImIp3nS	patřit
Granadě	Granada	k1gFnSc3	Granada
<g/>
.	.	kIx.	.
</s>
<s>
Modrý	modrý	k2eAgInSc1d1	modrý
srdcový	srdcový	k2eAgInSc1d1	srdcový
štítek	štítek	k1gInSc1	štítek
s	s	k7c7	s
červenou	červený	k2eAgFnSc7d1	červená
bordurou	bordura	k1gFnSc7	bordura
a	a	k8xC	a
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
zlatými	zlatý	k2eAgFnPc7d1	zlatá
liliemi	lilie	k1gFnPc7	lilie
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
náleží	náležet	k5eAaImIp3nP	náležet
vládnoucí	vládnoucí	k2eAgFnSc4d1	vládnoucí
dynastii	dynastie	k1gFnSc4	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
štítem	štít	k1gInSc7	štít
je	být	k5eAaImIp3nS	být
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
ještě	ještě	k9	ještě
nad	nad	k7c7	nad
levým	levý	k2eAgInSc7d1	levý
stříbrným	stříbrný	k2eAgInSc7d1	stříbrný
sloupem	sloup	k1gInSc7	sloup
doprovázejícím	doprovázející	k2eAgInSc7d1	doprovázející
štít	štít	k1gInSc4	štít
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nad	nad	k7c7	nad
pravým	pravý	k2eAgInSc7d1	pravý
sloupem	sloup	k1gInSc7	sloup
je	být	k5eAaImIp3nS	být
císařská	císařský	k2eAgFnSc1d1	císařská
koruna	koruna	k1gFnSc1	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Herkulovy	Herkulův	k2eAgInPc1d1	Herkulův
sloupy	sloup	k1gInPc1	sloup
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
Gibraltar	Gibraltar	k1gInSc4	Gibraltar
a	a	k8xC	a
Ceutu	Ceuta	k1gFnSc4	Ceuta
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ovinuté	ovinutý	k2eAgInPc1d1	ovinutý
červenými	červený	k2eAgFnPc7d1	červená
stuhami	stuha	k1gFnPc7	stuha
s	s	k7c7	s
devizou	deviza	k1gFnSc7	deviza
PLUS	plus	k1gInSc4	plus
ULTRA	ultra	k2eAgInSc4d1	ultra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
znělo	znět	k5eAaImAgNnS	znět
toto	tento	k3xDgNnSc1	tento
heslo	heslo	k1gNnSc1	heslo
Non	Non	k1gFnSc2	Non
plus	plus	k1gInSc1	plus
ultra	ultra	k2eAgInSc1d1	ultra
(	(	kIx(	(
<g/>
Dále	daleko	k6eAd2	daleko
už	už	k6eAd1	už
nic	nic	k3yNnSc1	nic
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
za	za	k7c7	za
Gibraltarem	Gibraltar	k1gInSc7	Gibraltar
nepředpokládala	předpokládat	k5eNaImAgFnS	předpokládat
už	už	k9	už
žádná	žádný	k3yNgFnSc1	žádný
zem	zem	k1gFnSc1	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
Ameriky	Amerika	k1gFnSc2	Amerika
ho	on	k3xPp3gInSc4	on
však	však	k9	však
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
opravil	opravit	k5eAaPmAgMnS	opravit
na	na	k7c4	na
Plus	plus	k1gInSc4	plus
ultra	ultra	k2eAgInSc4d1	ultra
(	(	kIx(	(
<g/>
Ještě	ještě	k9	ještě
něco	něco	k3yInSc1	něco
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
posunutý	posunutý	k2eAgMnSc1d1	posunutý
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
žerdi	žerď	k1gFnSc3	žerď
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
svislá	svislý	k2eAgFnSc1d1	svislá
osa	osa	k1gFnSc1	osa
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
polovinou	polovina	k1gFnSc7	polovina
šířky	šířka	k1gFnSc2	šířka
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
–	–	k?	–
mimo	mimo	k6eAd1	mimo
regionálních	regionální	k2eAgFnPc2d1	regionální
vlajek	vlajka	k1gFnPc2	vlajka
–	–	k?	–
též	též	k9	též
na	na	k7c6	na
Kanárských	kanárský	k2eAgInPc6d1	kanárský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Španielska	Španielska	k?	Španielska
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Španělska	Španělsko	k1gNnSc2	Španělsko
</s>
</p>
<p>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Španělska	Španělsko	k1gNnSc2	Španělsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Španělská	španělský	k2eAgFnSc1d1	španělská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Španělská	španělský	k2eAgFnSc1d1	španělská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
