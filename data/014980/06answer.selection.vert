<s>
Anglická	anglický	k2eAgFnSc1d1
míle	míle	k1gFnSc1
nebo	nebo	k8xC
jen	jen	k9
míle	míle	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
mile	mile	k6eAd1
[	[	kIx(
<g/>
majl	majl	k?
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
délková	délkový	k2eAgFnSc1d1
míra	míra	k1gFnSc1
užívaná	užívaný	k2eAgFnSc1d1
v	v	k7c6
anglosaských	anglosaský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
odpovídající	odpovídající	k2eAgFnSc4d1
přibližně	přibližně	k6eAd1
1,609	1,609	k4
km	km	kA
<g/>
.	.	kIx.
</s>