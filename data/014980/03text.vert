<s>
Míle	míle	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
anglické	anglický	k2eAgFnSc6d1
míli	míle	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Míle	míle	k1gFnSc2
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Anglická	anglický	k2eAgFnSc1d1
míle	míle	k1gFnSc1
nebo	nebo	k8xC
jen	jen	k9
míle	míle	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
mile	mile	k6eAd1
[	[	kIx(
<g/>
majl	majl	k?
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
délková	délkový	k2eAgFnSc1d1
míra	míra	k1gFnSc1
užívaná	užívaný	k2eAgFnSc1d1
v	v	k7c6
anglosaských	anglosaský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
odpovídající	odpovídající	k2eAgFnSc4d1
přibližně	přibližně	k6eAd1
1,609	1,609	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepatří	patřit	k5eNaImIp3nS
mezi	mezi	k7c4
jednotky	jednotka	k1gFnPc4
SI	si	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Historicky	historicky	k6eAd1
existovalo	existovat	k5eAaImAgNnS
mnoho	mnoho	k4c1
jednotek	jednotka	k1gFnPc2
se	s	k7c7
stejným	stejný	k2eAgInSc7d1
či	či	k8xC
podobným	podobný	k2eAgInSc7d1
názvem	název	k1gInSc7
a	a	k8xC
hodnotou	hodnota	k1gFnSc7
v	v	k7c6
rozsahu	rozsah	k1gInSc6
zhruba	zhruba	k6eAd1
jednoho	jeden	k4xCgInSc2
až	až	k9
dvanácti	dvanáct	k4xCc2
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římská	římský	k2eAgFnSc1d1
míle	míle	k1gFnSc1
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
mille	mille	k6eAd1
passus	passus	k1gMnSc1
neboli	neboli	k8xC
„	„	k?
<g/>
tisíc	tisíc	k4xCgInPc2
dvoukroků	dvoukrok	k1gInPc2
<g/>
“	“	k?
<g/>
)	)	kIx)
se	se	k3xPyFc4
používala	používat	k5eAaImAgFnS
ve	v	k7c6
starověku	starověk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglická	anglický	k2eAgFnSc1d1
míle	míle	k1gFnSc1
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
5	#num#	k4
280	#num#	k4
stopám	stopa	k1gFnPc3
<g/>
,	,	kIx,
resp.	resp.	kA
1	#num#	k4
760	#num#	k4
yardům	yard	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
dohody	dohoda	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1959	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
tzv.	tzv.	kA
mezinárodní	mezinárodní	k2eAgInSc4d1
yard	yard	k1gInSc4
definovala	definovat	k5eAaBmAgFnS
jako	jako	k9
0,9144	0,9144	k4
m	m	kA
(	(	kIx(
<g/>
přesně	přesně	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tedy	tedy	k9
mezinárodní	mezinárodní	k2eAgFnSc1d1
míle	míle	k1gFnSc1
rovna	roven	k2eAgFnSc1d1
1	#num#	k4
609,344	609,344	k4
m	m	kA
(	(	kIx(
<g/>
přesně	přesně	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Angloamerická	angloamerický	k2eAgFnSc1d1
měrná	měrný	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
míle	míle	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
