<s>
Norské	norský	k2eAgNnSc1d1
dřevo	dřevo	k1gNnSc1
(	(	kIx(
<g/>
román	román	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Norské	norský	k2eAgNnSc1d1
dřevo	dřevo	k1gNnSc1
Verze	verze	k1gFnSc2
japonské	japonský	k2eAgFnSc2d1
obálky	obálka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Autor	autor	k1gMnSc1
</s>
<s>
Haruki	Haruki	k6eAd1
Murakami	Muraka	k1gFnPc7
Původní	původní	k2eAgInSc4d1
název	název	k1gInSc4
</s>
<s>
Noruwei	Noruwei	k6eAd1
no	no	k9
mori	mori	k6eAd1
<g/>
(	(	kIx(
<g/>
ノ	ノ	k?
<g/>
)	)	kIx)
Překladatel	překladatel	k1gMnSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Jurkovič	Jurkovič	k1gMnSc1
Země	zem	k1gFnSc2
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
Jazyk	jazyk	k1gInSc1
</s>
<s>
japonština	japonština	k1gFnSc1
Žánr	žánr	k1gInSc1
</s>
<s>
bildungsroman	bildungsroman	k1gMnSc1
Vydavatel	vydavatel	k1gMnSc1
</s>
<s>
Kódanša	Kódanša	k6eAd1
Datum	datum	k1gNnSc1
vydání	vydání	k1gNnSc2
</s>
<s>
1987	#num#	k4
Český	český	k2eAgInSc1d1
vydavatel	vydavatel	k1gMnSc1
</s>
<s>
Odeon	odeon	k1gInSc1
Česky	česky	k6eAd1
vydáno	vydat	k5eAaPmNgNnS
</s>
<s>
2002	#num#	k4
Počet	počet	k1gInSc1
stran	strana	k1gFnPc2
</s>
<s>
302	#num#	k4
ISBN	ISBN	kA
</s>
<s>
80-207-1193-7	80-207-1193-7	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Norské	norský	k2eAgNnSc1d1
dřevo	dřevo	k1gNnSc1
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
ノ	ノ	k?
<g/>
,	,	kIx,
Noruwei	Noruwe	k1gFnPc1
no	no	k9
mori	mori	k6eAd1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
román	román	k1gInSc1
japonského	japonský	k2eAgMnSc2d1
spisovatele	spisovatel	k1gMnSc2
Haruki	Haruki	k1gNnSc2
Murakamiho	Murakami	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
románu	román	k1gInSc2
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
písničky	písnička	k1gFnSc2
Norwegian	Norwegian	k1gMnSc1
Wood	Wood	k1gMnSc1
od	od	k7c2
skupiny	skupina	k1gFnSc2
Beatles	Beatles	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Příběh	příběh	k1gInSc1
je	být	k5eAaImIp3nS
vyprávěn	vyprávět	k5eAaImNgInS
v	v	k7c6
retrospektivě	retrospektiva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtyřicetiletý	čtyřicetiletý	k2eAgInSc1d1
hrdina	hrdina	k1gMnSc1
Tóru	tóra	k1gFnSc4
Watanabe	Watanab	k1gInSc5
se	se	k3xPyFc4
ohlíží	ohlížet	k5eAaImIp3nS
na	na	k7c4
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
dvacet	dvacet	k4xCc1
a	a	k8xC
kdy	kdy	k6eAd1
se	se	k3xPyFc4
začínal	začínat	k5eAaImAgMnS
potýkat	potýkat	k5eAaImF
se	s	k7c7
samostatným	samostatný	k2eAgInSc7d1
životem	život	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
knihu	kniha	k1gFnSc4
režisér	režisér	k1gMnSc1
Tran	Tran	k1gMnSc1
Anh	Anh	k1gMnSc1
Hung	Hung	k1gMnSc1
převedl	převést	k5eAaPmAgMnS
do	do	k7c2
stejnojmenného	stejnojmenný	k2eAgInSc2d1
filmu	film	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Román	Román	k1gMnSc1
si	se	k3xPyFc3
lze	lze	k6eAd1
poslechnout	poslechnout	k5eAaPmF
také	také	k9
jako	jako	k9
audioknihu	audioknih	k1gInSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
načetli	načíst	k5eAaPmAgMnP,k5eAaBmAgMnP
Filip	Filip	k1gMnSc1
Čapka	Čapka	k1gMnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
Stryková	Stryková	k1gFnSc1
a	a	k8xC
Klára	Klára	k1gFnSc1
Issová	Issová	k1gFnSc1
(	(	kIx(
<g/>
nakladatelství	nakladatelství	k1gNnSc1
OneHotBook	OneHotBook	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Děj	děj	k1gInSc1
románu	román	k1gInSc2
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Devatenáctiletý	devatenáctiletý	k2eAgMnSc1d1
Tóru	tóra	k1gFnSc4
Watanabe	Watanab	k1gInSc5
studuje	studovat	k5eAaImIp3nS
drama	drama	k1gNnSc4
na	na	k7c6
vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
v	v	k7c6
Tokiu	Tokio	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
uzavřený	uzavřený	k2eAgInSc1d1
<g/>
,	,	kIx,
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
jediný	jediný	k2eAgMnSc1d1
kamarád	kamarád	k1gMnSc1
Kizuki	Kizuki	k1gNnSc2
spáchal	spáchat	k5eAaPmAgMnS
v	v	k7c6
sedmnácti	sedmnáct	k4xCc6
letech	léto	k1gNnPc6
sebevraždu	sebevražda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přátelí	přátelit	k5eAaImIp3nS
se	se	k3xPyFc4
s	s	k7c7
dívkou	dívka	k1gFnSc7
Naoko	naoko	k6eAd1
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yQgFnSc7,k3yIgFnSc7,k3yRgFnSc7
se	se	k3xPyFc4
při	při	k7c6
jejích	její	k3xOp3gFnPc6
dvacátých	dvacátý	k4xOgFnPc6
narozeninách	narozeniny	k1gFnPc6
intimně	intimně	k6eAd1
sblíží	sblížit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
poté	poté	k6eAd1
se	se	k3xPyFc4
však	však	k9
Naoko	naoko	k6eAd1
psychicky	psychicky	k6eAd1
zhroutí	zhroutit	k5eAaPmIp3nS
<g/>
,	,	kIx,
opustí	opustit	k5eAaPmIp3nS
školu	škola	k1gFnSc4
a	a	k8xC
odejde	odejít	k5eAaPmIp3nS
se	se	k3xPyFc4
léčit	léčit	k5eAaImF
do	do	k7c2
horské	horský	k2eAgFnSc2d1
ozdravovny	ozdravovna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
univerzitě	univerzita	k1gFnSc6
se	se	k3xPyFc4
na	na	k7c6
konci	konec	k1gInSc6
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
v	v	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
,	,	kIx,
bouří	bouřit	k5eAaImIp3nP
studenti	student	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrdina	Hrdina	k1gMnSc1
k	k	k7c3
celému	celý	k2eAgNnSc3d1
dění	dění	k1gNnSc3
zastává	zastávat	k5eAaImIp3nS
rezervovaný	rezervovaný	k2eAgInSc4d1
postoj	postoj	k1gInSc4
<g/>
:	:	kIx,
ideje	idea	k1gFnSc2
revoluce	revoluce	k1gFnSc2
neodmítá	odmítat	k5eNaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
kritický	kritický	k2eAgInSc1d1
vůči	vůči	k7c3
možnosti	možnost	k1gFnSc3
je	být	k5eAaImIp3nS
prosadit	prosadit	k5eAaPmF
a	a	k8xC
lidem	člověk	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
je	on	k3xPp3gFnPc4
prosazují	prosazovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
oblíbené	oblíbený	k2eAgFnSc3d1
knize	kniha	k1gFnSc3
Velký	velký	k2eAgInSc1d1
Gatsby	Gatsb	k1gInPc1
se	se	k3xPyFc4
seznámí	seznámit	k5eAaPmIp3nP
s	s	k7c7
ambiciózním	ambiciózní	k2eAgMnSc7d1
a	a	k8xC
uznávaným	uznávaný	k2eAgMnSc7d1
bonvivánem	bonviván	k1gMnSc7
Nagasawou	Nagasawa	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
houževnatě	houževnatě	k6eAd1
připravuje	připravovat	k5eAaImIp3nS
na	na	k7c4
práci	práce	k1gFnSc4
v	v	k7c6
diplomatických	diplomatický	k2eAgFnPc6d1
službách	služba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tóru	tóra	k1gFnSc4
Nagasawovi	Nagasawa	k1gMnSc3
přes	přes	k7c4
jistou	jistý	k2eAgFnSc4d1
nevýraznost	nevýraznost	k1gFnSc4
padne	padnout	k5eAaImIp3nS,k5eAaPmIp3nS
do	do	k7c2
oka	oko	k1gNnSc2
a	a	k8xC
několikrát	několikrát	k6eAd1
se	se	k3xPyFc4
vydávají	vydávat	k5eAaImIp3nP,k5eAaPmIp3nP
na	na	k7c4
lov	lov	k1gInSc4
dívek	dívka	k1gFnPc2
na	na	k7c4
jednu	jeden	k4xCgFnSc4
noc	noc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
pozvání	pozvání	k1gNnSc4
Naoko	naoko	k6eAd1
přijede	přijet	k5eAaPmIp3nS
Tóru	tóra	k1gFnSc4
na	na	k7c4
návštěvu	návštěva	k1gFnSc4
ozdravovny	ozdravovna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
seznámí	seznámit	k5eAaPmIp3nS
s	s	k7c7
nadanou	nadaný	k2eAgFnSc7d1
kytaristkou	kytaristka	k1gFnSc7
Reiko	Reiko	k1gNnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
odmalička	odmalička	k6eAd1
trpěla	trpět	k5eAaImAgFnS
duševními	duševní	k2eAgFnPc7d1
poruchami	porucha	k1gFnPc7
a	a	k8xC
do	do	k7c2
penzionu	penzion	k1gInSc2
ji	on	k3xPp3gFnSc4
přivedly	přivést	k5eAaPmAgFnP
lživé	lživý	k2eAgFnPc1d1
pomluvy	pomluva	k1gFnPc1
od	od	k7c2
třináctiletého	třináctiletý	k2eAgNnSc2d1
děvčete	děvče	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
neumělo	umět	k5eNaImAgNnS
vyrovnat	vyrovnat	k5eAaBmF,k5eAaPmF
s	s	k7c7
Reičiným	Reičin	k2eAgNnSc7d1
odmítnutím	odmítnutí	k1gNnSc7
její	její	k3xOp3gFnSc2
snahy	snaha	k1gFnSc2
o	o	k7c4
sex	sex	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stráví	strávit	k5eAaPmIp3nS
zde	zde	k6eAd1
několik	několik	k4yIc1
idylických	idylický	k2eAgInPc2d1
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Naočin	Naočin	k2eAgInSc1d1
stav	stav	k1gInSc1
se	se	k3xPyFc4
však	však	k9
zhoršuje	zhoršovat	k5eAaImIp3nS
a	a	k8xC
začíná	začínat	k5eAaImIp3nS
mít	mít	k5eAaImF
problémy	problém	k1gInPc4
s	s	k7c7
vyjadřováním	vyjadřování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návratu	návrat	k1gInSc6
do	do	k7c2
školy	škola	k1gFnSc2
se	se	k3xPyFc4
Tóru	tóra	k1gFnSc4
sblíží	sblížit	k5eAaPmIp3nP
s	s	k7c7
Midori	Midor	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
dříve	dříve	k6eAd2
pomáhal	pomáhat	k5eAaImAgInS
ošetřovat	ošetřovat	k5eAaImF
umírajícího	umírající	k2eAgMnSc4d1
otce	otec	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vztah	vztah	k1gInSc1
dlouho	dlouho	k6eAd1
nevydržel	vydržet	k5eNaPmAgInS
<g/>
,	,	kIx,
protože	protože	k8xS
Tóru	tóra	k1gFnSc4
měl	mít	k5eAaImAgInS
špatné	špatný	k2eAgNnSc4d1
svědomí	svědomí	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
Naoko	naoko	k6eAd1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gNnSc1
zdraví	zdraví	k1gNnSc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
zhoršovalo	zhoršovat	k5eAaImAgNnS
<g/>
,	,	kIx,
podvádí	podvádět	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naoko	naoko	k6eAd1
se	se	k3xPyFc4
vrátí	vrátit	k5eAaPmIp3nS
do	do	k7c2
ozdravovny	ozdravovna	k1gFnSc2
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
předstírala	předstírat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
uzdravuje	uzdravovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
tu	tu	k6eAd1
oběsila	oběsit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Tóru	tóra	k1gFnSc4
pak	pak	k6eAd1
cestuje	cestovat	k5eAaImIp3nS
po	po	k7c6
Japonsku	Japonsko	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zmírnil	zmírnit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
žal	žal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návratu	návrat	k1gInSc6
jej	on	k3xPp3gMnSc4
přivítá	přivítat	k5eAaPmIp3nS
Reiko	Reiko	k1gNnSc1
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgNnP
vrátit	vrátit	k5eAaPmF
do	do	k7c2
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
posledních	poslední	k2eAgInPc6d1
řádcích	řádek	k1gInPc6
knihy	kniha	k1gFnSc2
Tóru	tóra	k1gFnSc4
volá	volat	k5eAaImIp3nS
Midori	Midore	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Témata	téma	k1gNnPc4
</s>
<s>
Sebevražda	sebevražda	k1gFnSc1
</s>
<s>
Sebevraždou	sebevražda	k1gFnSc7
končí	končit	k5eAaImIp3nP
osudy	osud	k1gInPc1
hned	hned	k6eAd1
několika	několik	k4yIc2
postav	postava	k1gFnPc2
<g/>
:	:	kIx,
Naočina	Naočin	k2eAgFnSc1d1
sestra	sestra	k1gFnSc1
<g/>
,	,	kIx,
Kizuki	Kizuki	k1gNnSc1
<g/>
,	,	kIx,
Naoko	naoko	k6eAd1
<g/>
,	,	kIx,
Nagasawova	Nagasawův	k2eAgFnSc1d1
přítelkyně	přítelkyně	k1gFnSc1
Hacumi	Hacu	k1gFnPc7
<g/>
;	;	kIx,
ti	ten	k3xDgMnPc1
všichni	všechen	k3xTgMnPc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
zabijí	zabít	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkou	velký	k2eAgFnSc4d1
část	část	k1gFnSc4
úvah	úvaha	k1gFnPc2
hlavního	hlavní	k2eAgMnSc2d1
hrdiny	hrdina	k1gMnSc2
zabírá	zabírat	k5eAaImIp3nS
reflexe	reflexe	k1gFnSc1
tohoto	tento	k3xDgNnSc2
odcházení	odcházení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Recenze	recenze	k1gFnSc1
filmové	filmový	k2eAgFnSc2d1
adaptace	adaptace	k1gFnSc2
na	na	k7c4
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
od	od	k7c2
Jarmily	Jarmila	k1gFnSc2
Křenkové	Křenková	k1gFnSc2
o	o	k7c6
tom	ten	k3xDgNnSc6
píše	psát	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Norské	norský	k2eAgNnSc1d1
dřevo	dřevo	k1gNnSc1
je	být	k5eAaImIp3nS
taková	takový	k3xDgFnSc1
duchařina	duchařina	k1gFnSc1
naruby	naruby	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kizuki	Kizuk	k1gInSc6
se	se	k3xPyFc4
zabil	zabít	k5eAaPmAgMnS
a	a	k8xC
má	mít	k5eAaImIp3nS
to	ten	k3xDgNnSc1
vyřešené	vyřešený	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gMnPc1
přátelé	přítel	k1gMnPc1
se	s	k7c7
životem	život	k1gInSc7
plouží	ploužit	k5eAaImIp3nP
jako	jako	k8xC,k8xS
přízraky	přízrak	k1gInPc1
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Západní	západní	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1
postava	postava	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
chápána	chápat	k5eAaImNgFnS
jako	jako	k9
autobiografická	autobiografický	k2eAgFnSc1d1
z	z	k7c2
několika	několik	k4yIc2
důvodů	důvod	k1gInPc2
<g/>
:	:	kIx,
Murakami	Muraka	k1gFnPc7
kupříkladu	kupříkladu	k6eAd1
také	také	k9
pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
obchodě	obchod	k1gInSc6
s	s	k7c7
gramodeskami	gramodeska	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitý	důležitý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
ale	ale	k9
její	její	k3xOp3gInSc4
postoj	postoj	k1gInSc4
k	k	k7c3
japonskosti	japonskost	k1gFnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
s	s	k7c7
autorovým	autorův	k2eAgInSc7d1
také	také	k9
shoduje	shodovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tóru	tóra	k1gFnSc4
čte	číst	k5eAaImIp3nS
západní	západní	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
a	a	k8xC
sleduje	sledovat	k5eAaImIp3nS
americké	americký	k2eAgInPc4d1
filmy	film	k1gInPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jeho	jeho	k3xOp3gMnPc7
spolužáky	spolužák	k1gMnPc7
zajímá	zajímat	k5eAaImIp3nS
zejména	zejména	k9
japonská	japonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
a	a	k8xC
občas	občas	k6eAd1
také	také	k9
francouzská	francouzský	k2eAgFnSc1d1
<g/>
)	)	kIx)
literatura	literatura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Norské	norský	k2eAgNnSc1d1
dřevo	dřevo	k1gNnSc1
(	(	kIx(
<g/>
román	román	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
Československé	československý	k2eAgFnSc6d1
bibliografické	bibliografický	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Knihy	kniha	k1gFnSc2
Haruki	Haruk	k1gFnSc2
Murakamiho	Murakami	k1gMnSc2
Romány	Román	k1gMnPc7
</s>
<s>
Slyš	slyšet	k5eAaImRp2nS
vítr	vítr	k1gInSc1
zpívat	zpívat	k5eAaImF
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
<g/>
†	†	k?
•	•	k?
Pinball	Pinball	k1gMnSc1
<g/>
,	,	kIx,
1973	#num#	k4
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
<g/>
†	†	k?
•	•	k?
Hon	hon	k1gInSc1
na	na	k7c4
ovci	ovce	k1gFnSc4
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Konec	konec	k1gInSc1
světa	svět	k1gInSc2
&	&	k?
Hard-boiled	Hard-boiled	k1gInSc1
Wonderland	Wonderland	k1gInSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Norské	norský	k2eAgNnSc1d1
dřevo	dřevo	k1gNnSc1
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Tancuj	tancovat	k5eAaImRp2nS
<g/>
,	,	kIx,
tancuj	tancovat	k5eAaImRp2nS
<g/>
,	,	kIx,
tancuj	tancovat	k5eAaImRp2nS
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
<g/>
†	†	k?
•	•	k?
Na	na	k7c4
jih	jih	k1gInSc4
od	od	k7c2
hranic	hranice	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c4
západ	západ	k1gInSc4
od	od	k7c2
slunce	slunce	k1gNnSc2
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Kronika	kronika	k1gFnSc1
ptáčka	ptáček	k1gMnSc2
na	na	k7c4
klíček	klíček	k1gInSc4
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sputnik	sputnik	k1gInSc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
láska	láska	k1gFnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Kafka	Kafka	k1gMnSc1
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Afterdark	Afterdark	k1gInSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1Q84	1Q84	k4
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bezbarvý	bezbarvý	k2eAgInSc1d1
Cukuru	Cukura	k1gFnSc4
Tazaki	Tazak	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
léta	léto	k1gNnSc2
putování	putování	k1gNnSc4
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Komturova	komturův	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
Literatura	literatura	k1gFnSc1
faktu	fakt	k1gInSc2
</s>
<s>
Underground	underground	k1gInSc4
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
–	–	k?
<g/>
98	#num#	k4
<g/>
)	)	kIx)
<g/>
†	†	k?
•	•	k?
O	o	k7c6
čem	co	k3yQnSc6,k3yRnSc6,k3yInSc6
mluvím	mluvit	k5eAaImIp1nS
<g/>
,	,	kIx,
když	když	k8xS
mluvím	mluvit	k5eAaImIp1nS
o	o	k7c4
běhání	běhání	k1gNnSc4
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Spisovatel	spisovatel	k1gMnSc1
jako	jako	k8xS,k8xC
povolání	povolání	k1gNnSc4
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
Sbírky	sbírka	k1gFnSc2
povídek	povídka	k1gFnPc2
</s>
<s>
Zmizení	zmizení	k1gNnSc1
slona	slon	k1gMnSc2
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
†	†	k?
•	•	k?
Po	po	k7c6
otřesech	otřes	k1gInPc6
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Slepá	slepý	k2eAgFnSc1d1
vrba	vrba	k1gFnSc1
a	a	k8xC
spící	spící	k2eAgFnSc1d1
žena	žena	k1gFnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
†	†	k?
•	•	k?
Muži	muž	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
nemají	mít	k5eNaImIp3nP
ženy	žena	k1gFnPc4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
Povídky	povídka	k1gFnPc1
</s>
<s>
Spánek	spánek	k1gInSc1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Podivná	podivný	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
Knihy	kniha	k1gFnPc4
označené	označený	k2eAgFnPc4d1
symbolem	symbol	k1gInSc7
†	†	k?
nebyly	být	k5eNaImAgInP
oficiálně	oficiálně	k6eAd1
vydány	vydat	k5eAaPmNgInP
v	v	k7c6
českém	český	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
<g/>
.	.	kIx.
</s>
