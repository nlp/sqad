<p>
<s>
Lukáš	Lukáš	k1gMnSc1	Lukáš
Vydra	Vydra	k1gMnSc1	Vydra
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1973	[number]	k4	1973
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
běžec	běžec	k1gMnSc1	běžec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
středním	střední	k2eAgFnPc3d1	střední
tratím	trať	k1gFnPc3	trať
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
specializací	specializace	k1gFnSc7	specializace
byl	být	k5eAaImAgInS	být
běh	běh	k1gInSc1	běh
na	na	k7c4	na
800	[number]	k4	800
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
trati	trať	k1gFnSc6	trať
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
šampionátu	šampionát	k1gInSc6	šampionát
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1998	[number]	k4	1998
zaběhl	zaběhnout	k5eAaPmAgMnS	zaběhnout
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
dosud	dosud	k6eAd1	dosud
platný	platný	k2eAgInSc4d1	platný
český	český	k2eAgInSc4d1	český
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
800	[number]	k4	800
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
44,84	[number]	k4	44,84
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
zaběhl	zaběhnout	k5eAaPmAgMnS	zaběhnout
nový	nový	k2eAgInSc4d1	nový
český	český	k2eAgInSc4d1	český
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
1000	[number]	k4	1000
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
16,56	[number]	k4	16,56
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
žák	žák	k1gMnSc1	žák
zaběhl	zaběhnout	k5eAaPmAgMnS	zaběhnout
trať	trať	k1gFnSc4	trať
800	[number]	k4	800
m	m	kA	m
za	za	k7c4	za
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
57,4	[number]	k4	57,4
a	a	k8xC	a
jako	jako	k8xC	jako
dorostenec	dorostenec	k1gMnSc1	dorostenec
se	se	k3xPyFc4	se
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
na	na	k7c4	na
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
51,67	[number]	k4	51,67
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
závodil	závodit	k5eAaImAgMnS	závodit
za	za	k7c4	za
Olymp	Olymp	k1gInSc4	Olymp
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
za	za	k7c4	za
Duklu	Dukla	k1gFnSc4	Dukla
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
významnější	významný	k2eAgInSc4d2	významnější
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
měřítku	měřítko	k1gNnSc6	měřítko
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
na	na	k7c6	na
halovém	halový	k2eAgNnSc6d1	halové
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obsadil	obsadit	k5eAaPmAgMnS	obsadit
výkonem	výkon	k1gInSc7	výkon
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
46,20	[number]	k4	46,20
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
1500	[number]	k4	1500
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
a	a	k8xC	a
1998	[number]	k4	1998
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
mistra	mistr	k1gMnSc2	mistr
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
800	[number]	k4	800
m.	m.	k?	m.
Na	na	k7c6	na
halovém	halový	k2eAgInSc6d1	halový
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
v	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
Maebaši	Maebaš	k1gInSc6	Maebaš
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
obsadil	obsadit	k5eAaPmAgMnS	obsadit
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
semifinálovém	semifinálový	k2eAgNnSc6d1	semifinálové
kole	kolo	k1gNnSc6	kolo
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
běhu	běh	k1gInSc2	běh
na	na	k7c4	na
800	[number]	k4	800
nepostoupil	postoupit	k5eNaPmAgMnS	postoupit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osobní	osobní	k2eAgInSc1d1	osobní
rekord	rekord	k1gInSc1	rekord
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
800	[number]	k4	800
m	m	kA	m
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc1	jeho
výkony	výkon	k1gInPc1	výkon
postupně	postupně	k6eAd1	postupně
zhoršovaly	zhoršovat	k5eAaImAgInP	zhoršovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výkonností	výkonnost	k1gFnPc2	výkonnost
vývoj	vývoj	k1gInSc1	vývoj
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yRnSc1	kdo
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
atletice	atletika	k1gFnSc6	atletika
-	-	kIx~	-
Jan	Jan	k1gMnSc1	Jan
Jirka	Jirka	k1gMnSc1	Jirka
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
Olympia	Olympia	k1gFnSc1	Olympia
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7033	[number]	k4	7033
<g/>
-	-	kIx~	-
<g/>
864	[number]	k4	864
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
201	[number]	k4	201
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Lukáš	Lukáš	k1gMnSc1	Lukáš
Vydra	Vydra	k1gMnSc1	Vydra
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
IAAF	IAAF	kA	IAAF
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
ČAS	čas	k1gInSc1	čas
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
