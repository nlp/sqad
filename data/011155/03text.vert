<p>
<s>
Vincenc	Vincenc	k1gMnSc1	Vincenc
Brandl	Brandl	k1gMnSc1	Brandl
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1834	[number]	k4	1834
Staré	Stará	k1gFnSc6	Stará
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1901	[number]	k4	1901
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
moravský	moravský	k2eAgMnSc1d1	moravský
zemský	zemský	k2eAgMnSc1d1	zemský
archivář	archivář	k1gMnSc1	archivář
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgMnS	zabývat
se	se	k3xPyFc4	se
moravskými	moravský	k2eAgFnPc7d1	Moravská
dějinami	dějiny	k1gFnPc7	dějiny
<g/>
,	,	kIx,	,
místopisem	místopis	k1gInSc7	místopis
a	a	k8xC	a
národopisem	národopis	k1gInSc7	národopis
<g/>
.	.	kIx.	.
</s>
<s>
Připravoval	připravovat	k5eAaImAgInS	připravovat
kritická	kritický	k2eAgNnPc4d1	kritické
vydání	vydání	k1gNnPc4	vydání
historických	historický	k2eAgInPc2d1	historický
pramenů	pramen	k1gInPc2	pramen
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
studie	studie	k1gFnPc4	studie
o	o	k7c6	o
právní	právní	k2eAgFnSc6d1	právní
historii	historie	k1gFnSc6	historie
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
a	a	k8xC	a
životopisy	životopis	k1gInPc1	životopis
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Aktivně	aktivně	k6eAd1	aktivně
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
probouzení	probouzení	k1gNnSc4	probouzení
národního	národní	k2eAgNnSc2d1	národní
povědomí	povědomí	k1gNnSc2	povědomí
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
na	na	k7c6	na
činnosti	činnost	k1gFnSc6	činnost
českojazyčných	českojazyčný	k2eAgInPc2d1	českojazyčný
spolků	spolek	k1gInPc2	spolek
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Zasedal	zasedat	k5eAaImAgMnS	zasedat
v	v	k7c6	v
moravském	moravský	k2eAgInSc6d1	moravský
zemském	zemský	k2eAgInSc6d1	zemský
sněmu	sněm	k1gInSc6	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
vlivným	vlivný	k2eAgMnPc3d1	vlivný
vědcům	vědec	k1gMnPc3	vědec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
oceňovaný	oceňovaný	k2eAgInSc1d1	oceňovaný
pro	pro	k7c4	pro
mnohostrannou	mnohostranný	k2eAgFnSc4d1	mnohostranná
a	a	k8xC	a
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
vzdělanost	vzdělanost	k1gFnSc4	vzdělanost
<g/>
,	,	kIx,	,
objektivitu	objektivita	k1gFnSc4	objektivita
i	i	k8xC	i
vlastenectví	vlastenectví	k1gNnSc4	vlastenectví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
chudých	chudý	k2eAgInPc2d1	chudý
poměrů	poměr	k1gInPc2	poměr
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
rodiče	rodič	k1gMnPc1	rodič
pracovali	pracovat	k5eAaImAgMnP	pracovat
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
augustiniánského	augustiniánský	k2eAgInSc2d1	augustiniánský
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
měl	mít	k5eAaImAgInS	mít
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
vliv	vliv	k1gInSc4	vliv
učitel	učitel	k1gMnSc1	učitel
František	František	k1gMnSc1	František
Matouš	Matouš	k1gMnSc1	Matouš
Klácel	Klácel	k1gMnSc1	Klácel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
na	na	k7c6	na
Stavovské	stavovský	k2eAgFnSc6d1	stavovská
akademii	akademie	k1gFnSc6	akademie
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
studiu	studio	k1gNnSc6	studio
práv	právo	k1gNnPc2	právo
na	na	k7c6	na
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
univerzitě	univerzita	k1gFnSc6	univerzita
(	(	kIx(	(
<g/>
obojí	obojí	k4xRgMnSc1	obojí
ukončil	ukončit	k5eAaPmAgInS	ukončit
v	v	k7c6	v
r.	r.	kA	r.
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1856	[number]	k4	1856
<g/>
–	–	k?	–
<g/>
1857	[number]	k4	1857
historicko-filologický	historickoilologický	k2eAgInSc4d1	historicko-filologický
seminář	seminář	k1gInSc4	seminář
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
učil	učít	k5eAaPmAgInS	učít
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
německém	německý	k2eAgNnSc6d1	německé
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1857	[number]	k4	1857
<g/>
–	–	k?	–
<g/>
1861	[number]	k4	1861
pak	pak	k6eAd1	pak
na	na	k7c6	na
městském	městský	k2eAgNnSc6d1	Městské
reálném	reálný	k2eAgNnSc6d1	reálné
gymnáziu	gymnázium	k1gNnSc6	gymnázium
ve	v	k7c6	v
Starém	starý	k2eAgNnSc6d1	staré
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
své	svůj	k3xOyFgFnSc2	svůj
první	první	k4xOgFnSc2	první
odborné	odborný	k2eAgFnSc2d1	odborná
studie	studie	k1gFnSc2	studie
<g/>
,	,	kIx,	,
psané	psaný	k2eAgFnPc1d1	psaná
německy	německy	k6eAd1	německy
<g/>
.	.	kIx.	.
</s>
<s>
Významně	významně	k6eAd1	významně
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
upozornil	upozornit	k5eAaPmAgMnS	upozornit
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyvracel	vyvracet	k5eAaImAgMnS	vyvracet
názor	názor	k1gInSc4	názor
známého	známý	k2eAgMnSc2d1	známý
historika	historik	k1gMnSc2	historik
Bedy	Beda	k1gMnSc2	Beda
Dudíka	Dudík	k1gMnSc2	Dudík
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgMnSc2	jenž
byl	být	k5eAaImAgInS	být
Velehrad	Velehrad	k1gInSc4	Velehrad
bájným	bájný	k2eAgInSc7d1	bájný
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
skutečným	skutečný	k2eAgNnSc7d1	skutečné
centrem	centrum	k1gNnSc7	centrum
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Brandla	Brandla	k1gMnSc2	Brandla
se	se	k3xPyFc4	se
historický	historický	k2eAgInSc1d1	historický
Velehrad	Velehrad	k1gInSc1	Velehrad
nacházel	nacházet	k5eAaImAgInS	nacházet
blízko	blízko	k7c2	blízko
moderního	moderní	k2eAgNnSc2d1	moderní
<g/>
,	,	kIx,	,
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
u	u	k7c2	u
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Dudík	Dudík	k1gMnSc1	Dudík
toto	tento	k3xDgNnSc4	tento
stanovisko	stanovisko	k1gNnSc4	stanovisko
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Josefa	Josef	k1gMnSc2	Josef
Chytila	Chytil	k1gMnSc2	Chytil
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Brandl	Brandl	k1gMnSc1	Brandl
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
moravským	moravský	k2eAgMnSc7d1	moravský
zemským	zemský	k2eAgMnSc7d1	zemský
archivářem	archivář	k1gMnSc7	archivář
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
vykonával	vykonávat	k5eAaImAgInS	vykonávat
téměř	téměř	k6eAd1	téměř
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
se	se	k3xPyFc4	se
archiv	archiv	k1gInSc1	archiv
stal	stát	k5eAaPmAgInS	stát
střediskem	středisko	k1gNnSc7	středisko
vědy	věda	k1gFnSc2	věda
i	i	k8xC	i
národního	národní	k2eAgInSc2d1	národní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Probíhala	probíhat	k5eAaImAgFnS	probíhat
zde	zde	k6eAd1	zde
setkání	setkání	k1gNnSc1	setkání
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
literátů	literát	k1gMnPc2	literát
<g/>
,	,	kIx,	,
studentů	student	k1gMnPc2	student
i	i	k8xC	i
buditelů	buditel	k1gMnPc2	buditel
<g/>
.	.	kIx.	.
</s>
<s>
Kriticky	kriticky	k6eAd1	kriticky
se	se	k3xPyFc4	se
rozebírala	rozebírat	k5eAaImAgFnS	rozebírat
literatura	literatura	k1gFnSc1	literatura
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
domlouvaly	domlouvat	k5eAaImAgFnP	domlouvat
akce	akce	k1gFnPc1	akce
na	na	k7c4	na
zvyšování	zvyšování	k1gNnSc4	zvyšování
národního	národní	k2eAgNnSc2d1	národní
uvědomění	uvědomění	k1gNnSc2	uvědomění
mezi	mezi	k7c7	mezi
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
<g/>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
mnoha	mnoho	k4c2	mnoho
sdružení	sdružení	k1gNnPc2	sdružení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Čtenářského	čtenářský	k2eAgInSc2d1	čtenářský
spolku	spolek	k1gInSc2	spolek
<g/>
,	,	kIx,	,
Spolku	spolek	k1gInSc2	spolek
pro	pro	k7c4	pro
vydržování	vydržování	k1gNnSc4	vydržování
české	český	k2eAgFnSc2d1	Česká
obchodní	obchodní	k2eAgFnSc2d1	obchodní
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
nebo	nebo	k8xC	nebo
Musejního	musejní	k2eAgInSc2d1	musejní
spolku	spolek	k1gInSc2	spolek
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
tajemníkem	tajemník	k1gMnSc7	tajemník
Matice	matice	k1gFnSc2	matice
moravské	moravský	k2eAgFnPc1d1	Moravská
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Královské	královský	k2eAgFnSc2d1	královská
české	český	k2eAgFnSc2d1	Česká
společnosti	společnost	k1gFnSc2	společnost
nauk	nauka	k1gFnPc2	nauka
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
České	český	k2eAgFnSc2d1	Česká
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1865	[number]	k4	1865
<g/>
–	–	k?	–
<g/>
1868	[number]	k4	1868
byl	být	k5eAaImAgMnS	být
poslancem	poslanec	k1gMnSc7	poslanec
moravského	moravský	k2eAgInSc2d1	moravský
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Poslanecký	poslanecký	k2eAgInSc1d1	poslanecký
slib	slib	k1gInSc1	slib
skládal	skládat	k5eAaImAgInS	skládat
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1867	[number]	k4	1867
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
za	za	k7c2	za
kandidáta	kandidát	k1gMnSc2	kandidát
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
říšského	říšský	k2eAgInSc2d1	říšský
sněmu	sněm	k1gInSc2	sněm
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kandidatury	kandidatura	k1gFnSc2	kandidatura
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
<g/>
Brandl	Brandl	k1gFnSc1	Brandl
jako	jako	k8xC	jako
historik	historik	k1gMnSc1	historik
byl	být	k5eAaImAgMnS	být
samoukem	samouk	k1gMnSc7	samouk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedostatek	nedostatek	k1gInSc1	nedostatek
formálního	formální	k2eAgNnSc2d1	formální
vzdělání	vzdělání	k1gNnSc2	vzdělání
nahradil	nahradit	k5eAaPmAgMnS	nahradit
pílí	pílit	k5eAaImIp3nS	pílit
a	a	k8xC	a
vytrvalostí	vytrvalost	k1gFnSc7	vytrvalost
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
konzervativní	konzervativní	k2eAgInPc4d1	konzervativní
postoje	postoj	k1gInPc4	postoj
<g/>
,	,	kIx,	,
blízké	blízký	k2eAgInPc4d1	blízký
staročechům	staročech	k1gMnPc3	staročech
<g/>
,	,	kIx,	,
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
i	i	k9	i
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měl	mít	k5eAaImAgMnS	mít
toleranci	tolerance	k1gFnSc4	tolerance
pro	pro	k7c4	pro
jiné	jiný	k2eAgInPc4d1	jiný
názory	názor	k1gInPc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Současníci	současník	k1gMnPc1	současník
ho	on	k3xPp3gMnSc4	on
uznávali	uznávat	k5eAaImAgMnP	uznávat
pro	pro	k7c4	pro
mnohostrannou	mnohostranný	k2eAgFnSc4d1	mnohostranná
a	a	k8xC	a
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
vzdělanost	vzdělanost	k1gFnSc4	vzdělanost
<g/>
,	,	kIx,	,
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
a	a	k8xC	a
publikační	publikační	k2eAgFnSc4d1	publikační
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
pravdivost	pravdivost	k1gFnSc4	pravdivost
a	a	k8xC	a
objektivitu	objektivita	k1gFnSc4	objektivita
<g/>
,	,	kIx,	,
přátelské	přátelský	k2eAgNnSc4d1	přátelské
vystupování	vystupování	k1gNnSc4	vystupování
<g/>
,	,	kIx,	,
skromnost	skromnost	k1gFnSc4	skromnost
a	a	k8xC	a
vlídnost	vlídnost	k1gFnSc4	vlídnost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
mimořádně	mimořádně	k6eAd1	mimořádně
výkonný	výkonný	k2eAgInSc1d1	výkonný
a	a	k8xC	a
produktivní	produktivní	k2eAgInSc1d1	produktivní
<g/>
,	,	kIx,	,
šetřil	šetřit	k5eAaImAgInS	šetřit
s	s	k7c7	s
časem	čas	k1gInSc7	čas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
a	a	k8xC	a
spolupracovníky	spolupracovník	k1gMnPc4	spolupracovník
si	se	k3xPyFc3	se
jej	on	k3xPp3gInSc4	on
vždy	vždy	k6eAd1	vždy
našel	najít	k5eAaPmAgMnS	najít
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
znalostmi	znalost	k1gFnPc7	znalost
a	a	k8xC	a
objektivním	objektivní	k2eAgInSc7d1	objektivní
přístupem	přístup	k1gInSc7	přístup
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgInS	získat
respekt	respekt	k1gInSc4	respekt
i	i	k9	i
mezi	mezi	k7c7	mezi
Němci	Němec	k1gMnPc7	Němec
<g/>
.	.	kIx.	.
<g/>
Poslední	poslední	k2eAgNnSc4d1	poslední
léta	léto	k1gNnPc4	léto
života	život	k1gInSc2	život
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
byla	být	k5eAaImAgFnS	být
těžká	těžký	k2eAgFnSc1d1	těžká
–	–	k?	–
zemřela	zemřít	k5eAaPmAgFnS	zemřít
mu	on	k3xPp3gMnSc3	on
manželka	manželka	k1gFnSc1	manželka
(	(	kIx(	(
<g/>
velmi	velmi	k6eAd1	velmi
tolerantní	tolerantní	k2eAgFnSc1d1	tolerantní
Němka	Němka	k1gFnSc1	Němka
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
vědecké	vědecký	k2eAgFnSc2d1	vědecká
práce	práce	k1gFnSc2	práce
kvůli	kvůli	k7c3	kvůli
oční	oční	k2eAgFnSc3d1	oční
chorobě	choroba	k1gFnSc3	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
odpočinku	odpočinek	k1gInSc6	odpočinek
pak	pak	k6eAd1	pak
trávil	trávit	k5eAaImAgMnS	trávit
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
v	v	k7c6	v
brněnském	brněnský	k2eAgInSc6d1	brněnský
klášteře	klášter	k1gInSc6	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
náhle	náhle	k6eAd1	náhle
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
brněnském	brněnský	k2eAgInSc6d1	brněnský
Ústředním	ústřední	k2eAgInSc6d1	ústřední
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
student	student	k1gMnSc1	student
psal	psát	k5eAaImAgMnS	psát
německé	německý	k2eAgFnPc4d1	německá
a	a	k8xC	a
české	český	k2eAgFnPc4d1	Česká
básně	báseň	k1gFnPc4	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
objevena	objeven	k2eAgFnSc1d1	objevena
až	až	k9	až
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
brněnském	brněnský	k2eAgInSc6d1	brněnský
archivu	archiv	k1gInSc6	archiv
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
umělecká	umělecký	k2eAgFnSc1d1	umělecká
úroveň	úroveň	k1gFnSc1	úroveň
nebyla	být	k5eNaImAgFnS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
Brandl	Brandl	k1gMnSc1	Brandl
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
stavěl	stavět	k5eAaImAgMnS	stavět
kriticky	kriticky	k6eAd1	kriticky
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodná	pozoruhodný	k2eAgFnSc1d1	pozoruhodná
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
nevydaná	vydaný	k2eNgFnSc1d1	nevydaná
německá	německý	k2eAgFnSc1d1	německá
báseň	báseň	k1gFnSc1	báseň
Der	drát	k5eAaImRp2nS	drát
letzte	letzit	k5eAaPmRp2nP	letzit
Mönch	Mön	k1gFnPc6	Mön
im	im	k?	im
Kloster	Kloster	k1gInSc1	Kloster
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
vyslovil	vyslovit	k5eAaPmAgInS	vyslovit
svou	svůj	k3xOyFgFnSc4	svůj
nechuť	nechuť	k1gFnSc4	nechuť
stát	stát	k5eAaImF	stát
se	s	k7c7	s
knězem	kněz	k1gMnSc7	kněz
a	a	k8xC	a
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
přísnému	přísný	k2eAgInSc3d1	přísný
klášternímu	klášterní	k2eAgInSc3d1	klášterní
životu	život	k1gInSc3	život
–	–	k?	–
názory	názor	k1gInPc7	názor
<g/>
,	,	kIx,	,
od	od	k7c2	od
nichž	jenž	k3xRgMnPc2	jenž
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
přešel	přejít	k5eAaPmAgInS	přejít
k	k	k7c3	k
náboženskému	náboženský	k2eAgInSc3d1	náboženský
konzervatismu	konzervatismus	k1gInSc3	konzervatismus
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
pracím	práce	k1gFnPc3	práce
patří	patřit	k5eAaImIp3nS	patřit
dějepisné	dějepisný	k2eAgFnPc4d1	dějepisná
studie	studie	k1gFnPc4	studie
<g/>
,	,	kIx,	,
kritická	kritický	k2eAgNnPc4d1	kritické
vydání	vydání	k1gNnPc4	vydání
historických	historický	k2eAgInPc2d1	historický
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
právního	právní	k2eAgInSc2d1	právní
řádu	řád	k1gInSc2	řád
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
a	a	k8xC	a
životopisy	životopis	k1gInPc1	životopis
několika	několik	k4yIc2	několik
osobností	osobnost	k1gFnPc2	osobnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Handbuch	Handbuch	k1gMnSc1	Handbuch
der	drát	k5eAaImRp2nS	drát
Mährischen	Mährischna	k1gFnPc2	Mährischna
Vaterlandskunde	Vaterlandskund	k1gInSc5	Vaterlandskund
<g/>
,	,	kIx,	,
Brünn	Brünn	k1gInSc1	Brünn
1860	[number]	k4	1860
</s>
</p>
<p>
<s>
Welehrad	Welehrad	k1gInSc1	Welehrad
<g/>
,	,	kIx,	,
Brünn	Brünn	k1gInSc1	Brünn
1860	[number]	k4	1860
</s>
</p>
<p>
<s>
Poloha	poloha	k1gFnSc1	poloha
Starého	Starého	k2eAgInSc2d1	Starého
Velehradu	Velehrad	k1gInSc2	Velehrad
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
Moravana	Moravan	k1gMnSc4	Moravan
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
národopisná	národopisný	k2eAgFnSc1d1	národopisná
a	a	k8xC	a
místopisná	místopisný	k2eAgFnSc1d1	místopisná
publikace	publikace	k1gFnSc1	publikace
pro	pro	k7c4	pro
širokou	široký	k2eAgFnSc4d1	široká
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
,	,	kIx,	,
vydaná	vydaný	k2eAgFnSc1d1	vydaná
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
tisícího	tisící	k4xOgNnSc2	tisící
výročí	výročí	k1gNnSc2	výročí
příchodu	příchod	k1gInSc2	příchod
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
podpořit	podpořit	k5eAaPmF	podpořit
národní	národní	k2eAgNnSc4d1	národní
a	a	k8xC	a
historické	historický	k2eAgNnSc4d1	historické
povědomí	povědomí	k1gNnSc4	povědomí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sněm	sněm	k1gInSc1	sněm
držaný	držaný	k2eAgInSc1d1	držaný
léta	léto	k1gNnPc4	léto
1612	[number]	k4	1612
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
spisů	spis	k1gInPc2	spis
věnovaných	věnovaný	k2eAgInPc2d1	věnovaný
odkazu	odkaz	k1gInSc3	odkaz
Karla	Karel	k1gMnSc2	Karel
staršího	starší	k1gMnSc2	starší
ze	z	k7c2	z
Žerotína	Žerotín	k1gInSc2	Žerotín
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
Drnovská	Drnovská	k1gFnSc1	Drnovská
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kritické	kritický	k2eAgInPc4d1	kritický
a	a	k8xC	a
poznámkami	poznámka	k1gFnPc7	poznámka
opatřené	opatřený	k2eAgFnSc2d1	opatřená
vydání	vydání	k1gNnSc6	vydání
historických	historický	k2eAgInPc2d1	historický
záznamů	záznam	k1gInPc2	záznam
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
Tovačovská	Tovačovská	k1gFnSc1	Tovačovská
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
Pana	Pan	k1gMnSc2	Pan
Ctibora	Ctibor	k1gMnSc2	Ctibor
z	z	k7c2	z
Cimburka	Cimburek	k1gMnSc2	Cimburek
a	a	k8xC	a
z	z	k7c2	z
Tovačova	Tovačův	k2eAgInSc2d1	Tovačův
Pamět	Pamět	k?	Pamět
obyčejů	obyčej	k1gInPc2	obyčej
<g/>
,	,	kIx,	,
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
zvyklostí	zvyklost	k1gFnPc2	zvyklost
starodávných	starodávný	k2eAgFnPc2d1	starodávná
a	a	k8xC	a
řízení	řízení	k1gNnPc1	řízení
práva	právo	k1gNnSc2	právo
zemského	zemský	k2eAgNnSc2d1	zemské
v	v	k7c6	v
Mar	Mar	k1gFnSc6	Mar
<g/>
.	.	kIx.	.
</s>
<s>
Mor	mor	k1gInSc1	mor
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Libri	Libri	k6eAd1	Libri
citationum	citationum	k1gInSc1	citationum
et	et	k?	et
sententiarum	sententiarum	k1gInSc1	sententiarum
<g/>
,	,	kIx,	,
seu	seu	k?	seu
<g/>
,	,	kIx,	,
Knihy	kniha	k1gFnPc4	kniha
půhonné	půhonný	k2eAgFnPc4d1	půhonný
a	a	k8xC	a
nálezové	nálezový	k2eAgNnSc1d1	nálezový
(	(	kIx(	(
<g/>
řada	řada	k1gFnSc1	řada
svazků	svazek	k1gInPc2	svazek
po	po	k7c6	po
r.	r.	kA	r.
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kritická	kritický	k2eAgFnSc1d1	kritická
úvaha	úvaha	k1gFnSc1	úvaha
o	o	k7c6	o
pojednání	pojednání	k1gNnSc6	pojednání
Alfonsa	Alfons	k1gMnSc2	Alfons
Šťastného	Šťastný	k1gMnSc2	Šťastný
"	"	kIx"	"
<g/>
Ježíš	Ježíš	k1gMnSc1	Ježíš
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
poměr	poměr	k1gInSc1	poměr
ku	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kritická	kritický	k2eAgFnSc1d1	kritická
úvaha	úvaha	k1gFnSc1	úvaha
o	o	k7c6	o
pojednání	pojednání	k1gNnSc6	pojednání
Alfonsa	Alfons	k1gMnSc2	Alfons
Šťastného	Šťastný	k1gMnSc2	Šťastný
o	o	k7c4	o
spasení	spasení	k1gNnSc4	spasení
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
argumentace	argumentace	k1gFnSc1	argumentace
proti	proti	k7c3	proti
tvrzením	tvrzení	k1gNnSc7	tvrzení
padařovského	padařovský	k2eAgMnSc2d1	padařovský
sedláka	sedlák	k1gMnSc2	sedlák
a	a	k8xC	a
propagátora	propagátor	k1gMnSc2	propagátor
ateismu	ateismus	k1gInSc2	ateismus
Alfonse	Alfons	k1gMnSc2	Alfons
Šťastného	Šťastný	k1gMnSc2	Šťastný
<g/>
.	.	kIx.	.
</s>
<s>
Brandl	Brandnout	k5eAaPmAgMnS	Brandnout
varuje	varovat	k5eAaImIp3nS	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
materialismus	materialismus	k1gInSc1	materialismus
bez	bez	k7c2	bez
naděje	naděje	k1gFnSc2	naděje
na	na	k7c4	na
posmrtný	posmrtný	k2eAgInSc4d1	posmrtný
život	život	k1gInSc4	život
otevírá	otevírat	k5eAaImIp3nS	otevírat
cestu	cesta	k1gFnSc4	cesta
anarchii	anarchie	k1gFnSc3	anarchie
<g/>
,	,	kIx,	,
násilí	násilí	k1gNnSc3	násilí
a	a	k8xC	a
zločinu	zločin	k1gInSc3	zločin
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ukázala	ukázat	k5eAaPmAgFnS	ukázat
krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
komuna	komuna	k1gFnSc1	komuna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Glossarium	glossarium	k1gNnSc1	glossarium
illustrans	illustrans	k6eAd1	illustrans
bohemico-moravicae	bohemicooravicaat	k5eAaPmIp3nS	bohemico-moravicaat
historiae	historiae	k1gFnSc4	historiae
fontes	fontesa	k1gFnPc2	fontesa
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovník	slovník	k1gInSc1	slovník
staročeských	staročeský	k2eAgInPc2d1	staročeský
výrazů	výraz	k1gInPc2	výraz
<g/>
,	,	kIx,	,
vyskytujících	vyskytující	k2eAgMnPc2d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
historických	historický	k2eAgInPc6d1	historický
dokumentech	dokument	k1gInPc6	dokument
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
Rožmberská	rožmberský	k2eAgFnSc1d1	Rožmberská
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kritické	kritický	k2eAgNnSc4d1	kritické
a	a	k8xC	a
komentované	komentovaný	k2eAgNnSc4d1	komentované
vydání	vydání	k1gNnSc4	vydání
nejstarší	starý	k2eAgFnSc2d3	nejstarší
památky	památka	k1gFnSc2	památka
slovanského	slovanský	k2eAgNnSc2d1	slovanské
práva	právo	k1gNnSc2	právo
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnPc1d1	pocházející
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1300	[number]	k4	1300
<g/>
.	.	kIx.	.
</s>
<s>
Brandl	Brandnout	k5eAaPmAgMnS	Brandnout
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
zpřístupnil	zpřístupnit	k5eAaPmAgInS	zpřístupnit
studentům	student	k1gMnPc3	student
a	a	k8xC	a
badatelům	badatel	k1gMnPc3	badatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nemohli	moct	k5eNaImAgMnP	moct
nebo	nebo	k8xC	nebo
nechtěli	chtít	k5eNaImAgMnP	chtít
studovat	studovat	k5eAaImF	studovat
historické	historický	k2eAgInPc4d1	historický
archivní	archivní	k2eAgInPc4d1	archivní
materiály	materiál	k1gInPc4	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obrana	obrana	k1gFnSc1	obrana
Libušina	Libušin	k2eAgInSc2d1	Libušin
soudu	soud	k1gInSc2	soud
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
orlici	orlice	k1gFnSc6	orlice
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
–	–	k?	–
Studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Brandl	Brandl	k1gFnSc6	Brandl
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jazykovědných	jazykovědný	k2eAgInPc2d1	jazykovědný
argumentů	argument	k1gInPc2	argument
vyvrací	vyvracet	k5eAaImIp3nS	vyvracet
tvrzení	tvrzení	k1gNnSc1	tvrzení
profesora	profesor	k1gMnSc2	profesor
Šembery	Šembera	k1gMnSc2	Šembera
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rukopis	rukopis	k1gInSc1	rukopis
zelenohorský	zelenohorský	k2eAgInSc1d1	zelenohorský
je	být	k5eAaImIp3nS	být
novodobý	novodobý	k2eAgInSc1d1	novodobý
padělek	padělek	k1gInSc1	padělek
<g/>
.	.	kIx.	.
</s>
<s>
Pozdějších	pozdní	k2eAgInPc2d2	pozdější
Sporů	spor	k1gInPc2	spor
o	o	k7c4	o
Rukopisy	rukopis	k1gInPc4	rukopis
se	se	k3xPyFc4	se
ale	ale	k9	ale
už	už	k6eAd1	už
neúčastnil	účastnit	k5eNaImAgMnS	účastnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
Josefa	Josef	k1gMnSc2	Josef
Dobrovského	Dobrovský	k1gMnSc2	Dobrovský
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
životopis	životopis	k1gInSc1	životopis
významného	významný	k2eAgMnSc2d1	významný
jazykovědce	jazykovědec	k1gMnSc2	jazykovědec
<g/>
,	,	kIx,	,
oceňovaný	oceňovaný	k2eAgInSc4d1	oceňovaný
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
podrobnost	podrobnost	k1gFnSc4	podrobnost
a	a	k8xC	a
objektivitu	objektivita	k1gFnSc4	objektivita
<g/>
,	,	kIx,	,
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
každodenní	každodenní	k2eAgInSc4d1	každodenní
život	život	k1gInSc4	život
a	a	k8xC	a
kladné	kladný	k2eAgFnPc4d1	kladná
i	i	k8xC	i
záporné	záporný	k2eAgFnPc4d1	záporná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
tu	tu	k6eAd1	tu
na	na	k7c6	na
dopisech	dopis	k1gInPc6	dopis
dokládá	dokládat	k5eAaImIp3nS	dokládat
duševní	duševní	k2eAgFnSc4d1	duševní
poruchu	porucha	k1gFnSc4	porucha
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
života	život	k1gInSc2	život
trpěl	trpět	k5eAaImAgInS	trpět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
Pavla	Pavel	k1gMnSc2	Pavel
Josefa	Josef	k1gMnSc2	Josef
Šafaříka	Šafařík	k1gMnSc2	Šafařík
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
Karla	Karel	k1gMnSc2	Karel
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Erbena	Erben	k1gMnSc2	Erben
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
oceňovaný	oceňovaný	k2eAgMnSc1d1	oceňovaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
projevuje	projevovat	k5eAaImIp3nS	projevovat
jistý	jistý	k2eAgInSc4d1	jistý
úpadek	úpadek	k1gInSc4	úpadek
–	–	k?	–
Brandl	Brandl	k1gFnPc2	Brandl
např.	např.	kA	např.
nedocenil	docenit	k5eNaPmAgInS	docenit
jeho	jeho	k3xOp3gFnSc4	jeho
Kytici	kytice	k1gFnSc4	kytice
a	a	k8xC	a
sbírku	sbírka	k1gFnSc4	sbírka
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
.	.	kIx.	.
<g/>
Vydal	vydat	k5eAaPmAgMnS	vydat
moravské	moravský	k2eAgFnPc4d1	Moravská
památky	památka	k1gFnPc4	památka
právního	právní	k2eAgInSc2d1	právní
charakteru	charakter	k1gInSc2	charakter
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
předbělohorské	předbělohorský	k2eAgFnSc2d1	předbělohorská
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Kniha	kniha	k1gFnSc1	kniha
tovačovská	tovačovská	k1gFnSc1	tovačovská
<g/>
,	,	kIx,	,
Kniha	kniha	k1gFnSc1	kniha
drnovská	drnovská	k1gFnSc1	drnovská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sněmovní	sněmovní	k2eAgFnPc1d1	sněmovní
památky	památka	k1gFnPc1	památka
<g/>
,	,	kIx,	,
zemské	zemský	k2eAgInPc1d1	zemský
půhony	půhon	k1gInPc1	půhon
a	a	k8xC	a
nálezy	nález	k1gInPc1	nález
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgInSc1d1	moravský
diplomatář	diplomatář	k1gInSc1	diplomatář
<g/>
,	,	kIx,	,
spisy	spis	k1gInPc1	spis
Karla	Karel	k1gMnSc2	Karel
staršího	starší	k1gMnSc2	starší
ze	z	k7c2	z
Žerotína	Žerotín	k1gInSc2	Žerotín
a	a	k8xC	a
biografie	biografie	k1gFnSc1	biografie
Karla	Karel	k1gMnSc2	Karel
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Erbena	Erben	k1gMnSc2	Erben
<g/>
,	,	kIx,	,
Josefa	Josef	k1gMnSc2	Josef
Dobrovského	Dobrovský	k1gMnSc2	Dobrovský
či	či	k8xC	či
Pavla	Pavel	k1gMnSc2	Pavel
Josefa	Josef	k1gMnSc2	Josef
Šafaříka	Šafařík	k1gMnSc2	Šafařík
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
zastáncům	zastánce	k1gMnPc3	zastánce
pravosti	pravost	k1gFnSc2	pravost
Zelenohorského	zelenohorský	k2eAgInSc2d1	zelenohorský
rukopisu	rukopis	k1gInSc2	rukopis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Slavíček	Slavíček	k1gMnSc1	Slavíček
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Slovník	slovník	k1gInSc1	slovník
historiků	historik	k1gMnPc2	historik
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
kritiků	kritik	k1gMnPc2	kritik
<g/>
,	,	kIx,	,
teoretiků	teoretik	k1gMnPc2	teoretik
a	a	k8xC	a
publicistů	publicista	k1gMnPc2	publicista
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
z	z	k7c2	z
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
oborů	obor	k1gInPc2	obor
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1800	[number]	k4	1800
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sv.	sv.	kA	sv.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
145	[number]	k4	145
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
Praha	Praha	k1gFnSc1	Praha
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-200-2094-9	[number]	k4	978-80-200-2094-9
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
70	[number]	k4	70
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
7	[number]	k4	7
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
Bra	Bra	k1gFnSc1	Bra
<g/>
–	–	k?	–
<g/>
Brum	brum	k1gInSc1	brum
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
110	[number]	k4	110
<g/>
–	–	k?	–
<g/>
224	[number]	k4	224
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
248	[number]	k4	248
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
133	[number]	k4	133
<g/>
–	–	k?	–
<g/>
134	[number]	k4	134
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vincenc	Vincenc	k1gMnSc1	Vincenc
Brandl	Brandl	k1gFnSc7	Brandl
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Vincenc	Vincenc	k1gMnSc1	Vincenc
Brandl	Brandl	k1gFnSc2	Brandl
–	–	k?	–
Digitalizované	digitalizovaný	k2eAgFnSc2d1	digitalizovaná
knihy	kniha	k1gFnSc2	kniha
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Vincenc	Vincenc	k1gMnSc1	Vincenc
Brandl	Brandl	k1gMnSc1	Brandl
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Vincenc	Vincenc	k1gMnSc1	Vincenc
Brandl	Brandl	k1gMnSc1	Brandl
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Vincenc	Vincenc	k1gMnSc1	Vincenc
Brandl	Brandl	k1gMnSc1	Brandl
</s>
</p>
