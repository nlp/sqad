<s>
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgInSc1	třetí
největší	veliký	k2eAgInSc1d3	veliký
oceán	oceán	k1gInSc1	oceán
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
východním	východní	k2eAgNnSc7d1	východní
pobřežím	pobřeží	k1gNnSc7	pobřeží
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc7d1	jižní
částí	část	k1gFnSc7	část
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
západním	západní	k2eAgNnSc7d1	západní
pobřežím	pobřeží	k1gNnSc7	pobřeží
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
Antarktidou	Antarktida	k1gFnSc7	Antarktida
<g/>
.	.	kIx.	.
</s>
