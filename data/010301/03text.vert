<p>
<s>
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgInSc1	třetí
největší	veliký	k2eAgInSc1d3	veliký
oceán	oceán	k1gInSc1	oceán
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
východním	východní	k2eAgNnSc7d1	východní
pobřežím	pobřeží	k1gNnSc7	pobřeží
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc7d1	jižní
částí	část	k1gFnSc7	část
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
západním	západní	k2eAgNnSc7d1	západní
pobřežím	pobřeží	k1gNnSc7	pobřeží
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
Antarktidou	Antarktida	k1gFnSc7	Antarktida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografické	geografický	k2eAgFnPc1d1	geografická
charakteristiky	charakteristika	k1gFnPc1	charakteristika
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rozloha	rozloha	k1gFnSc1	rozloha
je	být	k5eAaImIp3nS	být
73,556	[number]	k4	73,556
milionu	milion	k4xCgInSc2	milion
km2	km2	k4	km2
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
procent	procento	k1gNnPc2	procento
povrchu	povrch	k1gInSc2	povrch
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
292,131	[number]	k4	292,131
milionu	milion	k4xCgInSc2	milion
km3	km3	k4	km3
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
°	°	k?	°
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
)	)	kIx)	)
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Atlantickým	atlantický	k2eAgInSc7d1	atlantický
oceánem	oceán	k1gInSc7	oceán
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
(	(	kIx(	(
<g/>
60	[number]	k4	60
<g/>
°	°	k?	°
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
)	)	kIx)	)
s	s	k7c7	s
Jižním	jižní	k2eAgInSc7d1	jižní
ledovým	ledový	k2eAgInSc7d1	ledový
oceánem	oceán	k1gInSc7	oceán
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
(	(	kIx(	(
<g/>
146	[number]	k4	146
<g/>
°	°	k?	°
<g/>
55	[number]	k4	55
<g/>
'	'	kIx"	'
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
)	)	kIx)	)
s	s	k7c7	s
Tichým	tichý	k2eAgInSc7d1	tichý
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
Středozemním	středozemní	k2eAgNnSc7d1	středozemní
mořem	moře	k1gNnSc7	moře
jej	on	k3xPp3gMnSc4	on
spojuje	spojovat	k5eAaImIp3nS	spojovat
Suezský	suezský	k2eAgInSc1d1	suezský
průplav	průplav	k1gInSc1	průplav
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Jihočínským	jihočínský	k2eAgNnSc7d1	Jihočínské
mořem	moře	k1gNnSc7	moře
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
Malackým	malacký	k2eAgInSc7d1	malacký
průlivem	průliv	k1gInSc7	průliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klimatické	klimatický	k2eAgFnPc1d1	klimatická
podmínky	podmínka	k1gFnPc1	podmínka
===	===	k?	===
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
je	být	k5eAaImIp3nS	být
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
nejteplejším	teplý	k2eAgInSc7d3	nejteplejší
světovým	světový	k2eAgInSc7d1	světový
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gFnSc2	jeho
plochy	plocha	k1gFnSc2	plocha
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
tropickém	tropický	k2eAgInSc6d1	tropický
nebo	nebo	k8xC	nebo
subtropickém	subtropický	k2eAgInSc6d1	subtropický
podnebném	podnebný	k2eAgInSc6d1	podnebný
pásu	pás	k1gInSc6	pás
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
monzunové	monzunový	k2eAgNnSc1d1	monzunové
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
jsou	být	k5eAaImIp3nP	být
větry	vítr	k1gInPc1	vítr
mírnější	mírný	k2eAgInPc1d2	mírnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
v	v	k7c6	v
číslech	číslo	k1gNnPc6	číslo
===	===	k?	===
</s>
</p>
<p>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
73,440	[number]	k4	73,440
milionu	milion	k4xCgInSc2	milion
km2	km2	k4	km2
(	(	kIx(	(
<g/>
hraničí	hraničit	k5eAaImIp3nS	hraničit
<g/>
-li	i	k?	-li
až	až	k6eAd1	až
s	s	k7c7	s
Antarktidou	Antarktida	k1gFnSc7	Antarktida
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
např.	např.	kA	např.
podle	podle	k7c2	podle
definice	definice	k1gFnSc2	definice
Národní	národní	k2eAgFnSc2d1	národní
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
společnosti	společnost	k1gFnSc2	společnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
68,556	[number]	k4	68,556
milionu	milion	k4xCgInSc2	milion
km2	km2	k4	km2
(	(	kIx(	(
<g/>
hraničí	hraničit	k5eAaImIp3nS	hraničit
<g/>
-li	i	k?	-li
na	na	k7c4	na
jižní	jižní	k2eAgNnSc4d1	jižní
60	[number]	k4	60
<g/>
.	.	kIx.	.
rovnoběžce	rovnoběžka	k1gFnSc3	rovnoběžka
s	s	k7c7	s
Jižním	jižní	k2eAgInSc7d1	jižní
oceánem	oceán	k1gInSc7	oceán
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
podle	podle	k7c2	podle
definice	definice	k1gFnSc2	definice
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
hydrografické	hydrografický	k2eAgFnSc2d1	hydrografická
organizace	organizace	k1gFnSc2	organizace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejhlubší	hluboký	k2eAgNnSc1d3	nejhlubší
místo	místo	k1gNnSc1	místo
<g/>
:	:	kIx,	:
Jávský	jávský	k2eAgInSc1d1	jávský
příkop	příkop	k1gInSc1	příkop
-	-	kIx~	-
7	[number]	k4	7
<g/>
,	,	kIx,	,
725	[number]	k4	725
m	m	kA	m
</s>
</p>
<p>
<s>
Podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
rozloze	rozloha	k1gFnSc6	rozloha
Země	zem	k1gFnSc2	zem
<g/>
:	:	kIx,	:
14,9	[number]	k4	14,9
%	%	kIx~	%
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
záliv	záliv	k1gInSc1	záliv
<g/>
:	:	kIx,	:
Bengálský	bengálský	k2eAgInSc1d1	bengálský
záliv	záliv	k1gInSc1	záliv
-	-	kIx~	-
2,172	[number]	k4	2,172
milionu	milion	k4xCgInSc2	milion
km2	km2	k4	km2
(	(	kIx(	(
<g/>
největší	veliký	k2eAgFnSc2d3	veliký
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Významné	významný	k2eAgInPc1d1	významný
ostrovy	ostrov	k1gInPc1	ostrov
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
charakteristiky	charakteristika	k1gFnPc1	charakteristika
==	==	k?	==
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
absenci	absence	k1gFnSc3	absence
významných	významný	k2eAgInPc2d1	významný
studených	studený	k2eAgInPc2d1	studený
proudů	proud	k1gInPc2	proud
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
pouze	pouze	k6eAd1	pouze
nízkému	nízký	k2eAgInSc3d1	nízký
výlovu	výlov	k1gInSc2	výlov
ryb	ryba	k1gFnPc2	ryba
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
krevety	kreveta	k1gFnSc2	kreveta
a	a	k8xC	a
tuňák	tuňák	k1gMnSc1	tuňák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
vedou	vést	k5eAaImIp3nP	vést
důležité	důležitý	k2eAgFnPc1d1	důležitá
námořní	námořní	k2eAgFnPc1d1	námořní
trasy	trasa	k1gFnPc1	trasa
po	po	k7c6	po
nichž	jenž	k3xRgFnPc6	jenž
je	být	k5eAaImIp3nS	být
přepravováno	přepravován	k2eAgNnSc1d1	přepravováno
zboží	zboží	k1gNnSc1	zboží
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
východní	východní	k2eAgFnSc7d1	východní
Asií	Asie	k1gFnSc7	Asie
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
<g/>
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
ropy	ropa	k1gFnSc2	ropa
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
somálského	somálský	k2eAgNnSc2d1	somálské
pobřeží	pobřeží	k1gNnSc2	pobřeží
a	a	k8xC	a
v	v	k7c6	v
Malackém	malacký	k2eAgInSc6d1	malacký
průlivu	průliv	k1gInSc6	průliv
trvá	trvat	k5eAaImIp3nS	trvat
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
námořního	námořní	k2eAgNnSc2d1	námořní
pirátství	pirátství	k1gNnSc2	pirátství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Významné	významný	k2eAgInPc1d1	významný
přístavy	přístav	k1gInPc1	přístav
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Fauna	fauna	k1gFnSc1	fauna
a	a	k8xC	a
flóra	flóra	k1gFnSc1	flóra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Rudém	rudý	k2eAgNnSc6d1	Rudé
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
kolem	kolem	k7c2	kolem
souostroví	souostroví	k1gNnSc2	souostroví
Lakadivy	Lakadivy	k1gFnPc4	Lakadivy
a	a	k8xC	a
Maledivy	Maledivy	k1gFnPc4	Maledivy
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
množství	množství	k1gNnSc1	množství
korálových	korálový	k2eAgInPc2d1	korálový
útesů	útes	k1gInPc2	útes
<g/>
.	.	kIx.	.
</s>
<s>
Oceán	oceán	k1gInSc1	oceán
je	být	k5eAaImIp3nS	být
také	také	k9	také
domovem	domov	k1gInSc7	domov
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
želv	želva	k1gFnPc2	želva
<g/>
,	,	kIx,	,
velryb	velryba	k1gFnPc2	velryba
<g/>
,	,	kIx,	,
ploutvonožců	ploutvonožec	k1gMnPc2	ploutvonožec
a	a	k8xC	a
vodních	vodní	k2eAgMnPc2d1	vodní
savců	savec	k1gMnPc2	savec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Dugong	dugong	k1gMnSc1	dugong
indický	indický	k2eAgMnSc1d1	indický
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
znečištění	znečištění	k1gNnSc2	znečištění
moře	moře	k1gNnSc2	moře
ropou	ropa	k1gFnSc7	ropa
a	a	k8xC	a
lodní	lodní	k2eAgFnSc7d1	lodní
dopravou	doprava	k1gFnSc7	doprava
je	být	k5eAaImIp3nS	být
aktuální	aktuální	k2eAgFnSc1d1	aktuální
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
Rudé	rudý	k2eAgNnSc4d1	Rudé
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
Arabské	arabský	k2eAgNnSc4d1	arabské
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
Perský	perský	k2eAgInSc4d1	perský
záliv	záliv	k1gInSc4	záliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Moře	moře	k1gNnSc1	moře
a	a	k8xC	a
oceány	oceán	k1gInPc1	oceán
<g/>
,	,	kIx,	,
Editions	Editions	k1gInSc1	Editions
Atlas	Atlas	k1gInSc1	Atlas
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Indický	indický	k2eAgInSc4d1	indický
oceán	oceán	k1gInSc4	oceán
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
