<s>
Západní	západní	k2eAgInSc1d1
břeh	břeh	k1gInSc1
Jordánu	Jordán	k1gInSc2
</s>
<s>
Západní	západní	k2eAgInSc1d1
břeh	břeh	k1gInSc1
Jordánu	Jordán	k1gInSc2
ا	ا	k?
ا	ا	k?
<g/>
(	(	kIx(
<g/>
ad-Dafa	ad-Daf	k1gMnSc2
al-Gharbíja	al-Gharbíjus	k1gMnSc2
<g/>
)	)	kIx)
Geografie	geografie	k1gFnSc1
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Ramalláh	Ramalláh	k1gInSc1
Status	status	k1gInSc1
</s>
<s>
a	a	k8xC
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
31	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
35	#num#	k4
<g/>
°	°	k?
<g/>
18	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
5	#num#	k4
860	#num#	k4
km²	km²	k?
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
</s>
<s>
Tal	Tal	k?
Ásúr	Ásúr	k1gInSc1
(	(	kIx(
<g/>
1022	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
2	#num#	k4
676	#num#	k4
740	#num#	k4
(	(	kIx(
<g/>
odhad	odhad	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
456,8	456,8	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Jazyk	jazyk	k1gInSc1
</s>
<s>
arabština	arabština	k1gFnSc1
(	(	kIx(
<g/>
úřední	úřední	k2eAgNnSc1d1
<g/>
)	)	kIx)
Náboženství	náboženství	k1gNnSc1
</s>
<s>
islám	islám	k1gInSc1
<g/>
,	,	kIx,
judaismus	judaismus	k1gInSc1
<g/>
,	,	kIx,
křesťanství	křesťanství	k1gNnSc4
Správa	správa	k1gFnSc1
regionu	region	k1gInSc2
Vznik	vznik	k1gInSc1
</s>
<s>
1949	#num#	k4
po	po	k7c4
první	první	k4xOgNnSc4
arabsko	arabsko	k1gNnSc4
<g/>
–	–	k?
<g/>
izraelské	izraelský	k2eAgFnSc3d1
válce	válka	k1gFnSc3
Prezident	prezident	k1gMnSc1
</s>
<s>
Mahmúd	Mahmúd	k1gMnSc1
Abbás	Abbása	k1gFnPc2
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Salám	salám	k1gInSc1
Fajjád	Fajjáda	k1gFnPc2
Měna	měna	k1gFnSc1
</s>
<s>
izraelský	izraelský	k2eAgInSc1d1
šekel	šekel	k1gInSc1
<g/>
,	,	kIx,
jordánský	jordánský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
Mezinárodní	mezinárodní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
+970	+970	k4
<g/>
,	,	kIx,
+972	+972	k4
Internetová	internetový	k2eAgFnSc1d1
doména	doména	k1gFnSc1
</s>
<s>
ps	ps	k0
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Západní	západní	k2eAgInSc1d1
břeh	břeh	k1gInSc1
Jordánu	Jordán	k1gInSc2
(	(	kIx(
<g/>
též	též	k9
Předjordánsko	Předjordánsko	k1gNnSc1
<g/>
,	,	kIx,
hebrejsky	hebrejsky	k6eAd1
<g/>
:	:	kIx,
י	י	k?
ו	ו	k?
<g/>
,	,	kIx,
Jehuda	Jehuda	k1gMnSc1
ve-Šomron	ve-Šomron	k1gMnSc1
(	(	kIx(
<g/>
Judea	Judea	k1gFnSc1
a	a	k8xC
Samaří	Samaří	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
ה	ה	k?
ה	ה	k?
<g/>
,	,	kIx,
ha-Gada	ha-Gada	k1gFnSc1
ha-ma	ha-ma	k1gFnSc1
<g/>
'	'	kIx"
<g/>
aravit	aravit	k1gInSc1
(	(	kIx(
<g/>
Západní	západní	k2eAgInSc1d1
břeh	břeh	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
arabsky	arabsky	k6eAd1
<g/>
:	:	kIx,
ا	ا	k?
ا	ا	k?
<g/>
,	,	kIx,
ad-Daffa	ad-Daff	k1gMnSc2
al-gharbíja	al-gharbíjus	k1gMnSc2
(	(	kIx(
<g/>
Západní	západní	k2eAgInSc1d1
břeh	břeh	k1gInSc1
<g/>
))	))	k?
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
pro	pro	k7c4
vnitrozemské	vnitrozemský	k2eAgNnSc4d1
území	území	k1gNnSc4
na	na	k7c6
západním	západní	k2eAgInSc6d1
(	(	kIx(
<g/>
pravém	pravý	k2eAgInSc6d1
<g/>
)	)	kIx)
břehu	břeh	k1gInSc6
řeky	řeka	k1gFnSc2
Jordán	Jordán	k1gInSc1
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodním	mezinárodní	k2eAgNnSc7d1
společenstvím	společenství	k1gNnSc7
je	být	k5eAaImIp3nS
částečně	částečně	k6eAd1
uznáváno	uznáván	k2eAgNnSc1d1
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc1
Palestinských	palestinský	k2eAgNnPc2d1
území	území	k1gNnPc2
(	(	kIx(
<g/>
Západní	západní	k2eAgInSc1d1
břeh	břeh	k1gInSc1
Jordánu	Jordán	k1gInSc2
a	a	k8xC
Pásmo	pásmo	k1gNnSc4
Gazy	Gaza	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Původně	původně	k6eAd1
kanaánské	kanaánský	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
,	,	kIx,
od	od	k7c2
14	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
postupně	postupně	k6eAd1
osidlované	osidlovaný	k2eAgInPc1d1
izraelskými	izraelský	k2eAgInPc7d1
kmeny	kmen	k1gInPc7
<g/>
,	,	kIx,
tzv.	tzv.	kA
Samaří	Samaří	k1gNnSc4
v	v	k7c6
severním	severní	k2eAgNnSc6d1
království	království	k1gNnSc6
do	do	k7c2
722	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byli	být	k5eAaImAgMnP
tamní	tamní	k2eAgMnPc1d1
Židé	Žid	k1gMnPc1
vystěhováni	vystěhovat	k5eAaPmNgMnP
do	do	k7c2
Asýrie	Asýrie	k1gFnSc2
a	a	k8xC
území	území	k1gNnSc1
částečně	částečně	k6eAd1
osídleno	osídlit	k5eAaPmNgNnS
jinými	jiný	k2eAgInPc7d1
národy	národ	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Kýrově	Kýrův	k2eAgInSc6d1
ediktu	edikt	k1gInSc6
v	v	k7c6
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
bylo	být	k5eAaImAgNnS
znovu	znovu	k6eAd1
osidlováno	osidlovat	k5eAaImNgNnS
Židy	Žid	k1gMnPc7
–	–	k?
po	po	k7c6
prvním	první	k4xOgInSc6
židovském	židovský	k2eAgNnSc6d1
povstání	povstání	k1gNnSc6
a	a	k8xC
po	po	k7c6
povstání	povstání	k1gNnSc6
Bar	bar	k1gInSc1
Kochby	Kochba	k1gFnSc2
132	#num#	k4
<g/>
–	–	k?
<g/>
135	#num#	k4
n.	n.	k?
l.	l.	k?
nastává	nastávat	k5eAaImIp3nS
pokles	pokles	k1gInSc1
trvalého	trvalý	k2eAgNnSc2d1
osídlení	osídlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
byzantské	byzantský	k2eAgFnSc6d1
<g/>
,	,	kIx,
mamlúcké	mamlúcký	k2eAgFnSc6d1
a	a	k8xC
turecké	turecký	k2eAgFnSc6d1
nadvládě	nadvláda	k1gFnSc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
postupnému	postupný	k2eAgNnSc3d1
židovskému	židovský	k2eAgNnSc3d1
znovuosídlení	znovuosídlení	k1gNnSc3
a	a	k8xC
začínají	začínat	k5eAaImIp3nP
se	se	k3xPyFc4
trvaleji	trvale	k6eAd2
usazovat	usazovat	k5eAaImF
i	i	k9
první	první	k4xOgMnPc1
Arabové	Arab	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
porážce	porážka	k1gFnSc6
osmanského	osmanský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
v	v	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
bylo	být	k5eAaImAgNnS
toto	tento	k3xDgNnSc1
území	území	k1gNnSc1
do	do	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
součástí	součást	k1gFnSc7
britské	britský	k2eAgFnSc2d1
mandátní	mandátní	k2eAgFnSc2d1
Palestiny	Palestina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
izraelské	izraelský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
obsadilo	obsadit	k5eAaPmAgNnS
území	území	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
v	v	k7c6
r.	r.	kA
1947	#num#	k4
předpokládala	předpokládat	k5eAaImAgFnS
OSN	OSN	kA
jako	jako	k8xS,k8xC
prostor	prostor	k1gInSc1
pro	pro	k7c4
arabskou	arabský	k2eAgFnSc4d1
„	„	k?
<g/>
národní	národní	k2eAgFnSc4d1
domovinu	domovina	k1gFnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
tehdejší	tehdejší	k2eAgNnSc1d1
Zajordánsko	Zajordánsko	k1gNnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
Západní	západní	k2eAgInSc1d1
břeh	břeh	k1gInSc1
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1950	#num#	k4
anektovalo	anektovat	k5eAaBmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Arabská	arabský	k2eAgFnSc1d1
populace	populace	k1gFnSc1
nevyvinula	vyvinout	k5eNaPmAgFnS
ani	ani	k8xC
před	před	k7c7
válkou	válka	k1gFnSc7
<g/>
,	,	kIx,
ani	ani	k8xC
po	po	k7c6
válce	válka	k1gFnSc6
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
snahu	snaha	k1gFnSc4
o	o	k7c4
vytvoření	vytvoření	k1gNnSc4
státu	stát	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
území	území	k1gNnSc1
fakticky	fakticky	k6eAd1
nepatřilo	patřit	k5eNaImAgNnS
žádné	žádný	k3yNgFnSc3
existující	existující	k2eAgFnSc3d1
zemi	zem	k1gFnSc3
v	v	k7c6
regionu	region	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1967	#num#	k4
bylo	být	k5eAaImAgNnS
území	území	k1gNnSc1
součástí	součást	k1gFnPc2
Jordánska	Jordánsko	k1gNnSc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
je	být	k5eAaImIp3nS
v	v	k7c6
šestidenní	šestidenní	k2eAgFnSc6d1
válce	válka	k1gFnSc6
obsadil	obsadit	k5eAaPmAgInS
Izrael	Izrael	k1gInSc1
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
je	být	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
okupováno	okupován	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
pohledu	pohled	k1gInSc2
mezinárodní	mezinárodní	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
je	být	k5eAaImIp3nS
území	území	k1gNnSc1
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
součást	součást	k1gFnSc4
vznikajícího	vznikající	k2eAgInSc2d1
Státu	stát	k1gInSc2
Palestina	Palestina	k1gFnSc1
s	s	k7c7
ne	ne	k9
jasně	jasně	k6eAd1
definovanými	definovaný	k2eAgFnPc7d1
hranicemi	hranice	k1gFnPc7
(	(	kIx(
<g/>
v	v	k7c6
rámci	rámec	k1gInSc6
Izraele	Izrael	k1gInSc2
je	být	k5eAaImIp3nS
děleno	dělen	k2eAgNnSc4d1
území	území	k1gNnSc4
na	na	k7c4
sektory	sektor	k1gInPc4
C	C	kA
<g/>
,	,	kIx,
B	B	kA
a	a	k8xC
A	a	k9
s	s	k7c7
převažujícím	převažující	k2eAgNnSc7d1
židovským	židovský	k2eAgNnSc7d1
<g/>
,	,	kIx,
smíšeným	smíšený	k2eAgNnSc7d1
nebo	nebo	k8xC
čistě	čistě	k6eAd1
arabským	arabský	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
však	však	k9
dosud	dosud	k6eAd1
nedosáhl	dosáhnout	k5eNaPmAgMnS
plné	plný	k2eAgFnSc3d1
nezávislosti	nezávislost	k1gFnSc3
a	a	k8xC
nemá	mít	k5eNaImIp3nS
skutečnou	skutečný	k2eAgFnSc4d1
územní	územní	k2eAgFnSc4d1
integritu	integrita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Izraeli	Izrael	k1gInSc6
existuje	existovat	k5eAaImIp3nS
řada	řada	k1gFnSc1
dobrovolnických	dobrovolnický	k2eAgNnPc2d1
hnutí	hnutí	k1gNnPc2
osídlujících	osídlující	k2eAgNnPc2d1
„	„	k?
<g/>
biblické	biblický	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
Izraele	Izrael	k1gInSc2
<g/>
“	“	k?
od	od	k7c2
okamžiku	okamžik	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
Izrael	Izrael	k1gInSc1
zabral	zabrat	k5eAaPmAgMnS
ve	v	k7c6
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
pohledu	pohled	k1gInSc2
mezinárodních	mezinárodní	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
jsou	být	k5eAaImIp3nP
tato	tento	k3xDgNnPc4
města	město	k1gNnPc4
a	a	k8xC
vesnice	vesnice	k1gFnPc4
označovány	označován	k2eAgFnPc4d1
za	za	k7c4
„	„	k?
<g/>
ilegální	ilegální	k2eAgFnPc4d1
osady	osada	k1gFnPc4
<g/>
“	“	k?
a	a	k8xC
některá	některý	k3yIgNnPc4
města	město	k1gNnPc4
a	a	k8xC
vesnice	vesnice	k1gFnPc4
s	s	k7c7
arabským	arabský	k2eAgInSc7d1
<g/>
,	,	kIx,
tzv.	tzv.	kA
palestinským	palestinský	k2eAgMnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
jsou	být	k5eAaImIp3nP
nazývány	nazývat	k5eAaImNgInP
„	„	k?
<g/>
utečenecké	utečenecký	k2eAgInPc4d1
tábory	tábor	k1gInPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
dnešního	dnešní	k2eAgInSc2d1
dne	den	k1gInSc2
zde	zde	k6eAd1
vzniklo	vzniknout	k5eAaPmAgNnS
242	#num#	k4
osad	osada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
srpnu	srpen	k1gInSc6
2005	#num#	k4
byly	být	k5eAaImAgInP
čtyři	čtyři	k4xCgFnPc4
osady	osada	k1gFnPc4
ze	z	k7c2
Západního	západní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
Jordánu	Jordán	k1gInSc2
vyklizeny	vyklizen	k2eAgFnPc4d1
na	na	k7c4
pokyn	pokyn	k1gInSc4
Ariela	Ariel	k1gMnSc2
Šarona	Šaron	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Guvernoráty	Guvernorát	k1gInPc1
</s>
<s>
Předjordánsko	Předjordánsko	k1gNnSc1
tvoří	tvořit	k5eAaImIp3nS
11	#num#	k4
guvernorátů	guvernorát	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Dženín	Dženína	k1gFnPc2
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Túbás	Túbása	k1gFnPc2
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Nábulus	Nábulus	k1gInSc1
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Tulkarm	Tulkarm	k1gInSc1
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Salfit	Salfita	k1gFnPc2
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Kalkílija	Kalkílij	k1gInSc2
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Ramalláh	Ramalláha	k1gFnPc2
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Jericho	Jericho	k1gNnSc1
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Jeruzalém	Jeruzalém	k1gInSc1
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Betlém	Betlém	k1gInSc1
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Hebron	Hebron	k1gInSc1
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Izraelská	izraelský	k2eAgFnSc1d1
osada	osada	k1gFnSc1
Ma	Ma	k1gFnSc2
<g/>
'	'	kIx"
<g/>
ale	ale	k8xC
Adumim	Adumim	k1gInSc1
na	na	k7c6
Západním	západní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Jordánu	Jordán	k1gInSc2
</s>
<s>
Zatčení	zatčení	k1gNnSc1
palestinského	palestinský	k2eAgMnSc2d1
demonstranta	demonstrant	k1gMnSc2
na	na	k7c6
Západním	západní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Jordánu	Jordán	k1gInSc2
</s>
<s>
Mapa	mapa	k1gFnSc1
Západního	západní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
Jordánu	Jordán	k1gInSc2
a	a	k8xC
Pásma	pásmo	k1gNnSc2
Gazy	Gaza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
pod	pod	k7c7
palestinskou	palestinský	k2eAgFnSc7d1
samosprávou	samospráva	k1gFnSc7
jsou	být	k5eAaImIp3nP
tmavě	tmavě	k6eAd1
zelená	zelený	k2eAgNnPc1d1
<g/>
,	,	kIx,
území	území	k1gNnSc1
pod	pod	k7c7
správou	správa	k1gFnSc7
Izraele	Izrael	k1gInSc2
jsou	být	k5eAaImIp3nP
zelenožlutá	zelenožlutý	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Mapa	mapa	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
pod	pod	k7c7
správou	správa	k1gFnSc7
Izraele	Izrael	k1gInSc2
jsou	být	k5eAaImIp3nP
modrá	modré	k1gNnPc1
<g/>
,	,	kIx,
izraelské	izraelský	k2eAgFnPc1d1
osady	osada	k1gFnPc1
jsou	být	k5eAaImIp3nP
vyznačeny	vyznačit	k5eAaPmNgFnP
fialově	fialově	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Mírový	mírový	k2eAgInSc1d1
plán	plán	k1gInSc1
prezidenta	prezident	k1gMnSc2
Trumpa	Trump	k1gMnSc2
z	z	k7c2
ledna	leden	k1gInSc2
2020	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
navrhoval	navrhovat	k5eAaImAgInS
vznik	vznik	k1gInSc4
samostatného	samostatný	k2eAgInSc2d1
Státu	stát	k1gInSc2
Palestina	Palestina	k1gFnSc1
(	(	kIx(
<g/>
vyznačen	vyznačit	k5eAaPmNgInS
modře	modro	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ČEJKA	Čejka	k1gMnSc1
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
moderního	moderní	k2eAgInSc2d1
Izraele	Izrael	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
351	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
2910	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
60	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Judea	Judea	k1gFnSc1
a	a	k8xC
Samaří	Samaří	k1gNnSc1
</s>
<s>
Izraelsko-palestinský	izraelsko-palestinský	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
</s>
<s>
Izraelská	izraelský	k2eAgFnSc1d1
bezpečnostní	bezpečnostní	k2eAgFnSc1d1
bariéra	bariéra	k1gFnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
</s>
<s>
Jordánsko	Jordánsko	k1gNnSc1
Jordánsko	Jordánsko	k1gNnSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
</s>
<s>
Jordánsko	Jordánsko	k1gNnSc1
Jordánsko	Jordánsko	k1gNnSc1
</s>
<s>
Západní	západní	k2eAgInSc1d1
břeh	břeh	k1gInSc1
Jordánu	Jordán	k1gInSc2
</s>
<s>
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
</s>
<s>
Mrtvé	mrtvý	k2eAgNnSc1d1
moře	moře	k1gNnSc1
•	•	k?
Jordánsko	Jordánsko	k1gNnSc1
Jordánsko	Jordánsko	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Města	město	k1gNnSc2
a	a	k8xC
uprchlické	uprchlický	k2eAgInPc4d1
tábory	tábor	k1gInPc4
ve	v	k7c6
Státě	stát	k1gInSc6
Palestina	Palestina	k1gFnSc1
Západní	západní	k2eAgFnSc1d1
břeh	břeh	k1gInSc4
Jordánu	Jordán	k1gInSc2
</s>
<s>
Anabta	Anabta	k1gFnSc1
</s>
<s>
Bajt	bajt	k1gInSc1
Džalá	Džalý	k2eAgFnSc1d1
</s>
<s>
Betlém	Betlém	k1gInSc1
</s>
<s>
al-Bíra	al-Bír	k1gMnSc4
</s>
<s>
az-Záhiríja	az-Záhiríja	k6eAd1
</s>
<s>
Dúrá	Dúrat	k5eAaPmIp3nS,k5eAaBmIp3nS,k5eAaImIp3nS
</s>
<s>
Dženín	Dženín	k1gMnSc1
</s>
<s>
Hebron	Hebron	k1gMnSc1
</s>
<s>
Halhúl	Halhúl	k1gMnSc1
</s>
<s>
Jatta	Jatta	k1gFnSc1
</s>
<s>
Jericho	Jericho	k1gNnSc1
</s>
<s>
Kalkílija	Kalkílija	k6eAd1
</s>
<s>
Náblus	Náblus	k1gMnSc1
</s>
<s>
Rámaláh	Rámaláh	k1gMnSc1
</s>
<s>
Rawábí	Rawábit	k5eAaPmIp3nS
</s>
<s>
Túbás	Túbás	k6eAd1
</s>
<s>
Túlkarim	Túlkarim	k6eAd1
</s>
<s>
Salfít	Salfít	k5eAaPmF
Pásmo	pásmo	k1gNnSc4
Gazy	Gaza	k1gFnSc2
</s>
<s>
Baní	baně	k1gFnSc7
Suhajlá	Suhajlý	k2eAgFnSc1d1
</s>
<s>
Bajt	bajt	k1gInSc1
Hánún	Hánúna	k1gFnPc2
</s>
<s>
Bajt	bajt	k1gInSc1
Lahíja	Lahíj	k1gInSc2
</s>
<s>
Dajr	Dajr	k1gMnSc1
al-Balah	al-Balah	k1gMnSc1
</s>
<s>
Džabálijá	Džabálijat	k5eAaBmIp3nS,k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
Gaza	Gaza	k6eAd1
</s>
<s>
Chán	chán	k1gMnSc1
Júnis	Júnis	k1gFnSc2
</s>
<s>
Rafah	Rafah	k1gMnSc1
</s>
<s>
Judea	Judea	k1gFnSc1
a	a	k8xC
Samaří	Samaří	k1gNnSc1
Města	město	k1gNnSc2
</s>
<s>
Ariel	Ariel	k1gInSc1
</s>
<s>
Bejtar	Bejtar	k1gMnSc1
Ilit	Ilit	k1gMnSc1
</s>
<s>
Ma	Ma	k?
<g/>
'	'	kIx"
<g/>
ale	ale	k8xC
Adumim	Adumim	k1gMnSc1
</s>
<s>
Modi	Modi	k1gNnSc1
<g/>
'	'	kIx"
<g/>
in	in	k?
Ilit	Ilit	k1gInSc4
Místní	místní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
</s>
<s>
Alfej	Alfej	k1gInSc1
Menaše	Menaše	k1gFnSc2
</s>
<s>
Bejt	Bejt	k?
Arje-Ofarim	Arje-Ofarim	k1gInSc1
</s>
<s>
Bejt	Bejt	k?
El	Ela	k1gFnPc2
</s>
<s>
Efrat	Efrat	k1gInSc1
</s>
<s>
Elkana	Elkana	k1gFnSc1
</s>
<s>
Giv	Giv	k?
<g/>
'	'	kIx"
<g/>
at	at	k?
Ze	z	k7c2
<g/>
'	'	kIx"
<g/>
ev	ev	k?
</s>
<s>
Har	Har	k?
Adar	Adar	k1gInSc1
</s>
<s>
Imanuel	Imanuel	k1gMnSc1
</s>
<s>
Karnej	Karnat	k5eAaPmRp2nS,k5eAaImRp2nS
Šomron	Šomron	k1gMnSc1
</s>
<s>
Kedumim	Kedumim	k6eAd1
</s>
<s>
Kirjat	Kirjat	k1gInSc1
Arba	arba	k1gFnSc1
</s>
<s>
Ma	Ma	k?
<g/>
'	'	kIx"
<g/>
ale	ale	k8xC
Efrajim	Efrajim	k1gMnSc1
</s>
<s>
Oranit	Oranit	k1gInSc1
Oblastní	oblastní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
</s>
<s>
Guš	Guš	k?
Ecion	Ecion	k1gInSc1
</s>
<s>
Har	Har	k?
Chevron	Chevron	k1gInSc1
</s>
<s>
Mate	mást	k5eAaImIp3nS
Binjamin	Binjamin	k2eAgMnSc1d1
</s>
<s>
Megilot	Megilot	k1gMnSc1
</s>
<s>
Šomron	Šomron	k1gMnSc1
</s>
<s>
Bik	bika	k1gFnPc2
<g/>
'	'	kIx"
<g/>
at	at	k?
ha-Jarden	ha-Jardna	k1gFnPc2
Další	další	k2eAgInPc1d1
distrikty	distrikt	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
Centrální	centrální	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
Haifský	haifský	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
Jeruzalémský	jeruzalémský	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
Severní	severní	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
Telavivský	telavivský	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
131479	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4065741-3	4065741-3	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85146164	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
3538148574267424430001	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85146164	#num#	k4
</s>
