<s>
Z	z	k7c2
pohledu	pohled	k1gInSc2
mezinárodní	mezinárodní	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
je	být	k5eAaImIp3nS
území	území	k1gNnSc1
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
součást	součást	k1gFnSc4
vznikajícího	vznikající	k2eAgInSc2d1
Státu	stát	k1gInSc2
Palestina	Palestina	k1gFnSc1
s	s	k7c7
ne	ne	k9
jasně	jasně	k6eAd1
definovanými	definovaný	k2eAgFnPc7d1
hranicemi	hranice	k1gFnPc7
(	(	kIx(
<g/>
v	v	k7c6
rámci	rámec	k1gInSc6
Izraele	Izrael	k1gInSc2
je	být	k5eAaImIp3nS
děleno	dělen	k2eAgNnSc4d1
území	území	k1gNnSc4
na	na	k7c4
sektory	sektor	k1gInPc4
C	C	kA
<g/>
,	,	kIx,
B	B	kA
a	a	k8xC
A	a	k9
s	s	k7c7
převažujícím	převažující	k2eAgNnSc7d1
židovským	židovský	k2eAgNnSc7d1
<g/>
,	,	kIx,
smíšeným	smíšený	k2eAgNnSc7d1
nebo	nebo	k8xC
čistě	čistě	k6eAd1
arabským	arabský	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
však	však	k9
dosud	dosud	k6eAd1
nedosáhl	dosáhnout	k5eNaPmAgMnS
plné	plný	k2eAgFnSc3d1
nezávislosti	nezávislost	k1gFnSc3
a	a	k8xC
nemá	mít	k5eNaImIp3nS
skutečnou	skutečný	k2eAgFnSc4d1
územní	územní	k2eAgFnSc4d1
integritu	integrita	k1gFnSc4
<g/>
.	.	kIx.
</s>