<s>
Rod	rod	k1gInSc1
Kinských	Kinský	k1gMnPc2
(	(	kIx(
<g/>
něm.	něm.	k?
Kinsky	Kinsky	k1gMnSc1
von	von	k7c2
Wchinitz	Wchinitz	k1gFnSc2
und	und	k8xC
Tettau	Tettau	k1gInSc2
–	–	k?
Kinští	Kinský	k1gMnPc1
z	z	k7c2
Vchynic	Vchynice	k1gFnPc2
a	a	k8xC
Tetova	Tetov	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
staročeský	staročeský	k2eAgInSc1d1
rytířský	rytířský	k2eAgInSc1d1
a	a	k8xC
později	pozdě	k6eAd2
panský	panský	k2eAgInSc1d1
<g/>
,	,	kIx,
hraběcí	hraběcí	k2eAgInSc1d1
a	a	k8xC
knížecí	knížecí	k2eAgInSc1d1
šlechtický	šlechtický	k2eAgInSc1d1
rod	rod	k1gInSc1
pocházející	pocházející	k2eAgInSc1d1
z	z	k7c2
počátku	počátek	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>