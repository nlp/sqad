<s>
Kinští	Kinštit	k5eAaPmIp3nS
</s>
<s>
Kinští	Kinštit	k5eAaPmIp3nP
(	(	kIx(
<g/>
z	z	k7c2
Vchynic	Vchynice	k1gFnPc2
a	a	k8xC
Tetova	Tetovo	k1gNnSc2
<g/>
)	)	kIx)
Rodový	rodový	k2eAgInSc1d1
erb	erb	k1gInSc1
KinskýchZemě	KinskýchZema	k1gFnSc3
</s>
<s>
České	český	k2eAgNnSc1d1
království	království	k1gNnSc1
České	český	k2eAgNnSc1d1
království	království	k1gNnSc3
Mateřská	mateřský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
</s>
<s>
Kinští	Kinštit	k5eAaPmIp3nP
(	(	kIx(
<g/>
Vchynští	Vchynský	k1gMnPc1
<g/>
)	)	kIx)
Tituly	titul	k1gInPc1
</s>
<s>
hrabata	hrabě	k1gNnPc1
<g/>
,	,	kIx,
říšská	říšský	k2eAgNnPc1d1
hrabata	hrabě	k1gNnPc1
<g/>
,	,	kIx,
knížata	kníže	k1gNnPc1
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Martin	Martin	k1gMnSc1
z	z	k7c2
Medvědíče	Medvědíč	k1gInSc2
Rok	rok	k1gInSc4
založení	založení	k1gNnSc2
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Větve	větev	k1gFnSc2
rodu	rod	k1gInSc2
</s>
<s>
Choceňská	choceňský	k2eAgFnSc1d1
<g/>
,	,	kIx,
perucká	perucký	k2eAgFnSc1d1
<g/>
,	,	kIx,
kostelecká	kostelecký	k2eAgFnSc1d1
<g/>
,	,	kIx,
chlumecká	chlumecký	k2eAgFnSc1d1
větev	větev	k1gFnSc1
(	(	kIx(
<g/>
knížecí	knížecí	k2eAgFnSc2d1
větve	větev	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Medvědičtí	Medvědický	k2eAgMnPc1d1
z	z	k7c2
Medvědíče	Medvědíč	k1gFnSc2
<g/>
,	,	kIx,
Razičtí	Razický	k2eAgMnPc1d1
ze	z	k7c2
Vchynic	Vchynice	k1gFnPc2
<g/>
,	,	kIx,
Žluničtí	Žlunický	k2eAgMnPc1d1
<g/>
,	,	kIx,
Ohničtí	Ohnický	k2eAgMnPc1d1
a	a	k8xC
Kremyžští	Kremyžský	k2eAgMnPc1d1
(	(	kIx(
<g/>
vedlejší	vedlejší	k2eAgFnPc4d1
a	a	k8xC
vymřelé	vymřelý	k2eAgFnPc4d1
větve	větev	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měruničtí	Měrunický	k2eAgMnPc5d1
<g/>
,	,	kIx,
synové	syn	k1gMnPc1
Jana	Jan	k1gMnSc2
Dlaska	Dlask	k1gMnSc2
<g/>
,	,	kIx,
drastská	drastský	k2eAgFnSc1d1
větev	větev	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nizozemská	nizozemský	k2eAgFnSc1d1
větev	větev	k1gFnSc1
</s>
<s>
Rod	rod	k1gInSc1
Kinských	Kinský	k1gMnPc2
(	(	kIx(
<g/>
něm.	něm.	k?
Kinsky	Kinsky	k1gMnSc1
von	von	k7c2
Wchinitz	Wchinitz	k1gFnSc2
und	und	k8xC
Tettau	Tettau	k1gInSc2
–	–	k?
Kinští	Kinský	k1gMnPc1
z	z	k7c2
Vchynic	Vchynice	k1gFnPc2
a	a	k8xC
Tetova	Tetov	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
staročeský	staročeský	k2eAgInSc1d1
rytířský	rytířský	k2eAgInSc1d1
a	a	k8xC
později	pozdě	k6eAd2
panský	panský	k2eAgInSc1d1
<g/>
,	,	kIx,
hraběcí	hraběcí	k2eAgInSc1d1
a	a	k8xC
knížecí	knížecí	k2eAgInSc1d1
šlechtický	šlechtický	k2eAgInSc1d1
rod	rod	k1gInSc1
pocházející	pocházející	k2eAgInSc1d1
z	z	k7c2
počátku	počátek	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
patřili	patřit	k5eAaImAgMnP
k	k	k7c3
přední	přední	k2eAgFnSc3d1
středoevropské	středoevropský	k2eAgFnSc3d1
aristokracii	aristokracie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
žijí	žít	k5eAaImIp3nP
členové	člen	k1gMnPc1
rodu	rod	k1gInSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
Rakousku	Rakousko	k1gNnSc6
a	a	k8xC
dalších	další	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
restituční	restituční	k2eAgInPc1d1
nároky	nárok	k1gInPc1
kostelecké	kostelecký	k2eAgFnSc2d1
a	a	k8xC
chlumecké	chlumecký	k2eAgFnSc2d1
větve	větev	k1gFnSc2
byly	být	k5eAaImAgFnP
po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
uznány	uznán	k2eAgInPc1d1
<g/>
,	,	kIx,
nároky	nárok	k1gInPc1
hlavní	hlavní	k2eAgFnSc2d1
knížecí	knížecí	k2eAgFnSc2d1
větve	větev	k1gFnSc2
Kinských	Kinských	k2eAgInSc1d1
českými	český	k2eAgInPc7d1
soudy	soud	k1gInPc7
uznány	uznán	k2eAgFnPc1d1
nebyly	být	k5eNaImAgFnP
<g/>
.	.	kIx.
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Palác	palác	k1gInSc1
Kinských	Kinský	k1gMnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Barokní	barokní	k2eAgInSc1d1
palác	palác	k1gInSc1
Kinských	Kinských	k2eAgInSc1d1
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
</s>
<s>
Letohrádek	letohrádek	k1gInSc1
Kinských	Kinských	k2eAgInSc1d1
–	–	k?
Musaion	Musaion	k1gInSc1
v	v	k7c6
Kinského	Kinského	k2eAgFnSc6d1
zahradě	zahrada	k1gFnSc6
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Jméno	jméno	k1gNnSc1
Kinský	Kinský	k2eAgMnSc1d1
se	se	k3xPyFc4
vyvinulo	vyvinout	k5eAaPmAgNnS
ze	z	k7c2
středověkého	středověký	k2eAgInSc2d1
německého	německý	k2eAgInSc2d1
přepisu	přepis	k1gInSc2
původního	původní	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
Wchynsky	Wchynsko	k1gNnPc7
<g/>
,	,	kIx,
tj.	tj.	kA
Vchynský	Vchynský	k1gMnSc1
ze	z	k7c2
Vchynic	Vchynice	k1gFnPc2
a	a	k8xC
z	z	k7c2
Tetova	Tetov	k1gInSc2
<g/>
,	,	kIx,
přídomku	přídomek	k1gInSc2
odvozeného	odvozený	k2eAgNnSc2d1
od	od	k7c2
jména	jméno	k1gNnSc2
vesnice	vesnice	k1gFnSc2
a	a	k8xC
tvrze	tvrz	k1gFnSc2
nedaleko	nedaleko	k7c2
Lovosic	Lovosice	k1gInPc2
na	na	k7c6
Litoměřicku	Litoměřicko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Toto	tento	k3xDgNnSc1
jméno	jméno	k1gNnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
začalo	začít	k5eAaPmAgNnS
psát	psát	k5eAaImF
Chynský	Chynský	k2eAgInSc4d1
a	a	k8xC
v	v	k7c6
období	období	k1gNnSc6
třicetileté	třicetiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
užívat	užívat	k5eAaImF
transkripce	transkripce	k1gFnPc4
ve	v	k7c6
tvaru	tvar	k1gInSc6
Kinský	Kinský	k2eAgMnSc1d1
nebo	nebo	k8xC
Kinski	Kinski	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Erb	erb	k1gInSc1
</s>
<s>
Rodový	rodový	k2eAgInSc1d1
erb	erb	k1gInSc1
na	na	k7c6
staroměstském	staroměstský	k2eAgInSc6d1
paláci	palác	k1gInSc6
</s>
<s>
Erb	erb	k1gInSc1
na	na	k7c6
špýcharu	špýchar	k1gInSc6
v	v	k7c6
Želevčicích	Želevčice	k1gFnPc6
u	u	k7c2
Slaného	Slaný	k1gInSc2
</s>
<s>
Erb	erb	k1gInSc1
této	tento	k3xDgFnSc2
rodiny	rodina	k1gFnSc2
obsahuje	obsahovat	k5eAaImIp3nS
tři	tři	k4xCgInPc4
stříbrné	stříbrný	k2eAgInPc4d1
vlčí	vlčí	k2eAgInPc4d1
zuby	zub	k1gInPc4
na	na	k7c6
červeném	červený	k2eAgInSc6d1
štítě	štít	k1gInSc6
<g/>
,	,	kIx,
jdoucí	jdoucí	k2eAgFnPc1d1
zprava	zprava	k6eAd1
doleva	doleva	k6eAd1
(	(	kIx(
<g/>
heraldicky	heraldicky	k6eAd1
ovšem	ovšem	k9
zleva	zleva	k6eAd1
doprava	doprava	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
štít	štít	k1gInSc1
rozdělen	rozdělit	k5eAaPmNgInS
těmito	tento	k3xDgInPc7
zuby	zub	k1gInPc7
na	na	k7c4
poloviny	polovina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
i	i	k9
vyobrazení	vyobrazený	k2eAgMnPc1d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
tři	tři	k4xCgInPc4
žluté	žlutý	k2eAgInPc4d1
vlčí	vlčí	k2eAgInPc4d1
zuby	zub	k1gInPc4
na	na	k7c6
černém	černé	k1gNnSc6
pozadí	pozadí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
historii	historie	k1gFnSc6
se	se	k3xPyFc4
lze	lze	k6eAd1
setkat	setkat	k5eAaPmF
i	i	k9
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
tyto	tento	k3xDgInPc1
vlčí	vlčí	k2eAgInPc1d1
zuby	zub	k1gInPc1
jdou	jít	k5eAaImIp3nP
zezdola	zezdola	k6eAd1
nahoru	nahoru	k6eAd1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
u	u	k7c2
Radslava	Radslav	k1gMnSc2
Kinského	Kinský	k1gMnSc2
(	(	kIx(
<g/>
na	na	k7c6
konci	konec	k1gInSc6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
pravý	pravý	k2eAgInSc1d1
vrchol	vrchol	k1gInSc1
dotýká	dotýkat	k5eAaImIp3nS
horního	horní	k2eAgInSc2d1
okraje	okraj	k1gInSc2
štítu	štít	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Právě	právě	k6eAd1
podobnost	podobnost	k1gFnSc4
jejich	jejich	k3xOp3gInSc2
erbu	erb	k1gInSc2
s	s	k7c7
erbem	erb	k1gInSc7
Tetauerů	Tetauer	k1gMnPc2
z	z	k7c2
Tetova	Tetovo	k1gNnSc2
vedla	vést	k5eAaImAgFnS
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
povýšení	povýšení	k1gNnSc3
do	do	k7c2
panského	panský	k2eAgInSc2d1
stavu	stav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Erb	erb	k1gInSc1
na	na	k7c6
budově	budova	k1gFnSc6
pražského	pražský	k2eAgInSc2d1
Paláce	palác	k1gInSc2
Kinských	Kinský	k1gMnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
neděleném	dělený	k2eNgNnSc6d1
poli	pole	k1gNnSc6
a	a	k8xC
jeho	jeho	k3xOp3gFnSc7
součástí	součást	k1gFnSc7
je	být	k5eAaImIp3nS
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
Bůh	bůh	k1gMnSc1
<g/>
,	,	kIx,
čest	čest	k1gFnSc1
<g/>
,	,	kIx,
vlast	vlast	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
nemá	mít	k5eNaImIp3nS
žádného	žádný	k3yNgMnSc4
štítonoše	štítonoš	k1gMnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
doplněn	doplnit	k5eAaPmNgInS
knížecím	knížecí	k2eAgInSc7d1
pláštěm	plášť	k1gInSc7
a	a	k8xC
mimo	mimo	k7c4
jiné	jiný	k2eAgFnPc4d1
kolanou	kolaná	k1gFnSc4
Řádu	řád	k1gInSc2
zlatého	zlatý	k2eAgNnSc2d1
rouna	rouno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Blason	Blason	k1gNnSc1
<g/>
:	:	kIx,
V	v	k7c6
červeném	červený	k2eAgNnSc6d1
poli	pole	k1gNnSc6
tři	tři	k4xCgFnPc1
stříbrné	stříbrná	k1gFnPc1
kančí	kančí	k2eAgFnPc1d1
zuby	zub	k1gInPc4
<g/>
,	,	kIx,
rostoucí	rostoucí	k2eAgInSc4d1
z	z	k7c2
levé	levý	k2eAgFnSc2d1
strany	strana	k1gFnSc2
štítu	štít	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přikryvadla	Přikryvadlo	k1gNnPc4
červeno-stříbrná	červeno-stříbrný	k2eAgNnPc4d1
<g/>
,	,	kIx,
v	v	k7c6
klenotu	klenot	k1gInSc6
červené	červený	k2eAgNnSc1d1
a	a	k8xC
stříbrné	stříbrný	k2eAgNnSc1d1
křídlo	křídlo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znak	znak	k1gInSc1
bývá	bývat	k5eAaImIp3nS
doplňován	doplňovat	k5eAaImNgInS
hraběcí	hraběcí	k2eAgFnSc7d1
korunou	koruna	k1gFnSc7
<g/>
,	,	kIx,
eventuálně	eventuálně	k6eAd1
knížecí	knížecí	k2eAgFnSc7d1
korunou	koruna	k1gFnSc7
či	či	k8xC
knížecím	knížecí	k2eAgInSc7d1
pláštěm	plášť	k1gInSc7
(	(	kIx(
<g/>
pouze	pouze	k6eAd1
pro	pro	k7c4
hlavu	hlava	k1gFnSc4
rodu	rod	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
rodu	rod	k1gInSc2
</s>
<s>
Historie	historie	k1gFnSc1
rodu	rod	k1gInSc2
Kinských	Kinských	k2eAgInSc2d1
je	být	k5eAaImIp3nS
dlouhá	dlouhý	k2eAgFnSc1d1
a	a	k8xC
jelikož	jelikož	k8xS
se	se	k3xPyFc4
zpočátku	zpočátku	k6eAd1
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
nevýznamný	významný	k2eNgInSc4d1
vladycký	vladycký	k2eAgInSc4d1
rod	rod	k1gInSc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
jeho	jeho	k3xOp3gInPc1
počátky	počátek	k1gInPc1
plné	plný	k2eAgInPc1d1
nejasností	nejasnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
spíše	spíše	k9
o	o	k7c4
významnější	významný	k2eAgInPc4d2
sedláky	sedlák	k1gInPc4
<g/>
,	,	kIx,
než	než	k8xS
o	o	k7c4
významný	významný	k2eAgInSc4d1
šlechtický	šlechtický	k2eAgInSc4d1
rod	rod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výraznějšího	výrazný	k2eAgInSc2d2
mocenského	mocenský	k2eAgInSc2d1
i	i	k8xC
finančního	finanční	k2eAgInSc2d1
vzestupu	vzestup	k1gInSc2
se	se	k3xPyFc4
tento	tento	k3xDgInSc4
rod	rod	k1gInSc4
dočkal	dočkat	k5eAaPmAgMnS
až	až	k9
po	po	k7c6
třicetileté	třicetiletý	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
odkdy	odkdy	k6eAd1
je	být	k5eAaImIp3nS
historie	historie	k1gFnSc1
dobře	dobře	k6eAd1
známa	znám	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
Medvědíče	Medvědíč	k1gInSc2
</s>
<s>
Počátky	počátek	k1gInPc4
historie	historie	k1gFnSc2
roku	rok	k1gInSc2
Kinských	Kinské	k1gNnPc2
jsou	být	k5eAaImIp3nP
nejasné	jasný	k2eNgInPc1d1
<g/>
,	,	kIx,
bývají	bývat	k5eAaImIp3nP
do	do	k7c2
ní	on	k3xPp3gFnSc2
zahrnováni	zahrnovat	k5eAaImNgMnP
i	i	k9
lidé	člověk	k1gMnPc1
s	s	k7c7
přídomkem	přídomek	k1gInSc7
„	„	k?
<g/>
z	z	k7c2
Medvědíče	Medvědíč	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnSc1
spojitost	spojitost	k1gFnSc1
s	s	k7c7
Kinskými	Kinský	k1gMnPc7
je	být	k5eAaImIp3nS
však	však	k9
neprokázaná	prokázaný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
předky	předek	k1gInPc4
tohoto	tento	k3xDgInSc2
rodu	rod	k1gInSc2
se	se	k3xPyFc4
usuzuje	usuzovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
z	z	k7c2
nepřímých	přímý	k2eNgFnPc2d1
indicií	indicie	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
držba	držba	k1gFnSc1
Medvědic	medvědice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
bývá	bývat	k5eAaImIp3nS
za	za	k7c2
prvního	první	k4xOgMnSc2
předka	předek	k1gMnSc2
Kinských	Kinských	k2eAgMnSc1d1
označován	označován	k2eAgMnSc1d1
Martin	Martin	k1gMnSc1
z	z	k7c2
Medvědíče	Medvědíč	k1gFnSc2
(	(	kIx(
<g/>
†	†	k?
asi	asi	k9
1209	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
něm	on	k3xPp3gInSc6
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
několik	několik	k4yIc1
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
s	s	k7c7
Martinem	Martin	k1gInSc7
z	z	k7c2
Medvědíče	Medvědíč	k1gFnSc2
nějak	nějak	k6eAd1
příbuzné	příbuzný	k2eAgInPc1d1
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
nelze	lze	k6eNd1
najít	najít	k5eAaPmF
přímou	přímý	k2eAgFnSc4d1
linii	linie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
bezvýznamný	bezvýznamný	k2eAgInSc4d1
rod	rod	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
neměl	mít	k5eNaImAgInS
žádné	žádný	k3yNgFnPc4
ambice	ambice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Vchynští	Vchynský	k1gMnPc1
do	do	k7c2
Jana	Jan	k1gMnSc2
Dlaska	Dlask	k1gMnSc2
</s>
<s>
Prvním	první	k4xOgInSc7
nezpochybnitelným	zpochybnitelný	k2eNgInSc7d1
předkem	předek	k1gInSc7
tohoto	tento	k3xDgInSc2
rodu	rod	k1gInSc2
je	být	k5eAaImIp3nS
Bohuslav	Bohuslav	k1gMnSc1
z	z	k7c2
Žernosek	Žernosek	k?
(	(	kIx(
<g/>
†	†	k?
1282	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
čtyři	čtyři	k4xCgMnPc4
syny	syn	k1gMnPc4
(	(	kIx(
<g/>
Bohuslav	Bohuslav	k1gMnSc1
<g/>
,	,	kIx,
Protivec	protivec	k1gMnSc1
<g/>
,	,	kIx,
Víremil	Víremil	k1gMnSc1
<g/>
,	,	kIx,
Zdeslav	Zdeslav	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
jako	jako	k9
první	první	k4xOgMnPc1
používali	používat	k5eAaImAgMnP
přídomek	přídomek	k1gInSc4
„	„	k?
<g/>
ze	z	k7c2
Vchynic	Vchynice	k1gFnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
ten	ten	k3xDgInSc4
převzali	převzít	k5eAaPmAgMnP
od	od	k7c2
vesnice	vesnice	k1gFnSc2
a	a	k8xC
tvrze	tvrz	k1gFnSc2
nedaleko	nedaleko	k7c2
Lovosic	Lovosice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
zhruba	zhruba	k6eAd1
roku	rok	k1gInSc2
1350	#num#	k4
lze	lze	k6eAd1
rod	rod	k1gInSc4
považovat	považovat	k5eAaImF
za	za	k7c4
jednotný	jednotný	k2eAgInSc4d1
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
z	z	k7c2
oddělení	oddělení	k1gNnSc2
spravování	spravování	k1gNnSc2
majetku	majetek	k1gInSc2
mezi	mezi	k7c4
jednotlivé	jednotlivý	k2eAgMnPc4d1
příbuzné	příbuzný	k1gMnPc4
vyplývalo	vyplývat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
rozštěpení	rozštěpení	k1gNnSc3
tohoto	tento	k3xDgInSc2
rodu	rod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1350	#num#	k4
tak	tak	k6eAd1
vzniklo	vzniknout	k5eAaPmAgNnS
větší	veliký	k2eAgNnSc1d2
množství	množství	k1gNnSc1
větví	větev	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
některé	některý	k3yIgInPc4
nelze	lze	k6eNd1
sledovat	sledovat	k5eAaImF
dále	daleko	k6eAd2
než	než	k8xS
jednu	jeden	k4xCgFnSc4
nebo	nebo	k8xC
dvě	dva	k4xCgFnPc4
generace	generace	k1gFnPc4
a	a	k8xC
ztratily	ztratit	k5eAaPmAgInP
s	s	k7c7
rodem	rod	k1gInSc7
kontakt	kontakt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rod	rod	k1gInSc1
se	se	k3xPyFc4
nerozpadl	rozpadnout	k5eNaPmAgInS
podle	podle	k7c2
významnosti	významnost	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
v	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
byli	být	k5eAaImAgMnP
pouze	pouze	k6eAd1
vladykové	vladyka	k1gMnPc1
<g/>
,	,	kIx,
bez	bez	k7c2
většího	veliký	k2eAgInSc2d2
významu	význam	k1gInSc2
<g/>
;	;	kIx,
za	za	k7c4
hlavní	hlavní	k2eAgNnSc4d1
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
větev	větev	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
držela	držet	k5eAaImAgFnS
Vchynice	Vchynice	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
z	z	k7c2
toho	ten	k3xDgInSc2
prostého	prostý	k2eAgInSc2d1
důvodu	důvod	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
nevymřeli	vymřít	k5eNaPmAgMnP
a	a	k8xC
dali	dát	k5eAaPmAgMnP
základ	základ	k1gInSc1
pozdějšímu	pozdní	k2eAgInSc3d2
významu	význam	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Tvrz	tvrz	k1gFnSc1
Vchynice	Vchynice	k1gFnSc1
a	a	k8xC
hrad	hrad	k1gInSc1
Oparen	Oparna	k1gFnPc2
–	–	k?
Opárno	Opárno	k1gNnSc1
podržel	podržet	k5eAaPmAgInS
v	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
Smil	smil	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smil	smil	k1gInSc1
(	(	kIx(
<g/>
†	†	k?
před	před	k7c7
rokem	rok	k1gInSc7
1417	#num#	k4
<g/>
)	)	kIx)
měl	mít	k5eAaImAgMnS
šest	šest	k4xCc4
synů	syn	k1gMnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnPc1
jména	jméno	k1gNnPc1
jsou	být	k5eAaImIp3nP
sice	sice	k8xC
známa	znám	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
jelikož	jelikož	k8xS
např.	např.	kA
jméno	jméno	k1gNnSc1
Jan	Jan	k1gMnSc1
se	se	k3xPyFc4
v	v	k7c6
rodě	rod	k1gInSc6
vyskytovalo	vyskytovat	k5eAaImAgNnS
často	často	k6eAd1
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
zcela	zcela	k6eAd1
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
některé	některý	k3yIgFnPc4
z	z	k7c2
těchto	tento	k3xDgFnPc2
dětí	dítě	k1gFnPc2
není	být	k5eNaImIp3nS
ztotožňováno	ztotožňovat	k5eAaImNgNnS
s	s	k7c7
někým	někdo	k3yInSc7
jiným	jiný	k2eAgNnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
bratři	bratr	k1gMnPc1
však	však	k9
nepochybně	pochybně	k6eNd1
drželi	držet	k5eAaImAgMnP
Měrunice	Měrunice	k1gFnSc1
<g/>
,	,	kIx,
Siřejovice	Siřejovice	k1gFnSc1
a	a	k8xC
Rochov	Rochov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smil	smil	k1gInSc4
mladší	mladý	k2eAgFnSc2d2
držel	držet	k5eAaImAgMnS
Opárno	Opárno	k1gNnSc4
a	a	k8xC
Vchynice	Vchynice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
následuje	následovat	k5eAaImIp3nS
období	období	k1gNnSc4
řady	řada	k1gFnSc2
nejasností	nejasnost	k1gFnPc2
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yIgInPc2,k3yQgInPc2,k3yRgInPc2
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
hrad	hrad	k1gInSc1
Opárno	Opárno	k1gNnSc4
byl	být	k5eAaImAgInS
rozdělen	rozdělit	k5eAaPmNgInS
na	na	k7c4
dvě	dva	k4xCgFnPc4
části	část	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
patrně	patrně	k6eAd1
drželi	držet	k5eAaImAgMnP
potomci	potomek	k1gMnPc1
Smila	Smilo	k1gNnSc2
mladšího	mladý	k2eAgNnSc2d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potomci	potomek	k1gMnPc1
pravděpodobně	pravděpodobně	k6eAd1
Martina	Martin	k1gMnSc2
(	(	kIx(
<g/>
syn	syn	k1gMnSc1
Smila	Smil	k1gMnSc2
staršího	starší	k1gMnSc2
<g/>
)	)	kIx)
vytvořili	vytvořit	k5eAaPmAgMnP
Měrunickou	Měrunický	k2eAgFnSc4d1
větev	větev	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Počátkem	počátkem	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
držel	držet	k5eAaImAgMnS
Vchynice	Vchynice	k1gFnPc4
i	i	k9
Opárno	Opárno	k1gNnSc1
Jan	Jan	k1gMnSc1
Dlask	Dlask	k1gMnSc1
spolu	spolu	k6eAd1
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
bratrem	bratr	k1gMnSc7
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
jméno	jméno	k1gNnSc1
není	být	k5eNaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdá	zdát	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
být	být	k5eAaImF
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
dnešní	dnešní	k2eAgMnPc1d1
Kinští	Kinský	k1gMnPc1
jsou	být	k5eAaImIp3nP
potomky	potomek	k1gMnPc7
Jana	Jan	k1gMnSc2
Dlaska	Dlask	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Synové	syn	k1gMnPc1
jeho	jeho	k3xOp3gMnPc1
bratra	bratr	k1gMnSc4
prodali	prodat	k5eAaPmAgMnP
roku	rok	k1gInSc2
1543	#num#	k4
Vchynice	Vchynice	k1gFnSc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
vlastnili	vlastnit	k5eAaImAgMnP
statek	statek	k1gInSc4
v	v	k7c6
Tuchořicích	Tuchořik	k1gInPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
poměrně	poměrně	k6eAd1
brzy	brzy	k6eAd1
vymřeli	vymřít	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Jan	Jan	k1gMnSc1
Dlask	Dlask	k1gMnSc1
měl	mít	k5eAaImAgMnS
tři	tři	k4xCgMnPc4
syny	syn	k1gMnPc4
–	–	k?
Jiříka	Jiřík	k1gMnSc2
<g/>
,	,	kIx,
Václava	Václav	k1gMnSc2
a	a	k8xC
Kryštofa	Kryštof	k1gMnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
založili	založit	k5eAaPmAgMnP
tři	tři	k4xCgFnPc4
nové	nový	k2eAgFnPc4d1
větve	větev	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
tvrdit	tvrdit	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
od	od	k7c2
tohoto	tento	k3xDgInSc2
okamžiku	okamžik	k1gInSc2
je	být	k5eAaImIp3nS
historie	historie	k1gFnSc1
rodu	rod	k1gInSc2
Kinských	Kinských	k2eAgFnSc1d1
relativně	relativně	k6eAd1
přehledná	přehledný	k2eAgFnSc1d1
a	a	k8xC
čitelná	čitelný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
z	z	k7c2
jeho	jeho	k3xOp3gMnPc2
synů	syn	k1gMnPc2
založil	založit	k5eAaPmAgInS
vlastní	vlastní	k2eAgFnSc4d1
větev	větev	k1gFnSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
větev	větev	k1gFnSc1
založená	založený	k2eAgFnSc1d1
Jiříkem	Jiřík	k1gMnSc7
vymřela	vymřít	k5eAaPmAgFnS
koncem	koncem	k7c2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
a	a	k8xC
větev	větev	k1gFnSc1
založená	založený	k2eAgFnSc1d1
Kryštofem	Kryštof	k1gMnSc7
odešla	odejít	k5eAaPmAgFnS
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
ztratila	ztratit	k5eAaPmAgFnS
s	s	k7c7
Kinskými	Kinský	k1gMnPc7
kontakt	kontakt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedinou	jediný	k2eAgFnSc4d1
pokračující	pokračující	k2eAgFnSc4d1
větev	větev	k1gFnSc4
tak	tak	k6eAd1
založil	založit	k5eAaPmAgMnS
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Vzestup	vzestup	k1gInSc1
rodu	rod	k1gInSc2
</s>
<s>
Františka	Františka	k1gFnSc1
Kinská	Kinská	k1gFnSc1
<g/>
,	,	kIx,
hraběnka	hraběnka	k1gFnSc1
z	z	k7c2
Vchynic	Vchynice	k1gFnPc2
a	a	k8xC
Tetova	Tetův	k2eAgFnSc1d1
<g/>
,	,	kIx,
lichtenštejnská	lichtenštejnský	k2eAgFnSc1d1
kněžna	kněžna	k1gFnSc1
v	v	k7c6
letech	let	k1gInPc6
1836	#num#	k4
<g/>
–	–	k?
<g/>
1857	#num#	k4
</s>
<s>
Marie	Marie	k1gFnSc1
Aglaë	Aglaë	k1gFnSc1
<g/>
,	,	kIx,
kněžna	kněžna	k1gFnSc1
lichtenštejnská	lichtenštejnský	k2eAgFnSc1d1
<g/>
,	,	kIx,
rozená	rozený	k2eAgFnSc1d1
Kinská	Kinská	k1gFnSc1
z	z	k7c2
Vchynic	Vchynice	k1gFnPc2
a	a	k8xC
Tetova	Tetov	k1gInSc2
</s>
<s>
Dlaskův	Dlaskův	k2eAgMnSc1d1
prostřední	prostřední	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Václav	Václav	k1gMnSc1
I.	I.	kA
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Annou	Anna	k1gFnSc7
z	z	k7c2
Vřesovic	Vřesovice	k1gFnPc2
a	a	k8xC
držel	držet	k5eAaImAgInS
Černoc	Černoc	k1gInSc1
<g/>
,	,	kIx,
Keblany	Keblan	k1gInPc1
a	a	k8xC
Petrovice	Petrovice	k1gFnPc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
nebyl	být	k5eNaImAgInS
nikterak	nikterak	k6eAd1
významný	významný	k2eAgInSc4d1
majetek	majetek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Václav	Václav	k1gMnSc1
byl	být	k5eAaImAgMnS
zavražděn	zavraždit	k5eAaPmNgMnS
roku	rok	k1gInSc2
1542	#num#	k4
a	a	k8xC
teprve	teprve	k6eAd1
roku	rok	k1gInSc2
1554	#num#	k4
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
správy	správa	k1gFnSc2
rodového	rodový	k2eAgInSc2d1
majetku	majetek	k1gInSc2
ujmout	ujmout	k5eAaPmF
Radslav	Radslava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radslav	Radslava	k1gFnPc2
(	(	kIx(
<g/>
†	†	k?
1619	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
velmi	velmi	k6eAd1
úspěšný	úspěšný	k2eAgInSc1d1
a	a	k8xC
velmi	velmi	k6eAd1
brzy	brzy	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
povedlo	povést	k5eAaPmAgNnS
rozšířit	rozšířit	k5eAaPmF
rodový	rodový	k2eAgInSc4d1
majetek	majetek	k1gInSc4
o	o	k7c4
Benešov	Benešov	k1gInSc4
nad	nad	k7c7
Ploučnicí	Ploučnice	k1gFnSc7
<g/>
,	,	kIx,
Novou	nový	k2eAgFnSc4d1
Bystřici	Bystřice	k1gFnSc4
<g/>
,	,	kIx,
Kamenici	Kamenice	k1gFnSc6
<g/>
,	,	kIx,
Malhostice	Malhostika	k1gFnSc6
<g/>
,	,	kIx,
Teplice	Teplice	k1gFnPc1
<g/>
,	,	kIx,
Tolštejn	Tolštejn	k1gNnSc1
a	a	k8xC
Zahořany	Zahořan	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1611	#num#	k4
<g/>
–	–	k?
<g/>
1618	#num#	k4
byl	být	k5eAaImAgInS
dvorním	dvorní	k2eAgMnSc7d1
hofmistrem	hofmistr	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
poručníkem	poručník	k1gMnSc7
Tetourovských	Tetourovský	k2eAgMnPc2d1
sirotků	sirotek	k1gMnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
všiml	všimnout	k5eAaPmAgMnS
jisté	jistý	k2eAgFnPc4d1
podoby	podoba	k1gFnPc4
mezi	mezi	k7c7
jejich	jejich	k3xOp3gInPc7
erby	erb	k1gInPc7
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
čehož	což	k3yQnSc2,k3yRnSc2
začal	začít	k5eAaPmAgMnS
požadovat	požadovat	k5eAaImF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
i	i	k9
jemu	on	k3xPp3gNnSc3
byl	být	k5eAaImAgInS
přidělen	přidělit	k5eAaPmNgInS
panský	panský	k2eAgInSc1d1
titul	titul	k1gInSc1
<g/>
,	,	kIx,
pro	pro	k7c4
což	což	k3yQnSc4,k3yRnSc4
i	i	k9
zfalšoval	zfalšovat	k5eAaPmAgMnS
některé	některý	k3yIgFnPc4
listiny	listina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
listiny	listina	k1gFnPc1
byly	být	k5eAaImAgFnP
českou	český	k2eAgFnSc7d1
kanceláří	kancelář	k1gFnSc7
potvrzeny	potvrdit	k5eAaPmNgInP
v	v	k7c6
roce	rok	k1gInSc6
1596	#num#	k4
a	a	k8xC
roku	rok	k1gInSc2
1611	#num#	k4
byl	být	k5eAaImAgInS
povýšen	povýšit	k5eAaPmNgMnS
do	do	k7c2
panského	panský	k2eAgInSc2d1
stavu	stav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
bezdětný	bezdětný	k2eAgMnSc1d1
<g/>
,	,	kIx,
proto	proto	k8xC
majetek	majetek	k1gInSc1
odkázal	odkázat	k5eAaPmAgInS
synovcům	synovec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
Jan	Jan	k1gMnSc1
byl	být	k5eAaImAgMnS
méně	málo	k6eAd2
úspěšný	úspěšný	k2eAgMnSc1d1
<g/>
,	,	kIx,
přesto	přesto	k8xC
držel	držet	k5eAaImAgMnS
Chřenice	Chřenice	k1gFnPc4
<g/>
,	,	kIx,
Jeneč	Jeneč	k1gInSc1
<g/>
,	,	kIx,
Kunratice	Kunratice	k1gFnPc1
<g/>
,	,	kIx,
Vlachovo	Vlachův	k2eAgNnSc1d1
Březí	březí	k1gNnSc1
a	a	k8xC
Zásmuky	Zásmuky	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1576	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
purkrabím	purkrabí	k1gMnSc7
na	na	k7c6
Karlštejně	Karlštejn	k1gInSc6
a	a	k8xC
v	v	k7c6
této	tento	k3xDgFnSc6
funkci	funkce	k1gFnSc6
roku	rok	k1gInSc2
1586	#num#	k4
urazil	urazit	k5eAaPmAgInS
císaře	císař	k1gMnSc4
Rudolfa	Rudolf	k1gMnSc4
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
za	za	k7c4
což	což	k3yQnSc4,k3yRnSc4
byl	být	k5eAaImAgInS
obžalován	obžalován	k2eAgInSc1d1
za	za	k7c4
faleš	faleš	k1gFnSc4
a	a	k8xC
od	od	k7c2
potrestání	potrestání	k1gNnSc2
ho	on	k3xPp3gMnSc2
zachránila	zachránit	k5eAaPmAgFnS
jen	jen	k9
jeho	jeho	k3xOp3gFnSc1
smrt	smrt	k1gFnSc1
roku	rok	k1gInSc2
1590	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
měl	mít	k5eAaImAgMnS
šest	šest	k4xCc4
dcer	dcera	k1gFnPc2
a	a	k8xC
šest	šest	k4xCc1
synů	syn	k1gMnPc2
<g/>
:	:	kIx,
Rudolf	Rudolf	k1gMnSc1
byl	být	k5eAaImAgMnS
zabit	zabít	k5eAaPmNgMnS
roku	rok	k1gInSc2
1597	#num#	k4
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
padl	padnout	k5eAaPmAgMnS,k5eAaImAgMnS
roku	rok	k1gInSc2
1599	#num#	k4
<g/>
,	,	kIx,
Radslav	Radslav	k1gMnSc1
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
direktorem	direktor	k1gMnSc7
(	(	kIx(
<g/>
1618	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
po	po	k7c6
bitvě	bitva	k1gFnSc6
na	na	k7c6
Bílé	bílý	k2eAgFnSc6d1
hoře	hora	k1gFnSc6
byl	být	k5eAaImAgInS
odsouzen	odsoudit	k5eAaPmNgInS,k5eAaImNgInS
k	k	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
uprchnout	uprchnout	k5eAaPmF
do	do	k7c2
Nizozemí	Nizozemí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k9
Oldřich	Oldřich	k1gMnSc1
se	se	k3xPyFc4
aktivně	aktivně	k6eAd1
zapojil	zapojit	k5eAaPmAgMnS
do	do	k7c2
stavovského	stavovský	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
<g/>
,	,	kIx,
při	při	k7c6
defenestraci	defenestrace	k1gFnSc6
vyhazoval	vyhazovat	k5eAaImAgMnS
Martinice	Martinice	k1gFnPc4
z	z	k7c2
okna	okno	k1gNnSc2
Pražského	pražský	k2eAgInSc2d1
hradu	hrad	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
zemřel	zemřít	k5eAaPmAgMnS
bezdětný	bezdětný	k2eAgMnSc1d1
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
majetek	majetek	k1gInSc1
byl	být	k5eAaImAgInS
zabrán	zabrat	k5eAaPmNgInS
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
přidělen	přidělit	k5eAaPmNgInS
Vilémovi	Vilém	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vilém	Viléma	k1gFnPc2
v	v	k7c6
průběhu	průběh	k1gInSc6
povstání	povstání	k1gNnSc2
vystupoval	vystupovat	k5eAaImAgInS
nejasně	jasně	k6eNd1
<g/>
,	,	kIx,
připadl	připadnout	k5eAaPmAgMnS
mu	on	k3xPp3gMnSc3
Oldřichův	Oldřichův	k2eAgInSc4d1
majetek	majetek	k1gInSc4
a	a	k8xC
roku	rok	k1gInSc2
1628	#num#	k4
získal	získat	k5eAaPmAgInS
hraběcí	hraběcí	k2eAgInSc1d1
titul	titul	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
zavražděn	zavraždit	k5eAaPmNgMnS
v	v	k7c6
Chebu	Cheb	k1gInSc6
roku	rok	k1gInSc2
1634	#num#	k4
spolu	spolu	k6eAd1
s	s	k7c7
Albrechtem	Albrecht	k1gMnSc7
z	z	k7c2
Valdštejna	Valdštejn	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
když	když	k8xS
se	se	k3xPyFc4
zapletl	zaplést	k5eAaPmAgMnS
do	do	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
spiknutí	spiknutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgInS
sice	sice	k8xC
dva	dva	k4xCgMnPc4
syny	syn	k1gMnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
tato	tento	k3xDgFnSc1
větev	větev	k1gFnSc1
vymřela	vymřít	k5eAaPmAgFnS
v	v	k7c6
další	další	k2eAgFnSc6d1
generaci	generace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1
pokračující	pokračující	k2eAgFnSc1d1
větev	větev	k1gFnSc1
tak	tak	k6eAd1
byla	být	k5eAaImAgFnS
od	od	k7c2
Janova	Janov	k1gInSc2
syna	syn	k1gMnSc2
Václava	Václav	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
působil	působit	k5eAaImAgMnS
jako	jako	k9
komorník	komorník	k1gMnSc1
pozdějšího	pozdní	k2eAgMnSc2d2
krále	král	k1gMnSc2
a	a	k8xC
císaře	císař	k1gMnSc2
Matyáše	Matyáš	k1gMnSc2
a	a	k8xC
roku	rok	k1gInSc2
1611	#num#	k4
byl	být	k5eAaImAgInS
přijat	přijmout	k5eAaPmNgInS
do	do	k7c2
panského	panský	k2eAgInSc2d1
stavu	stav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
krále	král	k1gMnSc2
získal	získat	k5eAaPmAgMnS
majestátem	majestát	k1gInSc7
Chlumec	Chlumec	k1gInSc1
a	a	k8xC
Kolín	Kolín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1615	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vyšla	vyjít	k5eAaPmAgFnS
najevo	najevo	k6eAd1
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
jeho	jeho	k3xOp3gInPc2
podvodů	podvod	k1gInPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
odsouzen	odsouzet	k5eAaImNgMnS,k5eAaPmNgMnS
k	k	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
mu	on	k3xPp3gMnSc3
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
císař	císař	k1gMnSc1
změnil	změnit	k5eAaPmAgMnS
na	na	k7c4
doživotí	doživotí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Václav	Václav	k1gMnSc1
pak	pak	k6eAd1
uprchl	uprchnout	k5eAaPmAgMnS
do	do	k7c2
Krakova	Krakov	k1gInSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
se	se	k3xPyFc4
však	však	k9
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1618	#num#	k4
vrátil	vrátit	k5eAaPmAgInS
a	a	k8xC
podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gNnSc3
získat	získat	k5eAaPmF
zpět	zpět	k6eAd1
Chlumec	Chlumec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1620	#num#	k4
proti	proti	k7c3
němu	on	k3xPp3gNnSc3
vypuklo	vypuknout	k5eAaPmAgNnS
povstání	povstání	k1gNnSc1
poddaných	poddaný	k1gMnPc2
a	a	k8xC
byl	být	k5eAaImAgMnS
opět	opět	k6eAd1
vězněn	věznit	k5eAaImNgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
bitvě	bitva	k1gFnSc6
na	na	k7c6
Bílé	bílý	k2eAgFnSc6d1
hoře	hora	k1gFnSc6
dostal	dostat	k5eAaPmAgMnS
milost	milost	k1gFnSc4
a	a	k8xC
byl	být	k5eAaImAgInS
mu	on	k3xPp3gMnSc3
navrácen	navrátit	k5eAaPmNgInS
Chlumec	Chlumec	k1gInSc1
a	a	k8xC
za	za	k7c4
Kolín	Kolín	k1gInSc4
obdržel	obdržet	k5eAaPmAgMnS
nějaké	nějaký	k3yIgInPc4
statky	statek	k1gInPc4
v	v	k7c6
okolí	okolí	k1gNnSc6
Chlumce	Chlumec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Jan	Jan	k1gMnSc1
Oktavián	Oktavián	k1gMnSc1
zdědil	zdědit	k5eAaPmAgInS
veškerý	veškerý	k3xTgInSc4
majetek	majetek	k1gInSc4
po	po	k7c6
otci	otec	k1gMnSc6
<g/>
,	,	kIx,
dále	daleko	k6eAd2
pak	pak	k6eAd1
po	po	k7c6
Radslavovi	Radslav	k1gMnSc6
Zásmuky	Zásmuky	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
prodal	prodat	k5eAaPmAgInS
<g/>
,	,	kIx,
a	a	k8xC
po	po	k7c6
Vilémovi	Vilém	k1gMnSc6
zdědil	zdědit	k5eAaPmAgInS
Českou	český	k2eAgFnSc4d1
Kamenici	Kamenice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1676	#num#	k4
získal	získat	k5eAaPmAgInS
hraběcí	hraběcí	k2eAgInSc1d1
titul	titul	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Oktavián	Oktavián	k1gMnSc1
měl	mít	k5eAaImAgMnS
čtyři	čtyři	k4xCgFnPc4
dcery	dcera	k1gFnPc4
a	a	k8xC
dva	dva	k4xCgMnPc4
syny	syn	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Oldřich	Oldřich	k1gMnSc1
vystřídal	vystřídat	k5eAaPmAgInS
řadu	řada	k1gFnSc4
dvorských	dvorský	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
mj.	mj.	kA
působil	působit	k5eAaImAgInS
i	i	k9
v	v	k7c6
císařské	císařský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
zemřel	zemřít	k5eAaPmAgMnS
bezdětný	bezdětný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Václav	Václav	k1gMnSc1
Norbert	Norbert	k1gMnSc1
Oktavián	Oktavián	k1gMnSc1
také	také	k9
vystřídal	vystřídat	k5eAaPmAgMnS
řadu	řada	k1gFnSc4
funkcí	funkce	k1gFnPc2
u	u	k7c2
dvora	dvůr	k1gInSc2
a	a	k8xC
roku	rok	k1gInSc2
1705	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
nejvyšším	vysoký	k2eAgMnSc7d3
kancléřem	kancléř	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
dvou	dva	k4xCgNnPc2
manželství	manželství	k1gNnPc2
měl	mít	k5eAaImAgMnS
18	#num#	k4
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
dospělosti	dospělost	k1gFnSc2
se	se	k3xPyFc4
dožilo	dožít	k5eAaPmAgNnS
osm	osm	k4xCc1
dcer	dcera	k1gFnPc2
a	a	k8xC
sedm	sedm	k4xCc1
synů	syn	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nich	on	k3xPp3gMnPc2
vzešly	vzejít	k5eAaPmAgFnP
tři	tři	k4xCgFnPc1
rodové	rodový	k2eAgFnPc1d1
linie	linie	k1gFnPc1
<g/>
,	,	kIx,
mezi	mezi	k7c4
něž	jenž	k3xRgMnPc4
rozdělil	rozdělit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
veliký	veliký	k2eAgInSc4d1
majetek	majetek	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
rodu	rod	k1gInSc2
</s>
<s>
Pět	pět	k4xCc1
ze	z	k7c2
synů	syn	k1gMnPc2
Václava	Václav	k1gMnSc2
Norberta	Norbert	k1gMnSc2
Oktaviána	Oktavián	k1gMnSc2
se	se	k3xPyFc4
aktivně	aktivně	k6eAd1
věnovalo	věnovat	k5eAaPmAgNnS,k5eAaImAgNnS
politické	politický	k2eAgNnSc1d1
či	či	k8xC
vojenské	vojenský	k2eAgFnSc6d1
kariéře	kariéra	k1gFnSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
dosáhli	dosáhnout	k5eAaPmAgMnP
značných	značný	k2eAgMnPc2d1
úspěchů	úspěch	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
další	další	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
rodu	rod	k1gInSc2
však	však	k9
měli	mít	k5eAaImAgMnP
vliv	vliv	k1gInSc4
jen	jen	k9
Štěpán	Štěpán	k1gMnSc1
Vilém	Vilém	k1gMnSc1
(	(	kIx(
<g/>
1679	#num#	k4
<g/>
–	–	k?
<g/>
1749	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
získal	získat	k5eAaPmAgInS
knížecí	knížecí	k2eAgInSc1d1
titul	titul	k1gInSc1
<g/>
,	,	kIx,
Filip	Filip	k1gMnSc1
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
1700	#num#	k4
<g/>
–	–	k?
<g/>
1749	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
založil	založit	k5eAaPmAgMnS
knížecí	knížecí	k2eAgFnSc4d1
větev	větev	k1gFnSc4
Kinských	Kinská	k1gFnPc2
a	a	k8xC
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Kinský	Kinský	k1gMnSc1
(	(	kIx(
<g/>
1678	#num#	k4
<g/>
–	–	k?
<g/>
1741	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
založil	založit	k5eAaPmAgMnS
chlumeckou	chlumecký	k2eAgFnSc4d1
větev	větev	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Norbert	Norbert	k1gMnSc1
Oktavián	Oktavián	k1gMnSc1
Kinský	Kinský	k1gMnSc1
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
poslední	poslední	k2eAgInSc4d1
společný	společný	k2eAgInSc4d1
předek	předek	k1gInSc4
všech	všecek	k3xTgNnPc2
nynějších	nynější	k2eAgNnPc2d1
Kinských	Kinské	k1gNnPc2
<g/>
,	,	kIx,
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
majetku	majetek	k1gInSc6
byl	být	k5eAaImAgInS
Chlumec	Chlumec	k1gInSc1
<g/>
,	,	kIx,
Choceň	Choceň	k1gFnSc1
<g/>
,	,	kIx,
Chroustovice	Chroustovice	k1gFnSc1
<g/>
,	,	kIx,
Kamenice	Kamenice	k1gFnSc1
<g/>
,	,	kIx,
Koloděje	Koloděje	k1gFnPc1
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
hrad	hrad	k1gInSc1
<g/>
,	,	kIx,
Prčice	Prčice	k1gFnPc1
<g/>
,	,	kIx,
Rataje	Rataje	k1gInPc1
<g/>
,	,	kIx,
Rosice	Rosice	k1gFnPc1
<g/>
,	,	kIx,
Rychmburk	Rychmburk	k1gInSc1
a	a	k8xC
Sloup	sloup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Štěpán	Štěpán	k1gMnSc1
Vilém	Vilém	k1gMnSc1
zdědil	zdědit	k5eAaPmAgMnS
Choceň	Choceň	k1gFnSc4
<g/>
,	,	kIx,
Rataje	Rataje	k1gInPc4
<g/>
,	,	kIx,
Rychmburk	Rychmburk	k1gInSc4
a	a	k8xC
Rosice	Rosice	k1gFnPc4
<g/>
,	,	kIx,
pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
diplomatických	diplomatický	k2eAgFnPc6d1
službách	služba	k1gFnPc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
mj.	mj.	kA
nejvyšším	vysoký	k2eAgMnSc7d3
komorníkem	komorník	k1gMnSc7
a	a	k8xC
později	pozdě	k6eAd2
nejvyšším	vysoký	k2eAgMnSc7d3
hofmistrem	hofmistr	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
své	svůj	k3xOyFgFnSc2
zásluhy	zásluha	k1gFnSc2
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1746	#num#	k4
resp.	resp.	kA
1747	#num#	k4
povýšen	povýšit	k5eAaPmNgMnS
na	na	k7c4
českého	český	k2eAgMnSc4d1
a	a	k8xC
říšského	říšský	k2eAgMnSc4d1
knížete	kníže	k1gMnSc4
<g/>
,	,	kIx,
kteroužto	kteroužto	k?
hodnost	hodnost	k1gFnSc1
získal	získat	k5eAaPmAgInS
dědičně	dědičně	k6eAd1
pro	pro	k7c4
představitele	představitel	k1gMnPc4
nejstarší	starý	k2eAgFnSc2d3
mužské	mužský	k2eAgFnSc2d1
větve	větev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
syn	syn	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
roku	rok	k1gInSc2
1752	#num#	k4
bez	bez	k1gInSc4
mužských	mužský	k2eAgMnPc2d1
potomků	potomek	k1gMnPc2
a	a	k8xC
jeho	jeho	k3xOp3gMnPc3
dědictví	dědictví	k1gNnSc1
včetně	včetně	k7c2
knížecích	knížecí	k2eAgInPc2d1
titulů	titul	k1gInPc2
přešlo	přejít	k5eAaPmAgNnS
na	na	k7c4
dědice	dědic	k1gMnSc4
Štěpánova	Štěpánův	k2eAgMnSc4d1
mladšího	mladý	k2eAgMnSc4d2
bratra	bratr	k1gMnSc4
Filipa	Filip	k1gMnSc4
Josefa	Josef	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Knížecí	knížecí	k2eAgFnSc1d1
větev	větev	k1gFnSc1
</s>
<s>
Zakladatel	zakladatel	k1gMnSc1
větve	větev	k1gFnSc2
Filip	Filip	k1gMnSc1
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
1700	#num#	k4
<g/>
–	–	k?
<g/>
1749	#num#	k4
<g/>
)	)	kIx)
působil	působit	k5eAaImAgMnS
v	v	k7c6
letech	let	k1gInPc6
1728	#num#	k4
<g/>
–	–	k?
<g/>
1735	#num#	k4
v	v	k7c6
diplomatických	diplomatický	k2eAgFnPc6d1
službách	služba	k1gFnPc6
jako	jako	k8xS,k8xC
rakouský	rakouský	k2eAgMnSc1d1
vyslanec	vyslanec	k1gMnSc1
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
,	,	kIx,
dále	daleko	k6eAd2
v	v	k7c6
České	český	k2eAgFnSc6d1
dvorské	dvorský	k2eAgFnSc6d1
kanceláři	kancelář	k1gFnSc6
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1738	#num#	k4
<g/>
–	–	k?
<g/>
1745	#num#	k4
ve	v	k7c6
funkci	funkce	k1gFnSc6
nejvyššího	vysoký	k2eAgMnSc2d3
kancléře	kancléř	k1gMnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
projevoval	projevovat	k5eAaImAgMnS
jako	jako	k9
český	český	k2eAgMnSc1d1
vlastenec	vlastenec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
patřil	patřit	k5eAaImAgMnS
k	k	k7c3
průkopníkům	průkopník	k1gMnPc3
manufakturní	manufakturní	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
sloupském	sloupský	k2eAgNnSc6d1
panství	panství	k1gNnSc6
úspěšně	úspěšně	k6eAd1
podnikal	podnikat	k5eAaImAgMnS
ve	v	k7c6
sklářství	sklářství	k1gNnSc6
<g/>
,	,	kIx,
méně	málo	k6eAd2
úspěšně	úspěšně	k6eAd1
v	v	k7c6
plátenictví	plátenictví	k1gNnSc6
a	a	k8xC
experimentoval	experimentovat	k5eAaImAgMnS
s	s	k7c7
hedvábnictvím	hedvábnictví	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Držel	držet	k5eAaImAgMnS
velkostatek	velkostatek	k1gInSc4
Česká	český	k2eAgFnSc1d1
Kamenice	Kamenice	k1gFnSc1
<g/>
,	,	kIx,
koupil	koupit	k5eAaPmAgMnS
a	a	k8xC
zrekonstruoval	zrekonstruovat	k5eAaPmAgMnS
zámek	zámek	k1gInSc4
Zlonice	Zlonice	k1gFnSc2
(	(	kIx(
<g/>
1720	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Mšené	Mšené	k1gNnSc1
(	(	kIx(
<g/>
1742	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Budenice	Budenice	k1gFnSc1
(	(	kIx(
<g/>
1748	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mladší	mladý	k2eAgMnSc1d2
bratr	bratr	k1gMnSc1
Filipa	Filip	k1gMnSc4
Josefa	Josef	k1gMnSc4
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Jan	Jan	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
Kinský	Kinský	k1gMnSc1
(	(	kIx(
<g/>
1705	#num#	k4
<g/>
–	–	k?
<g/>
1780	#num#	k4
<g/>
)	)	kIx)
studoval	studovat	k5eAaImAgMnS
na	na	k7c6
arcibiskupském	arcibiskupský	k2eAgInSc6d1
semináři	seminář	k1gInSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
podnikl	podniknout	k5eAaPmAgMnS
kavalírskou	kavalírský	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
na	na	k7c4
Maltu	Malta	k1gFnSc4
<g/>
,	,	kIx,
Rhodos	Rhodos	k1gInSc1
a	a	k8xC
do	do	k7c2
Palestiny	Palestina	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
rytíř	rytíř	k1gMnSc1
maltézského	maltézský	k2eAgInSc2d1
řádu	řád	k1gInSc2
přijal	přijmout	k5eAaPmAgMnS
závazek	závazek	k1gInSc4
celibátu	celibát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Držel	držet	k5eAaImAgInS
Zlonice	Zlonice	k1gFnSc2
<g/>
,	,	kIx,
Budenice	Budenice	k1gFnSc2
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
bratrem	bratr	k1gMnSc7
dědil	dědit	k5eAaImAgMnS
Sloup	sloup	k1gInSc4
<g/>
;	;	kIx,
na	na	k7c6
svých	svůj	k3xOyFgNnPc6
panstvích	panství	k1gNnPc6
provedl	provést	k5eAaPmAgMnS
pozemkovou	pozemkový	k2eAgFnSc4d1
reformu	reforma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patřil	patřit	k5eAaImAgMnS
k	k	k7c3
nejvýznamnějším	významný	k2eAgMnPc3d3
českým	český	k2eAgMnPc3d1
podnikatelům	podnikatel	k1gMnPc3
ve	v	k7c6
sklářském	sklářský	k2eAgInSc6d1
a	a	k8xC
textilním	textilní	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
(	(	kIx(
<g/>
Sloup	sloup	k1gInSc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
Bor	bor	k1gInSc1
<g/>
,	,	kIx,
Bělá	bělat	k5eAaImIp3nS
pod	pod	k7c7
Bezdězem	Bezděz	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kníže	kníže	k1gMnSc1
Filip	Filip	k1gMnSc1
Josef	Josef	k1gMnSc1
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
Marií	Maria	k1gFnSc7
Karolínou	Karolína	k1gFnSc7
z	z	k7c2
Martinic	Martinice	k1gFnPc2
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yQgFnSc7,k3yRgFnSc7,k3yIgFnSc7
měl	mít	k5eAaImAgMnS
čtyři	čtyři	k4xCgFnPc4
dcery	dcera	k1gFnPc4
a	a	k8xC
čtyři	čtyři	k4xCgMnPc4
syny	syn	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvorozený	prvorozený	k2eAgMnSc1d1
syn	syn	k1gMnSc1
František	František	k1gMnSc1
Oldřich	Oldřich	k1gMnSc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
Kinský	Kinský	k1gMnSc1
(	(	kIx(
<g/>
1726	#num#	k4
<g/>
–	–	k?
<g/>
1792	#num#	k4
<g/>
)	)	kIx)
zdědil	zdědit	k5eAaPmAgInS
choceňský	choceňský	k2eAgInSc1d1
fideikomis	fideikomis	k1gInSc1
<g/>
,	,	kIx,
druhorozený	druhorozený	k2eAgMnSc1d1
Jan	Jan	k1gMnSc1
Josef	Josef	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
(	(	kIx(
<g/>
1736	#num#	k4
<g/>
–	–	k?
<g/>
1804	#num#	k4
<g/>
)	)	kIx)
Zlonice	Zlonice	k1gFnSc1
a	a	k8xC
Sloup	sloup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Josef	Josef	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
Kinský	Kinský	k1gMnSc1
měl	mít	k5eAaImAgMnS
pouze	pouze	k6eAd1
syna	syn	k1gMnSc4
Bedřicha	Bedřich	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
zemřel	zemřít	k5eAaPmAgMnS
bezdětný	bezdětný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Sloup	sloup	k1gInSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
držení	držení	k1gNnSc2
chlumecké	chlumecký	k2eAgFnSc2d1
větve	větev	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
vyjma	vyjma	k7c2
Zlonic	Zlonice	k1gFnPc2
a	a	k8xC
Budenic	Budenice	k1gFnPc2
ještě	ještě	k6eAd1
zdědil	zdědit	k5eAaPmAgInS
Kamenici	Kamenice	k1gFnSc4
<g/>
,	,	kIx,
přikoupil	přikoupit	k5eAaPmAgMnS
Hospozín	Hospozín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1752	#num#	k4
zdědil	zdědit	k5eAaPmAgInS
knížecí	knížecí	k2eAgInSc1d1
titul	titul	k1gInSc1
a	a	k8xC
vstoupil	vstoupit	k5eAaPmAgInS
do	do	k7c2
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
získal	získat	k5eAaPmAgInS
hodnost	hodnost	k1gFnSc4
polního	polní	k2eAgMnSc2d1
maršálka	maršálek	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
mužských	mužský	k2eAgMnPc2d1
potomků	potomek	k1gMnPc2
ho	on	k3xPp3gMnSc4
přežil	přežít	k5eAaPmAgMnS
pouze	pouze	k6eAd1
syn	syn	k1gMnSc1
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
1754	#num#	k4
<g/>
–	–	k?
<g/>
1798	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
říšským	říšský	k2eAgMnSc7d1
dvorním	dvorní	k2eAgMnSc7d1
radou	rada	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Jan	Jan	k1gMnSc1
Josef	Josef	k1gMnSc1
dále	daleko	k6eAd2
rozšířil	rozšířit	k5eAaPmAgMnS
majetek	majetek	k1gInSc4
rodu	rod	k1gInSc2
a	a	k8xC
koupil	koupit	k5eAaPmAgMnS
Kostelec	Kostelec	k1gInSc4
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
<g/>
,	,	kIx,
Peruc	Peruc	k1gInSc1
<g/>
,	,	kIx,
Přestavlky	Přestavlek	k1gInPc1
<g/>
,	,	kIx,
Vejvanovice	Vejvanovice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
syny	syn	k1gMnPc4
Ferdinanda	Ferdinand	k1gMnSc4
a	a	k8xC
Františka	František	k1gMnSc4
Josefa	Josef	k1gMnSc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
založili	založit	k5eAaPmAgMnP
dvě	dva	k4xCgFnPc4
další	další	k2eAgFnPc4d1
větve	větev	k1gFnPc4
<g/>
;	;	kIx,
a	a	k8xC
jelikož	jelikož	k8xS
i	i	k9
Ferdinand	Ferdinand	k1gMnSc1
(	(	kIx(
<g/>
1781	#num#	k4
<g/>
–	–	k?
<g/>
1812	#num#	k4
<g/>
)	)	kIx)
měl	mít	k5eAaImAgMnS
dva	dva	k4xCgMnPc4
syny	syn	k1gMnPc4
Rudolfa	Rudolf	k1gMnSc2
(	(	kIx(
<g/>
1802	#num#	k4
<g/>
–	–	k?
<g/>
1836	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Josefa	Josef	k1gMnSc2
(	(	kIx(
<g/>
1806	#num#	k4
<g/>
–	–	k?
<g/>
1862	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rozdělila	rozdělit	k5eAaPmAgFnS
se	se	k3xPyFc4
knížecí	knížecí	k2eAgFnSc1d1
větev	větev	k1gFnSc1
celkem	celkem	k6eAd1
na	na	k7c4
tři	tři	k4xCgFnPc4
části	část	k1gFnPc4
<g/>
:	:	kIx,
choceňskou	choceňský	k2eAgFnSc4d1
(	(	kIx(
<g/>
hlavní	hlavní	k2eAgFnSc4d1
knížecí	knížecí	k2eAgFnSc4d1
větev	větev	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kosteleckou	kostelecký	k2eAgFnSc7d1
a	a	k8xC
peruckou	perucký	k2eAgFnSc7d1
(	(	kIx(
<g/>
náměšťskou	náměšťský	k2eAgFnSc7d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Choceňská	choceňský	k2eAgFnSc1d1
větev	větev	k1gFnSc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
kníže	kníže	k1gMnSc1
Kinský	Kinský	k1gMnSc1
</s>
<s>
Starší	starý	k2eAgMnPc1d2
syn	syn	k1gMnSc1
knížete	kníže	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
(	(	kIx(
<g/>
1781	#num#	k4
<g/>
–	–	k?
<g/>
1812	#num#	k4
<g/>
)	)	kIx)
Rudolf	Rudolf	k1gMnSc1
(	(	kIx(
<g/>
1802	#num#	k4
<g/>
–	–	k?
<g/>
1836	#num#	k4
<g/>
)	)	kIx)
podporoval	podporovat	k5eAaImAgMnS
české	český	k2eAgNnSc4d1
národní	národní	k2eAgNnSc4d1
obrození	obrození	k1gNnSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
spoluzakladatelem	spoluzakladatel	k1gMnSc7
a	a	k8xC
prvním	první	k4xOgMnSc7
kurátorem	kurátor	k1gMnSc7
Matice	matice	k1gFnSc2
české	český	k2eAgFnPc1d1
<g/>
,	,	kIx,
podporovatelem	podporovatel	k1gMnSc7
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pověřil	pověřit	k5eAaPmAgMnS
Františka	Františka	k1gFnSc1
Palackého	Palackého	k2eAgFnSc1d1
studiem	studio	k1gNnSc7
genealogie	genealogie	k1gFnSc2
svého	svůj	k3xOyFgInSc2
rodu	rod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přikoupil	přikoupit	k5eAaPmAgInS
Heřmanův	Heřmanův	k2eAgInSc1d1
Městec	Městec	k1gInSc1
<g/>
,	,	kIx,
Choceň	Choceň	k1gFnSc1
a	a	k8xC
Horažďovice	Horažďovice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
svou	svůj	k3xOyFgFnSc7
ženou	žena	k1gFnSc7
Vilemínou	Vilemína	k1gFnSc7
Eliškou	Eliška	k1gFnSc7
<g/>
,	,	kIx,
rozenou	rozený	k2eAgFnSc7d1
z	z	k7c2
Colloredo-Mannsfeldu	Colloredo-Mannsfeld	k1gInSc2
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
čtyři	čtyři	k4xCgFnPc4
dcery	dcera	k1gFnPc4
a	a	k8xC
syna	syn	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
Bonaventuru	Bonaventura	k1gFnSc4
(	(	kIx(
<g/>
1834	#num#	k4
<g/>
–	–	k?
<g/>
1904	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vlastnil	vlastnit	k5eAaImAgInS
Heřmanův	Heřmanův	k2eAgInSc4d1
Městec	Městec	k1gInSc4
<g/>
,	,	kIx,
Horažďovice	Horažďovice	k1gFnPc4
<g/>
,	,	kIx,
Choceň	Choceň	k1gFnSc4
<g/>
,	,	kIx,
Kamenici	Kamenice	k1gFnSc4
<g/>
,	,	kIx,
Rosice	Rosice	k1gFnPc4
a	a	k8xC
Zlonice	Zlonice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ferdinand	Ferdinand	k1gMnSc1
byl	být	k5eAaImAgMnS
dědičným	dědičný	k2eAgMnSc7d1
členem	člen	k1gMnSc7
rakouské	rakouský	k2eAgFnSc2d1
panské	panský	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
více	hodně	k6eAd2
než	než	k8xS
veřejným	veřejný	k2eAgInSc7d1
životem	život	k1gInSc7
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgInS
hospodářstvím	hospodářství	k1gNnSc7
a	a	k8xC
správou	správa	k1gFnSc7
rodinného	rodinný	k2eAgInSc2d1
majetku	majetek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
němu	on	k3xPp3gMnSc3
se	se	k3xPyFc4
rod	rod	k1gInSc1
Kinských	Kinský	k1gMnPc2
začal	začít	k5eAaPmAgInS
orientovat	orientovat	k5eAaBmF
na	na	k7c4
průmysl	průmysl	k1gInSc4
a	a	k8xC
na	na	k7c6
jejich	jejich	k3xOp3gInPc6
velkostatcích	velkostatek	k1gInPc6
vznikla	vzniknout	k5eAaPmAgFnS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
cukrovarů	cukrovar	k1gInPc2
<g/>
,	,	kIx,
pivovarů	pivovar	k1gInPc2
<g/>
,	,	kIx,
atp.	atp.	kA
Oženil	oženit	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
Marií	Maria	k1gFnSc7
Annou	Anna	k1gFnSc7
z	z	k7c2
Lichtenštejna	Lichtenštejn	k1gInSc2
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yIgFnSc7,k3yRgFnSc7,k3yQgFnSc7
měl	mít	k5eAaImAgMnS
tři	tři	k4xCgFnPc4
dcery	dcera	k1gFnPc4
a	a	k8xC
tři	tři	k4xCgMnPc4
syny	syn	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvorozená	prvorozený	k2eAgFnSc1d1
Vilemína	Vilemína	k1gFnSc1
se	se	k3xPyFc4
vdala	vdát	k5eAaPmAgFnS
roku	rok	k1gInSc2
1878	#num#	k4
za	za	k7c4
Františka	František	k1gMnSc4
Josefa	Josef	k1gMnSc4
<g/>
,	,	kIx,
knížete	kníže	k1gMnSc4
z	z	k7c2
Auerspergu	Auersperg	k1gInSc2
z	z	k7c2
nedalekých	daleký	k2eNgInPc2d1
Slatiňan	Slatiňany	k1gInPc2
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
Karel	Karel	k1gMnSc1
(	(	kIx(
<g/>
1858	#num#	k4
<g/>
–	–	k?
<g/>
1919	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
věnoval	věnovat	k5eAaPmAgInS,k5eAaImAgInS
diplomacii	diplomacie	k1gFnSc3
<g/>
,	,	kIx,
restauroval	restaurovat	k5eAaBmAgInS
park	park	k1gInSc1
v	v	k7c6
Budenicích	Budenice	k1gFnPc6
<g/>
;	;	kIx,
zemřel	zemřít	k5eAaPmAgMnS
bezdětný	bezdětný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgMnSc1
syn	syn	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
(	(	kIx(
<g/>
1859	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
<g/>
)	)	kIx)
měl	mít	k5eAaImAgInS
šest	šest	k4xCc4
dcer	dcera	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedině	jedině	k6eAd1
jejich	jejich	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Vincenc	Vincenc	k1gMnSc1
(	(	kIx(
<g/>
1866	#num#	k4
<g/>
–	–	k?
<g/>
1916	#num#	k4
<g/>
)	)	kIx)
měl	mít	k5eAaImAgMnS
kromě	kromě	k7c2
čtyř	čtyři	k4xCgFnPc2
dcer	dcera	k1gFnPc2
i	i	k8xC
tři	tři	k4xCgMnPc4
syny	syn	k1gMnPc4
a	a	k8xC
roku	rok	k1gInSc2
1908	#num#	k4
zdědil	zdědit	k5eAaPmAgInS
po	po	k7c6
matčině	matčin	k2eAgNnSc6d1
bratrovi	bratr	k1gMnSc3
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
lichtenštejnské	lichtenštejnský	k2eAgNnSc4d1
panství	panství	k1gNnSc4
Moravský	moravský	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1
pokračovateli	pokračovatel	k1gMnPc7
a	a	k8xC
představiteli	představitel	k1gMnPc7
hlavní	hlavní	k2eAgFnSc2d1
knížecí	knížecí	k2eAgFnSc2d1
větve	větev	k1gFnSc2
rodu	rod	k1gInSc2
Kinských	Kinská	k1gFnPc2
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
<g/>
,	,	kIx,
X.	X.	kA
kníže	kníže	k1gMnSc1
Kinský	Kinský	k1gMnSc1
ze	z	k7c2
Vchynic	Vchynice	k1gFnPc2
a	a	k8xC
Tetova	Tetovo	k1gNnSc2
(	(	kIx(
<g/>
1893	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
významným	významný	k2eAgMnSc7d1
představitelem	představitel	k1gMnSc7
Sudetoněmecké	sudetoněmecký	k2eAgFnSc2d1
strany	strana	k1gFnSc2
</s>
<s>
František	František	k1gMnSc1
Oldřich	Oldřich	k1gMnSc1
<g/>
,	,	kIx,
XI	XI	kA
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
Kinský	Kinský	k1gMnSc1
ze	z	k7c2
Vchynic	Vchynice	k1gFnPc2
a	a	k8xC
Tetova	Tetovo	k1gNnSc2
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
<g/>
,	,	kIx,
XII	XII	kA
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
Kinský	Kinský	k1gMnSc1
ze	z	k7c2
Vchynic	Vchynice	k1gFnPc2
a	a	k8xC
Tetova	Tetovo	k1gNnSc2
(	(	kIx(
<g/>
*	*	kIx~
1967	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Václav	Václav	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
Kinský	Kinský	k1gMnSc1
ze	z	k7c2
Vchynic	Vchynice	k1gFnPc2
a	a	k8xC
Tetova	Tetovo	k1gNnSc2
(	(	kIx(
<g/>
*	*	kIx~
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
tato	tento	k3xDgFnSc1
větev	větev	k1gFnSc1
uplatňovala	uplatňovat	k5eAaImAgFnS
restituční	restituční	k2eAgInPc4d1
nároky	nárok	k1gInPc4
(	(	kIx(
<g/>
mj.	mj.	kA
zámek	zámek	k1gInSc1
Budenice	Budenice	k1gFnSc1
a	a	k8xC
nedaleká	daleký	k2eNgFnSc1d1
hrobka	hrobka	k1gFnSc1
rodu	rod	k1gInSc2
Kinských	Kinský	k1gMnPc2
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Isidora	Isidor	k1gMnSc2
v	v	k7c6
Jarpicích	Jarpice	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
ale	ale	k9
nebyly	být	k5eNaImAgFnP
českými	český	k2eAgMnPc7d1
soudy	soud	k1gInPc4
uznány	uznán	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Hlavy	hlava	k1gFnPc1
knížecí	knížecí	k2eAgFnSc2d1
linie	linie	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
treeview	treeview	k?
ul	ul	kA
<g/>
{	{	kIx(
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
;	;	kIx,
<g/>
margin	margina	k1gFnPc2
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
treeview	treeview	k?
li	li	k8xS
<g/>
{	{	kIx(
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
;	;	kIx,
<g/>
margin	margina	k1gFnPc2
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
;	;	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
list-style-type	list-style-typ	k1gInSc5
<g/>
:	:	kIx,
<g/>
none	none	k1gNnSc3
<g/>
;	;	kIx,
<g/>
list-style-image	list-style-image	k1gNnSc3
<g/>
:	:	kIx,
<g/>
none	none	k1gNnSc3
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
treeview	treeview	k?
li	li	k8xS
li	li	k8xS
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
url	url	k?
<g/>
(	(	kIx(
<g/>
"	"	kIx"
<g/>
//	//	k?
<g/>
upload	upload	k1gInSc1
<g/>
.	.	kIx.
<g/>
wikimedia	wikimedium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
wikipedia	wikipedium	k1gNnSc2
<g/>
/	/	kIx~
<g/>
commons	commons	k1gInSc1
<g/>
/	/	kIx~
<g/>
f	f	k?
<g/>
/	/	kIx~
<g/>
f	f	k?
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
Treeview-grey-line	Treeview-grey-lin	k1gInSc5
<g/>
.	.	kIx.
<g/>
png	png	k?
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
no-repeat	no-repeat	k1gInSc1
0	#num#	k4
-2981	-2981	k4
<g/>
px	px	k?
<g/>
;	;	kIx,
<g/>
padding-left	padding-left	k1gMnSc1
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
px	px	k?
<g/>
;	;	kIx,
<g/>
text-indent	text-indent	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
<g/>
0.3	0.3	k4
<g/>
em	em	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
treeview	treeview	k?
li	li	k8xS
li	li	k8xS
<g/>
.	.	kIx.
<g/>
lastline	lastlin	k1gInSc5
<g/>
{	{	kIx(
<g/>
background-position	background-position	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
-5971	-5971	k4
<g/>
px	px	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
treeview	treeview	k?
li	li	k8xS
<g/>
.	.	kIx.
<g/>
emptyline	emptylin	k1gInSc5
<g/>
>	>	kIx)
<g/>
ul	ul	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
{	{	kIx(
<g/>
margin-left	margin-left	k5eAaPmF
<g/>
:	:	kIx,
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
px	px	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
treeview	treeview	k?
li	li	k8xS
<g/>
.	.	kIx.
<g/>
emptyline	emptylin	k1gInSc5
<g/>
>	>	kIx)
<g/>
ul	ul	kA
<g/>
>	>	kIx)
<g/>
li	li	k8xS
<g/>
:	:	kIx,
<g/>
first-child	first-child	k1gInSc1
<g/>
{	{	kIx(
<g/>
background-position	background-position	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
9	#num#	k4
<g/>
px	px	k?
<g/>
}	}	kIx)
</s>
<s>
Václav	Václav	k1gMnSc1
Norbert	Norbert	k1gMnSc1
Oktavián	Oktavián	k1gMnSc1
(	(	kIx(
<g/>
1642	#num#	k4
<g/>
–	–	k?
<g/>
1719	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
1687	#num#	k4
<g/>
–	–	k?
<g/>
1719	#num#	k4
</s>
<s>
Štěpán	Štěpán	k1gMnSc1
Vilém	Vilém	k1gMnSc1
(	(	kIx(
<g/>
1679	#num#	k4
<g/>
-	-	kIx~
<g/>
1749	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
1747	#num#	k4
<g/>
–	–	k?
<g/>
1749	#num#	k4
<g/>
,	,	kIx,
čtvrtý	čtvrtý	k4xOgMnSc1
přeživský	přeživský	k2eAgMnSc1d1
syn	syn	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
1726	#num#	k4
<g/>
–	–	k?
<g/>
1752	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
1749	#num#	k4
<g/>
–	–	k?
<g/>
1752	#num#	k4
</s>
<s>
Filip	Filip	k1gMnSc1
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
1700	#num#	k4
<g/>
–	–	k?
<g/>
1749	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pátý	pátý	k4xOgMnSc1
přeživší	přeživší	k2eAgMnSc1d1
syn	syn	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
Oldřich	Oldřich	k1gMnSc1
(	(	kIx(
<g/>
1726	#num#	k4
<g/>
–	–	k?
<g/>
1792	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
1752	#num#	k4
<g/>
–	–	k?
<g/>
1792	#num#	k4
</s>
<s>
Josef	Josef	k1gMnSc1
Arnošt	Arnošt	k1gMnSc1
(	(	kIx(
<g/>
1751	#num#	k4
<g/>
–	–	k?
<g/>
1798	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
1792	#num#	k4
<g/>
–	–	k?
<g/>
1798	#num#	k4
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
Jan	Jan	k1gMnSc1
(	(	kIx(
<g/>
1781	#num#	k4
<g/>
–	–	k?
<g/>
1812	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
1798	#num#	k4
<g/>
–	–	k?
<g/>
1812	#num#	k4
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
(	(	kIx(
<g/>
1802	#num#	k4
<g/>
–	–	k?
<g/>
1836	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
1812	#num#	k4
<g/>
–	–	k?
<g/>
1836	#num#	k4
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
Bonaventura	Bonaventura	k1gFnSc1
(	(	kIx(
<g/>
1834	#num#	k4
<g/>
–	–	k?
<g/>
1904	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
1836	#num#	k4
<g/>
–	–	k?
<g/>
1904	#num#	k4
</s>
<s>
Karel	Karel	k1gMnSc1
(	(	kIx(
<g/>
1858	#num#	k4
<g/>
–	–	k?
<g/>
1919	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
8	#num#	k4
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
1904	#num#	k4
<g/>
–	–	k?
<g/>
1919	#num#	k4
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
(	(	kIx(
<g/>
1859	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
1919	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
Vincent	Vincent	k1gMnSc1
(	(	kIx(
<g/>
1866	#num#	k4
<g/>
–	–	k?
<g/>
1919	#num#	k4
<g/>
)	)	kIx)
hrabě	hrabě	k1gMnSc1
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
(	(	kIx(
<g/>
1892	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
1930	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
</s>
<s>
František	František	k1gMnSc1
Oldřich	Oldřich	k1gMnSc1
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
11	#num#	k4
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
1938	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
</s>
<s>
Karel	Karel	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1967	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
od	od	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
</s>
<s>
Wenzel	Wenzet	k5eAaImAgMnS,k5eAaPmAgMnS
Ferdinand	Ferdinand	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
</s>
<s>
Maximilian	Maximilian	k1gMnSc1
Benedikt	Benedikt	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
</s>
<s>
Stephan	Stephan	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
</s>
<s>
dva	dva	k4xCgMnPc1
mladší	mladý	k2eAgMnPc1d2
synové	syn	k1gMnPc1
s	s	k7c7
mužskými	mužský	k2eAgInPc7d1
potomky	potomek	k1gMnPc7
</s>
<s>
Kostelecká	kostelecký	k2eAgFnSc1d1
větev	větev	k1gFnSc1
</s>
<s>
Tuto	tento	k3xDgFnSc4
větev	větev	k1gFnSc4
založil	založit	k5eAaPmAgMnS
hrabě	hrabě	k1gMnSc1
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
1806	#num#	k4
<g/>
–	–	k?
<g/>
1862	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
druhý	druhý	k4xOgMnSc1
syn	syn	k1gMnSc1
knížete	kníže	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
(	(	kIx(
<g/>
1781	#num#	k4
<g/>
–	–	k?
<g/>
1812	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
zdědil	zdědit	k5eAaPmAgMnS
Kostelec	Kostelec	k1gInSc4
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
a	a	k8xC
Borovnici	borovnice	k1gFnSc6
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnPc1
synové	syn	k1gMnPc1
se	se	k3xPyFc4
stejně	stejně	k6eAd1
jako	jako	k9
on	on	k3xPp3gMnSc1
věnovali	věnovat	k5eAaPmAgMnP,k5eAaImAgMnP
vojenství	vojenství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
měl	mít	k5eAaImAgMnS
syny	syn	k1gMnPc7
Bedřicha	Bedřich	k1gMnSc2
Karla	Karel	k1gMnSc2
(	(	kIx(
<g/>
1834	#num#	k4
<g/>
–	–	k?
<g/>
1899	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Volfganga	Volfganga	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1836	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Františka	František	k1gMnSc4
(	(	kIx(
<g/>
*	*	kIx~
1841	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Bedřich	Bedřich	k1gMnSc1
Karel	Karel	k1gMnSc1
(	(	kIx(
<g/>
1834	#num#	k4
<g/>
–	–	k?
<g/>
1899	#num#	k4
<g/>
)	)	kIx)
měl	mít	k5eAaImAgInS
ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
Kostelec	Kostelec	k1gInSc4
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
<g/>
,	,	kIx,
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
se	se	k3xPyFc4
politice	politika	k1gFnSc3
<g/>
,	,	kIx,
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
převzal	převzít	k5eAaPmAgMnS
správu	správa	k1gFnSc4
kosteleckého	kostelecký	k2eAgNnSc2d1
panství	panství	k1gNnSc2
jeho	jeho	k3xOp3gFnSc4
syn	syn	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
1879	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
a	a	k8xC
1939	#num#	k4
podepsal	podepsat	k5eAaPmAgInS
všechny	všechen	k3xTgFnPc1
tři	tři	k4xCgFnPc1
Deklarace	deklarace	k1gFnPc1
českých	český	k2eAgInPc2d1
šlechtických	šlechtický	k2eAgInPc2d1
rodů	rod	k1gInPc2
<g/>
,	,	kIx,
kterými	který	k3yIgInPc7,k3yRgInPc7,k3yQgInPc7
se	se	k3xPyFc4
tyto	tento	k3xDgInPc1
rody	rod	k1gInPc1
postavily	postavit	k5eAaPmAgInP
za	za	k7c4
Československo	Československo	k1gNnSc4
a	a	k8xC
přihlásily	přihlásit	k5eAaPmAgFnP
se	se	k3xPyFc4
ke	k	k7c3
svému	svůj	k3xOyFgNnSc3
češství	češství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgFnSc3
deklaraci	deklarace	k1gFnSc3
<g/>
,	,	kIx,
Národnostní	národnostní	k2eAgNnSc1d1
prohlášení	prohlášení	k1gNnSc1
prohlášení	prohlášení	k1gNnSc2
české	český	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
v	v	k7c6
září	září	k1gNnSc6
1939	#num#	k4
<g/>
,	,	kIx,
podepsali	podepsat	k5eAaPmAgMnP
i	i	k9
jeho	jeho	k3xOp3gMnPc1
synové	syn	k1gMnPc1
Bedřich	Bedřich	k1gMnSc1
Karel	Karel	k1gMnSc1
(	(	kIx(
<g/>
1911	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Alfons	Alfons	k1gMnSc1
(	(	kIx(
<g/>
1912	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
1913	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
František	František	k1gMnSc1
Antonín	Antonín	k1gMnSc1
(	(	kIx(
<g/>
1915	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
se	se	k3xPyFc4
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
neemigrovat	emigrovat	k5eNaBmF
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
jeho	jeho	k3xOp3gFnSc3
perzekuci	perzekuce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
v	v	k7c6
Československu	Československo	k1gNnSc6
zůstal	zůstat	k5eAaPmAgMnS
i	i	k9
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Josef	Josef	k1gMnSc1
Kinský	Kinský	k1gMnSc1
z	z	k7c2
Vchynic	Vchynice	k1gFnPc2
a	a	k8xC
Tetova	Tetovo	k1gNnSc2
(	(	kIx(
<g/>
1913	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
zrestituoval	zrestituovat	k5eAaPmAgInS,k5eAaBmAgInS
zámek	zámek	k1gInSc1
v	v	k7c6
Kostelci	Kostelec	k1gInSc6
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
rekonstrukci	rekonstrukce	k1gFnSc6
empírového	empírový	k2eAgInSc2d1
zámku	zámek	k1gInSc2
se	se	k3xPyFc4
po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
významně	významně	k6eAd1
zasloužili	zasloužit	k5eAaPmAgMnP
právě	právě	k9
Josef	Josef	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
František	František	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1947	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zámek	zámek	k1gInSc1
je	být	k5eAaImIp3nS
spolu	spolu	k6eAd1
s	s	k7c7
okolním	okolní	k2eAgInSc7d1
pečlivě	pečlivě	k6eAd1
upraveným	upravený	k2eAgInSc7d1
anglickým	anglický	k2eAgInSc7d1
parkem	park	k1gInSc7
zpřístupněn	zpřístupnit	k5eAaPmNgInS
veřejnosti	veřejnost	k1gFnSc6
–	–	k?
konají	konat	k5eAaImIp3nP
se	se	k3xPyFc4
v	v	k7c6
něm	on	k3xPp3gInSc6
kulturní	kulturní	k2eAgFnSc1d1
akce	akce	k1gFnSc1
(	(	kIx(
<g/>
koncerty	koncert	k1gInPc1
<g/>
,	,	kIx,
výstavy	výstava	k1gFnPc1
<g/>
,	,	kIx,
v	v	k7c6
suterénu	suterén	k1gInSc6
zámku	zámek	k1gInSc2
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
umístěno	umístit	k5eAaPmNgNnS
Muzeum	muzeum	k1gNnSc1
města	město	k1gNnSc2
Kostelec	Kostelec	k1gInSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zámecký	zámecký	k2eAgInSc1d1
park	park	k1gInSc1
je	být	k5eAaImIp3nS
proslulý	proslulý	k2eAgInSc1d1
vzácnými	vzácný	k2eAgFnPc7d1
dřevinami	dřevina	k1gFnPc7
<g/>
,	,	kIx,
rododendrony	rododendron	k1gInPc7
a	a	k8xC
na	na	k7c6
jaře	jaro	k1gNnSc6
rozsáhlými	rozsáhlý	k2eAgFnPc7d1
pláněmi	pláň	k1gFnPc7
kvetoucích	kvetoucí	k2eAgFnPc2d1
sněženek	sněženka	k1gFnPc2
<g/>
,	,	kIx,
bledulí	bledule	k1gFnPc2
a	a	k8xC
sasanek	sasanka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Kinský	Kinský	k1gMnSc1
byl	být	k5eAaImAgMnS
Čestným	čestný	k2eAgMnSc7d1
občanem	občan	k1gMnSc7
města	město	k1gNnSc2
Kostelec	Kostelec	k1gInSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
společníkem	společník	k1gMnSc7
a	a	k8xC
jednatelem	jednatel	k1gMnSc7
společnosti	společnost	k1gFnSc2
Cihelna	cihelna	k1gFnSc1
Kinský	Kinský	k1gMnSc1
<g/>
,	,	kIx,
spol	spol	k1gInSc1
<g/>
.	.	kIx.
s	s	k7c7
r.	r.	kA
o.	o.	k?
v	v	k7c6
Kostelci	Kostelec	k1gInSc6
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
a	a	k8xC
působil	působit	k5eAaImAgInS
i	i	k9
v	v	k7c6
dalších	další	k2eAgFnPc6d1
společnostech	společnost	k1gFnPc6
(	(	kIx(
<g/>
Písník	písník	k1gInSc4
Kinský	Kinský	k1gMnSc1
či	či	k8xC
Bytová	bytový	k2eAgFnSc1d1
výstavba	výstavba	k1gFnSc1
Kinský	Kinský	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
manželkou	manželka	k1gFnSc7
byla	být	k5eAaImAgFnS
Bernadetta	Bernadetta	k1gFnSc1
Kinská	Kinská	k1gFnSc1
(	(	kIx(
<g/>
rozená	rozený	k2eAgFnSc1d1
Merveldt	Merveldt	k1gInSc1
<g/>
,	,	kIx,
1922	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
synové	syn	k1gMnPc1
jsou	být	k5eAaImIp3nP
František	František	k1gMnSc1
Kinský	Kinský	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1947	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Antonín	Antonín	k1gMnSc1
Kinský	Kinský	k1gMnSc1
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rytíř	rytíř	k1gMnSc1
Vojenského	vojenský	k2eAgMnSc2d1
a	a	k8xC
špitálního	špitální	k2eAgInSc2d1
řádu	řád	k1gInSc2
sv.	sv.	kA
Lazara	Lazar	k1gMnSc2
Jeruzalémského	jeruzalémský	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Kinský	Kinský	k1gMnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
zastupitelem	zastupitel	k1gMnSc7
města	město	k1gNnSc2
Kostelec	Kostelec	k1gInSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
(	(	kIx(
<g/>
za	za	k7c4
Iniciativu	iniciativa	k1gFnSc4
občanů	občan	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
a	a	k8xC
znovu	znovu	k6eAd1
v	v	k7c6
r.	r.	kA
2018	#num#	k4
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
starostou	starosta	k1gMnSc7
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Preferenční	preferenční	k2eAgInPc1d1
hlasy	hlas	k1gInPc1
voličů	volič	k1gMnPc2
jej	on	k3xPp3gMnSc4
vynesly	vynést	k5eAaPmAgInP
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
do	do	k7c2
zastupitelstva	zastupitelstvo	k1gNnSc2
Královéhradeckého	královéhradecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
do	do	k7c2
r.	r.	kA
2016	#num#	k4
předsedou	předseda	k1gMnSc7
výboru	výbor	k1gInSc2
pro	pro	k7c4
kulturu	kultura	k1gFnSc4
a	a	k8xC
památkovou	památkový	k2eAgFnSc4d1
péči	péče	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
manželkou	manželka	k1gFnSc7
je	být	k5eAaImIp3nS
Martina	Martina	k1gFnSc1
Kinská	Kinský	k2eAgFnSc1d1
rozená	rozený	k2eAgFnSc1d1
Forejtová	Forejtová	k1gFnSc1
<g/>
,	,	kIx,
děti	dítě	k1gFnPc1
–	–	k?
dcera	dcera	k1gFnSc1
Barbora	Barbora	k1gFnSc1
Jarošová-Kinská	Jarošová-Kinský	k2eAgFnSc1d1
a	a	k8xC
syn	syn	k1gMnSc1
Kristian	Kristian	k1gMnSc1
Kinský	Kinský	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Náměšťská	náměšťský	k2eAgFnSc1d1
(	(	kIx(
<g/>
perucká	perucký	k2eAgFnSc1d1
<g/>
)	)	kIx)
větev	větev	k1gFnSc1
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7
této	tento	k3xDgFnSc2
hraběcí	hraběcí	k2eAgFnSc2d1
větve	větev	k1gFnSc2
byl	být	k5eAaImAgMnS
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
1784	#num#	k4
<g/>
–	–	k?
<g/>
1823	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
knížete	kníže	k1gMnSc2
Josefa	Josef	k1gMnSc2
Arnošta	Arnošt	k1gMnSc2
(	(	kIx(
<g/>
1751	#num#	k4
<g/>
–	–	k?
<g/>
1798	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
měl	mít	k5eAaImAgMnS
tři	tři	k4xCgMnPc4
syny	syn	k1gMnPc4
–	–	k?
Dominika	Dominik	k1gMnSc4
(	(	kIx(
<g/>
1810	#num#	k4
<g/>
–	–	k?
<g/>
1879	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Rudolfa	Rudolfa	k1gFnSc1
(	(	kIx(
<g/>
1815	#num#	k4
<g/>
–	–	k?
<g/>
1889	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Evžena	Evžen	k1gMnSc4
I.	I.	kA
(	(	kIx(
<g/>
1818	#num#	k4
<g/>
–	–	k?
<g/>
1896	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peruc	Peruc	k1gFnSc1
prodali	prodat	k5eAaPmAgMnP
<g/>
,	,	kIx,
ale	ale	k8xC
koupili	koupit	k5eAaPmAgMnP
Valašské	valašský	k2eAgNnSc4d1
Meziříčí	Meziříčí	k1gNnSc4
a	a	k8xC
zdědili	zdědit	k5eAaPmAgMnP
Náměšť	Náměšť	k1gFnSc4
na	na	k7c6
Hané	Haná	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
si	se	k3xPyFc3
také	také	k9
nechali	nechat	k5eAaPmAgMnP
vystavět	vystavět	k5eAaPmF
rodovou	rodový	k2eAgFnSc4d1
hrobku	hrobka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větev	větev	k1gFnSc1
vymřela	vymřít	k5eAaPmAgFnS
dětmi	dítě	k1gFnPc7
Evžena	Evžen	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
synem	syn	k1gMnSc7
Evženem	Evžen	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1859	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
po	po	k7c6
meči	meč	k1gInSc6
a	a	k8xC
dcerami	dcera	k1gFnPc7
Terezií	Terezie	k1gFnPc2
a	a	k8xC
Annou	Anna	k1gFnSc7
po	po	k7c6
přeslici	přeslice	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Chlumecká	chlumecký	k2eAgFnSc1d1
větev	větev	k1gFnSc1
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7
této	tento	k3xDgFnSc2
větve	větev	k1gFnSc2
je	být	k5eAaImIp3nS
hrabě	hrabě	k1gMnSc1
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Kinský	Kinský	k1gMnSc1
(	(	kIx(
<g/>
1678	#num#	k4
<g/>
–	–	k?
<g/>
1741	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejstarší	starý	k2eAgMnSc1d3
bratr	bratr	k1gMnSc1
zakladatelů	zakladatel	k1gMnPc2
knížecí	knížecí	k2eAgFnSc2d1
větve	větev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
politicky	politicky	k6eAd1
aktivní	aktivní	k2eAgMnSc1d1
<g/>
,	,	kIx,
působil	působit	k5eAaImAgMnS
jako	jako	k9
nejvyšší	vysoký	k2eAgMnSc1d3
kancléř	kancléř	k1gMnSc1
a	a	k8xC
poslanec	poslanec	k1gMnSc1
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
České	český	k2eAgFnSc2d1
při	při	k7c6
říšském	říšský	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Rakousku	Rakousko	k1gNnSc6
vyženil	vyženit	k5eAaPmAgInS
Matzen	Matzen	k2eAgInSc1d1
a	a	k8xC
Angern	Angern	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dospělosti	dospělost	k1gFnSc6
se	se	k3xPyFc4
dožilo	dožít	k5eAaPmAgNnS
pět	pět	k4xCc1
z	z	k7c2
jeho	jeho	k3xOp3gFnPc2
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
dvě	dva	k4xCgFnPc1
dcery	dcera	k1gFnPc1
a	a	k8xC
tři	tři	k4xCgMnPc1
synové	syn	k1gMnPc1
(	(	kIx(
<g/>
Josef	Josef	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
a	a	k8xC
Leopold	Leopold	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
1736	#num#	k4
<g/>
–	–	k?
<g/>
1804	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
polním	polní	k2eAgMnSc7d1
maršálkem	maršálek	k1gMnSc7
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
bezdětný	bezdětný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
bratr	bratr	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
1739	#num#	k4
<g/>
–	–	k?
<g/>
1805	#num#	k4
<g/>
)	)	kIx)
patřil	patřit	k5eAaImAgMnS
k	k	k7c3
nejvýznamnějším	významný	k2eAgMnPc3d3
osvícenským	osvícenský	k2eAgMnPc3d1
učencům	učenec	k1gMnPc3
a	a	k8xC
pedagogům	pedagog	k1gMnPc3
své	své	k1gNnSc4
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absolvoval	absolvovat	k5eAaPmAgMnS
vojenskou	vojenský	k2eAgFnSc4d1
akademii	akademie	k1gFnSc4
Theresianum	Theresianum	k1gInSc1
<g/>
,	,	kIx,
poté	poté	k6eAd1
vystudoval	vystudovat	k5eAaPmAgMnS
právnickou	právnický	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
působil	působit	k5eAaImAgMnS
u	u	k7c2
apelačního	apelační	k2eAgInSc2d1
soudu	soud	k1gInSc2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1759	#num#	k4
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
brzy	brzy	k6eAd1
generálmajorem	generálmajor	k1gMnSc7
<g/>
,	,	kIx,
vojenským	vojenský	k2eAgMnSc7d1
pedagogem	pedagog	k1gMnSc7
a	a	k8xC
reformátorem	reformátor	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založil	založit	k5eAaPmAgMnS
pražskou	pražský	k2eAgFnSc4d1
kadetní	kadetní	k2eAgFnSc4d1
školu	škola	k1gFnSc4
a	a	k8xC
zreformoval	zreformovat	k5eAaPmAgInS
vojenskou	vojenský	k2eAgFnSc4d1
akademii	akademie	k1gFnSc4
ve	v	k7c6
Vídeňském	vídeňský	k2eAgNnSc6d1
Novém	nový	k2eAgNnSc6d1
Městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1773	#num#	k4
<g/>
–	–	k?
<g/>
1774	#num#	k4
spoluzaložil	spoluzaložit	k5eAaPmAgInS
Soukromou	soukromý	k2eAgFnSc4d1
učenou	učený	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
pozdější	pozdní	k2eAgFnSc1d2
Královská	královský	k2eAgFnSc1d1
česká	český	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
nauk	nauka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
bohatou	bohatý	k2eAgFnSc4d1
knihovnu	knihovna	k1gFnSc4
odkázal	odkázat	k5eAaPmAgInS
Univerzitní	univerzitní	k2eAgInSc1d1
knihově	knihově	k6eAd1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
zasloužil	zasloužit	k5eAaPmAgMnS
se	se	k3xPyFc4
o	o	k7c4
Pražskou	pražský	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
a	a	k8xC
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
se	se	k3xPyFc4
hlásil	hlásit	k5eAaImAgMnS
ke	k	k7c3
Komenskému	Komenský	k1gMnSc3
a	a	k8xC
nazýval	nazývat	k5eAaImAgInS
jej	on	k3xPp3gNnSc4
„	„	k?
<g/>
prvním	první	k4xOgMnSc6
naším	náš	k3xOp1gInSc7
buditelem	buditel	k1gMnSc7
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
matce	matka	k1gFnSc6
zdědil	zdědit	k5eAaPmAgInS
a	a	k8xC
vlastnil	vlastnit	k5eAaImAgInS
do	do	k7c2
roku	rok	k1gInSc2
1783	#num#	k4
zámek	zámek	k1gInSc4
Radim	Radim	k1gMnSc1
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
bezdětný	bezdětný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
Leopold	Leopold	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
(	(	kIx(
<g/>
1713	#num#	k4
<g/>
–	–	k?
<g/>
1760	#num#	k4
<g/>
)	)	kIx)
zdědil	zdědit	k5eAaPmAgInS
Chlumec	Chlumec	k1gInSc1
a	a	k8xC
Matzen	Matzen	k2eAgInSc1d1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
nejvyšším	vysoký	k2eAgMnSc7d3
lovčím	lovčí	k1gMnSc7
v	v	k7c6
Českém	český	k2eAgNnSc6d1
království	království	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leopold	Leopold	k1gMnSc1
měl	mít	k5eAaImAgMnS
dvě	dva	k4xCgFnPc4
dcery	dcera	k1gFnPc4
a	a	k8xC
dva	dva	k4xCgMnPc4
syny	syn	k1gMnPc4
–	–	k?
Františka	František	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
a	a	k8xC
Filipa	Filip	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladší	mladý	k2eAgMnPc1d2
syn	syn	k1gMnSc1
Filip	Filip	k1gMnSc1
(	(	kIx(
<g/>
1742	#num#	k4
<g/>
–	–	k?
<g/>
1827	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
příbuznou	příbuzná	k1gFnSc7
z	z	k7c2
knížecí	knížecí	k2eAgFnSc2d1
větve	větev	k1gFnSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
získal	získat	k5eAaPmAgInS
Rychmburk	Rychmburk	k1gInSc1
a	a	k8xC
Chroustovice	Chroustovice	k1gFnSc1
<g/>
,	,	kIx,
mimoto	mimoto	k6eAd1
zdědil	zdědit	k5eAaPmAgInS
Sloup	sloup	k1gInSc1
a	a	k8xC
Zvíkovec	Zvíkovec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
prodal	prodat	k5eAaPmAgInS
prakticky	prakticky	k6eAd1
všechno	všechen	k3xTgNnSc4
dědictví	dědictví	k1gNnSc4
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	s	k7c7
zbrojmistrem	zbrojmistr	k1gMnSc7
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
bez	bez	k7c2
dědiců	dědic	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
bratr	bratr	k1gMnSc1
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
(	(	kIx(
<g/>
1738	#num#	k4
<g/>
–	–	k?
<g/>
1806	#num#	k4
<g/>
)	)	kIx)
měl	mít	k5eAaImAgInS
(	(	kIx(
<g/>
kromě	kromě	k7c2
dalších	další	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
<g/>
)	)	kIx)
tři	tři	k4xCgMnPc4
syny	syn	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
rozdělili	rozdělit	k5eAaPmAgMnP
tuto	tento	k3xDgFnSc4
větev	větev	k1gFnSc4
na	na	k7c4
tři	tři	k4xCgFnPc4
větve	větev	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
jimi	on	k3xPp3gMnPc7
Josef	Josef	k1gMnSc1
Leopold	Leopold	k1gMnSc1
(	(	kIx(
<g/>
1764	#num#	k4
<g/>
–	–	k?
<g/>
1831	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
(	(	kIx(
<g/>
1766	#num#	k4
<g/>
–	–	k?
<g/>
1831	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Kristián	Kristián	k1gMnSc1
(	(	kIx(
<g/>
1776	#num#	k4
<g/>
–	–	k?
<g/>
1835	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
byl	být	k5eAaImAgMnS
dědem	děd	k1gMnSc7
Berthy	Bertha	k1gFnSc2
von	von	k1gInSc1
Suttner	Suttner	k1gMnSc1
–	–	k?
první	první	k4xOgFnSc2
ženské	ženský	k2eAgFnSc2d1
nositelky	nositelka	k1gFnSc2
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
mír	mír	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jedním	jeden	k4xCgInSc7
z	z	k7c2
přímých	přímý	k2eAgMnPc2d1
potomků	potomek	k1gMnPc2
(	(	kIx(
<g/>
praprapravnukem	praprapravnuk	k1gMnSc7
<g/>
)	)	kIx)
Leopolda	Leopold	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
byl	být	k5eAaImAgInS
Zdenko	Zdenka	k1gFnSc5
Radslav	Radslav	k1gMnSc1
Kinský	Kinský	k1gMnSc1
(	(	kIx(
<g/>
1896	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
signatář	signatář	k1gMnSc1
všech	všecek	k3xTgFnPc2
tří	tři	k4xCgFnPc2
Deklarací	deklarace	k1gFnPc2
české	český	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
v	v	k7c6
letech	léto	k1gNnPc6
1938	#num#	k4
a	a	k8xC
1939	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Václav	Václav	k1gMnSc1
Norbert	Norbert	k1gMnSc1
(	(	kIx(
<g/>
1924	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
starší	starý	k2eAgMnSc1d2
syn	syn	k1gMnSc1
Zdeňka	Zdeněk	k1gMnSc2
Radslava	Radslav	k1gMnSc2
<g/>
,	,	kIx,
emigroval	emigrovat	k5eAaBmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
do	do	k7c2
Itálie	Itálie	k1gFnSc2
a	a	k8xC
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Annou	Anna	k1gFnSc7
Marií	Maria	k1gFnPc2
dal	dát	k5eAaPmAgInS
Borgo	Borgo	k6eAd1
Netolickou	netolický	k2eAgFnSc7d1
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
–	–	k?
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
rod	rod	k1gInSc1
získal	získat	k5eAaPmAgInS
majetek	majetek	k1gInSc4
jako	jako	k8xC,k8xS
např.	např.	kA
hrad	hrad	k1gInSc1
Kost	kost	k1gFnSc1
a	a	k8xC
Villu	Villa	k1gFnSc4
Dal	dát	k5eAaPmAgMnS
Borgo	Borgo	k1gMnSc1
Netolitzky	Netolitzka	k1gFnSc2
v	v	k7c6
Toskánsku	Toskánsko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
vyrůstala	vyrůstat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
její	její	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
profesním	profesní	k2eAgMnSc7d1
rytířem	rytíř	k1gMnSc7
Suverénního	suverénní	k2eAgInSc2d1
řádu	řád	k1gInSc2
maltézských	maltézský	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
<g/>
,	,	kIx,
následně	následně	k6eAd1
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
jeho	jeho	k3xOp3gMnSc7
ministrem	ministr	k1gMnSc7
zahraničí	zahraničí	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
letech	let	k1gInPc6
2002	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
byl	být	k5eAaImAgInS
českým	český	k2eAgMnSc7d1
velkopřevorem	velkopřevor	k1gMnSc7
maltézských	maltézský	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
Sametové	sametový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
zrestituoval	zrestituovat	k5eAaBmAgMnS,k5eAaPmAgMnS
mj.	mj.	kA
zámek	zámek	k1gInSc1
k	k	k7c3
v	v	k7c6
Chlumci	Chlumec	k1gInSc6
nad	nad	k7c7
Cidlinou	Cidlina	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
Žehuňskou	Žehuňský	k2eAgFnSc4d1
oboru	obora	k1gFnSc4
v	v	k7c6
Kněžičkách	kněžička	k1gFnPc6
s	s	k7c7
loveckým	lovecký	k2eAgInSc7d1
zámečkem	zámeček	k1gInSc7
Neugebau	Neugebaus	k1gInSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Hotel	hotel	k1gInSc1
Obora	obora	k1gFnSc1
Kinský	Kinský	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Majetek	majetek	k1gInSc4
zdědili	zdědit	k5eAaPmAgMnP
jeho	jeho	k3xOp3gMnPc1
dva	dva	k4xCgMnPc1
synové	syn	k1gMnPc1
<g/>
,	,	kIx,
Giovanni	Giovann	k1gMnPc1
(	(	kIx(
<g/>
*	*	kIx~
1949	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterému	který	k3yIgMnSc3,k3yRgMnSc3,k3yQgMnSc3
byl	být	k5eAaImAgInS
navíc	navíc	k6eAd1
jako	jako	k8xC,k8xS
dědictví	dědictví	k1gNnSc1
po	po	k7c6
matce	matka	k1gFnSc6
vrácen	vrátit	k5eAaPmNgInS
v	v	k7c6
restituci	restituce	k1gFnSc6
hrad	hrad	k1gInSc4
Kost	kost	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Pio	Pio	k1gMnPc1
Paolo	Paolo	k1gNnSc1
(	(	kIx(
<g/>
*	*	kIx~
1956	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
založili	založit	k5eAaPmAgMnP
společnost	společnost	k1gFnSc4
Kinský	Kinský	k2eAgMnSc1d1
dal	dát	k5eAaPmAgMnS
Borgo	Borgo	k6eAd1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
ke	k	k7c3
správě	správa	k1gFnSc3
rodinného	rodinný	k2eAgInSc2d1
majetku	majetek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
programovém	programový	k2eAgNnSc6d1
období	období	k1gNnSc6
EU	EU	kA
dotací	dotace	k1gFnPc2
IROP	IROP	kA
2014	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
získala	získat	k5eAaPmAgFnS
firma	firma	k1gFnSc1
dotace	dotace	k1gFnSc2
na	na	k7c4
opravu	oprava	k1gFnSc4
hradu	hrad	k1gInSc2
Kost	kost	k1gFnSc1
a	a	k8xC
zámku	zámek	k1gInSc6
Karlova	Karlův	k2eAgFnSc1d1
Koruna	koruna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opravy	oprava	k1gFnPc4
hradu	hrad	k1gInSc2
Kost	kost	k1gFnSc4
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
být	být	k5eAaImF
dokončeny	dokončit	k5eAaPmNgInP
v	v	k7c6
roce	rok	k1gInSc6
2021	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
opravy	oprava	k1gFnPc4
zámeckého	zámecký	k2eAgInSc2d1
areálu	areál	k1gInSc2
Karlova	Karlův	k2eAgFnSc1d1
Koruna	koruna	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2022	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mladší	mladý	k2eAgMnSc1d2
syn	syn	k1gMnSc1
Zdeňka	Zdeněk	k1gMnSc4
Radslava	Radslav	k1gMnSc4
<g/>
,	,	kIx,
Radslav	Radslav	k1gMnSc1
Kinský	Kinský	k1gMnSc1
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
významný	významný	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
imunolog	imunolog	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
reprodukční	reprodukční	k2eAgFnSc2d1
imunologie	imunologie	k1gFnSc2
a	a	k8xC
třetí	třetí	k4xOgMnSc1
český	český	k2eAgMnSc1d1
velkopřevor	velkopřevor	k1gMnSc1
lazariánů	lazarián	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1991	#num#	k4
zrestituoval	zrestituovat	k5eAaBmAgInS,k5eAaPmAgInS
někdejší	někdejší	k2eAgInSc1d1
klášter	klášter	k1gInSc1
a	a	k8xC
zámek	zámek	k1gInSc1
ve	v	k7c6
Žďáru	Žďár	k1gInSc6
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
syn	syn	k1gMnSc1
Constantin	Constantin	k1gMnSc1
Kinský	Kinský	k1gMnSc1
s	s	k7c7
manželkou	manželka	k1gFnSc7
Marií	Maria	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
v	v	k7c6
areálu	areál	k1gInSc6
někdejšího	někdejší	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
založili	založit	k5eAaPmAgMnP
Muzeum	muzeum	k1gNnSc4
nové	nový	k2eAgFnSc2d1
generace	generace	k1gFnSc2
<g/>
,	,	kIx,
Marie	Maria	k1gFnSc2
mimo	mimo	k7c4
to	ten	k3xDgNnSc4
od	od	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
je	být	k5eAaImIp3nS
organizátorkou	organizátorka	k1gFnSc7
a	a	k8xC
zakladatelkou	zakladatelka	k1gFnSc7
festivalu	festival	k1gInSc2
současného	současný	k2eAgInSc2d1
tance	tanec	k1gInSc2
a	a	k8xC
pohybového	pohybový	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
KoresponPance	KoresponPance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
tyto	tento	k3xDgFnPc4
činnosti	činnost	k1gFnPc4
obdrželi	obdržet	k5eAaPmAgMnP
společně	společně	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
skleněnou	skleněný	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
Kraje	kraj	k1gInSc2
Vysočina	vysočina	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rodokmen	rodokmen	k1gInSc1
chlumecké	chlumecký	k2eAgFnSc2d1
větve	větev	k1gFnSc2
Kinských	Kinských	k2eAgFnSc2d1
</s>
<s>
Držitelé	držitel	k1gMnPc1
chlumeckého	chlumecký	k2eAgNnSc2d1
panství	panství	k1gNnSc2
jsou	být	k5eAaImIp3nP
uvedeni	uveden	k2eAgMnPc1d1
tučně	tučně	k6eAd1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Kinský	Kinský	k1gMnSc1
(	(	kIx(
<g/>
1678	#num#	k4
<g/>
–	–	k?
<g/>
1741	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Václav	Václav	k1gMnSc1
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1707	#num#	k4
–	–	k?
1708	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
František	František	k1gMnSc1
Václav	Václav	k1gMnSc1
Kajetán	Kajetán	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1708	#num#	k4
–	–	k?
1711	#num#	k4
nebo	nebo	k8xC
3	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1708	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
1710	#num#	k4
nebo	nebo	k8xC
1	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1711	#num#	k4
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1717	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
Jan	Jan	k1gMnSc1
<g/>
)	)	kIx)
Leopold	Leopold	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1713	#num#	k4
–	–	k?
1761	#num#	k4
nebo	nebo	k8xC
24	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1760	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1734	#num#	k4
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
Marie	Marie	k1gFnSc1
Tereza	Tereza	k1gFnSc1
Capece	Capece	k1gFnSc1
dei	dei	k?
Marchesi	Marchese	k1gFnSc4
di	di	k?
Rofrano	Rofrana	k1gFnSc5
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1715	#num#	k4
–	–	k?
12	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1778	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnPc1
děti	dítě	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
(	(	kIx(
<g/>
Jeroným	Jeroným	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1738	#num#	k4
Vídeň	Vídeň	k1gFnSc1
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1806	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
éře	éra	k1gFnSc6
proběhlo	proběhnout	k5eAaPmAgNnS
selské	selské	k1gNnSc1
povstání	povstání	k1gNnSc2
v	v	k7c6
Chlumci	Chlumec	k1gInSc6
nad	nad	k7c7
Cidlinou	Cidlina	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
1775	#num#	k4
</s>
<s>
∞	∞	k?
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1761	#num#	k4
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
Marie	Marie	k1gFnSc1
Kristýna	Kristýna	k1gFnSc1
z	z	k7c2
Lichtenštejna	Lichtenštejn	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1741	#num#	k4
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1819	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnPc1
děti	dítě	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Josef	Josef	k1gMnSc1
Leopold	Leopold	k1gMnSc1
(	(	kIx(
<g/>
29	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1764	#num#	k4
Vídeň	Vídeň	k1gFnSc1
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1831	#num#	k4
Chlumec	Chlumec	k1gInSc1
nad	nad	k7c7
Cidlinou	Cidlina	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc1
<g/>
,	,	kIx,
rozená	rozený	k2eAgFnSc1d1
svobodná	svobodný	k2eAgFnSc1d1
paní	paní	k1gFnSc1
z	z	k7c2
Puteani	Puteaň	k1gFnSc6
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnPc1
děti	dítě	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Oktavián	Oktavián	k1gMnSc1
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1813	#num#	k4
<g/>
,	,	kIx,
zámek	zámek	k1gInSc1
Vlkov	Vlkov	k1gInSc1
–	–	k?
28	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1896	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
iniciátor	iniciátor	k1gMnSc1
Velké	velká	k1gFnSc2
pardubické	pardubický	k2eAgFnSc2d1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
chovu	chov	k1gInSc2
koní	koní	k2eAgInSc4d1
Equus	Equus	k1gInSc4
Kinsky	Kinska	k1gFnSc2
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
bezdětný	bezdětný	k2eAgMnSc1d1
<g/>
,	,	kIx,
panství	panství	k1gNnSc1
i	i	k8xC
chov	chov	k1gInSc1
koní	kůň	k1gMnPc2
předal	předat	k5eAaPmAgInS
synovi	syn	k1gMnSc3
svého	svůj	k3xOyFgMnSc4
bratra	bratr	k1gMnSc4
Jana	Jan	k1gMnSc4
Zdenkovi	Zdenkovi	k?
</s>
<s>
∞	∞	k?
?	?	kIx.
</s>
<s desamb="1">
<g/>
Anežka	Anežka	k1gFnSc1
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
–	–	k?
5	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1888	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bez	bez	k7c2
dětí	dítě	k1gFnPc2
</s>
<s>
∞	∞	k?
Marie	Marie	k1gFnSc1
Stubenvollová	Stubenvollová	k1gFnSc1
<g/>
,	,	kIx,
cirkusová	cirkusový	k2eAgFnSc1d1
krasojezdkyně	krasojezdkyně	k1gFnSc1
(	(	kIx(
<g/>
1860	#num#	k4
<g/>
–	–	k?
<g/>
1909	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bez	bez	k7c2
dětí	dítě	k1gFnPc2
</s>
<s>
Jan	Jan	k1gMnSc1
(	(	kIx(
<g/>
1815	#num#	k4
–	–	k?
1868	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Zdenko	Zdenka	k1gFnSc5
(	(	kIx(
<g/>
1844	#num#	k4
–	–	k?
1932	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
Georgina	Georgina	k1gFnSc1
Ernestina	Ernestina	k1gFnSc1
Festeticsová	Festeticsová	k1gFnSc1
z	z	k7c2
Tolny	Tolna	k1gFnSc2
(	(	kIx(
<g/>
1856	#num#	k4
<g/>
–	–	k?
<g/>
1934	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnPc1
děti	dítě	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Nora	Nora	k1gFnSc1
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1888	#num#	k4
Vídeň	Vídeň	k1gFnSc1
–	–	k?
27	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1923	#num#	k4
Vítkovice	Vítkovice	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
hrabě	hrabě	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Wilczek	Wilczek	k1gInSc4
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gNnSc4
dítě	dítě	k1gNnSc4
<g/>
:	:	kIx,
</s>
<s>
Georgina	Georgina	k1gFnSc1
<g/>
,	,	kIx,
pozdější	pozdní	k2eAgFnSc1d2
kněžna	kněžna	k1gFnSc1
lichtenštejnská	lichtenštejnský	k2eAgFnSc1d1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
(	(	kIx(
<g/>
též	též	k9
Zdenko	Zdenka	k1gFnSc5
<g/>
)	)	kIx)
Radslav	Radslav	k1gMnSc1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1896	#num#	k4
Chlumec	Chlumec	k1gInSc1
nad	nad	k7c7
Cidlinou	Cidlina	k1gFnSc7
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1975	#num#	k4
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
Eleonora	Eleonora	k1gFnSc1
Schwarzenbergová	Schwarzenbergový	k2eAgFnSc1d1
<g/>
,	,	kIx,
rozená	rozený	k2eAgFnSc1d1
Clam-Gallasová	Clam-Gallasová	k1gFnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1887	#num#	k4
Frýdlant	Frýdlant	k1gInSc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1967	#num#	k4
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnPc1
děti	dítě	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Václav	Václav	k1gMnSc1
Norbert	Norbert	k1gMnSc1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1924	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
Pugnano	Pugnana	k1gFnSc5
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
Anna	Anna	k1gFnSc1
Marie	Maria	k1gFnSc2
dal	dát	k5eAaPmAgMnS
Borgo-Netolická	Borgo-Netolický	k2eAgFnSc1d1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1925	#num#	k4
Pisa	Pisa	k1gFnSc1
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1980	#num#	k4
Pugnano	Pugnana	k1gFnSc5
<g/>
))	))	k?
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnPc1
děti	dítě	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Giovanni-Zdenko	Giovanni-Zdenka	k1gFnSc5
Kinský	Kinský	k2eAgInSc1d1
dal	dát	k5eAaPmAgInS
Borgo	Borgo	k6eAd1
(	(	kIx(
<g/>
Jan	Jan	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
<g/>
,	,	kIx,
*	*	kIx~
12	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1949	#num#	k4
Pisa	Pisa	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
(	(	kIx(
<g/>
30	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1976	#num#	k4
Pisa	Pisa	k1gFnSc1
<g/>
)	)	kIx)
Michelle	Michelle	k1gNnSc1
Hoskins	Hoskinsa	k1gFnPc2
(	(	kIx(
<g/>
28	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1956	#num#	k4
Shreveport	Shreveport	k1gInSc1
<g/>
,	,	kIx,
Louisiana	Louisiana	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnPc1
děti	dítě	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eleonora	Eleonora	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
30	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1979	#num#	k4
Pisa	Pisa	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francesco	Francesco	k1gMnSc1
(	(	kIx(
<g/>
František	František	k1gMnSc1
<g/>
,	,	kIx,
*	*	kIx~
26	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1983	#num#	k4
Pisa	Pisa	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
)	)	kIx)
Jana	Jana	k1gFnSc1
Hůlková	Hůlková	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
6	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1979	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pio	Pio	k1gMnSc1
Paolo	Paolo	k1gNnSc1
Kinský	Kinský	k1gMnSc1
dal	dát	k5eAaPmAgMnS
Borgo	Borgo	k1gMnSc1
(	(	kIx(
<g/>
Pius	Pius	k1gMnSc1
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
*	*	kIx~
23	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1956	#num#	k4
Pisa	Pisa	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1985	#num#	k4
Pugnano	Pugnana	k1gFnSc5
<g/>
)	)	kIx)
Natalia	Natalius	k1gMnSc2
Guidi	Guid	k1gMnPc1
(	(	kIx(
<g/>
*	*	kIx~
10	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1951	#num#	k4
Lari	lari	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnPc1
děti	dítě	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carlo	Carlo	k1gNnSc1
(	(	kIx(
<g/>
Karel	Karel	k1gMnSc1
<g/>
,	,	kIx,
*	*	kIx~
6	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1987	#num#	k4
Pisa	Pisa	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Federico	Federico	k1gMnSc1
(	(	kIx(
<g/>
Bedřich	Bedřich	k1gMnSc1
<g/>
,	,	kIx,
*	*	kIx~
22	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1988	#num#	k4
Pisa	Pisa	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anna	Anna	k1gFnSc1
Maria	Maria	k1gFnSc1
(	(	kIx(
<g/>
Anna	Anna	k1gFnSc1
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
*	*	kIx~
6	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1992	#num#	k4
Pisa	Pisa	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Radslav	Radslav	k1gMnSc1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1928	#num#	k4
Obora	obora	k1gFnSc1
Kněžičky	kněžička	k1gFnSc2
u	u	k7c2
Chlumce	Chlumec	k1gInSc2
nad	nad	k7c7
Cidlinou	Cidlina	k1gFnSc7
–	–	k?
12	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
Žďár	Žďár	k1gInSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
Marie	Marie	k1gFnSc1
Anna	Anna	k1gFnSc1
(	(	kIx(
<g/>
29	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1765	#num#	k4
–	–	k?
22	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1837	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
(	(	kIx(
<g/>
28	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1766	#num#	k4
Chlumec	Chlumec	k1gInSc1
nad	nad	k7c7
Cidlinou	Cidlina	k1gFnSc7
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1831	#num#	k4
Svojkov	Svojkov	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
sloupské	sloupský	k2eAgFnSc2d1
větve	větev	k1gFnSc2
rodu	rod	k1gInSc2
</s>
<s>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1768	#num#	k4
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1843	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
Berthy	Bertha	k1gFnSc2
von	von	k1gInSc1
Suttner	Suttner	k1gMnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
(	(	kIx(
<g/>
1770	#num#	k4
–	–	k?
1776	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
Prokop	Prokop	k1gMnSc1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1772	#num#	k4
–	–	k?
1843	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marie	Marie	k1gFnSc1
Kristina	Kristina	k1gFnSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1773	#num#	k4
-	-	kIx~
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
</s>
<s>
Kristián	Kristián	k1gMnSc1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1776	#num#	k4
Vídeň	Vídeň	k1gFnSc1
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1835	#num#	k4
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
matzenské	matzenský	k2eAgFnSc2d1
větve	větev	k1gFnSc2
rodu	rod	k1gInSc2
</s>
<s>
Antonín	Antonín	k1gMnSc1
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1774	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1864	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marie	Marie	k1gFnSc1
Barbora	Barbora	k1gFnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1775	#num#	k4
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1798	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Filip	Filip	k1gMnSc1
František	František	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1777	#num#	k4
–	–	k?
1794	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
1778	#num#	k4
–	–	k?
1798	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1740	#num#	k4
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1806	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1761	#num#	k4
<g/>
)	)	kIx)
Andrzej	Andrzej	k1gMnSc1
Poniatowski	Poniatowsk	k1gFnSc2
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1735	#num#	k4
–	–	k?
5	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1773	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Filip	Filip	k1gMnSc1
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1741	#num#	k4
–	–	k?
14	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1827	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rakouský	rakouský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
</s>
<s>
∞	∞	k?
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
/	/	kIx~
13	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
/	/	kIx~
10	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1787	#num#	k4
Vídeň	Vídeň	k1gFnSc1
<g/>
,	,	kIx,
rozvedeni	rozveden	k2eAgMnPc1d1
1788	#num#	k4
<g/>
)	)	kIx)
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
z	z	k7c2
Ditrichštejna	Ditrichštejn	k1gInSc2
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1768	#num#	k4
–	–	k?
16	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1822	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marie	Marie	k1gFnSc1
Anna	Anna	k1gFnSc1
(	(	kIx(
<g/>
pokřtěna	pokřtěn	k2eAgFnSc1d1
18	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1744	#num#	k4
Vídeň	Vídeň	k1gFnSc1
–	–	k?
13	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1829	#num#	k4
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1763	#num#	k4
<g/>
)	)	kIx)
Václav	Václav	k1gMnSc1
Arnošt	Arnošt	k1gMnSc1
Schaffgotsch	Schaffgotsch	k1gMnSc1
z	z	k7c2
Kynastu	Kynast	k1gInSc2
a	a	k8xC
Greiffensteinu	Greiffenstein	k1gInSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1733	#num#	k4
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1764	#num#	k4
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1731	#num#	k4
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1804	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marie	Marie	k1gFnSc1
Anna	Anna	k1gFnSc1
(	(	kIx(
<g/>
1736	#num#	k4
–	–	k?
květen	květen	k1gInSc1
1752	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marie	Marie	k1gFnSc1
Josefa	Josef	k1gMnSc2
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1738	#num#	k4
–	–	k?
1767	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
(	(	kIx(
<g/>
1766	#num#	k4
<g/>
)	)	kIx)
Maxmilián	Maxmilián	k1gMnSc1
František	František	k1gMnSc1
Xaver	Xaver	k1gMnSc1
Daun	Daun	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1721	#num#	k4
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1788	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1737	#num#	k4
/	/	kIx~
6	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1739	#num#	k4
Praha	Praha	k1gFnSc1
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1805	#num#	k4
Vídeň	Vídeň	k1gFnSc1
<g/>
,	,	kIx,
pohřben	pohřben	k2eAgInSc1d1
ve	v	k7c6
Vídeňském	vídeňský	k2eAgNnSc6d1
Novém	nový	k2eAgNnSc6d1
Městě	město	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rakouský	rakouský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
</s>
<s>
∞	∞	k?
(	(	kIx(
<g/>
29	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1779	#num#	k4
<g/>
)	)	kIx)
Marie	Marie	k1gFnSc1
Renata	Renata	k1gFnSc1
z	z	k7c2
Trauttmansdorffu	Trauttmansdorff	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1741	#num#	k4
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1808	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marie	Marie	k1gFnSc1
Antonie	Antonie	k1gFnSc2
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1739	#num#	k4
–	–	k?
28	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1816	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1755	#num#	k4
<g/>
)	)	kIx)
František	František	k1gMnSc1
de	de	k?
Paula	Paul	k1gMnSc4
Adam	Adam	k1gMnSc1
Wratislaw	Wratislaw	k1gMnSc1
z	z	k7c2
Mitrowicz	Mitrowicz	k1gMnSc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1732	#num#	k4
–	–	k?
19	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1788	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Vedlejší	vedlejší	k2eAgFnPc1d1
a	a	k8xC
vymřelé	vymřelý	k2eAgFnPc1d1
větve	větev	k1gFnPc1
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
po	po	k7c6
roce	rok	k1gInSc6
1350	#num#	k4
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1350	#num#	k4
se	se	k3xPyFc4
rod	rod	k1gInSc1
Vchynských	Vchynská	k1gFnPc2
rozpadl	rozpadnout	k5eAaPmAgInS
na	na	k7c4
několik	několik	k4yIc4
částí	část	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
některé	některý	k3yIgInPc1
zcela	zcela	k6eAd1
ztratily	ztratit	k5eAaPmAgInP
význam	význam	k1gInSc4
a	a	k8xC
staly	stát	k5eAaPmAgFnP
se	se	k3xPyFc4
tak	tak	k9
nevysledovatelnými	vysledovatelný	k2eNgInPc7d1
<g/>
,	,	kIx,
některé	některý	k3yIgInPc4
si	se	k3xPyFc3
však	však	k9
podržely	podržet	k5eAaPmAgFnP
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
určitý	určitý	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Medvědičtí	Medvědický	k2eAgMnPc1d1
z	z	k7c2
Medvědíče	Medvědíč	k1gFnSc2
</s>
<s>
Někdy	někdy	k6eAd1
též	též	k9
Nedvědíč	Nedvědíč	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátek	začátek	k1gInSc4
této	tento	k3xDgFnSc2
větve	větev	k1gFnSc2
je	být	k5eAaImIp3nS
dost	dost	k6eAd1
nejistý	jistý	k2eNgInSc1d1
<g/>
,	,	kIx,
všeobecně	všeobecně	k6eAd1
sem	sem	k6eAd1
bývá	bývat	k5eAaImIp3nS
řazen	řazen	k2eAgInSc1d1
Chotibor	Chotibor	k1gInSc1
(	(	kIx(
<g/>
†	†	k?
1346	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
však	však	k9
bývá	bývat	k5eAaImIp3nS
počátek	počátek	k1gInSc4
posunut	posunout	k5eAaPmNgInS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1393	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
v	v	k7c6
této	tento	k3xDgFnSc6
větvi	větev	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
poslední	poslední	k2eAgMnSc1d1
mužský	mužský	k2eAgMnSc1d1
potomek	potomek	k1gMnSc1
Hynek	Hynek	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
roku	rok	k1gInSc2
1437	#num#	k4
bezdětný	bezdětný	k2eAgInSc1d1
<g/>
,	,	kIx,
přešly	přejít	k5eAaPmAgFnP
Medvědice	medvědice	k1gFnPc1
přes	přes	k7c4
dceru	dcera	k1gFnSc4
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
bratra	bratr	k1gMnSc2
Zikmunda	Zikmund	k1gMnSc2
(	(	kIx(
<g/>
†	†	k?
1432	#num#	k4
<g/>
)	)	kIx)
do	do	k7c2
vlastnictví	vlastnictví	k1gNnSc2
jejího	její	k3xOp3gMnSc2
manžela	manžel	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
zakladatelem	zakladatel	k1gMnSc7
Nedvědických	Nedvědický	k2eAgFnPc2d1
z	z	k7c2
Račiněvsi	Račiněvs	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Razičtí	Razičtět	k5eAaPmIp3nS
ze	z	k7c2
Vchynic	Vchynice	k1gFnPc2
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
Razic	Razice	k1gFnPc2
u	u	k7c2
Bíliny	Bílina	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
vlastnilo	vlastnit	k5eAaImAgNnS
několik	několik	k4yIc1
bratrů	bratr	k1gMnPc2
<g/>
,	,	kIx,
dalším	další	k2eAgInSc7d1
vývojem	vývoj	k1gInSc7
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
Razice	Razice	k1gFnSc1
a	a	k8xC
podle	podle	k7c2
ekonomické	ekonomický	k2eAgFnSc2d1
situace	situace	k1gFnSc2
i	i	k9
několik	několik	k4yIc1
okolních	okolní	k2eAgFnPc2d1
vesnic	vesnice	k1gFnPc2
drželi	držet	k5eAaImAgMnP
jejich	jejich	k3xOp3gMnPc1
potomci	potomek	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádný	žádný	k1gMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
nepřesáhl	přesáhnout	k5eNaPmAgMnS
regionální	regionální	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
prodeji	prodej	k1gInSc3
Razic	Razice	k1gFnPc2
<g/>
,	,	kIx,
poslední	poslední	k2eAgMnSc1d1
potomek	potomek	k1gMnSc1
této	tento	k3xDgFnSc2
větve	větev	k1gFnSc2
zemřel	zemřít	k5eAaPmAgMnS
roku	rok	k1gInSc2
1612	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Žluničtí	Žlunický	k2eAgMnPc1d1
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
větev	větev	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
částečně	částečně	k6eAd1
provázána	provázat	k5eAaPmNgFnS
s	s	k7c7
Razickými	Razická	k1gFnPc7
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yIgFnPc2,k3yQgFnPc2,k3yRgFnPc2
možná	možná	k9
i	i	k9
vzešla	vzejít	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
této	tento	k3xDgFnSc6
větvi	větev	k1gFnSc6
se	se	k3xPyFc4
toho	ten	k3xDgNnSc2
ví	vědět	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
málo	málo	k1gNnSc1
<g/>
,	,	kIx,
jisté	jistý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
roku	rok	k1gInSc2
1412	#num#	k4
vlastnil	vlastnit	k5eAaImAgMnS
Žlunice	Žlunice	k1gFnSc1
a	a	k8xC
že	že	k8xS
zemřel	zemřít	k5eAaPmAgMnS
roku	rok	k1gInSc2
1415	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
jeho	jeho	k3xOp3gMnPc2
mužských	mužský	k2eAgMnPc2d1
potomků	potomek	k1gMnPc2
již	již	k9
nikdo	nikdo	k3yNnSc1
neměl	mít	k5eNaImAgMnS
syna	syn	k1gMnSc4
a	a	k8xC
tak	tak	k9
po	po	k7c6
smrti	smrt	k1gFnSc6
Jindřicha	Jindřich	k1gMnSc4
(	(	kIx(
<g/>
1455	#num#	k4
<g/>
)	)	kIx)
tato	tento	k3xDgFnSc1
větev	větev	k1gFnSc1
zanikla	zaniknout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Ohničtí	Ohnický	k2eAgMnPc1d1
a	a	k8xC
Kremyžští	Kremyžský	k2eAgMnPc1d1
</s>
<s>
Se	s	k7c7
oddělili	oddělit	k5eAaPmAgMnP
koncem	koncem	k7c2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
větev	větev	k1gFnSc4
založil	založit	k5eAaPmAgMnS
Litolt	Litolt	k1gMnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
synové	syn	k1gMnPc1
byli	být	k5eAaImAgMnP
Dobš	Dobš	k1gMnSc1
a	a	k8xC
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
roku	rok	k1gInSc2
1428	#num#	k4
purkrabím	purkrabí	k1gMnSc7
v	v	k7c6
Bílině	Bílina	k1gFnSc6
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
byl	být	k5eAaImAgMnS
finančně	finančně	k6eAd1
úspěšný	úspěšný	k2eAgMnSc1d1
koupil	koupit	k5eAaPmAgMnS
1440	#num#	k4
Bříšťany	Bříšťan	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větev	větev	k1gFnSc1
se	se	k3xPyFc4
sice	sice	k8xC
nerozdělila	rozdělit	k5eNaPmAgFnS
<g/>
,	,	kIx,
protože	protože	k8xS
Janův	Janův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Bohuslav	Bohuslav	k1gMnSc1
dědil	dědit	k5eAaImAgMnS
po	po	k7c6
obou	dva	k4xCgFnPc6
bratrech	bratr	k1gMnPc6
(	(	kIx(
<g/>
1441	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
nebyl	být	k5eNaImAgMnS
finančně	finančně	k6eAd1
úspěšný	úspěšný	k2eAgInSc4d1
a	a	k8xC
roku	rok	k1gInSc2
1455	#num#	k4
prodal	prodat	k5eAaPmAgInS
Ohníč	Ohníč	k1gInSc1
a	a	k8xC
následně	následně	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
roku	rok	k1gInSc3
1460	#num#	k4
i	i	k9
Bříšťany	Bříšťan	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
tato	tento	k3xDgFnSc1
větev	větev	k1gFnSc4
končí	končit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
po	po	k7c6
roce	rok	k1gInSc6
1410	#num#	k4
</s>
<s>
Měruničtí	Měrunický	k2eAgMnPc1d1
</s>
<s>
Někdy	někdy	k6eAd1
po	po	k7c6
roce	rok	k1gInSc6
1410	#num#	k4
spravoval	spravovat	k5eAaImAgInS
Měrunice	Měrunice	k1gFnSc2
Smilův	Smilův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Martin	Martin	k1gMnSc1
<g/>
,	,	kIx,
nejpravděpodobněji	pravděpodobně	k6eAd3
jeho	jeho	k3xOp3gMnPc1
potomci	potomek	k1gMnPc1
založili	založit	k5eAaPmAgMnP
na	na	k7c6
držbě	držba	k1gFnSc6
větší	veliký	k2eAgFnSc2d2
části	část	k1gFnSc2
vesnice	vesnice	k1gFnSc2
samostatnou	samostatný	k2eAgFnSc4d1
větev	větev	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
větev	větev	k1gFnSc4
nebyla	být	k5eNaImAgFnS
nijak	nijak	k6eAd1
významná	významný	k2eAgFnSc1d1
a	a	k8xC
není	být	k5eNaImIp3nS
ani	ani	k8xC
valně	valně	k6eAd1
zdokumentována	zdokumentován	k2eAgFnSc1d1
<g/>
,	,	kIx,
jisté	jistý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
počátku	počátek	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
získali	získat	k5eAaPmAgMnP
několik	několik	k4yIc4
dalších	další	k2eAgInPc2d1
statků	statek	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
však	však	k9
v	v	k7c6
letech	let	k1gInPc6
1530	#num#	k4
<g/>
–	–	k?
<g/>
1560	#num#	k4
prodali	prodat	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgMnPc1d1
známí	známý	k2eAgMnPc1d1
potomci	potomek	k1gMnPc1
této	tento	k3xDgFnSc2
větve	větev	k1gFnSc2
žili	žít	k5eAaImAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1594	#num#	k4
v	v	k7c6
Nehvízdkách	Nehvízdka	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
patrně	patrně	k6eAd1
ještě	ještě	k6eAd1
koncem	koncem	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vymřeli	vymřít	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Synové	syn	k1gMnPc1
Jana	Jan	k1gMnSc2
Dlaska	Dlask	k1gMnSc2
</s>
<s>
Drastská	Drastský	k2eAgFnSc1d1
větev	větev	k1gFnSc1
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
J.	J.	kA
Dlaska	Dlask	k1gMnSc2
Jiřík	Jiřík	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
1566	#num#	k4
<g/>
)	)	kIx)
založil	založit	k5eAaPmAgMnS
tzv.	tzv.	kA
drastskou	drastský	k2eAgFnSc4d1
větev	větev	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiřík	Jiřík	k1gMnSc1
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Dorotou	Dorota	k1gFnSc7
Sekerkovou	sekerkový	k2eAgFnSc7d1
ze	z	k7c2
Sedčic	Sedčice	k1gFnPc2
a	a	k8xC
zhruba	zhruba	k6eAd1
roku	rok	k1gInSc2
1560	#num#	k4
koupil	koupit	k5eAaPmAgMnS
Drasty	drasta	k1gFnPc4
a	a	k8xC
část	část	k1gFnSc4
Klecan	Klecana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
syn	syn	k1gMnSc1
Adam	Adam	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
1571	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
nechal	nechat	k5eAaPmAgInS
vyplatit	vyplatit	k5eAaPmF
starším	starý	k2eAgMnSc7d2
synem	syn	k1gMnSc7
Jaroslavem	Jaroslav	k1gMnSc7
(	(	kIx(
<g/>
†	†	k?
1614	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
bezdětný	bezdětný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslav	Jaroslava	k1gFnPc2
dlouhodobě	dlouhodobě	k6eAd1
(	(	kIx(
<g/>
1589	#num#	k4
<g/>
–	–	k?
<g/>
1611	#num#	k4
<g/>
)	)	kIx)
působil	působit	k5eAaImAgMnS
jako	jako	k9
místokomorník	místokomorník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
byl	být	k5eAaImAgInS
finančně	finančně	k6eAd1
relativně	relativně	k6eAd1
úspěšný	úspěšný	k2eAgInSc4d1
a	a	k8xC
roku	rok	k1gInSc2
1589	#num#	k4
koupil	koupit	k5eAaPmAgInS
Krakovec	krakovec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
Jaroslava	Jaroslav	k1gMnSc2
Jiří	Jiří	k1gMnSc1
získal	získat	k5eAaPmAgMnS
Blšany	Blšana	k1gFnPc4
a	a	k8xC
Krakovec	krakovec	k1gInSc4
<g/>
,	,	kIx,
po	po	k7c6
bitvě	bitva	k1gFnSc6
na	na	k7c6
Bílé	bílý	k2eAgFnSc6d1
hoře	hora	k1gFnSc6
mu	on	k3xPp3gMnSc3
byly	být	k5eAaImAgInP
sice	sice	k8xC
zabaveny	zabaven	k2eAgInPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
již	již	k9
roku	rok	k1gInSc2
1623	#num#	k4
je	být	k5eAaImIp3nS
jako	jako	k9
zadlužené	zadlužený	k2eAgNnSc1d1
získal	získat	k5eAaPmAgMnS
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc1
druhý	druhý	k4xOgMnSc1
syn	syn	k1gMnSc1
Adam	Adam	k1gMnSc1
vlastnil	vlastnit	k5eAaImAgMnS
Košťálov	Košťálov	k1gInSc4
a	a	k8xC
Zhoř	Zhoř	k1gInSc4
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k9
mu	on	k3xPp3gMnSc3
byly	být	k5eAaImAgFnP
zabrány	zabrána	k1gFnPc1
<g/>
,	,	kIx,
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
dánské	dánský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
(	(	kIx(
<g/>
1628	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Petr	Petr	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
1669	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
generálem	generál	k1gMnSc7
švédské	švédský	k2eAgFnSc2d1
jízdy	jízda	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
ani	ani	k8xC
on	on	k3xPp3gMnSc1
ani	ani	k8xC
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
1685	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
nedomohli	domoct	k5eNaPmAgMnP
navrácení	navrácení	k1gNnSc4
zabaveného	zabavený	k2eAgInSc2d1
majetku	majetek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgMnSc1
syn	syn	k1gMnSc1
Jan	Jan	k1gMnSc1
Bedřich	Bedřich	k1gMnSc1
byl	být	k5eAaImAgMnS
politicky	politicky	k6eAd1
aktivní	aktivní	k2eAgMnSc1d1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1620	#num#	k4
byl	být	k5eAaImAgInS
odsouzen	odsoudit	k5eAaPmNgInS,k5eAaImNgInS
ke	k	k7c3
ztrátě	ztráta	k1gFnSc3
majetku	majetek	k1gInSc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
sice	sice	k8xC
podědil	podědit	k5eAaPmAgInS
majetek	majetek	k1gInSc4
po	po	k7c6
ženě	žena	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
odkázal	odkázat	k5eAaPmAgInS
ho	on	k3xPp3gMnSc4
své	svůj	k3xOyFgFnSc3
druhé	druhý	k4xOgFnSc3
ženě	žena	k1gFnSc3
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
majetek	majetek	k1gInSc1
dostal	dostat	k5eAaPmAgInS
mimo	mimo	k7c4
rod	rod	k1gInSc4
Kinských	Kinských	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1
bratr	bratr	k1gMnSc1
Radslav	Radslav	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
1644	#num#	k4
<g/>
)	)	kIx)
měl	mít	k5eAaImAgMnS
dvě	dva	k4xCgFnPc4
dcery	dcera	k1gFnPc4
a	a	k8xC
dva	dva	k4xCgInPc4
syny	syn	k1gMnPc7
Bedřicha	Bedřich	k1gMnSc2
Jaroslava	Jaroslav	k1gMnSc2
(	(	kIx(
<g/>
†	†	k?
1653	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Petra	Petr	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
synem	syn	k1gMnSc7
Františkem	František	k1gMnSc7
Antonínem	Antonín	k1gMnSc7
tato	tento	k3xDgFnSc1
větev	větev	k1gFnSc4
vymřela	vymřít	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Nizozemská	nizozemský	k2eAgFnSc1d1
větev	větev	k1gFnSc1
</s>
<s>
Tuto	tento	k3xDgFnSc4
větev	větev	k1gFnSc4
založil	založit	k5eAaPmAgMnS
Kryštof	Kryštof	k1gMnSc1
Dlask	Dlask	k1gMnSc1
ze	z	k7c2
Vchynic	Vchynice	k1gFnPc2
(	(	kIx(
<g/>
†	†	k?
1555	#num#	k4
<g/>
)	)	kIx)
–	–	k?
nejmladší	mladý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
Jana	Jan	k1gMnSc2
Dlaska	Dlask	k1gMnSc2
(	(	kIx(
<g/>
†	†	k?
1521	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
zcela	zcela	k6eAd1
odtrhl	odtrhnout	k5eAaPmAgInS
od	od	k7c2
rodu	rod	k1gInSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnPc1
dva	dva	k4xCgMnPc1
synové	syn	k1gMnPc1
Burian	Burian	k1gMnSc1
starší	starší	k1gMnSc1
a	a	k8xC
Ferdinand	Ferdinand	k1gMnSc1
Kryštof	Kryštof	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
<g/>
1617	#num#	k4
<g/>
)	)	kIx)
emigrovali	emigrovat	k5eAaBmAgMnP
do	do	k7c2
Nizozemí	Nizozemí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Burkart	Burkart	k1gInSc1
(	(	kIx(
<g/>
†	†	k?
<g/>
1595	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
Ferdinanda	Ferdinand	k1gMnSc2
Kryštofa	Kryštof	k1gMnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
předkem	předek	k1gInSc7
všech	všecek	k3xTgMnPc2
Vchynských	Vchynský	k1gMnPc2
dále	daleko	k6eAd2
žijících	žijící	k2eAgMnPc2d1
v	v	k7c6
Belgii	Belgie	k1gFnSc6
a	a	k8xC
Nizozemí	Nizozemí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc1
potomci	potomek	k1gMnPc1
se	se	k3xPyFc4
pokoušeli	pokoušet	k5eAaImAgMnP
udržovat	udržovat	k5eAaImF
s	s	k7c7
českou	český	k2eAgFnSc7d1
větví	větvit	k5eAaImIp3nP
styky	styk	k1gInPc1
ještě	ještě	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1739	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
jako	jako	k9
protestanti	protestant	k1gMnPc1
byli	být	k5eAaImAgMnP
katolickými	katolický	k2eAgMnPc7d1
Kinskými	Kinský	k1gMnPc7
odmítnuti	odmítnut	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Kinští	Kinštit	k5eAaPmIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
</s>
<s>
V	v	k7c6
Česku	Česko	k1gNnSc6
žijí	žít	k5eAaImIp3nP
potomci	potomek	k1gMnPc1
kostelecké	kostelecký	k2eAgFnSc2d1
a	a	k8xC
chlumecké	chlumecký	k2eAgFnSc2d1
větve	větev	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
obě	dva	k4xCgFnPc1
hraběcí	hraběcí	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostelecká	kostelecký	k2eAgFnSc1d1
větev	větev	k1gFnSc1
vlastní	vlastnit	k5eAaImIp3nS
zámek	zámek	k1gInSc4
v	v	k7c6
Kostelci	Kostelec	k1gInSc6
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potomci	potomek	k1gMnPc1
Václava	Václav	k1gMnSc2
Norberta	Norbert	k1gMnSc2
z	z	k7c2
chlumecké	chlumecký	k2eAgFnSc2d1
větve	větev	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1961	#num#	k4
s	s	k7c7
příjmením	příjmení	k1gNnSc7
Kinský-dal-Borgo	Kinský-dal-Borgo	k1gMnSc1
<g/>
,	,	kIx,
vlastní	vlastní	k2eAgInSc4d1
zámek	zámek	k1gInSc4
Karlova	Karlův	k2eAgFnSc1d1
Koruna	koruna	k1gFnSc1
a	a	k8xC
hrad	hrad	k1gInSc1
Kost	kost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potomci	potomek	k1gMnPc1
Radslava	Radslav	k1gMnSc2
z	z	k7c2
téže	týž	k3xTgFnSc2,k3xDgFnSc2
větve	větev	k1gFnSc2
vlastní	vlastnit	k5eAaImIp3nS
zámek	zámek	k1gInSc1
Žďár	Žďár	k1gInSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Potomci	potomek	k1gMnPc1
další	další	k2eAgMnPc1d1
rodových	rodový	k2eAgFnPc2d1
větví	větvit	k5eAaImIp3nP
včetně	včetně	k7c2
představitelů	představitel	k1gMnPc2
hlavní	hlavní	k2eAgFnSc2d1
knížecí	knížecí	k2eAgFnSc2d1
linie	linie	k1gFnSc2
(	(	kIx(
<g/>
Karel	Karel	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
<g/>
,	,	kIx,
XII	XII	kA
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
Kinský	Kinský	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1967	#num#	k4
<g/>
))	))	k?
žijí	žít	k5eAaImIp3nP
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dolnorakouský	dolnorakouský	k2eAgInSc4d1
hrad	hrad	k1gInSc4
Heidenreichstein	Heidenreichstein	k1gMnSc1
připadl	připadnout	k5eAaPmAgMnS
Kinským	Kinský	k1gMnSc7
roku	rok	k1gInSc2
1961	#num#	k4
sňatkem	sňatek	k1gInSc7
hraběte	hrabě	k1gMnSc2
Kristiána	Kristián	k1gMnSc2
Leopolda	Leopold	k1gMnSc2
(	(	kIx(
<g/>
1924	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
z	z	k7c2
morkovického	morkovický	k2eAgInSc2d1
rodu	rod	k1gInSc2
s	s	k7c7
Josephinou	Josephin	k2eAgFnSc7d1
Marií	Maria	k1gFnSc7
<g/>
,	,	kIx,
hraběnkou	hraběnka	k1gFnSc7
van	vana	k1gFnPc2
der	drát	k5eAaImRp2nS
Straten-Ponthoz	Straten-Ponthoz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Johannes	Johannes	k1gMnSc1
„	„	k?
<g/>
Hans	Hans	k1gMnSc1
<g/>
“	“	k?
Kinský	Kinský	k1gMnSc1
(	(	kIx(
<g/>
1937	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
z	z	k7c2
horažďovické	horažďovický	k2eAgFnSc2d1
linie	linie	k1gFnSc2
roku	rok	k1gInSc2
1966	#num#	k4
získal	získat	k5eAaPmAgInS
zámek	zámek	k1gInSc1
Stadl	Stadl	k1gFnSc2
an	an	k?
der	drát	k5eAaImRp2nS
Raab	Raab	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Příbuzenstvo	příbuzenstvo	k1gNnSc1
</s>
<s>
Spojili	spojit	k5eAaPmAgMnP
se	se	k3xPyFc4
s	s	k7c7
Švihovskými	Švihovský	k2eAgInPc7d1
<g/>
,	,	kIx,
Šliky	šlika	k1gFnPc1
<g/>
,	,	kIx,
Schönborny	Schönborn	k1gInPc1
<g/>
,	,	kIx,
Ditrichštejny	Ditrichštejn	k1gInPc1
<g/>
,	,	kIx,
Nesselrody	Nesselrod	k1gInPc1
<g/>
,	,	kIx,
Poniatowskými	Poniatowská	k1gFnPc7
<g/>
,	,	kIx,
Martinici	Martinik	k1gMnPc1
<g/>
,	,	kIx,
Colloredo-Mannsfeldy	Colloredo-Mannsfelda	k1gFnPc1
<g/>
,	,	kIx,
Kerpeny	Kerpen	k2eAgFnPc1d1
<g/>
,	,	kIx,
Pálffyovci	Pálffyovec	k1gMnPc1
<g/>
,	,	kIx,
Lichtenštejny	Lichtenštejna	k1gFnPc1
či	či	k8xC
Thurny	Thurna	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Pohřebiště	pohřebiště	k1gNnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Isidora	Isidor	k1gMnSc2
(	(	kIx(
<g/>
Budenice	Budenice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Hrobka	hrobka	k1gFnSc1
Kinských	Kinských	k2eAgFnSc1d1
(	(	kIx(
<g/>
Budenice	Budenice	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
okrese	okres	k1gInSc6
Kladno	Kladno	k1gNnSc1
</s>
<s>
Hrobka	hrobka	k1gFnSc1
Kinských	Kinských	k2eAgFnSc1d1
při	při	k7c6
kostele	kostel	k1gInSc6
svatého	svatý	k2eAgMnSc2d1
Filipa	Filip	k1gMnSc2
a	a	k8xC
Jakuba	Jakub	k1gMnSc2
v	v	k7c6
Mlékosrbech	Mlékosrb	k1gInPc6
v	v	k7c6
okrese	okres	k1gInSc6
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Hrobka	hrobka	k1gFnSc1
Kinských	Kinská	k1gFnPc2
(	(	kIx(
<g/>
Veltrusy	Veltrusy	k1gInPc1
<g/>
)	)	kIx)
neboli	neboli	k8xC
pohřební	pohřební	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Kříže	Kříž	k1gMnSc2
v	v	k7c6
okrese	okres	k1gInSc6
Mělník	Mělník	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
pochován	pochovat	k5eAaPmNgInS
po	po	k7c6
tragické	tragický	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
Ferdinand	Ferdinand	k1gMnSc1
Jan	Jan	k1gMnSc1
Kinský	Kinský	k1gMnSc1
(	(	kIx(
<g/>
1781	#num#	k4
<g/>
–	–	k?
<g/>
1812	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hrobka	hrobka	k1gFnSc1
Kinských	Kinská	k1gFnPc2
na	na	k7c6
Břevnovském	břevnovský	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Kaple	kaple	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
(	(	kIx(
<g/>
Sloup	sloup	k1gInSc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
)	)	kIx)
</s>
<s>
Hrobka	hrobka	k1gFnSc1
Kinských	Kinských	k2eAgFnSc1d1
(	(	kIx(
<g/>
Náměšť	Náměšť	k1gFnSc1
na	na	k7c6
Hané	Haná	k1gFnSc6
<g/>
)	)	kIx)
v	v	k7c6
okrese	okres	k1gInSc6
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Hrobka	hrobka	k1gFnSc1
Kinských	Kinských	k2eAgFnSc1d1
na	na	k7c6
hřbitově	hřbitov	k1gInSc6
v	v	k7c4
Matzenu	Matzen	k2eAgFnSc4d1
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Gänserndorf	Gänserndorf	k1gInSc1
<g/>
,	,	kIx,
Dolní	dolní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Lesní	lesní	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
sv.	sv.	kA
Ferdinanda	Ferdinand	k1gMnSc2
v	v	k7c6
Žehuňské	Žehuňský	k2eAgFnSc6d1
oboře	obora	k1gFnSc6
v	v	k7c6
Kněžičkách	kněžička	k1gFnPc6
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Nymburk	Nymburk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
barokní	barokní	k2eAgFnSc6d1
kapli	kaple	k1gFnSc6
z	z	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
dočasně	dočasně	k6eAd1
pochována	pochovat	k5eAaPmNgFnS
Kristýna	Kristýna	k1gFnSc1
z	z	k7c2
Liechtensteinu	Liechtenstein	k1gMnSc6
(	(	kIx(
<g/>
1741	#num#	k4
<g/>
–	–	k?
<g/>
1819	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
manželka	manželka	k1gFnSc1
Františka	František	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
Kinského	Kinský	k1gMnSc2
(	(	kIx(
<g/>
1738	#num#	k4
<g/>
–	–	k?
<g/>
1806	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kaple	kaple	k1gFnSc1
v	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
zchátrala	zchátrat	k5eAaPmAgFnS
a	a	k8xC
rodina	rodina	k1gFnSc1
Kinský	Kinský	k1gMnSc1
dal	dát	k5eAaPmAgMnS
Borgo	Borgo	k6eAd1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
oboru	obora	k1gFnSc4
zrestituovala	zrestituovat	k5eAaBmAgFnS,k5eAaPmAgFnS
<g/>
,	,	kIx,
nechala	nechat	k5eAaPmAgFnS
kapli	kaple	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
hanebně	hanebně	k6eAd1
strhnout	strhnout	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
SMEJKAL	Smejkal	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
-	-	kIx~
Českolipsko	Českolipsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
:	:	kIx,
REGIA	REGIA	kA
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86367	#num#	k4
<g/>
-	-	kIx~
<g/>
65	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Hrabě	Hrabě	k1gMnSc1
Josef	Josef	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
Kinský	Kinský	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
165	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SEDLÁČEK	Sedláček	k1gMnSc1
<g/>
,	,	kIx,
August	August	k1gMnSc1
<g/>
:	:	kIx,
Atlasy	Atlas	k1gInPc1
erbů	erb	k1gInPc2
a	a	k8xC
pečetí	pečeť	k1gFnPc2
české	český	k2eAgFnSc2d1
a	a	k8xC
moravské	moravský	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
<g/>
;	;	kIx,
sv.	sv.	kA
3	#num#	k4
<g/>
;	;	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1043	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
;	;	kIx,
str	str	kA
<g/>
.	.	kIx.
389	#num#	k4
<g/>
,	,	kIx,
vyobrazení	vyobrazení	k1gNnSc2
1605	#num#	k4
<g/>
↑	↑	k?
SEDLÁČEK	Sedláček	k1gMnSc1
<g/>
,	,	kIx,
August	August	k1gMnSc1
<g/>
:	:	kIx,
Českomoravská	českomoravský	k2eAgFnSc1d1
heraldika	heraldika	k1gFnSc1
<g/>
,	,	kIx,
díl	díl	k1gInSc1
I.	I.	kA
<g/>
;	;	kIx,
1902	#num#	k4
str	str	kA
<g/>
.	.	kIx.
164	#num#	k4
<g/>
↑	↑	k?
HALADA	HALADA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lexikon	lexikon	k1gNnSc1
české	český	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
AKROPOLIS	Akropolis	k1gFnSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901020	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Kinští	Kinský	k1gMnPc1
<g/>
,	,	kIx,
s.	s.	k?
72	#num#	k4
<g/>
-	-	kIx~
<g/>
74	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
DROCÁR	DROCÁR	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
LOUŽECKÝ	LOUŽECKÝ	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historická	historický	k2eAgFnSc1d1
šlechta	šlechta	k1gFnSc1
<g/>
:	:	kIx,
Kinský	Kinský	k1gMnSc1
z	z	k7c2
Vchynic	Vchynice	k1gFnPc2
a	a	k8xC
Tetova	Tetov	k1gInSc2
<g/>
.	.	kIx.
www.historickaslechta.cz	www.historickaslechta.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
RICHTER	Richter	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sága	sága	k1gFnSc1
rodu	rod	k1gInSc2
Kinských	Kinská	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
.	.	kIx.
144	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
254	#num#	k4
<g/>
-	-	kIx~
<g/>
3592	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
140	#num#	k4
<g/>
–	–	k?
<g/>
141	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Preceptoři	preceptor	k1gMnPc1
<g/>
,	,	kIx,
mistři	mistr	k1gMnPc1
<g/>
,	,	kIx,
generální	generální	k2eAgMnPc1d1
převorové	převor	k1gMnPc1
<g/>
,	,	kIx,
velkopřevorové	velkopřevor	k1gMnPc1
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gMnPc1
náměstkové	náměstek	k1gMnPc1
a	a	k8xC
prokurátoři	prokurátor	k1gMnPc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
a	a	k8xC
v	v	k7c6
přivtělených	přivtělený	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řád	řád	k1gInSc4
Maltézských	maltézský	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
-	-	kIx~
České	český	k2eAgNnSc1d1
velkopřevorství	velkopřevorství	k1gNnSc1
<g/>
,	,	kIx,
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Hrad	hrad	k1gInSc1
Kost	kost	k1gFnSc1
-	-	kIx~
Historie	historie	k1gFnSc1
a	a	k8xC
prohlídky	prohlídka	k1gFnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kinský	Kinský	k2eAgInSc1d1
Castles	Castles	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Archiweb	Archiwba	k1gFnPc2
-	-	kIx~
Oprava	oprava	k1gFnSc1
hradu	hrad	k1gInSc2
Kost	kost	k1gFnSc1
na	na	k7c6
Jičínsku	Jičínsko	k1gNnSc6
je	být	k5eAaImIp3nS
hotová	hotová	k1gFnSc1
ze	z	k7c2
dvou	dva	k4xCgFnPc2
třetin	třetina	k1gFnPc2
<g/>
.	.	kIx.
www.archiweb.cz	www.archiweb.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Karlova	Karlův	k2eAgFnSc1d1
Koruna	koruna	k1gFnSc1
<g/>
:	:	kIx,
malebný	malebný	k2eAgInSc1d1
barokní	barokní	k2eAgInSc1d1
zámek	zámek	k1gInSc1
prochází	procházet	k5eAaImIp3nS
opravami	oprava	k1gFnPc7
za	za	k7c4
téměř	téměř	k6eAd1
sto	sto	k4xCgNnSc4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
|	|	kIx~
Stavebnictvi	Stavebnictev	k1gFnSc6
<g/>
3000	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.stavebnictvi3000.cz	www.stavebnictvi3000.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BROTHÁNKOVÁ	Brothánková	k1gFnSc1
<g/>
,	,	kIx,
Monika	Monika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgInSc1d3
ocenění	ocenění	k1gNnSc4
Kraje	kraj	k1gInSc2
Vysočina	vysočina	k1gFnSc1
letos	letos	k6eAd1
získalo	získat	k5eAaPmAgNnS
jedenáct	jedenáct	k4xCc1
osobností	osobnost	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihlava	Jihlava	k1gFnSc1
<g/>
:	:	kIx,
Kraj	kraj	k1gInSc1
Vysočina	vysočina	k1gFnSc1
<g/>
,	,	kIx,
2016-10-20	2016-10-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kinští	Kinský	k1gMnPc1
z	z	k7c2
Vchynic	Vchynice	k1gFnPc2
a	a	k8xC
Tetova	Tetov	k1gInSc2
na	na	k7c6
Sloupu	sloup	k1gInSc6
<g/>
.	.	kIx.
kinskysloup	kinskysloup	k1gInSc1
<g/>
.	.	kIx.
<g/>
rodokmenpro	rodokmenpro	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Střípky	střípek	k1gInPc1
z	z	k7c2
historie	historie	k1gFnSc2
a	a	k8xC
současnosti	současnost	k1gFnSc2
Luhů	luh	k1gInPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kinsky	Kinska	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paul	Paul	k1gMnSc1
Theroff	Theroff	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Royal	Royal	k1gMnSc1
Genealogy	genealog	k1gMnPc4
Site	Site	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Památkový	památkový	k2eAgInSc4d1
katalog	katalog	k1gInSc4
<g/>
:	:	kIx,
Kaple	kaple	k1gFnSc2
sv.	sv.	kA
Ferdinanda	Ferdinand	k1gMnSc2
(	(	kIx(
<g/>
lesní	lesní	k2eAgFnSc2d1
kaple	kaple	k1gFnSc2
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KREJSOVÁ	Krejsová	k1gFnSc1
<g/>
,	,	kIx,
Milena	Milena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
kněžičkovské	kněžičkovský	k2eAgFnSc2d1
obory	obora	k1gFnSc2
zmizela	zmizet	k5eAaPmAgFnS
kaple	kaple	k1gFnSc1
sv.	sv.	kA
Ferdinanda	Ferdinand	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2010-07-27	2010-07-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BROŽ	Brož	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgInPc1d1
postavy	postav	k1gInPc1
rodu	rod	k1gInSc2
Kinských	Kinský	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Petrklíč	petrklíč	k1gInSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
183	#num#	k4
s.	s.	k?
</s>
<s>
HALADA	HALADA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lexikon	lexikon	k1gNnSc1
české	český	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Akropolis	Akropolis	k1gFnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85770	#num#	k4
<g/>
-	-	kIx~
<g/>
79	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
Kinští	Kinský	k2eAgMnPc1d1
<g/>
,	,	kIx,
s.	s.	k?
265	#num#	k4
<g/>
–	–	k?
<g/>
267	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
JUŘÍK	Juřík	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kinští	Kinský	k1gMnPc1
<g/>
:	:	kIx,
Bůh	bůh	k1gMnSc1
<g/>
,	,	kIx,
čest	čest	k1gFnSc4
<g/>
,	,	kIx,
vlast	vlast	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Euromedia	Euromedium	k1gNnSc2
Group	Group	k1gMnSc1
k.	k.	k?
s.	s.	k?
–	–	k?
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
168	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Universum	universum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
242	#num#	k4
<g/>
-	-	kIx~
<g/>
6220	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KINSKÝ	KINSKÝ	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Radslav	Radslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rod	rod	k1gInSc1
Kinských	Kinských	k2eAgInSc1d1
na	na	k7c6
Chlumci	Chlumec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
jsem	být	k5eAaImIp1nS
slyšel	slyšet	k5eAaImAgMnS
a	a	k8xC
četl	číst	k5eAaImAgMnS
<g/>
..	..	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Edv	Edv	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leschinger	Leschingra	k1gFnPc2
<g/>
,	,	kIx,
1930	#num#	k4
<g/>
.	.	kIx.
74	#num#	k4
s.	s.	k?
</s>
<s>
MAŠEK	Mašek	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modrá	modrý	k2eAgFnSc1d1
krev	krev	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
760	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
Heslo	heslo	k1gNnSc4
Kinští	Kinský	k2eAgMnPc1d1
<g/>
,	,	kIx,
s.	s.	k?
129	#num#	k4
<g/>
–	–	k?
<g/>
133	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MAŠEK	Mašek	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlechtické	šlechtický	k2eAgInPc1d1
rody	rod	k1gInPc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
od	od	k7c2
Bílé	bílý	k2eAgFnSc2d1
hory	hora	k1gFnSc2
do	do	k7c2
současnosti	současnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
I.	I.	kA
A-	A-	k1gFnSc2
<g/>
M.	M.	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
257	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
27	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc1
Kinský	Kinský	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
454	#num#	k4
<g/>
–	–	k?
<g/>
457	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
RICHTER	Richter	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sága	sága	k1gFnSc1
rodu	rod	k1gInSc2
Kinských	Kinská	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
.	.	kIx.
144	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
254	#num#	k4
<g/>
-	-	kIx~
<g/>
3592	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VALENTA	Valenta	k1gMnSc1
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
rodu	rod	k1gInSc2
Kinských	Kinská	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
:	:	kIx,
Veduta	veduta	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86829	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ZECHOVSKÁ	ZECHOVSKÁ	kA
<g/>
,	,	kIx,
Markéta	Markéta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současné	současný	k2eAgInPc1d1
šlechtické	šlechtický	k2eAgInPc1d1
rody	rod	k1gInPc1
pohledem	pohled	k1gInSc7
médií	médium	k1gNnPc2
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgFnPc4
sondy	sonda	k1gFnPc4
<g/>
:	:	kIx,
Colloredo-Mansfeldové	Colloredo-Mansfeldové	k2eAgInSc4d1
a	a	k8xC
Kinský	Kinský	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
184	#num#	k4
s.	s.	k?
Bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
<g/>
,	,	kIx,
Fakulta	fakulta	k1gFnSc1
přírodovědně-humanitní	přírodovědně-humanitní	k2eAgFnSc1d1
a	a	k8xC
pedagogická	pedagogický	k2eAgFnSc1d1
<g/>
,	,	kIx,
Katedra	katedra	k1gFnSc1
historie	historie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k1gFnSc1
práce	práce	k1gFnSc2
PhDr.	PhDr.	kA
Milan	Milan	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
PhD	PhD	k1gMnSc1
<g/>
..	..	k?
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
českých	český	k2eAgInPc2d1
šlechtických	šlechtický	k2eAgInPc2d1
rodů	rod	k1gInPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kinští	Kinský	k2eAgMnPc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Stránky	stránka	k1gFnPc1
rodu	rod	k1gInSc2
–	–	k?
historie	historie	k1gFnSc2
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1
dědická	dědický	k2eAgFnSc1d1
linie	linie	k1gFnSc1
</s>
<s>
Historická	historický	k2eAgFnSc1d1
šlechta	šlechta	k1gFnSc1
<g/>
:	:	kIx,
Kinský	Kinský	k1gMnSc1
z	z	k7c2
Vchynic	Vchynice	k1gFnPc2
a	a	k8xC
Tetova	Tetovo	k1gNnSc2
</s>
<s>
Rodokmen	rodokmen	k1gInSc1
na	na	k7c4
genealogy	genealog	k1gMnPc4
<g/>
.	.	kIx.
<g/>
eu	eu	k?
<g/>
:	:	kIx,
Kinský	Kinský	k1gMnSc1
1	#num#	k4
</s>
<s>
Rodokmen	rodokmen	k1gInSc1
na	na	k7c4
genealogy	genealog	k1gMnPc4
<g/>
.	.	kIx.
<g/>
eu	eu	k?
<g/>
:	:	kIx,
Kinský	Kinský	k1gMnSc1
2	#num#	k4
</s>
<s>
Rodokmen	rodokmen	k1gInSc1
na	na	k7c4
genealogy	genealog	k1gMnPc4
<g/>
.	.	kIx.
<g/>
eu	eu	k?
<g/>
:	:	kIx,
Kinský	Kinský	k1gMnSc1
3	#num#	k4
</s>
<s>
Rodokmen	rodokmen	k1gInSc4
na	na	k7c4
Paul	Paula	k1gFnPc2
Theroff	Theroff	k1gInSc4
<g/>
’	’	k?
<g/>
s	s	k7c7
Royal	Royal	k1gMnSc1
Genealogy	genealog	k1gMnPc4
Site	Site	k1gNnSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jx	jx	k?
<g/>
20060320025	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
122692403	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
64895455	#num#	k4
</s>
