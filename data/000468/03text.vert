<s>
Amazonka	Amazonka	k1gFnSc1	Amazonka
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Río	Río	k1gMnSc1	Río
Amazonas	Amazonas	k1gMnSc1	Amazonas
<g/>
,	,	kIx,	,
portugalsky	portugalsky	k6eAd1	portugalsky
Rio	Rio	k1gMnSc1	Rio
Amazonas	Amazonas	k1gMnSc1	Amazonas
<g/>
,	,	kIx,	,
v	v	k7c6	v
domorodých	domorodý	k2eAgInPc6d1	domorodý
jazycích	jazyk	k1gInPc6	jazyk
Parana	Paran	k1gMnSc4	Paran
Tinga	Ting	k1gMnSc2	Ting
[	[	kIx(	[
<g/>
Bílá	bílý	k2eAgFnSc1d1	bílá
řeka	řeka	k1gFnSc1	řeka
<g/>
]	]	kIx)	]
nebo	nebo	k8xC	nebo
Parana	Parana	k1gFnSc1	Parana
Guasu	Guas	k1gInSc2	Guas
[	[	kIx(	[
<g/>
Velká	velký	k2eAgFnSc1d1	velká
řeka	řeka	k1gFnSc1	řeka
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvodnatější	vodnatý	k2eAgFnSc7d3	nejvodnatější
a	a	k8xC	a
nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
řekou	řeka	k1gFnSc7	řeka
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
rovníkové	rovníkový	k2eAgFnSc6d1	Rovníková
oblasti	oblast	k1gFnSc6	oblast
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
Peru	Peru	k1gNnSc6	Peru
a	a	k8xC	a
stéká	stékat	k5eAaImIp3nS	stékat
ze	z	k7c2	z
svahů	svah	k1gInPc2	svah
And	Anda	k1gFnPc2	Anda
do	do	k7c2	do
peruánské	peruánský	k2eAgFnSc2d1	peruánská
části	část	k1gFnSc2	část
Amazonské	amazonský	k2eAgFnSc2d1	Amazonská
nížiny	nížina	k1gFnSc2	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
tok	tok	k1gInSc1	tok
dále	daleko	k6eAd2	daleko
tvoří	tvořit	k5eAaImIp3nS	tvořit
relativně	relativně	k6eAd1	relativně
krátkou	krátký	k2eAgFnSc4d1	krátká
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Peru	Peru	k1gNnSc7	Peru
a	a	k8xC	a
Kolumbií	Kolumbie	k1gFnSc7	Kolumbie
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
Brazílie	Brazílie	k1gFnSc2	Brazílie
(	(	kIx(	(
<g/>
Amazonas	Amazonas	k1gInSc1	Amazonas
<g/>
,	,	kIx,	,
Pará	Pará	k1gFnSc1	Pará
<g/>
,	,	kIx,	,
Amapá	Amapý	k2eAgFnSc1d1	Amapá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
napříč	napříč	k7c7	napříč
jejím	její	k3xOp3gNnSc7	její
územím	území	k1gNnSc7	území
teče	téct	k5eAaImIp3nS	téct
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
západ-východ	západýchod	k1gInSc4	západ-východ
do	do	k7c2	do
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejvodnatější	vodnatý	k2eAgMnSc1d3	vodnatý
<g/>
,	,	kIx,	,
<g/>
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgNnSc4d3	veliký
povodí	povodí	k1gNnSc4	povodí
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
řek	řeka	k1gFnPc2	řeka
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Udávaná	udávaný	k2eAgFnSc1d1	udávaná
velikost	velikost	k1gFnSc1	velikost
povodí	povodí	k1gNnSc2	povodí
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
zdrojích	zdroj	k1gInPc6	zdroj
od	od	k7c2	od
6	[number]	k4	6
915	[number]	k4	915
000	[number]	k4	000
do	do	k7c2	do
7	[number]	k4	7
180	[number]	k4	180
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zabírá	zabírat	k5eAaImIp3nS	zabírat
40	[number]	k4	40
<g/>
%	%	kIx~	%
světadílu	světadíl	k1gInSc2	světadíl
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
jejího	její	k3xOp3gInSc2	její
toku	tok	k1gInSc2	tok
protéká	protékat	k5eAaImIp3nS	protékat
Amazonským	amazonský	k2eAgInSc7d1	amazonský
deštným	deštný	k2eAgInSc7d1	deštný
pralesem	prales	k1gInSc7	prales
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
pramenech	pramen	k1gInPc6	pramen
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
zdrojnice	zdrojnice	k1gFnSc2	zdrojnice
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
6	[number]	k4	6
296	[number]	k4	296
km	km	kA	km
(	(	kIx(	(
<g/>
Marañ	Marañ	k1gMnSc2	Marañ
<g/>
)	)	kIx)	)
do	do	k7c2	do
7	[number]	k4	7
062	[number]	k4	062
km	km	kA	km
(	(	kIx(	(
<g/>
Ucayali	Ucayali	k1gMnSc1	Ucayali
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
povodí	povodí	k1gNnSc2	povodí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
část	část	k1gFnSc1	část
toku	tok	k1gInSc2	tok
od	od	k7c2	od
soutoku	soutok	k1gInSc2	soutok
zdrojnic	zdrojnice	k1gFnPc2	zdrojnice
k	k	k7c3	k
soutoku	soutok	k1gInSc3	soutok
s	s	k7c7	s
Rio	Rio	k1gFnSc7	Rio
Negro	Negro	k1gNnSc4	Negro
nazývá	nazývat	k5eAaImIp3nS	nazývat
Solimõ	Solimõ	k1gMnSc1	Solimõ
<g/>
.	.	kIx.	.
</s>
<s>
Jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
povodí	povodí	k1gNnSc2	povodí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
území	území	k1gNnSc6	území
Bolívie	Bolívie	k1gFnSc2	Bolívie
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc2	Peru
<g/>
,	,	kIx,	,
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
a	a	k8xC	a
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
je	být	k5eAaImIp3nS	být
ohraničeno	ohraničit	k5eAaPmNgNnS	ohraničit
východními	východní	k2eAgInPc7d1	východní
svahy	svah	k1gInPc7	svah
And	Anda	k1gFnPc2	Anda
<g/>
,	,	kIx,	,
jižními	jižní	k2eAgInPc7d1	jižní
svahy	svah	k1gInPc7	svah
Guyanské	Guyanský	k2eAgFnSc2d1	Guyanská
vysočiny	vysočina	k1gFnSc2	vysočina
a	a	k8xC	a
severními	severní	k2eAgInPc7d1	severní
svahy	svah	k1gInPc7	svah
Brazilské	brazilský	k2eAgFnSc2d1	brazilská
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
Amazonskou	amazonský	k2eAgFnSc4d1	Amazonská
nížinu	nížina	k1gFnSc4	nížina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
nížin	nížina	k1gFnPc2	nížina
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
tok	tok	k1gInSc1	tok
prochází	procházet	k5eAaImIp3nS	procházet
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
na	na	k7c4	na
východ	východ	k1gInSc4	východ
mezi	mezi	k7c7	mezi
rovníkem	rovník	k1gInSc7	rovník
a	a	k8xC	a
5	[number]	k4	5
<g/>
°	°	k?	°
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
s	s	k7c7	s
největším	veliký	k2eAgNnSc7d3	veliký
množstvím	množství	k1gNnSc7	množství
srážek	srážka	k1gFnPc2	srážka
(	(	kIx(	(
<g/>
1	[number]	k4	1
500	[number]	k4	500
až	až	k9	až
3	[number]	k4	3
000	[number]	k4	000
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
rozloženy	rozložit	k5eAaPmNgInP	rozložit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
průtok	průtok	k1gInSc4	průtok
219	[number]	k4	219
000	[number]	k4	000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
U	u	k7c2	u
soutoku	soutok	k1gInSc2	soutok
zdrojnic	zdrojnice	k1gFnPc2	zdrojnice
je	být	k5eAaImIp3nS	být
šířka	šířka	k1gFnSc1	šířka
řeky	řeka	k1gFnSc2	řeka
2	[number]	k4	2
km	km	kA	km
<g/>
,	,	kIx,	,
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
5	[number]	k4	5
km	km	kA	km
<g/>
,	,	kIx,	,
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
15	[number]	k4	15
až	až	k9	až
20	[number]	k4	20
km	km	kA	km
a	a	k8xC	a
před	před	k7c7	před
ústím	ústí	k1gNnSc7	ústí
80	[number]	k4	80
až	až	k8xS	až
150	[number]	k4	150
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
řeky	řeka	k1gFnSc2	řeka
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
70	[number]	k4	70
m	m	kA	m
u	u	k7c2	u
Óbidosu	Óbidos	k1gInSc2	Óbidos
135	[number]	k4	135
m	m	kA	m
a	a	k8xC	a
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
15	[number]	k4	15
až	až	k9	až
45	[number]	k4	45
m.	m.	k?	m.
Od	od	k7c2	od
soutoku	soutok	k1gInSc2	soutok
zdrojnic	zdrojnice	k1gFnPc2	zdrojnice
Ucayali	Ucayali	k1gFnSc2	Ucayali
a	a	k8xC	a
Marañ	Marañ	k1gFnSc2	Marañ
teče	téct	k5eAaImIp3nS	téct
Amazonka	Amazonka	k1gFnSc1	Amazonka
po	po	k7c6	po
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
bažinatá	bažinatý	k2eAgFnSc1d1	bažinatá
a	a	k8xC	a
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
ji	on	k3xPp3gFnSc4	on
tropický	tropický	k2eAgInSc4d1	tropický
deštný	deštný	k2eAgInSc4d1	deštný
les	les	k1gInSc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Koryto	koryto	k1gNnSc1	koryto
je	být	k5eAaImIp3nS	být
lemováno	lemovat	k5eAaImNgNnS	lemovat
nízkými	nízký	k2eAgInPc7d1	nízký
břehy	břeh	k1gInPc7	břeh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
klesají	klesat	k5eAaImIp3nP	klesat
k	k	k7c3	k
vodě	voda	k1gFnSc3	voda
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
stupních	stupeň	k1gInPc6	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgInSc1d1	horní
stupeň	stupeň	k1gInSc1	stupeň
(	(	kIx(	(
<g/>
terra	terra	k1gFnSc1	terra
firma	firma	k1gFnSc1	firma
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nezatápěný	zatápěný	k2eNgInSc4d1	zatápěný
břeh	břeh	k1gInSc4	břeh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
tvořený	tvořený	k2eAgInSc1d1	tvořený
základním	základní	k2eAgInSc7d1	základní
sklonem	sklon	k1gInSc7	sklon
doliny	dolina	k1gFnSc2	dolina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgMnSc1d1	vysoký
50	[number]	k4	50
m	m	kA	m
i	i	k8xC	i
více	hodně	k6eAd2	hodně
a	a	k8xC	a
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
říční	říční	k2eAgInSc1d1	říční
úval	úval	k1gInSc1	úval
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgInSc1d1	střední
stupeň	stupeň	k1gInSc1	stupeň
(	(	kIx(	(
<g/>
varzea	varzea	k1gFnSc1	varzea
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
úvalu	úval	k1gInSc2	úval
zatápěna	zatápěn	k2eAgFnSc1d1	zatápěna
při	při	k7c6	při
velkých	velký	k2eAgInPc6d1	velký
vzestupech	vzestup	k1gInPc6	vzestup
hladiny	hladina	k1gFnSc2	hladina
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgInSc1d1	dolní
stupeň	stupeň	k1gInSc1	stupeň
(	(	kIx(	(
<g/>
igapo	igapa	k1gFnSc5	igapa
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
bažina	bažina	k1gFnSc1	bažina
je	být	k5eAaImIp3nS	být
zatápěna	zatápěn	k2eAgFnSc1d1	zatápěna
při	při	k7c6	při
běžných	běžný	k2eAgInPc6d1	běžný
vzestupech	vzestup	k1gInPc6	vzestup
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
soutokem	soutok	k1gInSc7	soutok
s	s	k7c7	s
Rio	Rio	k1gFnSc7	Rio
Negro	Negro	k1gNnSc4	Negro
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Encontro	Encontro	k1gNnSc1	Encontro
das	das	k?	das
Águas	Águas	k1gMnSc1	Águas
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dolina	dolina	k1gFnSc1	dolina
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
na	na	k7c4	na
80	[number]	k4	80
až	až	k9	až
100	[number]	k4	100
km	km	kA	km
a	a	k8xC	a
jen	jen	k9	jen
u	u	k7c2	u
měst	město	k1gNnPc2	město
Óbidos	Óbidosa	k1gFnPc2	Óbidosa
a	a	k8xC	a
Santarém	Santarý	k2eAgInSc6d1	Santarý
se	se	k3xPyFc4	se
o	o	k7c4	o
něco	něco	k3yInSc4	něco
zužuje	zužovat	k5eAaImIp3nS	zužovat
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
úvalu	úval	k1gInSc2	úval
je	být	k5eAaImIp3nS	být
členěn	členit	k5eAaImNgInS	členit
mnohými	mnohý	k2eAgMnPc7d1	mnohý
rameny	rameno	k1gNnPc7	rameno
a	a	k8xC	a
průtoky	průtok	k1gInPc7	průtok
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
četná	četný	k2eAgNnPc4d1	četné
jezera	jezero	k1gNnPc4	jezero
a	a	k8xC	a
stará	starý	k2eAgNnPc4d1	staré
ramena	rameno	k1gNnPc4	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
koryta	koryto	k1gNnSc2	koryto
se	se	k3xPyFc4	se
táhnou	táhnout	k5eAaImIp3nP	táhnout
nízké	nízký	k2eAgInPc1d1	nízký
valy	val	k1gInPc1	val
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
350	[number]	k4	350
km	km	kA	km
od	od	k7c2	od
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
začíná	začínat	k5eAaImIp3nS	začínat
delta	delta	k1gFnSc1	delta
Amazonky	Amazonka	k1gFnSc2	Amazonka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
100	[number]	k4	100
000	[number]	k4	000
km2	km2	k4	km2
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
vody	voda	k1gFnSc2	voda
odtéká	odtékat	k5eAaImIp3nS	odtékat
severovýchodními	severovýchodní	k2eAgNnPc7d1	severovýchodní
rameny	rameno	k1gNnPc7	rameno
a	a	k8xC	a
jen	jen	k9	jen
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
odtéká	odtékat	k5eAaImIp3nS	odtékat
jižním	jižní	k2eAgNnSc7d1	jižní
ramenem	rameno	k1gNnSc7	rameno
Pará	Pará	k1gFnSc1	Pará
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
hlavními	hlavní	k2eAgNnPc7d1	hlavní
rameny	rameno	k1gNnPc7	rameno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
říčních	říční	k2eAgInPc2d1	říční
ostrovů	ostrov	k1gInPc2	ostrov
Marajó	Marajó	k1gFnSc2	Marajó
<g/>
.	.	kIx.	.
</s>
<s>
Amazonka	Amazonka	k1gFnSc1	Amazonka
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
000	[number]	k4	000
větších	veliký	k2eAgInPc2d2	veliký
přítoků	přítok	k1gInPc2	přítok
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
dvacet	dvacet	k4xCc1	dvacet
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
představuje	představovat	k5eAaImIp3nS	představovat
velké	velký	k2eAgFnPc4d1	velká
řeky	řeka	k1gFnPc4	řeka
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
1	[number]	k4	1
500	[number]	k4	500
až	až	k9	až
3	[number]	k4	3
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
postupně	postupně	k6eAd1	postupně
<g/>
:	:	kIx,	:
zdrojnice	zdrojnice	k1gFnSc1	zdrojnice
zprava	zprava	k6eAd1	zprava
Ucayali	Ucayali	k1gMnSc2	Ucayali
(	(	kIx(	(
<g/>
Apurímac	Apurímac	k1gFnSc1	Apurímac
<g/>
(	(	kIx(	(
<g/>
-Lloqueta	-Lloqueta	k1gFnSc1	-Lloqueta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Urubamba	Urubamba	k1gFnSc1	Urubamba
<g/>
)	)	kIx)	)
zleva	zleva	k6eAd1	zleva
Marañ	Marañ	k1gMnSc2	Marañ
(	(	kIx(	(
<g/>
Morona	Moron	k1gMnSc2	Moron
<g/>
,	,	kIx,	,
Pastaza	Pastaz	k1gMnSc2	Pastaz
<g/>
,	,	kIx,	,
Huallaga	Huallag	k1gMnSc2	Huallag
<g/>
,	,	kIx,	,
Tigre	Tigr	k1gMnSc5	Tigr
<g/>
)	)	kIx)	)
Solimõ	Solimõ	k1gMnSc3	Solimõ
zprava	zprava	k6eAd1	zprava
Javari	Javari	k1gNnSc2	Javari
<g/>
,	,	kIx,	,
Jandiatuba	Jandiatuba	k1gFnSc1	Jandiatuba
<g/>
,	,	kIx,	,
Jutaí	Jutaí	k1gFnSc1	Jutaí
<g/>
,	,	kIx,	,
Juruá	Juruá	k1gFnSc1	Juruá
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Coari	Coar	k1gFnSc5	Coar
<g/>
,	,	kIx,	,
Purus	Purus	k1gMnSc1	Purus
zleva	zleva	k6eAd1	zleva
Nanay	Nana	k1gMnPc4	Nana
<g/>
,	,	kIx,	,
Napo	napa	k1gFnSc5	napa
<g/>
,	,	kIx,	,
Içá	Içá	k1gMnSc1	Içá
<g/>
/	/	kIx~	/
<g/>
Putumayo	Putumayo	k1gMnSc1	Putumayo
<g/>
,	,	kIx,	,
Japurá	Japurý	k2eAgFnSc1d1	Japurá
<g/>
,	,	kIx,	,
Rio	Rio	k1gMnSc5	Rio
Negro	Negra	k1gMnSc5	Negra
(	(	kIx(	(
<g/>
Rio	Rio	k1gMnSc5	Rio
Branco	Branca	k1gMnSc5	Branca
a	a	k8xC	a
Casiquiare	Casiquiar	k1gMnSc5	Casiquiar
<g/>
)	)	kIx)	)
Amazonka	Amazonka	k1gFnSc1	Amazonka
zprava	zprava	k6eAd1	zprava
Madeira	Madeira	k1gFnSc1	Madeira
<g/>
,	,	kIx,	,
Tapajós	Tapajós	k1gInSc1	Tapajós
(	(	kIx(	(
<g/>
Juruena	Juruena	k1gFnSc1	Juruena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Xingu	Xinga	k1gFnSc4	Xinga
zleva	zleva	k6eAd1	zleva
Uatuma	Uatum	k1gMnSc4	Uatum
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Trombetas	Trombetasa	k1gFnPc2	Trombetasa
<g/>
,	,	kIx,	,
Paru	Paros	k1gInSc2	Paros
<g/>
,	,	kIx,	,
Jari	jar	k1gFnSc6	jar
Pará	Pará	k1gFnSc1	Pará
Tocantins	Tocantinsa	k1gFnPc2	Tocantinsa
(	(	kIx(	(
<g/>
Araguaia	Araguaia	k1gFnSc1	Araguaia
<g/>
)	)	kIx)	)
Řeky	řeka	k1gFnPc1	řeka
(	(	kIx(	(
<g/>
přítoky	přítok	k1gInPc1	přítok
<g/>
)	)	kIx)	)
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Amazonky	Amazonka	k1gFnSc2	Amazonka
dle	dle	k7c2	dle
délky	délka	k1gFnSc2	délka
toku	tok	k1gInSc2	tok
<g/>
:	:	kIx,	:
3	[number]	k4	3
379	[number]	k4	379
km	km	kA	km
-	-	kIx~	-
Purus	Purus	k1gInSc1	Purus
3	[number]	k4	3
280	[number]	k4	280
km	km	kA	km
-	-	kIx~	-
Juruá	Juruá	k1gFnSc1	Juruá
3	[number]	k4	3
239	[number]	k4	239
km	km	kA	km
-	-	kIx~	-
Madeira	Madeira	k1gFnSc1	Madeira
2	[number]	k4	2
820	[number]	k4	820
km	km	kA	km
-	-	kIx~	-
Japurá	Japurý	k2eAgFnSc1d1	Japurá
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
750	[number]	k4	750
km	km	kA	km
-	-	kIx~	-
Tocantins	Tocantins	k1gInSc1	Tocantins
2	[number]	k4	2
575	[number]	k4	575
km	km	kA	km
-	-	kIx~	-
Araguaia	Araguaia	k1gFnSc1	Araguaia
(	(	kIx(	(
<g/>
přítok	přítok	k1gInSc1	přítok
Tocantinsu	Tocantins	k1gInSc2	Tocantins
<g/>
)	)	kIx)	)
2	[number]	k4	2
250	[number]	k4	250
km	km	kA	km
-	-	kIx~	-
Rio	Rio	k1gFnSc1	Rio
Negro	Negro	k1gNnSc1	Negro
2	[number]	k4	2
100	[number]	k4	100
km	km	kA	km
-	-	kIx~	-
Xingu	Xing	k1gInSc2	Xing
1	[number]	k4	1
900	[number]	k4	900
km	km	kA	km
-	-	kIx~	-
Tapajós	Tapajós	k1gInSc1	Tapajós
1	[number]	k4	1
749	[number]	k4	749
km	km	kA	km
-	-	kIx~	-
Guaporé	Guaporý	k2eAgFnPc4d1	Guaporý
(	(	kIx(	(
<g/>
přítok	přítok	k1gInSc4	přítok
Mamoré	Mamorý	k2eAgNnSc1d1	Mamoré
<g/>
)	)	kIx)	)
1	[number]	k4	1
600	[number]	k4	600
km	km	kA	km
-	-	kIx~	-
Ucayali	Ucayali	k1gFnSc1	Ucayali
1	[number]	k4	1
575	[number]	k4	575
km	km	kA	km
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Içá	Içá	k1gMnPc5	Içá
<g/>
/	/	kIx~	/
<g/>
Putumayo	Putumayo	k1gNnSc4	Putumayo
1	[number]	k4	1
415	[number]	k4	415
km	km	kA	km
-	-	kIx~	-
Marañ	Marañ	k1gFnSc1	Marañ
1	[number]	k4	1
300	[number]	k4	300
km	km	kA	km
-	-	kIx~	-
Iriri	Iriri	k1gNnSc1	Iriri
(	(	kIx(	(
<g/>
přítok	přítok	k1gInSc1	přítok
Xingu	Xing	k1gInSc2	Xing
<g/>
)	)	kIx)	)
1	[number]	k4	1
240	[number]	k4	240
km	km	kA	km
-	-	kIx~	-
Juruena	Juruena	k1gFnSc1	Juruena
(	(	kIx(	(
<g/>
přítok	přítok	k1gInSc1	přítok
Tapajós	Tapajós	k1gInSc1	Tapajós
<g/>
)	)	kIx)	)
1	[number]	k4	1
200	[number]	k4	200
km	km	kA	km
-	-	kIx~	-
Tapájos	Tapájos	k1gInSc1	Tapájos
1	[number]	k4	1
130	[number]	k4	130
km	km	kA	km
-	-	kIx~	-
Madre	Madr	k1gInSc5	Madr
de	de	k?	de
Dios	Dios	k1gInSc1	Dios
(	(	kIx(	(
<g/>
přítok	přítok	k1gInSc1	přítok
Madeiry	Madeira	k1gFnSc2	Madeira
<g/>
)	)	kIx)	)
1	[number]	k4	1
100	[number]	k4	100
km	km	kA	km
-	-	kIx~	-
Huallaga	Huallaga	k1gFnSc1	Huallaga
(	(	kIx(	(
<g/>
přítok	přítok	k1gInSc1	přítok
Marañ	Marañ	k1gFnSc2	Marañ
<g/>
)	)	kIx)	)
Amazonka	Amazonka	k1gFnSc1	Amazonka
má	mít	k5eAaImIp3nS	mít
složitý	složitý	k2eAgInSc4d1	složitý
a	a	k8xC	a
svérázný	svérázný	k2eAgInSc4d1	svérázný
vodní	vodní	k2eAgInSc4d1	vodní
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vysoký	vysoký	k2eAgInSc4d1	vysoký
stav	stav	k1gInSc4	stav
vody	voda	k1gFnSc2	voda
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Pravé	pravý	k2eAgInPc1d1	pravý
přítoky	přítok	k1gInPc1	přítok
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
povodí	povodí	k1gNnPc1	povodí
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
a	a	k8xC	a
levé	levý	k2eAgInPc1d1	levý
přítoky	přítok	k1gInPc1	přítok
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
povodí	povodí	k1gNnPc1	povodí
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
stavech	stav	k1gInPc6	stav
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgInPc1d1	vysoký
stavy	stav	k1gInPc1	stav
vody	voda	k1gFnSc2	voda
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
(	(	kIx(	(
<g/>
levé	levý	k2eAgInPc1d1	levý
přítoky	přítok	k1gInPc1	přítok
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
března	březen	k1gInSc2	březen
(	(	kIx(	(
<g/>
pravé	pravý	k2eAgInPc1d1	pravý
přítoky	přítok	k1gInPc1	přítok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
sezónní	sezónní	k2eAgNnSc1d1	sezónní
kolísání	kolísání	k1gNnSc1	kolísání
průtoku	průtok	k1gInSc2	průtok
u	u	k7c2	u
Amazonky	Amazonka	k1gFnSc2	Amazonka
vyrovnáno	vyrovnán	k2eAgNnSc4d1	vyrovnáno
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgInPc1d1	jižní
přítoky	přítok	k1gInPc1	přítok
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
vodnosti	vodnost	k1gFnSc2	vodnost
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
až	až	k6eAd1	až
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
jejich	jejich	k3xOp3gNnSc2	jejich
hladina	hladina	k1gFnSc1	hladina
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgInSc1d1	maximální
průtok	průtok	k1gInSc1	průtok
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
300	[number]	k4	300
000	[number]	k4	000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
řeky	řeka	k1gFnSc2	řeka
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
až	až	k6eAd1	až
300	[number]	k4	300
km	km	kA	km
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
v	v	k7c6	v
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nízkém	nízký	k2eAgInSc6d1	nízký
stavu	stav	k1gInSc6	stav
vody	voda	k1gFnSc2	voda
klesá	klesat	k5eAaImIp3nS	klesat
průtok	průtok	k1gInSc1	průtok
na	na	k7c4	na
70	[number]	k4	70
000	[number]	k4	000
až	až	k9	až
80	[number]	k4	80
000	[number]	k4	000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
průtok	průtok	k1gInSc1	průtok
činí	činit	k5eAaImIp3nS	činit
175	[number]	k4	175
000	[number]	k4	000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
a	a	k8xC	a
celkový	celkový	k2eAgInSc4d1	celkový
roční	roční	k2eAgInSc4d1	roční
odtok	odtok	k1gInSc4	odtok
z	z	k7c2	z
povodí	povodí	k1gNnSc2	povodí
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
520	[number]	k4	520
km3	km3	k4	km3
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Amazonku	Amazonka	k1gFnSc4	Amazonka
tak	tak	k6eAd1	tak
připadá	připadat	k5eAaPmIp3nS	připadat
15	[number]	k4	15
až	až	k9	až
17	[number]	k4	17
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
ročního	roční	k2eAgInSc2d1	roční
odtoku	odtok	k1gInSc2	odtok
všech	všecek	k3xTgFnPc2	všecek
řek	řeka	k1gFnPc2	řeka
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
odnese	odnést	k5eAaPmIp3nS	odnést
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
povodí	povodí	k1gNnSc2	povodí
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
Mt	Mt	k1gFnPc4	Mt
pevného	pevný	k2eAgInSc2d1	pevný
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
režim	režim	k1gInSc4	režim
řeky	řeka	k1gFnSc2	řeka
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc1	vliv
také	také	k9	také
příliv	příliv	k1gInSc1	příliv
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
postupuje	postupovat	k5eAaImIp3nS	postupovat
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
až	až	k6eAd1	až
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
1	[number]	k4	1
400	[number]	k4	400
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ústí	ústí	k1gNnSc2	ústí
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
příliv	příliv	k1gInSc1	příliv
tzv.	tzv.	kA	tzv.
hlomozivou	hlomozivý	k2eAgFnSc4d1	hlomozivý
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
prudkou	prudký	k2eAgFnSc4d1	prudká
vlnu	vlna	k1gFnSc4	vlna
vysokou	vysoký	k2eAgFnSc4d1	vysoká
4	[number]	k4	4
až	až	k8xS	až
5	[number]	k4	5
m	m	kA	m
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
s	s	k7c7	s
duněním	dunění	k1gNnSc7	dunění
valí	valit	k5eAaImIp3nP	valit
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
a	a	k8xC	a
zatopuje	zatopovat	k5eAaImIp3nS	zatopovat
a	a	k8xC	a
rozrušuje	rozrušovat	k5eAaImIp3nS	rozrušovat
břehy	břeh	k1gInPc4	břeh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
z	z	k7c2	z
indiánských	indiánský	k2eAgNnPc2d1	indiánské
nářečí	nářečí	k1gNnPc2	nářečí
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
vlna	vlna	k1gFnSc1	vlna
nazývá	nazývat	k5eAaImIp3nS	nazývat
amazunu	amazuna	k1gFnSc4	amazuna
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
že	že	k8xS	že
od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
slova	slovo	k1gNnSc2	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
současný	současný	k2eAgInSc4d1	současný
název	název	k1gInSc4	název
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
možností	možnost	k1gFnSc7	možnost
vzniku	vznik	k1gInSc2	vznik
názvu	název	k1gInSc2	název
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1541	[number]	k4	1541
na	na	k7c6	na
výpravě	výprava	k1gFnSc6	výprava
po	po	k7c6	po
toku	tok	k1gInSc6	tok
jihoamerické	jihoamerický	k2eAgFnSc2d1	jihoamerická
řeky	řeka	k1gFnSc2	řeka
objevil	objevit	k5eAaPmAgMnS	objevit
setník	setník	k1gMnSc1	setník
Francisco	Francisco	k1gMnSc1	Francisco
de	de	k?	de
Orellana	Orellan	k1gMnSc4	Orellan
indiánský	indiánský	k2eAgInSc1d1	indiánský
kmen	kmen	k1gInSc1	kmen
<g/>
,	,	kIx,	,
kterému	který	k3yIgInSc3	který
v	v	k7c6	v
roli	role	k1gFnSc6	role
náčelnice	náčelnice	k1gFnSc2	náčelnice
vládla	vládnout	k5eAaImAgFnS	vládnout
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
nazval	nazvat	k5eAaBmAgMnS	nazvat
tento	tento	k3xDgMnSc1	tento
dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
řeku	řeka	k1gFnSc4	řeka
Amazonkou	Amazonka	k1gFnSc7	Amazonka
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
z	z	k7c2	z
přítoků	přítok	k1gInPc2	přítok
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
rychle	rychle	k6eAd1	rychle
rostoucímu	rostoucí	k2eAgInSc3d1	rostoucí
průtoku	průtok	k1gInSc3	průtok
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Přítoky	přítok	k1gInPc1	přítok
se	se	k3xPyFc4	se
neliší	lišit	k5eNaImIp3nP	lišit
jen	jen	k9	jen
svou	svůj	k3xOyFgFnSc7	svůj
šířkou	šířka	k1gFnSc7	šířka
a	a	k8xC	a
vodností	vodnost	k1gFnSc7	vodnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
barvou	barva	k1gFnSc7	barva
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
především	především	k9	především
na	na	k7c6	na
oblasti	oblast	k1gFnSc6	oblast
a	a	k8xC	a
stáří	stáří	k1gNnSc6	stáří
pohoří	pohořet	k5eAaPmIp3nS	pohořet
kde	kde	k6eAd1	kde
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
přítoky	přítok	k1gInPc4	přítok
pramení	pramenit	k5eAaImIp3nS	pramenit
<g/>
.	.	kIx.	.
</s>
<s>
Rio	Rio	k?	Rio
Negro	Negro	k1gNnSc1	Negro
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
čistá	čistý	k2eAgFnSc1d1	čistá
a	a	k8xC	a
zabarvená	zabarvený	k2eAgFnSc1d1	zabarvená
do	do	k7c2	do
černa	černo	k1gNnSc2	černo
<g/>
,	,	kIx,	,
Rio	Rio	k1gFnSc1	Rio
Branco	Branco	k6eAd1	Branco
mléčně	mléčně	k6eAd1	mléčně
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
přítoky	přítok	k1gInPc1	přítok
jsou	být	k5eAaImIp3nP	být
žluté	žlutý	k2eAgFnPc1d1	žlutá
<g/>
,	,	kIx,	,
šedé	šedý	k2eAgFnPc1d1	šedá
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnPc1d1	zelená
nebo	nebo	k8xC	nebo
červené	červený	k2eAgFnPc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
s	s	k7c7	s
Rio	Rio	k1gFnSc7	Rio
Negro	Negro	k1gNnSc1	Negro
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
pomalému	pomalý	k2eAgNnSc3d1	pomalé
mísení	mísení	k1gNnSc3	mísení
žluto-šedé	žluto-šedý	k2eAgFnSc2d1	žluto-šedý
Amazonky	Amazonka	k1gFnSc2	Amazonka
a	a	k8xC	a
čisté	čistý	k2eAgFnSc2d1	čistá
černě	čerň	k1gFnSc2	čerň
zabarvené	zabarvený	k2eAgFnSc2d1	zabarvená
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
Rio	Rio	k1gFnSc2	Rio
Negro	Negro	k1gNnSc1	Negro
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
kilometrů	kilometr	k1gInPc2	kilometr
má	mít	k5eAaImIp3nS	mít
potom	potom	k6eAd1	potom
řeka	řeka	k1gFnSc1	řeka
dvě	dva	k4xCgFnPc4	dva
barvy	barva	k1gFnPc4	barva
-	-	kIx~	-
černou	černá	k1gFnSc4	černá
u	u	k7c2	u
levého	levý	k2eAgInSc2d1	levý
břehu	břeh	k1gInSc2	břeh
a	a	k8xC	a
žluto-šedou	žluto-šet	k5eAaPmIp3nP	žluto-šet
u	u	k7c2	u
pravého	pravý	k2eAgInSc2d1	pravý
břehu	břeh	k1gInSc2	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Rostlinný	rostlinný	k2eAgInSc1d1	rostlinný
i	i	k8xC	i
živočišný	živočišný	k2eAgInSc1d1	živočišný
svět	svět	k1gInSc1	svět
Amazonky	Amazonka	k1gFnSc2	Amazonka
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
bohatý	bohatý	k2eAgMnSc1d1	bohatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jezerech	jezero	k1gNnPc6	jezero
a	a	k8xC	a
průtocích	průtok	k1gInPc6	průtok
roste	růst	k5eAaImIp3nS	růst
obří	obří	k2eAgInSc4d1	obří
leknín	leknín	k1gInSc4	leknín
viktorie	viktorie	k1gFnSc2	viktorie
královská	královský	k2eAgFnSc1d1	královská
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc1	její
listy	list	k1gInPc1	list
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
obrovských	obrovský	k2eAgInPc2d1	obrovský
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
savců	savec	k1gMnPc2	savec
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
kapustňák	kapustňák	k1gMnSc1	kapustňák
jihoamerický	jihoamerický	k2eAgMnSc1d1	jihoamerický
a	a	k8xC	a
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
povodí	povodí	k1gNnSc6	povodí
delfínovec	delfínovec	k1gMnSc1	delfínovec
amazonský	amazonský	k2eAgInSc1d1	amazonský
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ryb	ryba	k1gFnPc2	ryba
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
1⁄	1⁄	k?	1⁄
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
sladkovodní	sladkovodní	k2eAgFnSc2d1	sladkovodní
fauny	fauna	k1gFnSc2	fauna
(	(	kIx(	(
<g/>
2	[number]	k4	2
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Arapaima	arapaima	k1gFnSc1	arapaima
velká	velký	k2eAgFnSc1d1	velká
dosahující	dosahující	k2eAgFnPc4d1	dosahující
délky	délka	k1gFnPc4	délka
až	až	k9	až
4	[number]	k4	4
m	m	kA	m
má	mít	k5eAaImIp3nS	mít
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
je	být	k5eAaImIp3nS	být
také	také	k9	také
dravá	dravý	k2eAgFnSc1d1	dravá
ryba	ryba	k1gFnSc1	ryba
piraňa	piraňa	k1gFnSc1	piraňa
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
zde	zde	k6eAd1	zde
také	také	k9	také
rejnoci	rejnok	k1gMnPc1	rejnok
<g/>
,	,	kIx,	,
úhoři	úhoř	k1gMnPc1	úhoř
<g/>
,	,	kIx,	,
hadi	had	k1gMnPc1	had
a	a	k8xC	a
krokodýli	krokodýl	k1gMnPc1	krokodýl
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
přítoky	přítok	k1gInPc7	přítok
představuje	představovat	k5eAaImIp3nS	představovat
Amazonka	Amazonka	k1gFnSc1	Amazonka
největší	veliký	k2eAgFnSc1d3	veliký
systém	systém	k1gInSc4	systém
vnitrozemských	vnitrozemský	k2eAgFnPc2d1	vnitrozemská
vodních	vodní	k2eAgFnPc2d1	vodní
cest	cesta	k1gFnPc2	cesta
na	na	k7c6	na
světě	svět	k1gInSc6	svět
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
více	hodně	k6eAd2	hodně
než	než	k8xS	než
25	[number]	k4	25
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
tok	tok	k1gInSc1	tok
(	(	kIx(	(
<g/>
Amazonka	Amazonka	k1gFnSc1	Amazonka
<g/>
,	,	kIx,	,
Solimõ	Solimõ	k1gFnSc1	Solimõ
<g/>
,	,	kIx,	,
Marañ	Marañ	k1gFnSc1	Marañ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
splavný	splavný	k2eAgMnSc1d1	splavný
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
4	[number]	k4	4
300	[number]	k4	300
km	km	kA	km
až	až	k8xS	až
k	k	k7c3	k
soutěsce	soutěska	k1gFnSc3	soutěska
Pongo	Pongo	k6eAd1	Pongo
de	de	k?	de
Manseriche	Manseriche	k1gInSc1	Manseriche
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
Manausu	Manaus	k1gInSc2	Manaus
(	(	kIx(	(
<g/>
1	[number]	k4	1
690	[number]	k4	690
km	km	kA	km
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
splavný	splavný	k2eAgMnSc1d1	splavný
i	i	k9	i
pro	pro	k7c4	pro
velké	velký	k2eAgFnPc4d1	velká
oceánské	oceánský	k2eAgFnPc4d1	oceánská
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgInPc1d3	nejvýznamnější
přístavy	přístav	k1gInPc1	přístav
jsou	být	k5eAaImIp3nP	být
Belém	Belý	k2eAgNnSc6d1	Belý
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
rameni	rameno	k1gNnSc6	rameno
delty	delta	k1gFnSc2	delta
Pará	Pará	k1gFnSc1	Pará
<g/>
,	,	kIx,	,
Santarém	Santarý	k2eAgNnSc6d1	Santarý
<g/>
,	,	kIx,	,
Óbidos	Óbidos	k1gMnSc1	Óbidos
<g/>
,	,	kIx,	,
Manaus	Manaus	k1gMnSc1	Manaus
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
a	a	k8xC	a
Iquitos	Iquitos	k1gInSc4	Iquitos
v	v	k7c6	v
Peru	Peru	k1gNnSc6	Peru
<g/>
.	.	kIx.	.
</s>
<s>
Hydroenergetický	Hydroenergetický	k2eAgInSc1d1	Hydroenergetický
potenciál	potenciál	k1gInSc1	potenciál
je	být	k5eAaImIp3nS	být
obrovský	obrovský	k2eAgInSc1d1	obrovský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnSc1	jeho
využití	využití	k1gNnSc1	využití
není	být	k5eNaImIp3nS	být
velké	velký	k2eAgNnSc1d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
Amazonky	Amazonka	k1gFnSc2	Amazonka
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
Španělem	Španěl	k1gMnSc7	Španěl
Vicente	Vicent	k1gInSc5	Vicent
Yáñ	Yáñ	k1gFnPc4	Yáñ
Pinzónem	Pinzón	k1gInSc7	Pinzón
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1500	[number]	k4	1500
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
řeku	řeka	k1gFnSc4	řeka
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Rio	Rio	k1gMnSc1	Rio
Santa	Santa	k1gMnSc1	Santa
Maria	Maria	k1gFnSc1	Maria
de	de	k?	de
la	la	k1gNnSc1	la
Mar	Mar	k1gMnSc1	Mar
Dulce	Dulce	k1gMnSc1	Dulce
[	[	kIx(	[
<g/>
řeka	řeka	k1gFnSc1	řeka
svaté	svatý	k2eAgFnSc2d1	svatá
Marie	Maria	k1gFnSc2	Maria
sladkého	sladký	k2eAgNnSc2d1	sladké
moře	moře	k1gNnSc2	moře
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ještě	ještě	k9	ještě
desítky	desítka	k1gFnPc1	desítka
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
břehu	břeh	k1gInSc2	břeh
byla	být	k5eAaImAgFnS	být
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
sladká	sladký	k2eAgFnSc1d1	sladká
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
podnikl	podniknout	k5eAaPmAgMnS	podniknout
jiný	jiný	k2eAgMnSc1d1	jiný
španělský	španělský	k2eAgMnSc1d1	španělský
conquistador	conquistador	k1gMnSc1	conquistador
Francisco	Francisco	k1gMnSc1	Francisco
de	de	k?	de
Orellana	Orellana	k1gFnSc1	Orellana
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1541	[number]	k4	1541
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
cestu	cesta	k1gFnSc4	cesta
pak	pak	k6eAd1	pak
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
vědec	vědec	k1gMnSc1	vědec
Charles	Charles	k1gMnSc1	Charles
Marie	Maria	k1gFnSc2	Maria
de	de	k?	de
La	la	k1gNnSc1	la
Condamine	Condamin	k1gInSc5	Condamin
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1743	[number]	k4	1743
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
průzkum	průzkum	k1gInSc1	průzkum
oblasti	oblast	k1gFnSc2	oblast
pramenů	pramen	k1gInPc2	pramen
řeky	řeka	k1gFnSc2	řeka
provedla	provést	k5eAaPmAgFnS	provést
německo-peruánská	německoeruánský	k2eAgFnSc1d1	německo-peruánský
expedice	expedice	k1gFnSc1	expedice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
českých	český	k2eAgMnPc2d1	český
výzkumníků	výzkumník	k1gMnPc2	výzkumník
byl	být	k5eAaImAgMnS	být
trutnovský	trutnovský	k2eAgMnSc1d1	trutnovský
rodák	rodák	k1gMnSc1	rodák
Samuel	Samuel	k1gMnSc1	Samuel
Fritz	Fritz	k1gMnSc1	Fritz
-	-	kIx~	-
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
,	,	kIx,	,
kartograf	kartograf	k1gMnSc1	kartograf
a	a	k8xC	a
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
jezuitský	jezuitský	k2eAgMnSc1d1	jezuitský
misionář	misionář	k1gMnSc1	misionář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1689	[number]	k4	1689
<g/>
-	-	kIx~	-
<g/>
1694	[number]	k4	1694
zmapoval	zmapovat	k5eAaPmAgInS	zmapovat
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
podrobná	podrobný	k2eAgFnSc1d1	podrobná
mapa	mapa	k1gFnSc1	mapa
Amazonky	Amazonka	k1gFnSc2	Amazonka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
tiskem	tisk	k1gInSc7	tisk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1707	[number]	k4	1707
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
Fritzova	Fritzův	k2eAgFnSc1d1	Fritzova
mapa	mapa	k1gFnSc1	mapa
má	mít	k5eAaImIp3nS	mít
rozměry	rozměr	k1gInPc4	rozměr
126	[number]	k4	126
×	×	k?	×
46	[number]	k4	46
cm	cm	kA	cm
a	a	k8xC	a
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přibližně	přibližně	k6eAd1	přibližně
měřítku	měřítko	k1gNnSc3	měřítko
1	[number]	k4	1
:	:	kIx,	:
4	[number]	k4	4
000	[number]	k4	000
000	[number]	k4	000
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
a	a	k8xC	a
hledání	hledání	k1gNnSc1	hledání
pramene	pramen	k1gInSc2	pramen
Amazonky	Amazonka	k1gFnSc2	Amazonka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zdrojnic	zdrojnice	k1gFnPc2	zdrojnice
řeky	řeka	k1gFnSc2	řeka
Lloquety	Lloqueta	k1gFnSc2	Lloqueta
(	(	kIx(	(
<g/>
-Apurímac	-Apurímac	k1gInSc1	-Apurímac
<g/>
,	,	kIx,	,
-Ucayali	-Ucayat	k5eAaPmAgMnP	-Ucayat
<g/>
,	,	kIx,	,
Amazonka	Amazonka	k1gFnSc1	Amazonka
<g/>
)	)	kIx)	)
prováděl	provádět	k5eAaImAgInS	provádět
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
a	a	k8xC	a
2000	[number]	k4	2000
vědecký	vědecký	k2eAgInSc4d1	vědecký
tým	tým	k1gInSc4	tým
prof.	prof.	kA	prof.
Bohumíra	Bohumír	k1gMnSc2	Bohumír
Janského	janský	k2eAgMnSc2d1	janský
z	z	k7c2	z
Přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
expedic	expedice	k1gFnPc2	expedice
nazvaných	nazvaný	k2eAgFnPc2d1	nazvaná
Hatun	Hatuna	k1gFnPc2	Hatuna
Mayu	Mayu	k?	Mayu
bylo	být	k5eAaImAgNnS	být
zmapování	zmapování	k1gNnSc2	zmapování
čtyř	čtyři	k4xCgInPc2	čtyři
hlavních	hlavní	k2eAgInPc2d1	hlavní
pramenných	pramenný	k2eAgInPc2d1	pramenný
toků	tok	k1gInPc2	tok
řeky	řeka	k1gFnSc2	řeka
Lloquety	Lloqueta	k1gFnSc2	Lloqueta
-	-	kIx~	-
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
řeky	řeka	k1gFnPc4	řeka
Carhuasanta	Carhuasant	k1gMnSc2	Carhuasant
<g/>
,	,	kIx,	,
Apacheta	Apachet	k1gMnSc2	Apachet
<g/>
,	,	kIx,	,
Ccaccansa	Ccaccans	k1gMnSc2	Ccaccans
a	a	k8xC	a
Sillanque	Sillanqu	k1gMnSc2	Sillanqu
<g/>
.	.	kIx.	.
</s>
<s>
Provedené	provedený	k2eAgInPc1d1	provedený
výzkumy	výzkum	k1gInPc1	výzkum
zjistily	zjistit	k5eAaPmAgInP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
a	a	k8xC	a
plošně	plošně	k6eAd1	plošně
největší	veliký	k2eAgNnSc4d3	veliký
povodí	povodí	k1gNnSc4	povodí
má	mít	k5eAaImIp3nS	mít
Carhuasanta	Carhuasanta	k1gFnSc1	Carhuasanta
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
vodnost	vodnost	k1gFnSc1	vodnost
Apacheta	Apacheto	k1gNnSc2	Apacheto
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
nadmořskou	nadmořský	k2eAgFnSc4d1	nadmořská
výšku	výška	k1gFnSc4	výška
pramene	pramen	k1gInSc2	pramen
řeka	řeka	k1gFnSc1	řeka
Ccaccansa	Ccaccansa	k1gFnSc1	Ccaccansa
<g/>
.	.	kIx.	.
</s>
<s>
Nejvzdálenějším	vzdálený	k2eAgNnSc7d3	nejvzdálenější
místem	místo	k1gNnSc7	místo
povodí	povodit	k5eAaPmIp3nS	povodit
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
Amazonky	Amazonka	k1gFnSc2	Amazonka
do	do	k7c2	do
Atlantiku	Atlantik	k1gInSc2	Atlantik
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
hory	hora	k1gFnSc2	hora
Nevado	Nevada	k1gFnSc5	Nevada
del	del	k?	del
Mismi	Mis	k1gFnPc7	Mis
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
odtéká	odtékat	k5eAaImIp3nS	odtékat
voda	voda	k1gFnSc1	voda
do	do	k7c2	do
povodí	povodí	k1gNnSc2	povodí
Carhuasanty	Carhuasanta	k1gFnSc2	Carhuasanta
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
čtyř	čtyři	k4xCgNnPc2	čtyři
hlavních	hlavní	k2eAgNnPc2d1	hlavní
kritérií	kritérion	k1gNnPc2	kritérion
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
hlavního	hlavní	k2eAgInSc2d1	hlavní
pramene	pramen	k1gInSc2	pramen
splňuje	splňovat	k5eAaImIp3nS	splňovat
dvě	dva	k4xCgFnPc4	dva
řeka	řeka	k1gFnSc1	řeka
Carhuasanta	Carhuasanta	k1gFnSc1	Carhuasanta
<g/>
.	.	kIx.	.
</s>
<s>
Zjištěné	zjištěný	k2eAgInPc1d1	zjištěný
rozdíly	rozdíl	k1gInPc1	rozdíl
však	však	k9	však
byly	být	k5eAaImAgInP	být
minimální	minimální	k2eAgFnSc1d1	minimální
a	a	k8xC	a
proto	proto	k8xC	proto
doc.	doc.	kA	doc.
Janský	janský	k2eAgMnSc1d1	janský
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
čtyřech	čtyři	k4xCgInPc6	čtyři
pramenných	pramenný	k2eAgInPc6d1	pramenný
tocích	tok	k1gInPc6	tok
řeky	řeka	k1gFnSc2	řeka
Lloquety	Lloqueta	k1gFnSc2	Lloqueta
(	(	kIx(	(
<g/>
a	a	k8xC	a
celé	celý	k2eAgFnSc2d1	celá
Amazonky	Amazonka	k1gFnSc2	Amazonka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
