<s>
Nejbližší	blízký	k2eAgMnSc1d3	nejbližší
příbuzný	příbuzný	k1gMnSc1	příbuzný
domácího	domácí	k2eAgMnSc2d1	domácí
psa	pes	k1gMnSc2	pes
je	být	k5eAaImIp3nS	být
vlk	vlk	k1gMnSc1	vlk
obecný	obecný	k2eAgMnSc1d1	obecný
–	–	k?	–
od	od	k7c2	od
psa	pes	k1gMnSc2	pes
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
v	v	k7c6	v
nanejvýš	nanejvýš	k6eAd1	nanejvýš
0,2	[number]	k4	0,2
%	%	kIx~	%
sekvence	sekvence	k1gFnSc1	sekvence
mtDNA	mtDNA	k?	mtDNA
<g/>
.	.	kIx.	.
</s>
