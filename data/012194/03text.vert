<p>
<s>
Hořké	hořký	k2eAgNnSc1d1	hořké
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
rozpuštěné	rozpuštěný	k2eAgNnSc1d1	rozpuštěné
určité	určitý	k2eAgNnSc1d1	určité
množství	množství	k1gNnSc1	množství
síranu	síran	k1gInSc2	síran
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ji	on	k3xPp3gFnSc4	on
dává	dávat	k5eAaImIp3nS	dávat
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
hořkou	hořký	k2eAgFnSc4d1	hořká
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
Egypt	Egypt	k1gInSc1	Egypt
–	–	k?	–
Velké	velká	k1gFnSc2	velká
Hořké	hořký	k2eAgNnSc1d1	hořké
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
Malé	Malé	k2eAgNnSc1d1	Malé
Hořké	hořký	k2eAgNnSc1d1	hořké
jezero	jezero	k1gNnSc1	jezero
</s>
</p>
<p>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
–	–	k?	–
Černokurinskoje	Černokurinskoj	k1gInSc2	Černokurinskoj
(	(	kIx(	(
<g/>
Altajský	altajský	k2eAgInSc1d1	altajský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hořkoslané	hořkoslaný	k2eAgNnSc1d1	hořkoslaný
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
Volgogradská	volgogradský	k2eAgFnSc1d1	Volgogradská
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Jezioro	Jeziora	k1gFnSc5	Jeziora
gorzkie	gorzkie	k1gFnPc1	gorzkie
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Sladkovodní	sladkovodní	k2eAgNnSc1d1	sladkovodní
jezero	jezero	k1gNnSc1	jezero
</s>
</p>
<p>
<s>
Slané	slaný	k2eAgNnSc1d1	slané
jezero	jezero	k1gNnSc1	jezero
</s>
</p>
<p>
<s>
Smíšené	smíšený	k2eAgNnSc1d1	smíšené
jezero	jezero	k1gNnSc1	jezero
</s>
</p>
<p>
<s>
Brakické	brakický	k2eAgNnSc1d1	brakické
jezero	jezero	k1gNnSc1	jezero
</s>
</p>
