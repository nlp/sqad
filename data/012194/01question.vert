<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
druh	druh	k1gInSc1	druh
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
rozpuštěné	rozpuštěný	k2eAgNnSc1d1	rozpuštěné
určité	určitý	k2eAgNnSc1d1	určité
množství	množství	k1gNnSc1	množství
síranu	síran	k1gInSc2	síran
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
?	?	kIx.	?
</s>
