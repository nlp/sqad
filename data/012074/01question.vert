<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
základní	základní	k2eAgFnSc1d1	základní
územní	územní	k2eAgFnSc1d1	územní
mocenská	mocenský	k2eAgFnSc1d1	mocenská
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnSc1	instituce
disponující	disponující	k2eAgFnSc2d1	disponující
mocí	moc	k1gFnSc7	moc
vládnout	vládnout	k5eAaImF	vládnout
<g/>
,	,	kIx,	,
soudit	soudit	k5eAaImF	soudit
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
zákony	zákon	k1gInPc4	zákon
společnosti	společnost	k1gFnSc2	společnost
<g/>
?	?	kIx.	?
</s>
