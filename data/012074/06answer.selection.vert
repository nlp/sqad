<s>
Stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
územní	územní	k2eAgFnSc1d1	územní
mocenská	mocenský	k2eAgFnSc1d1	mocenská
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnSc1	instituce
(	(	kIx(	(
<g/>
či	či	k8xC	či
organizace	organizace	k1gFnSc2	organizace
<g/>
)	)	kIx)	)
disponující	disponující	k2eAgFnSc7d1	disponující
mocí	moc	k1gFnSc7	moc
vládnout	vládnout	k5eAaImF	vládnout
<g/>
,	,	kIx,	,
soudit	soudit	k5eAaImF	soudit
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
zákony	zákon	k1gInPc4	zákon
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
