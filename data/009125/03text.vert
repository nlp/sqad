<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Knínice	Knínice	k1gFnSc1	Knínice
u	u	k7c2	u
Boskovic	Boskovice	k1gInPc2	Boskovice
je	být	k5eAaImIp3nS	být
územní	územní	k2eAgNnSc4d1	územní
společenství	společenství	k1gNnSc4	společenství
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
s	s	k7c7	s
farním	farní	k2eAgInSc7d1	farní
kostelem	kostel	k1gInSc7	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Marka	Marek	k1gMnSc2	Marek
v	v	k7c6	v
děkanátu	děkanát	k1gInSc6	děkanát
Boskovice	Boskovice	k1gInPc1	Boskovice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
farnosti	farnost	k1gFnSc2	farnost
patří	patřit	k5eAaImIp3nS	patřit
městys	městys	k1gInSc1	městys
Knínice	Knínice	k1gFnSc2	Knínice
a	a	k8xC	a
obce	obec	k1gFnSc2	obec
Vážany	Vážana	k1gFnSc2	Vážana
<g/>
,	,	kIx,	,
Šebetov	Šebetov	k1gInSc1	Šebetov
a	a	k8xC	a
Sudice	sudice	k1gFnSc1	sudice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
farnosti	farnost	k1gFnSc2	farnost
==	==	k?	==
</s>
</p>
<p>
<s>
Knínický	knínický	k2eAgInSc4d1	knínický
kostel	kostel	k1gInSc4	kostel
zbudovaný	zbudovaný	k2eAgInSc4d1	zbudovaný
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
staroslovanského	staroslovanský	k2eAgNnSc2d1	staroslovanské
hradiště	hradiště	k1gNnSc2	hradiště
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
středověk	středověk	k1gInSc4	středověk
jediným	jediný	k2eAgMnSc7d1	jediný
farním	farní	k2eAgInSc7d1	farní
kostelem	kostel	k1gInSc7	kostel
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
panství	panství	k1gNnSc6	panství
hradišťských	hradišťský	k2eAgMnPc2d1	hradišťský
premonstrátů	premonstrát	k1gMnPc2	premonstrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
církevní	církevní	k2eAgFnSc2d1	církevní
vrchnosti	vrchnost	k1gFnSc2	vrchnost
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
až	až	k9	až
do	do	k7c2	do
sklonku	sklonek	k1gInSc2	sklonek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
byl	být	k5eAaImAgInS	být
starý	starý	k2eAgInSc1d1	starý
kostel	kostel	k1gInSc1	kostel
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
zbořen	zbořit	k5eAaPmNgInS	zbořit
kvůli	kvůli	k7c3	kvůli
zchátralosti	zchátralost	k1gFnSc3	zchátralost
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
letech	let	k1gInPc6	let
1805	[number]	k4	1805
<g/>
–	–	k?	–
<g/>
1806	[number]	k4	1806
postaven	postaven	k2eAgInSc4d1	postaven
kostel	kostel	k1gInSc4	kostel
nový	nový	k2eAgInSc4d1	nový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Duchovní	duchovní	k2eAgMnPc1d1	duchovní
správci	správce	k1gMnPc1	správce
==	==	k?	==
</s>
</p>
<p>
<s>
Administrátorem	administrátor	k1gMnSc7	administrátor
excurrendo	excurrendo	k6eAd1	excurrendo
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2007	[number]	k4	2007
R.	R.	kA	R.
D.	D.	kA	D.
Mgr.	Mgr.	kA	Mgr.
Miroslav	Miroslav	k1gMnSc1	Miroslav
Šudoma	Šudom	k1gMnSc2	Šudom
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
července	červenec	k1gInSc2	červenec
2017	[number]	k4	2017
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
jako	jako	k9	jako
administrátor	administrátor	k1gMnSc1	administrátor
excurrendo	excurrendo	k1gNnSc1	excurrendo
R.	R.	kA	R.
D.	D.	kA	D.
Josef	Josef	k1gMnSc1	Josef
Pejř	pejř	k1gFnSc1	pejř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bohoslužby	bohoslužba	k1gFnSc2	bohoslužba
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Duchovní	duchovní	k1gMnPc1	duchovní
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
farnosti	farnost	k1gFnSc2	farnost
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
farnosti	farnost	k1gFnSc2	farnost
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
z	z	k7c2	z
Šebetova	Šebetův	k2eAgNnSc2d1	Šebetův
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
biskup	biskup	k1gMnSc1	biskup
Mořic	Mořic	k1gMnSc1	Mořic
Pícha	Pícha	k1gMnSc1	Pícha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Aktivity	aktivita	k1gFnPc1	aktivita
farnosti	farnost	k1gFnPc1	farnost
==	==	k?	==
</s>
</p>
<p>
<s>
Dnem	dnem	k7c2	dnem
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
modliteb	modlitba	k1gFnPc2	modlitba
farností	farnost	k1gFnPc2	farnost
za	za	k7c2	za
bohoslovce	bohoslovec	k1gMnSc2	bohoslovec
a	a	k8xC	a
bohoslovců	bohoslovec	k1gMnPc2	bohoslovec
za	za	k7c4	za
farnosti	farnost	k1gFnPc4	farnost
brněnské	brněnský	k2eAgFnSc2d1	brněnská
diecéze	diecéze	k1gFnSc2	diecéze
je	být	k5eAaImIp3nS	být
11	[number]	k4	11
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
<g/>
.	.	kIx.	.
</s>
<s>
Adorační	adorační	k2eAgInSc1d1	adorační
den	den	k1gInSc1	den
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Farnost	farnost	k1gFnSc1	farnost
vydává	vydávat	k5eAaPmIp3nS	vydávat
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
vlastní	vlastní	k2eAgInSc1d1	vlastní
časopis	časopis	k1gInSc1	časopis
Farníček	Farníček	k1gInSc1	Farníček
<g/>
.	.	kIx.	.
,	,	kIx,	,
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
lednu	leden	k1gInSc6	leden
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
farní	farní	k2eAgInSc1d1	farní
ples	ples	k1gInSc1	ples
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
farnosti	farnost	k1gFnSc6	farnost
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
pořádá	pořádat	k5eAaImIp3nS	pořádat
tříkrálová	tříkrálový	k2eAgFnSc1d1	Tříkrálová
sbírka	sbírka	k1gFnSc1	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
v	v	k7c6	v
Knínicích	Knínice	k1gFnPc6	Knínice
vybralo	vybrat	k5eAaPmAgNnS	vybrat
25	[number]	k4	25
792	[number]	k4	792
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
v	v	k7c6	v
Vážanech	Vážan	k1gInPc6	Vážan
14	[number]	k4	14
057	[number]	k4	057
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
v	v	k7c6	v
Šebetově	Šebetův	k2eAgMnSc6d1	Šebetův
17	[number]	k4	17
131	[number]	k4	131
korun	koruna	k1gFnPc2	koruna
a	a	k8xC	a
v	v	k7c6	v
Sudicích	sudice	k1gFnPc6	sudice
3	[number]	k4	3
645	[number]	k4	645
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
činil	činit	k5eAaImAgInS	činit
výtěžek	výtěžek	k1gInSc1	výtěžek
sbírky	sbírka	k1gFnSc2	sbírka
jen	jen	k9	jen
v	v	k7c6	v
Knínicích	Knínice	k1gFnPc6	Knínice
29	[number]	k4	29
293	[number]	k4	293
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Biskupství	biskupství	k1gNnSc1	biskupství
brněnské	brněnský	k2eAgNnSc1d1	brněnské
<g/>
.	.	kIx.	.
</s>
<s>
Katalog	katalog	k1gInSc1	katalog
brněnské	brněnský	k2eAgFnSc2d1	brněnská
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
:	:	kIx,	:
neproměnná	proměnný	k2eNgFnSc1d1	neproměnná
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Gerbrich	Gerbricha	k1gFnPc2	Gerbricha
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
;	;	kIx,	;
Doffek	Doffka	k1gFnPc2	Doffka
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
Saňová	saňový	k2eAgFnSc1d1	saňová
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Biskupství	biskupství	k1gNnSc1	biskupství
brněnské	brněnský	k2eAgNnSc1d1	brněnské
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
350	[number]	k4	350
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
254	[number]	k4	254
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
298	[number]	k4	298
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Knínice	Knínice	k1gFnSc1	Knínice
u	u	k7c2	u
Boskovic	Boskovice	k1gInPc2	Boskovice
na	na	k7c6	na
webu	web	k1gInSc6	web
Biskupství	biskupství	k1gNnSc2	biskupství
brněnského	brněnský	k2eAgInSc2d1	brněnský
</s>
</p>
<p>
<s>
Web	web	k1gInSc1	web
farnosti	farnost	k1gFnSc2	farnost
</s>
</p>
<p>
<s>
Pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
ve	v	k7c6	v
farním	farní	k2eAgInSc6d1	farní
kostele	kostel	k1gInSc6	kostel
</s>
</p>
