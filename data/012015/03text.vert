<p>
<s>
Pod	pod	k7c7	pod
junáckou	junácký	k2eAgFnSc7d1	Junácká
vlajkou	vlajka	k1gFnSc7	vlajka
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
kniha	kniha	k1gFnSc1	kniha
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
uličníkovi	uličník	k1gMnSc6	uličník
Mirkovi	Mirek	k1gMnSc6	Mirek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
do	do	k7c2	do
skautského	skautský	k2eAgInSc2d1	skautský
oddílu	oddíl	k1gInSc2	oddíl
se	s	k7c7	s
záměrem	záměr	k1gInSc7	záměr
ukrást	ukrást	k5eAaPmF	ukrást
stan	stan	k1gInSc4	stan
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
své	svůj	k3xOyFgFnSc2	svůj
party	parta	k1gFnSc2	parta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prostředí	prostředí	k1gNnSc2	prostředí
oddílu	oddíl	k1gInSc2	oddíl
ho	on	k3xPp3gMnSc4	on
okouzlí	okouzlit	k5eAaPmIp3nS	okouzlit
a	a	k8xC	a
napraví	napravit	k5eAaPmIp3nS	napravit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
příběh	příběh	k1gInSc4	příběh
později	pozdě	k6eAd2	pozdě
Foglar	Foglar	k1gInSc1	Foglar
navázal	navázat	k5eAaPmAgInS	navázat
románem	román	k1gInSc7	román
Devadesátka	devadesátka	k1gFnSc1	devadesátka
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Mirek	Mirek	k1gMnSc1	Mirek
Trojan	Trojan	k1gMnSc1	Trojan
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
party	parta	k1gFnSc2	parta
čtyř	čtyři	k4xCgMnPc2	čtyři
kluků	kluk	k1gMnPc2	kluk
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vede	vést	k5eAaImIp3nS	vést
Ondra	Ondra	k1gMnSc1	Ondra
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Mirek	Mirek	k1gMnSc1	Mirek
chce	chtít	k5eAaImIp3nS	chtít
zapůsobit	zapůsobit	k5eAaPmF	zapůsobit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
slíbí	slíbit	k5eAaPmIp3nS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
skautského	skautský	k2eAgInSc2d1	skautský
oddílu	oddíl	k1gInSc2	oddíl
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tam	tam	k6eAd1	tam
pro	pro	k7c4	pro
partu	parta	k1gFnSc4	parta
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
stan	stan	k1gInSc4	stan
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
schůzek	schůzka	k1gFnPc2	schůzka
90	[number]	k4	90
<g/>
.	.	kIx.	.
oddílu	oddíl	k1gInSc2	oddíl
(	(	kIx(	(
<g/>
zvaného	zvaný	k2eAgInSc2d1	zvaný
proto	proto	k8xC	proto
"	"	kIx"	"
<g/>
Devadesátka	devadesátka	k1gFnSc1	devadesátka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
však	však	k9	však
postupně	postupně	k6eAd1	postupně
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
skautské	skautský	k2eAgInPc1d1	skautský
hry	hra	k1gFnPc4	hra
a	a	k8xC	a
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
zamlouvají	zamlouvat	k5eAaImIp3nP	zamlouvat
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
nudné	nudný	k2eAgNnSc1d1	nudné
zevlování	zevlování	k1gNnSc1	zevlování
s	s	k7c7	s
partou	parta	k1gFnSc7	parta
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
oddíle	oddíl	k1gInSc6	oddíl
najde	najít	k5eAaPmIp3nS	najít
spoustu	spousta	k1gFnSc4	spousta
přátel	přítel	k1gMnPc2	přítel
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
chlapcem	chlapec	k1gMnSc7	chlapec
Jiřím	Jiří	k1gMnSc7	Jiří
a	a	k8xC	a
vedoucím	vedoucí	k1gMnSc7	vedoucí
Tapinem	Tapin	k1gMnSc7	Tapin
<g/>
.	.	kIx.	.
</s>
<s>
Ondra	Ondra	k1gMnSc1	Ondra
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
Mirka	Mirka	k1gFnSc1	Mirka
v	v	k7c6	v
hrsti	hrst	k1gFnSc6	hrst
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
má	mít	k5eAaImIp3nS	mít
úpis	úpis	k1gInSc1	úpis
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
Mirek	Mirek	k1gMnSc1	Mirek
ke	k	k7c3	k
krádeži	krádež	k1gFnSc3	krádež
stanu	stan	k1gInSc2	stan
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
<g/>
.	.	kIx.	.
</s>
<s>
Mirek	Mirek	k1gMnSc1	Mirek
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
cítí	cítit	k5eAaImIp3nP	cítit
být	být	k5eAaImF	být
opravdovým	opravdový	k2eAgMnSc7d1	opravdový
skautem	skaut	k1gMnSc7	skaut
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
nepředstavitelné	představitelný	k2eNgNnSc4d1	nepředstavitelné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
svůj	svůj	k3xOyFgInSc4	svůj
slib	slib	k1gInSc4	slib
splnil	splnit	k5eAaPmAgMnS	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Ondra	Ondra	k1gMnSc1	Ondra
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vážně	vážně	k6eAd1	vážně
zraní	zranit	k5eAaPmIp3nS	zranit
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
v	v	k7c6	v
dílně	dílna	k1gFnSc6	dílna
a	a	k8xC	a
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
léčí	léčit	k5eAaImIp3nP	léčit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Mirek	Mirek	k1gMnSc1	Mirek
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
dočasně	dočasně	k6eAd1	dočasně
pokoj	pokoj	k1gInSc4	pokoj
a	a	k8xC	a
odjede	odjet	k5eAaPmIp3nS	odjet
s	s	k7c7	s
oddílem	oddíl	k1gInSc7	oddíl
na	na	k7c4	na
tábor	tábor	k1gInSc4	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
si	se	k3xPyFc3	se
však	však	k9	však
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
uzdravený	uzdravený	k2eAgMnSc1d1	uzdravený
Ondra	Ondra	k1gMnSc1	Ondra
počíhá	počíhat	k5eAaPmIp3nS	počíhat
a	a	k8xC	a
vyhrožuje	vyhrožovat	k5eAaImIp3nS	vyhrožovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jestli	jestli	k8xS	jestli
mu	on	k3xPp3gMnSc3	on
Mirek	Mirek	k1gMnSc1	Mirek
stan	stan	k1gInSc4	stan
nedodá	dodat	k5eNaPmIp3nS	dodat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
okamžitě	okamžitě	k6eAd1	okamžitě
předá	předat	k5eAaPmIp3nS	předat
úpis	úpis	k1gInSc4	úpis
jejich	jejich	k3xOp3gMnSc3	jejich
vedoucímu	vedoucí	k1gMnSc3	vedoucí
<g/>
.	.	kIx.	.
</s>
<s>
Zdrcený	zdrcený	k2eAgMnSc1d1	zdrcený
Mirek	Mirek	k1gMnSc1	Mirek
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
má	mít	k5eAaImIp3nS	mít
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
sebere	sebrat	k5eAaPmIp3nS	sebrat
odvahu	odvaha	k1gFnSc4	odvaha
a	a	k8xC	a
Tapinovi	Tapinův	k2eAgMnPc1d1	Tapinův
se	se	k3xPyFc4	se
ke	k	k7c3	k
všemu	všecek	k3xTgNnSc3	všecek
přizná	přiznat	k5eAaPmIp3nS	přiznat
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Tapin	Tapin	k1gMnSc1	Tapin
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
Mirka	Mirek	k1gMnSc2	Mirek
mezitím	mezitím	k6eAd1	mezitím
už	už	k6eAd1	už
stal	stát	k5eAaPmAgMnS	stát
správný	správný	k2eAgMnSc1d1	správný
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
odpustí	odpustit	k5eAaPmIp3nP	odpustit
mu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
vyžene	vyhnat	k5eAaPmIp3nS	vyhnat
Ondru	Ondra	k1gMnSc4	Ondra
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
Mirek	Mirka	k1gFnPc2	Mirka
stává	stávat	k5eAaImIp3nS	stávat
plnoprávným	plnoprávný	k2eAgInSc7d1	plnoprávný
členem	člen	k1gInSc7	člen
skautského	skautský	k2eAgInSc2d1	skautský
oddílu	oddíl	k1gInSc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
přítel	přítel	k1gMnSc1	přítel
Jiří	Jiří	k1gMnSc1	Jiří
však	však	k9	však
oddíl	oddíl	k1gInSc4	oddíl
po	po	k7c6	po
táboře	tábor	k1gInSc6	tábor
opouští	opouštět	k5eAaImIp3nS	opouštět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vydání	vydání	k1gNnSc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
vydána	vydán	k2eAgFnSc1d1	vydána
v	v	k7c6	v
r.	r.	kA	r.
1940	[number]	k4	1940
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
zrušením	zrušení	k1gNnSc7	zrušení
skautské	skautský	k2eAgFnSc2d1	skautská
organizace	organizace	k1gFnSc2	organizace
Junáka	junák	k1gMnSc2	junák
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
r.	r.	kA	r.
1969	[number]	k4	1969
a	a	k8xC	a
poté	poté	k6eAd1	poté
až	až	k9	až
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
l.	l.	k?	l.
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
a	a	k8xC	a
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
===	===	k?	===
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
junáckou	junácký	k2eAgFnSc7d1	Junácká
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Kobes	Kobes	k1gMnSc1	Kobes
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
junáckou	junácký	k2eAgFnSc7d1	Junácká
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
junáckou	junácký	k2eAgFnSc7d1	Junácká
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7033	[number]	k4	7033
<g/>
-	-	kIx~	-
<g/>
129	[number]	k4	129
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
junáckou	junácký	k2eAgFnSc7d1	Junácká
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7033	[number]	k4	7033
<g/>
-	-	kIx~	-
<g/>
374	[number]	k4	374
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
junáckou	junácký	k2eAgFnSc7d1	Junácká
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7033	[number]	k4	7033
<g/>
-	-	kIx~	-
<g/>
437	[number]	k4	437
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
junáckou	junácký	k2eAgFnSc7d1	Junácká
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7033	[number]	k4	7033
<g/>
-	-	kIx~	-
<g/>
562	[number]	k4	562
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
junáckou	junácký	k2eAgFnSc7d1	Junácká
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7033	[number]	k4	7033
<g/>
-	-	kIx~	-
<g/>
924	[number]	k4	924
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlajkou	vlajka	k1gFnSc7	vlajka
Devadesátky	devadesátka	k1gFnSc2	devadesátka
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Jiří	Jiří	k1gMnSc1	Jiří
Stegbauer	Stegbauer	k1gMnSc1	Stegbauer
<g/>
;	;	kIx,	;
ilustrace	ilustrace	k1gFnSc1	ilustrace
Bohumír	Bohumír	k1gMnSc1	Bohumír
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
;	;	kIx,	;
doslov	doslov	k1gInSc1	doslov
Luboš	Luboš	k1gMnSc1	Luboš
Trkovský	Trkovský	k1gMnSc1	Trkovský
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Stegbauer	Stegbauer	k1gMnSc1	Stegbauer
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86289	[number]	k4	86289
<g/>
-	-	kIx~	-
<g/>
74	[number]	k4	74
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
názvem	název	k1gInSc7	název
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
-	-	kIx~	-
Obrazový	obrazový	k2eAgInSc1d1	obrazový
soupis	soupis	k1gInSc1	soupis
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
Lexikon	lexikon	k1gNnSc1	lexikon
dobrodružné	dobrodružný	k2eAgFnSc2d1	dobrodružná
literatury	literatura	k1gFnSc2	literatura
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Pod	pod	k7c7	pod
junáckou	junácký	k2eAgFnSc7d1	Junácká
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
,	,	kIx,	,
s.	s.	k?	s.
66	[number]	k4	66
-	-	kIx~	-
71	[number]	k4	71
<g/>
.	.	kIx.	.
</s>
<s>
Určeno	určen	k2eAgNnSc1d1	určeno
pro	pro	k7c4	pro
sběratelskou	sběratelský	k2eAgFnSc4d1	sběratelská
a	a	k8xC	a
badatelskou	badatelský	k2eAgFnSc4d1	badatelská
potřebu	potřeba	k1gFnSc4	potřeba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
-	-	kIx~	-
Obrazový	obrazový	k2eAgInSc1d1	obrazový
soupis	soupis	k1gInSc1	soupis
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
Lexikon	lexikon	k1gNnSc1	lexikon
dobrodružné	dobrodružný	k2eAgFnSc2d1	dobrodružná
literatury	literatura	k1gFnSc2	literatura
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Textová	textový	k2eAgFnSc1d1	textová
příloha	příloha	k1gFnSc1	příloha
<g/>
,	,	kIx,	,
s.	s.	k?	s.
337	[number]	k4	337
<g/>
.	.	kIx.	.
</s>
<s>
Určeno	určen	k2eAgNnSc1d1	určeno
pro	pro	k7c4	pro
sběratelskou	sběratelský	k2eAgFnSc4d1	sběratelská
a	a	k8xC	a
badatelskou	badatelský	k2eAgFnSc4d1	badatelská
potřebu	potřeba	k1gFnSc4	potřeba
<g/>
.	.	kIx.	.
</s>
</p>
