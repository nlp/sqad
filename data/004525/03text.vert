<s>
Zub	zub	k1gInSc1	zub
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
dens	dens	k1gInSc1	dens
<g/>
,	,	kIx,	,
řec.	řec.	k?	řec.
odus	odusit	k5eAaPmRp2nS	odusit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
útvar	útvar	k1gInSc4	útvar
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
ústní	ústní	k2eAgFnSc2d1	ústní
většiny	většina	k1gFnSc2	většina
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
slouží	sloužit	k5eAaImIp3nP	sloužit
hlavně	hlavně	k9	hlavně
k	k	k7c3	k
uchopování	uchopování	k1gNnSc3	uchopování
<g/>
,	,	kIx,	,
oddělování	oddělování	k1gNnSc3	oddělování
a	a	k8xC	a
rozmělňování	rozmělňování	k1gNnSc3	rozmělňování
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
také	také	k9	také
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
i	i	k8xC	i
útoku	útok	k1gInSc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
zubů	zub	k1gInPc2	zub
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
chrup	chrup	k1gInSc1	chrup
neboli	neboli	k8xC	neboli
dentice	dentice	k1gFnSc1	dentice
<g/>
.	.	kIx.	.
</s>
<s>
Zdravý	zdravý	k2eAgMnSc1d1	zdravý
dospělý	dospělý	k2eAgMnSc1d1	dospělý
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
32	[number]	k4	32
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
dítě	dítě	k1gNnSc4	dítě
20	[number]	k4	20
zubů	zub	k1gInPc2	zub
mléčných	mléčný	k2eAgInPc2d1	mléčný
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
kytovci	kytovec	k1gMnPc1	kytovec
(	(	kIx(	(
<g/>
jmenovitě	jmenovitě	k6eAd1	jmenovitě
kosticovci	kosticovec	k1gMnPc1	kosticovec
<g/>
)	)	kIx)	)
nemají	mít	k5eNaImIp3nP	mít
zuby	zub	k1gInPc1	zub
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
nich	on	k3xPp3gFnPc2	on
mají	mít	k5eAaImIp3nP	mít
rohovinové	rohovinový	k2eAgInPc4d1	rohovinový
pláty	plát	k1gInPc4	plát
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
doba	doba	k1gFnSc1	doba
jejich	jejich	k3xOp3gInSc2	jejich
růstu	růst	k1gInSc2	růst
-	-	kIx~	-
protože	protože	k8xS	protože
zuby	zub	k1gInPc1	zub
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
přijímání	přijímání	k1gNnSc3	přijímání
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
přizpůsobené	přizpůsobený	k2eAgInPc1d1	přizpůsobený
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
každému	každý	k3xTgMnSc3	každý
ozubenému	ozubený	k2eAgMnSc3d1	ozubený
živočichovi	živočich	k1gMnSc3	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
zuby	zub	k1gInPc4	zub
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
velmi	velmi	k6eAd1	velmi
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
býložravci	býložravec	k1gMnPc1	býložravec
mají	mít	k5eAaImIp3nP	mít
stoličky	stolička	k1gFnPc4	stolička
k	k	k7c3	k
rozmělňování	rozmělňování	k1gNnSc3	rozmělňování
tuhých	tuhý	k2eAgFnPc2d1	tuhá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
masožravci	masožravec	k1gMnPc1	masožravec
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
velké	velký	k2eAgInPc4d1	velký
špičáky	špičák	k1gInPc4	špičák
k	k	k7c3	k
zabití	zabití	k1gNnSc3	zabití
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
všech	všecek	k3xTgMnPc2	všecek
živočichů	živočich	k1gMnPc2	živočich
proto	proto	k8xC	proto
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
z	z	k7c2	z
několika	několik	k4yIc2	několik
hledisek	hledisko	k1gNnPc2	hledisko
<g/>
:	:	kIx,	:
homodontní	homodontní	k2eAgInSc1d1	homodontní
chrup	chrup	k1gInSc1	chrup
<g/>
:	:	kIx,	:
všechny	všechen	k3xTgInPc4	všechen
zuby	zub	k1gInPc4	zub
v	v	k7c6	v
čelistech	čelist	k1gFnPc6	čelist
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
tvar	tvar	k1gInSc4	tvar
(	(	kIx(	(
<g/>
příklad	příklad	k1gInSc4	příklad
<g/>
:	:	kIx,	:
dravé	dravý	k2eAgFnPc4d1	dravá
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
kytovci	kytovec	k1gMnPc1	kytovec
<g/>
)	)	kIx)	)
heterodontní	heterodontní	k2eAgInSc4d1	heterodontní
chrup	chrup	k1gInSc4	chrup
<g/>
:	:	kIx,	:
zuby	zub	k1gInPc1	zub
jsou	být	k5eAaImIp3nP	být
rozdělené	rozdělený	k2eAgInPc1d1	rozdělený
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
na	na	k7c4	na
řezáky	řezák	k1gInPc4	řezák
<g/>
,	,	kIx,	,
špičáky	špičák	k1gInPc4	špičák
<g/>
,	,	kIx,	,
premoláry	premolár	k1gInPc4	premolár
a	a	k8xC	a
moláry	molár	k1gInPc4	molár
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
zastoupených	zastoupený	k2eAgInPc2d1	zastoupený
druhů	druh	k1gInPc2	druh
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
počet	počet	k1gInSc4	počet
jsou	být	k5eAaImIp3nP	být
druhově	druhově	k6eAd1	druhově
specifické	specifický	k2eAgInPc1d1	specifický
<g/>
.	.	kIx.	.
zuby	zub	k1gInPc1	zub
s	s	k7c7	s
omezeným	omezený	k2eAgInSc7d1	omezený
růstem	růst	k1gInSc7	růst
(	(	kIx(	(
<g/>
brachyodontní	brachyodontnět	k5eAaPmIp3nS	brachyodontnět
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
prořezají	prořezat	k5eAaPmIp3nP	prořezat
z	z	k7c2	z
dásně	dáseň	k1gFnSc2	dáseň
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
dále	daleko	k6eAd2	daleko
nerostou	růst	k5eNaImIp3nP	růst
<g/>
.	.	kIx.	.
zuby	zub	k1gInPc1	zub
s	s	k7c7	s
prodlouženou	prodloužený	k2eAgFnSc7d1	prodloužená
dobou	doba	k1gFnSc7	doba
růstu	růst	k1gInSc2	růst
(	(	kIx(	(
<g/>
semihypselodotní	semihypselodotní	k2eAgMnSc1d1	semihypselodotní
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
rostou	růst	k5eAaImIp3nP	růst
i	i	k9	i
nějakou	nějaký	k3yIgFnSc4	nějaký
<g />
.	.	kIx.	.
</s>
<s>
dobu	doba	k1gFnSc4	doba
po	po	k7c4	po
prořezání	prořezání	k1gNnSc4	prořezání
zuby	zub	k1gInPc4	zub
trvale	trvale	k6eAd1	trvale
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
(	(	kIx(	(
<g/>
hypselodontní	hypselodontní	k2eAgFnSc7d1	hypselodontní
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
rostou	růst	k5eAaImIp3nP	růst
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
polyfyodontní	polyfyodontnět	k5eAaPmIp3nS	polyfyodontnět
<g/>
:	:	kIx,	:
zuby	zub	k1gInPc1	zub
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
několika	několik	k4yIc6	několik
řadách	řada	k1gFnPc6	řada
<g/>
,	,	kIx,	,
po	po	k7c6	po
vypadnutí	vypadnutí	k1gNnSc6	vypadnutí
je	být	k5eAaImIp3nS	být
zub	zub	k1gInSc1	zub
hned	hned	k6eAd1	hned
nahrazen	nahradit	k5eAaPmNgInS	nahradit
novým	nový	k2eAgInSc7d1	nový
difyodontní	difyodontní	k2eAgFnPc1d1	difyodontní
<g/>
:	:	kIx,	:
nejprve	nejprve	k6eAd1	nejprve
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
dočasný	dočasný	k2eAgInSc4d1	dočasný
<g/>
,	,	kIx,	,
mléčný	mléčný	k2eAgInSc4d1	mléčný
zub	zub	k1gInSc4	zub
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
později	pozdě	k6eAd2	pozdě
nahrazen	nahradit	k5eAaPmNgInS	nahradit
zubem	zub	k1gInSc7	zub
trvalým	trvalý	k2eAgInSc7d1	trvalý
monofyodontní	monofyodontní	k2eAgInSc1d1	monofyodontní
<g/>
:	:	kIx,	:
zub	zub	k1gInSc1	zub
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
nahrazován	nahrazovat	k5eAaImNgInS	nahrazovat
Zuby	zub	k1gInPc1	zub
člověka	člověk	k1gMnSc2	člověk
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
heterodontní	heterodontní	k2eAgFnPc1d1	heterodontní
<g/>
,	,	kIx,	,
s	s	k7c7	s
omezeným	omezený	k2eAgInSc7d1	omezený
růstem	růst	k1gInSc7	růst
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
difyodontní	difyodontní	k2eAgFnSc7d1	difyodontní
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
stoliček	stolička	k1gFnPc2	stolička
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
monofyodontní	monofyodontní	k2eAgFnPc1d1	monofyodontní
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
modifikací	modifikace	k1gFnSc7	modifikace
plakoidních	plakoidní	k2eAgFnPc2d1	plakoidní
šupin	šupina	k1gFnPc2	šupina
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
si	se	k3xPyFc3	se
tento	tento	k3xDgInSc4	tento
tvar	tvar	k1gInSc4	tvar
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
i	i	k9	i
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
obojživelníků	obojživelník	k1gMnPc2	obojživelník
a	a	k8xC	a
většiny	většina	k1gFnSc2	většina
plazů	plaz	k1gMnPc2	plaz
zuby	zub	k1gInPc7	zub
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
čelistní	čelistní	k2eAgFnSc2d1	čelistní
kosti	kost	k1gFnSc2	kost
<g/>
,	,	kIx,	,
u	u	k7c2	u
krokodýlů	krokodýl	k1gMnPc2	krokodýl
<g/>
,	,	kIx,	,
vyhynulých	vyhynulý	k2eAgMnPc2d1	vyhynulý
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
a	a	k8xC	a
savců	savec	k1gMnPc2	savec
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
jsou	být	k5eAaImIp3nP	být
zuby	zub	k1gInPc1	zub
zasazené	zasazený	k2eAgInPc1d1	zasazený
v	v	k7c6	v
zubních	zubní	k2eAgFnPc6d1	zubní
lůžcích	lůžce	k1gFnPc6	lůžce
<g/>
.	.	kIx.	.
</s>
<s>
Čelistní	čelistní	k2eAgFnSc1d1	čelistní
kost	kost	k1gFnSc1	kost
je	být	k5eAaImIp3nS	být
krytá	krytý	k2eAgFnSc1d1	krytá
dásní	dáseň	k1gFnSc7	dáseň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zuby	zub	k1gInPc4	zub
pevně	pevně	k6eAd1	pevně
obemyká	obemykat	k5eAaImIp3nS	obemykat
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
všechny	všechen	k3xTgInPc4	všechen
brachiodontní	brachiodontní	k2eAgInPc4d1	brachiodontní
zuby	zub	k1gInPc4	zub
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
z	z	k7c2	z
kořene	kořen	k1gInSc2	kořen
<g/>
,	,	kIx,	,
krčku	krček	k1gInSc2	krček
a	a	k8xC	a
korunky	korunka	k1gFnSc2	korunka
<g/>
.	.	kIx.	.
</s>
<s>
Korunka	korunka	k1gFnSc1	korunka
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
část	část	k1gFnSc1	část
zubu	zub	k1gInSc2	zub
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyčnívá	vyčnívat	k5eAaImIp3nS	vyčnívat
ze	z	k7c2	z
zubního	zubní	k2eAgNnSc2d1	zubní
lůžka	lůžko	k1gNnSc2	lůžko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
sklovinou	sklovina	k1gFnSc7	sklovina
<g/>
,	,	kIx,	,
nejtvrdší	tvrdý	k2eAgFnSc7d3	nejtvrdší
látkou	látka	k1gFnSc7	látka
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Sklovina	sklovina	k1gFnSc1	sklovina
je	být	k5eAaImIp3nS	být
tvořená	tvořený	k2eAgFnSc1d1	tvořená
mineralizovanými	mineralizovaný	k2eAgInPc7d1	mineralizovaný
hranoly	hranol	k1gInPc7	hranol
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
odolná	odolný	k2eAgFnSc1d1	odolná
a	a	k8xC	a
při	při	k7c6	při
poškození	poškození	k1gNnSc6	poškození
nemá	mít	k5eNaImIp3nS	mít
schopnost	schopnost	k1gFnSc4	schopnost
regenerace	regenerace	k1gFnSc2	regenerace
<g/>
.	.	kIx.	.
</s>
<s>
Vrstva	vrstva	k1gFnSc1	vrstva
skloviny	sklovina	k1gFnSc2	sklovina
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
jeden	jeden	k4xCgMnSc1	jeden
až	až	k9	až
tři	tři	k4xCgInPc1	tři
milimetry	milimetr	k1gInPc1	milimetr
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
hmoty	hmota	k1gFnSc2	hmota
zubu	zub	k1gInSc2	zub
tvoří	tvořit	k5eAaImIp3nS	tvořit
zubovina	zubovina	k1gFnSc1	zubovina
<g/>
,	,	kIx,	,
žlutobílá	žlutobílý	k2eAgFnSc1d1	žlutobílá
hmota	hmota	k1gFnSc1	hmota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
kosti	kost	k1gFnSc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
zubu	zub	k1gInSc2	zub
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
dřeňová	dřeňový	k2eAgFnSc1d1	dřeňová
dutina	dutina	k1gFnSc1	dutina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
je	být	k5eAaImIp3nS	být
zubní	zubní	k2eAgFnSc1d1	zubní
dřeň	dřeň	k1gFnSc1	dřeň
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
zubní	zubní	k2eAgFnSc2d1	zubní
dřeně	dřeň	k1gFnSc2	dřeň
kanálky	kanálka	k1gFnSc2	kanálka
v	v	k7c6	v
kořenech	kořen	k1gInPc6	kořen
zubu	zub	k1gInSc2	zub
pronikají	pronikat	k5eAaImIp3nP	pronikat
drobné	drobný	k2eAgFnPc4d1	drobná
cévy	céva	k1gFnPc4	céva
a	a	k8xC	a
také	také	k9	také
nervy	nerv	k1gInPc1	nerv
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
příčinou	příčina	k1gFnSc7	příčina
citlivosti	citlivost	k1gFnSc2	citlivost
zubu	zub	k1gInSc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Zubní	zubní	k2eAgInSc1d1	zubní
cement	cement	k1gInSc1	cement
je	být	k5eAaImIp3nS	být
vláknitá	vláknitý	k2eAgFnSc1d1	vláknitá
kost	kost	k1gFnSc1	kost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
tenké	tenký	k2eAgFnSc6d1	tenká
vrstvě	vrstva	k1gFnSc6	vrstva
kryje	krýt	k5eAaImIp3nS	krýt
kořen	kořen	k1gInSc4	kořen
zubu	zub	k1gInSc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
dospělých	dospělí	k1gMnPc2	dospělí
přirozeně	přirozeně	k6eAd1	přirozeně
tmavnou	tmavnout	k5eAaImIp3nP	tmavnout
během	během	k7c2	během
zrání	zrání	k1gNnSc2	zrání
<g/>
,	,	kIx,	,
zubní	zubní	k2eAgFnSc1d1	zubní
dřeň	dřeň	k1gFnSc1	dřeň
v	v	k7c6	v
zubech	zub	k1gInPc6	zub
se	se	k3xPyFc4	se
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nahrazován	nahrazovat	k5eAaImNgInS	nahrazovat
dentinem	dentin	k1gInSc7	dentin
(	(	kIx(	(
<g/>
zubovinou	zubovina	k1gFnSc7	zubovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lidského	lidský	k2eAgInSc2d1	lidský
zárodku	zárodek	k1gInSc2	zárodek
se	se	k3xPyFc4	se
chrup	chrup	k1gInSc1	chrup
začíná	začínat	k5eAaImIp3nS	začínat
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
asi	asi	k9	asi
v	v	k7c6	v
šestém	šestý	k4xOgInSc6	šestý
měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
šestého	šestý	k4xOgNnSc2	šestý
do	do	k7c2	do
osmého	osmý	k4xOgInSc2	osmý
měsíce	měsíc	k1gInSc2	měsíc
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
prořezávání	prořezávání	k1gNnSc3	prořezávání
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
roste	růst	k5eAaImIp3nS	růst
zubní	zubní	k2eAgInSc4d1	zubní
kořen	kořen	k1gInSc4	kořen
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
tlačí	tlačit	k5eAaImIp3nS	tlačit
korunku	korunka	k1gFnSc4	korunka
skrze	skrze	k?	skrze
dáseň	dáseň	k1gFnSc1	dáseň
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
mléčné	mléčný	k2eAgInPc1d1	mléčný
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
dočasným	dočasný	k2eAgInSc7d1	dočasný
souborem	soubor	k1gInSc7	soubor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
chybí	chybět	k5eAaImIp3nP	chybět
třenové	třenový	k2eAgInPc4d1	třenový
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
zuby	zub	k1gInPc4	zub
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
.	.	kIx.	.
</s>
<s>
Říkáme	říkat	k5eAaImIp1nP	říkat
mu	on	k3xPp3gNnSc3	on
dentes	dentes	k1gInSc1	dentes
decidui	decidu	k1gFnSc2	decidu
nebo	nebo	k8xC	nebo
dentes	dentesa	k1gFnPc2	dentesa
lactei	lacte	k1gFnSc2	lacte
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
prořezávají	prořezávat	k5eAaImIp3nP	prořezávat
dolní	dolní	k2eAgInPc4d1	dolní
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
řezáky	řezák	k1gInPc4	řezák
a	a	k8xC	a
pak	pak	k6eAd1	pak
horní	horní	k2eAgInPc4d1	horní
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
řezáky	řezák	k1gInPc4	řezák
<g/>
.	.	kIx.	.
</s>
<s>
Stálý	stálý	k2eAgInSc1d1	stálý
chrup	chrup	k1gInSc1	chrup
(	(	kIx(	(
<g/>
dentes	dentes	k1gMnSc1	dentes
permanentes	permanentes	k1gMnSc1	permanentes
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mléčným	mléčný	k2eAgInSc7d1	mléčný
chrupem	chrup	k1gInSc7	chrup
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
do	do	k7c2	do
pátého	pátý	k4xOgNnSc2	pátý
<g/>
,	,	kIx,	,
šestého	šestý	k4xOgNnSc2	šestý
až	až	k6eAd1	až
sedmého	sedmý	k4xOgInSc2	sedmý
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jejich	jejich	k3xOp3gInSc1	jejich
růst	růst	k1gInSc1	růst
naruší	narušit	k5eAaPmIp3nS	narušit
kořeny	kořen	k1gInPc4	kořen
dočasných	dočasný	k2eAgInPc2d1	dočasný
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
vypadnutí	vypadnutí	k1gNnSc3	vypadnutí
<g/>
.	.	kIx.	.
</s>
<s>
Stálý	stálý	k2eAgInSc1d1	stálý
chrup	chrup	k1gInSc1	chrup
roste	růst	k5eAaImIp3nS	růst
do	do	k7c2	do
čtrnáctého	čtrnáctý	k4xOgInSc2	čtrnáctý
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
prořezávají	prořezávat	k5eAaImIp3nP	prořezávat
"	"	kIx"	"
<g/>
sedmičky	sedmička	k1gFnPc1	sedmička
<g/>
"	"	kIx"	"
–	–	k?	–
druhé	druhý	k4xOgFnSc2	druhý
stoličky	stolička	k1gFnSc2	stolička
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
navíc	navíc	k6eAd1	navíc
zuby	zub	k1gInPc4	zub
třenové	třenový	k2eAgInPc4d1	třenový
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
rostou	růst	k5eAaImIp3nP	růst
hned	hned	k6eAd1	hned
za	za	k7c4	za
špičáky	špičák	k1gInPc4	špičák
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
za	za	k7c7	za
nimi	on	k3xPp3gFnPc7	on
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
stoličky	stolička	k1gFnPc1	stolička
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
moudrosti	moudrost	k1gFnSc2	moudrost
prořezávají	prořezávat	k5eAaImIp3nP	prořezávat
jako	jako	k9	jako
poslední	poslední	k2eAgInSc4d1	poslední
od	od	k7c2	od
patnáctého	patnáctý	k4xOgInSc2	patnáctý
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
lidského	lidský	k2eAgInSc2d1	lidský
chrupu	chrup	k1gInSc2	chrup
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
složitý	složitý	k2eAgInSc1d1	složitý
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
počet	počet	k1gInSc1	počet
lidských	lidský	k2eAgInPc2d1	lidský
zubů	zub	k1gInPc2	zub
byl	být	k5eAaImAgInS	být
44	[number]	k4	44
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
22	[number]	k4	22
zubů	zub	k1gInPc2	zub
v	v	k7c4	v
horní	horní	k2eAgFnSc4d1	horní
a	a	k8xC	a
22	[number]	k4	22
zubů	zub	k1gInPc2	zub
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
čelisti	čelist	k1gFnSc6	čelist
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupem	postup	k1gInSc7	postup
vývoje	vývoj	k1gInSc2	vývoj
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
s	s	k7c7	s
průmyslem	průmysl	k1gInSc7	průmysl
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
s	s	k7c7	s
tepelnou	tepelný	k2eAgFnSc7d1	tepelná
úpravou	úprava	k1gFnSc7	úprava
jídel	jídlo	k1gNnPc2	jídlo
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
a	a	k8xC	a
tvar	tvar	k1gInSc1	tvar
zubů	zub	k1gInPc2	zub
upravoval	upravovat	k5eAaImAgInS	upravovat
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
ustáleno	ustálen	k2eAgNnSc1d1	ustáleno
32	[number]	k4	32
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Odchylky	odchylka	k1gFnPc1	odchylka
se	se	k3xPyFc4	se
však	však	k9	však
nemusí	muset	k5eNaImIp3nS	muset
týkat	týkat	k5eAaImF	týkat
pouze	pouze	k6eAd1	pouze
počtu	počet	k1gInSc2	počet
zubů	zub	k1gInPc2	zub
–	–	k?	–
nadpočetné	nadpočetný	k2eAgInPc4d1	nadpočetný
zuby	zub	k1gInPc4	zub
<g/>
,	,	kIx,	,
menší	malý	k2eAgInPc4d2	menší
počet	počet	k1gInSc4	počet
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
kvality	kvalita	k1gFnSc2	kvalita
skloviny	sklovina	k1gFnSc2	sklovina
<g/>
,	,	kIx,	,
či	či	k8xC	či
zuboviny	zubovina	k1gFnPc1	zubovina
ať	ať	k9	ať
již	již	k6eAd1	již
vrozené	vrozený	k2eAgNnSc1d1	vrozené
<g/>
,	,	kIx,	,
či	či	k8xC	či
získané	získaný	k2eAgInPc1d1	získaný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
častá	častý	k2eAgFnSc1d1	častá
odchylka	odchylka	k1gFnSc1	odchylka
vývoje	vývoj	k1gInSc2	vývoj
chrupu	chrup	k1gInSc2	chrup
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
chybějí	chybět	k5eAaImIp3nP	chybět
třetí	třetí	k4xOgFnPc1	třetí
stoličky	stolička	k1gFnPc1	stolička
stálého	stálý	k2eAgInSc2d1	stálý
chrupu	chrup	k1gInSc2	chrup
–	–	k?	–
zuby	zub	k1gInPc1	zub
moudrosti	moudrost	k1gFnSc3	moudrost
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
tak	tak	k8xS	tak
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
čelisti	čelist	k1gFnSc6	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
moudrosti	moudrost	k1gFnSc2	moudrost
mohou	moct	k5eAaImIp3nP	moct
růst	růst	k5eAaImF	růst
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
často	často	k6eAd1	často
chybějícím	chybějící	k2eAgInSc7d1	chybějící
zubem	zub	k1gInSc7	zub
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgInSc1	druhý
třenový	třenový	k2eAgInSc1d1	třenový
zub	zub	k1gInSc1	zub
–	–	k?	–
premolár	premolár	k1gInSc1	premolár
<g/>
,	,	kIx,	,
častěji	často	k6eAd2	často
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
čelisti	čelist	k1gFnSc6	čelist
<g/>
,	,	kIx,	,
třetím	třetí	k4xOgInSc7	třetí
chybějícím	chybějící	k2eAgInSc7d1	chybějící
zubem	zub	k1gInSc7	zub
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
postranní	postranní	k2eAgInPc1d1	postranní
řezáky	řezák	k1gInPc1	řezák
<g/>
,	,	kIx,	,
častěji	často	k6eAd2	často
horní	horní	k2eAgFnSc1d1	horní
<g/>
.	.	kIx.	.
</s>
<s>
Býložravci	býložravec	k1gMnPc1	býložravec
mají	mít	k5eAaImIp3nP	mít
zuby	zub	k1gInPc4	zub
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
rostou	růst	k5eAaImIp3nP	růst
i	i	k9	i
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
rostliny	rostlina	k1gFnPc1	rostlina
jsou	být	k5eAaImIp3nP	být
tuhé	tuhý	k2eAgFnPc1d1	tuhá
<g/>
,	,	kIx,	,
těžko	těžko	k6eAd1	těžko
stravitelné	stravitelný	k2eAgInPc1d1	stravitelný
a	a	k8xC	a
zuby	zub	k1gInPc1	zub
se	se	k3xPyFc4	se
tak	tak	k9	tak
snadno	snadno	k6eAd1	snadno
obrušují	obrušovat	k5eAaImIp3nP	obrušovat
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
trvale	trvale	k6eAd1	trvale
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
zubů	zub	k1gInPc2	zub
jsou	být	k5eAaImIp3nP	být
řezáky	řezák	k1gInPc1	řezák
hlodavců	hlodavec	k1gMnPc2	hlodavec
nebo	nebo	k8xC	nebo
zajícovců	zajícovec	k1gMnPc2	zajícovec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kly	kel	k1gInPc1	kel
prasat	prase	k1gNnPc2	prase
a	a	k8xC	a
slonů	slon	k1gMnPc2	slon
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
s	s	k7c7	s
prodlouženou	prodloužený	k2eAgFnSc7d1	prodloužená
dobou	doba	k1gFnSc7	doba
růstu	růst	k1gInSc2	růst
jsou	být	k5eAaImIp3nP	být
stoličky	stolička	k1gFnPc1	stolička
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
<g/>
,	,	kIx,	,
hlodavců	hlodavec	k1gMnPc2	hlodavec
<g/>
,	,	kIx,	,
koně	kůň	k1gMnSc2	kůň
nebo	nebo	k8xC	nebo
slona	slon	k1gMnSc2	slon
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
zuby	zub	k1gInPc1	zub
nemají	mít	k5eNaImIp3nP	mít
krček	krček	k1gInSc1	krček
<g/>
,	,	kIx,	,
sklovina	sklovina	k1gFnSc1	sklovina
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
celé	celý	k2eAgNnSc4d1	celé
tělo	tělo	k1gNnSc4	tělo
zubu	zub	k1gInSc2	zub
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
netvoří	tvořit	k5eNaImIp3nS	tvořit
horní	horní	k2eAgFnSc4d1	horní
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
krytá	krytý	k2eAgNnPc4d1	kryté
cementem	cement	k1gInSc7	cement
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
zuby	zub	k1gInPc1	zub
s	s	k7c7	s
prodlouženou	prodloužený	k2eAgFnSc7d1	prodloužená
dobou	doba	k1gFnSc7	doba
růstu	růst	k1gInSc2	růst
nejsou	být	k5eNaImIp3nP	být
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
čtyři	čtyři	k4xCgInPc1	čtyři
základní	základní	k2eAgInPc1d1	základní
druhy	druh	k1gInPc1	druh
zubů	zub	k1gInPc2	zub
<g/>
:	:	kIx,	:
Stoličky	stolička	k1gFnPc1	stolička
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
rozmělňování	rozmělňování	k1gNnSc3	rozmělňování
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
jich	on	k3xPp3gMnPc2	on
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ploché	plochý	k2eAgInPc1d1	plochý
s	s	k7c7	s
malými	malý	k2eAgInPc7d1	malý
hrbolky	hrbolek	k1gInPc7	hrbolek
Třenové	třenový	k2eAgInPc4d1	třenový
zuby	zub	k1gInPc4	zub
podobné	podobný	k2eAgInPc4d1	podobný
stoličkám	stolička	k1gFnPc3	stolička
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnPc1d2	menší
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
méně	málo	k6eAd2	málo
kořenů	kořen	k1gInPc2	kořen
a	a	k8xC	a
méně	málo	k6eAd2	málo
hrbolků	hrbolek	k1gInPc2	hrbolek
Špičáky	špičák	k1gInPc7	špičák
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
trhání	trhání	k1gNnSc3	trhání
potravy	potrava	k1gFnSc2	potrava
Řezáky	řezák	k1gInPc4	řezák
slouží	sloužit	k5eAaImIp3nP	sloužit
<g />
.	.	kIx.	.
</s>
<s>
k	k	k7c3	k
uchopování	uchopování	k1gNnSc3	uchopování
a	a	k8xC	a
krájení	krájení	k1gNnSc3	krájení
potravy	potrava	k1gFnSc2	potrava
V	v	k7c6	v
anatomii	anatomie	k1gFnSc6	anatomie
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
lidské	lidský	k2eAgInPc1d1	lidský
zuby	zub	k1gInPc1	zub
popisovány	popisován	k2eAgInPc1d1	popisován
jako	jako	k8xS	jako
<g/>
:	:	kIx,	:
I	i	k9	i
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
Dens	Dens	k1gInSc1	Dens
incisivus	incisivus	k1gMnSc1	incisivus
medialis	medialis	k1gInSc1	medialis
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
řezák	řezák	k1gInSc1	řezák
<g/>
)	)	kIx)	)
I	i	k9	i
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
Dens	Dens	k1gInSc1	Dens
incisivus	incisivus	k1gMnSc1	incisivus
lateralis	lateralis	k1gInSc1	lateralis
(	(	kIx(	(
<g/>
druhý	druhý	k4xOgInSc1	druhý
řezák	řezák	k1gInSc1	řezák
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
:	:	kIx,	:
Dens	Dens	k1gInSc1	Dens
caninus	caninus	k1gInSc1	caninus
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
špičák	špičák	k1gMnSc1	špičák
<g/>
)	)	kIx)	)
P	P	kA	P
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
Dens	Dens	k1gInSc1	Dens
premolaris	premolaris	k1gInSc1	premolaris
primus	primus	k1gInSc1	primus
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
třenový	třenový	k2eAgInSc1d1	třenový
zub	zub	k1gInSc1	zub
<g/>
)	)	kIx)	)
P	P	kA	P
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
Dens	Dens	k1gInSc1	Dens
premolaris	premolaris	k1gInSc1	premolaris
secundus	secundus	k1gInSc1	secundus
(	(	kIx(	(
<g/>
druhý	druhý	k4xOgInSc1	druhý
třenový	třenový	k2eAgInSc1d1	třenový
zub	zub	k1gInSc1	zub
<g/>
)	)	kIx)	)
M	M	kA	M
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
Dens	Dens	k1gInSc1	Dens
molaris	molaris	k1gInSc1	molaris
primus	primus	k1gInSc4	primus
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
stolička	stolička	k1gFnSc1	stolička
<g/>
)	)	kIx)	)
M	M	kA	M
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
Dens	Dens	k1gInSc1	Dens
molaris	molaris	k1gInSc1	molaris
secundus	secundus	k1gInSc4	secundus
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
stolička	stolička	k1gFnSc1	stolička
<g/>
)	)	kIx)	)
M	M	kA	M
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
Dens	Densa	k1gFnPc2	Densa
molaris	molaris	k1gInSc1	molaris
tertius	tertius	k1gMnSc1	tertius
(	(	kIx(	(
<g/>
Dens	Dens	k1gInSc1	Dens
serotinus	serotinus	k1gInSc1	serotinus
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
stolička	stolička	k1gFnSc1	stolička
<g/>
,	,	kIx,	,
zub	zub	k1gInSc1	zub
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
)	)	kIx)	)
zubní	zubní	k2eAgInSc4d1	zubní
plak	plak	k1gInSc4	plak
zubní	zubní	k2eAgInSc4d1	zubní
kámen	kámen	k1gInSc4	kámen
zubní	zubní	k2eAgInSc4d1	zubní
kaz	kaz	k1gInSc4	kaz
parodontitida	parodontitida	k1gFnSc1	parodontitida
(	(	kIx(	(
<g/>
parodontóza	parodontóza	k1gFnSc1	parodontóza
<g/>
)	)	kIx)	)
</s>
