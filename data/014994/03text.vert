<s>
Lefkada	Lefkada	k1gFnSc1
</s>
<s>
LefkadaΛ	LefkadaΛ	k?
(	(	kIx(
<g/>
řecky	řecky	k6eAd1
<g/>
)	)	kIx)
Satelitní	satelitní	k2eAgInSc1d1
snímek	snímek	k1gInSc1
ostrova	ostrov	k1gInSc2
</s>
<s>
Lefkada	Lefkada	k1gFnSc1
</s>
<s>
Lefkada	Lefkada	k1gFnSc1
<g/>
,	,	kIx,
Řecko	Řecko	k1gNnSc1
Lokalizace	lokalizace	k1gFnSc2
</s>
<s>
Jónské	jónský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
Stát	stát	k1gInSc1
</s>
<s>
Řecko	Řecko	k1gNnSc1
Řecko	Řecko	k1gNnSc1
•	•	k?
Reg.	Reg.	k1gFnSc1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Lefkada	Lefkada	k1gFnSc1
Topografie	topografie	k1gFnSc1
Rozloha	rozloha	k1gFnSc1
</s>
<s>
356	#num#	k4
km²	km²	k?
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
38	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
20	#num#	k4
<g/>
°	°	k?
<g/>
38	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
</s>
<s>
Stavrota	Stavrota	k1gFnSc1
Osídlení	osídlení	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
23	#num#	k4
000	#num#	k4
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
64,6	64,6	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Největší	veliký	k2eAgNnSc1d3
sídlo	sídlo	k1gNnSc1
</s>
<s>
Lefkada	Lefkada	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lefkada	Lefkada	k1gFnSc1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
též	též	k9
Lefkáda	Lefkáda	k1gFnSc1
(	(	kIx(
<g/>
řecky	řecky	k6eAd1
Λ	Λ	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
řecký	řecký	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
v	v	k7c6
souostroví	souostroví	k1gNnSc6
Jónských	jónský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
o	o	k7c6
rozloze	rozloha	k1gFnSc6
356	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
délka	délka	k1gFnSc1
pláží	pláž	k1gFnPc2
je	být	k5eAaImIp3nS
117	#num#	k4
km	km	kA
a	a	k8xC
na	na	k7c6
ostrově	ostrov	k1gInSc6
žije	žít	k5eAaImIp3nS
23	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
položen	položit	k5eAaPmNgInS
mezi	mezi	k7c7
ostrovy	ostrov	k1gInPc7
Korfu	Korfu	k1gNnSc2
a	a	k8xC
Kefalonie	Kefalonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
je	být	k5eAaImIp3nS
vrchol	vrchol	k1gInSc4
Stavrota	Stavrota	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ostrově	ostrov	k1gInSc6
je	být	k5eAaImIp3nS
mírné	mírný	k2eAgNnSc1d1
středomořské	středomořský	k2eAgNnSc1d1
podnebí	podnebí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Ostrov	ostrov	k1gInSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
řecké	řecký	k2eAgFnSc2d1
regionální	regionální	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
Lefkada	Lefkada	k1gFnSc1
a	a	k8xC
je	být	k5eAaImIp3nS
rozdělen	rozdělit	k5eAaPmNgInS
do	do	k7c2
dvou	dva	k4xCgInPc2
krajů	kraj	k1gInPc2
a	a	k8xC
šesti	šest	k4xCc2
okresů	okres	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
protější	protější	k2eAgFnSc7d1
pevninskou	pevninský	k2eAgFnSc7d1
Akarnánií	Akarnánie	k1gFnSc7
je	být	k5eAaImIp3nS
propojen	propojit	k5eAaPmNgInS
umělou	umělý	k2eAgFnSc7d1
hrází	hráz	k1gFnSc7
a	a	k8xC
zvedacím	zvedací	k2eAgInSc7d1
mostem	most	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
ostrova	ostrov	k1gInSc2
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
z	z	k7c2
řeckého	řecký	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
Lefkos	Lefkos	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
bílý	bílý	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrov	ostrov	k1gInSc1
byl	být	k5eAaImAgInS
tak	tak	k6eAd1
pojmenován	pojmenovat	k5eAaPmNgInS
díky	díky	k7c3
bílým	bílý	k2eAgFnPc3d1
skalám	skála	k1gFnPc3
<g/>
,	,	kIx,
nacházejícím	nacházející	k2eAgMnSc7d1
se	se	k3xPyFc4
na	na	k7c6
jihu	jih	k1gInSc6
ostrova	ostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Slavná	slavný	k2eAgFnSc1d1
pláž	pláž	k1gFnSc1
Porto	porto	k1gNnSc1
Katsiki	Katsik	k1gFnSc2
</s>
<s>
Díky	díky	k7c3
svým	svůj	k3xOyFgFnPc3
plážím	pláž	k1gFnPc3
patří	patřit	k5eAaImIp3nS
ostrov	ostrov	k1gInSc4
k	k	k7c3
turisticky	turisticky	k6eAd1
významným	významný	k2eAgFnPc3d1
destinacím	destinace	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centrem	centrum	k1gNnSc7
ostrova	ostrov	k1gInSc2
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
Lefkada	Lefkada	k1gFnSc1
<g/>
,	,	kIx,
ležící	ležící	k2eAgFnSc1d1
na	na	k7c6
severu	sever	k1gInSc6
ostrova	ostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vnitrozemí	vnitrozemí	k1gNnSc1
ostrova	ostrov	k1gInSc2
je	být	k5eAaImIp3nS
velice	velice	k6eAd1
hornaté	hornatý	k2eAgInPc4d1
s	s	k7c7
vrcholy	vrchol	k1gInPc7
o	o	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
okolo	okolo	k7c2
1000	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Ostrov	ostrov	k1gInSc1
má	mít	k5eAaImIp3nS
také	také	k9
bohaté	bohatý	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
spodních	spodní	k2eAgFnPc2d1
vod	voda	k1gFnPc2
<g/>
,	,	kIx,
díky	díky	k7c3
nimž	jenž	k3xRgFnPc3
má	mít	k5eAaImIp3nS
bohatou	bohatý	k2eAgFnSc4d1
faunu	fauna	k1gFnSc4
a	a	k8xC
flóru	flóra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Lefkada	Lefkada	k1gFnSc1
</s>
<s>
Dnešní	dnešní	k2eAgNnSc1d1
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
je	být	k5eAaImIp3nS
postupně	postupně	k6eAd1
třetím	třetí	k4xOgMnSc7
hlavním	hlavní	k2eAgMnSc7d1
městem	město	k1gNnSc7
na	na	k7c6
ostrově	ostrov	k1gInSc6
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
bylo	být	k5eAaImAgNnS
Nyrikos	Nyrikosa	k1gFnPc2
<g/>
,	,	kIx,
druhým	druhý	k4xOgNnSc7
středověké	středověký	k2eAgNnSc4d1
Santa	Santa	k1gMnSc1
Maura	Maur	k1gMnSc2
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
roku	rok	k1gInSc3
1684	#num#	k4
přesunuto	přesunout	k5eAaPmNgNnS
na	na	k7c4
místo	místo	k1gNnSc4
kde	kde	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byla	být	k5eAaImAgFnS
pouze	pouze	k6eAd1
rybářská	rybářský	k2eAgFnSc1d1
osada	osada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Benátčané	Benátčan	k1gMnPc1
při	při	k7c6
jeho	jeho	k3xOp3gFnSc6
výstavbě	výstavba	k1gFnSc6
uplatnili	uplatnit	k5eAaPmAgMnP
urbanistický	urbanistický	k2eAgInSc4d1
styl	styl	k1gInSc4
běžný	běžný	k2eAgInSc4d1
v	v	k7c6
středověké	středověký	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
téměř	téměř	k6eAd1
zničujícím	zničující	k2eAgNnSc6d1
zemětřesení	zemětřesení	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1825	#num#	k4
bylo	být	k5eAaImAgNnS
znovu	znovu	k6eAd1
vystavěno	vystavěn	k2eAgNnSc1d1
podle	podle	k7c2
přísných	přísný	k2eAgNnPc2d1
anglických	anglický	k2eAgNnPc2d1
antiseismických	antiseismický	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Hrad	hrad	k1gInSc1
Santa	Sant	k1gMnSc2
Maura	Maur	k1gMnSc2
</s>
<s>
Hrad	hrad	k1gInSc1
Santa	Sant	k1gMnSc2
Maura	Maur	k1gMnSc2
je	být	k5eAaImIp3nS
středověká	středověký	k2eAgFnSc1d1
šestiboká	šestiboký	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
se	s	k7c7
sedmi	sedm	k4xCc7
baštami	bašta	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrad	hrad	k1gInSc1
je	být	k5eAaImIp3nS
vystavěn	vystavět	k5eAaPmNgInS
na	na	k7c6
výběžku	výběžek	k1gInSc6
země	zem	k1gFnSc2
velikém	veliký	k2eAgInSc6d1
45	#num#	k4
m³	m³	k?
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vybíhá	vybíhat	k5eAaImIp3nS
do	do	k7c2
Jónského	jónský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
příkopu	příkop	k1gInSc3
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
výběžek	výběžek	k1gInSc1
stal	stát	k5eAaPmAgInS
umělým	umělý	k2eAgInSc7d1
ostrovem	ostrov	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centrum	centrum	k1gNnSc1
hradu	hrad	k1gInSc2
je	být	k5eAaImIp3nS
ze	z	k7c2
14	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
a	a	k8xC
bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
franským	franský	k2eAgNnSc7d1
knížetem	kníže	k1gNnSc7wR
Ioannisem	Ioannis	k1gInSc7
Orsiemem	Orsiem	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
dostal	dostat	k5eAaPmAgInS
Lefkadu	Lefkad	k1gInSc2
věnem	věno	k1gNnSc7
od	od	k7c2
epirského	epirský	k2eAgMnSc2d1
despoty	despota	k1gMnSc2
Nikofera	Nikofero	k1gNnSc2
I.	I.	kA
</s>
<s>
Sídla	sídlo	k1gNnPc1
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
Nydri	Nydre	k1gFnSc4
</s>
<s>
SídlaYPES	SídlaYPES	k?
kódMístoSměrovací	kódMístoSměrovací	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
</s>
<s>
Apollonion	Apollonion	k1gInSc1
<g/>
3601	#num#	k4
<g/>
Vasiliki	Vasilik	k1gFnSc2
<g/>
310	#num#	k4
82	#num#	k4
</s>
<s>
Ellomenos	Ellomenos	k1gInSc1
<g/>
3602	#num#	k4
<g/>
Nydri	Nydr	k1gFnSc2
<g/>
311	#num#	k4
00	#num#	k4
</s>
<s>
Karya	Karya	k6eAd1
<g/>
3604310	#num#	k4
80	#num#	k4
</s>
<s>
Lefkada	Lefkada	k1gFnSc1
(	(	kIx(
<g/>
město	město	k1gNnSc1
<g/>
)	)	kIx)
<g/>
3606311	#num#	k4
00	#num#	k4
</s>
<s>
Meganisi	Meganis	k1gMnSc3
<g/>
3607	#num#	k4
<g/>
Katomeri	Katomer	k1gFnSc2
<g/>
310	#num#	k4
83	#num#	k4
</s>
<s>
Sfakiotes	Sfakiotesa	k1gFnPc2
<g/>
3608	#num#	k4
<g/>
Lazarata	Lazarat	k2eAgFnSc1d1
<g/>
310	#num#	k4
80	#num#	k4
</s>
<s>
Místní	místní	k2eAgInSc4d1
částiYPES	částiYPES	k?
kódMístoSměrovací	kódMístoSměrovací	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
Kalamos	Kalamos	k1gInSc1
<g/>
3603310	#num#	k4
81	#num#	k4
</s>
<s>
Kastos	Kastos	k1gInSc1
<g/>
3605310	#num#	k4
81	#num#	k4
</s>
<s>
Poznámka	poznámka	k1gFnSc1
<g/>
:	:	kIx,
Meganisi	Meganise	k1gFnSc4
<g/>
,	,	kIx,
Kalamos	Kalamos	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
Kastos	Kastos	k1gMnSc1
jsou	být	k5eAaImIp3nP
na	na	k7c6
separátních	separátní	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
zbytek	zbytek	k1gInSc1
výše	vysoce	k6eAd2
uvedeného	uvedený	k2eAgNnSc2d1
je	být	k5eAaImIp3nS
přímo	přímo	k6eAd1
na	na	k7c6
ostrově	ostrov	k1gInSc6
Lefkada	Lefkada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Zajímavá	zajímavý	k2eAgNnPc1d1
místa	místo	k1gNnPc1
</s>
<s>
vesnička	vesnička	k1gFnSc1
Agios	Agios	k1gMnSc1
Nikitas	Nikitas	k1gMnSc1
</s>
<s>
pláže	pláž	k1gFnPc1
Porto	porto	k1gNnSc1
Katsiki	Katsiki	k1gNnSc3
<g/>
,	,	kIx,
Kalamitsi	Kalamitse	k1gFnSc3
<g/>
,	,	kIx,
Egremni	Egremni	k1gFnSc4
<g/>
,	,	kIx,
Milos	Milos	k1gInSc4
</s>
<s>
na	na	k7c6
východní	východní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
přilehlý	přilehlý	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
Scorpios	Scorpios	k1gInSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
Onasisův	Onasisův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
vodopády	vodopád	k1gInPc1
</s>
<s>
větrné	větrný	k2eAgInPc1d1
mlýny	mlýn	k1gInPc1
na	na	k7c6
pláži	pláž	k1gFnSc6
Gyra	Gyr	k1gInSc2
</s>
<s>
Významné	významný	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
Lefkady	Lefkada	k1gFnSc2
</s>
<s>
Ioannis	Ioannis	k1gFnSc1
Orsiem	Orsium	k1gNnSc7
</s>
<s>
Ionnis	Ionnis	k1gFnSc1
Zambelios	Zambeliosa	k1gFnPc2
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Fiktivní	fiktivní	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
Malden	Maldna	k1gFnPc2
ve	v	k7c6
hře	hra	k1gFnSc6
Operace	operace	k1gFnSc2
Flashpoint	Flashpoint	k1gInSc1
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
vojsk	vojsko	k1gNnPc2
NATO	NATO	kA
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
na	na	k7c6
ostrově	ostrov	k1gInSc6
Lefkada	Lefkada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Lefkáda	Lefkáda	k1gFnSc1
-	-	kIx~
Všeobecný	všeobecný	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
II	II	kA
<g/>
.	.	kIx.
<g/>
vydání	vydání	k1gNnSc4
<g/>
,	,	kIx,
Fagotto	Fagotto	k1gNnSc4
Books	Booksa	k1gFnPc2
,2007	,2007	k4
ISBN	ISBN	kA
978-960-6685-10-1	978-960-6685-10-1	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Lefkada	Lefkada	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Lefkady	Lefkada	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Jónské	jónský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
</s>
<s>
Antikythéra	Antikythéra	k1gFnSc1
•	•	k?
Antipaxi	Antipaxe	k1gFnSc4
•	•	k?
Arkoudi	Arkoud	k1gMnPc1
•	•	k?
Atokos	Atokosa	k1gFnPc2
•	•	k?
Korfu	Korfu	k1gNnSc4
•	•	k?
Drakonera	Drakoner	k1gMnSc2
•	•	k?
Elafonisos	Elafonisos	k1gMnSc1
•	•	k?
Ereikoussa	Ereikoussa	k1gFnSc1
•	•	k?
Ithaka	Ithaka	k1gFnSc1
•	•	k?
Kalamos	Kalamos	k1gInSc1
•	•	k?
Kastos	Kastos	k1gInSc1
•	•	k?
Kefalonia	Kefalonium	k1gNnSc2
•	•	k?
Kravia	Kravium	k1gNnSc2
•	•	k?
Kythéra	Kythéra	k1gFnSc1
•	•	k?
Kythros	Kythrosa	k1gFnPc2
•	•	k?
Lazareto	Lazareto	k1gNnSc4
•	•	k?
Lefkada	Lefkada	k1gFnSc1
•	•	k?
Madouri	Madour	k1gFnSc2
•	•	k?
Makri	Makr	k1gFnSc2
•	•	k?
Makropoula	Makropoula	k1gFnSc1
•	•	k?
Mathraki	Mathrak	k1gFnSc2
•	•	k?
Meganisi	Meganise	k1gFnSc4
•	•	k?
Modia	Modium	k1gNnSc2
Islets	Isletsa	k1gFnPc2
•	•	k?
Othonoi	Othonoi	k1gNnSc4
•	•	k?
Oxeia	Oxeium	k1gNnSc2
•	•	k?
Paxos	Paxos	k1gMnSc1
•	•	k?
Petalas	Petalas	k1gInSc1
•	•	k?
Pistros	Pistrosa	k1gFnPc2
•	•	k?
Pontikos	Pontikos	k1gMnSc1
•	•	k?
Provati	Provati	k1gMnSc1
•	•	k?
Skorpios	Skorpios	k1gMnSc1
•	•	k?
Sparti	Spart	k1gMnPc1
Lefkados	Lefkados	k1gMnSc1
•	•	k?
Strofades	Strofades	k1gMnSc1
•	•	k?
Vidos	Vidos	k1gMnSc1
•	•	k?
Vromonas	Vromonas	k1gMnSc1
•	•	k?
Zakynthos	Zakynthos	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Řecko	Řecko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
129024	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4111277-5	4111277-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85076335	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
25144648206023778460	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85076335	#num#	k4
</s>
