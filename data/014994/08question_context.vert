<s desamb="1">
Název	název	k1gInSc1
ostrova	ostrov	k1gInSc2
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
z	z	k7c2
řeckého	řecký	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
Lefkos	Lefkos	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
bílý	bílý	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Lefkada	Lefkada	k1gFnSc1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
též	též	k9
Lefkáda	Lefkáda	k1gFnSc1
(	(	kIx(
<g/>
řecky	řecky	k6eAd1
Λ	Λ	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
řecký	řecký	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
v	v	k7c6
souostroví	souostroví	k1gNnSc6
Jónských	jónský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
o	o	k7c6
rozloze	rozloha	k1gFnSc6
356	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
délka	délka	k1gFnSc1
pláží	pláž	k1gFnPc2
je	být	k5eAaImIp3nS
117	#num#	k4
km	km	kA
a	a	k8xC
na	na	k7c6
ostrově	ostrov	k1gInSc6
žije	žít	k5eAaImIp3nS
23	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
položen	položit	k5eAaPmNgInS
mezi	mezi	k7c7
ostrovy	ostrov	k1gInPc7
Korfu	Korfu	k1gNnSc2
a	a	k8xC
Kefalonie	Kefalonie	k1gFnSc2
<g/>
.	.	kIx.
</s>