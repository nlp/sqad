<s>
Co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
hmotný	hmotný	k2eAgInSc4d1	hmotný
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
silné	silný	k2eAgNnSc1d1	silné
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgInSc4	žádný
objekt	objekt	k1gInSc4	objekt
ho	on	k3xPp3gMnSc4	on
nemůže	moct	k5eNaImIp3nS	moct
opustit	opustit	k5eAaPmF	opustit
<g/>
?	?	kIx.	?
</s>
