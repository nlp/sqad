<s>
Černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
hmotný	hmotný	k2eAgInSc4d1	hmotný
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jisté	jistý	k2eAgFnSc6d1	jistá
oblasti	oblast	k1gFnSc6	oblast
časoprostoru	časoprostor	k1gInSc2	časoprostor
natolik	natolik	k6eAd1	natolik
silné	silný	k2eAgNnSc1d1	silné
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgInSc4	žádný
objekt	objekt	k1gInSc4	objekt
včetně	včetně	k7c2	včetně
světla	světlo	k1gNnSc2	světlo
nemůže	moct	k5eNaImIp3nS	moct
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
