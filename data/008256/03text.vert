<p>
<s>
Obec	obec	k1gFnSc1	obec
Bystročice	Bystročice	k1gFnSc2	Bystročice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Olomouc	Olomouc	k1gFnSc1	Olomouc
v	v	k7c6	v
Olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
816	[number]	k4	816
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
vesnici	vesnice	k1gFnSc6	vesnice
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Památky	památka	k1gFnPc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1858	[number]	k4	1858
</s>
</p>
<p>
<s>
sochy	socha	k1gFnPc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
a	a	k8xC	a
Františka	František	k1gMnSc2	František
Saleského	Saleský	k2eAgInSc2d1	Saleský
u	u	k7c2	u
mostu	most	k1gInSc2	most
přes	přes	k7c4	přes
Blatu	Blata	k1gFnSc4	Blata
</s>
</p>
<p>
<s>
pomník	pomník	k1gInSc4	pomník
obětem	oběť	k1gFnPc3	oběť
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
</s>
</p>
<p>
<s>
==	==	k?	==
Vybavenost	vybavenost	k1gFnSc1	vybavenost
obce	obec	k1gFnSc2	obec
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
vybudována	vybudován	k2eAgFnSc1d1	vybudována
nová	nový	k2eAgFnSc1d1	nová
přístavba	přístavba	k1gFnSc1	přístavba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kapacita	kapacita	k1gFnSc1	kapacita
mateřské	mateřský	k2eAgFnSc2d1	mateřská
školy	škola	k1gFnSc2	škola
již	již	k6eAd1	již
nebyla	být	k5eNaImAgFnS	být
dostačující	dostačující	k2eAgFnSc1d1	dostačující
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
nachází	nacházet	k5eAaImIp3nS	nacházet
jezdecký	jezdecký	k2eAgInSc1d1	jezdecký
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
nechat	nechat	k5eAaPmF	nechat
ustájit	ustájit	k5eAaPmF	ustájit
vlastní	vlastní	k2eAgMnPc4d1	vlastní
koně	kůň	k1gMnPc4	kůň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
velké	velký	k2eAgNnSc1d1	velké
moderní	moderní	k2eAgNnSc1d1	moderní
sportoviště	sportoviště	k1gNnSc1	sportoviště
<g/>
:	:	kIx,	:
tenisové	tenisový	k2eAgInPc4d1	tenisový
kurty	kurt	k1gInPc4	kurt
<g/>
,	,	kIx,	,
minigolf	minigolf	k1gInSc4	minigolf
<g/>
,	,	kIx,	,
hřiště	hřiště	k1gNnSc4	hřiště
s	s	k7c7	s
umělým	umělý	k2eAgInSc7d1	umělý
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
dětské	dětský	k2eAgNnSc1d1	dětské
hřiště	hřiště	k1gNnSc1	hřiště
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Části	část	k1gFnSc3	část
obce	obec	k1gFnSc2	obec
==	==	k?	==
</s>
</p>
<p>
<s>
Bystročice	Bystročice	k1gFnSc1	Bystročice
</s>
</p>
<p>
<s>
Žerůvky	Žerůvka	k1gFnPc1	Žerůvka
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgInPc1d1	přírodní
poměry	poměr	k1gInPc1	poměr
==	==	k?	==
</s>
</p>
<p>
<s>
Jihozápadní	jihozápadní	k2eAgFnSc7d1	jihozápadní
částí	část	k1gFnSc7	část
obce	obec	k1gFnSc2	obec
protéká	protékat	k5eAaImIp3nS	protékat
říčka	říčka	k1gFnSc1	říčka
Blata	Blata	k1gNnPc1	Blata
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Bystročice	Bystročice	k1gFnSc2	Bystročice
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bystročice	Bystročice	k1gFnSc2	Bystročice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Bystročice	Bystročice	k1gFnSc2	Bystročice
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Bystročice	Bystročice	k1gFnSc2	Bystročice
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
