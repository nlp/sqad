<p>
<s>
Pengcun	Pengcun	k1gNnSc1	Pengcun
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Pcheng-čchun	Pcheng-čchuna	k1gFnPc2	Pcheng-čchuna
<g/>
,	,	kIx,	,
pchin-jinem	pchinino	k1gNnSc7	pchin-jino
Péngchū	Péngchū	k1gFnPc2	Péngchū
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc7	znak
朋	朋	k?	朋
nebo	nebo	k8xC	nebo
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Pcheng-chun	Pchenghuna	k1gFnPc2	Pcheng-chuna
<g/>
,	,	kIx,	,
pchin-jinem	pchinino	k1gNnSc7	pchin-jino
Pénghū	Pénghū	k1gFnSc2	Pénghū
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc4	znak
彭	彭	k?	彭
<g/>
;	;	kIx,	;
†	†	k?	†
1701	[number]	k4	1701
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
mandžuský	mandžuský	k2eAgMnSc1d1	mandžuský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
korouhevních	korouhevní	k2eAgFnPc2d1	korouhevní
vojsk	vojsko	k1gNnPc2	vojsko
říše	říš	k1gFnSc2	říš
Čching	Čching	k1gInSc1	Čching
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Pengcun	Pengcun	k1gMnSc1	Pengcun
byl	být	k5eAaImAgMnS	být
pravnukem	pravnuk	k1gMnSc7	pravnuk
mandžuského	mandžuský	k2eAgMnSc2d1	mandžuský
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
Hohoriho	Hohori	k1gMnSc2	Hohori
z	z	k7c2	z
klanu	klan	k1gInSc2	klan
Donggo	Donggo	k6eAd1	Donggo
<g/>
,	,	kIx,	,
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
rudé	rudý	k2eAgFnSc3d1	rudá
korouhvi	korouhev	k1gFnSc3	korouhev
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc3	rok
1652	[number]	k4	1652
zdědil	zdědit	k5eAaPmAgInS	zdědit
titul	titul	k1gInSc1	titul
vévody	vévoda	k1gMnSc2	vévoda
3	[number]	k4	3
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
povýšený	povýšený	k2eAgInSc4d1	povýšený
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1676	[number]	k4	1676
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
zástupcem	zástupce	k1gMnSc7	zástupce
velitele	velitel	k1gMnSc2	velitel
mongolské	mongolský	k2eAgFnSc2d1	mongolská
červené	červený	k2eAgFnSc2d1	červená
korouhve	korouhev	k1gFnSc2	korouhev
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1682	[number]	k4	1682
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
k	k	k7c3	k
mandžuské	mandžuský	k2eAgFnSc3d1	mandžuská
korouhvi	korouhev	k1gFnSc3	korouhev
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1682	[number]	k4	1682
<g/>
/	/	kIx~	/
<g/>
1683	[number]	k4	1683
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
Langtanem	Langtan	k1gInSc7	Langtan
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
výzvědnou	výzvědný	k2eAgFnSc4d1	výzvědná
expedici	expedice	k1gFnSc4	expedice
k	k	k7c3	k
Albazinu	Albazin	k1gInSc3	Albazin
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
obdržel	obdržet	k5eAaPmAgMnS	obdržet
velení	velení	k1gNnSc4	velení
nad	nad	k7c7	nad
mandžuskou	mandžuský	k2eAgFnSc7d1	mandžuská
červenou	červený	k2eAgFnSc7d1	červená
korouhví	korouhev	k1gFnSc7	korouhev
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1685	[number]	k4	1685
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
Amur	Amur	k1gInSc4	Amur
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
s	s	k7c7	s
oddílem	oddíl	k1gInSc7	oddíl
500	[number]	k4	500
vojáků	voják	k1gMnPc2	voják
z	z	k7c2	z
Fu-ťienu	Fu-ťien	k1gInSc2	Fu-ťien
a	a	k8xC	a
Tchaj-wanu	Tchajan	k1gInSc2	Tchaj-wan
zajatých	zajatá	k1gFnPc2	zajatá
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
s	s	k7c7	s
Čeng	Čeng	k1gInSc1	Čeng
Čcheng-kungem	Čchengung	k1gInSc7	Čcheng-kung
a	a	k8xC	a
dovedných	dovedný	k2eAgInPc2d1	dovedný
v	v	k7c6	v
používání	používání	k1gNnSc6	používání
velkých	velký	k2eAgInPc2d1	velký
štítů	štít	k1gInPc2	štít
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
Sabsuem	Sabsu	k1gInSc7	Sabsu
velel	velet	k5eAaImAgInS	velet
prvnímu	první	k4xOgNnSc3	první
obléhání	obléhání	k1gNnSc3	obléhání
Albazinu	Albazin	k1gInSc2	Albazin
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
Albazinu	Albazin	k1gInSc2	Albazin
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Pekingu	Peking	k1gInSc2	Peking
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1690	[number]	k4	1690
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
bojů	boj	k1gInPc2	boj
s	s	k7c7	s
Džúngary	Džúngar	k1gInPc7	Džúngar
včetně	včetně	k7c2	včetně
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Ulan	Ulana	k1gFnPc2	Ulana
Butung	Butunga	k1gFnPc2	Butunga
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
velel	velet	k5eAaImAgMnS	velet
pravému	pravý	k2eAgNnSc3d1	pravé
křídlu	křídlo	k1gNnSc3	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
padl	padnout	k5eAaPmAgMnS	padnout
císařův	císařův	k2eAgMnSc1d1	císařův
strýc	strýc	k1gMnSc1	strýc
Tchung	Tchung	k1gMnSc1	Tchung
Kuo-kang	Kuoang	k1gMnSc1	Kuo-kang
a	a	k8xC	a
džúngarský	džúngarský	k2eAgMnSc1d1	džúngarský
chán	chán	k1gMnSc1	chán
Galdan	Galdan	k1gMnSc1	Galdan
unikl	uniknout	k5eAaPmAgMnS	uniknout
<g/>
,	,	kIx,	,
Pengcun	Pengcun	k1gInSc1	Pengcun
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
degradován	degradovat	k5eAaBmNgInS	degradovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1696	[number]	k4	1696
opět	opět	k6eAd1	opět
dostal	dostat	k5eAaPmAgMnS	dostat
velení	velení	k1gNnSc4	velení
nad	nad	k7c7	nad
mongolskou	mongolský	k2eAgFnSc7d1	mongolská
rudou	rudý	k2eAgFnSc7d1	rudá
korouhví	korouhev	k1gFnSc7	korouhev
a	a	k8xC	a
funkci	funkce	k1gFnSc6	funkce
náčelníka	náčelník	k1gMnSc2	náčelník
štábu	štáb	k1gInSc2	štáb
západní	západní	k2eAgFnSc2d1	západní
armády	armáda	k1gFnSc2	armáda
vypravené	vypravený	k2eAgFnSc2d1	vypravená
proti	proti	k7c3	proti
Džúngarům	Džúngar	k1gInPc3	Džúngar
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	on	k3xPp3gFnPc4	on
porazila	porazit	k5eAaPmAgFnS	porazit
u	u	k7c2	u
Zúnmod	Zúnmoda	k1gFnPc2	Zúnmoda
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1699	[number]	k4	1699
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
postů	post	k1gInPc2	post
kvůli	kvůli	k7c3	kvůli
nemoci	nemoc	k1gFnSc3	nemoc
a	a	k8xC	a
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
