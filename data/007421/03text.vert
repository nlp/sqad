<s>
Forrest	Forrest	k1gInSc1	Forrest
Gump	Gump	k1gInSc1	Gump
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Roberta	Robert	k1gMnSc2	Robert
Zemeckise	Zemeckise	k1gFnSc2	Zemeckise
natočený	natočený	k2eAgInSc4d1	natočený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
knihy	kniha	k1gFnSc2	kniha
Winstona	Winston	k1gMnSc2	Winston
Grooma	groom	k1gMnSc2	groom
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc4	film
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
velkého	velký	k2eAgInSc2d1	velký
úspěchu	úspěch	k1gInSc2	úspěch
u	u	k7c2	u
diváků	divák	k1gMnPc2	divák
a	a	k8xC	a
vydělal	vydělat	k5eAaPmAgMnS	vydělat
celosvětově	celosvětově	k6eAd1	celosvětově
přes	přes	k7c4	přes
667	[number]	k4	667
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
13	[number]	k4	13
Oscarů	Oscar	k1gInPc2	Oscar
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
6	[number]	k4	6
cen	cena	k1gFnPc2	cena
získal	získat	k5eAaPmAgMnS	získat
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
ceny	cena	k1gFnSc2	cena
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
režisér	režisér	k1gMnSc1	režisér
(	(	kIx(	(
<g/>
Robert	Robert	k1gMnSc1	Robert
Zemeckis	Zemeckis	k1gFnSc2	Zemeckis
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
Tom	Tom	k1gMnSc1	Tom
Hanks	Hanksa	k1gFnPc2	Hanksa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
jednoduchého	jednoduchý	k2eAgMnSc2d1	jednoduchý
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
Forresta	Forrest	k1gMnSc2	Forrest
Gumpa	Gump	k1gMnSc2	Gump
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
IQ	iq	kA	iq
75	[number]	k4	75
<g/>
,	,	kIx,	,
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
historickými	historický	k2eAgFnPc7d1	historická
osobnostmi	osobnost	k1gFnPc7	osobnost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
3	[number]	k4	3
amerických	americký	k2eAgMnPc2d1	americký
prezidentů	prezident	k1gMnPc2	prezident
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
historických	historický	k2eAgFnPc2d1	historická
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
vloupání	vloupání	k1gNnPc1	vloupání
do	do	k7c2	do
hotelu	hotel	k1gInSc2	hotel
Watergate	Watergat	k1gMnSc5	Watergat
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
podstatně	podstatně	k6eAd1	podstatně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
knihy	kniha	k1gFnSc2	kniha
na	na	k7c4	na
jejíž	jejíž	k3xOyRp3gInPc4	jejíž
motivy	motiv	k1gInPc4	motiv
byl	být	k5eAaImAgInS	být
natočen	natočen	k2eAgInSc1d1	natočen
<g/>
.	.	kIx.	.
</s>
