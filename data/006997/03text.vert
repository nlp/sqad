<s>
Gregory	Gregor	k1gMnPc4	Gregor
Mark	Mark	k1gMnSc1	Mark
Kovacs	Kovacs	k1gInSc1	Kovacs
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Niagara	Niagara	k1gFnSc1	Niagara
Falls	Falls	k1gInSc1	Falls
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
−	−	k?	−
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
profesionální	profesionální	k2eAgMnSc1d1	profesionální
kulturista	kulturista	k1gMnSc1	kulturista
federace	federace	k1gFnSc2	federace
IFBB	IFBB	kA	IFBB
<g/>
.	.	kIx.	.
</s>
<s>
Kovacs	Kovacs	k1gInSc1	Kovacs
vyrostl	vyrůst	k5eAaPmAgInS	vyrůst
ve	v	k7c4	v
Fonthill	Fonthill	k1gInSc4	Fonthill
v	v	k7c6	v
Ontariu	Ontario	k1gNnSc6	Ontario
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
života	život	k1gInSc2	život
zde	zde	k6eAd1	zde
žil	žít	k5eAaImAgMnS	žít
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Kim	Kim	k1gFnPc2	Kim
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
plně	plně	k6eAd1	plně
věnovat	věnovat	k5eAaImF	věnovat
tréninku	trénink	k1gInSc3	trénink
<g/>
,	,	kIx,	,
studoval	studovat	k5eAaImAgInS	studovat
rok	rok	k1gInSc4	rok
elektrické	elektrický	k2eAgNnSc1d1	elektrické
inženýrství	inženýrství	k1gNnSc1	inženýrství
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
má	mít	k5eAaImIp3nS	mít
Kovacs	Kovacs	k1gInSc4	Kovacs
různé	různý	k2eAgInPc4d1	různý
soutěžní	soutěžní	k2eAgInPc4d1	soutěžní
úspěchy	úspěch	k1gInPc4	úspěch
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
profesionálů	profesionál	k1gMnPc2	profesionál
vážil	vážit	k5eAaImAgMnS	vážit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
liber	libra	k1gFnPc2	libra
(	(	kIx(	(
<g/>
~	~	kIx~	~
<g/>
181,4	[number]	k4	181,4
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1996	[number]	k4	1996
</s>
