<p>
<s>
Kravata	kravata	k1gFnSc1	kravata
(	(	kIx(	(
<g/>
také	také	k9	také
vázanka	vázanka	k1gFnSc1	vázanka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
než	než	k8xS	než
350	[number]	k4	350
let	léto	k1gNnPc2	léto
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
pánského	pánský	k2eAgInSc2d1	pánský
šatníku	šatník	k1gInSc2	šatník
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sociologů	sociolog	k1gMnPc2	sociolog
a	a	k8xC	a
módních	módní	k2eAgMnPc2d1	módní
historiků	historik	k1gMnPc2	historik
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
funkce	funkce	k1gFnSc1	funkce
pouze	pouze	k6eAd1	pouze
estetická	estetický	k2eAgFnSc1d1	estetická
a	a	k8xC	a
sociální	sociální	k2eAgFnSc1d1	sociální
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
kravaty	kravata	k1gFnSc2	kravata
upravuje	upravovat	k5eAaImIp3nS	upravovat
etiketa	etiketa	k1gFnSc1	etiketa
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
ji	on	k3xPp3gFnSc4	on
nosí	nosit	k5eAaImIp3nP	nosit
na	na	k7c4	na
společenské	společenský	k2eAgFnPc4d1	společenská
<g/>
,	,	kIx,	,
formální	formální	k2eAgFnPc4d1	formální
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,	,
v	v	k7c6	v
obchodním	obchodní	k2eAgInSc6d1	obchodní
styku	styk	k1gInSc6	styk
a	a	k8xC	a
na	na	k7c6	na
slavnostní	slavnostní	k2eAgFnSc6d1	slavnostní
události	událost	k1gFnSc6	událost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
několika	několik	k4yIc6	několik
dekádách	dekáda	k1gFnPc6	dekáda
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
od	od	k7c2	od
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
našla	najít	k5eAaPmAgFnS	najít
kravata	kravata	k1gFnSc1	kravata
místo	místo	k6eAd1	místo
i	i	k9	i
v	v	k7c6	v
šatníku	šatník	k1gInSc6	šatník
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
mnoha	mnoho	k4c2	mnoho
uniforem	uniforma	k1gFnPc2	uniforma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Kravata	kravata	k1gFnSc1	kravata
je	být	k5eAaImIp3nS	být
osově	osově	k6eAd1	osově
souměrného	souměrný	k2eAgInSc2d1	souměrný
čtyřúhelníkového	čtyřúhelníkový	k2eAgInSc2d1	čtyřúhelníkový
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
130	[number]	k4	130
<g/>
–	–	k?	–
<g/>
170	[number]	k4	170
cm	cm	kA	cm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejširším	široký	k2eAgNnSc6d3	nejširší
místě	místo	k1gNnSc6	místo
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
cm	cm	kA	cm
<g/>
,	,	kIx,	,
na	na	k7c6	na
opačném	opačný	k2eAgInSc6d1	opačný
konci	konec	k1gInSc6	konec
2-3	[number]	k4	2-3
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Šity	šita	k1gFnPc1	šita
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
rubové	rubový	k2eAgFnSc2d1	rubová
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
z	z	k7c2	z
více	hodně	k6eAd2	hodně
dílů	díl	k1gInPc2	díl
či	či	k8xC	či
vcelku	vcelku	k6eAd1	vcelku
<g/>
,	,	kIx,	,
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
zužující	zužující	k2eAgFnSc2d1	zužující
se	se	k3xPyFc4	se
trubice	trubice	k1gFnSc2	trubice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
protáhne	protáhnout	k5eAaPmIp3nS	protáhnout
na	na	k7c4	na
líc	líc	k1gFnSc4	líc
a	a	k8xC	a
konečný	konečný	k2eAgInSc1d1	konečný
tvar	tvar	k1gInSc1	tvar
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
souměrným	souměrný	k2eAgNnSc7d1	souměrné
přežehlením	přežehlení	k1gNnSc7	přežehlení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
strany	strana	k1gFnSc2	strana
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
koncích	konec	k1gInPc6	konec
kravaty	kravata	k1gFnSc2	kravata
bývá	bývat	k5eAaImIp3nS	bývat
podestýlka	podestýlka	k1gFnSc1	podestýlka
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
u	u	k7c2	u
dražších	drahý	k2eAgInPc2d2	dražší
<g/>
)	)	kIx)	)
ze	z	k7c2	z
saténu	satén	k1gInSc2	satén
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
polyesteru	polyester	k1gInSc2	polyester
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějšími	častý	k2eAgInPc7d3	nejčastější
dvěma	dva	k4xCgInPc7	dva
materiály	materiál	k1gInPc7	materiál
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
šije	šít	k5eAaImIp3nS	šít
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
hedvábí	hedvábí	k1gNnSc4	hedvábí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
použito	použít	k5eAaPmNgNnS	použít
u	u	k7c2	u
těch	ten	k3xDgFnPc2	ten
kvalitnějších	kvalitní	k2eAgFnPc2d2	kvalitnější
<g/>
;	;	kIx,	;
a	a	k8xC	a
polyester	polyester	k1gInSc1	polyester
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
zase	zase	k9	zase
menší	malý	k2eAgFnSc1d2	menší
mačkavost	mačkavost	k1gFnSc1	mačkavost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
dekádě	dekáda	k1gFnSc6	dekáda
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
"	"	kIx"	"
<g/>
odnož	odnož	k1gFnSc4	odnož
<g/>
"	"	kIx"	"
pletených	pletený	k2eAgFnPc2d1	pletená
kravat	kravata	k1gFnPc2	kravata
s	s	k7c7	s
hranatým	hranatý	k2eAgInSc7d1	hranatý
koncem	konec	k1gInSc7	konec
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
typické	typický	k2eAgFnSc2d1	typická
špičky	špička	k1gFnSc2	špička
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
semiformální	semiformální	k2eAgFnPc4d1	semiformální
příležitosti	příležitost	k1gFnPc4	příležitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kravaty	kravata	k1gFnPc1	kravata
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
mnoho	mnoho	k4c4	mnoho
motivů	motiv	k1gInPc2	motiv
–	–	k?	–
pruhy	pruh	k1gInPc7	pruh
<g/>
,	,	kIx,	,
puntíky	puntík	k1gInPc1	puntík
<g/>
,	,	kIx,	,
kostky	kostka	k1gFnPc1	kostka
<g/>
,	,	kIx,	,
paisley	paisley	k1gInPc1	paisley
<g/>
,	,	kIx,	,
vzory	vzor	k1gInPc1	vzor
–	–	k?	–
nebo	nebo	k8xC	nebo
být	být	k5eAaImF	být
jednolité	jednolitý	k2eAgInPc1d1	jednolitý
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tzv.	tzv.	kA	tzv.
univerzitní	univerzitní	k2eAgFnSc2d1	univerzitní
kravaty	kravata	k1gFnSc2	kravata
s	s	k7c7	s
emblémy	emblém	k1gInPc7	emblém
a	a	k8xC	a
symboly	symbol	k1gInPc7	symbol
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
kapely	kapela	k1gFnPc4	kapela
či	či	k8xC	či
hudební	hudební	k2eAgInPc4d1	hudební
spolky	spolek	k1gInPc4	spolek
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc4d1	jiný
instituty	institut	k1gInPc4	institut
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejformálnější	formální	k2eAgFnSc6d3	nejformálnější
příležitosti	příležitost	k1gFnSc6	příležitost
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
hedvábná	hedvábný	k2eAgFnSc1d1	hedvábná
kravata	kravata	k1gFnSc1	kravata
bez	bez	k7c2	bez
vzoru	vzor	k1gInSc2	vzor
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
nosit	nosit	k5eAaImF	nosit
i	i	k9	i
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
black	black	k6eAd1	black
tie	tie	k?	tie
events	events	k1gInSc1	events
<g/>
,	,	kIx,	,
zaměnitelně	zaměnitelně	k6eAd1	zaměnitelně
s	s	k7c7	s
černým	černý	k2eAgMnSc7d1	černý
motýlkem	motýlek	k1gMnSc7	motýlek
<g/>
.	.	kIx.	.
</s>
<s>
Šikmé	šikmý	k2eAgInPc1d1	šikmý
proužky	proužek	k1gInPc1	proužek
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
byznys	byznys	k1gInSc4	byznys
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
vzory	vzor	k1gInPc1	vzor
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
poloformální	poloformální	k2eAgFnSc7d1	poloformální
a	a	k8xC	a
otázkou	otázka	k1gFnSc7	otázka
osobního	osobní	k2eAgInSc2d1	osobní
stylu	styl	k1gInSc2	styl
jeho	jeho	k3xOp3gMnSc2	jeho
nositele	nositel	k1gMnSc2	nositel
<g/>
.	.	kIx.	.
</s>
<s>
Nejméně	málo	k6eAd3	málo
formální	formální	k2eAgFnPc1d1	formální
jsou	být	k5eAaImIp3nP	být
kravaty	kravata	k1gFnPc1	kravata
s	s	k7c7	s
potiskem	potisk	k1gInSc7	potisk
<g/>
,	,	kIx,	,
obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
tematickými	tematický	k2eAgInPc7d1	tematický
motivy	motiv	k1gInPc7	motiv
(	(	kIx(	(
<g/>
typu	typ	k1gInSc2	typ
vánoce	vánoce	k1gFnPc1	vánoce
<g/>
,	,	kIx,	,
komiksoví	komiksový	k2eAgMnPc1d1	komiksový
hrdinové	hrdina	k1gMnPc1	hrdina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
jako	jako	k8xC	jako
volba	volba	k1gFnSc1	volba
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
diskutabilní	diskutabilní	k2eAgMnSc1d1	diskutabilní
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
jako	jako	k9	jako
inovativní	inovativní	k2eAgInSc4d1	inovativní
vynález	vynález	k1gInSc4	vynález
sehnat	sehnat	k5eAaPmF	sehnat
kravaty	kravata	k1gFnPc4	kravata
s	s	k7c7	s
malými	malý	k2eAgFnPc7d1	malá
kapsičkami	kapsička	k1gFnPc7	kapsička
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
pánské	pánský	k2eAgFnSc2d1	pánská
módy	móda	k1gFnSc2	móda
a	a	k8xC	a
stylu	styl	k1gInSc2	styl
jsou	být	k5eAaImIp3nP	být
vnímány	vnímat	k5eAaImNgInP	vnímat
spíše	spíše	k9	spíše
jako	jako	k9	jako
výstřelky	výstřelek	k1gInPc4	výstřelek
a	a	k8xC	a
kuriozity	kuriozita	k1gFnPc4	kuriozita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kravaty	kravata	k1gFnPc1	kravata
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgFnP	určit
na	na	k7c4	na
košile	košile	k1gFnPc4	košile
s	s	k7c7	s
límečkem	límeček	k1gInSc7	límeček
a	a	k8xC	a
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
rukávy	rukáv	k1gInPc7	rukáv
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
výjimky	výjimek	k1gInPc4	výjimek
typu	typ	k1gInSc2	typ
pracovních	pracovní	k2eAgFnPc2d1	pracovní
uniforem	uniforma	k1gFnPc2	uniforma
<g/>
,	,	kIx,	,
Svědků	svědek	k1gMnPc2	svědek
Jehovových	Jehovův	k2eAgInPc2d1	Jehovův
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společenské	společenský	k2eAgFnPc1d1	společenská
košile	košile	k1gFnPc1	košile
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
ad	ad	k7c4	ad
hoc	hoc	k?	hoc
dírky	dírka	k1gFnPc4	dírka
s	s	k7c7	s
knoflíky	knoflík	k1gInPc7	knoflík
na	na	k7c6	na
špičce	špička	k1gFnSc6	špička
límečku	límeček	k1gInSc2	límeček
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kravatu	kravata	k1gFnSc4	kravata
u	u	k7c2	u
krku	krk	k1gInSc2	krk
lépe	dobře	k6eAd2	dobře
zafixovaly	zafixovat	k5eAaPmAgFnP	zafixovat
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
estetickým	estetický	k2eAgInSc7d1	estetický
doplňkem	doplněk	k1gInSc7	doplněk
(	(	kIx(	(
<g/>
a	a	k8xC	a
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
šperků	šperk	k1gInPc2	šperk
pánského	pánský	k2eAgNnSc2d1	pánské
oblékání	oblékání	k1gNnSc2	oblékání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kravatová	kravatový	k2eAgFnSc1d1	kravatová
jehlice	jehlice	k1gFnSc1	jehlice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
propichuje	propichovat	k5eAaImIp3nS	propichovat
kravatový	kravatový	k2eAgInSc4d1	kravatový
uzel	uzel	k1gInSc4	uzel
ke	k	k7c3	k
smyčce	smyčka	k1gFnSc3	smyčka
kolem	kolem	k7c2	kolem
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kravatový	kravatový	k2eAgInSc1d1	kravatový
špendlík	špendlík	k1gInSc1	špendlík
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vypadá	vypadat	k5eAaPmIp3nS	vypadat
jako	jako	k9	jako
větší	veliký	k2eAgInSc4d2	veliký
spínací	spínací	k2eAgInSc4d1	spínací
špendlík	špendlík	k1gInSc4	špendlík
a	a	k8xC	a
též	též	k9	též
upíná	upínat	k5eAaImIp3nS	upínat
kravatu	kravata	k1gFnSc4	kravata
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jejího	její	k3xOp3gInSc2	její
uzlu	uzel	k1gInSc2	uzel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
mívají	mívat	k5eAaImIp3nP	mívat
kravaty	kravata	k1gFnPc1	kravata
malé	malý	k2eAgNnSc1d1	malé
poutko	poutko	k1gNnSc1	poutko
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
se	se	k3xPyFc4	se
po	po	k7c6	po
uvázání	uvázání	k1gNnSc6	uvázání
uzlu	uzel	k1gInSc2	uzel
provlékne	provléknout	k5eAaPmIp3nS	provléknout
tenčí	tenký	k2eAgInSc4d2	tenčí
konec	konec	k1gInSc4	konec
kravaty	kravata	k1gFnSc2	kravata
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
neplandal	plandat	k5eNaImAgMnS	plandat
<g/>
.	.	kIx.	.
</s>
<s>
Kravata	kravata	k1gFnSc1	kravata
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
u	u	k7c2	u
prsou	prsa	k1gNnPc2	prsa
dodatečně	dodatečně	k6eAd1	dodatečně
upnout	upnout	k5eAaPmF	upnout
kravatovou	kravatový	k2eAgFnSc7d1	kravatová
sponou	spona	k1gFnSc7	spona
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ji	on	k3xPp3gFnSc4	on
ke	k	k7c3	k
košili	košile	k1gFnSc3	košile
připne	připnout	k5eAaPmIp3nS	připnout
u	u	k7c2	u
okraje	okraj	k1gInSc2	okraj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
knoflíkové	knoflíkový	k2eAgFnPc1d1	knoflíková
dírky	dírka	k1gFnPc1	dírka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
moderní	moderní	k2eAgFnSc2d1	moderní
kravaty	kravata	k1gFnSc2	kravata
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1635	[number]	k4	1635
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
dorazili	dorazit	k5eAaPmAgMnP	dorazit
chorvatští	chorvatský	k2eAgMnPc1d1	chorvatský
vojáci	voják	k1gMnPc1	voják
podpořit	podpořit	k5eAaPmF	podpořit
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
tradiční	tradiční	k2eAgNnSc1d1	tradiční
oblečení	oblečení	k1gNnSc1	oblečení
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
i	i	k9	i
barevný	barevný	k2eAgInSc4d1	barevný
šátek	šátek	k1gInSc4	šátek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
měli	mít	k5eAaImAgMnP	mít
uvázaný	uvázaný	k2eAgInSc1d1	uvázaný
kolem	kolem	k7c2	kolem
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Šátky	šátek	k1gInPc1	šátek
byly	být	k5eAaImAgInP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
z	z	k7c2	z
různého	různý	k2eAgInSc2d1	různý
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Kravaty	kravata	k1gFnPc1	kravata
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
brzy	brzy	k6eAd1	brzy
součástí	součást	k1gFnSc7	součást
uniformy	uniforma	k1gFnSc2	uniforma
francouzské	francouzský	k2eAgFnSc2d1	francouzská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
la	la	k0	la
Croate	Croat	k1gMnSc5	Croat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Chorvat	Chorvat	k1gMnSc1	Chorvat
<g/>
)	)	kIx)	)
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
la	la	k1gNnSc4	la
cravatte	cravatte	k5eAaPmIp2nP	cravatte
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jak	jak	k8xC	jak
vázat	vázat	k5eAaImF	vázat
kravaty	kravata	k1gFnPc4	kravata
==	==	k?	==
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
lze	lze	k6eAd1	lze
teoreticky	teoreticky	k6eAd1	teoreticky
vytvořit	vytvořit	k5eAaPmF	vytvořit
až	až	k9	až
85	[number]	k4	85
variant	varianta	k1gFnPc2	varianta
uvázání	uvázání	k1gNnSc2	uvázání
kravaty	kravata	k1gFnSc2	kravata
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
publikace	publikace	k1gFnSc2	publikace
The	The	k1gFnSc2	The
85	[number]	k4	85
Ways	Waysa	k1gFnPc2	Waysa
to	ten	k3xDgNnSc1	ten
Tie	Tie	k1gMnSc1	Tie
a	a	k8xC	a
Tie	Tie	k1gFnSc1	Tie
Thomase	Thomas	k1gMnSc2	Thomas
Finka	Finka	k1gFnSc1	Finka
a	a	k8xC	a
Yonga	Yonga	k1gFnSc1	Yonga
Maoa	Maoa	k1gFnSc1	Maoa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejběžnějších	běžný	k2eAgFnPc2d3	nejběžnější
je	být	k5eAaImIp3nS	být
pět	pět	k4xCc4	pět
typů	typ	k1gInPc2	typ
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
plný	plný	k2eAgInSc4d1	plný
<g/>
)	)	kIx)	)
Windsorvelký	Windsorvelký	k2eAgInSc4d1	Windsorvelký
symetrický	symetrický	k2eAgInSc4d1	symetrický
široký	široký	k2eAgInSc4d1	široký
uzel	uzel	k1gInSc4	uzel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
hodně	hodně	k6eAd1	hodně
délky	délka	k1gFnSc2	délka
</s>
</p>
<p>
<s>
hodí	hodit	k5eAaPmIp3nS	hodit
se	se	k3xPyFc4	se
na	na	k7c6	na
slavnostní	slavnostní	k2eAgFnSc6d1	slavnostní
příležitosti	příležitost	k1gFnSc6	příležitost
</s>
</p>
<p>
<s>
ke	k	k7c3	k
košili	košile	k1gFnSc3	košile
se	s	k7c7	s
širokým	široký	k2eAgInSc7d1	široký
límcem	límec	k1gInSc7	límec
</s>
</p>
<p>
<s>
připisuje	připisovat	k5eAaImIp3nS	připisovat
se	se	k3xPyFc4	se
britskému	britský	k2eAgMnSc3d1	britský
králi	král	k1gMnSc3	král
Edwardovi	Edward	k1gMnSc3	Edward
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
<g/>
Manhattanmenší	Manhattanmenší	k2eAgInSc1d1	Manhattanmenší
asymetrický	asymetrický	k2eAgInSc1d1	asymetrický
uzel	uzel	k1gInSc1	uzel
</s>
</p>
<p>
<s>
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
na	na	k7c4	na
uvázání	uvázání	k1gNnSc4	uvázání
</s>
</p>
<p>
<s>
ke	k	k7c3	k
košili	košile	k1gFnSc3	košile
s	s	k7c7	s
úzkým	úzký	k2eAgInSc7d1	úzký
až	až	k8xS	až
středním	střední	k2eAgInSc7d1	střední
límcempoloviční	límcempoloviční	k2eAgFnSc7d1	límcempoloviční
Windsormenší	Windsormenš	k1gFnSc7	Windsormenš
symetrický	symetrický	k2eAgInSc1d1	symetrický
trojúhelníkový	trojúhelníkový	k2eAgInSc1d1	trojúhelníkový
uzel	uzel	k1gInSc1	uzel
</s>
</p>
<p>
<s>
skromnější	skromný	k2eAgFnSc1d2	skromnější
varianta	varianta	k1gFnSc1	varianta
Windsoru	Windsor	k1gInSc2	Windsor
</s>
</p>
<p>
<s>
ke	k	k7c3	k
košili	košile	k1gFnSc3	košile
se	s	k7c7	s
středně	středně	k6eAd1	středně
širokým	široký	k2eAgInSc7d1	široký
límcemPratt	límcemPratt	k2eAgInSc4d1	límcemPratt
(	(	kIx(	(
<g/>
Shelby	Shelba	k1gFnSc2	Shelba
<g/>
)	)	kIx)	)
<g/>
představen	představen	k2eAgMnSc1d1	představen
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
Jerry	Jerra	k1gFnSc2	Jerra
Prattem	Pratto	k1gNnSc7	Pratto
v	v	k7c6	v
TV	TV	kA	TV
show	show	k1gFnSc1	show
Dona	Don	k1gMnSc4	Don
Shelbyho	Shelby	k1gMnSc4	Shelby
</s>
</p>
<p>
<s>
symetrický	symetrický	k2eAgInSc1d1	symetrický
uzel	uzel	k1gInSc1	uzel
<g/>
,	,	kIx,	,
populární	populární	k2eAgInSc1d1	populární
zejména	zejména	k9	zejména
v	v	k7c4	v
USALong	USALong	k1gInSc4	USALong
(	(	kIx(	(
<g/>
Four	Four	k1gInSc1	Four
in	in	k?	in
Hand	Hand	k1gInSc1	Hand
<g/>
)	)	kIx)	)
<g/>
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgMnSc1d1	vhodný
ke	k	k7c3	k
košilím	košile	k1gFnPc3	košile
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
límečky	límeček	k1gInPc1	límeček
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
blízko	blízko	k6eAd1	blízko
</s>
</p>
<p>
<s>
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
asymetrické	asymetrický	k2eAgNnSc4d1	asymetrické
uzle	uzel	k1gInSc5	uzel
</s>
</p>
<p>
<s>
dobře	dobře	k6eAd1	dobře
vynikne	vyniknout	k5eAaPmIp3nS	vyniknout
u	u	k7c2	u
širších	široký	k2eAgFnPc2d2	širší
kravatV	kravatV	k?	kravatV
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
doporučeno	doporučit	k5eAaPmNgNnS	doporučit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kravata	kravata	k1gFnSc1	kravata
u	u	k7c2	u
košile	košile	k1gFnSc2	košile
zakrývala	zakrývat	k5eAaImAgFnS	zakrývat
celý	celý	k2eAgInSc4d1	celý
svislý	svislý	k2eAgInSc4d1	svislý
pás	pás	k1gInSc4	pás
knoflíků	knoflík	k1gMnPc2	knoflík
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
špička	špička	k1gFnSc1	špička
sahala	sahat	k5eAaImAgFnS	sahat
přibližně	přibližně	k6eAd1	přibližně
k	k	k7c3	k
hornímu	horní	k2eAgInSc3d1	horní
konci	konec	k1gInSc3	konec
spony	spona	k1gFnSc2	spona
pásku	pásek	k1gInSc2	pásek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulých	minulý	k2eAgFnPc6d1	minulá
dekádách	dekáda	k1gFnPc6	dekáda
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
některých	některý	k3yIgFnPc2	některý
uniforem	uniforma	k1gFnPc2	uniforma
byla	být	k5eAaImAgFnS	být
<g/>
/	/	kIx~	/
<g/>
je	být	k5eAaImIp3nS	být
přijatelná	přijatelný	k2eAgFnSc1d1	přijatelná
kratší	krátký	k2eAgFnSc1d2	kratší
délka	délka	k1gFnSc1	délka
<g/>
.	.	kIx.	.
</s>
<s>
Případ	případ	k1gInSc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
kravata	kravata	k1gFnSc1	kravata
tak	tak	k6eAd1	tak
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gInSc1	její
kratší	krátký	k2eAgInSc1d2	kratší
konec	konec	k1gInSc1	konec
není	být	k5eNaImIp3nS	být
zakryt	zakryt	k2eAgInSc4d1	zakryt
širším	široký	k2eAgInSc7d2	širší
a	a	k8xC	a
"	"	kIx"	"
<g/>
přečuhuje	přečuhovat	k5eAaImIp3nS	přečuhovat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nepůsobí	působit	k5eNaImIp3nS	působit
esteticky	esteticky	k6eAd1	esteticky
pěkně	pěkně	k6eAd1	pěkně
–	–	k?	–
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
zvolit	zvolit	k5eAaPmF	zvolit
uzel	uzel	k1gInSc4	uzel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
používá	používat	k5eAaImIp3nS	používat
více	hodně	k6eAd2	hodně
délky	délka	k1gFnSc2	délka
kravaty	kravata	k1gFnSc2	kravata
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
plný	plný	k2eAgInSc4d1	plný
Winsor	Winsor	k1gInSc4	Winsor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kratší	krátký	k2eAgInSc1d2	kratší
konec	konec	k1gInSc1	konec
zastrčit	zastrčit	k5eAaPmF	zastrčit
do	do	k7c2	do
košile	košile	k1gFnSc2	košile
dírou	díra	k1gFnSc7	díra
mezi	mezi	k7c7	mezi
knoflíky	knoflík	k1gMnPc7	knoflík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
oděvní	oděvní	k2eAgFnPc1d1	oděvní
součástky	součástka	k1gFnPc1	součástka
</s>
</p>
<p>
<s>
spona	spona	k1gFnSc1	spona
na	na	k7c4	na
kravatu	kravata	k1gFnSc4	kravata
</s>
</p>
<p>
<s>
motýlek	motýlek	k1gMnSc1	motýlek
</s>
</p>
<p>
<s>
vesta	vesta	k1gFnSc1	vesta
</s>
</p>
<p>
<s>
svetr	svetr	k1gInSc4	svetr
</s>
</p>
<p>
<s>
bunda	bunda	k1gFnSc1	bunda
</s>
</p>
<p>
<s>
kabát	kabát	k1gInSc4	kabát
</s>
</p>
<p>
<s>
košile	košile	k1gFnSc1	košile
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kravata	kravata	k1gFnSc1	kravata
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kravata	kravata	k1gFnSc1	kravata
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
Kravatám	kravata	k1gFnPc3	kravata
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
zvoní	zvonit	k5eAaImIp3nS	zvonit
hrana	hrana	k1gFnSc1	hrana
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
