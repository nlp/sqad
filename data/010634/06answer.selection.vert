<s>
Kravata	kravata	k1gFnSc1	kravata
je	být	k5eAaImIp3nS	být
osově	osově	k6eAd1	osově
souměrného	souměrný	k2eAgInSc2d1	souměrný
čtyřúhelníkového	čtyřúhelníkový	k2eAgInSc2d1	čtyřúhelníkový
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
130	[number]	k4	130
<g/>
–	–	k?	–
<g/>
170	[number]	k4	170
cm	cm	kA	cm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejširším	široký	k2eAgNnSc6d3	nejširší
místě	místo	k1gNnSc6	místo
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
cm	cm	kA	cm
<g/>
,	,	kIx,	,
na	na	k7c6	na
opačném	opačný	k2eAgInSc6d1	opačný
konci	konec	k1gInSc6	konec
2-3	[number]	k4	2-3
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
