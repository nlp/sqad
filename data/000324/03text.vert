<s>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
zkr.	zkr.	kA	zkr.
PSP	PSP	kA	PSP
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
PS	PS	kA	PS
PČR	PČR	kA	PČR
<g/>
,	,	kIx,	,
v	v	k7c6	v
Ústavě	ústava	k1gFnSc6	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jen	jen	k9	jen
jako	jako	k8xC	jako
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dolní	dolní	k2eAgFnSc1d1	dolní
komora	komora	k1gFnSc1	komora
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
Ústavě	ústav	k1gInSc6	ústav
označovaného	označovaný	k2eAgInSc2d1	označovaný
jen	jen	k6eAd1	jen
jako	jako	k9	jako
Parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
200	[number]	k4	200
poslanců	poslanec	k1gMnPc2	poslanec
volených	volený	k2eAgMnPc2d1	volený
na	na	k7c4	na
4	[number]	k4	4
roky	rok	k1gInPc7	rok
podle	podle	k7c2	podle
poměrného	poměrný	k2eAgInSc2d1	poměrný
systému	systém	k1gInSc2	systém
s	s	k7c7	s
5	[number]	k4	5
<g/>
%	%	kIx~	%
klauzulí	klauzule	k1gFnPc2	klauzule
<g/>
.	.	kIx.	.
</s>
<s>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
1993	[number]	k4	1993
transformací	transformace	k1gFnPc2	transformace
z	z	k7c2	z
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Sněmovna	sněmovna	k1gFnSc1	sněmovna
je	být	k5eAaImIp3nS	být
usnášeníschopná	usnášeníschopný	k2eAgFnSc1d1	usnášeníschopná
při	při	k7c6	při
třetinovém	třetinový	k2eAgNnSc6d1	třetinové
kvóru	kvórum	k1gNnSc6	kvórum
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
je	být	k5eAaImIp3nS	být
přítomno	přítomen	k2eAgNnSc1d1	přítomno
aspoň	aspoň	k9	aspoň
67	[number]	k4	67
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
usnesení	usnesení	k1gNnPc1	usnesení
se	se	k3xPyFc4	se
schvalují	schvalovat	k5eAaImIp3nP	schvalovat
prostou	prostý	k2eAgFnSc7d1	prostá
většinou	většina	k1gFnSc7	většina
(	(	kIx(	(
<g/>
nadpoloviční	nadpoloviční	k2eAgFnSc7d1	nadpoloviční
většinou	většina	k1gFnSc7	většina
přítomných	přítomný	k1gMnPc2	přítomný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
usnesení	usnesení	k1gNnPc2	usnesení
<g/>
,	,	kIx,	,
specifikovaných	specifikovaný	k2eAgFnPc2d1	specifikovaná
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
o	o	k7c6	o
jednacím	jednací	k2eAgInSc6d1	jednací
řádu	řád	k1gInSc6	řád
sněmovny	sněmovna	k1gFnSc2	sněmovna
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
přijetí	přijetí	k1gNnSc4	přijetí
usnesení	usnesení	k1gNnSc2	usnesení
potřeba	potřeba	k6eAd1	potřeba
souhlasu	souhlas	k1gInSc3	souhlas
vyššího	vysoký	k2eAgInSc2d2	vyšší
počtu	počet	k1gInSc2	počet
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Absolutní	absolutní	k2eAgFnSc2d1	absolutní
většiny	většina	k1gFnSc2	většina
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
101	[number]	k4	101
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
)	)	kIx)	)
např.	např.	kA	např.
pro	pro	k7c4	pro
přehlasování	přehlasování	k1gNnSc4	přehlasování
prezidentského	prezidentský	k2eAgNnSc2d1	prezidentské
veta	veto	k1gNnSc2	veto
či	či	k8xC	či
vyslovení	vyslovení	k1gNnSc6	vyslovení
nedůvěry	nedůvěra	k1gFnSc2	nedůvěra
vládě	vláda	k1gFnSc3	vláda
<g/>
)	)	kIx)	)
Pro	pro	k7c4	pro
přijetí	přijetí	k1gNnSc4	přijetí
ústavních	ústavní	k2eAgInPc2d1	ústavní
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
ratifikaci	ratifikace	k1gFnSc6	ratifikace
některých	některý	k3yIgFnPc2	některý
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
a	a	k8xC	a
podání	podání	k1gNnSc6	podání
ústavní	ústavní	k2eAgFnSc2d1	ústavní
žaloby	žaloba	k1gFnSc2	žaloba
proti	proti	k7c3	proti
prezidentu	prezident	k1gMnSc3	prezident
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
kvalifikované	kvalifikovaný	k2eAgFnSc2d1	kvalifikovaná
většiny	většina	k1gFnSc2	většina
120	[number]	k4	120
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
tři	tři	k4xCgFnPc1	tři
pětiny	pětina	k1gFnPc1	pětina
všech	všecek	k3xTgMnPc2	všecek
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
nemůže	moct	k5eNaImIp3nS	moct
přehlasovat	přehlasovat	k5eAaBmF	přehlasovat
Senát	senát	k1gInSc1	senát
u	u	k7c2	u
návrhu	návrh	k1gInSc2	návrh
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
jednacím	jednací	k2eAgInSc6d1	jednací
řádu	řád	k1gInSc6	řád
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
volebního	volební	k2eAgInSc2d1	volební
zákona	zákon	k1gInSc2	zákon
či	či	k8xC	či
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
zásadách	zásada	k1gFnPc6	zásada
jednání	jednání	k1gNnSc2	jednání
a	a	k8xC	a
styku	styk	k1gInSc2	styk
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
navenek	navenek	k6eAd1	navenek
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
sněmovny	sněmovna	k1gFnSc2	sněmovna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1	[number]	k4	1
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
blocích	blok	k1gInPc6	blok
domů	dům	k1gInPc2	dům
a	a	k8xC	a
paláců	palác	k1gInPc2	palác
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mezi	mezi	k7c7	mezi
Malostranským	malostranský	k2eAgNnSc7d1	Malostranské
a	a	k8xC	a
Valdštejnským	valdštejnský	k2eAgNnSc7d1	Valdštejnské
náměstím	náměstí	k1gNnSc7	náměstí
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
ulic	ulice	k1gFnPc2	ulice
Sněmovní	sněmovní	k2eAgFnSc1d1	sněmovní
a	a	k8xC	a
Thunovská	Thunovský	k2eAgFnSc1d1	Thunovská
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
zejména	zejména	k9	zejména
Thunovský	Thunovský	k2eAgInSc1d1	Thunovský
palác	palác	k1gInSc1	palác
(	(	kIx(	(
<g/>
č.	č.	k?	č.
p.	p.	k?	p.
176	[number]	k4	176
<g/>
)	)	kIx)	)
při	při	k7c6	při
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
ulice	ulice	k1gFnSc2	ulice
Sněmovní	sněmovní	k2eAgFnSc2d1	sněmovní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1779	[number]	k4	1779
<g/>
-	-	kIx~	-
<g/>
1794	[number]	k4	1794
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
jej	on	k3xPp3gInSc4	on
koupili	koupit	k5eAaPmAgMnP	koupit
čeští	český	k2eAgMnPc1d1	český
stavové	stavový	k2eAgFnPc1d1	stavová
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jej	on	k3xPp3gMnSc4	on
přebudovali	přebudovat	k5eAaPmAgMnP	přebudovat
na	na	k7c4	na
sněmovnu	sněmovna	k1gFnSc4	sněmovna
<g/>
,	,	kIx,	,
kanceláře	kancelář	k1gFnPc4	kancelář
a	a	k8xC	a
archív	archív	k1gInSc4	archív
Zemského	zemský	k2eAgInSc2d1	zemský
výboru	výbor	k1gInSc2	výbor
Království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
obnovený	obnovený	k2eAgInSc4d1	obnovený
zemský	zemský	k2eAgInSc4d1	zemský
sněm	sněm	k1gInSc4	sněm
vybudován	vybudován	k2eAgInSc1d1	vybudován
sál	sál	k1gInSc1	sál
přes	přes	k7c4	přes
dvě	dva	k4xCgNnPc4	dva
patra	patro	k1gNnPc4	patro
výšky	výška	k1gFnSc2	výška
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
jednací	jednací	k2eAgInSc1d1	jednací
sál	sál	k1gInSc1	sál
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
přijata	přijmout	k5eAaPmNgFnS	přijmout
její	její	k3xOp3gFnSc1	její
ústava	ústava	k1gFnSc1	ústava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bloku	blok	k1gInSc6	blok
sídlí	sídlet	k5eAaImIp3nS	sídlet
rovněž	rovněž	k9	rovněž
Kancelář	kancelář	k1gFnSc1	kancelář
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
jednací	jednací	k2eAgInPc1d1	jednací
sály	sál	k1gInPc1	sál
sněmovních	sněmovní	k2eAgInPc2d1	sněmovní
klubů	klub	k1gInPc2	klub
a	a	k8xC	a
výborů	výbor	k1gInPc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
bloky	blok	k1gInPc7	blok
patřícími	patřící	k2eAgInPc7d1	patřící
k	k	k7c3	k
sídlu	sídlo	k1gNnSc3	sídlo
sněmovny	sněmovna	k1gFnSc2	sněmovna
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Dům	dům	k1gInSc1	dům
č.	č.	k?	č.
p.	p.	k?	p.
1	[number]	k4	1
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
horní	horní	k2eAgFnSc2d1	horní
části	část	k1gFnSc2	část
Malostranského	malostranský	k2eAgNnSc2d1	Malostranské
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sídlo	sídlo	k1gNnSc1	sídlo
Českého	český	k2eAgNnSc2d1	české
místodržitelství	místodržitelství	k1gNnSc2	místodržitelství
a	a	k8xC	a
jezuitského	jezuitský	k2eAgNnSc2d1	jezuitské
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Blok	blok	k1gInSc1	blok
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
dolní	dolní	k2eAgFnSc2d1	dolní
části	část	k1gFnSc2	část
Malostranského	malostranský	k2eAgNnSc2d1	Malostranské
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
:	:	kIx,	:
č.	č.	k?	č.
p.	p.	k?	p.
6	[number]	k4	6
(	(	kIx(	(
<g/>
Palác	palác	k1gInSc1	palác
Smiřických	smiřický	k2eAgFnPc2d1	Smiřická
neboli	neboli	k8xC	neboli
U	u	k7c2	u
Montágů	Montág	k1gInPc2	Montág
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
7	[number]	k4	7
(	(	kIx(	(
<g/>
Šternberský	šternberský	k2eAgInSc1d1	šternberský
palác	palác	k1gInSc1	palác
<g/>
)	)	kIx)	)
518	[number]	k4	518
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Velikovský	Velikovský	k2eAgInSc1d1	Velikovský
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
ulice	ulice	k1gFnSc2	ulice
Tomášská	tomášský	k2eAgFnSc1d1	Tomášská
<g/>
)	)	kIx)	)
a	a	k8xC	a
8	[number]	k4	8
(	(	kIx(	(
<g/>
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
ulice	ulice	k1gFnSc2	ulice
Thunovská	Thunovský	k2eAgNnPc1d1	Thunovský
a	a	k8xC	a
Tomášská	tomášský	k2eAgNnPc1d1	tomášský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Smiřický	smiřický	k2eAgInSc1d1	smiřický
palác	palác	k1gInSc1	palác
je	být	k5eAaImIp3nS	být
význačný	význačný	k2eAgMnSc1d1	význačný
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1618	[number]	k4	1618
zde	zde	k6eAd1	zde
skupina	skupina	k1gFnSc1	skupina
protestantských	protestantský	k2eAgMnPc2d1	protestantský
šlechticů	šlechtic	k1gMnPc2	šlechtic
tajně	tajně	k6eAd1	tajně
smluvila	smluvit	k5eAaPmAgFnS	smluvit
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
den	den	k1gInSc4	den
třetí	třetí	k4xOgFnSc4	třetí
pražskou	pražský	k2eAgFnSc4d1	Pražská
defenestraci	defenestrace	k1gFnSc4	defenestrace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
stála	stát	k5eAaImAgFnS	stát
u	u	k7c2	u
počátku	počátek	k1gInSc2	počátek
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Šternberský	šternberský	k2eAgInSc1d1	šternberský
palác	palác	k1gInSc1	palác
patřil	patřit	k5eAaImAgInS	patřit
za	za	k7c2	za
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
k	k	k7c3	k
centrům	centrum	k1gNnPc3	centrum
českého	český	k2eAgInSc2d1	český
kulturního	kulturní	k2eAgInSc2d1	kulturní
a	a	k8xC	a
vědeckého	vědecký	k2eAgInSc2d1	vědecký
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
byl	být	k5eAaImAgInS	být
celý	celý	k2eAgInSc1d1	celý
komplex	komplex	k1gInSc1	komplex
budov	budova	k1gFnPc2	budova
rekonstruován	rekonstruován	k2eAgMnSc1d1	rekonstruován
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
#	#	kIx~	#
<g/>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
tajným	tajný	k2eAgNnSc7d1	tajné
hlasováním	hlasování	k1gNnSc7	hlasování
na	na	k7c6	na
základě	základ	k1gInSc6	základ
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
<g/>
,	,	kIx,	,
rovného	rovný	k2eAgNnSc2d1	rovné
a	a	k8xC	a
přímého	přímý	k2eAgNnSc2d1	přímé
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
zásady	zásada	k1gFnSc2	zásada
poměrného	poměrný	k2eAgNnSc2d1	poměrné
zastoupení	zastoupení	k1gNnSc2	zastoupení
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
volit	volit	k5eAaImF	volit
má	mít	k5eAaImIp3nS	mít
každý	každý	k3xTgMnSc1	každý
občan	občan	k1gMnSc1	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
věku	věk	k1gInSc2	věk
18	[number]	k4	18
let	léto	k1gNnPc2	léto
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
zbaven	zbaven	k2eAgMnSc1d1	zbaven
svéprávnosti	svéprávnost	k1gFnSc3	svéprávnost
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc4	právo
být	být	k5eAaImF	být
volen	volen	k2eAgMnSc1d1	volen
má	mít	k5eAaImIp3nS	mít
každý	každý	k3xTgMnSc1	každý
občan	občan	k1gMnSc1	občan
starší	starší	k1gMnSc1	starší
21	[number]	k4	21
let	léto	k1gNnPc2	léto
s	s	k7c7	s
volebním	volební	k2eAgNnSc7d1	volební
právem	právo	k1gNnSc7	právo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnosti	podrobnost	k1gFnPc4	podrobnost
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
volební	volební	k2eAgInSc1d1	volební
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
nemá	mít	k5eNaImIp3nS	mít
rigiditu	rigidita	k1gFnSc4	rigidita
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ho	on	k3xPp3gMnSc4	on
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
měnit	měnit	k5eAaImF	měnit
skoro	skoro	k6eAd1	skoro
jako	jako	k8xS	jako
běžný	běžný	k2eAgInSc1d1	běžný
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
odlišností	odlišnost	k1gFnSc7	odlišnost
v	v	k7c6	v
zákonodárném	zákonodárný	k2eAgInSc6d1	zákonodárný
procesu	proces	k1gInSc6	proces
je	být	k5eAaImIp3nS	být
podmínka	podmínka	k1gFnSc1	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
musí	muset	k5eAaImIp3nP	muset
souhlasit	souhlasit	k5eAaImF	souhlasit
obě	dva	k4xCgFnPc1	dva
komory	komora	k1gFnPc1	komora
Parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
volebního	volební	k2eAgInSc2d1	volební
zákona	zákon	k1gInSc2	zákon
tak	tak	k6eAd1	tak
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
nemůže	moct	k5eNaImIp3nS	moct
přehlasovat	přehlasovat	k5eAaBmF	přehlasovat
zamítavé	zamítavý	k2eAgNnSc4d1	zamítavé
stanovisko	stanovisko	k1gNnSc4	stanovisko
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
změny	změna	k1gFnPc1	změna
volebního	volební	k2eAgInSc2d1	volební
zákona	zákon	k1gInSc2	zákon
časté	častý	k2eAgNnSc1d1	časté
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
ve	v	k7c6	v
14	[number]	k4	14
vícemandátových	vícemandátův	k2eAgInPc6d1	vícemandátův
obvodech	obvod	k1gInPc6	obvod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
kopírují	kopírovat	k5eAaImIp3nP	kopírovat
krajskou	krajský	k2eAgFnSc4d1	krajská
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
mandátů	mandát	k1gInPc2	mandát
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
obvodech	obvod	k1gInPc6	obvod
není	být	k5eNaImIp3nS	být
předem	předem	k6eAd1	předem
znám	znám	k2eAgMnSc1d1	znám
<g/>
,	,	kIx,	,
určuje	určovat	k5eAaImIp3nS	určovat
se	se	k3xPyFc4	se
až	až	k9	až
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
za	za	k7c4	za
pomoci	pomoc	k1gFnPc4	pomoc
počtu	počet	k1gInSc2	počet
odevzdaných	odevzdaný	k2eAgInPc2d1	odevzdaný
hlasů	hlas	k1gInPc2	hlas
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
obvodech	obvod	k1gInPc6	obvod
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
určení	určení	k1gNnSc3	určení
počtu	počet	k1gInSc2	počet
mandátů	mandát	k1gInPc2	mandát
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
nejprve	nejprve	k6eAd1	nejprve
zjistit	zjistit	k5eAaPmF	zjistit
celorepublikové	celorepublikový	k2eAgNnSc4d1	celorepublikové
mandátové	mandátový	k2eAgNnSc4d1	mandátové
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
vypočte	vypočíst	k5eAaPmIp3nS	vypočíst
jako	jako	k9	jako
podíl	podíl	k1gInSc4	podíl
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
odevzdaných	odevzdaný	k2eAgInPc2d1	odevzdaný
platných	platný	k2eAgInPc2d1	platný
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
mandátů	mandát	k1gInPc2	mandát
(	(	kIx(	(
<g/>
200	[number]	k4	200
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
se	se	k3xPyFc4	se
zaokrouhluje	zaokrouhlovat	k5eAaImIp3nS	zaokrouhlovat
na	na	k7c4	na
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
kraj	kraj	k1gInSc4	kraj
se	se	k3xPyFc4	se
vypočítá	vypočítat	k5eAaPmIp3nS	vypočítat
"	"	kIx"	"
<g/>
teoretický	teoretický	k2eAgInSc4d1	teoretický
<g/>
"	"	kIx"	"
počet	počet	k1gInSc4	počet
mandátů	mandát	k1gInPc2	mandát
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vezme	vzít	k5eAaPmIp3nS	vzít
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
odevzdaných	odevzdaný	k2eAgInPc2d1	odevzdaný
platných	platný	k2eAgInPc2d1	platný
hlasů	hlas	k1gInPc2	hlas
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
podělí	podělit	k5eAaPmIp3nS	podělit
se	se	k3xPyFc4	se
republikovým	republikový	k2eAgNnSc7d1	republikové
mandátovým	mandátový	k2eAgNnSc7d1	mandátové
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Zaokrouhluje	zaokrouhlovat	k5eAaImIp3nS	zaokrouhlovat
se	se	k3xPyFc4	se
dolů	dolů	k6eAd1	dolů
na	na	k7c4	na
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
kraj	kraj	k1gInSc4	kraj
se	se	k3xPyFc4	se
zjistí	zjistit	k5eAaPmIp3nS	zjistit
zbytek	zbytek	k1gInSc1	zbytek
po	po	k7c4	po
dělení	dělení	k1gNnSc4	dělení
počtu	počet	k1gInSc2	počet
odevzdaných	odevzdaný	k2eAgInPc2d1	odevzdaný
platných	platný	k2eAgInPc2d1	platný
hlasů	hlas	k1gInPc2	hlas
kraje	kraj	k1gInSc2	kraj
republikovým	republikový	k2eAgNnSc7d1	republikové
mandátovým	mandátový	k2eAgNnSc7d1	mandátové
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgInPc4d1	zbylý
mandáty	mandát	k1gInPc4	mandát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
přidělit	přidělit	k5eAaPmF	přidělit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přidělí	přidělit	k5eAaPmIp3nS	přidělit
těm	ten	k3xDgInPc3	ten
krajům	kraj	k1gInPc3	kraj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
největší	veliký	k2eAgFnPc1d3	veliký
tento	tento	k3xDgInSc4	tento
zbytek	zbytek	k1gInSc4	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
končí	končit	k5eAaImIp3nS	končit
určení	určení	k1gNnSc1	určení
počtu	počet	k1gInSc2	počet
mandátů	mandát	k1gInPc2	mandát
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
kraje	kraj	k1gInPc4	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sečtení	sečtení	k1gNnSc6	sečtení
hlasů	hlas	k1gInPc2	hlas
se	se	k3xPyFc4	se
vyřadí	vyřadit	k5eAaPmIp3nP	vyřadit
kandidující	kandidující	k2eAgInPc1d1	kandidující
subjekty	subjekt	k1gInPc1	subjekt
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
celostátní	celostátní	k2eAgInSc1d1	celostátní
podíl	podíl	k1gInSc1	podíl
hlasů	hlas	k1gInPc2	hlas
nepřekročil	překročit	k5eNaPmAgInS	překročit
stanovenou	stanovený	k2eAgFnSc4d1	stanovená
uzavírací	uzavírací	k2eAgFnSc4d1	uzavírací
klauzuli	klauzule	k1gFnSc4	klauzule
(	(	kIx(	(
<g/>
5	[number]	k4	5
%	%	kIx~	%
pro	pro	k7c4	pro
samostatně	samostatně	k6eAd1	samostatně
kandidující	kandidující	k2eAgFnPc4d1	kandidující
strany	strana	k1gFnPc4	strana
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
20	[number]	k4	20
%	%	kIx~	%
pro	pro	k7c4	pro
koalice	koalice	k1gFnPc4	koalice
dvou	dva	k4xCgNnPc2	dva
<g/>
,	,	kIx,	,
tří	tři	k4xCgNnPc2	tři
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
více	hodně	k6eAd2	hodně
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgInPc1d1	volební
výsledky	výsledek	k1gInPc1	výsledek
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
vyhodnocují	vyhodnocovat	k5eAaImIp3nP	vyhodnocovat
zvlášť	zvlášť	k6eAd1	zvlášť
po	po	k7c6	po
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
obvodech	obvod	k1gInPc6	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
rozdělovaných	rozdělovaný	k2eAgInPc2d1	rozdělovaný
mandátů	mandát	k1gInPc2	mandát
(	(	kIx(	(
<g/>
200	[number]	k4	200
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
odevzdaných	odevzdaný	k2eAgInPc2d1	odevzdaný
platných	platný	k2eAgInPc2d1	platný
hlasů	hlas	k1gInPc2	hlas
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
krajích	kraj	k1gInPc6	kraj
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
hlasy	hlas	k1gInPc1	hlas
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
kraji	kraj	k1gInSc6	kraj
zvlášť	zvlášť	k6eAd1	zvlášť
přepočítávat	přepočítávat	k5eAaImF	přepočítávat
na	na	k7c4	na
mandáty	mandát	k1gInPc4	mandát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Hondtova	Hondtův	k2eAgFnSc1d1	Hondtova
metoda	metoda	k1gFnSc1	metoda
<g/>
:	:	kIx,	:
počet	počet	k1gInSc1	počet
hlasů	hlas	k1gInPc2	hlas
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
stranu	strana	k1gFnSc4	strana
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
kraji	kraj	k1gInSc6	kraj
(	(	kIx(	(
<g/>
volebním	volební	k2eAgInSc6d1	volební
obvodu	obvod	k1gInSc6	obvod
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
postupně	postupně	k6eAd1	postupně
dělen	dělit	k5eAaImNgInS	dělit
přirozenými	přirozený	k2eAgInPc7d1	přirozený
čísly	číslo	k1gNnPc7	číslo
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
všechny	všechen	k3xTgInPc4	všechen
výsledné	výsledný	k2eAgInPc4d1	výsledný
podíly	podíl	k1gInPc4	podíl
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
jsou	být	k5eAaImIp3nP	být
seřazeny	seřadit	k5eAaPmNgInP	seřadit
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
(	(	kIx(	(
<g/>
a	a	k8xC	a
u	u	k7c2	u
každé	každý	k3xTgFnSc2	každý
položky	položka	k1gFnSc2	položka
je	být	k5eAaImIp3nS	být
označeno	označit	k5eAaPmNgNnS	označit
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
straně	strana	k1gFnSc6	strana
podíl	podíl	k1gInSc1	podíl
náleží	náležet	k5eAaImIp3nS	náležet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stranám	strana	k1gFnPc3	strana
uvedeným	uvedený	k2eAgFnPc3d1	uvedená
na	na	k7c6	na
prvních	první	k4xOgFnPc6	první
položkách	položka	k1gFnPc6	položka
tohoto	tento	k3xDgInSc2	tento
seřazeného	seřazený	k2eAgInSc2d1	seřazený
seznamu	seznam	k1gInSc2	seznam
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
přidělen	přidělit	k5eAaPmNgInS	přidělit
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
počet	počet	k1gInSc1	počet
mandátů	mandát	k1gInPc2	mandát
(	(	kIx(	(
<g/>
přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
bere	brát	k5eAaImIp3nS	brát
v	v	k7c4	v
potaz	potaz	k1gInSc4	potaz
pouze	pouze	k6eAd1	pouze
počet	počet	k1gInSc1	počet
podílů	podíl	k1gInPc2	podíl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
počtu	počet	k1gInSc3	počet
mandátů	mandát	k1gInPc2	mandát
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
kraji	kraj	k1gInSc6	kraj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivým	jednotlivý	k2eAgMnPc3d1	jednotlivý
kandidátům	kandidát	k1gMnPc3	kandidát
strany	strana	k1gFnSc2	strana
jsou	být	k5eAaImIp3nP	být
mandáty	mandát	k1gInPc1	mandát
přiřazovány	přiřazován	k2eAgInPc1d1	přiřazován
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gNnSc2	jejich
pořadí	pořadí	k1gNnSc2	pořadí
na	na	k7c6	na
kandidátkách	kandidátka	k1gFnPc6	kandidátka
<g/>
;	;	kIx,	;
voliči	volič	k1gMnPc1	volič
mohou	moct	k5eAaImIp3nP	moct
toto	tento	k3xDgNnSc4	tento
pořadí	pořadí	k1gNnSc4	pořadí
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
pomocí	pomocí	k7c2	pomocí
čtyř	čtyři	k4xCgInPc2	čtyři
tzv.	tzv.	kA	tzv.
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
zakroužkováním	zakroužkování	k1gNnSc7	zakroužkování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kandidáti	kandidát	k1gMnPc1	kandidát
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
získali	získat	k5eAaPmAgMnP	získat
nejméně	málo	k6eAd3	málo
5	[number]	k4	5
%	%	kIx~	%
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
stranu	strana	k1gFnSc4	strana
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kraje	kraj	k1gInSc2	kraj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
získají	získat	k5eAaPmIp3nP	získat
mandát	mandát	k1gInSc4	mandát
přednostně	přednostně	k6eAd1	přednostně
<g/>
.	.	kIx.	.
</s>
<s>
Rozpuštěním	rozpuštění	k1gNnSc7	rozpuštění
končí	končit	k5eAaImIp3nS	končit
zasedání	zasedání	k1gNnSc1	zasedání
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
zanikají	zanikat	k5eAaImIp3nP	zanikat
mandáty	mandát	k1gInPc1	mandát
zvolených	zvolený	k2eAgMnPc2d1	zvolený
poslanců	poslanec	k1gMnPc2	poslanec
a	a	k8xC	a
do	do	k7c2	do
šedesáti	šedesát	k4xCc2	šedesát
dnů	den	k1gInPc2	den
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
nové	nový	k2eAgFnPc1d1	nová
volby	volba	k1gFnPc1	volba
<g/>
.	.	kIx.	.
</s>
<s>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
ale	ale	k9	ale
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
rozpuštěna	rozpuštěn	k2eAgFnSc1d1	rozpuštěna
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
před	před	k7c7	před
skončením	skončení	k1gNnSc7	skončení
svého	svůj	k3xOyFgNnSc2	svůj
čtyřletého	čtyřletý	k2eAgNnSc2d1	čtyřleté
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Rozpustit	rozpustit	k5eAaPmF	rozpustit
ji	on	k3xPp3gFnSc4	on
může	moct	k5eAaImIp3nS	moct
svým	svůj	k3xOyFgNnSc7	svůj
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
Ústavou	ústava	k1gFnSc7	ústava
stanovených	stanovený	k2eAgInPc2d1	stanovený
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
nevysloví	vyslovit	k5eNaPmIp3nS	vyslovit
důvěru	důvěra	k1gFnSc4	důvěra
nově	nova	k1gFnSc3	nova
jmenované	jmenovaná	k1gFnSc3	jmenovaná
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
předseda	předseda	k1gMnSc1	předseda
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
předsedy	předseda	k1gMnSc2	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
(	(	kIx(	(
<g/>
nastává	nastávat	k5eAaImIp3nS	nastávat
až	až	k9	až
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
nevysloveních	nevyslovení	k1gNnPc6	nevyslovení
důvěry	důvěra	k1gFnSc2	důvěra
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
předseda	předseda	k1gMnSc1	předseda
byl	být	k5eAaImAgMnS	být
<g />
.	.	kIx.	.
</s>
<s>
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
prezidentem	prezident	k1gMnSc7	prezident
<g/>
)	)	kIx)	)
neusnese	usnést	k5eNaPmIp3nS	usnést
se	se	k3xPyFc4	se
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
o	o	k7c6	o
vládním	vládní	k2eAgInSc6d1	vládní
návrhu	návrh	k1gInSc6	návrh
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gNnSc7	jehož
projednáváním	projednávání	k1gNnSc7	projednávání
spojila	spojit	k5eAaPmAgFnS	spojit
vláda	vláda	k1gFnSc1	vláda
otázku	otázka	k1gFnSc4	otázka
důvěry	důvěra	k1gFnSc2	důvěra
přeruší	přerušit	k5eAaPmIp3nS	přerušit
zasedání	zasedání	k1gNnSc1	zasedání
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
delší	dlouhý	k2eAgFnSc4d2	delší
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
přípustné	přípustný	k2eAgNnSc1d1	přípustné
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
120	[number]	k4	120
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
3	[number]	k4	3
měsíce	měsíc	k1gInSc2	měsíc
způsobilá	způsobilý	k2eAgFnSc1d1	způsobilá
se	se	k3xPyFc4	se
usnášet	usnášet	k5eAaImF	usnášet
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
nebylo	být	k5eNaImAgNnS	být
její	její	k3xOp3gNnSc1	její
zasedání	zasedání	k1gNnSc1	zasedání
přerušeno	přerušen	k2eAgNnSc1d1	přerušeno
a	a	k8xC	a
ačkoliv	ačkoliv	k8xS	ačkoliv
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
opakovaně	opakovaně	k6eAd1	opakovaně
svolávána	svoláván	k2eAgFnSc1d1	svolávána
ke	k	k7c3	k
schůzi	schůze	k1gFnSc3	schůze
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
další	další	k2eAgFnSc1d1	další
možnost	možnost	k1gFnSc1	možnost
rozpuštění	rozpuštění	k1gNnSc1	rozpuštění
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
již	již	k6eAd1	již
ale	ale	k8xC	ale
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
nemá	mít	k5eNaImIp3nS	mít
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
<g/>
.	.	kIx.	.
</s>
<s>
Rozpustit	rozpustit	k5eAaPmF	rozpustit
ji	on	k3xPp3gFnSc4	on
musí	muset	k5eAaImIp3nP	muset
vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
<g/>
-li	i	k?	-li
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
usnesením	usnesení	k1gNnSc7	usnesení
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgNnSc7	jenž
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
souhlas	souhlas	k1gInSc4	souhlas
alespoň	alespoň	k9	alespoň
třípětinová	třípětinový	k2eAgFnSc1d1	třípětinová
většina	většina	k1gFnSc1	většina
všech	všecek	k3xTgMnPc2	všecek
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
vyžadovaná	vyžadovaný	k2eAgFnSc1d1	vyžadovaná
pro	pro	k7c4	pro
přijetí	přijetí	k1gNnSc4	přijetí
nebo	nebo	k8xC	nebo
změnu	změna	k1gFnSc4	změna
ústavních	ústavní	k2eAgInPc2d1	ústavní
zákonů	zákon	k1gInPc2	zákon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
novelizace	novelizace	k1gFnSc1	novelizace
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
když	když	k8xS	když
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
daného	daný	k2eAgInSc2d1	daný
roku	rok	k1gInSc2	rok
zrušil	zrušit	k5eAaPmAgInS	zrušit
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
zkrácení	zkrácení	k1gNnSc4	zkrácení
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
podobné	podobný	k2eAgNnSc1d1	podobné
zkrácení	zkrácení	k1gNnSc1	zkrácení
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
nebylo	být	k5eNaImAgNnS	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
rozpuštění	rozpuštění	k1gNnSc2	rozpuštění
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
nabývá	nabývat	k5eAaImIp3nS	nabývat
Senát	senát	k1gInSc1	senát
pravomoc	pravomoc	k1gFnSc4	pravomoc
přijímat	přijímat	k5eAaImF	přijímat
v	v	k7c6	v
neodkladných	odkladný	k2eNgFnPc6d1	neodkladná
věcech	věc	k1gFnPc6	věc
zákonná	zákonný	k2eAgNnPc4d1	zákonné
opatření	opatření	k1gNnPc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
historicky	historicky	k6eAd1	historicky
prvnímu	první	k4xOgNnSc3	první
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
o	o	k7c4	o
rozpuštění	rozpuštění	k1gNnSc4	rozpuštění
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
došlo	dojít	k5eAaPmAgNnS	dojít
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
koalice	koalice	k1gFnSc2	koalice
stran	strana	k1gFnPc2	strana
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
LIDEM	Lido	k1gNnSc7	Lido
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
ač	ač	k8xS	ač
deklarovala	deklarovat	k5eAaBmAgFnS	deklarovat
101	[number]	k4	101
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
podílela	podílet	k5eAaImAgFnS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
odmítnutí	odmítnutí	k1gNnSc4	odmítnutí
vyslovení	vyslovení	k1gNnSc4	vyslovení
důvěry	důvěra	k1gFnSc2	důvěra
vládě	vláda	k1gFnSc6	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
Rusnoka	Rusnoko	k1gNnSc2	Rusnoko
nižším	nízký	k2eAgInSc7d2	nižší
počtem	počet	k1gInSc7	počet
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
očekáváno	očekávat	k5eAaImNgNnS	očekávat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rozpuštění	rozpuštění	k1gNnSc4	rozpuštění
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
140	[number]	k4	140
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
rozpustil	rozpustit	k5eAaPmAgMnS	rozpustit
sněmovnu	sněmovna	k1gFnSc4	sněmovna
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
termín	termín	k1gInSc1	termín
předčasných	předčasný	k2eAgFnPc2d1	předčasná
voleb	volba	k1gFnPc2	volba
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
a	a	k8xC	a
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Seznam	seznam	k1gInSc4	seznam
předsedů	předseda	k1gMnPc2	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Seznam	seznam	k1gInSc1	seznam
členů	člen	k1gMnPc2	člen
vedení	vedení	k1gNnSc2	vedení
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Výbory	výbor	k1gInPc1	výbor
-	-	kIx~	-
podle	podle	k7c2	podle
jednacího	jednací	k2eAgInSc2d1	jednací
řádu	řád	k1gInSc2	řád
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
je	být	k5eAaImIp3nS	být
komora	komora	k1gFnSc1	komora
povinna	povinen	k2eAgFnSc1d1	povinna
zřídit	zřídit	k5eAaPmF	zřídit
mandátový	mandátový	k2eAgInSc4d1	mandátový
a	a	k8xC	a
imunitní	imunitní	k2eAgInSc4d1	imunitní
výbor	výbor	k1gInSc4	výbor
<g/>
,	,	kIx,	,
petiční	petiční	k2eAgInSc1d1	petiční
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
rozpočtový	rozpočtový	k2eAgInSc1d1	rozpočtový
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
organizační	organizační	k2eAgInSc1d1	organizační
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
volební	volební	k2eAgInSc1d1	volební
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
evropské	evropský	k2eAgFnPc4d1	Evropská
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Schůze	schůze	k1gFnPc1	schůze
výborů	výbor	k1gInPc2	výbor
jsou	být	k5eAaImIp3nP	být
veřejné	veřejný	k2eAgNnSc4d1	veřejné
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
schůzí	schůze	k1gFnSc7	schůze
organizačního	organizační	k2eAgInSc2d1	organizační
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
mandátového	mandátový	k2eAgInSc2d1	mandátový
a	a	k8xC	a
imunitního	imunitní	k2eAgInSc2d1	imunitní
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Výbory	výbor	k1gInPc1	výbor
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
zřizovat	zřizovat	k5eAaImF	zřizovat
podvýbory	podvýbor	k1gInPc1	podvýbor
<g/>
,	,	kIx,	,
jednání	jednání	k1gNnPc1	jednání
podvýborů	podvýbor	k1gInPc2	podvýbor
nejsou	být	k5eNaImIp3nP	být
veřejná	veřejný	k2eAgNnPc1d1	veřejné
<g/>
.	.	kIx.	.
</s>
<s>
Počty	počet	k1gInPc4	počet
výborů	výbor	k1gInPc2	výbor
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
strany	strana	k1gFnPc4	strana
v	v	k7c4	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
Seznam	seznam	k1gInSc1	seznam
výborů	výbor	k1gInPc2	výbor
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
<g/>
:	:	kIx,	:
Hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
výbor	výbor	k1gInSc1	výbor
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Ivan	Ivan	k1gMnSc1	Ivan
Pilný	pilný	k2eAgMnSc1d1	pilný
(	(	kIx(	(
<g/>
ANO	ano	k9	ano
<g/>
)	)	kIx)	)
Kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
výbor	výbor	k1gInSc1	výbor
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Vladimír	Vladimír	k1gMnSc1	Vladimír
Koníček	Koníček	k1gMnSc1	Koníček
(	(	kIx(	(
<g/>
KSČM	KSČM	kA	KSČM
<g/>
)	)	kIx)	)
Mandátový	mandátový	k2eAgInSc4d1	mandátový
a	a	k8xC	a
imunitní	imunitní	k2eAgInSc4d1	imunitní
<g />
.	.	kIx.	.
</s>
<s>
výbor	výbor	k1gInSc1	výbor
-	-	kIx~	-
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
<g/>
:	:	kIx,	:
Miroslava	Miroslava	k1gFnSc1	Miroslava
Němcová	Němcová	k1gFnSc1	Němcová
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
Organizační	organizační	k2eAgInSc4d1	organizační
výbor	výbor	k1gInSc4	výbor
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Hamáček	Hamáček	k1gMnSc1	Hamáček
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
Petiční	petiční	k2eAgInSc1d1	petiční
výbor	výbor	k1gInSc1	výbor
-	-	kIx~	-
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
<g/>
:	:	kIx,	:
Zuzka	Zuzka	k1gFnSc1	Zuzka
Bebarová-Rujbrová	Bebarová-Rujbrová	k1gFnSc1	Bebarová-Rujbrová
(	(	kIx(	(
<g/>
KSČM	KSČM	kA	KSČM
<g/>
)	)	kIx)	)
Rozpočtový	rozpočtový	k2eAgInSc1d1	rozpočtový
výbor	výbor	k1gInSc1	výbor
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Votava	Votava	k1gMnSc1	Votava
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Ústavně	ústavně	k6eAd1	ústavně
právní	právní	k2eAgInSc1d1	právní
výbor	výbor	k1gInSc1	výbor
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Jeroným	Jeroným	k1gMnSc1	Jeroným
Tejc	Tejc	k1gFnSc1	Tejc
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
Volební	volební	k2eAgInSc1d1	volební
výbor	výbor	k1gInSc1	výbor
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Martin	Martin	k1gMnSc1	Martin
Komárek	Komárek	k1gMnSc1	Komárek
(	(	kIx(	(
<g/>
ANO	ano	k9	ano
<g/>
)	)	kIx)	)
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Roman	Roman	k1gMnSc1	Roman
Váňa	Váňa	k1gMnSc1	Váňa
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
evropské	evropský	k2eAgFnPc4d1	Evropská
záležitosti	záležitost	k1gFnPc4	záležitost
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Benešík	Benešík	k1gMnSc1	Benešík
(	(	kIx(	(
<g/>
KDU-ČSL	KDU-ČSL	k1gMnSc1	KDU-ČSL
<g/>
)	)	kIx)	)
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
obranu	obrana	k1gFnSc4	obrana
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
David	David	k1gMnSc1	David
Kádner	Kádner	k1gMnSc1	Kádner
(	(	kIx(	(
<g/>
Úsvit	úsvit	k1gInSc1	úsvit
<g/>
)	)	kIx)	)
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
sociální	sociální	k2eAgFnSc4d1	sociální
politiku	politika	k1gFnSc4	politika
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Zavadil	Zavadil	k1gMnSc1	Zavadil
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
mládež	mládež	k1gFnSc4	mládež
a	a	k8xC	a
tělovýchovu	tělovýchova	k1gFnSc4	tělovýchova
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Zlatuška	Zlatuška	k1gMnSc1	Zlatuška
(	(	kIx(	(
<g/>
ANO	ano	k9	ano
<g/>
)	)	kIx)	)
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
správu	správa	k1gFnSc4	správa
a	a	k8xC	a
regionální	regionální	k2eAgInSc4d1	regionální
rozvoj	rozvoj	k1gInSc4	rozvoj
-	-	kIx~	-
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
<g/>
:	:	kIx,	:
Milada	Milada	k1gFnSc1	Milada
Halíková	Halíková	k1gFnSc1	Halíková
(	(	kIx(	(
<g/>
KSČM	KSČM	kA	KSČM
<g/>
)	)	kIx)	)
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
zdravotnictví	zdravotnictví	k1gNnSc4	zdravotnictví
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Rostislav	Rostislava	k1gFnPc2	Rostislava
Vyzula	vyzout	k5eAaPmAgFnS	vyzout
(	(	kIx(	(
<g/>
ANO	ano	k9	ano
<g/>
)	)	kIx)	)
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Robin	robin	k2eAgInSc1d1	robin
Böhnisch	Böhnisch	k1gInSc1	Böhnisch
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
Zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
výbor	výbor	k1gInSc1	výbor
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
(	(	kIx(	(
<g/>
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
Starostové	Starostové	k2eAgInSc1d1	Starostové
<g/>
)	)	kIx)	)
Zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
výbor	výbor	k1gInSc1	výbor
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Faltýnek	Faltýnka	k1gFnPc2	Faltýnka
(	(	kIx(	(
<g/>
ANO	ano	k9	ano
<g/>
)	)	kIx)	)
Komise	komise	k1gFnSc1	komise
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
vyšetření	vyšetření	k1gNnSc4	vyšetření
věcí	věc	k1gFnPc2	věc
veřejného	veřejný	k2eAgInSc2d1	veřejný
zájmu	zájem	k1gInSc2	zájem
vyšetřovací	vyšetřovací	k2eAgFnSc2d1	vyšetřovací
komise	komise	k1gFnSc2	komise
<g/>
)	)	kIx)	)
Organizační	organizační	k2eAgInSc4d1	organizační
a	a	k8xC	a
technický	technický	k2eAgInSc4d1	technický
provoz	provoz	k1gInSc4	provoz
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Kancelář	kancelář	k1gFnSc1	kancelář
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nP	stát
vedoucí	vedoucí	k2eAgFnPc1d1	vedoucí
Kanceláře	kancelář	k1gFnPc1	kancelář
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
přímo	přímo	k6eAd1	přímo
předsedovi	předseda	k1gMnSc3	předseda
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Poslanecký	poslanecký	k2eAgInSc1d1	poslanecký
klub	klub	k1gInSc1	klub
a	a	k8xC	a
Seznam	seznam	k1gInSc1	seznam
členů	člen	k1gMnPc2	člen
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Česka	Česko	k1gNnPc1	Česko
strana	strana	k1gFnSc1	strana
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
<g/>
:	:	kIx,	:
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
50	[number]	k4	50
<g/>
,	,	kIx,	,
členy	člen	k1gInPc4	člen
klubu	klub	k1gInSc2	klub
50	[number]	k4	50
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Roman	Roman	k1gMnSc1	Roman
Sklenák	Sklenák	k1gMnSc1	Sklenák
<g/>
)	)	kIx)	)
ANO	ano	k9	ano
<g/>
:	:	kIx,	:
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
47	[number]	k4	47
<g/>
,	,	kIx,	,
členy	člen	k1gInPc4	člen
klubu	klub	k1gInSc2	klub
47	[number]	k4	47
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Faltýnek	Faltýnka	k1gFnPc2	Faltýnka
<g/>
)	)	kIx)	)
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
33	[number]	k4	33
<g/>
,	,	kIx,	,
členy	člen	k1gInPc4	člen
klubu	klub	k1gInSc2	klub
33	[number]	k4	33
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Pavel	Pavel	k1gMnSc1	Pavel
Kováčik	Kováčik	k1gMnSc1	Kováčik
<g/>
)	)	kIx)	)
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
Starostové	Starostová	k1gFnPc1	Starostová
<g/>
:	:	kIx,	:
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
26	[number]	k4	26
<g/>
,	,	kIx,	,
členy	člen	k1gInPc4	člen
klubu	klub	k1gInSc2	klub
26	[number]	k4	26
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kalousek	Kalousek	k1gMnSc1	Kalousek
<g/>
)	)	kIx)	)
Občanská	občanský	k2eAgFnSc1d1	občanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
<g/>
:	:	kIx,	:
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
16	[number]	k4	16
<g/>
,	,	kIx,	,
členy	člen	k1gMnPc4	člen
<g />
.	.	kIx.	.
</s>
<s>
klubu	klub	k1gInSc2	klub
16	[number]	k4	16
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Stanjura	Stanjur	k1gMnSc2	Stanjur
<g/>
)	)	kIx)	)
Úsvit	úsvit	k1gInSc1	úsvit
-	-	kIx~	-
Národní	národní	k2eAgFnSc1d1	národní
Koalice	koalice	k1gFnSc1	koalice
<g/>
:	:	kIx,	:
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
14	[number]	k4	14
<g/>
,	,	kIx,	,
členy	člen	k1gInPc4	člen
klubu	klub	k1gInSc2	klub
9	[number]	k4	9
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Marek	Marek	k1gMnSc1	Marek
Černoch	Černoch	k1gMnSc1	Černoch
<g/>
)	)	kIx)	)
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
a	a	k8xC	a
demokratická	demokratický	k2eAgFnSc1d1	demokratická
unie	unie	k1gFnSc1	unie
-	-	kIx~	-
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
lidová	lidový	k2eAgFnSc1d1	lidová
<g/>
:	:	kIx,	:
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
14	[number]	k4	14
<g/>
,	,	kIx,	,
členy	člen	k1gMnPc4	člen
<g />
.	.	kIx.	.
</s>
<s>
klubu	klub	k1gInSc2	klub
14	[number]	k4	14
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Mihola	Mihola	k1gFnSc1	Mihola
<g/>
)	)	kIx)	)
Nezařazení	nezařazení	k1gNnSc1	nezařazení
<g/>
:	:	kIx,	:
5	[number]	k4	5
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
bývalí	bývalý	k2eAgMnPc1d1	bývalý
členové	člen	k1gMnPc1	člen
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
Úsvitu	úsvit	k1gInSc2	úsvit
přímé	přímý	k2eAgFnSc2d1	přímá
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
:	:	kIx,	:
Radim	Radim	k1gMnSc1	Radim
Fiala	Fiala	k1gMnSc1	Fiala
/	/	kIx~	/
<g/>
SPD	SPD	kA	SPD
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
Tomio	Tomio	k6eAd1	Tomio
Okamura	Okamura	k1gFnSc1	Okamura
/	/	kIx~	/
<g/>
SPD	SPD	kA	SPD
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Holík	Holík	k1gMnSc1	Holík
/	/	kIx~	/
<g/>
SPD	SPD	kA	SPD
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Šarapatka	šarapatka	k1gFnSc1	šarapatka
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Pražák	Pražák	k1gMnSc1	Pražák
<g/>
)	)	kIx)	)
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
