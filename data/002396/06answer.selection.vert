<s>
Rod	rod	k1gInSc1	rod
Clostridium	Clostridium	k1gNnSc1	Clostridium
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
80	[number]	k4	80
druhů	druh	k1gInPc2	druh
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
citlivostí	citlivost	k1gFnSc7	citlivost
ke	k	k7c3	k
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
schopností	schopnost	k1gFnSc7	schopnost
tvořit	tvořit	k5eAaImF	tvořit
klidové	klidový	k2eAgNnSc1d1	klidové
stadium	stadium	k1gNnSc1	stadium
<g/>
,	,	kIx,	,
endospory	endospora	k1gFnPc1	endospora
<g/>
.	.	kIx.	.
</s>
