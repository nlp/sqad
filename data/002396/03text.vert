<s>
Rod	rod	k1gInSc1	rod
Clostridium	Clostridium	k1gNnSc1	Clostridium
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
80	[number]	k4	80
druhů	druh	k1gInPc2	druh
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
citlivostí	citlivost	k1gFnSc7	citlivost
ke	k	k7c3	k
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
schopností	schopnost	k1gFnSc7	schopnost
tvořit	tvořit	k5eAaImF	tvořit
klidové	klidový	k2eAgNnSc1d1	klidové
stadium	stadium	k1gNnSc1	stadium
<g/>
,	,	kIx,	,
endospory	endospora	k1gFnPc1	endospora
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
saprofyty	saprofyt	k1gInPc1	saprofyt
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
mají	mít	k5eAaImIp3nP	mít
veterinární	veterinární	k2eAgInPc1d1	veterinární
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
<g/>
,	,	kIx,	,
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
v	v	k7c6	v
bahně	bahno	k1gNnSc6	bahno
rybníků	rybník	k1gInPc2	rybník
<g/>
,	,	kIx,	,
řek	řeka	k1gFnPc2	řeka
i	i	k8xC	i
mořského	mořský	k2eAgNnSc2d1	mořské
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
v	v	k7c6	v
prachu	prach	k1gInSc6	prach
i	i	k9	i
na	na	k7c4	na
vegetaci	vegetace	k1gFnSc4	vegetace
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
příčinou	příčina	k1gFnSc7	příčina
exogenních	exogenní	k2eAgFnPc2d1	exogenní
infekcí	infekce	k1gFnPc2	infekce
po	po	k7c6	po
poranění	poranění	k1gNnSc6	poranění
nebo	nebo	k8xC	nebo
pozření	pozření	k1gNnSc6	pozření
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
z	z	k7c2	z
patogenních	patogenní	k2eAgFnPc2d1	patogenní
klostridií	klostridie	k1gFnPc2	klostridie
je	být	k5eAaImIp3nS	být
komensálem	komensál	k1gInSc7	komensál
střevního	střevní	k2eAgInSc2d1	střevní
traktu	trakt	k1gInSc2	trakt
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
příčinou	příčina	k1gFnSc7	příčina
endogenních	endogenní	k2eAgFnPc2d1	endogenní
infekcí	infekce	k1gFnPc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
nebo	nebo	k8xC	nebo
typů	typ	k1gInPc2	typ
je	být	k5eAaImIp3nS	být
geograficky	geograficky	k6eAd1	geograficky
omezen	omezit	k5eAaPmNgInS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Vegetativní	vegetativní	k2eAgFnPc1d1	vegetativní
formy	forma	k1gFnPc1	forma
klostridií	klostridie	k1gFnPc2	klostridie
jsou	být	k5eAaImIp3nP	být
grampozitivní	grampozitivní	k2eAgFnPc1d1	grampozitivní
tyčinky	tyčinka	k1gFnPc1	tyčinka
různé	různý	k2eAgFnPc1d1	různá
velikosti	velikost	k1gFnSc3	velikost
(	(	kIx(	(
<g/>
0,3	[number]	k4	0,3
<g/>
-	-	kIx~	-
<g/>
1,3	[number]	k4	1,3
x	x	k?	x
3-10	[number]	k4	3-10
μ	μ	k?	μ
<g/>
)	)	kIx)	)
i	i	k8xC	i
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
pohyblivé	pohyblivý	k2eAgFnPc1d1	pohyblivá
pomocí	pomoc	k1gFnSc7	pomoc
peritrichálně	peritrichálně	k6eAd1	peritrichálně
uložených	uložený	k2eAgInPc2d1	uložený
bičíků	bičík	k1gInPc2	bičík
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
C.	C.	kA	C.
perfringens	perfringens	k6eAd1	perfringens
a	a	k8xC	a
jemu	on	k3xPp3gMnSc3	on
příbuzných	příbuzný	k2eAgMnPc2d1	příbuzný
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nemají	mít	k5eNaImIp3nP	mít
bičíky	bičík	k1gInPc4	bičík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klostridie	Klostridie	k1gFnPc1	Klostridie
fermentují	fermentovat	k5eAaBmIp3nP	fermentovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
oxidáza	oxidáza	k1gFnSc1	oxidáza
a	a	k8xC	a
kataláza	kataláza	k1gFnSc1	kataláza
negativní	negativní	k2eAgFnSc1d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
Vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
média	médium	k1gNnPc1	médium
obohacená	obohacený	k2eAgNnPc1d1	obohacené
o	o	k7c4	o
aminokyseliny	aminokyselina	k1gFnPc4	aminokyselina
<g/>
,	,	kIx,	,
sacharidy	sacharid	k1gInPc4	sacharid
<g/>
,	,	kIx,	,
vitamíny	vitamín	k1gInPc4	vitamín
a	a	k8xC	a
krev	krev	k1gFnSc4	krev
nebo	nebo	k8xC	nebo
sérum	sérum	k1gNnSc4	sérum
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
striktními	striktní	k2eAgMnPc7d1	striktní
anaeroby	anaerob	k1gMnPc7	anaerob
<g/>
,	,	kIx,	,
při	při	k7c6	při
kultivaci	kultivace	k1gFnSc6	kultivace
preferují	preferovat	k5eAaImIp3nP	preferovat
atmosféru	atmosféra	k1gFnSc4	atmosféra
obsahující	obsahující	k2eAgInPc1d1	obsahující
2-10	[number]	k4	2-10
%	%	kIx~	%
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
a	a	k8xC	a
teplotu	teplota	k1gFnSc4	teplota
37	[number]	k4	37
°	°	k?	°
<g/>
C.	C.	kA	C.
Většina	většina	k1gFnSc1	většina
patogenních	patogenní	k2eAgMnPc2d1	patogenní
druhů	druh	k1gInPc2	druh
produkuje	produkovat	k5eAaImIp3nS	produkovat
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
exotoxinů	exotoxin	k1gInPc2	exotoxin
<g/>
.	.	kIx.	.
</s>
<s>
Klostridie	Klostridie	k1gFnPc1	Klostridie
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
bakteriofágy	bakteriofág	k1gInPc4	bakteriofág
i	i	k8xC	i
episomy	episom	k1gInPc4	episom
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
kódují	kódovat	k5eAaBmIp3nP	kódovat
některé	některý	k3yIgInPc1	některý
toxiny	toxin	k1gInPc1	toxin
<g/>
.	.	kIx.	.
</s>
<s>
Episomálně	Episomálně	k6eAd1	Episomálně
je	být	k5eAaImIp3nS	být
kódována	kódovat	k5eAaBmNgFnS	kódovat
také	také	k9	také
rezistence	rezistence	k1gFnSc1	rezistence
na	na	k7c4	na
některá	některý	k3yIgNnPc4	některý
antibiotikum	antibiotikum	k1gNnSc4	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
klostridiemi	klostridie	k1gFnPc7	klostridie
vyvolávaného	vyvolávaný	k2eAgNnSc2d1	vyvolávané
onemocnění	onemocnění	k1gNnSc2	onemocnění
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
<g/>
:	:	kIx,	:
neurotropní	urotropní	k2eNgFnPc1d1	urotropní
klostridie	klostridie	k1gFnPc1	klostridie
(	(	kIx(	(
<g/>
C.	C.	kA	C.
tetani	tetaň	k1gFnSc6	tetaň
a	a	k8xC	a
C.	C.	kA	C.
botulinum	botulinum	k1gInSc1	botulinum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
produkují	produkovat	k5eAaImIp3nP	produkovat
potentní	potentní	k2eAgFnPc1d1	potentní
neurotoxiny	neurotoxina	k1gFnPc1	neurotoxina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
invazivní	invazivní	k2eAgMnPc1d1	invazivní
a	a	k8xC	a
kolonizují	kolonizovat	k5eAaBmIp3nP	kolonizovat
hostitele	hostitel	k1gMnSc4	hostitel
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
omezené	omezený	k2eAgFnSc3d1	omezená
míře	míra	k1gFnSc3	míra
<g/>
,	,	kIx,	,
histotoxické	histotoxická	k1gFnSc3	histotoxická
klostridie	klostridie	k1gFnSc2	klostridie
(	(	kIx(	(
<g/>
C.	C.	kA	C.
septicum	septicum	k1gNnSc1	septicum
<g/>
,	,	kIx,	,
C.	C.	kA	C.
perfringens	perfringens	k1gInSc1	perfringens
<g />
.	.	kIx.	.
</s>
<s>
typu	typ	k1gInSc3	typ
A	A	kA	A
<g/>
,	,	kIx,	,
C.	C.	kA	C.
colinum	colinum	k1gInSc1	colinum
<g/>
,	,	kIx,	,
C.	C.	kA	C.
chauvoei	chauvoei	k1gNnSc1	chauvoei
<g/>
,	,	kIx,	,
C.	C.	kA	C.
novyi	novyi	k1gNnSc1	novyi
<g/>
,	,	kIx,	,
C.	C.	kA	C.
haemolyticum	haemolyticum	k1gInSc1	haemolyticum
<g/>
,	,	kIx,	,
C.	C.	kA	C.
sordellii	sordellie	k1gFnSc6	sordellie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
produkují	produkovat	k5eAaImIp3nP	produkovat
méně	málo	k6eAd2	málo
potentní	potentní	k2eAgInPc1d1	potentní
toxiny	toxin	k1gInPc1	toxin
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
invazivní	invazivní	k2eAgMnPc1d1	invazivní
a	a	k8xC	a
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
nekrotizující	krotizující	k2eNgFnPc1d1	nekrotizující
infekce	infekce	k1gFnPc1	infekce
měkkých	měkký	k2eAgFnPc2d1	měkká
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
klostridie	klostridie	k1gFnSc1	klostridie
vyvolávající	vyvolávající	k2eAgFnSc2d1	vyvolávající
enterotoxémie	enterotoxémie	k1gFnSc2	enterotoxémie
(	(	kIx(	(
<g/>
C.	C.	kA	C.
perfringens	perfringens	k1gInSc4	perfringens
typu	typ	k1gInSc2	typ
A-E	A-E	k1gFnSc2	A-E
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Enterotoxiny	Enterotoxin	k1gInPc1	Enterotoxin
produkované	produkovaný	k2eAgInPc1d1	produkovaný
ve	v	k7c6	v
střevě	střevo	k1gNnSc6	střevo
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
a	a	k8xC	a
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
generalizované	generalizovaný	k2eAgFnPc1d1	generalizovaná
toxémie	toxémie	k1gFnPc1	toxémie
<g/>
,	,	kIx,	,
klostridie	klostridie	k1gFnPc1	klostridie
způsobující	způsobující	k2eAgFnPc1d1	způsobující
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
indukované	indukovaný	k2eAgFnPc4d1	indukovaná
enteritidy	enteritida	k1gFnSc2	enteritida
(	(	kIx(	(
<g/>
C.	C.	kA	C.
difficile	difficile	k6eAd1	difficile
a	a	k8xC	a
C.	C.	kA	C.
spiroforme	spiroform	k1gInSc5	spiroform
<g/>
)	)	kIx)	)
Klostridie	Klostridie	k1gFnPc1	Klostridie
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
autochtonní	autochtonní	k2eAgFnSc4d1	autochtonní
flóru	flóra	k1gFnSc4	flóra
u	u	k7c2	u
dravců	dravec	k1gMnPc2	dravec
a	a	k8xC	a
ptáků	pták	k1gMnPc2	pták
s	s	k7c7	s
vyvinutými	vyvinutý	k2eAgMnPc7d1	vyvinutý
slepými	slepý	k2eAgMnPc7d1	slepý
střevy	střevo	k1gNnPc7	střevo
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
u	u	k7c2	u
hrabavých	hrabavý	k2eAgMnPc2d1	hrabavý
ptáků	pták	k1gMnPc2	pták
z	z	k7c2	z
podřádu	podřád	k1gInSc2	podřád
Phasiani	Phasiaň	k1gFnSc3	Phasiaň
a	a	k8xC	a
u	u	k7c2	u
Anseriformes	Anseriformesa	k1gFnPc2	Anseriformesa
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
s	s	k7c7	s
nedostatečně	dostatečně	k6eNd1	dostatečně
vyvinutými	vyvinutý	k2eAgFnPc7d1	vyvinutá
slepými	slepý	k2eAgFnPc7d1	slepá
střevy	střevo	k1gNnPc7	střevo
nebo	nebo	k8xC	nebo
bez	bez	k7c2	bez
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
klostridie	klostridie	k1gFnPc1	klostridie
izolují	izolovat	k5eAaBmIp3nP	izolovat
zřídka	zřídka	k6eAd1	zřídka
ze	z	k7c2	z
střevního	střevní	k2eAgInSc2d1	střevní
traktu	trakt	k1gInSc2	trakt
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
přítomnost	přítomnost	k1gFnSc1	přítomnost
pouze	pouze	k6eAd1	pouze
přechodná	přechodný	k2eAgFnSc1d1	přechodná
<g/>
.	.	kIx.	.
</s>
<s>
Predispozičními	predispoziční	k2eAgInPc7d1	predispoziční
faktory	faktor	k1gInPc7	faktor
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
klostridiových	klostridiový	k2eAgNnPc2d1	klostridiový
onemocnění	onemocnění	k1gNnPc2	onemocnění
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
dietní	dietní	k2eAgFnPc1d1	dietní
závady	závada	k1gFnPc1	závada
<g/>
,	,	kIx,	,
stresy	stres	k1gInPc1	stres
<g/>
,	,	kIx,	,
špatná	špatný	k2eAgFnSc1d1	špatná
zoohygiena	zoohygiena	k1gFnSc1	zoohygiena
<g/>
,	,	kIx,	,
přeskladňování	přeskladňování	k1gNnSc1	přeskladňování
<g/>
,	,	kIx,	,
kokcidióza	kokcidióza	k1gFnSc1	kokcidióza
<g/>
,	,	kIx,	,
imunosuprese	imunosuprese	k1gFnSc1	imunosuprese
(	(	kIx(	(
<g/>
infekční	infekční	k2eAgFnSc1d1	infekční
burzitida	burzitida	k1gFnSc1	burzitida
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
infekční	infekční	k2eAgInPc1d1	infekční
anémie	anémie	k1gFnPc4	anémie
kuřat	kuře	k1gNnPc2	kuře
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
U	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
klostridie	klostridie	k1gFnPc1	klostridie
původci	původce	k1gMnSc3	původce
ulcerativní	ulcerativní	k2eAgFnSc2d1	ulcerativní
enteritidy	enteritida	k1gFnSc2	enteritida
(	(	kIx(	(
<g/>
C.	C.	kA	C.
colinum	colinum	k1gInSc1	colinum
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nekrotické	nekrotický	k2eAgFnSc2d1	nekrotická
enteritidy	enteritida	k1gFnSc2	enteritida
(	(	kIx(	(
<g/>
C.	C.	kA	C.
perfringens	perfringens	k1gInSc1	perfringens
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gangrenózní	gangrenózní	k2eAgFnSc2d1	gangrenózní
dermatitidy	dermatitida	k1gFnSc2	dermatitida
(	(	kIx(	(
<g/>
C.	C.	kA	C.
septicum	septicum	k1gNnSc1	septicum
<g/>
,	,	kIx,	,
C.	C.	kA	C.
perfringens	perfringens	k1gInSc1	perfringens
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
botulismu	botulismus	k1gInSc2	botulismus
(	(	kIx(	(
<g/>
C.	C.	kA	C.
botulinum	botulinum	k1gNnSc4	botulinum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
omfalitid	omfalitis	k1gFnPc2	omfalitis
a	a	k8xC	a
infekce	infekce	k1gFnSc2	infekce
žloutkového	žloutkový	k2eAgInSc2d1	žloutkový
vaku	vak	k1gInSc2	vak
(	(	kIx(	(
<g/>
C.	C.	kA	C.
perfringens	perfringens	k1gInSc1	perfringens
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
druhy	druh	k1gInPc1	druh
(	(	kIx(	(
<g/>
C.	C.	kA	C.
fallax	fallax	k1gInSc1	fallax
<g/>
,	,	kIx,	,
C.	C.	kA	C.
novyi	novy	k1gFnSc2	novy
a	a	k8xC	a
C.	C.	kA	C.
sporogenes	sporogenes	k1gInSc1	sporogenes
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
izolovány	izolovat	k5eAaBmNgInP	izolovat
ojediněle	ojediněle	k6eAd1	ojediněle
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
klinických	klinický	k2eAgInPc2d1	klinický
syndromů	syndrom	k1gInPc2	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
chauvoei	chauvoei	k1gNnSc1	chauvoei
bylo	být	k5eAaImAgNnS	být
izolováno	izolovat	k5eAaBmNgNnS	izolovat
u	u	k7c2	u
pštrosů	pštros	k1gMnPc2	pštros
s	s	k7c7	s
nervově	nervově	k6eAd1	nervově
paralytickými	paralytický	k2eAgInPc7d1	paralytický
příznaky	příznak	k1gInPc7	příznak
<g/>
,	,	kIx,	,
C.	C.	kA	C.
difficile	difficile	k6eAd1	difficile
u	u	k7c2	u
kuřat	kuře	k1gNnPc2	kuře
pštrosů	pštros	k1gMnPc2	pštros
postižených	postižený	k2eAgFnPc2d1	postižená
enterotoxémií	enterotoxémie	k1gFnPc2	enterotoxémie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
některých	některý	k3yIgInPc2	některý
starších	starý	k2eAgInPc2d2	starší
údajů	údaj	k1gInPc2	údaj
jsou	být	k5eAaImIp3nP	být
ptáci	pták	k1gMnPc1	pták
dnes	dnes	k6eAd1	dnes
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
rezistentní	rezistentní	k2eAgNnSc4d1	rezistentní
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
C.	C.	kA	C.
tetani	tetaň	k1gFnSc6	tetaň
<g/>
.	.	kIx.	.
</s>
<s>
JURAJDA	JURAJDA	kA	JURAJDA
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Nemoci	nemoc	k1gFnPc4	nemoc
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
ptactva	ptactvo	k1gNnSc2	ptactvo
-	-	kIx~	-
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
a	a	k8xC	a
mykotické	mykotický	k2eAgFnSc2d1	mykotická
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
ES	ES	kA	ES
VFU	VFU	kA	VFU
Brno	Brno	k1gNnSc4	Brno
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
185	[number]	k4	185
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7305	[number]	k4	7305
<g/>
-	-	kIx~	-
<g/>
464	[number]	k4	464
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
RITCHIE	RITCHIE	kA	RITCHIE
<g/>
,	,	kIx,	,
B.W.	B.W.	k1gFnSc1	B.W.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
..	..	k?	..
Avian	Avian	k1gInSc1	Avian
Medicine	Medicin	k1gInSc5	Medicin
<g/>
:	:	kIx,	:
Principles	Principles	k1gInSc1	Principles
and	and	k?	and
Application	Application	k1gInSc1	Application
<g/>
.	.	kIx.	.
</s>
<s>
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
USA	USA	kA	USA
:	:	kIx,	:
Wingers	Wingers	k1gInSc1	Wingers
Publ	Publ	k1gInSc1	Publ
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
1384	[number]	k4	1384
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
9636996	[number]	k4	9636996
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
SAIF	SAIF	kA	SAIF
<g/>
,	,	kIx,	,
Y.	Y.	kA	Y.
<g/>
M.	M.	kA	M.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Diseases	Diseases	k1gInSc1	Diseases
of	of	k?	of
Poultry	Poultr	k1gMnPc7	Poultr
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Ames	Ames	k1gInSc1	Ames
<g/>
,	,	kIx,	,
USA	USA	kA	USA
:	:	kIx,	:
Iowa	Iowa	k1gMnSc1	Iowa
State	status	k1gInSc5	status
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
Blackwell	Blackwell	k1gMnSc1	Blackwell
Publ	Publ	k1gMnSc1	Publ
<g/>
.	.	kIx.	.
</s>
<s>
Comp	Comp	k1gInSc1	Comp
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
1231	[number]	k4	1231
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8138	[number]	k4	8138
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
423	[number]	k4	423
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
