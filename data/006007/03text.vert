<s>
Šódžo	Šódžo	k1gNnSc1	Šódžo
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
少	少	k?	少
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
shō	shō	k?	shō
či	či	k8xC	či
shoujo	shoujo	k1gNnSc1	shoujo
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
dívka	dívka	k1gFnSc1	dívka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonské	japonský	k2eAgNnSc1d1	Japonské
slovo	slovo	k1gNnSc1	slovo
užívané	užívaný	k2eAgNnSc1d1	užívané
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
mangy	mango	k1gNnPc7	mango
nebo	nebo	k8xC	nebo
anime	animat	k5eAaPmIp3nS	animat
určeného	určený	k2eAgInSc2d1	určený
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
dívky	dívka	k1gFnPc4	dívka
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
11-19	[number]	k4	11-19
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
šónen	šónna	k1gFnPc2	šónna
zaměřeného	zaměřený	k2eAgMnSc2d1	zaměřený
na	na	k7c4	na
chlapce	chlapec	k1gMnSc4	chlapec
<g/>
.	.	kIx.	.
</s>
<s>
Mangy	mango	k1gNnPc7	mango
a	a	k8xC	a
anime	animat	k5eAaPmIp3nS	animat
tohoto	tento	k3xDgNnSc2	tento
zaměření	zaměření	k1gNnSc2	zaměření
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
romantické	romantický	k2eAgInPc1d1	romantický
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
obohacené	obohacený	k2eAgInPc1d1	obohacený
o	o	k7c4	o
milostný	milostný	k2eAgInSc4d1	milostný
mnohoúhelník	mnohoúhelník	k1gInSc4	mnohoúhelník
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Fušigi	Fušigi	k1gNnSc1	Fušigi
júgi	júg	k1gFnSc2	júg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
dospívající	dospívající	k2eAgFnSc1d1	dospívající
dívka	dívka	k1gFnSc1	dívka
čelící	čelící	k2eAgFnSc1d1	čelící
nejrůznějším	různý	k2eAgInPc3d3	nejrůznější
konfliktům	konflikt	k1gInPc3	konflikt
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
každodenní	každodenní	k2eAgNnSc4d1	každodenní
dilemata	dilema	k1gNnPc4	dilema
<g/>
,	,	kIx,	,
romantické	romantický	k2eAgFnPc4d1	romantická
pletky	pletka	k1gFnPc4	pletka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
jen	jen	k9	jen
snaží	snažit	k5eAaImIp3nS	snažit
zachránit	zachránit	k5eAaPmF	zachránit
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Šódžo	Šódžo	k6eAd1	Šódžo
nepostrádá	postrádat	k5eNaImIp3nS	postrádat
akci	akce	k1gFnSc4	akce
a	a	k8xC	a
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c4	na
tělesný	tělesný	k2eAgInSc4d1	tělesný
vývoj	vývoj	k1gInSc4	vývoj
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Ao	Ao	k?	Ao
haru	haru	k5eAaPmIp1nS	haru
Ride	Ride	k1gFnSc1	Ride
Bokura	Bokura	k1gFnSc1	Bokura
ga	ga	k?	ga
ita	ita	k?	ita
Card	Card	k1gInSc1	Card
Captor	Captor	k1gMnSc1	Captor
Sakura	sakura	k1gFnSc1	sakura
Fruits	Fruits	k1gInSc4	Fruits
Basket	basket	k1gInSc1	basket
Full	Full	k1gInSc1	Full
Moon	Moon	k1gInSc1	Moon
o	o	k7c6	o
sagašite	sagašit	k1gInSc5	sagašit
Med	med	k1gInSc1	med
a	a	k8xC	a
čtyřlístek	čtyřlístek	k1gInSc1	čtyřlístek
Kaičó	Kaičó	k1gMnSc2	Kaičó
wa	wa	k?	wa
Maid-sama	Maidam	k1gMnSc2	Maid-sam
Kaleido	Kaleida	k1gFnSc5	Kaleida
Star	star	k1gFnSc3	star
Kareši	Kareš	k1gMnPc7	Kareš
kanodžo	kanodžo	k6eAd1	kanodžo
no	no	k9	no
džidžó	džidžó	k?	džidžó
Kimi	Kimi	k1gNnSc1	Kimi
no	no	k9	no
todoke	todokat	k5eAaPmIp3nS	todokat
Maria-sama	Mariaama	k1gFnSc1	Maria-sama
ga	ga	k?	ga
miteru	mitrat	k5eAaPmIp1nS	mitrat
Nana	Nana	k1gFnSc1	Nana
Ouran	Ourana	k1gFnPc2	Ourana
High	Higha	k1gFnPc2	Higha
School	School	k1gInSc1	School
Host	host	k1gMnSc1	host
Club	club	k1gInSc1	club
Sailor	Sailor	k1gMnSc1	Sailor
Moon	Moon	k1gMnSc1	Moon
Vampire	Vampir	k1gInSc5	Vampir
Knight	Knight	k2eAgInSc1d1	Knight
</s>
