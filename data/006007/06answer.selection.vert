<s>
Šódžo	Šódžo	k1gNnSc1	Šódžo
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
少	少	k?	少
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
shō	shō	k?	shō
či	či	k8xC	či
shoujo	shoujo	k1gNnSc1	shoujo
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
dívka	dívka	k1gFnSc1	dívka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonské	japonský	k2eAgNnSc1d1	Japonské
slovo	slovo	k1gNnSc1	slovo
užívané	užívaný	k2eAgNnSc1d1	užívané
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
mangy	mango	k1gNnPc7	mango
nebo	nebo	k8xC	nebo
anime	animat	k5eAaPmIp3nS	animat
určeného	určený	k2eAgInSc2d1	určený
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
dívky	dívka	k1gFnPc4	dívka
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
11-19	[number]	k4	11-19
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
šónen	šónna	k1gFnPc2	šónna
zaměřeného	zaměřený	k2eAgMnSc2d1	zaměřený
na	na	k7c4	na
chlapce	chlapec	k1gMnSc4	chlapec
<g/>
.	.	kIx.	.
</s>
