<p>
<s>
Golet	golet	k1gInSc1	golet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
je	být	k5eAaImIp3nS	být
povídkový	povídkový	k2eAgInSc1d1	povídkový
soubor	soubor	k1gInSc1	soubor
českého	český	k2eAgMnSc2d1	český
spisovatele	spisovatel	k1gMnSc2	spisovatel
Ivana	Ivan	k1gMnSc2	Ivan
Olbrachta	Olbracht	k1gMnSc2	Olbracht
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
povídek	povídka	k1gFnPc2	povídka
(	(	kIx(	(
<g/>
Zázrak	zázrak	k1gInSc1	zázrak
s	s	k7c7	s
Julčou	Julča	k1gFnSc7	Julča
<g/>
,	,	kIx,	,
Událost	událost	k1gFnSc1	událost
v	v	k7c4	v
mikve	mikev	k1gFnPc4	mikev
a	a	k8xC	a
O	o	k7c6	o
smutných	smutný	k2eAgNnPc6d1	smutné
očích	oko	k1gNnPc6	oko
Hany	Hana	k1gFnSc2	Hana
Karadžičové	Karadžičové	k2eAgInPc4d1	Karadžičové
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
židovské	židovský	k2eAgFnSc6d1	židovská
vesnici	vesnice	k1gFnSc6	vesnice
Polana	polana	k1gFnSc1	polana
na	na	k7c6	na
Podkarpatské	podkarpatský	k2eAgFnSc6d1	Podkarpatská
Rusi	Rus	k1gFnSc6	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
golet	golet	k1gInSc1	golet
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
galut	galut	k1gInSc1	galut
<g/>
,	,	kIx,	,
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ג	ג	k?	ג
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
exil	exil	k1gInSc4	exil
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
použito	použít	k5eAaPmNgNnS	použít
coby	coby	k?	coby
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
zmíněnou	zmíněný	k2eAgFnSc4d1	zmíněná
vesnici	vesnice	k1gFnSc4	vesnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Film	film	k1gInSc1	film
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
natočil	natočit	k5eAaBmAgMnS	natočit
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
této	tento	k3xDgFnSc2	tento
knihy	kniha	k1gFnSc2	kniha
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
film	film	k1gInSc4	film
režisér	režisér	k1gMnSc1	režisér
Zeno	Zeno	k1gMnSc1	Zeno
Dostál	Dostál	k1gMnSc1	Dostál
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
sehrál	sehrát	k5eAaPmAgMnS	sehrát
herec	herec	k1gMnSc1	herec
Ondřej	Ondřej	k1gMnSc1	Ondřej
Vetchý	vetchý	k2eAgMnSc1d1	vetchý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Golet	golet	k1gInSc1	golet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Golet	golet	k1gInSc1	golet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
