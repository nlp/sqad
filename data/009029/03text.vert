<p>
<s>
Transplantace	transplantace	k1gFnSc1	transplantace
srdce	srdce	k1gNnSc2	srdce
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
heart	heart	k1gInSc1	heart
exchange	exchang	k1gFnSc2	exchang
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
HTX	HTX	kA	HTX
<g/>
)	)	kIx)	)
je	on	k3xPp3gFnPc4	on
transplantace	transplantace	k1gFnPc4	transplantace
biologicky	biologicky	k6eAd1	biologicky
aktivního	aktivní	k2eAgNnSc2d1	aktivní
srdce	srdce	k1gNnSc2	srdce
dárce	dárce	k1gMnSc2	dárce
(	(	kIx(	(
<g/>
prohlášeného	prohlášený	k2eAgMnSc2d1	prohlášený
za	za	k7c4	za
mrtvého	mrtvý	k1gMnSc4	mrtvý
pro	pro	k7c4	pro
mozkovou	mozkový	k2eAgFnSc4d1	mozková
inaktivitu	inaktivita	k1gFnSc4	inaktivita
<g/>
)	)	kIx)	)
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
příjemce	příjemce	k1gMnSc2	příjemce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
experimentální	experimentální	k2eAgFnSc1d1	experimentální
transplantace	transplantace	k1gFnSc1	transplantace
srdce	srdce	k1gNnSc2	srdce
na	na	k7c6	na
psovi	pes	k1gMnSc6	pes
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgNnP	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc4	první
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
transplantaci	transplantace	k1gFnSc4	transplantace
na	na	k7c6	na
lidech	lid	k1gInPc6	lid
provedl	provést	k5eAaPmAgInS	provést
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1967	[number]	k4	1967
v	v	k7c6	v
Kapském	kapský	k2eAgNnSc6d1	Kapské
Městě	město	k1gNnSc6	město
chirurg	chirurg	k1gMnSc1	chirurg
Christiaan	Christiaan	k1gMnSc1	Christiaan
Barnard	Barnard	k1gMnSc1	Barnard
<g/>
.	.	kIx.	.
</s>
<s>
Pacient	pacient	k1gMnSc1	pacient
Washkansky	Washkansky	k1gMnSc1	Washkansky
zemřel	zemřít	k5eAaPmAgMnS	zemřít
po	po	k7c6	po
18	[number]	k4	18
dnech	den	k1gInPc6	den
na	na	k7c4	na
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
pět	pět	k4xCc4	pět
dnů	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
transplantoval	transplantovat	k5eAaBmAgMnS	transplantovat
chirurg	chirurg	k1gMnSc1	chirurg
Adrian	Adrian	k1gMnSc1	Adrian
Kantowitz	Kantowitz	k1gMnSc1	Kantowitz
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
v	v	k7c6	v
Brooklyne	Brooklyn	k1gInSc5	Brooklyn
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
dětské	dětský	k2eAgNnSc1d1	dětské
srdce	srdce	k1gNnSc1	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Pacient	pacient	k1gMnSc1	pacient
ale	ale	k8xC	ale
zemřel	zemřít	k5eAaPmAgMnS	zemřít
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
operaci	operace	k1gFnSc6	operace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
1967	[number]	k4	1967
až	až	k9	až
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
1970	[number]	k4	1970
bylo	být	k5eAaImAgNnS	být
transplantovaných	transplantovaný	k2eAgMnPc2d1	transplantovaný
celkově	celkově	k6eAd1	celkově
164	[number]	k4	164
srdcí	srdce	k1gNnPc2	srdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Transplantace	transplantace	k1gFnSc1	transplantace
srdce	srdce	k1gNnSc2	srdce
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
transplantace	transplantace	k1gFnSc1	transplantace
srdce	srdce	k1gNnSc2	srdce
v	v	k7c6	v
bývalém	bývalý	k2eAgNnSc6d1	bývalé
Československu	Československo	k1gNnSc6	Československo
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
II	II	kA	II
<g/>
.	.	kIx.	.
chirurgické	chirurgický	k2eAgFnSc3d1	chirurgická
klinice	klinika	k1gFnSc3	klinika
Lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Komenského	Komenský	k1gMnSc2	Komenský
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
na	na	k7c6	na
Partizánské	partizánský	k2eAgFnSc6d1	partizánská
ulici	ulice	k1gFnSc6	ulice
ji	on	k3xPp3gFnSc4	on
vedl	vést	k5eAaImAgMnS	vést
akademik	akademik	k1gMnSc1	akademik
MUDr.	MUDr.	kA	MUDr.
Karol	Karol	k1gInSc1	Karol
Šiška	šiška	k1gFnSc1	šiška
a	a	k8xC	a
MUDr.	MUDr.	kA	MUDr.
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kužela	Kužel	k1gMnSc2	Kužel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
transplantaci	transplantace	k1gFnSc4	transplantace
srdce	srdce	k1gNnSc2	srdce
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
MUDr.	MUDr.	kA	MUDr.
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kužela	Kužel	k1gMnSc2	Kužel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
zkušenosti	zkušenost	k1gFnPc4	zkušenost
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Operační	operační	k2eAgFnSc4d1	operační
skupinu	skupina	k1gFnSc4	skupina
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
tvořili	tvořit	k5eAaImAgMnP	tvořit
<g/>
:	:	kIx,	:
akademik	akademik	k1gMnSc1	akademik
prof.	prof.	kA	prof.
Karol	Karol	k1gInSc1	Karol
Šiška	šiška	k1gFnSc1	šiška
<g/>
,	,	kIx,	,
MUDr.	MUDr.	kA	MUDr.
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kužela	Kužel	k1gMnSc2	Kužel
<g/>
,	,	kIx,	,
MUDr.	MUDr.	kA	MUDr.
Anna	Anna	k1gFnSc1	Anna
Pivková	Pivková	k1gFnSc1	Pivková
<g/>
,	,	kIx,	,
MUDr.	MUDr.	kA	MUDr.
Ján	Ján	k1gMnSc1	Ján
Tréger	Tréger	k1gMnSc1	Tréger
<g/>
,	,	kIx,	,
MUDr.	MUDr.	kA	MUDr.
Marian	Mariana	k1gFnPc2	Mariana
Holomáň	Holomáň	k1gFnSc1	Holomáň
<g/>
;	;	kIx,	;
odběr	odběr	k1gInSc1	odběr
srdce	srdce	k1gNnSc1	srdce
dárce	dárce	k1gMnSc2	dárce
<g/>
:	:	kIx,	:
MUDr.	MUDr.	kA	MUDr.
Milan	Milan	k1gMnSc1	Milan
Schnorrer	Schnorrer	k1gMnSc1	Schnorrer
<g/>
,	,	kIx,	,
MUDr.	MUDr.	kA	MUDr.
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kužela	Kužel	k1gMnSc2	Kužel
<g/>
,	,	kIx,	,
MUDr.	MUDr.	kA	MUDr.
Marian	Mariana	k1gFnPc2	Mariana
Holomáň	Holomáň	k1gFnSc1	Holomáň
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
umělý	umělý	k2eAgInSc4d1	umělý
krevní	krevní	k2eAgInSc4d1	krevní
oběh	oběh	k1gInSc4	oběh
(	(	kIx(	(
<g/>
ECC	ECC	kA	ECC
z	z	k7c2	z
angl.	angl.	k?	angl.
extracorporeal	extracorporeal	k1gInSc1	extracorporeal
circulation	circulation	k1gInSc1	circulation
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
MUDr.	MUDr.	kA	MUDr.
Jaromír	Jaromír	k1gMnSc1	Jaromír
Horecký	Horecký	k2eAgMnSc1d1	Horecký
<g/>
,	,	kIx,	,
MUDr.	MUDr.	kA	MUDr.
Stanislav	Stanislav	k1gMnSc1	Stanislav
Čársky	Čárska	k1gFnSc2	Čárska
<g/>
,	,	kIx,	,
Pavol	Pavol	k1gInSc4	Pavol
Podolay	Podolaa	k1gFnSc2	Podolaa
<g/>
;	;	kIx,	;
anestézie	anestézie	k1gFnSc1	anestézie
<g/>
:	:	kIx,	:
MUDr.	MUDr.	kA	MUDr.
Ivo	Ivo	k1gMnSc1	Ivo
Soběský	Soběský	k1gMnSc1	Soběský
<g/>
,	,	kIx,	,
MUDr.	MUDr.	kA	MUDr.
Viera	Viera	k1gFnSc1	Viera
Neumarová	Neumarová	k1gFnSc1	Neumarová
<g/>
;	;	kIx,	;
instrumentářky	instrumentářka	k1gFnPc1	instrumentářka
<g/>
:	:	kIx,	:
Márie	Márie	k1gFnSc1	Márie
Machková	Machková	k1gFnSc1	Machková
<g/>
,	,	kIx,	,
Mária	Mária	k1gFnSc1	Mária
Homerová	Homerová	k1gFnSc1	Homerová
<g/>
,	,	kIx,	,
Jožka	Jožka	k1gFnSc1	Jožka
Králová	Králová	k1gFnSc1	Králová
a	a	k8xC	a
laborantka	laborantka	k1gFnSc1	laborantka
Etka	Etka	k1gFnSc1	Etka
Malinová	Malinová	k1gFnSc1	Malinová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
první	první	k4xOgFnSc6	první
transplantaci	transplantace	k1gFnSc6	transplantace
srdce	srdce	k1gNnSc2	srdce
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Transplantaci	transplantace	k1gFnSc4	transplantace
předcházela	předcházet	k5eAaImAgFnS	předcházet
experimentální	experimentální	k2eAgFnSc1d1	experimentální
a	a	k8xC	a
klinická	klinický	k2eAgFnSc1d1	klinická
příprava	příprava	k1gFnSc1	příprava
<g/>
.	.	kIx.	.
</s>
<s>
Dárcem	dárce	k1gMnSc7	dárce
srdce	srdce	k1gNnSc2	srdce
byl	být	k5eAaImAgInS	být
46	[number]	k4	46
<g/>
letý	letý	k2eAgMnSc1d1	letý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
smrtelné	smrtelný	k2eAgNnSc4d1	smrtelné
zranění	zranění	k1gNnSc4	zranění
mozku	mozek	k1gInSc2	mozek
při	při	k7c6	při
pádu	pád	k1gInSc6	pád
z	z	k7c2	z
balkónu	balkón	k1gInSc2	balkón
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
s	s	k7c7	s
odběrem	odběr	k1gInSc7	odběr
srdce	srdce	k1gNnSc2	srdce
až	až	k6eAd1	až
po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
přesvědčování	přesvědčování	k1gNnSc6	přesvědčování
farářem	farář	k1gMnSc7	farář
<g/>
.	.	kIx.	.
</s>
<s>
Příjemcem	příjemce	k1gMnSc7	příjemce
byla	být	k5eAaImAgNnP	být
54	[number]	k4	54
<g/>
letá	letý	k2eAgFnSc1d1	letá
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Transplantaci	transplantace	k1gFnSc3	transplantace
však	však	k9	však
nepřežila	přežít	k5eNaPmAgFnS	přežít
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
zemřela	zemřít	k5eAaPmAgFnS	zemřít
po	po	k7c6	po
pěti	pět	k4xCc6	pět
hodinách	hodina	k1gFnPc6	hodina
ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
operačním	operační	k2eAgInSc6d1	operační
sále	sál	k1gInSc6	sál
na	na	k7c4	na
následky	následek	k1gInPc4	následek
jiných	jiný	k2eAgFnPc2d1	jiná
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Medicínské	medicínský	k2eAgInPc4d1	medicínský
aspekty	aspekt	k1gInPc4	aspekt
==	==	k?	==
</s>
</p>
<p>
<s>
Transplantace	transplantace	k1gFnSc1	transplantace
srdce	srdce	k1gNnSc2	srdce
je	být	k5eAaImIp3nS	být
hluboký	hluboký	k2eAgInSc1d1	hluboký
zásah	zásah	k1gInSc1	zásah
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
spojený	spojený	k2eAgMnSc1d1	spojený
s	s	k7c7	s
riziky	riziko	k1gNnPc7	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
potlačení	potlačení	k1gNnSc6	potlačení
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
hrozí	hrozit	k5eAaImIp3nS	hrozit
totiž	totiž	k9	totiž
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
zápalů	zápal	k1gInPc2	zápal
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
odvrácení	odvrácení	k1gNnSc4	odvrácení
odpuzení	odpuzení	k1gNnSc4	odpuzení
cizího	cizí	k2eAgInSc2d1	cizí
organismu	organismus	k1gInSc2	organismus
u	u	k7c2	u
příjemce	příjemce	k1gMnSc2	příjemce
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
potlačovat	potlačovat	k5eAaImF	potlačovat
imunitní	imunitní	k2eAgFnSc4d1	imunitní
reakci	reakce	k1gFnSc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Terapie	terapie	k1gFnSc1	terapie
se	se	k3xPyFc4	se
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
kombinací	kombinace	k1gFnSc7	kombinace
různých	různý	k2eAgInPc2d1	různý
léků	lék	k1gInPc2	lék
(	(	kIx(	(
<g/>
steroidy	steroid	k1gInPc1	steroid
<g/>
,	,	kIx,	,
azathiorpin	azathiorpin	k1gInSc1	azathiorpin
<g/>
,	,	kIx,	,
cyclosporin	cyclosporin	k1gInSc1	cyclosporin
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pětileté	pětiletý	k2eAgNnSc4d1	pětileté
přežití	přežití	k1gNnSc4	přežití
u	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
s	s	k7c7	s
transplantovaným	transplantovaný	k2eAgNnSc7d1	transplantované
srdcem	srdce	k1gNnSc7	srdce
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
procentech	procento	k1gNnPc6	procento
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
transplantace	transplantace	k1gFnSc2	transplantace
srdce	srdce	k1gNnSc2	srdce
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
z	z	k7c2	z
první	první	k4xOgFnSc2	první
transplantace	transplantace	k1gFnSc2	transplantace
srdce	srdce	k1gNnSc2	srdce
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
</s>
</p>
<p>
<s>
múzeum	múzeum	k1gNnSc1	múzeum
transplantace	transplantace	k1gFnSc2	transplantace
srdce	srdce	k1gNnSc2	srdce
-	-	kIx~	-
Kapské	kapský	k2eAgNnSc1d1	Kapské
město	město	k1gNnSc1	město
</s>
</p>
<p>
<s>
fotografie	fotografia	k1gFnPc1	fotografia
první	první	k4xOgFnSc2	první
transplantace	transplantace	k1gFnSc2	transplantace
srdce	srdce	k1gNnSc2	srdce
v	v	k7c6	v
USA	USA	kA	USA
</s>
</p>
