<s>
Roku	rok	k1gInSc2
1992	#num#	k4
objevil	objevit	k5eAaPmAgMnS
bílkovinu	bílkovina	k1gFnSc4
PD-	PD-1	kA
<g/>
1	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
funguje	fungovat	k5eAaImIp3nS
jako	jako	k9
brzda	brzda	k1gFnSc1
imunitního	imunitní	k2eAgInSc2d1
systému	systém	k1gInSc2
(	(	kIx(
<g/>
Allison	Allison	k1gInSc1
objevil	objevit	k5eAaPmAgInS
jinou	jiný	k2eAgFnSc4d1
takovou	takový	k3xDgFnSc4
brzdu	brzda	k1gFnSc4
<g/>
,	,	kIx,
protein	protein	k1gInSc4
CTLA-	CTLA-	k1gFnPc2
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>