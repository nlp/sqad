<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaImIp3nS,k5eAaBmIp3nS
protein	protein	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
objevil	objevit	k5eAaPmAgMnS
Tasuku	Tasuk	k1gMnSc3
Hondžo	Hondžo	k1gMnSc1
a	a	k8xC
dostal	dostat	k5eAaPmAgMnS
za	za	k7c4
něj	on	k3xPp3gMnSc4
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
?	?	kIx.
</s>