<s>
Tasuku	Tasuk	k1gMnSc3
Hondžó	Hondžó	k1gMnSc3
</s>
<s>
Tasuku	Tasuk	k1gMnSc5
Hondžo	Hondža	k1gMnSc5
Tasuku	Tasuk	k1gMnSc5
Hondžo	Hondža	k1gMnSc5
(	(	kIx(
<g/>
listopad	listopad	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
Narození	narození	k1gNnSc2
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1942	#num#	k4
(	(	kIx(
<g/>
79	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Kjóto	Kjóto	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Kjótská	Kjótský	k2eAgFnSc1d1
univerzitaJamagučiská	univerzitaJamagučiský	k2eAgFnSc1d1
prefekturní	prefekturní	k2eAgFnSc1d1
střední	střední	k2eAgFnSc1d1
škola	škola	k1gFnSc1
v	v	k7c6
Ube	Ube	k1gFnSc6
Povolání	povolání	k1gNnSc2
</s>
<s>
imunolog	imunolog	k1gMnSc1
<g/>
,	,	kIx,
biochemik	biochemik	k1gMnSc1
<g/>
,	,	kIx,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
a	a	k8xC
lékař	lékař	k1gMnSc1
Zaměstnavatelé	zaměstnavatel	k1gMnPc1
</s>
<s>
Tokijská	tokijský	k2eAgFnSc1d1
univerzitaÓsacká	univerzitaÓsacký	k2eAgFnSc1d1
univerzitaKjótská	univerzitaKjótský	k2eAgFnSc1d1
univerzitaHirosacká	univerzitaHirosacký	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
Asahi	Asah	k1gFnSc2
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
<g/>
Imperiální	imperiální	k2eAgFnSc1d1
cena	cena	k1gFnSc1
Japonské	japonský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
Bunka	Bunka	k1gMnSc1
Koroša	Koroša	k1gMnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
Cena	cena	k1gFnSc1
Roberta	Robert	k1gMnSc2
Kocha	Koch	k1gMnSc2
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
Řád	řád	k1gInSc1
kultury	kultura	k1gFnSc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Hidemasa	Hidemasa	k1gFnSc1
Nagata	Nagata	k1gFnSc1
(	(	kIx(
<g/>
švagrovství	švagrovství	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tasuku	Tasuk	k1gMnSc3
Hondžó	Hondžó	k1gMnSc3
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
本	本	k?
佑	佑	k?
<g/>
;	;	kIx,
*	*	kIx~
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1942	#num#	k4
<g/>
,	,	kIx,
Kjóto	Kjóto	k1gNnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
japonský	japonský	k2eAgMnSc1d1
imunolog	imunolog	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
získal	získat	k5eAaPmAgMnS
Nobelovu	Nobelův	k2eAgFnSc4d1
cena	cena	k1gFnSc1
za	za	k7c4
fyziologii	fyziologie	k1gFnSc4
a	a	k8xC
lékařství	lékařství	k1gNnSc4
za	za	k7c4
své	svůj	k3xOyFgInPc4
revoluční	revoluční	k2eAgInPc4d1
přístupy	přístup	k1gInPc4
k	k	k7c3
boji	boj	k1gInSc3
s	s	k7c7
rakovinou	rakovina	k1gFnSc7
za	za	k7c2
pomoci	pomoc	k1gFnSc2
imunitního	imunitní	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cenu	cena	k1gFnSc4
sdílel	sdílet	k5eAaImAgMnS
s	s	k7c7
Američanem	Američan	k1gMnSc7
James	James	k1gMnSc1
P.	P.	kA
Allisonem	Allison	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
1992	#num#	k4
objevil	objevit	k5eAaPmAgMnS
bílkovinu	bílkovina	k1gFnSc4
PD-	PD-1	kA
<g/>
1	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
funguje	fungovat	k5eAaImIp3nS
jako	jako	k9
brzda	brzda	k1gFnSc1
imunitního	imunitní	k2eAgInSc2d1
systému	systém	k1gInSc2
(	(	kIx(
<g/>
Allison	Allison	k1gInSc1
objevil	objevit	k5eAaPmAgInS
jinou	jiný	k2eAgFnSc4d1
takovou	takový	k3xDgFnSc4
brzdu	brzda	k1gFnSc4
<g/>
,	,	kIx,
protein	protein	k1gInSc4
CTLA-	CTLA-	k1gFnPc2
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
tyto	tento	k3xDgFnPc1
brzdy	brzda	k1gFnPc1
podaří	podařit	k5eAaPmIp3nP
zablokovat	zablokovat	k5eAaPmF
<g/>
,	,	kIx,
imunitní	imunitní	k2eAgInSc1d1
systém	systém	k1gInSc1
napadne	napadnout	k5eAaPmIp3nS
rakovinné	rakovinný	k2eAgFnPc4d1
buňky	buňka	k1gFnPc4
s	s	k7c7
mnohem	mnohem	k6eAd1
větší	veliký	k2eAgFnSc7d2
silou	síla	k1gFnSc7
než	než	k8xS
obvykle	obvykle	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Terapie	terapie	k1gFnSc1
za	za	k7c2
pomoci	pomoc	k1gFnSc2
tohoto	tento	k3xDgNnSc2
zablokování	zablokování	k1gNnSc2
je	být	k5eAaImIp3nS
mimořádně	mimořádně	k6eAd1
účinná	účinný	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
ve	v	k7c6
stadiích	stadion	k1gNnPc6
metastází	metastáze	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS
lékařství	lékařství	k1gNnSc1
na	na	k7c6
Kjótské	Kjótský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absolvoval	absolvovat	k5eAaPmAgMnS
roku	rok	k1gInSc2
1966	#num#	k4
<g/>
,	,	kIx,
doktorát	doktorát	k1gInSc1
získal	získat	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
učil	učít	k5eAaPmAgMnS,k5eAaImAgMnS
na	na	k7c6
Tokijské	tokijský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1979	#num#	k4
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Ósace	Ósaka	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
profesorem	profesor	k1gMnSc7
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1984	#num#	k4
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
Kjótskou	Kjótský	k2eAgFnSc4d1
univerzitu	univerzita	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
učinil	učinit	k5eAaPmAgInS,k5eAaImAgInS
své	svůj	k3xOyFgInPc4
hlavní	hlavní	k2eAgInPc4d1
objevy	objev	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hondžó	Hondžó	k?
je	být	k5eAaImIp3nS
členem	člen	k1gMnSc7
Japonské	japonský	k2eAgFnSc2d1
imunologické	imunologický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
a	a	k8xC
v	v	k7c6
letech	léto	k1gNnPc6
1999	#num#	k4
až	až	k9
2000	#num#	k4
působil	působit	k5eAaImAgInS
jako	jako	k8xS,k8xC
její	její	k3xOp3gMnSc1
prezident	prezident	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získal	získat	k5eAaPmAgMnS
rovněž	rovněž	k9
čestné	čestný	k2eAgNnSc4d1
členství	členství	k1gNnSc4
v	v	k7c6
Americké	americký	k2eAgFnSc6d1
asociaci	asociace	k1gFnSc6
imunologů	imunolog	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
2001	#num#	k4
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
zahraničním	zahraniční	k2eAgMnSc7d1
spolupracovníkem	spolupracovník	k1gMnSc7
Národní	národní	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
USA	USA	kA
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
členem	člen	k1gMnSc7
Německé	německý	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
přírodních	přírodní	k2eAgFnPc2d1
věd	věda	k1gFnPc2
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
působí	působit	k5eAaImIp3nS
jako	jako	k9
člen	člen	k1gMnSc1
Japonské	japonský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
zástupcem	zástupce	k1gMnSc7
generálního	generální	k2eAgMnSc2d1
ředitele	ředitel	k1gMnSc2
a	a	k8xC
profesorem	profesor	k1gMnSc7
Kjótského	Kjótský	k2eAgInSc2d1
univerzitního	univerzitní	k2eAgInSc2d1
institutu	institut	k1gInSc2
pro	pro	k7c4
pokročilé	pokročilý	k2eAgNnSc4d1
studium	studium	k1gNnSc4
(	(	kIx(
<g/>
KUIAS	KUIAS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Tasuku	Tasuk	k1gInSc2
Honjo	Honjo	k6eAd1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tasuku	Tasuk	k1gInSc2
Honjo	Honjo	k1gMnSc1
–	–	k?
Facts	Facts	k1gInSc1
–	–	k?
2018	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nobel	Nobel	k1gMnSc1
Media	medium	k1gNnSc2
AB	AB	kA
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HANNAH	HANNAH	kA
<g/>
,	,	kIx,
Devlin	Devlin	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
James	James	k1gInSc1
P	P	kA
Allison	Allison	k1gInSc1
and	and	k?
Tasuku	Tasuk	k1gInSc2
Honjo	Honjo	k1gMnSc1
win	win	k?
Nobel	Nobel	k1gMnSc1
prize	prize	k1gFnSc2
for	forum	k1gNnPc2
medicine	medicinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
za	za	k7c4
lékařství	lékařství	k1gNnSc4
získala	získat	k5eAaPmAgFnS
dvojice	dvojice	k1gFnSc1
vědců	vědec	k1gMnPc2
za	za	k7c4
revoluční	revoluční	k2eAgInSc4d1
postup	postup	k1gInSc4
v	v	k7c6
boji	boj	k1gInSc6
s	s	k7c7
rakovinou	rakovina	k1gFnSc7
-	-	kIx~
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
<g/>
.	.	kIx.
www.seznamzpravy.cz	www.seznamzpravy.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Tasuku	Tasuk	k1gInSc2
Honjo	Honjo	k6eAd1
|	|	kIx~
Biography	Biographa	k1gFnPc1
&	&	k?
PD-	PD-	k1gFnSc7
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopedia	Encyclopedium	k1gNnSc2
Britannica	Britannicum	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
AAI	AAI	kA
Members	Members	k1gInSc1
Awarded	Awarded	k1gInSc1
the	the	k?
2018	#num#	k4
Nobel	Nobel	k1gMnSc1
Prizein	Prizein	k1gMnSc1
Physiology	Physiolog	k1gMnPc4
or	or	k?
Medicine	Medicin	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
American	Americana	k1gFnPc2
Association	Association	k1gInSc4
of	of	k?
Immunologists	Immunologists	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Tasuku	Tasuk	k1gMnSc5
Honjo	Honja	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inamori	Inamori	k1gNnSc1
Foundation	Foundation	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Tasuku	Tasuk	k1gInSc2
Hondžo	Hondžo	k6eAd1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Nositelé	nositel	k1gMnPc1
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
fyziologii	fyziologie	k1gFnSc4
nebo	nebo	k8xC
lékařství	lékařství	k1gNnSc4
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
–	–	k?
<g/>
2025	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Leland	Leland	k1gInSc1
H.	H.	kA
Hartwell	Hartwell	k1gInSc1
/	/	kIx~
Tim	Tim	k?
Hunt	hunt	k1gInSc1
/	/	kIx~
Paul	Paul	k1gMnSc1
Nurse	Nurs	k1gMnSc2
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Sydney	Sydney	k1gNnSc1
Brenner	Brenner	k1gMnSc1
/	/	kIx~
H.	H.	kA
Robert	Robert	k1gMnSc1
Horvitz	Horvitz	k1gMnSc1
/	/	kIx~
John	John	k1gMnSc1
Sulston	Sulston	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Paul	Paul	k1gMnSc1
Lauterbur	Lauterbur	k1gMnSc1
/	/	kIx~
Peter	Peter	k1gMnSc1
Mansfield	Mansfield	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Richard	Richard	k1gMnSc1
Axel	Axel	k1gMnSc1
/	/	kIx~
Linda	Linda	k1gFnSc1
B.	B.	kA
Bucková	Bucková	k1gFnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Barry	Barr	k1gInPc1
Marshall	Marshalla	k1gFnPc2
/	/	kIx~
Robin	Robina	k1gFnPc2
Warren	Warrna	k1gFnPc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Andrew	Andrew	k?
Z.	Z.	kA
Fire	Fir	k1gFnSc2
/	/	kIx~
Craig	Craig	k1gInSc4
C.	C.	kA
Mello	Mello	k1gNnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Mario	Mario	k1gMnSc1
Capecchi	Capecchi	k1gNnSc2
/	/	kIx~
Martin	Martin	k2eAgInSc1d1
Evans	Evans	k1gInSc1
/	/	kIx~
Oliver	Oliver	k1gMnSc1
Smithies	Smithies	k1gMnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Harald	Harald	k1gMnSc1
zur	zur	k?
Hausen	Hausen	k1gInSc1
/	/	kIx~
Françoise	Françoise	k1gFnSc2
Barré-Sinoussi	Barré-Sinousse	k1gFnSc4
/	/	kIx~
Luc	Luc	k1gMnSc1
Montagnier	Montagnier	k1gMnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Elizabeth	Elizabeth	k1gFnSc1
Blackburnová	Blackburnová	k1gFnSc1
/	/	kIx~
Carol	Carol	k1gInSc1
W.	W.	kA
Greiderová	Greiderová	k1gFnSc1
/	/	kIx~
Jack	Jack	k1gMnSc1
W.	W.	kA
Szostak	Szostak	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Robert	Robert	k1gMnSc1
G.	G.	kA
Edwards	Edwards	k1gInSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Bruce	Bruce	k1gMnSc1
Beutler	Beutler	k1gMnSc1
/	/	kIx~
Jules	Jules	k1gMnSc1
A.	A.	kA
Hoffmann	Hoffmann	k1gMnSc1
/	/	kIx~
Ralph	Ralph	k1gMnSc1
M.	M.	kA
Steinman	Steinman	k1gMnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
John	John	k1gMnSc1
Gurdon	Gurdon	k1gMnSc1
/	/	kIx~
Šin	Šin	k1gMnSc1
<g/>
'	'	kIx"
<g/>
ja	ja	k?
Jamanaka	Jamanak	k1gMnSc2
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
James	James	k1gMnSc1
Rothman	Rothman	k1gMnSc1
/	/	kIx~
Randy	rand	k1gInPc1
Schekman	Schekman	k1gMnSc1
/	/	kIx~
Thomas	Thomas	k1gMnSc1
Südhof	Südhof	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
John	John	k1gMnSc1
O	O	kA
<g/>
'	'	kIx"
<g/>
Keefe	Keef	k1gInSc5
/	/	kIx~
May-Britt	May-Britt	k1gInSc1
Moserová	Moserová	k1gFnSc1
/	/	kIx~
Edvard	Edvard	k1gMnSc1
Moser	Moser	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
William	William	k1gInSc1
C.	C.	kA
Campbell	Campbell	k1gInSc1
/	/	kIx~
Satoši	Satoš	k1gMnPc1
Ómura	Ómur	k1gInSc2
/	/	kIx~
Tchu	Tch	k2eAgFnSc4d1
Jou-jou	Jou-já	k1gFnSc4
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jošinori	Jošinori	k6eAd1
Ósumi	Ósu	k1gFnPc7
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jeffrey	Jeffrey	k1gInPc1
C.	C.	kA
Hall	Hall	k1gMnSc1
/	/	kIx~
Michael	Michael	k1gMnSc1
Rosbash	Rosbash	k1gMnSc1
/	/	kIx~
Michael	Michael	k1gMnSc1
W.	W.	kA
Young	Young	k1gMnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
James	James	k1gMnSc1
P.	P.	kA
Allison	Allison	k1gMnSc1
/	/	kIx~
Tasuku	Tasuk	k1gInSc2
Hondžó	Hondžó	k1gFnSc2
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Gregg	Gregg	k1gMnSc1
L.	L.	kA
Semenza	Semenza	k1gFnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
J.	J.	kA
Ratcliffe	Ratcliff	k1gInSc5
<g/>
,	,	kIx,
William	William	k1gInSc1
Kaelin	Kaelin	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1901	#num#	k4
<g/>
–	–	k?
<g/>
1925	#num#	k4
•	•	k?
1926	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
•	•	k?
1951	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
•	•	k?
1976	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
172142881	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0868	#num#	k4
9509	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nr	nr	k?
<g/>
89011342	#num#	k4
|	|	kIx~
ORCID	ORCID	kA
<g/>
:	:	kIx,
0000-0003-2300-3928	0000-0003-2300-3928	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
9923766	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nr	lccn-nr	k1gInSc1
<g/>
89011342	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Japonsko	Japonsko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Medicína	medicína	k1gFnSc1
</s>
