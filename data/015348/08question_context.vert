<s>
Tasuku	Tasuk	k1gMnSc3
Hondžó	Hondžó	k1gMnSc3
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
本	本	k?
佑	佑	k?
<g/>
;	;	kIx,
*	*	kIx~
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1942	#num#	k4
<g/>
,	,	kIx,
Kjóto	Kjóto	k1gNnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
japonský	japonský	k2eAgMnSc1d1
imunolog	imunolog	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
získal	získat	k5eAaPmAgMnS
Nobelovu	Nobelův	k2eAgFnSc4d1
cena	cena	k1gFnSc1
za	za	k7c4
fyziologii	fyziologie	k1gFnSc4
a	a	k8xC
lékařství	lékařství	k1gNnSc4
za	za	k7c4
své	svůj	k3xOyFgInPc4
revoluční	revoluční	k2eAgInPc4d1
přístupy	přístup	k1gInPc4
k	k	k7c3
boji	boj	k1gInSc3
s	s	k7c7
rakovinou	rakovina	k1gFnSc7
za	za	k7c2
pomoci	pomoc	k1gFnSc2
imunitního	imunitní	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cenu	cena	k1gFnSc4
sdílel	sdílet	k5eAaImAgMnS
s	s	k7c7
Američanem	Američan	k1gMnSc7
James	James	k1gMnSc1
P.	P.	kA
Allisonem	Allison	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>