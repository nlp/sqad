<p>
<s>
Křížová	křížový	k2eAgFnSc1d1	křížová
cesta	cesta	k1gFnSc1	cesta
v	v	k7c6	v
Andělské	andělský	k2eAgFnSc6d1	Andělská
Hoře	hora	k1gFnSc6	hora
na	na	k7c6	na
Bruntálsku	Bruntálsko	k1gNnSc6	Bruntálsko
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
obce	obec	k1gFnSc2	obec
na	na	k7c4	na
Anenský	anenský	k2eAgInSc4d1	anenský
vrch	vrch	k1gInSc4	vrch
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
cca	cca	kA	cca
1,5	[number]	k4	1,5
kilometru	kilometr	k1gInSc2	kilometr
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Křížová	křížový	k2eAgFnSc1d1	křížová
cesta	cesta	k1gFnSc1	cesta
na	na	k7c6	na
Anenském	anenský	k2eAgInSc6d1	anenský
vrchu	vrch	k1gInSc6	vrch
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
alejí	alej	k1gFnSc7	alej
k	k	k7c3	k
poutnímu	poutní	k2eAgInSc3d1	poutní
kostelu	kostel	k1gInSc3	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
čtrnáct	čtrnáct	k4xCc4	čtrnáct
dřevěných	dřevěný	k2eAgInPc2d1	dřevěný
křížů	kříž	k1gInPc2	kříž
s	s	k7c7	s
orámovanými	orámovaný	k2eAgInPc7d1	orámovaný
pašijovými	pašijový	k2eAgInPc7d1	pašijový
obrazy	obraz	k1gInPc7	obraz
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnPc1d1	původní
zastavení	zastavení	k1gNnPc1	zastavení
pocházela	pocházet	k5eAaImAgNnP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
</s>
<s>
Čtrnáct	čtrnáct	k4xCc4	čtrnáct
stojanů	stojan	k1gInPc2	stojan
doplněných	doplněný	k2eAgInPc2d1	doplněný
obrazy	obraz	k1gInPc4	obraz
od	od	k7c2	od
malíře	malíř	k1gMnSc2	malíř
Franze	Franze	k1gFnSc2	Franze
Templera	Templer	k1gMnSc2	Templer
se	se	k3xPyFc4	se
nedochovalo	dochovat	k5eNaPmAgNnS	dochovat
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc2	jejich
torza	torzo	k1gNnSc2	torzo
byla	být	k5eAaImAgFnS	být
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
založení	založení	k1gNnSc6	založení
nové	nový	k2eAgFnSc2d1	nová
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
umělecký	umělecký	k2eAgMnSc1d1	umělecký
řezbář	řezbář	k1gMnSc1	řezbář
František	František	k1gMnSc1	František
Nedomlel	domlít	k5eNaPmAgMnS	domlít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poutní	poutní	k2eAgInSc1d1	poutní
kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
návrší	návrší	k1gNnSc6	návrší
pod	pod	k7c7	pod
Anenským	anenský	k2eAgInSc7d1	anenský
vrchem	vrch	k1gInSc7	vrch
původně	původně	k6eAd1	původně
stála	stát	k5eAaImAgFnS	stát
kaple	kaple	k1gFnSc1	kaple
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
letech	let	k1gInPc6	let
1694	[number]	k4	1694
<g/>
–	–	k?	–
<g/>
1696	[number]	k4	1696
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
pojmout	pojmout	k5eAaPmF	pojmout
tak	tak	k6eAd1	tak
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
scházel	scházet	k5eAaImAgInS	scházet
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
kamenného	kamenný	k2eAgInSc2d1	kamenný
kostela	kostel	k1gInSc2	kostel
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
kopce	kopec	k1gInSc2	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
letech	let	k1gInPc6	let
1767	[number]	k4	1767
<g/>
–	–	k?	–
<g/>
1770	[number]	k4	1770
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1770	[number]	k4	1770
na	na	k7c4	na
svátek	svátek	k1gInSc4	svátek
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vedle	vedle	k7c2	vedle
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
kříž	kříž	k1gInSc1	kříž
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1772	[number]	k4	1772
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
ve	v	k7c6	v
velice	velice	k6eAd1	velice
špatném	špatný	k2eAgInSc6d1	špatný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
mnoho	mnoho	k6eAd1	mnoho
utrpěl	utrpět	k5eAaPmAgInS	utrpět
za	za	k7c4	za
válek	válek	k1gInSc4	válek
a	a	k8xC	a
vlivem	vliv	k1gInSc7	vliv
nepříznivého	příznivý	k2eNgNnSc2d1	nepříznivé
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
poutní	poutní	k2eAgInSc1d1	poutní
kostel	kostel	k1gInSc1	kostel
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
poutnímu	poutní	k2eAgNnSc3d1	poutní
místu	místo	k1gNnSc3	místo
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
obce	obec	k1gFnSc2	obec
lipová	lipový	k2eAgFnSc1d1	Lipová
alej	alej	k1gFnSc1	alej
<g/>
,	,	kIx,	,
doplněná	doplněný	k2eAgFnSc1d1	doplněná
dalšími	další	k2eAgInPc7d1	další
druhy	druh	k1gInPc7	druh
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Křížová	křížový	k2eAgFnSc1d1	křížová
cesta	cesta	k1gFnSc1	cesta
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
aleje	alej	k1gFnSc2	alej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Andělská	andělský	k2eAgFnSc1d1	Andělská
Hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Bruntál	Bruntál	k1gInSc1	Bruntál
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Bruntálský	bruntálský	k2eAgInSc1d1	bruntálský
a	a	k8xC	a
Krnovský	krnovský	k2eAgInSc1d1	krnovský
deník	deník	k1gInSc1	deník
<g/>
:	:	kIx,	:
Pouť	pouť	k1gFnSc1	pouť
Křížovou	Křížová	k1gFnSc7	Křížová
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Dalibor	Dalibor	k1gMnSc1	Dalibor
Otáhal	Otáhal	k1gMnSc1	Otáhal
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alej	alej	k1gFnSc1	alej
roku	rok	k1gInSc2	rok
<g/>
:	:	kIx,	:
Alej	alej	k1gFnSc1	alej
na	na	k7c4	na
Anenský	anenský	k2eAgInSc4d1	anenský
vrch	vrch	k1gInSc4	vrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
:	:	kIx,	:
Nejkrásnější	krásný	k2eAgFnSc1d3	nejkrásnější
alej	alej	k1gFnSc1	alej
kraje	kraj	k1gInSc2	kraj
roste	růst	k5eAaImIp3nS	růst
podle	podle	k7c2	podle
Arniky	arnika	k1gFnSc2	arnika
v	v	k7c6	v
Andělské	andělský	k2eAgFnSc6d1	Andělská
Hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
Arnika	arnika	k1gFnSc1	arnika
<g/>
,	,	kIx,	,
ČTO	ČTO	kA	ČTO
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
organizace	organizace	k1gFnSc1	organizace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TV-Noe	TV-Noe	k6eAd1	TV-Noe
<g/>
.	.	kIx.	.
</s>
<s>
Zachraňme	zachránit	k5eAaPmRp1nP	zachránit
kostely	kostel	k1gInPc4	kostel
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
díl	díl	k1gInSc4	díl
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Poutní	poutní	k2eAgInSc4d1	poutní
kostel	kostel	k1gInSc4	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
na	na	k7c6	na
Annabergu	Annaberg	k1gInSc6	Annaberg
–	–	k?	–
Andělské	andělský	k2eAgFnSc6d1	Andělská
hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Videoarchív	Videoarchív	k1gInSc1	Videoarchív
<g/>
,	,	kIx,	,
aktualizováno	aktualizován	k2eAgNnSc1d1	Aktualizováno
<g/>
:	:	kIx,	:
9	[number]	k4	9
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bruntálský	bruntálský	k2eAgInSc1d1	bruntálský
a	a	k8xC	a
Krnovský	krnovský	k2eAgInSc1d1	krnovský
deník	deník	k1gInSc1	deník
<g/>
:	:	kIx,	:
Před	před	k7c7	před
kostelem	kostel	k1gInSc7	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Anny	Anna	k1gFnSc2	Anna
vysvětili	vysvětit	k5eAaPmAgMnP	vysvětit
nový	nový	k2eAgInSc4d1	nový
symbol	symbol	k1gInSc4	symbol
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
Zuzana	Zuzana	k1gFnSc1	Zuzana
Pršalová	Pršalová	k1gFnSc1	Pršalová
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
</p>
