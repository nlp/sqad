<s>
Harvey	Harvea	k1gMnSc2	Harvea
Forbes	forbes	k1gInSc1	forbes
Fierstein	Fierstein	k2eAgInSc1d1	Fierstein
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
ocenění	ocenění	k1gNnSc4	ocenění
Tony	Tony	k1gFnSc2	Tony
za	za	k7c4	za
režii	režie	k1gFnSc4	režie
a	a	k8xC	a
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
divadelní	divadelní	k2eAgFnSc6d1	divadelní
hře	hra	k1gFnSc6	hra
Torch	Torch	k1gInSc1	Torch
Song	song	k1gInSc4	song
Trilogy	Triloga	k1gFnSc2	Triloga
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
tematice	tematika	k1gFnSc6	tematika
drag	draga	k1gFnPc2	draga
queens	queens	k6eAd1	queens
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
hry	hra	k1gFnSc2	hra
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
i	i	k8xC	i
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
film	film	k1gInSc1	film
Torch	Torch	k1gInSc1	Torch
Song	song	k1gInSc4	song
Trilogy	Triloga	k1gFnSc2	Triloga
<g/>
.	.	kIx.	.
</s>
<s>
Fierstein	Fierstein	k1gMnSc1	Fierstein
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Brooklynu	Brooklyn	k1gInSc6	Brooklyn
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
Yorku	York	k1gInSc2	York
do	do	k7c2	do
Židovské	židovský	k2eAgFnSc2d1	židovská
rodiny	rodina	k1gFnSc2	rodina
s	s	k7c7	s
židovským	židovský	k2eAgNnSc7d1	Židovské
vyznáním	vyznání	k1gNnSc7	vyznání
<g/>
,	,	kIx,	,
k	k	k7c3	k
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
Harriet-ové	Harrietvé	k2eAgMnSc2d1	Harriet-ové
(	(	kIx(	(
<g/>
née	née	k?	née
Gilbert	Gilbert	k1gMnSc1	Gilbert
<g/>
)	)	kIx)	)
a	a	k8xC	a
Irving-a	Irving	k1gFnSc1	Irving-a
Fiersteinových	Fiersteinových	k2eAgFnSc1d1	Fiersteinových
<g/>
.	.	kIx.	.
</s>
<s>
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
se	se	k3xPyFc4	se
živila	živit	k5eAaImAgFnS	živit
jako	jako	k9	jako
školní	školní	k2eAgFnPc4d1	školní
knihovnice	knihovnice	k1gFnPc4	knihovnice
a	a	k8xC	a
Irving	Irving	k1gInSc4	Irving
byl	být	k5eAaImAgMnS	být
výrobce	výrobce	k1gMnSc1	výrobce
kapesníků	kapesník	k1gInPc2	kapesník
<g/>
.	.	kIx.	.
</s>
<s>
Fierstein	Fierstein	k1gInSc1	Fierstein
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
ateista	ateista	k1gMnSc1	ateista
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
příležitostně	příležitostně	k6eAd1	příležitostně
modlí	modlit	k5eAaImIp3nS	modlit
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
časem	čas	k1gInSc7	čas
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgInS	živit
též	též	k9	též
jako	jako	k9	jako
stand-up	standp	k1gMnSc1	stand-up
komik	komik	k1gMnSc1	komik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
pobývá	pobývat	k5eAaImIp3nS	pobývat
v	v	k7c6	v
Ridgefieldu	Ridgefieldo	k1gNnSc6	Ridgefieldo
<g/>
,	,	kIx,	,
Connecticut	Connecticut	k1gMnSc1	Connecticut
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
Baba	baba	k1gFnSc1	baba
na	na	k7c4	na
zabití	zabití	k1gNnSc4	zabití
(	(	kIx(	(
<g/>
Duplex	duplex	k1gInSc1	duplex
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Kenneth	Kenneth	k1gInSc1	Kenneth
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
Smoochy	Smooch	k1gInPc1	Smooch
(	(	kIx(	(
<g/>
Death	Death	k1gMnSc1	Death
to	ten	k3xDgNnSc4	ten
Smoochy	Smooch	k1gMnPc7	Smooch
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Merv	Merv	k1gMnSc1	Merv
Green	Green	k1gInSc4	Green
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
V	v	k7c6	v
roli	role	k1gFnSc6	role
Mony	Mona	k1gFnSc2	Mona
Lisy	Lisa	k1gFnSc2	Lisa
(	(	kIx(	(
<g/>
Playing	Playing	k1gInSc1	Playing
Mona	Mon	k2eAgFnSc1d1	Mona
Lisa	Lisa	k1gFnSc1	Lisa
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
jako	jako	k8xC	jako
Bennett	Bennett	k1gInSc1	Bennett
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
The	The	k1gFnSc2	The
Sissy	Sissa	k1gFnSc2	Sissa
Duckling	Duckling	k1gInSc1	Duckling
jako	jako	k8xC	jako
Elmer	Elmer	k1gInSc1	Elmer
(	(	kIx(	(
<g/>
hlas	hlas	k1gInSc1	hlas
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
Krutá	krutý	k2eAgFnSc1d1	krutá
daň	daň	k1gFnSc1	daň
(	(	kIx(	(
<g/>
Double	double	k2eAgInSc1d1	double
Platinum	Platinum	k1gInSc1	Platinum
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Gary	Gary	k1gInPc4	Gary
Millstein	Millstein	k1gInSc1	Millstein
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
Kull	Kull	k1gMnSc1	Kull
dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
(	(	kIx(	(
<g/>
Kull	Kull	k1gMnSc1	Kull
the	the	k?	the
Conqueror	Conqueror	k1gMnSc1	Conqueror
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Juba	Juba	k1gFnSc1	Juba
1996	[number]	k4	1996
<g/>
:	:	kIx,	:
Elmo	Elmo	k1gMnSc1	Elmo
Saves	Saves	k1gMnSc1	Saves
Christmas	Christmas	k1gMnSc1	Christmas
<g />
.	.	kIx.	.
</s>
<s>
jako	jako	k8xC	jako
králík	králík	k1gMnSc1	králík
Ernest	Ernest	k1gMnSc1	Ernest
1996	[number]	k4	1996
<g/>
:	:	kIx,	:
Den	den	k1gInSc1	den
nezávislosti	nezávislost	k1gFnSc2	nezávislost
(	(	kIx(	(
<g/>
Independence	Independence	k1gFnSc1	Independence
Day	Day	k1gFnSc2	Day
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Marty	Marta	k1gFnSc2	Marta
Gilbert	gilbert	k1gInSc1	gilbert
1995	[number]	k4	1995
<g/>
:	:	kIx,	:
4	[number]	k4	4
<g/>
%	%	kIx~	%
filmová	filmový	k2eAgNnPc1d1	filmové
tajemství	tajemství	k1gNnPc1	tajemství
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Celluloid	Celluloid	k1gInSc1	Celluloid
Closet	Closet	k1gInSc1	Closet
<g/>
)	)	kIx)	)
-	-	kIx~	-
cameo	cameo	k6eAd1	cameo
1995	[number]	k4	1995
<g/>
:	:	kIx,	:
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jekyll	Jekyll	k1gMnSc1	Jekyll
a	a	k8xC	a
slečna	slečna	k1gFnSc1	slečna
Hyde	Hyde	k1gFnSc1	Hyde
(	(	kIx(	(
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jekyll	Jekyll	k1gMnSc1	Jekyll
and	and	k?	and
Ms.	Ms.	k1gMnSc4	Ms.
Hyde	Hyd	k1gMnSc4	Hyd
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Yves	Yves	k1gInSc1	Yves
DuBois	DuBois	k1gFnSc2	DuBois
1994	[number]	k4	1994
<g/>
:	:	kIx,	:
Daddy	Dadda	k1gFnSc2	Dadda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Girls	girl	k1gFnPc7	girl
jako	jako	k9	jako
Dennis	Dennis	k1gInSc4	Dennis
Sinclair	Sinclair	k1gInSc1	Sinclair
1993	[number]	k4	1993
<g/>
:	:	kIx,	:
Mrs	Mrs	k1gFnSc2	Mrs
<g/>
.	.	kIx.	.
</s>
<s>
Doubtfire	Doubtfir	k1gMnSc5	Doubtfir
-	-	kIx~	-
táta	táta	k1gMnSc1	táta
v	v	k7c6	v
sukni	sukně	k1gFnSc6	sukně
(	(	kIx(	(
<g/>
Mrs	Mrs	k1gFnPc6	Mrs
<g/>
.	.	kIx.	.
</s>
<s>
Doubtfire	Doubtfir	k1gMnSc5	Doubtfir
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Frank	Frank	k1gMnSc1	Frank
Hillard	Hillard	k1gMnSc1	Hillard
1993	[number]	k4	1993
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Harvest	Harvest	k1gFnSc1	Harvest
-	-	kIx~	-
Sklizeň	sklizeň	k1gFnSc1	sklizeň
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Harvest	Harvest	k1gMnSc1	Harvest
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Bob	Bob	k1gMnSc1	Bob
Lakin	Lakin	k1gMnSc1	Lakin
1988	[number]	k4	1988
<g/>
:	:	kIx,	:
Mučivá	mučivý	k2eAgFnSc1d1	mučivá
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
Torch	Torch	k1gInSc1	Torch
Song	song	k1gInSc1	song
Trilogy	Triloga	k1gFnSc2	Triloga
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Arnold	Arnold	k1gMnSc1	Arnold
Beckoff	Beckoff	k1gMnSc1	Beckoff
1986	[number]	k4	1986
<g/>
:	:	kIx,	:
Apology	Apologa	k1gFnPc1	Apologa
jako	jako	k8xS	jako
bezejmenná	bezejmenný	k2eAgFnSc1d1	bezejmenná
postava	postava	k1gFnSc1	postava
1984	[number]	k4	1984
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Times	Times	k1gMnSc1	Times
of	of	k?	of
Harvey	Harvea	k1gFnSc2	Harvea
Milk	Milk	k1gMnSc1	Milk
jako	jako	k8xS	jako
vypravěč	vypravěč	k1gMnSc1	vypravěč
1984	[number]	k4	1984
<g/>
:	:	kIx,	:
Garbo	Garba	k1gFnSc5	Garba
mluví	mluvit	k5eAaImIp3nS	mluvit
(	(	kIx(	(
<g/>
Garbo	Garba	k1gFnSc5	Garba
Talks	Talksa	k1gFnPc2	Talksa
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Bernie	Bernie	k1gFnSc1	Bernie
Whitlock	Whitlocka	k1gFnPc2	Whitlocka
</s>
