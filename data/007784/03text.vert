<s>
Želva	želva	k1gFnSc1	želva
žlutočelá	žlutočelý	k2eAgFnSc1d1	žlutočelý
či	či	k8xC	či
želva	želva	k1gFnSc1	želva
vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
(	(	kIx(	(
<g/>
Cuora	Cuora	k1gFnSc1	Cuora
galbinifrons	galbinifrons	k1gInSc1	galbinifrons
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhem	druh	k1gInSc7	druh
rodu	rod	k1gInSc2	rod
Cuora	Cuoro	k1gNnSc2	Cuoro
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
želvy	želva	k1gFnPc4	želva
sladkovodní	sladkovodní	k2eAgFnPc4d1	sladkovodní
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
francouzským	francouzský	k2eAgMnSc7d1	francouzský
zoologem	zoolog	k1gMnSc7	zoolog
Reném	René	k1gMnSc7	René
Léonem	Léon	k1gMnSc7	Léon
Bourretim	Bourretima	k1gFnPc2	Bourretima
<g/>
.	.	kIx.	.
</s>
<s>
Trvalo	trvat	k5eAaImAgNnS	trvat
ale	ale	k9	ale
téměř	téměř	k6eAd1	téměř
40	[number]	k4	40
let	léto	k1gNnPc2	léto
do	do	k7c2	do
prvního	první	k4xOgInSc2	první
exempláře	exemplář	k1gInSc2	exemplář
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
dovezen	dovézt	k5eAaPmNgInS	dovézt
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Krunýř	krunýř	k1gInSc1	krunýř
má	mít	k5eAaImIp3nS	mít
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
asi	asi	k9	asi
19	[number]	k4	19
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Karapax	Karapax	k1gInSc1	Karapax
má	mít	k5eAaImIp3nS	mít
klenutý	klenutý	k2eAgInSc1d1	klenutý
s	s	k7c7	s
červenooranžovou	červenooranžový	k2eAgFnSc7d1	červenooranžová
kresbou	kresba	k1gFnSc7	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Kresby	kresba	k1gFnPc1	kresba
na	na	k7c6	na
krunýři	krunýř	k1gInSc6	krunýř
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
různé	různý	k2eAgInPc4d1	různý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
plachá	plachý	k2eAgFnSc1d1	plachá
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgNnSc1d1	aktivní
je	být	k5eAaImIp3nS	být
při	pře	k1gFnSc4	pře
a	a	k8xC	a
po	po	k7c6	po
setmění	setmění	k1gNnSc6	setmění
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
malé	malý	k2eAgFnSc3d1	malá
oblasti	oblast	k1gFnSc3	oblast
jejího	její	k3xOp3gNnSc2	její
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
masožravá	masožravý	k2eAgFnSc1d1	masožravá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepohrdne	pohrdnout	k5eNaPmIp3nS	pohrdnout
ani	ani	k8xC	ani
rostlinnou	rostlinný	k2eAgFnSc7d1	rostlinná
stravou	strava	k1gFnSc7	strava
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
malými	malý	k2eAgMnPc7d1	malý
obratlovci	obratlovec	k1gMnPc7	obratlovec
a	a	k8xC	a
bezobratlými	bezobratlý	k2eAgMnPc7d1	bezobratlý
živočichy	živočich	k1gMnPc7	živočich
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ovocem	ovoce	k1gNnSc7	ovoce
a	a	k8xC	a
zeleninou	zelenina	k1gFnSc7	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
po	po	k7c6	po
setmění	setmění	k1gNnSc6	setmění
<g/>
.	.	kIx.	.
</s>
<s>
Rostoucí	rostoucí	k2eAgNnPc1d1	rostoucí
mláďata	mládě	k1gNnPc1	mládě
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
dostatek	dostatek	k1gInSc4	dostatek
všech	všecek	k3xTgFnPc2	všecek
esenciálních	esenciální	k2eAgFnPc2d1	esenciální
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Klade	klást	k5eAaImIp3nS	klást
1	[number]	k4	1
až	až	k9	až
3	[number]	k4	3
vejce	vejce	k1gNnSc1	vejce
velká	velký	k2eAgFnSc1d1	velká
kolem	kolem	k7c2	kolem
7	[number]	k4	7
<g/>
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Zahrabává	zahrabávat	k5eAaImIp3nS	zahrabávat
vejce	vejce	k1gNnSc1	vejce
do	do	k7c2	do
důlku	důlek	k1gInSc2	důlek
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
do	do	k7c2	do
písku	písek	k1gInSc2	písek
<g/>
.	.	kIx.	.
</s>
<s>
Vejce	vejce	k1gNnSc1	vejce
snáší	snášet	k5eAaImIp3nS	snášet
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
března	březen	k1gInSc2	březen
do	do	k7c2	do
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
90	[number]	k4	90
dní	den	k1gInPc2	den
od	od	k7c2	od
snůšky	snůška	k1gFnSc2	snůška
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
mláďata	mládě	k1gNnPc1	mládě
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
masožravá	masožravý	k2eAgNnPc1d1	masožravé
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
chovu	chov	k1gInSc6	chov
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
líhnutí	líhnutí	k1gNnSc3	líhnutí
vajíček	vajíčko	k1gNnPc2	vajíčko
třeba	třeba	k8wRxS	třeba
vhodný	vhodný	k2eAgInSc1d1	vhodný
inkubátor	inkubátor	k1gInSc1	inkubátor
a	a	k8xC	a
substrát	substrát	k1gInSc1	substrát
pro	pro	k7c4	pro
inkubaci	inkubace	k1gFnSc4	inkubace
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Snesená	snesený	k2eAgNnPc1d1	snesené
vajíčka	vajíčko	k1gNnPc1	vajíčko
se	se	k3xPyFc4	se
zahrabou	zahrabat	k5eAaPmIp3nP	zahrabat
v	v	k7c6	v
inkubátoru	inkubátor	k1gInSc6	inkubátor
do	do	k7c2	do
substrátu	substrát	k1gInSc2	substrát
ze	z	k7c2	z
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
především	především	k9	především
Čínu	Čína	k1gFnSc4	Čína
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
provincii	provincie	k1gFnSc6	provincie
Kuang-si	Kuange	k1gFnSc4	Kuang-se
<g/>
,	,	kIx,	,
severní	severní	k2eAgInSc4d1	severní
Vietnam	Vietnam	k1gInSc4	Vietnam
<g/>
,	,	kIx,	,
Laos	Laos	k1gInSc4	Laos
a	a	k8xC	a
ostrov	ostrov	k1gInSc4	ostrov
Chaj-nan	Chajana	k1gFnPc2	Chaj-nana
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
sladkých	sladký	k2eAgFnPc6d1	sladká
vodách	voda	k1gFnPc6	voda
<g/>
,	,	kIx,	,
mělkých	mělký	k2eAgFnPc6d1	mělká
tůních	tůně	k1gFnPc6	tůně
<g/>
,	,	kIx,	,
zatopených	zatopený	k2eAgNnPc6d1	zatopené
polích	pole	k1gNnPc6	pole
a	a	k8xC	a
loukách	louka	k1gFnPc6	louka
<g/>
,	,	kIx,	,
potocích	potok	k1gInPc6	potok
s	s	k7c7	s
břehy	břeh	k1gInPc7	břeh
s	s	k7c7	s
hustým	hustý	k2eAgInSc7d1	hustý
rostlinným	rostlinný	k2eAgInSc7d1	rostlinný
porostem	porost	k1gInSc7	porost
bohatým	bohatý	k2eAgInSc7d1	bohatý
na	na	k7c4	na
vodní	vodní	k2eAgInSc4d1	vodní
hmyz	hmyz	k1gInSc4	hmyz
a	a	k8xC	a
drobné	drobný	k2eAgFnPc4d1	drobná
rybky	rybka	k1gFnPc4	rybka
<g/>
,	,	kIx,	,
především	především	k9	především
ve	v	k7c6	v
vlhkých	vlhký	k2eAgInPc6d1	vlhký
<g/>
,	,	kIx,	,
listnatých	listnatý	k2eAgInPc6d1	listnatý
a	a	k8xC	a
smíšených	smíšený	k2eAgInPc6d1	smíšený
lesích	les	k1gInPc6	les
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
přebývá	přebývat	k5eAaImIp3nS	přebývat
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInPc1d1	severní
a	a	k8xC	a
střední	střední	k2eAgInPc1d1	střední
druhy	druh	k1gInPc1	druh
želvy	želva	k1gFnSc2	želva
vietnamské	vietnamský	k2eAgInPc1d1	vietnamský
se	se	k3xPyFc4	se
kříží	křížit	k5eAaImIp3nP	křížit
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
s	s	k7c7	s
želvou	želva	k1gFnSc7	želva
hranatou	hranatý	k2eAgFnSc7d1	hranatá
(	(	kIx(	(
<g/>
Pyxidea	Pyxide	k2eAgFnSc1d1	Pyxide
mouhotii	mouhotie	k1gFnSc4	mouhotie
nebo	nebo	k8xC	nebo
Cuora	Cuora	k1gMnSc1	Cuora
mouhotii	mouhotie	k1gFnSc4	mouhotie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
se	se	k3xPyFc4	se
tak	tak	k9	tak
nový	nový	k2eAgInSc1d1	nový
druh	druh	k1gInSc1	druh
želv	želva	k1gFnPc2	želva
Cuora	Cuor	k1gInSc2	Cuor
serrata	serrat	k2eAgFnSc1d1	serrat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Indochinese	Indochinese	k1gFnSc2	Indochinese
box	box	k1gInSc1	box
turtle	turtlat	k5eAaPmIp3nS	turtlat
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Želva	želva	k1gFnSc1	želva
vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Taxon	taxon	k1gInSc1	taxon
Cuora	Cuor	k1gInSc2	Cuor
galbinifrons	galbinifrons	k6eAd1	galbinifrons
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
