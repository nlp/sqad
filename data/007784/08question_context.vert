<s>
Želva	želva	k1gFnSc1	želva
žlutočelá	žlutočelý	k2eAgFnSc1d1	žlutočelý
či	či	k8xC	či
želva	želva	k1gFnSc1	želva
vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
(	(	kIx(	(
<g/>
Cuora	Cuora	k1gFnSc1	Cuora
galbinifrons	galbinifrons	k1gInSc1	galbinifrons
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhem	druh	k1gInSc7	druh
rodu	rod	k1gInSc2	rod
Cuora	Cuoro	k1gNnSc2	Cuoro
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
želvy	želva	k1gFnPc4	želva
sladkovodní	sladkovodní	k2eAgFnPc4d1	sladkovodní
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
francouzským	francouzský	k2eAgMnSc7d1	francouzský
zoologem	zoolog	k1gMnSc7	zoolog
Reném	René	k1gMnSc7	René
Léonem	Léon	k1gMnSc7	Léon
Bourretim	Bourretima	k1gFnPc2	Bourretima
<g/>
.	.	kIx.	.
</s>

