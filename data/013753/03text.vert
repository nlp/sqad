<s>
Terezie	Terezie	k1gFnSc1
Blumová	blumový	k2eAgFnSc1d1
</s>
<s>
Terezie	Terezie	k1gFnSc1
Blumová	blumový	k2eAgFnSc1d1
Narození	narození	k1gNnPc2
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1909	#num#	k4
<g/>
Budapešť	Budapešť	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2008	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
98	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Praha	Praha	k1gFnSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Blum	bluma	k1gFnPc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Terezie	Terezie	k1gFnSc1
Blumová	blumový	k2eAgFnSc1d1
(	(	kIx(
<g/>
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1909	#num#	k4
<g/>
,	,	kIx,
Budapešť	Budapešť	k1gFnSc1
–	–	k?
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2008	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
česká	český	k2eAgFnSc1d1
hlasová	hlasový	k2eAgFnSc1d1
pedagožka	pedagožka	k1gFnSc1
maďarského	maďarský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narodila	narodit	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
Budapešti	Budapešť	k1gFnSc6
a	a	k8xC
tam	tam	k6eAd1
se	se	k3xPyFc4
také	také	k9
seznámila	seznámit	k5eAaPmAgFnS
Lajosem	Lajos	k1gInSc7
Szamosim	Szamosim	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
hlasové	hlasový	k2eAgFnSc3d1
rehabilitaci	rehabilitace	k1gFnSc3
a	a	k8xC
hlasové	hlasový	k2eAgFnSc3d1
pedagogice	pedagogika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozvinul	rozvinout	k5eAaPmAgMnS
postupně	postupně	k6eAd1
vlastní	vlastní	k2eAgFnSc4d1
metodu	metoda	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yQgFnSc3,k3yIgFnSc3,k3yRgFnSc3
se	se	k3xPyFc4
od	od	k7c2
něj	on	k3xPp3gMnSc2
Terezie	Terezie	k1gFnSc1
Blumová	blumový	k2eAgFnSc1d1
naučila	naučit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
se	se	k3xPyFc4
přestěhovala	přestěhovat	k5eAaPmAgFnS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
manželem	manžel	k1gMnSc7
Ladislavem	Ladislav	k1gMnSc7
Blumem	Blumem	k?
(	(	kIx(
<g/>
1911	#num#	k4
–	–	k?
1994	#num#	k4
<g/>
)	)	kIx)
do	do	k7c2
Prahy	Praha	k1gFnSc2
a	a	k8xC
tak	tak	k6eAd1
přinesla	přinést	k5eAaPmAgFnS
tuto	tento	k3xDgFnSc4
metodu	metoda	k1gFnSc4
do	do	k7c2
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ladislav	Ladislava	k1gFnPc2
Blum	bluma	k1gFnPc2
se	se	k3xPyFc4
zpěvu	zpěv	k1gInSc2
intenzivně	intenzivně	k6eAd1
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
Praze	Praha	k1gFnSc6
více	hodně	k6eAd2
než	než	k8xS
třicet	třicet	k4xCc4
let	léto	k1gNnPc2
působil	působit	k5eAaImAgMnS
jako	jako	k9
kantor	kantor	k1gMnSc1
(	(	kIx(
<g/>
chazan	chazan	k1gMnSc1
<g/>
)	)	kIx)
v	v	k7c6
Jeruzalémské	jeruzalémský	k2eAgFnSc6d1
synagoze	synagoga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Terezie	Terezie	k1gFnSc1
Blumová	blumový	k2eAgFnSc1d1
se	se	k3xPyFc4
výuce	výuka	k1gFnSc3
věnovala	věnovat	k5eAaImAgFnS,k5eAaPmAgFnS
po	po	k7c4
celý	celý	k2eAgInSc4d1
svůj	svůj	k3xOyFgInSc4
velmi	velmi	k6eAd1
dlouhý	dlouhý	k2eAgInSc4d1
život	život	k1gInSc4
a	a	k8xC
vychovala	vychovat	k5eAaPmAgFnS
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
zpěváků	zpěvák	k1gMnPc2
<g/>
,	,	kIx,
mimo	mimo	k6eAd1
jiné	jiný	k2eAgInPc1d1
Janu	Jana	k1gFnSc4
Lewitovou	Lewitový	k2eAgFnSc4d1
<g/>
,	,	kIx,
Markétu	Markéta	k1gFnSc4
Malcovou	Malcová	k1gFnSc4
nebo	nebo	k8xC
Pavlu	Pavla	k1gFnSc4
Kšicovou	Kšicový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Metoda	metoda	k1gFnSc1
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1
myšlenkou	myšlenka	k1gFnSc7
pěvecké	pěvecký	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
vyučovala	vyučovat	k5eAaImAgFnS
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
obnovení	obnovení	k1gNnSc1
přirozeného	přirozený	k2eAgNnSc2d1
plynutí	plynutí	k1gNnSc2
dechu	dech	k1gInSc2
a	a	k8xC
hlasu	hlas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Principem	princip	k1gInSc7
tedy	tedy	k9
mnohdy	mnohdy	k6eAd1
nebylo	být	k5eNaImAgNnS
něco	něco	k3yInSc4
se	se	k3xPyFc4
učit	učit	k5eAaImF
<g/>
,	,	kIx,
ale	ale	k8xC
odnaučovat	odnaučovat	k5eAaImF
<g/>
,	,	kIx,
uvolňovat	uvolňovat	k5eAaImF
cestu	cesta	k1gFnSc4
přirozeným	přirozený	k2eAgInPc3d1
impulsům	impuls	k1gInPc3
dechu	dech	k1gInSc2
a	a	k8xC
hlasu	hlas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dosažení	dosažení	k1gNnSc3
tohoto	tento	k3xDgInSc2
cíle	cíl	k1gInSc2
se	se	k3xPyFc4
používalo	používat	k5eAaImAgNnS
zvláštních	zvláštní	k2eAgInPc2d1
hmatů	hmat	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
učitel	učitel	k1gMnSc1
zamezoval	zamezovat	k5eAaImAgMnS
přílišnému	přílišný	k2eAgNnSc3d1
stahování	stahování	k1gNnSc3
některých	některý	k3yIgInPc2
svalů	sval	k1gInPc2
<g/>
,	,	kIx,
</s>
<s>
jež	jenž	k3xRgInPc1
mohou	moct	k5eAaImIp3nP
blokovat	blokovat	k5eAaImF
dech	dech	k1gInSc4
a	a	k8xC
hlas	hlas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Student	student	k1gMnSc1
tak	tak	k9
měl	mít	k5eAaImAgMnS
bezprostřední	bezprostřední	k2eAgFnSc4d1
zkušenost	zkušenost	k1gFnSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
má	mít	k5eAaImIp3nS
správně	správně	k6eAd1
zpívat	zpívat	k5eAaImF
a	a	k8xC
posléze	posléze	k6eAd1
ji	on	k3xPp3gFnSc4
dokázal	dokázat	k5eAaPmAgInS
uplatnit	uplatnit	k5eAaPmF
sám	sám	k3xTgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hmaty	hmat	k1gInPc1
spočívaly	spočívat	k5eAaImAgFnP
například	například	k6eAd1
v	v	k7c6
lehkém	lehký	k2eAgNnSc6d1
v	v	k7c6
přidržení	přidržení	k1gNnSc6
vyplazeného	vyplazený	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
čímž	což	k3yQnSc7,k3yRnSc7
vzniká	vznikat	k5eAaImIp3nS
více	hodně	k6eAd2
prostoru	prostor	k1gInSc2
v	v	k7c6
hrtanu	hrtan	k1gInSc6
nad	nad	k7c7
hlasivkami	hlasivka	k1gFnPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lehkém	lehký	k2eAgInSc6d1
stisku	stisk	k1gInSc6
mezi	mezi	k7c7
jazylkou	jazylka	k1gFnSc7
a	a	k8xC
štítnou	štítný	k2eAgFnSc7d1
chrupavkou	chrupavka	k1gFnSc7
či	či	k8xC
v	v	k7c6
lehkém	lehký	k2eAgNnSc6d1
uchopení	uchopení	k1gNnSc6
brady	brada	k1gFnSc2
(	(	kIx(
<g/>
kdy	kdy	k6eAd1
se	se	k3xPyFc4
zároveň	zároveň	k6eAd1
uvolní	uvolnit	k5eAaPmIp3nS
napětí	napětí	k1gNnSc4
kořene	kořen	k1gInSc2
jazyka	jazyk	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
vždy	vždy	k6eAd1
je	být	k5eAaImIp3nS
nalézt	nalézt	k5eAaPmF,k5eAaBmF
přirozené	přirozený	k2eAgNnSc1d1
užívání	užívání	k1gNnSc1
hlasu	hlas	k1gInSc2
a	a	k8xC
to	ten	k3xDgNnSc4
rozvíjet	rozvíjet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Motto	motto	k1gNnSc1
profesorky	profesorka	k1gFnSc2
Blumové	blumový	k2eAgFnSc2d1
bylo	být	k5eAaImAgNnS
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Volnost	volnost	k1gFnSc1
místo	místo	k7c2
násilí	násilí	k1gNnSc2
<g/>
,	,	kIx,
cit	cit	k1gInSc1
místo	místo	k7c2
sentimentu	sentiment	k1gInSc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Detailní	detailní	k2eAgInSc1d1
popis	popis	k1gInSc1
metody	metoda	k1gFnSc2
v	v	k7c6
článku	článek	k1gInSc6
Bronislavy	Bronislav	k1gMnPc4
Tomanové	Tomanové	k2eAgMnSc2d1
</s>
<s>
Přednáška	přednáška	k1gFnSc1
Jany	Jana	k1gFnSc2
Lewitové	Lewitový	k2eAgFnSc2d1
o	o	k7c6
výuce	výuka	k1gFnSc6
paní	paní	k1gFnSc2
Blumové	blumový	k2eAgFnSc2d1
</s>
<s>
Stránky	stránka	k1gFnPc1
Markéty	Markéta	k1gFnSc2
Malcové	Malcová	k1gFnSc2
</s>
<s>
Anglické	anglický	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
o	o	k7c6
Lajosi	Lajose	k1gFnSc6
Szamosim	Szamosima	k1gFnPc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc3
metodě	metoda	k1gFnSc3
výuky	výuka	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
