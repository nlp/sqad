<s>
Alpy	Alpy	k1gFnPc1
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
pohoří	pohořet	k5eAaPmIp3nP
Alpy	Alpy	k1gFnPc1
na	na	k7c6
Měsíci	měsíc	k1gInSc6
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
Montes	Montes	k1gMnSc1
Alpes	Alpes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
AlpyAlpen	AlpyAlpen	k2eAgInSc1d1
<g/>
,	,	kIx,
Alpes	Alpes	k1gInSc1
<g/>
,	,	kIx,
AlpsAlpi	AlpsAlpi	k1gNnSc1
<g/>
,	,	kIx,
Alpe	Alpe	k1gFnSc1
Mont	Mont	k1gMnSc1
Blanc	Blanc	k1gMnSc1
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
</s>
<s>
4	#num#	k4
810	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
(	(	kIx(
<g/>
Mont	Mont	k1gMnSc1
Blanc	Blanc	k1gMnSc1
<g/>
)	)	kIx)
Délka	délka	k1gFnSc1
</s>
<s>
1	#num#	k4
200	#num#	k4
km	km	kA
Rozloha	rozloha	k1gFnSc1
</s>
<s>
200	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
km²	km²	k?
</s>
<s>
Nadřazená	nadřazený	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Alpsko-himálajský	alpsko-himálajský	k2eAgInSc1d1
systém	systém	k1gInSc1
Sousedníjednotky	Sousedníjednotka	k1gFnSc2
</s>
<s>
Hercynská	hercynský	k2eAgNnPc1d1
pohoří	pohoří	k1gNnPc1
<g/>
,	,	kIx,
Karpaty	Karpaty	k1gInPc1
<g/>
,	,	kIx,
Apeniny	Apeniny	k1gFnPc1
<g/>
,	,	kIx,
Dinárské	dinárský	k2eAgFnPc1d1
hory	hora	k1gFnPc1
Podřazenéjednotky	Podřazenéjednotka	k1gFnSc2
</s>
<s>
Západní	západní	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
<g/>
,	,	kIx,
Východní	východní	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
<g/>
,	,	kIx,
Alpské	alpský	k2eAgNnSc1d1
podhůří	podhůří	k1gNnSc1
<g/>
,	,	kIx,
Jura	jura	k1gFnSc1
</s>
<s>
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
FrancieNěmecko	FrancieNěmecka	k1gMnSc5
NěmeckoŠvýcarsko	NěmeckoŠvýcarska	k1gMnSc5
ŠvýcarskoLichtenštejnsko	ŠvýcarskoLichtenštejnska	k1gMnSc5
LichtenštejnskoRakousko	LichtenštejnskoRakouska	k1gMnSc5
RakouskoItálie	RakouskoItálie	k1gFnPc1
ItálieSlovinsko	ItálieSlovinska	k1gFnSc5
SlovinskoMaďarsko	SlovinskoMaďarska	k1gFnSc5
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
reliéfní	reliéfní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Alp	Alpy	k1gFnPc2
</s>
<s>
Povodí	povodí	k1gNnSc1
</s>
<s>
Rhône	Rhônout	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
,	,	kIx,
Rýn	Rýn	k1gInSc1
<g/>
,	,	kIx,
Dunaj	Dunaj	k1gInSc1
<g/>
,	,	kIx,
Pád	Pád	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
46	#num#	k4
<g/>
°	°	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
10	#num#	k4
<g/>
°	°	k?
v.	v.	k?
d.	d.	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Alpy	Alpy	k1gFnPc1
–	–	k?
satelitní	satelitní	k2eAgInSc1d1
snímek	snímek	k1gInSc1
</s>
<s>
Alpy	Alpy	k1gFnPc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Alpen	Alpen	k2eAgInSc1d1
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
Alpes	Alpes	k1gInSc1
<g/>
,	,	kIx,
rétorománsky	rétorománsky	k6eAd1
Alps	Alps	k1gInSc1
<g/>
,	,	kIx,
italsky	italsky	k6eAd1
Alpi	Alp	k1gMnPc1
<g/>
,	,	kIx,
slovinsky	slovinsky	k6eAd1
Alpe	Alpe	k1gFnSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
rozsáhlé	rozsáhlý	k2eAgNnSc4d1
evropské	evropský	k2eAgNnSc4d1
pohoří	pohoří	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
„	„	k?
<g/>
Alpy	alpa	k1gFnSc2
<g/>
“	“	k?
resp.	resp.	kA
francouzský	francouzský	k2eAgMnSc1d1
a	a	k8xC
latinský	latinský	k2eAgInSc1d1
název	název	k1gInSc1
„	„	k?
<g/>
Alpes	Alpes	k1gInSc1
<g/>
“	“	k?
pravděpodobně	pravděpodobně	k6eAd1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
latinského	latinský	k2eAgInSc2d1
albus	albus	k1gInSc1
(	(	kIx(
<g/>
bílý	bílý	k2eAgInSc1d1
<g/>
)	)	kIx)
nebo	nebo	k8xC
altus	altus	k1gMnSc1
(	(	kIx(
<g/>
vysoký	vysoký	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgNnSc7d1
možným	možný	k2eAgNnSc7d1
vysvětlením	vysvětlení	k1gNnSc7
je	být	k5eAaImIp3nS
keltský	keltský	k2eAgInSc1d1
výraz	výraz	k1gInSc1
„	„	k?
<g/>
alp	alpa	k1gFnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
označuje	označovat	k5eAaImIp3nS
vysoké	vysoký	k2eAgNnSc4d1
pohoří	pohoří	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
Alp	Alpy	k1gFnPc2
je	být	k5eAaImIp3nS
pojmenováno	pojmenován	k2eAgNnSc4d1
pohoří	pohoří	k1gNnSc4
Montes	Montes	k1gMnSc1
Alpes	Alpes	k1gMnSc1
na	na	k7c6
přivrácené	přivrácený	k2eAgFnSc6d1
straně	strana	k1gFnSc6
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Geografický	geografický	k2eAgInSc1d1
popis	popis	k1gInSc1
</s>
<s>
Evropské	evropský	k2eAgNnSc1d1
celé	celý	k2eAgNnSc1d1
pohoří	pohoří	k1gNnSc1
se	se	k3xPyFc4
táhne	táhnout	k5eAaImIp3nS
v	v	k7c6
šířce	šířka	k1gFnSc6
od	od	k7c2
130	#num#	k4
až	až	k9
260	#num#	k4
kilometrů	kilometr	k1gInPc2
a	a	k8xC
v	v	k7c6
délce	délka	k1gFnSc6
kolem	kolem	k7c2
1200	#num#	k4
kilometrů	kilometr	k1gInPc2
celkem	celkem	k6eAd1
přes	přes	k7c4
sedm	sedm	k4xCc4
států	stát	k1gInPc2
od	od	k7c2
francouzské	francouzský	k2eAgFnSc2d1
Riviéry	Riviéra	k1gFnSc2
a	a	k8xC
pobřeží	pobřeží	k1gNnSc2
Středozemního	středozemní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
až	až	k9
k	k	k7c3
Vídni	Vídeň	k1gFnSc3
na	na	k7c6
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaujímá	zaujímat	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
kolem	kolem	k7c2
200	#num#	k4
000	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Řeka	řeka	k1gFnSc1
Rýn	rýna	k1gFnPc2
na	na	k7c6
švýcarsko-rakouských	švýcarsko-rakouský	k2eAgFnPc6d1
a	a	k8xC
švýcarsko-lichtenštejnských	švýcarsko-lichtenštejnský	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
a	a	k8xC
přibližná	přibližný	k2eAgFnSc1d1
spojnice	spojnice	k1gFnSc1
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2
s	s	k7c7
italským	italský	k2eAgNnSc7d1
městem	město	k1gNnSc7
Como	Como	k6eAd1
vytyčují	vytyčovat	k5eAaImIp3nP
hranici	hranice	k1gFnSc4
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
základními	základní	k2eAgInPc7d1
podcelky	podcelek	k1gInPc7
<g/>
:	:	kIx,
Západními	západní	k2eAgFnPc7d1
a	a	k8xC
Východními	východní	k2eAgFnPc7d1
Alpami	Alpy	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Horstvo	horstvo	k1gNnSc1
s	s	k7c7
názvem	název	k1gInSc7
Alpy	alpa	k1gFnSc2
se	se	k3xPyFc4
nenachází	nacházet	k5eNaImIp3nS
jen	jen	k9
na	na	k7c6
evropském	evropský	k2eAgInSc6d1
kontinentě	kontinent	k1gInSc6
–	–	k?
existují	existovat	k5eAaImIp3nP
také	také	k9
Australské	australský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
<g/>
,	,	kIx,
Japonské	japonský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
a	a	k8xC
Jižní	jižní	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
<g/>
,	,	kIx,
rozprostírající	rozprostírající	k2eAgFnPc1d1
se	se	k3xPyFc4
na	na	k7c6
Novém	nový	k2eAgInSc6d1
Zélandu	Zéland	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Geologie	geologie	k1gFnSc1
</s>
<s>
Vznik	vznik	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Geologie	geologie	k1gFnSc2
Alp	Alpy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Počátek	počátek	k1gInSc1
vzniku	vznik	k1gInSc2
se	se	k3xPyFc4
datuje	datovat	k5eAaImIp3nS
do	do	k7c2
doby	doba	k1gFnSc2
kolem	kolem	k7c2
spodní	spodní	k2eAgFnSc2d1
křídy	křída	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
na	na	k7c4
dno	dno	k1gNnSc4
geosynklinály	geosynklinála	k1gFnSc2
<g/>
,	,	kIx,
neboli	neboli	k8xC
mořské	mořský	k2eAgFnPc1d1
rozsedliny	rozsedlina	k1gFnPc1
v	v	k7c6
oceánu	oceán	k1gInSc6
Tethys	Tethysa	k1gFnPc2
usazovaly	usazovat	k5eAaImAgInP
sedimenty	sediment	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
třetihorách	třetihory	k1gFnPc6
<g/>
,	,	kIx,
asi	asi	k9
před	před	k7c7
40	#num#	k4
až	až	k9
60	#num#	k4
miliony	milion	k4xCgInPc1
let	léto	k1gNnPc2
vlivem	vlivem	k7c2
natlačování	natlačování	k1gNnSc2
Africké	africký	k2eAgFnSc2d1
desky	deska	k1gFnSc2
na	na	k7c4
Eurasijskou	Eurasijský	k2eAgFnSc4d1
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
vrstvy	vrstva	k1gFnPc1
usazenin	usazenina	k1gFnPc2
vrásnit	vrásnit	k5eAaImF
<g/>
,	,	kIx,
tzv.	tzv.	kA
alpínská	alpínský	k2eAgFnSc1d1
orogeneze	orogeneze	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazné	výrazný	k2eAgNnSc1d1
vyzdvihování	vyzdvihování	k1gNnSc1
probíhalo	probíhat	k5eAaImAgNnS
na	na	k7c6
přelomu	přelom	k1gInSc6
třetihor	třetihory	k1gFnPc2
a	a	k8xC
čtvrtohor	čtvrtohory	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
faktorem	faktor	k1gInSc7
modelování	modelování	k1gNnSc2
pohoří	pohoří	k1gNnSc2
bylo	být	k5eAaImAgNnS
pleistocénní	pleistocénní	k2eAgNnSc1d1
zalednění	zalednění	k1gNnSc1
<g/>
,	,	kIx,
po	po	k7c6
němž	jenž	k3xRgInSc6
nám	my	k3xPp1nPc3
zbylo	zbýt	k5eAaPmAgNnS
pouze	pouze	k6eAd1
něco	něco	k3yInSc4
kolem	kolem	k7c2
3	#num#	k4
000	#num#	k4
km²	km²	k?
ledovců	ledovec	k1gInPc2
<g/>
,	,	kIx,
četné	četný	k2eAgFnSc2d1
kary	kara	k1gFnSc2
a	a	k8xC
jezera	jezero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Složení	složení	k1gNnSc1
</s>
<s>
Alpy	Alpy	k1gFnPc1
jsou	být	k5eAaImIp3nP
složitý	složitý	k2eAgInSc4d1
příkrovový	příkrovový	k2eAgInSc4d1
horský	horský	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
severního	severní	k2eAgInSc2d1
a	a	k8xC
severozápadního	severozápadní	k2eAgInSc2d1
směru	směr	k1gInSc2
byly	být	k5eAaImAgInP
kdysi	kdysi	k6eAd1
nasunuty	nasunout	k5eAaPmNgInP
hlavní	hlavní	k2eAgInPc1d1
<g/>
,	,	kIx,
největší	veliký	k2eAgInPc1d3
příkrovy	příkrov	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
geologické	geologický	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
můžeme	moct	k5eAaImIp1nP
Východní	východní	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
rozdělit	rozdělit	k5eAaPmF
na	na	k7c4
Centrální	centrální	k2eAgFnPc4d1
krystalické	krystalický	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
(	(	kIx(
<g/>
rula	rula	k1gFnSc1
<g/>
,	,	kIx,
svor	svor	k1gInSc1
a	a	k8xC
břidlice	břidlice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Severní	severní	k2eAgFnPc1d1
vápencové	vápencový	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
(	(	kIx(
<g/>
vápenec	vápenec	k1gInSc1
<g/>
,	,	kIx,
dolomit	dolomit	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Jižní	jižní	k2eAgFnSc2d1
vápencové	vápencový	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vápencových	vápencový	k2eAgFnPc6d1
Alpách	Alpy	k1gFnPc6
převládají	převládat	k5eAaImIp3nP
horniny	hornina	k1gFnPc1
zejména	zejména	k9
z	z	k7c2
období	období	k1gNnSc2
triasu	trias	k1gInSc2
<g/>
,	,	kIx,
jury	jura	k1gFnSc2
a	a	k8xC
křídy	křída	k1gFnSc2
(	(	kIx(
<g/>
usazené	usazený	k2eAgFnSc2d1
horniny	hornina	k1gFnSc2
z	z	k7c2
druhohor	druhohory	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severozápadním	severozápadní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
Západních	západní	k2eAgFnPc2d1
Alp	Alpy	k1gFnPc2
(	(	kIx(
<g/>
Vnější	vnější	k2eAgFnSc2d1
krystalické	krystalický	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
také	také	k6eAd1
vyskytují	vyskytovat	k5eAaImIp3nP
pásma	pásmo	k1gNnPc1
vápenců	vápenec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krystalické	krystalický	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nP
na	na	k7c4
Vnější	vnější	k2eAgInPc4d1
(	(	kIx(
<g/>
hercynské	hercynský	k2eAgInPc4d1
krystalické	krystalický	k2eAgInPc4d1
masívy	masív	k1gInPc4
a	a	k8xC
Vnitřní	vnitřní	k2eAgInPc4d1
(	(	kIx(
<g/>
pevninské	pevninský	k2eAgInPc4d1
příkrovy	příkrov	k1gInPc4
a	a	k8xC
metamorfované	metamorfovaný	k2eAgFnPc4d1
horniny	hornina	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centrální	centrální	k2eAgInPc1d1
masívy	masív	k1gInPc1
tvoří	tvořit	k5eAaImIp3nP
jádro	jádro	k1gNnSc4
(	(	kIx(
<g/>
žuly	žula	k1gFnSc2
<g/>
,	,	kIx,
ruly	rula	k1gFnSc2
a	a	k8xC
krystalické	krystalický	k2eAgFnSc2d1
břidlice	břidlice	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
zbytky	zbytek	k1gInPc4
starších	starý	k2eAgFnPc2d2
hor	hora	k1gFnPc2
z	z	k7c2
karbonu	karbon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vodstvo	vodstvo	k1gNnSc1
a	a	k8xC
zalednění	zalednění	k1gNnSc1
</s>
<s>
Řeky	Řek	k1gMnPc4
</s>
<s>
řeka	řeka	k1gFnSc1
Rýn	rýna	k1gFnPc2
</s>
<s>
řeka	řeka	k1gFnSc1
Pád	Pád	k1gInSc1
</s>
<s>
Alpy	Alpy	k1gFnPc1
jsou	být	k5eAaImIp3nP
nejvýznamnější	významný	k2eAgFnSc7d3
pramennou	pramenný	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
mnoha	mnoho	k4c2
jejich	jejich	k3xOp3gInPc6
vrcholech	vrchol	k1gInPc6
či	či	k8xC
hřebenech	hřeben	k1gInPc6
probíhají	probíhat	k5eAaImIp3nP
i	i	k9
mnohá	mnohý	k2eAgNnPc4d1
evropská	evropský	k2eAgNnPc4d1
rozvodí	rozvodí	k1gNnPc4
(	(	kIx(
<g/>
Albula	Albula	k1gFnSc1
<g/>
,	,	kIx,
St.	st.	kA
Gotthardpass	Gotthardpass	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alpské	alpský	k2eAgFnPc4d1
řeky	řeka	k1gFnPc4
nebo	nebo	k8xC
povodí	povodí	k1gNnPc1
se	se	k3xPyFc4
po	po	k7c6
dlouhé	dlouhý	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
vlévají	vlévat	k5eAaImIp3nP
do	do	k7c2
moří	moře	k1gNnPc2
(	(	kIx(
<g/>
Středozemní	středozemní	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
,	,	kIx,
Jaderské	jaderský	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
,	,	kIx,
Černé	Černé	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
,	,	kIx,
Severní	severní	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
masivu	masiv	k1gInSc6
Gotthard	Gottharda	k1gFnPc2
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
se	se	k3xPyFc4
dokonce	dokonce	k9
rozcházejí	rozcházet	k5eAaImIp3nP
řeky	řeka	k1gFnPc1
do	do	k7c2
tří	tři	k4xCgNnPc2
moří	moře	k1gNnPc2
(	(	kIx(
<g/>
Rhôna	Rhôna	k1gFnSc1
<g/>
,	,	kIx,
Rýn	Rýn	k1gInSc1
<g/>
,	,	kIx,
Ticino	Ticino	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
v	v	k7c6
pohoří	pohoří	k1gNnSc6
Albula	Albulum	k1gNnSc2
poblíž	poblíž	k6eAd1
Svatého	svatý	k2eAgMnSc2d1
Mořice	Mořic	k1gMnSc2
(	(	kIx(
<g/>
Inn	Inn	k1gFnSc1
<g/>
,	,	kIx,
Rýn	Rýn	k1gInSc1
a	a	k8xC
Pád	Pád	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Východních	východní	k2eAgFnPc6d1
Alpách	Alpy	k1gFnPc6
tvoří	tvořit	k5eAaImIp3nP
toky	tok	k1gInPc1
povětšinou	povětšinou	k6eAd1
dlouhá	dlouhý	k2eAgNnPc1d1
údolí	údolí	k1gNnPc1
<g/>
,	,	kIx,
rovnoběžná	rovnoběžný	k2eAgFnSc1d1
s	s	k7c7
hřebeny	hřeben	k1gInPc7
hor	hora	k1gFnPc2
(	(	kIx(
<g/>
Dráva	Dráva	k1gFnSc1
<g/>
,	,	kIx,
Inn	Inn	k1gFnSc1
<g/>
,	,	kIx,
Enns	Enns	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západní	západní	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
jsou	být	k5eAaImIp3nP
na	na	k7c6
tom	ten	k3xDgNnSc6
naopak	naopak	k6eAd1
<g/>
,	,	kIx,
toky	tok	k1gInPc1
zde	zde	k6eAd1
vytvářejí	vytvářet	k5eAaImIp3nP
kratší	krátký	k2eAgNnPc1d2
a	a	k8xC
příkřejší	příkrý	k2eAgNnPc1d2
údolí	údolí	k1gNnPc1
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
s	s	k7c7
velkým	velký	k2eAgNnSc7d1
převýšením	převýšení	k1gNnSc7
(	(	kIx(
<g/>
Aara	Aarum	k1gNnPc1
<g/>
,	,	kIx,
Rýn	Rýn	k1gInSc1
<g/>
,	,	kIx,
Rhôna	Rhôna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
jsou	být	k5eAaImIp3nP
tyto	tento	k3xDgFnPc4
doliny	dolina	k1gFnPc4
směrově	směrově	k6eAd1
napříč	napříč	k6eAd1
horskými	horský	k2eAgInPc7d1
masivy	masiv	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
vznikly	vzniknout	k5eAaPmAgFnP
na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
vodopády	vodopád	k1gInPc1
(	(	kIx(
<g/>
např.	např.	kA
Krimmelské	Krimmelský	k2eAgInPc1d1
vodopády	vodopád	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řeky	řeka	k1gFnSc2
v	v	k7c6
Alpách	Alpy	k1gFnPc6
jsou	být	k5eAaImIp3nP
z	z	k7c2
velké	velký	k2eAgFnSc2d1
většiny	většina	k1gFnSc2
napájeny	napájet	k5eAaImNgInP
z	z	k7c2
ledovců	ledovec	k1gInPc2
a	a	k8xC
proto	proto	k8xC
jsou	být	k5eAaImIp3nP
v	v	k7c6
létě	léto	k1gNnSc6
bohaté	bohatý	k2eAgFnSc2d1
na	na	k7c4
vodu	voda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
nejvýznamnějším	významný	k2eAgInPc3d3
alpským	alpský	k2eAgInPc3d1
tokům	tok	k1gInPc3
náleží	náležet	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
název	název	k1gInSc1
</s>
<s>
délka	délka	k1gFnSc1
(	(	kIx(
<g/>
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
Rýn	Rýn	k1gInSc1
<g/>
1	#num#	k4
360	#num#	k4
</s>
<s>
Sáva	Sáva	k1gFnSc1
<g/>
946	#num#	k4
</s>
<s>
Rhôna	Rhôna	k1gFnSc1
<g/>
812	#num#	k4
</s>
<s>
Dráva	Dráva	k6eAd1
<g/>
720	#num#	k4
</s>
<s>
Pád	Pád	k1gInSc1
<g/>
652	#num#	k4
</s>
<s>
Inn	Inn	k?
<g/>
510	#num#	k4
</s>
<s>
Mura	mura	k1gFnSc1
<g/>
445	#num#	k4
</s>
<s>
Adiže	Adiže	k1gFnSc1
<g/>
410	#num#	k4
</s>
<s>
Durance	durance	k1gFnSc1
<g/>
305	#num#	k4
</s>
<s>
Isè	Isè	k1gMnSc5
<g/>
290	#num#	k4
</s>
<s>
Jezera	jezero	k1gNnPc1
</s>
<s>
Ženevské	ženevský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
</s>
<s>
Obrovské	obrovský	k2eAgInPc1d1
ledovce	ledovec	k1gInPc1
z	z	k7c2
období	období	k1gNnSc2
Pleistocénu	pleistocén	k1gInSc2
doslova	doslova	k6eAd1
vymačkaly	vymačkat	k5eAaPmAgInP
na	na	k7c6
krajích	kraj	k1gInPc6
hor	hora	k1gFnPc2
časté	častý	k2eAgFnSc2d1
mohutné	mohutný	k2eAgFnSc2d1
morény	moréna	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgFnPc6,k3yIgFnPc6,k3yQgFnPc6
následkem	následkem	k7c2
toho	ten	k3xDgNnSc2
vznikla	vzniknout	k5eAaPmAgFnS
jezera	jezero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžně	běžně	k6eAd1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
na	na	k7c6
severních	severní	k2eAgInPc6d1
i	i	k8xC
jižních	jižní	k2eAgInPc6d1
svazích	svah	k1gInPc6
hor.	hor.	k?
Také	také	k9
plesa	pleso	k1gNnPc1
zdobící	zdobící	k2eAgNnPc1d1
vyšší	vysoký	k2eAgNnPc1d2
pásma	pásmo	k1gNnPc1
mají	mít	k5eAaImIp3nP
svůj	svůj	k3xOyFgInSc4
původ	původ	k1gInSc4
v	v	k7c6
ledovcové	ledovcový	k2eAgFnSc6d1
činnosti	činnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3
jezerem	jezero	k1gNnSc7
Alp	Alpy	k1gFnPc2
je	být	k5eAaImIp3nS
Ženevské	ženevský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
na	na	k7c6
hranicích	hranice	k1gFnPc6
Švýcarska	Švýcarsko	k1gNnSc2
a	a	k8xC
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
nejhlubší	hluboký	k2eAgNnPc1d3
jsou	být	k5eAaImIp3nP
italská	italský	k2eAgNnPc1d1
jezera	jezero	k1gNnPc1
Lago	Lago	k6eAd1
di	di	k?
Como	Como	k1gNnSc1
(	(	kIx(
<g/>
Comské	Comský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
,	,	kIx,
409	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Lago	Laga	k1gMnSc5
Maggiore	Maggior	k1gMnSc5
(	(	kIx(
<g/>
365	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
Lago	Lago	k6eAd1
di	di	k?
Garda	garda	k1gFnSc1
(	(	kIx(
<g/>
Gardské	Gardský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
,	,	kIx,
346	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
nejvýznamnějším	významný	k2eAgNnPc3d3
jezerům	jezero	k1gNnPc3
Alp	Alpy	k1gFnPc2
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
název	název	k1gInSc1
</s>
<s>
plocha	plocha	k1gFnSc1
(	(	kIx(
<g/>
km²	km²	k?
<g/>
)	)	kIx)
</s>
<s>
Ženevské	ženevský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
581	#num#	k4
</s>
<s>
Bodamské	bodamský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
538	#num#	k4
</s>
<s>
Gardské	Gardský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
370	#num#	k4
</s>
<s>
Neuchâtelské	Neuchâtelský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
218	#num#	k4
</s>
<s>
Lago	Laga	k1gMnSc5
Maggiore	Maggior	k1gMnSc5
<g/>
212	#num#	k4
</s>
<s>
Komské	Komský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
146	#num#	k4
</s>
<s>
Vierwaldstätter	Vierwaldstätter	k1gInSc1
See	See	k1gFnSc2
<g/>
113	#num#	k4
</s>
<s>
Ledovce	ledovec	k1gInPc1
</s>
<s>
Velký	velký	k2eAgInSc1d1
Aletschský	Aletschský	k2eAgInSc1d1
ledovec	ledovec	k1gInSc1
</s>
<s>
Dnešní	dnešní	k2eAgNnSc1d1
zalednění	zalednění	k1gNnSc1
Alpského	alpský	k2eAgInSc2d1
systému	systém	k1gInSc2
(	(	kIx(
<g/>
3	#num#	k4
200	#num#	k4
km²	km²	k?
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
asi	asi	k9
2	#num#	k4
%	%	kIx~
původního	původní	k2eAgInSc2d1
plochy	plocha	k1gFnPc4
pleistocéních	pleistocéní	k2eAgInPc2d1
ledovců	ledovec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
holocénu	holocén	k1gInSc6
před	před	k7c7
5900	#num#	k4
lety	léto	k1gNnPc7
zde	zde	k6eAd1
ale	ale	k9
bylo	být	k5eAaImAgNnS
méně	málo	k6eAd2
ledovců	ledovec	k1gInPc2
než	než	k8xS
dnes	dnes	k6eAd1
a	a	k8xC
přibývaly	přibývat	k5eAaImAgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
posledních	poslední	k2eAgNnPc2d1
150	#num#	k4
let	léto	k1gNnPc2
pozorujeme	pozorovat	k5eAaImIp1nP
značný	značný	k2eAgInSc4d1
ústup	ústup	k1gInSc4
ledovců	ledovec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
proměna	proměna	k1gFnSc1
odtávání	odtávání	k1gNnSc2
(	(	kIx(
<g/>
ablace	ablace	k1gFnSc1
<g/>
)	)	kIx)
bývá	bývat	k5eAaImIp3nS
v	v	k7c6
některých	některý	k3yIgNnPc6
obdobích	období	k1gNnPc6
přerušována	přerušovat	k5eAaImNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
však	však	k9
i	i	k9
roky	rok	k1gInPc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
dochází	docházet	k5eAaImIp3nS
i	i	k9
k	k	k7c3
úbytkům	úbytek	k1gInPc3
v	v	k7c6
řádech	řád	k1gInPc6
metrů	metr	k1gInPc2
mocnosti	mocnost	k1gFnSc6
ledovců	ledovec	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
Dachstein	Dachstein	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
létě	léto	k1gNnSc6
nezamrzají	zamrzat	k5eNaImIp3nP
ledovce	ledovec	k1gInPc1
ani	ani	k8xC
přes	přes	k7c4
noc	noc	k1gFnSc4
a	a	k8xC
tím	ten	k3xDgNnSc7
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
velkým	velký	k2eAgInPc3d1
úbytkům	úbytek	k1gInPc3
v	v	k7c6
důsledku	důsledek	k1gInSc6
odtávání	odtávání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
fakt	fakt	k1gInSc1
způsobuje	způsobovat	k5eAaImIp3nS
velkou	velký	k2eAgFnSc7d1
měrou	míra	k1gFnSc7wR
globální	globální	k2eAgNnSc4d1
oteplování	oteplování	k1gNnSc4
planety	planeta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Největším	veliký	k2eAgInSc7d3
ledovcem	ledovec	k1gInSc7
Alp	Alpy	k1gFnPc2
je	být	k5eAaImIp3nS
Velký	velký	k2eAgInSc1d1
Aletschský	Aletschský	k2eAgInSc1d1
ledovec	ledovec	k1gInSc1
v	v	k7c6
Bernských	bernský	k2eAgFnPc6d1
Alpách	Alpy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délka	délka	k1gFnSc1
je	být	k5eAaImIp3nS
24	#num#	k4
km	km	kA
<g/>
,	,	kIx,
plocha	plocha	k1gFnSc1
asi	asi	k9
115	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k1gNnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
pod	pod	k7c7
vrcholem	vrchol	k1gInSc7
Aletschhorn	Aletschhorn	k1gInSc1
stéká	stékat	k5eAaImIp3nS
několik	několik	k4yIc4
ledovců	ledovec	k1gInPc2
v	v	k7c4
jeden	jeden	k4xCgInSc4
mohutný	mohutný	k2eAgInSc4d1
ledovcový	ledovcový	k2eAgInSc4d1
proud	proud	k1gInSc4
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
Konkordiaplatz	Konkordiaplatz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mocnost	mocnost	k1gFnSc1
ledu	led	k1gInSc2
zde	zde	k6eAd1
dosahuje	dosahovat	k5eAaImIp3nS
kolem	kolem	k7c2
350	#num#	k4
m.	m.	k?
</s>
<s>
Další	další	k2eAgInPc1d1
velké	velký	k2eAgInPc1d1
ledovce	ledovec	k1gInPc1
v	v	k7c6
Alpách	Alpy	k1gFnPc6
jsou	být	k5eAaImIp3nP
Gornergletscher	Gornergletschra	k1gFnPc2
ve	v	k7c6
Walliských	Walliský	k2eAgFnPc6d1
Alpách	Alpy	k1gFnPc6
<g/>
(	(	kIx(
<g/>
délka	délka	k1gFnSc1
15	#num#	k4
km	km	kA
a	a	k8xC
rozloha	rozloha	k1gFnSc1
67	#num#	k4
km²	km²	k?
<g/>
)	)	kIx)
nebo	nebo	k8xC
Mer	Mer	k1gMnSc1
de	de	k?
Glace	Glace	k1gMnSc1
v	v	k7c6
Savojských	savojský	k2eAgFnPc6d1
Alpách	Alpy	k1gFnPc6
(	(	kIx(
<g/>
délka	délka	k1gFnSc1
16	#num#	k4
km	km	kA
<g/>
,	,	kIx,
rozloha	rozloha	k1gFnSc1
55	#num#	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Členění	členění	k1gNnSc1
</s>
<s>
Nejběžněji	běžně	k6eAd3
se	se	k3xPyFc4
Alpy	Alpy	k1gFnPc1
dělí	dělit	k5eAaImIp3nP
na	na	k7c4
dva	dva	k4xCgInPc4
velké	velký	k2eAgInPc4d1
celky	celek	k1gInPc4
–	–	k?
Západní	západní	k2eAgMnPc1d1
a	a	k8xC
Východní	východní	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranici	hranice	k1gFnSc4
těchto	tento	k3xDgInPc2
celků	celek	k1gInPc2
tvoří	tvořit	k5eAaImIp3nS
spojnice	spojnice	k1gFnSc1
mezi	mezi	k7c7
Bodamským	bodamský	k2eAgNnSc7d1
jezerem	jezero	k1gNnSc7
<g/>
,	,	kIx,
Rýnem	Rýn	k1gInSc7
<g/>
,	,	kIx,
sedlem	sedlo	k1gNnSc7
Splügenpass	Splügenpassa	k1gFnPc2
<g/>
,	,	kIx,
řekami	řeka	k1gFnPc7
Liro	lira	k1gFnSc5
a	a	k8xC
Mera	Merus	k1gMnSc4
a	a	k8xC
Comským	Comský	k2eAgNnSc7d1
jezerem	jezero	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západní	západní	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
zabírají	zabírat	k5eAaImIp3nP
asi	asi	k9
40	#num#	k4
<g/>
%	%	kIx~
a	a	k8xC
Východní	východní	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
60	#num#	k4
<g/>
%	%	kIx~
celkové	celkový	k2eAgFnSc2d1
rozlohy	rozloha	k1gFnSc2
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
uznávaná	uznávaný	k2eAgFnSc1d1
hranice	hranice	k1gFnPc1
mezi	mezi	k7c7
těmito	tento	k3xDgInPc7
celky	celek	k1gInPc7
nebyla	být	k5eNaImAgFnS
odjakživa	odjakživa	k6eAd1
vedena	vést	k5eAaImNgFnS
tímto	tento	k3xDgInSc7
směrem	směr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1919	#num#	k4
bylo	být	k5eAaImAgNnS
rozdělení	rozdělení	k1gNnSc1
Východních	východní	k2eAgFnPc2d1
a	a	k8xC
Západních	západní	k2eAgFnPc2d1
Alp	Alpy	k1gFnPc2
podle	podle	k7c2
politické	politický	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšším	vysoký	k2eAgInSc7d3
vrcholem	vrchol	k1gInSc7
tehdejších	tehdejší	k2eAgFnPc2d1
Východních	východní	k2eAgFnPc2d1
Alp	Alpy	k1gFnPc2
byl	být	k5eAaImAgInS
Ortler	Ortler	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
se	se	k3xPyFc4
totiž	totiž	k9
nacházela	nacházet	k5eAaImAgFnS
na	na	k7c4
východ	východ	k1gInSc4
od	od	k7c2
švýcarských	švýcarský	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členění	členění	k1gNnPc1
do	do	k7c2
jednotlivých	jednotlivý	k2eAgNnPc2d1
pohoří	pohoří	k1gNnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
literatuře	literatura	k1gFnSc6
nejednotné	jednotný	k2eNgFnSc6d1
a	a	k8xC
mnohdy	mnohdy	k6eAd1
zcela	zcela	k6eAd1
rozdílné	rozdílný	k2eAgFnPc1d1
nebo	nebo	k8xC
až	až	k6eAd1
chaotické	chaotický	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
mnoha	mnoho	k4c2
skupin	skupina	k1gFnPc2
jsou	být	k5eAaImIp3nP
osamostatněny	osamostatněn	k2eAgFnPc1d1
jejich	jejich	k3xOp3gFnPc1
významné	významný	k2eAgFnPc1d1
části	část	k1gFnPc1
(	(	kIx(
<g/>
Savojské	savojský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
a	a	k8xC
Mont	Mont	k1gMnSc1
Blanc	Blanc	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
řady	řada	k1gFnSc2
horských	horský	k2eAgInPc2d1
masivů	masiv	k1gInPc2
se	se	k3xPyFc4
zase	zase	k9
oddělují	oddělovat	k5eAaImIp3nP
tzv.	tzv.	kA
Voralpen	Voralpen	k2eAgMnSc1d1
(	(	kIx(
<g/>
alpská	alpský	k2eAgNnPc4d1
předhůří	předhůří	k1gNnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranice	hranice	k1gFnSc1
těchto	tento	k3xDgNnPc2
předhůří	předhůří	k1gNnPc2
(	(	kIx(
<g/>
Bavorské	bavorský	k2eAgNnSc1d1
předhůří	předhůří	k1gNnSc1
<g/>
,	,	kIx,
Hornorakouské	hornorakouský	k2eAgNnSc1d1
předhůří	předhůří	k1gNnSc1
apod.	apod.	kA
<g/>
)	)	kIx)
nejsou	být	k5eNaImIp3nP
pevně	pevně	k6eAd1
vytyčeny	vytyčit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgInPc1d3
vrcholy	vrchol	k1gInPc1
systému	systém	k1gInSc2
se	se	k3xPyFc4
až	až	k9
na	na	k7c4
výjimky	výjimka	k1gFnPc4
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
Západních	západní	k2eAgFnPc6d1
Alpách	Alpy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrcholů	vrchol	k1gInPc2
dosahujících	dosahující	k2eAgInPc2d1
výšky	výška	k1gFnPc4
4	#num#	k4
000	#num#	k4
m	m	kA
je	být	k5eAaImIp3nS
60	#num#	k4
<g/>
,	,	kIx,
ovšem	ovšem	k9
pouze	pouze	k6eAd1
jeden	jeden	k4xCgMnSc1
(	(	kIx(
<g/>
Piz	Piz	k1gFnSc1
Bernina	Bernina	k1gFnSc1
<g/>
,	,	kIx,
4	#num#	k4
049	#num#	k4
m	m	kA
<g/>
)	)	kIx)
náleží	náležet	k5eAaImIp3nS
do	do	k7c2
Východních	východní	k2eAgFnPc2d1
Alp	Alpy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Západní	západní	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
</s>
<s>
Do	do	k7c2
tohoto	tento	k3xDgInSc2
celku	celek	k1gInSc2
patří	patřit	k5eAaImIp3nP
takřka	takřka	k6eAd1
celé	celý	k2eAgFnPc1d1
Švýcarské	švýcarský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
<g/>
,	,	kIx,
některé	některý	k3yIgInPc4
části	část	k1gFnPc1
Italských	italský	k2eAgFnPc2d1
Alp	Alpy	k1gFnPc2
a	a	k8xC
Francouzské	francouzský	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblouk	oblouk	k1gInSc1
Západních	západní	k2eAgFnPc2d1
Alp	Alpy	k1gFnPc2
je	být	k5eAaImIp3nS
otevřený	otevřený	k2eAgInSc1d1
k	k	k7c3
východu	východ	k1gInSc3
a	a	k8xC
obklopuje	obklopovat	k5eAaImIp3nS
severoitalské	severoitalský	k2eAgFnSc2d1
nížiny	nížina	k1gFnSc2
Piemontskou	Piemontský	k2eAgFnSc7d1
a	a	k8xC
Lombardskou	lombardský	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc4
pohoří	pohoří	k1gNnPc2
leží	ležet	k5eAaImIp3nP
západně	západně	k6eAd1
od	od	k7c2
linie	linie	k1gFnSc2
Bodamské	bodamský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
–	–	k?
horní	horní	k2eAgInSc4d1
tok	tok	k1gInSc4
Rýna	Rýn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecně	obecně	k6eAd1
lze	lze	k6eAd1
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
hory	hora	k1gFnPc1
Západních	západní	k2eAgFnPc2d1
Alp	Alpy	k1gFnPc2
jsou	být	k5eAaImIp3nP
vyšší	vysoký	k2eAgInPc1d2
a	a	k8xC
mají	mít	k5eAaImIp3nP
strmější	strmý	k2eAgInPc1d2
svahy	svah	k1gInPc1
s	s	k7c7
velkými	velký	k2eAgInPc7d1
výškovými	výškový	k2eAgInPc7d1
rozdíly	rozdíl	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nalézají	nalézat	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
nejdelší	dlouhý	k2eAgFnPc1d3
a	a	k8xC
nejrozsáhlejší	rozsáhlý	k2eAgInPc1d3
alpské	alpský	k2eAgInPc1d1
ledovce	ledovec	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mont	Mont	k1gMnSc1
Blanc	Blanc	k1gMnSc1
na	na	k7c6
italsko-francouzských	italsko-francouzský	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
je	být	k5eAaImIp3nS
se	s	k7c7
svými	svůj	k3xOyFgInPc7
4	#num#	k4
810	#num#	k4
metry	metr	k1gInPc4
nejvyšším	vysoký	k2eAgInSc7d3
vrcholem	vrchol	k1gInSc7
Alp	Alpy	k1gFnPc2
a	a	k8xC
zároveň	zároveň	k6eAd1
celé	celý	k2eAgFnSc2d1
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgInPc7d1
významnými	významný	k2eAgInPc7d1
vrcholy	vrchol	k1gInPc7
jsou	být	k5eAaImIp3nP
např.	např.	kA
Matterhorn	Matterhorn	k1gInSc4
na	na	k7c6
švýcarsko-italských	švýcarsko-italský	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
či	či	k8xC
Jungfrau	Jungfrau	k1gFnPc6
a	a	k8xC
Eiger	Eiger	k1gInSc4
v	v	k7c6
Bernských	bernský	k2eAgFnPc6d1
Alpách	Alpy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Západní	západní	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nP
na	na	k7c4
sedmnáct	sedmnáct	k4xCc4
hlavních	hlavní	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
:	:	kIx,
</s>
<s>
Grajské	Grajský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
<g/>
,	,	kIx,
Gran	Gran	k1gInSc1
Paradiso	Paradisa	k1gFnSc5
</s>
<s>
název	název	k1gInSc1
</s>
<s>
plocha	plocha	k1gFnSc1
(	(	kIx(
<g/>
km²	km²	k?
<g/>
)	)	kIx)
</s>
<s>
Ligurské	ligurský	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
4	#num#	k4
500	#num#	k4
</s>
<s>
Přímořské	přímořský	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
7	#num#	k4
000	#num#	k4
</s>
<s>
Provensalské	Provensalský	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
9	#num#	k4
300	#num#	k4
</s>
<s>
Kottické	Kottický	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
6	#num#	k4
900	#num#	k4
</s>
<s>
Dauphineské	Dauphineský	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
2	#num#	k4
400	#num#	k4
</s>
<s>
Grajské	Grajský	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
3	#num#	k4
200	#num#	k4
</s>
<s>
Vanoise	Vanoise	k1gFnSc1
<g/>
1	#num#	k4
800	#num#	k4
</s>
<s>
Mont	Mont	k2eAgMnSc1d1
Blanc	Blanc	k1gMnSc1
<g/>
1	#num#	k4
500	#num#	k4
</s>
<s>
Jura	jura	k1gFnSc1
<g/>
4	#num#	k4
200	#num#	k4
</s>
<s>
Walliské	Walliské	k1gNnSc1
Alpy	alpa	k1gFnSc2
<g/>
8	#num#	k4
300	#num#	k4
</s>
<s>
Freiburské	freiburský	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
2	#num#	k4
300	#num#	k4
</s>
<s>
Bernské	bernský	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
3	#num#	k4
900	#num#	k4
</s>
<s>
Tessinské	Tessinský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
<g/>
—	—	k?
</s>
<s>
Adulské	Adulský	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
2	#num#	k4
100	#num#	k4
</s>
<s>
Urnské	Urnské	k1gNnSc1
Alpy	alpa	k1gFnSc2
<g/>
1	#num#	k4
300	#num#	k4
</s>
<s>
Glarnské	Glarnský	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
2	#num#	k4
300	#num#	k4
</s>
<s>
Appenzellské	Appenzellský	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
2	#num#	k4
100	#num#	k4
</s>
<s>
Gotthard	Gotthard	k1gInSc1
<g/>
1	#num#	k4
100	#num#	k4
</s>
<s>
Východní	východní	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
</s>
<s>
Východní	východní	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
z	z	k7c2
celků	celek	k1gInPc2
:	:	kIx,
Rakouské	rakouský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
<g/>
,	,	kIx,
Italské	italský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
<g/>
,	,	kIx,
Slovinské	slovinský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
a	a	k8xC
malou	malý	k2eAgFnSc7d1
částí	část	k1gFnSc7
i	i	k9
Švýcarské	švýcarský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sněžná	sněžný	k2eAgFnSc1d1
čára	čára	k1gFnSc1
Východních	východní	k2eAgFnPc2d1
Alp	Alpy	k1gFnPc2
dosahuje	dosahovat	k5eAaImIp3nS
2	#num#	k4
800	#num#	k4
metrů	metr	k1gInPc2
na	na	k7c6
severním	severní	k2eAgInSc6d1
a	a	k8xC
2	#num#	k4
900	#num#	k4
metrů	metr	k1gInPc2
na	na	k7c6
jižním	jižní	k2eAgInSc6d1
svahu	svah	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
části	část	k1gFnSc6
dosahují	dosahovat	k5eAaImIp3nP
nejvyšších	vysoký	k2eAgInPc2d3
vrcholů	vrchol	k1gInPc2
Ötztaler	Ötztaler	k1gMnSc1
Alpen	Alpen	k1gInSc1
(	(	kIx(
<g/>
Wildspitze	Wildspitze	k1gFnSc1
3	#num#	k4
772	#num#	k4
m	m	kA
<g/>
)	)	kIx)
sahající	sahající	k2eAgInSc4d1
k	k	k7c3
švýcarským	švýcarský	k2eAgFnPc3d1
a	a	k8xC
italským	italský	k2eAgFnPc3d1
hranicím	hranice	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
této	tento	k3xDgFnSc2
oblasti	oblast	k1gFnSc2
naštěstí	naštěstí	k6eAd1
zasáhl	zasáhnout	k5eAaPmAgInS
rozmach	rozmach	k1gInSc1
turistiky	turistika	k1gFnSc2
o	o	k7c4
něco	něco	k3yInSc4
citlivěji	citlivě	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
takže	takže	k8xS
na	na	k7c6
ploše	plocha	k1gFnSc6
kolem	kolem	k7c2
500	#num#	k4
km²	km²	k?
narazíme	narazit	k5eAaPmIp1nP
jen	jen	k9
zřídka	zřídka	k6eAd1
na	na	k7c4
známky	známka	k1gFnPc4
lidské	lidský	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
přehradní	přehradní	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
Gepatsch	Gepatscha	k1gFnPc2
<g/>
,	,	kIx,
několik	několik	k4yIc4
sjezdovek	sjezdovka	k1gFnPc2
a	a	k8xC
malé	malý	k2eAgFnPc4d1
vesničky	vesnička	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
západě	západ	k1gInSc6
jsou	být	k5eAaImIp3nP
ohraničeny	ohraničit	k5eAaPmNgInP
Innem	Innum	k1gNnSc7
a	a	k8xC
na	na	k7c6
východě	východ	k1gInSc6
údolím	údolí	k1gNnSc7
Ötztal	Ötztal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c7
ním	on	k3xPp3gNnSc7
pak	pak	k6eAd1
navazují	navazovat	k5eAaImIp3nP
Stubaier	Stubaier	k1gInSc4
Alpen	Alpen	k1gInSc1
(	(	kIx(
<g/>
Zuckerhütl	Zuckerhütl	k1gFnSc1
3	#num#	k4
507	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
jsou	být	k5eAaImIp3nP
rovněž	rovněž	k9
panenské	panenský	k2eAgFnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
Brennerský	Brennerský	k2eAgInSc1d1
průsmyk	průsmyk	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
371	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
je	on	k3xPp3gNnSc4
ohraničuje	ohraničovat	k5eAaImIp3nS
z	z	k7c2
východu	východ	k1gInSc2
dnes	dnes	k6eAd1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
dopravní	dopravní	k2eAgFnSc1d1
tepna	tepna	k1gFnSc1
z	z	k7c2
Innsbrucku	Innsbruck	k1gInSc2
do	do	k7c2
italského	italský	k2eAgMnSc2d1
Bolzana	Bolzan	k1gMnSc2
<g/>
,	,	kIx,
čili	čili	k8xC
ideální	ideální	k2eAgNnSc4d1
místo	místo	k1gNnSc4
na	na	k7c4
využití	využití	k1gNnSc4
svahů	svah	k1gInPc2
pro	pro	k7c4
sportovní	sportovní	k2eAgInPc4d1
a	a	k8xC
rekreační	rekreační	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
průsmyku	průsmyk	k1gInSc6
vedla	vést	k5eAaImAgFnS
už	už	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1772	#num#	k4
silnice	silnice	k1gFnSc2
<g/>
,	,	kIx,
1867	#num#	k4
železnice	železnice	k1gFnSc2
a	a	k8xC
dnes	dnes	k6eAd1
dálnice	dálnice	k1gFnSc1
A	a	k9
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
na	na	k7c4
východ	východ	k1gInSc4
pokračují	pokračovat	k5eAaImIp3nP
Zillertalské	Zillertalský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
s	s	k7c7
kvalitními	kvalitní	k2eAgInPc7d1
lyžařskými	lyžařský	k2eAgInPc7d1
areály	areál	k1gInPc7
v	v	k7c6
okolí	okolí	k1gNnSc6
Mayrhofenu	Mayrhofen	k1gInSc2
nebo	nebo	k8xC
na	na	k7c6
severním	severní	k2eAgInSc6d1
svahu	svah	k1gInSc6
hory	hora	k1gFnSc2
Olperer	Olperra	k1gFnPc2
(	(	kIx(
<g/>
3	#num#	k4
476	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
sjezdové	sjezdový	k2eAgFnPc1d1
tratě	trať	k1gFnPc1
jsou	být	k5eAaImIp3nP
až	až	k9
12	#num#	k4
km	km	kA
dlouhé	dlouhý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Vnějších	vnější	k2eAgFnPc6d1
Alpách	Alpy	k1gFnPc6
se	se	k3xPyFc4
s	s	k7c7
místy	místo	k1gNnPc7
věčného	věčný	k2eAgInSc2d1
sněhu	sníh	k1gInSc2
nesetkáváme	setkávat	k5eNaImIp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
mezi	mezi	k7c7
řekami	řeka	k1gFnPc7
Inn	Inn	k1gFnSc1
<g/>
,	,	kIx,
Rosanna	Rosanna	k1gFnSc1
a	a	k8xC
Lech	Lech	k1gMnSc1
se	se	k3xPyFc4
táhne	táhnout	k5eAaImIp3nS
v	v	k7c6
délce	délka	k1gFnSc6
40	#num#	k4
kilometrů	kilometr	k1gInPc2
romantické	romantický	k2eAgNnSc4d1
horské	horský	k2eAgNnSc4d1
pásmo	pásmo	k1gNnSc4
Lechtaler	Lechtaler	k1gInSc4
Alpen	Alpen	k2eAgInSc4d1
s	s	k7c7
nejvyšší	vysoký	k2eAgFnSc7d3
horou	hora	k1gFnSc7
Parseierspitze	Parseierspitze	k1gFnSc2
(	(	kIx(
<g/>
3	#num#	k4
036	#num#	k4
m	m	kA
<g/>
)	)	kIx)
nad	nad	k7c7
městečkem	městečko	k1gNnSc7
Landeck	Landecka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horská	horský	k2eAgFnSc1d1
silnice	silnice	k1gFnSc1
mezi	mezi	k7c7
Imstem	Imst	k1gMnSc7
a	a	k8xC
Elmenem	Elmen	k1gMnSc7
překonává	překonávat	k5eAaImIp3nS
hřeben	hřeben	k1gInSc4
v	v	k7c6
sedle	sedlo	k1gNnSc6
Hahntennjoch	Hahntennjoch	k1gInSc1
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
1	#num#	k4
905	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc1d1
hřeben	hřeben	k1gInSc1
Východních	východní	k2eAgFnPc2d1
Alp	Alpy	k1gFnPc2
dosahuje	dosahovat	k5eAaImIp3nS
délky	délka	k1gFnPc4
takřka	takřka	k6eAd1
500	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s>
Východní	východní	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nP
na	na	k7c4
66	#num#	k4
horských	horský	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
:	:	kIx,
</s>
<s>
Totes	Totes	k1gInSc1
Gebirge	Gebirg	k1gFnSc2
<g/>
,	,	kIx,
Grosser	Grosser	k1gMnSc1
Priel	Priel	k1gMnSc1
</s>
<s>
Obec	obec	k1gFnSc1
Tux	Tux	k1gFnSc2
v	v	k7c6
Tuxských	Tuxský	k2eAgFnPc6d1
Alpách	Alpy	k1gFnPc6
v	v	k7c6
Tyrolsku	Tyrolsko	k1gNnSc6
</s>
<s>
název	název	k1gInSc1
</s>
<s>
plocha	plocha	k1gFnSc1
(	(	kIx(
<g/>
km²	km²	k?
<g/>
)	)	kIx)
</s>
<s>
název	název	k1gInSc1
</s>
<s>
plocha	plocha	k1gFnSc1
(	(	kIx(
<g/>
km²	km²	k?
<g/>
)	)	kIx)
</s>
<s>
Bregenzský	Bregenzský	k2eAgInSc1d1
les	les	k1gInSc1
<g/>
700	#num#	k4
<g/>
Sesvenna	Sesvenn	k1gInSc2
<g/>
600	#num#	k4
</s>
<s>
Lechquellengebirge	Lechquellengebirg	k1gInSc2
<g/>
800	#num#	k4
<g/>
Livigno	Livigno	k6eAd1
<g/>
1	#num#	k4
100	#num#	k4
</s>
<s>
Verwall	Verwalla	k1gFnPc2
<g/>
900	#num#	k4
<g/>
Bernina	Bernin	k2eAgFnSc1d1
<g/>
1	#num#	k4
500	#num#	k4
</s>
<s>
Allgäuské	Allgäuský	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
2	#num#	k4
800	#num#	k4
<g/>
Ortles	Ortles	k1gInSc1
<g/>
3	#num#	k4
000	#num#	k4
</s>
<s>
Lechtalské	Lechtalský	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
1	#num#	k4
150	#num#	k4
<g/>
Bergamské	Bergamský	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
<g/>
3	#num#	k4
500	#num#	k4
</s>
<s>
Samnaunské	Samnaunský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
<g/>
550	#num#	k4
<g/>
Adamello-Presanella	Adamello-Presanello	k1gNnSc2
<g/>
2	#num#	k4
000	#num#	k4
</s>
<s>
Ammergauské	Ammergauský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
<g/>
—	—	k?
<g/>
Ötztalské	Ötztalský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
<g/>
2	#num#	k4
850	#num#	k4
</s>
<s>
Wetterstein	Wetterstein	k1gInSc1
<g/>
700	#num#	k4
<g/>
Stubaiské	Stubaiská	k1gFnSc2
Alpy	alpa	k1gFnSc2
<g/>
2	#num#	k4
100	#num#	k4
</s>
<s>
Mieminger	Miemingrat	k5eAaPmRp2nS
Kette	Kett	k1gMnSc5
<g/>
—	—	k?
<g/>
Sarntalské	Sarntalský	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
<g/>
1	#num#	k4
150	#num#	k4
</s>
<s>
Bavorské	bavorský	k2eAgNnSc1d1
předhůří	předhůří	k1gNnSc1
<g/>
—	—	k?
<g/>
Zillertalské	Zillertalský	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
<g/>
2	#num#	k4
050	#num#	k4
</s>
<s>
Karwendel	Karwendlo	k1gNnPc2
<g/>
1	#num#	k4
150	#num#	k4
<g/>
Tuxské	Tuxský	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
<g/>
800	#num#	k4
</s>
<s>
Rofan	Rofan	k1gInSc1
<g/>
350	#num#	k4
<g/>
Kitzbühelské	Kitzbühelský	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
<g/>
1	#num#	k4
900	#num#	k4
</s>
<s>
Chiemgauské	Chiemgauský	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
1	#num#	k4
100	#num#	k4
<g/>
Vysoké	vysoký	k2eAgFnSc2d1
Taury	Taura	k1gFnSc2
<g/>
4	#num#	k4
950	#num#	k4
</s>
<s>
Kaisergebirge	Kaisergebirge	k6eAd1
<g/>
350	#num#	k4
<g/>
Nízké	nízký	k2eAgInPc1d1
Taury	Taur	k1gInPc1
<g/>
4	#num#	k4
900	#num#	k4
</s>
<s>
Steinberge	Steinberge	k6eAd1
<g/>
350	#num#	k4
<g/>
Rieserferner	Rieserfernra	k1gFnPc2
<g/>
300	#num#	k4
</s>
<s>
Berchtesgadenské	Berchtesgadenský	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
1	#num#	k4
300	#num#	k4
<g/>
Villgratenské	Villgratenský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
<g/>
850	#num#	k4
</s>
<s>
Dientener	Dientener	k1gInSc1
Berge	Berg	k1gFnSc2
<g/>
250	#num#	k4
<g/>
Nockberge	Nockberge	k1gFnSc1
<g/>
3	#num#	k4
900	#num#	k4
</s>
<s>
Salzkammergutberge	Salzkammergutberge	k6eAd1
<g/>
1	#num#	k4
750	#num#	k4
<g/>
Lavanttalské	Lavanttalský	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
<g/>
4	#num#	k4
400	#num#	k4
</s>
<s>
Tennengebirge	Tennengebirge	k1gInSc1
<g/>
300	#num#	k4
<g/>
Hory	hora	k1gFnSc2
východně	východně	k6eAd1
od	od	k7c2
řeky	řeka	k1gFnSc2
Mury	mura	k1gFnSc2
<g/>
—	—	k?
</s>
<s>
Dachstein	Dachstein	k1gInSc1
<g/>
900	#num#	k4
<g/>
Brenta	Brent	k1gInSc2
<g/>
700	#num#	k4
</s>
<s>
Hornorakouské	hornorakouský	k2eAgNnSc1d1
předhůří	předhůří	k1gNnSc1
<g/>
1	#num#	k4
850	#num#	k4
<g/>
Gardské	Gardský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
<g/>
2	#num#	k4
400	#num#	k4
</s>
<s>
Totes	Totes	k1gInSc1
Gebirge	Gebirg	k1gFnSc2
<g/>
1	#num#	k4
250	#num#	k4
<g/>
Dolomity	Dolomity	k1gInPc1
<g/>
4	#num#	k4
750	#num#	k4
</s>
<s>
Ennstalské	Ennstalský	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
1	#num#	k4
300	#num#	k4
<g/>
Fleimstalské	Fleimstalský	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
<g/>
1	#num#	k4
650	#num#	k4
</s>
<s>
Hochschwab	Hochschwab	k1gInSc1
<g/>
1	#num#	k4
0	#num#	k4
<g/>
50	#num#	k4
<g/>
Vicentinské	Vicentinský	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
<g/>
3	#num#	k4
000	#num#	k4
</s>
<s>
Ybbstalské	Ybbstalský	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
1	#num#	k4
750	#num#	k4
<g/>
Karnské	Karnský	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
<g/>
6	#num#	k4
600	#num#	k4
</s>
<s>
Türnitzské	Türnitzský	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
1	#num#	k4
100	#num#	k4
<g/>
Gailtalské	Gailtalský	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
<g/>
1	#num#	k4
300	#num#	k4
</s>
<s>
Mürzstegské	Mürzstegský	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
850	#num#	k4
<g/>
Julské	Julská	k1gFnSc2
Alpy	alpa	k1gFnSc2
<g/>
5	#num#	k4
300	#num#	k4
</s>
<s>
Rax	Rax	k?
<g/>
450	#num#	k4
<g/>
Karavanky	karavanka	k1gFnSc2
<g/>
1	#num#	k4
300	#num#	k4
</s>
<s>
Gutensteinské	Gutensteinský	k2eAgFnPc4d1
Alpy	Alpy	k1gFnPc4
<g/>
1	#num#	k4
300	#num#	k4
<g/>
Kamnicko-Savinjské	Kamnicko-Savinjský	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
<g/>
900	#num#	k4
</s>
<s>
Vídeňský	vídeňský	k2eAgInSc1d1
les	les	k1gInSc1
<g/>
1	#num#	k4
200	#num#	k4
<g/>
Pohorje	Pohorj	k1gInSc2
<g/>
900	#num#	k4
</s>
<s>
Platta	Platt	k1gInSc2
<g/>
700	#num#	k4
<g/>
Plessurské	Plessurský	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
<g/>
900	#num#	k4
</s>
<s>
Albula	Albula	k1gFnSc1
<g/>
1	#num#	k4
600	#num#	k4
<g/>
Rätikon	Rätikona	k1gFnPc2
<g/>
900	#num#	k4
</s>
<s>
Silvretta	Silvrett	k1gInSc2
<g/>
900	#num#	k4
<g/>
Tessinské	Tessinský	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
<g/>
2	#num#	k4
600	#num#	k4
</s>
<s>
Vrcholy	vrchol	k1gInPc1
</s>
<s>
Následující	následující	k2eAgFnPc1d1
tabulky	tabulka	k1gFnPc1
obsahují	obsahovat	k5eAaImIp3nP
seznam	seznam	k1gInSc4
10	#num#	k4
nejvýznamnějších	významný	k2eAgInPc2d3
alpských	alpský	k2eAgInPc2d1
vrcholů	vrchol	k1gInPc2
podle	podle	k7c2
výšky	výška	k1gFnSc2
a	a	k8xC
prominence	prominence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nejvyšší	vysoký	k2eAgInPc4d3
vrcholy	vrchol	k1gInPc4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Alpské	alpský	k2eAgFnSc2d1
čtyřtisícovky	čtyřtisícovka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Výška	výška	k1gFnSc1
</s>
<s>
Prominence	prominence	k1gFnSc1
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Pohoří	pohoří	k1gNnSc1
</s>
<s>
Mont	Mont	k2eAgMnSc1d1
Blanc	Blanc	k1gMnSc1
<g/>
4	#num#	k4
8104	#num#	k4
697	#num#	k4
<g/>
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
ItálieMasiv	ItálieMasiv	k1gMnSc1
Mont	Mont	k1gMnSc1
Blanc	Blanc	k1gMnSc1
</s>
<s>
Dufourspitze	Dufourspitze	k6eAd1
<g/>
4	#num#	k4
6342	#num#	k4
165	#num#	k4
<g/>
Švýcarsko	Švýcarsko	k1gNnSc4
<g/>
,	,	kIx,
ItálieWalliské	ItálieWalliský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
</s>
<s>
Nordend	Nordend	k1gInSc1
<g/>
4	#num#	k4
6090	#num#	k4
0	#num#	k4
<g/>
94	#num#	k4
<g/>
ŠvýcarskoWalliské	ŠvýcarskoWalliský	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
</s>
<s>
Zumsteinspitze	Zumsteinspitze	k6eAd1
<g/>
4	#num#	k4
5630	#num#	k4
110	#num#	k4
<g/>
Švýcarsko	Švýcarsko	k1gNnSc4
<g/>
,	,	kIx,
ItálieWalliské	ItálieWalliský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
</s>
<s>
Signalkuppe	Signalkuppat	k5eAaPmIp3nS
<g/>
4	#num#	k4
5560	#num#	k4
102	#num#	k4
<g/>
Švýcarsko	Švýcarsko	k1gNnSc4
<g/>
,	,	kIx,
ItálieWalliské	ItálieWalliský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
</s>
<s>
Dom	Dom	k?
<g/>
4	#num#	k4
5451	#num#	k4
0	#num#	k4
<g/>
46	#num#	k4
<g/>
ŠvýcarskoWalliské	ŠvýcarskoWalliský	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
</s>
<s>
Lyskamm	Lyskamm	k6eAd1
<g/>
4	#num#	k4
5270	#num#	k4
376	#num#	k4
<g/>
Švýcarsko	Švýcarsko	k1gNnSc4
<g/>
,	,	kIx,
ItálieWalliské	ItálieWalliský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
</s>
<s>
Weisshorn	Weisshorn	k1gInSc1
<g/>
4	#num#	k4
5051	#num#	k4
235	#num#	k4
<g/>
ŠvýcarskoWalliské	ŠvýcarskoWalliský	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
</s>
<s>
Täschhorn	Täschhorn	k1gInSc1
<g/>
4	#num#	k4
4910	#num#	k4
210	#num#	k4
<g/>
ŠvýcarskoWalliské	ŠvýcarskoWalliský	k2eAgFnSc2d1
Alpy	alpa	k1gFnSc2
</s>
<s>
Matterhorn	Matterhorn	k1gInSc1
<g/>
4	#num#	k4
4781	#num#	k4
0	#num#	k4
<g/>
40	#num#	k4
<g/>
Švýcarsko	Švýcarsko	k1gNnSc4
<g/>
,	,	kIx,
ItálieWalliské	ItálieWalliský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
</s>
<s>
Nejprominentnější	prominentní	k2eAgInPc1d3
vrcholy	vrchol	k1gInPc1
</s>
<s>
Wildspitze	Wildspitze	k6eAd1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
vrcholů	vrchol	k1gInPc2
v	v	k7c6
Alpách	Alpy	k1gFnPc6
podle	podle	k7c2
prominence	prominence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Prominence	prominence	k1gFnSc1
</s>
<s>
Výška	výška	k1gFnSc1
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Mateřský	mateřský	k2eAgInSc1d1
vrchol	vrchol	k1gInSc1
</s>
<s>
Mont	Mont	k2eAgMnSc1d1
Blanc	Blanc	k1gMnSc1
<g/>
4	#num#	k4
6974	#num#	k4
810	#num#	k4
<g/>
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
ItálieMount	ItálieMount	k1gInSc1
Everest	Everest	k1gInSc1
</s>
<s>
Grossglockner	Grossglockner	k1gInSc1
<g/>
2	#num#	k4
4233	#num#	k4
798	#num#	k4
<g/>
RakouskoMont	RakouskoMont	k1gMnSc1
Blanc	Blanc	k1gMnSc1
</s>
<s>
Finsteraarhorn	Finsteraarhorn	k1gInSc1
<g/>
2	#num#	k4
2804	#num#	k4
274	#num#	k4
<g/>
ŠvýcarskoMont	ŠvýcarskoMont	k1gMnSc1
Blanc	Blanc	k1gMnSc1
</s>
<s>
Wildspitze	Wildspitze	k6eAd1
<g/>
2	#num#	k4
2613	#num#	k4
768	#num#	k4
<g/>
RakouskoFinsteraarhorn	RakouskoFinsteraarhorna	k1gFnPc2
</s>
<s>
Piz	Piz	k?
Bernina	Bernina	k1gFnSc1
<g/>
2	#num#	k4
2344	#num#	k4
0	#num#	k4
<g/>
49	#num#	k4
<g/>
ŠvýcarskoFinsteraarhorn	ŠvýcarskoFinsteraarhorna	k1gFnPc2
</s>
<s>
Hochkönig	Hochkönig	k1gInSc1
<g/>
2	#num#	k4
1812	#num#	k4
941	#num#	k4
<g/>
RakouskoGrossglockner	RakouskoGrossglocknra	k1gFnPc2
</s>
<s>
Dufourspitze	Dufourspitze	k6eAd1
<g/>
2	#num#	k4
1654	#num#	k4
634	#num#	k4
<g/>
Švýcarsko	Švýcarsko	k1gNnSc4
<g/>
,	,	kIx,
ItálieMont	ItálieMont	k1gMnSc1
Blanc	Blanc	k1gMnSc1
</s>
<s>
Hoher	Hoher	k1gMnSc1
Dachstein	Dachstein	k1gMnSc1
<g/>
2	#num#	k4
1362	#num#	k4
995	#num#	k4
<g/>
RakouskoGrossglockner	RakouskoGrossglocknra	k1gFnPc2
</s>
<s>
Marmolada	Marmolada	k1gFnSc1
<g/>
2	#num#	k4
1313	#num#	k4
343	#num#	k4
<g/>
ItálieGrossglockner	ItálieGrossglocknra	k1gFnPc2
</s>
<s>
Monte	Monte	k5eAaPmIp2nP
Viso	viso	k1gNnSc4
<g/>
2	#num#	k4
0623	#num#	k4
841	#num#	k4
<g/>
ItálieMont	ItálieMont	k1gMnSc1
Blanc	Blanc	k1gMnSc1
</s>
<s>
Podnebí	podnebí	k1gNnSc1
</s>
<s>
Alpy	Alpy	k1gFnPc4
jsou	být	k5eAaImIp3nP
rozhraním	rozhraní	k1gNnSc7
klimat	klima	k1gNnPc2
středoevropského	středoevropský	k2eAgNnSc2d1
podnebí	podnebí	k1gNnSc2
a	a	k8xC
suchého	suchý	k2eAgNnSc2d1
podnebí	podnebí	k1gNnSc2
Panonské	panonský	k2eAgFnSc2d1
nížiny	nížina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
horách	hora	k1gFnPc6
hrají	hrát	k5eAaImIp3nP
roli	role	k1gFnSc4
faktory	faktor	k1gInPc1
nadmořské	nadmořský	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
<g/>
,	,	kIx,
návětrné	návětrný	k2eAgFnSc2d1
a	a	k8xC
závětrné	závětrný	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnPc1d3
roční	roční	k2eAgFnPc1d1
srážky	srážka	k1gFnPc1
spadnou	spadnout	k5eAaPmIp3nP
v	v	k7c6
průměru	průměr	k1gInSc6
v	v	k7c6
Julských	Julský	k2eAgFnPc6d1
Alpách	Alpy	k1gFnPc6
(	(	kIx(
<g/>
2500	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejnižší	nízký	k2eAgFnSc1d3
potom	potom	k6eAd1
ve	v	k7c6
Walliských	Walliský	k2eAgFnPc6d1
Alpách	Alpy	k1gFnPc6
(	(	kIx(
<g/>
500	#num#	k4
–	–	k?
600	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
výškou	výška	k1gFnSc7
3000	#num#	k4
m	m	kA
jsou	být	k5eAaImIp3nP
průměrné	průměrný	k2eAgFnPc1d1
roční	roční	k2eAgFnPc1d1
srážky	srážka	k1gFnPc1
v	v	k7c6
Alpách	Alpy	k1gFnPc6
téměř	téměř	k6eAd1
1500	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
přibývající	přibývající	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
klesá	klesat	k5eAaImIp3nS
teplota	teplota	k1gFnSc1
a	a	k8xC
to	ten	k3xDgNnSc1
zhruba	zhruba	k6eAd1
o	o	k7c4
jeden	jeden	k4xCgInSc4
stupeň	stupeň	k1gInSc4
na	na	k7c4
každých	každý	k3xTgFnPc2
+170	+170	k4
m	m	kA
výšky	výška	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alpy	Alpy	k1gFnPc1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
pásu	pás	k1gInSc6
pouhých	pouhý	k2eAgInPc2d1
5	#num#	k4
stupňů	stupeň	k1gInPc2
zeměpisné	zeměpisný	k2eAgFnSc2d1
šířky	šířka	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
setkáme	setkat	k5eAaPmIp1nP
se	se	k3xPyFc4
tady	tady	k6eAd1
prakticky	prakticky	k6eAd1
se	s	k7c7
všemi	všecek	k3xTgInPc7
druhy	druh	k1gInPc4
klimatu	klima	k1gNnSc2
od	od	k7c2
mírného	mírný	k2eAgNnSc2d1
podnebí	podnebí	k1gNnSc2
až	až	k9
po	po	k7c4
arktické	arktický	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Alpách	Alpy	k1gFnPc6
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
také	také	k9
padavé	padavý	k2eAgInPc4d1
větry	vítr	k1gInPc4
<g/>
,	,	kIx,
takzvané	takzvaný	k2eAgInPc4d1
fény	fén	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
Alp	Alpy	k1gFnPc2
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
jižní	jižní	k2eAgInPc1d1
fény	fén	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
Alpy	Alpy	k1gFnPc4
se	se	k3xPyFc4
přenášejí	přenášet	k5eAaImIp3nP
vzduchové	vzduchový	k2eAgFnPc1d1
vrstvy	vrstva	k1gFnPc1
od	od	k7c2
Středozemního	středozemní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižní	jižní	k2eAgInPc1d1
svahy	svah	k1gInPc1
jsou	být	k5eAaImIp3nP
svlažovány	svlažovat	k5eAaImNgInP
dešti	dešť	k1gInPc7
<g/>
,	,	kIx,
získávají	získávat	k5eAaImIp3nP
tak	tak	k9
kondenzační	kondenzační	k2eAgNnSc4d1
teplo	teplo	k1gNnSc4
a	a	k8xC
padají	padat	k5eAaImIp3nP
<g/>
,	,	kIx,
na	na	k7c4
severní	severní	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
hor.	hor.	k?
Přitom	přitom	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
na	na	k7c6
severu	sever	k1gInSc6
tepleji	teple	k6eAd2
než	než	k8xS
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
severním	severní	k2eAgNnSc6d1
proudění	proudění	k1gNnSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
i	i	k9
na	na	k7c6
jižních	jižní	k2eAgInPc6d1
svazích	svah	k1gInPc6
hor	hora	k1gFnPc2
tzv.	tzv.	kA
severní	severní	k2eAgInSc4d1
fén	fén	k1gInSc4
<g/>
,	,	kIx,
tady	tady	k6eAd1
zde	zde	k6eAd1
již	již	k6eAd1
nevytváří	vytvářet	k5eNaImIp3nS,k5eNaPmIp3nS
výraznější	výrazný	k2eAgNnSc4d2
oteplení	oteplení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Příroda	příroda	k1gFnSc1
</s>
<s>
Flora	Flora	k1gFnSc1
</s>
<s>
borovice	borovice	k1gFnSc1
limba	limba	k1gFnSc1
</s>
<s>
Pásmo	pásmo	k1gNnSc1
lesů	les	k1gInPc2
je	být	k5eAaImIp3nS
na	na	k7c6
severních	severní	k2eAgInPc6d1
okrajích	okraj	k1gInPc6
hor	hora	k1gFnPc2
ve	v	k7c6
výšce	výška	k1gFnSc6
kolem	kolem	k7c2
1	#num#	k4
600	#num#	k4
m	m	kA
<g/>
,	,	kIx,
na	na	k7c4
jižních	jižní	k2eAgInPc2d1
zasahuje	zasahovat	k5eAaImIp3nS
i	i	k9
do	do	k7c2
výšek	výška	k1gFnPc2
2	#num#	k4
200	#num#	k4
m	m	kA
<g/>
,	,	kIx,
ve	v	k7c6
vysokých	vysoký	k2eAgFnPc6d1
horách	hora	k1gFnPc6
až	až	k6eAd1
do	do	k7c2
2	#num#	k4
300	#num#	k4
m.	m.	k?
V	v	k7c6
nižších	nízký	k2eAgInPc6d2
stupních	stupeň	k1gInPc6
lesů	les	k1gInPc2
rostou	růst	k5eAaImIp3nP
buky	buk	k1gInPc1
a	a	k8xC
javory	javor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výše	vysoce	k6eAd2
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
jehličnaté	jehličnatý	k2eAgInPc1d1
lesy	les	k1gInPc1
složené	složený	k2eAgInPc1d1
převážně	převážně	k6eAd1
ze	z	k7c2
smrků	smrk	k1gInPc2
<g/>
,	,	kIx,
jedlí	jedle	k1gFnPc2
<g/>
,	,	kIx,
limb	limba	k1gFnPc2
a	a	k8xC
modřínů	modřín	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
tímto	tento	k3xDgNnSc7
pásmem	pásmo	k1gNnSc7
jsou	být	k5eAaImIp3nP
porosty	porost	k1gInPc1
kosodřeviny	kosodřevina	k1gFnSc2
<g/>
,	,	kIx,
jalovců	jalovec	k1gInPc2
<g/>
,	,	kIx,
olší	olše	k1gFnPc2
a	a	k8xC
vrb	vrba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vyšších	vysoký	k2eAgFnPc6d2
výškách	výška	k1gFnPc6
se	se	k3xPyFc4
pak	pak	k6eAd1
nalézají	nalézat	k5eAaImIp3nP
alpínské	alpínský	k2eAgFnPc4d1
louky	louka	k1gFnPc4
s	s	k7c7
rostoucími	rostoucí	k2eAgInPc7d1
rododendrony	rododendron	k1gInPc7
a	a	k8xC
typickou	typický	k2eAgFnSc7d1
alpskou	alpský	k2eAgFnSc7d1
květenou	květena	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
vápencích	vápenec	k1gInPc6
podložích	podloží	k1gNnPc6
v	v	k7c6
severních	severní	k2eAgFnPc6d1
Alpách	Alpy	k1gFnPc6
se	se	k3xPyFc4
v	v	k7c6
pásmu	pásmo	k1gNnSc6
lesů	les	k1gInPc2
daří	dařit	k5eAaImIp3nP
olši	olše	k1gFnSc4
<g/>
,	,	kIx,
bříze	bříza	k1gFnSc3
a	a	k8xC
borovici	borovice	k1gFnSc3
limbě	limba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výše	výše	k1gFnSc1
se	se	k3xPyFc4
mimo	mimo	k7c4
kosodřeviny	kosodřevina	k1gFnPc4
vyskytují	vyskytovat	k5eAaImIp3nP
také	také	k9
zakrslé	zakrslý	k2eAgFnPc1d1
vrby	vrba	k1gFnPc1
<g/>
,	,	kIx,
pěnišníky	pěnišník	k1gInPc1
nebo	nebo	k8xC
lýkovce	lýkovec	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvetoucí	kvetoucí	k2eAgFnPc4d1
rostliny	rostlina	k1gFnPc4
ve	v	k7c6
vysokých	vysoký	k2eAgFnPc6d1
polohách	poloha	k1gFnPc6
jsou	být	k5eAaImIp3nP
velice	velice	k6eAd1
druhově	druhově	k6eAd1
rozmanité	rozmanitý	k2eAgFnPc1d1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
např.	např.	kA
medvědice	medvědice	k1gFnSc2
alpské	alpský	k2eAgFnSc2d1
<g/>
,	,	kIx,
mydlice	mydlice	k1gFnSc2
<g/>
,	,	kIx,
kopretina	kopretina	k1gFnSc1
<g/>
,	,	kIx,
zvonky	zvonek	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
symbol	symbol	k1gInSc4
Alp	Alpy	k1gFnPc2
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
alpská	alpský	k2eAgFnSc1d1
protěž	protěž	k1gFnSc1
(	(	kIx(
<g/>
jinak	jinak	k6eAd1
zvaný	zvaný	k2eAgInSc1d1
plesnivec	plesnivec	k1gInSc1
alpský	alpský	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nejvyšších	vysoký	k2eAgFnPc6d3
polohách	poloha	k1gFnPc6
se	se	k3xPyFc4
daří	dařit	k5eAaImIp3nS
pochybkům	pochybek	k1gInPc3
<g/>
,	,	kIx,
lýkovci	lýkovec	k1gInSc6
<g/>
,	,	kIx,
zvonečníkům	zvonečník	k1gInPc3
(	(	kIx(
<g/>
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
symbolem	symbol	k1gInSc7
města	město	k1gNnSc2
Cortina	Cortin	k2eAgInSc2d1
d	d	k?
<g/>
'	'	kIx"
<g/>
Ampezzo	Ampezza	k1gFnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
silenkám	silenka	k1gFnPc3
a	a	k8xC
nízkým	nízký	k2eAgFnPc3d1
prvosenkám	prvosenka	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
typickou	typický	k2eAgFnSc7d1
květinou	květina	k1gFnSc7
Alp	Alpy	k1gFnPc2
je	být	k5eAaImIp3nS
hořec	hořec	k1gInSc1
<g/>
,	,	kIx,
kterých	který	k3yIgFnPc2,k3yQgFnPc2,k3yRgFnPc2
se	se	k3xPyFc4
zde	zde	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
druhů	druh	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
jižních	jižní	k2eAgFnPc6d1
částech	část	k1gFnPc6
Alp	Alpy	k1gFnPc2
druhy	druh	k1gInPc1
Gentiana	Gentian	k1gMnSc4
froelichii	froelichie	k1gFnSc4
<g/>
,	,	kIx,
hořec	hořec	k1gInSc4
žlutý	žlutý	k2eAgInSc4d1
<g/>
,	,	kIx,
Gentiana	Gentiana	k1gFnSc1
terglouensis	terglouensis	k1gFnSc1
a	a	k8xC
hořec	hořec	k1gInSc1
Cluisův	Cluisův	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Fauna	fauna	k1gFnSc1
</s>
<s>
svišť	svišť	k1gMnSc1
horský	horský	k2eAgMnSc1d1
</s>
<s>
V	v	k7c6
Alpách	Alpy	k1gFnPc6
je	být	k5eAaImIp3nS
fauna	fauna	k1gFnSc1
zastoupena	zastoupit	k5eAaPmNgFnS
druhy	druh	k1gInPc1
typickými	typický	k2eAgFnPc7d1
i	i	k9
pro	pro	k7c4
jiné	jiný	k2eAgInPc4d1
horské	horský	k2eAgInPc4d1
systémy	systém	k1gInPc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
těmto	tento	k3xDgNnPc3
známým	známý	k2eAgNnPc3d1
zvířatům	zvíře	k1gNnPc3
patří	patřit	k5eAaImIp3nS
např.	např.	kA
kamzík	kamzík	k1gMnSc1
<g/>
,	,	kIx,
svišť	svišť	k1gMnSc1
<g/>
,	,	kIx,
kozorožec	kozorožec	k1gMnSc1
<g/>
,	,	kIx,
medvěd	medvěd	k1gMnSc1
<g/>
,	,	kIx,
rys	rys	k1gMnSc1
<g/>
,	,	kIx,
zajíc	zajíc	k1gMnSc1
atd.	atd.	kA
Vlk	Vlk	k1gMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
Alpách	Alpy	k1gFnPc6
vyhuben	vyhubit	k5eAaPmNgInS
kolem	kolem	k7c2
roku	rok	k1gInSc2
1900	#num#	k4
<g/>
,	,	kIx,
kolem	kolem	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
se	se	k3xPyFc4
zase	zase	k9
vrátil	vrátit	k5eAaPmAgInS
z	z	k7c2
Apenin	Apeniny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
žije	žít	k5eAaImIp3nS
na	na	k7c4
100	#num#	k4
vlků	vlk	k1gMnPc2
v	v	k7c6
italských	italský	k2eAgFnPc6d1
a	a	k8xC
francouzských	francouzský	k2eAgFnPc6d1
Západních	západní	k2eAgFnPc6d1
Alpách	Alpy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
do	do	k7c2
výšek	výška	k1gFnPc2
kolem	kolem	k7c2
4	#num#	k4
000	#num#	k4
m	m	kA
se	se	k3xPyFc4
dostává	dostávat	k5eAaImIp3nS
hraboš	hraboš	k1gMnSc1
sněžný	sněžný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
ptáků	pták	k1gMnPc2
zde	zde	k6eAd1
lze	lze	k6eAd1
vidět	vidět	k5eAaImF
tetřeva	tetřev	k1gMnSc4
<g/>
,	,	kIx,
tetřívka	tetřívek	k1gMnSc4
<g/>
,	,	kIx,
bělokura	bělokur	k1gMnSc4
horského	horský	k2eAgMnSc4d1
či	či	k8xC
pěnkavu	pěnkava	k1gFnSc4
sněžnou	sněžný	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dravce	dravec	k1gMnPc4
zastupují	zastupovat	k5eAaImIp3nP
nejčastěji	často	k6eAd3
orel	orel	k1gMnSc1
skalní	skalní	k2eAgMnSc1d1
<g/>
,	,	kIx,
orlosup	orlosup	k1gMnSc1
bradatý	bradatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
sokol	sokol	k1gMnSc1
stěhovavý	stěhovavý	k2eAgMnSc1d1
<g/>
,	,	kIx,
jestřáb	jestřáb	k1gMnSc1
a	a	k8xC
jiní	jiný	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
ryb	ryba	k1gFnPc2
se	se	k3xPyFc4
zde	zde	k6eAd1
hojně	hojně	k6eAd1
vyskytují	vyskytovat	k5eAaImIp3nP
pstruzi	pstruh	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
v	v	k7c6
horských	horský	k2eAgNnPc6d1
jezerech	jezero	k1gNnPc6
i	i	k9
ve	v	k7c6
výškách	výška	k1gFnPc6
2	#num#	k4
600	#num#	k4
m	m	kA
<g/>
,	,	kIx,
líni	lín	k1gMnPc1
či	či	k8xC
lipani	lipan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
alpských	alpský	k2eAgInPc6d1
plesech	ples	k1gInPc6
nalezneme	nalézt	k5eAaBmIp1nP,k5eAaPmIp1nP
živočichy	živočich	k1gMnPc4
<g/>
,	,	kIx,
již	jenž	k3xRgMnPc1
jsou	být	k5eAaImIp3nP
přímými	přímý	k2eAgMnPc7d1
potomky	potomek	k1gMnPc7
druhů	druh	k1gInPc2
z	z	k7c2
doby	doba	k1gFnSc2
ledové	ledový	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
plazi	plaz	k1gMnPc1
se	se	k3xPyFc4
dostávají	dostávat	k5eAaImIp3nP
až	až	k9
do	do	k7c2
výšek	výška	k1gFnPc2
3000	#num#	k4
m	m	kA
(	(	kIx(
<g/>
zmije	zmije	k1gFnSc1
obecná	obecná	k1gFnSc1
a	a	k8xC
ještěrka	ještěrka	k1gFnSc1
živorodá	živorodý	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Turismus	turismus	k1gInSc1
</s>
<s>
Alpy	Alpy	k1gFnPc1
patří	patřit	k5eAaImIp3nP
k	k	k7c3
nejoblíbenějším	oblíbený	k2eAgInPc3d3
světovým	světový	k2eAgInPc3d1
cílům	cíl	k1gInPc3
lyžařů	lyžař	k1gMnPc2
a	a	k8xC
cestovatelů	cestovatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nepopulárnější	populární	k2eNgFnSc1d2
oblast	oblast	k1gFnSc1
tohoto	tento	k3xDgInSc2
typu	typ	k1gInSc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počty	počet	k1gInPc1
návštěvníků	návštěvník	k1gMnPc2
se	se	k3xPyFc4
pohybují	pohybovat	k5eAaImIp3nP
v	v	k7c6
řádu	řád	k1gInSc6
jednotek	jednotka	k1gFnPc2
milionů	milion	k4xCgInPc2
turistů	turist	k1gMnPc2
za	za	k7c4
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projevuje	projevovat	k5eAaImIp3nS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
pozitivně	pozitivně	k6eAd1
na	na	k7c6
ekonomice	ekonomika	k1gFnSc6
jednotlivých	jednotlivý	k2eAgInPc2d1
alpských	alpský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
hromadného	hromadný	k2eAgNnSc2d1
cestování	cestování	k1gNnSc2
do	do	k7c2
Alp	Alpy	k1gFnPc2
začínají	začínat	k5eAaImIp3nP
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
když	když	k8xS
cizinci	cizinec	k1gMnPc1
cestovali	cestovat	k5eAaImAgMnP
poznávat	poznávat	k5eAaImF
jiná	jiný	k2eAgNnPc4d1
území	území	k1gNnPc4
<g/>
,	,	kIx,
krajinu	krajina	k1gFnSc4
alpských	alpský	k2eAgFnPc2d1
hor	hora	k1gFnPc2
nebo	nebo	k8xC
relaxovat	relaxovat	k5eAaBmF
v	v	k7c6
různých	různý	k2eAgNnPc6d1
odpočinkových	odpočinkový	k2eAgNnPc6d1
střediscích	středisko	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
hotelů	hotel	k1gInPc2
byla	být	k5eAaImAgFnS
vystavěna	vystavět	k5eAaPmNgFnS
v	v	k7c6
období	období	k1gNnSc6
kolem	kolem	k7c2
francouzko-pruské	francouzko-pruský	k2eAgFnSc2d1
války	válka	k1gFnSc2
r.	r.	kA
1871	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
stavět	stavět	k5eAaImF
různá	různý	k2eAgNnPc4d1
centra	centrum	k1gNnPc4
ve	v	k7c6
vyšších	vysoký	k2eAgFnPc6d2
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
a	a	k8xC
zároveň	zároveň	k6eAd1
s	s	k7c7
nimi	on	k3xPp3gMnPc7
i	i	k9
první	první	k4xOgInPc4
vleky	vlek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
vlek	vlek	k1gInSc1
byl	být	k5eAaImAgInS
postaven	postaven	k2eAgInSc1d1
roku	rok	k1gInSc2
1908	#num#	k4
v	v	k7c6
Grindelwaldu	Grindelwald	k1gInSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Stále	stále	k6eAd1
probíhaly	probíhat	k5eAaImAgFnP
inovace	inovace	k1gFnPc1
<g/>
,	,	kIx,
modernizace	modernizace	k1gFnPc1
a	a	k8xC
stavby	stavba	k1gFnPc1
různých	různý	k2eAgInPc2d1
hotelů	hotel	k1gInPc2
a	a	k8xC
středisek	středisko	k1gNnPc2
<g/>
,	,	kIx,
Počátkem	počátkem	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
zde	zde	k6eAd1
uskutečnilo	uskutečnit	k5eAaPmAgNnS
mnoho	mnoho	k4c1
soutěží	soutěž	k1gFnPc2
v	v	k7c6
krasobruslení	krasobruslení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
konci	konec	k1gInSc6
1	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byla	být	k5eAaImAgFnS
na	na	k7c6
švýcarské	švýcarský	k2eAgFnSc6d1
a	a	k8xC
rakouské	rakouský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
vystavěna	vystavěn	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
k	k	k7c3
ubytování	ubytování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letní	letní	k2eAgInSc1d1
turismus	turismus	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
stále	stále	k6eAd1
významnější	významný	k2eAgFnPc1d2
než	než	k8xS
zimní	zimní	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
do	do	k7c2
Alp	Alpy	k1gFnPc2
přicházela	přicházet	k5eAaImAgFnS
velká	velká	k1gFnSc1
množství	množství	k1gNnSc2
turistů	turist	k1gMnPc2
i	i	k8xC
sportovců	sportovec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konala	konat	k5eAaImAgNnP
se	se	k3xPyFc4
zde	zde	k6eAd1
řada	řada	k1gFnSc1
zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
se	se	k3xPyFc4
konaly	konat	k5eAaImAgInP
v	v	k7c6
Chamonix	Chamonix	k1gNnSc6
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1928	#num#	k4
v	v	k7c6
St.	st.	kA
Moritz	moritz	k1gInSc1
(	(	kIx(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1936	#num#	k4
v	v	k7c6
Garmisch-Partenkirchenu	Garmisch-Partenkirchen	k1gInSc6
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byly	být	k5eAaImAgFnP
olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
přerušeny	přerušen	k2eAgFnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
opět	opět	k6eAd1
obnoveny	obnovit	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
popularita	popularita	k1gFnSc1
sjezdového	sjezdový	k2eAgNnSc2d1
lyžování	lyžování	k1gNnSc2
značně	značně	k6eAd1
zvýšila	zvýšit	k5eAaPmAgFnS
a	a	k8xC
byly	být	k5eAaImAgFnP
postaveny	postaven	k2eAgFnPc4d1
další	další	k2eAgFnPc4d1
vesnice	vesnice	k1gFnPc4
i	i	k9
na	na	k7c6
francouzské	francouzský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
<g/>
,	,	kIx,
určené	určený	k2eAgInPc4d1
hlavně	hlavně	k9
zimním	zimní	k2eAgMnPc3d1
návštěvníkům	návštěvník	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámějším	známý	k2eAgNnSc7d3
takovým	takový	k3xDgNnSc7
střediskem	středisko	k1gNnSc7
je	být	k5eAaImIp3nS
Les	les	k1gInSc1
Menuires	Menuires	k1gInSc1
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupem	postupem	k7c2
času	čas	k1gInSc2
se	se	k3xPyFc4
zimní	zimní	k2eAgNnPc1d1
střediska	středisko	k1gNnPc1
inovovala	inovovat	k5eAaBmAgNnP
<g/>
,	,	kIx,
navštěvovalo	navštěvovat	k5eAaImAgNnS
je	on	k3xPp3gMnPc4
více	hodně	k6eAd2
turistů	turist	k1gMnPc2
a	a	k8xC
dnes	dnes	k6eAd1
jsou	být	k5eAaImIp3nP
Alpy	Alpy	k1gFnPc1
navštěvovanější	navštěvovaný	k2eAgFnPc1d2
spíše	spíše	k9
v	v	k7c6
zimě	zima	k1gFnSc6
než	než	k8xS
v	v	k7c6
létě	léto	k1gNnSc6
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
tomu	ten	k3xDgNnSc3
bylo	být	k5eAaImAgNnS
naopak	naopak	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Hallstatt	Hallstatt	k1gInSc1
v	v	k7c6
pohoří	pohoří	k1gNnSc6
Dachstein	Dachsteina	k1gFnPc2
</s>
<s>
Kdysi	kdysi	k6eAd1
byly	být	k5eAaImAgFnP
Alpy	Alpy	k1gFnPc1
osídleny	osídlen	k2eAgMnPc4d1
Rétoromány	Rétoromán	k1gMnPc4
<g/>
,	,	kIx,
indoevropskými	indoevropský	k2eAgInPc7d1
kmeny	kmen	k1gInPc7
Ilyrů	Ilyr	k1gMnPc2
<g/>
,	,	kIx,
Kelty	Kelt	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
opravdu	opravdu	k6eAd1
původní	původní	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
se	se	k3xPyFc4
považují	považovat	k5eAaImIp3nP
Ladinové	Ladinový	k2eAgInPc1d1
(	(	kIx(
<g/>
Jižní	jižní	k2eAgInPc1d1
Tyroly	Tyroly	k1gInPc1
<g/>
)	)	kIx)
a	a	k8xC
Rétorománi	Rétoromán	k1gMnPc1
(	(	kIx(
<g/>
kanton	kanton	k1gInSc1
Graubünden	Graubündna	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
stěhování	stěhování	k1gNnSc6
národů	národ	k1gInPc2
do	do	k7c2
oblasti	oblast	k1gFnSc2
Alp	Alpy	k1gFnPc2
migrovali	migrovat	k5eAaImAgMnP
zejména	zejména	k9
Germáni	Germán	k1gMnPc1
a	a	k8xC
Slované	Slovan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
zastoupeni	zastoupen	k2eAgMnPc1d1
Francouzi	Francouz	k1gMnPc1
<g/>
,	,	kIx,
Italové	Ital	k1gMnPc1
<g/>
,	,	kIx,
Rakušané	Rakušan	k1gMnPc1
<g/>
,	,	kIx,
Němci	Němec	k1gMnPc1
<g/>
,	,	kIx,
Švýcaři	Švýcar	k1gMnPc1
a	a	k8xC
Slovinci	Slovinec	k1gMnPc1
(	(	kIx(
<g/>
a	a	k8xC
malou	malý	k2eAgFnSc7d1
částí	část	k1gFnSc7
na	na	k7c6
východě	východ	k1gInSc6
i	i	k9
Maďaři	Maďar	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgFnSc1d1
rétorománské	rétorománský	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
je	být	k5eAaImIp3nS
rozptýleno	rozptýlen	k2eAgNnSc1d1
do	do	k7c2
malých	malý	k2eAgFnPc2d1
menšin	menšina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
v	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
již	již	k6eAd1
splývají	splývat	k5eAaImIp3nP
s	s	k7c7
místními	místní	k2eAgMnPc7d1
obyvateli	obyvatel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhruba	zhruba	k6eAd1
34	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
hovoří	hovořit	k5eAaImIp3nS
německy	německy	k6eAd1
(	(	kIx(
<g/>
s	s	k7c7
mnoha	mnoho	k4c7
rozličnými	rozličný	k2eAgInPc7d1
dialekty	dialekt	k1gInPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
26	#num#	k4
%	%	kIx~
francouzsky	francouzsky	k6eAd1
<g/>
,	,	kIx,
29	#num#	k4
%	%	kIx~
italsky	italsky	k6eAd1
a	a	k8xC
11	#num#	k4
%	%	kIx~
slovanskými	slovanský	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Plocha	plocha	k1gFnSc1
Alp	Alpy	k1gFnPc2
je	být	k5eAaImIp3nS
220	#num#	k4
000	#num#	k4
km²	km²	k?
dle	dle	k7c2
nach	nach	k1gInSc1
AEIOU	AEIOU	kA
(	(	kIx(
<g/>
Austria-Forum	Austria-Forum	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
či	či	k8xC
190	#num#	k4
912	#num#	k4
km²	km²	k?
dle	dle	k7c2
nach	nach	k1gInSc4
CIPRA	Cipro	k1gMnSc4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Montes	Montes	k1gMnSc1
Alpes	Alpes	k1gMnSc1
<g/>
,	,	kIx,
Gazetteer	Gazetteer	k1gMnSc1
of	of	k?
Planetary	Planetara	k1gFnSc2
Nomenclature	Nomenclatur	k1gMnSc5
<g/>
,	,	kIx,
IAU	IAU	kA
<g/>
,	,	kIx,
USGS	USGS	kA
<g/>
,	,	kIx,
NASA	NASA	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
https://www.nature.com/articles/s41598-020-77518-9	https://www.nature.com/articles/s41598-020-77518-9	k4
-	-	kIx~
New	New	k1gMnPc2
glacier	glacier	k1gMnSc1
evidence	evidence	k1gFnSc2
for	forum	k1gNnPc2
ice-free	ice-freat	k5eAaPmIp3nS
summits	summits	k6eAd1
during	during	k1gInSc1
the	the	k?
life	life	k1gInSc1
of	of	k?
the	the	k?
Tyrolean	Tyrolean	k1gMnSc1
Iceman	Iceman	k1gMnSc1
<g/>
↑	↑	k?
ANDREW	ANDREW	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Beattie	Beattie	k1gFnSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Alps	Alpsa	k1gFnPc2
:	:	kIx,
a	a	k8xC
cultural	culturat	k5eAaPmAgMnS,k5eAaImAgMnS
history	histor	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
xiv	xiv	k?
<g/>
,	,	kIx,
246	#num#	k4
pages	pages	k1gInSc1
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
195309553	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780195309553	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
69021087	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
publikace	publikace	k1gFnSc1
Světová	světový	k2eAgFnSc1d1
pohoří	pohoří	k1gNnPc2
(	(	kIx(
<g/>
Evropa	Evropa	k1gFnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Šlégl	Šlégl	k1gMnSc1
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
ISBN	ISBN	kA
80-242-0822-9	80-242-0822-9	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Alpinismus	alpinismus	k1gInSc1
</s>
<s>
Alpské	alpský	k2eAgFnPc1d1
čtyřtisícovky	čtyřtisícovka	k1gFnPc1
</s>
<s>
Italské	italský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
</s>
<s>
Rakouské	rakouský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
</s>
<s>
Slovinské	slovinský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
</s>
<s>
SOIUSA	SOIUSA	kA
</s>
<s>
Švýcarské	švýcarský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Alpy	alpa	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Alpy	alpa	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Alpy	alpa	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Alpy	alpa	k1gFnSc2
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
NASA	NASA	kA
–	–	k?
Visible	Visible	k1gMnSc1
Earth	Earth	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
128379	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4001328-5	4001328-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85003839	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
315124935	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85003839	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
</s>
