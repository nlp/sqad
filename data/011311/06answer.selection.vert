<s>
První	první	k4xOgNnSc1	první
české	český	k2eAgNnSc1d1	české
předsednictví	předsednictví	k1gNnSc1	předsednictví
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
či	či	k8xC	či
zkráceně	zkráceně	k6eAd1	zkráceně
jen	jen	k9	jen
české	český	k2eAgNnSc1d1	české
předsednictví	předsednictví	k1gNnSc1	předsednictví
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
předsedala	předsedat	k5eAaImAgFnS	předsedat
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
radě	rada	k1gFnSc6	rada
<g/>
.	.	kIx.	.
</s>
