<p>
<s>
První	první	k4xOgNnSc1	první
české	český	k2eAgNnSc1d1	české
předsednictví	předsednictví	k1gNnSc1	předsednictví
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
či	či	k8xC	či
zkráceně	zkráceně	k6eAd1	zkráceně
jen	jen	k9	jen
české	český	k2eAgNnSc1d1	české
předsednictví	předsednictví	k1gNnSc1	předsednictví
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
předsedala	předsedat	k5eAaImAgFnS	předsedat
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
radě	rada	k1gFnSc6	rada
<g/>
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
se	se	k3xPyFc4	se
tak	tak	k9	tak
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
Rady	rada	k1gFnSc2	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
Unie	unie	k1gFnSc2	unie
a	a	k8xC	a
potažmo	potažmo	k6eAd1	potažmo
i	i	k9	i
předsedou	předseda	k1gMnSc7	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
předsednictví	předsednictví	k1gNnSc1	předsednictví
se	se	k3xPyFc4	se
do	do	k7c2	do
přijetí	přijetí	k1gNnSc2	přijetí
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
odvozovalo	odvozovat	k5eAaImAgNnS	odvozovat
od	od	k7c2	od
předsednictví	předsednictví	k1gNnSc2	předsednictví
Rady	rada	k1gFnSc2	rada
EU	EU	kA	EU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
byla	být	k5eAaImAgFnS	být
Topolánkově	Topolánkův	k2eAgFnSc3d1	Topolánkova
vládě	vláda	k1gFnSc3	vláda
vyslovena	vysloven	k2eAgFnSc1d1	vyslovena
nedůvěra	nedůvěra	k1gFnSc1	nedůvěra
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
nahradil	nahradit	k5eAaPmAgMnS	nahradit
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Jan	Jan	k1gMnSc1	Jan
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
bylo	být	k5eAaImAgNnS	být
předsednictví	předsednictví	k1gNnSc1	předsednictví
předáno	předat	k5eAaPmNgNnS	předat
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
Rady	rada	k1gFnSc2	rada
ministrů	ministr	k1gMnPc2	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
EU	EU	kA	EU
<g/>
;	;	kIx,	;
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
vystřídán	vystřídán	k2eAgInSc4d1	vystřídán
Janem	Jan	k1gMnSc7	Jan
Kohoutem	Kohout	k1gMnSc7	Kohout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
české	český	k2eAgNnSc1d1	české
předsednictví	předsednictví	k1gNnSc1	předsednictví
se	se	k3xPyFc4	se
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
konce	konec	k1gInSc2	konec
prosince	prosinec	k1gInSc2	prosinec
roku	rok	k1gInSc2	rok
2022	[number]	k4	2022
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vláda	vláda	k1gFnSc1	vláda
Mirka	Mirka	k1gFnSc1	Mirka
Topolánka	Topolánka	k1gFnSc1	Topolánka
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
Jana	Jan	k1gMnSc2	Jan
Fischera	Fischer	k1gMnSc2	Fischer
</s>
</p>
<p>
<s>
Francouzské	francouzský	k2eAgNnSc1d1	francouzské
předsednictví	předsednictví	k1gNnSc1	předsednictví
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
</s>
</p>
<p>
<s>
Švédské	švédský	k2eAgNnSc1d1	švédské
předsednictví	předsednictví	k1gNnSc1	předsednictví
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
</s>
</p>
<p>
<s>
Entropa	Entropa	k1gFnSc1	Entropa
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Tschechische	Tschechische	k1gInSc4	Tschechische
EU-Ratspräsidentschaft	EU-Ratspräsidentschaft	k1gInSc1	EU-Ratspräsidentschaft
2009	[number]	k4	2009
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
<g/>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Českého	český	k2eAgNnSc2d1	české
předsednictví	předsednictví	k1gNnSc2	předsednictví
</s>
</p>
