<s>
Prostoročas	prostoročas	k1gInSc1	prostoročas
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
časoprostor	časoprostor	k1gInSc1	časoprostor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
pojem	pojem	k1gInSc1	pojem
z	z	k7c2	z
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
sjednocující	sjednocující	k2eAgInSc4d1	sjednocující
prostor	prostor	k1gInSc4	prostor
a	a	k8xC	a
čas	čas	k1gInSc4	čas
do	do	k7c2	do
jednoho	jeden	k4xCgNnSc2	jeden
čtyřrozměrného	čtyřrozměrný	k2eAgNnSc2d1	čtyřrozměrné
kontinua	kontinuum	k1gNnSc2	kontinuum
<g/>
.	.	kIx.	.
</s>
