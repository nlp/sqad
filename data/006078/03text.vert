<s>
Prostoročas	prostoročas	k1gInSc1	prostoročas
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
časoprostor	časoprostor	k1gInSc1	časoprostor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
pojem	pojem	k1gInSc1	pojem
z	z	k7c2	z
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
sjednocující	sjednocující	k2eAgInSc4d1	sjednocující
prostor	prostor	k1gInSc4	prostor
a	a	k8xC	a
čas	čas	k1gInSc4	čas
do	do	k7c2	do
jednoho	jeden	k4xCgNnSc2	jeden
čtyřrozměrného	čtyřrozměrný	k2eAgNnSc2d1	čtyřrozměrné
kontinua	kontinuum	k1gNnSc2	kontinuum
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
hraje	hrát	k5eAaImIp3nS	hrát
roli	role	k1gFnSc4	role
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
rozměru	rozměr	k1gInSc2	rozměr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
zbylým	zbylý	k2eAgMnPc3d1	zbylý
třem	tři	k4xCgInPc3	tři
prostorovým	prostorový	k2eAgInPc3d1	prostorový
rozměrům	rozměr	k1gInPc3	rozměr
význačný	význačný	k2eAgMnSc1d1	význačný
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
lze	lze	k6eAd1	lze
pohybovat	pohybovat	k5eAaImF	pohybovat
jen	jen	k6eAd1	jen
jedním	jeden	k4xCgInSc7	jeden
směrem	směr	k1gInSc7	směr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
je	být	k5eAaImIp3nS	být
prostoročas	prostoročas	k1gInSc1	prostoročas
obecně	obecně	k6eAd1	obecně
zakřivený	zakřivený	k2eAgInSc1d1	zakřivený
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
strukturu	struktura	k1gFnSc4	struktura
variety	varieta	k1gFnSc2	varieta
<g/>
.	.	kIx.	.
</s>
<s>
Projevy	projev	k1gInPc4	projev
zakřivení	zakřivení	k1gNnSc2	zakřivení
prostoročasu	prostoročas	k1gInSc2	prostoročas
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
jako	jako	k9	jako
gravitaci	gravitace	k1gFnSc4	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
je	být	k5eAaImIp3nS	být
vnímání	vnímání	k1gNnSc1	vnímání
času	čas	k1gInSc2	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc2	prostor
odděleně	odděleně	k6eAd1	odděleně
závislé	závislý	k2eAgFnPc1d1	závislá
na	na	k7c4	na
pozorovateli	pozorovatel	k1gMnPc7	pozorovatel
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
klasické	klasický	k2eAgFnSc2d1	klasická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prostoročas	prostoročas	k1gInSc1	prostoročas
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
pozorovateli	pozorovatel	k1gMnSc6	pozorovatel
nezávislý	závislý	k2eNgInSc1d1	nezávislý
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
formulaci	formulace	k1gFnSc4	formulace
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
zákonů	zákon	k1gInPc2	zákon
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jejich	jejich	k3xOp3gInSc4	jejich
tvar	tvar	k1gInSc4	tvar
nezávisel	záviset	k5eNaImAgMnS	záviset
na	na	k7c6	na
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
body	bod	k1gInPc1	bod
prostoročasu	prostoročas	k1gInSc2	prostoročas
nazýváme	nazývat	k5eAaImIp1nP	nazývat
události	událost	k1gFnPc1	událost
a	a	k8xC	a
matematicky	matematicky	k6eAd1	matematicky
je	on	k3xPp3gInPc4	on
popisujeme	popisovat	k5eAaImIp1nP	popisovat
pomocí	pomocí	k7c2	pomocí
čtyřvektorů	čtyřvektor	k1gInPc2	čtyřvektor
<g/>
.	.	kIx.	.
</s>
<s>
Dráhy	dráha	k1gFnPc4	dráha
bodových	bodový	k2eAgFnPc2d1	bodová
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
prostoročasu	prostoročas	k1gInSc6	prostoročas
pak	pak	k6eAd1	pak
nazýváme	nazývat	k5eAaImIp1nP	nazývat
světočáry	světočára	k1gFnPc4	světočára
<g/>
.	.	kIx.	.
</s>
<s>
Vícerozměrný	vícerozměrný	k2eAgInSc1d1	vícerozměrný
objekt	objekt	k1gInSc1	objekt
vykresluje	vykreslovat	k5eAaImIp3nS	vykreslovat
v	v	k7c6	v
časoprostoru	časoprostor	k1gInSc6	časoprostor
tzv.	tzv.	kA	tzv.
světoplochu	světoploch	k1gInSc6	světoploch
<g/>
.	.	kIx.	.
</s>
<s>
Termíny	termín	k1gInPc1	termín
prostoročas	prostoročas	k1gInSc1	prostoročas
a	a	k8xC	a
časoprostor	časoprostor	k1gInSc1	časoprostor
označují	označovat	k5eAaImIp3nP	označovat
totéž	týž	k3xTgNnSc4	týž
-	-	kIx~	-
tentýž	týž	k3xTgInSc4	týž
pojem	pojem	k1gInSc4	pojem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odborných	odborný	k2eAgInPc6d1	odborný
kruzích	kruh	k1gInPc6	kruh
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
používá	používat	k5eAaImIp3nS	používat
termín	termín	k1gInSc1	termín
prostoročas	prostoročas	k1gInSc1	prostoročas
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
světové	světový	k2eAgInPc1d1	světový
jazyky	jazyk	k1gInPc1	jazyk
používají	používat	k5eAaImIp3nP	používat
"	"	kIx"	"
<g/>
spacetime	spacetimat	k5eAaPmIp3nS	spacetimat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Raumzeit	Raumzeit	k1gInSc1	Raumzeit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
espace-temps	espaceemps	k1gInSc1	espace-temps
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
speciální	speciální	k2eAgFnSc6d1	speciální
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
složky	složka	k1gFnPc1	složka
polohového	polohový	k2eAgInSc2d1	polohový
čtyřvektoru	čtyřvektor	k1gInSc2	čtyřvektor
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
první	první	k4xOgFnPc1	první
tři	tři	k4xCgFnPc1	tři
souřadnice	souřadnice	k1gFnPc1	souřadnice
prostorové	prostorový	k2eAgFnPc1d1	prostorová
a	a	k8xC	a
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
s	s	k7c7	s
imaginární	imaginární	k2eAgFnSc7d1	imaginární
jednotkou	jednotka	k1gFnSc7	jednotka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
1	[number]	k4	1
<g/>
;	;	kIx,	;
x	x	k?	x
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
x	x	k?	x
<g/>
3	[number]	k4	3
<g/>
;	;	kIx,	;
x	x	k?	x
<g/>
4	[number]	k4	4
=	=	kIx~	=
ict	ict	k?	ict
<g/>
)	)	kIx)	)
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
snadný	snadný	k2eAgInSc1d1	snadný
zápis	zápis	k1gInSc1	zápis
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
jako	jako	k8xC	jako
součet	součet	k1gInSc1	součet
čtverců	čtverec	k1gInPc2	čtverec
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
metrika	metrika	k1gFnSc1	metrika
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
pseudoeuklidovská	pseudoeuklidovský	k2eAgFnSc1d1	pseudoeuklidovský
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
pořadí	pořadí	k1gNnSc1	pořadí
obrácené	obrácený	k2eAgNnSc1d1	obrácené
a	a	k8xC	a
s	s	k7c7	s
výhradně	výhradně	k6eAd1	výhradně
reálnými	reálný	k2eAgFnPc7d1	reálná
souřadnicemi	souřadnice	k1gFnPc7	souřadnice
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
0	[number]	k4	0
=	=	kIx~	=
ct	ct	k?	ct
<g/>
;	;	kIx,	;
x	x	k?	x
<g/>
1	[number]	k4	1
<g/>
;	;	kIx,	;
x	x	k?	x
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
x	x	k?	x
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
pořadí	pořadí	k1gNnSc1	pořadí
užívá	užívat	k5eAaImIp3nS	užívat
vždy	vždy	k6eAd1	vždy
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
událostmi	událost	k1gFnPc7	událost
v	v	k7c6	v
prostoročasu	prostoročas	k1gInSc6	prostoročas
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
prostoročasová	prostoročasový	k2eAgFnSc1d1	prostoročasová
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
(	(	kIx(	(
<g/>
interval	interval	k1gInSc1	interval
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Prostoročas	prostoročas	k1gInSc1	prostoročas
užívaný	užívaný	k2eAgInSc1d1	užívaný
ve	v	k7c6	v
speciální	speciální	k2eAgFnSc6d1	speciální
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
je	být	k5eAaImIp3nS	být
čtyřrozměrný	čtyřrozměrný	k2eAgMnSc1d1	čtyřrozměrný
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
souřadnice	souřadnice	k1gFnSc1	souřadnice
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
,	,	kIx,	,
z	z	k7c2	z
představují	představovat	k5eAaImIp3nP	představovat
prostorové	prostorový	k2eAgFnPc4d1	prostorová
souřadnice	souřadnice	k1gFnPc4	souřadnice
a	a	k8xC	a
časová	časový	k2eAgFnSc1d1	časová
souřadnice	souřadnice	k1gFnSc1	souřadnice
je	být	k5eAaImIp3nS	být
vyjadřována	vyjadřovat	k5eAaImNgFnS	vyjadřovat
jako	jako	k9	jako
ct	ct	k?	ct
<g/>
,	,	kIx,	,
kde	kde	k9	kde
c	c	k0	c
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Čtveřice	čtveřice	k1gFnSc1	čtveřice
souřadnic	souřadnice	k1gFnPc2	souřadnice
tvoří	tvořit	k5eAaImIp3nS	tvořit
čtyřvektor	čtyřvektor	k1gInSc1	čtyřvektor
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
souřadnice	souřadnice	k1gFnPc1	souřadnice
(	(	kIx(	(
<g/>
prostorové	prostorový	k2eAgFnPc1d1	prostorová
i	i	k8xC	i
časová	časový	k2eAgFnSc1d1	časová
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
tedy	tedy	k9	tedy
prostorový	prostorový	k2eAgInSc4d1	prostorový
rozměr	rozměr	k1gInSc4	rozměr
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gFnSc7	jejich
jednotkou	jednotka	k1gFnSc7	jednotka
jsou	být	k5eAaImIp3nP	být
metry	metr	k1gInPc4	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
čtyřrozměrný	čtyřrozměrný	k2eAgInSc1d1	čtyřrozměrný
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
použitelný	použitelný	k2eAgInSc1d1	použitelný
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
prostoru	prostor	k1gInSc6	prostor
je	být	k5eAaImIp3nS	být
invariantní	invariantní	k2eAgFnSc1d1	invariantní
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
Lorentzově	Lorentzův	k2eAgFnSc3d1	Lorentzova
transformaci	transformace	k1gFnSc3	transformace
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
body	bod	k1gInPc4	bod
tohoto	tento	k3xDgInSc2	tento
prostoru	prostor	k1gInSc2	prostor
byla	být	k5eAaImAgFnS	být
definována	definovat	k5eAaBmNgFnS	definovat
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
v	v	k7c6	v
euklidovském	euklidovský	k2eAgInSc6d1	euklidovský
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Doplníme	doplnit	k5eAaPmIp1nP	doplnit
<g/>
-li	i	k?	-li
časovou	časový	k2eAgFnSc4d1	časová
souřadnici	souřadnice	k1gFnSc4	souřadnice
o	o	k7c4	o
imaginární	imaginární	k2eAgFnSc4d1	imaginární
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
časovou	časový	k2eAgFnSc4d1	časová
souřadnici	souřadnice	k1gFnSc4	souřadnice
vyjádříme	vyjádřit	k5eAaPmIp1nP	vyjádřit
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k8xC	i
:	:	kIx,	:
c	c	k0	c
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
ct	ct	k?	ct
<g/>
}	}	kIx)	}
,	,	kIx,	,
lze	lze	k6eAd1	lze
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
vztahem	vztah	k1gInSc7	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
s	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
Δ	Δ	k?	Δ
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
Δ	Δ	k?	Δ
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
Δ	Δ	k?	Δ
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-x_	_	k?	-x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-y_	_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-z_	_	k?	-z_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-c	-c	k?	-c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-t_	_	k?	-t_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-c	-c	k?	-c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
t	t	k?	t
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Veličina	veličina	k1gFnSc1	veličina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
lineární	lineární	k2eAgInSc1d1	lineární
element	element	k1gInSc1	element
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
definovaný	definovaný	k2eAgInSc1d1	definovaný
prostoročas	prostoročas	k1gInSc1	prostoročas
má	mít	k5eAaImIp3nS	mít
euklidovský	euklidovský	k2eAgInSc4d1	euklidovský
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
Minkowského	Minkowského	k2eAgInSc1d1	Minkowského
prostor	prostor	k1gInSc1	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Geometrie	geometrie	k1gFnSc1	geometrie
v	v	k7c6	v
Minkowskiho	Minkowski	k1gMnSc2	Minkowski
prostoročase	prostoročas	k1gInSc6	prostoročas
však	však	k9	však
není	být	k5eNaImIp3nS	být
euklidovská	euklidovský	k2eAgFnSc1d1	euklidovská
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pseudoeuklidovská	pseudoeuklidovský	k2eAgFnSc1d1	pseudoeuklidovský
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
přechodu	přechod	k1gInSc2	přechod
od	od	k7c2	od
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
k	k	k7c3	k
obecné	obecný	k2eAgFnSc3d1	obecná
teorii	teorie	k1gFnSc3	teorie
relativity	relativita	k1gFnSc2	relativita
je	být	k5eAaImIp3nS	být
však	však	k9	však
vhodnější	vhodný	k2eAgNnSc1d2	vhodnější
formulovat	formulovat	k5eAaImF	formulovat
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
zavedením	zavedení	k1gNnSc7	zavedení
metrického	metrický	k2eAgInSc2d1	metrický
tenzoru	tenzor	k1gInSc2	tenzor
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Minkowského	Minkowského	k2eAgInSc1d1	Minkowského
prostor	prostor	k1gInSc1	prostor
není	být	k5eNaImIp3nS	být
zakřivený	zakřivený	k2eAgInSc1d1	zakřivený
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
metrický	metrický	k2eAgInSc1d1	metrický
tenzor	tenzor	k1gInSc1	tenzor
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
tvar	tvar	k1gInSc4	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
η	η	k?	η
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
diag	diag	k1gInSc1	diag
:	:	kIx,	:
(	(	kIx(	(
−	−	k?	−
1	[number]	k4	1
,	,	kIx,	,
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
1	[number]	k4	1
,	,	kIx,	,
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
eta	eta	k?	eta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k2eAgInSc4d1	begin
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc4	pmatrix
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\\	\\	k?	\\
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
1	[number]	k4	1
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
\\	\\	k?	\\
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
1	[number]	k4	1
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
\\	\\	k?	\\
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
diag	diag	k1gInSc4	diag
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
1,1	[number]	k4	1,1
<g/>
,1	,1	k4	,1
<g/>
,1	,1	k4	,1
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ι	ι	k?	ι
,	,	kIx,	,
κ	κ	k?	κ
=	=	kIx~	=
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
,	,	kIx,	,
2	[number]	k4	2
,	,	kIx,	,
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
iota	iotum	k1gNnPc4	iotum
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc2	kappa
=	=	kIx~	=
<g/>
0,1	[number]	k4	0,1
<g/>
,2	,2	k4	,2
<g/>
,3	,3	k4	,3
<g/>
}	}	kIx)	}
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
index	index	k1gInSc1	index
0	[number]	k4	0
označuje	označovat	k5eAaImIp3nS	označovat
časovou	časový	k2eAgFnSc4d1	časová
složku	složka	k1gFnSc4	složka
a	a	k8xC	a
indexy	index	k1gInPc1	index
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
a	a	k8xC	a
3	[number]	k4	3
označují	označovat	k5eAaImIp3nP	označovat
prostorové	prostorový	k2eAgInPc4d1	prostorový
komponenty	komponent	k1gInPc4	komponent
metrického	metrický	k2eAgInSc2d1	metrický
tenzoru	tenzor	k1gInSc2	tenzor
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
uvedené	uvedený	k2eAgFnSc2d1	uvedená
metriky	metrika	k1gFnSc2	metrika
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
metrický	metrický	k2eAgInSc4d1	metrický
tenzor	tenzor	k1gInSc4	tenzor
s	s	k7c7	s
rozdílnou	rozdílný	k2eAgFnSc7d1	rozdílná
signaturou	signatura	k1gFnSc7	signatura
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
η	η	k?	η
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
diag	diag	k1gInSc1	diag
:	:	kIx,	:
(	(	kIx(	(
1	[number]	k4	1
,	,	kIx,	,
−	−	k?	−
1	[number]	k4	1
,	,	kIx,	,
−	−	k?	−
1	[number]	k4	1
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
−	−	k?	−
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
eta	eta	k?	eta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gInSc1	begin
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}	}	kIx)	}
<g/>
1	[number]	k4	1
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
\\	\\	k?	\\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
\\	\\	k?	\\
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
\\	\\	k?	\\
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
diag	diag	k1gInSc4	diag
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
dvou	dva	k4xCgInPc2	dva
<g />
.	.	kIx.	.
</s>
<s hack="1">
vektorů	vektor	k1gInPc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
ι	ι	k?	ι
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iota	k6eAd1	iota
}}	}}	k?	}}
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
ι	ι	k?	ι
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
B	B	kA	B
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
iota	iot	k1gInSc2	iot
}}	}}	k?	}}
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xC	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
η	η	k?	η
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
ι	ι	k?	ι
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
eta	eta	k?	eta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iota	k1gFnSc1	iota
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc1	kappa
}	}	kIx)	}
<g/>
A	A	kA	A
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iota	k1gMnSc1	iota
}	}	kIx)	}
<g/>
B	B	kA	B
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc1	kappa
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
užito	užít	k5eAaPmNgNnS	užít
Einsteinovo	Einsteinův	k2eAgNnSc1d1	Einsteinovo
sumační	sumační	k2eAgNnSc1d1	sumační
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
skalární	skalární	k2eAgInSc4d1	skalární
součin	součin	k1gInSc4	součin
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
i	i	k9	i
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
η	η	k?	η
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
ι	ι	k?	ι
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iota	k6eAd1	iota
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
x	x	k?	x
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc1	kappa
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
označuje	označovat	k5eAaImIp3nS	označovat
časovou	časový	k2eAgFnSc4d1	časová
souřadnici	souřadnice	k1gFnSc4	souřadnice
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
,	,	kIx,	,
2	[number]	k4	2
,	,	kIx,	,
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1,2	[number]	k4	1,2
<g/>
,3	,3	k4	,3
<g/>
}	}	kIx)	}
označuje	označovat	k5eAaImIp3nS	označovat
prostorové	prostorový	k2eAgFnPc4d1	prostorová
souřadnice	souřadnice	k1gFnPc4	souřadnice
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
využití	využití	k1gNnSc4	využití
prostředků	prostředek	k1gInPc2	prostředek
Riemannovy	Riemannův	k2eAgFnSc2d1	Riemannova
geometrie	geometrie	k1gFnSc2	geometrie
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
je	být	k5eAaImIp3nS	být
indefinitní	indefinitní	k2eAgFnSc1d1	indefinitní
<g/>
,	,	kIx,	,
označujeme	označovat	k5eAaImIp1nP	označovat
tuto	tento	k3xDgFnSc4	tento
geometrii	geometrie	k1gFnSc4	geometrie
jako	jako	k8xS	jako
pseudoriemannovskou	pseudoriemannovský	k2eAgFnSc4d1	pseudoriemannovský
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
bod	bod	k1gInSc4	bod
Minkowskiho	Minkowski	k1gMnSc2	Minkowski
prostoročasu	prostoročas	k1gInSc2	prostoročas
představuje	představovat	k5eAaImIp3nS	představovat
tzv.	tzv.	kA	tzv.
prostoročasovou	prostoročasový	k2eAgFnSc4d1	prostoročasová
událost	událost	k1gFnSc4	událost
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vyjadřujeme	vyjadřovat	k5eAaImIp1nP	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
prostorový	prostorový	k2eAgInSc4d1	prostorový
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
bod	bod	k1gInSc4	bod
prostoru	prostor	k1gInSc2	prostor
vztahující	vztahující	k2eAgFnSc1d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
danému	daný	k2eAgInSc3d1	daný
časovému	časový	k2eAgInSc3d1	časový
okamžiku	okamžik	k1gInSc3	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
událostmi	událost	k1gFnPc7	událost
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
prostoročasový	prostoročasový	k2eAgInSc4d1	prostoročasový
interval	interval	k1gInSc4	interval
(	(	kIx(	(
<g/>
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
Minkowskiho	Minkowski	k1gMnSc2	Minkowski
prostoročasu	prostoročas	k1gInSc2	prostoročas
používá	používat	k5eAaImIp3nS	používat
Riemannův	Riemannův	k2eAgInSc1d1	Riemannův
prostoročas	prostoročas	k1gInSc1	prostoročas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obecně	obecně	k6eAd1	obecně
zakřivený	zakřivený	k2eAgInSc1d1	zakřivený
a	a	k8xC	a
metrika	metrika	k1gFnSc1	metrika
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
symetrickým	symetrický	k2eAgInSc7d1	symetrický
metrickým	metrický	k2eAgInSc7d1	metrický
tenzorem	tenzor	k1gInSc7	tenzor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iota	k1gFnSc1	iota
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc1	kappa
}}	}}	k?	}}
,	,	kIx,	,
který	který	k3yIgMnSc1	který
obecně	obecně	k6eAd1	obecně
není	být	k5eNaImIp3nS	být
diagonální	diagonální	k2eAgFnSc1d1	diagonální
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
ι	ι	k?	ι
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iota	k1gFnSc1	iota
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc1	kappa
}}	}}	k?	}}
:	:	kIx,	:
Přechod	přechod	k1gInSc1	přechod
ke	k	k7c3	k
speciální	speciální	k2eAgFnSc3d1	speciální
teorii	teorie	k1gFnSc3	teorie
relativity	relativita	k1gFnSc2	relativita
lze	lze	k6eAd1	lze
zajistit	zajistit	k5eAaPmF	zajistit
položením	položení	k1gNnSc7	položení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
η	η	k?	η
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}}	}}	k?	}}
:	:	kIx,	:
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
pak	pak	k6eAd1	pak
získá	získat	k5eAaPmIp3nS	získat
tvar	tvar	k1gInSc4	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
η	η	k?	η
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
ι	ι	k?	ι
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
eta	eta	k?	eta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iota	k6eAd1	iota
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc1	kappa
}}	}}	k?	}}
:	:	kIx,	:
Prostoročasové	prostoročasový	k2eAgFnPc4d1	prostoročasová
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
událostmi	událost	k1gFnPc7	událost
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
událostmi	událost	k1gFnPc7	událost
předat	předat	k5eAaPmF	předat
informaci	informace	k1gFnSc4	informace
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
signálu	signál	k1gInSc2	signál
šířícího	šířící	k2eAgNnSc2d1	šířící
se	s	k7c7	s
světelnou	světelný	k2eAgFnSc7d1	světelná
nebo	nebo	k8xC	nebo
podsvětelnou	podsvětelný	k2eAgFnSc7d1	podsvětelná
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Časupodobný	Časupodobný	k2eAgInSc4d1	Časupodobný
interval	interval	k1gInSc4	interval
-	-	kIx~	-
též	též	k9	též
časový	časový	k2eAgInSc1d1	časový
nebo	nebo	k8xC	nebo
časového	časový	k2eAgInSc2d1	časový
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
událostmi	událost	k1gFnPc7	událost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
předána	předat	k5eAaPmNgFnS	předat
informace	informace	k1gFnSc1	informace
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
signálu	signál	k1gInSc2	signál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
podsvětelnou	podsvětelný	k2eAgFnSc7d1	podsvětelná
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
rychlostí	rychlost	k1gFnSc7	rychlost
nižší	nízký	k2eAgFnSc7d2	nižší
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovémto	takovýto	k3xDgInSc6	takovýto
uspořádání	uspořádání	k1gNnSc6	uspořádání
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
vznik	vznik	k1gInSc4	vznik
první	první	k4xOgFnSc2	první
události	událost	k1gFnSc2	událost
příčinou	příčina	k1gFnSc7	příčina
výskytu	výskyt	k1gInSc2	výskyt
druhé	druhý	k4xOgFnSc2	druhý
události	událost	k1gFnSc2	událost
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
událostmi	událost	k1gFnPc7	událost
existuje	existovat	k5eAaImIp3nS	existovat
příčinná	příčinný	k2eAgFnSc1d1	příčinná
souvislost	souvislost	k1gFnSc1	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zvolené	zvolený	k2eAgFnSc6d1	zvolená
metrice	metrika	k1gFnSc6	metrika
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
<	<	kIx(	<
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
<	<	kIx(	<
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
metrice	metrika	k1gFnSc6	metrika
s	s	k7c7	s
opačnou	opačný	k2eAgFnSc7d1	opačná
signaturou	signatura	k1gFnSc7	signatura
bude	být	k5eAaImBp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
>	>	kIx)	>
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Světelný	světelný	k2eAgInSc4d1	světelný
interval	interval	k1gInSc4	interval
-	-	kIx~	-
též	též	k6eAd1	též
světelného	světelný	k2eAgInSc2d1	světelný
charakteru	charakter	k1gInSc2	charakter
nebo	nebo	k8xC	nebo
také	také	k9	také
izotropní	izotropní	k2eAgMnSc1d1	izotropní
či	či	k8xC	či
nulový	nulový	k2eAgMnSc1d1	nulový
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
obě	dva	k4xCgFnPc1	dva
události	událost	k1gFnPc1	událost
spojeny	spojit	k5eAaPmNgFnP	spojit
pouze	pouze	k6eAd1	pouze
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
světelného	světelný	k2eAgInSc2d1	světelný
signálu	signál	k1gInSc2	signál
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
signálem	signál	k1gInSc7	signál
šířícím	šířící	k2eAgFnPc3d1	šířící
se	se	k3xPyFc4	se
rychlostí	rychlost	k1gFnSc7	rychlost
světla	světlo	k1gNnSc2	světlo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
událostmi	událost	k1gFnPc7	událost
existuje	existovat	k5eAaImIp3nS	existovat
příčinná	příčinný	k2eAgFnSc1d1	příčinná
souvislost	souvislost	k1gFnSc1	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
volbu	volba	k1gFnSc4	volba
metriky	metrika	k1gFnSc2	metrika
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Prostorupodobný	Prostorupodobný	k2eAgInSc4d1	Prostorupodobný
interval	interval	k1gInSc4	interval
-	-	kIx~	-
též	též	k6eAd1	též
prostorového	prostorový	k2eAgInSc2d1	prostorový
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
událostmi	událost	k1gFnPc7	událost
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
předána	předat	k5eAaPmNgFnS	předat
informace	informace	k1gFnSc1	informace
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
signálu	signál	k1gInSc2	signál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
podsvětelnou	podsvětelný	k2eAgFnSc7d1	podsvětelná
nebo	nebo	k8xC	nebo
světelnou	světelný	k2eAgFnSc7d1	světelná
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zvolené	zvolený	k2eAgFnSc6d1	zvolená
metrice	metrika	k1gFnSc6	metrika
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
>	>	kIx)	>
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
metrice	metrika	k1gFnSc6	metrika
s	s	k7c7	s
opačnou	opačný	k2eAgFnSc7d1	opačná
signaturou	signatura	k1gFnSc7	signatura
bude	být	k5eAaImBp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
<	<	kIx(	<
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
<	<	kIx(	<
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
prostoročasový	prostoročasový	k2eAgInSc4d1	prostoročasový
interval	interval	k1gInSc4	interval
větší	veliký	k2eAgFnSc1d2	veliký
nebo	nebo	k8xC	nebo
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
nula	nula	k1gFnSc1	nula
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
signatuře	signatura	k1gFnSc6	signatura
zvolené	zvolený	k2eAgFnSc2d1	zvolená
metriky	metrika	k1gFnSc2	metrika
<g/>
.	.	kIx.	.
</s>
<s>
Množinu	množina	k1gFnSc4	množina
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
od	od	k7c2	od
dané	daný	k2eAgFnSc2d1	daná
události	událost	k1gFnSc2	událost
A	a	k9	a
nulovou	nulový	k2eAgFnSc4d1	nulová
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
označujeme	označovat	k5eAaImIp1nP	označovat
světelný	světelný	k2eAgInSc4d1	světelný
kužel	kužel	k1gInSc4	kužel
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
prostoročas	prostoročas	k1gInSc4	prostoročas
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
oblasti	oblast	k1gFnPc4	oblast
<g/>
:	:	kIx,	:
absolutní	absolutní	k2eAgFnSc4d1	absolutní
minulost	minulost	k1gFnSc4	minulost
<g/>
,	,	kIx,	,
absolutní	absolutní	k2eAgFnSc4d1	absolutní
budoucnost	budoucnost	k1gFnSc4	budoucnost
a	a	k8xC	a
relativní	relativní	k2eAgFnSc4d1	relativní
současnost	současnost	k1gFnSc4	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Absolutní	absolutní	k2eAgFnSc7d1	absolutní
minulostí	minulost	k1gFnSc7	minulost
označujeme	označovat	k5eAaImIp1nP	označovat
ty	ten	k3xDgFnPc1	ten
události	událost	k1gFnPc1	událost
B	B	kA	B
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
pozorovatele	pozorovatel	k1gMnSc4	pozorovatel
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
události	událost	k1gFnSc2	událost
A	A	kA	A
<g/>
,	,	kIx,	,
absolutní	absolutní	k2eAgFnSc4d1	absolutní
budoucnost	budoucnost	k1gFnSc4	budoucnost
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
události	událost	k1gFnSc3	událost
B	B	kA	B
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
pozorovatele	pozorovatel	k1gMnSc4	pozorovatel
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
události	událost	k1gFnSc2	událost
A.	A.	kA	A.
Relativní	relativní	k2eAgFnSc1d1	relativní
současnost	současnost	k1gFnSc1	současnost
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
událostmi	událost	k1gFnPc7	událost
B	B	kA	B
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgFnPc4	jenž
existují	existovat	k5eAaImIp3nP	existovat
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
události	událost	k1gFnPc1	událost
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
současné	současný	k2eAgFnPc1d1	současná
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
některé	některý	k3yIgMnPc4	některý
jiné	jiný	k2eAgMnPc4d1	jiný
patří	patřit	k5eAaImIp3nP	patřit
B	B	kA	B
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
A	A	kA	A
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgMnPc4d1	ostatní
patří	patřit	k5eAaImIp3nP	patřit
B	B	kA	B
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
A.	A.	kA	A.
Čtyřvektor	Čtyřvektor	k1gMnSc1	Čtyřvektor
Speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
Obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
Teorie	teorie	k1gFnSc2	teorie
strun	struna	k1gFnPc2	struna
M-teorie	Meorie	k1gFnSc2	M-teorie
</s>
