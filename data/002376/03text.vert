<s>
Jako	jako	k8xS	jako
šmalkaldská	šmalkaldský	k2eAgFnSc1d1	šmalkaldská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Schmalkaldischer	Schmalkaldischra	k1gFnPc2	Schmalkaldischra
Krieg	Krieg	k1gInSc1	Krieg
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
obvykle	obvykle	k6eAd1	obvykle
označuje	označovat	k5eAaImIp3nS	označovat
období	období	k1gNnSc4	období
nepokojů	nepokoj	k1gInPc2	nepokoj
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1546	[number]	k4	1546
a	a	k8xC	a
1547	[number]	k4	1547
na	na	k7c6	na
území	území	k1gNnSc6	území
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
nepokoje	nepokoj	k1gInPc1	nepokoj
probíhaly	probíhat	k5eAaImAgInP	probíhat
mezi	mezi	k7c7	mezi
císařem	císař	k1gMnSc7	císař
Karlem	Karel	k1gMnSc7	Karel
V.	V.	kA	V.
a	a	k8xC	a
protestantskými	protestantský	k2eAgMnPc7d1	protestantský
říšskými	říšský	k2eAgMnPc7d1	říšský
knížaty	kníže	k1gMnPc7wR	kníže
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
válce	válka	k1gFnSc6	válka
sice	sice	k8xC	sice
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toto	tento	k3xDgNnSc1	tento
vítězství	vítězství	k1gNnSc1	vítězství
nedokázalo	dokázat	k5eNaPmAgNnS	dokázat
zvrátit	zvrátit	k5eAaPmF	zvrátit
postup	postup	k1gInSc4	postup
reformace	reformace	k1gFnSc2	reformace
a	a	k8xC	a
vydání	vydání	k1gNnSc2	vydání
augšpurského	augšpurský	k2eAgInSc2d1	augšpurský
míru	mír	k1gInSc2	mír
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1555	[number]	k4	1555
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
považoval	považovat	k5eAaImAgMnS	považovat
Karel	Karel	k1gMnSc1	Karel
za	za	k7c4	za
tak	tak	k6eAd1	tak
velkou	velký	k2eAgFnSc4d1	velká
prohru	prohra	k1gFnSc4	prohra
<g/>
,	,	kIx,	,
že	že	k8xS	že
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
z	z	k7c2	z
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
samotná	samotný	k2eAgFnSc1d1	samotná
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
jistý	jistý	k2eAgInSc4d1	jistý
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
země	zem	k1gFnPc4	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
požádal	požádat	k5eAaPmAgMnS	požádat
české	český	k2eAgInPc4d1	český
stavy	stav	k1gInPc4	stav
aby	aby	kYmCp3nP	aby
svolali	svolat	k5eAaPmAgMnP	svolat
zemskou	zemský	k2eAgFnSc4d1	zemská
hotovost	hotovost	k1gFnSc4	hotovost
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
císaři	císař	k1gMnSc3	císař
Karlu	Karel	k1gMnSc3	Karel
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
bratrovi	bratrův	k2eAgMnPc1d1	bratrův
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
však	však	k9	však
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
a	a	k8xC	a
následně	následně	k6eAd1	následně
po	po	k7c6	po
katolickém	katolický	k2eAgNnSc6d1	katolické
vítězství	vítězství	k1gNnSc6	vítězství
byli	být	k5eAaImAgMnP	být
za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
nuceni	nutit	k5eAaImNgMnP	nutit
k	k	k7c3	k
určitým	určitý	k2eAgInPc3d1	určitý
mocenským	mocenský	k2eAgInPc3d1	mocenský
ústupkům	ústupek	k1gInPc3	ústupek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
dotklo	dotknout	k5eAaPmAgNnS	dotknout
především	především	k6eAd1	především
městského	městský	k2eAgInSc2d1	městský
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1530	[number]	k4	1530
utvořilo	utvořit	k5eAaPmAgNnS	utvořit
v	v	k7c6	v
Šmalkaldech	Šmalkald	k1gInPc6	Šmalkald
devět	devět	k4xCc4	devět
protestantských	protestantský	k2eAgMnPc2d1	protestantský
knížat	kníže	k1gMnPc2wR	kníže
a	a	k8xC	a
jedenáct	jedenáct	k4xCc1	jedenáct
říšských	říšský	k2eAgNnPc2d1	říšské
měst	město	k1gNnPc2	město
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
Šmalkaldský	Šmalkaldský	k2eAgInSc1d1	Šmalkaldský
spolek	spolek	k1gInSc1	spolek
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
proti	proti	k7c3	proti
každému	každý	k3xTgInSc3	každý
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
protestantskou	protestantský	k2eAgFnSc4d1	protestantská
víru	víra	k1gFnSc4	víra
a	a	k8xC	a
politickou	politický	k2eAgFnSc4d1	politická
samostatnost	samostatnost	k1gFnSc4	samostatnost
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
V.	V.	kA	V.
a	a	k8xC	a
katolických	katolický	k2eAgInPc2d1	katolický
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
náčelníky	náčelník	k1gMnPc7	náčelník
byli	být	k5eAaImAgMnP	být
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
Jan	Jan	k1gMnSc1	Jan
Fridrich	Fridrich	k1gMnSc1	Fridrich
<g/>
,	,	kIx,	,
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
saský	saský	k2eAgMnSc1d1	saský
<g/>
,	,	kIx,	,
a	a	k8xC	a
Filip	Filip	k1gMnSc1	Filip
<g/>
,	,	kIx,	,
lankrabě	lankrabě	k1gMnSc1	lankrabě
hesenský	hesenský	k2eAgMnSc1d1	hesenský
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
spolku	spolek	k1gInSc2	spolek
navázali	navázat	k5eAaPmAgMnP	navázat
hned	hned	k6eAd1	hned
styky	styk	k1gInPc1	styk
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
,	,	kIx,	,
odepřeli	odepřít	k5eAaPmAgMnP	odepřít
Karlu	Karla	k1gFnSc4	Karla
V.	V.	kA	V.
pomoc	pomoc	k1gFnSc4	pomoc
proti	proti	k7c3	proti
Turkům	turek	k1gInPc3	turek
a	a	k8xC	a
neuznali	uznat	k5eNaPmAgMnP	uznat
jeho	jeho	k3xOp3gMnSc4	jeho
bratra	bratr	k1gMnSc4	bratr
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
za	za	k7c4	za
římského	římský	k2eAgMnSc4d1	římský
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
byl	být	k5eAaImAgMnS	být
nucen	nucen	k2eAgMnSc1d1	nucen
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1532	[number]	k4	1532
povolit	povolit	k5eAaPmF	povolit
náboženský	náboženský	k2eAgInSc4d1	náboženský
norimberský	norimberský	k2eAgInSc4d1	norimberský
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k1gFnSc1	moc
spolku	spolek	k1gInSc2	spolek
pak	pak	k6eAd1	pak
neustále	neustále	k6eAd1	neustále
vzrůstala	vzrůstat	k5eAaImAgFnS	vzrůstat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
vzrůstem	vzrůst	k1gInSc7	vzrůst
protestantismu	protestantismus	k1gInSc2	protestantismus
mu	on	k3xPp3gNnSc3	on
přibývalo	přibývat	k5eAaImAgNnS	přibývat
i	i	k9	i
nových	nový	k2eAgInPc2d1	nový
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
spolku	spolek	k1gInSc2	spolek
postupovali	postupovat	k5eAaImAgMnP	postupovat
společně	společně	k6eAd1	společně
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
náboženských	náboženský	k2eAgFnPc2d1	náboženská
a	a	k8xC	a
pořádali	pořádat	k5eAaImAgMnP	pořádat
roku	rok	k1gInSc2	rok
1537	[number]	k4	1537
ve	v	k7c6	v
Šmalkaldech	Šmalkaldo	k1gNnPc6	Šmalkaldo
schůzi	schůze	k1gFnSc3	schůze
<g/>
,	,	kIx,	,
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
byly	být	k5eAaImAgInP	být
podepsány	podepsán	k2eAgInPc1d1	podepsán
šmalkaldské	šmalkaldský	k2eAgInPc1d1	šmalkaldský
články	článek	k1gInPc1	článek
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
usneseno	usnesen	k2eAgNnSc1d1	usneseno
neobeslat	obeslat	k5eNaPmF	obeslat
žádný	žádný	k3yNgInSc4	žádný
koncil	koncil	k1gInSc4	koncil
mimo	mimo	k7c4	mimo
svatou	svatý	k2eAgFnSc4d1	svatá
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
přiostřil	přiostřit	k5eAaPmAgInS	přiostřit
poměr	poměr	k1gInSc1	poměr
obou	dva	k4xCgFnPc2	dva
náboženských	náboženský	k2eAgFnPc2d1	náboženská
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
když	když	k8xS	když
katoličtí	katolický	k2eAgMnPc1d1	katolický
stavové	stavový	k2eAgFnPc1d1	stavová
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
utvořili	utvořit	k5eAaPmAgMnP	utvořit
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1538	[number]	k4	1538
"	"	kIx"	"
<g/>
norimberský	norimberský	k2eAgInSc1d1	norimberský
spolek	spolek	k1gInSc1	spolek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
císař	císař	k1gMnSc1	císař
ukončil	ukončit	k5eAaPmAgMnS	ukončit
války	válek	k1gInPc4	válek
s	s	k7c7	s
Francii	Franci	k1gMnPc7	Franci
a	a	k8xC	a
Turky	Turek	k1gMnPc7	Turek
<g/>
,	,	kIx,	,
chystal	chystat	k5eAaImAgMnS	chystat
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
ukončiti	ukončit	k5eAaPmF	ukončit
náboženské	náboženský	k2eAgInPc4d1	náboženský
spory	spor	k1gInPc4	spor
pomocí	pomocí	k7c2	pomocí
tridentského	tridentský	k2eAgInSc2d1	tridentský
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
členové	člen	k1gMnPc1	člen
Šmalkaldského	Šmalkaldský	k2eAgInSc2d1	Šmalkaldský
spolku	spolek	k1gInSc2	spolek
svoji	svůj	k3xOyFgFnSc4	svůj
účast	účast	k1gFnSc4	účast
odepřeli	odepřít	k5eAaPmAgMnP	odepřít
<g/>
,	,	kIx,	,
chystal	chystat	k5eAaImAgMnS	chystat
se	se	k3xPyFc4	se
císař	císař	k1gMnSc1	císař
zlomit	zlomit	k5eAaPmF	zlomit
jejich	jejich	k3xOp3gInSc4	jejich
odpor	odpor	k1gInSc4	odpor
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tajnými	tajný	k2eAgFnPc7d1	tajná
smlouvami	smlouva	k1gFnPc7	smlouva
zajistil	zajistit	k5eAaPmAgMnS	zajistit
si	se	k3xPyFc3	se
podporu	podpora	k1gFnSc4	podpora
katolických	katolický	k2eAgMnPc2d1	katolický
stavů	stav	k1gInPc2	stav
i	i	k8xC	i
protestanského	protestanský	k2eAgMnSc2d1	protestanský
vévody	vévoda	k1gMnSc2	vévoda
Mořice	Mořic	k1gMnSc2	Mořic
Saského	saský	k2eAgMnSc2d1	saský
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
vykonavatelem	vykonavatel	k1gMnSc7	vykonavatel
říšské	říšský	k2eAgFnSc2d1	říšská
klatby	klatba	k1gFnSc2	klatba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
roku	rok	k1gInSc2	rok
1546	[number]	k4	1546
v	v	k7c6	v
Řezně	Řezno	k1gNnSc6	Řezno
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
začala	začít	k5eAaPmAgFnS	začít
šmalkaldská	šmalkaldský	k2eAgFnSc1d1	šmalkaldská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nesvornost	Nesvornost	k1gFnSc1	Nesvornost
v	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
protestantském	protestantský	k2eAgInSc6d1	protestantský
dovolovala	dovolovat	k5eAaImAgFnS	dovolovat
Karlovi	Karel	k1gMnSc3	Karel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
připravil	připravit	k5eAaPmAgMnS	připravit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
spojenci	spojenec	k1gMnPc1	spojenec
měli	mít	k5eAaImAgMnP	mít
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
války	válka	k1gFnSc2	válka
značnou	značný	k2eAgFnSc4d1	značná
přesilu	přesila	k1gFnSc4	přesila
<g/>
.	.	kIx.	.
</s>
<s>
Polní	polní	k2eAgMnSc1d1	polní
velitel	velitel	k1gMnSc1	velitel
Sebastian	Sebastian	k1gMnSc1	Sebastian
Schertlin	Schertlin	k2eAgInSc4d1	Schertlin
von	von	k1gInSc4	von
Burtenbach	Burtenbach	k1gInSc1	Burtenbach
(	(	kIx(	(
<g/>
též	též	k9	též
Schärtlin	Schärtlin	k1gInSc1	Schärtlin
<g/>
)	)	kIx)	)
manévroval	manévrovat	k5eAaImAgInS	manévrovat
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
hornoněmeckých	hornoněmecký	k2eAgNnPc2d1	hornoněmecké
měst	město	k1gNnPc2	město
podél	podél	k7c2	podél
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zavřel	zavřít	k5eAaPmAgMnS	zavřít
cestu	cesta	k1gFnSc4	cesta
císařskému	císařský	k2eAgInSc3d1	císařský
vojsku	vojsko	k1gNnSc3	vojsko
táhnoucímu	táhnoucí	k2eAgInSc3d1	táhnoucí
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nesvornost	nesvornost	k1gFnSc1	nesvornost
vrchních	vrchní	k2eAgMnPc2d1	vrchní
velitelů	velitel	k1gMnPc2	velitel
zmařila	zmařit	k5eAaPmAgFnS	zmařit
plány	plán	k1gInPc7	plán
tohoto	tento	k3xDgMnSc4	tento
výborného	výborný	k2eAgMnSc4d1	výborný
vojevůdce	vojevůdce	k1gMnSc4	vojevůdce
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Karlovi	Karel	k1gMnSc3	Karel
byla	být	k5eAaImAgFnS	být
zbytečně	zbytečně	k6eAd1	zbytečně
poskytnuta	poskytnut	k2eAgFnSc1d1	poskytnuta
příležitost	příležitost	k1gFnSc1	příležitost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nejen	nejen	k6eAd1	nejen
italské	italský	k2eAgNnSc1d1	italské
vojsko	vojsko	k1gNnSc1	vojsko
na	na	k7c6	na
Dunaji	Dunaj	k1gInSc6	Dunaj
soustředil	soustředit	k5eAaPmAgMnS	soustředit
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k9	i
pomoc	pomoc	k1gFnSc4	pomoc
z	z	k7c2	z
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
přivolal	přivolat	k5eAaPmAgInS	přivolat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
jih	jih	k1gInSc4	jih
zvěst	zvěst	k1gFnSc1	zvěst
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mořic	Mořic	k1gMnSc1	Mořic
Saský	saský	k2eAgMnSc1d1	saský
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
na	na	k7c6	na
území	území	k1gNnSc6	území
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
Jana	Jan	k1gMnSc2	Jan
Fridricha	Fridrich	k1gMnSc2	Fridrich
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
saský	saský	k2eAgMnSc1d1	saský
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
odebrali	odebrat	k5eAaPmAgMnP	odebrat
se	se	k3xPyFc4	se
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
zemí	zem	k1gFnPc2	zem
i	i	k8xC	i
ostatní	ostatní	k2eAgMnPc1d1	ostatní
spojenci	spojenec	k1gMnPc1	spojenec
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
celé	celý	k2eAgNnSc1d1	celé
šmalkaldské	šmalkaldský	k2eAgNnSc1d1	šmalkaldský
vojsko	vojsko	k1gNnSc1	vojsko
se	se	k3xPyFc4	se
rozešlo	rozejít	k5eAaPmAgNnS	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Spojenci	spojenec	k1gMnPc1	spojenec
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Německu	Německo	k1gNnSc6	Německo
i	i	k8xC	i
jiná	jiný	k2eAgNnPc4d1	jiné
knížata	kníže	k1gNnPc4	kníže
a	a	k8xC	a
města	město	k1gNnPc4	město
prosili	prosit	k5eAaImAgMnP	prosit
císaře	císař	k1gMnSc4	císař
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
Jan	Jan	k1gMnSc1	Jan
Fridrich	Fridrich	k1gMnSc1	Fridrich
nejen	nejen	k6eAd1	nejen
vypudil	vypudit	k5eAaPmAgMnS	vypudit
Mořice	Mořic	k1gMnSc2	Mořic
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
opanoval	opanovat	k5eAaPmAgMnS	opanovat
i	i	k9	i
jeho	jeho	k3xOp3gFnPc4	jeho
vlastní	vlastní	k2eAgFnPc4d1	vlastní
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Severoněmečtí	severoněmecký	k2eAgMnPc1d1	severoněmecký
spojenci	spojenec	k1gMnPc1	spojenec
stáli	stát	k5eAaImAgMnP	stát
věrně	věrně	k6eAd1	věrně
při	při	k7c6	při
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
se	se	k3xPyFc4	se
protestantští	protestantský	k2eAgMnPc1d1	protestantský
stavové	stavový	k2eAgNnSc4d1	stavové
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
mu	on	k3xPp3gMnSc3	on
přispět	přispět	k5eAaPmF	přispět
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
ano	ano	k9	ano
i	i	k9	i
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Anglie	Anglie	k1gFnSc2	Anglie
navázaly	navázat	k5eAaPmAgInP	navázat
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
kurfiřtem	kurfiřt	k1gMnSc7	kurfiřt
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
přitáhl	přitáhnout	k5eAaPmAgMnS	přitáhnout
s	s	k7c7	s
veškerou	veškerý	k3xTgFnSc7	veškerý
brannou	branný	k2eAgFnSc4d1	Branná
moci	moct	k5eAaImF	moct
a	a	k8xC	a
porazil	porazit	k5eAaPmAgMnS	porazit
spolkové	spolkový	k2eAgNnSc4d1	Spolkové
vojsko	vojsko	k1gNnSc4	vojsko
u	u	k7c2	u
Mühlberka	Mühlberka	k1gFnSc1	Mühlberka
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1547	[number]	k4	1547
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Fridrich	Fridrich	k1gMnSc1	Fridrich
byl	být	k5eAaImAgMnS	být
zajat	zajat	k2eAgMnSc1d1	zajat
<g/>
,	,	kIx,	,
lankrabí	lankrabí	k1gMnSc1	lankrabí
Filip	Filip	k1gMnSc1	Filip
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
vzdal	vzdát	k5eAaPmAgMnS	vzdát
po	po	k7c6	po
neobratném	obratný	k2eNgNnSc6d1	neobratné
Mořicově	Mořicův	k2eAgNnSc6d1	Mořicův
prostředkování	prostředkování	k1gNnSc6	prostředkování
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
pak	pak	k6eAd1	pak
držen	držet	k5eAaImNgMnS	držet
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
severoněmečtí	severoněmecký	k2eAgMnPc1d1	severoněmecký
členové	člen	k1gMnPc1	člen
spolku	spolek	k1gInSc2	spolek
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
Magdeburgu	Magdeburg	k1gInSc2	Magdeburg
a	a	k8xC	a
Brém	Brémy	k1gFnPc2	Brémy
<g/>
,	,	kIx,	,
vzdali	vzdát	k5eAaPmAgMnP	vzdát
se	se	k3xPyFc4	se
císaři	císař	k1gMnPc1	císař
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
spolek	spolek	k1gInSc1	spolek
úplně	úplně	k6eAd1	úplně
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
.	.	kIx.	.
</s>
