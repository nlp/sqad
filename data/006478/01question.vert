<s>
Proč	proč	k6eAd1	proč
Platón	Platón	k1gMnSc1	Platón
nebyl	být	k5eNaImAgMnS	být
podle	podle	k7c2	podle
Faidóna	Faidón	k1gMnSc2	Faidón
u	u	k7c2	u
Sókratovy	Sókratův	k2eAgFnSc2d1	Sókratova
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc7	jeho
žákem	žák	k1gMnSc7	žák
<g/>
?	?	kIx.	?
</s>
