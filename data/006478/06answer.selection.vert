<s>
Platón	Platón	k1gMnSc1	Platón
sám	sám	k3xTgMnSc1	sám
to	ten	k3xDgNnSc4	ten
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
dialozích	dialog	k1gInPc6	dialog
nikde	nikde	k6eAd1	nikde
výslovně	výslovně	k6eAd1	výslovně
neříká	říkat	k5eNaImIp3nS	říkat
a	a	k8xC	a
podle	podle	k7c2	podle
dialogu	dialog	k1gInSc2	dialog
"	"	kIx"	"
<g/>
Faidón	Faidón	k1gInSc1	Faidón
<g/>
"	"	kIx"	"
nebyl	být	k5eNaImAgInS	být
při	při	k7c6	při
Sókratově	Sókratův	k2eAgFnSc6d1	Sókratova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
nemocen	nemocen	k2eAgMnSc1d1	nemocen
<g/>
.	.	kIx.	.
</s>
