<s>
Denár	denár	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
stříbrné	stříbrný	k2eAgFnSc6d1
minci	mince	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
české	český	k2eAgFnSc6d1
jednotce	jednotka	k1gFnSc6
hmotnosti	hmotnost	k1gFnSc2
užívané	užívaný	k2eAgFnSc2d1
v	v	k7c6
mincovnictí	mincovnictí	k1gNnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
denár	denár	k1gInSc1
(	(	kIx(
<g/>
jednotka	jednotka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Denár	denár	k1gInSc1
Vratislava	Vratislav	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
strana	strana	k1gFnSc1
mince	mince	k1gFnSc2
</s>
<s>
Denár	denár	k1gInSc1
Vratislava	Vratislav	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
strana	strana	k1gFnSc1
mince	mince	k1gFnSc2
</s>
<s>
Denár	denár	k1gInSc1
je	být	k5eAaImIp3nS
stříbrná	stříbrný	k2eAgFnSc1d1
mince	mince	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
poprvé	poprvé	k6eAd1
ražena	razit	k5eAaImNgFnS
na	na	k7c4
území	území	k1gNnSc4
římského	římský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
slova	slovo	k1gNnSc2
denarius	denarius	k1gInSc4
později	pozdě	k6eAd2
vznikl	vzniknout	k5eAaPmAgInS
název	název	k1gInSc1
denár	denár	k1gInSc4
pro	pro	k7c4
středověké	středověký	k2eAgFnPc4d1
a	a	k8xC
novověké	novověký	k2eAgFnPc4d1
mince	mince	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
je	být	k5eAaImIp3nS
z	z	k7c2
něj	on	k3xPp3gMnSc2
odvozen	odvozen	k2eAgInSc1d1
název	název	k1gInSc4
britské	britský	k2eAgFnSc2d1
mince	mince	k1gFnSc2
penny	penny	k1gFnSc2
(	(	kIx(
<g/>
značka	značka	k1gFnSc1
d.	d.	k?
podle	podle	k7c2
francouzské	francouzský	k2eAgFnSc2d1
mince	mince	k1gFnSc2
denier	denira	k1gFnPc2
<g/>
)	)	kIx)
či	či	k8xC
název	název	k1gInSc1
platidla	platidlo	k1gNnSc2
dinár	dinár	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Římský	římský	k2eAgInSc1d1
denár	denár	k1gInSc1
</s>
<s>
Denarius	Denarius	k1gInSc4
bylo	být	k5eAaImAgNnS
stříbrné	stříbrný	k2eAgNnSc1d1
starořímské	starořímský	k2eAgNnSc1d1
platidlo	platidlo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
platilo	platit	k5eAaImAgNnS
v	v	k7c6
období	období	k1gNnSc6
římské	římský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
i	i	k9
později	pozdě	k6eAd2
římského	římský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
byl	být	k5eAaImAgInS
ražen	razit	k5eAaImNgInS
již	již	k6eAd1
roku	rok	k1gInSc2
211	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Název	název	k1gInSc1
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
z	z	k7c2
lat.	lat.	k?
dena	den	k1gInSc2
(	(	kIx(
<g/>
deset	deset	k4xCc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protože	protože	k8xS
denár	denár	k1gInSc1
se	se	k3xPyFc4
dělil	dělit	k5eAaImAgInS
na	na	k7c4
10	#num#	k4
měděných	měděný	k2eAgFnPc2d1
mincí	mince	k1gFnPc2
<g/>
,	,	kIx,
asů	as	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
vážil	vážit	k5eAaImAgInS
4,55	4,55	k4
g	g	kA
<g/>
,	,	kIx,
za	za	k7c2
vlády	vláda	k1gFnSc2
Nerona	Nero	k1gMnSc2
už	už	k6eAd1
pouhých	pouhý	k2eAgFnPc2d1
3,4	3,4	k4
g.	g.	k?
Septimius	Septimius	k1gMnSc1
Severus	Severus	k1gMnSc1
zvýšil	zvýšit	k5eAaPmAgMnS
příměs	příměs	k1gFnSc4
mědi	měď	k1gFnSc2
v	v	k7c6
denáru	denár	k1gInSc6
na	na	k7c4
50	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Denár	denár	k1gInSc1
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
</s>
<s>
Stejný	stejný	k2eAgInSc1d1
název	název	k1gInSc1
dostaly	dostat	k5eAaPmAgFnP
i	i	k8xC
první	první	k4xOgFnPc4
stříbrné	stříbrný	k2eAgFnPc4d1
mince	mince	k1gFnPc4
ražené	ražený	k2eAgFnPc4d1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
koncem	koncem	k7c2
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
zavedeny	zavést	k5eAaPmNgInP
Boleslavem	Boleslav	k1gMnSc7
I.	I.	kA
Až	až	k6eAd1
do	do	k7c2
mincovní	mincovní	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
Břetislava	Břetislav	k1gMnSc2
I.	I.	kA
roku	rok	k1gInSc2
1050	#num#	k4
byly	být	k5eAaImAgInP
denáry	denár	k1gInPc1
tzv.	tzv.	kA
velkého	velký	k2eAgInSc2d1
střížku	střížek	k1gInSc2
tj.	tj.	kA
průměr	průměr	k1gInSc4
18	#num#	k4
–	–	k?
20	#num#	k4
mm	mm	kA
a	a	k8xC
hmotnosti	hmotnost	k1gFnSc2
více	hodně	k6eAd2
než	než	k8xS
1	#num#	k4
g.	g.	k?
Nálezy	nález	k1gInPc1
velkého	velký	k2eAgInSc2d1
střížku	střížek	k1gInSc2
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
více	hodně	k6eAd2
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
než	než	k8xS
v	v	k7c6
Česku	Česko	k1gNnSc6
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
Plzni	Plzeň	k1gFnSc6
a	a	k8xC
v	v	k7c6
jejich	jejich	k3xOp3gNnSc6
okolí	okolí	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1050	#num#	k4
až	až	k9
do	do	k7c2
začátku	začátek	k1gInSc2
ražby	ražba	k1gFnSc2
brakteátů	brakteát	k1gInPc2
někdy	někdy	k6eAd1
okolo	okolo	k7c2
roku	rok	k1gInSc2
1217	#num#	k4
byly	být	k5eAaImAgInP
raženy	ražen	k2eAgInPc1d1
denáry	denár	k1gInPc1
malého	malý	k2eAgInSc2d1
střížku	střížek	k1gInSc2
o	o	k7c6
průměru	průměr	k1gInSc6
16	#num#	k4
mm	mm	kA
a	a	k8xC
hmotnosti	hmotnost	k1gFnSc2
pod	pod	k7c7
1	#num#	k4
g.	g.	k?
Ke	k	k7c3
konci	konec	k1gInSc3
ražby	ražba	k1gFnSc2
ryzost	ryzost	k1gFnSc1
kovu	kov	k1gInSc2
výrazně	výrazně	k6eAd1
poklesla	poklesnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Denáry	denár	k1gInPc1
mohly	moct	k5eAaImAgInP
být	být	k5eAaImF
například	například	k6eAd1
šípového	šípový	k2eAgInSc2d1
typu	typ	k1gInSc2
nebo	nebo	k8xC
mečového	mečový	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
opisu	opis	k1gInSc6
mince	mince	k1gFnSc2
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
napsáno	napsán	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
panovníka	panovník	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
končí	končit	k5eAaImIp3nS
slovem	slovo	k1gNnSc7
dux	dux	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývají	bývat	k5eAaImIp3nP
tam	tam	k6eAd1
také	také	k9
napsány	napsán	k2eAgInPc4d1
názvy	název	k1gInPc4
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
mince	mince	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
však	však	k9
názvy	název	k1gInPc1
mincoven	mincovna	k1gFnPc2
mizely	mizet	k5eAaImAgInP
a	a	k8xC
nahradily	nahradit	k5eAaPmAgInP
se	s	k7c7
jmény	jméno	k1gNnPc7
světců	světec	k1gMnPc2
a	a	k8xC
patronů	patron	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Ražba	ražba	k1gFnSc1
denáru	denár	k1gInSc2
v	v	k7c6
Českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
zanikla	zaniknout	k5eAaPmAgFnS
roku	rok	k1gInSc2
1300	#num#	k4
mincovní	mincovní	k2eAgFnSc7d1
reformou	reforma	k1gFnSc7
Václava	Václav	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
už	už	k6eAd1
mezi	mezi	k7c7
lidem	lid	k1gInSc7
málo	málo	k6eAd1
oblíbený	oblíbený	k2eAgInSc4d1
denár	denár	k1gInSc4
nahradil	nahradit	k5eAaPmAgMnS
později	pozdě	k6eAd2
velmi	velmi	k6eAd1
slavný	slavný	k2eAgInSc1d1
pražský	pražský	k2eAgInSc1d1
groš	groš	k1gInSc1
z	z	k7c2
mincovny	mincovna	k1gFnSc2
v	v	k7c6
Kutné	kutný	k2eAgFnSc6d1
Hoře	hora	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ražba	ražba	k1gFnSc1
denáru	denár	k1gInSc2
pokračovala	pokračovat	k5eAaImAgFnS
ještě	ještě	k9
po	po	k7c6
staletí	staletí	k1gNnPc4
v	v	k7c6
Uhrách	Uhry	k1gFnPc6
a	a	k8xC
uherský	uherský	k2eAgInSc4d1
denár	denár	k1gInSc4
s	s	k7c7
typickým	typický	k2eAgInSc7d1
obrazem	obraz	k1gInSc7
madony	madona	k1gFnSc2
ukončila	ukončit	k5eAaPmAgFnS
až	až	k9
rakouská	rakouský	k2eAgFnSc1d1
císařovna	císařovna	k1gFnSc1
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC
početní	početní	k2eAgFnSc1d1
peněžní	peněžní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
se	se	k3xPyFc4
ovšem	ovšem	k9
denár	denár	k1gInSc1
v	v	k7c6
Českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
udržel	udržet	k5eAaPmAgInS
až	až	k6eAd1
do	do	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	s	k7c7
1	#num#	k4
groš	groš	k1gInSc1
=	=	kIx~
7	#num#	k4
denárů	denár	k1gInPc2
a	a	k8xC
později	pozdě	k6eAd2
ve	v	k7c6
zlatkové	zlatkový	k2eAgFnSc6d1
měně	měna	k1gFnSc6
se	s	k7c7
1	#num#	k4
krejcar	krejcar	k1gInSc1
=	=	kIx~
6	#num#	k4
denárů	denár	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
České	český	k2eAgInPc1d1
denáry	denár	k1gInPc1
</s>
<s>
České	český	k2eAgFnSc2d1
denárové	denárový	k2eAgFnSc2d1
mince	mince	k1gFnSc2
razili	razit	k5eAaImAgMnP
následující	následující	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
<g/>
,	,	kIx,
v	v	k7c6
závorce	závorka	k1gFnSc6
je	být	k5eAaImIp3nS
uvedeno	uvést	k5eAaPmNgNnS
datum	datum	k1gNnSc1
ražby	ražba	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
Václav	Václav	k1gMnSc1
(	(	kIx(
<g/>
921	#num#	k4
<g/>
–	–	k?
<g/>
935	#num#	k4
<g/>
)	)	kIx)
–	–	k?
jsou	být	k5eAaImIp3nP
známy	znám	k2eAgInPc1d1
denáry	denár	k1gInPc1
s	s	k7c7
jeho	jeho	k3xOp3gNnSc7
jménem	jméno	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
nedají	dát	k5eNaPmIp3nP
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
s	s	k7c7
určitostí	určitost	k1gFnSc7
přisoudit	přisoudit	k5eAaPmF
</s>
<s>
Boleslav	Boleslav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
935	#num#	k4
<g/>
–	–	k?
<g/>
972	#num#	k4
<g/>
)	)	kIx)
–	–	k?
razil	razit	k5eAaImAgInS
první	první	k4xOgInSc4
historicky	historicky	k6eAd1
doložené	doložený	k2eAgFnSc2d1
mince	mince	k1gFnSc2
Českého	český	k2eAgInSc2d1
státu	stát	k1gInSc2
</s>
<s>
Boleslav	Boleslav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
972	#num#	k4
<g/>
–	–	k?
<g/>
999	#num#	k4
<g/>
)	)	kIx)
–	–	k?
na	na	k7c6
mincích	mince	k1gFnPc6
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
motivy	motiv	k1gInPc1
převzaté	převzatý	k2eAgInPc1d1
z	z	k7c2
anglických	anglický	k2eAgFnPc2d1
mincí	mince	k1gFnPc2
(	(	kIx(
<g/>
meč	meč	k1gInSc1
<g/>
,	,	kIx,
pták	pták	k1gMnSc1
<g/>
,	,	kIx,
luk	luk	k1gInSc1
se	s	k7c7
šípem	šíp	k1gInSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
založil	založit	k5eAaPmAgMnS
mincovnu	mincovna	k1gFnSc4
na	na	k7c6
Vyšehradě	Vyšehrad	k1gInSc6
</s>
<s>
Boleslav	Boleslav	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
999	#num#	k4
<g/>
–	–	k?
<g/>
1002	#num#	k4
<g/>
)	)	kIx)
–	–	k?
razil	razit	k5eAaImAgMnS
mince	mince	k1gFnPc4
řezenského	řezenský	k2eAgInSc2d1
typu	typ	k1gInSc2
</s>
<s>
Vladivoj	Vladivoj	k1gInSc1
(	(	kIx(
<g/>
1002	#num#	k4
<g/>
–	–	k?
<g/>
1003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Boleslav	Boleslav	k1gMnSc1
Chrabrý	chrabrý	k2eAgMnSc1d1
(	(	kIx(
<g/>
1003	#num#	k4
<g/>
–	–	k?
<g/>
1004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jaromír	Jaromír	k1gMnSc1
(	(	kIx(
<g/>
1003	#num#	k4
<g/>
,	,	kIx,
1004	#num#	k4
<g/>
–	–	k?
<g/>
1012	#num#	k4
<g/>
,	,	kIx,
1033	#num#	k4
<g/>
–	–	k?
<g/>
1034	#num#	k4
<g/>
)	)	kIx)
–	–	k?
mince	mince	k1gFnSc1
převážně	převážně	k6eAd1
karolinského	karolinský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
mince	mince	k1gFnSc1
s	s	k7c7
nápisem	nápis	k1gInSc7
(	(	kIx(
<g/>
jméno	jméno	k1gNnSc1
svatého	svatý	k2eAgMnSc2d1
Václava	Václav	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
(	(	kIx(
<g/>
1012	#num#	k4
<g/>
–	–	k?
<g/>
1033	#num#	k4
<g/>
,	,	kIx,
1034	#num#	k4
<g/>
)	)	kIx)
–	–	k?
na	na	k7c6
lícní	lícní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
často	často	k6eAd1
vyobrazen	vyobrazen	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
s	s	k7c7
péřovou	péřový	k2eAgFnSc7d1
čelenkou	čelenka	k1gFnSc7
</s>
<s>
Břetislav	Břetislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1035	#num#	k4
<g/>
–	–	k?
<g/>
1055	#num#	k4
<g/>
)	)	kIx)
–	–	k?
na	na	k7c6
lícní	lícní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
vyobrazena	vyobrazen	k2eAgFnSc1d1
postava	postava	k1gFnSc1
knížete	kníže	k1gMnSc2
či	či	k8xC
kníže	kníže	k1gMnSc1
sedící	sedící	k2eAgMnSc1d1
na	na	k7c6
koni	kůň	k1gMnSc6
<g/>
,	,	kIx,
na	na	k7c6
rubu	rub	k1gInSc6
svatý	svatý	k2eAgMnSc1d1
Václav	Václav	k1gMnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
motiv	motiv	k1gInSc4
ptáka	pták	k1gMnSc2
</s>
<s>
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1050	#num#	k4
byla	být	k5eAaImAgFnS
uskutečněna	uskutečnit	k5eAaPmNgFnS
měnová	měnový	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
<g/>
,	,	kIx,
nově	nově	k6eAd1
ražené	ražený	k2eAgInPc1d1
denáry	denár	k1gInPc1
mají	mít	k5eAaImIp3nP
menší	malý	k2eAgInPc1d2
rozměry	rozměr	k1gInPc1
a	a	k8xC
hmotnost	hmotnost	k1gFnSc1
<g/>
,	,	kIx,
jakost	jakost	k1gFnSc1
stříbra	stříbro	k1gNnSc2
byla	být	k5eAaImAgFnS
určena	určit	k5eAaPmNgFnS
na	na	k7c4
0,960	0,960	k4
</s>
<s>
Spytihněv	Spytihnět	k5eAaPmDgInS
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1055	#num#	k4
<g/>
–	–	k?
<g/>
1061	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1061	#num#	k4
<g/>
–	–	k?
<g/>
1092	#num#	k4
<g/>
)	)	kIx)
–	–	k?
za	za	k7c2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rychlému	rychlý	k2eAgNnSc3d1
znehodnocení	znehodnocení	k1gNnSc3
měny	měna	k1gFnSc2
</s>
<s>
Konrád	Konrád	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1092	#num#	k4
<g/>
)	)	kIx)
–	–	k?
razil	razit	k5eAaImAgMnS
mince	mince	k1gFnPc4
pouze	pouze	k6eAd1
na	na	k7c6
Moravě	Morava	k1gFnSc6
</s>
<s>
Břetislav	Břetislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1092	#num#	k4
<g/>
–	–	k?
<g/>
1100	#num#	k4
<g/>
)	)	kIx)
–	–	k?
svým	svůj	k3xOyFgInSc7
vzhledem	vzhled	k1gInSc7
a	a	k8xC
zpracováním	zpracování	k1gNnSc7
patřily	patřit	k5eAaImAgFnP
k	k	k7c3
nejkvalitnějším	kvalitní	k2eAgMnPc3d3
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
jejich	jejich	k3xOp3gFnSc1
kupní	kupní	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
nebyla	být	k5eNaImAgFnS
vysoká	vysoký	k2eAgFnSc1d1
</s>
<s>
Bořivoj	Bořivoj	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1101	#num#	k4
<g/>
–	–	k?
<g/>
1007	#num#	k4
<g/>
,	,	kIx,
1017	#num#	k4
<g/>
–	–	k?
<g/>
1020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Svatopluk	Svatopluk	k1gMnSc1
(	(	kIx(
<g/>
1107	#num#	k4
<g/>
–	–	k?
<g/>
1109	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1109	#num#	k4
<g/>
–	–	k?
<g/>
1118	#num#	k4
<g/>
,	,	kIx,
1120	#num#	k4
<g/>
–	–	k?
<g/>
1125	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Soběslav	Soběslav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1125	#num#	k4
<g/>
–	–	k?
<g/>
1140	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1140	#num#	k4
<g/>
–	–	k?
<g/>
1158	#num#	k4
<g/>
)	)	kIx)
–	–	k?
denáry	denár	k1gInPc1
měly	mít	k5eAaImAgInP
nízkou	nízký	k2eAgFnSc4d1
jakost	jakost	k1gFnSc4
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
až	až	k9
0,050	0,050	k4
(	(	kIx(
<g/>
namísto	namísto	k7c2
původně	původně	k6eAd1
určené	určený	k2eAgFnSc2d1
jakosti	jakost	k1gFnSc2
0,960	0,960	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
některé	některý	k3yIgInPc1
jsou	být	k5eAaImIp3nP
měděné	měděný	k2eAgInPc1d1
s	s	k7c7
pouhým	pouhý	k2eAgNnSc7d1
postříbřením	postříbření	k1gNnSc7
</s>
<s>
Bedřich	Bedřich	k1gMnSc1
(	(	kIx(
<g/>
1172	#num#	k4
<g/>
–	–	k?
<g/>
1173	#num#	k4
<g/>
,	,	kIx,
1178	#num#	k4
<g/>
)	)	kIx)
–	–	k?
jakost	jakost	k1gFnSc1
některých	některý	k3yIgFnPc2
mincí	mince	k1gFnPc2
klesla	klesnout	k5eAaPmAgFnS
až	až	k9
na	na	k7c4
úroveň	úroveň	k1gFnSc4
0,010	0,010	k4
</s>
<s>
Soběslav	Soběslav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1173	#num#	k4
<g/>
–	–	k?
<g/>
1178	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Konrád	Konrád	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ota	Ota	k1gMnSc1
(	(	kIx(
<g/>
1189	#num#	k4
<g/>
–	–	k?
<g/>
1191	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Václav	Václav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1191	#num#	k4
<g/>
–	–	k?
<g/>
1192	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Břetislav	Břetislav	k1gMnSc1
(	(	kIx(
<g/>
1193	#num#	k4
<g/>
–	–	k?
<g/>
1197	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
(	(	kIx(
<g/>
1197	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1192	#num#	k4
<g/>
–	–	k?
<g/>
1193	#num#	k4
<g/>
,	,	kIx,
1197	#num#	k4
<g/>
–	–	k?
<g/>
1210	#num#	k4
<g/>
)	)	kIx)
–	–	k?
české	český	k2eAgInPc4d1
denáry	denár	k1gInPc4
razil	razit	k5eAaImAgMnS
asi	asi	k9
do	do	k7c2
roku	rok	k1gInSc2
1210	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
byly	být	k5eAaImAgInP
postupně	postupně	k6eAd1
nahrazeny	nahradit	k5eAaPmNgInP
novým	nový	k2eAgInSc7d1
typem	typ	k1gInSc7
mincí	mince	k1gFnPc2
brakteáty	brakteát	k1gInPc7
</s>
<s>
Moravské	moravský	k2eAgInPc1d1
denáry	denár	k1gInPc1
</s>
<s>
Moravské	moravský	k2eAgFnSc2d1
denárové	denárový	k2eAgFnSc2d1
mince	mince	k1gFnSc2
razili	razit	k5eAaImAgMnP
následující	následující	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
<g/>
,	,	kIx,
v	v	k7c6
závorce	závorka	k1gFnSc6
je	být	k5eAaImIp3nS
uvedeno	uvést	k5eAaPmNgNnS
datum	datum	k1gNnSc1
ražby	ražba	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
Břetislav	Břetislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1028	#num#	k4
<g/>
–	–	k?
<g/>
1034	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Spytihněv	Spytihnět	k5eAaPmDgInS
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1048	#num#	k4
<g/>
–	–	k?
<g/>
1050	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Olomoucké	olomoucký	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Spytihněv	Spytihnět	k5eAaPmDgInS
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1050	#num#	k4
<g/>
–	–	k?
<g/>
1054	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1054	#num#	k4
<g/>
–	–	k?
<g/>
1061	#num#	k4
<g/>
–	–	k?
<g/>
1092	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ota	Ota	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1061	#num#	k4
<g/>
–	–	k?
<g/>
1087	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
Boleslav	Boleslav	k1gMnSc1
(	(	kIx(
<g/>
asi	asi	k9
1085	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Boleslav	Boleslav	k1gMnSc1
(	(	kIx(
<g/>
asi	asi	k9
1090	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bořivoj	Bořivoj	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1090	#num#	k4
<g/>
–	–	k?
<g/>
???	???	k?
<g/>
)	)	kIx)
</s>
<s>
Eufemie	eufemie	k1gFnSc1
(	(	kIx(
<g/>
1087	#num#	k4
<g/>
–	–	k?
<g/>
1095	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Svatopluk	Svatopluk	k1gMnSc1
(	(	kIx(
<g/>
1095	#num#	k4
<g/>
–	–	k?
<g/>
1107	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ota	Ota	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1107	#num#	k4
<g/>
–	–	k?
<g/>
1110	#num#	k4
<g/>
,	,	kIx,
1113	#num#	k4
<g/>
–	–	k?
<g/>
1125	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1110	#num#	k4
<g/>
–	–	k?
<g/>
1113	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
a	a	k8xC
Václav	Václav	k1gMnSc1
(	(	kIx(
<g/>
???	???	k?
<g/>
–	–	k?
<g/>
1125	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
(	(	kIx(
<g/>
???	???	k?
<g/>
–	–	k?
<g/>
1125	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Soběslav	Soběslav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
asi	asi	k9
1125	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Soběslav	Soběslav	k1gFnSc1
I.	I.	kA
a	a	k8xC
Václav	Václav	k1gMnSc1
(	(	kIx(
<g/>
asi	asi	k9
1125	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Václav	Václav	k1gMnSc1
(	(	kIx(
<g/>
1126	#num#	k4
<g/>
–	–	k?
<g/>
1130	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Znojemsko	Znojemsko	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Litold	Litold	k6eAd1
Znojemský	znojemský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1092	#num#	k4
<g/>
–	–	k?
<g/>
1097	#num#	k4
<g/>
,	,	kIx,
1100	#num#	k4
<g/>
–	–	k?
<g/>
1112	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Brněnské	brněnský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Konrád	Konrád	k1gMnSc1
I.	I.	kA
a	a	k8xC
Ota	Ota	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1055	#num#	k4
<g/>
–	–	k?
<g/>
1061	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Konrád	Konrád	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1061	#num#	k4
<g/>
–	–	k?
<g/>
1091	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Břetislav	Břetislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1092	#num#	k4
<g/>
–	–	k?
<g/>
1100	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Břetislav	Břetislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
asi	asi	k9
1092	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
(	(	kIx(
<g/>
1092	#num#	k4
<g/>
–	–	k?
<g/>
1115	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
???	???	k?
(	(	kIx(
<g/>
1097	#num#	k4
<g/>
–	–	k?
<g/>
1099	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ota	Ota	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
Spytihněv	Spytihněv	k1gMnSc1
(	(	kIx(
<g/>
1125	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Moravské	moravský	k2eAgNnSc1d1
markrabství	markrabství	k1gNnSc1
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
I.	I.	kA
Jindřich	Jindřich	k1gMnSc1
(	(	kIx(
<g/>
1197	#num#	k4
<g/>
–	–	k?
<g/>
1222	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1224	#num#	k4
<g/>
–	–	k?
<g/>
1227	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Přemysl	Přemysl	k1gMnSc1
(	(	kIx(
<g/>
1228	#num#	k4
<g/>
–	–	k?
<g/>
1239	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1246	#num#	k4
<g/>
–	–	k?
<g/>
1247	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1247	#num#	k4
<g/>
–	–	k?
<g/>
1253	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Denár	denár	k1gInSc1
(	(	kIx(
<g/>
minca	minca	k1gFnSc1
<g/>
)	)	kIx)
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Dinár	dinár	k1gInSc1
</s>
<s>
Denier	Denier	k1gMnSc1
</s>
<s>
Fenik	fenik	k1gInSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
FIALA	Fiala	k1gMnSc1
<g/>
,	,	kIx,
Eduard	Eduard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
denáry	denár	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
E.	E.	kA
Fiala	Fiala	k1gMnSc1
<g/>
,	,	kIx,
1895	#num#	k4
<g/>
.	.	kIx.
kramerius	kramerius	k1gInSc1
<g/>
.	.	kIx.
<g/>
nkp	nkp	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
denár	denár	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
denár	denár	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Středověk	středověk	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4257529-1	4257529-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85036691	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85036691	#num#	k4
</s>
