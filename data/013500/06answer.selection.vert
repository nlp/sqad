<s desamb="1">
Jako	jako	k8xC
početní	početní	k2eAgFnSc1d1
peněžní	peněžní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
se	se	k3xPyFc4
ovšem	ovšem	k9
denár	denár	k1gInSc1
v	v	k7c6
Českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
udržel	udržet	k5eAaPmAgInS
až	až	k6eAd1
do	do	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	s	k7c7
1	#num#	k4
groš	groš	k1gInSc1
=	=	kIx~
7	#num#	k4
denárů	denár	k1gInPc2
a	a	k8xC
později	pozdě	k6eAd2
ve	v	k7c6
zlatkové	zlatkový	k2eAgFnSc6d1
měně	měna	k1gFnSc6
se	s	k7c7
1	#num#	k4
krejcar	krejcar	k1gInSc1
=	=	kIx~
6	#num#	k4
denárů	denár	k1gInPc2
<g/>
.	.	kIx.
</s>