<p>
<s>
Šest	šest	k4xCc4	šest
set	sto	k4xCgNnPc2	sto
šedesát	šedesát	k4xCc1	šedesát
šest	šest	k4xCc1	šest
je	být	k5eAaImIp3nS	být
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
po	po	k7c6	po
číslu	číslo	k1gNnSc3	číslo
šest	šest	k4xCc4	šest
set	sto	k4xCgNnPc2	sto
šedesát	šedesát	k4xCc1	šedesát
pět	pět	k4xCc1	pět
a	a	k8xC	a
předchází	předcházet	k5eAaImIp3nS	předcházet
číslu	číslo	k1gNnSc3	číslo
šest	šest	k4xCc4	šest
set	sto	k4xCgNnPc2	sto
šedesát	šedesát	k4xCc1	šedesát
sedm	sedm	k4xCc1	sedm
<g/>
.	.	kIx.	.
</s>
<s>
Řadová	řadový	k2eAgFnSc1d1	řadová
číslovka	číslovka	k1gFnSc1	číslovka
je	být	k5eAaImIp3nS	být
šestistý	šestistý	k2eAgInSc1d1	šestistý
šedesátý	šedesátý	k4xOgInSc1	šedesátý
šestý	šestý	k4xOgMnSc1	šestý
nebo	nebo	k8xC	nebo
šestistýšestašedesátý	šestistýšestašedesátý	k2eAgMnSc1d1	šestistýšestašedesátý
<g/>
.	.	kIx.	.
</s>
<s>
Římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
DCLXVI	DCLXVI	kA	DCLXVI
a	a	k8xC	a
řeckými	řecký	k2eAgFnPc7d1	řecká
číslicemi	číslice	k1gFnPc7	číslice
χ	χ	k?	χ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Matematika	matematika	k1gFnSc1	matematika
==	==	k?	==
</s>
</p>
<p>
<s>
666	[number]	k4	666
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
<s>
abundantní	abundantní	k2eAgNnSc1d1	abundantní
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
<s>
součet	součet	k1gInSc1	součet
prvních	první	k4xOgMnPc2	první
36	[number]	k4	36
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
trojúhelníkové	trojúhelníkový	k2eAgNnSc4d1	trojúhelníkové
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
nešťastné	šťastný	k2eNgNnSc4d1	nešťastné
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
nepříznivé	příznivý	k2eNgNnSc1d1	nepříznivé
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
sinus	sinus	k1gInSc1	sinus
úhlu	úhel	k1gInSc2	úhel
666	[number]	k4	666
<g/>
°	°	k?	°
násobený	násobený	k2eAgInSc1d1	násobený
číslem	číslo	k1gNnSc7	číslo
-2	-2	k4	-2
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
konstantě	konstanta	k1gFnSc3	konstanta
zlatého	zlatý	k2eAgInSc2d1	zlatý
řezu	řez	k1gInSc2	řez
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
666	[number]	k4	666
</s>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
54	[number]	k4	54
</s>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
36	[number]	k4	36
</s>
</p>
<p>
</p>
<p>
<s>
∘	∘	k?	∘
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
φ	φ	k?	φ
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1.618	[number]	k4	1.618
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
<s>
39887	[number]	k4	39887
</s>
</p>
<p>
<s>
...	...	k?	...
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-2	-2	k4	-2
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
666	[number]	k4	666
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
54	[number]	k4	54
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
36	[number]	k4	36
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
circ	circ	k1gInSc4	circ
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}}}	}}}	k?	}}}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
1.618	[number]	k4	1.618
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
,39887	,39887	k4	,39887
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k6eAd1	ldots
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
==	==	k?	==
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Bible	bible	k1gFnSc2	bible
===	===	k?	===
</s>
</p>
<p>
<s>
Číslo	číslo	k1gNnSc1	číslo
666	[number]	k4	666
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
616	[number]	k4	616
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k9	jako
číslo	číslo	k1gNnSc1	číslo
šelmy	šelma	k1gFnSc2	šelma
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
název	název	k1gInSc1	název
konceptu	koncept	k1gInSc2	koncept
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zmiňován	zmiňovat	k5eAaImNgInS	zmiňovat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Zjevení	zjevení	k1gNnSc4	zjevení
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
je	být	k5eAaImIp3nS	být
určité	určitý	k2eAgNnSc4d1	určité
konkrétní	konkrétní	k2eAgNnSc4d1	konkrétní
číslo	číslo	k1gNnSc4	číslo
charakteristickým	charakteristický	k2eAgNnSc7d1	charakteristické
označením	označení	k1gNnSc7	označení
Antikrista	Antikrist	k1gMnSc2	Antikrist
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
v	v	k7c6	v
satanismu	satanismus	k1gInSc6	satanismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Roky	rok	k1gInPc1	rok
===	===	k?	===
</s>
</p>
<p>
<s>
666	[number]	k4	666
</s>
</p>
<p>
<s>
666	[number]	k4	666
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
