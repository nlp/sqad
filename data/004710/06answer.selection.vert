<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Portugalskem	Portugalsko	k1gNnSc7	Portugalsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
s	s	k7c7	s
Andorrou	Andorra	k1gFnSc7	Andorra
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Gibraltarem	Gibraltar	k1gInSc7	Gibraltar
<g/>
;	;	kIx,	;
španělské	španělský	k2eAgFnPc1d1	španělská
severoafrické	severoafrický	k2eAgFnPc1d1	severoafrická
državy	država	k1gFnPc1	država
Ceuta	Ceuto	k1gNnSc2	Ceuto
a	a	k8xC	a
Melilla	Melillo	k1gNnSc2	Melillo
mají	mít	k5eAaImIp3nP	mít
pozemní	pozemní	k2eAgFnSc4d1	pozemní
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Marokem	Maroko	k1gNnSc7	Maroko
<g/>
.	.	kIx.	.
</s>
