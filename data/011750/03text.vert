<p>
<s>
Shawn	Shawn	k1gMnSc1	Shawn
Crawford	Crawford	k1gMnSc1	Crawford
(	(	kIx(	(
<g/>
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1978	[number]	k4	1978
Van	vana	k1gFnPc2	vana
Wyck	Wycka	k1gFnPc2	Wycka
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Karolína	Karolína	k1gFnSc1	Karolína
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
sprinter	sprinter	k1gMnSc1	sprinter
<g/>
,	,	kIx,	,
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
a	a	k8xC	a
halový	halový	k2eAgMnSc1d1	halový
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
halovém	halový	k2eAgNnSc6d1	halové
MS	MS	kA	MS
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
60	[number]	k4	60
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
cíli	cíl	k1gInSc6	cíl
prohrál	prohrát	k5eAaPmAgInS	prohrát
o	o	k7c4	o
tři	tři	k4xCgFnPc4	tři
setiny	setina	k1gFnPc4	setina
s	s	k7c7	s
Britem	Brit	k1gMnSc7	Brit
Jasonem	Jason	k1gMnSc7	Jason
Gardenerem	Gardener	k1gMnSc7	Gardener
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2004	[number]	k4	2004
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
osobním	osobní	k2eAgInSc6d1	osobní
rekordu	rekord	k1gInSc6	rekord
19,79	[number]	k4	19,79
s	s	k7c7	s
zlatou	zlatá	k1gFnSc7	zlatá
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
těsně	těsně	k6eAd1	těsně
mu	on	k3xPp3gMnSc3	on
unikla	uniknout	k5eAaPmAgFnS	uniknout
medaile	medaile	k1gFnSc1	medaile
ze	z	k7c2	z
stometrové	stometrový	k2eAgFnSc2d1	stometrová
trati	trať	k1gFnSc2	trať
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
čase	čas	k1gInSc6	čas
9,89	[number]	k4	9,89
s.	s.	k?	s.
Olympijským	olympijský	k2eAgMnSc7d1	olympijský
vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
pozdější	pozdní	k2eAgMnSc1d2	pozdější
dopingový	dopingový	k2eAgMnSc1d1	dopingový
hříšník	hříšník	k1gMnSc1	hříšník
Justin	Justina	k1gFnPc2	Justina
Gatlin	Gatlin	k1gInSc1	Gatlin
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc1	stříbro
bral	brát	k5eAaImAgInS	brát
Francis	Francis	k1gInSc4	Francis
Obikwelu	Obikwel	k1gInSc2	Obikwel
z	z	k7c2	z
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
a	a	k8xC	a
bronz	bronz	k1gInSc1	bronz
Maurice	Maurika	k1gFnSc3	Maurika
Greene	Green	k1gInSc5	Green
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
setiny	setina	k1gFnPc4	setina
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
Crawford	Crawford	k1gMnSc1	Crawford
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
výrazný	výrazný	k2eAgInSc1d1	výrazný
úspěch	úspěch	k1gInSc1	úspěch
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
2009	[number]	k4	2009
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
dvoustovky	dvoustovka	k1gFnSc2	dvoustovka
v	v	k7c6	v
čase	čas	k1gInSc6	čas
19,89	[number]	k4	19,89
s	s	k7c7	s
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
jeho	jeho	k3xOp3gMnSc1	jeho
krajan	krajan	k1gMnSc1	krajan
Wallace	Wallace	k1gFnSc2	Wallace
Spearmon	Spearmon	k1gMnSc1	Spearmon
ztratil	ztratit	k5eAaPmAgMnS	ztratit
čtyři	čtyři	k4xCgFnPc4	čtyři
setiny	setina	k1gFnPc4	setina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInPc4d1	osobní
rekordy	rekord	k1gInPc4	rekord
==	==	k?	==
</s>
</p>
<p>
<s>
Hala	hala	k1gFnSc1	hala
</s>
</p>
<p>
<s>
60	[number]	k4	60
m	m	kA	m
-	-	kIx~	-
(	(	kIx(	(
<g/>
6,47	[number]	k4	6,47
s	s	k7c7	s
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Boston	Boston	k1gInSc1	Boston
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
200	[number]	k4	200
m	m	kA	m
-	-	kIx~	-
(	(	kIx(	(
<g/>
20,26	[number]	k4	20,26
s	s	k7c7	s
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
Fayetteville	Fayetteville	k1gFnSc1	Fayetteville
<g/>
)	)	kIx)	)
<g/>
Dráha	dráha	k1gFnSc1	dráha
</s>
</p>
<p>
<s>
100	[number]	k4	100
m	m	kA	m
-	-	kIx~	-
(	(	kIx(	(
<g/>
9,88	[number]	k4	9,88
s	s	k7c7	s
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Eugene	Eugen	k1gMnSc5	Eugen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
200	[number]	k4	200
m	m	kA	m
-	-	kIx~	-
(	(	kIx(	(
<g/>
19,79	[number]	k4	19,79
s	s	k7c7	s
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Athény	Athéna	k1gFnPc1	Athéna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Shawn	Shawna	k1gFnPc2	Shawna
Crawford	Crawfordo	k1gNnPc2	Crawfordo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Shawn	Shawn	k1gMnSc1	Shawn
Crawford	Crawford	k1gMnSc1	Crawford
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
IAAF	IAAF	kA	IAAF
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
www.usatf.org	www.usatf.org	k1gInSc4	www.usatf.org
</s>
</p>
