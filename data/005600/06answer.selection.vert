<s>
Kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
<g/>
,	,	kIx,	,
reaguje	reagovat	k5eAaBmIp3nS	reagovat
téměř	téměř	k6eAd1	téměř
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
kovy	kov	k1gInPc7	kov
kromě	kromě	k7c2	kromě
železa	železo	k1gNnSc2	železo
(	(	kIx(	(
<g/>
v	v	k7c6	v
koncentrovaném	koncentrovaný	k2eAgInSc6d1	koncentrovaný
stavu	stav	k1gInSc6	stav
jej	on	k3xPp3gMnSc4	on
pasivuje	pasivovat	k5eAaBmIp3nS	pasivovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
platiny	platina	k1gFnSc2	platina
a	a	k8xC	a
wolframu	wolfram	k1gInSc2	wolfram
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
%	%	kIx~	%
vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
s	s	k7c7	s
mědí	měď	k1gFnSc7	měď
<g/>
.	.	kIx.	.
</s>
