<p>
<s>
Ján	Ján	k1gMnSc1	Ján
Krajč	Krajč	k1gMnSc1	Krajč
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
slovenský	slovenský	k2eAgMnSc1d1	slovenský
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
brankář	brankář	k1gMnSc1	brankář
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
Zvolenu	Zvolen	k1gInSc6	Zvolen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
lize	liga	k1gFnSc6	liga
hrál	hrát	k5eAaImAgMnS	hrát
za	za	k7c2	za
AC	AC	kA	AC
Nitra	Nitra	k1gFnSc1	Nitra
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
ve	v	k7c6	v
12	[number]	k4	12
ligových	ligový	k2eAgNnPc6d1	ligové
utkáních	utkání	k1gNnPc6	utkání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ligová	ligový	k2eAgFnSc1d1	ligová
bilance	bilance	k1gFnSc1	bilance
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Horák	Horák	k1gMnSc1	Horák
<g/>
,	,	kIx,	,
Lubomír	Lubomír	k1gMnSc1	Lubomír
Král	Král	k1gMnSc1	Král
<g/>
:	:	kIx,	:
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
našeho	náš	k3xOp1gInSc2	náš
fotbalu	fotbal	k1gInSc2	fotbal
−	−	k?	−
Libri	Libri	k1gNnSc1	Libri
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
Radovan	Radovan	k1gMnSc1	Radovan
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Jenšík	Jenšík	k1gMnSc1	Jenšík
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
českého	český	k2eAgInSc2d1	český
fotbalu	fotbal	k1gInSc2	fotbal
−	−	k?	−
Radovan	Radovan	k1gMnSc1	Radovan
Jelínek	Jelínek	k1gMnSc1	Jelínek
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
Gól	gól	k1gInSc1	gól
<g/>
:	:	kIx,	:
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
a	a	k8xC	a
hokejový	hokejový	k2eAgInSc1d1	hokejový
týdeník	týdeník	k1gInSc1	týdeník
–	–	k?	–
ročník	ročník	k1gInSc1	ročník
1975	[number]	k4	1975
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
FC	FC	kA	FC
Nitra	Nitra	k1gFnSc1	Nitra
</s>
</p>
