<s>
Lomnický	lomnický	k2eAgInSc1d1	lomnický
štít	štít	k1gInSc1	štít
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
2634	[number]	k4	2634
m	m	kA	m
druhá	druhý	k4xOgFnSc1	druhý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Vysokých	vysoký	k2eAgFnPc2d1	vysoká
Tater	Tatra	k1gFnPc2	Tatra
a	a	k8xC	a
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
po	po	k7c6	po
turistické	turistický	k2eAgFnSc6d1	turistická
stezce	stezka	k1gFnSc6	stezka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgNnSc1d1	možné
využít	využít	k5eAaPmF	využít
lanovku	lanovka	k1gFnSc4	lanovka
ze	z	k7c2	z
Skalnatého	skalnatý	k2eAgNnSc2d1	skalnaté
plesa	pleso	k1gNnSc2	pleso
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
zaplatit	zaplatit	k5eAaPmF	zaplatit
výstup	výstup	k1gInSc4	výstup
se	s	k7c7	s
zkušeným	zkušený	k2eAgMnSc7d1	zkušený
horským	horský	k2eAgMnSc7d1	horský
vůdcem	vůdce	k1gMnSc7	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
proto	proto	k8xC	proto
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejnavštěvovanější	navštěvovaný	k2eAgInPc4d3	nejnavštěvovanější
vrcholy	vrchol	k1gInPc4	vrchol
Vysokých	vysoký	k2eAgFnPc2d1	vysoká
Tater	Tatra	k1gFnPc2	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nejvýše	vysoce	k6eAd3	vysoce
položená	položený	k2eAgFnSc1d1	položená
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
stanice	stanice	k1gFnSc1	stanice
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Lomnický	lomnický	k2eAgInSc1d1	lomnický
štít	štít	k1gInSc1	štít
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejznámějším	známý	k2eAgInPc3d3	nejznámější
štítům	štít	k1gInPc3	štít
Vysokých	vysoký	k2eAgFnPc2d1	vysoká
Tater	Tatra	k1gFnPc2	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
štít	štít	k1gInSc4	štít
Tater	Tatra	k1gFnPc2	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
připomíná	připomínat	k5eAaImIp3nS	připomínat
trojhrannou	trojhranný	k2eAgFnSc4d1	trojhranná
pyramidu	pyramida	k1gFnSc4	pyramida
převyšující	převyšující	k2eAgNnSc1d1	převyšující
okolí	okolí	k1gNnSc1	okolí
<g/>
,	,	kIx,	,
vedou	vést	k5eAaImIp3nP	vést
tři	tři	k4xCgInPc4	tři
hřebeny	hřeben	k1gInPc4	hřeben
<g/>
:	:	kIx,	:
severozápadním	severozápadní	k2eAgInSc7d1	severozápadní
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Pyšnému	pyšný	k2eAgInSc3d1	pyšný
štítu	štít	k1gInSc3	štít
<g/>
,	,	kIx,	,
Malému	malý	k2eAgNnSc3d1	malé
Pyšnému	pyšný	k2eAgNnSc3d1	pyšné
štítu	štít	k1gInSc6	štít
<g/>
,	,	kIx,	,
Spišskému	spišský	k2eAgInSc3d1	spišský
štítu	štít	k1gInSc3	štít
po	po	k7c4	po
Baranie	Baranie	k1gFnPc4	Baranie
sedlo	sednout	k5eAaPmAgNnS	sednout
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
přes	přes	k7c4	přes
Baranie	Baranie	k1gFnPc4	Baranie
rohy	roh	k1gInPc4	roh
k	k	k7c3	k
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Baraní	Baranit	k5eAaPmIp3nS	Baranit
strážnici	strážnice	k1gFnSc4	strážnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
napojuje	napojovat	k5eAaImIp3nS	napojovat
jako	jako	k9	jako
rozsocha	rozsocha	k1gFnSc1	rozsocha
na	na	k7c4	na
hlavní	hlavní	k2eAgInSc4d1	hlavní
hřebenem	hřeben	k1gInSc7	hřeben
Vysokých	vysoký	k2eAgFnPc2d1	vysoká
Tater	Tatra	k1gFnPc2	Tatra
<g/>
;	;	kIx,	;
severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vede	vést	k5eAaImIp3nS	vést
přes	přes	k7c4	přes
Vidlový	vidlový	k2eAgInSc4d1	vidlový
hřeben	hřeben	k1gInSc4	hřeben
a	a	k8xC	a
Kežmarský	kežmarský	k2eAgInSc4d1	kežmarský
štít	štít	k1gInSc4	štít
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
k	k	k7c3	k
Huncovskému	Huncovský	k2eAgInSc3d1	Huncovský
štítu	štít	k1gInSc3	štít
a	a	k8xC	a
přes	přes	k7c4	přes
Malý	malý	k2eAgInSc4d1	malý
Kežmarský	kežmarský	k2eAgInSc4d1	kežmarský
štít	štít	k1gInSc4	štít
k	k	k7c3	k
Veleké	Veleký	k2eAgFnSc3d1	Veleký
Svišťovce	Svišťovka	k1gFnSc3	Svišťovka
<g/>
;	;	kIx,	;
jižním	jižní	k2eAgNnSc6d1	jižní
<g/>
,	,	kIx,	,
Lomnický	lomnický	k2eAgInSc1d1	lomnický
hřeben	hřeben	k1gInSc1	hřeben
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gFnSc1	vedoucí
přes	přes	k7c4	přes
Lomnickou	lomnický	k2eAgFnSc4d1	Lomnická
kopu	kopa	k1gFnSc4	kopa
<g/>
,	,	kIx,	,
Lomnické	lomnický	k2eAgNnSc4d1	Lomnické
sedlo	sedlo	k1gNnSc4	sedlo
<g/>
,	,	kIx,	,
Velký	velký	k2eAgInSc4d1	velký
Lomnický	lomnický	k2eAgInSc4d1	lomnický
hrb	hrb	k1gInSc4	hrb
<g/>
,	,	kIx,	,
nad	nad	k7c4	nad
Zamkovského	Zamkovský	k2eAgMnSc4d1	Zamkovský
chatu	chata	k1gFnSc4	chata
až	až	k9	až
po	po	k7c4	po
Lomnickou	lomnický	k2eAgFnSc4d1	Lomnická
vyhlídku	vyhlídka	k1gFnSc4	vyhlídka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
Lomnického	lomnický	k2eAgInSc2d1	lomnický
štítu	štít	k1gInSc2	štít
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
nejvýše	nejvýše	k6eAd1	nejvýše
položená	položený	k2eAgFnSc1d1	položená
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
stanice	stanice	k1gFnSc1	stanice
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
visutá	visutý	k2eAgFnSc1d1	visutá
lanovka	lanovka	k1gFnSc1	lanovka
ze	z	k7c2	z
Skalnatého	skalnatý	k2eAgNnSc2d1	skalnaté
plesa	pleso	k1gNnSc2	pleso
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholové	vrcholový	k2eAgFnSc6d1	vrcholová
stanici	stanice	k1gFnSc6	stanice
lanové	lanový	k2eAgFnSc2d1	lanová
dráhy	dráha	k1gFnSc2	dráha
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
astronomická	astronomický	k2eAgNnPc1d1	astronomické
a	a	k8xC	a
meteorologická	meteorologický	k2eAgNnPc1d1	meteorologické
pracoviště	pracoviště	k1gNnPc1	pracoviště
Slovenského	slovenský	k2eAgInSc2d1	slovenský
hydrometeorologického	hydrometeorologický	k2eAgInSc2d1	hydrometeorologický
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
také	také	k6eAd1	také
televizní	televizní	k2eAgFnSc2d1	televizní
retranslační	retranslační	k2eAgFnSc2d1	retranslační
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Prvně	Prvně	k?	Prvně
vylezl	vylézt	k5eAaPmAgMnS	vylézt
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Lomnického	lomnický	k2eAgInSc2d1	lomnický
štítu	štít	k1gInSc2	štít
obuvník	obuvník	k1gMnSc1	obuvník
a	a	k8xC	a
amatérský	amatérský	k2eAgMnSc1d1	amatérský
horník	horník	k1gMnSc1	horník
Jakub	Jakub	k1gMnSc1	Jakub
Fábry	Fábra	k1gFnSc2	Fábra
(	(	kIx(	(
<g/>
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1760	[number]	k4	1760
<g/>
-	-	kIx~	-
<g/>
1790	[number]	k4	1790
<g/>
)	)	kIx)	)
cestou	cesta	k1gFnSc7	cesta
přes	přes	k7c4	přes
Měděné	měděný	k2eAgFnPc4d1	měděná
lávky	lávka	k1gFnPc4	lávka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
zaznamenaný	zaznamenaný	k2eAgInSc1d1	zaznamenaný
turistický	turistický	k2eAgInSc1d1	turistický
výstup	výstup	k1gInSc1	výstup
provedl	provést	k5eAaPmAgInS	provést
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1793	[number]	k4	1793
z	z	k7c2	z
Malé	Malé	k2eAgFnSc2d1	Malé
Studené	Studené	k2eAgFnSc2d1	Studené
doliny	dolina	k1gFnSc2	dolina
anglický	anglický	k2eAgMnSc1d1	anglický
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
a	a	k8xC	a
cestovatel	cestovatel	k1gMnSc1	cestovatel
Robert	Robert	k1gMnSc1	Robert
Townson	Townson	k1gMnSc1	Townson
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
cesty	cesta	k1gFnSc2	cesta
po	po	k7c6	po
zemích	zem	k1gFnPc6	zem
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Townson	Townson	k1gInSc1	Townson
barometrickým	barometrický	k2eAgNnSc7d1	barometrické
měřením	měření	k1gNnSc7	měření
určil	určit	k5eAaPmAgInS	určit
jen	jen	k6eAd1	jen
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
chybou	chyba	k1gFnSc7	chyba
výšku	výška	k1gFnSc4	výška
Lomnického	lomnický	k2eAgInSc2d1	lomnický
štítu	štít	k1gInSc2	štít
(	(	kIx(	(
<g/>
2	[number]	k4	2
633	[number]	k4	633
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
zimní	zimní	k2eAgInSc1d1	zimní
výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
štít	štít	k1gInSc4	štít
provedli	provést	k5eAaPmAgMnP	provést
Theodor	Theodor	k1gMnSc1	Theodor
Wundt	Wundt	k1gMnSc1	Wundt
a	a	k8xC	a
Jakob	Jakob	k1gMnSc1	Jakob
Horvay	Horvaa	k1gFnSc2	Horvaa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
</s>
<s>
Nejpopulárnější	populární	k2eAgFnSc7d3	nejpopulárnější
stěnou	stěna	k1gFnSc7	stěna
Lomnického	lomnický	k2eAgInSc2d1	lomnický
štítu	štít	k1gInSc2	štít
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
západní	západní	k2eAgFnSc1d1	západní
stěna	stěna	k1gFnSc1	stěna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
vede	vést	k5eAaImIp3nS	vést
řada	řada	k1gFnSc1	řada
populárních	populární	k2eAgFnPc2d1	populární
horolezeckých	horolezecký	k2eAgFnPc2d1	horolezecká
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
od	od	k7c2	od
výstupů	výstup	k1gInPc2	výstup
polských	polský	k2eAgMnPc2d1	polský
klasiků	klasik	k1gMnPc2	klasik
Stanislawského	Stanislawské	k1gNnSc2	Stanislawské
<g/>
,	,	kIx,	,
Orlowského	Orlowské	k1gNnSc2	Orlowské
<g/>
,	,	kIx,	,
Birkenmajera	Birkenmajero	k1gNnSc2	Birkenmajero
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
pověstnou	pověstný	k2eAgFnSc4d1	pověstná
Hokejku	hokejka	k1gFnSc4	hokejka
<g/>
,	,	kIx,	,
pojmenovanou	pojmenovaný	k2eAgFnSc4d1	pojmenovaná
podle	podle	k7c2	podle
převisu	převis	k1gInSc2	převis
tvaru	tvar	k1gInSc2	tvar
hokejky	hokejka	k1gFnSc2	hokejka
po	po	k7c4	po
extrémní	extrémní	k2eAgInPc4d1	extrémní
výstupy	výstup	k1gInPc4	výstup
(	(	kIx(	(
<g/>
Bonbónová	bonbónový	k2eAgFnSc1d1	bonbónová
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
Arnoldova	Arnoldův	k2eAgFnSc1d1	Arnoldova
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
Šokuj	šokovat	k5eAaBmRp2nS	šokovat
opicu	opicu	k6eAd1	opicu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
stěnou	stěna	k1gFnSc7	stěna
je	být	k5eAaImIp3nS	být
stěna	stěna	k1gFnSc1	stěna
jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
výstupů	výstup	k1gInPc2	výstup
vhodných	vhodný	k2eAgInPc2d1	vhodný
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
<g/>
.	.	kIx.	.
</s>
<s>
Štít	štít	k1gInSc1	štít
není	být	k5eNaImIp3nS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
po	po	k7c6	po
turistickém	turistický	k2eAgInSc6d1	turistický
značeném	značený	k2eAgInSc6d1	značený
chodníku	chodník	k1gInSc6	chodník
<g/>
.	.	kIx.	.
</s>
<s>
Běžní	běžný	k2eAgMnPc1d1	běžný
turisté	turist	k1gMnPc1	turist
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
mohou	moct	k5eAaImIp3nP	moct
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1941	[number]	k4	1941
vyvézt	vyvézt	k5eAaPmF	vyvézt
visutou	visutý	k2eAgFnSc7d1	visutá
lanovkou	lanovka	k1gFnSc7	lanovka
ze	z	k7c2	z
Skalnatého	skalnatý	k2eAgNnSc2d1	skalnaté
plesa	pleso	k1gNnSc2	pleso
(	(	kIx(	(
<g/>
svého	své	k1gNnSc2	své
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nejmodernější	moderní	k2eAgFnSc4d3	nejmodernější
lanovku	lanovka	k1gFnSc4	lanovka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
Lomnickému	lomnický	k2eAgInSc3d1	lomnický
štítu	štít	k1gInSc3	štít
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
prvenství	prvenství	k1gNnSc4	prvenství
v	v	k7c6	v
návštěvnosti	návštěvnost	k1gFnSc6	návštěvnost
mezi	mezi	k7c7	mezi
tatranskými	tatranský	k2eAgInPc7d1	tatranský
štíty	štít	k1gInPc7	štít
<g/>
.	.	kIx.	.
</s>
<s>
Lanovka	lanovka	k1gFnSc1	lanovka
překonává	překonávat	k5eAaImIp3nS	překonávat
na	na	k7c6	na
1867	[number]	k4	1867
metrech	metr	k1gInPc6	metr
převýšení	převýšení	k1gNnSc6	převýšení
861	[number]	k4	861
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
přepravní	přepravní	k2eAgFnSc1d1	přepravní
kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
50	[number]	k4	50
osob	osoba	k1gFnPc2	osoba
<g/>
/	/	kIx~	/
<g/>
hodina	hodina	k1gFnSc1	hodina
a	a	k8xC	a
čas	čas	k1gInSc1	čas
trvání	trvání	k1gNnSc2	trvání
jízdy	jízda	k1gFnSc2	jízda
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
9	[number]	k4	9
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kabinky	kabinka	k1gFnSc2	kabinka
se	se	k3xPyFc4	se
vejde	vejít	k5eAaPmIp3nS	vejít
maximálně	maximálně	k6eAd1	maximálně
15	[number]	k4	15
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Lomnickém	lomnický	k2eAgInSc6d1	lomnický
štítu	štít	k1gInSc6	štít
však	však	k9	však
můžete	moct	k5eAaImIp2nP	moct
strávit	strávit	k5eAaPmF	strávit
maximálně	maximálně	k6eAd1	maximálně
50	[number]	k4	50
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
i	i	k9	i
vyjít	vyjít	k5eAaPmF	vyjít
z	z	k7c2	z
Lomnického	lomnický	k2eAgNnSc2d1	Lomnické
sedla	sedlo	k1gNnSc2	sedlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
horského	horský	k2eAgMnSc2d1	horský
vůdce	vůdce	k1gMnSc2	vůdce
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jste	být	k5eAaImIp2nP	být
<g/>
-li	i	k?	-li
organizovaný	organizovaný	k2eAgMnSc1d1	organizovaný
horolezec	horolezec	k1gMnSc1	horolezec
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
vaším	váš	k3xOp2gInSc7	váš
cílem	cíl	k1gInSc7	cíl
cesta	cesta	k1gFnSc1	cesta
minimálně	minimálně	k6eAd1	minimálně
obtížnosti	obtížnost	k1gFnSc2	obtížnost
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
štítu	štít	k1gInSc2	štít
je	být	k5eAaImIp3nS	být
nádherný	nádherný	k2eAgInSc4d1	nádherný
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
okolní	okolní	k2eAgInPc4d1	okolní
vrcholy	vrchol	k1gInPc4	vrchol
i	i	k8xC	i
podhůří	podhůří	k1gNnPc4	podhůří
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
a	a	k8xC	a
Veľká	Veľký	k2eAgFnSc1d1	Veľká
Studená	studený	k2eAgFnSc1d1	studená
dolina	dolina	k1gFnSc1	dolina
<g/>
,	,	kIx,	,
po	po	k7c6	po
Tatranskej	Tatranskej	k?	Tatranskej
magistrále	magistrála	k1gFnSc6	magistrála
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Doline	Dolin	k1gInSc5	Dolin
Zeleného	zelené	k1gNnSc2	zelené
plesa	pleso	k1gNnSc2	pleso
Veľká	Veľká	k1gFnSc1	Veľká
Svišťovka	Svišťovka	k1gFnSc1	Svišťovka
<g/>
,	,	kIx,	,
Dolina	dolina	k1gFnSc1	dolina
Piatich	Piaticha	k1gFnPc2	Piaticha
Spišských	spišský	k2eAgFnPc2d1	Spišská
plies	pliesa	k1gFnPc2	pliesa
<g/>
,	,	kIx,	,
Veľká	Veľký	k2eAgFnSc1d1	Veľká
Zmrzlá	zmrzlý	k2eAgFnSc1d1	zmrzlá
dolina	dolina	k1gFnSc1	dolina
<g/>
,	,	kIx,	,
Skalnatá	skalnatý	k2eAgFnSc1d1	skalnatá
dolina	dolina	k1gFnSc1	dolina
<g/>
,	,	kIx,	,
Medená	Medený	k2eAgFnSc1d1	Medená
kotlinka	kotlinka	k1gFnSc1	kotlinka
<g/>
,	,	kIx,	,
Skalnaté	skalnatý	k2eAgNnSc1d1	skalnaté
pleso	pleso	k1gNnSc1	pleso
<g/>
,	,	kIx,	,
Jahňací	Jahňací	k2eAgInSc1d1	Jahňací
štít	štít	k1gInSc1	štít
<g/>
,	,	kIx,	,
Kežmarský	kežmarský	k2eAgInSc1d1	kežmarský
štít	štít	k1gInSc1	štít
<g/>
,	,	kIx,	,
Pyšný	pyšný	k2eAgInSc1d1	pyšný
štít	štít	k1gInSc1	štít
<g/>
,	,	kIx,	,
Skalnatá	skalnatý	k2eAgFnSc1d1	skalnatá
chata	chata	k1gFnSc1	chata
<g/>
,	,	kIx,	,
Téryho	Téry	k1gMnSc2	Téry
chata	chata	k1gFnSc1	chata
<g/>
,	,	kIx,	,
Zamkovského	Zamkovský	k2eAgMnSc2d1	Zamkovský
chata	chata	k1gFnSc1	chata
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
Lomnický	lomnický	k2eAgInSc1d1	lomnický
štít	štít	k1gInSc1	štít
nazýván	nazývat	k5eAaImNgInS	nazývat
i	i	k9	i
Dedo	Dedo	k6eAd1	Dedo
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Veľká	Veľká	k1gFnSc1	Veľká
Lomnica	Lomnica	k1gFnSc1	Lomnica
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
katastru	katastr	k1gInSc6	katastr
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
ležel	ležet	k5eAaImAgMnS	ležet
<g/>
.	.	kIx.	.
</s>
