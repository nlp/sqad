<p>
<s>
Zatímco	zatímco	k8xS	zatímco
ryzí	ryzí	k2eAgNnSc1d1	ryzí
zlato	zlato	k1gNnSc1	zlato
je	být	k5eAaImIp3nS	být
žluté	žlutý	k2eAgFnPc4d1	žlutá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
barevné	barevný	k2eAgNnSc4d1	barevné
zlato	zlato	k1gNnSc4	zlato
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
barvách	barva	k1gFnPc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
slitinou	slitina	k1gFnSc7	slitina
zlata	zlato	k1gNnSc2	zlato
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
chemickými	chemický	k2eAgInPc7d1	chemický
prvky	prvek	k1gInPc7	prvek
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
poměrech	poměr	k1gInPc6	poměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
slitina	slitina	k1gFnSc1	slitina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
14	[number]	k4	14
dílů	díl	k1gInPc2	díl
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
10	[number]	k4	10
dílů	díl	k1gInPc2	díl
jiných	jiný	k2eAgInPc2d1	jiný
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
14	[number]	k4	14
<g/>
karátové	karátový	k2eAgNnSc4d1	karátové
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
18	[number]	k4	18
dílů	díl	k1gInPc2	díl
zlata	zlato	k1gNnSc2	zlato
k	k	k7c3	k
6	[number]	k4	6
dílům	dílo	k1gNnPc3	dílo
jiných	jiný	k1gMnPc2	jiný
kovů	kov	k1gInPc2	kov
tvoří	tvořit	k5eAaImIp3nS	tvořit
18	[number]	k4	18
karátů	karát	k1gInPc2	karát
a	a	k8xC	a
tak	tak	k9	tak
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
vyjadřováno	vyjadřovat	k5eAaImNgNnS	vyjadřovat
jako	jako	k8xC	jako
poměr	poměr	k1gInSc1	poměr
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
14	[number]	k4	14
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
je	být	k5eAaImIp3nS	být
rovno	roven	k2eAgNnSc4d1	rovno
0,585	[number]	k4	0,585
a	a	k8xC	a
18	[number]	k4	18
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
je	být	k5eAaImIp3nS	být
0,750	[number]	k4	0,750
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
stovky	stovka	k1gFnPc1	stovka
možných	možný	k2eAgFnPc2d1	možná
slitin	slitina	k1gFnPc2	slitina
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
obecně	obecně	k6eAd1	obecně
vložením	vložení	k1gNnSc7	vložení
stříbra	stříbro	k1gNnSc2	stříbro
bude	být	k5eAaImBp3nS	být
barva	barva	k1gFnSc1	barva
zlata	zlato	k1gNnSc2	zlato
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
vložením	vložení	k1gNnSc7	vložení
mědi	měď	k1gFnSc2	měď
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
barvu	barva	k1gFnSc4	barva
červenou	červený	k2eAgFnSc4d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Směs	směs	k1gFnSc1	směs
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
mědi	měď	k1gFnSc2	měď
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
kolem	kolem	k7c2	kolem
50	[number]	k4	50
<g/>
/	/	kIx~	/
<g/>
50	[number]	k4	50
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
rozsah	rozsah	k1gInSc1	rozsah
žlutého	žlutý	k2eAgNnSc2d1	žluté
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
veřejnost	veřejnost	k1gFnSc1	veřejnost
zvyklá	zvyklý	k2eAgFnSc1d1	zvyklá
z	z	k7c2	z
obchodů	obchod	k1gInPc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
tvrdosti	tvrdost	k1gFnSc2	tvrdost
slitiny	slitina	k1gFnSc2	slitina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
do	do	k7c2	do
směsi	směs	k1gFnSc2	směs
vloženo	vložit	k5eAaPmNgNnS	vložit
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
zinku	zinek	k1gInSc2	zinek
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
0,2	[number]	k4	0,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejběžnější	běžný	k2eAgInPc1d3	nejběžnější
stupně	stupeň	k1gInPc1	stupeň
ryzosti	ryzost	k1gFnSc2	ryzost
zlata	zlato	k1gNnSc2	zlato
jsou	být	k5eAaImIp3nP	být
označeny	označit	k5eAaPmNgInP	označit
<g/>
:	:	kIx,	:
ryzí	ryzí	k2eAgFnSc7d1	ryzí
-	-	kIx~	-
24K	[number]	k4	24K
(	(	kIx(	(
<g/>
99,99	[number]	k4	99,99
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
22K	[number]	k4	22K
(	(	kIx(	(
<g/>
91,6	[number]	k4	91,6
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
18K	[number]	k4	18K
(	(	kIx(	(
<g/>
75	[number]	k4	75
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
14K	[number]	k4	14K
(	(	kIx(	(
<g/>
58,5	[number]	k4	58,5
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
9K	[number]	k4	9K
(	(	kIx(	(
<g/>
37,5	[number]	k4	37,5
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Barevné	barevný	k2eAgNnSc4d1	barevné
zlato	zlato	k1gNnSc4	zlato
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
3	[number]	k4	3
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Au-Ag-Cu	Au-Ag-Cu	k5eAaPmIp1nS	Au-Ag-Cu
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
produkující	produkující	k2eAgFnSc1d1	produkující
bílé	bílý	k2eAgNnSc4d1	bílé
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgNnSc4d1	žluté
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgNnSc4d1	zelené
a	a	k8xC	a
červené	červený	k2eAgNnSc4d1	červené
zlato	zlato	k1gNnSc4	zlato
<g/>
;	;	kIx,	;
obecně	obecně	k6eAd1	obecně
tvárné	tvárný	k2eAgFnSc2d1	tvárná
slitiny	slitina	k1gFnSc2	slitina
</s>
</p>
<p>
<s>
Intermetalické	Intermetalický	k2eAgFnPc1d1	Intermetalický
fáze	fáze	k1gFnPc1	fáze
<g/>
,	,	kIx,	,
vytvářející	vytvářející	k2eAgNnSc1d1	vytvářející
modré	modré	k1gNnSc1	modré
a	a	k8xC	a
purpurové	purpurový	k2eAgNnSc1d1	purpurové
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
jiné	jiný	k2eAgFnPc1d1	jiná
barvy	barva	k1gFnPc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
křehké	křehký	k2eAgNnSc1d1	křehké
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použito	použít	k5eAaPmNgNnS	použít
pro	pro	k7c4	pro
vložkování	vložkování	k1gNnSc4	vložkování
a	a	k8xC	a
jako	jako	k9	jako
drahokam	drahokam	k1gInSc1	drahokam
</s>
</p>
<p>
<s>
povrchově	povrchově	k6eAd1	povrchově
oxidovaná	oxidovaný	k2eAgFnSc1d1	oxidovaná
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
černé	černý	k2eAgNnSc1d1	černé
zlato	zlato	k1gNnSc1	zlato
<g/>
;	;	kIx,	;
mechanické	mechanický	k2eAgNnSc1d1	mechanické
složení	složení	k1gNnSc1	složení
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
objemu	objem	k1gInSc6	objem
a	a	k8xC	a
barevný	barevný	k2eAgInSc1d1	barevný
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c4	na
opotřebení	opotřebení	k1gNnSc4	opotřebení
</s>
</p>
<p>
<s>
==	==	k?	==
Bílé	bílý	k2eAgNnSc4d1	bílé
zlato	zlato	k1gNnSc4	zlato
==	==	k?	==
</s>
</p>
<p>
<s>
Bílé	bílý	k2eAgNnSc1d1	bílé
zlato	zlato	k1gNnSc1	zlato
je	být	k5eAaImIp3nS	být
slitina	slitina	k1gFnSc1	slitina
zlata	zlato	k1gNnSc2	zlato
s	s	k7c7	s
nejméně	málo	k6eAd3	málo
jedním	jeden	k4xCgMnSc7	jeden
bílým	bílý	k1gMnSc7	bílý
kovem	kov	k1gInSc7	kov
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
niklem	nikl	k1gInSc7	nikl
<g/>
,	,	kIx,	,
manganem	mangan	k1gInSc7	mangan
nebo	nebo	k8xC	nebo
palladiem	palladion	k1gNnSc7	palladion
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
žlutého	žlutý	k2eAgNnSc2d1	žluté
zlata	zlato	k1gNnSc2	zlato
je	být	k5eAaImIp3nS	být
čistota	čistota	k1gFnSc1	čistota
bílého	bílý	k2eAgNnSc2d1	bílé
zlata	zlato	k1gNnSc2	zlato
udávána	udáván	k2eAgFnSc1d1	udávána
v	v	k7c6	v
karátech	karát	k1gInPc6	karát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
bílého	bílý	k2eAgNnSc2d1	bílé
zlata	zlato	k1gNnSc2	zlato
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
kovech	kov	k1gInPc6	kov
a	a	k8xC	a
rozměrech	rozměr	k1gInPc6	rozměr
použití	použití	k1gNnSc2	použití
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použití	použití	k1gNnSc4	použití
slitin	slitina	k1gFnPc2	slitina
z	z	k7c2	z
bílého	bílý	k2eAgNnSc2d1	bílé
zlata	zlato	k1gNnSc2	zlato
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
různých	různý	k2eAgInPc2d1	různý
účelů	účel	k1gInPc2	účel
<g/>
;	;	kIx,	;
zatímco	zatímco	k8xS	zatímco
slitina	slitina	k1gFnSc1	slitina
niklu	nikl	k1gInSc2	nikl
je	být	k5eAaImIp3nS	být
tvrdší	tvrdý	k2eAgInSc1d2	tvrdší
a	a	k8xC	a
pevnější	pevný	k2eAgInSc1d2	pevnější
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
hodí	hodit	k5eAaPmIp3nS	hodit
pro	pro	k7c4	pro
prsteny	prsten	k1gInPc4	prsten
a	a	k8xC	a
náušnice	náušnice	k1gFnPc4	náušnice
<g/>
,	,	kIx,	,
slitiny	slitina	k1gFnPc4	slitina
paladia	paladium	k1gNnSc2	paladium
a	a	k8xC	a
zlata	zlato	k1gNnSc2	zlato
jsou	být	k5eAaImIp3nP	být
měkké	měkký	k2eAgInPc1d1	měkký
<g/>
,	,	kIx,	,
ohebné	ohebný	k2eAgInPc1d1	ohebný
a	a	k8xC	a
dobré	dobrý	k2eAgInPc1d1	dobrý
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
s	s	k7c7	s
drahými	drahý	k2eAgInPc7d1	drahý
kameny	kámen	k1gInPc7	kámen
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
druhy	druh	k1gInPc7	druh
kovů	kov	k1gInPc2	kov
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
měď	měď	k1gFnSc4	měď
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc4	stříbro
a	a	k8xC	a
platina	platina	k1gFnSc1	platina
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
váhu	váha	k1gFnSc4	váha
a	a	k8xC	a
trvanlivost	trvanlivost	k1gFnSc4	trvanlivost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
vyhledáváno	vyhledávat	k5eAaImNgNnS	vyhledávat
specializovanými	specializovaný	k2eAgInPc7d1	specializovaný
zlatníky	zlatník	k1gInPc7	zlatník
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc4	termín
bílé	bílý	k2eAgNnSc4d1	bílé
zlato	zlato	k1gNnSc4	zlato
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
velmi	velmi	k6eAd1	velmi
volně	volně	k6eAd1	volně
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
čistoty	čistota	k1gFnSc2	čistota
slitin	slitina	k1gFnPc2	slitina
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
bělavém	bělavý	k2eAgInSc6d1	bělavý
odstínu	odstín	k1gInSc6	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rhodiové	rhodiový	k2eAgNnSc1d1	rhodiový
pokovení	pokovení	k1gNnSc1	pokovení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
obchodních	obchodní	k2eAgNnPc6d1	obchodní
místech	místo	k1gNnPc6	místo
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
barva	barva	k1gFnSc1	barva
bílého	bílý	k2eAgNnSc2d1	bílé
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
"	"	kIx"	"
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
velké	velký	k2eAgNnSc4d1	velké
spektrum	spektrum	k1gNnSc4	spektrum
barev	barva	k1gFnPc2	barva
které	který	k3yRgNnSc1	který
jsou	být	k5eAaImIp3nP	být
ohraničené	ohraničený	k2eAgInPc1d1	ohraničený
nebo	nebo	k8xC	nebo
překrývají	překrývat	k5eAaImIp3nP	překrývat
světle	světle	k6eAd1	světle
žlutou	žlutý	k2eAgFnSc4d1	žlutá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
tónovanou	tónovaný	k2eAgFnSc4d1	tónovaná
hnědou	hnědý	k2eAgFnSc4d1	hnědá
a	a	k8xC	a
bledě	bledě	k6eAd1	bledě
růžovou	růžový	k2eAgFnSc4d1	růžová
<g/>
.	.	kIx.	.
</s>
<s>
Šperkařský	šperkařský	k2eAgInSc1d1	šperkařský
průmysl	průmysl	k1gInSc1	průmysl
často	často	k6eAd1	často
skrývá	skrývat	k5eAaImIp3nS	skrývat
tyto	tento	k3xDgFnPc4	tento
nebílé	bílý	k2eNgFnPc4d1	nebílá
barvy	barva	k1gFnPc4	barva
pokovováním	pokovování	k1gNnSc7	pokovování
pomocí	pomocí	k7c2	pomocí
rhodia	rhodium	k1gNnSc2	rhodium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Běžné	běžný	k2eAgNnSc1d1	běžné
bílé	bílý	k2eAgNnSc1d1	bílé
zlato	zlato	k1gNnSc1	zlato
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
90	[number]	k4	90
%	%	kIx~	%
hm	hm	k?	hm
<g/>
.	.	kIx.	.
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
10	[number]	k4	10
%	%	kIx~	%
hm	hm	k?	hm
<g/>
.	.	kIx.	.
niklu	nikl	k1gInSc2	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Měď	měď	k1gFnSc1	měď
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přidána	přidat	k5eAaPmNgFnS	přidat
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
kujnosti	kujnost	k1gFnSc2	kujnost
<g/>
.	.	kIx.	.
<g/>
Pevnost	pevnost	k1gFnSc1	pevnost
slitiny	slitina	k1gFnSc2	slitina
zlato-nikl-měď	zlatoiklědit	k5eAaPmRp2nS	zlato-nikl-mědit
je	on	k3xPp3gMnPc4	on
způsobena	způsobit	k5eAaPmNgFnS	způsobit
dvěma	dva	k4xCgFnPc7	dva
fázemi	fáze	k1gFnPc7	fáze
<g/>
,	,	kIx,	,
na	na	k7c4	na
zlato	zlato	k1gNnSc4	zlato
bohatá	bohatý	k2eAgFnSc1d1	bohatá
Au-Cu	Au-Ca	k1gFnSc4	Au-Ca
a	a	k8xC	a
na	na	k7c4	na
nikl	nikl	k1gInSc4	nikl
bohatá	bohatý	k2eAgFnSc1d1	bohatá
Ni-Cu	Ni-Ca	k1gFnSc4	Ni-Ca
a	a	k8xC	a
výsledkem	výsledek	k1gInSc7	výsledek
vytvrzování	vytvrzování	k1gNnSc2	vytvrzování
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
<g/>
Slitiny	slitina	k1gFnSc2	slitina
použité	použitý	k2eAgFnSc2d1	použitá
ve	v	k7c6	v
šperkařském	šperkařský	k2eAgInSc6d1	šperkařský
průmyslu	průmysl	k1gInSc6	průmysl
jsou	být	k5eAaImIp3nP	být
zlato-paladium-stříbro	zlatoaladiumtříbro	k6eAd1	zlato-paladium-stříbro
a	a	k8xC	a
zlato-nikl-měď-zinek	zlatoiklěďinek	k1gInSc4	zlato-nikl-měď-zinek
<g/>
.	.	kIx.	.
</s>
<s>
Paladium	paladium	k1gNnSc1	paladium
a	a	k8xC	a
nikl	nikl	k1gInSc1	nikl
jsou	být	k5eAaImIp3nP	být
primárními	primární	k2eAgInPc7d1	primární
bělícími	bělící	k2eAgInPc7d1	bělící
agenty	agens	k1gInPc7	agens
pro	pro	k7c4	pro
zlato	zlato	k1gNnSc4	zlato
<g/>
;	;	kIx,	;
zinek	zinek	k1gInSc1	zinek
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
sekundární	sekundární	k2eAgMnSc1d1	sekundární
bělící	bělící	k2eAgMnSc1d1	bělící
agent	agent	k1gMnSc1	agent
k	k	k7c3	k
zeslabení	zeslabení	k1gNnSc3	zeslabení
barvy	barva	k1gFnSc2	barva
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kontaktní	kontaktní	k2eAgFnSc2d1	kontaktní
alergie	alergie	k1gFnSc2	alergie
===	===	k?	===
</s>
</p>
<p>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
osmi	osm	k4xCc2	osm
osob	osoba	k1gFnPc2	osoba
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
nošení	nošení	k1gNnSc6	nošení
alergickou	alergický	k2eAgFnSc4d1	alergická
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
nikl	nikl	k1gInSc4	nikl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
přítomný	přítomný	k2eAgInSc1d1	přítomný
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
druzích	druh	k1gInPc6	druh
bílého	bílý	k2eAgNnSc2d1	bílé
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Typickou	typický	k2eAgFnSc7d1	typická
reakcí	reakce	k1gFnSc7	reakce
je	být	k5eAaImIp3nS	být
drobná	drobný	k2eAgFnSc1d1	drobná
kožní	kožní	k2eAgFnSc1d1	kožní
vyrážka	vyrážka	k1gFnSc1	vyrážka
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
mnoho	mnoho	k4c4	mnoho
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
nepoužívá	používat	k5eNaImIp3nS	používat
v	v	k7c6	v
bílém	bílý	k2eAgNnSc6d1	bílé
zlatě	zlato	k1gNnSc6	zlato
nikl	nikl	k1gInSc1	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Slitiny	slitina	k1gFnPc1	slitina
bílého	bílý	k2eAgNnSc2d1	bílé
zlata	zlato	k1gNnSc2	zlato
bez	bez	k7c2	bez
příměsi	příměs	k1gFnSc2	příměs
niklu	nikl	k1gInSc2	nikl
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
častými	častý	k2eAgInPc7d1	častý
alergeny	alergen	k1gInPc7	alergen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Colored	Colored	k1gMnSc1	Colored
gold	gold	k1gMnSc1	gold
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
