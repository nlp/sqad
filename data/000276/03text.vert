<s>
Mgr.	Mgr.	kA	Mgr.
Jaromír	Jaromír	k1gMnSc1	Jaromír
Bosák	Bosák	k1gMnSc1	Bosák
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1965	[number]	k4	1965
Cheb	Cheb	k1gInSc1	Cheb
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
sportovní	sportovní	k2eAgMnSc1d1	sportovní
komentátor	komentátor	k1gMnSc1	komentátor
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
Fakultu	fakulta	k1gFnSc4	fakulta
sociálních	sociální	k2eAgFnPc2d1	sociální
věd	věda	k1gFnPc2	věda
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Bydlí	bydlet	k5eAaImIp3nS	bydlet
v	v	k7c6	v
Šestajovicích	Šestajovice	k1gFnPc6	Šestajovice
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
novinářskou	novinářský	k2eAgFnSc7d1	novinářská
kariérou	kariéra	k1gFnSc7	kariéra
začínal	začínat	k5eAaImAgInS	začínat
v	v	k7c6	v
rádiu	rádius	k1gInSc6	rádius
Bonton	bonton	k1gInSc1	bonton
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgInS	psát
také	také	k9	také
články	článek	k1gInPc4	článek
o	o	k7c6	o
fotbalu	fotbal	k1gInSc6	fotbal
do	do	k7c2	do
časopisu	časopis	k1gInSc2	časopis
Gól	gól	k1gInSc4	gól
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
o	o	k7c6	o
německé	německý	k2eAgFnSc6d1	německá
Bundeslize	bundesliga	k1gFnSc6	bundesliga
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sportovně-žurnalistických	sportovně-žurnalistický	k2eAgInPc6d1	sportovně-žurnalistický
kruzích	kruh	k1gInPc6	kruh
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
mluví	mluvit	k5eAaImIp3nS	mluvit
jako	jako	k9	jako
o	o	k7c6	o
velkém	velký	k2eAgInSc6d1	velký
odborníkovi	odborník	k1gMnSc3	odborník
na	na	k7c4	na
německý	německý	k2eAgInSc4d1	německý
fotbal	fotbal	k1gInSc4	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
komentuje	komentovat	k5eAaBmIp3nS	komentovat
fotbalové	fotbalový	k2eAgInPc4d1	fotbalový
přenosy	přenos	k1gInPc4	přenos
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
televizi	televize	k1gFnSc4	televize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
až	až	k9	až
2002	[number]	k4	2002
byl	být	k5eAaImAgMnS	být
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
redakce	redakce	k1gFnSc2	redakce
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Momentálně	momentálně	k6eAd1	momentálně
kromě	kromě	k7c2	kromě
činnosti	činnost	k1gFnSc2	činnost
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
publikuje	publikovat	k5eAaBmIp3nS	publikovat
své	svůj	k3xOyFgInPc4	svůj
články	článek	k1gInPc4	článek
na	na	k7c6	na
serveru	server	k1gInSc6	server
ČTK	ČTK	kA	ČTK
-	-	kIx~	-
sportovní	sportovní	k2eAgFnPc1d1	sportovní
noviny	novina	k1gFnPc1	novina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgMnPc2d3	nejpopulárnější
komentátorů	komentátor	k1gMnPc2	komentátor
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
své	svůj	k3xOyFgInPc4	svůj
komentáře	komentář	k1gInPc4	komentář
prokládá	prokládat	k5eAaImIp3nS	prokládat
vtipem	vtip	k1gInSc7	vtip
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
fotbalu	fotbal	k1gInSc2	fotbal
je	být	k5eAaImIp3nS	být
také	také	k9	také
milovníkem	milovník	k1gMnSc7	milovník
golfu	golf	k1gInSc2	golf
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
také	také	k9	také
komentuje	komentovat	k5eAaBmIp3nS	komentovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
golfových	golfový	k2eAgInPc2d1	golfový
časopisů	časopis	k1gInPc2	časopis
Golf	golf	k1gInSc1	golf
Vacations	Vacationsa	k1gFnPc2	Vacationsa
a	a	k8xC	a
GolfPunk	GolfPunka	k1gFnPc2	GolfPunka
<g/>
.	.	kIx.	.
</s>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Bosák	Bosák	k1gMnSc1	Bosák
také	také	k9	také
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kolegou	kolega	k1gMnSc7	kolega
Svěceným	svěcený	k2eAgMnSc7d1	svěcený
propůjčil	propůjčit	k5eAaPmAgInS	propůjčit
svůj	svůj	k3xOyFgInSc4	svůj
hlas	hlas	k1gInSc4	hlas
namluvení	namluvení	k1gNnSc2	namluvení
PC	PC	kA	PC
hry	hra	k1gFnSc2	hra
FIFA	FIFA	kA	FIFA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
profesionální	profesionální	k2eAgFnSc7d1	profesionální
tanečnicí	tanečnice	k1gFnSc7	tanečnice
Evou	Eva	k1gFnSc7	Eva
Krejčířovou	Krejčířová	k1gFnSc7	Krejčířová
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
třetí	třetí	k4xOgInPc4	třetí
řady	řad	k1gInPc4	řad
taneční	taneční	k2eAgFnSc2d1	taneční
soutěže	soutěž	k1gFnSc2	soutěž
a	a	k8xC	a
mediální	mediální	k2eAgFnSc2d1	mediální
show	show	k1gFnSc2	show
Star	star	k1gFnSc2	star
Dance	Danka	k1gFnSc3	Danka
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
nikdy	nikdy	k6eAd1	nikdy
předtím	předtím	k6eAd1	předtím
netancoval	tancovat	k5eNaImAgInS	tancovat
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
podporu	podpora	k1gFnSc4	podpora
televizního	televizní	k2eAgNnSc2d1	televizní
publika	publikum	k1gNnSc2	publikum
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
sedm	sedm	k4xCc1	sedm
kol	kolo	k1gNnPc2	kolo
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
skončil	skončit	k5eAaPmAgInS	skončit
tedy	tedy	k9	tedy
na	na	k7c6	na
pomyslném	pomyslný	k2eAgNnSc6d1	pomyslné
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
týmu	tým	k1gInSc2	tým
Real	Real	k1gInSc1	Real
TOP	topit	k5eAaImRp2nS	topit
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
dresu	dres	k1gInSc6	dres
se	se	k3xPyFc4	se
zúčastňuje	zúčastňovat	k5eAaImIp3nS	zúčastňovat
charitativních	charitativní	k2eAgInPc2d1	charitativní
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Sny	sen	k1gInPc4	sen
<g/>
,	,	kIx,	,
střepy	střep	k1gInPc4	střep
a	a	k8xC	a
štěstí	štěstí	k1gNnSc4	štěstí
(	(	kIx(	(
<g/>
ZOH	ZOH	kA	ZOH
Turín	Turín	k1gInSc4	Turín
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Daranus	Daranus	k1gMnSc1	Daranus
Odsouzenci	odsouzenec	k1gMnSc3	odsouzenec
pro	pro	k7c4	pro
slávu	sláva	k1gFnSc4	sláva
<g/>
,	,	kIx,	,
Daranus	Daranus	k1gInSc4	Daranus
Fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
deník	deník	k1gInSc4	deník
2008	[number]	k4	2008
Jak	jak	k6eAd1	jak
jsem	být	k5eAaImIp1nS	být
potkal	potkat	k5eAaPmAgMnS	potkat
šampiony	šampion	k1gMnPc4	šampion
Zlaté	zlatá	k1gFnSc2	zlatá
nebe	nebe	k1gNnSc2	nebe
nad	nad	k7c7	nad
Berlínem	Berlín	k1gInSc7	Berlín
Čína	Čína	k1gFnSc1	Čína
-	-	kIx~	-
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
zrodu	zrod	k1gInSc6	zrod
velmoci	velmoc	k1gFnSc2	velmoc
(	(	kIx(	(
<g/>
spoluautor	spoluautor	k1gMnSc1	spoluautor
Dan	Dan	k1gMnSc1	Dan
Hrubý	Hrubý	k1gMnSc1	Hrubý
<g/>
)	)	kIx)	)
Finále	finále	k1gNnSc1	finále
na	na	k7c4	na
dosah	dosah	k1gInSc4	dosah
Africký	africký	k2eAgInSc1d1	africký
deník	deník	k1gInSc1	deník
Fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
deník	deník	k1gInSc4	deník
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
Životní	životní	k2eAgInSc4d1	životní
mač	mač	k1gInSc4	mač
<g/>
.	.	kIx.	.
</s>
<s>
Brazilský	brazilský	k2eAgInSc1d1	brazilský
deník	deník	k1gInSc1	deník
<g/>
.	.	kIx.	.
</s>
