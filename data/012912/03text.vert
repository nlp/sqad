<p>
<s>
Bleší	bleší	k2eAgInSc1d1	bleší
cirkus	cirkus	k1gInSc1	cirkus
je	být	k5eAaImIp3nS	být
pouťová	pouťový	k2eAgFnSc1d1	pouťová
atrakce	atrakce	k1gFnSc1	atrakce
s	s	k7c7	s
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
historií	historie	k1gFnSc7	historie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
blechy	blecha	k1gFnPc4	blecha
mající	mající	k2eAgInSc4d1	mající
krátký	krátký	k2eAgInSc4d1	krátký
život	život	k1gInSc4	život
necvičí	cvičit	k5eNaImIp3nS	cvičit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pečlivě	pečlivě	k6eAd1	pečlivě
se	se	k3xPyFc4	se
oddělí	oddělit	k5eAaPmIp3nP	oddělit
jedinci	jedinec	k1gMnPc1	jedinec
lezoucí	lezoucí	k2eAgMnPc1d1	lezoucí
a	a	k8xC	a
skákající	skákající	k2eAgMnPc1d1	skákající
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
je	být	k5eAaImIp3nS	být
blechám	blecha	k1gFnPc3	blecha
navléknut	navléknout	k5eAaPmNgInS	navléknout
na	na	k7c4	na
krk	krk	k1gInSc4	krk
obojek	obojek	k1gInSc4	obojek
z	z	k7c2	z
jemného	jemný	k2eAgInSc2d1	jemný
zlatého	zlatý	k2eAgInSc2d1	zlatý
drátu	drát	k1gInSc2	drát
<g/>
.	.	kIx.	.
</s>
<s>
Skákající	skákající	k2eAgFnPc1d1	skákající
blechy	blecha	k1gFnPc1	blecha
kopají	kopat	k5eAaImIp3nP	kopat
do	do	k7c2	do
malých	malý	k2eAgInPc2d1	malý
lehkých	lehký	k2eAgInPc2d1	lehký
míčků	míček	k1gInPc2	míček
<g/>
,	,	kIx,	,
lezoucí	lezoucí	k2eAgFnSc2d1	lezoucí
blechy	blecha	k1gFnSc2	blecha
tlačí	tlačit	k5eAaImIp3nP	tlačit
autíčka	autíčko	k1gNnPc1	autíčko
nebo	nebo	k8xC	nebo
roztáčejí	roztáčet	k5eAaImIp3nP	roztáčet
miniaturní	miniaturní	k2eAgNnSc4d1	miniaturní
ruské	ruský	k2eAgNnSc4d1	ruské
kolo	kolo	k1gNnSc4	kolo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
bleší	bleší	k2eAgInPc1d1	bleší
cirkusy	cirkus	k1gInPc1	cirkus
vypadají	vypadat	k5eAaImIp3nP	vypadat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
když	když	k8xS	když
v	v	k7c6	v
nich	on	k3xPp3gNnPc6	on
účinkují	účinkovat	k5eAaImIp3nP	účinkovat
živé	živý	k2eAgFnPc4d1	živá
blechy	blecha	k1gFnPc4	blecha
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
plné	plný	k2eAgFnPc1d1	plná
mechanických	mechanický	k2eAgNnPc2d1	mechanické
<g/>
,	,	kIx,	,
elektrických	elektrický	k2eAgNnPc2d1	elektrické
a	a	k8xC	a
magnetických	magnetický	k2eAgNnPc2d1	magnetické
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Egon	Egon	k1gMnSc1	Egon
Erwin	Erwin	k1gMnSc1	Erwin
Kisch	Kisch	k1gMnSc1	Kisch
<g/>
:	:	kIx,	:
Die	Die	k1gFnSc1	Die
Dramaturgie	dramaturgie	k1gFnSc2	dramaturgie
des	des	k1gNnSc2	des
Flohtheaters	Flohtheatersa	k1gFnPc2	Flohtheatersa
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
ders	ders	k1gInSc1	ders
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Abenteuer	Abenteuer	k1gInSc1	Abenteuer
in	in	k?	in
Prag	Prag	k1gInSc1	Prag
<g/>
.	.	kIx.	.
</s>
<s>
Prag	Prag	k1gMnSc1	Prag
<g/>
,	,	kIx,	,
Leipzig	Leipzig	k1gMnSc1	Leipzig
<g/>
,	,	kIx,	,
Wien	Wien	k1gMnSc1	Wien
1920	[number]	k4	1920
</s>
</p>
