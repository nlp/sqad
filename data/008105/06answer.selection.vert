<s>
Koloskopie	Koloskopie	k1gFnSc1	Koloskopie
neboli	neboli	k8xC	neboli
kolonoskopie	kolonoskopie	k1gFnSc1	kolonoskopie
je	být	k5eAaImIp3nS	být
metoda	metoda	k1gFnSc1	metoda
vyšetření	vyšetření	k1gNnSc2	vyšetření
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
tenkého	tenký	k2eAgNnSc2d1	tenké
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
endoskopem	endoskop	k1gInSc7	endoskop
(	(	kIx(	(
<g/>
optický	optický	k2eAgInSc4d1	optický
přístroj	přístroj	k1gInSc4	přístroj
k	k	k7c3	k
vyšetřování	vyšetřování	k1gNnSc3	vyšetřování
tělních	tělní	k2eAgFnPc2d1	tělní
dutin	dutina	k1gFnPc2	dutina
a	a	k8xC	a
dutých	dutý	k2eAgInPc2d1	dutý
orgánů	orgán	k1gInPc2	orgán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
