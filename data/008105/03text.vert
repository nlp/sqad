<s>
Koloskopie	Koloskopie	k1gFnSc1	Koloskopie
neboli	neboli	k8xC	neboli
kolonoskopie	kolonoskopie	k1gFnSc1	kolonoskopie
je	být	k5eAaImIp3nS	být
metoda	metoda	k1gFnSc1	metoda
vyšetření	vyšetření	k1gNnSc2	vyšetření
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
tenkého	tenký	k2eAgNnSc2d1	tenké
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
endoskopem	endoskop	k1gInSc7	endoskop
(	(	kIx(	(
<g/>
optický	optický	k2eAgInSc4d1	optický
přístroj	přístroj	k1gInSc4	přístroj
k	k	k7c3	k
vyšetřování	vyšetřování	k1gNnSc3	vyšetřování
tělních	tělní	k2eAgFnPc2d1	tělní
dutin	dutina	k1gFnPc2	dutina
a	a	k8xC	a
dutých	dutý	k2eAgInPc2d1	dutý
orgánů	orgán	k1gInPc2	orgán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Provádí	provádět	k5eAaImIp3nS	provádět
se	se	k3xPyFc4	se
při	při	k7c6	při
podezření	podezření	k1gNnSc6	podezření
na	na	k7c4	na
hemoroidy	hemoroidy	k1gFnPc4	hemoroidy
<g/>
,	,	kIx,	,
zánětlivé	zánětlivý	k2eAgNnSc4d1	zánětlivé
a	a	k8xC	a
nádorové	nádorový	k2eAgNnSc4d1	nádorové
onemocnění	onemocnění	k1gNnSc4	onemocnění
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Lékař	lékař	k1gMnSc1	lékař
může	moct	k5eAaImIp3nS	moct
takto	takto	k6eAd1	takto
nejen	nejen	k6eAd1	nejen
diagnostikovat	diagnostikovat	k5eAaBmF	diagnostikovat
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
známky	známka	k1gFnPc4	známka
zánětu	zánět	k1gInSc2	zánět
<g/>
,	,	kIx,	,
polypy	polyp	k1gInPc1	polyp
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nádory	nádor	k1gInPc4	nádor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
odebírat	odebírat	k5eAaImF	odebírat
vzorky	vzorek	k1gInPc4	vzorek
tkání	tkáň	k1gFnPc2	tkáň
pro	pro	k7c4	pro
histologické	histologický	k2eAgNnSc4d1	histologické
vyšetření	vyšetření	k1gNnSc4	vyšetření
nebo	nebo	k8xC	nebo
provádět	provádět	k5eAaImF	provádět
menší	malý	k2eAgInPc4d2	menší
léčebné	léčebný	k2eAgInPc4d1	léčebný
zákroky	zákrok	k1gInPc4	zákrok
<g/>
.	.	kIx.	.
</s>
<s>
Kolonoskopie	Kolonoskopie	k1gFnSc1	Kolonoskopie
jako	jako	k8xS	jako
vyšetřovací	vyšetřovací	k2eAgFnSc1d1	vyšetřovací
metoda	metoda	k1gFnSc1	metoda
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Přístroje	přístroj	k1gInPc1	přístroj
byly	být	k5eAaImAgFnP	být
zprvu	zprvu	k6eAd1	zprvu
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
vláknovou	vláknový	k2eAgFnSc7d1	vláknová
optikou	optika	k1gFnSc7	optika
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
přístroj	přístroj	k1gInSc1	přístroj
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
ohebnou	ohebný	k2eAgFnSc7d1	ohebná
"	"	kIx"	"
<g/>
hadicí	hadice	k1gFnSc7	hadice
<g/>
"	"	kIx"	"
se	s	k7c7	s
svazkem	svazek	k1gInSc7	svazek
světlovodných	světlovodný	k2eAgNnPc2d1	světlovodné
vláken	vlákno	k1gNnPc2	vlákno
využívajících	využívající	k2eAgInPc2d1	využívající
fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
principu	princip	k1gInSc2	princip
totální	totální	k2eAgFnSc2d1	totální
reflexe	reflexe	k1gFnSc2	reflexe
<g/>
,	,	kIx,	,
dalším	další	k2eAgInSc7d1	další
svazkem	svazek	k1gInSc7	svazek
vláken	vlákno	k1gNnPc2	vlákno
k	k	k7c3	k
osvětlení	osvětlení	k1gNnSc3	osvětlení
jinak	jinak	k6eAd1	jinak
tmavého	tmavý	k2eAgNnSc2d1	tmavé
střeva	střevo	k1gNnSc2	střevo
a	a	k8xC	a
pracovním	pracovní	k2eAgInSc7d1	pracovní
kanálem	kanál	k1gInSc7	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zevním	zevní	k2eAgInSc6d1	zevní
konci	konec	k1gInSc6	konec
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
okulárem	okulár	k1gInSc7	okulár
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
obraz	obraz	k1gInSc1	obraz
opět	opět	k6eAd1	opět
skládá	skládat	k5eAaImIp3nS	skládat
<g/>
,	,	kIx,	,
a	a	k8xC	a
mechanizmem	mechanizmus	k1gInSc7	mechanizmus
k	k	k7c3	k
řízení	řízení	k1gNnSc3	řízení
ohybného	ohybný	k2eAgInSc2d1	ohybný
konce	konec	k1gInSc2	konec
přístroje	přístroj	k1gInSc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgInPc1	tento
přístroje	přístroj	k1gInPc1	přístroj
nahrazeny	nahrazen	k2eAgInPc1d1	nahrazen
videokolonoskopy	videokolonoskop	k1gInPc1	videokolonoskop
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
konci	konec	k1gInSc6	konec
vybaveny	vybavit	k5eAaPmNgInP	vybavit
CCD	CCD	kA	CCD
senzorem	senzor	k1gInSc7	senzor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
převádí	převádět	k5eAaImIp3nS	převádět
optický	optický	k2eAgInSc4d1	optický
signál	signál	k1gInSc4	signál
na	na	k7c4	na
elektrický	elektrický	k2eAgInSc4d1	elektrický
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
digitálně	digitálně	k6eAd1	digitálně
zpracováván	zpracováván	k2eAgInSc1d1	zpracováván
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
samotného	samotný	k2eAgInSc2d1	samotný
kolonoskopu	kolonoskop	k1gInSc2	kolonoskop
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
přístroje	přístroj	k1gInSc2	přístroj
tzv.	tzv.	kA	tzv.
endoskopická	endoskopický	k2eAgFnSc1d1	endoskopická
věž	věž	k1gFnSc1	věž
s	s	k7c7	s
jednotkou	jednotka	k1gFnSc7	jednotka
ke	k	k7c3	k
zpracování	zpracování	k1gNnSc3	zpracování
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
zdrojem	zdroj	k1gInSc7	zdroj
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
monitorem	monitor	k1gInSc7	monitor
<g/>
,	,	kIx,	,
záznamovým	záznamový	k2eAgNnSc7d1	záznamové
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
,	,	kIx,	,
odsávačkou	odsávačka	k1gFnSc7	odsávačka
<g/>
,	,	kIx,	,
jednotkou	jednotka	k1gFnSc7	jednotka
k	k	k7c3	k
oplachu	oplach	k1gInSc3	oplach
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dalším	další	k2eAgNnSc7d1	další
příslušenstvím	příslušenství	k1gNnSc7	příslušenství
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
tlusté	tlustý	k2eAgNnSc1d1	tlusté
střevo	střevo	k1gNnSc1	střevo
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tuhou	tuhý	k2eAgFnSc4d1	tuhá
stolici	stolice	k1gFnSc4	stolice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
předpokladem	předpoklad	k1gInSc7	předpoklad
k	k	k7c3	k
úspěšnému	úspěšný	k2eAgNnSc3d1	úspěšné
vyšetření	vyšetření	k1gNnSc3	vyšetření
jeho	jeho	k3xOp3gNnSc2	jeho
vyprázdnění	vyprázdnění	k1gNnSc2	vyprázdnění
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
vypít	vypít	k5eAaPmF	vypít
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
množství	množství	k1gNnSc4	množství
prázdnícího	prázdnící	k2eAgInSc2d1	prázdnící
roztoku	roztok	k1gInSc2	roztok
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
den	den	k1gInSc4	den
předem	předem	k6eAd1	předem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
dělená	dělený	k2eAgFnSc1d1	dělená
příprava	příprava	k1gFnSc1	příprava
s	s	k7c7	s
vypitím	vypití	k1gNnSc7	vypití
části	část	k1gFnSc2	část
roztoku	roztok	k1gInSc2	roztok
ještě	ještě	k9	ještě
v	v	k7c4	v
den	den	k1gInSc4	den
vyšetření	vyšetření	k1gNnPc2	vyšetření
brzy	brzy	k6eAd1	brzy
ráno	ráno	k6eAd1	ráno
(	(	kIx(	(
<g/>
minimálně	minimálně	k6eAd1	minimálně
čtyři	čtyři	k4xCgFnPc4	čtyři
hodiny	hodina	k1gFnPc4	hodina
před	před	k7c7	před
vyšetřením	vyšetření	k1gNnSc7	vyšetření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
režim	režim	k1gInSc1	režim
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
lepšímu	dobrý	k2eAgNnSc3d2	lepší
vyčištění	vyčištění	k1gNnSc3	vyčištění
střeva	střevo	k1gNnSc2	střevo
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
používaných	používaný	k2eAgFnPc2d1	používaná
látek	látka	k1gFnPc2	látka
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
makrogolové	makrogolový	k2eAgFnPc4d1	makrogolový
roztoky	roztoka	k1gFnPc4	roztoka
<g/>
,	,	kIx,	,
fosfátové	fosfátový	k2eAgFnPc4d1	fosfátová
soli	sůl	k1gFnPc4	sůl
nebo	nebo	k8xC	nebo
kombinované	kombinovaný	k2eAgInPc4d1	kombinovaný
přípravky	přípravek	k1gInPc4	přípravek
s	s	k7c7	s
projímadly	projímadlo	k1gNnPc7	projímadlo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
před	před	k7c7	před
vyšetřením	vyšetření	k1gNnSc7	vyšetření
vynechat	vynechat	k5eAaPmF	vynechat
potravu	potrava	k1gFnSc4	potrava
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
nestravitelných	stravitelný	k2eNgInPc2d1	nestravitelný
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
zbytků	zbytek	k1gInPc2	zbytek
(	(	kIx(	(
<g/>
slupky	slupka	k1gFnSc2	slupka
<g/>
,	,	kIx,	,
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
zrníčka	zrníčko	k1gNnSc2	zrníčko
<g/>
)	)	kIx)	)
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
léky	lék	k1gInPc1	lék
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
preparáty	preparát	k1gInPc4	preparát
železa	železo	k1gNnSc2	železo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zhoršují	zhoršovat	k5eAaImIp3nP	zhoršovat
vyprazdňování	vyprazdňování	k1gNnSc4	vyprazdňování
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
před	před	k7c7	před
vyšetřením	vyšetření	k1gNnSc7	vyšetření
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
jen	jen	k9	jen
pít	pít	k5eAaImF	pít
tekutiny	tekutina	k1gFnPc4	tekutina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
v	v	k7c6	v
dopoledních	dopolední	k2eAgFnPc6d1	dopolední
hodinách	hodina	k1gFnPc6	hodina
konzumovat	konzumovat	k5eAaBmF	konzumovat
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
lehce	lehko	k6eAd1	lehko
stravitelné	stravitelný	k2eAgFnSc2d1	stravitelná
kašovité	kašovitý	k2eAgFnSc2d1	kašovitá
stravy	strava	k1gFnSc2	strava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
výhodou	výhoda	k1gFnSc7	výhoda
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
pacient	pacient	k1gMnSc1	pacient
takovou	takový	k3xDgFnSc4	takový
dietu	dieta	k1gFnSc4	dieta
dodržuje	dodržovat	k5eAaImIp3nS	dodržovat
již	již	k6eAd1	již
dva	dva	k4xCgInPc4	dva
až	až	k6eAd1	až
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
před	před	k7c7	před
vyšetřením	vyšetření	k1gNnSc7	vyšetření
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetření	vyšetření	k1gNnSc1	vyšetření
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
pro	pro	k7c4	pro
pacienta	pacient	k1gMnSc4	pacient
již	již	k6eAd1	již
dobře	dobře	k6eAd1	dobře
snesitelné	snesitelný	k2eAgNnSc1d1	snesitelné
<g/>
.	.	kIx.	.
</s>
<s>
Standardem	standard	k1gInSc7	standard
je	být	k5eAaImIp3nS	být
injekční	injekční	k2eAgNnSc1d1	injekční
podání	podání	k1gNnSc1	podání
léků	lék	k1gInPc2	lék
k	k	k7c3	k
celkovému	celkový	k2eAgNnSc3d1	celkové
zklidnění	zklidnění	k1gNnSc3	zklidnění
a	a	k8xC	a
utlumení	utlumení	k1gNnSc3	utlumení
bolesti	bolest	k1gFnSc2	bolest
před	před	k7c7	před
výkonem	výkon	k1gInSc7	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Případně	případně	k6eAd1	případně
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vyšetření	vyšetření	k1gNnSc4	vyšetření
provést	provést	k5eAaPmF	provést
i	i	k9	i
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
anestezii	anestezie	k1gFnSc6	anestezie
<g/>
.	.	kIx.	.
</s>
<s>
Zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
pohmatovým	pohmatový	k2eAgNnSc7d1	pohmatové
vyšetřením	vyšetření	k1gNnSc7	vyšetření
řitního	řitní	k2eAgInSc2d1	řitní
kanálu	kanál	k1gInSc2	kanál
a	a	k8xC	a
svěrače	svěrač	k1gInSc2	svěrač
<g/>
.	.	kIx.	.
</s>
<s>
Přístroj	přístroj	k1gInSc1	přístroj
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
konečníkem	konečník	k1gInSc7	konečník
zavádí	zavádět	k5eAaImIp3nS	zavádět
postupně	postupně	k6eAd1	postupně
do	do	k7c2	do
celého	celý	k2eAgNnSc2d1	celé
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
možno	možno	k6eAd1	možno
vyšetřit	vyšetřit	k5eAaPmF	vyšetřit
i	i	k9	i
konečnou	konečný	k2eAgFnSc4d1	konečná
část	část	k1gFnSc4	část
tenkého	tenký	k2eAgNnSc2d1	tenké
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
místo	místo	k1gNnSc1	místo
výskytu	výskyt	k1gInSc2	výskyt
střevních	střevní	k2eAgInPc2d1	střevní
zánětů	zánět	k1gInPc2	zánět
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Crohnovy	Crohnův	k2eAgFnPc4d1	Crohnova
choroby	choroba	k1gFnPc4	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vytahování	vytahování	k1gNnSc6	vytahování
se	se	k3xPyFc4	se
střevo	střevo	k1gNnSc1	střevo
podrobněji	podrobně	k6eAd2	podrobně
prohlíží	prohlížet	k5eAaImIp3nS	prohlížet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
odebírat	odebírat	k5eAaImF	odebírat
vzorky	vzorek	k1gInPc4	vzorek
tkáně	tkáň	k1gFnSc2	tkáň
k	k	k7c3	k
mikroskopickému	mikroskopický	k2eAgNnSc3d1	mikroskopické
vyšetření	vyšetření	k1gNnSc3	vyšetření
či	či	k8xC	či
provádět	provádět	k5eAaImF	provádět
některé	některý	k3yIgInPc4	některý
léčebné	léčebný	k2eAgInPc4d1	léčebný
zákroky	zákrok	k1gInPc4	zákrok
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vyšetření	vyšetření	k1gNnSc2	vyšetření
se	se	k3xPyFc4	se
monitorují	monitorovat	k5eAaImIp3nP	monitorovat
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
funkce	funkce	k1gFnSc2	funkce
vyšetřovaného	vyšetřovaný	k1gMnSc2	vyšetřovaný
(	(	kIx(	(
<g/>
srdeční	srdeční	k2eAgFnSc1d1	srdeční
frekvence	frekvence	k1gFnSc1	frekvence
<g/>
,	,	kIx,	,
okysličení	okysličení	k1gNnSc1	okysličení
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
stav	stav	k1gInSc1	stav
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
úroveň	úroveň	k1gFnSc4	úroveň
bolesti	bolest	k1gFnSc2	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
kolonoskopického	kolonoskopický	k2eAgNnSc2d1	kolonoskopické
vyšetření	vyšetření	k1gNnSc2	vyšetření
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
od	od	k7c2	od
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
zákroku	zákrok	k1gInSc6	zákrok
se	se	k3xPyFc4	se
pacient	pacient	k1gMnSc1	pacient
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
výsledek	výsledek	k1gInSc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
histologického	histologický	k2eAgNnSc2d1	histologické
vyšetření	vyšetření	k1gNnSc2	vyšetření
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
zhruba	zhruba	k6eAd1	zhruba
za	za	k7c4	za
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výkonu	výkon	k1gInSc6	výkon
pacient	pacient	k1gMnSc1	pacient
obvykle	obvykle	k6eAd1	obvykle
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
sledován	sledovat	k5eAaImNgInS	sledovat
na	na	k7c6	na
endoskopickém	endoskopický	k2eAgNnSc6d1	endoskopické
pracovišti	pracoviště	k1gNnSc6	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
podané	podaný	k2eAgFnSc3d1	podaná
premedikaci	premedikace	k1gFnSc3	premedikace
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
měl	mít	k5eAaImAgMnS	mít
pacient	pacient	k1gMnSc1	pacient
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
domů	dům	k1gInPc2	dům
doprovod	doprovod	k1gInSc1	doprovod
<g/>
,	,	kIx,	,
ideálně	ideálně	k6eAd1	ideálně
odvoz	odvoz	k1gInSc4	odvoz
osobním	osobní	k2eAgInSc7d1	osobní
automobilem	automobil	k1gInSc7	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
vyšetřovaný	vyšetřovaný	k1gMnSc1	vyšetřovaný
po	po	k7c6	po
podání	podání	k1gNnSc6	podání
premedikace	premedikace	k1gFnSc2	premedikace
nesmí	smět	k5eNaImIp3nS	smět
v	v	k7c4	v
ten	ten	k3xDgInSc4	ten
den	den	k1gInSc4	den
vykonávat	vykonávat	k5eAaImF	vykonávat
činnosti	činnost	k1gFnPc4	činnost
vyžadující	vyžadující	k2eAgFnSc4d1	vyžadující
větší	veliký	k2eAgFnSc4d2	veliký
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
řízení	řízení	k1gNnSc4	řízení
motorového	motorový	k2eAgNnSc2d1	motorové
vozidla	vozidlo	k1gNnSc2	vozidlo
či	či	k8xC	či
obsluhu	obsluha	k1gFnSc4	obsluha
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
vyšetření	vyšetření	k1gNnSc6	vyšetření
odstraňován	odstraňován	k2eAgInSc1d1	odstraňován
polyp	polyp	k1gInSc1	polyp
nebo	nebo	k8xC	nebo
prováděn	provádět	k5eAaImNgInS	provádět
jiný	jiný	k2eAgInSc1d1	jiný
zákrok	zákrok	k1gInSc1	zákrok
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
dietní	dietní	k2eAgNnSc1d1	dietní
omezení	omezení	k1gNnSc1	omezení
i	i	k9	i
po	po	k7c6	po
výkonu	výkon	k1gInSc6	výkon
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
přesném	přesný	k2eAgInSc6d1	přesný
režimu	režim	k1gInSc6	režim
nemocného	nemocný	k1gMnSc2	nemocný
informuje	informovat	k5eAaBmIp3nS	informovat
lékař	lékař	k1gMnSc1	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vyšetření	vyšetření	k1gNnSc3	vyšetření
střeva	střevo	k1gNnSc2	střevo
při	při	k7c6	při
podezření	podezření	k1gNnSc6	podezření
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
nemoci	nemoc	k1gFnSc6	nemoc
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
nádorová	nádorový	k2eAgFnSc1d1	nádorová
či	či	k8xC	či
zánětlivá	zánětlivý	k2eAgFnSc1d1	zánětlivá
<g/>
.	.	kIx.	.
</s>
<s>
Provádí	provádět	k5eAaImIp3nS	provádět
se	se	k3xPyFc4	se
při	při	k7c6	při
nejasných	jasný	k2eNgFnPc6d1	nejasná
bolestech	bolest	k1gFnPc6	bolest
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
déletrvajících	déletrvající	k2eAgFnPc6d1	déletrvající
zažívacích	zažívací	k2eAgFnPc6d1	zažívací
potížích	potíž	k1gFnPc6	potíž
<g/>
,	,	kIx,	,
známkách	známka	k1gFnPc6	známka
krvácení	krvácení	k1gNnPc2	krvácení
do	do	k7c2	do
stolice	stolice	k1gFnSc2	stolice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
vyšetření	vyšetření	k1gNnSc2	vyšetření
u	u	k7c2	u
chudokrevnosti	chudokrevnost	k1gFnSc2	chudokrevnost
z	z	k7c2	z
neznámé	známý	k2eNgFnSc2d1	neznámá
příčiny	příčina	k1gFnSc2	příčina
<g/>
,	,	kIx,	,
váhového	váhový	k2eAgInSc2d1	váhový
úbytku	úbytek	k1gInSc2	úbytek
nebo	nebo	k8xC	nebo
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgInPc2d1	další
příznaků	příznak	k1gInPc2	příznak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
spojeny	spojen	k2eAgInPc1d1	spojen
s	s	k7c7	s
nemocemi	nemoc	k1gFnPc7	nemoc
střeva	střevo	k1gNnSc2	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysokému	vysoký	k2eAgInSc3d1	vysoký
výskytu	výskyt	k1gInSc3	výskyt
rakoviny	rakovina	k1gFnSc2	rakovina
tlustého	tlusté	k1gNnSc2	tlusté
střeva	střevo	k1gNnSc2	střevo
a	a	k8xC	a
konečníku	konečník	k1gInSc2	konečník
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
zaveden	zavést	k5eAaPmNgInS	zavést
program	program	k1gInSc1	program
screeningu	screening	k1gInSc2	screening
kolorektálního	kolorektální	k2eAgInSc2d1	kolorektální
karcinomu	karcinom	k1gInSc2	karcinom
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vyhledávání	vyhledávání	k1gNnSc3	vyhledávání
polypů	polyp	k1gInPc2	polyp
nebo	nebo	k8xC	nebo
časných	časný	k2eAgNnPc2d1	časné
stádií	stádium	k1gNnPc2	stádium
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kolonoskopii	kolonoskopie	k1gFnSc3	kolonoskopie
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostávají	dostávat	k5eAaImIp3nP	dostávat
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
test	test	k1gInSc4	test
na	na	k7c4	na
skryté	skrytý	k2eAgNnSc4d1	skryté
krvácení	krvácení	k1gNnSc4	krvácení
do	do	k7c2	do
stolice	stolice	k1gFnSc2	stolice
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgInSc2	tento
programu	program	k1gInSc2	program
vyšetření	vyšetření	k1gNnSc1	vyšetření
provádí	provádět	k5eAaImIp3nS	provádět
i	i	k9	i
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
nad	nad	k7c4	nad
55	[number]	k4	55
let	léto	k1gNnPc2	léto
zcela	zcela	k6eAd1	zcela
bez	bez	k7c2	bez
potíží	potíž	k1gFnPc2	potíž
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
doporučováno	doporučován	k2eAgNnSc1d1	doporučováno
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
nemocných	nemocný	k1gMnPc2	nemocný
bez	bez	k7c2	bez
příznaků	příznak	k1gInPc2	příznak
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jejich	jejich	k3xOp3gMnSc1	jejich
blízký	blízký	k2eAgMnSc1d1	blízký
pokrevní	pokrevní	k2eAgMnSc1d1	pokrevní
příbuzný	příbuzný	k1gMnSc1	příbuzný
měl	mít	k5eAaImAgMnS	mít
zjištěnou	zjištěný	k2eAgFnSc4d1	zjištěná
rakovinu	rakovina	k1gFnSc4	rakovina
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
u	u	k7c2	u
příslušníků	příslušník	k1gMnPc2	příslušník
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
případy	případ	k1gInPc1	případ
vrozeného	vrozený	k2eAgInSc2d1	vrozený
mnohočetného	mnohočetný	k2eAgInSc2d1	mnohočetný
výskytu	výskyt	k1gInSc2	výskyt
polypů	polyp	k1gMnPc2	polyp
(	(	kIx(	(
<g/>
polypóza	polypóza	k1gFnSc1	polypóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
nemocných	nemocný	k1gMnPc2	nemocný
podstupuje	podstupovat	k5eAaImIp3nS	podstupovat
kolonoskopii	kolonoskopie	k1gFnSc4	kolonoskopie
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
kontrolní	kontrolní	k2eAgNnPc1d1	kontrolní
vyšetření	vyšetření	k1gNnSc1	vyšetření
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
provádět	provádět	k5eAaImF	provádět
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
byl	být	k5eAaImAgInS	být
odstraněn	odstranit	k5eAaPmNgInS	odstranit
(	(	kIx(	(
<g/>
i	i	k9	i
operačně	operačně	k6eAd1	operačně
<g/>
)	)	kIx)	)
zhoubný	zhoubný	k2eAgInSc1d1	zhoubný
nádor	nádor	k1gInSc1	nádor
nebo	nebo	k8xC	nebo
nádorový	nádorový	k2eAgInSc1d1	nádorový
polyp	polyp	k1gInSc1	polyp
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
časné	časný	k2eAgFnSc2d1	časná
detekce	detekce	k1gFnSc2	detekce
opětovného	opětovný	k2eAgInSc2d1	opětovný
výskytu	výskyt	k1gInSc2	výskyt
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
u	u	k7c2	u
nemocných	nemocný	k1gMnPc2	nemocný
s	s	k7c7	s
delším	dlouhý	k2eAgNnSc7d2	delší
trváním	trvání	k1gNnSc7	trvání
střevních	střevní	k2eAgInPc2d1	střevní
zánětů	zánět	k1gInPc2	zánět
(	(	kIx(	(
<g/>
ulcerózní	ulcerózní	k2eAgFnPc4d1	ulcerózní
kolitidy	kolitis	k1gFnPc4	kolitis
nebo	nebo	k8xC	nebo
Crohnovy	Crohnův	k2eAgFnPc4d1	Crohnova
choroby	choroba	k1gFnPc4	choroba
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
doporučováno	doporučován	k2eAgNnSc4d1	doporučováno
periodické	periodický	k2eAgNnSc4d1	periodické
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kolonoskopie	kolonoskopie	k1gFnSc2	kolonoskopie
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
provést	provést	k5eAaPmF	provést
některé	některý	k3yIgInPc4	některý
menší	malý	k2eAgInPc4d2	menší
léčebné	léčebný	k2eAgInPc4d1	léčebný
zákroky	zákrok	k1gInPc4	zákrok
na	na	k7c6	na
střevě	střevo	k1gNnSc6	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
nemocných	nemocný	k1gMnPc2	nemocný
podstupujících	podstupující	k2eAgInPc2d1	podstupující
takový	takový	k3xDgInSc4	takový
zákrok	zákrok	k1gInSc4	zákrok
by	by	kYmCp3nS	by
jinak	jinak	k6eAd1	jinak
byla	být	k5eAaImAgFnS	být
nucena	nucen	k2eAgFnSc1d1	nucena
podstoupit	podstoupit	k5eAaPmF	podstoupit
operační	operační	k2eAgInSc4d1	operační
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
mnohem	mnohem	k6eAd1	mnohem
náročnější	náročný	k2eAgFnSc1d2	náročnější
<g/>
.	.	kIx.	.
endoskopická	endoskopický	k2eAgFnSc1d1	endoskopická
polypektomie	polypektomie	k1gFnSc1	polypektomie
nebo	nebo	k8xC	nebo
mukosektomie	mukosektomie	k1gFnSc1	mukosektomie
–	–	k?	–
odstranění	odstranění	k1gNnSc3	odstranění
slizničních	slizniční	k2eAgInPc2d1	slizniční
výrůstků	výrůstek	k1gInPc2	výrůstek
(	(	kIx(	(
<g/>
polypů	polyp	k1gInPc2	polyp
<g/>
)	)	kIx)	)
zvláště	zvláště	k6eAd1	zvláště
tvořených	tvořený	k2eAgFnPc2d1	tvořená
nezhoubnými	zhoubný	k2eNgInPc7d1	nezhoubný
nádory	nádor	k1gInPc7	nádor
(	(	kIx(	(
<g/>
adenomové	adenomový	k2eAgInPc1d1	adenomový
polypy	polyp	k1gInPc1	polyp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
časem	časem	k6eAd1	časem
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
rakovina	rakovina	k1gFnSc1	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Výkon	výkon	k1gInSc1	výkon
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
pomocí	pomocí	k7c2	pomocí
kličky	klička	k1gFnSc2	klička
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
zaškrtí	zaškrtit	k5eAaPmIp3nS	zaškrtit
krček	krček	k1gInSc1	krček
polypu	polyp	k1gInSc2	polyp
a	a	k8xC	a
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
kličkou	klička	k1gFnSc7	klička
se	se	k3xPyFc4	se
tkáň	tkáň	k1gFnSc1	tkáň
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
a	a	k8xC	a
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
odříznutí	odříznutí	k1gNnSc3	odříznutí
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
plochý	plochý	k2eAgInSc4d1	plochý
adenom	adenom	k1gInSc4	adenom
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
aplikovat	aplikovat	k5eAaBmF	aplikovat
po	po	k7c4	po
podslizniční	podslizniční	k2eAgFnPc4d1	podslizniční
vrstvy	vrstva	k1gFnPc4	vrstva
roztok	roztok	k1gInSc4	roztok
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
puchýř	puchýř	k1gInSc1	puchýř
a	a	k8xC	a
oddělí	oddělit	k5eAaPmIp3nS	oddělit
tak	tak	k9	tak
sliznice	sliznice	k1gFnSc1	sliznice
s	s	k7c7	s
adenomem	adenom	k1gInSc7	adenom
od	od	k7c2	od
hlubších	hluboký	k2eAgFnPc2d2	hlubší
vrstev	vrstva	k1gFnPc2	vrstva
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
puchýř	puchýř	k1gInSc1	puchýř
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
možno	možno	k6eAd1	možno
zachytit	zachytit	k5eAaPmF	zachytit
do	do	k7c2	do
kličky	klička	k1gFnSc2	klička
a	a	k8xC	a
snést	snést	k5eAaPmF	snést
<g/>
.	.	kIx.	.
zástava	zástava	k1gFnSc1	zástava
krvácení	krvácení	k1gNnSc2	krvácení
–	–	k?	–
endoskopicky	endoskopicky	k6eAd1	endoskopicky
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ošetřit	ošetřit	k5eAaPmF	ošetřit
krvácející	krvácející	k2eAgNnSc4d1	krvácející
místo	místo	k1gNnSc4	místo
aplikací	aplikace	k1gFnPc2	aplikace
kovové	kovový	k2eAgFnSc2d1	kovová
svorky	svorka	k1gFnSc2	svorka
(	(	kIx(	(
<g/>
klipu	klip	k1gInSc2	klip
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
opichem	opich	k1gInSc7	opich
místa	místo	k1gNnSc2	místo
s	s	k7c7	s
adrenalinem	adrenalin	k1gInSc7	adrenalin
či	či	k8xC	či
tepelnou	tepelný	k2eAgFnSc7d1	tepelná
koagulací	koagulace	k1gFnSc7	koagulace
ošetření	ošetření	k1gNnSc2	ošetření
hemoroidů	hemoroidy	k1gInPc2	hemoroidy
ligací	ligací	k1gFnSc2	ligací
–	–	k?	–
naložením	naložení	k1gNnSc7	naložení
gumových	gumový	k2eAgInPc2d1	gumový
kroužků	kroužek	k1gInPc2	kroužek
endoskopická	endoskopický	k2eAgFnSc1d1	endoskopická
dilatace	dilatace	k1gFnSc1	dilatace
stenóz	stenóza	k1gFnPc2	stenóza
–	–	k?	–
rozšíření	rozšíření	k1gNnSc1	rozšíření
zúženého	zúžený	k2eAgNnSc2d1	zúžené
<g />
.	.	kIx.	.
</s>
<s>
místa	místo	k1gNnPc1	místo
opakovaným	opakovaný	k2eAgNnSc7d1	opakované
nafouknutím	nafouknutí	k1gNnSc7	nafouknutí
balónku	balónek	k1gInSc2	balónek
Komplikace	komplikace	k1gFnSc2	komplikace
při	při	k7c6	při
koloskopii	koloskopie	k1gFnSc6	koloskopie
jsou	být	k5eAaImIp3nP	být
řídké	řídký	k2eAgFnPc1d1	řídká
<g/>
,	,	kIx,	,
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c4	pod
jedno	jeden	k4xCgNnSc4	jeden
procento	procento	k1gNnSc4	procento
(	(	kIx(	(
<g/>
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
údaje	údaj	k1gInSc2	údaj
0,35	[number]	k4	0,35
až	až	k9	až
0,5	[number]	k4	0,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
Při	při	k7c6	při
léčebných	léčebný	k2eAgInPc6d1	léčebný
výkonech	výkon	k1gInPc6	výkon
jsou	být	k5eAaImIp3nP	být
častější	častý	k2eAgFnPc1d2	častější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
riziko	riziko	k1gNnSc1	riziko
významných	významný	k2eAgFnPc2d1	významná
komplikací	komplikace	k1gFnPc2	komplikace
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
pod	pod	k7c4	pod
jedno	jeden	k4xCgNnSc4	jeden
procento	procento	k1gNnSc4	procento
(	(	kIx(	(
<g/>
cca	cca	kA	cca
0,7	[number]	k4	0,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
polypu	polyp	k1gInSc2	polyp
či	či	k8xC	či
odběru	odběr	k1gInSc2	odběr
vzorků	vzorek	k1gInPc2	vzorek
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
krvácení	krvácení	k1gNnSc3	krvácení
<g/>
,	,	kIx,	,
jen	jen	k9	jen
vzácně	vzácně	k6eAd1	vzácně
intenzivnímu	intenzivní	k2eAgMnSc3d1	intenzivní
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ošetřit	ošetřit	k5eAaPmF	ošetřit
přímo	přímo	k6eAd1	přímo
při	při	k7c6	při
koloskopii	koloskopie	k1gFnSc6	koloskopie
<g/>
.	.	kIx.	.
</s>
<s>
Opožděné	opožděný	k2eAgNnSc1d1	opožděné
krvácení	krvácení	k1gNnSc1	krvácení
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
více	hodně	k6eAd2	hodně
než	než	k8xS	než
týden	týden	k1gInSc4	týden
po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
polypu	polyp	k1gInSc2	polyp
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
vyžádat	vyžádat	k5eAaPmF	vyžádat
opakování	opakování	k1gNnSc4	opakování
koloskopie	koloskopie	k1gFnSc2	koloskopie
s	s	k7c7	s
ošetřením	ošetření	k1gNnSc7	ošetření
krvácejícího	krvácející	k2eAgNnSc2d1	krvácející
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Závažnější	závažný	k2eAgFnSc7d2	závažnější
komplikací	komplikace	k1gFnSc7	komplikace
je	být	k5eAaImIp3nS	být
roztržení	roztržení	k1gNnSc1	roztržení
(	(	kIx(	(
<g/>
perforace	perforace	k1gFnPc1	perforace
<g/>
)	)	kIx)	)
stěny	stěna	k1gFnPc1	stěna
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
si	se	k3xPyFc3	se
většinou	většinou	k6eAd1	většinou
vyžádá	vyžádat	k5eAaPmIp3nS	vyžádat
chirurgický	chirurgický	k2eAgInSc1d1	chirurgický
operační	operační	k2eAgInSc1d1	operační
zákrok	zákrok	k1gInSc1	zákrok
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
riziko	riziko	k1gNnSc1	riziko
je	být	k5eAaImIp3nS	být
udáváno	udávat	k5eAaImNgNnS	udávat
asi	asi	k9	asi
0,09	[number]	k4	0,09
%	%	kIx~	%
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
polypu	polyp	k1gInSc2	polyp
pomocí	pomocí	k7c2	pomocí
elektrické	elektrický	k2eAgFnSc2d1	elektrická
kličky	klička	k1gFnSc2	klička
je	být	k5eAaImIp3nS	být
možnou	možný	k2eAgFnSc4d1	možná
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
vzácnou	vzácný	k2eAgFnSc7d1	vzácná
komplikací	komplikace	k1gFnSc7	komplikace
postpolypektomický	postpolypektomický	k2eAgInSc1d1	postpolypektomický
syndrom	syndrom	k1gInSc1	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
termické	termický	k2eAgNnSc4d1	termické
poškození	poškození	k1gNnSc4	poškození
stěny	stěna	k1gFnSc2	stěna
střeva	střevo	k1gNnSc2	střevo
s	s	k7c7	s
jejím	její	k3xOp3gNnSc7	její
částečným	částečný	k2eAgNnSc7d1	částečné
odumřením	odumření	k1gNnSc7	odumření
<g/>
.	.	kIx.	.
</s>
<s>
Léčí	léčit	k5eAaImIp3nS	léčit
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
vyloučením	vyloučení	k1gNnSc7	vyloučení
příjmu	příjem	k1gInSc2	příjem
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
podáváním	podávání	k1gNnSc7	podávání
výživy	výživa	k1gFnSc2	výživa
nitrožilně	nitrožilně	k6eAd1	nitrožilně
(	(	kIx(	(
<g/>
parenterální	parenterální	k2eAgFnSc1d1	parenterální
výživa	výživa	k1gFnSc1	výživa
<g/>
)	)	kIx)	)
a	a	k8xC	a
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
vzácnými	vzácný	k2eAgFnPc7d1	vzácná
komplikacemi	komplikace	k1gFnPc7	komplikace
jsou	být	k5eAaImIp3nP	být
roztržení	roztržený	k2eAgMnPc1d1	roztržený
sleziny	slezina	k1gFnSc2	slezina
<g/>
,	,	kIx,	,
infarkt	infarkt	k1gInSc4	infarkt
myokardu	myokard	k1gInSc2	myokard
<g/>
,	,	kIx,	,
komplikace	komplikace	k1gFnPc1	komplikace
související	související	k2eAgFnPc1d1	související
s	s	k7c7	s
anestezií	anestezie	k1gFnSc7	anestezie
<g/>
,	,	kIx,	,
komplikace	komplikace	k1gFnPc1	komplikace
související	související	k2eAgFnPc1d1	související
s	s	k7c7	s
přípravou	příprava	k1gFnSc7	příprava
(	(	kIx(	(
<g/>
dehydratace	dehydratace	k1gFnSc1	dehydratace
při	při	k7c6	při
intenzivních	intenzivní	k2eAgFnPc6d1	intenzivní
ztrátách	ztráta	k1gFnPc6	ztráta
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
,	,	kIx,	,
poškození	poškození	k1gNnSc1	poškození
ledvin	ledvina	k1gFnPc2	ledvina
některými	některý	k3yIgInPc7	některý
přípravky	přípravek	k1gInPc7	přípravek
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
fosfátová	fosfátový	k2eAgFnSc1d1	fosfátová
nefropatie	nefropatie	k1gFnSc1	nefropatie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
kolonoskopické	kolonoskopický	k2eAgNnSc1d1	kolonoskopické
vyšetření	vyšetření	k1gNnSc1	vyšetření
provést	provést	k5eAaPmF	provést
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
potřebujeme	potřebovat	k5eAaImIp1nP	potřebovat
vyšetřit	vyšetřit	k5eAaPmF	vyšetřit
pouze	pouze	k6eAd1	pouze
poslední	poslední	k2eAgFnSc4d1	poslední
třetinu	třetina	k1gFnSc4	třetina
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
lékař	lékař	k1gMnSc1	lékař
využít	využít	k5eAaPmF	využít
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
<g/>
,	,	kIx,	,
alternativní	alternativní	k2eAgFnPc4d1	alternativní
metody	metoda	k1gFnPc4	metoda
ke	k	k7c3	k
kolonoskopii	kolonoskopie	k1gFnSc3	kolonoskopie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
irigografie	irigografie	k1gFnSc1	irigografie
<g/>
,	,	kIx,	,
flexibilní	flexibilní	k2eAgFnSc1d1	flexibilní
sigmoideoskopie	sigmoideoskopie	k1gFnSc1	sigmoideoskopie
a	a	k8xC	a
virtuální	virtuální	k2eAgFnSc1d1	virtuální
kolonoskopie	kolonoskopie	k1gFnSc1	kolonoskopie
<g/>
.	.	kIx.	.
</s>
<s>
Irigografie	irigografie	k1gFnSc1	irigografie
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
vyšetření	vyšetření	k1gNnSc4	vyšetření
střeva	střevo	k1gNnSc2	střevo
prováděné	prováděný	k2eAgNnSc4d1	prováděné
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
pacient	pacient	k1gMnSc1	pacient
vypije	vypít	k5eAaPmIp3nS	vypít
kontrastní	kontrastní	k2eAgFnSc4d1	kontrastní
látku	látka	k1gFnSc4	látka
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
síran	síran	k1gInSc1	síran
barnatý	barnatý	k2eAgInSc1d1	barnatý
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nevstřebává	vstřebávat	k5eNaImIp3nS	vstřebávat
ze	z	k7c2	z
střev	střevo	k1gNnPc2	střevo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
volbou	volba	k1gFnSc7	volba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
tohoto	tento	k3xDgNnSc2	tento
vyšetření	vyšetření	k1gNnSc2	vyšetření
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
menší	malý	k2eAgFnPc1d2	menší
abnormality	abnormalita	k1gFnPc1	abnormalita
se	se	k3xPyFc4	se
nemusí	muset	k5eNaImIp3nP	muset
podařit	podařit	k5eAaPmF	podařit
zachytit	zachytit	k5eAaPmF	zachytit
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zjištění	zjištění	k1gNnSc2	zjištění
problému	problém	k1gInSc2	problém
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
provést	provést	k5eAaPmF	provést
vlastní	vlastní	k2eAgInSc4d1	vlastní
zákrok	zákrok	k1gInSc4	zákrok
s	s	k7c7	s
provedením	provedení	k1gNnSc7	provedení
kolonoskopie	kolonoskopie	k1gFnSc2	kolonoskopie
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnPc4d1	jiná
vhodné	vhodný	k2eAgFnPc4d1	vhodná
metody	metoda	k1gFnPc4	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Flexibilní	flexibilní	k2eAgFnSc1d1	flexibilní
sigmoideoskopie	sigmoideoskopie	k1gFnSc1	sigmoideoskopie
-	-	kIx~	-
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
se	se	k3xPyFc4	se
s	s	k7c7	s
výhodou	výhoda	k1gFnSc7	výhoda
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
lékař	lékař	k1gMnSc1	lékař
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
pouze	pouze	k6eAd1	pouze
poslední	poslední	k2eAgFnSc4d1	poslední
třetinu	třetina	k1gFnSc4	třetina
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
(	(	kIx(	(
<g/>
část	část	k1gFnSc4	část
zvanou	zvaný	k2eAgFnSc4d1	zvaná
colon	colon	k1gNnSc4	colon
sigmoideum	sigmoideum	k1gNnSc1	sigmoideum
-	-	kIx~	-
esovitý	esovitý	k2eAgInSc1d1	esovitý
tračník	tračník	k1gInSc1	tračník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
kratší	krátký	k2eAgFnSc1d2	kratší
a	a	k8xC	a
pružnější	pružný	k2eAgInSc1d2	pružnější
kolonoskop	kolonoskop	k1gInSc1	kolonoskop
<g/>
.	.	kIx.	.
</s>
<s>
Virtuální	virtuální	k2eAgFnSc1d1	virtuální
kolonoskopie	kolonoskopie	k1gFnSc1	kolonoskopie
-	-	kIx~	-
Virtuální	virtuální	k2eAgFnSc1d1	virtuální
kolonoskopie	kolonoskopie	k1gFnSc1	kolonoskopie
je	být	k5eAaImIp3nS	být
moderní	moderní	k2eAgFnSc1d1	moderní
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
využívá	využívat	k5eAaPmIp3nS	využívat
sledování	sledování	k1gNnSc4	sledování
sliznice	sliznice	k1gFnSc2	sliznice
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
CT	CT	kA	CT
snímků	snímek	k1gInPc2	snímek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
obraz	obraz	k1gInSc1	obraz
virtuálně	virtuálně	k6eAd1	virtuálně
rekonstruován	rekonstruován	k2eAgInSc1d1	rekonstruován
(	(	kIx(	(
<g/>
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
kolonoskopii	kolonoskopie	k1gFnSc6	kolonoskopie
je	být	k5eAaImIp3nS	být
zobrazován	zobrazován	k2eAgInSc1d1	zobrazován
přímo	přímo	k6eAd1	přímo
kamerou	kamera	k1gFnSc7	kamera
ve	v	k7c6	v
střevě	střevo	k1gNnSc6	střevo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pacientovi	pacient	k1gMnSc3	pacient
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
konečníku	konečník	k1gInSc2	konečník
zavedena	zaveden	k2eAgFnSc1d1	zavedena
hadička	hadička	k1gFnSc1	hadička
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
do	do	k7c2	do
vyprázdněných	vyprázdněný	k2eAgNnPc2d1	vyprázdněné
střev	střevo	k1gNnPc2	střevo
vhání	vhánět	k5eAaImIp3nS	vhánět
vzduch	vzduch	k1gInSc4	vzduch
a	a	k8xC	a
nafukuje	nafukovat	k5eAaImIp3nS	nafukovat
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
dobře	dobře	k6eAd1	dobře
zobrazit	zobrazit	k5eAaPmF	zobrazit
střevní	střevní	k2eAgFnSc4d1	střevní
sliznici	sliznice	k1gFnSc4	sliznice
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
CT	CT	kA	CT
snímků	snímek	k1gInPc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
umí	umět	k5eAaImIp3nS	umět
zachytit	zachytit	k5eAaPmF	zachytit
polypy	polyp	k1gInPc4	polyp
schované	schovaný	k2eAgInPc4d1	schovaný
za	za	k7c4	za
záhyby	záhyb	k1gInPc4	záhyb
a	a	k8xC	a
při	při	k7c6	při
správném	správný	k2eAgNnSc6d1	správné
provedení	provedení	k1gNnSc6	provedení
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
přínosná	přínosný	k2eAgFnSc1d1	přínosná
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
hlavní	hlavní	k2eAgFnSc7d1	hlavní
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
je	být	k5eAaImIp3nS	být
radiační	radiační	k2eAgFnSc1d1	radiační
zátěž	zátěž	k1gFnSc1	zátěž
pro	pro	k7c4	pro
pacienta	pacient	k1gMnSc4	pacient
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
CT	CT	kA	CT
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
virtuálně	virtuálně	k6eAd1	virtuálně
se	se	k3xPyFc4	se
jen	jen	k9	jen
obtížně	obtížně	k6eAd1	obtížně
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
přednádorové	přednádorový	k2eAgFnPc4d1	přednádorový
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
časném	časný	k2eAgNnSc6d1	časné
stádiu	stádium	k1gNnSc6	stádium
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
kolonoskopii	kolonoskopie	k1gFnSc6	kolonoskopie
většinou	většinou	k6eAd1	většinou
dobře	dobře	k6eAd1	dobře
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
koloskopie	koloskopie	k1gFnSc2	koloskopie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kolonoskopie	kolonoskopie	k1gFnSc2	kolonoskopie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Virtuální	virtuální	k2eAgMnSc1d1	virtuální
průvodce	průvodce	k1gMnSc1	průvodce
kolonoskopickým	kolonoskopický	k2eAgNnSc7d1	kolonoskopické
vyšetřením	vyšetření	k1gNnSc7	vyšetření
Co	co	k9	co
je	být	k5eAaImIp3nS	být
kolonoskopie	kolonoskopie	k1gFnSc1	kolonoskopie
Prevence	prevence	k1gFnSc2	prevence
rakoviny	rakovina	k1gFnSc2	rakovina
Kolonoskopie	Kolonoskopie	k1gFnSc2	Kolonoskopie
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
alternativy	alternativa	k1gFnSc2	alternativa
</s>
