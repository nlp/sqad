<p>
<s>
Motorová	motorový	k2eAgFnSc1d1	motorová
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
loď	loď	k1gFnSc1	loď
Bohemia	bohemia	k1gFnSc1	bohemia
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
pro	pro	k7c4	pro
Pražskou	pražský	k2eAgFnSc4d1	Pražská
paroplavební	paroplavební	k2eAgFnSc4d1	paroplavební
společnost	společnost	k1gFnSc4	společnost
a.s.	a.s.	k?	a.s.
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
Prague	Pragu	k1gFnSc2	Pragu
Boats	Boats	k1gInSc1	Boats
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
lodenici	lodenice	k1gFnSc6	lodenice
v	v	k7c4	v
Derbenu	Derben	k2eAgFnSc4d1	Derben
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejmodernějších	moderní	k2eAgFnPc2d3	nejmodernější
lodív	lodíva	k1gFnPc2	lodíva
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Vltavě	Vltava	k1gFnSc6	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Pluje	plout	k5eAaImIp3nS	plout
okružní	okružní	k2eAgFnSc2d1	okružní
vyhlídkové	vyhlídkový	k2eAgFnSc2d1	vyhlídková
plavby	plavba	k1gFnSc2	plavba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaPmNgFnS	využívat
také	také	k9	také
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
společenské	společenský	k2eAgFnPc4d1	společenská
a	a	k8xC	a
firemní	firemní	k2eAgFnPc4d1	firemní
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
je	být	k5eAaImIp3nS	být
pronajímána	pronajímán	k2eAgFnSc1d1	pronajímána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Loď	loď	k1gFnSc1	loď
Bohemia	bohemia	k1gFnSc1	bohemia
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
projekt	projekt	k1gInSc4	projekt
lodi	loď	k1gFnSc2	loď
Grand	grand	k1gMnSc1	grand
Bohemia	bohemia	k1gFnSc1	bohemia
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pluje	plout	k5eAaImIp3nS	plout
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
koncept	koncept	k1gInSc1	koncept
byl	být	k5eAaImAgInS	být
využit	využít	k5eAaPmNgInS	využít
i	i	k9	i
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
lodi	loď	k1gFnSc2	loď
Bohemia	bohemia	k1gFnSc1	bohemia
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
<g/>
.	.	kIx.	.
</s>
<s>
Zakázka	zakázka	k1gFnSc1	zakázka
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
lodi	loď	k1gFnSc2	loď
Bohemia	bohemia	k1gFnSc1	bohemia
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
byla	být	k5eAaImAgFnS	být
zadána	zadat	k5eAaPmNgFnS	zadat
společností	společnost	k1gFnSc7	společnost
Prague	Pragu	k1gInSc2	Pragu
Boats	Boats	k1gInSc1	Boats
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
a	a	k8xC	a
Pražskou	pražský	k2eAgFnSc7d1	Pražská
paroplavební	paroplavební	k2eAgFnSc7d1	paroplavební
společností	společnost	k1gFnSc7	společnost
a.s.	a.s.	k?	a.s.
německé	německý	k2eAgFnSc3d1	německá
loděnici	loděnice	k1gFnSc3	loděnice
Bolle	Bolle	k1gNnSc2	Bolle
v	v	k7c4	v
Derbenu	Derben	k2eAgFnSc4d1	Derben
<g/>
.	.	kIx.	.
</s>
<s>
Přáním	přání	k1gNnSc7	přání
zadavatele	zadavatel	k1gMnSc2	zadavatel
byla	být	k5eAaImAgFnS	být
loď	loď	k1gFnSc1	loď
vyhovující	vyhovující	k2eAgFnSc1d1	vyhovující
provozu	provoz	k1gInSc2	provoz
na	na	k7c6	na
vyhlídkových	vyhlídkový	k2eAgFnPc6d1	vyhlídková
plavbách	plavba	k1gFnPc6	plavba
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bude	být	k5eAaImBp3nS	být
navíc	navíc	k6eAd1	navíc
ekologická	ekologický	k2eAgFnSc1d1	ekologická
a	a	k8xC	a
nehlučná	hlučný	k2eNgFnSc1d1	nehlučná
<g/>
.	.	kIx.	.
</s>
<s>
Design	design	k1gInSc4	design
lodi	loď	k1gFnSc2	loď
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Marcel	Marcel	k1gMnSc1	Marcel
Bolle	Bolle	k1gFnSc2	Bolle
a	a	k8xC	a
úpravy	úprava	k1gFnSc2	úprava
interiéru	interiér	k1gInSc2	interiér
provedla	provést	k5eAaPmAgFnS	provést
architektka	architektka	k1gFnSc1	architektka
Vera	Vera	k1gMnSc1	Vera
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
lodi	loď	k1gFnSc2	loď
trvala	trvat	k5eAaImAgFnS	trvat
od	od	k7c2	od
1.10	[number]	k4	1.10
<g/>
.2014	.2014	k4	.2014
do	do	k7c2	do
dokončení	dokončení	k1gNnSc2	dokončení
30.4	[number]	k4	30.4
<g/>
.2015	.2015	k4	.2015
přesně	přesně	k6eAd1	přesně
212	[number]	k4	212
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2015	[number]	k4	2015
loď	loď	k1gFnSc4	loď
poprvé	poprvé	k6eAd1	poprvé
vyplula	vyplout	k5eAaPmAgFnS	vyplout
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
po	po	k7c6	po
Labi	Labe	k1gNnSc6	Labe
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
538	[number]	k4	538
km	km	kA	km
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
Derbenu	Derben	k2eAgFnSc4d1	Derben
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
trvala	trvat	k5eAaImAgFnS	trvat
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
3	[number]	k4	3
kapitánů	kapitán	k1gMnPc2	kapitán
4	[number]	k4	4
dny	den	k1gInPc7	den
<g/>
.	.	kIx.	.
</s>
<s>
Bohemia	bohemia	k1gFnSc1	bohemia
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
byla	být	k5eAaImAgFnS	být
Praze	Praha	k1gFnSc6	Praha
slavnostně	slavnostně	k6eAd1	slavnostně
představena	představit	k5eAaPmNgFnS	představit
v	v	k7c6	v
ekologickém	ekologický	k2eAgNnSc6d1	ekologické
přístavišti	přístaviště	k1gNnSc6	přístaviště
na	na	k7c6	na
Kampě	Kampa	k1gFnSc6	Kampa
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
a	a	k8xC	a
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
pokřtěna	pokřtěn	k2eAgFnSc1d1	pokřtěna
kardinálem	kardinál	k1gMnSc7	kardinál
Dominikem	Dominik	k1gMnSc7	Dominik
Dukou	Duka	k1gMnSc7	Duka
<g/>
,	,	kIx,	,
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
pražským	pražský	k2eAgMnSc7d1	pražský
a	a	k8xC	a
primasem	primas	k1gMnSc7	primas
českým	český	k2eAgMnSc7d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Kardinál	kardinál	k1gMnSc1	kardinál
Duka	Duka	k1gMnSc1	Duka
požehnal	požehnat	k5eAaPmAgMnS	požehnat
loď	loď	k1gFnSc4	loď
Bohemia	bohemia	k1gFnSc1	bohemia
Rhapsod	Rhapsoda	k1gFnPc2	Rhapsoda
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Prague	Prague	k1gFnSc1	Prague
Boats	Boats	k1gInSc1	Boats
<g/>
,	,	kIx,	,
rev	rev	k?	rev
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
flotilu	flotila	k1gFnSc4	flotila
právě	právě	k6eAd1	právě
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
výročí	výročí	k1gNnSc2	výročí
150	[number]	k4	150
let	léto	k1gNnPc2	léto
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
Pražské	pražský	k2eAgFnSc2d1	Pražská
paroplavební	paroplavební	k2eAgFnSc2d1	paroplavební
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technický	technický	k2eAgInSc1d1	technický
popis	popis	k1gInSc1	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Loď	loď	k1gFnSc1	loď
Bohemia	bohemia	k1gFnSc1	bohemia
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
je	být	k5eAaImIp3nS	být
jednopalubová	jednopalubový	k2eAgFnSc1d1	jednopalubový
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
aquacabrio	aquacabrio	k1gNnSc1	aquacabrio
<g/>
,	,	kIx,	,
s	s	k7c7	s
vyvýšenou	vyvýšený	k2eAgFnSc7d1	vyvýšená
panoramatickou	panoramatický	k2eAgFnSc7d1	panoramatická
kormidelnou	kormidelna	k1gFnSc7	kormidelna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
zastřešena	zastřešit	k5eAaPmNgFnS	zastřešit
posuvnou	posuvný	k2eAgFnSc7d1	posuvná
prosklenou	prosklený	k2eAgFnSc7d1	prosklená
střechou	střecha	k1gFnSc7	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
střechu	střecha	k1gFnSc4	střecha
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
kdykoli	kdykoli	k6eAd1	kdykoli
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
během	během	k7c2	během
chvíle	chvíle	k1gFnSc2	chvíle
odsunout	odsunout	k5eAaPmF	odsunout
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
vyhlídkovou	vyhlídkový	k2eAgFnSc4d1	vyhlídková
slunnou	slunný	k2eAgFnSc4d1	slunná
palubu	paluba	k1gFnSc4	paluba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ji	on	k3xPp3gFnSc4	on
naopak	naopak	k6eAd1	naopak
zatáhnout	zatáhnout	k5eAaPmF	zatáhnout
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
tak	tak	k6eAd1	tak
krytý	krytý	k2eAgInSc1d1	krytý
lodní	lodní	k2eAgInSc1d1	lodní
salon	salon	k1gInSc1	salon
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
výkonná	výkonný	k2eAgFnSc1d1	výkonná
klimatizace	klimatizace	k1gFnSc1	klimatizace
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
perfektní	perfektní	k2eAgFnSc1d1	perfektní
viditelnost	viditelnost	k1gFnSc1	viditelnost
i	i	k9	i
během	během	k7c2	během
deštivého	deštivý	k2eAgNnSc2d1	deštivé
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Celoroční	celoroční	k2eAgInSc1d1	celoroční
provoz	provoz	k1gInSc1	provoz
lodi	loď	k1gFnSc2	loď
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
právě	právě	k9	právě
díky	díky	k7c3	díky
klimatizaci	klimatizace	k1gFnSc3	klimatizace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
solárnímu	solární	k2eAgNnSc3d1	solární
temperování	temperování	k1gNnSc3	temperování
a	a	k8xC	a
větrání	větrání	k1gNnSc3	větrání
<g/>
.	.	kIx.	.
</s>
<s>
Paluba	paluba	k1gFnSc1	paluba
lodi	loď	k1gFnSc2	loď
je	být	k5eAaImIp3nS	být
izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
od	od	k7c2	od
hluku	hluk	k1gInSc2	hluk
a	a	k8xC	a
vibrací	vibrace	k1gFnPc2	vibrace
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Technické	technický	k2eAgInPc1d1	technický
parametry	parametr	k1gInPc1	parametr
motoru	motor	k1gInSc2	motor
splňují	splňovat	k5eAaImIp3nP	splňovat
nejpřísnější	přísný	k2eAgFnPc4d3	nejpřísnější
evropské	evropský	k2eAgFnPc4d1	Evropská
normy	norma	k1gFnPc4	norma
<g/>
.	.	kIx.	.
</s>
<s>
Chod	chod	k1gInSc1	chod
motoru	motor	k1gInSc2	motor
je	být	k5eAaImIp3nS	být
tichý	tichý	k2eAgMnSc1d1	tichý
a	a	k8xC	a
s	s	k7c7	s
mimořádně	mimořádně	k6eAd1	mimořádně
nízkými	nízký	k2eAgFnPc7d1	nízká
emisními	emisní	k2eAgFnPc7d1	emisní
hodnotami	hodnota	k1gFnPc7	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
motoru	motor	k1gInSc2	motor
Sisudiesel	Sisudiesel	k1gInSc1	Sisudiesel
představuje	představovat	k5eAaImIp3nS	představovat
226	[number]	k4	226
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
loď	loď	k1gFnSc1	loď
může	moct	k5eAaImIp3nS	moct
plout	plout	k5eAaImF	plout
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
20	[number]	k4	20
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejrychlejších	rychlý	k2eAgFnPc2d3	nejrychlejší
lodí	loď	k1gFnPc2	loď
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Vltavě	Vltava	k1gFnSc6	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Kapacita	kapacita	k1gFnSc1	kapacita
lodi	loď	k1gFnSc2	loď
je	být	k5eAaImIp3nS	být
220	[number]	k4	220
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
délku	délka	k1gFnSc4	délka
plavidlo	plavidlo	k1gNnSc1	plavidlo
měří	měřit	k5eAaImIp3nS	měřit
44,97	[number]	k4	44,97
m	m	kA	m
<g/>
,	,	kIx,	,
na	na	k7c4	na
šířku	šířka	k1gFnSc4	šířka
7,54	[number]	k4	7,54
m	m	kA	m
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
výtlak	výtlak	k1gInSc4	výtlak
123	[number]	k4	123
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Loď	loď	k1gFnSc1	loď
kotví	kotvit	k5eAaImIp3nS	kotvit
celoročně	celoročně	k6eAd1	celoročně
na	na	k7c6	na
Dvořákově	Dvořákův	k2eAgNnSc6d1	Dvořákovo
nábřeží	nábřeží	k1gNnSc6	nábřeží
na	na	k7c6	na
nástupišti	nástupiště	k1gNnSc6	nástupiště
č.	č.	k?	č.
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
vyplouvá	vyplouvat	k5eAaImIp3nS	vyplouvat
na	na	k7c4	na
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
vyhlídkové	vyhlídkový	k2eAgFnPc4d1	vyhlídková
plavby	plavba	k1gFnPc4	plavba
Prahou	Praha	k1gFnSc7	Praha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c4	na
plavby	plavba	k1gFnPc4	plavba
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
pronájmem	pronájem	k1gInSc7	pronájem
lodi	loď	k1gFnSc2	loď
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
soukromé	soukromý	k2eAgFnPc4d1	soukromá
či	či	k8xC	či
firemní	firemní	k2eAgFnPc4d1	firemní
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,	,
konference	konference	k1gFnPc4	konference
a	a	k8xC	a
setkání	setkání	k1gNnPc4	setkání
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
plaveb	plavba	k1gFnPc2	plavba
je	být	k5eAaImIp3nS	být
kompletní	kompletní	k2eAgInSc1d1	kompletní
gastronomický	gastronomický	k2eAgInSc1d1	gastronomický
servis	servis	k1gInSc1	servis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zajišťován	zajišťovat	k5eAaImNgInS	zajišťovat
formou	forma	k1gFnSc7	forma
cateringu	catering	k1gInSc2	catering
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
je	být	k5eAaImIp3nS	být
během	během	k7c2	během
plavby	plavba	k1gFnSc2	plavba
dostupný	dostupný	k2eAgInSc1d1	dostupný
výklad	výklad	k1gInSc1	výklad
o	o	k7c6	o
pražských	pražský	k2eAgFnPc6d1	Pražská
památkách	památka	k1gFnPc6	památka
v	v	k7c6	v
8	[number]	k4	8
světových	světový	k2eAgInPc6d1	světový
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Vichta	Vichta	k1gMnSc1	Vichta
<g/>
,	,	kIx,	,
Historie	historie	k1gFnSc1	historie
pražské	pražský	k2eAgFnSc2d1	Pražská
paroplavby	paroplavba	k1gFnSc2	paroplavba
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Loď	loď	k1gFnSc1	loď
Grand	grand	k1gMnSc1	grand
Bohemia	bohemia	k1gFnSc1	bohemia
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Video	video	k1gNnSc1	video
plavba	plavba	k1gFnSc1	plavba
lodě	loď	k1gFnSc2	loď
Bohemia	bohemia	k1gFnSc1	bohemia
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
z	z	k7c2	z
Derbenu	Derben	k2eAgFnSc4d1	Derben
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
</s>
</p>
<p>
<s>
Kardinál	kardinál	k1gMnSc1	kardinál
Duka	Duka	k1gMnSc1	Duka
požehnal	požehnat	k5eAaPmAgMnS	požehnat
loď	loď	k1gFnSc4	loď
Bohemia	bohemia	k1gFnSc1	bohemia
Rhapsody	Rhapsoda	k1gFnPc1	Rhapsoda
</s>
</p>
<p>
<s>
Loď	loď	k1gFnSc1	loď
Bohemia	bohemia	k1gFnSc1	bohemia
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
