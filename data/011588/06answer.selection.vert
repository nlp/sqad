<s>
Přáním	přání	k1gNnSc7	přání
zadavatele	zadavatel	k1gMnSc2	zadavatel
byla	být	k5eAaImAgFnS	být
loď	loď	k1gFnSc1	loď
vyhovující	vyhovující	k2eAgFnSc1d1	vyhovující
provozu	provoz	k1gInSc2	provoz
na	na	k7c6	na
vyhlídkových	vyhlídkový	k2eAgFnPc6d1	vyhlídková
plavbách	plavba	k1gFnPc6	plavba
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bude	být	k5eAaImBp3nS	být
navíc	navíc	k6eAd1	navíc
ekologická	ekologický	k2eAgFnSc1d1	ekologická
a	a	k8xC	a
nehlučná	hlučný	k2eNgFnSc1d1	nehlučná
<g/>
.	.	kIx.	.
</s>
