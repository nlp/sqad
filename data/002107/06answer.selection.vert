<s>
Podle	podle	k7c2	podle
statistiky	statistika	k1gFnSc2	statistika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
se	s	k7c7	s
35	[number]	k4	35
%	%	kIx~	%
přepravy	přeprava	k1gFnSc2	přeprava
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
32	[number]	k4	32
%	%	kIx~	%
automobilem	automobil	k1gInSc7	automobil
<g/>
,	,	kIx,	,
28	[number]	k4	28
%	%	kIx~	%
pěšky	pěšky	k6eAd1	pěšky
a	a	k8xC	a
5	[number]	k4	5
%	%	kIx~	%
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
