<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
zastánce	zastánce	k1gMnSc4	zastánce
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
organicistického	organicistický	k2eAgInSc2d1	organicistický
proudu	proud	k1gInSc2	proud
<g/>
"	"	kIx"	"
v	v	k7c6	v
sociologii	sociologie	k1gFnSc6	sociologie
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
podobnost	podobnost	k1gFnSc4	podobnost
rysů	rys	k1gInPc2	rys
lidské	lidský	k2eAgFnSc2d1	lidská
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
biologického	biologický	k2eAgInSc2d1	biologický
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
