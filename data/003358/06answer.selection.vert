<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Kerry	Kerr	k1gMnPc7	Kerr
Kingem	King	k1gMnSc7	King
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
druhého	druhý	k4xOgMnSc2	druhý
kytaristy	kytarista	k1gMnSc2	kytarista
našel	najít	k5eAaPmAgMnS	najít
Jeffa	Jeff	k1gMnSc4	Jeff
Hannemana	Hanneman	k1gMnSc4	Hanneman
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
nalezl	nalézt	k5eAaBmAgInS	nalézt
na	na	k7c4	na
post	post	k1gInSc4	post
baskytaristy	baskytarista	k1gMnSc2	baskytarista
Toma	Tom	k1gMnSc2	Tom
Arayu	Arayus	k1gInSc2	Arayus
<g/>
,	,	kIx,	,
bubeníkem	bubeník	k1gMnSc7	bubeník
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Dave	Dav	k1gInSc5	Dav
Lombardo	Lombardo	k1gMnSc1	Lombardo
<g/>
.	.	kIx.	.
</s>
