<s>
Montpelier	Montpelier	k1gInSc1	Montpelier
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Vermont	Vermonta	k1gFnPc2	Vermonta
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
též	též	k9	též
sídelní	sídelní	k2eAgNnSc1d1	sídelní
město	město	k1gNnSc1	město
kraji	kraj	k1gInSc6	kraj
Washington	Washington	k1gInSc1	Washington
County	Counta	k1gFnSc2	Counta
tohoto	tento	k3xDgInSc2	tento
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
žilo	žít	k5eAaImAgNnS	žít
7855	[number]	k4	7855
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
dělá	dělat	k5eAaImIp3nS	dělat
nejmenší	malý	k2eAgNnSc4d3	nejmenší
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
osadníci	osadník	k1gMnPc1	osadník
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
Montpelieru	Montpelier	k1gInSc2	Montpelier
přišli	přijít	k5eAaPmAgMnP	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1787	[number]	k4	1787
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1781	[number]	k4	1781
byl	být	k5eAaImAgInS	být
obecným	obecný	k2eAgNnSc7d1	obecné
shromážděním	shromáždění	k1gNnSc7	shromáždění
Vermont	Vermont	k1gMnSc1	Vermont
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
osadu	osada	k1gFnSc4	osada
plukovníkem	plukovník	k1gMnSc7	plukovník
Jacobem	Jacob	k1gMnSc7	Jacob
Davisem	Davis	k1gInSc7	Davis
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
místo	místo	k1gNnSc4	místo
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
po	po	k7c6	po
francouzském	francouzský	k2eAgNnSc6d1	francouzské
městě	město	k1gNnSc6	město
Montpellier	Montpellira	k1gFnPc2	Montpellira
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1805	[number]	k4	1805
byl	být	k5eAaImAgInS	být
Montpelier	Montpelier	k1gInSc1	Montpelier
zvolen	zvolit	k5eAaPmNgInS	zvolit
za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Vermontu	Vermont	k1gInSc2	Vermont
<g/>
,	,	kIx,	,
až	až	k8xS	až
roce	rok	k1gInSc6	rok
1818	[number]	k4	1818
Montpelier	Montpelier	k1gMnSc1	Montpelier
získal	získat	k5eAaPmAgMnS	získat
status	status	k1gInSc4	status
vesnice	vesnice	k1gFnSc2	vesnice
a	a	k8xC	a
1895	[number]	k4	1895
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
stalo	stát	k5eAaPmAgNnS	stát
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Montpelier	Montpelier	k1gInSc1	Montpelier
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nížinné	nížinný	k2eAgFnSc6d1	nížinná
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
(	(	kIx(	(
<g/>
182	[number]	k4	182
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obklopená	obklopený	k2eAgFnSc1d1	obklopená
kopci	kopec	k1gInPc7	kopec
a	a	k8xC	a
žulovými	žulový	k2eAgInPc7d1	žulový
skalními	skalní	k2eAgInPc7d1	skalní
převisy	převis	k1gInPc7	převis
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Winooski	Winoosk	k1gFnSc2	Winoosk
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
městu	město	k1gNnSc3	město
"	"	kIx"	"
<g/>
naděluje	nadělovat	k5eAaImIp3nS	nadělovat
<g/>
"	"	kIx"	"
periodické	periodický	k2eAgFnPc1d1	periodická
záplavy	záplava	k1gFnPc1	záplava
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc6	jenž
nejhorší	zlý	k2eAgMnSc1d3	nejhorší
zažilo	zažít	k5eAaPmAgNnS	zažít
městečko	městečko	k1gNnSc1	městečko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
a	a	k8xC	a
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
typickém	typický	k2eAgNnSc6d1	typické
pro	pro	k7c4	pro
Česko	Česko	k1gNnSc4	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
nejchladnějšími	chladný	k2eAgInPc7d3	nejchladnější
a	a	k8xC	a
nejteplejšími	teplý	k2eAgInPc7d3	nejteplejší
měsíci	měsíc	k1gInPc7	měsíc
činí	činit	k5eAaImIp3nS	činit
až	až	k9	až
k	k	k7c3	k
30	[number]	k4	30
°	°	k?	°
<g/>
C.	C.	kA	C.
Srážky	srážka	k1gFnSc2	srážka
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
60-80	[number]	k4	60-80
mm	mm	kA	mm
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
nejnižší	nízký	k2eAgMnSc1d3	nejnižší
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
50	[number]	k4	50
mm	mm	kA	mm
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
100	[number]	k4	100
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
městečkem	městečko	k1gNnSc7	městečko
Barre	Barr	k1gInSc5	Barr
tvoří	tvořit	k5eAaImIp3nP	tvořit
Montpelier	Montpelier	k1gInSc1	Montpelier
mikrokonurbaci	mikrokonurbak	k1gMnPc1	mikrokonurbak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmitisícovém	osmitisícový	k2eAgNnSc6d1	osmitisícové
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
žije	žít	k5eAaImIp3nS	žít
1940	[number]	k4	1940
rodin	rodina	k1gFnPc2	rodina
v	v	k7c6	v
3739	[number]	k4	3739
domech	dům	k1gInPc6	dům
nebo	nebo	k8xC	nebo
bytech	byt	k1gInPc6	byt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
přehoupl	přehoupnout	k5eAaPmAgInS	přehoupnout
šest	šest	k4xCc4	šest
tisícovek	tisícovka	k1gFnPc2	tisícovka
<g/>
,	,	kIx,	,
zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
sahal	sahat	k5eAaImAgInS	sahat
až	až	k6eAd1	až
k	k	k7c3	k
číslu	číslo	k1gNnSc3	číslo
8800	[number]	k4	8800
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
znát	znát	k5eAaImF	znát
odsun	odsun	k1gInSc4	odsun
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Věkové	věkový	k2eAgNnSc1d1	věkové
složení	složení	k1gNnSc1	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
přibližně	přibližně	k6eAd1	přibližně
21	[number]	k4	21
%	%	kIx~	%
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mladistvých	mladistvý	k1gMnPc2	mladistvý
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
8,6	[number]	k4	8,6
%	%	kIx~	%
dospělých	dospělí	k1gMnPc2	dospělí
do	do	k7c2	do
24	[number]	k4	24
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
28	[number]	k4	28
%	%	kIx~	%
do	do	k7c2	do
44	[number]	k4	44
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
27	[number]	k4	27
%	%	kIx~	%
do	do	k7c2	do
65	[number]	k4	65
let	léto	k1gNnPc2	léto
a	a	k8xC	a
15	[number]	k4	15
%	%	kIx~	%
seniorů	senior	k1gMnPc2	senior
<g/>
,	,	kIx,	,
s	s	k7c7	s
průměrným	průměrný	k2eAgInSc7d1	průměrný
věkem	věk	k1gInSc7	věk
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
je	být	k5eAaImIp3nS	být
84	[number]	k4	84
<g/>
:	:	kIx,	:
<g/>
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Montpelier	Montpelier	k1gInSc1	Montpelier
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
96	[number]	k4	96
%	%	kIx~	%
městem	město	k1gNnSc7	město
bělochů	běloch	k1gMnPc2	běloch
<g/>
,	,	kIx,	,
asijskoamerická	asijskoamerický	k2eAgFnSc1d1	asijskoamerický
a	a	k8xC	a
afroamerická	afroamerický	k2eAgFnSc1d1	afroamerická
komunita	komunita	k1gFnSc1	komunita
tvoří	tvořit	k5eAaImIp3nS	tvořit
každá	každý	k3xTgFnSc1	každý
necelé	celý	k2eNgNnSc4d1	necelé
procentní	procentní	k2eAgNnSc4d1	procentní
zastoupení	zastoupení	k1gNnSc4	zastoupení
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgNnPc1d1	ostatní
etnika	etnikum	k1gNnPc1	etnikum
ještě	ještě	k9	ještě
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Medián	medián	k1gInSc1	medián
příjmu	příjem	k1gInSc2	příjem
domácností	domácnost	k1gFnPc2	domácnost
je	být	k5eAaImIp3nS	být
37	[number]	k4	37
513	[number]	k4	513
(	(	kIx(	(
<g/>
nepatrně	patrně	k6eNd1	patrně
nad	nad	k7c4	nad
celoamerický	celoamerický	k2eAgInSc4d1	celoamerický
průměr	průměr	k1gInSc4	průměr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
medián	medián	k1gInSc1	medián
příjmu	příjem	k1gInSc2	příjem
rodin	rodina	k1gFnPc2	rodina
ale	ale	k8xC	ale
činí	činit	k5eAaImIp3nS	činit
51	[number]	k4	51
818	[number]	k4	818
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Příjem	příjem	k1gInSc1	příjem
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
je	být	k5eAaImIp3nS	být
22	[number]	k4	22
599	[number]	k4	599
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
9,8	[number]	k4	9,8
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
žije	žít	k5eAaImIp3nS	žít
na	na	k7c4	na
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Montpelieru	Montpelier	k1gInSc2	Montpelier
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
a	a	k8xC	a
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
žula	žula	k1gFnSc1	žula
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
v	v	k7c6	v
městě	město	k1gNnSc6	město
fungovala	fungovat	k5eAaImAgFnS	fungovat
výrobna	výrobna	k1gFnSc1	výrobna
kolíčků	kolíček	k1gInPc2	kolíček
na	na	k7c4	na
prádlo	prádlo	k1gNnSc4	prádlo
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
podniků	podnik	k1gInPc2	podnik
v	v	k7c6	v
Montpelieru	Montpelier	k1gInSc6	Montpelier
je	být	k5eAaImIp3nS	být
vlastněna	vlastněn	k2eAgFnSc1d1	vlastněna
místními	místní	k2eAgMnPc7d1	místní
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
(	(	kIx(	(
<g/>
Montpelier	Montpelier	k1gInSc1	Montpelier
High	Higha	k1gFnPc2	Higha
School	Schoola	k1gFnPc2	Schoola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
Vermont	Vermont	k1gMnSc1	Vermont
College	Colleg	k1gFnSc2	Colleg
of	of	k?	of
Fine	Fin	k1gMnSc5	Fin
Arts	Arts	k1gInSc4	Arts
-	-	kIx~	-
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
školu	škola	k1gFnSc4	škola
zaměřenou	zaměřený	k2eAgFnSc4d1	zaměřená
na	na	k7c4	na
tvůrčí	tvůrčí	k2eAgNnSc4d1	tvůrčí
psaní	psaní	k1gNnSc4	psaní
(	(	kIx(	(
<g/>
se	s	k7c7	s
specializací	specializace	k1gFnSc7	specializace
na	na	k7c4	na
psaní	psaní	k1gNnSc4	psaní
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislé	závislý	k2eNgNnSc1d1	nezávislé
studování	studování	k1gNnSc1	studování
s	s	k7c7	s
akademickými	akademický	k2eAgInPc7d1	akademický
kurzy	kurz	k1gInPc7	kurz
a	a	k8xC	a
workshopy	workshop	k1gInPc7	workshop
nabízí	nabízet	k5eAaImIp3nS	nabízet
program	program	k1gInSc1	program
<g/>
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Union	union	k1gInSc1	union
Institue	Institue	k1gFnSc1	Institue
and	and	k?	and
University	universita	k1gFnSc2	universita
of	of	k?	of
Vermont	Vermonta	k1gFnPc2	Vermonta
Center	centrum	k1gNnPc2	centrum
Master	master	k1gMnSc1	master
of	of	k?	of
Education	Education	k1gInSc1	Education
program	program	k1gInSc1	program
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Montpelier	Montpelier	k1gMnSc1	Montpelier
byl	být	k5eAaImAgMnS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
centrální	centrální	k2eAgFnSc3d1	centrální
poloze	poloha	k1gFnSc3	poloha
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
státu	stát	k1gInSc2	stát
Vermont	Vermonta	k1gFnPc2	Vermonta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
obcemi	obec	k1gFnPc7	obec
nejlépe	dobře	k6eAd3	dobře
dostupný	dostupný	k2eAgInSc1d1	dostupný
<g/>
.	.	kIx.	.
</s>
<s>
Nalézá	nalézat	k5eAaImIp3nS	nalézat
se	se	k3xPyFc4	se
na	na	k7c6	na
dálnici	dálnice	k1gFnSc6	dálnice
Interstate	Interstat	k1gInSc5	Interstat
89	[number]	k4	89
a	a	k8xC	a
lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
dostat	dostat	k5eAaPmF	dostat
i	i	k9	i
z	z	k7c2	z
exitu	exit	k1gInSc2	exit
8	[number]	k4	8
na	na	k7c4	na
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Route	Rout	k1gInSc5	Rout
2	[number]	k4	2
a	a	k8xC	a
Vermont	Vermont	k1gMnSc1	Vermont
Route	Rout	k1gInSc5	Rout
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Autobusovou	autobusový	k2eAgFnSc4d1	autobusová
dopravu	doprava	k1gFnSc4	doprava
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
The	The	k1gMnPc4	The
Green	Green	k2eAgInSc1d1	Green
Mountain	Mountain	k2eAgInSc1d1	Mountain
Transit	transit	k1gInSc1	transit
Authority	Authorita	k1gFnSc2	Authorita
(	(	kIx(	(
<g/>
GMTA	GMTA	kA	GMTA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
železniční	železniční	k2eAgFnSc4d1	železniční
dopravu	doprava	k1gFnSc4	doprava
je	být	k5eAaImIp3nS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
národní	národní	k2eAgInSc1d1	národní
železniční	železniční	k2eAgInSc1d1	železniční
systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
-	-	kIx~	-
Amtrak	Amtrak	k1gMnSc1	Amtrak
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
na	na	k7c6	na
severovýchodním	severovýchodní	k2eAgNnSc6d1	severovýchodní
pobřeží	pobřeží	k1gNnSc6	pobřeží
operuje	operovat	k5eAaImIp3nS	operovat
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
St.	st.	kA	st.
Albans	Albans	k1gInSc1	Albans
a	a	k8xC	a
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.	D.	kA	D.
C.	C.	kA	C.
Letecky	letecky	k6eAd1	letecky
má	mít	k5eAaImIp3nS	mít
Montpelier	Montpelier	k1gInSc1	Montpelier
nejblíže	blízce	k6eAd3	blízce
přístupné	přístupný	k2eAgNnSc4d1	přístupné
letiště	letiště	k1gNnSc4	letiště
Edward	Edward	k1gMnSc1	Edward
F.	F.	kA	F.
Knapp	Knapp	k1gMnSc1	Knapp
State	status	k1gInSc5	status
Airport	Airport	k1gInSc4	Airport
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
soukromé	soukromý	k2eAgInPc4d1	soukromý
lety	let	k1gInPc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
komerční	komerční	k2eAgInPc4d1	komerční
lety	let	k1gInPc4	let
je	být	k5eAaImIp3nS	být
nejbližší	blízký	k2eAgNnSc1d3	nejbližší
letiště	letiště	k1gNnSc1	letiště
Burlington	Burlington	k1gInSc1	Burlington
International	International	k1gMnSc1	International
Airport	Airport	k1gInSc1	Airport
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
od	od	k7c2	od
města	město	k1gNnSc2	město
vzdáleno	vzdálit	k5eAaPmNgNnS	vzdálit
asi	asi	k9	asi
56	[number]	k4	56
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
po	po	k7c6	po
vlastní	vlastní	k2eAgFnSc6d1	vlastní
ose	osa	k1gFnSc6	osa
je	být	k5eAaImIp3nS	být
Montpelier	Montpelier	k1gInSc1	Montpelier
dobrá	dobrý	k2eAgFnSc1d1	dobrá
volba	volba	k1gFnSc1	volba
jak	jak	k8xS	jak
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
turisty	turist	k1gMnPc4	turist
tak	tak	k6eAd1	tak
pro	pro	k7c4	pro
cykloturistiku	cykloturistika	k1gFnSc4	cykloturistika
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mu	on	k3xPp3gMnSc3	on
nechybí	chybit	k5eNaPmIp3nP	chybit
turistické	turistický	k2eAgFnPc1d1	turistická
a	a	k8xC	a
cyklostezky	cyklostezka	k1gFnPc1	cyklostezka
od	od	k7c2	od
vzdálenějšího	vzdálený	k2eAgNnSc2d2	vzdálenější
okolí	okolí	k1gNnSc2	okolí
až	až	k9	až
po	po	k7c4	po
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
</s>
