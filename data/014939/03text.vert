<s>
Werk	Werk	k1gInSc1
Arena	Aren	k1gInSc2
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Werk	Werk	k6eAd1
ArenaPlech	ArenaPl	k1gInPc6
Aréna	aréna	k1gFnSc1
Vchod	vchod	k1gInSc1
do	do	k7c2
administrativní	administrativní	k2eAgFnSc2d1
části	část	k1gFnSc2
Werk	Werka	k1gFnPc2
Areny	Arena	k1gFnSc2
Poloha	poloha	k1gFnSc1
</s>
<s>
Třinec	Třinec	k1gInSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
40	#num#	k4
<g/>
′	′	k?
<g/>
11,5	11,5	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
39	#num#	k4
<g/>
′	′	k?
<g/>
49,5	49,5	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Otevření	otevření	k1gNnSc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1967	#num#	k4
Přestavění	přestavění	k1gNnPc2
</s>
<s>
1976	#num#	k4
Uzavření	uzavření	k1gNnSc1
</s>
<s>
2016	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vlastník	vlastník	k1gMnSc1
</s>
<s>
Třinecká	třinecký	k2eAgFnSc1d1
aréna	aréna	k1gFnSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
Povrch	povrch	k1gInSc1
</s>
<s>
umělá	umělý	k2eAgFnSc1d1
ledová	ledový	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
Týmy	tým	k1gInPc4
</s>
<s>
HC	HC	kA
Oceláři	ocelář	k1gMnPc1
Třinec	Třinec	k1gInSc1
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
Kapacita	kapacita	k1gFnSc1
</s>
<s>
5	#num#	k4
200	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Rozměry	rozměra	k1gFnSc2
</s>
<s>
58,88	58,88	k4
x	x	k?
27,26	27,26	k4
m	m	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Werk	Werk	k1gInSc1
Arena	Aren	k1gInSc2
je	být	k5eAaImIp3nS
sportovní	sportovní	k2eAgInSc4d1
stadion	stadion	k1gInSc4
v	v	k7c6
Třinci	Třinec	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
odehrával	odehrávat	k5eAaImAgInS
svoje	svůj	k3xOyFgInPc4
domácí	domácí	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
klub	klub	k1gInSc1
HC	HC	kA
Oceláři	ocelář	k1gMnPc1
Třinec	Třinec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
roku	rok	k1gInSc2
1967	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
areálu	areál	k1gInSc6
na	na	k7c6
Lesní	lesní	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
postavena	postaven	k2eAgFnSc1d1
uměle	uměle	k6eAd1
chlazená	chlazený	k2eAgFnSc1d1
ledová	ledový	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1976	#num#	k4
byla	být	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
plocha	plocha	k1gFnSc1
zastřešena	zastřešen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ocelovou	ocelový	k2eAgFnSc4d1
konstrukci	konstrukce	k1gFnSc4
vyrobily	vyrobit	k5eAaPmAgFnP
Třinecké	třinecký	k2eAgFnPc1d1
železárny	železárna	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapacita	kapacita	k1gFnSc1
stadionu	stadion	k1gInSc2
dosahuje	dosahovat	k5eAaImIp3nS
5	#num#	k4
200	#num#	k4
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
extraligové	extraligový	k2eAgNnSc4d1
utkání	utkání	k1gNnSc4
Oceláři	ocelář	k1gMnPc1
Třinec	Třinec	k1gInSc4
odehrál	odehrát	k5eAaPmAgMnS
na	na	k7c6
tomto	tento	k3xDgInSc6
ledě	led	k1gInSc6
10	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2014	#num#	k4
proti	proti	k7c3
PSG	PSG	kA
Zlín	Zlín	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgNnSc6,k3yRgNnSc6,k3yQgNnSc6
prohráli	prohrát	k5eAaPmAgMnP
3	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
po	po	k7c6
prodloužení	prodloužení	k1gNnSc6
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
semifinálové	semifinálový	k2eAgNnSc1d1
utkání	utkání	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
sezóny	sezóna	k1gFnSc2
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
hraje	hrát	k5eAaImIp3nS
tým	tým	k1gInSc1
Ocelářů	ocelář	k1gMnPc2
v	v	k7c6
blízké	blízký	k2eAgFnSc6d1
nově	nově	k6eAd1
postavené	postavený	k2eAgFnSc6d1
sportovní	sportovní	k2eAgFnSc6d1
hale	hala	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
pojmenována	pojmenován	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
Werk	Werk	k1gInSc1
Arena	Aren	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
byla	být	k5eAaImAgFnS
ledová	ledový	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
stadionu	stadion	k1gInSc2
definitivně	definitivně	k6eAd1
odstavena	odstaven	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
i	i	k9
kvůli	kvůli	k7c3
otevření	otevření	k1gNnSc3
nové	nový	k2eAgFnSc2d1
tréninkové	tréninkový	k2eAgFnSc2d1
haly	hala	k1gFnSc2
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
nové	nový	k2eAgFnPc1d1
Werk	Werk	k1gInSc4
Areny	Arena	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
„	„	k?
<g/>
KONEC	konec	k1gInSc1
ÉRY	éra	k1gFnSc2
NA	na	k7c4
LESNÍ	lesní	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ZDÁ	zdát	k5eAaPmIp3nS,k5eAaImIp3nS
SE	se	k3xPyFc4
<g/>
,	,	kIx,
ŽE	že	k8xS
TEĎ	teď	k6eAd1
UŽ	už	k6eAd1
DEFINITIVNĚ	definitivně	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
hcofans	hcofans	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
0	#num#	k4
<g/>
7.04	7.04	k4
<g/>
.2016	.2016	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
„	„	k?
<g/>
Werk	Werk	k1gInSc1
Aréna	aréna	k1gFnSc1
-	-	kIx~
Třinec	Třinec	k1gInSc1
(	(	kIx(
<g/>
Zimní	zimní	k2eAgInSc1d1
stadion	stadion	k1gInSc1
<g/>
)	)	kIx)
<g/>
“	“	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
krasnecesko	krasnecesko	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
stadionu	stadion	k1gInSc6
na	na	k7c6
stránkách	stránka	k1gFnPc6
expedice	expedice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
rps	rps	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Video	video	k1gNnSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
|	|	kIx~
Slezsko	Slezsko	k1gNnSc1
</s>
