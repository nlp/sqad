<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Nepálu	Nepál	k1gInSc2	Nepál
je	být	k5eAaImIp3nS	být
tvarem	tvar	k1gInSc7	tvar
naprosto	naprosto	k6eAd1	naprosto
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
nestejně	stejně	k6eNd1	stejně
vysokých	vysoký	k2eAgFnPc2d1	vysoká
karmínových	karmínový	k2eAgFnPc2d1	karmínová
<g/>
,	,	kIx,	,
modře	modro	k6eAd1	modro
lemovaných	lemovaný	k2eAgInPc2d1	lemovaný
pravoúhlých	pravoúhlý	k2eAgInPc2d1	pravoúhlý
trojúhelníků	trojúhelník	k1gInPc2	trojúhelník
položených	položená	k1gFnPc2	položená
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
(	(	kIx(	(
<g/>
částečně	částečně	k6eAd1	částečně
přes	přes	k7c4	přes
sebe	sebe	k3xPyFc4	sebe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
