<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
Matka	matka	k1gFnSc1	matka
Tereza	Tereza	k1gFnSc1	Tereza
(	(	kIx(	(
<g/>
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Agnesë	Agnesë	k1gFnSc2	Agnesë
Gonxhe	Gonxhe	k1gNnSc3	Gonxhe
Bojaxhiu	Bojaxhium	k1gNnSc3	Bojaxhium
[	[	kIx(	[
<g/>
gondže	gondzat	k5eAaPmIp3nS	gondzat
bojadžiu	bojadžius	k1gMnSc3	bojadžius
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1910	[number]	k4	1910
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
indická	indický	k2eAgFnSc1d1	indická
humanitární	humanitární	k2eAgFnSc1d1	humanitární
pracovnice	pracovnice	k1gFnSc1	pracovnice
a	a	k8xC	a
řeholnice	řeholnice	k1gFnSc1	řeholnice
albánského	albánský	k2eAgInSc2d1	albánský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
uctívaná	uctívaný	k2eAgFnSc1d1	uctívaná
římskokatolickou	římskokatolický	k2eAgFnSc7d1	Římskokatolická
církví	církev	k1gFnSc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zakladatelkou	zakladatelka	k1gFnSc7	zakladatelka
a	a	k8xC	a
první	první	k4xOgFnSc7	první
představenou	představená	k1gFnSc7	představená
řádu	řád	k1gInSc2	řád
Misionářek	misionářka	k1gFnPc2	misionářka
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
humanitární	humanitární	k2eAgNnSc4d1	humanitární
dílo	dílo	k1gNnSc4	dílo
získala	získat	k5eAaPmAgFnS	získat
řadu	řada	k1gFnSc4	řada
ocenění	ocenění	k1gNnSc2	ocenění
<g/>
:	:	kIx,	:
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
nositelka	nositelka	k1gFnSc1	nositelka
Templetonovy	Templetonův	k2eAgFnSc2d1	Templetonova
ceny	cena	k1gFnSc2	cena
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
míru	mír	k1gInSc2	mír
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britský	britský	k2eAgInSc1d1	britský
Řád	řád	k1gInSc1	řád
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
indického	indický	k2eAgNnSc2d1	indické
civilního	civilní	k2eAgNnSc2d1	civilní
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
Bharat	Bharat	k1gMnSc1	Bharat
Ratna	Ratn	k1gMnSc2	Ratn
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Blahoslavenou	blahoslavený	k2eAgFnSc7d1	blahoslavená
byla	být	k5eAaImAgFnS	být
prohlášena	prohlášen	k2eAgFnSc1d1	prohlášena
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
a	a	k8xC	a
svatořečena	svatořečen	k2eAgFnSc1d1	svatořečena
byla	být	k5eAaImAgFnS	být
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	s	k7c7	s
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
v	v	k7c6	v
albánské	albánský	k2eAgFnSc6d1	albánská
rodině	rodina	k1gFnSc6	rodina
jako	jako	k8xC	jako
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
dcera	dcera	k1gFnSc1	dcera
úspěšného	úspěšný	k2eAgMnSc2d1	úspěšný
obchodníka	obchodník	k1gMnSc2	obchodník
a	a	k8xC	a
farmaceuta	farmaceut	k1gMnSc2	farmaceut
Nikolle	Nikoll	k1gMnSc2	Nikoll
Bojaxhiu	Bojaxhium	k1gNnSc3	Bojaxhium
a	a	k8xC	a
Drane	Dran	k1gInSc5	Dran
Bojaxhiu	Bojaxhius	k1gMnSc3	Bojaxhius
(	(	kIx(	(
<g/>
Barnaj	Barnaj	k1gMnSc1	Barnaj
rodné	rodný	k2eAgNnSc4d1	rodné
příjmení	příjmení	k1gNnSc4	příjmení
<g/>
)	)	kIx)	)
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Shkupi	Shkup	k1gFnSc2	Shkup
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Skopje	Skopje	k1gFnSc2	Skopje
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rodiště	rodiště	k1gNnSc1	rodiště
tehdy	tehdy	k6eAd1	tehdy
náleželo	náležet	k5eAaImAgNnS	náležet
Albánii	Albánie	k1gFnSc4	Albánie
v	v	k7c6	v
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Pokřtěna	pokřtěn	k2eAgFnSc1d1	pokřtěna
byla	být	k5eAaImAgFnS	být
jménem	jméno	k1gNnSc7	jméno
Agnese	Agnese	k1gFnSc2	Agnese
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
doma	doma	k6eAd1	doma
jí	on	k3xPp3gFnSc3	on
říkali	říkat	k5eAaImAgMnP	říkat
Gonxha	Gonxha	k1gFnSc1	Gonxha
(	(	kIx(	(
<g/>
poupátko	poupátko	k1gNnSc1	poupátko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
jí	jíst	k5eAaImIp3nS	jíst
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
bylo	být	k5eAaImAgNnS	být
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
dvanácti	dvanáct	k4xCc6	dvanáct
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
pro	pro	k7c4	pro
řeholní	řeholní	k2eAgInSc4d1	řeholní
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Balkánské	balkánský	k2eAgFnPc1d1	balkánská
války	válka	k1gFnPc1	válka
i	i	k9	i
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
změnily	změnit	k5eAaPmAgInP	změnit
politickou	politický	k2eAgFnSc4d1	politická
mapu	mapa	k1gFnSc4	mapa
světa	svět	k1gInSc2	svět
a	a	k8xC	a
Skopje	Skopje	k1gFnSc2	Skopje
připadlo	připadnout	k5eAaPmAgNnS	připadnout
nově	nově	k6eAd1	nově
vzniklému	vzniklý	k2eAgNnSc3d1	vzniklé
Království	království	k1gNnSc3	království
SHS	SHS	kA	SHS
<g/>
,	,	kIx,	,
od	od	k7c2	od
r.	r.	kA	r.
1929	[number]	k4	1929
Království	království	k1gNnPc2	království
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
Makedonii	Makedonie	k1gFnSc6	Makedonie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
události	událost	k1gFnPc1	událost
rozptýlily	rozptýlit	k5eAaPmAgFnP	rozptýlit
rodinu	rodina	k1gFnSc4	rodina
Bojaxhiuových	Bojaxhiuových	k2eAgFnSc4d1	Bojaxhiuových
po	po	k7c6	po
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Agnese	Agnese	k1gFnSc1	Agnese
odešla	odejít	k5eAaPmAgFnS	odejít
do	do	k7c2	do
Irska	Irsko	k1gNnSc2	Irsko
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
kongregace	kongregace	k1gFnSc2	kongregace
Loretánských	loretánský	k2eAgFnPc2d1	Loretánská
sester	sestra	k1gFnPc2	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
řeholní	řeholní	k2eAgNnSc1d1	řeholní
společenství	společenství	k1gNnSc1	společenství
bylo	být	k5eAaImAgNnS	být
aktivní	aktivní	k2eAgFnSc7d1	aktivní
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
Agnesu	Agnesa	k1gFnSc4	Agnesa
přitahovalo	přitahovat	k5eAaImAgNnS	přitahovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
přípravě	příprava	k1gFnSc6	příprava
v	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
Agnese	Agnese	k1gFnSc1	Agnese
odjela	odjet	k5eAaPmAgFnS	odjet
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dárdžilingu	Dárdžiling	k1gInSc6	Dárdžiling
složila	složit	k5eAaPmAgFnS	složit
slib	slib	k1gInSc4	slib
novicky	novicka	k1gFnSc2	novicka
a	a	k8xC	a
přijala	přijmout	k5eAaPmAgFnS	přijmout
jméno	jméno	k1gNnSc4	jméno
Tereza	Tereza	k1gFnSc1	Tereza
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
misionářské	misionářský	k2eAgFnSc6d1	misionářská
dívčí	dívčí	k2eAgFnSc6d1	dívčí
škole	škola	k1gFnSc6	škola
vyučovala	vyučovat	k5eAaImAgFnS	vyučovat
zeměpis	zeměpis	k1gInSc4	zeměpis
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nadání	nadání	k1gNnSc4	nadání
a	a	k8xC	a
organizační	organizační	k2eAgFnPc4d1	organizační
schopnosti	schopnost	k1gFnPc4	schopnost
bylo	být	k5eAaImAgNnS	být
Tereze	Tereza	k1gFnSc3	Tereza
svěřeno	svěřen	k2eAgNnSc1d1	svěřeno
vedení	vedení	k1gNnSc1	vedení
Vyšší	vysoký	k2eAgFnSc2d2	vyšší
misijní	misijní	k2eAgFnSc2d1	misijní
školy	škola	k1gFnSc2	škola
St.	st.	kA	st.
Mary	Mary	k1gFnSc1	Mary
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
High	High	k1gInSc1	High
School	School	k1gInSc1	School
v	v	k7c6	v
Kalkatě	Kalkata	k1gFnSc6	Kalkata
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
sestra	sestra	k1gFnSc1	sestra
Tereza	Tereza	k1gFnSc1	Tereza
po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
s	s	k7c7	s
představenými	představená	k1gFnPc7	představená
opustila	opustit	k5eAaPmAgFnS	opustit
loretánský	loretánský	k2eAgInSc4d1	loretánský
klášter	klášter	k1gInSc4	klášter
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaPmF	věnovat
nejchudším	chudý	k2eAgFnPc3d3	nejchudší
a	a	k8xC	a
umírajícím	umírající	k2eAgFnPc3d1	umírající
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
ji	on	k3xPp3gFnSc4	on
obklopil	obklopit	k5eAaPmAgInS	obklopit
dostatek	dostatek	k1gInSc1	dostatek
spolupracovnic	spolupracovnice	k1gFnPc2	spolupracovnice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
založit	založit	k5eAaPmF	založit
kalkatskou	kalkatský	k2eAgFnSc4d1	kalkatská
kongregaci	kongregace	k1gFnSc4	kongregace
Misionářek	misionářka	k1gFnPc2	misionářka
milosrdenství	milosrdenství	k1gNnSc2	milosrdenství
(	(	kIx(	(
<g/>
Misionářky	misionářka	k1gFnSc2	misionářka
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
kongregace	kongregace	k1gFnSc1	kongregace
Milosrdenství	milosrdenství	k1gNnSc2	milosrdenství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgInSc1d1	svatý
stolec	stolec	k1gInSc1	stolec
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1950	[number]	k4	1950
uznal	uznat	k5eAaPmAgInS	uznat
nové	nový	k2eAgNnSc4d1	nové
řeholní	řeholní	k2eAgNnSc4d1	řeholní
společenství	společenství	k1gNnSc4	společenství
řídící	řídící	k2eAgFnSc4d1	řídící
se	s	k7c7	s
františkánskými	františkánský	k2eAgInPc7d1	františkánský
principy	princip	k1gInPc7	princip
<g/>
.	.	kIx.	.
</s>
<s>
Tereza	Tereza	k1gFnSc1	Tereza
zakládala	zakládat	k5eAaImAgFnS	zakládat
útulky	útulek	k1gInPc4	útulek
pro	pro	k7c4	pro
umírající	umírající	k1gFnPc4	umírající
<g/>
,	,	kIx,	,
sirotky	sirotka	k1gFnPc4	sirotka
a	a	k8xC	a
opuštěné	opuštěný	k2eAgFnPc4d1	opuštěná
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Budovala	budovat	k5eAaImAgFnS	budovat
nemocnice	nemocnice	k1gFnSc1	nemocnice
a	a	k8xC	a
školy	škola	k1gFnPc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
roků	rok	k1gInPc2	rok
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
obrovské	obrovský	k2eAgNnSc4d1	obrovské
úsilí	úsilí	k1gNnSc4	úsilí
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
položit	položit	k5eAaPmF	položit
základy	základ	k1gInPc1	základ
velkému	velký	k2eAgNnSc3d1	velké
humanitárnímu	humanitární	k2eAgNnSc3d1	humanitární
dílu	dílo	k1gNnSc3	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
života	život	k1gInSc2	život
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
řadu	řada	k1gFnSc4	řada
cest	cesta	k1gFnPc2	cesta
po	po	k7c6	po
světě	svět	k1gInSc6	svět
a	a	k8xC	a
využívala	využívat	k5eAaPmAgFnS	využívat
své	svůj	k3xOyFgFnPc4	svůj
popularity	popularita	k1gFnPc4	popularita
a	a	k8xC	a
možností	možnost	k1gFnSc7	možnost
setkávat	setkávat	k5eAaImF	setkávat
se	se	k3xPyFc4	se
s	s	k7c7	s
významnými	významný	k2eAgFnPc7d1	významná
osobnostmi	osobnost	k1gFnPc7	osobnost
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
sociálních	sociální	k2eAgFnPc2d1	sociální
aktivit	aktivita	k1gFnPc2	aktivita
své	svůj	k3xOyFgFnSc2	svůj
kongregace	kongregace	k1gFnSc2	kongregace
a	a	k8xC	a
šíření	šíření	k1gNnSc2	šíření
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Odmítavý	odmítavý	k2eAgInSc4d1	odmítavý
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
potratům	potrat	k1gInPc3	potrat
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
podstatnou	podstatný	k2eAgFnSc7d1	podstatná
částí	část	k1gFnSc7	část
řeči	řeč	k1gFnSc2	řeč
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
udělení	udělení	k1gNnSc2	udělení
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Potraty	potrat	k1gInPc4	potrat
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c2	za
"	"	kIx"	"
<g/>
největšího	veliký	k2eAgMnSc2d3	veliký
ničitele	ničitel	k1gMnSc2	ničitel
míru	míra	k1gFnSc4	míra
současnosti	současnost	k1gFnSc2	současnost
<g/>
"	"	kIx"	"
a	a	k8xC	a
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Může	moct	k5eAaImIp3nS	moct
<g/>
-li	i	k?	-li
matka	matka	k1gFnSc1	matka
zabít	zabít	k5eAaPmF	zabít
své	svůj	k3xOyFgNnSc4	svůj
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
brání	bránit	k5eAaImIp3nP	bránit
mně	já	k3xPp1nSc3	já
zabít	zabít	k5eAaPmF	zabít
vás	vy	k3xPp2nPc4	vy
a	a	k8xC	a
vám	vy	k3xPp2nPc3	vy
zabít	zabít	k5eAaPmF	zabít
mě	já	k3xPp1nSc4	já
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Matka	matka	k1gFnSc1	matka
Tereza	Tereza	k1gFnSc1	Tereza
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
<g/>
,	,	kIx,	,
navštívila	navštívit	k5eAaPmAgFnS	navštívit
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
mnoho	mnoho	k4c4	mnoho
míst	místo	k1gNnPc2	místo
lidského	lidský	k2eAgNnSc2d1	lidské
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
:	:	kIx,	:
osadu	osada	k1gFnSc4	osada
s	s	k7c7	s
přesídlenci	přesídlenec	k1gMnPc7	přesídlenec
po	po	k7c6	po
černobylské	černobylský	k2eAgFnSc6d1	Černobylská
havárii	havárie	k1gFnSc6	havárie
<g/>
,	,	kIx,	,
v	v	k7c6	v
obleženém	obležený	k2eAgInSc6d1	obležený
Bejrútu	Bejrút	k1gInSc6	Bejrút
vyhledávala	vyhledávat	k5eAaImAgFnS	vyhledávat
děti	dítě	k1gFnPc4	dítě
ohrožené	ohrožený	k2eAgFnPc4d1	ohrožená
válkou	válka	k1gFnSc7	válka
-	-	kIx~	-
v	v	k7c6	v
rozvalinách	rozvalina	k1gFnPc6	rozvalina
města	město	k1gNnSc2	město
jich	on	k3xPp3gMnPc2	on
našla	najít	k5eAaPmAgFnS	najít
na	na	k7c4	na
šedesát	šedesát	k4xCc4	šedesát
zmrzačených	zmrzačený	k2eAgFnPc2d1	zmrzačená
a	a	k8xC	a
nemocných	mocný	k2eNgFnPc2d1	mocný
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
evakuovat	evakuovat	k5eAaBmF	evakuovat
do	do	k7c2	do
bezpečí	bezpečí	k1gNnSc2	bezpečí
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
šedesát	šedesát	k4xCc1	šedesát
dětí	dítě	k1gFnPc2	dítě
není	být	k5eNaImIp3nS	být
málo	málo	k1gNnSc1	málo
<g/>
,	,	kIx,	,
Matka	matka	k1gFnSc1	matka
Tereza	Tereza	k1gFnSc1	Tereza
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
<g/>
:	:	kIx,	:
Mě	já	k3xPp1nSc4	já
nezajímá	zajímat	k5eNaImIp3nS	zajímat
dav	dav	k1gInSc1	dav
<g/>
.	.	kIx.	.
</s>
<s>
Kdybych	kdyby	kYmCp1nS	kdyby
viděla	vidět	k5eAaImAgFnS	vidět
davy	dav	k1gInPc4	dav
ubožáků	ubožák	k1gMnPc2	ubožák
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
bych	by	kYmCp1nS	by
nemohla	moct	k5eNaImAgFnS	moct
svoji	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
začít	začít	k5eAaPmF	začít
<g/>
.	.	kIx.	.
</s>
<s>
Zajímá	zajímat	k5eAaImIp3nS	zajímat
mě	já	k3xPp1nSc4	já
jen	jen	k9	jen
můj	můj	k3xOp1gMnSc1	můj
bližní	bližní	k1gMnSc1	bližní
<g/>
,	,	kIx,	,
jednotlivec	jednotlivec	k1gMnSc1	jednotlivec
<g/>
,	,	kIx,	,
tomu	ten	k3xDgNnSc3	ten
mohu	moct	k5eAaImIp1nS	moct
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
pomáhám	pomáhat	k5eAaImIp1nS	pomáhat
<g/>
...	...	k?	...
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
při	při	k7c6	při
oslavách	oslava	k1gFnPc6	oslava
čtyřicátého	čtyřicátý	k4xOgNnSc2	čtyřicátý
výročí	výročí	k1gNnPc2	výročí
založení	založení	k1gNnSc2	založení
OSN	OSN	kA	OSN
měl	mít	k5eAaImAgMnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
s	s	k7c7	s
prostým	prostý	k2eAgInSc7d1	prostý
názvem	název	k1gInSc7	název
Matka	matka	k1gFnSc1	matka
Tereza	Tereza	k1gFnSc1	Tereza
<g/>
.	.	kIx.	.
</s>
<s>
Autorky	autorka	k1gFnPc1	autorka
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnSc2d1	známá
americké	americký	k2eAgFnSc2d1	americká
dokumentaristky	dokumentaristka	k1gFnSc2	dokumentaristka
Ann	Ann	k1gFnSc2	Ann
a	a	k8xC	a
Janette	Janett	k1gInSc5	Janett
Petrioovy	Petrioův	k2eAgFnPc1d1	Petrioův
(	(	kIx(	(
<g/>
slovem	slovem	k6eAd1	slovem
dokument	dokument	k1gInSc4	dokument
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
David	David	k1gMnSc1	David
Attenborough	Attenborough	k1gMnSc1	Attenborough
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zachytily	zachytit	k5eAaPmAgInP	zachytit
práci	práce	k1gFnSc4	práce
sester	sestra	k1gFnPc2	sestra
Matky	matka	k1gFnSc2	matka
Terezy	Tereza	k1gFnSc2	Tereza
na	na	k7c6	na
nejzbídačenějších	zbídačený	k2eAgNnPc6d3	zbídačený
místech	místo	k1gNnPc6	místo
naší	náš	k3xOp1gFnSc2	náš
planety	planeta	k1gFnSc2	planeta
<g/>
:	:	kIx,	:
Etiopii	Etiopie	k1gFnSc4	Etiopie
vysílenou	vysílený	k2eAgFnSc4d1	vysílená
hladomorem	hladomor	k1gInSc7	hladomor
<g/>
,	,	kIx,	,
Guatemalu	Guatemala	k1gFnSc4	Guatemala
po	po	k7c6	po
ničivém	ničivý	k2eAgNnSc6d1	ničivé
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
<g/>
,	,	kIx,	,
otřesné	otřesný	k2eAgInPc1d1	otřesný
brlohy	brloh	k1gInPc1	brloh
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
zářivých	zářivý	k2eAgFnPc2d1	zářivá
velkoměst	velkoměsto	k1gNnPc2	velkoměsto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
film	film	k1gInSc1	film
s	s	k7c7	s
provokativním	provokativní	k2eAgInSc7d1	provokativní
názvem	název	k1gInSc7	název
Matka	matka	k1gFnSc1	matka
Tereza	Tereza	k1gFnSc1	Tereza
<g/>
:	:	kIx,	:
Anděl	Anděla	k1gFnPc2	Anděla
pekla	peklo	k1gNnSc2	peklo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byla	být	k5eAaImAgFnS	být
řeholnice	řeholnice	k1gFnSc1	řeholnice
obviněna	obvinit	k5eAaPmNgFnS	obvinit
ze	z	k7c2	z
solidarity	solidarita	k1gFnSc2	solidarita
s	s	k7c7	s
diktátorskými	diktátorský	k2eAgInPc7d1	diktátorský
režimy	režim	k1gInPc7	režim
<g/>
.	.	kIx.	.
</s>
<s>
Umožnila	umožnit	k5eAaPmAgFnS	umožnit
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
za	za	k7c4	za
příspěvky	příspěvek	k1gInPc4	příspěvek
na	na	k7c4	na
její	její	k3xOp3gInPc4	její
útulky	útulek	k1gInPc4	útulek
hřáli	hřát	k5eAaImAgMnP	hřát
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
popularitě	popularita	k1gFnSc6	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
filmu	film	k1gInSc6	film
následovala	následovat	k5eAaImAgFnS	následovat
knižní	knižní	k2eAgFnSc1d1	knižní
verze	verze	k1gFnSc1	verze
Christophera	Christophera	k1gFnSc1	Christophera
Hitchense	Hitchense	k1gFnSc1	Hitchense
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Missionary	Missionara	k1gFnSc2	Missionara
Position	Position	k1gInSc1	Position
<g/>
:	:	kIx,	:
Mother	Mothra	k1gFnPc2	Mothra
Teresa	Teresa	k1gFnSc1	Teresa
In	In	k1gFnSc1	In
Theory	Theora	k1gFnSc2	Theora
And	Anda	k1gFnPc2	Anda
Practice	Practice	k1gFnSc2	Practice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
autor	autor	k1gMnSc1	autor
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
nedostatečnou	dostatečný	k2eNgFnSc4d1	nedostatečná
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
nemocné	nemocný	k1gMnPc4	nemocný
a	a	k8xC	a
umírající	umírající	k1gFnPc4	umírající
v	v	k7c6	v
domech	dům	k1gInPc6	dům
kongregace	kongregace	k1gFnSc1	kongregace
milosrdenství	milosrdenství	k1gNnPc1	milosrdenství
<g/>
,	,	kIx,	,
údajné	údajný	k2eAgFnPc1d1	údajná
snahy	snaha	k1gFnPc1	snaha
Matky	matka	k1gFnSc2	matka
Terezy	Tereza	k1gFnSc2	Tereza
získávat	získávat	k5eAaImF	získávat
své	svůj	k3xOyFgMnPc4	svůj
pacienty	pacient	k1gMnPc4	pacient
pro	pro	k7c4	pro
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
odmítání	odmítání	k1gNnSc3	odmítání
potratů	potrat	k1gInPc2	potrat
a	a	k8xC	a
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
a	a	k8xC	a
již	již	k6eAd1	již
zmíněné	zmíněný	k2eAgInPc4d1	zmíněný
styky	styk	k1gInPc4	styk
s	s	k7c7	s
diktátorskými	diktátorský	k2eAgInPc7d1	diktátorský
režimy	režim	k1gInPc7	režim
<g/>
.	.	kIx.	.
</s>
<s>
Mnoha	mnoho	k4c3	mnoho
lidem	člověk	k1gMnPc3	člověk
vadilo	vadit	k5eAaImAgNnS	vadit
její	její	k3xOp3gNnSc1	její
odmítání	odmítání	k1gNnSc1	odmítání
potratů	potrat	k1gInPc2	potrat
a	a	k8xC	a
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Kritikové	kritik	k1gMnPc1	kritik
ji	on	k3xPp3gFnSc4	on
označovali	označovat	k5eAaImAgMnP	označovat
za	za	k7c4	za
fanatickou	fanatický	k2eAgFnSc4d1	fanatická
antifeministku	antifeministka	k1gFnSc4	antifeministka
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
kvůli	kvůli	k7c3	kvůli
výroku	výrok	k1gInSc3	výrok
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
nikdy	nikdy	k6eAd1	nikdy
nesvěřila	svěřit	k5eNaPmAgFnS	svěřit
dítě	dítě	k1gNnSc4	dítě
k	k	k7c3	k
adopci	adopce	k1gFnSc3	adopce
matce	matka	k1gFnSc3	matka
užívající	užívající	k2eAgFnSc4d1	užívající
antikoncepci	antikoncepce	k1gFnSc4	antikoncepce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
důkaz	důkaz	k1gInSc1	důkaz
její	její	k3xOp3gFnSc2	její
neschopnosti	neschopnost	k1gFnSc2	neschopnost
milovat	milovat	k5eAaImF	milovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
poukázáno	poukázat	k5eAaPmNgNnS	poukázat
na	na	k7c6	na
její	její	k3xOp3gNnSc4	její
údajné	údajný	k2eAgNnSc4d1	údajné
pokrytectví	pokrytectví	k1gNnSc4	pokrytectví
<g/>
:	:	kIx,	:
zatímco	zatímco	k8xS	zatímco
prý	prý	k9	prý
svým	svůj	k3xOyFgMnPc3	svůj
pacientům	pacient	k1gMnPc3	pacient
odbornou	odborný	k2eAgFnSc4d1	odborná
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
péči	péče	k1gFnSc4	péče
nedopřávala	dopřávat	k5eNaImAgFnS	dopřávat
<g/>
,	,	kIx,	,
jen	jen	k8xS	jen
jim	on	k3xPp3gInPc3	on
ulehčovala	ulehčovat	k5eAaImAgFnS	ulehčovat
umírání	umírání	k1gNnSc4	umírání
s	s	k7c7	s
poukazem	poukaz	k1gInSc7	poukaz
na	na	k7c4	na
milost	milost	k1gFnSc4	milost
boží	božit	k5eAaImIp3nS	božit
(	(	kIx(	(
<g/>
kniha	kniha	k1gFnSc1	kniha
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
objektivně	objektivně	k6eAd1	objektivně
léčitelní	léčitelný	k2eAgMnPc1d1	léčitelný
pacienti	pacient	k1gMnPc1	pacient
nebyli	být	k5eNaImAgMnP	být
odesílání	odesílání	k1gNnSc4	odesílání
do	do	k7c2	do
nemocnic	nemocnice	k1gFnPc2	nemocnice
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
ponecháni	ponechán	k2eAgMnPc1d1	ponechán
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sama	sám	k3xTgFnSc1	sám
se	se	k3xPyFc4	se
léčila	léčit	k5eAaImAgFnS	léčit
na	na	k7c6	na
drahých	drahý	k2eAgFnPc6d1	drahá
klinikách	klinika	k1gFnPc6	klinika
západního	západní	k2eAgInSc2d1	západní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
největší	veliký	k2eAgFnSc4d3	veliký
kontroverzi	kontroverze	k1gFnSc4	kontroverze
okolo	okolo	k7c2	okolo
Matky	matka	k1gFnSc2	matka
Terezy	Tereza	k1gFnSc2	Tereza
rozpoutala	rozpoutat	k5eAaPmAgFnS	rozpoutat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
skupina	skupina	k1gFnSc1	skupina
kanadských	kanadský	k2eAgMnPc2d1	kanadský
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
Misionářek	misionářka	k1gFnPc2	misionářka
milosrdenství	milosrdenství	k1gNnPc2	milosrdenství
odhalila	odhalit	k5eAaPmAgFnS	odhalit
otřesné	otřesný	k2eAgFnPc4d1	otřesná
hygienické	hygienický	k2eAgFnPc4d1	hygienická
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc4	nedostatek
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
mizivé	mizivý	k2eAgNnSc4d1	mizivé
množství	množství	k1gNnSc4	množství
utišujících	utišující	k2eAgInPc2d1	utišující
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byl	být	k5eAaImAgInS	být
specifický	specifický	k2eAgInSc1d1	specifický
pohled	pohled	k1gInSc1	pohled
Matky	matka	k1gFnSc2	matka
Terezy	Tereza	k1gFnSc2	Tereza
na	na	k7c4	na
utrpení	utrpení	k1gNnSc4	utrpení
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
krásné	krásný	k2eAgNnSc1d1	krásné
vidět	vidět	k5eAaImF	vidět
chudé	chudý	k2eAgMnPc4d1	chudý
a	a	k8xC	a
nemocné	nemocný	k1gMnPc4	nemocný
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
trpně	trpně	k6eAd1	trpně
přijímají	přijímat	k5eAaImIp3nP	přijímat
svůj	svůj	k3xOyFgInSc4	svůj
úděl	úděl	k1gInSc4	úděl
a	a	k8xC	a
trpí	trpět	k5eAaImIp3nS	trpět
jako	jako	k9	jako
Kristus	Kristus	k1gMnSc1	Kristus
o	o	k7c6	o
pašijích	pašije	k1gFnPc6	pašije
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc7	jejich
utrpením	utrpení	k1gNnSc7	utrpení
bohatší	bohatý	k2eAgInSc1d2	bohatší
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
řekla	říct	k5eAaPmAgFnS	říct
matka	matka	k1gFnSc1	matka
Tereza	Tereza	k1gFnSc1	Tereza
při	při	k7c6	při
rozhovoru	rozhovor	k1gInSc6	rozhovor
britskému	britský	k2eAgMnSc3d1	britský
novináři	novinář	k1gMnSc3	novinář
Christopheru	Christopher	k1gMnSc3	Christopher
Hitchensovi	Hitchens	k1gMnSc3	Hitchens
<g/>
.	.	kIx.	.
</s>
<s>
Hitchens	Hitchens	k6eAd1	Hitchens
také	také	k9	také
upozornil	upozornit	k5eAaPmAgMnS	upozornit
na	na	k7c6	na
praxi	praxe	k1gFnSc6	praxe
křtu	křest	k1gInSc2	křest
bezmocných	bezmocný	k2eAgMnPc2d1	bezmocný
umírajících	umírající	k1gMnPc2	umírající
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
řádové	řádový	k2eAgFnPc1d1	řádová
sestry	sestra	k1gFnPc1	sestra
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
pokyn	pokyn	k1gInSc4	pokyn
běžně	běžně	k6eAd1	běžně
praktikovaly	praktikovat	k5eAaImAgInP	praktikovat
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
podle	podle	k7c2	podle
církevních	církevní	k2eAgInPc2d1	církevní
řádů	řád	k1gInPc2	řád
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
křtu	křest	k1gInSc2	křest
třeba	třeba	k6eAd1	třeba
informovaného	informovaný	k2eAgInSc2d1	informovaný
souhlasu	souhlas	k1gInSc2	souhlas
křtěnce	křtěnec	k1gMnSc4	křtěnec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Matky	matka	k1gFnSc2	matka
Terezy	Tereza	k1gFnSc2	Tereza
vyšly	vyjít	k5eAaPmAgInP	vyjít
knižně	knižně	k6eAd1	knižně
její	její	k3xOp3gInPc1	její
deníky	deník	k1gInPc1	deník
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
povolání	povolání	k1gNnSc6	povolání
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
ztratila	ztratit	k5eAaPmAgFnS	ztratit
jistotu	jistota	k1gFnSc4	jistota
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
Kristu	Krista	k1gFnSc4	Krista
sloužila	sloužit	k5eAaImAgFnS	sloužit
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
desetiletí	desetiletí	k1gNnPc2	desetiletí
navzdory	navzdory	k7c3	navzdory
hlubokým	hluboký	k2eAgFnPc3d1	hluboká
pochybnostem	pochybnost	k1gFnPc3	pochybnost
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Tereza	Tereza	k1gFnSc1	Tereza
navštívila	navštívit	k5eAaPmAgFnS	navštívit
socialistické	socialistický	k2eAgNnSc4d1	socialistické
Československo	Československo	k1gNnSc4	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
kardinála	kardinál	k1gMnSc2	kardinál
Františka	František	k1gMnSc2	František
Tomáška	Tomášek	k1gMnSc2	Tomášek
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
navštívila	navštívit	k5eAaPmAgFnS	navštívit
Prahu	Praha	k1gFnSc4	Praha
a	a	k8xC	a
Brno	Brno	k1gNnSc4	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
přijala	přijmout	k5eAaPmAgFnS	přijmout
pozvání	pozvání	k1gNnSc4	pozvání
prezidenta	prezident	k1gMnSc2	prezident
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
6	[number]	k4	6
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Na	na	k7c6	na
Zátorce	Zátorka	k1gFnSc6	Zátorka
jednopatrovou	jednopatrový	k2eAgFnSc4d1	jednopatrová
vilu	vila	k1gFnSc4	vila
pro	pro	k7c4	pro
zřízení	zřízení	k1gNnSc4	zřízení
útulku	útulek	k1gInSc2	útulek
své	svůj	k3xOyFgFnSc2	svůj
kongregace	kongregace	k1gFnSc2	kongregace
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
návštěva	návštěva	k1gFnSc1	návštěva
tehdy	tehdy	k6eAd1	tehdy
už	už	k6eAd1	už
federativního	federativní	k2eAgNnSc2d1	federativní
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
když	když	k8xS	když
její	její	k3xOp3gFnPc1	její
sestry	sestra	k1gFnPc1	sestra
založily	založit	k5eAaPmAgFnP	založit
útulek	útulek	k1gInSc4	útulek
v	v	k7c6	v
Bratislavě-Rači	Bratislavě-Rač	k1gInSc6	Bratislavě-Rač
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
papeže	papež	k1gMnSc2	papež
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
dostala	dostat	k5eAaPmAgFnS	dostat
srdeční	srdeční	k2eAgInSc4d1	srdeční
infarkt	infarkt	k1gInSc4	infarkt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
infarktu	infarkt	k1gInSc6	infarkt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
jí	on	k3xPp3gFnSc2	on
byl	být	k5eAaImAgInS	být
voperován	voperován	k2eAgInSc1d1	voperován
kardiostimulátor	kardiostimulátor	k1gInSc1	kardiostimulátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
Mexika	Mexiko	k1gNnSc2	Mexiko
dostala	dostat	k5eAaPmAgFnS	dostat
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
její	její	k3xOp3gFnPc4	její
srdeční	srdeční	k2eAgFnPc4d1	srdeční
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
svou	svůj	k3xOyFgFnSc4	svůj
rezignaci	rezignace	k1gFnSc4	rezignace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ovšem	ovšem	k9	ovšem
nebyla	být	k5eNaImAgFnS	být
řádem	řád	k1gInSc7	řád
přijata	přijmout	k5eAaPmNgFnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
si	se	k3xPyFc3	se
zlomila	zlomit	k5eAaPmAgFnS	zlomit
klíční	klíční	k2eAgFnSc1d1	klíční
kost	kost	k1gFnSc1	kost
a	a	k8xC	a
záchvat	záchvat	k1gInSc1	záchvat
malárie	malárie	k1gFnSc2	malárie
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
její	její	k3xOp3gFnPc4	její
srdeční	srdeční	k2eAgFnPc4d1	srdeční
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
z	z	k7c2	z
Kalkaty	Kalkata	k1gFnSc2	Kalkata
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
Sebastina	Sebastin	k1gMnSc2	Sebastin
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Souza	Souz	k1gMnSc2	Souz
<g/>
,	,	kIx,	,
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
nechal	nechat	k5eAaPmAgMnS	nechat
vyhnat	vyhnat	k5eAaPmF	vyhnat
ďábla	ďábel	k1gMnSc4	ďábel
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
prestižní	prestižní	k2eAgFnSc6d1	prestižní
nemocnici	nemocnice	k1gFnSc6	nemocnice
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
podstoupila	podstoupit	k5eAaPmAgFnS	podstoupit
operaci	operace	k1gFnSc4	operace
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1997	[number]	k4	1997
Matka	matka	k1gFnSc1	matka
Tereza	Tereza	k1gFnSc1	Tereza
<g/>
,	,	kIx,	,
těžce	těžce	k6eAd1	těžce
nemocná	mocný	k2eNgFnSc1d1	mocný
a	a	k8xC	a
unavená	unavený	k2eAgFnSc1d1	unavená
stářím	stáří	k1gNnSc7	stáří
<g/>
,	,	kIx,	,
předala	předat	k5eAaPmAgFnS	předat
vedení	vedení	k1gNnSc4	vedení
řádu	řád	k1gInSc6	řád
své	svůj	k3xOyFgFnSc3	svůj
zástupkyni	zástupkyně	k1gFnSc3	zástupkyně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvouměsíční	dvouměsíční	k2eAgFnSc6d1	dvouměsíční
poradě	porada	k1gFnSc6	porada
nakonec	nakonec	k6eAd1	nakonec
sestry	sestra	k1gFnPc1	sestra
generální	generální	k2eAgFnPc1d1	generální
kapituly	kapitula	k1gFnPc1	kapitula
řádu	řád	k1gInSc2	řád
zvolily	zvolit	k5eAaPmAgFnP	zvolit
za	za	k7c4	za
novou	nový	k2eAgFnSc4d1	nová
představenou	představená	k1gFnSc4	představená
řádu	řád	k1gInSc2	řád
třiapadesátiletou	třiapadesátiletý	k2eAgFnSc4d1	třiapadesátiletá
sestru	sestra	k1gFnSc4	sestra
Nirmalu	Nirmal	k1gInSc2	Nirmal
<g/>
,	,	kIx,	,
konvertitku	konvertitka	k1gFnSc4	konvertitka
z	z	k7c2	z
hinduismu	hinduismus	k1gInSc2	hinduismus
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Tereza	Tereza	k1gFnSc1	Tereza
zemřela	zemřít	k5eAaPmAgFnS	zemřít
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
státní	státní	k2eAgInSc1d1	státní
smutek	smutek	k1gInSc1	smutek
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
zesnulé	zesnulá	k1gFnSc2	zesnulá
bylo	být	k5eAaImAgNnS	být
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
v	v	k7c6	v
kalkatském	kalkatský	k2eAgInSc6d1	kalkatský
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Tomáše	Tomáš	k1gMnSc2	Tomáš
-	-	kIx~	-
rozloučit	rozloučit	k5eAaPmF	rozloučit
se	se	k3xPyFc4	se
přišly	přijít	k5eAaPmAgInP	přijít
statisíce	statisíce	k1gInPc1	statisíce
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
její	její	k3xOp3gFnSc2	její
smrti	smrt	k1gFnSc2	smrt
měl	mít	k5eAaImAgInS	mít
její	její	k3xOp3gInSc1	její
řád	řád	k1gInSc1	řád
přes	přes	k7c4	přes
4	[number]	k4	4
000	[number]	k4	000
sester	sestra	k1gFnPc2	sestra
v	v	k7c6	v
610	[number]	k4	610
misiích	misie	k1gFnPc6	misie
(	(	kIx(	(
<g/>
123	[number]	k4	123
zemí	zem	k1gFnPc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
byla	být	k5eAaImAgFnS	být
Matka	matka	k1gFnSc1	matka
Tereza	Tereza	k1gFnSc1	Tereza
pohřbena	pohřben	k2eAgFnSc1d1	pohřbena
-	-	kIx~	-
osm	osm	k4xCc1	osm
indických	indický	k2eAgMnPc2d1	indický
vojáků	voják	k1gMnPc2	voják
vyneslo	vynést	k5eAaPmAgNnS	vynést
z	z	k7c2	z
chrámu	chrám	k1gInSc2	chrám
její	její	k3xOp3gNnSc4	její
tělo	tělo	k1gNnSc4	tělo
zabalené	zabalený	k2eAgNnSc4d1	zabalené
do	do	k7c2	do
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
a	a	k8xC	a
položilo	položit	k5eAaPmAgNnS	položit
na	na	k7c4	na
dělovou	dělový	k2eAgFnSc4d1	dělová
lafetu	lafeta	k1gFnSc4	lafeta
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
před	před	k7c7	před
padesáti	padesát	k4xCc7	padesát
lety	léto	k1gNnPc7	léto
spočívalo	spočívat	k5eAaImAgNnS	spočívat
tělo	tělo	k1gNnSc1	tělo
mrtvého	mrtvý	k1gMnSc2	mrtvý
Mahátmy	Mahátma	k1gFnSc2	Mahátma
Gándhího	Gándhí	k1gMnSc2	Gándhí
<g/>
.	.	kIx.	.
</s>
<s>
Pohřebního	pohřební	k2eAgInSc2d1	pohřební
obřadu	obřad	k1gInSc2	obřad
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
na	na	k7c4	na
dvacet	dvacet	k4xCc4	dvacet
tisíc	tisíc	k4xCgInSc4	tisíc
hostů	host	k1gMnPc2	host
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1999	[number]	k4	1999
-	-	kIx~	-
V	v	k7c6	v
Kalkatě	Kalkata	k1gFnSc6	Kalkata
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
dispenze	dispenz	k1gFnSc2	dispenz
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
zahájil	zahájit	k5eAaPmAgInS	zahájit
proces	proces	k1gInSc1	proces
blahořečení	blahořečení	k1gNnSc2	blahořečení
Matky	matka	k1gFnSc2	matka
Terezy	Tereza	k1gFnSc2	Tereza
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2001	[number]	k4	2001
-	-	kIx~	-
Diecézní	diecézní	k2eAgInSc1d1	diecézní
proces	proces	k1gInSc1	proces
byl	být	k5eAaImAgInS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
a	a	k8xC	a
materiály	materiál	k1gInPc4	materiál
odeslány	odeslán	k2eAgInPc4d1	odeslán
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
k	k	k7c3	k
přezkoumání	přezkoumání	k1gNnSc3	přezkoumání
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2002	[number]	k4	2002
-	-	kIx~	-
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
vydal	vydat	k5eAaPmAgMnS	vydat
dva	dva	k4xCgInPc4	dva
dekrety	dekret	k1gInPc4	dekret
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
uznávají	uznávat	k5eAaImIp3nP	uznávat
heroické	heroický	k2eAgFnPc1d1	heroická
ctnosti	ctnost	k1gFnPc1	ctnost
Matky	matka	k1gFnSc2	matka
Terezy	Tereza	k1gFnSc2	Tereza
a	a	k8xC	a
také	také	k9	také
zázrak	zázrak	k1gInSc1	zázrak
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
základě	základ	k1gInSc6	základ
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
Matka	matka	k1gFnSc1	matka
Tereza	Tereza	k1gFnSc1	Tereza
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
za	za	k7c4	za
blahoslavenou	blahoslavený	k2eAgFnSc4d1	blahoslavená
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nevysvětlitelně	vysvětlitelně	k6eNd1	vysvětlitelně
náhlé	náhlý	k2eAgNnSc4d1	náhlé
uzdravení	uzdravení	k1gNnSc4	uzdravení
mladé	mladý	k2eAgFnSc2d1	mladá
indické	indický	k2eAgFnSc2d1	indická
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
vyznavačky	vyznavačka	k1gFnSc2	vyznavačka
animistického	animistický	k2eAgNnSc2d1	animistické
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2003	[number]	k4	2003
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Světového	světový	k2eAgInSc2d1	světový
misijního	misijní	k2eAgInSc2d1	misijní
dne	den	k1gInSc2	den
byla	být	k5eAaImAgFnS	být
Matka	matka	k1gFnSc1	matka
Tereza	Tereza	k1gFnSc1	Tereza
slavnostně	slavnostně	k6eAd1	slavnostně
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
za	za	k7c4	za
blahoslavenou	blahoslavený	k2eAgFnSc4d1	blahoslavená
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
uznal	uznat	k5eAaPmAgMnS	uznat
František	František	k1gMnSc1	František
druhý	druhý	k4xOgInSc4	druhý
zázrak	zázrak	k1gInSc4	zázrak
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
přímluvu	přímluva	k1gFnSc4	přímluva
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zázrak	zázrak	k1gInSc4	zázrak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
před	před	k7c7	před
7	[number]	k4	7
lety	let	k1gInPc7	let
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
uzdravení	uzdravení	k1gNnSc6	uzdravení
pacienta	pacient	k1gMnSc2	pacient
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
trpěl	trpět	k5eAaImAgMnS	trpět
zánětem	zánět	k1gInSc7	zánět
mozku	mozek	k1gInSc2	mozek
virového	virový	k2eAgInSc2d1	virový
původu	původ	k1gInSc2	původ
v	v	k7c6	v
brazilském	brazilský	k2eAgInSc6d1	brazilský
Santosu	Santosa	k1gFnSc4	Santosa
<g/>
..	..	k?	..
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
podepsal	podepsat	k5eAaPmAgMnS	podepsat
papež	papež	k1gMnSc1	papež
František	František	k1gMnSc1	František
dekret	dekret	k1gInSc4	dekret
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
svatořečení	svatořečení	k1gNnSc3	svatořečení
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
za	za	k7c7	za
svatou	svatá	k1gFnSc7	svatá
<g/>
.	.	kIx.	.
</s>
