<s>
Willis	Willis	k1gFnSc1	Willis
Tower	Towra	k1gFnPc2	Towra
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
Sears	Sears	k1gInSc1	Sears
Tower	Tower	k1gInSc4	Tower
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
442	[number]	k4	442
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgInSc1d1	vysoký
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
v	v	k7c6	v
Illinois	Illinois	k1gFnSc6	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgMnS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1970	[number]	k4	1970
až	až	k9	až
1973	[number]	k4	1973
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
firmy	firma	k1gFnSc2	firma
Skidmore	Skidmor	k1gInSc5	Skidmor
<g/>
,	,	kIx,	,
Owings	Owings	k1gInSc1	Owings
and	and	k?	and
Merrill	Merrill	k1gInSc1	Merrill
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgInSc7	druhý
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
mrakodrapem	mrakodrap	k1gInSc7	mrakodrap
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
One	One	k1gFnSc6	One
World	Worlda	k1gFnPc2	Worlda
Trade	Trad	k1gInSc5	Trad
Center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
dokončení	dokončení	k1gNnSc2	dokončení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
výškou	výška	k1gFnSc7	výška
překonal	překonat	k5eAaPmAgInS	překonat
věže	věž	k1gFnSc2	věž
World	World	k1gInSc1	World
Trade	Trad	k1gInSc5	Trad
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
budovou	budova	k1gFnSc7	budova
světa	svět	k1gInSc2	svět
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
jej	on	k3xPp3gMnSc4	on
předčil	předčit	k5eAaBmAgInS	předčit
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
Petronas	Petronas	k1gMnSc1	Petronas
Twin	Twina	k1gFnPc2	Twina
Towers	Towersa	k1gFnPc2	Towersa
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
architektem	architekt	k1gMnSc7	architekt
stavby	stavba	k1gFnSc2	stavba
byl	být	k5eAaImAgMnS	být
Bruce	Bruce	k1gMnSc1	Bruce
Graham	Graham	k1gMnSc1	Graham
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
má	mít	k5eAaImIp3nS	mít
108	[number]	k4	108
poschodí	poschodí	k1gNnPc2	poschodí
<g/>
,	,	kIx,	,
108	[number]	k4	108
fungujících	fungující	k2eAgInPc2d1	fungující
výtahů	výtah	k1gInPc2	výtah
a	a	k8xC	a
vlastní	vlastní	k2eAgNnSc4d1	vlastní
směrovací	směrovací	k2eAgNnSc4d1	směrovací
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
50	[number]	k4	50
000	[number]	k4	000
ji	on	k3xPp3gFnSc4	on
denně	denně	k6eAd1	denně
navštíví	navštívit	k5eAaPmIp3nS	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Willis	Willis	k1gFnSc1	Willis
Tower	Towra	k1gFnPc2	Towra
váží	vážit	k5eAaImIp3nS	vážit
celkem	celkem	k6eAd1	celkem
225	[number]	k4	225
500	[number]	k4	500
tun	tuna	k1gFnPc2	tuna
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc4d1	celý
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
sklem	sklo	k1gNnSc7	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vyhlídkovém	vyhlídkový	k2eAgMnSc6d1	vyhlídkový
103	[number]	k4	103
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
(	(	kIx(	(
<g/>
Skydeck	Skydeck	k1gMnSc1	Skydeck
<g/>
)	)	kIx)	)
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
412	[number]	k4	412
metrů	metr	k1gInPc2	metr
byly	být	k5eAaImAgFnP	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
tři	tři	k4xCgFnPc1	tři
prosklené	prosklený	k2eAgFnPc1d1	prosklená
kukaně	kukaň	k1gFnPc1	kukaň
(	(	kIx(	(
<g/>
Skyledge	Skyledg	k1gFnPc1	Skyledg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vysunuté	vysunutý	k2eAgNnSc1d1	vysunuté
zhruba	zhruba	k6eAd1	zhruba
1,3	[number]	k4	1,3
metru	metr	k1gInSc2	metr
ven	ven	k6eAd1	ven
z	z	k7c2	z
fasády	fasáda	k1gFnSc2	fasáda
a	a	k8xC	a
poskytující	poskytující	k2eAgInSc4d1	poskytující
nevšední	všední	k2eNgInSc4d1	nevšední
zážitek	zážitek	k1gInSc4	zážitek
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
budov	budova	k1gFnPc2	budova
USA	USA	kA	USA
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Willis	Willis	k1gFnSc2	Willis
Tower	Tower	k1gInSc1	Tower
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Zpráva	zpráva	k1gFnSc1	zpráva
Chicagská	chicagský	k2eAgFnSc1d1	Chicagská
Sears	Searsa	k1gFnPc2	Searsa
Tower	Towra	k1gFnPc2	Towra
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Willis	Willis	k1gFnSc1	Willis
Tower	Towra	k1gFnPc2	Towra
</s>
