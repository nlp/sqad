<p>
<s>
Severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Northwestern	Northwestern	k1gNnSc1	Northwestern
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
NU	nu	k9	nu
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
soukromá	soukromý	k2eAgFnSc1d1	soukromá
univerzita	univerzita	k1gFnSc1	univerzita
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
Evanstonu	Evanston	k1gInSc6	Evanston
v	v	k7c6	v
Illinois	Illinois	k1gFnSc6	Illinois
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Založena	založen	k2eAgFnSc1d1	založena
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
<g/>
,	,	kIx,	,
výuka	výuka	k1gFnSc1	výuka
zde	zde	k6eAd1	zde
začala	začít	k5eAaPmAgFnS	začít
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
přijímány	přijímat	k5eAaImNgInP	přijímat
až	až	k9	až
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gNnSc7	její
univerzitním	univerzitní	k2eAgNnSc7d1	univerzitní
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
je	být	k5eAaImIp3nS	být
Northwestern	Northwestern	k1gNnSc1	Northwestern
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
studuje	studovat	k5eAaImIp3nS	studovat
16	[number]	k4	16
267	[number]	k4	267
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgInPc4	tři
kampusy	kampus	k1gInPc4	kampus
a	a	k8xC	a
mezi	mezi	k7c4	mezi
její	její	k3xOp3gMnPc4	její
slavné	slavný	k2eAgMnPc4d1	slavný
absolventy	absolvent	k1gMnPc4	absolvent
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Rahm	Rahm	k1gMnSc1	Rahm
Emanuel	Emanuel	k1gMnSc1	Emanuel
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Barratt	Barratt	k1gMnSc1	Barratt
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Hanssen	Hanssen	k2eAgMnSc1d1	Hanssen
či	či	k8xC	či
David	David	k1gMnSc1	David
Schwimmer	Schwimmer	k1gMnSc1	Schwimmer
<g/>
.	.	kIx.	.
</s>
<s>
Univerzitní	univerzitní	k2eAgFnSc7d1	univerzitní
barvou	barva	k1gFnSc7	barva
je	být	k5eAaImIp3nS	být
fialová	fialový	k2eAgFnSc1d1	fialová
a	a	k8xC	a
neoficiálně	neoficiálně	k6eAd1	neoficiálně
bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
právních	právní	k2eAgFnPc2d1	právní
klinik	klinika	k1gFnPc2	klinika
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
založenou	založený	k2eAgFnSc7d1	založená
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Northwestern	Northwestern	k1gInSc1	Northwestern
University	universita	k1gFnSc2	universita
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Northwestern	Northwesterna	k1gFnPc2	Northwesterna
University	universita	k1gFnSc2	universita
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
internetové	internetový	k2eAgFnPc1d1	internetová
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
