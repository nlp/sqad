<s>
Slepotisk	slepotisk	k1gInSc1	slepotisk
čili	čili	k8xC	čili
embosování	embosování	k1gNnSc1	embosování
je	být	k5eAaImIp3nS	být
klasická	klasický	k2eAgFnSc1d1	klasická
technika	technika	k1gFnSc1	technika
jednostranného	jednostranný	k2eAgNnSc2d1	jednostranné
reliéfního	reliéfní	k2eAgNnSc2d1	reliéfní
zdobení	zdobení	k1gNnSc2	zdobení
kožených	kožený	k2eAgInPc2d1	kožený
předmětů	předmět	k1gInPc2	předmět
<g/>
:	:	kIx,	:
knižních	knižní	k2eAgFnPc2d1	knižní
vazeb	vazba	k1gFnPc2	vazba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k6eAd1	také
brašnářských	brašnářský	k2eAgInPc2d1	brašnářský
a	a	k8xC	a
sedlářských	sedlářský	k2eAgInPc2d1	sedlářský
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
