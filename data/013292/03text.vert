<p>
<s>
Slepotisk	slepotisk	k1gInSc1	slepotisk
je	být	k5eAaImIp3nS	být
technika	technika	k1gFnSc1	technika
reliéfní	reliéfní	k2eAgFnSc2d1	reliéfní
ražby	ražba	k1gFnSc2	ražba
do	do	k7c2	do
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
papíru	papír	k1gInSc2	papír
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
</s>
<s>
Provádí	provádět	k5eAaImIp3nS	provádět
se	se	k3xPyFc4	se
tiskovou	tiskový	k2eAgFnSc7d1	tisková
formou	forma	k1gFnSc7	forma
bez	bez	k7c2	bez
barvy	barva	k1gFnSc2	barva
nebo	nebo	k8xC	nebo
razicí	razicí	k2eAgFnSc7d1	razicí
formou	forma	k1gFnSc7	forma
zpravidla	zpravidla	k6eAd1	zpravidla
do	do	k7c2	do
kvalitního	kvalitní	k2eAgInSc2d1	kvalitní
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
ručního	ruční	k2eAgInSc2d1	ruční
<g/>
)	)	kIx)	)
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
do	do	k7c2	do
kožených	kožený	k2eAgFnPc2d1	kožená
či	či	k8xC	či
jiných	jiný	k2eAgFnPc2d1	jiná
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kůže	kůže	k1gFnSc2	kůže
==	==	k?	==
</s>
</p>
<p>
<s>
Slepotisk	slepotisk	k1gInSc1	slepotisk
čili	čili	k8xC	čili
embosování	embosování	k1gNnSc1	embosování
je	být	k5eAaImIp3nS	být
klasická	klasický	k2eAgFnSc1d1	klasická
technika	technika	k1gFnSc1	technika
jednostranného	jednostranný	k2eAgNnSc2d1	jednostranné
reliéfního	reliéfní	k2eAgNnSc2d1	reliéfní
zdobení	zdobení	k1gNnSc2	zdobení
kožených	kožený	k2eAgInPc2d1	kožený
předmětů	předmět	k1gInPc2	předmět
<g/>
:	:	kIx,	:
knižních	knižní	k2eAgFnPc2d1	knižní
vazeb	vazba	k1gFnPc2	vazba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k6eAd1	také
brašnářských	brašnářský	k2eAgInPc2d1	brašnářský
a	a	k8xC	a
sedlářských	sedlářský	k2eAgInPc2d1	sedlářský
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgInS	používat
se	se	k3xPyFc4	se
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
středověku	středověk	k1gInSc6	středověk
a	a	k8xC	a
renesanci	renesance	k1gFnSc3	renesance
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
ožil	ožít	k5eAaPmAgInS	ožít
na	na	k7c6	na
luxusních	luxusní	k2eAgFnPc6d1	luxusní
vazbách	vazba	k1gFnPc6	vazba
knih	kniha	k1gFnPc2	kniha
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Aplikuje	aplikovat	k5eAaBmIp3nS	aplikovat
se	se	k3xPyFc4	se
ručně	ručně	k6eAd1	ručně
na	na	k7c4	na
navlhčenou	navlhčený	k2eAgFnSc4d1	navlhčená
kůži	kůže	k1gFnSc4	kůže
ocelovým	ocelový	k2eAgNnSc7d1	ocelové
nebo	nebo	k8xC	nebo
mosazným	mosazný	k2eAgNnSc7d1	mosazné
razidlem	razidlo	k1gNnSc7	razidlo
<g/>
,	,	kIx,	,
zahřátým	zahřátý	k2eAgNnSc7d1	zahřáté
na	na	k7c6	na
80	[number]	k4	80
<g/>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
koželužském	koželužský	k2eAgInSc6d1	koželužský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
například	například	k6eAd1	například
napodobuje	napodobovat	k5eAaImIp3nS	napodobovat
krokodýlí	krokodýlí	k2eAgFnSc1d1	krokodýlí
kůže	kůže	k1gFnSc1	kůže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Papír	papír	k1gInSc1	papír
==	==	k?	==
</s>
</p>
<p>
<s>
Slepý	slepý	k2eAgInSc1d1	slepý
nebo	nebo	k8xC	nebo
ražený	ražený	k2eAgInSc1d1	ražený
tisk	tisk	k1gInSc1	tisk
se	se	k3xPyFc4	se
užíval	užívat	k5eAaImAgInS	užívat
jako	jako	k9	jako
ozdoba	ozdoba	k1gFnSc1	ozdoba
dopisních	dopisní	k2eAgInPc2d1	dopisní
papírů	papír	k1gInPc2	papír
<g/>
,	,	kIx,	,
drobných	drobný	k2eAgFnPc2d1	drobná
tiskovin	tiskovina	k1gFnPc2	tiskovina
<g/>
,	,	kIx,	,
pohlednic	pohlednice	k1gFnPc2	pohlednice
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
také	také	k9	také
při	při	k7c6	při
tisku	tisk	k1gInSc6	tisk
bankovek	bankovka	k1gFnPc2	bankovka
a	a	k8xC	a
zejména	zejména	k9	zejména
poštovních	poštovní	k2eAgFnPc2d1	poštovní
známek	známka	k1gFnPc2	známka
<g/>
.	.	kIx.	.
</s>
<s>
Reliéf	reliéf	k1gInSc1	reliéf
se	se	k3xPyFc4	se
vytlačí	vytlačit	k5eAaPmIp3nS	vytlačit
mezi	mezi	k7c7	mezi
pozitivní	pozitivní	k2eAgFnSc7d1	pozitivní
a	a	k8xC	a
negativní	negativní	k2eAgFnSc7d1	negativní
formou	forma	k1gFnSc7	forma
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
jen	jen	k9	jen
ozdobný	ozdobný	k2eAgInSc4d1	ozdobný
prvek	prvek	k1gInSc4	prvek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pečetě	pečeť	k1gFnSc2	pečeť
<g/>
,	,	kIx,	,
loga	logo	k1gNnSc2	logo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
při	při	k7c6	při
tisku	tisk	k1gInSc6	tisk
různých	různý	k2eAgFnPc2d1	různá
cenin	cenina	k1gFnPc2	cenina
a	a	k8xC	a
úředních	úřední	k2eAgInPc2d1	úřední
dokumentů	dokument	k1gInPc2	dokument
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vysokoškolské	vysokoškolský	k2eAgInPc1d1	vysokoškolský
diplomy	diplom	k1gInPc1	diplom
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
ochranný	ochranný	k2eAgInSc4d1	ochranný
prvek	prvek	k1gInSc4	prvek
proti	proti	k7c3	proti
napodobeninám	napodobenina	k1gFnPc3	napodobenina
a	a	k8xC	a
padělkům	padělek	k1gInPc3	padělek
<g/>
.	.	kIx.	.
</s>
<s>
Razí	razit	k5eAaImIp3nS	razit
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
malých	malý	k2eAgInPc2d1	malý
ručních	ruční	k2eAgInPc2d1	ruční
pákových	pákový	k2eAgInPc2d1	pákový
lisů	lis	k1gInPc2	lis
(	(	kIx(	(
<g/>
na	na	k7c4	na
malé	malý	k2eAgFnPc4d1	malá
série	série	k1gFnPc4	série
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
strojem	stroj	k1gInSc7	stroj
v	v	k7c6	v
tiskařských	tiskařský	k2eAgInPc6d1	tiskařský
a	a	k8xC	a
ocelotiskařských	ocelotiskařský	k2eAgInPc6d1	ocelotiskařský
lisech	lis	k1gInPc6	lis
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
použití	použití	k1gNnSc2	použití
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
reliéfní	reliéfní	k2eAgFnPc4d1	reliéfní
tapety	tapeta	k1gFnPc4	tapeta
<g/>
,	,	kIx,	,
balicí	balicí	k2eAgInPc4d1	balicí
papíry	papír	k1gInPc4	papír
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Knihařství	knihařství	k1gNnSc1	knihařství
</s>
</p>
<p>
<s>
Knižní	knižní	k2eAgFnSc1d1	knižní
vazba	vazba	k1gFnSc1	vazba
</s>
</p>
