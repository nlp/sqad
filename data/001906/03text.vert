<s>
Krista	Krista	k1gFnSc1	Krista
(	(	kIx(	(
<g/>
crista	crista	k1gFnSc1	crista
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
záhyb	záhyb	k1gInSc4	záhyb
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
membrány	membrána	k1gFnSc2	membrána
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
<g/>
,	,	kIx,	,
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
elektronovým	elektronový	k2eAgInSc7d1	elektronový
mikroskopem	mikroskop	k1gInSc7	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zvýšit	zvýšit	k5eAaPmF	zvýšit
povrch	povrch	k1gInSc4	povrch
této	tento	k3xDgFnSc2	tento
membrány	membrána	k1gFnSc2	membrána
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
osazena	osadit	k5eAaPmNgFnS	osadit
enzymatickými	enzymatický	k2eAgInPc7d1	enzymatický
komplexy	komplex	k1gInPc7	komplex
účastnícími	účastnící	k2eAgMnPc7d1	účastnící
se	se	k3xPyFc4	se
v	v	k7c6	v
buněčném	buněčný	k2eAgNnSc6d1	buněčné
dýchání	dýchání	k1gNnSc6	dýchání
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
elektronovém	elektronový	k2eAgInSc6d1	elektronový
transportním	transportní	k2eAgInSc6d1	transportní
řetězci	řetězec	k1gInSc6	řetězec
označovaném	označovaný	k2eAgInSc6d1	označovaný
jako	jako	k8xS	jako
dýchací	dýchací	k2eAgInSc1d1	dýchací
řetězec	řetězec	k1gInSc1	řetězec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejmohutnější	mohutný	k2eAgInSc4d3	nejmohutnější
systém	systém	k1gInSc4	systém
krist	krist	k5eAaPmF	krist
se	se	k3xPyFc4	se
logicky	logicky	k6eAd1	logicky
tvoří	tvořit	k5eAaImIp3nS	tvořit
v	v	k7c6	v
metabolicky	metabolicky	k6eAd1	metabolicky
nejaktivnějších	aktivní	k2eAgFnPc6d3	nejaktivnější
buňkách	buňka	k1gFnPc6	buňka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
například	například	k6eAd1	například
buňky	buňka	k1gFnPc4	buňka
srdeční	srdeční	k2eAgFnPc4d1	srdeční
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
tři	tři	k4xCgInPc4	tři
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
krist	krist	k5eAaPmF	krist
<g/>
:	:	kIx,	:
lamelární	lamelární	k2eAgInPc4d1	lamelární
(	(	kIx(	(
<g/>
ploché	plochý	k2eAgInPc4d1	plochý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
diskovitých	diskovitý	k2eAgFnPc2d1	diskovitá
-	-	kIx~	-
typické	typický	k2eAgFnSc6d1	typická
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
skrytěnky	skrytěnka	k1gFnSc2	skrytěnka
(	(	kIx(	(
<g/>
Cryptophyta	Cryptophyta	k1gFnSc1	Cryptophyta
<g/>
)	)	kIx)	)
vezikulární	vezikulární	k2eAgFnSc1d1	vezikulární
tubulární	tubulární	k2eAgFnSc1d1	tubulární
-	-	kIx~	-
např.	např.	kA	např.
nálevníci	nálevník	k1gMnPc1	nálevník
(	(	kIx(	(
<g/>
Ciliophora	Ciliophora	k1gFnSc1	Ciliophora
<g/>
)	)	kIx)	)
</s>
