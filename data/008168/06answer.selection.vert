<s>
Bach	Bach	k1gMnSc1	Bach
standardní	standardní	k2eAgFnSc4d1	standardní
formu	forma	k1gFnSc4	forma
suity	suita	k1gFnSc2	suita
(	(	kIx(	(
<g/>
allemanda	allemanda	k1gFnSc1	allemanda
–	–	k?	–
courante	courant	k1gMnSc5	courant
–	–	k?	–
sarabanda	sarabanda	k1gFnSc1	sarabanda
–	–	k?	–
gigue	gigue	k1gInSc1	gigue
<g/>
)	)	kIx)	)
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	dílo	k1gNnSc6	dílo
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
o	o	k7c4	o
úvodní	úvodní	k2eAgFnSc4d1	úvodní
předehru	předehra	k1gFnSc4	předehra
(	(	kIx(	(
<g/>
preludium	preludium	k1gNnSc4	preludium
<g/>
)	)	kIx)	)
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
mezi	mezi	k7c4	mezi
sarabandu	sarabanda	k1gFnSc4	sarabanda
a	a	k8xC	a
gigue	gigue	k1gInSc4	gigue
vkládal	vkládat	k5eAaImAgMnS	vkládat
ještě	ještě	k9	ještě
takzvanou	takzvaný	k2eAgFnSc4d1	takzvaná
galanterii	galanterie	k1gFnSc4	galanterie
<g/>
,	,	kIx,	,
taneční	taneční	k2eAgFnSc4d1	taneční
větu	věta	k1gFnSc4	věta
v	v	k7c6	v
uspořádání	uspořádání	k1gNnSc6	uspořádání
A	a	k8xC	a
B	B	kA	B
A.	A.	kA	A.
</s>
