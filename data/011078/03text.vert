<p>
<s>
Destruction	Destruction	k1gInSc1	Destruction
je	být	k5eAaImIp3nS	být
německá	německý	k2eAgFnSc1d1	německá
thrash	thrash	k1gInSc1	thrash
metalová	metalový	k2eAgFnSc1d1	metalová
kapela	kapela	k1gFnSc1	kapela
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
jméno	jméno	k1gNnSc1	jméno
kapely	kapela	k1gFnSc2	kapela
bylo	být	k5eAaImAgNnS	být
Knight	Knight	k1gInSc4	Knight
Of	Of	k1gFnSc2	Of
Demon	Demon	k1gNnSc4	Demon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
domnívala	domnívat	k5eAaImAgFnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dostatečně	dostatečně	k6eAd1	dostatečně
nekoresponduje	korespondovat	k5eNaImIp3nS	korespondovat
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
změněno	změnit	k5eAaPmNgNnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
neměli	mít	k5eNaImAgMnP	mít
Destruction	Destruction	k1gInSc1	Destruction
podepsanou	podepsaný	k2eAgFnSc4d1	podepsaná
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
nahrávací	nahrávací	k2eAgFnSc7d1	nahrávací
společností	společnost	k1gFnSc7	společnost
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
tak	tak	k8xC	tak
nuceni	nucen	k2eAgMnPc1d1	nucen
si	se	k3xPyFc3	se
produkovat	produkovat	k5eAaImF	produkovat
svá	svůj	k3xOyFgNnPc4	svůj
alba	album	k1gNnPc4	album
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
kapelami	kapela	k1gFnPc7	kapela
Kreator	Kreator	k1gInSc1	Kreator
<g/>
,	,	kIx,	,
Sodom	Sodoma	k1gFnPc2	Sodoma
a	a	k8xC	a
Tankard	Tankarda	k1gFnPc2	Tankarda
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
"	"	kIx"	"
<g/>
velkou	velký	k2eAgFnSc4d1	velká
čtyřku	čtyřka	k1gFnSc4	čtyřka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
big	big	k?	big
teutonic	teutonice	k1gFnPc2	teutonice
four	four	k1gMnSc1	four
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
německého	německý	k2eAgMnSc2d1	německý
thrash	thrash	k1gMnSc1	thrash
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgFnPc3	tento
kapelám	kapela	k1gFnPc3	kapela
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
přisuzován	přisuzován	k2eAgInSc1d1	přisuzován
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
rané	raný	k2eAgInPc4d1	raný
death	death	k1gInSc4	death
metalové	metalový	k2eAgFnSc2d1	metalová
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gFnSc1	jejich
hudba	hudba	k1gFnSc1	hudba
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
některé	některý	k3yIgInPc4	některý
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
v	v	k7c4	v
death	death	k1gInSc4	death
metal	metat	k5eAaImAgMnS	metat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Osmdesátá	osmdesátý	k4xOgNnPc4	osmdesátý
léta	léto	k1gNnPc4	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Kapela	kapela	k1gFnSc1	kapela
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Knight	Knight	k1gMnSc1	Knight
Of	Of	k1gMnSc1	Of
Demon	Demon	k1gMnSc1	Demon
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc4d1	původní
sestavu	sestava	k1gFnSc4	sestava
tvořili	tvořit	k5eAaImAgMnP	tvořit
Schmier	Schmier	k1gMnSc1	Schmier
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
a	a	k8xC	a
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
'	'	kIx"	'
<g/>
Tommy	Tomm	k1gInPc1	Tomm
<g/>
'	'	kIx"	'
Sandmann	Sandmann	k1gNnSc1	Sandmann
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
'	'	kIx"	'
<g/>
Mike	Mike	k1gInSc1	Mike
<g/>
'	'	kIx"	'
Sifringer	Sifringer	k1gInSc1	Sifringer
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
změnili	změnit	k5eAaPmAgMnP	změnit
název	název	k1gInSc4	název
na	na	k7c4	na
Destruction	Destruction	k1gInSc4	Destruction
a	a	k8xC	a
vydali	vydat	k5eAaPmAgMnP	vydat
první	první	k4xOgFnSc4	první
demo	demo	k2eAgFnSc4d1	demo
nahrávku	nahrávka	k1gFnSc4	nahrávka
s	s	k7c7	s
názvem	název	k1gInSc7	název
Bestial	Bestial	k1gInSc1	Bestial
Invasion	Invasion	k1gInSc1	Invasion
of	of	k?	of
Hell	Hell	k1gInSc1	Hell
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
demonahrávky	demonahrávka	k1gFnSc2	demonahrávka
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
nahrávací	nahrávací	k2eAgFnSc7d1	nahrávací
společností	společnost	k1gFnSc7	společnost
Steamhammer	Steamhammra	k1gFnPc2	Steamhammra
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
později	pozdě	k6eAd2	pozdě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vydala	vydat	k5eAaPmAgFnS	vydat
debutové	debutový	k2eAgFnSc3d1	debutová
EP	EP	kA	EP
Sentence	sentence	k1gFnPc1	sentence
Of	Of	k1gMnSc1	Of
Death	Death	k1gMnSc1	Death
<g/>
.	.	kIx.	.
</s>
<s>
Destruction	Destruction	k1gInSc4	Destruction
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
první	první	k4xOgInSc4	první
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
na	na	k7c6	na
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
metalové	metalový	k2eAgFnSc6d1	metalová
scéně	scéna	k1gFnSc6	scéna
a	a	k8xC	a
vyráží	vyrážet	k5eAaImIp3nS	vyrážet
na	na	k7c4	na
evropské	evropský	k2eAgInPc4d1	evropský
tour	tour	k1gInSc4	tour
se	se	k3xPyFc4	se
Slayer	Slayra	k1gFnPc2	Slayra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
Destruction	Destruction	k1gInSc1	Destruction
vydali	vydat	k5eAaPmAgMnP	vydat
debutové	debutový	k2eAgNnSc4d1	debutové
dlouhohrající	dlouhohrající	k2eAgNnSc4d1	dlouhohrající
album	album	k1gNnSc4	album
Infernal	Infernal	k1gFnSc2	Infernal
Overkill	Overkilla	k1gFnPc2	Overkilla
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nahrávka	nahrávka	k1gFnSc1	nahrávka
Eternal	Eternal	k1gMnSc1	Eternal
Devastation	Devastation	k1gInSc1	Devastation
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
tak	tak	k6eAd1	tak
ustálila	ustálit	k5eAaPmAgFnS	ustálit
svojí	svojit	k5eAaImIp3nP	svojit
pozici	pozice	k1gFnSc4	pozice
na	na	k7c6	na
předních	přední	k2eAgNnPc6d1	přední
místech	místo	k1gNnPc6	místo
evropské	evropský	k2eAgFnSc2d1	Evropská
thrashové	thrashový	k2eAgFnSc2d1	thrashová
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
úspěšné	úspěšný	k2eAgInPc4d1	úspěšný
tour	tour	k1gInSc4	tour
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
s	s	k7c7	s
kapelami	kapela	k1gFnPc7	kapela
Kreator	Kreator	k1gInSc4	Kreator
a	a	k8xC	a
Rage	Rage	k1gInSc4	Rage
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bubeník	Bubeník	k1gMnSc1	Bubeník
Tommy	Tomma	k1gFnSc2	Tomma
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
usedl	usednout	k5eAaPmAgMnS	usednout
Oliver	Oliver	k1gInSc4	Oliver
'	'	kIx"	'
<g/>
Olly	Olla	k1gMnSc2	Olla
<g/>
'	'	kIx"	'
Kaiser	Kaiser	k1gMnSc1	Kaiser
a	a	k8xC	a
ke	k	k7c3	k
kapele	kapela	k1gFnSc3	kapela
se	se	k3xPyFc4	se
také	také	k6eAd1	také
přidal	přidat	k5eAaPmAgMnS	přidat
druhý	druhý	k4xOgMnSc1	druhý
kytarista	kytarista	k1gMnSc1	kytarista
Harry	Harra	k1gFnSc2	Harra
Wilkens	Wilkens	k1gInSc1	Wilkens
<g/>
.	.	kIx.	.
</s>
<s>
Posíleni	posílen	k2eAgMnPc1d1	posílen
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
kytaru	kytara	k1gFnSc4	kytara
nahráli	nahrát	k5eAaBmAgMnP	nahrát
Destruction	Destruction	k1gInSc4	Destruction
druhé	druhý	k4xOgFnSc3	druhý
EP	EP	kA	EP
Mad	Mad	k1gFnPc2	Mad
Butcher	Butchra	k1gFnPc2	Butchra
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
a	a	k8xC	a
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
své	své	k1gNnSc4	své
první	první	k4xOgInSc1	první
tour	tour	k1gInSc1	tour
po	po	k7c6	po
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
vyšla	vyjít	k5eAaPmAgFnS	vyjít
deska	deska	k1gFnSc1	deska
Release	Releasa	k1gFnSc3	Releasa
from	from	k6eAd1	from
Agony	agon	k1gInPc4	agon
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
nově	nově	k6eAd1	nově
příchozími	příchozí	k1gMnPc7	příchozí
členy	člen	k1gMnPc4	člen
a	a	k8xC	a
Destruction	Destruction	k1gInSc4	Destruction
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
prezentovali	prezentovat	k5eAaBmAgMnP	prezentovat
techničtější	technický	k2eAgNnSc4d2	techničtější
pojetí	pojetí	k1gNnSc4	pojetí
thrash	thrasha	k1gFnPc2	thrasha
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
na	na	k7c4	na
tour	tour	k1gInSc4	tour
s	s	k7c7	s
Motörhead	Motörhead	k1gInSc4	Motörhead
a	a	k8xC	a
představila	představit	k5eAaPmAgFnS	představit
se	se	k3xPyFc4	se
na	na	k7c6	na
festivalových	festivalový	k2eAgNnPc6d1	festivalové
pódiích	pódium	k1gNnPc6	pódium
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
také	také	k9	také
záznam	záznam	k1gInSc1	záznam
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
tour	tour	k1gInSc1	tour
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Live	Live	k1gNnPc2	Live
Without	Without	k1gMnSc1	Without
Sense	Sense	k1gFnSc2	Sense
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
nahrávání	nahrávání	k1gNnSc2	nahrávání
další	další	k2eAgFnSc2d1	další
desky	deska	k1gFnSc2	deska
s	s	k7c7	s
názvem	název	k1gInSc7	název
Cracked	Cracked	k1gMnSc1	Cracked
Brain	Brain	k1gMnSc1	Brain
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
tvůrčím	tvůrčí	k2eAgInSc7d1	tvůrčí
neshodám	neshoda	k1gFnPc3	neshoda
uvnitř	uvnitř	k7c2	uvnitř
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
byl	být	k5eAaImAgInS	být
vyhozen	vyhozen	k2eAgInSc1d1	vyhozen
Schmier	Schmier	k1gInSc1	Schmier
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
založil	založit	k5eAaPmAgMnS	založit
power	power	k1gMnSc1	power
metalovou	metalový	k2eAgFnSc4d1	metalová
kapelu	kapela	k1gFnSc4	kapela
Headhunter	Headhuntra	k1gFnPc2	Headhuntra
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Cracked	Cracked	k1gMnSc1	Cracked
Brain	Brain	k1gMnSc1	Brain
nakonec	nakonec	k6eAd1	nakonec
nazpíval	nazpívat	k5eAaPmAgMnS	nazpívat
host	host	k1gMnSc1	host
André	André	k1gMnSc1	André
Grieder	Grieder	k1gMnSc1	Grieder
ze	z	k7c2	z
švýcarských	švýcarský	k2eAgMnPc2d1	švýcarský
Poltergeist	Poltergeist	k1gInSc4	Poltergeist
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kapely	kapela	k1gFnSc2	kapela
také	také	k9	také
přišel	přijít	k5eAaPmAgMnS	přijít
baskytarista	baskytarista	k1gMnSc1	baskytarista
Christian	Christian	k1gMnSc1	Christian
Engler	Engler	k1gMnSc1	Engler
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
působil	působit	k5eAaImAgMnS	působit
v	v	k7c4	v
německých	německý	k2eAgInPc2d1	německý
Necronomicon	Necronomicon	k1gInSc4	Necronomicon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Devadesátá	devadesátý	k4xOgNnPc4	devadesátý
léta	léto	k1gNnPc4	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
odešel	odejít	k5eAaPmAgMnS	odejít
kytarista	kytarista	k1gMnSc1	kytarista
Harry	Harra	k1gFnSc2	Harra
<g/>
.	.	kIx.	.
</s>
<s>
Mike	Mike	k6eAd1	Mike
a	a	k8xC	a
Olly	Olla	k1gFnSc2	Olla
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pokračovat	pokračovat	k5eAaImF	pokračovat
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
dále	daleko	k6eAd2	daleko
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Destruction	Destruction	k1gInSc1	Destruction
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
potřeba	potřeba	k6eAd1	potřeba
vyřešit	vyřešit	k5eAaPmF	vyřešit
problém	problém	k1gInSc4	problém
s	s	k7c7	s
postem	post	k1gInSc7	post
zpěváka	zpěvák	k1gMnSc2	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávací	nahrávací	k2eAgFnSc1d1	nahrávací
společnost	společnost	k1gFnSc1	společnost
přišla	přijít	k5eAaPmAgFnS	přijít
s	s	k7c7	s
návrhem	návrh	k1gInSc7	návrh
<g/>
,	,	kIx,	,
že	že	k8xS	že
Robert	Robert	k1gMnSc1	Robert
Gonella	Gonella	k1gMnSc1	Gonella
z	z	k7c2	z
německých	německý	k2eAgFnPc2d1	německá
Assassin	Assassina	k1gFnPc2	Assassina
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
ten	ten	k3xDgMnSc1	ten
správný	správný	k2eAgMnSc1d1	správný
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
ovšem	ovšem	k9	ovšem
nebyla	být	k5eNaImAgFnS	být
spokojena	spokojen	k2eAgFnSc1d1	spokojena
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
výkonem	výkon	k1gInSc7	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
oslovili	oslovit	k5eAaPmAgMnP	oslovit
zpěváka	zpěvák	k1gMnSc4	zpěvák
dánských	dánský	k2eAgInPc2d1	dánský
Artillery	Artiller	k1gInPc7	Artiller
<g/>
,	,	kIx,	,
Flemminga	Flemming	k1gMnSc2	Flemming
Rönsdorfa	Rönsdorf	k1gMnSc2	Rönsdorf
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
i	i	k9	i
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
nepřidal	přidat	k5eNaPmAgMnS	přidat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
nahrávací	nahrávací	k2eAgFnSc1d1	nahrávací
společnost	společnost	k1gFnSc1	společnost
nechtěla	chtít	k5eNaImAgFnS	chtít
poslat	poslat	k5eAaPmF	poslat
peníze	peníz	k1gInPc4	peníz
před	před	k7c7	před
nadcházejícím	nadcházející	k2eAgInSc7d1	nadcházející
tour	tour	k1gInSc4	tour
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
mikrofon	mikrofon	k1gInSc4	mikrofon
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
postavil	postavit	k5eAaPmAgMnS	postavit
zpěvák	zpěvák	k1gMnSc1	zpěvák
Thomas	Thomas	k1gMnSc1	Thomas
Rosenmerkel	Rosenmerkel	k1gMnSc1	Rosenmerkel
a	a	k8xC	a
na	na	k7c4	na
post	post	k1gInSc4	post
druhého	druhý	k4xOgMnSc2	druhý
kytaristy	kytarista	k1gMnSc2	kytarista
přišel	přijít	k5eAaPmAgMnS	přijít
Michael	Michael	k1gMnSc1	Michael
'	'	kIx"	'
<g/>
Ano	ano	k9	ano
<g/>
'	'	kIx"	'
Piranio	Piranio	k1gNnSc1	Piranio
(	(	kIx(	(
<g/>
oba	dva	k4xCgInPc1	dva
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Ephemera	Ephemero	k1gNnSc2	Ephemero
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Party	party	k1gFnSc7	party
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kapela	kapela	k1gFnSc1	kapela
během	během	k7c2	během
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
prošla	projít	k5eAaPmAgFnS	projít
komerčně	komerčně	k6eAd1	komerčně
neúspěšným	úspěšný	k2eNgNnSc7d1	neúspěšné
obdobím	období	k1gNnSc7	období
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
exploze	exploze	k1gFnSc2	exploze
žánru	žánr	k1gInSc2	žánr
grunge	grungat	k5eAaPmIp3nS	grungat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
kapela	kapela	k1gFnSc1	kapela
ztratila	ztratit	k5eAaPmAgFnS	ztratit
podporu	podpora	k1gFnSc4	podpora
své	svůj	k3xOyFgFnSc2	svůj
nahrávací	nahrávací	k2eAgFnSc2d1	nahrávací
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
nucená	nucený	k2eAgFnSc1d1	nucená
vydávat	vydávat	k5eAaPmF	vydávat
a	a	k8xC	a
produkovat	produkovat	k5eAaImF	produkovat
alba	album	k1gNnPc4	album
na	na	k7c4	na
vlastní	vlastní	k2eAgInPc4d1	vlastní
náklady	náklad	k1gInPc4	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
dvě	dva	k4xCgFnPc4	dva
EP	EP	kA	EP
<g/>
,	,	kIx,	,
Destruction	Destruction	k1gInSc1	Destruction
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
a	a	k8xC	a
Them	Them	k1gMnSc1	Them
Not	nota	k1gFnPc2	nota
Me	Me	k1gMnSc1	Me
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
dlouhohrající	dlouhohrající	k2eAgFnSc1d1	dlouhohrající
deska	deska	k1gFnSc1	deska
The	The	k1gMnSc1	The
Least	Least	k1gMnSc1	Least
Successful	Successfula	k1gFnPc2	Successfula
Human	Human	k1gMnSc1	Human
Cannonball	Cannonball	k1gMnSc1	Cannonball
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
éra	éra	k1gFnSc1	éra
kapelou	kapela	k1gFnSc7	kapela
označena	označit	k5eAaPmNgFnS	označit
jako	jako	k9	jako
Neo-Destruction	Neo-Destruction	k1gInSc1	Neo-Destruction
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
Destruction	Destruction	k1gInSc4	Destruction
bez	bez	k7c2	bez
Schmiera	Schmiero	k1gNnSc2	Schmiero
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávky	nahrávka	k1gFnPc1	nahrávka
vydané	vydaný	k2eAgFnPc1d1	vydaná
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
nejsou	být	k5eNaImIp3nP	být
kapelou	kapela	k1gFnSc7	kapela
počítány	počítán	k2eAgInPc4d1	počítán
do	do	k7c2	do
jejich	jejich	k3xOp3gFnSc2	jejich
oficiální	oficiální	k2eAgFnSc2d1	oficiální
diskografie	diskografie	k1gFnSc2	diskografie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
nátlaku	nátlak	k1gInSc3	nátlak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
fanoušků	fanoušek	k1gMnPc2	fanoušek
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
reunion	reunion	k1gInSc4	reunion
a	a	k8xC	a
Schmier	Schmier	k1gInSc4	Schmier
se	se	k3xPyFc4	se
po	po	k7c6	po
několika	několik	k4yIc6	několik
letém	letý	k2eAgNnSc6d1	leté
přemlouvání	přemlouvání	k1gNnSc6	přemlouvání
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
Destruction	Destruction	k1gInSc1	Destruction
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
opět	opět	k6eAd1	opět
jako	jako	k8xC	jako
trio	trio	k1gNnSc1	trio
(	(	kIx(	(
<g/>
Schmier	Schmier	k1gInSc1	Schmier
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
a	a	k8xC	a
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
Mike	Mike	k1gInSc1	Mike
-	-	kIx~	-
kytary	kytara	k1gFnPc1	kytara
a	a	k8xC	a
nový	nový	k2eAgMnSc1d1	nový
bubeník	bubeník	k1gMnSc1	bubeník
Sven	Sven	k1gMnSc1	Sven
Vormann	Vormann	k1gMnSc1	Vormann
<g/>
)	)	kIx)	)
podepsala	podepsat	k5eAaPmAgFnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
nahrávací	nahrávací	k2eAgFnSc7d1	nahrávací
společností	společnost	k1gFnSc7	společnost
Nuclear	Nuclear	k1gMnSc1	Nuclear
Blast	Blast	k1gMnSc1	Blast
Records	Records	k1gInSc1	Records
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
nastartovala	nastartovat	k5eAaPmAgFnS	nastartovat
svůj	svůj	k3xOyFgInSc4	svůj
návrat	návrat	k1gInSc4	návrat
na	na	k7c6	na
největších	veliký	k2eAgInPc6d3	veliký
německých	německý	k2eAgInPc6d1	německý
festivalech	festival	k1gInPc6	festival
<g/>
:	:	kIx,	:
Bang	Bang	k1gMnSc1	Bang
Your	Your	k1gMnSc1	Your
Head	Head	k1gMnSc1	Head
<g/>
,	,	kIx,	,
With	With	k1gMnSc1	With
Full	Full	k1gMnSc1	Full
Force	force	k1gFnSc1	force
a	a	k8xC	a
Wacken	Wacken	k2eAgMnSc1d1	Wacken
Open	Open	k1gMnSc1	Open
Air	Air	k1gMnSc1	Air
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nultá	nultý	k4xOgNnPc4	nultý
léta	léto	k1gNnPc4	léto
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
návratové	návratový	k2eAgNnSc1d1	návratové
album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
s	s	k7c7	s
názvem	název	k1gInSc7	název
All	All	k1gMnSc1	All
Hell	Hell	k1gMnSc1	Hell
Breaks	Breaks	k1gInSc4	Breaks
Loose	Loose	k1gFnSc2	Loose
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
následník	následník	k1gMnSc1	následník
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
The	The	k1gMnSc1	The
Antichrist	Antichrist	k1gMnSc1	Antichrist
byl	být	k5eAaImAgMnS	být
vydán	vydán	k2eAgInSc1d1	vydán
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
deska	deska	k1gFnSc1	deska
je	být	k5eAaImIp3nS	být
mnohými	mnohý	k2eAgFnPc7d1	mnohá
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
moderní	moderní	k2eAgInSc4d1	moderní
thrash	thrash	k1gInSc4	thrash
metalovou	metalový	k2eAgFnSc4d1	metalová
klasiku	klasika	k1gFnSc4	klasika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
alba	album	k1gNnSc2	album
bylo	být	k5eAaImAgNnS	být
naplánováno	naplánovat	k5eAaBmNgNnS	naplánovat
<g/>
,	,	kIx,	,
na	na	k7c4	na
konec	konec	k1gInSc4	konec
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
velké	velký	k2eAgInPc4d1	velký
tour	tour	k1gInSc4	tour
po	po	k7c6	po
boku	bok	k1gInSc6	bok
kapel	kapela	k1gFnPc2	kapela
Kreator	Kreator	k1gMnSc1	Kreator
a	a	k8xC	a
Sodom	Sodoma	k1gFnPc2	Sodoma
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
odešel	odejít	k5eAaPmAgMnS	odejít
bubeník	bubeník	k1gMnSc1	bubeník
Sven	Sven	k1gMnSc1	Sven
kvůli	kvůli	k7c3	kvůli
náročnosti	náročnost	k1gFnSc3	náročnost
turné	turné	k1gNnSc2	turné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
kapely	kapela	k1gFnSc2	kapela
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
Schmierových	Schmierův	k2eAgMnPc2d1	Schmierův
přátel	přítel	k1gMnPc2	přítel
nový	nový	k2eAgMnSc1d1	nový
bubeník	bubeník	k1gMnSc1	bubeník
Marc	Marc	k1gFnSc4	Marc
Reign	Reigna	k1gFnPc2	Reigna
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
již	již	k9	již
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
bubeníkem	bubeník	k1gMnSc7	bubeník
vydala	vydat	k5eAaPmAgFnS	vydat
živou	živá	k1gFnSc4	živá
nahrávku	nahrávka	k1gFnSc4	nahrávka
Alive	Aliev	k1gFnSc2	Aliev
Devastation	Devastation	k1gInSc1	Devastation
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vyšla	vyjít	k5eAaPmAgFnS	vyjít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
vydána	vydán	k2eAgFnSc1d1	vydána
jako	jako	k8xS	jako
bonusové	bonusový	k2eAgNnSc1d1	bonusové
CD	CD	kA	CD
k	k	k7c3	k
DVD	DVD	kA	DVD
Live	Live	k1gInSc1	Live
Discharge	Discharge	k1gInSc1	Discharge
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
vyšla	vyjít	k5eAaPmAgFnS	vyjít
deska	deska	k1gFnSc1	deska
Metal	metal	k1gInSc1	metal
Discharge	Discharge	k1gFnSc1	Discharge
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
nesl	nést	k5eAaImAgMnS	nést
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
oslav	oslava	k1gFnPc2	oslava
dvaceti	dvacet	k4xCc2	dvacet
let	léto	k1gNnPc2	léto
existence	existence	k1gFnSc2	existence
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vydala	vydat	k5eAaPmAgFnS	vydat
DVD	DVD	kA	DVD
Live	Live	k1gFnSc1	Live
Discharge	Discharge	k1gFnSc1	Discharge
se	s	k7c7	s
záznamem	záznam	k1gInSc7	záznam
vystoupení	vystoupení	k1gNnSc2	vystoupení
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
festivalu	festival	k1gInSc2	festival
ve	v	k7c6	v
Wackenu	Wacken	k1gInSc6	Wacken
a	a	k8xC	a
s	s	k7c7	s
bonusovým	bonusový	k2eAgInSc7d1	bonusový
materiálem	materiál	k1gInSc7	materiál
z	z	k7c2	z
historie	historie	k1gFnSc2	historie
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
Destruction	Destruction	k1gInSc1	Destruction
přešli	přejít	k5eAaPmAgMnP	přejít
pod	pod	k7c4	pod
nahrávací	nahrávací	k2eAgFnSc4d1	nahrávací
společnost	společnost	k1gFnSc4	společnost
AFM	AFM	kA	AFM
Records	Records	k1gInSc4	Records
a	a	k8xC	a
nahráli	nahrát	k5eAaBmAgMnP	nahrát
album	album	k1gNnSc4	album
Invertor	Invertor	k1gMnSc1	Invertor
Of	Of	k1gMnSc1	Of
Evil	Evil	k1gMnSc1	Evil
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Alliance	Alliance	k1gFnSc2	Alliance
Of	Of	k1gMnSc1	Of
Hellhoundz	Hellhoundz	k1gMnSc1	Hellhoundz
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
jako	jako	k9	jako
hosté	host	k1gMnPc1	host
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
objevili	objevit	k5eAaPmAgMnP	objevit
přední	přední	k2eAgMnPc1d1	přední
metaloví	metalový	k2eAgMnPc1d1	metalový
zpěváci	zpěvák	k1gMnPc1	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
Destruction	Destruction	k1gInSc4	Destruction
znovunahráli	znovunahrál	k1gMnSc3	znovunahrál
vybrané	vybraný	k2eAgFnSc2d1	vybraná
skladby	skladba	k1gFnSc2	skladba
z	z	k7c2	z
předreunionových	předreunionový	k2eAgFnPc2d1	předreunionový
nahrávek	nahrávka	k1gFnPc2	nahrávka
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kompilace	kompilace	k1gFnSc1	kompilace
<g/>
,	,	kIx,	,
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Thrash	Thrash	k1gInSc4	Thrash
Anthems	Anthemsa	k1gFnPc2	Anthemsa
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
dvě	dva	k4xCgFnPc4	dva
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgFnPc4d1	nová
skladby	skladba	k1gFnPc4	skladba
a	a	k8xC	a
nahrávku	nahrávka	k1gFnSc4	nahrávka
"	"	kIx"	"
<g/>
Cracked	Cracked	k1gMnSc1	Cracked
Brain	Brain	k1gMnSc1	Brain
<g/>
"	"	kIx"	"
ze	z	k7c2	z
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
původně	původně	k6eAd1	původně
Schmier	Schmier	k1gMnSc1	Schmier
nenazpíval	nazpívat	k5eNaPmAgMnS	nazpívat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
desce	deska	k1gFnSc6	deska
se	se	k3xPyFc4	se
jako	jako	k9	jako
host	host	k1gMnSc1	host
objevil	objevit	k5eAaPmAgMnS	objevit
i	i	k9	i
bývalý	bývalý	k2eAgMnSc1d1	bývalý
kytarista	kytarista	k1gMnSc1	kytarista
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
Harry	Harra	k1gFnSc2	Harra
Wilkens	Wilkensa	k1gFnPc2	Wilkensa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2008	[number]	k4	2008
vyšla	vyjít	k5eAaPmAgFnS	vyjít
deska	deska	k1gFnSc1	deska
D.E.V.O.L.U.T.I.O.	D.E.V.O.L.U.T.I.O.	k1gFnSc2	D.E.V.O.L.U.T.I.O.
<g/>
N.	N.	kA	N.
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
navázalo	navázat	k5eAaPmAgNnS	navázat
světové	světový	k2eAgNnSc1d1	světové
turné	turné	k1gNnSc1	turné
<g/>
.	.	kIx.	.
</s>
<s>
Destruction	Destruction	k1gInSc4	Destruction
oslavují	oslavovat	k5eAaImIp3nP	oslavovat
25	[number]	k4	25
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vydali	vydat	k5eAaPmAgMnP	vydat
koncertní	koncertní	k2eAgInSc4d1	koncertní
záznam	záznam	k1gInSc4	záznam
The	The	k1gFnSc2	The
Curse	Curse	k1gFnSc2	Curse
of	of	k?	of
The	The	k1gMnSc1	The
Antichrist	Antichrist	k1gMnSc1	Antichrist
-	-	kIx~	-
Live	Live	k1gFnSc1	Live
In	In	k1gFnSc7	In
Agony	agon	k1gInPc1	agon
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávka	nahrávka	k1gFnSc1	nahrávka
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
vystoupení	vystoupení	k1gNnSc2	vystoupení
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
ve	v	k7c6	v
Wackenu	Wacken	k1gInSc6	Wacken
a	a	k8xC	a
koncertu	koncert	k1gInSc6	koncert
v	v	k7c6	v
Tokyu	Tokyus	k1gInSc6	Tokyus
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měla	mít	k5eAaImAgFnS	mít
obsahovat	obsahovat	k5eAaImF	obsahovat
ještě	ještě	k9	ještě
záznam	záznam	k1gInSc4	záznam
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
ztracen	ztratit	k5eAaPmNgMnS	ztratit
při	při	k7c6	při
poškození	poškození	k1gNnSc6	poškození
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ho	on	k3xPp3gInSc4	on
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2009	[number]	k4	2009
nemohl	moct	k5eNaImAgMnS	moct
Mike	Mike	k1gFnSc4	Mike
Sifringer	Sifringer	k1gMnSc1	Sifringer
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
zlomenému	zlomený	k2eAgInSc3d1	zlomený
prstu	prst	k1gInSc3	prst
na	na	k7c4	na
ruce	ruka	k1gFnPc4	ruka
<g/>
,	,	kIx,	,
vystoupit	vystoupit	k5eAaPmF	vystoupit
na	na	k7c6	na
portugalském	portugalský	k2eAgInSc6d1	portugalský
festivalu	festival	k1gInSc6	festival
Caos	Caosa	k1gFnPc2	Caosa
Emergente	Emergent	k1gInSc5	Emergent
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
koncertu	koncert	k1gInSc6	koncert
ho	on	k3xPp3gInSc4	on
zastoupil	zastoupit	k5eAaPmAgMnS	zastoupit
Oliver	Oliver	k1gMnSc1	Oliver
'	'	kIx"	'
<g/>
Ol	Ola	k1gFnPc2	Ola
<g/>
'	'	kIx"	'
Drake	Drake	k1gFnSc1	Drake
z	z	k7c2	z
britské	britský	k2eAgFnSc2d1	britská
kapely	kapela	k1gFnSc2	kapela
Evile	Evile	k1gFnSc2	Evile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Desátá	desátý	k4xOgNnPc4	desátý
léta	léto	k1gNnPc4	léto
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Oslavy	oslava	k1gFnPc1	oslava
25	[number]	k4	25
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc1	výročí
kapela	kapela	k1gFnSc1	kapela
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
vydáním	vydání	k1gNnSc7	vydání
DVD	DVD	kA	DVD
A	a	k9	a
Savage	Savage	k1gNnSc2	Savage
Symphony	Symphona	k1gFnSc2	Symphona
-	-	kIx~	-
The	The	k1gFnSc1	The
History	Histor	k1gInPc1	Histor
of	of	k?	of
Annihilation	Annihilation	k1gInSc1	Annihilation
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
se	s	k7c7	s
záznamem	záznam	k1gInSc7	záznam
vystoupení	vystoupení	k1gNnSc1	vystoupení
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
ve	v	k7c6	v
Wackenu	Wacken	k1gInSc6	Wacken
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vystoupení	vystoupení	k1gNnSc2	vystoupení
se	se	k3xPyFc4	se
jako	jako	k9	jako
hosté	host	k1gMnPc1	host
k	k	k7c3	k
samotné	samotný	k2eAgFnSc3d1	samotná
kapele	kapela	k1gFnSc3	kapela
připojili	připojit	k5eAaPmAgMnP	připojit
všichni	všechen	k3xTgMnPc1	všechen
bývalí	bývalý	k2eAgMnPc1d1	bývalý
členové	člen	k1gMnPc1	člen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2010	[number]	k4	2010
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
bubeník	bubeník	k1gMnSc1	bubeník
Marc	Marc	k1gFnSc1	Marc
Reign	Reigno	k1gNnPc2	Reigno
a	a	k8xC	a
DVD	DVD	kA	DVD
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
jeho	jeho	k3xOp3gNnSc7	jeho
rozloučením	rozloučení	k1gNnSc7	rozloučení
s	s	k7c7	s
fanoušky	fanoušek	k1gMnPc7	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
důvody	důvod	k1gInPc4	důvod
odchodu	odchod	k1gInSc2	odchod
uvedl	uvést	k5eAaPmAgMnS	uvést
personální	personální	k2eAgMnSc1d1	personální
a	a	k8xC	a
hudební	hudební	k2eAgFnPc1d1	hudební
neshody	neshoda	k1gFnPc1	neshoda
způsobené	způsobený	k2eAgFnPc1d1	způsobená
stresem	stres	k1gInSc7	stres
z	z	k7c2	z
turné	turné	k1gNnSc2	turné
a	a	k8xC	a
koncentraci	koncentrace	k1gFnSc4	koncentrace
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
rockovou	rockový	k2eAgFnSc4d1	rocková
kapelu	kapela	k1gFnSc4	kapela
Volcano	Volcana	k1gFnSc5	Volcana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
stáje	stáj	k1gFnSc2	stáj
Nuclear	Nuclear	k1gInSc1	Nuclear
Blast	Blast	k1gMnSc1	Blast
Records	Records	k1gInSc1	Records
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
oficiálně	oficiálně	k6eAd1	oficiálně
oznámila	oznámit	k5eAaPmAgFnS	oznámit
nového	nový	k2eAgMnSc4d1	nový
bubeníka	bubeník	k1gMnSc4	bubeník
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Wawrzyniec	Wawrzyniec	k1gMnSc1	Wawrzyniec
'	'	kIx"	'
<g/>
Vaaver	Vaaver	k1gMnSc1	Vaaver
<g/>
'	'	kIx"	'
Dramowicz	Dramowicz	k1gMnSc1	Dramowicz
z	z	k7c2	z
polské	polský	k2eAgFnSc2d1	polská
progresivní	progresivní	k2eAgFnSc2d1	progresivní
kapely	kapela	k1gFnSc2	kapela
Indukti	Indukť	k1gFnSc2	Indukť
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
kapela	kapela	k1gFnSc1	kapela
nachystala	nachystat	k5eAaBmAgFnS	nachystat
na	na	k7c4	na
konec	konec	k1gInSc4	konec
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
novou	nový	k2eAgFnSc4d1	nová
desku	deska	k1gFnSc4	deska
Day	Day	k1gFnSc2	Day
of	of	k?	of
Reckoning	Reckoning	k1gInSc1	Reckoning
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
oproti	oproti	k7c3	oproti
předchozí	předchozí	k2eAgFnSc3d1	předchozí
nahrávce	nahrávka	k1gFnSc3	nahrávka
návratem	návrat	k1gInSc7	návrat
k	k	k7c3	k
větší	veliký	k2eAgFnSc3d2	veliký
rychlosti	rychlost	k1gFnSc3	rychlost
a	a	k8xC	a
přímočarosti	přímočarost	k1gFnSc3	přímočarost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
březen	březen	k1gInSc4	březen
bylo	být	k5eAaImAgNnS	být
naplánované	naplánovaný	k2eAgNnSc1d1	naplánované
evropské	evropský	k2eAgNnSc1d1	Evropské
turné	turné	k1gNnSc1	turné
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Overkill	Overkilla	k1gFnPc2	Overkilla
a	a	k8xC	a
Heathen	Heathna	k1gFnPc2	Heathna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
a	a	k8xC	a
květnu	květen	k1gInSc6	květen
pak	pak	k6eAd1	pak
kapela	kapela	k1gFnSc1	kapela
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
koncertování	koncertování	k1gNnSc6	koncertování
po	po	k7c6	po
Jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
oficiální	oficiální	k2eAgFnSc6d1	oficiální
facebookové	facebookový	k2eAgFnSc6d1	facebooková
stránce	stránka	k1gFnSc6	stránka
kapely	kapela	k1gFnSc2	kapela
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
nahrávání	nahrávání	k1gNnSc1	nahrávání
nové	nový	k2eAgFnSc2d1	nová
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
odtajněn	odtajněn	k2eAgInSc1d1	odtajněn
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
artwork	artwork	k1gInSc1	artwork
a	a	k8xC	a
datum	datum	k1gNnSc1	datum
vydání	vydání	k1gNnSc2	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
album	album	k1gNnSc1	album
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Under	Under	k1gMnSc1	Under
Attack	Attack	k1gMnSc1	Attack
<g/>
"	"	kIx"	"
a	a	k8xC	a
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
slov	slovo	k1gNnPc2	slovo
Schmiera	Schmier	k1gMnSc2	Schmier
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
"	"	kIx"	"
<g/>
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejklasičtějších	klasický	k2eAgNnPc2d3	nejklasičtější
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgNnSc1	jaký
kdy	kdy	k6eAd1	kdy
Destruction	Destruction	k1gInSc4	Destruction
nahráli	nahrát	k5eAaPmAgMnP	nahrát
(	(	kIx(	(
<g/>
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
především	především	k9	především
rychlostí	rychlost	k1gFnSc7	rychlost
a	a	k8xC	a
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
thrashový	thrashový	k2eAgInSc4d1	thrashový
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
předchozí	předchozí	k2eAgFnSc1d1	předchozí
deska	deska	k1gFnSc1	deska
zněla	znět	k5eAaImAgFnS	znět
spíš	spíš	k9	spíš
heavy	heav	k1gMnPc4	heav
metalově	metalově	k6eAd1	metalově
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
desku	deska	k1gFnSc4	deska
nahrávala	nahrávat	k5eAaImAgFnS	nahrávat
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
koncertními	koncertní	k2eAgFnPc7d1	koncertní
šňůrami	šňůra	k1gFnPc7	šňůra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vydání	vydání	k1gNnSc6	vydání
"	"	kIx"	"
<g/>
Under	Under	k1gMnSc1	Under
Attack	Attack	k1gMnSc1	Attack
<g/>
"	"	kIx"	"
navázalo	navázat	k5eAaPmAgNnS	navázat
celosvětové	celosvětový	k2eAgNnSc1d1	celosvětové
"	"	kIx"	"
<g/>
Down	Down	k1gNnSc1	Down
Under	Undra	k1gFnPc2	Undra
Attack	Attacka	k1gFnPc2	Attacka
<g/>
"	"	kIx"	"
turné	turné	k1gNnSc1	turné
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
např.	např.	kA	např.
zastávku	zastávka	k1gFnSc4	zastávka
i	i	k8xC	i
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Brutal	Brutal	k1gMnSc1	Brutal
Assault	Assault	k1gMnSc1	Assault
v	v	k7c6	v
Josefově	Josefov	k1gInSc6	Josefov
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
s	s	k7c7	s
pauzami	pauza	k1gFnPc7	pauza
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2018	[number]	k4	2018
kapela	kapela	k1gFnSc1	kapela
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
několika	několik	k4yIc7	několik
koncerty	koncert	k1gInPc7	koncert
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
turné	turné	k1gNnSc2	turné
k	k	k7c3	k
novému	nový	k2eAgNnSc3d1	nové
albu	album	k1gNnSc3	album
kapela	kapela	k1gFnSc1	kapela
oznámila	oznámit	k5eAaPmAgFnS	oznámit
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
facebookový	facebookový	k2eAgInSc4d1	facebookový
profil	profil	k1gInSc4	profil
<g/>
)	)	kIx)	)
vydání	vydání	k1gNnSc4	vydání
další	další	k2eAgFnSc2d1	další
nahrávky	nahrávka	k1gFnSc2	nahrávka
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
však	však	k9	však
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
znovu	znovu	k6eAd1	znovu
nahrané	nahraný	k2eAgFnPc4d1	nahraná
klasiky	klasika	k1gFnPc4	klasika
<g/>
,	,	kIx,	,
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
sedmi	sedm	k4xCc2	sedm
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Kompilace	kompilace	k1gFnSc1	kompilace
navázala	navázat	k5eAaPmAgFnS	navázat
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
předchůdce	předchůdce	k1gMnSc4	předchůdce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
Thrash	Thrash	k1gInSc1	Thrash
Anthems	Anthems	k1gInSc1	Anthems
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyšla	vyjít	k5eAaPmAgFnS	vyjít
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2017	[number]	k4	2017
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Thrash	Thrash	k1gInSc1	Thrash
Anthems	Anthems	k1gInSc1	Anthems
II	II	kA	II
<g/>
"	"	kIx"	"
a	a	k8xC	a
o	o	k7c6	o
výběru	výběr	k1gInSc6	výběr
písní	píseň	k1gFnSc7	píseň
rozhodovali	rozhodovat	k5eAaImAgMnP	rozhodovat
sami	sám	k3xTgMnPc1	sám
fanoušci	fanoušek	k1gMnPc1	fanoušek
skrze	skrze	k?	skrze
hlasování	hlasování	k1gNnSc2	hlasování
v	v	k7c6	v
aplikaci	aplikace	k1gFnSc6	aplikace
Pledge	Pledg	k1gInSc2	Pledg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sestava	sestava	k1gFnSc1	sestava
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Současní	současný	k2eAgMnPc1d1	současný
členové	člen	k1gMnPc1	člen
===	===	k?	===
</s>
</p>
<p>
<s>
Schmier	Schmier	k1gInSc1	Schmier
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
</s>
</p>
<p>
<s>
Mike	Mike	k1gNnSc1	Mike
-	-	kIx~	-
kytary	kytara	k1gFnPc1	kytara
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
-současnost	oučasnost	k1gFnSc1	-současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wawrzyniec	Wawrzyniec	k1gMnSc1	Wawrzyniec
'	'	kIx"	'
<g/>
Vaaver	Vaaver	k1gMnSc1	Vaaver
<g/>
'	'	kIx"	'
Dramowicz	Dramowicz	k1gMnSc1	Dramowicz
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
-současnost	oučasnost	k1gFnSc1	-současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
členové	člen	k1gMnPc1	člen
===	===	k?	===
</s>
</p>
<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
'	'	kIx"	'
<g/>
Tommy	Tomm	k1gInPc1	Tomm
<g/>
'	'	kIx"	'
Sandmann	Sandmann	k1gInSc1	Sandmann
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
–	–	k?	–
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Harry	Harr	k1gInPc1	Harr
Wilkens	Wilkensa	k1gFnPc2	Wilkensa
-	-	kIx~	-
kytary	kytara	k1gFnPc1	kytara
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oliver	Oliver	k1gInSc1	Oliver
'	'	kIx"	'
<g/>
Olly	Olla	k1gFnSc2	Olla
<g/>
'	'	kIx"	'
Kaiser	Kaiser	k1gMnSc1	Kaiser
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Christian	Christian	k1gMnSc1	Christian
Engler	Engler	k1gMnSc1	Engler
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Rosenmerkel	Rosenmerkel	k1gMnSc1	Rosenmerkel
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Michael	Michael	k1gMnSc1	Michael
'	'	kIx"	'
<g/>
Ano	ano	k9	ano
<g/>
'	'	kIx"	'
Piranio	Piranio	k1gNnSc1	Piranio
-	-	kIx~	-
kytary	kytara	k1gFnPc1	kytara
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sven	Sven	k1gMnSc1	Sven
Vormann	Vormann	k1gMnSc1	Vormann
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Marc	Marc	k1gInSc1	Marc
Reign	Reign	k1gInSc1	Reign
-	-	kIx~	-
bicí	bicí	k2eAgInSc1d1	bicí
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
Hostující	hostující	k2eAgMnPc1d1	hostující
hudebníci	hudebník	k1gMnPc1	hudebník
jsou	být	k5eAaImIp3nP	být
uvedeni	uvést	k5eAaPmNgMnP	uvést
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
alb	alba	k1gFnPc2	alba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
–	–	k?	–
Infernal	Infernal	k1gMnSc1	Infernal
Overkill	Overkill	k1gMnSc1	Overkill
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
–	–	k?	–
Eternal	Eternal	k1gMnSc4	Eternal
Devastation	Devastation	k1gInSc1	Devastation
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
–	–	k?	–
Release	Releas	k1gInSc6	Releas
from	from	k1gInSc1	from
Agony	agon	k1gInPc1	agon
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
–	–	k?	–
Cracked	Cracked	k1gMnSc1	Cracked
Brain	Brain	k1gMnSc1	Brain
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
–	–	k?	–
All	All	k1gMnSc1	All
Hell	Hell	k1gMnSc1	Hell
Breaks	Breaks	k1gInSc4	Breaks
Loose	Loose	k1gFnSc2	Loose
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
The	The	k1gMnSc1	The
Antichrist	Antichrist	k1gMnSc1	Antichrist
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
Metal	metat	k5eAaImAgMnS	metat
Discharge	Discharge	k1gInSc4	Discharge
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
Inventor	Inventor	k1gMnSc1	Inventor
of	of	k?	of
Evil	Evil	k1gMnSc1	Evil
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
–	–	k?	–
Thrash	Thrash	k1gInSc1	Thrash
Anthems	Anthems	k1gInSc1	Anthems
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
D.E.V.O.L.U.T.I.O.	D.E.V.O.L.U.T.I.O.	k1gFnPc2	D.E.V.O.L.U.T.I.O.
<g/>
N.	N.	kA	N.
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
–	–	k?	–
Day	Day	k1gMnSc1	Day
of	of	k?	of
Reckoning	Reckoning	k1gInSc1	Reckoning
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
–	–	k?	–
Under	Under	k1gMnSc1	Under
Attack	Attack	k1gMnSc1	Attack
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
–	–	k?	–
Thrash	Thrash	k1gInSc1	Thrash
Anthems	Anthems	k1gInSc1	Anthems
II	II	kA	II
</s>
</p>
<p>
<s>
===	===	k?	===
Živá	živý	k2eAgNnPc4d1	živé
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
–	–	k?	–
Live	Liv	k1gMnSc4	Liv
Without	Without	k1gMnSc1	Without
Sense	Sens	k1gMnSc4	Sens
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
–	–	k?	–
Alive	Aliev	k1gFnSc2	Aliev
Devastation	Devastation	k1gInSc4	Devastation
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
–	–	k?	–
<g/>
-	-	kIx~	-
The	The	k1gMnSc1	The
Curse	Curse	k1gFnSc2	Curse
of	of	k?	of
The	The	k1gMnSc1	The
Antichrist	Antichrist	k1gMnSc1	Antichrist
-	-	kIx~	-
Live	Live	k1gFnSc1	Live
In	In	k1gFnSc7	In
Agony	agon	k1gInPc1	agon
</s>
</p>
<p>
<s>
===	===	k?	===
DVD	DVD	kA	DVD
===	===	k?	===
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
Live	Liv	k1gMnSc4	Liv
Discharge	Discharg	k1gMnSc4	Discharg
-	-	kIx~	-
20	[number]	k4	20
Years	Years	k1gInSc1	Years
Of	Of	k1gFnSc2	Of
Total	totat	k5eAaImAgInS	totat
Destruction	Destruction	k1gInSc1	Destruction
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
A	a	k9	a
Savage	Savage	k1gNnSc1	Savage
Symphony	Symphona	k1gFnSc2	Symphona
-	-	kIx~	-
The	The	k1gFnSc1	The
History	Histor	k1gInPc1	Histor
of	of	k?	of
Annihilation	Annihilation	k1gInSc1	Annihilation
</s>
</p>
<p>
<s>
===	===	k?	===
EP	EP	kA	EP
===	===	k?	===
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
Sentence	sentence	k1gFnSc2	sentence
of	of	k?	of
Death	Death	k1gMnSc1	Death
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
–	–	k?	–
Mad	Mad	k1gFnSc2	Mad
Butcher	Butchra	k1gFnPc2	Butchra
</s>
</p>
<p>
<s>
===	===	k?	===
Dema	Demum	k1gNnSc2	Demum
===	===	k?	===
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
Bestial	Bestial	k1gInSc1	Bestial
Invasion	Invasion	k1gInSc1	Invasion
Of	Of	k1gFnSc1	Of
Hell	Hell	k1gMnSc1	Hell
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
–	–	k?	–
The	The	k1gFnSc2	The
Butcher	Butchra	k1gFnPc2	Butchra
Strikes	Strikes	k1gMnSc1	Strikes
Back	Back	k1gMnSc1	Back
</s>
</p>
<p>
<s>
===	===	k?	===
Neo-Destruction	Neo-Destruction	k1gInSc4	Neo-Destruction
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
====	====	k?	====
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
The	The	k1gMnSc1	The
Least	Least	k1gMnSc1	Least
Successful	Successfula	k1gFnPc2	Successfula
Human	Human	k1gMnSc1	Human
Cannonball	Cannonball	k1gMnSc1	Cannonball
</s>
</p>
<p>
<s>
====	====	k?	====
EP	EP	kA	EP
====	====	k?	====
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
–	–	k?	–
Destruction	Destruction	k1gInSc1	Destruction
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
–	–	k?	–
Them	Them	k1gInSc1	Them
Not	nota	k1gFnPc2	nota
Me	Me	k1gFnSc2	Me
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Thrash	Thrash	k1gMnSc1	Thrash
metal	metat	k5eAaImAgMnS	metat
</s>
</p>
<p>
<s>
Kreator	Kreator	k1gMnSc1	Kreator
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Destruction	Destruction	k1gInSc1	Destruction
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
www.destruction.de	www.destruction.de	k6eAd1	www.destruction.de
–	–	k?	–
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
kapely	kapela	k1gFnSc2	kapela
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Destruction	Destruction	k1gInSc1	Destruction
(	(	kIx(	(
<g/>
band	band	k1gInSc1	band
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
