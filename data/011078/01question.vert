<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
německá	německý	k2eAgFnSc1d1	německá
thrash	thrash	k1gInSc1	thrash
metalová	metalový	k2eAgFnSc1d1	metalová
kapela	kapela	k1gFnSc1	kapela
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
původní	původní	k2eAgNnSc1d1	původní
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
Knight	Knight	k1gInSc4	Knight
Of	Of	k1gMnPc2	Of
Demon	Demon	k1gNnSc4	Demon
<g/>
?	?	kIx.	?
</s>
