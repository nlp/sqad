<s>
Destruction	Destruction	k1gInSc1	Destruction
je	být	k5eAaImIp3nS	být
německá	německý	k2eAgFnSc1d1	německá
thrash	thrash	k1gInSc1	thrash
metalová	metalový	k2eAgFnSc1d1	metalová
kapela	kapela	k1gFnSc1	kapela
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
jméno	jméno	k1gNnSc1	jméno
kapely	kapela	k1gFnSc2	kapela
bylo	být	k5eAaImAgNnS	být
Knight	Knight	k1gInSc4	Knight
Of	Of	k1gFnSc2	Of
Demon	Demon	k1gNnSc4	Demon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
domnívala	domnívat	k5eAaImAgFnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dostatečně	dostatečně	k6eAd1	dostatečně
nekoresponduje	korespondovat	k5eNaImIp3nS	korespondovat
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
změněno	změnit	k5eAaPmNgNnS	změnit
<g/>
.	.	kIx.	.
</s>
