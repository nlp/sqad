<s>
Evropská	evropský	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
ESO	eso	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
astronomické	astronomický	k2eAgFnSc6d1
organizaci	organizace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Eso	eso	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
Dalekohledy	dalekohled	k1gInPc1
VLT	VLT	kA
–	–	k?
nejvýkonnější	výkonný	k2eAgInPc4d3
přístroje	přístroj	k1gInPc4
Evropské	evropský	k2eAgFnSc2d1
jižní	jižní	k2eAgFnSc2d1
observatoře	observatoř	k1gFnSc2
Zkratka	zkratka	k1gFnSc1
</s>
<s>
ESO	eso	k1gNnSc1
Motto	motto	k1gNnSc1
</s>
<s>
Reaching	Reaching	k1gInSc1
New	New	k1gFnSc2
Heights	Heightsa	k1gFnPc2
in	in	k?
Astronomy	astronom	k1gMnPc4
Vznik	vznik	k1gInSc1
</s>
<s>
1962	#num#	k4
Účel	účel	k1gInSc1
</s>
<s>
provozování	provozování	k1gNnSc4
pozemních	pozemní	k2eAgFnPc2d1
astronomických	astronomický	k2eAgFnPc2d1
observatoří	observatoř	k1gFnPc2
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Garching	Garching	k1gInSc1
bei	bei	k?
München	München	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
′	′	k?
<g/>
35	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
11	#num#	k4
<g/>
°	°	k?
<g/>
40	#num#	k4
<g/>
′	′	k?
<g/>
16	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Ředitel	ředitel	k1gMnSc1
</s>
<s>
Xavier	Xavier	k1gInSc1
Barcons	Barcons	k1gInSc1
Hlavní	hlavní	k2eAgInSc1d1
orgán	orgán	k1gInSc1
</s>
<s>
rada	rada	k1gFnSc1
Rozpočet	rozpočet	k1gInSc1
</s>
<s>
198	#num#	k4
milionů	milion	k4xCgInPc2
eur	euro	k1gNnPc2
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Zaměstnanců	zaměstnanec	k1gMnPc2
</s>
<s>
730	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.eso.org	www.eso.org	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
(	(	kIx(
<g/>
European	European	k1gMnSc1
Organisation	Organisation	k1gInSc4
for	forum	k1gNnPc2
Astronomical	Astronomical	k1gMnSc2
Research	Research	k1gFnPc2
in	in	k?
the	the	k?
Southern	Southern	k1gInSc1
Hemisphere	Hemisphere	k1gInSc5
–	–	k?
ESO	eso	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
společná	společný	k2eAgFnSc1d1
astronomická	astronomický	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
evropských	evropský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provozuje	provozovat	k5eAaImIp3nS
několik	několik	k4yIc4
pozemních	pozemní	k2eAgFnPc2d1
astronomických	astronomický	k2eAgFnPc2d1
observatoří	observatoř	k1gFnPc2
umístěných	umístěný	k2eAgFnPc2d1
ve	v	k7c6
vysokých	vysoký	k2eAgFnPc6d1
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
v	v	k7c6
Chile	Chile	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gMnSc7
současným	současný	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
je	být	k5eAaImIp3nS
Xavier	Xavier	k1gMnSc1
Barcons	Barcons	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Členy	člen	k1gMnPc7
Evropské	evropský	k2eAgFnSc2d1
jižní	jižní	k2eAgFnSc2d1
observatoře	observatoř	k1gFnSc2
je	být	k5eAaImIp3nS
většina	většina	k1gFnSc1
vyspělých	vyspělý	k2eAgFnPc2d1
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
je	být	k5eAaImIp3nS
jím	on	k3xPp3gNnSc7
i	i	k9
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Evropskou	evropský	k2eAgFnSc4d1
jižní	jižní	k2eAgFnSc4d1
observatoř	observatoř	k1gFnSc4
založily	založit	k5eAaPmAgFnP
v	v	k7c4
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
Nizozemsko	Nizozemsko	k1gNnSc1
<g/>
,	,	kIx,
Belgie	Belgie	k1gFnSc1
a	a	k8xC
Švédsko	Švédsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
projektu	projekt	k1gInSc2
je	být	k5eAaImIp3nS
umožnit	umožnit	k5eAaPmF
evropským	evropský	k2eAgMnPc3d1
vědcům	vědec	k1gMnPc3
pozorování	pozorování	k1gNnSc1
vesmíru	vesmír	k1gInSc2
z	z	k7c2
jižní	jižní	k2eAgFnSc2d1
polokoule	polokoule	k1gFnSc2
v	v	k7c6
co	co	k9
nejlepších	dobrý	k2eAgFnPc6d3
klimatických	klimatický	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
(	(	kIx(
<g/>
výhodou	výhoda	k1gFnSc7
observatoře	observatoř	k1gFnSc2
v	v	k7c6
jižních	jižní	k2eAgFnPc6d1
zeměpisných	zeměpisný	k2eAgFnPc6d1
šířkách	šířka	k1gFnPc6
je	být	k5eAaImIp3nS
například	například	k6eAd1
možnost	možnost	k1gFnSc4
pozorování	pozorování	k1gNnSc2
středu	střed	k1gInSc2
Mléčné	mléčný	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
bylo	být	k5eAaImAgNnS
vybíráno	vybírat	k5eAaImNgNnS
více	hodně	k6eAd2
lokalit	lokalita	k1gFnPc2
<g/>
,	,	kIx,
mj.	mj.	kA
i	i	k8xC
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
bylo	být	k5eAaImAgNnS
vybráno	vybrat	k5eAaPmNgNnS
místo	místo	k1gNnSc1
v	v	k7c6
Chile	Chile	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
poušti	poušť	k1gFnSc6
Atacama	Atacamum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
observatoř	observatoř	k1gFnSc1
vyrostla	vyrůst	k5eAaPmAgFnS
na	na	k7c6
hoře	hora	k1gFnSc6
La	la	k1gNnSc2
Sila	silo	k1gNnSc2
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
pouště	poušť	k1gFnSc2
<g/>
,	,	kIx,
další	další	k2eAgFnSc1d1
–	–	k?
Observatoř	observatoř	k1gFnSc1
Paranal	Paranal	k1gFnSc1
a	a	k8xC
Observatoř	observatoř	k1gFnSc1
Chajnantor	Chajnantor	k1gInSc1
–	–	k?
pak	pak	k6eAd1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Počet	počet	k1gInSc1
členských	členský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
ESO	eso	k1gNnSc4
se	se	k3xPyFc4
postupně	postupně	k6eAd1
zvýšil	zvýšit	k5eAaPmAgInS
na	na	k7c4
šestnáct	šestnáct	k4xCc4
(	(	kIx(
<g/>
15	#num#	k4
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
a	a	k8xC
Chile	Chile	k1gNnSc2
se	se	k3xPyFc4
zvláštním	zvláštní	k2eAgInSc7d1
statusem	status	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
podepsala	podepsat	k5eAaPmAgFnS
smlouvu	smlouva	k1gFnSc4
s	s	k7c7
ESO	eso	k1gNnSc1
i	i	k9
Brazílie	Brazílie	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
činnosti	činnost	k1gFnSc3
se	se	k3xPyFc4
neúčastnila	účastnit	k5eNaImAgFnS
a	a	k8xC
smlouvu	smlouva	k1gFnSc4
nakonec	nakonec	k6eAd1
neratifikovala	ratifikovat	k5eNaBmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Naopak	naopak	k6eAd1
smlouvu	smlouva	k1gFnSc4
podepsalo	podepsat	k5eAaPmAgNnS
Irsko	Irsko	k1gNnSc1
a	a	k8xC
28	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2018	#num#	k4
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
plnoprávným	plnoprávný	k2eAgInSc7d1
členem	člen	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
smlouvy	smlouva	k1gFnSc2
platné	platný	k2eAgNnSc1d1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2007	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
pořadí	pořadí	k1gNnSc6
13	#num#	k4
<g/>
.	.	kIx.
členem	člen	k1gInSc7
stala	stát	k5eAaPmAgFnS
i	i	k9
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
provoz	provoz	k1gInSc4
observatoře	observatoř	k1gFnSc2
přispívá	přispívat	k5eAaImIp3nS
1,02	1,02	k4
%	%	kIx~
celkového	celkový	k2eAgInSc2d1
rozpočtu	rozpočet	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
činil	činit	k5eAaImAgInS
198	#num#	k4
milionů	milion	k4xCgInPc2
eur	euro	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
zaměstnává	zaměstnávat	k5eAaImIp3nS
okolo	okolo	k7c2
730	#num#	k4
pracovníků	pracovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc1
ústředí	ústředí	k1gNnSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Garchingu	Garching	k1gInSc6
u	u	k7c2
Mnichova	Mnichov	k1gInSc2
(	(	kIx(
<g/>
společně	společně	k6eAd1
s	s	k7c7
centrálou	centrála	k1gFnSc7
ESA	eso	k1gNnSc2
pro	pro	k7c4
využití	využití	k1gNnSc4
vesmírného	vesmírný	k2eAgInSc2d1
teleskopu	teleskop	k1gInSc2
HST	HST	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
ESO	eso	k1gNnSc4
</s>
<s>
V	v	k7c6
Chile	Chile	k1gNnSc6
má	mít	k5eAaImIp3nS
ESO	eso	k1gNnSc1
kancelář	kancelář	k1gFnSc1
v	v	k7c6
Santiagu	Santiago	k1gNnSc6
de	de	k?
Chile	Chile	k1gNnSc6
<g/>
,	,	kIx,
tzv.	tzv.	kA
ESO	eso	k1gNnSc1
Vitacura	Vitacura	k1gFnSc1
office	office	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
centrem	centr	k1gInSc7
především	především	k9
administrativním	administrativní	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Santiagu	Santiago	k1gNnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
i	i	k9
ESO	eso	k1gNnSc1
Guesthouse	Guesthouse	k1gFnSc2
<g/>
,	,	kIx,
přechodné	přechodný	k2eAgNnSc4d1
ubytování	ubytování	k1gNnSc4
pro	pro	k7c4
astronomy	astronom	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
osobně	osobně	k6eAd1
účastní	účastnit	k5eAaImIp3nS
vlastních	vlastní	k2eAgNnPc2d1
pozorování	pozorování	k1gNnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
pro	pro	k7c4
jiné	jiný	k2eAgFnPc4d1
návštěvy	návštěva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Malé	Malé	k2eAgNnSc1d1
administrativní	administrativní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
je	být	k5eAaImIp3nS
umístěno	umístit	k5eAaPmNgNnS
i	i	k9
v	v	k7c6
Antofagastě	Antofagasta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
ubytovací	ubytovací	k2eAgFnSc1d1
a	a	k8xC
servisní	servisní	k2eAgNnPc1d1
centra	centrum	k1gNnPc1
jsou	být	k5eAaImIp3nP
na	na	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
observatořích	observatoř	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Organizace	organizace	k1gFnSc1
výzkumné	výzkumný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
</s>
<s>
V	v	k7c4
ESO	eso	k1gNnSc4
je	být	k5eAaImIp3nS
praktikován	praktikován	k2eAgInSc1d1
velmi	velmi	k6eAd1
efektivní	efektivní	k2eAgInSc1d1
způsob	způsob	k1gInSc1
práce	práce	k1gFnSc2
spočívající	spočívající	k2eAgFnSc1d1
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
kdokoliv	kdokoliv	k3yInSc1
z	z	k7c2
jakékoliv	jakýkoliv	k3yIgFnSc2
země	zem	k1gFnSc2
světa	svět	k1gInSc2
může	moct	k5eAaImIp3nS
předložit	předložit	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
návrh	návrh	k1gInSc4
komisi	komise	k1gFnSc4
složené	složený	k2eAgNnSc1d1
ze	z	k7c2
zástupců	zástupce	k1gMnPc2
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
ho	on	k3xPp3gInSc4
komise	komise	k1gFnSc1
vybere	vybrat	k5eAaPmIp3nS
<g/>
,	,	kIx,
buď	buď	k8xC
půjde	jít	k5eAaImIp3nS
koordinovat	koordinovat	k5eAaBmF
své	svůj	k3xOyFgNnSc4
pozorování	pozorování	k1gNnSc4
přímo	přímo	k6eAd1
na	na	k7c4
observatoř	observatoř	k1gFnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
bude	být	k5eAaImBp3nS
pozorování	pozorování	k1gNnSc1
probíhat	probíhat	k5eAaImF
v	v	k7c6
tzv.	tzv.	kA
servisním	servisní	k2eAgInPc3d1
módu	mód	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
astronom	astronom	k1gMnSc1
pouze	pouze	k6eAd1
předloží	předložit	k5eAaPmIp3nS
své	svůj	k3xOyFgInPc4
požadavky	požadavek	k1gInPc4
a	a	k8xC
astronomové	astronom	k1gMnPc1
na	na	k7c6
observatořích	observatoř	k1gFnPc6
je	on	k3xPp3gInPc4
splní	splnit	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Práce	práce	k1gFnSc1
v	v	k7c4
ESO	eso	k1gNnSc4
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
náročná	náročný	k2eAgFnSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
plnit	plnit	k5eAaImF
velmi	velmi	k6eAd1
přísné	přísný	k2eAgInPc4d1
výkonnostní	výkonnostní	k2eAgInPc4d1
limity	limit	k1gInPc4
a	a	k8xC
přestěhovat	přestěhovat	k5eAaPmF
se	se	k3xPyFc4
na	na	k7c4
několik	několik	k4yIc4
let	léto	k1gNnPc2
do	do	k7c2
Chile	Chile	k1gNnSc2
není	být	k5eNaImIp3nS
často	často	k6eAd1
jednoduché	jednoduchý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
se	se	k3xPyFc4
také	také	k9
stará	starat	k5eAaImIp3nS
o	o	k7c4
vzdělávání	vzdělávání	k1gNnSc4
<g/>
,	,	kIx,
od	od	k7c2
studentských	studentský	k2eAgFnPc2d1
stáží	stáž	k1gFnPc2
po	po	k7c4
soutěže	soutěž	k1gFnPc4
jako	jako	k8xS,k8xC
Catch	Catch	k1gInSc4
a	a	k8xC
Star	Star	kA
<g/>
!	!	kIx.
</s>
<s desamb="1">
pro	pro	k7c4
středoškoláky	středoškolák	k1gMnPc4
či	či	k8xC
výtvarné	výtvarný	k2eAgFnPc4d1
soutěže	soutěž	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESO	eso	k1gNnSc1
je	být	k5eAaImIp3nS
také	také	k9
přínosem	přínos	k1gInSc7
ekonomice	ekonomika	k1gFnSc3
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
ty	ten	k3xDgInPc1
se	se	k3xPyFc4
podílejí	podílet	k5eAaImIp3nP
na	na	k7c6
dodávkách	dodávka	k1gFnPc6
náročných	náročný	k2eAgFnPc2d1
konstrukcí	konstrukce	k1gFnPc2
dalekohledů	dalekohled	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádný	žádný	k1gMnSc1
stát	stát	k5eAaPmF,k5eAaImF
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
Chile	Chile	k1gNnSc2
(	(	kIx(
<g/>
10	#num#	k4
%	%	kIx~
pozorovacího	pozorovací	k2eAgInSc2d1
času	čas	k1gInSc2
na	na	k7c6
VLT	VLT	kA
<g/>
)	)	kIx)
nemá	mít	k5eNaImIp3nS
zaručený	zaručený	k2eAgInSc4d1
pozorovací	pozorovací	k2eAgInSc4d1
čas	čas	k1gInSc4
<g/>
;	;	kIx,
záleží	záležet	k5eAaImIp3nS
na	na	k7c6
aktivitě	aktivita	k1gFnSc6
národních	národní	k2eAgMnPc2d1
astronomů	astronom	k1gMnPc2
a	a	k8xC
kvalitě	kvalita	k1gFnSc3
jejich	jejich	k3xOp3gInPc2
projektů	projekt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
ESO	eso	k1gNnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
nákladná	nákladný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
mnoha	mnoho	k4c6
členských	členský	k2eAgInPc6d1
státech	stát	k1gInPc6
proto	proto	k8xC
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
rušení	rušení	k1gNnSc3
místních	místní	k2eAgFnPc2d1
méně	málo	k6eAd2
výkonných	výkonný	k2eAgFnPc2d1
observatoří	observatoř	k1gFnPc2
a	a	k8xC
přesouvání	přesouvání	k1gNnSc2
peněz	peníze	k1gInPc2
do	do	k7c2
ESO	eso	k1gNnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Probíhající	probíhající	k2eAgInPc1d1
projekty	projekt	k1gInPc1
jako	jako	k8xS,k8xC
ELT	ELT	kA
(	(	kIx(
<g/>
Extremely	Extremel	k1gInPc1
Large	Large	k1gFnSc1
Telescop	Telescop	k1gInSc1
<g/>
)	)	kIx)
s	s	k7c7
průměrem	průměr	k1gInSc7
primárního	primární	k2eAgNnSc2d1
zrcadla	zrcadlo	k1gNnSc2
39	#num#	k4
m	m	kA
budou	být	k5eAaImBp3nP
však	však	k9
velkým	velký	k2eAgInSc7d1
přínosem	přínos	k1gInSc7
světové	světový	k2eAgFnSc3d1
astronomii	astronomie	k1gFnSc3
a	a	k8xC
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejnadějnějším	nadějný	k2eAgMnPc3d3
<g/>
.	.	kIx.
</s>
<s>
Členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Radioteleskopy	radioteleskop	k1gInPc1
ze	z	k7c2
sítě	síť	k1gFnSc2
ALMA	alma	k1gFnSc1
na	na	k7c6
observatoři	observatoř	k1gFnSc6
Chajnantor	Chajnantor	k1gMnSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
<g/>
*	*	kIx~
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Irsko	Irsko	k1gNnSc1
<g/>
*	*	kIx~
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
*	*	kIx~
Přístupová	přístupový	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
není	být	k5eNaImIp3nS
zatím	zatím	k6eAd1
ratifikována	ratifikován	k2eAgFnSc1d1
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Evropskou	evropský	k2eAgFnSc7d1
jižní	jižní	k2eAgFnSc7d1
observatoř	observatoř	k1gFnSc1
založily	založit	k5eAaPmAgInP
v	v	k7c6
roce	rok	k1gInSc6
1962	#num#	k4
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
Belgie	Belgie	k1gFnSc1
<g/>
,	,	kIx,
Nizozemsko	Nizozemsko	k1gNnSc1
a	a	k8xC
Švédsko	Švédsko	k1gNnSc1
(	(	kIx(
<g/>
členy	člen	k1gInPc7
se	se	k3xPyFc4
formálně	formálně	k6eAd1
staly	stát	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
se	se	k3xPyFc4
připojilo	připojit	k5eAaPmAgNnS
Dánsko	Dánsko	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
Švýcarsko	Švýcarsko	k1gNnSc1
a	a	k8xC
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k1gNnSc1
rozšiřování	rozšiřování	k1gNnSc4
pokračovalo	pokračovat	k5eAaImAgNnS
až	až	k9
v	v	k7c6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
:	:	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
se	se	k3xPyFc4
připojilo	připojit	k5eAaPmAgNnS
Portugalsko	Portugalsko	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc3
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
Finsko	Finsko	k1gNnSc1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
Španělsko	Španělsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
13	#num#	k4
<g/>
.	.	kIx.
člen	člen	k1gMnSc1
se	se	k3xPyFc4
k	k	k7c3
ESO	eso	k1gNnSc1
připojila	připojit	k5eAaPmAgFnS
jako	jako	k9
první	první	k4xOgFnSc1
z	z	k7c2
postkomunistických	postkomunistický	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
,	,	kIx,
o	o	k7c4
rok	rok	k1gInSc4
po	po	k7c6
ní	on	k3xPp3gFnSc6
Rakousko	Rakousko	k1gNnSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
Polsko	Polsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
uzavřela	uzavřít	k5eAaPmAgFnS
s	s	k7c7
ESO	eso	k1gNnSc4
strategické	strategický	k2eAgNnSc1d1
partnerství	partnerství	k1gNnSc1
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
dohoda	dohoda	k1gFnSc1
umožní	umožnit	k5eAaPmIp3nS
australským	australský	k2eAgMnPc3d1
astronomům	astronom	k1gMnPc3
podílet	podílet	k5eAaImF
se	se	k3xPyFc4
na	na	k7c6
veškerých	veškerý	k3xTgFnPc6
aktivitách	aktivita	k1gFnPc6
vztahujících	vztahující	k2eAgFnPc6d1
se	se	k3xPyFc4
k	k	k7c3
činnosti	činnost	k1gFnSc3
observatoří	observatoř	k1gFnPc2
La	la	k1gNnSc2
Silla	Sillo	k1gNnSc2
a	a	k8xC
Paranal	Paranal	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Observatoře	observatoř	k1gFnPc1
</s>
<s>
Prstenec	prstenec	k1gInSc1
dalekohledů	dalekohled	k1gInPc2
na	na	k7c6
observatoři	observatoř	k1gFnSc6
La	la	k1gNnSc2
Sila	silo	k1gNnSc2
</s>
<s>
Model	model	k1gInSc1
největšího	veliký	k2eAgInSc2d3
připravovaného	připravovaný	k2eAgInSc2d1
dalekohledu	dalekohled	k1gInSc2
na	na	k7c6
světě	svět	k1gInSc6
–	–	k?
ELT	ELT	kA
</s>
<s>
Paranal	Paranat	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
Observatoř	observatoř	k1gFnSc1
Paranal	Paranal	k1gMnPc4
a	a	k8xC
Extrémně	extrémně	k6eAd1
velký	velký	k2eAgInSc4d1
dalekohled	dalekohled	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Koncem	koncem	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
postavit	postavit	k5eAaPmF
tzv.	tzv.	kA
Very	Very	k1gInPc4
Large	Largus	k1gMnSc5
Telescope	Telescop	k1gMnSc5
(	(	kIx(
<g/>
VLT	VLT	kA
<g/>
)	)	kIx)
se	s	k7c7
čtyřmi	čtyři	k4xCgInPc7
dalekohledy	dalekohled	k1gInPc7
UT	UT	kA
(	(	kIx(
<g/>
Antu	Ant	k1gMnSc3
<g/>
,	,	kIx,
Kueyen	Kueyen	k1gInSc1
<g/>
,	,	kIx,
Melipal	Melipal	k1gMnSc1
<g/>
,	,	kIx,
Yepun	Yepun	k1gMnSc1
<g/>
)	)	kIx)
vybavenými	vybavený	k2eAgNnPc7d1
zrcadly	zrcadlo	k1gNnPc7
o	o	k7c6
průměru	průměr	k1gInSc6
8,2	8,2	k4
metru	metr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
observatoři	observatoř	k1gFnSc6
Paranal	Paranal	k1gFnPc2
jsou	být	k5eAaImIp3nP
tak	tak	k9
dalekohledy	dalekohled	k1gInPc1
AT	AT	kA
s	s	k7c7
průměrem	průměr	k1gInSc7
hl.	hl.	k?
zrcadla	zrcadlo	k1gNnSc2
1,8	1,8	k4
m	m	kA
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
doplňují	doplňovat	k5eAaImIp3nP
UT	UT	kA
pro	pro	k7c4
pozorování	pozorování	k1gNnSc4
VLTI	VLTI	kA
(	(	kIx(
<g/>
VLT	VLT	kA
interferometrem	interferometr	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
britský	britský	k2eAgInSc1d1
dalekohled	dalekohled	k1gInSc1
VISTA	vista	k2eAgInSc1d1
a	a	k8xC
několik	několik	k4yIc1
menších	malý	k2eAgInPc2d2
přístrojů	přístroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1999	#num#	k4
prošlo	projít	k5eAaPmAgNnS
dalekohledem	dalekohled	k1gInSc7
Antu	Ant	k1gMnSc3
první	první	k4xOgNnSc4
světlo	světlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paranal	Paranal	k1gMnSc1
stojí	stát	k5eAaImIp3nS
na	na	k7c6
stejnojmenné	stejnojmenný	k2eAgFnSc6d1
hoře	hora	k1gFnSc6
v	v	k7c6
poušti	poušť	k1gFnSc6
Atacama	Atacamum	k1gNnSc2
<g/>
,	,	kIx,
daleko	daleko	k6eAd1
od	od	k7c2
jakéhokoliv	jakýkoliv	k3yIgNnSc2
světelného	světelný	k2eAgNnSc2d1
znečištění	znečištění	k1gNnSc2
(	(	kIx(
<g/>
nejbližší	blízký	k2eAgNnSc1d3
město	město	k1gNnSc1
Antofagasta	Antofagast	k1gMnSc2
je	být	k5eAaImIp3nS
vzdáleno	vzdálit	k5eAaPmNgNnS
více	hodně	k6eAd2
než	než	k8xS
100	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
hoře	hora	k1gFnSc6
Cerro	Cerro	k1gNnSc4
Armazones	Armazonesa	k1gFnPc2
<g/>
,	,	kIx,
vzdálené	vzdálený	k2eAgFnPc1d1
22	#num#	k4
km	km	kA
západně	západně	k6eAd1
<g/>
,	,	kIx,
staví	stavit	k5eAaImIp3nS,k5eAaPmIp3nS,k5eAaBmIp3nS
Evropská	evropský	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
Extrémně	extrémně	k6eAd1
velký	velký	k2eAgInSc4d1
dalekohled	dalekohled	k1gInSc4
(	(	kIx(
<g/>
ELT	ELT	kA
<g/>
)	)	kIx)
–	–	k?
největší	veliký	k2eAgInSc4d3
připravovovaný	připravovovaný	k2eAgInSc4d1
dalekohled	dalekohled	k1gInSc4
na	na	k7c6
světě	svět	k1gInSc6
o	o	k7c6
průměru	průměr	k1gInSc6
zrcadla	zrcadlo	k1gNnSc2
39	#num#	k4
m.	m.	k?
Tento	tento	k3xDgInSc4
dalekohled	dalekohled	k1gInSc4
bude	být	k5eAaImBp3nS
částečně	částečně	k6eAd1
využívat	využívat	k5eAaImF,k5eAaPmF
zázemí	zázemí	k1gNnSc4
na	na	k7c6
observatoři	observatoř	k1gFnSc6
Paranal	Paranal	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Chajnantor	Chajnantor	k1gMnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Observatoř	observatoř	k1gFnSc1
Chajnantor	Chajnantor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
observatoři	observatoř	k1gFnSc6
Chajnantor	Chajnantor	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
výšce	výška	k1gFnSc6
5	#num#	k4
062	#num#	k4
m	m	kA
nad	nad	k7c7
mořem	moře	k1gNnSc7
v	v	k7c6
poušti	poušť	k1gFnSc6
Atacama	Atacam	k1gMnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
radioteleskop	radioteleskop	k1gInSc1
APEX	apex	k1gInSc1
(	(	kIx(
<g/>
Atacama	Atacama	k1gFnSc1
Pathfinder	Pathfinder	k1gMnSc1
EXperiment	experiment	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
především	především	k9
síť	síť	k1gFnSc1
radioteleskopů	radioteleskop	k1gInPc2
ALMA	alma	k1gFnSc1
(	(	kIx(
<g/>
Atacama	Atacama	k1gFnSc1
Large	Large	k1gInSc1
Millimeter	Millimeter	k1gInSc1
<g/>
/	/	kIx~
<g/>
submillimeter	submillimeter	k1gInSc1
Array	Arraa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
nichž	jenž	k3xRgFnPc6
ESO	eso	k1gNnSc1
spolupracuje	spolupracovat	k5eAaImIp3nS
i	i	k9
s	s	k7c7
dalšími	další	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
La	la	k1gNnSc1
Silla	Sillo	k1gNnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Observatoř	observatoř	k1gFnSc1
La	la	k1gNnSc2
Silla	Sillo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
La	la	k1gNnSc1
Sila	silo	k1gNnSc2
observatory	observator	k1gMnPc4
byla	být	k5eAaImAgFnS
uvedena	uveden	k2eAgFnSc1d1
do	do	k7c2
provozu	provoz	k1gInSc2
v	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
ve	v	k7c6
výšce	výška	k1gFnSc6
2	#num#	k4
400	#num#	k4
m	m	kA
nad	nad	k7c7
mořem	moře	k1gNnSc7
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
pouště	poušť	k1gFnSc2
Atacama	Atacamum	k1gNnSc2
v	v	k7c6
Chile	Chile	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
observatoři	observatoř	k1gFnSc6
je	být	k5eAaImIp3nS
18	#num#	k4
dalekohledů	dalekohled	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
9	#num#	k4
patří	patřit	k5eAaImIp3nP
ESO	eso	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
dalekohledy	dalekohled	k1gInPc4
již	již	k6eAd1
nejsou	být	k5eNaImIp3nP
v	v	k7c6
provozu	provoz	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
observatoř	observatoř	k1gFnSc1
dnes	dnes	k6eAd1
již	již	k6eAd1
není	být	k5eNaImIp3nS
vzhledem	vzhledem	k7c3
k	k	k7c3
vysokým	vysoký	k2eAgInPc3d1
nákladům	náklad	k1gInPc3
velkým	velký	k2eAgInSc7d1
přínosem	přínos	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
této	tento	k3xDgFnSc6
observatoři	observatoř	k1gFnSc6
je	být	k5eAaImIp3nS
mnoho	mnoho	k4c1
dalekohledů	dalekohled	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
si	se	k3xPyFc3
postavily	postavit	k5eAaPmAgFnP
jednotlivé	jednotlivý	k2eAgFnPc4d1
členské	členský	k2eAgFnPc4d1
země	zem	k1gFnPc4
–	–	k?
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
však	však	k9
ESO	eso	k1gNnSc1
jde	jít	k5eAaImIp3nS
u	u	k7c2
velkých	velký	k2eAgInPc2d1
přístrojů	přístroj	k1gInPc2
hlavně	hlavně	k9
směrem	směr	k1gInSc7
společných	společný	k2eAgInPc2d1
projektů	projekt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Jedním	jeden	k4xCgNnSc7
ze	z	k7c2
stále	stále	k6eAd1
ještě	ještě	k6eAd1
používaných	používaný	k2eAgInPc2d1
dalekohledů	dalekohled	k1gInPc2
je	být	k5eAaImIp3nS
i	i	k9
NTT	NTT	kA
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
byla	být	k5eAaImAgFnS
použita	použit	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
adaptivní	adaptivní	k2eAgFnSc2d1
optiky	optika	k1gFnSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
využitá	využitý	k2eAgFnSc1d1
i	i	k9
pro	pro	k7c4
VLT	VLT	kA
<g/>
.	.	kIx.
nejnovějším	nový	k2eAgNnSc7d3
zařízením	zařízení	k1gNnSc7
je	být	k5eAaImIp3nS
v	v	k7c6
r.	r.	kA
2017	#num#	k4
zprovozněná	zprovozněný	k2eAgFnSc1d1
MASCARA	MASCARA	kA
(	(	kIx(
<g/>
Multi-site	Multi-sit	k1gInSc5
All-Sky	All-Ska	k1gFnPc4
CAmeRA	CAmeRA	k1gFnSc4
<g/>
)	)	kIx)
pro	pro	k7c4
detekci	detekce	k1gFnSc4
extrasolárních	extrasolární	k2eAgFnPc2d1
planet	planeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Centrála	centrála	k1gFnSc1
ESO	eso	k1gNnSc1
v	v	k7c6
Garchingu	Garching	k1gInSc6
u	u	k7c2
Mnichova	Mnichov	k1gInSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Llano	Llano	k1gNnSc4
de	de	k?
Chajnantor	Chajnantor	k1gInSc1
Observatory	Observator	k1gMnPc7
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
</s>
<s>
Členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropská	evropský	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2018	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
/	/	kIx~
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Ireland	Ireland	k1gInSc1
ratifies	ratifiesa	k1gFnPc2
ESO	eso	k1gNnSc1
membership	membership	k1gMnSc1
and	and	k?
becomes	becomes	k1gMnSc1
sixteenth	sixteenth	k1gMnSc1
Member	Member	k1gMnSc1
State	status	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropská	evropský	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2018-10-09	2018-10-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
členem	člen	k1gMnSc7
ESO	eso	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
InAstroNoviny	InAstroNovina	k1gFnPc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
Členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropská	evropský	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
<g/>
,	,	kIx,
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
uzavřela	uzavřít	k5eAaPmAgFnS
strategické	strategický	k2eAgNnSc4d1
partnerství	partnerství	k1gNnSc4
s	s	k7c7
ESO	eso	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropská	evropský	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
<g/>
,	,	kIx,
2017-07-11	2017-07-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Tiskové	tiskový	k2eAgFnPc1d1
zprávy	zpráva	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
HADRAVA	HADRAVA	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropská	evropský	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
a	a	k8xC
česká	český	k2eAgFnSc1d1
astronomie	astronomie	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
125	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1435	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
a	a	k8xC
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Evropská	evropský	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Stránky	stránka	k1gFnPc1
ESO	eso	k1gNnSc4
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Dokument	dokument	k1gInSc1
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
z	z	k7c2
cyklu	cyklus	k1gInSc2
Popularis	Popularis	k1gFnSc2
</s>
<s>
BĚLÍK	Bělík	k1gMnSc1
<g/>
,	,	kIx,
Marcel	Marcel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropská	evropský	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
–	–	k?
klenoty	klenot	k1gInPc4
nebe	nebe	k1gNnSc2
přístupné	přístupný	k2eAgFnSc2d1
i	i	k8xC
Česku	Česko	k1gNnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
astronomická	astronomický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
2013-03-19	2013-03-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Astro	astra	k1gFnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1065794045	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2364	#num#	k4
4835	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80097366	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
169775536	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80097366	#num#	k4
</s>
