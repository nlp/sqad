<s>
Julie	Julie	k1gFnSc1	Julie
je	být	k5eAaImIp3nS	být
ženské	ženský	k2eAgNnSc4d1	ženské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
latinského	latinský	k2eAgInSc2d1	latinský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
odvozené	odvozený	k2eAgInPc1d1	odvozený
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
patricijského	patricijský	k2eAgInSc2d1	patricijský
rodu	rod	k1gInSc2	rod
Iuliů	Iulius	k1gMnPc2	Iulius
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ženská	ženský	k2eAgFnSc1d1	ženská
varianta	varianta	k1gFnSc1	varianta
mužského	mužský	k2eAgNnSc2d1	mužské
jména	jméno	k1gNnSc2	jméno
Julius	Julius	k1gMnSc1	Julius
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
Julie	Julie	k1gFnSc2	Julie
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
ženskými	ženský	k2eAgNnPc7d1	ženské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaImF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgFnPc7d1	žijící
ženami	žena	k1gFnPc7	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
-3,7	-3,7	k4	-3,7
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
poměrně	poměrně	k6eAd1	poměrně
strmém	strmý	k2eAgInSc6d1	strmý
poklesu	pokles	k1gInSc6	pokles
obliby	obliba	k1gFnSc2	obliba
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
však	však	k9	však
četnost	četnost	k1gFnSc4	četnost
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Julie	Julie	k1gFnSc1	Julie
Adams	Adamsa	k1gFnPc2	Adamsa
–	–	k?	–
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
Julie	Julie	k1gFnSc2	Julie
Andrews	Andrews	k1gInSc1	Andrews
–	–	k?	–
britsko-americká	britskomerický	k2eAgFnSc1d1	britsko-americká
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Julie	Julie	k1gFnSc2	Julie
Billiart	Billiart	k1gInSc1	Billiart
–	–	k?	–
francouzská	francouzský	k2eAgFnSc1d1	francouzská
řeholnice	řeholnice	k1gFnSc1	řeholnice
Juliette	Juliett	k1gInSc5	Juliett
Binoche	Binoche	k1gFnSc7	Binoche
–	–	k?	–
francouzská	francouzský	k2eAgFnSc1d1	francouzská
herečka	herečka	k1gFnSc1	herečka
Julia	Julius	k1gMnSc2	Julius
Görges	Görges	k1gInSc1	Görges
–	–	k?	–
německá	německý	k2eAgFnSc1d1	německá
tenistka	tenistka	k1gFnSc1	tenistka
Julie	Julie	k1gFnSc1	Julie
Christie	Christie	k1gFnSc1	Christie
–	–	k?	–
britská	britský	k2eAgFnSc1d1	britská
herečka	herečka	k1gFnSc1	herečka
Júlia	Júlia	k1gFnSc1	Júlia
Liptáková	Liptáková	k1gFnSc1	Liptáková
–	–	k?	–
slovenská	slovenský	k2eAgFnSc1d1	slovenská
modelka	modelka	k1gFnSc1	modelka
Juliet	Julieta	k1gFnPc2	Julieta
Millsová	Millsový	k2eAgFnSc1d1	Millsová
–	–	k?	–
britská	britský	k2eAgFnSc1d1	britská
herečka	herečka	k1gFnSc1	herečka
Julia	Julius	k1gMnSc2	Julius
Ormond	Ormond	k1gInSc1	Ormond
–	–	k?	–
anglická	anglický	k2eAgFnSc1d1	anglická
herečka	herečka	k1gFnSc1	herečka
Julia	Julius	k1gMnSc2	Julius
Robertsová	Robertsový	k2eAgFnSc1d1	Robertsová
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
Julia	Julius	k1gMnSc2	Julius
Sanerová	Sanerový	k2eAgFnSc1d1	Sanerový
–	–	k?	–
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
modelka	modelka	k1gFnSc1	modelka
Julia	Julius	k1gMnSc2	Julius
Stiles	Stiles	k1gInSc1	Stiles
–	–	k?	–
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
Julie	Julie	k1gFnSc1	Julie
Šupichová	Šupichová	k1gFnSc1	Šupichová
–	–	k?	–
učitelka	učitelka	k1gFnSc1	učitelka
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
esperantistka	esperantistka	k1gFnSc1	esperantistka
Julia	Julius	k1gMnSc2	Julius
Tymošenko	Tymošenka	k1gFnSc5	Tymošenka
–	–	k?	–
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
politička	politička	k1gFnSc1	politička
Julie	Julie	k1gFnSc1	Julie
Wimmerová	Wimmerová	k1gFnSc1	Wimmerová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
designérka	designérka	k1gFnSc1	designérka
Božská	božská	k1gFnSc1	božská
Julie	Julie	k1gFnSc2	Julie
–	–	k?	–
americko-maďarsko-britský	americkoaďarskoritský	k2eAgInSc1d1	americko-maďarsko-britský
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
Julia	Julius	k1gMnSc2	Julius
–	–	k?	–
americký	americký	k2eAgInSc4d1	americký
film	film	k1gInSc4	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
Julie	Julie	k1gFnSc2	Julie
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
milenci	milenec	k1gMnPc1	milenec
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
Julie	Julie	k1gFnSc2	Julie
a	a	k8xC	a
Julia	Julius	k1gMnSc2	Julius
–	–	k?	–
americký	americký	k2eAgInSc4d1	americký
film	film	k1gInSc4	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
Masožravá	masožravý	k2eAgFnSc1d1	masožravá
Julie	Julie	k1gFnSc1	Julie
–	–	k?	–
československý	československý	k2eAgInSc1d1	československý
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Pavla	Pavel	k1gMnSc2	Pavel
Řezníčková	Řezníčková	k1gFnSc1	Řezníčková
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
–	–	k?	–
divadelní	divadelní	k2eAgNnSc1d1	divadelní
drama	drama	k1gNnSc1	drama
Williama	William	k1gMnSc2	William
Skakespeara	Skakespeara	k1gFnSc1	Skakespeara
Julie	Julie	k1gFnSc1	Julie
Kapuletová	Kapuletový	k2eAgFnSc1d1	Kapuletová
–	–	k?	–
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
tohoto	tento	k3xDgNnSc2	tento
dramatu	drama	k1gNnSc2	drama
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgFnSc1d3	nejznámější
Shakespearova	Shakespearův	k2eAgFnSc1d1	Shakespearova
romantická	romantický	k2eAgFnSc1d1	romantická
hrdinka	hrdinka	k1gFnSc1	hrdinka
Gnomeo	Gnomeo	k1gMnSc1	Gnomeo
&	&	k?	&
Julie	Julie	k1gFnSc2	Julie
–	–	k?	–
britsko-americký	britskomerický	k2eAgInSc1d1	britsko-americký
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
Seznam	seznam	k1gInSc4	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Julie	Julie	k1gFnSc1	Julie
<g/>
"	"	kIx"	"
Seznam	seznam	k1gInSc1	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Julia	Julius	k1gMnSc4	Julius
<g/>
"	"	kIx"	"
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Julie	Julie	k1gFnSc2	Julie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Julie	Julie	k1gFnSc2	Julie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
