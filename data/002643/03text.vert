<s>
Legolas	Legolas	k1gInSc1	Legolas
je	být	k5eAaImIp3nS	být
postava	postava	k1gFnSc1	postava
ve	v	k7c6	v
fiktivním	fiktivní	k2eAgInSc6d1	fiktivní
světě	svět	k1gInSc6	svět
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkieno	k1gNnPc4	Tolkieno
<g/>
,	,	kIx,	,
Středozemi	Středozem	k1gFnSc4	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
elf	elf	k1gMnSc1	elf
pocházející	pocházející	k2eAgMnSc1d1	pocházející
z	z	k7c2	z
Temného	temný	k2eAgInSc2d1	temný
hvozdu	hvozd	k1gInSc2	hvozd
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
trilogii	trilogie	k1gFnSc6	trilogie
Pán	pán	k1gMnSc1	pán
Prstenů	prsten	k1gInPc2	prsten
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
devíti	devět	k4xCc2	devět
členů	člen	k1gInPc2	člen
Společenstva	společenstvo	k1gNnSc2	společenstvo
Prstenu	prsten	k1gInSc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Legolas	Legolas	k1gMnSc1	Legolas
je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
Thranduila	Thranduil	k1gMnSc2	Thranduil
<g/>
,	,	kIx,	,
krále	král	k1gMnSc2	král
Lesních	lesní	k2eAgMnPc2d1	lesní
elfů	elf	k1gMnPc2	elf
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Temného	temný	k2eAgInSc2d1	temný
hvozdu	hvozd	k1gInSc2	hvozd
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zmiňován	zmiňovat	k5eAaImNgMnS	zmiňovat
v	v	k7c6	v
Hobitovi	hobit	k1gMnSc6	hobit
jako	jako	k8xC	jako
princ	princa	k1gFnPc2	princa
"	"	kIx"	"
<g/>
elfů	elf	k1gMnPc2	elf
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Legolas	Legolas	k1gInSc1	Legolas
však	však	k9	však
není	být	k5eNaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
lesních	lesní	k2eAgMnPc2d1	lesní
elfů	elf	k1gMnPc2	elf
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
považoval	považovat	k5eAaImAgMnS	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
totiž	totiž	k9	totiž
původně	původně	k6eAd1	původně
přišel	přijít	k5eAaPmAgMnS	přijít
z	z	k7c2	z
Doriathu	Doriath	k1gInSc2	Doriath
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
tedy	tedy	k9	tedy
jsou	být	k5eAaImIp3nP	být
sindarského	sindarský	k2eAgMnSc4d1	sindarský
původu	původa	k1gMnSc4	původa
a	a	k8xC	a
součástí	součást	k1gFnSc7	součást
menšiny	menšina	k1gFnSc2	menšina
Sindar	Sindar	k1gInSc1	Sindar
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vznešenější	vznešený	k2eAgFnSc1d2	vznešenější
a	a	k8xC	a
moudřejší	moudrý	k2eAgMnSc1d2	moudřejší
než	než	k8xS	než
Lesní	lesní	k2eAgMnSc1d1	lesní
elfové	elf	k1gMnPc1	elf
<g/>
.	.	kIx.	.
</s>
<s>
Stejná	stejný	k2eAgFnSc1d1	stejná
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
Lothlórienu	Lothlórien	k1gInSc6	Lothlórien
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
vládne	vládnout	k5eAaImIp3nS	vládnout
menšina	menšina	k1gFnSc1	menšina
Sindar	Sindara	k1gFnPc2	Sindara
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Noldo	Noldo	k1gNnSc4	Noldo
Galadriel	Galadriel	k1gInSc1	Galadriel
a	a	k8xC	a
Sindar	Sindar	k1gInSc1	Sindar
Celebornem	Celeborn	k1gInSc7	Celeborn
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Společenstvo	společenstvo	k1gNnSc1	společenstvo
Prstenu	prsten	k1gInSc2	prsten
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc2	první
části	část	k1gFnSc2	část
Pána	pán	k1gMnSc4	pán
Prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
při	při	k7c6	při
Elrondově	Elrondův	k2eAgFnSc6d1	Elrondova
radě	rada	k1gFnSc6	rada
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přichází	přicházet	k5eAaImIp3nS	přicházet
jako	jako	k9	jako
posel	posel	k1gMnSc1	posel
od	od	k7c2	od
svého	své	k1gNnSc2	své
otce	otec	k1gMnSc2	otec
se	s	k7c7	s
zprávou	zpráva	k1gFnSc7	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Glum	Glum	k1gMnSc1	Glum
utekl	utéct	k5eAaPmAgMnS	utéct
z	z	k7c2	z
elfského	elfský	k2eAgNnSc2d1	elfské
zajetí	zajetí	k1gNnSc2	zajetí
v	v	k7c6	v
Temném	temný	k2eAgInSc6d1	temný
hvozdě	hvozd	k1gInSc6	hvozd
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
ho	on	k3xPp3gMnSc4	on
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
cizího	cizí	k2eAgMnSc4d1	cizí
elfa	elf	k1gMnSc4	elf
v	v	k7c6	v
zelené	zelený	k2eAgFnSc6d1	zelená
a	a	k8xC	a
hnědé	hnědý	k2eAgFnSc6d1	hnědá
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
radě	rada	k1gFnSc6	rada
si	se	k3xPyFc3	se
Legolas	Legolas	k1gMnSc1	Legolas
sám	sám	k3xTgMnSc1	sám
zvolil	zvolit	k5eAaPmAgMnS	zvolit
být	být	k5eAaImF	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
členů	člen	k1gInPc2	člen
Společenstva	společenstvo	k1gNnSc2	společenstvo
Prstenu	prsten	k1gInSc2	prsten
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
zajistit	zajistit	k5eAaPmF	zajistit
zničení	zničení	k1gNnSc3	zničení
Jednoho	jeden	k4xCgInSc2	jeden
prstenu	prsten	k1gInSc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
Froda	Froda	k1gMnSc1	Froda
od	od	k7c2	od
Roklinky	roklinka	k1gFnSc2	roklinka
až	až	k9	až
po	po	k7c4	po
Amon	Amon	k1gNnSc4	Amon
Hen	hena	k1gFnPc2	hena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
společenstvo	společenstvo	k1gNnSc1	společenstvo
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sestupu	sestup	k1gInSc6	sestup
po	po	k7c6	po
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
pokusu	pokus	k1gInSc6	pokus
Společenstva	společenstvo	k1gNnSc2	společenstvo
zdolat	zdolat	k5eAaPmF	zdolat
průsmyk	průsmyk	k1gInSc4	průsmyk
Caradhras	Caradhrasa	k1gFnPc2	Caradhrasa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
napřed	napřed	k6eAd1	napřed
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Aragorn	Aragorn	k1gInSc4	Aragorn
a	a	k8xC	a
Boromir	Boromir	k1gInSc4	Boromir
prošlapávali	prošlapávat	k5eAaImAgMnP	prošlapávat
cestu	cesta	k1gFnSc4	cesta
sněhem	sníh	k1gInSc7	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
dovedl	dovést	k5eAaPmAgMnS	dovést
pohybovat	pohybovat	k5eAaImF	pohybovat
tak	tak	k9	tak
lehce	lehko	k6eAd1	lehko
<g/>
,	,	kIx,	,
že	že	k8xS	že
téměř	téměř	k6eAd1	téměř
nezanechával	zanechávat	k5eNaImAgMnS	zanechávat
stopy	stopa	k1gFnPc4	stopa
ve	v	k7c6	v
sněhu	sníh	k1gInSc6	sníh
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
přispívaly	přispívat	k5eAaImAgFnP	přispívat
i	i	k9	i
jeho	jeho	k3xOp3gFnPc1	jeho
lehké	lehký	k2eAgFnPc1d1	lehká
boty	bota	k1gFnPc1	bota
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yRnSc4	co
sestoupili	sestoupit	k5eAaPmAgMnP	sestoupit
z	z	k7c2	z
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
vůdce	vůdce	k1gMnSc1	vůdce
Gandalf	Gandalf	k1gMnSc1	Gandalf
je	být	k5eAaImIp3nS	být
zavvedl	zavvednout	k5eAaPmAgMnS	zavvednout
do	do	k7c2	do
Morie	Morie	k1gFnSc2	Morie
<g/>
,	,	kIx,	,
staré	starý	k2eAgFnSc2d1	stará
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
opuštěné	opuštěný	k2eAgFnSc2d1	opuštěná
říše	říš	k1gFnSc2	říš
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
někteří	některý	k3yIgMnPc1	některý
včetně	včetně	k7c2	včetně
Legolase	Legolas	k1gInSc6	Legolas
si	se	k3xPyFc3	se
tam	tam	k6eAd1	tam
nepřáli	přát	k5eNaImAgMnP	přát
jít	jít	k5eAaImF	jít
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
než	než	k8xS	než
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
Morie	Morie	k1gFnSc2	Morie
<g/>
,	,	kIx,	,
pomohl	pomoct	k5eAaPmAgMnS	pomoct
ještě	ještě	k6eAd1	ještě
odrazit	odrazit	k5eAaPmF	odrazit
útok	útok	k1gInSc4	útok
Sauronových	Sauronův	k2eAgMnPc2d1	Sauronův
vlků	vlk	k1gMnPc2	vlk
v	v	k7c6	v
Cesmínii	Cesmínie	k1gFnSc6	Cesmínie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Morii	Morie	k1gFnSc6	Morie
bojoval	bojovat	k5eAaImAgMnS	bojovat
proti	proti	k7c3	proti
skřetům	skřet	k1gMnPc3	skřet
a	a	k8xC	a
poznal	poznat	k5eAaPmAgMnS	poznat
v	v	k7c6	v
Durinově	Durinův	k2eAgFnSc6d1	Durinova
zhoubě	zhouba	k1gFnSc6	zhouba
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
Morgothových	Morgothův	k2eAgMnPc2d1	Morgothův
balrogů	balrog	k1gMnPc2	balrog
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
družina	družina	k1gFnSc1	družina
vyšla	vyjít	k5eAaPmAgFnS	vyjít
z	z	k7c2	z
Morie	Morie	k1gFnSc2	Morie
již	již	k6eAd1	již
bez	bez	k7c2	bez
Gandalfa	Gandalf	k1gMnSc2	Gandalf
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
ztratil	ztratit	k5eAaPmAgMnS	ztratit
při	při	k7c6	při
boji	boj	k1gInSc6	boj
s	s	k7c7	s
balrogem	balrog	k1gMnSc7	balrog
<g/>
,	,	kIx,	,
převzal	převzít	k5eAaPmAgInS	převzít
vedení	vedení	k1gNnSc4	vedení
Aragorn	Aragorna	k1gFnPc2	Aragorna
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgMnS	zavést
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
elfí	elfí	k2eAgFnSc2d1	elfí
říše	říš	k1gFnSc2	říš
Lothlórienu	Lothlórien	k1gInSc2	Lothlórien
<g/>
.	.	kIx.	.
</s>
<s>
Legolas	Legolas	k1gInSc1	Legolas
byl	být	k5eAaImAgInS	být
zpočátku	zpočátku	k6eAd1	zpočátku
jejich	jejich	k3xOp3gMnSc7	jejich
mluvčím	mluvčí	k1gMnSc7	mluvčí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
mohl	moct	k5eAaImAgMnS	moct
domluvit	domluvit	k5eAaPmF	domluvit
s	s	k7c7	s
Galadhrim	Galadhri	k1gNnSc7	Galadhri
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
strážili	strážit	k5eAaImAgMnP	strážit
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
Společenstva	společenstvo	k1gNnSc2	společenstvo
byly	být	k5eAaImAgFnP	být
třenice	třenice	k1gFnSc1	třenice
mezi	mezi	k7c7	mezi
Legolasem	Legolas	k1gMnSc7	Legolas
a	a	k8xC	a
trpaslíkem	trpaslík	k1gMnSc7	trpaslík
Gimlim	Gimlima	k1gFnPc2	Gimlima
vycházející	vycházející	k2eAgFnSc1d1	vycházející
ze	z	k7c2	z
starověkých	starověký	k2eAgFnPc2d1	starověká
hádek	hádka	k1gFnPc2	hádka
mezi	mezi	k7c4	mezi
elfy	elf	k1gMnPc4	elf
a	a	k8xC	a
trpaslíky	trpaslík	k1gMnPc4	trpaslík
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
vyplněním	vyplnění	k1gNnSc7	vyplnění
elfího	elfí	k2eAgNnSc2d1	elfí
království	království	k1gNnSc2	království
Doriathu	Doriath	k1gInSc2	Doriath
a	a	k8xC	a
zavražděním	zavraždění	k1gNnSc7	zavraždění
krále	král	k1gMnSc2	král
Thingola	Thingola	k1gFnSc1	Thingola
v	v	k7c6	v
Prvním	první	k4xOgInSc6	první
věku	věk	k1gInSc6	věk
a	a	k8xC	a
také	také	k9	také
uvězněním	uvěznění	k1gNnSc7	uvěznění
Gimliho	Gimli	k1gMnSc2	Gimli
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
Glóina	Glóin	k1gMnSc2	Glóin
<g/>
,	,	kIx,	,
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
12	[number]	k4	12
trpaslíků	trpaslík	k1gMnPc2	trpaslík
během	během	k7c2	během
Bilbovy	Bilbův	k2eAgFnSc2d1	Bilbova
výpravy	výprava	k1gFnSc2	výprava
k	k	k7c3	k
Osamělé	osamělý	k2eAgFnSc3d1	osamělá
hoře	hora	k1gFnSc3	hora
(	(	kIx(	(
<g/>
jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
v	v	k7c6	v
Hobitovi	hobit	k1gMnSc6	hobit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
v	v	k7c6	v
Lothlórienu	Lothlórien	k1gInSc6	Lothlórien
přátelství	přátelství	k1gNnSc2	přátelství
<g/>
,	,	kIx,	,
když	když	k8xS	když
Gimli	Giml	k1gMnSc3	Giml
spatřil	spatřit	k5eAaPmAgMnS	spatřit
Paní	paní	k1gFnSc4	paní
Zlatého	zlatý	k2eAgInSc2d1	zlatý
lesa	les	k1gInSc2	les
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jí	jíst	k5eAaImIp3nS	jíst
přívětivě	přívětivě	k6eAd1	přívětivě
přijat	přijmout	k5eAaPmNgInS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
opustila	opustit	k5eAaPmAgFnS	opustit
družina	družina	k1gFnSc1	družina
Prstenu	prsten	k1gInSc2	prsten
Lothlórien	Lothlórina	k1gFnPc2	Lothlórina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
bez	bez	k7c2	bez
darů	dar	k1gInPc2	dar
<g/>
.	.	kIx.	.
</s>
<s>
Legolas	Legolas	k1gMnSc1	Legolas
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
Galadriel	Galadriela	k1gFnPc2	Galadriela
nový	nový	k2eAgInSc4d1	nový
luk	luk	k1gInSc4	luk
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
byli	být	k5eAaImAgMnP	být
obdarováni	obdarovat	k5eAaPmNgMnP	obdarovat
plášti	plášť	k1gInPc7	plášť
a	a	k8xC	a
lembasem	lembas	k1gInSc7	lembas
<g/>
,	,	kIx,	,
elfským	elfský	k2eAgInSc7d1	elfský
chlebem	chléb	k1gInSc7	chléb
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
plavbě	plavba	k1gFnSc6	plavba
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
Anduině	Anduin	k2eAgFnSc6d1	Anduin
sestřelil	sestřelit	k5eAaPmAgMnS	sestřelit
jedinou	jediný	k2eAgFnSc7d1	jediná
ranou	rána	k1gFnSc7	rána
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
nového	nový	k2eAgInSc2d1	nový
luku	luk	k1gInSc2	luk
nazgû	nazgû	k?	nazgû
na	na	k7c6	na
létajícím	létající	k2eAgMnSc6d1	létající
oři	oř	k1gMnSc6	oř
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Sarumanovi	Sarumanův	k2eAgMnPc1d1	Sarumanův
Skurut-hai	Skuruta	k1gMnPc1	Skurut-ha
zabili	zabít	k5eAaPmAgMnP	zabít
Boromira	Boromir	k1gMnSc4	Boromir
a	a	k8xC	a
unesli	unést	k5eAaPmAgMnP	unést
Smíška	Smíšek	k1gMnSc4	Smíšek
s	s	k7c7	s
Pipinem	Pipin	k1gMnSc7	Pipin
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
Aragornem	Aragorn	k1gInSc7	Aragorn
a	a	k8xC	a
Gimlim	Gimlim	k1gInSc4	Gimlim
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
skřety	skřet	k1gMnPc4	skřet
<g/>
,	,	kIx,	,
jež	jenž	k3xRgMnPc4	jenž
unesli	unést	k5eAaPmAgMnP	unést
dva	dva	k4xCgMnPc4	dva
hobity	hobit	k1gMnPc4	hobit
(	(	kIx(	(
<g/>
Frodo	Frodo	k1gNnSc1	Frodo
a	a	k8xC	a
Sam	Sam	k1gMnSc1	Sam
odpluli	odplout	k5eAaPmAgMnP	odplout
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
břeh	břeh	k1gInSc4	břeh
a	a	k8xC	a
pak	pak	k6eAd1	pak
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
však	však	k9	však
pochovali	pochovat	k5eAaPmAgMnP	pochovat
Boromira	Boromir	k1gInSc2	Boromir
v	v	k7c6	v
lodi	loď	k1gFnSc6	loď
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
pustili	pustit	k5eAaPmAgMnP	pustit
do	do	k7c2	do
Rauroských	Rauroský	k2eAgInPc2d1	Rauroský
vodopádů	vodopád	k1gInPc2	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
trojici	trojice	k1gFnSc6	trojice
pak	pak	k6eAd1	pak
procházeli	procházet	k5eAaImAgMnP	procházet
všechna	všechen	k3xTgNnPc4	všechen
ostatní	ostatní	k2eAgNnPc4d1	ostatní
dobrodružství	dobrodružství	k1gNnPc4	dobrodružství
<g/>
.	.	kIx.	.
</s>
<s>
Skřety	skřet	k1gMnPc4	skřet
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nepovedlo	povést	k5eNaPmAgNnS	povést
dostihnout	dostihnout	k5eAaPmF	dostihnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byli	být	k5eAaImAgMnP	být
pobiti	pobít	k5eAaPmNgMnP	pobít
rohanskými	rohanský	k2eAgMnPc7d1	rohanský
jezdci	jezdec	k1gMnPc7	jezdec
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
Legolas	Legolas	k1gMnSc1	Legolas
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
vydali	vydat	k5eAaPmAgMnP	vydat
po	po	k7c6	po
hobitích	hobití	k1gNnPc6	hobití
stopách	stopa	k1gFnPc6	stopa
do	do	k7c2	do
Fangornu	Fangorn	k1gInSc2	Fangorn
<g/>
,	,	kIx,	,
našli	najít	k5eAaPmAgMnP	najít
zde	zde	k6eAd1	zde
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
velké	velký	k2eAgFnSc3d1	velká
radosti	radost	k1gFnSc3	radost
Gandalfa	Gandalf	k1gMnSc2	Gandalf
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
ze	z	k7c2	z
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
také	také	k9	také
přinesl	přinést	k5eAaPmAgInS	přinést
Legolasovi	Legolas	k1gMnSc3	Legolas
varování	varování	k1gNnPc4	varování
od	od	k7c2	od
Galadriel	Galadriela	k1gFnPc2	Galadriela
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zelený	Zelený	k1gMnSc1	Zelený
lístečku	lísteček	k1gInSc2	lísteček
<g/>
,	,	kIx,	,
dlouho	dlouho	k6eAd1	dlouho
jsi	být	k5eAaImIp2nS	být
pod	pod	k7c7	pod
stromem	strom	k1gInSc7	strom
v	v	k7c6	v
radosti	radost	k1gFnSc6	radost
přebýval	přebývat	k5eAaImAgMnS	přebývat
<g/>
.	.	kIx.	.
</s>
<s>
Varuj	varovat	k5eAaImRp2nS	varovat
se	se	k3xPyFc4	se
před	před	k7c7	před
Mořem	moře	k1gNnSc7	moře
<g/>
!	!	kIx.	!
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
jen	jen	k6eAd1	jen
zaslechneš	zaslechnout	k5eAaPmIp2nS	zaslechnout
křik	křik	k1gInSc1	křik
racků	racek	k1gMnPc2	racek
na	na	k7c6	na
mělčině	mělčina	k1gFnSc6	mělčina
<g/>
,	,	kIx,	,
víckrát	víckrát	k6eAd1	víckrát
už	už	k6eAd1	už
srdce	srdce	k1gNnSc4	srdce
tvé	tvůj	k3xOp2gNnSc4	tvůj
ve	v	k7c6	v
hvozdě	hvozd	k1gInSc6	hvozd
nespočine	spočinout	k5eNaPmIp3nS	spočinout
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Poté	poté	k6eAd1	poté
bojovali	bojovat	k5eAaImAgMnP	bojovat
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Helmův	Helmův	k2eAgInSc4d1	Helmův
žleb	žleb	k1gInSc4	žleb
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Legolas	Legolas	k1gMnSc1	Legolas
soutěžil	soutěžit	k5eAaImAgMnS	soutěžit
s	s	k7c7	s
Gimlim	Gimli	k1gNnSc7	Gimli
v	v	k7c4	v
zabíjení	zabíjení	k1gNnSc4	zabíjení
skřetů	skřet	k1gMnPc2	skřet
(	(	kIx(	(
<g/>
Gimli	Gimle	k1gFnSc4	Gimle
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
o	o	k7c4	o
jednoho	jeden	k4xCgMnSc4	jeden
<g/>
,	,	kIx,	,
zabil	zabít	k5eAaPmAgMnS	zabít
jich	on	k3xPp3gFnPc2	on
43	[number]	k4	43
<g/>
,	,	kIx,	,
Legolas	Legolas	k1gInSc1	Legolas
42	[number]	k4	42
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
táhli	táhnout	k5eAaImAgMnP	táhnout
s	s	k7c7	s
Gandalfem	Gandalf	k1gMnSc7	Gandalf
a	a	k8xC	a
králem	král	k1gMnSc7	král
Théodenem	Théoden	k1gMnSc7	Théoden
do	do	k7c2	do
Železného	železný	k2eAgInSc2d1	železný
pasu	pas	k1gInSc2	pas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
svědky	svědek	k1gMnPc4	svědek
Sarumanova	Sarumanův	k2eAgInSc2d1	Sarumanův
pádu	pád	k1gInSc2	pád
a	a	k8xC	a
kde	kde	k6eAd1	kde
také	také	k6eAd1	také
potkali	potkat	k5eAaPmAgMnP	potkat
postrádané	postrádaný	k2eAgMnPc4d1	postrádaný
hobity	hobit	k1gMnPc4	hobit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tažení	tažení	k1gNnSc6	tažení
do	do	k7c2	do
Gondoru	Gondor	k1gInSc2	Gondor
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
s	s	k7c7	s
Gimlim	Gimli	k1gNnSc7	Gimli
<g/>
,	,	kIx,	,
hraničáři	hraničář	k1gMnSc3	hraničář
ze	z	k7c2	z
Severu	sever	k1gInSc2	sever
a	a	k8xC	a
Elrondovými	Elrondový	k2eAgMnPc7d1	Elrondový
syny	syn	k1gMnPc7	syn
Aragorna	Aragorno	k1gNnSc2	Aragorno
na	na	k7c6	na
Stezkách	stezka	k1gFnPc6	stezka
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Gimliho	Gimli	k1gMnSc2	Gimli
neměl	mít	k5eNaImAgMnS	mít
Legolas	Legolas	k1gInSc4	Legolas
z	z	k7c2	z
Mrtvých	mrtvý	k2eAgFnPc2d1	mrtvá
žádný	žádný	k3yNgInSc4	žádný
strach	strach	k1gInSc4	strach
<g/>
.	.	kIx.	.
</s>
<s>
Aragorn	Aragorn	k1gMnSc1	Aragorn
přemluvil	přemluvit	k5eAaPmAgMnS	přemluvit
Mrtvé	mrtvý	k1gMnPc4	mrtvý
z	z	k7c2	z
Černoboru	Černobor	k1gInSc2	Černobor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
bojovali	bojovat	k5eAaImAgMnP	bojovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
vyslal	vyslat	k5eAaPmAgMnS	vyslat
proti	proti	k7c3	proti
Umbarským	Umbarský	k2eAgMnPc3d1	Umbarský
korzárům	korzár	k1gMnPc3	korzár
<g/>
.	.	kIx.	.
</s>
<s>
Legolas	Legolas	k1gMnSc1	Legolas
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
dostal	dostat	k5eAaPmAgInS	dostat
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
a	a	k8xC	a
když	když	k8xS	když
uslyšel	uslyšet	k5eAaPmAgInS	uslyšet
racky	racek	k1gMnPc4	racek
<g/>
,	,	kIx,	,
vyplnilo	vyplnit	k5eAaPmAgNnS	vyplnit
se	se	k3xPyFc4	se
Galadrielino	Galadrielin	k2eAgNnSc1d1	Galadrielin
proroctví	proroctví	k1gNnSc1	proroctví
<g/>
.	.	kIx.	.
</s>
<s>
Probudila	probudit	k5eAaPmAgFnS	probudit
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
touha	touha	k1gFnSc1	touha
odplout	odplout	k5eAaPmF	odplout
do	do	k7c2	do
Valinoru	Valinor	k1gInSc2	Valinor
<g/>
,	,	kIx,	,
skrytá	skrytý	k2eAgFnSc1d1	skrytá
v	v	k7c6	v
srdcích	srdce	k1gNnPc6	srdce
všech	všecek	k3xTgFnPc2	všecek
Eldar	Eldara	k1gFnPc2	Eldara
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
z	z	k7c2	z
Pelargiru	Pelargir	k1gInSc2	Pelargir
plavil	plavit	k5eAaImAgInS	plavit
do	do	k7c2	do
Osgiliathu	Osgiliath	k1gInSc2	Osgiliath
a	a	k8xC	a
bojoval	bojovat	k5eAaImAgInS	bojovat
na	na	k7c6	na
Pelennorských	Pelennorský	k2eAgNnPc6d1	Pelennorský
polích	pole	k1gNnPc6	pole
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
u	u	k7c2	u
Morannonu	Morannon	k1gInSc2	Morannon
a	a	k8xC	a
uviděl	uvidět	k5eAaPmAgInS	uvidět
pád	pád	k1gInSc1	pád
Barad-dû	Baradû	k1gMnSc4	Barad-dû
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
koncem	konec	k1gInSc7	konec
Saurona	Sauron	k1gMnSc2	Sauron
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zničení	zničení	k1gNnSc6	zničení
Jednoho	jeden	k4xCgInSc2	jeden
prstenu	prsten	k1gInSc3	prsten
chvíli	chvíle	k1gFnSc4	chvíle
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c4	v
Minas	Minas	k1gInSc4	Minas
Tirith	Tirith	k1gMnSc1	Tirith
a	a	k8xC	a
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
korunovace	korunovace	k1gFnSc2	korunovace
krále	král	k1gMnSc2	král
Aragorna	Aragorno	k1gNnSc2	Aragorno
Elessara	Elessar	k1gMnSc2	Elessar
králem	král	k1gMnSc7	král
Obnoveného	obnovený	k2eAgNnSc2d1	obnovené
království	království	k1gNnSc2	království
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
svatby	svatba	k1gFnSc2	svatba
s	s	k7c7	s
Arwen	Arwna	k1gFnPc2	Arwna
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
Gimlim	Gimli	k1gNnSc7	Gimli
procestoval	procestovat	k5eAaPmAgMnS	procestovat
Fangorn	Fangorn	k1gMnSc1	Fangorn
<g/>
.	.	kIx.	.
</s>
<s>
Šel	jít	k5eAaImAgMnS	jít
s	s	k7c7	s
otcovým	otcův	k2eAgNnSc7d1	otcovo
svolením	svolení	k1gNnSc7	svolení
s	s	k7c7	s
několika	několik	k4yIc7	několik
dalšími	další	k2eAgMnPc7d1	další
elfy	elf	k1gMnPc7	elf
do	do	k7c2	do
Ithilienu	Ithilien	k1gInSc2	Ithilien
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
obnovil	obnovit	k5eAaPmAgMnS	obnovit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
zbývajícím	zbývající	k2eAgInSc6d1	zbývající
čase	čas	k1gInSc6	čas
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
tamní	tamní	k2eAgInPc1d1	tamní
lesy	les	k1gInPc1	les
zdevastované	zdevastovaný	k2eAgInPc1d1	zdevastovaný
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
založil	založit	k5eAaPmAgMnS	založit
elfí	elfí	k2eAgFnSc4d1	elfí
kolonii	kolonie	k1gFnSc4	kolonie
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jejím	její	k3xOp3gMnSc7	její
pánem	pán	k1gMnSc7	pán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Červené	Červené	k2eAgFnSc6d1	Červené
knize	kniha	k1gFnSc6	kniha
(	(	kIx(	(
<g/>
psané	psaný	k2eAgFnSc2d1	psaná
nejprve	nejprve	k6eAd1	nejprve
Bilbem	Bilb	k1gMnSc7	Bilb
Pytlíkem	pytlík	k1gMnSc7	pytlík
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
Frodem	Frod	k1gMnSc7	Frod
Pytlíkem	pytlík	k1gMnSc7	pytlík
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dokončené	dokončený	k2eAgInPc1d1	dokončený
Samem	Samos	k1gInSc7	Samos
Křepelkou	křepelka	k1gFnSc7	křepelka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
napsáno	napsat	k5eAaBmNgNnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
krále	král	k1gMnSc2	král
Elessara	Elessar	k1gMnSc2	Elessar
postavil	postavit	k5eAaPmAgMnS	postavit
Legolas	Legolas	k1gInSc4	Legolas
v	v	k7c6	v
Ithilienu	Ithilien	k1gInSc6	Ithilien
šedou	šedý	k2eAgFnSc4d1	šedá
loď	loď	k1gFnSc4	loď
a	a	k8xC	a
odplul	odplout	k5eAaPmAgMnS	odplout
ze	z	k7c2	z
Středozemě	Středozem	k1gFnSc2	Středozem
do	do	k7c2	do
Valinoru	Valinor	k1gInSc2	Valinor
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
trpaslíkem	trpaslík	k1gMnSc7	trpaslík
Gimlim	Gimlima	k1gFnPc2	Gimlima
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
trilogii	trilogie	k1gFnSc6	trilogie
Pán	pán	k1gMnSc1	pán
Prstenů	prsten	k1gInPc2	prsten
od	od	k7c2	od
Petera	Peter	k1gMnSc2	Peter
Jacksona	Jackson	k1gMnSc2	Jackson
Legolase	Legolasa	k1gFnSc3	Legolasa
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
britský	britský	k2eAgMnSc1d1	britský
herec	herec	k1gMnSc1	herec
Orlando	Orlanda	k1gFnSc5	Orlanda
Bloom	Bloom	k1gInSc4	Bloom
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
postava	postava	k1gFnSc1	postava
celkem	celkem	k6eAd1	celkem
věrně	věrně	k6eAd1	věrně
drží	držet	k5eAaImIp3nP	držet
literární	literární	k2eAgFnPc4d1	literární
předlohy	předloha	k1gFnPc4	předloha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Hobit	hobit	k1gMnSc1	hobit
aneb	aneb	k?	aneb
Cesta	cesta	k1gFnSc1	cesta
tam	tam	k6eAd1	tam
a	a	k8xC	a
zase	zase	k9	zase
zpátky	zpátky	k6eAd1	zpátky
je	být	k5eAaImIp3nS	být
postava	postava	k1gFnSc1	postava
Legolase	Legolas	k1gInSc6	Legolas
postavou	postava	k1gFnSc7	postava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
předloze	předloha	k1gFnSc6	předloha
vůbec	vůbec	k9	vůbec
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
jiný	jiný	k2eAgInSc4d1	jiný
charakter	charakter	k1gInSc4	charakter
než	než	k8xS	než
v	v	k7c6	v
Pánovi	pán	k1gMnSc6	pán
Prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
filmu	film	k1gInSc6	film
byl	být	k5eAaImAgMnS	být
také	také	k9	také
zamilován	zamilován	k2eAgInSc4d1	zamilován
do	do	k7c2	do
elfky	elfka	k1gMnSc2	elfka
Tauriel	Tauriel	k1gInSc4	Tauriel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
jeho	jeho	k3xOp3gFnSc4	jeho
lásku	láska	k1gFnSc4	láska
neopětovala	opětovat	k5eNaImAgFnS	opětovat
<g/>
.	.	kIx.	.
</s>
