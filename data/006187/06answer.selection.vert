<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
Francie	Francie	k1gFnSc2	Francie
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1795	[number]	k4	1795
podle	podle	k7c2	podle
nové	nový	k2eAgFnSc2d1	nová
ústavy	ústava	k1gFnSc2	ústava
postavilo	postavit	k5eAaPmAgNnS	postavit
pětičlenné	pětičlenný	k2eAgNnSc1d1	pětičlenné
Direktorium	direktorium	k1gNnSc1	direktorium
<g/>
,	,	kIx,	,
za	za	k7c2	za
jehož	jehož	k3xOyRp3gFnSc2	jehož
vlády	vláda	k1gFnSc2	vláda
nastal	nastat	k5eAaPmAgInS	nastat
"	"	kIx"	"
<g/>
odliv	odliv	k1gInSc1	odliv
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
"	"	kIx"	"
a	a	k8xC	a
probíhal	probíhat	k5eAaImAgInS	probíhat
také	také	k9	také
tzv.	tzv.	kA	tzv.
bílý	bílý	k2eAgInSc1d1	bílý
teror	teror	k1gInSc1	teror
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
obětí	oběť	k1gFnSc7	oběť
byli	být	k5eAaImAgMnP	být
především	především	k9	především
jakobínští	jakobínský	k2eAgMnPc1d1	jakobínský
aktivisté	aktivista	k1gMnPc1	aktivista
<g/>
.	.	kIx.	.
</s>
