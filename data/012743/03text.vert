<p>
<s>
Guatemala	Guatemala	k1gFnSc1	Guatemala
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
sousedy	soused	k1gMnPc7	soused
jsou	být	k5eAaImIp3nP	být
Belize	Belize	k1gFnSc1	Belize
<g/>
,	,	kIx,	,
Salvador	Salvador	k1gInSc1	Salvador
<g/>
,	,	kIx,	,
Honduras	Honduras	k1gInSc1	Honduras
a	a	k8xC	a
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Guatemala	Guatemala	k1gFnSc1	Guatemala
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
indiánského	indiánský	k2eAgNnSc2d1	indiánské
slova	slovo	k1gNnSc2	slovo
v	v	k7c6	v
nahuatlu	nahuatlo	k1gNnSc6	nahuatlo
Cuauhtemallan	Cuauhtemallana	k1gFnPc2	Cuauhtemallana
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
znamená	znamenat	k5eAaImIp3nS	znamenat
místo	místo	k1gNnSc1	místo
(	(	kIx(	(
<g/>
země	zem	k1gFnPc1	zem
<g/>
)	)	kIx)	)
obklopené	obklopený	k2eAgInPc1d1	obklopený
stromy	strom	k1gInPc1	strom
(	(	kIx(	(
<g/>
lesy	les	k1gInPc1	les
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
téměř	téměř	k6eAd1	téměř
polovinu	polovina	k1gFnSc4	polovina
státu	stát	k1gInSc2	stát
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
tropické	tropický	k2eAgInPc1d1	tropický
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Guatemala	Guatemala	k1gFnSc1	Guatemala
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
větším	veliký	k2eAgInPc3d2	veliký
středoamerickým	středoamerický	k2eAgInPc3d1	středoamerický
státům	stát	k1gInPc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
středoamerické	středoamerický	k2eAgFnSc6d1	středoamerická
šíji	šíj	k1gFnSc6	šíj
mezi	mezi	k7c7	mezi
pobřežím	pobřeží	k1gNnSc7	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
Honduraským	honduraský	k2eAgInSc7d1	honduraský
zálivem	záliv	k1gInSc7	záliv
Karibského	karibský	k2eAgNnSc2d1	Karibské
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediný	jediný	k2eAgInSc1d1	jediný
stát	stát	k1gInSc1	stát
severní	severní	k2eAgFnSc2d1	severní
i	i	k8xC	i
střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třemi	tři	k4xCgInPc7	tři
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
nížinných	nížinný	k2eAgFnPc6d1	nížinná
oblastech	oblast	k1gFnPc6	oblast
Petén	Petén	k1gInSc1	Petén
a	a	k8xC	a
Izabal	Izabal	k1gInSc1	Izabal
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Guatemaly	Guatemala	k1gFnSc2	Guatemala
se	se	k3xPyFc4	se
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
rozkládalo	rozkládat	k5eAaImAgNnS	rozkládat
jádro	jádro	k1gNnSc1	jádro
osídlení	osídlení	k1gNnSc2	osídlení
rozvinuté	rozvinutý	k2eAgFnSc2d1	rozvinutá
Mayské	mayský	k2eAgFnSc2d1	mayská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Mayové	Mayová	k1gFnPc4	Mayová
se	se	k3xPyFc4	se
např.	např.	kA	např.
zabývali	zabývat	k5eAaImAgMnP	zabývat
astronomií	astronomie	k1gFnSc7	astronomie
a	a	k8xC	a
matematikou	matematika	k1gFnSc7	matematika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mj.	mj.	kA	mj.
zavedli	zavést	k5eAaPmAgMnP	zavést
pojem	pojem	k1gInSc4	pojem
nuly	nula	k1gFnSc2	nula
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
civilizace	civilizace	k1gFnSc1	civilizace
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
z	z	k7c2	z
dosud	dosud	k6eAd1	dosud
nejasných	jasný	k2eNgFnPc2d1	nejasná
příčin	příčina	k1gFnPc2	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Stavby	stavba	k1gFnPc1	stavba
Mayů	May	k1gMnPc2	May
včetně	včetně	k7c2	včetně
pyramid	pyramida	k1gFnPc2	pyramida
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
dnes	dnes	k6eAd1	dnes
navštívit	navštívit	k5eAaPmF	navštívit
např.	např.	kA	např.
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Tikal	tikal	k1gInSc1	tikal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1523	[number]	k4	1523
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
centrem	centrum	k1gNnSc7	centrum
mayské	mayský	k2eAgFnSc2d1	mayská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
rozvrátili	rozvrátit	k5eAaPmAgMnP	rozvrátit
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
.	.	kIx.	.
</s>
<s>
Podrobili	podrobit	k5eAaPmAgMnP	podrobit
si	se	k3xPyFc3	se
místní	místní	k2eAgNnSc4d1	místní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
a	a	k8xC	a
vládli	vládnout	k5eAaImAgMnP	vládnout
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1560	[number]	k4	1560
až	až	k9	až
1821	[number]	k4	1821
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
generálního	generální	k2eAgInSc2d1	generální
kapitanátu	kapitanát	k1gInSc2	kapitanát
Guatemala	Guatemala	k1gFnSc1	Guatemala
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1821	[number]	k4	1821
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc1	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
roku	rok	k1gInSc2	rok
1822	[number]	k4	1822
byla	být	k5eAaImAgFnS	být
Guatemala	Guatemala	k1gFnSc1	Guatemala
připojena	připojen	k2eAgFnSc1d1	připojena
k	k	k7c3	k
mexickému	mexický	k2eAgNnSc3d1	mexické
císařství	císařství	k1gNnSc3	císařství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1823	[number]	k4	1823
až	až	k9	až
1839	[number]	k4	1839
byla	být	k5eAaImAgFnS	být
členem	člen	k1gMnSc7	člen
federace	federace	k1gFnSc2	federace
Spojených	spojený	k2eAgFnPc2d1	spojená
středoamerických	středoamerický	k2eAgFnPc2d1	středoamerická
provincií	provincie	k1gFnPc2	provincie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
dostává	dostávat	k5eAaImIp3nS	dostávat
pod	pod	k7c4	pod
vliv	vliv	k1gInSc4	vliv
Spojených	spojený	k2eAgInPc2d1	spojený
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
United	United	k1gInSc1	United
Fruit	Fruita	k1gFnPc2	Fruita
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
výraznou	výrazný	k2eAgFnSc7d1	výrazná
měrou	míra	k1gFnSc7wR	míra
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
dění	dění	k1gNnSc6	dění
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
podporou	podpora	k1gFnSc7	podpora
jí	jíst	k5eAaImIp3nS	jíst
nakloněných	nakloněný	k2eAgMnPc2d1	nakloněný
prezidentů	prezident	k1gMnPc2	prezident
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
byl	být	k5eAaImAgInS	být
pučem	puč	k1gInSc7	puč
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
CIA	CIA	kA	CIA
svržen	svrhnout	k5eAaPmNgInS	svrhnout
ve	v	k7c6	v
svobodných	svobodný	k2eAgFnPc6d1	svobodná
volbách	volba	k1gFnPc6	volba
zvolený	zvolený	k2eAgInSc1d1	zvolený
Jacobo	Jacoba	k1gFnSc5	Jacoba
Árbenz	Árbenz	k1gInSc1	Árbenz
Guzmán	Guzmán	k2eAgInSc1d1	Guzmán
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Miguel	Miguel	k1gMnSc1	Miguel
Ydígoras	Ydígoras	k1gMnSc1	Ydígoras
Fuentes	Fuentes	k1gMnSc1	Fuentes
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
povolil	povolit	k5eAaPmAgInS	povolit
tréning	tréning	k1gInSc4	tréning
anti-castrovských	antiastrovský	k2eAgMnPc2d1	anti-castrovský
bojovníků	bojovník	k1gMnPc2	bojovník
pro	pro	k7c4	pro
invazi	invaze	k1gFnSc4	invaze
v	v	k7c6	v
Zátoce	zátoka	k1gFnSc6	zátoka
sviní	svině	k1gFnPc2	svině
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
mezi	mezi	k7c7	mezi
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
opozicí	opozice	k1gFnSc7	opozice
přerostl	přerůst	k5eAaPmAgMnS	přerůst
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
v	v	k7c4	v
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
vláda	vláda	k1gFnSc1	vláda
vycvičila	vycvičit	k5eAaPmAgFnS	vycvičit
eskadry	eskadra	k1gFnPc4	eskadra
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
likvidovat	likvidovat	k5eAaBmF	likvidovat
protivládní	protivládní	k2eAgFnPc4d1	protivládní
guerilly	guerilla	k1gFnPc4	guerilla
<g/>
.	.	kIx.	.
</s>
<s>
Volená	volený	k2eAgFnSc1d1	volená
vláda	vláda	k1gFnSc1	vláda
stát	stát	k1gInSc1	stát
spravuje	spravovat	k5eAaImIp3nS	spravovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
podmínky	podmínka	k1gFnPc1	podmínka
==	==	k?	==
</s>
</p>
<p>
<s>
Středem	středem	k7c2	středem
území	území	k1gNnSc2	území
Guatemaly	Guatemala	k1gFnSc2	Guatemala
prochází	procházet	k5eAaImIp3nS	procházet
hřbet	hřbet	k1gInSc1	hřbet
pohoří	pohoří	k1gNnSc2	pohoří
Sierra	Sierra	k1gFnSc1	Sierra
Madre	Madr	k1gInSc5	Madr
de	de	k?	de
Chiapas	Chiapas	k1gMnSc1	Chiapas
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zde	zde	k6eAd1	zde
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšek	výška	k1gFnPc2	výška
přes	přes	k7c4	přes
4000	[number]	k4	4000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
vrchol	vrchol	k1gInSc4	vrchol
Guatemaly	Guatemala	k1gFnSc2	Guatemala
sopka	sopka	k1gFnSc1	sopka
Tajumulco	Tajumulco	k6eAd1	Tajumulco
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Středoamerických	středoamerický	k2eAgFnPc2d1	středoamerická
Kordiller	Kordillery	k1gFnPc2	Kordillery
kromě	kromě	k7c2	kromě
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
ze	z	k7c2	z
zdejších	zdejší	k2eAgFnPc2d1	zdejší
sopek	sopka	k1gFnPc2	sopka
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
Acatenango	Acatenango	k6eAd1	Acatenango
a	a	k8xC	a
Fuego	Fuego	k6eAd1	Fuego
jsou	být	k5eAaImIp3nP	být
dosud	dosud	k6eAd1	dosud
činné	činný	k2eAgFnPc1d1	činná
<g/>
.	.	kIx.	.
</s>
<s>
Kužely	kužel	k1gInPc1	kužel
těch	ten	k3xDgMnPc2	ten
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
jsou	být	k5eAaImIp3nP	být
zčásti	zčásti	k6eAd1	zčásti
pokryté	pokrytý	k2eAgInPc1d1	pokrytý
sněhem	sníh	k1gInSc7	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Pacifické	pacifický	k2eAgNnSc4d1	pacifické
pobřeží	pobřeží	k1gNnSc4	pobřeží
Guatemaly	Guatemala	k1gFnSc2	Guatemala
lemují	lemovat	k5eAaImIp3nP	lemovat
poměrně	poměrně	k6eAd1	poměrně
úzké	úzký	k2eAgFnPc1d1	úzká
nížiny	nížina	k1gFnPc1	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
přechází	přecházet	k5eAaImIp3nS	přecházet
vápencová	vápencový	k2eAgFnSc1d1	vápencová
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
v	v	k7c4	v
bažinatou	bažinatý	k2eAgFnSc4d1	bažinatá
nížinu	nížina	k1gFnSc4	nížina
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jezera	jezero	k1gNnSc2	jezero
Petén	Petén	k1gInSc1	Petén
Itzá	Itzá	k1gFnSc1	Itzá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
na	na	k7c6	na
území	území	k1gNnSc6	území
Guatemaly	Guatemala	k1gFnSc2	Guatemala
Maya	Maya	k?	Maya
Mountains	Mountains	k1gInSc1	Mountains
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgFnPc7d3	nejvýznamnější
řekami	řeka	k1gFnPc7	řeka
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
Usumacinta	Usumacint	k1gInSc2	Usumacint
a	a	k8xC	a
Motagua	Motagua	k1gFnSc1	Motagua
ústící	ústící	k2eAgFnSc1d1	ústící
do	do	k7c2	do
Honduraského	honduraský	k2eAgInSc2d1	honduraský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
důležitá	důležitý	k2eAgNnPc4d1	důležité
jezera	jezero	k1gNnPc4	jezero
patří	patřit	k5eAaImIp3nS	patřit
Atitlán	Atitlán	k2eAgInSc1d1	Atitlán
<g/>
,	,	kIx,	,
Güija	Güija	k1gMnSc1	Güija
a	a	k8xC	a
Amatitlán	Amatitlán	k2eAgMnSc1d1	Amatitlán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
tropické	tropický	k2eAgNnSc1d1	tropické
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
okolo	okolo	k7c2	okolo
25	[number]	k4	25
°	°	k?	°
<g/>
C	C	kA	C
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
600	[number]	k4	600
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
do	do	k7c2	do
1800	[number]	k4	1800
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
14	[number]	k4	14
°	°	k?	°
<g/>
C	C	kA	C
do	do	k7c2	do
3200	[number]	k4	3200
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
výše	výše	k1gFnSc1	výše
nad	nad	k7c7	nad
3200	[number]	k4	3200
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
i	i	k9	i
pod	pod	k7c4	pod
10	[number]	k4	10
°	°	k?	°
<g/>
C.	C.	kA	C.
Svahy	svah	k1gInPc1	svah
vystavené	vystavená	k1gFnSc2	vystavená
působení	působení	k1gNnSc2	působení
atlantských	atlantský	k2eAgInPc2d1	atlantský
pasátových	pasátový	k2eAgInPc2d1	pasátový
větrů	vítr	k1gInPc2	vítr
mají	mít	k5eAaImIp3nP	mít
roční	roční	k2eAgInPc1d1	roční
úhrny	úhrn	k1gInPc1	úhrn
srážek	srážka	k1gFnPc2	srážka
vyšší	vysoký	k2eAgFnSc7d2	vyšší
než	než	k8xS	než
5000	[number]	k4	5000
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
jsou	být	k5eAaImIp3nP	být
srážky	srážka	k1gFnPc1	srážka
nižší	nízký	k2eAgFnPc1d2	nižší
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
500	[number]	k4	500
<g/>
–	–	k?	–
<g/>
1200	[number]	k4	1200
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
21	[number]	k4	21
guatemalských	guatemalský	k2eAgInPc2d1	guatemalský
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
má	mít	k5eAaImIp3nS	mít
souhrnnou	souhrnný	k2eAgFnSc4d1	souhrnná
rozlohu	rozloha	k1gFnSc4	rozloha
736	[number]	k4	736
634	[number]	k4	634
hektarů	hektar	k1gInPc2	hektar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
6,76	[number]	k4	6,76
%	%	kIx~	%
státní	státní	k2eAgFnSc2d1	státní
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
==	==	k?	==
Správní	správní	k2eAgNnSc4d1	správní
členění	členění	k1gNnSc4	členění
==	==	k?	==
</s>
</p>
<p>
<s>
Guatemala	Guatemala	k1gFnSc1	Guatemala
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
8	[number]	k4	8
regionů	region	k1gInPc2	region
(	(	kIx(	(
<g/>
regiones	regiones	k1gInSc1	regiones
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
22	[number]	k4	22
departementů	departement	k1gInPc2	departement
(	(	kIx(	(
<g/>
departamentos	departamentos	k1gInSc1	departamentos
<g/>
)	)	kIx)	)
a	a	k8xC	a
332	[number]	k4	332
obcí	obec	k1gFnPc2	obec
(	(	kIx(	(
<g/>
municipios	municipios	k1gInSc1	municipios
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
do	do	k7c2	do
regionů	region	k1gInPc2	region
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
informativní	informativní	k2eAgFnSc4d1	informativní
<g/>
,	,	kIx,	,
administrativní	administrativní	k2eAgFnSc4d1	administrativní
správu	správa	k1gFnSc4	správa
území	území	k1gNnSc2	území
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
departementy	departement	k1gInPc1	departement
Guatemaly	Guatemala	k1gFnSc2	Guatemala
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
Guatemala	Guatemala	k1gFnSc1	Guatemala
je	být	k5eAaImIp3nS	být
rozvojový	rozvojový	k2eAgInSc1d1	rozvojový
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
těží	těžet	k5eAaImIp3nS	těžet
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
antimon	antimon	k1gInSc1	antimon
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
odvětví	odvětví	k1gNnPc1	odvětví
jsou	být	k5eAaImIp3nP	být
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc4d1	chemický
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInSc4d1	textilní
<g/>
,	,	kIx,	,
papírenský	papírenský	k2eAgInSc4d1	papírenský
a	a	k8xC	a
dřevozpracující	dřevozpracující	k2eAgInSc4d1	dřevozpracující
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
měrou	míra	k1gFnSc7wR	míra
se	se	k3xPyFc4	se
na	na	k7c6	na
výrobě	výroba	k1gFnSc6	výroba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
podílejí	podílet	k5eAaImIp3nP	podílet
vodní	vodní	k2eAgFnPc1d1	vodní
elektrárny	elektrárna	k1gFnPc1	elektrárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
vysočině	vysočina	k1gFnSc6	vysočina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
plodina	plodina	k1gFnSc1	plodina
-	-	kIx~	-
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
<g/>
,	,	kIx,	,
banány	banán	k1gInPc1	banán
<g/>
,	,	kIx,	,
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
kakao	kakao	k1gNnSc1	kakao
<g/>
,	,	kIx,	,
citrusy	citrus	k1gInPc1	citrus
a	a	k8xC	a
ananasy	ananas	k1gInPc1	ananas
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
příjmu	příjem	k1gInSc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Farmy	farma	k1gFnPc1	farma
jsou	být	k5eAaImIp3nP	být
soustředěny	soustředit	k5eAaPmNgFnP	soustředit
především	především	k9	především
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
živočišnou	živočišný	k2eAgFnSc4d1	živočišná
výrobu	výroba	k1gFnSc4	výroba
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
chov	chov	k1gInSc4	chov
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
také	také	k9	také
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
těžba	těžba	k1gFnSc1	těžba
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
mahagonu	mahagon	k1gInSc2	mahagon
a	a	k8xC	a
cedru	cedr	k1gInSc2	cedr
<g/>
,	,	kIx,	,
a	a	k8xC	a
sběr	sběr	k1gInSc4	sběr
čikle	čikle	k1gFnSc2	čikle
a	a	k8xC	a
kaučuku	kaučuk	k1gInSc2	kaučuk
<g/>
.	.	kIx.	.
</s>
<s>
Mírné	mírný	k2eAgNnSc1d1	mírné
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
úrodná	úrodný	k2eAgFnSc1d1	úrodná
půda	půda	k1gFnSc1	půda
a	a	k8xC	a
dostatek	dostatek	k1gInSc1	dostatek
srážek	srážka	k1gFnPc2	srážka
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
Guatemale	Guatemala	k1gFnSc6	Guatemala
přinést	přinést	k5eAaPmF	přinést
prosperitu	prosperita	k1gFnSc4	prosperita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
země	země	k1gFnSc1	země
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
kruté	krutý	k2eAgFnSc3d1	krutá
historii	historie	k1gFnSc3	historie
chudá	chudý	k2eAgFnSc1d1	chudá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Guatemala	Guatemala	k1gFnSc1	Guatemala
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
DR-CAFTA	DR-CAFTA	k1gFnSc2	DR-CAFTA
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
hodnoty	hodnota	k1gFnSc2	hodnota
4	[number]	k4	4
831	[number]	k4	831
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
vývozní	vývozní	k2eAgInPc4d1	vývozní
artikly	artikl	k1gInPc4	artikl
patří	patřit	k5eAaImIp3nS	patřit
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
cukr	cukr	k1gInSc1	cukr
<g/>
,	,	kIx,	,
banány	banán	k1gInPc1	banán
<g/>
,	,	kIx,	,
koření	koření	k1gNnSc1	koření
(	(	kIx(	(
<g/>
muškátový	muškátový	k2eAgInSc1d1	muškátový
oříšek	oříšek	k1gInSc1	oříšek
<g/>
,	,	kIx,	,
kardamom	kardamom	k1gInSc1	kardamom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
palmový	palmový	k2eAgInSc1d1	palmový
olej	olej	k1gInSc1	olej
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nezemědělskými	zemědělský	k2eNgInPc7d1	nezemědělský
produkty	produkt	k1gInPc7	produkt
dominují	dominovat	k5eAaImIp3nP	dominovat
surová	surový	k2eAgFnSc1d1	surová
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
medikamenty	medikament	k1gInPc1	medikament
a	a	k8xC	a
kosmetické	kosmetický	k2eAgInPc1d1	kosmetický
přípravky	přípravek	k1gInPc1	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Guatemala	Guatemala	k1gFnSc1	Guatemala
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
řídkou	řídký	k2eAgFnSc7d1	řídká
silniční	silniční	k2eAgFnSc7d1	silniční
i	i	k8xC	i
železniční	železniční	k2eAgFnSc7d1	železniční
sítí	síť	k1gFnSc7	síť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
středoamerických	středoamerický	k2eAgInPc2d1	středoamerický
států	stát	k1gInPc2	stát
má	mít	k5eAaImIp3nS	mít
Guatemala	Guatemala	k1gFnSc1	Guatemala
největší	veliký	k2eAgFnSc1d3	veliký
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
Guatemala	Guatemala	k1gFnSc1	Guatemala
<g/>
.	.	kIx.	.
</s>
<s>
Guatemala	Guatemala	k1gFnSc1	Guatemala
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
svými	svůj	k3xOyFgFnPc7	svůj
památkami	památka	k1gFnPc7	památka
dávných	dávný	k2eAgFnPc2d1	dávná
civilizací	civilizace	k1gFnPc2	civilizace
<g/>
,	,	kIx,	,
velkolepými	velkolepý	k2eAgFnPc7d1	velkolepá
slavnostmi	slavnost	k1gFnPc7	slavnost
a	a	k8xC	a
hudbou	hudba	k1gFnSc7	hudba
marimba	marimb	k1gMnSc2	marimb
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
nástroj	nástroj	k1gInSc4	nástroj
podobný	podobný	k2eAgInSc4d1	podobný
xylofonu	xylofon	k1gInSc3	xylofon
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
polovinu	polovina	k1gFnSc4	polovina
obyvatel	obyvatel	k1gMnPc2	obyvatel
tvoří	tvořit	k5eAaImIp3nP	tvořit
potomci	potomek	k1gMnPc1	potomek
dávných	dávný	k2eAgInPc2d1	dávný
Mayů	May	k1gMnPc2	May
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
velmi	velmi	k6eAd1	velmi
chudí	chudý	k2eAgMnPc1d1	chudý
a	a	k8xC	a
pracují	pracovat	k5eAaImIp3nP	pracovat
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
převažují	převažovat	k5eAaImIp3nP	převažovat
ladinos	ladinos	k1gInSc4	ladinos
(	(	kIx(	(
<g/>
míšenci	míšenec	k1gMnSc3	míšenec
a	a	k8xC	a
v	v	k7c6	v
populace	populace	k1gFnSc1	populace
ze	z	k7c2	z
původu	původ	k1gInSc2	původ
Evropské	evropský	k2eAgFnSc2d1	Evropská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
řídí	řídit	k5eAaImIp3nP	řídit
správu	správa	k1gFnSc4	správa
státu	stát	k1gInSc2	stát
a	a	k8xC	a
ovládají	ovládat	k5eAaImIp3nP	ovládat
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jazyky	jazyk	k1gInPc1	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
úřední	úřední	k2eAgFnPc4d1	úřední
španělštiny	španělština	k1gFnPc4	španělština
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mluví	mluvit	k5eAaImIp3nS	mluvit
ještě	ještě	k9	ještě
těmito	tento	k3xDgInPc7	tento
jazyky	jazyk	k1gInPc7	jazyk
<g/>
:	:	kIx,	:
Acateco	Acateco	k1gMnSc1	Acateco
<g/>
,	,	kIx,	,
Aguatepeco	Aguatepeco	k1gMnSc1	Aguatepeco
<g/>
,	,	kIx,	,
kakčikelština	kakčikelština	k1gFnSc1	kakčikelština
(	(	kIx(	(
<g/>
Cakchiquel	Cakchiquel	k1gInSc1	Cakchiquel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Garífuna	Garífuna	k1gFnSc1	Garífuna
<g/>
,	,	kIx,	,
Huasteca	Huasteca	k1gFnSc1	Huasteca
<g/>
,	,	kIx,	,
Chicomucelteco	Chicomucelteco	k1gNnSc1	Chicomucelteco
<g/>
,	,	kIx,	,
čolština	čolština	k1gFnSc1	čolština
(	(	kIx(	(
<g/>
Chol	Chol	k1gInSc1	Chol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Choltí	Choltí	k1gNnSc1	Choltí
<g/>
,	,	kIx,	,
Chontal	Chontal	k1gMnSc1	Chontal
<g/>
,	,	kIx,	,
Chortí	Chortí	k1gMnSc1	Chortí
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
čujština	čujština	k1gFnSc1	čujština
(	(	kIx(	(
<g/>
Chuj	Chuj	k1gInSc1	Chuj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Itzá	Itzá	k1gFnSc1	Itzá
<g/>
,	,	kIx,	,
Ixil	Ixil	k1gMnSc1	Ixil
<g/>
,	,	kIx,	,
Jacalteco	Jacalteco	k1gMnSc1	Jacalteco
<g/>
,	,	kIx,	,
Kanjobal	Kanjobal	k1gMnSc1	Kanjobal
<g/>
,	,	kIx,	,
Kekchí	Kekchí	k1gMnSc1	Kekchí
<g/>
,	,	kIx,	,
lakandonština	lakandonština	k1gFnSc1	lakandonština
(	(	kIx(	(
<g/>
Lacandon	Lacandon	k1gInSc1	Lacandon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mamesština	mamesština	k1gFnSc1	mamesština
(	(	kIx(	(
<g/>
Mam	mam	k1gInSc1	mam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mopán	Mopán	k1gMnSc1	Mopán
<g/>
,	,	kIx,	,
Motozintleco	Motozintleco	k1gMnSc1	Motozintleco
<g/>
,	,	kIx,	,
Pokoman	Pokoman	k1gMnSc1	Pokoman
<g/>
,	,	kIx,	,
Pokomchí	Pokomchí	k1gMnSc1	Pokomchí
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
kyčéština	kyčéština	k1gFnSc1	kyčéština
(	(	kIx(	(
<g/>
Quiché	Quichý	k2eAgNnSc1d1	Quiché
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sacapulteco	Sacapulteco	k1gNnSc1	Sacapulteco
<g/>
,	,	kIx,	,
Sipacapa	Sipacapa	k1gFnSc1	Sipacapa
<g/>
,	,	kIx,	,
Teco	Teco	k1gNnSc1	Teco
<g/>
,	,	kIx,	,
tocholobalština	tocholobalština	k1gFnSc1	tocholobalština
(	(	kIx(	(
<g/>
Tojolabal	Tojolabal	k1gInSc1	Tojolabal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzeltalština	tzeltalština	k1gFnSc1	tzeltalština
(	(	kIx(	(
<g/>
Tzeltal	Tzeltal	k1gMnSc1	Tzeltal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzotzilština	tzotzilština	k1gFnSc1	tzotzilština
(	(	kIx(	(
<g/>
Tzotzil	Tzotzil	k1gFnSc1	Tzotzil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tzutuhil	Tzutuhil	k1gMnSc1	Tzutuhil
<g/>
,	,	kIx,	,
Uspanteco	Uspanteco	k1gMnSc1	Uspanteco
a	a	k8xC	a
Yucateco	Yucateco	k1gMnSc1	Yucateco
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Náboženství	náboženství	k1gNnSc1	náboženství
většiny	většina	k1gFnSc2	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
katolické	katolický	k2eAgNnSc1d1	katolické
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
snižuje	snižovat	k5eAaImIp3nS	snižovat
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
růstu	růst	k1gInSc2	růst
protestantsví	protestantsvět	k5eAaImIp3nS	protestantsvět
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
51	[number]	k4	51
%	%	kIx~	%
katolíků	katolík	k1gMnPc2	katolík
a	a	k8xC	a
36	[number]	k4	36
%	%	kIx~	%
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
.	.	kIx.	.
</s>
<s>
Praktici	praktik	k1gMnPc1	praktik
mayských	mayský	k2eAgNnPc2d1	mayské
náboženství	náboženství	k1gNnPc2	náboženství
mají	mít	k5eAaImIp3nP	mít
3	[number]	k4	3
%	%	kIx~	%
<g/>
,	,	kIx,	,
dalším	další	k2eAgNnSc7d1	další
místním	místní	k2eAgNnSc7d1	místní
náboženstvím	náboženství	k1gNnSc7	náboženství
je	být	k5eAaImIp3nS	být
synkretismus	synkretismus	k1gInSc1	synkretismus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mayské	mayský	k2eAgInPc1d1	mayský
rituály	rituál	k1gInPc1	rituál
mísí	mísit	k5eAaImIp3nP	mísit
s	s	k7c7	s
katolickými	katolický	k2eAgMnPc7d1	katolický
<g/>
.	.	kIx.	.
10	[number]	k4	10
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
bez	bez	k7c2	bez
náboženského	náboženský	k2eAgNnSc2d1	náboženské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Populace	populace	k1gFnSc1	populace
Ladinos	Ladinosa	k1gFnPc2	Ladinosa
praktikuje	praktikovat	k5eAaImIp3nS	praktikovat
katolickou	katolický	k2eAgFnSc4d1	katolická
víru	víra	k1gFnSc4	víra
svého	svůj	k3xOyFgNnSc2	svůj
španělského	španělský	k2eAgNnSc2d1	španělské
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
25	[number]	k4	25
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Guatemaly	Guatemala	k1gFnSc2	Guatemala
byli	být	k5eAaImAgMnP	být
evangelíci	evangelík	k1gMnPc1	evangelík
a	a	k8xC	a
katolíci	katolík	k1gMnPc1	katolík
tvořili	tvořit	k5eAaImAgMnP	tvořit
65	[number]	k4	65
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Mayské	mayský	k2eAgInPc4d1	mayský
rituály	rituál	k1gInPc4	rituál
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
jejich	jejich	k3xOp3gFnSc1	jejich
práva	práv	k2eAgFnSc1d1	práva
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
nedávné	dávný	k2eNgFnPc1d1	nedávná
statistiky	statistika	k1gFnPc1	statistika
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Guatemalaňané	Guatemalaňan	k1gMnPc1	Guatemalaňan
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
vzrostli	vzrůst	k5eAaPmAgMnP	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
země	zem	k1gFnSc2	zem
již	již	k6eAd1	již
nemá	mít	k5eNaImIp3nS	mít
oficiální	oficiální	k2eAgNnSc4d1	oficiální
náboženství	náboženství	k1gNnSc4	náboženství
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
katolické	katolický	k2eAgNnSc1d1	katolické
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Popol	Popol	k1gInSc1	Popol
Vuh	Vuh	k1gFnSc2	Vuh
</s>
</p>
<p>
<s>
Hispanoamerika	Hispanoamerika	k1gFnSc1	Hispanoamerika
</s>
</p>
<p>
<s>
Středoamerické	středoamerický	k2eAgFnPc1d1	středoamerická
a	a	k8xC	a
karibské	karibský	k2eAgFnPc1d1	karibská
hry	hra	k1gFnPc1	hra
</s>
</p>
<p>
<s>
Středoamerický	středoamerický	k2eAgInSc1d1	středoamerický
integrační	integrační	k2eAgInSc1d1	integrační
systém	systém	k1gInSc1	systém
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Guatemala	Guatemala	k1gFnSc1	Guatemala
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Guatemala	Guatemala	k1gFnSc1	Guatemala
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Guatemala	Guatemala	k1gFnSc1	Guatemala
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
</p>
<p>
<s>
LatinskaAmerikaDnes	LatinskaAmerikaDnes	k1gInSc1	LatinskaAmerikaDnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Guatemala	Guatemala	k1gFnSc1	Guatemala
(	(	kIx(	(
<g/>
informace	informace	k1gFnSc1	informace
<g/>
,	,	kIx,	,
reportáž	reportáž	k1gFnSc1	reportáž
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc1	historie
<g/>
)	)	kIx)	)
na	na	k7c6	na
webu	web	k1gInSc6	web
Latinská	latinský	k2eAgFnSc1d1	Latinská
Amerika	Amerika	k1gFnSc1	Amerika
Dnes	dnes	k6eAd1	dnes
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
guatemalské	guatemalský	k2eAgFnSc6d1	guatemalská
literatuře	literatura	k1gFnSc6	literatura
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
guatemalském	guatemalský	k2eAgNnSc6d1	Guatemalské
umění	umění	k1gNnSc6	umění
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Foto	foto	k1gNnSc1	foto
Guatemala	Guatemala	k1gFnSc1	Guatemala
</s>
</p>
<p>
<s>
Guatemala	Guatemala	k1gFnSc1	Guatemala
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Guatemala	Guatemala	k1gFnSc1	Guatemala
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Guatemala	Guatemala	k1gFnSc1	Guatemala
Country	country	k2eAgFnSc1d1	country
Report	report	k1gInSc1	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
Western	western	k1gInSc1	western
Hemispehere	Hemispeher	k1gInSc5	Hemispeher
Affairs	Affairsa	k1gFnPc2	Affairsa
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Guatemala	Guatemala	k1gFnSc1	Guatemala
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-07-27	[number]	k4	2011-07-27
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Guatemala	Guatemala	k1gFnSc1	Guatemala
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Guatemala	Guatemala	k1gFnSc1	Guatemala
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-04-05	[number]	k4	2011-04-05
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ANDERSON	Anderson	k1gMnSc1	Anderson
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
P	P	kA	P
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Guatemala	Guatemala	k1gFnSc1	Guatemala
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
