<s>
Newton	Newton	k1gMnSc1	Newton
zavedl	zavést	k5eAaPmAgMnS	zavést
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgInPc1	tři
pohybové	pohybový	k2eAgInPc1d1	pohybový
zákony	zákon	k1gInPc1	zákon
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
základ	základ	k1gInSc4	základ
klasické	klasický	k2eAgFnSc2d1	klasická
mechaniky	mechanika	k1gFnSc2	mechanika
a	a	k8xC	a
zejména	zejména	k9	zejména
dynamiky	dynamika	k1gFnSc2	dynamika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
příčiny	příčina	k1gFnPc1	příčina
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
