<p>
<s>
Estimated	Estimated	k1gMnSc1	Estimated
time	tim	k1gFnSc2	tim
of	of	k?	of
arrival	arrivat	k5eAaPmAgMnS	arrivat
(	(	kIx(	(
<g/>
zkratkou	zkratka	k1gFnSc7	zkratka
ETA	ETA	kA	ETA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
anglický	anglický	k2eAgInSc4d1	anglický
termín	termín	k1gInSc4	termín
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
přibližný	přibližný	k2eAgInSc4d1	přibližný
čas	čas	k1gInSc4	čas
příjezdu	příjezd	k1gInSc2	příjezd
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
odhadovaný	odhadovaný	k2eAgInSc4d1	odhadovaný
čas	čas	k1gInSc4	čas
příjezdu	příjezd	k1gInSc2	příjezd
dopravního	dopravní	k2eAgInSc2d1	dopravní
prostředku	prostředek	k1gInSc2	prostředek
(	(	kIx(	(
<g/>
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
automobil	automobil	k1gInSc1	automobil
<g/>
,	,	kIx,	,
letadlo	letadlo	k1gNnSc1	letadlo
<g/>
,	,	kIx,	,
vlak	vlak	k1gInSc1	vlak
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nákladu	náklad	k1gInSc2	náklad
<g/>
,	,	kIx,	,
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
pomoci	pomoc	k1gFnSc2	pomoc
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
složek	složka	k1gFnPc2	složka
<g/>
,	,	kIx,	,
dopravců	dopravce	k1gMnPc2	dopravce
nebo	nebo	k8xC	nebo
v	v	k7c6	v
letecké	letecký	k2eAgFnSc6d1	letecká
dopravě	doprava	k1gFnSc6	doprava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
IT.	IT.	k1gFnSc2	IT.
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
IT	IT	kA	IT
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
přesunu	přesun	k1gInSc6	přesun
<g/>
/	/	kIx~	/
<g/>
přenosu	přenos	k1gInSc2	přenos
počítačových	počítačový	k2eAgInPc2d1	počítačový
souborů	soubor	k1gInPc2	soubor
a	a	k8xC	a
složek	složka	k1gFnPc2	složka
a	a	k8xC	a
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
tak	tak	k6eAd1	tak
odhadovaný	odhadovaný	k2eAgInSc4d1	odhadovaný
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bude	být	k5eAaImBp3nS	být
operace	operace	k1gFnSc1	operace
přesunu	přesun	k1gInSc2	přesun
<g/>
/	/	kIx~	/
<g/>
přenosu	přenos	k1gInSc2	přenos
nad	nad	k7c7	nad
daným	daný	k2eAgInSc7d1	daný
souborem	soubor	k1gInSc7	soubor
<g/>
/	/	kIx~	/
<g/>
složkou	složka	k1gFnSc7	složka
dokončena	dokončit	k5eAaPmNgFnS	dokončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zkratka	zkratka	k1gFnSc1	zkratka
ETA	ETA	kA	ETA
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rovněž	rovněž	k9	rovněž
použita	použít	k5eAaPmNgFnS	použít
například	například	k6eAd1	například
v	v	k7c6	v
situacích	situace	k1gFnPc6	situace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
fyzický	fyzický	k2eAgInSc4d1	fyzický
přenos	přenos	k1gInSc4	přenos
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
tak	tak	k6eAd1	tak
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
odhadovaný	odhadovaný	k2eAgInSc4d1	odhadovaný
zbývající	zbývající	k2eAgInSc4d1	zbývající
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
nutný	nutný	k2eAgInSc4d1	nutný
k	k	k7c3	k
dokončení	dokončení	k1gNnSc3	dokončení
nějaké	nějaký	k3yIgFnSc2	nějaký
činnosti	činnost	k1gFnSc2	činnost
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
práce	práce	k1gFnSc1	práce
<g/>
/	/	kIx~	/
<g/>
úkol	úkol	k1gInSc1	úkol
vykonávající	vykonávající	k2eAgInSc1d1	vykonávající
nějakou	nějaký	k3yIgFnSc7	nějaký
osobou	osoba	k1gFnSc7	osoba
<g/>
,	,	kIx,	,
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
operace	operace	k1gFnSc2	operace
vykonávané	vykonávaný	k2eAgNnSc1d1	vykonávané
počítačovým	počítačový	k2eAgInSc7d1	počítačový
programem	program	k1gInSc7	program
nebo	nebo	k8xC	nebo
průběh	průběh	k1gInSc4	průběh
činnosti	činnost	k1gFnSc2	činnost
na	na	k7c6	na
úkolu	úkol	k1gInSc6	úkol
zastřešovaný	zastřešovaný	k2eAgMnSc1d1	zastřešovaný
nějakou	nějaký	k3yIgFnSc7	nějaký
organizací	organizace	k1gFnSc7	organizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
klienti	klient	k1gMnPc1	klient
počítačového	počítačový	k2eAgInSc2d1	počítačový
protokolu	protokol	k1gInSc2	protokol
BitTorrent	BitTorrent	k1gInSc1	BitTorrent
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
pomocí	pomocí	k7c2	pomocí
ETA	ETA	kA	ETA
očekávaný	očekávaný	k2eAgInSc1d1	očekávaný
čas	čas	k1gInSc1	čas
dokončení	dokončení	k1gNnSc2	dokončení
stahování	stahování	k1gNnSc2	stahování
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
souboru	soubor	k1gInSc2	soubor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
anglický	anglický	k2eAgInSc4d1	anglický
termín	termín	k1gInSc4	termín
<g/>
,	,	kIx,	,
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
užívání	užívání	k1gNnSc2	užívání
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
moc	moc	k6eAd1	moc
neuchytilo	uchytit	k5eNaPmAgNnS	uchytit
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
angličtina	angličtina	k1gFnSc1	angličtina
není	být	k5eNaImIp3nS	být
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
právě	právě	k9	právě
letiště	letiště	k1gNnSc4	letiště
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
