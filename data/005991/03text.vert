<s>
Chlad	chlad	k1gInSc1	chlad
je	být	k5eAaImIp3nS	být
pocit	pocit	k1gInSc4	pocit
zimy	zima	k1gFnSc2	zima
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
cítí	cítit	k5eAaImIp3nS	cítit
organismus	organismus	k1gInSc1	organismus
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
teplotě	teplota	k1gFnSc6	teplota
výrazně	výrazně	k6eAd1	výrazně
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
jeho	jeho	k3xOp3gNnPc2	jeho
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Organismus	organismus	k1gInSc1	organismus
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
snaží	snažit	k5eAaImIp3nP	snažit
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
pocitem	pocit	k1gInSc7	pocit
bojovat	bojovat	k5eAaImF	bojovat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vysílení	vysílení	k1gNnSc3	vysílení
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc3	jeho
útlumu	útlum	k1gInSc3	útlum
a	a	k8xC	a
v	v	k7c6	v
extrémním	extrémní	k2eAgInSc6d1	extrémní
případě	případ	k1gInSc6	případ
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
i	i	k9	i
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
snášet	snášet	k5eAaImF	snášet
jiné	jiný	k2eAgFnPc4d1	jiná
teploty	teplota	k1gFnPc4	teplota
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
bylo	být	k5eAaImAgNnS	být
kritické	kritický	k2eAgNnSc1d1	kritické
<g/>
.	.	kIx.	.
</s>
<s>
Vnímání	vnímání	k1gNnSc1	vnímání
chladu	chlad	k1gInSc2	chlad
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
pomocí	pomocí	k7c2	pomocí
termoreceptorů	termoreceptor	k1gInPc2	termoreceptor
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
chlad	chlad	k1gInSc4	chlad
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
postavení	postavení	k1gNnSc3	postavení
ochlupení	ochlupení	k1gNnSc2	ochlupení
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
husí	husí	k2eAgFnSc4d1	husí
kůži	kůže	k1gFnSc4	kůže
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
izolační	izolační	k2eAgFnSc1d1	izolační
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
zadržující	zadržující	k2eAgInSc1d1	zadržující
teplejší	teplý	k2eAgInSc1d2	teplejší
vzduch	vzduch	k1gInSc1	vzduch
blíže	blíž	k1gFnSc2	blíž
k	k	k7c3	k
tělu	tělo	k1gNnSc3	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
evoluční	evoluční	k2eAgInSc4d1	evoluční
relikt	relikt	k1gInSc4	relikt
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měl	mít	k5eAaImAgMnS	mít
člověk	člověk	k1gMnSc1	člověk
hustší	hustý	k2eAgNnSc4d2	hustší
ochlupení	ochlupení	k1gNnSc4	ochlupení
<g/>
,	,	kIx,	,
u	u	k7c2	u
současného	současný	k2eAgMnSc2d1	současný
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
izolační	izolační	k2eAgInSc1d1	izolační
efekt	efekt	k1gInSc1	efekt
poměrně	poměrně	k6eAd1	poměrně
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
.	.	kIx.	.
</s>
<s>
Husí	husí	k2eAgFnSc1d1	husí
kůže	kůže	k1gFnSc1	kůže
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
také	také	k9	také
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
silného	silný	k2eAgNnSc2d1	silné
vzrušení	vzrušení	k1gNnSc2	vzrušení
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
strachu	strach	k1gInSc2	strach
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pocit	pocit	k1gInSc1	pocit
chladu	chlad	k1gInSc2	chlad
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
bolestivému	bolestivý	k2eAgInSc3d1	bolestivý
svalovému	svalový	k2eAgInSc3d1	svalový
třasu	třas	k1gInSc3	třas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
tělo	tělo	k1gNnSc1	tělo
snaží	snažit	k5eAaImIp3nS	snažit
pohybem	pohyb	k1gInSc7	pohyb
získávat	získávat	k5eAaImF	získávat
teplo	teplo	k1gNnSc4	teplo
pro	pro	k7c4	pro
zahřátí	zahřátí	k1gNnSc4	zahřátí
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rychlému	rychlý	k2eAgNnSc3d1	rychlé
spotřebovávání	spotřebovávání	k1gNnSc3	spotřebovávání
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
oslabováním	oslabování	k1gNnSc7	oslabování
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
teplota	teplota	k1gFnSc1	teplota
člověka	člověk	k1gMnSc2	člověk
klesne	klesnout	k5eAaPmIp3nS	klesnout
pod	pod	k7c4	pod
30	[number]	k4	30
°	°	k?	°
<g/>
C	C	kA	C
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Lokálním	lokální	k2eAgInSc7d1	lokální
projevem	projev	k1gInSc7	projev
chladu	chlad	k1gInSc2	chlad
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc1	vznik
omrzlin	omrzlina	k1gFnPc2	omrzlina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nejčastěji	často	k6eAd3	často
vznikají	vznikat	k5eAaImIp3nP	vznikat
na	na	k7c4	na
periferii	periferie	k1gFnSc4	periferie
organismu	organismus	k1gInSc2	organismus
(	(	kIx(	(
<g/>
prsty	prst	k1gInPc4	prst
<g/>
,	,	kIx,	,
ruce	ruka	k1gFnPc4	ruka
<g/>
,	,	kIx,	,
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
nos	nos	k1gInSc1	nos
<g/>
,	,	kIx,	,
uši	ucho	k1gNnPc1	ucho
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
podchlazení	podchlazení	k1gNnSc2	podchlazení
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
postupně	postupně	k6eAd1	postupně
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
extrémní	extrémní	k2eAgInSc1d1	extrémní
přesun	přesun	k1gInSc1	přesun
do	do	k7c2	do
vyhřáté	vyhřátý	k2eAgFnSc2d1	vyhřátá
místnosti	místnost	k1gFnSc2	místnost
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
forma	forma	k1gFnSc1	forma
první	první	k4xOgFnSc2	první
pomoci	pomoc	k1gFnSc2	pomoc
zcela	zcela	k6eAd1	zcela
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
<g/>
.	.	kIx.	.
</s>
<s>
Organismy	organismus	k1gInPc1	organismus
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
chladu	chlad	k1gInSc3	chlad
aktivně	aktivně	k6eAd1	aktivně
chrání	chránit	k5eAaImIp3nS	chránit
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
pomocí	pomocí	k7c2	pomocí
srsti	srst	k1gFnSc2	srst
<g/>
,	,	kIx,	,
ochlupení	ochlupení	k1gNnSc6	ochlupení
<g/>
,	,	kIx,	,
choulením	choulení	k1gNnSc7	choulení
či	či	k8xC	či
v	v	k7c6	v
případě	případ	k1gInSc6	případ
člověka	člověk	k1gMnSc2	člověk
oblečením	oblečení	k1gNnSc7	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Kůže	kůže	k1gFnSc1	kůže
Husí	husí	k2eAgFnSc2d1	husí
kůže	kůže	k1gFnSc2	kůže
Zima	zima	k6eAd1	zima
Hmat	hmat	k1gInSc4	hmat
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
chlad	chlad	k1gInSc1	chlad
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
chlad	chlad	k1gInSc1	chlad
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
