<s>
Chlad	chlad	k1gInSc1	chlad
je	být	k5eAaImIp3nS	být
pocit	pocit	k1gInSc4	pocit
zimy	zima	k1gFnSc2	zima
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
cítí	cítit	k5eAaImIp3nS	cítit
organismus	organismus	k1gInSc1	organismus
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
teplotě	teplota	k1gFnSc6	teplota
výrazně	výrazně	k6eAd1	výrazně
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
jeho	jeho	k3xOp3gNnPc2	jeho
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
