<p>
<s>
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
z	z	k7c2	z
Mlčící	mlčící	k2eAgFnSc2d1	mlčící
plantety	planteta	k1gFnSc2	planteta
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
knihou	kniha	k1gFnSc7	kniha
z	z	k7c2	z
Kosmické	kosmický	k2eAgFnSc2d1	kosmická
trilogie	trilogie	k1gFnSc2	trilogie
britsko-irského	britskorský	k2eAgMnSc2d1	britsko-irský
autora	autor	k1gMnSc2	autor
C.	C.	kA	C.
S.	S.	kA	S.
Lewise	Lewise	k1gFnSc1	Lewise
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
sci-fi	scii	k1gFnSc6	sci-fi
román	román	k1gInSc4	román
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
vlivem	vliv	k1gInSc7	vliv
náboženské	náboženský	k2eAgFnSc2d1	náboženská
tematiky	tematika	k1gFnSc2	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
The	The	k1gMnSc2	The
Bodley	Bodlea	k1gMnSc2	Bodlea
Head	Head	k1gMnSc1	Head
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
postavy	postava	k1gFnPc1	postava
==	==	k?	==
</s>
</p>
<p>
<s>
Elwin	Elwin	k2eAgInSc1d1	Elwin
Ransom	Ransom	k1gInSc1	Ransom
</s>
</p>
<p>
<s>
profesor	profesor	k1gMnSc1	profesor
Weston	Weston	k1gInSc4	Weston
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Devine	Devin	k1gMnSc5	Devin
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kosmická	kosmický	k2eAgFnSc1d1	kosmická
trilogie	trilogie	k1gFnSc1	trilogie
</s>
</p>
<p>
<s>
Perelandra	Perelandra	k1gFnSc1	Perelandra
</s>
</p>
<p>
<s>
Ta	ten	k3xDgFnSc1	ten
obludná	obludný	k2eAgFnSc1d1	obludná
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
<s>
Clive	Clivat	k5eAaPmIp3nS	Clivat
Staples	Staples	k1gInSc1	Staples
Lewis	Lewis	k1gFnSc2	Lewis
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Out	Out	k1gFnSc2	Out
of	of	k?	of
the	the	k?	the
Silent	Silent	k1gInSc1	Silent
Planet	planeta	k1gFnPc2	planeta
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
knihy	kniha	k1gFnSc2	kniha
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
bibliografické	bibliografický	k2eAgFnSc6d1	bibliografická
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
