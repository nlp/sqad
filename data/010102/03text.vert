<p>
<s>
Mark	Mark	k1gMnSc1	Mark
Carroll	Carroll	k1gMnSc1	Carroll
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1972	[number]	k4	1972
Cork	Cork	k1gInSc1	Cork
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
irský	irský	k2eAgMnSc1d1	irský
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
běžec	běžec	k1gMnSc1	běžec
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
tratě	trata	k1gFnSc6	trata
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
juniorským	juniorský	k2eAgMnSc7d1	juniorský
mistrem	mistr	k1gMnSc7	mistr
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
5000	[number]	k4	5000
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dospělými	dospělí	k1gMnPc7	dospělí
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
trati	trať	k1gFnSc6	trať
získal	získat	k5eAaPmAgMnS	získat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
šampionátu	šampionát	k1gInSc6	šampionát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
stal	stát	k5eAaPmAgInS	stát
titul	titul	k1gInSc1	titul
halového	halový	k2eAgMnSc2d1	halový
mistra	mistr	k1gMnSc2	mistr
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
3000	[number]	k4	3000
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Mark	Mark	k1gMnSc1	Mark
Carroll	Carroll	k1gMnSc1	Carroll
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
IAAF	IAAF	kA	IAAF
</s>
</p>
