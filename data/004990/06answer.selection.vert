<s>
V	v	k7c6	v
Nibutani	Nibutaň	k1gFnSc6	Nibutaň
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
Biratori	Birator	k1gFnSc2	Birator
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Hokkaidó	Hokkaidó	k1gFnSc2	Hokkaidó
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
mnoho	mnoho	k4c1	mnoho
zbývajících	zbývající	k2eAgMnPc2d1	zbývající
rodilých	rodilý	k2eAgMnPc2d1	rodilý
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
100	[number]	k4	100
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
používalo	používat	k5eAaImAgNnS	používat
jazyk	jazyk	k1gInSc4	jazyk
denně	denně	k6eAd1	denně
pouze	pouze	k6eAd1	pouze
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
