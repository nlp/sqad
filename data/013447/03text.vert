<p>
<s>
Gwda	Gwda	k1gFnSc1	Gwda
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Küddow	Küddow	k1gMnSc2	Küddow
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
ve	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
(	(	kIx(	(
<g/>
Velkopolské	velkopolský	k2eAgFnPc1d1	Velkopolská
<g/>
,	,	kIx,	,
Pomořské	pomořský	k2eAgFnPc1d1	pomořská
<g/>
,	,	kIx,	,
Západopomořanské	Západopomořanský	k2eAgNnSc1d1	Západopomořanské
vojvodství	vojvodství	k1gNnSc1	vojvodství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
105	[number]	k4	105
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
4	[number]	k4	4
943	[number]	k4	943
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
územím	území	k1gNnSc7	území
Západopomořanského	Západopomořanský	k2eAgMnSc2d1	Západopomořanský
a	a	k8xC	a
Velkopolského	velkopolský	k2eAgNnSc2d1	Velkopolské
vojvodství	vojvodství	k1gNnSc2	vojvodství
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
druhého	druhý	k4xOgMnSc2	druhý
jmenovaného	jmenovaný	k1gMnSc2	jmenovaný
s	s	k7c7	s
Pomořským	pomořský	k2eAgNnSc7d1	Pomořské
vojvodstvím	vojvodství	k1gNnSc7	vojvodství
<g/>
.	.	kIx.	.
</s>
<s>
Teče	téct	k5eAaImIp3nS	téct
mezi	mezi	k7c7	mezi
Wałeckým	Wałecký	k2eAgNnSc7d1	Wałecký
pojezeřím	pojezeří	k1gNnSc7	pojezeří
a	a	k8xC	a
Krajeńským	Krajeńský	k2eAgNnSc7d1	Krajeńský
pojezeřím	pojezeří	k1gNnSc7	pojezeří
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
Pomořského	pomořský	k2eAgNnSc2d1	Pomořské
pojezeří	pojezeří	k1gNnSc2	pojezeří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
poblíž	poblíž	k7c2	poblíž
vesnice	vesnice	k1gFnSc2	vesnice
Porost	porost	k1gInSc1	porost
(	(	kIx(	(
<g/>
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
č.	č.	k?	č.
25	[number]	k4	25
<g/>
,	,	kIx,	,
úsek	úsek	k1gInSc1	úsek
Bobolice	Bobolice	k1gFnSc1	Bobolice
-	-	kIx~	-
Biały	Biały	k1gInPc1	Biały
Bór	bóra	k1gFnPc2	bóra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
jezerem	jezero	k1gNnSc7	jezero
Przybyszewko	Przybyszewko	k1gNnSc4	Przybyszewko
a	a	k8xC	a
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Grąbczyn	Grąbczyn	k1gInSc1	Grąbczyn
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
Wierzchowo	Wierzchowo	k6eAd1	Wierzchowo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
teče	téct	k5eAaImIp3nS	téct
podél	podél	k7c2	podél
silnice	silnice	k1gFnSc2	silnice
č.	č.	k?	č.
11	[number]	k4	11
jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Szczecinek	Szczecinek	k1gInSc4	Szczecinek
a	a	k8xC	a
po	po	k7c4	po
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
km	km	kA	km
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
Wielimie	Wielimie	k1gFnSc2	Wielimie
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
vesnice	vesnice	k1gFnSc2	vesnice
Gwda	Gwda	k1gMnSc1	Gwda
Mała	Mała	k1gMnSc1	Mała
přijímá	přijímat	k5eAaImIp3nS	přijímat
své	svůj	k3xOyFgNnSc4	svůj
rameno	rameno	k1gNnSc4	rameno
Dołga	Dołg	k1gMnSc2	Dołg
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
přes	přes	k7c4	přes
jezero	jezero	k1gNnSc4	jezero
Dołgie	Dołgie	k1gFnSc2	Dołgie
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
Gwda	Gwd	k2eAgFnSc1d1	Gwd
křižuje	křižovat	k5eAaImIp3nS	křižovat
silnice	silnice	k1gFnSc1	silnice
č.	č.	k?	č.
20	[number]	k4	20
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
železnici	železnice	k1gFnSc6	železnice
Szczecinek-	Szczecinek-	k1gFnSc6	Szczecinek-
Miastko-	Miastko-	k1gFnSc1	Miastko-
Słupsk	Słupsk	k1gInSc4	Słupsk
a	a	k8xC	a
teče	téct	k5eAaImIp3nS	téct
na	na	k7c4	na
jih	jih	k1gInSc4	jih
přibližně	přibližně	k6eAd1	přibližně
2,5	[number]	k4	2,5
km	km	kA	km
podél	podél	k7c2	podél
železnice	železnice	k1gFnSc2	železnice
Szczecinek-	Szczecinek-	k1gFnSc2	Szczecinek-
Czarne-	Czarne-	k1gFnSc2	Czarne-
Chojnice	Chojnice	k1gFnSc2	Chojnice
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
vesnice	vesnice	k1gFnSc2	vesnice
Łubnica	Łubnic	k1gInSc2	Łubnic
přijímá	přijímat	k5eAaImIp3nS	přijímat
svůj	svůj	k3xOyFgInSc4	svůj
největší	veliký	k2eAgInSc4d3	veliký
přítok	přítok	k1gInSc4	přítok
Czernici	Czernice	k1gFnSc4	Czernice
a	a	k8xC	a
až	až	k9	až
k	k	k7c3	k
vesnici	vesnice	k1gFnSc3	vesnice
Lędyczek	Lędyczka	k1gFnPc2	Lędyczka
tvoří	tvořit	k5eAaImIp3nS	tvořit
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c4	mezi
vojvodství	vojvodství	k1gNnSc4	vojvodství
(	(	kIx(	(
<g/>
Velkopolské	velkopolský	k2eAgNnSc4d1	Velkopolské
vojvodství	vojvodství	k1gNnSc4	vojvodství
i	i	k8xC	i
Pomořské	pomořský	k2eAgNnSc4d1	Pomořské
vojvodství	vojvodství	k1gNnSc4	vojvodství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
teče	téct	k5eAaImIp3nS	téct
podél	podél	k7c2	podél
silnic	silnice	k1gFnPc2	silnice
č.	č.	k?	č.
22	[number]	k4	22
a	a	k8xC	a
č.	č.	k?	č.
11	[number]	k4	11
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
města	město	k1gNnSc2	město
Jastrowie	Jastrowie	k1gFnSc2	Jastrowie
a	a	k8xC	a
vesnice	vesnice	k1gFnSc2	vesnice
Tarnówka	Tarnówek	k1gInSc2	Tarnówek
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
vesnicí	vesnice	k1gFnSc7	vesnice
Płytnica	Płytnicum	k1gNnSc2	Płytnicum
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
9	[number]	k4	9
km	km	kA	km
jižně	jižně	k6eAd1	jižně
přes	přes	k7c4	přes
vesnici	vesnice	k1gFnSc4	vesnice
Piła	Pił	k1gInSc2	Pił
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
Ujście	Ujście	k1gFnSc2	Ujście
ústí	ústit	k5eAaImIp3nS	ústit
zprava	zprava	k6eAd1	zprava
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Noteć	Noteć	k1gFnSc2	Noteć
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přítoky	přítok	k1gInPc4	přítok
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
k	k	k7c3	k
ústí	ústí	k1gNnPc1	ústí
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
zleva	zleva	k6eAd1	zleva
–	–	k?	–
Dołga	Dołga	k1gFnSc1	Dołga
<g/>
,	,	kIx,	,
Czernica	Czernica	k1gFnSc1	Czernica
<g/>
,	,	kIx,	,
Szczyra	Szczyra	k1gFnSc1	Szczyra
<g/>
,	,	kIx,	,
Dobrzynka	Dobrzynka	k1gFnSc1	Dobrzynka
<g/>
,	,	kIx,	,
Głomia	Głomia	k1gFnSc1	Głomia
</s>
</p>
<p>
<s>
zprava	zprava	k6eAd1	zprava
–	–	k?	–
Nizica	Nizica	k1gFnSc1	Nizica
<g/>
,	,	kIx,	,
Czarna	Czarna	k1gFnSc1	Czarna
<g/>
,	,	kIx,	,
Płytnica	Płytnica	k1gMnSc1	Płytnica
<g/>
,	,	kIx,	,
Rurzyca	Rurzyca	k1gMnSc1	Rurzyca
<g/>
,	,	kIx,	,
Piława	Piława	k1gMnSc1	Piława
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jezera	jezero	k1gNnSc2	jezero
Wierzchowo	Wierzchowo	k6eAd1	Wierzchowo
až	až	k9	až
k	k	k7c3	k
městu	město	k1gNnSc3	město
Ujście	Ujście	k1gFnSc2	Ujście
je	být	k5eAaImIp3nS	být
sjízdná	sjízdný	k2eAgFnSc1d1	sjízdná
pro	pro	k7c4	pro
kajaky	kajak	k1gInPc4	kajak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Г	Г	k?	Г
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gwda	Gwd	k1gInSc2	Gwd
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
