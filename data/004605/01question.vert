<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
srovnávacím	srovnávací	k2eAgNnSc7d1	srovnávací
studiem	studio	k1gNnSc7	studio
kultur	kultura	k1gFnPc2	kultura
a	a	k8xC	a
společností	společnost	k1gFnPc2	společnost
<g/>
?	?	kIx.	?
</s>
