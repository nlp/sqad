<s>
Etnologie	etnologie	k1gFnSc1	etnologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgMnSc2d1	řecký
ethnos	ethnos	k1gMnSc1	ethnos
<g/>
,	,	kIx,	,
kmen	kmen	k1gInSc1	kmen
<g/>
,	,	kIx,	,
národ	národ	k1gInSc1	národ
<g/>
,	,	kIx,	,
a	a	k8xC	a
logia	logia	k1gFnSc1	logia
<g/>
,	,	kIx,	,
nauka	nauka	k1gFnSc1	nauka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
společenská	společenský	k2eAgFnSc1d1	společenská
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
srovnávacím	srovnávací	k2eAgNnSc7d1	srovnávací
studiem	studio	k1gNnSc7	studio
kultur	kultura	k1gFnPc2	kultura
a	a	k8xC	a
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
pokračování	pokračování	k1gNnSc4	pokračování
etnografie	etnografie	k1gFnSc2	etnografie
(	(	kIx(	(
<g/>
národopisu	národopis	k1gInSc2	národopis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
potřeba	potřeba	k6eAd1	potřeba
nashromážděný	nashromážděný	k2eAgInSc4d1	nashromážděný
etnografický	etnografický	k2eAgInSc4d1	etnografický
materiál	materiál	k1gInSc4	materiál
utřídit	utřídit	k5eAaPmF	utřídit
a	a	k8xC	a
porovnat	porovnat	k5eAaPmF	porovnat
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
však	však	k9	však
pojmy	pojem	k1gInPc1	pojem
etnologie	etnologie	k1gFnSc2	etnologie
a	a	k8xC	a
etnografie	etnografie	k1gFnSc2	etnografie
užívají	užívat	k5eAaImIp3nP	užívat
jako	jako	k8xS	jako
synonyma	synonymum	k1gNnPc4	synonymum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
názvu	název	k1gInSc2	název
etnologie	etnologie	k1gFnSc2	etnologie
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc1	označení
kulturní	kulturní	k2eAgFnSc2d1	kulturní
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
antropologie	antropologie	k1gFnSc2	antropologie
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
někteří	některý	k3yIgMnPc1	některý
antropologové	antropolog	k1gMnPc1	antropolog
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
odlišnost	odlišnost	k1gFnSc4	odlišnost
antropologie	antropologie	k1gFnSc2	antropologie
od	od	k7c2	od
etnologie	etnologie	k1gFnSc2	etnologie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
použitím	použití	k1gNnSc7	použití
obou	dva	k4xCgInPc2	dva
názvů	název	k1gInPc2	název
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
i	i	k9	i
mocenské	mocenský	k2eAgInPc1d1	mocenský
zájmy	zájem	k1gInPc1	zájem
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
odborných	odborný	k2eAgNnPc2d1	odborné
pracovišť	pracoviště	k1gNnPc2	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
sociální	sociální	k2eAgMnPc1d1	sociální
a	a	k8xC	a
kulturní	kulturní	k2eAgMnPc1d1	kulturní
antropologové	antropolog	k1gMnPc1	antropolog
se	se	k3xPyFc4	se
odvolávají	odvolávat	k5eAaImIp3nP	odvolávat
na	na	k7c4	na
angloamerický	angloamerický	k2eAgInSc4d1	angloamerický
model	model	k1gInSc4	model
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
prezentují	prezentovat	k5eAaBmIp3nP	prezentovat
jakožto	jakožto	k8xS	jakožto
odlišný	odlišný	k2eAgInSc1d1	odlišný
a	a	k8xC	a
hodnotnější	hodnotný	k2eAgMnSc1d2	hodnotnější
<g/>
,	,	kIx,	,
etnologové	etnolog	k1gMnPc1	etnolog
vychází	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
tradic	tradice	k1gFnPc2	tradice
evropského	evropský	k2eAgInSc2d1	evropský
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
středoevropského	středoevropský	k2eAgInSc2d1	středoevropský
<g/>
,	,	kIx,	,
bádání	bádání	k1gNnSc2	bádání
<g/>
.	.	kIx.	.
</s>
