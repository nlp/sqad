<s>
Etnologie	etnologie	k1gFnSc1	etnologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgMnSc2d1	řecký
ethnos	ethnos	k1gMnSc1	ethnos
<g/>
,	,	kIx,	,
kmen	kmen	k1gInSc1	kmen
<g/>
,	,	kIx,	,
národ	národ	k1gInSc1	národ
<g/>
,	,	kIx,	,
a	a	k8xC	a
logia	logia	k1gFnSc1	logia
<g/>
,	,	kIx,	,
nauka	nauka	k1gFnSc1	nauka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
společenská	společenský	k2eAgFnSc1d1	společenská
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
srovnávacím	srovnávací	k2eAgNnSc7d1	srovnávací
studiem	studio	k1gNnSc7	studio
kultur	kultura	k1gFnPc2	kultura
a	a	k8xC	a
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
