<s>
Bryggen	Bryggen	k1gInSc1
</s>
<s>
BryggenSvětové	BryggenSvětový	k2eAgNnSc1d1
dědictví	dědictví	k1gNnSc1
UNESCO	UNESCO	kA
Smluvní	smluvní	k2eAgInSc1d1
stát	stát	k1gInSc1
</s>
<s>
Norsko	Norsko	k1gNnSc1
Norsko	Norsko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
60	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
5	#num#	k4
<g/>
°	°	k?
<g/>
19	#num#	k4
<g/>
′	′	k?
<g/>
20	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Typ	typ	k1gInSc1
</s>
<s>
kulturní	kulturní	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
Kritérium	kritérium	k1gNnSc1
</s>
<s>
iii	iii	k?
Odkaz	odkaz	k1gInSc1
</s>
<s>
59	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Zařazení	zařazení	k1gNnSc1
do	do	k7c2
seznamu	seznam	k1gInSc2
Zařazení	zařazení	k1gNnSc1
</s>
<s>
1979	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
zasedání	zasedání	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
Bryggen	Bryggen	k1gInSc1
je	být	k5eAaImIp3nS
nejstarší	starý	k2eAgFnSc1d3
čtvrť	čtvrť	k1gFnSc1
norského	norský	k2eAgInSc2d1
Bergenu	Bergen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
podél	podél	k7c2
východního	východní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
přístavu	přístav	k1gInSc2
Vå	Vå	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdysi	kdysi	k6eAd1
byla	být	k5eAaImAgFnS
hlavním	hlavní	k2eAgMnSc7d1
centrem	centr	k1gMnSc7
obchodu	obchod	k1gInSc2
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nP
ji	on	k3xPp3gFnSc4
souběžné	souběžný	k2eAgFnPc1d1
řady	řada	k1gFnPc1
domů	dům	k1gInPc2
s	s	k7c7
kamennými	kamenný	k2eAgInPc7d1
základy	základ	k1gInPc7
<g/>
,	,	kIx,
za	za	k7c7
jejichž	jejichž	k3xOyRp3gFnSc7
průčelími	průčelí	k1gNnPc7
s	s	k7c7
lomeným	lomený	k2eAgInSc7d1
štítem	štít	k1gInSc7
se	se	k3xPyFc4
ukrývají	ukrývat	k5eAaImIp3nP
rekonstruované	rekonstruovaný	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
z	z	k7c2
hrubých	hrubý	k2eAgFnPc2d1
fošen	fošna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
přísný	přísný	k2eAgInSc4d1
zákaz	zákaz	k1gInSc4
používání	používání	k1gNnSc1
ohně	oheň	k1gInSc2
bylo	být	k5eAaImAgNnS
mnoho	mnoho	k4c1
domů	dům	k1gInPc2
zničeno	zničen	k2eAgNnSc1d1
ohněm	oheň	k1gInSc7
<g/>
,	,	kIx,
naposledy	naposledy	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1955	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
bylo	být	k5eAaImAgNnS
58	#num#	k4
dřevěných	dřevěný	k2eAgInPc2d1
domů	dům	k1gInPc2
na	na	k7c6
bryggenském	bryggenský	k2eAgNnSc6d1
nábřeží	nábřeží	k1gNnSc6
zapsáno	zapsat	k5eAaPmNgNnS
na	na	k7c4
seznam	seznam	k1gInSc4
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
další	další	k2eAgFnPc1d1
památky	památka	k1gFnPc1
UNESCO	UNESCO	kA
v	v	k7c6
Norsku	Norsko	k1gNnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bryggen	Bryggen	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Na	na	k7c6
seznamu	seznam	k1gInSc6
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Norské	norský	k2eAgFnSc2d1
památky	památka	k1gFnSc2
na	na	k7c6
seznamu	seznam	k1gInSc6
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
</s>
<s>
Bryggen	Bryggen	k1gInSc1
v	v	k7c6
Bergenu	Bergen	k1gInSc6
•	•	k?
roubený	roubený	k2eAgInSc1d1
kostel	kostel	k1gInSc1
v	v	k7c6
Urnesu	Urnes	k1gInSc6
•	•	k?
Rø	Rø	k1gFnPc2
a	a	k8xC
okolí	okolí	k1gNnSc2
•	•	k?
Skalní	skalní	k2eAgFnSc2d1
rytiny	rytina	k1gFnSc2
v	v	k7c6
Altě	alt	k1gInSc6
•	•	k?
souostroví	souostroví	k1gNnSc2
Vega	Vegus	k1gMnSc2
•	•	k?
Struveho	Struve	k1gMnSc2
geodetický	geodetický	k2eAgInSc4d1
oblouk	oblouk	k1gInSc4
•	•	k?
Západonorské	Západonorský	k2eAgInPc4d1
fjordy	fjord	k1gInPc4
–	–	k?
Geirangerfjord	Geirangerfjord	k1gInSc1
a	a	k8xC
Næ	Næ	k1gMnSc1
•	•	k?
Průmyslové	průmyslový	k2eAgNnSc1d1
dědictví	dědictví	k1gNnSc1
regionu	region	k1gInSc2
Rjukan	Rjukana	k1gFnPc2
a	a	k8xC
Notodden	Notoddna	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Norsko	Norsko	k1gNnSc1
</s>
