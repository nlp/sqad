<s>
Kilimandžáro	Kilimandžáro	k1gNnSc1	Kilimandžáro
je	být	k5eAaImIp3nS	být
horský	horský	k2eAgInSc4d1	horský
masiv	masiv	k1gInSc4	masiv
téměř	téměř	k6eAd1	téměř
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
v	v	k7c6	v
Tanzanii	Tanzanie	k1gFnSc6	Tanzanie
při	při	k7c6	při
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Keňou	Keňa	k1gFnSc7	Keňa
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
3	[number]	k4	3
nečinných	činný	k2eNgInPc2d1	nečinný
stratovulkánů	stratovulkán	k1gInPc2	stratovulkán
-	-	kIx~	-
Shira	Shira	k1gMnSc1	Shira
<g/>
,	,	kIx,	,
Mawenzi	Mawenh	k1gMnPc1	Mawenh
a	a	k8xC	a
Kibo	Kibo	k6eAd1	Kibo
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
celého	celý	k2eAgInSc2d1	celý
Afrického	africký	k2eAgInSc2d1	africký
kontinentu	kontinent	k1gInSc2	kontinent
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Uhuru	Uhur	k1gInSc2	Uhur
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
vrchol	vrchol	k1gInSc4	vrchol
svobody	svoboda	k1gFnSc2	svoboda
<g/>
)	)	kIx)	)
na	na	k7c6	na
vulkánu	vulkán	k1gInSc6	vulkán
Kibo	Kibo	k6eAd1	Kibo
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
5895	[number]	k4	5895
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Vrchol	vrchol	k1gInSc1	vrchol
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
denně	denně	k6eAd1	denně
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
58	[number]	k4	58
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kilimandžáro	Kilimandžáro	k1gNnSc1	Kilimandžáro
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tři	tři	k4xCgInPc4	tři
stupně	stupeň	k1gInPc4	stupeň
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Moshi	Mosh	k1gFnSc2	Mosh
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
příkopové	příkopový	k2eAgFnSc6d1	příkopová
propadlině	propadlina	k1gFnSc6	propadlina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zlom	zlom	k1gInSc4	zlom
táhnoucí	táhnoucí	k2eAgInSc4d1	táhnoucí
se	se	k3xPyFc4	se
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
asi	asi	k9	asi
5000	[number]	k4	5000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c4	před
půl	půl	k1xP	půl
milionem	milion	k4xCgInSc7	milion
let	léto	k1gNnPc2	léto
zde	zde	k6eAd1	zde
probíhala	probíhat	k5eAaImAgFnS	probíhat
intenzivní	intenzivní	k2eAgFnSc1d1	intenzivní
sopečná	sopečný	k2eAgFnSc1d1	sopečná
činnost	činnost	k1gFnSc1	činnost
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
důsledkem	důsledek	k1gInSc7	důsledek
byl	být	k5eAaImAgMnS	být
mimo	mimo	k7c4	mimo
jiné	jiná	k1gFnPc4	jiná
vznik	vznik	k1gInSc4	vznik
masivu	masiv	k1gInSc2	masiv
Kilimandžára	Kilimandžáro	k1gNnSc2	Kilimandžáro
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
jména	jméno	k1gNnPc1	jméno
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
horu	hora	k1gFnSc4	hora
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Kilima	Kilima	k1gFnSc1	Kilima
Dscharo	Dschara	k1gFnSc5	Dschara
<g/>
,	,	kIx,	,
Oldoinyo	Oldoinyo	k1gMnSc1	Oldoinyo
Oibor	Oibor	k1gMnSc1	Oibor
(	(	kIx(	(
<g/>
v	v	k7c6	v
masajštině	masajština	k1gFnSc6	masajština
Bílá	bílý	k2eAgFnSc1d1	bílá
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Kilima	Kilima	k1gNnSc1	Kilima
Njaro	Njaro	k1gNnSc1	Njaro
(	(	kIx(	(
<g/>
svahilsky	svahilsky	k6eAd1	svahilsky
Svítící	svítící	k2eAgFnSc1d1	svítící
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kaiser-Wilhelm-Spitze	Kaiser-Wilhelm-Spitze	k1gFnSc1	Kaiser-Wilhelm-Spitze
(	(	kIx(	(
<g/>
Vrch	vrch	k1gInSc1	vrch
císaře	císař	k1gMnSc2	císař
Viléma	Vilém	k1gMnSc2	Vilém
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
Uhuru	Uhur	k1gInSc2	Uhur
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
zdolán	zdolat	k5eAaPmNgInS	zdolat
armádním	armádní	k2eAgInSc7d1	armádní
zvědem	zvěd	k1gInSc7	zvěd
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Marangu	Marang	k1gInSc2	Marang
jménem	jméno	k1gNnSc7	jméno
Johannes	Johannes	k1gMnSc1	Johannes
Kinyala	Kinyal	k1gMnSc2	Kinyal
Lauwo	Lauwo	k1gMnSc1	Lauwo
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vylezl	vylézt	k5eAaPmAgMnS	vylézt
na	na	k7c4	na
horu	hora	k1gFnSc4	hora
devětkrát	devětkrát	k6eAd1	devětkrát
<g/>
,	,	kIx,	,
než	než	k8xS	než
si	se	k3xPyFc3	se
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
kráter	kráter	k1gInSc4	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Lauwo	Lauwo	k1gMnSc1	Lauwo
byl	být	k5eAaImAgMnS	být
průvodcem	průvodce	k1gMnSc7	průvodce
prvních	první	k4xOgNnPc6	první
"	"	kIx"	"
<g/>
oficiálních	oficiální	k2eAgMnPc2d1	oficiální
<g/>
"	"	kIx"	"
pokořitelů	pokořitel	k1gMnPc2	pokořitel
hory	hora	k1gFnSc2	hora
Němce	Němec	k1gMnSc2	Němec
Hanse	Hans	k1gMnSc2	Hans
Meyera	Meyer	k1gMnSc2	Meyer
a	a	k8xC	a
Rakušana	Rakušan	k1gMnSc2	Rakušan
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Purtschellera	Purtscheller	k1gMnSc2	Purtscheller
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
dojeli	dojet	k5eAaPmAgMnP	dojet
na	na	k7c4	na
Uhuru	Uhura	k1gFnSc4	Uhura
dokonce	dokonce	k9	dokonce
dva	dva	k4xCgMnPc1	dva
cyklisté	cyklista	k1gMnPc1	cyklista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zdolal	zdolat	k5eAaPmAgInS	zdolat
vrchol	vrchol	k1gInSc1	vrchol
Martin	Martin	k1gMnSc1	Martin
Rota	rota	k1gFnSc1	rota
jakožto	jakožto	k8xS	jakožto
první	první	k4xOgMnSc1	první
tělesně	tělesně	k6eAd1	tělesně
postižený	postižený	k2eAgMnSc1d1	postižený
Čech	Čech	k1gMnSc1	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svazích	svah	k1gInPc6	svah
Kilimandžára	Kilimandžáro	k1gNnSc2	Kilimandžáro
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
klimatických	klimatický	k2eAgNnPc2d1	klimatické
pásem	pásmo	k1gNnPc2	pásmo
s	s	k7c7	s
jim	on	k3xPp3gMnPc3	on
příslušnou	příslušný	k2eAgFnSc7d1	příslušná
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
.	.	kIx.	.
1400	[number]	k4	1400
<g/>
-	-	kIx~	-
<g/>
1800	[number]	k4	1800
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
kultivovaná	kultivovaný	k2eAgFnSc1d1	kultivovaná
krajina	krajina	k1gFnSc1	krajina
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
vysokým	vysoký	k2eAgFnPc3d1	vysoká
srážkám	srážka	k1gFnPc3	srážka
a	a	k8xC	a
vulkanické	vulkanický	k2eAgFnSc6d1	vulkanická
půdě	půda	k1gFnSc6	půda
dobře	dobře	k6eAd1	dobře
daří	dařit	k5eAaImIp3nS	dařit
plodinám	plodina	k1gFnPc3	plodina
<g/>
.	.	kIx.	.
1800	[number]	k4	1800
<g/>
-	-	kIx~	-
<g/>
2900	[number]	k4	2900
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
pásmu	pásmo	k1gNnSc6	pásmo
následuje	následovat	k5eAaImIp3nS	následovat
divoký	divoký	k2eAgInSc1d1	divoký
horský	horský	k2eAgInSc1d1	horský
deštný	deštný	k2eAgInSc1d1	deštný
prales	prales	k1gInSc1	prales
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
vysokými	vysoký	k2eAgFnPc7d1	vysoká
srážkami	srážka	k1gFnPc7	srážka
<g/>
.	.	kIx.	.
2900	[number]	k4	2900
<g/>
-	-	kIx~	-
<g/>
3300	[number]	k4	3300
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
výškách	výška	k1gFnPc6	výška
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
pásmo	pásmo	k1gNnSc4	pásmo
vřesovišť	vřesoviště	k1gNnPc2	vřesoviště
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
pásma	pásmo	k1gNnSc2	pásmo
převažuje	převažovat	k5eAaImIp3nS	převažovat
nevlídné	vlídný	k2eNgNnSc1d1	nevlídné
počasí	počasí	k1gNnSc1	počasí
s	s	k7c7	s
mlhami	mlha	k1gFnPc7	mlha
a	a	k8xC	a
dešťovými	dešťový	k2eAgFnPc7d1	dešťová
či	či	k8xC	či
kroupovými	kroupový	k2eAgFnPc7d1	kroupová
přeháňkami	přeháňka	k1gFnPc7	přeháňka
<g/>
.	.	kIx.	.
3300	[number]	k4	3300
<g/>
-	-	kIx~	-
<g/>
4000	[number]	k4	4000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
chladnější	chladný	k2eAgMnSc1d2	chladnější
a	a	k8xC	a
jasnější	jasný	k2eAgNnSc1d2	jasnější
počasí	počasí	k1gNnSc1	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
zde	zde	k6eAd1	zde
svítí	svítit	k5eAaImIp3nS	svítit
slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Ustupují	ustupovat	k5eAaImIp3nP	ustupovat
mlhy	mlha	k1gFnPc1	mlha
<g/>
.	.	kIx.	.
</s>
<s>
Rostou	růst	k5eAaImIp3nP	růst
zde	zde	k6eAd1	zde
unikátní	unikátní	k2eAgFnPc1d1	unikátní
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
4000	[number]	k4	4000
<g/>
-	-	kIx~	-
<g/>
4800	[number]	k4	4800
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
pásmu	pásmo	k1gNnSc6	pásmo
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
velmi	velmi	k6eAd1	velmi
tvrdé	tvrdý	k2eAgFnPc4d1	tvrdá
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Teploty	teplota	k1gFnPc1	teplota
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
klesají	klesat	k5eAaImIp3nP	klesat
pod	pod	k7c4	pod
bod	bod	k1gInSc4	bod
mrazu	mráz	k1gInSc2	mráz
a	a	k8xC	a
ve	v	k7c6	v
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
šplhají	šplhat	k5eAaImIp3nP	šplhat
i	i	k9	i
na	na	k7c4	na
35	[number]	k4	35
°	°	k?	°
<g/>
C.	C.	kA	C.
Rostou	růst	k5eAaImIp3nP	růst
zde	zde	k6eAd1	zde
jen	jen	k9	jen
lišejníky	lišejník	k1gInPc1	lišejník
a	a	k8xC	a
mechové	mechový	k2eAgInPc1d1	mechový
porosty	porost	k1gInPc1	porost
<g/>
.	.	kIx.	.
nad	nad	k7c4	nad
4800	[number]	k4	4800
Nad	nad	k7c7	nad
touto	tento	k3xDgFnSc7	tento
výškou	výška	k1gFnSc7	výška
panují	panovat	k5eAaImIp3nP	panovat
již	již	k6eAd1	již
arktické	arktický	k2eAgFnPc4d1	arktická
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
ledovce	ledovec	k1gInPc1	ledovec
a	a	k8xC	a
sníh	sníh	k1gInSc1	sníh
<g/>
,	,	kIx,	,
vegetace	vegetace	k1gFnSc1	vegetace
žádná	žádný	k3yNgFnSc1	žádný
<g/>
.	.	kIx.	.
</s>
<s>
Ledovce	ledovec	k1gInPc1	ledovec
stékají	stékat	k5eAaImIp3nP	stékat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
zhruba	zhruba	k6eAd1	zhruba
4270	[number]	k4	4270
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Vrcholový	vrcholový	k2eAgInSc1d1	vrcholový
kráter	kráter	k1gInSc1	kráter
Kibo	Kibo	k6eAd1	Kibo
vypouští	vypouštět	k5eAaImIp3nS	vypouštět
plyn	plyn	k1gInSc4	plyn
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
vrcholu	vrchol	k1gInSc2	vrchol
Uhuru	Uhur	k1gInSc2	Uhur
<g/>
.	.	kIx.	.
</s>
<s>
Kibo	Kibo	k6eAd1	Kibo
má	mít	k5eAaImIp3nS	mít
vrcholový	vrcholový	k2eAgInSc4d1	vrcholový
kráter	kráter	k1gInSc4	kráter
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
2,4	[number]	k4	2,4
km	km	kA	km
s	s	k7c7	s
hloubkou	hloubka	k1gFnSc7	hloubka
až	až	k9	až
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
kráteru	kráter	k1gInSc2	kráter
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Uhuru	Uhur	k1gInSc2	Uhur
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byla	být	k5eAaImAgFnS	být
tekutá	tekutý	k2eAgFnSc1d1	tekutá
láva	láva	k1gFnSc1	láva
jenom	jenom	k9	jenom
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
špičkou	špička	k1gFnSc7	špička
kráteru	kráter	k1gInSc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
žádná	žádný	k3yNgFnSc1	žádný
nová	nový	k2eAgFnSc1d1	nová
vulkanická	vulkanický	k2eAgFnSc1d1	vulkanická
činnost	činnost	k1gFnSc1	činnost
neočekává	očekávat	k5eNaImIp3nS	očekávat
<g/>
,	,	kIx,	,
přece	přece	k9	přece
jenom	jenom	k9	jenom
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
obavy	obava	k1gFnSc2	obava
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
části	část	k1gFnSc2	část
hory	hora	k1gFnSc2	hora
můžou	můžou	k?	můžou
zřítit	zřítit	k5eAaPmF	zřítit
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnPc1d1	místní
legendy	legenda	k1gFnPc1	legenda
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c6	o
činné	činný	k2eAgFnSc6d1	činná
aktivitě	aktivita	k1gFnSc6	aktivita
před	před	k7c7	před
170	[number]	k4	170
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
další	další	k2eAgInPc1d1	další
zaniklé	zaniklý	k2eAgInPc1d1	zaniklý
vulkány	vulkán	k1gInPc1	vulkán
jsou	být	k5eAaImIp3nP	být
Mawenzi	Mawenze	k1gFnSc4	Mawenze
(	(	kIx(	(
<g/>
5	[number]	k4	5
149	[number]	k4	149
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
vrchol	vrchol	k1gInSc4	vrchol
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
po	po	k7c4	po
Mount	Mount	k1gInSc4	Mount
Kenya	Keny	k1gInSc2	Keny
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Shira	Shira	k1gFnSc1	Shira
(	(	kIx(	(
<g/>
3962	[number]	k4	3962
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
vrcholu	vrchol	k1gInSc2	vrchol
Kilimandžára	Kilimandžáro	k1gNnSc2	Kilimandžáro
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
využít	využít	k5eAaPmF	využít
některou	některý	k3yIgFnSc4	některý
z	z	k7c2	z
šesti	šest	k4xCc2	šest
základních	základní	k2eAgFnPc2d1	základní
tras	trasa	k1gFnPc2	trasa
<g/>
:	:	kIx,	:
Marangu	Marang	k1gInSc2	Marang
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc6d1	východní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mweka	Mweka	k1gFnSc1	Mweka
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc1d1	jižní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Umbwe	Umbwe	k1gInSc1	Umbwe
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc6d1	jižní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Shira	Shira	k1gFnSc1	Shira
(	(	kIx(	(
<g/>
západní	západní	k2eAgFnSc1d1	západní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rongai	Rongai	k1gNnSc1	Rongai
(	(	kIx(	(
<g/>
severní	severní	k2eAgMnSc1d1	severní
<g/>
)	)	kIx)	)
a	a	k8xC	a
Machame	Macham	k1gInSc5	Macham
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
90	[number]	k4	90
%	%	kIx~	%
turistů	turist	k1gMnPc2	turist
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
po	po	k7c6	po
trase	trasa	k1gFnSc6	trasa
Marangu	Marang	k1gInSc2	Marang
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
trasa	trasa	k1gFnSc1	trasa
Coca-Cola	cocaola	k1gFnSc1	coca-cola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
svých	svůj	k3xOyFgFnPc6	svůj
stanovištích	stanoviště	k1gNnPc6	stanoviště
noclehárnami	noclehárna	k1gFnPc7	noclehárna
či	či	k8xC	či
velmi	velmi	k6eAd1	velmi
skromným	skromný	k2eAgNnSc7d1	skromné
ubytovacím	ubytovací	k2eAgNnSc7d1	ubytovací
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklý	obvyklý	k2eAgInSc1d1	obvyklý
výstup	výstup	k1gInSc1	výstup
zabere	zabrat	k5eAaPmIp3nS	zabrat
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
3300	[number]	k4	3300
<g/>
-	-	kIx~	-
<g/>
4000	[number]	k4	4000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
rostou	růst	k5eAaImIp3nP	růst
senecie	senecie	k1gFnPc4	senecie
(	(	kIx(	(
<g/>
stromové	stromový	k2eAgInPc4d1	stromový
starčeky	starček	k1gInPc4	starček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lobélie	lobélie	k1gFnSc1	lobélie
(	(	kIx(	(
<g/>
lobelky	lobelka	k1gFnPc1	lobelka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protei	protei	k1gNnPc1	protei
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Senecie	Senecie	k1gFnPc1	Senecie
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
výšky	výška	k1gFnPc4	výška
až	až	k9	až
pěti	pět	k4xCc2	pět
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
světový	světový	k2eAgInSc1d1	světový
unikát	unikát	k1gInSc1	unikát
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
obrovské	obrovský	k2eAgInPc1d1	obrovský
exempláře	exemplář	k1gInPc1	exemplář
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
právě	právě	k9	právě
jen	jen	k9	jen
u	u	k7c2	u
afrických	africký	k2eAgFnPc2d1	africká
pětitisícovek	pětitisícovka	k1gFnPc2	pětitisícovka
<g/>
.	.	kIx.	.
</s>
<s>
Kilimandžáro	Kilimandžáro	k1gNnSc1	Kilimandžáro
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Julese	Julese	k1gFnSc2	Julese
Vernea	Verne	k1gInSc2	Verne
Pět	pět	k4xCc1	pět
neděl	neděle	k1gFnPc2	neděle
v	v	k7c6	v
baloně	balon	k1gInSc6	balon
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Hanzelka	Hanzelka	k1gMnSc1	Hanzelka
a	a	k8xC	a
Miroslav	Miroslav	k1gMnSc1	Miroslav
Zikmund	Zikmund	k1gMnSc1	Zikmund
napsali	napsat	k5eAaBmAgMnP	napsat
reportáž	reportáž	k1gFnSc4	reportáž
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
výstupu	výstup	k1gInSc6	výstup
na	na	k7c4	na
Kilimandžáro	Kilimandžáro	k1gNnSc4	Kilimandžáro
<g/>
,	,	kIx,	,
vyšla	vyjít	k5eAaPmAgFnS	vyjít
knižně	knižně	k6eAd1	knižně
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
S	s	k7c7	s
československou	československý	k2eAgFnSc7d1	Československá
vlajkou	vlajka	k1gFnSc7	vlajka
na	na	k7c4	na
Kilimandžáro	Kilimandžáro	k1gNnSc4	Kilimandžáro
<g/>
.	.	kIx.	.
</s>
<s>
Masiv	masiv	k1gInSc1	masiv
Kilimandžáro	Kilimandžáro	k1gNnSc1	Kilimandžáro
je	být	k5eAaImIp3nS	být
zmíněn	zmíněn	k2eAgInSc1d1	zmíněn
ve	v	k7c6	v
vědeckofantastickém	vědeckofantastický	k2eAgInSc6d1	vědeckofantastický
románu	román	k1gInSc6	román
Roberta	Roberta	k1gFnSc1	Roberta
A.	A.	kA	A.
Heinleina	Heinleina	k1gFnSc1	Heinleina
Měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
drsná	drsný	k2eAgFnSc1d1	drsná
milenka	milenka	k1gFnSc1	milenka
<g/>
.	.	kIx.	.
</s>
<s>
Ian	Ian	k?	Ian
McDonald	McDonald	k1gMnSc1	McDonald
napsal	napsat	k5eAaBmAgMnS	napsat
povídku	povídka	k1gFnSc4	povídka
s	s	k7c7	s
názvem	název	k1gInSc7	název
Ke	k	k7c3	k
Kilimandžáru	Kilimandžáro	k1gNnSc3	Kilimandžáro
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Toward	Toward	k1gInSc1	Toward
Kilimanjaro	Kilimanjara	k1gFnSc5	Kilimanjara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mike	Mike	k6eAd1	Mike
Resnick	Resnick	k1gMnSc1	Resnick
napsal	napsat	k5eAaPmAgMnS	napsat
povídku	povídka	k1gFnSc4	povídka
s	s	k7c7	s
názvem	název	k1gInSc7	název
Kilimandžáro	Kilimandžáro	k1gNnSc1	Kilimandžáro
<g/>
:	:	kIx,	:
Bajka	bajka	k1gFnSc1	bajka
o	o	k7c6	o
Utopii	utopie	k1gFnSc6	utopie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Kilimanjaro	Kilimanjara	k1gFnSc5	Kilimanjara
<g/>
:	:	kIx,	:
A	a	k9	a
Fable	Fabl	k1gMnSc2	Fabl
of	of	k?	of
Utopia	Utopius	k1gMnSc2	Utopius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ray	Ray	k?	Ray
Bradbury	Bradbura	k1gFnSc2	Bradbura
napsal	napsat	k5eAaPmAgMnS	napsat
povídku	povídka	k1gFnSc4	povídka
s	s	k7c7	s
názvem	název	k1gInSc7	název
Stroj	stroj	k1gInSc1	stroj
Kilimandžáro	Kilimandžáro	k1gNnSc1	Kilimandžáro
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
The	The	k1gFnSc1	The
Kilimanjaro	Kilimanjara	k1gFnSc5	Kilimanjara
Device	device	k1gInSc1	device
nebo	nebo	k8xC	nebo
The	The	k1gFnSc1	The
Kilimanjaro	Kilimanjara	k1gFnSc5	Kilimanjara
Machine	Machin	k1gMnSc5	Machin
<g/>
)	)	kIx)	)
</s>
