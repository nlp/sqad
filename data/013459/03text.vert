<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Chlajn	Chlajn	k1gMnSc1	Chlajn
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
březen	březen	k1gInSc4	březen
1904	[number]	k4	1904
Beroun	Beroun	k1gInSc4	Beroun
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1943	[number]	k4	1943
Osvětim	Osvětim	k1gFnSc1	Osvětim
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
důstojník	důstojník	k1gMnSc1	důstojník
československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
štábní	štábní	k2eAgMnSc1d1	štábní
kapitán	kapitán	k1gMnSc1	kapitán
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
zpravodajskou	zpravodajský	k2eAgFnSc4d1	zpravodajská
síť	síť	k1gFnSc4	síť
pro	pro	k7c4	pro
protektorátní	protektorátní	k2eAgFnSc4d1	protektorátní
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
jej	on	k3xPp3gMnSc4	on
zatklo	zatknout	k5eAaPmAgNnS	zatknout
gestapo	gestapo	k1gNnSc1	gestapo
<g/>
,	,	kIx,	,
stačil	stačit	k5eAaBmAgInS	stačit
ještě	ještě	k6eAd1	ještě
zničit	zničit	k5eAaPmF	zničit
dokumenty	dokument	k1gInPc4	dokument
a	a	k8xC	a
při	při	k7c6	při
výslechu	výslech	k1gInSc6	výslech
nic	nic	k6eAd1	nic
neprozradil	prozradit	k5eNaPmAgMnS	prozradit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
deportován	deportovat	k5eAaBmNgInS	deportovat
do	do	k7c2	do
Osvětimi	Osvětim	k1gFnSc2	Osvětim
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
zahynul	zahynout	k5eAaPmAgMnS	zahynout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
symbolický	symbolický	k2eAgInSc1d1	symbolický
hrob	hrob	k1gInSc1	hrob
na	na	k7c6	na
českobudějovickém	českobudějovický	k2eAgInSc6d1	českobudějovický
hřbitově	hřbitov	k1gInSc6	hřbitov
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
jedna	jeden	k4xCgFnSc1	jeden
ulice	ulice	k1gFnSc1	ulice
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Hroby	hrob	k1gInPc1	hrob
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
sv.	sv.	kA	sv.
Otýlie	Otýlie	k1gFnSc1	Otýlie
vojensko-historickým	vojenskoistorický	k2eAgNnSc7d1	vojensko-historické
prizmatem	prizma	k1gNnSc7	prizma
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
