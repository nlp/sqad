<p>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Španělské	španělský	k2eAgNnSc1d1	španělské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
a	a	k8xC	a
galicijsky	galicijsky	k6eAd1	galicijsky
Reino	Rein	k2eAgNnSc4d1	Reino
de	de	k?	de
Españ	Españ	k1gFnSc7	Españ
<g/>
;	;	kIx,	;
katalánsky	katalánsky	k6eAd1	katalánsky
Regne	Regn	k1gInSc5	Regn
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Espanya	Espanya	k1gMnSc1	Espanya
<g/>
;	;	kIx,	;
baskicky	baskicky	k6eAd1	baskicky
Espainiako	Espainiako	k1gNnSc1	Espainiako
Erresuma	Erresumum	k1gNnSc2	Erresumum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
ležící	ležící	k2eAgInSc4d1	ležící
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Portugalskem	Portugalsko	k1gNnSc7	Portugalsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
s	s	k7c7	s
Andorrou	Andorra	k1gFnSc7	Andorra
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Gibraltarem	Gibraltar	k1gInSc7	Gibraltar
<g/>
;	;	kIx,	;
španělské	španělský	k2eAgFnPc1d1	španělská
severoafrické	severoafrický	k2eAgFnPc1d1	severoafrická
državy	država	k1gFnPc1	država
Ceuta	Ceuto	k1gNnSc2	Ceuto
a	a	k8xC	a
Melilla	Melillo	k1gNnSc2	Melillo
mají	mít	k5eAaImIp3nP	mít
pozemní	pozemní	k2eAgFnSc4d1	pozemní
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Marokem	Maroko	k1gNnSc7	Maroko
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
Španělskému	španělský	k2eAgNnSc3d1	španělské
království	království	k1gNnSc3	království
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Kanárské	kanárský	k2eAgInPc4d1	kanárský
ostrovy	ostrov	k1gInPc4	ostrov
v	v	k7c6	v
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceánu	oceán	k1gInSc6	oceán
a	a	k8xC	a
Baleáry	Baleáry	k1gFnPc1	Baleáry
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
Španělska	Španělsko	k1gNnSc2	Španělsko
je	být	k5eAaImIp3nS	být
i	i	k9	i
katalánské	katalánský	k2eAgNnSc1d1	katalánské
město	město	k1gNnSc1	město
Llívia	Llívium	k1gNnSc2	Llívium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
obklopeno	obklopen	k2eAgNnSc1d1	obklopeno
územím	území	k1gNnSc7	území
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
je	být	k5eAaImIp3nS	být
konstituční	konstituční	k2eAgFnSc7d1	konstituční
monarchií	monarchie	k1gFnSc7	monarchie
<g/>
,	,	kIx,	,
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
NATO	NATO	kA	NATO
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
demokratické	demokratický	k2eAgFnSc2d1	demokratická
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
španělština	španělština	k1gFnSc1	španělština
(	(	kIx(	(
<g/>
kastilština	kastilština	k1gFnSc1	kastilština
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
další	další	k2eAgInPc4d1	další
jazyky	jazyk	k1gInPc4	jazyk
jsou	být	k5eAaImIp3nP	být
uznávány	uznáván	k2eAgInPc4d1	uznáván
jako	jako	k8xS	jako
úřední	úřední	k2eAgInPc4d1	úřední
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
autonomními	autonomní	k2eAgNnPc7d1	autonomní
společenstvími	společenství	k1gNnPc7	společenství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Dávná	dávný	k2eAgFnSc1d1	dávná
historie	historie	k1gFnSc1	historie
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
žili	žít	k5eAaImAgMnP	žít
lidé	člověk	k1gMnPc1	člověk
již	již	k6eAd1	již
před	před	k7c7	před
pěti	pět	k4xCc7	pět
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
doloženy	doložit	k5eAaPmNgInP	doložit
četné	četný	k2eAgInPc1d1	četný
kmeny	kmen	k1gInPc1	kmen
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
keltského	keltský	k2eAgNnSc2d1	keltské
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
iberského	iberský	k2eAgInSc2d1	iberský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
z	z	k7c2	z
iberštiny	iberština	k1gFnSc2	iberština
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
četné	četný	k2eAgInPc4d1	četný
jazykové	jazykový	k2eAgInPc4d1	jazykový
zlomky	zlomek	k1gInPc4	zlomek
v	v	k7c6	v
nápisech	nápis	k1gInPc6	nápis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
století	století	k1gNnSc6	století
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
sem	sem	k6eAd1	sem
expandovalo	expandovat	k5eAaImAgNnS	expandovat
Kartágo	Kartágo	k1gNnSc1	Kartágo
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgNnSc1d1	následované
římskou	římský	k2eAgFnSc7d1	římská
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
2.	[number]	k4	2.
století	století	k1gNnPc2	století
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
postupně	postupně	k6eAd1	postupně
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
celý	celý	k2eAgInSc1d1	celý
poloostrov	poloostrov	k1gInSc1	poloostrov
Řím	Řím	k1gInSc1	Řím
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
představitelé	představitel	k1gMnPc1	představitel
zde	zde	k6eAd1	zde
zřídili	zřídit	k5eAaPmAgMnP	zřídit
několik	několik	k4yIc4	několik
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
Lusitania	Lusitanium	k1gNnSc2	Lusitanium
<g/>
,	,	kIx,	,
Baetica	Baeticum	k1gNnSc2	Baeticum
<g/>
,	,	kIx,	,
Hispania	Hispanium	k1gNnSc2	Hispanium
Tarraconensis	Tarraconensis	k1gFnSc2	Tarraconensis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
romanizace	romanizace	k1gFnSc1	romanizace
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
nově	nově	k6eAd1	nově
zřizovaných	zřizovaný	k2eAgNnPc2d1	zřizované
či	či	k8xC	či
starších	starý	k2eAgNnPc2d2	starší
městských	městský	k2eAgNnPc2d1	Městské
center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byly	být	k5eAaImAgFnP	být
Toletum	Toletum	k1gNnSc4	Toletum
(	(	kIx(	(
<g/>
Toledo	Toledo	k1gNnSc4	Toledo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hispalis	Hispalis	k1gFnSc1	Hispalis
(	(	kIx(	(
<g/>
Sevilla	Sevilla	k1gFnSc1	Sevilla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tarraco	Tarraco	k1gMnSc1	Tarraco
(	(	kIx(	(
<g/>
Tarragona	Tarragona	k1gFnSc1	Tarragona
<g/>
)	)	kIx)	)
či	či	k8xC	či
Carthago	Carthago	k1gNnSc1	Carthago
Nova	novum	k1gNnSc2	novum
(	(	kIx(	(
<g/>
Cartagena	Cartageno	k1gNnSc2	Cartageno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
stavěli	stavět	k5eAaImAgMnP	stavět
cesty	cesta	k1gFnPc4	cesta
<g/>
,	,	kIx,	,
vodovody	vodovod	k1gInPc4	vodovod
<g/>
,	,	kIx,	,
přístavy	přístav	k1gInPc4	přístav
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
změnili	změnit	k5eAaPmAgMnP	změnit
tvář	tvář	k1gFnSc4	tvář
celé	celý	k2eAgFnSc2d1	celá
oblasti	oblast	k1gFnSc2	oblast
–	–	k?	–
germánské	germánský	k2eAgInPc1d1	germánský
kmeny	kmen	k1gInPc1	kmen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
sem	sem	k6eAd1	sem
začaly	začít	k5eAaPmAgFnP	začít
pronikat	pronikat	k5eAaImF	pronikat
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
antiky	antika	k1gFnSc2	antika
v	v	k7c6	v
5.	[number]	k4	5.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
starověkou	starověký	k2eAgFnSc4d1	starověká
Hispánii	Hispánie	k1gFnSc4	Hispánie
nalezly	nalézt	k5eAaBmAgInP	nalézt
jako	jako	k9	jako
plně	plně	k6eAd1	plně
romanizovanou	romanizovaný	k2eAgFnSc4d1	romanizovaná
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
nových	nový	k2eAgMnPc2d1	nový
dobyvatelů	dobyvatel	k1gMnPc2	dobyvatel
byli	být	k5eAaImAgMnP	být
nejúspěšnější	úspěšný	k2eAgMnPc1d3	nejúspěšnější
germánští	germánský	k2eAgMnPc1d1	germánský
Vizigóti	Vizigót	k1gMnPc1	Vizigót
a	a	k8xC	a
Svébové	Svéb	k1gMnPc1	Svéb
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
stabilní	stabilní	k2eAgNnSc4d1	stabilní
království	království	k1gNnSc4	království
<g/>
,	,	kIx,	,
od	od	k7c2	od
sklonku	sklonek	k1gInSc2	sklonek
6.	[number]	k4	6.
století	století	k1gNnSc4	století
sjednocené	sjednocený	k2eAgNnSc4d1	sjednocené
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
vizigótských	vizigótský	k2eAgMnPc2d1	vizigótský
králů	král	k1gMnPc2	král
v	v	k7c6	v
Toledu	Toledo	k1gNnSc6	Toledo
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Vizigótská	vizigótský	k2eAgFnSc1d1	Vizigótská
říše	říš	k1gFnSc2	říš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
germánská	germánský	k2eAgFnSc1d1	germánská
elita	elita	k1gFnSc1	elita
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
romanizovala	romanizovat	k5eAaBmAgFnS	romanizovat
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
starou	starý	k2eAgFnSc7d1	stará
elitou	elita	k1gFnSc7	elita
hispanořímskou	hispanořímský	k2eAgFnSc7d1	hispanořímský
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
novou	nový	k2eAgFnSc4d1	nová
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
vrstvu	vrstva	k1gFnSc4	vrstva
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ruku	ruka	k1gFnSc4	ruka
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
unifikaci	unifikace	k1gFnSc3	unifikace
náboženské	náboženský	k2eAgFnSc2d1	náboženská
<g/>
,	,	kIx,	,
když	když	k8xS	když
král	král	k1gMnSc1	král
Rekkared	Rekkared	k1gMnSc1	Rekkared
I.	I.	kA	I.
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
šlechtici	šlechtic	k1gMnPc7	šlechtic
ke	k	k7c3	k
katolicismu	katolicismus	k1gInSc3	katolicismus
a	a	k8xC	a
vzdal	vzdát	k5eAaPmAgMnS	vzdát
se	se	k3xPyFc4	se
tradičního	tradiční	k2eAgNnSc2d1	tradiční
germánského	germánský	k2eAgNnSc2d1	germánské
ariánství	ariánství	k1gNnSc2	ariánství
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ve	v	k7c6	v
vizigótském	vizigótský	k2eAgNnSc6d1	Vizigótské
království	království	k1gNnSc6	království
působily	působit	k5eAaImAgFnP	působit
odstředivé	odstředivý	k2eAgFnPc1d1	odstředivá
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
navenek	navenek	k6eAd1	navenek
se	se	k3xPyFc4	se
projevující	projevující	k2eAgInSc1d1	projevující
častými	častý	k2eAgInPc7d1	častý
boji	boj	k1gInPc7	boj
o	o	k7c4	o
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgNnSc2	tento
oslabení	oslabení	k1gNnSc2	oslabení
ústřední	ústřední	k2eAgFnSc2d1	ústřední
vlády	vláda	k1gFnSc2	vláda
využila	využít	k5eAaPmAgFnS	využít
arabsko-berberská	arabskoerberský	k2eAgFnSc1d1	arabsko-berberský
armáda	armáda	k1gFnSc1	armáda
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Tárika	Tárikum	k1gNnSc2	Tárikum
ibn	ibn	k?	ibn
Zijád	Zijáda	k1gFnPc2	Zijáda
a	a	k8xC	a
v	v	k7c6	v
osmidenní	osmidenní	k2eAgFnSc6d1	osmidenní
bitvě	bitva	k1gFnSc6	bitva
porazila	porazit	k5eAaPmAgFnS	porazit
vizigótské	vizigótský	k2eAgNnSc4d1	Vizigótské
vojsko	vojsko	k1gNnSc4	vojsko
roku	rok	k1gInSc2	rok
711	[number]	k4	711
u	u	k7c2	u
Jérez	Jéreza	k1gFnPc2	Jéreza
de	de	k?	de
la	la	k1gNnSc1	la
Frontera	Fronter	k1gMnSc2	Fronter
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
příštích	příští	k2eAgNnPc2d1	příští
několik	několik	k4yIc4	několik
staletí	staletí	k1gNnPc2	staletí
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
většinu	většina	k1gFnSc4	většina
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
muslimové	muslim	k1gMnPc1	muslim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Muslimské	muslimský	k2eAgNnSc4d1	muslimské
Španělsko	Španělsko	k1gNnSc4	Španělsko
a	a	k8xC	a
Reconquista	Reconquista	k1gMnSc1	Reconquista
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Berbeři	Berber	k1gMnPc1	Berber
a	a	k8xC	a
Arabové	Arab	k1gMnPc1	Arab
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
zvaní	zvaní	k1gNnPc2	zvaní
Maurové	Maurové	k?	Maurové
<g/>
)	)	kIx)	)
zanechali	zanechat	k5eAaPmAgMnP	zanechat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nesmazatelné	smazatelný	k2eNgFnSc2d1	nesmazatelná
stopy	stopa	k1gFnSc2	stopa
–	–	k?	–
Španělsko	Španělsko	k1gNnSc1	Španělsko
zvané	zvaný	k2eAgNnSc1d1	zvané
Al-Andalus	Al-Andalus	k1gInSc4	Al-Andalus
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejzápadnější	západní	k2eAgFnSc7d3	nejzápadnější
výspou	výspa	k1gFnSc7	výspa
islámského	islámský	k2eAgInSc2d1	islámský
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
929	[number]	k4	929
odtrhla	odtrhnout	k5eAaPmAgFnS	odtrhnout
od	od	k7c2	od
arabské	arabský	k2eAgFnSc2d1	arabská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
samostatný	samostatný	k2eAgInSc4d1	samostatný
córdobský	córdobský	k2eAgInSc4d1	córdobský
chalífát	chalífát	k1gInSc4	chalífát
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
vyspělý	vyspělý	k2eAgInSc4d1	vyspělý
státní	státní	k2eAgInSc4d1	státní
útvar	útvar	k1gInSc4	útvar
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
kulturní	kulturní	k2eAgMnSc1d1	kulturní
a	a	k8xC	a
vzdělanostní	vzdělanostní	k2eAgFnSc7d1	vzdělanostní
úrovní	úroveň	k1gFnSc7	úroveň
převyšoval	převyšovat	k5eAaImAgInS	převyšovat
většinu	většina	k1gFnSc4	většina
soudobých	soudobý	k2eAgInPc2d1	soudobý
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	on	k3xPp3gNnSc2	on
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
rozvinutou	rozvinutý	k2eAgFnSc7d1	rozvinutá
řemeslnou	řemeslný	k2eAgFnSc7d1	řemeslná
výrobou	výroba	k1gFnSc7	výroba
a	a	k8xC	a
obchodem	obchod	k1gInSc7	obchod
<g/>
,	,	kIx,	,
pokročilou	pokročilý	k2eAgFnSc7d1	pokročilá
architekturou	architektura	k1gFnSc7	architektura
a	a	k8xC	a
městskou	městský	k2eAgFnSc7d1	městská
zástavbou	zástavba	k1gFnSc7	zástavba
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
zachovalou	zachovalý	k2eAgFnSc4d1	zachovalá
v	v	k7c6	v
historických	historický	k2eAgNnPc6d1	historické
jádrech	jádro	k1gNnPc6	jádro
měst	město	k1gNnPc2	město
jako	jako	k9	jako
Córdoba	Córdoba	k1gFnSc1	Córdoba
<g/>
,	,	kIx,	,
Granada	Granada	k1gFnSc1	Granada
a	a	k8xC	a
Sevilla	Sevilla	k1gFnSc1	Sevilla
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
Mezquita	Mezquita	k1gFnSc1	Mezquita
v	v	k7c6	v
Córdobě	Córdoba	k1gFnSc6	Córdoba
<g/>
,	,	kIx,	,
přestavěná	přestavěný	k2eAgFnSc1d1	přestavěná
ovšem	ovšem	k9	ovšem
na	na	k7c4	na
katedrálu	katedrála	k1gFnSc4	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
chalífátu	chalífát	k1gInSc2	chalífát
víceméně	víceméně	k9	víceméně
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
koexistovali	koexistovat	k5eAaImAgMnP	koexistovat
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
křesťané	křesťan	k1gMnPc1	křesťan
i	i	k8xC	i
muslimové	muslim	k1gMnPc1	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1031	[number]	k4	1031
se	se	k3xPyFc4	se
však	však	k9	však
jednotný	jednotný	k2eAgInSc1d1	jednotný
córdobský	córdobský	k2eAgInSc1d1	córdobský
chalífát	chalífát	k1gInSc1	chalífát
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
menších	malý	k2eAgNnPc2d2	menší
království	království	k1gNnPc2	království
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
taifas	taifas	k1gInSc1	taifas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
razantnější	razantní	k2eAgInSc4d2	razantnější
nástup	nástup	k1gInSc4	nástup
reconquisty	reconquista	k1gMnSc2	reconquista
–	–	k?	–
znovudobývání	znovudobývání	k1gNnSc1	znovudobývání
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
křesťany	křesťan	k1gMnPc7	křesťan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reconquista	Reconquista	k1gMnSc1	Reconquista
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
Maurové	Maurové	k?	Maurové
Pyrenejský	pyrenejský	k2eAgInSc4d1	pyrenejský
poloostrov	poloostrov	k1gInSc4	poloostrov
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
vítěznou	vítězný	k2eAgFnSc7d1	vítězná
bitvou	bitva	k1gFnSc7	bitva
vizigótského	vizigótský	k2eAgMnSc4d1	vizigótský
šlechtice	šlechtic	k1gMnSc4	šlechtic
Pelaya	Pelayus	k1gMnSc4	Pelayus
proti	proti	k7c3	proti
arabskému	arabský	k2eAgInSc3d1	arabský
trestnému	trestný	k2eAgInSc3d1	trestný
oddílu	oddíl	k1gInSc3	oddíl
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
roku	rok	k1gInSc2	rok
718	[number]	k4	718
nebo	nebo	k8xC	nebo
722	[number]	k4	722
u	u	k7c2	u
Covadongy	Covadong	k1gInPc1	Covadong
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
severu	sever	k1gInSc6	sever
založeno	založen	k2eAgNnSc4d1	založeno
křesťanské	křesťanský	k2eAgNnSc4d1	křesťanské
království	království	k1gNnSc4	království
Asturie	Asturie	k1gFnSc2	Asturie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
do	do	k7c2	do
města	město	k1gNnSc2	město
León	León	k1gInSc1	León
a	a	k8xC	a
přijalo	přijmout	k5eAaPmAgNnS	přijmout
jeho	jeho	k3xOp3gFnSc4	jeho
jméno	jméno	k1gNnSc1	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
tu	tu	k6eAd1	tu
vykrystalizovaly	vykrystalizovat	k5eAaPmAgInP	vykrystalizovat
další	další	k2eAgInPc1d1	další
křesťanské	křesťanský	k2eAgInPc1d1	křesťanský
státečky	státeček	k1gInPc1	státeček
Kastilie	Kastilie	k1gFnSc2	Kastilie
<g/>
,	,	kIx,	,
Galicie	Galicie	k1gFnSc2	Galicie
a	a	k8xC	a
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
existovala	existovat	k5eAaImAgFnS	existovat
Navarra	Navarra	k1gFnSc1	Navarra
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
tu	tu	k6eAd1	tu
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
království	království	k1gNnSc1	království
Aragon	Aragon	k1gInSc1	Aragon
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
státní	státní	k2eAgInPc1d1	státní
celky	celek	k1gInPc1	celek
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zapojily	zapojit	k5eAaPmAgInP	zapojit
do	do	k7c2	do
reconquisty	reconquista	k1gMnSc2	reconquista
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
dlouhodobým	dlouhodobý	k2eAgInSc7d1	dlouhodobý
procesem	proces	k1gInSc7	proces
s	s	k7c7	s
četnými	četný	k2eAgInPc7d1	četný
zvraty	zvrat	k1gInPc7	zvrat
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
válku	válka	k1gFnSc4	válka
střídaly	střídat	k5eAaImAgFnP	střídat
dlouhodobé	dlouhodobý	k2eAgFnPc1d1	dlouhodobá
etapy	etapa	k1gFnPc1	etapa
mírového	mírový	k2eAgNnSc2d1	Mírové
soužití	soužití	k1gNnSc2	soužití
muslimů	muslim	k1gMnPc2	muslim
s	s	k7c7	s
křesťany	křesťan	k1gMnPc7	křesťan
a	a	k8xC	a
židy	žid	k1gMnPc7	žid
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
však	však	k9	však
byly	být	k5eAaImAgFnP	být
maurské	maurský	k2eAgFnPc1d1	maurská
državy	država	k1gFnPc1	država
zatlačovány	zatlačovat	k5eAaImNgFnP	zatlačovat
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
křesťanské	křesťanský	k2eAgInPc1d1	křesťanský
státy	stát	k1gInPc1	stát
rozšiřovaly	rozšiřovat	k5eAaImAgInP	rozšiřovat
svoje	své	k1gNnSc4	své
území	území	k1gNnPc2	území
a	a	k8xC	a
sjednocovaly	sjednocovat	k5eAaImAgInP	sjednocovat
se	s	k7c7	s
(	(	kIx(	(
<g/>
Kastilie	Kastilie	k1gFnSc1	Kastilie
<g/>
,	,	kIx,	,
Aragon	Aragon	k1gNnSc1	Aragon
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozhodující	rozhodující	k2eAgInSc1d1	rozhodující
mezník	mezník	k1gInSc1	mezník
reconquisty	reconquista	k1gMnSc2	reconquista
tvoří	tvořit	k5eAaImIp3nS	tvořit
dobytí	dobytí	k1gNnSc4	dobytí
Toleda	Toledo	k1gNnSc2	Toledo
roku	rok	k1gInSc2	rok
1085	[number]	k4	1085
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
základnou	základna	k1gFnSc7	základna
pro	pro	k7c4	pro
další	další	k2eAgInPc4d1	další
výboje	výboj	k1gInPc4	výboj
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Las	laso	k1gNnPc2	laso
Navas	Navas	k1gInSc1	Navas
de	de	k?	de
Tolosa	Tolosa	k1gFnSc1	Tolosa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1212	[number]	k4	1212
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
maurská	maurský	k2eAgNnPc4d1	maurské
vojska	vojsko	k1gNnPc4	vojsko
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
natolik	natolik	k6eAd1	natolik
zdrcující	zdrcující	k2eAgFnSc4d1	zdrcující
porážku	porážka	k1gFnSc4	porážka
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
bezprostřednímu	bezprostřední	k2eAgInSc3d1	bezprostřední
kolapsu	kolaps	k1gInSc3	kolaps
a	a	k8xC	a
následnému	následný	k2eAgNnSc3d1	následné
dobytí	dobytí	k1gNnSc3	dobytí
Sevilly	Sevilla	k1gFnSc2	Sevilla
a	a	k8xC	a
Córdoby	Córdoba	k1gFnSc2	Córdoba
<g/>
.	.	kIx.	.
</s>
<s>
Maurská	maurský	k2eAgNnPc1d1	maurské
království	království	k1gNnPc1	království
byla	být	k5eAaImAgNnP	být
postupně	postupně	k6eAd1	postupně
likvidována	likvidován	k2eAgNnPc1d1	likvidováno
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
13.	[number]	k4	13.
století	století	k1gNnSc2	století
zbylo	zbýt	k5eAaPmAgNnS	zbýt
pouze	pouze	k6eAd1	pouze
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
emirát	emirát	k1gInSc1	emirát
<g/>
)	)	kIx)	)
Granada	Granada	k1gFnSc1	Granada
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
posledním	poslední	k2eAgMnSc7d1	poslední
maurským	maurský	k2eAgMnSc7d1	maurský
králem	král	k1gMnSc7	král
byl	být	k5eAaImAgMnS	být
Boabdil	Boabdil	k1gMnSc1	Boabdil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
15.	[number]	k4	15.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
15.	[number]	k4	15.
století	století	k1gNnSc2	století
představoval	představovat	k5eAaImAgMnS	představovat
maurské	maurský	k2eAgNnSc1d1	maurské
panství	panství	k1gNnSc1	panství
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
už	už	k6eAd1	už
jen	jen	k9	jen
malý	malý	k2eAgInSc1d1	malý
emirát	emirát	k1gInSc1	emirát
Granada	Granada	k1gFnSc1	Granada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
Kastilie	Kastilie	k1gFnSc2	Kastilie
položil	položit	k5eAaPmAgInS	položit
sňatek	sňatek	k1gInSc1	sňatek
dědičky	dědička	k1gFnSc2	dědička
kastilského	kastilský	k2eAgInSc2d1	kastilský
trůnu	trůn	k1gInSc2	trůn
Isabely	Isabela	k1gFnSc2	Isabela
Kastilské	kastilský	k2eAgFnSc2d1	Kastilská
a	a	k8xC	a
dědice	dědic	k1gMnPc4	dědic
aragonského	aragonský	k2eAgInSc2d1	aragonský
trůnu	trůn	k1gInSc2	trůn
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Aragonského	aragonský	k2eAgInSc2d1	aragonský
(	(	kIx(	(
<g/>
1469	[number]	k4	1469
<g/>
)	)	kIx)	)
základy	základ	k1gInPc1	základ
k	k	k7c3	k
trvalému	trvalý	k2eAgNnSc3d1	trvalé
sjednocení	sjednocení	k1gNnSc3	sjednocení
dvou	dva	k4xCgInPc2	dva
nejmocnějších	mocný	k2eAgInPc2d3	nejmocnější
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
států	stát	k1gInPc2	stát
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
do	do	k7c2	do
španělského	španělský	k2eAgNnSc2d1	španělské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Formálně	formálně	k6eAd1	formálně
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
nástupem	nástup	k1gInSc7	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
jejich	jejich	k3xOp3gMnSc2	jejich
vnuka	vnuk	k1gMnSc2	vnuk
Karla	Karel	k1gMnSc2	Karel
I.	I.	kA	I.
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Isabela	Isabela	k1gFnSc1	Isabela
a	a	k8xC	a
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
vládli	vládnout	k5eAaImAgMnP	vládnout
samostatně	samostatně	k6eAd1	samostatně
každý	každý	k3xTgInSc4	každý
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
království	království	k1gNnSc6	království
a	a	k8xC	a
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
svého	svůj	k3xOyFgMnSc4	svůj
partnera	partner	k1gMnSc4	partner
byli	být	k5eAaImAgMnP	být
pouze	pouze	k6eAd1	pouze
zástupci	zástupce	k1gMnPc1	zástupce
panovníka	panovník	k1gMnSc4	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
za	za	k7c2	za
katolických	katolický	k2eAgNnPc2d1	katolické
Veličenstev	veličenstvo	k1gNnPc2	veličenstvo
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
byli	být	k5eAaImAgMnP	být
nazýváni	nazývat	k5eAaImNgMnP	nazývat
<g/>
,	,	kIx,	,
padla	padnout	k5eAaPmAgFnS	padnout
Granada	Granada	k1gFnSc1	Granada
(	(	kIx(	(
<g/>
1492	[number]	k4	1492
<g/>
)	)	kIx)	)
a	a	k8xC	a
Španělsku	Španělsko	k1gNnSc6	Španělsko
se	se	k3xPyFc4	se
otevřela	otevřít	k5eAaPmAgFnS	otevřít
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
expanzi	expanze	k1gFnSc3	expanze
mimo	mimo	k7c4	mimo
Pyrenejský	pyrenejský	k2eAgInSc4d1	pyrenejský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
jednak	jednak	k8xC	jednak
na	na	k7c6	na
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
již	již	k9	již
mnohem	mnohem	k6eAd1	mnohem
dříve	dříve	k6eAd2	dříve
prosadili	prosadit	k5eAaPmAgMnP	prosadit
Aragonci	Aragonek	k1gMnPc1	Aragonek
(	(	kIx(	(
<g/>
Sicílie	Sicílie	k1gFnSc1	Sicílie
<g/>
,	,	kIx,	,
Neapolsko	Neapolsko	k1gNnSc4	Neapolsko
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
na	na	k7c4	na
nové	nový	k2eAgInPc4d1	nový
zámořské	zámořský	k2eAgInPc4d1	zámořský
objevy	objev	k1gInPc4	objev
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
podporovala	podporovat	k5eAaImAgFnS	podporovat
Kastilie	Kastilie	k1gFnSc1	Kastilie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
objevná	objevný	k2eAgFnSc1d1	objevná
plavba	plavba	k1gFnSc1	plavba
byla	být	k5eAaImAgFnS	být
dílem	dílo	k1gNnSc7	dílo
janovského	janovský	k2eAgMnSc2d1	janovský
námořníka	námořník	k1gMnSc2	námořník
Kryštofa	Kryštof	k1gMnSc2	Kryštof
Kolumba	Kolumbus	k1gMnSc2	Kolumbus
(	(	kIx(	(
<g/>
1492	[number]	k4	1492
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gMnSc4	on
následovali	následovat	k5eAaImAgMnP	následovat
mnozí	mnohý	k2eAgMnPc1d1	mnohý
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
význam	význam	k1gInSc1	význam
slova	slovo	k1gNnSc2	slovo
Španělsko	Španělsko	k1gNnSc1	Španělsko
(	(	kIx(	(
<g/>
Hispanie	Hispanie	k1gFnSc1	Hispanie
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
označující	označující	k2eAgInSc1d1	označující
celý	celý	k2eAgInSc1d1	celý
Pyrenejský	pyrenejský	k2eAgInSc1d1	pyrenejský
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
)	)	kIx)	)
na	na	k7c4	na
označení	označení	k1gNnSc4	označení
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Španělsko	Španělsko	k1gNnSc1	Španělsko
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Habsburků	Habsburk	k1gMnPc2	Habsburk
v	v	k7c6	v
16.	[number]	k4	16.
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
16.	[number]	k4	16.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Španělsko	Španělsko	k1gNnSc1	Španělsko
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
španělských	španělský	k2eAgInPc2d1	španělský
Habsburků	Habsburk	k1gInPc2	Habsburk
Karla	Karel	k1gMnSc2	Karel
V.	V.	kA	V.
(	(	kIx(	(
<g/>
1516	[number]	k4	1516
<g/>
–	–	k?	–
<g/>
1556	[number]	k4	1556
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1556	[number]	k4	1556
<g/>
–	–	k?	–
<g/>
1598	[number]	k4	1598
<g/>
)	)	kIx)	)
stalo	stát	k5eAaPmAgNnS	stát
nejmocnější	mocný	k2eAgFnSc7d3	nejmocnější
zemí	zem	k1gFnSc7	zem
západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
vlády	vláda	k1gFnSc2	vláda
Karla	Karel	k1gMnSc2	Karel
I.	I.	kA	I.
a	a	k8xC	a
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
také	také	k9	také
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Mocenské	mocenský	k2eAgNnSc1d1	mocenské
postavení	postavení	k1gNnSc1	postavení
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
především	především	k9	především
na	na	k7c4	na
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dovážely	dovážet	k5eAaImAgInP	dovážet
do	do	k7c2	do
země	zem	k1gFnSc2	zem
tzv.	tzv.	kA	tzv.
stříbrné	stříbrný	k2eAgInPc1d1	stříbrný
konvoje	konvoj	k1gInPc1	konvoj
z	z	k7c2	z
nově	nově	k6eAd1	nově
objevených	objevený	k2eAgFnPc2d1	objevená
zámořských	zámořský	k2eAgFnPc2d1	zámořská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
rozsahu	rozsah	k1gInSc6	rozsah
habsburských	habsburský	k2eAgFnPc2d1	habsburská
držav	država	k1gFnPc2	država
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
vládl	vládnout	k5eAaImAgMnS	vládnout
nejen	nejen	k6eAd1	nejen
vlastnímu	vlastní	k2eAgNnSc3d1	vlastní
Španělsku	Španělsko	k1gNnSc3	Španělsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
neapolskému	neapolský	k2eAgNnSc3d1	Neapolské
království	království	k1gNnSc3	království
a	a	k8xC	a
Sicílii	Sicílie	k1gFnSc3	Sicílie
<g/>
,	,	kIx,	,
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
a	a	k8xC	a
obrovským	obrovský	k2eAgFnPc3d1	obrovská
zámořským	zámořský	k2eAgFnPc3d1	zámořská
državám	država	k1gFnPc3	država
<g/>
.	.	kIx.	.
</s>
<s>
Právem	právem	k6eAd1	právem
mohl	moct	k5eAaImAgMnS	moct
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gFnSc7	jeho
zemí	zem	k1gFnSc7	zem
Slunce	slunce	k1gNnSc2	slunce
nezapadá	zapadat	k5eNaPmIp3nS	zapadat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgInS	být
dědicem	dědic	k1gMnSc7	dědic
tzv.	tzv.	kA	tzv.
dědičných	dědičný	k2eAgFnPc2d1	dědičná
habsburských	habsburský	k2eAgFnPc2d1	habsburská
zemí	zem	k1gFnPc2	zem
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
přenechal	přenechat	k5eAaPmAgInS	přenechat
roku	rok	k1gInSc2	rok
1521	[number]	k4	1521
<g/>
–	–	k?	–
<g/>
1522	[number]	k4	1522
bruselskými	bruselský	k2eAgMnPc7d1	bruselský
smlouvami	smlouva	k1gFnPc7	smlouva
mladšímu	mladý	k2eAgMnSc3d2	mladší
bratru	bratr	k1gMnSc3	bratr
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1519	[number]	k4	1519
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
korunován	korunovat	k5eAaBmNgInS	korunovat
římským	římský	k2eAgMnSc7d1	římský
císařem	císař	k1gMnSc7	císař
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1580	[number]	k4	1580
byl	být	k5eAaImAgMnS	být
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
zvolen	zvolit	k5eAaPmNgMnS	zvolit
portugalským	portugalský	k2eAgMnSc7d1	portugalský
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
a	a	k8xC	a
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
spojilo	spojit	k5eAaPmAgNnS	spojit
personální	personální	k2eAgNnSc1d1	personální
unií	unie	k1gFnSc7	unie
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Objevení	objevení	k1gNnSc1	objevení
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
vyvrácení	vyvrácení	k1gNnSc2	vyvrácení
zdejších	zdejší	k2eAgFnPc2d1	zdejší
původních	původní	k2eAgFnPc2d1	původní
říší	říš	k1gFnPc2	říš
přineslo	přinést	k5eAaPmAgNnS	přinést
Španělsku	Španělsko	k1gNnSc3	Španělsko
velké	velká	k1gFnSc2	velká
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
produktivně	produktivně	k6eAd1	produktivně
využíváno	využívat	k5eAaImNgNnS	využívat
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
-	-	kIx~	-
z	z	k7c2	z
obchodování	obchodování	k1gNnSc2	obchodování
s	s	k7c7	s
dovezeným	dovezený	k2eAgNnSc7d1	dovezené
zbožím	zboží	k1gNnSc7	zboží
bohatli	bohatnout	k5eAaImAgMnP	bohatnout
především	především	k9	především
nizozemští	nizozemský	k2eAgMnPc1d1	nizozemský
kupci	kupec	k1gMnPc1	kupec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
bohatství	bohatství	k1gNnSc2	bohatství
plynoucí	plynoucí	k2eAgFnSc1d1	plynoucí
z	z	k7c2	z
amerických	americký	k2eAgFnPc2d1	americká
kolonií	kolonie	k1gFnPc2	kolonie
byla	být	k5eAaImAgFnS	být
spotřebována	spotřebovat	k5eAaPmNgFnS	spotřebovat
na	na	k7c4	na
přepychový	přepychový	k2eAgInSc4d1	přepychový
život	život	k1gInSc4	život
panovnického	panovnický	k2eAgInSc2d1	panovnický
dvora	dvůr	k1gInSc2	dvůr
a	a	k8xC	a
především	především	k9	především
na	na	k7c4	na
nákladné	nákladný	k2eAgNnSc4d1	nákladné
válčení	válčení	k1gNnSc4	válčení
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
vedl	vést	k5eAaImAgMnS	vést
zdlouhavé	zdlouhavý	k2eAgFnSc2d1	zdlouhavá
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
o	o	k7c6	o
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
bojoval	bojovat	k5eAaImAgMnS	bojovat
s	s	k7c7	s
protestanty	protestant	k1gMnPc7	protestant
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
Filip	Filip	k1gMnSc1	Filip
válčil	válčit	k5eAaImAgMnS	válčit
v	v	k7c6	v
odbojném	odbojný	k2eAgNnSc6d1	odbojné
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
a	a	k8xC	a
neúspěšně	úspěšně	k6eNd1	úspěšně
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1588	[number]	k4	1588
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
o	o	k7c4	o
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
protestantské	protestantský	k2eAgFnSc2d1	protestantská
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
projevilo	projevit	k5eAaPmAgNnS	projevit
v	v	k7c6	v
ekonomickém	ekonomický	k2eAgNnSc6d1	ekonomické
zaostávání	zaostávání	k1gNnSc6	zaostávání
Španělska	Španělsko	k1gNnSc2	Španělsko
za	za	k7c7	za
vyspělými	vyspělý	k2eAgFnPc7d1	vyspělá
zeměmi	zem	k1gFnPc7	zem
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
vinu	vina	k1gFnSc4	vina
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
neslo	nést	k5eAaImAgNnS	nést
také	také	k9	také
nastolení	nastolení	k1gNnSc4	nastolení
absolutistického	absolutistický	k2eAgInSc2d1	absolutistický
systému	systém	k1gInSc2	systém
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
podvazoval	podvazovat	k5eAaImAgInS	podvazovat
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
i	i	k8xC	i
politickou	politický	k2eAgFnSc4d1	politická
aktivitu	aktivita	k1gFnSc4	aktivita
měšťanstva	měšťanstvo	k1gNnSc2	měšťanstvo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
především	především	k9	především
po	po	k7c6	po
potlačení	potlačení	k1gNnSc6	potlačení
povstání	povstání	k1gNnSc2	povstání
vnitrozemských	vnitrozemský	k2eAgNnPc2d1	vnitrozemské
kastilských	kastilský	k2eAgNnPc2d1	Kastilské
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
probíhalo	probíhat	k5eAaImAgNnS	probíhat
v	v	k7c6	v
letech	let	k1gInPc6	let
1520	[number]	k4	1520
<g/>
–	–	k?	–
<g/>
1521	[number]	k4	1521
a	a	k8xC	a
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
povstání	povstání	k1gNnSc2	povstání
komunérů	komunér	k1gInPc2	komunér
<g/>
.	.	kIx.	.
</s>
<s>
Španělskému	španělský	k2eAgNnSc3d1	španělské
hospodářství	hospodářství	k1gNnSc3	hospodářství
neprospělo	prospět	k5eNaPmAgNnS	prospět
ani	ani	k8xC	ani
vyhnání	vyhnání	k1gNnSc1	vyhnání
morisků	morisk	k1gInPc2	morisk
(	(	kIx(	(
<g/>
pokřtěných	pokřtěný	k2eAgMnPc2d1	pokřtěný
Maurů	Maur	k1gMnPc2	Maur
<g/>
)	)	kIx)	)
v	v	k7c6	v
letech	let	k1gInPc6	let
1609	[number]	k4	1609
<g/>
–	–	k?	–
<g/>
1614	[number]	k4	1614
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Filipa	Filip	k1gMnSc2	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1598	[number]	k4	1598
<g/>
–	–	k?	–
<g/>
1621	[number]	k4	1621
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
úpadek	úpadek	k1gInSc1	úpadek
země	zem	k1gFnSc2	zem
naplno	naplno	k6eAd1	naplno
<g/>
.	.	kIx.	.
</s>
<s>
Řemeslná	řemeslný	k2eAgFnSc1d1	řemeslná
výroba	výroba	k1gFnSc1	výroba
se	se	k3xPyFc4	se
nerozvíjela	rozvíjet	k5eNaImAgFnS	rozvíjet
<g/>
,	,	kIx,	,
nevznikaly	vznikat	k5eNaImAgFnP	vznikat
tu	tu	k6eAd1	tu
ani	ani	k8xC	ani
manufaktury	manufaktura	k1gFnSc2	manufaktura
jako	jako	k9	jako
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
byl	být	k5eAaImAgInS	být
například	například	k6eAd1	například
dostatek	dostatek	k1gInSc1	dostatek
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
suroviny	surovina	k1gFnSc2	surovina
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
sukna	sukno	k1gNnSc2	sukno
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
Andalusii	Andalusie	k1gFnSc6	Andalusie
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
chovaly	chovat	k5eAaImAgFnP	chovat
ovce	ovce	k1gFnPc4	ovce
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgFnPc1d1	obchodní
transakce	transakce	k1gFnPc1	transakce
byly	být	k5eAaImAgFnP	být
podřízeny	podřízen	k2eAgFnPc1d1	podřízena
vysoké	vysoká	k1gFnPc1	vysoká
dani	daň	k1gFnSc3	daň
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
alcabale	alcabal	k1gInSc6	alcabal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důsledky	důsledek	k1gInPc1	důsledek
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
úpadku	úpadek	k1gInSc2	úpadek
Španělska	Španělsko	k1gNnSc2	Španělsko
se	se	k3xPyFc4	se
v	v	k7c6	v
politické	politický	k2eAgFnSc6d1	politická
oblasti	oblast	k1gFnSc6	oblast
projevily	projevit	k5eAaPmAgInP	projevit
během	během	k7c2	během
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
ještě	ještě	k6eAd1	ještě
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
mocnost	mocnost	k1gFnSc1	mocnost
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
konci	konec	k1gInSc6	konec
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
druhořadým	druhořadý	k2eAgInSc7d1	druhořadý
evropským	evropský	k2eAgInSc7d1	evropský
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Při	při	k7c6	při
nájezdech	nájezd	k1gInPc6	nájezd
berberských	berberský	k2eAgInPc2d1	berberský
(	(	kIx(	(
<g/>
barbarských	barbarský	k2eAgInPc2d1	barbarský
<g/>
)	)	kIx)	)
pirátů	pirát	k1gMnPc2	pirát
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
v	v	k7c6	v
16.	[number]	k4	16.
až	až	k9	až
19.	[number]	k4	19.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgFnP	být
statisíce	statisíce	k1gInPc4	statisíce
obyvatel	obyvatel	k1gMnPc2	obyvatel
Španělska	Španělsko	k1gNnSc2	Španělsko
odvlečeny	odvlečen	k2eAgMnPc4d1	odvlečen
do	do	k7c2	do
otroctví	otroctví	k1gNnPc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
piráty	pirát	k1gMnPc7	pirát
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k6eAd1	mnoho
Maurů	Maur	k1gMnPc2	Maur
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
mstili	mstít	k5eAaImAgMnP	mstít
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
vyhnání	vyhnání	k1gNnSc4	vyhnání
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
maurské	maurský	k2eAgFnSc2d1	maurská
(	(	kIx(	(
<g/>
arabské	arabský	k2eAgFnSc2d1	arabská
<g/>
)	)	kIx)	)
Granady	Granada	k1gFnSc2	Granada
roku	rok	k1gInSc2	rok
1492	[number]	k4	1492
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
severoafrických	severoafrický	k2eAgMnPc2d1	severoafrický
pirátů	pirát	k1gMnPc2	pirát
se	se	k3xPyFc4	se
počátkem	počátkem	k7c2	počátkem
16.	[number]	k4	16.
století	století	k1gNnSc2	století
postavil	postavit	k5eAaPmAgMnS	postavit
obávaný	obávaný	k2eAgMnSc1d1	obávaný
turecký	turecký	k2eAgMnSc1d1	turecký
pirát	pirát	k1gMnSc1	pirát
Chajruddín	Chajruddín	k1gMnSc1	Chajruddín
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1571	[number]	k4	1571
byla	být	k5eAaImAgFnS	být
spojená	spojený	k2eAgFnSc1d1	spojená
turecko-alžírská	tureckolžírský	k2eAgFnSc1d1	turecko-alžírský
flotila	flotila	k1gFnSc1	flotila
poražena	porazit	k5eAaPmNgFnS	porazit
Španěly	Španěl	k1gMnPc7	Španěl
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc7	jejich
spojenci	spojenec	k1gMnPc7	spojenec
v	v	k7c6	v
krvavé	krvavý	k2eAgFnSc6d1	krvavá
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Lepanta	Lepant	k1gMnSc2	Lepant
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
získali	získat	k5eAaPmAgMnP	získat
piráti	pirát	k1gMnPc1	pirát
svou	svůj	k3xOyFgFnSc4	svůj
převahu	převaha	k1gFnSc4	převaha
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
středomořských	středomořský	k2eAgInPc2d1	středomořský
ostrovů	ostrov	k1gInPc2	ostrov
bylo	být	k5eAaImAgNnS	být
prakticky	prakticky	k6eAd1	prakticky
vylidněno	vylidnit	k5eAaPmNgNnS	vylidnit
<g/>
,	,	kIx,	,
obyvatelé	obyvatel	k1gMnPc1	obyvatel
byli	být	k5eAaImAgMnP	být
odvlečeni	odvléct	k5eAaPmNgMnP	odvléct
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
nebo	nebo	k8xC	nebo
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
byl	být	k5eAaImAgMnS	být
unesen	unést	k5eAaPmNgMnS	unést
i	i	k8xC	i
mladý	mladý	k2eAgMnSc1d1	mladý
Miguel	Miguel	k1gMnSc1	Miguel
de	de	k?	de
Cervantes	Cervantes	k1gMnSc1	Cervantes
a	a	k8xC	a
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
žil	žít	k5eAaImAgMnS	žít
jako	jako	k9	jako
otrok	otrok	k1gMnSc1	otrok
v	v	k7c6	v
Alžíru	Alžír	k1gInSc6	Alžír
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgMnS	být
vykoupen	vykoupen	k2eAgMnSc1d1	vykoupen
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Definitivní	definitivní	k2eAgInSc1d1	definitivní
konec	konec	k1gInSc1	konec
pirátství	pirátství	k1gNnPc2	pirátství
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
nastal	nastat	k5eAaPmAgInS	nastat
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
Alžíru	Alžír	k1gInSc2	Alžír
Francouzi	Francouz	k1gMnPc7	Francouz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kosmopolitnost	Kosmopolitnost	k1gFnSc1	Kosmopolitnost
společnosti	společnost	k1gFnSc2	společnost
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
vyhnání	vyhnání	k1gNnSc6	vyhnání
Morisků	Morisk	k1gInPc2	Morisk
a	a	k8xC	a
židů	žid	k1gMnPc2	žid
zničena	zničit	k5eAaPmNgFnS	zničit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnoho	mnoho	k4c4	mnoho
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
národností	národnost	k1gFnPc2	národnost
a	a	k8xC	a
kultur	kultura	k1gFnPc2	kultura
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
architektuře	architektura	k1gFnSc6	architektura
<g/>
,	,	kIx,	,
jídle	jídlo	k1gNnSc6	jídlo
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgFnPc6d1	další
věcech	věc	k1gFnPc6	věc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
18.	[number]	k4	18.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
zemřel	zemřít	k5eAaPmAgMnS	zemřít
poslední	poslední	k2eAgMnSc1d1	poslední
španělský	španělský	k2eAgMnSc1d1	španělský
král	král	k1gMnSc1	král
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Habsburků	Habsburk	k1gMnPc2	Habsburk
Karel	Karel	k1gMnSc1	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
zanechal	zanechat	k5eAaPmAgMnS	zanechat
mužského	mužský	k2eAgMnSc4d1	mužský
dědice	dědic	k1gMnSc4	dědic
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
války	válka	k1gFnPc1	válka
o	o	k7c4	o
dědictví	dědictví	k1gNnSc4	dědictví
španělské	španělský	k2eAgFnSc2d1	španělská
(	(	kIx(	(
<g/>
1701	[number]	k4	1701
<g/>
–	–	k?	–
<g/>
1714	[number]	k4	1714
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
zapojily	zapojit	k5eAaPmAgFnP	zapojit
všechny	všechen	k3xTgFnPc1	všechen
tehdejší	tehdejší	k2eAgFnPc1d1	tehdejší
evropské	evropský	k2eAgFnPc1d1	Evropská
mocnosti	mocnost	k1gFnPc1	mocnost
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
,	,	kIx,	,
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
španělskou	španělský	k2eAgFnSc4d1	španělská
korunu	koruna	k1gFnSc4	koruna
usilovali	usilovat	k5eAaImAgMnP	usilovat
především	především	k9	především
rakouští	rakouský	k2eAgMnPc1d1	rakouský
Habsburkové	Habsburk	k1gMnPc1	Habsburk
a	a	k8xC	a
francouzští	francouzský	k2eAgMnPc1d1	francouzský
Bourboni	Bourbon	k1gMnPc1	Bourbon
<g/>
.	.	kIx.	.
</s>
<s>
Uspěli	uspět	k5eAaPmAgMnP	uspět
nakonec	nakonec	k6eAd1	nakonec
Bourboni	Bourbon	k1gMnPc1	Bourbon
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
museli	muset	k5eAaImAgMnP	muset
zaručit	zaručit	k5eAaPmF	zaručit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdy	nikdy	k6eAd1	nikdy
nedojde	dojít	k5eNaPmIp3nS	dojít
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
francouzského	francouzský	k2eAgInSc2d1	francouzský
a	a	k8xC	a
španělského	španělský	k2eAgInSc2d1	španělský
trůnu	trůn	k1gInSc2	trůn
v	v	k7c6	v
rukou	ruka	k1gFnSc7	ruka
jedné	jeden	k4xCgFnSc2	jeden
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
též	též	k9	též
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
své	svůj	k3xOyFgFnPc4	svůj
pozice	pozice	k1gFnPc4	pozice
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tamní	tamní	k2eAgNnSc1d1	tamní
území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
postoupeno	postoupit	k5eAaPmNgNnS	postoupit
jakožto	jakožto	k8xS	jakožto
kompenzace	kompenzace	k1gFnPc4	kompenzace
rakouským	rakouský	k2eAgInPc3d1	rakouský
Habsburkům	Habsburk	k1gInPc3	Habsburk
a	a	k8xC	a
rodu	rod	k1gInSc3	rod
savojských	savojský	k2eAgMnPc2d1	savojský
vévodů	vévoda	k1gMnPc2	vévoda
<g/>
.	.	kIx.	.
</s>
<s>
Války	válka	k1gFnPc1	válka
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
slabost	slabost	k1gFnSc4	slabost
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
území	území	k1gNnSc4	území
jimi	on	k3xPp3gMnPc7	on
bylo	být	k5eAaImAgNnS	být
poničeno	poničit	k5eAaPmNgNnS	poničit
<g/>
.	.	kIx.	.
</s>
<s>
Španělsko	Španělsko	k1gNnSc4	Španělsko
během	během	k7c2	během
nich	on	k3xPp3gMnPc2	on
přestalo	přestat	k5eAaPmAgNnS	přestat
být	být	k5eAaImF	být
vnímáno	vnímat	k5eAaImNgNnS	vnímat
jako	jako	k9	jako
velmoc	velmoc	k1gFnSc1	velmoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
19.	[number]	k4	19.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
podnikne	podniknout	k5eAaPmIp3nS	podniknout
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
na	na	k7c4	na
španělský	španělský	k2eAgInSc4d1	španělský
trůn	trůn	k1gInSc4	trůn
dosadil	dosadit	k5eAaPmAgMnS	dosadit
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
Josepha	Joseph	k1gMnSc4	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1808-1814	[number]	k4	1808-1814
probíhal	probíhat	k5eAaImAgInS	probíhat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
boj	boj	k1gInSc1	boj
o	o	k7c4	o
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Cádizu	Cádiz	k1gInSc6	Cádiz
přijata	přijmout	k5eAaPmNgFnS	přijmout
první	první	k4xOgFnSc1	první
španělská	španělský	k2eAgFnSc1d1	španělská
ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
na	na	k7c4	na
dobové	dobový	k2eAgInPc4d1	dobový
poměry	poměr	k1gInPc4	poměr
velmi	velmi	k6eAd1	velmi
liberální	liberální	k2eAgInSc1d1	liberální
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
po	po	k7c6	po
Vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
kongresu	kongres	k1gInSc6	kongres
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
návrat	návrat	k1gInSc4	návrat
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
VII	VII	kA	VII
<g/>
.	.	kIx.	.
na	na	k7c4	na
španělský	španělský	k2eAgInSc4d1	španělský
trůn	trůn	k1gInSc4	trůn
a	a	k8xC	a
obnovení	obnovení	k1gNnSc4	obnovení
absolutistických	absolutistický	k2eAgInPc2d1	absolutistický
pořádků	pořádek	k1gInPc2	pořádek
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
19.	[number]	k4	19.
století	století	k1gNnSc6	století
bylo	být	k5eAaImAgNnS	být
Španělsko	Španělsko	k1gNnSc1	Španělsko
chudým	chudý	k2eAgInSc7d1	chudý
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
války	válka	k1gFnPc1	válka
za	za	k7c4	za
osvobození	osvobození	k1gNnSc4	osvobození
od	od	k7c2	od
španělského	španělský	k2eAgInSc2d1	španělský
koloniálního	koloniální	k2eAgInSc2d1	koloniální
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
představiteli	představitel	k1gMnPc7	představitel
tohoto	tento	k3xDgNnSc2	tento
osvobozeneckého	osvobozenecký	k2eAgNnSc2d1	osvobozenecké
hnutí	hnutí	k1gNnSc2	hnutí
byli	být	k5eAaImAgMnP	být
Simón	Simón	k1gMnSc1	Simón
Bolívar	Bolívar	k1gInSc1	Bolívar
a	a	k8xC	a
José	José	k1gNnSc1	José
de	de	k?	de
San	San	k1gMnSc1	San
Martín	Martín	k1gMnSc1	Martín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
válek	válka	k1gFnPc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
přišlo	přijít	k5eAaPmAgNnS	přijít
Španělské	španělský	k2eAgNnSc1d1	španělské
impérium	impérium	k1gNnSc4	impérium
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
dvou	dva	k4xCgNnPc6	dva
desetiletích	desetiletí	k1gNnPc6	desetiletí
19.	[number]	k4	19.
století	století	k1gNnSc2	století
o	o	k7c4	o
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgFnPc2	svůj
amerických	americký	k2eAgFnPc2d1	americká
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
19.	[number]	k4	19.
století	století	k1gNnPc2	století
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nesl	nést	k5eAaImAgMnS	nést
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
bojů	boj	k1gInPc2	boj
mezi	mezi	k7c7	mezi
zastánci	zastánce	k1gMnPc7	zastánce
liberální	liberální	k2eAgFnSc2d1	liberální
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
absolutistické	absolutistický	k2eAgFnSc2d1	absolutistická
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
karlistických	karlistický	k2eAgFnPc2d1	karlistická
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
sporů	spor	k1gInPc2	spor
mezi	mezi	k7c4	mezi
monarchisty	monarchista	k1gMnPc4	monarchista
a	a	k8xC	a
republikány	republikán	k1gMnPc4	republikán
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1873-1874	[number]	k4	1873-1874
existovala	existovat	k5eAaImAgFnS	existovat
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
tzv.	tzv.	kA	tzv.
1.	[number]	k4	1.
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
Španělsko-americká	španělskomerický	k2eAgFnSc1d1	španělsko-americká
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgNnSc4	který
Španělsko	Španělsko	k1gNnSc1	Španělsko
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
ostrov	ostrov	k1gInSc4	ostrov
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
,	,	kIx,	,
o	o	k7c4	o
Portoriko	Portoriko	k1gNnSc4	Portoriko
a	a	k8xC	a
Filipíny	Filipíny	k1gFnPc4	Filipíny
a	a	k8xC	a
menší	malý	k2eAgInPc4d2	menší
ostrovy	ostrov	k1gInPc4	ostrov
v	v	k7c6	v
Mikronésii	Mikronésie	k1gFnSc6	Mikronésie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
20.	[number]	k4	20.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Počátek	počátek	k1gInSc1	počátek
20.	[number]	k4	20.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
klidu	klid	k1gInSc2	klid
<g/>
,	,	kIx,	,
míru	mír	k1gInSc2	mír
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Politická	politický	k2eAgFnSc1d1	politická
krize	krize	k1gFnSc1	krize
vedla	vést	k5eAaImAgFnS	vést
však	však	k9	však
nejprve	nejprve	k6eAd1	nejprve
k	k	k7c3	k
nastolení	nastolení	k1gNnSc3	nastolení
diktatury	diktatura	k1gFnSc2	diktatura
Miguela	Miguela	k1gFnSc1	Miguela
Primo	primo	k1gNnSc1	primo
de	de	k?	de
Rivery	Rivera	k1gFnSc2	Rivera
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
k	k	k7c3	k
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
druhé	druhý	k4xOgFnSc2	druhý
španělské	španělský	k2eAgFnSc2d1	španělská
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
odchodu	odchod	k1gInSc2	odchod
španělského	španělský	k2eAgMnSc2d1	španělský
krále	král	k1gMnSc2	král
Alfonse	Alfons	k1gMnSc2	Alfons
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
.	.	kIx.	.
</s>
<s>
Sílící	sílící	k2eAgInPc1d1	sílící
sociální	sociální	k2eAgInPc1d1	sociální
a	a	k8xC	a
politické	politický	k2eAgInPc1d1	politický
konflikty	konflikt	k1gInPc1	konflikt
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
vypuknutí	vypuknutí	k1gNnSc3	vypuknutí
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
přes	přes	k7c4	přes
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
nástupem	nástup	k1gInSc7	nástup
diktátora	diktátor	k1gMnSc2	diktátor
Francisca	Franciscus	k1gMnSc2	Franciscus
Franca	Franca	k?	Franca
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vládl	vládnout	k5eAaImAgInS	vládnout
až	až	k6eAd1	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
a	a	k8xC	a
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
nechal	nechat	k5eAaPmAgMnS	nechat
Franco	Franco	k6eAd1	Franco
popravit	popravit	k5eAaPmF	popravit
na	na	k7c4	na
180 000	[number]	k4	180 000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
za	za	k7c2	za
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
Franco	Franco	k6eAd1	Franco
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Španělsko	Španělsko	k1gNnSc1	Španělsko
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
monarchií	monarchie	k1gFnPc2	monarchie
a	a	k8xC	a
připravil	připravit	k5eAaPmAgMnS	připravit
předání	předání	k1gNnSc4	předání
moci	moct	k5eAaImF	moct
vnukovi	vnuk	k1gMnSc3	vnuk
svrženého	svržený	k2eAgMnSc2d1	svržený
krále	král	k1gMnSc4	král
Juanu	Juan	k1gMnSc3	Juan
Carlosovi	Carlos	k1gMnSc3	Carlos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgMnSc1	tento
král	král	k1gMnSc1	král
má	mít	k5eAaImIp3nS	mít
převážně	převážně	k6eAd1	převážně
reprezentativní	reprezentativní	k2eAgFnSc4d1	reprezentativní
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
pokusu	pokus	k1gInSc2	pokus
o	o	k7c4	o
vojenský	vojenský	k2eAgInSc4d1	vojenský
převrat	převrat	k1gInSc4	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
jednoznačně	jednoznačně	k6eAd1	jednoznačně
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
podpořit	podpořit	k5eAaPmF	podpořit
vzbouřence	vzbouřenec	k1gMnSc4	vzbouřenec
a	a	k8xC	a
postavil	postavit	k5eAaPmAgMnS	postavit
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
formální	formální	k2eAgFnSc7d1	formální
i	i	k8xC	i
neformální	formální	k2eNgFnSc7d1	neformální
autoritou	autorita	k1gFnSc7	autorita
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
úřadující	úřadující	k2eAgFnSc2d1	úřadující
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
pomohl	pomoct	k5eAaPmAgMnS	pomoct
k	k	k7c3	k
potlačení	potlačení	k1gNnSc3	potlačení
pokusu	pokus	k1gInSc2	pokus
o	o	k7c4	o
převrat	převrat	k1gInSc4	převrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zachovávalo	zachovávat	k5eAaImAgNnS	zachovávat
Španělsko	Španělsko	k1gNnSc4	Španělsko
neutralitu	neutralita	k1gFnSc4	neutralita
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
španělští	španělský	k2eAgMnPc1d1	španělský
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
z	z	k7c2	z
Modré	modrý	k2eAgFnSc2d1	modrá
divize	divize	k1gFnSc2	divize
bojovali	bojovat	k5eAaImAgMnP	bojovat
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
španělské	španělský	k2eAgFnSc2d1	španělská
vlády	vláda	k1gFnSc2	vláda
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
proti	proti	k7c3	proti
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
postupně	postupně	k6eAd1	postupně
z	z	k7c2	z
izolace	izolace	k1gFnSc2	izolace
a	a	k8xC	a
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
jednoznačně	jednoznačně	k6eAd1	jednoznačně
antikomunistické	antikomunistický	k2eAgFnSc3d1	antikomunistická
vládě	vláda	k1gFnSc3	vláda
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
ve	v	k7c6	v
studené	studený	k2eAgFnSc6d1	studená
válce	válka	k1gFnSc6	válka
spojencem	spojenec	k1gMnSc7	spojenec
západního	západní	k2eAgInSc2d1	západní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
zažívalo	zažívat	k5eAaImAgNnS	zažívat
nebývalý	nebývalý	k2eAgInSc4d1	nebývalý
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
rozvoj	rozvoj	k1gInSc4	rozvoj
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
turistického	turistický	k2eAgInSc2d1	turistický
ruchu	ruch	k1gInSc2	ruch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
bylo	být	k5eAaImAgNnS	být
Maroku	Maroko	k1gNnSc3	Maroko
postoupeno	postoupen	k2eAgNnSc4d1	postoupeno
Španělské	španělský	k2eAgNnSc4d1	španělské
Maroko	Maroko	k1gNnSc4	Maroko
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
se	se	k3xPyFc4	se
Španělsko	Španělsko	k1gNnSc1	Španělsko
stáhlo	stáhnout	k5eAaPmAgNnS	stáhnout
ze	z	k7c2	z
Západní	západní	k2eAgFnSc2d1	západní
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
členem	člen	k1gInSc7	člen
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
do	do	k7c2	do
eurozóny	eurozóna	k1gFnSc2	eurozóna
<g/>
,	,	kIx,	,
když	když	k8xS	když
kurs	kurs	k1gInSc1	kurs
národní	národní	k2eAgFnSc2d1	národní
měny	měna	k1gFnSc2	měna
svázalo	svázat	k5eAaPmAgNnS	svázat
pevným	pevný	k2eAgInSc7d1	pevný
přepočítacím	přepočítací	k2eAgInSc7d1	přepočítací
kurzem	kurz	k1gInSc7	kurz
s	s	k7c7	s
jednotnou	jednotný	k2eAgFnSc7d1	jednotná
evropskou	evropský	k2eAgFnSc7d1	Evropská
měnou	měna	k1gFnSc7	měna
eurem	euro	k1gNnSc7	euro
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
později	pozdě	k6eAd2	pozdě
přestoupilo	přestoupit	k5eAaPmAgNnS	přestoupit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
21.	[number]	k4	21.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Od	od	k7c2	od
1.	[number]	k4	1.
ledna	leden	k1gInSc2	leden
2002	[number]	k4	2002
zavedla	zavést	k5eAaPmAgFnS	zavést
země	země	k1gFnSc1	země
euro	euro	k1gNnSc4	euro
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
předchozí	předchozí	k2eAgFnSc4d1	předchozí
španělskou	španělský	k2eAgFnSc4d1	španělská
pesetu	peseta	k1gFnSc4	peseta
<g/>
.	.	kIx.	.
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
ekonomika	ekonomika	k1gFnSc1	ekonomika
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
rychle	rychle	k6eAd1	rychle
rostla	růst	k5eAaImAgFnS	růst
zejména	zejména	k9	zejména
následkem	následkem	k7c2	následkem
stavebního	stavební	k2eAgInSc2d1	stavební
boomu	boom	k1gInSc2	boom
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
však	však	k9	však
skončilo	skončit	k5eAaPmAgNnS	skončit
s	s	k7c7	s
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
krizí	krize	k1gFnSc7	krize
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
21.	[number]	k4	21.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
dobou	doba	k1gFnSc7	doba
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgFnSc2d1	silná
imigrace	imigrace	k1gFnSc2	imigrace
obyvatel	obyvatel	k1gMnPc2	obyvatel
zejména	zejména	k9	zejména
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Maroka	Maroko	k1gNnSc2	Maroko
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
11.	[number]	k4	11.
března	březen	k1gInSc2	březen
2004	[number]	k4	2004
se	se	k3xPyFc4	se
madridské	madridský	k2eAgInPc1d1	madridský
příměstské	příměstský	k2eAgInPc1d1	příměstský
vlaky	vlak	k1gInPc1	vlak
staly	stát	k5eAaPmAgInP	stát
terčem	terč	k1gInSc7	terč
teroristických	teroristický	k2eAgInPc2d1	teroristický
útoků	útok	k1gInPc2	útok
organizace	organizace	k1gFnSc2	organizace
al-Káida	al-Káida	k1gFnSc1	al-Káida
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
útocích	útok	k1gInPc6	útok
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
192	[number]	k4	192
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
1460	[number]	k4	1460
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
útoky	útok	k1gInPc1	útok
a	a	k8xC	a
neobratná	obratný	k2eNgFnSc1d1	neobratná
snaha	snaha	k1gFnSc1	snaha
španělské	španělský	k2eAgFnSc2d1	španělská
vlády	vláda	k1gFnSc2	vláda
připsat	připsat	k5eAaPmF	připsat
je	on	k3xPp3gMnPc4	on
ETA	ETA	kA	ETA
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
nečekaného	čekaný	k2eNgNnSc2d1	nečekané
vítězství	vítězství	k1gNnSc2	vítězství
socialistů	socialist	k1gMnPc2	socialist
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
(	(	kIx(	(
<g/>
42	[number]	k4	42
<g/>
,	,	kIx,	,
<g/>
59	[number]	k4	59
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
favorizovanými	favorizovaný	k2eAgMnPc7d1	favorizovaný
lidovci	lidovec	k1gMnPc7	lidovec
Josého	Josého	k2eAgFnSc1d1	Josého
Aznara	Aznara	k1gFnSc1	Aznara
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důvodem	důvod	k1gInSc7	důvod
byl	být	k5eAaImAgInS	být
liknavý	liknavý	k2eAgInSc1d1	liknavý
postup	postup	k1gInSc1	postup
při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
ekologické	ekologický	k2eAgFnSc2d1	ekologická
katastrofy	katastrofa	k1gFnSc2	katastrofa
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
způsobila	způsobit	k5eAaPmAgFnS	způsobit
havárie	havárie	k1gFnSc1	havárie
ropného	ropný	k2eAgInSc2d1	ropný
tankeru	tanker	k1gInSc2	tanker
Prestige	Prestig	k1gFnSc2	Prestig
u	u	k7c2	u
břehů	břeh	k1gInPc2	břeh
Galicie	Galicie	k1gFnSc2	Galicie
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
a	a	k8xC	a
Aznarova	Aznarův	k2eAgFnSc1d1	Aznarova
podpora	podpora	k1gFnSc1	podpora
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
<g/>
José	José	k1gNnSc1	José
Luis	Luisa	k1gFnPc2	Luisa
Rodríguez	Rodrígueza	k1gFnPc2	Rodrígueza
Zapatero	Zapatero	k1gNnSc1	Zapatero
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
Španělská	španělský	k2eAgFnSc1d1	španělská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
PSOE	PSOE	kA	PSOE
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
své	své	k1gNnSc4	své
postavení	postavení	k1gNnSc2	postavení
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
obhájila	obhájit	k5eAaPmAgFnS	obhájit
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
ještě	ještě	k6eAd1	ještě
posílila	posílit	k5eAaPmAgFnS	posílit
<g/>
,	,	kIx,	,
když	když	k8xS	když
místo	místo	k7c2	místo
164	[number]	k4	164
získala	získat	k5eAaPmAgFnS	získat
169	[number]	k4	169
poslaneckých	poslanecký	k2eAgInPc2d1	poslanecký
mandátů	mandát	k1gInPc2	mandát
(	(	kIx(	(
<g/>
43	[number]	k4	43
<g/>
,	,	kIx,	,
<g/>
87	[number]	k4	87
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
oslabily	oslabit	k5eAaPmAgFnP	oslabit
regionální	regionální	k2eAgFnPc1d1	regionální
(	(	kIx(	(
<g/>
nacionalistické	nacionalistický	k2eAgFnPc1d1	nacionalistická
<g/>
)	)	kIx)	)
a	a	k8xC	a
menší	malý	k2eAgFnPc1d2	menší
strany	strana	k1gFnPc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Španělsko	Španělsko	k1gNnSc4	Španělsko
těžce	těžce	k6eAd1	těžce
postihla	postihnout	k5eAaPmAgFnS	postihnout
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Evropskou	evropský	k2eAgFnSc4d1	Evropská
unii	unie	k1gFnSc4	unie
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
na	na	k7c4	na
22	[number]	k4	22
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2011	[number]	k4	2011
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
volby	volba	k1gFnSc2	volba
kandidát	kandidát	k1gMnSc1	kandidát
opoziční	opoziční	k2eAgFnSc2d1	opoziční
Lidové	lidový	k2eAgFnSc2d1	lidová
strany	strana	k1gFnSc2	strana
Mariano	Mariana	k1gFnSc5	Mariana
Rajoy	Rajo	k2eAgInPc1d1	Rajo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
baskická	baskický	k2eAgFnSc1d1	baskická
separatistická	separatistický	k2eAgFnSc1d1	separatistická
organizace	organizace	k1gFnSc1	organizace
ETA	ETA	kA	ETA
trvalé	trvalý	k2eAgNnSc1d1	trvalé
příměří	příměří	k1gNnSc1	příměří
a	a	k8xC	a
o	o	k7c4	o
nezávislost	nezávislost	k1gFnSc4	nezávislost
Baskicka	Baskicko	k1gNnSc2	Baskicko
na	na	k7c6	na
Španělsku	Španělsko	k1gNnSc6	Španělsko
chce	chtít	k5eAaImIp3nS	chtít
usilovat	usilovat	k5eAaImF	usilovat
pouze	pouze	k6eAd1	pouze
mírovou	mírový	k2eAgFnSc7d1	mírová
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Katalánsku	Katalánsko	k1gNnSc6	Katalánsko
v	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
regionální	regionální	k2eAgFnPc1d1	regionální
volby	volba	k1gFnPc1	volba
separatistické	separatistický	k2eAgFnSc2d1	separatistická
strany	strana	k1gFnSc2	strana
usilující	usilující	k2eAgFnSc2d1	usilující
o	o	k7c4	o
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
o	o	k7c4	o
republikánské	republikánský	k2eAgNnSc4d1	republikánské
zřízení	zřízení	k1gNnSc4	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Katalánský	katalánský	k2eAgMnSc1d1	katalánský
premiér	premiér	k1gMnSc1	premiér
Carles	Carles	k1gMnSc1	Carles
Puigdemont	Puigdemont	k1gMnSc1	Puigdemont
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
na	na	k7c6	na
1.	[number]	k4	1.
říjen	říjen	k1gInSc1	říjen
2017	[number]	k4	2017
referendum	referendum	k1gNnSc1	referendum
o	o	k7c6	o
nezávislosti	nezávislost	k1gFnSc6	nezávislost
Katalánska	Katalánsko	k1gNnSc2	Katalánsko
<g/>
.	.	kIx.	.
27.	[number]	k4	27.
října	říjen	k1gInSc2	říjen
2017	[number]	k4	2017
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
parlament	parlament	k1gInSc1	parlament
Katalánska	Katalánsko	k1gNnSc2	Katalánsko
na	na	k7c6	na
základě	základ	k1gInSc6	základ
hlasování	hlasování	k1gNnSc2	hlasování
nezávislost	nezávislost	k1gFnSc1	nezávislost
na	na	k7c6	na
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
Španělska	Španělsko	k1gNnSc2	Španělsko
však	však	k9	však
odsouhlasil	odsouhlasit	k5eAaPmAgMnS	odsouhlasit
omezení	omezení	k1gNnSc4	omezení
autonomie	autonomie	k1gFnSc2	autonomie
a	a	k8xC	a
převzetí	převzetí	k1gNnSc2	převzetí
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
Katalánskem	Katalánsko	k1gNnSc7	Katalánsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Katalánska	Katalánsko	k1gNnSc2	Katalánsko
demonstrovalo	demonstrovat	k5eAaBmAgNnS	demonstrovat
několikrát	několikrát	k6eAd1	několikrát
až	až	k9	až
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
katalánské	katalánský	k2eAgFnSc2d1	katalánská
regionální	regionální	k2eAgFnSc2d1	regionální
vlády	vláda	k1gFnSc2	vláda
Puigdemont	Puigdemonto	k1gNnPc2	Puigdemonto
byl	být	k5eAaImAgInS	být
posléze	posléze	k6eAd1	posléze
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Rajoyovy	Rajoyův	k2eAgFnSc2d1	Rajoyův
vlády	vláda	k1gFnSc2	vláda
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
povstání	povstání	k1gNnSc2	povstání
a	a	k8xC	a
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
Finsko	Finsko	k1gNnSc4	Finsko
a	a	k8xC	a
Dánsko	Dánsko	k1gNnSc4	Dánsko
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jej	on	k3xPp3gMnSc4	on
doposud	doposud	k6eAd1	doposud
nevydalo	vydat	k5eNaPmAgNnS	vydat
k	k	k7c3	k
trestnímu	trestní	k2eAgNnSc3d1	trestní
stíhání	stíhání	k1gNnSc3	stíhání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
1.	[number]	k4	1.
června	červen	k1gInSc2	červen
2018	[number]	k4	2018
vyslovil	vyslovit	k5eAaPmAgInS	vyslovit
španělský	španělský	k2eAgInSc1d1	španělský
parlament	parlament	k1gInSc1	parlament
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
předsedovi	předseda	k1gMnSc3	předseda
vlády	vláda	k1gFnSc2	vláda
Marianu	Marian	k1gMnSc3	Marian
Rajoyovi	Rajoya	k1gMnSc3	Rajoya
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
jej	on	k3xPp3gMnSc4	on
svrhl	svrhnout	k5eAaPmAgMnS	svrhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Vnitrozemí	vnitrozemí	k1gNnSc4	vnitrozemí
Španělska	Španělsko	k1gNnSc2	Španělsko
dominují	dominovat	k5eAaImIp3nP	dominovat
náhorní	náhorní	k2eAgFnSc1d1	náhorní
plošina	plošina	k1gFnSc1	plošina
Meseta	Meseto	k1gNnSc2	Meseto
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Meseta	Meset	k2eAgFnSc1d1	Meseta
Central	Central	k1gFnSc1	Central
<g/>
)	)	kIx)	)
a	a	k8xC	a
pohoří	pohořet	k5eAaPmIp3nP	pohořet
Pyreneje	Pyreneje	k1gFnPc1	Pyreneje
<g/>
,	,	kIx,	,
Sierra	Sierra	k1gFnSc1	Sierra
Nevada	Nevada	k1gFnSc1	Nevada
a	a	k8xC	a
Kantaberské	Kantaberský	k2eAgNnSc1d1	Kantaberské
pohoří	pohoří	k1gNnSc1	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
pohoří	pohoří	k1gNnPc2	pohoří
vytékají	vytékat	k5eAaImIp3nP	vytékat
řeky	řeka	k1gFnPc1	řeka
Tajo	Tajo	k6eAd1	Tajo
<g/>
,	,	kIx,	,
Ebro	Ebro	k1gNnSc1	Ebro
<g/>
,	,	kIx,	,
Duero	Duero	k1gNnSc1	Duero
<g/>
,	,	kIx,	,
Guadiana	Guadiana	k1gFnSc1	Guadiana
a	a	k8xC	a
Guadalquivir	Guadalquivir	k1gInSc1	Guadalquivir
<g/>
.	.	kIx.	.
</s>
<s>
Údolní	údolní	k2eAgFnPc1d1	údolní
nivy	niva	k1gFnPc1	niva
jsou	být	k5eAaImIp3nP	být
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
největší	veliký	k2eAgMnPc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
leží	ležet	k5eAaImIp3nP	ležet
u	u	k7c2	u
Guadalquivir	Guadalquivira	k1gFnPc2	Guadalquivira
v	v	k7c6	v
Andalusii	Andalusie	k1gFnSc6	Andalusie
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
jsou	být	k5eAaImIp3nP	být
údolí	údolí	k1gNnPc4	údolí
řek	řeka	k1gFnPc2	řeka
Segura	Segura	k1gFnSc1	Segura
<g/>
,	,	kIx,	,
Júcar	Júcar	k1gInSc1	Júcar
a	a	k8xC	a
Turia	Turia	k1gFnSc1	Turia
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
východu	východ	k1gInSc2	východ
Španělsko	Španělsko	k1gNnSc1	Španělsko
omývá	omývat	k5eAaImIp3nS	omývat
Středozemní	středozemní	k2eAgNnSc4d1	středozemní
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
Baleárské	baleárský	k2eAgInPc1d1	baleárský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
;	;	kIx,	;
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
Biskajský	biskajský	k2eAgInSc4d1	biskajský
záliv	záliv	k1gInSc4	záliv
<g/>
,	,	kIx,	,
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
Kanárské	kanárský	k2eAgInPc1d1	kanárský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
je	být	k5eAaImIp3nS	být
Pico	Pico	k6eAd1	Pico
de	de	k?	de
Teide	Teid	k1gInSc5	Teid
(	(	kIx(	(
<g/>
3718	[number]	k4	3718
m	m	kA	m
<g/>
)	)	kIx)	)
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Tenerife	Tenerif	k1gInSc5	Tenerif
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
sopek	sopka	k1gFnPc2	sopka
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pevninském	pevninský	k2eAgNnSc6d1	pevninské
Španělsku	Španělsko	k1gNnSc6	Španělsko
náleží	náležet	k5eAaImIp3nS	náležet
prvenství	prvenství	k1gNnSc1	prvenství
hoře	hoře	k1gNnSc2	hoře
Mulhacén	Mulhacén	k1gInSc4	Mulhacén
(	(	kIx(	(
<g/>
3478	[number]	k4	3478
m	m	kA	m
<g/>
)	)	kIx)	)
u	u	k7c2	u
Granady	Granada	k1gFnSc2	Granada
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
Pico	Pico	k1gMnSc1	Pico
de	de	k?	de
Aneto	Aneta	k1gFnSc5	Aneta
(	(	kIx(	(
<g/>
3404	[number]	k4	3404
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vrcholek	vrcholek	k1gInSc1	vrcholek
Pyrenejí	Pyreneje	k1gFnPc2	Pyreneje
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
rozdělit	rozdělit	k5eAaPmF	rozdělit
podle	podle	k7c2	podle
podnebí	podnebí	k1gNnSc2	podnebí
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
:	:	kIx,	:
nejteplejší	teplý	k2eAgFnSc4d3	nejteplejší
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
země	zem	k1gFnPc1	zem
<g/>
;	;	kIx,	;
deštivá	deštivý	k2eAgNnPc1d1	deštivé
období	období	k1gNnPc1	období
jsou	být	k5eAaImIp3nP	být
jaro	jaro	k6eAd1	jaro
a	a	k8xC	a
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Klidná	klidný	k2eAgNnPc4d1	klidné
léta	léto	k1gNnPc4	léto
s	s	k7c7	s
příjemnými	příjemný	k2eAgFnPc7d1	příjemná
teplotami	teplota	k1gFnPc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Teplotní	teplotní	k2eAgInPc1d1	teplotní
rekordy	rekord	k1gInPc1	rekord
<g/>
:	:	kIx,	:
Murcia	Murcia	k1gFnSc1	Murcia
47.	[number]	k4	47.
<g/>
2	[number]	k4	2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Malaga	Malaga	k1gFnSc1	Malaga
44.	[number]	k4	44.
<g/>
2	[number]	k4	2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Valencia	Valencia	k1gFnSc1	Valencia
42.	[number]	k4	42.
<g/>
5	[number]	k4	5
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Alicante	Alicant	k1gMnSc5	Alicant
41.	[number]	k4	41.
<g/>
4	[number]	k4	4
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Palma	palma	k1gFnSc1	palma
de	de	k?	de
Mallorca	Mallorca	k1gFnSc1	Mallorca
40.	[number]	k4	40.
<g/>
6	[number]	k4	6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Barcelona	Barcelona	k1gFnSc1	Barcelona
39.	[number]	k4	39.
<g/>
8	[number]	k4	8
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejnižší	nízký	k2eAgFnPc4d3	nejnižší
teploty	teplota	k1gFnPc4	teplota
<g/>
:	:	kIx,	:
Gerona	Gero	k1gMnSc2	Gero
-13.0	-13.0	k4	-13.0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Barcelona	Barcelona	k1gFnSc1	Barcelona
-10.0	-10.0	k4	-10.0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Valencia	Valencia	k1gFnSc1	Valencia
-7.2	-7.2	k4	-7.2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Murcia	Murcia	k1gFnSc1	Murcia
-6.0	-6.0	k4	-6.0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Alicante	Alicant	k1gMnSc5	Alicant
-4.6	-4.6	k4	-4.6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Malaga	Malaga	k1gFnSc1	Malaga
-3.8	-3.8	k4	-3.8
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Vnitrozemí	vnitrozemí	k1gNnSc1	vnitrozemí
<g/>
:	:	kIx,	:
Velmi	velmi	k6eAd1	velmi
studené	studený	k2eAgFnSc2d1	studená
zimy	zima	k1gFnSc2	zima
(	(	kIx(	(
<g/>
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
často	často	k6eAd1	často
se	s	k7c7	s
sněhem	sníh	k1gInSc7	sníh
<g/>
,	,	kIx,	,
teploty	teplota	k1gFnPc1	teplota
až	až	k8xS	až
-	-	kIx~	-
<g/>
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
horká	horký	k2eAgNnPc1d1	horké
a	a	k8xC	a
suchá	suchý	k2eAgNnPc1d1	suché
léta	léto	k1gNnPc1	léto
(	(	kIx(	(
<g/>
teploty	teplota	k1gFnSc2	teplota
až	až	k9	až
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teplotní	teplotní	k2eAgInPc1d1	teplotní
rekordy	rekord	k1gInPc1	rekord
<g/>
:	:	kIx,	:
Sevilla	Sevilla	k1gFnSc1	Sevilla
47.	[number]	k4	47.
<g/>
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Cordoba	Cordoba	k1gFnSc1	Cordoba
46.	[number]	k4	46.
<g/>
6	[number]	k4	6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Badajoz	Badajoz	k1gInSc1	Badajoz
45.	[number]	k4	45.
<g/>
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Albacete	Albace	k1gNnSc2	Albace
a	a	k8xC	a
Zaragoza	Zaragoza	k1gFnSc1	Zaragoza
42.	[number]	k4	42.
<g/>
6	[number]	k4	6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
42.	[number]	k4	42.
<g/>
2	[number]	k4	2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Burgos	Burgos	k1gInSc1	Burgos
41.	[number]	k4	41.
<g/>
8	[number]	k4	8
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Valladolid	Valladolid	k1gInSc1	Valladolid
40.	[number]	k4	40.
<g/>
2	[number]	k4	2
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejnižší	nízký	k2eAgFnSc2d3	nejnižší
teploty	teplota	k1gFnSc2	teplota
<g/>
:	:	kIx,	:
Albacete	Albace	k1gNnSc2	Albace
-24.0	-24.0	k4	-24.0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Burgos	Burgos	k1gInSc1	Burgos
-22.0	-22.0	k4	-22.0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Salamanca	Salamanca	k1gFnSc1	Salamanca
-20.0	-20.0	k4	-20.0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Teruel	Teruel	k1gInSc1	Teruel
-19.0	-19.0	k4	-19.0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
-14.8	-14.8	k4	-14.8
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Sevilla	Sevilla	k1gFnSc1	Sevilla
-5.5	-5.5	k4	-5.5
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Severní	severní	k2eAgNnSc4d1	severní
atlantické	atlantický	k2eAgNnSc4d1	atlantické
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
:	:	kIx,	:
ostré	ostrý	k2eAgFnSc2d1	ostrá
zimy	zima	k1gFnSc2	zima
s	s	k7c7	s
klidnými	klidný	k2eAgNnPc7d1	klidné
léty	léto	k1gNnPc7	léto
(	(	kIx(	(
<g/>
trošku	trošku	k6eAd1	trošku
chladnějšími	chladný	k2eAgMnPc7d2	chladnější
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teplotní	teplotní	k2eAgInPc1d1	teplotní
rekordy	rekord	k1gInPc1	rekord
<g/>
:	:	kIx,	:
Bilbao	Bilbao	k6eAd1	Bilbao
42.	[number]	k4	42.
<g/>
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
La	la	k1gNnSc2	la
Coruñ	Coruñ	k1gFnSc2	Coruñ
37.	[number]	k4	37.
<g/>
6	[number]	k4	6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Gijón	Gijón	k1gInSc1	Gijón
36.	[number]	k4	36.
<g/>
4	[number]	k4	4
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejnižší	nízký	k2eAgFnPc4d3	nejnižší
teploty	teplota	k1gFnPc4	teplota
<g/>
:	:	kIx,	:
Bilbao	Bilbao	k6eAd1	Bilbao
-8.6	-8.6	k4	-8.6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Oviedo	Oviedo	k1gNnSc1	Oviedo
-6.0	-6.0	k4	-6.0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Gijon	Gijon	k1gNnSc1	Gijon
a	a	k8xC	a
La	la	k1gNnSc1	la
Coruñ	Coruñ	k1gFnSc2	Coruñ
-4.8	-4.8	k4	-4.8
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Kanárské	kanárský	k2eAgInPc1d1	kanárský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
:	:	kIx,	:
subtropické	subtropický	k2eAgNnSc1d1	subtropické
počasí	počasí	k1gNnSc1	počasí
<g/>
,	,	kIx,	,
s	s	k7c7	s
příjemnými	příjemný	k2eAgFnPc7d1	příjemná
teplotami	teplota	k1gFnPc7	teplota
(	(	kIx(	(
<g/>
18	[number]	k4	18
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
24	[number]	k4	24
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Teplotní	teplotní	k2eAgInPc1d1	teplotní
rekordy	rekord	k1gInPc1	rekord
<g/>
:	:	kIx,	:
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
de	de	k?	de
Tenerife	Tenerif	k1gInSc5	Tenerif
42.	[number]	k4	42.
<g/>
6	[number]	k4	6
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejnižší	nízký	k2eAgFnPc4d3	nejnižší
teploty	teplota	k1gFnPc4	teplota
<g/>
:	:	kIx,	:
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
de	de	k?	de
Tenerife	Tenerif	k1gInSc5	Tenerif
8.	[number]	k4	8.
<g/>
1	[number]	k4	1
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Nejvyššími	vysoký	k2eAgInPc7d3	Nejvyšší
státními	státní	k2eAgInPc7d1	státní
orgány	orgán	k1gInPc7	orgán
jsou	být	k5eAaImIp3nP	být
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
parlament	parlament	k1gInSc1	parlament
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Král	Král	k1gMnSc1	Král
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
stát	stát	k5eAaPmF	stát
navenek	navenek	k6eAd1	navenek
<g/>
,	,	kIx,	,
svolává	svolávat	k5eAaImIp3nS	svolávat
a	a	k8xC	a
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
pravomoc	pravomoc	k1gFnSc4	pravomoc
schvalovat	schvalovat	k5eAaImF	schvalovat
a	a	k8xC	a
vyhlašovat	vyhlašovat	k5eAaImF	vyhlašovat
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
na	na	k7c6	na
základě	základ	k1gInSc6	základ
systému	systém	k1gInSc2	systém
primogenitury	primogenitura	k1gFnSc2	primogenitura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Parlament	parlament	k1gInSc1	parlament
je	být	k5eAaImIp3nS	být
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
<g/>
,	,	kIx,	,
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
Kongresu	kongres	k1gInSc2	kongres
poslanců	poslanec	k1gMnPc2	poslanec
a	a	k8xC	a
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
poslanců	poslanec	k1gMnPc2	poslanec
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
na	na	k7c4	na
4	[number]	k4	4
roky	rok	k1gInPc4	rok
na	na	k7c6	na
základě	základ	k1gInSc6	základ
poměrného	poměrný	k2eAgNnSc2d1	poměrné
zastoupení	zastoupení	k1gNnSc2	zastoupení
<g/>
,	,	kIx,	,
volebním	volební	k2eAgInSc7d1	volební
obvodem	obvod	k1gInSc7	obvod
je	být	k5eAaImIp3nS	být
provincie	provincie	k1gFnSc1	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
na	na	k7c4	na
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
a	a	k8xC	a
nařizovací	nařizovací	k2eAgFnSc1d1	nařizovací
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
svěřena	svěřit	k5eAaPmNgFnS	svěřit
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
poradním	poradní	k2eAgInSc7d1	poradní
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
Státní	státní	k2eAgFnSc1d1	státní
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zastoupen	zastoupen	k2eAgMnSc1d1	zastoupen
králem	král	k1gMnSc7	král
nebo	nebo	k8xC	nebo
parlamentem	parlament	k1gInSc7	parlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Územní	územní	k2eAgInPc1d1	územní
nároky	nárok	k1gInPc1	nárok
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Území	území	k1gNnSc1	území
nárokovaná	nárokovaný	k2eAgFnSc1d1	nárokovaná
Španělskem	Španělsko	k1gNnSc7	Španělsko
====	====	k?	====
</s>
</p>
<p>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
žádá	žádat	k5eAaImIp3nS	žádat
o	o	k7c4	o
navrácení	navrácení	k1gNnSc4	navrácení
Gibraltaru	Gibraltar	k1gInSc2	Gibraltar
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnSc2d1	malá
britské	britský	k2eAgFnSc2d1	britská
državy	država	k1gFnSc2	država
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
obsazen	obsadit	k5eAaPmNgInS	obsadit
během	během	k7c2	během
Války	válka	k1gFnSc2	válka
o	o	k7c4	o
španělské	španělský	k2eAgNnSc4d1	španělské
dědictví	dědictví	k1gNnSc4	dědictví
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1704	[number]	k4	1704
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
přiznán	přiznán	k2eAgInSc4d1	přiznán
Británii	Británie	k1gFnSc4	Británie
roku	rok	k1gInSc2	rok
1713	[number]	k4	1713
Utrechtskou	Utrechtský	k2eAgFnSc7d1	Utrechtská
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Španělská	španělský	k2eAgNnPc1d1	španělské
území	území	k1gNnPc1	území
nárokovaná	nárokovaný	k2eAgNnPc1d1	nárokované
jinými	jiný	k2eAgInPc7d1	jiný
státy	stát	k1gInPc7	stát
====	====	k?	====
</s>
</p>
<p>
<s>
Maroko	Maroko	k1gNnSc1	Maroko
nárokuje	nárokovat	k5eAaImIp3nS	nárokovat
španělská	španělský	k2eAgNnPc4d1	španělské
města	město	k1gNnPc4	město
Ceuta	Ceuto	k1gNnSc2	Ceuto
a	a	k8xC	a
Melilla	Melillo	k1gNnSc2	Melillo
a	a	k8xC	a
neobydlené	obydlený	k2eNgFnSc2d1	neobydlená
Španělské	španělský	k2eAgFnSc2d1	španělská
severoafrické	severoafrický	k2eAgFnSc2d1	severoafrická
državy	država	k1gFnSc2	država
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Maroko	Maroko	k1gNnSc1	Maroko
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
území	území	k1gNnSc3	území
byla	být	k5eAaImAgFnS	být
získána	získat	k5eAaPmNgFnS	získat
neoprávněně	oprávněně	k6eNd1	oprávněně
a	a	k8xC	a
Maroko	Maroko	k1gNnSc1	Maroko
nemohlo	moct	k5eNaImAgNnS	moct
nijak	nijak	k6eAd1	nijak
zabránit	zabránit	k5eAaPmF	zabránit
obsazení	obsazení	k1gNnSc2	obsazení
těchto	tento	k3xDgNnPc2	tento
území	území	k1gNnPc2	území
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
nebyly	být	k5eNaImAgFnP	být
podepsány	podepsán	k2eAgFnPc1d1	podepsána
smlouvy	smlouva	k1gFnPc1	smlouva
o	o	k7c4	o
předání	předání	k1gNnSc4	předání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
neuznává	uznávat	k5eNaImIp3nS	uznávat
španělskou	španělský	k2eAgFnSc4d1	španělská
suverenitu	suverenita	k1gFnSc4	suverenita
nad	nad	k7c7	nad
územím	území	k1gNnSc7	území
Olivenza	Olivenza	k1gFnSc1	Olivenza
<g/>
.	.	kIx.	.
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
si	se	k3xPyFc3	se
podle	podle	k7c2	podle
Vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
smlouvy	smlouva	k1gFnSc2	smlouva
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Španělsko	Španělsko	k1gNnSc1	Španělsko
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
<g/>
,	,	kIx,	,
vyhrazuje	vyhrazovat	k5eAaImIp3nS	vyhrazovat
navrácení	navrácení	k1gNnSc1	navrácení
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
smlouva	smlouva	k1gFnSc1	smlouva
je	být	k5eAaImIp3nS	být
neplatná	platný	k2eNgFnSc1d1	neplatná
a	a	k8xC	a
podle	podle	k7c2	podle
Badajozské	Badajozský	k2eAgFnSc2d1	Badajozský
smlouvy	smlouva	k1gFnSc2	smlouva
patří	patřit	k5eAaImIp3nS	patřit
území	území	k1gNnSc4	území
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
povzbuzuje	povzbuzovat	k5eAaImIp3nS	povzbuzovat
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
k	k	k7c3	k
diplomatickému	diplomatický	k2eAgNnSc3d1	diplomatické
řešení	řešení	k1gNnSc3	řešení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Správní	správní	k2eAgNnSc1d1	správní
rozdělení	rozdělení	k1gNnSc1	rozdělení
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Španělské	španělský	k2eAgNnSc1d1	španělské
království	království	k1gNnSc1	království
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
17	[number]	k4	17
autonomních	autonomní	k2eAgNnPc2d1	autonomní
společenství	společenství	k1gNnPc2	společenství
(	(	kIx(	(
<g/>
comunidades	comunidades	k1gMnSc1	comunidades
autónomas	autónomas	k1gMnSc1	autónomas
<g/>
)	)	kIx)	)
a	a	k8xC	a
2	[number]	k4	2
autonomní	autonomní	k2eAgFnSc4d1	autonomní
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
ciudades	ciudades	k1gMnSc1	ciudades
autónomas	autónomas	k1gMnSc1	autónomas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
dál	daleko	k6eAd2	daleko
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
50	[number]	k4	50
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Středopravicová	středopravicový	k2eAgFnSc1d1	středopravicová
vláda	vláda	k1gFnSc1	vláda
bývalého	bývalý	k2eAgMnSc2d1	bývalý
premiéra	premiér	k1gMnSc2	premiér
Aznara	Aznar	k1gMnSc2	Aznar
úspěšně	úspěšně	k6eAd1	úspěšně
pracovala	pracovat	k5eAaImAgFnS	pracovat
na	na	k7c4	na
přijetí	přijetí	k1gNnSc4	přijetí
jednotné	jednotný	k2eAgFnSc2d1	jednotná
evropské	evropský	k2eAgFnSc2d1	Evropská
měny	měna	k1gFnSc2	měna
Euro	euro	k1gNnSc1	euro
v	v	k7c6	v
první	první	k4xOgFnSc6	první
skupině	skupina	k1gFnSc6	skupina
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
1.	[number]	k4	1.
lednu	leden	k1gInSc3	leden
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Aznarova	Aznarův	k2eAgFnSc1d1	Aznarova
vláda	vláda	k1gFnSc1	vláda
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
prosazování	prosazování	k1gNnSc6	prosazování
liberalizace	liberalizace	k1gFnSc2	liberalizace
<g/>
,	,	kIx,	,
privatizace	privatizace	k1gFnSc2	privatizace
a	a	k8xC	a
deregulace	deregulace	k1gFnSc2	deregulace
ekonomiky	ekonomika	k1gFnSc2	ekonomika
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
představila	představit	k5eAaPmAgFnS	představit
i	i	k9	i
daňové	daňový	k2eAgFnPc4d1	daňová
reformy	reforma	k1gFnPc4	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
za	za	k7c2	za
doby	doba	k1gFnSc2	doba
Aznarovy	Aznarův	k2eAgFnSc2d1	Aznarova
vlády	vláda	k1gFnSc2	vláda
stabilně	stabilně	k6eAd1	stabilně
klesala	klesat	k5eAaImAgFnS	klesat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
na	na	k7c4	na
9,8	[number]	k4	9,8
%	%	kIx~	%
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
lepší	dobrý	k2eAgMnSc1d2	lepší
než	než	k8xS	než
na	na	k7c6	na
začátku	začátek	k1gInSc2	začátek
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
nezaměstnaných	zaměstnaný	k2eNgNnPc2d1	nezaměstnané
20	[number]	k4	20
%	%	kIx~	%
Španělů	Španěl	k1gMnPc2	Španěl
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
o	o	k7c4	o
2,4	[number]	k4	2,4
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
zapříčiněn	zapříčinit	k5eAaPmNgInS	zapříčinit
kolísáním	kolísání	k1gNnSc7	kolísání
evropské	evropský	k2eAgFnSc2d1	Evropská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
ustálila	ustálit	k5eAaPmAgFnS	ustálit
na	na	k7c4	na
asi	asi	k9	asi
3,3	[number]	k4	3,3
%	%	kIx~	%
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
José	Josá	k1gFnSc2	Josá
Luis	Luisa	k1gFnPc2	Luisa
Rodríguez	Rodríguez	k1gMnSc1	Rodríguez
Zapatero	Zapatero	k1gNnSc1	Zapatero
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
strana	strana	k1gFnSc1	strana
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
volby	volba	k1gFnSc2	volba
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
po	po	k7c6	po
výbuších	výbuch	k1gInPc6	výbuch
ve	v	k7c6	v
vlacích	vlak	k1gInPc6	vlak
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
plánoval	plánovat	k5eAaImAgMnS	plánovat
redukovat	redukovat	k5eAaBmF	redukovat
vládní	vládní	k2eAgNnSc4d1	vládní
vměšování	vměšování	k1gNnSc4	vměšování
do	do	k7c2	do
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
,	,	kIx,	,
daňové	daňový	k2eAgInPc4d1	daňový
úniky	únik	k1gInPc4	únik
a	a	k8xC	a
podporovat	podporovat	k5eAaImF	podporovat
inovace	inovace	k1gFnPc4	inovace
<g/>
,	,	kIx,	,
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
intenzivně	intenzivně	k6eAd1	intenzivně
zavádět	zavádět	k5eAaImF	zavádět
regulace	regulace	k1gFnSc1	regulace
trhu	trh	k1gInSc2	trh
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
údajů	údaj	k1gInPc2	údaj
žebříčků	žebříček	k1gInPc2	žebříček
HDP	HDP	kA	HDP
Světové	světový	k2eAgFnSc2d1	světová
Banky	banka	k1gFnSc2	banka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Španělsko	Španělsko	k1gNnSc1	Španělsko
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
osmou	osma	k1gFnSc7	osma
největší	veliký	k2eAgFnSc7d3	veliký
ekonomikou	ekonomika	k1gFnSc7	ekonomika
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
model	model	k1gInSc1	model
španělského	španělský	k2eAgInSc2d1	španělský
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
růstu	růst	k1gInSc2	růst
(	(	kIx(	(
<g/>
založený	založený	k2eAgMnSc1d1	založený
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
masové	masový	k2eAgFnSc6d1	masová
turistice	turistika	k1gFnSc6	turistika
<g/>
,	,	kIx,	,
neustálé	neustálý	k2eAgFnSc6d1	neustálá
výstavbě	výstavba	k1gFnSc6	výstavba
a	a	k8xC	a
dělnických	dělnický	k2eAgFnPc6d1	Dělnická
profesích	profes	k1gFnPc6	profes
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kolísavý	kolísavý	k2eAgMnSc1d1	kolísavý
a	a	k8xC	a
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
udržitelný	udržitelný	k2eAgInSc1d1	udržitelný
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zpráva	zpráva	k1gFnSc1	zpráva
Observatoře	observatoř	k1gFnSc2	observatoř
Udržitelnosti	udržitelnost	k1gFnSc2	udržitelnost
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Observatorio	Observatorio	k6eAd1	Observatorio
de	de	k?	de
Sostenibilidad	Sostenibilidad	k1gInSc1	Sostenibilidad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
a	a	k8xC	a
financována	financovat	k5eAaBmNgFnS	financovat
španělským	španělský	k2eAgNnSc7d1	španělské
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
univerzitou	univerzita	k1gFnSc7	univerzita
v	v	k7c4	v
Alcalá	Alcalý	k2eAgNnPc4d1	Alcalý
<g/>
,	,	kIx,	,
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
růst	růst	k1gInSc1	růst
HDP	HDP	kA	HDP
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
25	[number]	k4	25
%	%	kIx~	%
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
skleníkové	skleníkový	k2eAgInPc1d1	skleníkový
plyny	plyn	k1gInPc1	plyn
rostou	růst	k5eAaImIp3nP	růst
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
o	o	k7c4	o
45	[number]	k4	45
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
populace	populace	k1gFnPc1	populace
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
roste	růst	k5eAaImIp3nS	růst
jen	jen	k9	jen
o	o	k7c4	o
něco	něco	k3yInSc4	něco
málo	málo	k1gNnSc4	málo
přes	přes	k7c4	přes
5	[number]	k4	5
%	%	kIx~	%
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1990	[number]	k4	1990
a	a	k8xC	a
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
plocha	plocha	k1gFnSc1	plocha
měst	město	k1gNnPc2	město
se	se	k3xPyFc4	se
zvětšila	zvětšit	k5eAaPmAgFnS	zvětšit
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
25	[number]	k4	25
%	%	kIx~	%
za	za	k7c4	za
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
spotřeba	spotřeba	k1gFnSc1	spotřeba
energie	energie	k1gFnSc2	energie
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
20	[number]	k4	20
let	léto	k1gNnPc2	léto
více	hodně	k6eAd2	hodně
než	než	k8xS	než
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
roste	růst	k5eAaImIp3nS	růst
o	o	k7c4	o
6	[number]	k4	6
%	%	kIx~	%
ročně	ročně	k6eAd1	ročně
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
importu	import	k1gInSc6	import
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
znepokojující	znepokojující	k2eAgNnPc1d1	znepokojující
(	(	kIx(	(
<g/>
asi	asi	k9	asi
80	[number]	k4	80
%	%	kIx~	%
španělské	španělský	k2eAgFnSc2d1	španělská
energie	energie	k1gFnSc2	energie
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
ropu	ropa	k1gFnSc4	ropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
neudržitelný	udržitelný	k2eNgInSc1d1	neudržitelný
vývoj	vývoj	k1gInSc1	vývoj
je	být	k5eAaImIp3nS	být
viditelný	viditelný	k2eAgInSc1d1	viditelný
podél	podél	k7c2	podél
španělského	španělský	k2eAgNnSc2d1	španělské
pobřeží	pobřeží	k1gNnSc2	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
staví	stavit	k5eAaBmIp3nP	stavit
domy	dům	k1gInPc1	dům
a	a	k8xC	a
turistické	turistický	k2eAgInPc1d1	turistický
komplexy	komplex	k1gInPc1	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
zabírají	zabírat	k5eAaImIp3nP	zabírat
obrovské	obrovský	k2eAgFnPc4d1	obrovská
plochy	plocha	k1gFnPc4	plocha
země	zem	k1gFnSc2	zem
a	a	k8xC	a
vodní	vodní	k2eAgInPc1d1	vodní
zdroje	zdroj	k1gInPc1	zdroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
krize	krize	k1gFnSc2	krize
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
na	na	k7c4	na
27,2	[number]	k4	27,2
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
se	se	k3xPyFc4	se
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
zadlužením	zadlužení	k1gNnSc7	zadlužení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
činilo	činit	k5eAaImAgNnS	činit
98,3	[number]	k4	98,3
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
má	mít	k5eAaImIp3nS	mít
hustou	hustý	k2eAgFnSc4d1	hustá
dálniční	dálniční	k2eAgFnSc4d1	dálniční
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejrozsáhlejším	rozsáhlý	k2eAgFnPc3d3	nejrozsáhlejší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k8xC	i
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
dálnic	dálnice	k1gFnPc2	dálnice
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
státní	státní	k2eAgFnPc1d1	státní
<g/>
,	,	kIx,	,
autonomní	autonomní	k2eAgFnSc1d1	autonomní
a	a	k8xC	a
regionální	regionální	k2eAgFnSc1d1	regionální
silnice	silnice	k1gFnSc1	silnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
délka	délka	k1gFnSc1	délka
silniční	silniční	k2eAgFnSc2d1	silniční
sítě	síť	k1gFnSc2	síť
<g/>
:	:	kIx,	:
664 610	[number]	k4	664 610
km	km	kA	km
(	(	kIx(	(
<g/>
údaj	údaj	k1gInSc1	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
povolená	povolený	k2eAgFnSc1d1	povolená
rychlost	rychlost	k1gFnSc1	rychlost
na	na	k7c6	na
dálnicích	dálnice	k1gFnPc6	dálnice
120	[number]	k4	120
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
povolená	povolený	k2eAgFnSc1d1	povolená
rychlost	rychlost	k1gFnSc1	rychlost
na	na	k7c6	na
hlavních	hlavní	k2eAgFnPc6d1	hlavní
silnicích	silnice	k1gFnPc6	silnice
(	(	kIx(	(
<g/>
značené	značený	k2eAgNnSc1d1	značené
N	N	kA	N
<g/>
)	)	kIx)	)
100	[number]	k4	100
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
povolená	povolený	k2eAgFnSc1d1	povolená
rychlost	rychlost	k1gFnSc1	rychlost
na	na	k7c6	na
ostatních	ostatní	k2eAgFnPc6d1	ostatní
silnicích	silnice	k1gFnPc6	silnice
90	[number]	k4	90
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
</s>
</p>
<p>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
povolená	povolený	k2eAgFnSc1d1	povolená
rychlost	rychlost	k1gFnSc1	rychlost
v	v	k7c6	v
obcích	obec	k1gFnPc6	obec
50	[number]	k4	50
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
</s>
</p>
<p>
<s>
maximální	maximální	k2eAgNnSc4d1	maximální
povolené	povolený	k2eAgNnSc4d1	povolené
množství	množství	k1gNnSc4	množství
alkoholu	alkohol	k1gInSc2	alkohol
v	v	k7c6	v
dechu	dech	k1gInSc6	dech
0,5	[number]	k4	0,5
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
</s>
</p>
<p>
<s>
další	další	k2eAgFnPc1d1	další
informace	informace	k1gFnPc1	informace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
řidič	řidič	k1gMnSc1	řidič
nosící	nosící	k2eAgFnPc4d1	nosící
brýle	brýle	k1gFnPc4	brýle
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
u	u	k7c2	u
sebe	se	k3xPyFc2	se
brýle	brýle	k1gFnPc4	brýle
náhradní	náhradní	k2eAgFnSc2d1	náhradní
</s>
</p>
<p>
<s>
není	být	k5eNaImIp3nS	být
povoleno	povolit	k5eAaPmNgNnS	povolit
za	za	k7c2	za
jízdy	jízda	k1gFnSc2	jízda
telefonovat	telefonovat	k5eAaImF	telefonovat
(	(	kIx(	(
<g/>
ani	ani	k8xC	ani
s	s	k7c7	s
hands-free	handsree	k1gFnSc7	hands-free
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
telefonní	telefonní	k2eAgNnSc1d1	telefonní
číslo	číslo	k1gNnSc1	číslo
na	na	k7c4	na
dopravní	dopravní	k2eAgFnSc4d1	dopravní
policii	policie	k1gFnSc4	policie
<g/>
:	:	kIx,	:
062	[number]	k4	062
</s>
</p>
<p>
<s>
Označení	označení	k1gNnSc1	označení
silnic	silnice	k1gFnPc2	silnice
</s>
</p>
<p>
<s>
dálnice	dálnice	k1gFnSc1	dálnice
–	–	k?	–
autopista	autopist	k1gMnSc2	autopist
(	(	kIx(	(
<g/>
s	s	k7c7	s
poplatkem	poplatek	k1gInSc7	poplatek
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
AP	ap	kA	ap
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
dálnice	dálnice	k1gFnSc1	dálnice
–	–	k?	–
autovía	autovía	k1gFnSc1	autovía
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
poplatku	poplatek	k1gInSc2	poplatek
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
státní	státní	k2eAgFnPc1d1	státní
silnice	silnice	k1gFnPc1	silnice
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ostatní	ostatní	k2eAgFnPc1d1	ostatní
silnice	silnice	k1gFnPc1	silnice
<g/>
:	:	kIx,	:
různé	různý	k2eAgInPc1d1	různý
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
zkratek	zkratka	k1gFnPc2	zkratka
autonomních	autonomní	k2eAgNnPc2d1	autonomní
společenství	společenství	k1gNnPc2	společenství
<g/>
,	,	kIx,	,
regionů	region	k1gInPc2	region
<g/>
...	...	k?	...
<g/>
Ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
poměrně	poměrně	k6eAd1	poměrně
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
síť	síť	k1gFnSc1	síť
vysokorychlostních	vysokorychlostní	k2eAgFnPc2d1	vysokorychlostní
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
systému	systém	k1gInSc2	systém
AVE	ave	k1gNnSc2	ave
a	a	k8xC	a
Alvia	Alvium	k1gNnSc2	Alvium
<g/>
.	.	kIx.	.
</s>
<s>
Doplněna	doplněn	k2eAgFnSc1d1	doplněna
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
některé	některý	k3yIgFnPc4	některý
další	další	k2eAgFnPc4d1	další
regionální	regionální	k2eAgFnPc4d1	regionální
tratě	trať	k1gFnPc4	trať
<g/>
.	.	kIx.	.
</s>
<s>
Správcem	správce	k1gMnSc7	správce
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
ADIF	ADIF	kA	ADIF
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgMnSc7d1	hlavní
dopravcem	dopravce	k1gMnSc7	dopravce
je	být	k5eAaImIp3nS	být
Renfe	Renf	k1gInSc5	Renf
Operadora	Operador	k1gMnSc2	Operador
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Renfe	Renf	k1gInSc5	Renf
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aglomeracích	aglomerace	k1gFnPc6	aglomerace
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
provozovány	provozovat	k5eAaImNgInP	provozovat
vlaky	vlak	k1gInPc1	vlak
příměstské	příměstský	k2eAgFnSc2d1	příměstská
železnice	železnice	k1gFnSc2	železnice
Cercanías	Cercaníasa	k1gFnPc2	Cercaníasa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
větších	veliký	k2eAgNnPc6d2	veliký
městech	město	k1gNnPc6	město
je	být	k5eAaImIp3nS	být
provozována	provozován	k2eAgFnSc1d1	provozována
městská	městský	k2eAgFnSc1d1	městská
hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
metro	metro	k1gNnSc1	metro
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
městech	město	k1gNnPc6	město
–	–	k?	–
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
,	,	kIx,	,
Bilbao	Bilbao	k1gNnSc1	Bilbao
<g/>
,	,	kIx,	,
Sevilla	Sevilla	k1gFnSc1	Sevilla
<g/>
,	,	kIx,	,
Valencie	Valencie	k1gFnSc1	Valencie
a	a	k8xC	a
Palma	palma	k1gFnSc1	palma
de	de	k?	de
Mallorca	Mallorca	k1gFnSc1	Mallorca
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tři	tři	k4xCgNnPc1	tři
největší	veliký	k2eAgNnPc1d3	veliký
letiště	letiště	k1gNnPc1	letiště
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
jsou	být	k5eAaImIp3nP	být
Adolfa	Adolf	k1gMnSc2	Adolf
Suaréze	Suaréza	k1gFnSc6	Suaréza
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
Prat	Prata	k1gFnPc2	Prata
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
a	a	k8xC	a
v	v	k7c6	v
Palmě	palma	k1gFnSc6	palma
de	de	k?	de
Mallorce	Mallorka	k1gFnSc6	Mallorka
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlajkovou	vlajkový	k2eAgFnSc7d1	vlajková
leteckou	letecký	k2eAgFnSc7d1	letecká
společností	společnost	k1gFnSc7	společnost
je	být	k5eAaImIp3nS	být
Iberia	Iberium	k1gNnSc2	Iberium
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
velké	velký	k2eAgFnPc4d1	velká
společnosti	společnost	k1gFnPc4	společnost
patří	patřit	k5eAaImIp3nS	patřit
Air	Air	k1gFnSc1	Air
Europa	Europa	k1gFnSc1	Europa
<g/>
,	,	kIx,	,
ASL	ASL	kA	ASL
Airlines	Airlines	k1gMnSc1	Airlines
Spain	Spain	k1gMnSc1	Spain
<g/>
,	,	kIx,	,
Swiftair	Swiftair	k1gMnSc1	Swiftair
<g/>
,	,	kIx,	,
Volotea	Volotea	k1gMnSc1	Volotea
<g/>
,	,	kIx,	,
Vueling	Vueling	k1gInSc1	Vueling
či	či	k8xC	či
Wamos	Wamos	k1gMnSc1	Wamos
Air	Air	k1gMnSc1	Air
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
1.	[number]	k4	1.
lednu	leden	k1gInSc3	leden
2014	[number]	k4	2014
obývalo	obývat	k5eAaImAgNnS	obývat
Španělsko	Španělsko	k1gNnSc1	Španělsko
46,7	[number]	k4	46,7
miliónu	milión	k4xCgInSc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
činila	činit	k5eAaImAgFnS	činit
91,2	[number]	k4	91,2
obyvatele	obyvatel	k1gMnSc2	obyvatel
na	na	k7c4	na
km2	km2	k4	km2
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
pod	pod	k7c7	pod
průměrem	průměr	k1gInSc7	průměr
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Rozložení	rozložení	k1gNnSc1	rozložení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
nerovnoměrné	rovnoměrný	k2eNgNnSc1d1	nerovnoměrné
<g/>
:	:	kIx,	:
zatímco	zatímco	k8xS	zatímco
pobřežní	pobřežní	k2eAgFnPc1d1	pobřežní
oblasti	oblast	k1gFnPc1	oblast
a	a	k8xC	a
ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
hustě	hustě	k6eAd1	hustě
zalidněné	zalidněný	k2eAgNnSc1d1	zalidněné
<g/>
,	,	kIx,	,
centrální	centrální	k2eAgNnSc1d1	centrální
Španělsko	Španělsko	k1gNnSc1	Španělsko
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
pusté	pustý	k2eAgNnSc1d1	pusté
(	(	kIx(	(
<g/>
kastilská	kastilský	k2eAgFnSc1d1	Kastilská
provincie	provincie	k1gFnSc1	provincie
Soria	Sorium	k1gNnSc2	Sorium
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
9	[number]	k4	9
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trpící	trpící	k2eAgInSc1d1	trpící
dlouhodobým	dlouhodobý	k2eAgNnSc7d1	dlouhodobé
stárnutím	stárnutí	k1gNnSc7	stárnutí
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
odchodem	odchod	k1gInSc7	odchod
zejména	zejména	k9	zejména
do	do	k7c2	do
Madridu	Madrid	k1gInSc2	Madrid
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
aglomeraci	aglomerace	k1gFnSc6	aglomerace
žije	žít	k5eAaImIp3nS	žít
14	[number]	k4	14
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hodně	hodně	k6eAd1	hodně
Španělů	Španěl	k1gMnPc2	Španěl
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
SRN	srna	k1gFnPc2	srna
a	a	k8xC	a
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
latinskoamerických	latinskoamerický	k2eAgFnPc6d1	latinskoamerická
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
<g/>
.	.	kIx.	.
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
kolonie	kolonie	k1gFnSc1	kolonie
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
;	;	kIx,	;
vznikala	vznikat	k5eAaImAgFnS	vznikat
v	v	k7c6	v
období	období	k1gNnSc6	období
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
–	–	k?	–
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
žije	žít	k5eAaImIp3nS	žít
početná	početný	k2eAgFnSc1d1	početná
kolonie	kolonie	k1gFnSc1	kolonie
Maročanů	Maročan	k1gMnPc2	Maročan
<g/>
,	,	kIx,	,
Latinoameričanů	Latinoameričan	k1gMnPc2	Latinoameričan
<g/>
,	,	kIx,	,
Afričanů	Afričan	k1gMnPc2	Afričan
černé	černý	k2eAgFnSc2d1	černá
pleti	pleť	k1gFnSc2	pleť
<g/>
,	,	kIx,	,
Číňanů	Číňan	k1gMnPc2	Číňan
<g/>
,	,	kIx,	,
Rumunů	Rumun	k1gMnPc2	Rumun
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
několika	několik	k4yIc2	několik
stovek	stovka	k1gFnPc2	stovka
Čechů	Čech	k1gMnPc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Přirozený	přirozený	k2eAgInSc1d1	přirozený
demografický	demografický	k2eAgInSc1d1	demografický
růst	růst	k1gInSc1	růst
je	být	k5eAaImIp3nS	být
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
,	,	kIx,	,
přírůstky	přírůstek	k1gInPc1	přírůstek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
tvoří	tvořit	k5eAaImIp3nP	tvořit
imigranti	imigrant	k1gMnPc1	imigrant
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
rozlohu	rozloha	k1gFnSc4	rozloha
země	zem	k1gFnSc2	zem
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
vlivy	vliv	k1gInPc4	vliv
u	u	k7c2	u
obyvatel	obyvatel	k1gMnPc2	obyvatel
existují	existovat	k5eAaImIp3nP	existovat
určité	určitý	k2eAgInPc1d1	určitý
rozdíly	rozdíl	k1gInPc1	rozdíl
fyziologické	fyziologický	k2eAgInPc1d1	fyziologický
<g/>
,	,	kIx,	,
charakterové	charakterový	k2eAgInPc1d1	charakterový
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgInPc1d1	náboženský
<g/>
,	,	kIx,	,
jazykové	jazykový	k2eAgInPc1d1	jazykový
i	i	k9	i
ve	v	k7c6	v
všeobecném	všeobecný	k2eAgNnSc6d1	všeobecné
vzdělání	vzdělání	k1gNnSc6	vzdělání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
;	;	kIx,	;
část	část	k1gFnSc1	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
hovořících	hovořící	k2eAgFnPc2d1	hovořící
jinými	jiný	k2eAgInPc7d1	jiný
jazyky	jazyk	k1gInPc7	jazyk
než	než	k8xS	než
kastilštinou	kastilština	k1gFnSc7	kastilština
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
Španěly	Španěl	k1gMnPc4	Španěl
pouze	pouze	k6eAd1	pouze
Kastilce	Kastilec	k1gMnPc4	Kastilec
a	a	k8xC	a
samy	sám	k3xTgInPc1	sám
se	se	k3xPyFc4	se
často	často	k6eAd1	často
prohlašují	prohlašovat	k5eAaImIp3nP	prohlašovat
za	za	k7c4	za
Galicijce	Galicijce	k1gMnPc4	Galicijce
<g/>
,	,	kIx,	,
Basky	Bask	k1gMnPc4	Bask
a	a	k8xC	a
Katalánce	Katalánec	k1gMnPc4	Katalánec
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tři	tři	k4xCgFnPc4	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
podíl	podíl	k1gInSc4	podíl
ostatních	ostatní	k2eAgNnPc2d1	ostatní
náboženství	náboženství	k1gNnPc2	náboženství
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Jazyky	jazyk	k1gInPc1	jazyk
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
Španělska	Španělsko	k1gNnSc2	Španělsko
je	být	k5eAaImIp3nS	být
španělština	španělština	k1gFnSc1	španělština
<g/>
,	,	kIx,	,
označovaná	označovaný	k2eAgFnSc1d1	označovaná
také	také	k9	také
jako	jako	k8xC	jako
kastilština	kastilština	k1gFnSc1	kastilština
(	(	kIx(	(
<g/>
castellano	castellana	k1gFnSc5	castellana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
úředním	úřední	k2eAgMnPc3d1	úřední
jazykům	jazyk	k1gMnPc3	jazyk
náleží	náležet	k5eAaImIp3nP	náležet
regionální	regionální	k2eAgInPc1d1	regionální
jazyky	jazyk	k1gInPc1	jazyk
katalánština	katalánština	k1gFnSc1	katalánština
(	(	kIx(	(
<g/>
catalá	catalý	k2eAgFnSc1d1	catalý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
galicijština	galicijština	k1gFnSc1	galicijština
(	(	kIx(	(
<g/>
galego	galego	k1gNnSc1	galego
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
baskičtina	baskičtina	k1gFnSc1	baskičtina
(	(	kIx(	(
<g/>
euskara	euskara	k1gFnSc1	euskara
<g/>
)	)	kIx)	)
a	a	k8xC	a
okrajově	okrajově	k6eAd1	okrajově
aranéština	aranéština	k1gFnSc1	aranéština
<g/>
.	.	kIx.	.
</s>
<s>
Katalánština	katalánština	k1gFnSc1	katalánština
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgInSc7	druhý
nejpoužívanějším	používaný	k2eAgInSc7d3	nejpoužívanější
jazykem	jazyk	k1gInSc7	jazyk
<g/>
;	;	kIx,	;
mluví	mluvit	k5eAaImIp3nS	mluvit
jí	on	k3xPp3gFnSc3	on
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Katalánska	Katalánsko	k1gNnSc2	Katalánsko
<g/>
,	,	kIx,	,
Valencie	Valencie	k1gFnSc2	Valencie
a	a	k8xC	a
Baleár	Baleáry	k1gFnPc2	Baleáry
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
Andorry	Andorra	k1gFnSc2	Andorra
<g/>
.	.	kIx.	.
</s>
<s>
Galicijštinu	Galicijština	k1gFnSc4	Galicijština
aktivně	aktivně	k6eAd1	aktivně
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
venkovské	venkovský	k2eAgNnSc4d1	venkovské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Galicie	Galicie	k1gFnSc2	Galicie
<g/>
,	,	kIx,	,
baskičtinu	baskičtina	k1gFnSc4	baskičtina
většina	většina	k1gFnSc1	většina
Baskicka	Baskicko	k1gNnSc2	Baskicko
a	a	k8xC	a
Navarry	Navarra	k1gFnSc2	Navarra
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
těchto	tento	k3xDgInPc2	tento
jazyků	jazyk	k1gInPc2	jazyk
se	se	k3xPyFc4	se
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
také	také	k9	také
asturština	asturština	k1gFnSc1	asturština
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
sice	sice	k8xC	sice
regionální	regionální	k2eAgInSc4d1	regionální
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c4	v
Asturii	Asturie	k1gFnSc4	Asturie
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
místními	místní	k2eAgInPc7d1	místní
zákony	zákon	k1gInPc7	zákon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aragonština	aragonština	k1gFnSc1	aragonština
<g/>
,	,	kIx,	,
extremadurština	extremadurština	k1gFnSc1	extremadurština
<g/>
,	,	kIx,	,
leónština	leónština	k1gFnSc1	leónština
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
přechodné	přechodný	k2eAgInPc1d1	přechodný
dialekty	dialekt	k1gInPc1	dialekt
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
však	však	k9	však
úředními	úřední	k2eAgInPc7d1	úřední
jazyky	jazyk	k1gInPc7	jazyk
nejsou	být	k5eNaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Přistěhovalectví	přistěhovalectví	k1gNnSc2	přistěhovalectví
===	===	k?	===
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
20.	[number]	k4	20.
století	století	k1gNnSc6	století
bylo	být	k5eAaImAgNnS	být
Španělsko	Španělsko	k1gNnSc1	Španělsko
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
emigranti	emigrant	k1gMnPc1	emigrant
odcházeli	odcházet	k5eAaImAgMnP	odcházet
(	(	kIx(	(
<g/>
před	před	k7c7	před
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
frankismem	frankismus	k1gInSc7	frankismus
či	či	k8xC	či
ekonomickými	ekonomický	k2eAgFnPc7d1	ekonomická
krizemi	krize	k1gFnPc7	krize
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
20.	[number]	k4	20.
a	a	k8xC	a
21.	[number]	k4	21.
století	století	k1gNnPc2	století
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
silnou	silný	k2eAgFnSc4d1	silná
vlnu	vlna	k1gFnSc4	vlna
migrace	migrace	k1gFnSc2	migrace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
již	již	k9	již
o	o	k7c4	o
něco	něco	k3yInSc4	něco
slabší	slabý	k2eAgMnPc1d2	slabší
<g/>
,	,	kIx,	,
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
čistou	čistý	k2eAgFnSc7d1	čistá
mírou	míra	k1gFnSc7	míra
imigrace	imigrace	k1gFnSc2	imigrace
1,5	[number]	k4	1,5
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
Kypru	Kypr	k1gInSc6	Kypr
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
0,99	[number]	k4	0,99
%	%	kIx~	%
15.	[number]	k4	15.
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
tvořilo	tvořit	k5eAaImAgNnS	tvořit
5 220 000	[number]	k4	5 220 000
cizinců	cizinec	k1gMnPc2	cizinec
11,8	[number]	k4	11,8
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
<g/>
Počátkem	počátkem	k7c2	počátkem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Španělsko	Španělsko	k1gNnSc1	Španělsko
změnilo	změnit	k5eAaPmAgNnS	změnit
na	na	k7c6	na
imigrační	imigrační	k2eAgFnSc6d1	imigrační
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
tomu	ten	k3xDgNnSc3	ten
přizpůsobilo	přizpůsobit	k5eAaPmAgNnS	přizpůsobit
i	i	k9	i
své	svůj	k3xOyFgInPc4	svůj
zákony	zákon	k1gInPc4	zákon
a	a	k8xC	a
instituce	instituce	k1gFnPc4	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
Španělsko	Španělsko	k1gNnSc4	Španělsko
také	také	k9	také
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
proticizineckých	proticizinecký	k2eAgInPc2d1	proticizinecký
proudů	proud	k1gInPc2	proud
příchody	příchod	k1gInPc7	příchod
zhruba	zhruba	k6eAd1	zhruba
jednoho	jeden	k4xCgMnSc4	jeden
milionu	milion	k4xCgInSc2	milion
severoafrických	severoafrický	k2eAgMnPc2d1	severoafrický
sezónních	sezónní	k2eAgMnPc2d1	sezónní
pracovníků	pracovník	k1gMnPc2	pracovník
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
vízové	vízový	k2eAgFnSc2d1	vízová
povinnosti	povinnost	k1gFnSc2	povinnost
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
nelegálně	legálně	k6eNd1	legálně
pracující	pracující	k2eAgMnPc1d1	pracující
cizí	cizí	k2eAgMnPc1d1	cizí
občané	občan	k1gMnPc1	občan
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
asi	asi	k9	asi
čtvrt	čtvrt	k1xP	čtvrt
milionu	milion	k4xCgInSc2	milion
<g/>
,	,	kIx,	,
přicházeli	přicházet	k5eAaImAgMnP	přicházet
především	především	k9	především
z	z	k7c2	z
Maroka	Maroko	k1gNnSc2	Maroko
a	a	k8xC	a
z	z	k7c2	z
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
/	/	kIx~	/
<g/>
Peru	Peru	k1gNnSc1	Peru
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Pomocnice	pomocnice	k1gFnSc1	pomocnice
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
přicházely	přicházet	k5eAaImAgInP	přicházet
koncem	koncem	k7c2	koncem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
z	z	k7c2	z
Filipín	Filipíny	k1gFnPc2	Filipíny
a	a	k8xC	a
Dominikánské	dominikánský	k2eAgFnSc2d1	Dominikánská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
však	však	k8xC	však
mezi	mezi	k7c7	mezi
příchozími	příchozí	k1gFnPc7	příchozí
převážili	převážit	k5eAaPmAgMnP	převážit
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
zastoupení	zastoupení	k1gNnSc1	zastoupení
mají	mít	k5eAaImIp3nP	mít
Rumuni	Rumun	k1gMnPc1	Rumun
(	(	kIx(	(
<g/>
729	[number]	k4	729
000	[number]	k4	000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
stoupá	stoupat	k5eAaImIp3nS	stoupat
podíl	podíl	k1gInSc1	podíl
Britů	Brit	k1gMnPc2	Brit
(	(	kIx(	(
<g/>
352	[number]	k4	352
000	[number]	k4	000
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
občanů	občan	k1gMnPc2	občan
starých	starý	k2eAgInPc2d1	starý
států	stát	k1gInPc2	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
<g/>
např	např	kA	např
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
i	i	k8xC	i
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
imigrantů	imigrant	k1gMnPc2	imigrant
se	se	k3xPyFc4	se
usazuje	usazovat	k5eAaImIp3nS	usazovat
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
a	a	k8xC	a
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
Galicie	Galicie	k1gFnSc2	Galicie
či	či	k8xC	či
Asturie	Asturie	k1gFnSc2	Asturie
příliv	příliv	k1gInSc1	příliv
cizinců	cizinec	k1gMnPc2	cizinec
téměř	téměř	k6eAd1	téměř
nepocítily	pocítit	k5eNaPmAgInP	pocítit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Největší	veliký	k2eAgNnPc1d3	veliký
města	město	k1gNnPc1	město
a	a	k8xC	a
aglomerace	aglomerace	k1gFnPc1	aglomerace
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Tradiční	tradiční	k2eAgFnSc1d1	tradiční
španělská	španělský	k2eAgFnSc1d1	španělská
kultura	kultura	k1gFnSc1	kultura
je	být	k5eAaImIp3nS	být
pevně	pevně	k6eAd1	pevně
zakořeněna	zakořenit	k5eAaPmNgFnS	zakořenit
v	v	k7c6	v
katolictví	katolictví	k1gNnSc6	katolictví
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
mocný	mocný	k2eAgInSc4d1	mocný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
tvorbu	tvorba	k1gFnSc4	tvorba
tradice	tradice	k1gFnSc2	tradice
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
například	například	k6eAd1	například
procesí	procesí	k1gNnSc4	procesí
o	o	k7c6	o
velikonočním	velikonoční	k2eAgInSc6d1	velikonoční
týdnu	týden	k1gInSc6	týden
"	"	kIx"	"
<g/>
Semana	Seman	k1gMnSc4	Seman
Santa	Sant	k1gMnSc4	Sant
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vlivy	vliv	k1gInPc1	vliv
cikánské	cikánský	k2eAgFnSc2d1	cikánská
kultury	kultura	k1gFnSc2	kultura
se	se	k3xPyFc4	se
odrážejí	odrážet	k5eAaImIp3nP	odrážet
v	v	k7c6	v
tanci	tanec	k1gInSc6	tanec
a	a	k8xC	a
hudbě	hudba	k1gFnSc3	hudba
flamenca	flamenco	k1gNnSc2	flamenco
<g/>
,	,	kIx,	,
maurské	maurský	k2eAgNnSc1d1	maurské
dědictví	dědictví	k1gNnSc1	dědictví
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
nejvíce	hodně	k6eAd3	hodně
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
(	(	kIx(	(
<g/>
Alhambra	Alhambra	k1gFnSc1	Alhambra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
španělskou	španělský	k2eAgFnSc7d1	španělská
tradicí	tradice	k1gFnSc7	tradice
jsou	být	k5eAaImIp3nP	být
býčí	býčí	k2eAgInPc1d1	býčí
zápasy	zápas	k1gInPc1	zápas
neboli	neboli	k8xC	neboli
corrida	corrida	k1gFnSc1	corrida
<g/>
,	,	kIx,	,
provozované	provozovaný	k2eAgNnSc1d1	provozované
zejména	zejména	k9	zejména
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
<g/>
;	;	kIx,	;
v	v	k7c6	v
Katalánsku	Katalánsko	k1gNnSc6	Katalánsko
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
zákazu	zákaz	k1gInSc6	zákaz
<g/>
.	.	kIx.	.
<g/>
Španělsko	Španělsko	k1gNnSc4	Španělsko
proslavili	proslavit	k5eAaPmAgMnP	proslavit
světově	světově	k6eAd1	světově
věhlasní	věhlasný	k2eAgMnPc1d1	věhlasný
umělci	umělec	k1gMnPc1	umělec
–	–	k?	–
čtyři	čtyři	k4xCgInPc1	čtyři
z	z	k7c2	z
nejslavnějších	slavný	k2eAgInPc2d3	nejslavnější
světových	světový	k2eAgInPc2d1	světový
malířů	malíř	k1gMnPc2	malíř
Francisco	Francisco	k1gMnSc1	Francisco
de	de	k?	de
Goya	Goya	k1gMnSc1	Goya
<g/>
,	,	kIx,	,
Diego	Diego	k1gMnSc1	Diego
Velázquez	Velázquez	k1gMnSc1	Velázquez
<g/>
,	,	kIx,	,
Pablo	Pablo	k1gNnSc1	Pablo
Picasso	Picassa	k1gFnSc5	Picassa
a	a	k8xC	a
Salvador	Salvador	k1gInSc4	Salvador
Dalí	Dalí	k1gFnSc2	Dalí
byli	být	k5eAaImAgMnP	být
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
.	.	kIx.	.
</s>
<s>
Originálním	originální	k2eAgMnSc7d1	originální
modernistou	modernista	k1gMnSc7	modernista
byl	být	k5eAaImAgMnS	být
též	též	k9	též
Joan	Joan	k1gMnSc1	Joan
Miró	Miró	k1gMnSc1	Miró
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
slavný	slavný	k2eAgMnSc1d1	slavný
řecký	řecký	k2eAgMnSc1d1	řecký
malíř	malíř	k1gMnSc1	malíř
El	Ela	k1gFnPc2	Ela
Greco	Greco	k1gMnSc1	Greco
<g/>
.	.	kIx.	.
</s>
<s>
Jusepe	Jusepat	k5eAaPmIp3nS	Jusepat
de	de	k?	de
Ribera	Ribera	k1gFnSc1	Ribera
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
středověký	středověký	k2eAgInSc1d1	středověký
tenebrismus	tenebrismus	k1gInSc1	tenebrismus
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovými	klíčový	k2eAgMnPc7d1	klíčový
představiteli	představitel	k1gMnPc7	představitel
španělského	španělský	k2eAgNnSc2d1	španělské
malířského	malířský	k2eAgNnSc2d1	malířské
baroka	baroko	k1gNnSc2	baroko
byli	být	k5eAaImAgMnP	být
Bartolomè	Bartolomè	k1gMnSc1	Bartolomè
Esteban	Esteban	k1gMnSc1	Esteban
Murillo	Murillo	k1gNnSc4	Murillo
a	a	k8xC	a
Francisco	Francisco	k1gNnSc4	Francisco
de	de	k?	de
Zurbarán	Zurbarán	k1gInSc1	Zurbarán
<g/>
.	.	kIx.	.
</s>
<s>
Enrique	Enrique	k1gNnSc1	Enrique
Simonet	Simonet	k1gMnSc1	Simonet
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
důležitým	důležitý	k2eAgMnSc7d1	důležitý
představitelem	představitel	k1gMnSc7	představitel
tzv.	tzv.	kA	tzv.
orientalismu	orientalismus	k1gInSc2	orientalismus
19.	[number]	k4	19.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Joaquín	Joaquín	k1gMnSc1	Joaquín
Sorolla	Sorolla	k1gMnSc1	Sorolla
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
řadu	řad	k1gInSc2	řad
slavných	slavný	k2eAgFnPc2d1	slavná
historických	historický	k2eAgFnPc2d1	historická
maleb	malba	k1gFnPc2	malba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
avantgardistů	avantgardista	k1gMnPc2	avantgardista
20.	[number]	k4	20.
století	století	k1gNnPc2	století
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
kubista	kubista	k1gMnSc1	kubista
Juan	Juan	k1gMnSc1	Juan
Gris	Gris	k1gInSc1	Gris
<g/>
,	,	kIx,	,
expresionista	expresionista	k1gMnSc1	expresionista
Antón	Antón	k1gMnSc1	Antón
Lamazares	Lamazares	k1gMnSc1	Lamazares
či	či	k8xC	či
reprezentant	reprezentant	k1gMnSc1	reprezentant
informelu	informela	k1gFnSc4	informela
Antoni	Antoň	k1gFnSc3	Antoň
Tà	Tà	k1gFnSc3	Tà
<g/>
.	.	kIx.	.
</s>
<s>
Nejoceňovanějším	oceňovaný	k2eAgMnSc7d3	nejoceňovanější
španělským	španělský	k2eAgMnSc7d1	španělský
sochařem	sochař	k1gMnSc7	sochař
je	být	k5eAaImIp3nS	být
Eduardo	Eduardo	k1gNnSc1	Eduardo
Chillida	Chillida	k1gFnSc1	Chillida
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
svými	svůj	k3xOyFgFnPc7	svůj
abstraktními	abstraktní	k2eAgFnPc7d1	abstraktní
skulpturami	skulptura	k1gFnPc7	skulptura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Antoni	Anton	k1gMnPc1	Anton
Gaudí	Gaudí	k1gNnSc2	Gaudí
byl	být	k5eAaImAgInS	být
legendárním	legendární	k2eAgMnSc7d1	legendární
experimentujícím	experimentující	k2eAgMnSc7d1	experimentující
architektem	architekt	k1gMnSc7	architekt
<g/>
,	,	kIx,	,
představitelem	představitel	k1gMnSc7	představitel
radikálního	radikální	k2eAgInSc2d1	radikální
modernismu	modernismus	k1gInSc2	modernismus
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
je	být	k5eAaImIp3nS	být
Santiago	Santiago	k1gNnSc1	Santiago
Calatrava	Calatrava	k1gFnSc1	Calatrava
<g/>
.	.	kIx.	.
</s>
<s>
Rafael	Rafael	k1gMnSc1	Rafael
Moneo	Moneo	k1gMnSc1	Moneo
je	být	k5eAaImIp3nS	být
nositel	nositel	k1gMnSc1	nositel
prestižní	prestižní	k2eAgFnSc2d1	prestižní
Pritzkerovy	Pritzkerův	k2eAgFnSc2d1	Pritzkerova
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc1d1	zvaná
též	též	k6eAd1	též
"	"	kIx"	"
<g/>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
architekturu	architektura	k1gFnSc4	architektura
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
se	se	k3xPyFc4	se
stejné	stejný	k2eAgFnSc2d1	stejná
pocty	pocta	k1gFnSc2	pocta
dostalo	dostat	k5eAaPmAgNnS	dostat
katalánskému	katalánský	k2eAgNnSc3d1	katalánské
studiu	studio	k1gNnSc3	studio
RCR	RCR	kA	RCR
Arquitectes	Arquitectes	k1gInSc4	Arquitectes
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholnou	vrcholný	k2eAgFnSc4d1	vrcholná
španělskou	španělský	k2eAgFnSc4d1	španělská
renesanční	renesanční	k2eAgFnSc4d1	renesanční
architekturu	architektura	k1gFnSc4	architektura
tvořil	tvořit	k5eAaImAgMnS	tvořit
zejména	zejména	k9	zejména
Juan	Juan	k1gMnSc1	Juan
de	de	k?	de
Herrera	Herrera	k1gFnSc1	Herrera
(	(	kIx(	(
<g/>
Královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
v	v	k7c6	v
Aranjuezu	Aranjuez	k1gInSc6	Aranjuez
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lluís	Lluís	k6eAd1	Lluís
Domè	Domè	k1gFnSc1	Domè
i	i	k8xC	i
Montaner	Montaner	k1gInSc1	Montaner
byl	být	k5eAaImAgInS	být
v	v	k7c4	v
19.	[number]	k4	19.
století	století	k1gNnPc2	století
klíčovým	klíčový	k2eAgMnSc7d1	klíčový
představitelem	představitel	k1gMnSc7	představitel
katalánského	katalánský	k2eAgInSc2d1	katalánský
modernismu	modernismus	k1gInSc2	modernismus
<g/>
.	.	kIx.	.
</s>
<s>
Ricardo	Ricardo	k1gNnSc1	Ricardo
Bofill	Bofill	k1gMnSc1	Bofill
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
v	v	k7c6	v
21.	[number]	k4	21.
století	století	k1gNnSc6	století
i	i	k8xC	i
na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
Prahy	Praha	k1gFnSc2	Praha
(	(	kIx(	(
<g/>
Corso	Corsa	k1gFnSc5	Corsa
Karlín	Karlín	k1gInSc1	Karlín
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bratislavy	Bratislava	k1gFnSc2	Bratislava
(	(	kIx(	(
<g/>
Panorama	panorama	k1gNnSc1	panorama
City	city	k1gNnSc1	city
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
španělským	španělský	k2eAgMnSc7d1	španělský
skladatelem	skladatel	k1gMnSc7	skladatel
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
Manuel	Manuel	k1gMnSc1	Manuel
de	de	k?	de
Falla	Falla	k1gMnSc1	Falla
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
autorem	autor	k1gMnSc7	autor
16.	[number]	k4	16.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
Tomás	Tomás	k1gInSc1	Tomás
Luis	Luisa	k1gFnPc2	Luisa
de	de	k?	de
Victoria	Victorium	k1gNnSc2	Victorium
<g/>
.	.	kIx.	.
</s>
<s>
Isaac	Isaac	k6eAd1	Isaac
Albéniz	Albéniz	k1gInSc1	Albéniz
napsal	napsat	k5eAaPmAgInS	napsat
řadu	řada	k1gFnSc4	řada
moderních	moderní	k2eAgFnPc2d1	moderní
skladeb	skladba	k1gFnPc2	skladba
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
španělského	španělský	k2eAgInSc2d1	španělský
folklóru	folklór	k1gInSc2	folklór
<g/>
,	,	kIx,	,
Enrique	Enrique	k1gFnSc1	Enrique
Granados	Granados	k1gInSc1	Granados
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
vytvořit	vytvořit	k5eAaPmF	vytvořit
španělský	španělský	k2eAgInSc1d1	španělský
národní	národní	k2eAgInSc1d1	národní
styl	styl	k1gInSc1	styl
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
Joaquín	Joaquín	k1gMnSc1	Joaquín
Rodrigo	Rodrigo	k1gMnSc1	Rodrigo
probojovával	probojovávat	k5eAaImAgMnS	probojovávat
vstup	vstup	k1gInSc4	vstup
kytary	kytara	k1gFnSc2	kytara
do	do	k7c2	do
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
interpretů	interpret	k1gMnPc2	interpret
získali	získat	k5eAaPmAgMnP	získat
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
věhlas	věhlas	k1gInSc4	věhlas
tenoristé	tenorista	k1gMnPc1	tenorista
Plácido	Plácida	k1gFnSc5	Plácida
Domingo	Domingo	k1gNnSc4	Domingo
a	a	k8xC	a
José	José	k1gNnSc4	José
Carreras	Carreras	k1gMnSc1	Carreras
<g/>
,	,	kIx,	,
houslista	houslista	k1gMnSc1	houslista
Pablo	Pablo	k1gNnSc1	Pablo
de	de	k?	de
Sarasate	Sarasat	k1gMnSc5	Sarasat
<g/>
,	,	kIx,	,
violoncellista	violoncellista	k1gMnSc1	violoncellista
Pablo	Pablo	k1gNnSc1	Pablo
Casals	Casals	k1gInSc1	Casals
<g/>
,	,	kIx,	,
gambista	gambista	k1gMnSc1	gambista
Jordi	Jord	k1gMnPc1	Jord
Savall	Savall	k1gMnSc1	Savall
či	či	k8xC	či
kytaristé	kytarista	k1gMnPc1	kytarista
Francisco	Francisco	k6eAd1	Francisco
Tárrega	Tárreg	k1gMnSc4	Tárreg
<g/>
,	,	kIx,	,
Fernando	Fernanda	k1gFnSc5	Fernanda
Sor	Sor	k1gMnPc6	Sor
a	a	k8xC	a
Andrés	Andrés	k1gInSc4	Andrés
Segovia	Segovium	k1gNnSc2	Segovium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ryze	ryze	k6eAd1	ryze
národním	národní	k2eAgInSc6d1	národní
stylu	styl	k1gInSc6	styl
flamenco	flamenco	k1gNnSc4	flamenco
se	se	k3xPyFc4	se
prosadili	prosadit	k5eAaPmAgMnP	prosadit
kytaristé	kytarista	k1gMnPc1	kytarista
Paco	Paco	k1gMnSc1	Paco
de	de	k?	de
Lucía	Lucía	k1gMnSc1	Lucía
a	a	k8xC	a
Camarón	Camarón	k1gMnSc1	Camarón
de	de	k?	de
la	la	k1gNnSc4	la
Isla	Isl	k1gInSc2	Isl
<g/>
.	.	kIx.	.
</s>
<s>
Slavnou	slavný	k2eAgFnSc7d1	slavná
tanečnicí	tanečnice	k1gFnSc7	tanečnice
flamenca	flamenco	k1gNnSc2	flamenco
byla	být	k5eAaImAgFnS	být
Carmen	Carmen	k1gInSc4	Carmen
Amaya	Amay	k1gInSc2	Amay
<g/>
,	,	kIx,	,
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Lola	Lol	k1gInSc2	Lol
Flores	Flores	k1gInSc1	Flores
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
globálního	globální	k2eAgInSc2d1	globální
popu	pop	k1gInSc2	pop
pronikl	proniknout	k5eAaPmAgInS	proniknout
Julio	Julio	k6eAd1	Julio
Iglesias	Iglesias	k1gInSc1	Iglesias
<g/>
,	,	kIx,	,
Enrique	Enrique	k1gNnSc1	Enrique
Iglesias	Iglesiasa	k1gFnPc2	Iglesiasa
či	či	k8xC	či
Alejandro	Alejandra	k1gFnSc5	Alejandra
Sanz	Sanz	k1gInSc1	Sanz
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
povedlo	povést	k5eAaPmAgNnS	povést
i	i	k9	i
skupinám	skupina	k1gFnPc3	skupina
Baccara	Baccar	k1gMnSc2	Baccar
(	(	kIx(	(
<g/>
hit	hit	k1gInSc4	hit
Sorry	sorry	k9	sorry
<g/>
,	,	kIx,	,
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
a	a	k8xC	a
Lady	lady	k1gFnSc1	lady
<g/>
)	)	kIx)	)
či	či	k8xC	či
Las	laso	k1gNnPc2	laso
Ketchup	Ketchup	k1gInSc1	Ketchup
(	(	kIx(	(
<g/>
Aserejé	Aserejé	k1gNnSc1	Aserejé
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Massiel	Massiel	k1gInSc4	Massiel
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Eurovision	Eurovision	k1gInSc4	Eurovision
Song	song	k1gInSc1	song
Contest	Contest	k1gInSc1	Contest
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968.	[number]	k4	1968.
</s>
</p>
<p>
</p>
<p>
<s>
Lope	Lope	k1gFnSc1	Lope
de	de	k?	de
Vega	Vega	k1gFnSc1	Vega
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejslavnějším	slavný	k2eAgMnPc3d3	nejslavnější
světovým	světový	k2eAgMnPc3d1	světový
dramatikům	dramatik	k1gMnPc3	dramatik
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
prózy	próza	k1gFnSc2	próza
a	a	k8xC	a
poezie	poezie	k1gFnSc2	poezie
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
Španělů	Španěl	k1gMnPc2	Španěl
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
kánonu	kánon	k1gInSc2	kánon
světové	světový	k2eAgFnSc2d1	světová
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
na	na	k7c6	na
nejpřednějším	přední	k2eAgNnSc6d3	nejpřednější
místě	místo	k1gNnSc6	místo
jistě	jistě	k9	jistě
Miguel	Miguel	k1gMnSc1	Miguel
de	de	k?	de
Cervantes	Cervantes	k1gMnSc1	Cervantes
y	y	k?	y
Saavedra	Saavedra	k1gFnSc1	Saavedra
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
proslulého	proslulý	k2eAgInSc2d1	proslulý
románu	román	k1gInSc2	román
o	o	k7c6	o
Donu	Don	k1gMnSc6	Don
Quijotovi	Quijot	k1gMnSc6	Quijot
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
mezi	mezi	k7c7	mezi
klasiky	klasik	k1gMnPc7	klasik
patří	patřit	k5eAaImIp3nS	patřit
též	též	k9	též
Pedro	Pedro	k1gNnSc1	Pedro
Calderón	Calderón	k1gMnSc1	Calderón
de	de	k?	de
la	la	k1gNnSc4	la
Barca	Barc	k1gInSc2	Barc
<g/>
,	,	kIx,	,
Federico	Federico	k6eAd1	Federico
García	Garcí	k2eAgFnSc1d1	García
Lorca	Lorca	k1gFnSc1	Lorca
<g/>
,	,	kIx,	,
Fernando	Fernanda	k1gFnSc5	Fernanda
de	de	k?	de
Rojas	Rojas	k1gMnSc1	Rojas
<g/>
,	,	kIx,	,
Gustavo	Gustava	k1gFnSc5	Gustava
Adolfo	Adolfo	k1gNnSc1	Adolfo
Bécquer	Bécquer	k1gInSc1	Bécquer
<g/>
,	,	kIx,	,
Benito	Benit	k2eAgNnSc1d1	Benito
Pérez	Péreza	k1gFnPc2	Péreza
Galdós	Galdós	k1gInSc1	Galdós
<g/>
,	,	kIx,	,
z	z	k7c2	z
moderních	moderní	k2eAgMnPc2d1	moderní
tvůrců	tvůrce	k1gMnPc2	tvůrce
Miguel	Miguel	k1gMnSc1	Miguel
de	de	k?	de
Unamuno	Unamuna	k1gFnSc5	Unamuna
<g/>
,	,	kIx,	,
Antonio	Antonio	k1gMnSc1	Antonio
Machado	Machada	k1gFnSc5	Machada
či	či	k8xC	či
Matilde	Matild	k1gInSc5	Matild
Camusová	Camusový	k2eAgFnSc1d1	Camusový
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
autorům	autor	k1gMnPc3	autor
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
rovněž	rovněž	k9	rovněž
José	José	k1gNnSc1	José
de	de	k?	de
Espronceda	Espronceda	k1gMnSc1	Espronceda
<g/>
,	,	kIx,	,
Luis	Luisa	k1gFnPc2	Luisa
de	de	k?	de
Góngora	Góngor	k1gMnSc2	Góngor
<g/>
,	,	kIx,	,
Vicente	Vicent	k1gMnSc5	Vicent
Blasco	Blasca	k1gMnSc5	Blasca
Ibáñ	Ibáñ	k1gMnSc5	Ibáñ
<g/>
,	,	kIx,	,
Pío	Pío	k1gMnPc7	Pío
Baroja	Barojum	k1gNnSc2	Barojum
<g/>
,	,	kIx,	,
Rafael	Rafaela	k1gFnPc2	Rafaela
Alberti	Albert	k1gMnPc1	Albert
či	či	k8xC	či
Carlos	Carlos	k1gMnSc1	Carlos
Ruiz	Ruiz	k1gMnSc1	Ruiz
Zafón	Zafón	k1gMnSc1	Zafón
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
otcům	otec	k1gMnPc3	otec
zakladatelům	zakladatel	k1gMnPc3	zakladatel
pikareskního	pikareskní	k2eAgInSc2d1	pikareskní
románu	román	k1gInSc2	román
patří	patřit	k5eAaImIp3nS	patřit
Francisco	Francisco	k6eAd1	Francisco
de	de	k?	de
Quevedo	Quevedo	k1gNnSc4	Quevedo
<g/>
.	.	kIx.	.
</s>
<s>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
získali	získat	k5eAaPmAgMnP	získat
Camilo	Camila	k1gFnSc5	Camila
José	José	k1gNnPc1	José
Cela	cela	k1gFnSc1	cela
<g/>
,	,	kIx,	,
Vicente	Vicent	k1gInSc5	Vicent
Aleixandre	Aleixandr	k1gInSc5	Aleixandr
<g/>
,	,	kIx,	,
Juan	Juan	k1gMnSc1	Juan
Ramón	Ramón	k1gMnSc1	Ramón
Jiménez	Jiménez	k1gMnSc1	Jiménez
<g/>
,	,	kIx,	,
Jacinto	Jacinta	k1gFnSc5	Jacinta
Benavente	Benavent	k1gInSc5	Benavent
a	a	k8xC	a
José	Josý	k2eAgFnPc4d1	Josý
Echegaray	Echegaraa	k1gFnPc4	Echegaraa
<g/>
.	.	kIx.	.
</s>
<s>
Nejprestižnějším	prestižní	k2eAgNnSc7d3	nejprestižnější
španělským	španělský	k2eAgNnSc7d1	španělské
literárním	literární	k2eAgNnSc7d1	literární
oceněním	ocenění	k1gNnSc7	ocenění
je	být	k5eAaImIp3nS	být
Cervantesova	Cervantesův	k2eAgFnSc1d1	Cervantesova
cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mohou	moct	k5eAaImIp3nP	moct
získat	získat	k5eAaPmF	získat
nejen	nejen	k6eAd1	nejen
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všichni	všechen	k3xTgMnPc1	všechen
španělsky	španělsky	k6eAd1	španělsky
píšící	píšící	k2eAgMnPc1d1	píšící
autoři	autor	k1gMnPc1	autor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
nabývá	nabývat	k5eAaImIp3nS	nabývat
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
na	na	k7c6	na
významu	význam	k1gInSc6	význam
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
,	,	kIx,	,
známými	známý	k2eAgMnPc7d1	známý
režiséry	režisér	k1gMnPc7	režisér
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Luis	Luisa	k1gFnPc2	Luisa
Buňuel	Buňuel	k1gMnSc1	Buňuel
<g/>
,	,	kIx,	,
Carlos	Carlos	k1gMnSc1	Carlos
Saura	Saura	k1gMnSc1	Saura
<g/>
,	,	kIx,	,
Jess	Jess	k1gInSc1	Jess
Franco	Franco	k6eAd1	Franco
<g/>
,	,	kIx,	,
Fernando	Fernanda	k1gFnSc5	Fernanda
Arrabal	Arrabal	k1gInSc1	Arrabal
<g/>
,	,	kIx,	,
Bigas	Bigas	k1gInSc1	Bigas
Luna	luna	k1gFnSc1	luna
<g/>
,	,	kIx,	,
Fernando	Fernanda	k1gFnSc5	Fernanda
Trueba	Trueba	k1gFnSc1	Trueba
<g/>
,	,	kIx,	,
Luis	Luisa	k1gFnPc2	Luisa
García	Garcí	k2eAgFnSc1d1	García
Berlanga	Berlanga	k1gFnSc1	Berlanga
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
soudobé	soudobý	k2eAgMnPc4d1	soudobý
tvůrce	tvůrce	k1gMnPc4	tvůrce
patří	patřit	k5eAaImIp3nS	patřit
Alejandro	Alejandra	k1gFnSc5	Alejandra
Amenábar	Amenábar	k1gInSc1	Amenábar
<g/>
,	,	kIx,	,
Jaume	Jaum	k1gInSc5	Jaum
Collet-Serra	Collet-Serro	k1gNnSc2	Collet-Serro
<g/>
,	,	kIx,	,
Isabel	Isabela	k1gFnPc2	Isabela
Coixetová	Coixetový	k2eAgFnSc1d1	Coixetová
či	či	k8xC	či
Pedro	Pedro	k1gNnSc1	Pedro
Almodóvar	Almodóvar	k1gInSc4	Almodóvar
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInPc6	jehož
filmech	film	k1gInPc6	film
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
herečka	herečka	k1gFnSc1	herečka
Penélope	Penélop	k1gInSc5	Penélop
Cruz	Cruza	k1gFnPc2	Cruza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
se	se	k3xPyFc4	se
prosadili	prosadit	k5eAaPmAgMnP	prosadit
herci	herec	k1gMnPc1	herec
Antonio	Antonio	k1gMnSc1	Antonio
Banderas	Banderas	k1gMnSc1	Banderas
a	a	k8xC	a
Javier	Javier	k1gMnSc1	Javier
Bardem	bard	k1gMnSc7	bard
<g/>
.	.	kIx.	.
</s>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Brühl	Brühl	k1gMnSc1	Brühl
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
v	v	k7c6	v
komedii	komedie	k1gFnSc6	komedie
Good	Good	k1gMnSc1	Good
Bye	Bye	k1gMnSc1	Bye
<g/>
,	,	kIx,	,
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Památky	památka	k1gFnPc1	památka
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
disponuje	disponovat	k5eAaBmIp3nS	disponovat
mimořádným	mimořádný	k2eAgNnSc7d1	mimořádné
kulturním	kulturní	k2eAgNnSc7d1	kulturní
dědictvím	dědictví	k1gNnSc7	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
byly	být	k5eAaImAgFnP	být
zapsány	zapsán	k2eAgFnPc4d1	zapsána
jeskyně	jeskyně	k1gFnPc4	jeskyně
Atapuerca	Atapuerc	k1gInSc2	Atapuerc
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
archeologické	archeologický	k2eAgInPc1d1	archeologický
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
nejstaršího	starý	k2eAgNnSc2d3	nejstarší
osídlení	osídlení	k1gNnSc2	osídlení
hominidů	hominid	k1gMnPc2	hominid
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
jeskyně	jeskyně	k1gFnSc1	jeskyně
Altamira	Altamira	k1gFnSc1	Altamira
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
roku	rok	k1gInSc3	rok
1879	[number]	k4	1879
pravěké	pravěký	k2eAgFnPc1d1	pravěká
nástěnné	nástěnný	k2eAgFnPc1d1	nástěnná
malby	malba	k1gFnPc1	malba
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
s	s	k7c7	s
motivy	motiv	k1gInPc4	motiv
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
plus	plus	k1gInSc1	plus
skalní	skalní	k2eAgFnSc2d1	skalní
malby	malba	k1gFnSc2	malba
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
mnoha	mnoho	k4c3	mnoho
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neolitické	neolitický	k2eAgInPc1d1	neolitický
dolmeny	dolmen	k1gInPc1	dolmen
v	v	k7c6	v
Antequeře	Antequera	k1gFnSc6	Antequera
<g/>
,	,	kIx,	,
maják	maják	k1gInSc4	maják
Herkulova	Herkulův	k2eAgFnSc1d1	Herkulova
věž	věž	k1gFnSc1	věž
z	z	k7c2	z
římských	římský	k2eAgInPc2d1	římský
časů	čas	k1gInPc2	čas
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejlépe	dobře	k6eAd3	dobře
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
antických	antický	k2eAgInPc2d1	antický
akvaduktů	akvadukt	k1gInPc2	akvadukt
–	–	k?	–
akvadukt	akvadukt	k1gInSc1	akvadukt
v	v	k7c4	v
Segovii	Segovie	k1gFnSc4	Segovie
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
antické	antický	k2eAgFnPc4d1	antická
památky	památka	k1gFnPc4	památka
v	v	k7c6	v
Méridě	Mérida	k1gFnSc6	Mérida
<g/>
,	,	kIx,	,
Lugu	Lugum	k1gNnSc6	Lugum
a	a	k8xC	a
Tarragoně	Tarragona	k1gFnSc6	Tarragona
<g/>
,	,	kIx,	,
komplex	komplex	k1gInSc4	komplex
paláců	palác	k1gInPc2	palác
a	a	k8xC	a
pevností	pevnost	k1gFnPc2	pevnost
<g />
.	.	kIx.	.
</s>
<s>
maurských	maurský	k2eAgMnPc2d1	maurský
panovníků	panovník	k1gMnPc2	panovník
Granady	Granada	k1gFnSc2	Granada
Alhambra	Alhambra	k1gFnSc1	Alhambra
<g/>
,	,	kIx,	,
gotické	gotický	k2eAgFnSc2d1	gotická
katedrály	katedrála	k1gFnSc2	katedrála
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Burgosu	Burgos	k1gInSc6	Burgos
a	a	k8xC	a
v	v	k7c6	v
Seville	Sevilla	k1gFnSc6	Sevilla
<g/>
,	,	kIx,	,
bývalé	bývalý	k2eAgInPc1d1	bývalý
královské	královský	k2eAgInPc1d1	královský
paláce	palác	k1gInPc1	palác
El	Ela	k1gFnPc2	Ela
Escorial	Escorial	k1gInSc4	Escorial
u	u	k7c2	u
Madridu	Madrid	k1gInSc2	Madrid
a	a	k8xC	a
Alcázar	Alcázara	k1gFnPc2	Alcázara
v	v	k7c6	v
Seville	Sevilla	k1gFnSc6	Sevilla
<g/>
,	,	kIx,	,
současný	současný	k2eAgInSc1d1	současný
královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
v	v	k7c6	v
Aranjuezu	Aranjuez	k1gInSc6	Aranjuez
<g/>
,	,	kIx,	,
Indický	indický	k2eAgInSc1d1	indický
archiv	archiv	k1gInSc1	archiv
v	v	k7c6	v
Seville	Sevilla	k1gFnSc6	Sevilla
<g/>
,	,	kIx,	,
kláštery	klášter	k1gInPc1	klášter
Poblet	Poblet	k1gInSc1	Poblet
<g/>
,	,	kIx,	,
Santa	Santa	k1gMnSc1	Santa
María	Maríum	k1gNnSc2	Maríum
de	de	k?	de
<g />
.	.	kIx.	.
</s>
<s>
Guadalupe	Guadalupat	k5eAaPmIp3nS	Guadalupat
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Millán	Millán	k2eAgMnSc1d1	Millán
de	de	k?	de
Yuso	Yuso	k1gMnSc1	Yuso
a	a	k8xC	a
San	San	k1gMnSc1	San
Millán	Millán	k2eAgMnSc1d1	Millán
de	de	k?	de
Suso	Suso	k1gMnSc1	Suso
<g/>
,	,	kIx,	,
Llotja	Llotja	k1gMnSc1	Llotja
de	de	k?	de
la	la	k1gNnSc2	la
Seda	Seda	k1gFnSc1	Seda
(	(	kIx(	(
<g/>
hedvábná	hedvábný	k2eAgFnSc1d1	hedvábná
burza	burza	k1gFnSc1	burza
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Valencii	Valencie	k1gFnSc6	Valencie
<g/>
,	,	kIx,	,
série	série	k1gFnSc1	série
renesančních	renesanční	k2eAgFnPc2d1	renesanční
staveb	stavba	k1gFnPc2	stavba
v	v	k7c6	v
Úbedě	Úbeda	k1gFnSc6	Úbeda
a	a	k8xC	a
Baeze	Baeza	k1gFnSc6	Baeza
<g/>
,	,	kIx,	,
secesní	secesní	k2eAgInPc4d1	secesní
skvosty	skvost	k1gInPc4	skvost
Palau	Palaus	k1gInSc2	Palaus
de	de	k?	de
la	la	k1gNnSc2	la
Música	Músicus	k1gMnSc2	Músicus
Catalana	Catalan	k1gMnSc2	Catalan
a	a	k8xC	a
nemocnice	nemocnice	k1gFnSc2	nemocnice
Sant	Sant	k1gMnSc1	Sant
Pau	Pau	k1gMnSc1	Pau
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
či	či	k8xC	či
Biskajský	biskajský	k2eAgInSc4d1	biskajský
most	most	k1gInSc4	most
v	v	k7c6	v
Bilbau	Bilbaus	k1gInSc6	Bilbaus
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgInSc7d3	nejstarší
gondolovým	gondolový	k2eAgInSc7d1	gondolový
mostem	most	k1gInSc7	most
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
byla	být	k5eAaImAgFnS	být
zapsána	zapsat	k5eAaPmNgFnS	zapsat
i	i	k9	i
celá	celý	k2eAgNnPc1d1	celé
historická	historický	k2eAgNnPc1d1	historické
centra	centrum	k1gNnPc1	centrum
měst	město	k1gNnPc2	město
Córdoby	Córdoba	k1gFnSc2	Córdoba
(	(	kIx(	(
<g/>
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
např.	např.	kA	např.
slavná	slavný	k2eAgFnSc1d1	slavná
Mezquita	Mezquita	k1gFnSc1	Mezquita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ávila	Ávila	k1gFnSc1	Ávila
<g/>
,	,	kIx,	,
Toledo	Toledo	k1gNnSc1	Toledo
<g/>
,	,	kIx,	,
Cáceres	Cáceres	k1gInSc1	Cáceres
<g/>
,	,	kIx,	,
Salamanca	Salamanca	k1gFnSc1	Salamanca
<g/>
,	,	kIx,	,
Cuenca	Cuenca	k1gFnSc1	Cuenca
<g/>
,	,	kIx,	,
Alcalá	Alcalý	k2eAgFnSc1d1	Alcalá
de	de	k?	de
Henares	Henares	k1gInSc1	Henares
<g/>
,	,	kIx,	,
Medina	Medina	k1gFnSc1	Medina
Azahara	Azahara	k1gFnSc1	Azahara
<g/>
,	,	kIx,	,
San	San	k1gFnSc1	San
Cristóbal	Cristóbal	k1gInSc1	Cristóbal
de	de	k?	de
La	la	k1gNnSc2	la
Laguna	laguna	k1gFnSc1	laguna
a	a	k8xC	a
Santiago	Santiago	k1gNnSc1	Santiago
de	de	k?	de
Compostela	Compostela	k1gFnSc1	Compostela
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
poutních	poutní	k2eAgNnPc2d1	poutní
míst	místo	k1gNnPc2	místo
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
středu	střed	k1gInSc6	střed
září	zářit	k5eAaImIp3nS	zářit
známá	známý	k2eAgFnSc1d1	známá
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Samostatnou	samostatný	k2eAgFnSc7d1	samostatná
položkou	položka	k1gFnSc7	položka
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
je	být	k5eAaImIp3nS	být
i	i	k9	i
skupina	skupina	k1gFnSc1	skupina
sedmi	sedm	k4xCc2	sedm
barcelonských	barcelonský	k2eAgFnPc2d1	barcelonská
staveb	stavba	k1gFnPc2	stavba
architekta	architekt	k1gMnSc2	architekt
Antoni	Antoň	k1gFnSc6	Antoň
Gaudího	Gaudí	k1gMnSc4	Gaudí
<g/>
:	:	kIx,	:
Casa	Casa	k1gFnSc1	Casa
Vicens	Vicens	k1gInSc1	Vicens
<g/>
,	,	kIx,	,
Casa	Cas	k2eAgFnSc1d1	Casa
Batlló	Batlló	k1gFnSc1	Batlló
<g/>
,	,	kIx,	,
Casa	Cas	k2eAgFnSc1d1	Casa
Milà	Milà	k1gFnSc1	Milà
<g/>
,	,	kIx,	,
Palác	palác	k1gInSc1	palác
Güell	Güell	k1gInSc1	Güell
<g/>
,	,	kIx,	,
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
kostel	kostel	k1gInSc1	kostel
Colò	Colò	k1gFnPc2	Colò
Güell	Güell	k1gInSc1	Güell
<g/>
,	,	kIx,	,
park	park	k1gInSc1	park
Güell	Güell	k1gInSc1	Güell
a	a	k8xC	a
monumentální	monumentální	k2eAgInSc1d1	monumentální
chrám	chrám	k1gInSc1	chrám
Sagrada	Sagrada	k1gFnSc1	Sagrada
Família	Família	k1gFnSc1	Família
<g/>
,	,	kIx,	,
asi	asi	k9	asi
nejslavnější	slavný	k2eAgFnSc7d3	nejslavnější
Gaudího	Gaudí	k1gMnSc4	Gaudí
stavba	stavba	k1gFnSc1	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nejnovějších	nový	k2eAgFnPc2d3	nejnovější
staveb	stavba	k1gFnPc2	stavba
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
velké	velký	k2eAgFnPc4d1	velká
diskuse	diskuse	k1gFnPc4	diskuse
Auditorium	auditorium	k1gNnSc1	auditorium
na	na	k7c4	na
Tenerife	Tenerif	k1gInSc5	Tenerif
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
architekta	architekt	k1gMnSc2	architekt
Santiago	Santiago	k1gNnSc1	Santiago
Calatravy	Calatrava	k1gFnPc1	Calatrava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pera	pero	k1gNnSc2	pero
stejného	stejný	k2eAgMnSc4d1	stejný
tvůrce	tvůrce	k1gMnSc4	tvůrce
je	být	k5eAaImIp3nS	být
i	i	k9	i
unikátní	unikátní	k2eAgNnSc1d1	unikátní
Město	město	k1gNnSc1	město
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
reprezentativní	reprezentativní	k2eAgNnSc1d1	reprezentativní
národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
ve	v	k7c6	v
Valencii	Valencie	k1gFnSc6	Valencie
<g/>
,	,	kIx,	,
vybudované	vybudovaný	k2eAgFnPc1d1	vybudovaná
v	v	k7c6	v
letech	let	k1gInPc6	let
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgInSc7d1	nový
symbolem	symbol	k1gInSc7	symbol
Madridu	Madrid	k1gInSc2	Madrid
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgFnPc1	čtyři
výškové	výškový	k2eAgFnPc1d1	výšková
budovy	budova	k1gFnPc1	budova
v	v	k7c4	v
Cuatro	Cuatro	k1gNnSc4	Cuatro
Torres	Torres	k1gInSc4	Torres
Business	business	k1gInSc1	business
Area	area	k1gFnSc1	area
<g/>
:	:	kIx,	:
Torre	torr	k1gInSc5	torr
Caja	Caj	k2eAgFnSc1d1	Caja
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
Torre	torr	k1gInSc5	torr
de	de	k?	de
Cristal	Cristal	k1gMnSc6	Cristal
<g/>
,	,	kIx,	,
Torre	torr	k1gInSc5	torr
Sacyr	Sacyr	k1gInSc1	Sacyr
Vallehermoso	Vallehermosa	k1gFnSc5	Vallehermosa
a	a	k8xC	a
Torre	torr	k1gInSc5	torr
Espacio	Espacia	k1gFnSc5	Espacia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Věda	věda	k1gFnSc1	věda
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
má	mít	k5eAaImIp3nS	mít
významnou	významný	k2eAgFnSc4d1	významná
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Již	jenž	k3xRgFnSc4	jenž
oblast	oblast	k1gFnSc4	oblast
Al-Andalus	Al-Andalus	k1gInSc1	Al-Andalus
<g/>
,	,	kIx,	,
obývaná	obývaný	k2eAgFnSc1d1	obývaná
od	od	k7c2	od
8.	[number]	k4	8.
do	do	k7c2	do
15.	[number]	k4	15.
století	století	k1gNnSc1	století
muslimskými	muslimský	k2eAgMnPc7d1	muslimský
Maury	Maur	k1gMnPc7	Maur
(	(	kIx(	(
<g/>
současná	současný	k2eAgFnSc1d1	současná
Andalusie	Andalusie	k1gFnSc1	Andalusie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
na	na	k7c6	na
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgFnSc3d2	vyšší
vědecké	vědecký	k2eAgFnSc3d1	vědecká
úrovni	úroveň	k1gFnSc3	úroveň
než	než	k8xS	než
zbytek	zbytek	k1gInSc4	zbytek
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějšími	důležitý	k2eAgMnPc7d3	nejdůležitější
astronomy	astronom	k1gMnPc7	astronom
byli	být	k5eAaImAgMnP	být
Az-Zarkálí	Az-Zarkálý	k2eAgMnPc1d1	Az-Zarkálý
<g/>
,	,	kIx,	,
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Toleda	Toledo	k1gNnSc2	Toledo
<g/>
,	,	kIx,	,
a	a	k8xC	a
Džabir	Džabir	k1gMnSc1	Džabir
Ibn	Ibn	k1gMnSc1	Ibn
Aflach	Aflach	k1gMnSc1	Aflach
ze	z	k7c2	z
Sevilly	Sevilla	k1gFnSc2	Sevilla
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgNnSc4d1	tehdejší
andaluské	andaluský	k2eAgNnSc4d1	andaluské
lékařství	lékařství	k1gNnSc4	lékařství
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
například	například	k6eAd1	například
Abulcasis	Abulcasis	k1gInSc1	Abulcasis
(	(	kIx(	(
<g/>
Al-Zahráví	Al-Zahráví	k1gNnSc1	Al-Zahráví
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Ibn	Ibn	k1gMnSc1	Ibn
al-Bajtár	al-Bajtár	k1gMnSc1	al-Bajtár
<g/>
.	.	kIx.	.
</s>
<s>
Abbás	Abbás	k1gInSc1	Abbás
ibn	ibn	k?	ibn
Firnás	Firnás	k1gInSc1	Firnás
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
zvětšovací	zvětšovací	k2eAgNnSc4d1	zvětšovací
sklo	sklo	k1gNnSc4	sklo
<g/>
,	,	kIx,	,
metronom	metronom	k1gInSc4	metronom
a	a	k8xC	a
vodní	vodní	k2eAgFnSc2d1	vodní
hodiny	hodina	k1gFnSc2	hodina
<g/>
,	,	kIx,	,
přinesl	přinést	k5eAaPmAgInS	přinést
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
desítkovou	desítkový	k2eAgFnSc4d1	desítková
soustavu	soustava	k1gFnSc4	soustava
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
průkopníkem	průkopník	k1gMnSc7	průkopník
bezmotorového	bezmotorový	k2eAgNnSc2d1	bezmotorové
létání	létání	k1gNnSc2	létání
<g/>
.	.	kIx.	.
</s>
<s>
Andaluská	andaluský	k2eAgFnSc1d1	andaluská
tradice	tradice	k1gFnSc1	tradice
byla	být	k5eAaImAgFnS	být
přetnuta	přetnout	k5eAaPmNgFnS	přetnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
věda	věda	k1gFnSc1	věda
brzy	brzy	k6eAd1	brzy
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
vysoké	vysoký	k2eAgFnPc4d1	vysoká
úrovně	úroveň	k1gFnPc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
středověkým	středověký	k2eAgMnSc7d1	středověký
učencem	učenec	k1gMnSc7	učenec
byl	být	k5eAaImAgMnS	být
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
Mallorca	Mallorc	k1gInSc2	Mallorc
Ramon	Ramona	k1gFnPc2	Ramona
Llull	Llull	k1gInSc1	Llull
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
zvláště	zvláště	k6eAd1	zvláště
logiku	logika	k1gFnSc4	logika
a	a	k8xC	a
kombinatoriku	kombinatorika	k1gFnSc4	kombinatorika
<g/>
.	.	kIx.	.
</s>
<s>
Dospěl	dochvít	k5eAaPmAgInS	dochvít
až	až	k9	až
k	k	k7c3	k
myšlence	myšlenka	k1gFnSc3	myšlenka
vytvoření	vytvoření	k1gNnSc2	vytvoření
strojů	stroj	k1gInPc2	stroj
s	s	k7c7	s
umělou	umělý	k2eAgFnSc7d1	umělá
inteligencí	inteligence	k1gFnSc7	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Arnald	Arnald	k1gMnSc1	Arnald
z	z	k7c2	z
Villanovy	Villanův	k2eAgFnSc2d1	Villanova
byl	být	k5eAaImAgMnS	být
významným	významný	k2eAgMnSc7d1	významný
středověkým	středověký	k2eAgMnSc7d1	středověký
alchymistou	alchymista	k1gMnSc7	alchymista
<g/>
,	,	kIx,	,
Juan	Juan	k1gMnSc1	Juan
de	de	k?	de
Herrera	Herrero	k1gNnSc2	Herrero
matematikem	matematik	k1gMnSc7	matematik
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
architektem	architekt	k1gMnSc7	architekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
moderní	moderní	k2eAgFnSc2d1	moderní
vědy	věda	k1gFnSc2	věda
položil	položit	k5eAaPmAgMnS	položit
Michael	Michael	k1gMnSc1	Michael
Servetus	Servetus	k1gMnSc1	Servetus
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
Evropan	Evropan	k1gMnSc1	Evropan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vědecky	vědecky	k6eAd1	vědecky
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
plicní	plicní	k2eAgInSc1d1	plicní
oběh	oběh	k1gInSc1	oběh
<g/>
,	,	kIx,	,
upálený	upálený	k2eAgInSc1d1	upálený
protestanty	protestant	k1gMnPc7	protestant
jako	jako	k8xS	jako
kacíř	kacíř	k1gMnSc1	kacíř
<g/>
.	.	kIx.	.
</s>
<s>
Juan	Juan	k1gMnSc1	Juan
de	de	k?	de
la	la	k1gNnSc2	la
Cierva	Cierv	k1gMnSc2	Cierv
je	být	k5eAaImIp3nS	být
vynálezce	vynálezce	k1gMnSc4	vynálezce
vírníku	vírník	k1gInSc2	vírník
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
létajícího	létající	k2eAgInSc2d1	létající
stroje	stroj	k1gInSc2	stroj
podobného	podobný	k2eAgInSc2d1	podobný
vrtulníku	vrtulník	k1gInSc2	vrtulník
<g/>
,	,	kIx,	,
s	s	k7c7	s
rotujícími	rotující	k2eAgFnPc7d1	rotující
nosnými	nosný	k2eAgFnPc7d1	nosná
plochami	plocha	k1gFnPc7	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Narcís	Narcís	k6eAd1	Narcís
Monturiol	Monturiol	k1gInSc1	Monturiol
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
první	první	k4xOgFnSc4	první
ponorku	ponorka	k1gFnSc4	ponorka
bez	bez	k7c2	bez
průduchu	průduch	k1gInSc2	průduch
na	na	k7c4	na
hladinu	hladina	k1gFnSc4	hladina
a	a	k8xC	a
se	s	k7c7	s
spalovacím	spalovací	k2eAgInSc7d1	spalovací
motorem	motor	k1gInSc7	motor
<g/>
.	.	kIx.	.
</s>
<s>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
získal	získat	k5eAaPmAgMnS	získat
Santiago	Santiago	k1gNnSc4	Santiago
Ramón	Ramón	k1gMnSc1	Ramón
y	y	k?	y
Cajal	Cajal	k1gMnSc1	Cajal
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
a	a	k8xC	a
Severo	Severa	k1gMnSc5	Severa
Ochoa	Ochoum	k1gNnPc1	Ochoum
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
humanitní	humanitní	k2eAgFnPc1d1	humanitní
vědy	věda	k1gFnPc1	věda
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
hluboké	hluboký	k2eAgInPc4d1	hluboký
kořeny	kořen	k1gInPc4	kořen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Cordobě	Cordoba	k1gFnSc6	Cordoba
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
již	již	k6eAd1	již
římský	římský	k2eAgMnSc1d1	římský
filozof	filozof	k1gMnSc1	filozof
Seneca	Seneca	k1gMnSc1	Seneca
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovými	klíčový	k2eAgFnPc7d1	klíčová
filozofickými	filozofický	k2eAgFnPc7d1	filozofická
osobnostmi	osobnost	k1gFnPc7	osobnost
Andalusie	Andalusie	k1gFnSc2	Andalusie
byli	být	k5eAaImAgMnP	být
rodák	rodák	k1gMnSc1	rodák
ze	z	k7c2	z
Zaragozy	Zaragoz	k1gInPc4	Zaragoz
Avempace	Avempace	k1gFnSc2	Avempace
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pozoruhodně	pozoruhodně	k6eAd1	pozoruhodně
pronikl	proniknout	k5eAaPmAgInS	proniknout
na	na	k7c6	na
území	území	k1gNnSc6	území
politické	politický	k2eAgFnSc2d1	politická
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
žák	žák	k1gMnSc1	žák
Ibn	Ibn	k1gMnSc1	Ibn
Tufajl	Tufajl	k1gMnSc1	Tufajl
<g/>
,	,	kIx,	,
či	či	k8xC	či
Averroes	Averroes	k1gMnSc1	Averroes
narozený	narozený	k2eAgMnSc1d1	narozený
v	v	k7c6	v
Cordóbě	Cordóba	k1gFnSc6	Cordóba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
vnesl	vnést	k5eAaPmAgInS	vnést
do	do	k7c2	do
islámu	islám	k1gInSc2	islám
aristotelismus	aristotelismus	k1gInSc1	aristotelismus
<g/>
.	.	kIx.	.
</s>
<s>
Ibn	Ibn	k?	Ibn
Hazm	Hazm	k1gInSc1	Hazm
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
komparativní	komparativní	k2eAgFnSc2d1	komparativní
religionistiky	religionistika	k1gFnSc2	religionistika
<g/>
.	.	kIx.	.
</s>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
svých	svůj	k3xOyFgFnPc2	svůj
dějin	dějiny	k1gFnPc2	dějiny
kotlem	kotel	k1gInSc7	kotel
kultur	kultura	k1gFnPc2	kultura
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
Cordóbě	Cordóba	k1gFnSc6	Cordóba
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
i	i	k9	i
židovský	židovský	k2eAgMnSc1d1	židovský
učenec	učenec	k1gMnSc1	učenec
Maimonides	Maimonides	k1gMnSc1	Maimonides
a	a	k8xC	a
španělskými	španělský	k2eAgMnPc7d1	španělský
rodáky	rodák	k1gMnPc7	rodák
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
další	další	k2eAgMnPc1d1	další
významní	významný	k2eAgMnPc1d1	významný
judaisté	judaista	k1gMnPc1	judaista
<g/>
:	:	kIx,	:
Šlomo	Šloma	k1gFnSc5	Šloma
ibn	ibn	k?	ibn
Gabirol	Gabirol	k1gInSc1	Gabirol
<g/>
,	,	kIx,	,
Nachmanides	Nachmanides	k1gMnSc1	Nachmanides
<g/>
,	,	kIx,	,
Chasdaj	Chasdaj	k1gMnSc1	Chasdaj
Kreskas	Kreskas	k1gMnSc1	Kreskas
či	či	k8xC	či
Jehuda	Jehuda	k1gMnSc1	Jehuda
ha-Levi	ha-Levit	k5eAaPmRp2nS	ha-Levit
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
filozofie	filozofie	k1gFnSc1	filozofie
nakonec	nakonec	k6eAd1	nakonec
ovšem	ovšem	k9	ovšem
převážila	převážit	k5eAaPmAgFnS	převážit
<g/>
.	.	kIx.	.
</s>
<s>
Terezie	Terezie	k1gFnSc1	Terezie
od	od	k7c2	od
Ježíše	Ježíš	k1gMnSc2	Ježíš
je	být	k5eAaImIp3nS	být
představitelkou	představitelka	k1gFnSc7	představitelka
vrcholné	vrcholný	k2eAgFnSc2d1	vrcholná
středověké	středověký	k2eAgFnSc2d1	středověká
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
mystiky	mystika	k1gFnSc2	mystika
<g/>
,	,	kIx,	,
Isidor	Isidor	k1gMnSc1	Isidor
ze	z	k7c2	z
Sevilly	Sevilla	k1gFnSc2	Sevilla
patristiky	patristika	k1gFnSc2	patristika
<g/>
,	,	kIx,	,
Juan	Juan	k1gMnSc1	Juan
Luis	Luisa	k1gFnPc2	Luisa
Vives	Vives	k1gInSc1	Vives
humanismu	humanismus	k1gInSc2	humanismus
<g/>
,	,	kIx,	,
Baltasar	Baltasar	k1gMnSc1	Baltasar
Gracián	Gracián	k1gMnSc1	Gracián
či	či	k8xC	či
Francisco	Francisco	k1gMnSc1	Francisco
Suárez	Suárez	k1gMnSc1	Suárez
jezuitské	jezuitský	k2eAgFnSc2d1	jezuitská
etiky	etika	k1gFnSc2	etika
a	a	k8xC	a
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Pilíře	pilíř	k1gInPc1	pilíř
španělské	španělský	k2eAgFnSc2d1	španělská
jazykovědy	jazykověda	k1gFnSc2	jazykověda
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
stavěl	stavět	k5eAaImAgMnS	stavět
Elio	Elio	k1gMnSc1	Elio
Antonio	Antonio	k1gMnSc1	Antonio
de	de	k?	de
Nebrija	Nebrija	k1gMnSc1	Nebrija
<g/>
,	,	kIx,	,
otcem	otec	k1gMnSc7	otec
moderní	moderní	k2eAgFnSc2d1	moderní
španělské	španělský	k2eAgFnSc2d1	španělská
filologie	filologie	k1gFnSc2	filologie
byl	být	k5eAaImAgMnS	být
Ramón	Ramón	k1gMnSc1	Ramón
Menéndez	Menéndez	k1gMnSc1	Menéndez
Pidal	Pidal	k1gMnSc1	Pidal
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20.	[number]	k4	20.
století	století	k1gNnSc6	století
celosvětový	celosvětový	k2eAgInSc4d1	celosvětový
ohlas	ohlas	k1gInSc4	ohlas
získal	získat	k5eAaPmAgMnS	získat
filozof	filozof	k1gMnSc1	filozof
José	Josá	k1gFnSc2	Josá
Ortega	Ortega	k1gFnSc1	Ortega
y	y	k?	y
Gasset	Gasseta	k1gFnPc2	Gasseta
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc4	jeho
žačka	žačka	k1gFnSc1	žačka
María	María	k1gFnSc1	María
Zambranová	Zambranová	k1gFnSc1	Zambranová
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
filozofického	filozofický	k2eAgInSc2d1	filozofický
pragmatismu	pragmatismus	k1gInSc2	pragmatismus
George	Georg	k1gMnSc2	Georg
Santayana	Santayan	k1gMnSc2	Santayan
či	či	k8xC	či
sociolog	sociolog	k1gMnSc1	sociolog
Manuel	Manuel	k1gMnSc1	Manuel
Castells	Castells	k1gInSc4	Castells
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
myslitelům	myslitel	k1gMnPc3	myslitel
například	například	k6eAd1	například
ekonom	ekonom	k1gMnSc1	ekonom
Jesús	Jesúsa	k1gFnPc2	Jesúsa
Huerta	Huert	k1gMnSc2	Huert
de	de	k?	de
Soto	Soto	k1gMnSc1	Soto
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
teoretické	teoretický	k2eAgNnSc4d1	teoretické
bohatství	bohatství	k1gNnSc4	bohatství
rakouské	rakouský	k2eAgFnSc2d1	rakouská
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
filozof	filozof	k1gMnSc1	filozof
Fernando	Fernanda	k1gFnSc5	Fernanda
Savater	Savater	k1gInSc1	Savater
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
časopis	časopis	k1gInSc1	časopis
Foreign	Foreign	k1gNnSc1	Foreign
Policy	Polica	k1gFnSc2	Polica
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
54.	[number]	k4	54.
nejvlivnějšího	vlivný	k2eAgMnSc2d3	nejvlivnější
intelektuála	intelektuál	k1gMnSc2	intelektuál
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Španělským	španělský	k2eAgInSc7d1	španělský
národním	národní	k2eAgInSc7d1	národní
sportem	sport	k1gInSc7	sport
je	být	k5eAaImIp3nS	být
fotbal	fotbal	k1gInSc1	fotbal
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
na	na	k7c6	na
klubové	klubový	k2eAgFnSc6d1	klubová
úrovni	úroveň	k1gFnSc6	úroveň
Španělé	Španěl	k1gMnPc1	Španěl
patřili	patřit	k5eAaImAgMnP	patřit
vždy	vždy	k6eAd1	vždy
ke	k	k7c3	k
světové	světový	k2eAgFnSc3d1	světová
špičce	špička	k1gFnSc3	špička
<g/>
.	.	kIx.	.
</s>
<s>
Real	Real	k1gInSc1	Real
Madrid	Madrid	k1gInSc1	Madrid
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
třináctkrát	třináctkrát	k6eAd1	třináctkrát
Ligu	liga	k1gFnSc4	liga
mistrů	mistr	k1gMnPc2	mistr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
týmem	tým	k1gInSc7	tým
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
nejprestižnější	prestižní	k2eAgFnSc6d3	nejprestižnější
evropské	evropský	k2eAgFnSc6d1	Evropská
klubové	klubový	k2eAgFnSc6d1	klubová
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
si	se	k3xPyFc3	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
připsala	připsat	k5eAaPmAgFnS	připsat
pět	pět	k4xCc4	pět
vítězství	vítězství	k1gNnPc2	vítězství
<g/>
,	,	kIx,	,
drží	držet	k5eAaImIp3nS	držet
ovšem	ovšem	k9	ovšem
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
triumfů	triumf	k1gInPc2	triumf
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
dalších	další	k2eAgFnPc6d1	další
evropských	evropský	k2eAgFnPc6d1	Evropská
soutěžích	soutěž	k1gFnPc6	soutěž
-	-	kIx~	-
Poháru	pohár	k1gInSc2	pohár
vítězů	vítěz	k1gMnPc2	vítěz
pohárů	pohár	k1gInPc2	pohár
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc6d1	Evropská
lize	liga	k1gFnSc6	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropských	evropský	k2eAgInPc6d1	evropský
pohárech	pohár	k1gInPc6	pohár
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
dařilo	dařit	k5eAaImAgNnS	dařit
i	i	k9	i
dalším	další	k2eAgInPc3d1	další
španělským	španělský	k2eAgInPc3d1	španělský
klubům	klub	k1gInPc3	klub
<g/>
:	:	kIx,	:
vítězství	vítězství	k1gNnSc3	vítězství
v	v	k7c6	v
Poháru	pohár	k1gInSc6	pohár
vítězů	vítěz	k1gMnPc2	vítěz
nebo	nebo	k8xC	nebo
Evropské	evropský	k2eAgFnSc6d1	Evropská
lize	liga	k1gFnSc6	liga
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
kontě	konto	k1gNnSc6	konto
Valencia	Valencius	k1gMnSc4	Valencius
CF	CF	kA	CF
<g/>
,	,	kIx,	,
Atlético	Atlético	k6eAd1	Atlético
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
FC	FC	kA	FC
Sevilla	Sevilla	k1gFnSc1	Sevilla
a	a	k8xC	a
Real	Real	k1gInSc1	Real
Zaragoza	Zaragoz	k1gMnSc2	Zaragoz
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
se	se	k3xPyFc4	se
dočkali	dočkat	k5eAaPmAgMnP	dočkat
i	i	k9	i
velké	velká	k1gFnPc4	velká
éry	éra	k1gFnSc2	éra
reprezentační	reprezentační	k2eAgFnPc1d1	reprezentační
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008-2012	[number]	k4	2008-2012
získala	získat	k5eAaPmAgFnS	získat
nesmírně	smírně	k6eNd1	smírně
silná	silný	k2eAgFnSc1d1	silná
generace	generace	k1gFnSc1	generace
hráčů	hráč	k1gMnPc2	hráč
dva	dva	k4xCgInPc1	dva
tituly	titul	k1gInPc1	titul
mistra	mistr	k1gMnSc2	mistr
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovými	klíčový	k2eAgMnPc7d1	klíčový
představiteli	představitel	k1gMnPc7	představitel
této	tento	k3xDgFnSc2	tento
generace	generace	k1gFnSc2	generace
byli	být	k5eAaImAgMnP	být
Iker	Iker	k1gMnSc1	Iker
Casillas	Casillas	k1gMnSc1	Casillas
<g/>
,	,	kIx,	,
Xavi	Xavi	k1gNnSc1	Xavi
<g/>
,	,	kIx,	,
Andrés	Andrés	k1gInSc1	Andrés
Iniesta	Iniesta	k1gFnSc1	Iniesta
<g/>
,	,	kIx,	,
Fernando	Fernanda	k1gFnSc5	Fernanda
Torres	Torres	k1gInSc1	Torres
<g/>
,	,	kIx,	,
Francesc	Francesc	k1gFnSc1	Francesc
Fà	Fà	k1gFnSc1	Fà
<g/>
,	,	kIx,	,
Xabi	Xabi	k1gNnSc1	Xabi
Alonso	Alonsa	k1gFnSc5	Alonsa
<g/>
,	,	kIx,	,
Sergio	Sergio	k1gMnSc1	Sergio
Ramos	Ramos	k1gMnSc1	Ramos
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Silva	Silva	k1gFnSc1	Silva
<g/>
,	,	kIx,	,
Carles	Carles	k1gInSc1	Carles
Puyol	Puyol	k1gInSc1	Puyol
a	a	k8xC	a
David	David	k1gMnSc1	David
Villa	Villa	k1gMnSc1	Villa
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
starších	starý	k2eAgFnPc2d2	starší
dob	doba	k1gFnPc2	doba
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
legendám	legenda	k1gFnPc3	legenda
brankář	brankář	k1gMnSc1	brankář
Ricardo	Ricardo	k1gNnSc4	Ricardo
Zamora	Zamor	k1gMnSc2	Zamor
<g/>
,	,	kIx,	,
držitelé	držitel	k1gMnPc1	držitel
Zlatého	zlatý	k2eAgInSc2d1	zlatý
míče	míč	k1gInSc2	míč
Alfredo	Alfredo	k1gNnSc1	Alfredo
Di	Di	k1gFnSc2	Di
Stéfano	Stéfana	k1gFnSc5	Stéfana
a	a	k8xC	a
Luis	Luisa	k1gFnPc2	Luisa
Suárez	Suárez	k1gMnSc1	Suárez
Miramontes	Miramontes	k1gMnSc1	Miramontes
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
Emilio	Emilio	k6eAd1	Emilio
Butragueñ	Butragueñ	k1gMnPc2	Butragueñ
či	či	k8xC	či
Fernando	Fernanda	k1gFnSc5	Fernanda
Hierro	Hierro	k1gNnSc4	Hierro
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
španělští	španělský	k2eAgMnPc1d1	španělský
fotbaloví	fotbalový	k2eAgMnPc1d1	fotbalový
trenéři	trenér	k1gMnPc1	trenér
mají	mít	k5eAaImIp3nP	mít
vysoký	vysoký	k2eAgInSc4d1	vysoký
kredit	kredit	k1gInSc4	kredit
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Vicente	Vicent	k1gInSc5	Vicent
del	del	k?	del
Bosque	Bosque	k1gInSc1	Bosque
<g/>
,	,	kIx,	,
Josep	Josep	k1gInSc1	Josep
Guardiola	Guardiola	k1gFnSc1	Guardiola
a	a	k8xC	a
Rafael	Rafael	k1gMnSc1	Rafael
Benítez	Benítez	k1gMnSc1	Benítez
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgMnSc1d1	populární
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
i	i	k8xC	i
basketbal	basketbal	k1gInSc1	basketbal
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
basketbale	basketbal	k1gInSc6	basketbal
španělské	španělský	k2eAgInPc1d1	španělský
kluby	klub	k1gInPc1	klub
kralují	kralovat	k5eAaImIp3nP	kralovat
nejprestižnější	prestižní	k2eAgFnSc3d3	nejprestižnější
klubové	klubový	k2eAgFnSc3d1	klubová
soutěži	soutěž	k1gFnSc3	soutěž
Eurolize	Eurolize	k1gFnSc2	Eurolize
-	-	kIx~	-
Real	Real	k1gInSc1	Real
Madrid	Madrid	k1gInSc1	Madrid
ji	on	k3xPp3gFnSc4	on
už	už	k6eAd1	už
desetkrát	desetkrát	k6eAd1	desetkrát
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
a	a	k8xC	a
drží	držet	k5eAaImIp3nS	držet
tak	tak	k6eAd1	tak
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Věčný	věčný	k2eAgMnSc1d1	věčný
rival	rival	k1gMnSc1	rival
Barcelona	Barcelona	k1gFnSc1	Barcelona
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
tuto	tento	k3xDgFnSc4	tento
soutěž	soutěž	k1gFnSc4	soutěž
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
povedlo	povést	k5eAaPmAgNnS	povést
i	i	k8xC	i
provinčnímu	provinční	k2eAgInSc3d1	provinční
klubu	klub	k1gInSc3	klub
Joventut	Joventut	k1gInSc4	Joventut
Badalona	Badalon	k1gMnSc2	Badalon
<g/>
.	.	kIx.	.
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
mužská	mužský	k2eAgFnSc1d1	mužská
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
reprezentace	reprezentace	k1gFnSc1	reprezentace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
na	na	k7c4	na
titul	titul	k1gInSc4	titul
mistrů	mistr	k1gMnPc2	mistr
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
třikrát	třikrát	k6eAd1	třikrát
se	se	k3xPyFc4	se
Španělé	Španěl	k1gMnPc1	Španěl
stali	stát	k5eAaPmAgMnP	stát
šampiony	šampion	k1gMnPc4	šampion
evropskými	evropský	k2eAgMnPc7d1	evropský
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ženy	žena	k1gFnPc1	žena
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
španělští	španělský	k2eAgMnPc1d1	španělský
basketbalisté	basketbalista	k1gMnPc1	basketbalista
se	se	k3xPyFc4	se
již	již	k6eAd1	již
prosadili	prosadit	k5eAaPmAgMnP	prosadit
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
NBA	NBA	kA	NBA
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
její	její	k3xOp3gMnSc1	její
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
vítěz	vítěz	k1gMnSc1	vítěz
Pau	Pau	k1gFnSc2	Pau
Gasol	Gasol	k1gInSc1	Gasol
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
basketbalistou	basketbalista	k1gMnSc7	basketbalista
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Marc	Marc	k1gFnSc4	Marc
Gasol	Gasola	k1gFnPc2	Gasola
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tuto	tento	k3xDgFnSc4	tento
anketu	anketa	k1gFnSc4	anketa
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Fernando	Fernando	k6eAd1	Fernando
Martín	Martín	k1gInSc1	Martín
<g/>
,	,	kIx,	,
Emiliano	Emiliana	k1gFnSc5	Emiliana
Rodríguez	Rodríguez	k1gMnSc1	Rodríguez
a	a	k8xC	a
Juan	Juan	k1gMnSc1	Juan
Antonio	Antonio	k1gMnSc1	Antonio
San	San	k1gMnSc1	San
Epifanio	Epifanio	k1gMnSc1	Epifanio
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gMnPc7	člen
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
basketbalové	basketbalový	k2eAgFnSc2d1	basketbalová
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
známým	známý	k2eAgMnPc3d1	známý
hráčům	hráč	k1gMnPc3	hráč
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Ricky	Ricka	k1gFnPc4	Ricka
Rubio	Rubio	k6eAd1	Rubio
<g/>
,	,	kIx,	,
Jorge	Jorge	k1gFnSc1	Jorge
Garbajosa	Garbajosa	k1gFnSc1	Garbajosa
nebo	nebo	k8xC	nebo
Juan	Juan	k1gMnSc1	Juan
Carlos	Carlos	k1gMnSc1	Carlos
Navarro	Navarra	k1gFnSc5	Navarra
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
žen	žena	k1gFnPc2	žena
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
hvězdám	hvězda	k1gFnPc3	hvězda
Alba	album	k1gNnSc2	album
Torrensová	Torrensový	k2eAgFnSc1d1	Torrensový
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
vyhlášená	vyhlášený	k2eAgFnSc1d1	vyhlášená
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
basketbalistkou	basketbalistka	k1gFnSc7	basketbalistka
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
házenkářská	házenkářský	k2eAgFnSc1d1	házenkářská
reprezentace	reprezentace	k1gFnSc1	reprezentace
mužů	muž	k1gMnPc2	muž
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
dva	dva	k4xCgInPc4	dva
světové	světový	k2eAgInPc4d1	světový
šampionáty	šampionát	k1gInPc4	šampionát
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rafael	Rafael	k1gMnSc1	Rafael
Guijosa	Guijosa	k1gFnSc1	Guijosa
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
házenkářem	házenkář	k1gMnSc7	házenkář
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejlepším	dobrý	k2eAgMnPc3d3	nejlepší
tenistům	tenista	k1gMnPc3	tenista
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
patří	patřit	k5eAaImIp3nS	patřit
Rafael	Rafael	k1gMnSc1	Rafael
Nadal	nadat	k5eAaPmAgInS	nadat
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
španělských	španělský	k2eAgInPc2d1	španělský
tenisových	tenisový	k2eAgInPc2d1	tenisový
úspěchů	úspěch	k1gInPc2	úspěch
není	být	k5eNaImIp3nS	být
zdaleka	zdaleka	k6eAd1	zdaleka
osamocen	osamocen	k2eAgInSc1d1	osamocen
<g/>
.	.	kIx.	.
</s>
<s>
Arantxa	Arantx	k2eAgFnSc1d1	Arantxa
Sánchezová	Sánchezová	k1gFnSc1	Sánchezová
Vicariová	Vicariový	k2eAgFnSc1d1	Vicariová
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
třikrát	třikrát	k6eAd1	třikrát
Paříž	Paříž	k1gFnSc1	Paříž
a	a	k8xC	a
jednou	jednou	k6eAd1	jednou
US	US	kA	US
Open	Open	k1gNnSc1	Open
<g/>
,	,	kIx,	,
Garbiñ	Garbiñ	k1gFnSc1	Garbiñ
Muguruzaová	Muguruzaová	k1gFnSc1	Muguruzaová
Paříž	Paříž	k1gFnSc1	Paříž
a	a	k8xC	a
Wimbledon	Wimbledon	k1gInSc1	Wimbledon
<g/>
,	,	kIx,	,
Conchita	Conchit	k2eAgFnSc1d1	Conchita
Martínezová	Martínezová	k1gFnSc1	Martínezová
Wimbledon	Wimbledon	k1gInSc1	Wimbledon
<g/>
,	,	kIx,	,
Manuel	Manuel	k1gMnSc1	Manuel
Orantes	Orantes	k1gMnSc1	Orantes
US	US	kA	US
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
Sergi	Serg	k1gMnPc1	Serg
Bruguera	Bruguero	k1gNnSc2	Bruguero
<g/>
,	,	kIx,	,
Andrés	Andrésa	k1gFnPc2	Andrésa
Gimeno	Gimen	k2eAgNnSc1d1	Gimeno
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Costa	Costa	k1gMnSc1	Costa
<g/>
,	,	kIx,	,
Carlos	Carlos	k1gMnSc1	Carlos
Moyá	Moyá	k1gFnSc1	Moyá
a	a	k8xC	a
Juan	Juan	k1gMnSc1	Juan
Carlos	Carlos	k1gMnSc1	Carlos
Ferrero	Ferrero	k1gNnSc4	Ferrero
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
na	na	k7c4	na
Roland	Roland	k1gInSc4	Roland
Garros	Garrosa	k1gFnPc2	Garrosa
a	a	k8xC	a
dosvědčili	dosvědčit	k5eAaPmAgMnP	dosvědčit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
španělská	španělský	k2eAgFnSc1d1	španělská
tenisová	tenisový	k2eAgFnSc1d1	tenisová
škola	škola	k1gFnSc1	škola
umí	umět	k5eAaImIp3nS	umět
vychovávat	vychovávat	k5eAaImF	vychovávat
vynikající	vynikající	k2eAgMnPc4d1	vynikající
antukové	antukový	k2eAgMnPc4d1	antukový
specialisty	specialista	k1gMnPc4	specialista
<g/>
.	.	kIx.	.
</s>
<s>
Manuel	Manuel	k1gMnSc1	Manuel
Santana	Santan	k1gMnSc4	Santan
byl	být	k5eAaImAgInS	být
tenisovou	tenisový	k2eAgFnSc7d1	tenisová
hvězdou	hvězda	k1gFnSc7	hvězda
před	před	k7c7	před
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
open	opena	k1gFnPc2	opena
érou	éra	k1gFnSc7	éra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
také	také	k9	také
v	v	k7c6	v
cyklistice	cyklistika	k1gFnSc6	cyklistika
(	(	kIx(	(
<g/>
Miguel	Miguel	k1gMnSc1	Miguel
Indurain	Indurain	k1gMnSc1	Indurain
<g/>
,	,	kIx,	,
Alberto	Alberta	k1gFnSc5	Alberta
Contador	Contador	k1gInSc1	Contador
<g/>
,	,	kIx,	,
Roberto	Roberta	k1gFnSc5	Roberta
Heras	Herasa	k1gFnPc2	Herasa
<g/>
)	)	kIx)	)
a	a	k8xC	a
motorovém	motorový	k2eAgInSc6d1	motorový
sportu	sport	k1gInSc6	sport
(	(	kIx(	(
<g/>
Fernando	Fernanda	k1gFnSc5	Fernanda
Alonso	Alonsa	k1gFnSc5	Alonsa
<g/>
,	,	kIx,	,
Ángel	Ángel	k1gMnSc1	Ángel
Nieto	Nieto	k1gNnSc1	Nieto
<g/>
,	,	kIx,	,
Marc	Marc	k1gFnSc1	Marc
Márquez	Márquez	k1gInSc1	Márquez
<g/>
,	,	kIx,	,
Jorge	Jorge	k1gInSc1	Jorge
Lorenzo	Lorenza	k1gFnSc5	Lorenza
<g/>
,	,	kIx,	,
Jorge	Jorge	k1gFnPc3	Jorge
Martínez	Martíneza	k1gFnPc2	Martíneza
<g/>
,	,	kIx,	,
Dani	daň	k1gFnSc6	daň
Pedrosa	Pedrosa	k1gFnSc1	Pedrosa
<g/>
,	,	kIx,	,
Carlos	Carlos	k1gMnSc1	Carlos
Sainz	Sainz	k1gMnSc1	Sainz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Juan	Juan	k1gMnSc1	Juan
Antonio	Antonio	k1gMnSc1	Antonio
Samaranch	Samaranch	k1gMnSc1	Samaranch
stál	stát	k5eAaImAgMnS	stát
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
jednou	jednou	k6eAd1	jednou
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
pořádali	pořádat	k5eAaImAgMnP	pořádat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
sportem	sport	k1gInSc7	sport
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
je	být	k5eAaImIp3nS	být
jachting	jachting	k1gInSc1	jachting
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
v	v	k7c6	v
kolektivních	kolektivní	k2eAgInPc6d1	kolektivní
sportech	sport	k1gInPc6	sport
-	-	kIx~	-
zlato	zlato	k1gNnSc4	zlato
získali	získat	k5eAaPmAgMnP	získat
ženy	žena	k1gFnSc2	žena
v	v	k7c6	v
pozemním	pozemní	k2eAgInSc6d1	pozemní
hokeji	hokej	k1gInSc6	hokej
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
muži	muž	k1gMnPc1	muž
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
a	a	k8xC	a
vodním	vodní	k2eAgNnSc6d1	vodní
pólu	pólo	k1gNnSc6	pólo
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Triumf	triumf	k1gInSc4	triumf
si	se	k3xPyFc3	se
připsali	připsat	k5eAaPmAgMnP	připsat
i	i	k9	i
tři	tři	k4xCgMnPc1	tři
atleti	atlet	k1gMnPc1	atlet
<g/>
:	:	kIx,	:
chodec	chodec	k1gMnSc1	chodec
Daniel	Daniel	k1gMnSc1	Daniel
Plaza	plaz	k1gMnSc2	plaz
<g/>
,	,	kIx,	,
mílař	mílař	k1gMnSc1	mílař
Fermín	Fermín	k1gMnSc1	Fermín
Cacho	Cac	k1gMnSc2	Cac
a	a	k8xC	a
výškařka	výškařka	k1gFnSc1	výškařka
Ruth	Ruth	k1gFnSc1	Ruth
Beitiaová	Beitiaová	k1gFnSc1	Beitiaová
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgInPc4d1	zimní
sporty	sport	k1gInPc4	sport
nejsou	být	k5eNaImIp3nP	být
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
obvyklé	obvyklý	k2eAgInPc1d1	obvyklý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
jednu	jeden	k4xCgFnSc4	jeden
zlatou	zlatý	k2eAgFnSc4d1	zlatá
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
Španělé	Španěl	k1gMnPc1	Španěl
mají	mít	k5eAaImIp3nP	mít
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
ji	on	k3xPp3gFnSc4	on
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
ve	v	k7c6	v
slalomu	slalom	k1gInSc6	slalom
sjezdař	sjezdař	k1gMnSc1	sjezdař
Francisco	Francisco	k1gMnSc1	Francisco
Fernández	Fernández	k1gMnSc1	Fernández
Ochoa	Ochoa	k1gMnSc1	Ochoa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
CHALUPA	Chalupa	k1gMnSc1	Chalupa
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Španělska	Španělsko	k1gNnSc2	Španělsko
v	v	k7c6	v
datech	datum	k1gNnPc6	datum
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2011.	[number]	k4	2011.
551	[number]	k4	551
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-7277-482-1	[number]	k4	978-80-7277-482-1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHALUPA	Chalupa	k1gMnSc1	Chalupa
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
1.	[number]	k4	1.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2017.	[number]	k4	2017.
752	[number]	k4	752
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-7422-252-3	[number]	k4	978-80-7422-252-3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHALUPA	Chalupa	k1gMnSc1	Chalupa
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
.	.	kIx.	.
2.	[number]	k4	2.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2010.	[number]	k4	2010.
219	[number]	k4	219
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-7277-478-4	[number]	k4	978-80-7277-478-4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
UBIETO	UBIETO	kA	UBIETO
ARTETA	ARTETA	kA	ARTETA
<g/>
,	,	kIx,	,
Antonio	Antonio	k1gMnSc1	Antonio
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
3.	[number]	k4	3.
dopl	doplum	k1gNnPc2	doplum
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007.	[number]	k4	2007.
915	[number]	k4	915
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-7106-836-5	[number]	k4	978-80-7106-836-5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Španělska	Španělsko	k1gNnSc2	Španělsko
</s>
</p>
<p>
<s>
Hispanoamerika	Hispanoamerika	k1gFnSc1	Hispanoamerika
</s>
</p>
<p>
<s>
Pohřebiště	pohřebiště	k1gNnSc1	pohřebiště
španělských	španělský	k2eAgMnPc2d1	španělský
panovníků	panovník	k1gMnPc2	panovník
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
měst	město	k1gNnPc2	město
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
španělských	španělský	k2eAgMnPc2d1	španělský
vládců	vládce	k1gMnPc2	vládce
</s>
</p>
<p>
<s>
Španělské	španělský	k2eAgNnSc1d1	španělské
impérium	impérium	k1gNnSc1	impérium
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Španělsko	Španělsko	k1gNnSc4	Španělsko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Španělsko	Španělsko	k1gNnSc4	Španělsko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
</p>
<p>
<s>
Spain	Spain	k1gInSc1	Spain
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-22	[number]	k4	2011-08-22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spain	Spain	k1gInSc1	Spain
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-22	[number]	k4	2011-08-22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
European	European	k1gInSc1	European
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Spain	Spain	k1gInSc1	Spain
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.S.	U.S.	k?	U.S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
Spain	Spain	k1gInSc1	Spain
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-22	[number]	k4	2011-08-22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Spain	Spain	k1gInSc1	Spain
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-22	[number]	k4	2011-08-22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Španělsko	Španělsko	k1gNnSc1	Španělsko
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo.cz	Businessinfo.cz	k1gMnSc1	Businessinfo.cz
<g/>
,	,	kIx,	,
2011-04-15	[number]	k4	2011-04-15
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-22	[number]	k4	2011-08-22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012-01-11	[number]	k4	2012-01-11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CARR	CARR	kA	CARR
<g/>
,	,	kIx,	,
Sir	sir	k1gMnSc1	sir
Raymond	Raymond	k1gMnSc1	Raymond
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Spain	Spain	k1gInSc1	Spain
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-22	[number]	k4	2011-08-22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
