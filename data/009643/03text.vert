<p>
<s>
PET	PET	kA	PET
<g/>
/	/	kIx~	/
<g/>
CT	CT	kA	CT
je	být	k5eAaImIp3nS	být
diagnostická	diagnostický	k2eAgFnSc1d1	diagnostická
zobrazovací	zobrazovací	k2eAgFnSc1d1	zobrazovací
metoda	metoda	k1gFnSc1	metoda
spojující	spojující	k2eAgFnSc1d1	spojující
vyšetření	vyšetření	k1gNnSc4	vyšetření
počítačovou	počítačový	k2eAgFnSc7d1	počítačová
tomografií	tomografie	k1gFnSc7	tomografie
(	(	kIx(	(
<g/>
CT	CT	kA	CT
<g/>
)	)	kIx)	)
a	a	k8xC	a
pozitron	pozitron	k1gInSc1	pozitron
emisní	emisní	k2eAgInSc1d1	emisní
tomografií	tomografie	k1gFnSc7	tomografie
(	(	kIx(	(
<g/>
PET	PET	kA	PET
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Získané	získaný	k2eAgInPc1d1	získaný
obrazy	obraz	k1gInPc1	obraz
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jak	jak	k6eAd1	jak
poměrně	poměrně	k6eAd1	poměrně
podrobnou	podrobný	k2eAgFnSc4d1	podrobná
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
anatomické	anatomický	k2eAgFnSc6d1	anatomická
stavbě	stavba	k1gFnSc6	stavba
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
metabolické	metabolický	k2eAgFnSc6d1	metabolická
aktivitě	aktivita	k1gFnSc6	aktivita
tkání	tkáň	k1gFnPc2	tkáň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Radiologie	radiologie	k1gFnSc1	radiologie
</s>
</p>
<p>
<s>
Nukleární	nukleární	k2eAgFnSc1d1	nukleární
medicína	medicína	k1gFnSc1	medicína
</s>
</p>
<p>
<s>
Pozitronová	pozitronový	k2eAgFnSc1d1	pozitronová
emisní	emisní	k2eAgFnSc1d1	emisní
tomografie	tomografie	k1gFnSc1	tomografie
</s>
</p>
<p>
<s>
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
tomografie	tomografie	k1gFnSc1	tomografie
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
VOTRUBOVÁ	Votrubová	k1gFnSc1	Votrubová
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
<g/>
;	;	kIx,	;
BĚLOHLÁVEK	Bělohlávek	k1gMnSc1	Bělohlávek
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
<g/>
.	.	kIx.	.
</s>
<s>
PET	PET	kA	PET
<g/>
/	/	kIx~	/
<g/>
CT	CT	kA	CT
v	v	k7c6	v
klinické	klinický	k2eAgFnSc6d1	klinická
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Interní	interní	k2eAgInSc1d1	interní
Med	med	k1gInSc1	med
<g/>
..	..	k?	..
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
,	,	kIx,	,
s.	s.	k?	s.
303	[number]	k4	303
<g/>
-	-	kIx~	-
<g/>
305	[number]	k4	305
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1803	[number]	k4	1803
<g/>
-	-	kIx~	-
<g/>
5256	[number]	k4	5256
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
informací	informace	k1gFnPc2	informace
o	o	k7c4	o
PET	PET	kA	PET
<g/>
/	/	kIx~	/
<g/>
CT	CT	kA	CT
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
pacienty	pacient	k1gMnPc4	pacient
naleznete	naleznout	k5eAaPmIp2nP	naleznout
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Kliniky	klinika	k1gFnSc2	klinika
zobrazovacích	zobrazovací	k2eAgFnPc2d1	zobrazovací
metod	metoda	k1gFnPc2	metoda
Fakultní	fakultní	k2eAgFnSc2d1	fakultní
nemocnice	nemocnice	k1gFnSc2	nemocnice
Plzeň	Plzeň	k1gFnSc1	Plzeň
</s>
</p>
<p>
<s>
Popis	popis	k1gInSc1	popis
CT	CT	kA	CT
<g/>
/	/	kIx~	/
<g/>
PET	PET	kA	PET
pro	pro	k7c4	pro
pacienty	pacient	k1gMnPc4	pacient
Masarykova	Masarykův	k2eAgInSc2d1	Masarykův
onkologického	onkologický	k2eAgInSc2d1	onkologický
ústavu	ústav	k1gInSc2	ústav
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
