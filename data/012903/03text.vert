<p>
<s>
Lučavka	lučavka	k1gFnSc1	lučavka
královská	královský	k2eAgFnSc1d1	královská
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
aqua	aqu	k2eAgFnSc1d1	aqua
regia	regia	k1gFnSc1	regia
neboli	neboli	k8xC	neboli
královská	královský	k2eAgFnSc1d1	královská
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
také	také	k9	také
acidum	acidum	k1gInSc1	acidum
chloronitrosum	chloronitrosum	k1gInSc1	chloronitrosum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dýmavá	dýmavý	k2eAgFnSc1d1	dýmavá
žlutohnědá	žlutohnědý	k2eAgFnSc1d1	žlutohnědá
kapalina	kapalina	k1gFnSc1	kapalina
používaná	používaný	k2eAgFnSc1d1	používaná
pro	pro	k7c4	pro
rozpouštění	rozpouštění	k1gNnSc4	rozpouštění
obtížně	obtížně	k6eAd1	obtížně
rozpustných	rozpustný	k2eAgInPc2d1	rozpustný
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vzácných	vzácný	k2eAgInPc2d1	vzácný
(	(	kIx(	(
<g/>
královských	královský	k2eAgInPc2d1	královský
<g/>
)	)	kIx)	)
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
směs	směs	k1gFnSc4	směs
koncentrované	koncentrovaný	k2eAgFnSc2d1	koncentrovaná
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgFnSc2d1	dusičná
(	(	kIx(	(
<g/>
HNO	HNO	kA	HNO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
kyseliny	kyselina	k1gFnSc2	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
(	(	kIx(	(
<g/>
HCl	HCl	k1gFnSc2	HCl
<g/>
)	)	kIx)	)
v	v	k7c6	v
objemovém	objemový	k2eAgInSc6d1	objemový
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Nevydrží	vydržet	k5eNaPmIp3nS	vydržet
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
ji	on	k3xPp3gFnSc4	on
namíchat	namíchat	k5eAaBmF	namíchat
bezprostředně	bezprostředně	k6eAd1	bezprostředně
před	před	k7c7	před
použitím	použití	k1gNnSc7	použití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lučavka	lučavka	k1gFnSc1	lučavka
Leffortova	Leffortův	k2eAgFnSc1d1	Leffortův
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
obrácená	obrácený	k2eAgFnSc1d1	obrácená
lučavka	lučavka	k1gFnSc1	lučavka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
směs	směs	k1gFnSc1	směs
stejných	stejný	k2eAgFnPc2d1	stejná
kyselin	kyselina	k1gFnPc2	kyselina
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
poměru	poměr	k1gInSc6	poměr
–	–	k?	–
3	[number]	k4	3
díly	díl	k1gInPc4	díl
HNO3	HNO3	k1gFnSc1	HNO3
na	na	k7c4	na
1	[number]	k4	1
díl	díl	k1gInSc4	díl
HCl	HCl	k1gFnSc2	HCl
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
slovo	slovo	k1gNnSc1	slovo
lučavka	lučavka	k1gFnSc1	lučavka
(	(	kIx(	(
<g/>
aqua	aqu	k2eAgFnSc1d1	aqua
fortis	fortis	k1gFnSc1	fortis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
kyselinu	kyselina	k1gFnSc4	kyselina
dusičnou	dusičný	k2eAgFnSc4d1	dusičná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Princip	princip	k1gInSc1	princip
rozpouštění	rozpouštění	k1gNnSc1	rozpouštění
==	==	k?	==
</s>
</p>
<p>
<s>
Lučavka	lučavka	k1gFnSc1	lučavka
královská	královský	k2eAgFnSc1d1	královská
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
velmi	velmi	k6eAd1	velmi
odolné	odolný	k2eAgInPc4d1	odolný
kovové	kovový	k2eAgInPc4d1	kovový
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
zlato	zlato	k1gNnSc1	zlato
a	a	k8xC	a
platina	platina	k1gFnSc1	platina
<g/>
.	.	kIx.	.
</s>
<s>
Lučavce	lučavka	k1gFnSc3	lučavka
královské	královský	k2eAgFnSc2d1	královská
odolávají	odolávat	k5eAaImIp3nP	odolávat
titan	titan	k1gInSc1	titan
<g/>
,	,	kIx,	,
iridium	iridium	k1gNnSc1	iridium
<g/>
,	,	kIx,	,
ruthenium	ruthenium	k1gNnSc1	ruthenium
<g/>
,	,	kIx,	,
rhenium	rhenium	k1gNnSc1	rhenium
<g/>
,	,	kIx,	,
tantal	tantal	k1gInSc1	tantal
<g/>
,	,	kIx,	,
niob	niob	k1gInSc1	niob
<g/>
,	,	kIx,	,
hafnium	hafnium	k1gNnSc1	hafnium
<g/>
,	,	kIx,	,
osmium	osmium	k1gNnSc1	osmium
a	a	k8xC	a
rhodium	rhodium	k1gNnSc1	rhodium
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
kyselina	kyselina	k1gFnSc1	kyselina
dusičná	dusičný	k2eAgFnSc1d1	dusičná
ani	ani	k8xC	ani
chlorovodíková	chlorovodíkový	k2eAgFnSc1d1	chlorovodíková
se	s	k7c7	s
zlatem	zlato	k1gNnSc7	zlato
a	a	k8xC	a
platinou	platina	k1gFnSc7	platina
prakticky	prakticky	k6eAd1	prakticky
nereagují	reagovat	k5eNaBmIp3nP	reagovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
dusičná	dusičný	k2eAgFnSc1d1	dusičná
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgFnPc4d1	silná
oxidační	oxidační	k2eAgFnPc4d1	oxidační
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
způsobí	způsobit	k5eAaPmIp3nS	způsobit
rozpuštění	rozpuštění	k1gNnSc1	rozpuštění
nepatrného	patrný	k2eNgNnSc2d1	nepatrné
množství	množství	k1gNnSc2	množství
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Au	au	k0	au
+	+	kIx~	+
3NO3-	[number]	k4	3NO3-
+	+	kIx~	+
6	[number]	k4	6
H	H	kA	H
<g/>
+	+	kIx~	+
→	→	k?	→
Au	au	k0	au
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
+	+	kIx~	+
3	[number]	k4	3
NO2	NO2	k1gFnPc2	NO2
+	+	kIx~	+
3	[number]	k4	3
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
OChloridové	OChloridový	k2eAgInPc1d1	OChloridový
ionty	ion	k1gInPc1	ion
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
s	s	k7c7	s
kovovými	kovový	k2eAgInPc7d1	kovový
ionty	ion	k1gInPc7	ion
z	z	k7c2	z
roztoku	roztok	k1gInSc2	roztok
velmi	velmi	k6eAd1	velmi
stabilní	stabilní	k2eAgInPc4d1	stabilní
komplexní	komplexní	k2eAgInPc4d1	komplexní
ionty	ion	k1gInPc4	ion
[	[	kIx(	[
<g/>
AuCl	AuCl	k1gInSc1	AuCl
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
-	-	kIx~	-
či	či	k8xC	či
[	[	kIx(	[
<g/>
PtCl	PtCl	k1gInSc1	PtCl
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Au	au	k0	au
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
+	+	kIx~	+
4	[number]	k4	4
Cl-	Cl-	k1gFnSc2	Cl-
→	→	k?	→
[	[	kIx(	[
<g/>
AuCl	AuCl	k1gInSc1	AuCl
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
-Tím	-Tím	k6eAd1	-Tím
se	se	k3xPyFc4	se
koncentrace	koncentrace	k1gFnSc1	koncentrace
kovových	kovový	k2eAgInPc2d1	kovový
iontů	ion	k1gInPc2	ion
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
sníží	snížit	k5eAaPmIp3nS	snížit
a	a	k8xC	a
rozpouštění	rozpouštění	k1gNnSc1	rozpouštění
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zlato	zlato	k1gNnSc1	zlato
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
zpět	zpět	k6eAd1	zpět
vyloučit	vyloučit	k5eAaPmF	vyloučit
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
se	s	k7c7	s
zlatem	zlato	k1gNnSc7	zlato
amalgám	amalgáma	k1gFnPc2	amalgáma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Lučavku	lučavka	k1gFnSc4	lučavka
královskou	královský	k2eAgFnSc4d1	královská
popsal	popsat	k5eAaPmAgMnS	popsat
poprvé	poprvé	k6eAd1	poprvé
Pseudo-Geber	Pseudo-Geber	k1gMnSc1	Pseudo-Geber
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
během	během	k7c2	během
německé	německý	k2eAgFnSc2d1	německá
invaze	invaze	k1gFnSc2	invaze
do	do	k7c2	do
Dánska	Dánsko	k1gNnSc2	Dánsko
použil	použít	k5eAaPmAgInS	použít
lučavku	lučavka	k1gFnSc4	lučavka
královskou	královský	k2eAgFnSc4d1	královská
nebývalým	nebývalý	k2eAgInSc7d1	nebývalý
způsobem	způsob	k1gInSc7	způsob
maďarský	maďarský	k2eAgMnSc1d1	maďarský
chemik	chemik	k1gMnSc1	chemik
George	Georg	k1gMnSc2	Georg
de	de	k?	de
Hevesy	Hevesa	k1gFnSc2	Hevesa
<g/>
.	.	kIx.	.
</s>
<s>
Rozpustil	rozpustit	k5eAaPmAgMnS	rozpustit
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
dvě	dva	k4xCgFnPc1	dva
zlaté	zlatý	k2eAgFnPc1d1	zlatá
medaile	medaile	k1gFnPc1	medaile
Nobelových	Nobelových	k2eAgFnPc2d1	Nobelových
cen	cena	k1gFnPc2	cena
pro	pro	k7c4	pro
Maxe	Max	k1gMnPc4	Max
von	von	k1gInSc1	von
Laue	Lau	k1gFnPc1	Lau
a	a	k8xC	a
Jamese	Jamese	k1gFnPc1	Jamese
Francka	Francek	k1gMnSc2	Francek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnSc4	on
nacisté	nacista	k1gMnPc1	nacista
neukradli	ukradnout	k5eNaPmAgMnP	ukradnout
<g/>
.	.	kIx.	.
</s>
<s>
Roztok	roztok	k1gInSc1	roztok
položil	položit	k5eAaPmAgInS	položit
na	na	k7c4	na
polici	police	k1gFnSc4	police
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
laboratoři	laboratoř	k1gFnSc6	laboratoř
v	v	k7c6	v
Institutu	institut	k1gInSc6	institut
Nielse	Niels	k1gMnSc2	Niels
Bohra	Bohr	k1gMnSc2	Bohr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
,	,	kIx,	,
našel	najít	k5eAaPmAgInS	najít
roztok	roztok	k1gInSc1	roztok
neporušený	porušený	k2eNgInSc1d1	neporušený
a	a	k8xC	a
vysrážel	vysrážet	k5eAaPmAgInS	vysrážet
z	z	k7c2	z
něj	on	k3xPp3gMnSc4	on
zlato	zlato	k1gNnSc1	zlato
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
společnost	společnost	k1gFnSc1	společnost
pak	pak	k6eAd1	pak
znovu	znovu	k6eAd1	znovu
odlila	odlít	k5eAaPmAgFnS	odlít
medaile	medaile	k1gFnSc1	medaile
z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
