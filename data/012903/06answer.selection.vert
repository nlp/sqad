<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
směs	směs	k1gFnSc4	směs
koncentrované	koncentrovaný	k2eAgFnSc2d1	koncentrovaná
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgFnSc2d1	dusičná
(	(	kIx(	(
<g/>
HNO	HNO	kA	HNO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
kyseliny	kyselina	k1gFnSc2	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
(	(	kIx(	(
<g/>
HCl	HCl	k1gFnSc2	HCl
<g/>
)	)	kIx)	)
v	v	k7c6	v
objemovém	objemový	k2eAgInSc6d1	objemový
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
