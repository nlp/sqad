<s>
Jaroslav	Jaroslav	k1gMnSc1
Kekely	Kekela	k1gFnSc2
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
KekelyOsobní	KekelyOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Celé	celý	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Kekely	Kekela	k1gFnSc2
Datum	datum	k1gNnSc4
narození	narození	k1gNnSc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1957	#num#	k4
(	(	kIx(
<g/>
63	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Kysucký	Kysucký	k2eAgInSc1d1
Lieskovec	Lieskovec	k1gInSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
</s>
<s>
Konec	konec	k1gInSc1
hráčské	hráčský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
Číslo	číslo	k1gNnSc1
dresu	dres	k1gInSc2
</s>
<s>
10	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
Pozice	pozice	k1gFnSc1
</s>
<s>
útočník	útočník	k1gMnSc1
Profesionální	profesionální	k2eAgFnSc2d1
kluby	klub	k1gInPc1
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1982-19831983-1985TŽ	1982-19831983-1985TŽ	k4
TřinecZVL	TřinecZVL	k1gFnSc1
Žilina	Žilina	k1gFnSc1
<g/>
28	#num#	k4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
30	#num#	k4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
→	→	k?
Šipka	šipka	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
hostování	hostování	k1gNnSc4
hráče	hráč	k1gMnSc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Kekely	Kekela	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
26	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1957	#num#	k4
<g/>
,	,	kIx,
Kysucký	Kysucký	k2eAgInSc1d1
Lieskovec	Lieskovec	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
československý	československý	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
<g/>
,	,	kIx,
útočník	útočník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
V	v	k7c6
československé	československý	k2eAgFnSc6d1
lize	liga	k1gFnSc6
hrál	hrát	k5eAaImAgMnS
za	za	k7c2
ZVL	zvl	kA
Žilina	Žilina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
československé	československý	k2eAgFnSc6d1
lize	liga	k1gFnSc6
nastoupil	nastoupit	k5eAaPmAgMnS
ve	v	k7c6
30	#num#	k4
utkáních	utkání	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
hrál	hrát	k5eAaImAgInS
i	i	k9
za	za	k7c4
TŽ	TŽ	kA
Třinec	Třinec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Ligová	ligový	k2eAgFnSc1d1
bilance	bilance	k1gFnSc1
</s>
<s>
Ročník	ročník	k1gInSc1
</s>
<s>
Zápasy	zápas	k1gInPc1
</s>
<s>
Góly	gól	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
TŽ	TŽ	kA
Třinec	Třinec	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
ZVL	zvl	kA
Žilina	Žilina	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
ZVL	zvl	kA
Žilina	Žilina	k1gFnSc1
</s>
<s>
CELKEM	celkem	k6eAd1
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
30	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
CS	CS	kA
Fotbal	fotbal	k1gInSc1
</s>
<s>
E-Kysuce	E-Kysuce	k1gFnSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Príjemné	Príjemný	k2eAgFnPc1d1
stretnutie	stretnutie	k1gFnPc1
po	po	k7c4
rokoch	rokoch	k1gInSc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
