<s>
Vápenný	vápenný	k2eAgInSc1d1
potok	potok	k1gInSc1
(	(	kIx(
<g/>
přítok	přítok	k1gInSc1
Krížneho	Krížne	k1gMnSc4
potoka	potok	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Vápenný	vápenný	k2eAgInSc1d1
potok	potok	k1gInSc1
(	(	kIx(
<g/>
přítok	přítok	k1gInSc1
Krížneho	Krížne	k1gMnSc4
potoka	potok	k1gInSc2
<g/>
)	)	kIx)
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
1,2	1,2	k4
km	km	kA
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Ústí	ústí	k1gNnSc2
</s>
<s>
Krížny	Krížen	k2eAgInPc1d1
potok	potok	k1gInSc1
Protéká	protékat	k5eAaImIp3nS
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
(	(	kIx(
<g/>
Prešovský	prešovský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vápenný	vápenný	k2eAgInSc1d1
potok	potok	k1gInSc1
(	(	kIx(
<g/>
starší	starý	k2eAgInSc1d2
německý	německý	k2eAgInSc1d1
název	název	k1gInSc1
Kalkgrund	Kalkgrund	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
potok	potok	k1gInSc4
5	#num#	k4
<g/>
.	.	kIx.
řádu	řád	k1gInSc2
na	na	k7c6
Spiši	Spiš	k1gFnSc6
<g/>
,	,	kIx,
zčásti	zčásti	k6eAd1
v	v	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
města	město	k1gNnSc2
Podolínec	Podolínec	k1gInSc1
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Stará	starý	k2eAgFnSc1d1
Ľubovňa	Ľubovňa	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
zčásti	zčásti	k6eAd1
na	na	k7c6
území	území	k1gNnSc6
obce	obec	k1gFnSc2
Toporec	toporec	k1gInSc1
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Kežmarok	Kežmarok	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
pravostranný	pravostranný	k2eAgInSc1d1
přítok	přítok	k1gInSc1
Krížného	Krížný	k2eAgInSc2d1
potoka	potok	k1gInSc2
a	a	k8xC
měří	měřit	k5eAaImIp3nS
1,2	1,2	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS
pod	pod	k7c7
vrchem	vrch	k1gInSc7
Špičiak	Špičiak	k1gInSc1
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
815	#num#	k4
m	m	kA
<g/>
,	,	kIx,
nejprve	nejprve	k6eAd1
teče	téct	k5eAaImIp3nS
východním	východní	k2eAgInSc7d1
směrem	směr	k1gInSc7
dolinou	dolina	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
nazývána	nazýván	k2eAgFnSc1d1
Regner	Regner	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
dolina	dolina	k1gFnSc1
je	být	k5eAaImIp3nS
zalesněná	zalesněný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vápenný	vápenný	k2eAgInSc1d1
potok	potok	k1gInSc1
nemá	mít	k5eNaImIp3nS
významnější	významný	k2eAgInPc4d2
přítoky	přítok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibližně	přibližně	k6eAd1
5	#num#	k4
km	km	kA
severně	severně	k6eAd1
od	od	k7c2
centra	centrum	k1gNnSc2
města	město	k1gNnSc2
Podolínec	Podolínec	k1gMnSc1
se	se	k3xPyFc4
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
715	#num#	k4
m	m	kA
vlévá	vlévat	k5eAaImIp3nS
do	do	k7c2
Krížného	Krížný	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souřadnice	souřadnice	k1gFnSc1
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
ústí	ústí	k1gNnSc2
jsou	být	k5eAaImIp3nP
49	#num#	k4
°	°	k?
17	#num#	k4
<g/>
'	'	kIx"
<g/>
59	#num#	k4
<g/>
,	,	kIx,
09	#num#	k4
"	"	kIx"
<g/>
severní	severní	k2eAgFnSc2d1
geografické	geografický	k2eAgFnSc2d1
šířky	šířka	k1gFnSc2
a	a	k8xC
20	#num#	k4
°	°	k?
30	#num#	k4
<g/>
'	'	kIx"
<g/>
32	#num#	k4
<g/>
,	,	kIx,
97	#num#	k4
<g/>
"	"	kIx"
východní	východní	k2eAgFnSc2d1
geografické	geografický	k2eAgFnSc2d1
délky	délka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Vápenný	vápenný	k2eAgInSc1d1
potok	potok	k1gInSc1
(	(	kIx(
<g/>
prítok	prítok	k1gInSc1
Krížneho	Krížne	k1gMnSc4
potoka	potok	k1gInSc2
<g/>
)	)	kIx)
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
