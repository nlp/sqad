<p>
<s>
Krakov	Krakov	k1gInSc1	Krakov
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Kraków	Kraków	k1gFnSc1	Kraków
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
'	'	kIx"	'
<g/>
krakuf	krakuf	k1gMnSc1	krakuf
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Cracovia	Cracovia	k1gFnSc1	Cracovia
<g/>
,	,	kIx,	,
litevsky	litevsky	k6eAd1	litevsky
Krokuva	Krokuva	k1gFnSc1	Krokuva
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Krakau	Krakaa	k1gFnSc4	Krakaa
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Cracow	Cracow	k1gFnSc1	Cracow
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Krakkó	Krakkó	k1gFnSc1	Krakkó
<g/>
,	,	kIx,	,
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
К	К	k?	К
<g/>
,	,	kIx,	,
tatarsky	tatarsky	k6eAd1	tatarsky
К	К	k?	К
<g/>
,	,	kIx,	,
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
ק	ק	k?	ק
Krůke	Krůk	k1gInSc2	Krůk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
metropole	metropole	k1gFnSc1	metropole
Malopolského	malopolský	k2eAgNnSc2d1	Malopolské
vojvodství	vojvodství	k1gNnSc2	vojvodství
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
zemi	zem	k1gFnSc6	zem
Malopolsku	Malopolska	k1gFnSc4	Malopolska
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Visle	Visla	k1gFnSc6	Visla
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejužším	úzký	k2eAgNnSc6d3	nejužší
místě	místo	k1gNnSc6	místo
tzv.	tzv.	kA	tzv.
Krakovské	krakovský	k2eAgFnSc2d1	Krakovská
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
Sandoměřskou	Sandoměřský	k2eAgFnSc4d1	Sandoměřský
kotlinu	kotlina	k1gFnSc4	kotlina
s	s	k7c7	s
kotlinou	kotlina	k1gFnSc7	kotlina
Osvětimskou	osvětimský	k2eAgFnSc7d1	Osvětimská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
měl	mít	k5eAaImAgInS	mít
Krakov	Krakov	k1gInSc1	Krakov
766	[number]	k4	766
739	[number]	k4	739
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
polské	polský	k2eAgNnSc4d1	polské
město	město	k1gNnSc4	město
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
města	město	k1gNnSc2	město
činí	činit	k5eAaImIp3nS	činit
326,85	[number]	k4	326,85
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krakov	Krakov	k1gInSc1	Krakov
je	být	k5eAaImIp3nS	být
historická	historický	k2eAgFnSc1d1	historická
rezidence	rezidence	k1gFnSc1	rezidence
polských	polský	k2eAgMnPc2d1	polský
králů	král	k1gMnPc2	král
s	s	k7c7	s
hradem	hrad	k1gInSc7	hrad
Wawel	Wawela	k1gFnPc2	Wawela
a	a	k8xC	a
katedrálou	katedrála	k1gFnSc7	katedrála
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc4	sídlo
starobylé	starobylý	k2eAgFnSc2d1	starobylá
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgNnSc1d1	Staré
město	město	k1gNnSc1	město
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
i	i	k8xC	i
parků	park	k1gInPc2	park
je	být	k5eAaImIp3nS	být
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
cíl	cíl	k1gInSc4	cíl
turistů	turist	k1gMnPc2	turist
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
Světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
legend	legenda	k1gFnPc2	legenda
pochází	pocházet	k5eAaImIp3nS	pocházet
jméno	jméno	k1gNnSc1	jméno
města	město	k1gNnSc2	město
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
knížete	kníže	k1gMnSc2	kníže
Kraka	Kraka	k?	Kraka
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
Slovana	Slovan	k1gMnSc4	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
však	však	k9	však
jména	jméno	k1gNnPc1	jméno
pochází	pocházet	k5eAaImIp3nP	pocházet
od	od	k7c2	od
Keltů	Kelt	k1gMnPc2	Kelt
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
obývali	obývat	k5eAaImAgMnP	obývat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
z	z	k7c2	z
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
a	a	k8xC	a
z	z	k7c2	z
římských	římský	k2eAgMnPc2d1	římský
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
by	by	kYmCp3nS	by
název	název	k1gInSc1	název
zněl	znět	k5eAaImAgInS	znět
Carragh	Carragh	k1gInSc4	Carragh
nebo	nebo	k8xC	nebo
obdobně	obdobně	k6eAd1	obdobně
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
je	být	k5eAaImIp3nS	být
však	však	k9	však
i	i	k9	i
souvislost	souvislost	k1gFnSc4	souvislost
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
legendárního	legendární	k2eAgNnSc2d1	legendární
iberského	iberský	k2eAgNnSc2d1	Iberské
(	(	kIx(	(
<g/>
keltiberského	keltiberský	k2eAgMnSc2d1	keltiberský
<g/>
)	)	kIx)	)
krále	král	k1gMnSc2	král
Crocco	Crocco	k1gNnSc4	Crocco
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
legendách	legenda	k1gFnPc6	legenda
se	se	k3xPyFc4	se
také	také	k9	také
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
postava	postava	k1gFnSc1	postava
knížete	kníže	k1gMnSc2	kníže
Kroka	Kroek	k1gMnSc2	Kroek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
opevněném	opevněný	k2eAgNnSc6d1	opevněné
Wawelském	Wawelský	k2eAgNnSc6d1	Wawelský
návrší	návrší	k1gNnSc6	návrší
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgNnPc2d3	nejdůležitější
opevnění	opevnění	k1gNnPc2	opevnění
v	v	k7c6	v
kmenovém	kmenový	k2eAgNnSc6d1	kmenové
knížectví	knížectví	k1gNnSc6	knížectví
Vislanů	Vislan	k1gInPc2	Vislan
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
knížete	kníže	k1gMnSc2	kníže
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Velkomoravská	velkomoravský	k2eAgFnSc1d1	Velkomoravská
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Krakově	Krakov	k1gInSc6	Krakov
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
o	o	k7c6	o
důležitém	důležitý	k2eAgNnSc6d1	důležité
obchodním	obchodní	k2eAgNnSc6d1	obchodní
hradišti	hradiště	k1gNnSc6	hradiště
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
966	[number]	k4	966
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
českého	český	k2eAgMnSc2d1	český
knížete	kníže	k1gMnSc2	kníže
Boleslava	Boleslav	k1gMnSc2	Boleslav
I.	I.	kA	I.
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
přemyslovskou	přemyslovský	k2eAgFnSc7d1	Přemyslovská
vládou	vláda	k1gFnSc7	vláda
zůstal	zůstat	k5eAaPmAgInS	zůstat
i	i	k9	i
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
sklonku	sklonek	k1gInSc6	sklonek
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
999	[number]	k4	999
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Krakov	Krakov	k1gInSc1	Krakov
dobyt	dobyt	k2eAgInSc1d1	dobyt
Boleslavem	Boleslav	k1gMnSc7	Boleslav
Chrabrým	chrabrý	k2eAgMnSc7d1	chrabrý
<g/>
,	,	kIx,	,
vnukem	vnuk	k1gMnSc7	vnuk
českého	český	k2eAgNnSc2d1	české
knížete	kníže	k1gNnSc2wR	kníže
Boleslava	Boleslav	k1gMnSc2	Boleslav
I.	I.	kA	I.
<g/>
,	,	kIx,	,
a	a	k8xC	a
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
piastovskému	piastovský	k2eAgInSc3d1	piastovský
polskému	polský	k2eAgInSc3d1	polský
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
se	se	k3xPyFc4	se
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
nacházelo	nacházet	k5eAaImAgNnS	nacházet
biskupství	biskupství	k1gNnSc1	biskupství
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1038	[number]	k4	1038
<g/>
,	,	kIx,	,
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Kazimíra	Kazimír	k1gMnSc2	Kazimír
I.	I.	kA	I.
Obnovitele	obnovitel	k1gMnSc2	obnovitel
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Krakov	Krakov	k1gInSc1	Krakov
stal	stát	k5eAaPmAgInS	stát
polským	polský	k2eAgNnSc7d1	polské
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
rozpadu	rozpad	k1gInSc2	rozpad
státu	stát	k1gInSc2	stát
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
úděly	úděl	k1gInPc4	úděl
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
rozbicie	rozbicie	k1gFnSc1	rozbicie
dzielnicowe	dzielnicowe	k1gInSc1	dzielnicowe
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
sídlem	sídlo	k1gNnSc7	sídlo
hlavního	hlavní	k2eAgNnSc2d1	hlavní
knížete	kníže	k1gNnSc2wR	kníže
(	(	kIx(	(
<g/>
książę	książę	k?	książę
senior	senior	k1gMnSc1	senior
<g/>
)	)	kIx)	)
–	–	k?	–
principa	principa	k1gFnSc1	principa
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
tatarského	tatarský	k2eAgInSc2d1	tatarský
vpádu	vpád	k1gInSc2	vpád
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1241	[number]	k4	1241
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
takřka	takřka	k6eAd1	takřka
úplně	úplně	k6eAd1	úplně
zničeno	zničen	k2eAgNnSc1d1	zničeno
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
obnova	obnova	k1gFnSc1	obnova
Krakova	Krakov	k1gInSc2	Krakov
a	a	k8xC	a
kníže	kníže	k1gMnSc1	kníže
Boleslav	Boleslav	k1gMnSc1	Boleslav
V.	V.	kA	V.
Nesmělý	smělý	k2eNgMnSc1d1	nesmělý
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc3	jeho
matka	matka	k1gFnSc1	matka
Grzymisława	Grzymisława	k1gFnSc1	Grzymisława
a	a	k8xC	a
manželka	manželka	k1gFnSc1	manželka
bl.	bl.	k?	bl.
Kunhuta	Kunhuta	k1gFnSc1	Kunhuta
dali	dát	k5eAaPmAgMnP	dát
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1257	[number]	k4	1257
městu	město	k1gNnSc3	město
lokátorská	lokátorský	k2eAgNnPc4d1	lokátorský
práva	právo	k1gNnPc4	právo
podle	podle	k7c2	podle
magdeburského	magdeburský	k2eAgNnSc2d1	magdeburské
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Úkol	úkol	k1gInSc1	úkol
lokátorství	lokátorství	k1gNnSc2	lokátorství
byl	být	k5eAaImAgInS	být
svěřen	svěřen	k2eAgInSc1d1	svěřen
třem	tři	k4xCgFnPc3	tři
fojtům	fojt	k1gMnPc3	fojt
<g/>
:	:	kIx,	:
Gedkovi	Gedek	k1gMnSc3	Gedek
<g/>
,	,	kIx,	,
Jakubovi	Jakub	k1gMnSc3	Jakub
z	z	k7c2	z
Nisy	Nisa	k1gFnSc2	Nisa
a	a	k8xC	a
Dytmarovi	Dytmar	k1gMnSc6	Dytmar
Wolkovi	Wolek	k1gMnSc6	Wolek
<g/>
.	.	kIx.	.
</s>
<s>
Zakládací	zakládací	k2eAgInSc1d1	zakládací
dokument	dokument	k1gInSc1	dokument
hovořil	hovořit	k5eAaImAgInS	hovořit
o	o	k7c6	o
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
osvobození	osvobození	k1gNnSc1	osvobození
měšťanů	měšťan	k1gMnPc2	měšťan
od	od	k7c2	od
daní	daň	k1gFnPc2	daň
na	na	k7c6	na
období	období	k1gNnSc6	období
6	[number]	k4	6
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
osvobození	osvobození	k1gNnSc1	osvobození
měšťanů	měšťan	k1gMnPc2	měšťan
od	od	k7c2	od
cel	clo	k1gNnPc2	clo
na	na	k7c4	na
10	[number]	k4	10
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
dával	dávat	k5eAaImAgMnS	dávat
právo	právo	k1gNnSc4	právo
lovu	lov	k1gInSc2	lov
ryb	ryba	k1gFnPc2	ryba
ve	v	k7c6	v
Visle	Visla	k1gFnSc6	Visla
na	na	k7c6	na
úseku	úsek	k1gInSc6	úsek
od	od	k7c2	od
Zwierzyńca	Zwierzyńc	k1gInSc2	Zwierzyńc
do	do	k7c2	do
Mogiły	Mogiła	k1gFnSc2	Mogiła
</s>
</p>
<p>
<s>
dával	dávat	k5eAaImAgInS	dávat
fojtům	fojt	k1gMnPc3	fojt
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
příjmů	příjem	k1gInPc2	příjem
z	z	k7c2	z
obchodů	obchod	k1gInPc2	obchod
a	a	k8xC	a
tržišť	tržiště	k1gNnPc2	tržiště
</s>
</p>
<p>
<s>
dával	dávat	k5eAaImAgInS	dávat
fojtům	fojt	k1gMnPc3	fojt
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
věčné	věčný	k2eAgNnSc4d1	věčné
osvobození	osvobození	k1gNnSc4	osvobození
od	od	k7c2	od
cel	clo	k1gNnPc2	clo
</s>
</p>
<p>
<s>
dával	dávat	k5eAaImAgInS	dávat
fojtům	fojt	k1gMnPc3	fojt
právo	právo	k1gNnSc4	právo
vlastnit	vlastnit	k5eAaImF	vlastnit
jatka	jatka	k1gFnSc1	jatka
a	a	k8xC	a
mlýnyTehdy	mlýnyTehdy	k6eAd1	mlýnyTehdy
také	také	k9	také
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
šachovnicové	šachovnicový	k2eAgNnSc1d1	šachovnicové
uspořádání	uspořádání	k1gNnSc1	uspořádání
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byly	být	k5eAaImAgInP	být
zasazeny	zasadit	k5eAaPmNgInP	zasadit
dochované	dochovaný	k2eAgInPc1d1	dochovaný
dřívější	dřívější	k2eAgInPc1d1	dřívější
prvky	prvek	k1gInPc1	prvek
(	(	kIx(	(
<g/>
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Grodzka	Grodzka	k1gFnSc1	Grodzka
<g/>
,	,	kIx,	,
Mariánský	mariánský	k2eAgInSc1d1	mariánský
kostel	kostel	k1gInSc1	kostel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Krakovem	Krakov	k1gInSc7	Krakov
a	a	k8xC	a
Wawelem	Wawel	k1gInSc7	Wawel
ještě	ještě	k6eAd1	ještě
existovala	existovat	k5eAaImAgFnS	existovat
osada	osada	k1gFnSc1	osada
Okół	Okół	k1gFnSc1	Okół
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
nezávislou	závislý	k2eNgFnSc7d1	nezávislá
tvrzí	tvrz	k1gFnSc7	tvrz
do	do	k7c2	do
vzpoury	vzpoura	k1gFnSc2	vzpoura
fojta	fojt	k1gMnSc2	fojt
Alberta	Albert	k1gMnSc2	Albert
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
král	král	k1gMnSc1	král
Vladislav	Vladislav	k1gMnSc1	Vladislav
Lokýtek	lokýtek	k1gInSc4	lokýtek
zbořil	zbořit	k5eAaPmAgMnS	zbořit
její	její	k3xOp3gFnSc3	její
zdi	zeď	k1gFnSc3	zeď
a	a	k8xC	a
začlenil	začlenit	k5eAaPmAgMnS	začlenit
ji	on	k3xPp3gFnSc4	on
ke	k	k7c3	k
Krakovu	krakův	k2eAgNnSc3d1	krakův
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
staré	starý	k2eAgNnSc4d1	staré
město	město	k1gNnSc4	město
protáhlý	protáhlý	k2eAgInSc1d1	protáhlý
severo-jižní	severoižní	k2eAgInSc1d1	severo-jižní
tvar	tvar	k1gInSc1	tvar
a	a	k8xC	a
táhne	táhnout	k5eAaImIp3nS	táhnout
se	se	k3xPyFc4	se
až	až	k9	až
k	k	k7c3	k
Hradu	hrad	k1gInSc3	hrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1320	[number]	k4	1320
se	se	k3xPyFc4	se
ve	v	k7c6	v
wawelské	wawelský	k2eAgFnSc6d1	wawelský
katedrále	katedrála	k1gFnSc6	katedrála
konala	konat	k5eAaImAgFnS	konat
korunovace	korunovace	k1gFnSc1	korunovace
Vladislava	Vladislav	k1gMnSc2	Vladislav
Lokietka	Lokietka	k1gFnSc1	Lokietka
na	na	k7c4	na
polského	polský	k2eAgMnSc4d1	polský
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
skončilo	skončit	k5eAaPmAgNnS	skončit
období	období	k1gNnSc1	období
rozpadu	rozpad	k1gInSc2	rozpad
Polska	Polsko	k1gNnSc2	Polsko
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1734	[number]	k4	1734
byl	být	k5eAaImAgInS	být
Krakov	Krakov	k1gInSc4	Krakov
místem	místem	k6eAd1	místem
korunovace	korunovace	k1gFnSc1	korunovace
polských	polský	k2eAgMnPc2d1	polský
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
Krakova	Krakov	k1gInSc2	Krakov
dvě	dva	k4xCgNnPc4	dva
další	další	k2eAgNnPc4d1	další
města	město	k1gNnPc4	město
<g/>
:	:	kIx,	:
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Kazimierz	Kazimierza	k1gFnPc2	Kazimierza
(	(	kIx(	(
<g/>
1335	[number]	k4	1335
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Kleparz	Kleparza	k1gFnPc2	Kleparza
(	(	kIx(	(
<g/>
1366	[number]	k4	1366
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krakov	Krakov	k1gInSc1	Krakov
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
významných	významný	k2eAgInPc2d1	významný
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
rozvíjel	rozvíjet	k5eAaImAgMnS	rozvíjet
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
směru	směr	k1gInSc6	směr
–	–	k?	–
architektonickém	architektonický	k2eAgNnSc6d1	architektonické
<g/>
,	,	kIx,	,
obchodním	obchodní	k2eAgNnSc6d1	obchodní
<g/>
,	,	kIx,	,
řemeslnickém	řemeslnický	k2eAgNnSc6d1	řemeslnické
<g/>
,	,	kIx,	,
kulturním	kulturní	k2eAgNnSc6d1	kulturní
<g/>
,	,	kIx,	,
vědeckém	vědecký	k2eAgNnSc6d1	vědecké
<g/>
.	.	kIx.	.
</s>
<s>
Hradní	hradní	k2eAgInSc1d1	hradní
komplex	komplex	k1gInSc1	komplex
na	na	k7c4	na
Wawelu	Wawela	k1gFnSc4	Wawela
byl	být	k5eAaImAgInS	být
přestavěn	přestavět	k5eAaPmNgInS	přestavět
a	a	k8xC	a
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
v	v	k7c6	v
renesančním	renesanční	k2eAgInSc6d1	renesanční
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1364	[number]	k4	1364
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
byl	být	k5eAaImAgInS	být
zbudován	zbudován	k2eAgInSc1d1	zbudován
Barbakan	Barbakan	k1gInSc1	Barbakan
(	(	kIx(	(
<g/>
obranná	obranný	k2eAgFnSc1d1	obranná
bašta	bašta	k1gFnSc1	bašta
<g/>
,	,	kIx,	,
části	část	k1gFnPc1	část
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
tvář	tvář	k1gFnSc4	tvář
dostal	dostat	k5eAaPmAgInS	dostat
Krakovský	krakovský	k2eAgInSc1d1	krakovský
rynek	rynek	k1gInSc1	rynek
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Zikmunda	Zikmund	k1gMnSc2	Zikmund
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Augusta	Augusta	k1gMnSc1	Augusta
(	(	kIx(	(
<g/>
polovina	polovina	k1gFnSc1	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgInS	mít
Krakov	Krakov	k1gInSc1	Krakov
asi	asi	k9	asi
30	[number]	k4	30
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
polsko-litevské	polskoitevský	k2eAgFnSc6d1	polsko-litevská
unii	unie	k1gFnSc6	unie
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
Republiky	republika	k1gFnSc2	republika
obou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
Rzeczpospolita	Rzeczpospolit	k2eAgFnSc1d1	Rzeczpospolita
Obojga	Obojga	k1gFnSc1	Obojga
Narodów	Narodów	k1gFnSc2	Narodów
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Krakov	Krakov	k1gInSc1	Krakov
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
velkého	velký	k2eAgInSc2d1	velký
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
Sejmy	Sejm	k1gInPc1	Sejm
a	a	k8xC	a
volby	volba	k1gFnPc1	volba
monarchů	monarcha	k1gMnPc2	monarcha
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ležela	ležet	k5eAaImAgFnS	ležet
víceméně	víceméně	k9	víceméně
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
cesty	cesta	k1gFnSc2	cesta
mezi	mezi	k7c7	mezi
hlavními	hlavní	k2eAgNnPc7d1	hlavní
městy	město	k1gNnPc7	město
Koruny	koruna	k1gFnSc2	koruna
polské	polský	k2eAgFnSc2d1	polská
a	a	k8xC	a
Litvy	Litva	k1gFnSc2	Litva
<g/>
.	.	kIx.	.
</s>
<s>
Definitivně	definitivně	k6eAd1	definitivně
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Vasa	Vasa	k1gFnSc1	Vasa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1609	[number]	k4	1609
přenesl	přenést	k5eAaPmAgInS	přenést
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Polska	Polsko	k1gNnSc2	Polsko
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
formálního	formální	k2eAgInSc2d1	formální
právního	právní	k2eAgInSc2d1	právní
aktu	akt	k1gInSc2	akt
<g/>
)	)	kIx)	)
do	do	k7c2	do
Varšavy	Varšava	k1gFnSc2	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
katedrála	katedrála	k1gFnSc1	katedrála
na	na	k7c4	na
Wawelu	Wawela	k1gFnSc4	Wawela
zůstala	zůstat	k5eAaPmAgFnS	zůstat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
korunovačním	korunovační	k2eAgNnSc7d1	korunovační
místem	místo	k1gNnSc7	místo
polských	polský	k2eAgMnPc2d1	polský
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
úpadkem	úpadek	k1gInSc7	úpadek
Rzeczpospolity	Rzeczpospolit	k1gInPc4	Rzeczpospolit
začal	začít	k5eAaPmAgInS	začít
také	také	k9	také
úpadek	úpadek	k1gInSc1	úpadek
samotného	samotný	k2eAgNnSc2d1	samotné
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Válečné	válečný	k2eAgFnPc1d1	válečná
ztráty	ztráta	k1gFnPc1	ztráta
silně	silně	k6eAd1	silně
poškodily	poškodit	k5eAaPmAgFnP	poškodit
jeho	jeho	k3xOp3gFnSc4	jeho
pozici	pozice	k1gFnSc4	pozice
a	a	k8xC	a
zbrzdily	zbrzdit	k5eAaPmAgInP	zbrzdit
i	i	k9	i
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
Krakov	Krakov	k1gInSc1	Krakov
zničen	zničit	k5eAaPmNgInS	zničit
cizími	cizí	k2eAgNnPc7d1	cizí
vojsky	vojsko	k1gNnPc7	vojsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1655	[number]	k4	1655
během	během	k7c2	během
švédské	švédský	k2eAgFnSc2d1	švédská
"	"	kIx"	"
<g/>
potopy	potopa	k1gFnSc2	potopa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
dobýván	dobývat	k5eAaImNgMnS	dobývat
vojsky	vojsky	k6eAd1	vojsky
pruskými	pruský	k2eAgMnPc7d1	pruský
<g/>
,	,	kIx,	,
švédskými	švédský	k2eAgMnPc7d1	švédský
<g/>
,	,	kIx,	,
rakouskými	rakouský	k2eAgMnPc7d1	rakouský
a	a	k8xC	a
ruskými	ruský	k2eAgMnPc7d1	ruský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
třetím	třetí	k4xOgInSc6	třetí
dělení	dělení	k1gNnSc6	dělení
Polska	Polsko	k1gNnSc2	Polsko
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Krakov	Krakov	k1gInSc4	Krakov
Rakušané	Rakušan	k1gMnPc1	Rakušan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1809	[number]	k4	1809
<g/>
–	–	k?	–
<g/>
1815	[number]	k4	1815
náležel	náležet	k5eAaImAgMnS	náležet
do	do	k7c2	do
Varšavského	varšavský	k2eAgNnSc2d1	Varšavské
knížectví	knížectví	k1gNnSc2	knížectví
jako	jako	k9	jako
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
departamentu	departament	k1gInSc2	departament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1815	[number]	k4	1815
<g/>
–	–	k?	–
<g/>
1846	[number]	k4	1846
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
maličké	maličká	k1gFnSc2	maličká
<g/>
,	,	kIx,	,
formálně	formálně	k6eAd1	formálně
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
Krakovské	krakovský	k2eAgFnSc2d1	Krakovská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
začala	začít	k5eAaPmAgFnS	začít
celková	celkový	k2eAgFnSc1d1	celková
modernizace	modernizace	k1gFnSc1	modernizace
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zbourána	zbourán	k2eAgFnSc1d1	zbourána
většina	většina	k1gFnSc1	většina
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zasypán	zasypat	k5eAaPmNgInS	zasypat
příkop	příkop	k1gInSc1	příkop
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
byly	být	k5eAaImAgInP	být
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
městské	městský	k2eAgInPc1d1	městský
sady	sad	k1gInPc1	sad
<g/>
,	,	kIx,	,
obepínající	obepínající	k2eAgNnSc1d1	obepínající
staré	starý	k2eAgNnSc1d1	staré
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
"	"	kIx"	"
<g/>
krakovské	krakovský	k2eAgFnSc6d1	Krakovská
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
"	"	kIx"	"
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
byl	být	k5eAaImAgInS	být
Krakov	Krakov	k1gInSc1	Krakov
opět	opět	k6eAd1	opět
začleněn	začlenit	k5eAaPmNgInS	začlenit
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
jako	jako	k8xC	jako
velkovévodství	velkovévodství	k1gNnSc2	velkovévodství
Krakovské	krakovský	k2eAgFnSc2d1	Krakovská
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
zůstal	zůstat	k5eAaPmAgInS	zůstat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
zničil	zničit	k5eAaPmAgInS	zničit
velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
téměř	téměř	k6eAd1	téměř
půl	půl	k1xP	půl
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
a	a	k8xC	a
rozpadu	rozpad	k1gInSc3	rozpad
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
byl	být	k5eAaImAgInS	být
Krakov	Krakov	k1gInSc1	Krakov
přičleněn	přičlenit	k5eAaPmNgInS	přičlenit
k	k	k7c3	k
nově	nově	k6eAd1	nově
vzniklému	vzniklý	k2eAgNnSc3d1	vzniklé
Polsku	Polsko	k1gNnSc3	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
tzv.	tzv.	kA	tzv.
Generálního	generální	k2eAgInSc2d1	generální
gouvernementu	gouvernement	k1gInSc2	gouvernement
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
byl	být	k5eAaImAgMnS	být
nedaleko	nedaleko	k7c2	nedaleko
Krakova	Krakov	k1gInSc2	Krakov
zřízen	zřídit	k5eAaPmNgInS	zřídit
pracovní	pracovní	k2eAgInSc1d1	pracovní
tábor	tábor	k1gInSc1	tábor
pro	pro	k7c4	pro
Židovské	židovská	k1gFnPc4	židovská
vězně	vězeň	k1gMnSc2	vězeň
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
proměněn	proměnit	k5eAaPmNgMnS	proměnit
na	na	k7c4	na
koncentrační	koncentrační	k2eAgInSc4d1	koncentrační
tábor	tábor	k1gInSc4	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Táborem	Tábor	k1gInSc7	Tábor
"	"	kIx"	"
<g/>
prošlo	projít	k5eAaPmAgNnS	projít
<g/>
"	"	kIx"	"
na	na	k7c4	na
150000	[number]	k4	150000
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
Tábor	Tábor	k1gInSc1	Tábor
byl	být	k5eAaImAgInS	být
osvobozen	osvobodit	k5eAaPmNgInS	osvobodit
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krakov	Krakov	k1gInSc1	Krakov
byl	být	k5eAaImAgInS	být
osvobozen	osvobodit	k5eAaPmNgInS	osvobodit
RA	ra	k0	ra
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
rychlou	rychlý	k2eAgFnSc7d1	rychlá
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
operací	operace	k1gFnSc7	operace
<g/>
.	.	kIx.	.
<g/>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
zachránit	zachránit	k5eAaPmF	zachránit
hodně	hodně	k6eAd1	hodně
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
inventář	inventář	k1gInSc4	inventář
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
zámku	zámek	k1gInSc2	zámek
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
částečně	částečně	k6eAd1	částečně
odvezen	odvézt	k5eAaPmNgMnS	odvézt
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
i	i	k8xC	i
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
bylo	být	k5eAaImAgNnS	být
ke	k	k7c3	k
Krakovu	Krakov	k1gInSc3	Krakov
připojeno	připojen	k2eAgNnSc4d1	připojeno
nově	nově	k6eAd1	nově
založené	založený	k2eAgNnSc4d1	založené
město	město	k1gNnSc4	město
Nová	nový	k2eAgFnSc1d1	nová
Huť	huť	k1gFnSc1	huť
(	(	kIx(	(
<g/>
Nowa	Now	k2eAgFnSc1d1	Nowa
Huta	Huta	k1gFnSc1	Huta
<g/>
,	,	kIx,	,
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
na	na	k7c6	na
území	území	k1gNnSc6	území
vesnic	vesnice	k1gFnPc2	vesnice
Mogiła	Mogiłum	k1gNnSc2	Mogiłum
<g/>
,	,	kIx,	,
Płaszów	Płaszów	k1gFnSc2	Płaszów
a	a	k8xC	a
Krzesławice	Krzesławice	k1gFnSc2	Krzesławice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Krakov	Krakov	k1gInSc1	Krakov
bývá	bývat	k5eAaImIp3nS	bývat
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
polské	polský	k2eAgFnSc2d1	polská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
mj.	mj.	kA	mj.
nositelka	nositelka	k1gFnSc1	nositelka
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
Wisława	Wisławus	k1gMnSc2	Wisławus
Szymborska	Szymborsek	k1gMnSc2	Szymborsek
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
zde	zde	k6eAd1	zde
i	i	k9	i
držitel	držitel	k1gMnSc1	držitel
stejné	stejný	k2eAgFnSc2d1	stejná
ceny	cena	k1gFnSc2	cena
Czesław	Czesław	k1gMnSc1	Czesław
Miłosz	Miłosz	k1gMnSc1	Miłosz
nebo	nebo	k8xC	nebo
známý	známý	k2eAgMnSc1d1	známý
autor	autor	k1gMnSc1	autor
Stanisław	Stanisław	k1gFnSc2	Stanisław
Lem	lem	k1gInSc1	lem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgNnPc2d3	nejdůležitější
turistických	turistický	k2eAgNnPc2d1	turistické
<g/>
,	,	kIx,	,
kulturních	kulturní	k2eAgNnPc2d1	kulturní
a	a	k8xC	a
historických	historický	k2eAgNnPc2d1	historické
center	centrum	k1gNnPc2	centrum
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
metropolí	metropol	k1gFnSc7	metropol
Varšavou	Varšava	k1gFnSc7	Varšava
Krakov	Krakov	k1gInSc1	Krakov
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
turistů	turist	k1gMnPc2	turist
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
milióny	milión	k4xCgInPc4	milión
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kompaktní	kompaktní	k2eAgFnSc1d1	kompaktní
oblast	oblast	k1gFnSc1	oblast
starého	starý	k2eAgNnSc2d1	staré
města	město	k1gNnSc2	město
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
historickou	historický	k2eAgFnSc7d1	historická
zástavbou	zástavba	k1gFnSc7	zástavba
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
zařazena	zařadit	k5eAaPmNgFnS	zařadit
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
kulturních	kulturní	k2eAgFnPc2d1	kulturní
institucí	instituce	k1gFnPc2	instituce
významných	významný	k2eAgInPc2d1	významný
pro	pro	k7c4	pro
polský	polský	k2eAgInSc4d1	polský
kulturní	kulturní	k2eAgInSc4d1	kulturní
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Divadla	divadlo	k1gNnPc1	divadlo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Teatr	Teatr	k1gInSc1	Teatr
Groteska	groteska	k1gFnSc1	groteska
</s>
</p>
<p>
<s>
Teatr	Teatr	k1gInSc1	Teatr
Ludowy	Ludowa	k1gFnSc2	Ludowa
</s>
</p>
<p>
<s>
Teatr	Teatr	k1gInSc1	Teatr
Stary	star	k1gInPc1	star
</s>
</p>
<p>
<s>
Teatr	Teatr	k1gMnSc1	Teatr
Słowackiego	Słowackiego	k1gMnSc1	Słowackiego
</s>
</p>
<p>
<s>
Operetka	operetka	k1gFnSc1	operetka
Krakowska	Krakowsk	k1gInSc2	Krakowsk
</s>
</p>
<p>
<s>
Teatr	Teatr	k1gMnSc1	Teatr
BagatelaV	BagatelaV	k1gMnSc1	BagatelaV
Krakově	Krakov	k1gInSc6	Krakov
je	být	k5eAaImIp3nS	být
28	[number]	k4	28
muzeí	muzeum	k1gNnPc2	muzeum
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Národním	národní	k2eAgNnSc7d1	národní
muzeem	muzeum	k1gNnSc7	muzeum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
početné	početný	k2eAgFnPc4d1	početná
sbírky	sbírka	k1gFnPc4	sbírka
polského	polský	k2eAgMnSc2d1	polský
i	i	k8xC	i
světového	světový	k2eAgNnSc2d1	světové
malířství	malířství	k1gNnSc2	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
Muzeum	muzeum	k1gNnSc1	muzeum
polského	polský	k2eAgNnSc2d1	polské
letectví	letectví	k1gNnSc2	letectví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
rozdělení	rozdělení	k1gNnSc1	rozdělení
Krakova	Krakov	k1gInSc2	Krakov
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1991	[number]	k4	1991
je	být	k5eAaImIp3nS	být
Krakov	Krakov	k1gInSc1	Krakov
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
18	[number]	k4	18
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
označených	označený	k2eAgFnPc2d1	označená
římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
a	a	k8xC	a
názvem	název	k1gInSc7	název
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
krakovských	krakovský	k2eAgFnPc2d1	Krakovská
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
včetně	včetně	k7c2	včetně
rozlohy	rozloha	k1gFnSc2	rozloha
a	a	k8xC	a
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Dřívější	dřívější	k2eAgNnSc4d1	dřívější
administrativní	administrativní	k2eAgNnSc4d1	administrativní
rozdělení	rozdělení	k1gNnSc4	rozdělení
===	===	k?	===
</s>
</p>
<p>
<s>
1951	[number]	k4	1951
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
–	–	k?	–
6	[number]	k4	6
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
Stare	star	k1gInSc5	star
Miasto	Miasta	k1gFnSc5	Miasta
<g/>
,	,	kIx,	,
Zwierzyniec	Zwierzyniec	k1gMnSc1	Zwierzyniec
<g/>
,	,	kIx,	,
Kleparz	Kleparz	k1gMnSc1	Kleparz
<g/>
,	,	kIx,	,
Grzegórzki	Grzegórzki	k1gNnSc1	Grzegórzki
<g/>
,	,	kIx,	,
Podgórze	Podgórze	k1gFnSc1	Podgórze
<g/>
,	,	kIx,	,
Nowa	Now	k2eAgFnSc1d1	Nowa
Huta	Huta	k1gFnSc1	Huta
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
–	–	k?	–
4	[number]	k4	4
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
<g/>
:	:	kIx,	:
Śródmieście	Śródmieście	k1gFnSc1	Śródmieście
<g/>
,	,	kIx,	,
Podgórze	Podgórze	k1gFnSc1	Podgórze
<g/>
,	,	kIx,	,
Krowodrza	Krowodrza	k1gFnSc1	Krowodrza
<g/>
,	,	kIx,	,
Nowa	Now	k2eAgFnSc1d1	Nowa
Huta	Huta	k1gFnSc1	Huta
</s>
</p>
<p>
<s>
==	==	k?	==
Památky	památka	k1gFnPc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
Krakov	Krakov	k1gInSc1	Krakov
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgNnPc2d3	nejdůležitější
polských	polský	k2eAgNnPc2d1	polské
turistických	turistický	k2eAgNnPc2d1	turistické
center	centrum	k1gNnPc2	centrum
s	s	k7c7	s
cennými	cenný	k2eAgFnPc7d1	cenná
památkami	památka	k1gFnPc7	památka
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
epoch	epocha	k1gFnPc2	epocha
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
6	[number]	k4	6
tisíc	tisíc	k4xCgInSc4	tisíc
historických	historický	k2eAgInPc2d1	historický
objektů	objekt	k1gInPc2	objekt
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
čtvrti	čtvrt	k1gFnSc2	čtvrt
Stare	star	k1gInSc5	star
miasto	miasto	k6eAd1	miasto
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
historické	historický	k2eAgFnPc4d1	historická
části	část	k1gFnPc4	část
Staré	Staré	k2eAgNnSc1d1	Staré
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Wawel	Wawel	k1gInSc1	Wawel
a	a	k8xC	a
Kazimierz	Kazimierz	k1gInSc1	Kazimierz
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
měla	mít	k5eAaImAgFnS	mít
vlastní	vlastní	k2eAgFnPc4d1	vlastní
hradby	hradba	k1gFnPc4	hradba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Wawelské	Wawelský	k2eAgNnSc1d1	Wawelský
návrší	návrší	k1gNnSc1	návrší
===	===	k?	===
</s>
</p>
<p>
<s>
Katedrála	katedrála	k1gFnSc1	katedrála
na	na	k7c4	na
Wawelu	Wawela	k1gFnSc4	Wawela
čili	čili	k8xC	čili
Katedrála	katedrál	k1gMnSc4	katedrál
sv.	sv.	kA	sv.
Stanislava	Stanislav	k1gMnSc4	Stanislav
a	a	k8xC	a
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc4	Václav
je	být	k5eAaImIp3nS	být
cihlová	cihlový	k2eAgFnSc1d1	cihlová
gotická	gotický	k2eAgFnSc1d1	gotická
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
bazilika	bazilika	k1gFnSc1	bazilika
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
věžemi	věž	k1gFnPc7	věž
a	a	k8xC	a
věncem	věnec	k1gInSc7	věnec
kaplí	kaple	k1gFnPc2	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	let	k1gInPc6	let
1320	[number]	k4	1320
<g/>
–	–	k?	–
<g/>
1364	[number]	k4	1364
<g/>
,	,	kIx,	,
katedrála	katedrála	k1gFnSc1	katedrála
byla	být	k5eAaImAgFnS	být
tradiční	tradiční	k2eAgInSc4d1	tradiční
korunovační	korunovační	k2eAgInSc4d1	korunovační
kostel	kostel	k1gInSc4	kostel
i	i	k8xC	i
pohřebiště	pohřebiště	k1gNnSc4	pohřebiště
polských	polský	k2eAgMnPc2d1	polský
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
jsou	být	k5eAaImIp3nP	být
pochováni	pochován	k2eAgMnPc1d1	pochován
i	i	k8xC	i
svatý	svatý	k1gMnSc1	svatý
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
,	,	kIx,	,
svatá	svatý	k2eAgFnSc1d1	svatá
Hedvika	Hedvika	k1gFnSc1	Hedvika
z	z	k7c2	z
Anjou	Anjý	k2eAgFnSc7d1	Anjý
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgMnPc2d1	další
významných	významný	k2eAgMnPc2d1	významný
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Královský	královský	k2eAgInSc1d1	královský
hrad	hrad	k1gInSc1	hrad
Wawel	Wawel	k1gInSc1	Wawel
(	(	kIx(	(
<g/>
Zamek	Zamek	k1gInSc1	Zamek
Królewski	Królewsk	k1gFnSc2	Królewsk
na	na	k7c6	na
Wawelu	Wawel	k1gInSc6	Wawel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
nejstarší	starý	k2eAgNnSc4d3	nejstarší
osídlení	osídlení	k1gNnSc4	osídlení
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
,	,	kIx,	,
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
hrad	hrad	k1gInSc4	hrad
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
sídlo	sídlo	k1gNnSc1	sídlo
polských	polský	k2eAgMnPc2d1	polský
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
podobě	podoba	k1gFnSc6	podoba
renesanční	renesanční	k2eAgFnSc1d1	renesanční
a	a	k8xC	a
barokní	barokní	k2eAgFnSc1d1	barokní
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
,	,	kIx,	,
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
jako	jako	k8xC	jako
sídlo	sídlo	k1gNnSc1	sídlo
nacistického	nacistický	k2eAgMnSc2d1	nacistický
guvernéra	guvernér	k1gMnSc2	guvernér
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dračí	dračí	k2eAgFnSc1d1	dračí
jáma	jáma	k1gFnSc1	jáma
(	(	kIx(	(
<g/>
Smocza	Smocza	k1gFnSc1	Smocza
Jama	Jama	k1gMnSc1	Jama
<g/>
)	)	kIx)	)
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
pahorku	pahorek	k1gInSc2	pahorek
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
zde	zde	k6eAd1	zde
draka	drak	k1gMnSc4	drak
usmrtil	usmrtit	k5eAaPmAgMnS	usmrtit
rytíř	rytíř	k1gMnSc1	rytíř
Krak	Krak	k?	Krak
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
jméno	jméno	k1gNnSc1	jméno
město	město	k1gNnSc4	město
nese	nést	k5eAaImIp3nS	nést
</s>
</p>
<p>
<s>
===	===	k?	===
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
===	===	k?	===
</s>
</p>
<p>
<s>
Barbakán	Barbakán	k2eAgInSc1d1	Barbakán
<g/>
,	,	kIx,	,
opevněná	opevněný	k2eAgFnSc1d1	opevněná
pozdně	pozdně	k6eAd1	pozdně
gotická	gotický	k2eAgFnSc1d1	gotická
brána	brána	k1gFnSc1	brána
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
Starého	Starého	k2eAgNnSc2d1	Starého
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Gotická	gotický	k2eAgFnSc1d1	gotická
kruhová	kruhový	k2eAgFnSc1d1	kruhová
stavba	stavba	k1gFnSc1	stavba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1498	[number]	k4	1498
a	a	k8xC	a
1499	[number]	k4	1499
<g/>
.	.	kIx.	.
</s>
<s>
Barbakán	Barbakán	k2eAgInSc1d1	Barbakán
byl	být	k5eAaImAgInS	být
hradbami	hradba	k1gFnPc7	hradba
spojen	spojit	k5eAaPmNgMnS	spojit
s	s	k7c7	s
Floriánskou	Floriánský	k2eAgFnSc7d1	Floriánská
bránou	brána	k1gFnSc7	brána
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
cihlovou	cihlový	k2eAgFnSc4d1	cihlová
stavbu	stavba	k1gFnSc4	stavba
se	s	k7c7	s
sedmi	sedm	k4xCc7	sedm
pozorovacími	pozorovací	k2eAgFnPc7d1	pozorovací
věžemi	věž	k1gFnPc7	věž
a	a	k8xC	a
130	[number]	k4	130
střílnami	střílna	k1gFnPc7	střílna
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
stavby	stavba	k1gFnSc2	stavba
je	být	k5eAaImIp3nS	být
24.4	[number]	k4	24.4
metru	metr	k1gInSc2	metr
a	a	k8xC	a
zdi	zeď	k1gFnSc2	zeď
jsou	být	k5eAaImIp3nP	být
přes	přes	k7c4	přes
tři	tři	k4xCgInPc4	tři
metry	metr	k1gInPc4	metr
silné	silný	k2eAgFnPc1d1	silná
<g/>
.	.	kIx.	.
</s>
<s>
Barbakán	Barbakán	k2eAgMnSc1d1	Barbakán
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc1	dva
brány	brána	k1gFnPc1	brána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Floriánská	Floriánský	k2eAgFnSc1d1	Floriánská
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
pozůstatek	pozůstatek	k1gInSc1	pozůstatek
gotického	gotický	k2eAgNnSc2d1	gotické
opevnění	opevnění	k1gNnSc2	opevnění
Starého	Starého	k2eAgNnSc2d1	Starého
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jediná	jediné	k1gNnPc1	jediné
zachovaná	zachovaný	k2eAgNnPc1d1	zachované
z	z	k7c2	z
původních	původní	k2eAgNnPc2d1	původní
osmi	osm	k4xCc2	osm
bran	brána	k1gFnPc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vystavěna	vystavět	k5eAaPmNgFnS	vystavět
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
koncem	koncem	k7c2	koncem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
výšku	výška	k1gFnSc4	výška
přes	přes	k7c4	přes
34	[number]	k4	34
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zdobena	zdobit	k5eAaImNgFnS	zdobit
sochou	socha	k1gFnSc7	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Floriána	Florián	k1gMnSc2	Florián
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ulica	Ulic	k2eAgFnSc1d1	Ulica
Floriańska	Floriańska	k1gFnSc1	Floriańska
–	–	k?	–
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
a	a	k8xC	a
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
ulic	ulice	k1gFnPc2	ulice
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
335	[number]	k4	335
metrů	metr	k1gInPc2	metr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
ulice	ulice	k1gFnSc1	ulice
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1257	[number]	k4	1257
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
část	část	k1gFnSc1	část
tzv.	tzv.	kA	tzv.
královské	královský	k2eAgFnSc2d1	královská
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
Wawelu	Wawel	k1gInSc3	Wawel
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovoznit	k5eAaPmNgFnS	zprovoznit
první	první	k4xOgFnSc1	první
koňská	koňský	k2eAgFnSc1d1	koňská
tramvaj	tramvaj	k1gFnSc1	tramvaj
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
první	první	k4xOgFnSc1	první
elektrická	elektrický	k2eAgFnSc1d1	elektrická
tramvaj	tramvaj	k1gFnSc1	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
ulice	ulice	k1gFnSc1	ulice
je	být	k5eAaImIp3nS	být
pěší	pěší	k2eAgFnSc7d1	pěší
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
četné	četný	k2eAgInPc1d1	četný
patricijské	patricijský	k2eAgInPc1d1	patricijský
domy	dům	k1gInPc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mnohých	mnohý	k2eAgInPc6d1	mnohý
domech	dům	k1gInPc6	dům
jsou	být	k5eAaImIp3nP	být
zachovány	zachován	k2eAgFnPc1d1	zachována
gotické	gotický	k2eAgFnPc1d1	gotická
dekorace	dekorace	k1gFnPc1	dekorace
a	a	k8xC	a
renesanční	renesanční	k2eAgInPc1d1	renesanční
portály	portál	k1gInPc1	portál
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgInPc1d1	zajímavý
domy	dům	k1gInPc1	dům
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
nejstarší	starý	k2eAgInSc1d3	nejstarší
hotel	hotel	k1gInSc1	hotel
ve	v	k7c6	v
městě	město	k1gNnSc6	město
U	u	k7c2	u
růží	růž	k1gFnPc2	růž
(	(	kIx(	(
<g/>
č.	č.	k?	č.
14	[number]	k4	14
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Farmaceutické	farmaceutický	k2eAgNnSc1d1	farmaceutické
museum	museum	k1gNnSc1	museum
Jagelonské	jagelonský	k2eAgFnSc2d1	Jagelonská
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc1	číslo
25	[number]	k4	25
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dům	dům	k1gInSc4	dům
malíře	malíř	k1gMnSc2	malíř
Jana	Jan	k1gMnSc2	Jan
Matejka	Matejka	k1gFnSc1	Matejka
<g/>
,	,	kIx,	,
dneska	dneska	k?	dneska
filiálka	filiálka	k1gFnSc1	filiálka
krakovského	krakovský	k2eAgNnSc2d1	Krakovské
Národního	národní	k2eAgNnSc2d1	národní
musea	museum	k1gNnSc2	museum
(	(	kIx(	(
<g/>
č.	č.	k?	č.
41	[number]	k4	41
<g/>
)	)	kIx)	)
a	a	k8xC	a
legendární	legendární	k2eAgFnSc1d1	legendární
kavárna	kavárna	k1gFnSc1	kavárna
Michalikova	Michalikův	k2eAgFnSc1d1	Michalikův
jeskyně	jeskyně	k1gFnSc1	jeskyně
(	(	kIx(	(
<g/>
č.	č.	k?	č.
45	[number]	k4	45
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ulica	Ulic	k2eAgFnSc1d1	Ulica
Grodzka	Grodzka	k1gFnSc1	Grodzka
–	–	k?	–
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgFnPc3d3	nejstarší
reprezentativním	reprezentativní	k2eAgFnPc3d1	reprezentativní
ulicím	ulice	k1gFnPc3	ulice
Krakova	Krakov	k1gInSc2	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
částí	část	k1gFnSc7	část
tzv.	tzv.	kA	tzv.
královské	královský	k2eAgFnSc2d1	královská
cesty	cesta	k1gFnSc2	cesta
a	a	k8xC	a
staré	starý	k2eAgFnSc2d1	stará
obchodní	obchodní	k2eAgFnSc2d1	obchodní
stezky	stezka	k1gFnSc2	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Měří	měřit	k5eAaImIp3nS	měřit
skoro	skoro	k6eAd1	skoro
700	[number]	k4	700
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodné	pozoruhodný	k2eAgInPc1d1	pozoruhodný
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
následující	následující	k2eAgInPc1d1	následující
domy	dům	k1gInPc1	dům
<g/>
:	:	kIx,	:
dům	dům	k1gInSc1	dům
rodiny	rodina	k1gFnSc2	rodina
Stachowitz	Stachowitza	k1gFnPc2	Stachowitza
(	(	kIx(	(
<g/>
č.	č.	k?	č.
15	[number]	k4	15
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
dům	dům	k1gInSc1	dům
U	u	k7c2	u
lvů	lev	k1gInPc2	lev
(	(	kIx(	(
<g/>
č.	č.	k?	č.
32	[number]	k4	32
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
dům	dům	k1gInSc1	dům
U	u	k7c2	u
slonů	slon	k1gMnPc2	slon
(	(	kIx(	(
<g/>
č.	č.	k?	č.
38	[number]	k4	38
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Collegium	Collegium	k1gNnSc1	Collegium
Iuridicum	Iuridicum	k1gNnSc1	Iuridicum
(	(	kIx(	(
<g/>
č.	č.	k?	č.
53	[number]	k4	53
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
budov	budova	k1gFnPc2	budova
Jagelonské	jagelonský	k2eAgFnSc2d1	Jagelonská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
;	;	kIx,	;
dům	dům	k1gInSc1	dům
Veita	Veit	k1gMnSc2	Veit
Stossa	Stoss	k1gMnSc2	Stoss
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
č.	č.	k?	č.
39	[number]	k4	39
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
palác	palác	k1gInSc1	palác
rodiny	rodina	k1gFnSc2	rodina
Stadnických	Stadnický	k2eAgFnPc2d1	Stadnický
(	(	kIx(	(
<g/>
č.	č.	k?	č.
40	[number]	k4	40
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalé	bývalý	k2eAgNnSc1d1	bývalé
jezuitské	jezuitský	k2eAgNnSc1d1	jezuitské
kolegium	kolegium	k1gNnSc1	kolegium
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgNnSc1d1	dnešní
kolegium	kolegium	k1gNnSc1	kolegium
Broscianum	Broscianum	k1gInSc4	Broscianum
Jagelonské	jagelonský	k2eAgFnSc2d1	Jagelonská
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
č.	č.	k?	č.
52	[number]	k4	52
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
evangelický	evangelický	k2eAgInSc1d1	evangelický
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc2	Martin
(	(	kIx(	(
<g/>
č.	č.	k?	č.
58	[number]	k4	58
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dům	dům	k1gInSc4	dům
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
(	(	kIx(	(
<g/>
č.	č.	k?	č.
65	[number]	k4	65
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
arsenál	arsenál	k1gInSc4	arsenál
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
č.	č.	k?	č.
65	[number]	k4	65
<g/>
,	,	kIx,	,
vystavěn	vystavěn	k2eAgInSc1d1	vystavěn
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
králem	král	k1gMnSc7	král
Zikmundem	Zikmund	k1gMnSc7	Zikmund
I.	I.	kA	I.
Starým	Starý	k1gMnSc7	Starý
<g/>
)	)	kIx)	)
a	a	k8xC	a
malý	malý	k2eAgInSc1d1	malý
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jiljí	Jiljí	k1gMnPc2	Jiljí
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
kostel	kostel	k1gInSc4	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Ondřeje	Ondřej	k1gMnSc2	Ondřej
a	a	k8xC	a
klášter	klášter	k1gInSc1	klášter
sester	sestra	k1gFnPc2	sestra
klarisek	klariska	k1gFnPc2	klariska
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Grodzka	Grodzka	k1gFnSc1	Grodzka
č.	č.	k?	č.
56	[number]	k4	56
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
kostelům	kostel	k1gInPc3	kostel
Krakova	Krakov	k1gInSc2	Krakov
a	a	k8xC	a
nejhezčím	hezký	k2eAgInPc3d3	nejhezčí
románským	románský	k2eAgInPc3d1	románský
kostelům	kostel	k1gInPc3	kostel
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1079	[number]	k4	1079
až	až	k9	až
1098	[number]	k4	1098
díky	díky	k7c3	díky
Sieciehovi	Siecieh	k1gMnSc3	Siecieh
<g/>
,	,	kIx,	,
dvořanu	dvořan	k1gMnSc3	dvořan
knížete	kníže	k1gMnSc4	kníže
Vladislava	Vladislav	k1gMnSc2	Vladislav
I.	I.	kA	I.
Hermana	Herman	k1gMnSc2	Herman
<g/>
.	.	kIx.	.
</s>
<s>
Opevněný	opevněný	k2eAgInSc1d1	opevněný
kostel	kostel	k1gInSc1	kostel
(	(	kIx(	(
<g/>
silné	silný	k2eAgFnPc1d1	silná
zdi	zeď	k1gFnPc1	zeď
a	a	k8xC	a
střílny	střílna	k1gFnPc1	střílna
<g/>
)	)	kIx)	)
mohl	moct	k5eAaImAgInS	moct
odolat	odolat	k5eAaPmF	odolat
tatarskému	tatarský	k2eAgInSc3d1	tatarský
útoku	útok	k1gInSc3	útok
roku	rok	k1gInSc2	rok
1241	[number]	k4	1241
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
kamenná	kamenný	k2eAgFnSc1d1	kamenná
bazilika	bazilika	k1gFnSc1	bazilika
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
věžemi	věž	k1gFnPc7	věž
<g/>
,	,	kIx,	,
krátkým	krátký	k2eAgInSc7d1	krátký
transeptem	transept	k1gInSc7	transept
a	a	k8xC	a
presbytářem	presbytář	k1gInSc7	presbytář
s	s	k7c7	s
polokruhovou	polokruhový	k2eAgFnSc7d1	polokruhová
apsidou	apsida	k1gFnSc7	apsida
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
barokně	barokně	k6eAd1	barokně
rekonstruován	rekonstruován	k2eAgMnSc1d1	rekonstruován
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
pochází	pocházet	k5eAaImIp3nS	pocházet
štuková	štukový	k2eAgFnSc1d1	štuková
výzdoba	výzdoba	k1gFnSc1	výzdoba
od	od	k7c2	od
Balthasara	Balthasar	k1gMnSc2	Balthasar
Fontany	Fontan	k1gInPc4	Fontan
a	a	k8xC	a
nástěnné	nástěnný	k2eAgFnPc1d1	nástěnná
malby	malba	k1gFnPc1	malba
od	od	k7c2	od
Karla	Karel	k1gMnSc2	Karel
Dankwarta	Dankwart	k1gMnSc2	Dankwart
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
je	být	k5eAaImIp3nS	být
pozdně	pozdně	k6eAd1	pozdně
barokní	barokní	k2eAgFnSc1d1	barokní
<g/>
.	.	kIx.	.
</s>
<s>
Rokoková	rokokový	k2eAgFnSc1d1	rokoková
kazatelna	kazatelna	k1gFnSc1	kazatelna
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
člunu	člun	k1gInSc2	člun
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jihu	jih	k1gInSc2	jih
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
kostel	kostel	k1gInSc4	kostel
klášter	klášter	k1gInSc4	klášter
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
další	další	k2eAgNnPc4d1	další
umělecká	umělecký	k2eAgNnPc4d1	umělecké
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kanovnická	kanovnický	k2eAgFnSc1d1	Kanovnická
ulice	ulice	k1gFnSc1	ulice
(	(	kIx(	(
<g/>
Kanonicza	Kanonicza	k1gFnSc1	Kanonicza
<g/>
)	)	kIx)	)
–	–	k?	–
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejhezčím	hezký	k2eAgFnPc3d3	nejhezčí
ulicím	ulice	k1gFnPc3	ulice
krakovského	krakovský	k2eAgNnSc2d1	Krakovské
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
ležela	ležet	k5eAaImAgFnS	ležet
v	v	k7c6	v
osadě	osada	k1gFnSc6	osada
Okól	Okóla	k1gFnPc2	Okóla
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
leželo	ležet	k5eAaImAgNnS	ležet
mezi	mezi	k7c7	mezi
Krakovem	Krakov	k1gInSc7	Krakov
a	a	k8xC	a
Wawelem	Wawel	k1gInSc7	Wawel
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
přičleněno	přičleněn	k2eAgNnSc4d1	přičleněno
ke	k	k7c3	k
Krakovu	Krakov	k1gInSc3	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Ulice	ulice	k1gFnSc1	ulice
tvořila	tvořit	k5eAaImAgFnS	tvořit
poslední	poslední	k2eAgInSc4d1	poslední
díl	díl	k1gInSc4	díl
tzv.	tzv.	kA	tzv.
královské	královský	k2eAgFnSc2d1	královská
cesty	cesta	k1gFnSc2	cesta
a	a	k8xC	a
bydleli	bydlet	k5eAaImAgMnP	bydlet
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
kanovníci	kanovník	k1gMnPc1	kanovník
krakovské	krakovský	k2eAgFnSc2d1	Krakovská
katedrály	katedrála	k1gFnSc2	katedrála
na	na	k7c6	na
Wawelu	Wawel	k1gMnSc6	Wawel
a	a	k8xC	a
šlechtici	šlechtic	k1gMnSc6	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
Domy	dům	k1gInPc4	dům
pocházející	pocházející	k2eAgInPc4d1	pocházející
většinou	většinou	k6eAd1	většinou
ze	z	k7c2	z
středověku	středověk	k1gInSc2	středověk
mají	mít	k5eAaImIp3nP	mít
pěkné	pěkný	k2eAgFnPc1d1	pěkná
renesanční	renesanční	k2eAgFnPc1d1	renesanční
a	a	k8xC	a
barokní	barokní	k2eAgFnPc1d1	barokní
fasády	fasáda	k1gFnPc1	fasáda
<g/>
,	,	kIx,	,
bohatě	bohatě	k6eAd1	bohatě
dekorované	dekorovaný	k2eAgInPc4d1	dekorovaný
portály	portál	k1gInPc4	portál
a	a	k8xC	a
erby	erb	k1gInPc4	erb
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavý	zajímavý	k2eAgMnSc1d1	zajímavý
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
<g/>
)	)	kIx)	)
kapitulní	kapitulní	k2eAgInSc4d1	kapitulní
dům	dům	k1gInSc4	dům
číslo	číslo	k1gNnSc1	číslo
5	[number]	k4	5
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
šlechtický	šlechtický	k2eAgInSc1d1	šlechtický
dům	dům	k1gInSc1	dům
číslo	číslo	k1gNnSc1	číslo
6	[number]	k4	6
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
palác	palác	k1gInSc4	palác
Erazima	Erazim	k1gMnSc2	Erazim
Cioleka	Cioleek	k1gMnSc2	Cioleek
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc1	číslo
17	[number]	k4	17
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
filiálka	filiálka	k1gFnSc1	filiálka
Národního	národní	k2eAgNnSc2d1	národní
musea	museum	k1gNnSc2	museum
se	s	k7c7	s
sbírkou	sbírka	k1gFnSc7	sbírka
sakrálního	sakrální	k2eAgNnSc2d1	sakrální
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
arcidiecézní	arcidiecézní	k2eAgNnSc1d1	arcidiecézní
museum	museum	k1gNnSc1	museum
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc1	číslo
19	[number]	k4	19
a	a	k8xC	a
21	[number]	k4	21
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1951	[number]	k4	1951
až	až	k9	až
1963	[number]	k4	1963
bydlel	bydlet	k5eAaImAgMnS	bydlet
pozdější	pozdní	k2eAgMnSc1d2	pozdější
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
dům	dům	k1gInSc4	dům
významného	významný	k2eAgMnSc2d1	významný
kronikáře	kronikář	k1gMnSc2	kronikář
Jana	Jan	k1gMnSc2	Jan
Długosze	Długosze	k1gFnSc2	Długosze
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc1	číslo
25	[number]	k4	25
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Barbory	Barbora	k1gFnSc2	Barbora
(	(	kIx(	(
<g/>
Kościół	Kościół	k1gFnSc1	Kościół
św	św	k?	św
<g/>
.	.	kIx.	.
</s>
<s>
Barbary	Barbara	k1gFnPc1	Barbara
<g/>
,	,	kIx,	,
Maly	Maly	k?	Maly
rynek	rynek	k1gInSc1	rynek
č.	č.	k?	č.
8	[number]	k4	8
<g/>
)	)	kIx)	)
–	–	k?	–
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Mariánského	mariánský	k2eAgInSc2d1	mariánský
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1338	[number]	k4	1338
až	až	k9	až
1402	[number]	k4	1402
jako	jako	k8xC	jako
hřbitovní	hřbitovní	k2eAgFnSc2d1	hřbitovní
kaple	kaple	k1gFnSc2	kaple
<g/>
,	,	kIx,	,
hřbitova	hřbitov	k1gInSc2	hřbitov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nalézal	nalézat	k5eAaImAgMnS	nalézat
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
jednolodní	jednolodní	k2eAgInSc1d1	jednolodní
kostel	kostel	k1gInSc1	kostel
s	s	k7c7	s
půlkruhovou	půlkruhový	k2eAgFnSc7d1	půlkruhová
apsidou	apsida	k1gFnSc7	apsida
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
přestavby	přestavba	k1gFnSc2	přestavba
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
vstupu	vstup	k1gInSc2	vstup
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
předsíň	předsíň	k1gFnSc1	předsíň
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
Getsemanskou	getsemanský	k2eAgFnSc7d1	Getsemanská
zahradou	zahrada	k1gFnSc7	zahrada
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
kostela	kostel	k1gInSc2	kostel
jsou	být	k5eAaImIp3nP	být
početné	početný	k2eAgInPc1d1	početný
epitafy	epitaf	k1gInPc1	epitaf
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
je	být	k5eAaImIp3nS	být
barokní	barokní	k2eAgMnSc1d1	barokní
<g/>
.	.	kIx.	.
</s>
<s>
Stropní	stropní	k2eAgFnPc1d1	stropní
malby	malba	k1gFnPc1	malba
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
F.	F.	kA	F.
I.	I.	kA	I.
Molitora	Molitor	k1gMnSc2	Molitor
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
štuky	štuka	k1gFnPc4	štuka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1760	[number]	k4	1760
až	až	k6eAd1	až
1767	[number]	k4	1767
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodný	pozoruhodný	k2eAgMnSc1d1	pozoruhodný
je	být	k5eAaImIp3nS	být
gotický	gotický	k2eAgInSc1d1	gotický
krucifix	krucifix	k1gInSc4	krucifix
pocházející	pocházející	k2eAgInSc4d1	pocházející
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
umístěný	umístěný	k2eAgInSc1d1	umístěný
na	na	k7c6	na
hlavním	hlavní	k2eAgInSc6d1	hlavní
oltáři	oltář	k1gInSc6	oltář
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
dále	daleko	k6eAd2	daleko
barokní	barokní	k2eAgFnPc1d1	barokní
sochy	socha	k1gFnPc1	socha
<g/>
.	.	kIx.	.
</s>
<s>
Nalézá	nalézat	k5eAaImIp3nS	nalézat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
obraz	obraz	k1gInSc1	obraz
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc2d1	boží
z	z	k7c2	z
Jurowic	Jurowice	k1gFnPc2	Jurowice
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Collegium	Collegium	k1gNnSc1	Collegium
Maius	Maius	k1gMnSc1	Maius
–	–	k?	–
nejstarší	starý	k2eAgFnSc1d3	nejstarší
část	část	k1gFnSc1	část
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Jagiellońska	Jagiellońska	k1gFnSc1	Jagiellońska
č.	č.	k?	č.
15	[number]	k4	15
<g/>
)	)	kIx)	)
–	–	k?	–
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
univerzitní	univerzitní	k2eAgFnSc7d1	univerzitní
budovou	budova	k1gFnSc7	budova
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
byl	být	k5eAaImAgInS	být
věnován	věnován	k2eAgInSc1d1	věnován
roku	rok	k1gInSc2	rok
1400	[number]	k4	1400
králem	král	k1gMnSc7	král
Vladislavem	Vladislav	k1gMnSc7	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jagellem	Jagell	k1gInSc7	Jagell
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
univerzita	univerzita	k1gFnSc1	univerzita
úspěšně	úspěšně	k6eAd1	úspěšně
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
získány	získat	k5eAaPmNgFnP	získat
další	další	k2eAgFnPc1d1	další
budovy	budova	k1gFnPc1	budova
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
požárech	požár	k1gInPc6	požár
v	v	k7c6	v
letech	let	k1gInPc6	let
1462	[number]	k4	1462
a	a	k8xC	a
1492	[number]	k4	1492
byly	být	k5eAaImAgFnP	být
stavby	stavba	k1gFnPc1	stavba
spolu	spolu	k6eAd1	spolu
sjednoceny	sjednotit	k5eAaPmNgFnP	sjednotit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
arkádový	arkádový	k2eAgInSc1d1	arkádový
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
dvůr	dvůr	k1gInSc1	dvůr
se	s	k7c7	s
sloupovým	sloupový	k2eAgNnSc7d1	sloupové
s	s	k7c7	s
pozdněgotickou	pozdněgotický	k2eAgFnSc7d1	pozdněgotická
sklípkovou	sklípkový	k2eAgFnSc7d1	sklípková
klenbou	klenba	k1gFnSc7	klenba
a	a	k8xC	a
ochoz	ochoz	k1gInSc1	ochoz
v	v	k7c6	v
patře	patro	k1gNnSc6	patro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
čítárny	čítárna	k1gFnPc1	čítárna
<g/>
,	,	kIx,	,
v	v	k7c6	v
patře	patro	k1gNnSc6	patro
bibliotéka	bibliotéka	k1gFnSc1	bibliotéka
<g/>
,	,	kIx,	,
společné	společný	k2eAgInPc4d1	společný
prostory	prostor	k1gInPc4	prostor
<g/>
,	,	kIx,	,
aula	aula	k1gFnSc1	aula
a	a	k8xC	a
pokladnice	pokladnice	k1gFnSc1	pokladnice
<g/>
.	.	kIx.	.
</s>
<s>
Fasáda	fasáda	k1gFnSc1	fasáda
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Jagiellońska	Jagiellońska	k1gFnSc1	Jagiellońska
je	být	k5eAaImIp3nS	být
zdobena	zdobit	k5eAaImNgFnS	zdobit
gotickými	gotický	k2eAgInPc7d1	gotický
štíty	štít	k1gInPc7	štít
<g/>
,	,	kIx,	,
arkýřem	arkýř	k1gInSc7	arkýř
a	a	k8xC	a
gotickým	gotický	k2eAgInSc7d1	gotický
portálem	portál	k1gInSc7	portál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vede	vést	k5eAaImIp3nS	vést
do	do	k7c2	do
nádvoří	nádvoří	k1gNnSc2	nádvoří
se	s	k7c7	s
studnou	studna	k1gFnSc7	studna
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
kolegium	kolegium	k1gNnSc1	kolegium
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
univerzitní	univerzitní	k2eAgNnSc1d1	univerzitní
museum	museum	k1gNnSc1	museum
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Collegium	Collegium	k1gNnSc1	Collegium
Novum	novum	k1gNnSc1	novum
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Golebia	Golebia	k1gFnSc1	Golebia
č.	č.	k?	č.
24	[number]	k4	24
<g/>
)	)	kIx)	)
–	–	k?	–
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1883	[number]	k4	1883
až	až	k9	až
1887	[number]	k4	1887
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
studentských	studentský	k2eAgFnPc2d1	studentská
ubytoven	ubytovna	k1gFnPc2	ubytovna
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
novogotickou	novogotický	k2eAgFnSc4d1	novogotická
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dnes	dnes	k6eAd1	dnes
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
budova	budova	k1gFnSc1	budova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
rektorát	rektorát	k1gInSc4	rektorát
<g/>
,	,	kIx,	,
děkanáty	děkanát	k1gInPc4	děkanát
<g/>
,	,	kIx,	,
správa	správa	k1gFnSc1	správa
<g/>
,	,	kIx,	,
sborovny	sborovna	k1gFnPc1	sborovna
a	a	k8xC	a
čítárny	čítárna	k1gFnPc1	čítárna
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
shromážděno	shromáždit	k5eAaPmNgNnS	shromáždit
německými	německý	k2eAgInPc7d1	německý
nacionálními	nacionální	k2eAgInPc7d1	nacionální
socialisty	socialista	k1gMnSc2	socialista
na	na	k7c4	na
183	[number]	k4	183
profesorů	profesor	k1gMnPc2	profesor
a	a	k8xC	a
vědeckých	vědecký	k2eAgMnPc2d1	vědecký
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
krakovských	krakovský	k2eAgFnPc2d1	Krakovská
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
posláno	poslán	k2eAgNnSc1d1	Posláno
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Krakovský	krakovský	k2eAgInSc1d1	krakovský
rynek	rynek	k1gInSc1	rynek
===	===	k?	===
</s>
</p>
<p>
<s>
Mariánský	mariánský	k2eAgInSc1d1	mariánský
kostel	kostel	k1gInSc1	kostel
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
rohu	roh	k1gInSc6	roh
náměstí	náměstí	k1gNnSc2	náměstí
je	být	k5eAaImIp3nS	být
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
cihlová	cihlový	k2eAgFnSc1d1	cihlová
gotická	gotický	k2eAgFnSc1d1	gotická
bazilika	bazilika	k1gFnSc1	bazilika
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
věžemi	věž	k1gFnPc7	věž
v	v	k7c6	v
průčelí	průčelí	k1gNnSc6	průčelí
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dvou	dva	k4xCgFnPc2	dva
starších	starý	k2eAgFnPc2d2	starší
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
farním	farní	k2eAgInSc7d1	farní
kostelem	kostel	k1gInSc7	kostel
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
památku	památka	k1gFnSc4	památka
pověsti	pověst	k1gFnSc2	pověst
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
za	za	k7c2	za
tatarského	tatarský	k2eAgInSc2d1	tatarský
vpádu	vpád	k1gInSc2	vpád
zastřelen	zastřelen	k2eAgInSc1d1	zastřelen
trubač	trubač	k1gInSc1	trubač
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
troubil	troubit	k5eAaImAgInS	troubit
na	na	k7c4	na
poplach	poplach	k1gInSc4	poplach
<g/>
,	,	kIx,	,
troubí	troubit	k5eAaImIp3nS	troubit
se	se	k3xPyFc4	se
z	z	k7c2	z
věže	věž	k1gFnSc2	věž
každou	každý	k3xTgFnSc4	každý
hodinu	hodina	k1gFnSc4	hodina
tradiční	tradiční	k2eAgFnSc2d1	tradiční
melodie	melodie	k1gFnSc2	melodie
hejnal	hejnat	k5eAaImAgMnS	hejnat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostelík	kostelík	k1gInSc1	kostelík
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
kostel	kostel	k1gInSc1	kostel
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
snad	snad	k9	snad
kázal	kázat	k5eAaImAgMnS	kázat
sv.	sv.	kA	sv.
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
románský	románský	k2eAgInSc1d1	románský
jednolodní	jednolodní	k2eAgInSc1d1	jednolodní
kostel	kostel	k1gInSc1	kostel
se	s	k7c7	s
čtyřbokým	čtyřboký	k2eAgInSc7d1	čtyřboký
presbytářem	presbytář	k1gInSc7	presbytář
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
postaven	postavit	k5eAaPmNgInS	postavit
z	z	k7c2	z
kamenných	kamenný	k2eAgInPc2d1	kamenný
kvádrů	kvádr	k1gInPc2	kvádr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
časů	čas	k1gInPc2	čas
pochází	pocházet	k5eAaImIp3nS	pocházet
portál	portál	k1gInSc1	portál
a	a	k8xC	a
okno	okno	k1gNnSc1	okno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
barokně	barokně	k6eAd1	barokně
přestavěn	přestavěn	k2eAgInSc1d1	přestavěn
<g/>
,	,	kIx,	,
zdi	zeď	k1gFnPc1	zeď
byly	být	k5eAaImAgFnP	být
zvýšeny	zvýšit	k5eAaPmNgFnP	zvýšit
a	a	k8xC	a
kostel	kostel	k1gInSc1	kostel
dostal	dostat	k5eAaPmAgInS	dostat
novou	nový	k2eAgFnSc4d1	nová
kopuli	kopule	k1gFnSc4	kopule
a	a	k8xC	a
nový	nový	k2eAgInSc4d1	nový
vchod	vchod	k1gInSc4	vchod
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
byla	být	k5eAaImAgFnS	být
také	také	k9	také
sakristie	sakristie	k1gFnSc1	sakristie
a	a	k8xC	a
kaple	kaple	k1gFnSc1	kaple
blahoslaveného	blahoslavený	k2eAgMnSc2d1	blahoslavený
Wincenta	Wincent	k1gMnSc2	Wincent
Kadlubka	Kadlubek	k1gMnSc2	Kadlubek
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
je	být	k5eAaImIp3nS	být
pozoruhodný	pozoruhodný	k2eAgInSc1d1	pozoruhodný
gotický	gotický	k2eAgInSc1d1	gotický
krucifix	krucifix	k1gInSc1	krucifix
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
portrét	portrét	k1gInSc1	portrét
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
socha	socha	k1gFnSc1	socha
anděla	anděl	k1gMnSc2	anděl
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
kostelem	kostel	k1gInSc7	kostel
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
výstava	výstava	k1gFnSc1	výstava
Dějiny	dějiny	k1gFnPc1	dějiny
krakovského	krakovský	k2eAgNnSc2d1	Krakovské
tržiště	tržiště	k1gNnSc2	tržiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tržnice	tržnice	k1gFnSc1	tržnice
Sukiennice	Sukiennice	k1gFnSc2	Sukiennice
stojí	stát	k5eAaImIp3nS	stát
uprostřed	uprostřed	k7c2	uprostřed
rynku	rynek	k1gInSc2	rynek
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
symbolům	symbol	k1gInPc3	symbol
Krakova	Krakov	k1gInSc2	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
patrně	patrně	k6eAd1	patrně
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
v	v	k7c6	v
gotickém	gotický	k2eAgInSc6d1	gotický
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Zděná	zděný	k2eAgFnSc1d1	zděná
patrová	patrový	k2eAgFnSc1d1	patrová
budova	budova	k1gFnSc1	budova
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1556	[number]	k4	1556
<g/>
–	–	k?	–
<g/>
1559	[number]	k4	1559
<g/>
,	,	kIx,	,
108	[number]	k4	108
metrů	metr	k1gInPc2	metr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
řadami	řada	k1gFnPc7	řada
krámků	krámek	k1gInPc2	krámek
uvnitř	uvnitř	k6eAd1	uvnitř
a	a	k8xC	a
novogotickými	novogotický	k2eAgFnPc7d1	novogotická
arkádami	arkáda	k1gFnPc7	arkáda
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
delších	dlouhý	k2eAgFnPc6d2	delší
stranách	strana	k1gFnPc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
patře	patro	k1gNnSc6	patro
je	být	k5eAaImIp3nS	být
expozice	expozice	k1gFnSc1	expozice
polského	polský	k2eAgNnSc2d1	polské
malířství	malířství	k1gNnSc2	malířství
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Radniční	radniční	k2eAgFnSc1d1	radniční
věž	věž	k1gFnSc1	věž
je	být	k5eAaImIp3nS	být
čtyřhranná	čtyřhranný	k2eAgFnSc1d1	čtyřhranná
<g/>
,	,	kIx,	,
70	[number]	k4	70
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
cihel	cihla	k1gFnPc2	cihla
a	a	k8xC	a
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
vstup	vstup	k1gInSc4	vstup
střeží	střežit	k5eAaImIp3nP	střežit
dva	dva	k4xCgMnPc1	dva
lvi	lev	k1gMnPc1	lev
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
je	být	k5eAaImIp3nS	být
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
později	pozdě	k6eAd2	pozdě
zbořené	zbořený	k2eAgFnSc2d1	zbořená
radnice	radnice	k1gFnSc2	radnice
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pomník	pomník	k1gInSc1	pomník
básníka	básník	k1gMnSc2	básník
Adama	Adam	k1gMnSc2	Adam
Mickiewicze	Mickiewicze	k1gFnSc2	Mickiewicze
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
náměstí	náměstí	k1gNnSc6	náměstí
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
Teodorem	Teodor	k1gMnSc7	Teodor
Rygierem	Rygier	k1gMnSc7	Rygier
a	a	k8xC	a
odhalen	odhalen	k2eAgInSc1d1	odhalen
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
ke	k	k7c3	k
stému	stý	k4xOgNnSc3	stý
výročí	výročí	k1gNnSc3	výročí
narození	narození	k1gNnSc2	narození
básníka	básník	k1gMnSc2	básník
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
pomník	pomník	k1gInSc4	pomník
zničen	zničen	k2eAgInSc4d1	zničen
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
ke	k	k7c3	k
stému	stý	k4xOgNnSc3	stý
výročí	výročí	k1gNnSc3	výročí
básníkova	básníkův	k2eAgNnSc2d1	básníkovo
úmrtí	úmrtí	k1gNnSc2	úmrtí
znovu	znovu	k6eAd1	znovu
odhalen	odhalit	k5eAaPmNgMnS	odhalit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kazimierz	Kazimierz	k1gInSc4	Kazimierz
===	===	k?	===
</s>
</p>
<p>
<s>
Kazimierz	Kazimierz	k1gInSc1	Kazimierz
je	být	k5eAaImIp3nS	být
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Starého	Starého	k2eAgNnSc2d1	Starého
města	město	k1gNnSc2	město
mezi	mezi	k7c7	mezi
Wawelem	Wawel	k1gInSc7	Wawel
a	a	k8xC	a
Vislou	Visla	k1gFnSc7	Visla
<g/>
,	,	kIx,	,
tradiční	tradiční	k2eAgFnSc1d1	tradiční
židovská	židovský	k2eAgFnSc1d1	židovská
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starších	starý	k2eAgFnPc6d2	starší
dobách	doba	k1gFnPc6	doba
měl	mít	k5eAaImAgInS	mít
vlastní	vlastní	k2eAgFnPc4d1	vlastní
hradby	hradba	k1gFnPc4	hradba
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
oddělen	oddělit	k5eAaPmNgInS	oddělit
od	od	k7c2	od
města	město	k1gNnSc2	město
ramenem	rameno	k1gNnSc7	rameno
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
zasypaným	zasypaný	k2eAgMnPc3d1	zasypaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ulice	ulice	k1gFnSc1	ulice
Szeroka	Szeroka	k1gFnSc1	Szeroka
–	–	k?	–
byla	být	k5eAaImAgFnS	být
dříve	dříve	k6eAd2	dříve
středobodem	středobod	k1gInSc7	středobod
židovského	židovský	k2eAgNnSc2d1	Židovské
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
tržištěm	tržiště	k1gNnSc7	tržiště
<g/>
.	.	kIx.	.
</s>
<s>
Zachovány	zachován	k2eAgInPc1d1	zachován
jsou	být	k5eAaImIp3nP	být
četné	četný	k2eAgInPc4d1	četný
historické	historický	k2eAgInPc4d1	historický
objekty	objekt	k1gInPc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
některých	některý	k3yIgInPc2	některý
zde	zde	k6eAd1	zde
uvedených	uvedený	k2eAgInPc2d1	uvedený
jde	jít	k5eAaImIp3nS	jít
i	i	k9	i
o	o	k7c4	o
rituální	rituální	k2eAgFnSc4d1	rituální
židovskou	židovský	k2eAgFnSc4d1	židovská
lázeň	lázeň	k1gFnSc4	lázeň
(	(	kIx(	(
<g/>
mikve	mikvat	k5eAaPmIp3nS	mikvat
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
č.	č.	k?	č.
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
renesance	renesance	k1gFnSc2	renesance
pocházející	pocházející	k2eAgInSc1d1	pocházející
Landauův	Landauův	k2eAgInSc1d1	Landauův
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
č.	č.	k?	č.
2	[number]	k4	2
<g/>
)	)	kIx)	)
s	s	k7c7	s
fasádou	fasáda	k1gFnSc7	fasáda
z	z	k7c2	z
cihel	cihla	k1gFnPc2	cihla
a	a	k8xC	a
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
ulice	ulice	k1gFnSc2	ulice
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
kdysi	kdysi	k6eAd1	kdysi
malý	malý	k2eAgInSc4d1	malý
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
část	část	k1gFnSc1	část
hřbitova	hřbitov	k1gInSc2	hřbitov
Remuh	Remuha	k1gFnPc2	Remuha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Synagoga	synagoga	k1gFnSc1	synagoga
Wolfa	Wolf	k1gMnSc2	Wolf
Bociana	Bocian	k1gMnSc2	Bocian
(	(	kIx(	(
<g/>
Poppera	Popper	k1gMnSc2	Popper
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Szeroka	Szeroka	k1gFnSc1	Szeroka
16	[number]	k4	16
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1620	[number]	k4	1620
</s>
</p>
<p>
<s>
Stará	starý	k2eAgFnSc1d1	stará
synagoga	synagoga	k1gFnSc1	synagoga
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Szeroka	Szeroka	k1gFnSc1	Szeroka
24	[number]	k4	24
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
cihlová	cihlový	k2eAgFnSc1d1	cihlová
pozdně	pozdně	k6eAd1	pozdně
gotická	gotický	k2eAgFnSc1d1	gotická
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
přestavěná	přestavěný	k2eAgFnSc1d1	přestavěná
v	v	k7c6	v
letech	let	k1gInPc6	let
1533	[number]	k4	1533
<g/>
–	–	k?	–
<g/>
1570	[number]	k4	1570
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
městského	městský	k2eAgNnSc2d1	Městské
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Synagoga	synagoga	k1gFnSc1	synagoga
Remuh	Remuha	k1gFnPc2	Remuha
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Szeroka	Szeroka	k1gFnSc1	Szeroka
č.	č.	k?	č.
40	[number]	k4	40
<g/>
)	)	kIx)	)
–	–	k?	–
dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
slouží	sloužit	k5eAaImIp3nS	sloužit
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Renesanční	renesanční	k2eAgFnSc4d1	renesanční
synagogu	synagoga	k1gFnSc4	synagoga
založil	založit	k5eAaPmAgMnS	založit
uprostřed	uprostřed	k7c2	uprostřed
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
Israel	Israela	k1gFnPc2	Israela
Auerbach	Auerbacha	k1gFnPc2	Auerbacha
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gMnSc6	jeho
synu	syn	k1gMnSc6	syn
filozofovi	filozof	k1gMnSc6	filozof
a	a	k8xC	a
rabínu	rabín	k1gMnSc3	rabín
Mosesu	Moses	k1gInSc2	Moses
Isserlesovi	Isserles	k1gMnSc3	Isserles
získala	získat	k5eAaPmAgFnS	získat
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
stavba	stavba	k1gFnSc1	stavba
roku	rok	k1gInSc2	rok
1557	[number]	k4	1557
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
však	však	k9	však
znovu	znovu	k6eAd1	znovu
rychle	rychle	k6eAd1	rychle
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
<g/>
.	.	kIx.	.
</s>
<s>
Důkladně	důkladně	k6eAd1	důkladně
restaurována	restaurován	k2eAgFnSc1d1	restaurována
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1829	[number]	k4	1829
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
a	a	k8xC	a
vyrabována	vyrabovat	k5eAaPmNgFnS	vyrabovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřek	vnitřek	k1gInSc1	vnitřek
je	být	k5eAaImIp3nS	být
pravoúhlý	pravoúhlý	k2eAgInSc1d1	pravoúhlý
a	a	k8xC	a
jednolodní	jednolodní	k2eAgInSc1d1	jednolodní
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
renesanční	renesanční	k2eAgFnSc1d1	renesanční
oltářní	oltářní	k2eAgFnSc1d1	oltářní
skříňka	skříňka	k1gFnSc1	skříňka
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k6eAd1	uprostřed
je	být	k5eAaImIp3nS	být
rekonstruovaná	rekonstruovaný	k2eAgFnSc1d1	rekonstruovaná
bema	bema	k1gFnSc1	bema
s	s	k7c7	s
umělecky	umělecky	k6eAd1	umělecky
zpracovanou	zpracovaný	k2eAgFnSc7d1	zpracovaná
mříží	mříž	k1gFnSc7	mříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hřbitov	hřbitov	k1gInSc1	hřbitov
Remu	Remus	k1gInSc2	Remus
(	(	kIx(	(
<g/>
Starý	starý	k2eAgInSc1d1	starý
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
)	)	kIx)	)
–	–	k?	–
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgInSc7d3	nejstarší
židovským	židovský	k2eAgInSc7d1	židovský
hřbitovem	hřbitov	k1gInSc7	hřbitov
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1533	[number]	k4	1533
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
řada	řada	k1gFnSc1	řada
historických	historický	k2eAgInPc2d1	historický
náhrobků	náhrobek	k1gInPc2	náhrobek
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc3d3	veliký
úctě	úcta	k1gFnSc3	úcta
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
hrob	hrob	k1gInSc1	hrob
Mosese	Mosese	k1gFnSc2	Mosese
Isserlese	Isserlese	k1gFnSc2	Isserlese
<g/>
,	,	kIx,	,
řečeného	řečený	k2eAgMnSc4d1	řečený
"	"	kIx"	"
<g/>
Remu	Rema	k1gMnSc4	Rema
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Synagoga	synagoga	k1gFnSc1	synagoga
Izaaka	Izaak	k1gMnSc2	Izaak
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Kupa	kupa	k1gFnSc1	kupa
č.	č.	k?	č.
16	[number]	k4	16
<g/>
)	)	kIx)	)
–	–	k?	–
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
uprostřed	uprostřed	k7c2	uprostřed
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
obchodníkem	obchodník	k1gMnSc7	obchodník
Izaakem	Izaak	k1gMnSc7	Izaak
Jakubowiczem	Jakubowicz	k1gMnSc7	Jakubowicz
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
krakovská	krakovský	k2eAgFnSc1d1	Krakovská
synagoga	synagoga	k1gFnSc1	synagoga
je	být	k5eAaImIp3nS	být
impozantní	impozantní	k2eAgFnSc7d1	impozantní
barokní	barokní	k2eAgFnSc7d1	barokní
stavbou	stavba	k1gFnSc7	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Vchod	vchod	k1gInSc1	vchod
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
arkádovém	arkádový	k2eAgInSc6d1	arkádový
portálu	portál	k1gInSc6	portál
<g/>
,	,	kIx,	,
vnitřek	vnitřek	k1gInSc1	vnitřek
je	být	k5eAaImIp3nS	být
vyzdoben	vyzdobit	k5eAaPmNgInS	vyzdobit
barokními	barokní	k2eAgInPc7d1	barokní
štuky	štuk	k1gInPc7	štuk
a	a	k8xC	a
nápisy	nápis	k1gInPc1	nápis
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Synagoga	synagoga	k1gFnSc1	synagoga
Kupa	kupa	k1gFnSc1	kupa
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Warszauera	Warszauera	k1gFnSc1	Warszauera
č.	č.	k?	č.
8	[number]	k4	8
<g/>
)	)	kIx)	)
–	–	k?	–
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1643	[number]	k4	1643
díky	díky	k7c3	díky
nadaci	nadace	k1gFnSc3	nadace
Kahal	Kahal	k1gInSc4	Kahal
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Špitální	špitální	k2eAgFnSc1d1	špitální
synagoga	synagoga	k1gFnSc1	synagoga
či	či	k8xC	či
Synagogu	synagoga	k1gFnSc4	synagoga
chudých	chudý	k2eAgMnPc2d1	chudý
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodná	pozoruhodný	k2eAgFnSc1d1	pozoruhodná
je	být	k5eAaImIp3nS	být
oltářní	oltářní	k2eAgFnSc1d1	oltářní
skříňka	skříňka	k1gFnSc1	skříňka
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
jedinečné	jedinečný	k2eAgFnPc1d1	jedinečná
nástěnné	nástěnný	k2eAgFnPc1d1	nástěnná
a	a	k8xC	a
stropní	stropní	k2eAgFnPc1d1	stropní
dekorace	dekorace	k1gFnPc1	dekorace
představující	představující	k2eAgFnPc1d1	představující
města	město	k1gNnPc4	město
Svaté	svatý	k2eAgFnPc4d1	svatá
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
hvězdy	hvězda	k1gFnPc4	hvězda
a	a	k8xC	a
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
nacionálně	nacionálně	k6eAd1	nacionálně
socialistické	socialistický	k2eAgFnSc2d1	socialistická
okupace	okupace	k1gFnSc2	okupace
bylo	být	k5eAaImAgNnS	být
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
vybavení	vybavení	k1gNnSc1	vybavení
zničeno	zničen	k2eAgNnSc1d1	zničeno
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
synagoga	synagoga	k1gFnSc1	synagoga
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
místo	místo	k7c2	místo
setkávání	setkávání	k1gNnSc2	setkávání
<g/>
,	,	kIx,	,
koncertní	koncertní	k2eAgInSc4d1	koncertní
a	a	k8xC	a
výstavní	výstavní	k2eAgInSc4d1	výstavní
sál	sál	k1gInSc4	sál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tempelská	Tempelský	k2eAgFnSc1d1	Tempelský
synagoga	synagoga	k1gFnSc1	synagoga
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
bohoslužby	bohoslužba	k1gFnPc1	bohoslužba
</s>
</p>
<p>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
synagoga	synagoga	k1gFnSc1	synagoga
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
"	"	kIx"	"
<g/>
Synagoga	synagoga	k1gFnSc1	synagoga
Wysoka	Wysoka	k1gFnSc1	Wysoka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Józefa	Józef	k1gMnSc2	Józef
č.	č.	k?	č.
38	[number]	k4	38
<g/>
))	))	k?	))
–	–	k?	–
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
o	o	k7c4	o
něco	něco	k3yInSc4	něco
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
její	její	k3xOp3gFnSc1	její
údajná	údajný	k2eAgFnSc1d1	údajná
kopie	kopie	k1gFnSc1	kopie
<g/>
,	,	kIx,	,
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
synagoga	synagoga	k1gFnSc1	synagoga
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
věže	věž	k1gFnSc2	věž
židovského	židovský	k2eAgNnSc2d1	Židovské
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
monumentální	monumentální	k2eAgFnSc4d1	monumentální
renesanční	renesanční	k2eAgFnSc4d1	renesanční
synagogu	synagoga	k1gFnSc4	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jihu	jih	k1gInSc2	jih
je	být	k5eAaImIp3nS	být
zesílena	zesílit	k5eAaPmNgFnS	zesílit
čtyřmi	čtyři	k4xCgInPc7	čtyři
mocnými	mocný	k2eAgInPc7d1	mocný
opěrnými	opěrný	k2eAgInPc7d1	opěrný
pilíři	pilíř	k1gInPc7	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
je	být	k5eAaImIp3nS	být
zachováno	zachovat	k5eAaPmNgNnS	zachovat
jen	jen	k9	jen
několik	několik	k4yIc1	několik
hodnotných	hodnotný	k2eAgInPc2d1	hodnotný
elementů	element	k1gInPc2	element
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
:	:	kIx,	:
kamenné	kamenný	k2eAgNnSc1d1	kamenné
rámování	rámování	k1gNnSc1	rámování
oltářní	oltářní	k2eAgFnSc2d1	oltářní
skříňky	skříňka	k1gFnSc2	skříňka
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
nástěnných	nástěnný	k2eAgFnPc2d1	nástěnná
maleb	malba	k1gFnPc2	malba
s	s	k7c7	s
modlitebními	modlitební	k2eAgInPc7d1	modlitební
texty	text	k1gInPc7	text
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pořádají	pořádat	k5eAaImIp3nP	pořádat
setkání	setkání	k1gNnPc1	setkání
<g/>
,	,	kIx,	,
koncerty	koncert	k1gInPc1	koncert
a	a	k8xC	a
výstavy	výstava	k1gFnPc1	výstava
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc4d1	nový
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
</s>
</p>
<p>
<s>
===	===	k?	===
Mohyly	mohyla	k1gFnPc1	mohyla
a	a	k8xC	a
parky	park	k1gInPc1	park
===	===	k?	===
</s>
</p>
<p>
<s>
Kościuszkova	Kościuszkův	k2eAgFnSc1d1	Kościuszkův
mohyla	mohyla	k1gFnSc1	mohyla
(	(	kIx(	(
<g/>
Kopiec	Kopiec	k1gInSc1	Kopiec
Kościuszki	Kościuszk	k1gFnSc2	Kościuszk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Krakova	krakův	k2eAgFnSc1d1	Krakova
mohyla	mohyla	k1gFnSc1	mohyla
(	(	kIx(	(
<g/>
Kopiec	Kopiec	k1gInSc1	Kopiec
Krakusa	Krakus	k1gMnSc2	Krakus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Piłsudského	Piłsudský	k2eAgInSc2d1	Piłsudský
mohyla	mohyla	k1gFnSc1	mohyla
(	(	kIx(	(
<g/>
Kopiec	Kopiec	k1gMnSc1	Kopiec
Piłsudskiego	Piłsudskiego	k1gMnSc1	Piłsudskiego
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wandina	Wandin	k2eAgFnSc1d1	Wandin
mohyla	mohyla	k1gFnSc1	mohyla
(	(	kIx(	(
<g/>
Kopiec	Kopiec	k1gInSc1	Kopiec
Wandy	Wanda	k1gFnSc2	Wanda
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Planty	planta	k1gFnPc1	planta
park	park	k1gInSc1	park
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1822	[number]	k4	1822
až	až	k9	až
1830	[number]	k4	1830
díky	díky	k7c3	díky
Feliksi	Feliks	k1gMnSc3	Feliks
Radwanskému	Radwanský	k2eAgMnSc3d1	Radwanský
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
stržených	stržený	k2eAgFnPc2d1	stržená
středověkých	středověký	k2eAgFnPc2d1	středověká
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Definitivní	definitivní	k2eAgFnSc4d1	definitivní
podobu	podoba	k1gFnSc4	podoba
jim	on	k3xPp3gMnPc3	on
dal	dát	k5eAaPmAgMnS	dát
Florian	Florian	k1gMnSc1	Florian
Strawszewski	Strawszewsk	k1gFnSc2	Strawszewsk
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
získaly	získat	k5eAaPmAgInP	získat
po	po	k7c6	po
polském	polský	k2eAgInSc6d1	polský
výrazu	výraz	k1gInSc6	výraz
"	"	kIx"	"
<g/>
plantovat	plantovat	k5eAaImF	plantovat
<g/>
"	"	kIx"	"
čili	čili	k8xC	čili
rovnat	rovnat	k5eAaImF	rovnat
<g/>
.	.	kIx.	.
</s>
<s>
Planty	planta	k1gFnPc1	planta
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
celé	celý	k2eAgNnSc4d1	celé
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
plochu	plocha	k1gFnSc4	plocha
21	[number]	k4	21
hektarů	hektar	k1gInPc2	hektar
a	a	k8xC	a
délku	délka	k1gFnSc4	délka
4	[number]	k4	4
kilometry	kilometr	k1gInPc4	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
do	do	k7c2	do
osmi	osm	k4xCc2	osm
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Plantech	Plant	k1gInPc6	Plant
stojí	stát	k5eAaImIp3nS	stát
vícero	vícero	k1gNnSc1	vícero
pomníků	pomník	k1gInPc2	pomník
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
fontány	fontána	k1gFnPc1	fontána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Błonia	Błonium	k1gNnPc1	Błonium
</s>
</p>
<p>
<s>
===	===	k?	===
Památky	památka	k1gFnPc1	památka
podle	podle	k7c2	podle
slohů	sloh	k1gInPc2	sloh
===	===	k?	===
</s>
</p>
<p>
<s>
Gotický	gotický	k2eAgInSc1d1	gotický
KrakovKatedrála	KrakovKatedrál	k1gMnSc2	KrakovKatedrál
na	na	k7c4	na
Wawelu	Wawela	k1gFnSc4	Wawela
</s>
</p>
<p>
<s>
Mariánský	mariánský	k2eAgInSc1d1	mariánský
kostel	kostel	k1gInSc1	kostel
(	(	kIx(	(
<g/>
Rynek	rynek	k1gInSc1	rynek
glowny	glowna	k1gFnSc2	glowna
<g/>
)	)	kIx)	)
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
každou	každý	k3xTgFnSc4	každý
hodinu	hodina	k1gFnSc4	hodina
zní	znět	k5eAaImIp3nS	znět
hejnal	hejnat	k5eAaImAgMnS	hejnat
</s>
</p>
<p>
<s>
Středověké	středověký	k2eAgNnSc1d1	středověké
městské	městský	k2eAgNnSc1d1	Městské
opevnění	opevnění	k1gNnSc1	opevnění
–	–	k?	–
hradby	hradba	k1gFnPc1	hradba
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
opevnění	opevnění	k1gNnSc2	opevnění
byl	být	k5eAaImAgInS	být
vícekrát	vícekrát	k6eAd1	vícekrát
modernizován	modernizovat	k5eAaBmNgInS	modernizovat
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
začátku	začátek	k1gInSc2	začátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
zdvojen	zdvojen	k2eAgMnSc1d1	zdvojen
<g/>
.	.	kIx.	.
</s>
<s>
Hradby	hradba	k1gFnSc2	hradba
byly	být	k5eAaImAgInP	být
tři	tři	k4xCgInPc1	tři
kilometry	kilometr	k1gInPc1	kilometr
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
a	a	k8xC	a
měly	mít	k5eAaImAgFnP	mít
39	[number]	k4	39
bašt	bašta	k1gFnPc2	bašta
a	a	k8xC	a
osm	osm	k4xCc1	osm
bran	brána	k1gFnPc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
nimi	on	k3xPp3gInPc7	on
byl	být	k5eAaImAgInS	být
6	[number]	k4	6
až	až	k9	až
10	[number]	k4	10
metrů	metr	k1gInPc2	metr
široký	široký	k2eAgMnSc1d1	široký
a	a	k8xC	a
3,5	[number]	k4	3,5
metrů	metr	k1gInPc2	metr
hluboký	hluboký	k2eAgInSc4d1	hluboký
příkop	příkop	k1gInSc4	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
hradba	hradba	k1gFnSc1	hradba
byla	být	k5eAaImAgFnS	být
2,5	[number]	k4	2,5
metru	metr	k1gInSc2	metr
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
sedm	sedm	k4xCc4	sedm
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
hradeb	hradba	k1gFnPc2	hradba
stržena	strhnout	k5eAaPmNgFnS	strhnout
<g/>
.	.	kIx.	.
</s>
<s>
Zbyl	zbýt	k5eAaPmAgMnS	zbýt
úsek	úsek	k1gInSc4	úsek
u	u	k7c2	u
Florianské	Florianský	k2eAgFnSc2d1	Florianský
brány	brána	k1gFnSc2	brána
a	a	k8xC	a
Barbakanu	Barbakan	k1gInSc2	Barbakan
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
a	a	k8xC	a
klášter	klášter	k1gInSc1	klášter
Dominikánů	dominikán	k1gMnPc2	dominikán
(	(	kIx(	(
<g/>
Bazilika	bazilika	k1gFnSc1	bazilika
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
trojice	trojice	k1gFnSc2	trojice
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnSc2	ulice
Stolarska	Stolarsk	k1gInSc2	Stolarsk
č.	č.	k?	č.
12	[number]	k4	12
<g/>
)	)	kIx)	)
–	–	k?	–
první	první	k4xOgMnSc1	první
románský	románský	k2eAgInSc1d1	románský
kostel	kostel	k1gInSc1	kostel
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
byl	být	k5eAaImAgInS	být
dominikánům	dominikán	k1gMnPc3	dominikán
předán	předat	k5eAaPmNgInS	předat
krakovským	krakovský	k2eAgMnSc7d1	krakovský
biskupem	biskup	k1gMnSc7	biskup
Iwo	Iwo	k1gMnSc7	Iwo
Odrowazem	Odrowaz	k1gMnSc7	Odrowaz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
řád	řád	k1gInSc1	řád
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1221	[number]	k4	1221
pozval	pozvat	k5eAaPmAgMnS	pozvat
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc4d1	nový
kostel	kostel	k1gInSc4	kostel
a	a	k8xC	a
klášter	klášter	k1gInSc4	klášter
vystavěli	vystavět	k5eAaPmAgMnP	vystavět
dominikáni	dominikán	k1gMnPc1	dominikán
po	po	k7c6	po
mongolském	mongolský	k2eAgInSc6d1	mongolský
vpádu	vpád	k1gInSc6	vpád
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1241	[number]	k4	1241
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
trojlodní	trojlodní	k2eAgInSc1d1	trojlodní
halový	halový	k2eAgInSc1d1	halový
kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přeměněn	přeměnit	k5eAaPmNgInS	přeměnit
na	na	k7c4	na
baziliku	bazilika	k1gFnSc4	bazilika
<g/>
.	.	kIx.	.
</s>
<s>
Restaurován	restaurován	k2eAgMnSc1d1	restaurován
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Trojlodí	trojlodí	k1gNnSc1	trojlodí
kostela	kostel	k1gInSc2	kostel
má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc1	pět
klenebních	klenební	k2eAgFnPc2d1	klenební
polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
lemován	lemovat	k5eAaImNgInS	lemovat
řadou	řada	k1gFnSc7	řada
kaplí	kaple	k1gFnPc2	kaple
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
věnovány	věnovat	k5eAaImNgInP	věnovat
bohatými	bohatý	k2eAgFnPc7d1	bohatá
rodinami	rodina	k1gFnPc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
pozoruhodná	pozoruhodný	k2eAgFnSc1d1	pozoruhodná
je	být	k5eAaImIp3nS	být
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Hyacinta	hyacinta	k1gFnSc1	hyacinta
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novogotické	novogotický	k2eAgFnSc6d1	novogotická
předsíni	předsíň	k1gFnSc6	předsíň
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
portál	portál	k1gInSc1	portál
z	z	k7c2	z
konce	konec	k1gInSc2	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
ke	k	k7c3	k
kostelu	kostel	k1gInSc3	kostel
přimyká	přimykat	k5eAaImIp3nS	přimykat
klášter	klášter	k1gInSc1	klášter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
křížové	křížový	k2eAgFnSc6d1	křížová
chodbě	chodba	k1gFnSc6	chodba
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc3	století
jsou	být	k5eAaImIp3nP	být
zachovány	zachovat	k5eAaPmNgInP	zachovat
četně	četně	k6eAd1	četně
epitafy	epitaf	k1gInPc1	epitaf
<g/>
,	,	kIx,	,
náhrobky	náhrobek	k1gInPc1	náhrobek
<g/>
,	,	kIx,	,
pomníky	pomník	k1gInPc1	pomník
a	a	k8xC	a
malby	malba	k1gFnPc1	malba
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Františka	František	k1gMnSc4	František
z	z	k7c2	z
Assisi	Assise	k1gFnSc4	Assise
a	a	k8xC	a
klášter	klášter	k1gInSc4	klášter
františkánůGotické	františkánůGotický	k2eAgInPc1d1	františkánůGotický
kostely	kostel	k1gInPc1	kostel
na	na	k7c4	na
KazimierzuKostel	KazimierzuKostel	k1gInSc4	KazimierzuKostel
sv.	sv.	kA	sv.
Kateřiny	Kateřina	k1gFnSc2	Kateřina
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Augustiańska	Augustiańska	k1gFnSc1	Augustiańska
7	[number]	k4	7
<g/>
)	)	kIx)	)
–	–	k?	–
je	být	k5eAaImIp3nS	být
kopií	kopie	k1gFnSc7	kopie
stavby	stavba	k1gFnSc2	stavba
Mariánského	mariánský	k2eAgInSc2d1	mariánský
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc3	století
díky	díky	k7c3	díky
králi	král	k1gMnSc3	král
Kazimíru	Kazimír	k1gMnSc3	Kazimír
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Velikému	veliký	k2eAgMnSc3d1	veliký
pro	pro	k7c4	pro
augustiniány	augustinián	k1gMnPc4	augustinián
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
roku	rok	k1gInSc2	rok
1343	[number]	k4	1343
do	do	k7c2	do
Krakova	Krakov	k1gInSc2	Krakov
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
kostela	kostel	k1gInSc2	kostel
jsou	být	k5eAaImIp3nP	být
poznamenány	poznamenat	k5eAaPmNgInP	poznamenat
četnými	četný	k2eAgFnPc7d1	četná
fatálními	fatální	k2eAgFnPc7d1	fatální
událostmi	událost	k1gFnPc7	událost
a	a	k8xC	a
katastrofami	katastrofa	k1gFnPc7	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnSc2d1	velká
škody	škoda	k1gFnSc2	škoda
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
například	například	k6eAd1	například
během	během	k7c2	během
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
roku	rok	k1gInSc2	rok
1443	[number]	k4	1443
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
povodněmi	povodeň	k1gFnPc7	povodeň
a	a	k8xC	a
třemi	tři	k4xCgInPc7	tři
požáry	požár	k1gInPc7	požár
v	v	k7c4	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
atd.	atd.	kA	atd.
Během	během	k7c2	během
švédské	švédský	k2eAgFnSc2d1	švédská
okupace	okupace	k1gFnSc2	okupace
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
zde	zde	k6eAd1	zde
rakouská	rakouský	k2eAgNnPc1d1	rakouské
vojska	vojsko	k1gNnPc1	vojsko
měla	mít	k5eAaImAgNnP	mít
sklad	sklad	k1gInSc4	sklad
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
později	pozdě	k6eAd2	pozdě
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
sklad	sklad	k1gInSc1	sklad
slámy	sláma	k1gFnSc2	sláma
a	a	k8xC	a
sena	seno	k1gNnSc2	seno
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
dokonce	dokonce	k9	dokonce
stržen	stržen	k2eAgMnSc1d1	stržen
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgInSc2	ten
byl	být	k5eAaImAgMnS	být
však	však	k9	však
renovován	renovován	k2eAgMnSc1d1	renovován
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
třílodní	třílodní	k2eAgFnSc4d1	třílodní
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
příkladů	příklad	k1gInPc2	příklad
gotické	gotický	k2eAgFnSc2d1	gotická
architektury	architektura	k1gFnSc2	architektura
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
augustiniánský	augustiniánský	k2eAgInSc1d1	augustiniánský
klášter	klášter	k1gInSc1	klášter
s	s	k7c7	s
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
dvorem	dvůr	k1gInSc7	dvůr
a	a	k8xC	a
křížovou	křížový	k2eAgFnSc7d1	křížová
klenbou	klenba	k1gFnSc7	klenba
s	s	k7c7	s
hodnotnými	hodnotný	k2eAgFnPc7d1	hodnotná
freskami	freska	k1gFnPc7	freska
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
pozdněgotická	pozdněgotický	k2eAgFnSc1d1	pozdněgotická
kostelní	kostelní	k2eAgFnSc1d1	kostelní
předsíň	předsíň	k1gFnSc1	předsíň
s	s	k7c7	s
bohatými	bohatý	k2eAgFnPc7d1	bohatá
dekoracemi	dekorace	k1gFnPc7	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
Presbytář	presbytář	k1gInSc1	presbytář
má	mít	k5eAaImIp3nS	mít
hvězdicovou	hvězdicový	k2eAgFnSc4d1	hvězdicová
klenbu	klenba	k1gFnSc4	klenba
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
presbytáři	presbytář	k1gInSc6	presbytář
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
pěkně	pěkně	k6eAd1	pěkně
dekorované	dekorovaný	k2eAgFnPc1d1	dekorovaná
novogotické	novogotický	k2eAgFnPc1d1	novogotická
chórové	chórový	k2eAgFnPc1d1	chórová
lavice	lavice	k1gFnPc1	lavice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
lodi	loď	k1gFnSc6	loď
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
renesanční	renesanční	k2eAgNnSc1d1	renesanční
mausoleum	mausoleum	k1gNnSc1	mausoleum
Wawrzyniece	Wawrzyniece	k1gMnSc2	Wawrzyniece
Spyteka	Spyteek	k1gMnSc2	Spyteek
Jordana	Jordan	k1gMnSc2	Jordan
od	od	k7c2	od
Santi	Sanť	k1gFnSc2	Sanť
Gucciho	Gucci	k1gMnSc2	Gucci
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1603	[number]	k4	1603
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
je	být	k5eAaImIp3nS	být
také	také	k9	také
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Moniky	Monika	k1gFnSc2	Monika
Uherské	uherský	k2eAgFnSc2d1	uherská
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Klenbu	klenba	k1gFnSc4	klenba
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nese	nést	k5eAaImIp3nS	nést
oktogonální	oktogonální	k2eAgInSc4d1	oktogonální
pilíř	pilíř	k1gInSc4	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Kapli	kaple	k1gFnSc4	kaple
lze	lze	k6eAd1	lze
navštívit	navštívit	k5eAaPmF	navštívit
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
ročně	ročně	k6eAd1	ročně
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Božího	boží	k2eAgNnSc2d1	boží
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Bozego	Bozego	k1gMnSc1	Bozego
Ciala	Ciala	k1gMnSc1	Ciala
č.	č.	k?	č.
26	[number]	k4	26
<g/>
)	)	kIx)	)
–	–	k?	–
stavbu	stavba	k1gFnSc4	stavba
založil	založit	k5eAaPmAgMnS	založit
král	král	k1gMnSc1	král
Kazimír	Kazimír	k1gMnSc1	Kazimír
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Veliký	veliký	k2eAgInSc1d1	veliký
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1340	[number]	k4	1340
<g/>
,	,	kIx,	,
dohotovena	dohotovit	k5eAaPmNgFnS	dohotovit
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1405	[number]	k4	1405
<g/>
.	.	kIx.	.
</s>
<s>
Trojlodní	trojlodní	k2eAgInSc1d1	trojlodní
kostel	kostel	k1gInSc1	kostel
spojuje	spojovat	k5eAaImIp3nS	spojovat
architektonické	architektonický	k2eAgInPc4d1	architektonický
elementy	element	k1gInPc4	element
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
gotiky	gotika	k1gFnSc2	gotika
(	(	kIx(	(
<g/>
štíty	štít	k1gInPc1	štít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
renesance	renesance	k1gFnSc1	renesance
(	(	kIx(	(
<g/>
zvonice	zvonice	k1gFnSc1	zvonice
<g/>
)	)	kIx)	)
a	a	k8xC	a
baroka	baroko	k1gNnSc2	baroko
(	(	kIx(	(
<g/>
boční	boční	k2eAgFnSc1d1	boční
kaple	kaple	k1gFnSc1	kaple
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
stylu	styl	k1gInSc6	styl
provedený	provedený	k2eAgInSc1d1	provedený
interiér	interiér	k1gInSc1	interiér
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgFnPc1d1	zajímavá
jsou	být	k5eAaImIp3nP	být
chórové	chórový	k2eAgFnPc1d1	chórová
lavice	lavice	k1gFnPc1	lavice
v	v	k7c6	v
presbytáři	presbytář	k1gInSc6	presbytář
<g/>
,	,	kIx,	,
kazatelna	kazatelna	k1gFnSc1	kazatelna
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
na	na	k7c6	na
vlně	vlna	k1gFnSc6	vlna
kolísajícího	kolísající	k2eAgInSc2d1	kolísající
člunu	člun	k1gInSc2	člun
<g/>
,	,	kIx,	,
od	od	k7c2	od
dvou	dva	k4xCgFnPc2	dva
sirén	siréna	k1gFnPc2	siréna
a	a	k8xC	a
delfínů	delfín	k1gMnPc2	delfín
neseného	nesený	k2eAgInSc2d1	nesený
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
s	s	k7c7	s
malbou	malba	k1gFnSc7	malba
Tomase	Tomasa	k1gFnSc3	Tomasa
Dolabella	Dolabella	k1gFnSc1	Dolabella
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgNnSc1d1	renesanční
mausoleum	mausoleum	k1gNnSc1	mausoleum
blahoslaveného	blahoslavený	k2eAgMnSc2d1	blahoslavený
Stanislava	Stanislav	k1gMnSc2	Stanislav
Kazimierczyka	Kazimierczyek	k1gMnSc2	Kazimierczyek
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
věž	věž	k1gFnSc1	věž
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
70	[number]	k4	70
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
klášter	klášter	k1gInSc1	klášter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Marka	Marek	k1gMnSc2	Marek
(	(	kIx(	(
<g/>
Kościół	Kościół	k1gFnSc1	Kościół
św	św	k?	św
<g/>
.	.	kIx.	.
</s>
<s>
Marka	marka	k1gFnSc1	marka
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Świetego	Świetego	k6eAd1	Świetego
Marka	Marek	k1gMnSc2	Marek
č.	č.	k?	č.
10	[number]	k4	10
<g/>
)	)	kIx)	)
–	–	k?	–
byl	být	k5eAaImAgMnS	být
založen	založit	k5eAaPmNgMnS	založit
roku	rok	k1gInSc2	rok
1293	[number]	k4	1293
a	a	k8xC	a
svoji	svůj	k3xOyFgFnSc4	svůj
současnou	současný	k2eAgFnSc4d1	současná
gotickou	gotický	k2eAgFnSc4d1	gotická
podobu	podoba	k1gFnSc4	podoba
získal	získat	k5eAaPmAgMnS	získat
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Kříže	kříž	k1gInPc1	kříž
(	(	kIx(	(
<g/>
Kościół	Kościół	k1gFnSc1	Kościół
św	św	k?	św
<g/>
.	.	kIx.	.
</s>
<s>
Krzyża	Krzyża	k1gFnSc1	Krzyża
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Plac	plac	k1gInSc1	plac
Sw	Sw	k1gFnSc2	Sw
<g/>
.	.	kIx.	.
</s>
<s>
Ducha	duch	k1gMnSc2	duch
<g/>
)	)	kIx)	)
–	–	k?	–
první	první	k4xOgInSc4	první
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
kostel	kostel	k1gInSc4	kostel
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
zděný	zděný	k2eAgInSc1d1	zděný
kostel	kostel	k1gInSc1	kostel
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
postavena	postaven	k2eAgFnSc1d1	postavena
věž	věž	k1gFnSc1	věž
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Sofie	Sofia	k1gFnSc2	Sofia
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Ondřeje	Ondřej	k1gMnSc2	Ondřej
a	a	k8xC	a
uprostřed	uprostřed	k7c2	uprostřed
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
kaple	kaple	k1gFnSc2	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Loretské	loretský	k2eAgFnSc2d1	Loretská
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1528	[number]	k4	1528
kostel	kostel	k1gInSc1	kostel
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
a	a	k8xC	a
klenba	klenba	k1gFnSc1	klenba
se	se	k3xPyFc4	se
zřítila	zřítit	k5eAaPmAgFnS	zřítit
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
roku	rok	k1gInSc2	rok
1533	[number]	k4	1533
<g/>
.	.	kIx.	.
</s>
<s>
Jednolodní	jednolodní	k2eAgInSc1d1	jednolodní
kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
zaklenut	zaklenout	k5eAaPmNgInS	zaklenout
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
pilíř	pilíř	k1gInSc4	pilíř
pomocí	pomocí	k7c2	pomocí
ojedinělé	ojedinělý	k2eAgFnSc2d1	ojedinělá
palmové	palmový	k2eAgFnSc2d1	Palmová
klenby	klenba	k1gFnSc2	klenba
s	s	k7c7	s
malbami	malba	k1gFnPc7	malba
<g/>
.	.	kIx.	.
</s>
<s>
Presbytář	presbytář	k1gInSc1	presbytář
má	mít	k5eAaImIp3nS	mít
síťovou	síťový	k2eAgFnSc4d1	síťová
klenbu	klenba	k1gFnSc4	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Malby	malba	k1gFnPc1	malba
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
věku	věk	k1gInSc2	věk
restaurovány	restaurovat	k5eAaBmNgFnP	restaurovat
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
lodi	loď	k1gFnSc6	loď
dva	dva	k4xCgInPc4	dva
oltáře	oltář	k1gInPc4	oltář
<g/>
:	:	kIx,	:
P.	P.	kA	P.
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
křtitelnice	křtitelnice	k1gFnSc1	křtitelnice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1423	[number]	k4	1423
<g/>
,	,	kIx,	,
krucifix	krucifix	k1gInSc4	krucifix
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
chórové	chórový	k2eAgFnSc2d1	chórová
lavice	lavice	k1gFnSc2	lavice
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
kazatelna	kazatelna	k1gFnSc1	kazatelna
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jiljí	Jiljí	k1gMnSc2	Jiljí
(	(	kIx(	(
<g/>
Kościół	Kościół	k1gFnSc1	Kościół
św	św	k?	św
<g/>
.	.	kIx.	.
</s>
<s>
Idziego	Idziego	k1gNnSc1	Idziego
<g/>
)	)	kIx)	)
–	–	k?	–
postaven	postaven	k2eAgInSc1d1	postaven
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1595	[number]	k4	1595
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
majetkem	majetek	k1gInSc7	majetek
dominikánů	dominikán	k1gMnPc2	dominikán
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ho	on	k3xPp3gMnSc4	on
brzy	brzy	k6eAd1	brzy
dali	dát	k5eAaPmAgMnP	dát
rozšířit	rozšířit	k5eAaPmF	rozšířit
<g/>
.	.	kIx.	.
<g/>
Renesanční	renesanční	k2eAgFnSc1d1	renesanční
KrakovZikmundova	KrakovZikmundův	k2eAgFnSc1d1	KrakovZikmundův
kaple	kaple	k1gFnSc1	kaple
na	na	k7c6	na
Wawelu	Wawel	k1gInSc6	Wawel
(	(	kIx(	(
<g/>
kaplica	kaplic	k2eAgFnSc1d1	kaplic
Zygmuntowska	Zygmuntowska	k1gFnSc1	Zygmuntowska
na	na	k7c4	na
Wawelu	Wawela	k1gFnSc4	Wawela
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vila	vila	k1gFnSc1	vila
Decjusza	Decjusza	k1gFnSc1	Decjusza
(	(	kIx(	(
<g/>
willa	wilnout	k5eAaPmAgFnS	wilnout
Decjusza	Decjusza	k1gFnSc1	Decjusza
w	w	k?	w
Woli	Woli	k1gNnPc2	Woli
Justowskiej	Justowskiej	k1gInSc1	Justowskiej
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kaziměřská	Kaziměřský	k2eAgFnSc1d1	Kaziměřský
radnice	radnice	k1gFnSc1	radnice
(	(	kIx(	(
<g/>
Plac	plac	k1gInSc1	plac
Wolnica	Wolnic	k1gInSc2	Wolnic
č.	č.	k?	č.
1	[number]	k4	1
<g/>
)	)	kIx)	)
–	–	k?	–
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
jako	jako	k8xC	jako
sídlo	sídlo	k1gNnSc1	sídlo
představených	představená	k1gFnPc2	představená
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
vícekrát	vícekrát	k6eAd1	vícekrát
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jí	on	k3xPp3gFnSc3	on
byl	být	k5eAaImAgInS	být
propůjčen	propůjčen	k2eAgInSc1d1	propůjčen
renesanční	renesanční	k2eAgInSc1d1	renesanční
charakter	charakter	k1gInSc1	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Obdržela	obdržet	k5eAaPmAgFnS	obdržet
cimbuří	cimbuří	k1gNnSc4	cimbuří
a	a	k8xC	a
atiky	atika	k1gFnPc4	atika
a	a	k8xC	a
věž	věž	k1gFnSc4	věž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sloučení	sloučení	k1gNnSc6	sloučení
Kaziměře	Kaziměra	k1gFnSc6	Kaziměra
s	s	k7c7	s
Krakovem	Krakov	k1gInSc7	Krakov
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
radnice	radnice	k1gFnSc1	radnice
vydražena	vydražit	k5eAaPmNgFnS	vydražit
a	a	k8xC	a
změnila	změnit	k5eAaPmAgFnS	změnit
vícekrát	vícekrát	k6eAd1	vícekrát
majitele	majitel	k1gMnPc4	majitel
a	a	k8xC	a
pozvolně	pozvolně	k6eAd1	pozvolně
pustla	pustnout	k5eAaImAgFnS	pustnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1875	[number]	k4	1875
až	až	k9	až
1877	[number]	k4	1877
byla	být	k5eAaImAgFnS	být
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
nové	nový	k2eAgNnSc4d1	nové
jižní	jižní	k2eAgNnSc4d1	jižní
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Etnografickým	etnografický	k2eAgNnSc7d1	etnografické
museem	museum	k1gNnSc7	museum
<g/>
.	.	kIx.	.
<g/>
Barokní	barokní	k2eAgMnSc1d1	barokní
KrakovKostel	KrakovKostel	k1gMnSc1	KrakovKostel
Misionářů	misionář	k1gMnPc2	misionář
–	–	k?	–
Obrácení	obrácení	k1gNnPc2	obrácení
sv.	sv.	kA	sv.
Pavla	Pavel	k1gMnSc2	Pavel
(	(	kIx(	(
<g/>
kościół	kościół	k?	kościół
Misjonarzy	Misjonarza	k1gFnSc2	Misjonarza
na	na	k7c6	na
Stradomiu	Stradomium	k1gNnSc6	Stradomium
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Stradomska	Stradomska	k1gFnSc1	Stradomska
č.	č.	k?	č.
4	[number]	k4	4
<g/>
)	)	kIx)	)
–	–	k?	–
vystavěn	vystavěn	k2eAgInSc1d1	vystavěn
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
římských	římský	k2eAgFnPc2d1	římská
sakrálních	sakrální	k2eAgFnPc2d1	sakrální
staveb	stavba	k1gFnPc2	stavba
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1719	[number]	k4	1719
až	až	k9	až
1732	[number]	k4	1732
dle	dle	k7c2	dle
návrhu	návrh	k1gInSc2	návrh
krakovského	krakovský	k2eAgMnSc2d1	krakovský
architekta	architekt	k1gMnSc2	architekt
Kacpera	Kacper	k1gMnSc2	Kacper
Bazanki	Bazank	k1gFnSc2	Bazank
<g/>
.	.	kIx.	.
</s>
<s>
Pozdněbarokní	Pozdněbaroknit	k5eAaPmIp3nS	Pozdněbaroknit
<g/>
,	,	kIx,	,
bezvěžový	bezvěžový	k2eAgInSc1d1	bezvěžový
kostel	kostel	k1gInSc1	kostel
má	mít	k5eAaImIp3nS	mít
průčelí	průčelí	k1gNnPc4	průčelí
z	z	k7c2	z
kamenných	kamenný	k2eAgInPc2d1	kamenný
kvádrů	kvádr	k1gInPc2	kvádr
s	s	k7c7	s
dekoracemi	dekorace	k1gFnPc7	dekorace
z	z	k7c2	z
vápence	vápenec	k1gInSc2	vápenec
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
je	být	k5eAaImIp3nS	být
jednolodní	jednolodní	k2eAgInSc1d1	jednolodní
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
presbytářem	presbytář	k1gInSc7	presbytář
a	a	k8xC	a
s	s	k7c7	s
elipsově	elipsově	k6eAd1	elipsově
formovanou	formovaný	k2eAgFnSc7d1	formovaná
kopulí	kopule	k1gFnSc7	kopule
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
je	být	k5eAaImIp3nS	být
instalován	instalován	k2eAgInSc1d1	instalován
systém	systém	k1gInSc1	systém
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
opticky	opticky	k6eAd1	opticky
kostel	kostel	k1gInSc4	kostel
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
stejnoměrné	stejnoměrný	k2eAgNnSc4d1	stejnoměrné
osvětlení	osvětlení	k1gNnSc4	osvětlení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
je	být	k5eAaImIp3nS	být
mramorový	mramorový	k2eAgInSc1d1	mramorový
oltář	oltář	k1gInSc1	oltář
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1761	[number]	k4	1761
až	až	k6eAd1	až
1762	[number]	k4	1762
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
Tadeusze	Tadeusze	k1gFnPc1	Tadeusze
Kuntze	Kuntze	k1gFnSc2	Kuntze
Obrácení	obrácení	k1gNnSc2	obrácení
sv.	sv.	kA	sv.
Pavla	Pavel	k1gMnSc4	Pavel
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
stříbrný	stříbrný	k2eAgInSc4d1	stříbrný
tabernákl	tabernákl	k1gInSc4	tabernákl
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
lodi	loď	k1gFnSc3	loď
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
přimykají	přimykat	k5eAaImIp3nP	přimykat
boční	boční	k2eAgFnSc2d1	boční
kaple	kaple	k1gFnSc2	kaple
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
je	být	k5eAaImIp3nS	být
vroubena	vroubit	k5eAaImNgFnS	vroubit
nikami	nika	k1gFnPc7	nika
se	s	k7c7	s
sochami	socha	k1gFnPc7	socha
10	[number]	k4	10
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kostel	kostel	k1gInSc4	kostel
navazují	navazovat	k5eAaImIp3nP	navazovat
klášterní	klášterní	k2eAgFnPc1d1	klášterní
budovy	budova	k1gFnPc1	budova
s	s	k7c7	s
Historickým	historický	k2eAgNnSc7d1	historické
a	a	k8xC	a
misionářským	misionářský	k2eAgNnSc7d1	misionářské
museem	museum	k1gNnSc7	museum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Bernardýnů	bernardýn	k1gMnPc2	bernardýn
–	–	k?	–
sv.	sv.	kA	sv.
Berandina	Berandin	k2eAgMnSc4d1	Berandin
Siernského	Siernský	k1gMnSc4	Siernský
(	(	kIx(	(
<g/>
kościół	kościół	k?	kościół
Bernardynów	Bernardynów	k1gFnSc1	Bernardynów
na	na	k7c6	na
Stradomiu	Stradomium	k1gNnSc6	Stradomium
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Bernardyńska	Bernardyńska	k1gFnSc1	Bernardyńska
č.	č.	k?	č.
2	[number]	k4	2
<g/>
)	)	kIx)	)
–	–	k?	–
původní	původní	k2eAgInSc1d1	původní
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
kostel	kostel	k1gInSc1	kostel
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1453	[number]	k4	1453
díky	díky	k7c3	díky
Zbigniewu	Zbigniew	k1gMnSc3	Zbigniew
kardinálu	kardinál	k1gMnSc3	kardinál
Oleśnickiemu	Oleśnickiem	k1gMnSc3	Oleśnickiem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
přestavěn	přestavěn	k2eAgMnSc1d1	přestavěn
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
tak	tak	k6eAd1	tak
stavení	stavení	k1gNnSc1	stavení
v	v	k7c6	v
gotickém	gotický	k2eAgInSc6d1	gotický
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
za	za	k7c2	za
Švédské	švédský	k2eAgFnSc2d1	švédská
války	válka	k1gFnSc2	válka
roku	rok	k1gInSc2	rok
1655	[number]	k4	1655
zcela	zcela	k6eAd1	zcela
zničeno	zničen	k2eAgNnSc1d1	zničeno
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
raněbarokní	raněbarokní	k2eAgInSc1d1	raněbarokní
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1659	[number]	k4	1659
až	až	k9	až
1680	[number]	k4	1680
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Krzystofa	Krzystof	k1gMnSc2	Krzystof
Mieroszewskiho	Mieroszewski	k1gMnSc2	Mieroszewski
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třílodní	třílodní	k2eAgFnSc7d1	třílodní
basilikou	basilika	k1gFnSc7	basilika
s	s	k7c7	s
transeptem	transept	k1gInSc7	transept
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
kopulí	kopule	k1gFnSc7	kopule
<g/>
.	.	kIx.	.
</s>
<s>
Průčelí	průčelí	k1gNnSc1	průčelí
je	být	k5eAaImIp3nS	být
dvou	dva	k4xCgMnPc6	dva
věžové	věžový	k2eAgInPc1d1	věžový
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodný	pozoruhodný	k2eAgMnSc1d1	pozoruhodný
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
a	a	k8xC	a
boční	boční	k2eAgInPc1d1	boční
oltáře	oltář	k1gInPc1	oltář
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
pozdněbarokním	pozdněbarokní	k2eAgInSc6d1	pozdněbarokní
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgInPc1d1	zajímavý
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
štuky	štuka	k1gFnPc1	štuka
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
lodi	loď	k1gFnSc6	loď
a	a	k8xC	a
presbytáři	presbytář	k1gInSc6	presbytář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
této	tento	k3xDgFnSc2	tento
světice	světice	k1gFnSc2	světice
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dílo	dílo	k1gNnSc1	dílo
Veita	Veit	k1gMnSc2	Veit
Stossa	Stoss	k1gMnSc2	Stoss
<g/>
)	)	kIx)	)
a	a	k8xC	a
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Šimona	Šimona	k1gFnSc1	Šimona
z	z	k7c2	z
Lipnice	Lipnice	k1gFnSc2	Lipnice
s	s	k7c7	s
mausoleem	mausoleum	k1gNnSc7	mausoleum
z	z	k7c2	z
mramoru	mramor	k1gInSc2	mramor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
(	(	kIx(	(
<g/>
kościół	kościół	k?	kościół
św	św	k?	św
<g/>
.	.	kIx.	.
</s>
<s>
Piotra	Piotra	k1gFnSc1	Piotra
i	i	k8xC	i
Pawła	Pawła	k1gFnSc1	Pawła
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Grodzka	Grodzka	k1gFnSc1	Grodzka
č.	č.	k?	č.
54	[number]	k4	54
<g/>
)	)	kIx)	)
–	–	k?	–
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
barokní	barokní	k2eAgFnSc7d1	barokní
stavbou	stavba	k1gFnSc7	stavba
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1597	[number]	k4	1597
až	až	k9	až
1619	[number]	k4	1619
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc4	kostel
založil	založit	k5eAaPmAgMnS	založit
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Wasa	Wasa	k1gFnSc1	Wasa
pro	pro	k7c4	pro
jezuitský	jezuitský	k2eAgInSc4d1	jezuitský
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Stavitelé	stavitel	k1gMnPc1	stavitel
chrámu	chrám	k1gInSc2	chrám
byli	být	k5eAaImAgMnP	být
postupně	postupně	k6eAd1	postupně
<g/>
:	:	kIx,	:
Józef	Józef	k1gMnSc1	Józef
Britius	Britius	k1gMnSc1	Britius
<g/>
,	,	kIx,	,
Giovanni	Giovaneň	k1gFnSc6	Giovaneň
Maria	Maria	k1gFnSc1	Maria
Bernardoni	Bernardoň	k1gFnSc3	Bernardoň
a	a	k8xC	a
Giovanni	Giovaneň	k1gFnSc3	Giovaneň
Trevano	Trevana	k1gFnSc5	Trevana
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
dal	dát	k5eAaPmAgMnS	dát
kostelu	kostel	k1gInSc3	kostel
konečnou	konečný	k2eAgFnSc4d1	konečná
podobu	podoba	k1gFnSc4	podoba
a	a	k8xC	a
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
i	i	k9	i
fasádu	fasáda	k1gFnSc4	fasáda
<g/>
,	,	kIx,	,
kopuli	kopule	k1gFnSc4	kopule
a	a	k8xC	a
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Basilika	basilika	k1gFnSc1	basilika
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
podle	podle	k7c2	podle
římského	římský	k2eAgInSc2d1	římský
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
půdorys	půdorys	k1gInSc4	půdorys
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgNnPc4	tři
pole	pole	k1gNnPc4	pole
klenby	klenba	k1gFnSc2	klenba
<g/>
,	,	kIx,	,
širokou	široký	k2eAgFnSc4d1	široká
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
transept	transept	k1gInSc4	transept
a	a	k8xC	a
krátký	krátký	k2eAgInSc4d1	krátký
presbytář	presbytář	k1gInSc4	presbytář
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
kruhovou	kruhový	k2eAgFnSc7d1	kruhová
apsidou	apsida	k1gFnSc7	apsida
<g/>
.	.	kIx.	.
</s>
<s>
Fasáda	fasáda	k1gFnSc1	fasáda
je	být	k5eAaImIp3nS	být
zdobena	zdobit	k5eAaImNgFnS	zdobit
sochami	socha	k1gFnPc7	socha
řádových	řádový	k2eAgFnPc2d1	řádová
svatých	svatá	k1gFnPc2	svatá
a	a	k8xC	a
řádovým	řádový	k2eAgInSc7d1	řádový
znakem	znak	k1gInSc7	znak
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
svatými	svatý	k2eAgInPc7d1	svatý
patrony	patron	k1gInPc7	patron
Wasovské	Wasovský	k2eAgFnSc2d1	Wasovský
dynastie	dynastie	k1gFnSc2	dynastie
a	a	k8xC	a
erbem	erb	k1gInSc7	erb
této	tento	k3xDgFnSc2	tento
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
je	být	k5eAaImIp3nS	být
pozoruhodný	pozoruhodný	k2eAgInSc1d1	pozoruhodný
hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1735	[number]	k4	1735
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
od	od	k7c2	od
Józefa	Józef	k1gMnSc2	Józef
Brodowskeho	Brodowske	k1gMnSc2	Brodowske
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1820	[number]	k4	1820
<g/>
.	.	kIx.	.
</s>
<s>
Nádherné	nádherný	k2eAgFnPc1d1	nádherná
štuky	štuka	k1gFnPc1	štuka
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1619	[number]	k4	1619
až	až	k9	až
1633	[number]	k4	1633
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc7	jejich
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
Giovanni	Giovaneň	k1gFnSc3	Giovaneň
Falconi	Falcon	k1gMnPc1	Falcon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
presbytáři	presbytář	k1gInSc6	presbytář
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
scény	scéna	k1gFnPc1	scéna
ze	z	k7c2	z
života	život	k1gInSc2	život
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
a	a	k8xC	a
sochy	socha	k1gFnSc2	socha
patronů	patron	k1gMnPc2	patron
Polska	Polsko	k1gNnSc2	Polsko
–	–	k?	–
sv.	sv.	kA	sv.
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
a	a	k8xC	a
sv.	sv.	kA	sv.
Stanislav	Stanislav	k1gMnSc1	Stanislav
apod.	apod.	kA	apod.
Díky	díky	k7c3	díky
dobré	dobrý	k2eAgFnSc3d1	dobrá
akustice	akustika	k1gFnSc3	akustika
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konají	konat	k5eAaImIp3nP	konat
koncerty	koncert	k1gInPc1	koncert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc2	Martin
(	(	kIx(	(
<g/>
kościół	kościół	k?	kościół
św	św	k?	św
<g/>
.	.	kIx.	.
</s>
<s>
Marcina	Marcina	k1gFnSc1	Marcina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
(	(	kIx(	(
<g/>
kościół	kościół	k?	kościół
św	św	k?	św
<g/>
.	.	kIx.	.
</s>
<s>
Mikołaja	Mikołaja	k1gFnSc1	Mikołaja
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Kopernika	Kopernik	k1gMnSc2	Kopernik
č.	č.	k?	č.
9	[number]	k4	9
<g/>
)	)	kIx)	)
–	–	k?	–
první	první	k4xOgMnSc1	první
románský	románský	k2eAgInSc1d1	románský
kostel	kostel	k1gInSc1	kostel
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nalézal	nalézat	k5eAaImAgInS	nalézat
již	již	k6eAd1	již
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Prvně	Prvně	k?	Prvně
byl	být	k5eAaImAgInS	být
zmíněn	zmínit	k5eAaPmNgInS	zmínit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1229	[number]	k4	1229
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
benediktýnů	benediktýn	k1gMnPc2	benediktýn
z	z	k7c2	z
Tyniece	Tyniece	k1gFnSc2	Tyniece
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
ležel	ležet	k5eAaImAgInS	ležet
mimo	mimo	k7c4	mimo
hradby	hradba	k1gFnPc4	hradba
Krakova	Krakov	k1gInSc2	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1456	[number]	k4	1456
věnovali	věnovat	k5eAaImAgMnP	věnovat
kostel	kostel	k1gInSc4	kostel
benediktýni	benediktýn	k1gMnPc1	benediktýn
Jagellonské	jagellonský	k2eAgFnSc6d1	Jagellonská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
švédské	švédský	k2eAgFnSc2d1	švédská
okupace	okupace	k1gFnSc2	okupace
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
poničen	poničit	k5eAaPmNgInS	poničit
ostřelováním	ostřelování	k1gNnSc7	ostřelování
z	z	k7c2	z
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přestavbě	přestavba	k1gFnSc6	přestavba
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc4	kostel
roku	rok	k1gInSc2	rok
1682	[number]	k4	1682
znovu	znovu	k6eAd1	znovu
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
<g/>
.	.	kIx.	.
</s>
<s>
Fasáda	fasáda	k1gFnSc1	fasáda
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
strohá	strohý	k2eAgFnSc1d1	strohá
se	s	k7c7	s
sochami	socha	k1gFnPc7	socha
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc4	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc4	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
gotický	gotický	k2eAgInSc4d1	gotický
pentaptych	pentaptych	k1gInSc4	pentaptych
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvoře	dvůr	k1gInSc6	dvůr
okolo	okolo	k7c2	okolo
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
lucerna	lucerna	k1gFnSc1	lucerna
mrtvých	mrtvý	k1gMnPc2	mrtvý
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Proměny	proměna	k1gFnSc2	proměna
Páně	páně	k2eAgFnSc2d1	páně
(	(	kIx(	(
<g/>
kościół	kościół	k?	kościół
Przemienienia	Przemienienium	k1gNnSc2	Przemienienium
Pańskiego	Pańskiego	k6eAd1	Pańskiego
(	(	kIx(	(
<g/>
oo	oo	k?	oo
<g/>
.	.	kIx.	.
</s>
<s>
Pijarów	Pijarów	k?	Pijarów
<g/>
))	))	k?	))
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Pijarska	Pijarska	k1gFnSc1	Pijarska
č.	č.	k?	č.
2	[number]	k4	2
<g/>
)	)	kIx)	)
–	–	k?	–
pozdněbarokní	pozdněbaroknit	k5eAaPmIp3nS	pozdněbaroknit
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1714	[number]	k4	1714
až	až	k8xS	až
1728	[number]	k4	1728
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
krakovského	krakovský	k2eAgMnSc2d1	krakovský
architekta	architekt	k1gMnSc2	architekt
Kacpera	Kacper	k1gMnSc2	Kacper
Bazanky	Bazanka	k1gFnSc2	Bazanka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1759	[number]	k4	1759
až	až	k9	až
1761	[number]	k4	1761
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
pěkné	pěkný	k2eAgNnSc1d1	pěkné
třípatrové	třípatrový	k2eAgNnSc1d1	třípatrové
průčelí	průčelí	k1gNnSc1	průčelí
navržená	navržený	k2eAgFnSc1d1	navržená
Francescem	Francesce	k1gNnSc7	Francesce
Placidim	Placidim	k1gInSc1	Placidim
a	a	k8xC	a
hodinová	hodinový	k2eAgFnSc1d1	hodinová
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
schodišti	schodiště	k1gNnSc6	schodiště
na	na	k7c6	na
němž	jenž	k3xRgNnSc6	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
busta	busta	k1gFnSc1	busta
Stanislawa	Stanislawus	k1gMnSc2	Stanislawus
Konarskeho	Konarske	k1gMnSc2	Konarske
<g/>
,	,	kIx,	,
piaristy	piarista	k1gMnSc2	piarista
a	a	k8xC	a
reformátora	reformátor	k1gMnSc2	reformátor
polského	polský	k2eAgNnSc2d1	polské
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgMnSc1	týž
má	mít	k5eAaImIp3nS	mít
bustu	busta	k1gFnSc4	busta
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
presbytáře	presbytář	k1gInSc2	presbytář
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
zazděna	zazděn	k2eAgFnSc1d1	zazděna
i	i	k9	i
urna	urna	k1gFnSc1	urna
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
srdcem	srdce	k1gNnSc7	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
jednolodní	jednolodní	k2eAgInSc1d1	jednolodní
s	s	k7c7	s
půlkruhovým	půlkruhový	k2eAgInSc7d1	půlkruhový
presbytářem	presbytář	k1gInSc7	presbytář
<g/>
.	.	kIx.	.
</s>
<s>
Valená	valený	k2eAgFnSc1d1	valená
klenba	klenba	k1gFnSc1	klenba
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
iluzivními	iluzivní	k2eAgFnPc7d1	iluzivní
stropními	stropní	k2eAgFnPc7d1	stropní
malbami	malba	k1gFnPc7	malba
od	od	k7c2	od
Františka	František	k1gMnSc4	František
Ecksteina	Eckstein	k1gMnSc2	Eckstein
z	z	k7c2	z
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
je	být	k5eAaImIp3nS	být
lemována	lemovat	k5eAaImNgFnS	lemovat
kaplemi	kaple	k1gFnPc7	kaple
s	s	k7c7	s
malbami	malba	k1gFnPc7	malba
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Floriána	Florián	k1gMnSc2	Florián
(	(	kIx(	(
<g/>
Kościół	Kościół	k1gFnSc1	Kościół
św	św	k?	św
<g/>
.	.	kIx.	.
</s>
<s>
Floriana	Florian	k1gMnSc4	Florian
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Warszawska	Warszawska	k1gFnSc1	Warszawska
č.	č.	k?	č.
1	[number]	k4	1
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
–	–	k?	–
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
poutní	poutní	k2eAgInSc4d1	poutní
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
tzv.	tzv.	kA	tzv.
korunovační	korunovační	k2eAgFnPc4d1	korunovační
cesty	cesta	k1gFnPc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
románská	románský	k2eAgFnSc1d1	románská
stavba	stavba	k1gFnSc1	stavba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
Krakova	Krakov	k1gInSc2	Krakov
<g/>
,	,	kIx,	,
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
města	město	k1gNnSc2	město
a	a	k8xC	a
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Kleparz	Kleparza	k1gFnPc2	Kleparza
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
zničený	zničený	k2eAgInSc1d1	zničený
Tatary	tatar	k1gInPc4	tatar
vystavěn	vystavěn	k2eAgInSc4d1	vystavěn
v	v	k7c6	v
gotickém	gotický	k2eAgInSc6d1	gotický
stylu	styl	k1gInSc6	styl
jako	jako	k9	jako
halové	halový	k2eAgNnSc1d1	halové
trojlodí	trojlodí	k1gNnSc1	trojlodí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
požáru	požár	k1gInSc6	požár
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgMnS	být
znovu	znovu	k6eAd1	znovu
vystavěn	vystavět	k5eAaPmNgMnS	vystavět
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1657	[number]	k4	1657
až	až	k9	až
1684	[number]	k4	1684
jako	jako	k8xC	jako
třílodní	třílodní	k2eAgFnSc1d1	třílodní
basilika	basilika	k1gFnSc1	basilika
o	o	k7c6	o
čtyřech	čtyři	k4xCgNnPc6	čtyři
klenebních	klenební	k2eAgNnPc6d1	klenební
polích	pole	k1gNnPc6	pole
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
věžemi	věž	k1gFnPc7	věž
<g/>
.	.	kIx.	.
</s>
<s>
Zachován	zachován	k2eAgInSc1d1	zachován
zůstal	zůstat	k5eAaPmAgInS	zůstat
gotický	gotický	k2eAgInSc1d1	gotický
presbytář	presbytář	k1gInSc1	presbytář
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Floriána	Florián	k1gMnSc2	Florián
<g/>
.	.	kIx.	.
předsíň	předsíň	k1gFnSc1	předsíň
je	být	k5eAaImIp3nS	být
novobarokní	novobarokní	k2eAgFnSc1d1	novobarokní
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřek	vnitřek	k1gInSc1	vnitřek
je	být	k5eAaImIp3nS	být
proveden	provést	k5eAaPmNgInS	provést
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
a	a	k8xC	a
rokokovém	rokokový	k2eAgInSc6d1	rokokový
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Štukové	štukový	k2eAgFnPc1d1	štuková
dekorace	dekorace	k1gFnPc1	dekorace
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
novorokokovém	novorokokový	k2eAgInSc6d1	novorokokový
stylu	styl	k1gInSc6	styl
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
Nástěnné	nástěnný	k2eAgFnPc1d1	nástěnná
malby	malba	k1gFnPc1	malba
až	až	k9	až
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
presbytáři	presbytář	k1gInSc6	presbytář
stojí	stát	k5eAaImIp3nP	stát
pozdněbarokní	pozdněbarokní	k2eAgFnPc1d1	pozdněbarokní
chórové	chórový	k2eAgFnPc1d1	chórová
lavice	lavice	k1gFnPc1	lavice
a	a	k8xC	a
barokní	barokní	k2eAgInSc1d1	barokní
hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
sv.	sv.	kA	sv.
Floriána	Florián	k1gMnSc4	Florián
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1686	[number]	k4	1686
od	od	k7c2	od
Jana	Jan	k1gMnSc2	Jan
Trycjusze	Trycjusze	k1gFnSc2	Trycjusze
<g/>
,	,	kIx,	,
dvorního	dvorní	k2eAgMnSc2d1	dvorní
malíře	malíř	k1gMnSc2	malíř
krále	král	k1gMnSc4	král
Jana	Jan	k1gMnSc2	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Sobieskeho	Sobieskeze	k6eAd1	Sobieskeze
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodný	pozoruhodný	k2eAgInSc1d1	pozoruhodný
je	být	k5eAaImIp3nS	být
i	i	k9	i
z	z	k7c2	z
Mariánského	mariánský	k2eAgInSc2d1	mariánský
kostela	kostel	k1gInSc2	kostel
pocházející	pocházející	k2eAgInSc1d1	pocházející
tryptych	tryptych	k1gInSc1	tryptych
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1941	[number]	k4	1941
až	až	k9	až
1955	[number]	k4	1955
sloužil	sloužit	k5eAaImAgInS	sloužit
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
jako	jako	k9	jako
vikář	vikář	k1gMnSc1	vikář
pozdější	pozdní	k2eAgMnSc1d2	pozdější
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Karmelitánský	karmelitánský	k2eAgInSc1d1	karmelitánský
(	(	kIx(	(
<g/>
kościół	kościół	k?	kościół
Karmelitów	Karmelitów	k1gMnSc1	Karmelitów
na	na	k7c6	na
Piasku	Piasek	k1gInSc6	Piasek
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Karmelicka	Karmelicka	k1gFnSc1	Karmelicka
19	[number]	k4	19
<g/>
)	)	kIx)	)
–	–	k?	–
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
králem	král	k1gMnSc7	král
Vladislavem	Vladislav	k1gMnSc7	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jagellem	Jagell	k1gInSc7	Jagell
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
ženou	žena	k1gFnSc7	žena
sv.	sv.	kA	sv.
Hedvikou	Hedvika	k1gFnSc7	Hedvika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1390	[number]	k4	1390
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1397	[number]	k4	1397
byl	být	k5eAaImAgInS	být
předán	předat	k5eAaPmNgInS	předat
karmelitánům	karmelitán	k1gMnPc3	karmelitán
pocházejícím	pocházející	k2eAgFnPc3d1	pocházející
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
přestavěn	přestavět	k5eAaPmNgInS	přestavět
v	v	k7c4	v
halové	halový	k2eAgNnSc4d1	halové
trojlodí	trojlodí	k1gNnSc4	trojlodí
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Švédské	švédský	k2eAgFnSc2d1	švédská
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1655	[number]	k4	1655
zničen	zničit	k5eAaPmNgInS	zničit
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
postaven	postaven	k2eAgMnSc1d1	postaven
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1659	[number]	k4	1659
až	až	k9	až
1679	[number]	k4	1679
<g/>
.	.	kIx.	.
</s>
<s>
Fasáda	fasáda	k1gFnSc1	fasáda
připomíná	připomínat	k5eAaImIp3nS	připomínat
fasádu	fasáda	k1gFnSc4	fasáda
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Grodzka	Grodzka	k1gFnSc1	Grodzka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
osazena	osadit	k5eAaPmNgFnS	osadit
sochami	socha	k1gFnPc7	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
s	s	k7c7	s
ježíškem	ježíšek	k1gInSc7	ježíšek
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
svatými	svatá	k1gFnPc7	svatá
karmelitánského	karmelitánský	k2eAgInSc2d1	karmelitánský
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
třílodní	třílodní	k2eAgFnSc4d1	třílodní
basiliku	basilika	k1gFnSc4	basilika
<g/>
,	,	kIx,	,
presbytář	presbytář	k1gInSc1	presbytář
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
lodě	loď	k1gFnPc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
přesbytáře	přesbytář	k1gInSc2	přesbytář
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
dvě	dva	k4xCgFnPc1	dva
věže	věž	k1gFnPc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
oltáři	oltář	k1gInSc3	oltář
je	být	k5eAaImIp3nS	být
obraz	obraz	k1gInSc1	obraz
z	z	k7c2	z
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
od	od	k7c2	od
Jerzyho	Jerzy	k1gMnSc2	Jerzy
Hankinse	Hankins	k1gMnSc2	Hankins
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
dvě	dva	k4xCgFnPc1	dva
kaple	kaple	k1gFnPc1	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
Matky	matka	k1gFnSc2	matka
Boží	božit	k5eAaImIp3nS	božit
se	s	k7c7	s
škapulířem	škapulíř	k1gInSc7	škapulíř
Matky	matka	k1gFnSc2	matka
Boží	božit	k5eAaImIp3nS	božit
a	a	k8xC	a
s	s	k7c7	s
freskou	freska	k1gFnSc7	freska
představující	představující	k2eAgFnSc4d1	představující
Matku	matka	k1gFnSc4	matka
Boží	božit	k5eAaImIp3nP	božit
s	s	k7c7	s
Ježíškem	ježíšek	k1gInSc7	ježíšek
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
obraz	obraz	k1gInSc4	obraz
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
namaloval	namalovat	k5eAaPmAgMnS	namalovat
na	na	k7c4	na
vnější	vnější	k2eAgFnSc4d1	vnější
zeď	zeď	k1gFnSc4	zeď
kostela	kostel	k1gInSc2	kostel
neznámý	známý	k2eNgMnSc1d1	neznámý
řádový	řádový	k2eAgMnSc1d1	řádový
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
znám	znám	k2eAgMnSc1d1	znám
pro	pro	k7c4	pro
zázraky	zázrak	k1gInPc4	zázrak
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
otisk	otisk	k1gInSc4	otisk
nohy	noha	k1gFnSc2	noha
královny	královna	k1gFnSc2	královna
sv.	sv.	kA	sv.
Hedviky	Hedvika	k1gFnSc2	Hedvika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
se	se	k3xPyFc4	se
ke	k	k7c3	k
kostelu	kostel	k1gInSc3	kostel
přimykají	přimykat	k5eAaImIp3nP	přimykat
klášterní	klášterní	k2eAgFnPc4d1	klášterní
budovy	budova	k1gFnPc4	budova
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
(	(	kIx(	(
<g/>
Kościół	Kościół	k1gFnSc1	Kościół
św	św	k?	św
<g/>
.	.	kIx.	.
</s>
<s>
Anny	Anna	k1gFnSc2	Anna
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Ulice	ulice	k1gFnSc1	ulice
Sw	Sw	k1gFnSc1	Sw
<g/>
.	.	kIx.	.
</s>
<s>
Anny	Anna	k1gFnPc4	Anna
č.	č.	k?	č.
11	[number]	k4	11
<g/>
)	)	kIx)	)
–	–	k?	–
kostel	kostel	k1gInSc1	kostel
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
Jagelonské	jagelonský	k2eAgFnSc3d1	Jagelonská
univerzitě	univerzita	k1gFnSc3	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
kostel	kostel	k1gInSc4	kostel
stála	stát	k5eAaImAgFnS	stát
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
již	již	k6eAd1	již
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
požáru	požár	k1gInSc6	požár
roku	rok	k1gInSc2	rok
1407	[number]	k4	1407
založil	založit	k5eAaPmAgMnS	založit
král	král	k1gMnSc1	král
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jagello	Jagelnout	k5eAaPmAgNnS	Jagelnout
zděný	zděný	k2eAgInSc4d1	zděný
kostel	kostel	k1gInSc4	kostel
v	v	k7c6	v
gotickém	gotický	k2eAgInSc6d1	gotický
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
povýšen	povýšen	k2eAgInSc1d1	povýšen
na	na	k7c4	na
kolegiátní	kolegiátní	k2eAgInSc4d1	kolegiátní
kostel	kostel	k1gInSc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
barokní	barokní	k2eAgFnSc1d1	barokní
stavba	stavba	k1gFnSc1	stavba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
odstraněného	odstraněný	k2eAgInSc2d1	odstraněný
gotického	gotický	k2eAgInSc2d1	gotický
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1689	[number]	k4	1689
až	až	k9	až
1703	[number]	k4	1703
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
holandského	holandský	k2eAgMnSc2d1	holandský
architekta	architekt	k1gMnSc2	architekt
Tillmana	Tillman	k1gMnSc2	Tillman
von	von	k1gInSc4	von
Gameren	Gamerna	k1gFnPc2	Gamerna
<g/>
.	.	kIx.	.
</s>
<s>
Třílodní	třílodní	k2eAgFnSc1d1	třílodní
basilika	basilika	k1gFnSc1	basilika
má	mít	k5eAaImIp3nS	mít
transept	transept	k1gInSc4	transept
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c6	na
půdorysu	půdorys	k1gInSc6	půdorys
kříže	kříž	k1gInSc2	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
je	být	k5eAaImIp3nS	být
zakryta	zakrýt	k5eAaPmNgFnS	zakrýt
valenou	valený	k2eAgFnSc7d1	valená
klenbou	klenba	k1gFnSc7	klenba
a	a	k8xC	a
kopulí	kopule	k1gFnSc7	kopule
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bočních	boční	k2eAgFnPc6d1	boční
lodích	loď	k1gFnPc6	loď
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
šest	šest	k4xCc1	šest
kaplí	kaple	k1gFnPc2	kaple
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc1	tři
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Fasáda	fasáda	k1gFnSc1	fasáda
je	být	k5eAaImIp3nS	být
zakončena	zakončit	k5eAaPmNgFnS	zakončit
dvěma	dva	k4xCgFnPc7	dva
věžemi	věž	k1gFnPc7	věž
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
je	být	k5eAaImIp3nS	být
barokní	barokní	k2eAgMnSc1d1	barokní
<g/>
.	.	kIx.	.
</s>
<s>
Dekorace	dekorace	k1gFnPc1	dekorace
<g/>
,	,	kIx,	,
sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
reliéfy	reliéf	k1gInPc1	reliéf
pocházejí	pocházet	k5eAaImIp3nP	pocházet
od	od	k7c2	od
sochaře	sochař	k1gMnSc2	sochař
Balthasara	Balthasara	k1gFnSc1	Balthasara
Fontany	Fontan	k1gInPc4	Fontan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavním	hlavní	k2eAgInSc6d1	hlavní
oltáři	oltář	k1gInSc6	oltář
je	být	k5eAaImIp3nS	být
obraz	obraz	k1gInSc1	obraz
se	s	k7c7	s
sv.	sv.	kA	sv.
Annou	Anna	k1gFnSc7	Anna
samotřetí	samotřetí	k1gNnSc2	samotřetí
<g/>
.	.	kIx.	.
</s>
<s>
Varhany	varhany	k1gFnPc1	varhany
a	a	k8xC	a
kazatelna	kazatelna	k1gFnSc1	kazatelna
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Zvěstování	zvěstování	k1gNnSc2	zvěstování
panny	panna	k1gFnSc2	panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
kapucínský	kapucínský	k2eAgMnSc1d1	kapucínský
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Kościół	Kościół	k1gFnSc1	Kościół
Zwiastowania	Zwiastowanium	k1gNnSc2	Zwiastowanium
NP	NP	kA	NP
Marii	Maria	k1gFnSc4	Maria
<g/>
,	,	kIx,	,
oo	oo	k?	oo
<g/>
.	.	kIx.	.
</s>
<s>
Kapucynów	Kapucynów	k?	Kapucynów
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Loretańska	Loretańska	k1gFnSc1	Loretańska
č.	č.	k?	č.
11	[number]	k4	11
<g/>
)	)	kIx)	)
–	–	k?	–
byl	být	k5eAaImAgMnS	být
založen	založit	k5eAaPmNgMnS	založit
Wojciechem	Wojciech	k1gMnSc7	Wojciech
Dembińskim	Dembińskim	k1gMnSc1	Dembińskim
založen	založit	k5eAaPmNgMnS	založit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1696	[number]	k4	1696
až	až	k9	až
1699	[number]	k4	1699
pro	pro	k7c4	pro
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
pocházející	pocházející	k2eAgFnSc2d1	pocházející
kapucinský	kapucinský	k2eAgInSc4d1	kapucinský
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
toskánského	toskánský	k2eAgNnSc2d1	toskánské
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
jednolodní	jednolodní	k2eAgInSc1d1	jednolodní
<g/>
,	,	kIx,	,
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
přibyly	přibýt	k5eAaPmAgFnP	přibýt
boční	boční	k2eAgFnPc1d1	boční
lodě	loď	k1gFnPc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
je	být	k5eAaImIp3nS	být
zařízen	zařídit	k5eAaPmNgInS	zařídit
prostě	prostě	k6eAd1	prostě
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
několik	několik	k4yIc1	několik
barokních	barokní	k2eAgInPc2d1	barokní
oltářů	oltář	k1gInPc2	oltář
<g/>
,	,	kIx,	,
kazatelna	kazatelna	k1gFnSc1	kazatelna
<g/>
,	,	kIx,	,
zpovědnice	zpovědnice	k1gFnSc1	zpovědnice
a	a	k8xC	a
epitafy	epitaf	k1gInPc1	epitaf
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
hlavní	hlavní	k2eAgFnSc2d1	hlavní
lodi	loď	k1gFnSc2	loď
je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
černém	černý	k2eAgInSc6d1	černý
mramorovém	mramorový	k2eAgInSc6d1	mramorový
sarkofágu	sarkofág	k1gInSc6	sarkofág
zakladatel	zakladatel	k1gMnSc1	zakladatel
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavním	hlavní	k2eAgInSc6d1	hlavní
oltáři	oltář	k1gInSc6	oltář
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
malba	malba	k1gFnSc1	malba
Zvěstování	zvěstování	k1gNnSc2	zvěstování
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
od	od	k7c2	od
Pietera	Pieter	k1gMnSc2	Pieter
Dandiniho	Dandini	k1gMnSc2	Dandini
z	z	k7c2	z
Florencie	Florencie	k1gFnSc2	Florencie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1701	[number]	k4	1701
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
je	být	k5eAaImIp3nS	být
také	také	k9	také
dělostřelecká	dělostřelecký	k2eAgFnSc1d1	dělostřelecká
koule	koule	k1gFnSc1	koule
z	z	k7c2	z
bojů	boj	k1gInPc2	boj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1768	[number]	k4	1768
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zůstala	zůstat	k5eAaPmAgFnS	zůstat
vězet	vězet	k5eAaImF	vězet
ve	v	k7c6	v
zdi	zeď	k1gFnSc6	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1712	[number]	k4	1712
až	až	k8xS	až
1719	[number]	k4	1719
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
postavena	postaven	k2eAgFnSc1d1	postavena
loreta	loreta	k1gFnSc1	loreta
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
krakovského	krakovský	k2eAgMnSc2d1	krakovský
architekta	architekt	k1gMnSc2	architekt
Kacpera	Kacper	k1gMnSc2	Kacper
Bazanki	Bazank	k1gFnSc2	Bazank
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1794	[number]	k4	1794
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
povstání	povstání	k1gNnSc2	povstání
zde	zde	k6eAd1	zde
Tadeusz	Tadeusz	k1gInSc1	Tadeusz
Kościuszko	Kościuszka	k1gFnSc5	Kościuszka
světil	světit	k5eAaImAgMnS	světit
šavle	šavle	k1gFnSc1	šavle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
a	a	k8xC	a
klášter	klášter	k1gInSc1	klášter
Pavlínů	pavlín	k1gMnPc2	pavlín
na	na	k7c6	na
Skalce	skalka	k1gFnSc6	skalka
(	(	kIx(	(
<g/>
kościół	kościół	k?	kościół
i	i	k8xC	i
klasztor	klasztor	k1gMnSc1	klasztor
Paulinów	Paulinów	k1gMnSc1	Paulinów
<g/>
,	,	kIx,	,
na	na	k7c6	na
Skałce	Skałka	k1gFnSc6	Skałka
<g/>
)	)	kIx)	)
–	–	k?	–
basilika	basilika	k1gFnSc1	basilika
Archanděla	archanděl	k1gMnSc2	archanděl
Michaela	Michael	k1gMnSc2	Michael
a	a	k8xC	a
sv.	sv.	kA	sv.
Stanislava	Stanislava	k1gFnSc1	Stanislava
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Skaleczna	Skaleczna	k1gFnSc1	Skaleczna
no	no	k9	no
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
)	)	kIx)	)
–	–	k?	–
známá	známý	k2eAgFnSc1d1	známá
i	i	k9	i
jako	jako	k8xS	jako
kostel	kostel	k1gInSc4	kostel
Na	na	k7c6	na
skalce	skalka	k1gFnSc6	skalka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešní	dnešní	k2eAgFnSc2d1	dnešní
stavby	stavba	k1gFnSc2	stavba
měl	mít	k5eAaImAgInS	mít
stát	stát	k5eAaPmF	stát
pohanský	pohanský	k2eAgInSc1d1	pohanský
chrám	chrám	k1gInSc1	chrám
<g/>
,	,	kIx,	,
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
románská	románský	k2eAgFnSc1d1	románská
stavba	stavba	k1gFnSc1	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
lidové	lidový	k2eAgFnSc2d1	lidová
pověsti	pověst	k1gFnSc2	pověst
zde	zde	k6eAd1	zde
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
roku	rok	k1gInSc2	rok
1079	[number]	k4	1079
po	po	k7c6	po
konfliktu	konflikt	k1gInSc6	konflikt
světské	světský	k2eAgFnSc2d1	světská
a	a	k8xC	a
církevní	církevní	k2eAgFnSc2d1	církevní
moci	moc	k1gFnSc2	moc
zabit	zabít	k5eAaPmNgMnS	zabít
sv.	sv.	kA	sv.
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
připomínku	připomínka	k1gFnSc4	připomínka
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
koná	konat	k5eAaImIp3nS	konat
procesí	procesí	k1gNnSc4	procesí
z	z	k7c2	z
katedrály	katedrála	k1gFnSc2	katedrála
na	na	k7c4	na
Wawelu	Wawela	k1gFnSc4	Wawela
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
světec	světec	k1gMnSc1	světec
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
ke	k	k7c3	k
kostelu	kostel	k1gInSc3	kostel
Na	na	k7c6	na
skalce	skalka	k1gFnSc6	skalka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
světec	světec	k1gMnSc1	světec
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Gotický	gotický	k2eAgInSc4d1	gotický
kostel	kostel	k1gInSc4	kostel
založil	založit	k5eAaPmAgMnS	založit
král	král	k1gMnSc1	král
Kazimír	Kazimír	k1gMnSc1	Kazimír
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Veliký	veliký	k2eAgMnSc1d1	veliký
v	v	k7c4	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
barokní	barokní	k2eAgFnSc1d1	barokní
stavba	stavba	k1gFnSc1	stavba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
po	po	k7c6	po
důkladné	důkladný	k2eAgFnSc6d1	důkladná
přestavbě	přestavba	k1gFnSc6	přestavba
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1733	[number]	k4	1733
až	až	k8xS	až
1751	[number]	k4	1751
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
Antona	Anton	k1gMnSc2	Anton
Müntzera	Müntzer	k1gMnSc2	Müntzer
a	a	k8xC	a
poté	poté	k6eAd1	poté
dle	dle	k7c2	dle
plánů	plán	k1gInPc2	plán
Antona	Anton	k1gMnSc2	Anton
Solariho	Solari	k1gMnSc2	Solari
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
třílodní	třílodní	k2eAgFnSc4d1	třílodní
basiliku	basilika	k1gFnSc4	basilika
s	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
barokními	barokní	k2eAgFnPc7d1	barokní
věžemi	věž	k1gFnPc7	věž
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
vede	vést	k5eAaImIp3nS	vést
imposantní	imposantní	k2eAgNnSc1d1	imposantní
schodiště	schodiště	k1gNnSc1	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodné	pozoruhodný	k2eAgFnPc1d1	pozoruhodná
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
stylu	styl	k1gInSc6	styl
bohatě	bohatě	k6eAd1	bohatě
zdobené	zdobený	k2eAgNnSc1d1	zdobené
<g/>
:	:	kIx,	:
kazatelna	kazatelna	k1gFnSc1	kazatelna
<g/>
,	,	kIx,	,
zpovědnice	zpovědnice	k1gFnSc1	zpovědnice
<g/>
,	,	kIx,	,
novobarokní	novobarokní	k2eAgFnSc1d1	novobarokní
lavice	lavice	k1gFnSc1	lavice
a	a	k8xC	a
rokokové	rokokový	k2eAgInPc1d1	rokokový
varhany	varhany	k1gInPc1	varhany
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavním	hlavní	k2eAgInSc6d1	hlavní
oltáři	oltář	k1gInSc6	oltář
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
malba	malba	k1gFnSc1	malba
Archanděla	archanděl	k1gMnSc2	archanděl
Michaela	Michael	k1gMnSc2	Michael
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
bočními	boční	k2eAgInPc7d1	boční
oltáři	oltář	k1gInPc7	oltář
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
oltář	oltář	k1gInSc1	oltář
sv.	sv.	kA	sv.
Stanislava	Stanislava	k1gFnSc1	Stanislava
nebo	nebo	k8xC	nebo
oltář	oltář	k1gInSc1	oltář
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Čenstochovské	čenstochovský	k2eAgFnSc2d1	Čenstochovská
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
vícero	vícero	k1gNnSc1	vícero
bust	busta	k1gFnPc2	busta
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
kostelem	kostel	k1gInSc7	kostel
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
jezírko	jezírko	k1gNnSc1	jezírko
sv.	sv.	kA	sv.
Stanislava	Stanislav	k1gMnSc2	Stanislav
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
svatého	svatý	k2eAgNnSc2d1	svaté
a	a	k8xC	a
oltářem	oltář	k1gInSc7	oltář
Třítisíciletí	třítisíciletý	k2eAgMnPc1d1	třítisíciletý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kryptě	krypta	k1gFnSc6	krypta
kostela	kostel	k1gInSc2	kostel
se	s	k7c7	s
secesními	secesní	k2eAgFnPc7d1	secesní
malbami	malba	k1gFnPc7	malba
jsou	být	k5eAaImIp3nP	být
pohřbeny	pohřben	k2eAgFnPc4d1	pohřbena
významné	významný	k2eAgFnPc4d1	významná
osobnosti	osobnost	k1gFnPc4	osobnost
polské	polský	k2eAgFnSc2d1	polská
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
kronikář	kronikář	k1gMnSc1	kronikář
Jan	Jan	k1gMnSc1	Jan
Długosz	Długosz	k1gMnSc1	Długosz
<g/>
,	,	kIx,	,
Józef	Józef	k1gMnSc1	Józef
Ignacy	Ignaca	k1gFnSc2	Ignaca
Kraszewski	Kraszewsk	k1gFnSc2	Kraszewsk
<g/>
,	,	kIx,	,
Adam	Adam	k1gMnSc1	Adam
Asnyk	Asnyk	k1gMnSc1	Asnyk
<g/>
,	,	kIx,	,
Stanisław	Stanisław	k1gMnSc1	Stanisław
Wyspiański	Wyspiański	k1gNnSc2	Wyspiański
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
(	(	kIx(	(
<g/>
Bonifrátú	Bonifrátú	k1gMnSc1	Bonifrátú
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Krakowska	Krakowska	k1gFnSc1	Krakowska
č.	č.	k?	č.
48	[number]	k4	48
<g/>
)	)	kIx)	)
–	–	k?	–
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
uprostřed	uprostřed	k7c2	uprostřed
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pro	pro	k7c4	pro
řád	řád	k1gInSc4	řád
Trinitářů	trinitář	k1gMnPc2	trinitář
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úpadku	úpadek	k1gInSc6	úpadek
řádu	řád	k1gInSc2	řád
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1796	[number]	k4	1796
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
sklad	sklad	k1gInSc4	sklad
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
byl	být	k5eAaImAgInS	být
převzat	převzít	k5eAaPmNgInS	převzít
řádem	řád	k1gInSc7	řád
Bonifrátů	Bonifrát	k1gMnPc2	Bonifrát
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pozdněbarokní	pozdněbarokní	k2eAgFnSc4d1	pozdněbarokní
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Francesco	Francesco	k1gMnSc1	Francesco
Placidi	Placid	k1gMnPc1	Placid
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
jednolodní	jednolodní	k2eAgInSc1d1	jednolodní
s	s	k7c7	s
bočními	boční	k2eAgFnPc7d1	boční
kaplemi	kaple	k1gFnPc7	kaple
a	a	k8xC	a
pěkným	pěkný	k2eAgNnSc7d1	pěkné
průčelím	průčelí	k1gNnSc7	průčelí
<g/>
.	.	kIx.	.
</s>
<s>
Nástropní	nástropní	k2eAgFnPc1d1	nástropní
malby	malba	k1gFnPc1	malba
přibližují	přibližovat	k5eAaImIp3nP	přibližovat
dějiny	dějiny	k1gFnPc1	dějiny
řádu	řád	k1gInSc2	řád
trinitářů	trinitář	k1gMnPc2	trinitář
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
je	být	k5eAaImIp3nS	být
novobarokní	novobarokní	k2eAgInSc1d1	novobarokní
<g/>
.	.	kIx.	.
<g/>
Krakov	Krakov	k1gInSc1	Krakov
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
stoletíRakovický	stoletíRakovický	k2eAgInSc1d1	stoletíRakovický
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
,	,	kIx,	,
zal	zal	k?	zal
<g/>
.	.	kIx.	.
1803	[number]	k4	1803
(	(	kIx(	(
<g/>
Cmentarz	Cmentarz	k1gInSc1	Cmentarz
Rakowicki	Rakowick	k1gFnSc2	Rakowick
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Krypta	krypta	k1gFnSc1	krypta
zasloužilých	zasloužilý	k2eAgMnPc2d1	zasloužilý
na	na	k7c6	na
Skalce	skalka	k1gFnSc6	skalka
(	(	kIx(	(
<g/>
Krypta	krypta	k1gFnSc1	krypta
Zasłużonych	Zasłużonycha	k1gFnPc2	Zasłużonycha
na	na	k7c6	na
Skałce	Skałka	k1gFnSc6	Skałka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
domy	dům	k1gInPc4	dům
Teodora	Teodor	k1gMnSc4	Teodor
Talovského	Talovský	k1gMnSc4	Talovský
(	(	kIx(	(
<g/>
kamienice	kamienice	k1gFnSc2	kamienice
Teodora	Teodor	k1gMnSc4	Teodor
Talowskiego	Talowskiego	k1gMnSc1	Talowskiego
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Juliusze	Juliusze	k1gFnSc2	Juliusze
Slowackiho	Slowacki	k1gMnSc2	Slowacki
(	(	kIx(	(
<g/>
Plac	plac	k1gInSc1	plac
Sw	Sw	k1gFnSc2	Sw
<g/>
.	.	kIx.	.
</s>
<s>
Ducha	duch	k1gMnSc4	duch
č.	č.	k?	č.
1	[number]	k4	1
<g/>
)	)	kIx)	)
–	–	k?	–
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
divadel	divadlo	k1gNnPc2	divadlo
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1891	[number]	k4	1891
až	až	k9	až
1893	[number]	k4	1893
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Jana	Jan	k1gMnSc2	Jan
Zawiejskiho	Zawiejski	k1gMnSc2	Zawiejski
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
novorenesanční	novorenesanční	k2eAgFnSc1d1	novorenesanční
a	a	k8xC	a
novobarokní	novobarokní	k2eAgFnSc1d1	novobarokní
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
vzor	vzor	k1gInSc1	vzor
jí	on	k3xPp3gFnSc2	on
sloužila	sloužit	k5eAaImAgFnS	sloužit
pařížská	pařížský	k2eAgFnSc1d1	Pařížská
opera	opera	k1gFnSc1	opera
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
jsou	být	k5eAaImIp3nP	být
fresky	freska	k1gFnPc1	freska
od	od	k7c2	od
vídeňského	vídeňský	k2eAgMnSc2d1	vídeňský
umělce	umělec	k1gMnSc2	umělec
Antona	Anton	k1gMnSc2	Anton
Tucha	tucha	k1gFnSc1	tucha
a	a	k8xC	a
opona	opona	k1gFnSc1	opona
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
od	od	k7c2	od
Henryka	Henryek	k1gInSc2	Henryek
Siemiradzki	Siemiradzk	k1gFnSc2	Siemiradzk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
a	a	k8xC	a
konvent	konvent	k1gInSc1	konvent
sester	sestra	k1gFnPc2	sestra
Srdce	srdce	k1gNnSc2	srdce
Páne	Páne	k1gNnSc2	Páne
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Garncarska	Garncarska	k1gFnSc1	Garncarska
č.	č.	k?	č.
26	[number]	k4	26
<g/>
)	)	kIx)	)
–	–	k?	–
vystavěn	vystavěn	k2eAgInSc1d1	vystavěn
v	v	k7c6	v
novorománském	novorománský	k2eAgInSc6d1	novorománský
slohu	sloh	k1gInSc6	sloh
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1895	[number]	k4	1895
až	až	k9	až
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
pod	pod	k7c7	pod
zpívající	zpívající	k2eAgFnSc7d1	zpívající
žábou	žába	k1gFnSc7	žába
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Retoyka	Retoyka	k1gFnSc1	Retoyka
č.	č.	k?	č.
1	[number]	k4	1
<g/>
)	)	kIx)	)
–	–	k?	–
stavba	stavba	k1gFnSc1	stavba
Teodora	Teodor	k1gMnSc2	Teodor
Talowského	Talowský	k1gMnSc2	Talowský
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1889	[number]	k4	1889
až	až	k6eAd1	až
1890	[number]	k4	1890
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
Sokola	Sokol	k1gMnSc2	Sokol
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Pilsudskiego	Pilsudskiego	k1gNnSc1	Pilsudskiego
č.	č.	k?	č.
27	[number]	k4	27
<g/>
)	)	kIx)	)
–	–	k?	–
budova	budova	k1gFnSc1	budova
tělovýchovné	tělovýchovný	k2eAgFnPc1d1	Tělovýchovná
společnosti	společnost	k1gFnPc1	společnost
Sokol	Sokol	k1gMnSc1	Sokol
postavena	postaven	k2eAgFnSc1d1	postavena
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
<g/>
Secesní	secesní	k2eAgFnSc2d1	secesní
KrakovDům	KrakovD	k1gInPc3	KrakovD
Celestyna	Celestyn	k1gMnSc4	Celestyn
Czynciela	Czynciela	k1gFnSc1	Czynciela
(	(	kIx(	(
<g/>
Rynek	rynek	k1gInSc1	rynek
Glówny	Glówna	k1gFnSc2	Glówna
<g/>
)	)	kIx)	)
–	–	k?	–
secesní	secesní	k2eAgInSc1d1	secesní
dům	dům	k1gInSc1	dům
vystavěn	vystavěn	k2eAgInSc1d1	vystavěn
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
dle	dle	k7c2	dle
návrhu	návrh	k1gInSc2	návrh
Ludwika	Ludwika	k1gFnSc1	Ludwika
Wojtyczka	Wojtyczka	k1gFnSc1	Wojtyczka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
patře	patro	k1gNnSc6	patro
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
štukový	štukový	k2eAgInSc1d1	štukový
(	(	kIx(	(
<g/>
secesní	secesní	k2eAgInSc1d1	secesní
<g/>
)	)	kIx)	)
dekor	dekor	k1gInSc1	dekor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Stary	star	k1gInPc1	star
(	(	kIx(	(
<g/>
Plac	plac	k1gInSc1	plac
Szczepański	Szczepańsk	k1gFnSc2	Szczepańsk
<g/>
)	)	kIx)	)
–	–	k?	–
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejstarší	starý	k2eAgFnSc4d3	nejstarší
divadelní	divadelní	k2eAgFnSc4d1	divadelní
budovu	budova	k1gFnSc4	budova
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
svoji	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
plní	plnit	k5eAaImIp3nS	plnit
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1798	[number]	k4	1798
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1903	[number]	k4	1903
až	až	k9	až
1905	[number]	k4	1905
byla	být	k5eAaImAgFnS	být
stavba	stavba	k1gFnSc1	stavba
secesně	secesně	k6eAd1	secesně
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Srdce	srdce	k1gNnSc2	srdce
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
<g/>
,	,	kIx,	,
jezuitský	jezuitský	k2eAgMnSc1d1	jezuitský
(	(	kIx(	(
<g/>
Kościół	Kościół	k1gMnSc1	Kościół
Serca	Serca	k1gMnSc1	Serca
Jezusowego	Jezusowego	k1gMnSc1	Jezusowego
<g/>
,	,	kIx,	,
oo	oo	k?	oo
<g/>
.	.	kIx.	.
</s>
<s>
Jezuitów	Jezuitów	k?	Jezuitów
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Kopernika	Kopernik	k1gMnSc2	Kopernik
č.	č.	k?	č.
36	[number]	k4	36
<g/>
)	)	kIx)	)
–	–	k?	–
jezuitský	jezuitský	k2eAgInSc4d1	jezuitský
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
vystavěn	vystavěn	k2eAgInSc4d1	vystavěn
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1909	[number]	k4	1909
až	až	k9	až
1921	[number]	k4	1921
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Francziska	Franczisko	k1gNnSc2	Franczisko
Maczyńského	Maczyńské	k1gNnSc2	Maczyńské
<g/>
.	.	kIx.	.
</s>
<s>
Kombinace	kombinace	k1gFnSc1	kombinace
různých	různý	k2eAgInPc2d1	různý
historizujících	historizující	k2eAgInPc2d1	historizující
slohů	sloh	k1gInPc2	sloh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
pod	pod	k7c7	pod
glóbem	glóbus	k1gInSc7	glóbus
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Dluga	Dluga	k1gFnSc1	Dluga
č.	č.	k?	č.
1	[number]	k4	1
<g/>
)	)	kIx)	)
–	–	k?	–
secesní	secesní	k2eAgInSc4d1	secesní
dům	dům	k1gInSc4	dům
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Francziska	Franczisko	k1gNnSc2	Franczisko
Maczyńského	Maczyńské	k1gNnSc2	Maczyńské
a	a	k8xC	a
Tadesze	Tadesze	k1gFnSc2	Tadesze
Stryjeńského	Stryjeńského	k2eAgMnSc1d1	Stryjeńského
postaven	postavit	k5eAaPmNgMnS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1904	[number]	k4	1904
až	až	k9	až
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
je	být	k5eAaImIp3nS	být
dům	dům	k1gInSc1	dům
zdobený	zdobený	k2eAgInSc1d1	zdobený
malbami	malba	k1gFnPc7	malba
Józefa	Józef	k1gMnSc2	Józef
Mehofera	Mehofer	k1gMnSc2	Mehofer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pavoučí	pavoučí	k2eAgInSc1d1	pavoučí
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Karmelicka	Karmelicka	k1gFnSc1	Karmelicka
č.	č.	k?	č.
35	[number]	k4	35
<g/>
)	)	kIx)	)
–	–	k?	–
historizující	historizující	k2eAgInSc4d1	historizující
dům	dům	k1gInSc4	dům
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
od	od	k7c2	od
architekta	architekt	k1gMnSc2	architekt
Teodora	Teodor	k1gMnSc2	Teodor
Talowského	Talowský	k1gMnSc2	Talowský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgFnPc1d1	další
sakrální	sakrální	k2eAgFnPc1d1	sakrální
stavby	stavba	k1gFnPc1	stavba
===	===	k?	===
</s>
</p>
<p>
<s>
Dom	Dom	k?	Dom
Kapituły	Kapituły	k1gInPc1	Kapituły
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc2d1	boží
Sněžné	sněžný	k2eAgFnSc2d1	sněžná
sester	sestra	k1gFnPc2	sestra
Dominikánek	dominikánka	k1gFnPc2	dominikánka
(	(	kIx(	(
<g/>
Kościół	Kościół	k1gMnSc1	Kościół
MB	MB	kA	MB
Śnieżnej	Śnieżnej	k1gMnSc1	Śnieżnej
<g/>
,	,	kIx,	,
ss	ss	k?	ss
<g/>
.	.	kIx.	.
</s>
<s>
Dominikanek	Dominikanka	k1gFnPc2	Dominikanka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
(	(	kIx(	(
<g/>
Kościół	Kościół	k1gFnSc1	Kościół
św	św	k?	św
<g/>
.	.	kIx.	.
</s>
<s>
Jana	Jana	k1gFnSc1	Jana
<g/>
,	,	kIx,	,
ss	ss	k?	ss
<g/>
.	.	kIx.	.
</s>
<s>
Prezentek	Prezentka	k1gFnPc2	Prezentka
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc2	ulice
Świetego	Świetego	k1gMnSc1	Świetego
Jana	Jana	k1gFnSc1	Jana
č.	č.	k?	č.
7	[number]	k4	7
<g/>
)	)	kIx)	)
–	–	k?	–
barokní	barokní	k2eAgFnSc1d1	barokní
stavba	stavba	k1gFnSc1	stavba
si	se	k3xPyFc3	se
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
románské	románský	k2eAgInPc4d1	románský
a	a	k8xC	a
gotické	gotický	k2eAgInPc4d1	gotický
elementy	element	k1gInPc4	element
svých	svůj	k3xOyFgMnPc2	svůj
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Josefa	Josef	k1gMnSc2	Josef
(	(	kIx(	(
<g/>
Kościół	Kościół	k1gFnSc1	Kościół
św	św	k?	św
<g/>
.	.	kIx.	.
</s>
<s>
Józefa	Józef	k1gMnSc2	Józef
<g/>
,	,	kIx,	,
ss	ss	k?	ss
<g/>
.	.	kIx.	.
</s>
<s>
Bernardynek	Bernardynka	k1gFnPc2	Bernardynka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Kazimíra	Kazimír	k1gMnSc2	Kazimír
(	(	kIx(	(
<g/>
Kościół	Kościół	k1gFnSc1	Kościół
św	św	k?	św
<g/>
.	.	kIx.	.
</s>
<s>
Kazimierza	Kazimierza	k1gFnSc1	Kazimierza
<g/>
,	,	kIx,	,
oo	oo	k?	oo
<g/>
.	.	kIx.	.
</s>
<s>
Reformatów	Reformatów	k?	Reformatów
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Reformacka	Reformacka	k1gFnSc1	Reformacka
č.	č.	k?	č.
4	[number]	k4	4
<g/>
)	)	kIx)	)
–	–	k?	–
barokní	barokní	k2eAgInSc4d1	barokní
kostel	kostel	k1gInSc4	kostel
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1666	[number]	k4	1666
až	až	k6eAd1	až
1672	[number]	k4	1672
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Norberta	Norberta	k1gFnSc1	Norberta
(	(	kIx(	(
<g/>
Kościół	Kościół	k1gFnSc1	Kościół
św	św	k?	św
<g/>
.	.	kIx.	.
</s>
<s>
Norberta	Norberta	k1gFnSc1	Norberta
<g/>
,	,	kIx,	,
ss	ss	k?	ss
<g/>
.	.	kIx.	.
</s>
<s>
Norbertanek	Norbertanka	k1gFnPc2	Norbertanka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Arcibiskupský	arcibiskupský	k2eAgInSc1d1	arcibiskupský
palác	palác	k1gInSc1	palác
(	(	kIx(	(
<g/>
Pałac	Pałac	k1gInSc1	Pałac
Arcybiskupi	Arcybiskup	k1gFnSc2	Arcybiskup
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Augustýna	Augustýn	k1gMnSc2	Augustýn
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
a	a	k8xC	a
konvent	konvent	k1gInSc1	konvent
Norbertinů	Norbertin	k1gInPc2	Norbertin
ve	v	k7c6	v
Zwierzynci	Zwierzynec	k1gInSc6	Zwierzynec
–	–	k?	–
datující	datující	k2eAgFnSc1d1	datující
se	se	k3xPyFc4	se
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
podoba	podoba	k1gFnSc1	podoba
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Vykupitele	vykupitel	k1gMnSc2	vykupitel
ve	v	k7c6	v
Zwierzynci	Zwierzynec	k1gInSc6	Zwierzynec
–	–	k?	–
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
kostelů	kostel	k1gInPc2	kostel
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
údajně	údajně	k6eAd1	údajně
ze	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
přestavěn	přestavěn	k2eAgInSc1d1	přestavěn
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Markéty	Markéta	k1gFnSc2	Markéta
–	–	k?	–
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
kostel	kostel	k1gInSc4	kostel
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1680	[number]	k4	1680
až	až	k6eAd1	až
1690	[number]	k4	1690
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
kopce	kopec	k1gInSc2	kopec
Blahoslavené	blahoslavený	k2eAgFnSc2d1	blahoslavená
Salome	Salom	k1gInSc5	Salom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Univerzity	univerzita	k1gFnSc2	univerzita
==	==	k?	==
</s>
</p>
<p>
<s>
Jagellonská	jagellonský	k2eAgFnSc1d1	Jagellonská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1364	[number]	k4	1364
<g/>
,	,	kIx,	,
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
48	[number]	k4	48
tisíci	tisíc	k4xCgInPc7	tisíc
studentů	student	k1gMnPc2	student
na	na	k7c6	na
15	[number]	k4	15
fakultách	fakulta	k1gFnPc6	fakulta
</s>
</p>
<p>
<s>
Technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
</s>
</p>
<p>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c4	v
KrakověAkademieAkademie	KrakověAkademieAkademie	k1gFnPc4	KrakověAkademieAkademie
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výchovy	výchova	k1gFnSc2	výchova
awf	awf	k?	awf
<g/>
.	.	kIx.	.
<g/>
krakow	krakow	k?	krakow
<g/>
.	.	kIx.	.
<g/>
pl	pl	k?	pl
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Akademie	akademie	k1gFnSc1	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
</s>
</p>
<p>
<s>
Hornicko-hutnická	hornickoutnický	k2eAgFnSc1d1	hornicko-hutnický
akademie	akademie	k1gFnSc1	akademie
agh	agh	k?	agh
<g/>
.	.	kIx.	.
<g/>
edu	edu	k?	edu
<g/>
.	.	kIx.	.
<g/>
pl	pl	k?	pl
<g/>
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
akademie	akademie	k1gFnSc1	akademie
</s>
</p>
<p>
<s>
Krakovská	krakovský	k2eAgFnSc1d1	Krakovská
akademie	akademie	k1gFnSc1	akademie
A.	A.	kA	A.
Frycza-Modrzewskiego	Frycza-Modrzewskiego	k6eAd1	Frycza-Modrzewskiego
</s>
</p>
<p>
<s>
Papežská	papežský	k2eAgFnSc1d1	Papežská
teologická	teologický	k2eAgFnSc1d1	teologická
akademie	akademie	k1gFnSc1	akademie
</s>
</p>
<p>
<s>
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
akademie	akademie	k1gFnSc1	akademie
</s>
</p>
<p>
<s>
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
akademieOstatní	akademieOstatní	k2eAgFnSc1d1	akademieOstatní
vysoké	vysoká	k1gFnSc2	vysoká
školyStátní	školyStátní	k2eAgFnSc1d1	školyStátní
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
divadelní	divadelní	k2eAgFnSc2d1	divadelní
</s>
</p>
<p>
<s>
Krakovská	krakovský	k2eAgFnSc1d1	Krakovská
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
podpory	podpora	k1gFnSc2	podpora
zdraví	zdraví	k1gNnSc2	zdraví
</s>
</p>
<p>
<s>
Małopolská	Małopolský	k2eAgFnSc1d1	Małopolský
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
škola	škola	k1gFnSc1	škola
sv.	sv.	kA	sv.
Rodiny	rodina	k1gFnPc1	rodina
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
škola	škola	k1gFnSc1	škola
ekonomie	ekonomie	k1gFnSc2	ekonomie
a	a	k8xC	a
informatiky	informatika	k1gFnSc2	informatika
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
evropská	evropský	k2eAgFnSc1d1	Evropská
škola	škola	k1gFnSc1	škola
P.	P.	kA	P.
Józefa	Józef	k1gMnSc2	Józef
Tischnera	Tischner	k1gMnSc2	Tischner
wse	wse	k?	wse
<g/>
.	.	kIx.	.
<g/>
krakow	krakow	k?	krakow
<g/>
.	.	kIx.	.
<g/>
pl	pl	k?	pl
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
filosoficko-pedagogická	filosofickoedagogický	k2eAgFnSc1d1	filosoficko-pedagogický
škola	škola	k1gFnSc1	škola
Ignatianum	Ignatianum	k1gInSc1	Ignatianum
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
škola	škola	k1gFnSc1	škola
obchodní	obchodní	k2eAgFnSc2d1	obchodní
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
škola	škola	k1gFnSc1	škola
ochrany	ochrana	k1gFnSc2	ochrana
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
turistiky	turistika	k1gFnSc2	turistika
a	a	k8xC	a
rekreace	rekreace	k1gFnSc2	rekreace
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
škola	škola	k1gFnSc1	škola
pojišťovnictví	pojišťovnictví	k1gNnSc2	pojišťovnictví
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
škola	škola	k1gFnSc1	škola
managementu	management	k1gInSc2	management
a	a	k8xC	a
bankovnictví	bankovnictví	k1gNnSc2	bankovnictví
</s>
</p>
<p>
<s>
==	==	k?	==
Známí	známý	k2eAgMnPc1d1	známý
rodáci	rodák	k1gMnPc1	rodák
==	==	k?	==
</s>
</p>
<p>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Křivoústý	křivoústý	k2eAgMnSc1d1	křivoústý
(	(	kIx(	(
<g/>
1085	[number]	k4	1085
<g/>
–	–	k?	–
<g/>
1138	[number]	k4	1138
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
kníže	kníže	k1gMnSc1	kníže
</s>
</p>
<p>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Vyhnanec	vyhnanec	k1gMnSc1	vyhnanec
(	(	kIx(	(
<g/>
1105	[number]	k4	1105
<g/>
–	–	k?	–
<g/>
1159	[number]	k4	1159
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
velkovévoda	velkovévoda	k1gMnSc1	velkovévoda
</s>
</p>
<p>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
(	(	kIx(	(
<g/>
1456	[number]	k4	1456
<g/>
–	–	k?	–
<g/>
1516	[number]	k4	1516
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Kazimír	Kazimír	k1gMnSc1	Kazimír
(	(	kIx(	(
<g/>
1458	[number]	k4	1458
<g/>
–	–	k?	–
<g/>
1484	[number]	k4	1484
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
katolický	katolický	k2eAgMnSc1d1	katolický
kardinál	kardinál	k1gMnSc1	kardinál
a	a	k8xC	a
polský	polský	k2eAgMnSc1d1	polský
princ	princ	k1gMnSc1	princ
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
I.	I.	kA	I.
Olbracht	Olbracht	k1gMnSc1	Olbracht
(	(	kIx(	(
<g/>
1459	[number]	k4	1459
<g/>
–	–	k?	–
<g/>
1501	[number]	k4	1501
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
</s>
</p>
<p>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
August	August	k1gMnSc1	August
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1520	[number]	k4	1520
<g/>
–	–	k?	–
<g/>
1572	[number]	k4	1572
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Kazimír	Kazimír	k1gMnSc1	Kazimír
Vasa	Vasa	k1gMnSc1	Vasa
(	(	kIx(	(
<g/>
1609	[number]	k4	1609
<g/>
–	–	k?	–
<g/>
1672	[number]	k4	1672
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Matejko	Matejka	k1gFnSc5	Matejka
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
–	–	k?	–
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Ludwig	Ludwig	k1gMnSc1	Ludwig
Gumplowicz	Gumplowicz	k1gMnSc1	Gumplowicz
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sociolog	sociolog	k1gMnSc1	sociolog
a	a	k8xC	a
právník	právník	k1gMnSc1	právník
</s>
</p>
<p>
<s>
Olga	Olga	k1gFnSc1	Olga
Boznańska	Boznańska	k1gFnSc1	Boznańska
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malířka	malířka	k1gFnSc1	malířka
</s>
</p>
<p>
<s>
Bronisław	Bronisław	k?	Bronisław
Malinowski	Malinowski	k1gNnSc1	Malinowski
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polsko-anglický	polskonglický	k2eAgMnSc1d1	polsko-anglický
etnolog	etnolog	k1gMnSc1	etnolog
a	a	k8xC	a
sociolog	sociolog	k1gMnSc1	sociolog
</s>
</p>
<p>
<s>
Stefan	Stefan	k1gMnSc1	Stefan
Banach	Banach	k1gMnSc1	Banach
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
</s>
</p>
<p>
<s>
Roman	Roman	k1gMnSc1	Roman
Ingarden	Ingardna	k1gFnPc2	Ingardna
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
</s>
</p>
<p>
<s>
Jo	jo	k9	jo
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
Zusman	Zusman	k1gMnSc1	Zusman
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
izraelský	izraelský	k2eAgMnSc1d1	izraelský
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
soudce	soudce	k1gMnSc1	soudce
</s>
</p>
<p>
<s>
Cvi	Cvi	k?	Cvi
Hecker	Hecker	k1gInSc1	Hecker
(	(	kIx(	(
<g/>
*	*	kIx~	*
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
izraelský	izraelský	k2eAgMnSc1d1	izraelský
architekt	architekt	k1gMnSc1	architekt
</s>
</p>
<p>
<s>
Jerzy	Jerza	k1gFnSc2	Jerza
Hoffman	Hoffman	k1gMnSc1	Hoffman
(	(	kIx(	(
<g/>
*	*	kIx~	*
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
</s>
</p>
<p>
<s>
Ewa	Ewa	k?	Ewa
Lipska	Lipsko	k1gNnPc1	Lipsko
(	(	kIx(	(
<g/>
*	*	kIx~	*
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polská	polský	k2eAgFnSc1d1	polská
básnířka	básnířka	k1gFnSc1	básnířka
</s>
</p>
<p>
<s>
Jerzy	Jerz	k1gInPc1	Jerz
Stuhr	Stuhra	k1gFnPc2	Stuhra
(	(	kIx(	(
<g/>
*	*	kIx~	*
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Kubica	Kubica	k1gMnSc1	Kubica
(	(	kIx(	(
<g/>
*	*	kIx~	*
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pilot	pilot	k1gMnSc1	pilot
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
</s>
</p>
<p>
<s>
Agnieszka	Agnieszka	k1gFnSc1	Agnieszka
Radwańská	Radwańská	k1gFnSc1	Radwańská
(	(	kIx(	(
<g/>
*	*	kIx~	*
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesionální	profesionální	k2eAgFnSc1d1	profesionální
tenistka	tenistka	k1gFnSc1	tenistka
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
spojené	spojený	k2eAgFnPc1d1	spojená
s	s	k7c7	s
městem	město	k1gNnSc7	město
==	==	k?	==
</s>
</p>
<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
I.	I.	kA	I.
ze	z	k7c2	z
Szczepanowa	Szczepanow	k1gInSc2	Szczepanow
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
1030	[number]	k4	1030
<g/>
–	–	k?	–
<g/>
1079	[number]	k4	1079
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
katolický	katolický	k2eAgMnSc1d1	katolický
biskup	biskup	k1gMnSc1	biskup
a	a	k8xC	a
mučedník	mučedník	k1gMnSc1	mučedník
<g/>
,	,	kIx,	,
krakovský	krakovský	k2eAgMnSc1d1	krakovský
biskup	biskup	k1gMnSc1	biskup
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Turzo	Turza	k1gFnSc5	Turza
(	(	kIx(	(
<g/>
1437	[number]	k4	1437
<g/>
–	–	k?	–
<g/>
1508	[number]	k4	1508
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patricij	patricij	k1gMnSc1	patricij
<g/>
,	,	kIx,	,
obchodník	obchodník	k1gMnSc1	obchodník
a	a	k8xC	a
podnikatel	podnikatel	k1gMnSc1	podnikatel
</s>
</p>
<p>
<s>
Veit	Veit	k2eAgInSc1d1	Veit
Stoss	Stoss	k1gInSc1	Stoss
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Wit	Wit	k1gMnSc1	Wit
Stwosz	Stwosz	k1gMnSc1	Stwosz
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
1448	[number]	k4	1448
<g/>
–	–	k?	–
<g/>
1533	[number]	k4	1533
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
sochař	sochař	k1gMnSc1	sochař
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Koperník	Koperník	k1gMnSc1	Koperník
(	(	kIx(	(
<g/>
1473	[number]	k4	1473
<g/>
–	–	k?	–
<g/>
1543	[number]	k4	1543
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polsko-německý	polskoěmecký	k2eAgMnSc1d1	polsko-německý
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
humanista	humanista	k1gMnSc1	humanista
</s>
</p>
<p>
<s>
Georg	Georg	k1gInSc1	Georg
Trakl	Trakl	k1gInSc1	Trakl
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
–	–	k?	–
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
lyrik	lyrik	k1gMnSc1	lyrik
a	a	k8xC	a
expresionistický	expresionistický	k2eAgMnSc1d1	expresionistický
básník	básník	k1gMnSc1	básník
</s>
</p>
<p>
<s>
Oskar	Oskar	k1gMnSc1	Oskar
Schindler	Schindler	k1gMnSc1	Schindler
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
továrník	továrník	k1gMnSc1	továrník
<g/>
,	,	kIx,	,
záchrance	záchranka	k1gFnSc6	záchranka
1	[number]	k4	1
200	[number]	k4	200
Židů	Žid	k1gMnPc2	Žid
před	před	k7c7	před
holocaustem	holocaust	k1gInSc7	holocaust
</s>
</p>
<p>
<s>
Czesław	Czesław	k?	Czesław
Miłosz	Miłosz	k1gInSc1	Miłosz
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
akademik	akademik	k1gMnSc1	akademik
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
264	[number]	k4	264
<g/>
.	.	kIx.	.
papež	papež	k1gMnSc1	papež
<g/>
,	,	kIx,	,
krakovský	krakovský	k2eAgMnSc1d1	krakovský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
</s>
</p>
<p>
<s>
Stanisław	Stanisław	k?	Stanisław
Lem	lem	k1gInSc1	lem
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
sci-fi	scii	k1gFnSc2	sci-fi
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
futurolog	futurolog	k1gMnSc1	futurolog
</s>
</p>
<p>
<s>
Wisława	Wisława	k6eAd1	Wisława
Szymborská	Szymborský	k2eAgFnSc1d1	Szymborská
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básnířka	básnířka	k1gFnSc1	básnířka
<g/>
,	,	kIx,	,
esejistka	esejistka	k1gFnSc1	esejistka
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc1d1	literární
kritička	kritička	k1gFnSc1	kritička
a	a	k8xC	a
překladatelka	překladatelka	k1gFnSc1	překladatelka
<g/>
,	,	kIx,	,
držitelka	držitelka	k1gFnSc1	držitelka
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
</s>
</p>
<p>
<s>
Roman	Roman	k1gMnSc1	Roman
Polanski	Polansk	k1gFnSc2	Polansk
(	(	kIx(	(
<g/>
*	*	kIx~	*
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
producent	producent	k1gMnSc1	producent
</s>
</p>
<p>
<s>
Nigel	Nigel	k1gMnSc1	Nigel
Kennedy	Kenneda	k1gMnSc2	Kenneda
(	(	kIx(	(
<g/>
*	*	kIx~	*
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
houslista	houslista	k1gMnSc1	houslista
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Krakau	Krakau	k5eAaPmIp1nS	Krakau
<g/>
:	:	kIx,	:
Ein	Ein	k1gMnSc1	Ein
Führer	Führer	k1gMnSc1	Führer
durch	durch	k6eAd1	durch
Symbole	symbol	k1gInSc5	symbol
<g/>
,	,	kIx,	,
Sehenswürdigkeiten	Sehenswürdigkeiten	k2eAgInSc4d1	Sehenswürdigkeiten
und	und	k?	und
Attraktionen	Attraktionen	k1gInSc4	Attraktionen
<g/>
.	.	kIx.	.
</s>
<s>
Krakov	Krakov	k1gInSc1	Krakov
:	:	kIx,	:
Wydawnictwo	Wydawnictwo	k6eAd1	Wydawnictwo
GAUSS	gauss	k1gInSc1	gauss
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
Jabloński	Jabloński	k1gNnSc1	Jabloński
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Kraków	Kraków	k1gFnPc1	Kraków
and	and	k?	and
Surroundings	Surroundings	k1gInSc1	Surroundings
<g/>
.	.	kIx.	.
</s>
<s>
Warsaw	Warsaw	k?	Warsaw
:	:	kIx,	:
Festina	Festina	k1gFnSc1	Festina
Publishers	Publishersa	k1gFnPc2	Publishersa
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
920325	[number]	k4	920325
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Wawel	Wawel	k1gMnSc1	Wawel
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
</s>
</p>
<p>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
</s>
</p>
<p>
<s>
Obvařanek	Obvařanka	k1gFnPc2	Obvařanka
-	-	kIx~	-
tradiční	tradiční	k2eAgNnSc1d1	tradiční
krakovské	krakovský	k2eAgNnSc1d1	Krakovské
pečivo	pečivo	k1gNnSc1	pečivo
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Krakova	Krakov	k1gInSc2	Krakov
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Krakov	Krakov	k1gInSc1	Krakov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Krakov	Krakov	k1gInSc1	Krakov
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Památky	památka	k1gFnPc1	památka
Krakova	Krakov	k1gInSc2	Krakov
-	-	kIx~	-
přehled	přehled	k1gInSc1	přehled
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
památek	památka	k1gFnPc2	památka
očima	oko	k1gNnPc7	oko
turisty	turist	k1gMnPc4	turist
</s>
</p>
