<s>
Poločas	poločas	k1gInSc1	poločas
přeměny	přeměna	k1gFnSc2	přeměna
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
označovaný	označovaný	k2eAgInSc1d1	označovaný
T1⁄	T1⁄	k1gFnSc3	T1⁄
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
polovina	polovina	k1gFnSc1	polovina
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
atomárních	atomární	k2eAgNnPc2d1	atomární
jader	jádro	k1gNnPc2	jádro
ve	v	k7c6	v
vzorku	vzorek	k1gInSc6	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
izotop	izotop	k1gInSc4	izotop
je	být	k5eAaImIp3nS	být
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
od	od	k7c2	od
zlomku	zlomek	k1gInSc2	zlomek
sekundy	sekunda	k1gFnSc2	sekunda
až	až	k9	až
po	po	k7c6	po
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
termín	termín	k1gInSc4	termín
poločas	poločas	k1gInSc1	poločas
rozpadu	rozpad	k1gInSc2	rozpad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
obecný	obecný	k2eAgInSc1d1	obecný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ne	ne	k9	ne
každá	každý	k3xTgFnSc1	každý
radioaktivní	radioaktivní	k2eAgFnSc1d1	radioaktivní
přeměna	přeměna	k1gFnSc1	přeměna
představuje	představovat	k5eAaImIp3nS	představovat
rozpad	rozpad	k1gInSc1	rozpad
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
vyzáření	vyzáření	k1gNnSc1	vyzáření
fotonu	foton	k1gInSc2	foton
gama	gama	k1gNnSc2	gama
záření	záření	k1gNnSc2	záření
z	z	k7c2	z
excitovaného	excitovaný	k2eAgNnSc2d1	excitované
jádra	jádro	k1gNnSc2	jádro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
konkrétní	konkrétní	k2eAgNnSc4d1	konkrétní
jádro	jádro	k1gNnSc4	jádro
nuklidu	nuklid	k1gInSc2	nuklid
(	(	kIx(	(
<g/>
určitého	určitý	k2eAgInSc2d1	určitý
izotopu	izotop	k1gInSc2	izotop
daného	daný	k2eAgInSc2d1	daný
prvku	prvek	k1gInSc2	prvek
<g/>
)	)	kIx)	)
nelze	lze	k6eNd1	lze
v	v	k7c6	v
principu	princip	k1gInSc6	princip
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
<g/>
.	.	kIx.	.
</s>
<s>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
mechanika	mechanika	k1gFnSc1	mechanika
jako	jako	k8xS	jako
pravděpodobnostní	pravděpodobnostní	k2eAgFnSc1d1	pravděpodobnostní
teorie	teorie	k1gFnSc1	teorie
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
stanovit	stanovit	k5eAaPmF	stanovit
pouze	pouze	k6eAd1	pouze
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
dojde	dojít	k5eAaPmIp3nS	dojít
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
časovém	časový	k2eAgInSc6d1	časový
úseku	úsek	k1gInSc6	úsek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
10	[number]	k4	10
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pravděpodobnostní	pravděpodobnostní	k2eAgInSc1d1	pravděpodobnostní
charakter	charakter	k1gInSc1	charakter
prakticky	prakticky	k6eAd1	prakticky
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
vzorek	vzorek	k1gInSc4	vzorek
látky	látka	k1gFnSc2	látka
(	(	kIx(	(
<g/>
obsahující	obsahující	k2eAgInSc1d1	obsahující
jediný	jediný	k2eAgInSc1d1	jediný
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
nuklid	nuklid	k1gInSc1	nuklid
<g/>
)	)	kIx)	)
běžné	běžný	k2eAgFnSc2d1	běžná
velikosti	velikost	k1gFnSc2	velikost
a	a	k8xC	a
tedy	tedy	k9	tedy
o	o	k7c6	o
velkém	velký	k2eAgInSc6d1	velký
počtu	počet	k1gInSc6	počet
atomů	atom	k1gInPc2	atom
(	(	kIx(	(
<g/>
srovnej	srovnat	k5eAaPmRp2nS	srovnat
Avogadrova	Avogadrův	k2eAgFnSc1d1	Avogadrova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
můžeme	moct	k5eAaImIp1nP	moct
přesně	přesně	k6eAd1	přesně
vypočítat	vypočítat	k5eAaPmF	vypočítat
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
za	za	k7c4	za
jakou	jaký	k3yRgFnSc4	jaký
se	se	k3xPyFc4	se
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
právě	právě	k6eAd1	právě
polovina	polovina	k1gFnSc1	polovina
přítomných	přítomný	k2eAgNnPc2d1	přítomné
jader	jádro	k1gNnPc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Stabilita	stabilita	k1gFnSc1	stabilita
izotopu	izotop	k1gInSc2	izotop
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
právě	právě	k9	právě
na	na	k7c6	na
základě	základ	k1gInSc6	základ
poločasu	poločas	k1gInSc2	poločas
přeměny	přeměna	k1gFnSc2	přeměna
<g/>
.	.	kIx.	.
</s>
<s>
Stabilní	stabilní	k2eAgInPc1d1	stabilní
izotopy	izotop	k1gInPc1	izotop
ho	on	k3xPp3gInSc2	on
mají	mít	k5eAaImIp3nP	mít
nekonečný	konečný	k2eNgInSc4d1	nekonečný
<g/>
,	,	kIx,	,
jádra	jádro	k1gNnPc1	jádro
se	se	k3xPyFc4	se
samovolně	samovolně	k6eAd1	samovolně
nepřeměňují	přeměňovat	k5eNaImIp3nP	přeměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
stabilnější	stabilní	k2eAgInPc4d2	stabilnější
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
izotop	izotop	k1gInSc1	izotop
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
poločasem	poločas	k1gInSc7	poločas
přeměny	přeměna	k1gFnSc2	přeměna
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gInPc7	jeho
nuklidy	nuklid	k1gInPc7	nuklid
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
déle	dlouho	k6eAd2	dlouho
vydrží	vydržet	k5eAaPmIp3nS	vydržet
být	být	k5eAaImF	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
čím	co	k3yRnSc7	co
jsou	být	k5eAaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodní	přírodní	k2eAgFnSc6d1	přírodní
směsi	směs	k1gFnSc6	směs
daného	daný	k2eAgInSc2d1	daný
chemického	chemický	k2eAgInSc2d1	chemický
prvku	prvek	k1gInSc2	prvek
jsou	být	k5eAaImIp3nP	být
určitá	určitý	k2eAgNnPc1d1	určité
procentuální	procentuální	k2eAgNnPc1d1	procentuální
zastoupení	zastoupení	k1gNnPc1	zastoupení
několika	několik	k4yIc7	několik
jeho	jeho	k3xOp3gMnPc2	jeho
izotopů	izotop	k1gInPc2	izotop
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
vodík	vodík	k1gInSc1	vodík
v	v	k7c6	v
oceánské	oceánský	k2eAgFnSc6d1	oceánská
vodě	voda	k1gFnSc6	voda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
99,984	[number]	k4	99,984
<g/>
4	[number]	k4	4
%	%	kIx~	%
protia	protium	k1gNnSc2	protium
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
atomy	atom	k1gInPc4	atom
se	s	k7c7	s
samotným	samotný	k2eAgInSc7d1	samotný
protonem	proton	k1gInSc7	proton
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
)	)	kIx)	)
a	a	k8xC	a
0,0156	[number]	k4	0,0156
%	%	kIx~	%
deuteria	deuterium	k1gNnSc2	deuterium
<g/>
,	,	kIx,	,
takzvaného	takzvaný	k2eAgInSc2d1	takzvaný
těžkého	těžký	k2eAgInSc2d1	těžký
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
navíc	navíc	k6eAd1	navíc
vázaný	vázaný	k2eAgInSc4d1	vázaný
jeden	jeden	k4xCgInSc4	jeden
neutron	neutron	k1gInSc4	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
izotopy	izotop	k1gInPc1	izotop
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
stabilní	stabilní	k2eAgFnPc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
existuje	existovat	k5eAaImIp3nS	existovat
izotop	izotop	k1gInSc1	izotop
vodíku	vodík	k1gInSc2	vodík
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
nukleony	nukleon	k1gInPc7	nukleon
zvaný	zvaný	k2eAgInSc1d1	zvaný
tritium	tritium	k1gNnSc4	tritium
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodní	přírodní	k2eAgFnSc6d1	přírodní
směsi	směs	k1gFnSc6	směs
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
uměle	uměle	k6eAd1	uměle
<g/>
.	.	kIx.	.
</s>
<s>
Tritium	tritium	k1gNnSc1	tritium
je	být	k5eAaImIp3nS	být
radioaktivním	radioaktivní	k2eAgInSc7d1	radioaktivní
zářičem	zářič	k1gInSc7	zářič
β	β	k?	β
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
přeměny	přeměna	k1gFnSc2	přeměna
12,36	[number]	k4	12,36
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
chemické	chemický	k2eAgInPc1d1	chemický
prvky	prvek	k1gInPc1	prvek
vůbec	vůbec	k9	vůbec
nemají	mít	k5eNaImIp3nP	mít
stabilní	stabilní	k2eAgInPc1d1	stabilní
izotopy	izotop	k1gInPc1	izotop
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
radon	radon	k1gInSc1	radon
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
jak	jak	k8xC	jak
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
stabilních	stabilní	k2eAgInPc2d1	stabilní
izotopů	izotop	k1gInPc2	izotop
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
nestabilních	stabilní	k2eNgFnPc2d1	nestabilní
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
uhlík	uhlík	k1gInSc4	uhlík
v	v	k7c6	v
atmosférickém	atmosférický	k2eAgInSc6d1	atmosférický
oxidu	oxid	k1gInSc6	oxid
uhličitém	uhličitý	k2eAgInSc6d1	uhličitý
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
díky	díky	k7c3	díky
kosmickému	kosmický	k2eAgNnSc3d1	kosmické
záření	záření	k1gNnSc3	záření
stálý	stálý	k2eAgInSc4d1	stálý
podíl	podíl	k1gInSc4	podíl
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
izotopu	izotop	k1gInSc2	izotop
C	C	kA	C
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc4	měření
jeho	on	k3xPp3gNnSc2	on
procentuálního	procentuální	k2eAgNnSc2d1	procentuální
zastoupení	zastoupení	k1gNnSc2	zastoupení
v	v	k7c6	v
předmětech	předmět	k1gInPc6	předmět
organického	organický	k2eAgInSc2d1	organický
původu	původ	k1gInSc2	původ
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
určit	určit	k5eAaPmF	určit
jejich	jejich	k3xOp3gNnSc4	jejich
stáří	stáří	k1gNnSc4	stáří
díky	díky	k7c3	díky
známému	známý	k2eAgInSc3d1	známý
poločasu	poločas	k1gInSc3	poločas
přeměny	přeměna	k1gFnSc2	přeměna
(	(	kIx(	(
<g/>
5715	[number]	k4	5715
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
měření	měření	k1gNnSc2	měření
stáří	stáří	k1gNnSc2	stáří
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
radiokarbonová	radiokarbonový	k2eAgFnSc1d1	radiokarbonová
metoda	metoda	k1gFnSc1	metoda
datování	datování	k1gNnSc2	datování
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
radioizotopů	radioizotop	k1gInPc2	radioizotop
<g/>
,	,	kIx,	,
řazeno	řazen	k2eAgNnSc1d1	řazeno
dle	dle	k7c2	dle
poločasu	poločas	k1gInSc2	poločas
přeměny	přeměna	k1gFnSc2	přeměna
<g/>
.	.	kIx.	.
</s>
<s>
Obecněji	obecně	k6eAd2	obecně
definovanou	definovaný	k2eAgFnSc7d1	definovaná
veličinou	veličina	k1gFnSc7	veličina
stejného	stejný	k2eAgInSc2d1	stejný
charakteru	charakter	k1gInSc2	charakter
je	být	k5eAaImIp3nS	být
střední	střední	k2eAgFnSc1d1	střední
doba	doba	k1gFnSc1	doba
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
značená	značený	k2eAgFnSc1d1	značená
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
τ	τ	k?	τ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
tau	tau	k1gNnPc3	tau
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
exponenciální	exponenciální	k2eAgFnSc4d1	exponenciální
přeměnu	přeměna	k1gFnSc4	přeměna
lze	lze	k6eAd1	lze
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
přeměny	přeměna	k1gFnSc2	přeměna
zapsat	zapsat	k5eAaPmF	zapsat
vztahem	vztah	k1gInSc7	vztah
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
τ	τ	k?	τ
:	:	kIx,	:
ln	ln	k?	ln
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T_	T_	k1gMnSc6	T_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc1	tau
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
2	[number]	k4	2
<g/>
}	}	kIx)	}
:	:	kIx,	:
</s>
