<s>
Router	Router	k1gInSc1	Router
(	(	kIx(	(
<g/>
směrovač	směrovač	k1gInSc1	směrovač
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
počítačových	počítačový	k2eAgFnPc6d1	počítačová
sítích	síť	k1gFnPc6	síť
aktivní	aktivní	k2eAgFnSc1d1	aktivní
síťové	síťový	k2eAgNnSc4d1	síťové
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
procesem	proces	k1gInSc7	proces
zvaným	zvaný	k2eAgInSc7d1	zvaný
routování	routování	k1gNnSc4	routování
přeposílá	přeposílat	k5eAaPmIp3nS	přeposílat
datagramy	datagram	k1gInPc1	datagram
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
cíli	cíl	k1gInSc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Routování	Routování	k1gNnSc1	Routování
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
třetí	třetí	k4xOgFnSc6	třetí
vrstvě	vrstva	k1gFnSc6	vrstva
referenčního	referenční	k2eAgInSc2d1	referenční
modelu	model	k1gInSc2	model
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
OSI	OSI	kA	OSI
(	(	kIx(	(
<g/>
síťová	síťový	k2eAgFnSc1d1	síťová
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Netechnicky	technicky	k6eNd1	technicky
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
router	router	k1gInSc1	router
spojuje	spojovat	k5eAaImIp3nS	spojovat
dvě	dva	k4xCgFnPc4	dva
sítě	síť	k1gFnPc4	síť
a	a	k8xC	a
přenáší	přenášet	k5eAaImIp3nS	přenášet
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
data	datum	k1gNnPc4	datum
<g/>
.	.	kIx.	.
</s>
<s>
Router	Router	k1gMnSc1	Router
se	se	k3xPyFc4	se
podstatně	podstatně	k6eAd1	podstatně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
switche	switch	k1gInSc2	switch
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
počítače	počítač	k1gInPc4	počítač
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
síti	síť	k1gFnSc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílné	rozdílný	k2eAgFnPc1d1	rozdílná
funkce	funkce	k1gFnPc1	funkce
routerů	router	k1gMnPc2	router
a	a	k8xC	a
switchů	switcha	k1gMnPc2	switcha
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
představit	představit	k5eAaPmF	představit
jako	jako	k8xS	jako
switche	switche	k6eAd1	switche
coby	coby	k?	coby
silnice	silnice	k1gFnSc1	silnice
spojující	spojující	k2eAgFnSc1d1	spojující
všechna	všechen	k3xTgNnPc4	všechen
města	město	k1gNnPc4	město
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
a	a	k8xC	a
routery	router	k1gInPc1	router
coby	coby	k?	coby
hraniční	hraniční	k2eAgInPc1d1	hraniční
přechody	přechod	k1gInPc1	přechod
spojující	spojující	k2eAgFnSc2d1	spojující
různé	různý	k2eAgFnSc2d1	různá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Routování	Routování	k1gNnSc1	Routování
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
spojováno	spojovat	k5eAaImNgNnS	spojovat
s	s	k7c7	s
protokolem	protokol	k1gInSc7	protokol
IP	IP	kA	IP
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
populární	populární	k2eAgInPc1d1	populární
protokoly	protokol	k1gInPc1	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
jako	jako	k9	jako
router	router	k1gMnSc1	router
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
počítač	počítač	k1gInSc4	počítač
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
síťování	síťování	k1gNnSc2	síťování
a	a	k8xC	a
pro	pro	k7c4	pro
routování	routování	k1gNnSc4	routování
v	v	k7c6	v
menších	malý	k2eAgFnPc6d2	menší
sítích	síť	k1gFnPc6	síť
se	se	k3xPyFc4	se
často	často	k6eAd1	často
dodnes	dodnes	k6eAd1	dodnes
používají	používat	k5eAaImIp3nP	používat
běžné	běžný	k2eAgInPc1d1	běžný
osobní	osobní	k2eAgInPc1d1	osobní
počítače	počítač	k1gInPc1	počítač
<g/>
,	,	kIx,	,
do	do	k7c2	do
vysokorychlostních	vysokorychlostní	k2eAgFnPc2d1	vysokorychlostní
sítí	síť	k1gFnPc2	síť
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
jako	jako	k9	jako
routery	router	k1gInPc1	router
používány	používán	k2eAgInPc1d1	používán
vysoce	vysoce	k6eAd1	vysoce
účelové	účelový	k2eAgInPc1d1	účelový
počítače	počítač	k1gInPc1	počítač
obvykle	obvykle	k6eAd1	obvykle
se	s	k7c7	s
speciálním	speciální	k2eAgInSc7d1	speciální
hardwarem	hardware	k1gInSc7	hardware
<g/>
,	,	kIx,	,
optimalizovaným	optimalizovaný	k2eAgNnSc7d1	optimalizované
jak	jak	k8xC	jak
pro	pro	k7c4	pro
běžné	běžný	k2eAgNnSc4d1	běžné
přeposílání	přeposílání	k1gNnSc4	přeposílání
(	(	kIx(	(
<g/>
forwarding	forwarding	k1gInSc4	forwarding
<g/>
)	)	kIx)	)
datagramů	datagram	k1gInPc2	datagram
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pro	pro	k7c4	pro
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
funkce	funkce	k1gFnPc4	funkce
jako	jako	k8xC	jako
šifrování	šifrování	k1gNnSc4	šifrování
u	u	k7c2	u
IPsec	IPsec	k1gInSc4	IPsec
tunelů	tunel	k1gInPc2	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
změny	změna	k1gFnPc1	změna
také	také	k9	také
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
spolehlivost	spolehlivost	k1gFnSc4	spolehlivost
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
používání	používání	k1gNnSc1	používání
stejnosměrného	stejnosměrný	k2eAgNnSc2d1	stejnosměrné
napájení	napájení	k1gNnSc2	napájení
(	(	kIx(	(
<g/>
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
datových	datový	k2eAgInPc6d1	datový
centrech	centr	k1gInPc6	centr
odebírat	odebírat	k5eAaImF	odebírat
z	z	k7c2	z
baterií	baterie	k1gFnPc2	baterie
<g/>
)	)	kIx)	)
místo	místo	k7c2	místo
napájení	napájení	k1gNnSc2	napájení
přímo	přímo	k6eAd1	přímo
ze	z	k7c2	z
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
používání	používání	k1gNnSc1	používání
flash	flasha	k1gFnPc2	flasha
pamětí	paměť	k1gFnPc2	paměť
místo	místo	k7c2	místo
pevných	pevný	k2eAgInPc2d1	pevný
disků	disk	k1gInPc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
moderní	moderní	k2eAgInPc1d1	moderní
routery	router	k1gInPc1	router
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
podobají	podobat	k5eAaImIp3nP	podobat
spíše	spíše	k9	spíše
telefonním	telefonní	k2eAgFnPc3d1	telefonní
ústřednám	ústředna	k1gFnPc3	ústředna
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
technologie	technologie	k1gFnSc1	technologie
k	k	k7c3	k
routerům	router	k1gInPc3	router
(	(	kIx(	(
<g/>
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
stále	stále	k6eAd1	stále
častějšímu	častý	k2eAgNnSc3d2	častější
nasazování	nasazování	k1gNnSc3	nasazování
protokolu	protokol	k1gInSc2	protokol
IP	IP	kA	IP
i	i	k8xC	i
ke	k	k7c3	k
spojování	spojování	k1gNnSc3	spojování
hovorů	hovor	k1gInPc2	hovor
<g/>
)	)	kIx)	)
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
a	a	k8xC	a
které	který	k3yRgInPc1	který
routery	router	k1gInPc1	router
případně	případně	k6eAd1	případně
nahradí	nahradit	k5eAaPmIp3nP	nahradit
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
malé	malý	k2eAgInPc1d1	malý
routery	router	k1gInPc1	router
<g/>
,	,	kIx,	,
kombinované	kombinovaný	k2eAgFnPc1d1	kombinovaná
například	například	k6eAd1	například
s	s	k7c7	s
kabelovými	kabelový	k2eAgInPc7d1	kabelový
nebo	nebo	k8xC	nebo
DSL	DSL	kA	DSL
modemy	modem	k1gInPc1	modem
<g/>
,	,	kIx,	,
eventuálně	eventuálně	k6eAd1	eventuálně
WiFi	WiFi	k1gNnSc1	WiFi
přístupovými	přístupový	k2eAgInPc7d1	přístupový
body	bod	k1gInPc7	bod
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
běžným	běžný	k2eAgNnSc7d1	běžné
vybavením	vybavení	k1gNnSc7	vybavení
domácností	domácnost	k1gFnPc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
moderními	moderní	k2eAgFnPc7d1	moderní
(	(	kIx(	(
<g/>
vyhrazenými	vyhrazený	k2eAgFnPc7d1	vyhrazená
<g/>
,	,	kIx,	,
samostatnými	samostatný	k2eAgInPc7d1	samostatný
<g/>
)	)	kIx)	)
routery	router	k1gInPc7	router
byly	být	k5eAaImAgInP	být
routery	router	k1gInPc1	router
Fuzzball	Fuzzballa	k1gFnPc2	Fuzzballa
<g/>
.	.	kIx.	.
</s>
<s>
Router	Router	k1gInSc1	Router
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
alespoň	alespoň	k9	alespoň
dvou	dva	k4xCgFnPc2	dva
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Speciálním	speciální	k2eAgInSc7d1	speciální
případem	případ	k1gInSc7	případ
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
jednoruký	jednoruký	k2eAgInSc4d1	jednoruký
<g/>
"	"	kIx"	"
router	router	k1gInSc4	router
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
používá	používat	k5eAaImIp3nS	používat
jednu	jeden	k4xCgFnSc4	jeden
zásuvku	zásuvka	k1gFnSc4	zásuvka
(	(	kIx(	(
<g/>
port	port	k1gInSc4	port
<g/>
)	)	kIx)	)
a	a	k8xC	a
routuje	routovat	k5eAaImIp3nS	routovat
pakety	paket	k1gInPc4	paket
mezi	mezi	k7c7	mezi
virtuálními	virtuální	k2eAgFnPc7d1	virtuální
sítěmi	síť	k1gFnPc7	síť
VLAN	VLAN	kA	VLAN
provozovanými	provozovaný	k2eAgFnPc7d1	provozovaná
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
zásuvce	zásuvka	k1gFnSc6	zásuvka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mobilních	mobilní	k2eAgInPc6d1	mobilní
ad-hoc	adoc	k6eAd1	ad-hoc
sítích	síť	k1gFnPc6	síť
si	se	k3xPyFc3	se
každý	každý	k3xTgInSc1	každý
počítač	počítač	k1gInSc1	počítač
routuje	routovat	k5eAaPmIp3nS	routovat
a	a	k8xC	a
forwarduje	forwardovat	k5eAaPmIp3nS	forwardovat
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
metalických	metalický	k2eAgFnPc6d1	metalická
a	a	k8xC	a
optických	optický	k2eAgFnPc6d1	optická
sítích	síť	k1gFnPc6	síť
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
router	router	k1gInSc1	router
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
broadcastovou	broadcastový	k2eAgFnSc4d1	broadcastový
doménu	doména	k1gFnSc4	doména
<g/>
.	.	kIx.	.
</s>
<s>
Routeru	Router	k1gInSc2	Router
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
připojuje	připojovat	k5eAaImIp3nS	připojovat
klienty	klient	k1gMnPc4	klient
k	k	k7c3	k
vnější	vnější	k2eAgFnPc4d1	vnější
sítí	síť	k1gFnSc7	síť
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
Internetu	Internet	k1gInSc3	Internet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
okrajový	okrajový	k2eAgInSc4d1	okrajový
router	router	k1gInSc4	router
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
edge	edgat	k5eAaPmIp3nS	edgat
router	router	k1gMnSc1	router
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
"	"	kIx"	"
<g/>
brána	brána	k1gFnSc1	brána
<g/>
"	"	kIx"	"
–	–	k?	–
gateway	gatewaa	k1gFnSc2	gatewaa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zastaralé	zastaralý	k2eAgNnSc4d1	zastaralé
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
routery	router	k1gMnPc4	router
obecně	obecně	k6eAd1	obecně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Router	Router	k1gInSc4	Router
přenášející	přenášející	k2eAgNnPc4d1	přenášející
data	datum	k1gNnPc4	datum
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgInPc7d1	jiný
routery	router	k1gInPc7	router
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
router	router	k1gInSc1	router
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
core	corat	k5eAaPmIp3nS	corat
router	router	k1gMnSc1	router
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Router	Router	k1gInSc1	Router
používá	používat	k5eAaImIp3nS	používat
routovací	routovací	k2eAgFnSc4d1	routovací
tabulku	tabulka	k1gFnSc4	tabulka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
cesty	cesta	k1gFnPc4	cesta
k	k	k7c3	k
jistým	jistý	k2eAgInPc3d1	jistý
cílům	cíl	k1gInPc3	cíl
a	a	k8xC	a
routovací	routovací	k2eAgFnPc4d1	routovací
metriky	metrika	k1gFnPc4	metrika
spojené	spojený	k2eAgNnSc1d1	spojené
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
cestami	cesta	k1gFnPc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
routování	routování	k1gNnSc1	routování
<g/>
.	.	kIx.	.
</s>
<s>
Nedávno	nedávno	k6eAd1	nedávno
se	se	k3xPyFc4	se
routovací	routovací	k2eAgFnPc1d1	routovací
funkce	funkce	k1gFnPc1	funkce
začaly	začít	k5eAaPmAgFnP	začít
přidávat	přidávat	k5eAaImF	přidávat
ke	k	k7c3	k
switchům	switch	k1gInPc3	switch
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
switche	switch	k1gInSc2	switch
"	"	kIx"	"
<g/>
Layer	Layer	k1gInSc1	Layer
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
routují	routovat	k5eAaPmIp3nP	routovat
provoz	provoz	k1gInSc4	provoz
rychlostí	rychlost	k1gFnSc7	rychlost
srovnatelnou	srovnatelný	k2eAgFnSc4d1	srovnatelná
s	s	k7c7	s
rychlostí	rychlost	k1gFnSc7	rychlost
linky	linka	k1gFnSc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Routery	Router	k1gInPc1	Router
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
implementují	implementovat	k5eAaImIp3nP	implementovat
také	také	k9	také
jako	jako	k9	jako
"	"	kIx"	"
<g/>
internetové	internetový	k2eAgFnPc1d1	internetová
brány	brána	k1gFnPc1	brána
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
primárně	primárně	k6eAd1	primárně
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
sítě	síť	k1gFnPc4	síť
jako	jako	k8xC	jako
ty	ten	k3xDgFnPc4	ten
používané	používaný	k2eAgFnPc4d1	používaná
doma	doma	k6eAd1	doma
a	a	k8xC	a
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
kancelářích	kancelář	k1gFnPc6	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
hlavě	hlava	k1gFnSc3	hlava
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
internetové	internetový	k2eAgNnSc1d1	internetové
připojení	připojení	k1gNnSc1	připojení
rychlé	rychlý	k2eAgFnSc2d1	rychlá
a	a	k8xC	a
"	"	kIx"	"
<g/>
vždy	vždy	k6eAd1	vždy
připojené	připojený	k2eAgInPc4d1	připojený
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
kabelový	kabelový	k2eAgInSc4d1	kabelový
modem	modem	k1gInSc4	modem
nebo	nebo	k8xC	nebo
DSL	DSL	kA	DSL
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
zařízení	zařízení	k1gNnPc1	zařízení
ale	ale	k9	ale
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
principu	princip	k1gInSc6	princip
routery	router	k1gInPc4	router
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
počítače	počítač	k1gInPc1	počítač
ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
síti	síť	k1gFnSc6	síť
efektivně	efektivně	k6eAd1	efektivně
skrývají	skrývat	k5eAaImIp3nP	skrývat
pod	pod	k7c4	pod
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
IP	IP	kA	IP
adresu	adresa	k1gFnSc4	adresa
ve	v	k7c6	v
vnější	vnější	k2eAgFnSc6d1	vnější
síti	síť	k1gFnSc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
technika	technika	k1gFnSc1	technika
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
NAT	NAT	kA	NAT
(	(	kIx(	(
<g/>
network	network	k1gInSc1	network
address	addressa	k1gFnPc2	addressa
translation	translation	k1gInSc1	translation
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
adres	adresa	k1gFnPc2	adresa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výrobců	výrobce	k1gMnPc2	výrobce
routerů	router	k1gInPc2	router
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c4	mnoho
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
Com	Com	k1gFnPc2	Com
<g/>
,	,	kIx,	,
Alcatel	Alcatel	kA	Alcatel
<g/>
,	,	kIx,	,
Cisco	Cisco	k6eAd1	Cisco
Systems	Systems	k1gInSc1	Systems
<g/>
,	,	kIx,	,
Juniper	Juniper	k1gInSc1	Juniper
Networks	Networks	k1gInSc1	Networks
<g/>
,	,	kIx,	,
MikroTik	MikroTik	k1gInSc1	MikroTik
<g/>
,	,	kIx,	,
NETGEAR	NETGEAR	kA	NETGEAR
<g/>
,	,	kIx,	,
Nortel	Nortel	k1gMnSc1	Nortel
<g/>
,	,	kIx,	,
SMC	SMC	kA	SMC
Networks	Networks	k1gInSc1	Networks
<g/>
,	,	kIx,	,
...	...	k?	...
S	s	k7c7	s
vhodným	vhodný	k2eAgInSc7d1	vhodný
softwarem	software	k1gInSc7	software
se	se	k3xPyFc4	se
i	i	k9	i
z	z	k7c2	z
obyčejného	obyčejný	k2eAgInSc2d1	obyčejný
osobního	osobní	k2eAgInSc2d1	osobní
počítače	počítač	k1gInSc2	počítač
<g />
.	.	kIx.	.
</s>
<s>
dá	dát	k5eAaPmIp3nS	dát
udělat	udělat	k5eAaPmF	udělat
router	router	k1gInSc1	router
<g/>
:	:	kIx,	:
Sdílení	sdílení	k1gNnSc1	sdílení
internetového	internetový	k2eAgNnSc2d1	internetové
připojení	připojení	k1gNnSc2	připojení
ve	v	k7c6	v
Windows	Windows	kA	Windows
XP	XP	kA	XP
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
Internet	Internet	k1gInSc1	Internet
Sharing	Sharing	k1gInSc1	Sharing
BIRD	BIRD	kA	BIRD
Internet	Internet	k1gInSc1	Internet
Routing	Routing	k1gInSc1	Routing
Daemon	Daemon	k1gInSc1	Daemon
fdgw	fdgw	k?	fdgw
CoyoteLinux	CoyoteLinux	k1gInSc1	CoyoteLinux
FREESCO	FREESCO	kA	FREESCO
GNU	gnu	k1gNnSc2	gnu
Zebra	zebra	k1gFnSc1	zebra
–	–	k?	–
open	open	k1gNnSc1	open
source	sourec	k1gInSc2	sourec
implementace	implementace	k1gFnSc2	implementace
routovacích	routovací	k2eAgInPc2d1	routovací
protokolů	protokol	k1gInPc2	protokol
RIP	RIP	kA	RIP
<g/>
,	,	kIx,	,
OSPF	OSPF	kA	OSPF
a	a	k8xC	a
BGP	BGP	kA	BGP
Quagga	Quagga	k1gFnSc1	Quagga
–	–	k?	–
open	open	k1gNnSc1	open
source	sourec	k1gInSc2	sourec
implementace	implementace	k1gFnSc2	implementace
routovacích	routovací	k2eAgInPc2d1	routovací
protokolů	protokol	k1gInPc2	protokol
RIP	RIP	kA	RIP
<g/>
,	,	kIx,	,
OSPF	OSPF	kA	OSPF
a	a	k8xC	a
BGP	BGP	kA	BGP
<g />
.	.	kIx.	.
</s>
<s>
pokračovatel	pokračovatel	k1gMnSc1	pokračovatel
Zebry	zebra	k1gFnSc2	zebra
IPCop	IPCop	k1gInSc1	IPCop
SmoothWall	SmoothWall	k1gMnSc1	SmoothWall
The	The	k1gMnSc1	The
Linux	Linux	kA	Linux
Router	Router	k1gMnSc1	Router
Project	Project	k1gMnSc1	Project
m	m	kA	m
<g/>
0	[number]	k4	0
<g/>
n	n	k0	n
<g/>
0	[number]	k4	0
<g/>
wall	walnout	k5eAaPmAgMnS	walnout
FreeBSD	FreeBSD	k1gMnSc3	FreeBSD
NetBSD	NetBSD	k1gMnSc1	NetBSD
OpenBSD	OpenBSD	k1gMnSc3	OpenBSD
Úplně	úplně	k6eAd1	úplně
první	první	k4xOgNnSc1	první
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
stejné	stejný	k2eAgFnSc2d1	stejná
funkce	funkce	k1gFnSc2	funkce
jako	jako	k8xS	jako
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
router	router	k1gInSc1	router
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
packet	packet	k1gMnSc1	packet
switch	switch	k1gMnSc1	switch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Interface	interface	k1gInSc1	interface
Message	Message	k1gFnSc1	Message
Processor	Processor	k1gMnSc1	Processor
(	(	kIx(	(
<g/>
IMP	IMP	kA	IMP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
IMP	IMP	kA	IMP
byla	být	k5eAaImAgFnS	být
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
propojovala	propojovat	k5eAaImAgFnS	propojovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
ARPANETu	ARPANETus	k1gInSc2	ARPANETus
–	–	k?	–
první	první	k4xOgFnSc1	první
počítačová	počítačový	k2eAgFnSc1d1	počítačová
síť	síť	k1gFnSc1	síť
využívající	využívající	k2eAgNnSc1d1	využívající
přepínání	přepínání	k1gNnSc1	přepínání
paketů	paket	k1gInPc2	paket
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
routeru	router	k1gInSc2	router
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
říkalo	říkat	k5eAaImAgNnS	říkat
brány	brána	k1gFnSc2	brána
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
skupině	skupina	k1gFnSc6	skupina
počítačových	počítačový	k2eAgMnPc2d1	počítačový
síťových	síťový	k2eAgMnPc2d1	síťový
vývojářů	vývojář	k1gMnPc2	vývojář
"	"	kIx"	"
<g/>
International	International	k1gFnSc1	International
Network	network	k1gInSc1	network
Working	Working	k1gInSc1	Working
Group	Group	k1gInSc4	Group
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
INWG	INWG	kA	INWG
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
neoficiální	oficiální	k2eNgFnSc1d1	neoficiální
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
technickými	technický	k2eAgFnPc7d1	technická
otázkami	otázka	k1gFnPc7	otázka
spojenými	spojený	k2eAgInPc7d1	spojený
s	s	k7c7	s
propojováním	propojování	k1gNnSc7	propojování
různých	různý	k2eAgFnPc2d1	různá
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
stala	stát	k5eAaPmAgFnS	stát
podvýborem	podvýbor	k1gInSc7	podvýbor
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
pro	pro	k7c4	pro
zpracování	zpracování	k1gNnSc4	zpracování
informací	informace	k1gFnPc2	informace
(	(	kIx(	(
<g/>
IFIP	IFIP	kA	IFIP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
IMP	IMP	kA	IMP
se	se	k3xPyFc4	se
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
předchozích	předchozí	k2eAgInPc2d1	předchozí
paketových	paketový	k2eAgInPc2d1	paketový
přepínačů	přepínač	k1gInPc2	přepínač
lišil	lišit	k5eAaImAgMnS	lišit
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
směrech	směr	k1gInPc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prvé	prvý	k4xOgFnPc4	prvý
spojovaly	spojovat	k5eAaImAgInP	spojovat
různé	různý	k2eAgInPc1d1	různý
typy	typ	k1gInPc1	typ
sítí	síť	k1gFnPc2	síť
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
sériové	sériový	k2eAgFnSc2d1	sériová
linky	linka	k1gFnSc2	linka
a	a	k8xC	a
LAN	lano	k1gNnPc2	lano
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
druhé	druhý	k4xOgNnSc4	druhý
se	se	k3xPyFc4	se
tato	tento	k3xDgNnPc1	tento
zařízení	zařízení	k1gNnPc1	zařízení
nijak	nijak	k6eAd1	nijak
nepodílela	podílet	k5eNaImAgNnP	podílet
na	na	k7c4	na
vytváření	vytváření	k1gNnSc4	vytváření
spojení	spojení	k1gNnSc2	spojení
a	a	k8xC	a
nehrála	hrát	k5eNaImAgFnS	hrát
ani	ani	k9	ani
žádnou	žádný	k3yNgFnSc4	žádný
roli	role	k1gFnSc4	role
při	při	k7c6	při
zajišťování	zajišťování	k1gNnSc6	zajišťování
spolehlivosti	spolehlivost	k1gFnSc2	spolehlivost
doručování	doručování	k1gNnSc2	doručování
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zcela	zcela	k6eAd1	zcela
ponechávala	ponechávat	k5eAaImAgFnS	ponechávat
na	na	k7c6	na
koncových	koncový	k2eAgInPc6d1	koncový
bodech	bod	k1gInPc6	bod
komunikace	komunikace	k1gFnSc2	komunikace
(	(	kIx(	(
<g/>
i	i	k8xC	i
když	když	k8xS	když
tento	tento	k3xDgInSc1	tento
nápad	nápad	k1gInSc1	nápad
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
již	již	k6eAd1	již
využit	využít	k5eAaPmNgInS	využít
v	v	k7c6	v
sítích	síť	k1gFnPc6	síť
CYCLADES	CYCLADES	kA	CYCLADES
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úmyslem	úmysl	k1gInSc7	úmysl
detailnějšího	detailní	k2eAgNnSc2d2	detailnější
zaměření	zaměření	k1gNnSc2	zaměření
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
myšlenku	myšlenka	k1gFnSc4	myšlenka
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
skutečný	skutečný	k2eAgInSc4d1	skutečný
prototyp	prototyp	k1gInSc4	prototyp
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
dvou	dva	k4xCgInPc2	dva
současných	současný	k2eAgMnPc2d1	současný
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
položil	položit	k5eAaPmAgInS	položit
základy	základ	k1gInPc4	základ
dnešní	dnešní	k2eAgFnSc1d1	dnešní
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
architektury	architektura	k1gFnSc2	architektura
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
agenturou	agentura	k1gFnSc7	agentura
DARPA	DARPA	kA	DARPA
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zavedl	zavést	k5eAaPmAgMnS	zavést
PARC	PARC	kA	PARC
Universal	Universal	k1gMnSc1	Universal
Packet	Packet	k1gMnSc1	Packet
system	syst	k1gMnSc7	syst
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
firmou	firma	k1gFnSc7	firma
Xerox	Xerox	kA	Xerox
PARC	PARC	kA	PARC
pro	pro	k7c4	pro
hledání	hledání	k1gNnSc4	hledání
nových	nový	k2eAgFnPc2d1	nová
síťových	síťový	k2eAgFnPc2d1	síťová
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
Xerox	Xerox	kA	Xerox
routery	router	k1gInPc1	router
byly	být	k5eAaImAgInP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
někdy	někdy	k6eAd1	někdy
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
opravdový	opravdový	k2eAgInSc1d1	opravdový
IP	IP	kA	IP
router	router	k1gInSc1	router
byl	být	k5eAaImAgMnS	být
vyvinut	vyvinout	k5eAaPmNgMnS	vyvinout
vývojářkou	vývojářka	k1gFnSc7	vývojářka
Virginia	Virginium	k1gNnSc2	Virginium
Strazisar	Strazisar	k1gInSc1	Strazisar
z	z	k7c2	z
firmy	firma	k1gFnSc2	firma
BBN	BBN	kA	BBN
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
rokem	rok	k1gInSc7	rok
1975	[number]	k4	1975
až	až	k8xS	až
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
sloužily	sloužit	k5eAaImAgInP	sloužit
tři	tři	k4xCgFnPc4	tři
PDP-11	PDP-11	k1gFnPc4	PDP-11
(	(	kIx(	(
<g/>
série	série	k1gFnSc1	série
16	[number]	k4	16
<g/>
-bit	itum	k1gNnPc2	-bitum
minipočítačů	minipočítač	k1gInPc2	minipočítač
<g/>
)	)	kIx)	)
propojené	propojený	k2eAgInPc4d1	propojený
routery	router	k1gInPc4	router
v	v	k7c6	v
experimentálním	experimentální	k2eAgInSc6d1	experimentální
modelu	model	k1gInSc6	model
internetu	internet	k1gInSc2	internet
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
multiprotokolové	multiprotokolový	k2eAgInPc1d1	multiprotokolový
routery	router	k1gInPc1	router
byly	být	k5eAaImAgInP	být
nezávisle	závisle	k6eNd1	závisle
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
skupinou	skupina	k1gFnSc7	skupina
vývojářů	vývojář	k1gMnPc2	vývojář
na	na	k7c6	na
MIT	MIT	kA	MIT
a	a	k8xC	a
Stanfordu	Stanforda	k1gFnSc4	Stanforda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
;	;	kIx,	;
Stanfordský	Stanfordský	k2eAgInSc1d1	Stanfordský
router	router	k1gInSc1	router
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
Williamem	William	k1gInSc7	William
Yeagerem	Yeager	k1gInSc7	Yeager
a	a	k8xC	a
MIT	MIT	kA	MIT
router	router	k1gInSc4	router
Noelem	Noel	k1gMnSc7	Noel
Chiappem	Chiapp	k1gMnSc7	Chiapp
<g/>
.	.	kIx.	.
</s>
<s>
Faktem	fakt	k1gInSc7	fakt
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
již	již	k9	již
většina	většina	k1gFnSc1	většina
počítačových	počítačový	k2eAgFnPc2d1	počítačová
sítí	síť	k1gFnPc2	síť
používá	používat	k5eAaImIp3nS	používat
protokol	protokol	k1gInSc1	protokol
IP	IP	kA	IP
<g/>
.	.	kIx.	.
</s>
<s>
Multiprotokolové	Multiprotokolový	k2eAgInPc1d1	Multiprotokolový
routery	router	k1gInPc1	router
jsou	být	k5eAaImIp3nP	být
už	už	k6eAd1	už
většinou	většinou	k6eAd1	většinou
zastaralé	zastaralý	k2eAgFnPc1d1	zastaralá
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
důležité	důležitý	k2eAgFnPc1d1	důležitá
v	v	k7c6	v
počátečních	počáteční	k2eAgFnPc6d1	počáteční
fázích	fáze	k1gFnPc6	fáze
vývoje	vývoj	k1gInSc2	vývoj
počítačových	počítačový	k2eAgFnPc2d1	počítačová
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
hodně	hodně	k6eAd1	hodně
využívány	využíván	k2eAgInPc4d1	využíván
jinými	jiný	k2eAgInPc7d1	jiný
protokoly	protokol	k1gInPc7	protokol
než	než	k8xS	než
byly	být	k5eAaImAgInP	být
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
<g/>
.	.	kIx.	.
</s>
<s>
Routery	Router	k1gInPc1	Router
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zvládnou	zvládnout	k5eAaPmIp3nP	zvládnout
IPv	IPv	k1gFnSc4	IPv
<g/>
4	[number]	k4	4
i	i	k8xC	i
IPv	IPv	k1gFnSc4	IPv
<g/>
6	[number]	k4	6
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
multiprotokolové	multiprotokolový	k2eAgFnPc1d1	multiprotokolová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
než	než	k8xS	než
routery	router	k1gInPc1	router
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zpracovávaly	zpracovávat	k5eAaImAgFnP	zpracovávat
AppleTalk	AppleTalk	k1gInSc4	AppleTalk
<g/>
,	,	kIx,	,
DECnet	DECnet	k1gInSc4	DECnet
<g/>
,	,	kIx,	,
Xerox	Xerox	kA	Xerox
a	a	k8xC	a
IP	IP	kA	IP
protokoly	protokol	k1gInPc1	protokol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
éře	éra	k1gFnSc6	éra
routování	routování	k1gNnSc2	routování
(	(	kIx(	(
<g/>
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
až	až	k6eAd1	až
do	do	k7c2	do
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
sloužily	sloužit	k5eAaImAgInP	sloužit
jako	jako	k9	jako
routry	routr	k1gInPc1	routr
víceúčelové	víceúčelový	k2eAgInPc1d1	víceúčelový
minipočítače	minipočítač	k1gInPc1	minipočítač
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
víceúčelové	víceúčelový	k2eAgInPc1d1	víceúčelový
počítače	počítač	k1gInPc1	počítač
mohou	moct	k5eAaImIp3nP	moct
provádět	provádět	k5eAaImF	provádět
směrování	směrování	k1gNnSc4	směrování
(	(	kIx(	(
<g/>
routing	routing	k1gInSc1	routing
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgInSc1d1	moderní
high-speed	highpeed	k1gInSc1	high-speed
routry	routr	k1gInPc1	routr
jsou	být	k5eAaImIp3nP	být
vysoce	vysoce	k6eAd1	vysoce
specializované	specializovaný	k2eAgInPc1d1	specializovaný
počítače	počítač	k1gInPc1	počítač
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
speciálním	speciální	k2eAgInPc3d1	speciální
hardware	hardware	k1gInSc4	hardware
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
hlavní	hlavní	k2eAgFnPc4d1	hlavní
routovací	routovací	k2eAgFnPc4d1	routovací
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
odesílání	odesílání	k1gNnSc1	odesílání
paketů	paket	k1gInPc2	paket
a	a	k8xC	a
specializované	specializovaný	k2eAgFnSc2d1	specializovaná
funkce	funkce	k1gFnSc2	funkce
jako	jako	k8xC	jako
šifrování	šifrování	k1gNnSc2	šifrování
IPsec	IPsec	k1gMnSc1	IPsec
<g/>
.	.	kIx.	.
</s>
<s>
NAT	NAT	kA	NAT
Switch	Switch	k1gMnSc1	Switch
(	(	kIx(	(
<g/>
přepínač	přepínač	k1gInSc1	přepínač
<g/>
)	)	kIx)	)
Hub	hubit	k5eAaImRp2nS	hubit
(	(	kIx(	(
<g/>
rozbočovač	rozbočovač	k1gInSc1	rozbočovač
<g/>
)	)	kIx)	)
Repeater	Repeater	k1gInSc1	Repeater
(	(	kIx(	(
<g/>
opakovač	opakovač	k1gInSc1	opakovač
<g/>
)	)	kIx)	)
</s>
