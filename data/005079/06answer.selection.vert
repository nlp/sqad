<s>
Router	Router	k1gInSc1	Router
používá	používat	k5eAaImIp3nS	používat
routovací	routovací	k2eAgFnSc4d1	routovací
tabulku	tabulka	k1gFnSc4	tabulka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
cesty	cesta	k1gFnPc4	cesta
k	k	k7c3	k
jistým	jistý	k2eAgInPc3d1	jistý
cílům	cíl	k1gInPc3	cíl
a	a	k8xC	a
routovací	routovací	k2eAgFnPc4d1	routovací
metriky	metrika	k1gFnPc4	metrika
spojené	spojený	k2eAgNnSc1d1	spojené
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
cestami	cesta	k1gFnPc7	cesta
<g/>
.	.	kIx.	.
</s>
