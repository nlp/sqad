<s>
Dekáda	dekáda	k1gFnSc1
</s>
<s>
Dekáda	dekáda	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
řeckého	řecký	k2eAgMnSc2d1
dekas	dekas	k1gMnSc1
<g/>
,	,	kIx,
desítka	desítka	k1gFnSc1
<g/>
)	)	kIx)
znamená	znamenat	k5eAaImIp3nS
soubor	soubor	k1gInSc4
o	o	k7c6
deseti	deset	k4xCc6
prvcích	prvek	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
starém	starý	k2eAgNnSc6d1
Řecku	Řecko	k1gNnSc6
označoval	označovat	k5eAaImAgInS
desítku	desítka	k1gFnSc4
bojovníků	bojovník	k1gMnPc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
také	také	k9
desítku	desítka	k1gFnSc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
současném	současný	k2eAgNnSc6d1
užití	užití	k1gNnSc6
obvykle	obvykle	k6eAd1
znamená	znamenat	k5eAaImIp3nS
desetiletí	desetiletí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tedy	tedy	k8xC
desítku	desítka	k1gFnSc4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
pro	pro	k7c4
letopočet	letopočet	k1gInSc4
(	(	kIx(
<g/>
historiograficky	historiograficky	k6eAd1
řadová	řadový	k2eAgFnSc1d1
číslovka	číslovka	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
pro	pro	k7c4
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
neexistuje	existovat	k5eNaImIp3nS
rok	rok	k1gInSc4
nula	nula	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
první	první	k4xOgNnPc1
desetiletí	desetiletí	k1gNnPc2
letopočtu	letopočet	k1gInSc2
tak	tak	k8xS,k8xC
nemůže	moct	k5eNaImIp3nS
začínat	začínat	k5eAaImF
rokem	rok	k1gInSc7
končícím	končící	k2eAgInSc7d1
nulou	nula	k1gFnSc7
<g/>
)	)	kIx)
desetiletí	desetiletí	k1gNnSc1
začínající	začínající	k2eAgNnSc1d1
rokem	rok	k1gInSc7
končícím	končící	k2eAgInSc7d1
pořadovou	pořadový	k2eAgFnSc7d1
jedničkou	jednička	k1gFnSc7
(	(	kIx(
<g/>
například	například	k6eAd1
1961	#num#	k4
<g/>
–	–	k?
<g/>
1970	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
jsou	být	k5eAaImIp3nP
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
pořadové	pořadový	k2eAgInPc4d1
roky	rok	k1gInPc4
1961	#num#	k4
<g/>
.	.	kIx.
až	až	k9
1970	#num#	k4
<g/>
.	.	kIx.
od	od	k7c2
počátku	počátek	k1gInSc2
letopočtu	letopočet	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
konzistentně	konzistentně	k6eAd1
tak	tak	k9
jako	jako	k9
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
století	století	k1gNnSc2
či	či	k8xC
tisíciletí	tisíciletí	k1gNnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
případně	případně	k6eAd1
nekonzistentně	konzistentně	k6eNd1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
s	s	k7c7
počítáním	počítání	k1gNnSc7
století	století	k1gNnSc2
a	a	k8xC
tisíciletí	tisíciletí	k1gNnSc2
od	od	k7c2
roku	rok	k1gInSc2
s	s	k7c7
nulou	nula	k1gFnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
na	na	k7c6
konci	konec	k1gInSc6
(	(	kIx(
<g/>
například	například	k6eAd1
1960	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
<g/>
,	,	kIx,
"	"	kIx"
<g/>
šedesátá	šedesátý	k4xOgNnPc4
léta	léto	k1gNnPc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
196	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
podle	podle	k7c2
normy	norma	k1gFnSc2
ISO	ISO	kA
8601	#num#	k4
platné	platný	k2eAgInPc1d1
od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
jakékoli	jakýkoli	k3yIgNnSc4
jiné	jiný	k2eAgNnSc4d1
desetileté	desetiletý	k2eAgNnSc4d1
období	období	k1gNnSc4
<g/>
,	,	kIx,
například	například	k6eAd1
"	"	kIx"
<g/>
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
dekádě	dekáda	k1gFnSc6
Mozartova	Mozartův	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Různé	různý	k2eAgInPc1d1
způsoby	způsob	k1gInPc1
vyjádření	vyjádření	k1gNnSc2
desetiletí	desetiletí	k1gNnSc2
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
19	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
2000	#num#	k4
</s>
<s>
2001	#num#	k4
</s>
<s>
2002	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
2009	#num#	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
2011	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
2019	#num#	k4
</s>
<s>
2020	#num#	k4
</s>
<s>
2021	#num#	k4
</s>
<s>
2022	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
2029	#num#	k4
</s>
<s>
2030	#num#	k4
</s>
<s>
dekáda	dekáda	k1gFnSc1
podle	podle	k7c2
cifry	cifra	k1gFnSc2
čísla	číslo	k1gNnSc2
</s>
<s>
nultá	nultý	k4xOgNnPc4
léta	léto	k1gNnPc4
</s>
<s>
desátá	desátá	k1gFnSc1
léta	léto	k1gNnSc2
</s>
<s>
...	...	k?
</s>
<s>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
</s>
<s>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
</s>
<s>
2020-2021	2020-2021	k4
</s>
<s>
...	...	k?
</s>
<s>
desetiletí	desetiletí	k1gNnSc4
konzistentní	konzistentní	k2eAgFnSc2d1
se	se	k3xPyFc4
stoletím	století	k1gNnSc7
</s>
<s>
1	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
2001	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
</s>
<s>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
</s>
<s>
2021	#num#	k4
<g/>
–	–	k?
<g/>
2030	#num#	k4
</s>
<s>
V	v	k7c6
epidemiologii	epidemiologie	k1gFnSc6
se	se	k3xPyFc4
k	k	k7c3
popisu	popis	k1gInSc3
pravděpodobnosti	pravděpodobnost	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
v	v	k7c6
životě	život	k1gInSc6
člověka	člověk	k1gMnSc2
objeví	objevit	k5eAaPmIp3nS
určitá	určitý	k2eAgFnSc1d1
choroba	choroba	k1gFnSc1
<g/>
,	,	kIx,
také	také	k9
užívají	užívat	k5eAaImIp3nP
dekády	dekáda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
ovšem	ovšem	k9
první	první	k4xOgFnSc1
dekáda	dekáda	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
narozením	narození	k1gNnSc7
(	(	kIx(
<g/>
rokem	rok	k1gInSc7
0	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
končí	končit	k5eAaImIp3nS
desátými	desátý	k4xOgFnPc7
narozeninami	narozeniny	k1gFnPc7
<g/>
,	,	kIx,
takže	takže	k8xS
"	"	kIx"
<g/>
4	#num#	k4
<g/>
.	.	kIx.
dekáda	dekáda	k1gFnSc1
<g/>
"	"	kIx"
znamená	znamenat	k5eAaImIp3nS
stáří	stáří	k1gNnSc4
přibližně	přibližně	k6eAd1
30	#num#	k4
až	až	k9
40	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
nikoli	nikoli	k9
40	#num#	k4
<g/>
–	–	k?
<g/>
49	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInPc4d1
významy	význam	k1gInPc4
</s>
<s>
Desetidenní	desetidenní	k2eAgInSc4d1
týden	týden	k1gInSc4
<g/>
,	,	kIx,
zavedený	zavedený	k2eAgInSc4d1
v	v	k7c6
letech	léto	k1gNnPc6
1793	#num#	k4
až	až	k9
1802	#num#	k4
ve	v	k7c6
Francii	Francie	k1gFnSc6
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc1
revolučního	revoluční	k2eAgInSc2d1
kalendáře	kalendář	k1gInSc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
opět	opět	k6eAd1
ve	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
v	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Odporová	odporový	k2eAgFnSc1d1
dekáda	dekáda	k1gFnSc1
<g/>
,	,	kIx,
sada	sada	k1gFnSc1
rezistorů	rezistor	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnPc1
hodnoty	hodnota	k1gFnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
poměru	poměr	k1gInSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
100	#num#	k4
atd.	atd.	kA
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Dekáda	dekáda	k1gFnSc1
romské	romský	k2eAgFnSc2d1
integrace	integrace	k1gFnSc2
</s>
<s>
Dekadická	dekadický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Dekadický	dekadický	k2eAgInSc1d1
logaritmus	logaritmus	k1gInSc1
</s>
<s>
Století	století	k1gNnSc1
</s>
<s>
Tisíciletí	tisíciletí	k1gNnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Dekáda	dekáda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
https://zoommagazin.iprima.cz/zajimavosti/desetileti-staleti-pocitani-datum	https://zoommagazin.iprima.cz/zajimavosti/desetileti-staleti-pocitani-datum	k1gNnSc4
-	-	kIx~
Jak	jak	k8xC,k8xS
se	se	k3xPyFc4
správně	správně	k6eAd1
určují	určovat	k5eAaImIp3nP
desetiletí	desetiletí	k1gNnPc4
a	a	k8xC
staletí	staletí	k1gNnPc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
zapeklitější	zapeklitý	k2eAgNnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
si	se	k3xPyFc3
myslíte	myslet	k5eAaImIp2nP
<g/>
,	,	kIx,
"	"	kIx"
<g/>
90	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
totiž	totiž	k9
končí	končit	k5eAaImIp3nS
31	#num#	k4
<g/>
.	.	kIx.
prosincem	prosinec	k1gInSc7
1999	#num#	k4
<g/>
,	,	kIx,
jenže	jenže	k8xC
za	za	k7c4
konec	konec	k1gInSc4
samotného	samotný	k2eAgNnSc2d1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
považujeme	považovat	k5eAaImIp1nP
až	až	k9
31	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
roku	rok	k1gInSc2
2000	#num#	k4
<g/>
"	"	kIx"
<g/>
↑	↑	k?
https://www.loc.gov/standards/datetime/iso-tc154-wg5_n0039_iso_wd_8601-2_2016-02-16.pdf	https://www.loc.gov/standards/datetime/iso-tc154-wg5_n0039_iso_wd_8601-2_2016-02-16.pdf	k1gInSc1
-	-	kIx~
Data	datum	k1gNnSc2
elements	elements	k6eAd1
and	and	k?
interchange	interchange	k1gInSc1
formats	formats	k1gInSc1
—	—	k?
Information	Information	k1gInSc1
interchange	interchange	k1gFnSc1
-	-	kIx~
Representation	Representation	k1gInSc1
of	of	k?
dates	dates	k1gInSc1
and	and	k?
times	times	k1gInSc1
—	—	k?
Part	part	k1gInSc1
2	#num#	k4
<g/>
:	:	kIx,
Extensions	Extensions	k1gInSc1
<g/>
↑	↑	k?
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Dekáda	dekáda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Dekáda	dekáda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
7	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
175	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
dekáda	dekáda	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
dekáda	dekáda	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
