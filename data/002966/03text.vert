<s>
Binturong	Binturong	k1gInSc1	Binturong
(	(	kIx(	(
<g/>
Arctictis	Arctictis	k1gInSc1	Arctictis
binturong	binturong	k1gInSc1	binturong
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
stromová	stromový	k2eAgFnSc1d1	stromová
cibetka	cibetka	k1gFnSc1	cibetka
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
chápavým	chápavý	k2eAgInSc7d1	chápavý
ocasem	ocas	k1gInSc7	ocas
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
stromů	strom	k1gInPc2	strom
deštných	deštný	k2eAgInPc2d1	deštný
pralesů	prales	k1gInPc2	prales
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
binturong	binturonga	k1gFnPc2	binturonga
je	být	k5eAaImIp3nS	být
malajského	malajský	k2eAgInSc2d1	malajský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
"	"	kIx"	"
<g/>
bearcat	bearcat	k5eAaPmF	bearcat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
medvědokočka	medvědokočka	k1gFnSc1	medvědokočka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Binturong	Binturong	k1gInSc1	Binturong
totiž	totiž	k9	totiž
skutečně	skutečně	k6eAd1	skutečně
připomíná	připomínat	k5eAaImIp3nS	připomínat
křížence	kříženec	k1gMnSc4	kříženec
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgNnPc7	tento
dvěma	dva	k4xCgNnPc7	dva
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
.	.	kIx.	.
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
kg	kg	kA	kg
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
61	[number]	k4	61
<g/>
-	-	kIx~	-
<g/>
96	[number]	k4	96
cm	cm	kA	cm
délka	délka	k1gFnSc1	délka
ocasu	ocas	k1gInSc2	ocas
<g/>
:	:	kIx,	:
61	[number]	k4	61
<g/>
-	-	kIx~	-
<g/>
96	[number]	k4	96
cm	cm	kA	cm
Binturong	Binturonga	k1gFnPc2	Binturonga
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velká	velký	k2eAgFnSc1d1	velká
šelma	šelma	k1gFnSc1	šelma
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
<g/>
,	,	kIx,	,
huňatým	huňatý	k2eAgInSc7d1	huňatý
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
chápavým	chápavý	k2eAgInSc7d1	chápavý
ocasem	ocas	k1gInSc7	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Chápavý	chápavý	k2eAgInSc1d1	chápavý
ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
šelem	šelma	k1gFnPc2	šelma
vzácný	vzácný	k2eAgInSc4d1	vzácný
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgInSc4d1	jediný
další	další	k2eAgInSc4d1	další
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
jím	on	k3xPp3gInSc7	on
může	moct	k5eAaImIp3nS	moct
pyšnit	pyšnit	k5eAaImF	pyšnit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jihoamerický	jihoamerický	k2eAgMnSc1d1	jihoamerický
kynkažu	kynkazat	k5eAaPmIp1nS	kynkazat
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
medvídkovití	medvídkovitý	k2eAgMnPc1d1	medvídkovitý
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
také	také	k9	také
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
o	o	k7c4	o
20	[number]	k4	20
<g/>
%	%	kIx~	%
těžší	těžký	k2eAgMnSc1d2	těžší
a	a	k8xC	a
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
samci	samec	k1gMnSc3	samec
<g/>
,	,	kIx,	,
u	u	k7c2	u
bornejského	bornejský	k2eAgInSc2d1	bornejský
poddruhu	poddruh	k1gInSc2	poddruh
A.	A.	kA	A.
b.	b.	k?	b.
penicillatus	penicillatus	k1gInSc1	penicillatus
mohou	moct	k5eAaImIp3nP	moct
vážit	vážit	k5eAaImF	vážit
až	až	k9	až
40	[number]	k4	40
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Binturong	Binturong	k1gMnSc1	Binturong
je	být	k5eAaImIp3nS	být
ploskochodec	ploskochodec	k1gMnSc1	ploskochodec
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
medvěd	medvěd	k1gMnSc1	medvěd
našlapuje	našlapovat	k5eAaImIp3nS	našlapovat
celou	celý	k2eAgFnSc7d1	celá
plochou	plocha	k1gFnSc7	plocha
chodidla	chodidlo	k1gNnSc2	chodidlo
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
nápadně	nápadně	k6eAd1	nápadně
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
hrubá	hrubý	k2eAgFnSc1d1	hrubá
a	a	k8xC	a
obyčejně	obyčejně	k6eAd1	obyčejně
tmavě	tmavě	k6eAd1	tmavě
hnědá	hnědat	k5eAaImIp3nS	hnědat
až	až	k6eAd1	až
černá	černý	k2eAgFnSc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Uši	ucho	k1gNnPc1	ucho
jsou	být	k5eAaImIp3nP	být
krátké	krátká	k1gFnPc1	krátká
a	a	k8xC	a
zakončené	zakončený	k2eAgNnSc1d1	zakončené
delší	dlouhý	k2eAgFnSc7d2	delší
štětičkou	štětička	k1gFnSc7	štětička
chlupů	chlup	k1gInPc2	chlup
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
červenohnědé	červenohnědý	k2eAgFnPc1d1	červenohnědá
barvy	barva	k1gFnPc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Binturongové	Binturong	k1gMnPc1	Binturong
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
poddruzích	poddruh	k1gInPc6	poddruh
rozšířeni	rozšířen	k2eAgMnPc1d1	rozšířen
na	na	k7c6	na
území	území	k1gNnSc6	území
zahrnujícím	zahrnující	k2eAgNnSc6d1	zahrnující
severovýchod	severovýchod	k1gInSc1	severovýchod
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Thajsko	Thajsko	k1gNnSc1	Thajsko
<g/>
,	,	kIx,	,
Barmu	Barma	k1gFnSc4	Barma
<g/>
,	,	kIx,	,
Malajsii	Malajsie	k1gFnSc4	Malajsie
<g/>
,	,	kIx,	,
indonéské	indonéský	k2eAgInPc4d1	indonéský
ostrovy	ostrov	k1gInPc4	ostrov
Sumatra	Sumatra	k1gFnSc1	Sumatra
<g/>
,	,	kIx,	,
Jáva	Jáva	k1gFnSc1	Jáva
<g/>
,	,	kIx,	,
Borneo	Borneo	k1gNnSc1	Borneo
<g/>
,	,	kIx,	,
Bangka	Bangka	k1gFnSc1	Bangka
a	a	k8xC	a
filipínský	filipínský	k2eAgInSc1d1	filipínský
ostrov	ostrov	k1gInSc1	ostrov
Palawan	Palawana	k1gFnPc2	Palawana
<g/>
.	.	kIx.	.
</s>
<s>
Obývají	obývat	k5eAaImIp3nP	obývat
horní	horní	k2eAgNnSc4d1	horní
stromové	stromový	k2eAgNnSc4d1	stromové
patro	patro	k1gNnSc4	patro
neprostupných	prostupný	k2eNgInPc2d1	neprostupný
deštných	deštný	k2eAgInPc2d1	deštný
lesů	les	k1gInPc2	les
do	do	k7c2	do
1400	[number]	k4	1400
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Binturong	Binturong	k1gInSc1	Binturong
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
při	při	k7c6	při
šplhání	šplhání	k1gNnSc6	šplhání
si	se	k3xPyFc3	se
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
ocasem	ocas	k1gInSc7	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
problém	problém	k1gInSc4	problém
viset	viset	k5eAaImF	viset
z	z	k7c2	z
větví	větev	k1gFnPc2	větev
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
lenochod	lenochod	k1gMnSc1	lenochod
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
šplhat	šplhat	k5eAaImF	šplhat
po	po	k7c6	po
kmeni	kmen	k1gInSc6	kmen
hlavou	hlava	k1gFnSc7	hlava
dolů	dol	k1gInPc2	dol
<g/>
;	;	kIx,	;
dokáže	dokázat	k5eAaPmIp3nS	dokázat
také	také	k9	také
výborně	výborně	k6eAd1	výborně
plavat	plavat	k5eAaImF	plavat
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
jsou	být	k5eAaImIp3nP	být
aktivní	aktivní	k2eAgMnPc1d1	aktivní
ve	v	k7c6	v
dne	den	k1gInSc2	den
i	i	k8xC	i
během	během	k7c2	během
noci	noc	k1gFnSc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
žije	žít	k5eAaImIp3nS	žít
samotářsky	samotářsky	k6eAd1	samotářsky
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
i	i	k9	i
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
samici	samice	k1gFnSc4	samice
s	s	k7c7	s
mláďaty	mládě	k1gNnPc7	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
samicí	samice	k1gFnSc7	samice
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
samec	samec	k1gMnSc1	samec
i	i	k9	i
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
mláďat	mládě	k1gNnPc2	mládě
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
ho	on	k3xPp3gMnSc4	on
nijak	nijak	k6eAd1	nijak
neodhání	odhánět	k5eNaImIp3nS	odhánět
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc1	pohlaví
mají	mít	k5eAaImIp3nP	mít
výrazné	výrazný	k2eAgFnPc4d1	výrazná
pachové	pachový	k2eAgFnPc4d1	pachová
žlázy	žláza	k1gFnPc4	žláza
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
využívají	využívat	k5eAaImIp3nP	využívat
k	k	k7c3	k
signalizování	signalizování	k1gNnSc3	signalizování
své	svůj	k3xOyFgFnSc2	svůj
přítomnosti	přítomnost	k1gFnSc2	přítomnost
jiným	jiný	k2eAgInPc3d1	jiný
binturongům	binturong	k1gInPc3	binturong
<g/>
.	.	kIx.	.
</s>
<s>
Páří	pářit	k5eAaImIp3nS	pářit
se	se	k3xPyFc4	se
celoročně	celoročně	k6eAd1	celoročně
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
však	však	k9	však
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
února	únor	k1gInSc2	únor
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
pak	pak	k6eAd1	pak
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
trvá	trvat	k5eAaImIp3nS	trvat
90	[number]	k4	90
<g/>
-	-	kIx~	-
<g/>
92	[number]	k4	92
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
samice	samice	k1gFnPc1	samice
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
stromu	strom	k1gInSc2	strom
porodí	porodit	k5eAaPmIp3nS	porodit
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Binturong	Binturong	k1gInSc1	Binturong
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
plodožravý	plodožravý	k2eAgInSc1d1	plodožravý
a	a	k8xC	a
významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c4	na
šíření	šíření	k1gNnSc4	šíření
semen	semeno	k1gNnPc2	semeno
<g/>
,	,	kIx,	,
pojídá	pojídat	k5eAaImIp3nS	pojídat
také	také	k9	také
mršiny	mršina	k1gFnPc4	mršina
<g/>
,	,	kIx,	,
loví	lovit	k5eAaImIp3nP	lovit
hlodavce	hlodavec	k1gMnPc4	hlodavec
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc4	ryba
nebo	nebo	k8xC	nebo
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
vybírá	vybírat	k5eAaImIp3nS	vybírat
ptačí	ptačí	k2eAgNnPc4d1	ptačí
hnízda	hnízdo	k1gNnPc4	hnízdo
a	a	k8xC	a
nepohrdne	pohrdnout	k5eNaPmIp3nS	pohrdnout
ani	ani	k8xC	ani
malými	malý	k2eAgMnPc7d1	malý
bezobratlými	bezobratlí	k1gMnPc7	bezobratlí
<g/>
,	,	kIx,	,
listy	list	k1gInPc7	list
nebo	nebo	k8xC	nebo
výhonky	výhonek	k1gInPc7	výhonek
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnPc1d1	místní
domorodci	domorodec	k1gMnPc1	domorodec
chovají	chovat	k5eAaImIp3nP	chovat
binturongy	binturong	k1gInPc4	binturong
jako	jako	k8xS	jako
domácí	domácí	k2eAgNnPc4d1	domácí
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
maso	maso	k1gNnSc1	maso
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
lahůdku	lahůdka	k1gFnSc4	lahůdka
a	a	k8xC	a
využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
orientální	orientální	k2eAgFnSc6d1	orientální
medicíně	medicína	k1gFnSc6	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Binturonga	Binturonga	k1gFnSc1	Binturonga
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
pavilonu	pavilon	k1gInSc6	pavilon
Indonéská	indonéský	k2eAgFnSc1d1	Indonéská
džungle	džungle	k1gFnSc1	džungle
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
zoologické	zoologický	k2eAgFnSc6d1	zoologická
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
také	také	k9	také
v	v	k7c6	v
ostravské	ostravský	k2eAgFnSc6d1	Ostravská
zoologické	zoologický	k2eAgFnSc6d1	zoologická
zahradě	zahrada	k1gFnSc6	zahrada
a	a	k8xC	a
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
na	na	k7c6	na
Svatém	svatý	k2eAgInSc6d1	svatý
Kopečku	kopeček	k1gInSc6	kopeček
u	u	k7c2	u
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Binturong	Binturonga	k1gFnPc2	Binturonga
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Taxon	taxon	k1gInSc4	taxon
Arctictis	Arctictis	k1gFnSc4	Arctictis
binturong	binturong	k1gInSc4	binturong
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
ZOO	zoo	k1gFnPc2	zoo
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Lexikon	lexikon	k1gNnSc1	lexikon
zvířat	zvíře	k1gNnPc2	zvíře
-	-	kIx~	-
Binturong	Binturong	k1gInSc1	Binturong
McPhee	McPhe	k1gFnSc2	McPhe
<g/>
,	,	kIx,	,
M.	M.	kA	M.
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Arctictis	Arctictis	k1gFnSc1	Arctictis
binturong	binturong	k1gMnSc1	binturong
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Animal	animal	k1gMnSc1	animal
Diversity	Diversit	k1gInPc4	Diversit
Web	web	k1gInSc1	web
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Lioncrusher	Lioncrushra	k1gFnPc2	Lioncrushra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Domain	Domaina	k1gFnPc2	Domaina
<g/>
:	:	kIx,	:
Binturong	Binturong	k1gInSc1	Binturong
(	(	kIx(	(
<g/>
Arctictis	Arctictis	k1gInSc1	Arctictis
binturong	binturong	k1gInSc1	binturong
<g/>
)	)	kIx)	)
facts	facts	k1gInSc1	facts
and	and	k?	and
pictures	pictures	k1gInSc1	pictures
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ostravská	ostravský	k2eAgFnSc1d1	Ostravská
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
</s>
