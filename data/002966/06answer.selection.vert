<s>
Binturong	Binturong	k1gInSc1	Binturong
(	(	kIx(	(
<g/>
Arctictis	Arctictis	k1gInSc1	Arctictis
binturong	binturong	k1gInSc1	binturong
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
stromová	stromový	k2eAgFnSc1d1	stromová
cibetka	cibetka	k1gFnSc1	cibetka
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
chápavým	chápavý	k2eAgInSc7d1	chápavý
ocasem	ocas	k1gInSc7	ocas
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
stromů	strom	k1gInPc2	strom
deštných	deštný	k2eAgInPc2d1	deštný
pralesů	prales	k1gInPc2	prales
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
