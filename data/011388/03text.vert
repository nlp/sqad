<p>
<s>
Pleskanka	Pleskanka	k1gFnSc1	Pleskanka
mexická	mexický	k2eAgFnSc1d1	mexická
(	(	kIx(	(
<g/>
Argemone	Argemon	k1gInSc5	Argemon
mexicana	mexicana	k1gFnSc1	mexicana
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
ostnatá	ostnatý	k2eAgFnSc1d1	ostnatá
<g/>
,	,	kIx,	,
jednoletá	jednoletý	k2eAgFnSc1d1	jednoletá
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
druh	druh	k1gInSc1	druh
nevelkého	velký	k2eNgInSc2d1	nevelký
rodu	rod	k1gInSc2	rod
pleskanka	pleskanka	k1gFnSc1	pleskanka
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
makovitých	makovití	k1gMnPc2	makovití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
bylina	bylina	k1gFnSc1	bylina
je	být	k5eAaImIp3nS	být
původem	původ	k1gInSc7	původ
středo	středa	k1gFnSc5	středa
a	a	k8xC	a
jihoamerická	jihoamerický	k2eAgFnSc1d1	jihoamerická
<g/>
;	;	kIx,	;
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
ostrovů	ostrov	k1gInPc2	ostrov
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
,	,	kIx,	,
ze	z	k7c2	z
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
severovýchodu	severovýchod	k1gInSc2	severovýchod
a	a	k8xC	a
severozápadu	severozápad	k1gInSc2	severozápad
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
květy	květ	k1gInPc4	květ
a	a	k8xC	a
nenáročnost	nenáročnost	k1gFnSc1	nenáročnost
byla	být	k5eAaImAgFnS	být
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
téměř	téměř	k6eAd1	téměř
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
tropických	tropický	k2eAgFnPc2d1	tropická
a	a	k8xC	a
teplých	teplý	k2eAgFnPc2d1	teplá
oblastí	oblast	k1gFnPc2	oblast
mírného	mírný	k2eAgNnSc2d1	mírné
pásma	pásmo	k1gNnSc2	pásmo
<g/>
,	,	kIx,	,
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
nebo	nebo	k8xC	nebo
sama	sám	k3xTgFnSc1	sám
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
obydlených	obydlený	k2eAgInPc6d1	obydlený
kontinentech	kontinent	k1gInPc6	kontinent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
také	také	k9	také
ze	z	k7c2	z
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
zavlečena	zavleknout	k5eAaPmNgFnS	zavleknout
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c2	za
příležitostně	příležitostně	k6eAd1	příležitostně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgInSc1d1	vyskytující
neofyt	neofyt	k1gInSc1	neofyt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
pozorována	pozorován	k2eAgFnSc1d1	pozorována
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
rostlinou	rostlina	k1gFnSc7	rostlina
na	na	k7c4	na
půdu	půda	k1gFnSc4	půda
i	i	k8xC	i
klimatické	klimatický	k2eAgNnSc1d1	klimatické
prostředí	prostředí	k1gNnSc1	prostředí
nenáročnou	náročný	k2eNgFnSc7d1	nenáročná
<g/>
,	,	kIx,	,
samovolně	samovolně	k6eAd1	samovolně
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
škále	škála	k1gFnSc6	škála
stanovišť	stanoviště	k1gNnPc2	stanoviště
<g/>
,	,	kIx,	,
od	od	k7c2	od
vysýchavých	vysýchavý	k2eAgInPc2d1	vysýchavý
až	až	k8xS	až
suchých	suchý	k2eAgInPc2d1	suchý
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
polosuché	polosuchý	k2eAgNnSc4d1	polosuché
až	až	k9	až
po	po	k7c6	po
příležitostně	příležitostně	k6eAd1	příležitostně
zaplavované	zaplavovaný	k2eAgFnSc6d1	zaplavovaná
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
od	od	k7c2	od
hladiny	hladina	k1gFnSc2	hladina
moře	moře	k1gNnSc2	moře
až	až	k9	až
po	po	k7c4	po
3000	[number]	k4	3000
m.	m.	k?	m.
Nezřídka	nezřídka	k6eAd1	nezřídka
obsazuje	obsazovat	k5eAaImIp3nS	obsazovat
prostory	prostor	k1gInPc1	prostor
dotčené	dotčený	k2eAgInPc1d1	dotčený
lidskou	lidský	k2eAgFnSc7d1	lidská
činností	činnost	k1gFnSc7	činnost
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
spatřit	spatřit	k5eAaPmF	spatřit
na	na	k7c6	na
skládkách	skládka	k1gFnPc6	skládka
<g/>
,	,	kIx,	,
rumištích	rumiště	k1gNnPc6	rumiště
<g/>
,	,	kIx,	,
polních	polní	k2eAgInPc6d1	polní
úhorech	úhor	k1gInPc6	úhor
<g/>
,	,	kIx,	,
zanedbaných	zanedbaný	k2eAgNnPc6d1	zanedbané
polích	pole	k1gNnPc6	pole
a	a	k8xC	a
vinicích	vinice	k1gFnPc6	vinice
<g/>
,	,	kIx,	,
pastvinách	pastvina	k1gFnPc6	pastvina
<g/>
,	,	kIx,	,
afrických	africký	k2eAgFnPc6d1	africká
savanách	savana	k1gFnPc6	savana
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
cest	cesta	k1gFnPc2	cesta
i	i	k9	i
na	na	k7c6	na
obdělávaných	obdělávaný	k2eAgNnPc6d1	obdělávané
polích	pole	k1gNnPc6	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Jednoletá	jednoletý	k2eAgFnSc1d1	jednoletá
<g/>
,	,	kIx,	,
dlouze	dlouho	k6eAd1	dlouho
kvetoucí	kvetoucí	k2eAgFnSc1d1	kvetoucí
bylina	bylina	k1gFnSc1	bylina
s	s	k7c7	s
přímou	přímý	k2eAgFnSc7d1	přímá
<g/>
,	,	kIx,	,
jednoduchou	jednoduchý	k2eAgFnSc7d1	jednoduchá
nebo	nebo	k8xC	nebo
jen	jen	k6eAd1	jen
chudě	chudě	k6eAd1	chudě
rozvětvenou	rozvětvený	k2eAgFnSc7d1	rozvětvená
lodyhou	lodyha	k1gFnSc7	lodyha
<g/>
,	,	kIx,	,
30	[number]	k4	30
až	až	k9	až
80	[number]	k4	80
cm	cm	kA	cm
vysokou	vysoká	k1gFnSc4	vysoká
se	s	k7c7	s
žlutohnědými	žlutohnědý	k2eAgInPc7d1	žlutohnědý
ostny	osten	k1gInPc7	osten
<g/>
.	.	kIx.	.
</s>
<s>
Listy	lista	k1gFnPc4	lista
má	mít	k5eAaImIp3nS	mít
střídavé	střídavý	k2eAgNnSc1d1	střídavé
<g/>
,	,	kIx,	,
bazální	bazální	k2eAgInPc1d1	bazální
krátce	krátce	k6eAd1	krátce
řapíkaté	řapíkatý	k2eAgInPc1d1	řapíkatý
a	a	k8xC	a
lodyžní	lodyžní	k2eAgInPc1d1	lodyžní
přisedlé	přisedlý	k2eAgInPc1d1	přisedlý
a	a	k8xC	a
objímavé	objímavý	k2eAgInPc1d1	objímavý
<g/>
,	,	kIx,	,
5	[number]	k4	5
až	až	k9	až
20	[number]	k4	20
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
a	a	k8xC	a
3	[number]	k4	3
až	až	k9	až
8	[number]	k4	8
cm	cm	kA	cm
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
čepele	čepel	k1gInPc1	čepel
jsou	být	k5eAaImIp3nP	být
obkopinaté	obkopinatý	k2eAgInPc1d1	obkopinatý
až	až	k9	až
eliptické	eliptický	k2eAgInPc1d1	eliptický
<g/>
,	,	kIx,	,
šedozelené	šedozelený	k2eAgInPc1d1	šedozelený
se	s	k7c7	s
světlými	světlý	k2eAgFnPc7d1	světlá
žilkami	žilka	k1gFnPc7	žilka
a	a	k8xC	a
po	po	k7c6	po
obvodě	obvod	k1gInSc6	obvod
jsou	být	k5eAaImIp3nP	být
zpeřeně	zpeřeně	k6eAd1	zpeřeně
dělené	dělený	k2eAgInPc1d1	dělený
do	do	k7c2	do
vlnitých	vlnitý	k2eAgInPc2d1	vlnitý
<g/>
,	,	kIx,	,
hrubě	hrubě	k6eAd1	hrubě
zubatých	zubatý	k2eAgInPc2d1	zubatý
<g/>
,	,	kIx,	,
ostnitých	ostnitý	k2eAgInPc2d1	ostnitý
laloků	lalok	k1gInPc2	lalok
s	s	k7c7	s
ostrými	ostrý	k2eAgInPc7d1	ostrý
vrcholy	vrchol	k1gInPc7	vrchol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Květy	Květa	k1gFnPc1	Květa
4	[number]	k4	4
až	až	k9	až
8	[number]	k4	8
cm	cm	kA	cm
velké	velká	k1gFnPc1	velká
vykvétají	vykvétat	k5eAaImIp3nP	vykvétat
jednotlivě	jednotlivě	k6eAd1	jednotlivě
z	z	k7c2	z
hranatých	hranatý	k2eAgNnPc2d1	hranaté
poupat	poupě	k1gNnPc2	poupě
<g/>
.	.	kIx.	.
</s>
<s>
Oboupohlavné	oboupohlavný	k2eAgNnSc1d1	oboupohlavné
<g/>
,	,	kIx,	,
miskovitě	miskovitě	k6eAd1	miskovitě
rozprostřené	rozprostřený	k2eAgInPc4d1	rozprostřený
květy	květ	k1gInPc4	květ
s	s	k7c7	s
listeny	listen	k1gInPc7	listen
a	a	k8xC	a
krátkými	krátký	k2eAgFnPc7d1	krátká
stopkami	stopka	k1gFnPc7	stopka
mají	mít	k5eAaImIp3nP	mít
trojčetný	trojčetný	k2eAgInSc4d1	trojčetný
kalich	kalich	k1gInSc4	kalich
s	s	k7c7	s
člunkovitými	člunkovitý	k2eAgInPc7d1	člunkovitý
<g/>
,	,	kIx,	,
1	[number]	k4	1
cm	cm	kA	cm
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
plátky	plátek	k1gInPc7	plátek
a	a	k8xC	a
šest	šest	k4xCc4	šest
široce	široko	k6eAd1	široko
vejčitých	vejčitý	k2eAgFnPc2d1	vejčitá
<g/>
,	,	kIx,	,
nahoře	nahoře	k6eAd1	nahoře
zaoblených	zaoblený	k2eAgFnPc2d1	zaoblená
<g/>
,	,	kIx,	,
překrývajících	překrývající	k2eAgFnPc2d1	překrývající
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
sytě	sytě	k6eAd1	sytě
žlutých	žlutý	k2eAgInPc2d1	žlutý
korunních	korunní	k2eAgInPc2d1	korunní
plátků	plátek	k1gInPc2	plátek
velkých	velký	k2eAgInPc2d1	velký
až	až	k6eAd1	až
3	[number]	k4	3
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květu	květ	k1gInSc6	květ
je	být	k5eAaImIp3nS	být
30	[number]	k4	30
až	až	k9	až
50	[number]	k4	50
tyčinek	tyčinka	k1gFnPc2	tyčinka
se	s	k7c7	s
žlutými	žlutý	k2eAgFnPc7d1	žlutá
nitkami	nitka	k1gFnPc7	nitka
a	a	k8xC	a
úzkými	úzký	k2eAgInPc7d1	úzký
prašníky	prašník	k1gInPc7	prašník
<g/>
.	.	kIx.	.
</s>
<s>
Eliptický	eliptický	k2eAgInSc1d1	eliptický
semeník	semeník	k1gInSc1	semeník
velký	velký	k2eAgInSc1d1	velký
až	až	k9	až
10	[number]	k4	10
mm	mm	kA	mm
má	mít	k5eAaImIp3nS	mít
krátkou	krátký	k2eAgFnSc4d1	krátká
čnělku	čnělka	k1gFnSc4	čnělka
s	s	k7c7	s
tmavě	tmavě	k6eAd1	tmavě
červenou	červený	k2eAgFnSc7d1	červená
<g/>
,	,	kIx,	,
vícelaločnou	vícelaločný	k2eAgFnSc7d1	vícelaločný
bliznou	blizna	k1gFnSc7	blizna
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
diploidním	diploidní	k2eAgInSc7d1	diploidní
druhem	druh	k1gInSc7	druh
2	[number]	k4	2
<g/>
n	n	k0	n
=	=	kIx~	=
28	[number]	k4	28
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
eliptická	eliptický	k2eAgFnSc1d1	eliptická
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
obloukovitě	obloukovitě	k6eAd1	obloukovitě
zahnutá	zahnutý	k2eAgFnSc1d1	zahnutá
<g/>
,	,	kIx,	,
ostnatá	ostnatý	k2eAgFnSc1d1	ostnatá
tobolka	tobolka	k1gFnSc1	tobolka
3	[number]	k4	3
až	až	k9	až
5	[number]	k4	5
cm	cm	kA	cm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
se	s	k7c7	s
suchou	suchý	k2eAgFnSc7d1	suchá
čnělkou	čnělka	k1gFnSc7	čnělka
<g/>
.	.	kIx.	.
</s>
<s>
Otvírá	otvírat	k5eAaImIp3nS	otvírat
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
až	až	k9	až
šesti	šest	k4xCc7	šest
chlopněmi	chlopeň	k1gFnPc7	chlopeň
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
2	[number]	k4	2
mm	mm	kA	mm
kulovitá	kulovitý	k2eAgNnPc4d1	kulovité
semena	semeno	k1gNnPc4	semeno
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
olej	olej	k1gInSc4	olej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Pleskanka	Pleskanka	k1gFnSc1	Pleskanka
mexická	mexický	k2eAgFnSc1d1	mexická
se	se	k3xPyFc4	se
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
výhradně	výhradně	k6eAd1	výhradně
semeny	semeno	k1gNnPc7	semeno
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
různou	různý	k2eAgFnSc4d1	různá
délku	délka	k1gFnSc4	délka
dormance	dormanka	k1gFnSc3	dormanka
<g/>
,	,	kIx,	,
od	od	k7c2	od
několika	několik	k4yIc2	několik
týdnů	týden	k1gInPc2	týden
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
v	v	k7c6	v
semenné	semenný	k2eAgFnSc6d1	semenná
bance	banka	k1gFnSc6	banka
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
vydrží	vydržet	k5eAaPmIp3nS	vydržet
i	i	k9	i
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mohutnější	mohutný	k2eAgFnSc1d2	mohutnější
rostlina	rostlina	k1gFnSc1	rostlina
může	moct	k5eAaImIp3nS	moct
vyprodukovat	vyprodukovat	k5eAaPmF	vyprodukovat
60	[number]	k4	60
až	až	k9	až
90	[number]	k4	90
tobolek	tobolka	k1gFnPc2	tobolka
a	a	k8xC	a
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
po	po	k7c6	po
300	[number]	k4	300
až	až	k8xS	až
400	[number]	k4	400
semenech	semeno	k1gNnPc6	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
semen	semeno	k1gNnPc2	semeno
vypadá	vypadat	k5eAaImIp3nS	vypadat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
rostliny	rostlina	k1gFnSc2	rostlina
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
tak	tak	k6eAd1	tak
koberec	koberec	k1gInSc4	koberec
semenáčů	semenáč	k1gInPc2	semenáč
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
rozptylovat	rozptylovat	k5eAaImF	rozptylovat
po	po	k7c6	po
deštích	dešť	k1gInPc6	dešť
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
vodou	voda	k1gFnSc7	voda
nebo	nebo	k8xC	nebo
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
rostoucích	rostoucí	k2eAgFnPc2d1	rostoucí
blízko	blízko	k7c2	blízko
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
vodou	voda	k1gFnSc7	voda
tekoucí	tekoucí	k2eAgInPc1d1	tekoucí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
jsou	být	k5eAaImIp3nP	být
považované	považovaný	k2eAgFnPc1d1	považovaná
za	za	k7c4	za
nepříjemný	příjemný	k2eNgInSc4d1	nepříjemný
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
šířící	šířící	k2eAgInSc1d1	šířící
toxický	toxický	k2eAgInSc1d1	toxický
plevel	plevel	k1gInSc1	plevel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pastvinách	pastvina	k1gFnPc6	pastvina
se	se	k3xPyFc4	se
jim	on	k3xPp3gInPc3	on
pasoucí	pasoucí	k2eAgInPc1d1	pasoucí
se	se	k3xPyFc4	se
zvířata	zvíře	k1gNnPc1	zvíře
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
otrávit	otrávit	k5eAaPmF	otrávit
senu	sena	k1gFnSc4	sena
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kulturních	kulturní	k2eAgFnPc2d1	kulturní
plodin	plodina	k1gFnPc2	plodina
nejvíce	hodně	k6eAd3	hodně
zapleveluje	zaplevelovat	k5eAaImIp3nS	zaplevelovat
porosty	porost	k1gInPc4	porost
obilnin	obilnina	k1gFnPc2	obilnina
<g/>
,	,	kIx,	,
luštěnin	luštěnina	k1gFnPc2	luštěnina
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
,	,	kIx,	,
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
,	,	kIx,	,
tabáku	tabák	k1gInSc2	tabák
<g/>
,	,	kIx,	,
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
i	i	k8xC	i
brambor	brambora	k1gFnPc2	brambora
<g/>
.	.	kIx.	.
</s>
<s>
Ovčí	ovčí	k2eAgFnSc1d1	ovčí
vlna	vlna	k1gFnSc1	vlna
se	s	k7c7	s
zapletenými	zapletený	k2eAgInPc7d1	zapletený
pichlavými	pichlavý	k2eAgInPc7d1	pichlavý
plody	plod	k1gInPc7	plod
je	být	k5eAaImIp3nS	být
hodnocená	hodnocený	k2eAgFnSc1d1	hodnocená
jako	jako	k8xC	jako
méně	málo	k6eAd2	málo
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
<g/>
.	.	kIx.	.
</s>
<s>
Námezdní	námezdní	k2eAgMnPc1d1	námezdní
sběrači	sběrač	k1gMnPc1	sběrač
za	za	k7c4	za
ruční	ruční	k2eAgInSc4d1	ruční
sběr	sběr	k1gInSc4	sběr
nízkorostoucích	nízkorostoucí	k2eAgFnPc2d1	nízkorostoucí
plodin	plodina	k1gFnPc2	plodina
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
porostlých	porostlý	k2eAgFnPc2d1	porostlá
zímto	zímto	k1gNnSc1	zímto
pichlavým	pichlavý	k2eAgInSc7d1	pichlavý
plevelem	plevel	k1gInSc7	plevel
požadují	požadovat	k5eAaImIp3nP	požadovat
příplatky	příplatek	k1gInPc4	příplatek
za	za	k7c4	za
obtížnou	obtížný	k2eAgFnSc4d1	obtížná
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kořeny	kořen	k1gInPc1	kořen
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
alelopatické	alelopatický	k2eAgFnPc4d1	alelopatický
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
brání	bránit	k5eAaImIp3nP	bránit
vyklíčit	vyklíčit	k5eAaPmF	vyklíčit
semenům	semeno	k1gNnPc3	semeno
v	v	k7c6	v
okolní	okolní	k2eAgFnSc6d1	okolní
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
kyselinu	kyselina	k1gFnSc4	kyselina
benzoovou	benzoový	k2eAgFnSc4d1	benzoová
<g/>
,	,	kIx,	,
kyselina	kyselina	k1gFnSc1	kyselina
linolovou	linolový	k2eAgFnSc4d1	linolová
<g/>
,	,	kIx,	,
kyselina	kyselina	k1gFnSc1	kyselina
myristovou	myristův	k2eAgFnSc7d1	myristův
<g/>
,	,	kIx,	,
kyselina	kyselina	k1gFnSc1	kyselina
olejovou	olejový	k2eAgFnSc4d1	olejová
<g/>
,	,	kIx,	,
kyselinu	kyselina	k1gFnSc4	kyselina
skořicovou	skořicová	k1gFnSc4	skořicová
<g/>
,	,	kIx,	,
alkaloidy	alkaloid	k1gInPc1	alkaloid
berberin	berberin	k1gInSc1	berberin
<g/>
,	,	kIx,	,
chelerytherin	chelerytherin	k1gInSc1	chelerytherin
<g/>
,	,	kIx,	,
optisin	optisin	k1gInSc1	optisin
<g/>
,	,	kIx,	,
protopin	protopin	k1gInSc1	protopin
<g/>
,	,	kIx,	,
sarguinarin	sarguinarin	k1gInSc1	sarguinarin
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
semen	semeno	k1gNnPc2	semeno
se	se	k3xPyFc4	se
lisuje	lisovat	k5eAaImIp3nS	lisovat
olej	olej	k1gInSc1	olej
užívaný	užívaný	k2eAgInSc1d1	užívaný
pro	pro	k7c4	pro
svícení	svícení	k1gNnSc4	svícení
<g/>
,	,	kIx,	,
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
mýdla	mýdlo	k1gNnSc2	mýdlo
a	a	k8xC	a
napouštění	napouštění	k1gNnSc2	napouštění
dřeva	dřevo	k1gNnSc2	dřevo
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
proti	proti	k7c3	proti
termitům	termit	k1gMnPc3	termit
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
savce	savec	k1gMnPc4	savec
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
nepoživatelný	poživatelný	k2eNgMnSc1d1	nepoživatelný
a	a	k8xC	a
při	při	k7c6	při
konzumaci	konzumace	k1gFnSc6	konzumace
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
úmrtí	úmrtí	k1gNnSc4	úmrtí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zahradách	zahrada	k1gFnPc6	zahrada
bývá	bývat	k5eAaImIp3nS	bývat
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
květy	květ	k1gInPc4	květ
pleskanka	pleskanka	k1gFnSc1	pleskanka
mexická	mexický	k2eAgFnSc1d1	mexická
občas	občas	k6eAd1	občas
vysazována	vysazován	k2eAgFnSc1d1	vysazována
jako	jako	k8xC	jako
nenáročná	náročný	k2eNgFnSc1d1	nenáročná
<g/>
,	,	kIx,	,
okrasná	okrasný	k2eAgFnSc1d1	okrasná
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
vysévá	vysévat	k5eAaImIp3nS	vysévat
na	na	k7c4	na
ladem	ladem	k6eAd1	ladem
ležící	ležící	k2eAgFnSc4d1	ležící
<g/>
,	,	kIx,	,
suchou	suchý	k2eAgFnSc4d1	suchá
<g/>
,	,	kIx,	,
alkalickou	alkalický	k2eAgFnSc4d1	alkalická
a	a	k8xC	a
málo	málo	k6eAd1	málo
úrodnou	úrodný	k2eAgFnSc4d1	úrodná
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
zúrodnění	zúrodnění	k1gNnSc4	zúrodnění
<g/>
,	,	kIx,	,
zelený	zelený	k2eAgInSc4d1	zelený
porost	porost	k1gInSc4	porost
obsahující	obsahující	k2eAgInSc4d1	obsahující
mnoho	mnoho	k6eAd1	mnoho
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
fosforu	fosfor	k1gInSc2	fosfor
a	a	k8xC	a
draslíku	draslík	k1gInSc2	draslík
se	se	k3xPyFc4	se
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
kvetením	kvetení	k1gNnSc7	kvetení
pokosí	pokosit	k5eAaPmIp3nS	pokosit
a	a	k8xC	a
zaoře	zaorat	k5eAaPmIp3nS	zaorat
<g/>
.	.	kIx.	.
</s>
<s>
Místně	místně	k6eAd1	místně
se	se	k3xPyFc4	se
roztřepenými	roztřepený	k2eAgFnPc7d1	roztřepená
konci	konec	k1gInSc6	konec
lodyh	lodyha	k1gFnPc2	lodyha
tlumí	tlumit	k5eAaImIp3nS	tlumit
bolesti	bolest	k1gFnPc4	bolest
zubů	zub	k1gInPc2	zub
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
prášek	prášek	k1gInSc1	prášek
ze	z	k7c2	z
semen	semeno	k1gNnPc2	semeno
přidává	přidávat	k5eAaImIp3nS	přidávat
do	do	k7c2	do
piva	pivo	k1gNnSc2	pivo
nebo	nebo	k8xC	nebo
čaje	čaj	k1gInSc2	čaj
pro	pro	k7c4	pro
navození	navození	k1gNnSc4	navození
pocitu	pocit	k1gInSc2	pocit
opojení	opojení	k1gNnSc2	opojení
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
léčebným	léčebný	k2eAgInPc3d1	léčebný
účelům	účel	k1gInPc3	účel
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pleskanka	pleskanka	k1gFnSc1	pleskanka
mexická	mexický	k2eAgFnSc1d1	mexická
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Argemone	Argemon	k1gInSc5	Argemon
mexicana	mexican	k1gMnSc4	mexican
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
