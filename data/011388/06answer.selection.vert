<s>
Jednoletá	jednoletý	k2eAgFnSc1d1	jednoletá
<g/>
,	,	kIx,	,
dlouze	dlouho	k6eAd1	dlouho
kvetoucí	kvetoucí	k2eAgFnSc1d1	kvetoucí
bylina	bylina	k1gFnSc1	bylina
s	s	k7c7	s
přímou	přímý	k2eAgFnSc7d1	přímá
<g/>
,	,	kIx,	,
jednoduchou	jednoduchý	k2eAgFnSc7d1	jednoduchá
nebo	nebo	k8xC	nebo
jen	jen	k6eAd1	jen
chudě	chudě	k6eAd1	chudě
rozvětvenou	rozvětvený	k2eAgFnSc7d1	rozvětvená
lodyhou	lodyha	k1gFnSc7	lodyha
<g/>
,	,	kIx,	,
30	[number]	k4	30
až	až	k9	až
80	[number]	k4	80
cm	cm	kA	cm
vysokou	vysoká	k1gFnSc4	vysoká
se	s	k7c7	s
žlutohnědými	žlutohnědý	k2eAgInPc7d1	žlutohnědý
ostny	osten	k1gInPc7	osten
<g/>
.	.	kIx.	.
</s>
