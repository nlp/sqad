<p>
<s>
Punské	punský	k2eAgFnPc1d1	punská
války	válka	k1gFnPc1	válka
(	(	kIx(	(
<g/>
264	[number]	k4	264
<g/>
–	–	k?	–
<g/>
146	[number]	k4	146
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
tří	tři	k4xCgInPc2	tři
konfliktů	konflikt	k1gInPc2	konflikt
mezi	mezi	k7c7	mezi
Římskou	římský	k2eAgFnSc7d1	římská
Republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
Kartágem	Kartágo	k1gNnSc7	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
pojmenování	pojmenování	k1gNnSc2	pojmenování
Poeni	Poeen	k2eAgMnPc1d1	Poeen
<g/>
,	,	kIx,	,
Puni	Pun	k1gMnPc1	Pun
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Punové	Pun	k1gMnPc1	Pun
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
Římané	Říman	k1gMnPc1	Říman
používali	používat	k5eAaImAgMnP	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
Kartaginců	Kartaginec	k1gMnPc2	Kartaginec
kvůli	kvůli	k7c3	kvůli
jejich	jejich	k3xOp3gInPc3	jejich
fénickým	fénický	k2eAgInPc3d1	fénický
předkům	předek	k1gInPc3	předek
<g/>
.	.	kIx.	.
</s>
<s>
Střetly	střetnout	k5eAaPmAgFnP	střetnout
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zásadně	zásadně	k6eAd1	zásadně
odlišné	odlišný	k2eAgFnSc2d1	odlišná
státní	státní	k2eAgFnSc2d1	státní
a	a	k8xC	a
vojenské	vojenský	k2eAgFnSc2d1	vojenská
koncepce	koncepce	k1gFnSc2	koncepce
<g/>
:	:	kIx,	:
zatímco	zatímco	k8xS	zatímco
Řím	Řím	k1gInSc1	Řím
byl	být	k5eAaImAgInS	být
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
římské	římský	k2eAgNnSc1d1	římské
vojsko	vojsko	k1gNnSc1	vojsko
se	se	k3xPyFc4	se
skládalo	skládat	k5eAaImAgNnS	skládat
z	z	k7c2	z
občanů	občan	k1gMnPc2	občan
placených	placený	k2eAgMnPc2d1	placený
žoldem	žold	k1gInSc7	žold
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kartágu	Kartágo	k1gNnSc6	Kartágo
panovala	panovat	k5eAaImAgFnS	panovat
oligarchie	oligarchie	k1gFnPc4	oligarchie
a	a	k8xC	a
kartaginskou	kartaginský	k2eAgFnSc4d1	kartaginská
armádu	armáda	k1gFnSc4	armáda
tvořili	tvořit	k5eAaImAgMnP	tvořit
převážně	převážně	k6eAd1	převážně
najatí	najatý	k2eAgMnPc1d1	najatý
cizí	cizí	k2eAgMnPc1d1	cizí
žoldnéři	žoldnér	k1gMnPc1	žoldnér
<g/>
.	.	kIx.	.
</s>
<s>
Konečným	konečný	k2eAgInSc7d1	konečný
výsledkem	výsledek	k1gInSc7	výsledek
konfliktu	konflikt	k1gInSc2	konflikt
obou	dva	k4xCgFnPc2	dva
mocností	mocnost	k1gFnPc2	mocnost
bylo	být	k5eAaImAgNnS	být
zničení	zničení	k1gNnSc1	zničení
Kartága	Kartágo	k1gNnSc2	Kartágo
a	a	k8xC	a
dominantní	dominantní	k2eAgNnSc4d1	dominantní
postavení	postavení	k1gNnSc4	postavení
Říma	Řím	k1gInSc2	Řím
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příčiny	příčina	k1gFnPc1	příčina
punských	punský	k2eAgFnPc2d1	punská
válek	válka	k1gFnPc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Když	když	k8xS	když
Římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
dokázala	dokázat	k5eAaPmAgFnS	dokázat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
zhruba	zhruba	k6eAd1	zhruba
150	[number]	k4	150
let	léto	k1gNnPc2	léto
ovládnout	ovládnout	k5eAaPmF	ovládnout
Itálii	Itálie	k1gFnSc3	Itálie
po	po	k7c6	po
tvrdých	tvrdý	k2eAgInPc6d1	tvrdý
bojích	boj	k1gInPc6	boj
se	se	k3xPyFc4	se
Samnity	Samnita	k1gFnPc1	Samnita
a	a	k8xC	a
Sarmaty	Sarmat	k1gMnPc4	Sarmat
a	a	k8xC	a
porážce	porážka	k1gFnSc3	porážka
řeckých	řecký	k2eAgInPc2d1	řecký
států	stát	k1gInPc2	stát
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
Římská	římský	k2eAgFnSc1d1	římská
říše	říš	k1gFnSc2	říš
středomořskou	středomořský	k2eAgFnSc7d1	středomořská
mocností	mocnost	k1gFnSc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Nebyla	být	k5eNaImAgFnS	být
však	však	k9	však
mocností	mocnost	k1gFnSc7	mocnost
jedinou	jediný	k2eAgFnSc7d1	jediná
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
silou	síla	k1gFnSc7	síla
a	a	k8xC	a
významem	význam	k1gInSc7	význam
mohlo	moct	k5eAaImAgNnS	moct
rovnat	rovnat	k5eAaImF	rovnat
Římu	Řím	k1gInSc3	Řím
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Kartágo	Kartágo	k1gNnSc1	Kartágo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
obyvatele	obyvatel	k1gMnPc4	obyvatel
Římané	Říman	k1gMnPc1	Říman
nazývali	nazývat	k5eAaImAgMnP	nazývat
Punny	Punn	k1gInPc4	Punn
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
Kartágo	Kartágo	k1gNnSc1	Kartágo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Řím	Řím	k1gInSc4	Řím
měli	mít	k5eAaImAgMnP	mít
snahu	snaha	k1gFnSc4	snaha
o	o	k7c6	o
hegemonii	hegemonie	k1gFnSc6	hegemonie
nad	nad	k7c7	nad
celým	celý	k2eAgNnSc7d1	celé
Středomořím	středomoří	k1gNnSc7	středomoří
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
musela	muset	k5eAaImAgFnS	muset
zákonitě	zákonitě	k6eAd1	zákonitě
vypuknout	vypuknout	k5eAaPmF	vypuknout
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
vítěz	vítěz	k1gMnSc1	vítěz
pánem	pán	k1gMnSc7	pán
celého	celý	k2eAgInSc2d1	celý
středomořského	středomořský	k2eAgInSc2d1	středomořský
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
záminkou	záminka	k1gFnSc7	záminka
k	k	k7c3	k
první	první	k4xOgFnSc3	první
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
válek	válka	k1gFnPc2	válka
stala	stát	k5eAaPmAgFnS	stát
Sicílie	Sicílie	k1gFnSc1	Sicílie
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yRgFnSc4	který
s	s	k7c7	s
Kartágem	Kartágo	k1gNnSc7	Kartágo
již	již	k6eAd1	již
přes	přes	k7c4	přes
dvě	dva	k4xCgNnPc4	dva
století	století	k1gNnSc2	století
vedla	vést	k5eAaImAgFnS	vést
řecko-kartáginské	řeckoartáginský	k2eAgFnPc4d1	řecko-kartáginský
války	válka	k1gFnSc2	válka
sicilská	sicilský	k2eAgNnPc1d1	sicilské
města	město	k1gNnPc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
264	[number]	k4	264
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
==	==	k?	==
První	první	k4xOgFnSc1	první
punská	punský	k2eAgFnSc1d1	punská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
264	[number]	k4	264
<g/>
–	–	k?	–
<g/>
241	[number]	k4	241
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Příčinou	příčina	k1gFnSc7	příčina
první	první	k4xOgInSc4	první
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
krvavých	krvavý	k2eAgFnPc2d1	krvavá
válek	válka	k1gFnPc2	válka
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
městy	město	k1gNnPc7	město
bylo	být	k5eAaImAgNnS	být
obsazení	obsazení	k1gNnSc1	obsazení
italského	italský	k2eAgNnSc2d1	italské
města	město	k1gNnSc2	město
Messina	Messina	k1gFnSc1	Messina
syrakuským	syrakuský	k2eAgMnSc7d1	syrakuský
vládcem	vládce	k1gMnSc7	vládce
Agathoklem	Agathokl	k1gInSc7	Agathokl
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
požádalo	požádat	k5eAaPmAgNnS	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
Kartagince	Kartaginec	k1gMnSc2	Kartaginec
i	i	k9	i
Římany	Říman	k1gMnPc4	Říman
<g/>
,	,	kIx,	,
Kartaginci	Kartaginec	k1gMnPc1	Kartaginec
však	však	k9	však
dorazili	dorazit	k5eAaPmAgMnP	dorazit
o	o	k7c4	o
něco	něco	k3yInSc4	něco
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
připluli	připlout	k5eAaPmAgMnP	připlout
i	i	k9	i
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
chtěli	chtít	k5eAaImAgMnP	chtít
také	také	k9	také
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
Messině	Messin	k2eAgFnSc3d1	Messina
pomoci	pomoc	k1gFnSc3	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgMnSc1d1	římský
konzul	konzul	k1gMnSc1	konzul
Appius	Appius	k1gMnSc1	Appius
Claudius	Claudius	k1gMnSc1	Claudius
Caudex	Caudex	k1gInSc4	Caudex
se	se	k3xPyFc4	se
dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
s	s	k7c7	s
kartaginským	kartaginský	k2eAgMnSc7d1	kartaginský
velitelem	velitel	k1gMnSc7	velitel
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Kartaginci	Kartaginec	k1gMnPc1	Kartaginec
vrátí	vrátit	k5eAaPmIp3nP	vrátit
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
Kartágu	Kartágo	k1gNnSc6	Kartágo
však	však	k9	však
brala	brát	k5eAaImAgFnS	brát
velitelův	velitelův	k2eAgInSc4d1	velitelův
ústup	ústup	k1gInSc4	ústup
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Římanů	Říman	k1gMnPc2	Říman
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
a	a	k8xC	a
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Římu	Řím	k1gInSc3	Řím
válku	válek	k1gInSc2	válek
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
napětím	napětí	k1gNnSc7	napětí
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
trvalo	trvat	k5eAaImAgNnS	trvat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
272	[number]	k4	272
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Kartaginci	Kartaginec	k1gMnPc1	Kartaginec
bez	bez	k7c2	bez
předchozího	předchozí	k2eAgNnSc2d1	předchozí
oznámení	oznámení	k1gNnSc2	oznámení
svým	svůj	k3xOyFgMnPc3	svůj
římským	římský	k2eAgMnPc3d1	římský
spojencům	spojenec	k1gMnPc3	spojenec
vylodili	vylodit	k5eAaPmAgMnP	vylodit
v	v	k7c4	v
Tarentu	Tarenta	k1gFnSc4	Tarenta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
toto	tento	k3xDgNnSc4	tento
město	město	k1gNnSc4	město
vysvobodili	vysvobodit	k5eAaPmAgMnP	vysvobodit
od	od	k7c2	od
řecké	řecký	k2eAgFnSc2d1	řecká
Pyrrhovy	Pyrrhův	k2eAgFnSc2d1	Pyrrhova
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
dá	dát	k5eAaPmIp3nS	dát
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavními	hlavní	k2eAgInPc7d1	hlavní
důvody	důvod	k1gInPc7	důvod
punských	punský	k2eAgFnPc2d1	punská
válek	válka	k1gFnPc2	válka
bylo	být	k5eAaImAgNnS	být
mocenské	mocenský	k2eAgNnSc1d1	mocenské
soupeření	soupeření	k1gNnSc1	soupeření
<g/>
,	,	kIx,	,
politická	politický	k2eAgFnSc1d1	politická
rivalita	rivalita	k1gFnSc1	rivalita
a	a	k8xC	a
nenávist	nenávist	k1gFnSc1	nenávist
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
obavy	obava	k1gFnPc1	obava
z	z	k7c2	z
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
vyspělosti	vyspělost	k1gFnSc2	vyspělost
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zprvu	zprvu	k6eAd1	zprvu
obléhalo	obléhat	k5eAaImAgNnS	obléhat
Kartágo	Kartágo	k1gNnSc1	Kartágo
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Hannóna	Hannón	k1gMnSc2	Hannón
již	již	k6eAd1	již
zmiňovanou	zmiňovaný	k2eAgFnSc4d1	zmiňovaná
Mesinu	Mesina	k1gFnSc4	Mesina
<g/>
,	,	kIx,	,
Římané	Říman	k1gMnPc1	Říman
je	on	k3xPp3gInPc4	on
však	však	k9	však
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
města	město	k1gNnSc2	město
odehnali	odehnat	k5eAaPmAgMnP	odehnat
a	a	k8xC	a
roku	rok	k1gInSc2	rok
262	[number]	k4	262
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
po	po	k7c6	po
půlročním	půlroční	k2eAgNnSc6d1	půlroční
obléhání	obléhání	k1gNnSc6	obléhání
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
nejvýznamnějšího	významný	k2eAgNnSc2d3	nejvýznamnější
města	město	k1gNnSc2	město
Kartaginců	Kartaginec	k1gMnPc2	Kartaginec
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
Akragantu	Akragant	k1gInSc2	Akragant
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
souši	souš	k1gFnSc6	souš
tedy	tedy	k9	tedy
vítězil	vítězit	k5eAaImAgInS	vítězit
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
moře	moře	k1gNnSc1	moře
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
doménou	doména	k1gFnSc7	doména
Kartaginců	Kartaginec	k1gMnPc2	Kartaginec
<g/>
.	.	kIx.	.
</s>
<s>
Hannón	Hannón	k1gInSc1	Hannón
vysílal	vysílat	k5eAaImAgInS	vysílat
své	svůj	k3xOyFgFnPc4	svůj
lodě	loď	k1gFnPc4	loď
na	na	k7c4	na
římská	římský	k2eAgNnPc4d1	římské
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
plenily	plenit	k5eAaImAgFnP	plenit
a	a	k8xC	a
bránily	bránit	k5eAaImAgFnP	bránit
Římu	Řím	k1gInSc3	Řím
v	v	k7c6	v
posunu	posun	k1gInSc6	posun
posil	posila	k1gFnPc2	posila
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
utkat	utkat	k5eAaPmF	utkat
se	se	k3xPyFc4	se
s	s	k7c7	s
Kartágem	Kartágo	k1gNnSc7	Kartágo
i	i	k9	i
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
poprvé	poprvé	k6eAd1	poprvé
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
260	[number]	k4	260
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
u	u	k7c2	u
Liparských	Liparský	k2eAgInPc2d1	Liparský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
Řím	Řím	k1gInSc1	Řím
utrpěl	utrpět	k5eAaPmAgInS	utrpět
rychlou	rychlý	k2eAgFnSc4d1	rychlá
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
<g/>
,	,	kIx,	,
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Mylae	Myla	k1gFnSc2	Myla
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
ale	ale	k8xC	ale
římské	římský	k2eAgNnSc1d1	římské
loďstvo	loďstvo	k1gNnSc1	loďstvo
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
<g/>
,	,	kIx,	,
a	a	k8xC	a
nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
256	[number]	k4	256
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
porazil	porazit	k5eAaPmAgInS	porazit
Řím	Řím	k1gInSc1	Řím
kartaginské	kartaginský	k2eAgFnSc2d1	kartaginská
lodě	loď	k1gFnSc2	loď
také	také	k9	také
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Eknomu	Eknom	k1gInSc2	Eknom
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
námořní	námořní	k2eAgFnSc4d1	námořní
bitvu	bitva	k1gFnSc4	bitva
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
350	[number]	k4	350
kartaginských	kartaginský	k2eAgFnPc2d1	kartaginská
lodí	loď	k1gFnPc2	loď
jich	on	k3xPp3gFnPc2	on
Římani	říman	k1gMnPc1	říman
třicet	třicet	k4xCc1	třicet
potopili	potopit	k5eAaPmAgMnP	potopit
a	a	k8xC	a
64	[number]	k4	64
zajali	zajmout	k5eAaPmAgMnP	zajmout
a	a	k8xC	a
dohromady	dohromady	k6eAd1	dohromady
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
utkalo	utkat	k5eAaPmAgNnS	utkat
na	na	k7c4	na
300	[number]	k4	300
tisíc	tisíc	k4xCgInPc2	tisíc
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
obléháním	obléhání	k1gNnSc7	obléhání
Kartága	Kartágo	k1gNnSc2	Kartágo
ale	ale	k8xC	ale
na	na	k7c4	na
římské	římský	k2eAgFnPc4d1	římská
jednotky	jednotka	k1gFnPc4	jednotka
nečekaně	nečekaně	k6eAd1	nečekaně
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
spartský	spartský	k2eAgMnSc1d1	spartský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Xanthippus	Xanthippus	k1gMnSc1	Xanthippus
najatý	najatý	k2eAgMnSc1d1	najatý
Kartágem	Kartágo	k1gNnSc7	Kartágo
a	a	k8xC	a
roku	rok	k1gInSc2	rok
255	[number]	k4	255
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
římské	římský	k2eAgMnPc4d1	římský
vojáky	voják	k1gMnPc4	voják
drtivě	drtivě	k6eAd1	drtivě
porazil	porazit	k5eAaPmAgMnS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtrnáct	čtrnáct	k4xCc4	čtrnáct
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
zase	zase	k9	zase
římané	říman	k1gMnPc1	říman
porazili	porazit	k5eAaPmAgMnP	porazit
Kartagince	Kartaginec	k1gMnSc4	Kartaginec
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Aegatských	Aegatský	k2eAgInPc2d1	Aegatský
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
Kartágo	Kartágo	k1gNnSc1	Kartágo
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
vzdalo	vzdát	k5eAaPmAgNnS	vzdát
<g/>
,	,	kIx,	,
muselo	muset	k5eAaImAgNnS	muset
opustit	opustit	k5eAaPmF	opustit
všechna	všechen	k3xTgNnPc4	všechen
svá	svůj	k3xOyFgNnPc4	svůj
území	území	k1gNnPc4	území
na	na	k7c4	na
Sicílii	Sicílie	k1gFnSc4	Sicílie
a	a	k8xC	a
zaplatit	zaplatit	k5eAaPmF	zaplatit
přes	přes	k7c4	přes
83	[number]	k4	83
tun	tuna	k1gFnPc2	tuna
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhá	druhý	k4xOgFnSc1	druhý
punská	punský	k2eAgFnSc1d1	punská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
218	[number]	k4	218
<g/>
–	–	k?	–
<g/>
201	[number]	k4	201
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Situace	situace	k1gFnSc1	situace
Kartága	Kartágo	k1gNnSc2	Kartágo
po	po	k7c6	po
první	první	k4xOgFnSc6	první
punské	punský	k2eAgFnSc6d1	punská
válce	válka	k1gFnSc6	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
porážkách	porážka	k1gFnPc6	porážka
z	z	k7c2	z
první	první	k4xOgFnSc2	první
punské	punský	k2eAgFnSc2d1	punská
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
Kartágo	Kartágo	k1gNnSc1	Kartágo
finančně	finančně	k6eAd1	finančně
téměř	téměř	k6eAd1	téměř
zruinované	zruinovaný	k2eAgNnSc1d1	zruinované
<g/>
.	.	kIx.	.
</s>
<s>
Nemělo	mít	k5eNaImAgNnS	mít
ani	ani	k8xC	ani
na	na	k7c4	na
zaplacení	zaplacení	k1gNnSc4	zaplacení
a	a	k8xC	a
udržení	udržení	k1gNnSc4	udržení
vlastní	vlastní	k2eAgFnSc2d1	vlastní
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
241	[number]	k4	241
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
otroků	otrok	k1gMnPc2	otrok
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
trvalo	trvat	k5eAaImAgNnS	trvat
3	[number]	k4	3
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Potlačil	potlačit	k5eAaPmAgMnS	potlačit
ho	on	k3xPp3gMnSc4	on
až	až	k9	až
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
kartaginských	kartaginský	k2eAgMnPc2d1	kartaginský
vůdců	vůdce	k1gMnPc2	vůdce
–	–	k?	–
Hamilkar	Hamilkara	k1gFnPc2	Hamilkara
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
oponentem	oponent	k1gMnSc7	oponent
Hannónem	Hannón	k1gMnSc7	Hannón
výjimečně	výjimečně	k6eAd1	výjimečně
shodl	shodnout	k5eAaPmAgMnS	shodnout
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
věci	věc	k1gFnSc6	věc
–	–	k?	–
totiž	totiž	k9	totiž
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
Římu	Řím	k1gInSc3	Řím
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
novou	nový	k2eAgFnSc4d1	nová
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
zvítězit	zvítězit	k5eAaPmF	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
Hamilkar	Hamilkar	k1gMnSc1	Hamilkar
tedy	tedy	k9	tedy
zmobilizoval	zmobilizovat	k5eAaPmAgMnS	zmobilizovat
veškeré	veškerý	k3xTgInPc4	veškerý
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
sestavil	sestavit	k5eAaPmAgMnS	sestavit
novou	nový	k2eAgFnSc4d1	nová
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
severozápadní	severozápadní	k2eAgInSc4d1	severozápadní
cíp	cíp	k1gInSc4	cíp
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Odtamtud	odtamtud	k6eAd1	odtamtud
se	se	k3xPyFc4	se
přeplavil	přeplavit	k5eAaPmAgMnS	přeplavit
na	na	k7c4	na
sever	sever	k1gInSc4	sever
do	do	k7c2	do
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dobyl	dobýt	k5eAaPmAgInS	dobýt
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
strávených	strávený	k2eAgNnPc6d1	strávené
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
nové	nový	k2eAgFnSc6d1	nová
kolonii	kolonie	k1gFnSc6	kolonie
však	však	k9	však
nečekaně	nečekaně	k6eAd1	nečekaně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
pak	pak	k6eAd1	pak
za	za	k7c4	za
něj	on	k3xPp3gNnSc4	on
jeho	jeho	k3xOp3gMnSc1	jeho
zeť	zeť	k1gMnSc1	zeť
Hasdrubal	Hasdrubal	k1gMnSc1	Hasdrubal
Sličný	sličný	k2eAgMnSc1d1	sličný
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dobyl	dobýt	k5eAaPmAgMnS	dobýt
další	další	k2eAgNnSc4d1	další
území	území	k1gNnSc4	území
a	a	k8xC	a
na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgNnSc6d1	jihovýchodní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
založil	založit	k5eAaPmAgMnS	založit
Nové	Nové	k2eAgNnSc4d1	Nové
Kartágo	Kartágo	k1gNnSc4	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
Daně	daň	k1gFnPc4	daň
z	z	k7c2	z
podmaněných	podmaněný	k2eAgNnPc2d1	podmaněné
území	území	k1gNnPc2	území
pak	pak	k6eAd1	pak
brzy	brzy	k6eAd1	brzy
vrátily	vrátit	k5eAaPmAgFnP	vrátit
Kartágu	Kartágo	k1gNnSc3	Kartágo
jeho	jeho	k3xOp3gFnSc4	jeho
dřívější	dřívější	k2eAgFnSc4d1	dřívější
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
konečně	konečně	k6eAd1	konečně
plně	plně	k6eAd1	plně
připraveno	připravit	k5eAaPmNgNnS	připravit
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
Římany	Říman	k1gMnPc7	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
221	[number]	k4	221
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
padl	padnout	k5eAaImAgInS	padnout
nečekaně	nečekaně	k6eAd1	nečekaně
i	i	k9	i
Hasdrubal	Hasdrubal	k1gFnSc1	Hasdrubal
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
velení	velení	k1gNnSc2	velení
ujal	ujmout	k5eAaPmAgMnS	ujmout
Hamilkarův	Hamilkarův	k2eAgMnSc1d1	Hamilkarův
syn	syn	k1gMnSc1	syn
Hannibal	Hannibal	k1gInSc1	Hannibal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Začátek	začátek	k1gInSc4	začátek
druhé	druhý	k4xOgFnSc2	druhý
punské	punský	k2eAgFnSc2d1	punská
války	válka	k1gFnSc2	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Hannibal	Hannibal	k1gInSc1	Hannibal
ujal	ujmout	k5eAaPmAgInS	ujmout
vrchního	vrchní	k2eAgNnSc2d1	vrchní
velení	velení	k1gNnSc2	velení
<g/>
,	,	kIx,	,
dobyl	dobýt	k5eAaPmAgInS	dobýt
roku	rok	k1gInSc2	rok
220	[number]	k4	220
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
již	již	k9	již
zase	zase	k9	zase
velmi	velmi	k6eAd1	velmi
silnou	silný	k2eAgFnSc7d1	silná
<g/>
)	)	kIx)	)
armádou	armáda	k1gFnSc7	armáda
město	město	k1gNnSc4	město
Saguntum	Saguntum	k1gNnSc1	Saguntum
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
mělo	mít	k5eAaImAgNnS	mít
s	s	k7c7	s
Římem	Řím	k1gInSc7	Řím
spojeneckou	spojenecký	k2eAgFnSc4d1	spojenecká
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Řím	Řím	k1gInSc1	Řím
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
oslaben	oslabit	k5eAaPmNgInS	oslabit
válkami	válka	k1gFnPc7	válka
s	s	k7c7	s
Galy	Gal	k1gMnPc7	Gal
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jen	jen	k6eAd1	jen
vyslal	vyslat	k5eAaPmAgMnS	vyslat
do	do	k7c2	do
Kartága	Kartágo	k1gNnSc2	Kartágo
posly	posel	k1gMnPc4	posel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
žádali	žádat	k5eAaImAgMnP	žádat
o	o	k7c6	o
vydání	vydání	k1gNnSc6	vydání
Hannibala	Hannibal	k1gMnSc2	Hannibal
k	k	k7c3	k
potrestání	potrestání	k1gNnSc3	potrestání
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
však	však	k9	však
hrdě	hrdě	k6eAd1	hrdě
odpovědělo	odpovědět	k5eAaPmAgNnS	odpovědět
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
218	[number]	k4	218
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pak	pak	k6eAd1	pak
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
Hannibal	Hannibal	k1gInSc4	Hannibal
z	z	k7c2	z
Nového	Nového	k2eAgNnSc2d1	Nového
Kartága	Kartágo	k1gNnSc2	Kartágo
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
armáda	armáda	k1gFnSc1	armáda
čítala	čítat	k5eAaImAgFnS	čítat
50	[number]	k4	50
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
,	,	kIx,	,
9	[number]	k4	9
000	[number]	k4	000
jezdců	jezdec	k1gMnPc2	jezdec
a	a	k8xC	a
37	[number]	k4	37
válečných	válečný	k2eAgMnPc2d1	válečný
slonů	slon	k1gMnPc2	slon
<g/>
,	,	kIx,	,
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
Alp	Alpy	k1gFnPc2	Alpy
a	a	k8xC	a
Pyrenejí	Pyreneje	k1gFnPc2	Pyreneje
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
33	[number]	k4	33
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
přivyklých	přivyklý	k2eAgMnPc2d1	přivyklý
obtížím	obtížit	k5eAaPmIp1nS	obtížit
pod	pod	k7c7	pod
africkým	africký	k2eAgNnSc7d1	africké
sluncem	slunce	k1gNnSc7	slunce
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
sněhu	sníh	k1gInSc2	sníh
<g/>
,	,	kIx,	,
dravé	dravý	k2eAgFnSc3d1	dravá
zvěři	zvěř	k1gFnSc3	zvěř
<g/>
,	,	kIx,	,
pralesům	prales	k1gInPc3	prales
a	a	k8xC	a
terénu	terén	k1gInSc6	terén
velehor	velehora	k1gFnPc2	velehora
bez	bez	k7c2	bez
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
mostů	most	k1gInPc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
Pyrenejí	Pyreneje	k1gFnPc2	Pyreneje
a	a	k8xC	a
překročili	překročit	k5eAaPmAgMnP	překročit
řeku	řeka	k1gFnSc4	řeka
Rhônu	Rhôna	k1gFnSc4	Rhôna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
jim	on	k3xPp3gMnPc3	on
pomohli	pomoct	k5eAaPmAgMnP	pomoct
Galové	Gal	k1gMnPc1	Gal
<g/>
,	,	kIx,	,
nespokojeni	spokojen	k2eNgMnPc1d1	nespokojen
s	s	k7c7	s
nadvládou	nadvláda	k1gFnSc7	nadvláda
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Drtivé	drtivý	k2eAgFnSc2d1	drtivá
porážky	porážka	k1gFnSc2	porážka
Říma	Řím	k1gInSc2	Řím
===	===	k?	===
</s>
</p>
<p>
<s>
U	u	k7c2	u
řek	řeka	k1gFnPc2	řeka
Ticin	Ticin	k1gInSc1	Ticin
a	a	k8xC	a
Trebia	Trebia	k1gFnSc1	Trebia
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc4	on
Římané	Říman	k1gMnPc1	Říman
marně	marně	k6eAd1	marně
pokusili	pokusit	k5eAaPmAgMnP	pokusit
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
při	při	k7c6	při
pochodu	pochod	k1gInSc6	pochod
přes	přes	k7c4	přes
Pyreneje	Pyreneje	k1gFnPc4	Pyreneje
a	a	k8xC	a
Alpy	Alpy	k1gFnPc4	Alpy
ztratil	ztratit	k5eAaPmAgInS	ztratit
Hannibal	Hannibal	k1gInSc1	Hannibal
přes	přes	k7c4	přes
polovinu	polovina	k1gFnSc4	polovina
svých	svůj	k3xOyFgMnPc2	svůj
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
216	[number]	k4	216
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
s	s	k7c7	s
Římany	Říman	k1gMnPc7	Říman
střetl	střetnout	k5eAaPmAgMnS	střetnout
ve	v	k7c4	v
světově	světově	k6eAd1	světově
proslulé	proslulý	k2eAgFnSc6d1	proslulá
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Cann	Canna	k1gFnPc2	Canna
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
zde	zde	k6eAd1	zde
měli	mít	k5eAaImAgMnP	mít
80	[number]	k4	80
000	[number]	k4	000
pěšáků	pěšák	k1gMnPc2	pěšák
a	a	k8xC	a
6000	[number]	k4	6000
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
jízdy	jízda	k1gFnSc2	jízda
pak	pak	k6eAd1	pak
stanuli	stanout	k5eAaPmAgMnP	stanout
dva	dva	k4xCgMnPc1	dva
římští	římský	k2eAgMnPc1d1	římský
konzulové	konzul	k1gMnPc1	konzul
–	–	k?	–
Gaius	Gaius	k1gMnSc1	Gaius
Terentius	Terentius	k1gMnSc1	Terentius
Varro	Varro	k1gNnSc4	Varro
a	a	k8xC	a
Lucius	Lucius	k1gMnSc1	Lucius
Aemilius	Aemilius	k1gMnSc1	Aemilius
Paulus	Paulus	k1gMnSc1	Paulus
<g/>
.	.	kIx.	.
</s>
<s>
Paulus	Paulus	k1gMnSc1	Paulus
se	se	k3xPyFc4	se
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
k	k	k7c3	k
boji	boj	k1gInSc3	boj
neodhodlal	odhodlat	k5eNaPmAgMnS	odhodlat
<g/>
.	.	kIx.	.
</s>
<s>
Varro	Varro	k6eAd1	Varro
proto	proto	k8xC	proto
hned	hned	k6eAd1	hned
ráno	ráno	k6eAd1	ráno
v	v	k7c4	v
den	den	k1gInSc4	den
bitvy	bitva	k1gFnSc2	bitva
rozestavěl	rozestavět	k5eAaPmAgMnS	rozestavět
své	svůj	k3xOyFgFnPc4	svůj
jednotky	jednotka	k1gFnPc4	jednotka
do	do	k7c2	do
šiku	šik	k1gInSc2	šik
a	a	k8xC	a
dal	dát	k5eAaPmAgInS	dát
rozkaz	rozkaz	k1gInSc1	rozkaz
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Hannibalovi	Hannibalův	k2eAgMnPc1d1	Hannibalův
vojáci	voják	k1gMnPc1	voják
zaujali	zaujmout	k5eAaPmAgMnP	zaujmout
rozestavění	rozestavění	k1gNnSc4	rozestavění
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
půlměsíce	půlměsíc	k1gInSc2	půlměsíc
a	a	k8xC	a
uprostřed	uprostřed	k7c2	uprostřed
bojů	boj	k1gInPc2	boj
pak	pak	k6eAd1	pak
římské	římský	k2eAgMnPc4d1	římský
vojáky	voják	k1gMnPc4	voják
napadli	napadnout	k5eAaPmAgMnP	napadnout
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
kruté	krutý	k2eAgFnSc6d1	krutá
bitvě	bitva	k1gFnSc6	bitva
padlo	padnout	k5eAaPmAgNnS	padnout
na	na	k7c4	na
70	[number]	k4	70
000	[number]	k4	000
římských	římský	k2eAgMnPc2d1	římský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
211	[number]	k4	211
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
Říma	Řím	k1gInSc2	Řím
začalo	začít	k5eAaPmAgNnS	začít
rozléhat	rozléhat	k5eAaImF	rozléhat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hannibal	Hannibal	k1gInSc1	Hannibal
před	před	k7c7	před
branami	brána	k1gFnPc7	brána
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Obrat	obrat	k1gInSc1	obrat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Hannibalovo	Hannibalův	k2eAgNnSc1d1	Hannibalovo
přiblížení	přiblížení	k1gNnSc1	přiblížení
se	se	k3xPyFc4	se
k	k	k7c3	k
Římu	Řím	k1gInSc3	Řím
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
ohromnou	ohromný	k2eAgFnSc4d1	ohromná
paniku	panika	k1gFnSc4	panika
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
zakázal	zakázat	k5eAaPmAgInS	zakázat
pozůstalým	pozůstalý	k1gMnPc3	pozůstalý
vdovám	vdova	k1gFnPc3	vdova
po	po	k7c6	po
padlých	padlý	k2eAgMnPc6d1	padlý
vojácích	voják	k1gMnPc6	voják
truchlit	truchlit	k5eAaImF	truchlit
a	a	k8xC	a
povolal	povolat	k5eAaPmAgMnS	povolat
do	do	k7c2	do
zbraně	zbraň	k1gFnSc2	zbraň
všechny	všechen	k3xTgMnPc4	všechen
muže	muž	k1gMnPc4	muž
starší	starý	k2eAgMnPc1d2	starší
17	[number]	k4	17
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Udělil	udělit	k5eAaPmAgMnS	udělit
také	také	k6eAd1	také
amnestii	amnestie	k1gFnSc4	amnestie
6000	[number]	k4	6000
otroků	otrok	k1gMnPc2	otrok
a	a	k8xC	a
začlenil	začlenit	k5eAaPmAgMnS	začlenit
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
210	[number]	k4	210
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Římané	Říman	k1gMnPc1	Říman
poslední	poslední	k2eAgFnSc2d1	poslední
kartaginské	kartaginský	k2eAgFnSc2d1	kartaginská
město	město	k1gNnSc4	město
na	na	k7c4	na
Sicílii	Sicílie	k1gFnSc4	Sicílie
<g/>
,	,	kIx,	,
Akragás	Akragás	k1gInSc4	Akragás
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
Tarentu	Tarent	k1gInSc2	Tarent
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
Hannibalovým	Hannibalův	k2eAgInSc7d1	Hannibalův
hlavním	hlavní	k2eAgInSc7d1	hlavní
opěrným	opěrný	k2eAgInSc7d1	opěrný
bodem	bod	k1gInSc7	bod
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
opět	opět	k6eAd1	opět
dostala	dostat	k5eAaPmAgFnS	dostat
pod	pod	k7c4	pod
kontrolu	kontrola	k1gFnSc4	kontrola
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
roce	rok	k1gInSc6	rok
209	[number]	k4	209
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
dobýt	dobýt	k5eAaPmF	dobýt
také	také	k9	také
Nové	Nové	k2eAgNnSc4d1	Nové
Kartágo	Kartágo	k1gNnSc4	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
204	[number]	k4	204
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
vylodili	vylodit	k5eAaPmAgMnP	vylodit
u	u	k7c2	u
africké	africký	k2eAgFnSc2d1	africká
Utiky	Utika	k1gFnSc2	Utika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Porážka	porážka	k1gFnSc1	porážka
Kartága	Kartágo	k1gNnSc2	Kartágo
===	===	k?	===
</s>
</p>
<p>
<s>
Hannibal	Hannibal	k1gInSc1	Hannibal
tak	tak	k9	tak
musel	muset	k5eAaImAgInS	muset
rychle	rychle	k6eAd1	rychle
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
Kartágu	Kartágo	k1gNnSc3	Kartágo
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
202	[number]	k4	202
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
s	s	k7c7	s
Římany	Říman	k1gMnPc7	Říman
utkal	utkat	k5eAaPmAgMnS	utkat
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Zamy	Zama	k1gFnSc2	Zama
(	(	kIx(	(
<g/>
asi	asi	k9	asi
150	[number]	k4	150
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Publius	Publius	k1gMnSc1	Publius
Cornelius	Cornelius	k1gMnSc1	Cornelius
Scipio	Scipio	k1gMnSc1	Scipio
pak	pak	k6eAd1	pak
nadiktoval	nadiktovat	k5eAaPmAgMnS	nadiktovat
Kartágu	Kartágo	k1gNnSc3	Kartágo
mír	mír	k1gInSc1	mír
<g/>
.	.	kIx.	.
</s>
<s>
Muselo	muset	k5eAaImAgNnS	muset
se	se	k3xPyFc4	se
vzdát	vzdát	k5eAaPmF	vzdát
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Říma	Řím	k1gInSc2	Řím
všech	všecek	k3xTgNnPc2	všecek
území	území	k1gNnPc2	území
mimo	mimo	k7c4	mimo
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
,	,	kIx,	,
vydat	vydat	k5eAaPmF	vydat
všechny	všechen	k3xTgFnPc4	všechen
lodě	loď	k1gFnPc4	loď
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
ve	v	k7c6	v
splátkách	splátka	k1gFnPc6	splátka
rozložených	rozložený	k2eAgInPc2d1	rozložený
do	do	k7c2	do
50	[number]	k4	50
let	léto	k1gNnPc2	léto
splatit	splatit	k5eAaPmF	splatit
10	[number]	k4	10
000	[number]	k4	000
talentů	talent	k1gInPc2	talent
(	(	kIx(	(
<g/>
262	[number]	k4	262
tun	tuna	k1gFnPc2	tuna
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k1gFnSc1	moc
Kartága	Kartágo	k1gNnPc4	Kartágo
tak	tak	k9	tak
byla	být	k5eAaImAgFnS	být
zlomena	zlomit	k5eAaPmNgFnS	zlomit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Třetí	třetí	k4xOgFnSc1	třetí
punská	punský	k2eAgFnSc1d1	punská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
149	[number]	k4	149
<g/>
–	–	k?	–
<g/>
146	[number]	k4	146
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Situace	situace	k1gFnSc1	situace
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
punské	punský	k2eAgFnSc6d1	punská
válce	válka	k1gFnSc6	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
důsledkem	důsledek	k1gInSc7	důsledek
tvrdých	tvrdý	k2eAgFnPc2d1	tvrdá
podmínek	podmínka	k1gFnPc2	podmínka
míru	mír	k1gInSc2	mír
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
africký	africký	k2eAgMnSc1d1	africký
spojenec	spojenec	k1gMnSc1	spojenec
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
numidský	numidský	k2eAgMnSc1d1	numidský
král	král	k1gMnSc1	král
Massinissa	Massiniss	k1gMnSc2	Massiniss
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Římanům	Říman	k1gMnPc3	Říman
pomohl	pomoct	k5eAaPmAgInS	pomoct
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Zamy	Zama	k1gFnSc2	Zama
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
téměř	téměř	k6eAd1	téměř
volně	volně	k6eAd1	volně
zabírat	zabírat	k5eAaImF	zabírat
kartaginská	kartaginský	k2eAgNnPc4d1	kartaginské
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
Kartágo	Kartágo	k1gNnSc1	Kartágo
vyprovokovalo	vyprovokovat	k5eAaPmAgNnS	vyprovokovat
a	a	k8xC	a
odvážilo	odvážit	k5eAaPmAgNnS	odvážit
se	se	k3xPyFc4	se
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
numidské	numidský	k2eAgInPc4d1	numidský
požadavky	požadavek	k1gInPc4	požadavek
a	a	k8xC	a
zabírání	zabírání	k1gNnSc4	zabírání
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
odpovědět	odpovědět	k5eAaPmF	odpovědět
protiútokem	protiútok	k1gInSc7	protiútok
(	(	kIx(	(
<g/>
151	[number]	k4	151
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Řím	Řím	k1gInSc1	Řím
to	ten	k3xDgNnSc1	ten
kvalifikoval	kvalifikovat	k5eAaBmAgInS	kvalifikovat
jako	jako	k9	jako
porušení	porušení	k1gNnSc4	porušení
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
roku	rok	k1gInSc2	rok
149	[number]	k4	149
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vypověděl	vypovědět	k5eAaPmAgInS	vypovědět
Kartágu	Kartágo	k1gNnSc6	Kartágo
novou	nový	k2eAgFnSc4d1	nová
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
poté	poté	k6eAd1	poté
ale	ale	k8xC	ale
Kartaginská	kartaginský	k2eAgFnSc1d1	kartaginská
státní	státní	k2eAgFnSc1d1	státní
rada	rada	k1gFnSc1	rada
vyslala	vyslat	k5eAaPmAgFnS	vyslat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
posly	posel	k1gMnPc4	posel
s	s	k7c7	s
omluvou	omluva	k1gFnSc7	omluva
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
podřídí	podřídit	k5eAaPmIp3nS	podřídit
požadavkům	požadavek	k1gInPc3	požadavek
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Konzul	konzul	k1gMnSc1	konzul
Lucius	Lucius	k1gMnSc1	Lucius
Marcius	Marcius	k1gMnSc1	Marcius
Censorinus	Censorinus	k1gMnSc1	Censorinus
tedy	tedy	k9	tedy
přikázal	přikázat	k5eAaPmAgMnS	přikázat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
Kartaginci	Kartaginec	k1gMnPc1	Kartaginec
vydali	vydat	k5eAaPmAgMnP	vydat
své	svůj	k3xOyFgFnPc4	svůj
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
učinili	učinit	k5eAaImAgMnP	učinit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
podmínka	podmínka	k1gFnSc1	podmínka
nicméně	nicméně	k8xC	nicméně
zněla	znět	k5eAaImAgFnS	znět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
své	svůj	k3xOyFgNnSc4	svůj
město	město	k1gNnSc4	město
neprodleně	prodleně	k6eNd1	prodleně
zbourali	zbourat	k5eAaPmAgMnP	zbourat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
vzdáleném	vzdálený	k2eAgNnSc6d1	vzdálené
minimálně	minimálně	k6eAd1	minimálně
10	[number]	k4	10
mil	míle	k1gFnPc2	míle
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
mohou	moct	k5eAaImIp3nP	moct
vybudovat	vybudovat	k5eAaPmF	vybudovat
město	město	k1gNnSc4	město
nové	nový	k2eAgNnSc4d1	nové
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
co	co	k8xS	co
takovýto	takovýto	k3xDgInSc1	takovýto
požadavek	požadavek	k1gInSc1	požadavek
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
své	svůj	k3xOyFgFnPc4	svůj
hradby	hradba	k1gFnPc4	hradba
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
vyrábět	vyrábět	k5eAaImF	vyrábět
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
hlásit	hlásit	k5eAaImF	hlásit
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gMnPc2	jeho
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Římské	římský	k2eAgNnSc1d1	římské
obléhání	obléhání	k1gNnSc1	obléhání
a	a	k8xC	a
zánik	zánik	k1gInSc1	zánik
Kartága	Kartágo	k1gNnSc2	Kartágo
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
149	[number]	k4	149
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vyslal	vyslat	k5eAaPmAgMnS	vyslat
Řím	Řím	k1gInSc4	Řím
na	na	k7c6	na
území	území	k1gNnSc6	území
Kartága	Kartágo	k1gNnSc2	Kartágo
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
několik	několik	k4yIc4	několik
menších	malý	k2eAgFnPc2d2	menší
porážek	porážka	k1gFnPc2	porážka
a	a	k8xC	a
trpěla	trpět	k5eAaImAgFnS	trpět
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
epidemií	epidemie	k1gFnPc2	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgFnPc1d1	rozhodující
akce	akce	k1gFnPc1	akce
vojsko	vojsko	k1gNnSc1	vojsko
podniklo	podniknout	k5eAaPmAgNnS	podniknout
až	až	k9	až
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
konzula	konzul	k1gMnSc2	konzul
Publia	Publius	k1gMnSc2	Publius
Cornelia	Cornelius	k1gMnSc2	Cornelius
Scipiona	Scipion	k1gMnSc2	Scipion
Aemiliana	Aemilian	k1gMnSc2	Aemilian
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
loďstvem	loďstvo	k1gNnSc7	loďstvo
zablokoval	zablokovat	k5eAaPmAgInS	zablokovat
kartaginský	kartaginský	k2eAgInSc1d1	kartaginský
přístav	přístav	k1gInSc1	přístav
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
město	město	k1gNnSc4	město
obléhat	obléhat	k5eAaImF	obléhat
<g/>
.	.	kIx.	.
</s>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
rok	rok	k1gInSc4	rok
odříznuto	odříznut	k2eAgNnSc4d1	odříznuto
od	od	k7c2	od
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
jen	jen	k9	jen
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
vlastními	vlastní	k2eAgFnPc7d1	vlastní
zásobami	zásoba	k1gFnPc7	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
Kartaginci	Kartaginec	k1gMnPc1	Kartaginec
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
146	[number]	k4	146
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
Římanům	Říman	k1gMnPc3	Říman
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
usoudil	usoudit	k5eAaPmAgMnS	usoudit
Aemilianus	Aemilianus	k1gMnSc1	Aemilianus
<g/>
,	,	kIx,	,
že	že	k8xS	že
nastal	nastat	k5eAaPmAgInS	nastat
ten	ten	k3xDgInSc1	ten
pravý	pravý	k2eAgInSc1d1	pravý
čas	čas	k1gInSc1	čas
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
dní	den	k1gInPc2	den
a	a	k8xC	a
šest	šest	k4xCc1	šest
nocí	noc	k1gFnPc2	noc
zuřily	zuřit	k5eAaImAgInP	zuřit
boje	boj	k1gInPc1	boj
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
římské	římský	k2eAgFnPc1d1	římská
legie	legie	k1gFnPc1	legie
dobyly	dobýt	k5eAaPmAgFnP	dobýt
hrad	hrad	k1gInSc4	hrad
Byrsu	Byrs	k1gInSc2	Byrs
<g/>
.	.	kIx.	.
</s>
<s>
Osud	osud	k1gInSc1	osud
Kartága	Kartágo	k1gNnSc2	Kartágo
byl	být	k5eAaImAgInS	být
navždy	navždy	k6eAd1	navždy
zpečetěn	zpečetit	k5eAaPmNgInS	zpečetit
<g/>
.	.	kIx.	.
</s>
<s>
Přeživší	přeživší	k2eAgMnPc1d1	přeživší
obyvatelé	obyvatel	k1gMnPc1	obyvatel
byli	být	k5eAaImAgMnP	být
prodáni	prodat	k5eAaPmNgMnP	prodat
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
zmizelo	zmizet	k5eAaPmAgNnS	zmizet
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
údajně	údajně	k6eAd1	údajně
dokonce	dokonce	k9	dokonce
posoleno	posolen	k2eAgNnSc1d1	posoleno
a	a	k8xC	a
prokleto	proklet	k2eAgNnSc1d1	prokleto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nikdy	nikdy	k6eAd1	nikdy
více	hodně	k6eAd2	hodně
nemohlo	moct	k5eNaImAgNnS	moct
být	být	k5eAaImF	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledek	výsledek	k1gInSc1	výsledek
punských	punský	k2eAgFnPc2d1	punská
válek	válka	k1gFnPc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
válek	válka	k1gFnPc2	válka
bylo	být	k5eAaImAgNnS	být
zničení	zničení	k1gNnSc1	zničení
Kartága	Kartágo	k1gNnSc2	Kartágo
a	a	k8xC	a
hegemonie	hegemonie	k1gFnSc2	hegemonie
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zcela	zcela	k6eAd1	zcela
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
většinu	většina	k1gFnSc4	většina
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Hispánie	Hispánie	k1gFnSc2	Hispánie
<g/>
,	,	kIx,	,
Sicílie	Sicílie	k1gFnSc2	Sicílie
a	a	k8xC	a
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
dlouho	dlouho	k6eAd1	dlouho
považovali	považovat	k5eAaImAgMnP	považovat
války	válek	k1gInPc4	válek
s	s	k7c7	s
Kartágem	Kartágo	k1gNnSc7	Kartágo
za	za	k7c4	za
nejtěžší	těžký	k2eAgFnPc4d3	nejtěžší
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc4	jaký
kdy	kdy	k6eAd1	kdy
vedli	vést	k5eAaImAgMnP	vést
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
existence	existence	k1gFnSc2	existence
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
vnímání	vnímání	k1gNnSc1	vnímání
potlačeno	potlačit	k5eAaPmNgNnS	potlačit
katastrofálními	katastrofální	k2eAgFnPc7d1	katastrofální
porážkami	porážka	k1gFnPc7	porážka
a	a	k8xC	a
rozpadem	rozpad	k1gInSc7	rozpad
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
díky	díky	k7c3	díky
zániku	zánik	k1gInSc3	zánik
samotného	samotný	k2eAgNnSc2d1	samotné
města	město	k1gNnSc2	město
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
i	i	k9	i
kartaginská	kartaginský	k2eAgFnSc1d1	kartaginská
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
posledním	poslední	k2eAgMnSc7d1	poslední
zástupcem	zástupce	k1gMnSc7	zástupce
kultury	kultura	k1gFnSc2	kultura
Féničanů	Féničan	k1gMnPc2	Féničan
a	a	k8xC	a
také	také	k9	také
téměř	téměř	k6eAd1	téměř
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
féničtina	féničtina	k1gFnSc1	féničtina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
se	se	k3xPyFc4	se
fénické	fénický	k2eAgNnSc1d1	fénické
písmo	písmo	k1gNnSc1	písmo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
dalo	dát	k5eAaPmAgNnS	dát
základ	základ	k1gInSc4	základ
písmu	písmo	k1gNnSc6	písmo
řeckému	řecký	k2eAgNnSc3d1	řecké
a	a	k8xC	a
také	také	k9	také
latince	latinka	k1gFnSc6	latinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
všechna	všechen	k3xTgNnPc4	všechen
předchozí	předchozí	k2eAgNnPc4d1	předchozí
utrpení	utrpení	k1gNnPc4	utrpení
Kartágo	Kartágo	k1gNnSc1	Kartágo
navěky	navěky	k6eAd1	navěky
nezahynulo	zahynout	k5eNaPmAgNnS	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
zničení	zničení	k1gNnSc6	zničení
ho	on	k3xPp3gMnSc4	on
dal	dát	k5eAaPmAgInS	dát
znovu	znovu	k6eAd1	znovu
vybudovat	vybudovat	k5eAaPmF	vybudovat
Gaius	Gaius	k1gInSc4	Gaius
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
již	již	k9	již
jako	jako	k9	jako
město	město	k1gNnSc1	město
římské	římský	k2eAgNnSc1d1	římské
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
také	také	k9	také
nabylo	nabýt	k5eAaPmAgNnS	nabýt
svého	svůj	k3xOyFgInSc2	svůj
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
obchodního	obchodní	k2eAgInSc2d1	obchodní
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Udržela	udržet	k5eAaPmAgFnS	udržet
se	se	k3xPyFc4	se
i	i	k9	i
punská	punský	k2eAgFnSc1d1	punská
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
mluvil	mluvit	k5eAaImAgMnS	mluvit
jí	jíst	k5eAaImIp3nS	jíst
např.	např.	kA	např.
císař	císař	k1gMnSc1	císař
Septimius	Septimius	k1gMnSc1	Septimius
Severus	Severus	k1gMnSc1	Severus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Foiničanech	Foiničan	k1gMnPc6	Foiničan
a	a	k8xC	a
Římanech	Říman	k1gMnPc6	Říman
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
Vandalové	Vandal	k1gMnPc1	Vandal
<g/>
,	,	kIx,	,
Byzantici	Byzantik	k1gMnPc1	Byzantik
<g/>
,	,	kIx,	,
Turci	Turek	k1gMnPc1	Turek
<g/>
,	,	kIx,	,
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
,	,	kIx,	,
Francouzi	Francouz	k1gMnPc1	Francouz
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
měl	mít	k5eAaImAgMnS	mít
svůj	svůj	k3xOyFgInSc4	svůj
stan	stan	k1gInSc4	stan
generál	generál	k1gMnSc1	generál
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
<g/>
.	.	kIx.	.
</s>
<s>
Natrvalo	natrvalo	k6eAd1	natrvalo
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
pak	pak	k6eAd1	pak
usadili	usadit	k5eAaPmAgMnP	usadit
Arabové	Arab	k1gMnPc1	Arab
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejnavštěvovanějších	navštěvovaný	k2eAgFnPc2d3	nejnavštěvovanější
oblastí	oblast	k1gFnPc2	oblast
Tuniska	Tunisko	k1gNnSc2	Tunisko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
LIVIUS	LIVIUS	kA	LIVIUS
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
I.	I.	kA	I.
<g/>
–	–	k?	–
<g/>
VII	VII	kA	VII
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Punské	punský	k2eAgFnSc2d1	punská
války	válka	k1gFnSc2	válka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
