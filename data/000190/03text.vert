<s>
SkyTeam	SkyTeam	k1gInSc1	SkyTeam
(	(	kIx(	(
<g/>
SKYTEAM	SKYTEAM	kA	SKYTEAM
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
globální	globální	k2eAgFnSc1d1	globální
aliance	aliance	k1gFnSc1	aliance
leteckých	letecký	k2eAgFnPc2d1	letecká
společností	společnost	k1gFnPc2	společnost
různých	různý	k2eAgInPc2d1	různý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2000	[number]	k4	2000
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
zakladateli	zakladatel	k1gMnPc7	zakladatel
<g/>
:	:	kIx,	:
Aeroméxico	Aeroméxico	k6eAd1	Aeroméxico
<g/>
,	,	kIx,	,
Air	Air	k1gMnSc4	Air
France	Franc	k1gMnSc4	Franc
<g/>
,	,	kIx,	,
Delta	delta	k1gFnSc1	delta
Air	Air	k1gMnSc1	Air
Lines	Lines	k1gMnSc1	Lines
a	a	k8xC	a
Korean	Korean	k1gMnSc1	Korean
Air	Air	k1gMnSc1	Air
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
leteckou	letecký	k2eAgFnSc7d1	letecká
aliancí	aliance	k1gFnSc7	aliance
po	po	k7c6	po
Star	star	k1gFnSc6	star
Alliance	Allianka	k1gFnSc6	Allianka
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
celosvětově	celosvětově	k6eAd1	celosvětově
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
síti	síť	k1gFnSc3	síť
působení	působení	k1gNnSc2	působení
může	moct	k5eAaImIp3nS	moct
chlubit	chlubit	k5eAaImF	chlubit
1000	[number]	k4	1000
destinacemi	destinace	k1gFnPc7	destinace
ve	v	k7c6	v
přibližně	přibližně	k6eAd1	přibližně
170	[number]	k4	170
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
s	s	k7c7	s
přes	přes	k7c4	přes
15	[number]	k4	15
000	[number]	k4	000
lety	let	k1gInPc7	let
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
členy	člen	k1gMnPc7	člen
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
určitých	určitý	k2eAgFnPc6d1	určitá
linkách	linka	k1gFnPc6	linka
provozován	provozován	k2eAgInSc1d1	provozován
tzv.	tzv.	kA	tzv.
codesharing	codesharing	k1gInSc1	codesharing
<g/>
.	.	kIx.	.
</s>
<s>
Aerolinky	aerolinka	k1gFnPc1	aerolinka
sdružené	sdružený	k2eAgFnPc1d1	sdružená
do	do	k7c2	do
aliance	aliance	k1gFnSc2	aliance
SkyTeam	SkyTeam	k1gInSc4	SkyTeam
si	se	k3xPyFc3	se
také	také	k9	také
vzájemně	vzájemně	k6eAd1	vzájemně
uznávají	uznávat	k5eAaImIp3nP	uznávat
své	svůj	k3xOyFgInPc4	svůj
věrnostní	věrnostní	k2eAgInPc4d1	věrnostní
programy	program	k1gInPc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
míle	míle	k1gFnPc4	míle
proletěné	proletěný	k2eAgFnPc4d1	proletěný
s	s	k7c7	s
jiným	jiný	k2eAgMnSc7d1	jiný
partnerem	partner	k1gMnSc7	partner
vám	vy	k3xPp2nPc3	vy
proto	proto	k8xC	proto
například	například	k6eAd1	například
České	český	k2eAgFnPc1d1	Česká
aerolinie	aerolinie	k1gFnPc1	aerolinie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
členem	člen	k1gMnSc7	člen
aliance	aliance	k1gFnSc2	aliance
od	od	k7c2	od
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
načtou	načíst	k5eAaPmIp3nP	načíst
body	bod	k1gInPc1	bod
do	do	k7c2	do
svého	své	k1gNnSc2	své
OK	oka	k1gFnPc2	oka
plus	plus	k6eAd1	plus
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2000	[number]	k4	2000
oznámily	oznámit	k5eAaPmAgFnP	oznámit
letecké	letecký	k2eAgFnPc1d1	letecká
společnosti	společnost	k1gFnPc1	společnost
Aeroméxico	Aeroméxico	k6eAd1	Aeroméxico
<g/>
,	,	kIx,	,
Air	Air	k1gMnSc4	Air
France	Franc	k1gMnSc4	Franc
<g/>
,	,	kIx,	,
Delta	delta	k1gFnSc1	delta
Air	Air	k1gMnSc1	Air
Lines	Lines	k1gMnSc1	Lines
a	a	k8xC	a
Korean	Korean	k1gMnSc1	Korean
Air	Air	k1gFnSc2	Air
vznik	vznik	k1gInSc1	vznik
aliance	aliance	k1gFnSc2	aliance
leteckých	letecký	k2eAgMnPc2d1	letecký
dopravců	dopravce	k1gMnPc2	dopravce
SkyTeam	SkyTeam	k1gInSc4	SkyTeam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
září	září	k1gNnSc4	září
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
největší	veliký	k2eAgFnSc1d3	veliký
aliance	aliance	k1gFnSc1	aliance
nákladních	nákladní	k2eAgMnPc2d1	nákladní
leteckých	letecký	k2eAgMnPc2d1	letecký
dopravců	dopravce	k1gMnPc2	dopravce
-	-	kIx~	-
SkyTeam	SkyTeam	k1gInSc1	SkyTeam
Cargo	Cargo	k1gNnSc1	Cargo
<g/>
.	.	kIx.	.
</s>
<s>
Klíčová	klíčový	k2eAgNnPc1d1	klíčové
data	datum	k1gNnPc1	datum
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
březen	březen	k1gInSc1	březen
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
České	český	k2eAgFnSc2d1	Česká
aerolinie	aerolinie	k1gFnSc2	aerolinie
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
členem	člen	k1gInSc7	člen
aliance	aliance	k1gFnSc2	aliance
červenec	červenec	k1gInSc1	červenec
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
Alitalia	Alitalia	k1gFnSc1	Alitalia
se	se	k3xPyFc4	se
připojuje	připojovat	k5eAaImIp3nS	připojovat
k	k	k7c3	k
alianci	aliance	k1gFnSc3	aliance
červenec	červenec	k1gInSc1	červenec
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
SkyTeam	SkyTeam	k1gInSc1	SkyTeam
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
první	první	k4xOgFnSc7	první
globální	globální	k2eAgFnSc7d1	globální
leteckou	letecký	k2eAgFnSc7d1	letecká
aliancí	aliance	k1gFnSc7	aliance
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
členy	člen	k1gMnPc4	člen
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
kód	kód	k1gInSc4	kód
ATI	ATI	kA	ATI
pro	pro	k7c4	pro
trasy	trasa	k1gFnPc4	trasa
nad	nad	k7c7	nad
dvěma	dva	k4xCgFnPc7	dva
<g />
.	.	kIx.	.
</s>
<s>
oceány	oceán	k1gInPc1	oceán
-	-	kIx~	-
Atlantikem	Atlantik	k1gInSc7	Atlantik
a	a	k8xC	a
Pacifikem	Pacifik	k1gInSc7	Pacifik
srpen	srpen	k1gInSc4	srpen
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
SkyTeam	SkyTeam	k1gInSc1	SkyTeam
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
tarif	tarif	k1gInSc1	tarif
Europe	Europ	k1gInSc5	Europ
Pass	Pass	k1gInSc4	Pass
leden	leden	k1gInSc1	leden
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
SkyTeam	SkyTeam	k1gInSc1	SkyTeam
oznámil	oznámit	k5eAaPmAgInS	oznámit
svůj	svůj	k3xOyFgInSc4	svůj
plán	plán	k1gInSc4	plán
na	na	k7c6	na
zahájení	zahájení	k1gNnSc6	zahájení
Přidružených	přidružený	k2eAgInPc2d1	přidružený
programů	program	k1gInPc2	program
červenec	červenec	k1gInSc1	červenec
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
SkyTeam	SkyTeam	k1gInSc1	SkyTeam
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
tarif	tarif	k1gInSc4	tarif
America	Americ	k1gInSc2	Americ
Pass	Pass	k1gInSc4	Pass
září	září	k1gNnSc2	září
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
Continental	Continental	k1gMnSc1	Continental
Airlines	Airlines	k1gMnSc1	Airlines
<g/>
,	,	kIx,	,
KLM	KLM	kA	KLM
Royal	Royal	k1gMnSc1	Royal
Dutch	Dutch	k1gMnSc1	Dutch
Airlines	Airlines	k1gMnSc1	Airlines
a	a	k8xC	a
Northwest	Northwest	k1gMnSc1	Northwest
Airlines	Airlines	k1gMnSc1	Airlines
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
připojují	připojovat	k5eAaImIp3nP	připojovat
k	k	k7c3	k
alianci	aliance	k1gFnSc3	aliance
říjen	říjen	k1gInSc1	říjen
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
SkyTeam	SkyTeam	k1gInSc1	SkyTeam
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
tarif	tarif	k1gInSc4	tarif
Asia	Asi	k1gInSc2	Asi
Pass	Pass	k1gInSc4	Pass
duben	duben	k1gInSc1	duben
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Aeroflot	aeroflot	k1gInSc1	aeroflot
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
členem	člen	k1gInSc7	člen
aliance	aliance	k1gFnSc2	aliance
duben	duben	k1gInSc1	duben
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
SkyTeam	SkyTeam	k1gInSc1	SkyTeam
oznámil	oznámit	k5eAaPmAgInS	oznámit
zahájení	zahájení	k1gNnSc4	zahájení
SkyTeam	SkyTeam	k1gInSc4	SkyTeam
Global	globat	k5eAaImAgInS	globat
Meetings	Meetings	k1gInSc1	Meetings
červen	červen	k1gInSc1	červen
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
podepsána	podepsán	k2eAgFnSc1d1	podepsána
dohoda	dohoda	k1gFnSc1	dohoda
s	s	k7c7	s
Britskou	britský	k2eAgFnSc7d1	britská
správou	správa	k1gFnSc7	správa
letišť	letiště	k1gNnPc2	letiště
(	(	kIx(	(
<g/>
BAA	BAA	kA	BAA
<g/>
)	)	kIx)	)
o	o	k7c4	o
přidružení	přidružení	k1gNnSc4	přidružení
členských	členský	k2eAgFnPc2d1	členská
<g />
.	.	kIx.	.
</s>
<s>
aerolinií	aerolinie	k1gFnSc7	aerolinie
do	do	k7c2	do
terminálu	terminál	k1gInSc2	terminál
T4	T4	k1gFnSc2	T4
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
Heathrow	Heathrow	k1gFnSc2	Heathrow
v	v	k7c6	v
londýně	londýno	k1gNnSc6	londýno
říjen	říjen	k1gInSc1	říjen
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
čtyři	čtyři	k4xCgMnPc1	čtyři
členové	člen	k1gMnPc1	člen
SkyTeamu	SkyTeam	k1gInSc2	SkyTeam
<g/>
,	,	kIx,	,
provozující	provozující	k2eAgInPc4d1	provozující
pravidelné	pravidelný	k2eAgInPc4d1	pravidelný
lety	let	k1gInPc4	let
do	do	k7c2	do
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
oznámily	oznámit	k5eAaPmAgInP	oznámit
vytvoření	vytvoření	k1gNnSc4	vytvoření
společné	společný	k2eAgFnSc2d1	společná
operační	operační	k2eAgFnSc2d1	operační
struktury	struktura	k1gFnSc2	struktura
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
Guarulhos	Guarulhosa	k1gFnPc2	Guarulhosa
v	v	k7c6	v
Sã	Sã	k1gFnSc6	Sã
Paulo	Paula	k1gFnSc5	Paula
červen	červen	k1gInSc1	červen
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
Leo	Leo	k1gMnSc1	Leo
van	vana	k1gFnPc2	vana
Wijk	Wijk	k1gMnSc1	Wijk
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
prvním	první	k4xOgMnSc6	první
předsedou	předseda	k1gMnSc7	předseda
SkyTeamu	SkyTeam	k1gInSc2	SkyTeam
září	zářit	k5eAaImIp3nS	zářit
<g />
.	.	kIx.	.
</s>
<s>
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
SkyTeam	SkyTeam	k1gInSc1	SkyTeam
oznámil	oznámit	k5eAaPmAgInS	oznámit
první	první	k4xOgInPc4	první
tři	tři	k4xCgInPc4	tři
členy	člen	k1gInPc4	člen
svého	svůj	k3xOyFgInSc2	svůj
přidruženého	přidružený	k2eAgInSc2d1	přidružený
programu	program	k1gInSc2	program
-	-	kIx~	-
Air	Air	k1gFnSc1	Air
Europa	Europa	k1gFnSc1	Europa
<g/>
,	,	kIx,	,
Copa	Copa	k1gMnSc1	Copa
Airlines	Airlines	k1gMnSc1	Airlines
a	a	k8xC	a
Kenya	Kenya	k1gFnSc1	Kenya
Airways	Airways	k1gInSc4	Airways
listopad	listopad	k1gInSc1	listopad
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
China	China	k1gFnSc1	China
Southern	Southern	k1gMnSc1	Southern
Airlines	Airlines	k1gMnSc1	Airlines
se	se	k3xPyFc4	se
připojuje	připojovat	k5eAaImIp3nS	připojovat
k	k	k7c3	k
alianci	aliance	k1gFnSc3	aliance
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
SkyTeam	SkyTeam	k1gInSc1	SkyTeam
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
plány	plán	k1gInPc4	plán
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
společné	společný	k2eAgFnSc2d1	společná
operační	operační	k2eAgFnSc2d1	operační
struktury	struktura	k1gFnSc2	struktura
v	v	k7c6	v
terminálu	terminál	k1gInSc6	terminál
T4	T4	k1gFnSc2	T4
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
letišti	letiště	k1gNnSc6	letiště
Londýn-Heathrow	Londýn-Heathrow	k1gFnSc2	Londýn-Heathrow
září	září	k1gNnSc2	září
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
SkyTeam	SkyTeam	k1gInSc1	SkyTeam
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
tarif	tarif	k1gInSc4	tarif
China	China	k1gFnSc1	China
Pass	Pass	k1gInSc1	Pass
duben	duben	k1gInSc1	duben
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
začínají	začínat	k5eAaImIp3nP	začínat
se	se	k3xPyFc4	se
používat	používat	k5eAaImF	používat
speciální	speciální	k2eAgFnPc4d1	speciální
uniformy	uniforma	k1gFnPc4	uniforma
a	a	k8xC	a
nátěry	nátěr	k1gInPc4	nátěr
letadel	letadlo	k1gNnPc2	letadlo
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
SkyTeamu	SkyTeam	k1gInSc2	SkyTeam
září	zářit	k5eAaImIp3nS	zářit
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
SkyTeam	SkyTeam	k1gInSc1	SkyTeam
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
tarif	tarif	k1gInSc1	tarif
Mexiko	Mexiko	k1gNnSc1	Mexiko
Pass	Pass	k1gInSc1	Pass
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Continental	Continental	k1gMnSc1	Continental
Airlines	Airlines	k1gMnSc1	Airlines
a	a	k8xC	a
Copa	Copa	k1gMnSc1	Copa
Airlines	Airlines	k1gMnSc1	Airlines
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
China	China	k1gFnSc1	China
Eastern	Eastern	k1gMnSc1	Eastern
Airlines	Airlines	k1gMnSc1	Airlines
oznámily	oznámit	k5eAaPmAgFnP	oznámit
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
připojit	připojit	k5eAaPmF	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
alianci	aliance	k1gFnSc3	aliance
SkyTeam	SkyTeam	k1gInSc4	SkyTeam
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2011	[number]	k4	2011
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Vietnam	Vietnam	k1gInSc1	Vietnam
Airlines	Airlines	k1gInSc1	Airlines
členem	člen	k1gInSc7	člen
aliance	aliance	k1gFnSc2	aliance
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
SkyTeam	SkyTeam	k1gInSc1	SkyTeam
obnovuje	obnovovat	k5eAaImIp3nS	obnovovat
své	svůj	k3xOyFgNnSc4	svůj
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
programu	program	k1gInSc6	program
s	s	k7c7	s
okamžitou	okamžitý	k2eAgFnSc7d1	okamžitá
platností	platnost	k1gFnSc7	platnost
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
plnohodnotné	plnohodnotný	k2eAgNnSc4d1	plnohodnotné
i	i	k8xC	i
přidlužené	přidlužený	k2eAgNnSc4d1	přidlužený
<g />
.	.	kIx.	.
</s>
<s>
členy	člen	k1gInPc4	člen
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
:	:	kIx,	:
TAROM	TAROM	kA	TAROM
členem	člen	k1gMnSc7	člen
aliance	aliance	k1gFnSc2	aliance
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
China	China	k1gFnSc1	China
Airlines	Airlinesa	k1gFnPc2	Airlinesa
oficiálně	oficiálně	k6eAd1	oficiálně
oznámily	oznámit	k5eAaPmAgFnP	oznámit
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
připojit	připojit	k5eAaPmF	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
alianci	aliance	k1gFnSc3	aliance
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
oficiální	oficiální	k2eAgNnPc1d1	oficiální
oznámení	oznámení	k1gNnPc1	oznámení
o	o	k7c4	o
začlenění	začlenění	k1gNnSc4	začlenění
Shanghai	Shangha	k1gFnSc2	Shangha
Airlines	Airlinesa	k1gFnPc2	Airlinesa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
jako	jako	k8xC	jako
partner	partner	k1gMnSc1	partner
China	China	k1gFnSc1	China
<g />
.	.	kIx.	.
</s>
<s>
Eastern	Eastern	k1gInSc1	Eastern
Airlines	Airlines	k1gInSc1	Airlines
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
oficiální	oficiální	k2eAgNnPc1d1	oficiální
oznámení	oznámení	k1gNnPc1	oznámení
o	o	k7c4	o
začlenění	začlenění	k1gNnSc4	začlenění
Garuda	Garudo	k1gNnSc2	Garudo
Indonesia	Indonesius	k1gMnSc2	Indonesius
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
oficiální	oficiální	k2eAgNnPc1d1	oficiální
oznámení	oznámení	k1gNnPc1	oznámení
o	o	k7c4	o
začlenění	začlenění	k1gNnSc4	začlenění
Aerolineas	Aerolineas	k1gMnSc1	Aerolineas
Argentinas	Argentinas	k1gMnSc1	Argentinas
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
oficiální	oficiální	k2eAgNnPc1d1	oficiální
oznámení	oznámení	k1gNnPc1	oznámení
o	o	k7c4	o
začlenění	začlenění	k1gNnSc4	začlenění
Saudia	Saudium	k1gNnSc2	Saudium
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
19	[number]	k4	19
<g/>
<g />
.	.	kIx.	.
</s>
<s>
ledna	ledno	k1gNnPc1wB	ledno
<g/>
:	:	kIx,	:
oficiální	oficiální	k2eAgNnPc1d1	oficiální
oznámení	oznámení	k1gNnPc1	oznámení
o	o	k7c4	o
začlenění	začlenění	k1gNnSc4	začlenění
Middle	Middle	k1gFnSc2	Middle
East	East	k1gMnSc1	East
Airlines	Airlines	k1gMnSc1	Airlines
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
Stav	stav	k1gInSc1	stav
ze	z	k7c2	z
září	září	k1gNnSc2	září
2016	[number]	k4	2016
Aerolitoral	Aerolitoral	k1gFnPc2	Aerolitoral
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
-	-	kIx~	-
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Alitalia	Alitalia	k1gFnSc1	Alitalia
L.A.I.	L.A.I.	k1gMnSc1	L.A.I.
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
-	-	kIx~	-
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Continental	Continental	k1gMnSc1	Continental
Airlines	Airlines	k1gMnSc1	Airlines
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
-	-	kIx~	-
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Northwest	Northwest	k1gMnSc1	Northwest
Airlines	Airlines	k1gMnSc1	Airlines
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
VLM	VLM	kA	VLM
Airlines	Airlines	k1gMnSc1	Airlines
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
-	-	kIx~	-
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Song	song	k1gInSc1	song
Airlines	Airlines	k1gInSc1	Airlines
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
-	-	kIx~	-
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Delta	delta	k1gFnSc1	delta
Express	express	k1gInSc1	express
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
-	-	kIx~	-
2001	[number]	k4	2001
Copa	Copa	k1gMnSc1	Copa
Airlines	Airlines	k1gMnSc1	Airlines
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
-	-	kIx~	-
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
MALÉV	MALÉV	kA	MALÉV
Hungarian	Hungarian	k1gMnSc1	Hungarian
Airlines	Airlines	k1gMnSc1	Airlines
(	(	kIx(	(
<g/>
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
přestoupit	přestoupit	k5eAaPmF	přestoupit
k	k	k7c3	k
Oneworldu	Oneworld	k1gInSc3	Oneworld
<g/>
)	)	kIx)	)
Aeroflot	aeroflot	k1gInSc1	aeroflot
<g />
.	.	kIx.	.
</s>
<s>
Cargo	Cargo	k1gNnSc1	Cargo
(	(	kIx(	(
<g/>
od	od	k7c2	od
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Aéromexico	Aéromexico	k6eAd1	Aéromexico
Cargo	Cargo	k6eAd1	Cargo
(	(	kIx(	(
<g/>
od	od	k7c2	od
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Air	Air	k1gFnSc2	Air
France	Franc	k1gMnSc4	Franc
Cargo	Cargo	k1gMnSc1	Cargo
(	(	kIx(	(
<g/>
od	od	k7c2	od
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Alitalia	Alitalia	k1gFnSc1	Alitalia
Cargo	Cargo	k1gMnSc1	Cargo
(	(	kIx(	(
<g/>
od	od	k7c2	od
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
China	China	k1gFnSc1	China
Southern	Southern	k1gMnSc1	Southern
Cargo	Cargo	k1gMnSc1	Cargo
(	(	kIx(	(
<g/>
od	od	k7c2	od
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Czech	Czech	k1gMnSc1	Czech
Airlines	Airlines	k1gMnSc1	Airlines
Cargo	Cargo	k1gMnSc1	Cargo
(	(	kIx(	(
<g/>
od	od	k7c2	od
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Delta	delta	k1gFnSc1	delta
Cargo	Cargo	k6eAd1	Cargo
(	(	kIx(	(
<g/>
od	od	k7c2	od
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
NWA	NWA	kA	NWA
Cargo	Cargo	k6eAd1	Cargo
od	od	k7c2	od
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
KLM	KLM	kA	KLM
Cargo	Cargo	k6eAd1	Cargo
(	(	kIx(	(
<g/>
od	od	k7c2	od
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Korean	Korean	k1gMnSc1	Korean
Air	Air	k1gMnSc1	Air
Cargo	Cargo	k1gMnSc1	Cargo
(	(	kIx(	(
<g/>
od	od	k7c2	od
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgFnSc2	jaký
úrovně	úroveň	k1gFnSc2	úroveň
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
věrnostní	věrnostní	k2eAgInPc1d1	věrnostní
programy	program	k1gInPc1	program
leteckých	letecký	k2eAgFnPc2d1	letecká
společností	společnost	k1gFnPc2	společnost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
SkyTeamu	SkyTeam	k1gInSc2	SkyTeam
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
mají	mít	k5eAaImIp3nP	mít
členské	členský	k2eAgFnSc2d1	členská
společnosti	společnost	k1gFnSc2	společnost
alespoň	alespoň	k9	alespoň
jedno	jeden	k4xCgNnSc1	jeden
letadlo	letadlo	k1gNnSc1	letadlo
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
SkyTeamu	SkyTeam	k1gInSc2	SkyTeam
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
SkyTeam	SkyTeam	k1gInSc4	SkyTeam
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
</s>
