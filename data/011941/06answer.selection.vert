<s>
Cavendishové	Cavendish	k1gMnPc1	Cavendish
jsou	být	k5eAaImIp3nP	být
starý	starý	k2eAgInSc4d1	starý
anglický	anglický	k2eAgInSc4d1	anglický
šlechtický	šlechtický	k2eAgInSc4d1	šlechtický
rod	rod	k1gInSc4	rod
připomínaný	připomínaný	k2eAgInSc4d1	připomínaný
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
normanského	normanský	k2eAgInSc2d1	normanský
vpádu	vpád	k1gInSc2	vpád
<g/>
,	,	kIx,	,
v	v	k7c6	v
linii	linie	k1gFnSc6	linie
vévodů	vévoda	k1gMnPc2	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
patří	patřit	k5eAaImIp3nS	patřit
dodnes	dodnes	k6eAd1	dodnes
k	k	k7c3	k
nejbohatším	bohatý	k2eAgInPc3d3	nejbohatší
aristokratickým	aristokratický	k2eAgInPc3d1	aristokratický
klanům	klan	k1gInPc3	klan
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
