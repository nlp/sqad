<p>
<s>
Cavendishové	Cavendish	k1gMnPc1	Cavendish
jsou	být	k5eAaImIp3nP	být
starý	starý	k2eAgInSc4d1	starý
anglický	anglický	k2eAgInSc4d1	anglický
šlechtický	šlechtický	k2eAgInSc4d1	šlechtický
rod	rod	k1gInSc4	rod
připomínaný	připomínaný	k2eAgInSc4d1	připomínaný
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
normanského	normanský	k2eAgInSc2d1	normanský
vpádu	vpád	k1gInSc2	vpád
<g/>
,	,	kIx,	,
v	v	k7c6	v
linii	linie	k1gFnSc6	linie
vévodů	vévoda	k1gMnPc2	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
patří	patřit	k5eAaImIp3nS	patřit
dodnes	dodnes	k6eAd1	dodnes
k	k	k7c3	k
nejbohatším	bohatý	k2eAgInPc3d3	nejbohatší
aristokratickým	aristokratický	k2eAgInPc3d1	aristokratický
klanům	klan	k1gInPc3	klan
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
obdržela	obdržet	k5eAaPmAgFnS	obdržet
jedna	jeden	k4xCgFnSc1	jeden
linie	linie	k1gFnSc1	linie
za	za	k7c4	za
podporu	podpora	k1gFnSc4	podpora
Stuartovců	Stuartovec	k1gMnPc2	Stuartovec
titul	titul	k1gInSc4	titul
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Newcastle	Newcastle	k1gFnSc2	Newcastle
(	(	kIx(	(
<g/>
1664	[number]	k4	1664
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
linie	linie	k1gFnSc1	linie
naopak	naopak	k6eAd1	naopak
profitovala	profitovat	k5eAaBmAgFnS	profitovat
na	na	k7c6	na
pádu	pád	k1gInSc6	pád
Stuartovců	Stuartovec	k1gMnPc2	Stuartovec
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
titul	titul	k1gInSc4	titul
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
(	(	kIx(	(
<g/>
1694	[number]	k4	1694
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
až	až	k9	až
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
řada	řada	k1gFnSc1	řada
členů	člen	k1gMnPc2	člen
rodu	rod	k1gInSc2	rod
zastávala	zastávat	k5eAaImAgFnS	zastávat
vysoké	vysoký	k2eAgInPc4d1	vysoký
úřady	úřad	k1gInPc4	úřad
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
správě	správa	k1gFnSc6	správa
a	a	k8xC	a
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k1gInSc1	William
Cavendish	Cavendisha	k1gFnPc2	Cavendisha
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
(	(	kIx(	(
<g/>
1720	[number]	k4	1720
<g/>
–	–	k?	–
<g/>
1764	[number]	k4	1764
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
britským	britský	k2eAgMnSc7d1	britský
premiérem	premiér	k1gMnSc7	premiér
a	a	k8xC	a
sňatkem	sňatek	k1gInSc7	sňatek
s	s	k7c7	s
dědičkou	dědička	k1gFnSc7	dědička
rodu	rod	k1gInSc2	rod
Boyle	Boyle	k1gInSc1	Boyle
významně	významně	k6eAd1	významně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
majetek	majetek	k1gInSc4	majetek
rodu	rod	k1gInSc2	rod
o	o	k7c4	o
statky	statek	k1gInPc4	statek
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dva	dva	k4xCgMnPc1	dva
vévodové	vévoda	k1gMnPc1	vévoda
z	z	k7c2	z
Newcastle	Newcastle	k1gFnPc2	Newcastle
a	a	k8xC	a
jedenáct	jedenáct	k4xCc1	jedenáct
vévodů	vévoda	k1gMnPc2	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
získalo	získat	k5eAaPmAgNnS	získat
Podvazkový	podvazkový	k2eAgInSc4d1	podvazkový
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
linii	linie	k1gFnSc6	linie
vévodů	vévoda	k1gMnPc2	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
patřili	patřit	k5eAaImAgMnP	patřit
Cavendishové	Cavendishová	k1gFnPc4	Cavendishová
přes	přes	k7c4	přes
dvě	dva	k4xCgFnPc4	dva
století	století	k1gNnPc1	století
k	k	k7c3	k
příznivcům	příznivec	k1gMnPc3	příznivec
whigů	whig	k1gMnPc2	whig
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
liberálů	liberál	k1gMnPc2	liberál
<g/>
.	.	kIx.	.
</s>
<s>
Spencer	Spencer	k1gMnSc1	Spencer
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshiru	Devonshir	k1gInSc2	Devonshir
(	(	kIx(	(
<g/>
1833	[number]	k4	1833
<g/>
–	–	k?	–
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
liberálním	liberální	k2eAgMnPc3d1	liberální
politikům	politik	k1gMnPc3	politik
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
stál	stát	k5eAaImAgInS	stát
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
strany	strana	k1gFnSc2	strana
liberálních	liberální	k2eAgMnPc2d1	liberální
unionistů	unionista	k1gMnPc2	unionista
a	a	k8xC	a
přechodu	přechod	k1gInSc2	přechod
Cavendishů	Cavendish	k1gInPc2	Cavendish
ke	k	k7c3	k
konzervativcům	konzervativec	k1gMnPc3	konzervativec
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
významným	významný	k2eAgMnSc7d1	významný
zástupcem	zástupce	k1gMnSc7	zástupce
rodu	rod	k1gInSc2	rod
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
byl	být	k5eAaImAgMnS	být
Andrew	Andrew	k1gMnSc1	Andrew
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1962-1964	[number]	k4	1962-1964
ministrem	ministr	k1gMnSc7	ministr
pro	pro	k7c4	pro
záležitosti	záležitost	k1gFnPc4	záležitost
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
reprezentantem	reprezentant	k1gMnSc7	reprezentant
rodu	rod	k1gInSc2	rod
je	být	k5eAaImIp3nS	být
Peregrine	Peregrin	k1gInSc5	Peregrin
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hodnota	hodnota	k1gFnSc1	hodnota
jeho	on	k3xPp3gInSc2	on
majetku	majetek	k1gInSc2	majetek
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
880	[number]	k4	880
miliónů	milión	k4xCgInPc2	milión
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
představiteli	představitel	k1gMnPc7	představitel
rodu	rod	k1gInSc2	rod
jsou	být	k5eAaImIp3nP	být
Charles	Charles	k1gMnSc1	Charles
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
baron	baron	k1gMnSc1	baron
Chesham	Chesham	k1gInSc1	Chesham
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Roderick	Roderick	k1gMnSc1	Roderick
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
baron	baron	k1gMnSc1	baron
Waterpark	Waterpark	k1gInSc1	Waterpark
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
a	a	k8xC	a
doživotní	doživotní	k2eAgMnSc1d1	doživotní
člen	člen	k1gMnSc1	člen
Sněmovny	sněmovna	k1gFnSc2	sněmovna
lordů	lord	k1gMnPc2	lord
Richard	Richard	k1gMnSc1	Richard
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
baron	baron	k1gMnSc1	baron
Cavendish	Cavendish	k1gMnSc1	Cavendish
of	of	k?	of
Furness	Furness	k1gInSc1	Furness
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Majetkem	majetek	k1gInSc7	majetek
Cavendishů	Cavendish	k1gInPc2	Cavendish
především	především	k9	především
v	v	k7c6	v
linii	linie	k1gFnSc6	linie
vévodů	vévoda	k1gMnPc2	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
architektonicky	architektonicky	k6eAd1	architektonicky
hodnotných	hodnotný	k2eAgInPc2d1	hodnotný
hradů	hrad	k1gInPc2	hrad
a	a	k8xC	a
zámků	zámek	k1gInPc2	zámek
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
rodovým	rodový	k2eAgNnSc7d1	rodové
sídlem	sídlo	k1gNnSc7	sídlo
je	být	k5eAaImIp3nS	být
Chatsworth	Chatsworth	k1gInSc1	Chatsworth
House	house	k1gNnSc1	house
(	(	kIx(	(
<g/>
Derbyshire	Derbyshir	k1gMnSc5	Derbyshir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Vzácné	vzácný	k2eAgFnPc1d1	vzácná
umělecké	umělecký	k2eAgFnPc1d1	umělecká
sbírky	sbírka	k1gFnPc1	sbírka
soustředěné	soustředěný	k2eAgFnPc1d1	soustředěná
v	v	k7c6	v
interiérech	interiér	k1gInPc6	interiér
zámku	zámek	k1gInSc2	zámek
tvoří	tvořit	k5eAaImIp3nP	tvořit
významnou	významný	k2eAgFnSc4d1	významná
položku	položka	k1gFnSc4	položka
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
hodnotě	hodnota	k1gFnSc6	hodnota
majetku	majetek	k1gInSc2	majetek
vévodů	vévoda	k1gMnPc2	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gMnSc5	Devonshir
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
známým	známý	k2eAgInSc7d1	známý
předkem	předek	k1gInSc7	předek
rodu	rod	k1gInSc2	rod
je	být	k5eAaImIp3nS	být
Robert	Robert	k1gMnSc1	Robert
de	de	k?	de
Gernon	Gernon	k1gMnSc1	Gernon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
za	za	k7c4	za
Jindřicha	Jindřich	k1gMnSc4	Jindřich
I.	I.	kA	I.
získal	získat	k5eAaPmAgMnS	získat
statky	statek	k1gInPc7	statek
v	v	k7c6	v
Gloucesteru	Gloucester	k1gInSc6	Gloucester
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Robert	Robert	k1gMnSc1	Robert
získal	získat	k5eAaPmAgMnS	získat
sňatkem	sňatek	k1gInSc7	sňatek
vesnici	vesnice	k1gFnSc4	vesnice
Cavendish	Cavendish	k1gInSc4	Cavendish
v	v	k7c6	v
Suffolku	Suffolek	k1gInSc6	Suffolek
a	a	k8xC	a
toto	tento	k3xDgNnSc4	tento
jméno	jméno	k1gNnSc4	jméno
pak	pak	k6eAd1	pak
přijali	přijmout	k5eAaPmAgMnP	přijmout
potomci	potomek	k1gMnPc1	potomek
jako	jako	k8xS	jako
rodové	rodový	k2eAgFnPc1d1	rodová
<g/>
.	.	kIx.	.
</s>
<s>
Sir	sir	k1gMnSc1	sir
John	John	k1gMnSc1	John
Cavendish	Cavendish	k1gMnSc1	Cavendish
(	(	kIx(	(
<g/>
1346	[number]	k4	1346
<g/>
–	–	k?	–
<g/>
1381	[number]	k4	1381
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1372-1381	[number]	k4	1372-1381
lordem	lord	k1gMnSc7	lord
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
sudím	sudí	k1gMnSc7	sudí
<g/>
,	,	kIx,	,
potomci	potomek	k1gMnPc1	potomek
pak	pak	k6eAd1	pak
zastávali	zastávat	k5eAaImAgMnP	zastávat
správní	správní	k2eAgFnPc4d1	správní
funkce	funkce	k1gFnPc4	funkce
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
hrabství	hrabství	k1gNnSc2	hrabství
Suffolk	Suffolka	k1gFnPc2	Suffolka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žili	žít	k5eAaImAgMnP	žít
po	po	k7c4	po
několik	několik	k4yIc4	několik
generací	generace	k1gFnPc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
vlivnější	vlivný	k2eAgFnSc4d2	vlivnější
a	a	k8xC	a
bohatou	bohatý	k2eAgFnSc4d1	bohatá
šlechtu	šlechta	k1gFnSc4	šlechta
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
až	až	k9	až
díky	díky	k7c3	díky
Siru	sir	k1gMnSc3	sir
Williamu	William	k1gInSc2	William
Cavendishovi	Cavendish	k1gMnSc3	Cavendish
(	(	kIx(	(
<g/>
1505	[number]	k4	1505
<g/>
–	–	k?	–
<g/>
1557	[number]	k4	1557
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
obohatil	obohatit	k5eAaPmAgMnS	obohatit
při	při	k7c6	při
rozprodeji	rozprodej	k1gInSc6	rozprodej
majetku	majetek	k1gInSc2	majetek
zrušených	zrušený	k2eAgInPc2d1	zrušený
klášterů	klášter	k1gInPc2	klášter
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
potomci	potomek	k1gMnPc1	potomek
pak	pak	k6eAd1	pak
získali	získat	k5eAaPmAgMnP	získat
nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
šlechtické	šlechtický	k2eAgInPc4d1	šlechtický
tituly	titul	k1gInPc4	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vévodové	vévoda	k1gMnPc1	vévoda
z	z	k7c2	z
Newcastle	Newcastle	k1gFnSc2	Newcastle
(	(	kIx(	(
<g/>
1664	[number]	k4	1664
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Vévodové	vévoda	k1gMnPc1	vévoda
z	z	k7c2	z
Newcastle	Newcastle	k1gFnSc2	Newcastle
byli	být	k5eAaImAgMnP	být
vedlejší	vedlejší	k2eAgFnSc7d1	vedlejší
linií	linie	k1gFnSc7	linie
hrabat	hrabat	k5eAaImF	hrabat
z	z	k7c2	z
Devonshire	Devonshir	k1gMnSc5	Devonshir
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
této	tento	k3xDgFnSc2	tento
větve	větev	k1gFnSc2	větev
byl	být	k5eAaImAgInS	být
William	William	k1gInSc1	William
Cavendish	Cavendish	k1gInSc1	Cavendish
(	(	kIx(	(
<g/>
1592	[number]	k4	1592
<g/>
–	–	k?	–
<g/>
1676	[number]	k4	1676
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
synovec	synovec	k1gMnSc1	synovec
1	[number]	k4	1
<g/>
.	.	kIx.	.
hraběte	hrabě	k1gMnSc4	hrabě
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
po	po	k7c6	po
rodičích	rodič	k1gMnPc6	rodič
zdědil	zdědit	k5eAaPmAgInS	zdědit
značný	značný	k2eAgInSc4d1	značný
majetek	majetek	k1gInSc4	majetek
v	v	k7c6	v
několika	několik	k4yIc6	několik
hrabstvích	hrabství	k1gNnPc6	hrabství
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1620	[number]	k4	1620
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
vikomta	vikomt	k1gMnSc4	vikomt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
stoupenec	stoupenec	k1gMnSc1	stoupenec
Stuartovců	Stuartovec	k1gMnPc2	Stuartovec
získal	získat	k5eAaPmAgMnS	získat
nakonec	nakonec	k6eAd1	nakonec
titul	titul	k1gInSc4	titul
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Newcastle	Newcastle	k1gFnSc2	Newcastle
(	(	kIx(	(
<g/>
1664	[number]	k4	1664
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oporou	opora	k1gFnSc7	opora
stuartovského	stuartovský	k2eAgInSc2d1	stuartovský
režimu	režim	k1gInSc2	režim
byl	být	k5eAaImAgInS	být
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Henry	Henry	k1gMnSc1	Henry
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Newcastle	Newcastle	k1gFnSc2	Newcastle
(	(	kIx(	(
<g/>
1630	[number]	k4	1630
<g/>
–	–	k?	–
<g/>
1691	[number]	k4	1691
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
však	však	k9	však
zemřel	zemřít	k5eAaPmAgMnS	zemřít
bez	bez	k7c2	bez
potomstva	potomstvo	k1gNnSc2	potomstvo
a	a	k8xC	a
titul	titul	k1gInSc1	titul
tak	tak	k6eAd1	tak
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gFnPc7	jeho
dcerami	dcera	k1gFnPc7	dcera
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
soudnímu	soudní	k2eAgInSc3d1	soudní
sporu	spor	k1gInSc3	spor
o	o	k7c4	o
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
,	,	kIx,	,
titul	titul	k1gInSc4	titul
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Newcastle	Newcastle	k1gFnSc2	Newcastle
byl	být	k5eAaImAgInS	být
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
obnoven	obnovit	k5eAaPmNgInS	obnovit
pro	pro	k7c4	pro
spřízněný	spřízněný	k2eAgInSc4d1	spřízněný
rod	rod	k1gInSc4	rod
Hollesů	Holles	k1gInPc2	Holles
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
titul	titul	k1gInSc1	titul
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Newcastle	Newcastle	k1gFnSc2	Newcastle
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
rodina	rodina	k1gFnSc1	rodina
Pelhamů	pelham	k1gInPc2	pelham
<g/>
,	,	kIx,	,
stěžejní	stěžejní	k2eAgFnSc1d1	stěžejní
část	část	k1gFnSc1	část
dědictví	dědictví	k1gNnSc2	dědictví
přešla	přejít	k5eAaPmAgFnS	přejít
na	na	k7c4	na
spřízněný	spřízněný	k2eAgInSc4d1	spřízněný
rod	rod	k1gInSc4	rod
vévodů	vévoda	k1gMnPc2	vévoda
z	z	k7c2	z
Portlandu	Portland	k1gInSc2	Portland
<g/>
,	,	kIx,	,
ktetý	ktetý	k2eAgMnSc1d1	ktetý
dodnes	dodnes	k6eAd1	dodnes
užívá	užívat	k5eAaImIp3nS	užívat
příjmení	příjmení	k1gNnSc4	příjmení
Cavendish-Bentinck	Cavendish-Bentincka	k1gFnPc2	Cavendish-Bentincka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Majetkem	majetek	k1gInSc7	majetek
vévodů	vévoda	k1gMnPc2	vévoda
z	z	k7c2	z
Newcastle	Newcastle	k1gFnSc2	Newcastle
byla	být	k5eAaImAgFnS	být
honosná	honosný	k2eAgFnSc1d1	honosná
sídla	sídlo	k1gNnSc2	sídlo
Welbeck	Welbeck	k1gInSc4	Welbeck
Abbey	Abbea	k1gFnSc2	Abbea
(	(	kIx(	(
<g/>
Nottinghamshire	Nottinghamshir	k1gInSc5	Nottinghamshir
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bolsover	Bolsover	k1gMnSc1	Bolsover
Castle	Castle	k1gFnSc2	Castle
(	(	kIx(	(
<g/>
Derbyshire	Derbyshir	k1gMnSc5	Derbyshir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
1	[number]	k4	1
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
s	s	k7c7	s
vynaložením	vynaložení	k1gNnSc7	vynaložení
vysokých	vysoký	k2eAgInPc2d1	vysoký
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
několikrát	několikrát	k6eAd1	několikrát
hostil	hostit	k5eAaImAgInS	hostit
Jakuba	Jakub	k1gMnSc2	Jakub
I.	I.	kA	I.
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
I.	I.	kA	I.
Oba	dva	k4xCgInPc4	dva
zámky	zámek	k1gInPc4	zámek
zdědili	zdědit	k5eAaPmAgMnP	zdědit
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
vévodové	vévoda	k1gMnPc1	vévoda
z	z	k7c2	z
Portlandu	Portland	k1gInSc2	Portland
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
rod	rod	k1gInSc1	rod
užívá	užívat	k5eAaImIp3nS	užívat
příjmení	příjmení	k1gNnSc4	příjmení
Cavendish-Bentinck	Cavendish-Bentincka	k1gFnPc2	Cavendish-Bentincka
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
majetku	majetek	k1gInSc6	majetek
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
Welbeck	Welbeck	k1gInSc1	Welbeck
Abbey	Abbea	k1gFnSc2	Abbea
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Bolsover	Bolsover	k1gInSc1	Bolsover
Castle	Castle	k1gFnSc2	Castle
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
převeden	převést	k5eAaPmNgInS	převést
do	do	k7c2	do
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vévodové	vévoda	k1gMnPc5	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
(	(	kIx(	(
<g/>
1694	[number]	k4	1694
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
vzestupu	vzestup	k1gInSc2	vzestup
této	tento	k3xDgFnSc2	tento
rodové	rodový	k2eAgFnSc2d1	rodová
větve	větev	k1gFnSc2	větev
stál	stát	k5eAaImAgMnS	stát
Sir	sir	k1gMnSc1	sir
William	William	k1gInSc4	William
Cavendish	Cavendish	k1gInSc1	Cavendish
(	(	kIx(	(
<g/>
1505	[number]	k4	1505
<g/>
-	-	kIx~	-
<g/>
1557	[number]	k4	1557
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zastával	zastávat	k5eAaImAgMnS	zastávat
funkce	funkce	k1gFnPc4	funkce
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1546	[number]	k4	1546
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
zbohatl	zbohatnout	k5eAaPmAgMnS	zbohatnout
při	při	k7c6	při
rozprodeji	rozprodej	k1gInSc6	rozprodej
majetku	majetek	k1gInSc2	majetek
zrušených	zrušený	k2eAgInPc2d1	zrušený
klášterů	klášter	k1gInPc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
William	William	k1gInSc1	William
(	(	kIx(	(
<g/>
1552	[number]	k4	1552
<g/>
–	–	k?	–
<g/>
1626	[number]	k4	1626
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
manželství	manželství	k1gNnSc2	manželství
své	svůj	k3xOyFgFnSc2	svůj
sestry	sestra	k1gFnSc2	sestra
Anne	Anne	k1gInSc1	Anne
vzdáleně	vzdáleně	k6eAd1	vzdáleně
spřízněn	spříznit	k5eAaPmNgInS	spříznit
s	s	k7c7	s
rodem	rod	k1gInSc7	rod
Stuartovců	Stuartovec	k1gMnPc2	Stuartovec
a	a	k8xC	a
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Jakuba	Jakub	k1gMnSc2	Jakub
I.	I.	kA	I.
na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
trůn	trůn	k1gInSc4	trůn
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
barona	baron	k1gMnSc2	baron
(	(	kIx(	(
<g/>
1605	[number]	k4	1605
<g/>
)	)	kIx)	)
a	a	k8xC	a
hraběte	hrabě	k1gMnSc4	hrabě
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
(	(	kIx(	(
<g/>
1618	[number]	k4	1618
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
místodržitelem	místodržitel	k1gMnSc7	místodržitel
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Derby	derby	k1gNnSc2	derby
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
úřad	úřad	k1gInSc4	úřad
zastávali	zastávat	k5eAaImAgMnP	zastávat
jeho	jeho	k3xOp3gMnPc1	jeho
potomci	potomek	k1gMnPc1	potomek
s	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
krátkými	krátký	k2eAgFnPc7d1	krátká
přestávkami	přestávka	k1gFnPc7	přestávka
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
pravnuk	pravnuk	k1gMnSc1	pravnuk
William	William	k1gInSc4	William
Cavendish	Cavendisha	k1gFnPc2	Cavendisha
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
(	(	kIx(	(
<g/>
1640	[number]	k4	1640
<g/>
–	–	k?	–
<g/>
1707	[number]	k4	1707
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aktivně	aktivně	k6eAd1	aktivně
podpořil	podpořit	k5eAaPmAgMnS	podpořit
Slavnou	slavný	k2eAgFnSc4d1	slavná
revoluci	revoluce	k1gFnSc4	revoluce
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1694	[number]	k4	1694
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
generacích	generace	k1gFnPc6	generace
vévodové	vévoda	k1gMnPc1	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
po	po	k7c6	po
několik	několik	k4yIc4	několik
staletí	staletí	k1gNnPc2	staletí
zastávali	zastávat	k5eAaImAgMnP	zastávat
nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
úřady	úřad	k1gInPc4	úřad
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
správě	správa	k1gFnSc6	správa
a	a	k8xC	a
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
vévoda	vévoda	k1gMnSc1	vévoda
byl	být	k5eAaImAgMnS	být
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
hofmistrem	hofmistr	k1gMnSc7	hofmistr
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
hodnost	hodnost	k1gFnSc4	hodnost
zastával	zastávat	k5eAaImAgMnS	zastávat
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgMnSc1	třetí
vévoda	vévoda	k1gMnSc1	vévoda
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
navíc	navíc	k6eAd1	navíc
místokrálem	místokrál	k1gMnSc7	místokrál
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
nositel	nositel	k1gMnSc1	nositel
titulu	titul	k1gInSc2	titul
William	William	k1gInSc1	William
Cavendish	Cavendish	k1gInSc1	Cavendish
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
(	(	kIx(	(
<g/>
1720	[number]	k4	1720
<g/>
–	–	k?	–
<g/>
1764	[number]	k4	1764
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
krátce	krátce	k6eAd1	krátce
i	i	k9	i
britským	britský	k2eAgMnSc7d1	britský
premiérem	premiér	k1gMnSc7	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
generacích	generace	k1gFnPc6	generace
zastávali	zastávat	k5eAaImAgMnP	zastávat
vévodové	vévoda	k1gMnPc1	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
čtyřicet	čtyřicet	k4xCc1	čtyřicet
let	léto	k1gNnPc2	léto
post	posta	k1gFnPc2	posta
kancléře	kancléř	k1gMnSc2	kancléř
pokladu	poklad	k1gInSc2	poklad
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
také	také	k9	také
díky	díky	k7c3	díky
sňatku	sňatek	k1gInSc3	sňatek
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
své	svůj	k3xOyFgFnPc4	svůj
majetkové	majetkový	k2eAgFnPc4d1	majetková
aktivity	aktivita	k1gFnPc4	aktivita
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
dědičkou	dědička	k1gFnSc7	dědička
rodu	rod	k1gInSc2	rod
Boyle	Boyle	k1gFnSc2	Boyle
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
přibližně	přibližně	k6eAd1	přibližně
25	[number]	k4	25
000	[number]	k4	000
hektarů	hektar	k1gInPc2	hektar
půdy	půda	k1gFnSc2	půda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dědictvím	dědictví	k1gNnSc7	dědictví
po	po	k7c6	po
rodu	rod	k1gInSc6	rod
Boyle	Boyle	k1gFnSc2	Boyle
byla	být	k5eAaImAgFnS	být
také	také	k9	také
zřícenina	zřícenina	k1gFnSc1	zřícenina
kláštera	klášter	k1gInSc2	klášter
Bolton	Bolton	k1gInSc1	Bolton
Abbey	Abbey	k1gInPc1	Abbey
(	(	kIx(	(
<g/>
Yorkshire	Yorkshir	k1gInSc5	Yorkshir
<g/>
)	)	kIx)	)
a	a	k8xC	a
londýnský	londýnský	k2eAgInSc4d1	londýnský
palác	palác	k1gInSc4	palác
Burlington	Burlington	k1gInSc1	Burlington
House	house	k1gNnSc1	house
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
osobností	osobnost	k1gFnSc7	osobnost
politiky	politika	k1gFnSc2	politika
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
Spencer	Spencer	k1gMnSc1	Spencer
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshiru	Devonshir	k1gInSc2	Devonshir
(	(	kIx(	(
<g/>
1833	[number]	k4	1833
<g/>
–	–	k?	–
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
rodinné	rodinný	k2eAgFnSc6d1	rodinná
tradici	tradice	k1gFnSc6	tradice
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
liberálům	liberál	k1gMnPc3	liberál
a	a	k8xC	a
zastával	zastávat	k5eAaImAgMnS	zastávat
několik	několik	k4yIc4	několik
ministerských	ministerský	k2eAgFnPc2d1	ministerská
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
strany	strana	k1gFnSc2	strana
Liberálních	liberální	k2eAgMnPc2d1	liberální
unionistů	unionista	k1gMnPc2	unionista
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přechodu	přechod	k1gInSc3	přechod
Cavendishů	Cavendish	k1gMnPc2	Cavendish
ke	k	k7c3	k
konzervativcům	konzervativec	k1gMnPc3	konzervativec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
synovec	synovec	k1gMnSc1	synovec
9	[number]	k4	9
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
byl	být	k5eAaImAgInS	být
generálním	generální	k2eAgMnSc7d1	generální
guvernérem	guvernér	k1gMnSc7	guvernér
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
ministrem	ministr	k1gMnSc7	ministr
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
konzervativních	konzervativní	k2eAgMnPc2d1	konzervativní
politiků	politik	k1gMnPc2	politik
se	se	k3xPyFc4	se
uplatnili	uplatnit	k5eAaPmAgMnP	uplatnit
i	i	k9	i
další	další	k2eAgMnPc1d1	další
nositelé	nositel	k1gMnPc1	nositel
titulu	titul	k1gInSc2	titul
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gMnSc5	Devonshir
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
představitelem	představitel	k1gMnSc7	představitel
rodu	rod	k1gInSc2	rod
je	být	k5eAaImIp3nS	být
Peregrine	Peregrin	k1gInSc5	Peregrin
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sídla	sídlo	k1gNnPc1	sídlo
vévodů	vévoda	k1gMnPc2	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gMnSc5	Devonshir
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
sídlem	sídlo	k1gNnSc7	sídlo
vévodů	vévoda	k1gMnPc2	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc4	zámek
Chatsworth	Chatsworth	k1gInSc1	Chatsworth
House	house	k1gNnSc1	house
(	(	kIx(	(
<g/>
Derbyshire	Derbyshir	k1gMnSc5	Derbyshir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
rodině	rodina	k1gFnSc3	rodina
patří	patřit	k5eAaImIp3nS	patřit
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
podoba	podoba	k1gFnSc1	podoba
zámku	zámek	k1gInSc2	zámek
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
mnoha	mnoho	k4c2	mnoho
přestaveb	přestavba	k1gFnPc2	přestavba
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
několika	několik	k4yIc2	několik
století	století	k1gNnPc2	století
<g/>
,	,	kIx,	,
Cavendishové	Cavendish	k1gMnPc1	Cavendish
zde	zde	k6eAd1	zde
soustředili	soustředit	k5eAaPmAgMnP	soustředit
bohaté	bohatý	k2eAgFnPc4d1	bohatá
sbírky	sbírka	k1gFnPc4	sbírka
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
malířského	malířský	k2eAgNnSc2d1	malířské
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
Rembrandt	Rembrandt	k1gInSc1	Rembrandt
<g/>
,	,	kIx,	,
Van	van	k1gInSc1	van
Dyck	Dyck	k1gInSc1	Dyck
<g/>
,	,	kIx,	,
Tintoretto	Tintoretto	k1gNnSc1	Tintoretto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzácné	vzácný	k2eAgFnPc1d1	vzácná
umělecké	umělecký	k2eAgFnPc1d1	umělecká
sbírky	sbírka	k1gFnPc1	sbírka
tvoří	tvořit	k5eAaImIp3nP	tvořit
významnou	významný	k2eAgFnSc4d1	významná
položku	položka	k1gFnSc4	položka
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
bohatství	bohatství	k1gNnSc6	bohatství
vévodů	vévoda	k1gMnPc2	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gMnSc5	Devonshir
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
10	[number]	k4	10
<g/>
.	.	kIx.	.
vévody	vévoda	k1gMnPc4	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
rodina	rodina	k1gFnSc1	rodina
finančně	finančně	k6eAd1	finančně
zatížena	zatížit	k5eAaPmNgFnS	zatížit
vysokou	vysoký	k2eAgFnSc7d1	vysoká
dědickou	dědický	k2eAgFnSc7d1	dědická
daní	daň	k1gFnSc7	daň
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zpřístupnění	zpřístupnění	k1gNnSc3	zpřístupnění
zámku	zámek	k1gInSc2	zámek
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
majetkem	majetek	k1gInSc7	majetek
rodu	rod	k1gInSc2	rod
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc1	zámek
Holker	Holker	k1gMnSc1	Holker
Hall	Hall	k1gMnSc1	Hall
(	(	kIx(	(
<g/>
Cumberland	Cumberland	k1gInSc1	Cumberland
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1754	[number]	k4	1754
dědictvím	dědictví	k1gNnSc7	dědictví
po	po	k7c6	po
rodu	rod	k1gInSc6	rod
Lowtherů	Lowther	k1gInPc2	Lowther
lord	lord	k1gMnSc1	lord
George	Georg	k1gMnSc2	Georg
Cavendish	Cavendish	k1gMnSc1	Cavendish
(	(	kIx(	(
<g/>
1727	[number]	k4	1727
<g/>
–	–	k?	–
<g/>
1794	[number]	k4	1794
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgMnSc1d2	mladší
syn	syn	k1gMnSc1	syn
3	[number]	k4	3
<g/>
.	.	kIx.	.
vévody	vévoda	k1gMnPc4	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
nechal	nechat	k5eAaPmAgInS	nechat
nákladně	nákladně	k6eAd1	nákladně
přestavět	přestavět	k5eAaPmF	přestavět
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
současná	současný	k2eAgFnSc1d1	současná
podoba	podoba	k1gFnSc1	podoba
ale	ale	k8xC	ale
pochází	pocházet	k5eAaImIp3nS	pocházet
až	až	k6eAd1	až
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
prošel	projít	k5eAaPmAgMnS	projít
několika	několik	k4yIc2	několik
novogotickými	novogotický	k2eAgFnPc7d1	novogotická
úpravami	úprava	k1gFnPc7	úprava
za	za	k7c4	za
7	[number]	k4	7
<g/>
.	.	kIx.	.
vévody	vévoda	k1gMnPc4	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gMnSc5	Devonshir
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tento	tento	k3xDgInSc1	tento
zámek	zámek	k1gInSc1	zámek
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
zpřístupněn	zpřístupněn	k2eAgInSc1d1	zpřístupněn
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
majitelem	majitel	k1gMnSc7	majitel
je	být	k5eAaImIp3nS	být
Richard	Richard	k1gMnSc1	Richard
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
baron	baron	k1gMnSc1	baron
Cavendish	Cavendish	k1gMnSc1	Cavendish
z	z	k7c2	z
Furnessu	Furness	k1gInSc2	Furness
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
architektonickou	architektonický	k2eAgFnSc7d1	architektonická
památkou	památka	k1gFnSc7	památka
alžbětinské	alžbětinský	k2eAgFnSc2d1	Alžbětinská
éry	éra	k1gFnSc2	éra
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc1	zámek
Hardwick	Hardwick	k1gInSc1	Hardwick
Hall	Hall	k1gInSc4	Hall
(	(	kIx(	(
<g/>
Derbyshire	Derbyshir	k1gMnSc5	Derbyshir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
nějž	jenž	k3xRgMnSc4	jenž
byl	být	k5eAaImAgMnS	být
také	také	k9	také
odvozen	odvozen	k2eAgMnSc1d1	odvozen
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
titulů	titul	k1gInPc2	titul
Cavendishů	Cavendish	k1gInPc2	Cavendish
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jej	on	k3xPp3gMnSc4	on
nechala	nechat	k5eAaPmAgFnS	nechat
postavit	postavit	k5eAaPmF	postavit
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
(	(	kIx(	(
<g/>
Bess	Bess	k1gInSc1	Bess
<g/>
)	)	kIx)	)
z	z	k7c2	z
Hardwicku	Hardwick	k1gInSc2	Hardwick
(	(	kIx(	(
<g/>
1527	[number]	k4	1527
<g/>
–	–	k?	–
<g/>
1608	[number]	k4	1608
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Sira	sir	k1gMnSc2	sir
Williama	William	k1gMnSc2	William
Cavendishe	Cavendish	k1gMnSc2	Cavendish
<g/>
.	.	kIx.	.
</s>
<s>
Hrabata	hrabě	k1gNnPc1	hrabě
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
později	pozdě	k6eAd2	pozdě
přesídlila	přesídlit	k5eAaPmAgFnS	přesídlit
na	na	k7c4	na
Chatsworth	Chatsworth	k1gInSc4	Chatsworth
House	house	k1gNnSc1	house
a	a	k8xC	a
Hardwick	Hardwick	k1gMnSc1	Hardwick
Hall	Hall	k1gMnSc1	Hall
zůstal	zůstat	k5eAaPmAgMnS	zůstat
stranou	stranou	k6eAd1	stranou
jejich	jejich	k3xOp3gInSc2	jejich
zájmu	zájem	k1gInSc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
vévodové	vévoda	k1gMnPc1	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
opomíjený	opomíjený	k2eAgInSc1d1	opomíjený
a	a	k8xC	a
zanedbaný	zanedbaný	k2eAgInSc1d1	zanedbaný
zámek	zámek	k1gInSc1	zámek
zůstal	zůstat	k5eAaPmAgInS	zůstat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
původní	původní	k2eAgFnSc6d1	původní
renesanční	renesanční	k2eAgFnSc6d1	renesanční
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc4	zámek
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
organizace	organizace	k1gFnSc2	organizace
National	National	k1gFnSc2	National
Trust	trust	k1gInSc1	trust
a	a	k8xC	a
přístupný	přístupný	k2eAgInSc1d1	přístupný
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sňatkem	sňatek	k1gInSc7	sňatek
premiéra	premiér	k1gMnSc2	premiér
4	[number]	k4	4
<g/>
.	.	kIx.	.
vévody	vévoda	k1gMnPc4	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
s	s	k7c7	s
Charlotte	Charlott	k1gInSc5	Charlott
Boyle	Boyle	k1gNnPc1	Boyle
(	(	kIx(	(
<g/>
1731	[number]	k4	1731
<g/>
–	–	k?	–
<g/>
1754	[number]	k4	1754
<g/>
)	)	kIx)	)
přešlo	přejít	k5eAaPmAgNnS	přejít
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
Cavendishů	Cavendish	k1gMnPc2	Cavendish
bohaté	bohatý	k2eAgNnSc1d1	bohaté
dědictví	dědictví	k1gNnSc1	dědictví
rodu	rod	k1gInSc2	rod
Boyle	Boyle	k1gFnSc2	Boyle
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
i	i	k8xC	i
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
dědictví	dědictví	k1gNnSc2	dědictví
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
Lismore	Lismor	k1gInSc5	Lismor
Castle	Castle	k1gFnSc1	Castle
(	(	kIx(	(
<g/>
hrabství	hrabství	k1gNnSc1	hrabství
Waterford	Waterforda	k1gFnPc2	Waterforda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xC	jako
sídlo	sídlo	k1gNnSc4	sídlo
vévodů	vévoda	k1gMnPc2	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
při	při	k7c6	při
pobytech	pobyt	k1gInPc6	pobyt
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
novogoticky	novogoticky	k6eAd1	novogoticky
přestavěn	přestavět	k5eAaPmNgInS	přestavět
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
sídlem	sídlo	k1gNnSc7	sídlo
Charlese	Charles	k1gMnSc2	Charles
Cavendishe	Cavendish	k1gMnSc2	Cavendish
<g/>
,	,	kIx,	,
markýza	markýz	k1gMnSc2	markýz
Hartingtona	Hartington	k1gMnSc2	Hartington
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
dědicem	dědic	k1gMnSc7	dědic
10	[number]	k4	10
<g/>
.	.	kIx.	.
vévody	vévoda	k1gMnPc4	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gMnSc5	Devonshir
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
padl	padnout	k5eAaPmAgInS	padnout
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Adele	Adele	k1gFnSc1	Adele
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
amerického	americký	k2eAgMnSc2d1	americký
herce	herec	k1gMnSc2	herec
a	a	k8xC	a
tanečníka	tanečník	k1gMnSc2	tanečník
Freda	Fred	k1gMnSc2	Fred
Astaira	Astair	k1gMnSc2	Astair
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
žila	žít	k5eAaImAgFnS	žít
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
majetkem	majetek	k1gInSc7	majetek
vévodů	vévoda	k1gMnPc2	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gMnSc5	Devonshir
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
částečně	částečně	k6eAd1	částečně
je	být	k5eAaImIp3nS	být
zpřístupněn	zpřístupněn	k2eAgInSc1d1	zpřístupněn
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
sídlem	sídlo	k1gNnSc7	sídlo
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
majetku	majetek	k1gInSc2	majetek
Boylů	Boyl	k1gMnPc2	Boyl
byl	být	k5eAaImAgInS	být
zámek	zámek	k1gInSc1	zámek
Londesborough	Londesborough	k1gInSc1	Londesborough
Hall	Hall	k1gInSc4	Hall
(	(	kIx(	(
<g/>
Yorkshire	Yorkshir	k1gMnSc5	Yorkshir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
6	[number]	k4	6
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
nechal	nechat	k5eAaPmAgMnS	nechat
zbořit	zbořit	k5eAaPmF	zbořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1819	[number]	k4	1819
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
měl	mít	k5eAaImAgMnS	mít
řadu	řada	k1gFnSc4	řada
jiných	jiný	k2eAgNnPc2d1	jiné
sídel	sídlo	k1gNnPc2	sídlo
a	a	k8xC	a
potýkal	potýkat	k5eAaImAgInS	potýkat
se	se	k3xPyFc4	se
s	s	k7c7	s
finančními	finanční	k2eAgInPc7d1	finanční
problémy	problém	k1gInPc7	problém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
byly	být	k5eAaImAgInP	být
majetkem	majetek	k1gInSc7	majetek
rodu	rod	k1gInSc2	rod
městské	městský	k2eAgInPc1d1	městský
paláce	palác	k1gInPc1	palác
Devonshire	Devonshir	k1gInSc5	Devonshir
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
z	z	k7c2	z
dědictví	dědictví	k1gNnSc2	dědictví
po	po	k7c6	po
rodině	rodina	k1gFnSc6	rodina
Boyle	Boyle	k1gNnSc2	Boyle
paláce	palác	k1gInSc2	palác
Chiswick	Chiswicka	k1gFnPc2	Chiswicka
House	house	k1gNnSc1	house
a	a	k8xC	a
Burlington	Burlington	k1gInSc1	Burlington
House	house	k1gNnSc1	house
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
získali	získat	k5eAaPmAgMnP	získat
dědictvím	dědictví	k1gNnSc7	dědictví
zámek	zámek	k1gInSc1	zámek
Compton	Compton	k1gInSc1	Compton
Place	plac	k1gInSc6	plac
(	(	kIx(	(
<g/>
Surrey	Surrea	k1gFnSc2	Surrea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
občasným	občasný	k2eAgNnSc7d1	občasné
rodovým	rodový	k2eAgNnSc7d1	rodové
sídlem	sídlo	k1gNnSc7	sídlo
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
přešel	přejít	k5eAaPmAgMnS	přejít
do	do	k7c2	do
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hrabata	hrabě	k1gNnPc1	hrabě
z	z	k7c2	z
Burlingtonu	Burlington	k1gInSc2	Burlington
(	(	kIx(	(
<g/>
1831	[number]	k4	1831
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Titul	titul	k1gInSc4	titul
hrabat	hrabě	k1gNnPc2	hrabě
z	z	k7c2	z
Burlingtonu	Burlington	k1gInSc2	Burlington
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
udělen	udělit	k5eAaPmNgInS	udělit
rodině	rodina	k1gFnSc3	rodina
Boyle	Boyle	k1gNnSc2	Boyle
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
majetek	majetek	k1gInSc4	majetek
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Dcera	dcera	k1gFnSc1	dcera
3	[number]	k4	3
<g/>
.	.	kIx.	.
hraběte	hrabě	k1gMnSc4	hrabě
z	z	k7c2	z
Burlingtonu	Burlington	k1gInSc2	Burlington
<g/>
,	,	kIx,	,
Charlotte	Charlott	k1gMnSc5	Charlott
Boyle	Boyl	k1gMnSc5	Boyl
(	(	kIx(	(
<g/>
1731	[number]	k4	1731
<g/>
–	–	k?	–
<g/>
1754	[number]	k4	1754
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
premiéra	premiér	k1gMnSc2	premiér
4	[number]	k4	4
<g/>
.	.	kIx.	.
vévodu	vévoda	k1gMnSc4	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
<g/>
,	,	kIx,	,
věnem	věno	k1gNnSc7	věno
mu	on	k3xPp3gMnSc3	on
také	také	k6eAd1	také
přinesla	přinést	k5eAaPmAgFnS	přinést
statky	statek	k1gInPc4	statek
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
a	a	k8xC	a
také	také	k9	také
londýnský	londýnský	k2eAgInSc4d1	londýnský
palác	palác	k1gInSc4	palác
Burlington	Burlington	k1gInSc1	Burlington
House	house	k1gNnSc1	house
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc1	jejich
mladší	mladý	k2eAgMnSc1d2	mladší
syn	syn	k1gMnSc1	syn
lord	lord	k1gMnSc1	lord
George	Georg	k1gMnSc2	Georg
Cavendish	Cavendish	k1gMnSc1	Cavendish
(	(	kIx(	(
<g/>
1754	[number]	k4	1754
<g/>
–	–	k?	–
<g/>
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
členem	člen	k1gMnSc7	člen
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
své	svůj	k3xOyFgFnSc2	svůj
politické	politický	k2eAgFnSc2d1	politická
kariéry	kariéra	k1gFnSc2	kariéra
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1831	[number]	k4	1831
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
hraběte	hrabě	k1gMnSc4	hrabě
z	z	k7c2	z
Burlingtonu	Burlington	k1gInSc2	Burlington
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
byl	být	k5eAaImAgMnS	být
dědicem	dědic	k1gMnSc7	dědic
majetku	majetek	k1gInSc2	majetek
svého	svůj	k3xOyFgMnSc2	svůj
strýce	strýc	k1gMnSc2	strýc
maršála	maršál	k1gMnSc2	maršál
Fredericka	Fredericka	k1gFnSc1	Fredericka
Cavendishe	Cavendishe	k1gFnSc1	Cavendishe
(	(	kIx(	(
<g/>
1729	[number]	k4	1729
<g/>
–	–	k?	–
<g/>
1803	[number]	k4	1803
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
synovce	synovec	k1gMnSc2	synovec
6	[number]	k4	6
<g/>
.	.	kIx.	.
vévody	vévoda	k1gMnPc4	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
koupil	koupit	k5eAaPmAgMnS	koupit
londýnský	londýnský	k2eAgInSc4d1	londýnský
palác	palác	k1gInSc4	palác
Burlington	Burlington	k1gInSc4	Burlington
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pak	pak	k6eAd1	pak
nechal	nechat	k5eAaPmAgMnS	nechat
přestavět	přestavět	k5eAaPmF	přestavět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
vnuk	vnuk	k1gMnSc1	vnuk
William	William	k1gInSc4	William
Cavendish	Cavendisha	k1gFnPc2	Cavendisha
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Burlingtonu	Burlington	k1gInSc2	Burlington
(	(	kIx(	(
<g/>
1808	[number]	k4	1808
<g/>
-	-	kIx~	-
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zdědil	zdědit	k5eAaPmAgInS	zdědit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
titul	titul	k1gInSc1	titul
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgInPc1	dva
tituly	titul	k1gInPc1	titul
spojeny	spojen	k2eAgInPc1d1	spojen
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc4	palác
Burlington	Burlington	k1gInSc1	Burlington
House	house	k1gNnSc1	house
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
prodán	prodán	k2eAgInSc1d1	prodán
britské	britský	k2eAgFnSc3d1	britská
vládě	vláda	k1gFnSc3	vláda
za	za	k7c4	za
140	[number]	k4	140
000	[number]	k4	000
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Baronové	baron	k1gMnPc1	baron
z	z	k7c2	z
Cheshamu	Chesham	k1gInSc2	Chesham
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Charles	Charles	k1gMnSc1	Charles
Compton	Compton	k1gInSc4	Compton
Cavendish	Cavendish	k1gInSc1	Cavendish
(	(	kIx(	(
<g/>
1793	[number]	k4	1793
<g/>
–	–	k?	–
<g/>
1863	[number]	k4	1863
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
mladším	mladý	k2eAgMnSc7d2	mladší
synem	syn	k1gMnSc7	syn
1	[number]	k4	1
<g/>
.	.	kIx.	.
hraběte	hrabě	k1gMnSc4	hrabě
z	z	k7c2	z
Burlingtonu	Burlington	k1gInSc2	Burlington
a	a	k8xC	a
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
za	za	k7c4	za
stranu	strana	k1gFnSc4	strana
liberálů	liberál	k1gMnPc2	liberál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
barona	baron	k1gMnSc2	baron
z	z	k7c2	z
Cheshamu	Chesham	k1gInSc2	Chesham
a	a	k8xC	a
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
lordů	lord	k1gMnPc2	lord
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
vnuk	vnuk	k1gMnSc1	vnuk
Charles	Charles	k1gMnSc1	Charles
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
baron	baron	k1gMnSc1	baron
Chesham	Chesham	k1gInSc1	Chesham
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
–	–	k?	–
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
brigádním	brigádní	k2eAgMnSc7d1	brigádní
generálem	generál	k1gMnSc7	generál
a	a	k8xC	a
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
proslul	proslout	k5eAaPmAgInS	proslout
jako	jako	k9	jako
organizátor	organizátor	k1gInSc1	organizátor
honů	hon	k1gInPc2	hon
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1901	[number]	k4	1901
byl	být	k5eAaImAgInS	být
krátce	krátce	k6eAd1	krátce
nejvyšším	vysoký	k2eAgMnPc3d3	nejvyšší
lovčím	lovčí	k1gMnPc3	lovčí
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Zahynul	zahynout	k5eAaPmAgMnS	zahynout
na	na	k7c6	na
honu	hon	k1gInSc6	hon
pádem	pád	k1gInSc7	pád
z	z	k7c2	z
koně	kůň	k1gMnSc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
baron	baron	k1gMnSc1	baron
Chesham	Chesham	k1gInSc1	Chesham
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
konzervativním	konzervativní	k2eAgMnSc7d1	konzervativní
politikem	politik	k1gMnSc7	politik
a	a	k8xC	a
zastával	zastávat	k5eAaImAgMnS	zastávat
nižší	nízký	k2eAgInPc4d2	nižší
úřady	úřad	k1gInPc4	úřad
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
představitelem	představitel	k1gMnSc7	představitel
této	tento	k3xDgFnSc2	tento
rodové	rodový	k2eAgFnSc2d1	rodová
linie	linie	k1gFnSc2	linie
je	být	k5eAaImIp3nS	být
Charles	Charles	k1gMnSc1	Charles
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
baron	baron	k1gMnSc1	baron
Chesham	Chesham	k1gInSc1	Chesham
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
reorganizace	reorganizace	k1gFnSc2	reorganizace
Sněmovny	sněmovna	k1gFnSc2	sněmovna
lordů	lord	k1gMnPc2	lord
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
mu	on	k3xPp3gMnSc3	on
ale	ale	k8xC	ale
nenáleží	náležet	k5eNaImIp3nS	náležet
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Horní	horní	k2eAgFnSc6d1	horní
sněmovně	sněmovna	k1gFnSc6	sněmovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
této	tento	k3xDgFnSc2	tento
rodové	rodový	k2eAgFnSc2d1	rodová
linie	linie	k1gFnSc2	linie
Cavendishů	Cavendish	k1gMnPc2	Cavendish
byl	být	k5eAaImAgInS	být
zámek	zámek	k1gInSc1	zámek
Latimer	Latimer	k1gInSc4	Latimer
House	house	k1gNnSc1	house
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Buckinghamshire	Buckinghamshir	k1gMnSc5	Buckinghamshir
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Cavendishové	Cavendish	k1gMnPc1	Cavendish
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
angažovali	angažovat	k5eAaBmAgMnP	angažovat
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
samosprávě	samospráva	k1gFnSc6	samospráva
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1838	[number]	k4	1838
pro	pro	k7c4	pro
1	[number]	k4	1
<g/>
.	.	kIx.	.
barona	baron	k1gMnSc2	baron
Cheshama	Chesham	k1gMnSc2	Chesham
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
4	[number]	k4	4
<g/>
.	.	kIx.	.
barona	baron	k1gMnSc2	baron
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
sídlo	sídlo	k1gNnSc1	sídlo
kvůli	kvůli	k7c3	kvůli
vysoké	vysoký	k2eAgFnSc3d1	vysoká
dědické	dědický	k2eAgFnSc3d1	dědická
dani	daň	k1gFnSc3	daň
předáno	předat	k5eAaPmNgNnS	předat
do	do	k7c2	do
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Baronové	baron	k1gMnPc1	baron
z	z	k7c2	z
Waterparku	Waterpark	k1gInSc2	Waterpark
(	(	kIx(	(
<g/>
1792	[number]	k4	1792
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
této	tento	k3xDgFnSc2	tento
rodové	rodový	k2eAgFnSc2d1	rodová
linie	linie	k1gFnSc2	linie
byl	být	k5eAaImAgMnS	být
Sir	sir	k1gMnSc1	sir
Henry	Henry	k1gMnSc1	Henry
Cavendish	Cavendish	k1gMnSc1	Cavendish
(	(	kIx(	(
<g/>
1707	[number]	k4	1707
<g/>
–	–	k?	–
<g/>
1776	[number]	k4	1776
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
patřil	patřit	k5eAaImAgInS	patřit
ke	k	k7c3	k
vzdálenému	vzdálený	k2eAgNnSc3d1	vzdálené
příbuzenstvu	příbuzenstvo	k1gNnSc3	příbuzenstvo
vévodů	vévoda	k1gMnPc2	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
a	a	k8xC	a
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
statky	statek	k1gInPc7	statek
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Derby	derby	k1gNnSc2	derby
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
členem	člen	k1gMnSc7	člen
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
zastával	zastávat	k5eAaImAgInS	zastávat
také	také	k9	také
státní	státní	k2eAgInPc4d1	státní
úřady	úřad	k1gInPc4	úřad
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1755	[number]	k4	1755
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
baroneta	baronet	k1gMnSc4	baronet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Sir	sir	k1gMnSc1	sir
Henry	Henry	k1gMnSc1	Henry
Cavendish	Cavendish	k1gMnSc1	Cavendish
(	(	kIx(	(
<g/>
1732	[number]	k4	1732
<g/>
–	–	k?	–
<g/>
1804	[number]	k4	1804
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Sarah	Sarah	k1gFnSc1	Sarah
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Bradshaw	Bradshaw	k1gFnSc1	Bradshaw
(	(	kIx(	(
<g/>
1740	[number]	k4	1740
<g/>
-	-	kIx~	-
<g/>
1807	[number]	k4	1807
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1792	[number]	k4	1792
titul	titul	k1gInSc4	titul
baronky	baronka	k1gFnSc2	baronka
Waterpark	Waterpark	k1gInSc1	Waterpark
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
platil	platit	k5eAaImAgInS	platit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
Irsko	Irsko	k1gNnSc4	Irsko
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
tedy	tedy	k9	tedy
spojeno	spojen	k2eAgNnSc1d1	spojeno
členství	členství	k1gNnSc1	členství
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
Sněmovně	sněmovna	k1gFnSc6	sněmovna
lordů	lord	k1gMnPc2	lord
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
potomci	potomek	k1gMnPc1	potomek
angažovali	angažovat	k5eAaBmAgMnP	angažovat
v	v	k7c6	v
Dolní	dolní	k2eAgFnSc6d1	dolní
sněmovně	sněmovna	k1gFnSc6	sněmovna
<g/>
,	,	kIx,	,
současným	současný	k2eAgMnSc7d1	současný
představitelem	představitel	k1gMnSc7	představitel
této	tento	k3xDgFnSc2	tento
větve	větev	k1gFnSc2	větev
je	být	k5eAaImIp3nS	být
Roderick	Roderick	k1gMnSc1	Roderick
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
baron	baron	k1gMnSc1	baron
Waterpark	Waterpark	k1gInSc1	Waterpark
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
této	tento	k3xDgFnSc2	tento
rodové	rodový	k2eAgFnSc2d1	rodová
linie	linie	k1gFnSc2	linie
byl	být	k5eAaImAgInS	být
zámek	zámek	k1gInSc1	zámek
Doveridge	Doveridg	k1gFnSc2	Doveridg
Hall	Halla	k1gFnPc2	Halla
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Derbyshire	Derbyshir	k1gInSc5	Derbyshir
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1769	[number]	k4	1769
pro	pro	k7c4	pro
Sira	sir	k1gMnSc4	sir
Henryho	Henry	k1gMnSc4	Henry
Cavendishe	Cavendish	k1gMnSc4	Cavendish
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Sir	sir	k1gMnSc1	sir
John	John	k1gMnSc1	John
Cavendish	Cavendish	k1gMnSc1	Cavendish
(	(	kIx(	(
<g/>
1346	[number]	k4	1346
<g/>
–	–	k?	–
<g/>
1381	[number]	k4	1381
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
sudíSir	sudíSir	k1gInSc1	sudíSir
William	William	k1gInSc1	William
Cavendish	Cavendish	k1gInSc1	Cavendish
(	(	kIx(	(
<g/>
1505	[number]	k4	1505
<g/>
–	–	k?	–
<g/>
1557	[number]	k4	1557
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokladník	pokladník	k1gMnSc1	pokladník
královské	královský	k2eAgFnSc2d1	královská
komoryWilliam	komoryWilliam	k6eAd1	komoryWilliam
Cavendish	Cavendisha	k1gFnPc2	Cavendisha
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
(	(	kIx(	(
<g/>
1552	[number]	k4	1552
<g/>
–	–	k?	–
<g/>
1626	[number]	k4	1626
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
místodržitel	místodržitel	k1gMnSc1	místodržitel
v	v	k7c6	v
Derby	derby	k1gNnSc6	derby
<g/>
,	,	kIx,	,
1618	[number]	k4	1618
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
DevonshireThomas	DevonshireThomasa	k1gFnPc2	DevonshireThomasa
Cavendish	Cavendisha	k1gFnPc2	Cavendisha
(	(	kIx(	(
<g/>
1560	[number]	k4	1560
<g/>
–	–	k?	–
<g/>
1592	[number]	k4	1592
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cestovatelWilliam	cestovatelWilliam	k6eAd1	cestovatelWilliam
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Newcastle	Newcastle	k1gFnSc2	Newcastle
(	(	kIx(	(
<g/>
1592	[number]	k4	1592
<g/>
-	-	kIx~	-
<g/>
1676	[number]	k4	1676
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvořan	dvořan	k1gMnSc1	dvořan
<g/>
,	,	kIx,	,
vychovatel	vychovatel	k1gMnSc1	vychovatel
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1664	[number]	k4	1664
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
NewcastleSir	NewcastleSira	k1gFnPc2	NewcastleSira
Charles	Charles	k1gMnSc1	Charles
Cavendish	Cavendish	k1gMnSc1	Cavendish
(	(	kIx(	(
<g/>
1595	[number]	k4	1595
<g/>
–	–	k?	–
<g/>
1664	[number]	k4	1664
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
generálHenry	generálHenra	k1gFnSc2	generálHenra
Cavendish	Cavendisha	k1gFnPc2	Cavendisha
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Newcastle	Newcastle	k1gFnSc2	Newcastle
(	(	kIx(	(
<g/>
1630	[number]	k4	1630
<g/>
–	–	k?	–
<g/>
1691	[number]	k4	1691
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvořanWilliam	dvořanWilliam	k6eAd1	dvořanWilliam
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshiru	Devonshir	k1gInSc2	Devonshir
(	(	kIx(	(
<g/>
1640	[number]	k4	1640
<g/>
–	–	k?	–
<g/>
1707	[number]	k4	1707
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
hofmistr	hofmistr	k1gMnSc1	hofmistr
<g/>
,	,	kIx,	,
1694	[number]	k4	1694
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
DevonshireWilliam	DevonshireWilliam	k1gInSc4	DevonshireWilliam
Cavendish	Cavendisha	k1gFnPc2	Cavendisha
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshiru	Devonshir	k1gInSc2	Devonshir
(	(	kIx(	(
<g/>
1673	[number]	k4	1673
<g/>
–	–	k?	–
<g/>
1729	[number]	k4	1729
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
hofmistr	hofmistr	k1gMnSc1	hofmistr
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
<g />
.	.	kIx.	.
</s>
<s>
Tajné	tajný	k2eAgFnPc1d1	tajná
radyPhilip	radyPhilip	k1gInSc4	radyPhilip
Cavendish	Cavendisha	k1gFnPc2	Cavendisha
(	(	kIx(	(
<g/>
1680	[number]	k4	1680
<g/>
–	–	k?	–
<g/>
1743	[number]	k4	1743
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nemanželský	manželský	k2eNgMnSc1d1	nemanželský
syn	syn	k1gMnSc1	syn
1	[number]	k4	1
<g/>
.	.	kIx.	.
vévody	vévoda	k1gMnPc4	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
<g/>
,	,	kIx,	,
admirál	admirál	k1gMnSc1	admirál
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Dolní	dolní	k2eAgInSc4d1	dolní
sněmovnyWilliam	sněmovnyWilliam	k1gInSc4	sněmovnyWilliam
Cavendish	Cavendisha	k1gFnPc2	Cavendisha
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshiru	Devonshir	k1gInSc2	Devonshir
(	(	kIx(	(
<g/>
1698	[number]	k4	1698
<g/>
–	–	k?	–
<g/>
1755	[number]	k4	1755
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
hofmistr	hofmistr	k1gMnSc1	hofmistr
<g/>
,	,	kIx,	,
místokrál	místokrál	k1gMnSc1	místokrál
v	v	k7c4	v
IrskuLord	IrskuLord	k1gInSc4	IrskuLord
Charles	Charles	k1gMnSc1	Charles
Cavendish	Cavendish	k1gMnSc1	Cavendish
(	(	kIx(	(
<g/>
1704	[number]	k4	1704
<g/>
–	–	k?	–
<g/>
1783	[number]	k4	1783
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvořan	dvořan	k1gMnSc1	dvořan
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
vědecSir	vědecSir	k1gMnSc1	vědecSir
Henry	Henry	k1gMnSc1	Henry
Cavendish	Cavendish	k1gMnSc1	Cavendish
(	(	kIx(	(
<g/>
1707	[number]	k4	1707
<g/>
–	–	k?	–
<g/>
1776	[number]	k4	1776
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
irské	irský	k2eAgFnSc2d1	irská
Tajné	tajný	k2eAgFnSc2d1	tajná
<g />
.	.	kIx.	.
</s>
<s>
radyWilliam	radyWilliam	k1gInSc1	radyWilliam
Cavendish	Cavendisha	k1gFnPc2	Cavendisha
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
(	(	kIx(	(
<g/>
1720	[number]	k4	1720
<g/>
–	–	k?	–
<g/>
1764	[number]	k4	1764
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
premiér	premiér	k1gMnSc1	premiér
1756	[number]	k4	1756
<g/>
–	–	k?	–
<g/>
1757	[number]	k4	1757
<g/>
,	,	kIx,	,
místokrál	místokrál	k1gMnSc1	místokrál
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
komoříLord	komoříLord	k1gMnSc1	komoříLord
George	George	k1gNnSc2	George
Augustus	Augustus	k1gMnSc1	Augustus
Cavendish	Cavendish	k1gMnSc1	Cavendish
(	(	kIx(	(
<g/>
1726	[number]	k4	1726
<g/>
–	–	k?	–
<g/>
1794	[number]	k4	1794
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
dvořan	dvořan	k1gMnSc1	dvořan
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Dolní	dolní	k2eAgMnSc1d1	dolní
sněmovnyLord	sněmovnyLord	k1gMnSc1	sněmovnyLord
Frederick	Frederick	k1gMnSc1	Frederick
Cavendish	Cavendish	k1gMnSc1	Cavendish
(	(	kIx(	(
<g/>
1729	[number]	k4	1729
<g/>
–	–	k?	–
<g/>
1803	[number]	k4	1803
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polní	polní	k2eAgMnSc1d1	polní
maršál	maršál	k1gMnSc1	maršál
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovnyHenry	sněmovnyHenra	k1gFnSc2	sněmovnyHenra
Cavendish	Cavendish	k1gMnSc1	Cavendish
(	(	kIx(	(
<g/>
1731	[number]	k4	1731
<g/>
–	–	k?	–
<g/>
1810	[number]	k4	1810
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
vodíkuLord	vodíkuLord	k1gMnSc1	vodíkuLord
John	John	k1gMnSc1	John
Cavendish	Cavendish	k1gMnSc1	Cavendish
(	(	kIx(	(
<g/>
1732	[number]	k4	1732
<g/>
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
<g/>
1796	[number]	k4	1796
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
financíSir	financíSir	k1gMnSc1	financíSir
Henry	Henry	k1gMnSc1	Henry
Cavendish	Cavendish	k1gMnSc1	Cavendish
(	(	kIx(	(
<g/>
1732	[number]	k4	1732
<g/>
–	–	k?	–
<g/>
1804	[number]	k4	1804
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
irské	irský	k2eAgFnSc2d1	irská
Tajné	tajný	k2eAgFnPc1d1	tajná
radyWilliam	radyWilliam	k1gInSc4	radyWilliam
Cavendish	Cavendisha	k1gFnPc2	Cavendisha
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
(	(	kIx(	(
<g/>
1748	[number]	k4	1748
<g/>
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1811	[number]	k4	1811
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
hofmistr	hofmistr	k1gMnSc1	hofmistr
<g/>
,	,	kIx,	,
kancléř	kancléř	k1gMnSc1	kancléř
pokladu	poklad	k1gInSc2	poklad
v	v	k7c6	v
IrskuGeorge	IrskuGeorge	k1gFnSc6	IrskuGeorge
Augustus	Augustus	k1gMnSc1	Augustus
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Burlingtonu	Burlington	k1gInSc2	Burlington
(	(	kIx(	(
<g/>
1754	[number]	k4	1754
<g/>
–	–	k?	–
<g/>
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
1831	[number]	k4	1831
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
BurlingtonuHenry	BurlingtonuHenra	k1gFnSc2	BurlingtonuHenra
Cavendish	Cavendish	k1gInSc1	Cavendish
(	(	kIx(	(
<g/>
1789	[number]	k4	1789
<g/>
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
<g/>
,	,	kIx,	,
dvořanWilliam	dvořanWilliam	k6eAd1	dvořanWilliam
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
(	(	kIx(	(
<g/>
1790	[number]	k4	1790
<g/>
–	–	k?	–
<g/>
1858	[number]	k4	1858
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
komoříCharles	komoříCharles	k1gMnSc1	komoříCharles
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
baron	baron	k1gMnSc1	baron
Chesham	Chesham	k1gInSc1	Chesham
(	(	kIx(	(
<g/>
1793	[number]	k4	1793
<g/>
–	–	k?	–
<g/>
1863	[number]	k4	1863
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
1858	[number]	k4	1858
baron	baron	k1gMnSc1	baron
CheshamHenry	CheshamHenra	k1gFnSc2	CheshamHenra
Manners	Mannersa	k1gFnPc2	Mannersa
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
baron	baron	k1gMnSc1	baron
Waterpark	Waterpark	k1gInSc1	Waterpark
(	(	kIx(	(
<g/>
1793	[number]	k4	1793
<g/>
–	–	k?	–
<g/>
1863	[number]	k4	1863
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
dvořanWilliam	dvořanWilliam	k6eAd1	dvořanWilliam
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
(	(	kIx(	(
<g/>
1808	[number]	k4	1808
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
William	William	k1gInSc1	William
Cavendish	Cavendisha	k1gFnPc2	Cavendisha
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
baron	baron	k1gMnSc1	baron
Chesham	Chesham	k1gInSc1	Chesham
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
<g/>
-	-	kIx~	-
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Královské	královský	k2eAgFnSc2d1	královská
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
společnostiSpencer	společnostiSpencer	k1gMnSc1	společnostiSpencer
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshiru	Devonshir	k1gInSc2	Devonshir
(	(	kIx(	(
<g/>
1833	[number]	k4	1833
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
pro	pro	k7c4	pro
Indii	Indie	k1gFnSc4	Indie
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Tajné	tajný	k1gMnPc4	tajný
radyLord	radyLord	k1gMnSc1	radyLord
Frederick	Frederick	k1gMnSc1	Frederick
Charles	Charles	k1gMnSc1	Charles
Cavendish	Cavendish	k1gMnSc1	Cavendish
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
-	-	kIx~	-
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
pro	pro	k7c4	pro
Irsko	Irsko	k1gNnSc4	Irsko
<g/>
,	,	kIx,	,
zavražděn	zavražděn	k2eAgInSc4d1	zavražděn
v	v	k7c4	v
DublinuLord	DublinuLord	k1gInSc4	DublinuLord
Edward	Edward	k1gMnSc1	Edward
Cavendish	Cavendish	k1gMnSc1	Cavendish
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
–	–	k?	–
<g/>
1891	[number]	k4	1891
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Dolní	dolní	k2eAgMnSc1d1	dolní
sněmovnyCharles	sněmovnyCharles	k1gMnSc1	sněmovnyCharles
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
baron	baron	k1gMnSc1	baron
Chesham	Chesham	k1gInSc1	Chesham
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
–	–	k?	–
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
lovčíVictor	lovčíVictor	k1gMnSc1	lovčíVictor
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshiru	Devonshir	k1gInSc2	Devonshir
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
koloniíEvelyn	koloniíEvelyn	k1gMnSc1	koloniíEvelyn
Cavendishová	Cavendishová	k1gFnSc1	Cavendishová
<g/>
,	,	kIx,	,
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
hofmistryně	hofmistryně	k1gFnSc2	hofmistryně
královny	královna	k1gFnSc2	královna
MaryEdward	MaryEdward	k1gMnSc1	MaryEdward
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshiru	Devonshir	k1gInSc2	Devonshir
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konzervativní	konzervativní	k2eAgInSc1d1	konzervativní
<g />
.	.	kIx.	.
</s>
<s>
politikJohn	politikJohn	k1gMnSc1	politikJohn
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
baron	baron	k1gMnSc1	baron
Chesham	Chesham	k1gInSc1	Chesham
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konzervativní	konzervativní	k2eAgMnSc1d1	konzervativní
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
dvořanAndrew	dvořanAndrew	k?	dvořanAndrew
Cavendish	Cavendish	k1gInSc1	Cavendish
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Devonshire	Devonshir	k1gInSc5	Devonshir
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konzervativní	konzervativní	k2eAgMnSc1d1	konzervativní
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
pro	pro	k7c4	pro
záležitosti	záležitost	k1gFnPc4	záležitost
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Chatsworth	Chatsworth	k1gInSc1	Chatsworth
House	house	k1gNnSc1	house
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
(	(	kIx(	(
<g/>
reprint	reprint	k1gInSc1	reprint
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
246	[number]	k4	246
<g/>
–	–	k?	–
<g/>
247	[number]	k4	247
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
7203	[number]	k4	7203
<g/>
–	–	k?	–
<g/>
133	[number]	k4	133
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
7	[number]	k4	7
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
(	(	kIx(	(
<g/>
reprint	reprint	k1gInSc1	reprint
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
440	[number]	k4	440
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
7203	[number]	k4	7203
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
<g/>
Historic	Historice	k1gInPc2	Historice
houses	housesa	k1gFnPc2	housesa
and	and	k?	and
gardens	gardensa	k1gFnPc2	gardensa
<g/>
,	,	kIx,	,
Banbury	Banbura	k1gFnSc2	Banbura
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
;	;	kIx,	;
s.	s.	k?	s.
178	[number]	k4	178
<g/>
,	,	kIx,	,
181	[number]	k4	181
<g/>
,	,	kIx,	,
252	[number]	k4	252
<g/>
,	,	kIx,	,
378	[number]	k4	378
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
9531426	[number]	k4	9531426
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Rodokmen	rodokmen	k1gInSc1	rodokmen
vévodů	vévoda	k1gMnPc2	vévoda
z	z	k7c2	z
DevonshireRodokmen	DevonshireRodokmen	k1gInSc1	DevonshireRodokmen
vévodů	vévoda	k1gMnPc2	vévoda
z	z	k7c2	z
NewcastleRodokmen	NewcastleRodokmen	k1gInSc1	NewcastleRodokmen
baronů	baron	k1gMnPc2	baron
z	z	k7c2	z
CheshamuOficiální	CheshamuOficiální	k2eAgFnSc1d1	CheshamuOficiální
web	web	k1gInSc4	web
zámku	zámek	k1gInSc2	zámek
Chatsworth	Chatswortha	k1gFnPc2	Chatswortha
</s>
</p>
