<s>
Kanton	Kanton	k1gInSc1
Bidache	Bidach	k1gFnSc2
</s>
<s>
Kanton	Kanton	k1gInSc1
Bidache	Bidache	k1gInSc1
Kanton	Kanton	k1gInSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
arrondissementu	arrondissement	k1gInSc2
Bayonne	Bayonn	k1gMnSc5
Stát	stát	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
Region	region	k1gInSc1
</s>
<s>
Akvitánie	Akvitánie	k1gFnSc1
Departement	departement	k1gInSc1
</s>
<s>
Pyrénées-Atlantiques	Pyrénées-Atlantiques	k1gMnSc1
Arrondissement	Arrondissement	k1gMnSc1
</s>
<s>
Bayonne	Bayonnout	k5eAaImIp3nS,k5eAaPmIp3nS
Počet	počet	k1gInSc1
obcí	obec	k1gFnPc2
</s>
<s>
7	#num#	k4
Sídlo	sídlo	k1gNnSc4
správy	správa	k1gFnSc2
</s>
<s>
Bidache	Bidache	k1gFnSc1
Rozloha	rozloha	k1gFnSc1
</s>
<s>
161,64	161,64	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
5	#num#	k4
535	#num#	k4
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
34,2	34,2	k4
ob.	ob.	k?
<g/>
/	/	kIx~
<g/>
km²	km²	k?
</s>
<s>
Kanton	Kanton	k1gInSc1
Bidache	Bidache	k1gFnSc1
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
Canton	Canton	k1gInSc1
de	de	k7
Bidache	Bidache	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
francouzský	francouzský	k2eAgInSc4d1
kanton	kanton	k1gInSc1
v	v	k7c6
departementu	departement	k1gInSc6
Pyrénées-Atlantiques	 Pyrénées-Atlantiques		k1gFnPc2
v	v	k7c6
regionu	region	k1gInSc6
Akvitánie	Akvitánie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
ho	on	k3xPp3gInSc4
sedm	sedm	k4xCc4
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Obce	obec	k1gFnPc1
kantonu	kanton	k1gInSc2
</s>
<s>
Arancou	Arancou	k6eAd1
</s>
<s>
Bardos	Bardos	k1gMnSc1
</s>
<s>
Bergouey-Viellenave	Bergouey-Viellenavat	k5eAaPmIp3nS
</s>
<s>
Bidache	Bidache	k6eAd1
</s>
<s>
Came	Came	k6eAd1
</s>
<s>
Guiche	Guiche	k6eAd1
</s>
<s>
Sames	Sames	k1gMnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kantony	Kanton	k1gInPc1
v	v	k7c6
departementu	departement	k1gInSc6
Pyrénées-Atlantiques	Pyrénées-Atlantiquesa	k1gFnPc2
</s>
<s>
Accous	Accous	k1gMnSc1
•	•	k?
Anglet-Nord	Anglet-Nord	k1gMnSc1
•	•	k?
Anglet-Sud	Anglet-Sud	k1gMnSc1
•	•	k?
Aramits	Aramits	k1gInSc1
•	•	k?
Arthez-de-Béarn	Arthez-de-Béarn	k1gInSc1
•	•	k?
Arudy	Aruda	k1gFnSc2
•	•	k?
Arzacq-Arraziguet	Arzacq-Arraziguet	k1gInSc1
•	•	k?
La	la	k1gNnSc7
Bastide-Clairence	Bastide-Clairence	k1gFnSc2
•	•	k?
Bayonne-Est	Bayonne-Est	k1gMnSc1
•	•	k?
Bayonne-Nord	Bayonne-Nord	k1gMnSc1
•	•	k?
Bayonne-Ouest	Bayonne-Ouest	k1gMnSc1
•	•	k?
Biarritz-Est	Biarritz-Est	k1gMnSc1
•	•	k?
Biarritz-Ouest	Biarritz-Ouest	k1gMnSc1
•	•	k?
Bidache	Bidache	k1gInSc1
•	•	k?
Billè	Billè	k1gInSc5
•	•	k?
Espelette	Espelett	k1gInSc5
•	•	k?
Garlin	Garlin	k2eAgInSc1d1
•	•	k?
Hasparren	Hasparrno	k1gNnPc2
•	•	k?
Hendaye	Henday	k1gFnSc2
•	•	k?
Iholdy	Iholda	k1gFnSc2
•	•	k?
Jurançon	Jurançon	k1gMnSc1
•	•	k?
Lagor	Lagor	k1gMnSc1
•	•	k?
Laruns	Laruns	k1gInSc1
•	•	k?
Lasseube	Lasseub	k1gInSc5
•	•	k?
Lembeye	Lembeye	k1gNnSc6
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Lescar	Lescar	k1gMnSc1
•	•	k?
Mauléon-Licharre	Mauléon-Licharr	k1gInSc5
•	•	k?
Monein	Monein	k2eAgInSc1d1
•	•	k?
Montaner	Montaner	k1gInSc1
•	•	k?
Morlaà	Morlaà	k1gInSc1
•	•	k?
Navarrenx	Navarrenx	k1gInSc1
•	•	k?
Nay-Est	Nay-Est	k1gMnSc1
•	•	k?
Nay-Ouest	Nay-Ouest	k1gMnSc1
•	•	k?
Oloron-Sainte-Marie-Est	Oloron-Sainte-Marie-Est	k1gMnSc1
•	•	k?
Oloron-Sainte-Marie-Ouest	Oloron-Sainte-Marie-Ouest	k1gMnSc1
•	•	k?
Orthez	Orthez	k1gMnSc1
•	•	k?
Pau-Centre	Pau-Centr	k1gInSc5
•	•	k?
Pau-Est	Pau-Est	k1gMnSc1
•	•	k?
Pau-Nord	Pau-Nord	k1gMnSc1
•	•	k?
Pau-Ouest	Pau-Ouest	k1gMnSc1
•	•	k?
Pau-Sud	Pau-Sud	k1gMnSc1
•	•	k?
Pontacq	Pontacq	k1gMnSc1
•	•	k?
Saint-Étienne-de-Baï	Saint-Étienne-de-Baï	k1gFnSc2
•	•	k?
Saint-Jean-de-Luz	Saint-Jean-de-Luz	k1gInSc1
•	•	k?
Saint-Jean-Pied-de-Port	Saint-Jean-Pied-de-Port	k1gInSc1
•	•	k?
Saint-Palais	Saint-Palais	k1gInSc1
•	•	k?
Saint-Pierre-d	Saint-Pierre-d	k1gInSc4
<g/>
'	'	kIx"
<g/>
Irube	Irub	k1gMnSc5
•	•	k?
Salies-de-Béarn	Salies-de-Béarn	k1gMnSc1
•	•	k?
Sauveterre-de-Béarn	Sauveterre-de-Béarn	k1gMnSc1
•	•	k?
Tardets-Sorholus	Tardets-Sorholus	k1gMnSc1
•	•	k?
Thè	Thè	k1gFnSc2
•	•	k?
Ustaritz	Ustaritz	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
89148570761924312772	#num#	k4
</s>
