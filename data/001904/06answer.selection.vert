<s>
Pokud	pokud	k8xS	pokud
budeme	být	k5eAaImBp1nP	být
houby	houby	k6eAd1	houby
dělit	dělit	k5eAaImF	dělit
dle	dle	k7c2	dle
způsobu	způsob	k1gInSc2	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
získávají	získávat	k5eAaImIp3nP	získávat
živiny	živina	k1gFnPc1	živina
<g/>
,	,	kIx,	,
dostáváme	dostávat	k5eAaImIp1nP	dostávat
dvě	dva	k4xCgFnPc1	dva
základní	základní	k2eAgFnPc1d1	základní
skupiny	skupina	k1gFnPc1	skupina
hub	houba	k1gFnPc2	houba
-	-	kIx~	-
saprofytické	saprofytický	k2eAgFnSc2d1	saprofytická
(	(	kIx(	(
<g/>
hniložijné	hniložijný	k2eAgFnSc2d1	hniložijný
<g/>
)	)	kIx)	)
a	a	k8xC	a
parazitické	parazitický	k2eAgFnPc1d1	parazitická
(	(	kIx(	(
<g/>
příživné	příživný	k2eAgFnPc1d1	příživná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
