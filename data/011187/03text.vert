<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Zamy	Zama	k1gFnSc2	Zama
byla	být	k5eAaImAgFnS	být
svedena	sveden	k2eAgFnSc1d1	svedena
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
202	[number]	k4	202
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Byla	být	k5eAaImAgFnS	být
poslední	poslední	k2eAgFnSc7d1	poslední
a	a	k8xC	a
rozhodující	rozhodující	k2eAgFnSc7d1	rozhodující
bitvou	bitva	k1gFnSc7	bitva
druhé	druhý	k4xOgFnSc2	druhý
punské	punský	k2eAgFnSc2d1	punská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Římská	římský	k2eAgFnSc1d1	římská
armáda	armáda	k1gFnSc1	armáda
vedená	vedený	k2eAgFnSc1d1	vedená
Publiem	Publium	k1gNnSc7	Publium
Corneliem	Cornelium	k1gNnSc7	Cornelium
Scipionem	Scipion	k1gInSc7	Scipion
(	(	kIx(	(
<g/>
Africanem	African	k1gInSc7	African
<g/>
)	)	kIx)	)
porazila	porazit	k5eAaPmAgFnS	porazit
kartaginské	kartaginský	k2eAgNnSc4d1	kartaginské
vojsko	vojsko	k1gNnSc4	vojsko
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Hannibala	Hannibal	k1gMnSc2	Hannibal
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
porážce	porážka	k1gFnSc6	porážka
požádal	požádat	k5eAaPmAgInS	požádat
kartaginský	kartaginský	k2eAgInSc1d1	kartaginský
senát	senát	k1gInSc1	senát
o	o	k7c4	o
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Před	před	k7c7	před
bitvou	bitva	k1gFnSc7	bitva
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
Hannibala	Hannibal	k1gMnSc2	Hannibal
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
203	[number]	k4	203
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
doufali	doufat	k5eAaImAgMnP	doufat
Kartaginci	Kartaginec	k1gMnPc1	Kartaginec
ve	v	k7c4	v
zmírnění	zmírnění	k1gNnSc4	zmírnění
tvrdých	tvrdý	k2eAgFnPc2d1	tvrdá
mírových	mírový	k2eAgFnPc2d1	mírová
podmínek	podmínka	k1gFnPc2	podmínka
stanovených	stanovený	k2eAgFnPc2d1	stanovená
Římany	Říman	k1gMnPc7	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Hannibal	Hannibal	k1gInSc1	Hannibal
vyslal	vyslat	k5eAaPmAgMnS	vyslat
vyjednávače	vyjednávač	k1gMnSc4	vyjednávač
do	do	k7c2	do
Scipionova	Scipionův	k2eAgInSc2d1	Scipionův
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ale	ale	k9	ale
svůj	svůj	k3xOyFgInSc4	svůj
úkol	úkol	k1gInSc4	úkol
nesplnili	splnit	k5eNaPmAgMnP	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Scipio	Scipio	k6eAd1	Scipio
je	být	k5eAaImIp3nS	být
přesto	přesto	k8xC	přesto
nechal	nechat	k5eAaPmAgMnS	nechat
volně	volně	k6eAd1	volně
odejít	odejít	k5eAaPmF	odejít
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
si	se	k3xPyFc3	se
všimli	všimnout	k5eAaPmAgMnP	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Římané	Říman	k1gMnPc1	Říman
neoplývají	oplývat	k5eNaImIp3nP	oplývat
příliš	příliš	k6eAd1	příliš
silnou	silný	k2eAgFnSc7d1	silná
jízdou	jízda	k1gFnSc7	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Hannibal	Hannibal	k1gInSc1	Hannibal
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
odhodlal	odhodlat	k5eAaPmAgInS	odhodlat
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Scipiona	Scipiona	k1gFnSc1	Scipiona
ještě	ještě	k6eAd1	ještě
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
bitvou	bitva	k1gFnSc7	bitva
posílil	posílit	k5eAaPmAgInS	posílit
numidský	numidský	k2eAgInSc1d1	numidský
náčelník	náčelník	k1gInSc1	náčelník
Massinissa	Massinissa	k1gFnSc1	Massinissa
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
6000	[number]	k4	6000
jezdci	jezdec	k1gInPc7	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Massinissa	Massinissa	k1gFnSc1	Massinissa
sice	sice	k8xC	sice
bojoval	bojovat	k5eAaImAgMnS	bojovat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
války	válka	k1gFnSc2	válka
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Kartaginců	Kartaginec	k1gMnPc2	Kartaginec
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
kvůli	kvůli	k7c3	kvůli
svatbě	svatba	k1gFnSc3	svatba
svého	svůj	k3xOyFgMnSc2	svůj
konkurenta	konkurent	k1gMnSc2	konkurent
<g/>
,	,	kIx,	,
numidského	numidský	k2eAgMnSc2d1	numidský
krále	král	k1gMnSc2	král
Syfaka	Syfak	k1gMnSc2	Syfak
<g/>
,	,	kIx,	,
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
Hasdrubala	Hasdrubala	k1gFnSc1	Hasdrubala
Giscona	Giscona	k1gFnSc1	Giscona
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
přislíbena	přislíbit	k5eAaPmNgFnS	přislíbit
právě	právě	k6eAd1	právě
Massinissovi	Massiniss	k1gMnSc3	Massiniss
<g/>
,	,	kIx,	,
ochotně	ochotně	k6eAd1	ochotně
změnil	změnit	k5eAaPmAgMnS	změnit
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
přidal	přidat	k5eAaPmAgMnS	přidat
se	se	k3xPyFc4	se
k	k	k7c3	k
Římanům	Říman	k1gMnPc3	Říman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc4	průběh
bitvy	bitva	k1gFnSc2	bitva
==	==	k?	==
</s>
</p>
<p>
<s>
Zama	Zama	k1gFnSc1	Zama
byla	být	k5eAaImAgFnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
bitvou	bitva	k1gFnSc7	bitva
druhé	druhý	k4xOgFnSc2	druhý
punské	punský	k2eAgFnSc2d1	punská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
Římané	Říman	k1gMnPc1	Říman
postavili	postavit	k5eAaPmAgMnP	postavit
do	do	k7c2	do
pole	pole	k1gNnSc2	pole
méně	málo	k6eAd2	málo
pěchoty	pěchota	k1gFnSc2	pěchota
než	než	k8xS	než
Hannibal	Hannibal	k1gInSc1	Hannibal
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
římská	římský	k2eAgFnSc1d1	římská
a	a	k8xC	a
numidská	numidský	k2eAgFnSc1d1	numidská
jízda	jízda	k1gFnSc1	jízda
početně	početně	k6eAd1	početně
převyšovala	převyšovat	k5eAaImAgFnS	převyšovat
kartaginskou	kartaginský	k2eAgFnSc7d1	kartaginská
<g/>
.	.	kIx.	.
</s>
<s>
Hannibal	Hannibal	k1gInSc1	Hannibal
umístil	umístit	k5eAaPmAgInS	umístit
do	do	k7c2	do
první	první	k4xOgFnSc2	první
linie	linie	k1gFnSc2	linie
kolem	kolem	k7c2	kolem
80	[number]	k4	80
válečných	válečný	k2eAgMnPc2d1	válečný
slonů	slon	k1gMnPc2	slon
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ale	ale	k9	ale
byli	být	k5eAaImAgMnP	být
málo	málo	k6eAd1	málo
trénovaní	trénovaný	k2eAgMnPc1d1	trénovaný
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
pěchota	pěchota	k1gFnSc1	pěchota
byla	být	k5eAaImAgFnS	být
odstupňována	odstupňovat	k5eAaPmNgFnS	odstupňovat
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
řad	řada	k1gFnPc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
dvě	dva	k4xCgFnPc1	dva
linie	linie	k1gFnPc1	linie
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
římské	římský	k2eAgMnPc4d1	římský
legionáře	legionář	k1gMnPc4	legionář
nejprve	nejprve	k6eAd1	nejprve
vyčerpat	vyčerpat	k5eAaPmF	vyčerpat
a	a	k8xC	a
uvést	uvést	k5eAaPmF	uvést
v	v	k7c4	v
nepořádek	nepořádek	k1gInSc4	nepořádek
<g/>
.	.	kIx.	.
</s>
<s>
Veteráni	veterán	k1gMnPc1	veterán
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
provázeli	provázet	k5eAaImAgMnP	provázet
Hannibala	Hannibal	k1gMnSc4	Hannibal
během	během	k7c2	během
jeho	jeho	k3xOp3gNnSc2	jeho
tažení	tažení	k1gNnSc2	tažení
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
poslední	poslední	k2eAgFnSc4d1	poslední
řadu	řada	k1gFnSc4	řada
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
přivodit	přivodit	k5eAaBmF	přivodit
vlastní	vlastní	k2eAgNnSc4d1	vlastní
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
překvapivým	překvapivý	k2eAgInSc7d1	překvapivý
Scipionovým	Scipionový	k2eAgInSc7d1	Scipionový
manévrem	manévr	k1gInSc7	manévr
<g/>
:	:	kIx,	:
Na	na	k7c4	na
místo	místo	k1gNnSc4	místo
obvyklého	obvyklý	k2eAgNnSc2d1	obvyklé
šachovnicového	šachovnicový	k2eAgNnSc2d1	šachovnicové
uspořádání	uspořádání	k1gNnSc2	uspořádání
byly	být	k5eAaImAgInP	být
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
manipuly	manipul	k1gInPc1	manipul
seřazeny	seřadit	k5eAaPmNgInP	seřadit
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
kolmo	kolmo	k6eAd1	kolmo
k	k	k7c3	k
bitevní	bitevní	k2eAgFnSc3d1	bitevní
linii	linie	k1gFnSc3	linie
značné	značný	k2eAgFnSc2d1	značná
mezery	mezera	k1gFnSc2	mezera
<g/>
.	.	kIx.	.
</s>
<s>
Těmi	ten	k3xDgInPc7	ten
později	pozdě	k6eAd2	pozdě
prošla	projít	k5eAaPmAgFnS	projít
většina	většina	k1gFnSc1	většina
válečných	válečný	k2eAgMnPc2d1	válečný
slonů	slon	k1gMnPc2	slon
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
vyplašení	vyplašení	k1gNnSc2	vyplašení
strašlivým	strašlivý	k2eAgInSc7d1	strašlivý
rámusem	rámus	k1gInSc7	rámus
a	a	k8xC	a
řevem	řev	k1gInSc7	řev
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
jim	on	k3xPp3gMnPc3	on
přitom	přitom	k6eAd1	přitom
způsobili	způsobit	k5eAaPmAgMnP	způsobit
vážnější	vážní	k2eAgFnPc4d2	vážnější
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Spousta	spousta	k1gFnSc1	spousta
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
kvůli	kvůli	k7c3	kvůli
již	již	k6eAd1	již
zmiňovanému	zmiňovaný	k2eAgInSc3d1	zmiňovaný
hluku	hluk	k1gInSc3	hluk
ani	ani	k8xC	ani
k	k	k7c3	k
římským	římský	k2eAgFnPc3d1	římská
liniím	linie	k1gFnPc3	linie
nedorazila	dorazit	k5eNaPmAgFnS	dorazit
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
toho	ten	k3xDgInSc2	ten
se	se	k3xPyFc4	se
poděšení	poděšený	k2eAgMnPc1d1	poděšený
sloni	slon	k1gMnPc1	slon
obrátili	obrátit	k5eAaPmAgMnP	obrátit
proti	proti	k7c3	proti
vlastní	vlastní	k2eAgFnSc3d1	vlastní
jízdě	jízda	k1gFnSc3	jízda
na	na	k7c6	na
křídlech	křídlo	k1gNnPc6	křídlo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
to	ten	k3xDgNnSc4	ten
byli	být	k5eAaImAgMnP	být
nakonec	nakonec	k6eAd1	nakonec
Kartaginci	Kartaginec	k1gMnPc1	Kartaginec
<g/>
,	,	kIx,	,
komu	kdo	k3yInSc3	kdo
způsobili	způsobit	k5eAaPmAgMnP	způsobit
sloni	slon	k1gMnPc1	slon
větší	veliký	k2eAgFnSc2d2	veliký
ztráty	ztráta	k1gFnSc2	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
Římané	Říman	k1gMnPc1	Říman
vypořádali	vypořádat	k5eAaPmAgMnP	vypořádat
s	s	k7c7	s
počátečním	počáteční	k2eAgInSc7d1	počáteční
útokem	útok	k1gInSc7	útok
slonů	slon	k1gMnPc2	slon
a	a	k8xC	a
kartaginské	kartaginský	k2eAgFnSc2d1	kartaginská
lehké	lehký	k2eAgFnSc2d1	lehká
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
střetu	střet	k1gInSc3	střet
hlavních	hlavní	k2eAgInPc2d1	hlavní
uskupení	uskupení	k1gNnSc2	uskupení
obou	dva	k4xCgNnPc2	dva
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Římská	římský	k2eAgFnSc1d1	římská
a	a	k8xC	a
spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
kavalérie	kavalérie	k1gFnSc1	kavalérie
získala	získat	k5eAaPmAgFnS	získat
převahu	převaha	k1gFnSc4	převaha
nad	nad	k7c7	nad
svým	svůj	k3xOyFgMnSc7	svůj
protivníkem	protivník	k1gMnSc7	protivník
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
jej	on	k3xPp3gNnSc4	on
přemohla	přemoct	k5eAaPmAgFnS	přemoct
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
místo	místo	k6eAd1	místo
aby	aby	kYmCp3nS	aby
využila	využít	k5eAaPmAgFnS	využít
svého	svůj	k3xOyFgNnSc2	svůj
vítězství	vítězství	k1gNnSc2	vítězství
a	a	k8xC	a
napadla	napadnout	k5eAaPmAgFnS	napadnout
nyní	nyní	k6eAd1	nyní
nechráněnou	chráněný	k2eNgFnSc4d1	nechráněná
kartaginskou	kartaginský	k2eAgFnSc4d1	kartaginská
pěchotu	pěchota	k1gFnSc4	pěchota
<g/>
,	,	kIx,	,
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
pronásledování	pronásledování	k1gNnSc6	pronásledování
prchající	prchající	k2eAgFnSc2d1	prchající
nepřátelské	přátelský	k2eNgFnSc2d1	nepřátelská
jízdy	jízda	k1gFnSc2	jízda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hannibalova	Hannibalův	k2eAgFnSc1d1	Hannibalova
pěchota	pěchota	k1gFnSc1	pěchota
dostala	dostat	k5eAaPmAgFnS	dostat
svého	svůj	k3xOyFgMnSc4	svůj
protivníka	protivník	k1gMnSc4	protivník
pod	pod	k7c4	pod
silný	silný	k2eAgInSc4d1	silný
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sice	sice	k8xC	sice
opotřeboval	opotřebovat	k5eAaPmAgInS	opotřebovat
první	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
linie	linie	k1gFnPc4	linie
jeho	jeho	k3xOp3gFnSc2	jeho
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
jeho	jeho	k3xOp3gInSc7	jeho
záměrem	záměr	k1gInSc7	záměr
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
totiž	totiž	k9	totiž
nasadil	nasadit	k5eAaPmAgMnS	nasadit
do	do	k7c2	do
boje	boj	k1gInSc2	boj
své	svůj	k3xOyFgMnPc4	svůj
čerstvé	čerstvý	k2eAgMnPc4d1	čerstvý
veterány	veterán	k1gMnPc4	veterán
ze	z	k7c2	z
třetí	třetí	k4xOgFnSc2	třetí
linie	linie	k1gFnSc2	linie
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
již	již	k6eAd1	již
silně	silně	k6eAd1	silně
unavené	unavený	k2eAgMnPc4d1	unavený
Římany	Říman	k1gMnPc4	Říman
dorazit	dorazit	k5eAaPmF	dorazit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
druhá	druhý	k4xOgFnSc1	druhý
linie	linie	k1gFnSc1	linie
začala	začít	k5eAaPmAgFnS	začít
povolovat	povolovat	k5eAaImF	povolovat
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
Hannibal	Hannibal	k1gInSc4	Hannibal
veteránům	veterán	k1gMnPc3	veterán
rozkaz	rozkaz	k1gInSc4	rozkaz
namířit	namířit	k5eAaPmF	namířit
svá	svůj	k3xOyFgNnPc4	svůj
kopí	kopí	k1gNnPc4	kopí
proti	proti	k7c3	proti
vlastním	vlastní	k2eAgMnPc3d1	vlastní
ustupujícím	ustupující	k2eAgMnPc3d1	ustupující
vojákům	voják	k1gMnPc3	voják
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
hrozilo	hrozit	k5eAaImAgNnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
stahující	stahující	k2eAgFnPc1d1	stahující
se	se	k3xPyFc4	se
jednotky	jednotka	k1gFnPc1	jednotka
uvedou	uvést	k5eAaPmIp3nP	uvést
ve	v	k7c4	v
zmatek	zmatek	k1gInSc4	zmatek
nastupující	nastupující	k2eAgFnSc4d1	nastupující
řadu	řada	k1gFnSc4	řada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
prchající	prchající	k2eAgFnPc1d1	prchající
jednotky	jednotka	k1gFnPc1	jednotka
pokusily	pokusit	k5eAaPmAgFnP	pokusit
uniknout	uniknout	k5eAaPmF	uniknout
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
a	a	k8xC	a
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
shromáždily	shromáždit	k5eAaPmAgFnP	shromáždit
po	po	k7c6	po
křídlech	křídlo	k1gNnPc6	křídlo
poslední	poslední	k2eAgFnSc2d1	poslední
linie	linie	k1gFnSc2	linie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
momentě	moment	k1gInSc6	moment
se	se	k3xPyFc4	se
Římané	Říman	k1gMnPc1	Říman
nacházeli	nacházet	k5eAaImAgMnP	nacházet
blízko	blízko	k7c2	blízko
porážky	porážka	k1gFnSc2	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Římské	římský	k2eAgNnSc1d1	římské
jezdectvo	jezdectvo	k1gNnSc1	jezdectvo
však	však	k9	však
právě	právě	k9	právě
nyní	nyní	k6eAd1	nyní
upustilo	upustit	k5eAaPmAgNnS	upustit
od	od	k7c2	od
dalšího	další	k2eAgNnSc2d1	další
stíhání	stíhání	k1gNnSc2	stíhání
protivníkovy	protivníkův	k2eAgFnSc2d1	protivníkova
jízdy	jízda	k1gFnSc2	jízda
a	a	k8xC	a
vpadlo	vpadnout	k5eAaPmAgNnS	vpadnout
Hannibalově	Hannibalův	k2eAgFnSc3d1	Hannibalova
pěchotě	pěchota	k1gFnSc3	pěchota
do	do	k7c2	do
zad	záda	k1gNnPc2	záda
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
narušilo	narušit	k5eAaPmAgNnS	narušit
její	její	k3xOp3gNnSc4	její
bitevní	bitevní	k2eAgNnSc4d1	bitevní
uspořádání	uspořádání	k1gNnSc4	uspořádání
a	a	k8xC	a
donutilo	donutit	k5eAaPmAgNnS	donutit
tak	tak	k6eAd1	tak
Kartagince	Kartaginec	k1gMnSc2	Kartaginec
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
následné	následný	k2eAgFnSc2d1	následná
řeže	řež	k1gFnSc2	řež
se	se	k3xPyFc4	se
Hannibalovi	Hannibal	k1gMnSc3	Hannibal
podařilo	podařit	k5eAaPmAgNnS	podařit
uprchnout	uprchnout	k5eAaPmF	uprchnout
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
tábora	tábor	k1gInSc2	tábor
v	v	k7c6	v
Hadrumetu	Hadrumet	k1gInSc6	Hadrumet
<g/>
.	.	kIx.	.
</s>
<s>
Pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
jednou	jednou	k6eAd1	jednou
nabídnout	nabídnout	k5eAaPmF	nabídnout
Římanům	Říman	k1gMnPc3	Říman
mír	mír	k1gInSc4	mír
a	a	k8xC	a
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kartágo	Kartágo	k1gNnSc1	Kartágo
se	se	k3xPyFc4	se
stáhne	stáhnout	k5eAaPmIp3nS	stáhnout
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
evropských	evropský	k2eAgNnPc2d1	Evropské
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Scipio	Scipio	k6eAd1	Scipio
už	už	k6eAd1	už
nyní	nyní	k6eAd1	nyní
nežádal	žádat	k5eNaImAgMnS	žádat
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgMnSc4d1	jiný
než	než	k8xS	než
úplnou	úplný	k2eAgFnSc4d1	úplná
kapitulaci	kapitulace	k1gFnSc4	kapitulace
Punů	Pun	k1gMnPc2	Pun
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
Hannibal	Hannibal	k1gInSc1	Hannibal
uprchl	uprchnout	k5eAaPmAgInS	uprchnout
do	do	k7c2	do
Kartága	Kartágo	k1gNnSc2	Kartágo
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgInS	vyzvat
senát	senát	k1gInSc1	senát
ke	k	k7c3	k
kapitulaci	kapitulace	k1gFnSc3	kapitulace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politické	politický	k2eAgInPc1d1	politický
důsledky	důsledek	k1gInPc1	důsledek
bitvy	bitva	k1gFnSc2	bitva
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
vítězství	vítězství	k1gNnSc6	vítězství
Scipio	Scipio	k6eAd1	Scipio
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
tříměsíční	tříměsíční	k2eAgInSc1d1	tříměsíční
klid	klid	k1gInSc1	klid
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
Kartaginci	Kartaginec	k1gMnSc3	Kartaginec
akceptováno	akceptován	k2eAgNnSc1d1	akceptováno
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgInSc4d1	římský
senát	senát	k1gInSc4	senát
poté	poté	k6eAd1	poté
Scipiona	Scipiona	k1gFnSc1	Scipiona
pověřil	pověřit	k5eAaPmAgInS	pověřit
konečným	konečný	k2eAgNnSc7d1	konečné
vyjednáváním	vyjednávání	k1gNnSc7	vyjednávání
s	s	k7c7	s
poraženým	poražený	k1gMnSc7	poražený
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Stanovené	stanovený	k2eAgFnPc1d1	stanovená
mírové	mírový	k2eAgFnPc1d1	mírová
podmínky	podmínka	k1gFnPc1	podmínka
oslabily	oslabit	k5eAaPmAgFnP	oslabit
Kartágo	Kartágo	k1gNnSc4	Kartágo
takovým	takový	k3xDgInSc7	takový
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
nadále	nadále	k6eAd1	nadále
nepředstavovalo	představovat	k5eNaImAgNnS	představovat
pro	pro	k7c4	pro
římskou	římský	k2eAgFnSc4d1	římská
hegemonii	hegemonie	k1gFnSc4	hegemonie
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
vážnou	vážný	k2eAgFnSc4d1	vážná
překážku	překážka	k1gFnSc4	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Vítězný	vítězný	k2eAgMnSc1d1	vítězný
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
nechal	nechat	k5eAaPmAgMnS	nechat
jako	jako	k9	jako
symbol	symbol	k1gInSc4	symbol
římského	římský	k2eAgNnSc2d1	římské
vítězství	vítězství	k1gNnSc2	vítězství
spálit	spálit	k5eAaPmF	spálit
téměř	téměř	k6eAd1	téměř
veškeré	veškerý	k3xTgNnSc4	veškerý
zbývající	zbývající	k2eAgNnSc4d1	zbývající
kartaginské	kartaginský	k2eAgNnSc4d1	kartaginské
loďstvo	loďstvo	k1gNnSc4	loďstvo
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgMnSc1d1	římský
spojenec	spojenec	k1gMnSc1	spojenec
Massinissa	Massiniss	k1gMnSc2	Massiniss
byl	být	k5eAaImAgInS	být
uznán	uznat	k5eAaPmNgInS	uznat
za	za	k7c4	za
krále	král	k1gMnPc4	král
Numidie	Numidie	k1gFnSc2	Numidie
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
obdařen	obdařen	k2eAgInSc1d1	obdařen
velkým	velký	k2eAgNnSc7d1	velké
územím	území	k1gNnSc7	území
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
pokořeného	pokořený	k2eAgNnSc2d1	pokořené
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
DUPUY	DUPUY	kA	DUPUY
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Ernest	Ernest	k1gMnSc1	Ernest
<g/>
,	,	kIx,	,
Historie	historie	k1gFnSc1	historie
vojenství	vojenství	k1gNnSc2	vojenství
<g/>
:	:	kIx,	:
Harperova	Harperův	k2eAgFnSc1d1	Harperova
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
3500	[number]	k4	3500
př	př	kA	př
<g/>
.	.	kIx.	.
Kr.	Kr.	k1gMnSc1	Kr.
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7213-000-5	[number]	k4	80-7213-000-5
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
punská	punský	k2eAgFnSc1d1	punská
válka	válka	k1gFnSc1	válka
</s>
</p>
<p>
<s>
Scipio	Scipio	k1gMnSc1	Scipio
Africanus	Africanus	k1gMnSc1	Africanus
</s>
</p>
<p>
<s>
Hannibal	Hannibal	k1gInSc1	Hannibal
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Zamy	Zama	k1gFnSc2	Zama
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Zama	Zam	k1gInSc2	Zam
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Staré	Staré	k2eAgInPc1d1	Staré
národy	národ	k1gInPc1	národ
|	|	kIx~	|
civilizace	civilizace	k1gFnSc1	civilizace
-	-	kIx~	-
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Zamy	Zama	k1gFnSc2	Zama
</s>
</p>
<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Zamy	Zama	k1gFnSc2	Zama
</s>
</p>
<p>
<s>
The	The	k?	The
Battle	Battle	k1gFnSc1	Battle
of	of	k?	of
Zama	Zama	k1gFnSc1	Zama
</s>
</p>
