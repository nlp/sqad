<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
(	(	kIx(	(
<g/>
zkracováno	zkracován	k2eAgNnSc1d1	zkracováno
NPP	NPP	kA	NPP
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
přírodní	přírodní	k2eAgInSc1d1	přírodní
útvar	útvar	k1gInSc1	útvar
menší	malý	k2eAgFnSc2d2	menší
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
geologický	geologický	k2eAgInSc1d1	geologický
či	či	k8xC	či
geomorfologický	geomorfologický	k2eAgInSc1d1	geomorfologický
útvar	útvar	k1gInSc1	útvar
<g/>
,	,	kIx,	,
naleziště	naleziště	k1gNnSc1	naleziště
nerostů	nerost	k1gInPc2	nerost
nebo	nebo	k8xC	nebo
vzácných	vzácný	k2eAgInPc2d1	vzácný
či	či	k8xC	či
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
ve	v	k7c6	v
fragmentech	fragment	k1gInPc6	fragment
ekosystémů	ekosystém	k1gInPc2	ekosystém
<g/>
,	,	kIx,	,
s	s	k7c7	s
národním	národní	k2eAgInSc7d1	národní
nebo	nebo	k8xC	nebo
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
ekologickým	ekologický	k2eAgInSc7d1	ekologický
<g/>
,	,	kIx,	,
vědeckým	vědecký	k2eAgInSc7d1	vědecký
či	či	k8xC	či
estetickým	estetický	k2eAgInSc7d1	estetický
významem	význam	k1gInSc7	význam
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
takový	takový	k3xDgMnSc1	takový
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vedle	vedle	k7c2	vedle
přírody	příroda	k1gFnSc2	příroda
formoval	formovat	k5eAaImAgMnS	formovat
svou	svůj	k3xOyFgFnSc4	svůj
činností	činnost	k1gFnSc7	činnost
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
V	v	k7c6	v
místním	místní	k2eAgNnSc6d1	místní
měřítku	měřítko	k1gNnSc6	měřítko
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc7	její
obdobou	obdoba	k1gFnSc7	obdoba
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chráněná	chráněný	k2eAgNnPc1d1	chráněné
území	území	k1gNnPc1	území
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
národní	národní	k2eAgFnPc4d1	národní
přírodní	přírodní	k2eAgFnPc4d1	přírodní
památky	památka	k1gFnPc4	památka
jsou	být	k5eAaImIp3nP	být
zařazovány	zařazovat	k5eAaImNgFnP	zařazovat
geologické	geologický	k2eAgFnPc1d1	geologická
nebo	nebo	k8xC	nebo
geomorfologické	geomorfologický	k2eAgInPc1d1	geomorfologický
útvary	útvar	k1gInPc1	útvar
(	(	kIx(	(
<g/>
jeskyně	jeskyně	k1gFnSc1	jeskyně
<g/>
,	,	kIx,	,
geologické	geologický	k2eAgInPc4d1	geologický
profily	profil	k1gInPc4	profil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naleziště	naleziště	k1gNnSc4	naleziště
vzácných	vzácný	k2eAgInPc2d1	vzácný
nerostů	nerost	k1gInPc2	nerost
nebo	nebo	k8xC	nebo
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
fauny	fauna	k1gFnSc2	fauna
a	a	k8xC	a
flóry	flóra	k1gFnSc2	flóra
v	v	k7c6	v
ekosystému	ekosystém	k1gInSc6	ekosystém
<g/>
,	,	kIx,	,
i	i	k8xC	i
útvary	útvar	k1gInPc1	útvar
zformované	zformovaný	k2eAgInPc1d1	zformovaný
lidskými	lidský	k2eAgFnPc7d1	lidská
aktivitami	aktivita	k1gFnPc7	aktivita
(	(	kIx(	(
<g/>
historické	historický	k2eAgFnPc1d1	historická
parkové	parkový	k2eAgFnPc1d1	parková
úpravy	úprava	k1gFnPc1	úprava
krajiny	krajina	k1gFnSc2	krajina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
národních	národní	k2eAgFnPc2d1	národní
přírodních	přírodní	k2eAgFnPc2d1	přírodní
památek	památka	k1gFnPc2	památka
je	být	k5eAaImIp3nS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
v	v	k7c6	v
paragrafu	paragraf	k1gInSc6	paragraf
35	[number]	k4	35
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
lednu	leden	k1gInSc3	leden
2018	[number]	k4	2018
existovalo	existovat	k5eAaImAgNnS	existovat
124	[number]	k4	124
území	území	k1gNnSc1	území
vyhlášených	vyhlášený	k2eAgInPc2d1	vyhlášený
jako	jako	k8xS	jako
národní	národní	k2eAgFnPc4d1	národní
přírodní	přírodní	k2eAgFnPc4d1	přírodní
památky	památka	k1gFnPc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
veškeré	veškerý	k3xTgFnPc4	veškerý
činnosti	činnost	k1gFnPc4	činnost
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
objekt	objekt	k1gInSc4	objekt
či	či	k8xC	či
území	území	k1gNnSc4	území
poškodit	poškodit	k5eAaPmF	poškodit
nebo	nebo	k8xC	nebo
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
zaručeno	zaručit	k5eAaPmNgNnS	zaručit
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
využití	využití	k1gNnSc1	využití
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
památky	památka	k1gFnSc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
děje	dít	k5eAaImIp3nS	dít
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vyhlašování	vyhlašování	k1gNnSc2	vyhlašování
==	==	k?	==
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnPc1d1	národní
přírodní	přírodní	k2eAgFnPc1d1	přírodní
památky	památka	k1gFnPc1	památka
jsou	být	k5eAaImIp3nP	být
vyhlašovány	vyhlašován	k2eAgFnPc1d1	vyhlašována
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
národních	národní	k2eAgFnPc2d1	národní
přírodních	přírodní	k2eAgFnPc2d1	přírodní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
rezervace	rezervace	k1gFnSc1	rezervace
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnPc1d1	národní
přírodní	přírodní	k2eAgFnPc1d1	přírodní
památky	památka	k1gFnPc1	památka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
</p>
