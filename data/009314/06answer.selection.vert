<s>
Národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
(	(	kIx(	(
<g/>
zkracováno	zkracován	k2eAgNnSc1d1	zkracováno
NPP	NPP	kA	NPP
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
přírodní	přírodní	k2eAgInSc1d1	přírodní
útvar	útvar	k1gInSc1	útvar
menší	malý	k2eAgFnSc2d2	menší
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
geologický	geologický	k2eAgInSc1d1	geologický
či	či	k8xC	či
geomorfologický	geomorfologický	k2eAgInSc1d1	geomorfologický
útvar	útvar	k1gInSc1	útvar
<g/>
,	,	kIx,	,
naleziště	naleziště	k1gNnSc1	naleziště
nerostů	nerost	k1gInPc2	nerost
nebo	nebo	k8xC	nebo
vzácných	vzácný	k2eAgInPc2d1	vzácný
či	či	k8xC	či
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
ve	v	k7c6	v
fragmentech	fragment	k1gInPc6	fragment
ekosystémů	ekosystém	k1gInPc2	ekosystém
<g/>
,	,	kIx,	,
s	s	k7c7	s
národním	národní	k2eAgInSc7d1	národní
nebo	nebo	k8xC	nebo
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
ekologickým	ekologický	k2eAgInSc7d1	ekologický
<g/>
,	,	kIx,	,
vědeckým	vědecký	k2eAgInSc7d1	vědecký
či	či	k8xC	či
estetickým	estetický	k2eAgInSc7d1	estetický
významem	význam	k1gInSc7	význam
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
takový	takový	k3xDgMnSc1	takový
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vedle	vedle	k7c2	vedle
přírody	příroda	k1gFnSc2	příroda
formoval	formovat	k5eAaImAgMnS	formovat
svou	svůj	k3xOyFgFnSc4	svůj
činností	činnost	k1gFnSc7	činnost
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
