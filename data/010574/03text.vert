<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
Hürth-Hermühlheim	Hürth-Hermühlheim	k1gInSc1	Hürth-Hermühlheim
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
německý	německý	k2eAgMnSc1d1	německý
automobilový	automobilový	k2eAgMnSc1d1	automobilový
závodník	závodník	k1gMnSc1	závodník
<g/>
,	,	kIx,	,
sedminásobný	sedminásobný	k2eAgMnSc1d1	sedminásobný
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
a	a	k8xC	a
statisticky	statisticky	k6eAd1	statisticky
nejúspěšnější	úspěšný	k2eAgMnSc1d3	nejúspěšnější
pilot	pilot	k1gMnSc1	pilot
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
tohoto	tento	k3xDgInSc2	tento
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Drží	držet	k5eAaImIp3nS	držet
mnoho	mnoho	k4c1	mnoho
rekordů	rekord	k1gInPc2	rekord
ve	v	k7c6	v
formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
nejvíce	hodně	k6eAd3	hodně
mistrovských	mistrovský	k2eAgInPc2d1	mistrovský
titulů	titul	k1gInPc2	titul
<g/>
,	,	kIx,	,
vítězství	vítězství	k1gNnPc2	vítězství
<g/>
,	,	kIx,	,
nejrychlejších	rychlý	k2eAgNnPc2d3	nejrychlejší
kol	kolo	k1gNnPc2	kolo
a	a	k8xC	a
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
počet	počet	k1gInSc1	počet
výher	výhra	k1gFnPc2	výhra
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
sezoně	sezona	k1gFnSc6	sezona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2002	[number]	k4	2002
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jediným	jediný	k2eAgMnSc7d1	jediný
jezdcem	jezdec	k1gMnSc7	jezdec
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
závodech	závod	k1gInPc6	závod
sezony	sezona	k1gFnSc2	sezona
na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
začátcích	začátek	k1gInPc6	začátek
na	na	k7c6	na
motokárách	motokára	k1gFnPc6	motokára
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
německé	německý	k2eAgInPc4d1	německý
šampionáty	šampionát	k1gInPc4	šampionát
Formule	formule	k1gFnSc2	formule
König	König	k1gInSc1	König
a	a	k8xC	a
Formule	formule	k1gFnSc1	formule
3	[number]	k4	3
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
závodil	závodit	k5eAaImAgMnS	závodit
za	za	k7c4	za
Mercedes	mercedes	k1gInSc4	mercedes
v	v	k7c6	v
Mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
sportovních	sportovní	k2eAgInPc2d1	sportovní
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Belgie	Belgie	k1gFnSc2	Belgie
1991	[number]	k4	1991
debutoval	debutovat	k5eAaBmAgMnS	debutovat
ve	v	k7c6	v
formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
Jordan	Jordan	k1gMnSc1	Jordan
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
stáje	stáj	k1gFnSc2	stáj
Benetton	Benetton	k1gInSc1	Benetton
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1994	[number]	k4	1994
a	a	k8xC	a
1995	[number]	k4	1995
stal	stát	k5eAaPmAgMnS	stát
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
týmu	tým	k1gInSc2	tým
Ferrari	Ferrari	k1gMnSc2	Ferrari
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
v	v	k7c6	v
letech	let	k1gInPc6	let
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
pětkrát	pětkrát	k6eAd1	pětkrát
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kariéru	kariéra	k1gFnSc4	kariéra
ukončil	ukončit	k5eAaPmAgMnS	ukončit
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
ve	v	k7c6	v
Scuderii	Scuderie	k1gFnSc6	Scuderie
Ferrari	Ferrari	k1gMnSc2	Ferrari
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
poradce	poradce	k1gMnSc1	poradce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2009	[number]	k4	2009
podepsal	podepsat	k5eAaPmAgMnS	podepsat
tříletou	tříletý	k2eAgFnSc4d1	tříletá
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
stájí	stáj	k1gFnSc7	stáj
Mercedes	mercedes	k1gInSc1	mercedes
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
definitivně	definitivně	k6eAd1	definitivně
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
kariéru	kariéra	k1gFnSc4	kariéra
protknulo	protknout	k5eAaPmAgNnS	protknout
několik	několik	k4yIc1	několik
kontroverzních	kontroverzní	k2eAgInPc2d1	kontroverzní
momentů	moment	k1gInPc2	moment
<g/>
,	,	kIx,	,
v	v	k7c6	v
závěrečných	závěrečný	k2eAgInPc6d1	závěrečný
závodech	závod	k1gInPc6	závod
dvakrát	dvakrát	k6eAd1	dvakrát
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
kolidoval	kolidovat	k5eAaImAgMnS	kolidovat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
soupeři	soupeř	k1gMnPc7	soupeř
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
s	s	k7c7	s
Damonem	Damon	k1gInSc7	Damon
Hillem	Hill	k1gInSc7	Hill
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
s	s	k7c7	s
Jacquesem	Jacques	k1gMnSc7	Jacques
Villeneuvem	Villeneuv	k1gMnSc7	Villeneuv
<g/>
,	,	kIx,	,
za	za	k7c4	za
druhý	druhý	k4xOgInSc4	druhý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
incidentů	incident	k1gInPc2	incident
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
ze	z	k7c2	z
šampionátu	šampionát	k1gInSc2	šampionát
(	(	kIx(	(
<g/>
body	bod	k1gInPc4	bod
a	a	k8xC	a
vítězství	vítězství	k1gNnSc4	vítězství
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
nicméně	nicméně	k8xC	nicméně
ponechány	ponechán	k2eAgFnPc1d1	ponechána
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
byl	být	k5eAaImAgMnS	být
potrestán	potrestat	k5eAaPmNgMnS	potrestat
za	za	k7c4	za
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
jízdu	jízda	k1gFnSc4	jízda
a	a	k8xC	a
týmovou	týmový	k2eAgFnSc4d1	týmová
režii	režie	k1gFnSc4	režie
(	(	kIx(	(
<g/>
trest	trest	k1gInSc4	trest
udělen	udělen	k2eAgInSc4d1	udělen
stáji	stáj	k1gFnSc3	stáj
Ferrari	ferrari	k1gNnSc1	ferrari
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
vedení	vedení	k1gNnSc1	vedení
týmovou	týmový	k2eAgFnSc4d1	týmová
režii	režie	k1gFnSc4	režie
řídilo	řídit	k5eAaImAgNnS	řídit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
vyslanec	vyslanec	k1gMnSc1	vyslanec
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
angažoval	angažovat	k5eAaBmAgInS	angažovat
se	se	k3xPyFc4	se
v	v	k7c6	v
četných	četný	k2eAgInPc6d1	četný
charitativních	charitativní	k2eAgInPc6d1	charitativní
projektech	projekt	k1gInPc6	projekt
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
daroval	darovat	k5eAaPmAgMnS	darovat
desítky	desítka	k1gFnPc1	desítka
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Corinnou	Corinný	k2eAgFnSc7d1	Corinný
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vážného	vážný	k2eAgInSc2d1	vážný
úrazu	úraz	k1gInSc2	úraz
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
při	při	k7c6	při
lyžování	lyžování	k1gNnSc6	lyžování
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
upoután	upoutat	k5eAaPmNgMnS	upoutat
na	na	k7c4	na
lůžko	lůžko	k1gNnSc4	lůžko
a	a	k8xC	a
nevystupuje	vystupovat	k5eNaImIp3nS	vystupovat
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
není	být	k5eNaImIp3nS	být
veřejně	veřejně	k6eAd1	veřejně
známý	známý	k2eAgMnSc1d1	známý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Ralf	Ralf	k1gMnSc1	Ralf
je	být	k5eAaImIp3nS	být
bývalým	bývalý	k2eAgMnSc7d1	bývalý
pilotem	pilot	k1gMnSc7	pilot
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Mick	Mick	k1gMnSc1	Mick
je	být	k5eAaImIp3nS	být
také	také	k9	také
automobilovým	automobilový	k2eAgMnSc7d1	automobilový
závodníkem	závodník	k1gMnSc7	závodník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Hürth-Hermühlheim	Hürth-Hermühlheim	k1gMnSc1	Hürth-Hermühlheim
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
vyučeného	vyučený	k2eAgMnSc4d1	vyučený
kamnáře	kamnář	k1gMnSc4	kamnář
a	a	k8xC	a
zedníka	zedník	k1gMnSc4	zedník
Rolfa	Rolf	k1gMnSc4	Rolf
Schumachera	Schumacher	k1gMnSc4	Schumacher
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
ženy	žena	k1gFnPc1	žena
Elisabeth	Elisabetha	k1gFnPc2	Elisabetha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
provozovala	provozovat	k5eAaImAgFnS	provozovat
u	u	k7c2	u
motokárové	motokárový	k2eAgFnSc2d1	motokárová
trati	trať	k1gFnSc2	trať
Kerpen-Horrem	Kerpen-Horrma	k1gFnPc2	Kerpen-Horrma
kavárnu	kavárna	k1gFnSc4	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
Ralf	Ralf	k1gMnSc1	Ralf
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
rovněž	rovněž	k9	rovněž
závodil	závodit	k5eAaImAgMnS	závodit
ve	v	k7c6	v
formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Reálku	reálka	k1gFnSc4	reálka
Otto	Otto	k1gMnSc1	Otto
Hahna	Hahn	k1gInSc2	Hahn
v	v	k7c4	v
Kerpenu	Kerpen	k2eAgFnSc4d1	Kerpen
ukončil	ukončit	k5eAaPmAgInS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
jako	jako	k9	jako
učeň	učeň	k1gMnSc1	učeň
v	v	k7c6	v
autoservisu	autoservis	k1gInSc6	autoservis
obchodního	obchodní	k2eAgMnSc2d1	obchodní
zástupce	zástupce	k1gMnSc2	zástupce
Volkswagenu	volkswagen	k1gInSc2	volkswagen
a	a	k8xC	a
BMW	BMW	kA	BMW
a	a	k8xC	a
automobilového	automobilový	k2eAgMnSc4d1	automobilový
závodníka	závodník	k1gMnSc4	závodník
Williho	Willi	k1gMnSc4	Willi
Bergmeistera	Bergmeister	k1gMnSc4	Bergmeister
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečné	závěrečný	k2eAgFnPc4d1	závěrečná
zkoušky	zkouška	k1gFnPc4	zkouška
složil	složit	k5eAaPmAgMnS	složit
kvůli	kvůli	k7c3	kvůli
závodění	závodění	k1gNnSc3	závodění
o	o	k7c6	o
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1989	[number]	k4	1989
napoprvé	napoprvé	k6eAd1	napoprvé
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Corinnou	Corinný	k2eAgFnSc4d1	Corinný
Schumacherovou	Schumacherová	k1gFnSc4	Schumacherová
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
Halver	Halver	k1gInSc1	Halver
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Betschová	Betschová	k1gFnSc1	Betschová
<g/>
)	)	kIx)	)
začal	začít	k5eAaPmAgInS	začít
chodit	chodit	k5eAaImF	chodit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1991	[number]	k4	1991
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
rozchodu	rozchod	k1gInSc6	rozchod
se	s	k7c7	s
Schumacherovým	Schumacherův	k2eAgMnSc7d1	Schumacherův
soupeřem	soupeř	k1gMnSc7	soupeř
Heinz-Haraldem	Heinz-Harald	k1gMnSc7	Heinz-Harald
Frentzenem	Frentzen	k1gMnSc7	Frentzen
<g/>
.	.	kIx.	.
</s>
<s>
Úřední	úřední	k2eAgFnSc1d1	úřední
svatba	svatba	k1gFnSc1	svatba
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1995	[number]	k4	1995
v	v	k7c4	v
Kerpenu	Kerpen	k2eAgFnSc4d1	Kerpen
<g/>
,	,	kIx,	,
církevní	církevní	k2eAgFnSc4d1	církevní
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Petersberg	Petersberg	k1gInSc4	Petersberg
nedaleko	nedaleko	k7c2	nedaleko
Bonnu	Bonn	k1gInSc2	Bonn
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
spolu	spolu	k6eAd1	spolu
dvě	dva	k4xCgFnPc1	dva
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Gina-Maria	Gina-Marium	k1gNnSc2	Gina-Marium
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1997	[number]	k4	1997
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
Mick	Mick	k1gMnSc1	Mick
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
až	až	k9	až
1996	[number]	k4	1996
bydlel	bydlet	k5eAaImAgMnS	bydlet
kvůli	kvůli	k7c3	kvůli
výhodným	výhodný	k2eAgFnPc3d1	výhodná
daním	daň	k1gFnPc3	daň
v	v	k7c6	v
Monte	Mont	k1gInSc5	Mont
Carlu	Carl	k1gInSc6	Carl
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
Vufflens-le-Château	Vufflense-Châteaus	k1gInSc2	Vufflens-le-Châteaus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
14	[number]	k4	14
<g/>
hektarovém	hektarový	k2eAgInSc6d1	hektarový
pozemku	pozemek	k1gInSc6	pozemek
v	v	k7c6	v
Glandu	Gland	k1gInSc6	Gland
u	u	k7c2	u
Ženevského	ženevský	k2eAgNnSc2d1	Ženevské
jezera	jezero	k1gNnSc2	jezero
v	v	k7c6	v
pětiposchoďovém	pětiposchoďový	k2eAgInSc6d1	pětiposchoďový
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
údajně	údajně	k6eAd1	údajně
stál	stát	k5eAaImAgInS	stát
jedenáct	jedenáct	k4xCc4	jedenáct
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Elisabeth	Elisabetha	k1gFnPc2	Elisabetha
Schumacherová	Schumacherová	k1gFnSc1	Schumacherová
zemřela	zemřít	k5eAaPmAgFnS	zemřít
během	během	k7c2	během
čekání	čekání	k1gNnSc2	čekání
na	na	k7c4	na
transplantaci	transplantace	k1gFnSc4	transplantace
jater	játra	k1gNnPc2	játra
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
konání	konání	k1gNnSc2	konání
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
San	San	k1gFnSc2	San
Marina	Marina	k1gFnSc1	Marina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
přesto	přesto	k6eAd1	přesto
oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
podle	podle	k7c2	podle
vyjádření	vyjádření	k1gNnSc2	vyjádření
M.	M.	kA	M.
Schumachera	Schumachero	k1gNnPc4	Schumachero
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
matka	matka	k1gFnSc1	matka
přála	přát	k5eAaImAgFnS	přát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
klubů	klub	k1gInPc2	klub
FC	FC	kA	FC
Aubonne	Aubonn	k1gInSc5	Aubonn
a	a	k8xC	a
FC	FC	kA	FC
Echichens	Echichens	k1gInSc1	Echichens
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
hrají	hrát	k5eAaImIp3nP	hrát
3	[number]	k4	3
<g/>
.	.	kIx.	.
švýcarskou	švýcarský	k2eAgFnSc4d1	švýcarská
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Úraz	úraz	k1gInSc1	úraz
na	na	k7c6	na
horách	hora	k1gFnPc6	hora
===	===	k?	===
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
při	při	k7c6	při
lyžování	lyžování	k1gNnSc6	lyžování
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
vážný	vážný	k2eAgInSc4d1	vážný
úraz	úraz	k1gInSc4	úraz
hlavy	hlava	k1gFnSc2	hlava
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
měl	mít	k5eAaImAgInS	mít
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
helmu	helma	k1gFnSc4	helma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
sjezdu	sjezd	k1gInSc2	sjezd
neupraveného	upravený	k2eNgInSc2d1	neupravený
svahu	svah	k1gInSc2	svah
narazil	narazit	k5eAaPmAgMnS	narazit
při	při	k7c6	při
pádu	pád	k1gInSc6	pád
hlavou	hlava	k1gFnSc7	hlava
do	do	k7c2	do
skály	skála	k1gFnSc2	skála
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
urychleně	urychleně	k6eAd1	urychleně
transportován	transportovat	k5eAaBmNgInS	transportovat
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
v	v	k7c4	v
Moutiers	Moutiers	k1gInSc4	Moutiers
a	a	k8xC	a
poté	poté	k6eAd1	poté
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
v	v	k7c6	v
Grenoblu	Grenoble	k1gInSc6	Grenoble
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
kritickém	kritický	k2eAgInSc6d1	kritický
stavu	stav	k1gInSc6	stav
v	v	k7c6	v
kómatu	kóma	k1gNnSc6	kóma
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
ve	v	k7c4	v
22	[number]	k4	22
hodin	hodina	k1gFnPc2	hodina
prodělal	prodělat	k5eAaPmAgMnS	prodělat
druhou	druhý	k4xOgFnSc4	druhý
operaci	operace	k1gFnSc4	operace
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
stabilizoval	stabilizovat	k5eAaBmAgInS	stabilizovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nadále	nadále	k6eAd1	nadále
zůstal	zůstat	k5eAaPmAgMnS	zůstat
kritický	kritický	k2eAgMnSc1d1	kritický
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
lékaři	lékař	k1gMnPc1	lékař
začali	začít	k5eAaPmAgMnP	začít
s	s	k7c7	s
postupným	postupný	k2eAgNnSc7d1	postupné
probouzením	probouzení	k1gNnSc7	probouzení
z	z	k7c2	z
kómatu	kóma	k1gNnSc2	kóma
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
oznámila	oznámit	k5eAaPmAgFnS	oznámit
světová	světový	k2eAgFnSc1d1	světová
média	médium	k1gNnPc1	médium
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
Schumacher	Schumachra	k1gFnPc2	Schumachra
opustil	opustit	k5eAaPmAgMnS	opustit
nemocnici	nemocnice	k1gFnSc4	nemocnice
v	v	k7c6	v
bdělém	bdělý	k2eAgInSc6d1	bdělý
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
podrobovat	podrobovat	k5eAaImF	podrobovat
rehabilitaci	rehabilitace	k1gFnSc4	rehabilitace
na	na	k7c6	na
klinice	klinika	k1gFnSc6	klinika
v	v	k7c6	v
Lausanne	Lausanne	k1gNnSc6	Lausanne
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
zprávu	zpráva	k1gFnSc4	zpráva
posléze	posléze	k6eAd1	posléze
v	v	k7c6	v
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
prohlášení	prohlášení	k1gNnSc6	prohlášení
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
i	i	k9	i
Schumacherova	Schumacherův	k2eAgFnSc1d1	Schumacherova
manažerka	manažerka	k1gFnSc1	manažerka
Sabine	Sabin	k1gInSc5	Sabin
Kehmová	Kehmová	k1gFnSc1	Kehmová
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
Schumacher	Schumachra	k1gFnPc2	Schumachra
převezen	převezen	k2eAgInSc1d1	převezen
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
jeho	jeho	k3xOp3gNnSc4	jeho
léčení	léčení	k1gNnSc4	léčení
a	a	k8xC	a
rehabilitace	rehabilitace	k1gFnPc4	rehabilitace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Začátky	začátek	k1gInPc1	začátek
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Motokáry	motokára	k1gFnSc2	motokára
===	===	k?	===
</s>
</p>
<p>
<s>
Otec	otec	k1gMnSc1	otec
Rolf	Rolf	k1gMnSc1	Rolf
ho	on	k3xPp3gMnSc4	on
k	k	k7c3	k
motoristickému	motoristický	k2eAgInSc3d1	motoristický
sportu	sport	k1gInSc3	sport
přivedl	přivést	k5eAaPmAgInS	přivést
již	již	k6eAd1	již
ve	v	k7c6	v
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
odměny	odměna	k1gFnSc2	odměna
za	za	k7c4	za
práce	práce	k1gFnPc4	práce
na	na	k7c4	na
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
motokárové	motokárový	k2eAgFnSc2d1	motokárová
dráhy	dráha	k1gFnSc2	dráha
Kerpen-Horrem	Kerpen-Horr	k1gMnSc7	Kerpen-Horr
získal	získat	k5eAaPmAgMnS	získat
členství	členství	k1gNnSc4	členství
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
novinový	novinový	k2eAgInSc4d1	novinový
článek	článek	k1gInSc4	článek
o	o	k7c6	o
Schumacherovi	Schumacher	k1gMnSc6	Schumacher
<g/>
,	,	kIx,	,
nazvaný	nazvaný	k2eAgMnSc1d1	nazvaný
Michael	Michael	k1gMnSc1	Michael
–	–	k?	–
klein	klein	k1gMnSc1	klein
<g/>
,	,	kIx,	,
aber	aber	k1gMnSc1	aber
schnell	schnell	k1gMnSc1	schnell
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Michael	Michael	k1gMnSc1	Michael
–	–	k?	–
malý	malý	k2eAgMnSc1d1	malý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
)	)	kIx)	)
vyšel	vyjít	k5eAaPmAgInS	vyjít
právě	právě	k9	právě
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
čtyřech	čtyři	k4xCgInPc6	čtyři
letech	let	k1gInPc6	let
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
asi	asi	k9	asi
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
členem	člen	k1gMnSc7	člen
motokárového	motokárový	k2eAgInSc2d1	motokárový
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Noviny	novina	k1gFnPc1	novina
jej	on	k3xPp3gNnSc2	on
zachytily	zachytit	k5eAaPmAgFnP	zachytit
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
trati	trať	k1gFnSc6	trať
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
po	po	k7c6	po
závodníkovi	závodník	k1gMnSc6	závodník
Wolfgangu	Wolfgang	k1gMnSc6	Wolfgang
von	von	k1gInSc1	von
Tripsovi	Tripsovi	k1gRnPc5	Tripsovi
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
motokára	motokára	k1gFnSc1	motokára
byla	být	k5eAaImAgFnS	být
podomácku	podomácku	k6eAd1	podomácku
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
z	z	k7c2	z
dílů	díl	k1gInPc2	díl
šlapacího	šlapací	k2eAgNnSc2d1	šlapací
autíčka	autíčko	k1gNnSc2	autíčko
Kettcar	Kettcara	k1gFnPc2	Kettcara
a	a	k8xC	a
motoru	motor	k1gInSc2	motor
z	z	k7c2	z
malého	malý	k2eAgInSc2d1	malý
mopedu	moped	k1gInSc2	moped
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
nádrž	nádrž	k1gFnSc1	nádrž
sloužila	sloužit	k5eAaImAgFnS	sloužit
plechovka	plechovka	k1gFnSc1	plechovka
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
vozítko	vozítko	k1gNnSc4	vozítko
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
dostal	dostat	k5eAaPmAgMnS	dostat
ke	k	k7c3	k
čtvrtým	čtvrtý	k4xOgFnPc3	čtvrtý
narozeninám	narozeniny	k1gFnPc3	narozeniny
<g/>
,	,	kIx,	,
dosahovalo	dosahovat	k5eAaImAgNnS	dosahovat
rychlosti	rychlost	k1gFnSc2	rychlost
až	až	k6eAd1	až
45	[number]	k4	45
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Závod	závod	k1gInSc1	závod
poprvé	poprvé	k6eAd1	poprvé
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
získal	získat	k5eAaPmAgMnS	získat
první	první	k4xOgNnSc4	první
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
juniorským	juniorský	k2eAgMnSc7d1	juniorský
mistrem	mistr	k1gMnSc7	mistr
kerpenského	kerpenský	k2eAgInSc2d1	kerpenský
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
letech	léto	k1gNnPc6	léto
chtěl	chtít	k5eAaImAgMnS	chtít
poprvé	poprvé	k6eAd1	poprvé
startovat	startovat	k5eAaBmF	startovat
v	v	k7c6	v
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
mistrovství	mistrovství	k1gNnSc6	mistrovství
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
německé	německý	k2eAgInPc1d1	německý
předpisy	předpis	k1gInPc1	předpis
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
start	start	k1gInSc1	start
až	až	k9	až
jezdcům	jezdec	k1gMnPc3	jezdec
starším	starý	k2eAgMnPc3d2	starší
čtrnácti	čtrnáct	k4xCc2	čtrnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
proto	proto	k8xC	proto
pro	pro	k7c4	pro
syna	syn	k1gMnSc4	syn
musel	muset	k5eAaImAgInS	muset
sehnat	sehnat	k5eAaPmF	sehnat
potřebná	potřebný	k2eAgNnPc4d1	potřebné
povolení	povolení	k1gNnPc4	povolení
<g/>
;	;	kIx,	;
nakonec	nakonec	k6eAd1	nakonec
startoval	startovat	k5eAaBmAgInS	startovat
pod	pod	k7c7	pod
lucemburskou	lucemburský	k2eAgFnSc7d1	Lucemburská
licencí	licence	k1gFnSc7	licence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
juniorské	juniorský	k2eAgNnSc4d1	juniorské
mistrovství	mistrovství	k1gNnSc4	mistrovství
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
titul	titul	k1gInSc1	titul
obhájil	obhájit	k5eAaPmAgInS	obhájit
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
vicemistra	vicemistr	k1gMnSc2	vicemistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
1986	[number]	k4	1986
si	se	k3xPyFc3	se
vyjel	vyjet	k5eAaPmAgMnS	vyjet
třetí	třetí	k4xOgNnPc4	třetí
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
seniorském	seniorský	k2eAgNnSc6d1	seniorské
mistrovství	mistrovství	k1gNnSc6	mistrovství
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Sezonou	sezona	k1gFnSc7	sezona
1987	[number]	k4	1987
zakončil	zakončit	k5eAaPmAgMnS	zakončit
své	svůj	k3xOyFgNnSc4	svůj
působení	působení	k1gNnSc4	působení
v	v	k7c6	v
motokárách	motokára	k1gFnPc6	motokára
tituly	titul	k1gInPc4	titul
mistra	mistr	k1gMnSc4	mistr
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
stanul	stanout	k5eAaPmAgMnS	stanout
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
jihoafrické	jihoafrický	k2eAgFnSc3d1	Jihoafrická
velké	velký	k2eAgFnSc3d1	velká
ceně	cena	k1gFnSc3	cena
motokár	motokára	k1gFnPc2	motokára
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Juniorské	juniorský	k2eAgFnSc2d1	juniorská
formule	formule	k1gFnSc2	formule
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
druhého	druhý	k4xOgNnSc2	druhý
mistrovství	mistrovství	k1gNnPc2	mistrovství
Německa	Německo	k1gNnSc2	Německo
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
König	Königa	k1gFnPc2	Königa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
devíti	devět	k4xCc6	devět
z	z	k7c2	z
deseti	deset	k4xCc2	deset
závodů	závod	k1gInPc2	závod
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
mistrem	mistr	k1gMnSc7	mistr
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Startoval	startovat	k5eAaBmAgMnS	startovat
také	také	k9	také
za	za	k7c4	za
tým	tým	k1gInSc4	tým
Eufra	Eufr	k1gInSc2	Eufr
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
závodech	závod	k1gInPc6	závod
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
mistrovství	mistrovství	k1gNnSc6	mistrovství
Formule	formule	k1gFnSc1	formule
Ford	ford	k1gInSc1	ford
1600	[number]	k4	1600
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
v	v	k7c6	v
Zandvoortu	Zandvoort	k1gInSc6	Zandvoort
zajistil	zajistit	k5eAaPmAgMnS	zajistit
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k6eAd1	rovněž
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
osm	osm	k4xCc1	osm
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
velkých	velký	k2eAgFnPc2d1	velká
cen	cena	k1gFnPc2	cena
německé	německý	k2eAgFnSc2d1	německá
Formule	formule	k1gFnSc2	formule
Ford	ford	k1gInSc1	ford
1600	[number]	k4	1600
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obsadil	obsadit	k5eAaPmAgMnS	obsadit
šesté	šestý	k4xOgNnSc4	šestý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
jej	on	k3xPp3gInSc4	on
pozval	pozvat	k5eAaPmAgMnS	pozvat
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
majitel	majitel	k1gMnSc1	majitel
týmu	tým	k1gInSc2	tým
WTS	WTS	kA	WTS
(	(	kIx(	(
<g/>
Weber	Weber	k1gMnSc1	Weber
Trella	Trella	k1gMnSc1	Trella
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
<g/>
)	)	kIx)	)
Willi	Will	k1gMnPc1	Will
Weber	weber	k1gInSc4	weber
na	na	k7c4	na
testy	test	k1gInPc4	test
formule	formule	k1gFnSc2	formule
3	[number]	k4	3
na	na	k7c6	na
Nürburgringu	Nürburgring	k1gInSc6	Nürburgring
<g/>
.	.	kIx.	.
</s>
<s>
Schumacher	Schumachra	k1gFnPc2	Schumachra
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
zapůsobil	zapůsobit	k5eAaPmAgMnS	zapůsobit
<g/>
,	,	kIx,	,
Weber	weber	k1gInSc1	weber
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
rok	rok	k1gInSc4	rok
1989	[number]	k4	1989
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
manažerskou	manažerský	k2eAgFnSc4d1	manažerská
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yRgFnSc4	který
jej	on	k3xPp3gMnSc4	on
byl	být	k5eAaImAgInS	být
ochotný	ochotný	k2eAgInSc4d1	ochotný
celou	celý	k2eAgFnSc4d1	celá
sezonu	sezona	k1gFnSc4	sezona
finančně	finančně	k6eAd1	finančně
podporovat	podporovat	k5eAaImF	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
oporou	opora	k1gFnSc7	opora
týmové	týmový	k2eAgFnSc2d1	týmová
jedničce	jednička	k1gFnSc3	jednička
Frentzenovi	Frentzenův	k2eAgMnPc1d1	Frentzenův
<g/>
,	,	kIx,	,
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
tři	tři	k4xCgInPc4	tři
závody	závod	k1gInPc4	závod
a	a	k8xC	a
skončil	skončit	k5eAaPmAgMnS	skončit
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
za	za	k7c7	za
mistrem	mistr	k1gMnSc7	mistr
Wendlingerem	Wendlinger	k1gMnSc7	Wendlinger
na	na	k7c6	na
děleném	dělený	k2eAgInSc6d1	dělený
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Frentzenem	Frentzen	k1gInSc7	Frentzen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
mistrem	mistr	k1gMnSc7	mistr
německé	německý	k2eAgFnSc2d1	německá
Formule	formule	k1gFnSc2	formule
3	[number]	k4	3
se	s	k7c7	s
šesti	šest	k4xCc7	šest
výhrami	výhra	k1gFnPc7	výhra
z	z	k7c2	z
jedenácti	jedenáct	k4xCc2	jedenáct
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
vavřín	vavřín	k1gInSc1	vavřín
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
prestižní	prestižní	k2eAgFnSc6d1	prestižní
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Macau	Macao	k1gNnSc3	Macao
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sportovní	sportovní	k2eAgInPc1d1	sportovní
vozy	vůz	k1gInPc1	vůz
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Frentzenem	Frentzeno	k1gNnSc7	Frentzeno
a	a	k8xC	a
Wendlingerem	Wendlingero	k1gNnSc7	Wendlingero
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
juniorského	juniorský	k2eAgInSc2d1	juniorský
týmu	tým	k1gInSc2	tým
Mercedesu	mercedes	k1gInSc2	mercedes
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
k	k	k7c3	k
výchově	výchova	k1gFnSc3	výchova
mladých	mladý	k2eAgMnPc2d1	mladý
jezdců	jezdec	k1gMnPc2	jezdec
pro	pro	k7c4	pro
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
sportovních	sportovní	k2eAgInPc2d1	sportovní
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Vůz	vůz	k1gInSc1	vůz
poprvé	poprvé	k6eAd1	poprvé
testoval	testovat	k5eAaImAgInS	testovat
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Paula	Paul	k1gMnSc2	Paul
Ricarda	Ricard	k1gMnSc2	Ricard
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
startoval	startovat	k5eAaBmAgMnS	startovat
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
závodech	závod	k1gInPc6	závod
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
skončil	skončit	k5eAaPmAgMnS	skončit
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jochenem	Jochen	k1gMnSc7	Jochen
Massem	Mass	k1gMnSc7	Mass
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
s	s	k7c7	s
Mercedesem	mercedes	k1gInSc7	mercedes
celou	celý	k2eAgFnSc4d1	celá
sezonu	sezona	k1gFnSc4	sezona
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
sportovních	sportovní	k2eAgInPc2d1	sportovní
vozů	vůz	k1gInPc2	vůz
skupiny	skupina	k1gFnSc2	skupina
C	C	kA	C
<g/>
,	,	kIx,	,
pět	pět	k4xCc1	pět
z	z	k7c2	z
osmi	osm	k4xCc2	osm
závodů	závod	k1gInPc2	závod
kvůli	kvůli	k7c3	kvůli
poruchám	porucha	k1gFnPc3	porucha
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
,	,	kIx,	,
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
Autopolisu	Autopolis	k1gInSc6	Autopolis
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
Silverstonu	Silverston	k1gInSc6	Silverston
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
obsadil	obsadit	k5eAaPmAgMnS	obsadit
dělené	dělený	k2eAgNnSc4d1	dělené
deváté	devátý	k4xOgNnSc4	devátý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závodě	závod	k1gInSc6	závod
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
Le	Le	k1gFnSc2	Le
Mans	Mans	k1gInSc1	Mans
zajel	zajet	k5eAaPmAgMnS	zajet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
nejrychlejší	rychlý	k2eAgInSc4d3	nejrychlejší
kolo	kolo	k1gNnSc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
závodů	závod	k1gInPc2	závod
DTM	DTM	kA	DTM
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
a	a	k8xC	a
nejlépe	dobře	k6eAd3	dobře
dojel	dojet	k5eAaPmAgMnS	dojet
čtrnáctý	čtrnáctý	k4xOgMnSc1	čtrnáctý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závodě	závod	k1gInSc6	závod
japonské	japonský	k2eAgFnSc2d1	japonská
F	F	kA	F
3000	[number]	k4	3000
dojel	dojet	k5eAaPmAgMnS	dojet
druhý	druhý	k4xOgMnSc1	druhý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
Jordan	Jordan	k1gMnSc1	Jordan
===	===	k?	===
</s>
</p>
<p>
<s>
Tým	tým	k1gInSc1	tým
Jordan	Jordan	k1gMnSc1	Jordan
mu	on	k3xPp3gMnSc3	on
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
místo	místo	k7c2	místo
závodního	závodní	k1gMnSc2	závodní
pilota	pilot	k1gMnSc2	pilot
pro	pro	k7c4	pro
Velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
mecenáši	mecenáš	k1gMnPc1	mecenáš
Mercedesu	mercedes	k1gInSc2	mercedes
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
start	start	k1gInSc4	start
ovšem	ovšem	k9	ovšem
zaplatili	zaplatit	k5eAaPmAgMnP	zaplatit
250	[number]	k4	250
000	[number]	k4	000
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
u	u	k7c2	u
Jordanu	Jordan	k1gMnSc3	Jordan
se	se	k3xPyFc4	se
uvolnilo	uvolnit	k5eAaPmAgNnS	uvolnit
kvůli	kvůli	k7c3	kvůli
incidentu	incident	k1gInSc3	incident
belgického	belgický	k2eAgMnSc2d1	belgický
pilota	pilot	k1gMnSc2	pilot
Bertranda	Bertrando	k1gNnSc2	Bertrando
Gachota	Gachot	k1gMnSc2	Gachot
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1990	[number]	k4	1990
postříkal	postříkat	k5eAaPmAgInS	postříkat
slzným	slzný	k2eAgInSc7d1	slzný
plynem	plyn	k1gInSc7	plyn
londýnského	londýnský	k2eAgMnSc2d1	londýnský
taxikáře	taxikář	k1gMnSc2	taxikář
Erica	Ericus	k1gMnSc2	Ericus
Courta	Court	k1gMnSc2	Court
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
dostal	dostat	k5eAaPmAgInS	dostat
trest	trest	k1gInSc1	trest
osmnáct	osmnáct	k4xCc1	osmnáct
měsíců	měsíc	k1gInPc2	měsíc
vězení	vězení	k1gNnPc2	vězení
<g/>
;	;	kIx,	;
ten	ten	k3xDgInSc1	ten
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
odvolání	odvolání	k1gNnSc6	odvolání
zkrácen	zkrácen	k2eAgMnSc1d1	zkrácen
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
Schumacherově	Schumacherův	k2eAgNnSc6d1	Schumacherovo
angažmá	angažmá	k1gNnSc6	angažmá
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
i	i	k8xC	i
jeho	jeho	k3xOp3gNnSc1	jeho
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
trať	trať	k1gFnSc1	trať
ve	v	k7c6	v
Spa	Spa	k1gFnSc6	Spa
dobře	dobře	k6eAd1	dobře
zná	znát	k5eAaImIp3nS	znát
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
zde	zde	k6eAd1	zde
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
pouze	pouze	k6eAd1	pouze
vyjížďku	vyjížďka	k1gFnSc4	vyjížďka
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Vůz	vůz	k1gInSc1	vůz
formule	formule	k1gFnSc1	formule
1	[number]	k4	1
poprvé	poprvé	k6eAd1	poprvé
testoval	testovat	k5eAaImAgMnS	testovat
v	v	k7c6	v
Silverstonu	Silverston	k1gInSc6	Silverston
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
belgické	belgický	k2eAgFnSc6d1	belgická
grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc4	prix
startoval	startovat	k5eAaBmAgMnS	startovat
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
32	[number]	k4	32
<g/>
;	;	kIx,	;
všechny	všechen	k3xTgInPc4	všechen
překvapil	překvapit	k5eAaPmAgInS	překvapit
<g/>
,	,	kIx,	,
kvalifikoval	kvalifikovat	k5eAaBmAgMnS	kvalifikovat
se	se	k3xPyFc4	se
na	na	k7c6	na
sedmém	sedmý	k4xOgNnSc6	sedmý
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
o	o	k7c4	o
čtyři	čtyři	k4xCgFnPc4	čtyři
pozice	pozice	k1gFnPc4	pozice
porazil	porazit	k5eAaPmAgMnS	porazit
svého	svůj	k3xOyFgMnSc4	svůj
zkušeného	zkušený	k2eAgMnSc4d1	zkušený
kolegu	kolega	k1gMnSc4	kolega
<g/>
,	,	kIx,	,
Itala	Ital	k1gMnSc4	Ital
Andreu	Andrea	k1gFnSc4	Andrea
de	de	k?	de
Cesarise	Cesarise	k1gFnSc1	Cesarise
<g/>
.	.	kIx.	.
</s>
<s>
Schumacherova	Schumacherův	k2eAgFnSc1d1	Schumacherova
sedmá	sedmý	k4xOgFnSc1	sedmý
pozice	pozice	k1gFnSc1	pozice
na	na	k7c6	na
startu	start	k1gInSc6	start
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
kvalifikačním	kvalifikační	k2eAgInSc7d1	kvalifikační
výsledkem	výsledek	k1gInSc7	výsledek
německého	německý	k2eAgMnSc4d1	německý
pilota	pilot	k1gMnSc4	pilot
ve	v	k7c6	v
formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
od	od	k7c2	od
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Kanady	Kanada	k1gFnSc2	Kanada
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
Manfred	Manfred	k1gInSc4	Manfred
Winkelhock	Winkelhocka	k1gFnPc2	Winkelhocka
také	také	k9	také
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
sedmý	sedmý	k4xOgMnSc1	sedmý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Schumacher	Schumachra	k1gFnPc2	Schumachra
kvůli	kvůli	k7c3	kvůli
poruše	porucha	k1gFnSc3	porucha
spojky	spojka	k1gFnSc2	spojka
ujel	ujet	k5eAaPmAgMnS	ujet
od	od	k7c2	od
startu	start	k1gInSc2	start
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
výkonem	výkon	k1gInSc7	výkon
udělal	udělat	k5eAaPmAgInS	udělat
dojem	dojem	k1gInSc4	dojem
na	na	k7c4	na
Eddieho	Eddie	k1gMnSc4	Eddie
Jordana	Jordan	k1gMnSc4	Jordan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
stálé	stálý	k2eAgNnSc4d1	stálé
angažmá	angažmá	k1gNnSc4	angažmá
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Mercedes	mercedes	k1gInSc1	mercedes
by	by	kYmCp3nS	by
musel	muset	k5eAaImAgInS	muset
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
zaplatit	zaplatit	k5eAaPmF	zaplatit
osm	osm	k4xCc4	osm
milionů	milion	k4xCgInPc2	milion
marek	marka	k1gFnPc2	marka
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
za	za	k7c4	za
čtrnáct	čtrnáct	k4xCc4	čtrnáct
dní	den	k1gInPc2	den
v	v	k7c6	v
Monze	Monza	k1gFnSc6	Monza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
<g/>
:	:	kIx,	:
Benetton	Benetton	k1gInSc1	Benetton
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
1991	[number]	k4	1991
====	====	k?	====
</s>
</p>
<p>
<s>
Manažer	manažer	k1gMnSc1	manažer
Benettonu	Benetton	k1gInSc2	Benetton
Flavio	Flavio	k1gMnSc1	Flavio
Briatore	Briator	k1gMnSc5	Briator
si	se	k3xPyFc3	se
německého	německý	k2eAgMnSc4d1	německý
pilota	pilot	k1gMnSc4	pilot
při	při	k7c6	při
předchozí	předchozí	k2eAgFnSc6d1	předchozí
velké	velký	k2eAgFnSc6d1	velká
ceně	cena	k1gFnSc6	cena
povšiml	povšimnout	k5eAaPmAgMnS	povšimnout
a	a	k8xC	a
vyhodil	vyhodit	k5eAaPmAgMnS	vyhodit
z	z	k7c2	z
týmu	tým	k1gInSc2	tým
Brazilce	Brazilec	k1gMnSc2	Brazilec
Roberta	Robert	k1gMnSc2	Robert
Morena	Moren	k1gMnSc2	Moren
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uvolnil	uvolnit	k5eAaPmAgMnS	uvolnit
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
Schumachera	Schumacher	k1gMnSc4	Schumacher
<g/>
.	.	kIx.	.
</s>
<s>
Roli	role	k1gFnSc4	role
hrál	hrát	k5eAaImAgMnS	hrát
i	i	k9	i
Jordanův	Jordanův	k2eAgMnSc1d1	Jordanův
již	již	k6eAd1	již
nasmlouvaný	nasmlouvaný	k2eAgInSc1d1	nasmlouvaný
motor	motor	k1gInSc1	motor
Yamaha	yamaha	k1gFnSc1	yamaha
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nevyhovoval	vyhovovat	k5eNaImAgInS	vyhovovat
vysokým	vysoký	k2eAgInSc7d1	vysoký
ambicím	ambice	k1gFnPc3	ambice
jezdcova	jezdcův	k2eAgMnSc2d1	jezdcův
manažera	manažer	k1gMnSc2	manažer
Webera	Weber	k1gMnSc2	Weber
<g/>
.	.	kIx.	.
</s>
<s>
Schumacher	Schumachra	k1gFnPc2	Schumachra
dokázal	dokázat	k5eAaPmAgMnS	dokázat
i	i	k9	i
ve	v	k7c6	v
žlutozeleném	žlutozelený	k2eAgInSc6d1	žlutozelený
benettonu	benetton	k1gInSc6	benetton
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
devatenáct	devatenáct	k4xCc4	devatenáct
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
porazit	porazit	k5eAaPmF	porazit
týmového	týmový	k2eAgMnSc4d1	týmový
kolegu	kolega	k1gMnSc4	kolega
<g/>
,	,	kIx,	,
trojnásobného	trojnásobný	k2eAgMnSc2d1	trojnásobný
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
Nelsona	Nelson	k1gMnSc4	Nelson
Piqueta	Piquet	k1gMnSc4	Piquet
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
porazil	porazit	k5eAaPmAgMnS	porazit
i	i	k9	i
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
pátým	pátý	k4xOgNnSc7	pátý
místem	místo	k1gNnSc7	místo
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
tak	tak	k6eAd1	tak
své	svůj	k3xOyFgInPc4	svůj
první	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
body	bod	k1gInPc4	bod
do	do	k7c2	do
Poháru	pohár	k1gInSc2	pohár
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
body	bod	k1gInPc4	bod
získal	získat	k5eAaPmAgMnS	získat
za	za	k7c4	za
dvě	dva	k4xCgNnPc4	dva
šestá	šestý	k4xOgNnPc4	šestý
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
a	a	k8xC	a
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dokonce	dokonce	k9	dokonce
bojoval	bojovat	k5eAaImAgInS	bojovat
o	o	k7c6	o
vedení	vedení	k1gNnSc6	vedení
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc4d1	poslední
dvě	dva	k4xCgFnPc4	dva
velké	velký	k2eAgFnPc4d1	velká
ceny	cena	k1gFnPc4	cena
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
,	,	kIx,	,
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
mu	on	k3xPp3gMnSc3	on
po	po	k7c6	po
těžké	těžký	k2eAgFnSc6d1	těžká
nehodě	nehoda	k1gFnSc6	nehoda
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
explodoval	explodovat	k5eAaBmAgInS	explodovat
v	v	k7c6	v
35	[number]	k4	35
<g/>
.	.	kIx.	.
okruhu	okruh	k1gInSc2	okruh
motor	motor	k1gInSc1	motor
a	a	k8xC	a
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jezdil	jezdit	k5eAaImAgMnS	jezdit
na	na	k7c6	na
mokré	mokrý	k2eAgFnSc6d1	mokrá
trati	trať	k1gFnSc6	trať
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
<g/>
,	,	kIx,	,
kolidoval	kolidovat	k5eAaImAgInS	kolidovat
v	v	k7c6	v
šestém	šestý	k4xOgNnSc6	šestý
kole	kolo	k1gNnSc6	kolo
s	s	k7c7	s
Alesim	Alesi	k1gNnSc7	Alesi
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
získal	získat	k5eAaPmAgMnS	získat
čtyři	čtyři	k4xCgInPc4	čtyři
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
v	v	k7c6	v
šampionátu	šampionát	k1gInSc6	šampionát
skončil	skončit	k5eAaPmAgMnS	skončit
na	na	k7c6	na
dvanáctém	dvanáctý	k4xOgInSc6	dvanáctý
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
kvalifikačním	kvalifikační	k2eAgInSc7d1	kvalifikační
výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
pátá	pátá	k1gFnSc1	pátá
startovní	startovní	k2eAgFnSc2d1	startovní
pozice	pozice	k1gFnSc2	pozice
v	v	k7c6	v
GP	GP	kA	GP
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1992	[number]	k4	1992
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
kompletní	kompletní	k2eAgFnSc4d1	kompletní
sezonu	sezona	k1gFnSc4	sezona
ve	v	k7c6	v
formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
předsezonním	předsezonní	k2eAgNnSc6d1	předsezonní
testování	testování	k1gNnSc6	testování
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
Schumacherem	Schumacher	k1gMnSc7	Schumacher
počítá	počítat	k5eAaImIp3nS	počítat
i	i	k9	i
tým	tým	k1gInSc1	tým
Sauber	Sauber	k1gInSc1	Sauber
podporovaný	podporovaný	k2eAgInSc1d1	podporovaný
Mercedesem	mercedes	k1gInSc7	mercedes
<g/>
,	,	kIx,	,
po	po	k7c6	po
zákulisních	zákulisní	k2eAgNnPc6d1	zákulisní
jednáních	jednání	k1gNnPc6	jednání
se	se	k3xPyFc4	se
svých	svůj	k3xOyFgNnPc2	svůj
smluvních	smluvní	k2eAgNnPc2d1	smluvní
práv	právo	k1gNnPc2	právo
vzdal	vzdát	k5eAaPmAgMnS	vzdát
se	se	k3xPyFc4	se
sdělením	sdělení	k1gNnSc7	sdělení
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Nechceme	chtít	k5eNaImIp1nP	chtít
stát	stát	k5eAaImF	stát
Michaelovi	Michael	k1gMnSc3	Michael
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Schumacher	Schumachra	k1gFnPc2	Schumachra
začal	začít	k5eAaPmAgInS	začít
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
místem	místo	k1gNnSc7	místo
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
a	a	k8xC	a
Brazílii	Brazílie	k1gFnSc6	Brazílie
skončil	skončit	k5eAaPmAgMnS	skončit
třetí	třetí	k4xOgMnSc1	třetí
<g/>
,	,	kIx,	,
když	když	k8xS	když
dvakrát	dvakrát	k6eAd1	dvakrát
předjel	předjet	k5eAaPmAgMnS	předjet
Ayrtona	Ayrton	k1gMnSc4	Ayrton
Sennu	Senna	k1gMnSc4	Senna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
poprvé	poprvé	k6eAd1	poprvé
startoval	startovat	k5eAaBmAgInS	startovat
z	z	k7c2	z
první	první	k4xOgFnSc2	první
řady	řada	k1gFnSc2	řada
a	a	k8xC	a
po	po	k7c4	po
Patreseho	Patrese	k1gMnSc4	Patrese
hodinách	hodina	k1gFnPc6	hodina
dojel	dojet	k5eAaPmAgMnS	dojet
druhý	druhý	k4xOgMnSc1	druhý
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
Benettonu	Benetton	k1gInSc6	Benetton
B	B	kA	B
<g/>
192	[number]	k4	192
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
poprvé	poprvé	k6eAd1	poprvé
vyvíjeli	vyvíjet	k5eAaImAgMnP	vyvíjet
Ross	Ross	k1gInSc4	Ross
Brawn	Brawna	k1gFnPc2	Brawna
a	a	k8xC	a
Rory	Rora	k1gFnSc2	Rora
Byrne	Byrn	k1gMnSc5	Byrn
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
Velké	velký	k2eAgFnSc6d1	velká
ceně	cena	k1gFnSc6	cena
San	San	k1gFnSc1	San
Marina	Marina	k1gFnSc1	Marina
po	po	k7c6	po
chybě	chyba	k1gFnSc6	chyba
vylétl	vylétnout	k5eAaPmAgMnS	vylétnout
z	z	k7c2	z
trati	trať	k1gFnSc2	trať
<g/>
,	,	kIx,	,
v	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
po	po	k7c6	po
kolizi	kolize	k1gFnSc6	kolize
s	s	k7c7	s
Alesim	Alesi	k1gNnSc7	Alesi
dokončil	dokončit	k5eAaPmAgMnS	dokončit
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
skončil	skončit	k5eAaPmAgMnS	skončit
druhý	druhý	k4xOgMnSc1	druhý
za	za	k7c7	za
Bergerovým	Bergerův	k2eAgInSc7d1	Bergerův
McLarenem	McLaren	k1gInSc7	McLaren
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vlhké	vlhký	k2eAgFnSc2d1	vlhká
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Francie	Francie	k1gFnSc2	Francie
naboural	nabourat	k5eAaPmAgMnS	nabourat
zadní	zadní	k2eAgFnSc4d1	zadní
část	část	k1gFnSc4	část
Sennova	Sennův	k2eAgInSc2d1	Sennův
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
velké	velký	k2eAgFnSc6d1	velká
ceně	cena	k1gFnSc6	cena
v	v	k7c6	v
Silverstonu	Silverston	k1gInSc6	Silverston
jej	on	k3xPp3gMnSc4	on
poprvé	poprvé	k6eAd1	poprvé
porazil	porazit	k5eAaPmAgMnS	porazit
týmový	týmový	k2eAgMnSc1d1	týmový
kolega	kolega	k1gMnSc1	kolega
Martin	Martin	k1gMnSc1	Martin
Brundle	Brundle	k1gMnSc1	Brundle
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
kolidoval	kolidovat	k5eAaImAgMnS	kolidovat
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
a	a	k8xC	a
závod	závod	k1gInSc1	závod
kvůli	kvůli	k7c3	kvůli
ulomení	ulomení	k1gNnSc3	ulomení
zadního	zadní	k2eAgNnSc2d1	zadní
křídla	křídlo	k1gNnSc2	křídlo
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
po	po	k7c6	po
premiéře	premiéra	k1gFnSc6	premiéra
ve	v	k7c6	v
Spa	Spa	k1gFnSc6	Spa
tu	tu	k6eAd1	tu
slavil	slavit	k5eAaImAgMnS	slavit
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
,	,	kIx,	,
když	když	k8xS	když
po	po	k7c6	po
dešti	dešť	k1gInSc6	dešť
strategicky	strategicky	k6eAd1	strategicky
nasadil	nasadit	k5eAaPmAgMnS	nasadit
hladké	hladký	k2eAgFnPc4d1	hladká
pneumatiky	pneumatika	k1gFnPc4	pneumatika
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgFnSc1	první
výhra	výhra	k1gFnSc1	výhra
německého	německý	k2eAgMnSc2d1	německý
pilota	pilot	k1gMnSc2	pilot
od	od	k7c2	od
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Španělska	Španělsko	k1gNnSc2	Španělsko
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
Schumacherův	Schumacherův	k2eAgMnSc1d1	Schumacherův
rádce	rádce	k1gMnSc1	rádce
v	v	k7c6	v
Mercedesu	mercedes	k1gInSc6	mercedes
Jochen	Jochen	k2eAgInSc4d1	Jochen
Mass	Mass	k1gInSc4	Mass
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
Itálie	Itálie	k1gFnSc2	Itálie
dokončil	dokončit	k5eAaPmAgInS	dokončit
třetí	třetí	k4xOgInSc1	třetí
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
první	první	k4xOgFnSc6	první
zatáčce	zatáčka	k1gFnSc6	zatáčka
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
přední	přední	k2eAgNnSc4d1	přední
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gFnSc6	jehož
výměně	výměna	k1gFnSc6	výměna
a	a	k8xC	a
následné	následný	k2eAgFnSc6d1	následná
šílené	šílený	k2eAgFnSc6d1	šílená
stíhací	stíhací	k2eAgFnSc6d1	stíhací
jízdě	jízda	k1gFnSc6	jízda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
po	po	k7c6	po
25	[number]	k4	25
kolech	kolo	k1gNnPc6	kolo
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
páté	pátý	k4xOgNnSc4	pátý
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
se	s	k7c7	s
spojkou	spojka	k1gFnSc7	spojka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
velké	velký	k2eAgFnPc1d1	velká
ceny	cena	k1gFnPc1	cena
jej	on	k3xPp3gNnSc2	on
provázely	provázet	k5eAaImAgFnP	provázet
technické	technický	k2eAgFnPc4d1	technická
potíže	potíž	k1gFnPc4	potíž
<g/>
,	,	kIx,	,
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
mu	on	k3xPp3gNnSc3	on
na	na	k7c6	na
startu	start	k1gInSc6	start
zhasl	zhasnout	k5eAaPmAgInS	zhasnout
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgInS	muset
proto	proto	k8xC	proto
startovat	startovat	k5eAaBmF	startovat
z	z	k7c2	z
konce	konec	k1gInSc2	konec
roštu	rošt	k1gInSc2	rošt
<g/>
,	,	kIx,	,
během	během	k7c2	během
závodu	závod	k1gInSc2	závod
měl	mít	k5eAaImAgMnS	mít
defekt	defekt	k1gInSc4	defekt
a	a	k8xC	a
dojel	dojet	k5eAaPmAgMnS	dojet
na	na	k7c6	na
nebodovaném	bodovaný	k2eNgNnSc6d1	nebodované
sedmém	sedmý	k4xOgNnSc6	sedmý
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
mu	on	k3xPp3gMnSc3	on
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
selhala	selhat	k5eAaPmAgFnS	selhat
převodovka	převodovka	k1gFnSc1	převodovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
závodě	závod	k1gInSc6	závod
zajel	zajet	k5eAaPmAgMnS	zajet
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
kolo	kolo	k1gNnSc4	kolo
závodu	závod	k1gInSc2	závod
a	a	k8xC	a
dojel	dojet	k5eAaPmAgMnS	dojet
druhý	druhý	k4xOgMnSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Poháru	pohár	k1gInSc6	pohár
jezdců	jezdec	k1gMnPc2	jezdec
získal	získat	k5eAaPmAgMnS	získat
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
za	za	k7c7	za
dominujícími	dominující	k2eAgInPc7d1	dominující
jezdci	jezdec	k1gInPc7	jezdec
Williamsu	Williams	k1gInSc2	Williams
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
držel	držet	k5eAaImAgInS	držet
od	od	k7c2	od
druhého	druhý	k4xOgInSc2	druhý
závodu	závod	k1gInSc2	závod
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
týmovém	týmový	k2eAgInSc6d1	týmový
souboji	souboj	k1gInSc6	souboj
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
Brita	Brit	k1gMnSc2	Brit
porazil	porazit	k5eAaPmAgInS	porazit
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
těchto	tento	k3xDgFnPc2	tento
kvalifikací	kvalifikace	k1gFnPc2	kvalifikace
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
jen	jen	k9	jen
o	o	k7c6	o
jediném	jediné	k1gNnSc6	jediné
místo	místo	k1gNnSc1	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1993	[number]	k4	1993
====	====	k?	====
</s>
</p>
<p>
<s>
I	i	k9	i
sezona	sezona	k1gFnSc1	sezona
1993	[number]	k4	1993
se	se	k3xPyFc4	se
nesla	nést	k5eAaImAgFnS	nést
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
boje	boj	k1gInSc2	boj
Schumachera	Schumacher	k1gMnSc2	Schumacher
se	s	k7c7	s
Sennou	senný	k2eAgFnSc7d1	senná
<g/>
.	.	kIx.	.
</s>
<s>
Týmovým	týmový	k2eAgMnSc7d1	týmový
kolegou	kolega	k1gMnSc7	kolega
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Ital	Ital	k1gMnSc1	Ital
Riccardo	Riccardo	k1gNnSc4	Riccardo
Patrese	Patrese	k1gFnSc2	Patrese
<g/>
.	.	kIx.	.
</s>
<s>
Rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Kerpenu	Kerpen	k2eAgFnSc4d1	Kerpen
do	do	k7c2	do
běžné	běžný	k2eAgFnSc2d1	běžná
závodní	závodní	k2eAgFnSc2d1	závodní
rutiny	rutina	k1gFnSc2	rutina
vnesl	vnést	k5eAaPmAgMnS	vnést
nový	nový	k2eAgInSc4d1	nový
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
před	před	k7c7	před
závodem	závod	k1gInSc7	závod
vždy	vždy	k6eAd1	vždy
objel	objet	k5eAaPmAgMnS	objet
okruh	okruh	k1gInSc4	okruh
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
závody	závod	k1gInPc4	závod
museli	muset	k5eAaImAgMnP	muset
oba	dva	k4xCgMnPc1	dva
jezdci	jezdec	k1gMnPc1	jezdec
používat	používat	k5eAaImF	používat
loňský	loňský	k2eAgInSc4d1	loňský
model	model	k1gInSc4	model
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
závodě	závod	k1gInSc6	závod
se	se	k3xPyFc4	se
Brazilce	Brazilec	k1gMnSc2	Brazilec
snažil	snažit	k5eAaImAgMnS	snažit
předjet	předjet	k5eAaPmF	předjet
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
vylétl	vylétnout	k5eAaPmAgMnS	vylétnout
z	z	k7c2	z
trati	trať	k1gFnSc2	trať
a	a	k8xC	a
závod	závod	k1gInSc4	závod
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Interlagosu	Interlagos	k1gInSc6	Interlagos
po	po	k7c6	po
nevydařené	vydařený	k2eNgFnSc6d1	nevydařená
zastávce	zastávka	k1gFnSc6	zastávka
v	v	k7c6	v
boxech	box	k1gInPc6	box
skončil	skončit	k5eAaPmAgMnS	skončit
třetí	třetí	k4xOgMnSc1	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Deštivou	deštivý	k2eAgFnSc4d1	deštivá
velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
v	v	k7c4	v
Donington	Donington	k1gInSc4	Donington
Parku	park	k1gInSc2	park
po	po	k7c6	po
smyku	smyk	k1gInSc6	smyk
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Imole	Imol	k1gInSc6	Imol
dojel	dojet	k5eAaPmAgMnS	dojet
druhý	druhý	k4xOgMnSc1	druhý
za	za	k7c7	za
Alainem	Alain	k1gMnSc7	Alain
Prostem	Prost	k1gMnSc7	Prost
<g/>
,	,	kIx,	,
na	na	k7c4	na
stupně	stupeň	k1gInPc4	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
i	i	k9	i
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
v	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tým	tým	k1gInSc1	tým
do	do	k7c2	do
monopostu	monopost	k1gInSc2	monopost
implementoval	implementovat	k5eAaImAgMnS	implementovat
elektronickou	elektronický	k2eAgFnSc4d1	elektronická
kontrolu	kontrola	k1gFnSc4	kontrola
prokluzu	prokluz	k1gInSc2	prokluz
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
závadě	závada	k1gFnSc3	závada
hydrauliky	hydraulika	k1gFnSc2	hydraulika
nemohl	moct	k5eNaImAgMnS	moct
využít	využít	k5eAaPmF	využít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
čtyřech	čtyři	k4xCgInPc6	čtyři
závodech	závod	k1gInPc6	závod
se	se	k3xPyFc4	se
radoval	radovat	k5eAaImAgMnS	radovat
ze	z	k7c2	z
stupňů	stupeň	k1gInPc2	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
dojel	dojet	k5eAaPmAgMnS	dojet
druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
v	v	k7c4	v
Magny-Cours	Magny-Cours	k1gInSc4	Magny-Cours
třetí	třetí	k4xOgNnSc4	třetí
po	po	k7c6	po
předjetí	předjetí	k1gNnSc6	předjetí
Senny	Senny	k?	Senny
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
porazil	porazit	k5eAaPmAgMnS	porazit
i	i	k9	i
stříbrnou	stříbrný	k2eAgFnSc7d1	stříbrná
příčkou	příčka	k1gFnSc7	příčka
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Velké	velká	k1gFnSc2	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
stejné	stejný	k2eAgNnSc1d1	stejné
umístění	umístění	k1gNnSc4	umístění
získal	získat	k5eAaPmAgInS	získat
i	i	k9	i
v	v	k7c6	v
domácí	domácí	k2eAgFnSc6d1	domácí
Velké	velký	k2eAgFnSc6d1	velká
ceně	cena	k1gFnSc6	cena
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
závodě	závod	k1gInSc6	závod
na	na	k7c6	na
Hungaroringu	Hungaroring	k1gInSc6	Hungaroring
po	po	k7c6	po
startu	start	k1gInSc6	start
ze	z	k7c2	z
třetího	třetí	k4xOgNnSc2	třetí
místa	místo	k1gNnSc2	místo
udělal	udělat	k5eAaPmAgInS	udělat
dva	dva	k4xCgInPc4	dva
smyky	smyk	k1gInPc4	smyk
a	a	k8xC	a
skončil	skončit	k5eAaPmAgInS	skončit
kvůli	kvůli	k7c3	kvůli
závadě	závada	k1gFnSc3	závada
benzinového	benzinový	k2eAgNnSc2d1	benzinové
čerpadla	čerpadlo	k1gNnSc2	čerpadlo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
oblíbené	oblíbený	k2eAgFnSc6d1	oblíbená
Belgii	Belgie	k1gFnSc6	Belgie
skončil	skončit	k5eAaPmAgMnS	skončit
druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
když	když	k8xS	když
předjel	předjet	k5eAaPmAgMnS	předjet
Sennu	Senna	k1gFnSc4	Senna
i	i	k9	i
Prosta	prost	k2eAgFnSc1d1	prosta
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
technické	technický	k2eAgFnPc1d1	technická
obtíže	obtíž	k1gFnPc1	obtíž
jej	on	k3xPp3gNnSc2	on
postihly	postihnout	k5eAaPmAgFnP	postihnout
při	při	k7c6	při
Velké	velký	k2eAgFnSc6d1	velká
ceně	cena	k1gFnSc6	cena
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
musel	muset	k5eAaImAgMnS	muset
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
místa	místo	k1gNnSc2	místo
odstoupit	odstoupit	k5eAaPmF	odstoupit
kvůli	kvůli	k7c3	kvůli
závadě	závada	k1gFnSc3	závada
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
vítězství	vítězství	k1gNnSc1	vítězství
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
třetí	třetí	k4xOgFnSc6	třetí
sezoně	sezona	k1gFnSc6	sezona
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
po	po	k7c6	po
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
Prostem	Prost	k1gInSc7	Prost
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
dva	dva	k4xCgInPc1	dva
závody	závod	k1gInPc1	závod
sezony	sezona	k1gFnSc2	sezona
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nevydařily	vydařit	k5eNaPmAgFnP	vydařit
<g/>
,	,	kIx,	,
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Japonska	Japonsko	k1gNnSc2	Japonsko
se	se	k3xPyFc4	se
vyřadil	vyřadit	k5eAaPmAgInS	vyřadit
v	v	k7c6	v
jedenáctém	jedenáctý	k4xOgNnSc6	jedenáctý
kole	kolo	k1gNnSc6	kolo
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
Damonem	Damon	k1gMnSc7	Damon
Hillem	Hill	k1gMnSc7	Hill
a	a	k8xC	a
účinkování	účinkování	k1gNnSc1	účinkování
v	v	k7c6	v
australské	australský	k2eAgFnSc6d1	australská
grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc4	prix
mu	on	k3xPp3gMnSc3	on
překazil	překazit	k5eAaPmAgMnS	překazit
po	po	k7c6	po
devatenácti	devatenáct	k4xCc2	devatenáct
kolech	kolo	k1gNnPc6	kolo
motor	motor	k1gInSc1	motor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
konečné	konečný	k2eAgNnSc4d1	konečné
pořadí	pořadí	k1gNnSc4	pořadí
šampionátu	šampionát	k1gInSc2	šampionát
obsadil	obsadit	k5eAaPmAgMnS	obsadit
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
52	[number]	k4	52
body	bod	k1gInPc7	bod
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgNnSc7	jeden
vítězstvím	vítězství	k1gNnSc7	vítězství
<g/>
,	,	kIx,	,
pěti	pět	k4xCc7	pět
stříbrnými	stříbrná	k1gFnPc7	stříbrná
a	a	k8xC	a
třemi	tři	k4xCgFnPc7	tři
bronzovými	bronzový	k2eAgFnPc7d1	bronzová
příčkami	příčka	k1gFnPc7	příčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
opět	opět	k6eAd1	opět
svého	svůj	k3xOyFgMnSc4	svůj
stájového	stájový	k2eAgMnSc4d1	stájový
kolegu	kolega	k1gMnSc4	kolega
porazil	porazit	k5eAaPmAgMnS	porazit
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezony	sezona	k1gFnSc2	sezona
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
manažera	manažer	k1gMnSc2	manažer
Webera	Weber	k1gMnSc2	Weber
i	i	k8xC	i
vlastníka	vlastník	k1gMnSc2	vlastník
týmu	tým	k1gInSc2	tým
McLaren	McLarno	k1gNnPc2	McLarno
Rona	Ron	k1gMnSc2	Ron
Dennise	Dennise	k1gFnSc2	Dennise
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
o	o	k7c6	o
služby	služba	k1gFnPc1	služba
německého	německý	k2eAgMnSc2d1	německý
pilota	pilot	k1gMnSc2	pilot
zajímal	zajímat	k5eAaImAgInS	zajímat
<g/>
,	,	kIx,	,
Briatore	Briator	k1gMnSc5	Briator
Schumacherovi	Schumacher	k1gMnSc3	Schumacher
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
novou	nový	k2eAgFnSc4d1	nová
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
zaručovala	zaručovat	k5eAaImAgFnS	zaručovat
roční	roční	k2eAgInSc4d1	roční
příjem	příjem	k1gInSc4	příjem
šesti	šest	k4xCc2	šest
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1994	[number]	k4	1994
====	====	k?	====
</s>
</p>
<p>
<s>
Rozporuplný	rozporuplný	k2eAgInSc1d1	rozporuplný
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
Schumachera	Schumacher	k1gMnSc4	Schumacher
přinesl	přinést	k5eAaPmAgInS	přinést
další	další	k2eAgInSc1d1	další
ročník	ročník	k1gInSc1	ročník
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
tři	tři	k4xCgFnPc4	tři
grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc4	prix
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
předjel	předjet	k5eAaPmAgMnS	předjet
celé	celý	k2eAgNnSc4d1	celé
závodní	závodní	k2eAgNnSc4d1	závodní
pole	pole	k1gNnSc4	pole
o	o	k7c4	o
kolo	kolo	k1gNnSc4	kolo
a	a	k8xC	a
v	v	k7c6	v
Pacifiku	Pacifik	k1gInSc6	Pacifik
předčil	předčit	k5eAaBmAgMnS	předčit
druhého	druhý	k4xOgMnSc4	druhý
Hilla	Hill	k1gMnSc4	Hill
o	o	k7c4	o
75	[number]	k4	75
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tragické	tragický	k2eAgFnSc6d1	tragická
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
San	San	k1gFnSc2	San
Marina	Marina	k1gFnSc1	Marina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
Roland	Roland	k1gInSc4	Roland
Ratzenberger	Ratzenbergra	k1gFnPc2	Ratzenbergra
a	a	k8xC	a
Schumacherův	Schumacherův	k2eAgInSc1d1	Schumacherův
vzor	vzor	k1gInSc1	vzor
Ayrton	Ayrton	k1gInSc1	Ayrton
Senna	Senna	k1gFnSc1	Senna
<g/>
,	,	kIx,	,
Němec	Němec	k1gMnSc1	Němec
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c6	o
úplném	úplný	k2eAgInSc6d1	úplný
odchodu	odchod	k1gInSc6	odchod
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
automobilových	automobilový	k2eAgInPc2d1	automobilový
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Monaka	Monako	k1gNnSc2	Monako
si	se	k3xPyFc3	se
vyjel	vyjet	k5eAaPmAgMnS	vyjet
první	první	k4xOgNnSc4	první
pole	pole	k1gNnSc4	pole
position	position	k1gInSc1	position
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
proměnil	proměnit	k5eAaPmAgMnS	proměnit
ve	v	k7c6	v
vítězství	vítězství	k1gNnSc6	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
dojel	dojet	k5eAaPmAgMnS	dojet
druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
když	když	k8xS	když
43	[number]	k4	43
z	z	k7c2	z
65	[number]	k4	65
okruhů	okruh	k1gInPc2	okruh
musel	muset	k5eAaImAgMnS	muset
odjet	odjet	k5eAaPmF	odjet
s	s	k7c7	s
poškozenou	poškozený	k2eAgFnSc7d1	poškozená
převodovkou	převodovka	k1gFnSc7	převodovka
na	na	k7c4	na
pátý	pátý	k4xOgInSc4	pátý
převodový	převodový	k2eAgInSc4d1	převodový
stupeň	stupeň	k1gInSc4	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalším	další	k2eAgNnSc6d1	další
prvenství	prvenství	k1gNnSc6	prvenství
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
i	i	k9	i
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
ceně	cena	k1gFnSc6	cena
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zbytek	zbytek	k1gInSc1	zbytek
závodníků	závodník	k1gMnPc2	závodník
na	na	k7c6	na
střídavě	střídavě	k6eAd1	střídavě
deštivé	deštivý	k2eAgFnSc6d1	deštivá
trati	trať	k1gFnSc6	trať
deklasoval	deklasovat	k5eAaBmAgInS	deklasovat
<g/>
,	,	kIx,	,
druhého	druhý	k4xOgMnSc4	druhý
Hilla	Hill	k1gMnSc4	Hill
dělilo	dělit	k5eAaImAgNnS	dělit
od	od	k7c2	od
vítěze	vítěz	k1gMnSc4	vítěz
čtyřicet	čtyřicet	k4xCc4	čtyřicet
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Francie	Francie	k1gFnSc2	Francie
přes	přes	k7c4	přes
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
dokázal	dokázat	k5eAaPmAgMnS	dokázat
porazit	porazit	k5eAaPmF	porazit
oba	dva	k4xCgInPc4	dva
piloty	pilot	k1gInPc4	pilot
stáje	stáj	k1gFnSc2	stáj
Williams	Williamsa	k1gFnPc2	Williamsa
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
startovali	startovat	k5eAaBmAgMnP	startovat
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
zajel	zajet	k5eAaPmAgMnS	zajet
druhý	druhý	k4xOgInSc4	druhý
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
kvalifikační	kvalifikační	k2eAgInSc4d1	kvalifikační
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
v	v	k7c6	v
zahřívacím	zahřívací	k2eAgNnSc6d1	zahřívací
kole	kolo	k1gNnSc6	kolo
předjel	předjet	k5eAaPmAgMnS	předjet
Damona	Damon	k1gMnSc4	Damon
Hilla	Hill	k1gMnSc4	Hill
a	a	k8xC	a
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
zařadil	zařadit	k5eAaPmAgMnS	zařadit
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
dostal	dostat	k5eAaPmAgMnS	dostat
pětisekundovou	pětisekundový	k2eAgFnSc4d1	pětisekundová
penalizaci	penalizace	k1gFnSc4	penalizace
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
týmu	tým	k1gInSc2	tým
ignoroval	ignorovat	k5eAaImAgMnS	ignorovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
mu	on	k3xPp3gMnSc3	on
traťoví	traťový	k2eAgMnPc1d1	traťový
komisaři	komisar	k1gMnPc1	komisar
ukázali	ukázat	k5eAaPmAgMnP	ukázat
černou	černý	k2eAgFnSc4d1	černá
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
tu	ten	k3xDgFnSc4	ten
opět	opět	k6eAd1	opět
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
týmu	tým	k1gInSc2	tým
neuposlechl	uposlechnout	k5eNaPmAgInS	uposlechnout
a	a	k8xC	a
po	po	k7c6	po
diskuzi	diskuze	k1gFnSc6	diskuze
vedení	vedení	k1gNnSc2	vedení
týmu	tým	k1gInSc2	tým
s	s	k7c7	s
traťovými	traťový	k2eAgMnPc7d1	traťový
komisaři	komisař	k1gMnPc7	komisař
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
původní	původní	k2eAgFnSc4d1	původní
pětisekundovou	pětisekundový	k2eAgFnSc4d1	pětisekundová
penalizaci	penalizace	k1gFnSc4	penalizace
<g/>
,	,	kIx,	,
po	po	k7c6	po
závodě	závod	k1gInSc6	závod
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
ponecháno	ponechán	k2eAgNnSc4d1	ponecháno
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tým	tým	k1gInSc1	tým
i	i	k8xC	i
Schumacher	Schumachra	k1gFnPc2	Schumachra
dostali	dostat	k5eAaPmAgMnP	dostat
pokutu	pokuta	k1gFnSc4	pokuta
15	[number]	k4	15
000	[number]	k4	000
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
situace	situace	k1gFnSc2	situace
se	se	k3xPyFc4	se
vložil	vložit	k5eAaPmAgMnS	vložit
prezident	prezident	k1gMnSc1	prezident
FIA	FIA	kA	FIA
Max	Max	k1gMnSc1	Max
Mosley	Moslea	k1gMnSc2	Moslea
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
červnovém	červnový	k2eAgInSc6d1	červnový
soudu	soud	k1gInSc6	soud
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
Schumacherovi	Schumacher	k1gMnSc3	Schumacher
bylo	být	k5eAaImAgNnS	být
odebráno	odebrán	k2eAgNnSc1d1	odebráno
druhé	druhý	k4xOgFnSc6	druhý
místo	místo	k1gNnSc4	místo
i	i	k9	i
šest	šest	k4xCc4	šest
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
Výkon	výkon	k1gInSc1	výkon
trestu	trest	k1gInSc2	trest
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
odvolání	odvolání	k1gNnSc6	odvolání
Benettonu	Benetton	k1gInSc2	Benetton
odložen	odložit	k5eAaPmNgInS	odložit
o	o	k7c4	o
tři	tři	k4xCgFnPc4	tři
grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc1	prix
<g/>
.	.	kIx.	.
<g/>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Německa	Německo	k1gNnSc2	Německo
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
poruchy	porucha	k1gFnSc2	porucha
motoru	motor	k1gInSc2	motor
držel	držet	k5eAaImAgMnS	držet
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
i	i	k8xC	i
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
diskvalifikován	diskvalifikovat	k5eAaBmNgMnS	diskvalifikovat
pro	pro	k7c4	pro
nepovolenou	povolený	k2eNgFnSc4d1	nepovolená
techniku	technika	k1gFnSc4	technika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
deska	deska	k1gFnSc1	deska
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
vozu	vůz	k1gInSc2	vůz
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
milimetr	milimetr	k1gInSc4	milimetr
tenčí	tenčit	k5eAaImIp3nS	tenčit
<g/>
,	,	kIx,	,
než	než	k8xS	než
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc1d1	následující
dva	dva	k4xCgInPc1	dva
závody	závod	k1gInPc1	závod
si	se	k3xPyFc3	se
odpykával	odpykávat	k5eAaImAgInS	odpykávat
trest	trest	k1gInSc4	trest
zákazu	zákaz	k1gInSc2	zákaz
startu	start	k1gInSc2	start
z	z	k7c2	z
GP	GP	kA	GP
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
;	;	kIx,	;
Schumacherův	Schumacherův	k2eAgMnSc1d1	Schumacherův
hlavní	hlavní	k2eAgMnSc1d1	hlavní
konkurent	konkurent	k1gMnSc1	konkurent
Hill	Hill	k1gMnSc1	Hill
oba	dva	k4xCgInPc4	dva
závody	závod	k1gInPc4	závod
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
průběžném	průběžný	k2eAgNnSc6d1	průběžné
pořadí	pořadí	k1gNnSc6	pořadí
Poháru	pohár	k1gInSc2	pohár
jezdců	jezdec	k1gMnPc2	jezdec
zdramatizovala	zdramatizovat	k5eAaPmAgFnS	zdramatizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Evropy	Evropa	k1gFnSc2	Evropa
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
a	a	k8xC	a
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
souboji	souboj	k1gInSc6	souboj
porazil	porazit	k5eAaPmAgMnS	porazit
svého	svůj	k3xOyFgMnSc4	svůj
rivala	rival	k1gMnSc4	rival
Damona	Damon	k1gMnSc4	Damon
Hilla	Hill	k1gMnSc4	Hill
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
na	na	k7c4	na
japonské	japonský	k2eAgFnPc4d1	japonská
grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc4	prix
odstartoval	odstartovat	k5eAaPmAgMnS	odstartovat
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
na	na	k7c6	na
mokru	mokro	k1gNnSc6	mokro
prohrál	prohrát	k5eAaPmAgInS	prohrát
souboj	souboj	k1gInSc1	souboj
s	s	k7c7	s
Hillem	Hill	k1gInSc7	Hill
během	během	k7c2	během
zastávek	zastávka	k1gFnPc2	zastávka
v	v	k7c6	v
boxech	box	k1gInPc6	box
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
posledního	poslední	k2eAgInSc2d1	poslední
závodu	závod	k1gInSc2	závod
nastupoval	nastupovat	k5eAaImAgMnS	nastupovat
s	s	k7c7	s
jednobodovým	jednobodový	k2eAgInSc7d1	jednobodový
náskokem	náskok	k1gInSc7	náskok
před	před	k7c7	před
Britem	Brit	k1gMnSc7	Brit
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
porazil	porazit	k5eAaPmAgMnS	porazit
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
v	v	k7c6	v
35	[number]	k4	35
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
narazil	narazit	k5eAaPmAgMnS	narazit
do	do	k7c2	do
zdi	zeď	k1gFnSc2	zeď
<g/>
,	,	kIx,	,
s	s	k7c7	s
poškozeným	poškozený	k2eAgInSc7d1	poškozený
vozem	vůz	k1gInSc7	vůz
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
na	na	k7c4	na
trať	trať	k1gFnSc4	trať
<g/>
,	,	kIx,	,
Hill	Hill	k1gMnSc1	Hill
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
snažil	snažit	k5eAaImAgMnS	snažit
v	v	k7c6	v
pravotočivé	pravotočivý	k2eAgFnSc6d1	pravotočivá
zatáčce	zatáčka	k1gFnSc6	zatáčka
podjet	podjet	k5eAaPmF	podjet
<g/>
,	,	kIx,	,
Němec	Němec	k1gMnSc1	Němec
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nenechal	nechat	k5eNaPmAgInS	nechat
dostatek	dostatek	k1gInSc1	dostatek
místa	místo	k1gNnSc2	místo
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
kolize	kolize	k1gFnSc1	kolize
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
oba	dva	k4xCgInPc1	dva
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
<g/>
;	;	kIx,	;
šampionem	šampion	k1gMnSc7	šampion
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
Němec	Němec	k1gMnSc1	Němec
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1995	[number]	k4	1995
====	====	k?	====
</s>
</p>
<p>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
zahájil	zahájit	k5eAaPmAgMnS	zahájit
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
motorem	motor	k1gInSc7	motor
Renault	renault	k1gInSc1	renault
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
používali	používat	k5eAaImAgMnP	používat
i	i	k9	i
jeho	jeho	k3xOp3gMnPc1	jeho
konkurenti	konkurent	k1gMnPc1	konkurent
z	z	k7c2	z
Williamsu	Williams	k1gInSc2	Williams
<g/>
,	,	kIx,	,
a	a	k8xC	a
obtížně	obtížně	k6eAd1	obtížně
ovladatelným	ovladatelný	k2eAgInSc7d1	ovladatelný
vozem	vůz	k1gInSc7	vůz
B	B	kA	B
<g/>
195	[number]	k4	195
<g/>
,	,	kIx,	,
vítězně	vítězně	k6eAd1	vítězně
závodem	závod	k1gInSc7	závod
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
byl	být	k5eAaImAgInS	být
posléze	posléze	k6eAd1	posléze
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Coulthardem	Coulthard	k1gInSc7	Coulthard
diskvalifikován	diskvalifikovat	k5eAaBmNgMnS	diskvalifikovat
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
nepovoleného	povolený	k2eNgNnSc2d1	nepovolené
paliva	palivo	k1gNnSc2	palivo
<g/>
;	;	kIx,	;
po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
FIA	FIA	kA	FIA
diskvalifikaci	diskvalifikace	k1gFnSc3	diskvalifikace
zrušila	zrušit	k5eAaPmAgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Velké	velký	k2eAgFnSc6d1	velká
ceně	cena	k1gFnSc6	cena
San	San	k1gFnPc2	San
Marina	Marina	k1gFnSc1	Marina
se	se	k3xPyFc4	se
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
průběžného	průběžný	k2eAgNnSc2d1	průběžné
pořadí	pořadí	k1gNnSc2	pořadí
dostal	dostat	k5eAaPmAgMnS	dostat
Damon	Damon	k1gMnSc1	Damon
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
když	když	k8xS	když
Schumacher	Schumachra	k1gFnPc2	Schumachra
s	s	k7c7	s
hladkými	hladký	k2eAgFnPc7d1	hladká
pneumatikami	pneumatika	k1gFnPc7	pneumatika
havaroval	havarovat	k5eAaPmAgMnS	havarovat
na	na	k7c6	na
mokré	mokrý	k2eAgFnSc6d1	mokrá
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgMnSc1d1	německý
pilot	pilot	k1gMnSc1	pilot
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
závod	závod	k1gInSc4	závod
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
a	a	k8xC	a
vedení	vedení	k1gNnSc2	vedení
si	se	k3xPyFc3	se
už	už	k6eAd1	už
nenechal	nechat	k5eNaPmAgMnS	nechat
vzít	vzít	k5eAaPmF	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
v	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
<g/>
,	,	kIx,	,
když	když	k8xS	když
vedoucí	vedoucí	k1gMnSc1	vedoucí
Hill	Hill	k1gMnSc1	Hill
zvolil	zvolit	k5eAaPmAgMnS	zvolit
špatnou	špatný	k2eAgFnSc4d1	špatná
strategii	strategie	k1gFnSc4	strategie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Kanady	Kanada	k1gFnSc2	Kanada
startoval	startovat	k5eAaBmAgMnS	startovat
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
šance	šance	k1gFnPc4	šance
na	na	k7c4	na
vítězství	vítězství	k1gNnSc4	vítězství
ovšem	ovšem	k9	ovšem
zhatily	zhatit	k5eAaPmAgInP	zhatit
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
převodovkou	převodovka	k1gFnSc7	převodovka
a	a	k8xC	a
pedálem	pedál	k1gInSc7	pedál
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
spekulovat	spekulovat	k5eAaImF	spekulovat
o	o	k7c6	o
Schumacherově	Schumacherův	k2eAgFnSc6d1	Schumacherova
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
,	,	kIx,	,
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
právo	právo	k1gNnSc1	právo
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
uplatní	uplatnit	k5eAaPmIp3nS	uplatnit
Mercedes	mercedes	k1gInSc1	mercedes
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
ovšem	ovšem	k9	ovšem
dosud	dosud	k6eAd1	dosud
neměl	mít	k5eNaImAgInS	mít
kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
Weber	Weber	k1gMnSc1	Weber
začal	začít	k5eAaPmAgMnS	začít
jednat	jednat	k5eAaImF	jednat
s	s	k7c7	s
Ferrari	Ferrari	k1gMnSc7	Ferrari
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
prezident	prezident	k1gMnSc1	prezident
Fiatu	fiat	k1gInSc2	fiat
Gianni	Gianň	k1gMnSc3	Gianň
Agnelli	Agnell	k1gMnSc3	Agnell
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Schumacher	Schumachra	k1gFnPc2	Schumachra
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
s	s	k7c7	s
italskou	italský	k2eAgFnSc7d1	italská
stájí	stáj	k1gFnSc7	stáj
dvouletý	dvouletý	k2eAgInSc1d1	dvouletý
kontrakt	kontrakt	k1gInSc1	kontrakt
s	s	k7c7	s
ročním	roční	k2eAgInSc7d1	roční
platem	plat	k1gInSc7	plat
25	[number]	k4	25
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vyjednávání	vyjednávání	k1gNnSc2	vyjednávání
měl	mít	k5eAaImAgInS	mít
údajně	údajně	k6eAd1	údajně
zasahovat	zasahovat	k5eAaImF	zasahovat
i	i	k9	i
Bernie	Bernie	k1gFnSc1	Bernie
Ecclestone	Eccleston	k1gInSc5	Eccleston
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
zájem	zájem	k1gInSc4	zájem
na	na	k7c6	na
návratu	návrat	k1gInSc6	návrat
maranellského	maranellský	k2eAgInSc2d1	maranellský
týmu	tým	k1gInSc2	tým
na	na	k7c6	na
výsluní	výsluní	k1gNnSc6	výsluní
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
místa	místo	k1gNnSc2	místo
díky	díky	k7c3	díky
rychlým	rychlý	k2eAgInPc3d1	rychlý
časům	čas	k1gInPc3	čas
před	před	k7c7	před
první	první	k4xOgFnSc7	první
zastávkou	zastávka	k1gFnSc7	zastávka
v	v	k7c6	v
boxech	box	k1gInPc6	box
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
v	v	k7c4	v
46	[number]	k4	46
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
v	v	k7c6	v
zatáčce	zatáčka	k1gFnSc6	zatáčka
Priory	prior	k1gMnPc7	prior
pokusil	pokusit	k5eAaPmAgInS	pokusit
předjet	předjet	k2eAgInSc1d1	předjet
na	na	k7c6	na
první	první	k4xOgFnSc6	první
místo	místo	k7c2	místo
Hill	Hilla	k1gFnPc2	Hilla
<g/>
,	,	kIx,	,
narazil	narazit	k5eAaPmAgMnS	narazit
do	do	k7c2	do
boku	bok	k1gInSc2	bok
Němcova	Němcův	k2eAgInSc2d1	Němcův
monopostu	monopost	k1gInSc2	monopost
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
vyřadil	vyřadit	k5eAaPmAgInS	vyřadit
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
páté	pátý	k4xOgFnSc6	pátý
sezoně	sezona	k1gFnSc6	sezona
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
před	před	k7c7	před
domácími	domácí	k2eAgMnPc7d1	domácí
fanoušky	fanoušek	k1gMnPc7	fanoušek
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
prvním	první	k4xOgMnSc7	první
německý	německý	k2eAgMnSc1d1	německý
pilotem	pilot	k1gMnSc7	pilot
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
to	ten	k3xDgNnSc4	ten
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
měl	mít	k5eAaImAgInS	mít
nejprve	nejprve	k6eAd1	nejprve
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
tankováním	tankování	k1gNnSc7	tankování
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
s	s	k7c7	s
elektronikou	elektronika	k1gFnSc7	elektronika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jej	on	k3xPp3gMnSc4	on
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
ze	z	k7c2	z
závodu	závod	k1gInSc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
na	na	k7c4	na
Velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
Belgie	Belgie	k1gFnSc2	Belgie
podle	podle	k7c2	podle
traťových	traťový	k2eAgMnPc2d1	traťový
komisařů	komisař	k1gMnPc2	komisař
blokoval	blokovat	k5eAaImAgMnS	blokovat
Hilla	Hill	k1gMnSc4	Hill
<g/>
,	,	kIx,	,
za	za	k7c4	za
trest	trest	k1gInSc4	trest
musel	muset	k5eAaImAgInS	muset
startovat	startovat	k5eAaBmF	startovat
až	až	k9	až
ze	z	k7c2	z
šestnácté	šestnáctý	k4xOgFnSc2	šestnáctý
pozice	pozice	k1gFnSc2	pozice
<g/>
,	,	kIx,	,
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
stíhací	stíhací	k2eAgFnSc6d1	stíhací
jízdě	jízda	k1gFnSc6	jízda
však	však	k9	však
slavil	slavit	k5eAaImAgMnS	slavit
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
vyřadil	vyřadit	k5eAaPmAgInS	vyřadit
při	při	k7c6	při
dobrzďování	dobrzďování	k1gNnSc6	dobrzďování
Hill	Hill	k1gInSc4	Hill
Schumachera	Schumachero	k1gNnSc2	Schumachero
i	i	k8xC	i
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Souboj	souboj	k1gInSc1	souboj
jezdců	jezdec	k1gMnPc2	jezdec
Benettonu	Benetton	k1gInSc2	Benetton
a	a	k8xC	a
Williamsu	Williams	k1gInSc2	Williams
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
v	v	k7c6	v
Estrorilu	Estroril	k1gInSc6	Estroril
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Němec	Němec	k1gMnSc1	Němec
Brita	Brit	k1gMnSc4	Brit
předjel	předjet	k5eAaPmAgMnS	předjet
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
závody	závod	k1gInPc4	závod
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
z	z	k7c2	z
třetího	třetí	k4xOgNnSc2	třetí
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
startu	start	k1gInSc6	start
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
Japonska	Japonsko	k1gNnSc2	Japonsko
triumfoval	triumfovat	k5eAaBmAgInS	triumfovat
způsobem	způsob	k1gInSc7	způsob
start-cíl	startíla	k1gFnPc2	start-cíla
<g/>
,	,	kIx,	,
devátým	devátý	k4xOgNnSc7	devátý
vítězstvím	vítězství	k1gNnSc7	vítězství
vyrovnal	vyrovnat	k5eAaPmAgInS	vyrovnat
Mansellův	Mansellův	k2eAgInSc1d1	Mansellův
rekord	rekord	k1gInSc1	rekord
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
výher	výhra	k1gFnPc2	výhra
za	za	k7c4	za
sezonu	sezona	k1gFnSc4	sezona
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Schumacher	Schumachra	k1gFnPc2	Schumachra
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
podruhé	podruhé	k6eAd1	podruhé
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
již	již	k9	již
před	před	k7c7	před
závěrečným	závěrečný	k2eAgInSc7d1	závěrečný
závodem	závod	k1gInSc7	závod
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Austrálie	Austrálie	k1gFnSc2	Austrálie
odjížděl	odjíždět	k5eAaImAgMnS	odjíždět
jako	jako	k9	jako
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
šampion	šampion	k1gMnSc1	šampion
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
při	při	k7c6	při
boji	boj	k1gInSc6	boj
o	o	k7c4	o
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
kolidoval	kolidovat	k5eAaImAgMnS	kolidovat
s	s	k7c7	s
Alesim	Alesi	k1gNnSc7	Alesi
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc4	titul
mu	on	k3xPp3gMnSc3	on
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
jedenáct	jedenáct	k4xCc1	jedenáct
pódiových	pódiový	k2eAgFnPc2d1	pódiová
umístění	umístění	k1gNnPc2	umístění
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
devětkrát	devětkrát	k6eAd1	devětkrát
slavil	slavit	k5eAaImAgInS	slavit
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
mu	on	k3xPp3gMnSc3	on
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
i	i	k9	i
osm	osm	k4xCc4	osm
nejrychlejších	rychlý	k2eAgNnPc2d3	nejrychlejší
kol	kolo	k1gNnPc2	kolo
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
získal	získat	k5eAaPmAgInS	získat
102	[number]	k4	102
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Ferrari	Ferrari	k1gMnSc1	Ferrari
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
1996	[number]	k4	1996
====	====	k?	====
</s>
</p>
<p>
<s>
Když	když	k8xS	když
Schumacher	Schumachra	k1gFnPc2	Schumachra
poprvé	poprvé	k6eAd1	poprvé
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
sídla	sídlo	k1gNnSc2	sídlo
Ferrari	Ferrari	k1gMnSc2	Ferrari
<g/>
,	,	kIx,	,
vítalo	vítat	k5eAaImAgNnS	vítat
jej	on	k3xPp3gMnSc4	on
4	[number]	k4	4
500	[number]	k4	500
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
týmového	týmový	k2eAgMnSc4d1	týmový
kolegu	kolega	k1gMnSc4	kolega
si	se	k3xPyFc3	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
Eddieho	Eddie	k1gMnSc4	Eddie
Irvina	Irvin	k1gMnSc4	Irvin
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
tým	tým	k1gInSc1	tým
musel	muset	k5eAaImAgInS	muset
vykoupit	vykoupit	k5eAaPmF	vykoupit
ze	z	k7c2	z
smlouvy	smlouva	k1gFnSc2	smlouva
u	u	k7c2	u
Jordanu	Jordan	k1gMnSc6	Jordan
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
sportovním	sportovní	k2eAgMnSc7d1	sportovní
ředitelem	ředitel	k1gMnSc7	ředitel
Jeanem	Jean	k1gMnSc7	Jean
Todtem	Todt	k1gMnSc7	Todt
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
první	první	k4xOgFnSc4	první
sezonu	sezona	k1gFnSc4	sezona
stanovili	stanovit	k5eAaPmAgMnP	stanovit
cíl	cíl	k1gInSc4	cíl
–	–	k?	–
dvě	dva	k4xCgFnPc4	dva
výhry	výhra	k1gFnPc1	výhra
<g/>
.	.	kIx.	.
</s>
<s>
Schumacherovo	Schumacherův	k2eAgNnSc1d1	Schumacherovo
Ferrari	ferrari	k1gNnSc1	ferrari
nestačilo	stačit	k5eNaBmAgNnS	stačit
na	na	k7c4	na
konkurenční	konkurenční	k2eAgInSc4d1	konkurenční
Williams	Williams	k1gInSc4	Williams
a	a	k8xC	a
bojovalo	bojovat	k5eAaImAgNnS	bojovat
s	s	k7c7	s
Benettonem	Benetton	k1gInSc7	Benetton
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yIgMnSc4	který
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
závod	závod	k1gInSc1	závod
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
kvůli	kvůli	k7c3	kvůli
úniku	únik	k1gInSc3	únik
brzdové	brzdový	k2eAgFnSc2d1	brzdová
kapaliny	kapalina	k1gFnSc2	kapalina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
první	první	k4xOgNnSc4	první
pódiové	pódiový	k2eAgNnSc4d1	pódiové
umístění	umístění	k1gNnSc4	umístění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
kvůli	kvůli	k7c3	kvůli
poškození	poškození	k1gNnSc3	poškození
zadního	zadní	k2eAgInSc2d1	zadní
spoileru	spoiler	k1gInSc2	spoiler
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
týmu	tým	k1gInSc2	tým
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
postupně	postupně	k6eAd1	postupně
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
rychlé	rychlý	k2eAgInPc4d1	rychlý
pitstopy	pitstop	k1gInPc4	pitstop
na	na	k7c6	na
Nürburgringu	Nürburgring	k1gInSc6	Nürburgring
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
díky	díky	k7c3	díky
nim	on	k3xPp3gFnPc3	on
dojel	dojet	k5eAaPmAgMnS	dojet
druhý	druhý	k4xOgMnSc1	druhý
za	za	k7c7	za
Villeneuvem	Villeneuv	k1gInSc7	Villeneuv
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
pole	pole	k1gFnSc1	pole
position	position	k1gInSc1	position
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
z	z	k7c2	z
Maranella	Maranello	k1gNnSc2	Maranello
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
San	San	k1gFnSc2	San
Marina	Marina	k1gFnSc1	Marina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kvůli	kvůli	k7c3	kvůli
prasklé	prasklý	k2eAgFnSc3d1	prasklá
pneumatice	pneumatika	k1gFnSc3	pneumatika
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
kole	kolo	k1gNnSc6	kolo
dojel	dojet	k5eAaPmAgMnS	dojet
druhý	druhý	k4xOgMnSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
místo	místo	k1gNnSc1	místo
na	na	k7c6	na
startu	start	k1gInSc6	start
vyjel	vyjet	k5eAaPmAgInS	vyjet
i	i	k9	i
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Monaka	Monako	k1gNnSc2	Monako
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
špatném	špatný	k2eAgInSc6d1	špatný
startu	start	k1gInSc6	start
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
italskou	italský	k2eAgFnSc7d1	italská
stáj	stáj	k1gFnSc1	stáj
provázely	provázet	k5eAaImAgInP	provázet
celou	celý	k2eAgFnSc4d1	celá
sezonu	sezona	k1gFnSc4	sezona
kvůli	kvůli	k7c3	kvůli
špatné	špatný	k2eAgFnSc3d1	špatná
převodovce	převodovka	k1gFnSc3	převodovka
<g/>
,	,	kIx,	,
naboural	nabourat	k5eAaPmAgMnS	nabourat
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
do	do	k7c2	do
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
vítězství	vítězství	k1gNnSc1	vítězství
přišlo	přijít	k5eAaPmAgNnS	přijít
v	v	k7c6	v
sedmém	sedmý	k4xOgInSc6	sedmý
závodě	závod	k1gInSc6	závod
<g/>
,	,	kIx,	,
do	do	k7c2	do
deštivé	deštivý	k2eAgFnSc2d1	deštivá
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Španělska	Španělsko	k1gNnSc2	Španělsko
startoval	startovat	k5eAaBmAgMnS	startovat
ze	z	k7c2	z
třetího	třetí	k4xOgNnSc2	třetí
místa	místo	k1gNnSc2	místo
a	a	k8xC	a
přes	přes	k7c4	přes
prasklý	prasklý	k2eAgInSc4d1	prasklý
výfuk	výfuk	k1gInSc4	výfuk
druhého	druhý	k4xOgMnSc2	druhý
Alesiho	Alesi	k1gMnSc2	Alesi
porazil	porazit	k5eAaPmAgInS	porazit
o	o	k7c6	o
45	[number]	k4	45
s.	s.	k?	s.
<g/>
Do	do	k7c2	do
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Kanady	Kanada	k1gFnSc2	Kanada
startoval	startovat	k5eAaBmAgMnS	startovat
z	z	k7c2	z
posledního	poslední	k2eAgNnSc2d1	poslední
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
zaváděcím	zaváděcí	k2eAgNnSc6d1	zaváděcí
kole	kolo	k1gNnSc6	kolo
zhasl	zhasnout	k5eAaPmAgInS	zhasnout
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
velké	velký	k2eAgFnSc6d1	velká
ceně	cena	k1gFnSc6	cena
jej	on	k3xPp3gMnSc4	on
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
v	v	k7c6	v
42	[number]	k4	42
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
porucha	porucha	k1gFnSc1	porucha
řazení	řazení	k1gNnSc2	řazení
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
závadě	závada	k1gFnSc3	závada
motoru	motor	k1gInSc2	motor
v	v	k7c6	v
zahřívacím	zahřívací	k2eAgNnSc6d1	zahřívací
kole	kolo	k1gNnSc6	kolo
do	do	k7c2	do
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Francie	Francie	k1gFnSc2	Francie
ani	ani	k8xC	ani
neodstartoval	odstartovat	k5eNaPmAgMnS	odstartovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
ho	on	k3xPp3gMnSc4	on
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
kolech	kolo	k1gNnPc6	kolo
zastavila	zastavit	k5eAaPmAgFnS	zastavit
hydraulika	hydraulika	k1gFnSc1	hydraulika
převodovky	převodovka	k1gFnSc2	převodovka
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
domácími	domácí	k2eAgMnPc7d1	domácí
fanoušky	fanoušek	k1gMnPc7	fanoušek
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
dojel	dojet	k5eAaPmAgMnS	dojet
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
pitstopem	pitstop	k1gInSc7	pitstop
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
<g/>
,	,	kIx,	,
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
příčku	příčka	k1gFnSc4	příčka
na	na	k7c6	na
Hungaroringu	Hungaroring	k1gInSc6	Hungaroring
mu	on	k3xPp3gMnSc3	on
překazil	překazit	k5eAaPmAgInS	překazit
zlomený	zlomený	k2eAgInSc1d1	zlomený
plynový	plynový	k2eAgInSc1d1	plynový
pedál	pedál	k1gInSc1	pedál
a	a	k8xC	a
Němec	Němec	k1gMnSc1	Němec
tak	tak	k6eAd1	tak
dojel	dojet	k5eAaPmAgMnS	dojet
devátý	devátý	k4xOgMnSc1	devátý
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
triumfy	triumf	k1gInPc4	triumf
za	za	k7c7	za
sebou	se	k3xPyFc7	se
ze	z	k7c2	z
třetího	třetí	k4xOgNnSc2	třetí
místa	místo	k1gNnSc2	místo
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dvanáct	dvanáct	k4xCc1	dvanáct
kol	kolo	k1gNnPc2	kolo
před	před	k7c7	před
cílem	cíl	k1gInSc7	cíl
v	v	k7c6	v
Eau	Eau	k1gFnSc6	Eau
Rouge	rouge	k1gFnSc2	rouge
předjel	předjet	k5eAaPmAgMnS	předjet
Villeneuva	Villeneuv	k1gMnSc4	Villeneuv
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
před	před	k7c7	před
domácími	domácí	k2eAgMnPc7d1	domácí
fanoušky	fanoušek	k1gMnPc7	fanoušek
Scuderie	Scuderie	k1gFnSc2	Scuderie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
dojel	dojet	k5eAaPmAgMnS	dojet
po	po	k7c6	po
předjetí	předjetí	k1gNnSc6	předjetí
Villeneuvem	Villeneuv	k1gMnSc7	Villeneuv
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
v	v	k7c4	v
japonské	japonský	k2eAgFnPc4d1	japonská
Suzuce	Suzuce	k1gFnPc4	Suzuce
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
druhou	druhý	k4xOgFnSc4	druhý
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
s	s	k7c7	s
59	[number]	k4	59
body	bod	k1gInPc7	bod
a	a	k8xC	a
třemi	tři	k4xCgFnPc7	tři
výhrami	výhra	k1gFnPc7	výhra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
všichni	všechen	k3xTgMnPc1	všechen
jezdci	jezdec	k1gMnPc1	jezdec
Ferrari	Ferrari	k1gMnPc1	Ferrari
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kvalifikacích	kvalifikace	k1gFnPc6	kvalifikace
porazil	porazit	k5eAaPmAgMnS	porazit
Irvina	Irvina	k1gMnSc1	Irvina
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1997	[number]	k4	1997
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
představil	představit	k5eAaPmAgMnS	představit
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
Ralf	Ralf	k1gMnSc1	Ralf
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
dominoval	dominovat	k5eAaImAgInS	dominovat
Williams	Williams	k1gInSc1	Williams
<g/>
.	.	kIx.	.
</s>
<s>
Ferrari	ferrari	k1gNnSc1	ferrari
nasadilo	nasadit	k5eAaPmAgNnS	nasadit
nový	nový	k2eAgInSc4d1	nový
vůz	vůz	k1gInSc4	vůz
F	F	kA	F
<g/>
310	[number]	k4	310
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
zkonstruovali	zkonstruovat	k5eAaPmAgMnP	zkonstruovat
již	již	k6eAd1	již
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
za	za	k7c7	za
Schumacherem	Schumacher	k1gMnSc7	Schumacher
z	z	k7c2	z
Benettonu	Benetton	k1gInSc2	Benetton
<g/>
,	,	kIx,	,
např.	např.	kA	např.
technický	technický	k2eAgMnSc1d1	technický
ředitel	ředitel	k1gMnSc1	ředitel
Ross	Rossa	k1gFnPc2	Rossa
Brawn	Brawn	k1gMnSc1	Brawn
<g/>
,	,	kIx,	,
designér	designér	k1gMnSc1	designér
Rory	Rora	k1gFnSc2	Rora
Byrne	Byrn	k1gInSc5	Byrn
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodní	úvodní	k2eAgFnSc6d1	úvodní
Velké	velký	k2eAgFnSc6d1	velká
ceně	cena	k1gFnSc6	cena
Austrálie	Austrálie	k1gFnSc2	Austrálie
dojel	dojet	k5eAaPmAgInS	dojet
druhý	druhý	k4xOgInSc1	druhý
za	za	k7c7	za
Davidem	David	k1gMnSc7	David
Coulthardem	Coulthard	k1gMnSc7	Coulthard
s	s	k7c7	s
McLarenem	McLaren	k1gInSc7	McLaren
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
po	po	k7c6	po
startu	start	k1gInSc6	start
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
místa	místo	k1gNnSc2	místo
zpočátku	zpočátku	k6eAd1	zpočátku
vedl	vést	k5eAaImAgInS	vést
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skončil	skončit	k5eAaPmAgMnS	skončit
až	až	k9	až
pátý	pátý	k4xOgMnSc1	pátý
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Argentiny	Argentina	k1gFnSc2	Argentina
kolidoval	kolidovat	k5eAaImAgMnS	kolidovat
po	po	k7c6	po
startu	start	k1gInSc6	start
s	s	k7c7	s
Rubensem	Rubens	k1gMnSc7	Rubens
Barrichellem	Barrichell	k1gMnSc7	Barrichell
<g/>
.	.	kIx.	.
</s>
<s>
Vylepšený	vylepšený	k2eAgInSc4d1	vylepšený
monopost	monopost	k1gInSc4	monopost
mu	on	k3xPp3gMnSc3	on
přinesl	přinést	k5eAaPmAgMnS	přinést
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
San	San	k1gFnSc2	San
Marina	Marina	k1gFnSc1	Marina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Monackém	monacký	k2eAgNnSc6d1	Monacké
knížectví	knížectví	k1gNnSc6	knížectví
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
díky	díky	k7c3	díky
strategickému	strategický	k2eAgNnSc3d1	strategické
přezutí	přezutí	k1gNnSc3	přezutí
pneumatik	pneumatika	k1gFnPc2	pneumatika
do	do	k7c2	do
mokra	mokro	k1gNnSc2	mokro
na	na	k7c6	na
startu	start	k1gInSc6	start
před	před	k7c7	před
Barrichellem	Barrichell	k1gInSc7	Barrichell
o	o	k7c4	o
53	[number]	k4	53
sekund	sekunda	k1gFnPc2	sekunda
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
u	u	k7c2	u
Ferrari	Ferrari	k1gMnSc2	Ferrari
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
šampionátu	šampionát	k1gInSc2	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
dokončil	dokončit	k5eAaPmAgMnS	dokončit
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
Francii	Francie	k1gFnSc6	Francie
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
jak	jak	k8xS	jak
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgInS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
v	v	k7c6	v
průběžném	průběžný	k2eAgNnSc6d1	průběžné
pořadí	pořadí	k1gNnSc6	pořadí
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc7	svůj
pětadvacátou	pětadvacátý	k4xOgFnSc7	pětadvacátý
výhrou	výhra	k1gFnSc7	výhra
překonal	překonat	k5eAaPmAgMnS	překonat
Fangia	Fangia	k1gFnSc1	Fangia
a	a	k8xC	a
vyrovnal	vyrovnat	k5eAaBmAgMnS	vyrovnat
Clarka	Clarko	k1gNnPc4	Clarko
a	a	k8xC	a
Laudu	Lauda	k1gFnSc4	Lauda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
v	v	k7c6	v
39	[number]	k4	39
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
kvůli	kvůli	k7c3	kvůli
poruše	porucha	k1gFnSc3	porucha
ložiska	ložisko	k1gNnSc2	ložisko
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
částečně	částečně	k6eAd1	částečně
nefunkční	funkční	k2eNgFnSc3d1	nefunkční
převodovce	převodovka	k1gFnSc3	převodovka
dojel	dojet	k5eAaPmAgMnS	dojet
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
druhý	druhý	k4xOgInSc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
dokončil	dokončit	k5eAaPmAgMnS	dokončit
na	na	k7c6	na
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
v	v	k7c6	v
dešti	dešť	k1gInSc6	dešť
opět	opět	k6eAd1	opět
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
při	pře	k1gFnSc3	pře
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
když	když	k8xS	když
za	za	k7c4	za
šest	šest	k4xCc4	šest
kol	kolo	k1gNnPc2	kolo
vyjel	vyjet	k5eAaPmAgMnS	vyjet
náskok	náskok	k1gInSc4	náskok
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
sekund	sekunda	k1gFnPc2	sekunda
před	před	k7c7	před
zbytkem	zbytek	k1gInSc7	zbytek
jezdeckého	jezdecký	k2eAgNnSc2d1	jezdecké
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
zde	zde	k6eAd1	zde
již	již	k9	již
počtvrté	počtvrté	k4xO	počtvrté
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
dvě	dva	k4xCgNnPc1	dva
šestá	šestý	k4xOgNnPc1	šestý
místa	místo	k1gNnPc1	místo
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
penalizován	penalizovat	k5eAaImNgInS	penalizovat
za	za	k7c4	za
předjetí	předjetí	k1gNnSc4	předjetí
Frentzena	Frentzen	k2eAgNnPc4d1	Frentzen
pod	pod	k7c7	pod
žlutými	žlutý	k2eAgFnPc7d1	žlutá
vlajkami	vlajka	k1gFnPc7	vlajka
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
jej	on	k3xPp3gMnSc4	on
porazil	porazit	k5eAaPmAgMnS	porazit
bratr	bratr	k1gMnSc1	bratr
Ralf	Ralf	k1gMnSc1	Ralf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
mladší	mladý	k2eAgMnSc1d2	mladší
sourozenec	sourozenec	k1gMnSc1	sourozenec
<g/>
,	,	kIx,	,
Schumacher	Schumachra	k1gFnPc2	Schumachra
tak	tak	k8xS	tak
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
průběžné	průběžný	k2eAgNnSc4d1	průběžné
vedení	vedení	k1gNnSc4	vedení
Poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgMnS	vrátit
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
Schumacher	Schumachra	k1gFnPc2	Schumachra
nastupoval	nastupovat	k5eAaImAgInS	nastupovat
s	s	k7c7	s
jednobodovým	jednobodový	k2eAgInSc7d1	jednobodový
náskokem	náskok	k1gInSc7	náskok
před	před	k7c7	před
druhým	druhý	k4xOgInSc7	druhý
Villeneuvem	Villeneuv	k1gInSc7	Villeneuv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
sedm	sedm	k4xCc4	sedm
závodů	závod	k1gInPc2	závod
oproti	oproti	k7c3	oproti
Schumacherovým	Schumacherův	k2eAgFnPc3d1	Schumacherova
pěti	pět	k4xCc3	pět
<g/>
.	.	kIx.	.
</s>
<s>
Schumacher	Schumachra	k1gFnPc2	Schumachra
vedl	vést	k5eAaImAgInS	vést
do	do	k7c2	do
48	[number]	k4	48
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
zpomalil	zpomalit	k5eAaPmAgInS	zpomalit
kvůli	kvůli	k7c3	kvůli
zvyšující	zvyšující	k2eAgFnSc3d1	zvyšující
se	se	k3xPyFc4	se
teplotě	teplota	k1gFnSc3	teplota
motoru	motor	k1gInSc2	motor
<g/>
,	,	kIx,	,
Villeneuve	Villeneuev	k1gFnPc1	Villeneuev
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
pitstopu	pitstop	k1gInSc2	pitstop
zrychlil	zrychlit	k5eAaPmAgMnS	zrychlit
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
pilota	pilot	k1gMnSc2	pilot
Ferrari	Ferrari	k1gMnSc2	Ferrari
předjet	předjet	k5eAaPmNgInS	předjet
v	v	k7c6	v
pravotočivé	pravotočivý	k2eAgFnSc6d1	pravotočivá
zatáčce	zatáčka	k1gFnSc6	zatáčka
Dry	Dry	k1gMnSc1	Dry
Sack	Sack	k1gMnSc1	Sack
<g/>
,	,	kIx,	,
Němec	Němec	k1gMnSc1	Němec
hájil	hájit	k5eAaImAgMnS	hájit
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
pozici	pozice	k1gFnSc4	pozice
manévrem	manévr	k1gInSc7	manévr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
podobal	podobat	k5eAaImAgMnS	podobat
tomu	ten	k3xDgMnSc3	ten
z	z	k7c2	z
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Austrálie	Austrálie	k1gFnSc2	Austrálie
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
a	a	k8xC	a
do	do	k7c2	do
Williamsu	Williams	k1gInSc2	Williams
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
ideální	ideální	k2eAgFnSc6d1	ideální
stopě	stopa	k1gFnSc6	stopa
o	o	k7c4	o
půl	půl	k1xP	půl
délky	délka	k1gFnSc2	délka
vozu	vůz	k1gInSc2	vůz
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
najel	najet	k5eAaPmAgMnS	najet
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
vyřadit	vyřadit	k5eAaPmF	vyřadit
jej	on	k3xPp3gMnSc4	on
ze	z	k7c2	z
závodu	závod	k1gInSc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
Schumacher	Schumachra	k1gFnPc2	Schumachra
s	s	k7c7	s
poškozeným	poškozený	k2eAgNnSc7d1	poškozené
předním	přední	k2eAgNnSc7d1	přední
zavěšením	zavěšení	k1gNnSc7	zavěšení
skončil	skončit	k5eAaPmAgMnS	skončit
v	v	k7c6	v
kačírku	kačírek	k1gInSc6	kačírek
<g/>
,	,	kIx,	,
Villeneuve	Villeneuev	k1gFnPc4	Villeneuev
závod	závod	k1gInSc1	závod
dokončil	dokončit	k5eAaPmAgInS	dokončit
třetí	třetí	k4xOgInSc4	třetí
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
boxů	box	k1gInPc2	box
se	se	k3xPyFc4	se
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
,	,	kIx,	,
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
nevině	nevina	k1gFnSc6	nevina
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
,	,	kIx,	,
po	po	k7c6	po
zhlédnutí	zhlédnutí	k1gNnSc6	zhlédnutí
videa	video	k1gNnSc2	video
však	však	k8xC	však
uznal	uznat	k5eAaPmAgInS	uznat
svou	svůj	k3xOyFgFnSc4	svůj
vinu	vina	k1gFnSc4	vina
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
nesportovní	sportovní	k2eNgNnSc4d1	nesportovní
chování	chování	k1gNnSc4	chování
chtěla	chtít	k5eAaImAgFnS	chtít
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
automobilová	automobilový	k2eAgFnSc1d1	automobilová
federace	federace	k1gFnSc1	federace
potrestat	potrestat	k5eAaPmF	potrestat
zákazem	zákaz	k1gInSc7	zákaz
startu	start	k1gInSc2	start
v	v	k7c6	v
úvodních	úvodní	k2eAgNnPc6d1	úvodní
dvou	dva	k4xCgNnPc6	dva
grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc4	prix
příští	příští	k2eAgFnSc2d1	příští
sezony	sezona	k1gFnSc2	sezona
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
před	před	k7c7	před
závodem	závod	k1gInSc7	závod
naznačoval	naznačovat	k5eAaImAgMnS	naznačovat
Bernie	Bernie	k1gFnPc4	Bernie
Ecclestone	Eccleston	k1gInSc5	Eccleston
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
zvolila	zvolit	k5eAaPmAgFnS	zvolit
diskvalifikaci	diskvalifikace	k1gFnSc4	diskvalifikace
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
šampionátu	šampionát	k1gInSc2	šampionát
<g/>
,	,	kIx,	,
Schumacher	Schumachra	k1gFnPc2	Schumachra
si	se	k3xPyFc3	se
však	však	k9	však
mohl	moct	k5eAaImAgMnS	moct
ponechat	ponechat	k5eAaPmF	ponechat
vítězství	vítězství	k1gNnSc4	vítězství
a	a	k8xC	a
body	bod	k1gInPc4	bod
v	v	k7c6	v
osobních	osobní	k2eAgFnPc6d1	osobní
statistikách	statistika	k1gFnPc6	statistika
<g/>
.	.	kIx.	.
</s>
<s>
Schumacherovy	Schumacherův	k2eAgInPc1d1	Schumacherův
body	bod	k1gInPc1	bod
se	se	k3xPyFc4	se
započítaly	započítat	k5eAaPmAgInP	započítat
i	i	k9	i
týmu	tým	k1gInSc2	tým
Ferrari	Ferrari	k1gMnSc2	Ferrari
v	v	k7c6	v
Poháru	pohár	k1gInSc6	pohár
konstruktérů	konstruktér	k1gMnPc2	konstruktér
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
musel	muset	k5eAaImAgInS	muset
odpracovat	odpracovat	k5eAaPmF	odpracovat
šedesát	šedesát	k4xCc4	šedesát
hodin	hodina	k1gFnPc2	hodina
veřejně	veřejně	k6eAd1	veřejně
prospěšných	prospěšný	k2eAgFnPc2d1	prospěšná
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
diváků	divák	k1gMnPc2	divák
poté	poté	k6eAd1	poté
u	u	k7c2	u
hesenského	hesenský	k2eAgInSc2d1	hesenský
soudu	soud	k1gInSc2	soud
podal	podat	k5eAaPmAgMnS	podat
žalobu	žaloba	k1gFnSc4	žaloba
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
manévr	manévr	k1gInSc4	manévr
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
zabití	zabití	k1gNnSc4	zabití
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
však	však	k9	však
zamítnuta	zamítnout	k5eAaPmNgFnS	zamítnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1998	[number]	k4	1998
====	====	k?	====
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
monopost	monopost	k1gInSc1	monopost
italské	italský	k2eAgFnSc2d1	italská
stáje	stáj	k1gFnSc2	stáj
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
s	s	k7c7	s
konkurencí	konkurence	k1gFnSc7	konkurence
<g/>
,	,	kIx,	,
McLaren	McLarna	k1gFnPc2	McLarna
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
dodavatelem	dodavatel	k1gMnSc7	dodavatel
pneumatik	pneumatika	k1gFnPc2	pneumatika
Bridgestone	Bridgeston	k1gInSc5	Bridgeston
a	a	k8xC	a
sezona	sezona	k1gFnSc1	sezona
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
téměř	téměř	k6eAd1	téměř
rozhodnuta	rozhodnout	k5eAaPmNgFnS	rozhodnout
<g/>
.	.	kIx.	.
</s>
<s>
Převahu	převaha	k1gFnSc4	převaha
McLarenu	McLaren	k1gInSc2	McLaren
dokazovalo	dokazovat	k5eAaImAgNnS	dokazovat
i	i	k9	i
dvanáct	dvanáct	k4xCc4	dvanáct
pole	pole	k1gNnSc2	pole
position	position	k1gInSc1	position
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
třinácti	třináct	k4xCc2	třináct
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodní	úvodní	k2eAgFnSc6d1	úvodní
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Austrálie	Austrálie	k1gFnSc2	Austrálie
Schumacher	Schumachra	k1gFnPc2	Schumachra
vypadl	vypadnout	k5eAaPmAgInS	vypadnout
s	s	k7c7	s
poruchou	porucha	k1gFnSc7	porucha
motoru	motor	k1gInSc2	motor
již	již	k6eAd1	již
v	v	k7c6	v
šestém	šestý	k4xOgInSc6	šestý
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
skončil	skončit	k5eAaPmAgMnS	skončit
v	v	k7c6	v
Interlagosu	Interlagos	k1gInSc6	Interlagos
<g/>
.	.	kIx.	.
</s>
<s>
Prvního	první	k4xOgNnSc2	první
vítězství	vítězství	k1gNnSc2	vítězství
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
podniku	podnik	k1gInSc6	podnik
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
snížil	snížit	k5eAaPmAgInS	snížit
náskok	náskok	k1gInSc4	náskok
Häkkinena	Häkkineno	k1gNnSc2	Häkkineno
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
San	San	k1gFnSc2	San
Marina	Marina	k1gFnSc1	Marina
dokončil	dokončit	k5eAaPmAgInS	dokončit
druhý	druhý	k4xOgInSc4	druhý
a	a	k8xC	a
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
odstupem	odstup	k1gInSc7	odstup
třetí	třetí	k4xOgFnSc2	třetí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
při	při	k7c6	při
předjíždění	předjíždění	k1gNnSc6	předjíždění
Wurze	Wurze	k1gFnSc2	Wurze
poškodil	poškodit	k5eAaPmAgInS	poškodit
monopost	monopost	k1gInSc1	monopost
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
desátý	desátý	k4xOgMnSc1	desátý
<g/>
;	;	kIx,	;
po	po	k7c6	po
šesti	šest	k4xCc6	šest
závodech	závod	k1gInPc6	závod
figuroval	figurovat	k5eAaImAgInS	figurovat
v	v	k7c4	v
průběžné	průběžný	k2eAgNnSc4d1	průběžné
pořadí	pořadí	k1gNnSc4	pořadí
šampionátu	šampionát	k1gInSc2	šampionát
až	až	k6eAd1	až
třetí	třetí	k4xOgInSc1	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
tři	tři	k4xCgFnPc1	tři
vítězné	vítězný	k2eAgFnPc1d1	vítězná
velké	velký	k2eAgFnPc1d1	velká
ceny	cena	k1gFnPc1	cena
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
průběžné	průběžný	k2eAgNnSc4d1	průběžné
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
ceně	cena	k1gFnSc6	cena
Kanady	Kanada	k1gFnSc2	Kanada
přes	přes	k7c4	přes
trest	trest	k1gInSc4	trest
stop	stopa	k1gFnPc2	stopa
<g/>
&	&	k?	&
<g/>
go	go	k?	go
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
za	za	k7c4	za
kontroverzní	kontroverzní	k2eAgNnSc4d1	kontroverzní
vytlačení	vytlačení	k1gNnSc4	vytlačení
Frentzena	Frentzen	k2eAgFnSc1d1	Frentzena
vyjíždějícího	vyjíždějící	k2eAgInSc2d1	vyjíždějící
z	z	k7c2	z
boxové	boxový	k2eAgFnSc2d1	boxová
uličky	ulička	k1gFnSc2	ulička
<g/>
,	,	kIx,	,
předjel	předjet	k5eAaPmAgMnS	předjet
Fisichellu	Fisichella	k1gFnSc4	Fisichella
a	a	k8xC	a
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
souboji	souboj	k1gInSc6	souboj
porazil	porazit	k5eAaPmAgInS	porazit
oba	dva	k4xCgInPc4	dva
McLareny	McLaren	k1gInPc4	McLaren
<g/>
;	;	kIx,	;
Scuderia	Scuderium	k1gNnSc2	Scuderium
zde	zde	k6eAd1	zde
oslavila	oslavit	k5eAaPmAgFnS	oslavit
první	první	k4xOgInSc4	první
double	double	k1gInSc4	double
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Velké	velká	k1gFnSc2	velká
Británie	Británie	k1gFnSc1	Británie
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
závodu	závod	k1gInSc2	závod
dostal	dostat	k5eAaPmAgInS	dostat
desetisekundový	desetisekundový	k2eAgInSc1d1	desetisekundový
trest	trest	k1gInSc1	trest
stop	stopa	k1gFnPc2	stopa
<g/>
&	&	k?	&
<g/>
go	go	k?	go
za	za	k7c4	za
předjetí	předjetí	k1gNnSc4	předjetí
Wurze	Wurze	k1gFnSc2	Wurze
pod	pod	k7c7	pod
žlutými	žlutý	k2eAgFnPc7d1	žlutá
vlajkami	vlajka	k1gFnPc7	vlajka
<g/>
;	;	kIx,	;
odpykat	odpykat	k5eAaPmF	odpykat
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
přijel	přijet	k5eAaPmAgInS	přijet
na	na	k7c4	na
Todtův	Todtův	k2eAgInSc4d1	Todtův
pokyn	pokyn	k1gInSc4	pokyn
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
okruhu	okruh	k1gInSc6	okruh
<g/>
,	,	kIx,	,
cílem	cíl	k1gInSc7	cíl
tak	tak	k8xC	tak
projel	projet	k5eAaPmAgMnS	projet
v	v	k7c6	v
uličce	ulička	k1gFnSc6	ulička
před	před	k7c7	před
boxy	box	k1gInPc7	box
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
boxové	boxový	k2eAgNnSc1d1	boxové
stání	stání	k1gNnSc1	stání
týmu	tým	k1gInSc2	tým
z	z	k7c2	z
Maranella	Maranell	k1gMnSc2	Maranell
bylo	být	k5eAaImAgNnS	být
až	až	k9	až
za	za	k7c7	za
cílovou	cílový	k2eAgFnSc7d1	cílová
čarou	čára	k1gFnSc7	čára
<g/>
,	,	kIx,	,
desetisekundový	desetisekundový	k2eAgInSc4d1	desetisekundový
trest	trest	k1gInSc4	trest
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
odečten	odečten	k2eAgMnSc1d1	odečten
až	až	k9	až
po	po	k7c4	po
projetí	projetí	k1gNnSc4	projetí
"	"	kIx"	"
<g/>
cílem	cíl	k1gInSc7	cíl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k8xC	tak
prvním	první	k4xOgNnSc7	první
a	a	k8xC	a
doposud	doposud	k6eAd1	doposud
jediným	jediný	k2eAgMnSc7d1	jediný
pilotem	pilot	k1gMnSc7	pilot
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
se	se	k3xPyFc4	se
něco	něco	k3yInSc1	něco
podobného	podobný	k2eAgMnSc2d1	podobný
povedlo	povést	k5eAaPmAgNnS	povést
<g/>
,	,	kIx,	,
Häkkinenův	Häkkinenův	k2eAgInSc4d1	Häkkinenův
náskok	náskok	k1gInSc4	náskok
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
po	po	k7c6	po
dvojnásobném	dvojnásobný	k2eAgNnSc6d1	dvojnásobné
vyjetí	vyjetí	k1gNnSc6	vyjetí
z	z	k7c2	z
tratě	trať	k1gFnSc2	trať
dojel	dojet	k5eAaPmAgMnS	dojet
třetí	třetí	k4xOgMnSc1	třetí
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
domácí	domácí	k1gMnSc1	domácí
grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc4	prix
startoval	startovat	k5eAaBmAgMnS	startovat
po	po	k7c6	po
problémech	problém	k1gInPc6	problém
s	s	k7c7	s
vozem	vůz	k1gInSc7	vůz
devátý	devátý	k4xOgMnSc1	devátý
<g/>
,	,	kIx,	,
za	za	k7c4	za
páté	pátý	k4xOgNnSc4	pátý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
cíli	cíl	k1gInSc6	cíl
inkasoval	inkasovat	k5eAaBmAgInS	inkasovat
dva	dva	k4xCgInPc4	dva
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
taktikou	taktika	k1gFnSc7	taktika
tří	tři	k4xCgFnPc2	tři
zastávek	zastávka	k1gFnPc2	zastávka
v	v	k7c6	v
boxech	box	k1gInPc6	box
a	a	k8xC	a
překonal	překonat	k5eAaPmAgMnS	překonat
svým	svůj	k3xOyFgInSc7	svůj
32	[number]	k4	32
<g/>
.	.	kIx.	.
triumfem	triumf	k1gInSc7	triumf
Nigela	Nigel	k1gMnSc2	Nigel
Mansella	Mansell	k1gMnSc2	Mansell
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Belgie	Belgie	k1gFnSc2	Belgie
vedl	vést	k5eAaImAgMnS	vést
s	s	k7c7	s
náskokem	náskok	k1gInSc7	náskok
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
předjíždění	předjíždění	k1gNnSc6	předjíždění
pomalého	pomalý	k2eAgMnSc2d1	pomalý
Coultharda	Coulthard	k1gMnSc2	Coulthard
o	o	k7c4	o
kolo	kolo	k1gNnSc4	kolo
Skot	skot	k1gInSc4	skot
nečekaně	nečekaně	k6eAd1	nečekaně
zpomalil	zpomalit	k5eAaPmAgInS	zpomalit
v	v	k7c6	v
kaluži	kaluž	k1gFnSc6	kaluž
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
Schumacher	Schumachra	k1gFnPc2	Schumachra
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
urazil	urazit	k5eAaPmAgInS	urazit
přední	přední	k2eAgNnPc4d1	přední
kolo	kolo	k1gNnSc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
boxů	box	k1gInPc2	box
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
pilota	pilot	k1gMnSc2	pilot
McLarenu	McLarena	k1gFnSc4	McLarena
napadnout	napadnout	k5eAaPmF	napadnout
<g/>
;	;	kIx,	;
Coulthard	Coulthard	k1gInSc1	Coulthard
se	se	k3xPyFc4	se
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
chybu	chyba	k1gFnSc4	chyba
omluvil	omluvit	k5eAaPmAgMnS	omluvit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
kraloval	kralovat	k5eAaImAgMnS	kralovat
Schumacher	Schumachra	k1gFnPc2	Schumachra
a	a	k8xC	a
o	o	k7c4	o
vedení	vedení	k1gNnSc4	vedení
v	v	k7c6	v
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
dělil	dělit	k5eAaImAgMnS	dělit
s	s	k7c7	s
Finem	Fin	k1gMnSc7	Fin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vrátil	vrátit	k5eAaPmAgMnS	vrátit
porážku	porážka	k1gFnSc4	porážka
rodákovi	rodákův	k2eAgMnPc1d1	rodákův
z	z	k7c2	z
Kerpenu	Kerpen	k2eAgFnSc4d1	Kerpen
v	v	k7c6	v
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
mistrovském	mistrovský	k2eAgInSc6d1	mistrovský
titulu	titul	k1gInSc6	titul
se	se	k3xPyFc4	se
rozhodovalo	rozhodovat	k5eAaImAgNnS	rozhodovat
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
závodě	závod	k1gInSc6	závod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
hostila	hostit	k5eAaImAgFnS	hostit
japonská	japonský	k2eAgFnSc1d1	japonská
Suzuka	Suzuk	k1gMnSc4	Suzuk
<g/>
.	.	kIx.	.
</s>
<s>
Schumacher	Schumachra	k1gFnPc2	Schumachra
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
<g/>
,	,	kIx,	,
v	v	k7c6	v
zahřívacím	zahřívací	k2eAgNnSc6d1	zahřívací
kole	kolo	k1gNnSc6	kolo
zůstal	zůstat	k5eAaPmAgInS	zůstat
stát	stát	k5eAaPmF	stát
na	na	k7c6	na
startu	start	k1gInSc6	start
<g/>
,	,	kIx,	,
při	při	k7c6	při
stíhací	stíhací	k2eAgFnSc6d1	stíhací
jízdě	jízda	k1gFnSc6	jízda
z	z	k7c2	z
posledního	poslední	k2eAgNnSc2d1	poslední
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
roštu	rošt	k1gInSc6	rošt
mu	on	k3xPp3gMnSc3	on
na	na	k7c6	na
průběžném	průběžný	k2eAgNnSc6d1	průběžné
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
32	[number]	k4	32
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
explodovala	explodovat	k5eAaBmAgFnS	explodovat
pravá	pravý	k2eAgFnSc1d1	pravá
zadní	zadní	k2eAgFnSc1d1	zadní
pneumatika	pneumatika	k1gFnSc1	pneumatika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
celkové	celkový	k2eAgNnSc4d1	celkové
pořadí	pořadí	k1gNnSc4	pořadí
šampionátu	šampionát	k1gInSc2	šampionát
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
druhý	druhý	k4xOgMnSc1	druhý
s	s	k7c7	s
86	[number]	k4	86
body	bod	k1gInPc7	bod
<g/>
,	,	kIx,	,
šesti	šest	k4xCc7	šest
triumfy	triumf	k1gInPc7	triumf
a	a	k8xC	a
třemi	tři	k4xCgNnPc7	tři
pole	pole	k1gNnSc4	pole
position	position	k1gInSc4	position
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1999	[number]	k4	1999
====	====	k?	====
</s>
</p>
<p>
<s>
Ferrari	Ferrari	k1gMnSc1	Ferrari
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
sezony	sezona	k1gFnSc2	sezona
nasadilo	nasadit	k5eAaPmAgNnS	nasadit
nový	nový	k2eAgInSc4d1	nový
monopost	monopost	k1gInSc4	monopost
F	F	kA	F
<g/>
399	[number]	k4	399
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Austrálie	Austrálie	k1gFnSc2	Austrálie
Schumacher	Schumachra	k1gFnPc2	Schumachra
zůstal	zůstat	k5eAaPmAgInS	zůstat
stát	stát	k1gInSc1	stát
na	na	k7c6	na
startu	start	k1gInSc6	start
a	a	k8xC	a
po	po	k7c6	po
explozi	exploze	k1gFnSc6	exploze
pneumatiky	pneumatika	k1gFnSc2	pneumatika
v	v	k7c6	v
31	[number]	k4	31
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
dokončil	dokončit	k5eAaPmAgMnS	dokončit
na	na	k7c6	na
nebodované	bodovaný	k2eNgFnSc6d1	nebodovaná
osmé	osmý	k4xOgFnSc6	osmý
pozici	pozice	k1gFnSc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Brazílie	Brazílie	k1gFnSc2	Brazílie
vedl	vést	k5eAaImAgMnS	vést
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rychlejší	rychlý	k2eAgInSc1d2	rychlejší
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
jej	on	k3xPp3gMnSc4	on
předjel	předjet	k5eAaPmAgInS	předjet
a	a	k8xC	a
Schumacher	Schumachra	k1gFnPc2	Schumachra
dojel	dojet	k5eAaPmAgInS	dojet
druhý	druhý	k4xOgInSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
prvního	první	k4xOgMnSc2	první
Häkkinena	Häkkinen	k1gMnSc2	Häkkinen
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
GP	GP	kA	GP
San	San	k1gFnSc1	San
Marina	Marina	k1gFnSc1	Marina
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
průběžného	průběžný	k2eAgNnSc2d1	průběžné
vedení	vedení	k1gNnSc2	vedení
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rychlém	rychlý	k2eAgInSc6d1	rychlý
startu	start	k1gInSc6	start
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
ceně	cena	k1gFnSc6	cena
Monaka	Monako	k1gNnSc2	Monako
se	se	k3xPyFc4	se
protáhl	protáhnout	k5eAaPmAgInS	protáhnout
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgNnSc6	který
vydržel	vydržet	k5eAaPmAgMnS	vydržet
až	až	k6eAd1	až
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Šestnáctou	šestnáctý	k4xOgFnSc7	šestnáctý
výhrou	výhra	k1gFnSc7	výhra
za	za	k7c4	za
Ferrari	ferrari	k1gNnSc4	ferrari
překonal	překonat	k5eAaPmAgMnS	překonat
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
vítězství	vítězství	k1gNnSc2	vítězství
jednoho	jeden	k4xCgMnSc2	jeden
jezdce	jezdec	k1gMnSc2	jezdec
u	u	k7c2	u
italské	italský	k2eAgFnSc2d1	italská
stáje	stáj	k1gFnSc2	stáj
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
držel	držet	k5eAaImAgMnS	držet
Niki	Nik	k1gFnSc3	Nik
Lauda	Lauda	k1gMnSc1	Lauda
s	s	k7c7	s
patnácti	patnáct	k4xCc7	patnáct
vavříny	vavřín	k1gInPc7	vavřín
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
jej	on	k3xPp3gMnSc4	on
oba	dva	k4xCgInPc1	dva
stříbrné	stříbrný	k2eAgInPc1d1	stříbrný
šípy	šíp	k1gInPc1	šíp
předčily	předčít	k5eAaPmAgInP	předčít
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
nevydržel	vydržet	k5eNaPmAgMnS	vydržet
tlak	tlak	k1gInSc4	tlak
dotírajícího	dotírající	k2eAgNnSc2d1	dotírající
Häkkinena	Häkkineno	k1gNnSc2	Häkkineno
a	a	k8xC	a
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
narazil	narazit	k5eAaPmAgInS	narazit
ve	v	k7c6	v
třicátém	třicátý	k4xOgNnSc6	třicátý
kole	kolo	k1gNnSc6	kolo
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
zdi	zeď	k1gFnSc2	zeď
šampionů	šampion	k1gMnPc2	šampion
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
ztratil	ztratit	k5eAaPmAgMnS	ztratit
vedení	vedení	k1gNnSc4	vedení
v	v	k7c6	v
šampionátu	šampionát	k1gInSc6	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
propršené	propršený	k2eAgFnSc6d1	propršená
Francii	Francie	k1gFnSc6	Francie
vedl	vést	k5eAaImAgMnS	vést
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
problémech	problém	k1gInPc6	problém
s	s	k7c7	s
elektronikou	elektronika	k1gFnSc7	elektronika
skončil	skončit	k5eAaPmAgInS	skončit
pátý	pátý	k4xOgMnSc1	pátý
<g/>
,	,	kIx,	,
když	když	k8xS	když
prohrál	prohrát	k5eAaPmAgInS	prohrát
souboj	souboj	k1gInSc1	souboj
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
<g/>
.	.	kIx.	.
<g/>
Sezóna	sezóna	k1gFnSc1	sezóna
1999	[number]	k4	1999
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
rudé	rudý	k2eAgInPc4d1	rudý
vozy	vůz	k1gInPc4	vůz
dobře	dobře	k6eAd1	dobře
rozjetá	rozjetý	k2eAgFnSc1d1	rozjetá
i	i	k9	i
díky	díky	k7c3	díky
Eddiemu	Eddiem	k1gMnSc3	Eddiem
Irvinovi	Irvin	k1gMnSc3	Irvin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
až	až	k6eAd1	až
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
ceny	cena	k1gFnSc2	cena
Británie	Británie	k1gFnSc2	Británie
měl	mít	k5eAaImAgInS	mít
stejně	stejně	k6eAd1	stejně
bodů	bod	k1gInPc2	bod
jako	jako	k8xS	jako
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
Silverstonu	Silverston	k1gInSc6	Silverston
havaroval	havarovat	k5eAaPmAgMnS	havarovat
kvůli	kvůli	k7c3	kvůli
selhání	selhání	k1gNnSc3	selhání
brzd	brzda	k1gFnPc2	brzda
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ztráty	ztráta	k1gFnSc2	ztráta
hydraulického	hydraulický	k2eAgInSc2d1	hydraulický
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
zlomeninu	zlomenina	k1gFnSc4	zlomenina
lýtkové	lýtkový	k2eAgFnSc2d1	lýtková
a	a	k8xC	a
holenní	holenní	k2eAgFnSc2d1	holenní
kosti	kost	k1gFnSc2	kost
pravé	pravý	k2eAgFnSc2d1	pravá
nohy	noha	k1gFnSc2	noha
v	v	k7c6	v
zatáčce	zatáčka	k1gFnSc6	zatáčka
Stowe	Stow	k1gMnSc2	Stow
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
závodu	závod	k1gInSc2	závod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
přerušen	přerušit	k5eAaPmNgInS	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
zdi	zeď	k1gFnSc2	zeď
obložené	obložený	k2eAgNnSc1d1	obložené
modrobílou	modrobílý	k2eAgFnSc7d1	modrobílá
hradbou	hradba	k1gFnSc7	hradba
pneumatik	pneumatika	k1gFnPc2	pneumatika
narazil	narazit	k5eAaPmAgMnS	narazit
v	v	k7c6	v
rychlosti	rychlost	k1gFnSc6	rychlost
107	[number]	k4	107
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Zranění	zranění	k1gNnSc1	zranění
léčil	léčit	k5eAaImAgInS	léčit
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
nemohl	moct	k5eNaImAgMnS	moct
tak	tak	k6eAd1	tak
startovat	startovat	k5eAaBmF	startovat
v	v	k7c6	v
šesti	šest	k4xCc6	šest
závodech	závod	k1gInPc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
poslední	poslední	k2eAgFnPc4d1	poslední
dvě	dva	k4xCgFnPc4	dva
velké	velký	k2eAgFnPc4d1	velká
ceny	cena	k1gFnPc4	cena
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
obě	dva	k4xCgFnPc4	dva
kvalifikace	kvalifikace	k1gFnPc4	kvalifikace
<g/>
,	,	kIx,	,
v	v	k7c6	v
závodech	závod	k1gInPc6	závod
se	se	k3xPyFc4	se
jako	jako	k9	jako
týmová	týmový	k2eAgFnSc1d1	týmová
dvojka	dvojka	k1gFnSc1	dvojka
snažil	snažit	k5eAaImAgMnS	snažit
dopomoci	dopomoc	k1gFnSc2	dopomoc
k	k	k7c3	k
titulu	titul	k1gInSc3	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
svému	svůj	k3xOyFgMnSc3	svůj
irskému	irský	k2eAgMnSc3d1	irský
kolegovi	kolega	k1gMnSc3	kolega
<g/>
;	;	kIx,	;
v	v	k7c6	v
Malajsii	Malajsie	k1gFnSc6	Malajsie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
pustil	pustit	k5eAaPmAgMnS	pustit
Irvina	Irvina	k1gMnSc1	Irvina
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
dojel	dojet	k5eAaPmAgMnS	dojet
druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
Ferrari	ferrari	k1gNnSc1	ferrari
bylo	být	k5eAaImAgNnS	být
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
a	a	k8xC	a
Schumacher	Schumachra	k1gFnPc2	Schumachra
měl	mít	k5eAaImAgMnS	mít
možnost	možnost	k1gFnSc4	možnost
předjet	předjet	k5eAaPmF	předjet
Häkkinena	Häkkinen	k1gMnSc4	Häkkinen
a	a	k8xC	a
pro	pro	k7c4	pro
Irvina	Irvin	k1gMnSc4	Irvin
zajistit	zajistit	k5eAaPmF	zajistit
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
o	o	k7c6	o
Schumacherově	Schumacherův	k2eAgFnSc6d1	Schumacherova
neochotě	neochota	k1gFnSc6	neochota
pomoci	pomoc	k1gFnSc2	pomoc
Irovi	Ir	k1gMnSc3	Ir
k	k	k7c3	k
titulu	titul	k1gInSc2	titul
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
do	do	k7c2	do
seriálu	seriál	k1gInSc2	seriál
vrátit	vrátit	k5eAaPmF	vrátit
již	již	k9	již
při	při	k7c6	při
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
absenci	absence	k1gFnSc4	absence
v	v	k7c6	v
části	část	k1gFnSc6	část
sezony	sezona	k1gFnSc2	sezona
v	v	k7c6	v
šampionátu	šampionát	k1gInSc6	šampionát
obsadil	obsadit	k5eAaPmAgMnS	obsadit
páté	pátý	k4xOgNnSc4	pátý
místo	místo	k1gNnSc4	místo
se	s	k7c7	s
44	[number]	k4	44
body	bod	k1gInPc7	bod
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
výhrami	výhra	k1gFnPc7	výhra
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Irvinem	Irvin	k1gMnSc7	Irvin
vybojovali	vybojovat	k5eAaPmAgMnP	vybojovat
výhru	výhra	k1gFnSc4	výhra
v	v	k7c6	v
Poháru	pohár	k1gInSc6	pohár
konstruktérů	konstruktér	k1gMnPc2	konstruktér
pro	pro	k7c4	pro
Ferrari	ferrari	k1gNnSc4	ferrari
po	po	k7c6	po
šestnácti	šestnáct	k4xCc6	šestnáct
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
2000	[number]	k4	2000
====	====	k?	====
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
měl	mít	k5eAaImAgInS	mít
Schumacher	Schumachra	k1gFnPc2	Schumachra
nového	nový	k2eAgMnSc4d1	nový
týmového	týmový	k2eAgMnSc4d1	týmový
kolegu	kolega	k1gMnSc4	kolega
<g/>
,	,	kIx,	,
Rubense	Rubens	k1gMnSc4	Rubens
Barrichella	Barrichell	k1gMnSc4	Barrichell
<g/>
.	.	kIx.	.
</s>
<s>
Sezonu	sezona	k1gFnSc4	sezona
zahájil	zahájit	k5eAaPmAgMnS	zahájit
třemi	tři	k4xCgFnPc7	tři
výhrami	výhra	k1gFnPc7	výhra
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Velké	velká	k1gFnSc2	velká
Británie	Británie	k1gFnSc2	Británie
jej	on	k3xPp3gMnSc4	on
porazily	porazit	k5eAaPmAgInP	porazit
oba	dva	k4xCgInPc1	dva
McLareny	McLaren	k1gInPc1	McLaren
<g/>
,	,	kIx,	,
Schumacher	Schumachra	k1gFnPc2	Schumachra
dojel	dojet	k5eAaPmAgInS	dojet
třetí	třetí	k4xOgInSc1	třetí
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
GP	GP	kA	GP
Španělska	Španělsko	k1gNnSc2	Španělsko
získaly	získat	k5eAaPmAgInP	získat
stříbrné	stříbrný	k2eAgInPc1d1	stříbrný
šípy	šíp	k1gInPc1	šíp
double	double	k2eAgInPc1d1	double
<g/>
,	,	kIx,	,
Schumacher	Schumachra	k1gFnPc2	Schumachra
se	se	k3xPyFc4	se
po	po	k7c6	po
defektu	defekt	k1gInSc6	defekt
musel	muset	k5eAaImAgMnS	muset
spokojit	spokojit	k5eAaPmF	spokojit
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
body	bod	k1gInPc7	bod
za	za	k7c4	za
páté	pátý	k4xOgNnSc4	pátý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
deštivé	deštivý	k2eAgFnSc6d1	deštivá
GP	GP	kA	GP
Evropy	Evropa	k1gFnSc2	Evropa
porazil	porazit	k5eAaPmAgInS	porazit
z	z	k7c2	z
pole	pole	k1gNnSc2	pole
position	position	k1gInSc1	position
startujícího	startující	k2eAgMnSc2d1	startující
Coultharda	Coulthard	k1gMnSc2	Coulthard
i	i	k8xC	i
svého	svůj	k3xOyFgMnSc2	svůj
největšího	veliký	k2eAgMnSc2d3	veliký
rivala	rival	k1gMnSc2	rival
Miku	Mik	k1gMnSc3	Mik
Häkkinena	Häkkinen	k1gMnSc2	Häkkinen
a	a	k8xC	a
předstihl	předstihnout	k5eAaPmAgMnS	předstihnout
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
získaných	získaný	k2eAgInPc2d1	získaný
bodů	bod	k1gInPc2	bod
Ayrtona	Ayrton	k1gMnSc2	Ayrton
Sennu	Senn	k1gInSc2	Senn
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
ceně	cena	k1gFnSc6	cena
Monaka	Monako	k1gNnSc2	Monako
mu	on	k3xPp3gNnSc3	on
překazila	překazit	k5eAaPmAgFnS	překazit
trhlina	trhlina	k1gFnSc1	trhlina
ve	v	k7c6	v
výfuku	výfuk	k1gInSc6	výfuk
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
roztavila	roztavit	k5eAaPmAgFnS	roztavit
zavěšení	zavěšení	k1gNnSc4	zavěšení
brzd	brzda	k1gFnPc2	brzda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kanadském	kanadský	k2eAgInSc6d1	kanadský
závodě	závod	k1gInSc6	závod
opět	opět	k6eAd1	opět
zapršelo	zapršet	k5eAaPmAgNnS	zapršet
<g/>
,	,	kIx,	,
Němec	Němec	k1gMnSc1	Němec
z	z	k7c2	z
deště	dešť	k1gInSc2	dešť
vytěžil	vytěžit	k5eAaPmAgMnS	vytěžit
další	další	k2eAgNnSc4d1	další
prvenství	prvenství	k1gNnSc4	prvenství
a	a	k8xC	a
náskok	náskok	k1gInSc4	náskok
24	[number]	k4	24
bodů	bod	k1gInPc2	bod
na	na	k7c4	na
Létajícího	létající	k2eAgMnSc4d1	létající
Fina	Fin	k1gMnSc4	Fin
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
VC	VC	kA	VC
Francie	Francie	k1gFnSc2	Francie
shořel	shořet	k5eAaPmAgInS	shořet
Schumacherovi	Schumacher	k1gMnSc3	Schumacher
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
v	v	k7c6	v
první	první	k4xOgFnSc6	první
zatáčce	zatáčka	k1gFnSc6	zatáčka
nabourali	nabourat	k5eAaPmAgMnP	nabourat
Ricardo	Ricardo	k1gNnSc4	Ricardo
Zonta	Zont	k1gInSc2	Zont
a	a	k8xC	a
Jarno	Jarno	k6eAd1	Jarno
Trulli	Trull	k1gMnPc1	Trull
<g/>
,	,	kIx,	,
a	a	k8xC	a
ani	ani	k8xC	ani
v	v	k7c6	v
domácím	domácí	k2eAgInSc6d1	domácí
závodě	závod	k1gInSc6	závod
nepřekonal	překonat	k5eNaPmAgMnS	překonat
nástrahy	nástraha	k1gFnPc1	nástraha
první	první	k4xOgFnSc2	první
zatáčky	zatáčka	k1gFnSc2	zatáčka
a	a	k8xC	a
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
smazal	smazat	k5eAaPmAgInS	smazat
bodovou	bodový	k2eAgFnSc4d1	bodová
ztrátu	ztráta	k1gFnSc4	ztráta
na	na	k7c4	na
německého	německý	k2eAgMnSc4d1	německý
jezdce	jezdec	k1gMnSc4	jezdec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
zatáčce	zatáčka	k1gFnSc3	zatáčka
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
finský	finský	k2eAgMnSc1d1	finský
závodník	závodník	k1gMnSc1	závodník
Schumachera	Schumacher	k1gMnSc4	Schumacher
předjel	předjet	k5eAaPmAgMnS	předjet
a	a	k8xC	a
ujal	ujmout	k5eAaPmAgMnS	ujmout
se	se	k3xPyFc4	se
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
body	bod	k1gInPc4	bod
vedení	vedení	k1gNnSc2	vedení
šampionátu	šampionát	k1gInSc6	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
Němec	Němec	k1gMnSc1	Němec
obsadil	obsadit	k5eAaPmAgMnS	obsadit
druhou	druhý	k4xOgFnSc4	druhý
příčku	příčka	k1gFnSc4	příčka
i	i	k9	i
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gInSc4	on
Häkkinen	Häkkinen	k1gInSc4	Häkkinen
odvážným	odvážný	k2eAgInSc7d1	odvážný
manévrem	manévr	k1gInSc7	manévr
v	v	k7c6	v
zatáčce	zatáčka	k1gFnSc6	zatáčka
Les	les	k1gInSc1	les
Combes	Combesa	k1gFnPc2	Combesa
předjel	předjet	k5eAaPmAgInS	předjet
kolem	kolem	k7c2	kolem
pomalého	pomalý	k2eAgMnSc2d1	pomalý
Zonty	Zonta	k1gMnSc2	Zonta
čtyři	čtyři	k4xCgMnPc4	čtyři
kola	kolo	k1gNnPc4	kolo
před	před	k7c7	před
cílem	cíl	k1gInSc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
VC	VC	kA	VC
Itálie	Itálie	k1gFnSc1	Itálie
Schumacher	Schumachra	k1gFnPc2	Schumachra
podporovaný	podporovaný	k2eAgInSc1d1	podporovaný
tifosi	tifos	k1gMnSc6	tifos
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
i	i	k8xC	i
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vyrovnal	vyrovnat	k5eAaBmAgMnS	vyrovnat
41	[number]	k4	41
vítězství	vítězství	k1gNnSc2	vítězství
Ayrtona	Ayrtona	k1gFnSc1	Ayrtona
Senny	Senny	k?	Senny
<g/>
.	.	kIx.	.
</s>
<s>
Prvenstvím	prvenství	k1gNnPc3	prvenství
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
i	i	k9	i
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
závodě	závod	k1gInSc6	závod
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
po	po	k7c6	po
explozi	exploze	k1gFnSc6	exploze
motoru	motor	k1gInSc2	motor
Häkkinenova	Häkkinenův	k2eAgInSc2d1	Häkkinenův
McLarenu	McLaren	k1gInSc2	McLaren
ujal	ujmout	k5eAaPmAgInS	ujmout
vedení	vedení	k1gNnSc4	vedení
v	v	k7c6	v
průběžném	průběžný	k2eAgNnSc6d1	průběžné
pořadí	pořadí	k1gNnSc6	pořadí
Poháru	pohár	k1gInSc2	pohár
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
si	se	k3xPyFc3	se
jezdec	jezdec	k1gMnSc1	jezdec
Ferrari	Ferrari	k1gMnSc1	Ferrari
vyjel	vyjet	k5eAaPmAgMnS	vyjet
pole	pole	k1gNnSc4	pole
position	position	k1gInSc1	position
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
vynikajícím	vynikající	k2eAgInSc6d1	vynikající
startu	start	k1gInSc6	start
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
vedení	vedení	k1gNnSc4	vedení
Fin.	Fin.	k1gFnSc2	Fin.
Změna	změna	k1gFnSc1	změna
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
nastala	nastat	k5eAaPmAgFnS	nastat
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
pitstopu	pitstop	k1gInSc6	pitstop
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Schumacher	Schumachra	k1gFnPc2	Schumachra
Häkkinena	Häkkineno	k1gNnPc4	Häkkineno
předjel	předjet	k5eAaPmAgInS	předjet
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
vítězství	vítězství	k1gNnSc3	vítězství
a	a	k8xC	a
98	[number]	k4	98
bodům	bod	k1gInPc3	bod
jej	on	k3xPp3gMnSc4	on
Häkkinen	Häkkinen	k1gInSc1	Häkkinen
již	již	k6eAd1	již
nemohl	moct	k5eNaImAgInS	moct
překonat	překonat	k5eAaPmF	překonat
<g/>
,	,	kIx,	,
Schumacher	Schumachra	k1gFnPc2	Schumachra
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
potřetí	potřetí	k4xO	potřetí
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
s	s	k7c7	s
Ferrari	Ferrari	k1gMnSc7	Ferrari
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
jezdecký	jezdecký	k2eAgInSc4d1	jezdecký
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
čekalo	čekat	k5eAaImAgNnS	čekat
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
vítěznou	vítězný	k2eAgFnSc4d1	vítězná
sezonu	sezona	k1gFnSc4	sezona
ve	v	k7c6	v
stáji	stáj	k1gFnSc6	stáj
se	s	k7c7	s
vzpínajícím	vzpínající	k2eAgMnSc7d1	vzpínající
se	se	k3xPyFc4	se
koníkem	koník	k1gMnSc7	koník
završil	završit	k5eAaPmAgInS	završit
prvenstvím	prvenství	k1gNnSc7	prvenství
v	v	k7c6	v
GP	GP	kA	GP
Malajsie	Malajsie	k1gFnSc1	Malajsie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
2001	[number]	k4	2001
====	====	k?	====
</s>
</p>
<p>
<s>
Obhajobu	obhajoba	k1gFnSc4	obhajoba
titulu	titul	k1gInSc2	titul
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
úvodním	úvodní	k2eAgInSc6d1	úvodní
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
australském	australský	k2eAgNnSc6d1	Australské
Melbourne	Melbourne	k1gNnSc6	Melbourne
pole	pole	k1gFnSc1	pole
position	position	k1gInSc1	position
i	i	k9	i
výhrou	výhra	k1gFnSc7	výhra
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sérii	série	k1gFnSc6	série
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
i	i	k9	i
při	při	k7c6	při
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Malajsie	Malajsie	k1gFnSc2	Malajsie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
dešti	dešť	k1gInSc6	dešť
zajížděl	zajíždět	k5eAaImAgInS	zajíždět
kola	kolo	k1gNnPc4	kolo
o	o	k7c4	o
pět	pět	k4xCc4	pět
sekund	sekunda	k1gFnPc2	sekunda
rychlejší	rychlý	k2eAgFnSc1d2	rychlejší
než	než	k8xS	než
zbytek	zbytek	k1gInSc1	zbytek
startovního	startovní	k2eAgNnSc2d1	startovní
pole	pole	k1gNnSc2	pole
<g/>
;	;	kIx,	;
bratři	bratr	k1gMnPc1	bratr
Schumacherové	Schumacherová	k1gFnSc2	Schumacherová
zde	zde	k6eAd1	zde
podvacáté	podvacáté	k4xO	podvacáté
společně	společně	k6eAd1	společně
bodovali	bodovat	k5eAaImAgMnP	bodovat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
předčil	předčit	k5eAaBmAgMnS	předčit
všechny	všechen	k3xTgFnPc4	všechen
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
<g/>
,	,	kIx,	,
v	v	k7c6	v
dešti	dešť	k1gInSc6	dešť
jej	on	k3xPp3gMnSc4	on
kvůli	kvůli	k7c3	kvůli
kolizi	kolize	k1gFnSc3	kolize
s	s	k7c7	s
Montoyou	Montoya	k1gFnSc7	Montoya
porazil	porazit	k5eAaPmAgInS	porazit
Coulthard	Coulthard	k1gInSc1	Coulthard
v	v	k7c6	v
McLarenu	McLaren	k1gInSc6	McLaren
<g/>
.	.	kIx.	.
</s>
<s>
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
San	San	k1gFnSc2	San
Marina	Marina	k1gFnSc1	Marina
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
kvůli	kvůli	k7c3	kvůli
poruše	porucha	k1gFnSc3	porucha
zavěšení	zavěšení	k1gNnSc2	zavěšení
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
výhru	výhra	k1gFnSc4	výhra
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
tu	tu	k6eAd1	tu
získal	získat	k5eAaPmAgMnS	získat
bratr	bratr	k1gMnSc1	bratr
Ralf	Ralf	k1gMnSc1	Ralf
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
až	až	k9	až
do	do	k7c2	do
posledního	poslední	k2eAgNnSc2d1	poslední
kola	kolo	k1gNnSc2	kolo
jezdil	jezdit	k5eAaImAgMnS	jezdit
s	s	k7c7	s
vibrujícím	vibrující	k2eAgInSc7d1	vibrující
vozem	vůz	k1gInSc7	vůz
za	za	k7c7	za
Häkkinenem	Häkkinen	k1gMnSc7	Häkkinen
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
však	však	k9	však
pět	pět	k4xCc4	pět
zatáček	zatáčka	k1gFnPc2	zatáčka
před	před	k7c7	před
cílem	cíl	k1gInSc7	cíl
selhal	selhat	k5eAaPmAgInS	selhat
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
a	a	k8xC	a
Schumacher	Schumachra	k1gFnPc2	Schumachra
tak	tak	k6eAd1	tak
slavil	slavit	k5eAaImAgMnS	slavit
další	další	k2eAgFnSc4d1	další
výhru	výhra	k1gFnSc4	výhra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
po	po	k7c6	po
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
Montoyou	Montoya	k1gMnSc7	Montoya
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gMnSc4	on
vystrčil	vystrčit	k5eAaPmAgMnS	vystrčit
z	z	k7c2	z
trati	trať	k1gFnSc2	trať
<g/>
,	,	kIx,	,
dojel	dojet	k5eAaPmAgMnS	dojet
po	po	k7c6	po
stíhací	stíhací	k2eAgFnSc6d1	stíhací
jízdě	jízda	k1gFnSc6	jízda
druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
týmu	tým	k1gInSc2	tým
na	na	k7c4	na
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
příčku	příčka	k1gFnSc4	příčka
pustil	pustit	k5eAaPmAgMnS	pustit
Barrichello	Barrichello	k1gNnSc4	Barrichello
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
Monaka	Monako	k1gNnSc2	Monako
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
po	po	k7c6	po
problémech	problém	k1gInPc6	problém
obou	dva	k4xCgFnPc6	dva
jezdců	jezdec	k1gMnPc2	jezdec
McLarenu	McLaren	k1gInSc2	McLaren
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
šestou	šestý	k4xOgFnSc4	šestý
z	z	k7c2	z
osmi	osm	k4xCc2	osm
kvalifikací	kvalifikace	k1gFnPc2	kvalifikace
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závodě	závod	k1gInSc6	závod
jej	on	k3xPp3gInSc4	on
porazil	porazit	k5eAaPmAgMnS	porazit
bratr	bratr	k1gMnSc1	bratr
Ralf	Ralf	k1gMnSc1	Ralf
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
tak	tak	k6eAd1	tak
první	první	k4xOgFnPc1	první
dvě	dva	k4xCgFnPc1	dva
pozice	pozice	k1gFnPc1	pozice
v	v	k7c6	v
cíli	cíl	k1gInSc6	cíl
patřily	patřit	k5eAaImAgInP	patřit
bratrské	bratrský	k2eAgInPc1d1	bratrský
dvojici	dvojice	k1gFnSc4	dvojice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Nürburgringu	Nürburgring	k1gInSc6	Nürburgring
i	i	k8xC	i
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
starší	starší	k1gMnSc1	starší
Schumacher	Schumachra	k1gFnPc2	Schumachra
nad	nad	k7c7	nad
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Velké	velká	k1gFnSc2	velká
Británie	Británie	k1gFnSc2	Británie
se	se	k3xPyFc4	se
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
o	o	k7c4	o
titul	titul	k1gInSc4	titul
vložil	vložit	k5eAaPmAgMnS	vložit
svým	svůj	k3xOyFgInSc7	svůj
triumfem	triumf	k1gInSc7	triumf
Létající	létající	k2eAgMnSc1d1	létající
Fin	Fin	k1gMnSc1	Fin
<g/>
,	,	kIx,	,
Schumacher	Schumachra	k1gFnPc2	Schumachra
skončil	skončit	k5eAaPmAgInS	skončit
druhý	druhý	k4xOgInSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Německou	německý	k2eAgFnSc4d1	německá
grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc1	prix
kvůli	kvůli	k7c3	kvůli
ztrátě	ztráta	k1gFnSc3	ztráta
tlaku	tlak	k1gInSc2	tlak
v	v	k7c6	v
palivovém	palivový	k2eAgInSc6d1	palivový
systému	systém	k1gInSc6	systém
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
si	se	k3xPyFc3	se
triumfem	triumf	k1gInSc7	triumf
start-cíl	startíl	k1gMnSc1	start-cíl
zajistil	zajistit	k5eAaPmAgMnS	zajistit
již	již	k6eAd1	již
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
Barrichellovu	Barrichellův	k2eAgNnSc3d1	Barrichellův
druhému	druhý	k4xOgNnSc3	druhý
místu	místo	k1gNnSc3	místo
Scuderia	Scuderium	k1gNnSc2	Scuderium
Ferrari	Ferrari	k1gMnSc7	Ferrari
slavila	slavit	k5eAaImAgFnS	slavit
výhru	výhra	k1gFnSc4	výhra
v	v	k7c6	v
Poháru	pohár	k1gInSc6	pohár
konstruktérů	konstruktér	k1gMnPc2	konstruktér
<g/>
.	.	kIx.	.
52	[number]	k4	52
<g/>
.	.	kIx.	.
vítězstvím	vítězství	k1gNnSc7	vítězství
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Belgie	Belgie	k1gFnSc2	Belgie
překonal	překonat	k5eAaPmAgMnS	překonat
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
vítězství	vítězství	k1gNnSc2	vítězství
Alaina	Alain	k1gMnSc2	Alain
Prosta	prost	k2eAgFnSc1d1	prosta
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
domácí	domácí	k1gMnSc1	domácí
grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc4	prix
pro	pro	k7c4	pro
Ferrari	ferrari	k1gNnSc4	ferrari
ze	z	k7c2	z
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
roštu	rošt	k1gInSc6	rošt
dojel	dojet	k5eAaPmAgMnS	dojet
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
jej	on	k3xPp3gNnSc4	on
předčil	předčít	k5eAaPmAgMnS	předčít
výhrou	výhra	k1gFnSc7	výhra
se	se	k3xPyFc4	se
loučící	loučící	k2eAgInSc4d1	loučící
Häkkinen	Häkkinen	k1gInSc4	Häkkinen
<g/>
.	.	kIx.	.
</s>
<s>
Sezonu	sezona	k1gFnSc4	sezona
završil	završit	k5eAaPmAgInS	završit
triumfem	triumf	k1gInSc7	triumf
start-cíl	startíla	k1gFnPc2	start-cíla
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrté	čtvrtý	k4xOgNnSc1	čtvrtý
prvenství	prvenství	k1gNnSc1	prvenství
mezi	mezi	k7c4	mezi
piloty	pilota	k1gFnPc4	pilota
si	se	k3xPyFc3	se
zajistil	zajistit	k5eAaPmAgInS	zajistit
devíti	devět	k4xCc7	devět
výhrami	výhra	k1gFnPc7	výhra
<g/>
,	,	kIx,	,
jedenácti	jedenáct	k4xCc2	jedenáct
pole	pole	k1gNnSc2	pole
position	position	k1gInSc1	position
a	a	k8xC	a
123	[number]	k4	123
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmnácti	sedmnáct	k4xCc6	sedmnáct
velkých	velký	k2eAgFnPc6d1	velká
cenách	cena	k1gFnPc6	cena
stanul	stanout	k5eAaPmAgInS	stanout
čtrnáctkrát	čtrnáctkrát	k6eAd1	čtrnáctkrát
na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
2002	[number]	k4	2002
====	====	k?	====
</s>
</p>
<p>
<s>
Do	do	k7c2	do
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
závodů	závod	k1gInPc2	závod
sezony	sezona	k1gFnSc2	sezona
2002	[number]	k4	2002
Ferrari	Ferrari	k1gMnPc2	Ferrari
nasadilo	nasadit	k5eAaPmAgNnS	nasadit
loňský	loňský	k2eAgInSc4d1	loňský
vůz	vůz	k1gInSc4	vůz
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
v	v	k7c6	v
úvodní	úvodní	k2eAgFnSc6d1	úvodní
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Austrálie	Austrálie	k1gFnSc2	Austrálie
Schumacher	Schumachra	k1gFnPc2	Schumachra
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
závod	závod	k1gInSc1	závod
v	v	k7c6	v
Malajsii	Malajsie	k1gFnSc6	Malajsie
dokončil	dokončit	k5eAaPmAgMnS	dokončit
třetí	třetí	k4xOgMnSc1	třetí
po	po	k7c6	po
kolizi	kolize	k1gFnSc6	kolize
s	s	k7c7	s
Montoyou	Montoya	k1gFnSc7	Montoya
v	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
již	již	k9	již
s	s	k7c7	s
novým	nový	k2eAgNnSc7d1	nové
F	F	kA	F
<g/>
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
San	San	k1gFnSc6	San
Marinu	Marina	k1gFnSc4	Marina
a	a	k8xC	a
Španělsku	Španělsko	k1gNnSc6	Španělsko
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
i	i	k8xC	i
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Rakouska	Rakousko	k1gNnSc2	Rakousko
jezdil	jezdit	k5eAaImAgMnS	jezdit
druhý	druhý	k4xOgMnSc1	druhý
za	za	k7c7	za
týmovým	týmový	k2eAgMnSc7d1	týmový
kolegou	kolega	k1gMnSc7	kolega
Barrichellem	Barrichell	k1gMnSc7	Barrichell
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mu	on	k3xPp3gMnSc3	on
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
týmu	tým	k1gInSc2	tým
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
zatáčce	zatáčka	k1gFnSc6	zatáčka
přenechal	přenechat	k5eAaPmAgMnS	přenechat
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
vrátil	vrátit	k5eAaPmAgMnS	vrátit
Coulthard	Coulthard	k1gMnSc1	Coulthard
Němci	Němec	k1gMnSc3	Němec
porážku	porážka	k1gFnSc4	porážka
z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
ročníku	ročník	k1gInSc2	ročník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
po	po	k7c6	po
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
kolumbijským	kolumbijský	k2eAgInSc7d1	kolumbijský
pilotem	pilot	k1gInSc7	pilot
Williamsu	Williams	k1gInSc2	Williams
získal	získat	k5eAaPmAgInS	získat
150	[number]	k4	150
<g/>
.	.	kIx.	.
vítězství	vítězství	k1gNnSc3	vítězství
pro	pro	k7c4	pro
Ferrari	Ferrari	k1gMnPc4	Ferrari
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Evropy	Evropa	k1gFnSc2	Evropa
po	po	k7c6	po
smyku	smyk	k1gInSc6	smyk
na	na	k7c6	na
startu	start	k1gInSc6	start
dojel	dojet	k5eAaPmAgMnS	dojet
druhý	druhý	k4xOgMnSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
deštivé	deštivý	k2eAgFnSc6d1	deštivá
britské	britský	k2eAgFnSc6d1	britská
velké	velký	k2eAgFnSc6d1	velká
ceně	cena	k1gFnSc6	cena
po	po	k7c6	po
dalším	další	k2eAgInSc6d1	další
duelu	duel	k1gInSc6	duel
s	s	k7c7	s
Montoyou	Montoya	k1gFnSc7	Montoya
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pět	pět	k4xCc1	pět
kol	kolo	k1gNnPc2	kolo
před	před	k7c7	před
cílem	cíl	k1gInSc7	cíl
předjel	předjet	k5eAaPmAgInS	předjet
chybujícího	chybující	k2eAgNnSc2d1	chybující
Räikkönena	Räikköneno	k1gNnSc2	Räikköneno
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
dojel	dojet	k5eAaPmAgMnS	dojet
pro	pro	k7c4	pro
výhru	výhra	k1gFnSc4	výhra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
znamenala	znamenat	k5eAaImAgFnS	znamenat
zisk	zisk	k1gInSc4	zisk
pátého	pátý	k4xOgInSc2	pátý
titulu	titul	k1gInSc2	titul
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
vyrovnal	vyrovnat	k5eAaBmAgMnS	vyrovnat
Juanu	Juan	k1gMnSc3	Juan
Manuelu	Manuel	k1gMnSc3	Manuel
Fangiovi	Fangius	k1gMnSc3	Fangius
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
Velké	velký	k2eAgFnSc2d1	velká
ceny	cena	k1gFnSc2	cena
Německa	Německo	k1gNnSc2	Německo
získal	získat	k5eAaPmAgMnS	získat
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
Němec	Němec	k1gMnSc1	Němec
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
pole	pole	k1gNnSc2	pole
position	position	k1gInSc1	position
a	a	k8xC	a
před	před	k7c7	před
oslavujícími	oslavující	k2eAgMnPc7d1	oslavující
diváky	divák	k1gMnPc7	divák
svedl	svést	k5eAaPmAgInS	svést
vítězný	vítězný	k2eAgInSc1d1	vítězný
souboj	souboj	k1gInSc1	souboj
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
dokončil	dokončit	k5eAaPmAgMnS	dokončit
jako	jako	k8xC	jako
druhý	druhý	k4xOgMnSc1	druhý
za	za	k7c2	za
Barrichellem	Barrichello	k1gNnSc7	Barrichello
<g/>
,	,	kIx,	,
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Belgie	Belgie	k1gFnSc2	Belgie
si	se	k3xPyFc3	se
dojel	dojet	k5eAaPmAgMnS	dojet
pro	pro	k7c4	pro
rekordní	rekordní	k2eAgNnSc4d1	rekordní
desáté	desátý	k4xOgNnSc4	desátý
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
cenách	cena	k1gFnPc6	cena
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
USA	USA	kA	USA
skončil	skončit	k5eAaPmAgInS	skončit
druhý	druhý	k4xOgInSc1	druhý
za	za	k7c7	za
Barrichellem	Barrichell	k1gInSc7	Barrichell
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
sezonu	sezona	k1gFnSc4	sezona
završil	završit	k5eAaPmAgInS	završit
hattrickem	hattrick	k1gInSc7	hattrick
v	v	k7c4	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Pátý	pátý	k4xOgInSc4	pátý
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
získal	získat	k5eAaPmAgMnS	získat
díky	díky	k7c3	díky
jedenácti	jedenáct	k4xCc3	jedenáct
vítězstvím	vítězství	k1gNnPc3	vítězství
<g/>
,	,	kIx,	,
sedmi	sedm	k4xCc2	sedm
pole	pole	k1gNnSc2	pole
position	position	k1gInSc1	position
<g/>
,	,	kIx,	,
sedmi	sedm	k4xCc3	sedm
nejrychlejším	rychlý	k2eAgMnPc3d3	nejrychlejší
kolům	kolo	k1gNnPc3	kolo
a	a	k8xC	a
144	[number]	k4	144
bodům	bod	k1gInPc3	bod
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
získal	získat	k5eAaPmAgInS	získat
dvojnásobek	dvojnásobek	k1gInSc1	dvojnásobek
bodů	bod	k1gInPc2	bod
svého	svůj	k3xOyFgMnSc2	svůj
týmového	týmový	k2eAgMnSc2d1	týmový
kolegy	kolega	k1gMnSc2	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
sedmnácti	sedmnáct	k4xCc6	sedmnáct
závodech	závod	k1gInPc6	závod
stál	stát	k5eAaImAgMnS	stát
na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
2003	[number]	k4	2003
====	====	k?	====
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
automobilová	automobilový	k2eAgFnSc1d1	automobilová
federace	federace	k1gFnSc1	federace
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
omezit	omezit	k5eAaPmF	omezit
dominantní	dominantní	k2eAgFnSc3d1	dominantní
pozici	pozice	k1gFnSc3	pozice
Ferrari	ferrari	k1gNnSc2	ferrari
změnou	změna	k1gFnSc7	změna
pravidel	pravidlo	k1gNnPc2	pravidlo
bodování	bodování	k1gNnSc6	bodování
a	a	k8xC	a
jednokolovým	jednokolový	k2eAgInSc7d1	jednokolový
formátem	formát	k1gInSc7	formát
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
Ferrari	Ferrari	k1gMnSc1	Ferrari
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2002	[number]	k4	2002
nasadilo	nasadit	k5eAaPmAgNnS	nasadit
na	na	k7c4	na
úvod	úvod	k1gInSc4	úvod
sezony	sezona	k1gFnSc2	sezona
mírně	mírně	k6eAd1	mírně
modifikovaný	modifikovaný	k2eAgInSc4d1	modifikovaný
loňský	loňský	k2eAgInSc4d1	loňský
monopost	monopost	k1gInSc4	monopost
<g/>
.	.	kIx.	.
</s>
<s>
Schumacher	Schumachra	k1gFnPc2	Schumachra
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
odstartoval	odstartovat	k5eAaPmAgMnS	odstartovat
z	z	k7c2	z
pole	pole	k1gNnSc2	pole
position	position	k1gInSc1	position
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
poškození	poškození	k1gNnSc6	poškození
podlahy	podlaha	k1gFnSc2	podlaha
vozu	vůz	k1gInSc2	vůz
dojel	dojet	k5eAaPmAgMnS	dojet
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
a	a	k8xC	a
po	po	k7c6	po
devatenácti	devatenáct	k4xCc6	devatenáct
velkých	velký	k2eAgFnPc6d1	velká
cenách	cena	k1gFnPc6	cena
chyběl	chybět	k5eAaImAgMnS	chybět
na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Malajsii	Malajsie	k1gFnSc6	Malajsie
po	po	k7c6	po
trestu	trest	k1gInSc6	trest
průjezdu	průjezd	k1gInSc2	průjezd
boxy	box	k1gInPc7	box
za	za	k7c4	za
souboj	souboj	k1gInSc4	souboj
s	s	k7c7	s
Trullim	Trulli	k1gNnSc7	Trulli
získal	získat	k5eAaPmAgMnS	získat
šestým	šestý	k4xOgMnSc7	šestý
místem	místem	k6eAd1	místem
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mokré	mokrý	k2eAgFnSc6d1	mokrá
trati	trať	k1gFnSc6	trať
Brazílie	Brazílie	k1gFnSc2	Brazílie
havaroval	havarovat	k5eAaPmAgMnS	havarovat
po	po	k7c6	po
smyku	smyk	k1gInSc6	smyk
v	v	k7c6	v
27	[number]	k4	27
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
výhru	výhra	k1gFnSc4	výhra
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
San	San	k1gFnSc2	San
Marina	Marina	k1gFnSc1	Marina
<g/>
.	.	kIx.	.
</s>
<s>
Radost	radost	k1gFnSc1	radost
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
mu	on	k3xPp3gMnSc3	on
zkalila	zkalit	k5eAaPmAgFnS	zkalit
smrt	smrt	k1gFnSc4	smrt
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
Elisabeth	Elisabetha	k1gFnPc2	Elisabetha
v	v	k7c6	v
dopoledních	dopolední	k2eAgFnPc6d1	dopolední
hodinách	hodina	k1gFnPc6	hodina
v	v	k7c4	v
den	den	k1gInSc4	den
konání	konání	k1gNnSc2	konání
velké	velký	k2eAgFnSc2d1	velká
ceny	cena	k1gFnSc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
výhry	výhra	k1gFnPc1	výhra
přišly	přijít	k5eAaPmAgFnP	přijít
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
představilo	představit	k5eAaPmAgNnS	představit
F	F	kA	F
<g/>
2003	[number]	k4	2003
<g/>
-GA	-GA	k?	-GA
<g/>
,	,	kIx,	,
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
během	během	k7c2	během
pitstopu	pitstop	k1gInSc2	pitstop
vzplanul	vzplanout	k5eAaPmAgMnS	vzplanout
monopost	monopost	k1gInSc4	monopost
<g/>
.	.	kIx.	.
</s>
<s>
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Monaka	Monako	k1gNnSc2	Monako
po	po	k7c6	po
marné	marný	k2eAgFnSc6d1	marná
snaze	snaha	k1gFnSc6	snaha
předjet	předjet	k5eAaPmF	předjet
pomalého	pomalý	k2eAgMnSc4d1	pomalý
Trulliho	Trulli	k1gMnSc4	Trulli
dokončil	dokončit	k5eAaPmAgMnS	dokončit
třetí	třetí	k4xOgMnSc1	třetí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
průběžného	průběžný	k2eAgNnSc2d1	průběžné
vedení	vedení	k1gNnSc2	vedení
šampionátu	šampionát	k1gInSc2	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kolizi	kolize	k1gFnSc6	kolize
s	s	k7c7	s
Montoyou	Montoya	k1gFnSc7	Montoya
na	na	k7c6	na
Nürburgringu	Nürburgring	k1gInSc6	Nürburgring
inkasoval	inkasovat	k5eAaBmAgInS	inkasovat
čtyři	čtyři	k4xCgInPc4	čtyři
body	bod	k1gInPc4	bod
za	za	k7c4	za
pátou	pátý	k4xOgFnSc4	pátý
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
díky	díky	k7c3	díky
třetímu	třetí	k4xOgMnSc3	třetí
místu	místo	k1gNnSc3	místo
postodvacáté	postodvacátý	k2eAgFnSc2d1	postodvacátý
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
stanul	stanout	k5eAaPmAgMnS	stanout
na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
obsadil	obsadit	k5eAaPmAgMnS	obsadit
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
příčku	příčka	k1gFnSc4	příčka
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
po	po	k7c6	po
defektu	defekt	k1gInSc6	defekt
pneumatiky	pneumatika	k1gFnSc2	pneumatika
sedmou	sedma	k1gFnSc7	sedma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
po	po	k7c6	po
předjetí	předjetí	k1gNnSc6	předjetí
o	o	k7c4	o
kolo	kolo	k1gNnSc4	kolo
vítězem	vítěz	k1gMnSc7	vítěz
Alonsem	Alons	k1gMnSc7	Alons
dojel	dojet	k5eAaPmAgMnS	dojet
osmý	osmý	k4xOgMnSc1	osmý
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
náskok	náskok	k1gInSc1	náskok
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
šampionátu	šampionát	k1gInSc2	šampionát
se	se	k3xPyFc4	se
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
na	na	k7c4	na
pouhý	pouhý	k2eAgInSc4d1	pouhý
bod	bod	k1gInSc4	bod
před	před	k7c7	před
Montoyou	Montoya	k1gFnSc7	Montoya
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
body	bod	k1gInPc4	bod
před	před	k7c7	před
Kimim	Kimi	k1gNnSc7	Kimi
Räikkönenem	Räikkönen	k1gMnSc7	Räikkönen
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Itálie	Itálie	k1gFnSc2	Itálie
potěšil	potěšit	k5eAaPmAgMnS	potěšit
domácí	domácí	k2eAgMnPc4d1	domácí
fanoušky	fanoušek	k1gMnPc4	fanoušek
hattrickem	hattrick	k1gInSc7	hattrick
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
prvenství	prvenství	k1gNnSc4	prvenství
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
při	při	k7c6	při
Velké	velký	k2eAgFnSc6d1	velká
ceně	cena	k1gFnSc6	cena
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
Schumacherovi	Schumacher	k1gMnSc3	Schumacher
stačilo	stačit	k5eAaBmAgNnS	stačit
pro	pro	k7c4	pro
obhajobu	obhajoba	k1gFnSc4	obhajoba
titulu	titul	k1gInSc2	titul
získat	získat	k5eAaPmF	získat
bod	bod	k1gInSc4	bod
za	za	k7c4	za
osmé	osmý	k4xOgNnSc4	osmý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čtrnáctém	čtrnáctý	k4xOgInSc6	čtrnáctý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
si	se	k3xPyFc3	se
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
se	se	k3xPyFc4	se
Takumou	Takuma	k1gFnSc7	Takuma
Satóem	Satóem	k1gInSc1	Satóem
ulomil	ulomit	k5eAaPmAgInS	ulomit
přední	přední	k2eAgInSc4d1	přední
spoiler	spoiler	k1gInSc4	spoiler
<g/>
,	,	kIx,	,
v	v	k7c6	v
šikaně	šikana	k1gFnSc6	šikana
se	se	k3xPyFc4	se
srazil	srazit	k5eAaPmAgMnS	srazit
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
<g/>
,	,	kIx,	,
dokončil	dokončit	k5eAaPmAgMnS	dokončit
na	na	k7c6	na
osmém	osmý	k4xOgInSc6	osmý
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
potřebný	potřebný	k2eAgInSc4d1	potřebný
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
šampionátu	šampionát	k1gInSc6	šampionát
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
již	již	k6eAd1	již
pošesté	pošesté	k4xO	pošesté
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
překonal	překonat	k5eAaPmAgMnS	překonat
Fangiův	Fangiův	k2eAgInSc4d1	Fangiův
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
šestkrát	šestkrát	k6eAd1	šestkrát
<g/>
,	,	kIx,	,
pětkrát	pětkrát	k6eAd1	pětkrát
zajel	zajet	k5eAaPmAgMnS	zajet
pole	pole	k1gNnSc4	pole
position	position	k1gInSc1	position
i	i	k8xC	i
nejrychlejší	rychlý	k2eAgNnSc1d3	nejrychlejší
kolo	kolo	k1gNnSc1	kolo
<g/>
,	,	kIx,	,
v	v	k7c6	v
šestnácti	šestnáct	k4xCc6	šestnáct
závodech	závod	k1gInPc6	závod
stál	stát	k5eAaImAgMnS	stát
osmkrát	osmkrát	k6eAd1	osmkrát
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
získal	získat	k5eAaPmAgInS	získat
93	[number]	k4	93
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
2004	[number]	k4	2004
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
pěti	pět	k4xCc6	pět
závodech	závod	k1gInPc6	závod
sezony	sezona	k1gFnSc2	sezona
2004	[number]	k4	2004
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
<g/>
,	,	kIx,	,
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
získal	získat	k5eAaPmAgMnS	získat
pole	pole	k1gNnSc4	pole
position	position	k1gInSc1	position
a	a	k8xC	a
nejrychlejší	rychlý	k2eAgNnSc1d3	nejrychlejší
kolo	kolo	k1gNnSc1	kolo
závodu	závod	k1gInSc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šestém	šestý	k4xOgInSc6	šestý
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
po	po	k7c6	po
kolizi	kolize	k1gFnSc6	kolize
s	s	k7c7	s
Montoyou	Montoya	k1gFnSc7	Montoya
v	v	k7c6	v
46	[number]	k4	46
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
havaroval	havarovat	k5eAaPmAgMnS	havarovat
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
v	v	k7c6	v
tunelu	tunel	k1gInSc6	tunel
při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
pneumatik	pneumatika	k1gFnPc2	pneumatika
během	během	k7c2	během
jízdy	jízda	k1gFnSc2	jízda
za	za	k7c7	za
safety	safet	k1gMnPc7	safet
carem	car	k1gMnSc7	car
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
sedmi	sedm	k4xCc6	sedm
závodech	závod	k1gInPc6	závod
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
tak	tak	k6eAd1	tak
prvenství	prvenství	k1gNnSc4	prvenství
v	v	k7c6	v
dvanácti	dvanáct	k4xCc2	dvanáct
ze	z	k7c2	z
třinácti	třináct	k4xCc2	třináct
odjetých	odjetý	k2eAgFnPc2d1	odjetá
grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc1	prix
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
posté	posté	k4xO	posté
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
startoval	startovat	k5eAaBmAgInS	startovat
z	z	k7c2	z
první	první	k4xOgFnSc2	první
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Jezdecká	jezdecký	k2eAgFnSc1d1	jezdecká
sestava	sestava	k1gFnSc1	sestava
Schumacher-Barrichello	Schumacher-Barrichello	k1gNnSc1	Schumacher-Barrichello
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
zajistila	zajistit	k5eAaPmAgFnS	zajistit
dvacátý	dvacátý	k4xOgInSc4	dvacátý
double	double	k1gInSc4	double
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výsledek	výsledek	k1gInSc1	výsledek
oba	dva	k4xCgMnPc1	dva
piloti	pilot	k1gMnPc1	pilot
zopakovali	zopakovat	k5eAaPmAgMnP	zopakovat
i	i	k9	i
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tým	tým	k1gInSc1	tým
z	z	k7c2	z
Maranella	Maranello	k1gNnSc2	Maranello
slavil	slavit	k5eAaImAgInS	slavit
čtrnáctý	čtrnáctý	k4xOgInSc4	čtrnáctý
zisk	zisk	k1gInSc4	zisk
Poháru	pohár	k1gInSc2	pohár
konstruktérů	konstruktér	k1gMnPc2	konstruktér
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Belgie	Belgie	k1gFnSc2	Belgie
si	se	k3xPyFc3	se
druhým	druhý	k4xOgNnSc7	druhý
místem	místo	k1gNnSc7	místo
zajistil	zajistit	k5eAaPmAgMnS	zajistit
sedmý	sedmý	k4xOgInSc4	sedmý
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
skončil	skončit	k5eAaPmAgInS	skončit
také	také	k9	také
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Premiérovou	premiérový	k2eAgFnSc4d1	premiérová
Velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
Číny	Čína	k1gFnSc2	Čína
po	po	k7c6	po
jezdeckých	jezdecký	k2eAgFnPc6d1	jezdecká
chybách	chyba	k1gFnPc6	chyba
dokončil	dokončit	k5eAaPmAgInS	dokončit
dvanáctý	dvanáctý	k4xOgMnSc1	dvanáctý
<g/>
.	.	kIx.	.
</s>
<s>
Triumfem	triumf	k1gInSc7	triumf
start-cíl	startílum	k1gNnPc2	start-cílum
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Japonska	Japonsko	k1gNnSc2	Japonsko
získal	získat	k5eAaPmAgMnS	získat
rekordní	rekordní	k2eAgMnSc1d1	rekordní
třinácté	třináctý	k4xOgInPc1	třináctý
vítězství	vítězství	k1gNnPc2	vítězství
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
sezoně	sezona	k1gFnSc6	sezona
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
překonal	překonat	k5eAaPmAgMnS	překonat
dvanáct	dvanáct	k4xCc4	dvanáct
prvenství	prvenství	k1gNnPc2	prvenství
Nigela	Nigela	k1gFnSc1	Nigela
Mansella	Mansella	k1gFnSc1	Mansella
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Ralfem	Ralf	k1gMnSc7	Ralf
obsadili	obsadit	k5eAaPmAgMnP	obsadit
popáté	popáté	k4xO	popáté
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
první	první	k4xOgNnPc1	první
dvě	dva	k4xCgNnPc1	dva
místa	místo	k1gNnPc1	místo
v	v	k7c6	v
cíli	cíl	k1gInSc6	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
závod	závod	k1gInSc1	závod
sezony	sezona	k1gFnSc2	sezona
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
dojel	dojet	k5eAaPmAgMnS	dojet
po	po	k7c6	po
smyku	smyk	k1gInSc6	smyk
sedmý	sedmý	k4xOgMnSc1	sedmý
<g/>
.	.	kIx.	.
</s>
<s>
Sedmý	sedmý	k4xOgInSc4	sedmý
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
díky	díky	k7c3	díky
třinácti	třináct	k4xCc3	třináct
výhrám	výhra	k1gFnPc3	výhra
<g/>
,	,	kIx,	,
148	[number]	k4	148
bodům	bod	k1gInPc3	bod
<g/>
,	,	kIx,	,
osmi	osm	k4xCc2	osm
pole	pole	k1gNnSc2	pole
position	position	k1gInSc1	position
a	a	k8xC	a
deseti	deset	k4xCc7	deset
nejrychlejším	rychlý	k2eAgNnPc3d3	nejrychlejší
kolům	kolo	k1gNnPc3	kolo
<g/>
,	,	kIx,	,
patnáctkrát	patnáctkrát	k6eAd1	patnáctkrát
z	z	k7c2	z
osmnácti	osmnáct	k4xCc2	osmnáct
velkých	velký	k2eAgFnPc2d1	velká
cen	cena	k1gFnPc2	cena
obsadil	obsadit	k5eAaPmAgMnS	obsadit
stupně	stupeň	k1gInPc4	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
2005	[number]	k4	2005
====	====	k?	====
</s>
</p>
<p>
<s>
FIA	FIA	kA	FIA
pro	pro	k7c4	pro
sezonu	sezona	k1gFnSc4	sezona
2005	[number]	k4	2005
znovu	znovu	k6eAd1	znovu
upravila	upravit	k5eAaPmAgNnP	upravit
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zamezila	zamezit	k5eAaPmAgFnS	zamezit
dominanci	dominance	k1gFnSc4	dominance
italské	italský	k2eAgFnSc2d1	italská
stáje	stáj	k1gFnSc2	stáj
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c4	na
úvodní	úvodní	k2eAgInPc4d1	úvodní
závody	závod	k1gInPc4	závod
použila	použít	k5eAaPmAgFnS	použít
vůz	vůz	k1gInSc4	vůz
z	z	k7c2	z
předchozí	předchozí	k2eAgFnSc2d1	předchozí
sezony	sezona	k1gFnSc2	sezona
<g/>
.	.	kIx.	.
</s>
<s>
Schumacher	Schumachra	k1gFnPc2	Schumachra
úvodní	úvodní	k2eAgMnSc1d1	úvodní
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Austrálie	Austrálie	k1gFnSc2	Austrálie
po	po	k7c6	po
kolizi	kolize	k1gFnSc6	kolize
s	s	k7c7	s
Nickem	Nicek	k1gMnSc7	Nicek
Heidfeldem	Heidfeld	k1gMnSc7	Heidfeld
ve	v	k7c6	v
43	[number]	k4	43
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Malajsii	Malajsie	k1gFnSc6	Malajsie
jako	jako	k8xS	jako
sedmý	sedmý	k4xOgMnSc1	sedmý
zaostal	zaostat	k5eAaPmAgMnS	zaostat
o	o	k7c4	o
osmdesát	osmdesát	k4xCc4	osmdesát
sekund	sekunda	k1gFnPc2	sekunda
za	za	k7c7	za
vítězem	vítěz	k1gMnSc7	vítěz
Alonsem	Alons	k1gMnSc7	Alons
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
Bahrajnu	Bahrajn	k1gInSc2	Bahrajn
Ferrari	Ferrari	k1gMnSc2	Ferrari
narychlo	narychlo	k6eAd1	narychlo
dokončilo	dokončit	k5eAaPmAgNnS	dokončit
nový	nový	k2eAgInSc4d1	nový
monopost	monopost	k1gInSc4	monopost
<g/>
,	,	kIx,	,
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
o	o	k7c6	o
vítězství	vítězství	k1gNnSc6	vítězství
s	s	k7c7	s
Alonsem	Alons	k1gInSc7	Alons
jej	on	k3xPp3gInSc2	on
po	po	k7c6	po
dvanácti	dvanáct	k4xCc6	dvanáct
kolech	kolo	k1gNnPc6	kolo
zastavila	zastavit	k5eAaPmAgFnS	zastavit
porucha	porucha	k1gFnSc1	porucha
hydrauliky	hydraulika	k1gFnSc2	hydraulika
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
San	San	k1gFnSc2	San
Marina	Marina	k1gFnSc1	Marina
po	po	k7c6	po
startu	start	k1gInSc6	start
ze	z	k7c2	z
třináctého	třináctý	k4xOgNnSc2	třináctý
místa	místo	k1gNnSc2	místo
dojel	dojet	k5eAaPmAgMnS	dojet
druhý	druhý	k4xOgMnSc1	druhý
v	v	k7c6	v
závěsu	závěs	k1gInSc6	závěs
za	za	k7c7	za
Alonsem	Alons	k1gInSc7	Alons
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
defektech	defekt	k1gInPc6	defekt
odstoupil	odstoupit	k5eAaPmAgInS	odstoupit
ze	z	k7c2	z
třetí	třetí	k4xOgFnSc2	třetí
příčky	příčka	k1gFnSc2	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
problémech	problém	k1gInPc6	problém
s	s	k7c7	s
pneumatikami	pneumatika	k1gFnPc7	pneumatika
v	v	k7c6	v
Monackém	monacký	k2eAgNnSc6d1	Monacké
knížectví	knížectví	k1gNnSc6	knížectví
skončil	skončit	k5eAaPmAgInS	skončit
sedmý	sedmý	k4xOgMnSc1	sedmý
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
spokojit	spokojit	k5eAaPmF	spokojit
s	s	k7c7	s
pátým	pátý	k4xOgNnSc7	pátý
místem	místo	k1gNnSc7	místo
za	za	k7c7	za
Barrichellem	Barrichell	k1gInSc7	Barrichell
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
stříbro	stříbro	k1gNnSc4	stříbro
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
jeho	on	k3xPp3gInSc4	on
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výsledek	výsledek	k1gInSc4	výsledek
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
sezony	sezona	k1gFnSc2	sezona
<g/>
,	,	kIx,	,
a	a	k8xC	a
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
USA	USA	kA	USA
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
kvůli	kvůli	k7c3	kvůli
pneumatikám	pneumatika	k1gFnPc3	pneumatika
Michellin	Michellin	k1gInSc4	Michellin
nenastoupilo	nastoupit	k5eNaPmAgNnS	nastoupit
sedm	sedm	k4xCc1	sedm
týmů	tým	k1gInPc2	tým
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
odstoupení	odstoupení	k1gNnSc6	odstoupení
obou	dva	k4xCgMnPc2	dva
McLarenů	McLaren	k1gMnPc2	McLaren
i	i	k9	i
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c4	na
stupně	stupeň	k1gInPc4	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
dojel	dojet	k5eAaPmAgMnS	dojet
šestý	šestý	k4xOgMnSc1	šestý
<g/>
,	,	kIx,	,
páté	pátý	k4xOgNnSc4	pátý
místo	místo	k1gNnSc4	místo
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
domácí	domácí	k2eAgFnSc6d1	domácí
VC	VC	kA	VC
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
pole	pola	k1gFnSc6	pola
position	position	k1gInSc1	position
sezony	sezona	k1gFnSc2	sezona
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
byl	být	k5eAaImAgInS	být
klasifikován	klasifikován	k2eAgMnSc1d1	klasifikován
druhý	druhý	k4xOgInSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
premiérové	premiérový	k2eAgFnSc6d1	premiérová
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Turecka	Turecko	k1gNnSc2	Turecko
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
v	v	k7c6	v
33	[number]	k4	33
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
kvůli	kvůli	k7c3	kvůli
poruše	porucha	k1gFnSc3	porucha
řízení	řízení	k1gNnSc2	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
Itálie	Itálie	k1gFnSc2	Itálie
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
propadl	propadnout	k5eAaPmAgInS	propadnout
až	až	k9	až
na	na	k7c4	na
konečné	konečný	k2eAgNnSc4d1	konečné
desáté	desátý	k4xOgNnSc4	desátý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
jej	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
čtrnáctém	čtrnáctý	k4xOgInSc6	čtrnáctý
kole	kolo	k1gNnSc6	kolo
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
Sató	Sató	k1gMnSc1	Sató
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
pozicí	pozice	k1gFnSc7	pozice
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
v	v	k7c6	v
průběžném	průběžný	k2eAgNnSc6d1	průběžné
pořadí	pořadí	k1gNnSc6	pořadí
šampionátu	šampionát	k1gInSc2	šampionát
klesl	klesnout	k5eAaPmAgInS	klesnout
na	na	k7c4	na
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
za	za	k7c4	za
Montoyou	Montoyá	k1gFnSc4	Montoyá
<g/>
,	,	kIx,	,
před	před	k7c4	před
kterého	který	k3yRgMnSc4	který
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
sedmým	sedmý	k4xOgNnSc7	sedmý
místem	místo	k1gNnSc7	místo
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
Velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
Číny	Čína	k1gFnSc2	Čína
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
v	v	k7c6	v
23	[number]	k4	23
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
jezdců	jezdec	k1gMnPc2	jezdec
obsadil	obsadit	k5eAaPmAgMnS	obsadit
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
výhrou	výhra	k1gFnSc7	výhra
<g/>
,	,	kIx,	,
62	[number]	k4	62
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
jednou	jednou	k6eAd1	jednou
pole	pole	k1gFnSc1	pole
position	position	k1gInSc1	position
a	a	k8xC	a
třemi	tři	k4xCgNnPc7	tři
nejrychlejšími	rychlý	k2eAgNnPc7d3	nejrychlejší
koly	kolo	k1gNnPc7	kolo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pěti	pět	k4xCc6	pět
závodech	závod	k1gInPc6	závod
z	z	k7c2	z
devatenácti	devatenáct	k4xCc2	devatenáct
obsadil	obsadit	k5eAaPmAgMnS	obsadit
pódiové	pódiový	k2eAgNnSc4d1	pódiové
umístění	umístění	k1gNnSc4	umístění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
2006	[number]	k4	2006
====	====	k?	====
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnSc4d1	poslední
sezonu	sezona	k1gFnSc4	sezona
u	u	k7c2	u
Ferrari	Ferrari	k1gMnSc2	Ferrari
začal	začít	k5eAaPmAgMnS	začít
pole	pole	k1gNnSc4	pole
position	position	k1gInSc1	position
v	v	k7c6	v
Bahrajnu	Bahrajno	k1gNnSc6	Bahrajno
<g/>
,	,	kIx,	,
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
dojel	dojet	k5eAaPmAgMnS	dojet
druhý	druhý	k4xOgMnSc1	druhý
těsně	těsně	k6eAd1	těsně
za	za	k7c7	za
obhájcem	obhájce	k1gMnSc7	obhájce
titulu	titul	k1gInSc2	titul
Alonsem	Alons	k1gInSc7	Alons
<g/>
.	.	kIx.	.
</s>
<s>
GP	GP	kA	GP
Malajsie	Malajsie	k1gFnSc1	Malajsie
dokončil	dokončit	k5eAaPmAgInS	dokončit
na	na	k7c6	na
šestém	šestý	k4xOgNnSc6	šestý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
33	[number]	k4	33
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
Velké	velký	k2eAgFnSc2d1	velká
ceny	cena	k1gFnSc2	cena
Austrálie	Austrálie	k1gFnSc2	Austrálie
havaroval	havarovat	k5eAaPmAgMnS	havarovat
kvůli	kvůli	k7c3	kvůli
nízké	nízký	k2eAgFnSc3d1	nízká
přilnavosti	přilnavost	k1gFnSc3	přilnavost
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
výhru	výhra	k1gFnSc4	výhra
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
v	v	k7c6	v
San	San	k1gFnSc6	San
Marinu	Marina	k1gFnSc4	Marina
<g/>
,	,	kIx,	,
prvenstvím	prvenství	k1gNnPc3	prvenství
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
i	i	k9	i
při	při	k7c6	při
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
místo	místo	k1gNnSc4	místo
si	se	k3xPyFc3	se
pohoršil	pohoršit	k5eAaPmAgMnS	pohoršit
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončil	skončit	k5eAaPmAgInS	skončit
druhý	druhý	k4xOgInSc1	druhý
za	za	k7c7	za
španělským	španělský	k2eAgMnSc7d1	španělský
pilotem	pilot	k1gMnSc7	pilot
Renaultu	renault	k1gInSc2	renault
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
Velké	velký	k2eAgFnSc2d1	velká
ceny	cena	k1gFnSc2	cena
Monaka	Monako	k1gNnSc2	Monako
fingoval	fingovat	k5eAaBmAgMnS	fingovat
poruchu	porucha	k1gFnSc4	porucha
vozu	vůz	k1gInSc2	vůz
v	v	k7c6	v
zatáčce	zatáčka	k1gFnSc6	zatáčka
Rascasse	Rascasse	k1gFnSc2	Rascasse
<g/>
,	,	kIx,	,
odstaveným	odstavený	k2eAgInSc7d1	odstavený
monopostem	monopost	k1gInSc7	monopost
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
zbrzdit	zbrzdit	k5eAaPmF	zbrzdit
Alonsovo	Alonsův	k2eAgNnSc4d1	Alonsovo
kvalifikační	kvalifikační	k2eAgNnSc4d1	kvalifikační
kolo	kolo	k1gNnSc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
počínání	počínání	k1gNnSc4	počínání
byl	být	k5eAaImAgMnS	být
odsunut	odsunout	k5eAaPmNgMnS	odsunout
na	na	k7c4	na
konec	konec	k1gInSc4	konec
startovního	startovní	k2eAgInSc2d1	startovní
roštu	rošt	k1gInSc2	rošt
<g/>
,	,	kIx,	,
z	z	k7c2	z
dvaadvacáté	dvaadvacátý	k4xOgFnSc2	dvaadvacátý
startovní	startovní	k2eAgFnSc2d1	startovní
pozice	pozice	k1gFnSc2	pozice
získal	získat	k5eAaPmAgMnS	získat
čtyři	čtyři	k4xCgInPc4	čtyři
body	bod	k1gInPc4	bod
za	za	k7c4	za
páté	pátý	k4xOgNnSc4	pátý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
dovezl	dovézt	k5eAaPmAgInS	dovézt
své	svůj	k3xOyFgNnSc4	svůj
Ferrari	ferrari	k1gNnSc4	ferrari
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
závody	závod	k1gInPc1	závod
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
Francii	Francie	k1gFnSc6	Francie
po	po	k7c6	po
zisku	zisk	k1gInSc6	zisk
pole	pole	k1gNnSc2	pole
position	position	k1gInSc1	position
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
způsobem	způsob	k1gInSc7	způsob
start-cíl	startíla	k1gFnPc2	start-cíla
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
prvenství	prvenství	k1gNnSc4	prvenství
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
při	při	k7c6	při
domácí	domácí	k1gFnSc6	domácí
grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc1	prix
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
ceny	cena	k1gFnSc2	cena
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
startoval	startovat	k5eAaBmAgInS	startovat
po	po	k7c6	po
penalizaci	penalizace	k1gFnSc6	penalizace
z	z	k7c2	z
jedenácté	jedenáctý	k4xOgFnSc2	jedenáctý
příčky	příčka	k1gFnSc2	příčka
<g/>
,	,	kIx,	,
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
po	po	k7c6	po
lehké	lehký	k2eAgFnSc6d1	lehká
kolizi	kolize	k1gFnSc6	kolize
s	s	k7c7	s
Fisichellou	Fisichella	k1gFnSc7	Fisichella
obdržel	obdržet	k5eAaPmAgInS	obdržet
bod	bod	k1gInSc1	bod
za	za	k7c4	za
osmé	osmý	k4xOgNnSc4	osmý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
obsadil	obsadit	k5eAaPmAgMnS	obsadit
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
když	když	k8xS	když
při	při	k7c6	při
výjezdu	výjezd	k1gInSc6	výjezd
safety	safeta	k1gFnSc2	safeta
caru	car	k1gMnSc6	car
čekal	čekat	k5eAaImAgMnS	čekat
při	při	k7c6	při
zastávce	zastávka	k1gFnSc6	zastávka
v	v	k7c6	v
boxech	box	k1gInPc6	box
za	za	k7c7	za
týmovou	týmový	k2eAgFnSc7d1	týmová
dvojkou	dvojka	k1gFnSc7	dvojka
Felipem	Felip	k1gInSc7	Felip
Massou	Massa	k1gFnSc7	Massa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
italskými	italský	k2eAgMnPc7d1	italský
tifosi	tifos	k1gMnPc7	tifos
po	po	k7c6	po
startu	start	k1gInSc6	start
z	z	k7c2	z
první	první	k4xOgFnSc2	první
řady	řada	k1gFnSc2	řada
triumfoval	triumfovat	k5eAaBmAgInS	triumfovat
a	a	k8xC	a
po	po	k7c6	po
závodě	závod	k1gInSc6	závod
oznámil	oznámit	k5eAaPmAgMnS	oznámit
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
ze	z	k7c2	z
šestého	šestý	k4xOgNnSc2	šestý
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
startu	start	k1gInSc6	start
porazil	porazit	k5eAaPmAgMnS	porazit
Alonsa	Alonsa	k1gFnSc1	Alonsa
a	a	k8xC	a
ujal	ujmout	k5eAaPmAgMnS	ujmout
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc4	vedení
v	v	k7c6	v
průběžném	průběžný	k2eAgNnSc6d1	průběžné
pořadí	pořadí	k1gNnSc6	pořadí
šampionátu	šampionát	k1gInSc2	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
po	po	k7c6	po
startu	start	k1gInSc6	start
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
místa	místo	k1gNnSc2	místo
vedl	vést	k5eAaImAgInS	vést
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
37	[number]	k4	37
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
kvůli	kvůli	k7c3	kvůli
závadě	závada	k1gFnSc3	závada
na	na	k7c6	na
motoru	motor	k1gInSc6	motor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
naposledy	naposledy	k6eAd1	naposledy
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Francie	Francie	k1gFnSc2	Francie
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Závod	závod	k1gInSc1	závod
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Alonso	Alonsa	k1gFnSc5	Alonsa
a	a	k8xC	a
Schumacher	Schumachra	k1gFnPc2	Schumachra
měl	mít	k5eAaImAgMnS	mít
pouze	pouze	k6eAd1	pouze
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
Španěl	Španěl	k1gMnSc1	Španěl
by	by	kYmCp3nS	by
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
závodě	závod	k1gInSc6	závod
nesměl	smět	k5eNaImAgInS	smět
bodovat	bodovat	k5eAaImF	bodovat
a	a	k8xC	a
Schumacher	Schumachra	k1gFnPc2	Schumachra
by	by	kYmCp3nS	by
musel	muset	k5eAaImAgInS	muset
vyhrát	vyhrát	k5eAaPmF	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
ceny	cena	k1gFnSc2	cena
Brazílie	Brazílie	k1gFnSc2	Brazílie
se	se	k3xPyFc4	se
kvalifikoval	kvalifikovat	k5eAaBmAgMnS	kvalifikovat
desátý	desátý	k4xOgMnSc1	desátý
<g/>
,	,	kIx,	,
po	po	k7c6	po
defektu	defekt	k1gInSc6	defekt
v	v	k7c6	v
devátém	devátý	k4xOgNnSc6	devátý
kole	kolo	k1gNnSc6	kolo
jezdil	jezdit	k5eAaImAgInS	jezdit
poslední	poslední	k2eAgInSc1d1	poslední
<g/>
,	,	kIx,	,
po	po	k7c6	po
stíhací	stíhací	k2eAgFnSc6d1	stíhací
jízdě	jízda	k1gFnSc6	jízda
dojel	dojet	k5eAaPmAgMnS	dojet
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
<g/>
.	.	kIx.	.
</s>
<s>
Šestnáctou	šestnáctý	k4xOgFnSc4	šestnáctý
sezonu	sezona	k1gFnSc4	sezona
zakončil	zakončit	k5eAaPmAgMnS	zakončit
jako	jako	k8xS	jako
druhý	druhý	k4xOgMnSc1	druhý
za	za	k7c2	za
obhájcem	obhájce	k1gMnSc7	obhájce
titulu	titul	k1gInSc2	titul
Fernandem	Fernand	k1gInSc7	Fernand
Alonsem	Alons	k1gMnSc7	Alons
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
sedmi	sedm	k4xCc2	sedm
výher	výhra	k1gFnPc2	výhra
<g/>
,	,	kIx,	,
čtyř	čtyři	k4xCgMnPc2	čtyři
pole	pole	k1gNnSc2	pole
position	position	k1gInSc1	position
<g/>
,	,	kIx,	,
sedmi	sedm	k4xCc2	sedm
nejrychlejších	rychlý	k2eAgNnPc2d3	nejrychlejší
kol	kolo	k1gNnPc2	kolo
a	a	k8xC	a
121	[number]	k4	121
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtrnácti	čtrnáct	k4xCc2	čtrnáct
z	z	k7c2	z
osmnácti	osmnáct	k4xCc2	osmnáct
závodů	závod	k1gInPc2	závod
vystoupal	vystoupat	k5eAaPmAgInS	vystoupat
na	na	k7c4	na
stupně	stupeň	k1gInPc4	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Poradce	poradce	k1gMnSc1	poradce
Ferrari	Ferrari	k1gMnSc1	Ferrari
===	===	k?	===
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
jej	on	k3xPp3gInSc4	on
Scuderia	Scuderium	k1gNnSc2	Scuderium
Ferrari	Ferrari	k1gMnPc4	Ferrari
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
poradcem	poradce	k1gMnSc7	poradce
a	a	k8xC	a
"	"	kIx"	"
<g/>
super	super	k2eAgMnSc7d1	super
asistentem	asistent	k1gMnSc7	asistent
<g/>
"	"	kIx"	"
nového	nový	k2eAgMnSc4d1	nový
generální	generální	k2eAgMnSc1d1	generální
ředitele	ředitel	k1gMnSc4	ředitel
Scuderie	Scuderie	k1gFnSc2	Scuderie
Jeana	Jean	k1gMnSc4	Jean
Todta	Todt	k1gMnSc4	Todt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
několika	několik	k4yIc2	několik
velkých	velký	k2eAgFnPc2d1	velká
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Fiorano	Fiorana	k1gFnSc5	Fiorana
pět	pět	k4xCc4	pět
kol	kolo	k1gNnPc2	kolo
s	s	k7c7	s
novým	nový	k2eAgNnSc7d1	nové
Ferrari	ferrari	k1gNnSc7	ferrari
F	F	kA	F
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
časy	čas	k1gInPc1	čas
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
nezaznamenávaly	zaznamenávat	k5eNaImAgFnP	zaznamenávat
<g/>
,	,	kIx,	,
jízda	jízda	k1gFnSc1	jízda
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
nejvyšší	vysoký	k2eAgMnPc4d3	nejvyšší
představitele	představitel	k1gMnPc4	představitel
Fiatu	fiat	k1gInSc2	fiat
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
oficiální	oficiální	k2eAgNnSc4d1	oficiální
testování	testování	k1gNnSc4	testování
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pomohl	pomoct	k5eAaPmAgMnS	pomoct
italskému	italský	k2eAgInSc3d1	italský
týmu	tým	k1gInSc3	tým
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
vozu	vůz	k1gInSc2	vůz
a	a	k8xC	a
pneumatik	pneumatika	k1gFnPc2	pneumatika
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
změnou	změna	k1gFnSc7	změna
pravidel	pravidlo	k1gNnPc2	pravidlo
byla	být	k5eAaImAgFnS	být
zakázána	zakázat	k5eAaPmNgFnS	zakázat
elektronická	elektronický	k2eAgFnSc1d1	elektronická
kontrola	kontrola	k1gFnSc1	kontrola
prokluzu	prokluz	k1gInSc2	prokluz
a	a	k8xC	a
Schumacher	Schumachra	k1gFnPc2	Schumachra
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
závodil	závodit	k5eAaImAgMnS	závodit
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nepoužívala	používat	k5eNaImAgFnS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
odjel	odjet	k5eAaPmAgInS	odjet
64	[number]	k4	64
kol	kolo	k1gNnPc2	kolo
a	a	k8xC	a
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
nejrychlejší	rychlý	k2eAgInSc4d3	nejrychlejší
čas	čas	k1gInSc4	čas
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
21,922	[number]	k4	21,922
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
několik	několik	k4yIc4	několik
zkušebních	zkušební	k2eAgFnPc2d1	zkušební
jízd	jízda	k1gFnPc2	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
motocyklovým	motocyklový	k2eAgMnSc7d1	motocyklový
závodům	závod	k1gInPc3	závod
<g/>
.29	.29	k4	.29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
tým	tým	k1gInSc1	tým
Ferrari	Ferrari	k1gMnSc1	Ferrari
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
dočasně	dočasně	k6eAd1	dočasně
vrátí	vrátit	k5eAaPmIp3nS	vrátit
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zastoupil	zastoupit	k5eAaPmAgMnS	zastoupit
zraněného	zraněný	k2eAgMnSc4d1	zraněný
Brazilce	Brazilec	k1gMnSc4	Brazilec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgInS	mít
nehodu	nehoda	k1gFnSc4	nehoda
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
části	část	k1gFnSc6	část
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
GP	GP	kA	GP
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
testování	testování	k1gNnSc4	testování
s	s	k7c7	s
Ferrari	Ferrari	k1gMnSc7	Ferrari
F2007	F2007	k1gFnSc2	F2007
a	a	k8xC	a
pneumatikami	pneumatika	k1gFnPc7	pneumatika
z	z	k7c2	z
GP	GP	kA	GP
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
testovat	testovat	k5eAaImF	testovat
s	s	k7c7	s
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
F60	F60	k1gMnSc7	F60
nemohl	moct	k5eNaImAgMnS	moct
kvůli	kvůli	k7c3	kvůli
zákazu	zákaz	k1gInSc3	zákaz
testování	testování	k1gNnSc2	testování
během	během	k7c2	během
sezony	sezona	k1gFnSc2	sezona
<g/>
,	,	kIx,	,
výjimku	výjimka	k1gFnSc4	výjimka
pro	pro	k7c4	pro
sedminásobného	sedminásobný	k2eAgMnSc4d1	sedminásobný
šampiona	šampion	k1gMnSc4	šampion
zamítly	zamítnout	k5eAaPmAgInP	zamítnout
týmy	tým	k1gInPc1	tým
Williams	Williamsa	k1gFnPc2	Williamsa
<g/>
,	,	kIx,	,
Red	Red	k1gFnPc2	Red
Bull	bulla	k1gFnPc2	bulla
a	a	k8xC	a
Toro	Toro	k?	Toro
Rosso	Rossa	k1gFnSc5	Rossa
<g/>
.	.	kIx.	.
</s>
<s>
Schumacher	Schumachra	k1gFnPc2	Schumachra
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
informoval	informovat	k5eAaBmAgMnS	informovat
prezidenta	prezident	k1gMnSc2	prezident
Ferrari	Ferrari	k1gMnSc1	Ferrari
Luku	luk	k1gInSc2	luk
di	di	k?	di
Montezemola	Montezemola	k1gFnSc1	Montezemola
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvůli	kvůli	k7c3	kvůli
přetrvávajícím	přetrvávající	k2eAgInPc3d1	přetrvávající
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
krkem	krk	k1gInSc7	krk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
poranil	poranit	k5eAaPmAgInS	poranit
při	při	k7c6	při
únorových	únorový	k2eAgInPc6d1	únorový
motocyklových	motocyklový	k2eAgInPc6d1	motocyklový
závodech	závod	k1gInPc6	závod
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
do	do	k7c2	do
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
nevrátí	vrátit	k5eNaPmIp3nP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
sezonu	sezona	k1gFnSc4	sezona
2010	[number]	k4	2010
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
Ferrari	Ferrari	k1gMnSc1	Ferrari
marně	marně	k6eAd1	marně
vyjednávali	vyjednávat	k5eAaImAgMnP	vyjednávat
třetí	třetí	k4xOgInSc4	třetí
vůz	vůz	k1gInSc4	vůz
pro	pro	k7c4	pro
Schumachera	Schumacher	k1gMnSc4	Schumacher
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
před	před	k7c4	před
oznámení	oznámení	k1gNnSc4	oznámení
přestupu	přestup	k1gInSc2	přestup
k	k	k7c3	k
Mercedesu	mercedes	k1gInSc3	mercedes
měl	mít	k5eAaImAgMnS	mít
již	již	k9	již
ústně	ústně	k6eAd1	ústně
dojednanou	dojednaný	k2eAgFnSc4d1	dojednaná
tříletou	tříletý	k2eAgFnSc4d1	tříletá
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Ferrari	Ferrari	k1gMnSc7	Ferrari
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
poradce	poradce	k1gMnSc2	poradce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Mercedes	mercedes	k1gInSc1	mercedes
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
2010	[number]	k4	2010
====	====	k?	====
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
tým	tým	k1gInSc1	tým
Mercedes	mercedes	k1gInSc1	mercedes
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
oficiálně	oficiálně	k6eAd1	oficiálně
oznámil	oznámit	k5eAaPmAgMnS	oznámit
návrat	návrat	k1gInSc4	návrat
sedminásobného	sedminásobný	k2eAgMnSc2d1	sedminásobný
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
do	do	k7c2	do
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Němec	Němec	k1gMnSc1	Němec
si	se	k3xPyFc3	se
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Mercedesem	mercedes	k1gInSc7	mercedes
vyjednal	vyjednat	k5eAaPmAgMnS	vyjednat
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc7	svůj
novou	nový	k2eAgFnSc7d1	nová
manažerkou	manažerka	k1gFnSc7	manažerka
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
svou	svůj	k3xOyFgFnSc7	svůj
tiskovou	tiskový	k2eAgFnSc7d1	tisková
mluvčí	mluvčí	k1gFnSc7	mluvčí
Sabine	Sabin	k1gInSc5	Sabin
Kehm	Kehm	k1gMnSc1	Kehm
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
manažer	manažer	k1gMnSc1	manažer
Willi	Will	k1gMnSc3	Will
Weber	weber	k1gInSc4	weber
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
obchodními	obchodní	k2eAgFnPc7d1	obchodní
záležitostmi	záležitost	k1gFnPc7	záležitost
německého	německý	k2eAgMnSc2d1	německý
pilota	pilot	k1gMnSc2	pilot
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Schumacherově	Schumacherův	k2eAgInSc6d1	Schumacherův
možném	možný	k2eAgInSc6d1	možný
návratu	návrat	k1gInSc6	návrat
se	se	k3xPyFc4	se
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
od	od	k7c2	od
převzetí	převzetí	k1gNnSc2	převzetí
Brawn	Brawna	k1gFnPc2	Brawna
GP	GP	kA	GP
Mercedesem	mercedes	k1gInSc7	mercedes
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
rozhovoru	rozhovor	k1gInSc6	rozhovor
prozradil	prozradit	k5eAaPmAgMnS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
uměl	umět	k5eAaImAgMnS	umět
představit	představit	k5eAaPmF	představit
konec	konec	k1gInSc4	konec
kariéry	kariéra	k1gFnSc2	kariéra
u	u	k7c2	u
této	tento	k3xDgFnSc2	tento
německé	německý	k2eAgFnSc2d1	německá
automobilky	automobilka	k1gFnSc2	automobilka
<g/>
.	.	kIx.	.
</s>
<s>
Radost	radost	k1gFnSc1	radost
z	z	k7c2	z
události	událost	k1gFnSc2	událost
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
například	například	k6eAd1	například
šéf	šéf	k1gMnSc1	šéf
F1	F1	k1gFnSc2	F1
Bernie	Bernie	k1gFnSc2	Bernie
Ecclestone	Eccleston	k1gInSc5	Eccleston
či	či	k8xC	či
Nigel	Nigel	k1gMnSc1	Nigel
Mansell	Mansell	k1gMnSc1	Mansell
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
Němcův	Němcův	k2eAgMnSc1d1	Němcův
bývalý	bývalý	k2eAgMnSc1d1	bývalý
soupeř	soupeř	k1gMnSc1	soupeř
Damon	Damon	k1gMnSc1	Damon
Hill	Hill	k1gMnSc1	Hill
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
obavu	obava	k1gFnSc4	obava
z	z	k7c2	z
nadržování	nadržování	k1gNnSc2	nadržování
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
FIA	FIA	kA	FIA
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vede	vést	k5eAaImIp3nS	vést
Schumacherův	Schumacherův	k2eAgMnSc1d1	Schumacherův
přítel	přítel	k1gMnSc1	přítel
Jean	Jean	k1gMnSc1	Jean
Todt	Todt	k1gMnSc1	Todt
<g/>
.	.	kIx.	.
</s>
<s>
Kritikou	kritika	k1gFnSc7	kritika
nešetřily	šetřit	k5eNaImAgFnP	šetřit
ani	ani	k8xC	ani
italské	italský	k2eAgInPc1d1	italský
deníky	deník	k1gInPc1	deník
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
rodáka	rodák	k1gMnSc4	rodák
z	z	k7c2	z
Kerpenu	Kerpen	k2eAgFnSc4d1	Kerpen
označily	označit	k5eAaPmAgFnP	označit
za	za	k7c4	za
zrádce	zrádce	k1gMnPc4	zrádce
<g/>
.	.	kIx.	.
</s>
<s>
Spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Schumacherův	Schumacherův	k2eAgInSc1d1	Schumacherův
poraněný	poraněný	k2eAgInSc1d1	poraněný
krk	krk	k1gInSc1	krk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
zabránil	zabránit	k5eAaPmAgInS	zabránit
v	v	k7c6	v
návratu	návrat	k1gInSc6	návrat
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
vydrží	vydržet	k5eAaPmIp3nS	vydržet
zátěž	zátěž	k1gFnSc1	zátěž
ve	v	k7c6	v
voze	vůz	k1gInSc6	vůz
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Německým	německý	k2eAgMnPc3d1	německý
zaměstnancům	zaměstnanec	k1gMnPc3	zaměstnanec
Mercedesu	mercedes	k1gInSc2	mercedes
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
propouštěl	propouštět	k5eAaImAgMnS	propouštět
a	a	k8xC	a
stěhoval	stěhovat	k5eAaImAgMnS	stěhovat
továrny	továrna	k1gFnPc4	továrna
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
vadil	vadit	k5eAaImAgInS	vadit
Němcův	Němcův	k2eAgInSc4d1	Němcův
roční	roční	k2eAgInSc4d1	roční
plat	plat	k1gInSc4	plat
sedm	sedm	k4xCc4	sedm
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
se	se	k3xPyFc4	se
připravoval	připravovat	k5eAaImAgInS	připravovat
testováním	testování	k1gNnSc7	testování
nových	nový	k2eAgInPc2d1	nový
monopostů	monopost	k1gInPc2	monopost
GP	GP	kA	GP
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
testování	testování	k1gNnSc1	testování
ve	v	k7c6	v
f	f	k?	f
<g/>
1	[number]	k4	1
bylo	být	k5eAaImAgNnS	být
až	až	k6eAd1	až
do	do	k7c2	do
února	únor	k1gInSc2	únor
zakázané	zakázaný	k2eAgNnSc1d1	zakázané
<g/>
.	.	kIx.	.
</s>
<s>
Přípravu	příprava	k1gFnSc4	příprava
s	s	k7c7	s
Mercedesem	mercedes	k1gInSc7	mercedes
W01	W01	k1gFnSc2	W01
zahájil	zahájit	k5eAaPmAgInS	zahájit
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
společně	společně	k6eAd1	společně
s	s	k7c7	s
týmovým	týmový	k2eAgMnSc7d1	týmový
kolegou	kolega	k1gMnSc7	kolega
Rosbergem	Rosberg	k1gMnSc7	Rosberg
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
porazil	porazit	k5eAaPmAgMnS	porazit
třetím	třetí	k4xOgInSc7	třetí
časem	čas	k1gInSc7	čas
o	o	k7c4	o
půl	půl	k1xP	půl
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
sezonou	sezona	k1gFnSc7	sezona
požádal	požádat	k5eAaPmAgInS	požádat
tým	tým	k1gInSc1	tým
o	o	k7c4	o
vůz	vůz	k1gInSc4	vůz
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
tři	tři	k4xCgNnPc1	tři
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
preferuje	preferovat	k5eAaImIp3nS	preferovat
lichá	lichý	k2eAgNnPc4d1	liché
čísla	číslo	k1gNnPc4	číslo
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
získal	získat	k5eAaPmAgMnS	získat
všechny	všechen	k3xTgInPc4	všechen
tituly	titul	k1gInPc4	titul
mistra	mistr	k1gMnSc4	mistr
světa	svět	k1gInSc2	svět
a	a	k8xC	a
jezdec	jezdec	k1gMnSc1	jezdec
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
čtyři	čtyři	k4xCgNnPc1	čtyři
nikdy	nikdy	k6eAd1	nikdy
šampionát	šampionát	k1gInSc4	šampionát
nevyhrál	vyhrát	k5eNaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Žádosti	žádost	k1gFnSc2	žádost
bylo	být	k5eAaImAgNnS	být
vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
úvodní	úvodní	k2eAgFnSc2d1	úvodní
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Bahrajnu	Bahrajn	k1gInSc2	Bahrajn
odstartoval	odstartovat	k5eAaPmAgMnS	odstartovat
jako	jako	k8xC	jako
sedmý	sedmý	k4xOgMnSc1	sedmý
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
debutu	debut	k1gInSc6	debut
<g/>
)	)	kIx)	)
a	a	k8xC	a
skončil	skončit	k5eAaPmAgMnS	skončit
za	za	k7c7	za
týmovým	týmový	k2eAgMnSc7d1	týmový
kolegou	kolega	k1gMnSc7	kolega
šestý	šestý	k4xOgMnSc1	šestý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
volném	volný	k2eAgInSc6d1	volný
tréninku	trénink	k1gInSc6	trénink
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
zajel	zajet	k5eAaPmAgMnS	zajet
nejrychleji	rychle	k6eAd3	rychle
druhý	druhý	k4xOgInSc4	druhý
sektor	sektor	k1gInSc4	sektor
<g/>
,	,	kIx,	,
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
získal	získat	k5eAaPmAgMnS	získat
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
za	za	k7c4	za
desáté	desátý	k4xOgNnSc4	desátý
místo	místo	k1gNnSc4	místo
po	po	k7c6	po
neplánovaném	plánovaný	k2eNgInSc6d1	neplánovaný
pitstopu	pitstop	k1gInSc6	pitstop
kvůli	kvůli	k7c3	kvůli
ulomenému	ulomený	k2eAgInSc3d1	ulomený
přednímu	přední	k2eAgInSc3d1	přední
přítlačnému	přítlačný	k2eAgInSc3d1	přítlačný
spoileru	spoiler	k1gInSc3	spoiler
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
desátém	desátý	k4xOgInSc6	desátý
kole	kolo	k1gNnSc6	kolo
Velké	velký	k2eAgFnSc2d1	velká
ceny	cena	k1gFnSc2	cena
Malajsie	Malajsie	k1gFnSc2	Malajsie
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
levým	levý	k2eAgNnSc7d1	levé
zadním	zadní	k2eAgNnSc7d1	zadní
kolem	kolo	k1gNnSc7	kolo
<g/>
,	,	kIx,	,
kolega	kolega	k1gMnSc1	kolega
Rosberg	Rosberg	k1gMnSc1	Rosberg
dojel	dojet	k5eAaPmAgMnS	dojet
třetí	třetí	k4xOgMnSc1	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
závodě	závod	k1gInSc6	závod
v	v	k7c4	v
Kuala	Kualo	k1gNnPc4	Kualo
Lumpur	Lumpura	k1gFnPc2	Lumpura
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
organizace	organizace	k1gFnSc2	organizace
jezdců	jezdec	k1gMnPc2	jezdec
GPDA	GPDA	kA	GPDA
údajně	údajně	k6eAd1	údajně
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
nezvolení	nezvolení	k1gNnSc3	nezvolení
Pedra	Pedro	k1gNnSc2	Pedro
de	de	k?	de
la	la	k1gNnSc2	la
Rosy	Rosa	k1gMnSc2	Rosa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
organizace	organizace	k1gFnSc2	organizace
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
incidentu	incident	k1gInSc3	incident
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Monaka	Monako	k1gNnSc2	Monako
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
desátou	desátý	k4xOgFnSc4	desátý
příčku	příčka	k1gFnSc4	příčka
obsadil	obsadit	k5eAaPmAgMnS	obsadit
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
závěrečných	závěrečný	k2eAgNnPc6d1	závěrečné
kolech	kolo	k1gNnPc6	kolo
předjel	předjet	k5eAaPmAgInS	předjet
nováček	nováček	k1gInSc1	nováček
Vitalij	Vitalij	k1gMnSc1	Vitalij
Petrov	Petrov	k1gInSc1	Petrov
<g/>
.	.	kIx.	.
</s>
<s>
Rosberg	Rosberg	k1gMnSc1	Rosberg
znovu	znovu	k6eAd1	znovu
stanul	stanout	k5eAaPmAgMnS	stanout
na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
tým	tým	k1gInSc4	tým
přivezl	přivézt	k5eAaPmAgInS	přivézt
monopost	monopost	k1gInSc1	monopost
více	hodně	k6eAd2	hodně
přizpůsobený	přizpůsobený	k2eAgInSc1d1	přizpůsobený
Schumacherovi	Schumacher	k1gMnSc3	Schumacher
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vytěžil	vytěžit	k5eAaPmAgMnS	vytěžit
první	první	k4xOgFnSc4	první
porážku	porážka	k1gFnSc4	porážka
Rosberga	Rosberg	k1gMnSc2	Rosberg
a	a	k8xC	a
dvanáct	dvanáct	k4xCc1	dvanáct
bodů	bod	k1gInPc2	bod
za	za	k7c4	za
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Monaka	Monako	k1gNnSc2	Monako
po	po	k7c6	po
předjetí	předjetí	k1gNnSc6	předjetí
Alonsa	Alonsa	k1gFnSc1	Alonsa
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
kole	kolo	k1gNnSc6	kolo
dokončil	dokončit	k5eAaPmAgInS	dokončit
šestý	šestý	k4xOgMnSc1	šestý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
závodě	závod	k1gInSc6	závod
jej	on	k3xPp3gMnSc4	on
traťoví	traťový	k2eAgMnPc1d1	traťový
komisaři	komisar	k1gMnPc1	komisar
penalizovali	penalizovat	k5eAaImAgMnP	penalizovat
dvaceti	dvacet	k4xCc6	dvacet
sekundami	sekunda	k1gFnPc7	sekunda
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
závod	závod	k1gInSc1	závod
nebyl	být	k5eNaImAgInS	být
restartován	restartovat	k5eAaBmNgInS	restartovat
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
týmy	tým	k1gInPc1	tým
dostali	dostat	k5eAaPmAgMnP	dostat
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c4	o
odvolání	odvolání	k1gNnSc4	odvolání
safety	safeta	k1gFnSc2	safeta
caru	car	k1gMnSc3	car
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
po	po	k7c6	po
předjetí	předjetí	k1gNnSc6	předjetí
Buttona	Buttona	k1gFnSc1	Buttona
v	v	k7c6	v
první	první	k4xOgFnSc6	první
zatáčce	zatáčka	k1gFnSc6	zatáčka
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
příčkou	příčka	k1gFnSc7	příčka
porazil	porazit	k5eAaPmAgMnS	porazit
Rosberga	Rosberga	k1gFnSc1	Rosberga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
poprvé	poprvé	k6eAd1	poprvé
nepostoupil	postoupit	k5eNaPmAgMnS	postoupit
do	do	k7c2	do
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
části	část	k1gFnSc2	část
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
a	a	k8xC	a
dojel	dojet	k5eAaPmAgMnS	dojet
jedenáctý	jedenáctý	k4xOgMnSc1	jedenáctý
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Evropy	Evropa	k1gFnSc2	Evropa
po	po	k7c6	po
mylném	mylný	k2eAgInSc6d1	mylný
výkladu	výklad	k1gInSc6	výklad
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zapříčinil	zapříčinit	k5eAaPmAgInS	zapříčinit
zbytečné	zbytečný	k2eAgFnPc4d1	zbytečná
zastávky	zastávka	k1gFnPc4	zastávka
v	v	k7c6	v
boxech	box	k1gInPc6	box
<g/>
,	,	kIx,	,
skončil	skončit	k5eAaPmAgMnS	skončit
patnáctý	patnáctý	k4xOgMnSc1	patnáctý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
bodoval	bodovat	k5eAaImAgMnS	bodovat
devátými	devátý	k4xOgInPc7	devátý
místy	místo	k1gNnPc7	místo
<g/>
.	.	kIx.	.
</s>
<s>
Čtrnácté	čtrnáctý	k4xOgNnSc1	čtrnáctý
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
na	na	k7c4	na
maďarskou	maďarský	k2eAgFnSc4d1	maďarská
grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc1	prix
bylo	být	k5eAaImAgNnS	být
Schumacherovým	Schumacherův	k2eAgNnSc7d1	Schumacherovo
nejhorším	zlý	k2eAgNnSc7d3	nejhorší
kvalifikačním	kvalifikační	k2eAgNnSc7d1	kvalifikační
umístěním	umístění	k1gNnSc7	umístění
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
velké	velký	k2eAgFnSc2d1	velká
ceny	cena	k1gFnSc2	cena
natlačil	natlačit	k5eAaBmAgMnS	natlačit
na	na	k7c6	na
cílové	cílový	k2eAgFnSc6d1	cílová
rovince	rovinka	k1gFnSc6	rovinka
Barrichella	Barrichello	k1gNnSc2	Barrichello
na	na	k7c4	na
boxovou	boxový	k2eAgFnSc4d1	boxová
zídku	zídka	k1gFnSc4	zídka
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
dostal	dostat	k5eAaPmAgInS	dostat
penalizaci	penalizace	k1gFnSc3	penalizace
posunu	posun	k1gInSc2	posun
o	o	k7c4	o
deset	deset	k4xCc4	deset
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
startovním	startovní	k2eAgInSc6d1	startovní
roštu	rošt	k1gInSc6	rošt
Velké	velký	k2eAgFnSc2d1	velká
ceny	cena	k1gFnSc2	cena
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
po	po	k7c6	po
startu	start	k1gInSc6	start
z	z	k7c2	z
21	[number]	k4	21
<g/>
.	.	kIx.	.
místa	místo	k1gNnPc4	místo
za	za	k7c2	za
deštivého	deštivý	k2eAgNnSc2d1	deštivé
počasí	počasí	k1gNnSc2	počasí
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
sedmou	sedmý	k4xOgFnSc4	sedmý
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
si	se	k3xPyFc3	se
na	na	k7c4	na
konto	konto	k1gNnSc4	konto
připsal	připsat	k5eAaPmAgMnS	připsat
dva	dva	k4xCgInPc4	dva
body	bod	k1gInPc4	bod
za	za	k7c4	za
devátou	devátý	k4xOgFnSc4	devátý
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Singapuru	Singapur	k1gInSc6	Singapur
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
jedenáctý	jedenáctý	k4xOgMnSc1	jedenáctý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
šestém	šestý	k4xOgNnSc6	šestý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
vyrovnal	vyrovnat	k5eAaBmAgMnS	vyrovnat
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Koreje	Korea	k1gFnSc2	Korea
svůj	svůj	k3xOyFgInSc4	svůj
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výsledek	výsledek	k1gInSc4	výsledek
sezony	sezona	k1gFnSc2	sezona
<g/>
,	,	kIx,	,
když	když	k8xS	když
potřetí	potřetí	k4xO	potřetí
dojel	dojet	k5eAaPmAgMnS	dojet
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgNnPc4	tři
místa	místo	k1gNnPc4	místo
si	se	k3xPyFc3	se
pohoršil	pohoršit	k5eAaPmAgMnS	pohoršit
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Abú	abú	k1gMnPc6	abú
Dhabí	Dhabí	k1gNnSc2	Dhabí
kolidoval	kolidovat	k5eAaImAgMnS	kolidovat
s	s	k7c7	s
Barrichellem	Barrichell	k1gMnSc7	Barrichell
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
obsadil	obsadit	k5eAaPmAgMnS	obsadit
deváté	devátý	k4xOgNnSc4	devátý
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
72	[number]	k4	72
body	bod	k1gInPc7	bod
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
dojel	dojet	k5eAaPmAgMnS	dojet
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devatenácti	devatenáct	k4xCc6	devatenáct
závodech	závod	k1gInPc6	závod
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
a	a	k8xC	a
pětkrát	pětkrát	k6eAd1	pětkrát
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
porazil	porazit	k5eAaPmAgMnS	porazit
týmového	týmový	k2eAgMnSc4d1	týmový
kolegu	kolega	k1gMnSc4	kolega
Rosberga	Rosberg	k1gMnSc4	Rosberg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
2011	[number]	k4	2011
====	====	k?	====
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
odstartovalo	odstartovat	k5eAaPmAgNnS	odstartovat
po	po	k7c6	po
nepokojích	nepokoj	k1gInPc6	nepokoj
v	v	k7c6	v
Bahrajnu	Bahrajn	k1gInSc6	Bahrajn
až	až	k9	až
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Schumacher	Schumachra	k1gFnPc2	Schumachra
z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
odstoupil	odstoupit	k5eAaPmAgInS	odstoupit
kvůli	kvůli	k7c3	kvůli
poškození	poškození	k1gNnSc3	poškození
vozu	vůz	k1gInSc2	vůz
od	od	k7c2	od
Alguersuariho	Alguersuari	k1gMnSc2	Alguersuari
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Malajsii	Malajsie	k1gFnSc6	Malajsie
za	za	k7c4	za
deváté	devátý	k4xOgNnSc4	devátý
místo	místo	k1gNnSc4	místo
inkasoval	inkasovat	k5eAaBmAgMnS	inkasovat
dva	dva	k4xCgInPc4	dva
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
startu	start	k1gInSc6	start
ze	z	k7c2	z
čtrnáctého	čtrnáctý	k4xOgNnSc2	čtrnáctý
místa	místo	k1gNnSc2	místo
dojel	dojet	k5eAaPmAgInS	dojet
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Číny	Čína	k1gFnSc2	Čína
osmý	osmý	k4xOgMnSc1	osmý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
části	část	k1gFnSc2	část
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
,	,	kIx,	,
po	po	k7c6	po
kolizi	kolize	k1gFnSc6	kolize
s	s	k7c7	s
Petrovem	Petrov	k1gInSc7	Petrov
obsadil	obsadit	k5eAaPmAgMnS	obsadit
nebodovanou	bodovaný	k2eNgFnSc4d1	nebodovaná
dvanáctou	dvanáctý	k4xOgFnSc4	dvanáctý
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Španělska	Španělsko	k1gNnSc2	Španělsko
dorazil	dorazit	k5eAaPmAgMnS	dorazit
šestý	šestý	k4xOgMnSc1	šestý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
startu	start	k1gInSc6	start
monacké	monacký	k2eAgFnSc2d1	monacká
grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc4	prix
kolidoval	kolidovat	k5eAaImAgMnS	kolidovat
z	z	k7c2	z
pátého	pátý	k4xOgNnSc2	pátý
místa	místo	k1gNnSc2	místo
s	s	k7c7	s
Hamiltonem	Hamilton	k1gInSc7	Hamilton
a	a	k8xC	a
po	po	k7c6	po
32	[number]	k4	32
kolech	kolo	k1gNnPc6	kolo
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
kvůli	kvůli	k7c3	kvůli
požáru	požár	k1gInSc3	požár
monopostu	monopost	k1gInSc2	monopost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Velké	velký	k2eAgFnSc2d1	velká
ceny	cena	k1gFnSc2	cena
Kanady	Kanada	k1gFnSc2	Kanada
na	na	k7c6	na
mokré	mokrý	k2eAgFnSc6d1	mokrá
trati	trať	k1gFnSc6	trať
atakoval	atakovat	k5eAaBmAgMnS	atakovat
vedoucího	vedoucí	k2eAgMnSc4d1	vedoucí
Vettela	Vettel	k1gMnSc4	Vettel
<g/>
,	,	kIx,	,
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
závodu	závod	k1gInSc2	závod
jej	on	k3xPp3gMnSc4	on
na	na	k7c6	na
osychající	osychající	k2eAgFnSc6d1	osychající
trati	trať	k1gFnSc6	trať
předjeli	předjet	k5eAaPmAgMnP	předjet
Button	Button	k1gInSc4	Button
a	a	k8xC	a
Webber	Webbra	k1gFnPc2	Webbra
a	a	k8xC	a
Schumacher	Schumachra	k1gFnPc2	Schumachra
získal	získat	k5eAaPmAgMnS	získat
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
Valencii	Valencie	k1gFnSc4	Valencie
obsadil	obsadit	k5eAaPmAgMnS	obsadit
sedmnáctou	sedmnáctý	k4xOgFnSc4	sedmnáctý
příčku	příčka	k1gFnSc4	příčka
po	po	k7c6	po
kolizi	kolize	k1gFnSc6	kolize
s	s	k7c7	s
Petrovem	Petrov	k1gInSc7	Petrov
na	na	k7c6	na
výjezdu	výjezd	k1gInSc6	výjezd
z	z	k7c2	z
boxů	box	k1gInPc2	box
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
penalizaci	penalizace	k1gFnSc6	penalizace
stop	stopa	k1gFnPc2	stopa
<g/>
&	&	k?	&
<g/>
go	go	k?	go
za	za	k7c4	za
předjetí	předjetí	k1gNnSc4	předjetí
Kobajašiho	Kobajaši	k1gMnSc2	Kobajaši
Velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
dokončil	dokončit	k5eAaPmAgInS	dokončit
devátý	devátý	k4xOgMnSc1	devátý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
trati	trať	k1gFnSc6	trať
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
osmý	osmý	k4xOgMnSc1	osmý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
26	[number]	k4	26
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
Velké	velký	k2eAgFnSc2d1	velká
ceny	cena	k1gFnSc2	cena
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
odstoupil	odstoupit	k5eAaPmAgInS	odstoupit
kvůli	kvůli	k7c3	kvůli
závadě	závada	k1gFnSc3	závada
převodovky	převodovka	k1gFnSc2	převodovka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
trénink	trénink	k1gInSc1	trénink
na	na	k7c4	na
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Belgie	Belgie	k1gFnSc2	Belgie
na	na	k7c6	na
deštivé	deštivý	k2eAgFnSc6d1	deštivá
trati	trať	k1gFnSc6	trať
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
závodu	závod	k1gInSc2	závod
odstartoval	odstartovat	k5eAaPmAgMnS	odstartovat
z	z	k7c2	z
posledního	poslední	k2eAgMnSc2d1	poslední
24	[number]	k4	24
<g/>
.	.	kIx.	.
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
když	když	k8xS	když
havaroval	havarovat	k5eAaPmAgMnS	havarovat
v	v	k7c6	v
úvodní	úvodní	k2eAgFnSc6d1	úvodní
části	část	k1gFnSc6	část
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
,	,	kIx,	,
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
pátý	pátý	k4xOgMnSc1	pátý
před	před	k7c7	před
šestým	šestý	k4xOgMnSc7	šestý
Rosbergem	Rosberg	k1gMnSc7	Rosberg
<g/>
.	.	kIx.	.
</s>
<s>
Stejnou	stejný	k2eAgFnSc4d1	stejná
příčku	příčka	k1gFnSc4	příčka
obsadil	obsadit	k5eAaPmAgMnS	obsadit
i	i	k8xC	i
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
29	[number]	k4	29
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Singapuru	Singapur	k1gInSc2	Singapur
při	při	k7c6	při
předjíždění	předjíždění	k1gNnSc6	předjíždění
Sergia	Sergius	k1gMnSc2	Sergius
Pereze	Pereze	k1gFnSc2	Pereze
těžce	těžce	k6eAd1	těžce
havaroval	havarovat	k5eAaPmAgMnS	havarovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
startu	start	k1gInSc6	start
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
místa	místo	k1gNnSc2	místo
obsadil	obsadit	k5eAaPmAgMnS	obsadit
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
šestou	šestý	k4xOgFnSc4	šestý
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
trénink	trénink	k1gInSc1	trénink
na	na	k7c4	na
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Koreje	Korea	k1gFnSc2	Korea
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
v	v	k7c6	v
deštivém	deštivý	k2eAgNnSc6d1	deštivé
počasí	počasí	k1gNnSc6	počasí
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvanáctém	dvanáctý	k4xOgNnSc6	dvanáctý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
Vitalijem	Vitalij	k1gInSc7	Vitalij
Petrovem	Petrov	k1gInSc7	Petrov
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Indie	Indie	k1gFnSc2	Indie
obsadil	obsadit	k5eAaPmAgMnS	obsadit
páté	pátý	k4xOgNnSc4	pátý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Abú	abú	k1gMnSc2	abú
Dhabí	Dhabí	k1gNnSc2	Dhabí
dokončil	dokončit	k5eAaPmAgMnS	dokončit
závod	závod	k1gInSc4	závod
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
po	po	k7c6	po
startu	start	k1gInSc6	start
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
místa	místo	k1gNnSc2	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
závodě	závod	k1gInSc6	závod
sezony	sezona	k1gFnSc2	sezona
se	se	k3xPyFc4	se
Schumacher	Schumachra	k1gFnPc2	Schumachra
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
patnáctý	patnáctý	k4xOgInSc4	patnáctý
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
při	při	k7c6	při
snaze	snaha	k1gFnSc6	snaha
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
před	před	k7c4	před
Bruna	Bruno	k1gMnSc4	Bruno
Sennu	Senna	k1gMnSc4	Senna
prorazil	prorazit	k5eAaPmAgMnS	prorazit
levou	levý	k2eAgFnSc4d1	levá
zadní	zadní	k2eAgFnSc4d1	zadní
pneumatiku	pneumatika	k1gFnSc4	pneumatika
a	a	k8xC	a
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
výměně	výměna	k1gFnSc6	výměna
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
na	na	k7c6	na
posledním	poslední	k2eAgNnSc6d1	poslední
místě	místo	k1gNnSc6	místo
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
ztrátou	ztráta	k1gFnSc7	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Senna	Senna	k6eAd1	Senna
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
incident	incident	k1gInSc4	incident
potrestán	potrestat	k5eAaPmNgMnS	potrestat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šampionátu	šampionát	k1gInSc6	šampionát
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
osmém	osmý	k4xOgInSc6	osmý
místě	místo	k1gNnSc6	místo
se	s	k7c7	s
76	[number]	k4	76
body	bod	k1gInPc7	bod
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
13	[number]	k4	13
bodů	bod	k1gInPc2	bod
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
než	než	k8xS	než
získal	získat	k5eAaPmAgMnS	získat
jeho	jeho	k3xOp3gMnSc1	jeho
týmový	týmový	k2eAgMnSc1d1	týmový
kolega	kolega	k1gMnSc1	kolega
Nico	Nico	k1gMnSc1	Nico
Rosberg	Rosberg	k1gMnSc1	Rosberg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
2012	[number]	k4	2012
====	====	k?	====
</s>
</p>
<p>
<s>
Z	z	k7c2	z
úvodní	úvodní	k2eAgFnSc2d1	úvodní
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
ze	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
jej	on	k3xPp3gMnSc4	on
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
po	po	k7c6	po
deseti	deset	k4xCc6	deset
kolech	kolo	k1gNnPc6	kolo
převodovka	převodovka	k1gFnSc1	převodovka
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
třetího	třetí	k4xOgNnSc2	třetí
místa	místo	k1gNnSc2	místo
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
na	na	k7c4	na
Velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
Malajsie	Malajsie	k1gFnSc2	Malajsie
si	se	k3xPyFc3	se
dojel	dojet	k5eAaPmAgMnS	dojet
po	po	k7c6	po
kolizi	kolize	k1gFnSc6	kolize
s	s	k7c7	s
Romainem	Romain	k1gMnSc7	Romain
Grosjeanem	Grosjean	k1gMnSc7	Grosjean
pro	pro	k7c4	pro
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
za	za	k7c4	za
desátou	desátý	k4xOgFnSc4	desátý
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čínské	čínský	k2eAgFnSc2d1	čínská
grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc4	prix
se	se	k3xPyFc4	se
kvalifikoval	kvalifikovat	k5eAaBmAgMnS	kvalifikovat
jako	jako	k9	jako
třetí	třetí	k4xOgInSc4	třetí
<g/>
,	,	kIx,	,
po	po	k7c6	po
dvanácti	dvanáct	k4xCc6	dvanáct
kolech	kolo	k1gNnPc6	kolo
jej	on	k3xPp3gMnSc4	on
ze	z	k7c2	z
závodu	závod	k1gInSc2	závod
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
chyba	chyba	k1gFnSc1	chyba
mechanika	mechanika	k1gFnSc1	mechanika
v	v	k7c6	v
boxech	box	k1gInPc6	box
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
na	na	k7c4	na
GP	GP	kA	GP
Bahrajnu	Bahrajna	k1gFnSc4	Bahrajna
si	se	k3xPyFc3	se
vyjel	vyjet	k5eAaPmAgInS	vyjet
po	po	k7c6	po
problémech	problém	k1gInPc6	problém
se	s	k7c7	s
systémem	systém	k1gInSc7	systém
DRS	DRS	kA	DRS
až	až	k6eAd1	až
18	[number]	k4	18
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
<g/>
,	,	kIx,	,
po	po	k7c6	po
penalizaci	penalizace	k1gFnSc6	penalizace
za	za	k7c4	za
výměnu	výměna	k1gFnSc4	výměna
převodovky	převodovka	k1gFnSc2	převodovka
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
z	z	k7c2	z
22	[number]	k4	22
<g/>
.	.	kIx.	.
pozice	pozice	k1gFnPc4	pozice
<g/>
,	,	kIx,	,
závod	závod	k1gInSc4	závod
dokončil	dokončit	k5eAaPmAgMnS	dokončit
desátý	desátý	k4xOgMnSc1	desátý
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
GP	GP	kA	GP
Španělska	Španělsko	k1gNnSc2	Španělsko
po	po	k7c6	po
startu	start	k1gInSc6	start
ze	z	k7c2	z
sedmého	sedmý	k4xOgNnSc2	sedmý
místa	místo	k1gNnSc2	místo
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
kolidoval	kolidovat	k5eAaImAgMnS	kolidovat
s	s	k7c7	s
Brunem	Bruno	k1gMnSc7	Bruno
Sennou	senný	k2eAgFnSc4d1	senná
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
jej	on	k3xPp3gMnSc4	on
sportovní	sportovní	k2eAgMnPc1d1	sportovní
komisaři	komisar	k1gMnPc1	komisar
potrestali	potrestat	k5eAaPmAgMnP	potrestat
posunem	posun	k1gInSc7	posun
o	o	k7c4	o
pět	pět	k4xCc4	pět
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
startovním	startovní	k2eAgInSc6d1	startovní
roštu	rošt	k1gInSc6	rošt
monacké	monacký	k2eAgFnSc2d1	monacká
velké	velký	k2eAgFnSc2d1	velká
ceny	cena	k1gFnSc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Monackou	monacký	k2eAgFnSc4d1	monacká
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
trestu	trest	k1gInSc3	trest
z	z	k7c2	z
předchozí	předchozí	k2eAgFnSc2d1	předchozí
grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc1	prix
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
výhra	výhra	k1gFnSc1	výhra
nepočítala	počítat	k5eNaImAgFnS	počítat
jako	jako	k9	jako
další	další	k2eAgInSc4d1	další
pole-position	poleosition	k1gInSc4	pole-position
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
startu	start	k1gInSc6	start
ze	z	k7c2	z
šestého	šestý	k4xOgNnSc2	šestý
místa	místo	k1gNnSc2	místo
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
najel	najet	k5eAaPmAgMnS	najet
Romain	Romain	k1gMnSc1	Romain
Grosjean	Grosjeana	k1gFnPc2	Grosjeana
<g/>
,	,	kIx,	,
Schumacher	Schumachra	k1gFnPc2	Schumachra
se	se	k3xPyFc4	se
propadl	propadnout	k5eAaPmAgInS	propadnout
a	a	k8xC	a
po	po	k7c6	po
stabilní	stabilní	k2eAgFnSc6d1	stabilní
jízdě	jízda	k1gFnSc6	jízda
na	na	k7c6	na
sedmém	sedmý	k4xOgNnSc6	sedmý
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
potížích	potíž	k1gFnPc6	potíž
s	s	k7c7	s
tlakem	tlak	k1gInSc7	tlak
paliva	palivo	k1gNnSc2	palivo
začal	začít	k5eAaPmAgInS	začít
propadat	propadat	k5eAaPmF	propadat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
63	[number]	k4	63
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
zajel	zajet	k5eAaPmAgMnS	zajet
do	do	k7c2	do
boxů	box	k1gInPc2	box
a	a	k8xC	a
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
ceně	cena	k1gFnSc6	cena
Brazílie	Brazílie	k1gFnSc1	Brazílie
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnPc4d1	poslední
VC	VC	kA	VC
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
kariéře	kariéra	k1gFnSc6	kariéra
závodního	závodní	k1gMnSc2	závodní
jezdce	jezdec	k1gMnSc2	jezdec
startoval	startovat	k5eAaBmAgMnS	startovat
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
pozice	pozice	k1gFnSc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závodě	závod	k1gInSc6	závod
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
startu	start	k1gInSc6	start
dostal	dostat	k5eAaPmAgMnS	dostat
defekt	defekt	k1gInSc4	defekt
a	a	k8xC	a
propadl	propadnout	k5eAaPmAgMnS	propadnout
se	se	k3xPyFc4	se
na	na	k7c4	na
20	[number]	k4	20
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
bouračkám	bouračka	k1gFnPc3	bouračka
soupeřů	soupeř	k1gMnPc2	soupeř
a	a	k8xC	a
proměnlivému	proměnlivý	k2eAgNnSc3d1	proměnlivé
počasí	počasí	k1gNnSc3	počasí
se	se	k3xPyFc4	se
však	však	k9	však
dokázal	dokázat	k5eAaPmAgInS	dokázat
probojovat	probojovat	k5eAaPmF	probojovat
až	až	k9	až
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
cíli	cíl	k1gInSc6	cíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
Velkou	velký	k2eAgFnSc7d1	velká
Cenou	cena	k1gFnSc7	cena
Japonska	Japonsko	k1gNnSc2	Japonsko
2012	[number]	k4	2012
Michael	Michaela	k1gFnPc2	Michaela
Schumacher	Schumachra	k1gFnPc2	Schumachra
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
definitivně	definitivně	k6eAd1	definitivně
ukončí	ukončit	k5eAaPmIp3nS	ukončit
závodní	závodní	k2eAgFnSc4d1	závodní
kariéru	kariéra	k1gFnSc4	kariéra
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Helma	helma	k1gFnSc1	helma
==	==	k?	==
</s>
</p>
<p>
<s>
Schumacher	Schumachra	k1gFnPc2	Schumachra
na	na	k7c6	na
přilbě	přilba	k1gFnSc6	přilba
původně	původně	k6eAd1	původně
nosil	nosit	k5eAaImAgMnS	nosit
pouze	pouze	k6eAd1	pouze
barvy	barva	k1gFnPc4	barva
německé	německý	k2eAgFnPc4d1	německá
vlajky	vlajka	k1gFnPc4	vlajka
a	a	k8xC	a
loga	logo	k1gNnPc4	logo
sponzorů	sponzor	k1gMnPc2	sponzor
<g/>
,	,	kIx,	,
horní	horní	k2eAgFnSc4d1	horní
část	část	k1gFnSc4	část
pokrýval	pokrývat	k5eAaImAgInS	pokrývat
modrý	modrý	k2eAgInSc1d1	modrý
kruh	kruh	k1gInSc1	kruh
s	s	k7c7	s
bílými	bílý	k2eAgFnPc7d1	bílá
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
helmu	helma	k1gFnSc4	helma
doplnil	doplnit	k5eAaPmAgMnS	doplnit
šesti	šest	k4xCc7	šest
červenými	červený	k2eAgInPc7d1	červený
proužky	proužek	k1gInPc7	proužek
nad	nad	k7c7	nad
hledím	hledí	k1gNnSc7	hledí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
odlišil	odlišit	k5eAaPmAgInS	odlišit
od	od	k7c2	od
stájového	stájový	k2eAgMnSc2d1	stájový
kolegy	kolega	k1gMnSc2	kolega
Verstappena	Verstappen	k2eAgFnSc1d1	Verstappena
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
k	k	k7c3	k
Ferrari	Ferrari	k1gMnSc1	Ferrari
si	se	k3xPyFc3	se
na	na	k7c4	na
zadní	zadní	k2eAgFnSc4d1	zadní
část	část	k1gFnSc4	část
přilby	přilba	k1gFnSc2	přilba
přidal	přidat	k5eAaPmAgMnS	přidat
vzpínajícího	vzpínající	k2eAgMnSc4d1	vzpínající
se	se	k3xPyFc4	se
koně	kůň	k1gMnSc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Monaka	Monako	k1gNnSc2	Monako
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
změnil	změnit	k5eAaPmAgMnS	změnit
vzhled	vzhled	k1gInSc4	vzhled
kvůli	kvůli	k7c3	kvůli
lepšímu	dobrý	k2eAgNnSc3d2	lepší
rozlišení	rozlišení	k1gNnSc3	rozlišení
od	od	k7c2	od
Barrichella	Barrichello	k1gNnSc2	Barrichello
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
současný	současný	k2eAgInSc1d1	současný
design	design	k1gInSc1	design
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
většinu	většina	k1gFnSc4	většina
přilby	přilba	k1gFnSc2	přilba
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Zůstaly	zůstat	k5eAaPmAgFnP	zůstat
pouze	pouze	k6eAd1	pouze
německá	německý	k2eAgFnSc1d1	německá
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
sponzorská	sponzorský	k2eAgNnPc1d1	sponzorské
loga	logo	k1gNnPc1	logo
a	a	k8xC	a
symbol	symbol	k1gInSc1	symbol
Scuderie	Scuderie	k1gFnSc2	Scuderie
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
při	při	k7c6	při
zvláštních	zvláštní	k2eAgFnPc6d1	zvláštní
příležitostech	příležitost	k1gFnPc6	příležitost
použil	použít	k5eAaPmAgMnS	použít
speciální	speciální	k2eAgInSc4d1	speciální
design	design	k1gInSc4	design
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
1998	[number]	k4	1998
vyměnil	vyměnit	k5eAaPmAgMnS	vyměnit
německou	německý	k2eAgFnSc4d1	německá
vlajku	vlajka	k1gFnSc4	vlajka
za	za	k7c4	za
šachovnicovou	šachovnicový	k2eAgFnSc4d1	šachovnicová
a	a	k8xC	a
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
za	za	k7c4	za
stříbrnou	stříbrná	k1gFnSc4	stříbrná
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Itálie	Itálie	k1gFnSc2	Itálie
2004	[number]	k4	2004
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Ferrari	Ferrari	k1gMnPc2	Ferrari
německou	německý	k2eAgFnSc4d1	německá
vlajku	vlajka	k1gFnSc4	vlajka
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
italská	italský	k2eAgFnSc1d1	italská
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
poslední	poslední	k2eAgFnSc6d1	poslední
grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc4	prix
za	za	k7c7	za
Ferrari	Ferrari	k1gMnSc7	Ferrari
(	(	kIx(	(
<g/>
Brazílie	Brazílie	k1gFnSc1	Brazílie
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
použil	použít	k5eAaPmAgInS	použít
červenou	červený	k2eAgFnSc4d1	červená
přilbu	přilba	k1gFnSc4	přilba
bez	bez	k7c2	bez
reklam	reklama	k1gFnPc2	reklama
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
názvy	název	k1gInPc7	název
vítězných	vítězný	k2eAgInPc2d1	vítězný
grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc1	prix
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
naposled	naposled	k6eAd1	naposled
použil	použít	k5eAaPmAgMnS	použít
jiné	jiný	k2eAgFnPc4d1	jiná
barvy	barva	k1gFnPc4	barva
při	při	k7c6	při
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Belgie	Belgie	k1gFnSc2	Belgie
2011	[number]	k4	2011
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
dvacetiletého	dvacetiletý	k2eAgNnSc2d1	dvacetileté
působení	působení	k1gNnSc2	působení
ve	v	k7c6	v
formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
300	[number]	k4	300
<g/>
.	.	kIx.	.
<g/>
startu	start	k1gInSc2	start
<g/>
.	.	kIx.	.
</s>
<s>
Přilba	přilba	k1gFnSc1	přilba
z	z	k7c2	z
2011	[number]	k4	2011
měla	mít	k5eAaImAgFnS	mít
zlatou	zlatý	k2eAgFnSc4d1	zlatá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
uvedeny	uveden	k2eAgInPc1d1	uveden
letopočty	letopočet	k1gInPc1	letopočet
1991	[number]	k4	1991
a	a	k8xC	a
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
doplněné	doplněný	k2eAgFnPc4d1	doplněná
o	o	k7c4	o
sedm	sedm	k4xCc4	sedm
hvězdiček	hvězdička	k1gFnPc2	hvězdička
znázorňujících	znázorňující	k2eAgInPc2d1	znázorňující
získané	získaný	k2eAgInPc4d1	získaný
tituly	titul	k1gInPc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Schumacher	Schumachra	k1gFnPc2	Schumachra
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Schuberth	Schubertha	k1gFnPc2	Schubertha
pomohl	pomoct	k5eAaPmAgMnS	pomoct
vyvinout	vyvinout	k5eAaPmF	vyvinout
první	první	k4xOgFnSc4	první
lehkou	lehký	k2eAgFnSc4d1	lehká
uhlíkovou	uhlíkový	k2eAgFnSc4d1	uhlíková
přilbu	přilba	k1gFnSc4	přilba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jezdce	jezdec	k1gMnPc4	jezdec
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
proudícím	proudící	k2eAgInSc7d1	proudící
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
měla	mít	k5eAaImAgFnS	mít
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
poslední	poslední	k2eAgFnSc6d1	poslední
GP	GP	kA	GP
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
použil	použít	k5eAaPmAgInS	použít
stejnou	stejný	k2eAgFnSc4d1	stejná
helmu	helma	k1gFnSc4	helma
<g/>
,	,	kIx,	,
jen	jen	k9	jen
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
bylo	být	k5eAaImAgNnS	být
byl	být	k5eAaImAgInS	být
vzkaz	vzkaz	k1gInSc4	vzkaz
pro	pro	k7c4	pro
fanoušky	fanoušek	k1gMnPc4	fanoušek
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Life	Life	k1gInSc1	Life
is	is	k?	is
about	about	k2eAgInSc1d1	about
passions	passions	k1gInSc1	passions
<g/>
,	,	kIx,	,
thank	thank	k1gMnSc1	thank
you	you	k?	you
for	forum	k1gNnPc2	forum
sharing	sharing	k1gInSc1	sharing
mine	minout	k5eAaImIp3nS	minout
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
vášních	vášeň	k1gFnPc6	vášeň
<g/>
,	,	kIx,	,
děkuji	děkovat	k5eAaImIp1nS	děkovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jste	být	k5eAaImIp2nP	být
se	s	k7c7	s
mnou	já	k3xPp1nSc7	já
sdíleli	sdílet	k5eAaImAgMnP	sdílet
mou	můj	k3xOp1gFnSc4	můj
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
a	a	k8xC	a
kontroverzní	kontroverzní	k2eAgInPc1d1	kontroverzní
momenty	moment	k1gInPc1	moment
==	==	k?	==
</s>
</p>
<p>
<s>
Schumacherovu	Schumacherův	k2eAgFnSc4d1	Schumacherova
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
kariéru	kariéra	k1gFnSc4	kariéra
provází	provázet	k5eAaImIp3nS	provázet
kritika	kritika	k1gFnSc1	kritika
za	za	k7c4	za
několik	několik	k4yIc4	několik
kontroverzních	kontroverzní	k2eAgInPc2d1	kontroverzní
momentů	moment	k1gInPc2	moment
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
patří	patřit	k5eAaImIp3nP	patřit
úmyslné	úmyslný	k2eAgFnPc1d1	úmyslná
kolize	kolize	k1gFnPc1	kolize
se	s	k7c7	s
soupeři	soupeř	k1gMnPc7	soupeř
o	o	k7c4	o
titul	titul	k1gInSc4	titul
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
závodech	závod	k1gInPc6	závod
sezony	sezona	k1gFnSc2	sezona
<g/>
,	,	kIx,	,
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
jízda	jízda	k1gFnSc1	jízda
a	a	k8xC	a
závody	závod	k1gInPc1	závod
poznamenané	poznamenaný	k2eAgInPc1d1	poznamenaný
týmovou	týmový	k2eAgFnSc7d1	týmová
režií	režie	k1gFnSc7	režie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
závodě	závod	k1gInSc6	závod
sezóny	sezóna	k1gFnSc2	sezóna
1994	[number]	k4	1994
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
střetu	střet	k1gInSc3	střet
s	s	k7c7	s
Damonem	Damon	k1gMnSc7	Damon
Hillem	Hill	k1gMnSc7	Hill
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
na	na	k7c4	na
Schumachera	Schumacher	k1gMnSc4	Schumacher
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
pořadí	pořadí	k1gNnSc6	pořadí
ztrácel	ztrácet	k5eAaImAgInS	ztrácet
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
tak	tak	k6eAd1	tak
reálnou	reálný	k2eAgFnSc4d1	reálná
šanci	šance	k1gFnSc4	šance
získat	získat	k5eAaPmF	získat
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
jezdci	jezdec	k1gMnPc1	jezdec
byli	být	k5eAaImAgMnP	být
vinou	vinou	k7c2	vinou
kolize	kolize	k1gFnSc2	kolize
ze	z	k7c2	z
závodu	závod	k1gInSc2	závod
vyřazeni	vyřadit	k5eAaPmNgMnP	vyřadit
a	a	k8xC	a
tak	tak	k8xC	tak
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
získal	získat	k5eAaPmAgInS	získat
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
obviňován	obviňovat	k5eAaImNgInS	obviňovat
z	z	k7c2	z
úmyslného	úmyslný	k2eAgNnSc2d1	úmyslné
zavinění	zavinění	k1gNnSc2	zavinění
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
závodní	závodní	k2eAgMnPc1d1	závodní
komisaři	komisar	k1gMnPc1	komisar
kolizi	kolize	k1gFnSc4	kolize
ohodnotili	ohodnotit	k5eAaPmAgMnP	ohodnotit
jako	jako	k8xC	jako
běžný	běžný	k2eAgInSc1d1	běžný
závodní	závodní	k2eAgInSc1d1	závodní
incident	incident	k1gInSc1	incident
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
sporný	sporný	k2eAgInSc1d1	sporný
závěrečný	závěrečný	k2eAgInSc1d1	závěrečný
závod	závod	k1gInSc1	závod
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Schumacher	Schumachra	k1gFnPc2	Schumachra
narazil	narazit	k5eAaPmAgMnS	narazit
do	do	k7c2	do
Villeneuvova	Villeneuvův	k2eAgInSc2d1	Villeneuvův
vozu	vůz	k1gInSc2	vůz
a	a	k8xC	a
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
ze	z	k7c2	z
závodu	závod	k1gInSc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
Kanaďan	Kanaďan	k1gMnSc1	Kanaďan
závod	závod	k1gInSc4	závod
dokončil	dokončit	k5eAaPmAgMnS	dokončit
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
závodě	závod	k1gInSc6	závod
byl	být	k5eAaImAgMnS	být
Němec	Němec	k1gMnSc1	Němec
za	za	k7c4	za
úmyslné	úmyslný	k2eAgNnSc4d1	úmyslné
způsobení	způsobení	k1gNnSc4	způsobení
kolize	kolize	k1gFnSc2	kolize
diskvalifikován	diskvalifikovat	k5eAaBmNgMnS	diskvalifikovat
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
sezóny	sezóna	k1gFnSc2	sezóna
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c4	za
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
jízdu	jízda	k1gFnSc4	jízda
byl	být	k5eAaImAgInS	být
potrestán	potrestat	k5eAaPmNgMnS	potrestat
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
za	za	k7c4	za
kolizi	kolize	k1gFnSc4	kolize
s	s	k7c7	s
Frentzenem	Frentzen	k1gInSc7	Frentzen
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
po	po	k7c6	po
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
na	na	k7c4	na
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Monaka	Monako	k1gNnSc2	Monako
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Schumacher	Schumachra	k1gFnPc2	Schumachra
svým	svůj	k3xOyFgInSc7	svůj
odstaveným	odstavený	k2eAgInSc7d1	odstavený
vozem	vůz	k1gInSc7	vůz
zablokoval	zablokovat	k5eAaPmAgInS	zablokovat
zatáčku	zatáčka	k1gFnSc4	zatáčka
Rascasse	Rascass	k1gMnSc2	Rascass
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podle	podle	k7c2	podle
traťových	traťový	k2eAgMnPc2d1	traťový
komisařů	komisař	k1gMnPc2	komisař
udělal	udělat	k5eAaPmAgInS	udělat
úmyslně	úmyslně	k6eAd1	úmyslně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gMnSc1	jeho
soupeř	soupeř	k1gMnSc1	soupeř
Fernando	Fernanda	k1gFnSc5	Fernanda
Alonso	Alonsa	k1gFnSc5	Alonsa
nemohl	moct	k5eNaImAgMnS	moct
zajet	zajet	k2eAgInSc4d1	zajet
lepší	dobrý	k2eAgInSc4d2	lepší
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
přitlačení	přitlačení	k1gNnSc4	přitlačení
Rubense	Rubense	k1gFnSc2	Rubense
Barrichella	Barrichello	k1gNnSc2	Barrichello
ke	k	k7c3	k
zdi	zeď	k1gFnSc3	zeď
v	v	k7c4	v
Gran	Gran	k1gInSc4	Gran
Prix	Prix	k1gInSc4	Prix
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
2010	[number]	k4	2010
byl	být	k5eAaImAgMnS	být
potrestán	potrestat	k5eAaPmNgMnS	potrestat
posunem	posun	k1gInSc7	posun
o	o	k7c4	o
deset	deset	k4xCc4	deset
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
roštu	rošt	k1gInSc6	rošt
následující	následující	k2eAgMnSc1d1	následující
grand	grand	k1gMnSc1	grand
prix	prix	k1gInSc1	prix
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
Schumacherovy	Schumacherův	k2eAgInPc1d1	Schumacherův
výsledky	výsledek	k1gInPc1	výsledek
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
Ferrari	ferrari	k1gNnSc2	ferrari
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
týmová	týmový	k2eAgFnSc1d1	týmová
režie	režie	k1gFnSc1	režie
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Rakouska	Rakousko	k1gNnSc2	Rakousko
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vedoucí	vedoucí	k2eAgInSc1d1	vedoucí
Rubens	Rubens	k1gInSc1	Rubens
Barrichello	Barrichello	k1gNnSc1	Barrichello
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
týmu	tým	k1gInSc2	tým
na	na	k7c6	na
cílové	cílový	k2eAgFnSc6d1	cílová
rovince	rovinka	k1gFnSc6	rovinka
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
kole	kolo	k1gNnSc6	kolo
pustil	pustit	k5eAaPmAgMnS	pustit
Schumachera	Schumacher	k1gMnSc4	Schumacher
před	před	k7c4	před
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
daroval	darovat	k5eAaPmAgMnS	darovat
mu	on	k3xPp3gMnSc3	on
tak	tak	k8xC	tak
vítězství	vítězství	k1gNnSc3	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Němec	Němec	k1gMnSc1	Němec
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
postavit	postavit	k5eAaPmF	postavit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
vítěze	vítěz	k1gMnSc2	vítěz
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgMnPc4	který
gentlemansky	gentlemansky	k6eAd1	gentlemansky
pustil	pustit	k5eAaPmAgMnS	pustit
svého	svůj	k3xOyFgMnSc4	svůj
kolegu	kolega	k1gMnSc4	kolega
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
ihned	ihned	k6eAd1	ihned
předal	předat	k5eAaPmAgInS	předat
i	i	k9	i
vítěznou	vítězný	k2eAgFnSc4d1	vítězná
trofej	trofej	k1gFnSc4	trofej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
obdržel	obdržet	k5eAaPmAgInS	obdržet
četná	četný	k2eAgNnPc4d1	četné
ocenění	ocenění	k1gNnPc4	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
mu	on	k3xPp3gMnSc3	on
ADAC	ADAC	kA	ADAC
udělil	udělit	k5eAaPmAgInS	udělit
titul	titul	k1gInSc4	titul
nejlepšího	dobrý	k2eAgMnSc2d3	nejlepší
sportovce	sportovec	k1gMnSc2	sportovec
v	v	k7c6	v
motorsportu	motorsport	k1gInSc6	motorsport
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
získal	získat	k5eAaPmAgInS	získat
Zlatý	zlatý	k2eAgInSc1d1	zlatý
volant	volant	k1gInSc1	volant
od	od	k7c2	od
bulvárního	bulvární	k2eAgInSc2d1	bulvární
časopisu	časopis	k1gInSc2	časopis
Bild	Bild	k1gMnSc1	Bild
am	am	k?	am
Sonntag	Sonntag	k1gMnSc1	Sonntag
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
sportovcem	sportovec	k1gMnSc7	sportovec
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
třikrát	třikrát	k6eAd1	třikrát
za	za	k7c7	za
sebou	se	k3xPyFc7	se
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
evropského	evropský	k2eAgMnSc2d1	evropský
sportovce	sportovec	k1gMnSc2	sportovec
roku	rok	k1gInSc2	rok
a	a	k8xC	a
také	také	k9	také
titul	titul	k1gInSc4	titul
šampiona	šampion	k1gMnSc2	šampion
šampionů	šampion	k1gMnPc2	šampion
od	od	k7c2	od
deníku	deník	k1gInSc2	deník
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Équipe	Équip	k1gInSc5	Équip
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
jej	on	k3xPp3gInSc4	on
deník	deník	k1gInSc4	deník
La	la	k1gNnPc2	la
Gazzetta	Gazzett	k1gMnSc2	Gazzett
dello	delnout	k5eAaImAgNnS	delnout
Sport	sport	k1gInSc4	sport
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
sportovcem	sportovec	k1gMnSc7	sportovec
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
mu	on	k3xPp3gMnSc3	on
město	město	k1gNnSc1	město
Modena	Modena	k1gFnSc1	Modena
udělilo	udělit	k5eAaPmAgNnS	udělit
čestné	čestný	k2eAgNnSc4d1	čestné
občanství	občanství	k1gNnSc4	občanství
<g/>
,	,	kIx,	,
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
obdržel	obdržet	k5eAaPmAgInS	obdržet
Řád	řád	k1gInSc1	řád
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
Italskou	italský	k2eAgFnSc4d1	italská
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
jej	on	k3xPp3gMnSc4	on
San	San	k1gMnSc1	San
Marino	Marina	k1gFnSc5	Marina
jmenovalo	jmenovat	k5eAaImAgNnS	jmenovat
svým	svůj	k3xOyFgMnSc7	svůj
zvláštním	zvláštní	k2eAgMnSc7d1	zvláštní
vyslancem	vyslanec	k1gMnSc7	vyslanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2002	[number]	k4	2002
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
přispění	přispění	k1gNnSc4	přispění
sportu	sport	k1gInSc2	sport
Šampionem	šampion	k1gMnSc7	šampion
sportu	sport	k1gInSc2	sport
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002	[number]	k4	2002
a	a	k8xC	a
2004	[number]	k4	2004
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc4	titul
světového	světový	k2eAgMnSc2d1	světový
sportovce	sportovec	k1gMnSc2	sportovec
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
také	také	k9	také
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
a	a	k8xC	a
2007	[number]	k4	2007
<g/>
;	;	kIx,	;
nikdo	nikdo	k3yNnSc1	nikdo
jiný	jiný	k2eAgMnSc1d1	jiný
nebyl	být	k5eNaImAgInS	být
vícekrát	vícekrát	k6eAd1	vícekrát
nominován	nominován	k2eAgMnSc1d1	nominován
než	než	k8xS	než
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byl	být	k5eAaImAgMnS	být
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
sportovcem	sportovec	k1gMnSc7	sportovec
roku	rok	k1gInSc2	rok
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
sportovcem	sportovec	k1gMnSc7	sportovec
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
ZDF	ZDF	kA	ZDF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
členy	člen	k1gMnPc4	člen
Spolku	spolek	k1gInSc2	spolek
sportovců	sportovec	k1gMnPc2	sportovec
za	za	k7c4	za
dárcovství	dárcovství	k1gNnSc4	dárcovství
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
převzal	převzít	k5eAaPmAgMnS	převzít
na	na	k7c6	na
slavnostním	slavnostní	k2eAgInSc6d1	slavnostní
večeru	večer	k1gInSc6	večer
FIA	FIA	kA	FIA
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
za	za	k7c4	za
přínos	přínos	k1gInSc1	přínos
automobilovému	automobilový	k2eAgInSc3d1	automobilový
sportu	sport	k1gInSc3	sport
(	(	kIx(	(
<g/>
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
a	a	k8xC	a
úsilí	úsilí	k1gNnSc4	úsilí
o	o	k7c4	o
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
F	F	kA	F
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
jej	on	k3xPp3gMnSc4	on
město	město	k1gNnSc1	město
Maranello	Maranello	k1gNnSc4	Maranello
poctilo	poctít	k5eAaPmAgNnS	poctít
čestným	čestný	k2eAgNnSc7d1	čestné
občanstvím	občanství	k1gNnSc7	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
nese	nést	k5eAaImIp3nS	nést
vlásenka	vlásenka	k1gFnSc1	vlásenka
zatáček	zatáčka	k1gFnPc2	zatáčka
osm	osm	k4xCc1	osm
a	a	k8xC	a
devět	devět	k4xCc1	devět
na	na	k7c6	na
Nürburgringu	Nürburgring	k1gInSc6	Nürburgring
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
počest	počest	k1gFnSc6	počest
název	název	k1gInSc1	název
Schumacher	Schumachra	k1gFnPc2	Schumachra
S.	S.	kA	S.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Cenu	cena	k1gFnSc4	cena
prince	princ	k1gMnSc2	princ
Austruského	Austruský	k2eAgMnSc2d1	Austruský
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sportu	sport	k1gInSc2	sport
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
sportovní	sportovní	k2eAgInPc4d1	sportovní
výkony	výkon	k1gInPc4	výkon
a	a	k8xC	a
angažovanost	angažovanost	k1gFnSc4	angažovanost
v	v	k7c6	v
humanitární	humanitární	k2eAgFnSc6d1	humanitární
pomoci	pomoc	k1gFnSc6	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
asociace	asociace	k1gFnSc1	asociace
jej	on	k3xPp3gMnSc4	on
ustanovila	ustanovit	k5eAaPmAgFnS	ustanovit
svým	svůj	k3xOyFgMnSc7	svůj
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
pro	pro	k7c4	pro
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
od	od	k7c2	od
francouzského	francouzský	k2eAgMnSc2d1	francouzský
premiéra	premiér	k1gMnSc2	premiér
François	François	k1gFnSc2	François
Fillona	Fillon	k1gMnSc2	Fillon
dostal	dostat	k5eAaPmAgMnS	dostat
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
důstojníka	důstojník	k1gMnSc2	důstojník
Řádu	řád	k1gInSc2	řád
čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charita	charita	k1gFnSc1	charita
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
podporuje	podporovat	k5eAaImIp3nS	podporovat
projekt	projekt	k1gInSc1	projekt
UNESCO	Unesco	k1gNnSc1	Unesco
Děti	dítě	k1gFnPc1	dítě
v	v	k7c6	v
nouzi	nouze	k1gFnSc6	nouze
a	a	k8xC	a
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
funkci	funkce	k1gFnSc4	funkce
zvláštního	zvláštní	k2eAgMnSc2d1	zvláštní
vyslance	vyslanec	k1gMnSc2	vyslanec
UNICEF	UNICEF	kA	UNICEF
pro	pro	k7c4	pro
sport	sport	k1gInSc4	sport
a	a	k8xC	a
výchovu	výchova	k1gFnSc4	výchova
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
získává	získávat	k5eAaImIp3nS	získávat
především	především	k6eAd1	především
organizací	organizace	k1gFnPc2	organizace
exhibičních	exhibiční	k2eAgNnPc2d1	exhibiční
fotbalových	fotbalový	k2eAgNnPc2d1	fotbalové
utkání	utkání	k1gNnPc2	utkání
se	s	k7c7	s
známými	známý	k2eAgFnPc7d1	známá
osobnostmi	osobnost	k1gFnPc7	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
přispívá	přispívat	k5eAaImIp3nS	přispívat
do	do	k7c2	do
rozpočtů	rozpočet	k1gInPc2	rozpočet
dětských	dětský	k2eAgInPc2d1	dětský
fondů	fond	k1gInPc2	fond
UNESCO	UNESCO	kA	UNESCO
a	a	k8xC	a
UNICEF	UNICEF	kA	UNICEF
<g/>
.	.	kIx.	.
</s>
<s>
Obětem	oběť	k1gFnPc3	oběť
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Asii	Asie	k1gFnSc6	Asie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
daroval	darovat	k5eAaPmAgMnS	darovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
organizace	organizace	k1gFnSc2	organizace
UNICEF	UNICEF	kA	UNICEF
deset	deset	k4xCc1	deset
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
celá	celý	k2eAgFnSc1d1	celá
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInPc3	jeho
příspěvkům	příspěvek	k1gInPc3	příspěvek
se	se	k3xPyFc4	se
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
Dakaru	Dakar	k1gInSc6	Dakar
<g/>
,	,	kIx,	,
nemocnice	nemocnice	k1gFnSc1	nemocnice
v	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
nebo	nebo	k8xC	nebo
středisko	středisko	k1gNnSc4	středisko
pro	pro	k7c4	pro
opuštěné	opuštěný	k2eAgFnPc4d1	opuštěná
děti	dítě	k1gFnPc4	dítě
v	v	k7c6	v
Limě	Lima	k1gFnSc6	Lima
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
suma	suma	k1gFnSc1	suma
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
daroval	darovat	k5eAaPmAgMnS	darovat
na	na	k7c4	na
charitativní	charitativní	k2eAgInPc4d1	charitativní
účely	účel	k1gInPc4	účel
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
letech	let	k1gInPc6	let
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
daroval	darovat	k5eAaPmAgMnS	darovat
nejméně	málo	k6eAd3	málo
padesát	padesát	k4xCc4	padesát
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Schumacher	Schumachra	k1gFnPc2	Schumachra
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
model	model	k1gInSc1	model
každého	každý	k3xTgInSc2	každý
monopostu	monopost	k1gInSc2	monopost
F	F	kA	F
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
závodil	závodit	k5eAaImAgMnS	závodit
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
benettonů	benetton	k1gInPc2	benetton
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1992	[number]	k4	1992
a	a	k8xC	a
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
asteroid	asteroid	k1gInSc1	asteroid
(	(	kIx(	(
<g/>
15761	[number]	k4	15761
<g/>
)	)	kIx)	)
Schumi	Schu	k1gFnPc7	Schu
<g/>
.	.	kIx.	.
</s>
<s>
Třikrát	třikrát	k6eAd1	třikrát
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
:	:	kIx,	:
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Auta	auto	k1gNnSc2	auto
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
namluvil	namluvit	k5eAaPmAgMnS	namluvit
Ferrari	Ferrari	k1gMnSc1	Ferrari
F	F	kA	F
<g/>
430	[number]	k4	430
<g/>
,	,	kIx,	,
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Asterix	Asterix	k1gInSc1	Asterix
a	a	k8xC	a
Olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgInS	hrát
vozataje	vozataj	k1gMnSc4	vozataj
červené	červený	k2eAgFnSc2d1	červená
stáje	stáj	k1gFnSc2	stáj
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Horst	Horst	k1gMnSc1	Horst
Schlämmer	Schlämmer	k1gMnSc1	Schlämmer
–	–	k?	–
Isch	Isch	k1gMnSc1	Isch
kandidiere	kandidirat	k5eAaPmIp3nS	kandidirat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
plynule	plynule	k6eAd1	plynule
italsky	italsky	k6eAd1	italsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kompletní	kompletní	k2eAgInPc4d1	kompletní
výsledky	výsledek	k1gInPc4	výsledek
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
==	==	k?	==
</s>
</p>
<p>
<s>
Tučně	tučně	k6eAd1	tučně
–	–	k?	–
Pole	pole	k1gNnPc1	pole
positionKurzívou	positionKurzívat	k5eAaPmIp3nP	positionKurzívat
–	–	k?	–
Nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
kolo	kolo	k1gNnSc4	kolo
závodu	závod	k1gInSc2	závod
</s>
</p>
<p>
<s>
†	†	k?	†
Nedojel	dojet	k5eNaPmAgMnS	dojet
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
klasifikován	klasifikovat	k5eAaImNgMnS	klasifikovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
odjel	odjet	k5eAaPmAgMnS	odjet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
90	[number]	k4	90
%	%	kIx~	%
délky	délka	k1gFnSc2	délka
závodu	závod	k1gInSc2	závod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rekordy	rekord	k1gInPc7	rekord
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ALLEN	Allen	k1gMnSc1	Allen
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
:	:	kIx,	:
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
XYZ	XYZ	kA	XYZ
<g/>
,	,	kIx,	,
20112	[number]	k4	20112
<g/>
.	.	kIx.	.
277	[number]	k4	277
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7388	[number]	k4	7388
<g/>
-	-	kIx~	-
<g/>
628	[number]	k4	628
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
ALESSIO	ALESSIO	kA	ALESSIO
<g/>
,	,	kIx,	,
Paolo	Paolo	k1gNnSc1	Paolo
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Čestlice	Čestlice	k1gFnSc1	Čestlice
<g/>
:	:	kIx,	:
Rebo	Rebo	k1gNnSc1	Rebo
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
188	[number]	k4	188
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7234	[number]	k4	7234
<g/>
-	-	kIx~	-
<g/>
317	[number]	k4	317
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PETRÁNSKY	PETRÁNSKY	kA	PETRÁNSKY
<g/>
,	,	kIx,	,
L.	L.	kA	L.
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Motýl	motýl	k1gMnSc1	motýl
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
127	[number]	k4	127
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
88978	[number]	k4	88978
<g/>
-	-	kIx~	-
<g/>
55	[number]	k4	55
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KNUPP	KNUPP	kA	KNUPP
<g/>
,	,	kIx,	,
Willy	Will	k1gInPc7	Will
<g/>
.	.	kIx.	.
</s>
<s>
Red	Red	k?	Red
Magic	Magic	k1gMnSc1	Magic
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
und	und	k?	und
Ferrari	Ferrari	k1gMnSc1	Ferrari
<g/>
.	.	kIx.	.
</s>
<s>
Hart	Hart	k2eAgInSc1d1	Hart
am	am	k?	am
Limit	limit	k1gInSc1	limit
<g/>
..	..	k?	..
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
<g/>
:	:	kIx,	:
Motorbuch	Motorbuch	k1gInSc1	Motorbuch
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
160	[number]	k4	160
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
613	[number]	k4	613
<g/>
-	-	kIx~	-
<g/>
30377	[number]	k4	30377
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Michael	Michaela	k1gFnPc2	Michaela
Schumacher	Schumachra	k1gFnPc2	Schumachra
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Michael	Michaela	k1gFnPc2	Michaela
Schumacher	Schumachra	k1gFnPc2	Schumachra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Michael	Michael	k1gMnSc1	Michael
Schumachera	Schumacher	k1gMnSc2	Schumacher
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Motokárová	motokárový	k2eAgFnSc1d1	motokárová
dráha	dráha	k1gFnSc1	dráha
a	a	k8xC	a
museum	museum	k1gNnSc1	museum
Schumachera	Schumacher	k1gMnSc2	Schumacher
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Motokárový	motokárový	k2eAgInSc1d1	motokárový
tým	tým	k1gInSc1	tým
Kaiser-Schumacher-Muchow	Kaiser-Schumacher-Muchow	k1gFnSc2	Kaiser-Schumacher-Muchow
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Profil	profil	k1gInSc1	profil
na	na	k7c6	na
webu	web	k1gInSc6	web
formula	formout	k5eAaPmAgFnS	formout
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
Race-F	Race-F	k1gFnSc2	Race-F
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
