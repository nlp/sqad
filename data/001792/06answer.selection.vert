<s>
Artušovská	artušovský	k2eAgFnSc1d1
legenda	legenda	k1gFnSc1
praví	pravit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
bájný	bájný	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
Avalon	Avalon	k1gInSc1
je	být	k5eAaImIp3nS
místem	místo	k1gNnSc7
posledního	poslední	k2eAgInSc2d1
odpočinku	odpočinek	k1gInSc2
legendárního	legendární	k2eAgMnSc4d1
krále	král	k1gMnSc4
Artuše	Artuš	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
ženy	žena	k1gFnSc2
<g/>
.	.	kIx.
</s>