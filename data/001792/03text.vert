<s>
Avalon	Avalon	k1gInSc1	Avalon
je	být	k5eAaImIp3nS	být
legendární	legendární	k2eAgInSc4d1	legendární
ostrov	ostrov	k1gInSc4	ostrov
artušovských	artušovský	k2eAgFnPc2d1	artušovská
legend	legenda	k1gFnPc2	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
keltského	keltský	k2eAgNnSc2d1	keltské
slova	slovo	k1gNnSc2	slovo
abal	abala	k1gFnPc2	abala
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
jablko	jablko	k1gNnSc4	jablko
<g/>
,	,	kIx,	,
kterými	který	k3yRgMnPc7	který
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
ostrov	ostrov	k1gInSc4	ostrov
znám	znát	k5eAaImIp1nS	znát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
symbolice	symbolika	k1gFnSc6	symbolika
jablka	jablko	k1gNnSc2	jablko
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
promítá	promítat	k5eAaImIp3nS	promítat
jak	jak	k6eAd1	jak
keltská	keltský	k2eAgFnSc1d1	keltská
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
symbolika	symbolika	k1gFnSc1	symbolika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
u	u	k7c2	u
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
autorů	autor	k1gMnPc2	autor
docházelo	docházet	k5eAaImAgNnS	docházet
i	i	k9	i
k	k	k7c3	k
ovlivnění	ovlivnění	k1gNnSc4	ovlivnění
řeckou	řecký	k2eAgFnSc7d1	řecká
mytologií	mytologie	k1gFnSc7	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
ostrov	ostrov	k1gInSc4	ostrov
Avalon	Avalon	k1gInSc4	Avalon
poprvé	poprvé	k6eAd1	poprvé
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
díle	díl	k1gInSc6	díl
Historia	Historium	k1gNnSc2	Historium
regum	regum	k1gInSc1	regum
Britanniae	Britanniae	k1gFnSc4	Britanniae
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1136	[number]	k4	1136
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Geoffrey	Geoffreum	k1gNnPc7	Geoffreum
z	z	k7c2	z
Monmouthu	Monmouth	k1gInSc2	Monmouth
<g/>
.	.	kIx.	.
</s>
<s>
Artušovská	artušovský	k2eAgFnSc1d1	artušovská
legenda	legenda	k1gFnSc1	legenda
praví	pravit	k5eAaImIp3nS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bájný	bájný	k2eAgInSc1d1	bájný
ostrov	ostrov	k1gInSc1	ostrov
Avalon	Avalon	k1gInSc1	Avalon
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
legendárního	legendární	k2eAgMnSc4d1	legendární
krále	král	k1gMnSc4	král
Artuše	Artuš	k1gMnSc4	Artuš
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
hledání	hledání	k1gNnSc2	hledání
posledního	poslední	k2eAgNnSc2d1	poslední
místa	místo	k1gNnSc2	místo
odpočinku	odpočinek	k1gInSc2	odpočinek
krále	král	k1gMnSc2	král
Artuše	Artuš	k1gMnSc2	Artuš
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1195	[number]	k4	1195
Giraldus	Giraldus	k1gInSc1	Giraldus
Cambrensis	Cambrensis	k1gFnSc2	Cambrensis
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
poznámkách	poznámka	k1gFnPc6	poznámka
<g/>
.	.	kIx.	.
</s>
