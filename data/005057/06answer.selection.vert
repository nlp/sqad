<s>
První	první	k4xOgInSc1	první
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
svitek	svitek	k1gInSc1	svitek
papyru	papyr	k1gInSc2	papyr
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nepopsaný	popsaný	k2eNgInSc1d1	nepopsaný
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
hrobky	hrobka	k1gFnSc2	hrobka
velmože	velmož	k1gMnPc4	velmož
Hemaky	Hemak	k1gInPc1	Hemak
v	v	k7c4	v
Sakkáře	Sakkář	k1gMnPc4	Sakkář
z	z	k7c2	z
období	období	k1gNnSc2	období
1	[number]	k4	1
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInPc1d3	nejstarší
papyry	papyr	k1gInPc1	papyr
popsané	popsaný	k2eAgInPc1d1	popsaný
hieroglyfy	hieroglyf	k1gInPc1	hieroglyf
jsou	být	k5eAaImIp3nP	být
doloženy	doložit	k5eAaPmNgInP	doložit
z	z	k7c2	z
konce	konec	k1gInSc2	konec
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
(	(	kIx(	(
<g/>
archiv	archiv	k1gInSc1	archiv
nalezený	nalezený	k2eAgInSc1d1	nalezený
v	v	k7c6	v
Abúsíru	Abúsír	k1gInSc6	Abúsír
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
