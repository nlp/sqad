<s>
Papyrus	papyrus	k1gInSc1	papyrus
je	být	k5eAaImIp3nS	být
psací	psací	k2eAgInSc1d1	psací
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
,	,	kIx,	,
lehkost	lehkost	k1gFnSc4	lehkost
a	a	k8xC	a
skladnost	skladnost	k1gFnSc4	skladnost
brzy	brzy	k6eAd1	brzy
vytlačil	vytlačit	k5eAaPmAgInS	vytlačit
ostatní	ostatní	k2eAgInPc4d1	ostatní
psací	psací	k2eAgInPc4d1	psací
materiály	materiál	k1gInPc4	materiál
(	(	kIx(	(
<g/>
hliněné	hliněný	k2eAgFnSc2d1	hliněná
<g/>
,	,	kIx,	,
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
<g/>
,	,	kIx,	,
kovové	kovový	k2eAgFnSc2d1	kovová
<g/>
,	,	kIx,	,
voskované	voskovaný	k2eAgFnSc2d1	voskovaná
destičky	destička	k1gFnSc2	destička
<g/>
,	,	kIx,	,
ostraka	ostrak	k1gMnSc2	ostrak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
nikdy	nikdy	k6eAd1	nikdy
zcela	zcela	k6eAd1	zcela
nenahradil	nahradit	k5eNaPmAgInS	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
byla	být	k5eAaImAgFnS	být
stébla	stéblo	k1gNnPc1	stéblo
šáchoru	šáchor	k1gInSc2	šáchor
papírodárného	papírodárný	k2eAgInSc2d1	papírodárný
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
rostl	růst	k5eAaImAgInS	růst
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
těžen	těžit	k5eAaImNgInS	těžit
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
výroba	výroba	k1gFnSc1	výroba
papyru	papyr	k1gInSc2	papyr
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vyvážel	vyvážet	k5eAaImAgInS	vyvážet
do	do	k7c2	do
celého	celý	k2eAgNnSc2d1	celé
středomoří	středomoří	k1gNnSc2	středomoří
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
preferovaným	preferovaný	k2eAgInSc7d1	preferovaný
materiálem	materiál	k1gInSc7	materiál
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
antickém	antický	k2eAgInSc6d1	antický
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
papyrus	papyrus	k1gInSc1	papyrus
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
koptštiny	koptština	k1gFnSc2	koptština
(	(	kIx(	(
<g/>
pozdní	pozdní	k2eAgFnPc1d1	pozdní
fáze	fáze	k1gFnPc1	fáze
vývoje	vývoj	k1gInSc2	vývoj
staroegyptštiny	staroegyptština	k1gFnPc1	staroegyptština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
slovo	slovo	k1gNnSc4	slovo
papuro	papura	k1gFnSc5	papura
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
patřící	patřící	k2eAgFnSc1d1	patřící
králi	král	k1gMnPc7	král
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
prozrazuje	prozrazovat	k5eAaImIp3nS	prozrazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
ptolemaiovské	ptolemaiovský	k2eAgFnSc6d1	ptolemaiovský
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
královským	královský	k2eAgInSc7d1	královský
monopolem	monopol	k1gInSc7	monopol
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
svitek	svitek	k1gInSc1	svitek
papyru	papyr	k1gInSc2	papyr
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nepopsaný	popsaný	k2eNgInSc1d1	nepopsaný
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
hrobky	hrobka	k1gFnSc2	hrobka
velmože	velmož	k1gMnPc4	velmož
Hemaky	Hemak	k1gInPc1	Hemak
v	v	k7c4	v
Sakkáře	Sakkář	k1gMnPc4	Sakkář
z	z	k7c2	z
období	období	k1gNnSc2	období
1	[number]	k4	1
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInPc1d3	nejstarší
papyry	papyr	k1gInPc1	papyr
popsané	popsaný	k2eAgInPc1d1	popsaný
hieroglyfy	hieroglyf	k1gInPc1	hieroglyf
jsou	být	k5eAaImIp3nP	být
doloženy	doložit	k5eAaPmNgInP	doložit
z	z	k7c2	z
konce	konec	k1gInSc2	konec
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
(	(	kIx(	(
<g/>
archiv	archiv	k1gInSc1	archiv
nalezený	nalezený	k2eAgInSc1d1	nalezený
v	v	k7c6	v
Abúsíru	Abúsír	k1gInSc6	Abúsír
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Papyrus	papyrus	k1gInSc1	papyrus
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
až	až	k9	až
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1100	[number]	k4	1100
po	po	k7c6	po
Kr.	Kr.	k1gFnSc6	Kr.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
změna	změna	k1gFnSc1	změna
egyptského	egyptský	k2eAgNnSc2d1	egyptské
klimatu	klima	k1gNnSc2	klima
způsobila	způsobit	k5eAaPmAgFnS	způsobit
vymizení	vymizení	k1gNnSc4	vymizení
příslušné	příslušný	k2eAgFnSc2d1	příslušná
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
,	,	kIx,	,
přetrvaly	přetrvat	k5eAaPmAgFnP	přetrvat
jen	jen	k9	jen
divoké	divoký	k2eAgFnPc4d1	divoká
formy	forma	k1gFnPc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Papyrus	papyrus	k1gInSc1	papyrus
však	však	k9	však
již	již	k6eAd1	již
od	od	k7c2	od
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
po	po	k7c6	po
Kr.	Kr.	k1gFnSc6	Kr.
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
nahrazován	nahrazovat	k5eAaImNgInS	nahrazovat
novým	nový	k2eAgInSc7d1	nový
psacím	psací	k2eAgInSc7d1	psací
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
pergamenem	pergamen	k1gInSc7	pergamen
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
papyry	papyr	k1gInPc7	papyr
používaly	používat	k5eAaImAgInP	používat
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
snad	snad	k9	snad
všude	všude	k6eAd1	všude
<g/>
,	,	kIx,	,
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
se	se	k3xPyFc4	se
jen	jen	k9	jen
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vládne	vládnout	k5eAaImIp3nS	vládnout
suché	suchý	k2eAgNnSc1d1	suché
klima	klima	k1gNnSc1	klima
<g/>
;	;	kIx,	;
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
se	se	k3xPyFc4	se
papyrus	papyrus	k1gInSc1	papyrus
rozpadá	rozpadat	k5eAaPmIp3nS	rozpadat
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznačnější	význačný	k2eAgInPc1d3	nejvýznačnější
papyrové	papyrový	k2eAgInPc1d1	papyrový
nálezy	nález	k1gInPc1	nález
tedy	tedy	k9	tedy
pocházejí	pocházet	k5eAaImIp3nP	pocházet
opět	opět	k6eAd1	opět
především	především	k9	především
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Studiem	studio	k1gNnSc7	studio
starověkých	starověký	k2eAgInPc2d1	starověký
papyrů	papyr	k1gInPc2	papyr
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
papyrologie	papyrologie	k1gFnSc1	papyrologie
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
papyrus	papyrus	k1gInSc1	papyrus
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
vyráběl	vyrábět	k5eAaImAgMnS	vyrábět
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
dnes	dnes	k6eAd1	dnes
úplně	úplně	k6eAd1	úplně
přesně	přesně	k6eAd1	přesně
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
papyrus	papyrus	k1gInSc1	papyrus
přestal	přestat	k5eAaPmAgInS	přestat
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1100	[number]	k4	1100
používat	používat	k5eAaImF	používat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
umění	umění	k1gNnSc1	umění
zapomenuto	zapomnět	k5eAaImNgNnS	zapomnět
<g/>
.	.	kIx.	.
</s>
<s>
Znalost	znalost	k1gFnSc1	znalost
výroby	výroba	k1gFnSc2	výroba
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
až	až	k9	až
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Pliniových	Pliniový	k2eAgFnPc2d1	Pliniový
Přírodních	přírodní	k2eAgFnPc2d1	přírodní
dějin	dějiny	k1gFnPc2	dějiny
(	(	kIx(	(
<g/>
Naturalis	Naturalis	k1gInSc1	Naturalis
Historia	Historium	k1gNnSc2	Historium
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dalších	další	k2eAgInPc2d1	další
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
nalezených	nalezený	k2eAgInPc2d1	nalezený
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
nepodařilo	podařit	k5eNaPmAgNnS	podařit
vyrobit	vyrobit	k5eAaPmF	vyrobit
papyrus	papyrus	k1gInSc4	papyrus
takové	takový	k3xDgFnSc2	takový
kvality	kvalita	k1gFnSc2	kvalita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vyrovnala	vyrovnat	k5eAaPmAgFnS	vyrovnat
starověkým	starověký	k2eAgInPc3d1	starověký
exemplářům	exemplář	k1gInPc3	exemplář
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
se	se	k3xPyFc4	se
postupovalo	postupovat	k5eAaImAgNnS	postupovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
typicky	typicky	k6eAd1	typicky
trojhranná	trojhranný	k2eAgNnPc4d1	trojhranné
stébla	stéblo	k1gNnPc4	stéblo
šáchoru	šáchor	k1gInSc2	šáchor
zbavila	zbavit	k5eAaPmAgFnS	zbavit
svrchní	svrchní	k2eAgFnSc1d1	svrchní
zelené	zelený	k2eAgFnPc4d1	zelená
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
obnažila	obnažit	k5eAaPmAgFnS	obnažit
bílá	bílý	k2eAgFnSc1d1	bílá
dřeň	dřeň	k1gFnSc1	dřeň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
stonek	stonek	k1gInSc4	stonek
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
podélně	podélně	k6eAd1	podélně
rozřezala	rozřezat	k5eAaPmAgFnS	rozřezat
na	na	k7c4	na
(	(	kIx(	(
<g/>
cca	cca	kA	cca
1-2	[number]	k4	1-2
mm	mm	kA	mm
<g/>
)	)	kIx)	)
tenké	tenký	k2eAgInPc4d1	tenký
plátky	plátek	k1gInPc4	plátek
<g/>
.	.	kIx.	.
</s>
<s>
Jakého	jaký	k3yIgInSc2	jaký
způsobu	způsob	k1gInSc2	způsob
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
k	k	k7c3	k
řezání	řezání	k1gNnSc3	řezání
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
předmětem	předmět	k1gInSc7	předmět
dohadů	dohad	k1gInPc2	dohad
<g/>
.	.	kIx.	.
</s>
<s>
Proužky	proužka	k1gFnPc1	proužka
se	se	k3xPyFc4	se
namáčely	namáčet	k5eAaImAgFnP	namáčet
na	na	k7c4	na
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
změkly	změknout	k5eAaPmAgFnP	změknout
–	–	k?	–
samotná	samotný	k2eAgFnSc1d1	samotná
dřeň	dřeň	k1gFnSc1	dřeň
šáchoru	šáchor	k1gInSc2	šáchor
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hygrofyt	hygrofyt	k1gInSc1	hygrofyt
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
křehká	křehký	k2eAgFnSc1d1	křehká
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
láme	lámat	k5eAaImIp3nS	lámat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
dřeň	dřeň	k1gFnSc1	dřeň
máčela	máčet	k5eAaImAgFnS	máčet
cca	cca	kA	cca
6	[number]	k4	6
dní	den	k1gInPc2	den
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
světlého	světlý	k2eAgNnSc2d1	světlé
a	a	k8xC	a
12	[number]	k4	12
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
<g/>
-li	i	k?	-li
být	být	k5eAaImF	být
výsledkem	výsledek	k1gInSc7	výsledek
tmavě	tmavě	k6eAd1	tmavě
hnědý	hnědý	k2eAgInSc4d1	hnědý
papyrus	papyrus	k1gInSc4	papyrus
<g/>
.	.	kIx.	.
</s>
<s>
Panovalo	panovat	k5eAaImAgNnS	panovat
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
je	být	k5eAaImIp3nS	být
nilská	nilský	k2eAgFnSc1d1	nilská
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
nemá	mít	k5eNaImIp3nS	mít
opodstatnění	opodstatnění	k1gNnSc4	opodstatnění
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
proužky	proužka	k1gFnPc1	proužka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
paličkou	palička	k1gFnSc7	palička
<g/>
)	)	kIx)	)
jemně	jemně	k6eAd1	jemně
zpracovaly	zpracovat	k5eAaPmAgFnP	zpracovat
a	a	k8xC	a
válečkem	váleček	k1gInSc7	váleček
slisovaly	slisovat	k5eAaPmAgFnP	slisovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
ploché	plochý	k2eAgFnPc1d1	plochá
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
rovnaly	rovnat	k5eAaImAgFnP	rovnat
těsně	těsně	k6eAd1	těsně
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
nebo	nebo	k8xC	nebo
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
překryvem	překryv	k1gInSc7	překryv
na	na	k7c4	na
rovný	rovný	k2eAgInSc4d1	rovný
podklad	podklad	k1gInSc4	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
se	se	k3xPyFc4	se
napříč	napříč	k6eAd1	napříč
kladla	klást	k5eAaImAgFnS	klást
další	další	k2eAgFnSc1d1	další
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Vrstev	vrstva	k1gFnPc2	vrstva
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
několik	několik	k4yIc4	několik
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
list	list	k1gInSc1	list
papyru	papyr	k1gInSc2	papyr
silný	silný	k2eAgInSc1d1	silný
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
dvě	dva	k4xCgFnPc1	dva
<g/>
.	.	kIx.	.
</s>
<s>
Výsledné	výsledný	k2eAgInPc1d1	výsledný
příčně	příčně	k6eAd1	příčně
přeložené	přeložený	k2eAgInPc1d1	přeložený
plátky	plátek	k1gInPc1	plátek
byly	být	k5eAaImAgInP	být
vloženy	vložit	k5eAaPmNgInP	vložit
na	na	k7c6	na
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
pod	pod	k7c4	pod
mechanický	mechanický	k2eAgInSc4d1	mechanický
lis	lis	k1gInSc4	lis
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
několik	několik	k4yIc1	několik
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
novodobé	novodobý	k2eAgFnSc2d1	novodobá
výroby	výroba	k1gFnSc2	výroba
šest	šest	k4xCc4	šest
<g/>
)	)	kIx)	)
dní	den	k1gInPc2	den
sušily	sušit	k5eAaImAgFnP	sušit
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
stlačení	stlačení	k1gNnSc1	stlačení
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
vrstvy	vrstva	k1gFnPc1	vrstva
plátků	plátek	k1gInPc2	plátek
k	k	k7c3	k
sobě	se	k3xPyFc3	se
přilnuly	přilnout	k5eAaPmAgInP	přilnout
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c4	po
vytažení	vytažení	k1gNnSc4	vytažení
z	z	k7c2	z
lisu	lis	k1gInSc2	lis
držely	držet	k5eAaImAgFnP	držet
vcelku	vcelku	k6eAd1	vcelku
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
papyrus	papyrus	k1gInSc1	papyrus
namáhán	namáhat	k5eAaImNgInS	namáhat
nebo	nebo	k8xC	nebo
překládán	překládat	k5eAaImNgInS	překládat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
výrobce	výrobce	k1gMnSc1	výrobce
zarovnal	zarovnat	k5eAaPmAgMnS	zarovnat
hrubý	hrubý	k2eAgInSc4d1	hrubý
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
případně	případně	k6eAd1	případně
ořezal	ořezat	k5eAaPmAgMnS	ořezat
nerovné	rovný	k2eNgInPc4d1	nerovný
okraje	okraj	k1gInPc4	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
listy	list	k1gInPc1	list
papyru	papyr	k1gInSc2	papyr
se	se	k3xPyFc4	se
lepily	lepit	k5eAaImAgFnP	lepit
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
škrobovou	škrobový	k2eAgFnSc7d1	škrobová
kaší	kaše	k1gFnSc7	kaše
<g/>
,	,	kIx,	,
tak	tak	k9	tak
že	že	k8xS	že
mohl	moct	k5eAaImAgInS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
i	i	k9	i
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
svitek	svitek	k1gInSc4	svitek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
dobách	doba	k1gFnPc6	doba
(	(	kIx(	(
<g/>
přelom	přelom	k1gInSc4	přelom
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
svitků	svitek	k1gInPc2	svitek
používaly	používat	k5eAaImAgInP	používat
i	i	k9	i
papyrové	papyrový	k2eAgInPc1d1	papyrový
kodexy	kodex	k1gInPc1	kodex
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
byly	být	k5eAaImAgInP	být
svázány	svázat	k5eAaPmNgInP	svázat
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
dnešní	dnešní	k2eAgFnSc2d1	dnešní
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
listy	list	k1gInPc1	list
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
popsány	popsat	k5eAaPmNgInP	popsat
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
šetřilo	šetřit	k5eAaImAgNnS	šetřit
místem	místo	k1gNnSc7	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
kolem	kolem	k7c2	kolem
Luxoru	Luxor	k1gInSc2	Luxor
a	a	k8xC	a
Asuánu	Asuán	k1gInSc2	Asuán
výrobny	výrobna	k1gFnSc2	výrobna
papyru	papyr	k1gInSc2	papyr
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
jako	jako	k8xS	jako
suvenýru	suvenýr	k1gInSc6	suvenýr
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
<g/>
.	.	kIx.	.
</s>
<s>
Papyrus	papyrus	k1gInSc1	papyrus
se	se	k3xPyFc4	se
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
kvalitách	kvalita	k1gFnPc6	kvalita
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
lišily	lišit	k5eAaImAgFnP	lišit
i	i	k9	i
cenou	cena	k1gFnSc7	cena
<g/>
.	.	kIx.	.
</s>
<s>
Nejdražší	drahý	k2eAgInSc4d3	nejdražší
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgInP	používat
na	na	k7c4	na
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
nejlevnější	levný	k2eAgFnPc4d3	nejlevnější
na	na	k7c4	na
osobní	osobní	k2eAgInPc4d1	osobní
dopisy	dopis	k1gInPc4	dopis
<g/>
,	,	kIx,	,
záznamy	záznam	k1gInPc1	záznam
apod.	apod.	kA	apod.
</s>
<s>
Na	na	k7c4	na
papyrus	papyrus	k1gInSc4	papyrus
se	se	k3xPyFc4	se
psalo	psát	k5eAaImAgNnS	psát
násadkami	násadka	k1gFnPc7	násadka
ze	z	k7c2	z
seříznutého	seříznutý	k2eAgInSc2d1	seříznutý
rákosu	rákos	k1gInSc2	rákos
<g/>
.	.	kIx.	.
</s>
<s>
Inkoust	inkoust	k1gMnSc1	inkoust
se	se	k3xPyFc4	se
vyráběl	vyrábět	k5eAaImAgMnS	vyrábět
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
sazí	saze	k1gFnPc2	saze
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
arabské	arabský	k2eAgFnSc2d1	arabská
gumy	guma	k1gFnSc2	guma
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
psalo	psát	k5eAaImAgNnS	psát
na	na	k7c4	na
svitky	svitek	k1gInPc4	svitek
<g/>
,	,	kIx,	,
psalo	psát	k5eAaImAgNnS	psát
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
jen	jen	k9	jen
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
<g/>
,	,	kIx,	,
vnější	vnější	k2eAgFnSc1d1	vnější
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
čistá	čistý	k2eAgFnSc1d1	čistá
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
v	v	k7c6	v
případě	případ	k1gInSc6	případ
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
sepsání	sepsání	k1gNnSc3	sepsání
skládaly	skládat	k5eAaImAgFnP	skládat
a	a	k8xC	a
sešívaly	sešívat	k5eAaImAgFnP	sešívat
či	či	k8xC	či
svazovaly	svazovat	k5eAaImAgFnP	svazovat
šňůrkou	šňůrka	k1gFnSc7	šňůrka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
též	též	k9	též
psalo	psát	k5eAaImAgNnS	psát
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vnější	vnější	k2eAgFnSc4d1	vnější
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
psala	psát	k5eAaImAgFnS	psát
adresa	adresa	k1gFnSc1	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Psalo	psát	k5eAaImAgNnS	psát
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
v	v	k7c6	v
sloupcích	sloupec	k1gInPc6	sloupec
po	po	k7c6	po
řádcích	řádek	k1gInPc6	řádek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
způsob	způsob	k1gInSc4	způsob
pro	pro	k7c4	pro
psaní	psaní	k1gNnSc4	psaní
a	a	k8xC	a
čtení	čtení	k1gNnSc4	čtení
v	v	k7c6	v
případě	případ	k1gInSc6	případ
svitků	svitek	k1gInPc2	svitek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
zápisu	zápis	k1gInSc2	zápis
se	se	k3xPyFc4	se
přenesl	přenést	k5eAaPmAgInS	přenést
později	pozdě	k6eAd2	pozdě
i	i	k9	i
na	na	k7c4	na
kodexy	kodex	k1gInPc4	kodex
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
též	též	k9	též
objevují	objevovat	k5eAaImIp3nP	objevovat
sloupce	sloupec	k1gInPc1	sloupec
<g/>
.	.	kIx.	.
</s>
