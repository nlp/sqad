<s>
Sinokvět	Sinokvět	k5eAaPmF	Sinokvět
chrpovitý	chrpovitý	k2eAgMnSc1d1	chrpovitý
(	(	kIx(	(
<g/>
Jurinea	Jurinea	k1gMnSc1	Jurinea
cyanoides	cyanoides	k1gMnSc1	cyanoides
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
druhů	druh	k1gInPc2	druh
rodu	rod	k1gInSc2	rod
sinokvět	sinokvět	k1gInSc1	sinokvět
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
rostou	růst	k5eAaImIp3nP	růst
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rostlina	rostlina	k1gFnSc1	rostlina
svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
vzdáleně	vzdáleně	k6eAd1	vzdáleně
připomínající	připomínající	k2eAgInSc4d1	připomínající
bodlák	bodlák	k1gInSc4	bodlák
nebo	nebo	k8xC	nebo
chrpu	chrpa	k1gFnSc4	chrpa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
přírodě	příroda	k1gFnSc6	příroda
velice	velice	k6eAd1	velice
vzácná	vzácný	k2eAgFnSc1d1	vzácná
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Druh	druh	k1gInSc1	druh
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
oblasti	oblast	k1gFnSc2	oblast
rozkládající	rozkládající	k2eAgFnSc2d1	rozkládající
se	se	k3xPyFc4	se
od	od	k7c2	od
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
přes	přes	k7c4	přes
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
,	,	kIx,	,
okolí	okolí	k1gNnSc4	okolí
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
a	a	k8xC	a
západní	západní	k2eAgFnSc4d1	západní
Sibiř	Sibiř	k1gFnSc4	Sibiř
až	až	k9	až
do	do	k7c2	do
Turkmenistánu	Turkmenistán	k1gInSc2	Turkmenistán
a	a	k8xC	a
po	po	k7c4	po
okraj	okraj	k1gInSc4	okraj
Altaje	Altaj	k1gInSc2	Altaj
<g/>
.	.	kIx.	.
</s>
<s>
Druhotně	druhotně	k6eAd1	druhotně
se	se	k3xPyFc4	se
sinokvět	sinokvět	k1gInSc1	sinokvět
chrpovitý	chrpovitý	k2eAgInSc1d1	chrpovitý
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
Střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
ostrůvkovitě	ostrůvkovitě	k6eAd1	ostrůvkovitě
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
refugium	refugium	k1gNnSc1	refugium
na	na	k7c6	na
lokalitách	lokalita	k1gFnPc6	lokalita
reliktního	reliktní	k2eAgNnSc2d1	reliktní
charakterů	charakter	k1gInPc2	charakter
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
Rýna	Rýn	k1gInSc2	Rýn
a	a	k8xC	a
Mohanu	Mohan	k1gInSc2	Mohan
a	a	k8xC	a
v	v	k7c6	v
předhůří	předhůří	k1gNnSc6	předhůří
Harzu	Harz	k1gInSc2	Harz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
nacházel	nacházet	k5eAaImAgMnS	nacházet
asi	asi	k9	asi
na	na	k7c6	na
30	[number]	k4	30
stanovištích	stanoviště	k1gNnPc6	stanoviště
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
vátých	vátý	k2eAgInPc2d1	vátý
písků	písek	k1gInPc2	písek
ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
Polabí	Polabí	k1gNnSc6	Polabí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
tento	tento	k3xDgInSc1	tento
velice	velice	k6eAd1	velice
vzácný	vzácný	k2eAgInSc4d1	vzácný
druh	druh	k1gInSc4	druh
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
jen	jen	k9	jen
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
lokalitách	lokalita	k1gFnPc6	lokalita
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Mělník	Mělník	k1gInSc1	Mělník
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
železniční	železniční	k2eAgFnSc1d1	železniční
násep	násep	k1gFnSc1	násep
v	v	k7c6	v
přírodní	přírodní	k2eAgFnSc6d1	přírodní
památce	památka	k1gFnSc6	památka
Písčina	písčina	k1gFnSc1	písčina
u	u	k7c2	u
Tišic	Tišice	k1gFnPc2	Tišice
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
<g/>
,	,	kIx,	,
obnovená	obnovený	k2eAgFnSc1d1	obnovená
lokalita	lokalita	k1gFnSc1	lokalita
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přírodní	přírodní	k2eAgFnSc6d1	přírodní
památce	památka	k1gFnSc6	památka
Písčina	písčina	k1gFnSc1	písčina
u	u	k7c2	u
Tuhaně	Tuhaň	k1gFnSc2	Tuhaň
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
byl	být	k5eAaImAgInS	být
původní	původní	k2eAgInSc1d1	původní
výskyt	výskyt	k1gInSc1	výskyt
obnoven	obnovit	k5eAaPmNgInS	obnovit
výsevem	výsev	k1gInSc7	výsev
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
a	a	k8xC	a
výsadbou	výsadba	k1gFnSc7	výsadba
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
uváděná	uváděný	k2eAgFnSc1d1	uváděná
lokalita	lokalita	k1gFnSc1	lokalita
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Oleško	Oleška	k1gFnSc5	Oleška
(	(	kIx(	(
<g/>
též	též	k9	též
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Travčice	Travčice	k1gFnSc2	Travčice
<g/>
)	)	kIx)	)
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Litoměřice	Litoměřice	k1gInPc4	Litoměřice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
zaniklou	zaniklý	k2eAgFnSc4d1	zaniklá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
středoevropské	středoevropský	k2eAgFnPc1d1	středoevropská
lokality	lokalita	k1gFnPc1	lokalita
bývají	bývat	k5eAaImIp3nP	bývat
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
jemnozrnných	jemnozrnný	k2eAgFnPc6d1	jemnozrnná
a	a	k8xC	a
minerálně	minerálně	k6eAd1	minerálně
bohatých	bohatý	k2eAgFnPc6d1	bohatá
půdách	půda	k1gFnPc6	půda
v	v	k7c6	v
písečných	písečný	k2eAgInPc6d1	písečný
přesypech	přesyp	k1gInPc6	přesyp
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vytrvalý	vytrvalý	k2eAgInSc1d1	vytrvalý
druh	druh	k1gInSc1	druh
roste	růst	k5eAaImIp3nS	růst
roztroušeně	roztroušeně	k6eAd1	roztroušeně
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
až	až	k8xS	až
pahorkatinách	pahorkatina	k1gFnPc6	pahorkatina
na	na	k7c6	na
suchých	suchý	k2eAgNnPc6d1	suché
osluněných	osluněný	k2eAgNnPc6d1	osluněné
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
řídkých	řídký	k2eAgInPc6d1	řídký
borových	borový	k2eAgInPc6d1	borový
lesích	les	k1gInPc6	les
a	a	k8xC	a
vřesovištích	vřesoviště	k1gNnPc6	vřesoviště
<g/>
,	,	kIx,	,
v	v	k7c6	v
pískovnách	pískovna	k1gFnPc6	pískovna
<g/>
,	,	kIx,	,
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
cest	cesta	k1gFnPc2	cesta
i	i	k8xC	i
náspech	násep	k1gInPc6	násep
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vinohradech	vinohrad	k1gInPc6	vinohrad
aj.	aj.	kA	aj.
Je	být	k5eAaImIp3nS	být
vázán	vázat	k5eAaImNgInS	vázat
na	na	k7c4	na
mírně	mírně	k6eAd1	mírně
kyselé	kyselý	k2eAgInPc4d1	kyselý
až	až	k8xS	až
mírně	mírně	k6eAd1	mírně
zásadité	zásaditý	k2eAgFnPc1d1	zásaditá
půdy	půda	k1gFnPc1	půda
s	s	k7c7	s
často	často	k6eAd1	často
rozrušovaným	rozrušovaný	k2eAgInSc7d1	rozrušovaný
povrchem	povrch	k1gInSc7	povrch
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
chudý	chudý	k2eAgMnSc1d1	chudý
a	a	k8xC	a
nezapojený	zapojený	k2eNgInSc1d1	nezapojený
rostlinný	rostlinný	k2eAgInSc1d1	rostlinný
porost	porost	k1gInSc1	porost
<g/>
.	.	kIx.	.
</s>
<s>
Nevadí	vadit	k5eNaImIp3nS	vadit
mu	on	k3xPp3gMnSc3	on
extrémní	extrémní	k2eAgNnSc1d1	extrémní
sucho	sucho	k1gNnSc1	sucho
ani	ani	k8xC	ani
nevýživné	výživný	k2eNgFnPc1d1	nevýživná
půdy	půda	k1gFnPc1	půda
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
nesnáší	snášet	k5eNaImIp3nS	snášet
konkurenční	konkurenční	k2eAgFnPc4d1	konkurenční
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
hlavního	hlavní	k2eAgNnSc2d1	hlavní
rozšíření	rozšíření	k1gNnSc2	rozšíření
však	však	k9	však
rostlina	rostlina	k1gFnSc1	rostlina
není	být	k5eNaImIp3nS	být
vázaná	vázaný	k2eAgFnSc1d1	vázaná
jen	jen	k9	jen
na	na	k7c4	na
písčité	písčitý	k2eAgFnPc4d1	písčitá
duny	duna	k1gFnPc4	duna
jako	jako	k8xS	jako
ve	v	k7c4	v
středu	středa	k1gFnSc4	středa
Evropě	Evropa	k1gFnSc3	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roste	růst	k5eAaImIp3nS	růst
také	také	k9	také
na	na	k7c6	na
hlubokých	hluboký	k2eAgFnPc6d1	hluboká
černozemích	černozem	k1gFnPc6	černozem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
bylina	bylina	k1gFnSc1	bylina
dorůstající	dorůstající	k2eAgFnSc1d1	dorůstající
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
25	[number]	k4	25
až	až	k9	až
70	[number]	k4	70
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
z	z	k7c2	z
málo	málo	k6eAd1	málo
větveného	větvený	k2eAgInSc2d1	větvený
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
2	[number]	k4	2
až	až	k9	až
2,5	[number]	k4	2,5
m	m	kA	m
hluboko	hluboko	k6eAd1	hluboko
sahajícího	sahající	k2eAgInSc2d1	sahající
kůlovitého	kůlovitý	k2eAgInSc2d1	kůlovitý
kořene	kořen	k1gInSc2	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vodorovně	vodorovně	k6eAd1	vodorovně
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
mělce	mělce	k6eAd1	mělce
uložené	uložený	k2eAgInPc4d1	uložený
výběžkaté	výběžkatý	k2eAgInPc4d1	výběžkatý
kořeny	kořen	k1gInPc4	kořen
ze	z	k7c2	z
kterých	který	k3yIgNnPc2	který
pučí	pučet	k5eAaImIp3nP	pučet
listové	listový	k2eAgFnPc1d1	listová
růžice	růžice	k1gFnPc1	růžice
<g/>
.	.	kIx.	.
</s>
<s>
Přímé	přímý	k2eAgNnSc1d1	přímé
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
řídce	řídce	k6eAd1	řídce
větvené	větvený	k2eAgFnPc1d1	větvená
lodyhy	lodyha	k1gFnPc1	lodyha
jsou	být	k5eAaImIp3nP	být
podélně	podélně	k6eAd1	podélně
rýhované	rýhovaný	k2eAgFnPc1d1	rýhovaná
a	a	k8xC	a
řídce	řídce	k6eAd1	řídce
porůstají	porůstat	k5eAaImIp3nP	porůstat
chloupky	chloupek	k1gInPc1	chloupek
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgFnPc4d1	mladá
rostliny	rostlina	k1gFnPc4	rostlina
mají	mít	k5eAaImIp3nP	mít
listy	list	k1gInPc1	list
kopinaté	kopinatý	k2eAgInPc1d1	kopinatý
až	až	k6eAd1	až
čárkovité	čárkovitý	k2eAgInPc1d1	čárkovitý
<g/>
,	,	kIx,	,
celistvé	celistvý	k2eAgInPc1d1	celistvý
a	a	k8xC	a
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
podvinuté	podvinutý	k2eAgFnSc2d1	podvinutá
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
rostlin	rostlina	k1gFnPc2	rostlina
jsou	být	k5eAaImIp3nP	být
dlouze	dlouho	k6eAd1	dlouho
řapíkaté	řapíkatý	k2eAgInPc1d1	řapíkatý
listy	list	k1gInPc1	list
s	s	k7c7	s
okrouhlou	okrouhlý	k2eAgFnSc7d1	okrouhlá
peřenosečnou	peřenosečný	k2eAgFnSc7d1	peřenosečná
čepelí	čepel	k1gFnSc7	čepel
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mívá	mívat	k5eAaImIp3nS	mívat
pět	pět	k4xCc4	pět
až	až	k9	až
šest	šest	k4xCc4	šest
párů	pár	k1gInPc2	pár
úzce	úzko	k6eAd1	úzko
kopinatých	kopinatý	k2eAgInPc2d1	kopinatý
úkrojků	úkrojek	k1gInPc2	úkrojek
až	až	k9	až
40	[number]	k4	40
mm	mm	kA	mm
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
a	a	k8xC	a
4	[number]	k4	4
mm	mm	kA	mm
širokých	široký	k2eAgInPc2d1	široký
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
svrchní	svrchní	k2eAgFnSc6d1	svrchní
straně	strana	k1gFnSc6	strana
řídce	řídce	k6eAd1	řídce
pavučinatě	pavučinatě	k6eAd1	pavučinatě
chlupaté	chlupatý	k2eAgFnPc1d1	chlupatá
a	a	k8xC	a
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
hustě	hustě	k6eAd1	hustě
stříbřitě	stříbřitě	k6eAd1	stříbřitě
plstnaté	plstnatý	k2eAgNnSc1d1	plstnaté
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
lodyh	lodyha	k1gFnPc2	lodyha
nebo	nebo	k8xC	nebo
větví	větev	k1gFnPc2	větev
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
jednotlivě	jednotlivě	k6eAd1	jednotlivě
kulovité	kulovitý	k2eAgInPc1d1	kulovitý
květní	květní	k2eAgInPc1d1	květní
úbory	úbor	k1gInPc1	úbor
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Rostlina	rostlina	k1gFnSc1	rostlina
jich	on	k3xPp3gFnPc2	on
mívá	mívat	k5eAaImIp3nS	mívat
jeden	jeden	k4xCgInSc4	jeden
až	až	k8xS	až
dvanáct	dvanáct	k4xCc4	dvanáct
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
tvořeny	tvořen	k2eAgInPc1d1	tvořen
oboupohlavnými	oboupohlavný	k2eAgInPc7d1	oboupohlavný
pětičetnými	pětičetný	k2eAgInPc7d1	pětičetný
kvítky	kvítek	k1gInPc7	kvítek
s	s	k7c7	s
červenofialovou	červenofialový	k2eAgFnSc7d1	červenofialová
trubkovitou	trubkovitý	k2eAgFnSc7d1	trubkovitá
korunou	koruna	k1gFnSc7	koruna
s	s	k7c7	s
asi	asi	k9	asi
5	[number]	k4	5
mm	mm	kA	mm
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
čárkovitými	čárkovitý	k2eAgInPc7d1	čárkovitý
cípy	cíp	k1gInPc7	cíp
<g/>
.	.	kIx.	.
</s>
<s>
Střechovitý	střechovitý	k2eAgInSc1d1	střechovitý
zákrov	zákrov	k1gInSc1	zákrov
úboru	úbor	k1gInSc2	úbor
je	být	k5eAaImIp3nS	být
kulovitý	kulovitý	k2eAgMnSc1d1	kulovitý
<g/>
,	,	kIx,	,
víceřadý	víceřadý	k2eAgMnSc1d1	víceřadý
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
listeny	listen	k1gInPc1	listen
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
16	[number]	k4	16
až	až	k9	až
18	[number]	k4	18
mm	mm	kA	mm
jsou	být	k5eAaImIp3nP	být
kopinaté	kopinatý	k2eAgMnPc4d1	kopinatý
až	až	k9	až
čárkovité	čárkovitý	k2eAgNnSc1d1	čárkovité
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
špičaté	špičatý	k2eAgFnPc1d1	špičatá
a	a	k8xC	a
mívají	mívat	k5eAaImIp3nP	mívat
červenofialový	červenofialový	k2eAgInSc4d1	červenofialový
nádech	nádech	k1gInSc4	nádech
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
rozkvétají	rozkvétat	k5eAaImIp3nP	rozkvétat
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
nažky	nažka	k1gFnSc2	nažka
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
cizosprašné	cizosprašný	k2eAgFnPc1d1	cizosprašná
rostliny	rostlina	k1gFnPc1	rostlina
jsou	být	k5eAaImIp3nP	být
opylovány	opylovat	k5eAaImNgFnP	opylovat
běžnými	běžný	k2eAgFnPc7d1	běžná
druhy	druh	k1gInPc4	druh
hmyzu	hmyz	k1gInSc2	hmyz
z	z	k7c2	z
řádů	řád	k1gInPc2	řád
blanokřídlých	blanokřídlí	k1gMnPc2	blanokřídlí
<g/>
,	,	kIx,	,
dvoukřídlých	dvoukřídlí	k1gMnPc2	dvoukřídlí
a	a	k8xC	a
motýlů	motýl	k1gMnPc2	motýl
<g/>
.	.	kIx.	.
</s>
<s>
Dozrávající	dozrávající	k2eAgFnPc1d1	dozrávající
nažky	nažka	k1gFnPc1	nažka
vyžírají	vyžírat	k5eAaImIp3nP	vyžírat
larvy	larva	k1gFnPc4	larva
motýlů	motýl	k1gMnPc2	motýl
zavíječů	zavíječ	k1gInPc2	zavíječ
rodu	rod	k1gInSc3	rod
Dioryctria	Dioryctrium	k1gNnSc2	Dioryctrium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Sinokvět	Sinokvět	k1gInSc1	Sinokvět
chrpovitý	chrpovitý	k2eAgInSc1d1	chrpovitý
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
vegetativně	vegetativně	k6eAd1	vegetativně
(	(	kIx(	(
<g/>
postranními	postranní	k2eAgFnPc7d1	postranní
růžicemi	růžice	k1gFnPc7	růžice
rostoucími	rostoucí	k2eAgFnPc7d1	rostoucí
z	z	k7c2	z
podzemních	podzemní	k2eAgInPc2d1	podzemní
výběžků	výběžek	k1gInPc2	výběžek
<g/>
)	)	kIx)	)
a	a	k8xC	a
generativně	generativně	k6eAd1	generativně
(	(	kIx(	(
<g/>
semeny	semeno	k1gNnPc7	semeno
s	s	k7c7	s
hladkými	hladký	k2eAgFnPc7d1	hladká
nebo	nebo	k8xC	nebo
zbrázděnými	zbrázděný	k2eAgFnPc7d1	zbrázděná
čtyřhrannými	čtyřhranný	k2eAgFnPc7d1	čtyřhranná
nažkami	nažka	k1gFnPc7	nažka
s	s	k7c7	s
chmýrem	chmýr	k1gInSc7	chmýr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
lokalitách	lokalita	k1gFnPc6	lokalita
však	však	k9	však
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
nebyla	být	k5eNaImAgFnS	být
zjištěná	zjištěný	k2eAgFnSc1d1	zjištěná
nová	nový	k2eAgFnSc1d1	nová
rostlina	rostlina	k1gFnSc1	rostlina
vyrostlá	vyrostlý	k2eAgFnSc1d1	vyrostlá
ze	z	k7c2	z
semene	semeno	k1gNnSc2	semeno
<g/>
,	,	kIx,	,
druh	druh	k1gInSc1	druh
se	se	k3xPyFc4	se
na	na	k7c6	na
těchto	tento	k3xDgNnPc6	tento
stanovištích	stanoviště	k1gNnPc6	stanoviště
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
množí	množit	k5eAaImIp3nP	množit
jen	jen	k9	jen
vegetativně	vegetativně	k6eAd1	vegetativně
<g/>
.	.	kIx.	.
</s>
<s>
Rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
na	na	k7c6	na
krátké	krátký	k2eAgFnSc6d1	krátká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
rozrůstáním	rozrůstání	k1gNnSc7	rozrůstání
kořenových	kořenový	k2eAgInPc2d1	kořenový
výběžků	výběžek	k1gInPc2	výběžek
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
klony	klon	k1gInPc1	klon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
úborů	úbor	k1gInPc2	úbor
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
50	[number]	k4	50
až	až	k9	až
100	[number]	k4	100
nažek	nažka	k1gFnPc2	nažka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
neplodné	plodný	k2eNgInPc1d1	neplodný
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
embrya	embryo	k1gNnSc2	embryo
<g/>
.	.	kIx.	.
</s>
<s>
Hranaté	hranatý	k2eAgNnSc1d1	hranaté
semeno	semeno	k1gNnSc1	semeno
bývá	bývat	k5eAaImIp3nS	bývat
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
i	i	k8xC	i
široké	široký	k2eAgNnSc1d1	široké
okolo	okolo	k7c2	okolo
2	[number]	k4	2
mm	mm	kA	mm
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nS	vážit
průměrně	průměrně	k6eAd1	průměrně
4,3	[number]	k4	4,3
mg	mg	kA	mg
a	a	k8xC	a
chmýr	chmýr	k1gInSc1	chmýr
nažky	nažka	k1gFnSc2	nažka
nejčastěji	často	k6eAd3	často
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
délky	délka	k1gFnSc2	délka
12	[number]	k4	12
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
poměrně	poměrně	k6eAd1	poměrně
těžká	těžký	k2eAgNnPc1d1	těžké
semena	semeno	k1gNnPc1	semeno
bývají	bývat	k5eAaImIp3nP	bývat
větrem	vítr	k1gInSc7	vítr
zavátá	zavátý	k2eAgFnSc1d1	zavátá
jen	jen	k6eAd1	jen
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
nejvíce	nejvíce	k6eAd1	nejvíce
10	[number]	k4	10
m	m	kA	m
<g/>
,	,	kIx,	,
neudrží	udržet	k5eNaPmIp3nS	udržet
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
v	v	k7c6	v
srsti	srst	k1gFnSc6	srst
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
životnosti	životnost	k1gFnSc2	životnost
a	a	k8xC	a
proto	proto	k8xC	proto
jich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
nebývá	bývat	k5eNaImIp3nS	bývat
zásoba	zásoba	k1gFnSc1	zásoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plodná	plodný	k2eAgNnPc1d1	plodné
semena	semeno	k1gNnPc1	semeno
nejsou	být	k5eNaImIp3nP	být
dormantní	dormantní	k2eAgNnPc1d1	dormantní
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
klíčí	klíčit	k5eAaImIp3nS	klíčit
již	již	k6eAd1	již
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
častěji	často	k6eAd2	často
však	však	k9	však
až	až	k9	až
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
a	a	k8xC	a
dubnu	duben	k1gInSc6	duben
příštího	příští	k2eAgInSc2d1	příští
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Prvým	prvý	k4xOgInSc7	prvý
rokem	rok	k1gInSc7	rok
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
rostliny	rostlina	k1gFnPc4	rostlina
listovou	listový	k2eAgFnSc4d1	listová
růžici	růžice	k1gFnSc4	růžice
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
kořenové	kořenový	k2eAgFnSc2d1	kořenová
odnože	odnož	k1gFnSc2	odnož
s	s	k7c7	s
dceřinými	dceřin	k2eAgFnPc7d1	dceřina
růžicemi	růžice	k1gFnPc7	růžice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
propojeny	propojit	k5eAaPmNgInP	propojit
s	s	k7c7	s
mateřskou	mateřský	k2eAgFnSc7d1	mateřská
rostlinou	rostlina	k1gFnSc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgNnSc1d1	zimní
období	období	k1gNnSc1	období
přežívá	přežívat	k5eAaImIp3nS	přežívat
pouze	pouze	k6eAd1	pouze
podzemní	podzemní	k2eAgFnSc4d1	podzemní
část	část	k1gFnSc4	část
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
rostlina	rostlina	k1gFnSc1	rostlina
obrůstá	obrůstat	k5eAaImIp3nS	obrůstat
z	z	k7c2	z
obnovovacích	obnovovací	k2eAgInPc2d1	obnovovací
pupenů	pupen	k1gInPc2	pupen
umístěných	umístěný	k2eAgInPc2d1	umístěný
těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Ohrožení	ohrožení	k1gNnSc1	ohrožení
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
spočívá	spočívat	k5eAaImIp3nS	spočívat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
ubývání	ubývání	k1gNnSc6	ubývání
vhodných	vhodný	k2eAgFnPc2d1	vhodná
lokalit	lokalita	k1gFnPc2	lokalita
a	a	k8xC	a
v	v	k7c6	v
nevhodném	vhodný	k2eNgInSc6d1	nevhodný
hospodařením	hospodaření	k1gNnSc7	hospodaření
na	na	k7c6	na
stávajících	stávající	k2eAgFnPc6d1	stávající
<g/>
.	.	kIx.	.
</s>
