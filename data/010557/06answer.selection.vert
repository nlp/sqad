<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusila	pokusit	k5eAaPmAgFnS	pokusit
přeplavat	přeplavat	k5eAaPmF	přeplavat
Lamanšský	lamanšský	k2eAgInSc4d1	lamanšský
průliv	průliv	k1gInSc4	průliv
<g/>
,	,	kIx,	,
zdařil	zdařit	k5eAaPmAgInS	zdařit
se	se	k3xPyFc4	se
až	až	k9	až
druhý	druhý	k4xOgInSc1	druhý
pokus	pokus	k1gInSc1	pokus
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
z	z	k7c2	z
mysu	mys	k1gInSc2	mys
Gris	Gris	k1gInSc1	Gris
Nez	Nez	k1gFnSc2	Nez
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
anglické	anglický	k2eAgNnSc4d1	anglické
pobřeží	pobřeží	k1gNnSc4	pobřeží
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Kingsdown	Kingsdowna	k1gFnPc2	Kingsdowna
<g/>
.	.	kIx.	.
</s>
