<p>
<s>
Gertrude	Gertrude	k6eAd1	Gertrude
Caroline	Carolin	k1gInSc5	Carolin
Ederleová	Ederleová	k1gFnSc1	Ederleová
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1905	[number]	k4	1905
New	New	k1gFnPc2	New
York	York	k1gInSc4	York
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2003	[number]	k4	2003
Wyckoff	Wyckoff	k1gInSc1	Wyckoff
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
plavkyně	plavkyně	k1gFnSc1	plavkyně
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
překonala	překonat	k5eAaPmAgFnS	překonat
Lamanšský	lamanšský	k2eAgInSc4d1	lamanšský
průliv	průliv	k1gInSc4	průliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
německých	německý	k2eAgMnPc2d1	německý
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
otec	otec	k1gMnSc1	otec
provozoval	provozovat	k5eAaImAgMnS	provozovat
řeznictví	řeznictví	k1gNnSc1	řeznictví
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šestnácti	šestnáct	k4xCc6	šestnáct
letech	léto	k1gNnPc6	léto
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
Women	Womna	k1gFnPc2	Womna
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Swimming	Swimming	k1gInSc1	Swimming
Association	Association	k1gInSc4	Association
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
trénoval	trénovat	k5eAaImAgMnS	trénovat
Louis	Louis	k1gMnSc1	Louis
Handley	Handlea	k1gFnSc2	Handlea
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
závodní	závodní	k2eAgFnSc2d1	závodní
kariéry	kariéra	k1gFnSc2	kariéra
překonala	překonat	k5eAaPmAgFnS	překonat
devět	devět	k4xCc4	devět
světových	světový	k2eAgInPc2d1	světový
rekordů	rekord	k1gInPc2	rekord
<g/>
,	,	kIx,	,
startovala	startovat	k5eAaBmAgFnS	startovat
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získala	získat	k5eAaPmAgFnS	získat
bronzové	bronzový	k2eAgFnPc4d1	bronzová
medaile	medaile	k1gFnPc4	medaile
v	v	k7c6	v
individuálních	individuální	k2eAgInPc6d1	individuální
závodech	závod	k1gInPc6	závod
na	na	k7c4	na
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
volným	volný	k2eAgInSc7d1	volný
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
členkou	členka	k1gFnSc7	členka
vítězné	vítězný	k2eAgFnSc2d1	vítězná
štafety	štafeta	k1gFnSc2	štafeta
na	na	k7c4	na
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
100	[number]	k4	100
m.	m.	k?	m.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
uplavala	uplavat	k5eAaPmAgFnS	uplavat
22	[number]	k4	22
mil	míle	k1gFnPc2	míle
z	z	k7c2	z
Battery	Batter	k1gInPc7	Batter
Parku	park	k1gInSc2	park
na	na	k7c4	na
Sandy	Sanda	k1gFnPc4	Sanda
Hook	Hooka	k1gFnPc2	Hooka
za	za	k7c2	za
rekordních	rekordní	k2eAgFnPc2d1	rekordní
sedm	sedm	k4xCc1	sedm
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
jedenáct	jedenáct	k4xCc1	jedenáct
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusila	pokusit	k5eAaPmAgFnS	pokusit
přeplavat	přeplavat	k5eAaPmF	přeplavat
Lamanšský	lamanšský	k2eAgInSc4d1	lamanšský
průliv	průliv	k1gInSc4	průliv
<g/>
,	,	kIx,	,
zdařil	zdařit	k5eAaPmAgInS	zdařit
se	se	k3xPyFc4	se
až	až	k9	až
druhý	druhý	k4xOgInSc1	druhý
pokus	pokus	k1gInSc1	pokus
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
z	z	k7c2	z
mysu	mys	k1gInSc2	mys
Gris	Gris	k1gInSc1	Gris
Nez	Nez	k1gFnSc2	Nez
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
anglické	anglický	k2eAgNnSc4d1	anglické
pobřeží	pobřeží	k1gNnSc4	pobřeží
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Kingsdown	Kingsdowna	k1gFnPc2	Kingsdowna
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Kanál	kanál	k1gInSc4	kanál
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
přitom	přitom	k6eAd1	přitom
rekordní	rekordní	k2eAgInSc4d1	rekordní
čas	čas	k1gInSc4	čas
14	[number]	k4	14
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
39	[number]	k4	39
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úspěch	úspěch	k1gInSc1	úspěch
jí	on	k3xPp3gFnSc3	on
vynesl	vynést	k5eAaPmAgInS	vynést
přezdívku	přezdívka	k1gFnSc4	přezdívka
Queen	Queen	k2eAgInSc1d1	Queen
of	of	k?	of
the	the	k?	the
Waves	Waves	k1gInSc1	Waves
(	(	kIx(	(
<g/>
Královna	královna	k1gFnSc1	královna
vln	vlna	k1gFnPc2	vlna
<g/>
)	)	kIx)	)
a	a	k8xC	a
přijetí	přijetí	k1gNnSc1	přijetí
u	u	k7c2	u
prezidenta	prezident	k1gMnSc2	prezident
Calvina	Calvin	k2eAgInSc2d1	Calvin
Coolidge	Coolidg	k1gInSc2	Coolidg
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
popularitě	popularita	k1gFnSc3	popularita
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
profesionálně	profesionálně	k6eAd1	profesionálně
v	v	k7c6	v
plaveckých	plavecký	k2eAgFnPc6d1	plavecká
exhibicích	exhibice	k1gFnPc6	exhibice
a	a	k8xC	a
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
němém	němý	k2eAgInSc6d1	němý
filmu	film	k1gInSc6	film
Swim	Swi	k1gNnSc7	Swi
Girl	girl	k1gFnSc2	girl
<g/>
,	,	kIx,	,
Swim	Swima	k1gFnPc2	Swima
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
působila	působit	k5eAaImAgFnS	působit
jako	jako	k9	jako
učitelka	učitelka	k1gFnSc1	učitelka
plavání	plavání	k1gNnSc2	plavání
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
pro	pro	k7c4	pro
nedoslýchavé	nedoslýchavý	k2eAgInPc4d1	nedoslýchavý
(	(	kIx(	(
<g/>
sama	sám	k3xTgFnSc1	sám
byla	být	k5eAaImAgFnS	být
sluchově	sluchově	k6eAd1	sluchově
postižená	postižený	k2eAgFnSc1d1	postižená
po	po	k7c6	po
prodělaných	prodělaný	k2eAgFnPc6d1	prodělaná
spalničkách	spalničky	k1gFnPc6	spalničky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žila	žít	k5eAaImAgFnS	žít
neprovdána	provdat	k5eNaPmNgFnS	provdat
a	a	k8xC	a
dožila	dožít	k5eAaPmAgFnS	dožít
se	s	k7c7	s
98	[number]	k4	98
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gertrude	Gertrud	k1gInSc5	Gertrud
Ederleová	Ederleová	k1gFnSc1	Ederleová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Biography	Biographa	k1gFnPc1	Biographa
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Mashable	Mashable	k6eAd1	Mashable
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Sports	Sports	k6eAd1	Sports
Reference	reference	k1gFnPc1	reference
</s>
</p>
