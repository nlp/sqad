<s>
Alexandr	Alexandr	k1gMnSc1
Fomič	Fomič	k1gMnSc1
Veltman	Veltman	k1gMnSc1
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Fomič	Fomič	k1gMnSc1
Veltman	Veltman	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
červencejul	červencejout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
/	/	kIx~
20	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1800	#num#	k4
<g/>
greg	grega	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Petrohrad	Petrohrad	k1gInSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
lednajul	lednajout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
/	/	kIx~
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1870	#num#	k4
<g/>
greg	grega	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
69	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Moskva	Moskva	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
etnograf	etnograf	k1gMnSc1
<g/>
,	,	kIx,
archeolog	archeolog	k1gMnSc1
<g/>
,	,	kIx,
básník	básník	k1gMnSc1
a	a	k8xC
prozaik	prozaik	k1gMnSc1
Stát	stát	k1gInSc1
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
Žánr	žánr	k1gInSc1
</s>
<s>
bizarní	bizarní	k2eAgFnPc1d1
<g/>
,	,	kIx,
mravoličné	mravoličný	k2eAgFnPc1d1
<g/>
,	,	kIx,
fantastické	fantastický	k2eAgFnPc1d1
a	a	k8xC
dobrodružné	dobrodružný	k2eAgFnPc1d1
prózy	próza	k1gFnPc1
Literární	literární	k2eAgFnPc1d1
hnutí	hnutí	k1gNnPc1
</s>
<s>
Romantismus	romantismus	k1gInSc4
Významná	významný	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
</s>
<s>
Lunatik	lunatik	k1gMnSc1
<g/>
,	,	kIx,
Salomea	Salomea	k1gMnSc1
Ocenění	ocenění	k1gNnSc2
</s>
<s>
Řád	řád	k1gInSc1
sv.	sv.	kA
Anny	Anna	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
třídyŘád	třídyŘáda	k1gFnPc2
sv.	sv.	kA
Vladimíra	Vladimíra	k1gFnSc1
4	#num#	k4
<g/>
.	.	kIx.
třídyŘád	třídyŘáda	k1gFnPc2
sv.	sv.	kA
Stanislava	Stanislava	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
třídyŘád	třídyŘáda	k1gFnPc2
sv.	sv.	kA
Vladimíra	Vladimíra	k1gFnSc1
3	#num#	k4
<g/>
.	.	kIx.
třídyŘád	třídyŘáda	k1gFnPc2
sv.	sv.	kA
Stanislava	Stanislava	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Anna	Anna	k1gFnSc1
Pavlovna	Pavlovna	k1gFnSc1
Wejdelová	Wejdelová	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Fomič	Fomič	k1gMnSc1
Veltman	Veltman	k1gMnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
А	А	k?
Ф	Ф	k?
В	В	k?
<g/>
,	,	kIx,
8	#num#	k4
<g/>
.	.	kIx.
červencejul	červencejout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
/	/	kIx~
20	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1800	#num#	k4
<g/>
greg	grega	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Petrohrad	Petrohrad	k1gInSc1
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
lednajul	lednajout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
/	/	kIx~
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1870	#num#	k4
<g/>
greg	grega	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
ruský	ruský	k2eAgMnSc1d1
romantický	romantický	k2eAgMnSc1d1
básník	básník	k1gMnSc1
a	a	k8xC
prozaik	prozaik	k1gMnSc1
švédského	švédský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Veltman	Veltman	k1gMnSc1
roku	rok	k1gInSc2
1871	#num#	k4
</s>
<s>
První	první	k4xOgNnSc1
vydání	vydání	k1gNnSc1
románu	román	k1gInSc2
Lunatik	lunatik	k1gMnSc1
z	z	k7c2
roku	rok	k1gInSc2
1834	#num#	k4
</s>
<s>
První	první	k4xOgNnSc1
vydání	vydání	k1gNnSc1
románu	román	k1gInSc2
Salomea	Salomea	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1848	#num#	k4
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
chudé	chudý	k2eAgFnSc2d1
šlechtické	šlechtický	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
švédského	švédský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1811	#num#	k4
začal	začít	k5eAaPmAgInS
studovat	studovat	k5eAaImF
na	na	k7c6
Šlechtické	šlechtický	k2eAgFnSc6d1
internátní	internátní	k2eAgFnSc6d1
škole	škola	k1gFnSc6
při	při	k7c6
Moskevské	moskevský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velký	velký	k2eAgInSc1d1
vliv	vliv	k1gInSc1
na	na	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
duchovní	duchovní	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
mělo	mít	k5eAaImAgNnS
obsazení	obsazení	k1gNnSc1
Moskvy	Moskva	k1gFnSc2
Napoleonovou	Napoleonův	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
a	a	k8xC
následný	následný	k2eAgInSc1d1
požár	požár	k1gInSc1
města	město	k1gNnSc2
roku	rok	k1gInSc2
1812	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1816	#num#	k4
začal	začít	k5eAaPmAgInS
navštěvovat	navštěvovat	k5eAaImF
moskevskou	moskevský	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
školu	škola	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
jeho	jeho	k3xOp3gFnSc1
rodina	rodina	k1gFnSc1
zůstala	zůstat	k5eAaPmAgFnS
téměř	téměř	k6eAd1
bez	bez	k7c2
prostředků	prostředek	k1gInPc2
<g/>
,	,	kIx,
vyučoval	vyučovat	k5eAaImAgMnS
hru	hra	k1gFnSc4
na	na	k7c4
housle	housle	k1gFnPc4
a	a	k8xC
vydal	vydat	k5eAaPmAgInS
učebnici	učebnice	k1gFnSc4
základů	základ	k1gInPc2
aritmetiky	aritmetika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
1817	#num#	k4
ukončil	ukončit	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
vzdělání	vzdělání	k1gNnSc4
a	a	k8xC
v	v	k7c6
hodnosti	hodnost	k1gFnSc6
praporčíka	praporčík	k1gMnSc2
byl	být	k5eAaImAgInS
odvelen	odvelet	k5eAaPmNgInS
do	do	k7c2
Besarábie	Besarábie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
sloužil	sloužit	k5eAaImAgMnS
ve	v	k7c6
vojenské	vojenský	k2eAgFnSc6d1
topografické	topografický	k2eAgFnSc6d1
komisi	komise	k1gFnSc6
a	a	k8xC
kde	kde	k6eAd1
se	se	k3xPyFc4
seznámil	seznámit	k5eAaPmAgMnS
s	s	k7c7
Puškinem	Puškin	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Války	válka	k1gFnPc1
s	s	k7c7
Tureckem	Turecko	k1gNnSc7
v	v	k7c6
letech	let	k1gInPc6
1828	#num#	k4
<g/>
–	–	k?
<g/>
1829	#num#	k4
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
jako	jako	k9
náčelník	náčelník	k1gMnSc1
historiografického	historiografický	k2eAgNnSc2d1
oddělení	oddělení	k1gNnSc2
generálního	generální	k2eAgInSc2d1
štábu	štáb	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
svou	svůj	k3xOyFgFnSc4
službu	služba	k1gFnSc4
ve	v	k7c6
vojsku	vojsko	k1gNnSc6
obdržel	obdržet	k5eAaPmAgMnS
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
vyznamenání	vyznamenání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1831	#num#	k4
odešel	odejít	k5eAaPmAgMnS
v	v	k7c6
hodnosti	hodnost	k1gFnSc6
podplukovníka	podplukovník	k1gMnSc2
z	z	k7c2
důvodu	důvod	k1gInSc2
nemoci	nemoc	k1gFnSc2
do	do	k7c2
výslužby	výslužba	k1gFnSc2
a	a	k8xC
usadil	usadit	k5eAaPmAgInS
se	se	k3xPyFc4
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1832	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Annou	Anna	k1gFnSc7
Pavlovnou	Pavlovna	k1gFnSc7
Wejdelovou	Wejdelová	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začal	začít	k5eAaPmAgMnS
se	se	k3xPyFc4
zabývat	zabývat	k5eAaImF
etnografií	etnografie	k1gFnSc7
a	a	k8xC
archeologií	archeologie	k1gFnSc7
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
členem	člen	k1gInSc7
různých	různý	k2eAgInPc2d1
uměleckých	umělecký	k2eAgInPc2d1
a	a	k8xC
vědeckých	vědecký	k2eAgInPc2d1
spolků	spolek	k1gInPc2
(	(	kIx(
<g/>
Společnost	společnost	k1gFnSc1
milovníků	milovník	k1gMnPc2
ruské	ruský	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
při	při	k7c6
Moskevské	moskevský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
,	,	kIx,
Moskevská	moskevský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
ruských	ruský	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1842	#num#	k4
až	až	k9
do	do	k7c2
smrti	smrt	k1gFnSc2
pracoval	pracovat	k5eAaImAgMnS
nejprve	nejprve	k6eAd1
jako	jako	k8xC,k8xS
asistent	asistent	k1gMnSc1
ředitele	ředitel	k1gMnSc2
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1852	#num#	k4
jako	jako	k8xC,k8xS
ředitel	ředitel	k1gMnSc1
muzea	muzeum	k1gNnSc2
ve	v	k7c6
Zbrojnici	zbrojnice	k1gFnSc6
moskevského	moskevský	k2eAgInSc2d1
Kremlu	Kreml	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1845	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
redaktorů	redaktor	k1gMnPc2
encyklopedie	encyklopedie	k1gFnSc2
Starožitnosti	starožitnost	k1gFnSc2
ruského	ruský	k2eAgInSc2d1
státu	stát	k1gInSc2
(	(	kIx(
<g/>
1849	#num#	k4
<g/>
–	–	k?
<g/>
1853	#num#	k4
<g/>
,	,	kIx,
Д	Д	k?
Р	Р	k?
г	г	k?
<g/>
)	)	kIx)
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
autorem	autor	k1gMnSc7
textu	text	k1gInSc2
tří	tři	k4xCgFnPc2
z	z	k7c2
jejich	jejich	k3xOp3gNnPc2
šesti	šest	k4xCc2
sekcí	sekce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1854	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
členem-korespondentem	členem-korespondent	k1gMnSc7
Ruské	ruský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1869	#num#	k4
členem	člen	k1gMnSc7
Ruské	ruský	k2eAgFnSc2d1
archeologické	archeologický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Literární	literární	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
zahájil	zahájit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1828	#num#	k4
jako	jako	k8xC,k8xS
básník	básník	k1gMnSc1
exotickými	exotický	k2eAgFnPc7d1
epickými	epický	k2eAgFnPc7d1
básněmi	báseň	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úspěch	úspěch	k1gInSc4
mu	on	k3xPp3gMnSc3
přinesl	přinést	k5eAaPmAgMnS
až	až	k6eAd1
třídílný	třídílný	k2eAgInSc4d1
román	román	k1gInSc4
Poutník	poutník	k1gMnSc1
(	(	kIx(
<g/>
С	С	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dokončený	dokončený	k2eAgInSc1d1
roku	rok	k1gInSc2
1832	#num#	k4
<g/>
,	,	kIx,
s	s	k7c7
rozvolněnou	rozvolněný	k2eAgFnSc7d1
kompozicí	kompozice	k1gFnSc7
po	po	k7c6
vzoru	vzor	k1gInSc6
Laurence	Laurenec	k1gMnSc2
Sterneho	Sterne	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
dílo	dílo	k1gNnSc1
tvoří	tvořit	k5eAaImIp3nS
bizarní	bizarní	k2eAgFnPc4d1
<g/>
,	,	kIx,
mravoličné	mravoličný	k2eAgFnPc4d1
i	i	k8xC
dobrodružné	dobrodružný	k2eAgFnPc4d1
prózy	próza	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc2
uplatnil	uplatnit	k5eAaPmAgMnS
poetiku	poetika	k1gFnSc4
<g/>
,	,	kIx,
založenou	založený	k2eAgFnSc4d1
na	na	k7c6
groteskním	groteskní	k2eAgNnSc6d1
míšení	míšení	k1gNnSc6
fantastických	fantastický	k2eAgInPc2d1
a	a	k8xC
reálných	reálný	k2eAgInPc2d1
motivů	motiv	k1gInPc2
a	a	k8xC
na	na	k7c6
jazyku	jazyk	k1gInSc6
stylizovaném	stylizovaný	k2eAgInSc6d1
až	až	k6eAd1
k	k	k7c3
ornamentálnosti	ornamentálnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývá	bývat	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
předchůdce	předchůdce	k1gMnSc4
Dostojevského	Dostojevský	k2eAgMnSc4d1
a	a	k8xC
za	za	k7c4
jednoho	jeden	k4xCgMnSc4
ze	z	k7c2
zakladatelů	zakladatel	k1gMnPc2
fantastického	fantastický	k2eAgInSc2d1
žánru	žánr	k1gInSc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pochován	pochován	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
na	na	k7c6
hřbitově	hřbitov	k1gInSc6
Novo-Alexejevského	Novo-Alexejevský	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Básně	báseň	k1gFnPc1
</s>
<s>
Čekání	čekání	k1gNnSc1
(	(	kIx(
<g/>
1828	#num#	k4
<g/>
,	,	kIx,
О	О	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Uprchlík	uprchlík	k1gMnSc1
(	(	kIx(
<g/>
1831	#num#	k4
<g/>
,	,	kIx,
Б	Б	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Muromské	Muromský	k2eAgInPc1d1
lesy	les	k1gInPc1
(	(	kIx(
<g/>
1831	#num#	k4
<g/>
,	,	kIx,
М	М	k?
л	л	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mohamed	Mohamed	k1gMnSc1
(	(	kIx(
<g/>
1849	#num#	k4
<g/>
,	,	kIx,
М	М	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Próza	próza	k1gFnSc1
</s>
<s>
Poutník	poutník	k1gMnSc1
(	(	kIx(
<g/>
1830	#num#	k4
<g/>
-	-	kIx~
<g/>
1831	#num#	k4
<g/>
,	,	kIx,
С	С	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
třídílný	třídílný	k2eAgInSc4d1
román	román	k1gInSc4
obsahující	obsahující	k2eAgNnSc4d1
vylíčení	vylíčení	k1gNnSc4
fiktivní	fiktivní	k2eAgFnSc2d1
autorovy	autorův	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
putování	putování	k1gNnSc4
po	po	k7c6
mapě	mapa	k1gFnSc6
<g/>
,	,	kIx,
prolínající	prolínající	k2eAgMnSc1d1
se	se	k3xPyFc4
s	s	k7c7
putováním	putování	k1gNnSc7
reálnou	reálný	k2eAgFnSc4d1
krajinou	krajina	k1gFnSc7
principem	princip	k1gInSc7
bizarních	bizarní	k2eAgInPc2d1
přechodů	přechod	k1gInPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
propojení	propojení	k1gNnSc1
mikrosvěta	mikrosvět	k1gInSc2
všedních	všední	k2eAgFnPc2d1
událostií	událostie	k1gFnPc2
s	s	k7c7
makrosvětem	makrosvět	k1gInSc7
událostí	událost	k1gFnPc2
světových	světový	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Román	román	k1gInSc1
má	mít	k5eAaImIp3nS
rozvolněnou	rozvolněný	k2eAgFnSc7d1
kompozicí	kompozice	k1gFnSc7
po	po	k7c6
vzoru	vzor	k1gInSc6
Laurence	Laurenec	k1gMnSc2
Sterneho	Sterne	k1gMnSc2
(	(	kIx(
<g/>
přerušování	přerušování	k1gNnSc1
proudu	proud	k1gInSc2
vyprávění	vyprávění	k1gNnSc2
<g/>
.	.	kIx.
disonance	disonance	k1gFnSc1
kompozičních	kompoziční	k2eAgInPc2d1
elementů	element	k1gInPc2
<g/>
,	,	kIx,
hra	hra	k1gFnSc1
se	s	k7c7
slovy	slovo	k1gNnPc7
atp.	atp.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rok	rok	k1gInSc1
MMMCDXLVIII	MMMCDXLVIII	kA
<g/>
:	:	kIx,
Rukopis	rukopis	k1gInSc1
Martina	Martina	k1gFnSc1
Zadeky	Zadek	k1gInPc1
(	(	kIx(
<g/>
1833	#num#	k4
<g/>
,	,	kIx,
MMMCDXLVIII	MMMCDXLVIII	kA
г	г	k?
<g/>
:	:	kIx,
Р	Р	k?
М	М	k?
З	З	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
utopický	utopický	k2eAgInSc4d1
román	román	k1gInSc4
<g/>
,	,	kIx,
odehrávající	odehrávající	k2eAgInSc4d1
se	se	k3xPyFc4
v	v	k7c6
ideálním	ideální	k2eAgInSc6d1
státě	stát	k1gInSc6
na	na	k7c6
Balkánském	balkánský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Trilogie	trilogie	k1gFnSc1
ze	z	k7c2
staré	starý	k2eAgFnSc2d1
Rusi	Rus	k1gFnSc2
(	(	kIx(
<g/>
Т	Т	k?
о	о	k?
Д	Д	k?
Р	Р	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
pseudohistorické	pseudohistorický	k2eAgInPc4d1
romány	román	k1gInPc4
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
idealizoval	idealizovat	k5eAaBmAgInS
patriarchální	patriarchální	k2eAgInSc1d1
středověk	středověk	k1gInSc1
v	v	k7c6
duchu	duch	k1gMnSc6
slavjanofilství	slavjanofilství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Kostěj	Kostět	k5eAaImRp2nS,k5eAaPmRp2nS
nesmrtelný	nesmrtelný	k1gMnSc5
(	(	kIx(
<g/>
1833	#num#	k4
<g/>
,	,	kIx,
К	К	k?
б	б	k?
<g/>
,	,	kIx,
б	б	k?
с	с	k?
в	в	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bylina	bylina	k1gFnSc1
ze	z	k7c2
staré	starý	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Svjatoslavič	Svjatoslavič	k1gMnSc1
<g/>
,	,	kIx,
ďáblův	ďáblův	k2eAgMnSc1d1
schovanec	schovanec	k1gMnSc1
(	(	kIx(
<g/>
1835	#num#	k4
<g/>
,	,	kIx,
С	С	k?
<g/>
,	,	kIx,
в	в	k?
п	п	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
román	román	k1gInSc1
má	mít	k5eAaImIp3nS
podtitul	podtitul	k1gInSc1
zázrak	zázrak	k1gInSc1
z	z	k7c2
doby	doba	k1gFnSc2
Vladímíra	Vladímír	k1gMnSc2
Jasného	jasný	k2eAgNnSc2d1
slunéčka	slunéčko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Rajna	Rajna	k1gFnSc1
<g/>
,	,	kIx,
bulharská	bulharský	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
(	(	kIx(
<g/>
1843	#num#	k4
<g/>
,	,	kIx,
Р	Р	k?
<g/>
,	,	kIx,
к	к	k?
Б	Б	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
román	román	k1gInSc4
o	o	k7c6
dceři	dcera	k1gFnSc6
bulharského	bulharský	k2eAgMnSc2d1
cara	car	k1gMnSc2
Petra	Petr	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Lunatik	lunatik	k1gMnSc1
<g/>
:	:	kIx,
Případ	případ	k1gInSc1
(	(	kIx(
<g/>
1834	#num#	k4
<g/>
,	,	kIx,
Л	Л	k?
<g/>
:	:	kIx,
С	С	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgMnSc7d1
hrdinou	hrdina	k1gMnSc7
románu	román	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
děj	děj	k1gInSc1
osciluje	oscilovat	k5eAaImIp3nS
mezi	mezi	k7c7
snem	sen	k1gInSc7
a	a	k8xC
realitou	realita	k1gFnSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
mladý	mladý	k2eAgMnSc1d1
náměsíčný	náměsíčný	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
v	v	k7c6
hořící	hořící	k2eAgFnSc6d1
Moskvě	Moskva	k1gFnSc6
roku	rok	k1gInSc2
1812	#num#	k4
prožívá	prožívat	k5eAaImIp3nS
fantastická	fantastický	k2eAgNnPc4d1
dobrodružství	dobrodružství	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Předkové	předek	k1gMnPc1
Kalimerose	Kalimerosa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alexandr	Alexandr	k1gMnSc1
Filipovič	Filipovič	k1gMnSc1
Makedoský	Makedoský	k1gMnSc1
(	(	kIx(
<g/>
1835	#num#	k4
<g/>
,	,	kIx,
П	П	k?
К	К	k?
<g/>
.	.	kIx.
А	А	k?
Ф	Ф	k?
М	М	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
první	první	k4xOgInSc4
ruský	ruský	k2eAgInSc4d1
vědeckofantastický	vědeckofantastický	k2eAgInSc4d1
román	román	k1gInSc4
s	s	k7c7
popisem	popis	k1gInSc7
cestování	cestování	k1gNnSc2
v	v	k7c6
čase	čas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Erotida	Erotida	k1gFnSc1
(	(	kIx(
<g/>
1835	#num#	k4
<g/>
,	,	kIx,
Э	Э	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
novela	novela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Zuřivý	zuřivý	k2eAgInSc1d1
Rolamd	Rolamd	k1gInSc1
(	(	kIx(
<g/>
1835	#num#	k4
<g/>
,	,	kIx,
Н	Н	k?
Р	Р	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
povídka	povídka	k1gFnSc1
prvně	prvně	k?
vydána	vydat	k5eAaPmNgFnS
pod	pod	k7c7
názvem	název	k1gInSc7
Provinční	provinční	k2eAgMnPc1d1
herci	herec	k1gMnPc1
(	(	kIx(
<g/>
П	П	k?
а	а	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Alenka	Alenka	k1gFnSc1
(	(	kIx(
<g/>
1836	#num#	k4
<g/>
,	,	kIx,
А	А	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
povídka	povídka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Povídky	povídka	k1gFnPc1
(	(	kIx(
<g/>
1836	#num#	k4
<g/>
,	,	kIx,
П	П	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sbírka	sbírka	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
povídky	povídka	k1gFnSc2
a	a	k8xC
novely	novela	k1gFnSc2
Erotida	Erotida	k1gFnSc1
<g/>
,	,	kIx,
Zuřivý	zuřivý	k2eAgInSc1d1
Roland	Roland	k1gInSc1
a	a	k8xC
Alenka	Alenka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Srdce	srdce	k1gNnSc1
a	a	k8xC
dumka	dumka	k1gFnSc1
(	(	kIx(
<g/>
1838	#num#	k4
<g/>
,	,	kIx,
С	С	k?
и	и	k?
д	д	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
román	román	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kerém	keré	k1gNnSc6
se	se	k3xPyFc4
prolínají	prolínat	k5eAaImIp3nP
fantazijní	fantazijní	k2eAgInPc1d1
pohádkové	pohádkový	k2eAgInPc1d1
motivy	motiv	k1gInPc1
se	s	k7c7
scénami	scéna	k1gFnPc7
ze	z	k7c2
soudobého	soudobý	k2eAgInSc2d1
moskevského	moskevský	k2eAgInSc2d1
života	život	k1gInSc2
a	a	k8xC
jehož	jehož	k3xOyRp3gFnSc1
fabule	fabule	k1gFnSc1
je	být	k5eAaImIp3nS
založena	založit	k5eAaPmNgFnS
na	na	k7c4
rozdvojení	rozdvojení	k1gNnSc4
hlavní	hlavní	k2eAgFnSc2d1
hrdinky	hrdinka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Generál	generál	k1gMnSc1
Kalomeros	Kalomerosa	k1gFnPc2
(	(	kIx(
<g/>
1840	#num#	k4
<g/>
,	,	kIx,
Г	Г	k?
К	К	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
román	román	k1gInSc4
ze	z	k7c2
žánru	žánr	k1gInSc2
alternativní	alternativní	k2eAgFnSc2d1
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Povídky	povídka	k1gFnPc1
A.	A.	kA
Veltmana	Veltman	k1gMnSc4
(	(	kIx(
<g/>
1843	#num#	k4
<g/>
,	,	kIx,
П	П	k?
А	А	k?
В	В	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sbírka	sbírka	k1gFnSc1
povídek	povídka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Zevloun	zevloun	k1gMnSc1
<g/>
,	,	kIx,
neboli	neboli	k8xC
Proměny	proměna	k1gFnPc1
(	(	kIx(
<g/>
1845	#num#	k4
<g/>
,	,	kIx,
Н	Н	k?
Е	Е	k?
<g/>
,	,	kIx,
и	и	k?
П	П	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
román	román	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
se	se	k3xPyFc4
současný	současný	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
událostí	událost	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
1812	#num#	k4
<g/>
,	,	kIx,
pohybuje	pohybovat	k5eAaImIp3nS
ve	v	k7c6
fantastickém	fantastický	k2eAgNnSc6d1
pohádkovém	pohádkový	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
a	a	k8xC
zažívá	zažívat	k5eAaImIp3nS
neustálé	neustálý	k2eAgFnPc4d1
proměny	proměna	k1gFnPc4
(	(	kIx(
<g/>
stane	stanout	k5eAaPmIp3nS
se	se	k3xPyFc4
například	například	k6eAd1
francouzským	francouzský	k2eAgInSc7d1
genrálem	genrál	k1gInSc7
<g/>
,	,	kIx,
hned	hned	k6eAd1
zase	zase	k9
bohatým	bohatý	k2eAgMnSc7d1
dědicem	dědic	k1gMnSc7
<g/>
,	,	kIx,
pak	pak	k6eAd1
zase	zase	k9
statkářem	statkář	k1gMnSc7
a	a	k8xC
vzápětí	vzápětí	k6eAd1
šaškem	šašek	k1gMnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dobrodružství	dobrodružství	k1gNnPc1
vylovená	vylovený	k2eAgNnPc1d1
z	z	k7c2
moře	moře	k1gNnSc2
života	život	k1gInSc2
(	(	kIx(
<g/>
П	П	k?
п	п	k?
и	и	k?
м	м	k?
ж	ж	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čtyřdílný	čtyřdílný	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
románů	román	k1gInPc2
v	v	k7c6
žánru	žánr	k1gInSc6
tzv.	tzv.	kA
avanturního	avanturní	k2eAgInSc2d1
románu	román	k1gInSc2
(	(	kIx(
<g/>
ruská	ruský	k2eAgFnSc1d1
pozdní	pozdní	k2eAgFnSc1d1
varianta	varianta	k1gFnSc1
pikareskního	pikareskní	k2eAgInSc2d1
románu	román	k1gInSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Salomea	Salomea	k1gFnSc1
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
,	,	kIx,
С	С	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
autorovo	autorův	k2eAgNnSc4d1
vrcholné	vrcholný	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
freska	freska	k1gFnSc1
ruské	ruský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
se	se	k3xPyFc4
mísí	mísit	k5eAaImIp3nP
fantastika	fantastika	k1gFnSc1
a	a	k8xC
realita	realita	k1gFnSc1
a	a	k8xC
dobrodružné	dobrodružný	k2eAgInPc1d1
prvky	prvek	k1gInPc1
s	s	k7c7
mravoličnými	mravoličný	k2eAgFnPc7d1
a	a	k8xC
groteskními	groteskní	k2eAgFnPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Román	román	k1gInSc1
popisuje	popisovat	k5eAaImIp3nS
degeneraci	degenerace	k1gFnSc4
aristokracie	aristokracie	k1gFnSc2
<g/>
,	,	kIx,
morální	morální	k2eAgInSc1d1
rozklad	rozklad	k1gInSc1
<g/>
,	,	kIx,
cynismus	cynismus	k1gInSc1
a	a	k8xC
chamtivost	chamtivost	k1gFnSc1
průmyslníků	průmyslník	k1gMnPc2
a	a	k8xC
obchodníků	obchodník	k1gMnPc2
i	i	k8xC
degradaci	degradace	k1gFnSc4
maloburžoazie	maloburžoazie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tomto	tento	k3xDgNnSc6
pozadí	pozadí	k1gNnSc6
se	se	k3xPyFc4
odehrávají	odehrávat	k5eAaImIp3nP
dramatické	dramatický	k2eAgInPc1d1
osudy	osud	k1gInPc1
ctižádostivé	ctižádostivý	k2eAgInPc1d1
<g/>
,	,	kIx,
vášnivé	vášnivý	k2eAgNnSc1d1
a	a	k8xC
démonické	démonický	k2eAgNnSc1d1
Salomei	Salomei	k1gNnSc1
a	a	k8xC
vychytralého	vychytralý	k2eAgMnSc4d1
dobrodruha	dobrodruh	k1gMnSc4
Dmitrického	Dmitrický	k2eAgMnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Miláček	miláček	k1gMnSc1
osudu	osud	k1gInSc2
(	(	kIx(
<g/>
1856	#num#	k4
<g/>
,	,	kIx,
Б	Б	k?
с	с	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přestože	přestože	k8xS
má	mít	k5eAaImIp3nS
hlavní	hlavní	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
románu	román	k1gInSc2
vše	všechen	k3xTgNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
potřebuje	potřebovat	k5eAaImIp3nS
ke	k	k7c3
štěstí	štěstí	k1gNnSc3
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
pohledný	pohledný	k2eAgMnSc1d1
a	a	k8xC
bohatý	bohatý	k2eAgMnSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
krásnou	krásný	k2eAgFnSc4d1
ženu	žena	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přesto	přesto	k8xC
pociťuje	pociťovat	k5eAaImIp3nS
neustálý	neustálý	k2eAgInSc4d1
smutek	smutek	k1gInSc4
a	a	k8xC
trápí	trápit	k5eAaImIp3nS
se	se	k3xPyFc4
láskou	láska	k1gFnSc7
k	k	k7c3
jiným	jiný	k2eAgFnPc3d1
ženám	žena	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Schovanka	schovanka	k1gFnSc1
Sára	Sára	k1gFnSc1
(	(	kIx(
<g/>
1862	#num#	k4
<g/>
,	,	kIx,
В	В	k?
С	С	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Štěstí-neštěstí	Štěstí-neštěstí	k1gNnSc1
(	(	kIx(
<g/>
1863	#num#	k4
<g/>
,	,	kIx,
С	С	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Divadelní	divadelní	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
<s>
Ratibor	Ratibor	k1gInSc1
(	(	kIx(
<g/>
1841	#num#	k4
<g/>
,	,	kIx,
Р	Р	k?
Х	Х	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1
(	(	kIx(
<g/>
1842	#num#	k4
<g/>
,	,	kIx,
К	К	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kouzelná	kouzelný	k2eAgFnSc1d1
noc	noc	k1gFnSc1
(	(	kIx(
<g/>
1844	#num#	k4
<g/>
,	,	kIx,
В	В	k?
н	н	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odborné	odborný	k2eAgFnPc1d1
práce	práce	k1gFnPc1
</s>
<s>
Stará	starat	k5eAaImIp3nS
slovanská	slovanský	k2eAgNnPc4d1
vlastní	vlastní	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
(	(	kIx(
<g/>
1840	#num#	k4
<g/>
,	,	kIx,
Д	Д	k?
с	с	k?
с	с	k?
и	и	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
moskevského	moskevský	k2eAgInSc2d1
Kremlu	Kreml	k1gInSc2
(	(	kIx(
<g/>
1843	#num#	k4
<g/>
,	,	kIx,
Д	Д	k?
М	М	k?
К	К	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Moskevská	moskevský	k2eAgFnSc1d1
zbrojnice	zbrojnice	k1gFnSc1
(	(	kIx(
<g/>
1844	#num#	k4
<g/>
,	,	kIx,
М	М	k?
О	О	k?
п	п	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
nového	nový	k2eAgInSc2d1
paláce	palác	k1gInSc2
v	v	k7c6
Kremlu	Kreml	k1gInSc6
(	(	kIx(
<g/>
1850	#num#	k4
<g/>
,	,	kIx,
О	О	k?
н	н	k?
д	д	k?
в	в	k?
К	К	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Studie	studie	k1gFnPc1
o	o	k7c6
Svévech	Svév	k1gMnPc6
<g/>
,	,	kIx,
Hunech	Hun	k1gMnPc6
a	a	k8xC
Mongolech	mongol	k1gInPc6
(	(	kIx(
<g/>
1856	#num#	k4
<g/>
-	-	kIx~
<g/>
1860	#num#	k4
<g/>
,	,	kIx,
И	И	k?
о	о	k?
с	с	k?
<g/>
,	,	kIx,
г	г	k?
и	и	k?
м	м	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Attila	Attila	k1gMnSc1
a	a	k8xC
Rus	Rus	k1gMnSc1
4	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
1858	#num#	k4
<g/>
,	,	kIx,
А	А	k?
и	и	k?
Р	Р	k?
IV	IV	kA
и	и	k?
V	v	k7c6
в	в	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Primitivní	primitivní	k2eAgNnPc1d1
náboženství	náboženství	k1gNnPc1
a	a	k8xC
buddhismus	buddhismus	k1gInSc1
(	(	kIx(
<g/>
1864	#num#	k4
<g/>
,	,	kIx,
П	П	k?
в	в	k?
и	и	k?
б	б	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgNnPc1d1
vydání	vydání	k1gNnPc1
</s>
<s>
Salomea	Salomea	k1gFnSc1
<g/>
,	,	kIx,
Lidová	lidový	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1969	#num#	k4
<g/>
,	,	kIx,
přeložila	přeložit	k5eAaPmAgFnS
Anna	Anna	k1gFnSc1
Nováková	Nováková	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Lunatik	lunatik	k1gMnSc1
<g/>
:	:	kIx,
Případ	případ	k1gInSc1
<g/>
,	,	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1976	#num#	k4
<g/>
,	,	kIx,
přeložila	přeložit	k5eAaPmAgFnS
Nina	Nina	k1gFnSc1
Vangeli	Vangel	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Zuřivý	zuřivý	k2eAgInSc1d1
Roland	Roland	k1gInSc1
<g/>
,	,	kIx,
povídka	povídka	k1gFnSc1
je	být	k5eAaImIp3nS
obsažena	obsáhnout	k5eAaPmNgFnS
v	v	k7c6
antologii	antologie	k1gFnSc6
Chorobopisy	chorobopis	k1gInPc4
krutého	krutý	k2eAgInSc2d1
věku	věk	k1gInSc2
<g/>
,	,	kIx,
Lidové	lidový	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Erotida	Erotida	k1gFnSc1
<g/>
,	,	kIx,
povídka	povídka	k1gFnSc1
je	být	k5eAaImIp3nS
obsažena	obsáhnout	k5eAaPmNgFnS
v	v	k7c6
antologii	antologie	k1gFnSc6
Hráči	hráč	k1gMnPc1
<g/>
,	,	kIx,
Lidové	lidový	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Slovník	slovník	k1gInSc4
spisovatelů	spisovatel	k1gMnPc2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1978	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
513	#num#	k4
<g/>
-	-	kIx~
<g/>
514.1	514.1	k4
2	#num#	k4
3	#num#	k4
А	А	k?
В	В	k?
-	-	kIx~
Л	Л	k?
Ф	Ф	k?
<g/>
↑	↑	k?
Alexandr	Alexandr	k1gMnSc1
Veltman	Veltman	k1gMnSc1
<g/>
:	:	kIx,
Lunatik	lunatik	k1gMnSc1
<g/>
,	,	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1976	#num#	k4
<g/>
,	,	kIx,
doslov	doslov	k1gInSc1
Niny	Nina	k1gFnSc2
Vangeli	Vangel	k1gInSc3
<g/>
,	,	kIx,
S.	S.	kA
272	#num#	k4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Vangeli	Vangel	k1gInSc3
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Vangeli	Vangel	k1gInSc3
<g/>
,	,	kIx,
S.	S.	kA
276	#num#	k4
</s>
<s>
Extermí	Extermit	k5eAaPmIp3nS
odkazy	odkaz	k1gInPc4
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Alexander	Alexandra	k1gFnPc2
Veltman	Veltman	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Dílo	dílo	k1gNnSc1
А	А	k?
Ф	Ф	k?
В	В	k?
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Alexandr	Alexandr	k1gMnSc1
Fomič	Fomič	k1gMnSc1
Veltman	Veltman	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
В	В	k?
А	А	k?
Ф	Ф	k?
–	–	k?
Lib	Liba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
ru	ru	k?
</s>
<s>
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
В	В	k?
А	А	k?
Ф	Ф	k?
–	–	k?
Hrono	Hrono	k1gNnSc4
<g/>
.	.	kIx.
<g/>
ru	ru	k?
</s>
<s>
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
В	В	k?
А	А	k?
Ф	Ф	k?
–	–	k?
Э	Э	k?
К	К	k?
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Alexandr	Alexandr	k1gMnSc1
Fomič	Fomič	k1gMnSc1
Veltman	Veltman	k1gMnSc1
na	na	k7c6
webu	web	k1gInSc6
Databazeknih	Databazekniha	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Alexandr	Alexandr	k1gMnSc1
Fomič	Fomič	k1gMnSc1
Veltman	Veltman	k1gMnSc1
–	–	k?
LEGIE	legie	k1gFnSc2
–	–	k?
databáze	databáze	k1gFnSc2
knih	kniha	k1gFnPc2
fantasy	fantas	k1gInPc1
a	a	k8xC
sci-fi	sci-fi	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3448	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
119301121	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0854	#num#	k4
828X	828X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80010530	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
5103133	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80010530	#num#	k4
</s>
