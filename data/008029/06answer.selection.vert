<s>
Ateroskleróza	ateroskleróza	k1gFnSc1	ateroskleróza
(	(	kIx(	(
<g/>
též	též	k9	též
arterioskleróza	arterioskleróza	k1gFnSc1	arterioskleróza
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kornatění	kornatění	k1gNnSc1	kornatění
tepen	tepna	k1gFnPc2	tepna
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ukládání	ukládání	k1gNnSc2	ukládání
tukových	tukový	k2eAgFnPc2d1	tuková
látek	látka	k1gFnPc2	látka
do	do	k7c2	do
stěny	stěna	k1gFnSc2	stěna
tepny	tepna	k1gFnSc2	tepna
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
vznikem	vznik	k1gInSc7	vznik
arterómů	arteróm	k1gInPc2	arteróm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
