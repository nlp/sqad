<s>
Ateroskleróza	ateroskleróza	k1gFnSc1	ateroskleróza
(	(	kIx(	(
<g/>
též	též	k9	též
arterioskleróza	arterioskleróza	k1gFnSc1	arterioskleróza
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kornatění	kornatění	k1gNnSc1	kornatění
tepen	tepna	k1gFnPc2	tepna
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ukládání	ukládání	k1gNnSc2	ukládání
tukových	tukový	k2eAgFnPc2d1	tuková
látek	látka	k1gFnPc2	látka
do	do	k7c2	do
stěny	stěna	k1gFnSc2	stěna
tepny	tepna	k1gFnSc2	tepna
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
vznikem	vznik	k1gInSc7	vznik
arterómů	arteróm	k1gInPc2	arteróm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
ve	v	k7c6	v
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
zemích	zem	k1gFnPc6	zem
představuje	představovat	k5eAaImIp3nS	představovat
významný	významný	k2eAgInSc1d1	významný
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
problém	problém	k1gInSc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Faktickou	faktický	k2eAgFnSc7d1	faktická
příčinou	příčina	k1gFnSc7	příčina
aterosklerózy	ateroskleróza	k1gFnSc2	ateroskleróza
je	být	k5eAaImIp3nS	být
ukládání	ukládání	k1gNnSc1	ukládání
tukových	tukový	k2eAgFnPc2d1	tuková
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
cévní	cévní	k2eAgFnSc2d1	cévní
stěny	stěna	k1gFnSc2	stěna
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
tzv.	tzv.	kA	tzv.
aterosklerotických	aterosklerotický	k2eAgInPc2d1	aterosklerotický
plátů	plát	k1gInPc2	plát
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
usazování	usazování	k1gNnSc3	usazování
polysacharidů	polysacharid	k1gInPc2	polysacharid
a	a	k8xC	a
vápníku	vápník	k1gInSc2	vápník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ukládaní	ukládaný	k2eAgMnPc1d1	ukládaný
těchto	tento	k3xDgFnPc2	tento
látek	látka	k1gFnPc2	látka
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zúžení	zúžení	k1gNnSc3	zúžení
stěny	stěna	k1gFnSc2	stěna
tepny	tepna	k1gFnSc2	tepna
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc4	snížení
její	její	k3xOp3gFnSc2	její
pružnosti	pružnost	k1gFnSc2	pružnost
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
omezit	omezit	k5eAaPmF	omezit
tok	tok	k1gInSc4	tok
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Orgány	orgán	k1gInPc1	orgán
nejsou	být	k5eNaImIp3nP	být
zásobeny	zásoben	k2eAgInPc1d1	zásoben
dostatečně	dostatečně	k6eAd1	dostatečně
kyslíkem	kyslík	k1gInSc7	kyslík
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
poškození	poškození	k1gNnSc3	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Ateroskleróza	ateroskleróza	k1gFnSc1	ateroskleróza
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
např.	např.	kA	např.
infarkt	infarkt	k1gInSc4	infarkt
myokardu	myokard	k1gInSc2	myokard
<g/>
,	,	kIx,	,
cévní	cévní	k2eAgFnSc1d1	cévní
mozková	mozkový	k2eAgFnSc1d1	mozková
příhoda	příhoda	k1gFnSc1	příhoda
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
rizikových	rizikový	k2eAgInPc2d1	rizikový
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
urychlují	urychlovat	k5eAaImIp3nP	urychlovat
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnPc1d1	zásadní
pro	pro	k7c4	pro
její	její	k3xOp3gInSc4	její
rozvoj	rozvoj	k1gInSc4	rozvoj
je	být	k5eAaImIp3nS	být
nadměrný	nadměrný	k2eAgInSc1d1	nadměrný
příjem	příjem	k1gInSc1	příjem
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
tedy	tedy	k8xC	tedy
příliš	příliš	k6eAd1	příliš
tučných	tučný	k2eAgNnPc2d1	tučné
jídel	jídlo	k1gNnPc2	jídlo
<g/>
,	,	kIx,	,
jistou	jistý	k2eAgFnSc4d1	jistá
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
i	i	k9	i
genetika	genetika	k1gFnSc1	genetika
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
poruše	porucha	k1gFnSc3	porucha
transportu	transport	k1gInSc2	transport
tuků	tuk	k1gInPc2	tuk
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
projevované	projevovaný	k2eAgNnSc1d1	projevované
změnou	změna	k1gFnSc7	změna
koncentrace	koncentrace	k1gFnSc2	koncentrace
lipoproteinů	lipoprotein	k1gMnPc2	lipoprotein
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
poruše	porucha	k1gFnSc6	porucha
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
výstelky	výstelka	k1gFnSc2	výstelka
cévy	céva	k1gFnSc2	céva
dochází	docházet	k5eAaImIp3nS	docházet
(	(	kIx(	(
<g/>
kouření	kouření	k1gNnSc1	kouření
<g/>
,	,	kIx,	,
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
tlak	tlak	k1gInSc1	tlak
krve	krev	k1gFnSc2	krev
<g/>
)	)	kIx)	)
k	k	k7c3	k
průniku	průnik	k1gInSc3	průnik
lipoproteinů	lipoprotein	k1gInPc2	lipoprotein
do	do	k7c2	do
stěny	stěna	k1gFnSc2	stěna
cévy	céva	k1gFnSc2	céva
a	a	k8xC	a
ke	k	k7c3	k
krystalizaci	krystalizace	k1gFnSc3	krystalizace
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgInPc1d1	tradiční
rizikové	rizikový	k2eAgInPc1d1	rizikový
faktory	faktor	k1gInPc1	faktor
<g/>
:	:	kIx,	:
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
celkový	celkový	k2eAgInSc1d1	celkový
cholesterol	cholesterol	k1gInSc1	cholesterol
a	a	k8xC	a
LDL	LDL	kA	LDL
kouření	kouření	k1gNnSc4	kouření
hypertenze	hypertenze	k1gFnSc2	hypertenze
-	-	kIx~	-
vysoký	vysoký	k2eAgInSc1d1	vysoký
krevní	krevní	k2eAgInSc1d1	krevní
tlak	tlak	k1gInSc1	tlak
diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc1	mellitus
fyzická	fyzický	k2eAgFnSc1d1	fyzická
inaktivita	inaktivita	k1gFnSc1	inaktivita
obezita	obezita	k1gFnSc1	obezita
dědičné	dědičný	k2eAgFnSc2d1	dědičná
predispozice	predispozice	k1gFnSc2	predispozice
Nově	nově	k6eAd1	nově
objevené	objevený	k2eAgInPc1d1	objevený
rizikové	rizikový	k2eAgInPc1d1	rizikový
faktory	faktor	k1gInPc1	faktor
<g/>
:	:	kIx,	:
VLDL	VLDL	kA	VLDL
částice	částice	k1gFnSc1	částice
homocystein	homocystein	k1gMnSc1	homocystein
lipoprotein	lipoprotein	k1gMnSc1	lipoprotein
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
oxidační	oxidační	k2eAgInSc4d1	oxidační
stres	stres	k1gInSc4	stres
hemostatické	hemostatický	k2eAgInPc1d1	hemostatický
faktory	faktor	k1gInPc1	faktor
infekční	infekční	k2eAgInSc1d1	infekční
agens	agens	k1gInSc4	agens
C-reaktivní	Ceaktivní	k2eAgInSc4d1	C-reaktivní
protein	protein	k1gInSc4	protein
Prevencí	prevence	k1gFnPc2	prevence
je	být	k5eAaImIp3nS	být
zdravý	zdravý	k2eAgInSc4d1	zdravý
životní	životní	k2eAgInSc4d1	životní
<g />
.	.	kIx.	.
</s>
<s>
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
omezit	omezit	k5eAaPmF	omezit
vysokoenergetická	vysokoenergetický	k2eAgFnSc1d1	vysokoenergetická
(	(	kIx(	(
<g/>
tučná	tučný	k2eAgFnSc1d1	tučná
a	a	k8xC	a
sladká	sladký	k2eAgNnPc1d1	sladké
jídla	jídlo	k1gNnPc1	jídlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvýšit	zvýšit	k5eAaPmF	zvýšit
množství	množství	k1gNnSc4	množství
vlákniny	vláknina	k1gFnSc2	vláknina
(	(	kIx(	(
<g/>
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
,	,	kIx,	,
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
)	)	kIx)	)
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
živočišných	živočišný	k2eAgInPc2d1	živočišný
tuků	tuk	k1gInPc2	tuk
používat	používat	k5eAaImF	používat
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
za	za	k7c2	za
studena	studeno	k1gNnSc2	studeno
lisované	lisovaný	k2eAgInPc4d1	lisovaný
oleje	olej	k1gInPc4	olej
<g/>
,	,	kIx,	,
sportovat	sportovat	k5eAaImF	sportovat
a	a	k8xC	a
nekouřit	kouřit	k5eNaImF	kouřit
<g/>
.	.	kIx.	.
</s>
<s>
Léčí	léčit	k5eAaImIp3nP	léčit
se	se	k3xPyFc4	se
příčiny	příčina	k1gFnPc1	příčina
tohoto	tento	k3xDgNnSc2	tento
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
aterosklerotických	aterosklerotický	k2eAgInPc2d1	aterosklerotický
plátů	plát	k1gInPc2	plát
(	(	kIx(	(
<g/>
ateromů	aterom	k1gInPc2	aterom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
léčbě	léčba	k1gFnSc6	léčba
aterosklerózy	ateroskleróza	k1gFnSc2	ateroskleróza
dominuje	dominovat	k5eAaImIp3nS	dominovat
léčba	léčba	k1gFnSc1	léčba
především	především	k9	především
vysoké	vysoký	k2eAgFnPc4d1	vysoká
hladiny	hladina	k1gFnPc4	hladina
cholesterolu	cholesterol	k1gInSc2	cholesterol
(	(	kIx(	(
<g/>
hypercholesterolemie	hypercholesterolemie	k1gFnSc1	hypercholesterolemie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
léčba	léčba	k1gFnSc1	léčba
hypertenze	hypertenze	k1gFnSc2	hypertenze
<g/>
,	,	kIx,	,
obezity	obezita	k1gFnSc2	obezita
<g/>
,	,	kIx,	,
cukrovky	cukrovka	k1gFnSc2	cukrovka
a	a	k8xC	a
odvykání	odvykání	k1gNnSc2	odvykání
kouření	kouření	k1gNnSc2	kouření
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
příčin	příčina	k1gFnPc2	příčina
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
výběr	výběr	k1gInSc1	výběr
vhodného	vhodný	k2eAgInSc2d1	vhodný
léku	lék	k1gInSc2	lék
pro	pro	k7c4	pro
pacienta	pacient	k1gMnSc4	pacient
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
zohledňuje	zohledňovat	k5eAaImIp3nS	zohledňovat
řada	řada	k1gFnSc1	řada
faktorů	faktor	k1gInPc2	faktor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
doprovodná	doprovodný	k2eAgNnPc1d1	doprovodné
onemocnění	onemocnění	k1gNnPc1	onemocnění
<g/>
,	,	kIx,	,
interakce	interakce	k1gFnPc1	interakce
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
léky	lék	k1gInPc7	lék
atd	atd	kA	atd
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
krokem	krok	k1gInSc7	krok
je	být	k5eAaImIp3nS	být
zdravý	zdravý	k2eAgInSc1d1	zdravý
životní	životní	k2eAgInSc1d1	životní
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
při	při	k7c6	při
nadváze	nadváha	k1gFnSc6	nadváha
nebo	nebo	k8xC	nebo
obezitě	obezita	k1gFnSc6	obezita
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
snížení	snížení	k1gNnSc1	snížení
hmotnosti	hmotnost	k1gFnSc2	hmotnost
a	a	k8xC	a
stabilizace	stabilizace	k1gFnSc2	stabilizace
váhy	váha	k1gFnSc2	váha
<g/>
.	.	kIx.	.
</s>
<s>
Lékař	lékař	k1gMnSc1	lékař
může	moct	k5eAaImIp3nS	moct
nejprve	nejprve	k6eAd1	nejprve
navrhnout	navrhnout	k5eAaPmF	navrhnout
nefarmakologickou	farmakologický	k2eNgFnSc4d1	nefarmakologická
léčbu	léčba	k1gFnSc4	léčba
(	(	kIx(	(
<g/>
dieta	dieta	k1gFnSc1	dieta
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
cholesterolu	cholesterol	k1gInSc2	cholesterol
a	a	k8xC	a
dostatek	dostatek	k1gInSc4	dostatek
pohybu	pohyb	k1gInSc2	pohyb
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
farmakologickou	farmakologický	k2eAgFnSc4d1	farmakologická
léčbu	léčba	k1gFnSc4	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
používanými	používaný	k2eAgInPc7d1	používaný
léčivými	léčivý	k2eAgInPc7d1	léčivý
přípravky	přípravek	k1gInPc7	přípravek
na	na	k7c6	na
snížení	snížení	k1gNnSc6	snížení
cholesterolu	cholesterol	k1gInSc2	cholesterol
jsou	být	k5eAaImIp3nP	být
statiny	statin	k1gInPc1	statin
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
<g/>
,	,	kIx,	,
ezetimib	ezetimib	k1gInSc1	ezetimib
<g/>
,	,	kIx,	,
fibráty	fibrát	k1gInPc1	fibrát
a	a	k8xC	a
kyselina	kyselina	k1gFnSc1	kyselina
nikotinová	nikotinový	k2eAgFnSc1d1	nikotinová
<g/>
.	.	kIx.	.
</s>
<s>
Pacient	pacient	k1gMnSc1	pacient
musí	muset	k5eAaImIp3nS	muset
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
léčba	léčba	k1gFnSc1	léčba
je	být	k5eAaImIp3nS	být
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Statiny	Statina	k1gFnSc2	Statina
(	(	kIx(	(
<g/>
atorvastatin	atorvastatina	k1gFnPc2	atorvastatina
<g/>
,	,	kIx,	,
fluvastatin	fluvastatina	k1gFnPc2	fluvastatina
<g/>
,	,	kIx,	,
lovastatin	lovastatina	k1gFnPc2	lovastatina
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
nízkým	nízký	k2eAgInSc7d1	nízký
výskytem	výskyt	k1gInSc7	výskyt
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
a	a	k8xC	a
inhibují	inhibovat	k5eAaBmIp3nP	inhibovat
enzym	enzym	k1gInSc4	enzym
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
nutný	nutný	k2eAgInSc1d1	nutný
pro	pro	k7c4	pro
syntézu	syntéza	k1gFnSc4	syntéza
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
.	.	kIx.	.
</s>
<s>
Pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
(	(	kIx(	(
<g/>
cholestyramin	cholestyramin	k1gInSc1	cholestyramin
<g/>
,	,	kIx,	,
colestipol	colestipol	k1gInSc1	colestipol
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jediné	jediný	k2eAgInPc1d1	jediný
doporučované	doporučovaný	k2eAgInPc1d1	doporučovaný
léky	lék	k1gInPc1	lék
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
vysoké	vysoký	k2eAgFnSc2d1	vysoká
hladiny	hladina	k1gFnSc2	hladina
cholesterolu	cholesterol	k1gInSc2	cholesterol
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Nevstřebávají	vstřebávat	k5eNaImIp3nP	vstřebávat
se	se	k3xPyFc4	se
v	v	k7c6	v
tenkém	tenký	k2eAgNnSc6d1	tenké
střevě	střevo	k1gNnSc6	střevo
a	a	k8xC	a
přerušují	přerušovat	k5eAaImIp3nP	přerušovat
enterohepatální	enterohepatální	k2eAgInSc4d1	enterohepatální
oběh	oběh	k1gInSc4	oběh
(	(	kIx(	(
<g/>
střevo-jaterní	střevoaterní	k2eAgInSc4d1	střevo-jaterní
oběh	oběh	k1gInSc4	oběh
<g/>
)	)	kIx)	)
žlučových	žlučový	k2eAgFnPc2d1	žlučová
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
nepříjemná	příjemný	k2eNgFnSc1d1	nepříjemná
chuť	chuť	k1gFnSc1	chuť
a	a	k8xC	a
často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zácpám	zácpa	k1gFnPc3	zácpa
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
statinů	statin	k1gInPc2	statin
<g/>
,	,	kIx,	,
fibrátů	fibrát	k1gInPc2	fibrát
a	a	k8xC	a
kyseliny	kyselina	k1gFnSc2	kyselina
nikotinové	nikotinový	k2eAgFnSc2d1	nikotinová
není	být	k5eNaImIp3nS	být
zatím	zatím	k6eAd1	zatím
žádná	žádný	k3yNgFnSc1	žádný
dlouhodobější	dlouhodobý	k2eAgFnSc1d2	dlouhodobější
studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
prokázala	prokázat	k5eAaPmAgFnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
látky	látka	k1gFnPc4	látka
vhodné	vhodný	k2eAgFnPc4d1	vhodná
pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
hypercholesterolémie	hypercholesterolémie	k1gFnSc2	hypercholesterolémie
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Ezetimib	Ezetimib	k1gInSc1	Ezetimib
(	(	kIx(	(
<g/>
léčivý	léčivý	k2eAgInSc1d1	léčivý
přípravek	přípravek	k1gInSc1	přípravek
Ezetrol	Ezetrol	k1gInSc1	Ezetrol
<g/>
)	)	kIx)	)
inhibuje	inhibovat	k5eAaBmIp3nS	inhibovat
vstřebávání	vstřebávání	k1gNnSc4	vstřebávání
cholesterolu	cholesterol	k1gInSc2	cholesterol
v	v	k7c6	v
tenkém	tenký	k2eAgNnSc6d1	tenké
střevě	střevo	k1gNnSc6	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
často	často	k6eAd1	často
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
se	s	k7c7	s
statiny	statin	k1gInPc7	statin
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
není	být	k5eNaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
monoterapii	monoterapie	k1gFnSc4	monoterapie
<g/>
.	.	kIx.	.
</s>
<s>
Fibráty	Fibrát	k1gInPc1	Fibrát
(	(	kIx(	(
<g/>
fenofibrát	fenofibrát	k1gInSc1	fenofibrát
<g/>
,	,	kIx,	,
bezafibrát	bezafibrát	k1gInSc1	bezafibrát
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
deriváty	derivát	k1gInPc1	derivát
kyseliny	kyselina	k1gFnSc2	kyselina
fibrové	fibrový	k2eAgInPc1d1	fibrový
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
specifických	specifický	k2eAgInPc2d1	specifický
receptorů	receptor	k1gInPc2	receptor
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
metabolismus	metabolismus	k1gInSc4	metabolismus
tuků	tuk	k1gInPc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
nevýhod	nevýhoda	k1gFnPc2	nevýhoda
fibrátů	fibrát	k1gMnPc2	fibrát
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
hypolipidemiky	hypolipidemik	k1gMnPc7	hypolipidemik
<g/>
,	,	kIx,	,
dostatečně	dostatečně	k6eAd1	dostatečně
nesnižují	snižovat	k5eNaImIp3nP	snižovat
koncentraci	koncentrace	k1gFnSc4	koncentrace
LDL-cholesterolu	LDLholesterol	k1gInSc2	LDL-cholesterol
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
aterosklerotického	aterosklerotický	k2eAgInSc2d1	aterosklerotický
plátu	plát	k1gInSc2	plát
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
nikotinová	nikotinový	k2eAgFnSc1d1	nikotinová
inhibuje	inhibovat	k5eAaBmIp3nS	inhibovat
lipolýzu	lipolýza	k1gFnSc4	lipolýza
(	(	kIx(	(
<g/>
štěpení	štěpení	k1gNnSc1	štěpení
tuku	tuk	k1gInSc2	tuk
<g/>
)	)	kIx)	)
v	v	k7c6	v
tukové	tukový	k2eAgFnSc6d1	tuková
tkáni	tkáň	k1gFnSc6	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
nevyužívá	využívat	k5eNaImIp3nS	využívat
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
registrována	registrován	k2eAgFnSc1d1	registrována
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
se	se	k3xPyFc4	se
3	[number]	k4	3
stupně	stupeň	k1gInSc2	stupeň
vysokého	vysoký	k2eAgInSc2d1	vysoký
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
(	(	kIx(	(
<g/>
TK	TK	kA	TK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
hodnotou	hodnota	k1gFnSc7	hodnota
systolického	systolický	k2eAgNnSc2d1	systolické
(	(	kIx(	(
<g/>
STK	STK	kA	STK
<g/>
)	)	kIx)	)
a	a	k8xC	a
diastolického	diastolický	k2eAgNnSc2d1	diastolické
(	(	kIx(	(
<g/>
DTK	DTK	kA	DTK
<g/>
)	)	kIx)	)
tlaku	tlak	k1gInSc2	tlak
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
existují	existovat	k5eAaImIp3nP	existovat
3	[number]	k4	3
vývojová	vývojový	k2eAgNnPc1d1	vývojové
stádia	stádium	k1gNnPc1	stádium
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
stádium	stádium	k1gNnSc1	stádium
prostého	prostý	k2eAgNnSc2d1	prosté
zvýšení	zvýšení	k1gNnSc2	zvýšení
tlaku	tlak	k1gInSc2	tlak
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
stádium	stádium	k1gNnSc1	stádium
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgFnPc1d1	patrná
orgánové	orgánový	k2eAgFnPc1d1	orgánová
změny	změna	k1gFnPc1	změna
a	a	k8xC	a
poslední	poslední	k2eAgNnPc1d1	poslední
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
stádium	stádium	k1gNnSc4	stádium
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hypertenze	hypertenze	k1gFnSc1	hypertenze
s	s	k7c7	s
těžšími	těžký	k2eAgFnPc7d2	těžší
orgánovými	orgánový	k2eAgFnPc7d1	orgánová
změnami	změna	k1gFnPc7	změna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
již	již	k6eAd1	již
k	k	k7c3	k
selhávání	selhávání	k1gNnSc3	selhávání
<g/>
.	.	kIx.	.
</s>
<s>
Nefarmakologická	farmakologický	k2eNgFnSc1d1	nefarmakologická
léčba	léčba	k1gFnSc1	léčba
hypertenze	hypertenze	k1gFnSc2	hypertenze
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
u	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
s	s	k7c7	s
hypertenzí	hypertenze	k1gFnSc7	hypertenze
i	i	k8xC	i
vysokým	vysoký	k2eAgInSc7d1	vysoký
normálním	normální	k2eAgInSc7d1	normální
tlakem	tlak	k1gInSc7	tlak
krve	krev	k1gFnSc2	krev
(	(	kIx(	(
<g/>
STK	STK	kA	STK
130	[number]	k4	130
-	-	kIx~	-
139	[number]	k4	139
mm	mm	kA	mm
Hg	Hg	k1gFnPc2	Hg
DTK	DTK	kA	DTK
85	[number]	k4	85
-	-	kIx~	-
89	[number]	k4	89
mm	mm	kA	mm
Hg	Hg	k1gFnPc2	Hg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zanechání	zanechání	k1gNnSc1	zanechání
kouření	kouření	k1gNnSc2	kouření
Snížení	snížení	k1gNnSc2	snížení
nadměrné	nadměrný	k2eAgFnSc2d1	nadměrná
konzumace	konzumace	k1gFnSc2	konzumace
alkoholu	alkohol	k1gInSc2	alkohol
Omezení	omezení	k1gNnPc2	omezení
příjmu	příjem	k1gInSc2	příjem
soli	sůl	k1gFnSc2	sůl
Konzumovat	konzumovat	k5eAaBmF	konzumovat
více	hodně	k6eAd2	hodně
ovoce	ovoce	k1gNnSc4	ovoce
a	a	k8xC	a
zeleninu	zelenina	k1gFnSc4	zelenina
Redukce	redukce	k1gFnSc2	redukce
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
u	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
s	s	k7c7	s
nadváhou	nadváha	k1gFnSc7	nadváha
a	a	k8xC	a
obezitou	obezita	k1gFnSc7	obezita
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
stabilizace	stabilizace	k1gFnSc1	stabilizace
váhy	váha	k1gFnSc2	váha
Dostatečná	dostatečný	k2eAgFnSc1d1	dostatečná
fyzická	fyzický	k2eAgFnSc1d1	fyzická
aktivita	aktivita	k1gFnSc1	aktivita
Omezit	omezit	k5eAaPmF	omezit
léky	lék	k1gInPc1	lék
podporující	podporující	k2eAgInPc1d1	podporující
zadržování	zadržování	k1gNnSc4	zadržování
sodíku	sodík	k1gInSc2	sodík
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
Léčivé	léčivý	k2eAgFnSc2d1	léčivá
látky	látka	k1gFnSc2	látka
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
monoterapii	monoterapie	k1gFnSc6	monoterapie
i	i	k8xC	i
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
léčivými	léčivý	k2eAgFnPc7d1	léčivá
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
diuretika	diuretikum	k1gNnPc1	diuretikum
<g/>
,	,	kIx,	,
ACE-1	ACE-1	k1gFnPc1	ACE-1
inhibitory	inhibitor	k1gInPc4	inhibitor
<g/>
,	,	kIx,	,
AT1	AT1	k1gFnPc4	AT1
inhibitory	inhibitor	k1gInPc4	inhibitor
<g/>
,	,	kIx,	,
blokátory	blokátor	k1gInPc4	blokátor
kalciových	kalciový	k2eAgInPc2d1	kalciový
kanálů	kanál	k1gInPc2	kanál
a	a	k8xC	a
β	β	k?	β
<g/>
.	.	kIx.	.
</s>
<s>
Preferované	preferovaný	k2eAgFnPc1d1	preferovaná
látky	látka	k1gFnPc1	látka
u	u	k7c2	u
následujících	následující	k2eAgFnPc2d1	následující
situací	situace	k1gFnPc2	situace
<g/>
:	:	kIx,	:
Starší	starý	k2eAgFnPc1d2	starší
osoby	osoba	k1gFnPc1	osoba
–	–	k?	–
diuretika	diuretikum	k1gNnPc4	diuretikum
a	a	k8xC	a
blokátory	blokátor	k1gInPc4	blokátor
kalciových	kalciový	k2eAgInPc2d1	kalciový
kanálů	kanál	k1gInPc2	kanál
Angina	angina	k1gFnSc1	angina
pectoris	pectoris	k1gFnSc1	pectoris
–	–	k?	–
β	β	k?	β
a	a	k8xC	a
blokátory	blokátor	k1gInPc7	blokátor
kalciových	kalciový	k2eAgInPc2d1	kalciový
kanálů	kanál	k1gInPc2	kanál
Srdeční	srdeční	k2eAgNnSc1d1	srdeční
selhání	selhání	k1gNnSc1	selhání
–	–	k?	–
diuretika	diuretikum	k1gNnPc4	diuretikum
a	a	k8xC	a
β	β	k?	β
Po	po	k7c6	po
infarktu	infarkt	k1gInSc6	infarkt
myokardu	myokard	k1gInSc2	myokard
–	–	k?	–
ACE-1	ACE-1	k1gMnSc1	ACE-1
inhibitory	inhibitor	k1gInPc4	inhibitor
a	a	k8xC	a
AT1	AT1	k1gFnPc4	AT1
inhibitory	inhibitor	k1gInPc7	inhibitor
Diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gMnSc1	mellitus
–	–	k?	–
ACE-1	ACE-1	k1gMnSc1	ACE-1
inhibitory	inhibitor	k1gInPc4	inhibitor
a	a	k8xC	a
AT1	AT1	k1gMnPc1	AT1
inhibitory	inhibitor	k1gInPc4	inhibitor
Těhotenství	těhotenství	k1gNnSc4	těhotenství
–	–	k?	–
β	β	k?	β
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
blokátory	blokátor	k1gInPc1	blokátor
kalciových	kalciový	k2eAgInPc2d1	kalciový
kanálů	kanál	k1gInPc2	kanál
Ischemická	ischemický	k2eAgFnSc1d1	ischemická
choroba	choroba	k1gFnSc1	choroba
dolních	dolní	k2eAgFnPc2d1	dolní
končetin	končetina	k1gFnPc2	končetina
–	–	k?	–
blokátory	blokátor	k1gInPc1	blokátor
kalciových	kalciový	k2eAgInPc2d1	kalciový
kanálů	kanál	k1gInPc2	kanál
Diuretika	diuretikum	k1gNnSc2	diuretikum
(	(	kIx(	(
<g/>
indapamid	indapamid	k1gInSc1	indapamid
<g/>
,	,	kIx,	,
furosemid	furosemid	k1gInSc1	furosemid
<g/>
,	,	kIx,	,
amilorid	amilorid	k1gInSc1	amilorid
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
ledvinových	ledvinový	k2eAgInPc6d1	ledvinový
tubulech	tubulus	k1gInPc6	tubulus
a	a	k8xC	a
zabraňují	zabraňovat	k5eAaImIp3nP	zabraňovat
reabsorbci	reabsorbce	k1gMnPc1	reabsorbce
(	(	kIx(	(
<g/>
zpětnému	zpětný	k2eAgNnSc3d1	zpětné
vstřebávání	vstřebávání	k1gNnSc3	vstřebávání
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
<g/>
)	)	kIx)	)
vody	voda	k1gFnSc2	voda
nebo	nebo	k8xC	nebo
sodíku	sodík	k1gInSc2	sodík
(	(	kIx(	(
<g/>
při	při	k7c6	při
zpětném	zpětný	k2eAgNnSc6d1	zpětné
vstřebávání	vstřebávání	k1gNnSc6	vstřebávání
sodíku	sodík	k1gInSc2	sodík
strhává	strhávat	k5eAaImIp3nS	strhávat
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
sebou	se	k3xPyFc7	se
molekuly	molekula	k1gFnPc1	molekula
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zadržování	zadržování	k1gNnSc3	zadržování
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
TK	TK	kA	TK
<g/>
)	)	kIx)	)
β	β	k?	β
(	(	kIx(	(
<g/>
bisoprolol	bisoprolol	k1gInSc1	bisoprolol
<g/>
,	,	kIx,	,
metipranolol	metipranolol	k1gInSc1	metipranolol
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
našly	najít	k5eAaPmAgFnP	najít
široké	široký	k2eAgNnSc4d1	široké
uplatnění	uplatnění	k1gNnSc4	uplatnění
například	například	k6eAd1	například
u	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
s	s	k7c7	s
arytmií	arytmie	k1gFnSc7	arytmie
Blokátory	blokátor	k1gInPc7	blokátor
kalciových	kalciový	k2eAgInPc2d1	kalciový
kanálů	kanál	k1gInPc2	kanál
(	(	kIx(	(
<g/>
amlodipin	amlodipin	k1gMnSc1	amlodipin
<g/>
,	,	kIx,	,
felodipin	felodipin	k1gMnSc1	felodipin
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
dilataci	dilatace	k1gFnSc4	dilatace
(	(	kIx(	(
<g/>
rozšíření	rozšíření	k1gNnSc4	rozšíření
<g/>
)	)	kIx)	)
periferních	periferní	k2eAgFnPc2d1	periferní
arteriol	arteriola	k1gFnPc2	arteriola
(	(	kIx(	(
<g/>
tepének	tepénka	k1gFnPc2	tepénka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
experimentu	experiment	k1gInSc6	experiment
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
průtok	průtok	k1gInSc4	průtok
krve	krev	k1gFnSc2	krev
koronárními	koronární	k2eAgFnPc7d1	koronární
tepnami	tepna	k1gFnPc7	tepna
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
využívat	využívat	k5eAaImF	využívat
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
ischemické	ischemický	k2eAgFnSc2d1	ischemická
choroby	choroba	k1gFnSc2	choroba
srdeční	srdeční	k2eAgFnSc2d1	srdeční
ACE-1	ACE-1	k1gFnSc2	ACE-1
inhibitory	inhibitor	k1gInPc1	inhibitor
(	(	kIx(	(
<g/>
monopril	monopril	k1gMnSc1	monopril
<g/>
,	,	kIx,	,
cilazapril	cilazapril	k1gMnSc1	cilazapril
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
inhibují	inhibovat	k5eAaBmIp3nP	inhibovat
enzym	enzym	k1gInSc4	enzym
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
renin-angiotenzinovém	reninngiotenzinový	k2eAgInSc6d1	renin-angiotenzinový
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hlavní	hlavní	k2eAgFnSc1d1	hlavní
úloha	úloha	k1gFnSc1	úloha
je	být	k5eAaImIp3nS	být
regulace	regulace	k1gFnSc1	regulace
tlaku	tlak	k1gInSc2	tlak
krve	krev	k1gFnSc2	krev
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgFnP	být
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
výsledky	výsledek	k1gInPc1	výsledek
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
možnost	možnost	k1gFnSc4	možnost
využití	využití	k1gNnSc2	využití
antihypertenziv	antihypertenzit	k5eAaPmDgInS	antihypertenzit
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
výskytu	výskyt	k1gInSc2	výskyt
cukrovky	cukrovka	k1gFnSc2	cukrovka
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
léky	lék	k1gInPc1	lék
byly	být	k5eAaImAgInP	být
původně	původně	k6eAd1	původně
určeny	určit	k5eAaPmNgInP	určit
pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
vysokého	vysoký	k2eAgInSc2d1	vysoký
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
ACE-1	ACE-1	k1gFnSc6	ACE-1
inhibitorech	inhibitor	k1gInPc6	inhibitor
a	a	k8xC	a
AT1	AT1	k1gFnPc6	AT1
inhibitorech	inhibitor	k1gInPc6	inhibitor
se	se	k3xPyFc4	se
již	již	k6eAd1	již
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
citlivost	citlivost	k1gFnSc4	citlivost
organismu	organismus	k1gInSc2	organismus
na	na	k7c4	na
inzulín	inzulín	k1gInSc4	inzulín
<g/>
.	.	kIx.	.
</s>
<s>
Předpoklady	předpoklad	k1gInPc1	předpoklad
se	se	k3xPyFc4	se
potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
v	v	k7c6	v
rozsáhlých	rozsáhlý	k2eAgNnPc6d1	rozsáhlé
studiích	studio	k1gNnPc6	studio
<g/>
.	.	kIx.	.
</s>
<s>
Aterosklerotický	Aterosklerotický	k2eAgInSc1d1	Aterosklerotický
proces	proces	k1gInSc1	proces
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
funkci	funkce	k1gFnSc4	funkce
tepen	tepna	k1gFnPc2	tepna
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
podle	podle	k7c2	podle
lokalizace	lokalizace	k1gFnSc2	lokalizace
specifické	specifický	k2eAgInPc4d1	specifický
následky	následek	k1gInPc4	následek
<g/>
:	:	kIx,	:
Ateroskleróza	ateroskleróza	k1gFnSc1	ateroskleróza
koronárních	koronární	k2eAgFnPc2d1	koronární
(	(	kIx(	(
<g/>
věnčitých	věnčitý	k2eAgFnPc2d1	věnčitá
<g/>
)	)	kIx)	)
tepen	tepna	k1gFnPc2	tepna
–	–	k?	–
angina	angina	k1gFnSc1	angina
pectoris	pectoris	k1gFnSc4	pectoris
<g/>
,	,	kIx,	,
infarkt	infarkt	k1gInSc4	infarkt
myokardu	myokard	k1gInSc2	myokard
Ateroskleróza	ateroskleróza	k1gFnSc1	ateroskleróza
krkavice	krkavice	k1gFnSc1	krkavice
–	–	k?	–
ischemická	ischemický	k2eAgFnSc1d1	ischemická
cévní	cévní	k2eAgFnSc1d1	cévní
mozková	mozkový	k2eAgFnSc1d1	mozková
příhoda	příhoda	k1gFnSc1	příhoda
Ateroskleróza	ateroskleróza	k1gFnSc1	ateroskleróza
renálních	renální	k2eAgFnPc2d1	renální
(	(	kIx(	(
<g/>
ledvinných	ledvinný	k2eAgFnPc2d1	ledvinná
<g/>
)	)	kIx)	)
tepen	tepna	k1gFnPc2	tepna
–	–	k?	–
hypertenze	hypertenze	k1gFnSc2	hypertenze
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
podporuje	podporovat	k5eAaImIp3nS	podporovat
rozvoj	rozvoj	k1gInSc4	rozvoj
aterosklerózy	ateroskleróza	k1gFnSc2	ateroskleróza
Ateroskleróza	ateroskleróza	k1gFnSc1	ateroskleróza
<g />
.	.	kIx.	.
</s>
<s>
tepen	tepna	k1gFnPc2	tepna
dolních	dolní	k2eAgFnPc2d1	dolní
končetin	končetina	k1gFnPc2	končetina
–	–	k?	–
ischemická	ischemický	k2eAgFnSc1d1	ischemická
choroba	choroba	k1gFnSc1	choroba
dolních	dolní	k2eAgFnPc2d1	dolní
končetin	končetina	k1gFnPc2	končetina
Ateroskleróza	ateroskleróza	k1gFnSc1	ateroskleróza
břišní	břišní	k2eAgFnSc2d1	břišní
aorty	aorta	k1gFnSc2	aorta
–	–	k?	–
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
také	také	k9	také
k	k	k7c3	k
uzávěru	uzávěr	k1gInSc3	uzávěr
renálních	renální	k2eAgFnPc2d1	renální
tepen	tepna	k1gFnPc2	tepna
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
ateromový	ateromový	k2eAgMnSc1d1	ateromový
plát	plát	k5eAaImF	plát
poškozením	poškození	k1gNnSc7	poškození
stěny	stěna	k1gFnSc2	stěna
tepny	tepna	k1gFnSc2	tepna
a	a	k8xC	a
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
následujícím	následující	k2eAgFnPc3d1	následující
komplikacím	komplikace	k1gFnPc3	komplikace
<g/>
:	:	kIx,	:
Trombotizace	Trombotizace	k1gFnSc1	Trombotizace
–	–	k?	–
na	na	k7c6	na
plátu	plát	k1gInSc6	plát
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
krevní	krevní	k2eAgFnSc2d1	krevní
sraženiny	sraženina	k1gFnSc2	sraženina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
omezovat	omezovat	k5eAaImF	omezovat
krevní	krevní	k2eAgInSc4d1	krevní
<g />
.	.	kIx.	.
</s>
<s>
tok	tok	k1gInSc1	tok
–	–	k?	–
trombóza	trombóza	k1gFnSc1	trombóza
Embolizace	embolizace	k1gFnSc1	embolizace
–	–	k?	–
krevní	krevní	k2eAgFnSc1d1	krevní
sraženina	sraženina	k1gFnSc1	sraženina
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
utrhnout	utrhnout	k5eAaPmF	utrhnout
a	a	k8xC	a
být	být	k5eAaImF	být
vmetena	vmeten	k2eAgFnSc1d1	vmetena
(	(	kIx(	(
<g/>
embolována	embolován	k2eAgFnSc1d1	embolován
<g/>
)	)	kIx)	)
do	do	k7c2	do
vzdáleného	vzdálený	k2eAgNnSc2d1	vzdálené
místa	místo	k1gNnSc2	místo
–	–	k?	–
embolie	embolie	k1gFnSc1	embolie
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
pohotovost	pohotovost	k1gFnSc4	pohotovost
tepny	tepna	k1gFnSc2	tepna
ke	k	k7c3	k
křečovitým	křečovitý	k2eAgInPc3d1	křečovitý
stahům	stah	k1gInPc3	stah
–	–	k?	–
hraje	hrát	k5eAaImIp3nS	hrát
roli	role	k1gFnSc4	role
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
forem	forma	k1gFnPc2	forma
anginy	angina	k1gFnSc2	angina
pectoris	pectoris	k1gFnSc2	pectoris
Oslabení	oslabení	k1gNnSc1	oslabení
stěny	stěna	k1gFnSc2	stěna
tepny	tepna	k1gFnSc2	tepna
–	–	k?	–
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
výdutím	výduť	k1gFnPc3	výduť
(	(	kIx(	(
<g/>
aneurysmatům	aneurysma	k1gNnPc3	aneurysma
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
k	k	k7c3	k
prasknutí	prasknutí	k1gNnSc3	prasknutí
(	(	kIx(	(
<g/>
ruptuře	ruptura	k1gFnSc6	ruptura
<g/>
)	)	kIx)	)
tepny	tepna	k1gFnSc2	tepna
</s>
