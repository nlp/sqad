<s>
Český	český	k2eAgInSc1d1	český
ráj	ráj	k1gInSc1	ráj
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Böhmisches	Böhmisches	k1gMnSc1	Böhmisches
Paradies	Paradies	k1gMnSc1	Paradies
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
pro	pro	k7c4	pro
území	území	k1gNnSc4	území
ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
Pojizeří	Pojizeří	k1gNnSc6	Pojizeří
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyniká	vynikat	k5eAaImIp3nS	vynikat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
koncentrací	koncentrace	k1gFnSc7	koncentrace
přírodních	přírodní	k2eAgFnPc2d1	přírodní
i	i	k8xC	i
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Český	český	k2eAgInSc1d1	český
ráj	ráj	k1gInSc1	ráj
původně	původně	k6eAd1	původně
označoval	označovat	k5eAaImAgInS	označovat
oblast	oblast	k1gFnSc4	oblast
Litoměřicka	Litoměřicko	k1gNnSc2	Litoměřicko
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Zahrada	zahrada	k1gFnSc1	zahrada
Čech	Čechy	k1gFnPc2	Čechy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
osídlenou	osídlený	k2eAgFnSc4d1	osídlená
německy	německy	k6eAd1	německy
mluvícím	mluvící	k2eAgNnSc7d1	mluvící
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgNnSc1d1	současné
vymezení	vymezení	k1gNnSc1	vymezení
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jeho	jeho	k3xOp3gMnPc1	jeho
autoři	autor	k1gMnPc1	autor
bývají	bývat	k5eAaImIp3nP	bývat
uváděni	uváděn	k2eAgMnPc1d1	uváděn
lázeňští	lázeňský	k2eAgMnPc1d1	lázeňský
hosté	host	k1gMnPc1	host
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
navštěvovali	navštěvovat	k5eAaImAgMnP	navštěvovat
lázně	lázeň	k1gFnPc4	lázeň
Sedmihorky	Sedmihorky	k?	Sedmihorky
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
doložené	doložený	k2eAgNnSc1d1	doložené
použití	použití	k1gNnSc1	použití
však	však	k9	však
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
redaktora	redaktor	k1gMnSc2	redaktor
Václava	Václav	k1gMnSc2	Václav
Durycha	Durych	k1gMnSc2	Durych
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
ležící	ležící	k2eAgNnSc1d1	ležící
zhruba	zhruba	k6eAd1	zhruba
90	[number]	k4	90
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
ohraničeno	ohraničen	k2eAgNnSc1d1	ohraničeno
městy	město	k1gNnPc7	město
<g/>
:	:	kIx,	:
Sobotka	Sobotka	k1gFnSc1	Sobotka
<g/>
,	,	kIx,	,
Mnichovo	mnichův	k2eAgNnSc1d1	Mnichovo
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
,	,	kIx,	,
Sychrov	Sychrov	k1gInSc1	Sychrov
<g/>
,	,	kIx,	,
Frýdštejn	Frýdštejn	k1gInSc1	Frýdštejn
<g/>
,	,	kIx,	,
Železný	železný	k2eAgInSc1d1	železný
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Semily	Semily	k1gInPc1	Semily
<g/>
,	,	kIx,	,
Lomnice	Lomnice	k1gFnPc1	Lomnice
nad	nad	k7c7	nad
Popelkou	Popelka	k1gFnSc7	Popelka
<g/>
,	,	kIx,	,
Železnice	železnice	k1gFnSc1	železnice
a	a	k8xC	a
Jičín	Jičín	k1gInSc1	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
"	"	kIx"	"
<g/>
srdce	srdce	k1gNnSc4	srdce
Českého	český	k2eAgInSc2d1	český
ráje	ráj	k1gInSc2	ráj
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
považován	považován	k2eAgInSc1d1	považován
Turnov	Turnov	k1gInSc1	Turnov
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
dominantami	dominanta	k1gFnPc7	dominanta
kraje	kraj	k1gInSc2	kraj
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
Kozákov	Kozákov	k1gInSc1	Kozákov
a	a	k8xC	a
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
Trosky	troska	k1gFnSc2	troska
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgNnPc1d1	významné
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
skalní	skalní	k2eAgNnPc1d1	skalní
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Prachovské	prachovský	k2eAgFnPc1d1	Prachovská
skály	skála	k1gFnPc1	skála
<g/>
,	,	kIx,	,
Příhrazské	Příhrazský	k2eAgFnPc1d1	Příhrazský
skály	skála	k1gFnPc1	skála
<g/>
,	,	kIx,	,
Hruboskalsko	Hruboskalsko	k1gNnSc1	Hruboskalsko
a	a	k8xC	a
rybníky	rybník	k1gInPc1	rybník
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Žabakor	Žabakor	k1gInSc1	Žabakor
<g/>
,	,	kIx,	,
Komárovský	Komárovský	k2eAgInSc1d1	Komárovský
rybník	rybník	k1gInSc1	rybník
a	a	k8xC	a
rybníky	rybník	k1gInPc1	rybník
v	v	k7c6	v
podtroseckých	podtrosecká	k1gFnPc6	podtrosecká
a	a	k8xC	a
podkosteckých	podkostecký	k2eAgNnPc6d1	podkostecký
údolích	údolí	k1gNnPc6	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
ráj	ráj	k1gInSc1	ráj
je	být	k5eAaImIp3nS	být
také	také	k9	také
název	název	k1gInSc1	název
pro	pro	k7c4	pro
chráněnou	chráněný	k2eAgFnSc4d1	chráněná
krajinnou	krajinný	k2eAgFnSc4d1	krajinná
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
existující	existující	k2eAgFnSc4d1	existující
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgNnPc4	tři
menší	malý	k2eAgNnPc4d2	menší
nespojitá	spojitý	k2eNgNnPc4d1	nespojité
území	území	k1gNnPc4	území
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
větší	veliký	k2eAgFnSc2d2	veliký
pomyslné	pomyslný	k2eAgFnSc2d1	pomyslná
oblasti	oblast	k1gFnSc2	oblast
turistického	turistický	k2eAgInSc2d1	turistický
regionu	region	k1gInSc2	region
Český	český	k2eAgInSc1d1	český
ráj	ráj	k1gInSc1	ráj
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
ráj	ráj	k1gInSc1	ráj
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
několik	několik	k4yIc4	několik
území	území	k1gNnPc2	území
<g/>
:	:	kIx,	:
Severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Turnova	Turnov	k1gInSc2	Turnov
Maloskalsko	Maloskalsko	k1gNnSc1	Maloskalsko
Klokočské	Klokočský	k2eAgFnSc2d1	Klokočský
skály	skála	k1gFnSc2	skála
oblast	oblast	k1gFnSc1	oblast
okolí	okolí	k1gNnSc2	okolí
Kozákova	Kozákův	k2eAgNnSc2d1	Kozákovo
Jihozápadně	jihozápadně	k6eAd1	jihozápadně
až	až	k6eAd1	až
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Turnova	Turnov	k1gInSc2	Turnov
Příhrazské	Příhrazský	k2eAgFnSc2d1	Příhrazský
skály	skála	k1gFnSc2	skála
Hruboskalsko	Hruboskalsko	k1gNnSc4	Hruboskalsko
oblast	oblast	k1gFnSc1	oblast
okolí	okolí	k1gNnSc2	okolí
Trosek	troska	k1gFnPc2	troska
Prachovské	prachovský	k2eAgFnSc2d1	Prachovská
skály	skála	k1gFnSc2	skála
</s>
