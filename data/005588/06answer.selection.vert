<s>
Český	český	k2eAgInSc1d1	český
ráj	ráj	k1gInSc1	ráj
je	být	k5eAaImIp3nS	být
také	také	k9	také
název	název	k1gInSc1	název
pro	pro	k7c4	pro
chráněnou	chráněný	k2eAgFnSc4d1	chráněná
krajinnou	krajinný	k2eAgFnSc4d1	krajinná
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
existující	existující	k2eAgFnSc4d1	existující
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgNnPc4	tři
menší	malý	k2eAgNnPc4d2	menší
nespojitá	spojitý	k2eNgNnPc4d1	nespojité
území	území	k1gNnPc4	území
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
větší	veliký	k2eAgFnSc2d2	veliký
pomyslné	pomyslný	k2eAgFnSc2d1	pomyslná
oblasti	oblast	k1gFnSc2	oblast
turistického	turistický	k2eAgInSc2d1	turistický
regionu	region	k1gInSc2	region
Český	český	k2eAgInSc1d1	český
ráj	ráj	k1gInSc1	ráj
<g/>
.	.	kIx.	.
</s>
