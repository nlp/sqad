<s>
Hattínské	Hattínský	k2eAgInPc1d1
rohy	roh	k1gInPc1
</s>
<s>
Hattínské	Hattínský	k2eAgInPc1d1
rohy	roh	k1gInPc1
Hattínské	Hattínský	k2eAgInPc1d1
rohy	roh	k1gInPc1
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
326	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Poloha	poloha	k1gFnSc1
Světadíl	světadíl	k1gInSc1
</s>
<s>
Asie	Asie	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
32	#num#	k4
<g/>
°	°	k?
<g/>
48	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
35	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
<g/>
34	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Hattínské	Hattínský	k2eAgInPc1d1
rohy	roh	k1gInPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hattínské	Hattínský	k2eAgInPc1d1
rohy	roh	k1gInPc1
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
ק	ק	k?
ח	ח	k?
<g/>
,	,	kIx,
arabsky	arabsky	k6eAd1
ق	ق	k?
ح	ح	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vyhaslá	vyhaslý	k2eAgFnSc1d1
sopka	sopka	k1gFnSc1
v	v	k7c6
Dolní	dolní	k2eAgFnSc6d1
Galileji	Galilea	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojmenování	pojmenování	k1gNnPc4
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
bývalé	bývalý	k2eAgFnSc2d1
blízké	blízký	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
Hattín	Hattína	k1gFnPc2
a	a	k8xC
skutečnosti	skutečnost	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
sopka	sopka	k1gFnSc1
má	mít	k5eAaImIp3nS
dva	dva	k4xCgInPc4
viditelné	viditelný	k2eAgInPc4d1
vrcholy	vrchol	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hattínské	Hattínský	k2eAgInPc1d1
rohy	roh	k1gInPc1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
na	na	k7c6
Ježíšově	Ježíšův	k2eAgFnSc6d1
stezce	stezka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
Hattínských	Hattínský	k2eAgInPc2d1
rohů	roh	k1gInPc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
326	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1187	#num#	k4
na	na	k7c6
Hattínských	Hattínský	k2eAgInPc6d1
rozích	roh	k1gInPc6
proběhla	proběhnout	k5eAaPmAgFnS
poslední	poslední	k2eAgFnSc1d1
fáze	fáze	k1gFnSc1
bitvy	bitva	k1gFnSc2
u	u	k7c2
Hattínu	Hattín	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
důsledku	důsledek	k1gInSc6
padl	padnout	k5eAaPmAgInS,k5eAaImAgInS
Jeruzalém	Jeruzalém	k1gInSc1
opět	opět	k6eAd1
do	do	k7c2
rukou	ruka	k1gFnPc2
muslimů	muslim	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hattínské	Hattínský	k2eAgInPc1d1
rohy	roh	k1gInPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
316601920	#num#	k4
</s>
