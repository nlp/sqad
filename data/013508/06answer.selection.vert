<s>
Brněnský	brněnský	k2eAgInSc1d1	brněnský
orloj	orloj	k1gInSc1	orloj
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
kamenná	kamenný	k2eAgFnSc1d1	kamenná
plastika	plastika	k1gFnSc1	plastika
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
projektilu	projektil	k1gInSc2	projektil
s	s	k7c7	s
hodinovým	hodinový	k2eAgInSc7d1	hodinový
strojem	stroj	k1gInSc7	stroj
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
,	,	kIx,	,
vypouštějící	vypouštějící	k2eAgFnPc1d1	vypouštějící
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
v	v	k7c4	v
11.00	[number]	k4	11.00
hodin	hodina	k1gFnPc2	hodina
kuličku	kulička	k1gFnSc4	kulička
<g/>
.	.	kIx.	.
</s>
