<s>
Radnice	radnice	k1gFnSc1
v	v	k7c6
Zákupech	zákup	k1gInPc6
je	být	k5eAaImIp3nS
budova	budova	k1gFnSc1
na	na	k7c6
severní	severní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
náměstí	náměstí	k1gNnSc2
Svobody	svoboda	k1gFnSc2
v	v	k7c6
pseudogotickém	pseudogotický	k2eAgInSc6d1
slohu	sloh	k1gInSc6
postavená	postavený	k2eAgFnSc1d1
v	v	k7c6
letech	let	k1gInPc6
1866	#num#	k4
<g/>
–	–	k?
<g/>
1867	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
svému	svůj	k3xOyFgInSc3
účelu	účel	k1gInSc3
ve	v	k7c6
středu	střed	k1gInSc6
města	město	k1gNnSc2
Zákupy	zákup	k1gInPc1
slouží	sloužit	k5eAaImIp3nP
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>