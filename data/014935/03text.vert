<s>
Zákupská	Zákupský	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
</s>
<s>
Radnice	radnice	k1gFnSc1
v	v	k7c6
Zákupech	zákup	k1gInPc6
Radnice	radnice	k1gFnSc2
v	v	k7c6
Zákupech	zákup	k1gInPc6
(	(	kIx(
<g/>
listopad	listopad	k1gInSc1
2009	#num#	k4
<g/>
)	)	kIx)
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Sloh	sloha	k1gFnPc2
</s>
<s>
Novogotika	novogotika	k1gFnSc1
<g/>
,	,	kIx,
novorenesance	novorenesance	k1gFnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1866	#num#	k4
<g/>
–	–	k?
<g/>
1867	#num#	k4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1884	#num#	k4
novorenesanční	novorenesanční	k2eAgFnSc1d1
přestavba	přestavba	k1gFnSc1
<g/>
)	)	kIx)
Stavebník	stavebník	k1gMnSc1
</s>
<s>
město	město	k1gNnSc1
Zákupy	zákup	k1gInPc4
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Zákupy	zákup	k1gInPc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
41	#num#	k4
<g/>
′	′	k?
<g/>
5,09	5,09	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
38	#num#	k4
<g/>
′	′	k?
<g/>
42,95	42,95	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Radnice	radnice	k1gFnSc1
v	v	k7c6
Zákupech	zákup	k1gInPc6
je	být	k5eAaImIp3nS
budova	budova	k1gFnSc1
na	na	k7c6
severní	severní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
náměstí	náměstí	k1gNnSc2
Svobody	svoboda	k1gFnSc2
v	v	k7c6
pseudogotickém	pseudogotický	k2eAgInSc6d1
slohu	sloh	k1gInSc6
postavená	postavený	k2eAgFnSc1d1
v	v	k7c6
letech	let	k1gInPc6
1866	#num#	k4
<g/>
–	–	k?
<g/>
1867	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
svému	svůj	k3xOyFgInSc3
účelu	účel	k1gInSc3
ve	v	k7c6
středu	střed	k1gInSc6
města	město	k1gNnSc2
Zákupy	zákup	k1gInPc1
slouží	sloužit	k5eAaImIp3nP
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
městské	městský	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
</s>
<s>
Náměstí	náměstí	k1gNnSc1
Svobody	svoboda	k1gFnSc2
<g/>
:	:	kIx,
bílá	bílý	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
<g/>
,	,	kIx,
vlevo	vlevo	k6eAd1
věžička	věžička	k1gFnSc1
hotelu	hotel	k1gInSc2
Orlík	Orlík	k1gInSc4
<g/>
,	,	kIx,
zcela	zcela	k6eAd1
vpravo	vpravo	k6eAd1
část	část	k1gFnSc1
sousoší	sousoší	k1gNnSc2
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
</s>
<s>
Částečně	částečně	k6eAd1
hrázděný	hrázděný	k2eAgInSc1d1
<g/>
,	,	kIx,
roubený	roubený	k2eAgInSc1d1
dům	dům	k1gInSc1
původní	původní	k2eAgFnSc2d1
radnice	radnice	k1gFnSc2
stával	stávat	k5eAaImAgInS
na	na	k7c6
stejném	stejný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
jej	on	k3xPp3gInSc4
později	pozdě	k6eAd2
nahradila	nahradit	k5eAaPmAgFnS
stavba	stavba	k1gFnSc1
nová	nový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zachoval	Zachoval	k1gMnSc1
se	se	k3xPyFc4
z	z	k7c2
něj	on	k3xPp3gNnSc2
zvon	zvon	k1gInSc1
<g/>
,	,	kIx,
přenesený	přenesený	k2eAgInSc1d1
na	na	k7c4
radnici	radnice	k1gFnSc4
novou	nový	k2eAgFnSc4d1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
odlit	odlít	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1589	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
zvonařem	zvonař	k1gMnSc7
Jakubem	Jakub	k1gMnSc7
v	v	k7c6
Mladé	mladý	k2eAgFnSc6d1
Boleslavi	Boleslaev	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nápis	nápis	k1gInSc1
o	o	k7c6
jeho	jeho	k3xOp3gFnSc6
výrobě	výroba	k1gFnSc6
byl	být	k5eAaImAgInS
napsaný	napsaný	k2eAgInSc1d1
česky	česky	k6eAd1
a	a	k8xC
pod	pod	k7c7
ním	on	k3xPp3gInSc7
jsou	být	k5eAaImIp3nP
psána	psát	k5eAaImNgNnP
německy	německy	k6eAd1
jména	jméno	k1gNnSc2
zákupských	zákupský	k2eAgFnPc2d1
radních	radní	k1gFnPc2
z	z	k7c2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dům	dům	k1gInSc1
původně	původně	k6eAd1
městu	město	k1gNnSc3
nepatřil	patřit	k5eNaImAgInS
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
obecní	obecní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
městečka	městečko	k1gNnSc2
Zákupy	zákup	k1gInPc4
se	se	k3xPyFc4
na	na	k7c6
své	svůj	k3xOyFgFnSc6
schůzi	schůze	k1gFnSc6
28	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1849	#num#	k4
rozhodla	rozhodnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
dům	dům	k1gInSc1
vykoupí	vykoupit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
téže	tenže	k3xDgFnSc6,k3xTgFnSc6
schůzi	schůze	k1gFnSc6
pověřila	pověřit	k5eAaPmAgFnS
jmenovitě	jmenovitě	k6eAd1
osm	osm	k4xCc4
měšťanů	měšťan	k1gMnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
vše	všechen	k3xTgNnSc4
projednali	projednat	k5eAaPmAgMnP
a	a	k8xC
uzavřeli	uzavřít	k5eAaPmAgMnP
s	s	k7c7
předešlým	předešlý	k2eAgMnSc7d1
majitelem	majitel	k1gMnSc7
kupní	kupní	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
stavbou	stavba	k1gFnSc7
radnice	radnice	k1gFnSc2
nové	nový	k2eAgFnSc2d1
</s>
<s>
Znak	znak	k1gInSc1
města	město	k1gNnSc2
na	na	k7c6
rohu	roh	k1gInSc6
budovy	budova	k1gFnSc2
radnice	radnice	k1gFnSc2
</s>
<s>
Později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
veřejná	veřejný	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
<g/>
,	,	kIx,
z	z	k7c2
jejíhož	jejíž	k3xOyRp3gInSc2
výtěžku	výtěžek	k1gInSc2
se	se	k3xPyFc4
měla	mít	k5eAaImAgFnS
postavit	postavit	k5eAaPmF
radnice	radnice	k1gFnSc1
nová	nový	k2eAgFnSc1d1
<g/>
,	,	kIx,
zděná	zděný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
záznamů	záznam	k1gInPc2
radní	radní	k2eAgFnSc2d1
Strohmerové	Strohmerová	k1gFnSc2
v	v	k7c6
červenci	červenec	k1gInSc6
1865	#num#	k4
přednesli	přednést	k5eAaPmAgMnP
na	na	k7c4
schůzi	schůze	k1gFnSc4
zprávu	zpráva	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
zdůvodnili	zdůvodnit	k5eAaPmAgMnP
potřebu	potřeba	k1gFnSc4
nevyhovující	vyhovující	k2eNgFnSc4d1
starou	starý	k2eAgFnSc4d1
radnici	radnice	k1gFnSc4
nahradit	nahradit	k5eAaPmF
budovou	budova	k1gFnSc7
novou	nový	k2eAgFnSc7d1
<g/>
,	,	kIx,
s	s	k7c7
dostatečnými	dostatečný	k2eAgFnPc7d1
kancelářskými	kancelářský	k2eAgFnPc7d1
prostorami	prostora	k1gFnPc7
a	a	k8xC
bez	bez	k7c2
akutního	akutní	k2eAgNnSc2d1
nebezpečí	nebezpečí	k1gNnSc2
požáru	požár	k1gInSc2
ve	v	k7c6
stávající	stávající	k2eAgFnSc6d1
dřevěné	dřevěný	k2eAgFnSc6d1
budově	budova	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záměr	záměr	k1gInSc1
byl	být	k5eAaImAgInS
stvrzen	stvrdit	k5eAaPmNgInS
hlasováním	hlasování	k1gNnSc7
v	v	k7c6
poměru	poměr	k1gInSc6
15	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
hlasu	hlas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgInS
pětičlenný	pětičlenný	k2eAgInSc1d1
výbor	výbor	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
měl	mít	k5eAaImAgInS
stavbu	stavba	k1gFnSc4
připravit	připravit	k5eAaPmF
a	a	k8xC
byla	být	k5eAaImAgFnS
předána	předat	k5eAaPmNgFnS
žádost	žádost	k1gFnSc1
finanční	finanční	k2eAgFnSc1d1
příspěvek	příspěvek	k1gInSc4
excísaři	excísař	k1gMnSc3
Ferdinandovi	Ferdinand	k1gMnSc3
V.	V.	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
měl	mít	k5eAaImAgInS
na	na	k7c6
zákupském	zákupský	k2eAgInSc6d1
zámku	zámek	k1gInSc6
své	svůj	k3xOyFgNnSc4
letní	letní	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
žádosti	žádost	k1gFnSc3
vyhověl	vyhovět	k5eAaPmAgMnS
a	a	k8xC
radním	radní	k2eAgMnPc3d1
poslal	poslat	k5eAaPmAgMnS
5	#num#	k4
000	#num#	k4
zlatých	zlatá	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Veřejná	veřejný	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
nakonec	nakonec	k6eAd1
stavbu	stavba	k1gFnSc4
umožnila	umožnit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plánek	plánek	k1gInSc1
na	na	k7c4
stavbu	stavba	k1gFnSc4
vypracoval	vypracovat	k5eAaPmAgMnS
na	na	k7c6
jaře	jaro	k1gNnSc6
1866	#num#	k4
Josef	Josef	k1gMnSc1
Goldbach	Goldbach	k1gMnSc1
z	z	k7c2
Nového	Nového	k2eAgInSc2d1
Zámku	zámek	k1gInSc2
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
obec	obec	k1gFnSc1
Zahrádky	zahrádka	k1gFnSc2
u	u	k7c2
České	český	k2eAgFnSc2d1
Lípy	lípa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nová	nový	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
</s>
<s>
Na	na	k7c6
jaře	jaro	k1gNnSc6
1866	#num#	k4
byla	být	k5eAaImAgFnS
přestavba	přestavba	k1gFnSc1
zahájena	zahájit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zachovala	zachovat	k5eAaPmAgFnS
se	se	k3xPyFc4
zpráva	zpráva	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
věži	věž	k1gFnSc6
původní	původní	k2eAgFnSc2d1
radnice	radnice	k1gFnSc2
byl	být	k5eAaImAgInS
zabodnutý	zabodnutý	k2eAgInSc1d1
baskirský	baskirský	k2eAgInSc1d1
šíp	šíp	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1813	#num#	k4
a	a	k8xC
při	při	k7c6
přestavbě	přestavba	k1gFnSc6
se	se	k3xPyFc4
ztratil	ztratit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
základech	základ	k1gInPc6
byl	být	k5eAaImAgInS
zachován	zachován	k2eAgInSc1d1
kámen	kámen	k1gInSc1
z	z	k7c2
původní	původní	k2eAgFnSc2d1
barokní	barokní	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
s	s	k7c7
vyrytým	vyrytý	k2eAgNnSc7d1
datem	datum	k1gNnSc7
1417	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
konce	konec	k1gInSc2
příštího	příští	k2eAgInSc2d1
roku	rok	k1gInSc2
1867	#num#	k4
byla	být	k5eAaImAgFnS
nová	nový	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
hotová	hotový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náklady	náklad	k1gInPc1
na	na	k7c4
stavbu	stavba	k1gFnSc4
dosáhly	dosáhnout	k5eAaPmAgInP
19	#num#	k4
530	#num#	k4
zlatých	zlatá	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavnostní	slavnostní	k2eAgInSc4d1
otevření	otevření	k1gNnSc1
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
23	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1867	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pozdější	pozdní	k2eAgFnPc1d2
přestavby	přestavba	k1gFnPc1
</s>
<s>
Zvenčí	zvenčí	k6eAd1
na	na	k7c6
rohu	roh	k1gInSc6
budovy	budova	k1gFnSc2
byl	být	k5eAaImAgInS
zhotoven	zhotovit	k5eAaPmNgInS
dodatečně	dodatečně	k6eAd1
další	další	k2eAgInSc1d1
městský	městský	k2eAgInSc1d1
znak	znak	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budova	budova	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1884	#num#	k4
přestavěna	přestavět	k5eAaPmNgFnS
v	v	k7c6
pseudorenesančním	pseudorenesanční	k2eAgInSc6d1
slohu	sloh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Přístavba	přístavba	k1gFnSc1
pro	pro	k7c4
hasiče	hasič	k1gMnPc4
</s>
<s>
Zadní	zadní	k2eAgFnSc1d1
část	část	k1gFnSc1
radnice	radnice	k1gFnSc1
s	s	k7c7
přístavbou	přístavba	k1gFnSc7
pro	pro	k7c4
hasiče	hasič	k1gMnPc4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
byla	být	k5eAaImAgFnS
zezadu	zezadu	k6eAd1
od	od	k7c2
dvora	dvůr	k1gInSc2
k	k	k7c3
radnici	radnice	k1gFnSc3
svépomocí	svépomoc	k1gFnPc2
přistavěna	přistavět	k5eAaPmNgFnS
přízemní	přízemní	k2eAgFnSc1d1
hasičská	hasičský	k2eAgFnSc1d1
zbrojnice	zbrojnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
uhradilo	uhradit	k5eAaPmAgNnS
stavební	stavební	k2eAgInSc4d1
materiál	materiál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
ji	on	k3xPp3gFnSc4
hasiči	hasič	k1gMnPc7
SDH	SDH	kA
Zákupy	zákup	k1gInPc4
museli	muset	k5eAaImAgMnP
vyklidit	vyklidit	k5eAaPmF
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
městský	městský	k2eAgInSc1d1
úřad	úřad	k1gInSc1
zde	zde	k6eAd1
umístil	umístit	k5eAaPmAgInS
své	svůj	k3xOyFgFnPc4
kanceláře	kancelář	k1gFnPc4
stavebního	stavební	k2eAgInSc2d1
odboru	odbor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hasiči	hasič	k1gMnPc7
pak	pak	k6eAd1
získali	získat	k5eAaPmAgMnP
novou	nový	k2eAgFnSc4d1
zbrojnici	zbrojnice	k1gFnSc4
v	v	k7c6
Gagarinově	Gagarinův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Současný	současný	k2eAgInSc1d1
stav	stav	k1gInSc1
</s>
<s>
Vpředu	vpředu	k6eAd1
hotel	hotel	k1gInSc1
Orlík	Orlík	k1gInSc1
<g/>
,	,	kIx,
za	za	k7c7
ním	on	k3xPp3gMnSc7
radnice	radnice	k1gFnSc1
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1
vchod	vchod	k1gInSc1
a	a	k8xC
tedy	tedy	k9
i	i	k9
adresa	adresa	k1gFnSc1
radnice	radnice	k1gFnSc2
je	být	k5eAaImIp3nS
Borská	Borská	k1gFnSc1
5	#num#	k4
(	(	kIx(
<g/>
kdysi	kdysi	k6eAd1
Schlossgasse	Schlossgass	k1gMnSc2
<g/>
)	)	kIx)
vedoucí	vedoucí	k1gFnSc1
k	k	k7c3
zámku	zámek	k1gInSc3
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
rohovou	rohový	k2eAgFnSc4d1
stavbu	stavba	k1gFnSc4
<g/>
,	,	kIx,
jižní	jižní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
se	s	k7c7
slunečními	sluneční	k2eAgFnPc7d1
hodinami	hodina	k1gFnPc7
je	být	k5eAaImIp3nS
na	na	k7c6
náměstí	náměstí	k1gNnSc6
Svobody	svoboda	k1gFnSc2
(	(	kIx(
<g/>
kdysi	kdysi	k6eAd1
Klášterní	klášterní	k2eAgInSc1d1
náměstí-Markplatz	náměstí-Markplatz	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
budově	budova	k1gFnSc6
mají	mít	k5eAaImIp3nP
své	svůj	k3xOyFgNnSc4
sídlo	sídlo	k1gNnSc4
jsou	být	k5eAaImIp3nP
všichni	všechen	k3xTgMnPc1
městští	městský	k2eAgMnPc1d1
úředníci	úředník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přízemí	přízemí	k1gNnSc6
je	být	k5eAaImIp3nS
městské	městský	k2eAgNnSc1d1
informační	informační	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
a	a	k8xC
místnost	místnost	k1gFnSc1
městských	městský	k2eAgMnPc2d1
strážníků	strážník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
několika	několik	k4yIc6
městských	městský	k2eAgFnPc6d1
slavnostech	slavnost	k1gFnPc6
byly	být	k5eAaImAgInP
zpřístupněny	zpřístupněn	k2eAgInPc1d1
i	i	k8xC
sklepy	sklep	k1gInPc1
s	s	k7c7
výstavkou	výstavka	k1gFnSc7
maleb	malba	k1gFnPc2
a	a	k8xC
fotografií	fotografia	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Starostové	Starosta	k1gMnPc1
a	a	k8xC
předsedové	předseda	k1gMnPc1
MNV	MNV	kA
</s>
<s>
???	???	k?
-	-	kIx~
1895	#num#	k4
-	-	kIx~
???	???	k?
-	-	kIx~
Vincenc	Vincenc	k1gMnSc1
Strohmer	Strohmer	k1gMnSc1
<g/>
,	,	kIx,
starosta	starosta	k1gMnSc1
</s>
<s>
1920	#num#	k4
<g/>
–	–	k?
<g/>
1928	#num#	k4
–	–	k?
Eduard	Eduard	k1gMnSc1
Held	Held	k1gMnSc1
<g/>
,	,	kIx,
starosta	starosta	k1gMnSc1
</s>
<s>
1928-1938	1928-1938	k4
-	-	kIx~
Eduard	Eduard	k1gMnSc1
Held	Held	k1gMnSc1
junior	junior	k1gMnSc1
<g/>
,	,	kIx,
starosta	starosta	k1gMnSc1
</s>
<s>
1938	#num#	k4
-	-	kIx~
???	???	k?
-	-	kIx~
Rudolf	Rudolf	k1gMnSc1
Löfler	Löfler	k1gMnSc1
</s>
<s>
???	???	k?
-	-	kIx~
1945	#num#	k4
-	-	kIx~
Reinhold	Reinhold	k1gInSc1
Struy	Strua	k1gFnSc2
</s>
<s>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
–	–	k?
Rudolf	Rudolf	k1gMnSc1
Kalina	Kalina	k1gMnSc1
,	,	kIx,
předseda	předseda	k1gMnSc1
MNV	MNV	kA
</s>
<s>
1946	#num#	k4
-	-	kIx~
Ladislav	Ladislav	k1gMnSc1
Pech	Pech	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
MěNV	MěNV	k1gMnSc1
</s>
<s>
1990	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
–	–	k?
Ing.	ing.	kA
Zdeněk	Zdeněk	k1gMnSc1
Patočka	Patočka	k1gMnSc1
<g/>
,	,	kIx,
starosta	starosta	k1gMnSc1
</s>
<s>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
–	–	k?
Rudolf	Rudolf	k1gMnSc1
Kučera	Kučera	k1gMnSc1
<g/>
,	,	kIx,
starosta	starosta	k1gMnSc1
</s>
<s>
2004	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
–	–	k?
Miloslava	Miloslava	k1gFnSc1
Hudaková	Hudakový	k2eAgFnSc1d1
<g/>
,	,	kIx,
starostka	starostka	k1gFnSc1
</s>
<s>
od	od	k7c2
2010	#num#	k4
–	–	k?
Ing.	ing.	kA
Radek	Radek	k1gMnSc1
Lípa	lípa	k1gFnSc1
<g/>
,	,	kIx,
starosta	starosta	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ŠIMEK	Šimek	k1gMnSc1
MGR	Mgr	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
RYDYGR	RYDYGR	kA
ING	ing	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
;	;	kIx,
KIRSCHNER	KIRSCHNER	kA
<g/>
,	,	kIx,
Bernard	Bernard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákupy	zákup	k1gInPc4
a	a	k8xC
okolí	okolí	k1gNnSc4
na	na	k7c6
starých	starý	k2eAgFnPc6d1
pohlednicích	pohlednice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hostivice	Hostivice	k1gFnSc1
<g/>
:	:	kIx,
Mgr	Mgr	kA
Petr	Petr	k1gMnSc1
Prášil	Prášil	k1gMnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86914	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
65	#num#	k4
<g/>
,	,	kIx,
68	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŠIMEK	Šimek	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povídání	povídání	k1gNnPc1
o	o	k7c6
Zákupech	zákup	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákupy	zákup	k1gInPc4
<g/>
:	:	kIx,
Město	město	k1gNnSc1
Zákupy	zákup	k1gInPc7
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
239	#num#	k4
<g/>
-	-	kIx~
<g/>
4495	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Od	od	k7c2
roku	rok	k1gInSc2
1848	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
32	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zákupy	zákup	k1gInPc7
a	a	k8xC
okolí	okolí	k1gNnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
11	#num#	k4
↑	↑	k?
RŮŽIČKA	Růžička	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Českolipsko	Českolipsko	k1gNnSc1
do	do	k7c2
kapsy	kapsa	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
KMa	KMa	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o	o	k7c4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7309	#num#	k4
<g/>
-	-	kIx~
<g/>
488	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Zákupy	zákup	k1gInPc4
<g/>
,	,	kIx,
s.	s.	k?
200	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Povídání	povídání	k1gNnSc1
o	o	k7c6
Zákupech	zákup	k1gInPc6
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
32	#num#	k4
<g/>
↑	↑	k?
HORSKÝ	Horský	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
SDH	SDH	kA
Zákupy	zákup	k1gInPc4
1850	#num#	k4
<g/>
-	-	kIx~
<g/>
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákupy	zákup	k1gInPc4
<g/>
:	:	kIx,
SDH	SDH	kA
Zákupy	zákup	k1gInPc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Historie	historie	k1gFnSc1
hasičská	hasičský	k2eAgFnSc1d1
<g/>
,	,	kIx,
s.	s.	k?
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Zákupská	Zákupský	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Web	web	k1gInSc1
Českolipsko	Českolipsko	k1gNnSc1
</s>
