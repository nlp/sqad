<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
vůči	vůči	k7c3	vůči
hladině	hladina	k1gFnSc3	hladina
moře	moře	k1gNnSc2	moře
nejhlubší	hluboký	k2eAgNnSc4d3	nejhlubší
místo	místo	k1gNnSc4	místo
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
podle	podle	k7c2	podle
posledního	poslední	k2eAgNnSc2d1	poslední
měření	měření	k1gNnSc2	měření
činí	činit	k5eAaImIp3nS	činit
10	[number]	k4	10
994	[number]	k4	994
m	m	kA	m
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
uváděno	uvádět	k5eAaImNgNnS	uvádět
10	[number]	k4	10
911	[number]	k4	911
m	m	kA	m
až	až	k9	až
11	[number]	k4	11
034	[number]	k4	034
m	m	kA	m
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
severního	severní	k2eAgInSc2d1	severní
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
