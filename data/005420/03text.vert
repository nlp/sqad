<s>
Marianský	Marianský	k2eAgInSc1d1	Marianský
příkop	příkop	k1gInSc1	příkop
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
2550	[number]	k4	2550
km	km	kA	km
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
a	a	k8xC	a
průměrně	průměrně	k6eAd1	průměrně
69	[number]	k4	69
km	km	kA	km
široké	široký	k2eAgNnSc4d1	široké
podmořské	podmořský	k2eAgNnSc4d1	podmořské
údolí	údolí	k1gNnSc4	údolí
tektonického	tektonický	k2eAgInSc2d1	tektonický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
východně	východně	k6eAd1	východně
od	od	k7c2	od
souostroví	souostroví	k1gNnSc2	souostroví
Mariany	Mariana	k1gFnSc2	Mariana
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
ostrova	ostrov	k1gInSc2	ostrov
Guam	Guama	k1gFnPc2	Guama
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
vůči	vůči	k7c3	vůči
hladině	hladina	k1gFnSc3	hladina
moře	moře	k1gNnSc2	moře
nejhlubší	hluboký	k2eAgNnSc4d3	nejhlubší
místo	místo	k1gNnSc4	místo
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
podle	podle	k7c2	podle
posledního	poslední	k2eAgNnSc2d1	poslední
měření	měření	k1gNnSc2	měření
činí	činit	k5eAaImIp3nS	činit
10	[number]	k4	10
994	[number]	k4	994
m	m	kA	m
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
uváděno	uvádět	k5eAaImNgNnS	uvádět
10	[number]	k4	10
911	[number]	k4	911
m	m	kA	m
až	až	k9	až
11	[number]	k4	11
034	[number]	k4	034
m	m	kA	m
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
severního	severní	k2eAgInSc2d1	severní
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejhlubší	hluboký	k2eAgInSc4d3	nejhlubší
známý	známý	k2eAgInSc4d1	známý
podmořský	podmořský	k2eAgInSc4d1	podmořský
příkop	příkop	k1gInSc4	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Nejblíže	blízce	k6eAd3	blízce
ke	k	k7c3	k
středu	střed	k1gInSc3	střed
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
Polární	polární	k2eAgFnSc1d1	polární
hlubokomořská	hlubokomořský	k2eAgFnSc1d1	hlubokomořská
planina	planina	k1gFnSc1	planina
v	v	k7c6	v
Severním	severní	k2eAgInSc6d1	severní
ledovém	ledový	k2eAgInSc6d1	ledový
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Příkop	příkop	k1gInSc1	příkop
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
dvou	dva	k4xCgFnPc2	dva
subdukujících	subdukující	k2eAgFnPc2d1	subdukující
desek	deska	k1gFnPc2	deska
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přesněji	přesně	k6eAd2	přesně
na	na	k7c6	na
subdukci	subdukce	k1gFnSc6	subdukce
Pacifické	pacifický	k2eAgFnSc2d1	Pacifická
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
podsouvá	podsouvat	k5eAaImIp3nS	podsouvat
pod	pod	k7c4	pod
desku	deska	k1gFnSc4	deska
Filipínskou	filipínský	k2eAgFnSc4d1	filipínská
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
hloubka	hloubka	k1gFnSc1	hloubka
příkopu	příkop	k1gInSc2	příkop
byla	být	k5eAaImAgFnS	být
změřena	změřit	k5eAaPmNgFnS	změřit
na	na	k7c4	na
10	[number]	k4	10
994	[number]	k4	994
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
35	[number]	k4	35
798	[number]	k4	798
stop	stopa	k1gFnPc2	stopa
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
má	mít	k5eAaImIp3nS	mít
Mount	Mount	k1gInSc4	Mount
Everest	Everest	k1gInSc4	Everest
nad	nad	k7c7	nad
mořskou	mořský	k2eAgFnSc7d1	mořská
hladinou	hladina	k1gFnSc7	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Příkop	příkop	k1gInSc1	příkop
byl	být	k5eAaImAgInS	být
prvně	prvně	k?	prvně
prozkoumán	prozkoumán	k2eAgInSc1d1	prozkoumán
plavidlem	plavidlo	k1gNnSc7	plavidlo
britského	britský	k2eAgNnSc2d1	Britské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
Challenger	Challengra	k1gFnPc2	Challengra
II	II	kA	II
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
Průzkumná	průzkumný	k2eAgFnSc1d1	průzkumná
loď	loď	k1gFnSc1	loď
dala	dát	k5eAaPmAgFnS	dát
i	i	k9	i
jméno	jméno	k1gNnSc4	jméno
nejhlubší	hluboký	k2eAgFnSc2d3	nejhlubší
části	část	k1gFnSc2	část
příkopu	příkop	k1gInSc2	příkop
tzv.	tzv.	kA	tzv.
Challenger	Challenger	k1gMnSc1	Challenger
Deep	Deep	k1gMnSc1	Deep
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
echo	echo	k1gNnSc1	echo
odrazů	odraz	k1gInPc2	odraz
od	od	k7c2	od
mořského	mořský	k2eAgNnSc2d1	mořské
dna	dno	k1gNnSc2	dno
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
hloubka	hloubka	k1gFnSc1	hloubka
na	na	k7c4	na
10	[number]	k4	10
900	[number]	k4	900
metrů	metr	k1gInPc2	metr
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
.	.	kIx.	.
</s>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
pomocí	pomocí	k7c2	pomocí
výpočtu	výpočet	k1gInSc2	výpočet
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
odraz	odraz	k1gInSc1	odraz
vrátil	vrátit	k5eAaPmAgInS	vrátit
od	od	k7c2	od
mořského	mořský	k2eAgNnSc2d1	mořské
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
rychlost	rychlost	k1gFnSc1	rychlost
šíření	šíření	k1gNnSc2	šíření
vlny	vlna	k1gFnSc2	vlna
ve	v	k7c6	v
vodním	vodní	k2eAgNnSc6d1	vodní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
hlášení	hlášení	k1gNnSc6	hlášení
se	se	k3xPyFc4	se
ale	ale	k9	ale
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
chyby	chyba	k1gFnSc2	chyba
uvedla	uvést	k5eAaPmAgFnS	uvést
hodnota	hodnota	k1gFnSc1	hodnota
10	[number]	k4	10
863	[number]	k4	863
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Chybu	chyba	k1gFnSc4	chyba
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
ruční	ruční	k2eAgNnSc4d1	ruční
spouštění	spouštění	k1gNnSc4	spouštění
a	a	k8xC	a
zastavení	zastavení	k1gNnSc4	zastavení
stopek	stopka	k1gFnPc2	stopka
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
doby	doba	k1gFnSc2	doba
odrazu	odraz	k1gInSc2	odraz
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nepatrná	nepatrný	k2eAgFnSc1d1	nepatrná
odchylka	odchylka	k1gFnSc1	odchylka
ubrala	ubrat	k5eAaPmAgFnS	ubrat
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
měření	měření	k1gNnSc6	měření
37	[number]	k4	37
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
další	další	k2eAgInSc1d1	další
průzkum	průzkum	k1gInSc1	průzkum
příkopu	příkop	k1gInSc2	příkop
tentokrát	tentokrát	k6eAd1	tentokrát
pomocí	pomocí	k7c2	pomocí
sovětské	sovětský	k2eAgFnSc2d1	sovětská
lodi	loď	k1gFnSc2	loď
Viťaz	Viťaz	k1gInSc1	Viťaz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
naměřil	naměřit	k5eAaBmAgInS	naměřit
hloubku	hloubka	k1gFnSc4	hloubka
11	[number]	k4	11
034	[number]	k4	034
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Mariana	Marian	k1gMnSc2	Marian
Hollow	Hollow	k1gMnSc2	Hollow
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
měření	měření	k1gNnSc1	měření
ale	ale	k9	ale
nebylo	být	k5eNaImAgNnS	být
znovu	znovu	k6eAd1	znovu
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
loď	loď	k1gFnSc1	loď
Spencer	Spencra	k1gFnPc2	Spencra
F.	F.	kA	F.
Baird	Baird	k1gMnSc1	Baird
určila	určit	k5eAaPmAgFnS	určit
největší	veliký	k2eAgFnSc4d3	veliký
hloubku	hloubka	k1gFnSc4	hloubka
na	na	k7c4	na
10	[number]	k4	10
915	[number]	k4	915
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
pak	pak	k6eAd1	pak
japonská	japonský	k2eAgFnSc1d1	japonská
výzkumná	výzkumný	k2eAgFnSc1d1	výzkumná
loď	loď	k1gFnSc1	loď
Takujó	Takujó	k1gFnSc2	Takujó
(	(	kIx(	(
<g/>
拓	拓	k?	拓
<g/>
)	)	kIx)	)
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
na	na	k7c4	na
hloubkový	hloubkový	k2eAgInSc4d1	hloubkový
průzkum	průzkum	k1gInSc4	průzkum
určila	určit	k5eAaPmAgFnS	určit
maximální	maximální	k2eAgFnSc4d1	maximální
hloubku	hloubka	k1gFnSc4	hloubka
na	na	k7c4	na
11	[number]	k4	11
0	[number]	k4	0
<g/>
40,4	[number]	k4	40,4
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
často	často	k6eAd1	často
interpretuje	interpretovat	k5eAaBmIp3nS	interpretovat
jako	jako	k9	jako
10	[number]	k4	10
920	[number]	k4	920
<g/>
±	±	k?	±
<g/>
10	[number]	k4	10
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
japonská	japonský	k2eAgFnSc1d1	japonská
výzkumná	výzkumný	k2eAgFnSc1d1	výzkumná
loď	loď	k1gFnSc1	loď
určila	určit	k5eAaPmAgFnS	určit
pomocí	pomocí	k7c2	pomocí
dálkově	dálkově	k6eAd1	dálkově
ovládané	ovládaný	k2eAgFnSc2d1	ovládaná
ponorky	ponorka	k1gFnSc2	ponorka
Kaikó	Kaikó	k1gFnSc2	Kaikó
(	(	kIx(	(
<g/>
か	か	k?	か
<g/>
)	)	kIx)	)
maximální	maximální	k2eAgFnSc4d1	maximální
hloubku	hloubka	k1gFnSc4	hloubka
na	na	k7c4	na
10	[number]	k4	10
911	[number]	k4	911
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Samostatnou	samostatný	k2eAgFnSc7d1	samostatná
kapitolou	kapitola	k1gFnSc7	kapitola
byl	být	k5eAaImAgInS	být
průzkum	průzkum	k1gInSc1	průzkum
příkopu	příkop	k1gInSc2	příkop
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgMnSc6	jenž
americký	americký	k2eAgMnSc1d1	americký
poručík	poručík	k1gMnSc1	poručík
Don	Don	k1gMnSc1	Don
Walsh	Walsh	k1gMnSc1	Walsh
a	a	k8xC	a
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
oceánolog	oceánolog	k1gMnSc1	oceánolog
Jacques	Jacques	k1gMnSc1	Jacques
Piccard	Piccard	k1gMnSc1	Piccard
pomocí	pomocí	k7c2	pomocí
batyskafu	batyskaf	k1gInSc2	batyskaf
US	US	kA	US
Navy	Navy	k?	Navy
Trieste	Triest	k1gInSc5	Triest
prozkoumali	prozkoumat	k5eAaPmAgMnP	prozkoumat
dno	dno	k1gNnSc4	dno
příkopu	příkop	k1gInSc2	příkop
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1960	[number]	k4	1960
v	v	k7c4	v
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
dna	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Hloubkoměr	hloubkoměr	k1gInSc1	hloubkoměr
hlásil	hlásit	k5eAaImAgInS	hlásit
hloubku	hloubka	k1gFnSc4	hloubka
11	[number]	k4	11
521	[number]	k4	521
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
ale	ale	k9	ale
později	pozdě	k6eAd2	pozdě
redukováno	redukovat	k5eAaBmNgNnS	redukovat
na	na	k7c4	na
10	[number]	k4	10
916	[number]	k4	916
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
překvapení	překvapení	k1gNnSc3	překvapení
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
objeven	objeven	k2eAgInSc4d1	objeven
život	život	k1gInSc4	život
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
platýsů	platýs	k1gMnPc2	platýs
a	a	k8xC	a
garnátů	garnát	k1gMnPc2	garnát
<g/>
.	.	kIx.	.
</s>
<s>
Naměřený	naměřený	k2eAgInSc1d1	naměřený
tlak	tlak	k1gInSc1	tlak
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
Marianského	Marianský	k2eAgInSc2d1	Marianský
příkopu	příkop	k1gInSc2	příkop
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
1086	[number]	k4	1086
baru	bar	k1gInSc2	bar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
tlaku	tlak	k1gInSc2	tlak
108,6	[number]	k4	108,6
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
1000	[number]	k4	1000
<g/>
x	x	k?	x
většímu	veliký	k2eAgInSc3d2	veliký
tlaku	tlak	k1gInSc3	tlak
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgMnSc7	třetí
návštěvníkem	návštěvník	k1gMnSc7	návštěvník
dna	dno	k1gNnSc2	dno
Marianského	Marianský	k2eAgInSc2d1	Marianský
příkopu	příkop	k1gInSc2	příkop
se	s	k7c7	s
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
stal	stát	k5eAaPmAgMnS	stát
americký	americký	k2eAgMnSc1d1	americký
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
amatérský	amatérský	k2eAgMnSc1d1	amatérský
oceánograf	oceánograf	k1gMnSc1	oceánograf
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
v	v	k7c6	v
batyskafu	batyskaf	k1gInSc6	batyskaf
Deepsea	Deepsea	k1gMnSc1	Deepsea
Challenger	Challenger	k1gMnSc1	Challenger
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
kromě	kromě	k7c2	kromě
průzkumu	průzkum	k1gInSc2	průzkum
také	také	k9	také
natočení	natočení	k1gNnSc4	natočení
unikátních	unikátní	k2eAgInPc2d1	unikátní
záběrů	záběr	k1gInPc2	záběr
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInPc4	jeho
další	další	k2eAgInPc4d1	další
dokumentární	dokumentární	k2eAgInPc4d1	dokumentární
i	i	k8xC	i
hrané	hraný	k2eAgInPc4d1	hraný
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
použití	použití	k1gNnSc3	použití
dvojice	dvojice	k1gFnSc2	dvojice
stereoskopických	stereoskopický	k2eAgFnPc2d1	stereoskopická
kamer	kamera	k1gFnPc2	kamera
na	na	k7c6	na
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
výložnících	výložník	k1gInPc6	výložník
budou	být	k5eAaImBp3nP	být
tyto	tento	k3xDgInPc1	tento
záběry	záběr	k1gInPc1	záběr
dostupné	dostupný	k2eAgInPc1d1	dostupný
ve	v	k7c4	v
3	[number]	k4	3
<g/>
D.	D.	kA	D.
Sestup	sestup	k1gInSc1	sestup
zahájil	zahájit	k5eAaPmAgInS	zahájit
v	v	k7c6	v
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
a	a	k8xC	a
asi	asi	k9	asi
za	za	k7c4	za
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
hloubky	hloubka	k1gFnSc2	hloubka
10	[number]	k4	10
898	[number]	k4	898
m.	m.	k?	m.
Cesta	cesta	k1gFnSc1	cesta
zpět	zpět	k6eAd1	zpět
mu	on	k3xPp3gMnSc3	on
trvala	trvat	k5eAaImAgFnS	trvat
asi	asi	k9	asi
70	[number]	k4	70
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
na	na	k7c4	na
hladinu	hladina	k1gFnSc4	hladina
se	se	k3xPyFc4	se
vynořil	vynořit	k5eAaPmAgInS	vynořit
kolem	kolem	k7c2	kolem
poledne	poledne	k1gNnSc2	poledne
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vědeckofantastickém	vědeckofantastický	k2eAgInSc6d1	vědeckofantastický
filmu	film	k1gInSc6	film
Jádro	jádro	k1gNnSc1	jádro
byl	být	k5eAaImAgInS	být
Marianský	Marianský	k2eAgInSc1d1	Marianský
příkop	příkop	k1gInSc1	příkop
zvolen	zvolit	k5eAaPmNgInS	zvolit
jako	jako	k8xC	jako
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
začíná	začínat	k5eAaImIp3nS	začínat
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
středu	střed	k1gInSc2	střed
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
díle	dílo	k1gNnSc6	dílo
filmové	filmový	k2eAgFnSc2d1	filmová
série	série	k1gFnSc2	série
Transformers	Transformersa	k1gFnPc2	Transformersa
jsou	být	k5eAaImIp3nP	být
zbytky	zbytek	k1gInPc1	zbytek
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
mimozemšťanů	mimozemšťan	k1gMnPc2	mimozemšťan
svrženy	svrhnout	k5eAaPmNgFnP	svrhnout
na	na	k7c4	na
dno	dno	k1gNnSc4	dno
Marianského	Marianský	k2eAgInSc2d1	Marianský
příkopu	příkop	k1gInSc2	příkop
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
lidstvo	lidstvo	k1gNnSc4	lidstvo
nedosažitelné	dosažitelný	k2eNgNnSc4d1	nedosažitelné
<g/>
.	.	kIx.	.
</s>
<s>
Zeměpisné	zeměpisný	k2eAgInPc1d1	zeměpisný
rekordy	rekord	k1gInPc1	rekord
světa	svět	k1gInSc2	svět
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Marianský	Marianský	k2eAgInSc4d1	Marianský
příkop	příkop	k1gInSc4	příkop
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
