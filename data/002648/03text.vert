<s>
Uljanovsk	Uljanovsk	k1gInSc1	Uljanovsk
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
У	У	k?	У
<g/>
́	́	k?	́
<g/>
н	н	k?	н
<g/>
;	;	kIx,	;
čuvašsky	čuvašsky	k6eAd1	čuvašsky
Ч	Ч	k?	Ч
<g/>
;	;	kIx,	;
tatarsky	tatarsky	k6eAd1	tatarsky
Sember	Sember	k1gInSc1	Sember
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Simbirsk	Simbirsk	k1gInSc1	Simbirsk
(	(	kIx(	(
<g/>
С	С	k?	С
<g/>
́	́	k?	́
<g/>
р	р	k?	р
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ruské	ruský	k2eAgNnSc1d1	ruské
město	město	k1gNnSc1	město
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Volze	Volha	k1gFnSc6	Volha
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
630000	[number]	k4	630000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
administrativním	administrativní	k2eAgNnSc7d1	administrativní
centrem	centrum	k1gNnSc7	centrum
Uljanovské	Uljanovský	k2eAgFnSc2d1	Uljanovská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nP	sídlet
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
komplexy	komplex	k1gInPc1	komplex
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
automobilka	automobilka	k1gFnSc1	automobilka
UAZ	UAZ	kA	UAZ
<g/>
.	.	kIx.	.
</s>
<s>
Někdejší	někdejší	k2eAgInSc1d1	někdejší
Simbirsk	Simbirsk	k1gInSc1	Simbirsk
(	(	kIx(	(
<g/>
též	též	k9	též
Sinbirsk	Sinbirsk	k1gInSc1	Sinbirsk
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1648	[number]	k4	1648
jako	jako	k8xS	jako
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1670	[number]	k4	1670
ho	on	k3xPp3gMnSc4	on
obsadily	obsadit	k5eAaPmAgFnP	obsadit
jednotky	jednotka	k1gFnPc1	jednotka
Stěnky	stěnka	k1gFnSc2	stěnka
Razina	Razina	k1gFnSc1	Razina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
sídlem	sídlo	k1gNnSc7	sídlo
Simbirské	Simbirský	k2eAgFnSc2d1	Simbirský
gubernie	gubernie	k1gFnSc2	gubernie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
zde	zde	k6eAd1	zde
zuřil	zuřit	k5eAaImAgInS	zuřit
devítidenní	devítidenní	k2eAgInSc1d1	devítidenní
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
zničil	zničit	k5eAaPmAgInS	zničit
většinu	většina	k1gFnSc4	většina
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
železnice	železnice	k1gFnSc2	železnice
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
rozvoji	rozvoj	k1gInSc3	rozvoj
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
tu	tu	k6eAd1	tu
také	také	k9	také
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
pobývaly	pobývat	k5eAaImAgFnP	pobývat
jednotky	jednotka	k1gFnPc1	jednotka
Československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
Uljanovsk	Uljanovsk	k1gInSc1	Uljanovsk
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
narodil	narodit	k5eAaPmAgMnS	narodit
Vladimir	Vladimir	k1gMnSc1	Vladimir
Iljič	Iljič	k1gMnSc1	Iljič
Uljanov	Uljanov	k1gInSc1	Uljanov
(	(	kIx(	(
<g/>
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Narodili	narodit	k5eAaPmAgMnP	narodit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
Alexandr	Alexandr	k1gMnSc1	Alexandr
Kerenskij	Kerenskij	k1gMnSc1	Kerenskij
a	a	k8xC	a
Ivan	Ivan	k1gMnSc1	Ivan
Gončarov	Gončarov	k1gInSc1	Gončarov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
sídlem	sídlo	k1gNnSc7	sídlo
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
Sergej	Sergej	k1gMnSc1	Sergej
Jermakov	Jermakov	k1gInSc1	Jermakov
návrat	návrat	k1gInSc1	návrat
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
názvu	název	k1gInSc3	název
Simbirsk	Simbirsk	k1gInSc4	Simbirsk
<g/>
.	.	kIx.	.
</s>
