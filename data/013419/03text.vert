<p>
<s>
Eleonora	Eleonora	k1gFnSc1	Eleonora
Kastilská	kastilský	k2eAgFnSc1d1	Kastilská
(	(	kIx(	(
<g/>
katalánsky	katalánsky	k6eAd1	katalánsky
Elionor	Elionor	k1gMnSc1	Elionor
de	de	k?	de
Castell	Castell	k1gMnSc1	Castell
<g/>
,	,	kIx,	,
1202	[number]	k4	1202
<g/>
–	–	k?	–
<g/>
1244	[number]	k4	1244
<g/>
,	,	kIx,	,
Burgos	Burgos	k1gInSc1	Burgos
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
aragonská	aragonský	k2eAgFnSc1d1	Aragonská
královna	královna	k1gFnSc1	královna
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
aragonského	aragonský	k2eAgMnSc2d1	aragonský
krále	král	k1gMnSc2	král
Jakuba	Jakub	k1gMnSc2	Jakub
I.	I.	kA	I.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Eleonora	Eleonora	k1gFnSc1	Eleonora
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
jako	jako	k9	jako
desáté	desátý	k4xOgNnSc4	desátý
dítě	dítě	k1gNnSc4	dítě
kastilského	kastilský	k2eAgMnSc2d1	kastilský
krále	král	k1gMnSc2	král
Alfonse	Alfons	k1gMnSc2	Alfons
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
anglické	anglický	k2eAgFnSc2d1	anglická
princezny	princezna	k1gFnSc2	princezna
Eleonory	Eleonora	k1gFnSc2	Eleonora
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
tedy	tedy	k9	tedy
vnučkou	vnučka	k1gFnSc7	vnučka
slavné	slavný	k2eAgFnSc2d1	slavná
krásky	kráska	k1gFnSc2	kráska
Eleonory	Eleonora	k1gFnSc2	Eleonora
Akvitánské	Akvitánský	k2eAgFnSc2d1	Akvitánská
a	a	k8xC	a
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Plantageneta	Plantagenet	k1gMnSc2	Plantagenet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svatba	svatba	k1gFnSc1	svatba
devatenáctileté	devatenáctiletý	k2eAgFnSc2d1	devatenáctiletá
dívky	dívka	k1gFnSc2	dívka
s	s	k7c7	s
o	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
mladším	mladý	k2eAgMnSc7d2	mladší
aragonským	aragonský	k2eAgMnSc7d1	aragonský
králem	král	k1gMnSc7	král
Jakubem	Jakub	k1gMnSc7	Jakub
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1221	[number]	k4	1221
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1229	[number]	k4	1229
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Alfons	Alfons	k1gMnSc1	Alfons
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
anulování	anulování	k1gNnSc3	anulování
manželství	manželství	k1gNnSc2	manželství
údajně	údajně	k6eAd1	údajně
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
blízkého	blízký	k2eAgNnSc2d1	blízké
příbuzenství	příbuzenství	k1gNnSc2	příbuzenství
manželů	manžel	k1gMnPc2	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgMnSc1d1	jediný
syn	syn	k1gMnSc1	syn
Alfons	Alfons	k1gMnSc1	Alfons
byl	být	k5eAaImAgMnS	být
uznán	uznat	k5eAaPmNgMnS	uznat
legitimním	legitimní	k2eAgMnSc7d1	legitimní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
následníkem	následník	k1gMnSc7	následník
trůnu	trůn	k1gInSc2	trůn
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
jeho	on	k3xPp3gInSc4	on
brzký	brzký	k2eAgInSc4d1	brzký
skon	skon	k1gInSc4	skon
stal	stát	k5eAaPmAgMnS	stát
syn	syn	k1gMnSc1	syn
z	z	k7c2	z
Jakubova	Jakubův	k2eAgNnSc2d1	Jakubovo
nového	nový	k2eAgNnSc2d1	nové
manželství	manželství	k1gNnSc2	manželství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Král	Král	k1gMnSc1	Král
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
dobývat	dobývat	k5eAaImF	dobývat
Mallorku	Mallorka	k1gFnSc4	Mallorka
a	a	k8xC	a
Eleonora	Eleonora	k1gFnSc1	Eleonora
se	se	k3xPyFc4	se
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
do	do	k7c2	do
klášterního	klášterní	k2eAgNnSc2d1	klášterní
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
strávila	strávit	k5eAaPmAgFnS	strávit
po	po	k7c6	po
boku	bok	k1gInSc6	bok
své	svůj	k3xOyFgFnSc2	svůj
starší	starý	k2eAgFnSc2d2	starší
sestry	sestra	k1gFnSc2	sestra
Berenguely	Berenguela	k1gFnSc2	Berenguela
v	v	k7c6	v
cisterciáckém	cisterciácký	k2eAgInSc6d1	cisterciácký
klášteře	klášter	k1gInSc6	klášter
Las	laso	k1gNnPc2	laso
Huelgas	Huelgasa	k1gFnPc2	Huelgasa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Eleonora	Eleonora	k1gFnSc1	Eleonora
Kastilská	kastilský	k2eAgFnSc1d1	Kastilská
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Šaty	šat	k1gInPc1	šat
Eleonory	Eleonora	k1gFnSc2	Eleonora
Kastilské	kastilský	k2eAgInPc1d1	kastilský
</s>
</p>
<p>
<s>
Pelota	pelota	k1gFnSc1	pelota
</s>
</p>
