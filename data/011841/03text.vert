<p>
<s>
Soubor	soubor	k1gInSc1	soubor
Trapné	trapný	k2eAgFnSc2d1	trapná
povídky	povídka	k1gFnSc2	povídka
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
osmi	osm	k4xCc2	osm
povídkových	povídkový	k2eAgFnPc2d1	povídková
próz	próza	k1gFnPc2	próza
autora	autor	k1gMnSc2	autor
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
povídky	povídka	k1gFnPc4	povídka
pojí	pojíst	k5eAaPmIp3nS	pojíst
opět	opět	k6eAd1	opět
filozofické	filozofický	k2eAgNnSc1d1	filozofické
zamyšlení	zamyšlení	k1gNnSc1	zamyšlení
nad	nad	k7c7	nad
dvojí	dvojit	k5eAaImIp3nP	dvojit
tváří	tvář	k1gFnSc7	tvář
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
smutnění	smutnění	k1gNnPc1	smutnění
nad	nad	k7c7	nad
lidským	lidský	k2eAgInSc7d1	lidský
osudem	osud	k1gInSc7	osud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Povídky	povídka	k1gFnPc1	povídka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
Na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
je	být	k5eAaImIp3nS	být
ústřední	ústřední	k2eAgInSc1d1	ústřední
postavou	postava	k1gFnSc7	postava
vychovatelka	vychovatelka	k1gFnSc1	vychovatelka
Olga	Olga	k1gFnSc1	Olga
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
nenalezla	nalézt	k5eNaBmAgFnS	nalézt
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
a	a	k8xC	a
ani	ani	k8xC	ani
hraběcí	hraběcí	k2eAgFnSc3d1	hraběcí
rodině	rodina	k1gFnSc3	rodina
žádné	žádný	k3yNgNnSc4	žádný
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vychovává	vychovávat	k5eAaImIp3nS	vychovávat
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
neposlouchají	poslouchat	k5eNaImIp3nP	poslouchat
a	a	k8xC	a
dělají	dělat	k5eAaImIp3nP	dělat
naschvály	naschvál	k1gInPc1	naschvál
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
dál	daleko	k6eAd2	daleko
žít	žít	k5eAaImF	žít
nemůže	moct	k5eNaImIp3nS	moct
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
do	do	k7c2	do
rodného	rodný	k2eAgInSc2d1	rodný
domu	dům	k1gInSc2	dům
ke	k	k7c3	k
svým	svůj	k3xOyFgMnPc3	svůj
rodičům	rodič	k1gMnPc3	rodič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
chce	chtít	k5eAaImIp3nS	chtít
oznámit	oznámit	k5eAaPmF	oznámit
hraběti	hrabě	k1gMnSc3	hrabě
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
dopis	dopis	k1gInSc4	dopis
od	od	k7c2	od
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
píše	psát	k5eAaImIp3nS	psát
špatnou	špatný	k2eAgFnSc4d1	špatná
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
je	být	k5eAaImIp3nS	být
nemocný	mocný	k2eNgMnSc1d1	nemocný
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc4	její
příjezd	příjezd	k1gInSc4	příjezd
a	a	k8xC	a
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
bez	bez	k7c2	bez
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
špatně	špatně	k6eAd1	špatně
nesl	nést	k5eAaImAgMnS	nést
<g/>
.	.	kIx.	.
</s>
<s>
Olga	Olga	k1gFnSc1	Olga
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nadále	nadále	k6eAd1	nadále
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
vychovatelkou	vychovatelka	k1gFnSc7	vychovatelka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
povídka	povídka	k1gFnSc1	povídka
Otcové	otec	k1gMnPc1	otec
vypráví	vyprávět	k5eAaImIp3nP	vyprávět
o	o	k7c6	o
otcovské	otcovský	k2eAgFnSc6d1	otcovská
lásce	láska	k1gFnSc6	láska
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
chodí	chodit	k5eAaImIp3nS	chodit
pravidelně	pravidelně	k6eAd1	pravidelně
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
na	na	k7c4	na
procházky	procházka	k1gFnPc4	procházka
<g/>
.	.	kIx.	.
</s>
<s>
Dcerka	dcerka	k1gFnSc1	dcerka
však	však	k9	však
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
a	a	k8xC	a
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Otci	otec	k1gMnPc7	otec
na	na	k7c6	na
těžké	těžký	k2eAgFnSc6d1	těžká
situaci	situace	k1gFnSc6	situace
nepřidává	přidávat	k5eNaImIp3nS	přidávat
ani	ani	k8xC	ani
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
jeho	jeho	k3xOp3gNnSc4	jeho
žena	žena	k1gFnSc1	žena
byla	být	k5eAaImAgFnS	být
nevěrná	věrný	k2eNgFnSc1d1	nevěrná
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
nevlastním	vlastní	k2eNgMnSc7d1	nevlastní
otcem	otec	k1gMnSc7	otec
děvčete	děvče	k1gNnSc2	děvče
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
ve	v	k7c6	v
městě	město	k1gNnSc6	město
to	ten	k3xDgNnSc4	ten
vědí	vědět	k5eAaImIp3nP	vědět
a	a	k8xC	a
posmívají	posmívat	k5eAaImIp3nP	posmívat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
dívence	dívenka	k1gFnSc3	dívenka
to	ten	k3xDgNnSc1	ten
však	však	k9	však
nezničí	zničit	k5eNaPmIp3nS	zničit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Surovec	surovec	k1gMnSc1	surovec
je	být	k5eAaImIp3nS	být
povídkou	povídka	k1gFnSc7	povídka
o	o	k7c6	o
továrníkovi	továrník	k1gMnSc6	továrník
Pelikánovi	Pelikán	k1gMnSc6	Pelikán
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
veškerý	veškerý	k3xTgInSc4	veškerý
čas	čas	k1gInSc4	čas
tráví	trávit	k5eAaImIp3nP	trávit
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
doma	doma	k6eAd1	doma
osamocena	osamocen	k2eAgFnSc1d1	osamocena
<g/>
.	.	kIx.	.
</s>
<s>
Paní	paní	k1gFnSc1	paní
Pelikánová	Pelikánová	k1gFnSc1	Pelikánová
se	se	k3xPyFc4	se
jednou	jednou	k6eAd1	jednou
seznámí	seznámit	k5eAaPmIp3nP	seznámit
s	s	k7c7	s
panem	pan	k1gMnSc7	pan
Ježkem	Ježek	k1gMnSc7	Ježek
(	(	kIx(	(
<g/>
Pelikánův	Pelikánův	k2eAgMnSc1d1	Pelikánův
kamarád	kamarád	k1gMnSc1	kamarád
z	z	k7c2	z
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
dvojice	dvojice	k1gFnSc1	dvojice
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Manžel	manžel	k1gMnSc1	manžel
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
doví	dovědět	k5eAaPmIp3nS	dovědět
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
si	se	k3xPyFc3	se
jist	jist	k2eAgMnSc1d1	jist
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
rozvést	rozvést	k5eAaPmF	rozvést
<g/>
,	,	kIx,	,
nechat	nechat	k5eAaPmF	nechat
ji	on	k3xPp3gFnSc4	on
jít	jít	k5eAaImF	jít
za	za	k7c4	za
štěstí	štěstí	k1gNnSc4	štěstí
a	a	k8xC	a
chudobou	chudoba	k1gFnSc7	chudoba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
zůstat	zůstat	k5eAaPmF	zůstat
a	a	k8xC	a
dopřávat	dopřávat	k5eAaImF	dopřávat
ji	on	k3xPp3gFnSc4	on
luxus	luxus	k1gInSc4	luxus
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yRgInSc4	který
byla	být	k5eAaImAgFnS	být
zvyklá	zvyklý	k2eAgFnSc1d1	zvyklá
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
odpustí	odpustit	k5eAaPmIp3nP	odpustit
a	a	k8xC	a
Ježkovi	Ježek	k1gMnSc3	Ježek
zakáže	zakázat	k5eAaPmIp3nS	zakázat
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
stýkat	stýkat	k5eAaImF	stýkat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tato	tento	k3xDgFnSc1	tento
povídka	povídka	k1gFnSc1	povídka
končí	končit	k5eAaImIp3nS	končit
smutkem	smutek	k1gInSc7	smutek
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
paní	paní	k1gFnSc3	paní
Pelikánové	Pelikánová	k1gFnSc2	Pelikánová
a	a	k8xC	a
pana	pan	k1gMnSc2	pan
Ježka	Ježek	k1gMnSc2	Ježek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tři	tři	k4xCgMnPc1	tři
</s>
</p>
<p>
<s>
Helena	Helena	k1gFnSc1	Helena
</s>
</p>
<p>
<s>
Peníze	peníz	k1gInPc1	peníz
</s>
</p>
<p>
<s>
Košile	košile	k1gFnSc1	košile
</s>
</p>
<p>
<s>
Uražený	uražený	k2eAgInSc1d1	uražený
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
převzaty	převzít	k5eAaPmNgFnP	převzít
z	z	k7c2	z
anotace	anotace	k1gFnSc2	anotace
k	k	k7c3	k
dílu	dílo	k1gNnSc3	dílo
Trapné	trapný	k2eAgFnSc2d1	trapná
povídky	povídka	k1gFnSc2	povídka
v	v	k7c6	v
katalogu	katalog	k1gInSc6	katalog
Městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Trapné	trapný	k2eAgFnSc2d1	trapná
povídky	povídka	k1gFnSc2	povídka
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
jazyk	jazyk	k1gInSc1	jazyk
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
na	na	k7c6	na
webu	web	k1gInSc6	web
Městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
