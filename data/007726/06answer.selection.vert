<s>
Nejbližší	blízký	k2eAgNnSc1d3	nejbližší
přiblížení	přiblížení	k1gNnSc1	přiblížení
k	k	k7c3	k
Neptunu	Neptun	k1gInSc3	Neptun
nastalo	nastat	k5eAaPmAgNnS	nastat
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sonda	sonda	k1gFnSc1	sonda
pozorovala	pozorovat	k5eAaImAgFnS	pozorovat
planetu	planeta	k1gFnSc4	planeta
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
