<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
International	International	k1gFnSc1	International
Ice	Ice	k1gFnSc2	Ice
Hockey	Hockea	k1gFnSc2	Hockea
Federation	Federation	k1gInSc1	Federation
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
IIHF	IIHF	kA	IIHF
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
federace	federace	k1gFnSc1	federace
národních	národní	k2eAgFnPc2d1	národní
hokejových	hokejový	k2eAgFnPc2d1	hokejová
asociací	asociace	k1gFnPc2	asociace
řídící	řídící	k2eAgNnSc4d1	řídící
sportovní	sportovní	k2eAgNnSc4d1	sportovní
odvětví	odvětví	k1gNnSc4	odvětví
lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
a	a	k8xC	a
inline	inlinout	k5eAaPmIp3nS	inlinout
hokej	hokej	k1gInSc4	hokej
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
i	i	k8xC	i
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Ligue	Ligu	k1gMnSc2	Ligu
Internationale	Internationale	k1gMnSc2	Internationale
de	de	k?	de
Hockey	Hockea	k1gMnSc2	Hockea
sur	sur	k?	sur
Glace	Glace	k1gMnSc2	Glace
(	(	kIx(	(
<g/>
LIHG	LIHG	kA	LIHG
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
má	mít	k5eAaImIp3nS	mít
74	[number]	k4	74
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
<g/>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
organizuje	organizovat	k5eAaBmIp3nS	organizovat
většinu	většina	k1gFnSc4	většina
významných	významný	k2eAgInPc2d1	významný
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
turnajů	turnaj	k1gInPc2	turnaj
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
hokejové	hokejový	k2eAgNnSc4d1	hokejové
dění	dění	k1gNnSc4	dění
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Evropské	evropský	k2eAgFnPc1d1	Evropská
národní	národní	k2eAgFnPc1d1	národní
ligové	ligový	k2eAgFnPc1d1	ligová
soutěže	soutěž	k1gFnPc1	soutěž
respektují	respektovat	k5eAaImIp3nP	respektovat
hokejová	hokejový	k2eAgNnPc4d1	hokejové
pravidla	pravidlo	k1gNnPc4	pravidlo
stanovená	stanovený	k2eAgNnPc4d1	stanovené
IIHF	IIHF	kA	IIHF
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
organizuje	organizovat	k5eAaBmIp3nS	organizovat
turnaje	turnaj	k1gInPc4	turnaj
pro	pro	k7c4	pro
profesionální	profesionální	k2eAgInPc4d1	profesionální
kluby	klub	k1gInPc4	klub
a	a	k8xC	a
i	i	k9	i
pro	pro	k7c4	pro
národní	národní	k2eAgInPc4d1	národní
výběry	výběr	k1gInPc4	výběr
<g/>
.	.	kIx.	.
</s>
<s>
IIHF	IIHF	kA	IIHF
má	mít	k5eAaImIp3nS	mít
tak	tak	k6eAd1	tak
jistou	jistý	k2eAgFnSc4d1	jistá
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
pravomoc	pravomoc	k1gFnSc4	pravomoc
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
udělování	udělování	k1gNnSc4	udělování
trestů	trest	k1gInPc2	trest
pro	pro	k7c4	pro
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
vydávání	vydávání	k1gNnSc4	vydávání
celoevropských	celoevropský	k2eAgInPc2d1	celoevropský
zákazů	zákaz	k1gInPc2	zákaz
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
IIHF	IIHF	kA	IIHF
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
hráč	hráč	k1gMnSc1	hráč
odvolat	odvolat	k5eAaPmF	odvolat
na	na	k7c4	na
Sportovní	sportovní	k2eAgInSc4d1	sportovní
arbitrážní	arbitrážní	k2eAgInSc4d1	arbitrážní
soud	soud	k1gInSc4	soud
v	v	k7c6	v
Lausanne	Lausanne	k1gNnSc6	Lausanne
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IIHF	IIHF	kA	IIHF
není	být	k5eNaImIp3nS	být
respektována	respektován	k2eAgFnSc1d1	respektována
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
hraje	hrát	k5eAaImIp3nS	hrát
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Založení	založení	k1gNnSc1	založení
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
dny	den	k1gInPc4	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1908	[number]	k4	1908
svolal	svolat	k5eAaPmAgMnS	svolat
Louis	Louis	k1gMnSc1	Louis
Magnus	Magnus	k1gMnSc1	Magnus
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
schůzku	schůzka	k1gFnSc4	schůzka
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
kromě	kromě	k7c2	kromě
něho	on	k3xPp3gInSc2	on
další	další	k2eAgMnPc1d1	další
dva	dva	k4xCgMnPc1	dva
francouzští	francouzský	k2eAgMnPc1d1	francouzský
zástupci	zástupce	k1gMnPc1	zástupce
R.	R.	kA	R.
Planque	Planque	k1gFnSc1	Planque
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
van	vana	k1gFnPc2	vana
der	drát	k5eAaImRp2nS	drát
Hoeven	Hoevno	k1gNnPc2	Hoevno
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
Belgičané	Belgičan	k1gMnPc1	Belgičan
Eddie	Eddie	k1gFnSc2	Eddie
Declercq	Declercq	k1gMnSc1	Declercq
a	a	k8xC	a
Eduard	Eduard	k1gMnSc1	Eduard
Malaret	Malaret	k1gMnSc1	Malaret
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
Švýcaři	Švýcar	k1gMnPc1	Švýcar
Eduard	Eduard	k1gMnSc1	Eduard
Mellor	Mellor	k1gMnSc1	Mellor
a	a	k8xC	a
L.	L.	kA	L.
Dufour	Dufour	k1gMnSc1	Dufour
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
Angličan	Angličan	k1gMnSc1	Angličan
<g/>
,	,	kIx,	,
pan	pan	k1gMnSc1	pan
Mavrogrodato	Mavrogrodat	k2eAgNnSc4d1	Mavrogrodat
<g/>
.	.	kIx.	.
</s>
<s>
Pozvánku	pozvánka	k1gFnSc4	pozvánka
dostali	dostat	k5eAaPmAgMnP	dostat
i	i	k9	i
Rusko	Rusko	k1gNnSc4	Rusko
a	a	k8xC	a
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedostavili	dostavit	k5eNaPmAgMnP	dostavit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dvoudenní	dvoudenní	k2eAgFnSc2d1	dvoudenní
debaty	debata	k1gFnSc2	debata
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
založit	založit	k5eAaPmF	založit
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
federaci	federace	k1gFnSc4	federace
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Ligue	Ligu	k1gMnSc2	Ligu
Internationale	Internationale	k1gMnSc2	Internationale
de	de	k?	de
Hockey	Hockea	k1gMnSc2	Hockea
sur	sur	k?	sur
Glace	Glace	k1gMnSc2	Glace
(	(	kIx(	(
<g/>
LIHG	LIHG	kA	LIHG
<g/>
)	)	kIx)	)
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
byli	být	k5eAaImAgMnP	být
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
první	první	k4xOgMnPc1	první
funkcionáři	funkcionář	k1gMnPc1	funkcionář
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
Magnus	Magnus	k1gInSc1	Magnus
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
předsedou	předseda	k1gMnSc7	předseda
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
krajan	krajan	k1gMnSc1	krajan
Robert	Robert	k1gMnSc1	Robert
Planque	Planque	k1gFnSc4	Planque
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
účastníků	účastník	k1gMnPc2	účastník
schůzky	schůzka	k1gFnSc2	schůzka
<g/>
,	,	kIx,	,
generálním	generální	k2eAgMnSc7d1	generální
sekretářem	sekretář	k1gMnSc7	sekretář
<g/>
.	.	kIx.	.
</s>
<s>
Účastníci	účastník	k1gMnPc1	účastník
se	se	k3xPyFc4	se
rozjeli	rozjet	k5eAaPmAgMnP	rozjet
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
referovali	referovat	k5eAaBmAgMnP	referovat
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
národní	národní	k2eAgInPc1d1	národní
svazy	svaz	k1gInPc1	svaz
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
přihlašovali	přihlašovat	k5eAaImAgMnP	přihlašovat
za	za	k7c7	za
členy	člen	k1gMnPc7	člen
LIHG	LIHG	kA	LIHG
<g/>
.	.	kIx.	.
</s>
<s>
Přihlášky	přihláška	k1gFnPc1	přihláška
přicházely	přicházet	k5eAaImAgFnP	přicházet
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Vstup	vstup	k1gInSc4	vstup
Čech	Čechy	k1gFnPc2	Čechy
do	do	k7c2	do
LIHG	LIHG	kA	LIHG
==	==	k?	==
</s>
</p>
<p>
<s>
Dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
označovala	označovat	k5eAaImAgFnS	označovat
Emila	Emil	k1gMnSc4	Emil
Procházku	Procházka	k1gMnSc4	Procházka
za	za	k7c4	za
funkcionáře	funkcionář	k1gMnSc4	funkcionář
poněkud	poněkud	k6eAd1	poněkud
dobrodružné	dobrodružný	k2eAgFnSc2d1	dobrodružná
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Nález	nález	k1gInSc1	nález
zásadních	zásadní	k2eAgInPc2d1	zásadní
dokumentů	dokument	k1gInPc2	dokument
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
Českého	český	k2eAgInSc2d1	český
svazu	svaz	k1gInSc2	svaz
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
v	v	k7c6	v
archívu	archív	k1gInSc6	archív
Zemského	zemský	k2eAgNnSc2d1	zemské
místodržitelství	místodržitelství	k1gNnSc2	místodržitelství
–	–	k?	–
Procházkovy	Procházkův	k2eAgFnSc2d1	Procházkova
žádosti	žádost	k1gFnSc2	žádost
o	o	k7c6	o
schválení	schválení	k1gNnSc6	schválení
stanov	stanova	k1gFnPc2	stanova
a	a	k8xC	a
odpovědi	odpověď	k1gFnSc2	odpověď
příslušného	příslušný	k2eAgInSc2d1	příslušný
orgánu	orgán	k1gInSc2	orgán
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
neobyčejně	obyčejně	k6eNd1	obyčejně
energického	energický	k2eAgMnSc4d1	energický
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nehodlal	hodlat	k5eNaImAgMnS	hodlat
ztrácet	ztrácet	k5eAaImF	ztrácet
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
schůzce	schůzka	k1gFnSc6	schůzka
<g/>
,	,	kIx,	,
svolané	svolaný	k2eAgFnPc4d1	svolaná
Magnusem	Magnus	k1gInSc7	Magnus
na	na	k7c4	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1908	[number]	k4	1908
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
zvyklý	zvyklý	k2eAgMnSc1d1	zvyklý
jednat	jednat	k5eAaImF	jednat
co	co	k9	co
nejrychleji	rychle	k6eAd3	rychle
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
usuzovat	usuzovat	k5eAaImF	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
až	až	k9	až
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pozvánku	pozvánka	k1gFnSc4	pozvánka
na	na	k7c6	na
ustavující	ustavující	k2eAgFnSc6d1	ustavující
schůzi	schůze	k1gFnSc6	schůze
Českého	český	k2eAgInSc2d1	český
svazu	svaz	k1gInSc2	svaz
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
rozesílal	rozesílat	k5eAaImAgInS	rozesílat
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
v	v	k7c6	v
hostinci	hostinec	k1gInSc6	hostinec
U	u	k7c2	u
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
pracoval	pracovat	k5eAaImAgMnS	pracovat
s	s	k7c7	s
neobyčejnou	obyčejný	k2eNgFnSc7d1	neobyčejná
intenzitou	intenzita	k1gFnSc7	intenzita
<g/>
.	.	kIx.	.
</s>
<s>
Návrhy	návrh	k1gInPc1	návrh
stanov	stanova	k1gFnPc2	stanova
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
účastníci	účastník	k1gMnPc1	účastník
přednesli	přednést	k5eAaPmAgMnP	přednést
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
přinesl	přinést	k5eAaPmAgInS	přinést
domů	domů	k6eAd1	domů
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
večer	večer	k6eAd1	večer
a	a	k8xC	a
již	již	k6eAd1	již
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
předložil	předložit	k5eAaPmAgMnS	předložit
hotový	hotový	k2eAgInSc4d1	hotový
text	text	k1gInSc4	text
na	na	k7c4	na
místodržitelství	místodržitelství	k1gNnSc4	místodržitelství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
vypracování	vypracování	k1gNnSc6	vypracování
měl	mít	k5eAaImAgInS	mít
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
–	–	k?	–
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
musel	muset	k5eAaImAgMnS	muset
také	také	k9	také
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
–	–	k?	–
úctyhodný	úctyhodný	k2eAgInSc1d1	úctyhodný
výkon	výkon	k1gInSc1	výkon
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
hostinci	hostinec	k1gInSc6	hostinec
U	u	k7c2	u
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Procházka	Procházka	k1gMnSc1	Procházka
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
odeslal	odeslat	k5eAaPmAgMnS	odeslat
přihlášku	přihláška	k1gFnSc4	přihláška
Čech	Čechy	k1gFnPc2	Čechy
do	do	k7c2	do
LIHG	LIHG	kA	LIHG
<g/>
;	;	kIx,	;
ta	ten	k3xDgFnSc1	ten
dorazila	dorazit	k5eAaPmAgFnS	dorazit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
úředník	úředník	k1gMnSc1	úředník
Českého	český	k2eAgNnSc2d1	české
místodržitelství	místodržitelství	k1gNnSc2	místodržitelství
opatřil	opatřit	k5eAaPmAgInS	opatřit
Procházkovu	Procházkův	k2eAgFnSc4d1	Procházkova
žádost	žádost	k1gFnSc4	žádost
schvalujícím	schvalující	k2eAgNnSc7d1	schvalující
razítkem	razítko	k1gNnSc7	razítko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
jediný	jediný	k2eAgInSc1d1	jediný
okamžik	okamžik	k1gInSc1	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Procházka	Procházka	k1gMnSc1	Procházka
zahrál	zahrát	k5eAaPmAgMnS	zahrát
vabank	vabank	k6eAd1	vabank
a	a	k8xC	a
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
trochu	trochu	k6eAd1	trochu
přelstít	přelstít	k5eAaPmF	přelstít
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čechy	Čechy	k1gFnPc1	Čechy
tak	tak	k9	tak
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
vstupujících	vstupující	k2eAgInPc2d1	vstupující
do	do	k7c2	do
LIHG	LIHG	kA	LIHG
<g/>
,	,	kIx,	,
předstihly	předstihnout	k5eAaPmAgFnP	předstihnout
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
a	a	k8xC	a
Belgii	Belgie	k1gFnSc4	Belgie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
citovaným	citovaný	k2eAgInSc7d1	citovaný
faktem	fakt	k1gInSc7	fakt
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Čechy	Čechy	k1gFnPc1	Čechy
byly	být	k5eAaImAgFnP	být
spoluzakladateli	spoluzakladatel	k1gMnSc3	spoluzakladatel
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
federace	federace	k1gFnSc2	federace
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Zase	zase	k9	zase
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
<g/>
;	;	kIx,	;
budeme	být	k5eAaImBp1nP	být
<g/>
-li	i	k?	-li
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
okamžik	okamžik	k1gInSc4	okamžik
vzniku	vznik	k1gInSc2	vznik
LIHG	LIHG	kA	LIHG
květnovou	květnový	k2eAgFnSc4d1	květnová
schůzku	schůzka	k1gFnSc4	schůzka
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jasné	jasný	k2eAgNnSc1d1	jasné
–	–	k?	–
český	český	k2eAgMnSc1d1	český
zástupce	zástupce	k1gMnSc1	zástupce
tam	tam	k6eAd1	tam
nebyl	být	k5eNaImAgMnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
ověřenou	ověřený	k2eAgFnSc7d1	ověřená
skutečností	skutečnost	k1gFnSc7	skutečnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Český	český	k2eAgInSc1d1	český
hokejový	hokejový	k2eAgInSc1d1	hokejový
svaz	svaz	k1gInSc1	svaz
do	do	k7c2	do
organizace	organizace	k1gFnSc2	organizace
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
jako	jako	k9	jako
druhý	druhý	k4xOgMnSc1	druhý
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
pěti	pět	k4xCc7	pět
svazy	svaz	k1gInPc7	svaz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
LIHG	LIHG	kA	LIHG
v	v	k7c6	v
roce	rok	k1gInSc6	rok
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
sdružovala	sdružovat	k5eAaImAgFnS	sdružovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členy	člen	k1gInPc1	člen
LIHG	LIHG	kA	LIHG
nebyly	být	k5eNaImAgInP	být
jen	jen	k9	jen
samostatné	samostatný	k2eAgInPc1d1	samostatný
státy	stát	k1gInPc1	stát
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
založení	založení	k1gNnSc6	založení
LIHG	LIHG	kA	LIHG
se	se	k3xPyFc4	se
vyskytli	vyskytnout	k5eAaPmAgMnP	vyskytnout
kritici	kritik	k1gMnPc1	kritik
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
poukazovali	poukazovat	k5eAaImAgMnP	poukazovat
na	na	k7c4	na
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Čechy	Čechy	k1gFnPc1	Čechy
nejsou	být	k5eNaImIp3nP	být
samostatný	samostatný	k2eAgInSc4d1	samostatný
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
členem	člen	k1gInSc7	člen
LIHG	LIHG	kA	LIHG
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
ani	ani	k8xC	ani
později	pozdě	k6eAd2	pozdě
nebyly	být	k5eNaImAgInP	být
do	do	k7c2	do
LIHG	LIHG	kA	LIHG
(	(	kIx(	(
<g/>
či	či	k8xC	či
IIHF	IIHF	kA	IIHF
<g/>
)	)	kIx)	)
přijímány	přijímán	k2eAgInPc1d1	přijímán
jen	jen	k8xS	jen
suverénní	suverénní	k2eAgInPc1d1	suverénní
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1911	[number]	k4	1911
–	–	k?	–
1920	[number]	k4	1920
patřilo	patřit	k5eAaImAgNnS	patřit
členství	členství	k1gNnSc1	členství
v	v	k7c6	v
LIHG	LIHG	kA	LIHG
klubu	klub	k1gInSc2	klub
Oxford	Oxford	k1gInSc1	Oxford
Canadiens	Canadiens	k1gInSc1	Canadiens
<g/>
,	,	kIx,	,
tvořenému	tvořený	k2eAgInSc3d1	tvořený
kanadskými	kanadský	k2eAgMnPc7d1	kanadský
vysokoškoláky	vysokoškolák	k1gMnPc7	vysokoškolák
studujícími	studující	k1gMnPc7	studující
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
LIHG	LIHG	kA	LIHG
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
britským	britský	k2eAgNnSc7d1	Britské
dominiem	dominion	k1gNnSc7	dominion
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
na	na	k7c6	na
Britském	britský	k2eAgNnSc6d1	Britské
impériu	impérium	k1gNnSc6	impérium
získávala	získávat	k5eAaImAgFnS	získávat
postupně	postupně	k6eAd1	postupně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1867	[number]	k4	1867
Vytvořeno	vytvořen	k2eAgNnSc4d1	vytvořeno
spojené	spojený	k2eAgNnSc4d1	spojené
dominium	dominium	k1gNnSc4	dominium
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
konfederace	konfederace	k1gFnSc1	konfederace
<g/>
,	,	kIx,	,
založen	založit	k5eAaPmNgInS	založit
Kanadský	kanadský	k2eAgInSc1d1	kanadský
parlament	parlament	k1gInSc1	parlament
</s>
</p>
<p>
<s>
1919	[number]	k4	1919
Zástupci	zástupce	k1gMnPc1	zástupce
Kanady	Kanada	k1gFnSc2	Kanada
podepsali	podepsat	k5eAaPmAgMnP	podepsat
Versailleskou	versailleský	k2eAgFnSc4d1	Versailleská
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
byla	být	k5eAaImAgFnS	být
ukončena	ukončen	k2eAgFnSc1d1	ukončena
1	[number]	k4	1
<g/>
.	.	kIx.	.
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
Kanada	Kanada	k1gFnSc1	Kanada
do	do	k7c2	do
války	válka	k1gFnSc2	válka
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
automaticky	automaticky	k6eAd1	automaticky
–	–	k?	–
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
aktem	akt	k1gInSc7	akt
Kanada	Kanada	k1gFnSc1	Kanada
stala	stát	k5eAaPmAgFnS	stát
zakládajícím	zakládající	k2eAgInSc7d1	zakládající
členem	člen	k1gInSc7	člen
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
</s>
</p>
<p>
<s>
1931	[number]	k4	1931
Westminsterský	Westminsterský	k2eAgInSc4d1	Westminsterský
status	status	k1gInSc4	status
–	–	k?	–
získání	získání	k1gNnSc2	získání
suverenity	suverenita	k1gFnSc2	suverenita
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
Canada	Canada	k1gFnSc1	Canada
Act	Act	k1gFnSc1	Act
–	–	k?	–
zákon	zákon	k1gInSc4	zákon
Britského	britský	k2eAgInSc2d1	britský
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
svých	svůj	k3xOyFgFnPc2	svůj
posledních	poslední	k2eAgFnPc2d1	poslední
pravomocí	pravomoc	k1gFnPc2	pravomoc
nad	nad	k7c7	nad
Kanadou	Kanada	k1gFnSc7	Kanada
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1933	[number]	k4	1933
–	–	k?	–
1936	[number]	k4	1936
bylo	být	k5eAaImAgNnS	být
členem	člen	k1gInSc7	člen
IIHF	IIHF	kA	IIHF
Dominium	dominium	k1gNnSc1	dominium
Newfoundland	Newfoundland	k1gInSc1	Newfoundland
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
roku	rok	k1gInSc3	rok
1934	[number]	k4	1934
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
vlivem	vliv	k1gInSc7	vliv
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
svoji	svůj	k3xOyFgFnSc4	svůj
autonomii	autonomie	k1gFnSc4	autonomie
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
spravováno	spravován	k2eAgNnSc1d1	spravováno
šestičlennou	šestičlenný	k2eAgFnSc7d1	šestičlenná
komisí	komise	k1gFnSc7	komise
(	(	kIx(	(
<g/>
3	[number]	k4	3
Britové	Brit	k1gMnPc1	Brit
+	+	kIx~	+
3	[number]	k4	3
Newfoundlanďané	Newfoundlanďaná	k1gFnSc2	Newfoundlanďaná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
1909	[number]	k4	1909
</s>
</p>
<p>
<s>
Od	od	k7c2	od
23	[number]	k4	23
<g/>
.	.	kIx.	.
do	do	k7c2	do
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
Chamonix	Chamonix	k1gNnSc6	Chamonix
první	první	k4xOgMnSc1	první
velký	velký	k2eAgInSc1d1	velký
turnaj	turnaj	k1gInSc1	turnaj
v	v	k7c6	v
"	"	kIx"	"
<g/>
kanadském	kanadský	k2eAgInSc6d1	kanadský
<g/>
"	"	kIx"	"
hokeji	hokej	k1gInSc6	hokej
<g/>
,	,	kIx,	,
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
Francie	Francie	k1gFnSc2	Francie
(	(	kIx(	(
<g/>
zastupované	zastupovaný	k2eAgFnPc1d1	zastupovaná
CP	CP	kA	CP
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
(	(	kIx(	(
<g/>
FPB	FPB	kA	FPB
Brusel	Brusel	k1gInSc1	Brusel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc2	Británie
(	(	kIx(	(
<g/>
Prince	princ	k1gMnSc2	princ
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
IHC	IHC	kA	IHC
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
(	(	kIx(	(
<g/>
CP	CP	kA	CP
Lausanne	Lausanne	k1gNnSc4	Lausanne
<g/>
)	)	kIx)	)
a	a	k8xC	a
výběru	výběr	k1gInSc2	výběr
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
22	[number]	k4	22
<g/>
.	.	kIx.	.
do	do	k7c2	do
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
druhý	druhý	k4xOgInSc1	druhý
kongres	kongres	k1gInSc1	kongres
LIHG	LIHG	kA	LIHG
v	v	k7c6	v
Chamonix	Chamonix	k1gNnSc6	Chamonix
<g/>
.	.	kIx.	.
</s>
<s>
LIHG	LIHG	kA	LIHG
definuje	definovat	k5eAaBmIp3nS	definovat
vlastní	vlastní	k2eAgNnSc1d1	vlastní
pravidla	pravidlo	k1gNnPc1	pravidlo
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozhodnuto	rozhodnut	k2eAgNnSc1d1	rozhodnuto
pořádat	pořádat	k5eAaImF	pořádat
každoročně	každoročně	k6eAd1	každoročně
evropský	evropský	k2eAgInSc4d1	evropský
šampionát	šampionát	k1gInSc4	šampionát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Německo	Německo	k1gNnSc1	Německo
šestým	šestý	k4xOgMnSc7	šestý
členem	člen	k1gInSc7	člen
LIHG	LIHG	kA	LIHG
<g/>
.1910	.1910	k4	.1910
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
první	první	k4xOgNnSc1	první
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
v	v	k7c4	v
Les	les	k1gInSc4	les
Avants	Avantsa	k1gFnPc2	Avantsa
u	u	k7c2	u
Montreux	Montreux	k1gInSc1	Montreux
<g/>
.	.	kIx.	.
</s>
<s>
Šampionátu	šampionát	k1gInSc3	šampionát
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
jako	jako	k9	jako
neoficiální	oficiální	k2eNgMnSc1d1	oficiální
host	host	k1gMnSc1	host
Oxford	Oxford	k1gInSc4	Oxford
Canadiens	Canadiens	k1gInSc1	Canadiens
<g/>
.1911	.1911	k4	.1911
</s>
</p>
<p>
<s>
LIHG	LIHG	kA	LIHG
přebrala	přebrat	k5eAaPmAgNnP	přebrat
kanadská	kanadský	k2eAgNnPc1d1	kanadské
pravidla	pravidlo	k1gNnPc1	pravidlo
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
členské	členský	k2eAgFnPc4d1	členská
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
do	do	k7c2	do
25	[number]	k4	25
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
bylo	být	k5eAaImAgNnS	být
členem	člen	k1gInSc7	člen
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
LIHG	LIHG	kA	LIHG
přijat	přijat	k2eAgInSc1d1	přijat
klub	klub	k1gInSc1	klub
Oxford	Oxford	k1gInSc1	Oxford
Canadiens	Canadiens	k1gInSc1	Canadiens
<g/>
.1912	.1912	k4	.1912
</s>
</p>
<p>
<s>
ME	ME	kA	ME
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
bylo	být	k5eAaImAgNnS	být
anulováno	anulovat	k5eAaBmNgNnS	anulovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
také	také	k6eAd1	také
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nebylo	být	k5eNaImAgNnS	být
členem	člen	k1gInSc7	člen
LIHG	LIHG	kA	LIHG
<g/>
.18	.18	k4	.18
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
LIHG	LIHG	kA	LIHG
přijato	přijat	k2eAgNnSc1d1	přijato
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
a	a	k8xC	a
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
<g/>
.1914	.1914	k4	.1914
</s>
</p>
<p>
<s>
Na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
předseda	předseda	k1gMnSc1	předseda
LIHG	LIHG	kA	LIHG
Adrien	Adriena	k1gFnPc2	Adriena
van	vana	k1gFnPc2	vana
den	den	k1gInSc1	den
Bulcke	Bulck	k1gInSc2	Bulck
a	a	k8xC	a
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
organizace	organizace	k1gFnSc2	organizace
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
Louis	Louis	k1gMnSc1	Louis
Magnus	Magnus	k1gMnSc1	Magnus
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jemu	on	k3xPp3gMnSc3	on
se	se	k3xPyFc4	se
však	však	k9	však
nesplnily	splnit	k5eNaPmAgFnP	splnit
představy	představa	k1gFnPc1	představa
o	o	k7c4	o
vedení	vedení	k1gNnSc4	vedení
LIHG	LIHG	kA	LIHG
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
a	a	k8xC	a
vedení	vedení	k1gNnSc1	vedení
organizace	organizace	k1gFnSc2	organizace
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
B.	B.	kA	B.
M.	M.	kA	M.
Patron	patron	k1gMnSc1	patron
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádný	mimořádný	k2eAgInSc1d1	mimořádný
kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ihned	ihned	k6eAd1	ihned
svolal	svolat	k5eAaPmAgInS	svolat
<g/>
,	,	kIx,	,
zvolil	zvolit	k5eAaPmAgInS	zvolit
za	za	k7c4	za
prezidenta	prezident	k1gMnSc4	prezident
znovu	znovu	k6eAd1	znovu
van	van	k1gInSc1	van
den	den	k1gInSc1	den
Bulckeho	Bulcke	k1gMnSc2	Bulcke
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
během	během	k7c2	během
několika	několik	k4yIc2	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
.1914	.1914	k4	.1914
–	–	k?	–
1918	[number]	k4	1918
</s>
</p>
<p>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
přerušena	přerušen	k2eAgFnSc1d1	přerušena
činnost	činnost	k1gFnSc1	činnost
LIHG	LIHG	kA	LIHG
<g/>
.1920	.1920	k4	.1920
</s>
</p>
<p>
<s>
Na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
byl	být	k5eAaImAgInS	být
lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
zařazen	zařadit	k5eAaPmNgInS	zařadit
jako	jako	k8xS	jako
ukázkový	ukázkový	k2eAgInSc1d1	ukázkový
sport	sport	k1gInSc1	sport
<g/>
.	.	kIx.	.
</s>
<s>
Účastnily	účastnit	k5eAaImAgInP	účastnit
se	se	k3xPyFc4	se
i	i	k9	i
celky	celek	k1gInPc1	celek
ze	z	k7c2	z
zámoří	zámoří	k1gNnSc2	zámoří
Kanada	Kanada	k1gFnSc1	Kanada
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
LIHG	LIHG	kA	LIHG
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
během	během	k7c2	během
olympijského	olympijský	k2eAgInSc2d1	olympijský
turnaje	turnaj	k1gInSc2	turnaj
bylo	být	k5eAaImAgNnS	být
vyloučeno	vyloučen	k2eAgNnSc1d1	vyloučeno
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
novými	nový	k2eAgInPc7d1	nový
členy	člen	k1gInPc7	člen
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
26	[number]	k4	26
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
převedlo	převést	k5eAaPmAgNnS	převést
členství	členství	k1gNnSc1	členství
Čech	Čechy	k1gFnPc2	Čechy
na	na	k7c4	na
nově	nově	k6eAd1	nově
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
stát	stát	k1gInSc4	stát
Československo	Československo	k1gNnSc1	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
zámoří	zámoří	k1gNnSc2	zámoří
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozsáhlým	rozsáhlý	k2eAgFnPc3d1	rozsáhlá
úpravám	úprava	k1gFnPc3	úprava
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.1922	.1922	k4	.1922
</s>
</p>
<p>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
LIHG	LIHG	kA	LIHG
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Belgičan	Belgičan	k1gMnSc1	Belgičan
Paul	Paul	k1gMnSc1	Paul
Loicq	Loicq	k1gMnSc1	Loicq
<g/>
.1923	.1923	k4	.1923
</s>
</p>
<p>
<s>
Na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
hokejový	hokejový	k2eAgInSc1d1	hokejový
turnaj	turnaj	k1gInSc1	turnaj
na	na	k7c6	na
Týdnu	týden	k1gInSc6	týden
zimních	zimní	k2eAgInPc2d1	zimní
sportů	sport	k1gInPc2	sport
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
<g/>
)	)	kIx)	)
v	v	k7c6	v
Chamonix	Chamonix	k1gNnSc6	Chamonix
1924	[number]	k4	1924
byl	být	k5eAaImAgMnS	být
i	i	k9	i
mistrovstvím	mistrovství	k1gNnSc7	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
konat	konat	k5eAaImF	konat
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Novým	nový	k2eAgMnSc7d1	nový
členem	člen	k1gMnSc7	člen
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
.1924	.1924	k4	.1924
</s>
</p>
<p>
<s>
Novými	nový	k2eAgInPc7d1	nový
členy	člen	k1gInPc7	člen
se	se	k3xPyFc4	se
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
stalo	stát	k5eAaPmAgNnS	stát
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
a	a	k8xC	a
Itálie	Itálie	k1gFnSc1	Itálie
a	a	k8xC	a
také	také	k9	také
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
členství	členství	k1gNnSc4	členství
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
nepřijetí	nepřijetí	k1gNnSc3	nepřijetí
Německa	Německo	k1gNnSc2	Německo
vystoupilo	vystoupit	k5eAaPmAgNnS	vystoupit
z	z	k7c2	z
LIHG	LIHG	kA	LIHG
<g/>
.1926	.1926	k4	.1926
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
členství	členství	k1gNnSc4	členství
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Švédsko	Švédsko	k1gNnSc4	Švédsko
a	a	k8xC	a
novým	nový	k2eAgInSc7d1	nový
členem	člen	k1gInSc7	člen
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
ještě	ještě	k9	ještě
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
1927	[number]	k4	1927
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1928	[number]	k4	1928
Finsko	Finsko	k1gNnSc4	Finsko
<g/>
.1929	.1929	k4	.1929
</s>
</p>
<p>
<s>
Po	po	k7c6	po
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
se	se	k3xPyFc4	se
direktoriát	direktoriát	k1gInSc1	direktoriát
LIHG	LIHG	kA	LIHG
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
zasedání	zasedání	k1gNnSc6	zasedání
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
1930	[number]	k4	1930
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
hokejové	hokejový	k2eAgNnSc1d1	hokejové
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
konat	konat	k5eAaImF	konat
v	v	k7c6	v
některé	některý	k3yIgFnSc6	některý
členské	členský	k2eAgFnSc6d1	členská
zemi	zem	k1gFnSc6	zem
LIHG	LIHG	kA	LIHG
ne	ne	k9	ne
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
dosud	dosud	k6eAd1	dosud
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
každoročně	každoročně	k6eAd1	každoročně
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
bude	být	k5eAaImBp3nS	být
součástí	součást	k1gFnSc7	součást
MS	MS	kA	MS
<g/>
,	,	kIx,	,
pořadí	pořadí	k1gNnSc6	pořadí
ME	ME	kA	ME
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
určovat	určovat	k5eAaImF	určovat
z	z	k7c2	z
pořadí	pořadí	k1gNnSc2	pořadí
MS	MS	kA	MS
bez	bez	k7c2	bez
mimoevropských	mimoevropský	k2eAgInPc2d1	mimoevropský
celků	celek	k1gInPc2	celek
<g/>
.1930	.1930	k4	.1930
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
mimo	mimo	k7c4	mimo
rámec	rámec	k1gInSc4	rámec
ZOH	ZOH	kA	ZOH
skončilo	skončit	k5eAaPmAgNnS	skončit
díky	díky	k7c3	díky
přírodním	přírodní	k2eAgFnPc3d1	přírodní
podmínkám	podmínka	k1gFnPc3	podmínka
částečným	částečný	k2eAgMnSc7d1	částečný
organizačním	organizační	k2eAgInSc7d1	organizační
neúspěchem	neúspěch	k1gInSc7	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Turnaj	turnaj	k1gInSc1	turnaj
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
stěhovat	stěhovat	k5eAaImF	stěhovat
z	z	k7c2	z
přírodního	přírodní	k2eAgInSc2d1	přírodní
ledu	led	k1gInSc2	led
v	v	k7c4	v
Chamonix	Chamonix	k1gNnSc4	Chamonix
do	do	k7c2	do
měst	město	k1gNnPc2	město
s	s	k7c7	s
umělým	umělý	k2eAgInSc7d1	umělý
ledem	led	k1gInSc7	led
Berlína	Berlín	k1gInSc2	Berlín
a	a	k8xC	a
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Novým	nový	k2eAgMnSc7d1	nový
členem	člen	k1gMnSc7	člen
od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
z	z	k7c2	z
asijského	asijský	k2eAgInSc2d1	asijský
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
třicátých	třicátý	k4xOgNnPc2	třicátý
byli	být	k5eAaImAgMnP	být
přijati	přijat	k2eAgMnPc1d1	přijat
další	další	k2eAgMnPc1d1	další
členové	člen	k1gMnPc1	člen
<g/>
:	:	kIx,	:
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Newfoundland	Newfoundland	k1gInSc1	Newfoundland
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
a	a	k8xC	a
Norsko	Norsko	k1gNnSc1	Norsko
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
jediný	jediný	k2eAgMnSc1d1	jediný
zástupce	zástupce	k1gMnSc1	zástupce
z	z	k7c2	z
afrického	africký	k2eAgInSc2d1	africký
kontinentu	kontinent	k1gInSc2	kontinent
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1932	[number]	k4	1932
</s>
</p>
<p>
<s>
Zimní	zimní	k2eAgFnPc1d1	zimní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
v	v	k7c6	v
Lake	Lake	k1gFnSc6	Lake
Placid	Placida	k1gFnPc2	Placida
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
globální	globální	k2eAgFnSc3d1	globální
finanční	finanční	k2eAgFnSc3d1	finanční
krizi	krize	k1gFnSc3	krize
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
jen	jen	k9	jen
dvě	dva	k4xCgFnPc1	dva
evropské	evropský	k2eAgFnPc1d1	Evropská
země	zem	k1gFnPc1	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
naposledy	naposledy	k6eAd1	naposledy
konalo	konat	k5eAaImAgNnS	konat
odděleně	odděleně	k6eAd1	odděleně
od	od	k7c2	od
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.1933	.1933	k4	.1933
</s>
</p>
<p>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
přišla	přijít	k5eAaPmAgFnS	přijít
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
o	o	k7c4	o
svoji	svůj	k3xOyFgFnSc4	svůj
neporazitelnost	neporazitelnost	k1gFnSc4	neporazitelnost
a	a	k8xC	a
ztratila	ztratit	k5eAaPmAgFnS	ztratit
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
.1936	.1936	k4	.1936
</s>
</p>
<p>
<s>
Rekordní	rekordní	k2eAgInSc1d1	rekordní
počet	počet	k1gInSc1	počet
15	[number]	k4	15
účastníků	účastník	k1gMnPc2	účastník
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Hokejový	hokejový	k2eAgInSc4d1	hokejový
turnaj	turnaj	k1gInSc4	turnaj
senzačně	senzačně	k6eAd1	senzačně
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
turnaj	turnaj	k1gInSc1	turnaj
považován	považován	k2eAgInSc1d1	považován
za	za	k7c7	za
MS	MS	kA	MS
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
aspoň	aspoň	k9	aspoň
jedna	jeden	k4xCgFnSc1	jeden
mimoevropská	mimoevropský	k2eAgFnSc1d1	mimoevropská
země	země	k1gFnSc1	země
<g/>
.1939	.1939	k4	.1939
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
způsobila	způsobit	k5eAaPmAgFnS	způsobit
zrušení	zrušení	k1gNnSc4	zrušení
mistrovství	mistrovství	k1gNnSc2	mistrovství
v	v	k7c6	v
letech	let	k1gInPc6	let
1940	[number]	k4	1940
–	–	k?	–
1946.1946	[number]	k4	1946.1946
</s>
</p>
<p>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
po	po	k7c6	po
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
kongres	kongres	k1gInSc4	kongres
LIHG	LIHG	kA	LIHG
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
bylo	být	k5eAaImAgNnS	být
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
členství	členství	k1gNnSc2	členství
<g/>
,	,	kIx,	,
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
anektovaných	anektovaný	k2eAgInPc2d1	anektovaný
pobaltských	pobaltský	k2eAgInPc2d1	pobaltský
států	stát	k1gInPc2	stát
–	–	k?	–
Estonska	Estonsko	k1gNnSc2	Estonsko
<g/>
,	,	kIx,	,
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
a	a	k8xC	a
Litvy	Litva	k1gFnSc2	Litva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
členství	členství	k1gNnSc4	členství
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
po	po	k7c4	po
připojení	připojení	k1gNnSc4	připojení
země	zem	k1gFnSc2	zem
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Novým	nový	k2eAgInSc7d1	nový
členem	člen	k1gInSc7	člen
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Angličtina	angličtina	k1gFnSc1	angličtina
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
druhým	druhý	k4xOgInSc7	druhý
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
také	také	k9	také
používat	používat	k5eAaImF	používat
zkratka	zkratka	k1gFnSc1	zkratka
IIHF	IIHF	kA	IIHF
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
rotující	rotující	k2eAgNnSc1d1	rotující
předsednictví	předsednictví	k1gNnSc1	předsednictví
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Severní	severní	k2eAgFnSc7d1	severní
Amerikou	Amerika	k1gFnSc7	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
se	se	k3xPyFc4	se
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
vzdala	vzdát	k5eAaPmAgFnS	vzdát
pořadatelství	pořadatelství	k1gNnSc4	pořadatelství
MS	MS	kA	MS
<g/>
,	,	kIx,	,
pořadatelství	pořadatelství	k1gNnSc2	pořadatelství
převzalo	převzít	k5eAaPmAgNnS	převzít
Československo	Československo	k1gNnSc1	Československo
<g/>
.1947	.1947	k4	.1947
</s>
</p>
<p>
<s>
Na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
konaném	konaný	k2eAgInSc6d1	konaný
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
při	při	k7c6	při
MS	MS	kA	MS
<g/>
,	,	kIx,	,
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
po	po	k7c6	po
25	[number]	k4	25
letech	léto	k1gNnPc6	léto
vládnutí	vládnutí	k1gNnSc2	vládnutí
Paul	Paul	k1gMnSc1	Paul
Loicq	Loicq	k1gMnSc7	Loicq
<g/>
,	,	kIx,	,
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Fritz	Fritz	k1gMnSc1	Fritz
Kraatz	Kraatz	k1gMnSc1	Kraatz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Československo	Československo	k1gNnSc1	Československo
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc2	první
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
hráči	hráč	k1gMnPc1	hráč
se	se	k3xPyFc4	se
neučili	učít	k5eNaPmAgMnP	učít
hrát	hrát	k5eAaImF	hrát
hokej	hokej	k1gInSc4	hokej
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentě	kontinent	k1gInSc6	kontinent
a	a	k8xC	a
také	také	k9	také
první	první	k4xOgFnSc1	první
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
domácí	domácí	k2eAgNnSc4d1	domácí
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.1948	.1948	k4	.1948
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Zimní	zimní	k2eAgFnPc4d1	zimní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
ve	v	k7c6	v
Svatém	svatý	k2eAgMnSc6d1	svatý
Mořici	Mořic	k1gMnSc6	Mořic
vyslaly	vyslat	k5eAaPmAgInP	vyslat
USA	USA	kA	USA
dvě	dva	k4xCgNnPc1	dva
reprezentační	reprezentační	k2eAgNnPc1d1	reprezentační
mužstva	mužstvo	k1gNnPc1	mužstvo
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
tvořili	tvořit	k5eAaImAgMnP	tvořit
hráči	hráč	k1gMnPc1	hráč
vyslaní	vyslaný	k2eAgMnPc1d1	vyslaný
AHAUS	AHAUS	kA	AHAUS
(	(	kIx(	(
<g/>
Amateur	Amateur	k1gMnSc1	Amateur
Hockey	Hockea	k1gFnSc2	Hockea
Association	Association	k1gInSc1	Association
of	of	k?	of
United	United	k1gInSc1	United
States	States	k1gInSc1	States
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
členem	člen	k1gMnSc7	člen
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
LIHG	LIHG	kA	LIHG
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
tým	tým	k1gInSc4	tým
sestavila	sestavit	k5eAaPmAgFnS	sestavit
AAU	AAU	kA	AAU
(	(	kIx(	(
<g/>
Amateur	Amateur	k1gMnSc1	Amateur
Athletic	Athletice	k1gFnPc2	Athletice
Union	union	k1gInSc1	union
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
hokej	hokej	k1gInSc1	hokej
nakonec	nakonec	k6eAd1	nakonec
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
výběr	výběr	k1gInSc1	výběr
AHAUS	AHAUS	kA	AHAUS
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
turnaji	turnaj	k1gInSc6	turnaj
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
výsledky	výsledek	k1gInPc1	výsledek
anulovány	anulován	k2eAgInPc1d1	anulován
<g/>
,	,	kIx,	,
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
byly	být	k5eAaImAgFnP	být
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
tabulky	tabulka	k1gFnSc2	tabulka
souběžného	souběžný	k2eAgNnSc2d1	souběžné
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.1951	.1951	k4	.1951
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
členství	členství	k1gNnSc4	členství
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.1952	.1952	k4	.1952
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
přijetí	přijetí	k1gNnSc4	přijetí
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.1954	.1954	k4	.1954
</s>
</p>
<p>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
senzačně	senzačně	k6eAd1	senzačně
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
prvním	první	k4xOgInSc6	první
startu	start	k1gInSc6	start
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
přijata	přijmout	k5eAaPmNgNnP	přijmout
jako	jako	k9	jako
25	[number]	k4	25
člen	člen	k1gInSc1	člen
NDR	NDR	kA	NDR
<g/>
.1957	.1957	k4	.1957
</s>
</p>
<p>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
invaze	invaze	k1gFnSc2	invaze
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
do	do	k7c2	do
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
bojkotovaly	bojkotovat	k5eAaImAgInP	bojkotovat
"	"	kIx"	"
<g/>
západní	západní	k2eAgNnSc1d1	západní
<g/>
"	"	kIx"	"
země	země	k1gFnSc1	země
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
přijata	přijat	k2eAgFnSc1d1	přijata
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Američan	Američan	k1gMnSc1	Američan
Walter	Walter	k1gMnSc1	Walter
Brown	Brown	k1gMnSc1	Brown
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
na	na	k7c6	na
International	International	k1gFnSc6	International
Ice	Ice	k1gFnSc2	Ice
Hockey	Hockea	k1gFnSc2	Hockea
Federation	Federation	k1gInSc1	Federation
(	(	kIx(	(
<g/>
IIHF	IIHF	kA	IIHF
<g/>
)	)	kIx)	)
<g/>
.1960	.1960	k4	.1960
</s>
</p>
<p>
<s>
Od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
novými	nový	k2eAgMnPc7d1	nový
členy	člen	k1gMnPc7	člen
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
a	a	k8xC	a
Korejská	korejský	k2eAgFnSc1d1	Korejská
republika	republika	k1gFnSc1	republika
<g/>
.1961	.1961	k4	.1961
</s>
</p>
<p>
<s>
Zavedeny	zaveden	k2eAgFnPc1d1	zavedena
výkonnostní	výkonnostní	k2eAgFnPc1d1	výkonnostní
skupiny	skupina	k1gFnPc1	skupina
na	na	k7c6	na
MS.	MS.	k1gFnSc6	MS.
<g/>
1962	[number]	k4	1962
</s>
</p>
<p>
<s>
MS	MS	kA	MS
bylo	být	k5eAaImAgNnS	být
opět	opět	k6eAd1	opět
narušeno	narušit	k5eAaPmNgNnS	narušit
politickou	politický	k2eAgFnSc7d1	politická
událostí	událost	k1gFnSc7	událost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
stavby	stavba	k1gFnSc2	stavba
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
Američané	Američan	k1gMnPc1	Američan
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
udělit	udělit	k5eAaPmF	udělit
víza	vízo	k1gNnPc4	vízo
reprezentantům	reprezentant	k1gMnPc3	reprezentant
NDR	NDR	kA	NDR
<g/>
.	.	kIx.	.
</s>
<s>
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
a	a	k8xC	a
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
ze	z	k7c2	z
solidarity	solidarita	k1gFnSc2	solidarita
bojkotovalo	bojkotovat	k5eAaImAgNnS	bojkotovat
MS.	MS.	k1gMnPc2	MS.
<g/>
1963	[number]	k4	1963
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
přijetí	přijetí	k1gNnSc2	přijetí
KLDR	KLDR	kA	KLDR
<g/>
.1966	.1966	k4	.1966
</s>
</p>
<p>
<s>
Zavedena	zaveden	k2eAgFnSc1d1	zavedena
klubová	klubový	k2eAgFnSc1d1	klubová
soutěž	soutěž	k1gFnSc1	soutěž
Pohár	pohár	k1gInSc1	pohár
mistrů	mistr	k1gMnPc2	mistr
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
PMEZ	PMEZ	kA	PMEZ
z	z	k7c2	z
kopané	kopaná	k1gFnSc2	kopaná
<g/>
.1968	.1968	k4	.1968
</s>
</p>
<p>
<s>
Zavedení	zavedení	k1gNnSc1	zavedení
juniorského	juniorský	k2eAgNnSc2d1	juniorské
mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.1969	.1969	k4	.1969
</s>
</p>
<p>
<s>
Československo	Československo	k1gNnSc1	Československo
se	se	k3xPyFc4	se
vzdalo	vzdát	k5eAaPmAgNnS	vzdát
pořadatelství	pořadatelství	k1gNnSc3	pořadatelství
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
převzalo	převzít	k5eAaPmAgNnS	převzít
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
.1970	.1970	k4	.1970
</s>
</p>
<p>
<s>
Zatím	zatím	k6eAd1	zatím
největší	veliký	k2eAgFnSc1d3	veliký
roztržka	roztržka	k1gFnSc1	roztržka
mezi	mezi	k7c7	mezi
IIHF	IIHF	kA	IIHF
a	a	k8xC	a
Kanadou	Kanada	k1gFnSc7	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
se	se	k3xPyFc4	se
vzdala	vzdát	k5eAaPmAgFnS	vzdát
pořadatelství	pořadatelství	k1gNnSc2	pořadatelství
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
1970	[number]	k4	1970
a	a	k8xC	a
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
bojkotovala	bojkotovat	k5eAaImAgFnS	bojkotovat
veškeré	veškerý	k3xTgFnPc4	veškerý
akce	akce	k1gFnPc4	akce
pořádané	pořádaný	k2eAgFnSc2d1	pořádaná
IIHF	IIHF	kA	IIHF
<g/>
.1972	.1972	k4	.1972
</s>
</p>
<p>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
olympijského	olympijský	k2eAgInSc2d1	olympijský
turnaje	turnaj	k1gInSc2	turnaj
a	a	k8xC	a
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.1975	.1975	k4	.1975
</s>
</p>
<p>
<s>
Zvolení	zvolení	k1gNnSc1	zvolení
Günthera	Günther	k1gMnSc2	Günther
Sabetzkeho	Sabetzke	k1gMnSc2	Sabetzke
na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
v	v	k7c6	v
Gstaadu	Gstaad	k1gInSc6	Gstaad
předsedou	předseda	k1gMnSc7	předseda
IIHF	IIHF	kA	IIHF
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kongres	kongres	k1gInSc1	kongres
odhlasoval	odhlasovat	k5eAaPmAgInS	odhlasovat
pořádání	pořádání	k1gNnSc4	pořádání
otevřených	otevřený	k2eAgFnPc2d1	otevřená
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kanaďané	Kanaďan	k1gMnPc1	Kanaďan
se	se	k3xPyFc4	se
zavázali	zavázat	k5eAaPmAgMnP	zavázat
účastnit	účastnit	k5eAaImF	účastnit
pravidelně	pravidelně	k6eAd1	pravidelně
MS.	MS.	k1gFnSc4	MS.
</s>
</p>
<p>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
a	a	k8xC	a
USA	USA	kA	USA
se	se	k3xPyFc4	se
vzdaly	vzdát	k5eAaPmAgInP	vzdát
pořádání	pořádání	k1gNnSc4	pořádání
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
Kanadský	kanadský	k2eAgInSc1d1	kanadský
pohár	pohár	k1gInSc1	pohár
(	(	kIx(	(
<g/>
soutěž	soutěž	k1gFnSc1	soutěž
pořádána	pořádán	k2eAgFnSc1d1	pořádána
NHL	NHL	kA	NHL
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
čtyř	čtyři	k4xCgInPc2	čtyři
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
týmů	tým	k1gInPc2	tým
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Místo	místo	k7c2	místo
francouzštiny	francouzština	k1gFnSc2	francouzština
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgInSc7	druhý
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
po	po	k7c6	po
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zrušeno	zrušen	k2eAgNnSc1d1	zrušeno
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
tříleté	tříletý	k2eAgNnSc1d1	tříleté
období	období	k1gNnSc1	období
předsedů	předseda	k1gMnPc2	předseda
evropského	evropský	k2eAgMnSc2d1	evropský
a	a	k8xC	a
kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
výboru	výbor	k1gInSc2	výbor
<g/>
.1977	.1977	k4	.1977
</s>
</p>
<p>
<s>
Zavedení	zavedení	k1gNnSc1	zavedení
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
juniorů	junior	k1gMnPc2	junior
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Návrat	návrat	k1gInSc1	návrat
Kanady	Kanada	k1gFnSc2	Kanada
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.1980	.1980	k4	.1980
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
přestalo	přestat	k5eAaPmAgNnS	přestat
konat	konat	k5eAaImF	konat
v	v	k7c6	v
olympijském	olympijský	k2eAgInSc6d1	olympijský
roce	rok	k1gInSc6	rok
<g/>
.1988	.1988	k4	.1988
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
olympijský	olympijský	k2eAgInSc1d1	olympijský
turnaj	turnaj	k1gInSc1	turnaj
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
také	také	k9	také
hokejisté	hokejista	k1gMnPc1	hokejista
z	z	k7c2	z
NHL	NHL	kA	NHL
<g/>
.1990	.1990	k4	.1990
</s>
</p>
<p>
<s>
Zavedeno	zaveden	k2eAgNnSc1d1	zavedeno
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
žen	žena	k1gFnPc2	žena
<g/>
.1991	.1991	k4	.1991
</s>
</p>
<p>
<s>
Zrušeno	zrušen	k2eAgNnSc1d1	zrušeno
mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.1992	.1992	k4	.1992
</s>
</p>
<p>
<s>
Na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
a	a	k8xC	a
olympijském	olympijský	k2eAgInSc6d1	olympijský
turnaji	turnaj	k1gInSc6	turnaj
zavedeno	zavést	k5eAaPmNgNnS	zavést
playoff	playoff	k1gInSc1	playoff
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
účastníků	účastník	k1gMnPc2	účastník
MS	MS	kA	MS
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
z	z	k7c2	z
osmi	osm	k4xCc2	osm
na	na	k7c4	na
dvanáct	dvanáct	k4xCc4	dvanáct
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
začalo	začít	k5eAaPmAgNnS	začít
konat	konat	k5eAaImF	konat
i	i	k9	i
v	v	k7c6	v
olympijském	olympijský	k2eAgInSc6d1	olympijský
roce	rok	k1gInSc6	rok
<g/>
.1994	.1994	k4	.1994
</s>
</p>
<p>
<s>
Novým	nový	k2eAgMnSc7d1	nový
předsedou	předseda	k1gMnSc7	předseda
IIHF	IIHF	kA	IIHF
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Švýcar	Švýcar	k1gMnSc1	Švýcar
René	René	k1gMnSc1	René
Fasel	Fasel	k1gMnSc1	Fasel
<g/>
.1997	.1997	k4	.1997
</s>
</p>
<p>
<s>
Založení	založení	k1gNnSc1	založení
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
IIHF	IIHF	kA	IIHF
<g/>
.1998	.1998	k4	.1998
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
konání	konání	k1gNnSc2	konání
olympijského	olympijský	k2eAgInSc2d1	olympijský
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
NHL	NHL	kA	NHL
přerušila	přerušit	k5eAaPmAgFnS	přerušit
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
soutěže	soutěž	k1gFnSc2	soutěž
mohli	moct	k5eAaImAgMnP	moct
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
světoví	světový	k2eAgMnPc1d1	světový
hráči	hráč	k1gMnPc1	hráč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
počet	počet	k1gInSc1	počet
účastníků	účastník	k1gMnPc2	účastník
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
na	na	k7c4	na
šestnáct	šestnáct	k4xCc4	šestnáct
<g/>
.2006	.2006	k4	.2006
</s>
</p>
<p>
<s>
Švédové	Švéd	k1gMnPc1	Švéd
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
roce	rok	k1gInSc6	rok
olympijský	olympijský	k2eAgInSc4d1	olympijský
turnaj	turnaj	k1gInSc4	turnaj
a	a	k8xC	a
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.2008	.2008	k4	.2008
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
kolébce	kolébka	k1gFnSc6	kolébka
hokeje	hokej	k1gInSc2	hokej
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc3	Kanada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Založen	založen	k2eAgInSc1d1	založen
Victoria	Victorium	k1gNnSc2	Victorium
cup	cup	k1gInSc1	cup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Růst	růst	k1gInSc1	růst
počtu	počet	k1gInSc2	počet
členů	člen	k1gMnPc2	člen
na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
během	během	k7c2	během
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
neustále	neustále	k6eAd1	neustále
rostl	růst	k5eAaImAgInS	růst
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
nárůstu	nárůst	k1gInSc2	nárůst
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
rozšíření	rozšíření	k1gNnSc4	rozšíření
hokeje	hokej	k1gInSc2	hokej
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
politické	politický	k2eAgFnSc3d1	politická
události	událost	k1gFnSc3	událost
<g/>
:	:	kIx,	:
rozpad	rozpad	k1gInSc1	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
a	a	k8xC	a
rozdělení	rozdělení	k1gNnSc2	rozdělení
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Členství	členství	k1gNnSc1	členství
po	po	k7c6	po
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
bylo	být	k5eAaImAgNnS	být
převedeno	převést	k5eAaPmNgNnS	převést
na	na	k7c4	na
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
,	,	kIx,	,
obnoveno	obnoven	k2eAgNnSc1d1	obnoveno
členství	členství	k1gNnSc1	členství
bylo	být	k5eAaImAgNnS	být
Lotyšsku	Lotyšsko	k1gNnSc3	Lotyšsko
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Litvě	Litva	k1gFnSc6	Litva
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
a	a	k8xC	a
Estonsku	Estonsko	k1gNnSc3	Estonsko
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
a	a	k8xC	a
novými	nový	k2eAgInPc7d1	nový
členy	člen	k1gInPc7	člen
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
–	–	k?	–
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Členství	členství	k1gNnSc1	členství
po	po	k7c6	po
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
bylo	být	k5eAaImAgNnS	být
převedeno	převést	k5eAaPmNgNnS	převést
na	na	k7c4	na
Srbsko	Srbsko	k1gNnSc4	Srbsko
a	a	k8xC	a
novými	nový	k2eAgMnPc7d1	nový
členy	člen	k1gMnPc7	člen
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
a	a	k8xC	a
Slovinsko	Slovinsko	k1gNnSc4	Slovinsko
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
o	o	k7c6	o
Slovensku	Slovensko	k1gNnSc6	Slovensko
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Členství	členství	k1gNnSc1	členství
po	po	k7c6	po
Československu	Československo	k1gNnSc6	Československo
bylo	být	k5eAaImAgNnS	být
převedeno	převést	k5eAaPmNgNnS	převést
na	na	k7c4	na
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	s	k7c7	s
členy	člen	k1gMnPc7	člen
federace	federace	k1gFnSc2	federace
staly	stát	k5eAaPmAgFnP	stát
také	také	k9	také
další	další	k2eAgFnSc3d1	další
nově	nova	k1gFnSc3	nova
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
republiky	republika	k1gFnSc2	republika
–	–	k?	–
Makedonie	Makedonie	k1gFnSc1	Makedonie
a	a	k8xC	a
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
z	z	k7c2	z
postsovětských	postsovětský	k2eAgFnPc2d1	postsovětská
republik	republika	k1gFnPc2	republika
Arménie	Arménie	k1gFnSc2	Arménie
(	(	kIx(	(
<g/>
členství	členství	k1gNnSc1	členství
později	pozdě	k6eAd2	pozdě
pozastaveno	pozastavit	k5eAaPmNgNnS	pozastavit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moldávie	Moldávie	k1gFnSc1	Moldávie
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gruzie	Gruzie	k1gFnSc1	Gruzie
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
prozatím	prozatím	k6eAd1	prozatím
poslední	poslední	k2eAgInSc4d1	poslední
Kyrgyzstán	Kyrgyzstán	k1gInSc4	Kyrgyzstán
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Noví	nový	k2eAgMnPc1d1	nový
členové	člen	k1gMnPc1	člen
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přistoupili	přistoupit	k5eAaPmAgMnP	přistoupit
k	k	k7c3	k
IIHF	IIHF	kA	IIHF
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
posledních	poslední	k2eAgNnPc2d1	poslední
35	[number]	k4	35
let	léto	k1gNnPc2	léto
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
globální	globální	k2eAgFnSc2d1	globální
expanze	expanze	k1gFnSc2	expanze
–	–	k?	–
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
:	:	kIx,	:
Řecko	Řecko	k1gNnSc1	Řecko
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Island	Island	k1gInSc1	Island
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Andorra	Andorra	k1gFnSc1	Andorra
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
a	a	k8xC	a
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
:	:	kIx,	:
Brazílie	Brazílie	k1gFnSc2	Brazílie
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
a	a	k8xC	a
Argentina	Argentina	k1gFnSc1	Argentina
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
:	:	kIx,	:
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
arabské	arabský	k2eAgInPc1d1	arabský
emiráty	emirát	k1gInPc1	emirát
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
z	z	k7c2	z
dálného	dálný	k2eAgInSc2d1	dálný
východu	východ	k1gInSc2	východ
a	a	k8xC	a
Oceánie	Oceánie	k1gFnSc2	Oceánie
<g/>
:	:	kIx,	:
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Hongkong	Hongkong	k1gInSc1	Hongkong
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Thajsko	Thajsko	k1gNnSc1	Thajsko
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Singapur	Singapur	k1gInSc1	Singapur
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Macao	Macao	k1gNnSc1	Macao
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
a	a	k8xC	a
Malajsie	Malajsie	k1gFnSc1	Malajsie
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
:	:	kIx,	:
Namibie	Namibie	k1gFnSc1	Namibie
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
a	a	k8xC	a
Maroko	Maroko	k1gNnSc4	Maroko
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovějšími	nový	k2eAgMnPc7d3	Nejnovější
členy	člen	k1gMnPc7	člen
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
Jamajka	Jamajka	k1gFnSc1	Jamajka
a	a	k8xC	a
Katar	katar	k1gInSc1	katar
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
Omán	Omán	k1gInSc1	Omán
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
Turkmenistán	Turkmenistán	k1gInSc4	Turkmenistán
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnPc4	Indonésie
a	a	k8xC	a
Nepál	Nepál	k1gInSc4	Nepál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prezidenti	prezident	k1gMnPc1	prezident
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Členské	členský	k2eAgFnPc1d1	členská
země	zem	k1gFnPc1	zem
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Plné	plný	k2eAgNnSc4d1	plné
členství	členství	k1gNnSc4	členství
===	===	k?	===
</s>
</p>
<p>
<s>
Plné	plný	k2eAgNnSc4d1	plné
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
IIHF	IIHF	kA	IIHF
mohou	moct	k5eAaImIp3nP	moct
získat	získat	k5eAaPmF	získat
pouze	pouze	k6eAd1	pouze
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
národní	národní	k2eAgInSc4d1	národní
svaz	svaz	k1gInSc4	svaz
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
a	a	k8xC	a
účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
hlasovací	hlasovací	k2eAgNnSc4d1	hlasovací
právo	právo	k1gNnSc4	právo
ve	v	k7c6	v
valném	valný	k2eAgNnSc6d1	Valné
shromáždění	shromáždění	k1gNnSc6	shromáždění
IIHF	IIHF	kA	IIHF
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přidružení	přidružený	k2eAgMnPc1d1	přidružený
členové	člen	k1gMnPc1	člen
===	===	k?	===
</s>
</p>
<p>
<s>
Přidružení	přidružený	k2eAgMnPc1d1	přidružený
členové	člen	k1gMnPc1	člen
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gMnPc7	člen
IIHF	IIHF	kA	IIHF
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nemají	mít	k5eNaImIp3nP	mít
žádný	žádný	k3yNgInSc4	žádný
samostatný	samostatný	k2eAgInSc4d1	samostatný
národní	národní	k2eAgInSc4d1	národní
hokejový	hokejový	k2eAgInSc4d1	hokejový
svaz	svaz	k1gInSc4	svaz
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
neúčastní	účastnit	k5eNaImIp3nS	účastnit
mistrovsví	mistrovsev	k1gFnSc7	mistrovsev
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
valném	valný	k2eAgNnSc6d1	Valné
shromáždění	shromáždění	k1gNnSc6	shromáždění
IIHF	IIHF	kA	IIHF
nemají	mít	k5eNaImIp3nP	mít
hlasovací	hlasovací	k2eAgNnPc4d1	hlasovací
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přidružení	přidružený	k2eAgMnPc1d1	přidružený
členové	člen	k1gMnPc1	člen
Inline	Inlin	k1gInSc5	Inlin
hokej	hokej	k1gInSc4	hokej
===	===	k?	===
</s>
</p>
<p>
<s>
Účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
soutěží	soutěž	k1gFnSc7	soutěž
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
inline	inlinout	k5eAaPmIp3nS	inlinout
hokeji	hokej	k1gInPc7	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
valném	valný	k2eAgNnSc6d1	Valné
shromáždění	shromáždění	k1gNnSc6	shromáždění
IIHF	IIHF	kA	IIHF
nemají	mít	k5eNaImIp3nP	mít
hlasovací	hlasovací	k2eAgNnPc4d1	hlasovací
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
členové	člen	k1gMnPc1	člen
===	===	k?	===
</s>
</p>
<p>
<s>
v	v	k7c6	v
letech	let	k1gInPc6	let
1911	[number]	k4	1911
–	–	k?	–
1920	[number]	k4	1920
byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
LIHG	LIHG	kA	LIHG
klub	klub	k1gInSc1	klub
Oxford	Oxford	k1gInSc1	Oxford
Canadiens	Canadiens	k1gInSc1	Canadiens
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soutěže	soutěž	k1gFnSc2	soutěž
pořádané	pořádaný	k2eAgFnSc2d1	pořádaná
IIHF	IIHF	kA	IIHF
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Soutěže	soutěž	k1gFnPc4	soutěž
národních	národní	k2eAgInPc2d1	národní
týmů	tým	k1gInPc2	tým
===	===	k?	===
</s>
</p>
<p>
<s>
Lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
do	do	k7c2	do
20	[number]	k4	20
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ženy	žena	k1gFnSc2	žena
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ženy	žena	k1gFnSc2	žena
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
inline	inlin	k1gInSc5	inlin
hokeji	hokej	k1gInSc6	hokej
</s>
</p>
<p>
<s>
===	===	k?	===
Klubové	klubový	k2eAgFnSc2d1	klubová
soutěže	soutěž	k1gFnSc2	soutěž
===	===	k?	===
</s>
</p>
<p>
<s>
Hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
mistrů	mistr	k1gMnPc2	mistr
</s>
</p>
<p>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
ženský	ženský	k2eAgInSc1d1	ženský
Champions	Champions	k1gInSc1	Champions
Cup	cup	k1gInSc1	cup
</s>
</p>
<p>
<s>
Kontinentální	kontinentální	k2eAgInSc4d1	kontinentální
pohár	pohár	k1gInSc4	pohár
</s>
</p>
<p>
<s>
Victoria	Victorium	k1gNnPc4	Victorium
Cup	cup	k1gInSc1	cup
</s>
</p>
<p>
<s>
===	===	k?	===
Zaniklé	zaniklý	k2eAgFnSc2d1	zaniklá
soutěže	soutěž	k1gFnSc2	soutěž
===	===	k?	===
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
1910	[number]	k4	1910
–	–	k?	–
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
do	do	k7c2	do
19	[number]	k4	19
let	léto	k1gNnPc2	léto
1968	[number]	k4	1968
–	–	k?	–
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
1977	[number]	k4	1977
–	–	k?	–
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
žen	žena	k1gFnPc2	žena
1989	[number]	k4	1989
–	–	k?	–
1996	[number]	k4	1996
</s>
</p>
<p>
<s>
Pohár	pohár	k1gInSc1	pohár
mistrů	mistr	k1gMnPc2	mistr
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
1966	[number]	k4	1966
–	–	k?	–
1996	[number]	k4	1996
</s>
</p>
<p>
<s>
Pohár	pohár	k1gInSc1	pohár
federace	federace	k1gFnSc2	federace
1995-1996	[number]	k4	1995-1996
</s>
</p>
<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
1997-2000	[number]	k4	1997-2000
</s>
</p>
<p>
<s>
IIHF	IIHF	kA	IIHF
Superpohár	superpohár	k1gInSc4	superpohár
1997-2000	[number]	k4	1997-2000
</s>
</p>
<p>
<s>
Super	super	k2eAgInSc1d1	super
six	six	k?	six
2005-2008	[number]	k4	2005-2008
</s>
</p>
<p>
<s>
===	===	k?	===
Ocenění	ocenění	k1gNnSc1	ocenění
===	===	k?	===
</s>
</p>
<p>
<s>
Síň	síň	k1gFnSc1	síň
slávy	sláva	k1gFnSc2	sláva
IIHF	IIHF	kA	IIHF
1997	[number]	k4	1997
-	-	kIx~	-
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
kniha	kniha	k1gFnSc1	kniha
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
–	–	k?	–
Gustav	Gustav	k1gMnSc1	Gustav
Vlk	Vlk	k1gMnSc1	Vlk
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Gut	Gut	k1gMnSc1	Gut
<g/>
,	,	kIx,	,
Olympia	Olympia	k1gFnSc1	Olympia
1978	[number]	k4	1978
</s>
</p>
<p>
<s>
Malá	malý	k2eAgFnSc1d1	malá
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Gut	Gut	k1gMnSc1	Gut
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Pacina	Pacina	k1gMnSc1	Pacina
<g/>
,	,	kIx,	,
Olympia	Olympia	k1gFnSc1	Olympia
1986	[number]	k4	1986
</s>
</p>
<p>
<s>
Kronika	kronika	k1gFnSc1	kronika
českého	český	k2eAgInSc2d1	český
hokeje	hokej	k1gInSc2	hokej
–	–	k?	–
Miloslav	Miloslav	k1gMnSc1	Miloslav
Jenšík	Jenšík	k1gMnSc1	Jenšík
<g/>
,	,	kIx,	,
Olympia	Olympia	k1gFnSc1	Olympia
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
100	[number]	k4	100
let	let	k1gInSc1	let
českého	český	k2eAgInSc2d1	český
hokeje	hokej	k1gInSc2	hokej
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Gut	Gut	k1gMnSc1	Gut
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Prchal	Prchal	k1gMnSc1	Prchal
<g/>
,	,	kIx,	,
AS	as	k9	as
press	press	k6eAd1	press
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
federace	federace	k1gFnSc2	federace
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
IIHF	IIHF	kA	IIHF
–	–	k?	–
Historie	historie	k1gFnSc1	historie
IIHF	IIHF	kA	IIHF
</s>
</p>
<p>
<s>
hockeyarchives	hockeyarchives	k1gMnSc1	hockeyarchives
<g/>
.	.	kIx.	.
<g/>
info	info	k1gMnSc1	info
</s>
</p>
