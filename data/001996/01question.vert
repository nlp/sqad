<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejznámnějších	známný	k2eAgInPc2d3	nejznámnější
problémů	problém	k1gInPc2	problém
v	v	k7c6	v
kryptografii	kryptografie	k1gFnSc6	kryptografie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
útočník	útočník	k1gMnSc1	útočník
stává	stávat	k5eAaImIp3nS	stávat
aktivním	aktivní	k2eAgMnSc7d1	aktivní
prostředníkem	prostředník	k1gMnSc7	prostředník
při	při	k7c6	při
odposlouchávání	odposlouchávání	k1gNnSc6	odposlouchávání
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
?	?	kIx.	?
</s>
