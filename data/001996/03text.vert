<s>
Man-in-the-middle	Mannheiddle	k6eAd1	Man-in-the-middle
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
MITM	MITM	kA	MITM
<g/>
,	,	kIx,	,
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
"	"	kIx"	"
<g/>
člověk	člověk	k1gMnSc1	člověk
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
člověk	člověk	k1gMnSc1	člověk
mezi	mezi	k7c7	mezi
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
název	název	k1gInSc1	název
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
kryptografii	kryptografie	k1gFnSc4	kryptografie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
podstatou	podstata	k1gFnSc7	podstata
je	být	k5eAaImIp3nS	být
snaha	snaha	k1gFnSc1	snaha
útočníka	útočník	k1gMnSc2	útočník
odposlouchávat	odposlouchávat	k5eAaImF	odposlouchávat
komunikaci	komunikace	k1gFnSc4	komunikace
mezi	mezi	k7c7	mezi
účastníky	účastník	k1gMnPc7	účastník
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
aktivním	aktivní	k2eAgInSc7d1	aktivní
prostředníkem	prostředník	k1gInSc7	prostředník
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
obrázku	obrázek	k1gInSc2	obrázek
vpravo	vpravo	k6eAd1	vpravo
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
současných	současný	k2eAgFnPc6d1	současná
počítačových	počítačový	k2eAgFnPc6d1	počítačová
sítích	síť	k1gFnPc6	síť
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
útočník	útočník	k1gMnSc1	útočník
(	(	kIx(	(
<g/>
Mallory	Mallora	k1gFnPc1	Mallora
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
fyzické	fyzický	k2eAgFnSc6d1	fyzická
cestě	cesta	k1gFnSc6	cesta
mezi	mezi	k7c4	mezi
účastníky	účastník	k1gMnPc4	účastník
(	(	kIx(	(
<g/>
Alice	Alice	k1gFnSc1	Alice
a	a	k8xC	a
Bob	Bob	k1gMnSc1	Bob
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
síťový	síťový	k2eAgInSc4d1	síťový
provoz	provoz	k1gInSc4	provoz
lze	lze	k6eAd1	lze
přesměrovat	přesměrovat	k5eAaPmF	přesměrovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
příkladu	příklad	k1gInSc6	příklad
jsou	být	k5eAaImIp3nP	být
použita	použit	k2eAgNnPc1d1	použito
standardní	standardní	k2eAgNnPc1d1	standardní
jména	jméno	k1gNnPc1	jméno
Alice	Alice	k1gFnSc2	Alice
a	a	k8xC	a
Bob	Bob	k1gMnSc1	Bob
(	(	kIx(	(
<g/>
osoby	osoba	k1gFnPc1	osoba
"	"	kIx"	"
<g/>
A	a	k9	a
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
B	B	kA	B
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
funkce	funkce	k1gFnSc2	funkce
a	a	k8xC	a
problémů	problém	k1gInPc2	problém
kryptografických	kryptografický	k2eAgInPc2d1	kryptografický
protokolů	protokol	k1gInPc2	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Útočníkem	útočník	k1gMnSc7	útočník
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
Mallory	Mallora	k1gFnSc2	Mallora
(	(	kIx(	(
<g/>
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
útočník	útočník	k1gMnSc1	útočník
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
malicious	malicious	k1gInSc1	malicious
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
běžné	běžný	k2eAgFnSc6d1	běžná
nezabezpečené	zabezpečený	k2eNgFnSc6d1	nezabezpečená
komunikaci	komunikace	k1gFnSc6	komunikace
je	být	k5eAaImIp3nS	být
úloha	úloha	k1gFnSc1	úloha
Malloryho	Mallory	k1gMnSc2	Mallory
velmi	velmi	k6eAd1	velmi
snadná	snadný	k2eAgFnSc1d1	snadná
<g/>
.	.	kIx.	.
</s>
<s>
Stačí	stačit	k5eAaBmIp3nS	stačit
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
ke	k	k7c3	k
komunikačnímu	komunikační	k2eAgInSc3d1	komunikační
kanálu	kanál	k1gInSc3	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Pouhým	pouhý	k2eAgNnSc7d1	pouhé
sledováním	sledování	k1gNnSc7	sledování
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
posílá	posílat	k5eAaImIp3nS	posílat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
komunikaci	komunikace	k1gFnSc4	komunikace
odposlouchávat	odposlouchávat	k5eAaImF	odposlouchávat
<g/>
.	.	kIx.	.
</s>
<s>
Přerušením	přerušení	k1gNnSc7	přerušení
kanálu	kanál	k1gInSc2	kanál
<g/>
,	,	kIx,	,
příjmem	příjem	k1gInSc7	příjem
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
vysíláním	vysílání	k1gNnSc7	vysílání
k	k	k7c3	k
druhé	druhý	k4xOgFnSc3	druhý
straně	strana	k1gFnSc3	strana
účastníků	účastník	k1gMnPc2	účastník
komunikace	komunikace	k1gFnSc2	komunikace
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
snadno	snadno	k6eAd1	snadno
měnit	měnit	k5eAaImF	měnit
obsah	obsah	k1gInSc4	obsah
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
řešení	řešení	k1gNnSc1	řešení
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
je	být	k5eAaImIp3nS	být
využití	využití	k1gNnSc1	využití
veřejných	veřejný	k2eAgInPc2d1	veřejný
klíčů	klíč	k1gInPc2	klíč
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
i	i	k9	i
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
narazit	narazit	k5eAaPmF	narazit
na	na	k7c4	na
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Alice	Alice	k1gFnSc1	Alice
chce	chtít	k5eAaImIp3nS	chtít
poslat	poslat	k5eAaPmF	poslat
zprávu	zpráva	k1gFnSc4	zpráva
Bobovi	Bob	k1gMnSc3	Bob
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
si	se	k3xPyFc3	se
proto	proto	k8xC	proto
chtějí	chtít	k5eAaImIp3nP	chtít
vyměnit	vyměnit	k5eAaPmF	vyměnit
své	svůj	k3xOyFgInPc4	svůj
veřejné	veřejný	k2eAgInPc4d1	veřejný
klíče	klíč	k1gInPc4	klíč
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pak	pak	k6eAd1	pak
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
budou	být	k5eAaImBp3nP	být
komunikovat	komunikovat	k5eAaImF	komunikovat
bezpečně	bezpečně	k6eAd1	bezpečně
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
komunikaci	komunikace	k1gFnSc4	komunikace
Alice	Alice	k1gFnSc2	Alice
a	a	k8xC	a
Boba	Bob	k1gMnSc2	Bob
se	se	k3xPyFc4	se
ale	ale	k9	ale
dostala	dostat	k5eAaPmAgFnS	dostat
třetí	třetí	k4xOgFnSc1	třetí
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
Mallory	Mallor	k1gInPc1	Mallor
<g/>
.	.	kIx.	.
</s>
<s>
Alice	Alice	k1gFnSc1	Alice
tedy	tedy	k9	tedy
posílá	posílat	k5eAaImIp3nS	posílat
zprávu	zpráva	k1gFnSc4	zpráva
Bobovi	Bob	k1gMnSc3	Bob
<g/>
.	.	kIx.	.
</s>
<s>
Mallory	Mallora	k1gFnPc1	Mallora
zachytí	zachytit	k5eAaPmIp3nP	zachytit
veřejný	veřejný	k2eAgInSc4d1	veřejný
klíč	klíč	k1gInSc4	klíč
Alice	Alice	k1gFnSc2	Alice
a	a	k8xC	a
nahradí	nahradit	k5eAaPmIp3nS	nahradit
ho	on	k3xPp3gInSc4	on
svým	svůj	k3xOyFgInSc7	svůj
veřejným	veřejný	k2eAgInSc7d1	veřejný
klíčem	klíč	k1gInSc7	klíč
(	(	kIx(	(
<g/>
s	s	k7c7	s
obsaženou	obsažený	k2eAgFnSc7d1	obsažená
falešnou	falešný	k2eAgFnSc7d1	falešná
informací	informace	k1gFnSc7	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Alice	Alice	k1gFnSc2	Alice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bob	Bob	k1gMnSc1	Bob
pošle	poslat	k5eAaPmIp3nS	poslat
veřejný	veřejný	k2eAgInSc4d1	veřejný
klíč	klíč	k1gInSc4	klíč
Alici	Alice	k1gFnSc4	Alice
a	a	k8xC	a
opět	opět	k6eAd1	opět
ho	on	k3xPp3gMnSc4	on
zachytí	zachytit	k5eAaPmIp3nP	zachytit
Mallory	Mallora	k1gFnPc1	Mallora
a	a	k8xC	a
vymění	vyměnit	k5eAaPmIp3nP	vyměnit
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
klíč	klíč	k1gInSc4	klíč
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
falešnou	falešný	k2eAgFnSc4d1	falešná
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Boba	Bob	k1gMnSc2	Bob
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
si	se	k3xPyFc3	se
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
(	(	kIx(	(
<g/>
Alice	Alice	k1gFnSc1	Alice
a	a	k8xC	a
Bob	Bob	k1gMnSc1	Bob
<g/>
)	)	kIx)	)
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
veřejný	veřejný	k2eAgInSc4d1	veřejný
klíč	klíč	k1gInSc4	klíč
toho	ten	k3xDgNnSc2	ten
druhého	druhý	k4xOgNnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
Malloryho	Mallory	k1gMnSc4	Mallory
klíče	klíč	k1gInSc2	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Cokoliv	cokoliv	k3yInSc1	cokoliv
si	se	k3xPyFc3	se
Alice	Alice	k1gFnSc1	Alice
a	a	k8xC	a
Bob	Bob	k1gMnSc1	Bob
pošlou	poslat	k5eAaPmIp3nP	poslat
<g/>
,	,	kIx,	,
Mallory	Mallora	k1gFnPc1	Mallora
zachytí	zachytit	k5eAaPmIp3nP	zachytit
<g/>
,	,	kIx,	,
dešifruje	dešifrovat	k5eAaBmIp3nS	dešifrovat
<g/>
,	,	kIx,	,
přečte	přečíst	k5eAaPmIp3nS	přečíst
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
zašifruje	zašifrovat	k5eAaPmIp3nS	zašifrovat
a	a	k8xC	a
odešle	odeslat	k5eAaPmIp3nS	odeslat
druhému	druhý	k4xOgInSc3	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Zprávu	zpráva	k1gFnSc4	zpráva
může	moct	k5eAaImIp3nS	moct
dokonce	dokonce	k9	dokonce
i	i	k9	i
změnit	změnit	k5eAaPmF	změnit
a	a	k8xC	a
poslat	poslat	k5eAaPmF	poslat
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
pravá	pravý	k2eAgFnSc1d1	pravá
<g/>
.	.	kIx.	.
</s>
<s>
Alice	Alice	k1gFnSc1	Alice
ani	ani	k8xC	ani
Bob	Bob	k1gMnSc1	Bob
nic	nic	k3yNnSc1	nic
nepoznají	poznat	k5eNaPmIp3nP	poznat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
MITM	MITM	kA	MITM
útoku	útok	k1gInSc6	útok
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
spoléhání	spoléhání	k1gNnSc2	spoléhání
na	na	k7c4	na
digitální	digitální	k2eAgInSc4d1	digitální
certifikát	certifikát	k1gInSc4	certifikát
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
útočník	útočník	k1gMnSc1	útočník
(	(	kIx(	(
<g/>
Mallory	Mallora	k1gFnPc1	Mallora
<g/>
)	)	kIx)	)
filtroval	filtrovat	k5eAaImAgInS	filtrovat
celou	celý	k2eAgFnSc4d1	celá
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Stačí	stačit	k5eAaBmIp3nS	stačit
otrávit	otrávit	k5eAaPmF	otrávit
DNS	DNS	kA	DNS
nebo	nebo	k8xC	nebo
ARP	ARP	kA	ARP
cache	cachat	k5eAaPmIp3nS	cachat
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ho	on	k3xPp3gInSc4	on
nevědomky	nevědomky	k6eAd1	nevědomky
přesměrovat	přesměrovat	k5eAaPmF	přesměrovat
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
webové	webový	k2eAgInPc4d1	webový
servery	server	k1gInPc4	server
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
uživatel	uživatel	k1gMnSc1	uživatel
do	do	k7c2	do
svého	svůj	k3xOyFgMnSc2	svůj
prohlížeče	prohlížeč	k1gMnSc2	prohlížeč
bez	bez	k7c2	bez
ověření	ověření	k1gNnSc2	ověření
nainstaluje	nainstalovat	k5eAaPmIp3nS	nainstalovat
falešný	falešný	k2eAgInSc4d1	falešný
kořenový	kořenový	k2eAgInSc4d1	kořenový
certifikát	certifikát	k1gInSc4	certifikát
certifikační	certifikační	k2eAgFnSc2d1	certifikační
autority	autorita	k1gFnSc2	autorita
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
pak	pak	k6eAd1	pak
důvěřovat	důvěřovat	k5eAaImF	důvěřovat
falešnému	falešný	k2eAgInSc3d1	falešný
elektronickému	elektronický	k2eAgInSc3d1	elektronický
podpisu	podpis	k1gInSc3	podpis
nebo	nebo	k8xC	nebo
phishingové	phishingový	k2eAgFnSc3d1	phishingová
webové	webový	k2eAgFnSc3d1	webová
stránce	stránka	k1gFnSc3	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Útok	útok	k1gInSc1	útok
Man	Man	k1gMnSc1	Man
in	in	k?	in
the	the	k?	the
middle	middle	k6eAd1	middle
lze	lze	k6eAd1	lze
řešit	řešit	k5eAaImF	řešit
několika	několik	k4yIc7	několik
způsoby	způsob	k1gInPc7	způsob
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
příkladu	příklad	k1gInSc2	příklad
uvedeného	uvedený	k2eAgInSc2d1	uvedený
výše	vysoce	k6eAd2	vysoce
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
vzájemnou	vzájemný	k2eAgFnSc7d1	vzájemná
výměnou	výměna	k1gFnSc7	výměna
veřejných	veřejný	k2eAgInPc2d1	veřejný
klíčů	klíč	k1gInPc2	klíč
jiným	jiný	k2eAgInSc7d1	jiný
<g/>
,	,	kIx,	,
bezpečným	bezpečný	k2eAgInSc7d1	bezpečný
kanálem	kanál	k1gInSc7	kanál
(	(	kIx(	(
<g/>
požadavkem	požadavek	k1gInSc7	požadavek
na	na	k7c4	na
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
kanál	kanál	k1gInSc4	kanál
pro	pro	k7c4	pro
výměnu	výměna	k1gFnSc4	výměna
klíčů	klíč	k1gInPc2	klíč
ovšem	ovšem	k9	ovšem
přicházíme	přicházet	k5eAaImIp1nP	přicházet
o	o	k7c4	o
zásadní	zásadní	k2eAgFnSc4d1	zásadní
výhodu	výhoda	k1gFnSc4	výhoda
asymetrické	asymetrický	k2eAgFnSc2d1	asymetrická
kryptografie	kryptografie	k1gFnSc2	kryptografie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ideálně	ideálně	k6eAd1	ideálně
takový	takový	k3xDgInSc4	takový
kanál	kanál	k1gInSc4	kanál
<g />
.	.	kIx.	.
</s>
<s>
nepotřebujeme	potřebovat	k5eNaImIp1nP	potřebovat
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ověřením	ověření	k1gNnSc7	ověření
získaných	získaný	k2eAgInPc2d1	získaný
veřejných	veřejný	k2eAgInPc2d1	veřejný
klíčů	klíč	k1gInPc2	klíč
jiným	jiný	k2eAgInSc7d1	jiný
bezpečným	bezpečný	k2eAgInSc7d1	bezpečný
kanálem	kanál	k1gInSc7	kanál
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
pomocí	pomocí	k7c2	pomocí
jejich	jejich	k3xOp3gInSc2	jejich
otisku	otisk	k1gInSc2	otisk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
telefonicky	telefonicky	k6eAd1	telefonicky
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ověřením	ověření	k1gNnSc7	ověření
klíčů	klíč	k1gInPc2	klíč
pomocí	pomocí	k7c2	pomocí
elektronického	elektronický	k2eAgInSc2d1	elektronický
podpisu	podpis	k1gInSc2	podpis
Alice	Alice	k1gFnSc2	Alice
i	i	k8xC	i
Boba	Bob	k1gMnSc4	Bob
pomocí	pomocí	k7c2	pomocí
certifikační	certifikační	k2eAgFnSc2d1	certifikační
autority	autorita	k1gFnSc2	autorita
nebo	nebo	k8xC	nebo
sítě	síť	k1gFnSc2	síť
důvěry	důvěra	k1gFnSc2	důvěra
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
digitální	digitální	k2eAgInSc4d1	digitální
certifikát	certifikát	k1gInSc4	certifikát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
kryptografie	kryptografie	k1gFnSc1	kryptografie
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
připravit	připravit	k5eAaPmF	připravit
takový	takový	k3xDgInSc4	takový
kanál	kanál	k1gInSc4	kanál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ideálním	ideální	k2eAgInSc6d1	ideální
případě	případ	k1gInSc6	případ
teoreticky	teoreticky	k6eAd1	teoreticky
neodposlouchávatelný	odposlouchávatelný	k2eNgInSc1d1	odposlouchávatelný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
každou	každý	k3xTgFnSc4	každý
snahu	snaha	k1gFnSc4	snaha
o	o	k7c4	o
odposlouchávání	odposlouchávání	k1gNnSc4	odposlouchávání
dokáže	dokázat	k5eAaPmIp3nS	dokázat
pravý	pravý	k2eAgMnSc1d1	pravý
příjemce	příjemce	k1gMnSc1	příjemce
detekovat	detekovat	k5eAaImF	detekovat
<g/>
.	.	kIx.	.
</s>
<s>
Útok	útok	k1gInSc4	útok
postranním	postranní	k2eAgInSc7d1	postranní
kanálem	kanál	k1gInSc7	kanál
</s>
