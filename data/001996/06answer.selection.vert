<s>
Man-in-the-middle	Mannheiddle	k6eAd1	Man-in-the-middle
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
MITM	MITM	kA	MITM
<g/>
,	,	kIx,	,
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
"	"	kIx"	"
<g/>
člověk	člověk	k1gMnSc1	člověk
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
člověk	člověk	k1gMnSc1	člověk
mezi	mezi	k7c7	mezi
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
název	název	k1gInSc1	název
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
kryptografii	kryptografie	k1gFnSc4	kryptografie
<g/>
.	.	kIx.	.
</s>
