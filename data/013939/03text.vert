<s>
Katedra	katedra	k1gFnSc1
jaderné	jaderný	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
FJFI	FJFI	kA
ČVUT	ČVUT	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Katedra	katedra	k1gFnSc1
jaderné	jaderný	k2eAgFnSc2d1
chemieFakulta	chemieFakulta	k1gFnSc1
jaderná	jaderný	k2eAgFnSc1d1
a	a	k8xC
fyzikálně	fyzikálně	k6eAd1
inženýrská	inženýrský	k2eAgFnSc1d1
Českého	český	k2eAgNnSc2d1
vysokého	vysoký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
technického	technický	k2eAgNnSc2d1
v	v	k7c4
PrazeDepartment	PrazeDepartment	k1gInSc4
of	of	k?
Nuclear	Nuclear	k1gInSc1
Chemistry	Chemistr	k1gMnPc4
FNSPE	FNSPE	kA
CTU	CTU	kA
in	in	k?
Prague	Prague	k1gNnSc2
Břehová	břehový	k2eAgFnSc1d1
7	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
-	-	kIx~
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
-	-	kIx~
sídlo	sídlo	k1gNnSc1
KJCH	KJCH	kA
Statistické	statistický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
k	k	k7c3
2018	#num#	k4
Zaměstnanci	zaměstnanec	k1gMnPc1
</s>
<s>
26	#num#	k4
Studijní	studijní	k2eAgInSc1d1
program	program	k1gInSc1
Bakalářský	bakalářský	k2eAgInSc1d1
</s>
<s>
64	#num#	k4
Navaz	Navaz	k1gInSc1
<g/>
.	.	kIx.
magisterský	magisterský	k2eAgInSc1d1
</s>
<s>
18	#num#	k4
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Katedra	katedra	k1gFnSc1
jaderné	jaderný	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
Fakulty	fakulta	k1gFnSc2
technické	technický	k2eAgFnSc2d1
a	a	k8xC
jaderné	jaderný	k2eAgFnSc2d1
fysiky	fysika	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc4
1957	#num#	k4
Kontaktní	kontaktní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Břehová	břehový	k2eAgFnSc1d1
7	#num#	k4
<g/>
,	,	kIx,
115	#num#	k4
19	#num#	k4
Praha	Praha	k1gFnSc1
1	#num#	k4
-	-	kIx~
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
https://www.jaderna-chemie.cz/	https://www.jaderna-chemie.cz/	k?
</s>
<s>
Katedra	katedra	k1gFnSc1
jaderné	jaderný	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
KJCH	KJCH	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednou	jednou	k4c7
z	z	k7c2
10	#num#	k4c2
kateder	katedra	k1gFnPc2
Fakulty	fakulta	k1gFnSc2
jaderné	jaderný	k2eAgFnSc2d1
a	a	k8xC
fyzikálně	fyzikálně	k6eAd1
inženýrské	inženýrský	k2eAgFnSc2d1
Českého	český	k2eAgNnSc2d1
vysokého	vysoký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
technického	technický	k2eAgNnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
byla	být	k5eAaImAgFnS
druhou	druhý	k4xOgFnSc7
největší	veliký	k2eAgFnSc7d3
katedrou	katedra	k1gFnSc7
fakulty	fakulta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
katedru	katedra	k1gFnSc4
absolvovalo	absolvovat	k5eAaPmAgNnS
453	#num#	k4
inženýrů	inženýr	k1gMnPc2
jaderné	jaderný	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Úvahy	úvaha	k1gFnPc1
o	o	k7c6
založení	založení	k1gNnSc6
</s>
<s>
První	první	k4xOgInPc1
náznaky	náznak	k1gInPc1
snah	snaha	k1gFnPc2
o	o	k7c6
založení	založení	k1gNnSc6
katedry	katedra	k1gFnSc2
jaderné	jaderný	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
se	se	k3xPyFc4
datují	datovat	k5eAaImIp3nP
do	do	k7c2
poloviny	polovina	k1gFnSc2
roku	rok	k1gInSc2
1955	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
při	při	k7c6
Ministerstvu	ministerstvo	k1gNnSc6
školství	školství	k1gNnSc2
a	a	k8xC
kultury	kultura	k1gFnSc2
Komise	komise	k1gFnSc2
pro	pro	k7c4
vypracování	vypracování	k1gNnSc4
návrhu	návrh	k1gInSc2
na	na	k7c4
výchovu	výchova	k1gFnSc4
kádrů	kádr	k1gInPc2
v	v	k7c6
jaderné	jaderný	k2eAgFnSc6d1
fyzice	fyzika	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
jaderné	jaderný	k2eAgFnSc6d1
chemii	chemie	k1gFnSc6
a	a	k8xC
v	v	k7c6
jaderném	jaderný	k2eAgNnSc6d1
inženýrství	inženýrství	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Založení	založení	k1gNnSc1
a	a	k8xC
první	první	k4xOgInPc4
roky	rok	k1gInPc4
existence	existence	k1gFnSc2
</s>
<s>
Katedra	katedra	k1gFnSc1
jaderné	jaderný	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
byla	být	k5eAaImAgFnS
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
tří	tři	k4xCgFnPc2
prvních	první	k4xOgFnPc2
kateder	katedra	k1gFnPc2
fakulty	fakulta	k1gFnSc2
<g/>
,	,	kIx,
založena	založen	k2eAgFnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1957	#num#	k4
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc4
Fakulty	fakulta	k1gFnSc2
technické	technický	k2eAgFnSc2d1
a	a	k8xC
jaderné	jaderný	k2eAgFnSc2d1
fysiky	fysika	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výuka	výuka	k1gFnSc1
chemiků	chemik	k1gMnPc2
však	však	k9
probíhala	probíhat	k5eAaImAgFnS
již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1955	#num#	k4
<g/>
;	;	kIx,
nikoliv	nikoliv	k9
však	však	k9
pod	pod	k7c7
hlavičkou	hlavička	k1gFnSc7
Katedry	katedra	k1gFnSc2
jaderné	jaderný	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
prvního	první	k4xOgInSc2
ročníku	ročník	k1gInSc2
nastoupilo	nastoupit	k5eAaPmAgNnS
zhruba	zhruba	k6eAd1
60	#num#	k4
studentů	student	k1gMnPc2
<g/>
,	,	kIx,
vesměs	vesměs	k6eAd1
studentů	student	k1gMnPc2
Vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
chemicko-technologické	chemicko-technologický	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
však	však	k9
zájem	zájem	k1gInSc4
o	o	k7c4
obor	obor	k1gInSc4
opadal	opadat	k5eAaBmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
byla	být	k5eAaImAgFnS
výuka	výuka	k1gFnSc1
problematická	problematický	k2eAgFnSc1d1
-	-	kIx~
bylo	být	k5eAaImAgNnS
málo	málo	k6eAd1
profesorů	profesor	k1gMnPc2
a	a	k8xC
katedře	katedra	k1gFnSc6
(	(	kIx(
<g/>
ba	ba	k9
ani	ani	k8xC
fakultě	fakulta	k1gFnSc6
<g/>
)	)	kIx)
nebyla	být	k5eNaImAgFnS
přidělena	přidělen	k2eAgFnSc1d1
žádná	žádný	k3yNgFnSc1
budova	budova	k1gFnSc1
–	–	k?
původně	původně	k6eAd1
se	se	k3xPyFc4
totiž	totiž	k9
plánovalo	plánovat	k5eAaImAgNnS
postavení	postavení	k1gNnSc1
vlastní	vlastní	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
však	však	k9
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
odložilo	odložit	k5eAaPmAgNnS
na	na	k7c4
neurčito	neurčito	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
tam	tam	k6eAd1
byla	být	k5eAaImAgFnS
fakultě	fakulta	k1gFnSc6
přidělena	přidělen	k2eAgFnSc1d1
budova	budova	k1gFnSc1
v	v	k7c6
Břehové	břehový	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
však	však	k8xC
problém	problém	k1gInSc4
nevyřešilo	vyřešit	k5eNaPmAgNnS
<g/>
,	,	kIx,
neboť	neboť	k8xC
v	v	k7c6
prvních	první	k4xOgNnPc6
letech	léto	k1gNnPc6
fungování	fungování	k1gNnSc2
musela	muset	k5eAaImAgFnS
projít	projít	k5eAaPmF
rozsáhlou	rozsáhlý	k2eAgFnSc7d1
rekonstrukcí	rekonstrukce	k1gFnSc7
a	a	k8xC
adaptací	adaptace	k1gFnSc7
prostorů	prostor	k1gInPc2
tak	tak	k8xS,k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
do	do	k7c2
ní	on	k3xPp3gFnSc2
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
umístit	umístit	k5eAaPmF
laboratoře	laboratoř	k1gFnPc4
a	a	k8xC
jiné	jiný	k2eAgInPc4d1
specifické	specifický	k2eAgInPc4d1
prostory	prostor	k1gInPc4
(	(	kIx(
<g/>
plné	plný	k2eAgNnSc4d1
využívání	využívání	k1gNnSc4
tedy	tedy	k9
začalo	začít	k5eAaPmAgNnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přechod	přechod	k1gInSc1
katedry	katedra	k1gFnSc2
pod	pod	k7c7
ČVUT	ČVUT	kA
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
<g/>
,	,	kIx,
při	při	k7c6
přechodu	přechod	k1gInSc6
fakulty	fakulta	k1gFnSc2
z	z	k7c2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
na	na	k7c6
ČVUT	ČVUT	kA
<g/>
,	,	kIx,
se	se	k3xPyFc4
členové	člen	k1gMnPc1
katedry	katedra	k1gFnSc2
proti	proti	k7c3
tomuto	tento	k3xDgInSc3
přesunu	přesun	k1gInSc3
ostře	ostro	k6eAd1
ohradili	ohradit	k5eAaPmAgMnP
<g/>
,	,	kIx,
neboť	neboť	k8xC
na	na	k7c4
ČVUT	ČVUT	kA
se	se	k3xPyFc4
nevyučoval	vyučovat	k5eNaImAgMnS
žádný	žádný	k3yNgInSc4
jiný	jiný	k2eAgInSc4d1
chemický	chemický	k2eAgInSc4d1
obor	obor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
přechodu	přechod	k1gInSc3
i	i	k9
tak	tak	k6eAd1
došlo	dojít	k5eAaPmAgNnS
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
P	P	kA
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
to	ten	k3xDgNnSc1
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1959	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Přírodovědecké	přírodovědecký	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
vznikla	vzniknout	k5eAaPmAgFnS
Katedra	katedra	k1gFnSc1
organické	organický	k2eAgFnSc2d1
a	a	k8xC
jaderné	jaderný	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
(	(	kIx(
<g/>
tehdy	tehdy	k6eAd1
Katedra	katedra	k1gFnSc1
radiochemie	radiochemie	k1gFnSc2
<g/>
)	)	kIx)
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
výuka	výuka	k1gFnSc1
jaderné	jaderný	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
zůstala	zůstat	k5eAaPmAgFnS
zachována	zachován	k2eAgFnSc1d1
(	(	kIx(
<g/>
výuka	výuka	k1gFnSc1
tohoto	tento	k3xDgInSc2
oboru	obor	k1gInSc2
však	však	k9
zanikla	zaniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Plánované	plánovaný	k2eAgNnSc1d1
sloučení	sloučení	k1gNnSc1
s	s	k7c7
VŠCHT	VŠCHT	kA
</s>
<s>
Většina	většina	k1gFnSc1
přednášek	přednáška	k1gFnPc2
a	a	k8xC
laboratorních	laboratorní	k2eAgFnPc2d1
praktik	praktika	k1gFnPc2
se	se	k3xPyFc4
v	v	k7c6
prvních	první	k4xOgNnPc6
letech	léto	k1gNnPc6
výuky	výuka	k1gFnSc2
prováděla	provádět	k5eAaImAgFnS
na	na	k7c6
Přírodovědecké	přírodovědecký	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
na	na	k7c6
Albertově	Albertův	k2eAgInSc6d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
1959	#num#	k4
nacházelo	nacházet	k5eAaImAgNnS
detašované	detašovaný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
katedry	katedra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problém	problém	k1gInSc1
s	s	k7c7
nedostatkem	nedostatek	k1gInSc7
volných	volný	k2eAgFnPc2d1
kapacit	kapacita	k1gFnPc2
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
vyřešit	vyřešit	k5eAaPmF
založením	založení	k1gNnSc7
Katedry	katedra	k1gFnSc2
radiochemie	radiochemie	k1gFnSc2
na	na	k7c4
VŠCHT	VŠCHT	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
by	by	kYmCp3nP
studovali	studovat	k5eAaImAgMnP
studenti	student	k1gMnPc1
jak	jak	k8xC,k8xS
VŠCHT	VŠCHT	kA
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
ČVUT	ČVUT	kA
(	(	kIx(
<g/>
původně	původně	k6eAd1
katedra	katedra	k1gFnSc1
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
přeměněna	přeměnit	k5eAaPmNgFnS
na	na	k7c4
katedru	katedra	k1gFnSc4
obecné	obecný	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
plán	plán	k1gInSc1
se	se	k3xPyFc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
převedení	převedení	k1gNnSc1
celé	celý	k2eAgFnSc2d1
Katedry	katedra	k1gFnSc2
jaderné	jaderný	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
pod	pod	k7c7
VŠCHT	VŠCHT	kA
<g/>
,	,	kIx,
však	však	k9
neuskutečnil	uskutečnit	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
však	však	k8xC
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
zřízení	zřízení	k1gNnSc3
katedry	katedra	k1gFnSc2
technologie	technologie	k1gFnSc2
jaderných	jaderný	k2eAgNnPc2d1
paliv	palivo	k1gNnPc2
a	a	k8xC
radiochemie	radiochemie	k1gFnSc2
na	na	k7c6
VŠCHT	VŠCHT	kA
a	a	k8xC
její	její	k3xOp3gFnPc4
studenti	student	k1gMnPc1
na	na	k7c4
některé	některý	k3yIgInPc4
předměty	předmět	k1gInPc4
docházeli	docházet	k5eAaImAgMnP
na	na	k7c4
katedru	katedra	k1gFnSc4
jaderné	jaderný	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
do	do	k7c2
jejího	její	k3xOp3gNnSc2
zrušení	zrušení	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
(	(	kIx(
<g/>
respektive	respektive	k9
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Plánovaný	plánovaný	k2eAgInSc1d1
přechod	přechod	k1gInSc1
pod	pod	k7c7
UK	UK	kA
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1962	#num#	k4
se	se	k3xPyFc4
uvažovalo	uvažovat	k5eAaImAgNnS
o	o	k7c4
fúzi	fúze	k1gFnSc4
celé	celý	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
technické	technický	k2eAgFnSc2d1
a	a	k8xC
jaderné	jaderný	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
pod	pod	k7c4
Matematicko-fyzikální	matematicko-fyzikální	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
UK	UK	kA
<g/>
,	,	kIx,
avšak	avšak	k8xC
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
katedra	katedra	k1gFnSc1
jaderné	jaderný	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
by	by	kYmCp3nS
se	se	k3xPyFc4
sloučila	sloučit	k5eAaPmAgFnS
s	s	k7c7
Přírodovědnou	přírodovědný	k2eAgFnSc7d1
fakultou	fakulta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdejší	tehdejší	k2eAgFnSc1d1
vedoucí	vedoucí	k1gFnSc1
katedry	katedra	k1gFnSc2
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
Majer	Majer	k1gMnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
proti	proti	k7c3
tomuto	tento	k3xDgInSc3
kroku	krok	k1gInSc3
ohradil	ohradit	k5eAaPmAgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
byl	být	k5eAaImAgMnS
toho	ten	k3xDgInSc2
názoru	názor	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
fakultu	fakulta	k1gFnSc4
rozdělit	rozdělit	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
že	že	k8xS
všechny	všechen	k3xTgInPc1
obory	obor	k1gInPc1
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
pohromadě	pohromadě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nápad	nápad	k1gInSc1
na	na	k7c4
takový	takový	k3xDgInSc4
přechod	přechod	k1gInSc4
nakonec	nakonec	k6eAd1
vyšuměl	vyšumět	k5eAaPmAgMnS
do	do	k7c2
prázdna	prázdno	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Plánované	plánovaný	k2eAgNnSc1d1
rozšíření	rozšíření	k1gNnSc1
kateder	katedra	k1gFnPc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
bylo	být	k5eAaImAgNnS
uvažováno	uvažován	k2eAgNnSc1d1
o	o	k7c6
vyčlenění	vyčlenění	k1gNnSc6
fyzikální	fyzikální	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
a	a	k8xC
radiační	radiační	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
do	do	k7c2
samostatných	samostatný	k2eAgFnPc2d1
kateder	katedra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
plán	plán	k1gInSc1
se	se	k3xPyFc4
však	však	k9
také	také	k9
neuskutečnil	uskutečnit	k5eNaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
studia	studio	k1gNnSc2
</s>
<s>
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
1955	#num#	k4
a	a	k8xC
1961	#num#	k4
získal	získat	k5eAaPmAgMnS
student	student	k1gMnSc1
po	po	k7c6
pěti	pět	k4xCc6
letech	léto	k1gNnPc6
studia	studio	k1gNnSc2
oboru	obor	k1gInSc6
Jaderná	jaderný	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
titul	titul	k1gInSc4
promovaný	promovaný	k2eAgMnSc1d1
chemik	chemik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1962	#num#	k4
se	se	k3xPyFc4
název	název	k1gInSc1
oboru	obor	k1gInSc2
změnil	změnit	k5eAaPmAgInS
na	na	k7c4
Fysikální	fysikální	k2eAgNnSc4d1
a	a	k8xC
jaderná	jaderný	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
včetně	včetně	k7c2
chemie	chemie	k1gFnSc2
radiační	radiační	k2eAgFnSc2d1
a	a	k8xC
dosimetrie	dosimetrie	k1gFnSc2
a	a	k8xC
absolventi	absolvent	k1gMnPc1
již	již	k6eAd1
získali	získat	k5eAaPmAgMnP
titul	titul	k1gInSc4
inženýr	inženýr	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
P	P	kA
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
se	se	k3xPyFc4
však	však	k9
obor	obor	k1gInSc1
vrátil	vrátit	k5eAaPmAgInS
ke	k	k7c3
svému	svůj	k3xOyFgInSc3
původnímu	původní	k2eAgInSc3d1
názvu	název	k1gInSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
ho	on	k3xPp3gMnSc4
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
změnil	změnit	k5eAaPmAgInS
na	na	k7c4
Jaderně-chemické	Jaderně-chemický	k2eAgNnSc4d1
inženýrství	inženýrství	k1gNnSc4
<g/>
,	,	kIx,
kterýžto	kterýžto	k?
název	název	k1gInSc1
vydržel	vydržet	k5eAaPmAgMnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
nese	nést	k5eAaImIp3nS
opět	opět	k6eAd1
obor	obor	k1gInSc1
název	název	k1gInSc1
Jaderná	jaderný	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
(	(	kIx(
<g/>
magisterské	magisterský	k2eAgNnSc1d1
studium	studium	k1gNnSc1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
rozděleno	rozdělit	k5eAaPmNgNnS
na	na	k7c6
zaměření	zaměření	k1gNnSc6
s	s	k7c7
názvem	název	k1gInSc7
Aplikovaná	aplikovaný	k2eAgFnSc1d1
jaderná	jaderný	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
a	a	k8xC
Jaderná	jaderný	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
v	v	k7c6
biologii	biologie	k1gFnSc6
a	a	k8xC
medicíně	medicína	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1
s	s	k7c7
PřF	PřF	k1gFnSc7
UK	UK	kA
</s>
<s>
Až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1972	#num#	k4
(	(	kIx(
<g/>
kromě	kromě	k7c2
přestávky	přestávka	k1gFnSc2
v	v	k7c6
letech	léto	k1gNnPc6
1962	#num#	k4
až	až	k9
1964	#num#	k4
<g/>
)	)	kIx)
probíhala	probíhat	k5eAaImAgFnS
výuka	výuka	k1gFnSc1
prvních	první	k4xOgInPc2
dvou	dva	k4xCgInPc2
ročníků	ročník	k1gInPc2
takřka	takřka	k6eAd1
výhradně	výhradně	k6eAd1
na	na	k7c4
PřF	PřF	k1gFnSc4
UK	UK	kA
(	(	kIx(
<g/>
výuka	výuka	k1gFnSc1
chemických	chemický	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
a	a	k8xC
mineralogie	mineralogie	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
MFF	MFF	kA
UK	UK	kA
(	(	kIx(
<g/>
výuka	výuka	k1gFnSc1
matematických	matematický	k2eAgInPc2d1
a	a	k8xC
fyzikálních	fyzikální	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
zmíněné	zmíněný	k2eAgFnSc2d1
tříleté	tříletý	k2eAgFnSc2d1
přestávky	přestávka	k1gFnSc2
byla	být	k5eAaImAgFnS
výuka	výuka	k1gFnSc1
přesunuta	přesunout	k5eAaPmNgFnS
na	na	k7c4
Fakultu	fakulta	k1gFnSc4
jadernou	jaderný	k2eAgFnSc4d1
a	a	k8xC
fyzikálně	fyzikálně	k6eAd1
inženýrskou	inženýrský	k2eAgFnSc7d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
však	však	k9
neosvědčilo	osvědčit	k5eNaPmAgNnS
hlavně	hlavně	k9
kvůli	kvůli	k7c3
absenci	absence	k1gFnSc3
chemických	chemický	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1962	#num#	k4
totiž	totiž	k9
došlo	dojít	k5eAaPmAgNnS
vedení	vedení	k1gNnSc1
k	k	k7c3
názoru	názor	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
snížit	snížit	k5eAaPmF
závislost	závislost	k1gFnSc4
katedry	katedra	k1gFnSc2
na	na	k7c6
Karlově	Karlův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
(	(	kIx(
<g/>
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
například	například	k6eAd1
katedra	katedra	k1gFnSc1
jazyků	jazyk	k1gInPc2
<g/>
;	;	kIx,
matematiku	matematika	k1gFnSc4
a	a	k8xC
fyziku	fyzika	k1gFnSc4
přebrala	přebrat	k5eAaPmAgFnS
fakulta	fakulta	k1gFnSc1
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
postupnému	postupný	k2eAgNnSc3d1
nahrazování	nahrazování	k1gNnSc3
předmětů	předmět	k1gInPc2
z	z	k7c2
UK	UK	kA
přednáškami	přednáška	k1gFnPc7
vlastními	vlastní	k2eAgFnPc7d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
probíhá	probíhat	k5eAaImIp3nS
na	na	k7c4
PřF	PřF	k1gFnSc4
UK	UK	kA
výuka	výuka	k1gFnSc1
některých	některý	k3yIgInPc2
chemických	chemický	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
hlavně	hlavně	k9
v	v	k7c6
nižších	nízký	k2eAgInPc6d2
ročnících	ročník	k1gInPc6
bakalářského	bakalářský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1
katedry	katedra	k1gFnSc2
</s>
<s>
profesor	profesor	k1gMnSc1
František	František	k1gMnSc1
Běhounek	běhounek	k1gMnSc1
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
vedoucí	vedoucí	k1gMnSc1
katedry	katedra	k1gFnSc2
</s>
<s>
Během	během	k7c2
existence	existence	k1gFnSc2
katedry	katedra	k1gFnSc2
se	se	k3xPyFc4
na	na	k7c6
pozici	pozice	k1gFnSc6
vedoucího	vedoucí	k1gMnSc2
vystřídalo	vystřídat	k5eAaPmAgNnS
celkem	celkem	k6eAd1
6	#num#	k4
chemiků	chemik	k1gMnPc2
<g/>
:	:	kIx,
</s>
<s>
prof.	prof.	kA
RNDr.	RNDr.	kA
akad	akad	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Běhounek	běhounek	k1gMnSc1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1957	#num#	k4
–	–	k?
1958	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
prof.	prof.	kA
Vladimír	Vladimír	k1gMnSc1
Majer	Majer	k1gMnSc1
(	(	kIx(
<g/>
1958	#num#	k4
–	–	k?
1972	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
P	P	kA
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
prof.	prof.	kA
RNDr.	RNDr.	kA
Josef	Josef	k1gMnSc1
Cabicar	Cabicar	k1gMnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
(	(	kIx(
<g/>
1972	#num#	k4
–	–	k?
1986	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
prof.	prof.	kA
Petr	Petr	k1gMnSc1
Beneš	Beneš	k1gMnSc1
(	(	kIx(
<g/>
1986	#num#	k4
–	–	k?
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
prof.	prof.	kA
Ing.	ing.	kA
Viliam	Viliam	k1gMnSc1
Múčka	Múčka	k1gMnSc1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2003	#num#	k4
–	–	k?
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
prof.	prof.	kA
Ing.	ing.	kA
Jan	Jan	k1gMnSc1
John	John	k1gMnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
(	(	kIx(
<g/>
2010	#num#	k4
–	–	k?
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
významné	významný	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
katedry	katedra	k1gFnSc2
</s>
<s>
promovaný	promovaný	k2eAgMnSc1d1
chemik	chemik	k1gMnSc1
Václav	Václav	k1gMnSc1
Černík	Černík	k1gMnSc1
–	–	k?
organický	organický	k2eAgMnSc1d1
chemik	chemik	k1gMnSc1
</s>
<s>
promovaný	promovaný	k2eAgMnSc1d1
chemik	chemik	k1gMnSc1
doc.	doc.	kA
RNDr.	RNDr.	kA
Alexander	Alexandra	k1gFnPc2
Gosman	Gosman	k1gMnSc1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
–	–	k?
zakladatel	zakladatel	k1gMnSc1
radiochemického	radiochemický	k2eAgMnSc2d1
praktika	praktik	k1gMnSc2
</s>
<s>
promovaný	promovaný	k2eAgMnSc1d1
chemik	chemik	k1gMnSc1
RNDr.	RNDr.	kA
Jarmila	Jarmila	k1gFnSc1
Prášilová	Prášilová	k1gFnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
–	–	k?
fyzikální	fyzikální	k2eAgFnSc1d1
chemička	chemička	k1gFnSc1
</s>
<s>
promovaný	promovaný	k2eAgMnSc1d1
chemik	chemik	k1gMnSc1
Jaromír	Jaromír	k1gMnSc1
Růžička	Růžička	k1gMnSc1
–	–	k?
analytický	analytický	k2eAgMnSc1d1
chemik	chemik	k1gMnSc1
</s>
<s>
promovaný	promovaný	k2eAgMnSc1d1
chemik	chemik	k1gMnSc1
prof.	prof.	kA
RNDr.	RNDr.	kA
Jiří	Jiří	k1gMnSc1
Starý	Starý	k1gMnSc1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
–	–	k?
chemik	chemik	k1gMnSc1
se	s	k7c7
specializací	specializace	k1gFnSc7
na	na	k7c4
separační	separační	k2eAgFnPc4d1
techniky	technika	k1gFnPc4
</s>
<s>
doc.	doc.	kA
RNDr.	RNDr.	kA
Adolf	Adolf	k1gMnSc1
Zeman	Zeman	k1gMnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
–	–	k?
analytický	analytický	k2eAgMnSc1d1
chemik	chemik	k1gMnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://www.fjfi.cvut.cz/files/Uredni_deska/Vyrocni_zpravy/190617-VZ_2018.pdf1	https://www.fjfi.cvut.cz/files/Uredni_deska/Vyrocni_zpravy/190617-VZ_2018.pdf1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
BENEŠ	Beneš	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
55	#num#	k4
let	léto	k1gNnPc2
výuky	výuka	k1gFnSc2
a	a	k8xC
výzkumu	výzkum	k1gInSc2
na	na	k7c6
Katedře	katedra	k1gFnSc6
jaderné	jaderný	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
Fakulty	fakulta	k1gFnSc2
jaderné	jaderný	k2eAgFnSc2d1
a	a	k8xC
fyzikálně	fyzikálně	k6eAd1
inženýrské	inženýrský	k2eAgFnPc4d1
ČVUT	ČVUT	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
první	první	k4xOgFnSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
81	#num#	k4
s.	s.	k?
↑	↑	k?
https://inis.iaea.org/collection/NCLCollectionStore/_Public/08/300/8300050.pdf	https://inis.iaea.org/collection/NCLCollectionStore/_Public/08/300/8300050.pdf	k1gInSc4
<g/>
↑	↑	k?
Studijní	studijní	k2eAgInPc4d1
programy	program	k1gInPc4
a	a	k8xC
předpisy	předpis	k1gInPc4
-	-	kIx~
FJFI	FJFI	kA
ČVUT	ČVUT	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
www.fjfi.cvut.cz	www.fjfi.cvut.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
na	na	k7c4
doporučení	doporučení	k1gNnSc4
Komise	komise	k1gFnSc2
pro	pro	k7c4
</s>
<s>
jadernou	jaderný	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
zřízená	zřízený	k2eAgFnSc1d1
Ministerstvem	ministerstvo	k1gNnSc7
školství	školství	k1gNnSc4
ze	z	k7c2
dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1958	#num#	k4
<g/>
↑	↑	k?
v	v	k7c6
letech	léto	k1gNnPc6
1962	#num#	k4
a	a	k8xC
1963	#num#	k4
získali	získat	k5eAaPmAgMnP
titul	titul	k1gInSc4
inženýr	inženýr	k1gMnSc1
technické	technický	k2eAgFnSc2d1
fysiky	fysika	k1gFnSc2
se	s	k7c7
zkratkou	zkratka	k1gFnSc7
Inž	Inž	k1gFnSc7
<g/>
.	.	kIx.
<g/>
↑	↑	k?
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1958	#num#	k4
a	a	k8xC
1959	#num#	k4
byl	být	k5eAaImAgInS
pověřen	pověřit	k5eAaPmNgMnS
řízením	řízení	k1gNnSc7
katedry	katedra	k1gFnSc2
<g/>
;	;	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1959	#num#	k4
byl	být	k5eAaImAgMnS
pak	pak	k6eAd1
plnohodnotným	plnohodnotný	k2eAgMnSc7d1
vedoucím	vedoucí	k1gMnSc7
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Katedry	katedra	k1gFnPc1
Fakulty	fakulta	k1gFnSc2
jaderné	jaderný	k2eAgFnPc1d1
a	a	k8xC
fyzikálně	fyzikálně	k6eAd1
inženýrské	inženýrský	k2eAgFnPc4d1
ČVUT	ČVUT	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
Katedry	katedra	k1gFnSc2
</s>
<s>
Katedra	katedra	k1gFnSc1
matematiky	matematika	k1gFnSc2
•	•	k?
Katedra	katedra	k1gFnSc1
fyziky	fyzika	k1gFnSc2
•	•	k?
Katedra	katedra	k1gFnSc1
humanitních	humanitní	k2eAgFnPc2d1
věd	věda	k1gFnPc2
a	a	k8xC
jazyků	jazyk	k1gInPc2
•	•	k?
Katedra	katedra	k1gFnSc1
inženýrství	inženýrství	k1gNnSc2
pevných	pevný	k2eAgFnPc2d1
látek	látka	k1gFnPc2
•	•	k?
Katedra	katedra	k1gFnSc1
fyzikální	fyzikální	k2eAgFnSc2d1
elektroniky	elektronika	k1gFnSc2
•	•	k?
Katedra	katedra	k1gFnSc1
materiálů	materiál	k1gInPc2
•	•	k?
Katedra	katedra	k1gFnSc1
jaderné	jaderný	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
•	•	k?
Katedra	katedra	k1gFnSc1
dozimetrie	dozimetrie	k1gFnSc1
a	a	k8xC
aplikace	aplikace	k1gFnSc1
ionizujícího	ionizující	k2eAgNnSc2d1
záření	záření	k1gNnSc2
•	•	k?
Katedra	katedra	k1gFnSc1
jaderných	jaderný	k2eAgInPc2d1
reaktorů	reaktor	k1gInPc2
•	•	k?
Katedra	katedra	k1gFnSc1
softwarového	softwarový	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
Zrušené	zrušený	k2eAgFnSc2d1
katedry	katedra	k1gFnSc2
</s>
<s>
Katedra	katedra	k1gFnSc1
marxismu	marxismus	k1gInSc2
(	(	kIx(
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
1969	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Katedra	katedra	k1gFnSc1
užité	užitý	k2eAgFnSc2d1
jaderné	jaderný	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
(	(	kIx(
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
1981	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Kabinet	kabinet	k1gInSc4
vojenské	vojenský	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
Převedené	převedený	k2eAgFnSc2d1
katedry	katedra	k1gFnSc2
</s>
<s>
Katedra	katedra	k1gFnSc1
jaderné	jaderný	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
(	(	kIx(
<g/>
1967	#num#	k4
na	na	k7c6
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Katedra	katedra	k1gFnSc1
teoretické	teoretický	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
(	(	kIx(
<g/>
1967	#num#	k4
na	na	k7c6
UK	UK	kA
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
10251	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
137702949	#num#	k4
</s>
