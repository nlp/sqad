<s>
Katedra	katedra	k1gFnSc1
jaderné	jaderný	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
KJCH	KJCH	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednou	jednou	k4c7
z	z	k7c2
10	#num#	k4c2
kateder	katedra	k1gFnPc2
Fakulty	fakulta	k1gFnSc2
jaderné	jaderný	k2eAgFnSc2d1
a	a	k8xC
fyzikálně	fyzikálně	k6eAd1
inženýrské	inženýrský	k2eAgFnSc2d1
Českého	český	k2eAgNnSc2d1
vysokého	vysoký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
technického	technický	k2eAgNnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
byla	být	k5eAaImAgFnS
druhou	druhý	k4xOgFnSc7
největší	veliký	k2eAgFnSc7d3
katedrou	katedra	k1gFnSc7
fakulty	fakulta	k1gFnSc2
<g/>
.	.	kIx.
</s>