<s>
Bipolární	bipolární	k2eAgInSc1d1
tranzistor	tranzistor	k1gInSc1
s	s	k7c7
izolovaným	izolovaný	k2eAgNnSc7d1
hradlem	hradlo	k1gNnSc7
(	(	kIx(
<g/>
Anglicky	anglicky	k2eAgInSc1d1
Insulated	insulated	k2eAgInSc1d1
Gate	gate	k1gInSc1
Bipolar	bipolar	k2eAgInSc1d1
Transistor	transistor	k1gInSc1
IGBT	IGBT	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
tranzistorů	tranzistor	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1
je	být	k5eAaImIp3nS
zkonstruován	zkonstruovat	k5eAaPmNgInS
pro	pro	k7c4
velký	velký	k2eAgInSc4d1
rozsah	rozsah	k1gInSc4
spínaných	spínaný	k2eAgInPc2d1
výkonů	výkon	k1gInPc2
(	(	kIx(
<g/>
od	od	k7c2
zlomků	zlomek	k1gInPc2
W	W	kA
až	až	k9
po	po	k7c4
desítky	desítka	k1gFnPc4
MW	MW	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>