<s>
IGBT	IGBT	kA
</s>
<s>
IGBT	IGBT	kA
3300V	3300V	k4
1200A	1200A	k4
Mitsubishi	mitsubishi	k1gNnPc2
</s>
<s>
Bipolární	bipolární	k2eAgInSc1d1
tranzistor	tranzistor	k1gInSc1
s	s	k7c7
izolovaným	izolovaný	k2eAgNnSc7d1
hradlem	hradlo	k1gNnSc7
(	(	kIx(
<g/>
Anglicky	anglicky	k2eAgInSc1d1
Insulated	insulated	k2eAgInSc1d1
Gate	gate	k1gInSc1
Bipolar	bipolar	k2eAgInSc1d1
Transistor	transistor	k1gInSc1
IGBT	IGBT	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
tranzistorů	tranzistor	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1
je	být	k5eAaImIp3nS
zkonstruován	zkonstruovat	k5eAaPmNgInS
pro	pro	k7c4
velký	velký	k2eAgInSc4d1
rozsah	rozsah	k1gInSc4
spínaných	spínaný	k2eAgInPc2d1
výkonů	výkon	k1gInPc2
(	(	kIx(
<g/>
od	od	k7c2
zlomků	zlomek	k1gInPc2
W	W	kA
až	až	k9
po	po	k7c4
desítky	desítka	k1gFnPc4
MW	MW	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
a	a	k8xC
vysokou	vysoký	k2eAgFnSc4d1
pulzní	pulzní	k2eAgFnSc4d1
frekvenci	frekvence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
IGBT	IGBT	kA
je	být	k5eAaImIp3nS
integrovaná	integrovaný	k2eAgFnSc1d1
kombinace	kombinace	k1gFnSc1
unipolární	unipolární	k2eAgFnSc2d1
a	a	k8xC
bipolární	bipolární	k2eAgFnSc2d1
součástky	součástka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čip	čip	k1gInSc1
tranzistoru	tranzistor	k1gInSc2
má	mít	k5eAaImIp3nS
hradlo	hradlo	k1gNnSc1
izolované	izolovaný	k2eAgNnSc1d1
tenkou	tenký	k2eAgFnSc7d1
oxidovou	oxidový	k2eAgFnSc7d1
vrstvou	vrstva	k1gFnSc7
stejně	stejně	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
výkonový	výkonový	k2eAgInSc1d1
MOSFET	MOSFET	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
kolektorové	kolektorový	k2eAgFnSc6d1
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
vytvořen	vytvořen	k2eAgInSc1d1
PN	PN	kA
přechod	přechod	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
injektuje	injektovat	k5eAaBmIp3nS
minoritní	minoritní	k2eAgInPc4d1
nosiče	nosič	k1gInPc4
do	do	k7c2
kanálu	kanál	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
je	být	k5eAaImIp3nS
IGBT	IGBT	kA
sepnut	sepnout	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
výrazně	výrazně	k6eAd1
snižuje	snižovat	k5eAaImIp3nS
úbytek	úbytek	k1gInSc1
napětí	napětí	k1gNnSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
ztrátový	ztrátový	k2eAgInSc4d1
výkon	výkon	k1gInSc4
v	v	k7c6
sepnutém	sepnutý	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
</s>
<s>
Do	do	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
bylo	být	k5eAaImAgNnS
poměrně	poměrně	k6eAd1
těžké	těžký	k2eAgNnSc1d1
spínat	spínat	k5eAaImF
krátké	krátký	k2eAgInPc4d1
vysokovýkonové	vysokovýkonový	k2eAgInPc4d1
impulsy	impuls	k1gInPc4
řádově	řádově	k6eAd1
v	v	k7c6
desítkách	desítka	k1gFnPc6
MW	MW	kA
<g/>
,	,	kIx,
protože	protože	k8xS
např.	např.	kA
bipolární	bipolární	k2eAgInPc4d1
tranzistory	tranzistor	k1gInPc4
a	a	k8xC
hlavně	hlavně	k9
tyristory	tyristor	k1gInPc1
byly	být	k5eAaImAgInP
sice	sice	k8xC
výkonné	výkonný	k2eAgInPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gNnSc1
spínání	spínání	k1gNnSc1
a	a	k8xC
vypínání	vypínání	k1gNnSc1
bylo	být	k5eAaImAgNnS
pomalé	pomalý	k2eAgNnSc1d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
NMOS	NMOS	kA
měly	mít	k5eAaImAgFnP
omezené	omezený	k2eAgNnSc4d1
závěrné	závěrný	k2eAgNnSc4d1
napětí	napětí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Proto	proto	k8xC
byly	být	k5eAaImAgInP
vytvořeny	vytvořit	k5eAaPmNgInP
IGBT	IGBT	kA
tranzistory	tranzistor	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
ze	z	k7c2
začátku	začátek	k1gInSc2
nespolehlivé	spolehlivý	k2eNgInPc4d1
a	a	k8xC
velmi	velmi	k6eAd1
drahé	drahý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupem	postupem	k7c2
času	čas	k1gInSc2
se	se	k3xPyFc4
jejich	jejich	k3xOp3gFnSc1
spolehlivost	spolehlivost	k1gFnSc1
podstatně	podstatně	k6eAd1
zlepšila	zlepšit	k5eAaPmAgFnS
a	a	k8xC
cena	cena	k1gFnSc1
klesla	klesnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Různé	různý	k2eAgInPc1d1
měniče	měnič	k1gInPc1
statických	statický	k2eAgInPc2d1
i	i	k8xC
mobilních	mobilní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
napájení	napájení	k1gNnSc2
elektrických	elektrický	k2eAgInPc2d1
pohonů	pohon	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
střídače	střídač	k1gMnSc4
DC	DC	kA
<g/>
/	/	kIx~
<g/>
AC	AC	kA
<g/>
,	,	kIx,
</s>
<s>
usměrňovače	usměrňovač	k1gInPc1
AC	AC	kA
<g/>
/	/	kIx~
<g/>
DC	DC	kA
<g/>
,	,	kIx,
</s>
<s>
pulsní	pulsní	k2eAgInPc1d1
měniče	měnič	k1gInPc1
<g/>
,	,	kIx,
</s>
<s>
např.	např.	kA
<g/>
:	:	kIx,
</s>
<s>
do	do	k7c2
elektrických	elektrický	k2eAgFnPc2d1
lokomotiv	lokomotiva	k1gFnPc2
a	a	k8xC
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
</s>
<s>
do	do	k7c2
trolejbusů	trolejbus	k1gInPc2
a	a	k8xC
tramvají	tramvaj	k1gFnPc2
</s>
<s>
Výhody	výhoda	k1gFnPc1
</s>
<s>
nízké	nízký	k2eAgFnPc1d1
ztráty	ztráta	k1gFnPc1
v	v	k7c6
zapnutém	zapnutý	k2eAgInSc6d1
stavu	stav	k1gInSc6
</s>
<s>
nízký	nízký	k2eAgInSc1d1
budící	budící	k2eAgInSc1d1
výkon	výkon	k1gInSc1
</s>
<s>
větší	veliký	k2eAgInSc1d2
rozsah	rozsah	k1gInSc1
pracovních	pracovní	k2eAgNnPc2d1
napětí	napětí	k1gNnPc2
a	a	k8xC
proudu	proud	k1gInSc2
než	než	k8xS
tranzistory	tranzistor	k1gInPc1
MOSFET	MOSFET	kA
</s>
<s>
Nevýhody	nevýhoda	k1gFnPc1
</s>
<s>
nižší	nízký	k2eAgFnSc1d2
spínací	spínací	k2eAgFnSc1d1
frekvence	frekvence	k1gFnSc1
než	než	k8xS
tranzistory	tranzistor	k1gInPc1
MOSFET	MOSFET	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4273802-7	4273802-7	k4
</s>
