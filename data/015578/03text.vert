<s>
Takamacu	Takamacu	k6eAd1
</s>
<s>
Takamacu	Takamacu	k6eAd1
高	高	k?
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
34	#num#	k4
<g/>
°	°	k?
<g/>
21	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
134	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Stát	stát	k1gInSc1
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
Japonsko	Japonsko	k1gNnSc1
Region	region	k1gInSc1
</s>
<s>
Šikoku	Šikok	k1gInSc3
</s>
<s>
Gifu	Gifu	k6eAd1
</s>
<s>
Gifu	Gifa	k1gFnSc4
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
375,11	375,11	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
419	#num#	k4
429	#num#	k4
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
1	#num#	k4
118,1	118,1	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Hideto	Hidet	k2eAgNnSc1d1
Ō	Ō	k1gNnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1890	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.city.takamatsu.kagawa.jp	www.city.takamatsu.kagawa.jp	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přístav	přístav	k1gInSc1
Takamacu	Takamacus	k1gInSc2
</s>
<s>
Takamacu	Takamacu	k5eAaPmIp1nS
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
:	:	kIx,
高	高	k?
<g/>
;	;	kIx,
Takamacu-ši	Takamacu-š	k1gMnPc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
prefektury	prefektura	k1gFnSc2
Kagawa	Kagaw	k1gInSc2
na	na	k7c6
severu	sever	k1gInSc6
ostrova	ostrov	k1gInSc2
Šikoku	Šikok	k1gInSc2
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takamacu	Takamacus	k1gInSc2
leží	ležet	k5eAaImIp3nS
na	na	k7c6
břehu	břeh	k1gInSc6
Vnitřního	vnitřní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
únoru	únor	k1gInSc3
2006	#num#	k4
mělo	mít	k5eAaImAgNnS
podle	podle	k7c2
odhadu	odhad	k1gInSc2
město	město	k1gNnSc4
418	#num#	k4
242	#num#	k4
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
hustotu	hustota	k1gFnSc4
osídlení	osídlení	k1gNnPc2
1115	#num#	k4
ob.	ob.	k?
<g/>
/	/	kIx~
<g/>
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celková	celkový	k2eAgFnSc1d1
rozloha	rozloha	k1gFnSc1
města	město	k1gNnSc2
je	být	k5eAaImIp3nS
375,09	375,09	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s>
Město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
oficiálně	oficiálně	k6eAd1
založeno	založit	k5eAaPmNgNnS
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1890	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
politickým	politický	k2eAgNnSc7d1
a	a	k8xC
ekonomickým	ekonomický	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
oblasti	oblast	k1gFnSc2
už	už	k9
od	od	k7c2
období	období	k1gNnSc2
Edo	Eda	k1gMnSc5
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
rod	rod	k1gInSc1
Macudaira	Macudairo	k1gNnSc2
udělal	udělat	k5eAaPmAgInS
z	z	k7c2
Takamacu	Takamacus	k1gInSc2
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
svého	svůj	k3xOyFgNnSc2
panství	panství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1
turistickou	turistický	k2eAgFnSc7d1
atrakcí	atrakce	k1gFnSc7
města	město	k1gNnSc2
je	být	k5eAaImIp3nS
překrásná	překrásný	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
Ricurin-kóen	Ricurin-kóna	k1gFnPc2
založená	založený	k2eAgFnSc1d1
v	v	k7c6
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Přibližně	přibližně	k6eAd1
patnáct	patnáct	k4xCc4
kilometrů	kilometr	k1gInPc2
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
centra	centrum	k1gNnSc2
leží	ležet	k5eAaImIp3nS
letiště	letiště	k1gNnSc4
Takamacu	Takamacus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Saint	Saint	k1gMnSc1
Petersburg	Petersburg	k1gMnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
Tours	Tours	k1gInSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
</s>
<s>
Nan-čchang	Nan-čchang	k1gInSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Takamatsu	Takamats	k1gInSc2
<g/>
,	,	kIx,
Kagawa	Kagawa	k1gFnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Takamacu	Takamacus	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
města	město	k1gNnSc2
Takamacu	Takamacus	k1gInSc2
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Japonsko	Japonsko	k1gNnSc1
–	–	k?
日	日	k?
(	(	kIx(
<g/>
Nihon	Nihon	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
(	(	kIx(
<g/>
J	J	kA
<g/>
)	)	kIx)
Prefektury	prefektura	k1gFnSc2
(	(	kIx(
<g/>
都	都	k?
<g/>
)	)	kIx)
a	a	k8xC
jejich	jejich	k3xOp3gNnPc4
hlavní	hlavní	k2eAgNnPc4d1
města	město	k1gNnPc4
</s>
<s>
Prefektura	prefektura	k1gFnSc1
Aiči	Aič	k1gFnSc2
(	(	kIx(
<g/>
Nagoja	Nagoja	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Akita	Akita	k1gFnSc1
(	(	kIx(
<g/>
Akita	Akita	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Aomori	Aomor	k1gFnSc2
(	(	kIx(
<g/>
Aomori	Aomor	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Čiba	Čiba	k1gFnSc1
(	(	kIx(
<g/>
Čiba	Čiba	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Ehime	Ehim	k1gInSc5
(	(	kIx(
<g/>
Macujama	Macujamum	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Fukui	Fuku	k1gFnSc2
(	(	kIx(
<g/>
Fukui	Fuku	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Fukuoka	Fukuoka	k1gFnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Fukuoka	Fukuoka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Fukušima	Fukušima	k1gFnSc1
(	(	kIx(
<g/>
Fukušima	Fukušima	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Gifu	Gifus	k1gInSc2
(	(	kIx(
<g/>
Gifu	Gifus	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Gunma	Gunma	k1gFnSc1
(	(	kIx(
<g/>
Maebaši	Maebaše	k1gFnSc4
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Hirošima	Hirošima	k1gFnSc1
(	(	kIx(
<g/>
Hirošima	Hirošima	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Hokkaidó	Hokkaidó	k1gFnSc1
(	(	kIx(
<g/>
Sapporo	Sappora	k1gFnSc5
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Hjógo	Hjógo	k1gMnSc1
(	(	kIx(
<g/>
Kóbe	Kóbe	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Ibaraki	Ibarak	k1gFnSc2
(	(	kIx(
<g/>
Mito	Mito	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Išikawa	Išikawa	k1gFnSc1
(	(	kIx(
<g/>
Kanazawa	Kanazawa	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Iwate	Iwat	k1gInSc5
(	(	kIx(
<g/>
Morioka	Morioko	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Jamagata	Jamagata	k1gFnSc1
(	(	kIx(
<g/>
Jamagata	Jamagata	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Jamaguči	Jamaguč	k1gInPc7
(	(	kIx(
<g/>
Jamaguči	Jamaguč	k1gInPc7
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Jamanaši	Jamanaše	k1gFnSc4
(	(	kIx(
<g/>
Kófu	Kófa	k1gFnSc4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kagawa	Kagawa	k1gFnSc1
(	(	kIx(
<g/>
Takamacu	Takamacus	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kagošima	Kagošima	k1gFnSc1
(	(	kIx(
<g/>
Kagošima	Kagošima	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kanagawa	Kanagawa	k1gFnSc1
(	(	kIx(
<g/>
Jokohama	Jokohama	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kjóto	Kjóto	k1gNnSc4
(	(	kIx(
<g/>
Kjóto	Kjóto	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kóči	Kóč	k1gFnSc2
(	(	kIx(
<g/>
Kóči	Kóč	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kumamoto	Kumamota	k1gFnSc5
(	(	kIx(
<g/>
Kumamoto	Kumamota	k1gFnSc5
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Mie	Mie	k1gFnSc1
(	(	kIx(
<g/>
Cu	Cu	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Mijagi	Mijag	k1gFnSc2
(	(	kIx(
<g/>
Sendai	Senda	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Mijazaki	Mijazak	k1gFnSc2
(	(	kIx(
<g/>
Mijazaki	Mijazak	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Nagano	Nagano	k1gNnSc4
(	(	kIx(
<g/>
Nagano	Nagano	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Nagasaki	Nagasaki	k1gNnSc2
(	(	kIx(
<g/>
Nagasaki	Nagasaki	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Nara	Nara	k1gFnSc1
(	(	kIx(
<g/>
Nara	Nara	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Niigata	Niigata	k1gFnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Niigata	Niigata	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Óita	Óita	k1gFnSc1
(	(	kIx(
<g/>
Óita	Óita	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Okajama	Okajama	k1gFnSc1
(	(	kIx(
<g/>
Okajama	Okajama	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Okinawa	Okinawa	k1gFnSc1
(	(	kIx(
<g/>
Naha	naho	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Ósaka	Ósaka	k1gFnSc1
(	(	kIx(
<g/>
Ósaka	Ósaka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Saga	Saga	k1gFnSc1
(	(	kIx(
<g/>
Saga	Saga	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Saitama	Saitama	k1gFnSc1
(	(	kIx(
<g/>
Saitama	Saitama	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Šiga	Šiga	k1gFnSc1
(	(	kIx(
<g/>
Ócu	Ócu	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Šimane	Šiman	k1gMnSc5
(	(	kIx(
<g/>
Macue	Macue	k1gNnSc7
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Šizuoka	Šizuoka	k1gFnSc1
(	(	kIx(
<g/>
Šizuoka	Šizuoka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Točigi	Točig	k1gFnSc2
(	(	kIx(
<g/>
Ucunomija	Ucunomija	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Tokušima	Tokušima	k1gFnSc1
(	(	kIx(
<g/>
Tokušima	Tokušima	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Tokio	Tokio	k1gNnSc4
(	(	kIx(
<g/>
Šindžuku	Šindžuk	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Tottori	Tottor	k1gFnSc2
(	(	kIx(
<g/>
Tottori	Tottor	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Tojama	Tojama	k1gFnSc1
(	(	kIx(
<g/>
Tojama	Tojama	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Wakajama	Wakajama	k1gFnSc1
(	(	kIx(
<g/>
Wakajama	Wakajama	k1gFnSc1
<g/>
)	)	kIx)
Japonské	japonský	k2eAgInPc1d1
regiony	region	k1gInPc1
(	(	kIx(
<g/>
地	地	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Hokkaidó	Hokkaidó	k1gFnSc1
•	•	k?
Tóhoku	Tóhok	k1gInSc2
•	•	k?
Kantó	Kantó	k1gMnSc1
•	•	k?
Čúbu	Čúba	k1gFnSc4
•	•	k?
Kansai	Kansa	k1gFnSc3
•	•	k?
Čúgoku	Čúgok	k1gInSc6
•	•	k?
Šikoku	Šikok	k1gInSc2
•	•	k?
Kjúšú	Kjúšú	k1gMnSc1
</s>
<s>
Prefektura	prefektura	k1gFnSc1
Kagawa	Kagaw	k1gInSc2
–	–	k?
香	香	k?
(	(	kIx(
<g/>
Kagawa-ken	Kagawa-ken	k1gInSc1
<g/>
)	)	kIx)
Města	město	k1gNnSc2
(	(	kIx(
<g/>
市	市	k?
<g/>
,	,	kIx,
ši	ši	k?
<g/>
)	)	kIx)
</s>
<s>
Higašikagawa	Higašikagawa	k1gFnSc1
(	(	kIx(
<g/>
東	東	k?
<g/>
)	)	kIx)
•	•	k?
Kan	Kan	k1gFnSc1
<g/>
'	'	kIx"
<g/>
ondži	ondzat	k5eAaPmIp1nS
(	(	kIx(
<g/>
観	観	k?
<g/>
)	)	kIx)
•	•	k?
Marugame	Marugam	k1gInSc5
(	(	kIx(
<g/>
丸	丸	k?
<g/>
)	)	kIx)
•	•	k?
Mitojo	Mitojo	k1gMnSc1
(	(	kIx(
<g/>
三	三	k?
<g/>
)	)	kIx)
•	•	k?
Sakaide	Sakaid	k1gInSc5
(	(	kIx(
<g/>
坂	坂	k?
<g/>
)	)	kIx)
•	•	k?
Sanuki	Sanuk	k1gFnSc2
(	(	kIx(
<g/>
さ	さ	k?
<g/>
)	)	kIx)
•	•	k?
Takamacu	Takamacus	k1gInSc2
(	(	kIx(
<g/>
高	高	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
prefektury	prefektura	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Zencúdži	Zencúdž	k1gFnSc6
(	(	kIx(
<g/>
善	善	k?
<g/>
)	)	kIx)
Okresy	okres	k1gInPc1
(	(	kIx(
<g/>
郡	郡	k?
<g/>
,	,	kIx,
gun	gun	k?
<g/>
)	)	kIx)
</s>
<s>
městečka	městečko	k1gNnPc4
(	(	kIx(
<g/>
町	町	k?
<g/>
,	,	kIx,
čó	čó	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Šózu	Šóz	k2eAgFnSc4d1
(	(	kIx(
<g/>
小	小	k?
<g/>
)	)	kIx)
</s>
<s>
Tonošó-čó	Tonošó-čó	k?
(	(	kIx(
<g/>
土	土	k?
<g/>
)	)	kIx)
•	•	k?
Šódošima-čó	Šódošima-čó	k1gFnSc1
(	(	kIx(
<g/>
小	小	k?
<g/>
)	)	kIx)
Kita	Kita	k1gMnSc1
(	(	kIx(
<g/>
木	木	k?
<g/>
)	)	kIx)
</s>
<s>
Miki-čó	Miki-čó	k?
(	(	kIx(
<g/>
三	三	k?
<g/>
)	)	kIx)
Kagawa	Kagawa	k1gMnSc1
(	(	kIx(
<g/>
香	香	k?
<g/>
)	)	kIx)
</s>
<s>
Naošima-čó	Naošima-čó	k?
(	(	kIx(
<g/>
直	直	k?
<g/>
)	)	kIx)
Ajauta	Ajaut	k1gMnSc2
(	(	kIx(
<g/>
綾	綾	k?
<g/>
)	)	kIx)
</s>
<s>
Ajagawa-čó	Ajagawa-čó	k?
(	(	kIx(
<g/>
綾	綾	k?
<g/>
)	)	kIx)
•	•	k?
Utazu-čó	Utazu-čó	k1gFnSc1
(	(	kIx(
<g/>
宇	宇	k?
<g/>
)	)	kIx)
Nakatado	Nakatada	k1gFnSc5
(	(	kIx(
<g/>
仲	仲	k?
<g/>
)	)	kIx)
</s>
<s>
Kotohira-čó	Kotohira-čó	k?
(	(	kIx(
<g/>
琴	琴	k?
<g/>
)	)	kIx)
•	•	k?
Tadocu-čó	Tadocu-čó	k1gFnSc1
(	(	kIx(
<g/>
多	多	k?
<g/>
)	)	kIx)
•	•	k?
Minnó-čó	Minnó-čó	k1gFnSc1
(	(	kIx(
<g/>
ま	ま	k?
<g/>
)	)	kIx)
<g/>
*	*	kIx~
Jiné	jiný	k2eAgFnPc1d1
</s>
<s>
souostroví	souostroví	k1gNnSc1
<g/>
/	/	kIx~
<g/>
ostrovy	ostrov	k1gInPc1
<g/>
:	:	kIx,
Šiwaku-šótó	Šiwaku-šótó	k1gFnSc1
(	(	kIx(
<g/>
塩	塩	k?
<g/>
)	)	kIx)
<g/>
*	*	kIx~
•	•	k?
(	(	kIx(
<g/>
Awašima	Awašima	k1gFnSc1
(	(	kIx(
<g/>
粟	粟	k?
<g/>
)	)	kIx)
<g/>
*	*	kIx~
•	•	k?
Hondžima	Hondžima	k1gFnSc1
(	(	kIx(
<g/>
本	本	k?
<g/>
)	)	kIx)
<g/>
*	*	kIx~
•	•	k?
Sanagišima	Sanagišima	k1gFnSc1
(	(	kIx(
<g/>
佐	佐	k?
<g/>
)	)	kIx)
<g/>
*	*	kIx~
•	•	k?
Šišidžima	Šišidžima	k1gFnSc1
(	(	kIx(
<g/>
志	志	k?
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
*	*	kIx~
•	•	k?
Hirošima	Hirošima	k1gFnSc1
(	(	kIx(
<g/>
広	広	k?
<g/>
)	)	kIx)
<g/>
*	*	kIx~
•	•	k?
Jošima	Jošima	k1gFnSc1
(	(	kIx(
<g/>
与	与	k?
<g/>
)	)	kIx)
<g/>
*	*	kIx~
•	•	k?
Wasašima	Wasašima	k1gFnSc1
(	(	kIx(
<g/>
羽	羽	k?
<g/>
)	)	kIx)
<g/>
*	*	kIx~
a	a	k8xC
j.	j.	k?
<g/>
)	)	kIx)
•	•	k?
Naošima-šótó	Naošima-šótó	k1gFnSc1
(	(	kIx(
<g/>
直	直	k?
<g/>
)	)	kIx)
<g/>
†	†	k?
•	•	k?
(	(	kIx(
<g/>
Išima	Išima	k1gFnSc1
(	(	kIx(
<g/>
井	井	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
†	†	k?
•	•	k?
Naošima	Naošima	k1gFnSc1
(	(	kIx(
<g/>
直	直	k?
<g/>
)	)	kIx)
<g/>
†	†	k?
•	•	k?
Tešima	Tešima	k1gFnSc1
(	(	kIx(
<g/>
豊	豊	k?
<g/>
)	)	kIx)
<g/>
†	†	k?
•	•	k?
Mukaedžima	Mukaedžima	k1gFnSc1
(	(	kIx(
<g/>
向	向	k?
<g/>
)	)	kIx)
<g/>
†	†	k?
a	a	k8xC
j.	j.	k?
<g/>
)	)	kIx)
•	•	k?
Ibukišima	Ibukišima	k1gFnSc1
(	(	kIx(
<g/>
伊	伊	k?
<g/>
)	)	kIx)
•	•	k?
Jošima	Jošima	k1gFnSc1
(	(	kIx(
<g/>
余	余	k?
<g/>
)	)	kIx)
•	•	k?
Ogidžima	Ogidžima	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
男	男	k?
<g/>
)	)	kIx)
•	•	k?
Megidžima	Megidžima	k1gFnSc1
(	(	kIx(
<g/>
女	女	k?
<g/>
)	)	kIx)
•	•	k?
Šódošima	Šódošima	k1gFnSc1
(	(	kIx(
<g/>
小	小	k?
<g/>
)	)	kIx)
•	•	k?
vodopád	vodopád	k1gInSc1
Kónotaki	Kónotak	k1gFnSc2
(	(	kIx(
<g/>
虹	虹	k?
<g/>
)	)	kIx)
•	•	k?
pohoří	pohoří	k1gNnSc4
<g/>
:	:	kIx,
Sanuki-sanmjaku	Sanuki-sanmjak	k1gInSc2
(	(	kIx(
<g/>
讃	讃	k?
<g/>
)	)	kIx)
•	•	k?
hory	hora	k1gFnPc1
<g/>
:	:	kIx,
Iinojama	Iinojama	k1gFnSc1
(	(	kIx(
<g/>
飯	飯	k?
<g/>
)	)	kIx)
•	•	k?
Unbendžisan	Unbendžisan	k1gInSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
飯	飯	k?
<g/>
)	)	kIx)
•	•	k?
Ótakisan	Ótakisan	k1gInSc1
(	(	kIx(
<g/>
大	大	k?
<g/>
)	)	kIx)
•	•	k?
Rjúó-zan	Rjúó-zan	k1gInSc1
(	(	kIx(
<g/>
竜	竜	k?
<g/>
)	)	kIx)
•	•	k?
onsen	onsen	k1gInSc1
<g/>
:	:	kIx,
Šódošima-onsen	Šódošima-onsen	k1gInSc1
(	(	kIx(
<g/>
小	小	k?
<g/>
)	)	kIx)
•	•	k?
hrady	hrad	k1gInPc4
<g/>
:	:	kIx,
Sogódžó	Sogódžó	k1gFnSc1
(	(	kIx(
<g/>
十	十	k?
<g/>
)	)	kIx)
•	•	k?
Takamacu-džó	Takamacu-džó	k1gFnSc1
(	(	kIx(
<g/>
高	高	k?
<g/>
)	)	kIx)
•	•	k?
Hirune-džó	Hirune-džó	k1gFnSc1
(	(	kIx(
<g/>
昼	昼	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Marugame-džó	Marugame-džó	k1gFnSc1
(	(	kIx(
<g/>
丸	丸	k?
<g/>
)	)	kIx)
•	•	k?
Jašima	Jašima	k1gNnSc1
no	no	k9
ki	ki	k?
(	(	kIx(
<g/>
屋	屋	k?
<g/>
)	)	kIx)
•	•	k?
chrámy	chrám	k1gInPc4
<g/>
:	:	kIx,
Motojama-dži	Motojama-dž	k1gFnSc6
(	(	kIx(
<g/>
本	本	k?
<g/>
)	)	kIx)
•	•	k?
Ókubo-dži	Ókubo-dž	k1gFnSc6
(	(	kIx(
<g/>
大	大	k?
<g/>
)	)	kIx)
•	•	k?
Kaigan-dži	Kaigan-dž	k1gFnSc6
(	(	kIx(
<g/>
海	海	k?
<g/>
)	)	kIx)
•	•	k?
Góšó-dži	Góšó-dž	k1gFnSc6
(	(	kIx(
<g/>
郷	郷	k?
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Konzódži	Konzódž	k1gFnSc6
(	(	kIx(
<g/>
金	金	k?
<g/>
)	)	kIx)
•	•	k?
Šido-dži	Šido-dž	k1gFnSc6
(	(	kIx(
<g/>
志	志	k?
<g/>
)	)	kIx)
•	•	k?
Šuššakadži	Šuššakadž	k1gFnSc6
(	(	kIx(
<g/>
出	出	k?
<g/>
)	)	kIx)
•	•	k?
Širomine-dži	Širomine-dž	k1gFnSc6
(	(	kIx(
<g/>
白	白	k?
<g/>
)	)	kIx)
•	•	k?
Zencú-dži	Zencú-dž	k1gFnSc6
(	(	kIx(
<g/>
善	善	k?
<g/>
)	)	kIx)
•	•	k?
Tennó-dži	Tennó-dž	k1gFnSc6
(	(	kIx(
<g/>
天	天	k?
<g/>
)	)	kIx)
•	•	k?
Mandara-dži	Mandara-dž	k1gFnSc6
(	(	kIx(
<g/>
曼	曼	k?
<g/>
)	)	kIx)
•	•	k?
Jodadži	Jodadž	k1gFnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
與	與	k?
<g/>
)	)	kIx)
•	•	k?
Sanuki	Sanuk	k1gFnSc6
Kokubundži	Kokubundž	k1gFnSc6
(	(	kIx(
<g/>
讃	讃	k?
<g/>
)	)	kIx)
•	•	k?
Susakidži	Susakidž	k1gFnSc6
(	(	kIx(
<g/>
洲	洲	k?
<g/>
)	)	kIx)
•	•	k?
Hokke-dži	Hokke-dž	k1gFnSc6
(	(	kIx(
<g/>
法	法	k?
<g/>
)	)	kIx)
•	•	k?
Jakuridži	Jakuridž	k1gFnSc6
(	(	kIx(
<g/>
八	八	k?
<g/>
)	)	kIx)
•	•	k?
Jašimadži	Jašimadž	k1gFnSc6
(	(	kIx(
<g/>
屋	屋	k?
<g/>
)	)	kIx)
•	•	k?
svatyně	svatyně	k1gFnSc1
<g/>
:	:	kIx,
Kotohiki	Kotohiki	k1gNnSc1
Hačimangú	Hačimangú	k1gFnPc2
(	(	kIx(
<g/>
琴	琴	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Kandanidžindža	Kandanidžindža	k1gFnSc1
(	(	kIx(
<g/>
神	神	k?
<g/>
)	)	kIx)
•	•	k?
Kijama-džindža	Kijama-džindža	k1gFnSc1
(	(	kIx(
<g/>
城	城	k?
<g/>
)	)	kIx)
•	•	k?
Kotohiragú	Kotohiragú	k1gFnSc1
(	(	kIx(
<g/>
金	金	k?
<g/>
)	)	kIx)
•	•	k?
Širaminegú	Širaminegú	k1gFnSc1
(	(	kIx(
<g/>
白	白	k?
<g/>
)	)	kIx)
•	•	k?
Tamura-džindža	Tamura-džindža	k1gFnSc1
(	(	kIx(
<g/>
田	田	k?
<g/>
)	)	kIx)
Sousedící	sousedící	k2eAgFnSc2d1
prefektury	prefektura	k1gFnSc2
(	(	kIx(
<g/>
県	県	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Prefektura	prefektura	k1gFnSc1
Ehime	Ehim	k1gInSc5
(	(	kIx(
<g/>
愛	愛	k?
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Tokušima	Tokušima	k1gFnSc1
(	(	kIx(
<g/>
徳	徳	k?
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80053160	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
138371392	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80053160	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Japonsko	Japonsko	k1gNnSc1
</s>
