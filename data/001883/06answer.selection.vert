<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
buněk	buňka	k1gFnPc2	buňka
je	být	k5eAaImIp3nS	být
cytoplazmatická	cytoplazmatický	k2eAgFnSc1d1	cytoplazmatická
membrána	membrána	k1gFnSc1	membrána
podobná	podobný	k2eAgFnSc1d1	podobná
membráně	membrána	k1gFnSc3	membrána
eukaryot	eukaryot	k1gInSc1	eukaryot
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
většinou	většina	k1gFnSc7	většina
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádné	žádný	k3yNgInPc4	žádný
steroidy	steroid	k1gInPc4	steroid
<g/>
.	.	kIx.	.
</s>
