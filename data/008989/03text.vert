<p>
<s>
Religiozita	religiozita	k1gFnSc1	religiozita
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
státy	stát	k1gInPc7	stát
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
některé	některý	k3yIgFnSc3	některý
z	z	k7c2	z
církví	církev	k1gFnPc2	církev
či	či	k8xC	či
náboženských	náboženský	k2eAgNnPc2d1	náboženské
hnutí	hnutí	k1gNnPc2	hnutí
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
dvěstěmilionové	dvěstěmilionový	k2eAgFnSc6d1	dvěstěmilionový
zemi	zem	k1gFnSc6	zem
hlásí	hlásit	k5eAaImIp3nS	hlásit
asi	asi	k9	asi
92	[number]	k4	92
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zhruba	zhruba	k6eAd1	zhruba
65	[number]	k4	65
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
asi	asi	k9	asi
123	[number]	k4	123
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
Brazílie	Brazílie	k1gFnSc2	Brazílie
činí	činit	k5eAaImIp3nS	činit
zemi	zem	k1gFnSc4	zem
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
katolíků	katolík	k1gMnPc2	katolík
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
22	[number]	k4	22
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
asi	asi	k9	asi
42	[number]	k4	42
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
deklarovalo	deklarovat	k5eAaBmAgNnS	deklarovat
svou	svůj	k3xOyFgFnSc4	svůj
příslušnost	příslušnost	k1gFnSc4	příslušnost
k	k	k7c3	k
protestantismu	protestantismus	k1gInSc3	protestantismus
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
pět	pět	k4xCc4	pět
procent	procento	k1gNnPc2	procento
(	(	kIx(	(
<g/>
9,3	[number]	k4	9,3
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
k	k	k7c3	k
jiným	jiný	k2eAgNnPc3d1	jiné
náboženstvím	náboženství	k1gNnPc3	náboženství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
asi	asi	k9	asi
8	[number]	k4	8
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
15,3	[number]	k4	15,3
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pouhých	pouhý	k2eAgNnPc2d1	pouhé
600	[number]	k4	600
tisíc	tisíc	k4xCgInSc4	tisíc
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
však	však	k9	však
definuje	definovat	k5eAaBmIp3nS	definovat
jako	jako	k9	jako
ateisté	ateista	k1gMnPc1	ateista
<g/>
,	,	kIx,	,
dalších	další	k2eAgInPc2d1	další
124	[number]	k4	124
tisíc	tisíc	k4xCgInPc2	tisíc
jako	jako	k8xS	jako
agnostici	agnostik	k1gMnPc1	agnostik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Zdaleka	zdaleka	k6eAd1	zdaleka
nejvýznamnější	významný	k2eAgNnSc4d3	nejvýznamnější
postavení	postavení	k1gNnSc4	postavení
zde	zde	k6eAd1	zde
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
měla	mít	k5eAaImAgFnS	mít
a	a	k8xC	a
stále	stále	k6eAd1	stále
má	mít	k5eAaImIp3nS	mít
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
koloniálním	koloniální	k2eAgNnSc6d1	koloniální
období	období	k1gNnSc6	období
možnost	možnost	k1gFnSc4	možnost
zvolit	zvolit	k5eAaPmF	zvolit
si	se	k3xPyFc3	se
jiné	jiný	k2eAgNnSc4d1	jiné
náboženské	náboženský	k2eAgNnSc4d1	náboženské
vyznání	vyznání	k1gNnSc4	vyznání
ani	ani	k8xC	ani
neexistovala	existovat	k5eNaImAgFnS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Brazílie	Brazílie	k1gFnSc2	Brazílie
se	se	k3xPyFc4	se
však	však	k9	však
katolická	katolický	k2eAgFnSc1d1	katolická
víra	víra	k1gFnSc1	víra
<g/>
,	,	kIx,	,
šířená	šířený	k2eAgFnSc1d1	šířená
jezuity	jezuita	k1gMnPc4	jezuita
a	a	k8xC	a
vyznávaná	vyznávaný	k2eAgFnSc1d1	vyznávaná
portugalskými	portugalský	k2eAgMnPc7d1	portugalský
osadníky	osadník	k1gMnPc7	osadník
<g/>
,	,	kIx,	,
setkávala	setkávat	k5eAaImAgFnS	setkávat
a	a	k8xC	a
mísila	mísit	k5eAaImAgFnS	mísit
s	s	k7c7	s
náboženskými	náboženský	k2eAgFnPc7d1	náboženská
tradicemi	tradice	k1gFnPc7	tradice
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
i	i	k8xC	i
otroků	otrok	k1gMnPc2	otrok
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
brazilská	brazilský	k2eAgFnSc1d1	brazilská
ústava	ústava	k1gFnSc1	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
sice	sice	k8xC	sice
zavedla	zavést	k5eAaPmAgFnS	zavést
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
náboženstvím	náboženství	k1gNnSc7	náboženství
ale	ale	k8xC	ale
zůstal	zůstat	k5eAaPmAgInS	zůstat
římský	římský	k2eAgInSc1d1	římský
katolicismus	katolicismus	k1gInSc1	katolicismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
odluce	odluka	k1gFnSc3	odluka
státu	stát	k1gInSc2	stát
od	od	k7c2	od
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
si	se	k3xPyFc3	se
nicméně	nicméně	k8xC	nicméně
zachovala	zachovat	k5eAaPmAgFnS	zachovat
politický	politický	k2eAgInSc4d1	politický
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
naprosto	naprosto	k6eAd1	naprosto
dominantní	dominantní	k2eAgNnSc4d1	dominantní
postavení	postavení	k1gNnSc4	postavení
až	až	k9	až
do	do	k7c2	do
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
jejímu	její	k3xOp3gInSc3	její
odporu	odpor	k1gInSc3	odpor
např.	např.	kA	např.
nebyl	být	k5eNaImAgInS	být
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
povolen	povolit	k5eAaPmNgInS	povolit
rozvod	rozvod	k1gInSc1	rozvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výraznému	výrazný	k2eAgInSc3d1	výrazný
vzestupu	vzestup	k1gInSc3	vzestup
pentekostalismu	pentekostalismus	k1gInSc2	pentekostalismus
a	a	k8xC	a
afro-brazilských	afrorazilský	k2eAgInPc2d1	afro-brazilský
náboženských	náboženský	k2eAgInPc2d1	náboženský
směrů	směr	k1gInPc2	směr
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
nehlásí	hlásit	k5eNaImIp3nP	hlásit
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
tendence	tendence	k1gFnPc1	tendence
snížily	snížit	k5eAaPmAgFnP	snížit
podíl	podíl	k1gInSc4	podíl
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
v	v	k7c6	v
brazilské	brazilský	k2eAgFnSc6d1	brazilská
společnosti	společnost	k1gFnSc6	společnost
<g/>
:	:	kIx,	:
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
se	se	k3xPyFc4	se
za	za	k7c4	za
ně	on	k3xPp3gNnSc4	on
považovalo	považovat	k5eAaImAgNnS	považovat
90	[number]	k4	90
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
méně	málo	k6eAd2	málo
než	než	k8xS	než
65	[number]	k4	65
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Církevní	církevní	k2eAgFnSc1d1	církevní
příslušnost	příslušnost	k1gFnSc1	příslušnost
==	==	k?	==
</s>
</p>
<p>
<s>
Spektrum	spektrum	k1gNnSc1	spektrum
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgFnPc3	který
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
Brazilci	Brazilec	k1gMnPc1	Brazilec
hlásí	hlásit	k5eAaImIp3nP	hlásit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
níže	nízce	k6eAd2	nízce
uvedené	uvedený	k2eAgInPc1d1	uvedený
počty	počet	k1gInPc1	počet
jejich	jejich	k3xOp3gInPc2	jejich
členů	člen	k1gInPc2	člen
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
uvedeno	uvést	k5eAaPmNgNnS	uvést
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
===	===	k?	===
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
značný	značný	k2eAgInSc4d1	značný
pokles	pokles	k1gInSc4	pokles
členů	člen	k1gMnPc2	člen
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
se	s	k7c7	s
123	[number]	k4	123
miliony	milion	k4xCgInPc7	milion
věřících	věřící	k2eAgFnPc2d1	věřící
nejmohutnějším	mohutný	k2eAgInSc7d3	nejmohutnější
náboženským	náboženský	k2eAgInSc7d1	náboženský
proudem	proud	k1gInSc7	proud
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
katolicismu	katolicismus	k1gInSc3	katolicismus
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
hlásí	hlásit	k5eAaImIp3nS	hlásit
Brazilská	brazilský	k2eAgFnSc1d1	brazilská
katolická	katolický	k2eAgFnSc1d1	katolická
apoštolská	apoštolský	k2eAgFnSc1d1	apoštolská
církev	církev	k1gFnSc1	církev
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
půl	půl	k1xP	půl
milionem	milion	k4xCgInSc7	milion
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
odštěpila	odštěpit	k5eAaPmAgFnS	odštěpit
a	a	k8xC	a
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
na	na	k7c4	na
Římu	Řím	k1gInSc3	Řím
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Protestantismus	protestantismus	k1gInSc4	protestantismus
===	===	k?	===
</s>
</p>
<p>
<s>
Protestantismus	protestantismus	k1gInSc1	protestantismus
se	se	k3xPyFc4	se
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
šířil	šířit	k5eAaImAgMnS	šířit
zejména	zejména	k9	zejména
s	s	k7c7	s
působením	působení	k1gNnSc7	působení
amerických	americký	k2eAgMnPc2d1	americký
misionářů	misionář	k1gMnPc2	misionář
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
pol.	pol.	k?	pol.
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zažívá	zažívat	k5eAaImIp3nS	zažívat
nebývalý	bývalý	k2eNgInSc4d1	bývalý
vzestup	vzestup	k1gInSc4	vzestup
a	a	k8xC	a
k	k	k7c3	k
nejrůznějším	různý	k2eAgFnPc3d3	nejrůznější
protestantským	protestantský	k2eAgFnPc3d1	protestantská
církvím	církev	k1gFnPc3	církev
a	a	k8xC	a
hnutím	hnutí	k1gNnPc3	hnutí
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
hlásilo	hlásit	k5eAaImAgNnS	hlásit
již	již	k6eAd1	již
přes	přes	k7c4	přes
22	[number]	k4	22
%	%	kIx~	%
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
15	[number]	k4	15
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dominantní	dominantní	k2eAgNnSc1d1	dominantní
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
rámci	rámec	k1gInSc6	rámec
jsou	být	k5eAaImIp3nP	být
pentekostální	pentekostální	k2eAgInPc1d1	pentekostální
směry	směr	k1gInPc1	směr
<g/>
:	:	kIx,	:
mohutný	mohutný	k2eAgInSc1d1	mohutný
růst	růst	k1gInSc1	růst
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
především	především	k9	především
Shromáždění	shromáždění	k1gNnSc4	shromáždění
Boží	boží	k2eAgFnSc2d1	boží
(	(	kIx(	(
<g/>
12,3	[number]	k4	12,3
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vs	vs	k?	vs
<g/>
.	.	kIx.	.
8,4	[number]	k4	8,4
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
silnější	silný	k2eAgNnSc4d2	silnější
postavení	postavení	k1gNnSc4	postavení
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
dále	daleko	k6eAd2	daleko
mají	mít	k5eAaImIp3nP	mít
Brazilská	brazilský	k2eAgFnSc1d1	brazilská
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
kongregace	kongregace	k1gFnSc1	kongregace
(	(	kIx(	(
<g/>
2,3	[number]	k4	2,3
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
církev	církev	k1gFnSc1	církev
království	království	k1gNnSc2	království
Božího	boží	k2eAgInSc2d1	boží
(	(	kIx(	(
<g/>
1,9	[number]	k4	1,9
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
tradičními	tradiční	k2eAgFnPc7d1	tradiční
protestantskými	protestantský	k2eAgFnPc7d1	protestantská
církvemi	církev	k1gFnPc7	církev
mají	mít	k5eAaImIp3nP	mít
nejvíce	nejvíce	k6eAd1	nejvíce
členů	člen	k1gInPc2	člen
baptisté	baptista	k1gMnPc1	baptista
(	(	kIx(	(
<g/>
asi	asi	k9	asi
3,7	[number]	k4	3,7
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
protestantismu	protestantismus	k1gInSc3	protestantismus
se	se	k3xPyFc4	se
však	však	k9	však
celkem	celkem	k6eAd1	celkem
hlásí	hlásit	k5eAaImIp3nP	hlásit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
15	[number]	k4	15
významnějších	významný	k2eAgFnPc2d2	významnější
církví	církev	k1gFnPc2	církev
či	či	k8xC	či
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
,	,	kIx,	,
z	z	k7c2	z
početnějších	početní	k2eAgInPc2d2	početnější
dále	daleko	k6eAd2	daleko
např.	např.	kA	např.
pentekostální	pentekostální	k2eAgFnSc1d1	pentekostální
Igreja	Igreja	k1gFnSc1	Igreja
do	do	k7c2	do
Evangelho	Evangel	k1gMnSc2	Evangel
Quadrangular	Quadrangulara	k1gFnPc2	Quadrangulara
(	(	kIx(	(
<g/>
1,8	[number]	k4	1,8
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
či	či	k8xC	či
tradiční	tradiční	k2eAgMnPc1d1	tradiční
Adventisté	adventista	k1gMnPc1	adventista
sedmého	sedmý	k4xOgInSc2	sedmý
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
1,6	[number]	k4	1,6
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
luteráni	luterán	k1gMnPc1	luterán
(	(	kIx(	(
<g/>
1	[number]	k4	1
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgNnPc1d1	ostatní
náboženství	náboženství	k1gNnPc1	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
ostatními	ostatní	k2eAgNnPc7d1	ostatní
náboženstvími	náboženství	k1gNnPc7	náboženství
převažuje	převažovat	k5eAaImIp3nS	převažovat
spiritismus	spiritismus	k1gInSc1	spiritismus
(	(	kIx(	(
<g/>
3,8	[number]	k4	3,8
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vs	vs	k?	vs
<g/>
.	.	kIx.	.
2,3	[number]	k4	2,3
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
(	(	kIx(	(
<g/>
1,4	[number]	k4	1,4
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
afro-brazilské	afrorazilský	k2eAgInPc1d1	afro-brazilský
synkretické	synkretický	k2eAgInPc1d1	synkretický
kulty	kult	k1gInPc1	kult
umbanda	umbando	k1gNnSc2	umbando
(	(	kIx(	(
<g/>
400	[number]	k4	400
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
)	)	kIx)	)
a	a	k8xC	a
candomblé	candomblý	k2eAgNnSc1d1	candomblé
(	(	kIx(	(
<g/>
170	[number]	k4	170
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
240	[number]	k4	240
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
