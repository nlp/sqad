<s>
Známější	známý	k2eAgMnSc1d2	známější
z	z	k7c2	z
obou	dva	k4xCgMnPc2	dva
bratrů	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
Jacob	Jacoba	k1gFnPc2	Jacoba
Grimm	Grimma	k1gFnPc2	Grimma
(	(	kIx(	(
<g/>
1785	[number]	k4	1785
<g/>
–	–	k?	–
<g/>
1863	[number]	k4	1863
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
německých	německý	k2eAgMnPc2d1	německý
jazykovědců	jazykovědec	k1gMnPc2	jazykovědec
<g/>
.	.	kIx.	.
</s>
