<s>
Bratři	bratr	k1gMnPc1	bratr
Grimmové	Grimmová	k1gFnSc2	Grimmová
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Brüder	Brüder	k1gMnSc1	Brüder
Grimm	Grimm	k1gMnSc1	Grimm
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Gebrüder	Gebrüder	k1gMnSc1	Gebrüder
Grimm	Grimm	k1gMnSc1	Grimm
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
sběratelé	sběratel	k1gMnPc1	sběratel
klasických	klasický	k2eAgFnPc2d1	klasická
lidových	lidový	k2eAgFnPc2d1	lidová
německých	německý	k2eAgFnPc2d1	německá
pohádek	pohádka	k1gFnPc2	pohádka
a	a	k8xC	a
mytologických	mytologický	k2eAgInPc2d1	mytologický
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Známější	známý	k2eAgMnSc1d2	známější
z	z	k7c2	z
obou	dva	k4xCgMnPc2	dva
bratrů	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
Jacob	Jacoba	k1gFnPc2	Jacoba
Grimm	Grimma	k1gFnPc2	Grimma
(	(	kIx(	(
<g/>
1785	[number]	k4	1785
<g/>
–	–	k?	–
<g/>
1863	[number]	k4	1863
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
německých	německý	k2eAgMnPc2d1	německý
jazykovědců	jazykovědec	k1gMnPc2	jazykovědec
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgMnSc1d2	mladší
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Grimm	Grimm	k1gMnSc1	Grimm
(	(	kIx(	(
<g/>
1786	[number]	k4	1786
<g/>
–	–	k?	–
<g/>
1859	[number]	k4	1859
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgInS	zapsat
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
jako	jako	k8xS	jako
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
svého	svůj	k3xOyFgMnSc2	svůj
staršího	starý	k2eAgMnSc2d2	starší
bratra	bratr	k1gMnSc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Bratří	bratr	k1gMnPc1	bratr
Grimmové	Grimmová	k1gFnSc2	Grimmová
prosluli	proslout	k5eAaPmAgMnP	proslout
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
i	i	k8xC	i
světové	světový	k2eAgFnSc6d1	světová
literatuře	literatura	k1gFnSc6	literatura
svou	svůj	k3xOyFgFnSc4	svůj
sbírkou	sbírka	k1gFnSc7	sbírka
lidových	lidový	k2eAgFnPc2d1	lidová
pohádek	pohádka	k1gFnPc2	pohádka
a	a	k8xC	a
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
pověstí	pověst	k1gFnPc2	pověst
a	a	k8xC	a
legend	legenda	k1gFnPc2	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
svými	svůj	k3xOyFgMnPc7	svůj
Kinder-und	Kindernd	k1gInSc4	Kinder-und
Hausmärchen	Hausmärchen	k1gInSc4	Hausmärchen
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Pohádky	pohádka	k1gFnPc1	pohádka
bratří	bratr	k1gMnPc2	bratr
Grimmů	Grimm	k1gInPc2	Grimm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
položili	položit	k5eAaPmAgMnP	položit
základy	základ	k1gInPc1	základ
nové	nový	k2eAgFnSc2d1	nová
vědy	věda	k1gFnSc2	věda
–	–	k?	–
folkloristiky	folkloristika	k1gFnSc2	folkloristika
<g/>
.	.	kIx.	.
</s>
<s>
Pohádky	pohádka	k1gFnPc1	pohádka
jako	jako	k8xC	jako
Sněhurka	Sněhurka	k1gFnSc1	Sněhurka
či	či	k8xC	či
Šípková	šípkový	k2eAgFnSc1d1	šípková
Růženka	Růženka	k1gFnSc1	Růženka
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
zpracování	zpracování	k1gNnPc2	zpracování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prvními	první	k4xOgMnPc7	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
je	on	k3xPp3gNnSc4	on
zapsali	zapsat	k5eAaPmAgMnP	zapsat
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
právě	právě	k6eAd1	právě
bratří	bratr	k1gMnPc1	bratr
Grimmové	Grimmová	k1gFnSc2	Grimmová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
činorodé	činorodý	k2eAgFnSc6d1	činorodá
spolupráci	spolupráce	k1gFnSc6	spolupráce
byl	být	k5eAaImAgMnS	být
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
pohádky	pohádka	k1gFnPc4	pohádka
sbíral	sbírat	k5eAaImAgMnS	sbírat
a	a	k8xC	a
upravoval	upravovat	k5eAaImAgMnS	upravovat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Jacob	Jacoba	k1gFnPc2	Jacoba
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
byl	být	k5eAaImAgMnS	být
lingvista	lingvista	k1gMnSc1	lingvista
a	a	k8xC	a
filolog	filolog	k1gMnSc1	filolog
<g/>
,	,	kIx,	,
pohádky	pohádka	k1gFnPc4	pohádka
povýšil	povýšit	k5eAaPmAgMnS	povýšit
na	na	k7c4	na
předmět	předmět	k1gInSc4	předmět
seriózního	seriózní	k2eAgInSc2d1	seriózní
vědeckého	vědecký	k2eAgInSc2d1	vědecký
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Lidové	lidový	k2eAgFnPc1d1	lidová
pohádky	pohádka	k1gFnPc1	pohádka
bratřím	bratr	k1gMnPc3	bratr
vyprávělo	vyprávět	k5eAaImAgNnS	vyprávět
dohromady	dohromady	k6eAd1	dohromady
asi	asi	k9	asi
40	[number]	k4	40
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
vypravěček	vypravěčka	k1gFnPc2	vypravěčka
byla	být	k5eAaImAgFnS	být
Marie	Marie	k1gFnSc1	Marie
Hassenpflugová	Hassenpflugový	k2eAgFnSc1d1	Hassenpflugový
<g/>
,	,	kIx,	,
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
jejich	jejich	k3xOp3gFnSc2	jejich
sestry	sestra	k1gFnSc2	sestra
Charlotty	Charlotta	k1gFnSc2	Charlotta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
dobře	dobře	k6eAd1	dobře
situované	situovaný	k2eAgFnSc2d1	situovaná
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
mluvící	mluvící	k2eAgFnSc2d1	mluvící
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Mariininy	Mariinin	k2eAgInPc1d1	Mariinin
příběhy	příběh	k1gInPc1	příběh
byly	být	k5eAaImAgInP	být
směsí	směs	k1gFnSc7	směs
motivů	motiv	k1gInPc2	motiv
pocházejících	pocházející	k2eAgInPc2d1	pocházející
z	z	k7c2	z
lidové	lidový	k2eAgFnSc2d1	lidová
tradice	tradice	k1gFnSc2	tradice
a	a	k8xC	a
vlivů	vliv	k1gInPc2	vliv
z	z	k7c2	z
četby	četba	k1gFnSc2	četba
Perraultových	Perraultův	k2eAgFnPc2d1	Perraultova
Pohádek	pohádka	k1gFnPc2	pohádka
Matky	matka	k1gFnSc2	matka
husy	husa	k1gFnSc2	husa
(	(	kIx(	(
<g/>
1697	[number]	k4	1697
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
se	se	k3xPyFc4	se
bratři	bratr	k1gMnPc1	bratr
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
Göttingenu	Göttingen	k1gInSc2	Göttingen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Jacob	Jacoba	k1gFnPc2	Jacoba
stal	stát	k5eAaPmAgInS	stát
univerzitním	univerzitní	k2eAgMnSc7d1	univerzitní
knihovníkem	knihovník	k1gMnSc7	knihovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1837	[number]	k4	1837
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
propuštěni	propustit	k5eAaPmNgMnP	propustit
za	za	k7c4	za
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
zrušení	zrušení	k1gNnSc3	zrušení
tzv.	tzv.	kA	tzv.
Hannoverské	hannoverský	k2eAgFnSc2d1	hannoverská
konstituce	konstituce	k1gFnSc2	konstituce
králem	král	k1gMnSc7	král
Ernestem	Ernest	k1gMnSc7	Ernest
Augustem	August	k1gMnSc7	August
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
se	se	k3xPyFc4	se
bratři	bratr	k1gMnPc1	bratr
Grimmové	Grimmové	k2eAgMnPc1d1	Grimmové
stávají	stávat	k5eAaImIp3nP	stávat
profesory	profesor	k1gMnPc4	profesor
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
německém	německý	k2eAgInSc6d1	německý
výkladovém	výkladový	k2eAgInSc6d1	výkladový
slovníku	slovník	k1gInSc6	slovník
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
svazek	svazek	k1gInSc4	svazek
tohoto	tento	k3xDgNnSc2	tento
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
díla	dílo	k1gNnSc2	dílo
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
16	[number]	k4	16
<g/>
.	.	kIx.	.
svazek	svazek	k1gInSc1	svazek
vyšel	vyjít	k5eAaPmAgInS	vyjít
až	až	k9	až
dlouho	dlouho	k6eAd1	dlouho
po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
Grimmové	Grimmová	k1gFnSc2	Grimmová
pracovali	pracovat	k5eAaImAgMnP	pracovat
velmi	velmi	k6eAd1	velmi
činorodě	činorodě	k6eAd1	činorodě
a	a	k8xC	a
důkladně	důkladně	k6eAd1	důkladně
také	také	k9	také
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
heroické	heroický	k2eAgFnSc2d1	heroická
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
starověkého	starověký	k2eAgNnSc2d1	starověké
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
úsilí	úsilí	k1gNnSc1	úsilí
nepolevilo	polevit	k5eNaPmAgNnS	polevit
ani	ani	k8xC	ani
když	když	k8xS	když
se	se	k3xPyFc4	se
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1825	[number]	k4	1825
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Jacob	Jacoba	k1gFnPc2	Jacoba
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neoženil	oženit	k5eNaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1859	[number]	k4	1859
a	a	k8xC	a
Jacob	Jacoba	k1gFnPc2	Jacoba
o	o	k7c4	o
4	[number]	k4	4
roky	rok	k1gInPc7	rok
později	pozdě	k6eAd2	pozdě
tamtéž	tamtéž	k6eAd1	tamtéž
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1863	[number]	k4	1863
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
Grimmové	Grimmová	k1gFnSc2	Grimmová
tvořili	tvořit	k5eAaImAgMnP	tvořit
dokonalý	dokonalý	k2eAgInSc4d1	dokonalý
pracovní	pracovní	k2eAgInSc4d1	pracovní
tým	tým	k1gInSc4	tým
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
Jacob	Jacoba	k1gFnPc2	Jacoba
zaměřoval	zaměřovat	k5eAaImAgInS	zaměřovat
na	na	k7c4	na
studia	studio	k1gNnPc4	studio
lingvistická	lingvistický	k2eAgNnPc4d1	lingvistické
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
byl	být	k5eAaImAgMnS	být
spíše	spíše	k9	spíše
literárním	literární	k2eAgMnSc7d1	literární
badatelem	badatel	k1gMnSc7	badatel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
byli	být	k5eAaImAgMnP	být
ovlivnění	ovlivnění	k1gNnSc4	ovlivnění
současným	současný	k2eAgInSc7d1	současný
německým	německý	k2eAgInSc7d1	německý
romantismem	romantismus	k1gInSc7	romantismus
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
láskou	láska	k1gFnSc7	láska
k	k	k7c3	k
mytologii	mytologie	k1gFnSc3	mytologie
<g/>
,	,	kIx,	,
folklóru	folklór	k1gInSc3	folklór
a	a	k8xC	a
fantaziím	fantazie	k1gFnPc3	fantazie
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
prací	práce	k1gFnSc7	práce
dokázali	dokázat	k5eAaPmAgMnP	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
základě	základ	k1gInSc6	základ
studia	studio	k1gNnSc2	studio
ústních	ústní	k2eAgMnPc2d1	ústní
pramenů	pramen	k1gInPc2	pramen
<g/>
,	,	kIx,	,
muže	muž	k1gMnSc4	muž
být	být	k5eAaImF	být
sestavena	sestavit	k5eAaPmNgFnS	sestavit
sbírka	sbírka	k1gFnSc1	sbírka
folklórních	folklórní	k2eAgInPc2d1	folklórní
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
o	o	k7c4	o
ryzí	ryzí	k2eAgFnSc4d1	ryzí
reprodukci	reprodukce	k1gFnSc4	reprodukce
originálních	originální	k2eAgInPc2d1	originální
motivů	motiv	k1gInPc2	motiv
<g/>
,	,	kIx,	,
a	a	k8xC	a
ukázali	ukázat	k5eAaPmAgMnP	ukázat
tak	tak	k9	tak
cestu	cesta	k1gFnSc4	cesta
dalším	další	k2eAgMnPc3d1	další
badatelům	badatel	k1gMnPc3	badatel
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
byly	být	k5eAaImAgInP	být
sesbírané	sesbíraný	k2eAgInPc1d1	sesbíraný
příběhy	příběh	k1gInPc1	příběh
zaznamenány	zaznamenán	k2eAgInPc1d1	zaznamenán
s	s	k7c7	s
co	co	k9	co
největší	veliký	k2eAgFnSc7d3	veliký
přesností	přesnost	k1gFnSc7	přesnost
a	a	k8xC	a
věrností	věrnost	k1gFnSc7	věrnost
<g/>
,	,	kIx,	,
přece	přece	k9	přece
však	však	k9	však
se	se	k3xPyFc4	se
zvláště	zvláště	k6eAd1	zvláště
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
při	při	k7c6	při
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
vydáních	vydání	k1gNnPc6	vydání
neubránil	ubránit	k5eNaPmAgInS	ubránit
svým	svůj	k3xOyFgFnPc3	svůj
literárním	literární	k2eAgFnPc3d1	literární
ambicím	ambice	k1gFnPc3	ambice
a	a	k8xC	a
některým	některý	k3yIgFnPc3	některý
pohádkám	pohádka	k1gFnPc3	pohádka
dodal	dodat	k5eAaPmAgInS	dodat
poetický	poetický	k2eAgInSc1d1	poetický
nádech	nádech	k1gInSc1	nádech
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
celoživotní	celoživotní	k2eAgFnSc7d1	celoživotní
prací	práce	k1gFnSc7	práce
bratři	bratr	k1gMnPc1	bratr
Grimmové	Grimmová	k1gFnSc2	Grimmová
dokázali	dokázat	k5eAaPmAgMnP	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pohádky	pohádka	k1gFnPc1	pohádka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zaznamenávány	zaznamenáván	k2eAgFnPc1d1	zaznamenávána
a	a	k8xC	a
poté	poté	k6eAd1	poté
uveřejněny	uveřejněn	k2eAgFnPc1d1	uveřejněna
v	v	k7c6	v
tištěné	tištěný	k2eAgFnSc6d1	tištěná
formě	forma	k1gFnSc6	forma
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
originálním	originální	k2eAgNnSc6d1	originální
znění	znění	k1gNnSc6	znění
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
být	být	k5eAaImF	být
dále	daleko	k6eAd2	daleko
šířeny	šířen	k2eAgFnPc4d1	šířena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
byly	být	k5eAaImAgInP	být
pak	pak	k6eAd1	pak
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
pohádky	pohádka	k1gFnPc4	pohádka
upravovány	upravován	k2eAgFnPc4d1	upravována
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ve	v	k7c6	v
Sněhurce	Sněhurka	k1gFnSc6	Sněhurka
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
konec	konec	k1gInSc1	konec
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
znění	znění	k1gNnSc6	znění
musela	muset	k5eAaImAgFnS	muset
zlá	zlý	k2eAgFnSc1d1	zlá
královna	královna	k1gFnSc1	královna
tančit	tančit	k5eAaImF	tančit
v	v	k7c6	v
rozžhavených	rozžhavený	k2eAgFnPc6d1	rozžhavená
botách	bota	k1gFnPc6	bota
až	až	k6eAd1	až
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Jeníčkovi	Jeníček	k1gMnSc6	Jeníček
a	a	k8xC	a
Mařence	Mařenka	k1gFnSc6	Mařenka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
zlá	zlý	k2eAgFnSc1d1	zlá
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
mukách	muka	k1gNnPc6	muka
zaživa	zaživa	k6eAd1	zaživa
upečena	upéct	k5eAaPmNgFnS	upéct
v	v	k7c6	v
peci	pec	k1gFnSc6	pec
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
nezůstaly	zůstat	k5eNaPmAgFnP	zůstat
pohádky	pohádka	k1gFnPc1	pohádka
bratří	bratr	k1gMnPc2	bratr
Grimmů	Grimm	k1gInPc2	Grimm
ušetřeny	ušetřen	k2eAgFnPc1d1	ušetřena
pozornosti	pozornost	k1gFnPc1	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
nacisté	nacista	k1gMnPc1	nacista
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
německého	německý	k2eAgInSc2d1	německý
lidu	lid	k1gInSc2	lid
pohádku	pohádka	k1gFnSc4	pohádka
O	o	k7c6	o
červené	červený	k2eAgFnSc6d1	červená
Karkulce	Karkulka	k1gFnSc6	Karkulka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
interpretaci	interpretace	k1gFnSc6	interpretace
byla	být	k5eAaImAgFnS	být
zachráněna	zachránit	k5eAaPmNgFnS	zachránit
před	před	k7c7	před
zlým	zlý	k2eAgMnSc7d1	zlý
židovským	židovský	k2eAgMnSc7d1	židovský
vlkem	vlk	k1gMnSc7	vlk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
pohádky	pohádka	k1gFnPc1	pohádka
bratří	bratr	k1gMnPc2	bratr
Grimmů	Grimm	k1gInPc2	Grimm
zase	zase	k9	zase
předmětem	předmět	k1gInSc7	předmět
pohrdání	pohrdání	k1gNnSc2	pohrdání
a	a	k8xC	a
kritiky	kritika	k1gFnSc2	kritika
feministického	feministický	k2eAgNnSc2d1	feministické
hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
neskrývaný	skrývaný	k2eNgInSc4d1	neskrývaný
sexistický	sexistický	k2eAgInSc4d1	sexistický
popis	popis	k1gInSc4	popis
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
činnosti	činnost	k1gFnSc2	činnost
folkloristické	folkloristický	k2eAgFnSc2d1	folkloristická
se	se	k3xPyFc4	se
bratři	bratr	k1gMnPc1	bratr
Grimmové	Grimmová	k1gFnSc2	Grimmová
věnovali	věnovat	k5eAaImAgMnP	věnovat
i	i	k8xC	i
jazykovědě	jazykověda	k1gFnSc3	jazykověda
–	–	k?	–
spolu	spolu	k6eAd1	spolu
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1838	[number]	k4	1838
projekt	projekt	k1gInSc1	projekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
Deutsches	Deutschesa	k1gFnPc2	Deutschesa
Wörterbuch	Wörterbuch	k1gInSc1	Wörterbuch
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
slovníků	slovník	k1gInPc2	slovník
německého	německý	k2eAgInSc2d1	německý
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
dodnes	dodnes	k6eAd1	dodnes
a	a	k8xC	a
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
dvaatřiceti	dvaatřicet	k4xCc7	dvaatřicet
svazky	svazek	k1gInPc7	svazek
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
také	také	k9	také
k	k	k7c3	k
nejrozsáhlejším	rozsáhlý	k2eAgInPc3d3	nejrozsáhlejší
slovníkům	slovník	k1gInPc3	slovník
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
dílem	díl	k1gInSc7	díl
Jacoba	Jacoba	k1gFnSc1	Jacoba
Grimma	Grimma	k1gFnSc1	Grimma
byl	být	k5eAaImAgInS	být
spis	spis	k1gInSc1	spis
Deutsche	Deutsch	k1gFnSc2	Deutsch
Mythologie	mythologie	k1gFnSc2	mythologie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
rozsáhle	rozsáhle	k6eAd1	rozsáhle
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
germánské	germánský	k2eAgFnSc6d1	germánská
mytologii	mytologie	k1gFnSc6	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Pohádky	pohádka	k1gFnPc1	pohádka
bratří	bratr	k1gMnPc2	bratr
Grimmů	Grimm	k1gInPc2	Grimm
(	(	kIx(	(
<g/>
Kinder-	Kinder-	k1gFnSc1	Kinder-
und	und	k?	und
Hausmärchen	Hausmärchen	k1gInSc4	Hausmärchen
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
publikovány	publikovat	k5eAaBmNgInP	publikovat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
svazcích	svazek	k1gInPc6	svazek
(	(	kIx(	(
<g/>
1812	[number]	k4	1812
<g/>
–	–	k?	–
<g/>
1815	[number]	k4	1815
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc1d1	poslední
edice	edice	k1gFnSc1	edice
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
a	a	k8xC	a
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
211	[number]	k4	211
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
,	,	kIx,	,
dalších	další	k2eAgMnPc2d1	další
28	[number]	k4	28
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
edicích	edice	k1gFnPc6	edice
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
celkem	celkem	k6eAd1	celkem
tedy	tedy	k9	tedy
sbírka	sbírka	k1gFnSc1	sbírka
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
239	[number]	k4	239
pohádkových	pohádkový	k2eAgInPc2d1	pohádkový
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Pohádky	pohádka	k1gFnPc1	pohádka
byly	být	k5eAaImAgFnP	být
zapsány	zapsat	k5eAaPmNgFnP	zapsat
podle	podle	k7c2	podle
ústní	ústní	k2eAgFnSc2d1	ústní
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
vydání	vydání	k1gNnSc6	vydání
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
10	[number]	k4	10
dialektů	dialekt	k1gInPc2	dialekt
německého	německý	k2eAgInSc2d1	německý
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
pohádky	pohádka	k1gFnPc1	pohádka
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
proslulými	proslulý	k2eAgFnPc7d1	proslulá
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
půvabné	půvabný	k2eAgFnPc1d1	půvabná
svým	svůj	k3xOyFgNnSc7	svůj
kouzlem	kouzlo	k1gNnSc7	kouzlo
<g/>
,	,	kIx,	,
motivy	motiv	k1gInPc4	motiv
komunikace	komunikace	k1gFnSc2	komunikace
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
zvířaty	zvíře	k1gNnPc7	zvíře
a	a	k8xC	a
morálním	morální	k2eAgInSc7d1	morální
patosem	patos	k1gInSc7	patos
boje	boj	k1gInSc2	boj
dobra	dobro	k1gNnSc2	dobro
se	s	k7c7	s
zlem	zlo	k1gNnSc7	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
publikované	publikovaný	k2eAgFnPc4d1	publikovaná
pohádky	pohádka	k1gFnPc4	pohádka
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
Sněhurka	Sněhurka	k1gFnSc1	Sněhurka
<g/>
,	,	kIx,	,
Červená	červený	k2eAgFnSc1d1	červená
karkulka	karkulka	k1gFnSc1	karkulka
<g/>
,	,	kIx,	,
Šípková	šípkový	k2eAgFnSc1d1	šípková
Růženka	Růženka	k1gFnSc1	Růženka
<g/>
,	,	kIx,	,
Jeníček	Jeníček	k1gMnSc1	Jeníček
a	a	k8xC	a
Mařenka	Mařenka	k1gFnSc1	Mařenka
<g/>
,	,	kIx,	,
Popelka	Popelka	k1gMnSc1	Popelka
<g/>
,	,	kIx,	,
Brémští	brémský	k2eAgMnPc1d1	brémský
muzikanti	muzikant	k1gMnPc1	muzikant
<g/>
,	,	kIx,	,
Dupynožka	Dupynožka	k1gFnSc1	Dupynožka
<g/>
,	,	kIx,	,
Tři	tři	k4xCgFnPc1	tři
přadleny	přadlena	k1gFnPc1	přadlena
nebo	nebo	k8xC	nebo
Bělinka	Bělinka	k1gFnSc1	Bělinka
a	a	k8xC	a
Růženka	Růženka	k1gFnSc1	Růženka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
také	také	k9	také
<g/>
:	:	kIx,	:
Čiperná	čiperný	k2eAgFnSc1d1	čiperná
Markyta	Markyta	k1gFnSc1	Markyta
<g/>
,	,	kIx,	,
Medvědáč	Medvědáč	k1gMnSc1	Medvědáč
<g/>
,	,	kIx,	,
Železný	Železný	k1gMnSc1	Železný
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Strakatinka	strakatinka	k1gFnSc1	strakatinka
<g/>
,	,	kIx,	,
Král	Král	k1gMnSc1	Král
Odřivous	odřivous	k1gMnSc1	odřivous
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Kletba	kletba	k1gFnSc1	kletba
Bratří	bratr	k1gMnPc2	bratr
Grimmů	Grimm	k1gInPc2	Grimm
<g/>
,	,	kIx,	,
česko-britsko-americký	českoritskomerický	k2eAgInSc1d1	česko-britsko-americký
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Terry	Terra	k1gFnSc2	Terra
Gilliam	Gilliam	k1gInSc1	Gilliam
<g/>
,	,	kIx,	,
historická	historický	k2eAgFnSc1d1	historická
fikce	fikce	k1gFnSc1	fikce
ze	z	k7c2	z
života	život	k1gInSc2	život
obou	dva	k4xCgNnPc2	dva
bratrů	bratr	k1gMnPc2	bratr
Věčný	věčný	k2eAgInSc4d1	věčný
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
pohádkový	pohádkový	k2eAgInSc1d1	pohádkový
romantický	romantický	k2eAgInSc1d1	romantický
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
zde	zde	k6eAd1	zde
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
filmového	filmový	k2eAgInSc2d1	filmový
příběhu	příběh	k1gInSc2	příběh
v	v	k7c6	v
rolích	role	k1gFnPc6	role
posluchačů	posluchač	k1gMnPc2	posluchač
vyprávěného	vyprávěný	k2eAgInSc2d1	vyprávěný
příběhu	příběh	k1gInSc2	příběh
Grimm	Grimm	k1gInSc1	Grimm
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
seriál	seriál	k1gInSc1	seriál
</s>
