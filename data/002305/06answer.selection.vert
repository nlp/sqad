<s>
Utrpení	utrpení	k1gNnSc1	utrpení
knížete	kníže	k1gMnSc2	kníže
Sternenhocha	Sternenhoch	k1gMnSc2	Sternenhoch
je	být	k5eAaImIp3nS	být
expresionistický	expresionistický	k2eAgInSc4d1	expresionistický
román	román	k1gInSc4	román
českého	český	k2eAgMnSc2d1	český
spisovatele	spisovatel	k1gMnSc2	spisovatel
Ladislava	Ladislav	k1gMnSc2	Ladislav
Klímy	Klíma	k1gMnSc2	Klíma
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
měsíc	měsíc	k1gInSc4	měsíc
po	po	k7c6	po
autorově	autorův	k2eAgFnSc6d1	autorova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
