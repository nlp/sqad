<s>
Účinná	účinný	k2eAgFnSc1d1	účinná
látka	látka	k1gFnSc1	látka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
marihuany	marihuana	k1gFnSc2	marihuana
<g/>
,	,	kIx,	,
tetrahydrocannabinol	tetrahydrocannabinol	k1gInSc1	tetrahydrocannabinol
(	(	kIx(	(
<g/>
THC	THC	kA	THC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
koncentracích	koncentrace	k1gFnPc6	koncentrace
<g/>
.	.	kIx.	.
</s>
