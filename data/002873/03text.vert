<s>
Hašiš	hašiš	k1gInSc1	hašiš
je	být	k5eAaImIp3nS	být
droga	droga	k1gFnSc1	droga
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
z	z	k7c2	z
květenství	květenství	k1gNnSc2	květenství
samičích	samičí	k2eAgFnPc2d1	samičí
rostlin	rostlina	k1gFnPc2	rostlina
konopí	konopí	k1gNnSc2	konopí
(	(	kIx(	(
<g/>
Cannabis	Cannabis	k1gFnSc1	Cannabis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejdostupnější	dostupný	k2eAgInSc1d3	nejdostupnější
a	a	k8xC	a
asi	asi	k9	asi
i	i	k9	i
nejčistší	čistý	k2eAgInSc1d3	nejčistší
způsob	způsob	k1gInSc1	způsob
je	být	k5eAaImIp3nS	být
oddělit	oddělit	k5eAaPmF	oddělit
pryskyřici	pryskyřice	k1gFnSc4	pryskyřice
přes	přes	k7c4	přes
jemné	jemný	k2eAgNnSc4d1	jemné
síto	síto	k1gNnSc4	síto
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
oka	oko	k1gNnSc2	oko
cca	cca	kA	cca
<g/>
.	.	kIx.	.
0,2	[number]	k4	0,2
-	-	kIx~	-
0,1	[number]	k4	0,1
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
prášek	prášek	k1gInSc1	prášek
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kief	kief	k1gInSc1	kief
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
kif	kif	k?	kif
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
tepla	teplo	k1gNnSc2	teplo
stlačuje	stlačovat	k5eAaImIp3nS	stlačovat
do	do	k7c2	do
výsledných	výsledný	k2eAgInPc2d1	výsledný
bloků	blok	k1gInPc2	blok
hašiše	hašiš	k1gInSc2	hašiš
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
hašiš	hašiš	k1gInSc1	hašiš
např.	např.	kA	např.
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
a	a	k8xC	a
řadě	řada	k1gFnSc6	řada
dalších	další	k2eAgInPc2d1	další
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
způsoby	způsob	k1gInPc1	způsob
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
tření	tření	k1gNnSc4	tření
květenství	květenství	k1gNnPc2	květenství
v	v	k7c6	v
rukách	ruka	k1gFnPc6	ruka
(	(	kIx(	(
<g/>
na	na	k7c6	na
rukou	ruka	k1gFnPc6	ruka
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
vrstva	vrstva	k1gFnSc1	vrstva
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
čaras	čaras	k1gInSc1	čaras
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
extrakci	extrakce	k1gFnSc4	extrakce
v	v	k7c6	v
organických	organický	k2eAgNnPc6d1	organické
rozpouštědlech	rozpouštědlo	k1gNnPc6	rozpouštědlo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
80	[number]	k4	80
<g/>
%	%	kIx~	%
líh	líh	k1gInSc1	líh
<g/>
,	,	kIx,	,
ether	ether	k1gInSc1	ether
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
beze	beze	k7c2	beze
zbytku	zbytek	k1gInSc2	zbytek
odpaří	odpařit	k5eAaPmIp3nS	odpařit
<g/>
.	.	kIx.	.
</s>
<s>
Účinná	účinný	k2eAgFnSc1d1	účinná
látka	látka	k1gFnSc1	látka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
marihuany	marihuana	k1gFnSc2	marihuana
<g/>
,	,	kIx,	,
tetrahydrocannabinol	tetrahydrocannabinol	k1gInSc1	tetrahydrocannabinol
(	(	kIx(	(
<g/>
THC	THC	kA	THC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
koncentracích	koncentrace	k1gFnPc6	koncentrace
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
hnědou	hnědý	k2eAgFnSc4d1	hnědá
nebo	nebo	k8xC	nebo
tmavě	tmavě	k6eAd1	tmavě
zelenou	zelený	k2eAgFnSc4d1	zelená
až	až	k8xS	až
černou	černý	k2eAgFnSc4d1	černá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pevného	pevný	k2eAgNnSc2d1	pevné
skupenství	skupenství	k1gNnSc2	skupenství
<g/>
,	,	kIx,	,
po	po	k7c6	po
zahřátí	zahřátí	k1gNnSc6	zahřátí
měkne	měknout	k5eAaImIp3nS	měknout
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
pevné	pevný	k2eAgFnSc2d1	pevná
formy	forma	k1gFnSc2	forma
také	také	k9	také
existuje	existovat	k5eAaImIp3nS	existovat
hašišový	hašišový	k2eAgInSc1d1	hašišový
olej	olej	k1gInSc1	olej
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
silnější	silný	k2eAgInSc1d2	silnější
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
buď	buď	k8xC	buď
extrakcí	extrakce	k1gFnSc7	extrakce
pomocí	pomocí	k7c2	pomocí
butanu	butan	k1gInSc2	butan
(	(	kIx(	(
<g/>
v	v	k7c6	v
kapalném	kapalný	k2eAgNnSc6d1	kapalné
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
extrakcí	extrakce	k1gFnPc2	extrakce
v	v	k7c6	v
nepolárních	polární	k2eNgNnPc6d1	nepolární
rozpouštědlech	rozpouštědlo	k1gNnPc6	rozpouštědlo
<g/>
.	.	kIx.	.
</s>
<s>
Obojí	oboj	k1gFnSc7	oboj
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
komplikované	komplikovaný	k2eAgNnSc1d1	komplikované
a	a	k8xC	a
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
(	(	kIx(	(
<g/>
butan	butan	k1gInSc1	butan
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
hořlavý	hořlavý	k2eAgInSc1d1	hořlavý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Účinky	účinek	k1gInPc1	účinek
jsou	být	k5eAaImIp3nP	být
závislé	závislý	k2eAgInPc1d1	závislý
na	na	k7c6	na
typu	typ	k1gInSc6	typ
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
marihuany	marihuana	k1gFnSc2	marihuana
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
je	být	k5eAaImIp3nS	být
vnímán	vnímat	k5eAaImNgInS	vnímat
zkresleně	zkresleně	k6eAd1	zkresleně
<g/>
,	,	kIx,	,
plyne	plynout	k5eAaImIp3nS	plynout
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
<s>
Zvuky	zvuk	k1gInPc1	zvuk
a	a	k8xC	a
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
vnímány	vnímat	k5eAaImNgFnP	vnímat
jasněji	jasně	k6eAd2	jasně
<g/>
,	,	kIx,	,
při	při	k7c6	při
požití	požití	k1gNnSc6	požití
větších	veliký	k2eAgFnPc2d2	veliký
dávek	dávka	k1gFnPc2	dávka
mohou	moct	k5eAaImIp3nP	moct
přejít	přejít	k5eAaPmF	přejít
až	až	k9	až
ve	v	k7c4	v
zrakové	zrakový	k2eAgFnPc4d1	zraková
nebo	nebo	k8xC	nebo
sluchové	sluchový	k2eAgFnPc4d1	sluchová
halucinace	halucinace	k1gFnPc4	halucinace
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
pocity	pocit	k1gInPc4	pocit
depersonalizace	depersonalizace	k1gFnSc2	depersonalizace
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
poruchy	porucha	k1gFnSc2	porucha
krátkodobé	krátkodobý	k2eAgFnSc2d1	krátkodobá
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
kostička	kostička	k1gFnSc1	kostička
hašiše	hašiš	k1gInSc2	hašiš
nahřeje	nahřát	k5eAaPmIp3nS	nahřát
zapalovačem	zapalovač	k1gInSc7	zapalovač
<g/>
,	,	kIx,	,
nadrolí	nadrolit	k5eAaPmIp3nP	nadrolit
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
méně	málo	k6eAd2	málo
než	než	k8xS	než
250	[number]	k4	250
miligramů	miligram	k1gInPc2	miligram
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
smíchá	smíchat	k5eAaPmIp3nS	smíchat
se	se	k3xPyFc4	se
s	s	k7c7	s
tabákem	tabák	k1gInSc7	tabák
a	a	k8xC	a
ubalí	ubalit	k5eAaPmIp3nS	ubalit
se	se	k3xPyFc4	se
hašišový	hašišový	k2eAgInSc1d1	hašišový
joint	joint	k1gInSc1	joint
<g/>
.	.	kIx.	.
</s>
<s>
Nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
kouří	kouřit	k5eAaImIp3nS	kouřit
samotný	samotný	k2eAgInSc1d1	samotný
ve	v	k7c6	v
vodní	vodní	k2eAgFnSc6d1	vodní
dýmce	dýmka	k1gFnSc6	dýmka
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
používat	používat	k5eAaImF	používat
i	i	k9	i
ústně	ústně	k6eAd1	ústně
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
(	(	kIx(	(
<g/>
asi	asi	k9	asi
2	[number]	k4	2
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
míchá	míchat	k5eAaImIp3nS	míchat
s	s	k7c7	s
něčím	něco	k3yInSc7	něco
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tuky	tuk	k1gInPc4	tuk
např.	např.	kA	např.
s	s	k7c7	s
jogurtem	jogurt	k1gInSc7	jogurt
nebo	nebo	k8xC	nebo
kapučínem	kapučín	k1gInSc7	kapučín
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
účinná	účinný	k2eAgFnSc1d1	účinná
látka	látka	k1gFnSc1	látka
je	být	k5eAaImIp3nS	být
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
v	v	k7c6	v
tucích	tuk	k1gInPc6	tuk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hašiš	hašiš	k1gInSc1	hašiš
lze	lze	k6eAd1	lze
také	také	k9	také
užívat	užívat	k5eAaImF	užívat
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
suchý	suchý	k2eAgInSc1d1	suchý
drink	drink	k1gInSc1	drink
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
hašiš	hašiš	k1gInSc1	hašiš
se	se	k3xPyFc4	se
uválí	uválet	k5eAaPmIp3nS	uválet
na	na	k7c4	na
váleček	váleček	k1gInSc4	váleček
<g/>
,	,	kIx,	,
na	na	k7c4	na
skleničku	sklenička	k1gFnSc4	sklenička
se	se	k3xPyFc4	se
položí	položit	k5eAaPmIp3nS	položit
kus	kus	k1gInSc1	kus
papíru	papír	k1gInSc2	papír
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pivní	pivní	k2eAgInSc1d1	pivní
tácek	tácek	k1gInSc1	tácek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
propíchne	propíchnout	k5eAaPmIp3nS	propíchnout
špendlík	špendlík	k1gInSc1	špendlík
<g/>
,	,	kIx,	,
hašiš	hašiš	k1gInSc1	hašiš
se	se	k3xPyFc4	se
zapíchne	zapíchnout	k5eAaPmIp3nS	zapíchnout
na	na	k7c4	na
špendlík	špendlík	k1gInSc4	špendlík
a	a	k8xC	a
zapálí	zapálit	k5eAaPmIp3nP	zapálit
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
hašiš	hašiš	k1gInSc1	hašiš
sfoukne	sfouknout	k5eAaPmIp3nS	sfouknout
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
dýmit	dýmit	k5eAaImF	dýmit
uvnitř	uvnitř	k7c2	uvnitř
skleničky	sklenička	k1gFnSc2	sklenička
<g/>
,	,	kIx,	,
po	po	k7c6	po
naplnění	naplnění	k1gNnSc6	naplnění
kouřem	kouř	k1gInSc7	kouř
se	se	k3xPyFc4	se
víko	víko	k1gNnSc1	víko
poodkryje	poodkrýt	k5eAaPmIp3nS	poodkrýt
a	a	k8xC	a
kouř	kouř	k1gInSc4	kouř
z	z	k7c2	z
hašiše	hašiš	k1gInSc2	hašiš
se	se	k3xPyFc4	se
vysaje	vysát	k5eAaPmIp3nS	vysát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
hašiš	hašiš	k1gInSc1	hašiš
ilegální	ilegální	k2eAgInSc1d1	ilegální
měkkou	měkký	k2eAgFnSc7d1	měkká
drogou	droga	k1gFnSc7	droga
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
černém	černý	k2eAgInSc6d1	černý
trhu	trh	k1gInSc6	trh
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
marocký	marocký	k2eAgInSc1d1	marocký
hašiš	hašiš	k1gInSc1	hašiš
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
jako	jako	k8xS	jako
např.	např.	kA	např.
afghánský	afghánský	k2eAgMnSc1d1	afghánský
nebo	nebo	k8xC	nebo
nepálský	nepálský	k2eAgMnSc1d1	nepálský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
1	[number]	k4	1
gramu	gram	k1gInSc2	gram
představuje	představovat	k5eAaImIp3nS	představovat
kvádr	kvádr	k1gInSc1	kvádr
veliký	veliký	k2eAgInSc1d1	veliký
asi	asi	k9	asi
15	[number]	k4	15
×	×	k?	×
8	[number]	k4	8
×	×	k?	×
4	[number]	k4	4
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Nekvalitní	kvalitní	k2eNgInSc1d1	nekvalitní
hašiš	hašiš	k1gInSc1	hašiš
se	se	k3xPyFc4	se
pozná	poznat	k5eAaPmIp3nS	poznat
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
drolí	drolit	k5eAaImIp3nS	drolit
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
hašiš	hašiš	k1gInSc1	hašiš
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
"	"	kIx"	"
<g/>
plastik	plastik	k1gInSc1	plastik
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
vysoce	vysoce	k6eAd1	vysoce
kvalitní	kvalitní	k2eAgNnSc1d1	kvalitní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hodně	hodně	k6eAd1	hodně
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
a	a	k8xC	a
nedrolí	drolit	k5eNaImIp3nS	drolit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
připomíná	připomínat	k5eAaImIp3nS	připomínat
spíš	spíš	k9	spíš
plastelínu	plastelína	k1gFnSc4	plastelína
<g/>
.	.	kIx.	.
</s>
<s>
Hašišový	hašišový	k2eAgInSc1d1	hašišový
olej	olej	k1gInSc1	olej
se	se	k3xPyFc4	se
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
nedováží	dovážit	k5eNaPmIp3nS	dovážit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
ho	on	k3xPp3gNnSc4	on
extrémně	extrémně	k6eAd1	extrémně
těžké	těžký	k2eAgNnSc1d1	těžké
sehnat	sehnat	k5eAaPmF	sehnat
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
pěstiteli	pěstitel	k1gMnPc7	pěstitel
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
domácí	domácí	k2eAgInSc4d1	domácí
hašiš	hašiš	k1gInSc4	hašiš
a	a	k8xC	a
haš	haš	k?	haš
olej	olej	k1gInSc1	olej
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kvalitnější	kvalitní	k2eAgInSc1d2	kvalitnější
než	než	k8xS	než
dovozový	dovozový	k2eAgInSc1d1	dovozový
hašiš	hašiš	k1gInSc1	hašiš
<g/>
.	.	kIx.	.
</s>
