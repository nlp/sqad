<p>
<s>
Haec	Haec	k1gFnSc1	Haec
sancta	sanct	k1gInSc2	sanct
je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
používaný	používaný	k2eAgInSc1d1	používaný
název	název	k1gInSc1	název
dekretu	dekret	k1gInSc2	dekret
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vydal	vydat	k5eAaPmAgInS	vydat
Kostnický	kostnický	k2eAgInSc1d1	kostnický
koncil	koncil	k1gInSc1	koncil
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1415	[number]	k4	1415
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
postulát	postulát	k1gInSc1	postulát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
deklaruje	deklarovat	k5eAaBmIp3nS	deklarovat
nadřazenost	nadřazenost	k1gFnSc4	nadřazenost
Koncilu	koncil	k1gInSc2	koncil
nad	nad	k7c7	nad
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Text	text	k1gInSc1	text
dekretu	dekret	k1gInSc2	dekret
==	==	k?	==
</s>
</p>
<p>
<s>
Text	text	k1gInSc1	text
dekretu	dekret	k1gInSc2	dekret
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
součástí	součást	k1gFnPc2	součást
akt	akt	k1gInSc1	akt
5	[number]	k4	5
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc1	zasedání
Kostnického	kostnický	k2eAgMnSc2d1	kostnický
rady	rada	k1gMnSc2	rada
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Dubna	duben	k1gInSc2	duben
1415	[number]	k4	1415
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
svaté	svatý	k2eAgFnSc2d1	svatá
a	a	k8xC	a
nerozdílné	rozdílný	k2eNgFnSc2d1	nerozdílná
Trojice	trojice	k1gFnSc2	trojice
–	–	k?	–
Otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Syn	syn	k1gMnSc1	syn
a	a	k8xC	a
Duch	duch	k1gMnSc1	duch
Svatý	svatý	k1gMnSc1	svatý
<g/>
.	.	kIx.	.
</s>
<s>
Amen	amen	k1gNnSc1	amen
<g/>
.	.	kIx.	.
<g/>
Tato	tento	k3xDgFnSc1	tento
svatá	svatý	k2eAgFnSc1d1	svatá
Kostnická	kostnický	k2eAgFnSc1d1	Kostnická
konference	konference	k1gFnSc1	konference
(	(	kIx(	(
<g/>
Haec	Haec	k1gFnSc1	Haec
sancta	sancta	k1gFnSc1	sancta
synodus	synodus	k1gInSc4	synodus
Constantiensis	Constantiensis	k1gInSc1	Constantiensis
<g/>
)	)	kIx)	)
,	,	kIx,	,
který	který	k3yIgInSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
obecný	obecný	k2eAgInSc1d1	obecný
ekumenický	ekumenický	k2eAgInSc1d1	ekumenický
koncil	koncil	k1gInSc1	koncil
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
sešel	sejít	k5eAaPmAgMnS	sejít
v	v	k7c6	v
Duchu	duch	k1gMnSc6	duch
Svatém	svatý	k2eAgMnSc6d1	svatý
k	k	k7c3	k
slávě	sláva	k1gFnSc3	sláva
Boha	bůh	k1gMnSc2	bůh
všemohoucího	všemohoucí	k2eAgMnSc2d1	všemohoucí
<g/>
,	,	kIx,	,
k	k	k7c3	k
dokončení	dokončení	k1gNnSc3	dokončení
jeho	jeho	k3xOp3gFnSc2	jeho
aktuální	aktuální	k2eAgFnSc2d1	aktuální
schizmu	schizmus	k1gInSc3	schizmus
<g/>
,	,	kIx,	,
sjednotit	sjednotit	k5eAaPmF	sjednotit
Boží	boží	k2eAgFnSc4d1	boží
církev	církev	k1gFnSc4	církev
a	a	k8xC	a
zreformoval	zreformovat	k5eAaPmAgMnS	zreformovat
její	její	k3xOp3gFnSc2	její
hlava	hlava	k1gFnSc1	hlava
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
končetiny	končetina	k1gFnPc4	končetina
–	–	k?	–
v	v	k7c6	v
dobré	dobrý	k2eAgFnSc6d1	dobrá
víře	víra	k1gFnSc6	víra
<g/>
,	,	kIx,	,
nestranně	stranně	k6eNd1	stranně
a	a	k8xC	a
rozhodně	rozhodně	k6eAd1	rozhodně
<g/>
,	,	kIx,	,
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
jednoty	jednota	k1gFnPc4	jednota
a	a	k8xC	a
obnovy	obnova	k1gFnPc4	obnova
církve	církev	k1gFnSc2	církev
Boží	boží	k2eAgFnSc2d1	boží
<g/>
,	,	kIx,	,
objednávky	objednávka	k1gFnPc4	objednávka
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
<g/>
,	,	kIx,	,
řeší	řešit	k5eAaImIp3nS	řešit
a	a	k8xC	a
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
následující	následující	k2eAgInSc1d1	následující
<g/>
:	:	kIx,	:
<g/>
Po	po	k7c6	po
prvé	prvý	k4xOgFnSc6	prvý
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
v	v	k7c4	v
Ducha	duch	k1gMnSc4	duch
Svatého	svatý	k1gMnSc4	svatý
zákonně	zákonně	k6eAd1	zákonně
shromážděny	shromážděn	k2eAgFnPc1d1	shromážděna
konference	konference	k1gFnPc1	konference
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
obecné	obecný	k2eAgFnSc2d1	obecná
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
bojující	bojující	k2eAgFnSc4d1	bojující
katolickou	katolický	k2eAgFnSc4d1	katolická
církev	církev	k1gFnSc4	církev
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
plnou	plný	k2eAgFnSc4d1	plná
moc	moc	k1gFnSc4	moc
(	(	kIx(	(
<g/>
potestas	potestas	k1gInSc4	potestas
<g/>
)	)	kIx)	)
bezprostředně	bezprostředně	k6eAd1	bezprostředně
od	od	k7c2	od
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
hodnosti	hodnost	k1gFnSc2	hodnost
<g/>
,	,	kIx,	,
i	i	k9	i
kdyby	kdyby	kYmCp3nS	kdyby
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
papež	papež	k1gMnSc1	papež
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
uposlechnout	uposlechnout	k5eAaPmF	uposlechnout
<g/>
:	:	kIx,	:
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
překonání	překonání	k1gNnSc1	překonání
rozkolu	rozkol	k1gInSc2	rozkol
a	a	k8xC	a
reforma	reforma	k1gFnSc1	reforma
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
končetiny	končetina	k1gFnPc4	končetina
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
Boží	božit	k5eAaImIp3nS	božit
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
<g/>
Dále	daleko	k6eAd2	daleko
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikomu	nikdo	k3yNnSc3	nikdo
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
každého	každý	k3xTgNnSc2	každý
povolání	povolání	k1gNnSc2	povolání
<g/>
,	,	kIx,	,
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
hodnosti	hodnost	k1gFnSc2	hodnost
<g/>
,	,	kIx,	,
i	i	k9	i
kdyby	kdyby	kYmCp3nS	kdyby
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
papež	papež	k1gMnSc1	papež
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
věcech	věc	k1gFnPc6	věc
tvrdohlavě	tvrdohlavě	k6eAd1	tvrdohlavě
odmítá	odmítat	k5eAaImIp3nS	odmítat
podstoupit	podstoupit	k5eAaPmF	podstoupit
předpisy	předpis	k1gInPc4	předpis
<g/>
,	,	kIx,	,
ustanovení	ustanovení	k1gNnSc1	ustanovení
a	a	k8xC	a
nařízení	nařízení	k1gNnSc1	nařízení
<g/>
,	,	kIx,	,
této	tento	k3xDgFnSc2	tento
svaté	svatý	k2eAgFnSc2d1	svatá
synody	synoda	k1gFnSc2	synoda
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
legitimní	legitimní	k2eAgFnSc2d1	legitimní
souhrnné	souhrnný	k2eAgFnSc2d1	souhrnná
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
s	s	k7c7	s
výhradou	výhrada	k1gFnSc7	výhrada
příslušných	příslušný	k2eAgInPc2d1	příslušný
trest	trest	k1gInSc4	trest
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
potrestán	potrestat	k5eAaPmNgMnS	potrestat
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
případu	případ	k1gInSc2	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
použít	použít	k5eAaPmF	použít
jiné	jiný	k2eAgNnSc4d1	jiné
vhodné	vhodný	k2eAgNnSc4d1	vhodné
opatření	opatření	k1gNnSc4	opatření
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Znění	znění	k1gNnPc1	znění
aktů	akt	k1gInPc2	akt
5	[number]	k4	5
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc6	zasedání
Kostnické	kostnický	k2eAgFnSc2d1	Kostnická
rady	rada	k1gFnSc2	rada
dále	daleko	k6eAd2	daleko
plynule	plynule	k6eAd1	plynule
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
reštrikčnými	reštrikčná	k1gFnPc7	reštrikčná
opatření	opatření	k1gNnSc2	opatření
proti	proti	k7c3	proti
proti	proti	k7c3	proti
papežovi	papež	k1gMnSc3	papež
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výklad	výklad	k1gInSc1	výklad
==	==	k?	==
</s>
</p>
<p>
<s>
Koncil	koncil	k1gInSc4	koncil
je	být	k5eAaImIp3nS	být
nadřazen	nadřadit	k5eAaPmNgMnS	nadřadit
papeži	papež	k1gMnPc7	papež
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zastánce	zastánce	k1gMnSc4	zastánce
tohoto	tento	k3xDgInSc2	tento
názoru	názor	k1gInSc2	názor
představují	představovat	k5eAaImIp3nP	představovat
příslušné	příslušný	k2eAgFnPc1d1	příslušná
části	část	k1gFnPc1	část
dekretu	dekret	k1gInSc2	dekret
Haec	Haec	k1gInSc1	Haec
sancta	sanct	k1gInSc2	sanct
všeobecně	všeobecně	k6eAd1	všeobecně
platné	platný	k2eAgNnSc4d1	platné
dogma	dogma	k1gNnSc4	dogma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
tvrzení	tvrzení	k1gNnSc6	tvrzení
stojí	stát	k5eAaImIp3nP	stát
nápady	nápad	k1gInPc4	nápad
konciliarizmu	konciliarizmus	k1gInSc2	konciliarizmus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostnický	kostnický	k2eAgInSc1d1	kostnický
ekumenické	ekumenický	k2eAgFnSc2d1	ekumenická
rady	rada	k1gFnSc2	rada
byl	být	k5eAaImAgInS	být
lepší	dobrý	k2eAgMnSc1d2	lepší
než	než	k8xS	než
papež	papež	k1gMnSc1	papež
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
tohoto	tento	k3xDgInSc2	tento
názoru	názor	k1gInSc2	názor
uznat	uznat	k5eAaPmF	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
současně	současně	k6eAd1	současně
tři	tři	k4xCgMnPc1	tři
papežové	papež	k1gMnPc1	papež
(	(	kIx(	(
<g/>
Řehoř	Řehoř	k1gMnSc1	Řehoř
XII	XII	kA	XII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
Benedikt	Benedikt	k1gMnSc1	Benedikt
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
legitimní	legitimní	k2eAgMnSc1d1	legitimní
<g/>
,	,	kIx,	,
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak-zvané	takvan	k1gMnPc1	tak-zvan
západní	západní	k2eAgMnPc1d1	západní
v	v	k7c6	v
moci	moc	k1gFnSc6	moc
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
zpětně	zpětně	k6eAd1	zpětně
<g/>
)	)	kIx)	)
být	být	k5eAaImF	být
přijat	přijat	k2eAgMnSc1d1	přijat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ekumenický	ekumenický	k2eAgInSc1d1	ekumenický
koncil	koncil	k1gInSc1	koncil
Kostnický	kostnický	k2eAgInSc1d1	kostnický
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
speciálně	speciálně	k6eAd1	speciálně
svolané	svolaný	k2eAgNnSc4d1	svolané
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
vyřešena	vyřešit	k5eAaPmNgFnS	vyřešit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
lepší	dobrý	k2eAgFnSc1d2	lepší
než	než	k8xS	než
papežským	papežský	k2eAgFnPc3d1	Papežská
kanceláře	kancelář	k1gFnSc2	kancelář
jako	jako	k8xC	jako
takové	takový	k3xDgFnSc2	takový
<g/>
.	.	kIx.	.
</s>
<s>
Faktem	fakt	k1gInSc7	fakt
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
jediný	jediný	k2eAgMnSc1d1	jediný
legitimní	legitimní	k2eAgMnSc1d1	legitimní
papež	papež	k1gMnSc1	papež
v	v	k7c6	v
období	období	k1gNnSc6	období
západní	západní	k2eAgNnSc4d1	západní
schizma	schizma	k1gNnSc4	schizma
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
uznala	uznat	k5eAaPmAgFnS	uznat
<g/>
,	,	kIx,	,
Řehoř	Řehoř	k1gMnSc1	Řehoř
XII	XII	kA	XII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
dobrovolně	dobrovolně	k6eAd1	dobrovolně
vzdal	vzdát	k5eAaPmAgMnS	vzdát
svého	svůj	k3xOyFgInSc2	svůj
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgInPc1d1	zbývající
dva	dva	k4xCgInPc1	dva
"	"	kIx"	"
<g/>
papežů	papež	k1gMnPc2	papež
<g/>
"	"	kIx"	"
vatican	vatican	k1gMnSc1	vatican
council	council	k1gMnSc1	council
ii	ii	k?	ii
byl	být	k5eAaImAgMnS	být
nucený	nucený	k2eAgInSc4d1	nucený
režim	režim	k1gInSc4	režim
od	od	k7c2	od
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
později	pozdě	k6eAd2	pozdě
jejich	jejich	k3xOp3gFnPc4	jejich
zosadenie	zosadenie	k1gFnPc4	zosadenie
uznal	uznat	k5eAaPmAgMnS	uznat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dekret	dekret	k1gInSc1	dekret
Haec	Haec	k1gInSc1	Haec
sancta	sancta	k1gFnSc1	sancta
není	být	k5eNaImIp3nS	být
dogmou	dogma	k1gFnSc7	dogma
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
tohoto	tento	k3xDgInSc2	tento
názoru	názor	k1gInSc2	názor
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
formulace	formulace	k1gFnSc1	formulace
vyhlášky	vyhláška	k1gFnSc2	vyhláška
nemá	mít	k5eNaImIp3nS	mít
tradiční	tradiční	k2eAgInPc4d1	tradiční
znaky	znak	k1gInPc4	znak
nezpochybnitelného	zpochybnitelný	k2eNgNnSc2d1	nezpochybnitelné
<g/>
,	,	kIx,	,
věčně	věčně	k6eAd1	věčně
a	a	k8xC	a
univerzálně	univerzálně	k6eAd1	univerzálně
platné	platný	k2eAgNnSc1d1	platné
dogmatického	dogmatický	k2eAgNnSc2d1	dogmatické
prohlášení	prohlášení	k1gNnSc2	prohlášení
<g/>
.	.	kIx.	.
</s>
<s>
Prohlášení	prohlášení	k1gNnSc1	prohlášení
rady	rada	k1gFnSc2	rada
není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
dogma	dogma	k1gNnSc4	dogma
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
poslušnost	poslušnost	k1gFnSc4	poslušnost
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
této	tento	k3xDgFnSc2	tento
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
městě	město	k1gNnSc6	město
z	z	k7c2	z
kostí	kost	k1gFnPc2	kost
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
kostnica	kostnica	k1gFnSc1	kostnica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
funkce	funkce	k1gFnSc1	funkce
dogma	dogma	k1gNnSc4	dogma
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
jasné	jasný	k2eAgFnSc3d1	jasná
formulaci	formulace	k1gFnSc3	formulace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
tvorbě	tvorba	k1gFnSc3	tvorba
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
další	další	k2eAgFnSc2d1	další
pochybnosti	pochybnost	k1gFnSc2	pochybnost
<g/>
.	.	kIx.	.
</s>
<s>
Diskuse	diskuse	k1gFnSc1	diskuse
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
rozprúdil	rozprúdit	k5eAaPmAgMnS	rozprúdit
této	tento	k3xDgFnSc2	tento
vyhlášky	vyhláška	k1gFnSc2	vyhláška
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
naplněn	naplnit	k5eAaPmNgInS	naplnit
řadou	řada	k1gFnSc7	řada
nějaký	nějaký	k3yIgInSc4	nějaký
spor	spor	k1gInSc1	spor
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
zde	zde	k6eAd1	zde
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
dogma	dogma	k1gNnSc4	dogma
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
nakonec	nakonec	k6eAd1	nakonec
Kostnický	kostnický	k2eAgInSc4d1	kostnický
rady	rad	k1gInPc4	rad
ještě	ještě	k9	ještě
zvolen	zvolen	k2eAgMnSc1d1	zvolen
nový	nový	k2eAgMnSc1d1	nový
papež	papež	k1gMnSc1	papež
Martin	Martin	k1gMnSc1	Martin
V.	V.	kA	V.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nP	by
toto	tento	k3xDgNnSc4	tento
kontroverzní	kontroverzní	k2eAgNnSc4d1	kontroverzní
prohlášení	prohlášení	k1gNnSc4	prohlášení
později	pozdě	k6eAd2	pozdě
potvrdit	potvrdit	k5eAaPmF	potvrdit
nebo	nebo	k8xC	nebo
jinak	jinak	k6eAd1	jinak
objasnit	objasnit	k5eAaPmF	objasnit
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
neudělal	udělat	k5eNaPmAgInS	udělat
to	ten	k3xDgNnSc4	ten
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Papež	Papež	k1gMnSc1	Papež
je	být	k5eAaImIp3nS	být
nadřazený	nadřazený	k2eAgInSc4d1	nadřazený
rady	rad	k1gInPc4	rad
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
tohoto	tento	k3xDgInSc2	tento
názoru	názor	k1gInSc2	názor
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ipso	ipso	k6eAd1	ipso
facto	facto	k1gNnSc1	facto
žádné	žádný	k3yNgFnSc2	žádný
ekumenické	ekumenický	k2eAgFnSc2d1	ekumenická
rady	rada	k1gFnSc2	rada
nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgFnS	být
a	a	k8xC	a
ani	ani	k8xC	ani
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
lepší	dobrý	k2eAgMnSc1d2	lepší
než	než	k8xS	než
papež	papež	k1gMnSc1	papež
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dekret	dekret	k1gInSc4	dekret
Haec	Haec	k1gFnSc1	Haec
sancta	sancto	k1gNnSc2	sancto
schválila	schválit	k5eAaPmAgFnS	schválit
rada	rada	k1gFnSc1	rada
svolána	svolat	k5eAaPmNgFnS	svolat
i	i	k9	i
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
de	de	k?	de
iure	iure	k1gNnPc1	iure
nelegitimní	legitimní	k2eNgFnSc2d1	nelegitimní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Dokumenty	dokument	k1gInPc1	dokument
rady	rada	k1gFnSc2	rada
tedy	tedy	k9	tedy
také	také	k9	také
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
této	tento	k3xDgFnSc2	tento
argumentace	argumentace	k1gFnSc2	argumentace
je	být	k5eAaImIp3nS	být
také	také	k9	také
poukazem	poukaz	k1gInSc7	poukaz
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
církev	církev	k1gFnSc1	církev
konečně	konečně	k6eAd1	konečně
uznal	uznat	k5eAaPmAgInS	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediný	jediný	k2eAgMnSc1d1	jediný
legitimní	legitimní	k2eAgMnSc1d1	legitimní
papež	papež	k1gMnSc1	papež
byl	být	k5eAaImAgMnS	být
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
Řehoře	Řehoř	k1gMnSc2	Řehoř
XII	XII	kA	XII
<g/>
.	.	kIx.	.
</s>
<s>
Řehoř	Řehoř	k1gMnSc1	Řehoř
XII	XII	kA	XII
<g/>
.	.	kIx.	.
nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
dobrovolně	dobrovolně	k6eAd1	dobrovolně
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
papežského	papežský	k2eAgInSc2d1	papežský
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
dekret	dekret	k1gInSc4	dekret
Haec	Haec	k1gFnSc1	Haec
sancta	sancta	k1gFnSc1	sancta
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
případě	případ	k1gInSc6	případ
bych	by	kYmCp1nS	by
ani	ani	k9	ani
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgFnS	použít
<g/>
.	.	kIx.	.
</s>
<s>
Dokumenty	dokument	k1gInPc1	dokument
Kostnického	kostnický	k2eAgMnSc2d1	kostnický
rady	rada	k1gMnSc2	rada
byly	být	k5eAaImAgFnP	být
legitimní	legitimní	k2eAgFnSc4d1	legitimní
pápežmi	pápež	k1gFnPc7	pápež
zlegalizované	zlegalizovaný	k2eAgFnSc2d1	zlegalizovaná
až	až	k8xS	až
následně	následně	k6eAd1	následně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Haec	Haec	k1gFnSc1	Haec
sancta	sancta	k1gFnSc1	sancta
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Die	Die	k?	Die
Päpste	Päpst	k1gMnSc5	Päpst
–	–	k?	–
Herrscher	Herrschra	k1gFnPc2	Herrschra
über	über	k1gMnSc1	über
Himmel	Himmel	k1gMnSc1	Himmel
und	und	k?	und
Erde	Erde	k1gInSc1	Erde
<g/>
,	,	kIx,	,
Hans-Christian	Hans-Christian	k1gInSc1	Hans-Christian
Huf	huf	k0	huf
(	(	kIx(	(
<g/>
Hrsg	Hrsg	k1gInSc4	Hrsg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ullstein	Ullstein	k2eAgInSc1d1	Ullstein
Buchverlage	Buchverlage	k1gInSc1	Buchverlage
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-3-550-08693-9	[number]	k4	978-3-550-08693-9
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Kompletní	kompletní	k2eAgFnSc1d1	kompletní
aktech	akta	k1gNnPc6	akta
Kostnického	kostnický	k2eAgMnSc2d1	kostnický
rady	rada	k1gMnSc2	rada
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karl-Heinz	Karl-Heinz	k1gMnSc1	Karl-Heinz
Braun	Braun	k1gMnSc1	Braun
<g/>
:	:	kIx,	:
Die	Die	k1gMnSc1	Die
Konstanzer	Konstanzer	k1gMnSc1	Konstanzer
Dekrete	dekret	k1gInSc5	dekret
Haec	Haec	k1gFnSc1	Haec
sancta	sancta	k1gFnSc1	sancta
a	a	k8xC	a
Frequens	Frequens	k1gInSc1	Frequens
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dekret	dekret	k1gInSc1	dekret
Haec	Haec	k1gInSc1	Haec
sancta	sancta	k1gFnSc1	sancta
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dekret	dekret	k1gInSc1	dekret
Haec	Haec	k1gInSc1	Haec
sancta	sancta	k1gFnSc1	sancta
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kostnický	kostnický	k2eAgMnSc1d1	kostnický
rady	rada	k1gFnPc4	rada
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Konziliarismus	Konziliarismus	k1gInSc1	Konziliarismus
als	als	k?	als
Problem	Probl	k1gInSc7	Probl
der	drát	k5eAaImRp2nS	drát
neueren	neuerna	k1gFnPc2	neuerna
katholischen	katholischna	k1gFnPc2	katholischna
Theologie	theologie	k1gFnSc2	theologie
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
