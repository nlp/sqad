<s>
Živá	živý	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Živá	živý	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
(	(	kIx(
<g/>
s	s	k7c7
podtitulem	podtitul	k1gInSc7
Nesuď	soudit	k5eNaImRp2nS
knihu	kniha	k1gFnSc4
podle	podle	k7c2
obalu	obal	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
speciální	speciální	k2eAgInSc4d1
typ	typ	k1gInSc4
knihovny	knihovna	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
namísto	namísto	k7c2
knih	kniha	k1gFnPc2
půjčováni	půjčován	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
živé	živý	k2eAgFnPc1d1
knihy	kniha	k1gFnPc1
jsou	být	k5eAaImIp3nP
„	„	k?
<g/>
čteny	čten	k2eAgInPc1d1
<g/>
"	"	kIx"
pomocí	pomocí	k7c2
rozhovorů	rozhovor	k1gInPc2
a	a	k8xC
tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
odkrýván	odkrývat	k5eAaImNgInS
jejich	jejich	k3xOp3gInSc1
osud	osud	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osobnosti	osobnost	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
v	v	k7c6
živé	živý	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
nachází	nacházet	k5eAaImIp3nS
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
různé	různý	k2eAgInPc4d1
příběhy	příběh	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinou	většinou	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c6
zástupce	zástupka	k1gFnSc6
nejrůznějších	různý	k2eAgFnPc2d3
menšin	menšina	k1gFnPc2
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yIgInPc7,k3yRgInPc7,k3yQgInPc7
se	se	k3xPyFc4
pojí	pojit	k5eAaImIp3nS,k5eAaPmIp3nS
také	také	k9
množství	množství	k1gNnSc1
předsudků	předsudek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
například	například	k6eAd1
lidé	člověk	k1gMnPc1
s	s	k7c7
jinou	jiný	k2eAgFnSc7d1
etnickou	etnický	k2eAgFnSc7d1
příslušností	příslušnost	k1gFnSc7
<g/>
,	,	kIx,
sexuální	sexuální	k2eAgFnSc7d1
orientací	orientace	k1gFnSc7
<g/>
,	,	kIx,
náboženstvím	náboženství	k1gNnSc7
<g/>
,	,	kIx,
zdravotním	zdravotní	k2eAgInSc7d1
stavem	stav	k1gInSc7
<g/>
,	,	kIx,
životním	životní	k2eAgInSc7d1
stylem	styl	k1gInSc7
apod.	apod.	kA
</s>
<s>
Tento	tento	k3xDgInSc1
projekt	projekt	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
Dánsku	Dánsko	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
silně	silně	k6eAd1
jej	on	k3xPp3gInSc2
začaly	začít	k5eAaPmAgInP
šířit	šířit	k5eAaImF
členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
Rady	rada	k1gFnSc2
Evropy	Evropa	k1gFnSc2
a	a	k8xC
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
je	být	k5eAaImIp3nS
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
poprvé	poprvé	k6eAd1
uvedla	uvést	k5eAaPmAgFnS
Jana	Jana	k1gFnSc1
Tikalová	Tikalová	k1gFnSc1
<g/>
,	,	kIx,
ředitelka	ředitelka	k1gFnSc1
neziskové	ziskový	k2eNgFnSc2d1
organizace	organizace	k1gFnSc2
OPIM	OPIM	kA
<g/>
.	.	kIx.
</s>
<s>
Timto	Tento	k3xDp3gInSc7
konceptem	koncept	k1gInSc7
se	se	k3xPyFc4
inspirovaly	inspirovat	k5eAaBmAgFnP
nejrůznější	různý	k2eAgFnPc1d3
neziskové	ziskový	k2eNgFnPc1d1
organizace	organizace	k1gFnPc1
s	s	k7c7
cílem	cíl	k1gInSc7
odbourat	odbourat	k5eAaPmF
předsudky	předsudek	k1gInPc4
a	a	k8xC
stereotypy	stereotyp	k1gInPc4
spojené	spojený	k2eAgInPc4d1
s	s	k7c7
danými	daný	k2eAgFnPc7d1
menšinami	menšina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Živé	živý	k2eAgFnPc4d1
knihovny	knihovna	k1gFnPc4
pořádá	pořádat	k5eAaImIp3nS
nadále	nadále	k6eAd1
OPIM	OPIM	kA
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
ve	v	k7c6
školách	škola	k1gFnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
například	například	k6eAd1
i	i	k9
lidskoprávní	lidskoprávní	k2eAgFnPc4d1
organizace	organizace	k1gFnPc4
Amnesty	Amnest	k1gInPc1
International	International	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
svého	svůj	k3xOyFgInSc2
vzdělávacího	vzdělávací	k2eAgInSc2d1
projektu	projekt	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poslání	poslání	k1gNnSc1
Živé	živý	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
</s>
<s>
Živá	živý	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
má	mít	k5eAaImIp3nS
odbourat	odbourat	k5eAaPmF
předsudky	předsudek	k1gInPc4
a	a	k8xC
stereotypy	stereotyp	k1gInPc4
vůči	vůči	k7c3
menšinám	menšina	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návštěvníci	návštěvník	k1gMnPc1
knihovny	knihovna	k1gFnSc2
získají	získat	k5eAaPmIp3nP
osobní	osobní	k2eAgFnSc4d1
zkušenost	zkušenost	k1gFnSc4
se	s	k7c7
zástupci	zástupce	k1gMnPc7
menšin	menšina	k1gFnPc2
<g/>
,	,	kIx,
seznámí	seznámit	k5eAaPmIp3nP
se	se	k3xPyFc4
s	s	k7c7
jejich	jejich	k3xOp3gInSc7
příběhem	příběh	k1gInSc7
a	a	k8xC
s	s	k7c7
problémy	problém	k1gInPc7
a	a	k8xC
bariérami	bariéra	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
musí	muset	k5eAaImIp3nP
tito	tento	k3xDgMnPc1
lidé	člověk	k1gMnPc1
denně	denně	k6eAd1
překonávat	překonávat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
Živé	živý	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1
Živé	živý	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
přišla	přijít	k5eAaPmAgFnS
z	z	k7c2
Dánska	Dánsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevládní	vládní	k2eNgInSc4d1
hnutí	hnutí	k1gNnSc1
mládeže	mládež	k1gFnSc2
Stop	stop	k1gInSc1
the	the	k?
Violence	Violence	k1gFnSc2
zorganizovalo	zorganizovat	k5eAaPmAgNnS
první	první	k4xOgFnSc4
Živou	živý	k2eAgFnSc4d1
knihovnu	knihovna	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
na	na	k7c6
festivalu	festival	k1gInSc6
Roskilde	Roskild	k1gInSc5
Music	Music	k1gMnSc1
Festival	festival	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Živými	živý	k2eAgFnPc7d1
knihami	kniha	k1gFnPc7
i	i	k8xC
čtenáři	čtenář	k1gMnPc1
byli	být	k5eAaImAgMnP
návštěvníci	návštěvník	k1gMnPc1
festivalu	festival	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
premiéra	premiéra	k1gFnSc1
Živé	živý	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
v	v	k7c6
červnu	červen	k1gInSc6
2007	#num#	k4
na	na	k7c6
festivalu	festival	k1gInSc6
United	United	k1gInSc1
Island	Island	k1gInSc1
of	of	k?
Prague	Pragu	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
také	také	k9
na	na	k7c6
letním	letní	k2eAgInSc6d1
festivalu	festival	k1gInSc6
Music	Music	k1gMnSc1
in	in	k?
the	the	k?
park	park	k1gInSc1
ve	v	k7c6
Stromovce	Stromovka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Knihovnu	knihovna	k1gFnSc4
zde	zde	k6eAd1
pořádala	pořádat	k5eAaImAgFnS
Organizace	organizace	k1gFnSc1
na	na	k7c4
podporu	podpora	k1gFnSc4
a	a	k8xC
integraci	integrace	k1gFnSc4
menšin	menšina	k1gFnPc2
(	(	kIx(
<g/>
OPIM	OPIM	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Živou	živý	k2eAgFnSc4d1
knihovnu	knihovna	k1gFnSc4
pak	pak	k6eAd1
následně	následně	k6eAd1
realizovaly	realizovat	k5eAaBmAgFnP
také	také	k9
další	další	k2eAgFnPc1d1
neziskové	ziskový	k2eNgFnPc1d1
organizace	organizace	k1gFnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
Nesehnutí	nesehnutí	k1gNnSc1
nebo	nebo	k8xC
LOS	los	k1gInSc1
-	-	kIx~
Liberecká	liberecký	k2eAgFnSc1d1
občanská	občanský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
pořádá	pořádat	k5eAaImIp3nS
pravidelně	pravidelně	k6eAd1
Živé	živý	k2eAgFnPc4d1
knihovny	knihovna	k1gFnPc4
krom	krom	k7c2
výše	vysoce	k6eAd2
uvedených	uvedený	k2eAgNnPc2d1
i	i	k9
organizace	organizace	k1gFnPc4
Amnesty	Amnest	k1gInPc4
International	International	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
rámci	rámec	k1gInSc6
svého	svůj	k3xOyFgInSc2
vzdělávacího	vzdělávací	k2eAgInSc2d1
projektu	projekt	k1gInSc2
FAIR	fair	k6eAd1
PLAY	play	k0
–	–	k?
Studenti	student	k1gMnPc1
za	za	k7c4
rovnoprávnost	rovnoprávnost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Projekt	projekt	k1gInSc1
je	být	k5eAaImIp3nS
určený	určený	k2eAgInSc1d1
žákům	žák	k1gMnPc3
a	a	k8xC
studentům	student	k1gMnPc3
základních	základní	k2eAgFnPc2d1
a	a	k8xC
středních	střední	k2eAgFnPc2d1
škol	škola	k1gFnPc2
a	a	k8xC
nízkoprahovým	nízkoprahový	k2eAgInPc3d1
klubům	klub	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Živé	živý	k2eAgFnPc4d1
knihovny	knihovna	k1gFnPc4
Amnesty	Amnest	k1gInPc1
International	International	k1gFnSc2
probíhají	probíhat	k5eAaImIp3nP
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
Ostravě	Ostrava	k1gFnSc6
a	a	k8xC
také	také	k9
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
2014	#num#	k4
Amnesty	Amnest	k1gInPc7
International	International	k1gFnSc4
organizovala	organizovat	k5eAaBmAgFnS
také	také	k9
veřejnou	veřejný	k2eAgFnSc4d1
Živou	živý	k2eAgFnSc4d1
knihovnu	knihovna	k1gFnSc4
na	na	k7c6
romském	romský	k2eAgInSc6d1
festivalu	festival	k1gInSc6
Ghettofest	Ghettofest	k1gInSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
LOS	los	k1gInSc1
–	–	k?
Liberecká	liberecký	k2eAgFnSc1d1
občanská	občanský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejčastější	častý	k2eAgMnPc4d3
realizátory	realizátor	k1gMnPc4
této	tento	k3xDgFnSc2
metody	metoda	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
dílčích	dílčí	k2eAgInPc2d1
projektů	projekt	k1gInPc2
i	i	k9
mimo	mimo	k7c4
ně	on	k3xPp3gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
již	již	k6eAd1
zapojila	zapojit	k5eAaPmAgFnS
více	hodně	k6eAd2
než	než	k8xS
200	#num#	k4
knih	kniha	k1gFnPc2
a	a	k8xC
oslovila	oslovit	k5eAaPmAgFnS
tak	tak	k9
přes	přes	k7c4
2.000	2.000	k4
čtenářů	čtenář	k1gMnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgInPc7
byli	být	k5eAaImAgMnP
především	především	k9
žáci	žák	k1gMnPc1
základních	základní	k2eAgInPc2d1
a	a	k8xC
středních	střední	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Živé	živý	k2eAgFnPc1d1
knihy	kniha	k1gFnPc1
</s>
<s>
Mezi	mezi	k7c4
osobnosti	osobnost	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
představují	představovat	k5eAaImIp3nP
živé	živý	k2eAgFnPc1d1
knihy	kniha	k1gFnPc1
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nP
například	například	k6eAd1
cizinci	cizinec	k1gMnPc1
<g/>
,	,	kIx,
uprchlíci	uprchlík	k1gMnPc1
<g/>
,	,	kIx,
muslimové	muslim	k1gMnPc1
<g/>
,	,	kIx,
Romové	Rom	k1gMnPc1
<g/>
,	,	kIx,
vozíčkáři	vozíčkář	k1gMnPc1
<g/>
,	,	kIx,
nevidomí	nevidomý	k1gMnPc1
<g/>
,	,	kIx,
homosexuálové	homosexuál	k1gMnPc1
<g/>
,	,	kIx,
vegani	vegan	k1gMnPc1
<g/>
,	,	kIx,
aktivisté	aktivista	k1gMnPc1
za	za	k7c2
práva	právo	k1gNnSc2
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
bezdomovci	bezdomovec	k1gMnPc1
apod.	apod.	kA
</s>
<s>
Knihy	kniha	k1gFnPc1
<g/>
,	,	kIx,
čtenáři	čtenář	k1gMnPc1
a	a	k8xC
knihovníci	knihovník	k1gMnPc1
</s>
<s>
V	v	k7c6
knihovně	knihovna	k1gFnSc6
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
se	se	k3xPyFc4
setkat	setkat	k5eAaPmF
se	s	k7c7
třemi	tři	k4xCgFnPc7
různými	různý	k2eAgFnPc7d1
funkcemi	funkce	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
KNIHY	kniha	k1gFnPc1
<g/>
"	"	kIx"
jsou	být	k5eAaImIp3nP
originální	originální	k2eAgFnPc1d1
a	a	k8xC
jedinečné	jedinečný	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
prezentují	prezentovat	k5eAaBmIp3nP
svůj	svůj	k3xOyFgInSc4
životní	životní	k2eAgInSc4d1
osud	osud	k1gInSc4
a	a	k8xC
zkušenosti	zkušenost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
kniha	kniha	k1gFnSc1
vypráví	vyprávět	k5eAaImIp3nS
čtenářům	čtenář	k1gMnPc3
svůj	svůj	k3xOyFgInSc4
příběh	příběh	k1gInSc1
a	a	k8xC
následně	následně	k6eAd1
probíhá	probíhat	k5eAaImIp3nS
diskuze	diskuze	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
ČTENÁŘI	čtenář	k1gMnPc7
<g/>
"	"	kIx"
jsou	být	k5eAaImIp3nP
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
naslouchají	naslouchat	k5eAaImIp3nP
"	"	kIx"
<g/>
knihám	kniha	k1gFnPc3
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
osobním	osobní	k2eAgInPc3d1
rozhovorům	rozhovor	k1gInPc3
se	se	k3xPyFc4
dozví	dozvědět	k5eAaPmIp3nS
informace	informace	k1gFnSc1
o	o	k7c6
životě	život	k1gInSc6
menšin	menšina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohou	moct	k5eAaImIp3nP
se	se	k3xPyFc4
knih	kniha	k1gFnPc2
zeptat	zeptat	k5eAaPmF
na	na	k7c4
cokoliv	cokoliv	k3yInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
vždy	vždy	k6eAd1
s	s	k7c7
respektem	respekt	k1gInSc7
a	a	k8xC
slušností	slušnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
KNIHOVNÍCI	knihovník	k1gMnPc1
<g/>
"	"	kIx"
se	se	k3xPyFc4
starají	starat	k5eAaImIp3nP
o	o	k7c4
knihy	kniha	k1gFnPc4
a	a	k8xC
čtenáře	čtenář	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
vše	všechen	k3xTgNnSc4
fungovalo	fungovat	k5eAaImAgNnS
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
nejlépe	dobře	k6eAd3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asistují	asistovat	k5eAaImIp3nP
<g/>
,	,	kIx,
evidují	evidovat	k5eAaImIp3nP
čtenáře	čtenář	k1gMnPc4
a	a	k8xC
pomáhají	pomáhat	k5eAaImIp3nP
jim	on	k3xPp3gMnPc3
s	s	k7c7
výběrem	výběr	k1gInSc7
knih	kniha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
funkci	funkce	k1gFnSc4
zastávají	zastávat	k5eAaImIp3nP
pracovníci	pracovník	k1gMnPc1
a	a	k8xC
dobrovolníci	dobrovolník	k1gMnPc1
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
danou	daný	k2eAgFnSc4d1
knihovnu	knihovna	k1gFnSc4
pořádají	pořádat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Jak	jak	k6eAd1
probíhá	probíhat	k5eAaImIp3nS
Živá	živý	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
</s>
<s>
Čtenář	čtenář	k1gMnSc1
Živé	živý	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
registrovat	registrovat	k5eAaBmF
a	a	k8xC
seznámit	seznámit	k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
výpůjčními	výpůjční	k2eAgNnPc7d1
pravidly	pravidlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
si	se	k3xPyFc3
vybere	vybrat	k5eAaPmIp3nS
z	z	k7c2
katalogu	katalog	k1gInSc2
knih	kniha	k1gFnPc2
tu	tu	k6eAd1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
ho	on	k3xPp3gMnSc4
zajímá	zajímat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
katalogu	katalog	k1gInSc6
jsou	být	k5eAaImIp3nP
uvedené	uvedený	k2eAgFnPc1d1
krátké	krátká	k1gFnPc1
knižní	knižní	k2eAgFnSc2d1
anotace	anotace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
si	se	k3xPyFc3
po	po	k7c4
stanovenou	stanovený	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
může	moct	k5eAaImIp3nS
s	s	k7c7
knihou	kniha	k1gFnSc7
popovídat	popovídat	k5eAaPmF
a	a	k8xC
vyslechnout	vyslechnout	k5eAaPmF
si	se	k3xPyFc3
její	její	k3xOp3gInSc4
příběh	příběh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtení	čtení	k1gNnPc4
může	moct	k5eAaImIp3nS
probíhat	probíhat	k5eAaImF
jednotlivě	jednotlivě	k6eAd1
i	i	k9
v	v	k7c6
malých	malý	k2eAgFnPc6d1
skupinách	skupina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.amnesty.cz	www.amnesty.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.amnesty.cz	www.amnesty.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.ghettofest.cz	www.ghettofest.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
FERDAN	FERDAN	kA
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Živá	živý	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
-	-	kIx~
manuál	manuál	k1gInSc1
pro	pro	k7c4
pedagogy	pedagog	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liberec	Liberec	k1gInSc1
<g/>
:	:	kIx,
LOS	los	k1gInSc1
-	-	kIx~
Liberecká	liberecký	k2eAgFnSc1d1
občanská	občanský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
z.	z.	k?
s.	s.	k?
<g/>
,	,	kIx,
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
