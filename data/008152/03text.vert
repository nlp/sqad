<p>
<s>
Letadlo	letadlo	k1gNnSc1	letadlo
je	být	k5eAaImIp3nS	být
létající	létající	k2eAgInSc4d1	létající
dopravní	dopravní	k2eAgInSc4d1	dopravní
prostředek	prostředek	k1gInSc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
pojmu	pojem	k1gInSc3	pojem
lze	lze	k6eAd1	lze
přisuzovat	přisuzovat	k5eAaImF	přisuzovat
různě	různě	k6eAd1	různě
široké	široký	k2eAgInPc4d1	široký
významy	význam	k1gInPc4	význam
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Podle	podle	k7c2	podle
normy	norma	k1gFnSc2	norma
ČSN	ČSN	kA	ČSN
31 000	[number]	k4	31 000
je	být	k5eAaImIp3nS	být
letadlo	letadlo	k1gNnSc4	letadlo
"	"	kIx"	"
<g/>
zařízení	zařízení	k1gNnSc1	zařízení
schopné	schopný	k2eAgNnSc1d1	schopné
vyvozovat	vyvozovat	k5eAaImF	vyvozovat
síly	síla	k1gFnPc4	síla
nesoucí	nesoucí	k2eAgFnPc4d1	nesoucí
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
z	z	k7c2	z
reakcí	reakce	k1gFnPc2	reakce
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
nejsou	být	k5eNaImIp3nP	být
reakcemi	reakce	k1gFnPc7	reakce
vůči	vůči	k7c3	vůči
zemskému	zemský	k2eAgInSc3d1	zemský
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
O	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
významu	význam	k1gInSc6	význam
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
tento	tento	k3xDgInSc4	tento
článek	článek	k1gInSc4	článek
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
řeči	řeč	k1gFnSc6	řeč
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc1	slovo
letadlo	letadlo	k1gNnSc1	letadlo
nepřesně	přesně	k6eNd1	přesně
chápe	chápat	k5eAaImIp3nS	chápat
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
významu	význam	k1gInSc6	význam
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
letadlo	letadlo	k1gNnSc1	letadlo
těžší	těžký	k2eAgMnSc1d2	těžší
vzduchu	vzduch	k1gInSc2	vzduch
s	s	k7c7	s
pevným	pevný	k2eAgNnSc7d1	pevné
křídlem	křídlo	k1gNnSc7	křídlo
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
letoun	letoun	k1gInSc1	letoun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
nejširším	široký	k2eAgInSc6d3	nejširší
významu	význam	k1gInSc6	význam
lze	lze	k6eAd1	lze
jako	jako	k8xS	jako
letadlo	letadlo	k1gNnSc4	letadlo
označit	označit	k5eAaPmF	označit
i	i	k9	i
libovolný	libovolný	k2eAgInSc4d1	libovolný
jiný	jiný	k2eAgInSc4d1	jiný
létající	létající	k2eAgInSc4d1	létající
stroj	stroj	k1gInSc4	stroj
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mimo	mimo	k7c4	mimo
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
umělé	umělý	k2eAgNnSc4d1	umělé
kosmické	kosmický	k2eAgNnSc4d1	kosmické
těleso	těleso	k1gNnSc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
např.	např.	kA	např.
raketoplán	raketoplán	k1gInSc4	raketoplán
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
chceme	chtít	k5eAaImIp1nP	chtít
"	"	kIx"	"
<g/>
běžné	běžný	k2eAgNnSc1d1	běžné
letadlo	letadlo	k1gNnSc1	letadlo
<g/>
"	"	kIx"	"
rozlišit	rozlišit	k5eAaPmF	rozlišit
od	od	k7c2	od
kosmických	kosmický	k2eAgNnPc2d1	kosmické
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
atmosférické	atmosférický	k2eAgNnSc1d1	atmosférické
letadlo	letadlo	k1gNnSc1	letadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
letadel	letadlo	k1gNnPc2	letadlo
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
vzniku	vznik	k1gInSc2	vznik
vztlaku	vztlak	k1gInSc2	vztlak
===	===	k?	===
</s>
</p>
<p>
<s>
Lehčí	lehký	k2eAgFnSc1d2	lehčí
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
(	(	kIx(	(
<g/>
aerostaty	aerostat	k1gInPc1	aerostat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bezmotorové	bezmotorový	k2eAgNnSc1d1	bezmotorové
</s>
</p>
<p>
<s>
Balón	balón	k1gInSc1	balón
</s>
</p>
<p>
<s>
Motorové	motorový	k2eAgInPc1d1	motorový
</s>
</p>
<p>
<s>
Vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
</s>
</p>
<p>
<s>
Těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
(	(	kIx(	(
<g/>
aerodyny	aerodyn	k1gInPc1	aerodyn
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bezmotorové	bezmotorový	k2eAgNnSc1d1	bezmotorové
</s>
</p>
<p>
<s>
S	s	k7c7	s
nepohyblivými	pohyblivý	k2eNgFnPc7d1	nepohyblivá
nosnými	nosný	k2eAgFnPc7d1	nosná
plochami	plocha	k1gFnPc7	plocha
</s>
</p>
<p>
<s>
Kluzák	kluzák	k1gInSc1	kluzák
</s>
</p>
<p>
<s>
Padákový	padákový	k2eAgInSc1d1	padákový
kluzák	kluzák	k1gInSc1	kluzák
</s>
</p>
<p>
<s>
Rogalo	rogalo	k1gNnSc1	rogalo
</s>
</p>
<p>
<s>
Padák	padák	k1gInSc1	padák
</s>
</p>
<p>
<s>
Drak	drak	k1gMnSc1	drak
</s>
</p>
<p>
<s>
S	s	k7c7	s
rotujícími	rotující	k2eAgFnPc7d1	rotující
nosnými	nosný	k2eAgFnPc7d1	nosná
plochami	plocha	k1gFnPc7	plocha
</s>
</p>
<p>
<s>
Rotorový	rotorový	k2eAgInSc1d1	rotorový
kluzák	kluzák	k1gInSc1	kluzák
</s>
</p>
<p>
<s>
Motorové	motorový	k2eAgInPc1d1	motorový
</s>
</p>
<p>
<s>
Bez	bez	k7c2	bez
nosných	nosný	k2eAgFnPc2d1	nosná
ploch	plocha	k1gFnPc2	plocha
</s>
</p>
<p>
<s>
Raketa	raketa	k1gFnSc1	raketa
</s>
</p>
<p>
<s>
S	s	k7c7	s
nepohyblivými	pohyblivý	k2eNgFnPc7d1	nepohyblivá
nosnými	nosný	k2eAgFnPc7d1	nosná
plochami	plocha	k1gFnPc7	plocha
</s>
</p>
<p>
<s>
Letoun	letoun	k1gMnSc1	letoun
</s>
</p>
<p>
<s>
Padákový	padákový	k2eAgInSc1d1	padákový
kluzák	kluzák	k1gInSc1	kluzák
s	s	k7c7	s
paramotorem	paramotor	k1gInSc7	paramotor
</s>
</p>
<p>
<s>
Motorové	motorový	k2eAgNnSc1d1	motorové
rogalo	rogalo	k1gNnSc1	rogalo
</s>
</p>
<p>
<s>
Motorový	motorový	k2eAgInSc1d1	motorový
kluzák	kluzák	k1gInSc1	kluzák
</s>
</p>
<p>
<s>
S	s	k7c7	s
rotujícími	rotující	k2eAgFnPc7d1	rotující
nosnými	nosný	k2eAgFnPc7d1	nosná
plochami	plocha	k1gFnPc7	plocha
</s>
</p>
<p>
<s>
Vrtulník	vrtulník	k1gInSc1	vrtulník
</s>
</p>
<p>
<s>
Vírník	vírník	k1gMnSc1	vírník
</s>
</p>
<p>
<s>
Cyklokoptéra	Cyklokoptér	k1gMnSc4	Cyklokoptér
</s>
</p>
<p>
<s>
S	s	k7c7	s
kombinovanými	kombinovaný	k2eAgFnPc7d1	kombinovaná
nosnými	nosný	k2eAgFnPc7d1	nosná
plochami	plocha	k1gFnPc7	plocha
</s>
</p>
<p>
<s>
Konvertoplán	Konvertoplán	k2eAgInSc1d1	Konvertoplán
</s>
</p>
<p>
<s>
S	s	k7c7	s
mávajícími	mávající	k2eAgFnPc7d1	mávající
nosnými	nosný	k2eAgFnPc7d1	nosná
plochami	plocha	k1gFnPc7	plocha
</s>
</p>
<p>
<s>
Ornitoptéra	ornitoptéra	k1gFnSc1	ornitoptéra
</s>
</p>
<p>
<s>
S	s	k7c7	s
prstencovými	prstencový	k2eAgFnPc7d1	prstencová
nosnými	nosný	k2eAgFnPc7d1	nosná
plochami	plocha	k1gFnPc7	plocha
</s>
</p>
<p>
<s>
Koleoptéra	Koleoptér	k1gMnSc4	Koleoptér
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
účelu	účel	k1gInSc2	účel
===	===	k?	===
</s>
</p>
<p>
<s>
Civilní	civilní	k2eAgNnSc1d1	civilní
letadlo	letadlo	k1gNnSc1	letadlo
</s>
</p>
<p>
<s>
Dopravní	dopravní	k2eAgNnSc1d1	dopravní
letadlo	letadlo	k1gNnSc1	letadlo
</s>
</p>
<p>
<s>
Osobní	osobní	k2eAgInSc1d1	osobní
</s>
</p>
<p>
<s>
Nákladní	nákladní	k2eAgFnSc1d1	nákladní
</s>
</p>
<p>
<s>
Smíšené	smíšený	k2eAgNnSc1d1	smíšené
</s>
</p>
<p>
<s>
Sportovní	sportovní	k2eAgNnSc1d1	sportovní
letadlo	letadlo	k1gNnSc1	letadlo
</s>
</p>
<p>
<s>
Letadlo	letadlo	k1gNnSc1	letadlo
pro	pro	k7c4	pro
letecké	letecký	k2eAgFnPc4d1	letecká
práce	práce	k1gFnPc4	práce
</s>
</p>
<p>
<s>
Model	model	k1gInSc1	model
letadla	letadlo	k1gNnSc2	letadlo
</s>
</p>
<p>
<s>
Vojenské	vojenský	k2eAgNnSc1d1	vojenské
letadlo	letadlo	k1gNnSc1	letadlo
</s>
</p>
<p>
<s>
Pozorovací	pozorovací	k2eAgNnSc1d1	pozorovací
letadlo	letadlo	k1gNnSc1	letadlo
</s>
</p>
<p>
<s>
Stíhací	stíhací	k2eAgNnSc1d1	stíhací
letadlo	letadlo	k1gNnSc1	letadlo
</s>
</p>
<p>
<s>
Bombardovací	bombardovací	k2eAgNnSc1d1	bombardovací
letadlo	letadlo	k1gNnSc1	letadlo
</s>
</p>
<p>
<s>
Bitevní	bitevní	k2eAgNnSc1d1	bitevní
letadlo	letadlo	k1gNnSc1	letadlo
</s>
</p>
<p>
<s>
Dopravní	dopravní	k2eAgNnSc1d1	dopravní
letadlo	letadlo	k1gNnSc1	letadlo
</s>
</p>
<p>
<s>
Cvičné	cvičný	k2eAgNnSc1d1	cvičné
letadlo	letadlo	k1gNnSc1	letadlo
</s>
</p>
<p>
<s>
Víceúčelové	víceúčelový	k2eAgNnSc1d1	víceúčelové
letadlo	letadlo	k1gNnSc1	letadlo
</s>
</p>
<p>
<s>
Speciální	speciální	k2eAgNnSc1d1	speciální
letadlo	letadlo	k1gNnSc1	letadlo
</s>
</p>
<p>
<s>
Spojovací	spojovací	k2eAgNnSc1d1	spojovací
letadlo	letadlo	k1gNnSc1	letadlo
</s>
</p>
<p>
<s>
Sanitní	sanitní	k2eAgNnSc1d1	sanitní
letadlo	letadlo	k1gNnSc1	letadlo
</s>
</p>
<p>
<s>
Výsadkové	výsadkový	k2eAgNnSc1d1	výsadkové
letadlo	letadlo	k1gNnSc1	letadlo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgNnSc1d1	další
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
klasifikačních	klasifikační	k2eAgFnPc2d1	klasifikační
tříd	třída	k1gFnPc2	třída
</s>
</p>
<p>
<s>
Třídy	třída	k1gFnPc1	třída
použití	použití	k1gNnSc2	použití
</s>
</p>
<p>
<s>
Třídy	třída	k1gFnPc1	třída
řiditelnosti	řiditelnost	k1gFnSc2	řiditelnost
a	a	k8xC	a
namáhání	namáhání	k1gNnSc2	namáhání
</s>
</p>
<p>
<s>
Třídy	třída	k1gFnPc1	třída
provozní	provozní	k2eAgFnSc2d1	provozní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
konstrukčních	konstrukční	k2eAgInPc2d1	konstrukční
znaků	znak	k1gInPc2	znak
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
polohy	poloha	k1gFnSc2	poloha
nosných	nosný	k2eAgFnPc2d1	nosná
ploch	plocha	k1gFnPc2	plocha
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
nosných	nosný	k2eAgFnPc2d1	nosná
ploch	plocha	k1gFnPc2	plocha
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
nosných	nosný	k2eAgFnPc2d1	nosná
ploch	plocha	k1gFnPc2	plocha
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
uspořádání	uspořádání	k1gNnSc2	uspořádání
trupu	trup	k1gInSc2	trup
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
pohonu	pohon	k1gInSc2	pohon
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
motorů	motor	k1gInPc2	motor
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
podvozku	podvozek	k1gInSc2	podvozek
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
účelových	účelový	k2eAgInPc2d1	účelový
znaků	znak	k1gInPc2	znak
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
délky	délka	k1gFnSc2	délka
vzletu	vzlet	k1gInSc2	vzlet
a	a	k8xC	a
přistání	přistání	k1gNnSc4	přistání
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
doletu	dolet	k1gInSc2	dolet
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
provozních	provozní	k2eAgFnPc2d1	provozní
rychlostí	rychlost	k1gFnPc2	rychlost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Konstrukce	konstrukce	k1gFnPc4	konstrukce
letadel	letadlo	k1gNnPc2	letadlo
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Letadlo	letadlo	k1gNnSc1	letadlo
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
následujících	následující	k2eAgInPc2d1	následující
celků	celek	k1gInPc2	celek
(	(	kIx(	(
<g/>
pozn.	pozn.	kA	pozn.
Letadlo	letadlo	k1gNnSc1	letadlo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
složeno	složit	k5eAaPmNgNnS	složit
jen	jen	k9	jen
z	z	k7c2	z
některých	některý	k3yIgInPc2	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
je	on	k3xPp3gInPc4	on
najdeme	najít	k5eAaPmIp1nP	najít
například	například	k6eAd1	například
u	u	k7c2	u
</s>
</p>
<p>
<s>
soudobého	soudobý	k2eAgInSc2d1	soudobý
stíhacího	stíhací	k2eAgInSc2d1	stíhací
letounu	letoun	k1gInSc2	letoun
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
drak	drak	k1gMnSc1	drak
letadla	letadlo	k1gNnSc2	letadlo
–	–	k?	–
Vlastní	vlastní	k2eAgFnPc1d1	vlastní
"	"	kIx"	"
<g/>
konstrukce	konstrukce	k1gFnPc1	konstrukce
<g/>
"	"	kIx"	"
letadla	letadlo	k1gNnPc4	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
trup	trup	k1gInSc1	trup
letadla	letadlo	k1gNnSc2	letadlo
</s>
</p>
<p>
<s>
nosná	nosný	k2eAgFnSc1d1	nosná
soustava	soustava	k1gFnSc1	soustava
</s>
</p>
<p>
<s>
ocasní	ocasní	k2eAgFnPc1d1	ocasní
plochy	plocha	k1gFnPc1	plocha
</s>
</p>
<p>
<s>
řízení	řízení	k1gNnSc1	řízení
</s>
</p>
<p>
<s>
a	a	k8xC	a
přistávací	přistávací	k2eAgNnSc1d1	přistávací
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
pohonná	pohonný	k2eAgFnSc1d1	pohonná
jednotka	jednotka	k1gFnSc1	jednotka
–	–	k?	–
Motor	motor	k1gInSc1	motor
s	s	k7c7	s
agregáty	agregát	k1gInPc7	agregát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zabezpečují	zabezpečovat	k5eAaImIp3nP	zabezpečovat
jeho	jeho	k3xOp3gFnSc4	jeho
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
vrtule	vrtule	k1gFnSc1	vrtule
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
letadlové	letadlový	k2eAgFnPc1d1	letadlová
soustavy	soustava	k1gFnPc1	soustava
</s>
</p>
<p>
<s>
silové	silový	k2eAgFnSc2d1	silová
soustavy	soustava	k1gFnSc2	soustava
–	–	k?	–
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
ovládání	ovládání	k1gNnSc3	ovládání
některých	některý	k3yIgFnPc2	některý
částí	část	k1gFnPc2	část
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
například	například	k6eAd1	například
hydraulická	hydraulický	k2eAgFnSc1d1	hydraulická
a	a	k8xC	a
pneumatická	pneumatický	k2eAgFnSc1d1	pneumatická
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
soustavy	soustava	k1gFnPc1	soustava
vybavení	vybavení	k1gNnSc2	vybavení
letadla	letadlo	k1gNnSc2	letadlo
–	–	k?	–
Např.	např.	kA	např.
palivová	palivový	k2eAgFnSc1d1	palivová
<g/>
,	,	kIx,	,
klimatizační	klimatizační	k2eAgFnSc1d1	klimatizační
nebo	nebo	k8xC	nebo
protipožární	protipožární	k2eAgFnSc1d1	protipožární
soustava	soustava	k1gFnSc1	soustava
</s>
</p>
<p>
<s>
vybavení	vybavení	k1gNnSc1	vybavení
</s>
</p>
<p>
<s>
elektrické	elektrický	k2eAgInPc1d1	elektrický
–	–	k?	–
Zdroje	zdroj	k1gInPc1	zdroj
<g/>
,	,	kIx,	,
sítě	síť	k1gFnPc1	síť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
radiové	radiový	k2eAgFnPc4d1	radiová
–	–	k?	–
Radiostanice	radiostanice	k1gFnPc4	radiostanice
<g/>
,	,	kIx,	,
radionavigační	radionavigační	k2eAgNnSc4d1	radionavigační
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
speciální	speciální	k2eAgInPc1d1	speciální
–	–	k?	–
Přístroje	přístroj	k1gInPc1	přístroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
výzbroj	výzbroj	k1gFnSc1	výzbroj
</s>
</p>
<p>
<s>
letecká	letecký	k2eAgFnSc1d1	letecká
výzbroj	výzbroj	k1gFnSc1	výzbroj
</s>
</p>
<p>
<s>
vystřelovací	vystřelovací	k2eAgFnSc1d1	vystřelovací
sedačka	sedačka	k1gFnSc1	sedačka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Ing.	ing.	kA	ing.
Ladislav	Ladislav	k1gMnSc1	Ladislav
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
Učebnice	učebnice	k1gFnSc1	učebnice
pilota	pilota	k1gFnSc1	pilota
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
Severografie	Severografie	k1gFnSc1	Severografie
Most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-85280-30-2	[number]	k4	80-85280-30-2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Letoun	letoun	k1gMnSc1	letoun
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
letectví	letectví	k1gNnSc2	letectví
</s>
</p>
<p>
<s>
Strava	strava	k1gFnSc1	strava
v	v	k7c6	v
letadle	letadlo	k1gNnSc6	letadlo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
letadlo	letadlo	k1gNnSc4	letadlo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
letadlo	letadlo	k1gNnSc4	letadlo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Fotogalerie	Fotogalerie	k1gFnSc1	Fotogalerie
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Letecké	letecký	k2eAgNnSc1d1	letecké
fórum	fórum	k1gNnSc1	fórum
</s>
</p>
