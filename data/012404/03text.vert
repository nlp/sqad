<p>
<s>
Tetřívek	tetřívek	k1gMnSc1	tetřívek
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Tetrao	Tetrao	k6eAd1	Tetrao
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Lyrurus	Lyrurus	k1gInSc1	Lyrurus
<g/>
)	)	kIx)	)
tetrix	tetrix	k1gInSc1	tetrix
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
hrabavého	hrabavý	k2eAgMnSc2d1	hrabavý
ptáka	pták	k1gMnSc2	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
tetřevovitých	tetřevovitý	k2eAgFnPc2d1	tetřevovitý
(	(	kIx(	(
<g/>
Tetraonidae	Tetraonidae	k1gFnPc2	Tetraonidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Velikosti	velikost	k1gFnPc1	velikost
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnPc1	tělo
samců	samec	k1gInPc2	samec
49	[number]	k4	49
<g/>
–	–	k?	–
<g/>
58	[number]	k4	58
cm	cm	kA	cm
<g/>
,	,	kIx,	,
samic	samice	k1gFnPc2	samice
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgMnSc1d1	černý
(	(	kIx(	(
<g/>
s	s	k7c7	s
nafialovělým	nafialovělý	k2eAgInSc7d1	nafialovělý
leskem	lesk	k1gInSc7	lesk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgInPc1d1	bílý
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
spodní	spodní	k2eAgFnPc1d1	spodní
ocasní	ocasní	k2eAgFnPc1d1	ocasní
krovky	krovka	k1gFnPc1	krovka
<g/>
,	,	kIx,	,
spodina	spodina	k1gFnSc1	spodina
křídla	křídlo	k1gNnSc2	křídlo
a	a	k8xC	a
úzká	úzký	k2eAgFnSc1d1	úzká
křídelní	křídelní	k2eAgFnSc1d1	křídelní
páska	páska	k1gFnSc1	páska
<g/>
.	.	kIx.	.
</s>
<s>
Nápadný	nápadný	k2eAgMnSc1d1	nápadný
je	být	k5eAaImIp3nS	být
červený	červený	k2eAgInSc1d1	červený
hřebínek	hřebínek	k1gInSc1	hřebínek
nad	nad	k7c7	nad
okem	oke	k1gNnSc7	oke
a	a	k8xC	a
lyrovitý	lyrovitý	k2eAgInSc1d1	lyrovitý
ocas	ocas	k1gInSc1	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
nenápadná	nápadný	k2eNgFnSc1d1	nenápadná
<g/>
,	,	kIx,	,
šedohnědá	šedohnědý	k2eAgFnSc1d1	šedohnědá
s	s	k7c7	s
černým	černý	k2eAgNnSc7d1	černé
proužkováním	proužkování	k1gNnSc7	proužkování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
druhu	druh	k1gInSc2	druh
sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
přes	přes	k7c4	přes
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
území	území	k1gNnSc4	území
Eurasie	Eurasie	k1gFnSc2	Eurasie
východně	východně	k6eAd1	východně
až	až	k9	až
po	po	k7c4	po
Ochotské	ochotský	k2eAgNnSc4d1	Ochotské
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Stálý	stálý	k2eAgInSc1d1	stálý
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
byl	být	k5eAaImAgMnS	být
tetřívek	tetřívek	k1gMnSc1	tetřívek
nejpočetnější	početní	k2eAgFnSc2d3	nejpočetnější
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jeho	jeho	k3xOp3gInPc1	jeho
počty	počet	k1gInPc1	počet
neustále	neustále	k6eAd1	neustále
klesají	klesat	k5eAaImIp3nP	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
postupně	postupně	k6eAd1	postupně
vymizel	vymizet	k5eAaPmAgMnS	vymizet
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
oblastmi	oblast	k1gFnPc7	oblast
výskytu	výskyt	k1gInSc2	výskyt
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
Krušné	krušný	k2eAgFnSc2d1	krušná
hory	hora	k1gFnSc2	hora
(	(	kIx(	(
<g/>
200	[number]	k4	200
samců	samec	k1gMnPc2	samec
<g/>
,	,	kIx,	,
cca	cca	kA	cca
polovina	polovina	k1gFnSc1	polovina
české	český	k2eAgFnSc2d1	Česká
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jizerské	jizerský	k2eAgFnPc1d1	Jizerská
hory	hora	k1gFnPc1	hora
(	(	kIx(	(
<g/>
35	[number]	k4	35
samců	samec	k1gInPc2	samec
<g/>
)	)	kIx)	)
a	a	k8xC	a
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
(	(	kIx(	(
<g/>
75	[number]	k4	75
samců	samec	k1gInPc2	samec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnPc1d1	malá
populace	populace	k1gFnPc1	populace
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
,	,	kIx,	,
v	v	k7c6	v
Doupovských	Doupovský	k2eAgFnPc6d1	Doupovská
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
Slavkovském	slavkovský	k2eAgInSc6d1	slavkovský
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
Novohradských	novohradský	k2eAgFnPc6d1	Novohradská
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
Oderských	oderský	k2eAgFnPc6d1	Oderská
vrších	vrš	k1gFnPc6	vrš
<g/>
,	,	kIx,	,
Králickém	králický	k2eAgInSc6d1	králický
Sněžníku	Sněžník	k1gInSc6	Sněžník
a	a	k8xC	a
Jeseníkách	Jeseníky	k1gInPc6	Jeseníky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1977	[number]	k4	1977
byla	být	k5eAaImAgFnS	být
celková	celkový	k2eAgFnSc1d1	celková
početnost	početnost	k1gFnSc1	početnost
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
2500-4500	[number]	k4	2500-4500
samců	samec	k1gMnPc2	samec
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
na	na	k7c4	na
1100	[number]	k4	1100
<g/>
–	–	k?	–
<g/>
2200	[number]	k4	2200
samců	samec	k1gInPc2	samec
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
na	na	k7c4	na
800	[number]	k4	800
<g/>
–	–	k?	–
<g/>
1000	[number]	k4	1000
samců	samec	k1gInPc2	samec
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2015	[number]	k4	2015
-	-	kIx~	-
2018	[number]	k4	2018
již	již	k6eAd1	již
jen	jen	k9	jen
na	na	k7c4	na
400	[number]	k4	400
-	-	kIx~	-
500	[number]	k4	500
samců	samec	k1gMnPc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
chráněný	chráněný	k2eAgInSc1d1	chráněný
jako	jako	k8xC	jako
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
<g/>
Osidluje	osidlovat	k5eAaImIp3nS	osidlovat
různé	různý	k2eAgInPc4d1	různý
biotopy	biotop	k1gInPc4	biotop
(	(	kIx(	(
<g/>
slatiny	slatina	k1gFnPc4	slatina
<g/>
,	,	kIx,	,
rašeliniště	rašeliniště	k1gNnPc4	rašeliniště
<g/>
,	,	kIx,	,
lesní	lesní	k2eAgFnPc4d1	lesní
paseky	paseka	k1gFnPc4	paseka
<g/>
,	,	kIx,	,
vřesoviště	vřesoviště	k1gNnSc1	vřesoviště
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
je	být	k5eAaImIp3nS	být
zastoupena	zastoupen	k2eAgFnSc1d1	zastoupena
zejména	zejména	k9	zejména
rostlinná	rostlinný	k2eAgFnSc1d1	rostlinná
složka	složka	k1gFnSc1	složka
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
pak	pak	k6eAd1	pak
i	i	k8xC	i
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
zpočátku	zpočátku	k6eAd1	zpočátku
téměř	téměř	k6eAd1	téměř
výlučné	výlučný	k2eAgNnSc4d1	výlučné
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Polygamní	polygamní	k2eAgInSc1d1	polygamní
druh	druh	k1gInSc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
až	až	k8xS	až
květnu	květen	k1gInSc6	květen
charakterističtí	charakteristický	k2eAgMnPc1d1	charakteristický
společným	společný	k2eAgInSc7d1	společný
tokem	tok	k1gInSc7	tok
na	na	k7c6	na
otevřených	otevřený	k2eAgInPc6d1	otevřený
prostranstvích	prostranství	k1gNnPc6	prostranství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
roztaženým	roztažený	k2eAgInSc7d1	roztažený
ocasem	ocas	k1gInSc7	ocas
a	a	k8xC	a
spuštěnými	spuštěný	k2eAgNnPc7d1	spuštěné
křídly	křídlo	k1gNnPc7	křídlo
pobíhají	pobíhat	k5eAaImIp3nP	pobíhat
<g/>
,	,	kIx,	,
vyskakují	vyskakovat	k5eAaImIp3nP	vyskakovat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
a	a	k8xC	a
často	často	k6eAd1	často
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
bojují	bojovat	k5eAaImIp3nP	bojovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejúspěšnější	úspěšný	k2eAgMnPc1d3	nejúspěšnější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
páří	pářit	k5eAaImIp3nS	pářit
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
samic	samice	k1gFnPc2	samice
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
skryté	skrytý	k2eAgFnPc1d1	skrytá
pod	pod	k7c7	pod
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
,	,	kIx,	,
snůška	snůška	k1gFnSc1	snůška
čítá	čítat	k5eAaImIp3nS	čítat
9-10	[number]	k4	9-10
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
žlutohnědých	žlutohnědý	k2eAgFnPc2d1	žlutohnědá
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
i	i	k9	i
načervenalých	načervenalý	k2eAgFnPc2d1	načervenalá
nebo	nebo	k8xC	nebo
okrových	okrový	k2eAgFnPc2d1	okrová
<g/>
,	,	kIx,	,
tmavohnědě	tmavohnědě	k6eAd1	tmavohnědě
skvrnitých	skvrnitý	k2eAgNnPc2d1	skvrnité
vajec	vejce	k1gNnPc2	vejce
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
49,7	[number]	k4	49,7
x	x	k?	x
36,0	[number]	k4	36,0
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
trvá	trvat	k5eAaImIp3nS	trvat
typicky	typicky	k6eAd1	typicky
25-27	[number]	k4	25-27
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
sedí	sedit	k5eAaImIp3nP	sedit
samotná	samotný	k2eAgFnSc1d1	samotná
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
také	také	k9	také
vodí	vodit	k5eAaImIp3nS	vodit
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
vzletná	vzletný	k2eAgNnPc1d1	vzletné
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
2	[number]	k4	2
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
pohromadě	pohromadě	k6eAd1	pohromadě
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
srpna	srpen	k1gInSc2	srpen
nebo	nebo	k8xC	nebo
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
do	do	k7c2	do
menších	malý	k2eAgNnPc2d2	menší
hejnek	hejnko	k1gNnPc2	hejnko
podle	podle	k7c2	podle
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tetřívek	tetřívek	k1gMnSc1	tetřívek
obecný	obecný	k2eAgMnSc1d1	obecný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
tetřívek	tetřívek	k1gMnSc1	tetřívek
obecný	obecný	k2eAgMnSc1d1	obecný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Lyrurus	Lyrurus	k1gInSc1	Lyrurus
tetrix	tetrix	k1gInSc1	tetrix
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
