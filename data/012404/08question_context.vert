<s>
Tetřívek	tetřívek	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Tetrao	Tetrao	k6eAd1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Lyrurus	Lyrurus	k1gInSc1
<g/>
)	)	kIx)
tetrix	tetrix	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
velký	velký	k2eAgInSc1d1
druh	druh	k1gInSc1
hrabavého	hrabavý	k2eAgMnSc2d1
ptáka	pták	k1gMnSc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
tetřevovitých	tetřevovitý	k2eAgFnPc2d1
(	(	kIx(
<g/>
Tetraonidae	Tetraonidae	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>