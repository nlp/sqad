<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
přilehlá	přilehlý	k2eAgFnSc1d1	přilehlá
oblast	oblast	k1gFnSc1	oblast
styku	styk	k1gInSc2	styk
mezi	mezi	k7c7	mezi
pevninou	pevnina	k1gFnSc7	pevnina
a	a	k8xC	a
oceánem	oceán	k1gInSc7	oceán
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mapách	mapa	k1gFnPc6	mapa
zakreslována	zakreslovat	k5eAaImNgFnS	zakreslovat
jako	jako	k8xC	jako
linie	linie	k1gFnSc1	linie
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
klifové	klifový	k2eAgNnSc4d1	klifový
pobřeží	pobřeží	k1gNnSc4	pobřeží
a	a	k8xC	a
plážové	plážový	k2eAgNnSc4d1	plážové
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
liší	lišit	k5eAaImIp3nP	lišit
stavbou	stavba	k1gFnSc7	stavba
a	a	k8xC	a
vertikální	vertikální	k2eAgFnSc7d1	vertikální
členitostí	členitost	k1gFnSc7	členitost
<g/>
.	.	kIx.	.
</s>
<s>
Klifové	Klifový	k2eAgNnSc1d1	Klifový
pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
skalními	skalní	k2eAgInPc7d1	skalní
tvary	tvar	k1gInPc7	tvar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
ostrý	ostrý	k2eAgInSc4d1	ostrý
přechod	přechod	k1gInSc4	přechod
mezi	mezi	k7c7	mezi
oceánem	oceán	k1gInSc7	oceán
a	a	k8xC	a
pevninou	pevnina	k1gFnSc7	pevnina
a	a	k8xC	a
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
klify	klif	k1gInPc1	klif
(	(	kIx(	(
<g/>
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
významu	význam	k1gInSc6	význam
útesy	útes	k1gInPc1	útes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
abrazní	abrazní	k2eAgFnSc2d1	abrazní
činnosti	činnost	k1gFnSc2	činnost
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
pevnina	pevnina	k1gFnSc1	pevnina
postupně	postupně	k6eAd1	postupně
zatlačována	zatlačován	k2eAgFnSc1d1	zatlačována
a	a	k8xC	a
horninový	horninový	k2eAgInSc1d1	horninový
materiál	materiál	k1gInSc1	materiál
je	být	k5eAaImIp3nS	být
odnášen	odnášen	k2eAgInSc4d1	odnášen
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
různé	různý	k2eAgInPc1d1	různý
specifické	specifický	k2eAgInPc1d1	specifický
tvary	tvar	k1gInPc1	tvar
jako	jako	k8xS	jako
suky	suk	k1gInPc1	suk
<g/>
,	,	kIx,	,
skalní	skalní	k2eAgFnPc1d1	skalní
brány	brána	k1gFnPc1	brána
či	či	k8xC	či
abrazní	abrazní	k2eAgFnPc1d1	abrazní
mořské	mořský	k2eAgFnPc1d1	mořská
terasy	terasa	k1gFnPc1	terasa
<g/>
.	.	kIx.	.
</s>
<s>
Plážové	plážový	k2eAgNnSc1d1	plážové
pobřeží	pobřeží	k1gNnSc1	pobřeží
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
zde	zde	k6eAd1	zde
ukládá	ukládat	k5eAaImIp3nS	ukládat
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
abrazní	abrazní	k2eAgFnSc1d1	abrazní
činností	činnost	k1gFnSc7	činnost
odnáší	odnášet	k5eAaImIp3nS	odnášet
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
tak	tak	k9	tak
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
oblasti	oblast	k1gFnPc4	oblast
vyplaveného	vyplavený	k2eAgInSc2d1	vyplavený
písku	písek	k1gInSc2	písek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
formuje	formovat	k5eAaImIp3nS	formovat
do	do	k7c2	do
pláže	pláž	k1gFnSc2	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vlivem	k7c2	vlivem
odnosu	odnos	k1gInSc2	odnos
větru	vítr	k1gInSc2	vítr
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
písečných	písečný	k2eAgFnPc2d1	písečná
dun	duna	k1gFnPc2	duna
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
menší	malý	k2eAgFnPc4d2	menší
dílčí	dílčí	k2eAgFnPc4d1	dílčí
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
specifické	specifický	k2eAgFnPc1d1	specifická
pro	pro	k7c4	pro
lokální	lokální	k2eAgFnPc4d1	lokální
oblasti	oblast	k1gFnPc4	oblast
<g/>
:	:	kIx,	:
útes	útes	k1gInSc1	útes
–	–	k?	–
skalní	skalní	k2eAgNnSc1d1	skalní
těleso	těleso	k1gNnSc4	těleso
vystupující	vystupující	k2eAgNnSc4d1	vystupující
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
či	či	k8xC	či
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
ním	on	k3xPp3gNnSc7	on
tyčící	tyčící	k2eAgFnSc1d1	tyčící
<g/>
,	,	kIx,	,
delta	delta	k1gFnSc1	delta
–	–	k?	–
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
/	/	kIx~	/
<g/>
oceánu	oceán	k1gInSc2	oceán
vlévá	vlévat	k5eAaImIp3nS	vlévat
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
pláž	pláž	k1gFnSc1	pláž
–	–	k?	–
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
značné	značný	k2eAgInPc4d1	značný
nánosy	nános	k1gInPc4	nános
písku	písek	k1gInSc2	písek
<g/>
,	,	kIx,	,
kosa	kosa	k1gFnSc1	kosa
–	–	k?	–
písečný	písečný	k2eAgInSc1d1	písečný
předbřežní	předbřežní	k2eAgInSc1d1	předbřežní
val	val	k1gInSc1	val
tvořící	tvořící	k2eAgFnSc1d1	tvořící
souš	souš	k1gFnSc1	souš
<g/>
,	,	kIx,	,
tombolo	tombolo	k1gNnSc1	tombolo
–	–	k?	–
písečný	písečný	k2eAgInSc1d1	písečný
val	val	k1gInSc1	val
spojující	spojující	k2eAgInSc1d1	spojující
ostrov	ostrov	k1gInSc4	ostrov
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
ostrovem	ostrov	k1gInSc7	ostrov
či	či	k8xC	či
pevninou	pevnina	k1gFnSc7	pevnina
<g/>
,	,	kIx,	,
sebcha	sebcha	k1gFnSc1	sebcha
–	–	k?	–
přímořská	přímořský	k2eAgFnSc1d1	přímořská
plošina	plošina	k1gFnSc1	plošina
občasně	občasně	k6eAd1	občasně
zaplavovaná	zaplavovaný	k2eAgFnSc1d1	zaplavovaná
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
