<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
přilehlá	přilehlý	k2eAgFnSc1d1	přilehlá
oblast	oblast	k1gFnSc1	oblast
styku	styk	k1gInSc2	styk
mezi	mezi	k7c7	mezi
pevninou	pevnina	k1gFnSc7	pevnina
a	a	k8xC	a
oceánem	oceán	k1gInSc7	oceán
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mapách	mapa	k1gFnPc6	mapa
zakreslována	zakreslovat	k5eAaImNgFnS	zakreslovat
jako	jako	k8xC	jako
linie	linie	k1gFnSc1	linie
<g/>
.	.	kIx.	.
</s>
