<p>
<s>
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Mloky	mlok	k1gMnPc7	mlok
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
klasická	klasický	k2eAgFnSc1d1	klasická
alegorie	alegorie	k1gFnSc1	alegorie
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
,	,	kIx,	,
napsaná	napsaný	k2eAgFnSc1d1	napsaná
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
vydaná	vydaný	k2eAgFnSc1d1	vydaná
v	v	k7c6	v
letech	let	k1gInPc6	let
1935	[number]	k4	1935
<g/>
–	–	k?	–
<g/>
1936	[number]	k4	1936
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
v	v	k7c6	v
Lidových	lidový	k2eAgFnPc6d1	lidová
novinách	novina	k1gFnPc6	novina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
Van	vana	k1gFnPc2	vana
Toch	Toch	k1gMnSc1	Toch
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
<g/>
)	)	kIx)	)
objeví	objevit	k5eAaPmIp3nS	objevit
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Tana	tanout	k5eAaImSgInS	tanout
Masa	maso	k1gNnSc2	maso
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
podivné	podivný	k2eAgNnSc1d1	podivné
<g/>
,	,	kIx,	,
inteligentní	inteligentní	k2eAgInPc1d1	inteligentní
<g/>
,	,	kIx,	,
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
žijící	žijící	k2eAgMnPc4d1	žijící
tvory	tvor	k1gMnPc4	tvor
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgInPc1d1	podobný
mlokům	mlok	k1gMnPc3	mlok
<g/>
.	.	kIx.	.
</s>
<s>
Zahájí	zahájit	k5eAaPmIp3nP	zahájit
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
výměnný	výměnný	k2eAgInSc4d1	výměnný
obchod	obchod	k1gInSc4	obchod
<g/>
:	:	kIx,	:
kapitán	kapitán	k1gMnSc1	kapitán
Van	vana	k1gFnPc2	vana
Toch	Toch	k1gMnSc1	Toch
dodá	dodat	k5eAaPmIp3nS	dodat
mlokům	mlok	k1gMnPc3	mlok
nože	nůž	k1gInSc2	nůž
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
proti	proti	k7c3	proti
žralokům	žralok	k1gMnPc3	žralok
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mločí	mločí	k2eAgFnSc4d1	mločí
populaci	populace	k1gFnSc4	populace
decimují	decimovat	k5eAaBmIp3nP	decimovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
mloci	mlok	k1gMnPc1	mlok
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
budou	být	k5eAaImBp3nP	být
nosit	nosit	k5eAaImF	nosit
kapitánu	kapitán	k1gMnSc3	kapitán
Van	van	k1gInSc4	van
Tochovi	Tochův	k2eAgMnPc1d1	Tochův
perly	perla	k1gFnSc2	perla
<g/>
.	.	kIx.	.
</s>
<s>
Seznámí	seznámit	k5eAaPmIp3nP	seznámit
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
projektem	projekt	k1gInSc7	projekt
svého	svůj	k3xOyFgMnSc2	svůj
známého	známý	k1gMnSc2	známý
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
továrníka	továrník	k1gMnSc2	továrník
G.	G.	kA	G.
H.	H.	kA	H.
Bondyho	Bondy	k1gMnSc2	Bondy
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
začne	začít	k5eAaPmIp3nS	začít
mlokům	mlok	k1gMnPc3	mlok
dodávat	dodávat	k5eAaImF	dodávat
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
potřeby	potřeba	k1gFnPc4	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
Van	vana	k1gFnPc2	vana
Toch	Toch	k1gInSc1	Toch
mezitím	mezitím	k6eAd1	mezitím
vysazuje	vysazovat	k5eAaImIp3nS	vysazovat
mloky	mlok	k1gMnPc4	mlok
i	i	k9	i
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
ostrovech	ostrov	k1gInPc6	ostrov
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
kapitána	kapitán	k1gMnSc2	kapitán
Van	vana	k1gFnPc2	vana
Tocha	Tocha	k1gMnSc1	Tocha
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
obchodovat	obchodovat	k5eAaImF	obchodovat
jako	jako	k9	jako
s	s	k7c7	s
levnou	levný	k2eAgFnSc7d1	levná
pracovní	pracovní	k2eAgFnSc7d1	pracovní
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
podrobeni	podroben	k2eAgMnPc1d1	podroben
vědeckému	vědecký	k2eAgInSc3d1	vědecký
výzkumu	výzkum	k1gInSc3	výzkum
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
vyjde	vyjít	k5eAaPmIp3nS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
druh	druh	k1gInSc4	druh
Andrias	Andrias	k1gInSc4	Andrias
scheuchzeri	scheuchzer	k1gFnSc2	scheuchzer
<g/>
,	,	kIx,	,
miocénního	miocénní	k2eAgMnSc2d1	miocénní
mloka	mlok	k1gMnSc2	mlok
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
vyhynutí	vyhynutí	k1gNnSc1	vyhynutí
<g/>
.	.	kIx.	.
</s>
<s>
Mloci	mlok	k1gMnPc1	mlok
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
vyvinou	vyvinout	k5eAaPmIp3nP	vyvinout
v	v	k7c4	v
ještě	ještě	k6eAd1	ještě
inteligentnější	inteligentní	k2eAgMnPc4d2	inteligentnější
tvory	tvor	k1gMnPc4	tvor
(	(	kIx(	(
<g/>
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
se	se	k3xPyFc4	se
přemnoží	přemnožit	k5eAaPmIp3nP	přemnožit
<g/>
,	,	kIx,	,
vzbouří	vzbouřit	k5eAaPmIp3nP	vzbouřit
<g/>
,	,	kIx,	,
začnou	začít	k5eAaPmIp3nP	začít
vyhrožovat	vyhrožovat	k5eAaImF	vyhrožovat
lidem	člověk	k1gMnPc3	člověk
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
jim	on	k3xPp3gMnPc3	on
nedovolí	dovolit	k5eNaPmIp3nS	dovolit
zbourat	zbourat	k5eAaPmF	zbourat
břehy	břeh	k1gInPc4	břeh
pevnin	pevnina	k1gFnPc2	pevnina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
rozšířit	rozšířit	k5eAaPmF	rozšířit
svůj	svůj	k3xOyFgInSc4	svůj
podmořský	podmořský	k2eAgInSc4d1	podmořský
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mloci	mlok	k1gMnPc1	mlok
bourají	bourat	k5eAaImIp3nP	bourat
pevniny	pevnina	k1gFnPc4	pevnina
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
potřebě	potřeba	k1gFnSc3	potřeba
vzniku	vznik	k1gInSc2	vznik
mělčin	mělčina	k1gFnPc2	mělčina
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
<g/>
,	,	kIx,	,
že	že	k8xS	že
mloci	mlok	k1gMnPc1	mlok
nemohou	moct	k5eNaImIp3nP	moct
žít	žít	k5eAaImF	žít
ve	v	k7c6	v
sladké	sladký	k2eAgFnSc6d1	sladká
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Beznadějnost	beznadějnost	k1gFnSc1	beznadějnost
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
zdůrazněna	zdůraznit	k5eAaPmNgFnS	zdůraznit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
už	už	k9	už
i	i	k9	i
ve	v	k7c6	v
Vltavě	Vltava	k1gFnSc6	Vltava
objeví	objevit	k5eAaPmIp3nS	objevit
černá	černý	k2eAgFnSc1d1	černá
hlava	hlava	k1gFnSc1	hlava
s	s	k7c7	s
opačnými	opačný	k2eAgNnPc7d1	opačné
víčky	víčko	k1gNnPc7	víčko
<g/>
.	.	kIx.	.
</s>
<s>
Budoucnost	budoucnost	k1gFnSc1	budoucnost
lidstva	lidstvo	k1gNnSc2	lidstvo
je	být	k5eAaImIp3nS	být
nejistá	jistý	k2eNgFnSc1d1	nejistá
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
==	==	k?	==
Výklad	výklad	k1gInSc1	výklad
==	==	k?	==
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
tématu	téma	k1gNnSc2	téma
relativně	relativně	k6eAd1	relativně
klasickou	klasický	k2eAgFnSc7d1	klasická
antiutopií	antiutopie	k1gFnSc7	antiutopie
<g/>
,	,	kIx,	,
obávající	obávající	k2eAgNnSc4d1	obávající
se	se	k3xPyFc4	se
odlidštění	odlidštění	k1gNnSc4	odlidštění
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
textu	text	k1gInSc6	text
je	být	k5eAaImIp3nS	být
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
narážek	narážka	k1gFnPc2	narážka
na	na	k7c4	na
německý	německý	k2eAgInSc4d1	německý
nacismus	nacismus	k1gInSc4	nacismus
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
Čapek	Čapek	k1gMnSc1	Čapek
hluboce	hluboko	k6eAd1	hluboko
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Zejména	zejména	k9	zejména
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
byla	být	k5eAaImAgFnS	být
veškerá	veškerý	k3xTgFnSc1	veškerý
vivisekce	vivisekce	k1gFnSc1	vivisekce
(	(	kIx(	(
<g/>
mloků	mlok	k1gMnPc2	mlok
<g/>
)	)	kIx)	)
přísně	přísně	k6eAd1	přísně
zapovězena	zapovědět	k5eAaPmNgFnS	zapovědět
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
židovským	židovský	k2eAgMnPc3d1	židovský
badatelům	badatel	k1gMnPc3	badatel
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celé	celý	k2eAgNnSc1d1	celé
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
také	také	k9	také
možno	možno	k6eAd1	možno
vykládat	vykládat	k5eAaImF	vykládat
jako	jako	k9	jako
alegorii	alegorie	k1gFnSc4	alegorie
na	na	k7c4	na
rozpínavost	rozpínavost	k1gFnSc4	rozpínavost
Třetí	třetí	k4xOgFnSc1	třetí
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
teze	teze	k1gFnSc1	teze
o	o	k7c6	o
lebensraumu	lebensraum	k1gInSc6	lebensraum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Adaptace	adaptace	k1gFnSc1	adaptace
a	a	k8xC	a
inspirace	inspirace	k1gFnSc1	inspirace
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
adaptace	adaptace	k1gFnSc1	adaptace
románu	román	k1gInSc2	román
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Högerem	Höger	k1gMnSc7	Höger
<g/>
,	,	kIx,	,
Janem	Jan	k1gMnSc7	Jan
Pivcem	Pivce	k1gMnSc7	Pivce
<g/>
,	,	kIx,	,
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Hrušínským	Hrušínský	k2eAgMnSc7d1	Hrušínský
<g/>
,	,	kIx,	,
Františkem	František	k1gMnSc7	František
Filipovským	Filipovský	k1gMnSc7	Filipovský
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
režírovaná	režírovaný	k2eAgFnSc1d1	režírovaná
Jiřím	Jiří	k1gMnSc7	Jiří
Horčičkou	Horčička	k1gMnSc7	Horčička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vyšla	vyjít	k5eAaPmAgFnS	vyjít
na	na	k7c4	na
2CD	[number]	k4	2CD
ve	v	k7c6	v
vydavatelství	vydavatelství	k1gNnSc6	vydavatelství
Radioservis	Radioservis	k1gFnSc2	Radioservis
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
chystal	chystat	k5eAaImAgMnS	chystat
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
napsat	napsat	k5eAaBmF	napsat
na	na	k7c4	na
námět	námět	k1gInSc4	námět
románu	román	k1gInSc2	román
hudební	hudební	k2eAgNnSc4d1	hudební
představení	představení	k1gNnSc4	představení
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
texty	text	k1gInPc4	text
deseti	deset	k4xCc2	deset
písniček	písnička	k1gFnPc2	písnička
(	(	kIx(	(
<g/>
Byznys	byznys	k1gInSc1	byznys
<g/>
,	,	kIx,	,
Čerti	čert	k1gMnPc1	čert
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Kapitáni	kapitán	k1gMnPc1	kapitán
<g/>
,	,	kIx,	,
Kuplet	kuplet	k1gInSc1	kuplet
<g/>
,	,	kIx,	,
Salamandr	salamandr	k1gMnSc1	salamandr
song	song	k1gInSc1	song
<g/>
,	,	kIx,	,
Star	Star	kA	Star
<g/>
,	,	kIx,	,
Tana	tanout	k5eAaImSgMnS	tanout
Masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
perla	perla	k1gFnSc1	perla
kapitáne	kapitán	k1gMnSc5	kapitán
<g/>
,	,	kIx,	,
To	ten	k3xDgNnSc1	ten
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
přece	přece	k9	přece
kapitál	kapitál	k1gInSc1	kapitál
<g/>
,	,	kIx,	,
Ty	ten	k3xDgInPc4	ten
starý	starý	k2eAgInSc4d1	starý
časy	čas	k1gInPc4	čas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
zhudebnil	zhudebnit	k5eAaPmAgMnS	zhudebnit
Václav	Václav	k1gMnSc1	Václav
Kašlík	kašlík	k1gInSc4	kašlík
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
nerealizoval	realizovat	k5eNaBmAgInS	realizovat
<g/>
,	,	kIx,	,
texty	text	k1gInPc1	text
písniček	písnička	k1gFnPc2	písnička
však	však	k9	však
vyšly	vyjít	k5eAaPmAgInP	vyjít
knižně	knižně	k6eAd1	knižně
v	v	k7c6	v
Encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
Jiřího	Jiří	k1gMnSc2	Jiří
Suchého	Suchý	k1gMnSc2	Suchý
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
7	[number]	k4	7
<g/>
,	,	kIx,	,
Písničky	písnička	k1gFnPc4	písnička
To	to	k9	to
<g/>
–	–	k?	–
<g/>
Ž	Ž	kA	Ž
<g/>
,	,	kIx,	,
Karolinum	Karolinum	k1gNnSc1	Karolinum
a	a	k8xC	a
Pražská	pražský	k2eAgFnSc1d1	Pražská
imaginace	imaginace	k1gFnSc1	imaginace
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
s.	s.	k?	s.
89	[number]	k4	89
<g/>
–	–	k?	–
<g/>
99	[number]	k4	99
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgInP	objevovat
v	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c6	o
připravovaném	připravovaný	k2eAgInSc6d1	připravovaný
filmu	film	k1gInSc6	film
Tomáše	Tomáš	k1gMnSc2	Tomáš
Krejčího	Krejčí	k1gMnSc2	Krejčí
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Státní	státní	k2eAgFnSc6d1	státní
opeře	opera	k1gFnSc6	opera
Praha	Praha	k1gFnSc1	Praha
uvedena	uveden	k2eAgFnSc1d1	uvedena
opera	opera	k1gFnSc1	opera
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
skladatele	skladatel	k1gMnSc2	skladatel
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Franze	Franze	k1gFnSc2	Franze
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2018	[number]	k4	2018
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Divadle	divadlo	k1gNnSc6	divadlo
D21	D21	k1gFnPc2	D21
uvedena	uvést	k5eAaPmNgFnS	uvést
kabaretní	kabaretní	k2eAgFnSc1d1	kabaretní
adaptace	adaptace	k1gFnSc1	adaptace
Války	válka	k1gFnSc2	válka
s	s	k7c7	s
Mloky	mlok	k1gMnPc7	mlok
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
kterou	který	k3yQgFnSc4	který
pro	pro	k7c4	pro
D21	D21	k1gFnSc4	D21
zdramatizoval	zdramatizovat	k5eAaPmAgMnS	zdramatizovat
David	David	k1gMnSc1	David
Košťák	Košťák	k1gMnSc1	Košťák
a	a	k8xC	a
zrežíroval	zrežírovat	k5eAaPmAgMnS	zrežírovat
Jakub	Jakub	k1gMnSc1	Jakub
Šmíd	Šmíd	k1gMnSc1	Šmíd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
na	na	k7c6	na
webu	web	k1gInSc6	web
Osel	osel	k1gMnSc1	osel
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
mnohozvířetem	mnohozvíře	k1gNnSc7	mnohozvíře
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
nemoc	nemoc	k1gFnSc1	nemoc
</s>
</p>
<p>
<s>
Antiutopie	Antiutopie	k1gFnSc1	Antiutopie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Válka	Válek	k1gMnSc2	Válek
s	s	k7c7	s
Mloky	mlok	k1gMnPc7	mlok
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Mloky	mlok	k1gMnPc7	mlok
v	v	k7c6	v
Digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
na	na	k7c6	na
webu	web	k1gInSc6	web
Městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Mloky	mlok	k1gMnPc7	mlok
v	v	k7c6	v
Československé	československý	k2eAgFnSc6d1	Československá
bibliografické	bibliografický	k2eAgFnSc6d1	bibliografická
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
