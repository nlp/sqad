<s>
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Mloky	mlok	k1gMnPc7	mlok
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
klasická	klasický	k2eAgFnSc1d1	klasická
alegorie	alegorie	k1gFnSc1	alegorie
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
,	,	kIx,	,
napsaná	napsaný	k2eAgFnSc1d1	napsaná
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
vydaná	vydaný	k2eAgFnSc1d1	vydaná
v	v	k7c6	v
letech	let	k1gInPc6	let
1935	[number]	k4	1935
<g/>
–	–	k?	–
<g/>
1936	[number]	k4	1936
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
v	v	k7c6	v
Lidových	lidový	k2eAgFnPc6d1	lidová
novinách	novina	k1gFnPc6	novina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
