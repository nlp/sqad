<s>
Belgická	belgický	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
</s>
<s>
Belgická	belgický	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
vlajkaPoměr	vlajkaPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Belgická	belgický	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
a	a	k8xC
válečná	válečný	k2eAgFnSc1d1
vlajkaPoměr	vlajkaPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
13	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
</s>
<s>
Belgická	belgický	k2eAgFnSc1d1
státní	státní	k2eAgInSc1d1
námořní	námořní	k2eAgInSc1d1
vlajkaPoměr	vlajkaPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Belgická	belgický	k2eAgFnSc1d1
námořní	námořní	k2eAgFnSc1d1
válečná	válečná	k1gFnSc1
vlajkaPoměr	vlajkaPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Belgie	Belgie	k1gFnSc1
má	mít	k5eAaImIp3nS
podobu	podoba	k1gFnSc4
tří	tři	k4xCgInPc2
stejně	stejně	k6eAd1
širokých	široký	k2eAgInPc2d1
svislých	svislý	k2eAgInPc2d1
pruhů	pruh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zleva	zleva	k6eAd1
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc4
<g/>
:	:	kIx,
černý	černý	k2eAgInSc4d1
<g/>
,	,	kIx,
žlutý	žlutý	k2eAgInSc4d1
a	a	k8xC
červený	červený	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poměr	poměr	k1gInSc1
stran	stran	k7c2
listu	list	k1gInSc2
národní	národní	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
je	být	k5eAaImIp3nS
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podoba	podoba	k1gFnSc1
vlajky	vlajka	k1gFnSc2
je	být	k5eAaImIp3nS
odvozena	odvodit	k5eAaPmNgFnS
od	od	k7c2
francouzské	francouzský	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
a	a	k8xC
jako	jako	k9
barvy	barva	k1gFnPc1
jsou	být	k5eAaImIp3nP
užity	užit	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
Brabantského	Brabantský	k2eAgNnSc2d1
vévodství	vévodství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Civilní	civilní	k2eAgFnSc1d1
garda	garda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
po	po	k7c6
belgickém	belgický	k2eAgNnSc6d1
povstání	povstání	k1gNnSc6
proti	proti	k7c3
holandské	holandský	k2eAgFnSc3d1
nadvládě	nadvláda	k1gFnSc3
<g/>
,	,	kIx,
nosila	nosit	k5eAaImAgFnS
brabantské	brabantský	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neobvyklý	obvyklý	k2eNgInSc1d1
poměr	poměr	k1gInSc1
stran	strana	k1gFnPc2
státní	státní	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
13	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
je	být	k5eAaImIp3nS
neznámého	známý	k2eNgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vlajka	vlajka	k1gFnSc1
byla	být	k5eAaImAgFnS
oficiálně	oficiálně	k6eAd1
přijata	přijat	k2eAgFnSc1d1
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1831	#num#	k4
<g/>
,	,	kIx,
krátce	krátce	k6eAd1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
Belgie	Belgie	k1gFnSc1
získala	získat	k5eAaPmAgFnS
nezávislost	nezávislost	k1gFnSc4
na	na	k7c6
Nizozemsku	Nizozemsko	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1830	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
měla	mít	k5eAaImAgFnS
vlajka	vlajka	k1gFnSc1
pruhy	pruh	k1gInPc4
vodorovné	vodorovný	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
z	z	k7c2
důvodu	důvod	k1gInSc2
podoby	podoba	k1gFnSc2
s	s	k7c7
nizozemskou	nizozemský	k2eAgFnSc7d1
vlajkou	vlajka	k1gFnSc7
byly	být	k5eAaImAgFnP
změněny	změněn	k2eAgInPc1d1
na	na	k7c4
svislé	svislý	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Paradoxně	paradoxně	k6eAd1
může	moct	k5eAaImIp3nS
působit	působit	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
článek	článek	k1gInSc1
193	#num#	k4
belgické	belgický	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
popisuje	popisovat	k5eAaImIp3nS
národní	národní	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
jako	jako	k9
červenou	červený	k2eAgFnSc4d1
<g/>
,	,	kIx,
žlutou	žlutý	k2eAgFnSc4d1
a	a	k8xC
černou	černý	k2eAgFnSc4d1
<g/>
,	,	kIx,
namísto	namísto	k7c2
pořadí	pořadí	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
použito	použít	k5eAaPmNgNnS
na	na	k7c6
vlajce	vlajka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Užívání	užívání	k1gNnSc1
vlajky	vlajka	k1gFnSc2
</s>
<s>
Užívání	užívání	k1gNnSc1
státní	státní	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
a	a	k8xC
ostatních	ostatní	k2eAgFnPc2d1
vlajek	vlajka	k1gFnPc2
řeší	řešit	k5eAaImIp3nS
Královský	královský	k2eAgInSc4d1
dekret	dekret	k1gInSc4
o	o	k7c4
vyvěšování	vyvěšování	k1gNnSc4
vlajek	vlajka	k1gFnPc2
na	na	k7c6
veřejných	veřejný	k2eAgFnPc6d1
budovách	budova	k1gFnPc6
z	z	k7c2
5	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1974	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
se	se	k3xPyFc4
veřejně	veřejně	k6eAd1
vyvěšuje	vyvěšovat	k5eAaImIp3nS
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
23	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
</s>
<s>
v	v	k7c4
den	den	k1gInSc4
voleb	volba	k1gFnPc2
do	do	k7c2
EP	EP	kA
</s>
<s>
Ministr	ministr	k1gMnSc1
vnitra	vnitro	k1gNnSc2
může	moct	k5eAaImIp3nS
nařídit	nařídit	k5eAaPmF
vlajkovou	vlajkový	k2eAgFnSc4d1
výzdobu	výzdoba	k1gFnSc4
i	i	k9
v	v	k7c6
jiných	jiný	k2eAgInPc6d1
dnech	den	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
národní	národní	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
vyvěšovány	vyvěšován	k2eAgFnPc1d1
jiné	jiný	k2eAgFnPc1d1
úřední	úřední	k2eAgFnPc1d1
vlajky	vlajka	k1gFnPc1
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
články	článek	k1gInPc4
tohoto	tento	k3xDgInSc2
královského	královský	k2eAgInSc2d1
dekretu	dekret	k1gInSc2
<g/>
,	,	kIx,
obřady	obřad	k1gInPc7
či	či	k8xC
místními	místní	k2eAgFnPc7d1
zvyklostmi	zvyklost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
oficiální	oficiální	k2eAgFnSc6d1
návštěvě	návštěva	k1gFnSc6
hlavy	hlava	k1gFnSc2
jiného	jiný	k2eAgInSc2d1
státu	stát	k1gInSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vyvěšena	vyvěsit	k5eAaPmNgFnS
i	i	k8xC
vlajka	vlajka	k1gFnSc1
tohoto	tento	k3xDgInSc2
státu	stát	k1gInSc2
a	a	k8xC
při	při	k7c6
oficiálních	oficiální	k2eAgInPc6d1
svátcích	svátek	k1gInPc6
mezinárodních	mezinárodní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
jichž	jenž	k3xRgFnPc2
je	být	k5eAaImIp3nS
Belgie	Belgie	k1gFnSc1
členem	člen	k1gInSc7
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vyvěšena	vyvěsit	k5eAaPmNgFnS
tato	tento	k3xDgFnSc1
vlajka	vlajka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
vyvěšována	vyvěšovat	k5eAaImNgFnS
také	také	k9
vlajka	vlajka	k1gFnSc1
jiného	jiný	k2eAgInSc2d1
státu	stát	k1gInSc2
a	a	k8xC
<g/>
/	/	kIx~
<g/>
nebo	nebo	k8xC
mezinárodní	mezinárodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pořadí	pořadí	k1gNnSc1
toto	tento	k3xDgNnSc1
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
národní	národní	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
národní	národní	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
jiného	jiný	k2eAgInSc2d1
státu	stát	k1gInSc2
a	a	k8xC
<g/>
/	/	kIx~
<g/>
nebo	nebo	k8xC
vlajka	vlajka	k1gFnSc1
organizace	organizace	k1gFnSc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
vlajka	vlajka	k1gFnSc1
společenství	společenství	k1gNnSc2
<g/>
/	/	kIx~
<g/>
regionu	region	k1gInSc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
vlajka	vlajka	k1gFnSc1
provincie	provincie	k1gFnSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
vlajka	vlajka	k1gFnSc1
obce	obec	k1gFnSc2
</s>
<s>
Pořadí	pořadí	k1gNnSc1
se	se	k3xPyFc4
posuzuje	posuzovat	k5eAaImIp3nS
při	při	k7c6
pohledu	pohled	k1gInSc6
z	z	k7c2
budovy	budova	k1gFnSc2
nebo	nebo	k8xC
tribuny	tribuna	k1gFnSc2
<g/>
,	,	kIx,
jakoby	jakoby	k8xS
pozorovatel	pozorovatel	k1gMnSc1
stál	stát	k5eAaImAgMnS
zády	záda	k1gNnPc7
k	k	k7c3
budově	budova	k1gFnSc3
nebo	nebo	k8xC
tribuně	tribuna	k1gFnSc3
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
jsou	být	k5eAaImIp3nP
vyvěšeny	vyvěsit	k5eAaPmNgFnP
vlajky	vlajka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
počet	počet	k1gInSc1
vlajek	vlajka	k1gFnPc2
lichý	lichý	k2eAgMnSc1d1
<g/>
,	,	kIx,
pořadí	pořadí	k1gNnSc1
je	být	k5eAaImIp3nS
5-3-1-2-4	5-3-1-2-4	k4
a	a	k8xC
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
sudý	sudý	k2eAgInSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pořadí	pořadí	k1gNnSc1
6	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevyvěšuje	vyvěšovat	k5eNaImIp3nS
<g/>
-li	-li	k?
se	se	k3xPyFc4
některá	některý	k3yIgFnSc1
z	z	k7c2
vlajek	vlajka	k1gFnPc2
<g/>
,	,	kIx,
odstraní	odstranit	k5eAaPmIp3nP
se	se	k3xPyFc4
vlajky	vlajka	k1gFnSc2
menšího	malý	k2eAgInSc2d2
významu	význam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
vlajky	vlajka	k1gFnPc1
vyvěšují	vyvěšovat	k5eAaImIp3nP
na	na	k7c4
stejný	stejný	k2eAgInSc4d1
stožár	stožár	k1gInSc4
<g/>
,	,	kIx,
počítá	počítat	k5eAaImIp3nS
se	se	k3xPyFc4
pořadí	pořadí	k1gNnSc1
vlajek	vlajka	k1gFnPc2
od	od	k7c2
vrcholu	vrchol	k1gInSc2
stožáru	stožár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vlajky	vlajka	k1gFnPc1
belgických	belgický	k2eAgInPc2d1
regionů	region	k1gInPc2
a	a	k8xC
provincií	provincie	k1gFnPc2
</s>
<s>
Belgie	Belgie	k1gFnSc1
je	být	k5eAaImIp3nS
federálním	federální	k2eAgInSc7d1
státem	stát	k1gInSc7
a	a	k8xC
skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
ze	z	k7c2
tří	tři	k4xCgInPc2
regionů	region	k1gInPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgFnPc6,k3yQgFnPc6,k3yRgFnPc6
žijí	žít	k5eAaImIp3nP
nizozemsky	nizozemsky	k6eAd1
mluvící	mluvící	k2eAgMnPc1d1
Vlámové	Vlám	k1gMnPc1
<g/>
,	,	kIx,
frankofonní	frankofonní	k2eAgMnPc1d1
Valoni	Valon	k1gMnPc1
a	a	k8xC
německá	německý	k2eAgFnSc1d1
komunita	komunita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
administrativně-správního	administrativně-správní	k2eAgInSc2d1
principu	princip	k1gInSc2
je	být	k5eAaImIp3nS
Belgie	Belgie	k1gFnSc1
členěna	členit	k5eAaImNgFnS
do	do	k7c2
3	#num#	k4
regionů	region	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlámský	vlámský	k2eAgInSc4d1
a	a	k8xC
Valonský	valonský	k2eAgInSc4d1
region	region	k1gInSc4
se	se	k3xPyFc4
dále	daleko	k6eAd2
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
provincie	provincie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viz	vidět	k5eAaImRp2nS
Administrativní	administrativní	k2eAgNnSc4d1
dělení	dělení	k1gNnSc4
Belgie	Belgie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Belgické	belgický	k2eAgInPc1d1
regiony	region	k1gInPc1
</s>
<s>
Belgické	belgický	k2eAgFnPc4d1
provincie	provincie	k1gFnPc4
</s>
<s>
Jazykové	jazykový	k2eAgNnSc1d1
členění	členění	k1gNnSc1
Belgie	Belgie	k1gFnSc2
</s>
<s>
Bruselský	bruselský	k2eAgInSc1d1
regionPoměr	regionPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
VlámskoPoměr	VlámskoPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
ValonskoPoměr	ValonskoPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Vlámské	vlámský	k2eAgFnPc4d1
provincie	provincie	k1gFnPc4
</s>
<s>
AntverpyPoměr	AntverpyPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Vlámský	vlámský	k2eAgInSc1d1
BrabantPoměr	BrabantPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Východní	východní	k2eAgInSc1d1
FlandryPoměr	FlandryPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Západní	západní	k2eAgInSc1d1
FlandryPoměr	FlandryPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
LimburkPoměr	LimburkPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Valonské	valonský	k2eAgFnPc4d1
provincie	provincie	k1gFnPc4
</s>
<s>
Valonský	valonský	k2eAgInSc1d1
BrabantPoměr	BrabantPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
~	~	kIx~
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
HenegavskoPoměr	HenegavskoPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
LutychPoměr	LutychPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
LucemburkPoměr	LucemburkPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
3	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
NamurPoměr	NamurPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.crwflags.com/fotw/flags/be.html	http://www.crwflags.com/fotw/flags/be.htmla	k1gFnPc2
Belgická	belgický	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
na	na	k7c4
Flags	Flags	k1gInSc4
Of	Of	k1gMnSc1
The	The	k1gMnSc1
World	World	k1gMnSc1
<g/>
↑	↑	k?
https://www.belgium.be/fr/la_belgique/connaitre_le_pays/la_belgique_en_bref/symboles/drapeaux/	https://www.belgium.be/fr/la_belgique/connaitre_le_pays/la_belgique_en_bref/symboles/drapeaux/	k?
<g/>
↑	↑	k?
Dny	den	k1gInPc1
vyvěšování	vyvěšování	k1gNnPc2
belbických	belbický	k2eAgFnPc2d1
vlajek	vlajka	k1gFnPc2
na	na	k7c4
Flags	Flags	k1gInSc4
Of	Of	k1gMnSc1
The	The	k1gMnSc1
World	World	k1gMnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
Belgie	Belgie	k1gFnSc2
</s>
<s>
Belgická	belgický	k2eAgFnSc1d1
hymna	hymna	k1gFnSc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Belgie	Belgie	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Belgická	belgický	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vlajky	vlajka	k1gFnSc2
evropských	evropský	k2eAgInPc2d1
států	stát	k1gInPc2
Nezávislé	závislý	k2eNgInPc4d1
státy	stát	k1gInPc4
</s>
<s>
Albánie	Albánie	k1gFnSc1
•	•	k?
Andorra	Andorra	k1gFnSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Belgie	Belgie	k1gFnSc1
•	•	k?
Bělorusko	Bělorusko	k1gNnSc1
•	•	k?
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
•	•	k?
Bulharsko	Bulharsko	k1gNnSc1
•	•	k?
Černá	černat	k5eAaImIp3nS
Hora	hora	k1gFnSc1
•	•	k?
Česko	Česko	k1gNnSc1
•	•	k?
Dánsko	Dánsko	k1gNnSc1
•	•	k?
Estonsko	Estonsko	k1gNnSc1
•	•	k?
Finsko	Finsko	k1gNnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Chorvatsko	Chorvatsko	k1gNnSc1
•	•	k?
Irsko	Irsko	k1gNnSc4
•	•	k?
Island	Island	k1gInSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
•	•	k?
Litva	Litva	k1gFnSc1
•	•	k?
Lotyšsko	Lotyšsko	k1gNnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Lucembursko	Lucembursko	k1gNnSc1
•	•	k?
Maďarsko	Maďarsko	k1gNnSc1
•	•	k?
Malta	Malta	k1gFnSc1
•	•	k?
Moldavsko	Moldavsko	k1gNnSc1
•	•	k?
Monako	Monako	k1gNnSc1
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Nizozemsko	Nizozemsko	k1gNnSc1
•	•	k?
Norsko	Norsko	k1gNnSc1
•	•	k?
Polsko	Polsko	k1gNnSc1
•	•	k?
Portugalsko	Portugalsko	k1gNnSc1
•	•	k?
Rakousko	Rakousko	k1gNnSc1
•	•	k?
Rumunsko	Rumunsko	k1gNnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Řecko	Řecko	k1gNnSc1
•	•	k?
San	San	k1gMnSc1
Marino	Marina	k1gFnSc5
•	•	k?
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
•	•	k?
Slovensko	Slovensko	k1gNnSc1
•	•	k?
Slovinsko	Slovinsko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Srbsko	Srbsko	k1gNnSc1
•	•	k?
Španělsko	Španělsko	k1gNnSc1
•	•	k?
Švédsko	Švédsko	k1gNnSc1
•	•	k?
Švýcarsko	Švýcarsko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc1
•	•	k?
Ukrajina	Ukrajina	k1gFnSc1
•	•	k?
Vatikán	Vatikán	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
a	a	k8xC
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Alandy	Aland	k1gInPc1
(	(	kIx(
<g/>
FIN	Fin	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Faerské	Faerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
DNK	DNK	kA
<g/>
)	)	kIx)
•	•	k?
Gibraltar	Gibraltar	k1gInSc1
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Grónsko	Grónsko	k1gNnSc4
(	(	kIx(
<g/>
DNK	DNK	kA
<g/>
)	)	kIx)
•	•	k?
Guernsey	Guernsea	k1gFnSc2
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Jersey	Jersea	k1gFnSc2
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Ostrov	ostrov	k1gInSc1
Man	Man	k1gMnSc1
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Republika	republika	k1gFnSc1
srbská	srbský	k2eAgFnSc1d1
(	(	kIx(
<g/>
BIH	BIH	kA
<g/>
)	)	kIx)
Státy	stát	k1gInPc7
bez	bez	k7c2
plného	plný	k2eAgNnSc2d1
uznání	uznání	k1gNnSc2
</s>
<s>
Abcházie	Abcházie	k1gFnSc1
•	•	k?
Arcach	Arcach	k1gInSc1
•	•	k?
Doněcká	doněcký	k2eAgFnSc1d1
lid	lid	k1gInSc1
<g/>
.	.	kIx.
rep	rep	k?
<g/>
.	.	kIx.
•	•	k?
Jižní	jižní	k2eAgFnSc2d1
Osetie	Osetie	k1gFnSc2
•	•	k?
Kosovo	Kosův	k2eAgNnSc1d1
•	•	k?
Luhanská	Luhanský	k2eAgFnSc1d1
lid	lid	k1gInSc1
<g/>
.	.	kIx.
rep	rep	k?
<g/>
.	.	kIx.
•	•	k?
Podněstří	Podněstří	k1gFnSc2
•	•	k?
Severní	severní	k2eAgInSc1d1
Kypr	Kypr	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Belgie	Belgie	k1gFnSc1
</s>
