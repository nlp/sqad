<s>
Děkanát	děkanát	k1gInSc1
Svitavy	Svitava	k1gFnSc2
</s>
<s>
Děkanát	děkanát	k1gInSc1
SvitavyDiecéze	SvitavyDiecéza	k1gFnSc3
</s>
<s>
Arcidiecéze	arcidiecéze	k1gFnSc1
olomoucká	olomoucký	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
Moravská	moravský	k2eAgFnSc1d1
Děkan	děkan	k1gMnSc1
</s>
<s>
Mgr.	Mgr.	kA
Václav	Václav	k1gMnSc1
Dolák	Dolák	k1gMnSc1
Další	další	k2eAgInSc1d1
úřad	úřad	k1gInSc1
děkana	děkan	k1gMnSc2
</s>
<s>
Farář	farář	k1gMnSc1
ve	v	k7c6
farnosti	farnost	k1gFnSc6
Svitavy	Svitava	k1gFnSc2
Údaje	údaj	k1gInSc2
v	v	k7c6
infoboxu	infobox	k1gInSc6
aktuální	aktuální	k2eAgFnSc2d1
k	k	k7c3
prosinci	prosinec	k1gInSc3
2017	#num#	k4
</s>
<s>
Děkanát	děkanát	k1gInSc1
Svitavy	Svitava	k1gFnSc2
je	být	k5eAaImIp3nS
územní	územní	k2eAgFnSc1d1
část	část	k1gFnSc1
Arcidiecéze	arcidiecéze	k1gFnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
ho	on	k3xPp3gNnSc2
24	#num#	k4
farností	farnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozloha	rozloha	k1gFnSc1
děknátu	děknát	k1gInSc2
je	být	k5eAaImIp3nS
650	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
59	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
počtu	počet	k1gInSc2
je	být	k5eAaImIp3nS
katolíků	katolík	k1gMnPc2
18	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
30,5	30,5	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
průměrná	průměrný	k2eAgFnSc1d1
účast	účast	k1gFnSc1
na	na	k7c6
nedělních	nedělní	k2eAgFnPc6d1
bohoslužbách	bohoslužba	k1gFnPc6
činí	činit	k5eAaImIp3nS
2000	#num#	k4
(	(	kIx(
<g/>
11,1	11,1	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
děkanátu	děkanát	k1gInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
68	#num#	k4
kostelů	kostel	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děkanem	děkan	k1gMnSc7
je	být	k5eAaImIp3nS
P.	P.	kA
Mgr.	Mgr.	kA
Václav	Václav	k1gMnSc1
Dolák	Dolák	k1gMnSc1
<g/>
,	,	kIx,
farář	farář	k1gMnSc1
ve	v	k7c6
Svitavách	Svitava	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
děkanátu	děkanát	k1gInSc6
působí	působit	k5eAaImIp3nS
8	#num#	k4
diecézních	diecézní	k2eAgMnPc2d1
kněží	kněz	k1gMnPc2
a	a	k8xC
4	#num#	k4
řeholní	řeholní	k2eAgMnPc1d1
kněží	kněz	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Znak	znak	k1gInSc1
děkanátu	děkanát	k1gInSc2
</s>
<s>
Erb	erb	k1gInSc1
děkanátu	děkanát	k1gInSc2
Svitavy	Svitava	k1gFnSc2
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
V	v	k7c6
červeném	červený	k2eAgInSc6d1
štítě	štít	k1gInSc6
stříbrná	stříbrnat	k5eAaImIp3nS
lilie	lilie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okraj	okraj	k1gInSc4
štítu	štít	k1gInSc2
stříbrný	stříbrný	k1gInSc1
pilovaný	pilovaný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
</s>
<s>
Biskup	biskup	k1gMnSc1
Bruno	Bruno	k1gMnSc1
ze	z	k7c2
Schauenburku	Schauenburk	k1gInSc2
připojil	připojit	k5eAaPmAgMnS
Svitavy	Svitava	k1gFnPc4
a	a	k8xC
okolí	okolí	k1gNnSc4
k	k	k7c3
olomoucké	olomoucký	k2eAgFnSc3d1
provincii	provincie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
erbu	erb	k1gInSc2
byl	být	k5eAaImAgInS
vzat	vzat	k2eAgInSc1d1
červený	červený	k2eAgInSc1d1
štít	štít	k1gInSc1
s	s	k7c7
pilovitým	pilovitý	k2eAgInSc7d1
okrajem	okraj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stříbrná	stříbrný	k2eAgFnSc1d1
lilie	lilie	k1gFnSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
zasvěcení	zasvěcení	k1gNnSc4
svitavského	svitavský	k2eAgInSc2d1
děkanského	děkanský	k2eAgInSc2d1
chrámu	chrám	k1gInSc2
Panně	Panna	k1gFnSc3
Marii	Maria	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Farnosti	farnost	k1gFnPc1
</s>
<s>
FarnostSprávceFarní	FarnostSprávceFarní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
</s>
<s>
Biskupice	Biskupice	k1gFnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
Jaroměřice	Jaroměřice	k1gFnPc1
u	u	k7c2
Jevíčka	Jevíčko	k1gNnSc2
</s>
<s>
sv.	sv.	kA
Anny	Anna	k1gFnPc1
</s>
<s>
Borotín	Borotín	k1gInSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
P.	P.	kA
Krzysztof	Krzysztof	k1gInSc1
Kuklinski	Kuklinski	k1gNnSc2
CP	CP	kA
</s>
<s>
Povýšení	povýšení	k1gNnSc1
svatého	svatý	k2eAgMnSc2d1
Kříže	Kříž	k1gMnSc2
</s>
<s>
Boršov	Boršov	k1gInSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
-	-	kIx~
P.	P.	kA
Mgr.	Mgr.	kA
Ing.	ing.	kA
Miroslav	Miroslav	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
Kubík	Kubík	k1gMnSc1
<g/>
,	,	kIx,
OFM	OFM	kA
z	z	k7c2
Moravské	moravský	k2eAgFnSc2d1
Třebové	Třebová	k1gFnSc2
</s>
<s>
sv.	sv.	kA
Anny	Anna	k1gFnPc1
</s>
<s>
Březová	březový	k2eAgFnSc1d1
nad	nad	k7c7
Svitavou	Svitava	k1gFnSc7
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
Radiměř	Radiměř	k1gFnSc4
</s>
<s>
sv.	sv.	kA
Bartoloměje	Bartoloměj	k1gMnSc4
</s>
<s>
Deštná	deštný	k2eAgFnSc1d1
u	u	k7c2
Velkých	velký	k2eAgFnPc2d1
Opatovic	Opatovice	k1gFnPc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
Velké	velký	k2eAgFnSc2d1
Opatovice	Opatovice	k1gFnSc2
</s>
<s>
sv.	sv.	kA
Petra	Petr	k1gMnSc4
a	a	k8xC
Pavla	Pavel	k1gMnSc4
</s>
<s>
Horní	horní	k2eAgFnSc1d1
Hynčina	Hynčina	k1gFnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
Radiměř	Radiměř	k1gFnSc4
</s>
<s>
sv.	sv.	kA
Mikuláše	Mikuláš	k1gMnSc4
</s>
<s>
Hradec	Hradec	k1gInSc1
nad	nad	k7c7
Svitavou	Svitava	k1gFnSc7
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
Svitavy	Svitava	k1gFnSc2
</s>
<s>
sv.	sv.	kA
Kateřiny	Kateřina	k1gFnPc1
</s>
<s>
Chornice	Chornice	k1gFnPc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
Jaroměřice	Jaroměřice	k1gFnPc1
u	u	k7c2
Jevíčka	Jevíčko	k1gNnSc2
</s>
<s>
sv.	sv.	kA
Vavřinec	Vavřinec	k1gMnSc1
</s>
<s>
Jaroměřice	Jaroměřice	k1gFnPc1
u	u	k7c2
Jevíčka	Jevíčko	k1gNnSc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
-	-	kIx~
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Pawel	Pawel	k1gMnSc1
Wójcik	Wójcik	k1gMnSc1
</s>
<s>
Povýšení	povýšení	k1gNnSc1
svatého	svatý	k2eAgMnSc2d1
Kříže	Kříž	k1gMnSc2
</s>
<s>
Jevíčko	Jevíčko	k1gNnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
-	-	kIx~
R.	R.	kA
D.	D.	kA
ThLic	ThLic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Andrzej	Andrzej	k1gMnSc1
Sebastian	Sebastian	k1gMnSc1
Walak	Walak	k1gMnSc1
</s>
<s>
Nanebevzetí	nanebevzetí	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Křenov	Křenov	k1gInSc1
</s>
<s>
farář	farář	k1gMnSc1
-	-	kIx~
Krzysztof	Krzysztof	k1gMnSc1
Kuklinski	Kuklinsk	k1gFnSc2
CP	CP	kA
</s>
<s>
sv.	sv.	kA
Jana	Jan	k1gMnSc4
Křtitele	křtitel	k1gMnSc4
</s>
<s>
Kunčina	Kunčin	k2eAgFnSc1d1
</s>
<s>
administrátor	administrátor	k1gMnSc1
-	-	kIx~
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Karel	Karel	k1gMnSc1
Macků	Macků	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Jiří	Jiří	k1gMnSc1
</s>
<s>
Městečko	městečko	k1gNnSc1
Trnávka	Trnávka	k1gFnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
Svitavy	Svitava	k1gFnSc2
</s>
<s>
sv.	sv.	kA
Jakuba	Jakub	k1gMnSc4
staršího	starší	k1gMnSc4
</s>
<s>
Mladějov	Mladějov	k1gInSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
Rychnov	Rychnov	k1gInSc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
</s>
<s>
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
</s>
<s>
Moravská	moravský	k2eAgFnSc1d1
Třebová	Třebová	k1gFnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
-	-	kIx~
P.	P.	kA
Mgr.	Mgr.	kA
Šebestián	Šebestián	k1gMnSc1
Pavel	Pavel	k1gMnSc1
Smrčina	smrčina	k1gFnSc1
<g/>
,	,	kIx,
OFM	OFM	kA
</s>
<s>
Nanebevzetí	nanebevzetí	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Radiměř	Radiměř	k1gFnSc1
</s>
<s>
farář	farář	k1gMnSc1
-	-	kIx~
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Pavel	Pavel	k1gMnSc1
Michut	Michut	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Anny	Anna	k1gFnPc1
</s>
<s>
Roubanina	Roubanina	k1gFnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
Velké	velký	k2eAgFnSc2d1
Opatovice	Opatovice	k1gFnSc2
</s>
<s>
sv.	sv.	kA
Ondřej	Ondřej	k1gMnSc1
</s>
<s>
Rychnov	Rychnov	k1gInSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
</s>
<s>
administrátor	administrátor	k1gMnSc1
-	-	kIx~
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Pavel	Pavel	k1gMnSc1
Jagoš	Jagoš	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Mikuláše	Mikuláš	k1gMnSc4
</s>
<s>
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
u	u	k7c2
Moravské	moravský	k2eAgFnSc2d1
Třebové	Třebová	k1gFnSc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
-	-	kIx~
Bradna	Bradna	k1gFnSc1
Pavel	Pavel	k1gMnSc1
Lukáš	Lukáš	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
</s>
<s>
sv.	sv.	kA
Kateřiny	Kateřina	k1gFnPc1
</s>
<s>
Svitavy	Svitava	k1gFnPc1
</s>
<s>
farář	farář	k1gMnSc1
-	-	kIx~
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Václav	Václav	k1gMnSc1
Dolák	Dolák	k1gMnSc1
</s>
<s>
Navštívení	navštívení	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Třebařov	Třebařov	k1gInSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
Rychnov	Rychnov	k1gInSc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
</s>
<s>
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
</s>
<s>
Velké	velký	k2eAgFnPc1d1
Opatovice	Opatovice	k1gFnPc1
</s>
<s>
farář	farář	k1gMnSc1
-	-	kIx~
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Miroslav	Miroslav	k1gMnSc1
Hřib	hřib	k1gInSc1
</s>
<s>
sv.	sv.	kA
Jiří	Jiří	k1gMnSc1
</s>
<s>
Vendolí	Vendolit	k5eAaPmIp3nS,k5eAaImIp3nS
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
Svitavy	Svitava	k1gFnSc2
</s>
<s>
sv.	sv.	kA
Ondřeje	Ondřej	k1gMnSc4
</s>
<s>
Vranová	Vranový	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
Svitavy	Svitava	k1gFnSc2
</s>
<s>
sv.	sv.	kA
Kateřiny	Kateřina	k1gFnPc1
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
svatých	svatý	k1gMnPc2
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
<g/>
,	,	kIx,
Biskupice	Biskupic	k1gMnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
Povýšení	povýšení	k1gNnSc2
svatého	svatý	k2eAgInSc2d1
Kříže	kříž	k1gInSc2
na	na	k7c4
Kalvárii	Kalvárie	k1gFnSc4
<g/>
,	,	kIx,
Jaroměřice	Jaroměřice	k1gFnPc4
</s>
<s>
Augustiniánský	augustiniánský	k2eAgInSc1d1
klášter	klášter	k1gInSc1
<g/>
,	,	kIx,
za	za	k7c7
ním	on	k3xPp3gInSc7
kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc6
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
Jevíčko	Jevíčko	k1gNnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Jiří	Jiří	k1gMnSc2
<g/>
,	,	kIx,
Velké	velká	k1gFnSc2
Opatovice	Opatovice	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Děkanát	děkanát	k1gInSc1
Svitavy	Svitava	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
ado	ado	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
KREJSA	Krejsa	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katalog	katalog	k1gInSc1
olomoucké	olomoucký	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
olomoucké	olomoucký	k2eAgNnSc1d1
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Děkanát	děkanát	k1gInSc4
Svitavy	Svitava	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
218	#num#	k4
<g/>
–	–	k?
<g/>
237	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Katalog	katalog	k1gInSc1
arcidiecéze	arcidiecéze	k1gFnSc2
olomoucké	olomoucký	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
olomoucké	olomoucký	k2eAgNnSc1d1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Děkanát	děkanát	k1gInSc4
Svitavy	Svitava	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
207	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Farnosti	farnost	k1gFnSc2
Děkanátu	děkanát	k1gInSc2
Svitavy	Svitava	k1gFnSc2
</s>
<s>
Biskupice	Biskupice	k1gFnSc1
•	•	k?
Borotín	Borotína	k1gFnPc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
•	•	k?
Boršov	Boršov	k1gInSc1
•	•	k?
Březová	březový	k2eAgFnSc1d1
nad	nad	k7c7
Svitavou	Svitava	k1gFnSc7
•	•	k?
Deštná	deštný	k2eAgFnSc1d1
u	u	k7c2
Velkých	velký	k2eAgFnPc2d1
Opatovic	Opatovice	k1gFnPc2
•	•	k?
Horní	horní	k2eAgFnSc1d1
Hynčina	Hynčina	k1gFnSc1
•	•	k?
Hradec	Hradec	k1gInSc1
nad	nad	k7c7
Svitavou	Svitava	k1gFnSc7
•	•	k?
Chornice	Chornice	k1gFnPc4
•	•	k?
Jaroměřice	Jaroměřice	k1gFnPc4
u	u	k7c2
Jevíčka	Jevíčko	k1gNnSc2
•	•	k?
Jevíčko	Jevíčko	k1gNnSc1
•	•	k?
Křenov	Křenovo	k1gNnPc2
•	•	k?
Kunčina	Kunčin	k2eAgMnSc4d1
•	•	k?
Městečko	městečko	k1gNnSc1
Trnávka	Trnávka	k1gFnSc1
•	•	k?
Mladějov	Mladějov	k1gInSc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
•	•	k?
Moravská	moravský	k2eAgFnSc1d1
Třebová	Třebová	k1gFnSc1
•	•	k?
Radiměř	Radiměř	k1gInSc1
•	•	k?
Roubanina	Roubanina	k1gFnSc1
•	•	k?
Rychnov	Rychnov	k1gInSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
•	•	k?
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
u	u	k7c2
Moravské	moravský	k2eAgFnSc2d1
Třebové	Třebová	k1gFnSc2
•	•	k?
Svitavy	Svitava	k1gFnSc2
•	•	k?
Třebařov	Třebařov	k1gInSc1
•	•	k?
<g/>
Velké	velká	k1gFnSc2
Opatovice	Opatovice	k1gFnSc1
•	•	k?
Vendolí	Vendolí	k1gNnSc2
•	•	k?
Vranová	Vranový	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
</s>
<s>
Děkanáty	děkanát	k1gInPc1
Arcidiecéze	arcidiecéze	k1gFnSc2
olomoucké	olomoucký	k2eAgFnSc2d1
</s>
<s>
Holešov	Holešov	k1gInSc1
•	•	k?
Hranice	hranice	k1gFnSc2
•	•	k?
Konice	Konice	k1gFnSc2
•	•	k?
Kroměříž	Kroměříž	k1gFnSc1
•	•	k?
Kyjov	Kyjov	k1gInSc1
•	•	k?
Olomouc	Olomouc	k1gFnSc1
•	•	k?
Prostějov	Prostějov	k1gInSc1
•	•	k?
Přerov	Přerov	k1gInSc1
•	•	k?
Svitavy	Svitava	k1gFnSc2
•	•	k?
Šternberk	Šternberk	k1gInSc1
•	•	k?
Šumperk	Šumperk	k1gInSc1
•	•	k?
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
•	•	k?
Uherský	uherský	k2eAgInSc1d1
Brod	Brod	k1gInSc1
•	•	k?
Valašské	valašský	k2eAgInPc1d1
Klobouky	Klobouky	k1gInPc1
•	•	k?
Valašské	valašský	k2eAgNnSc1d1
Meziříčí	Meziříčí	k1gNnSc1
•	•	k?
Veselí	veselí	k1gNnSc1
nad	nad	k7c7
Moravou	Morava	k1gFnSc7
•	•	k?
Vizovice	Vizovice	k1gFnPc4
•	•	k?
Vsetín	Vsetín	k1gInSc1
•	•	k?
Vyškov	Vyškov	k1gInSc1
•	•	k?
Zábřeh	Zábřeh	k1gInSc1
•	•	k?
Zlín	Zlín	k1gInSc1
</s>
