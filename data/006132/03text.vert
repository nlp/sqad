<s>
Rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgNnSc2d1	jiné
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
je	být	k5eAaImIp3nS	být
definována	definován	k2eAgFnSc1d1	definována
jako	jako	k8xC	jako
fázová	fázový	k2eAgFnSc1d1	fázová
rychlost	rychlost	k1gFnSc1	rychlost
postupného	postupný	k2eAgNnSc2d1	postupné
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
vlnění	vlnění	k1gNnSc2	vlnění
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soustavách	soustava	k1gFnPc6	soustava
jednotek	jednotka	k1gFnPc2	jednotka
založených	založený	k2eAgFnPc2d1	založená
na	na	k7c6	na
metrickém	metrický	k2eAgInSc6d1	metrický
systému	systém	k1gInSc6	systém
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
i	i	k9	i
SI	si	k1gNnSc2	si
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
definici	definice	k1gFnSc3	definice
metru	metr	k1gInSc2	metr
přesnou	přesný	k2eAgFnSc4d1	přesná
hodnotu	hodnota	k1gFnSc4	hodnota
299	[number]	k4	299
792	[number]	k4	792
458	[number]	k4	458
metrů	metr	k1gInPc2	metr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
(	(	kIx(	(
<g/>
1	[number]	k4	1
079	[number]	k4	079
252	[number]	k4	252
848,8	[number]	k4	848,8
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
písmenem	písmeno	k1gNnSc7	písmeno
c	c	k0	c
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
celeritas	celeritas	k1gMnSc1	celeritas
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
constans	constansa	k1gFnPc2	constansa
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgFnSc1d1	znamenající
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rychlost	rychlost	k1gFnSc1	rychlost
dává	dávat	k5eAaImIp3nS	dávat
přirozený	přirozený	k2eAgInSc4d1	přirozený
poměr	poměr	k1gInSc4	poměr
měřítek	měřítko	k1gNnPc2	měřítko
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
času	čas	k1gInSc2	čas
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
možnou	možný	k2eAgFnSc7d1	možná
rychlostí	rychlost	k1gFnSc7	rychlost
šíření	šíření	k1gNnSc2	šíření
signálu	signál	k1gInSc2	signál
či	či	k8xC	či
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Například	například	k6eAd1
světlo	světlo	k1gNnSc1
by	by	kYmCp3nS
oběhlo	oběhnout	k5eAaPmAgNnS
Zeměkouli	zeměkoule	k1gFnSc4
kolem	kolem	k7c2
rovníku	rovník	k1gInSc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
asi	asi	k9
7,5	7,5	k4
krát	krát	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
látkovém	látkový	k2eAgNnSc6d1	látkové
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mikroskopickém	mikroskopický	k2eAgInSc6d1	mikroskopický
popisu	popis	k1gInSc6	popis
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xS	jako
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
c	c	k0	c
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
světlo	světlo	k1gNnSc1	světlo
kvantově	kvantově	k6eAd1	kvantově
interaguje	interagovat	k5eAaBmIp3nS	interagovat
s	s	k7c7	s
částicemi	částice	k1gFnPc7	částice
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
makroskopickém	makroskopický	k2eAgInSc6d1	makroskopický
popisu	popis	k1gInSc6	popis
jeví	jevit	k5eAaImIp3nS	jevit
jeho	jeho	k3xOp3gFnSc1	jeho
fázová	fázový	k2eAgFnSc1d1	fázová
rychlost	rychlost	k1gFnSc1	rychlost
pomalejší	pomalý	k2eAgFnSc1d2	pomalejší
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
šíření	šíření	k1gNnSc2	šíření
světla	světlo	k1gNnSc2	světlo
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
rovna	roven	k2eAgFnSc1d1	rovna
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
/	/	kIx~	/
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
index	index	k1gInSc4	index
lomu	lom	k1gInSc2	lom
příslušné	příslušný	k2eAgFnSc2d1	příslušná
látky	látka	k1gFnSc2	látka
(	(	kIx(	(
<g/>
materiálu	materiál	k1gInSc2	materiál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
standardní	standardní	k2eAgFnSc2d1	standardní
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
teorie	teorie	k1gFnSc2	teorie
se	se	k3xPyFc4	se
všechno	všechen	k3xTgNnSc1	všechen
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
šíří	šířit	k5eAaImIp3nS	šířit
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
konstantní	konstantní	k2eAgFnSc7d1	konstantní
rychlostí	rychlost	k1gFnSc7	rychlost
všeobecně	všeobecně	k6eAd1	všeobecně
známou	známý	k2eAgFnSc7d1	známá
jako	jako	k8xS	jako
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
konstanta	konstanta	k1gFnSc1	konstanta
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
písmenem	písmeno	k1gNnSc7	písmeno
c.	c.	k?	c.
Rychlostí	rychlost	k1gFnPc2	rychlost
c	c	k0	c
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
také	také	k9	také
gravitace	gravitace	k1gFnSc1	gravitace
v	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Zákony	zákon	k1gInPc1	zákon
elektromagnetismu	elektromagnetismus	k1gInSc2	elektromagnetismus
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Maxwellovy	Maxwellův	k2eAgFnPc1d1	Maxwellova
rovnice	rovnice	k1gFnPc1	rovnice
<g/>
)	)	kIx)	)
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
c	c	k0	c
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
objektu	objekt	k1gInSc2	objekt
vyzařujícího	vyzařující	k2eAgNnSc2d1	vyzařující
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
například	například	k6eAd1	například
světlo	světlo	k1gNnSc1	světlo
vyzařující	vyzařující	k2eAgFnSc1d1	vyzařující
z	z	k7c2	z
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
pohybujícího	pohybující	k2eAgInSc2d1	pohybující
zdroje	zdroj	k1gInSc2	zdroj
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
stejnou	stejná	k1gFnSc7	stejná
rychlosti	rychlost	k1gFnSc2	rychlost
jako	jako	k8xC	jako
světlo	světlo	k1gNnSc4	světlo
vyzařované	vyzařovaný	k2eAgNnSc4d1	vyzařované
ze	z	k7c2	z
statického	statický	k2eAgInSc2d1	statický
zdroje	zdroj	k1gInSc2	zdroj
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
podle	podle	k7c2	podle
relativistického	relativistický	k2eAgInSc2d1	relativistický
Dopplerova	Dopplerův	k2eAgInSc2d1	Dopplerův
jevu	jev	k1gInSc2	jev
se	se	k3xPyFc4	se
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
frekvence	frekvence	k1gFnSc1	frekvence
<g/>
,	,	kIx,	,
energie	energie	k1gFnSc1	energie
a	a	k8xC	a
hybnost	hybnost	k1gFnSc1	hybnost
světla	světlo	k1gNnSc2	světlo
změní	změnit	k5eAaPmIp3nS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
zkombinuje	zkombinovat	k5eAaPmIp3nS	zkombinovat
pozorování	pozorování	k1gNnSc1	pozorování
s	s	k7c7	s
principem	princip	k1gInSc7	princip
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
naměří	naměřit	k5eAaBmIp3nP	naměřit
shodnou	shodný	k2eAgFnSc4d1	shodná
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
,	,	kIx,	,
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
nebo	nebo	k8xC	nebo
rychlosti	rychlost	k1gFnPc4	rychlost
objektu	objekt	k1gInSc2	objekt
vyzařujícího	vyzařující	k2eAgInSc2d1	vyzařující
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
na	na	k7c4	na
c	c	k0	c
může	moct	k5eAaImIp3nS	moct
nahlížet	nahlížet	k5eAaImF	nahlížet
jako	jako	k9	jako
na	na	k7c4	na
fyzikální	fyzikální	k2eAgFnSc4d1	fyzikální
konstantu	konstanta	k1gFnSc4	konstanta
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
poznamenat	poznamenat	k5eAaPmF	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
základem	základ	k1gInSc7	základ
speciální	speciální	k2eAgFnSc2d1	speciální
relativity	relativita	k1gFnSc2	relativita
je	být	k5eAaImIp3nS	být
konstanta	konstanta	k1gFnSc1	konstanta
c	c	k0	c
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
samotné	samotný	k2eAgNnSc1d1	samotné
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
světlo	světlo	k1gNnSc1	světlo
nějak	nějak	k6eAd1	nějak
upraveno	upravit	k5eAaPmNgNnS	upravit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
šířilo	šířit	k5eAaImAgNnS	šířit
rychlosti	rychlost	k1gFnSc3	rychlost
menší	malý	k2eAgFnSc3d2	menší
nebo	nebo	k8xC	nebo
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
c	c	k0	c
<g/>
,	,	kIx,	,
tak	tak	k9	tak
to	ten	k3xDgNnSc4	ten
přímo	přímo	k6eAd1	přímo
neovlivní	ovlivnit	k5eNaPmIp3nP	ovlivnit
speciální	speciální	k2eAgFnSc4d1	speciální
teorii	teorie	k1gFnSc4	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
cestující	cestující	k2eAgFnSc1d1	cestující
velkými	velký	k2eAgFnPc7d1	velká
rychlostmi	rychlost	k1gFnPc7	rychlost
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
a	a	k8xC	a
časy	čas	k1gInPc4	čas
jsou	být	k5eAaImIp3nP	být
zdeformované	zdeformovaný	k2eAgInPc1d1	zdeformovaný
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
dilatované	dilatovaný	k2eAgNnSc1d1	dilatovaný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
Lorentzovými	Lorentzův	k2eAgFnPc7d1	Lorentzova
transformacemi	transformace	k1gFnPc7	transformace
<g/>
.	.	kIx.	.
</s>
<s>
Transformace	transformace	k1gFnSc1	transformace
ale	ale	k8xC	ale
deformují	deformovat	k5eAaImIp3nP	deformovat
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
a	a	k8xC	a
časy	čas	k1gInPc4	čas
takovým	takový	k3xDgInSc7	takový
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
vůči	vůči	k7c3	vůči
nim	on	k3xPp3gInPc3	on
konstantní	konstantní	k2eAgFnPc1d1	konstantní
<g/>
.	.	kIx.	.
</s>
<s>
Osoba	osoba	k1gFnSc1	osoba
cestující	cestující	k1gFnSc2	cestující
rychlostí	rychlost	k1gFnSc7	rychlost
blízkou	blízký	k2eAgFnSc7d1	blízká
rychlosti	rychlost	k1gFnSc3	rychlost
světla	světlo	k1gNnSc2	světlo
by	by	kYmCp3nS	by
viděla	vidět	k5eAaImAgFnS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
barva	barva	k1gFnSc1	barva
světla	světlo	k1gNnSc2	světlo
vpředu	vpříst	k5eAaPmIp1nS	vpříst
(	(	kIx(	(
<g/>
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
pohybu	pohyb	k1gInSc2	pohyb
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
modrý	modrý	k2eAgInSc4d1	modrý
posuv	posuv	k1gInSc4	posuv
a	a	k8xC	a
barva	barva	k1gFnSc1	barva
vzadu	vzadu	k6eAd1	vzadu
rudý	rudý	k2eAgInSc4d1	rudý
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
informace	informace	k1gFnSc1	informace
mohla	moct	k5eAaImAgFnS	moct
šířit	šířit	k5eAaImF	šířit
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
c	c	k0	c
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
by	by	kYmCp3nS	by
porušena	porušen	k2eAgFnSc1d1	porušena
kauzalita	kauzalita	k1gFnSc1	kauzalita
<g/>
:	:	kIx,	:
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
vztažných	vztažný	k2eAgFnPc6d1	vztažná
soustavách	soustava	k1gFnPc6	soustava
by	by	kYmCp3nP	by
informace	informace	k1gFnSc1	informace
byla	být	k5eAaImAgFnS	být
doručena	doručit	k5eAaPmNgFnS	doručit
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
vyslána	vyslat	k5eAaPmNgFnS	vyslat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
příčina	příčina	k1gFnSc1	příčina
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
až	až	k9	až
po	po	k7c6	po
následku	následek	k1gInSc6	následek
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
dilataci	dilatace	k1gFnSc3	dilatace
času	čas	k1gInSc2	čas
podle	podle	k7c2	podle
speciální	speciální	k2eAgFnSc2d1	speciální
relativity	relativita	k1gFnSc2	relativita
se	se	k3xPyFc4	se
poměr	poměr	k1gInSc1	poměr
mezi	mezi	k7c7	mezi
časem	časem	k6eAd1	časem
vnímaným	vnímaný	k2eAgMnSc7d1	vnímaný
vnějším	vnější	k2eAgMnSc7d1	vnější
pozorovatelem	pozorovatel	k1gMnSc7	pozorovatel
a	a	k8xC	a
časem	čas	k1gInSc7	čas
vnímaným	vnímaný	k2eAgInSc7d1	vnímaný
pozorovatelem	pozorovatel	k1gMnSc7	pozorovatel
pohybujícím	pohybující	k2eAgMnSc7d1	pohybující
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
blízko	blízko	k7c2	blízko
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
blíží	blížit	k5eAaImIp3nS	blížit
k	k	k7c3	k
nule	nula	k1gFnSc3	nula
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
něco	něco	k3yInSc1	něco
mohlo	moct	k5eAaImAgNnS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
poměr	poměr	k1gInSc1	poměr
by	by	kYmCp3nS	by
nebyl	být	k5eNaImAgInS	být
reálným	reálný	k2eAgNnSc7d1	reálné
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgNnSc1d1	podobné
porušení	porušení	k1gNnSc1	porušení
kauzality	kauzalita	k1gFnSc2	kauzalita
nebylo	být	k5eNaImAgNnS	být
nikdy	nikdy	k6eAd1	nikdy
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
informace	informace	k1gFnSc1	informace
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
do	do	k7c2	do
a	a	k8xC	a
z	z	k7c2	z
bodů	bod	k1gInPc2	bod
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
definovaných	definovaný	k2eAgFnPc2d1	definovaná
světelným	světelný	k2eAgInSc7d1	světelný
kuželem	kužel	k1gInSc7	kužel
<g/>
.	.	kIx.	.
</s>
<s>
Interval	interval	k1gInSc1	interval
AB	AB	kA	AB
na	na	k7c6	na
diagramu	diagram	k1gInSc6	diagram
vpravo	vpravo	k6eAd1	vpravo
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
časový	časový	k2eAgInSc4d1	časový
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tu	tu	k6eAd1	tu
máme	mít	k5eAaImIp1nP	mít
soustavu	soustava	k1gFnSc4	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
událost	událost	k1gFnSc1	událost
A	a	k9	a
a	a	k8xC	a
událost	událost	k1gFnSc4	událost
B	B	kA	B
nastávají	nastávat	k5eAaImIp3nP	nastávat
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
jen	jen	k9	jen
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
A	a	k9	a
předchází	předcházet	k5eAaImIp3nS	předcházet
B	B	kA	B
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
soustavě	soustava	k1gFnSc6	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
A	a	k9	a
předchází	předcházet	k5eAaImIp3nS	předcházet
B	B	kA	B
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
soustavách	soustava	k1gFnPc6	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
.	.	kIx.	.
</s>
<s>
Hypoteticky	hypoteticky	k6eAd1	hypoteticky
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přemísťování	přemísťování	k1gNnSc1	přemísťování
hmoty	hmota	k1gFnSc2	hmota
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
informace	informace	k1gFnPc4	informace
<g/>
)	)	kIx)	)
z	z	k7c2	z
A	A	kA	A
do	do	k7c2	do
B	B	kA	B
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
zde	zde	k6eAd1	zde
nastávat	nastávat	k5eAaImF	nastávat
příčinný	příčinný	k2eAgInSc4d1	příčinný
vztah	vztah	k1gInSc4	vztah
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
A	a	k9	a
je	být	k5eAaImIp3nS	být
příčina	příčina	k1gFnSc1	příčina
a	a	k8xC	a
B	B	kA	B
je	být	k5eAaImIp3nS	být
následek	následek	k1gInSc4	následek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Interval	interval	k1gInSc1	interval
AC	AC	kA	AC
v	v	k7c6	v
diagramu	diagram	k1gInSc6	diagram
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
prostorový	prostorový	k2eAgMnSc1d1	prostorový
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
máme	mít	k5eAaImIp1nP	mít
soustavu	soustava	k1gFnSc4	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
událost	událost	k1gFnSc1	událost
A	a	k9	a
a	a	k8xC	a
událost	událost	k1gFnSc1	událost
C	C	kA	C
staly	stát	k5eAaPmAgInP	stát
současně	současně	k6eAd1	současně
<g/>
,	,	kIx,	,
oddělené	oddělený	k2eAgInPc4d1	oddělený
jen	jen	k9	jen
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
zde	zde	k6eAd1	zde
existují	existovat	k5eAaImIp3nP	existovat
souřadnicové	souřadnicový	k2eAgInPc4d1	souřadnicový
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgMnPc6	který
A	a	k9	a
předchází	předcházet	k5eAaImIp3nP	předcházet
C	C	kA	C
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
vyznačeno	vyznačen	k2eAgNnSc1d1	vyznačeno
<g/>
)	)	kIx)	)
a	a	k8xC	a
souřadnicové	souřadnicový	k2eAgInPc4d1	souřadnicový
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
C	C	kA	C
předchází	předcházet	k5eAaImIp3nS	předcházet
A	a	k9	a
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
cestování	cestování	k1gNnSc2	cestování
nadsvětelnou	nadsvětelna	k1gFnSc7	nadsvětelna
rychlosti	rychlost	k1gFnSc2	rychlost
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
žádné	žádný	k3yNgNnSc4	žádný
těleso	těleso	k1gNnSc4	těleso
(	(	kIx(	(
<g/>
ani	ani	k8xC	ani
informaci	informace	k1gFnSc4	informace
<g/>
)	)	kIx)	)
možné	možný	k2eAgInPc1d1	možný
cestovat	cestovat	k5eAaImF	cestovat
z	z	k7c2	z
A	A	kA	A
<g />
.	.	kIx.	.
</s>
<s>
do	do	k7c2	do
C	C	kA	C
nebo	nebo	k8xC	nebo
z	z	k7c2	z
C	C	kA	C
do	do	k7c2	do
A.	A.	kA	A.
Proto	proto	k8xC	proto
nemůže	moct	k5eNaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
žádná	žádný	k3yNgFnSc1	žádný
příčinná	příčinný	k2eAgFnSc1d1	příčinná
souvislost	souvislost	k1gFnSc1	souvislost
mezi	mezi	k7c7	mezi
A	A	kA	A
a	a	k8xC	a
C.	C.	kA	C.
Podle	podle	k7c2	podle
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
běžné	běžný	k2eAgFnSc2d1	běžná
definice	definice	k1gFnSc2	definice
<g/>
,	,	kIx,	,
přijaté	přijatý	k2eAgFnSc2d1	přijatá
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
přesně	přesně	k6eAd1	přesně
299	[number]	k4	299
792	[number]	k4	792
458	[number]	k4	458
metrů	metr	k1gInPc2	metr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
108	[number]	k4	108
metrů	metr	k1gInPc2	metr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
nebo	nebo	k8xC	nebo
30	[number]	k4	30
centimetrů	centimetr	k1gInPc2	centimetr
(	(	kIx(	(
<g/>
1	[number]	k4	1
stopa	stopa	k1gFnSc1	stopa
<g/>
)	)	kIx)	)
za	za	k7c4	za
nanosekundu	nanosekunda	k1gFnSc4	nanosekunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
definuje	definovat	k5eAaBmIp3nS	definovat
permitivitu	permitivita	k1gFnSc4	permitivita
vakua	vakuum	k1gNnSc2	vakuum
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
)	)	kIx)	)
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
SI	si	k1gNnSc2	si
jako	jako	k8xC	jako
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
10	[number]	k4	10
:	:	kIx,	:
7	[number]	k4	7
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
4	[number]	k4	4
π	π	k?	π
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
k	k	k7c3	k
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
−	−	k?	−
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
n	n	k0	n
e	e	k0	e
b	b	k?	b
o	o	k7c4	o
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
quad	quad	k1gInSc1	quad
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
v	v	k7c6	v
<g/>
~	~	kIx~	~
<g/>
A	a	k9	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
kg	kg	kA	kg
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
m	m	kA	m
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
nebo	nebo	k8xC	nebo
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
F	F	kA	F
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
m	m	kA	m
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
}	}	kIx)	}
:	:	kIx,	:
permeabilita	permeabilita	k1gFnSc1	permeabilita
vakua	vakuum	k1gNnSc2	vakuum
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
)	)	kIx)	)
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c4	na
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
a	a	k8xC	a
v	v	k7c6	v
SI	si	k1gNnSc6	si
jednotkách	jednotka	k1gFnPc6	jednotka
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
4	[number]	k4	4
:	:	kIx,	:
π	π	k?	π
:	:	kIx,	:
:	:	kIx,	:
10	[number]	k4	10
:	:	kIx,	:
−	−	k?	−
7	[number]	k4	7
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
v	v	k7c6	v
:	:	kIx,	:
k	k	k7c3	k
g	g	kA	g
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
−	−	k?	−
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
−	−	k?	−
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
n	n	k0	n
e	e	k0	e
b	b	k?	b
o	o	k7c4	o
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
−	−	k?	−
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
,10	,10	k4	,10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
quad	quad	k1gInSc1	quad
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
v	v	k7c6	v
<g/>
~	~	kIx~	~
<g/>
kg	kg	kA	kg
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
m	m	kA	m
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
A	a	k9	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
nebo	nebo	k8xC	nebo
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
N	N	kA	N
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
A	a	k9	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
konstanty	konstanta	k1gFnPc1	konstanta
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
Maxwellových	Maxwellův	k2eAgFnPc6d1	Maxwellova
rovnicích	rovnice	k1gFnPc6	rovnice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
popisují	popisovat	k5eAaImIp3nP	popisovat
elektromagnetismus	elektromagnetismus	k1gInSc4	elektromagnetismus
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}}}	}}}}}	k?	}}}}}
:	:	kIx,	:
Astronomické	astronomický	k2eAgFnPc1d1	astronomická
jednotky	jednotka	k1gFnPc1	jednotka
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
(	(	kIx(	(
<g/>
obzvlášť	obzvlášť	k6eAd1	obzvlášť
v	v	k7c6	v
popularizovaných	popularizovaný	k2eAgInPc6d1	popularizovaný
textech	text	k1gInPc6	text
<g/>
)	)	kIx)	)
udávány	udáván	k2eAgFnPc4d1	udávána
ve	v	k7c6	v
světelných	světelný	k2eAgInPc6d1	světelný
letech	let	k1gInPc6	let
<g/>
.	.	kIx.	.
</s>
<s>
Světelný	světelný	k2eAgInSc1d1	světelný
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
urazí	urazit	k5eAaPmIp3nS	urazit
světlo	světlo	k1gNnSc1	světlo
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
t.	t.	k?	t.
j.	j.	k?	j.
přibližně	přibližně	k6eAd1	přibližně
9,46	[number]	k4	9,46
<g/>
×	×	k?	×
<g/>
1012	[number]	k4	1012
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
v	v	k7c6	v
komunikaci	komunikace	k1gFnSc6	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
rovníkový	rovníkový	k2eAgInSc4d1	rovníkový
obvod	obvod	k1gInSc4	obvod
Země	zem	k1gFnSc2	zem
40	[number]	k4	40
075	[number]	k4	075
km	km	kA	km
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
299	[number]	k4	299
792	[number]	k4	792
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
je	on	k3xPp3gNnSc4	on
teoreticky	teoreticky	k6eAd1	teoreticky
nejkratší	krátký	k2eAgInSc1d3	nejkratší
dobou	doba	k1gFnSc7	doba
na	na	k7c6	na
přenesení	přenesení	k1gNnSc6	přenesení
informace	informace	k1gFnSc2	informace
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
Země	zem	k1gFnSc2	zem
0,066838	[number]	k4	0,066838
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Skutečný	skutečný	k2eAgInSc1d1	skutečný
čas	čas	k1gInSc1	čas
přenosu	přenos	k1gInSc2	přenos
ale	ale	k8xC	ale
trvá	trvat	k5eAaImIp3nS	trvat
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
světlo	světlo	k1gNnSc4	světlo
v	v	k7c6	v
optickém	optický	k2eAgNnSc6d1	optické
vlákně	vlákno	k1gNnSc6	vlákno
šíří	šířit	k5eAaImIp3nS	šířit
asi	asi	k9	asi
o	o	k7c4	o
30	[number]	k4	30
%	%	kIx~	%
pomaleji	pomale	k6eAd2	pomale
a	a	k8xC	a
přímá	přímý	k2eAgNnPc4d1	přímé
spojení	spojení	k1gNnPc4	spojení
nejsou	být	k5eNaImIp3nP	být
v	v	k7c4	v
globální	globální	k2eAgFnSc4d1	globální
komunikaci	komunikace	k1gFnSc4	komunikace
častá	častý	k2eAgFnSc1d1	častá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
zdržením	zdržení	k1gNnPc3	zdržení
v	v	k7c6	v
síťových	síťový	k2eAgInPc6d1	síťový
přepínačích	přepínač	k1gInPc6	přepínač
(	(	kIx(	(
<g/>
switches	switches	k1gMnSc1	switches
<g/>
)	)	kIx)	)
a	a	k8xC	a
směrovačích	směrovač	k1gInPc6	směrovač
(	(	kIx(	(
<g/>
routers	routers	k6eAd1	routers
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgInSc4d1	typický
čas	čas	k1gInSc4	čas
odezvy	odezva	k1gFnSc2	odezva
(	(	kIx(	(
<g/>
ping	pingo	k1gNnPc2	pingo
<g/>
)	)	kIx)	)
počítače	počítač	k1gInPc1	počítač
mezi	mezi	k7c7	mezi
Austrálií	Austrálie	k1gFnSc7	Austrálie
a	a	k8xC	a
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
asi	asi	k9	asi
0,18	[number]	k4	0,18
sekundy	sekunda	k1gFnPc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
informace	informace	k1gFnSc1	informace
navíc	navíc	k6eAd1	navíc
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
řešení	řešení	k1gNnSc1	řešení
částí	část	k1gFnPc2	část
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
bezdrátové	bezdrátový	k2eAgFnSc3d1	bezdrátová
komunikaci	komunikace	k1gFnSc3	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Konečná	konečný	k2eAgFnSc1d1	konečná
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
byla	být	k5eAaImAgFnS	být
zřetelná	zřetelný	k2eAgFnSc1d1	zřetelná
například	například	k6eAd1	například
při	při	k7c6	při
komunikaci	komunikace	k1gFnSc6	komunikace
mezi	mezi	k7c7	mezi
pozemním	pozemní	k2eAgNnSc7d1	pozemní
centrem	centrum	k1gNnSc7	centrum
Houston	Houston	k1gInSc1	Houston
a	a	k8xC	a
Neilem	Neil	k1gMnSc7	Neil
Armstrongem	Armstrong	k1gMnSc7	Armstrong
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
každou	každý	k3xTgFnSc4	každý
odpověď	odpověď	k1gFnSc4	odpověď
museli	muset	k5eAaImAgMnP	muset
v	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
čekat	čekat	k5eAaImF	čekat
téměř	téměř	k6eAd1	téměř
3	[number]	k4	3
sekundy	sekunda	k1gFnPc4	sekunda
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
astronauti	astronaut	k1gMnPc1	astronaut
odpovídali	odpovídat	k5eAaImAgMnP	odpovídat
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
také	také	k9	také
nemožné	možný	k2eNgNnSc4d1	nemožné
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
dálkové	dálkový	k2eAgNnSc4d1	dálkové
ovládání	ovládání	k1gNnSc4	ovládání
meziplanetární	meziplanetární	k2eAgFnSc2d1	meziplanetární
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
od	od	k7c2	od
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pozemní	pozemní	k2eAgFnSc1d1	pozemní
kontrola	kontrola	k1gFnSc1	kontrola
rozpozná	rozpoznat	k5eAaPmIp3nS	rozpoznat
problém	problém	k1gInSc4	problém
a	a	k8xC	a
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
loď	loď	k1gFnSc1	loď
příjme	příjem	k1gInSc5	příjem
signál	signál	k1gInSc4	signál
z	z	k7c2	z
pozemního	pozemní	k2eAgNnSc2d1	pozemní
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
i	i	k9	i
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
se	se	k3xPyFc4	se
ale	ale	k9	ale
může	moct	k5eAaImIp3nS	moct
projevit	projevit	k5eAaPmF	projevit
i	i	k9	i
při	při	k7c6	při
malých	malý	k2eAgFnPc6d1	malá
vzdálenostech	vzdálenost	k1gFnPc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
superpočítačích	superpočítač	k1gInPc6	superpočítač
omezuje	omezovat	k5eAaImIp3nS	omezovat
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
přenos	přenos	k1gInSc1	přenos
dat	datum	k1gNnPc2	datum
mezi	mezi	k7c4	mezi
procesory	procesor	k1gInPc4	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
procesor	procesor	k1gInSc1	procesor
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
frekvencí	frekvence	k1gFnSc7	frekvence
1	[number]	k4	1
GHz	GHz	k1gFnPc2	GHz
<g/>
,	,	kIx,	,
signál	signál	k1gInSc1	signál
se	se	k3xPyFc4	se
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
cyklu	cyklus	k1gInSc2	cyklus
dostane	dostat	k5eAaPmIp3nS	dostat
jen	jen	k9	jen
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
300	[number]	k4	300
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
procesory	procesor	k1gInPc7	procesor
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
omezení	omezení	k1gNnSc2	omezení
latence	latence	k1gFnSc2	latence
umístěny	umístěn	k2eAgFnPc1d1	umístěna
těsně	těsně	k6eAd1	těsně
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
procesory	procesor	k1gInPc1	procesor
budou	být	k5eAaImBp3nP	být
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
vyšších	vysoký	k2eAgFnPc6d2	vyšší
frekvencích	frekvence	k1gFnPc6	frekvence
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
stane	stanout	k5eAaPmIp3nS	stanout
omezujícím	omezující	k2eAgInSc7d1	omezující
faktorem	faktor	k1gInSc7	faktor
i	i	k9	i
při	při	k7c6	při
návrhu	návrh	k1gInSc6	návrh
procesoru	procesor	k1gInSc2	procesor
samotného	samotný	k2eAgInSc2d1	samotný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
není	být	k5eNaImIp3nS	být
"	"	kIx"	"
<g/>
rychlostním	rychlostní	k2eAgNnSc7d1	rychlostní
omezením	omezení	k1gNnSc7	omezení
<g/>
"	"	kIx"	"
v	v	k7c6	v
tradičním	tradiční	k2eAgInSc6d1	tradiční
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
pronásledující	pronásledující	k2eAgFnSc2d1	pronásledující
světelný	světelný	k2eAgInSc4d1	světelný
paprsek	paprsek	k1gInSc4	paprsek
naměří	naměřit	k5eAaBmIp3nP	naměřit
shodnou	shodný	k2eAgFnSc4d1	shodná
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
od	od	k7c2	od
něho	on	k3xPp3gMnSc2	on
vzdaluje	vzdalovat	k5eAaImIp3nS	vzdalovat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
stojící	stojící	k2eAgMnSc1d1	stojící
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
chápání	chápání	k1gNnSc4	chápání
rychlosti	rychlost	k1gFnSc2	rychlost
pozoruhodné	pozoruhodný	k2eAgInPc4d1	pozoruhodný
důsledky	důsledek	k1gInPc4	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlosti	rychlost	k1gFnPc1	rychlost
se	se	k3xPyFc4	se
sčítají	sčítat	k5eAaImIp3nP	sčítat
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
dvě	dva	k4xCgNnPc1	dva
auta	auto	k1gNnPc1	auto
jedou	jet	k5eAaImIp3nP	jet
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
a	a	k8xC	a
každé	každý	k3xTgFnSc3	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
má	mít	k5eAaImIp3nS	mít
rychlost	rychlost	k1gFnSc1	rychlost
50	[number]	k4	50
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
očekává	očekávat	k5eAaImIp3nS	očekávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
každé	každý	k3xTgNnSc4	každý
z	z	k7c2	z
aut	auto	k1gNnPc2	auto
bude	být	k5eAaImBp3nS	být
vnímat	vnímat	k5eAaImF	vnímat
celkovou	celkový	k2eAgFnSc4d1	celková
rychlost	rychlost	k1gFnSc4	rychlost
přibližování	přibližování	k1gNnSc6	přibližování
druhého	druhý	k4xOgMnSc2	druhý
jako	jako	k8xS	jako
50	[number]	k4	50
+	+	kIx~	+
50	[number]	k4	50
=	=	kIx~	=
100	[number]	k4	100
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Z	z	k7c2	z
výsledků	výsledek	k1gInPc2	výsledek
experimentů	experiment	k1gInPc2	experiment
s	s	k7c7	s
rychlostmi	rychlost	k1gFnPc7	rychlost
blížícími	blížící	k2eAgFnPc7d1	blížící
se	se	k3xPyFc4	se
rychlosti	rychlost	k1gFnSc3	rychlost
světla	světlo	k1gNnSc2	světlo
však	však	k9	však
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
pravidlo	pravidlo	k1gNnSc1	pravidlo
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
vesmírné	vesmírný	k2eAgFnPc1d1	vesmírná
lodi	loď	k1gFnPc1	loď
letící	letící	k2eAgFnPc1d1	letící
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
nezávislého	závislý	k2eNgMnSc2d1	nezávislý
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
relativní	relativní	k2eAgFnSc2d1	relativní
rychlosti	rychlost	k1gFnSc2	rychlost
90	[number]	k4	90
%	%	kIx~	%
rychlosti	rychlost	k1gFnSc3	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
nevnímají	vnímat	k5eNaImIp3nP	vnímat
přibližování	přibližování	k1gNnSc4	přibližování
rychlostí	rychlost	k1gFnPc2	rychlost
90	[number]	k4	90
%	%	kIx~	%
+	+	kIx~	+
90	[number]	k4	90
%	%	kIx~	%
=	=	kIx~	=
180	[number]	k4	180
%	%	kIx~	%
rychlosti	rychlost	k1gFnSc3	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
vnímají	vnímat	k5eAaImIp3nP	vnímat
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
přibližování	přibližování	k1gNnSc4	přibližování
s	s	k7c7	s
rychlosti	rychlost	k1gFnPc1	rychlost
o	o	k7c4	o
něco	něco	k3yInSc4	něco
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
99,5	[number]	k4	99,5
%	%	kIx~	%
rychlosti	rychlost	k1gFnSc3	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
Einsteinovým	Einsteinův	k2eAgInSc7d1	Einsteinův
vzorcem	vzorec	k1gInSc7	vzorec
sčítání	sčítání	k1gNnSc4	sčítání
rychlostí	rychlost	k1gFnPc2	rychlost
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
u	u	k7c2	u
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
+	+	kIx~	+
w	w	k?	w
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
+	+	kIx~	+
v	v	k7c6	v
w	w	k?	w
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
u	u	k7c2	u
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
v	v	k7c4	v
<g/>
+	+	kIx~	+
<g/>
w	w	k?	w
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
vw	vw	k?	vw
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
kde	kde	k6eAd1	kde
v	v	k7c4	v
a	a	k8xC	a
w	w	k?	w
jsou	být	k5eAaImIp3nP	být
rychlosti	rychlost	k1gFnSc3	rychlost
pozorované	pozorovaný	k2eAgFnSc3d1	pozorovaná
třetím	třetí	k4xOgMnSc7	třetí
pozorovatelem	pozorovatel	k1gMnSc7	pozorovatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
u	u	k7c2	u
je	on	k3xPp3gNnPc4	on
rychlost	rychlost	k1gFnSc1	rychlost
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
přibližování	přibližování	k1gNnSc2	přibližování
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vnímají	vnímat	k5eAaImIp3nP	vnímat
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
letící	letící	k2eAgFnPc4d1	letící
vesmírné	vesmírný	k2eAgFnPc4d1	vesmírná
lodi	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
s	s	k7c7	s
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
intuicí	intuice	k1gFnSc7	intuice
a	a	k8xC	a
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
relativní	relativní	k2eAgFnSc6d1	relativní
rychlosti	rychlost	k1gFnSc6	rychlost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
jeden	jeden	k4xCgMnSc1	jeden
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
k	k	k7c3	k
jinému	jiný	k2eAgMnSc3d1	jiný
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
naměří	naměřit	k5eAaBmIp3nP	naměřit
rychlost	rychlost	k1gFnSc4	rychlost
přicházejícího	přicházející	k2eAgInSc2d1	přicházející
světelného	světelný	k2eAgInSc2d1	světelný
paprsku	paprsek	k1gInSc2	paprsek
jako	jako	k8xC	jako
stejnou	stejný	k2eAgFnSc4d1	stejná
konstantní	konstantní	k2eAgFnSc4d1	konstantní
hodnotu	hodnota	k1gFnSc4	hodnota
rovnající	rovnající	k2eAgFnSc2d1	rovnající
se	se	k3xPyFc4	se
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Rovnice	rovnice	k1gFnSc1	rovnice
uvedená	uvedený	k2eAgFnSc1d1	uvedená
výše	výše	k1gFnSc1	výše
byla	být	k5eAaImAgFnS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
Albertem	Albert	k1gMnSc7	Albert
Einsteinem	Einstein	k1gMnSc7	Einstein
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
principu	princip	k1gInSc2	princip
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
princip	princip	k1gInSc1	princip
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
navržený	navržený	k2eAgInSc4d1	navržený
Galileiem	Galileium	k1gNnSc7	Galileium
<g/>
)	)	kIx)	)
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
zákony	zákon	k1gInPc1	zákon
chovaly	chovat	k5eAaImAgInP	chovat
stejně	stejně	k6eAd1	stejně
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
vztažných	vztažný	k2eAgFnPc6d1	vztažná
soustavách	soustava	k1gFnPc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
přímo	přímo	k6eAd1	přímo
daná	daný	k2eAgFnSc1d1	daná
Maxwellovými	Maxwellův	k2eAgFnPc7d1	Maxwellova
rovnicemi	rovnice	k1gFnPc7	rovnice
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
stejná	stejný	k2eAgNnPc1d1	stejné
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
pozorovatele	pozorovatel	k1gMnSc4	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
zpomalováno	zpomalován	k2eAgNnSc1d1	zpomalováno
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
menší	malý	k2eAgFnSc4d2	menší
než	než	k8xS	než
c	c	k0	c
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
daném	daný	k2eAgInSc6d1	daný
indexem	index	k1gInSc7	index
lomu	lom	k1gInSc2	lom
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k1gNnSc4	málo
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
c.	c.	k?	c.
Hustší	hustý	k2eAgNnPc1d2	hustší
média	médium	k1gNnPc1	médium
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
světlo	světlo	k1gNnSc4	světlo
zpomalit	zpomalit	k5eAaPmF	zpomalit
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
víc	hodně	k6eAd2	hodně
–	–	k?	–
na	na	k7c4	na
hodnoty	hodnota	k1gFnPc4	hodnota
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
a	a	k8xC	a
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
c.	c.	k?	c.
Toto	tento	k3xDgNnSc1	tento
zpomalování	zpomalování	k1gNnSc1	zpomalování
světla	světlo	k1gNnSc2	světlo
je	být	k5eAaImIp3nS	být
zodpovědné	zodpovědný	k2eAgNnSc1d1	zodpovědné
i	i	k9	i
za	za	k7c4	za
vychýlení	vychýlení	k1gNnSc4	vychýlení
světla	světlo	k1gNnSc2	světlo
na	na	k7c6	na
styčné	styčný	k2eAgFnSc6d1	styčná
ploše	plocha	k1gFnSc6	plocha
dvou	dva	k4xCgInPc2	dva
materiálů	materiál	k1gInPc2	materiál
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
indexy	index	k1gInPc7	index
lomu	lom	k1gInSc2	lom
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
lom	lom	k1gInSc1	lom
světla	světlo	k1gNnSc2	světlo
nebo	nebo	k8xC	nebo
refrakce	refrakce	k1gFnSc2	refrakce
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
materiálu	materiál	k1gInSc6	materiál
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
indexu	index	k1gInSc6	index
lomu	lom	k1gInSc2	lom
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
frekvenci	frekvence	k1gFnSc4	frekvence
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
světlo	světlo	k1gNnSc1	světlo
různých	různý	k2eAgFnPc2d1	různá
frekvencí	frekvence	k1gFnPc2	frekvence
prochází	procházet	k5eAaImIp3nS	procházet
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
materiálu	materiál	k1gInSc6	materiál
různými	různý	k2eAgFnPc7d1	různá
rychlostmi	rychlost	k1gFnPc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
deformaci	deformace	k1gFnSc3	deformace
elektromagnetických	elektromagnetický	k2eAgFnPc2d1	elektromagnetická
vln	vlna	k1gFnPc2	vlna
složených	složený	k2eAgFnPc2d1	složená
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
frekvencí	frekvence	k1gFnPc2	frekvence
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
disperze	disperze	k1gFnSc1	disperze
nebo	nebo	k8xC	nebo
rozptyl	rozptyl	k1gInSc1	rozptyl
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Všimněte	všimnout	k5eAaPmRp2nP	všimnout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
zmiňovaná	zmiňovaný	k2eAgFnSc1d1	zmiňovaná
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
je	být	k5eAaImIp3nS	být
pozorovaná	pozorovaný	k2eAgFnSc1d1	pozorovaná
nebo	nebo	k8xC	nebo
měřená	měřený	k2eAgFnSc1d1	měřená
rychlost	rychlost	k1gFnSc1	rychlost
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
médiu	médium	k1gNnSc6	médium
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
skutečná	skutečný	k2eAgFnSc1d1	skutečná
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mikroskopickém	mikroskopický	k2eAgNnSc6d1	mikroskopické
měřítku	měřítko	k1gNnSc6	měřítko
a	a	k8xC	a
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
částice	částice	k1gFnSc1	částice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
lom	lom	k1gInSc1	lom
světla	světlo	k1gNnSc2	světlo
způsoben	způsoben	k2eAgInSc1d1	způsoben
opakovaným	opakovaný	k2eAgNnSc7d1	opakované
pohlcováním	pohlcování	k1gNnSc7	pohlcování
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
vysíláním	vysílání	k1gNnSc7	vysílání
fotonů	foton	k1gInPc2	foton
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
atomy	atom	k1gInPc7	atom
nebo	nebo	k8xC	nebo
molekulami	molekula	k1gFnPc7	molekula
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
které	který	k3yIgFnPc4	který
prochází	procházet	k5eAaImIp3nS	procházet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
světlo	světlo	k1gNnSc1	světlo
šíří	šířit	k5eAaImIp3nS	šířit
jen	jen	k9	jen
vakuem	vakuum	k1gNnSc7	vakuum
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
atomy	atom	k1gInPc7	atom
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jimi	on	k3xPp3gInPc7	on
zdržováno	zdržovat	k5eAaImNgNnS	zdržovat
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
pohlcování	pohlcování	k1gNnSc2	pohlcování
a	a	k8xC	a
následného	následný	k2eAgNnSc2d1	následné
vysílání	vysílání	k1gNnSc2	vysílání
trvá	trvat	k5eAaImIp3nS	trvat
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
dojem	dojem	k1gInSc1	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
světlo	světlo	k1gNnSc1	světlo
zdrželo	zdržet	k5eAaPmAgNnS	zdržet
(	(	kIx(	(
<g/>
t.	t.	k?	t.
j.	j.	k?	j.
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
vstupem	vstup	k1gInSc7	vstup
a	a	k8xC	a
výstupem	výstup	k1gInSc7	výstup
z	z	k7c2	z
média	médium	k1gNnSc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
média	médium	k1gNnSc2	médium
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
získalo	získat	k5eAaPmAgNnS	získat
dodatečnou	dodatečný	k2eAgFnSc4d1	dodatečná
energii	energie	k1gFnSc4	energie
šíří	šířit	k5eAaImIp3nS	šířit
opět	opět	k6eAd1	opět
svou	svůj	k3xOyFgFnSc7	svůj
původní	původní	k2eAgFnSc7d1	původní
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
jen	jen	k9	jen
jediné	jediný	k2eAgNnSc1d1	jediné
<g/>
:	:	kIx,	:
buď	buď	k8xC	buď
se	se	k3xPyFc4	se
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
nikdy	nikdy	k6eAd1	nikdy
nezměnila	změnit	k5eNaPmAgFnS	změnit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
náboj	náboj	k1gInSc1	náboj
každého	každý	k3xTgInSc2	každý
atomu	atom	k1gInSc2	atom
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
elektronů	elektron	k1gInPc2	elektron
<g/>
)	)	kIx)	)
interferuje	interferovat	k5eAaImIp3nS	interferovat
s	s	k7c7	s
elektrickými	elektrický	k2eAgNnPc7d1	elektrické
a	a	k8xC	a
magnetickými	magnetický	k2eAgNnPc7d1	magnetické
poli	pole	k1gNnPc7	pole
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
jeho	jeho	k3xOp3gNnSc7	jeho
šíření	šíření	k1gNnSc4	šíření
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nadsvětelná	Nadsvětelný	k2eAgFnSc1d1	Nadsvětelná
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
experimentu	experiment	k1gInSc6	experiment
byla	být	k5eAaImAgFnS	být
dosažena	dosažen	k2eAgFnSc1d1	dosažena
fázová	fázový	k2eAgFnSc1d1	fázová
rychlost	rychlost	k1gFnSc1	rychlost
laserových	laserový	k2eAgInPc2d1	laserový
paprsků	paprsek	k1gInPc2	paprsek
na	na	k7c6	na
extrémně	extrémně	k6eAd1	extrémně
krátké	krátký	k2eAgFnSc6d1	krátká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
přes	přes	k7c4	přes
atomy	atom	k1gInPc4	atom
cesia	cesium	k1gNnSc2	cesium
300	[number]	k4	300
<g/>
krát	krát	k6eAd1	krát
c.	c.	k?	c.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tato	tento	k3xDgFnSc1	tento
technika	technika	k1gFnSc1	technika
ale	ale	k9	ale
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgFnS	použít
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
informací	informace	k1gFnPc2	informace
rychlostí	rychlost	k1gFnPc2	rychlost
vyšší	vysoký	k2eAgFnSc7d2	vyšší
než	než	k8xS	než
c.	c.	k?	c.
Rychlost	rychlost	k1gFnSc1	rychlost
přenosu	přenos	k1gInSc2	přenos
informace	informace	k1gFnSc2	informace
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
grupové	grupový	k2eAgFnSc6d1	grupová
rychlosti	rychlost	k1gFnSc6	rychlost
(	(	kIx(	(
<g/>
skupinová	skupinový	k2eAgFnSc1d1	skupinová
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
změny	změna	k1gFnPc1	změna
tvaru	tvar	k1gInSc2	tvar
vlny	vlna	k1gFnPc1	vlna
<g/>
)	)	kIx)	)
a	a	k8xC	a
součin	součin	k1gInSc1	součin
fázové	fázový	k2eAgFnSc2d1	fázová
a	a	k8xC	a
grupové	grupový	k2eAgFnSc2d1	grupová
rychlosti	rychlost	k1gFnSc2	rychlost
je	být	k5eAaImIp3nS	být
rovný	rovný	k2eAgMnSc1d1	rovný
druhé	druhý	k4xOgFnSc3	druhý
mocnině	mocnina	k1gFnSc3	mocnina
normální	normální	k2eAgFnSc2d1	normální
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
materiálu	materiál	k1gInSc6	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Překonání	překonání	k1gNnSc1	překonání
fázové	fázový	k2eAgFnSc2d1	fázová
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
porovnatelné	porovnatelný	k2eAgNnSc1d1	porovnatelné
s	s	k7c7	s
překonáním	překonání	k1gNnSc7	překonání
rychlosti	rychlost	k1gFnSc2	rychlost
zvuku	zvuk	k1gInSc2	zvuk
uspořádáním	uspořádání	k1gNnSc7	uspořádání
lidí	člověk	k1gMnPc2	člověk
do	do	k7c2	do
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
řady	řada	k1gFnSc2	řada
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
odstupy	odstup	k1gInPc7	odstup
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
úlohou	úloha	k1gFnSc7	úloha
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
zakřičet	zakřičet	k5eAaPmF	zakřičet
"	"	kIx"	"
<g/>
jsem	být	k5eAaImIp1nS	být
zde	zde	k6eAd1	zde
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
jeden	jeden	k4xCgInSc1	jeden
po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
v	v	k7c6	v
krátkých	krátký	k2eAgInPc6d1	krátký
intervalech	interval	k1gInPc6	interval
měřených	měřený	k2eAgFnPc2d1	měřená
hodinkami	hodinka	k1gFnPc7	hodinka
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemusí	muset	k5eNaImIp3nS	muset
čekat	čekat	k5eAaImF	čekat
než	než	k8xS	než
uslyší	uslyšet	k5eAaPmIp3nP	uslyšet
předcházející	předcházející	k2eAgFnSc4d1	předcházející
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
některých	některý	k3yIgInPc6	některý
dalších	další	k2eAgInPc6d1	další
experimentech	experiment	k1gInPc6	experiment
souvisejících	související	k2eAgInPc6d1	související
s	s	k7c7	s
nestálými	stálý	k2eNgFnPc7d1	nestálá
vlnami	vlna	k1gFnPc7	vlna
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
tunelování	tunelování	k1gNnSc1	tunelování
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
zdát	zdát	k5eAaImF	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
je	být	k5eAaImIp3nS	být
překonána	překonat	k5eAaPmNgFnS	překonat
<g/>
.	.	kIx.	.
</s>
<s>
Experimenty	experiment	k1gInPc1	experiment
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
fázová	fázový	k2eAgFnSc1d1	fázová
rychlost	rychlost	k1gFnSc1	rychlost
nestálých	stálý	k2eNgFnPc2d1	nestálá
vln	vlna	k1gFnPc2	vlna
může	moct	k5eAaImIp3nS	moct
překonat	překonat	k5eAaPmF	překonat
c	c	k0	c
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
grupová	grupový	k2eAgFnSc1d1	grupová
a	a	k8xC	a
čelní	čelní	k2eAgFnSc1d1	čelní
rychlost	rychlost	k1gFnSc1	rychlost
(	(	kIx(	(
<g/>
front	front	k1gInSc1	front
velocity	velocit	k1gInPc1	velocit
–	–	k?	–
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
první	první	k4xOgInSc1	první
nadnulový	nadnulový	k2eAgInSc1d1	nadnulový
pulz	pulz	k1gInSc1	pulz
<g/>
)	)	kIx)	)
nepřekoná	překonat	k5eNaPmIp3nS	překonat
c	c	k0	c
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
opět	opět	k6eAd1	opět
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přenést	přenést	k5eAaPmF	přenést
informaci	informace	k1gFnSc4	informace
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
c.	c.	k?	c.
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
interpretacích	interpretace	k1gFnPc6	interpretace
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
kvantové	kvantový	k2eAgInPc1d1	kvantový
jevy	jev	k1gInPc1	jev
přenášeny	přenášet	k5eAaImNgInP	přenášet
rychlostmi	rychlost	k1gFnPc7	rychlost
vyššími	vysoký	k2eAgFnPc7d2	vyšší
než	než	k8xS	než
c	c	k0	c
(	(	kIx(	(
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
byla	být	k5eAaImAgFnS	být
interakce	interakce	k1gFnPc4	interakce
dvou	dva	k4xCgInPc2	dva
těles	těleso	k1gNnPc2	těleso
oddělených	oddělený	k2eAgFnPc6d1	oddělená
prostorem	prostor	k1gInSc7	prostor
bez	bez	k7c2	bez
známého	známý	k2eAgMnSc2d1	známý
zprostředkovatele	zprostředkovatel	k1gMnSc2	zprostředkovatel
dlouho	dlouho	k6eAd1	dlouho
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k8xS	jako
problém	problém	k1gInSc1	problém
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
,	,	kIx,	,
podívejte	podívat	k5eAaPmRp2nP	podívat
se	se	k3xPyFc4	se
na	na	k7c4	na
EPR	EPR	kA	EPR
paradox	paradox	k1gInSc4	paradox
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
kvantové	kvantový	k2eAgInPc1d1	kvantový
stavy	stav	k1gInPc1	stav
dvou	dva	k4xCgFnPc2	dva
částic	částice	k1gFnPc2	částice
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
propletené	propletený	k2eAgInPc1d1	propletený
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
stav	stav	k1gInSc1	stav
jedné	jeden	k4xCgFnSc2	jeden
částice	částice	k1gFnSc2	částice
určuje	určovat	k5eAaImIp3nS	určovat
stav	stav	k1gInSc4	stav
druhé	druhý	k4xOgFnSc2	druhý
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jedna	jeden	k4xCgFnSc1	jeden
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
spin	spin	k1gInSc4	spin
+	+	kIx~	+
<g/>
1⁄	1⁄	k?	1⁄
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
−	−	k?	−
<g/>
1⁄	1⁄	k?	1⁄
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
okamžiku	okamžik	k1gInSc2	okamžik
pozorování	pozorování	k1gNnPc2	pozorování
jsou	být	k5eAaImIp3nP	být
částice	částice	k1gFnPc1	částice
v	v	k7c6	v
superpozici	superpozice	k1gFnSc6	superpozice
dvou	dva	k4xCgInPc2	dva
kvantových	kvantový	k2eAgInPc2d1	kvantový
stavů	stav	k1gInPc2	stav
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1⁄	1⁄	k?	1⁄
<g/>
,	,	kIx,	,
−	−	k?	−
<g/>
1⁄	1⁄	k?	1⁄
<g/>
)	)	kIx)	)
a	a	k8xC	a
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
1⁄	1⁄	k?	1⁄
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
1⁄	1⁄	k?	1⁄
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
částice	částice	k1gFnPc1	částice
oddělí	oddělit	k5eAaPmIp3nP	oddělit
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
podrobí	podrobit	k5eAaPmIp3nP	podrobit
pozorování	pozorování	k1gNnPc1	pozorování
na	na	k7c4	na
zjištění	zjištění	k1gNnSc4	zjištění
kvantového	kvantový	k2eAgInSc2d1	kvantový
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
stav	stav	k1gInSc1	stav
druhé	druhý	k4xOgFnSc2	druhý
částice	částice	k1gFnSc2	částice
je	být	k5eAaImIp3nS	být
automaticky	automaticky	k6eAd1	automaticky
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
interpretacích	interpretace	k1gFnPc6	interpretace
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
,	,	kIx,	,
že	že	k8xS	že
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
kvantovém	kvantový	k2eAgInSc6d1	kvantový
stavu	stav	k1gInSc6	stav
částice	částice	k1gFnSc2	částice
je	být	k5eAaImIp3nS	být
lokální	lokální	k2eAgFnSc1d1	lokální
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
možné	možný	k2eAgNnSc1d1	možné
vydedukovat	vydedukovat	k5eAaPmF	vydedukovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
druhá	druhý	k4xOgFnSc1	druhý
částice	částice	k1gFnSc1	částice
získá	získat	k5eAaPmIp3nS	získat
svůj	svůj	k3xOyFgInSc4	svůj
kvantový	kvantový	k2eAgInSc4d1	kvantový
stav	stav	k1gInSc4	stav
okamžitě	okamžitě	k6eAd1	okamžitě
po	po	k7c6	po
provedení	provedení	k1gNnSc6	provedení
prvního	první	k4xOgInSc2	první
pozorování	pozorování	k1gNnPc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
kvantový	kvantový	k2eAgInSc1d1	kvantový
stav	stav	k1gInSc1	stav
získá	získat	k5eAaPmIp3nS	získat
první	první	k4xOgFnPc1	první
částice	částice	k1gFnPc1	částice
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
pozorování	pozorování	k1gNnSc6	pozorování
<g/>
,	,	kIx,	,
nedá	dát	k5eNaPmIp3nS	dát
se	se	k3xPyFc4	se
informace	informace	k1gFnSc1	informace
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
přenášet	přenášet	k5eAaImF	přenášet
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
zákony	zákon	k1gInPc1	zákon
nedovolují	dovolovat	k5eNaImIp3nP	dovolovat
přenášet	přenášet	k5eAaImF	přenášet
informace	informace	k1gFnPc4	informace
důmyslněji	důmyslně	k6eAd2	důmyslně
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
formulaci	formulace	k1gFnSc3	formulace
pravidel	pravidlo	k1gNnPc2	pravidlo
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
teorém	teorém	k1gInSc1	teorém
klonování	klonování	k1gNnSc2	klonování
kvantových	kvantový	k2eAgInPc2d1	kvantový
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
supersvětelný	supersvětelný	k2eAgInSc1d1	supersvětelný
pohyb	pohyb	k1gInSc1	pohyb
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
superluminal	superluminat	k5eAaImAgMnS	superluminat
motion	motion	k1gInSc4	motion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
také	také	k9	také
viditelný	viditelný	k2eAgInSc1d1	viditelný
na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
astronomických	astronomický	k2eAgInPc6d1	astronomický
objektech	objekt	k1gInPc6	objekt
jako	jako	k8xC	jako
například	například	k6eAd1	například
relativistické	relativistický	k2eAgInPc4d1	relativistický
výtrysky	výtrysk	k1gInPc4	výtrysk
v	v	k7c6	v
radiových	radiový	k2eAgFnPc2d1	radiová
galaxií	galaxie	k1gFnPc2	galaxie
a	a	k8xC	a
kvasarech	kvasar	k1gInPc6	kvasar
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
však	však	k9	však
proudy	proud	k1gInPc1	proud
nepohybují	pohybovat	k5eNaImIp3nP	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
překračující	překračující	k2eAgFnSc1d1	překračující
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Zdánlivý	zdánlivý	k2eAgInSc1d1	zdánlivý
supersvětelný	supersvětelný	k2eAgInSc1d1	supersvětelný
pohyb	pohyb	k1gInSc1	pohyb
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
projekce	projekce	k1gFnSc1	projekce
způsobená	způsobený	k2eAgFnSc1d1	způsobená
objekty	objekt	k1gInPc7	objekt
pohybujícími	pohybující	k2eAgFnPc7d1	pohybující
se	se	k3xPyFc4	se
rychlostmi	rychlost	k1gFnPc7	rychlost
blízkými	blízký	k2eAgMnPc7d1	blízký
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
malém	malé	k1gNnSc6	malé
úhlu	úhel	k1gInSc2	úhel
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
pozorovaného	pozorovaný	k2eAgInSc2d1	pozorovaný
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Elektromagnetickým	elektromagnetický	k2eAgNnSc7d1	elektromagnetické
zářením	záření	k1gNnSc7	záření
lze	lze	k6eAd1	lze
vytvořit	vytvořit	k5eAaPmF	vytvořit
i	i	k9	i
šokové	šokový	k2eAgFnPc4d1	šoková
vlny	vlna	k1gFnPc4	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Průchodem	průchod	k1gInSc7	průchod
nabité	nabitý	k2eAgFnSc2d1	nabitá
částice	částice	k1gFnSc2	částice
přes	přes	k7c4	přes
izolační	izolační	k2eAgNnSc4d1	izolační
médium	médium	k1gNnSc4	médium
se	se	k3xPyFc4	se
naruší	narušit	k5eAaPmIp3nS	narušit
jeho	jeho	k3xOp3gNnSc4	jeho
lokální	lokální	k2eAgNnSc4d1	lokální
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Elektrony	elektron	k1gInPc1	elektron
v	v	k7c6	v
atomech	atom	k1gInPc6	atom
izolantu	izolant	k1gInSc2	izolant
jsou	být	k5eAaImIp3nP	být
vytlačeny	vytlačen	k2eAgFnPc1d1	vytlačena
a	a	k8xC	a
polarizovány	polarizován	k2eAgFnPc1d1	polarizována
polem	polem	k6eAd1	polem
nabité	nabitý	k2eAgFnPc1d1	nabitá
částice	částice	k1gFnPc1	částice
a	a	k8xC	a
při	při	k7c6	při
obnovení	obnovení	k1gNnSc3	obnovení
rovnováhy	rovnováha	k1gFnSc2	rovnováha
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
médiu	médium	k1gNnSc6	médium
<g/>
,	,	kIx,	,
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
narušení	narušení	k1gNnSc2	narušení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
emitují	emitovat	k5eAaBmIp3nP	emitovat
fotony	foton	k1gInPc1	foton
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Ve	v	k7c6	v
vodiči	vodič	k1gInSc6	vodič
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tato	tento	k3xDgFnSc1	tento
rovnováha	rovnováha	k1gFnSc1	rovnováha
obnovena	obnoven	k2eAgFnSc1d1	obnovena
bez	bez	k7c2	bez
emise	emise	k1gFnSc2	emise
fotonů	foton	k1gInPc2	foton
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnSc7	okolnost
tyto	tento	k3xDgInPc4	tento
fotony	foton	k1gInPc4	foton
vzájemně	vzájemně	k6eAd1	vzájemně
destrukčně	destrukčně	k6eAd1	destrukčně
interferují	interferovat	k5eAaImIp3nP	interferovat
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
žádné	žádný	k3yNgNnSc1	žádný
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
ale	ale	k9	ale
toto	tento	k3xDgNnSc1	tento
rušení	rušení	k1gNnSc1	rušení
šíří	šířit	k5eAaImIp3nS	šířit
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
jaká	jaký	k3yRgFnSc1	jaký
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
fotonů	foton	k1gInPc2	foton
<g/>
,	,	kIx,	,
fotony	foton	k1gInPc4	foton
interferují	interferovat	k5eAaImIp3nP	interferovat
konstruktivně	konstruktivně	k6eAd1	konstruktivně
a	a	k8xC	a
zesilují	zesilovat	k5eAaImIp3nP	zesilovat
pozorovanou	pozorovaný	k2eAgFnSc4d1	pozorovaná
radiaci	radiace	k1gFnSc4	radiace
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
(	(	kIx(	(
<g/>
analogický	analogický	k2eAgInSc1d1	analogický
k	k	k7c3	k
aerodynamickému	aerodynamický	k2eAgInSc3d1	aerodynamický
třesku	třesk	k1gInSc3	třesk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
Čerenkovovo	Čerenkovův	k2eAgNnSc1d1	Čerenkovovo
záření	záření	k1gNnSc1	záření
<g/>
...	...	k?	...
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Možnost	možnost	k1gFnSc4	možnost
komunikovat	komunikovat	k5eAaImF	komunikovat
nebo	nebo	k8xC	nebo
cestovat	cestovat	k5eAaImF	cestovat
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
tématem	téma	k1gNnSc7	téma
vědecko-fantastických	vědeckoantastický	k2eAgNnPc2d1	vědecko-fantastický
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
současných	současný	k2eAgInPc2d1	současný
vědeckých	vědecký	k2eAgInPc2d1	vědecký
poznatků	poznatek	k1gInPc2	poznatek
však	však	k9	však
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
teorie	teorie	k1gFnSc2	teorie
proměnlivé	proměnlivý	k2eAgFnSc2d1	proměnlivá
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
především	především	k9	především
Joã	Joã	k1gMnSc1	Joã
Magueijo	Magueijo	k1gMnSc1	Magueijo
a	a	k8xC	a
John	John	k1gMnSc1	John
Moffat	Moffat	k1gMnSc1	Moffat
<g/>
,	,	kIx,	,
zastávají	zastávat	k5eAaImIp3nP	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
šířilo	šířit	k5eAaImAgNnS	šířit
mnohem	mnohem	k6eAd1	mnohem
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
jaká	jaký	k3yRgFnSc1	jaký
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
současná	současný	k2eAgFnSc1d1	současná
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
vysvětlovalo	vysvětlovat	k5eAaImAgNnS	vysvětlovat
mnoho	mnoho	k4c1	mnoho
kosmologických	kosmologický	k2eAgFnPc2d1	kosmologická
záhad	záhada	k1gFnPc2	záhada
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
konkurenční	konkurenční	k2eAgFnSc1d1	konkurenční
teorie	teorie	k1gFnSc1	teorie
rozpínání	rozpínání	k1gNnSc2	rozpínání
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
však	však	k9	však
zatím	zatím	k6eAd1	zatím
nezískala	získat	k5eNaPmAgFnS	získat
širší	široký	k2eAgFnSc4d2	širší
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
každé	každý	k3xTgNnSc1	každý
světlo	světlo	k1gNnSc1	světlo
procházející	procházející	k2eAgNnSc1d1	procházející
jiným	jiný	k2eAgNnSc7d1	jiné
médiem	médium	k1gNnSc7	médium
než	než	k8xS	než
vakuem	vakuum	k1gNnSc7	vakuum
šíří	šířit	k5eAaImIp3nS	šířit
kvůli	kvůli	k7c3	kvůli
refrakci	refrakce	k1gFnSc3	refrakce
pomaleji	pomale	k6eAd2	pomale
než	než	k8xS	než
c.	c.	k?	c.
Některé	některý	k3yIgInPc4	některý
materiály	materiál	k1gInPc4	materiál
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
neobyčejně	obyčejně	k6eNd1	obyčejně
vysoký	vysoký	k2eAgInSc4d1	vysoký
index	index	k1gInSc4	index
lomu	lom	k1gInSc2	lom
<g/>
.	.	kIx.	.
</s>
<s>
Zvlášť	zvlášť	k6eAd1	zvlášť
vysoká	vysoká	k1gFnSc1	vysoká
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
optická	optický	k2eAgFnSc1d1	optická
hustota	hustota	k1gFnSc1	hustota
Bose-Einsteinova	Bose-Einsteinův	k2eAgInSc2d1	Bose-Einsteinův
kondenzátu	kondenzát	k1gInSc2	kondenzát
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
vědců	vědec	k1gMnPc2	vědec
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Leny	Lena	k1gFnSc2	Lena
Haueové	Haueové	k2eAgFnSc2d1	Haueové
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
dokázala	dokázat	k5eAaPmAgFnS	dokázat
zpomalit	zpomalit	k5eAaPmF	zpomalit
světelný	světelný	k2eAgInSc4d1	světelný
paprsek	paprsek	k1gInSc4	paprsek
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
asi	asi	k9	asi
17	[number]	k4	17
metrů	metr	k1gInPc2	metr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
dokonce	dokonce	k9	dokonce
na	na	k7c4	na
okamžik	okamžik	k1gInSc4	okamžik
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
uspěl	uspět	k5eAaPmAgMnS	uspět
Michail	Michail	k1gMnSc1	Michail
Lukin	Lukin	k1gMnSc1	Lukin
s	s	k7c7	s
vědci	vědec	k1gMnPc7	vědec
Harvardovy	Harvardův	k2eAgFnSc2d1	Harvardova
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
Lebeděvovým	Lebeděvův	k2eAgInSc7d1	Lebeděvův
institutem	institut	k1gInSc7	institut
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
v	v	k7c6	v
úplném	úplný	k2eAgNnSc6d1	úplné
zastavení	zastavení	k1gNnSc6	zastavení
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
jeho	jeho	k3xOp3gNnSc7	jeho
nasměrováním	nasměrování	k1gNnSc7	nasměrování
do	do	k7c2	do
masy	masa	k1gFnSc2	masa
horkého	horký	k2eAgInSc2d1	horký
rubidiového	rubidiový	k2eAgInSc2d1	rubidiový
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
atomy	atom	k1gInPc1	atom
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
Lukinových	Lukinův	k2eAgNnPc2d1	Lukinův
slov	slovo	k1gNnPc2	slovo
chovaly	chovat	k5eAaImAgFnP	chovat
"	"	kIx"	"
<g/>
jako	jako	k9	jako
maličká	maličká	k1gFnSc1	maličká
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
<g/>
"	"	kIx"	"
díky	díky	k7c3	díky
interferenčnímu	interferenční	k2eAgInSc3d1	interferenční
obrazci	obrazec	k1gInSc3	obrazec
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
"	"	kIx"	"
<g/>
kontrolních	kontrolní	k2eAgInPc6d1	kontrolní
<g/>
"	"	kIx"	"
paprscích	paprsek	k1gInPc6	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
nedávné	dávný	k2eNgFnSc2d1	nedávná
minulosti	minulost	k1gFnSc2	minulost
byla	být	k5eAaImAgFnS	být
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
jen	jen	k6eAd1	jen
otázkou	otázka	k1gFnSc7	otázka
dohadů	dohad	k1gInPc2	dohad
<g/>
.	.	kIx.	.
</s>
<s>
Antický	antický	k2eAgMnSc1d1	antický
filosof	filosof	k1gMnSc1	filosof
Empedoklés	Empedoklésa	k1gFnPc2	Empedoklésa
zastával	zastávat	k5eAaImAgMnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
něco	něco	k3yInSc1	něco
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
a	a	k8xC	a
šíří	šířit	k5eAaImIp3nS	šířit
mezi	mezi	k7c7	mezi
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
oblohou	obloha	k1gFnSc7	obloha
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
to	ten	k3xDgNnSc4	ten
můžeme	moct	k5eAaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
cesta	cesta	k1gFnSc1	cesta
světla	světlo	k1gNnSc2	světlo
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
jiné	jiná	k1gFnSc6	jiná
trvat	trvat	k5eAaImF	trvat
určitý	určitý	k2eAgInSc4d1	určitý
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgMnSc1d1	jiný
řecký	řecký	k2eAgMnSc1d1	řecký
filosof	filosof	k1gMnSc1	filosof
Aristotelés	Aristotelésa	k1gFnPc2	Aristotelésa
to	ten	k3xDgNnSc4	ten
odmítal	odmítat	k5eAaImAgMnS	odmítat
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
určité	určitý	k2eAgFnSc2d1	určitá
přítomnosti	přítomnost	k1gFnSc2	přítomnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
<g/>
,	,	kIx,	,
statická	statický	k2eAgFnSc1d1	statická
matérie	matérie	k1gFnSc1	matérie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
opakem	opak	k1gInSc7	opak
tmy	tma	k1gFnSc2	tma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepohybuje	pohybovat	k5eNaImIp3nS	pohybovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
by	by	kYmCp3nS	by
světlo	světlo	k1gNnSc1	světlo
mělo	mít	k5eAaImAgNnS	mít
konečnou	konečný	k2eAgFnSc4d1	konečná
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
velká	velký	k2eAgFnSc1d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelés	Aristotelés	k6eAd1	Aristotelés
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
až	až	k9	až
příliš	příliš	k6eAd1	příliš
neuvěřitelné	uvěřitelný	k2eNgNnSc1d1	neuvěřitelné
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
starověkých	starověký	k2eAgFnPc2d1	starověká
teorií	teorie	k1gFnPc2	teorie
vidění	vidění	k1gNnSc2	vidění
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
vyzařováno	vyzařovat	k5eAaImNgNnS	vyzařovat
z	z	k7c2	z
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
zdroje	zdroj	k1gInSc2	zdroj
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
odráženo	odrážen	k2eAgNnSc1d1	odráženo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
odvodil	odvodit	k5eAaPmAgMnS	odvodit
Hérón	Hérón	k1gMnSc1	Hérón
z	z	k7c2	z
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
argument	argument	k1gInSc1	argument
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vzdálené	vzdálený	k2eAgInPc1d1	vzdálený
objekty	objekt	k1gInPc1	objekt
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
oko	oko	k1gNnSc1	oko
otevře	otevřít	k5eAaPmIp3nS	otevřít
<g/>
...	...	k?	...
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Islámští	islámský	k2eAgMnPc1d1	islámský
filozofové	filozof	k1gMnPc1	filozof
Avicenna	Avicenno	k1gNnSc2	Avicenno
a	a	k8xC	a
Alhazen	Alhazen	k2eAgMnSc1d1	Alhazen
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
má	mít	k5eAaImIp3nS	mít
konečnou	konečný	k2eAgFnSc4d1	konečná
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgMnPc2d1	ostatní
filosofů	filosof	k1gMnPc2	filosof
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bodě	bod	k1gInSc6	bod
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
s	s	k7c7	s
Aristotelem	Aristoteles	k1gMnSc7	Aristoteles
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
považovala	považovat	k5eAaImAgFnS	považovat
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
za	za	k7c7	za
konečnou	konečná	k1gFnSc7	konečná
i	i	k9	i
árijská	árijský	k2eAgFnSc1d1	árijská
filosofická	filosofický	k2eAgFnSc1d1	filosofická
škola	škola	k1gFnSc1	škola
ve	v	k7c6	v
starověké	starověký	k2eAgFnSc6d1	starověká
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Johannes	Johannes	k1gMnSc1	Johannes
Kepler	Kepler	k1gMnSc1	Kepler
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
putuje	putovat	k5eAaImIp3nS	putovat
neomezenou	omezený	k2eNgFnSc7d1	neomezená
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
prostoru	prostor	k1gInSc6	prostor
mu	on	k3xPp3gMnSc3	on
nestojí	stát	k5eNaImIp3nS	stát
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
žádné	žádný	k3yNgFnSc2	žádný
překážky	překážka	k1gFnSc2	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Francis	Francis	k1gInSc1	Francis
Bacon	Bacon	k1gInSc1	Bacon
argumentoval	argumentovat	k5eAaImAgMnS	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
nutně	nutně	k6eAd1	nutně
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tak	tak	k6eAd1	tak
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
nejsme	být	k5eNaImIp1nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
vnímat	vnímat	k5eAaImF	vnímat
<g/>
.	.	kIx.	.
</s>
<s>
René	René	k1gMnSc1	René
Descartes	Descartes	k1gMnSc1	Descartes
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdyby	kdyby	kYmCp3nS	kdyby
byla	být	k5eAaImAgFnS	být
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
konečná	konečný	k2eAgFnSc1d1	konečná
<g/>
,	,	kIx,	,
nemohly	moct	k5eNaImAgFnP	moct
by	by	kYmCp3nP	by
Slunce	slunce	k1gNnPc4	slunce
<g/>
,	,	kIx,	,
Měsíc	měsíc	k1gInSc1	měsíc
a	a	k8xC	a
Země	země	k1gFnSc1	země
být	být	k5eAaImF	být
během	během	k7c2	během
zatmění	zatmění	k1gNnSc2	zatmění
v	v	k7c6	v
zákrytu	zákryt	k1gInSc6	zákryt
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
nic	nic	k3yNnSc1	nic
takového	takový	k3xDgMnSc4	takový
nebylo	být	k5eNaImAgNnS	být
pozorováno	pozorován	k2eAgNnSc1d1	pozorováno
<g/>
,	,	kIx,	,
odvodil	odvodit	k5eAaPmAgMnS	odvodit
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
je	být	k5eAaImIp3nS	být
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
<g/>
.	.	kIx.	.
</s>
<s>
Descartes	Descartes	k1gMnSc1	Descartes
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nazýval	nazývat	k5eAaImAgInS	nazývat
plenum	plenum	k1gInSc1	plenum
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vidění	vidění	k1gNnSc3	vidění
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
byl	být	k5eAaImAgInS	být
přesvědčen	přesvědčit	k5eAaPmNgInS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdyby	kdyby	kYmCp3nS	kdyby
připustil	připustit	k5eAaPmAgMnS	připustit
konečnost	konečnost	k1gFnSc4	konečnost
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc1d1	celý
jeho	jeho	k3xOp3gInSc1	jeho
filosofický	filosofický	k2eAgInSc1d1	filosofický
systém	systém	k1gInSc1	systém
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
Isaac	Isaac	k6eAd1	Isaac
Beeckman	Beeckman	k1gMnSc1	Beeckman
<g/>
,	,	kIx,	,
Descartův	Descartův	k2eAgMnSc1d1	Descartův
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1629	[number]	k4	1629
experiment	experiment	k1gInSc1	experiment
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgNnSc6	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
záblesk	záblesk	k1gInSc4	záblesk
z	z	k7c2	z
kanónu	kanón	k1gInSc2	kanón
odražený	odražený	k2eAgInSc4d1	odražený
ze	z	k7c2	z
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
vzdáleného	vzdálený	k2eAgNnSc2d1	vzdálené
asi	asi	k9	asi
1	[number]	k4	1
míli	míle	k1gFnSc4	míle
<g/>
.	.	kIx.	.
</s>
<s>
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gFnSc5	Galile
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1638	[number]	k4	1638
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
měřit	měřit	k5eAaImF	měřit
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
pozorováním	pozorování	k1gNnSc7	pozorování
prodlevy	prodleva	k1gFnSc2	prodleva
mezi	mezi	k7c7	mezi
odkrytím	odkrytí	k1gNnSc7	odkrytí
lucerny	lucerna	k1gFnSc2	lucerna
a	a	k8xC	a
zpozorováním	zpozorování	k1gNnSc7	zpozorování
světla	světlo	k1gNnSc2	světlo
z	z	k7c2	z
určité	určitý	k2eAgFnSc2d1	určitá
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Descartes	Descartes	k1gMnSc1	Descartes
tento	tento	k3xDgInSc4	tento
experiment	experiment	k1gInSc4	experiment
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
jako	jako	k9	jako
zbytečný	zbytečný	k2eAgInSc4d1	zbytečný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
experiment	experiment	k1gInSc1	experiment
během	během	k7c2	během
zatmění	zatmění	k1gNnSc2	zatmění
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
lepší	dobrý	k2eAgInPc4d2	lepší
předpoklady	předpoklad	k1gInPc4	předpoklad
ke	k	k7c3	k
zjištění	zjištění	k1gNnSc3	zjištění
konečné	konečný	k2eAgFnSc2d1	konečná
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
negativní	negativní	k2eAgMnSc1d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
experiment	experiment	k1gInSc1	experiment
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1667	[number]	k4	1667
Florentinská	florentinský	k2eAgFnSc1d1	florentinská
Accademia	Accademia	k1gFnSc1	Accademia
del	del	k?	del
Cimento	Cimento	k1gNnSc1	Cimento
<g/>
,	,	kIx,	,
s	s	k7c7	s
lucernami	lucerna	k1gFnPc7	lucerna
vzdálenými	vzdálený	k2eAgFnPc7d1	vzdálená
asi	asi	k9	asi
1	[number]	k4	1
míli	míle	k1gFnSc4	míle
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
však	však	k9	však
byla	být	k5eAaImAgFnS	být
příliš	příliš	k6eAd1	příliš
malá	malý	k2eAgFnSc1d1	malá
a	a	k8xC	a
tak	tak	k9	tak
žádné	žádný	k3yNgNnSc1	žádný
zpoždění	zpoždění	k1gNnSc1	zpoždění
nebylo	být	k5eNaImAgNnS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Hooke	Hook	k1gInSc2	Hook
negativní	negativní	k2eAgInSc4d1	negativní
výsledek	výsledek	k1gInSc4	výsledek
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
potvrzení	potvrzení	k1gNnSc4	potvrzení
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
pohybovat	pohybovat	k5eAaImF	pohybovat
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
kvantitativní	kvantitativní	k2eAgInSc1d1	kvantitativní
odhad	odhad	k1gInSc1	odhad
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
provedl	provést	k5eAaPmAgInS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1676	[number]	k4	1676
Ole	Ola	k1gFnSc3	Ola
Rø	Rø	k1gFnSc2	Rø
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pomocí	pomocí	k7c2	pomocí
dalekohledu	dalekohled	k1gInSc2	dalekohled
studoval	studovat	k5eAaImAgMnS	studovat
pohyb	pohyb	k1gInSc4	pohyb
Jupiterova	Jupiterův	k2eAgInSc2d1	Jupiterův
měsíce	měsíc	k1gInSc2	měsíc
Io	Io	k1gFnSc2	Io
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Io	Io	k1gFnSc1	Io
vchází	vcházet	k5eAaImIp3nS	vcházet
a	a	k8xC	a
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
Jupiterova	Jupiterův	k2eAgInSc2d1	Jupiterův
stínu	stín	k1gInSc2	stín
v	v	k7c6	v
pravidelných	pravidelný	k2eAgInPc6d1	pravidelný
intervalech	interval	k1gInPc6	interval
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
změřit	změřit	k5eAaPmF	změřit
trvání	trvání	k1gNnSc4	trvání
doby	doba	k1gFnSc2	doba
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Rø	Rø	k?	Rø
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Jupiter	Jupiter	k1gMnSc1	Jupiter
nejblíž	blízce	k6eAd3	blízce
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
doba	doba	k1gFnSc1	doba
oběhu	oběh	k1gInSc2	oběh
Io	Io	k1gMnPc2	Io
kolem	kolem	k7c2	kolem
Jupitera	Jupiter	k1gMnSc2	Jupiter
42,5	[number]	k4	42,5
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
Jupiter	Jupiter	k1gMnSc1	Jupiter
a	a	k8xC	a
Země	země	k1gFnSc1	země
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
vzdalovaly	vzdalovat	k5eAaImAgInP	vzdalovat
<g/>
,	,	kIx,	,
Io	Io	k1gFnSc4	Io
vycházel	vycházet	k5eAaImAgMnS	vycházet
ze	z	k7c2	z
stínu	stín	k1gInSc2	stín
Jupitera	Jupiter	k1gMnSc2	Jupiter
postupně	postupně	k6eAd1	postupně
stále	stále	k6eAd1	stále
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
tomuto	tento	k3xDgInSc3	tento
výstupnímu	výstupní	k2eAgInSc3d1	výstupní
"	"	kIx"	"
<g/>
signálu	signál	k1gInSc2	signál
<g/>
"	"	kIx"	"
trvalo	trvat	k5eAaImAgNnS	trvat
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
Země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
Země	země	k1gFnSc1	země
a	a	k8xC	a
Jupiter	Jupiter	k1gInSc4	Jupiter
vzdalovaly	vzdalovat	k5eAaImAgFnP	vzdalovat
<g/>
,	,	kIx,	,
zvětšoval	zvětšovat	k5eAaImAgInS	zvětšovat
se	se	k3xPyFc4	se
interval	interval	k1gInSc1	interval
mezi	mezi	k7c7	mezi
signály	signál	k1gInPc7	signál
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
projevoval	projevovat	k5eAaImAgMnS	projevovat
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
světlu	světlo	k1gNnSc3	světlo
zabere	zabrat	k5eAaPmIp3nS	zabrat
překonání	překonání	k1gNnSc4	překonání
dodatečné	dodatečný	k2eAgFnSc2d1	dodatečná
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
mezi	mezi	k7c7	mezi
planetami	planeta	k1gFnPc7	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
asi	asi	k9	asi
o	o	k7c4	o
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
vstupy	vstup	k1gInPc1	vstup
měsíce	měsíc	k1gInSc2	měsíc
Io	Io	k1gFnSc2	Io
do	do	k7c2	do
stínu	stín	k1gInSc2	stín
Jupitera	Jupiter	k1gMnSc2	Jupiter
o	o	k7c6	o
něco	něco	k3yInSc4	něco
častější	častý	k2eAgNnSc1d2	častější
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
Jupiter	Jupiter	k1gInSc4	Jupiter
přibližovaly	přibližovat	k5eAaImAgInP	přibližovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgFnPc2	tento
pozorování	pozorování	k1gNnPc4	pozorování
Rø	Rø	k1gMnSc1	Rø
odhadoval	odhadovat	k5eAaImAgMnS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
překonání	překonání	k1gNnSc6	překonání
průměru	průměr	k1gInSc2	průměr
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
astronomické	astronomický	k2eAgFnSc2d1	astronomická
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
světlo	světlo	k1gNnSc1	světlo
potřebovalo	potřebovat	k5eAaImAgNnS	potřebovat
22	[number]	k4	22
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
moderní	moderní	k2eAgInSc1d1	moderní
odhad	odhad	k1gInSc1	odhad
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
16	[number]	k4	16
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
40	[number]	k4	40
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
velikost	velikost	k1gFnSc1	velikost
astronomické	astronomický	k2eAgFnSc2d1	astronomická
jednotky	jednotka	k1gFnSc2	jednotka
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
140	[number]	k4	140
milionů	milion	k4xCgInPc2	milion
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
astronomické	astronomický	k2eAgFnSc2d1	astronomická
jednotky	jednotka	k1gFnSc2	jednotka
a	a	k8xC	a
Rø	Rø	k1gFnSc2	Rø
odhadu	odhad	k1gInSc2	odhad
času	čas	k1gInSc2	čas
vypočítal	vypočítat	k5eAaPmAgMnS	vypočítat
autor	autor	k1gMnSc1	autor
vlnové	vlnový	k2eAgFnSc2d1	vlnová
teorie	teorie	k1gFnSc2	teorie
Nizozemec	Nizozemec	k1gMnSc1	Nizozemec
Christiaan	Christiaan	k1gMnSc1	Christiaan
Huygens	Huygensa	k1gFnPc2	Huygensa
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
na	na	k7c4	na
1	[number]	k4	1
000	[number]	k4	000
průměrů	průměr	k1gInPc2	průměr
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
220	[number]	k4	220
000	[number]	k4	000
kilometrů	kilometr	k1gInPc2	kilometr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
významně	významně	k6eAd1	významně
méně	málo	k6eAd2	málo
než	než	k8xS	než
dnes	dnes	k6eAd1	dnes
uznávaná	uznávaný	k2eAgFnSc1d1	uznávaná
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k6eAd1	tak
tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
převyšovala	převyšovat	k5eAaImAgFnS	převyšovat
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
fyzikální	fyzikální	k2eAgInSc4d1	fyzikální
jev	jev	k1gInSc4	jev
známý	známý	k2eAgInSc4d1	známý
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Isaac	Isaac	k1gInSc1	Isaac
Newton	newton	k1gInSc1	newton
uznával	uznávat	k5eAaImAgInS	uznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
je	být	k5eAaImIp3nS	být
konečná	konečný	k2eAgFnSc1d1	konečná
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
"	"	kIx"	"
<g/>
Opticks	Opticks	k1gInSc1	Opticks
<g/>
"	"	kIx"	"
dokonce	dokonce	k9	dokonce
publikoval	publikovat	k5eAaBmAgMnS	publikovat
přesnější	přesný	k2eAgFnSc4d2	přesnější
hodnotu	hodnota	k1gFnSc4	hodnota
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
–	–	k?	–
16	[number]	k4	16
průměrů	průměr	k1gInPc2	průměr
Země	zem	k1gFnSc2	zem
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
sám	sám	k3xTgMnSc1	sám
odvodil	odvodit	k5eAaPmAgInS	odvodit
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
z	z	k7c2	z
Rø	Rø	k1gFnSc2	Rø
údajů	údaj	k1gInPc2	údaj
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
něčeho	něco	k3yInSc2	něco
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
úkaz	úkaz	k1gInSc1	úkaz
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
pozorován	pozorovat	k5eAaImNgInS	pozorovat
Rø	Rø	k1gFnSc3	Rø
na	na	k7c6	na
rotující	rotující	k2eAgFnSc6d1	rotující
"	"	kIx"	"
<g/>
skvrně	skvrna	k1gFnSc6	skvrna
<g/>
"	"	kIx"	"
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Efekt	efekt	k1gInSc1	efekt
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
i	i	k9	i
později	pozdě	k6eAd2	pozdě
u	u	k7c2	u
obtížnějšího	obtížný	k2eAgNnSc2d2	obtížnější
pozorování	pozorování	k1gNnSc2	pozorování
tří	tři	k4xCgInPc2	tři
dalších	další	k2eAgInPc2d1	další
Galileových	Galileův	k2eAgInPc2d1	Galileův
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
tato	tento	k3xDgNnPc1	tento
pozorování	pozorování	k1gNnPc1	pozorování
však	však	k9	však
nepřesvědčila	přesvědčit	k5eNaPmAgNnP	přesvědčit
každého	každý	k3xTgMnSc2	každý
(	(	kIx(	(
<g/>
především	především	k9	především
Giovanniho	Giovanni	k1gMnSc2	Giovanni
Domenica	Domenicum	k1gNnSc2	Domenicum
Cassiniho	Cassini	k1gMnSc2	Cassini
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
definitivnímu	definitivní	k2eAgNnSc3d1	definitivní
odmítnutí	odmítnutí	k1gNnSc3	odmítnutí
hypotézy	hypotéza	k1gFnSc2	hypotéza
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
po	po	k7c6	po
pozorováních	pozorování	k1gNnPc6	pozorování
Jamese	Jamese	k1gFnSc2	Jamese
Bradleyho	Bradley	k1gMnSc2	Bradley
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1728	[number]	k4	1728
<g/>
.	.	kIx.	.
</s>
<s>
Bradley	Bradlea	k1gFnPc1	Bradlea
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
změřit	změřit	k5eAaPmF	změřit
paralaxu	paralaxa	k1gFnSc4	paralaxa
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
určit	určit	k5eAaPmF	určit
jejich	jejich	k3xOp3gFnSc4	jejich
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
naměřil	naměřit	k5eAaBmAgMnS	naměřit
aberaci	aberace	k1gFnSc3	aberace
<g/>
.	.	kIx.	.
</s>
<s>
Vyvodil	vyvodit	k5eAaBmAgMnS	vyvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc4	světlo
hvězd	hvězda	k1gFnPc2	hvězda
dopadající	dopadající	k2eAgFnSc2d1	dopadající
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
musí	muset	k5eAaImIp3nP	muset
přicházet	přicházet	k5eAaImF	přicházet
z	z	k7c2	z
mírného	mírný	k2eAgInSc2d1	mírný
úhlu	úhel	k1gInSc2	úhel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vypočítat	vypočítat	k5eAaPmF	vypočítat
porovnáním	porovnání	k1gNnSc7	porovnání
rychlosti	rychlost	k1gFnSc2	rychlost
Země	zem	k1gFnSc2	zem
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
k	k	k7c3	k
rychlosti	rychlost	k1gFnSc3	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
aberace	aberace	k1gFnSc1	aberace
byla	být	k5eAaImAgFnS	být
asi	asi	k9	asi
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
200	[number]	k4	200
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Bradleym	Bradleym	k1gInSc1	Bradleym
vypočítaná	vypočítaný	k2eAgFnSc1d1	vypočítaná
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
byla	být	k5eAaImAgFnS	být
298	[number]	k4	298
000	[number]	k4	000
kilometrů	kilometr	k1gInPc2	kilometr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k4c4	málo
méně	málo	k6eAd2	málo
než	než	k8xS	než
dnes	dnes	k6eAd1	dnes
uznávaná	uznávaný	k2eAgFnSc1d1	uznávaná
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Aberace	aberace	k1gFnSc1	aberace
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
století	století	k1gNnPc2	století
široce	široko	k6eAd1	široko
zkoumána	zkoumán	k2eAgFnSc1d1	zkoumána
<g/>
,	,	kIx,	,
především	především	k9	především
Friedrichem	Friedrich	k1gMnSc7	Friedrich
von	von	k1gInSc4	von
Struve	Struev	k1gFnSc2	Struev
a	a	k8xC	a
Magnusem	Magnus	k1gMnSc7	Magnus
Nyrenem	Nyren	k1gMnSc7	Nyren
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
měření	měření	k1gNnSc1	měření
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
pozemním	pozemní	k2eAgInSc7d1	pozemní
přístrojem	přístroj	k1gInSc7	přístroj
provedl	provést	k5eAaPmAgInS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
francouzský	francouzský	k2eAgInSc1d1	francouzský
fyzik	fyzik	k1gMnSc1	fyzik
Hippolyte	Hippolyt	k1gInSc5	Hippolyt
Fizeau	Fizea	k1gMnSc3	Fizea
<g/>
.	.	kIx.	.
</s>
<s>
Fizeauv	Fizeauv	k1gInSc1	Fizeauv
experiment	experiment	k1gInSc1	experiment
byl	být	k5eAaImAgInS	být
koncepčně	koncepčně	k6eAd1	koncepčně
podobný	podobný	k2eAgInSc1d1	podobný
návrhům	návrh	k1gInPc3	návrh
Beeckmana	Beeckman	k1gMnSc2	Beeckman
a	a	k8xC	a
Galilea	Galilea	k1gFnSc1	Galilea
<g/>
.	.	kIx.	.
</s>
<s>
Paprsek	paprsek	k1gInSc1	paprsek
světla	světlo	k1gNnSc2	světlo
byl	být	k5eAaImAgInS	být
namířen	namířit	k5eAaPmNgInS	namířit
na	na	k7c4	na
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
umístěné	umístěný	k2eAgNnSc4d1	umístěné
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
8633	[number]	k4	8633
m.	m.	k?	m.
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
od	od	k7c2	od
zdroje	zdroj	k1gInSc2	zdroj
světla	světlo	k1gNnSc2	světlo
k	k	k7c3	k
zrcadlu	zrcadlo	k1gNnSc3	zrcadlo
paprsek	paprsek	k1gInSc1	paprsek
procházel	procházet	k5eAaImAgInS	procházet
rotujícím	rotující	k2eAgInSc7d1	rotující
diskem	disk	k1gInSc7	disk
se	s	k7c7	s
zářezy	zářez	k1gInPc7	zářez
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
určité	určitý	k2eAgFnSc6d1	určitá
rychlosti	rychlost	k1gFnSc6	rychlost
rotace	rotace	k1gFnSc1	rotace
disku	disk	k1gInSc2	disk
projde	projít	k5eAaPmIp3nS	projít
paprsek	paprsek	k1gInSc4	paprsek
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
zdroje	zdroj	k1gInSc2	zdroj
jedním	jeden	k4xCgInSc7	jeden
zářezem	zářez	k1gInSc7	zářez
a	a	k8xC	a
při	při	k7c6	při
návratu	návrat	k1gInSc3	návrat
zářezem	zářez	k1gInSc7	zářez
následujícím	následující	k2eAgInSc7d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
dojde	dojít	k5eAaPmIp3nS	dojít
třeba	třeba	k6eAd1	třeba
i	i	k9	i
jen	jen	k9	jen
k	k	k7c3	k
malému	malý	k2eAgNnSc3d1	malé
zrychlení	zrychlení	k1gNnSc3	zrychlení
nebo	nebo	k8xC	nebo
zpomalení	zpomalení	k1gNnSc3	zpomalení
rotace	rotace	k1gFnSc2	rotace
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
zpětný	zpětný	k2eAgInSc1d1	zpětný
paprsek	paprsek	k1gInSc1	paprsek
samotný	samotný	k2eAgInSc1d1	samotný
disk	disk	k1gInSc4	disk
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gInSc1	jeho
zub	zub	k1gInSc1	zub
<g/>
)	)	kIx)	)
a	a	k8xC	a
nedostane	dostat	k5eNaPmIp3nS	dostat
se	se	k3xPyFc4	se
nazpět	nazpět	k6eAd1	nazpět
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vypočítat	vypočítat	k5eAaPmF	vypočítat
ze	z	k7c2	z
známé	známý	k2eAgFnSc2d1	známá
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
zdroje	zdroj	k1gInSc2	zdroj
a	a	k8xC	a
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
<g/>
,	,	kIx,	,
počtu	počet	k1gInSc2	počet
zářezů	zářez	k1gInPc2	zářez
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
zubů	zub	k1gInPc2	zub
<g/>
)	)	kIx)	)
na	na	k7c6	na
disku	disk	k1gInSc6	disk
a	a	k8xC	a
rychlosti	rychlost	k1gFnSc3	rychlost
rotace	rotace	k1gFnSc2	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
Fizeaem	Fizeaem	k1gInSc4	Fizeaem
byla	být	k5eAaImAgFnS	být
313	[number]	k4	313
000	[number]	k4	000
kilometrů	kilometr	k1gInPc2	kilometr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Fizeauova	Fizeauův	k2eAgFnSc1d1	Fizeauův
metoda	metoda	k1gFnSc1	metoda
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
zdokonalena	zdokonalit	k5eAaPmNgFnS	zdokonalit
M.	M.	kA	M.
A.	A.	kA	A.
Cornuem	Cornuem	k1gInSc1	Cornuem
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
a	a	k8xC	a
J.	J.	kA	J.
Perrotinem	Perrotin	k1gMnSc7	Perrotin
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Leon	Leona	k1gFnPc2	Leona
Foucault	Foucaulta	k1gFnPc2	Foucaulta
vylepšil	vylepšit	k5eAaPmAgInS	vylepšit
Fizeauovu	Fizeauův	k2eAgFnSc4d1	Fizeauův
metodu	metoda	k1gFnSc4	metoda
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nahradil	nahradit	k5eAaPmAgMnS	nahradit
disk	disk	k1gInSc4	disk
se	s	k7c7	s
zářezy	zářez	k1gInPc7	zářez
rotujícím	rotující	k2eAgNnSc7d1	rotující
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Foucaultův	Foucaultův	k2eAgInSc1d1	Foucaultův
odhad	odhad	k1gInSc1	odhad
publikovaný	publikovaný	k2eAgInSc1d1	publikovaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
byl	být	k5eAaImAgInS	být
298	[number]	k4	298
000	[number]	k4	000
kilometrů	kilometr	k1gInPc2	kilometr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Foucaultovu	Foucaultův	k2eAgFnSc4d1	Foucaultova
metodu	metoda	k1gFnSc4	metoda
použili	použít	k5eAaPmAgMnP	použít
i	i	k9	i
Simon	Simon	k1gMnSc1	Simon
Newcomb	Newcomb	k1gMnSc1	Newcomb
a	a	k8xC	a
Albert	Albert	k1gMnSc1	Albert
A.	A.	kA	A.
Michelson	Michelson	k1gMnSc1	Michelson
<g/>
.	.	kIx.	.
</s>
<s>
Michelson	Michelson	k1gMnSc1	Michelson
použil	použít	k5eAaPmAgMnS	použít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
rotující	rotující	k2eAgNnPc4d1	rotující
zrcadla	zrcadlo	k1gNnPc4	zrcadlo
pro	pro	k7c4	pro
změření	změření	k1gNnSc4	změření
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
světlo	světlo	k1gNnSc4	světlo
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
na	na	k7c6	na
překonání	překonání	k1gNnSc6	překonání
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
35	[number]	k4	35
km	km	kA	km
mezi	mezi	k7c7	mezi
horami	hora	k1gFnPc7	hora
Mount	Mount	k1gMnSc1	Mount
Wilson	Wilson	k1gMnSc1	Wilson
a	a	k8xC	a
Mount	Mount	k1gMnSc1	Mount
San	San	k1gMnSc1	San
Antonio	Antonio	k1gMnSc1	Antonio
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
těchto	tento	k3xDgNnPc2	tento
měření	měření	k1gNnPc2	měření
byla	být	k5eAaImAgFnS	být
relativně	relativně	k6eAd1	relativně
přesně	přesně	k6eAd1	přesně
určená	určený	k2eAgFnSc1d1	určená
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
na	na	k7c4	na
299	[number]	k4	299
796	[number]	k4	796
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Díky	díky	k7c3	díky
práci	práce	k1gFnSc3	práce
Jamese	Jamese	k1gFnSc2	Jamese
Clerka	Clerko	k1gNnSc2	Clerko
Maxwella	Maxwello	k1gNnSc2	Maxwello
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
je	být	k5eAaImIp3nS	být
konstanta	konstanta	k1gFnSc1	konstanta
definovaná	definovaný	k2eAgFnSc1d1	definovaná
elektromagnetickými	elektromagnetický	k2eAgFnPc7d1	elektromagnetická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
vakua	vakuum	k1gNnSc2	vakuum
(	(	kIx(	(
<g/>
permitivitou	permitivita	k1gFnSc7	permitivita
a	a	k8xC	a
permeabilitou	permeabilita	k1gFnSc7	permeabilita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikové	fyzik	k1gMnPc1	fyzik
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
však	však	k9	však
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
dána	dán	k2eAgFnSc1d1	dána
relativně	relativně	k6eAd1	relativně
k	k	k7c3	k
světlonosnému	světlonosný	k2eAgInSc3d1	světlonosný
éteru	éter	k1gInSc3	éter
<g/>
.	.	kIx.	.
</s>
<s>
Éter	éter	k1gInSc1	éter
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
nekonečně	konečně	k6eNd1	konečně
jemné	jemný	k2eAgNnSc4d1	jemné
médium	médium	k1gNnSc4	médium
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
všechny	všechen	k3xTgFnPc4	všechen
látky	látka	k1gFnPc4	látka
pronikají	pronikat	k5eAaImIp3nP	pronikat
a	a	k8xC	a
které	který	k3yRgNnSc1	který
současně	současně	k6eAd1	současně
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
veškerý	veškerý	k3xTgInSc4	veškerý
prostor	prostor	k1gInSc4	prostor
kolem	kolem	k7c2	kolem
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgFnPc2	tento
představ	představa	k1gFnPc2	představa
se	se	k3xPyFc4	se
světlo	světlo	k1gNnSc1	světlo
mohlo	moct	k5eAaImAgNnS	moct
šířit	šířit	k5eAaImF	šířit
právě	právě	k9	právě
jen	jen	k9	jen
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
éteru	éter	k1gInSc2	éter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
byl	být	k5eAaImAgInS	být
uskutečněn	uskutečnit	k5eAaPmNgMnS	uskutečnit
fyziky	fyzik	k1gMnPc7	fyzik
Albertem	Albert	k1gMnSc7	Albert
Michelsonem	Michelson	k1gMnSc7	Michelson
a	a	k8xC	a
Edwardem	Edward	k1gMnSc7	Edward
Morleyem	Morley	k1gMnSc7	Morley
významný	významný	k2eAgInSc4d1	významný
experiment	experiment	k1gInSc4	experiment
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
změření	změření	k1gNnSc1	změření
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
tohoto	tento	k3xDgInSc2	tento
experimentu	experiment	k1gInSc2	experiment
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
zvaného	zvaný	k2eAgMnSc2d1	zvaný
Michelson-Morleyův	Michelson-Morleyův	k2eAgInSc4d1	Michelson-Morleyův
experiment	experiment	k1gInSc4	experiment
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
měření	měření	k1gNnSc1	měření
rychlosti	rychlost	k1gFnSc2	rychlost
Země	zem	k1gFnSc2	zem
pohybující	pohybující	k2eAgFnSc2d1	pohybující
se	se	k3xPyFc4	se
domnělým	domnělý	k2eAgInSc7d1	domnělý
"	"	kIx"	"
<g/>
světlonosným	světlonosný	k2eAgInSc7d1	světlonosný
éterem	éter	k1gInSc7	éter
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
znázorněno	znázorněn	k2eAgNnSc1d1	znázorněno
na	na	k7c6	na
nákresu	nákres	k1gInSc6	nákres
Michelsonova	Michelsonův	k2eAgInSc2d1	Michelsonův
interferometru	interferometr	k1gInSc2	interferometr
<g/>
,	,	kIx,	,
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
světla	světlo	k1gNnSc2	světlo
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
monochromatické	monochromatický	k2eAgInPc4d1	monochromatický
paprsky	paprsek	k1gInPc4	paprsek
(	(	kIx(	(
<g/>
t.	t.	k?	t.
j.	j.	k?	j.
mající	mající	k2eAgMnSc1d1	mající
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
délku	délka	k1gFnSc4	délka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
šíří	šířit	k5eAaImIp3nS	šířit
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
úhlu	úhel	k1gInSc6	úhel
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
polopropustné	polopropustný	k2eAgNnSc1d1	polopropustné
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
s	s	k7c7	s
tenkou	tenký	k2eAgFnSc7d1	tenká
vrstvou	vrstva	k1gFnSc7	vrstva
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
tohoto	tento	k3xDgNnSc2	tento
dělícího	dělící	k2eAgNnSc2d1	dělící
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc1	dva
paprsky	paprsek	k1gInPc1	paprsek
odrážejí	odrážet	k5eAaImIp3nP	odrážet
několikrát	několikrát	k6eAd1	několikrát
mezi	mezi	k7c7	mezi
dalšími	další	k2eAgNnPc7d1	další
zrcadly	zrcadlo	k1gNnPc7	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	k9	aby
oba	dva	k4xCgInPc1	dva
paprsky	paprsek	k1gInPc1	paprsek
urazily	urazit	k5eAaPmAgInP	urazit
stejnou	stejný	k2eAgFnSc4d1	stejná
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
počet	počet	k1gInSc1	počet
odrazů	odraz	k1gInPc2	odraz
shodný	shodný	k2eAgMnSc1d1	shodný
(	(	kIx(	(
<g/>
během	během	k7c2	během
skutečného	skutečný	k2eAgInSc2d1	skutečný
Michelson-Morleyova	Michelson-Morleyův	k2eAgInSc2d1	Michelson-Morleyův
experimentu	experiment	k1gInSc2	experiment
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
více	hodně	k6eAd2	hodně
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
obrázku	obrázek	k1gInSc6	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
následném	následný	k2eAgNnSc6d1	následné
sloučení	sloučení	k1gNnSc6	sloučení
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
obrazec	obrazec	k1gInSc1	obrazec
konstruktivní	konstruktivní	k2eAgFnSc2d1	konstruktivní
a	a	k8xC	a
destruktivní	destruktivní	k2eAgFnSc2d1	destruktivní
interference	interference	k1gFnSc2	interference
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
malá	malý	k2eAgFnSc1d1	malá
změna	změna	k1gFnSc1	změna
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
některém	některý	k3yIgInSc6	některý
z	z	k7c2	z
ramen	rameno	k1gNnPc2	rameno
interferometru	interferometr	k1gInSc2	interferometr
(	(	kIx(	(
<g/>
způsobená	způsobený	k2eAgFnSc1d1	způsobená
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
přístroj	přístroj	k1gInSc1	přístroj
společně	společně	k6eAd1	společně
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
měl	mít	k5eAaImAgMnS	mít
pohybovat	pohybovat	k5eAaImF	pohybovat
předpokládaným	předpokládaný	k2eAgMnSc7d1	předpokládaný
"	"	kIx"	"
<g/>
éterem	éter	k1gInSc7	éter
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
zapříčinit	zapříčinit	k5eAaPmF	zapříčinit
změnu	změna	k1gFnSc4	změna
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
paprsek	paprsek	k1gInSc1	paprsek
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
na	na	k7c6	na
překonání	překonání	k1gNnSc6	překonání
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
projevit	projevit	k5eAaPmF	projevit
jako	jako	k9	jako
změna	změna	k1gFnSc1	změna
interferenčního	interferenční	k2eAgInSc2d1	interferenční
obrazce	obrazec	k1gInSc2	obrazec
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
zařízení	zařízení	k1gNnSc1	zařízení
se	se	k3xPyFc4	se
otáčelo	otáčet	k5eAaImAgNnS	otáčet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
dráha	dráha	k1gFnSc1	dráha
paprsků	paprsek	k1gInPc2	paprsek
v	v	k7c6	v
"	"	kIx"	"
<g/>
éteru	éter	k1gInSc6	éter
<g/>
"	"	kIx"	"
vlivem	vlivem	k7c2	vlivem
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlosti	rychlost	k1gFnPc1	rychlost
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
Země	zem	k1gFnPc1	zem
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgInP	mít
sčítat	sčítat	k5eAaImF	sčítat
<g/>
.	.	kIx.	.
</s>
<s>
Experiment	experiment	k1gInSc1	experiment
neměl	mít	k5eNaImAgInS	mít
žádný	žádný	k3yNgInSc4	žádný
výsledek	výsledek	k1gInSc4	výsledek
<g/>
,	,	kIx,	,
ať	ať	k9	ať
bylo	být	k5eAaImAgNnS	být
aparaturou	aparatura	k1gFnSc7	aparatura
otáčeno	otáčet	k5eAaImNgNnS	otáčet
jakkoliv	jakkoliv	k6eAd1	jakkoliv
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejznámějším	známý	k2eAgInSc7d3	nejznámější
a	a	k8xC	a
nejužitečnějším	užitečný	k2eAgInSc7d3	nejužitečnější
neúspěšným	úspěšný	k2eNgInSc7d1	neúspěšný
experimentem	experiment	k1gInSc7	experiment
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Česko-rakouský	českoakouský	k2eAgMnSc1d1	česko-rakouský
fyzik	fyzik	k1gMnSc1	fyzik
Ernst	Ernst	k1gMnSc1	Ernst
Mach	Mach	k1gMnSc1	Mach
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
experiment	experiment	k1gInSc1	experiment
vlastně	vlastně	k9	vlastně
vyvrátil	vyvrátit	k5eAaPmAgInS	vyvrátit
teorii	teorie	k1gFnSc4	teorie
éteru	éter	k1gInSc2	éter
<g/>
.	.	kIx.	.
</s>
<s>
Pokrok	pokrok	k1gInSc1	pokrok
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
nabízel	nabízet	k5eAaImAgInS	nabízet
alternativní	alternativní	k2eAgFnSc4d1	alternativní
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
Lorentz-Fitzgeraldovu	Lorentz-Fitzgeraldův	k2eAgFnSc4d1	Lorentz-Fitzgeraldův
kontrakci	kontrakce	k1gFnSc4	kontrakce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dovolila	dovolit	k5eAaPmAgFnS	dovolit
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
i	i	k9	i
negativní	negativní	k2eAgInSc4d1	negativní
výsledek	výsledek	k1gInSc4	výsledek
Michelson-Morleyova	Michelson-Morleyův	k2eAgInSc2d1	Michelson-Morleyův
experimentu	experiment	k1gInSc2	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
znal	znát	k5eAaImAgMnS	znát
výsledek	výsledek	k1gInSc4	výsledek
Michelson-Morleyova	Michelson-Morleyův	k2eAgInSc2d1	Michelson-Morleyův
experimentu	experiment	k1gInSc2	experiment
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	on	k3xPp3gInSc4	on
nulový	nulový	k2eAgInSc4d1	nulový
výsledek	výsledek	k1gInSc4	výsledek
velmi	velmi	k6eAd1	velmi
pomohl	pomoct	k5eAaPmAgMnS	pomoct
všeobecnému	všeobecný	k2eAgNnSc3d1	všeobecné
přijetí	přijetí	k1gNnSc3	přijetí
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Einsteinova	Einsteinův	k2eAgFnSc1d1	Einsteinova
teorie	teorie	k1gFnSc1	teorie
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
výsledkem	výsledek	k1gInSc7	výsledek
experimentu	experiment	k1gInSc2	experiment
<g/>
:	:	kIx,	:
éter	éter	k1gInSc1	éter
neexistoval	existovat	k5eNaImAgInS	existovat
a	a	k8xC	a
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
byla	být	k5eAaImAgFnS	být
stejná	stejný	k2eAgFnSc1d1	stejná
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Konstantní	konstantní	k2eAgFnSc1d1	konstantní
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
kauzalitou	kauzalita	k1gFnSc7	kauzalita
a	a	k8xC	a
rovnocenností	rovnocennost	k1gFnSc7	rovnocennost
inerciálních	inerciální	k2eAgFnPc2d1	inerciální
vztažných	vztažný	k2eAgFnPc2d1	vztažná
soustav	soustava	k1gFnPc2	soustava
<g/>
)	)	kIx)	)
jedním	jeden	k4xCgNnSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgNnPc2d1	základní
východisek	východisko	k1gNnPc2	východisko
speciální	speciální	k2eAgFnSc1d1	speciální
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Rýchlosť	Rýchlosť	k1gMnSc2	Rýchlosť
svetla	svetnout	k5eAaPmAgFnS	svetnout
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
Teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
Nadsvětelná	Nadsvětelný	k2eAgFnSc1d1	Nadsvětelná
rychlost	rychlost	k1gFnSc1	rychlost
Kauzalita	kauzalita	k1gFnSc1	kauzalita
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Optika	optika	k1gFnSc1	optika
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
historie	historie	k1gFnSc2	historie
pátrání	pátrání	k1gNnSc2	pátrání
po	po	k7c6	po
podstatě	podstata	k1gFnSc6	podstata
světla	světlo	k1gNnSc2	světlo
na	na	k7c6	na
serveru	server	k1gInSc6	server
scienceworld	scienceworlda	k1gFnPc2	scienceworlda
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Stavba	stavba	k1gFnSc1	stavba
Michelsonova	Michelsonův	k2eAgInSc2d1	Michelsonův
interferometru	interferometr	k1gInSc2	interferometr
a	a	k8xC	a
ověření	ověření	k1gNnSc4	ověření
jeho	jeho	k3xOp3gFnSc2	jeho
funkce	funkce	k1gFnSc2	funkce
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Interaktivní	interaktivní	k2eAgFnSc1d1	interaktivní
animace	animace	k1gFnSc1	animace
Michelson-Morley	Michelson-Morlea	k1gFnSc2	Michelson-Morlea
experimentu	experiment	k1gInSc2	experiment
</s>
