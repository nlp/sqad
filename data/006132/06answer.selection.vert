<s>
Rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgNnSc2d1	jiné
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
je	být	k5eAaImIp3nS	být
definována	definován	k2eAgFnSc1d1	definována
jako	jako	k8xS	jako
fázová	fázový	k2eAgFnSc1d1	fázová
rychlost	rychlost	k1gFnSc1	rychlost
postupného	postupný	k2eAgNnSc2d1	postupné
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
vlnění	vlnění	k1gNnSc2	vlnění
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
.	.	kIx.	.
</s>
