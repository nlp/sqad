<s>
Hard	Hard	k6eAd1	Hard
rock	rock	k1gInSc1	rock
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgInSc1d1	vycházející
z	z	k7c2	z
rock	rock	k1gInSc4	rock
and	and	k?	and
rollu	roll	k1gInSc2	roll
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
kořeny	kořen	k1gInPc4	kořen
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
garage	garag	k1gInSc2	garag
a	a	k8xC	a
Blues-rocku	Bluesock	k1gInSc2	Blues-rock
(	(	kIx(	(
<g/>
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
i	i	k9	i
psychedelický	psychedelický	k2eAgInSc1d1	psychedelický
rock	rock	k1gInSc1	rock
<g/>
)	)	kIx)	)
začátku	začátek	k1gInSc2	začátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
deštník	deštník	k1gInSc1	deštník
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
styly	styl	k1gInPc4	styl
jako	jako	k8xS	jako
punk	punk	k1gInSc4	punk
a	a	k8xC	a
grunge	grunge	k1gFnSc4	grunge
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
pop-rocku	popock	k1gInSc2	pop-rock
<g/>
,	,	kIx,	,
hraného	hraný	k2eAgInSc2d1	hraný
v	v	k7c6	v
rádiích	rádius	k1gInPc6	rádius
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristický	charakteristický	k2eAgMnSc1d1	charakteristický
je	být	k5eAaImIp3nS	být
zkresleným	zkreslený	k2eAgMnSc7d1	zkreslený
(	(	kIx(	(
<g/>
silově	silově	k6eAd1	silově
deformovaným	deformovaný	k2eAgInSc7d1	deformovaný
<g/>
)	)	kIx)	)
zvukem	zvuk	k1gInSc7	zvuk
elektrických	elektrický	k2eAgFnPc2d1	elektrická
kytar	kytara	k1gFnPc2	kytara
<g/>
,	,	kIx,	,
baskytary	baskytara	k1gFnSc2	baskytara
a	a	k8xC	a
bicích	bicí	k2eAgInPc2d1	bicí
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
Hard	Hard	k1gInSc1	Hard
Rock	rock	k1gInSc1	rock
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
aplikován	aplikovat	k5eAaBmNgInS	aplikovat
na	na	k7c4	na
víc	hodně	k6eAd2	hodně
stylů	styl	k1gInPc2	styl
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
odchylkami	odchylka	k1gFnPc7	odchylka
"	"	kIx"	"
<g/>
radiového	radiový	k2eAgMnSc2d1	radiový
<g/>
"	"	kIx"	"
pop	pop	k1gInSc1	pop
rocku	rock	k1gInSc2	rock
a	a	k8xC	a
jen	jen	k9	jen
všeobecně	všeobecně	k6eAd1	všeobecně
navazují	navazovat	k5eAaImIp3nP	navazovat
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
punk	punk	k1gInSc1	punk
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
využívá	využívat	k5eAaImIp3nS	využívat
rychlejší	rychlý	k2eAgNnSc4d2	rychlejší
tempo	tempo	k1gNnSc4	tempo
a	a	k8xC	a
méně	málo	k6eAd2	málo
akordů	akord	k1gInPc2	akord
(	(	kIx(	(
<g/>
také	také	k9	také
silově	silově	k6eAd1	silově
deformovaných	deformovaný	k2eAgFnPc2d1	deformovaná
<g/>
)	)	kIx)	)
a	a	k8xC	a
styl	styl	k1gInSc1	styl
grunge	grungat	k5eAaPmIp3nS	grungat
kombinující	kombinující	k2eAgInSc1d1	kombinující
punk	punk	k1gInSc1	punk
rock	rock	k1gInSc1	rock
s	s	k7c7	s
heavy	heava	k1gFnPc4	heava
metalem	metal	k1gInSc7	metal
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pětistupňová	pětistupňový	k2eAgFnSc1d1	pětistupňová
notová	notový	k2eAgFnSc1d1	notová
škála	škála	k1gFnSc1	škála
typická	typický	k2eAgFnSc1d1	typická
pro	pro	k7c4	pro
blues	blues	k1gNnSc4	blues
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
tradičního	tradiční	k2eAgInSc2d1	tradiční
rokenrolu	rokenrol	k1gInSc2	rokenrol
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přebírá	přebírat	k5eAaImIp3nS	přebírat
prvky	prvek	k1gInPc4	prvek
z	z	k7c2	z
klasického	klasický	k2eAgNnSc2d1	klasické
blues	blues	k1gNnSc2	blues
<g/>
,	,	kIx,	,
hard	hard	k6eAd1	hard
rock	rock	k1gInSc1	rock
využívá	využívat	k5eAaPmIp3nS	využívat
prvky	prvek	k1gInPc4	prvek
britského	britský	k2eAgNnSc2d1	Britské
blues	blues	k1gNnSc2	blues
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
rychlejší	rychlý	k2eAgInSc4d2	rychlejší
rytmus	rytmus	k1gInSc4	rytmus
a	a	k8xC	a
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
více	hodně	k6eAd2	hodně
moderních	moderní	k2eAgInPc2d1	moderní
nástrojů	nástroj	k1gInPc2	nástroj
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
elektrické	elektrický	k2eAgFnPc4d1	elektrická
kytary	kytara	k1gFnPc4	kytara
(	(	kIx(	(
<g/>
sólové	sólový	k2eAgFnPc4d1	sólová
a	a	k8xC	a
doprovodné	doprovodný	k2eAgFnPc4d1	doprovodná
<g/>
)	)	kIx)	)
a	a	k8xC	a
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgFnSc1d1	bicí
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
klávesové	klávesový	k2eAgInPc4d1	klávesový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Britské	britský	k2eAgFnPc1d1	britská
rockové	rockový	k2eAgFnPc1d1	rocková
skupiny	skupina	k1gFnPc1	skupina
jako	jako	k9	jako
například	například	k6eAd1	například
The	The	k1gFnSc1	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gInSc1	Stones
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Beatles	beatles	k1gMnSc1	beatles
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Yardbirds	Yardbirds	k1gInSc1	Yardbirds
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Who	Who	k1gFnSc1	Who
a	a	k8xC	a
The	The	k1gFnSc1	The
Kinks	Kinksa	k1gFnPc2	Kinksa
modifikovaly	modifikovat	k5eAaBmAgFnP	modifikovat
standardní	standardní	k2eAgInSc4d1	standardní
rock	rock	k1gInSc4	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
roll	roll	k1gMnSc1	roll
přidáváním	přidávání	k1gNnSc7	přidávání
tvrdšího	tvrdý	k2eAgInSc2d2	tvrdší
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
těžšími	těžký	k2eAgInPc7d2	těžší
kytarovými	kytarový	k2eAgInPc7d1	kytarový
riffy	riff	k1gInPc7	riff
<g/>
,	,	kIx,	,
dunivými	dunivý	k2eAgInPc7d1	dunivý
bicími	bicí	k2eAgInPc7d1	bicí
a	a	k8xC	a
hlasitým	hlasitý	k2eAgInSc7d1	hlasitý
zpěvem	zpěv	k1gInSc7	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zvuk	zvuk	k1gInSc1	zvuk
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
hardrockový	hardrockový	k2eAgInSc4d1	hardrockový
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Rané	raný	k2eAgFnPc1d1	raná
skladby	skladba	k1gFnPc1	skladba
byly	být	k5eAaImAgFnP	být
například	například	k6eAd1	například
Helter	Helter	k1gInSc4	Helter
Skelter	Skeltra	k1gFnPc2	Skeltra
<g/>
,	,	kIx,	,
I	i	k9	i
Want	Want	k1gMnSc1	Want
You	You	k1gMnSc1	You
(	(	kIx(	(
<g/>
She	She	k1gMnSc1	She
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
So	So	kA	So
heavy	heav	k1gInPc4	heav
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hey	Hey	k1gMnSc1	Hey
bulldog	bulldog	k1gMnSc1	bulldog
od	od	k7c2	od
The	The	k1gFnSc2	The
Beatles	Beatles	k1gFnSc2	Beatles
<g/>
,	,	kIx,	,
I	i	k8xC	i
Can	Can	k1gFnSc3	Can
See	See	k1gFnSc3	See
for	forum	k1gNnPc2	forum
Miles	Miles	k1gInSc1	Miles
od	od	k7c2	od
The	The	k1gFnSc2	The
Who	Who	k1gFnSc2	Who
a	a	k8xC	a
"	"	kIx"	"
<g/>
You	You	k1gFnSc1	You
Really	Realla	k1gMnSc2	Realla
Got	Got	k1gMnSc2	Got
Me	Me	k1gMnSc2	Me
od	od	k7c2	od
The	The	k1gFnSc2	The
Kinks	Kinksa	k1gFnPc2	Kinksa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
produkoval	produkovat	k5eAaImAgMnS	produkovat
novou	nový	k2eAgFnSc4d1	nová
formu	forma	k1gFnSc4	forma
blues	blues	k1gFnSc2	blues
ovlivněnou	ovlivněný	k2eAgFnSc4d1	ovlivněná
psychedelickým	psychedelický	k2eAgInSc7d1	psychedelický
rockem	rock	k1gInSc7	rock
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
kombinací	kombinace	k1gFnSc7	kombinace
jazzu	jazz	k1gInSc2	jazz
<g/>
,	,	kIx,	,
blues	blues	k1gNnSc4	blues
a	a	k8xC	a
rock	rock	k1gInSc4	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
rollu	roll	k1gInSc3	roll
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
kytaristů	kytarista	k1gMnPc2	kytarista
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
experimentovali	experimentovat	k5eAaImAgMnP	experimentovat
s	s	k7c7	s
novými	nový	k2eAgInPc7d1	nový
kytarovými	kytarový	k2eAgInPc7d1	kytarový
efekty	efekt	k1gInPc7	efekt
jako	jako	k8xC	jako
byla	být	k5eAaImAgFnS	být
modulace	modulace	k1gFnSc1	modulace
<g/>
,	,	kIx,	,
zpětná	zpětný	k2eAgFnSc1d1	zpětná
vazba	vazba	k1gFnSc1	vazba
a	a	k8xC	a
deformace	deformace	k1gFnSc1	deformace
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
skupiny	skupina	k1gFnSc2	skupina
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbath	k1gInSc1	Sabbath
a	a	k8xC	a
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
začaly	začít	k5eAaPmAgFnP	začít
mísit	mísit	k5eAaImF	mísit
rané	raný	k2eAgFnPc1d1	raná
britské	britský	k2eAgFnPc1d1	britská
rockové	rockový	k2eAgFnPc1d1	rocková
skladby	skladba	k1gFnPc1	skladba
do	do	k7c2	do
tvrdší	tvrdý	k2eAgFnSc2d2	tvrdší
ohraničené	ohraničený	k2eAgFnSc2d1	ohraničená
formy	forma	k1gFnSc2	forma
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nazývaly	nazývat	k5eAaImAgFnP	nazývat
Blues	blues	k1gNnSc1	blues
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
blues-rockového	bluesockový	k2eAgInSc2d1	blues-rockový
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zároveň	zároveň	k6eAd1	zároveň
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
začátky	začátek	k1gInPc4	začátek
hard	harda	k1gFnPc2	harda
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
I.	I.	kA	I.
Ve	v	k7c6	v
skladbách	skladba	k1gFnPc6	skladba
alba	album	k1gNnPc4	album
jsou	být	k5eAaImIp3nP	být
jasné	jasný	k2eAgInPc1d1	jasný
základy	základ	k1gInPc1	základ
blues	blues	k1gNnSc2	blues
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
známých	známý	k2eAgFnPc2d1	známá
skladeb	skladba	k1gFnPc2	skladba
bluesových	bluesový	k2eAgMnPc2d1	bluesový
muzikantů	muzikant	k1gMnPc2	muzikant
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
album	album	k1gNnSc1	album
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
(	(	kIx(	(
<g/>
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
III	III	kA	III
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
oproti	oproti	k7c3	oproti
druhému	druhý	k4xOgInSc3	druhý
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
progresivního	progresivní	k2eAgNnSc2d1	progresivní
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
tvrdě	tvrdě	k6eAd1	tvrdě
laděného	laděný	k2eAgInSc2d1	laděný
<g/>
)	)	kIx)	)
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
skupiny	skupina	k1gFnSc2	skupina
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
byla	být	k5eAaImAgFnS	být
v	v	k7c4	v
hard	hard	k1gInSc4	hard
rocku	rock	k1gInSc2	rock
vskutku	vskutku	k9	vskutku
revoluční	revoluční	k2eAgFnSc7d1	revoluční
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
typická	typický	k2eAgFnSc1d1	typická
temně	temně	k6eAd1	temně
laděnými	laděný	k2eAgInPc7d1	laděný
texty	text	k1gInPc7	text
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
tvrdými	tvrdý	k2eAgInPc7d1	tvrdý
kytarovými	kytarový	k2eAgInPc7d1	kytarový
riffy	riff	k1gInPc7	riff
a	a	k8xC	a
atmosférou	atmosféra	k1gFnSc7	atmosféra
těžké	těžký	k2eAgFnSc2d1	těžká
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
atmosféra	atmosféra	k1gFnSc1	atmosféra
transformovala	transformovat	k5eAaBmAgFnS	transformovat
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
hard	hard	k1gInSc4	hard
rock	rock	k1gInSc4	rock
do	do	k7c2	do
formy	forma	k1gFnSc2	forma
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
skupina	skupina	k1gFnSc1	skupina
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
vydala	vydat	k5eAaPmAgFnS	vydat
první	první	k4xOgMnPc4	první
heavy	heav	k1gMnPc4	heav
metalové	metalový	k2eAgNnSc1d1	metalové
album	album	k1gNnSc1	album
"	"	kIx"	"
<g/>
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
na	na	k7c4	na
hardrockovou	hardrockový	k2eAgFnSc4d1	hardrocková
scénu	scéna	k1gFnSc4	scéna
uvedla	uvést	k5eAaPmAgFnS	uvést
skupina	skupina	k1gFnSc1	skupina
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
svými	svůj	k3xOyFgFnPc7	svůj
alby	alba	k1gFnSc2	alba
"	"	kIx"	"
<g/>
Shades	Shades	k1gMnSc1	Shades
of	of	k?	of
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Book	Book	k1gMnSc1	Book
of	of	k?	of
Taliesyn	Taliesyn	k1gMnSc1	Taliesyn
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
a	a	k8xC	a
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hardrocková	hardrockový	k2eAgFnSc1d1	hardrocková
transformace	transformace	k1gFnSc1	transformace
skupiny	skupina	k1gFnSc2	skupina
Deep	Deep	k1gInSc1	Deep
Purple	Purple	k1gFnSc4	Purple
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
alby	alba	k1gFnPc4	alba
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
in	in	k?	in
Rock	rock	k1gInSc1	rock
a	a	k8xC	a
Machine	Machin	k1gInSc5	Machin
Head	Heado	k1gNnPc2	Heado
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
považováno	považován	k2eAgNnSc1d1	považováno
i	i	k9	i
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
prototypů	prototyp	k1gInPc2	prototyp
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
speed	speed	k1gInSc4	speed
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
Highway	Highwaa	k1gFnSc2	Highwaa
Star	Star	kA	Star
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považovaná	považovaný	k2eAgFnSc1d1	považovaná
za	za	k7c4	za
první	první	k4xOgFnSc4	první
speed	speed	k1gMnSc1	speed
metalovou	metalový	k2eAgFnSc4d1	metalová
skladbu	skladba	k1gFnSc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
Nazareth	Nazareth	k1gInSc1	Nazareth
<g/>
,	,	kIx,	,
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
spojení	spojení	k1gNnSc4	spojení
mezi	mezi	k7c7	mezi
hardrockem	hardrock	k1gInSc7	hardrock
a	a	k8xC	a
komerčním	komerční	k2eAgInSc7d1	komerční
úspěchem	úspěch	k1gInSc7	úspěch
při	při	k7c6	při
vydání	vydání	k1gNnSc6	vydání
desky	deska	k1gFnSc2	deska
Hair	Hair	k1gInSc4	Hair
of	of	k?	of
the	the	k?	the
Dog	doga	k1gFnPc2	doga
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
způsobila	způsobit	k5eAaPmAgFnS	způsobit
nárůst	nárůst	k1gInSc4	nárůst
počtu	počet	k1gInSc2	počet
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
začaly	začít	k5eAaPmAgFnP	začít
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Hard	Hard	k1gInSc1	Hard
rock	rock	k1gInSc1	rock
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
členit	členit	k5eAaImF	členit
do	do	k7c2	do
dalších	další	k2eAgInPc2d1	další
"	"	kIx"	"
<g/>
subžánrů	subžánr	k1gInPc2	subžánr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Alice	Alice	k1gFnSc1	Alice
Cooper	Coopra	k1gFnPc2	Coopra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
vydal	vydat	k5eAaPmAgMnS	vydat
první	první	k4xOgMnSc1	první
tzv.	tzv.	kA	tzv.
shock	shock	k6eAd1	shock
rockové	rockový	k2eAgNnSc4d1	rockové
album	album	k1gNnSc4	album
"	"	kIx"	"
<g/>
School	School	k1gInSc1	School
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Out	Out	k1gFnSc7	Out
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
vydali	vydat	k5eAaPmAgMnP	vydat
své	svůj	k3xOyFgInPc4	svůj
debuty	debut	k1gInPc4	debut
Aerosmith	Aerosmitha	k1gFnPc2	Aerosmitha
<g/>
,	,	kIx,	,
Queen	Quena	k1gFnPc2	Quena
a	a	k8xC	a
Lynyrd	Lynyrda	k1gFnPc2	Lynyrda
Skynyrd	Skynyrda	k1gFnPc2	Skynyrda
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
svými	svůj	k3xOyFgMnPc7	svůj
alby	alba	k1gFnPc1	alba
demonstrovali	demonstrovat	k5eAaBmAgMnP	demonstrovat
možnosti	možnost	k1gFnPc1	možnost
rozšíření	rozšíření	k1gNnSc2	rozšíření
hardockového	hardockový	k2eAgInSc2d1	hardockový
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
skupina	skupina	k1gFnSc1	skupina
Bad	Bad	k1gMnSc2	Bad
Company	Compana	k1gFnSc2	Compana
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
Queen	Quena	k1gFnPc2	Quena
přišla	přijít	k5eAaPmAgFnS	přijít
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
třetím	třetí	k4xOgNnSc7	třetí
albem	album	k1gNnSc7	album
Sheer	Sheer	k1gInSc1	Sheer
Heart	Heart	k1gInSc1	Heart
Attack	Attacka	k1gFnPc2	Attacka
a	a	k8xC	a
skladbou	skladba	k1gFnSc7	skladba
Stone	ston	k1gInSc5	ston
Cold	Colda	k1gFnPc2	Colda
Crazy	Craza	k1gFnPc4	Craza
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
pozdější	pozdní	k2eAgInSc4d2	pozdější
Thrash	Thrash	k1gInSc4	Thrash
metalové	metalový	k2eAgMnPc4d1	metalový
muzikanty	muzikant	k1gMnPc4	muzikant
jako	jako	k8xC	jako
Metallica	Metallica	k1gMnSc1	Metallica
a	a	k8xC	a
Megadeth	Megadeth	k1gMnSc1	Megadeth
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Queen	Quena	k1gFnPc2	Quena
dokázala	dokázat	k5eAaPmAgFnS	dokázat
dokonce	dokonce	k9	dokonce
spojit	spojit	k5eAaPmF	spojit
zpěv	zpěv	k1gInSc4	zpěv
<g/>
,	,	kIx,	,
kytarovou	kytarový	k2eAgFnSc4d1	kytarová
hudbu	hudba	k1gFnSc4	hudba
s	s	k7c7	s
glam	glam	k6eAd1	glam
rockem	rock	k1gInSc7	rock
<g/>
,	,	kIx,	,
heavy	heav	k1gMnPc4	heav
metalem	metal	k1gInSc7	metal
<g/>
,	,	kIx,	,
progresivním	progresivní	k2eAgInSc7d1	progresivní
rockem	rock	k1gInSc7	rock
a	a	k8xC	a
operou	opera	k1gFnSc7	opera
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Kiss	Kissa	k1gFnPc2	Kissa
vydala	vydat	k5eAaPmAgFnS	vydat
téměř	téměř	k6eAd1	téměř
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
svoje	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnPc1	první
tři	tři	k4xCgNnPc1	tři
alba	album	k1gNnPc1	album
<g/>
:	:	kIx,	:
Kiss	Kiss	k1gInSc1	Kiss
<g/>
,	,	kIx,	,
Hotter	Hotter	k1gMnSc1	Hotter
Than	Than	k1gMnSc1	Than
Hell	Hell	k1gMnSc1	Hell
a	a	k8xC	a
Dressed	Dressed	k1gMnSc1	Dressed
to	ten	k3xDgNnSc1	ten
Kill	Kill	k1gInSc4	Kill
a	a	k8xC	a
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
komerčního	komerční	k2eAgInSc2d1	komerční
úspěchu	úspěch	k1gInSc2	úspěch
svým	svůj	k3xOyFgNnSc7	svůj
koncertním	koncertní	k2eAgNnSc7d1	koncertní
dvojalbem	dvojalbum	k1gNnSc7	dvojalbum
Alive	Aliev	k1gFnSc2	Aliev
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Smrtí	smrt	k1gFnSc7	smrt
Tommyho	Tommy	k1gMnSc2	Tommy
Bolina	Bolin	k2eAgMnSc2d1	Bolin
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
skupina	skupina	k1gFnSc1	skupina
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
zemřel	zemřít	k5eAaPmAgMnS	zemřít
při	při	k7c6	při
leteckém	letecký	k2eAgNnSc6d1	letecké
neštěstí	neštěstí	k1gNnSc6	neštěstí
Ronnie	Ronnie	k1gFnSc2	Ronnie
Van	vana	k1gFnPc2	vana
Zant	Zant	k1gMnSc1	Zant
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
z	z	k7c2	z
Lynyrd	Lynyrda	k1gFnPc2	Lynyrda
Skynyrd	Skynyrda	k1gFnPc2	Skynyrda
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
předávkování	předávkování	k1gNnSc6	předávkování
drogami	droga	k1gFnPc7	droga
Keith	Keith	k1gMnSc1	Keith
Moon	Moon	k1gMnSc1	Moon
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc2	The
Who	Who	k1gMnSc2	Who
<g/>
.	.	kIx.	.
</s>
<s>
Pronikáním	pronikání	k1gNnSc7	pronikání
stylu	styl	k1gInSc2	styl
disco	disco	k1gNnSc2	disco
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
punk	punk	k1gInSc1	punk
rocku	rock	k1gInSc2	rock
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
začal	začít	k5eAaPmAgInS	začít
hard	hard	k6eAd1	hard
rock	rock	k1gInSc4	rock
ztrácet	ztrácet	k5eAaImF	ztrácet
na	na	k7c6	na
popularitě	popularita	k1gFnSc6	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
disco	disco	k1gNnSc2	disco
oslovoval	oslovovat	k5eAaImAgInS	oslovovat
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
širší	široký	k2eAgNnSc4d2	širší
publikum	publikum	k1gNnSc4	publikum
a	a	k8xC	a
punk	punk	k1gInSc4	punk
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
víc	hodně	k6eAd2	hodně
rebelující	rebelující	k2eAgFnSc4d1	rebelující
mládež	mládež	k1gFnSc4	mládež
než	než	k8xS	než
hard	hard	k6eAd1	hard
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
i	i	k9	i
skupina	skupina	k1gFnSc1	skupina
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
posunula	posunout	k5eAaPmAgFnS	posunout
od	od	k7c2	od
temného	temný	k2eAgInSc2d1	temný
stylu	styl	k1gInSc2	styl
svých	svůj	k3xOyFgNnPc2	svůj
raných	raný	k2eAgNnPc2d1	rané
děl	dělo	k1gNnPc2	dělo
vydáním	vydání	k1gNnSc7	vydání
alba	alba	k1gFnSc1	alba
Technical	Technical	k1gMnSc3	Technical
Ecstasy	Ecstasa	k1gFnSc2	Ecstasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
hardrocková	hardrockový	k2eAgFnSc1d1	hardrocková
skupina	skupina	k1gFnSc1	skupina
Van	vana	k1gFnPc2	vana
Halen	halena	k1gFnPc2	halena
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
byla	být	k5eAaImAgFnS	být
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
kytarové	kytarový	k2eAgFnSc6d1	kytarová
zručnosti	zručnost	k1gFnSc6	zručnost
jejich	jejich	k3xOp3gMnSc2	jejich
sólového	sólový	k2eAgMnSc2d1	sólový
kytaristy	kytarista	k1gMnSc2	kytarista
Eddieho	Eddie	k1gMnSc2	Eddie
Van	van	k1gInSc4	van
Halena	halit	k5eAaImNgFnS	halit
<g/>
.	.	kIx.	.
</s>
<s>
Podmaňující	podmaňující	k2eAgFnSc1d1	podmaňující
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
song	song	k1gInSc1	song
Eruption	Eruption	k1gInSc1	Eruption
z	z	k7c2	z
alba	album	k1gNnSc2	album
Van	vana	k1gFnPc2	vana
Halen	halena	k1gFnPc2	halena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
když	když	k8xS	když
australská	australský	k2eAgFnSc1d1	australská
skupina	skupina	k1gFnSc1	skupina
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgInPc4	svůj
šesté	šestý	k4xOgNnSc1	šestý
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
Highway	Highwaa	k1gFnSc2	Highwaa
to	ten	k3xDgNnSc1	ten
Hell	Hell	k1gInSc4	Hell
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
prohlubovat	prohlubovat	k5eAaImF	prohlubovat
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
metalem	metal	k1gInSc7	metal
a	a	k8xC	a
hardrockem	hardrock	k1gInSc7	hardrock
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
byla	být	k5eAaImAgFnS	být
víc	hodně	k6eAd2	hodně
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B.	B.	kA	B.
Rok	rok	k1gInSc4	rok
1979	[number]	k4	1979
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
rokem	rok	k1gInSc7	rok
rozchodů	rozchod	k1gInPc2	rozchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
vrcholí	vrcholit	k5eAaImIp3nP	vrcholit
rozpory	rozpor	k1gInPc1	rozpor
mezi	mezi	k7c7	mezi
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Ozzy	Ozza	k1gMnSc2	Ozza
Osbournem	Osbourn	k1gInSc7	Osbourn
a	a	k8xC	a
kytaristou	kytarista	k1gMnSc7	kytarista
Tony	Tony	k1gMnSc1	Tony
Iommim	Iommim	k1gMnSc1	Iommim
uvnitř	uvnitř	k7c2	uvnitř
skupiny	skupina	k1gFnSc2	skupina
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
.	.	kIx.	.
</s>
<s>
Osbourne	Osbournout	k5eAaPmIp3nS	Osbournout
záhy	záhy	k6eAd1	záhy
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
opouští	opouštět	k5eAaImIp3nS	opouštět
<g/>
.	.	kIx.	.
</s>
<s>
Názorové	názorový	k2eAgFnPc1d1	názorová
rozepře	rozepře	k1gFnPc1	rozepře
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
nevyhýbají	vyhýbat	k5eNaImIp3nP	vyhýbat
skupině	skupina	k1gFnSc3	skupina
Rainbow	Rainbow	k1gMnSc2	Rainbow
bývalého	bývalý	k2eAgMnSc2d1	bývalý
kytaristy	kytarista	k1gMnSc2	kytarista
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
<g/>
,	,	kIx,	,
Ritchie	Ritchie	k1gFnSc1	Ritchie
Blackmora	Blackmora	k1gFnSc1	Blackmora
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
opouští	opouštět	k5eAaImIp3nS	opouštět
frontman	frontman	k1gMnSc1	frontman
Ronnie	Ronnie	k1gFnSc2	Ronnie
James	James	k1gMnSc1	James
Dio	Dio	k1gMnSc1	Dio
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
záhy	záhy	k6eAd1	záhy
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
Ozzyho	Ozzy	k1gMnSc4	Ozzy
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
UFO	UFO	kA	UFO
opouští	opouštět	k5eAaImIp3nS	opouštět
kytarista	kytarista	k1gMnSc1	kytarista
Michael	Michael	k1gMnSc1	Michael
Schenker	Schenker	k1gMnSc1	Schenker
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osudné	osudný	k2eAgFnSc6d1	osudná
smrti	smrt	k1gFnSc6	smrt
bubeníka	bubeník	k1gMnSc2	bubeník
skupiny	skupina	k1gFnSc2	skupina
Johna	John	k1gMnSc2	John
Bonhama	Bonham	k1gMnSc2	Bonham
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
skupina	skupina	k1gFnSc1	skupina
Led	led	k1gInSc4	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
Bon	bona	k1gFnPc2	bona
Scott	Scott	k1gInSc4	Scott
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
zemřel	zemřít	k5eAaPmAgMnS	zemřít
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Smrtí	smrt	k1gFnSc7	smrt
těchto	tento	k3xDgFnPc2	tento
osobností	osobnost	k1gFnPc2	osobnost
končila	končit	k5eAaImAgFnS	končit
éra	éra	k1gFnSc1	éra
klasických	klasický	k2eAgFnPc2d1	klasická
hardrockových	hardrockový	k2eAgFnPc2d1	hardrocková
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Queen	Queen	k2eAgInSc1d1	Queen
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stylově	stylově	k6eAd1	stylově
přiblížily	přiblížit	k5eAaPmAgFnP	přiblížit
k	k	k7c3	k
popu	pop	k1gInSc3	pop
<g/>
.	.	kIx.	.
</s>
<s>
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
nahráli	nahrát	k5eAaBmAgMnP	nahrát
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Brianem	Brian	k1gMnSc7	Brian
Johnsonem	Johnson	k1gMnSc7	Johnson
album	album	k1gNnSc4	album
Back	Back	k1gMnSc1	Back
in	in	k?	in
Black	Black	k1gInSc1	Black
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
druhým	druhý	k4xOgInSc7	druhý
nejprodávanějším	prodávaný	k2eAgFnPc3d3	nejprodávanější
albem	album	k1gNnSc7	album
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
žánru	žánr	k1gInSc6	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
úspěchu	úspěch	k1gInSc6	úspěch
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
i	i	k9	i
Van	van	k1gInSc1	van
Halen	halen	k2eAgInSc1d1	halen
vydávat	vydávat	k5eAaPmF	vydávat
alba	album	k1gNnPc4	album
s	s	k7c7	s
hardrockem	hardrock	k1gInSc7	hardrock
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
víc	hodně	k6eAd2	hodně
přijatelnější	přijatelný	k2eAgMnSc1d2	přijatelnější
pro	pro	k7c4	pro
rádia	rádio	k1gNnPc4	rádio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
americká	americký	k2eAgFnSc1d1	americká
skupina	skupina	k1gFnSc1	skupina
Mötley	Mötlea	k1gFnSc2	Mötlea
Crüe	Crüe	k1gFnSc1	Crüe
vydala	vydat	k5eAaPmAgFnS	vydat
album	album	k1gNnSc4	album
Too	Too	k1gFnSc2	Too
Fats	Fatsa	k1gFnPc2	Fatsa
For	forum	k1gNnPc2	forum
Love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
vznikající	vznikající	k2eAgInSc4d1	vznikající
žánr	žánr	k1gInSc4	žánr
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
glam	glam	k1gInSc1	glam
metal	metat	k5eAaImAgInS	metat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
žánrem	žánr	k1gInSc7	žánr
úspěšně	úspěšně	k6eAd1	úspěšně
prorazily	prorazit	k5eAaPmAgFnP	prorazit
skupiny	skupina	k1gFnPc1	skupina
Twisted	Twisted	k1gMnSc1	Twisted
Sister	Sister	k1gMnSc1	Sister
a	a	k8xC	a
Quiet	Quiet	k1gMnSc1	Quiet
Riot	Riot	k1gMnSc1	Riot
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Metal	metat	k5eAaImAgInS	metat
Health	Health	k1gInSc4	Health
od	od	k7c2	od
Quiet	Quieta	k1gFnPc2	Quieta
Riot	Riot	k1gMnSc1	Riot
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgMnSc7	první
heavy	heav	k1gInPc1	heav
metalovým	metalový	k2eAgNnSc7d1	metalové
albem	album	k1gNnSc7	album
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
první	první	k4xOgFnPc4	první
příčky	příčka	k1gFnPc4	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Bilboard	Bilboarda	k1gFnPc2	Bilboarda
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
hard	hard	k6eAd1	hard
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
Def	Def	k1gFnSc1	Def
Leppard	Leppard	k1gInSc1	Leppard
vydala	vydat	k5eAaPmAgFnS	vydat
album	album	k1gNnSc4	album
Pyromania	Pyromanium	k1gNnSc2	Pyromanium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
druhé	druhý	k4xOgFnPc4	druhý
příčky	příčka	k1gFnPc4	příčka
v	v	k7c6	v
amerických	americký	k2eAgInPc6d1	americký
žebříčcích	žebříček	k1gInPc6	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
styl	styl	k1gInSc4	styl
byl	být	k5eAaImAgMnS	být
mixem	mix	k1gInSc7	mix
glam	glam	k1gInSc4	glam
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
klasického	klasický	k2eAgInSc2d1	klasický
rocku	rock	k1gInSc2	rock
nahraným	nahraný	k2eAgMnSc7d1	nahraný
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
přijatelný	přijatelný	k2eAgMnSc1d1	přijatelný
pro	pro	k7c4	pro
rádia	rádio	k1gNnPc4	rádio
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc4	způsob
mixovaní	mixovaný	k2eAgMnPc1d1	mixovaný
stylů	styl	k1gInPc2	styl
potom	potom	k6eAd1	potom
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
mnoho	mnoho	k4c4	mnoho
hardrockových	hardrockový	k2eAgFnPc2d1	hardrocková
a	a	k8xC	a
glamrockových	glamrockův	k2eAgFnPc2d1	glamrockův
skupin	skupina	k1gFnPc2	skupina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
Mötley	Mötlea	k1gFnSc2	Mötlea
Crüe	Crüe	k1gFnPc2	Crüe
vydali	vydat	k5eAaPmAgMnP	vydat
album	album	k1gNnSc4	album
Shout	Shout	k1gMnSc1	Shout
at	at	k?	at
the	the	k?	the
Devil	Devil	k1gInSc1	Devil
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
obrovským	obrovský	k2eAgInSc7d1	obrovský
hitem	hit	k1gInSc7	hit
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
1984	[number]	k4	1984
od	od	k7c2	od
Van	vana	k1gFnPc2	vana
Halena	halena	k1gFnSc1	halena
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
obrovského	obrovský	k2eAgNnSc2d1	obrovské
úspěchu	úspěch	k1gInSc3	úspěch
druhou	druhý	k4xOgFnSc7	druhý
příčkou	příčka	k1gFnSc7	příčka
v	v	k7c6	v
albumovém	albumový	k2eAgInSc6d1	albumový
žebříčku	žebříček	k1gInSc6	žebříček
Bilboardu	Bilboard	k1gInSc2	Bilboard
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Jump	Jump	k1gInSc1	Jump
<g/>
"	"	kIx"	"
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
singlovém	singlový	k2eAgInSc6d1	singlový
žebříčku	žebříček	k1gInSc6	žebříček
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považovaná	považovaný	k2eAgFnSc1d1	považovaná
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgInPc2d3	nejpopulárnější
rockových	rockový	k2eAgInPc2d1	rockový
songů	song	k1gInPc2	song
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
zkomponován	zkomponován	k2eAgInSc1d1	zkomponován
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předešlých	předešlý	k2eAgFnPc2d1	předešlá
klasických	klasický	k2eAgFnPc2d1	klasická
kytarových	kytarový	k2eAgFnPc2d1	kytarová
skladeb	skladba	k1gFnPc2	skladba
Van	vana	k1gFnPc2	vana
Halen	halena	k1gFnPc2	halena
použil	použít	k5eAaPmAgInS	použít
v	v	k7c6	v
albu	album	k1gNnSc6	album
1984	[number]	k4	1984
klávesy	kláves	k1gInPc7	kláves
a	a	k8xC	a
syntezátory	syntezátor	k1gInPc7	syntezátor
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodný	pozoruhodný	k2eAgInSc4d1	pozoruhodný
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
žebříčcích	žebříček	k1gInPc6	žebříček
popularity	popularita	k1gFnSc2	popularita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
také	také	k9	také
Guns	Guns	k1gInSc4	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
albem	album	k1gNnSc7	album
Appetite	Appetit	k1gInSc5	Appetit
for	forum	k1gNnPc2	forum
Destruction	Destruction	k1gInSc4	Destruction
a	a	k8xC	a
Def	Def	k1gFnSc4	Def
Leppard	Lepparda	k1gFnPc2	Lepparda
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
alb	alba	k1gFnPc2	alba
Bilboardu	Bilboarda	k1gFnSc4	Bilboarda
s	s	k7c7	s
albem	album	k1gNnSc7	album
Hysteria	Hysterium	k1gNnSc2	Hysterium
<g/>
,	,	kIx,	,
Mötley	Mötlea	k1gFnSc2	Mötlea
Crüe	Crü	k1gFnSc2	Crü
albem	album	k1gNnSc7	album
Girls	girl	k1gFnPc2	girl
<g/>
,	,	kIx,	,
Girls	girl	k1gFnPc2	girl
<g/>
,	,	kIx,	,
Girls	girl	k1gFnPc2	girl
<g/>
,	,	kIx,	,
Whitesnake	Whitesnake	k1gFnSc4	Whitesnake
vydali	vydat	k5eAaPmAgMnP	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Skupiny	skupina	k1gFnPc1	skupina
Dinosaur	Dinosaur	k1gMnSc1	Dinosaur
Jr	Jr	k1gMnSc1	Jr
a	a	k8xC	a
Sonic	Sonic	k1gMnSc1	Sonic
Youth	Youth	k1gMnSc1	Youth
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
první	první	k4xOgInPc1	první
úspěchy	úspěch	k1gInPc1	úspěch
pro	pro	k7c4	pro
underground	underground	k1gInSc4	underground
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
proudem	proud	k1gInSc7	proud
vývoje	vývoj	k1gInSc2	vývoj
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
skupina	skupina	k1gFnSc1	skupina
Skid	Skida	k1gFnPc2	Skida
Row	Row	k1gFnSc1	Row
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
nazvané	nazvaný	k2eAgInPc1d1	nazvaný
Skid	Skid	k1gInSc1	Skid
Row	Row	k1gFnSc2	Row
a	a	k8xC	a
vydané	vydaný	k2eAgFnPc4d1	vydaná
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
Skupina	skupina	k1gFnSc1	skupina
Mother	Mothra	k1gFnPc2	Mothra
Love	lov	k1gInSc5	lov
Bone	bon	k1gInSc5	bon
měla	mít	k5eAaImAgNnP	mít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
krátký	krátký	k2eAgInSc4d1	krátký
oslnivý	oslnivý	k2eAgInSc4d1	oslnivý
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
přeměnou	přeměna	k1gFnSc7	přeměna
na	na	k7c4	na
skupinu	skupina	k1gFnSc4	skupina
Pearl	Pearla	k1gFnPc2	Pearla
Jam	jáma	k1gFnPc2	jáma
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
den	den	k1gInSc1	den
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Freddie	Freddie	k1gFnSc1	Freddie
Mercury	Mercura	k1gFnPc1	Mercura
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
AIDS	aids	k1gInSc4	aids
<g/>
.	.	kIx.	.
</s>
<s>
Začátek	začátek	k1gInSc1	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
v	v	k7c4	v
znamení	znamení	k1gNnSc4	znamení
skupin	skupina	k1gFnPc2	skupina
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
a	a	k8xC	a
Metallica	Metallica	k1gMnSc1	Metallica
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byly	být	k5eAaImAgInP	být
vydána	vydán	k2eAgFnSc1d1	vydána
jejich	jejich	k3xOp3gFnSc1	jejich
multiplatinová	multiplatinový	k2eAgFnSc1d1	multiplatinová
alba	alba	k1gFnSc1	alba
<g/>
:	:	kIx,	:
Black	Black	k1gInSc1	Black
Album	album	k1gNnSc1	album
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Metallica	Metallicum	k1gNnSc2	Metallicum
a	a	k8xC	a
Use	usus	k1gInSc5	usus
Your	Your	k1gInSc4	Your
Illusion	Illusion	k1gInSc1	Illusion
I	I	kA	I
a	a	k8xC	a
II	II	kA	II
od	od	k7c2	od
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
zachránilo	zachránit	k5eAaPmAgNnS	zachránit
nové	nový	k2eAgNnSc1d1	nové
směřování	směřování	k1gNnSc1	směřování
hardrockového	hardrockový	k2eAgInSc2d1	hardrockový
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
grunge	grunge	k1gNnSc1	grunge
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
představiteli	představitel	k1gMnPc7	představitel
stylu	styl	k1gInSc2	styl
grunge	grung	k1gInSc2	grung
byly	být	k5eAaImAgFnP	být
skupiny	skupina	k1gFnPc1	skupina
Nirvana	Nirvan	k1gMnSc2	Nirvan
<g/>
,	,	kIx,	,
Pearl	Pearl	k1gInSc4	Pearl
Jam	jáma	k1gFnPc2	jáma
<g/>
,	,	kIx,	,
Soundgarden	Soundgardna	k1gFnPc2	Soundgardna
a	a	k8xC	a
Alice	Alice	k1gFnSc2	Alice
in	in	k?	in
Chains	Chains	k1gInSc1	Chains
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgMnS	být
grunge	grungat	k5eAaPmIp3nS	grungat
krom	krom	k7c2	krom
punk	punk	k1gInSc4	punk
rocku	rock	k1gInSc2	rock
<g/>
/	/	kIx~	/
<g/>
hardcore	hardcor	k1gInSc5	hardcor
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
i	i	k9	i
klasickým	klasický	k2eAgInSc7d1	klasický
hard	hard	k1gInSc1	hard
rockem	rock	k1gInSc7	rock
a	a	k8xC	a
heavy	heav	k1gMnPc4	heav
metalem	metal	k1gInSc7	metal
<g/>
,	,	kIx,	,
skupiny	skupina	k1gFnPc1	skupina
vystupovaly	vystupovat	k5eAaImAgFnP	vystupovat
proti	proti	k7c3	proti
pompézním	pompézní	k2eAgInPc3d1	pompézní
heavy	heav	k1gInPc1	heav
rockovým	rockový	k2eAgInSc7d1	rockový
kapelám	kapela	k1gFnPc3	kapela
čímž	což	k3yQnSc7	což
znegovaly	znegovat	k5eAaPmAgFnP	znegovat
celá	celý	k2eAgNnPc4d1	celé
osmdesátá	osmdesátý	k4xOgNnPc4	osmdesátý
léta	léto	k1gNnPc4	léto
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
nesla	nést	k5eAaImAgFnS	nést
na	na	k7c6	na
vlně	vlna	k1gFnSc6	vlna
glam	glam	k6eAd1	glam
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
skupiny	skupina	k1gFnPc1	skupina
Soundgarden	Soundgardna	k1gFnPc2	Soundgardna
<g/>
,	,	kIx,	,
Alice	Alice	k1gFnSc1	Alice
in	in	k?	in
Chains	Chainsa	k1gFnPc2	Chainsa
a	a	k8xC	a
Nirvana	Nirvan	k1gMnSc2	Nirvan
byly	být	k5eAaImAgFnP	být
ovlivněny	ovlivnit	k5eAaPmNgFnP	ovlivnit
metalovou	metalový	k2eAgFnSc7d1	metalová
hudbou	hudba	k1gFnSc7	hudba
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Alice	Alice	k1gFnSc2	Alice
in	in	k?	in
Chains	Chains	k1gInSc1	Chains
měla	mít	k5eAaImAgFnS	mít
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
heavy	heava	k1gFnSc2	heava
metalový	metalový	k2eAgInSc4d1	metalový
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
proudu	proud	k1gInSc2	proud
se	se	k3xPyFc4	se
začlenily	začlenit	k5eAaPmAgFnP	začlenit
i	i	k9	i
skupiny	skupina	k1gFnPc1	skupina
The	The	k1gMnPc2	The
Screaming	Screaming	k1gInSc4	Screaming
Trees	Treesa	k1gFnPc2	Treesa
<g/>
,	,	kIx,	,
Mudhoney	Mudhonea	k1gFnSc2	Mudhonea
a	a	k8xC	a
pár	pár	k4xCyI	pár
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
neměly	mít	k5eNaImAgFnP	mít
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Stone	ston	k1gInSc5	ston
Temple	templ	k1gInSc5	templ
Pilots	Pilotsa	k1gFnPc2	Pilotsa
<g/>
,	,	kIx,	,
Nudeswirl	Nudeswirla	k1gFnPc2	Nudeswirla
<g/>
,	,	kIx,	,
Prong	Pronga	k1gFnPc2	Pronga
a	a	k8xC	a
Hum	hum	k0wR	hum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
skupiny	skupina	k1gFnPc1	skupina
Swervedriwer	Swervedriwero	k1gNnPc2	Swervedriwero
<g/>
,	,	kIx,	,
Catherine	Catherin	k1gInSc5	Catherin
Wheel	Wheel	k1gInSc4	Wheel
a	a	k8xC	a
Ride	Rid	k1gInPc4	Rid
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
demonstrovaly	demonstrovat	k5eAaBmAgInP	demonstrovat
kytarovou	kytarový	k2eAgFnSc4d1	kytarová
hudbu	hudba	k1gFnSc4	hudba
hardrockových	hardrockový	k2eAgFnPc2d1	hardrocková
kapel	kapela	k1gFnPc2	kapela
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
popularita	popularita	k1gFnSc1	popularita
skupin	skupina	k1gFnPc2	skupina
z	z	k7c2	z
80	[number]	k4	80
<g/>
.	.	kIx.	.
do	do	k7c2	do
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Metallica	Metallic	k2eAgFnSc1d1	Metallica
<g/>
,	,	kIx,	,
přetrvávala	přetrvávat	k5eAaImAgFnS	přetrvávat
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
skupiny	skupina	k1gFnPc1	skupina
začaly	začít	k5eAaPmAgFnP	začít
opět	opět	k6eAd1	opět
hrát	hrát	k5eAaImF	hrát
mix	mix	k1gInSc4	mix
metalu	metal	k1gInSc2	metal
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
elektrickými	elektrický	k2eAgInPc7d1	elektrický
nástroji	nástroj	k1gInPc7	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
byly	být	k5eAaImAgFnP	být
známy	znám	k2eAgFnPc1d1	známa
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
alternativní	alternativní	k2eAgMnPc1d1	alternativní
metaloví	metalový	k2eAgMnPc1d1	metalový
muzikanti	muzikant	k1gMnPc1	muzikant
<g/>
,	,	kIx,	,
odnož	odnož	k1gFnSc4	odnož
alternativního	alternativní	k2eAgInSc2d1	alternativní
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Primus	primus	k1gMnSc1	primus
<g/>
,	,	kIx,	,
Red	Red	k1gMnSc1	Red
Hot	hot	k0	hot
Chili	Chile	k1gFnSc3	Chile
Peppers	Peppers	k1gInSc1	Peppers
<g/>
,	,	kIx,	,
Rage	Rage	k1gFnSc1	Rage
Against	Against	k1gFnSc1	Against
the	the	k?	the
Machine	Machin	k1gMnSc5	Machin
<g/>
,	,	kIx,	,
Living	Living	k1gInSc4	Living
Colour	Coloura	k1gFnPc2	Coloura
a	a	k8xC	a
White	Whit	k1gMnSc5	Whit
Zombie	Zombius	k1gMnSc5	Zombius
<g/>
,	,	kIx,	,
spojovali	spojovat	k5eAaImAgMnP	spojovat
funky	funk	k1gInPc4	funk
s	s	k7c7	s
metalovým	metalový	k2eAgInSc7d1	metalový
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Faith	Faith	k1gMnSc1	Faith
No	no	k9	no
More	mor	k1gInSc5	mor
<g/>
,	,	kIx,	,
Mr	Mr	k1gFnSc5	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bungle	Bungle	k6eAd1	Bungle
spojili	spojit	k5eAaPmAgMnP	spojit
s	s	k7c7	s
hardrockem	hardrock	k1gInSc7	hardrock
ještě	ještě	k9	ještě
víc	hodně	k6eAd2	hodně
žánrů	žánr	k1gInPc2	žánr
od	od	k7c2	od
rapu	rap	k1gMnSc3	rap
po	po	k7c4	po
soul	soul	k1gInSc4	soul
<g/>
...	...	k?	...
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
Aerosmith	Aerosmith	k1gMnSc1	Aerosmith
Airbourne	Airbourn	k1gInSc5	Airbourn
<g/>
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
Alice	Alice	k1gFnSc2	Alice
Cooper	Cooper	k1gMnSc1	Cooper
Amboy	Amboa	k1gFnSc2	Amboa
Dukes	Dukes	k1gMnSc1	Dukes
April	April	k1gMnSc1	April
Wine	Wine	k1gFnPc2	Wine
Asia	Asia	k1gMnSc1	Asia
Avenged	Avenged	k1gMnSc1	Avenged
Sevenfold	Sevenfold	k1gMnSc1	Sevenfold
<g/>
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Bad	Bad	k1gMnSc1	Bad
Company	Compana	k1gFnSc2	Compana
Blackfoot	Blackfoot	k1gMnSc1	Blackfoot
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
Black	Black	k1gMnSc1	Black
Veil	Veil	k1gMnSc1	Veil
Brides	Brides	k1gMnSc1	Brides
Blue	Blu	k1gFnSc2	Blu
<g />
.	.	kIx.	.
</s>
<s>
Cheer	Cheer	k1gMnSc1	Cheer
Blue	Blu	k1gFnSc2	Blu
Murder	Murder	k1gMnSc1	Murder
Blue	Blue	k1gFnPc2	Blue
Öyster	Öystra	k1gFnPc2	Öystra
Cult	Cult	k2eAgInSc1d1	Cult
Boston	Boston	k1gInSc1	Boston
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
Budgie	Budgie	k1gFnSc2	Budgie
Cactus	Cactus	k1gInSc1	Cactus
Cream	Cream	k1gInSc1	Cream
Deep	Deep	k1gInSc1	Deep
Purple	Purple	k1gMnSc2	Purple
Def	Def	k1gMnSc2	Def
Leppard	Leppard	k1gMnSc1	Leppard
Rick	Rick	k1gMnSc1	Rick
Deringer	Deringer	k1gMnSc1	Deringer
Dio	Dio	k1gMnSc1	Dio
Europe	Europ	k1gInSc5	Europ
Evanescence	Evanescence	k1gFnSc2	Evanescence
Focus	Focus	k1gMnSc1	Focus
Foreigner	Foreigner	k1gMnSc1	Foreigner
Free	Free	k1gFnSc1	Free
Gamma	Gamm	k1gMnSc2	Gamm
Gary	Gara	k1gMnSc2	Gara
Moore	Moor	k1gInSc5	Moor
Genesis	Genesis	k1gFnSc1	Genesis
Girl	girl	k1gFnSc2	girl
Girlschool	Girlschool	k1gInSc1	Girlschool
Grand	grand	k1gMnSc1	grand
Funk	funk	k1gInSc1	funk
Railroad	Railroad	k1gInSc1	Railroad
Guns	Guns	k1gInSc4	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
HIM	HIM	kA	HIM
Cheap	Cheap	k1gMnSc1	Cheap
Trick	Trick	k1gMnSc1	Trick
Ian	Ian	k1gMnSc1	Ian
Gillan	Gillan	k1gMnSc1	Gillan
James	James	k1gMnSc1	James
Gang	gang	k1gInSc4	gang
Jethro	Jethra	k1gFnSc5	Jethra
Tull	Tull	k1gInSc1	Tull
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc1	Hendrix
Joan	Joan	k1gMnSc1	Joan
<g />
.	.	kIx.	.
</s>
<s>
Jett	Jett	k2eAgInSc1d1	Jett
&	&	k?	&
Black	Black	k1gInSc1	Black
Hearts	Hearts	k1gInSc1	Hearts
Journey	Journea	k1gFnSc2	Journea
Judas	Judasa	k1gFnPc2	Judasa
Priest	Priest	k1gFnSc1	Priest
Kansas	Kansas	k1gInSc1	Kansas
Kiss	Kiss	k1gInSc1	Kiss
Krokus	krokus	k1gInSc4	krokus
L.	L.	kA	L.
A.	A.	kA	A.
Guns	Guns	k1gInSc1	Guns
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
Lordi	lord	k1gMnPc1	lord
Meat	Meat	k1gMnSc1	Meat
Loaf	Loaf	k1gMnSc1	Loaf
Magnum	Magnum	k1gInSc4	Magnum
Mahogany	Mahogana	k1gFnSc2	Mahogana
Rush	Rusha	k1gFnPc2	Rusha
Metallica	Metallica	k1gMnSc1	Metallica
Molly	Molla	k1gFnSc2	Molla
Hatchet	Hatchet	k1gMnSc1	Hatchet
Montrose	Montrosa	k1gFnSc6	Montrosa
Michael	Michael	k1gMnSc1	Michael
Schenker	Schenkra	k1gFnPc2	Schenkra
Group	Group	k1gMnSc1	Group
Mötley	Mötlea	k1gFnSc2	Mötlea
Crüe	Crüe	k1gInSc1	Crüe
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
Motörhead	Motörhead	k1gInSc1	Motörhead
Mountain	Mountain	k1gMnSc1	Mountain
Nazareth	Nazareth	k1gMnSc1	Nazareth
Neurotic	Neurotice	k1gFnPc2	Neurotice
Outsiders	Outsidersa	k1gFnPc2	Outsidersa
Ozzy	Ozza	k1gFnSc2	Ozza
Osbourne	Osbourn	k1gInSc5	Osbourn
Papa	papa	k1gMnSc1	papa
Roach	Roacha	k1gFnPc2	Roacha
Queen	Queen	k1gInSc1	Queen
Quiet	Quiet	k1gMnSc1	Quiet
Riot	Riot	k1gMnSc1	Riot
Rainbow	Rainbow	k1gMnSc1	Rainbow
Riot	Riot	k2eAgInSc4d1	Riot
Rolling	Rolling	k1gInSc4	Rolling
Stones	Stones	k1gMnSc1	Stones
Rose	Rose	k1gMnSc1	Rose
Tattoo	Tattoo	k1gMnSc1	Tattoo
<g />
.	.	kIx.	.
</s>
<s>
Rush	Rush	k1gMnSc1	Rush
Saxon	Saxon	k1gMnSc1	Saxon
Scorpions	Scorpions	k1gInSc4	Scorpions
Slade	slad	k1gInSc5	slad
Status	status	k1gInSc1	status
Quo	Quo	k1gMnSc3	Quo
Styx	Styx	k1gInSc1	Styx
Suzi	Suzi	k1gNnSc2	Suzi
Quatro	Quatro	k1gNnSc1	Quatro
Sweet	Sweet	k1gInSc1	Sweet
Ted	Ted	k1gMnSc1	Ted
Nugent	Nugent	k1gMnSc1	Nugent
The	The	k1gMnSc1	The
Who	Who	k1gMnSc1	Who
Thin	Thin	k1gMnSc1	Thin
Lizzy	Lizza	k1gFnSc2	Lizza
Trapeze	Trapeze	k1gFnSc2	Trapeze
T.	T.	kA	T.
<g/>
Rex	Rex	k1gMnSc7	Rex
Pat	pata	k1gFnPc2	pata
Travers	travers	k1gInSc1	travers
Triumph	Triumph	k1gMnSc1	Triumph
Twisted	Twisted	k1gMnSc1	Twisted
Sister	Sister	k1gMnSc1	Sister
Uriah	Uriah	k1gMnSc1	Uriah
Heep	Heep	k1gMnSc1	Heep
Van	van	k1gInSc4	van
Halen	halen	k2eAgInSc4d1	halen
Whitesnake	Whitesnakus	k1gMnSc5	Whitesnakus
Wishbone	Wishbon	k1gMnSc5	Wishbon
Ash	Ash	k1gMnSc5	Ash
ZZ	ZZ	kA	ZZ
Top	topit	k5eAaImRp2nS	topit
Jiří	Jiří	k1gMnSc1	Jiří
Schelinger	Schelinger	k1gMnSc1	Schelinger
&	&	k?	&
Skupina	skupina	k1gFnSc1	skupina
F.	F.	kA	F.
<g/>
R.	R.	kA	R.
<g/>
Čecha	Čech	k1gMnSc2	Čech
Arakain	Arakain	k1gMnSc1	Arakain
Black	Black	k1gMnSc1	Black
Bull	bulla	k1gFnPc2	bulla
Citron	citron	k1gInSc4	citron
Coda	coda	k1gFnSc1	coda
Dead	Deada	k1gFnPc2	Deada
Daniels	Danielsa	k1gFnPc2	Danielsa
Elán	elán	k1gInSc1	elán
Kabát	Kabát	k1gMnSc1	Kabát
Katapult	katapulta	k1gFnPc2	katapulta
Kern	Kern	k1gMnSc1	Kern
Kreyson	Kreyson	k1gMnSc1	Kreyson
Odyssea	Odyssea	k1gFnSc1	Odyssea
Metalinda	Metalind	k1gMnSc2	Metalind
Medium	medium	k1gNnSc1	medium
Olympic	Olympic	k1gMnSc1	Olympic
Projektil	projektil	k1gInSc1	projektil
Red	Red	k1gFnSc1	Red
Baron	baron	k1gMnSc1	baron
Band	band	k1gInSc1	band
Tublatanka	Tublatanka	k1gFnSc1	Tublatanka
Vitacit	Vitacit	k1gMnSc1	Vitacit
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hard	hard	k6eAd1	hard
rock	rock	k1gInSc4	rock
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Rockový	rockový	k2eAgInSc4d1	rockový
portál	portál	k1gInSc4	portál
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Česko-Slovenská	českolovenský	k2eAgFnSc1d1	česko-slovenská
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
Hard	Hardo	k1gNnPc2	Hardo
<g/>
&	&	k?	&
<g/>
Heavy	Heava	k1gFnSc2	Heava
hudby	hudba	k1gFnSc2	hudba
</s>
