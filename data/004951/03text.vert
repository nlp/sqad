<s>
Japonsko-rjúkjúské	japonskojúkjúský	k2eAgInPc1d1	japonsko-rjúkjúský
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
jazykových	jazykový	k2eAgFnPc2d1	jazyková
rodin	rodina	k1gFnPc2	rodina
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
rozšířeny	rozšířit	k5eAaPmNgFnP	rozšířit
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
v	v	k7c6	v
mluvené	mluvený	k2eAgFnSc6d1	mluvená
formě	forma	k1gFnSc6	forma
vzájemně	vzájemně	k6eAd1	vzájemně
nesrozumitelné	srozumitelný	k2eNgFnPc1d1	nesrozumitelná
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
pouhé	pouhý	k2eAgInPc4d1	pouhý
dialekty	dialekt	k1gInPc4	dialekt
japonštiny	japonština	k1gFnSc2	japonština
<g/>
.	.	kIx.	.
</s>
<s>
Příbuznost	příbuznost	k1gFnSc1	příbuznost
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
jazyky	jazyk	k1gInPc7	jazyk
je	být	k5eAaImIp3nS	být
nejasná	jasný	k2eNgFnSc1d1	nejasná
–	–	k?	–
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
hledá	hledat	k5eAaImIp3nS	hledat
u	u	k7c2	u
korejštiny	korejština	k1gFnSc2	korejština
a	a	k8xC	a
jazyků	jazyk	k1gInPc2	jazyk
altajských	altajský	k2eAgInPc2d1	altajský
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
japonsko-rjúkjúské	japonskojúkjúský	k2eAgInPc1d1	japonsko-rjúkjúský
jazyky	jazyk	k1gInPc1	jazyk
byly	být	k5eAaImAgInP	být
výrazně	výrazně	k6eAd1	výrazně
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
původním	původní	k2eAgInSc7d1	původní
jazykovým	jazykový	k2eAgInSc7d1	jazykový
substrátem	substrát	k1gInSc7	substrát
<g/>
,	,	kIx,	,
snad	snad	k9	snad
podobným	podobný	k2eAgInSc7d1	podobný
ainštině	ainština	k1gFnSc3	ainština
či	či	k8xC	či
jazykům	jazyk	k1gInPc3	jazyk
austronéské	austronéský	k2eAgFnSc2d1	austronéský
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Japonština	japonština	k1gFnSc1	japonština
(	(	kIx(	(
<g/>
日	日	k?	日
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
130	[number]	k4	130
milionů	milion	k4xCgInPc2	milion
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
)	)	kIx)	)
hačijó	hačijó	k?	hačijó
–	–	k?	–
starobylé	starobylý	k2eAgFnSc2d1	starobylá
dialkety	dialketa	k1gFnSc2	dialketa
ostrova	ostrov	k1gInSc2	ostrov
Hačijódžima	Hačijódžimum	k1gNnSc2	Hačijódžimum
a	a	k8xC	a
ostrovů	ostrov	k1gInPc2	ostrov
Daitó	Daitó	k1gFnPc2	Daitó
<g/>
.	.	kIx.	.
vlastní	vlastní	k2eAgFnSc1d1	vlastní
japonština	japonština	k1gFnSc1	japonština
východní	východní	k2eAgFnSc1d1	východní
japonština	japonština	k1gFnSc1	japonština
západní	západní	k2eAgFnSc1d1	západní
japonština	japonština	k1gFnSc1	japonština
dialekty	dialekt	k1gInPc1	dialekt
ostrova	ostrov	k1gInSc2	ostrov
Kjúšú	Kjúšú	k1gFnSc2	Kjúšú
sacuma-ben	sacumana	k1gFnPc2	sacuma-bna
–	–	k?	–
dialekt	dialekt	k1gInSc1	dialekt
jižního	jižní	k2eAgMnSc2d1	jižní
Kjúšú	Kjúšú	k1gMnSc2	Kjúšú
(	(	kIx(	(
<g/>
Sacagú	Sacagú	k1gFnSc1	Sacagú
<g/>
)	)	kIx)	)
Rjúkjúské	rjúkjúský	k2eAgInPc1d1	rjúkjúský
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
琉	琉	k?	琉
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
amamština	amamština	k1gFnSc1	amamština
(	(	kIx(	(
<g/>
奄	奄	k?	奄
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
130	[number]	k4	130
000	[number]	k4	000
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
bývají	bývat	k5eAaImIp3nP	bývat
její	její	k3xOp3gFnSc1	její
severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgNnPc1d1	jižní
nářečí	nářečí	k1gNnPc1	nářečí
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
samostatné	samostatný	k2eAgInPc4d1	samostatný
jazyky	jazyk	k1gInPc4	jazyk
<g/>
)	)	kIx)	)
mijakština	mijakština	k1gFnSc1	mijakština
(	(	kIx(	(
<g/>
mjáku	mjáek	k1gMnSc5	mjáek
haci	hac	k1gMnSc5	hac
<g/>
,	,	kIx,	,
宮	宮	k?	宮
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
55	[number]	k4	55
000	[number]	k4	000
mluvčích	mluvčí	k1gFnPc2	mluvčí
<g/>
)	)	kIx)	)
okinawština	okinawština	k1gFnSc1	okinawština
(	(	kIx(	(
<g/>
učinágači	učinágač	k1gMnPc1	učinágač
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
沖	沖	k?	沖
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
900	[number]	k4	900
000	[number]	k4	000
mluvčích	mluvčí	k1gFnPc2	mluvčí
<g/>
)	)	kIx)	)
Kunigamština	Kunigamština	k1gFnSc1	Kunigamština
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
dialekt	dialekt	k1gInSc4	dialekt
okinawštiny	okinawština	k1gFnSc2	okinawština
<g/>
)	)	kIx)	)
jaejamština	jaejamština	k1gFnSc1	jaejamština
(	(	kIx(	(
<g/>
jaima	jaima	k1gFnSc1	jaima
muní	muní	k1gFnSc1	muní
<g/>
,	,	kIx,	,
八	八	k?	八
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
45	[number]	k4	45
000	[number]	k4	000
mluvčích	mluvčí	k1gFnPc2	mluvčí
<g/>
)	)	kIx)	)
jonagunština	jonagunština	k1gFnSc1	jonagunština
(	(	kIx(	(
<g/>
与	与	k?	与
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1	[number]	k4	1
800	[number]	k4	800
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
dialekt	dialekt	k1gInSc4	dialekt
jaejamštiny	jaejamština	k1gFnSc2	jaejamština
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
jazyků	jazyk	k1gInPc2	jazyk
Japonština	japonština	k1gFnSc1	japonština
Rjúkjú	Rjúkjú	k1gNnPc1	Rjúkjú
</s>
