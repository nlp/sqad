<s>
Ďábel	ďábel	k1gMnSc1	ďábel
medvědovitý	medvědovitý	k2eAgMnSc1d1	medvědovitý
(	(	kIx(	(
<g/>
Sarcophilus	Sarcophilus	k1gMnSc1	Sarcophilus
harrisii	harrisie	k1gFnSc4	harrisie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
též	též	k9	též
jako	jako	k9	jako
tasmánský	tasmánský	k2eAgMnSc1d1	tasmánský
čert	čert	k1gMnSc1	čert
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgMnSc1d1	jediný
žijící	žijící	k2eAgMnSc1d1	žijící
druh	druh	k1gMnSc1	druh
rodu	rod	k1gInSc2	rod
Sarcophilus	Sarcophilus	k1gMnSc1	Sarcophilus
a	a	k8xC	a
od	od	k7c2	od
vyhubení	vyhubení	k1gNnSc2	vyhubení
vakovlka	vakovlko	k1gNnSc2	vakovlko
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
i	i	k8xC	i
největší	veliký	k2eAgMnSc1d3	veliký
žijící	žijící	k2eAgMnSc1d1	žijící
dravý	dravý	k2eAgMnSc1d1	dravý
vačnatec	vačnatec	k1gMnSc1	vačnatec
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhled	k1gInSc7	vzhled
připomíná	připomínat	k5eAaImIp3nS	připomínat
malého	malý	k1gMnSc2	malý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nadměrně	nadměrně	k6eAd1	nadměrně
svalnatého	svalnatý	k2eAgNnSc2d1	svalnaté
a	a	k8xC	a
zavalitého	zavalitý	k2eAgMnSc2d1	zavalitý
psa	pes	k1gMnSc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
žije	žít	k5eAaImIp3nS	žít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Tasmánii	Tasmánie	k1gFnSc6	Tasmánie
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc1	jeho
kosterní	kosterní	k2eAgInPc1d1	kosterní
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
však	však	k9	však
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
prakticky	prakticky	k6eAd1	prakticky
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Austrálii	Austrálie	k1gFnSc6	Austrálie
(	(	kIx(	(
<g/>
na	na	k7c6	na
australské	australský	k2eAgFnSc6d1	australská
pevnině	pevnina	k1gFnSc6	pevnina
vyhynul	vyhynout	k5eAaPmAgInS	vyhynout
zhruba	zhruba	k6eAd1	zhruba
400	[number]	k4	400
let	léto	k1gNnPc2	léto
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
prvních	první	k4xOgInPc2	první
evropských	evropský	k2eAgMnPc2d1	evropský
osadníků	osadník	k1gMnPc2	osadník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1788	[number]	k4	1788
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickými	charakteristický	k2eAgInPc7d1	charakteristický
znaky	znak	k1gInPc7	znak
jsou	být	k5eAaImIp3nP	být
černá	černý	k2eAgFnSc1d1	černá
srst	srst	k1gFnSc1	srst
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
hlasitý	hlasitý	k2eAgInSc1d1	hlasitý
hlasový	hlasový	k2eAgInSc1d1	hlasový
projev	projev	k1gInSc1	projev
a	a	k8xC	a
dravý	dravý	k2eAgInSc1d1	dravý
způsob	způsob	k1gInSc1	způsob
příjmu	příjem	k1gInSc2	příjem
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zpravidla	zpravidla	k6eAd1	zpravidla
samotářské	samotářský	k2eAgNnSc1d1	samotářské
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
u	u	k7c2	u
potravy	potrava	k1gFnSc2	potrava
se	se	k3xPyFc4	se
jich	on	k3xPp3gNnPc2	on
schází	scházet	k5eAaImIp3nS	scházet
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dosti	dosti	k6eAd1	dosti
neoprávněnou	oprávněný	k2eNgFnSc4d1	neoprávněná
pověst	pověst	k1gFnSc4	pověst
zákeřného	zákeřný	k2eAgMnSc2d1	zákeřný
a	a	k8xC	a
extrémně	extrémně	k6eAd1	extrémně
zuřivého	zuřivý	k2eAgNnSc2d1	zuřivé
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
faktem	fakt	k1gInSc7	fakt
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
stresujících	stresující	k2eAgFnPc6d1	stresující
podmínkách	podmínka	k1gFnPc6	podmínka
či	či	k8xC	či
zahnáno	zahnán	k2eAgNnSc1d1	zahnáno
do	do	k7c2	do
kouta	kout	k1gInSc2	kout
dokáže	dokázat	k5eAaPmIp3nS	dokázat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
každé	každý	k3xTgNnSc1	každý
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
opravdu	opravdu	k6eAd1	opravdu
zuřivě	zuřivě	k6eAd1	zuřivě
a	a	k8xC	a
nesmiřitelně	smiřitelně	k6eNd1	smiřitelně
bojovat	bojovat	k5eAaImF	bojovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tasmánii	Tasmánie	k1gFnSc6	Tasmánie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
lidé	člověk	k1gMnPc1	člověk
pohlíželi	pohlížet	k5eAaImAgMnP	pohlížet
jako	jako	k9	jako
na	na	k7c4	na
škůdce	škůdce	k1gMnPc4	škůdce
domácích	domácí	k2eAgFnPc2d1	domácí
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
hubeni	hubit	k5eAaImNgMnP	hubit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
vláda	vláda	k1gFnSc1	vláda
začala	začít	k5eAaPmAgFnS	začít
chránit	chránit	k5eAaImF	chránit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
rakovina	rakovina	k1gFnSc1	rakovina
tváře	tvář	k1gFnSc2	tvář
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
velmi	velmi	k6eAd1	velmi
snížila	snížit	k5eAaPmAgFnS	snížit
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
snižuje	snižovat	k5eAaImIp3nS	snižovat
jejich	jejich	k3xOp3gFnSc4	jejich
populaci	populace	k1gFnSc4	populace
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
začala	začít	k5eAaPmAgFnS	začít
s	s	k7c7	s
programy	program	k1gInPc7	program
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
dopadu	dopad	k1gInSc2	dopad
této	tento	k3xDgFnSc2	tento
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Přírodopisec	přírodopisec	k1gMnSc1	přírodopisec
George	Georg	k1gFnSc2	Georg
Prideaux	Prideaux	k1gInSc1	Prideaux
Robert	Robert	k1gMnSc1	Robert
Harris	Harris	k1gInSc1	Harris
(	(	kIx(	(
<g/>
1775	[number]	k4	1775
-	-	kIx~	-
1810	[number]	k4	1810
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
prvního	první	k4xOgInSc2	první
popisu	popis	k1gInSc2	popis
tohoto	tento	k3xDgNnSc2	tento
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1807	[number]	k4	1807
<g/>
.	.	kIx.	.
</s>
<s>
Jím	jíst	k5eAaImIp1nS	jíst
použité	použitý	k2eAgNnSc1d1	Použité
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
Didelphis	Didelphis	k1gFnSc4	Didelphis
ursina	ursino	k1gNnSc2	ursino
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1838	[number]	k4	1838
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
na	na	k7c4	na
Dasyurus	Dasyurus	k1gInSc4	Dasyurus
laniarius	laniarius	k1gInSc4	laniarius
(	(	kIx(	(
<g/>
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
Richard	Richard	k1gMnSc1	Richard
Owen	Owen	k1gMnSc1	Owen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
přeřazení	přeřazení	k1gNnSc6	přeřazení
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Sarcophilus	Sarcophilus	k1gMnSc1	Sarcophilus
druh	druh	k1gMnSc1	druh
dostal	dostat	k5eAaPmAgMnS	dostat
jméno	jméno	k1gNnSc4	jméno
Sarcophilus	Sarcophilus	k1gInSc1	Sarcophilus
harrisii	harrisie	k1gFnSc3	harrisie
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
Harrisův	Harrisův	k2eAgMnSc1d1	Harrisův
milovník	milovník	k1gMnSc1	milovník
masa	maso	k1gNnSc2	maso
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Pierre	Pierr	k1gInSc5	Pierr
Boitard	Boitarda	k1gFnPc2	Boitarda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnSc1d2	pozdější
revize	revize	k1gFnSc1	revize
systematiky	systematika	k1gFnSc2	systematika
<g/>
,	,	kIx,	,
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pokusila	pokusit	k5eAaPmAgFnS	pokusit
o	o	k7c4	o
změnu	změna	k1gFnSc4	změna
jména	jméno	k1gNnSc2	jméno
na	na	k7c4	na
Sarcophilus	Sarcophilus	k1gInSc4	Sarcophilus
laniarius	laniarius	k1gInSc4	laniarius
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vědecká	vědecký	k2eAgFnSc1d1	vědecká
obec	obec	k1gFnSc1	obec
toto	tento	k3xDgNnSc4	tento
přejmenování	přejmenování	k1gNnSc4	přejmenování
nepřijala	přijmout	k5eNaPmAgFnS	přijmout
pro	pro	k7c4	pro
příliš	příliš	k6eAd1	příliš
malý	malý	k2eAgInSc4d1	malý
počet	počet	k1gInSc4	počet
fosílií	fosílie	k1gFnPc2	fosílie
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jméno	jméno	k1gNnSc1	jméno
S.	S.	kA	S.
laniarius	laniarius	k1gInSc4	laniarius
nadále	nadále	k6eAd1	nadále
přináleží	přináležet	k5eAaImIp3nP	přináležet
toliko	toliko	k6eAd1	toliko
fosilnímu	fosilní	k2eAgInSc3d1	fosilní
druhu	druh	k1gInSc3	druh
<g/>
.	.	kIx.	.
</s>
<s>
Fylogenetická	fylogenetický	k2eAgFnSc1d1	fylogenetická
analýza	analýza	k1gFnSc1	analýza
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnPc3	jeho
nejbližším	blízký	k2eAgMnPc3d3	nejbližší
příbuzným	příbuzný	k1gMnPc3	příbuzný
jsou	být	k5eAaImIp3nP	být
kunovci	kunovec	k1gInSc3	kunovec
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
blízkým	blízký	k2eAgNnSc7d1	blízké
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
ne	ne	k9	ne
tak	tak	k6eAd1	tak
blízkým	blízký	k2eAgMnPc3d1	blízký
příbuzným	příbuzný	k1gMnPc3	příbuzný
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
vakovlk	vakovlk	k1gMnSc1	vakovlk
<g/>
.	.	kIx.	.
</s>
<s>
Ďábel	ďábel	k1gMnSc1	ďábel
medvědovitý	medvědovitý	k2eAgMnSc1d1	medvědovitý
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
žijícím	žijící	k2eAgMnSc7d1	žijící
vačnatcem	vačnatec	k1gMnSc7	vačnatec
Tasmánie	Tasmánie	k1gFnSc2	Tasmánie
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
zavalité	zavalitý	k2eAgNnSc1d1	zavalité
tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
osvalené	osvalený	k2eAgFnSc6d1	osvalená
<g/>
,	,	kIx,	,
velkou	velký	k2eAgFnSc4d1	velká
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
ocas	ocas	k1gInSc4	ocas
dosahující	dosahující	k2eAgFnSc1d1	dosahující
asi	asi	k9	asi
poloviny	polovina	k1gFnSc2	polovina
délky	délka	k1gFnSc2	délka
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ocasu	ocas	k1gInSc6	ocas
si	se	k3xPyFc3	se
také	také	k9	také
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
zásoby	zásoba	k1gFnPc4	zásoba
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
podle	podle	k7c2	podle
hubeného	hubený	k2eAgInSc2d1	hubený
ocasu	ocas	k1gInSc2	ocas
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
často	často	k6eAd1	často
poznat	poznat	k5eAaPmF	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zvíře	zvíře	k1gNnSc4	zvíře
nemocné	mocný	k2eNgNnSc4d1	nemocné
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
přední	přední	k2eAgFnPc1d1	přední
nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
o	o	k7c4	o
kousek	kousek	k1gInSc4	kousek
delší	dlouhý	k2eAgInSc1d2	delší
než	než	k8xS	než
zadní	zadní	k2eAgInSc1d1	zadní
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
vačnatce	vačnatec	k1gMnSc4	vačnatec
netypické	typický	k2eNgNnSc1d1	netypické
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
rychlost	rychlost	k1gFnSc4	rychlost
až	až	k9	až
13	[number]	k4	13
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
časté	častý	k2eAgFnPc1d1	častá
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgFnPc1d1	bílá
skvrny	skvrna	k1gFnPc1	skvrna
nebo	nebo	k8xC	nebo
pruhy	pruh	k1gInPc1	pruh
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
a	a	k8xC	a
zadku	zadek	k1gInSc6	zadek
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgMnPc1d2	veliký
než	než	k8xS	než
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
samce	samec	k1gInSc2	samec
je	být	k5eAaImIp3nS	být
65,2	[number]	k4	65,2
cm	cm	kA	cm
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
ocasu	ocas	k1gInSc2	ocas
25,8	[number]	k4	25,8
cm	cm	kA	cm
<g/>
,	,	kIx,	,
váha	váha	k1gFnSc1	váha
8	[number]	k4	8
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
samic	samice	k1gFnPc2	samice
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
hodnoty	hodnota	k1gFnPc1	hodnota
57	[number]	k4	57
cm	cm	kA	cm
<g/>
,	,	kIx,	,
24,4	[number]	k4	24,4
cm	cm	kA	cm
<g/>
,	,	kIx,	,
a	a	k8xC	a
6	[number]	k4	6
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
odhadována	odhadován	k2eAgFnSc1d1	odhadována
na	na	k7c4	na
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
mohou	moct	k5eAaImIp3nP	moct
žít	žít	k5eAaImF	žít
o	o	k7c4	o
něco	něco	k3yInSc4	něco
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obličejové	obličejový	k2eAgFnSc2d1	obličejová
části	část	k1gFnSc2	část
a	a	k8xC	a
z	z	k7c2	z
vrcholku	vrcholek	k1gInSc2	vrcholek
hlavy	hlava	k1gFnSc2	hlava
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
citlivé	citlivý	k2eAgInPc4d1	citlivý
hmatové	hmatový	k2eAgInPc4d1	hmatový
vousy	vous	k1gInPc4	vous
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
zvířeti	zvíře	k1gNnSc3	zvíře
lokalizovat	lokalizovat	k5eAaBmF	lokalizovat
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
jiné	jiný	k2eAgMnPc4d1	jiný
příslušníky	příslušník	k1gMnPc4	příslušník
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rozrušení	rozrušení	k1gNnSc6	rozrušení
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vylučovat	vylučovat	k5eAaImF	vylučovat
silný	silný	k2eAgInSc4d1	silný
zápach	zápach	k1gInSc4	zápach
srovnatelný	srovnatelný	k2eAgInSc4d1	srovnatelný
se	se	k3xPyFc4	se
skunkem	skunk	k1gMnSc7	skunk
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
dominantním	dominantní	k2eAgInSc7d1	dominantní
smyslem	smysl	k1gInSc7	smysl
je	být	k5eAaImIp3nS	být
sluch	sluch	k1gInSc4	sluch
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
výtečný	výtečný	k2eAgInSc4d1	výtečný
čich	čich	k1gInSc4	čich
<g/>
.	.	kIx.	.
</s>
<s>
Poněvadž	poněvadž	k8xS	poněvadž
loví	lovit	k5eAaImIp3nP	lovit
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vidí	vidět	k5eAaImIp3nS	vidět
černobíle	černobíle	k6eAd1	černobíle
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
nejlépe	dobře	k6eAd3	dobře
vidí	vidět	k5eAaImIp3nP	vidět
hýbající	hýbající	k2eAgInPc1d1	hýbající
se	se	k3xPyFc4	se
objekty	objekt	k1gInPc7	objekt
<g/>
,	,	kIx,	,
vidět	vidět	k5eAaImF	vidět
nehybné	hybný	k2eNgInPc4d1	nehybný
předměty	předmět	k1gInPc4	předmět
jim	on	k3xPp3gMnPc3	on
činí	činit	k5eAaImIp3nS	činit
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
síly	síla	k1gFnSc2	síla
stisku	stisk	k1gInSc2	stisk
čelistí	čelist	k1gFnPc2	čelist
savců	savec	k1gMnPc2	savec
prokázala	prokázat	k5eAaPmAgFnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
mezi	mezi	k7c7	mezi
silou	síla	k1gFnSc7	síla
skusu	skus	k1gInSc2	skus
a	a	k8xC	a
velikostí	velikost	k1gFnSc7	velikost
těla	tělo	k1gNnSc2	tělo
má	mít	k5eAaImIp3nS	mít
právě	právě	k9	právě
ďábel	ďábel	k1gMnSc1	ďábel
medvědovitý	medvědovitý	k2eAgMnSc1d1	medvědovitý
největší	veliký	k2eAgFnSc4d3	veliký
relativní	relativní	k2eAgFnSc4d1	relativní
sílu	síla	k1gFnSc4	síla
v	v	k7c6	v
čelistech	čelist	k1gFnPc6	čelist
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgMnPc7	všecek
žijícími	žijící	k2eAgMnPc7d1	žijící
savci	savec	k1gMnPc7	savec
<g/>
.	.	kIx.	.
</s>
<s>
Sílu	síla	k1gFnSc4	síla
stisku	stisk	k1gInSc2	stisk
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
připsat	připsat	k5eAaPmF	připsat
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
tělu	tělo	k1gNnSc3	tělo
veliké	veliký	k2eAgFnSc6d1	veliká
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
jedinou	jediný	k2eAgFnSc4d1	jediná
sadu	sada	k1gFnSc4	sada
chrupu	chrup	k1gInSc2	chrup
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
během	během	k7c2	během
života	život	k1gInSc2	život
stále	stále	k6eAd1	stále
pomalu	pomalu	k6eAd1	pomalu
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
sexuální	sexuální	k2eAgFnSc2d1	sexuální
zralosti	zralost	k1gFnSc2	zralost
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
toho	ten	k3xDgInSc2	ten
okamžiku	okamžik	k1gInSc2	okamžik
jsou	být	k5eAaImIp3nP	být
plodné	plodný	k2eAgFnPc1d1	plodná
jednou	jednou	k6eAd1	jednou
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Páří	pářit	k5eAaImIp3nS	pářit
se	se	k3xPyFc4	se
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
<g/>
,	,	kIx,	,
na	na	k7c6	na
skrytých	skrytý	k2eAgNnPc6d1	skryté
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
bojují	bojovat	k5eAaImIp3nP	bojovat
o	o	k7c4	o
samice	samice	k1gFnPc4	samice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
páří	pářit	k5eAaImIp3nS	pářit
s	s	k7c7	s
dominantním	dominantní	k2eAgInSc7d1	dominantní
samcem	samec	k1gInSc7	samec
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
monogamní	monogamní	k2eAgMnPc1d1	monogamní
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
samice	samice	k1gFnSc1	samice
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pářit	pářit	k5eAaImF	pářit
s	s	k7c7	s
více	hodně	k6eAd2	hodně
samci	samec	k1gInPc7	samec
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
samec	samec	k1gMnSc1	samec
občas	občas	k6eAd1	občas
nehlídá	hlídat	k5eNaImIp3nS	hlídat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
21	[number]	k4	21
dnech	den	k1gInPc6	den
březosti	březost	k1gFnSc2	březost
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
20	[number]	k4	20
–	–	k?	–
30	[number]	k4	30
mláďat	mládě	k1gNnPc2	mládě
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
každé	každý	k3xTgNnSc1	každý
váží	vážit	k5eAaImIp3nS	vážit
0,18	[number]	k4	0,18
–	–	k?	–
0,24	[number]	k4	0,24
gramu	gram	k1gInSc2	gram
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
narození	narození	k1gNnSc6	narození
začne	začít	k5eAaPmIp3nS	začít
tuhý	tuhý	k2eAgInSc1d1	tuhý
boj	boj	k1gInSc1	boj
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
z	z	k7c2	z
vagíny	vagína	k1gFnSc2	vagína
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
kapsy	kapsa	k1gFnSc2	kapsa
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
do	do	k7c2	do
kapsy	kapsa	k1gFnSc2	kapsa
dostanou	dostat	k5eAaPmIp3nP	dostat
<g/>
,	,	kIx,	,
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
přisátá	přisátý	k2eAgNnPc1d1	přisáté
na	na	k7c6	na
bradavce	bradavka	k1gFnSc6	bradavka
dalších	další	k2eAgInPc2d1	další
100	[number]	k4	100
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
kapsa	kapsa	k1gFnSc1	kapsa
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
u	u	k7c2	u
vombata	vombat	k1gMnSc2	vombat
<g/>
)	)	kIx)	)
otevírá	otevírat	k5eAaImIp3nS	otevírat
dozadu	dozadu	k6eAd1	dozadu
–	–	k?	–
ďábel	ďábel	k1gMnSc1	ďábel
medvědovitý	medvědovitý	k2eAgMnSc1d1	medvědovitý
totiž	totiž	k9	totiž
skvěle	skvěle	k6eAd1	skvěle
hrabe	hrabat	k5eAaImIp3nS	hrabat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
kapsa	kapsa	k1gFnSc1	kapsa
otevřená	otevřený	k2eAgFnSc1d1	otevřená
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
hrozilo	hrozit	k5eAaImAgNnS	hrozit
zanesení	zanesení	k1gNnSc1	zanesení
hlínou	hlína	k1gFnSc7	hlína
–	–	k?	–
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
samici	samice	k1gFnSc4	samice
kontakt	kontakt	k1gInSc1	kontakt
s	s	k7c7	s
mláďaty	mládě	k1gNnPc7	mládě
obtížný	obtížný	k2eAgInSc4d1	obtížný
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
velkému	velký	k2eAgInSc3d1	velký
počtu	počet	k1gInSc3	počet
mláďat	mládě	k1gNnPc2	mládě
má	můj	k3xOp1gFnSc1	můj
samice	samice	k1gFnSc1	samice
pouze	pouze	k6eAd1	pouze
čtyři	čtyři	k4xCgFnPc4	čtyři
bradavky	bradavka	k1gFnPc4	bradavka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
kapse	kapsa	k1gFnSc6	kapsa
nikdy	nikdy	k6eAd1	nikdy
nejsou	být	k5eNaImIp3nP	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtyři	čtyři	k4xCgNnPc1	čtyři
mláďata	mládě	k1gNnPc1	mládě
<g/>
;	;	kIx,	;
jak	jak	k8xC	jak
samice	samice	k1gFnSc1	samice
stárne	stárnout	k5eAaImIp3nS	stárnout
<g/>
,	,	kIx,	,
mláďat	mládě	k1gNnPc2	mládě
ve	v	k7c6	v
vrhu	vrh	k1gInSc6	vrh
ubývá	ubývat	k5eAaImIp3nS	ubývat
<g/>
.	.	kIx.	.
</s>
<s>
Přežívá	přežívat	k5eAaImIp3nS	přežívat
více	hodně	k6eAd2	hodně
samic	samice	k1gFnPc2	samice
než	než	k8xS	než
samců	samec	k1gMnPc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapse	kapsa	k1gFnSc6	kapsa
je	být	k5eAaImIp3nS	být
vývin	vývin	k1gInSc1	vývin
mláďat	mládě	k1gNnPc2	mládě
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
dni	den	k1gInPc1	den
jsou	být	k5eAaImIp3nP	být
viditelná	viditelný	k2eAgNnPc4d1	viditelné
ouška	ouško	k1gNnPc4	ouško
<g/>
,	,	kIx,	,
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
oční	oční	k2eAgNnPc1d1	oční
víčka	víčko	k1gNnPc1	víčko
<g/>
,	,	kIx,	,
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
vousky	vouska	k1gFnSc2	vouska
a	a	k8xC	a
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
rtíky	rtík	k1gInPc1	rtík
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
narůstá	narůstat	k5eAaImIp3nS	narůstat
od	od	k7c2	od
49	[number]	k4	49
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
a	a	k8xC	a
nejpozději	pozdě	k6eAd3	pozdě
90	[number]	k4	90
<g/>
.	.	kIx.	.
den	den	k1gInSc4	den
jsou	být	k5eAaImIp3nP	být
plně	plně	k6eAd1	plně
osrstěná	osrstěný	k2eAgNnPc1d1	osrstěné
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
nárůstu	nárůst	k1gInSc6	nárůst
srsti	srst	k1gFnSc2	srst
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
87	[number]	k4	87
<g/>
.	.	kIx.	.
a	a	k8xC	a
93	[number]	k4	93
<g/>
.	.	kIx.	.
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
otvírají	otvírat	k5eAaImIp3nP	otvírat
oči	oko	k1gNnPc1	oko
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
100	[number]	k4	100
dnech	den	k1gInPc6	den
dovedou	dovést	k5eAaPmIp3nP	dovést
mláďata	mládě	k1gNnPc4	mládě
pustit	pustit	k5eAaPmF	pustit
bradavku	bradavka	k1gFnSc4	bradavka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
105	[number]	k4	105
<g/>
.	.	kIx.	.
dni	den	k1gInPc1	den
opouštějí	opouštět	k5eAaImIp3nP	opouštět
kapsu	kapsa	k1gFnSc4	kapsa
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nS	vážit
zhruba	zhruba	k6eAd1	zhruba
půl	půl	k1xP	půl
kilogramu	kilogram	k1gInSc2	kilogram
a	a	k8xC	a
vypadají	vypadat	k5eAaPmIp3nP	vypadat
jako	jako	k9	jako
zmenšené	zmenšený	k2eAgFnPc1d1	zmenšená
kopie	kopie	k1gFnPc1	kopie
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
klokaních	klokaní	k2eAgNnPc2d1	klokaní
mláďat	mládě	k1gNnPc2	mládě
se	se	k3xPyFc4	se
do	do	k7c2	do
kapsy	kapsa	k1gFnSc2	kapsa
nevrací	vracet	k5eNaImIp3nS	vracet
<g/>
;	;	kIx,	;
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
v	v	k7c6	v
noře	nora	k1gFnSc6	nora
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
opouštějí	opouštět	k5eAaImIp3nP	opouštět
nejprve	nejprve	k6eAd1	nejprve
mezi	mezi	k7c7	mezi
říjnem	říjen	k1gInSc7	říjen
a	a	k8xC	a
prosincem	prosinec	k1gInSc7	prosinec
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
stanou	stanout	k5eAaPmIp3nP	stanout
plně	plně	k6eAd1	plně
samostatnými	samostatný	k2eAgNnPc7d1	samostatné
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
netráví	trávit	k5eNaImIp3nP	trávit
výchovou	výchova	k1gFnSc7	výchova
potomstva	potomstvo	k1gNnSc2	potomstvo
pouze	pouze	k6eAd1	pouze
šest	šest	k4xCc4	šest
týdnů	týden	k1gInPc2	týden
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ďábel	ďábel	k1gMnSc1	ďábel
medvědovitý	medvědovitý	k2eAgMnSc1d1	medvědovitý
žije	žít	k5eAaImIp3nS	žít
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Tasmánii	Tasmánie	k1gFnSc6	Tasmánie
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
habitatech	habite	k1gNnPc6	habite
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
okrajů	okraj	k1gInPc2	okraj
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Nejraději	rád	k6eAd3	rád
mají	mít	k5eAaImIp3nP	mít
suché	suchý	k2eAgInPc1d1	suchý
sklerofylní	sklerofylní	k2eAgInPc1d1	sklerofylní
lesy	les	k1gInPc1	les
a	a	k8xC	a
pobřežní	pobřežní	k2eAgInPc1d1	pobřežní
lesíky	lesík	k1gInPc1	lesík
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
soumračnými	soumračný	k2eAgMnPc7d1	soumračný
a	a	k8xC	a
nočními	noční	k2eAgMnPc7d1	noční
lovci	lovec	k1gMnPc7	lovec
<g/>
,	,	kIx,	,
dny	den	k1gInPc1	den
přečkávají	přečkávat	k5eAaImIp3nP	přečkávat
v	v	k7c6	v
hustých	hustý	k2eAgInPc6d1	hustý
keřích	keř	k1gInPc6	keř
nebo	nebo	k8xC	nebo
v	v	k7c6	v
noře	nora	k1gFnSc6	nora
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
jedinci	jedinec	k1gMnPc1	jedinec
dokáží	dokázat	k5eAaPmIp3nP	dokázat
šplhat	šplhat	k5eAaImF	šplhat
na	na	k7c4	na
stromy	strom	k1gInPc4	strom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jak	jak	k6eAd1	jak
stárnou	stárnout	k5eAaImIp3nP	stárnout
<g/>
,	,	kIx,	,
narůstající	narůstající	k2eAgFnSc1d1	narůstající
váha	váha	k1gFnSc1	váha
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc1	ten
znesnadňuje	znesnadňovat	k5eAaImIp3nS	znesnadňovat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
samotářské	samotářský	k2eAgNnSc4d1	samotářské
zvíře	zvíře	k1gNnSc4	zvíře
netvořící	tvořící	k2eNgFnSc2d1	netvořící
smečky	smečka	k1gFnSc2	smečka
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
teritorium	teritorium	k1gNnSc1	teritorium
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
8	[number]	k4	8
–	–	k?	–
20	[number]	k4	20
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
,	,	kIx,	,
a	a	k8xC	a
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
zvířat	zvíře	k1gNnPc2	zvíře
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dosti	dosti	k6eAd1	dosti
překrývat	překrývat	k5eAaImF	překrývat
<g/>
.	.	kIx.	.
</s>
<s>
Rádi	rád	k2eAgMnPc1d1	rád
plavou	plavat	k5eAaImIp3nP	plavat
<g/>
;	;	kIx,	;
plaváním	plavání	k1gNnSc7	plavání
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
unikají	unikat	k5eAaImIp3nP	unikat
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
dokáží	dokázat	k5eAaPmIp3nP	dokázat
ulovit	ulovit	k5eAaPmF	ulovit
i	i	k9	i
menší	malý	k2eAgInPc4d2	menší
druhy	druh	k1gInPc4	druh
klokanů	klokan	k1gMnPc2	klokan
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
využívají	využívat	k5eAaImIp3nP	využívat
příležitosti	příležitost	k1gFnPc4	příležitost
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
ulovená	ulovený	k2eAgNnPc1d1	ulovené
zvířata	zvíře	k1gNnPc1	zvíře
požírají	požírat	k5eAaImIp3nP	požírat
mršiny	mršin	k2eAgFnPc1d1	mršina
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc7	jeho
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
kořistí	kořist	k1gFnSc7	kořist
je	být	k5eAaImIp3nS	být
vombat	vombat	k1gMnSc1	vombat
<g/>
,	,	kIx,	,
požírá	požírat	k5eAaImIp3nS	požírat
všechny	všechen	k3xTgMnPc4	všechen
malé	malý	k2eAgMnPc4d1	malý
domorodé	domorodý	k2eAgMnPc4d1	domorodý
savce	savec	k1gMnPc4	savec
<g/>
,	,	kIx,	,
domácí	domácí	k2eAgNnPc1d1	domácí
zvířata	zvíře	k1gNnPc1	zvíře
(	(	kIx(	(
<g/>
i	i	k9	i
ovce	ovce	k1gFnSc1	ovce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
žáby	žába	k1gFnPc4	žába
a	a	k8xC	a
ještěry	ještěr	k1gMnPc4	ještěr
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
strava	strava	k1gFnSc1	strava
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
liší	lišit	k5eAaImIp3nS	lišit
podle	podle	k7c2	podle
obývaného	obývaný	k2eAgNnSc2d1	obývané
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Průměrně	průměrně	k6eAd1	průměrně
denně	denně	k6eAd1	denně
sežerou	sežrat	k5eAaPmIp3nP	sežrat
asi	asi	k9	asi
15	[number]	k4	15
<g/>
%	%	kIx~	%
toho	ten	k3xDgNnSc2	ten
kolik	kolik	k9	kolik
sami	sám	k3xTgMnPc1	sám
váží	vážit	k5eAaImIp3nP	vážit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
naskytne	naskytnout	k5eAaPmIp3nS	naskytnout
příležitost	příležitost	k1gFnSc1	příležitost
<g/>
,	,	kIx,	,
dokáží	dokázat	k5eAaPmIp3nP	dokázat
sežrat	sežrat	k5eAaPmF	sežrat
až	až	k9	až
40	[number]	k4	40
<g/>
%	%	kIx~	%
toho	ten	k3xDgNnSc2	ten
co	co	k9	co
sami	sám	k3xTgMnPc1	sám
váží	vážit	k5eAaImIp3nP	vážit
během	během	k7c2	během
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Mršiny	mršina	k1gFnPc1	mršina
likvidují	likvidovat	k5eAaBmIp3nP	likvidovat
kompletně	kompletně	k6eAd1	kompletně
<g/>
;	;	kIx,	;
požírají	požírat	k5eAaImIp3nP	požírat
i	i	k9	i
kosti	kost	k1gFnPc1	kost
a	a	k8xC	a
srst	srst	k1gFnSc1	srst
–	–	k?	–
tím	ten	k3xDgNnSc7	ten
si	se	k3xPyFc3	se
získávají	získávat	k5eAaImIp3nP	získávat
náklonnost	náklonnost	k1gFnSc4	náklonnost
farmářů	farmář	k1gMnPc2	farmář
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	nízce	k6eAd2	nízce
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
tlamách	tlama	k1gFnPc6	tlama
mizí	mizet	k5eAaImIp3nP	mizet
mršiny	mršina	k1gFnPc1	mršina
<g/>
,	,	kIx,	,
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
rozšíření	rozšíření	k1gNnSc1	rozšíření
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
jinak	jinak	k6eAd1	jinak
mohl	moct	k5eAaImAgInS	moct
ohrožovat	ohrožovat	k5eAaImF	ohrožovat
jejich	jejich	k3xOp3gInPc4	jejich
chovy	chov	k1gInPc4	chov
<g/>
.	.	kIx.	.
</s>
<s>
Požírání	požírání	k1gNnSc1	požírání
kořistí	kořist	k1gFnPc2	kořist
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
zvíře	zvíře	k1gNnSc4	zvíře
společenskou	společenský	k2eAgFnSc7d1	společenská
událostí	událost	k1gFnSc7	událost
–	–	k?	–
chraptivý	chraptivý	k2eAgInSc4d1	chraptivý
hřmot	hřmot	k1gInSc4	hřmot
a	a	k8xC	a
poštěkávání	poštěkávání	k1gNnSc4	poštěkávání
až	až	k9	až
dvanácti	dvanáct	k4xCc2	dvanáct
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
sejít	sejít	k5eAaPmF	sejít
u	u	k7c2	u
jedné	jeden	k4xCgFnSc2	jeden
mršiny	mršina	k1gFnSc2	mršina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
často	často	k6eAd1	často
nese	nést	k5eAaImIp3nS	nést
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnPc1	studie
identifikovaly	identifikovat	k5eAaBmAgFnP	identifikovat
dvacet	dvacet	k4xCc4	dvacet
různých	různý	k2eAgFnPc2d1	různá
pozic	pozice	k1gFnPc2	pozice
zaujímaných	zaujímaný	k2eAgFnPc2d1	zaujímaná
při	při	k7c6	při
přijímání	přijímání	k1gNnSc6	přijímání
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
jejich	jejich	k3xOp3gFnPc4	jejich
slavné	slavný	k2eAgFnPc4d1	slavná
<g/>
,	,	kIx,	,
hrůzostrašně	hrůzostrašně	k6eAd1	hrůzostrašně
vypadající	vypadající	k2eAgNnSc1d1	vypadající
zívání	zívání	k1gNnSc3	zívání
<g/>
,	,	kIx,	,
a	a	k8xC	a
jedenáct	jedenáct	k4xCc1	jedenáct
odlišných	odlišný	k2eAgInPc2d1	odlišný
zvuků	zvuk	k1gInPc2	zvuk
používaných	používaný	k2eAgInPc2d1	používaný
ke	k	k7c3	k
komunikaci	komunikace	k1gFnSc3	komunikace
během	během	k7c2	během
krmení	krmení	k1gNnSc2	krmení
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Dominance	dominance	k1gFnSc1	dominance
je	být	k5eAaImIp3nS	být
vyjadřována	vyjadřovat	k5eAaImNgFnS	vyjadřovat
právě	právě	k9	právě
těmito	tento	k3xDgInPc7	tento
postoji	postoj	k1gInPc7	postoj
a	a	k8xC	a
zvuky	zvuk	k1gInPc7	zvuk
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
boj	boj	k1gInSc1	boj
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
strhne	strhnout	k5eAaPmIp3nS	strhnout
<g/>
.	.	kIx.	.
</s>
<s>
Nejagresivnější	agresivní	k2eAgMnPc1d3	nejagresivnější
jsou	být	k5eAaImIp3nP	být
dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc1	jejich
těla	tělo	k1gNnPc1	tělo
často	často	k6eAd1	často
nesou	nést	k5eAaImIp3nP	nést
jizvy	jizva	k1gFnPc4	jizva
po	po	k7c6	po
bojích	boj	k1gInPc6	boj
o	o	k7c4	o
jídlo	jídlo	k1gNnSc4	jídlo
nebo	nebo	k8xC	nebo
samici	samice	k1gFnSc4	samice
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
člověka	člověk	k1gMnSc2	člověk
na	na	k7c6	na
australské	australský	k2eAgFnSc6d1	australská
pevnině	pevnina	k1gFnSc6	pevnina
vyhynuli	vyhynout	k5eAaPmAgMnP	vyhynout
velcí	velký	k2eAgMnPc1d1	velký
masožraví	masožravý	k2eAgMnPc1d1	masožravý
vačnatci	vačnatec	k1gMnPc1	vačnatec
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Tasmánie	Tasmánie	k1gFnSc1	Tasmánie
jejich	jejich	k3xOp3gNnSc7	jejich
posledním	poslední	k2eAgNnSc7d1	poslední
útočištěm	útočiště	k1gNnSc7	útočiště
<g/>
.	.	kIx.	.
</s>
<s>
Přežily	přežít	k5eAaPmAgInP	přežít
ale	ale	k9	ale
pouze	pouze	k6eAd1	pouze
nejmenší	malý	k2eAgInPc1d3	nejmenší
a	a	k8xC	a
nejpřizpůsobivější	přizpůsobivý	k2eAgInPc1d3	nejpřizpůsobivější
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
před	před	k7c7	před
600	[number]	k4	600
lety	let	k1gInPc7	let
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
400	[number]	k4	400
let	léto	k1gNnPc2	léto
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Evropanů	Evropan	k1gMnPc2	Evropan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ďábel	ďábel	k1gMnSc1	ďábel
medvědovitý	medvědovitý	k2eAgMnSc1d1	medvědovitý
fosíliemi	fosílie	k1gFnPc7	fosílie
doložen	doložen	k2eAgInSc1d1	doložen
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
státu	stát	k1gInSc2	stát
Victoria	Victorium	k1gNnSc2	Victorium
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
vyhynutí	vyhynutí	k1gNnSc1	vyhynutí
je	být	k5eAaImIp3nS	být
přičítáno	přičítán	k2eAgNnSc1d1	přičítáno
psu	pes	k1gMnSc6	pes
dingo	dingo	k1gMnSc1	dingo
a	a	k8xC	a
lovu	lov	k1gInSc2	lov
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gFnPc4	jejich
lebky	lebka	k1gFnPc4	lebka
bývají	bývat	k5eAaImIp3nP	bývat
nalézány	nalézat	k5eAaImNgFnP	nalézat
v	v	k7c6	v
kuchyňských	kuchyňský	k2eAgInPc6d1	kuchyňský
odpadcích	odpadek	k1gInPc6	odpadek
domorodců	domorodec	k1gMnPc2	domorodec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
jeden	jeden	k4xCgMnSc1	jeden
jedinec	jedinec	k1gMnSc1	jedinec
usmrcen	usmrtit	k5eAaPmNgMnS	usmrtit
asi	asi	k9	asi
90	[number]	k4	90
km	km	kA	km
od	od	k7c2	od
Melbourne	Melbourne	k1gNnSc2	Melbourne
<g/>
,	,	kIx,	,
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
se	se	k3xPyFc4	se
však	však	k9	však
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
uprchlíka	uprchlík	k1gMnSc4	uprchlík
z	z	k7c2	z
domácího	domácí	k2eAgInSc2d1	domácí
chovu	chov	k1gInSc2	chov
<g/>
;	;	kIx,	;
o	o	k7c6	o
jejich	jejich	k3xOp3gNnSc6	jejich
možném	možný	k2eAgNnSc6d1	možné
přežití	přežití	k1gNnSc6	přežití
na	na	k7c6	na
kontinentu	kontinent	k1gInSc6	kontinent
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
stále	stále	k6eAd1	stále
vedou	vést	k5eAaImIp3nP	vést
debaty	debata	k1gFnPc4	debata
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Tasmánii	Tasmánie	k1gFnSc6	Tasmánie
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
dingo	dingo	k1gMnSc1	dingo
nikdy	nikdy	k6eAd1	nikdy
nedostal	dostat	k5eNaPmAgMnS	dostat
<g/>
,	,	kIx,	,
draví	dravý	k2eAgMnPc1d1	dravý
vačnatci	vačnatec	k1gMnPc1	vačnatec
prosperovali	prosperovat	k5eAaImAgMnP	prosperovat
až	až	k9	až
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
Evropanů	Evropan	k1gMnPc2	Evropan
<g/>
.	.	kIx.	.
</s>
<s>
Osud	osud	k1gInSc1	osud
postihnuvší	postihnuvší	k2eAgFnSc1d1	postihnuvší
vakovlka	vakovlka	k1gFnSc1	vakovlka
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
znám	znám	k2eAgMnSc1d1	znám
<g/>
;	;	kIx,	;
ďábel	ďábel	k1gMnSc1	ďábel
medvědovitý	medvědovitý	k2eAgMnSc1d1	medvědovitý
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
také	také	k9	také
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
osadníci	osadník	k1gMnPc1	osadník
jedli	jíst	k5eAaImAgMnP	jíst
jejich	jejich	k3xOp3gNnSc4	jejich
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
chuť	chuť	k1gFnSc4	chuť
popisovali	popisovat	k5eAaImAgMnP	popisovat
jako	jako	k8xC	jako
podobnou	podobný	k2eAgFnSc4d1	podobná
telecímu	telecí	k2eAgInSc3d1	telecí
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
navíc	navíc	k6eAd1	navíc
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
jejich	jejich	k3xOp3gNnPc4	jejich
domácí	domácí	k2eAgNnPc4d1	domácí
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
se	se	k3xPyFc4	se
za	za	k7c4	za
každého	každý	k3xTgMnSc4	každý
jedince	jedinec	k1gMnSc4	jedinec
zabitého	zabitý	k1gMnSc4	zabitý
na	na	k7c6	na
obdělávané	obdělávaný	k2eAgFnSc6d1	obdělávaná
půdě	půda	k1gFnSc6	půda
vyplácela	vyplácet	k5eAaImAgFnS	vyplácet
prémie	prémie	k1gFnPc4	prémie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
druh	druh	k1gInSc1	druh
kvůli	kvůli	k7c3	kvůli
pastem	past	k1gFnPc3	past
a	a	k8xC	a
jedům	jed	k1gInPc3	jed
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
vyhubení	vyhubení	k1gNnSc2	vyhubení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úhynu	úhyn	k1gInSc6	úhyn
posledního	poslední	k2eAgNnSc2d1	poslední
vakovlka	vakovlko	k1gNnSc2	vakovlko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
rozpoznáno	rozpoznán	k2eAgNnSc1d1	rozpoznáno
a	a	k8xC	a
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
druh	druh	k1gInSc1	druh
stal	stát	k5eAaPmAgInS	stát
zákonem	zákon	k1gInSc7	zákon
chráněným	chráněný	k2eAgInSc7d1	chráněný
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jeho	jeho	k3xOp3gFnSc4	jeho
populaci	populace	k1gFnSc4	populace
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
vzpamatovat	vzpamatovat	k5eAaPmF	vzpamatovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zaznamenané	zaznamenaný	k2eAgFnSc6d1	zaznamenaná
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
dvě	dva	k4xCgNnPc4	dva
obrovská	obrovský	k2eAgNnPc4d1	obrovské
snížení	snížení	k1gNnPc4	snížení
populace	populace	k1gFnSc2	populace
<g/>
:	:	kIx,	:
1909	[number]	k4	1909
a	a	k8xC	a
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
zřejmě	zřejmě	k6eAd1	zřejmě
zaviněná	zaviněný	k2eAgFnSc1d1	zaviněná
nějakou	nějaký	k3yIgFnSc7	nějaký
epidemickou	epidemický	k2eAgFnSc7d1	epidemická
chorobou	choroba	k1gFnSc7	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
populace	populace	k1gFnSc1	populace
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
deset	deset	k4xCc4	deset
až	až	k6eAd1	až
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInPc2	tisíc
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
tasmánského	tasmánský	k2eAgNnSc2d1	Tasmánské
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
dospělých	dospělý	k2eAgInPc2d1	dospělý
kusů	kus	k1gInPc2	kus
dvacet	dvacet	k4xCc4	dvacet
až	až	k8xS	až
padesát	padesát	k4xCc4	padesát
tisíc	tisíc	k4xCgInSc4	tisíc
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Hamish	Hamish	k1gInSc1	Hamish
McCallum	McCallum	k1gInSc1	McCallum
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
vědců	vědec	k1gMnPc2	vědec
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
výzkum	výzkum	k1gInSc4	výzkum
současné	současný	k2eAgFnSc2d1	současná
epidemie	epidemie	k1gFnSc2	epidemie
rakoviny	rakovina	k1gFnSc2	rakovina
tváře	tvář	k1gFnSc2	tvář
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
stav	stav	k1gInSc1	stav
mezi	mezi	k7c7	mezi
dvaceti	dvacet	k4xCc7	dvacet
až	až	k8xS	až
pětasedmdesáti	pětasedmdesát	k4xCc7	pětasedmdesát
tisíci	tisíc	k4xCgInPc7	tisíc
kusy	kus	k1gInPc7	kus
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
zaznamenána	zaznamenat	k5eAaPmNgNnP	zaznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
rakovina	rakovina	k1gFnSc1	rakovina
tváře	tvář	k1gFnSc2	tvář
ďábla	ďábel	k1gMnSc2	ďábel
medvědovitého	medvědovitý	k2eAgMnSc2d1	medvědovitý
(	(	kIx(	(
<g/>
devil	devil	k1gInSc1	devil
facial	facial	k1gMnSc1	facial
tumour	tumour	k1gMnSc1	tumour
disease	disease	k6eAd1	disease
<g/>
,	,	kIx,	,
DFTD	DFTD	kA	DFTD
<g/>
)	)	kIx)	)
pustoší	pustošit	k5eAaImIp3nS	pustošit
jeho	jeho	k3xOp3gFnSc4	jeho
současnou	současný	k2eAgFnSc4d1	současná
populaci	populace	k1gFnSc4	populace
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dvaceti-	dvaceti-	k?	dvaceti-
až	až	k8xS	až
padesátiprocentnímu	padesátiprocentní	k2eAgNnSc3d1	padesátiprocentní
snížení	snížení	k1gNnSc3	snížení
počtu	počet	k1gInSc2	počet
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
zasaženo	zasažen	k2eAgNnSc1d1	zasaženo
je	být	k5eAaImIp3nS	být
až	až	k9	až
65	[number]	k4	65
%	%	kIx~	%
Tasmánie	Tasmánie	k1gFnSc2	Tasmánie
<g/>
.	.	kIx.	.
</s>
<s>
Smrtnost	smrtnost	k1gFnSc1	smrtnost
je	být	k5eAaImIp3nS	být
100	[number]	k4	100
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
smrt	smrt	k1gFnSc1	smrt
nastává	nastávat	k5eAaImIp3nS	nastávat
po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
vyhladověním	vyhladovění	k1gNnPc3	vyhladovění
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
populární	populární	k2eAgInSc1d1	populární
druh	druh	k1gInSc1	druh
veden	vést	k5eAaImNgInS	vést
jako	jako	k8xC	jako
zranitelný	zranitelný	k2eAgMnSc1d1	zranitelný
(	(	kIx(	(
<g/>
tasmánským	tasmánský	k2eAgInSc7d1	tasmánský
výnosem	výnos	k1gInSc7	výnos
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
australským	australský	k2eAgFnPc3d1	australská
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
IUCN	IUCN	kA	IUCN
původně	původně	k6eAd1	původně
vedla	vést	k5eAaImAgFnS	vést
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
jako	jako	k8xC	jako
málo	málo	k6eAd1	málo
dotčený	dotčený	k2eAgMnSc1d1	dotčený
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
veden	vést	k5eAaImNgInS	vést
jako	jako	k8xS	jako
ohrožený	ohrožený	k2eAgInSc1d1	ohrožený
(	(	kIx(	(
<g/>
od	od	k7c2	od
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Divoká	divoký	k2eAgFnSc1d1	divoká
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
monitorována	monitorovat	k5eAaImNgFnS	monitorovat
kvůli	kvůli	k7c3	kvůli
mapování	mapování	k1gNnSc3	mapování
výskytu	výskyt	k1gInSc2	výskyt
této	tento	k3xDgFnSc2	tento
choroby	choroba	k1gFnSc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Monitorování	monitorování	k1gNnSc1	monitorování
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
odchyt	odchyt	k1gInSc4	odchyt
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
opakované	opakovaný	k2eAgFnSc2d1	opakovaná
návštěvy	návštěva	k1gFnSc2	návštěva
lokalit	lokalita	k1gFnPc2	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
zatím	zatím	k6eAd1	zatím
probíhá	probíhat	k5eAaImIp3nS	probíhat
příliš	příliš	k6eAd1	příliš
krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
něco	něco	k3yInSc1	něco
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
že	že	k8xS	že
krátkodobý	krátkodobý	k2eAgInSc4d1	krátkodobý
dopad	dopad	k1gInSc4	dopad
je	být	k5eAaImIp3nS	být
hrozný	hrozný	k2eAgInSc1d1	hrozný
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
populace	populace	k1gFnPc1	populace
dokáží	dokázat	k5eAaPmIp3nP	dokázat
vzpamatovat	vzpamatovat	k5eAaPmF	vzpamatovat
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
z	z	k7c2	z
lokalit	lokalita	k1gFnPc2	lokalita
přemisťují	přemisťovat	k5eAaImIp3nP	přemisťovat
již	již	k6eAd1	již
nemocní	mocný	k2eNgMnPc1d1	nemocný
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
;	;	kIx,	;
terénní	terénní	k2eAgMnPc1d1	terénní
pracovníci	pracovník	k1gMnPc1	pracovník
doufají	doufat	k5eAaImIp3nP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tímto	tento	k3xDgNnSc7	tento
opatřením	opatření	k1gNnSc7	opatření
pomohou	pomoct	k5eAaPmIp3nP	pomoct
ostatním	ostatní	k2eAgMnPc3d1	ostatní
jedincům	jedinec	k1gMnPc3	jedinec
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
let	let	k1gInSc4	let
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
moci	moct	k5eAaImF	moct
rozmnožit	rozmnožit	k5eAaPmF	rozmnožit
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
přenos	přenos	k1gInSc1	přenos
rakovinných	rakovinný	k2eAgFnPc2d1	rakovinná
buněk	buňka	k1gFnPc2	buňka
kontaktem	kontakt	k1gInSc7	kontakt
v	v	k7c6	v
době	doba	k1gFnSc6	doba
páření	páření	k1gNnSc2	páření
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
samci	samec	k1gMnPc1	samec
bojují	bojovat	k5eAaImIp3nP	bojovat
o	o	k7c4	o
samice	samice	k1gFnPc4	samice
<g/>
.	.	kIx.	.
</s>
<s>
Rakovinné	rakovinný	k2eAgFnPc1d1	rakovinná
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
uchycují	uchycovat	k5eAaImIp3nP	uchycovat
v	v	k7c6	v
ranách	rána	k1gFnPc6	rána
a	a	k8xC	a
začnou	začít	k5eAaPmIp3nP	začít
se	se	k3xPyFc4	se
množit	množit	k5eAaImF	množit
<g/>
.	.	kIx.	.
</s>
<s>
Genetickou	genetický	k2eAgFnSc7d1	genetická
analýzou	analýza	k1gFnSc7	analýza
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
zmutovaný	zmutovaný	k2eAgInSc4d1	zmutovaný
klon	klon	k1gInSc4	klon
ďáblových	ďáblův	k2eAgFnPc2d1	Ďáblova
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
nedávno	nedávno	k6eAd1	nedávno
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
až	až	k9	až
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
desetiletí	desetiletí	k1gNnSc6	desetiletí
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obdobná	obdobný	k2eAgFnSc1d1	obdobná
přenosná	přenosný	k2eAgFnSc1d1	přenosná
rakovina	rakovina	k1gFnSc1	rakovina
CTVT	CTVT	kA	CTVT
je	být	k5eAaImIp3nS	být
také	také	k9	také
známa	znám	k2eAgFnSc1d1	známa
z	z	k7c2	z
pohlavních	pohlavní	k2eAgInPc2d1	pohlavní
orgánů	orgán	k1gInPc2	orgán
psa	pes	k1gMnSc2	pes
domácího	domácí	k1gMnSc2	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatím	zatím	k6eAd1	zatím
neexistuje	existovat	k5eNaImIp3nS	existovat
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tuto	tento	k3xDgFnSc4	tento
jejich	jejich	k3xOp3gFnSc1	jejich
nemoc	nemoc	k1gFnSc1	nemoc
léčit	léčit	k5eAaImF	léčit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
opatření	opatření	k1gNnPc1	opatření
omezují	omezovat	k5eAaImIp3nP	omezovat
na	na	k7c4	na
přemisťování	přemisťování	k1gNnSc4	přemisťování
nakažených	nakažený	k2eAgMnPc2d1	nakažený
jedinců	jedinec	k1gMnPc2	jedinec
a	a	k8xC	a
karanténu	karanténa	k1gFnSc4	karanténa
zdravých	zdravá	k1gFnPc2	zdravá
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
že	že	k8xS	že
by	by	kYmCp3nP	by
populace	populace	k1gFnPc1	populace
v	v	k7c6	v
divoké	divoký	k2eAgFnSc6d1	divoká
přírodě	příroda	k1gFnSc6	příroda
zcela	zcela	k6eAd1	zcela
vyhynula	vyhynout	k5eAaPmAgFnS	vyhynout
<g/>
.	.	kIx.	.
</s>
<s>
Extrémně	extrémně	k6eAd1	extrémně
malá	malý	k2eAgFnSc1d1	malá
genová	genový	k2eAgFnSc1d1	genová
diverzita	diverzita	k1gFnSc1	diverzita
a	a	k8xC	a
chromozomální	chromozomální	k2eAgFnPc1d1	chromozomální
mutace	mutace	k1gFnPc1	mutace
jedinečné	jedinečný	k2eAgFnPc1d1	jedinečná
mezi	mezi	k7c7	mezi
savci	savec	k1gMnPc7	savec
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ďábel	ďábel	k1gMnSc1	ďábel
medvědovitý	medvědovitý	k2eAgMnSc1d1	medvědovitý
je	být	k5eAaImIp3nS	být
náchylný	náchylný	k2eAgInSc1d1	náchylný
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
infekční	infekční	k2eAgFnSc3d1	infekční
rakovině	rakovina	k1gFnSc3	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
"	"	kIx"	"
<g/>
pojistné	pojistný	k2eAgFnSc2d1	pojistná
<g/>
"	"	kIx"	"
nenakažené	nakažený	k2eNgFnSc2d1	nenakažená
populace	populace	k1gFnSc2	populace
se	se	k3xPyFc4	se
budují	budovat	k5eAaImIp3nP	budovat
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Taroona	Taroona	k1gFnSc1	Taroona
(	(	kIx(	(
<g/>
město	město	k1gNnSc1	město
Hobart	Hobarta	k1gFnPc2	Hobarta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Maria	Mario	k1gMnSc2	Mario
u	u	k7c2	u
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Tasmánie	Tasmánie	k1gFnSc2	Tasmánie
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
záloze	záloha	k1gFnSc6	záloha
chov	chov	k1gInSc4	chov
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
na	na	k7c6	na
australské	australský	k2eAgFnSc6d1	australská
pevnině	pevnina	k1gFnSc6	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Mizení	mizení	k1gNnSc1	mizení
ďábla	ďábel	k1gMnSc2	ďábel
medvědovitého	medvědovitý	k2eAgMnSc2d1	medvědovitý
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Tasmánii	Tasmánie	k1gFnSc4	Tasmánie
ekologický	ekologický	k2eAgInSc4d1	ekologický
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gFnPc1	jeho
populace	populace	k1gFnPc1	populace
v	v	k7c6	v
lesích	les	k1gInPc6	les
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
nerozšířila	rozšířit	k5eNaPmAgFnS	rozšířit
liška	liška	k1gFnSc1	liška
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
ilegálně	ilegálně	k6eAd1	ilegálně
zavlečená	zavlečený	k2eAgFnSc1d1	zavlečená
do	do	k7c2	do
Tasmánie	Tasmánie	k1gFnSc2	Tasmánie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
Austrálii	Austrálie	k1gFnSc6	Austrálie
představuje	představovat	k5eAaImIp3nS	představovat
liška	liška	k1gFnSc1	liška
jako	jako	k8xC	jako
invazní	invazní	k2eAgInSc1d1	invazní
druh	druh	k1gInSc1	druh
velký	velký	k2eAgInSc4d1	velký
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
rozšíření	rozšíření	k1gNnSc1	rozšíření
po	po	k7c6	po
Tasmánii	Tasmánie	k1gFnSc6	Tasmánie
by	by	kYmCp3nS	by
ďábla	ďábel	k1gMnSc2	ďábel
medvědovitého	medvědovitý	k2eAgNnSc2d1	medvědovitý
rovněž	rovněž	k9	rovněž
ohrozilo	ohrozit	k5eAaPmAgNnS	ohrozit
<g/>
.	.	kIx.	.
</s>
<s>
Nedávný	dávný	k2eNgInSc1d1	nedávný
výzkum	výzkum	k1gInSc1	výzkum
Sydneyské	sydneyský	k2eAgFnSc2d1	Sydneyská
univerzity	univerzita	k1gFnSc2	univerzita
znovu	znovu	k6eAd1	znovu
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
malá	malý	k2eAgFnSc1d1	malá
je	být	k5eAaImIp3nS	být
genová	genový	k2eAgFnSc1d1	genová
rozmanitost	rozmanitost	k1gFnSc1	rozmanitost
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znovu	znovu	k6eAd1	znovu
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
otázku	otázka	k1gFnSc4	otázka
možné	možný	k2eAgFnSc2d1	možná
degenerace	degenerace	k1gFnSc2	degenerace
populace	populace	k1gFnSc2	populace
znovuobnovené	znovuobnovený	k2eAgFnSc6d1	znovuobnovená
z	z	k7c2	z
nízkého	nízký	k2eAgInSc2d1	nízký
počtu	počet	k1gInSc2	počet
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Ďábel	ďábel	k1gMnSc1	ďábel
medvědovitý	medvědovitý	k2eAgMnSc1d1	medvědovitý
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
zvíře	zvíře	k1gNnSc1	zvíře
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
nesmírně	smírně	k6eNd1	smírně
populární	populární	k2eAgMnSc1d1	populární
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
Tasmánské	tasmánský	k2eAgFnSc2d1	tasmánská
správy	správa	k1gFnSc2	správa
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
a	a	k8xC	a
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
klub	klub	k1gInSc1	klub
australského	australský	k2eAgInSc2d1	australský
fotbalu	fotbal	k1gInSc2	fotbal
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
zvířeti	zvíře	k1gNnSc6	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
zvířat	zvíře	k1gNnPc2	zvíře
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
na	na	k7c6	na
jubilejní	jubilejní	k2eAgFnSc6d1	jubilejní
sadě	sada	k1gFnSc6	sada
dvousetdolarových	dvousetdolarový	k2eAgFnPc2d1	dvousetdolarový
mincí	mince	k1gFnPc2	mince
ražených	ražený	k2eAgFnPc2d1	ražená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zvíře	zvíře	k1gNnSc1	zvíře
milované	milovaný	k2eAgNnSc1d1	milované
turisty	turist	k1gMnPc7	turist
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
domácími	domácí	k1gMnPc7	domácí
<g/>
,	,	kIx,	,
tak	tak	k9	tak
těmi	ten	k3xDgMnPc7	ten
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
jedinečné	jedinečný	k2eAgFnSc3d1	jedinečná
povaze	povaha	k1gFnSc3	povaha
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
dokumentech	dokument	k1gInPc6	dokument
a	a	k8xC	a
vědeckopopulárních	vědeckopopulární	k2eAgFnPc6d1	vědeckopopulární
dětských	dětský	k2eAgFnPc6d1	dětská
knihách	kniha	k1gFnPc6	kniha
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
"	"	kIx"	"
<g/>
Taz	Taz	k1gMnSc1	Taz
<g/>
"	"	kIx"	"
z	z	k7c2	z
kreslených	kreslený	k2eAgInPc2d1	kreslený
seriálů	seriál	k1gInPc2	seriál
"	"	kIx"	"
<g/>
Looney	Looney	k1gInPc4	Looney
Tunes	Tunes	k1gInSc1	Tunes
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jeho	jeho	k3xOp3gNnSc1	jeho
chování	chování	k1gNnSc1	chování
je	být	k5eAaImIp3nS	být
skutečnému	skutečný	k2eAgNnSc3d1	skutečné
zvířeti	zvíře	k1gNnSc3	zvíře
podobné	podobný	k2eAgFnPc1d1	podobná
převážně	převážně	k6eAd1	převážně
jen	jen	k9	jen
hlučností	hlučnost	k1gFnSc7	hlučnost
a	a	k8xC	a
apetitem	apetit	k1gInSc7	apetit
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnPc1d1	původní
pověsti	pověst	k1gFnPc1	pověst
o	o	k7c6	o
zuřivosti	zuřivost	k1gFnSc6	zuřivost
pocházejí	pocházet	k5eAaImIp3nP	pocházet
zřejmě	zřejmě	k6eAd1	zřejmě
od	od	k7c2	od
bílých	bílý	k2eAgMnPc2d1	bílý
osadníků	osadník	k1gMnPc2	osadník
a	a	k8xC	a
od	od	k7c2	od
objevitele	objevitel	k1gMnSc2	objevitel
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zdají	zdát	k5eAaPmIp3nP	zdát
se	se	k3xPyFc4	se
nezkrotně	zkrotně	k6eNd1	zkrotně
divocí	divoký	k2eAgMnPc1d1	divoký
a	a	k8xC	a
vydávají	vydávat	k5eAaImIp3nP	vydávat
hlasité	hlasitý	k2eAgNnSc4d1	hlasité
štěkavé	štěkavý	k2eAgNnSc4d1	štěkavé
vrčení	vrčení	k1gNnSc4	vrčení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
popis	popis	k1gInSc1	popis
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
rázem	rázem	k6eAd1	rázem
postaven	postavit	k5eAaPmNgInS	postavit
do	do	k7c2	do
správného	správný	k2eAgNnSc2d1	správné
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
přečteme	přečíst	k5eAaPmIp1nP	přečíst
dodatek	dodatek	k1gInSc4	dodatek
<g/>
,	,	kIx,	,
že	že	k8xS	že
vědec	vědec	k1gMnSc1	vědec
držel	držet	k5eAaImAgMnS	držet
pár	pár	k4xCyI	pár
zvířat	zvíře	k1gNnPc2	zvíře
"	"	kIx"	"
<g/>
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
měsíců	měsíc	k1gInPc2	měsíc
připoutaná	připoutaný	k2eAgFnSc1d1	připoutaná
k	k	k7c3	k
sobě	se	k3xPyFc3	se
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
kádi	káď	k1gFnSc6	káď
<g/>
"	"	kIx"	"
<g/>
!	!	kIx.	!
</s>
<s>
Zkušenosti	zkušenost	k1gFnPc1	zkušenost
však	však	k9	však
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
že	že	k8xS	že
jak	jak	k6eAd1	jak
mladí	mladý	k2eAgMnPc1d1	mladý
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
staří	starý	k2eAgMnPc1d1	starý
jedinci	jedinec	k1gMnPc1	jedinec
(	(	kIx(	(
<g/>
z	z	k7c2	z
odchytu	odchyt	k1gInSc2	odchyt
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
ochočí	ochočit	k5eAaPmIp3nS	ochočit
<g/>
.	.	kIx.	.
</s>
<s>
Ďábel	ďábel	k1gMnSc1	ďábel
medvědovitý	medvědovitý	k2eAgMnSc1d1	medvědovitý
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
vyvážen	vyvážen	k2eAgInSc1d1	vyvážen
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
australských	australský	k2eAgFnPc6d1	australská
zoo	zoo	k1gFnPc6	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
známý	známý	k2eAgMnSc1d1	známý
jedinec	jedinec	k1gMnSc1	jedinec
chovaný	chovaný	k2eAgInSc4d1	chovaný
mimo	mimo	k7c4	mimo
Austrálii	Austrálie	k1gFnSc4	Austrálie
uhynul	uhynout	k5eAaPmAgMnS	uhynout
ve	v	k7c6	v
Fort	Fort	k?	Fort
Wayne	Wayn	k1gInSc5	Wayn
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Children	Childrna	k1gFnPc2	Childrna
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Zoo	zoo	k1gNnSc7	zoo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nS	tvořit
pár	pár	k1gInSc1	pár
zaslaný	zaslaný	k2eAgInSc1d1	zaslaný
kodaňské	kodaňský	k2eAgFnSc3d1	Kodaňská
zoo	zoo	k1gFnSc3	zoo
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
narození	narození	k1gNnSc2	narození
prvního	první	k4xOgMnSc2	první
syna	syn	k1gMnSc2	syn
dánského	dánský	k2eAgMnSc2d1	dánský
korunního	korunní	k2eAgMnSc2d1	korunní
prince	princ	k1gMnSc2	princ
Frederika	Frederik	k1gMnSc2	Frederik
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
Mary	Mary	k1gFnSc2	Mary
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
Tasmánie	Tasmánie	k1gFnSc2	Tasmánie
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
spatřit	spatřit	k5eAaPmF	spatřit
tasmánské	tasmánský	k2eAgMnPc4d1	tasmánský
čerty	čert	k1gMnPc4	čert
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Auckland	Aucklanda	k1gFnPc2	Aucklanda
<g/>
.	.	kIx.	.
</s>
