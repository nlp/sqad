<s>
Ďábel	ďábel	k1gMnSc1	ďábel
medvědovitý	medvědovitý	k2eAgMnSc1d1	medvědovitý
(	(	kIx(	(
<g/>
Sarcophilus	Sarcophilus	k1gMnSc1	Sarcophilus
harrisii	harrisie	k1gFnSc4	harrisie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
též	též	k9	též
jako	jako	k9	jako
tasmánský	tasmánský	k2eAgMnSc1d1	tasmánský
čert	čert	k1gMnSc1	čert
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgMnSc1d1	jediný
žijící	žijící	k2eAgMnSc1d1	žijící
druh	druh	k1gMnSc1	druh
rodu	rod	k1gInSc2	rod
Sarcophilus	Sarcophilus	k1gMnSc1	Sarcophilus
a	a	k8xC	a
od	od	k7c2	od
vyhubení	vyhubení	k1gNnSc2	vyhubení
vakovlka	vakovlko	k1gNnSc2	vakovlko
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
i	i	k8xC	i
největší	veliký	k2eAgMnSc1d3	veliký
žijící	žijící	k2eAgMnSc1d1	žijící
dravý	dravý	k2eAgMnSc1d1	dravý
vačnatec	vačnatec	k1gMnSc1	vačnatec
<g/>
.	.	kIx.	.
</s>
