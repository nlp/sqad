<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
zaznamenané	zaznamenaný	k2eAgNnSc1d1	zaznamenané
použití	použití	k1gNnSc1	použití
termínu	termín	k1gInSc2	termín
makrobiotika	makrobiotika	k1gFnSc1	makrobiotika
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Hippokratově	Hippokratův	k2eAgInSc6d1	Hippokratův
spisu	spis	k1gInSc6	spis
O	o	k7c6	o
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
vodách	voda	k1gFnPc6	voda
a	a	k8xC	a
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
vlivu	vliv	k1gInSc6	vliv
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
výživy	výživa	k1gFnSc2	výživa
na	na	k7c4	na
lidské	lidský	k2eAgNnSc4d1	lidské
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
