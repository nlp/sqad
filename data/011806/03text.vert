<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Ko	Ko	k1gFnSc2	Ko
Čangu	Čang	k1gInSc2	Čang
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
bataille	bataill	k1gMnSc2	bataill
de	de	k?	de
Koh	Koh	k1gMnSc1	Koh
Chang	Chang	k1gMnSc1	Chang
<g/>
,	,	kIx,	,
thajsky	thajsky	k6eAd1	thajsky
ก	ก	k?	ก
<g/>
ี	ี	k?	ี
<g/>
่	่	k?	่
<g/>
เ	เ	k?	เ
<g/>
้	้	k?	้
<g/>
า	า	k?	า
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
námořní	námořní	k2eAgFnSc1d1	námořní
bitva	bitva	k1gFnSc1	bitva
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Thajskem	Thajsko	k1gNnSc7	Thajsko
<g/>
.	.	kIx.	.
</s>
<s>
Odehrála	odehrát	k5eAaPmAgFnS	odehrát
se	s	k7c7	s
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1941	[number]	k4	1941
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
francouzsko-thajské	francouzskohajský	k2eAgFnSc2d1	francouzsko-thajský
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
u	u	k7c2	u
thajského	thajský	k2eAgInSc2d1	thajský
ostrova	ostrov	k1gInSc2	ostrov
Ko	Ko	k1gMnSc1	Ko
Čang	Čang	k1gMnSc1	Čang
<g/>
,	,	kIx,	,
blízko	blízko	k7c2	blízko
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
Thajskem	Thajsko	k1gNnSc7	Thajsko
a	a	k8xC	a
Francouzskou	francouzský	k2eAgFnSc7d1	francouzská
Indočínou	Indočína	k1gFnSc7	Indočína
<g/>
.	.	kIx.	.
</s>
<s>
Skončila	skončit	k5eAaPmAgFnS	skončit
vítězstvím	vítězství	k1gNnSc7	vítězství
francouzských	francouzský	k2eAgFnPc2d1	francouzská
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
</p>
