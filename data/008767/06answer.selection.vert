<s>
Hanácké	hanácký	k2eAgNnSc1d1	Hanácké
nářečí	nářečí	k1gNnSc1	nářečí
spadá	spadat	k5eAaPmIp3nS	spadat
pod	pod	k7c4	pod
moravské	moravský	k2eAgInPc4d1	moravský
dialekty	dialekt	k1gInPc4	dialekt
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
zahrnováno	zahrnován	k2eAgNnSc1d1	zahrnováno
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
středomoravské	středomoravský	k2eAgFnSc2d1	Středomoravská
nářeční	nářeční	k2eAgFnSc2d1	nářeční
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
