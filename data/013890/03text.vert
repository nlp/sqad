<s>
Píseň	píseň	k1gFnSc1
vyšehradská	vyšehradský	k2eAgFnSc1d1
</s>
<s>
Píseň	píseň	k1gFnSc1
vyšehradská	vyšehradský	k2eAgFnSc1d1
Autor	autor	k1gMnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Hanka	Hanka	k1gFnSc1
Původní	původní	k2eAgFnSc1d1
název	název	k1gInSc4
</s>
<s>
Milostná	milostný	k2eAgFnSc1d1
píseň	píseň	k1gFnSc1
pod	pod	k7c7
Vyšehradem	Vyšehrad	k1gInSc7
Jazyk	jazyk	k1gInSc1
</s>
<s>
čeština	čeština	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Píseň	píseň	k1gFnSc1
vyšehradská	vyšehradský	k2eAgFnSc1d1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
také	také	k9
Píseň	píseň	k1gFnSc1
pod	pod	k7c7
Vyšehradem	Vyšehrad	k1gInSc7
nebo	nebo	k8xC
Milostná	milostný	k2eAgFnSc1d1
píseň	píseň	k1gFnSc1
pod	pod	k7c7
Vyšehradem	Vyšehrad	k1gInSc7
(	(	kIx(
<g/>
používaná	používaný	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
PV	PV	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgInSc1d1
rukopis	rukopis	k1gInSc1
údajně	údajně	k6eAd1
nalezený	nalezený	k2eAgInSc1d1
během	během	k7c2
národního	národní	k2eAgNnSc2d1
obrození	obrození	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
vedlejších	vedlejší	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
sporu	spor	k1gInSc2
o	o	k7c4
pravost	pravost	k1gFnSc4
rukopisů	rukopis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
do	do	k7c2
souboru	soubor	k1gInSc2
těch	ten	k3xDgFnPc2
památek	památka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
nalezl	nalézt	k5eAaBmAgMnS,k5eAaPmAgMnS
buď	buď	k8xC
sám	sám	k3xTgInSc4
Hanka	Hanka	k1gFnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
někdo	někdo	k3yInSc1
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
okruhu	okruh	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
text	text	k1gInSc1
Písně	píseň	k1gFnSc2
vyšehradské	vyšehradský	k2eAgFnSc2d1
je	být	k5eAaImIp3nS
také	také	k9
obvykle	obvykle	k6eAd1
vydáván	vydávat	k5eAaImNgInS,k5eAaPmNgInS
společně	společně	k6eAd1
s	s	k7c7
texty	text	k1gInPc7
Rukopisu	rukopis	k1gInSc2
královédvorského	královédvorský	k2eAgInSc2d1
<g/>
,	,	kIx,
Rukopisu	rukopis	k1gInSc2
zelenohorského	zelenohorský	k2eAgNnSc2d1
a	a	k8xC
Milostné	milostný	k2eAgFnPc1d1
písně	píseň	k1gFnPc1
krále	král	k1gMnSc2
Václava	Václav	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Objevení	objevení	k1gNnSc1
a	a	k8xC
publikace	publikace	k1gFnSc1
</s>
<s>
Rukopis	rukopis	k1gInSc1
jako	jako	k9
vůbec	vůbec	k9
první	první	k4xOgInSc4
ze	z	k7c2
sporných	sporný	k2eAgInPc2d1
pergamenů	pergamen	k1gInPc2
objevil	objevit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1816	#num#	k4
Josef	Josef	k1gMnSc1
Linda	Linda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jeden	jeden	k4xCgInSc4
list	list	k1gInSc4
rozměru	rozměr	k1gInSc2
asi	asi	k9
15	#num#	k4
x	x	k?
22	#num#	k4
centimetrů	centimetr	k1gInPc2
<g/>
,	,	kIx,
popsaný	popsaný	k2eAgInSc1d1
po	po	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Text	text	k1gInSc1
má	mít	k5eAaImIp3nS
19	#num#	k4
řádků	řádek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prý	prý	k9
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgMnS
v	v	k7c6
deskách	deska	k1gFnPc6
velké	velký	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
nájemců	nájemce	k1gMnPc2
domu	dům	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Linda	Linda	k1gFnSc1
bydlel	bydlet	k5eAaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
používal	používat	k5eAaImAgMnS
jako	jako	k8xC,k8xS
podnožku	podnožka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linda	Linda	k1gFnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
údajně	údajně	k6eAd1
přiznal	přiznat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
špatně	špatně	k6eAd1
čitelná	čitelný	k2eAgNnPc1d1
písmena	písmeno	k1gNnPc1
z	z	k7c2
neznalosti	neznalost	k1gFnSc2
nevhodně	vhodně	k6eNd1
na	na	k7c6
pergamenu	pergamen	k1gInSc6
obtáhl	obtáhnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
první	první	k4xOgFnSc1
část	část	k1gFnSc1
útržku	útržek	k1gInSc2
básně	báseň	k1gFnSc2
opěvuje	opěvovat	k5eAaImIp3nS
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
odvozovaly	odvozovat	k5eAaImAgInP
názvy	název	k1gInPc1
pergamenu	pergamen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Národním	národní	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
je	být	k5eAaImIp3nS
uložen	uložen	k2eAgInSc1d1
pod	pod	k7c7
označením	označení	k1gNnSc7
Píseň	píseň	k1gFnSc1
pod	pod	k7c7
Vyšehradem	Vyšehrad	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Pod	pod	k7c7
stejnou	stejný	k2eAgFnSc7d1
signaturou	signatura	k1gFnSc7
1	#num#	k4
A	A	kA
b	b	k?
4	#num#	k4
je	být	k5eAaImIp3nS
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
uložen	uložit	k5eAaPmNgInS
také	také	k9
rukou	ruka	k1gFnSc7
psaný	psaný	k2eAgInSc4d1
opis	opis	k1gInSc4
textu	text	k1gInSc2
Písně	píseň	k1gFnSc2
pod	pod	k7c7
Vyšehradem	Vyšehrad	k1gInSc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
původ	původ	k1gInSc1
není	být	k5eNaImIp3nS
známý	známý	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
u	u	k7c2
něj	on	k3xPp3gMnSc2
i	i	k9
německo-latinská	německo-latinský	k2eAgFnSc1d1
poznámka	poznámka	k1gFnSc1
připisovaná	připisovaný	k2eAgFnSc1d1
Josefu	Josef	k1gMnSc3
Dobrovskému	Dobrovský	k1gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
19	#num#	k4
řádků	řádek	k1gInPc2
na	na	k7c6
pergamenovém	pergamenový	k2eAgInSc6d1
listu	list	k1gInSc6
objevil	objevit	k5eAaPmAgMnS
Linda	Linda	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přepis	přepis	k1gInSc4
textu	text	k1gInSc2
publikoval	publikovat	k5eAaBmAgMnS
Lindův	Lindův	k2eAgMnSc1d1
tehdejší	tehdejší	k2eAgMnSc1d1
spolubydlící	spolubydlící	k1gMnSc1
a	a	k8xC
údajný	údajný	k2eAgMnSc1d1
objevitel	objevitel	k1gMnSc1
Rukopisu	rukopis	k1gInSc2
královédvorského	královédvorský	k2eAgNnSc2d1
Václav	Václav	k1gMnSc1
Hanka	Hanka	k1gFnSc1
v	v	k7c6
prvním	první	k4xOgInSc6
svazku	svazek	k1gInSc6
Starobylých	starobylý	k2eAgNnPc2d1
skládání	skládání	k1gNnPc2
roku	rok	k1gInSc2
1817	#num#	k4
<g/>
;	;	kIx,
tím	ten	k3xDgNnSc7
ho	on	k3xPp3gInSc4
zařadil	zařadit	k5eAaPmAgInS
do	do	k7c2
období	období	k1gNnSc2
13	#num#	k4
<g/>
.	.	kIx.
až	až	k9
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
text	text	k1gInSc4
roku	rok	k1gInSc2
1818	#num#	k4
otiskl	otisknout	k5eAaPmAgMnS
i	i	k9
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
jako	jako	k9
ukázku	ukázka	k1gFnSc4
staročeské	staročeský	k2eAgFnSc2d1
poezie	poezie	k1gFnSc2
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
německy	německy	k6eAd1
psaném	psaný	k2eAgNnSc6d1
díle	dílo	k1gNnSc6
Dějiny	dějiny	k1gFnPc1
české	český	k2eAgFnSc2d1
řeči	řeč	k1gFnSc2
a	a	k8xC
literatury	literatura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obdobný	obdobný	k2eAgInSc4d1
text	text	k1gInSc4
byl	být	k5eAaImAgMnS
Národnímu	národní	k2eAgNnSc3d1
muzeu	muzeum	k1gNnSc3
později	pozdě	k6eAd2
předán	předat	k5eAaPmNgInS
ještě	ještě	k9
dvakrát	dvakrát	k6eAd1
<g/>
:	:	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1856	#num#	k4
německý	německý	k2eAgInSc1d1
překlad	překlad	k1gInSc1
<g/>
,	,	kIx,
nalezený	nalezený	k2eAgInSc1d1
údajně	údajně	k6eAd1
ve	v	k7c6
městě	město	k1gNnSc6
Raabu	Raab	k1gInSc2
a	a	k8xC
antedatovaný	antedatovaný	k2eAgInSc1d1
do	do	k7c2
roku	rok	k1gInSc2
1724	#num#	k4
(	(	kIx(
<g/>
s	s	k7c7
označením	označení	k1gNnSc7
„	„	k?
<g/>
Alt-böhmisches	Alt-böhmisches	k1gMnSc1
Lied	Lied	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
staročeská	staročeský	k2eAgFnSc1d1
píseň	píseň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1933	#num#	k4
takzvaný	takzvaný	k2eAgInSc1d1
Negativní	negativní	k2eAgInSc1d1
otisk	otisk	k1gInSc1
<g/>
,	,	kIx,
objevený	objevený	k2eAgInSc1d1
údajně	údajně	k6eAd1
ve	v	k7c6
Štýrském	štýrský	k2eAgInSc6d1
Hradci	Hradec	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1891	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hodnocení	hodnocení	k1gNnSc1
pravosti	pravost	k1gFnSc2
</s>
<s>
Už	už	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1824	#num#	k4
označil	označit	k5eAaPmAgMnS
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
svůj	svůj	k3xOyFgInSc4
dřívější	dřívější	k2eAgInSc4d1
názor	názor	k1gInSc4
za	za	k7c4
omyl	omyl	k1gInSc4
a	a	k8xC
označil	označit	k5eAaPmAgMnS
Píseň	píseň	k1gFnSc4
vyšehradskou	vyšehradský	k2eAgFnSc4d1
za	za	k7c4
podvrh	podvrh	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Pravost	pravost	k1gFnSc1
rukopisu	rukopis	k1gInSc2
zkoumala	zkoumat	k5eAaImAgFnS
koncem	koncem	k7c2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
komise	komise	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc2
práce	práce	k1gFnSc2
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
i	i	k9
Pavel	Pavel	k1gMnSc1
Josef	Josef	k1gMnSc1
Šafařík	Šafařík	k1gMnSc1
a	a	k8xC
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
dospěla	dochvít	k5eAaPmAgFnS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
padělek	padělek	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1886	#num#	k4
zkoumala	zkoumat	k5eAaImAgFnS
Píseň	píseň	k1gFnSc1
vyšehradskou	vyšehradský	k2eAgFnSc4d1
další	další	k2eAgFnSc4d1
muzejní	muzejní	k2eAgFnSc4d1
komise	komise	k1gFnPc4
s	s	k7c7
profesory	profesor	k1gMnPc7
Vojtěchem	Vojtěch	k1gMnSc7
Šafaříkem	Šafařík	k1gMnSc7
a	a	k8xC
Antonínem	Antonín	k1gMnSc7
Bělohoubkem	Bělohoubek	k1gMnSc7
<g/>
:	:	kIx,
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
potvrdila	potvrdit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
písmo	písmo	k1gNnSc1
má	mít	k5eAaImIp3nS
dvojité	dvojitý	k2eAgInPc4d1
tahy	tah	k1gInPc4
(	(	kIx(
<g/>
což	což	k3yQnSc1,k3yRnSc1
mohl	moct	k5eAaImAgMnS
způsobit	způsobit	k5eAaPmF
Linda	Linda	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
hlavně	hlavně	k9
konstatovala	konstatovat	k5eAaBmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
při	při	k7c6
chemických	chemický	k2eAgFnPc6d1
zkouškách	zkouška	k1gFnPc6
–	–	k?
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Rukopisu	rukopis	k1gInSc2
královédvorského	královédvorský	k2eAgInSc2d1
-	-	kIx~
rukopis	rukopis	k1gInSc4
Písně	píseň	k1gFnSc2
vyšehradské	vyšehradský	k2eAgFnSc2d1
choval	chovat	k5eAaImAgMnS
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
rukopisy	rukopis	k1gInPc4
nedávného	dávný	k2eNgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
jako	jako	k9
rukopis	rukopis	k1gInSc1
středověký	středověký	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1897	#num#	k4
se	se	k3xPyFc4
dokonce	dokonce	k9
objevil	objevit	k5eAaPmAgInS
názor	názor	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
exemplář	exemplář	k1gInSc1
Písně	píseň	k1gFnSc2
vyšehradské	vyšehradský	k2eAgFnSc2d1
uchovávaný	uchovávaný	k2eAgInSc1d1
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
je	být	k5eAaImIp3nS
„	„	k?
<g/>
padělek	padělek	k1gInSc1
padělku	padělek	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
tedy	tedy	k9
že	že	k8xS
bylo	být	k5eAaImAgNnS
původní	původní	k2eAgNnSc1d1
falzum	falzum	k1gNnSc1
nahrazeno	nahrazen	k2eAgNnSc1d1
jiným	jiné	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
věrohodnější	věrohodný	k2eAgNnSc1d2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Píseň	píseň	k1gFnSc1
vyšehradská	vyšehradský	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
zkoumána	zkoumat	k5eAaImNgFnS
také	také	k9
v	v	k7c6
Kriminalistickém	kriminalistický	k2eAgInSc6d1
ústavu	ústav	k1gInSc6
takzvaným	takzvaný	k2eAgInSc7d1
Ivanovovým	Ivanovův	k2eAgInSc7d1
týmem	tým	k1gInSc7
v	v	k7c6
letech	let	k1gInPc6
1968	#num#	k4
–	–	k?
1971	#num#	k4
s	s	k7c7
jednoznačným	jednoznačný	k2eAgInSc7d1
závěrem	závěr	k1gInSc7
<g/>
:	:	kIx,
exemplář	exemplář	k1gInSc1
z	z	k7c2
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
je	být	k5eAaImIp3nS
palimpsest	palimpsest	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Text	text	k1gInSc1
Písně	píseň	k1gFnSc2
vyšehradské	vyšehradský	k2eAgFnSc2d1
</s>
<s>
Pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
obsah	obsah	k1gInSc4
textu	text	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Píseň	píseň	k1gFnSc1
vyšehradská	vyšehradský	k2eAgFnSc1d1
fragmentem	fragment	k1gInSc7
písně	píseň	k1gFnSc2
<g/>
,	,	kIx,
připomínající	připomínající	k2eAgFnSc4d1
středověkou	středověký	k2eAgFnSc4d1
milostnou	milostný	k2eAgFnSc4d1
lyriku	lyrika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začíná	začínat	k5eAaImIp3nS
vlasteneckou	vlastenecký	k2eAgFnSc7d1
oslavou	oslava	k1gFnSc7
Vyšehradu	Vyšehrad	k1gInSc2
a	a	k8xC
pokračuje	pokračovat	k5eAaImIp3nS
citovými	citový	k2eAgInPc7d1
výlevy	výlev	k1gInPc7
zamilovaného	zamilovaný	k1gMnSc2
mladíka	mladík	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
české	český	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
nenašel	najít	k5eNaPmAgMnS
tento	tento	k3xDgInSc4
text	text	k1gInSc4
větší	veliký	k2eAgFnSc4d2
odezvu	odezva	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
tetů	tet	k1gInPc2
Rukopisu	rukopis	k1gInSc2
královedvorského	královedvorský	k2eAgInSc2d1
či	či	k8xC
Rukopisu	rukopis	k1gInSc2
zelenohorského	zelenohorský	k2eAgInSc2d1
(	(	kIx(
<g/>
líbil	líbit	k5eAaImAgInS
se	se	k3xPyFc4
prý	prý	k9
Karlu	Karel	k1gMnSc3
Hynku	Hynek	k1gMnSc3
Máchovi	Mácha	k1gMnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Autorství	autorství	k1gNnSc1
textu	text	k1gInSc2
je	být	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
připisováno	připisovat	k5eAaImNgNnS
Václavu	Václav	k1gMnSc3
Hankovi	Hanek	k1gMnSc3
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
např.	např.	kA
podle	podle	k7c2
Julia	Julius	k1gMnSc2
Enderse	Enderse	k1gFnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
text	text	k1gInSc4
Písně	píseň	k1gFnSc2
vyšehradské	vyšehradský	k2eAgFnSc2d1
opravdu	opravdu	k6eAd1
opisem	opis	k1gInSc7
nebo	nebo	k8xC
zápisem	zápis	k1gInSc7
nějakého	nějaký	k3yIgInSc2
textu	text	k1gInSc2
vzniklého	vzniklý	k2eAgInSc2d1
dříve	dříve	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
I	i	k9
u	u	k7c2
německé	německý	k2eAgFnSc2d1
verze	verze	k1gFnSc2
dodané	dodaný	k2eAgFnSc6d1
do	do	k7c2
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1856	#num#	k4
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc4
„	„	k?
<g/>
stará	starat	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
píseň	píseň	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Případná	případný	k2eAgFnSc1d1
znalost	znalost	k1gFnSc1
podobného	podobný	k2eAgNnSc2d1
<g/>
,	,	kIx,
již	již	k6eAd1
existujícího	existující	k2eAgInSc2d1
textu	text	k1gInSc2
není	být	k5eNaImIp3nS
v	v	k7c6
rozporu	rozpor	k1gInSc6
ani	ani	k8xC
s	s	k7c7
hypotézou	hypotéza	k1gFnSc7
Zdenka	Zdenka	k1gFnSc1
F.	F.	kA
Daneše	Daneš	k1gMnSc2
<g/>
,	,	kIx,
že	že	k8xS
Píseň	píseň	k1gFnSc1
vyšehradská	vyšehradský	k2eAgFnSc1d1
mohla	moct	k5eAaImAgFnS
vzniknout	vzniknout	k5eAaPmF
jako	jako	k9
pokus	pokus	k1gInSc4
začínajícího	začínající	k2eAgMnSc2d1
písaře	písař	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
si	se	k3xPyFc3
pergamen	pergamen	k1gInSc1
jako	jako	k9
poměrně	poměrně	k6eAd1
vzácný	vzácný	k2eAgInSc4d1
materiál	materiál	k1gInSc4
uchoval	uchovat	k5eAaPmAgMnS
pro	pro	k7c4
možné	možný	k2eAgNnSc4d1
další	další	k2eAgNnSc4d1
použití	použití	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
IVANOV	IVANOV	kA
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Utajené	utajený	k2eAgInPc1d1
protokoly	protokol	k1gInPc1
aneb	aneb	k?
geniální	geniální	k2eAgInSc1d1
podvod	podvod	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vydavatelství	vydavatelství	k1gNnSc1
a	a	k8xC
nakladatelství	nakladatelství	k1gNnSc1
MV	MV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
224	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85821	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
101,102	101,102	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
DOBIÁŠ	Dobiáš	k1gMnSc1
<g/>
,	,	kIx,
Dalibor	Dalibor	k1gMnSc1
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rukopisy	rukopis	k1gInPc1
královédvorský	královédvorský	k2eAgInSc1d1
a	a	k8xC
zelenohorský	zelenohorský	k2eAgInSc1d1
a	a	k8xC
česká	český	k2eAgFnSc1d1
věda	věda	k1gFnSc1
(	(	kIx(
<g/>
1817	#num#	k4
<g/>
-	-	kIx~
<g/>
1885	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
2421	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
598	#num#	k4
<g/>
-	-	kIx~
<g/>
601	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
BRČÁK	BRČÁK	kA
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rukopisné	rukopisný	k2eAgInPc1d1
zlomky	zlomek	k1gInPc1
Knihovny	knihovna	k1gFnSc2
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
<g/>
:	:	kIx,
Signatura	signatura	k1gFnSc1
1	#num#	k4
A.	A.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
Scriptorium	Scriptorium	k1gNnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7036	#num#	k4
<g/>
-	-	kIx~
<g/>
421	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
201	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NESMĚRÁK	NESMĚRÁK	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosud	dosud	k6eAd1
neznámý	známý	k2eNgInSc4d1
opis	opis	k1gInSc4
Písně	píseň	k1gFnSc2
vyšehradské	vyšehradský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Almanach	almanach	k1gInSc1
rukopisné	rukopisný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
NEKLAN	NEKLAN	kA
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
104	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
900884	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
53	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BENEŠ	Beneš	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rukopisné	rukopisný	k2eAgInPc1d1
zlomky	zlomek	k1gInPc1
Knihovny	knihovna	k1gFnSc2
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
<g/>
:	:	kIx,
signatury	signatura	k1gFnSc2
1	#num#	k4
B	B	kA
a	a	k8xC
1	#num#	k4
C.	C.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
Scriptorium	Scriptorium	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7036	#num#	k4
<g/>
-	-	kIx~
<g/>
448	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
845	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
AUGUSTA	Augusta	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
;	;	kIx,
HONZÁK	HONZÁK	kA
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
byl	být	k5eAaImAgMnS
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
v	v	k7c6
našich	náš	k3xOp1gFnPc6
dějinách	dějiny	k1gFnPc6
do	do	k7c2
roku	rok	k1gInSc2
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901579	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
302	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NOVOTNÝ	Novotný	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pavel	Pavel	k1gMnSc1
Josef	Josef	k1gMnSc1
Šafařík	Šafařík	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Melantrich	Melantrich	k1gMnSc1
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
195	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
PhDr.	PhDr.	kA
Z.	Z.	kA
F.	F.	kA
Daneš	Daneš	k1gMnSc1
<g/>
,	,	kIx,
Rukopisy	rukopis	k1gInPc1
Královédvorské	královédvorský	k2eAgInPc1d1
a	a	k8xC
Zelenohorské	zelenohorský	k2eAgInPc1d1
<g/>
.	.	kIx.
zfdanes	zfdanes	k1gInSc1
<g/>
.	.	kIx.
<g/>
sweb	sweb	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TRUHLÁŘ	Truhlář	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
účelech	účel	k1gInPc6
padělků	padělek	k1gInPc2
musejních	musejní	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athenaeum	Athenaeum	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Březen	březen	k1gInSc1
1887	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
184	#num#	k4
<g/>
-	-	kIx~
<g/>
189	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
IVANOV	IVANOV	kA
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protokoly	protokol	k1gInPc4
o	o	k7c6
zkoumání	zkoumání	k1gNnSc6
Rukopisů	rukopis	k1gInPc2
královédvorského	královédvorský	k2eAgNnSc2d1
a	a	k8xC
zelenohorského	zelenohorský	k2eAgMnSc2d1
a	a	k8xC
některých	některý	k3yIgInPc2
dalších	další	k2eAgInPc2d1
rukopisů	rukopis	k1gInPc2
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
-	-	kIx~
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Nakladatelství	nakladatelství	k1gNnSc1
a	a	k8xC
vydavatelství	vydavatelství	k1gNnSc1
Panorama	panorama	k1gNnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
116	#num#	k4
s.	s.	k?
S.	S.	kA
110	#num#	k4
<g/>
,	,	kIx,
111	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Almanach	almanach	k1gInSc4
rukopisné	rukopisný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
<g/>
.	.	kIx.
www.rukopisy-rkz.cz	www.rukopisy-rkz.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PhDr.	PhDr.	kA
Z.	Z.	kA
F.	F.	kA
Daneš	Daneš	k1gMnSc1
<g/>
,	,	kIx,
Rukopisy	rukopis	k1gInPc1
Královédvorské	královédvorský	k2eAgInPc1d1
a	a	k8xC
Zelenohorské	zelenohorský	k2eAgInPc1d1
<g/>
.	.	kIx.
zfdanes	zfdanes	k1gInSc1
<g/>
.	.	kIx.
<g/>
sweb	sweb	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
HANKA	Hanka	k1gFnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
:	:	kIx,
Starobylá	starobylý	k2eAgFnSc1d1
Skládanie	Skládanie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památka	památka	k1gFnSc1
XIII	XIII	kA
<g/>
.	.	kIx.
a	a	k8xC
XIV	XIV	kA
<g/>
.	.	kIx.
věku	věk	k1gInSc2
z	z	k7c2
nejvzácnějších	vzácný	k2eAgInPc2d3
rukopisů	rukopis	k1gInPc2
vydaná	vydaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
první	první	k4xOgInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Bohumil	Bohumil	k1gMnSc1
Haase	Haase	k1gFnSc2
<g/>
,	,	kIx,
1817	#num#	k4
</s>
<s>
DOBROVSKÝ	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
:	:	kIx,
Geschichte	Geschicht	k1gInSc5
der	drát	k5eAaImRp2nS
Böhmischen	Böhmischen	k1gInSc4
Sprache	Sprach	k1gMnSc2
und	und	k?
ältern	ältern	k1gInSc1
Literatur	literatura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ganz	Ganz	k1gInSc1
umgearbeitete	umgearbeiíst	k5eAaPmIp2nP
Ausgabe	Ausgab	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Bohumil	Bohumil	k1gMnSc1
Haase	Haase	k1gFnSc2
<g/>
,	,	kIx,
1818	#num#	k4
</s>
<s>
Novodobé	novodobý	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
</s>
<s>
Staročeské	staročeský	k2eAgInPc1d1
zpěvy	zpěv	k1gInPc1
hrdinské	hrdinský	k2eAgInPc1d1
a	a	k8xC
milostné	milostný	k2eAgInPc1d1
<g/>
:	:	kIx,
Rukopisy	rukopis	k1gInPc1
královédvorský	královédvorský	k2eAgMnSc1d1
a	a	k8xC
zelenohorský	zelenohorský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydání	vydání	k1gNnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílovice	Bílovice	k1gInPc4
<g/>
:	:	kIx,
Černý	Černý	k1gMnSc1
Drak	drak	k1gMnSc1
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
143	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
907324	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Obsahuje	obsahovat	k5eAaImIp3nS
oba	dva	k4xCgInPc4
Rukopisy	rukopis	k1gInPc4
v	v	k7c6
novodobé	novodobý	k2eAgFnSc6d1
transkripci	transkripce	k1gFnSc6
společně	společně	k6eAd1
s	s	k7c7
Písní	píseň	k1gFnSc7
vyšehradskou	vyšehradský	k2eAgFnSc7d1
a	a	k8xC
Milostnou	milostný	k2eAgFnSc7d1
písní	píseň	k1gFnSc7
krále	král	k1gMnSc2
Václava	Václav	k1gMnSc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Dílo	dílo	k1gNnSc4
Milostná	milostný	k2eAgFnSc1d1
píseň	píseň	k1gFnSc1
pod	pod	k7c7
Vyšehradem	Vyšehrad	k1gInSc7
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Píseň	píseň	k1gFnSc1
vyšehradská	vyšehradský	k2eAgFnSc1d1
v	v	k7c6
textech	text	k1gInPc6
J.	J.	kA
Enderse	Enderse	k1gFnSc2
–	–	k?
zastánce	zastánce	k1gMnPc4
pravosti	pravost	k1gFnSc2
rukopisu	rukopis	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
</s>
