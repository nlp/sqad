<s>
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
</s>
<s>
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
</s>
<s>
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
<g/>
Stát	stát	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
Narození	narození	k1gNnSc6
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1981	#num#	k4
(	(	kIx(
<g/>
39	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Oviedo	Oviedo	k1gNnSc1
<g/>
,	,	kIx,
Asturias	Asturias	k1gInSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
Současné	současný	k2eAgNnSc1d1
působení	působení	k1gNnSc1
ve	v	k7c6
Formuli	formule	k1gFnSc6
1	#num#	k4
Současný	současný	k2eAgInSc1d1
tým	tým	k1gInSc1
</s>
<s>
Alpine	Alpinout	k5eAaPmIp3nS
<g/>
–	–	k?
<g/>
Renault	renault	k1gInSc1
Číslo	číslo	k1gNnSc1
vozu	vůz	k1gInSc2
</s>
<s>
14	#num#	k4
Umístění	umístění	k1gNnSc1
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
(	(	kIx(
<g/>
5	#num#	k4
b.	b.	k?
<g/>
)	)	kIx)
Kariéra	kariéra	k1gFnSc1
ve	v	k7c6
Formuli	formule	k1gFnSc6
1	#num#	k4
Aktivní	aktivní	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
</s>
<s>
2001	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
-	-	kIx~
<g/>
2018	#num#	k4
<g/>
,	,	kIx,
2021	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
Týmy	tým	k1gInPc4
</s>
<s>
Minardi	Minard	k1gMnPc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
Renault	renault	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
-	-	kIx~
<g/>
2006	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
Ferrari	Ferrari	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
McLaren	McLarna	k1gFnPc2
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
Alpine	Alpin	k1gInSc5
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
Závody	závod	k1gInPc1
</s>
<s>
317	#num#	k4
(	(	kIx(
<g/>
314	#num#	k4
startů	start	k1gInPc2
<g/>
)	)	kIx)
Mistr	mistr	k1gMnSc1
světa	svět	k1gInSc2
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
)	)	kIx)
Vyhrané	vyhraný	k2eAgInPc1d1
závody	závod	k1gInPc1
</s>
<s>
32	#num#	k4
Stupně	stupeň	k1gInSc2
vítězů	vítěz	k1gMnPc2
</s>
<s>
97	#num#	k4
Pole	pole	k1gNnSc1
positions	positions	k6eAd1
</s>
<s>
22	#num#	k4
Nejrychlejší	rychlý	k2eAgFnSc1d3
kola	kola	k1gFnSc1
</s>
<s>
23	#num#	k4
Body	bod	k1gInPc7
celkem	celkem	k6eAd1
</s>
<s>
1	#num#	k4
904	#num#	k4
Nejlepší	dobrý	k2eAgInPc1d3
umístění	umístění	k1gNnSc4
v	v	k7c6
závodě	závod	k1gInSc6
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
První	první	k4xOgInSc1
závod	závod	k1gInSc1
</s>
<s>
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Austrálie	Austrálie	k1gFnSc2
2001	#num#	k4
První	první	k4xOgInSc4
vítězství	vítězství	k1gNnPc2
</s>
<s>
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Maďarska	Maďarsko	k1gNnSc2
2003	#num#	k4
Poslední	poslední	k2eAgFnSc1d1
vítězství	vítězství	k1gNnSc3
</s>
<s>
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Španělska	Španělsko	k1gNnSc2
2013	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fernando	Fernando	k1gMnSc1
Alonso	Alonso	k1gMnSc1
Díaz	Díaz	k1gInSc4
(	(	kIx(
<g/>
*	*	kIx~
29	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1981	#num#	k4
Oviedo	Oviedo	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
španělský	španělský	k2eAgMnSc1d1
automobilový	automobilový	k2eAgMnSc1d1
závodník	závodník	k1gMnSc1
<g/>
,	,	kIx,
pilot	pilot	k1gMnSc1
Formule	formule	k1gFnSc2
1	#num#	k4
<g/>
,	,	kIx,
dvojnásobný	dvojnásobný	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
světa	svět	k1gInSc2
z	z	k7c2
let	léto	k1gNnPc2
2005	#num#	k4
a	a	k8xC
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Žije	žít	k5eAaImIp3nS
v	v	k7c6
rodném	rodný	k2eAgMnSc6d1
Oviedu	Ovied	k1gMnSc6
<g/>
,	,	kIx,
kam	kam	k6eAd1
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
v	v	k7c6
zimě	zima	k1gFnSc6
roku	rok	k1gInSc2
2010	#num#	k4
ze	z	k7c2
švýcarského	švýcarský	k2eAgMnSc2d1
Lugana	Lugan	k1gMnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgMnS
blíže	blízce	k6eAd2
rodině	rodina	k1gFnSc3
a	a	k8xC
přátelům	přítel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
přesun	přesun	k1gInSc1
ho	on	k3xPp3gMnSc4
ovšem	ovšem	k9
přišel	přijít	k5eAaPmAgMnS
na	na	k7c6
daních	daň	k1gFnPc6
na	na	k7c4
50	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
liber	libra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2005	#num#	k4
se	se	k3xPyFc4
ve	v	k7c6
24	#num#	k4
letech	let	k1gInPc6
a	a	k8xC
58	#num#	k4
dnech	den	k1gInPc6
stal	stát	k5eAaPmAgInS
nejmladším	mladý	k2eAgMnSc7d3
mistrem	mistr	k1gMnSc7
světa	svět	k1gInSc2
v	v	k7c6
historii	historie	k1gFnSc6
Formule	formule	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
překonal	překonat	k5eAaPmAgMnS
rekord	rekord	k1gInSc4
Emersona	Emerson	k1gMnSc2
Fittipaldiho	Fittipaldi	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1972	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
dalších	další	k2eAgNnPc6d1
letech	léto	k1gNnPc6
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
rekord	rekord	k1gInSc1
překonán	překonán	k2eAgInSc1d1
Lewisem	Lewis	k1gInSc7
Hamiltonem	Hamilton	k1gInSc7
a	a	k8xC
Sebastianem	Sebastian	k1gMnSc7
Vettelem	Vettel	k1gMnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
přezdívka	přezdívka	k1gFnSc1
je	být	k5eAaImIp3nS
El	Ela	k1gFnPc2
Nano	Nano	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
rodné	rodný	k2eAgFnSc2d1
španělštiny	španělština	k1gFnSc2
mluví	mluvit	k5eAaImIp3nS
také	také	k9
anglicky	anglicky	k6eAd1
a	a	k8xC
italsky	italsky	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgInS
29	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1981	#num#	k4
v	v	k7c4
Oviedu	Ovieda	k1gMnSc4
<g/>
,	,	kIx,
v	v	k7c6
provincii	provincie	k1gFnSc6
Asturie	Asturie	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
severním	severní	k2eAgNnSc6d1
Španělsku	Španělsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
matka	matka	k1gFnSc1
pracovala	pracovat	k5eAaImAgFnS
v	v	k7c6
obchodním	obchodní	k2eAgInSc6d1
domě	dům	k1gInSc6
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
zaměstnán	zaměstnat	k5eAaPmNgMnS
jako	jako	k8xS,k8xC
expert	expert	k1gMnSc1
na	na	k7c4
výbušniny	výbušnina	k1gFnPc4
v	v	k7c6
těžebním	těžební	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Alonso	Alonsa	k1gFnSc5
má	mít	k5eAaImIp3nS
starší	starý	k2eAgFnSc4d2
sestru	sestra	k1gFnSc4
Lorenu	Loren	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alonsův	Alonsův	k2eAgMnSc1d1
otec	otec	k1gMnSc1
José	José	k1gNnSc2
Luis	Luisa	k1gFnPc2
byl	být	k5eAaImAgMnS
amatérským	amatérský	k2eAgMnSc7d1
motokárovým	motokárový	k2eAgMnSc7d1
jezdcem	jezdec	k1gMnSc7
a	a	k8xC
přál	přát	k5eAaImAgInS
si	se	k3xPyFc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
jeho	jeho	k3xOp3gFnPc1
děti	dítě	k1gFnPc1
pokračovaly	pokračovat	k5eAaImAgFnP
v	v	k7c6
jeho	jeho	k3xOp3gFnPc6
stopách	stopa	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sestrojil	sestrojit	k5eAaPmAgMnS
jim	on	k3xPp3gMnPc3
motokáru	motokára	k1gFnSc4
<g/>
,	,	kIx,
nejprve	nejprve	k6eAd1
určenou	určený	k2eAgFnSc7d1
pro	pro	k7c4
tehdy	tehdy	k6eAd1
osmiletou	osmiletý	k2eAgFnSc4d1
Lorenu	Lorena	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
ale	ale	k9
o	o	k7c4
motokáry	motokára	k1gFnPc4
příliš	příliš	k6eAd1
zájem	zájem	k1gInSc4
nejevila	jevit	k5eNaImAgFnS
<g/>
,	,	kIx,
oproti	oproti	k7c3
tříletému	tříletý	k2eAgMnSc3d1
Fernandovi	Fernand	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Fernando	Fernando	k6eAd1
se	s	k7c7
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2006	#num#	k4
oženil	oženit	k5eAaPmAgInS
s	s	k7c7
Raquel	Raquel	k1gMnSc1
del	del	k?
Rosario	Rosario	k1gMnSc1
Macías	Macías	k1gMnSc1
<g/>
,	,	kIx,
zpěvačkou	zpěvačka	k1gFnSc7
španělské	španělský	k2eAgFnSc2d1
popové	popový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
El	Ela	k1gFnPc2
Sueňo	Sueňo	k1gMnSc1
de	de	k?
Morfeo	morfea	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
Kariéra	kariéra	k1gFnSc1
před	před	k7c7
formulí	formule	k1gFnSc7
1	#num#	k4
</s>
<s>
Jako	jako	k9
dítě	dítě	k1gNnSc4
objížděl	objíždět	k5eAaImAgMnS
Fernando	Fernanda	k1gFnSc5
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
otcem	otec	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgMnS
rovněž	rovněž	k6eAd1
mechanikem	mechanik	k1gMnSc7
<g/>
,	,	kIx,
motokárové	motokárový	k2eAgInPc1d1
závody	závod	k1gInPc1
po	po	k7c6
Španělsku	Španělsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alonsova	Alonsův	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
neměla	mít	k5eNaImAgFnS
příliš	příliš	k6eAd1
peněz	peníze	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
Fernandovy	Fernandův	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
znamenaly	znamenat	k5eAaImAgFnP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
o	o	k7c4
něj	on	k3xPp3gMnSc4
zajímali	zajímat	k5eAaImAgMnP
sponzoři	sponzor	k1gMnPc1
a	a	k8xC
o	o	k7c4
peníze	peníz	k1gInPc4
bylo	být	k5eAaImAgNnS
postaráno	postarán	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Fernando	Fernando	k6eAd1
čtyřikrát	čtyřikrát	k6eAd1
vyhrál	vyhrát	k5eAaPmAgMnS
Španělský	španělský	k2eAgInSc4d1
šampionát	šampionát	k1gInSc4
v	v	k7c6
juniorské	juniorský	k2eAgFnSc6d1
kategorii	kategorie	k1gFnSc6
v	v	k7c6
letech	léto	k1gNnPc6
1993	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
1995	#num#	k4
a	a	k8xC
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
vyhrál	vyhrát	k5eAaPmAgInS
Juniorský	juniorský	k2eAgInSc1d1
světový	světový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
vyhrál	vyhrát	k5eAaPmAgMnS
Španělský	španělský	k2eAgMnSc1d1
a	a	k8xC
Italský	italský	k2eAgMnSc1d1
Inter-A	Inter-A	k1gMnSc1
titul	titul	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
a	a	k8xC
skončil	skončit	k5eAaPmAgMnS
druhý	druhý	k4xOgMnSc1
v	v	k7c6
Evropském	evropský	k2eAgInSc6d1
šampionátu	šampionát	k1gInSc6
roku	rok	k1gInSc2
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
témže	týž	k3xTgInSc6
roce	rok	k1gInSc6
znovu	znovu	k6eAd1
vyhrál	vyhrát	k5eAaPmAgMnS
Španělský	španělský	k2eAgMnSc1d1
Inter-A	Inter-A	k1gMnSc1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1
formulový	formulový	k2eAgMnSc1d1
jezdec	jezdec	k1gMnSc1
týmu	tým	k1gInSc2
Minardi	Minard	k1gMnPc1
<g/>
,	,	kIx,
Adrián	Adrián	k1gMnSc1
Campos	Campos	k1gMnSc1
<g/>
,	,	kIx,
dal	dát	k5eAaPmAgMnS
v	v	k7c6
říjnu	říjen	k1gInSc6
1998	#num#	k4
Fernandovi	Fernandův	k2eAgMnPc1d1
šanci	šance	k1gFnSc4
vyzkoušet	vyzkoušet	k5eAaPmF
si	se	k3xPyFc3
závodní	závodní	k2eAgInSc4d1
vůz	vůz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
třech	tři	k4xCgInPc6
dnech	den	k1gInPc6
testování	testování	k1gNnSc2
měl	mít	k5eAaImAgInS
Alonso	Alonsa	k1gFnSc5
srovnatelné	srovnatelný	k2eAgInPc4d1
časy	čas	k1gInPc4
s	s	k7c7
Camposovým	Camposový	k2eAgMnSc7d1
předchozím	předchozí	k2eAgMnSc7d1
závodníkem	závodník	k1gMnSc7
Marcem	Marce	k1gMnSc7
Geném	Gený	k2eAgNnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Campos	Campos	k1gInSc1
poté	poté	k6eAd1
dal	dát	k5eAaPmAgInS
Fernandovi	Fernandův	k2eAgMnPc1d1
šanci	šance	k1gFnSc4
závodit	závodit	k5eAaImF
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
roku	rok	k1gInSc2
1999	#num#	k4
ve	v	k7c6
Španělské	španělský	k2eAgFnSc6d1
Formuli	formule	k1gFnSc6
Nissan	nissan	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Hned	hned	k6eAd1
ve	v	k7c6
druhém	druhý	k4xOgInSc6
závodě	závod	k1gInSc6
Alonso	Alonsa	k1gFnSc5
zvítězil	zvítězit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
na	na	k7c6
okruhu	okruh	k1gInSc6
Albecete	Albece	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
testoval	testovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledním	poslední	k2eAgInSc6d1
závodě	závod	k1gInSc6
potřeboval	potřebovat	k5eAaImAgInS
Alonso	Alonsa	k1gFnSc5
vyhrát	vyhrát	k5eAaPmF
a	a	k8xC
zajet	zajet	k5eAaPmF
nejrychlejší	rychlý	k2eAgNnSc4d3
kolo	kolo	k1gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
porazil	porazit	k5eAaPmAgMnS
Manuela	Manuel	k1gMnSc4
Giaoa	Giaous	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
s	s	k7c7
ním	on	k3xPp3gMnSc7
soupeřil	soupeřit	k5eAaImAgMnS
o	o	k7c4
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
Alonsovi	Alons	k1gMnSc3
podařilo	podařit	k5eAaPmAgNnS
a	a	k8xC
titul	titul	k1gInSc4
získal	získat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alonso	Alonsa	k1gFnSc5
se	se	k3xPyFc4
poté	poté	k6eAd1
chopil	chopit	k5eAaPmAgMnS
příležitosti	příležitost	k1gFnSc3
testovat	testovat	k5eAaImF
tým	tým	k1gInSc4
Minardi	Minard	k1gMnPc1
ve	v	k7c6
formuli	formule	k1gFnSc6
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
si	se	k3xPyFc3
ho	on	k3xPp3gNnSc4
všiml	všimnout	k5eAaPmAgMnS
sportovní	sportovní	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Cesare	Cesar	k1gMnSc5
Fiorio	Fioria	k1gMnSc5
<g/>
,	,	kIx,
když	když	k8xS
Fernando	Fernanda	k1gFnSc5
zajížděl	zajíždět	k5eAaImAgInS
o	o	k7c4
1,5	1,5	k4
sekundy	sekunda	k1gFnSc2
rychlejší	rychlý	k2eAgInPc1d2
časy	čas	k1gInPc1
než	než	k8xS
ostatní	ostatní	k2eAgMnPc1d1
jezdci	jezdec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
testovali	testovat	k5eAaImAgMnP
pro	pro	k7c4
Minardi	Minard	k1gMnPc5
<g/>
.	.	kIx.
</s>
<s>
Následující	následující	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
se	se	k3xPyFc4
Alonso	Alonsa	k1gFnSc5
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
Formule	formule	k1gFnSc2
3000	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
obvykle	obvykle	k6eAd1
byla	být	k5eAaImAgFnS
posledním	poslední	k2eAgInSc7d1
krokem	krok	k1gInSc7
před	před	k7c7
vstupem	vstup	k1gInSc7
do	do	k7c2
Formule	formule	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alonso	Alonsa	k1gFnSc5
se	se	k3xPyFc4
připojil	připojit	k5eAaPmAgInS
k	k	k7c3
týmu	tým	k1gInSc3
Astromega	Astromeg	k1gMnSc2
a	a	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
roce	rok	k1gInSc6
byl	být	k5eAaImAgMnS
také	také	k6eAd1
nejmladším	mladý	k2eAgMnSc7d3
pilotem	pilot	k1gMnSc7
(	(	kIx(
<g/>
o	o	k7c4
11	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
ve	v	k7c6
formuli	formule	k1gFnSc6
3000	#num#	k4
závodil	závodit	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
prvních	první	k4xOgInPc6
sedmi	sedm	k4xCc6
závodech	závod	k1gInPc6
nezískal	získat	k5eNaPmAgMnS
Alonso	Alonsa	k1gFnSc5
ani	ani	k8xC
bod	bod	k1gInSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
v	v	k7c6
posledních	poslední	k2eAgInPc6d1
dvou	dva	k4xCgInPc6
závodech	závod	k1gInPc6
skončil	skončit	k5eAaPmAgInS
jednou	jednou	k6eAd1
druhý	druhý	k4xOgMnSc1
a	a	k8xC
jednou	jeden	k4xCgFnSc7
vyhrál	vyhrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
skončil	skončit	k5eAaPmAgInS
čtvrtý	čtvrtý	k4xOgInSc1
za	za	k7c7
Brunem	Bruno	k1gMnSc7
Junqueirou	Junqueira	k1gMnSc7
<g/>
,	,	kIx,
Nicolasem	Nicolas	k1gMnSc7
Minassianem	Minassian	k1gMnSc7
a	a	k8xC
Markem	Marek	k1gMnSc7
Webberem	Webber	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
2001	#num#	k4
<g/>
:	:	kIx,
Minardi	Minard	k1gMnPc1
</s>
<s>
Alonso	Alonsa	k1gFnSc5
byl	být	k5eAaImAgMnS
třetím	třetí	k4xOgMnSc7
nejmladším	mladý	k2eAgMnSc7d3
pilotem	pilot	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
kdy	kdy	k6eAd1
ve	v	k7c6
formuli	formule	k1gFnSc6
1	#num#	k4
startoval	startovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
když	když	k8xS
si	se	k3xPyFc3
odbyl	odbýt	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
debut	debut	k1gInSc4
v	v	k7c6
Minardi	Minard	k1gMnPc1
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2001	#num#	k4
na	na	k7c6
australském	australský	k2eAgInSc6d1
okruhu	okruh	k1gInSc6
v	v	k7c6
Melbourne	Melbourne	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Minardi	Minard	k1gMnPc1
odstartovalo	odstartovat	k5eAaPmAgNnS
do	do	k7c2
nové	nový	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
s	s	k7c7
novým	nový	k2eAgMnSc7d1
vlastníkem	vlastník	k1gMnSc7
Paulem	Paul	k1gMnSc7
Stoddartem	Stoddart	k1gMnSc7
a	a	k8xC
také	také	k9
s	s	k7c7
novým	nový	k2eAgInSc7d1
vozem	vůz	k1gInSc7
Minardi	Minard	k1gMnPc1
PS	PS	kA
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alonso	Alonsa	k1gFnSc5
se	se	k3xPyFc4
ukázal	ukázat	k5eAaPmAgInS
jako	jako	k9
rychlý	rychlý	k2eAgInSc1d1
pilot	pilot	k1gInSc1
hned	hned	k6eAd1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
první	první	k4xOgFnSc6
kvalifikaci	kvalifikace	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
porazil	porazit	k5eAaPmAgMnS
týmového	týmový	k2eAgMnSc4d1
kolegu	kolega	k1gMnSc4
Tarso	Tarsa	k1gFnSc5
Marquese	Marques	k1gInSc6
o	o	k7c4
2,6	2,6	k4
sekundy	sekunda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
ve	v	k7c6
čtvrté	čtvrtý	k4xOgFnSc6
kvalifikaci	kvalifikace	k1gFnSc6
v	v	k7c6
Imole	Imola	k1gFnSc6
porazil	porazit	k5eAaPmAgMnS
oba	dva	k4xCgInPc4
Benettony	Benetton	k1gInPc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
zopakoval	zopakovat	k5eAaPmAgMnS
i	i	k8xC
v	v	k7c6
další	další	k2eAgFnSc6d1
části	část	k1gFnSc6
sezóny	sezóna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
své	svůj	k3xOyFgInPc4
výkony	výkon	k1gInPc4
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
si	se	k3xPyFc3
ho	on	k3xPp3gInSc2
začínaly	začínat	k5eAaImAgInP
všímat	všímat	k5eAaImF
i	i	k9
lepší	dobrý	k2eAgInPc4d2
týmy	tým	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
2001	#num#	k4
po	po	k7c6
závodech	závod	k1gInPc6
v	v	k7c6
Evropě	Evropa	k1gFnSc6
se	se	k3xPyFc4
Sauber	Sauber	k1gMnSc1
poohlížel	poohlížet	k5eAaImAgMnS
po	po	k7c6
jezdci	jezdec	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
by	by	kYmCp3nS
nahradil	nahradit	k5eAaPmAgInS
odcházejícího	odcházející	k2eAgMnSc4d1
Kimiho	Kimi	k1gMnSc4
Räikkönena	Räikkönen	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sauber	Sauber	k1gMnSc1
se	se	k3xPyFc4
ale	ale	k9
též	též	k9
zajímal	zajímat	k5eAaImAgMnS
o	o	k7c4
Felipeho	Felipe	k1gMnSc4
Massu	Mass	k1gInSc2
<g/>
,	,	kIx,
nadějného	nadějný	k2eAgMnSc4d1
Brazilce	Brazilec	k1gMnSc4
a	a	k8xC
též	též	k9
o	o	k7c4
testovacího	testovací	k2eAgMnSc4d1
jezdce	jezdec	k1gMnSc4
Jaguaru	Jaguar	k1gInSc2
Andre	Andr	k1gInSc5
Lotterera	Lotterero	k1gNnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
měsíc	měsíc	k1gInSc4
později	pozdě	k6eAd2
bylo	být	k5eAaImAgNnS
potvrzeno	potvrdit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
usedne	usednout	k5eAaPmIp3nS
do	do	k7c2
kokpitu	kokpit	k1gInSc2
Sauberu	Sauber	k1gInSc2
Felipe	Felip	k1gInSc5
Massa	Mass	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c4
září	září	k1gNnSc4
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
začal	začít	k5eAaPmAgMnS
Alonsův	Alonsův	k2eAgMnSc1d1
manažer	manažer	k1gMnSc1
Flavio	Flavio	k1gMnSc1
Briatore	Briator	k1gMnSc5
plánovat	plánovat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
Fernando	Fernanda	k1gFnSc5
mohl	moct	k5eAaImAgInS
jezdit	jezdit	k5eAaImF
za	za	k7c4
Benetton	Benetton	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Briatore	Briator	k1gMnSc5
zvažoval	zvažovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
mohl	moct	k5eAaImAgInS
Alonso	Alonsa	k1gFnSc5
nahradit	nahradit	k5eAaPmF
Jensona	Jenson	k1gMnSc2
Buttona	Button	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Alonso	Alonsa	k1gFnSc5
bude	být	k5eAaImBp3nS
oficiálním	oficiální	k2eAgMnSc7d1
testovacím	testovací	k2eAgMnSc7d1
jezdcem	jezdec	k1gMnSc7
Renaultu	renault	k1gInSc2
pro	pro	k7c4
rok	rok	k1gInSc4
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledním	poslední	k2eAgInSc6d1
závodě	závod	k1gInSc6
roku	rok	k1gInSc2
2001	#num#	k4
v	v	k7c4
Suzuce	Suzuce	k1gFnPc4
skončil	skončit	k5eAaPmAgMnS
Alonso	Alonsa	k1gFnSc5
jedenáctý	jedenáctý	k4xOgInSc4
<g/>
,	,	kIx,
tenkrát	tenkrát	k6eAd1
pět	pět	k4xCc1
míst	místo	k1gNnPc2
za	za	k7c7
bodovaným	bodovaný	k2eAgNnSc7d1
umístěním	umístění	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
porazil	porazit	k5eAaPmAgMnS
Heinze-Haralda	Heinze-Haralda	k1gFnSc1
Frentzena	Frentzen	k2eAgFnSc1d1
(	(	kIx(
<g/>
z	z	k7c2
týmu	tým	k1gInSc2
Prost	prost	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Oliviera	Oliviera	k1gFnSc1
Panise	Panise	k1gFnSc1
(	(	kIx(
<g/>
BAR	bar	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
oba	dva	k4xCgInPc1
jezdce	jezdec	k1gInPc1
týmu	tým	k1gInSc2
Arrows	Arrowsa	k1gFnPc2
a	a	k8xC
Alexe	Alex	k1gMnSc5
Yoonga	Yoong	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
nahradil	nahradit	k5eAaPmAgMnS
v	v	k7c6
Minardi	Minard	k1gMnPc1
Tarsa	Tarsa	k1gFnSc1
Marquese	Marquese	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
:	:	kIx,
Renault	renault	k1gInSc1
</s>
<s>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
</s>
<s>
Alonso	Alonsa	k1gFnSc5
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
stal	stát	k5eAaPmAgInS
testovacím	testovací	k2eAgInSc7d1
jezdcem	jezdec	k1gInSc7
týmu	tým	k1gInSc2
Renault	renault	k1gInSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Benetton	Benetton	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
najezdil	najezdit	k5eAaBmAgMnS,k5eAaPmAgMnS
za	za	k7c4
celý	celý	k2eAgInSc4d1
rok	rok	k1gInSc4
1642	#num#	k4
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
testoval	testovat	k5eAaImAgMnS
pro	pro	k7c4
tým	tým	k1gInSc4
Jaguar	Jaguara	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
Briatore	Briator	k1gMnSc5
propustil	propustit	k5eAaPmAgInS
Buttona	Button	k1gMnSc4
a	a	k8xC
Alonsa	Alons	k1gMnSc4
posadil	posadit	k5eAaPmAgMnS
do	do	k7c2
druhého	druhý	k4xOgInSc2
vozu	vůz	k1gInSc2
vedle	vedle	k7c2
jedničky	jednička	k1gFnSc2
týmu	tým	k1gInSc2
Jarna	Jarn	k1gMnSc2
Trulliho	Trulli	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Španěl	Španěl	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
nejmladším	mladý	k2eAgInSc7d3
jezdcem	jezdec	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
získal	získat	k5eAaPmAgInS
pole	pola	k1gFnSc3
position	position	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
okruhu	okruh	k1gInSc6
v	v	k7c6
Malajsii	Malajsie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závodě	závod	k1gInSc6
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
měl	mít	k5eAaImAgMnS
Alonso	Alonsa	k1gFnSc5
těžkou	těžký	k2eAgFnSc4d1
nehodu	nehoda	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
dvousetkilometrové	dvousetkilometrový	k2eAgFnSc6d1
rychlosti	rychlost	k1gFnSc6
havaroval	havarovat	k5eAaPmAgMnS
<g/>
,	,	kIx,
když	když	k8xS
vrazil	vrazit	k5eAaPmAgMnS
do	do	k7c2
zbytků	zbytek	k1gInPc2
vozu	vůz	k1gInSc2
Marka	Marek	k1gMnSc2
Webbera	Webber	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
havaroval	havarovat	k5eAaPmAgMnS
těsně	těsně	k6eAd1
před	před	k7c7
ním	on	k3xPp3gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závod	závod	k1gInSc1
byl	být	k5eAaImAgInS
poté	poté	k6eAd1
zastaven	zastavit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
dva	dva	k4xCgInPc4
závody	závod	k1gInPc4
později	pozdě	k6eAd2
skončil	skončit	k5eAaPmAgInS
Alonso	Alonsa	k1gFnSc5
při	při	k7c6
své	svůj	k3xOyFgInPc4
domácí	domácí	k2eAgMnSc1d1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Španělska	Španělsko	k1gNnSc2
druhý	druhý	k4xOgInSc4
a	a	k8xC
poté	poté	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
nejmladším	mladý	k2eAgMnSc7d3
jezdcem	jezdec	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
vyhrál	vyhrát	k5eAaPmAgMnS
závod	závod	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
zvítězil	zvítězit	k5eAaPmAgMnS
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Ceně	cena	k1gFnSc6
Maďarska	Maďarsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
skončil	skončit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
na	na	k7c6
šestém	šestý	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
získal	získat	k5eAaPmAgMnS
55	#num#	k4
bodů	bod	k1gInPc2
a	a	k8xC
čtyřikrát	čtyřikrát	k6eAd1
stanul	stanout	k5eAaPmAgMnS
na	na	k7c6
stupních	stupeň	k1gInPc6
vítězů	vítěz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
ve	v	k7c6
voze	vůz	k1gInSc6
Renault	renault	k1gInSc1
F1	F1	k1gMnSc1
Team	team	k1gInSc1
při	při	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
USA	USA	kA
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
</s>
<s>
Alonso	Alonsa	k1gFnSc5
zůstal	zůstat	k5eAaPmAgInS
u	u	k7c2
Renaultu	renault	k1gInSc2
i	i	k9
pro	pro	k7c4
rok	rok	k1gInSc4
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
začátku	začátek	k1gInSc6
sezóny	sezóna	k1gFnSc2
byl	být	k5eAaImAgInS
Alonso	Alonsa	k1gFnSc5
trochu	trochu	k6eAd1
zpochybňován	zpochybňovat	k5eAaImNgMnS
<g/>
,	,	kIx,
když	když	k8xS
byl	být	k5eAaImAgInS
v	v	k7c6
kvalifikacích	kvalifikace	k1gFnPc6
většinou	většinou	k6eAd1
porážen	porážet	k5eAaImNgMnS
stájovým	stájový	k2eAgMnSc7d1
kolegou	kolega	k1gMnSc7
Jarnem	Jarn	k1gMnSc7
Trullim	Trullima	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alonso	Alonsa	k1gFnSc5
předvedl	předvést	k5eAaPmAgMnS
slušný	slušný	k2eAgInSc4d1
výkon	výkon	k1gInSc4
ve	v	k7c6
velké	velký	k2eAgFnSc6d1
ceně	cena	k1gFnSc6
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
získal	získat	k5eAaPmAgInS
nejdříve	dříve	k6eAd3
pole	pole	k1gNnSc4
position	position	k1gInSc1
a	a	k8xC
o	o	k7c6
vítězství	vítězství	k1gNnSc6
ho	on	k3xPp3gInSc4
nakonec	nakonec	k6eAd1
připravila	připravit	k5eAaPmAgFnS
jen	jen	k9
fantastická	fantastický	k2eAgFnSc1d1
taktika	taktika	k1gFnSc1
Ferrari	Ferrari	k1gMnSc2
a	a	k8xC
Michaela	Michael	k1gMnSc4
Schumachera	Schumacher	k1gMnSc4
se	s	k7c7
čtyřmi	čtyři	k4xCgFnPc7
zastávkami	zastávka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
Trulliho	Trulliha	k1gFnSc5
výsledky	výsledek	k1gInPc1
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgInP
zhoršovat	zhoršovat	k5eAaImF
a	a	k8xC
tak	tak	k6eAd1
ho	on	k3xPp3gInSc4
Renault	renault	k1gInSc4
propustil	propustit	k5eAaPmAgInS
a	a	k8xC
místo	místo	k7c2
něj	on	k3xPp3gMnSc2
posadil	posadit	k5eAaPmAgMnS
do	do	k7c2
závodní	závodní	k2eAgFnSc2d1
sedačky	sedačka	k1gFnSc2
šampióna	šampión	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
<g/>
,	,	kIx,
Jacquese	Jacques	k1gMnSc2
Villeneuva	Villeneuv	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alonso	Alonsa	k1gFnSc5
nakonec	nakonec	k6eAd1
v	v	k7c6
šampionátu	šampionát	k1gInSc6
skončil	skončit	k5eAaPmAgMnS
čtvrtý	čtvrtý	k4xOgMnSc1
s	s	k7c7
59	#num#	k4
body	bod	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
2005	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
se	se	k3xPyFc4
k	k	k7c3
týmu	tým	k1gInSc3
Renault	renault	k1gInSc1
připojil	připojit	k5eAaPmAgMnS
Ital	Ital	k1gMnSc1
Giancarlo	Giancarlo	k1gNnSc4
Fisichella	Fisichell	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alonso	Alonsa	k1gFnSc5
si	se	k3xPyFc3
vybojoval	vybojovat	k5eAaPmAgMnS
cestu	cesta	k1gFnSc4
k	k	k7c3
vrcholu	vrchol	k1gInSc3
v	v	k7c6
prvním	první	k4xOgInSc6
závodě	závod	k1gInSc6
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
skončil	skončit	k5eAaPmAgMnS
třetí	třetí	k4xOgMnSc1
po	po	k7c6
startu	start	k1gInSc6
ze	z	k7c2
zadních	zadní	k2eAgFnPc2d1
pozic	pozice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
druhém	druhý	k4xOgInSc6
závodě	závod	k1gInSc6
v	v	k7c6
Malajsii	Malajsie	k1gFnSc6
si	se	k3xPyFc3
vyjel	vyjet	k5eAaPmAgMnS
pole	pole	k1gNnSc4
position	position	k1gInSc1
a	a	k8xC
závod	závod	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
tak	tak	k6eAd1
dopadl	dopadnout	k5eAaPmAgInS
i	i	k9
v	v	k7c6
závodě	závod	k1gInSc6
v	v	k7c6
Bahrajnu	Bahrajn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
vyhrál	vyhrát	k5eAaPmAgMnS
závod	závod	k1gInSc4
v	v	k7c6
San	San	k1gFnSc6
Marinu	Marina	k1gFnSc4
po	po	k7c6
13	#num#	k4
kolové	kolový	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
se	s	k7c7
sedminásobným	sedminásobný	k2eAgMnSc7d1
šampiónem	šampión	k1gMnSc7
Michaelem	Michael	k1gMnSc7
Schumacherem	Schumacher	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Španělské	španělský	k2eAgFnSc6d1
Velké	velký	k2eAgFnSc6d1
Ceně	cena	k1gFnSc6
skončil	skončit	k5eAaPmAgMnS
Fernando	Fernanda	k1gFnSc5
druhý	druhý	k4xOgInSc1
za	za	k7c4
Kimim	Kimim	k1gInSc4
Räikkönenem	Räikkönen	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Zlepšující	zlepšující	k2eAgFnSc1d1
se	se	k3xPyFc4
McLaren	McLarno	k1gNnPc2
s	s	k7c7
Räikkönenem	Räikkönen	k1gInSc7
dokázal	dokázat	k5eAaPmAgInS
vyhrát	vyhrát	k5eAaPmF
i	i	k9
v	v	k7c6
Monaku	Monako	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
skončil	skončit	k5eAaPmAgInS
Alonso	Alonsa	k1gFnSc5
až	až	k9
čtvrtý	čtvrtý	k4xOgMnSc1
po	po	k7c6
problémech	problém	k1gInPc6
s	s	k7c7
pneumatikami	pneumatika	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
z	z	k7c2
nejzajímavějších	zajímavý	k2eAgInPc2d3
momentů	moment	k1gInPc2
sezóny	sezóna	k1gFnSc2
se	se	k3xPyFc4
odehrál	odehrát	k5eAaPmAgInS
při	při	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
posledním	poslední	k2eAgNnSc6d1
kole	kolo	k1gNnSc6
odstoupil	odstoupit	k5eAaPmAgInS
Räikkönen	Räikkönen	k1gInSc4
po	po	k7c6
problémech	problém	k1gInPc6
s	s	k7c7
pneumatikou	pneumatika	k1gFnSc7
a	a	k8xC
Alonsovi	Alonsův	k2eAgMnPc1d1
spadlo	spadnout	k5eAaPmAgNnS
vítězství	vítězství	k1gNnSc4
vyloženě	vyloženě	k6eAd1
do	do	k7c2
klína	klín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Alonso	Alonsa	k1gFnSc5
při	při	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
USA	USA	kA
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Kanadské	kanadský	k2eAgFnSc6d1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
udělal	udělat	k5eAaPmAgMnS
Alonso	Alonsa	k1gFnSc5
chybu	chyba	k1gFnSc4
a	a	k8xC
ve	v	k7c6
Villeneuvově	Villeneuvův	k2eAgFnSc6d1
zatáčce	zatáčka	k1gFnSc6
skončil	skončit	k5eAaPmAgMnS
ve	v	k7c6
zdi	zeď	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trochu	trochu	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
tom	ten	k3xDgNnSc6
podepsal	podepsat	k5eAaPmAgInS
i	i	k9
tlak	tlak	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
na	na	k7c4
něj	on	k3xPp3gMnSc4
vyvíjely	vyvíjet	k5eAaImAgInP
v	v	k7c6
závodě	závod	k1gInSc6
oba	dva	k4xCgInPc4
McLareny	McLaren	k1gInPc4
Juana	Juan	k1gMnSc4
Pabla	Pabl	k1gMnSc2
Montoyi	Montoyi	k1gNnSc2
a	a	k8xC
Kimiho	Kimi	k1gMnSc2
Räikkönena	Räikkönen	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Ceně	cena	k1gFnSc6
USA	USA	kA
po	po	k7c6
doporučení	doporučení	k1gNnSc6
Michelinu	Michelina	k1gFnSc4
Alonso	Alonsa	k1gFnSc5
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
všichni	všechen	k3xTgMnPc1
jezdci	jezdec	k1gMnPc1
na	na	k7c6
těchto	tento	k3xDgFnPc6
pneumatikách	pneumatika	k1gFnPc6
nestartoval	startovat	k5eNaBmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Třetí	třetí	k4xOgFnSc1
pole	pole	k1gFnSc1
position	position	k1gInSc1
a	a	k8xC
páté	pátý	k4xOgNnSc4
vítězství	vítězství	k1gNnSc4
si	se	k3xPyFc3
Alonso	Alonsa	k1gFnSc5
připsal	připsat	k5eAaPmAgMnS
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Ceně	cena	k1gFnSc6
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejrychlejší	rychlý	k2eAgFnSc1d3
v	v	k7c4
kvalifikaci	kvalifikace	k1gFnSc4
byl	být	k5eAaImAgInS
i	i	k9
o	o	k7c4
týden	týden	k1gInSc4
později	pozdě	k6eAd2
v	v	k7c6
Britské	britský	k2eAgFnSc6d1
Velké	velký	k2eAgFnSc6d1
Ceně	cena	k1gFnSc6
<g/>
,	,	kIx,
dojel	dojet	k5eAaPmAgMnS
však	však	k9
druhý	druhý	k4xOgMnSc1
za	za	k7c4
Montoyou	Montoyý	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Špatná	špatný	k2eAgFnSc1d1
spolehlivost	spolehlivost	k1gFnSc1
McLarenů	McLaren	k1gMnPc2
garantovala	garantovat	k5eAaBmAgFnS
Alonsovi	Alons	k1gMnSc3
vítězství	vítězství	k1gNnSc4
v	v	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Räikkönen	Räikkönen	k1gInSc1
odstoupil	odstoupit	k5eAaPmAgInS
po	po	k7c6
potížích	potíž	k1gFnPc6
s	s	k7c7
hydraulikou	hydraulika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc4
dny	den	k1gInPc4
před	před	k7c7
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Maďarska	Maďarsko	k1gNnSc2
oslavil	oslavit	k5eAaPmAgMnS
Fernando	Fernanda	k1gFnSc5
své	své	k1gNnSc4
24	#num#	k4
<g/>
.	.	kIx.
narozeniny	narozeniny	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závod	závod	k1gInSc4
ale	ale	k8xC
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
moc	moc	k6eAd1
dobrý	dobrý	k2eAgMnSc1d1
nebyl	být	k5eNaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvalifikoval	kvalifikovat	k5eAaBmAgMnS
se	se	k3xPyFc4
jako	jako	k9
šestý	šestý	k4xOgMnSc1
a	a	k8xC
v	v	k7c6
závodě	závod	k1gInSc6
skončil	skončit	k5eAaPmAgMnS
jedenáctý	jedenáctý	k4xOgMnSc1
po	po	k7c6
kolizi	kolize	k1gFnSc6
s	s	k7c7
Ralfem	Ralf	k1gMnSc7
Schumacherem	Schumacher	k1gMnSc7
v	v	k7c6
první	první	k4xOgFnSc6
zatáčce	zatáčka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	V	kA
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Turecka	Turecko	k1gNnSc2
bral	brát	k5eAaImAgMnS
Alonso	Alonsa	k1gFnSc5
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
závěru	závěr	k1gInSc6
závodu	závod	k1gInSc2
vylétl	vylétnout	k5eAaPmAgMnS
Montoya	Montoya	k1gMnSc1
mimo	mimo	k7c4
trať	trať	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
italské	italský	k2eAgFnSc6d1
Monze	Monza	k1gFnSc6
byl	být	k5eAaImAgInS
v	v	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
Alonso	Alonsa	k1gFnSc5
třetí	třetí	k4xOgFnSc4
<g/>
,	,	kIx,
v	v	k7c6
závodě	závod	k1gInSc6
druhý	druhý	k4xOgInSc1
za	za	k7c4
Montoyou	Montoyý	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Belgické	belgický	k2eAgFnSc6d1
velké	velký	k2eAgFnSc6d1
ceně	cena	k1gFnSc6
dojel	dojet	k5eAaPmAgMnS
Fernando	Fernanda	k1gFnSc5
taktéž	taktéž	k?
druhý	druhý	k4xOgMnSc1
<g/>
,	,	kIx,
když	když	k8xS
postoupil	postoupit	k5eAaPmAgMnS
vzhůru	vzhůru	k6eAd1
po	po	k7c4
kolizi	kolize	k1gFnSc4
Antonia	Antonio	k1gMnSc4
Pizzonii	Pizzonie	k1gFnSc4
a	a	k8xC
Juana	Juan	k1gMnSc4
Pabla	Pablo	k1gNnSc2
Montoyi	Montoy	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
vybojoval	vybojovat	k5eAaPmAgMnS
pole	pole	k1gNnSc4
position	position	k1gInSc1
i	i	k9
ve	v	k7c6
velké	velký	k2eAgFnSc6d1
ceně	cena	k1gFnSc6
Brazílie	Brazílie	k1gFnSc2
<g/>
,	,	kIx,
skončil	skončit	k5eAaPmAgInS
třetí	třetí	k4xOgMnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mu	on	k3xPp3gMnSc3
ale	ale	k9
stačilo	stačit	k5eAaBmAgNnS
k	k	k7c3
zisku	zisk	k1gInSc3
titulu	titul	k1gInSc2
mistra	mistr	k1gMnSc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
mistrem	mistr	k1gMnSc7
světa	svět	k1gInSc2
v	v	k7c6
24	#num#	k4
letech	let	k1gInPc6
a	a	k8xC
58	#num#	k4
dnech	den	k1gInPc6
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
překonal	překonat	k5eAaPmAgMnS
rekord	rekord	k1gInSc4
Emersona	Emerson	k1gMnSc2
Fittipaldiho	Fittipaldi	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taktéž	Taktéž	k?
ukončil	ukončit	k5eAaPmAgMnS
pětiletou	pětiletý	k2eAgFnSc4d1
dominanci	dominance	k1gFnSc4
Michaela	Michael	k1gMnSc2
Schumachera	Schumacher	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alonso	Alonsa	k1gFnSc5
vedl	vést	k5eAaImAgInS
šampionát	šampionát	k1gInSc1
od	od	k7c2
druhého	druhý	k4xOgInSc2
závodu	závod	k1gInSc2
a	a	k8xC
poté	poté	k6eAd1
už	už	k6eAd1
před	před	k7c4
sebe	sebe	k3xPyFc4
nikoho	nikdo	k3yNnSc4
nepustil	pustit	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zisku	zisk	k1gInSc6
titulu	titul	k1gInSc2
uvedl	uvést	k5eAaPmAgInS
<g/>
:	:	kIx,
„	„	k?
<g/>
Chtěl	chtít	k5eAaImAgMnS
bych	by	kYmCp1nS
tento	tento	k3xDgInSc4
titul	titul	k1gInSc4
věnovat	věnovat	k5eAaImF,k5eAaPmF
mé	můj	k3xOp1gFnSc3
rodině	rodina	k1gFnSc3
<g/>
,	,	kIx,
a	a	k8xC
všem	všecek	k3xTgMnPc3
blízkým	blízký	k2eAgMnPc3d1
přátelům	přítel	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
mě	já	k3xPp1nSc4
podporovali	podporovat	k5eAaImAgMnP
v	v	k7c6
mé	můj	k3xOp1gFnSc6
kariéře	kariéra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španělsko	Španělsko	k1gNnSc1
není	být	k5eNaImIp3nS
země	zem	k1gFnPc4
s	s	k7c7
kulturou	kultura	k1gFnSc7
F	F	kA
<g/>
1	#num#	k4
<g/>
,	,	kIx,
proto	proto	k8xC
jsme	být	k5eAaImIp1nP
museli	muset	k5eAaImAgMnP
bojovat	bojovat	k5eAaImF
sami	sám	k3xTgMnPc1
za	za	k7c4
každý	každý	k3xTgInSc4
krok	krok	k1gInSc4
k	k	k7c3
zisku	zisk	k1gInSc3
tohoto	tento	k3xDgInSc2
titulu	titul	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velká	k1gFnSc2
poděkování	poděkování	k1gNnSc2
patří	patřit	k5eAaImIp3nS
samozřejmě	samozřejmě	k6eAd1
také	také	k9
týmu	tým	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
nejlepší	dobrý	k2eAgMnSc1d3
ve	v	k7c6
Formuli	formule	k1gFnSc6
1	#num#	k4
a	a	k8xC
se	s	k7c7
kterým	který	k3yIgInSc7,k3yRgInSc7,k3yQgInSc7
jsme	být	k5eAaImIp1nP
společně	společně	k6eAd1
všechno	všechen	k3xTgNnSc4
zvládli	zvládnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bude	být	k5eAaImBp3nS
se	se	k3xPyFc4
říkat	říkat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
já	já	k3xPp1nSc1
jsem	být	k5eAaImIp1nS
šampiónem	šampión	k1gMnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
my	my	k3xPp1nPc1
všichni	všechen	k3xTgMnPc1
jsme	být	k5eAaImIp1nP
šampióni	šampión	k1gMnPc1
a	a	k8xC
všichni	všechen	k3xTgMnPc1
si	se	k3xPyFc3
to	ten	k3xDgNnSc1
zaslouží	zasloužit	k5eAaPmIp3nS
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
2007	#num#	k4
uvedl	uvést	k5eAaPmAgMnS
Alonso	Alonsa	k1gFnSc5
pro	pro	k7c4
časopis	časopis	k1gInSc4
F1	F1	k1gFnSc3
Racing	Racing	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
velká	velký	k2eAgFnSc1d1
cena	cena	k1gFnSc1
Brazílie	Brazílie	k1gFnSc2
2005	#num#	k4
byla	být	k5eAaImAgFnS
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
největším	veliký	k2eAgInSc7d3
závodem	závod	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekl	říct	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
sen	sen	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
skutečností	skutečnost	k1gFnSc7
a	a	k8xC
také	také	k9
velmi	velmi	k6eAd1
emocionální	emocionální	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
kolech	kolo	k1gNnPc6
mi	já	k3xPp1nSc3
už	už	k6eAd1
asi	asi	k9
vážně	vážně	k6eAd1
přeskočilo	přeskočit	k5eAaPmAgNnS
<g/>
,	,	kIx,
slyšel	slyšet	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
hluk	hluk	k1gInSc4
z	z	k7c2
motoru	motor	k1gInSc2
snad	snad	k9
všude	všude	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
všechno	všechen	k3xTgNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
pořádku	pořádek	k1gInSc6
a	a	k8xC
já	já	k3xPp1nSc1
si	se	k3xPyFc3
pamatuji	pamatovat	k5eAaImIp1nS
tu	ten	k3xDgFnSc4
úlevu	úleva	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
jsem	být	k5eAaImIp1nS
cítil	cítit	k5eAaImAgMnS
po	po	k7c4
projetí	projetí	k1gNnSc4
cílovou	cílový	k2eAgFnSc7d1
čárou	čára	k1gFnSc7
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Japonské	japonský	k2eAgFnSc6d1
a	a	k8xC
Čínské	čínský	k2eAgFnSc6d1
Velké	velký	k2eAgFnSc6d1
Ceně	cena	k1gFnSc6
už	už	k6eAd1
nebyl	být	k5eNaImAgInS
znát	znát	k5eAaImF
ten	ten	k3xDgInSc1
opatrný	opatrný	k2eAgInSc1d1
styl	styl	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
evidentní	evidentní	k2eAgMnSc1d1
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Alonso	Alonsa	k1gFnSc5
ještě	ještě	k9
bojoval	bojovat	k5eAaImAgMnS
o	o	k7c4
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
startoval	startovat	k5eAaBmAgInS
Alonso	Alonsa	k1gFnSc5
šestnáctý	šestnáctý	k4xOgMnSc1
<g/>
,	,	kIx,
skončil	skončit	k5eAaPmAgInS
v	v	k7c6
závodě	závod	k1gInSc6
třetí	třetí	k4xOgFnPc1
za	za	k7c7
vítězem	vítěz	k1gMnSc7
závodu	závod	k1gInSc2
Kimim	Kimi	k1gNnSc7
Räikkönenem	Räikkönen	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
startoval	startovat	k5eAaBmAgInS
dokonce	dokonce	k9
až	až	k9
sedmnáctý	sedmnáctý	k4xOgMnSc1
a	a	k8xC
zvítězil	zvítězit	k5eAaPmAgMnS
po	po	k7c6
předjetí	předjetí	k1gNnSc6
Giancarla	Giancarl	k1gMnSc2
Fisichelly	Fisichella	k1gMnSc2
v	v	k7c6
posledním	poslední	k2eAgNnSc6d1
kole	kolo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Čínské	čínský	k2eAgFnSc6d1
velké	velký	k2eAgFnSc6d1
ceně	cena	k1gFnSc6
si	se	k3xPyFc3
Renault	renault	k1gInSc1
pojistil	pojistit	k5eAaPmAgInS
vítězství	vítězství	k1gNnSc4
v	v	k7c6
poháru	pohár	k1gInSc6
konstruktérů	konstruktér	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Většina	většina	k1gFnSc1
fanoušků	fanoušek	k1gMnPc2
F1	F1	k1gFnPc2
prohlašovala	prohlašovat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Kimi	Kime	k1gFnSc4
Raikkonen	Raikkonen	k1gInSc1
byl	být	k5eAaImAgInS
nejlepším	dobrý	k2eAgInSc7d3
jezdce	jezdec	k1gInSc2
roku	rok	k1gInSc2
2005	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
vyhrál	vyhrát	k5eAaPmAgMnS
stejný	stejný	k2eAgInSc4d1
počet	počet	k1gInSc4
závodů	závod	k1gInPc2
jako	jako	k8xC,k8xS
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
<g/>
,	,	kIx,
neboť	neboť	k8xC
McLaren	McLarna	k1gFnPc2
s	s	k7c7
Räikkönenem	Räikköneno	k1gNnSc7
se	se	k3xPyFc4
potýkal	potýkat	k5eAaImAgMnS
s	s	k7c7
problémy	problém	k1gInPc7
se	se	k3xPyFc4
spolehlivostí	spolehlivost	k1gFnSc7
vozu	vůz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
2005	#num#	k4
získal	získat	k5eAaPmAgInS
též	též	k9
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
cenu	cena	k1gFnSc4
Prince	princ	k1gMnSc2
z	z	k7c2
Asturie	Asturie	k1gFnSc2
za	za	k7c4
sportovní	sportovní	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
</s>
<s>
Alonsova	Alonsův	k2eAgFnSc1d1
další	další	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
v	v	k7c6
Renaultu	renault	k1gInSc6
odstartovala	odstartovat	k5eAaPmAgFnS
těsným	těsný	k2eAgNnSc7d1
vítězstvím	vítězství	k1gNnSc7
před	před	k7c7
Michaelem	Michael	k1gMnSc7
Schumacherem	Schumacher	k1gMnSc7
v	v	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Bahrajnu	Bahrajn	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Alonso	Alonsa	k1gFnSc5
Schumachera	Schumacher	k1gMnSc2
předjel	předjet	k5eAaPmAgInS
osmnáct	osmnáct	k4xCc4
kol	kolo	k1gNnPc2
před	před	k7c7
cílem	cíl	k1gInSc7
po	po	k7c6
zastávce	zastávka	k1gFnSc6
v	v	k7c6
boxech	box	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
byl	být	k5eAaImAgMnS
čtvrtý	čtvrtý	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
další	další	k2eAgFnSc6d1
kvalifikaci	kvalifikace	k1gFnSc6
v	v	k7c6
Malajsii	Malajsie	k1gFnSc6
měl	mít	k5eAaImAgMnS
Alonso	Alonsa	k1gFnSc5
problémy	problém	k1gInPc4
s	s	k7c7
tankováním	tankování	k1gNnSc7
a	a	k8xC
startoval	startovat	k5eAaBmAgMnS
ze	z	k7c2
sedmého	sedmý	k4xOgNnSc2
místa	místo	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
nakonec	nakonec	k6eAd1
skončil	skončit	k5eAaPmAgInS
druhý	druhý	k4xOgInSc1
za	za	k7c7
svým	svůj	k3xOyFgMnSc7
týmovým	týmový	k2eAgMnSc7d1
kolegou	kolega	k1gMnSc7
Fisichellou	Fisichella	k1gMnSc7
po	po	k7c6
velmi	velmi	k6eAd1
dobrém	dobrý	k2eAgInSc6d1
startu	start	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
předjel	předjet	k5eAaPmAgInS
hned	hned	k6eAd1
4	#num#	k4
soupeře	soupeř	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Australské	australský	k2eAgFnSc6d1
Velké	velký	k2eAgFnSc6d1
ceně	cena	k1gFnSc6
Alonso	Alonsa	k1gFnSc5
vyhrál	vyhrát	k5eAaPmAgMnS
po	po	k7c6
předjetí	předjetí	k1gNnSc6
Jensona	Jenson	k1gMnSc2
Buttona	Button	k1gMnSc2
na	na	k7c6
Hondě	honda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
nevydařené	vydařený	k2eNgFnSc6d1
kvalifikaci	kvalifikace	k1gFnSc6
v	v	k7c6
San	San	k1gFnSc6
Marinu	Marina	k1gFnSc4
<g/>
,	,	kIx,
nebyl	být	k5eNaImAgMnS
Alonso	Alonsa	k1gFnSc5
schopen	schopen	k2eAgMnSc1d1
předjet	předjet	k5eAaPmF
Michaela	Michael	k1gMnSc2
Schumachera	Schumacher	k1gMnSc2
v	v	k7c6
závodě	závod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podstatě	podstata	k1gFnSc6
se	se	k3xPyFc4
opakovalo	opakovat	k5eAaImAgNnS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2005	#num#	k4
<g/>
,	,	kIx,
akorát	akorát	k6eAd1
že	že	k8xS
Němec	Němec	k1gMnSc1
držel	držet	k5eAaImAgMnS
Alonsa	Alons	k1gMnSc4
za	za	k7c7
sebou	se	k3xPyFc7
dvakrát	dvakrát	k6eAd1
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
než	než	k8xS
Fernando	Fernanda	k1gFnSc5
držel	držet	k5eAaImAgInS
Michaela	Michael	k1gMnSc4
v	v	k7c6
loňském	loňský	k2eAgInSc6d1
závodě	závod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schumacher	Schumachra	k1gFnPc2
se	se	k3xPyFc4
kvalifikoval	kvalifikovat	k5eAaBmAgMnS
až	až	k9
čtrnáctý	čtrnáctý	k4xOgMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
téměř	téměř	k6eAd1
ihned	ihned	k6eAd1
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
na	na	k7c6
špici	špice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	V	kA
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Evropy	Evropa	k1gFnSc2
si	se	k3xPyFc3
Alonso	Alonsa	k1gFnSc5
vyjel	vyjet	k5eAaPmAgMnS
pole	pole	k1gNnSc4
position	position	k1gInSc1
před	před	k7c7
Michaelem	Michael	k1gMnSc7
Schumacherem	Schumacher	k1gMnSc7
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
ho	on	k3xPp3gInSc4
ale	ale	k9
po	po	k7c6
zastávce	zastávka	k1gFnSc6
v	v	k7c6
boxech	box	k1gInPc6
předjel	předjet	k5eAaPmAgMnS
a	a	k8xC
Fernando	Fernanda	k1gFnSc5
se	se	k3xPyFc4
musel	muset	k5eAaImAgMnS
spokojit	spokojit	k5eAaPmF
se	s	k7c7
druhým	druhý	k4xOgNnSc7
místem	místo	k1gNnSc7
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2006	#num#	k4
vyhrál	vyhrát	k5eAaPmAgMnS
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Španělska	Španělsko	k1gNnSc2
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	s	k7c7
prvním	první	k4xOgMnSc7
Španělem	Španěl	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
dokázal	dokázat	k5eAaPmAgMnS
vyhrát	vyhrát	k5eAaPmF
na	na	k7c6
domácí	domácí	k2eAgFnSc6d1
trati	trať	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Jubilejní	jubilejní	k2eAgInSc1d1
12	#num#	k4
<g/>
.	.	kIx.
vítězství	vítězství	k1gNnSc6
a	a	k8xC
12	#num#	k4
<g/>
.	.	kIx.
pole	pole	k1gFnSc1
position	position	k1gInSc1
si	se	k3xPyFc3
Alonso	Alonsa	k1gFnSc5
zajistil	zajistit	k5eAaPmAgMnS
v	v	k7c6
Monaku	Monako	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
získal	získat	k5eAaPmAgMnS
první	první	k4xOgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
až	až	k9
po	po	k7c6
penalizování	penalizování	k1gNnSc6
Michaela	Michael	k1gMnSc2
Schumachera	Schumacher	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
odstavil	odstavit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
vůz	vůz	k1gInSc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
nikdo	nikdo	k3yNnSc1
nebyl	být	k5eNaImAgMnS
schopen	schopen	k2eAgInSc4d1
zajet	zajet	k2eAgInSc4d1
pořádný	pořádný	k2eAgInSc4d1
čas	čas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	V	kA
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Kanady	Kanada	k1gFnSc2
2006	#num#	k4
si	se	k3xPyFc3
Alonso	Alonsa	k1gFnSc5
vybojoval	vybojovat	k5eAaPmAgMnS
šesté	šestý	k4xOgNnSc4
vítězství	vítězství	k1gNnSc4
v	v	k7c6
sezóně	sezóna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Alonso	Alonsa	k1gFnSc5
dominoval	dominovat	k5eAaImAgInS
i	i	k9
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
ceně	cena	k1gFnSc6
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
opět	opět	k6eAd1
vyhrál	vyhrát	k5eAaPmAgMnS
kvalifikaci	kvalifikace	k1gFnSc4
i	i	k8xC
závod	závod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
poprvé	poprvé	k6eAd1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Alonso	Alonsa	k1gFnSc5
získal	získat	k5eAaPmAgMnS
tzv.	tzv.	kA
„	„	k?
<g/>
triple	tripl	k1gInSc5
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
vyhrál	vyhrát	k5eAaPmAgMnS
kvalifikaci	kvalifikace	k1gFnSc4
<g/>
,	,	kIx,
závod	závod	k1gInSc4
a	a	k8xC
zajel	zajet	k5eAaPmAgMnS
nejrychlejší	rychlý	k2eAgNnSc4d3
kolo	kolo	k1gNnSc4
v	v	k7c6
závodě	závod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Kanadě	Kanada	k1gFnSc6
si	se	k3xPyFc3
vyjel	vyjet	k5eAaPmAgMnS
páté	pátý	k4xOgNnSc4
pole	pole	k1gNnSc4
position	position	k1gInSc1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
a	a	k8xC
závod	závod	k1gInSc1
také	také	k9
vyhrál	vyhrát	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
V	V	kA
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
USA	USA	kA
skončil	skončit	k5eAaPmAgInS
v	v	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
pátý	pátý	k4xOgMnSc1
a	a	k8xC
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
pozici	pozice	k1gFnSc6
dojel	dojet	k5eAaPmAgMnS
i	i	k9
v	v	k7c6
závodě	závod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schumacher	Schumachra	k1gFnPc2
svým	svůj	k3xOyFgNnSc7
vítězstvím	vítězství	k1gNnSc7
stáhl	stáhnout	k5eAaPmAgInS
Alonsův	Alonsův	k2eAgInSc1d1
náskok	náskok	k1gInSc1
z	z	k7c2
25	#num#	k4
bodů	bod	k1gInPc2
na	na	k7c4
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schumacher	Schumachra	k1gFnPc2
vyhrál	vyhrát	k5eAaPmAgMnS
i	i	k9
velkou	velký	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
Alonso	Alonsa	k1gFnSc5
byl	být	k5eAaImAgInS
druhý	druhý	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
ceně	cena	k1gFnSc6
Německa	Německo	k1gNnSc2
skončil	skončit	k5eAaPmAgMnS
Fernando	Fernanda	k1gFnSc5
až	až	k9
pátý	pátý	k4xOgInSc4
<g/>
,	,	kIx,
oba	dva	k4xCgInPc4
Renaulty	renault	k1gInPc4
v	v	k7c6
tomto	tento	k3xDgInSc6
závodě	závod	k1gInSc6
nebyly	být	k5eNaImAgFnP
příliš	příliš	k6eAd1
konkurenceschopné	konkurenceschopný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
dvousekundové	dvousekundový	k2eAgFnSc6d1
penalizaci	penalizace	k1gFnSc6
v	v	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Maďarska	Maďarsko	k1gNnSc2
se	se	k3xPyFc4
Alonso	Alonsa	k1gFnSc5
kvalifikoval	kvalifikovat	k5eAaBmAgMnS
až	až	k9
jako	jako	k9
patnáctý	patnáctý	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
rival	rival	k1gMnSc1
ale	ale	k8xC
dostal	dostat	k5eAaPmAgMnS
stejný	stejný	k2eAgInSc4d1
trest	trest	k1gInSc4
a	a	k8xC
startoval	startovat	k5eAaBmAgInS
jedenáctý	jedenáctý	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fernando	Fernanda	k1gFnSc5
se	se	k3xPyFc4
ihned	ihned	k6eAd1
dral	drát	k5eAaImAgMnS
ke	k	k7c3
špici	špice	k1gFnSc3
závodu	závod	k1gInSc2
a	a	k8xC
předjížděl	předjíždět	k5eAaImAgMnS
jednoho	jeden	k4xCgMnSc4
jezdce	jezdec	k1gMnSc2
za	za	k7c7
druhým	druhý	k4xOgMnSc7
včetně	včetně	k7c2
Schumachera	Schumachero	k1gNnSc2
a	a	k8xC
dokázal	dokázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
umí	umět	k5eAaImIp3nS
jezdit	jezdit	k5eAaImF
i	i	k9
na	na	k7c6
mokru	mokro	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
závod	závod	k1gInSc4
nedokončil	dokončit	k5eNaPmAgMnS
kvůli	kvůli	k7c3
mechanickým	mechanický	k2eAgFnPc3d1
potížím	potíž	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schumacher	Schumachra	k1gFnPc2
skončil	skončit	k5eAaPmAgInS
osmý	osmý	k4xOgInSc1
<g/>
,	,	kIx,
po	po	k7c6
diskvalifikaci	diskvalifikace	k1gFnSc6
Roberta	Robert	k1gMnSc2
Kubici	Kubica	k1gMnSc2
a	a	k8xC
stáhl	stáhnout	k5eAaPmAgInS
tak	tak	k9
bod	bod	k1gInSc1
z	z	k7c2
náskoku	náskok	k1gInSc2
Alonsa	Alonsa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujícím	následující	k2eAgInSc6d1
závodě	závod	k1gInSc6
v	v	k7c6
Turecku	Turecko	k1gNnSc6
dojel	dojet	k5eAaPmAgMnS
Alonso	Alonsa	k1gFnSc5
druhý	druhý	k4xOgInSc1
před	před	k7c7
třetím	třetí	k4xOgMnSc7
Schumacherem	Schumacher	k1gMnSc7
<g/>
,	,	kIx,
po	po	k7c6
dlouhé	dlouhý	k2eAgFnSc6d1
třináctikolové	třináctikolový	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Italské	italský	k2eAgFnSc6d1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
měl	mít	k5eAaImAgMnS
Alonso	Alonsa	k1gFnSc5
smůlu	smůla	k1gFnSc4
při	při	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
mu	on	k3xPp3gMnSc3
praskla	prasknout	k5eAaPmAgFnS
pneumatika	pneumatika	k1gFnSc1
a	a	k8xC
také	také	k9
poničil	poničit	k5eAaPmAgInS
karosérii	karosérie	k1gFnSc4
na	na	k7c6
zádi	záď	k1gFnSc6
svého	svůj	k3xOyFgNnSc2
šasi	šasi	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvalifikoval	kvalifikovat	k5eAaBmAgMnS
se	se	k3xPyFc4
jako	jako	k9
pátý	pátý	k4xOgInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
stewardy	steward	k1gMnPc7
byl	být	k5eAaImAgInS
odsunut	odsunut	k2eAgInSc1d1
na	na	k7c4
desáté	desátý	k4xOgNnSc4
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
když	když	k8xS
měl	mít	k5eAaImAgInS
údajně	údajně	k6eAd1
blokovat	blokovat	k5eAaImF
Felipeho	Felipe	k1gMnSc4
Massu	Mass	k1gInSc2
z	z	k7c2
Ferrari	ferrari	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závodě	závod	k1gInSc6
se	se	k3xPyFc4
Španěl	Španěl	k1gMnSc1
probojoval	probojovat	k5eAaPmAgMnS
na	na	k7c4
třetí	třetí	k4xOgNnSc4
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
když	když	k8xS
díky	díky	k7c3
zastávkám	zastávka	k1gFnPc3
v	v	k7c6
boxech	box	k1gInPc6
předjel	předjet	k5eAaPmAgMnS
Massu	Massa	k1gFnSc4
a	a	k8xC
také	také	k9
Roberta	Robert	k1gMnSc4
Kubicu	Kubica	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zradil	zradit	k5eAaPmAgMnS
ho	on	k3xPp3gNnSc4
však	však	k9
motor	motor	k1gInSc4
a	a	k8xC
Alonso	Alonsa	k1gFnSc5
musel	muset	k5eAaImAgMnS
ze	z	k7c2
závodu	závod	k1gInSc2
odstoupit	odstoupit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michael	Michael	k1gMnSc1
Schumacher	Schumachra	k1gFnPc2
závod	závod	k1gInSc4
vyhrál	vyhrát	k5eAaPmAgMnS
a	a	k8xC
stáhl	stáhnout	k5eAaPmAgMnS
Alonsův	Alonsův	k2eAgInSc4d1
náskok	náskok	k1gInSc4
na	na	k7c4
pouhé	pouhý	k2eAgInPc4d1
2	#num#	k4
body	bod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dalším	další	k2eAgInSc6d1
závodě	závod	k1gInSc6
v	v	k7c6
Číně	Čína	k1gFnSc6
si	se	k3xPyFc3
v	v	k7c6
mokré	mokrý	k2eAgFnSc6d1
kvalifikaci	kvalifikace	k1gFnSc6
vybojoval	vybojovat	k5eAaPmAgInS
Alonso	Alonsa	k1gFnSc5
pole	pole	k1gNnSc4
position	position	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
nestálém	stálý	k2eNgNnSc6d1
počasí	počasí	k1gNnSc6
v	v	k7c6
závodě	závod	k1gInSc6
nejprve	nejprve	k6eAd1
Alonso	Alonsa	k1gFnSc5
vedl	vést	k5eAaImAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
kvůli	kvůli	k7c3
špatné	špatný	k2eAgFnSc6d1
druhé	druhý	k4xOgFnSc6
sadě	sada	k1gFnSc6
pneumatik	pneumatika	k1gFnPc2
dojel	dojet	k5eAaPmAgMnS
Alonso	Alonsa	k1gFnSc5
druhý	druhý	k4xOgInSc1
za	za	k7c7
Schumacherem	Schumacher	k1gMnSc7
a	a	k8xC
bodový	bodový	k2eAgInSc1d1
stav	stav	k1gInSc1
se	se	k3xPyFc4
vyrovnal	vyrovnat	k5eAaBmAgInS,k5eAaPmAgInS
<g/>
,	,	kIx,
přesto	přesto	k8xC
byl	být	k5eAaImAgInS
Alonso	Alonsa	k1gFnSc5
až	až	k6eAd1
druhý	druhý	k4xOgMnSc1
díky	díky	k7c3
Schumacherově	Schumacherův	k2eAgNnSc6d1
vítězství	vítězství	k1gNnSc6
navíc	navíc	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Alonso	Alonsa	k1gFnSc5
získal	získat	k5eAaPmAgMnS
titul	titul	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
při	při	k7c6
GP	GP	kA
Brazílie	Brazílie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Japonské	japonský	k2eAgFnSc6d1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
se	se	k3xPyFc4
obě	dva	k4xCgNnPc1
Ferrari	ferrari	k1gNnPc1
Schumacher	Schumachra	k1gFnPc2
a	a	k8xC
Massy	Massa	k1gFnPc1
kvalifikovaly	kvalifikovat	k5eAaBmAgFnP
na	na	k7c6
prvním	první	k4xOgMnSc6
a	a	k8xC
druhém	druhý	k4xOgInSc6
místě	místo	k1gNnSc6
a	a	k8xC
porazily	porazit	k5eAaPmAgFnP
Renaulty	renault	k1gInPc4
na	na	k7c6
pátém	pátý	k4xOgInSc6
a	a	k8xC
šestém	šestý	k4xOgInSc6
místě	místo	k1gNnSc6
o	o	k7c6
více	hodně	k6eAd2
než	než	k8xS
půl	půl	k1xP
sekundy	sekunda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouho	dlouho	k6eAd1
Alonso	Alonsa	k1gFnSc5
jezdil	jezdit	k5eAaImAgMnS
druhý	druhý	k4xOgInSc4
a	a	k8xC
měl	mít	k5eAaImAgMnS
na	na	k7c4
Schumachera	Schumacher	k1gMnSc4
ztrátu	ztráta	k1gFnSc4
5	#num#	k4
sekund	sekunda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potom	potom	k8xC
Schumacherovi	Schumacher	k1gMnSc3
selhal	selhat	k5eAaPmAgMnS
motor	motor	k1gInSc4
a	a	k8xC
musel	muset	k5eAaImAgMnS
odstoupit	odstoupit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alonso	Alonsa	k1gFnSc5
nakonec	nakonec	k6eAd1
zvítězil	zvítězit	k5eAaPmAgInS
a	a	k8xC
před	před	k7c7
posledním	poslední	k2eAgInSc7d1
závodem	závod	k1gInSc7
měl	mít	k5eAaImAgInS
náskok	náskok	k1gInSc4
10	#num#	k4
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
velké	velký	k2eAgFnSc6d1
ceně	cena	k1gFnSc6
mu	on	k3xPp3gMnSc3
tak	tak	k9
stačil	stačit	k5eAaBmAgInS
k	k	k7c3
zisku	zisk	k1gInSc3
titulu	titul	k1gInSc2
mistra	mistr	k1gMnSc2
světa	svět	k1gInSc2
jediný	jediný	k2eAgInSc4d1
bod	bod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Brazílie	Brazílie	k1gFnSc2
<g/>
,	,	kIx,
posledním	poslední	k2eAgInSc6d1
závodě	závod	k1gInSc6
sezóny	sezóna	k1gFnSc2
22	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
<g/>
,	,	kIx,
dojel	dojet	k5eAaPmAgMnS
Alonso	Alonsa	k1gFnSc5
druhý	druhý	k4xOgInSc1
za	za	k7c7
Felippem	Felipp	k1gInSc7
Massou	Massa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schumacher	Schumachra	k1gFnPc2
skončil	skončit	k5eAaPmAgInS
čtvrtý	čtvrtý	k4xOgInSc4
a	a	k8xC
výsledný	výsledný	k2eAgInSc4d1
rozdíl	rozdíl	k1gInSc4
mezi	mezi	k7c7
ním	on	k3xPp3gMnSc7
a	a	k8xC
Alonsem	Alons	k1gMnSc7
byl	být	k5eAaImAgMnS
tak	tak	k6eAd1
13	#num#	k4
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
nejmladším	mladý	k2eAgMnSc7d3
dvojnásobným	dvojnásobný	k2eAgMnSc7d1
šampiónem	šampión	k1gMnSc7
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc1
Renault	renault	k1gInSc4
zvítězil	zvítězit	k5eAaPmAgInS
i	i	k9
v	v	k7c6
poháru	pohár	k1gInSc6
konstruktérů	konstruktér	k1gMnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
porazil	porazit	k5eAaPmAgMnS
Ferrari	Ferrari	k1gMnSc1
o	o	k7c4
5	#num#	k4
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
<g/>
:	:	kIx,
McLaren	McLarna	k1gFnPc2
</s>
<s>
Alonso	Alonsa	k1gFnSc5
při	při	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Malajsie	Malajsie	k1gFnSc2
2007	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
zvítězil	zvítězit	k5eAaPmAgMnS
poprvé	poprvé	k6eAd1
za	za	k7c2
McLaren	McLarna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Už	už	k6eAd1
19	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2005	#num#	k4
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
bude	být	k5eAaImBp3nS
Alonso	Alonsa	k1gFnSc5
jezdit	jezdit	k5eAaImF
za	za	k7c4
tým	tým	k1gInSc4
McLaren	McLarna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alonsova	Alonsův	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
s	s	k7c7
Renaultem	renault	k1gInSc7
vypršela	vypršet	k5eAaPmAgFnS
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2006	#num#	k4
<g/>
,	,	kIx,
přesto	přesto	k8xC
už	už	k6eAd1
15	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
Alonso	Alonsa	k1gFnSc5
po	po	k7c4
svolení	svolení	k1gNnSc4
Flavia	Flavius	k1gMnSc2
Briatoreho	Briatore	k1gMnSc2
a	a	k8xC
týmu	tým	k1gInSc2
Renault	renault	k1gInSc1
testoval	testovat	k5eAaImAgInS
pro	pro	k7c4
McLaren	McLarno	k1gNnPc2
na	na	k7c6
okruhu	okruh	k1gInSc6
v	v	k7c6
Jerezu	Jerez	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fernando	Fernanda	k1gFnSc5
jel	jet	k5eAaImAgMnS
v	v	k7c6
neoznačeném	označený	k2eNgInSc6d1
voze	vůz	k1gInSc6
McLaren	McLarna	k1gFnPc2
MP	MP	kA
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
hlavě	hlava	k1gFnSc6
měl	mít	k5eAaImAgMnS
bílou	bílý	k2eAgFnSc4d1
přilbu	přilba	k1gFnSc4
a	a	k8xC
objel	objet	k5eAaPmAgMnS
95	#num#	k4
kol	kola	k1gFnPc2
<g/>
,	,	kIx,
nastaly	nastat	k5eAaPmAgFnP
však	však	k9
potíže	potíž	k1gFnPc1
s	s	k7c7
měřením	měření	k1gNnSc7
a	a	k8xC
tak	tak	k9
Alonsovy	Alonsův	k2eAgInPc1d1
časy	čas	k1gInPc1
nejsou	být	k5eNaImIp3nP
zcela	zcela	k6eAd1
ověřené	ověřený	k2eAgFnPc1d1
a	a	k8xC
přesné	přesný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
Alonsův	Alonsův	k2eAgMnSc1d1
stájový	stájový	k2eAgMnSc1d1
kolega	kolega	k1gMnSc1
byl	být	k5eAaImAgMnS
vybrán	vybrán	k2eAgInSc4d1
Lewis	Lewis	k1gInSc4
Hamilton	Hamilton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
McLaren	McLarna	k1gFnPc2
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Alonsův	Alonsův	k2eAgInSc1d1
plat	plat	k1gInSc1
pro	pro	k7c4
rok	rok	k1gInSc4
2007	#num#	k4
bude	být	k5eAaImBp3nS
asi	asi	k9
20	#num#	k4
milionů	milion	k4xCgInPc2
liber	libra	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
přepočtu	přepočet	k1gInSc6
39	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
novým	nový	k2eAgMnSc7d1
McLarenem	McLaren	k1gMnSc7
debutoval	debutovat	k5eAaBmAgMnS
Alonso	Alonsa	k1gFnSc5
oficiálně	oficiálně	k6eAd1
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2007	#num#	k4
v	v	k7c6
ulicích	ulice	k1gFnPc6
Valencie	Valencie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alonso	Alonsa	k1gFnSc5
se	se	k3xPyFc4
též	též	k9
projel	projet	k5eAaPmAgMnS
v	v	k7c6
limitované	limitovaný	k2eAgFnSc6d1
edici	edice	k1gFnSc6
auta	auto	k1gNnSc2
Mercedes-Benz	Mercedes-Benza	k1gFnPc2
SLR	SLR	kA
McLaren	McLarna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španěl	Španěl	k1gMnSc1
označil	označit	k5eAaPmAgMnS
toto	tento	k3xDgNnSc4
auto	auto	k1gNnSc4
za	za	k7c4
„	„	k?
<g/>
nejkrásnější	krásný	k2eAgFnSc1d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
McLareny	McLarena	k1gFnPc1
už	už	k9
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
ukázaly	ukázat	k5eAaPmAgFnP
svou	svůj	k3xOyFgFnSc4
konkurenceschopnost	konkurenceschopnost	k1gFnSc4
a	a	k8xC
Alonso	Alonsa	k1gFnSc5
a	a	k8xC
Hamilton	Hamilton	k1gInSc4
dojeli	dojet	k5eAaPmAgMnP
na	na	k7c6
druhém	druhý	k4xOgInSc6
a	a	k8xC
třetím	třetí	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alonso	Alonsa	k1gFnSc5
v	v	k7c6
druhém	druhý	k4xOgInSc6
závodě	závod	k1gInSc6
sezóny	sezóna	k1gFnSc2
v	v	k7c6
Malajsii	Malajsie	k1gFnSc6
získal	získat	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
první	první	k4xOgNnSc4
vítězství	vítězství	k1gNnSc4
pro	pro	k7c4
McLaren	McLarna	k1gFnPc2
a	a	k8xC
pro	pro	k7c4
tým	tým	k1gInSc4
první	první	k4xOgFnSc2
od	od	k7c2
velké	velký	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
Japonska	Japonsko	k1gNnSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obtížná	obtížný	k2eAgFnSc1d1
jízda	jízda	k1gFnSc1
v	v	k7c6
Bahrajnském	Bahrajnský	k2eAgInSc6d1
Sakhiru	Sakhir	k1gInSc6
znamenala	znamenat	k5eAaImAgFnS
pro	pro	k7c4
Alonsa	Alons	k1gMnSc4
až	až	k9
páté	pátý	k4xOgNnSc4
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jeho	jeho	k3xOp3gMnSc1
stájový	stájový	k2eAgMnSc1d1
kolega	kolega	k1gMnSc1
Hamilton	Hamilton	k1gInSc4
opět	opět	k6eAd1
dojel	dojet	k5eAaPmAgMnS
na	na	k7c6
stupních	stupeň	k1gInPc6
vítězů	vítěz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
čtvrtém	čtvrtý	k4xOgInSc6
závodě	závod	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c6
domácí	domácí	k2eAgFnSc6d1
trati	trať	k1gFnSc6
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
Alonso	Alonsa	k1gFnSc5
kvalifikoval	kvalifikovat	k5eAaBmAgMnS
jako	jako	k9
druhý	druhý	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgNnSc6
kole	kolo	k1gNnSc6
měl	mít	k5eAaImAgInS
kontakt	kontakt	k1gInSc4
s	s	k7c7
Felipem	Felip	k1gInSc7
Massou	Massý	k2eAgFnSc4d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
drobné	drobný	k2eAgNnSc4d1
poničení	poničení	k1gNnSc4
vozu	vůz	k1gInSc2
<g/>
,	,	kIx,
přesto	přesto	k8xC
dojel	dojet	k5eAaPmAgMnS
Alonso	Alonsa	k1gFnSc5
třetí	třetí	k4xOgFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalším	další	k2eAgInSc6d1
závodě	závod	k1gInSc6
se	se	k3xPyFc4
ale	ale	k9
Alonsovi	Alonsův	k2eAgMnPc1d1
opět	opět	k6eAd1
dařilo	dařit	k5eAaImAgNnS
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
v	v	k7c6
ulicích	ulice	k1gFnPc6
Monaka	Monako	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Fernando	Fernanda	k1gFnSc5
vybojoval	vybojovat	k5eAaPmAgMnS
druhé	druhý	k4xOgNnSc4
vítězství	vítězství	k1gNnSc4
v	v	k7c6
sezóně	sezóna	k1gFnSc6
<g/>
,	,	kIx,
pole	pola	k1gFnSc6
position	position	k1gInSc4
i	i	k8xC
nejrychlejší	rychlý	k2eAgNnSc4d3
kolo	kolo	k1gNnSc4
<g/>
,	,	kIx,
tzv.	tzv.	kA
„	„	k?
<g/>
triple	tripl	k1gInSc5
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
další	další	k2eAgFnSc6d1
části	část	k1gFnSc6
sezóny	sezóna	k1gFnSc2
pokračoval	pokračovat	k5eAaImAgMnS
Alonso	Alonsa	k1gFnSc5
v	v	k7c6
umístěních	umístění	k1gNnPc6
na	na	k7c6
bodovaných	bodovaný	k2eAgFnPc6d1
příčkách	příčka	k1gFnPc6
a	a	k8xC
závody	závod	k1gInPc7
v	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
v	v	k7c6
Itálii	Itálie	k1gFnSc6
vyhrál	vyhrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
a	a	k8xC
též	též	k9
poslední	poslední	k2eAgInSc4d1
výpadek	výpadek	k1gInSc4
v	v	k7c6
sezóně	sezóna	k1gFnSc6
zaznamenal	zaznamenat	k5eAaPmAgMnS
Fernando	Fernanda	k1gFnSc5
v	v	k7c6
Japonsku	Japonsko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
předposledním	předposlední	k2eAgInSc6d1
závodě	závod	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
dojel	dojet	k5eAaPmAgMnS
druhý	druhý	k4xOgMnSc1
a	a	k8xC
před	před	k7c7
posledním	poslední	k2eAgInSc7d1
závodem	závod	k1gInSc7
ztrácel	ztrácet	k5eAaImAgMnS
na	na	k7c4
lídra	lídr	k1gMnSc4
šampionátu	šampionát	k1gInSc2
<g/>
,	,	kIx,
Lewise	Lewise	k1gFnSc1
Hamiltona	Hamiltona	k1gFnSc1
<g/>
,	,	kIx,
4	#num#	k4
body	bod	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Brazílii	Brazílie	k1gFnSc6
tak	tak	k6eAd1
měl	mít	k5eAaImAgInS
ještě	ještě	k9
šanci	šance	k1gFnSc4
dosáhnout	dosáhnout	k5eAaPmF
na	na	k7c4
titul	titul	k1gInSc4
mistra	mistr	k1gMnSc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
ale	ale	k9
nepovedlo	povést	k5eNaPmAgNnS
<g/>
,	,	kIx,
Hamilton	Hamilton	k1gInSc1
sice	sice	k8xC
skončil	skončit	k5eAaPmAgInS
až	až	k9
na	na	k7c4
7	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
mu	on	k3xPp3gMnSc3
titul	titul	k1gInSc1
mistra	mistr	k1gMnSc4
světa	svět	k1gInSc2
nezaručovalo	zaručovat	k5eNaImAgNnS
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
Alonso	Alonsa	k1gFnSc5
dojel	dojet	k5eAaPmAgMnS
až	až	k6eAd1
třetí	třetí	k4xOgMnPc1
za	za	k7c7
Massou	Massa	k1gFnSc7
a	a	k8xC
Räikkönenem	Räikkönen	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
nakonec	nakonec	k6eAd1
"	"	kIx"
<g/>
vyfouknul	vyfouknout	k5eAaPmAgInS
<g/>
"	"	kIx"
titul	titul	k1gInSc1
jezdcům	jezdec	k1gInPc3
McLarenu	McLaren	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hamilton	Hamilton	k1gInSc1
Alonsa	Alons	k1gMnSc4
nakonec	nakonec	k6eAd1
porazil	porazit	k5eAaPmAgMnS
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
již	již	k6eAd1
od	od	k7c2
VC	VC	kA
Kanady	Kanada	k1gFnSc2
byl	být	k5eAaImAgInS
ve	v	k7c6
vedení	vedení	k1gNnSc6
a	a	k8xC
ve	v	k7c6
vedení	vedení	k1gNnSc6
před	před	k7c7
ním	on	k3xPp3gNnSc7
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
sezóny	sezóna	k1gFnSc2
zůstal	zůstat	k5eAaPmAgMnS
<g/>
,	,	kIx,
tj.	tj.	kA
nováček	nováček	k1gInSc1
Hamilton	Hamilton	k1gInSc1
porazil	porazit	k5eAaPmAgInS
dvojnásobného	dvojnásobný	k2eAgMnSc4d1
mistra	mistr	k1gMnSc4
světa	svět	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c4
obhájce	obhájce	k1gMnSc4
titulu	titul	k1gInSc2
Alonsa	Alonsa	k1gFnSc1
tedy	tedy	k8xC
zbyla	zbýt	k5eAaPmAgFnS
až	až	k9
třetí	třetí	k4xOgFnSc1
pozice	pozice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
mimořádně	mimořádně	k6eAd1
kontroverzní	kontroverzní	k2eAgFnSc3d1
sezoně	sezona	k1gFnSc3
Alonsa	Alonsa	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
vedení	vedení	k1gNnSc2
týmu	tým	k1gInSc2
McLaren	McLarna	k1gFnPc2
oznámilo	oznámit	k5eAaPmAgNnS
rozvázání	rozvázání	k1gNnSc4
smlouvy	smlouva	k1gFnSc2
s	s	k7c7
Alonsem	Alons	k1gInSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
platit	platit	k5eAaImF
do	do	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
Alonso	Alonsa	k1gFnSc5
nemusí	muset	k5eNaImIp3nP
za	za	k7c4
uvolnění	uvolnění	k1gNnSc4
ze	z	k7c2
smlouvy	smlouva	k1gFnSc2
zaplatit	zaplatit	k5eAaPmF
žádné	žádný	k3yNgInPc4
peníze	peníz	k1gInPc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ale	ale	k9
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
odchod	odchod	k1gInSc4
z	z	k7c2
týmu	tým	k1gInSc2
zdaleka	zdaleka	k6eAd1
nebyl	být	k5eNaImAgInS
dobrovolný	dobrovolný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
:	:	kIx,
Renault	renault	k1gInSc1
</s>
<s>
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
při	při	k7c6
GP	GP	kA
Singapuru	Singapur	k1gInSc2
2009	#num#	k4
při	při	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
získal	získat	k5eAaPmAgMnS
jediné	jediný	k2eAgNnSc4d1
pódium	pódium	k1gNnSc4
v	v	k7c6
sezoně	sezona	k1gFnSc6
</s>
<s>
Po	po	k7c6
nedobrovolném	dobrovolný	k2eNgNnSc6d1
ukončení	ukončení	k1gNnSc6
spolupráce	spolupráce	k1gFnSc2
s	s	k7c7
McLarenem	McLaren	k1gInSc7
media	medium	k1gNnSc2
spekulovala	spekulovat	k5eAaImAgFnS
<g/>
,	,	kIx,
za	za	k7c4
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
tým	tým	k1gInSc4
bude	být	k5eAaImBp3nS
Alonso	Alonsa	k1gFnSc5
závodit	závodit	k5eAaImF
v	v	k7c6
sezoně	sezona	k1gFnSc6
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mluvilo	mluvit	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c6
Renaultu	renault	k1gInSc6
<g/>
,	,	kIx,
Red	Red	k1gMnPc4
Bullu	bulla	k1gFnSc4
<g/>
,	,	kIx,
Toyotě	toyota	k1gFnSc3
a	a	k8xC
Hondě	honda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
Fernando	Fernanda	k1gFnSc5
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Renaultu	renault	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
kterým	který	k3yRgMnPc3,k3yQgMnPc3,k3yIgMnPc3
podepsal	podepsat	k5eAaPmAgMnS
smlouvu	smlouva	k1gFnSc4
na	na	k7c4
2	#num#	k4
roky	rok	k1gInPc4
<g/>
,	,	kIx,
za	za	k7c4
zhruba	zhruba	k6eAd1
25	#num#	k4
milionu	milion	k4xCgInSc2
liber	libra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
sezónou	sezóna	k1gFnSc7
byl	být	k5eAaImAgInS
sice	sice	k8xC
Alonso	Alonsa	k1gFnSc5
pasován	pasovat	k5eAaImNgMnS,k5eAaBmNgMnS
do	do	k7c2
role	role	k1gFnSc2
černého	černý	k2eAgMnSc4d1
koně	kůň	k1gMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
první	první	k4xOgInSc4
závod	závod	k1gInSc4
všechny	všechen	k3xTgFnPc4
ambice	ambice	k1gFnPc4
vyvrátil	vyvrátit	k5eAaPmAgMnS
<g/>
,	,	kIx,
když	když	k8xS
dojel	dojet	k5eAaPmAgMnS
na	na	k7c6
šťastném	šťastný	k2eAgMnSc6d1
4	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
rychlostně	rychlostně	k6eAd1
Renault	renault	k1gInSc1
nestačil	stačit	k5eNaBmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Malajsii	Malajsie	k1gFnSc6
vybojoval	vybojovat	k5eAaPmAgInS
pouhý	pouhý	k2eAgInSc1d1
bod	bod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Bahrajnu	Bahrajn	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
na	na	k7c4
nekonkurenceschopný	konkurenceschopný	k2eNgInSc4d1
vůz	vůz	k1gInSc4
Fernanda	Fernanda	k1gFnSc1
Alonsa	Alons	k1gMnSc2
nejprve	nejprve	k6eAd1
najel	najet	k5eAaPmAgMnS
Lewis	Lewis	k1gFnSc3
Hamilton	Hamilton	k1gInSc4
<g/>
,	,	kIx,
dosáhl	dosáhnout	k5eAaPmAgInS
Alonso	Alonsa	k1gFnSc5
s	s	k7c7
Renaultem	renault	k1gInSc7
na	na	k7c4
10	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Španělska	Španělsko	k1gNnSc2
přivezl	přivézt	k5eAaPmAgInS
tým	tým	k1gInSc1
aerodynamické	aerodynamický	k2eAgFnSc2d1
novinky	novinka	k1gFnSc2
<g/>
,	,	kIx,
<g/>
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
okamžitě	okamžitě	k6eAd1
pomohly	pomoct	k5eAaPmAgFnP
Alonsovi	Alonsův	k2eAgMnPc1d1
do	do	k7c2
první	první	k4xOgFnSc2
řady	řada	k1gFnSc2
v	v	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Start	start	k1gInSc1
už	už	k6eAd1
tak	tak	k9
dobrý	dobrý	k2eAgMnSc1d1
nebyl	být	k5eNaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
Alonso	Alonsa	k1gFnSc5
se	se	k3xPyFc4
tak	tak	k6eAd1
propadl	propadnout	k5eAaPmAgInS
na	na	k7c4
třetí	třetí	k4xOgNnSc4
místo	místo	k1gNnSc4
za	za	k7c4
obě	dva	k4xCgNnPc4
Ferrari	ferrari	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedl	vést	k5eAaImAgMnS
si	se	k3xPyFc3
skvěle	skvěle	k6eAd1
<g/>
,	,	kIx,
dokud	dokud	k8xS
ho	on	k3xPp3gInSc4
nezastavila	zastavit	k5eNaPmAgFnS
porucha	porucha	k1gFnSc1
motoru	motor	k1gInSc2
<g/>
,	,	kIx,
po	po	k7c4
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
musel	muset	k5eAaImAgMnS
odstoupit	odstoupit	k5eAaPmF
z	z	k7c2
výborně	výborně	k6eAd1
rozjetého	rozjetý	k2eAgInSc2d1
závodu	závod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Turecku	Turecko	k1gNnSc6
na	na	k7c4
Alonsa	Alons	k1gMnSc4
čekalo	čekat	k5eAaImAgNnS
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
čili	čili	k8xC
potvrzení	potvrzení	k1gNnSc4
faktu	fakt	k1gInSc2
<g/>
,	,	kIx,
<g/>
že	že	k8xS
se	se	k3xPyFc4
Renault	renault	k1gInSc1
zlepšil	zlepšit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebýt	být	k5eNaImF
problémů	problém	k1gInPc2
s	s	k7c7
pneumatikou	pneumatika	k1gFnSc7
v	v	k7c6
Monaku	Monako	k1gNnSc6
a	a	k8xC
poté	poté	k6eAd1
také	také	k9
kolize	kolize	k1gFnSc1
s	s	k7c7
Heidfeldem	Heidfeld	k1gInSc7
mohl	moct	k5eAaImAgMnS
Alonso	Alonsa	k1gFnSc5
pomýšlet	pomýšlet	k5eAaImF
na	na	k7c4
velmi	velmi	k6eAd1
dobré	dobrý	k2eAgNnSc4d1
umístění	umístění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
bláznivém	bláznivý	k2eAgInSc6d1
závodu	závod	k1gInSc6
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
mohli	moct	k5eAaImAgMnP
u	u	k7c2
Renaultu	renault	k1gInSc2
díky	díky	k7c3
Alonsovi	Alons	k1gMnSc3
pomýšlet	pomýšlet	k5eAaImF
dokonce	dokonce	k9
na	na	k7c4
vítězství	vítězství	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
strategie	strategie	k1gFnSc1
a	a	k8xC
velké	velký	k2eAgNnSc1d1
zdržení	zdržení	k1gNnSc1
za	za	k7c7
Nickem	Nicek	k1gMnSc7
Heidfeldem	Heidfeld	k1gMnSc7
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
maximální	maximální	k2eAgNnSc4d1
úsilí	úsilí	k1gNnSc4
dostat	dostat	k5eAaPmF
se	se	k3xPyFc4
před	před	k7c4
pomalejší	pomalý	k2eAgInSc4d2
vůz	vůz	k1gInSc4
BMW	BMW	kA
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
vyústilo	vyústit	k5eAaPmAgNnS
v	v	k7c6
havárii	havárie	k1gFnSc6
a	a	k8xC
konec	konec	k1gInSc4
v	v	k7c6
závodě	závod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Francii	Francie	k1gFnSc6
startoval	startovat	k5eAaBmAgMnS
Španěl	Španěl	k1gMnSc1
z	z	k7c2
výborné	výborný	k2eAgFnSc2d1
třetí	třetí	k4xOgFnSc2
pozice	pozice	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
během	během	k7c2
závodu	závod	k1gInSc2
propadl	propadnout	k5eAaPmAgInS
až	až	k9
na	na	k7c4
konečné	konečný	k2eAgNnSc4d1
osmé	osmý	k4xOgNnSc4
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
když	když	k8xS
ho	on	k3xPp3gInSc4
pár	pár	k1gInSc4
kol	kolo	k1gNnPc2
před	před	k7c7
koncem	konec	k1gInSc7
předjel	předjet	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
kolega	kolega	k1gMnSc1
Piquet	Piquet	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silverstone	Silverston	k1gInSc5
přinesl	přinést	k5eAaPmAgInS
dobré	dobré	k1gNnSc4
i	i	k8xC
špatné	špatný	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
během	během	k7c2
deštivého	deštivý	k2eAgInSc2d1
závodu	závod	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamenalo	znamenat	k5eAaImAgNnS
šestou	šestý	k4xOgFnSc4
příčku	příčka	k1gFnSc4
v	v	k7c6
cíli	cíl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
v	v	k7c4
Mangy-Cours	Mangy-Cours	k1gInSc4
i	i	k9
na	na	k7c6
Hockenheimringu	Hockenheimring	k1gInSc6
svitla	svitnout	k5eAaPmAgFnS
po	po	k7c6
výborné	výborný	k2eAgFnSc6d1
kvalifikaci	kvalifikace	k1gFnSc6
naděje	naděje	k1gFnSc2
na	na	k7c4
dobrý	dobrý	k2eAgInSc4d1
výsledek	výsledek	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
katastrofální	katastrofální	k2eAgInSc1d1
průběh	průběh	k1gInSc1
se	s	k7c7
spoustou	spousta	k1gFnSc7
chyb	chyba	k1gFnPc2
nepřinesl	přinést	k5eNaPmAgMnS
do	do	k7c2
tábora	tábor	k1gInSc2
Renaultu	renault	k1gInSc2
z	z	k7c2
Alonsovy	Alonsův	k2eAgFnSc2d1
strany	strana	k1gFnSc2
ani	ani	k8xC
bod	bod	k1gInSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Piquet	Piquet	k1gInSc1
jich	on	k3xPp3gFnPc2
za	za	k7c4
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
přivezl	přivézt	k5eAaPmAgMnS
hned	hned	k6eAd1
osm	osm	k4xCc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trať	trať	k1gFnSc1
v	v	k7c6
maďarském	maďarský	k2eAgInSc6d1
Hungaroringu	Hungaroring	k1gInSc6
pomohla	pomoct	k5eAaPmAgFnS
Alonsovi	Alons	k1gMnSc3
většinu	většina	k1gFnSc4
závodu	závod	k1gInSc2
za	za	k7c7
sebou	se	k3xPyFc7
držet	držet	k5eAaImF
rychlejšího	rychlý	k2eAgMnSc4d2
Räikkönena	Räikkönen	k1gMnSc4
a	a	k8xC
odměnou	odměna	k1gFnSc7
mu	on	k3xPp3gInSc3
byla	být	k5eAaImAgFnS
čtvrtá	čtvrtý	k4xOgFnSc1
pozice	pozice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
konce	konec	k1gInSc2
sezóny	sezóna	k1gFnSc2
Alonso	Alonsa	k1gFnSc5
přece	přece	k9
jen	jen	k6eAd1
vyhrál	vyhrát	k5eAaPmAgInS
2	#num#	k4
GP	GP	kA
(	(	kIx(
<g/>
Singapur	Singapur	k1gInSc1
a	a	k8xC
Japonsko	Japonsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
VC	VC	kA
Singapuru	Singapur	k1gInSc2
po	po	k7c6
kontroverzním	kontroverzní	k2eAgInSc6d1
průběhu	průběh	k1gInSc6
(	(	kIx(
<g/>
díky	díky	k7c3
úmyslné	úmyslný	k2eAgFnSc3d1
havárii	havárie	k1gFnSc3
týmového	týmový	k2eAgMnSc2d1
kolegy	kolega	k1gMnSc2
Nelsona	Nelson	k1gMnSc4
Piqueta	Piquet	k1gMnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
způsobila	způsobit	k5eAaPmAgFnS
výjezd	výjezd	k1gInSc4
Safety	Safeta	k1gFnSc2
Caru	car	k1gMnSc3
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc2,k3yIgMnSc2,k3yQgMnSc2
Alonso	Alonsa	k1gFnSc5
využil	využít	k5eAaPmAgInS
při	při	k7c6
pitstopu	pitstop	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
posledním	poslední	k2eAgInSc6d1
závodě	závod	k1gInSc6
(	(	kIx(
<g/>
Brazílie	Brazílie	k1gFnSc1
<g/>
)	)	kIx)
získal	získat	k5eAaPmAgMnS
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
:	:	kIx,
Ferrari	Ferrari	k1gMnSc1
</s>
<s>
Alonso	Alonsa	k1gFnSc5
Canadian	Canadian	k1gInSc4
GP	GP	kA
2010	#num#	k4
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
podepsal	podepsat	k5eAaPmAgMnS
Fernando	Fernanda	k1gFnSc5
smlouvu	smlouva	k1gFnSc4
s	s	k7c7
Ferrari	ferrari	k1gNnSc7
na	na	k7c4
3	#num#	k4
sezóny	sezóna	k1gFnSc2
do	do	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
za	za	k7c4
cca	cca	kA
25	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
euro	euro	k1gNnSc1
za	za	k7c4
sezónu	sezóna	k1gFnSc4
<g/>
,	,	kIx,
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k9
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2010	#num#	k4
nejdražším	drahý	k2eAgInSc7d3
platícím	platící	k2eAgInSc7d1
pilotem	pilot	k1gInSc7
ve	v	k7c6
startovním	startovní	k2eAgNnSc6d1
poli	pole	k1gNnSc6
(	(	kIx(
<g/>
a	a	k8xC
v	v	k7c6
historii	historie	k1gFnSc6
F	F	kA
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Začal	začít	k5eAaPmAgInS
vítězstvím	vítězství	k1gNnSc7
v	v	k7c6
úvodním	úvodní	k2eAgInSc6d1
závodě	závod	k1gInSc6
v	v	k7c6
Bahrajnu	Bahrajn	k1gInSc6
po	po	k7c6
technických	technický	k2eAgInPc6d1
problémech	problém	k1gInPc6
Sebastiana	Sebastian	k1gMnSc4
Vettela	Vettel	k1gMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
dalších	další	k2eAgInPc6d1
závodech	závod	k1gInPc6
již	již	k6eAd1
nestačil	stačit	k5eNaBmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Držel	držet	k5eAaImAgMnS
se	se	k3xPyFc4
stále	stále	k6eAd1
v	v	k7c6
boji	boj	k1gInSc6
o	o	k7c4
titul	titul	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
opět	opět	k6eAd1
si	se	k3xPyFc3
znepřátelil	znepřátelit	k5eAaPmAgMnS
některé	některý	k3yIgMnPc4
fanoušky	fanoušek	k1gMnPc4
<g/>
,	,	kIx,
po	po	k7c6
použití	použití	k1gNnSc6
týmové	týmový	k2eAgFnSc2d1
režie	režie	k1gFnSc2
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
ale	ale	k8xC
ukázal	ukázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
dokáže	dokázat	k5eAaPmIp3nS
stále	stále	k6eAd1
držet	držet	k5eAaImF
krok	krok	k1gInSc4
s	s	k7c7
těmi	ten	k3xDgMnPc7
nejlepšími	dobrý	k2eAgMnPc7d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
posledním	poslední	k2eAgInSc7d1
závodem	závod	k1gInSc7
v	v	k7c4
Abú	abú	k1gMnPc4
Dhabí	Dhabí	k1gNnSc3
vedl	vést	k5eAaImAgInS
pořadí	pořadí	k1gNnSc4
před	před	k7c7
Markem	Marek	k1gMnSc7
Webberem	Webber	k1gMnSc7
a	a	k8xC
Sebastianem	Sebastian	k1gMnSc7
Vettelem	Vettel	k1gMnSc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
měli	mít	k5eAaImAgMnP
ještě	ještě	k6eAd1
reálnou	reálný	k2eAgFnSc4d1
šanci	šance	k1gFnSc4
na	na	k7c4
zisk	zisk	k1gInSc4
titulu	titul	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ferrari	Ferrari	k1gMnSc1
se	se	k3xPyFc4
ale	ale	k8xC
zaměřilo	zaměřit	k5eAaPmAgNnS
pouze	pouze	k6eAd1
na	na	k7c4
Webbera	Webbero	k1gNnPc4
nikoli	nikoli	k9
na	na	k7c4
Vettela	Vettel	k1gMnSc4
<g/>
,	,	kIx,
tudíž	tudíž	k8xC
když	když	k8xS
Webber	Webber	k1gMnSc1
zavadil	zavadit	k5eAaPmAgMnS
o	o	k7c4
vozidla	vozidlo	k1gNnPc4
a	a	k8xC
jel	jet	k5eAaImAgInS
ke	k	k7c3
svému	svůj	k3xOyFgInSc3
pitstopu	pitstop	k1gInSc3
<g/>
,	,	kIx,
rozhodlo	rozhodnout	k5eAaPmAgNnS
se	se	k3xPyFc4
Ferrari	ferrari	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
nejdříve	dříve	k6eAd3
pošle	poslat	k5eAaPmIp3nS
Felipeho	Felipe	k1gMnSc4
Massu	Mass	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
zjistili	zjistit	k5eAaPmAgMnP
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
s	s	k7c7
pneumatikami	pneumatika	k1gFnPc7
a	a	k8xC
po	po	k7c6
dalších	další	k2eAgNnPc6d1
2	#num#	k4
kolech	kolo	k1gNnPc6
zastavil	zastavit	k5eAaPmAgMnS
u	u	k7c2
svých	svůj	k3xOyFgMnPc2
mechaniků	mechanik	k1gMnPc2
také	také	k9
Alonso	Alonsa	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrátil	vrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
do	do	k7c2
závodu	závod	k1gInSc2
za	za	k7c7
Rusem	Rus	k1gMnSc7
Vitalyem	Vitaly	k1gMnSc7
Petrovem	Petrov	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
absolvoval	absolvovat	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
pit	pit	k2eAgInSc4d1
stop	stop	k1gInSc4
po	po	k7c6
kolizi	kolize	k1gFnSc6
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jenomže	jenomže	k8xC
Alonso	Alonsa	k1gFnSc5
ho	on	k3xPp3gMnSc4
do	do	k7c2
konce	konec	k1gInSc2
závodu	závod	k1gInSc2
nedokázal	dokázat	k5eNaPmAgMnS
předjet	předjet	k2eAgMnSc1d1
a	a	k8xC
dojel	dojet	k5eAaPmAgMnS
na	na	k7c4
7	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
Webber	Webber	k1gMnSc1
zůstal	zůstat	k5eAaPmAgMnS
zaseklý	zaseklý	k2eAgMnSc1d1
za	za	k7c7
touto	tento	k3xDgFnSc7
dvojicí	dvojice	k1gFnSc7
a	a	k8xC
byl	být	k5eAaImAgInS
až	až	k9
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
těmto	tento	k3xDgInPc3
výsledkům	výsledek	k1gInPc3
si	se	k3xPyFc3
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
stylem	styl	k1gInSc7
start	start	k1gInSc4
<g/>
–	–	k?
<g/>
cíl	cíl	k1gInSc1
dojel	dojet	k5eAaPmAgInS
nejen	nejen	k6eAd1
pro	pro	k7c4
vítězství	vítězství	k1gNnSc4
v	v	k7c6
závodě	závod	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
v	v	k7c6
celém	celý	k2eAgInSc6d1
šampionátu	šampionát	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
</s>
<s>
Po	po	k7c6
dvanáctém	dvanáctý	k4xOgNnSc6
místě	místo	k1gNnSc6
v	v	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
na	na	k7c4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Austrálie	Austrálie	k1gFnSc2
obsadil	obsadit	k5eAaPmAgMnS
pátou	pátý	k4xOgFnSc4
příčku	příčka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
devátém	devátý	k4xOgNnSc6
místě	místo	k1gNnSc6
v	v	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
malajsijskou	malajsijský	k2eAgFnSc4d1
velkou	velký	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
vyhrál	vyhrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Devátý	devátý	k4xOgMnSc1
skončil	skončit	k5eAaPmAgMnS
i	i	k8xC
v	v	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
na	na	k7c4
Velkou	velký	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
Číny	Čína	k1gFnSc2
a	a	k8xC
také	také	k9
v	v	k7c6
závodě	závod	k1gInSc6
obsadil	obsadit	k5eAaPmAgMnS
deváté	devátý	k4xOgNnSc4
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
deváté	devátý	k4xOgFnSc2
pozice	pozice	k1gFnSc2
na	na	k7c6
startu	start	k1gInSc6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
Bahrajnu	Bahrajno	k1gNnSc3
obsadil	obsadit	k5eAaPmAgInS
sedmou	sedmý	k4xOgFnSc4
příčku	příčka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
startu	start	k1gInSc6
ze	z	k7c2
druhého	druhý	k4xOgNnSc2
místa	místo	k1gNnSc2
bojoval	bojovat	k5eAaImAgInS
s	s	k7c7
Pastorem	pastor	k1gMnSc7
Maldonadem	Maldonad	k1gInSc7
o	o	k7c4
vítězství	vítězství	k1gNnSc4
v	v	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Španělska	Španělsko	k1gNnSc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
po	po	k7c6
druhé	druhý	k4xOgFnSc6
zastávce	zastávka	k1gFnSc6
v	v	k7c6
boxech	box	k1gInPc6
se	se	k3xPyFc4
vedení	vedení	k1gNnSc2
ujal	ujmout	k5eAaPmAgMnS
Venezuelan	Venezuelan	k1gMnSc1
a	a	k8xC
zaslouženě	zaslouženě	k6eAd1
vyhrál	vyhrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
startu	start	k1gInSc6
z	z	k7c2
pátého	pátý	k4xOgNnSc2
místa	místo	k1gNnSc2
přitlačil	přitlačit	k5eAaPmAgInS
Romaina	Romain	k1gMnSc4
Grosjeana	Grosjean	k1gMnSc4
ke	k	k7c3
svodidlům	svodidlo	k1gNnPc3
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
průběhu	průběh	k1gInSc6
závodu	závod	k1gInSc2
se	se	k3xPyFc4
probojoval	probojovat	k5eAaPmAgMnS
na	na	k7c4
konečné	konečný	k2eAgNnSc4d1
třetí	třetí	k4xOgNnSc4
místo	místo	k1gNnSc4
a	a	k8xC
ujal	ujmout	k5eAaPmAgMnS
se	se	k3xPyFc4
vedení	vedení	k1gNnSc4
šampionátu	šampionát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
konce	konec	k1gInSc2
sezony	sezona	k1gFnSc2
nedojel	dojet	k5eNaPmAgMnS
2	#num#	k4
závody	závod	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Spa	Spa	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
mu	on	k3xPp3gNnSc3
vůz	vůz	k1gInSc4
Romaina	Romain	k1gMnSc4
Grosjeana	Grosjean	k1gMnSc4
proletěl	proletět	k5eAaPmAgInS
před	před	k7c7
hlavou	hlava	k1gFnSc7
a	a	k8xC
zadní	zadní	k2eAgFnSc4d1
část	část	k1gFnSc4
vozu	vůz	k1gInSc2
mu	on	k3xPp3gMnSc3
zničil	zničit	k5eAaPmAgMnS
Lewis	Lewis	k1gFnPc4
Hamilton	Hamilton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
Suzuce	Suzuce	k1gFnPc4
se	se	k3xPyFc4
vyřadil	vyřadit	k5eAaPmAgInS
sám	sám	k3xTgMnSc1
vlastní	vlastnit	k5eAaImIp3nS
chybou	chyba	k1gFnSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
hned	hned	k6eAd1
po	po	k7c6
startu	start	k1gInSc6
vyjel	vyjet	k5eAaPmAgInS
z	z	k7c2
trati	trať	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
všechny	všechen	k3xTgMnPc4
fanoušky	fanoušek	k1gMnPc4
nadchl	nadchnout	k5eAaPmAgInS
svými	svůj	k3xOyFgInPc7
výkony	výkon	k1gInPc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
s	s	k7c7
podřadným	podřadný	k2eAgMnSc7d1
Ferrari	Ferrari	k1gMnSc7
držel	držet	k5eAaImAgMnS
naději	naděje	k1gFnSc4
na	na	k7c4
titul	titul	k1gInSc4
až	až	k9
do	do	k7c2
úplného	úplný	k2eAgInSc2d1
konce	konec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Brazílii	Brazílie	k1gFnSc6
musel	muset	k5eAaImAgMnS
dojet	dojet	k5eAaPmF
na	na	k7c6
pódiu	pódium	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
měl	mít	k5eAaImAgInS
alespoň	alespoň	k9
malou	malý	k2eAgFnSc4d1
šanci	šance	k1gFnSc4
na	na	k7c4
zisk	zisk	k1gInSc4
titulu	titul	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
dojel	dojet	k5eAaPmAgMnS
6	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
mu	on	k3xPp3gMnSc3
zaručilo	zaručit	k5eAaPmAgNnS
3	#num#	k4
<g/>
.	.	kIx.
mistrovský	mistrovský	k2eAgInSc4d1
titul	titul	k1gInSc4
v	v	k7c6
řadě	řada	k1gFnSc6
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
nejmenším	malý	k2eAgInSc7d3
rozdílem	rozdíl	k1gInSc7
o	o	k7c4
3	#num#	k4
body	bod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
</s>
<s>
Alonso	Alonsa	k1gFnSc5
začal	začít	k5eAaPmAgInS
novou	nový	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
druhým	druhý	k4xOgNnSc7
místem	místo	k1gNnSc7
v	v	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalším	další	k2eAgInSc6d1
závodě	závod	k1gInSc6
v	v	k7c6
Malajsii	Malajsie	k1gFnSc6
se	se	k3xPyFc4
Alonso	Alonsa	k1gFnSc5
kvalifikoval	kvalifikovat	k5eAaBmAgMnS
třetí	třetí	k4xOgMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
startu	start	k1gInSc6
narazil	narazit	k5eAaPmAgMnS
do	do	k7c2
zádě	záď	k1gFnSc2
Red	Red	k1gFnSc4
Bullu	bulla	k1gFnSc4
Vettela	Vettel	k1gMnSc2
a	a	k8xC
poničil	poničit	k5eAaPmAgInS
si	se	k3xPyFc3
přední	přední	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alonso	Alonsa	k1gFnSc5
si	se	k3xPyFc3
však	však	k9
pro	pro	k7c4
nové	nový	k2eAgMnPc4d1
do	do	k7c2
boxů	box	k1gInPc2
nezajel	zajet	k5eNaPmAgMnS
<g/>
,	,	kIx,
doufal	doufat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
neulomí	ulomit	k5eNaPmIp3nS
úplně	úplně	k6eAd1
a	a	k8xC
bude	být	k5eAaImBp3nS
moci	moct	k5eAaImF
zajet	zajet	k5eAaPmF
do	do	k7c2
boxů	box	k1gInPc2
ve	v	k7c4
správnou	správný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
později	pozdě	k6eAd2
v	v	k7c6
závodě	závod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jenže	jenže	k8xC
křídlo	křídlo	k1gNnSc4
se	se	k3xPyFc4
ulomilo	ulomit	k5eAaPmAgNnS
hned	hned	k6eAd1
v	v	k7c6
druhém	druhý	k4xOgInSc6
kole	kolo	k1gNnSc6
a	a	k8xC
Alonso	Alonsa	k1gFnSc5
musel	muset	k5eAaImAgMnS
odstoupit	odstoupit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
v	v	k7c6
dalším	další	k2eAgInSc6d1
závodě	závod	k1gInSc6
v	v	k7c6
Číně	Čína	k1gFnSc6
vyhrál	vyhrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
jeho	jeho	k3xOp3gFnSc2
domácí	domácí	k2eAgFnSc2d1
Velké	velký	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
Španělska	Španělsko	k1gNnSc2
se	se	k3xPyFc4
kvalifikoval	kvalifikovat	k5eAaBmAgMnS
na	na	k7c6
pátém	pátý	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závodě	závod	k1gInSc6
se	se	k3xPyFc4
dokázal	dokázat	k5eAaPmAgMnS
dostat	dostat	k5eAaPmF
na	na	k7c4
první	první	k4xOgNnSc4
místo	místo	k1gNnSc4
a	a	k8xC
nakonec	nakonec	k6eAd1
dokázal	dokázat	k5eAaPmAgMnS
vyhrát	vyhrát	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Monaku	Monako	k1gNnSc6
dojel	dojet	k5eAaPmAgMnS
až	až	k9
sedmý	sedmý	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgInPc6d1
dvou	dva	k4xCgInPc6
závodech	závod	k1gInPc6
byl	být	k5eAaImAgMnS
na	na	k7c6
stupních	stupeň	k1gInPc6
vítězů	vítěz	k1gMnPc2
<g/>
:	:	kIx,
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
druhý	druhý	k4xOgMnSc1
a	a	k8xC
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
třetí	třetí	k4xOgFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
však	však	k9
v	v	k7c6
půlce	půlka	k1gFnSc6
sezóny	sezóna	k1gFnSc2
Pirelli	Pirelle	k1gFnSc6
představilo	představit	k5eAaPmAgNnS
nové	nový	k2eAgFnPc4d1
gumy	guma	k1gFnPc4
<g/>
,	,	kIx,
Red	Red	k1gMnPc1
Bull	bulla	k1gFnPc2
s	s	k7c7
Vettelem	Vettel	k1gMnSc7
začali	začít	k5eAaPmAgMnP
být	být	k5eAaImF
neporazitelní	porazitelný	k2eNgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vettel	Vettel	k1gMnSc1
vyhrál	vyhrát	k5eAaPmAgMnS
posledních	poslední	k2eAgNnPc2d1
9	#num#	k4
závodů	závod	k1gInPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Alonso	Alonsa	k1gFnSc5
měl	mít	k5eAaImAgInS
problém	problém	k1gInSc1
dojet	dojet	k5eAaPmF
aspoň	aspoň	k9
na	na	k7c6
stupních	stupeň	k1gInPc6
vítězů	vítěz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
opět	opět	k6eAd1
stal	stát	k5eAaPmAgMnS
vicemistrem	vicemistr	k1gMnSc7
s	s	k7c7
242	#num#	k4
body	bod	k1gInPc7
<g/>
,	,	kIx,
o	o	k7c4
155	#num#	k4
méně	málo	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
měl	mít	k5eAaImAgMnS
šampion	šampion	k1gMnSc1
Vettel	Vettel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
</s>
<s>
Hned	hned	k6eAd1
po	po	k7c6
prvním	první	k4xOgInSc6
závodě	závod	k1gInSc6
bylo	být	k5eAaImAgNnS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Ferrari	ferrari	k1gNnSc1
je	být	k5eAaImIp3nS
nekonkurenceschopné	konkurenceschopný	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alonso	Alonsa	k1gFnSc5
však	však	k9
dokázal	dokázat	k5eAaPmAgInS
dělat	dělat	k5eAaImF
divy	diva	k1gFnPc4
a	a	k8xC
v	v	k7c6
prvních	první	k4xOgInPc6
dvou	dva	k4xCgInPc6
závodech	závod	k1gInPc6
dojel	dojet	k5eAaPmAgMnS
čtvrtý	čtvrtý	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnPc1
pódium	pódium	k1gNnSc1
přišlo	přijít	k5eAaPmAgNnS
ve	v	k7c6
čtvrtém	čtvrtý	k4xOgInSc6
závodě	závod	k1gInSc6
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
Alonso	Alonsa	k1gFnSc5
skončil	skončit	k5eAaPmAgInS
třetí	třetí	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
úspěch	úspěch	k1gInSc1
přišel	přijít	k5eAaPmAgInS
až	až	k9
o	o	k7c4
sedm	sedm	k4xCc4
závodů	závod	k1gInPc2
později	pozdě	k6eAd2
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Alonso	Alonsa	k1gFnSc5
část	část	k1gFnSc4
závodu	závod	k1gInSc2
vedl	vést	k5eAaImAgInS
a	a	k8xC
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
kolech	kolo	k1gNnPc6
odolával	odolávat	k5eAaImAgMnS
útokům	útok	k1gInPc3
ze	z	k7c2
strany	strana	k1gFnSc2
Daniela	Daniel	k1gMnSc2
Ricciarda	Ricciard	k1gMnSc2
z	z	k7c2
Red	Red	k1gFnSc2
Bullu	bulla	k1gFnSc4
a	a	k8xC
obou	dva	k4xCgInPc2
Mercedesů	mercedes	k1gInPc2
Lewise	Lewise	k1gFnSc2
Hamiltona	Hamiltona	k1gFnSc1
a	a	k8xC
Nico	Nico	k1gMnSc1
Rosberga	Rosberga	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ricciardo	Ricciardo	k1gNnSc1
ho	on	k3xPp3gMnSc4
však	však	k9
dokázal	dokázat	k5eAaPmAgMnS
předjet	předjet	k5eAaPmF
a	a	k8xC
tak	tak	k6eAd1
Alonso	Alonsa	k1gFnSc5
skončil	skončit	k5eAaPmAgInS
druhý	druhý	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
konce	konec	k1gInSc2
sezóny	sezóna	k1gFnSc2
se	se	k3xPyFc4
Alonso	Alonsa	k1gFnSc5
na	na	k7c4
pódium	pódium	k1gNnSc4
už	už	k6eAd1
nedostal	dostat	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skončil	skončit	k5eAaPmAgMnS
na	na	k7c6
celkovém	celkový	k2eAgNnSc6d1
šestém	šestý	k4xOgNnSc6
místě	místo	k1gNnSc6
s	s	k7c7
161	#num#	k4
body	bod	k1gInPc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
naprosto	naprosto	k6eAd1
"	"	kIx"
<g/>
rozdrtil	rozdrtit	k5eAaPmAgMnS
<g/>
"	"	kIx"
svého	svůj	k3xOyFgMnSc4
týmového	týmový	k2eAgMnSc4d1
kolegu	kolega	k1gMnSc4
Kimiho	Kimi	k1gMnSc4
Raikkonena	Raikkonen	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
měl	mít	k5eAaImAgMnS
bodů	bod	k1gInPc2
jen	jen	k9
55	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2015	#num#	k4
<g/>
–	–	k?
<g/>
:	:	kIx,
McLaren	McLarna	k1gFnPc2
</s>
<s>
2015	#num#	k4
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
2014	#num#	k4
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
Alonso	Alonsa	k1gFnSc5
u	u	k7c2
Ferrari	Ferrari	k1gMnSc2
končí	končit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6
Alonso	Alonsa	k1gFnSc5
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
vrací	vracet	k5eAaImIp3nS
k	k	k7c3
McLarenu	McLareno	k1gNnSc3
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
poháněným	poháněný	k2eAgInSc7d1
motorem	motor	k1gInSc7
Honda	honda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
druhého	druhý	k4xOgInSc2
dne	den	k1gInSc2
předsezónních	předsezónní	k2eAgInPc2d1
testů	test	k1gInPc2
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
Alonso	Alonsa	k1gFnSc5
havaroval	havarovat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
letecky	letecky	k6eAd1
převezen	převézt	k5eAaPmNgMnS
do	do	k7c2
nemocnice	nemocnice	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgMnS
zjištěn	zjištěn	k2eAgInSc4d1
otřes	otřes	k1gInSc4
mozku	mozek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
mu	on	k3xPp3gMnSc3
doporučeno	doporučit	k5eAaPmNgNnS
vynechat	vynechat	k5eAaPmF
první	první	k4xOgInSc1
závod	závod	k1gInSc1
sezóny	sezóna	k1gFnSc2
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
také	také	k9
udělal	udělat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
nahrazen	nahradit	k5eAaPmNgInS
Kevinem	Kevin	k1gMnSc7
Magnussenem	Magnussen	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Austrálii	Austrálie	k1gFnSc6
se	se	k3xPyFc4
McLaren	McLarna	k1gFnPc2
velmi	velmi	k6eAd1
trápil	trápit	k5eAaImAgMnS
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Alonsův	Alonsův	k2eAgInSc1d1
bývalý	bývalý	k2eAgInSc1d1
tým	tým	k1gInSc1
Ferrari	Ferrari	k1gMnSc2
skončil	skončit	k5eAaPmAgInS
na	na	k7c6
pódiu	pódium	k1gNnSc6
díky	díky	k7c3
třetímu	třetí	k4xOgMnSc3
místu	místo	k1gNnSc3
Sebastiana	Sebastian	k1gMnSc2
Vettela	Vettel	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
Alonsa	Alonsa	k1gFnSc1
ve	v	k7c6
Ferrari	ferrari	k1gNnSc6
nahradil	nahradit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
to	ten	k3xDgNnSc4
Alonso	Alonsa	k1gFnSc5
zareagoval	zareagovat	k5eAaPmAgMnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
po	po	k7c6
14	#num#	k4
letech	léto	k1gNnPc6
ve	v	k7c6
Formuli	formule	k1gFnSc6
1	#num#	k4
není	být	k5eNaImIp3nS
spokojen	spokojen	k2eAgMnSc1d1
s	s	k7c7
pódii	pódium	k1gNnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
že	že	k8xS
je	být	k5eAaImIp3nS
připraven	připravit	k5eAaPmNgMnS
riskovat	riskovat	k5eAaBmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vyhrával	vyhrávat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Malajsii	Malajsie	k1gFnSc6
<g/>
,	,	kIx,
prvním	první	k4xOgInSc6
závodě	závod	k1gInSc6
sezóny	sezóna	k1gFnSc2
pro	pro	k7c4
Alonsa	Alons	k1gMnSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
porouchalo	porouchat	k5eAaPmAgNnS
auto	auto	k1gNnSc4
a	a	k8xC
musel	muset	k5eAaImAgMnS
odstoupit	odstoupit	k5eAaPmF
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Vettel	Vettel	k1gMnSc1
s	s	k7c7
Ferrari	Ferrari	k1gMnSc7
vyhrál	vyhrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alonso	Alonsa	k1gFnSc5
se	se	k3xPyFc4
vyjádřil	vyjádřit	k5eAaPmAgMnS
i	i	k9
k	k	k7c3
této	tento	k3xDgFnSc3
události	událost	k1gFnSc3
<g/>
,	,	kIx,
když	když	k8xS
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
od	od	k7c2
Ferrari	Ferrari	k1gMnSc2
odešel	odejít	k5eAaPmAgMnS
<g/>
,	,	kIx,
přestože	přestože	k8xS
mu	on	k3xPp3gMnSc3
slibovali	slibovat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
auto	auto	k1gNnSc1
pro	pro	k7c4
rok	rok	k1gInSc4
2015	#num#	k4
bude	být	k5eAaImBp3nS
mnohem	mnohem	k6eAd1
lepší	dobrý	k2eAgNnSc1d2
než	než	k8xS
to	ten	k3xDgNnSc1
z	z	k7c2
minulého	minulý	k2eAgInSc2d1
roku	rok	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
on	on	k3xPp3gMnSc1
to	ten	k3xDgNnSc4
Ferrari	ferrari	k1gNnSc4
už	už	k6eAd1
nevěřil	věřit	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taky	taky	k6eAd1
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
přestupu	přestup	k1gInSc3
litoval	litovat	k5eAaImAgMnS
jen	jen	k9
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
kdyby	kdyby	kYmCp3nS
Ferrari	ferrari	k1gNnSc1
nakonec	nakonec	k6eAd1
vyhrálo	vyhrát	k5eAaPmAgNnS
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalším	další	k2eAgInSc6d1
závodě	závod	k1gInSc6
v	v	k7c6
Číně	Čína	k1gFnSc6
opět	opět	k6eAd1
dojel	dojet	k5eAaPmAgMnS
mimo	mimo	k7c4
body	bod	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
šťastný	šťastný	k2eAgMnSc1d1
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
dojel	dojet	k5eAaPmAgMnS
a	a	k8xC
tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
získat	získat	k5eAaPmF
informace	informace	k1gFnPc4
podstatné	podstatný	k2eAgFnPc4d1
pro	pro	k7c4
vývoj	vývoj	k1gInSc4
auta	auto	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dalších	další	k2eAgInPc2d1
čtyř	čtyři	k4xCgInPc2
závodů	závod	k1gInPc2
odstoupil	odstoupit	k5eAaPmAgInS
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
problémům	problém	k1gInPc3
s	s	k7c7
nespolehlivým	spolehlivý	k2eNgInSc7d1
motorem	motor	k1gInSc7
Honda	honda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Velké	velká	k1gFnSc2
Británie	Británie	k1gFnSc1
bral	brát	k5eAaImAgMnS
Alonso	Alonsa	k1gFnSc5
první	první	k4xOgFnSc6
bod	bod	k1gInSc1
za	za	k7c4
desáté	desátý	k4xOgNnSc4
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
v	v	k7c6
následujícím	následující	k2eAgInSc6d1
závodě	závod	k1gInSc6
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
bral	brát	k5eAaImAgMnS
Alonso	Alonsa	k1gFnSc5
další	další	k2eAgInSc4d1
body	bod	k1gInPc4
za	za	k7c4
páté	pátý	k4xOgNnSc4
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalších	další	k2eAgInPc6d1
závodech	závod	k1gInPc6
už	už	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
však	však	k9
bodovat	bodovat	k5eAaImF
nepodařilo	podařit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezónu	sezóna	k1gFnSc4
tedy	tedy	k8xC
dokončil	dokončit	k5eAaPmAgMnS
s	s	k7c7
pouhými	pouhý	k2eAgNnPc7d1
11	#num#	k4
body	bod	k1gInPc4
na	na	k7c6
celkovém	celkový	k2eAgInSc6d1
17	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
</s>
<s>
Během	během	k7c2
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Austrálie	Austrálie	k1gFnSc2
Alonso	Alonsa	k1gFnSc5
kolidoval	kolidovat	k5eAaImAgInS
s	s	k7c7
Haasem	Haaso	k1gNnSc7
Estebana	Esteban	k1gMnSc2
Gutiérreze	Gutiérreze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejtěžších	těžký	k2eAgFnPc2d3
havárií	havárie	k1gFnPc2
za	za	k7c4
poslední	poslední	k2eAgNnPc4d1
léta	léto	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyvázl	vyváznout	k5eAaPmAgInS
se	s	k7c7
zlomeninami	zlomenina	k1gFnPc7
žeber	žebro	k1gNnPc2
a	a	k8xC
částečným	částečný	k2eAgNnSc7d1
selháním	selhání	k1gNnSc7
plic	plíce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
ke	k	k7c3
zraněním	zranění	k1gNnPc3
byl	být	k5eAaImAgInS
nucen	nutit	k5eAaImNgMnS
vynechat	vynechat	k5eAaPmF
další	další	k2eAgFnSc4d1
Velkou	velký	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
v	v	k7c6
Bahrajnu	Bahrajn	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
nahrazen	nahradit	k5eAaPmNgInS
Stoffelem	Stoffel	k1gMnSc7
Vandoornem	Vandoorn	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrátil	vrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
dojel	dojet	k5eAaPmAgMnS
dvanáctý	dvanáctý	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalším	další	k2eAgInSc6d1
závodě	závod	k1gInSc6
v	v	k7c6
Rusku	Rusko	k1gNnSc6
dojel	dojet	k5eAaPmAgMnS
šestý	šestý	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
na	na	k7c4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Španělska	Španělsko	k1gNnSc2
se	se	k3xPyFc4
Alonso	Alonsa	k1gFnSc5
dostal	dostat	k5eAaPmAgInS
do	do	k7c2
třetí	třetí	k4xOgFnSc2
části	část	k1gFnSc2
kvalifikace	kvalifikace	k1gFnSc2
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
od	od	k7c2
spojení	spojení	k1gNnSc2
McLarenu	McLaren	k1gInSc2
a	a	k8xC
Hondy	honda	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
závodě	závod	k1gInSc6
však	však	k9
odstoupil	odstoupit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Monaku	Monako	k1gNnSc6
se	se	k3xPyFc4
opět	opět	k6eAd1
probojoval	probojovat	k5eAaPmAgMnS
do	do	k7c2
třetí	třetí	k4xOgFnSc2
části	část	k1gFnSc2
kvalifikace	kvalifikace	k1gFnSc2
a	a	k8xC
po	po	k7c6
penalizaci	penalizace	k1gFnSc6
Raikkonena	Raikkoneno	k1gNnSc2
startoval	startovat	k5eAaBmAgInS
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závodě	závod	k1gInSc6
dojel	dojet	k5eAaPmAgMnS
5	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
vyrovnal	vyrovnat	k5eAaBmAgMnS,k5eAaPmAgMnS
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
nejlepší	dobrý	k2eAgInSc4d3
výsledek	výsledek	k1gInSc4
pro	pro	k7c4
McLaren	McLarna	k1gFnPc2
poháněný	poháněný	k2eAgInSc4d1
Hondou	Honda	k1gMnSc7
z	z	k7c2
Maďarska	Maďarsko	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Kanadě	Kanada	k1gFnSc6
se	se	k3xPyFc4
během	během	k7c2
kvalifikace	kvalifikace	k1gFnSc2
opět	opět	k6eAd1
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
třetí	třetí	k4xOgFnSc2
části	část	k1gFnSc2
<g/>
,	,	kIx,
potřetí	potřetí	k4xO
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
,	,	kIx,
v	v	k7c6
závodě	závod	k1gInSc6
však	však	k9
dojel	dojet	k5eAaPmAgMnS
těsně	těsně	k6eAd1
mimo	mimo	k7c4
body	bod	k1gInPc4
<g/>
,	,	kIx,
skončil	skončit	k5eAaPmAgMnS
totiž	totiž	k9
jedenáctý	jedenáctý	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
následující	následující	k2eAgFnSc2d1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Evropy	Evropa	k1gFnSc2
odstoupil	odstoupit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Kontroverzní	kontroverzní	k2eAgFnSc1d1
situace	situace	k1gFnSc1
</s>
<s>
Při	při	k7c6
Velké	velký	k2eAgFnSc6d1
ceně	cena	k1gFnSc6
Evropy	Evropa	k1gFnSc2
2003	#num#	k4
obvinili	obvinit	k5eAaPmAgMnP
David	David	k1gMnSc1
Coulthard	Coulthard	k1gMnSc1
<g/>
,	,	kIx,
tým	tým	k1gInSc1
McLaren	McLarna	k1gFnPc2
a	a	k8xC
generální	generální	k2eAgMnSc1d1
manažer	manažer	k1gMnSc1
Martin	Martin	k1gMnSc1
Whitmarsh	Whitmarsh	k1gMnSc1
Alonsa	Alonsa	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
závodě	závod	k1gInSc6
brzdil	brzdit	k5eAaImAgMnS
Coultharda	Coulthard	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
incidentu	incident	k1gInSc6
z	z	k7c2
tréninku	trénink	k1gInSc2
byl	být	k5eAaImAgInS
Michael	Michael	k1gMnSc1
Schumacher	Schumachra	k1gFnPc2
tázán	tázat	k5eAaImNgInS
<g/>
,	,	kIx,
jestli	jestli	k8xS
Alonso	Alonsa	k1gFnSc5
zpomalil	zpomalit	k5eAaPmAgInS
úmyslně	úmyslně	k6eAd1
a	a	k8xC
Schumacher	Schumachra	k1gFnPc2
ho	on	k3xPp3gNnSc4
tak	tak	k6eAd1
předjel	předjet	k5eAaPmAgMnS
při	při	k7c6
červených	červený	k2eAgFnPc6d1
vlajkách	vlajka	k1gFnPc6
v	v	k7c6
tréninku	trénink	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schumacher	Schumachra	k1gFnPc2
odpověděl	odpovědět	k5eAaPmAgInS
<g/>
:	:	kIx,
„	„	k?
<g/>
To	to	k9
jste	být	k5eAaImIp2nP
řekl	říct	k5eAaPmAgMnS
vy	vy	k3xPp2nPc1
<g/>
,	,	kIx,
já	já	k3xPp1nSc1
ne	ne	k9
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Při	při	k7c6
Velké	velký	k2eAgFnSc6d1
ceně	cena	k1gFnSc6
Itálie	Itálie	k1gFnSc1
2006	#num#	k4
Alonso	Alonsa	k1gFnSc5
blokoval	blokovat	k5eAaImAgInS
Felipeho	Felipe	k1gMnSc4
Massu	Massa	k1gFnSc4
při	při	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
byl	být	k5eAaImAgMnS
odsunut	odsunout	k5eAaPmNgMnS
o	o	k7c4
pět	pět	k4xCc4
míst	místo	k1gNnPc2
dozadu	dozadu	k6eAd1
na	na	k7c6
startovním	startovní	k2eAgInSc6d1
roštu	rošt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
na	na	k7c4
Velkou	velký	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
Maďarska	Maďarsko	k1gNnSc2
2007	#num#	k4
úmyslně	úmyslně	k6eAd1
zablokoval	zablokovat	k5eAaPmAgInS
výjezd	výjezd	k1gInSc1
z	z	k7c2
boxu	box	k1gInSc2
týmovému	týmový	k2eAgMnSc3d1
kolegovi	kolega	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
tak	tak	k6eAd1
nemohl	moct	k5eNaImAgMnS
zajet	zajet	k5eAaPmF
poslední	poslední	k2eAgNnSc4d1
měřené	měřený	k2eAgNnSc4d1
kolo	kolo	k1gNnSc4
<g/>
,	,	kIx,
za	za	k7c4
což	což	k3yQnSc4,k3yRnSc4
byl	být	k5eAaImAgInS
od	od	k7c2
FIA	FIA	kA
potrestán	potrestat	k5eAaPmNgMnS
posunutím	posunutí	k1gNnSc7
5	#num#	k4
míst	místo	k1gNnPc2
na	na	k7c6
startovním	startovní	k2eAgInSc6d1
roštu	rošt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
Velké	velký	k2eAgFnSc6d1
ceně	cena	k1gFnSc6
Singapuru	Singapur	k1gInSc2
2008	#num#	k4
tým	tým	k1gInSc1
Renault	renault	k1gInSc4
jehož	jehož	k3xOyRp3gInSc4
byl	být	k5eAaImAgInS
Alonso	Alonsa	k1gFnSc5
členem	člen	k1gMnSc7
<g/>
,	,	kIx,
zinscenoval	zinscenovat	k5eAaPmAgMnS
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
po	po	k7c6
Alonsově	Alonsův	k2eAgFnSc6d1
zastávce	zastávka	k1gFnSc6
v	v	k7c6
boxech	box	k1gInPc6
nehodu	nehoda	k1gFnSc4
týmové	týmový	k2eAgFnSc2d1
dvojky	dvojka	k1gFnSc2
Nelsona	Nelson	k1gMnSc4
Piqueta	Piquet	k1gMnSc4
juniora	junior	k1gMnSc4
<g/>
,	,	kIx,
po	po	k7c6
níž	jenž	k3xRgFnSc6
vyjel	vyjet	k5eAaPmAgInS
na	na	k7c4
trať	trať	k1gFnSc4
safety	safeta	k1gFnSc2
car	car	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alonso	Alonsa	k1gFnSc5
se	se	k3xPyFc4
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
z	z	k7c2
původně	původně	k6eAd1
patnáctého	patnáctý	k4xOgNnSc2
místa	místo	k1gNnSc2
na	na	k7c6
startu	start	k1gInSc6
dostal	dostat	k5eAaPmAgMnS
na	na	k7c4
první	první	k4xOgFnSc4
pozici	pozice	k1gFnSc4
a	a	k8xC
závod	závod	k1gInSc4
vyhrál	vyhrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vítězství	vítězství	k1gNnSc1
ve	v	k7c6
Formuli	formule	k1gFnSc6
1	#num#	k4
</s>
<s>
#	#	kIx~
</s>
<s>
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Monopost	monopost	k1gInSc1
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
1	#num#	k4
</s>
<s>
XIX	XIX	kA
Marlboro	Marlboro	k1gNnSc1
Magyar	Magyar	k1gMnSc1
Nagydíj	Nagydíj	k1gMnSc1
</s>
<s>
2003	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
R23	R23	k1gFnSc2
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
2	#num#	k4
</s>
<s>
VII	VII	kA
Petronas	Petronas	k1gMnSc1
Malaysian	Malaysian	k1gMnSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
</s>
<s>
2005	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
R25	R25	k1gFnSc2
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
3	#num#	k4
</s>
<s>
II	II	kA
Gulf	Gulf	k1gMnSc1
Air	Air	k1gMnSc1
Bahrain	Bahrain	k1gMnSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
</s>
<s>
2005	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
R25	R25	k1gFnSc2
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
4	#num#	k4
</s>
<s>
XXV	XXV	kA
Gran	Gran	k1gMnSc1
Premio	Premio	k1gMnSc1
Foster	Foster	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
di	di	k?
San	San	k1gFnSc7
Marino	Marina	k1gFnSc5
</s>
<s>
2005	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
R25	R25	k1gFnSc2
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
5	#num#	k4
</s>
<s>
XIV	XIV	kA
Formula	Formula	k1gFnSc1
1	#num#	k4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
of	of	k?
Europe	Europ	k1gMnSc5
</s>
<s>
2005	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
R25	R25	k1gFnSc2
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
6	#num#	k4
</s>
<s>
83	#num#	k4
<g/>
éme	éme	k?
Mobil	mobil	k1gInSc1
1	#num#	k4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
de	de	k?
France	Franc	k1gMnSc2
</s>
<s>
2005	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
R25	R25	k1gFnSc2
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
7	#num#	k4
</s>
<s>
LXVII	LXVII	kA
Großer	Großer	k1gInSc1
Mobil	mobil	k1gInSc1
1	#num#	k4
Preis	Preis	k1gFnSc2
von	von	k1gInSc1
Deutschland	Deutschland	k1gInSc1
</s>
<s>
2005	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
R25	R25	k1gFnSc2
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
8	#num#	k4
</s>
<s>
II	II	kA
Sinopec	Sinopec	k1gMnSc1
Chinese	Chinese	k1gFnSc2
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
</s>
<s>
2005	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
R25	R25	k1gFnSc2
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
9	#num#	k4
</s>
<s>
III	III	kA
Gulf	Gulf	k1gMnSc1
Air	Air	k1gMnSc1
Bahrain	Bahrain	k1gMnSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
</s>
<s>
2006	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
R26	R26	k1gFnSc2
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
10	#num#	k4
</s>
<s>
LXXI	LXXI	kA
Foster	Foster	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Australian	Australian	k1gMnSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
</s>
<s>
2006	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
R26	R26	k1gFnSc2
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
11	#num#	k4
</s>
<s>
XLVIII	XLVIII	kA
Gran	Gran	k1gMnSc1
Premio	Premio	k1gMnSc1
Telefónica	Telefónica	k1gMnSc1
de	de	k?
Españ	Españ	k1gMnSc1
</s>
<s>
2006	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
R26	R26	k1gFnSc2
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
12	#num#	k4
</s>
<s>
LXIV	LXIV	kA
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
de	de	k?
Monaco	Monaco	k6eAd1
</s>
<s>
2006	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
R26	R26	k1gFnSc2
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
13	#num#	k4
</s>
<s>
60	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formula	Formula	k1gFnSc1
1	#num#	k4
Foster	Fostrum	k1gNnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
British	British	k1gMnSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
</s>
<s>
2006	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
R26	R26	k1gFnSc2
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
14	#num#	k4
</s>
<s>
XLIII	XLIII	kA
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
du	du	k?
Canada	Canada	k1gFnSc1
</s>
<s>
2006	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
R26	R26	k1gFnSc2
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
15	#num#	k4
</s>
<s>
XXXII	XXXII	kA
Fuji	Fuji	kA
Television	Television	k1gInSc1
Japanese	Japanese	k1gFnSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
</s>
<s>
2006	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
R26	R26	k1gFnSc2
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
16	#num#	k4
</s>
<s>
IX	IX	kA
Petronas	Petronas	k1gMnSc1
Malaysian	Malaysian	k1gMnSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
</s>
<s>
2007	#num#	k4
</s>
<s>
McLaren	McLarna	k1gFnPc2
MP4-22	MP4-22	k1gFnSc2
</s>
<s>
Vodafone	Vodafon	k1gInSc5
McLaren	McLarna	k1gFnPc2
Mercedes	mercedes	k1gInSc1
</s>
<s>
17	#num#	k4
</s>
<s>
LXV	LXV	kA
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
de	de	k?
Monaco	Monaco	k6eAd1
</s>
<s>
2007	#num#	k4
</s>
<s>
McLaren	McLarna	k1gFnPc2
MP4-22	MP4-22	k1gFnPc2
</s>
<s>
Vodafone	Vodafon	k1gInSc5
McLaren	McLarno	k1gNnPc2
Mercedes	mercedes	k1gInSc1
</s>
<s>
18	#num#	k4
</s>
<s>
LI	li	k8xS
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
of	of	k?
Europe	Europ	k1gInSc5
</s>
<s>
2007	#num#	k4
</s>
<s>
McLaren	McLarna	k1gFnPc2
MP4-22	MP4-22	k1gFnSc2
</s>
<s>
Vodafone	Vodafon	k1gInSc5
McLaren	McLarno	k1gNnPc2
Mercedes	mercedes	k1gInSc1
</s>
<s>
19	#num#	k4
</s>
<s>
LXXVIII	LXXVIII	kA
Gran	Gran	k1gMnSc1
Premio	Premio	k1gMnSc1
Santander	Santander	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Italia	Italius	k1gMnSc2
</s>
<s>
2007	#num#	k4
</s>
<s>
McLaren	McLarna	k1gFnPc2
MP4-22	MP4-22	k1gFnSc2
</s>
<s>
Vodafone	Vodafon	k1gInSc5
McLaren	McLarna	k1gFnPc2
Mercedes	mercedes	k1gInSc1
</s>
<s>
20	#num#	k4
</s>
<s>
IX	IX	kA
Formula	Formula	k1gFnSc1
1	#num#	k4
SingTel	SingTela	k1gFnPc2
Singapore	Singapor	k1gInSc5
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
</s>
<s>
2008	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
R28	R28	k1gFnSc2
</s>
<s>
ING	ing	kA
Renault	renault	k1gInSc1
F1	F1	k1gMnSc1
Team	team	k1gInSc1
</s>
<s>
21	#num#	k4
</s>
<s>
XXXIV	XXXIV	kA
Fuji	Fuji	kA
Television	Television	k1gInSc1
Japanese	Japanese	k1gFnSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
</s>
<s>
2008	#num#	k4
</s>
<s>
Renault	renault	k1gInSc1
R28	R28	k1gFnSc2
</s>
<s>
ING	ing	kA
Renault	renault	k1gInSc1
F1	F1	k1gMnSc1
Team	team	k1gInSc1
</s>
<s>
22	#num#	k4
</s>
<s>
VII	VII	kA
Gulf	Gulf	k1gMnSc1
Air	Air	k1gMnSc1
Bahrain	Bahrain	k1gMnSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
</s>
<s>
2010	#num#	k4
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
F10	F10	k1gMnSc1
</s>
<s>
Scuderia	Scuderium	k1gNnSc2
Ferrari	Ferrari	k1gMnPc2
Marlboro	Marlboro	k1gNnSc2
</s>
<s>
23	#num#	k4
</s>
<s>
71	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formula	Formula	k1gFnSc1
1	#num#	k4
Großer	Großra	k1gFnPc2
Preis	Preis	k1gFnSc1
Santander	Santander	k1gInSc1
von	von	k1gInSc1
Deutschland	Deutschlanda	k1gFnPc2
</s>
<s>
2010	#num#	k4
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
F10	F10	k1gMnSc1
</s>
<s>
Scuderia	Scuderium	k1gNnSc2
Ferrari	Ferrari	k1gMnPc2
Marlboro	Marlboro	k1gNnSc2
</s>
<s>
24	#num#	k4
</s>
<s>
LXXXI	LXXXI	kA
Gran	Gran	k1gMnSc1
Premio	Premio	k1gMnSc1
Santander	Santander	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Italia	Italius	k1gMnSc2
</s>
<s>
2010	#num#	k4
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
F10	F10	k1gMnSc1
</s>
<s>
Scuderia	Scuderium	k1gNnSc2
Ferrari	Ferrari	k1gMnPc2
Marlboro	Marlboro	k1gNnSc2
</s>
<s>
25	#num#	k4
</s>
<s>
XI	XI	kA
Formula	Formula	k1gFnSc1
1	#num#	k4
SingTel	SingTela	k1gFnPc2
Singapore	Singapor	k1gInSc5
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
</s>
<s>
2010	#num#	k4
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
F10	F10	k1gMnSc1
</s>
<s>
Scuderia	Scuderium	k1gNnSc2
Ferrari	Ferrari	k1gMnPc2
Marlboro	Marlboro	k1gNnSc2
</s>
<s>
26	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formula	Formula	k1gFnSc1
1	#num#	k4
Korean	Korean	k1gMnSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
</s>
<s>
2010	#num#	k4
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
F10	F10	k1gMnSc1
</s>
<s>
Scuderia	Scuderium	k1gNnSc2
Ferrari	Ferrari	k1gMnPc2
Marlboro	Marlboro	k1gNnSc2
</s>
<s>
27	#num#	k4
</s>
<s>
66	#num#	k4
<g/>
th	th	k?
Santander	Santander	k1gMnSc1
British	British	k1gMnSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
</s>
<s>
2011	#num#	k4
</s>
<s>
Ferrari	ferrari	k1gNnSc1
150	#num#	k4
Italia	Italium	k1gNnSc2
</s>
<s>
Scuderia	Scuderium	k1gNnSc2
Ferrari	Ferrari	k1gMnPc2
Marlboro	Marlboro	k1gNnSc2
</s>
<s>
28	#num#	k4
</s>
<s>
XIV	XIV	kA
Formula	Formula	k1gFnSc1
1	#num#	k4
Petronas	Petronas	k1gInSc1
Malaysia	Malaysia	k1gFnSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
</s>
<s>
2012	#num#	k4
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
F2012	F2012	k1gMnSc1
</s>
<s>
Scuderia	Scuderium	k1gNnPc1
Ferrari	ferrari	k1gNnSc2
</s>
<s>
29	#num#	k4
</s>
<s>
22	#num#	k4
<g/>
nd	nd	k?
Formula	Formula	k1gFnSc1
1	#num#	k4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
of	of	k?
Europe	Europ	k1gMnSc5
</s>
<s>
2012	#num#	k4
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
F2012	F2012	k1gMnSc1
</s>
<s>
Scuderia	Scuderium	k1gNnPc1
Ferrari	ferrari	k1gNnSc2
</s>
<s>
30	#num#	k4
</s>
<s>
73	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formula	Formula	k1gFnSc1
1	#num#	k4
Großer	Großra	k1gFnPc2
Preis	Preis	k1gFnSc1
Santander	Santander	k1gInSc1
von	von	k1gInSc1
Deutschland	Deutschlanda	k1gFnPc2
</s>
<s>
2012	#num#	k4
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
F2012	F2012	k1gMnSc1
</s>
<s>
Scuderia	Scuderium	k1gNnPc1
Ferrari	ferrari	k1gNnSc2
</s>
<s>
31	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formula	Formula	k1gFnSc1
1	#num#	k4
UBS	UBS	kA
Chinese	Chinese	k1gFnSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
</s>
<s>
2013	#num#	k4
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
F138	F138	k1gMnSc1
</s>
<s>
Scuderia	Scuderium	k1gNnPc1
Ferrari	ferrari	k1gNnSc2
</s>
<s>
32	#num#	k4
</s>
<s>
54	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formula	Formula	k1gFnSc1
1	#num#	k4
Gran	Gran	k1gMnSc1
Premio	Premio	k1gMnSc1
de	de	k?
Españ	Españ	k1gMnSc1
</s>
<s>
2013	#num#	k4
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
F138	F138	k1gMnSc1
</s>
<s>
Scuderia	Scuderium	k1gNnPc1
Ferrari	ferrari	k1gNnPc2
</s>
<s>
Kompletní	kompletní	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
ve	v	k7c6
Formuli	formule	k1gFnSc6
1	#num#	k4
</s>
<s>
Legenda	legenda	k1gFnSc1
k	k	k7c3
tabulce	tabulka	k1gFnSc3
</s>
<s>
Barva	barva	k1gFnSc1
</s>
<s>
Výsledek	výsledek	k1gInSc1
</s>
<s>
ZlatáVítěz	ZlatáVítěz	k1gMnSc1
</s>
<s>
Stříbrná	stříbrná	k1gFnSc1
<g/>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Bronzová	bronzový	k2eAgFnSc1d1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
ZelenáBodované	ZelenáBodovaný	k2eAgNnSc1d1
umístění	umístění	k1gNnSc1
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1
</s>
<s>
Nebodované	bodovaný	k2eNgNnSc1d1
umístění	umístění	k1gNnSc1
</s>
<s>
Dokončil	dokončit	k5eAaPmAgMnS
neklasifikován	klasifikován	k2eNgMnSc1d1
(	(	kIx(
<g/>
NC	NC	kA
<g/>
)	)	kIx)
</s>
<s>
FialováOdstoupil	FialováOdstoupit	k5eAaPmAgInS
(	(	kIx(
<g/>
Ret	ret	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Červená	červený	k2eAgFnSc1d1
</s>
<s>
Nekvalifikoval	kvalifikovat	k5eNaBmAgMnS
se	s	k7c7
(	(	kIx(
<g/>
DNQ	DNQ	kA
<g/>
)	)	kIx)
</s>
<s>
Nepředkvalifikoval	Nepředkvalifikovat	k5eAaImAgMnS,k5eAaPmAgMnS,k5eAaBmAgMnS
se	s	k7c7
(	(	kIx(
<g/>
DNPQ	DNPQ	kA
<g/>
)	)	kIx)
</s>
<s>
ČernáDiskvalifikován	ČernáDiskvalifikován	k2eAgMnSc1d1
(	(	kIx(
<g/>
DSQ	DSQ	kA
<g/>
)	)	kIx)
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1
</s>
<s>
Nestartoval	startovat	k5eNaBmAgMnS
(	(	kIx(
<g/>
DNS	DNS	kA
<g/>
)	)	kIx)
</s>
<s>
Závod	závod	k1gInSc1
zrušen	zrušit	k5eAaPmNgInS
(	(	kIx(
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Světle	světle	k6eAd1
modrá	modrý	k2eAgFnSc1d1
</s>
<s>
Pouze	pouze	k6eAd1
trénoval	trénovat	k5eAaImAgMnS
(	(	kIx(
<g/>
PO	po	k7c6
<g/>
)	)	kIx)
</s>
<s>
Páteční	páteční	k2eAgMnSc1d1
testovací	testovací	k2eAgMnSc1d1
jezdec	jezdec	k1gMnSc1
(	(	kIx(
<g/>
TD	TD	kA
<g/>
)	)	kIx)
</s>
<s>
Bez	bez	k7c2
barvy	barva	k1gFnSc2
</s>
<s>
Netrénoval	trénovat	k5eNaImAgMnS
(	(	kIx(
<g/>
DNP	DNP	kA
<g/>
)	)	kIx)
</s>
<s>
Vyřazen	vyřazen	k2eAgMnSc1d1
(	(	kIx(
<g/>
EX	ex	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Nepřijel	přijet	k5eNaPmAgMnS
(	(	kIx(
<g/>
DNA	dna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Odvolal	odvolat	k5eAaPmAgMnS
účast	účast	k1gFnSc4
(	(	kIx(
<g/>
WD	WD	kA
<g/>
)	)	kIx)
</s>
<s>
Označení	označení	k1gNnSc1
</s>
<s>
Význam	význam	k1gInSc1
</s>
<s>
Tučnost	tučnost	k1gFnSc1
</s>
<s>
Pole	pole	k1gFnSc1
position	position	k1gInSc1
</s>
<s>
Kurzíva	kurzíva	k1gFnSc1
</s>
<s>
Nejrychlejší	rychlý	k2eAgNnSc1d3
kolo	kolo	k1gNnSc1
</s>
<s>
F	F	kA
</s>
<s>
FanBoost	FanBoost	k1gFnSc1
</s>
<s>
G	G	kA
</s>
<s>
Nejrychlejší	rychlý	k2eAgMnSc1d3
v	v	k7c6
kvalifikační	kvalifikační	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
</s>
<s>
†	†	k?
</s>
<s>
Jezdec	jezdec	k1gMnSc1
nedojel	dojet	k5eNaPmAgMnS
do	do	k7c2
cíle	cíl	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgMnS
klasifikován	klasifikovat	k5eAaImNgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
odjel	odjet	k5eAaPmAgMnS
více	hodně	k6eAd2
než	než	k8xS
90	#num#	k4
%	%	kIx~
délky	délka	k1gFnSc2
závodu	závod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
‡	‡	k?
</s>
<s>
Byl	být	k5eAaImAgMnS
udělován	udělován	k2eAgInSc4d1
poloviční	poloviční	k2eAgInSc4d1
počet	počet	k1gInSc4
bodů	bod	k1gInPc2
<g/>
,	,	kIx,
protože	protože	k8xS
bylo	být	k5eAaImAgNnS
odjeto	odjet	k2eAgNnSc1d1
méně	málo	k6eAd2
než	než	k8xS
75	#num#	k4
%	%	kIx~
délky	délka	k1gFnSc2
závodu	závod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Šasi	šasi	k1gNnSc1
</s>
<s>
Motor	motor	k1gInSc1
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
Body	bod	k1gInPc4
</s>
<s>
Umístění	umístění	k1gNnSc1
</s>
<s>
2001	#num#	k4
</s>
<s>
European	European	k1gMnSc1
Minardi	Minard	k1gMnPc1
F1	F1	k1gMnSc1
</s>
<s>
Minardi	Minard	k1gMnPc1
PS01	PS01	k1gFnSc2
</s>
<s>
European	European	k1gMnSc1
<g/>
(	(	kIx(
<g/>
Cosworth	Cosworth	k1gMnSc1
<g/>
)	)	kIx)
3,0	3,0	k4
V10	V10	k1gFnPc2
</s>
<s>
AUS12	AUS12	k4
</s>
<s>
MAL13	MAL13	k4
</s>
<s>
BRARet	BRARet	k1gMnSc1
</s>
<s>
SMRRet	SMRRet	k1gMnSc1
</s>
<s>
ESP13	ESP13	k4
</s>
<s>
AUTRet	AUTRet	k1gMnSc1
</s>
<s>
MONRet	MONRet	k1gMnSc1
</s>
<s>
CANRet	CANRet	k1gMnSc1
</s>
<s>
EUR14	EUR14	k4
</s>
<s>
FRA17	FRA17	k4
</s>
<s>
GBR16	GBR16	k4
</s>
<s>
GER10	GER10	k4
</s>
<s>
HUNRet	HUNRet	k1gMnSc1
</s>
<s>
BELRet	BELRet	k1gMnSc1
</s>
<s>
ITA13	ITA13	k4
</s>
<s>
USARet	USARet	k1gMnSc1
</s>
<s>
JPN11	JPN11	k4
</s>
<s>
0	#num#	k4
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2002	#num#	k4
<g/>
:	:	kIx,
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
se	se	k3xPyFc4
neúčastnil	účastnit	k5eNaImAgInS
Formule	formule	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
Renault	renault	k1gInSc1
R23	R23	k1gFnSc2
</s>
<s>
Renault	renault	k1gInSc1
RS23	RS23	k1gFnSc2
3,0	3,0	k4
V10	V10	k1gFnPc2
</s>
<s>
AUS7	AUS7	k4
</s>
<s>
MAL3	MAL3	k4
</s>
<s>
BRA3	BRA3	k4
</s>
<s>
SMR6	SMR6	k4
</s>
<s>
ESP2	ESP2	k4
</s>
<s>
AUTRet	AUTRet	k1gMnSc1
</s>
<s>
MON5	MON5	k4
</s>
<s>
CAN4	CAN4	k4
</s>
<s>
EUR4	EUR4	k4
</s>
<s>
FRARet	FRARet	k1gMnSc1
</s>
<s>
55	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Renault	renault	k1gInSc1
R23B	R23B	k1gFnSc2
</s>
<s>
GBRRet	GBRRet	k1gMnSc1
</s>
<s>
GER4	GER4	k4
</s>
<s>
HUN1	HUN1	k4
</s>
<s>
ITA8	ITA8	k4
</s>
<s>
USARet	USARet	k1gMnSc1
</s>
<s>
JPNRet	JPNRet	k1gMnSc1
</s>
<s>
2004	#num#	k4
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
Renault	renault	k1gInSc1
R24	R24	k1gFnSc2
</s>
<s>
Renault	renault	k1gInSc1
RS24	RS24	k1gFnSc2
3,0	3,0	k4
V10	V10	k1gFnPc2
</s>
<s>
AUS3	AUS3	k4
</s>
<s>
MAL7	MAL7	k4
</s>
<s>
BHR6	BHR6	k4
</s>
<s>
SMR4	SMR4	k4
</s>
<s>
ESP4	ESP4	k4
</s>
<s>
MONRet	MONRet	k1gMnSc1
</s>
<s>
EUR5	EUR5	k4
</s>
<s>
CANRet	CANRet	k1gMnSc1
</s>
<s>
USARet	USARet	k1gMnSc1
</s>
<s>
FRA2	FRA2	k4
</s>
<s>
GBR10	GBR10	k4
</s>
<s>
GER3	GER3	k4
</s>
<s>
HUN3	HUN3	k4
</s>
<s>
BELRet	BELRet	k1gMnSc1
</s>
<s>
ITARet	ITARet	k1gMnSc1
</s>
<s>
CHN4	CHN4	k4
</s>
<s>
JPN5	JPN5	k4
</s>
<s>
BRA4	BRA4	k4
</s>
<s>
59	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2005	#num#	k4
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
Renault	renault	k1gInSc1
R25	R25	k1gFnSc2
</s>
<s>
Renault	renault	k1gInSc1
RS25	RS25	k1gFnSc2
3,0	3,0	k4
V10	V10	k1gFnPc2
</s>
<s>
AUS3	AUS3	k4
</s>
<s>
MAL1	MAL1	k4
</s>
<s>
BHR1	BHR1	k4
</s>
<s>
SMR1	SMR1	k4
</s>
<s>
ESP2	ESP2	k4
</s>
<s>
MON4	MON4	k4
</s>
<s>
EUR1	EUR1	k4
</s>
<s>
CANRet	CANRet	k1gMnSc1
</s>
<s>
USADNS	USADNS	kA
</s>
<s>
FRA1	FRA1	k4
</s>
<s>
GBR2	GBR2	k4
</s>
<s>
GER1	GER1	k4
</s>
<s>
HUN11	HUN11	k4
</s>
<s>
TUR2	TUR2	k4
</s>
<s>
ITA2	ITA2	k4
</s>
<s>
BEL2	BEL2	k4
</s>
<s>
BRA3	BRA3	k4
</s>
<s>
JPN3	JPN3	k4
</s>
<s>
CHN1	CHN1	k4
</s>
<s>
133	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2006	#num#	k4
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
Renault	renault	k1gInSc1
R26	R26	k1gFnSc2
</s>
<s>
Renault	renault	k1gInSc1
RS26	RS26	k1gFnSc2
2,4	2,4	k4
V8	V8	k1gFnPc2
</s>
<s>
BHR1	BHR1	k4
</s>
<s>
MAL2	MAL2	k4
</s>
<s>
AUS1	AUS1	k4
</s>
<s>
SMR2	SMR2	k4
</s>
<s>
EUR2	EUR2	k4
</s>
<s>
ESP1	ESP1	k4
</s>
<s>
MON1	MON1	k4
</s>
<s>
GBR1	GBR1	k4
</s>
<s>
CAN1	CAN1	k4
</s>
<s>
USA5	USA5	k4
</s>
<s>
FRA2	FRA2	k4
</s>
<s>
GER5	GER5	k4
</s>
<s>
HUNRet	HUNRet	k1gMnSc1
</s>
<s>
TUR2	TUR2	k4
</s>
<s>
ITARet	ITARet	k1gMnSc1
</s>
<s>
CHN2	CHN2	k4
</s>
<s>
JPN1	JPN1	k4
</s>
<s>
BRA2	BRA2	k4
</s>
<s>
134	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2007	#num#	k4
</s>
<s>
Vodafone	Vodafon	k1gInSc5
McLaren	McLarna	k1gFnPc2
Mercedes	mercedes	k1gInSc1
</s>
<s>
McLaren	McLarna	k1gFnPc2
MP4-22	MP4-22	k1gFnPc2
</s>
<s>
Mercedes	mercedes	k1gInSc1
FO	FO	kA
108T	108T	k4
2,4	2,4	k4
V8	V8	k1gFnPc2
</s>
<s>
AUS2	AUS2	k4
</s>
<s>
MAL1	MAL1	k4
</s>
<s>
BHR5	BHR5	k4
</s>
<s>
ESP3	ESP3	k4
</s>
<s>
MON1	MON1	k4
</s>
<s>
CAN7	CAN7	k4
</s>
<s>
USA2	USA2	k4
</s>
<s>
FRA7	FRA7	k4
</s>
<s>
GBR2	GBR2	k4
</s>
<s>
EUR1	EUR1	k4
</s>
<s>
HUN4	HUN4	k4
</s>
<s>
TUR3	TUR3	k4
</s>
<s>
ITA1	ITA1	k4
</s>
<s>
BEL3	BEL3	k4
</s>
<s>
JPNRet	JPNRet	k1gMnSc1
</s>
<s>
CHN2	CHN2	k4
</s>
<s>
BRA3	BRA3	k4
</s>
<s>
109	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2008	#num#	k4
</s>
<s>
ING	ing	kA
Renault	renault	k1gInSc1
F1	F1	k1gMnSc1
Team	team	k1gInSc1
</s>
<s>
Renault	renault	k1gInSc1
R28	R28	k1gFnSc2
</s>
<s>
Renault	renault	k1gInSc1
RS27	RS27	k1gFnSc2
2,4	2,4	k4
V8	V8	k1gFnPc2
</s>
<s>
AUS4	AUS4	k4
</s>
<s>
MAL8	MAL8	k4
</s>
<s>
BHR10	BHR10	k4
</s>
<s>
ESPRet	ESPRet	k1gMnSc1
</s>
<s>
TUR6	TUR6	k4
</s>
<s>
MON10	MON10	k4
</s>
<s>
CANRet	CANRet	k1gMnSc1
</s>
<s>
FRA8	FRA8	k4
</s>
<s>
GBR6	GBR6	k4
</s>
<s>
GER11	GER11	k4
</s>
<s>
HUN4	HUN4	k4
</s>
<s>
EURRet	EURRet	k1gMnSc1
</s>
<s>
BEL4	BEL4	k4
</s>
<s>
ITA4	ITA4	k4
</s>
<s>
SIN1	SIN1	k4
</s>
<s>
JPN1	JPN1	k4
</s>
<s>
CHN4	CHN4	k4
</s>
<s>
BRA2	BRA2	k4
</s>
<s>
61	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2009	#num#	k4
</s>
<s>
ING	ing	kA
Renault	renault	k1gInSc1
F1	F1	k1gMnSc1
Team	team	k1gInSc1
</s>
<s>
Renault	renault	k1gInSc1
R29	R29	k1gFnSc2
</s>
<s>
Renault	renault	k1gInSc1
RS27	RS27	k1gFnSc2
2,4	2,4	k4
V8	V8	k1gFnPc2
</s>
<s>
AUS5	AUS5	k4
</s>
<s>
MAL11	MAL11	k4
</s>
<s>
CHN9	CHN9	k4
</s>
<s>
BHR8	BHR8	k4
</s>
<s>
ESP5	ESP5	k4
</s>
<s>
MON7	MON7	k4
</s>
<s>
TUR10	TUR10	k4
</s>
<s>
GBR14	GBR14	k4
</s>
<s>
GER7	GER7	k4
</s>
<s>
HUNRet	HUNRet	k1gMnSc1
</s>
<s>
EUR6	EUR6	k4
</s>
<s>
BELRet	BELRet	k1gMnSc1
</s>
<s>
ITA5	ITA5	k4
</s>
<s>
26	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
SIN3	SIN3	k4
</s>
<s>
JPN10	JPN10	k4
</s>
<s>
BRARet	BRARet	k1gMnSc1
</s>
<s>
ABU14	ABU14	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
Scuderia	Scuderium	k1gNnSc2
Ferrari	Ferrari	k1gMnPc2
Marlboro	Marlboro	k1gNnSc2
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
F10	F10	k1gMnSc1
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
056	#num#	k4
2,4	2,4	k4
V8	V8	k1gFnPc2
</s>
<s>
BHR1	BHR1	k4
</s>
<s>
AUS4	AUS4	k4
</s>
<s>
MAL	málit	k5eAaImRp2nS
<g/>
13	#num#	k4
<g/>
†	†	k?
</s>
<s>
CHN4	CHN4	k4
</s>
<s>
ESP2	ESP2	k4
</s>
<s>
MON6	MON6	k4
</s>
<s>
TUR8	TUR8	k4
</s>
<s>
CAN3	CAN3	k4
</s>
<s>
EUR8	EUR8	k4
</s>
<s>
GBR14	GBR14	k4
</s>
<s>
GER1	GER1	k4
</s>
<s>
HUN2	HUN2	k4
</s>
<s>
BELRet	BELRet	k1gMnSc1
</s>
<s>
ITA1	ITA1	k4
</s>
<s>
SIN1	SIN1	k4
</s>
<s>
JPN3	JPN3	k4
</s>
<s>
KOR1	KOR1	k4
</s>
<s>
BRA3	BRA3	k4
</s>
<s>
ABU7	ABU7	k4
</s>
<s>
252	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2011	#num#	k4
</s>
<s>
Scuderia	Scuderium	k1gNnSc2
Ferrari	Ferrari	k1gMnPc2
Marlboro	Marlboro	k1gNnSc2
</s>
<s>
Ferrari	Ferrari	k1gMnSc7
150	#num#	k4
<g/>
°	°	k?
Italia	Italia	k1gFnSc1
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
056	#num#	k4
2,4	2,4	k4
V8	V8	k1gFnPc2
</s>
<s>
AUS4	AUS4	k4
</s>
<s>
MAL6	MAL6	k4
</s>
<s>
CHN7	CHN7	k4
</s>
<s>
TUR3	TUR3	k4
</s>
<s>
ESP5	ESP5	k4
</s>
<s>
MON2	MON2	k4
</s>
<s>
CANRet	CANRet	k1gMnSc1
</s>
<s>
EUR2	EUR2	k4
</s>
<s>
257	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Scuderia	Scuderium	k1gNnPc1
Ferrari	ferrari	k1gNnPc2
</s>
<s>
GBR1	GBR1	k4
</s>
<s>
GER2	GER2	k4
</s>
<s>
HUN3	HUN3	k4
</s>
<s>
BEL4	BEL4	k4
</s>
<s>
ITA3	ITA3	k4
</s>
<s>
SIN4	SIN4	k4
</s>
<s>
JPN2	JPN2	k4
</s>
<s>
KOR5	KOR5	k4
</s>
<s>
IND3	IND3	k4
</s>
<s>
ABU2	ABU2	k4
</s>
<s>
BRA4	BRA4	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
Scuderia	Scuderium	k1gNnPc1
Ferrari	ferrari	k1gNnSc2
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
F2012	F2012	k1gMnSc1
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
056	#num#	k4
2,4	2,4	k4
V8	V8	k1gFnPc2
</s>
<s>
AUS5	AUS5	k4
</s>
<s>
MAL1	MAL1	k4
</s>
<s>
CHN9	CHN9	k4
</s>
<s>
BHR7	BHR7	k4
</s>
<s>
ESP2	ESP2	k4
</s>
<s>
MON3	MON3	k4
</s>
<s>
CAN5	CAN5	k4
</s>
<s>
EUR1	EUR1	k4
</s>
<s>
GBR2	GBR2	k4
</s>
<s>
GER1	GER1	k4
</s>
<s>
HUN5	HUN5	k4
</s>
<s>
BELRet	BELRet	k1gMnSc1
</s>
<s>
ITA3	ITA3	k4
</s>
<s>
SIN3	SIN3	k4
</s>
<s>
JPNRet	JPNRet	k1gMnSc1
</s>
<s>
KOR3	KOR3	k4
</s>
<s>
IND2	IND2	k4
</s>
<s>
ABU2	ABU2	k4
</s>
<s>
USA3	USA3	k4
</s>
<s>
BRA2	BRA2	k4
</s>
<s>
278	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2013	#num#	k4
</s>
<s>
Scuderia	Scuderium	k1gNnPc1
Ferrari	ferrari	k1gNnPc2
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
F138	F138	k1gMnSc1
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
056	#num#	k4
2,4	2,4	k4
V8	V8	k1gFnPc2
</s>
<s>
AUS2	AUS2	k4
</s>
<s>
MALRet	MALRet	k1gMnSc1
</s>
<s>
CHN1	CHN1	k4
</s>
<s>
BHR8	BHR8	k4
</s>
<s>
ESP1	ESP1	k4
</s>
<s>
MON7	MON7	k4
</s>
<s>
CAN2	CAN2	k4
</s>
<s>
GBR3	GBR3	k4
</s>
<s>
GER4	GER4	k4
</s>
<s>
HUN5	HUN5	k4
</s>
<s>
BEL2	BEL2	k4
</s>
<s>
ITA2	ITA2	k4
</s>
<s>
SIN2	SIN2	k4
</s>
<s>
KOR6	KOR6	k4
</s>
<s>
JPN4	JPN4	k4
</s>
<s>
IND11	IND11	k4
</s>
<s>
ABU5	ABU5	k4
</s>
<s>
USA5	USA5	k4
</s>
<s>
BRA3	BRA3	k4
</s>
<s>
242	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2014	#num#	k4
</s>
<s>
Scuderia	Scuderium	k1gNnPc1
Ferrari	Ferrari	k1gMnPc1
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
F14	F14	k1gFnSc2
T	T	kA
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
0	#num#	k4
<g/>
59	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
1,6	1,6	k4
V6	V6	k1gMnPc2
t	t	k?
</s>
<s>
AUS4	AUS4	k4
</s>
<s>
MAL4	MAL4	k4
</s>
<s>
BHR8	BHR8	k4
</s>
<s>
CHN3	CHN3	k4
</s>
<s>
ESP6	ESP6	k4
</s>
<s>
MON4	MON4	k4
</s>
<s>
CAN6	CAN6	k4
</s>
<s>
AUT5	AUT5	k4
</s>
<s>
GBR6	GBR6	k4
</s>
<s>
GER5	GER5	k4
</s>
<s>
HUN2	HUN2	k4
</s>
<s>
BEL7	BEL7	k4
</s>
<s>
ITARet	ITARet	k1gMnSc1
</s>
<s>
SIN4	SIN4	k4
</s>
<s>
JPNRet	JPNRet	k1gMnSc1
</s>
<s>
RUS6	RUS6	k4
</s>
<s>
USA6	USA6	k4
</s>
<s>
BRA6	BRA6	k4
</s>
<s>
ABU9	ABU9	k4
</s>
<s>
161	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2015	#num#	k4
</s>
<s>
McLaren	McLarna	k1gFnPc2
Honda	honda	k1gFnSc1
</s>
<s>
McLaren	McLarna	k1gFnPc2
MP4-30	MP4-30	k1gFnPc2
</s>
<s>
Honda	honda	k1gFnSc1
RA616H	RA616H	k1gFnSc2
1,6	1,6	k4
V6	V6	k1gMnSc1
t	t	k?
</s>
<s>
AUSDNS	AUSDNS	kA
</s>
<s>
MALRet	MALRet	k1gMnSc1
</s>
<s>
CHN12	CHN12	k4
</s>
<s>
BHR11	BHR11	k4
</s>
<s>
ESPRet	ESPRet	k1gMnSc1
</s>
<s>
MONRet	MONRet	k1gMnSc1
</s>
<s>
CANRet	CANRet	k1gMnSc1
</s>
<s>
AUTRet	AUTRet	k1gMnSc1
</s>
<s>
GBR10	GBR10	k4
</s>
<s>
HUN5	HUN5	k4
</s>
<s>
BEL13	BEL13	k4
</s>
<s>
ITA	ITA	kA
<g/>
18	#num#	k4
<g/>
†	†	k?
</s>
<s>
SINRet	SINRet	k1gMnSc1
</s>
<s>
JPN11	JPN11	k4
</s>
<s>
RUS11	RUS11	k4
</s>
<s>
USA11	USA11	k4
</s>
<s>
MEXRet	MEXRet	k1gMnSc1
</s>
<s>
BRA15	BRA15	k4
</s>
<s>
ABU17	ABU17	k4
</s>
<s>
11	#num#	k4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2016	#num#	k4
</s>
<s>
McLaren	McLarna	k1gFnPc2
Honda	honda	k1gFnSc1
</s>
<s>
McLaren	McLarna	k1gFnPc2
MP4-31	MP4-31	k1gFnPc2
</s>
<s>
Honda	honda	k1gFnSc1
RA616H	RA616H	k1gFnSc2
1,6	1,6	k4
V6	V6	k1gMnSc1
t	t	k?
</s>
<s>
AUSRet	AUSRet	k1gMnSc1
</s>
<s>
BHR	BHR	kA
</s>
<s>
CHN12	CHN12	k4
</s>
<s>
RUS6	RUS6	k4
</s>
<s>
ESPRet	ESPRet	k1gMnSc1
</s>
<s>
MON5	MON5	k4
</s>
<s>
CAN11	CAN11	k4
</s>
<s>
EURRet	EURRet	k1gMnSc1
</s>
<s>
AUT	aut	k1gInSc1
<g/>
18	#num#	k4
<g/>
†	†	k?
</s>
<s>
GBR13	GBR13	k4
</s>
<s>
HUN7	HUN7	k4
</s>
<s>
BEL12	BEL12	k4
</s>
<s>
BEL7	BEL7	k4
</s>
<s>
ITA14	ITA14	k4
</s>
<s>
SIN7	SIN7	k4
</s>
<s>
MAL7	MAL7	k4
</s>
<s>
JPN16	JPN16	k4
</s>
<s>
USA5	USA5	k4
</s>
<s>
MEX13	MEX13	k4
</s>
<s>
BRA10	BRA10	k4
</s>
<s>
ABU10	ABU10	k4
</s>
<s>
54	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2017	#num#	k4
</s>
<s>
McLaren	McLarna	k1gFnPc2
Honda	honda	k1gFnSc1
</s>
<s>
McLaren	McLarna	k1gFnPc2
MCL32	MCL32	k1gFnSc2
</s>
<s>
Honda	honda	k1gFnSc1
RA617H	RA617H	k1gFnSc2
1,6	1,6	k4
V6	V6	k1gMnSc1
t	t	k?
</s>
<s>
AUSRet	AUSRet	k1gMnSc1
</s>
<s>
CHNRet	CHNRet	k1gMnSc1
</s>
<s>
BHR	BHR	kA
<g/>
14	#num#	k4
<g/>
†	†	k?
</s>
<s>
RUSDNS	RUSDNS	kA
</s>
<s>
ESP12	ESP12	k4
</s>
<s>
MON	MON	kA
</s>
<s>
CAN	CAN	kA
<g/>
16	#num#	k4
<g/>
†	†	k?
</s>
<s>
AZE9	AZE9	k4
</s>
<s>
AUTRet	AUTRet	k1gMnSc1
</s>
<s>
GBRRet	GBRRet	k1gMnSc1
</s>
<s>
HUN6	HUN6	k4
</s>
<s>
BELRet	BELRet	k1gMnSc1
</s>
<s>
ITA	ITA	kA
<g/>
17	#num#	k4
<g/>
†	†	k?
</s>
<s>
SINRet	SINRet	k1gMnSc1
</s>
<s>
MAL11	MAL11	k4
</s>
<s>
JPN11	JPN11	k4
</s>
<s>
USARet	USARet	k1gMnSc1
</s>
<s>
MEX10	MEX10	k4
</s>
<s>
BRA8	BRA8	k4
</s>
<s>
ABU9	ABU9	k4
</s>
<s>
17	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2018	#num#	k4
</s>
<s>
McLaren	McLarna	k1gFnPc2
F1	F1	k1gFnSc1
Team	team	k1gInSc1
</s>
<s>
McLaren	McLarna	k1gFnPc2
MCL33	MCL33	k1gFnPc2
</s>
<s>
Renault	renault	k1gInSc1
R.	R.	kA
<g/>
E	E	kA
<g/>
.18	.18	k4
1,6	1,6	k4
V6	V6	k1gMnPc2
t	t	k?
</s>
<s>
AUS5	AUS5	k4
</s>
<s>
BHR7	BHR7	k4
</s>
<s>
CHN7	CHN7	k4
</s>
<s>
AZE7	AZE7	k4
</s>
<s>
ESP8	ESP8	k4
</s>
<s>
MONRet	MONRet	k1gMnSc1
</s>
<s>
CANRet	CANRet	k1gMnSc1
</s>
<s>
FRA	FRA	kA
<g/>
16	#num#	k4
<g/>
†	†	k?
</s>
<s>
AUT8	AUT8	k4
</s>
<s>
GBR8	GBR8	k4
</s>
<s>
GER	Gera	k1gFnPc2
<g/>
16	#num#	k4
<g/>
†	†	k?
</s>
<s>
HUN8	HUN8	k4
</s>
<s>
BELRet	BELRet	k1gMnSc1
</s>
<s>
ITARet	ITARet	k1gMnSc1
</s>
<s>
SIN7	SIN7	k4
</s>
<s>
RUS14	RUS14	k4
</s>
<s>
JPN14	JPN14	k4
</s>
<s>
USARet	USARet	k1gMnSc1
</s>
<s>
MEXRet	MEXRet	k1gMnSc1
</s>
<s>
BRA17	BRA17	k4
</s>
<s>
ABU11	ABU11	k4
</s>
<s>
50	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2019	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
<g/>
:	:	kIx,
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
se	se	k3xPyFc4
neúčastnil	účastnit	k5eNaImAgInS
Formule	formule	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2021	#num#	k4
</s>
<s>
Alpine	Alpinout	k5eAaPmIp3nS
F1	F1	k1gFnSc1
Team	team	k1gInSc1
</s>
<s>
Alpine	Alpinout	k5eAaPmIp3nS
A521	A521	k1gFnSc1
</s>
<s>
Renault	renault	k1gInSc1
E-Tech	E-Tech	k1gInSc1
20B	20B	k4
1,6	1,6	k4
V6	V6	k1gMnPc2
t	t	k?
</s>
<s>
BHRRet	BHRRet	k1gMnSc1
</s>
<s>
EMI10	EMI10	k4
</s>
<s>
POR8	POR8	k4
</s>
<s>
ESP	ESP	kA
</s>
<s>
MON	MON	kA
</s>
<s>
AZE	AZE	kA
</s>
<s>
TUR	tur	k1gMnSc1
</s>
<s>
FRA	FRA	kA
</s>
<s>
AUT	aut	k1gInSc1
</s>
<s>
GBR	GBR	kA
</s>
<s>
HUN	Hun	k1gMnSc1
</s>
<s>
BEL	bel	k1gInSc1
</s>
<s>
NED	NED	kA
</s>
<s>
ITA	ITA	kA
</s>
<s>
RUS	Rus	k1gFnSc1
</s>
<s>
SIN	sin	kA
</s>
<s>
JPN	JPN	kA
</s>
<s>
USA	USA	kA
</s>
<s>
MEX	MEX	kA
</s>
<s>
SAP	sapa	k1gFnPc2
</s>
<s>
AUS	AUS	kA
</s>
<s>
SAU	SAU	kA
</s>
<s>
ABU	ABU	kA
</s>
<s>
5	#num#	k4
<g/>
*	*	kIx~
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
*	*	kIx~
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
*	*	kIx~
Sezóna	sezóna	k1gFnSc1
v	v	k7c6
průběhu	průběh	k1gInSc6
</s>
<s>
Výsledky	výsledek	k1gInPc1
v	v	k7c6
ostatních	ostatní	k2eAgFnPc6d1
formulových	formulový	k2eAgFnPc6d1
kategoriích	kategorie	k1gFnPc6
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Série	série	k1gFnSc1
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Vůz	vůz	k1gInSc1
</s>
<s>
Závody	závod	k1gInPc1
</s>
<s>
Vítězství	vítězství	k1gNnSc1
</s>
<s>
Pole	pole	k1gFnSc1
Position	Position	k1gInSc1
</s>
<s>
Podium	podium	k1gNnSc1
</s>
<s>
Body	bod	k1gInPc4
</s>
<s>
Konečné	Konečné	k2eAgNnSc1d1
umístění	umístění	k1gNnSc1
</s>
<s>
1999	#num#	k4
</s>
<s>
Euro	euro	k1gNnSc1
Open	Opena	k1gFnPc2
Nissan	nissan	k1gInSc1
</s>
<s>
Adrian	Adrian	k1gMnSc1
Campos	Campos	k1gMnSc1
Motorsport	Motorsport	k1gInSc4
</s>
<s>
Coloni	Coloň	k1gFnSc3
CN	CN	kA
<g/>
1	#num#	k4
<g/>
-Nissan	-Nissany	k1gInPc2
</s>
<s>
15	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
164	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2000	#num#	k4
</s>
<s>
FIA	FIA	kA
Formula	Formula	k1gFnSc1
3000	#num#	k4
</s>
<s>
Team	team	k1gInSc1
Astromega	Astromeg	k1gMnSc2
</s>
<s>
Lola	Lola	k1gFnSc1
T	T	kA
<g/>
99	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
<g/>
-Zytek	-Zytka	k1gFnPc2
</s>
<s>
9	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Výsledky	výsledek	k1gInPc1
ve	v	k7c6
speciálních	speciální	k2eAgInPc6d1
závodech	závod	k1gInPc6
</s>
<s>
Výsledky	výsledek	k1gInPc1
v	v	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Monaka	Monako	k1gNnSc2
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Team	team	k1gInSc1
</s>
<s>
Vůz	vůz	k1gInSc1
</s>
<s>
Kvalifikace	kvalifikace	k1gFnSc1
</s>
<s>
Závod	závod	k1gInSc1
</s>
<s>
Šampionát	šampionát	k1gInSc1
</s>
<s>
2001	#num#	k4
</s>
<s>
European	European	k1gMnSc1
Minardi	Minard	k1gMnPc1
F1	F1	k1gMnSc1
</s>
<s>
Minardi	Minard	k1gMnPc1
PS01	PS01	k1gFnPc2
–	–	k?
European	Europeana	k1gFnPc2
3.0	3.0	k4
V10	V10	k1gFnPc2
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Nedokončil	dokončit	k5eNaPmAgMnS
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
2003	#num#	k4
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
Renault	renault	k1gInSc1
R23	R23	k1gFnSc2
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
2004	#num#	k4
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
Renault	renault	k1gInSc1
R24	R24	k1gFnSc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Nedokončil	dokončit	k5eNaPmAgMnS
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
Renault	renault	k1gInSc1
R25	R25	k1gFnSc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
Mild	Mild	k6eAd1
Seven	Seven	k2eAgInSc1d1
Renault	renault	k1gInSc1
F1	F1	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
Renault	renault	k1gInSc1
R26	R26	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
Vodafone	Vodafon	k1gInSc5
McLaren	McLarno	k1gNnPc2
Mercedes	mercedes	k1gInSc1
</s>
<s>
McLaren	McLarna	k1gFnPc2
MP	MP	kA
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
-Mercedes	-Mercedes	k1gInSc1
FO	FO	kA
108T	108T	k4
2.4	2.4	k4
V8	V8	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
ING	ing	kA
Renault	renault	k1gInSc1
F1	F1	k1gMnSc1
Team	team	k1gInSc1
</s>
<s>
Renault	renault	k1gInSc1
R28	R28	k1gFnSc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
2009	#num#	k4
</s>
<s>
ING	ing	kA
Renault	renault	k1gInSc1
F1	F1	k1gMnSc1
Team	team	k1gInSc1
</s>
<s>
Renault	renault	k1gInSc1
R29	R29	k1gFnSc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
Scuderia	Scuderium	k1gNnSc2
Ferrari	Ferrari	k1gMnPc2
Marlboro	Marlboro	k1gNnSc2
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
F10	F10	k1gMnSc1
</s>
<s>
Bez	bez	k7c2
času	čas	k1gInSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
2011	#num#	k4
</s>
<s>
Scuderia	Scuderium	k1gNnSc2
Ferrari	Ferrari	k1gMnPc2
Marlboro	Marlboro	k1gNnSc2
</s>
<s>
Ferrari	ferrari	k1gNnSc1
150	#num#	k4
Italia	Italium	k1gNnSc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
Scuderia	Scuderium	k1gNnPc1
Ferrari	ferrari	k1gNnSc2
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
F2012	F2012	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
Scuderia	Scuderium	k1gNnPc1
Ferrari	Ferrari	k1gMnPc2
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
F138	F138	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
2014	#num#	k4
</s>
<s>
Scuderia	Scuderium	k1gNnPc1
Ferrari	Ferrari	k1gMnPc1
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
F14T	F14T	k1gMnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
2015	#num#	k4
</s>
<s>
McLaren	McLarna	k1gFnPc2
Honda	honda	k1gFnSc1
F1	F1	k1gFnSc1
Team	team	k1gInSc1
</s>
<s>
McLaren	McLarna	k1gFnPc2
MP	MP	kA
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
-Honda	-Honda	k1gFnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Nedokončil	dokončit	k5eNaPmAgMnS
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
2016	#num#	k4
</s>
<s>
McLaren	McLarna	k1gFnPc2
Honda	honda	k1gFnSc1
F1	F1	k1gFnSc1
Team	team	k1gInSc1
</s>
<s>
McLaren	McLarna	k1gFnPc2
MP	MP	kA
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
-Honda	-Honda	k1gFnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
2018	#num#	k4
</s>
<s>
McLaren	McLarna	k1gFnPc2
F1	F1	k1gFnSc1
Team	team	k1gInSc1
</s>
<s>
McLaren	McLarna	k1gFnPc2
MCL	MCL	kA
<g/>
33	#num#	k4
<g/>
-Renault	-Renault	k5eAaPmF
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Nedokončil	dokončit	k5eNaPmAgMnS
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
Indianapolis	Indianapolis	k1gInSc1
500	#num#	k4
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Šasi	šasi	k1gNnSc1
</s>
<s>
Motor	motor	k1gInSc1
</s>
<s>
Kvalifikace	kvalifikace	k1gFnSc1
</s>
<s>
Umístění	umístění	k1gNnSc1
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
2017	#num#	k4
</s>
<s>
Dallara	Dallara	k1gFnSc1
</s>
<s>
Honda	honda	k1gFnSc1
</s>
<s>
5	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
McLaren-Honda-Andretti	McLaren-Honda-Andretti	k1gNnSc1
</s>
<s>
2019	#num#	k4
</s>
<s>
Dallara	Dallara	k1gFnSc1
</s>
<s>
Chevrolet	chevrolet	k1gInSc1
</s>
<s>
DNQ	DNQ	kA
</s>
<s>
McLaren	McLarna	k1gFnPc2
Racing	Racing	k1gInSc1
</s>
<s>
24	#num#	k4
<g/>
h	h	k?
Le	Le	k1gMnSc4
Mans	Mans	k1gInSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Kolegové	kolega	k1gMnPc1
</s>
<s>
Vůz	vůz	k1gInSc1
</s>
<s>
Třída	třída	k1gFnSc1
</s>
<s>
Kola	Kola	k1gFnSc1
</s>
<s>
Celkově	celkově	k6eAd1
</s>
<s>
ClassPos	ClassPos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
</s>
<s>
Toyota	toyota	k1gFnSc1
Gazoo	Gazoo	k6eAd1
Racing	Racing	k1gInSc4
</s>
<s>
Sébastien	Sébastino	k1gNnPc2
Buemi	Bue	k1gFnPc7
Kazuki	Kazuk	k1gFnSc2
Nakajima	Nakajimum	k1gNnSc2
</s>
<s>
Toyota	toyota	k1gFnSc1
TS050	TS050	k1gFnSc2
Hybrid	hybrida	k1gFnPc2
</s>
<s>
LMP1	LMP1	k4
</s>
<s>
388	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
2019	#num#	k4
</s>
<s>
Toyota	toyota	k1gFnSc1
Gazoo	Gazoo	k6eAd1
Racing	Racing	k1gInSc4
</s>
<s>
Sébastien	Sébastino	k1gNnPc2
Buemi	Bue	k1gFnPc7
Kazuki	Kazuk	k1gFnSc2
Nakajima	Nakajimum	k1gNnSc2
</s>
<s>
Toyota	toyota	k1gFnSc1
TS050	TS050	k1gFnSc2
Hybrid	hybrida	k1gFnPc2
</s>
<s>
LMP1	LMP1	k4
</s>
<s>
385	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
24	#num#	k4
<g/>
h	h	k?
Daytona	Daytona	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Kolegové	kolega	k1gMnPc1
</s>
<s>
Vůz	vůz	k1gInSc1
</s>
<s>
Třída	třída	k1gFnSc1
</s>
<s>
Kola	Kola	k1gFnSc1
</s>
<s>
Celkově	celkově	k6eAd1
</s>
<s>
ClassPos	ClassPos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
</s>
<s>
United	United	k1gInSc1
Autosports	Autosportsa	k1gFnPc2
</s>
<s>
Philip	Philip	k1gMnSc1
Hanson	Hanson	k1gMnSc1
Lando	Landa	k1gMnSc5
Norris	Norris	k1gFnPc6
</s>
<s>
Ligier	Ligier	k1gMnSc1
JS	JS	kA
P	P	kA
<g/>
217	#num#	k4
<g/>
-Gibson	-Gibsona	k1gFnPc2
</s>
<s>
P	P	kA
</s>
<s>
718	#num#	k4
</s>
<s>
38	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
13	#num#	k4
<g/>
th	th	k?
</s>
<s>
2019	#num#	k4
</s>
<s>
Konica	Konica	kA
Minolta	Minolta	kA
Cadillac	cadillac	k1gInSc1
</s>
<s>
Kamui	Kamu	k1gMnPc1
Kobayashi	Kobayash	k1gFnSc2
Jordan	Jordan	k1gMnSc1
Taylor	Taylor	k1gMnSc1
Renger	Renger	k1gMnSc1
van	van	k1gInSc4
der	drát	k5eAaImRp2nS
Zande	Zand	k1gInSc5
</s>
<s>
Cadillac	cadillac	k1gInSc1
DPi-	DPi-	k1gMnSc2
<g/>
V.R	V.R	k1gMnSc2
</s>
<s>
DPi	dpi	kA
</s>
<s>
593	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Rally	Ralla	k1gFnPc1
Dakar	Dakar	k1gInSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Třída	třída	k1gFnSc1
</s>
<s>
Vůz	vůz	k1gInSc1
</s>
<s>
Umístění	umístění	k1gNnSc1
</s>
<s>
Vyhrané	vyhraný	k2eAgFnPc1d1
zkoušky	zkouška	k1gFnPc1
</s>
<s>
2020	#num#	k4
</s>
<s>
Automobily	automobil	k1gInPc1
</s>
<s>
Toyota	toyota	k1gFnSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
0	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
returns	returns	k1gInSc1
to	ten	k3xDgNnSc4
live	livat	k5eAaPmIp3nS
in	in	k?
Spain	Spain	k1gInSc1
<g/>
↑	↑	k?
FABRY	FABRY	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Webber	Webber	k1gMnSc1
triumfoval	triumfovat	k5eAaBmAgMnS
v	v	k7c6
Monaku	Monako	k1gNnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
šestým	šestý	k4xOgMnSc7
různým	různý	k2eAgMnSc7d1
vítězem	vítěz	k1gMnSc7
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
formule	formule	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2012-05-27	2012-05-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Fernanda	Fernanda	k1gFnSc1
Alonsa	Alonsa	k1gFnSc1
</s>
<s>
Fernando-Alonso	Fernando-Alonsa	k1gFnSc5
<g/>
.	.	kIx.
<g/>
ru	ru	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Španělští	španělský	k2eAgMnPc1d1
jezdci	jezdec	k1gMnPc1
Formule	formule	k1gFnSc2
1	#num#	k4
</s>
<s>
Jaime	Jaimat	k5eAaPmIp3nS
Alguersuari	Alguersuare	k1gFnSc4
•	•	k?
</s>
<s>
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
</s>
<s>
Jorge	Jorge	k1gFnSc1
de	de	k?
Bagration	Bagration	k1gInSc1
•	•	k?
</s>
<s>
Adrian	Adrian	k1gMnSc1
Campos	Campos	k1gMnSc1
•	•	k?
</s>
<s>
Antonio	Antonio	k1gMnSc1
Creus	Creus	k1gMnSc1
•	•	k?
</s>
<s>
Marc	Marc	k1gFnSc1
Gené	Gená	k1gFnSc2
•	•	k?
</s>
<s>
Chico	Chico	k1gMnSc1
Godia	Godium	k1gNnSc2
Sales	Sales	k1gMnSc1
•	•	k?
</s>
<s>
Juan	Juan	k1gMnSc1
Jover	Jover	k1gMnSc1
•	•	k?
</s>
<s>
Roberto	Roberta	k1gFnSc5
Merhi	Merh	k1gFnSc5
•	•	k?
</s>
<s>
Luis	Luisa	k1gFnPc2
Perez	Perez	k1gMnSc1
Sala	Sala	k1gMnSc1
•	•	k?
</s>
<s>
Alfonso	Alfonso	k1gMnSc1
de	de	k?
Portago	Portago	k1gMnSc1
•	•	k?
</s>
<s>
Pedro	Pedro	k1gNnSc1
de	de	k?
la	la	k1gNnSc1
Rosa	Rosa	k1gMnSc1
•	•	k?
</s>
<s>
Carlos	Carlos	k1gMnSc1
Sainz	Sainz	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
•	•	k?
</s>
<s>
Alex	Alex	k1gMnSc1
Soler	Soler	k1gMnSc1
Roig	Roig	k1gMnSc1
•	•	k?
</s>
<s>
Emilio	Emilio	k6eAd1
de	de	k?
Villota	Villota	k1gFnSc1
•	•	k?
</s>
<s>
Emilio	Emilio	k1gMnSc1
Zapico	Zapico	k1gMnSc1
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
–	–	k?
mistr	mistr	k1gMnSc1
světa	svět	k1gInSc2
</s>
<s>
Mistři	mistr	k1gMnPc1
světa	svět	k1gInSc2
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
1950	#num#	k4
<g/>
–	–	k?
<g/>
19701971	#num#	k4
<g/>
–	–	k?
<g/>
19911992	#num#	k4
<g/>
–	–	k?
<g/>
20122013	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
</s>
<s>
1950	#num#	k4
Giuseppe	Giusepp	k1gInSc5
Farina	Farin	k2eAgFnSc1d1
(	(	kIx(
<g/>
Alfa	alfa	k1gFnSc1
Romeo	Romeo	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1951	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
(	(	kIx(
<g/>
Alfa	alfa	k1gFnSc1
Romeo	Romeo	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1952	#num#	k4
Alberto	Alberta	k1gFnSc5
Ascari	Ascar	k1gMnPc7
(	(	kIx(
<g/>
Ferrari	Ferrari	k1gMnSc7
<g/>
)	)	kIx)
</s>
<s>
1953	#num#	k4
Alberto	Alberta	k1gFnSc5
Ascari	Ascar	k1gMnPc7
(	(	kIx(
<g/>
Ferrari	Ferrari	k1gMnSc7
<g/>
)	)	kIx)
</s>
<s>
1954	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
(	(	kIx(
<g/>
Mercedes	mercedes	k1gInSc1
<g/>
/	/	kIx~
<g/>
Maserati	Maserat	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
1955	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
(	(	kIx(
<g/>
Mercedes	mercedes	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1956	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
(	(	kIx(
<g/>
Ferrari	Ferrari	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1957	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
(	(	kIx(
<g/>
Maserati	Maserat	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
1958	#num#	k4
Mike	Mik	k1gInSc2
Hawthorn	Hawthorn	k1gInSc1
(	(	kIx(
<g/>
Ferrari	Ferrari	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1959	#num#	k4
Jack	Jack	k1gInSc1
Brabham	Brabham	k1gInSc4
(	(	kIx(
<g/>
Cooper-Climax	Cooper-Climax	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1960	#num#	k4
Jack	Jack	k1gInSc1
Brabham	Brabham	k1gInSc4
(	(	kIx(
<g/>
Cooper-Climax	Cooper-Climax	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1961	#num#	k4
Phil	Phil	k1gMnSc1
Hill	Hill	k1gMnSc1
(	(	kIx(
<g/>
Ferrari	Ferrari	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1962	#num#	k4
Graham	Graham	k1gMnSc1
Hill	Hill	k1gMnSc1
(	(	kIx(
<g/>
BRM	brm	k0
<g/>
)	)	kIx)
</s>
<s>
1963	#num#	k4
Jim	on	k3xPp3gMnPc3
Clark	Clark	k1gInSc1
(	(	kIx(
<g/>
Lotus-Climax	Lotus-Climax	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1964	#num#	k4
John	John	k1gMnSc1
Surtees	Surtees	k1gMnSc1
(	(	kIx(
<g/>
Ferrari	Ferrari	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1965	#num#	k4
Jim	on	k3xPp3gMnPc3
Clark	Clark	k1gInSc1
(	(	kIx(
<g/>
Lotus-Climax	Lotus-Climax	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1966	#num#	k4
Jack	Jack	k1gInSc1
Brabham	Brabham	k1gInSc4
(	(	kIx(
<g/>
Brabham-Repco	Brabham-Repco	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
1967	#num#	k4
Denny	Denna	k1gFnSc2
Hulme	houlit	k5eAaImRp1nP
(	(	kIx(
<g/>
Brabham-Repco	Brabham-Repco	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
1968	#num#	k4
Graham	Graham	k1gMnSc1
Hill	Hill	k1gMnSc1
(	(	kIx(
<g/>
Lotus-Ford	Lotus-Ford	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1969	#num#	k4
Jackie	Jackie	k1gFnSc1
Stewart	Stewart	k1gMnSc1
(	(	kIx(
<g/>
Matra-Ford	Matra-Ford	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1970	#num#	k4
Jochen	Jochen	k1gInSc1
Rindt	Rindt	k2eAgInSc1d1
(	(	kIx(
<g/>
Lotus-Ford	Lotus-Ford	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1971	#num#	k4
Jackie	Jackie	k1gFnSc1
Stewart	Stewart	k1gMnSc1
(	(	kIx(
<g/>
Tyrell-Ford	Tyrell-Ford	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1972	#num#	k4
Emerson	Emersona	k1gFnPc2
Fittipaldi	Fittipald	k1gMnPc1
(	(	kIx(
<g/>
Lotus-Ford	Lotus-Ford	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1973	#num#	k4
Jackie	Jackie	k1gFnSc1
Stewart	Stewart	k1gMnSc1
(	(	kIx(
<g/>
Tyrell-Ford	Tyrell-Ford	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1974	#num#	k4
Emerson	Emersona	k1gFnPc2
Fittipaldi	Fittipald	k1gMnPc1
(	(	kIx(
<g/>
McLaren-Ford	McLaren-Ford	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1975	#num#	k4
Niki	Niki	k1gNnSc7
Lauda	Laudo	k1gNnSc2
(	(	kIx(
<g/>
Ferrari	Ferrari	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1976	#num#	k4
James	James	k1gInSc1
Hunt	hunt	k1gInSc4
(	(	kIx(
<g/>
McLaren-Ford	McLaren-Ford	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1977	#num#	k4
Niki	Niki	k1gNnSc7
Lauda	Laudo	k1gNnSc2
(	(	kIx(
<g/>
Ferrari	Ferrari	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1978	#num#	k4
Mario	Mario	k1gMnSc1
Andretti	Andrett	k2eAgMnPc1d1
(	(	kIx(
<g/>
Lotus-Ford	Lotus-Ford	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1979	#num#	k4
Jody	jod	k1gInPc1
Scheckter	Scheckter	k1gMnSc1
(	(	kIx(
<g/>
Ferrari	Ferrari	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1980	#num#	k4
Alan	Alan	k1gMnSc1
Jones	Jones	k1gMnSc1
(	(	kIx(
<g/>
Williams-Ford	Williams-Ford	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1981	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
(	(	kIx(
<g/>
Brabham-Ford	Brabham-Ford	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1982	#num#	k4
Keke	Kek	k1gInSc2
Rosberg	Rosberg	k1gInSc1
(	(	kIx(
<g/>
Williams-Ford	Williams-Ford	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1983	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
(	(	kIx(
<g/>
Brabham-BMW	Brabham-BMW	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1984	#num#	k4
Niki	Niki	k1gNnSc7
Lauda	Laudo	k1gNnSc2
(	(	kIx(
<g/>
McLaren-TAG	McLaren-TAG	k1gFnSc1
Porsche	Porsche	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
1985	#num#	k4
Alain	Alain	k1gInSc1
Prost	prost	k2eAgInSc1d1
(	(	kIx(
<g/>
McLaren-TAG	McLaren-TAG	k1gFnSc1
Porsche	Porsche	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
1986	#num#	k4
Alain	Alain	k1gInSc1
Prost	prost	k2eAgInSc1d1
(	(	kIx(
<g/>
McLaren-TAG	McLaren-TAG	k1gFnSc1
Porsche	Porsche	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
1987	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
(	(	kIx(
<g/>
Williams-Honda	Williams-Honda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1988	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
(	(	kIx(
<g/>
McLaren-Honda	McLaren-Honda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1989	#num#	k4
Alain	Alain	k1gInSc1
Prost	prost	k2eAgInSc1d1
(	(	kIx(
<g/>
McLaren-Honda	McLaren-Honda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
(	(	kIx(
<g/>
McLaren-Honda	McLaren-Honda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1991	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
(	(	kIx(
<g/>
McLaren-Honda	McLaren-Honda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1992	#num#	k4
Nigel	Nigel	k1gMnSc1
Mansell	Mansell	k1gMnSc1
(	(	kIx(
<g/>
Williams-Renault	Williams-Renault	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1993	#num#	k4
Alain	Alain	k1gInSc1
Prost	prost	k2eAgInSc1d1
(	(	kIx(
<g/>
Williams-Renault	Williams-Renault	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1994	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
(	(	kIx(
<g/>
Benetton-Ford	Benetton-Ford	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1995	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
(	(	kIx(
<g/>
Benetton-Renault	Benetton-Renault	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1996	#num#	k4
Damon	Damon	k1gMnSc1
Hill	Hill	k1gMnSc1
(	(	kIx(
<g/>
Williams-Renault	Williams-Renault	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1997	#num#	k4
Jacques	Jacques	k1gMnSc1
Villeneuve	Villeneuev	k1gFnSc2
(	(	kIx(
<g/>
Williams-Renault	Williams-Renault	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1998	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinna	k1gFnPc2
(	(	kIx(
<g/>
McLaren-Mercedes	McLaren-Mercedes	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1999	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinna	k1gFnPc2
(	(	kIx(
<g/>
McLaren-Mercedes	McLaren-Mercedes	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2000	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
(	(	kIx(
<g/>
Ferrari	Ferrari	k1gMnSc7
<g/>
)	)	kIx)
</s>
<s>
2001	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
(	(	kIx(
<g/>
Ferrari	Ferrari	k1gMnSc7
<g/>
)	)	kIx)
</s>
<s>
2002	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
(	(	kIx(
<g/>
Ferrari	Ferrari	k1gMnSc7
<g/>
)	)	kIx)
</s>
<s>
2003	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
(	(	kIx(
<g/>
Ferrari	Ferrari	k1gMnSc7
<g/>
)	)	kIx)
</s>
<s>
2004	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
(	(	kIx(
<g/>
Ferrari	Ferrari	k1gMnSc7
<g/>
)	)	kIx)
</s>
<s>
2005	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
(	(	kIx(
<g/>
Renault	renault	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2006	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
(	(	kIx(
<g/>
Renault	renault	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2007	#num#	k4
Kimi	Kimi	k1gNnSc1
Räikkönen	Räikkönna	k1gFnPc2
(	(	kIx(
<g/>
Ferrari	Ferrari	k1gMnSc7
<g/>
)	)	kIx)
</s>
<s>
2008	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
(	(	kIx(
<g/>
McLaren-Mercedes	McLaren-Mercedes	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
2009	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
(	(	kIx(
<g/>
Brawn-Mercedes	Brawn-Mercedes	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
2010	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
(	(	kIx(
<g/>
Red	Red	k1gMnSc1
Bull-Renault	Bull-Renault	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
2011	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
(	(	kIx(
<g/>
Red	Red	k1gMnSc1
Bull-Renault	Bull-Renault	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
2012	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
(	(	kIx(
<g/>
Red	Red	k1gMnSc1
Bull-Renault	Bull-Renault	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
2013	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
(	(	kIx(
<g/>
Red	Red	k1gMnSc1
Bull-Renault	Bull-Renault	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
2014	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
(	(	kIx(
<g/>
Mercedes	mercedes	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2015	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
(	(	kIx(
<g/>
Mercedes	mercedes	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2016	#num#	k4
Nico	Nico	k1gMnSc1
Rosberg	Rosberg	k1gMnSc1
(	(	kIx(
<g/>
Mercedes	mercedes	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2017	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
(	(	kIx(
<g/>
Mercedes	mercedes	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2018	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
(	(	kIx(
<g/>
Mercedes	mercedes	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2019	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
(	(	kIx(
<g/>
Mercedes	mercedes	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2020	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
(	(	kIx(
<g/>
Mercedes	mercedes	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
1985	#num#	k4
Keke	Keke	k1gInSc1
Rosberg	Rosberg	k1gInSc4
•	•	k?
1986	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1987	#num#	k4
Gerhard	Gerhard	k1gMnSc1
Berger	Berger	k1gMnSc1
•	•	k?
1988	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1989	#num#	k4
Thierry	Thierra	k1gFnSc2
Boutsen	Boutsna	k1gFnPc2
•	•	k?
1990	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1991	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1992	#num#	k4
Gerhard	Gerhard	k1gMnSc1
Berger	Berger	k1gMnSc1
•	•	k?
1993	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1994	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1995	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1996	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1997	#num#	k4
David	David	k1gMnSc1
Coulthard	Coulthard	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1998	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
1999	#num#	k4
Eddie	Eddie	k1gFnSc1
Irvine	Irvin	k1gInSc5
•	•	k?
2000	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2001	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2002	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2003	#num#	k4
David	David	k1gMnSc1
Coulthard	Coulthard	k1gMnSc1
•	•	k?
2004	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2005	#num#	k4
Giancarlo	Giancarlo	k1gNnSc4
Fisichella	Fisichello	k1gNnSc2
•	•	k?
2006	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2007	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2008	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2009	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
•	•	k?
2010	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
•	•	k?
2011	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2012	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
•	•	k?
2013	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2014	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2015	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2016	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2017	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2018	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2019	#num#	k4
Valtteri	Valtteri	k1gNnPc2
Bottas	Bottasa	k1gFnPc2
•	•	k?
2020	#num#	k4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Bahrajnu	Bahrajn	k1gMnSc3
</s>
<s>
2004	#num#	k4
Michael	Michael	k1gMnSc1
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2005	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2006	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2007	#num#	k4
Felipe	Felip	k1gInSc5
Massa	Massa	k1gFnSc1
•	•	k?
2008	#num#	k4
Felipe	Felip	k1gInSc5
Massa	Massa	k1gFnSc1
•	•	k?
2009	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
•	•	k?
2010	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2011	#num#	k4
•	•	k?
2012	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2013	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2014	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2015	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2016	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2017	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2018	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2019	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2020	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2021	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Číny	Čína	k1gFnSc2
</s>
<s>
2004	#num#	k4
Rubens	Rubensa	k1gFnPc2
Barrichello	Barrichello	k1gNnSc1
•	•	k?
2005	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2006	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2007	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2008	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2009	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2010	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
•	•	k?
2011	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2012	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2013	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2014	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2015	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2016	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2017	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2018	#num#	k4
Daniel	Daniela	k1gFnPc2
Ricciardo	Ricciardo	k1gNnSc1
•	•	k?
2019	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2020	#num#	k4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Evropy	Evropa	k1gFnSc2
</s>
<s>
1983	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1984	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1985	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1993	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1994	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1995	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1996	#num#	k4
Jacques	Jacques	k1gMnSc1
Villeneuve	Villeneuev	k1gFnSc2
•	•	k?
1997	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
1999	#num#	k4
Johnny	Johnna	k1gFnSc2
Herbert	Herbert	k1gInSc1
•	•	k?
2000	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2001	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2002	#num#	k4
Rubens	Rubensa	k1gFnPc2
Barrichello	Barrichello	k1gNnSc1
•	•	k?
2003	#num#	k4
Ralf	Ralf	k1gInSc1
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2004	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2005	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2006	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2007	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2008	#num#	k4
Felipe	Felip	k1gInSc5
Massa	Massa	k1gFnSc1
•	•	k?
2009	#num#	k4
Rubens	Rubensa	k1gFnPc2
Barrichello	Barrichello	k1gNnSc1
•	•	k?
2010	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2011	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2012	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2016	#num#	k4
Nico	Nico	k1gMnSc1
Rosberg	Rosberg	k1gMnSc1
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Francie	Francie	k1gFnSc2
</s>
<s>
1950	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
•	•	k?
1951	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
•	•	k?
1952	#num#	k4
Alberto	Alberta	k1gFnSc5
Ascari	Ascar	k1gInPc7
•	•	k?
1953	#num#	k4
Mike	Mike	k1gInSc1
Hawthorn	Hawthorn	k1gInSc4
•	•	k?
1954	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
•	•	k?
1955	#num#	k4
•	•	k?
1956	#num#	k4
Peter	Petra	k1gFnPc2
Collins	Collinsa	k1gFnPc2
•	•	k?
1957	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
•	•	k?
1958	#num#	k4
Mike	Mike	k1gInSc1
Hawthorn	Hawthorn	k1gInSc4
•	•	k?
1959	#num#	k4
Tony	Tony	k1gFnSc2
Brooks	Brooksa	k1gFnPc2
•	•	k?
1960	#num#	k4
Jack	Jack	k1gInSc1
Brabham	Brabham	k1gInSc4
•	•	k?
1961	#num#	k4
Giancarlo	Giancarlo	k1gNnSc4
Baghelti	Baghelť	k1gFnSc2
•	•	k?
1962	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Dan	Dana	k1gFnPc2
Gurney	Gurnea	k1gFnSc2
•	•	k?
1963	#num#	k4
Jim	on	k3xPp3gFnPc3
Clark	Clark	k1gInSc1
•	•	k?
1964	#num#	k4
Dan	Dana	k1gFnPc2
Gurney	Gurnea	k1gFnSc2
•	•	k?
1965	#num#	k4
Jim	on	k3xPp3gFnPc3
Clark	Clark	k1gInSc1
•	•	k?
1966	#num#	k4
Jack	Jack	k1gInSc1
Brabham	Brabham	k1gInSc4
•	•	k?
1967	#num#	k4
Jack	Jack	k1gInSc1
Brabham	Brabham	k1gInSc4
•	•	k?
1968	#num#	k4
Jacky	Jacka	k1gFnSc2
Ickx	Ickx	k1gInSc1
•	•	k?
1969	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1970	#num#	k4
Jochen	Jochen	k2eAgInSc4d1
Rindt	Rindt	k1gInSc4
•	•	k?
1971	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1972	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1973	#num#	k4
Ronnie	Ronnie	k1gFnSc2
Peterson	Petersona	k1gFnPc2
•	•	k?
1974	#num#	k4
Ronnie	Ronnie	k1gFnSc2
Peterson	Petersona	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1975	#num#	k4
Niki	Nik	k1gFnSc2
Lauda	Lauda	k1gFnSc1
•	•	k?
1976	#num#	k4
James	James	k1gInSc1
Hunt	hunt	k1gInSc4
•	•	k?
1977	#num#	k4
Mario	Mario	k1gMnSc1
Andretti	Andretti	k1gNnSc1
•	•	k?
1978	#num#	k4
Mario	Mario	k1gMnSc1
Andretti	Andretti	k1gNnSc1
•	•	k?
1979	#num#	k4
Jean-Pierre	Jean-Pierr	k1gInSc5
Jabouille	Jabouille	k1gInSc4
•	•	k?
1980	#num#	k4
Alan	alan	k1gInSc1
Jones	Jones	k1gInSc4
•	•	k?
1981	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1982	#num#	k4
René	René	k1gFnPc2
Arnoux	Arnoux	k1gInSc4
•	•	k?
1983	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1984	#num#	k4
Niki	Nik	k1gFnSc2
Lauda	Lauda	k1gFnSc1
•	•	k?
1985	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1986	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1987	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1988	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1989	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1990	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1991	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1992	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1993	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1994	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1995	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1996	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1997	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1998	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1999	#num#	k4
Heinz-Harald	Heinz-Harald	k1gInSc1
Frentzen	Frentzen	k2eAgInSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2000	#num#	k4
David	David	k1gMnSc1
Coulthard	Coulthard	k1gMnSc1
•	•	k?
2001	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2002	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2003	#num#	k4
Ralf	Ralf	k1gInSc1
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2004	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2005	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2006	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2007	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2008	#num#	k4
Felipe	Felip	k1gInSc5
Massa	Massa	k1gFnSc1
•	•	k?
2009	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
•	•	k?
2018	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2019	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2020	#num#	k4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Itálie	Itálie	k1gFnSc2
</s>
<s>
1950	#num#	k4
Giuseppe	Giusepp	k1gInSc5
Farina	Farin	k2eAgInSc2d1
•	•	k?
1951	#num#	k4
Alberto	Alberta	k1gFnSc5
Ascari	Ascar	k1gInPc7
•	•	k?
1952	#num#	k4
Alberto	Alberta	k1gFnSc5
Ascari	Ascar	k1gInPc7
•	•	k?
1953	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
•	•	k?
1954	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
•	•	k?
1955	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
•	•	k?
1956	#num#	k4
Stirling	Stirling	k1gInSc1
Moss	Moss	k1gInSc4
•	•	k?
1957	#num#	k4
Stirling	Stirling	k1gInSc1
Moss	Moss	k1gInSc4
•	•	k?
1958	#num#	k4
Tony	Tony	k1gFnSc2
Brooks	Brooksa	k1gFnPc2
•	•	k?
1959	#num#	k4
Stirling	Stirling	k1gInSc1
Moss	Moss	k1gInSc4
•	•	k?
1960	#num#	k4
Phil	Phil	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1961	#num#	k4
Phil	Phil	k1gMnSc1
Hill	Hill	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1962	#num#	k4
Graham	graham	k1gInSc4
Hill	Hilla	k1gFnPc2
•	•	k?
1963	#num#	k4
Jim	on	k3xPp3gMnPc3
Clark	Clark	k1gInSc4
•	•	k?
1964	#num#	k4
John	John	k1gMnSc1
Surtees	Surteesa	k1gFnPc2
•	•	k?
1965	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1966	#num#	k4
Ludovico	Ludovico	k1gNnSc4
Scarfiotti	Scarfiotť	k1gFnSc2
•	•	k?
1967	#num#	k4
John	John	k1gMnSc1
Surtees	Surteesa	k1gFnPc2
•	•	k?
1968	#num#	k4
Denny	Denna	k1gFnSc2
Hulme	houlit	k5eAaImRp1nP
•	•	k?
1969	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1970	#num#	k4
Clay	Claa	k1gFnSc2
Regazzoni	Regazzon	k1gMnPc1
•	•	k?
1971	#num#	k4
Peter	Petra	k1gFnPc2
Gethin	Gethina	k1gFnPc2
•	•	k?
1972	#num#	k4
Emerson	Emersona	k1gFnPc2
Fittipaldi	Fittipald	k1gMnPc1
•	•	k?
1973	#num#	k4
Ronnie	Ronnie	k1gFnSc2
Peterson	Petersona	k1gFnPc2
•	•	k?
1974	#num#	k4
Ronnie	Ronnie	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Peterson	Peterson	k1gInSc1
•	•	k?
1975	#num#	k4
Clay	Claa	k1gFnSc2
Regazzoni	Regazzon	k1gMnPc1
•	•	k?
1976	#num#	k4
Ronnie	Ronnie	k1gFnSc2
Peterson	Petersona	k1gFnPc2
•	•	k?
1977	#num#	k4
Mario	Mario	k1gMnSc1
Andretti	Andretti	k1gNnSc1
•	•	k?
1978	#num#	k4
Niki	Nik	k1gFnSc2
Lauda	Lauda	k1gFnSc1
•	•	k?
1979	#num#	k4
Jody	jod	k1gInPc1
Scheckter	Scheckter	k1gInSc1
•	•	k?
1980	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1981	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1982	#num#	k4
René	René	k1gFnPc2
Arnoux	Arnoux	k1gInSc4
•	•	k?
1983	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1984	#num#	k4
Niki	Nik	k1gFnSc2
Lauda	Lauda	k1gFnSc1
•	•	k?
1985	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1986	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1987	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1988	#num#	k4
Gerhard	Gerhard	k1gMnSc1
Berger	Berger	k1gMnSc1
•	•	k?
1989	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1990	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1991	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1992	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1993	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1994	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1995	#num#	k4
Johnny	Johnna	k1gFnSc2
Herbert	Herbert	k1gInSc1
•	•	k?
1996	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1997	#num#	k4
David	David	k1gMnSc1
Coulthard	Coulthard	k1gMnSc1
•	•	k?
1998	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1999	#num#	k4
Heinz-Harald	Heinz-Harald	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Frentzen	Frentzen	k2eAgInSc1d1
•	•	k?
2000	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2001	#num#	k4
Juan	Juan	k1gMnSc1
Pablo	Pablo	k1gNnSc1
Montoya	Montoya	k1gMnSc1
•	•	k?
2002	#num#	k4
Rubens	Rubensa	k1gFnPc2
Barrichello	Barrichello	k1gNnSc1
•	•	k?
2003	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2004	#num#	k4
Rubens	Rubensa	k1gFnPc2
Barrichello	Barrichello	k1gNnSc1
•	•	k?
2005	#num#	k4
Juan	Juan	k1gMnSc1
Pablo	Pablo	k1gNnSc1
Montoya	Montoya	k1gMnSc1
•	•	k?
2006	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2007	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2008	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2009	#num#	k4
Rubens	Rubensa	k1gFnPc2
Barrichello	Barrichello	k1gNnSc1
•	•	k?
2010	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2011	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2012	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2013	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2014	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2015	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2016	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2017	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2018	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2019	#num#	k4
Charles	Charles	k1gMnSc1
Leclerc	Leclerc	k1gInSc1
•	•	k?
2020	#num#	k4
Pierre	Pierr	k1gMnSc5
Gasly	Gasl	k1gMnPc7
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Japonska	Japonsko	k1gNnSc2
</s>
<s>
1976	#num#	k4
Mario	Mario	k1gMnSc1
Andretti	Andretti	k1gNnSc1
•	•	k?
1977	#num#	k4
James	James	k1gInSc1
Hunt	hunt	k1gInSc4
•	•	k?
1978	#num#	k4
<g/>
–	–	k?
<g/>
1986	#num#	k4
•	•	k?
1987	#num#	k4
Gerhard	Gerhard	k1gMnSc1
Berger	Berger	k1gMnSc1
•	•	k?
1988	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1989	#num#	k4
Alessandro	Alessandra	k1gFnSc5
Nannini	Nannin	k2eAgMnPc1d1
•	•	k?
1990	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1991	#num#	k4
Gerhard	Gerhard	k1gMnSc1
Berger	Berger	k1gMnSc1
•	•	k?
1992	#num#	k4
Riccardo	Riccardo	k1gNnSc4
Patrese	Patrese	k1gFnSc2
•	•	k?
1993	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1994	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1995	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1996	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Damon	Damon	k1gMnSc1
Hill	Hill	k1gMnSc1
•	•	k?
1997	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1998	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
1999	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
2000	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2001	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2002	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2003	#num#	k4
Rubens	Rubensa	k1gFnPc2
Barrichello	Barrichello	k1gNnSc1
•	•	k?
2004	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2005	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2006	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2007	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2008	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2009	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2010	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2011	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
•	•	k?
2012	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2013	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2014	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2015	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2016	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2017	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2018	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2019	#num#	k4
Valtteri	Valtteri	k1gNnPc2
Bottas	Bottasa	k1gFnPc2
•	•	k?
2020	#num#	k4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Kanady	Kanada	k1gFnSc2
</s>
<s>
1967	#num#	k4
Jack	Jacka	k1gFnPc2
Brabham	Brabham	k1gInSc4
•	•	k?
1968	#num#	k4
Denny	Denna	k1gFnSc2
Hulme	houlit	k5eAaImRp1nP
•	•	k?
1969	#num#	k4
Jacky	Jacka	k1gMnSc2
Ickx	Ickx	k1gInSc4
•	•	k?
1970	#num#	k4
Jacky	Jacka	k1gMnSc2
Ickx	Ickx	k1gInSc4
•	•	k?
1971	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1972	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1973	#num#	k4
Peter	Petra	k1gFnPc2
Revson	Revsona	k1gFnPc2
•	•	k?
1974	#num#	k4
Emerson	Emersona	k1gFnPc2
Fittipaldi	Fittipald	k1gMnPc1
•	•	k?
1975	#num#	k4
•	•	k?
1976	#num#	k4
James	Jamesa	k1gFnPc2
Hunt	hunt	k1gInSc4
•	•	k?
1977	#num#	k4
Jody	jod	k1gInPc4
Scheckter	Schecktrum	k1gNnPc2
•	•	k?
1978	#num#	k4
Gilles	Gillesa	k1gFnPc2
Villeneuve	Villeneuev	k1gFnSc2
•	•	k?
1979	#num#	k4
Alan	alan	k1gInSc4
Jones	Jonesa	k1gFnPc2
•	•	k?
1980	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Alan	Alan	k1gMnSc1
Jones	Jones	k1gMnSc1
•	•	k?
1981	#num#	k4
Jacques	Jacques	k1gMnSc1
Laffite	Laffit	k1gInSc5
•	•	k?
1982	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1983	#num#	k4
René	René	k1gFnPc2
Arnoux	Arnoux	k1gInSc4
•	•	k?
1984	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1985	#num#	k4
Michele	Michela	k1gFnSc6
Alboreto	Alboreto	k1gNnSc1
•	•	k?
1986	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1987	#num#	k4
•	•	k?
1988	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1989	#num#	k4
Thierry	Thierra	k1gFnSc2
Boutsen	Boutsna	k1gFnPc2
•	•	k?
1990	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1991	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1992	#num#	k4
Gerhard	Gerhard	k1gMnSc1
Berger	Berger	k1gMnSc1
•	•	k?
1993	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Alain	Alain	k1gInSc1
Prost	prost	k2eAgInSc1d1
•	•	k?
1994	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1995	#num#	k4
Jean	Jean	k1gMnSc1
Alesi	Alese	k1gFnSc4
•	•	k?
1996	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1997	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1998	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1999	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
2000	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2001	#num#	k4
Ralf	Ralf	k1gInSc1
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2002	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2003	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2004	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2005	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2006	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2007	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2008	#num#	k4
Robert	Robert	k1gMnSc1
Kubica	Kubica	k1gMnSc1
•	•	k?
2009	#num#	k4
•	•	k?
2010	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2011	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
•	•	k?
2012	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2013	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2014	#num#	k4
Daniel	Daniela	k1gFnPc2
Ricciardo	Ricciardo	k1gNnSc1
•	•	k?
2015	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2016	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2017	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2018	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2019	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2020	#num#	k4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Koreje	Korea	k1gFnSc2
</s>
<s>
2010	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2011	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2012	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2013	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Maďarska	Maďarsko	k1gNnSc2
</s>
<s>
1986	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1987	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1988	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1989	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1990	#num#	k4
Thierry	Thierra	k1gFnSc2
Boutsen	Boutsna	k1gFnPc2
•	•	k?
1991	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1992	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1993	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1994	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1995	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1996	#num#	k4
Jacques	Jacques	k1gMnSc1
Villeneuve	Villeneuev	k1gFnSc2
•	•	k?
1997	#num#	k4
Jacques	Jacques	k1gMnSc1
Villeneuve	Villeneuev	k1gFnSc2
•	•	k?
1998	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1999	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
2000	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
2001	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2002	#num#	k4
Rubens	Rubensa	k1gFnPc2
Barrichello	Barrichello	k1gNnSc1
•	•	k?
2003	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2004	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2005	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2006	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
•	•	k?
2007	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2008	#num#	k4
Heikki	Heikki	k1gNnPc2
Kovalainen	Kovalainna	k1gFnPc2
•	•	k?
2009	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2010	#num#	k4
Mark	Mark	k1gMnSc1
Webber	Webber	k1gMnSc1
•	•	k?
2011	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
•	•	k?
2012	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2013	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2014	#num#	k4
Daniel	Daniela	k1gFnPc2
Ricciardo	Ricciardo	k1gNnSc1
•	•	k?
2015	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2016	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2017	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2018	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2019	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2020	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Malajsie	Malajsie	k1gFnSc2
</s>
<s>
1999	#num#	k4
Eddie	Eddie	k1gFnSc1
Irvine	Irvin	k1gInSc5
•	•	k?
2000	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2001	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2002	#num#	k4
Ralf	Ralf	k1gInSc1
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2003	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2004	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2005	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2006	#num#	k4
Giancarlo	Giancarlo	k1gNnSc4
Fisichella	Fisichello	k1gNnSc2
•	•	k?
2007	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2008	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2009	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
•	•	k?
2010	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2011	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2012	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2013	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2014	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2015	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2016	#num#	k4
Daniel	Daniela	k1gFnPc2
Ricciardo	Ricciardo	k1gNnSc1
•	•	k?
2017	#num#	k4
Max	max	kA
Verstappen	Verstappen	k2eAgInSc4d1
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Monaka	Monako	k1gNnSc2
</s>
<s>
1950	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
•	•	k?
1951	#num#	k4
<g/>
–	–	k?
<g/>
1954	#num#	k4
•	•	k?
1955	#num#	k4
Maurice	Maurika	k1gFnSc6
Trintignant	Trintignant	k1gInSc1
•	•	k?
1956	#num#	k4
Stirling	Stirling	k1gInSc1
Moss	Moss	k1gInSc4
•	•	k?
1957	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
•	•	k?
1958	#num#	k4
Maurice	Maurika	k1gFnSc6
Trintignant	Trintignant	k1gInSc1
•	•	k?
1959	#num#	k4
Jack	Jack	k1gInSc1
Brabham	Brabham	k1gInSc4
•	•	k?
1960	#num#	k4
Stirling	Stirling	k1gInSc1
Moss	Moss	k1gInSc4
•	•	k?
1961	#num#	k4
Stirling	Stirling	k1gInSc1
Moss	Moss	k1gInSc4
•	•	k?
1962	#num#	k4
Bruce	Bruce	k1gFnSc2
McLaren	McLarna	k1gFnPc2
•	•	k?
1963	#num#	k4
Graham	graham	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1964	#num#	k4
Graham	graham	k1gInSc1
Hill	Hill	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1965	#num#	k4
Graham	graham	k1gInSc4
Hill	Hilla	k1gFnPc2
•	•	k?
1966	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1967	#num#	k4
Denny	Denna	k1gFnSc2
Hulme	houlit	k5eAaImRp1nP
•	•	k?
1968	#num#	k4
Graham	graham	k1gInSc4
Hill	Hilla	k1gFnPc2
•	•	k?
1969	#num#	k4
Graham	graham	k1gInSc4
Hill	Hilla	k1gFnPc2
•	•	k?
1970	#num#	k4
Jochen	Jochen	k2eAgInSc4d1
Rindt	Rindt	k1gInSc4
•	•	k?
1971	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1972	#num#	k4
Jean-Pierre	Jean-Pierr	k1gInSc5
Beltoise	Beltois	k1gInSc6
•	•	k?
1973	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1974	#num#	k4
Ronnie	Ronnie	k1gFnSc2
Peterson	Petersona	k1gFnPc2
•	•	k?
1975	#num#	k4
Niki	Nik	k1gFnSc2
Lauda	Laudo	k1gNnSc2
•	•	k?
1976	#num#	k4
Niki	Nik	k1gFnSc2
Lauda	Laudo	k1gNnSc2
•	•	k?
1977	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Jody	jod	k1gInPc1
Scheckter	Scheckter	k1gInSc1
•	•	k?
1978	#num#	k4
Patrick	Patrick	k1gInSc1
Depailler	Depailler	k1gInSc4
•	•	k?
1979	#num#	k4
Jody	jod	k1gInPc1
Scheckter	Scheckter	k1gInSc1
•	•	k?
1980	#num#	k4
Carlos	Carlos	k1gInSc1
Reutemann	Reutemann	k1gInSc4
•	•	k?
1981	#num#	k4
Gilles	Gilles	k1gInSc1
Villeneuve	Villeneuev	k1gFnSc2
•	•	k?
1982	#num#	k4
Riccardo	Riccardo	k1gNnSc4
Patrese	Patrese	k1gFnSc2
•	•	k?
1983	#num#	k4
Keke	Keke	k1gInSc1
Rosberg	Rosberg	k1gInSc4
•	•	k?
1984	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1985	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1986	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1987	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1988	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1989	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1990	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1991	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1992	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1993	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1994	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1995	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1996	#num#	k4
Olivier	Olivier	k1gInSc1
Panis	Panis	k1gInSc4
•	•	k?
1997	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1998	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
1999	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2000	#num#	k4
David	David	k1gMnSc1
Coulthard	Coulthard	k1gMnSc1
•	•	k?
2001	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2002	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
David	David	k1gMnSc1
Coulthard	Coulthard	k1gMnSc1
•	•	k?
2003	#num#	k4
Juan	Juan	k1gMnSc1
Pablo	Pablo	k1gNnSc1
Montoya	Montoya	k1gMnSc1
•	•	k?
2004	#num#	k4
Jarno	Jarno	k6eAd1
Trulli	Trulle	k1gFnSc4
•	•	k?
2005	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2006	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2007	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2008	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2009	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
•	•	k?
2010	#num#	k4
Mark	Mark	k1gMnSc1
Webber	Webber	k1gMnSc1
•	•	k?
2011	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2012	#num#	k4
Mark	Mark	k1gMnSc1
Webber	Webber	k1gMnSc1
•	•	k?
2013	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2014	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2015	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2016	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2017	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2018	#num#	k4
Daniel	Daniela	k1gFnPc2
Ricciardo	Ricciardo	k1gNnSc1
•	•	k?
2019	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2020	#num#	k4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Německa	Německo	k1gNnSc2
</s>
<s>
1951	#num#	k4
Alberto	Alberta	k1gFnSc5
Ascari	Ascar	k1gInPc7
•	•	k?
1952	#num#	k4
Alberto	Alberta	k1gFnSc5
Ascari	Ascar	k1gInPc7
•	•	k?
1953	#num#	k4
Giuseppe	Giusepp	k1gInSc5
Farina	Farin	k2eAgInSc2d1
•	•	k?
1954	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
•	•	k?
1955	#num#	k4
•	•	k?
1956	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
•	•	k?
1957	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
•	•	k?
1958	#num#	k4
Tony	Tony	k1gFnSc2
Brooks	Brooksa	k1gFnPc2
•	•	k?
1959	#num#	k4
Tony	Tony	k1gFnSc2
Brooks	Brooksa	k1gFnPc2
•	•	k?
1966	#num#	k4
•	•	k?
1961	#num#	k4
Stirling	Stirling	k1gInSc1
Moss	Moss	k1gInSc4
•	•	k?
1962	#num#	k4
Graham	graham	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1963	#num#	k4
John	John	k1gMnSc1
Surtees	Surtees	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1964	#num#	k4
John	John	k1gMnSc1
Surtees	Surteesa	k1gFnPc2
•	•	k?
1965	#num#	k4
Jim	on	k3xPp3gMnPc3
Clark	Clark	k1gInSc4
•	•	k?
1966	#num#	k4
Jack	Jacka	k1gFnPc2
Brabham	Brabham	k1gInSc4
•	•	k?
1967	#num#	k4
Denny	Denna	k1gFnSc2
Hulme	houlit	k5eAaImRp1nP
•	•	k?
1968	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1969	#num#	k4
Jacky	Jacka	k1gMnSc2
Ickx	Ickx	k1gInSc4
•	•	k?
1970	#num#	k4
Jochen	Jochen	k2eAgInSc4d1
Rindt	Rindt	k1gInSc4
•	•	k?
1971	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1972	#num#	k4
Jacky	Jacka	k1gMnSc2
Ickx	Ickx	k1gInSc4
•	•	k?
1973	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1974	#num#	k4
Clay	Claa	k1gFnSc2
Regazzoni	Regazzon	k1gMnPc1
•	•	k?
1975	#num#	k4
Carlos	Carlosa	k1gFnPc2
Reutemann	Reutemanna	k1gFnPc2
•	•	k?
1976	#num#	k4
James	Jamesa	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
Hunt	hunt	k1gInSc1
•	•	k?
1977	#num#	k4
Niki	Nik	k1gFnSc2
Lauda	Lauda	k1gFnSc1
•	•	k?
1978	#num#	k4
Mario	Mario	k1gMnSc1
Andretti	Andretti	k1gNnSc1
•	•	k?
1979	#num#	k4
Alan	alan	k1gInSc1
Jones	Jones	k1gInSc4
•	•	k?
1980	#num#	k4
Jacques	Jacques	k1gMnSc1
Laffite	Laffit	k1gInSc5
•	•	k?
1981	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1982	#num#	k4
Patrick	Patrick	k1gInSc1
Tambay	Tambaa	k1gFnSc2
•	•	k?
1983	#num#	k4
René	René	k1gFnPc2
Arnoux	Arnoux	k1gInSc4
•	•	k?
1984	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1985	#num#	k4
Michele	Michela	k1gFnSc6
Alboreto	Alboreto	k1gNnSc1
•	•	k?
1986	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1987	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1988	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1989	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1990	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1991	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1992	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1993	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1994	#num#	k4
Gerhard	Gerhard	k1gMnSc1
Berger	Berger	k1gMnSc1
•	•	k?
1995	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1996	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1997	#num#	k4
Gerhard	Gerhard	k1gMnSc1
Berger	Berger	k1gMnSc1
•	•	k?
1998	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
1999	#num#	k4
Eddie	Eddie	k1gFnSc1
Irvine	Irvin	k1gInSc5
•	•	k?
2000	#num#	k4
Rubens	Rubensa	k1gFnPc2
Barrichello	Barrichello	k1gNnSc1
•	•	k?
2001	#num#	k4
Ralf	Ralf	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2002	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2003	#num#	k4
Juan	Juan	k1gMnSc1
Pablo	Pablo	k1gNnSc1
Montoya	Montoya	k1gMnSc1
•	•	k?
2004	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2005	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2006	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2007	#num#	k4
•	•	k?
2008	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2009	#num#	k4
Mark	Mark	k1gMnSc1
Webber	Webber	k1gMnSc1
•	•	k?
2010	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2011	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2012	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2013	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2014	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2015	#num#	k4
•	•	k?
2016	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2017	#num#	k4
•	•	k?
2018	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2019	#num#	k4
Max	max	kA
Verstappen	Verstappen	k2eAgInSc4d1
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Singapuru	Singapur	k1gInSc2
</s>
<s>
2008	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2009	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2010	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2011	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2012	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2013	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2014	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2015	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2016	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2017	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2018	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2019	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2020	#num#	k4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Španělska	Španělsko	k1gNnSc2
</s>
<s>
1951	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
•	•	k?
1952	#num#	k4
<g/>
–	–	k?
<g/>
1953	#num#	k4
•	•	k?
1954	#num#	k4
Mike	Mike	k1gInSc1
Hawthorn	Hawthorn	k1gInSc4
•	•	k?
1955	#num#	k4
<g/>
–	–	k?
<g/>
1967	#num#	k4
•	•	k?
1968	#num#	k4
Graham	graham	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1969	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1970	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1971	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1972	#num#	k4
Emerson	Emerson	k1gNnSc1
Fittipaldi	Fittipald	k1gMnPc1
•	•	k?
1973	#num#	k4
Emerson	Emerson	k1gNnSc1
Fittipaldi	Fittipald	k1gMnPc1
•	•	k?
1974	#num#	k4
Niki	Nik	k1gFnSc2
Lauda	Lauda	k1gFnSc1
•	•	k?
1975	#num#	k4
Jochen	Jochen	k2eAgMnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
Mass	Mass	k1gInSc1
•	•	k?
1976	#num#	k4
James	James	k1gInSc1
Hunt	hunt	k1gInSc4
•	•	k?
1977	#num#	k4
Mario	Mario	k1gMnSc1
Andretti	Andretti	k1gNnSc1
•	•	k?
1978	#num#	k4
Mario	Mario	k1gMnSc1
Andretti	Andretti	k1gNnSc1
•	•	k?
1979	#num#	k4
Patrick	Patrick	k1gInSc1
Depailler	Depailler	k1gInSc4
•	•	k?
1980	#num#	k4
•	•	k?
1981	#num#	k4
Gilles	Gilles	k1gInSc1
Villeneuve	Villeneuev	k1gFnSc2
•	•	k?
1982	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
•	•	k?
1986	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1987	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1988	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1989	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1990	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1991	#num#	k4
Nigel	Nigela	k1gFnPc2
Mansell	Mansella	k1gFnPc2
•	•	k?
1992	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1993	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1994	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1995	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1996	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1997	#num#	k4
Jacques	Jacques	k1gMnSc1
Villeneuve	Villeneuev	k1gFnSc2
•	•	k?
1998	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
1999	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
2000	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
2001	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2002	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2003	#num#	k4
Michael	Michael	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2004	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2005	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2006	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2007	#num#	k4
Felipe	Felip	k1gInSc5
Massa	Massa	k1gFnSc1
•	•	k?
2008	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2009	#num#	k4
Jenson	Jenson	k1gInSc1
Button	Button	k1gInSc4
•	•	k?
2010	#num#	k4
Mark	Mark	k1gMnSc1
Webber	Webber	k1gMnSc1
•	•	k?
2011	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2012	#num#	k4
Pastor	pastor	k1gMnSc1
Maldonado	Maldonada	k1gFnSc5
•	•	k?
2013	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2014	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2015	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2016	#num#	k4
Max	max	kA
Verstappen	Verstappen	k2eAgInSc1d1
•	•	k?
2017	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2018	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2019	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2020	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Velké	velká	k1gFnSc2
Británie	Británie	k1gFnSc2
</s>
<s>
1951	#num#	k4
José	José	k1gNnSc7
Froilán	Froilán	k2eAgInSc1d1
González	González	k1gInSc1
•	•	k?
1952	#num#	k4
Alberto	Alberta	k1gFnSc5
Ascari	Ascar	k1gInPc7
•	•	k?
1953	#num#	k4
Alberto	Alberta	k1gFnSc5
Ascari	Ascar	k1gInPc7
•	•	k?
1954	#num#	k4
José	José	k1gNnSc6
Froilán	Froilán	k2eAgInSc4d1
González	González	k1gInSc4
•	•	k?
1955	#num#	k4
Stirling	Stirling	k1gInSc1
Moss	Moss	k1gInSc4
•	•	k?
1956	#num#	k4
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
•	•	k?
1957	#num#	k4
•	•	k?
1958	#num#	k4
Peter	Petra	k1gFnPc2
Collins	Collinsa	k1gFnPc2
•	•	k?
1959	#num#	k4
Jack	Jack	k1gInSc1
Brabham	Brabham	k1gInSc4
•	•	k?
1960	#num#	k4
Jack	Jack	k1gInSc1
Brabham	Brabham	k1gInSc4
•	•	k?
1961	#num#	k4
Wolfgang	Wolfgang	k1gMnSc1
von	von	k1gInSc1
Tripps	Tripps	k1gInSc4
•	•	k?
1962	#num#	k4
Jim	on	k3xPp3gFnPc3
Clark	Clark	k1gInSc1
•	•	k?
1963	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Jim	on	k3xPp3gFnPc3
Clark	Clark	k1gInSc1
•	•	k?
1964	#num#	k4
•	•	k?
1965	#num#	k4
Jim	on	k3xPp3gFnPc3
Clark	Clark	k1gInSc1
•	•	k?
1966	#num#	k4
Jack	Jack	k1gInSc1
Brabham	Brabham	k1gInSc4
•	•	k?
1967	#num#	k4
Jim	on	k3xPp3gFnPc3
Clark	Clark	k1gInSc1
•	•	k?
1968	#num#	k4
Jo	jo	k9
Siffert	Siffert	k1gInSc1
•	•	k?
1969	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1970	#num#	k4
Jochen	Jochen	k2eAgInSc4d1
Rindt	Rindt	k1gInSc4
•	•	k?
1971	#num#	k4
Jackie	Jackie	k1gFnSc2
Stewart	Stewart	k1gMnSc1
•	•	k?
1972	#num#	k4
•	•	k?
1973	#num#	k4
Peter	Petra	k1gFnPc2
Revson	Revsona	k1gFnPc2
•	•	k?
1974	#num#	k4
Jody	jod	k1gInPc1
Scheckter	Scheckter	k1gInSc1
•	•	k?
1975	#num#	k4
Emerson	Emerson	k1gNnSc1
Fittipaldi	Fittipald	k1gMnPc1
•	•	k?
1976	#num#	k4
Niki	Nik	k1gFnSc2
Lauda	Lauda	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1977	#num#	k4
•	•	k?
1978	#num#	k4
Carlos	Carlos	k1gInSc1
Reutemann	Reutemann	k1gInSc4
•	•	k?
1979	#num#	k4
Clay	Claa	k1gFnSc2
Regazzoni	Regazzon	k1gMnPc1
•	•	k?
1980	#num#	k4
Alan	alan	k1gInSc1
Jones	Jones	k1gInSc4
•	•	k?
1981	#num#	k4
John	John	k1gMnSc1
Watson	Watson	k1gMnSc1
•	•	k?
1982	#num#	k4
Niki	Nik	k1gFnSc2
Lauda	Lauda	k1gFnSc1
•	•	k?
1983	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1984	#num#	k4
Niki	Nik	k1gFnSc2
Lauda	Lauda	k1gFnSc1
•	•	k?
1985	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1986	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1987	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1988	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1989	#num#	k4
Alain	Alain	k1gInSc1
Prost	prost	k2eAgInSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1990	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1991	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1992	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1993	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1994	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1995	#num#	k4
Johnny	Johnna	k1gFnSc2
Herbert	Herbert	k1gInSc1
•	•	k?
1996	#num#	k4
Jacques	Jacques	k1gMnSc1
Villeneuve	Villeneuev	k1gFnSc2
•	•	k?
1997	#num#	k4
Jacques	Jacques	k1gMnSc1
Villeneuve	Villeneuev	k1gFnSc2
•	•	k?
1998	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1999	#num#	k4
David	David	k1gMnSc1
Coulthard	Coulthard	k1gMnSc1
•	•	k?
2000	#num#	k4
David	David	k1gMnSc1
Coulthard	Coulthard	k1gMnSc1
•	•	k?
2001	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
2002	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Michael	Michael	k1gMnSc1
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2003	#num#	k4
Rubens	Rubensa	k1gFnPc2
Barrichello	Barrichello	k1gNnSc1
•	•	k?
2004	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2005	#num#	k4
Juan	Juan	k1gMnSc1
Pablo	Pablo	k1gNnSc1
Montoya	Montoya	k1gMnSc1
•	•	k?
2006	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2007	#num#	k4
Kimi	Kimi	k1gNnPc2
Räikkönen	Räikkönna	k1gFnPc2
•	•	k?
2008	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2009	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2010	#num#	k4
Mark	Mark	k1gMnSc1
Webber	Webber	k1gMnSc1
•	•	k?
2011	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2012	#num#	k4
Mark	Mark	k1gMnSc1
Webber	Webber	k1gMnSc1
•	•	k?
2013	#num#	k4
Nico	Nico	k6eAd1
Rosberg	Rosberg	k1gInSc1
•	•	k?
2014	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2015	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2016	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2017	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2018	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2019	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2020	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
</s>
<s>
Vítězové	vítěz	k1gMnPc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
San	San	k1gFnSc2
Marina	Marina	k1gFnSc1
</s>
<s>
1981	#num#	k4
Nelson	Nelson	k1gMnSc1
Piquet	Piquet	k1gMnSc1
•	•	k?
1982	#num#	k4
Didier	Didier	k1gInSc1
Pironi	Piroň	k1gFnSc6
•	•	k?
1983	#num#	k4
Patrick	Patrick	k1gInSc1
Tambay	Tambaa	k1gFnSc2
•	•	k?
1984	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1985	#num#	k4
Elio	Elio	k1gNnSc4
de	de	k?
Angelis	Angelis	k1gInSc1
•	•	k?
<g/>
1986	#num#	k4
Alain	Alain	k2eAgInSc4d1
Prost	prost	k2eAgInSc4d1
•	•	k?
1987	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1988	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1989	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1990	#num#	k4
Riccardo	Riccardo	k1gNnSc4
Patrese	Patrese	k1gFnSc2
•	•	k?
1991	#num#	k4
Ayrton	Ayrton	k1gInSc1
Senna	Senn	k1gInSc2
•	•	k?
1992	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1993	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Alain	Alain	k1gInSc1
Prost	prost	k2eAgInSc1d1
•	•	k?
1994	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
1995	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1996	#num#	k4
Damon	Damon	k1gInSc1
Hill	Hill	k1gInSc4
•	•	k?
1997	#num#	k4
Heinz-Harald	Heinz-Harald	k1gInSc1
Frentzen	Frentzen	k2eAgInSc1d1
•	•	k?
1998	#num#	k4
David	David	k1gMnSc1
Coulthard	Coulthard	k1gMnSc1
•	•	k?
1999	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2000	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2001	#num#	k4
Ralf	Ralf	k1gInSc1
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2002	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2003	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2004	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2005	#num#	k4
Fernando	Fernanda	k1gFnSc5
Alonso	Alonsa	k1gFnSc5
•	•	k?
2006	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
132674416	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7843	#num#	k4
0917	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2005017325	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
47000102	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2005017325	#num#	k4
</s>
