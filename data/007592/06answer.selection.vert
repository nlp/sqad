<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
federalizována	federalizován	k2eAgFnSc1d1	federalizována
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Česká	český	k2eAgFnSc1d1	Česká
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
formálně	formálně	k6eAd1	formálně
svrchovaný	svrchovaný	k2eAgInSc1d1	svrchovaný
národní	národní	k2eAgInSc1d1	národní
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
přímý	přímý	k2eAgMnSc1d1	přímý
předchůdce	předchůdce	k1gMnSc1	předchůdce
současné	současný	k2eAgFnSc2d1	současná
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
