<p>
<s>
Karelská	karelský	k2eAgFnSc1d1	Karelská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
karelsky	karelsky	k6eAd1	karelsky
Karjalan	Karjalan	k1gMnSc1	Karjalan
Tazavalta	Tazavalta	k1gMnSc1	Tazavalta
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
Р	Р	k?	Р
К	К	k?	К
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
republika	republika	k1gFnSc1	republika
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1923	[number]	k4	1923
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
RSFSR	RSFSR	kA	RSFSR
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
172	[number]	k4	172
400	[number]	k4	400
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
697	[number]	k4	697
521	[number]	k4	521
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Petrozavodsk	Petrozavodsk	k1gInSc1	Petrozavodsk
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
v	v	k7c4	v
jediné	jediný	k2eAgFnPc4d1	jediná
z	z	k7c2	z
republik	republika	k1gFnPc2	republika
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
není	být	k5eNaImIp3nS	být
její	její	k3xOp3gInSc1	její
titulární	titulární	k2eAgInSc1d1	titulární
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
karelština	karelština	k1gFnSc1	karelština
<g/>
,	,	kIx,	,
jazykem	jazyk	k1gInSc7	jazyk
úředním	úřední	k2eAgInSc7d1	úřední
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
většinu	většina	k1gFnSc4	většina
historického	historický	k2eAgNnSc2d1	historické
území	území	k1gNnSc2	území
Karelie	Karelie	k1gFnSc2	Karelie
(	(	kIx(	(
<g/>
zbytek	zbytek	k1gInSc4	zbytek
náleží	náležet	k5eAaImIp3nP	náležet
Finsku	Finsko	k1gNnSc3	Finsko
a	a	k8xC	a
Leningradské	leningradský	k2eAgFnSc3d1	Leningradská
oblasti	oblast	k1gFnSc3	oblast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
nenacházejí	nacházet	k5eNaImIp3nP	nacházet
téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgFnPc4	žádný
hory	hora	k1gFnPc4	hora
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zato	zato	k6eAd1	zato
mnoho	mnoho	k4c4	mnoho
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poloha	poloha	k1gFnSc1	poloha
===	===	k?	===
</s>
</p>
<p>
<s>
Karélie	Karélie	k1gFnSc1	Karélie
je	být	k5eAaImIp3nS	být
severská	severský	k2eAgFnSc1d1	severská
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc7	její
severní	severní	k2eAgFnSc7d1	severní
hranicí	hranice	k1gFnSc7	hranice
probíhá	probíhat	k5eAaImIp3nS	probíhat
severní	severní	k2eAgInSc4d1	severní
polární	polární	k2eAgInSc4d1	polární
kruh	kruh	k1gInSc4	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Republika	republika	k1gFnSc1	republika
hraničí	hraničit	k5eAaImIp3nS	hraničit
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
oblastmi	oblast	k1gFnPc7	oblast
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
Murmanskou	murmanský	k2eAgFnSc7d1	Murmanská
oblastí	oblast	k1gFnSc7	oblast
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
Archangelskou	archangelský	k2eAgFnSc7d1	Archangelská
oblastí	oblast	k1gFnSc7	oblast
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
Vologdskou	Vologdský	k2eAgFnSc7d1	Vologdský
oblastí	oblast	k1gFnSc7	oblast
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
a	a	k8xC	a
Leningradskou	leningradský	k2eAgFnSc7d1	Leningradská
oblastí	oblast	k1gFnSc7	oblast
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
pěti	pět	k4xCc3	pět
provinciemi	provincie	k1gFnPc7	provincie
Finska	Finsko	k1gNnSc2	Finsko
(	(	kIx(	(
<g/>
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
k	k	k7c3	k
severu	sever	k1gInSc3	sever
postupně	postupně	k6eAd1	postupně
Jižní	jižní	k2eAgFnSc1d1	jižní
Karélií	Karélie	k1gFnSc7	Karélie
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc7d1	severní
Karélií	Karélie	k1gFnSc7	Karélie
<g/>
,	,	kIx,	,
Kainuu	Kainuum	k1gNnSc6	Kainuum
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Pohjanmaa	Pohjanma	k2eAgFnSc1d1	Pohjanma
a	a	k8xC	a
Laponskem	Laponsko	k1gNnSc7	Laponsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
břehy	břeh	k1gInPc1	břeh
omývá	omývat	k5eAaImIp3nS	omývat
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Bílé	bílý	k2eAgNnSc1d1	bílé
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Oněžské	oněžský	k2eAgNnSc1d1	Oněžské
jezero	jezero	k1gNnSc1	jezero
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Ladožské	ladožský	k2eAgNnSc1d1	Ladožské
jezero	jezero	k1gNnSc1	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klima	klima	k1gNnSc1	klima
===	===	k?	===
</s>
</p>
<p>
<s>
Klima	klima	k1gNnSc1	klima
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
rysy	rys	k1gInPc7	rys
klimatu	klima	k1gNnSc2	klima
kontinentálního	kontinentální	k2eAgNnSc2d1	kontinentální
i	i	k8xC	i
přímořského	přímořský	k2eAgNnSc2d1	přímořské
<g/>
.	.	kIx.	.
</s>
<s>
Zima	zima	k1gFnSc1	zima
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
mírná	mírný	k2eAgFnSc1d1	mírná
ale	ale	k8xC	ale
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
léto	léto	k1gNnSc1	léto
je	být	k5eAaImIp3nS	být
chladné	chladný	k2eAgNnSc1d1	chladné
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1	relativní
vlhkost	vlhkost	k1gFnSc1	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
změny	změna	k1gFnPc1	změna
teplot	teplota	k1gFnPc2	teplota
nejsou	být	k5eNaImIp3nP	být
velké	velký	k2eAgFnPc1d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
14	[number]	k4	14
až	až	k9	až
16	[number]	k4	16
°	°	k?	°
<g/>
С	С	k?	С
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
-9	-9	k4	-9
až	až	k9	až
-13	-13	k4	-13
°	°	k?	°
<g/>
С	С	k?	С
Průměrné	průměrný	k2eAgNnSc4d1	průměrné
množství	množství	k1gNnSc4	množství
srážek	srážka	k1gFnPc2	srážka
za	za	k7c4	za
rok	rok	k1gInSc4	rok
je	být	k5eAaImIp3nS	být
600	[number]	k4	600
mm	mm	kA	mm
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
a	a	k8xC	a
400	[number]	k4	400
až	až	k9	až
500	[number]	k4	500
mm	mm	kA	mm
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jich	on	k3xPp3gFnPc2	on
spadne	spadnout	k5eAaPmIp3nS	spadnout
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reliéf	reliéf	k1gInSc1	reliéf
===	===	k?	===
</s>
</p>
<p>
<s>
Republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Baltského	baltský	k2eAgInSc2d1	baltský
štítu	štít	k1gInSc2	štít
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
území	území	k1gNnSc1	území
představuje	představovat	k5eAaImIp3nS	představovat
kopcovitou	kopcovitý	k2eAgFnSc4d1	kopcovitá
rovinu	rovina	k1gFnSc4	rovina
s	s	k7c7	s
jasně	jasně	k6eAd1	jasně
vyjádřenými	vyjádřený	k2eAgFnPc7d1	vyjádřená
stopami	stopa	k1gFnPc7	stopa
vlivu	vliv	k1gInSc2	vliv
ledovce	ledovec	k1gInSc2	ledovec
(	(	kIx(	(
<g/>
morénové	morénový	k2eAgInPc1d1	morénový
valy	val	k1gInPc1	val
<g/>
,	,	kIx,	,
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
kruhové	kruhový	k2eAgNnSc1d1	kruhové
(	(	kIx(	(
<g/>
kamy	kamy	k?	kamy
<g/>
)	)	kIx)	)
a	a	k8xC	a
podlouhlé	podlouhlý	k2eAgNnSc1d1	podlouhlé
(	(	kIx(	(
<g/>
ozy	ozy	k?	ozy
<g/>
)	)	kIx)	)
kopce	kopec	k1gInSc2	kopec
a	a	k8xC	a
jezerní	jezerní	k2eAgFnSc2d1	jezerní
kotliny	kotlina	k1gFnSc2	kotlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
místa	místo	k1gNnPc1	místo
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
severozápadě	severozápad	k1gInSc6	severozápad
hřbetu	hřbet	k1gInSc2	hřbet
Mansalkja	Mansalkja	k1gFnSc1	Mansalkja
(	(	kIx(	(
<g/>
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
do	do	k7c2	do
578	[number]	k4	578
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Západokarelská	Západokarelský	k2eAgFnSc1d1	Západokarelský
vysočina	vysočina	k1gFnSc1	vysočina
(	(	kIx(	(
<g/>
do	do	k7c2	do
417	[number]	k4	417
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
nížiny	nížina	k1gFnPc1	nížina
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
jezer	jezero	k1gNnPc2	jezero
(	(	kIx(	(
<g/>
Přibělomorská	Přibělomorský	k2eAgFnSc1d1	Přibělomorský
<g/>
,	,	kIx,	,
Oloněcká	Oloněcký	k2eAgFnSc1d1	Oloněcký
<g/>
,	,	kIx,	,
Vodlinská	Vodlinský	k2eAgFnSc1d1	Vodlinský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
Bílého	bílý	k2eAgNnSc2d1	bílé
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
Pomorský	Pomorský	k2eAgInSc1d1	Pomorský
a	a	k8xC	a
Karelský	karelský	k2eAgInSc1d1	karelský
břeh	břeh	k1gInSc1	břeh
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
zálivů	záliv	k1gInPc2	záliv
a	a	k8xC	a
zátok	zátoka	k1gFnPc2	zátoka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nerosty	nerost	k1gInPc4	nerost
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
naleziště	naleziště	k1gNnSc4	naleziště
různých	různý	k2eAgInPc2d1	různý
stavebních	stavební	k2eAgInPc2d1	stavební
materiálů	materiál	k1gInPc2	materiál
(	(	kIx(	(
<g/>
žula	žula	k1gFnSc1	žula
<g/>
,	,	kIx,	,
diabas	diabas	k1gInSc1	diabas
<g/>
,	,	kIx,	,
křemenec	křemenec	k1gInSc1	křemenec
<g/>
,	,	kIx,	,
dolomity	dolomit	k1gInPc1	dolomit
<g/>
,	,	kIx,	,
mramor	mramor	k1gInSc1	mramor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pegmatit	pegmatit	k1gInSc1	pegmatit
<g/>
,	,	kIx,	,
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
<g/>
,	,	kIx,	,
železité	železitý	k2eAgInPc1d1	železitý
křemence	křemenec	k1gInPc1	křemenec
(	(	kIx(	(
<g/>
Kostomukšskogo	Kostomukšskogo	k6eAd1	Kostomukšskogo
<g/>
)	)	kIx)	)
a	a	k8xC	a
titanomagnetity	titanomagnetit	k1gInPc1	titanomagnetit
(	(	kIx(	(
<g/>
Pudožgorskogo	Pudožgorskogo	k6eAd1	Pudožgorskogo
<g/>
)	)	kIx)	)
a	a	k8xC	a
slídy	slída	k1gFnSc2	slída
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vodní	vodní	k2eAgInPc1d1	vodní
zdroje	zdroj	k1gInPc1	zdroj
===	===	k?	===
</s>
</p>
<p>
<s>
Karélie	Karélie	k1gFnSc1	Karélie
je	být	k5eAaImIp3nS	být
kraj	kraj	k1gInSc1	kraj
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
bažin	bažina	k1gFnPc2	bažina
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	řeka	k1gFnPc1	řeka
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
úmoří	úmoří	k1gNnSc3	úmoří
Bílého	bílé	k1gNnSc2	bílé
a	a	k8xC	a
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
velké	velký	k2eAgInPc4d1	velký
průtoky	průtok	k1gInPc4	průtok
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
peřeje	peřej	k1gInPc4	peřej
a	a	k8xC	a
vodopády	vodopád	k1gInPc4	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
jsou	být	k5eAaImIp3nP	být
Kem	Kem	k1gFnPc1	Kem
<g/>
,	,	kIx,	,
Vyg	Vyg	k1gFnSc1	Vyg
<g/>
,	,	kIx,	,
Kereť	Kereť	k1gFnSc1	Kereť
ústící	ústící	k2eAgFnSc1d1	ústící
do	do	k7c2	do
Bílého	bílý	k2eAgNnSc2d1	bílé
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
Vodla	Vodlo	k1gNnSc2	Vodlo
<g/>
,	,	kIx,	,
Suna	suna	k1gFnSc1	suna
a	a	k8xC	a
Šuja	Šuja	k1gFnSc1	Šuja
ústící	ústící	k2eAgFnSc1d1	ústící
do	do	k7c2	do
Oněžského	oněžský	k2eAgNnSc2d1	Oněžské
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	řeka	k1gFnPc1	řeka
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
jako	jako	k9	jako
zdroje	zdroj	k1gInPc1	zdroj
hydroenergie	hydroenergie	k1gFnSc2	hydroenergie
a	a	k8xC	a
pro	pro	k7c4	pro
splavování	splavování	k1gNnSc4	splavování
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Splavné	splavný	k2eAgInPc1d1	splavný
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
dolní	dolní	k2eAgInPc1d1	dolní
toky	tok	k1gInPc1	tok
největších	veliký	k2eAgFnPc2d3	veliký
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Jezera	jezero	k1gNnSc2	jezero
tvoří	tvořit	k5eAaImIp3nS	tvořit
18	[number]	k4	18
%	%	kIx~	%
území	území	k1gNnSc6	území
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
jsou	být	k5eAaImIp3nP	být
Ladožské	ladožský	k2eAgFnPc1d1	Ladožská
a	a	k8xC	a
Oněžské	oněžský	k2eAgFnPc1d1	oněžský
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Vygozero	Vygozero	k1gNnSc1	Vygozero
<g/>
,	,	kIx,	,
Topozero	Topozero	k1gNnSc1	Topozero
<g/>
,	,	kIx,	,
Pjaozero	Pjaozero	k1gNnSc1	Pjaozero
a	a	k8xC	a
Segozero	Segozero	k1gNnSc1	Segozero
<g/>
.	.	kIx.	.
</s>
<s>
Jezera	jezero	k1gNnPc1	jezero
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
spojená	spojený	k2eAgFnSc1d1	spojená
průtoky	průtok	k1gInPc7	průtok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Půdy	půda	k1gFnPc1	půda
a	a	k8xC	a
flóra	flóra	k1gFnSc1	flóra
===	===	k?	===
</s>
</p>
<p>
<s>
Základní	základní	k2eAgInPc1d1	základní
typy	typ	k1gInPc1	typ
půd	půda	k1gFnPc2	půda
jsou	být	k5eAaImIp3nP	být
podzolové	podzolový	k2eAgFnPc1d1	podzolová
<g/>
,	,	kIx,	,
bažinato-podzolové	bažinatoodzolový	k2eAgFnPc1d1	bažinato-podzolový
a	a	k8xC	a
bažinaté	bažinatý	k2eAgFnPc1d1	bažinatá
<g/>
.	.	kIx.	.
</s>
<s>
Karélie	Karélie	k1gFnSc1	Karélie
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
lesy	les	k1gInPc4	les
(	(	kIx(	(
<g/>
borovice	borovice	k1gFnPc4	borovice
<g/>
,	,	kIx,	,
smrky	smrk	k1gInPc4	smrk
<g/>
,	,	kIx,	,
karelské	karelský	k2eAgFnPc4d1	Karelská
břízy	bříza	k1gFnPc4	bříza
<g/>
,	,	kIx,	,
olše	olše	k1gFnPc4	olše
<g/>
,	,	kIx,	,
osiky	osika	k1gFnPc4	osika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zabírají	zabírat	k5eAaImIp3nP	zabírat
polovinu	polovina	k1gFnSc4	polovina
rozlohy	rozloha	k1gFnSc2	rozloha
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Bažiny	bažina	k1gFnPc1	bažina
zabírají	zabírat	k5eAaImIp3nP	zabírat
18	[number]	k4	18
%	%	kIx~	%
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fauna	fauna	k1gFnSc1	fauna
===	===	k?	===
</s>
</p>
<p>
<s>
Zastoupení	zastoupení	k1gNnSc1	zastoupení
živočichů	živočich	k1gMnPc2	živočich
je	být	k5eAaImIp3nS	být
rozmanité	rozmanitý	k2eAgNnSc1d1	rozmanité
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
zde	zde	k6eAd1	zde
sněžní	sněžní	k2eAgMnPc1d1	sněžní
zajíci	zajíc	k1gMnPc1	zajíc
<g/>
,	,	kIx,	,
bobři	bobr	k1gMnPc1	bobr
<g/>
,	,	kIx,	,
ondatry	ondatra	k1gFnPc1	ondatra
(	(	kIx(	(
<g/>
aklimatizované	aklimatizovaný	k2eAgFnPc1d1	aklimatizovaná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hnědí	hnědý	k2eAgMnPc1d1	hnědý
medvědi	medvěd	k1gMnPc1	medvěd
<g/>
,	,	kIx,	,
sobi	sob	k1gMnPc1	sob
a	a	k8xC	a
losi	los	k1gMnPc1	los
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
význam	význam	k1gInSc1	význam
mají	mít	k5eAaImIp3nP	mít
mořské	mořský	k2eAgFnPc1d1	mořská
i	i	k8xC	i
sladkovodní	sladkovodní	k2eAgFnPc1d1	sladkovodní
ryby	ryba	k1gFnPc1	ryba
(	(	kIx(	(
<g/>
navagy	navaga	k1gFnPc1	navaga
<g/>
,	,	kIx,	,
sledi	sleď	k1gMnPc1	sleď
<g/>
,	,	kIx,	,
tresky	treska	k1gFnPc1	treska
<g/>
,	,	kIx,	,
platýsi	platýs	k1gMnPc5	platýs
<g/>
,	,	kIx,	,
lososi	losos	k1gMnPc5	losos
<g/>
,	,	kIx,	,
pstruzi	pstruh	k1gMnPc5	pstruh
<g/>
)	)	kIx)	)
a	a	k8xC	a
tuleni	tulen	k2eAgMnPc1d1	tulen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rezervace	rezervace	k1gFnSc1	rezervace
Kivač	Kivač	k1gInSc1	Kivač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Karélie	Karélie	k1gFnSc1	Karélie
byla	být	k5eAaImAgFnS	být
osídlena	osídlit	k5eAaPmNgFnS	osídlit
již	již	k9	již
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
okolo	okolo	k7c2	okolo
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
oblast	oblast	k1gFnSc1	oblast
obsadila	obsadit	k5eAaPmAgFnS	obsadit
Novgorodská	novgorodský	k2eAgFnSc1d1	Novgorodská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
pod	pod	k7c4	pod
vliv	vliv	k1gInSc4	vliv
Moskvy	Moskva	k1gFnSc2	Moskva
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
častým	častý	k2eAgInSc7d1	častý
terčem	terč	k1gInSc7	terč
útoků	útok	k1gInPc2	útok
Švédů	Švéd	k1gMnPc2	Švéd
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ukusovali	ukusovat	k5eAaImAgMnP	ukusovat
z	z	k7c2	z
jejího	její	k3xOp3gNnSc2	její
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
zase	zase	k9	zase
ustálil	ustálit	k5eAaPmAgInS	ustálit
původní	původní	k2eAgInSc1d1	původní
stav	stav	k1gInSc1	stav
–	–	k?	–
Karélie	Karélie	k1gFnSc1	Karélie
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgFnPc6d1	poslední
dobách	doba	k1gFnPc6	doba
carského	carský	k2eAgNnSc2d1	carské
Ruska	Rusko	k1gNnSc2	Rusko
se	se	k3xPyFc4	se
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
výroba	výroba	k1gFnSc1	výroba
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
říjnové	říjnový	k2eAgFnSc6d1	říjnová
revoluci	revoluce	k1gFnSc6	revoluce
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Karelská	karelský	k2eAgFnSc1d1	Karelská
pracovní	pracovní	k2eAgFnSc1d1	pracovní
komuna	komuna	k1gFnSc1	komuna
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
přeměněna	přeměnit	k5eAaPmNgFnS	přeměnit
na	na	k7c4	na
Karelskou	karelský	k2eAgFnSc4d1	Karelská
ASSR	ASSR	kA	ASSR
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
zimní	zimní	k2eAgFnSc6d1	zimní
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
k	k	k7c3	k
ní	on	k3xPp3gFnSc2	on
byla	být	k5eAaImAgFnS	být
přičleněna	přičleněn	k2eAgFnSc1d1	přičleněna
část	část	k1gFnSc1	část
Finské	finský	k2eAgFnSc2d1	finská
Karélie	Karélie	k1gFnSc2	Karélie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
reorganizována	reorganizován	k2eAgFnSc1d1	reorganizována
jako	jako	k8xC	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
<g/>
,	,	kIx,	,
šestnáctá	šestnáctý	k4xOgFnSc1	šestnáctý
sovětská	sovětský	k2eAgFnSc1d1	sovětská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Karelofinská	Karelofinský	k2eAgFnSc1d1	Karelofinský
SSR	SSR	kA	SSR
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
status	status	k1gInSc4	status
země	zem	k1gFnSc2	zem
opět	opět	k6eAd1	opět
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c6	na
ASSR	ASSR	kA	ASSR
<g/>
.	.	kIx.	.
</s>
<s>
Současného	současný	k2eAgNnSc2d1	současné
postavení	postavení	k1gNnSc2	postavení
požívá	požívat	k5eAaImIp3nS	požívat
oblast	oblast	k1gFnSc1	oblast
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
současných	současný	k2eAgMnPc2d1	současný
obyvatel	obyvatel	k1gMnPc2	obyvatel
Karélie	Karélie	k1gFnSc2	Karélie
jsou	být	k5eAaImIp3nP	být
Rusové	Rus	k1gMnPc1	Rus
(	(	kIx(	(
<g/>
76,6	[number]	k4	76,6
%	%	kIx~	%
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
,	,	kIx,	,
Karelové	Karel	k1gMnPc1	Karel
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
9,2	[number]	k4	9,2
%	%	kIx~	%
(	(	kIx(	(
<g/>
ještě	ještě	k6eAd1	ještě
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
40	[number]	k4	40
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následují	následovat	k5eAaImIp3nP	následovat
Bělorusové	Bělorus	k1gMnPc1	Bělorus
(	(	kIx(	(
<g/>
5,3	[number]	k4	5,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
(	(	kIx(	(
<g/>
2,7	[number]	k4	2,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Finové	Fin	k1gMnPc1	Fin
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vepsové	Vepsové	k2eAgFnSc1d1	Vepsové
(	(	kIx(	(
<g/>
0,7	[number]	k4	0,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
ortodoxní	ortodoxní	k2eAgNnSc4d1	ortodoxní
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Karélie	Karélie	k1gFnSc1	Karélie
je	být	k5eAaImIp3nS	být
proslulá	proslulý	k2eAgFnSc1d1	proslulá
dřevěnými	dřevěný	k2eAgInPc7d1	dřevěný
kostely	kostel	k1gInPc1	kostel
s	s	k7c7	s
cibulovitými	cibulovitý	k2eAgFnPc7d1	cibulovitá
báněmi	báně	k1gFnPc7	báně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
těžbě	těžba	k1gFnSc6	těžba
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc1	zemědělství
není	být	k5eNaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
severní	severní	k2eAgFnSc3d1	severní
poloze	poloha	k1gFnSc3	poloha
oblasti	oblast	k1gFnSc2	oblast
vůbec	vůbec	k9	vůbec
možné	možný	k2eAgInPc1d1	možný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Karélii	Karélie	k1gFnSc6	Karélie
<g/>
,	,	kIx,	,
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Petrozavodsku	Petrozavodsek	k1gInSc6	Petrozavodsek
je	být	k5eAaImIp3nS	být
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
dřevozpracujících	dřevozpracující	k2eAgInPc2d1	dřevozpracující
podniků	podnik	k1gInPc2	podnik
a	a	k8xC	a
také	také	k9	také
papíren	papírna	k1gFnPc2	papírna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
je	být	k5eAaImIp3nS	být
Karelská	karelský	k2eAgFnSc1d1	Karelská
republika	republika	k1gFnSc1	republika
s	s	k7c7	s
dluhem	dluh	k1gInSc7	dluh
114,1	[number]	k4	114,1
miliard	miliarda	k4xCgFnPc2	miliarda
rublů	rubl	k1gInPc2	rubl
třetí	třetí	k4xOgFnSc7	třetí
nejzadluženější	zadlužený	k2eAgFnSc7d3	nejzadluženější
oblastí	oblast	k1gFnSc7	oblast
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ostré	ostrý	k2eAgFnSc2d1	ostrá
rozpočtové	rozpočtový	k2eAgFnSc2d1	rozpočtová
krize	krize	k1gFnSc2	krize
plánuje	plánovat	k5eAaImIp3nS	plánovat
ruská	ruský	k2eAgFnSc1d1	ruská
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
takřka	takřka	k6eAd1	takřka
sedminásobně	sedminásobně	k6eAd1	sedminásobně
snížit	snížit	k5eAaPmF	snížit
objem	objem	k1gInSc4	objem
rozpočtových	rozpočtový	k2eAgInPc2d1	rozpočtový
úvěrů	úvěr	k1gInPc2	úvěr
pro	pro	k7c4	pro
regiony	region	k1gInPc4	region
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dostane	dostat	k5eAaPmIp3nS	dostat
Karelskou	karelský	k2eAgFnSc4d1	Karelská
republiku	republika	k1gFnSc4	republika
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
19	[number]	k4	19
regiony	region	k1gInPc1	region
na	na	k7c4	na
pokraj	pokraj	k1gInSc4	pokraj
bankrotu	bankrot	k1gInSc2	bankrot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
К	К	k?	К
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Republika	republika	k1gFnSc1	republika
Karélie	Karélie	k1gFnSc2	Karélie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
