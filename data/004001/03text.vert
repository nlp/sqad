<s>
Nadávka	nadávka	k1gFnSc1	nadávka
je	být	k5eAaImIp3nS	být
úmyslně	úmyslně	k6eAd1	úmyslně
hanlivé	hanlivý	k2eAgNnSc1d1	hanlivé
nebo	nebo	k8xC	nebo
urážlivé	urážlivý	k2eAgNnSc1d1	urážlivé
pojmenování	pojmenování	k1gNnSc1	pojmenování
nebo	nebo	k8xC	nebo
oslovení	oslovení	k1gNnSc1	oslovení
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
označení	označení	k1gNnSc4	označení
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
abstraktního	abstraktní	k2eAgInSc2d1	abstraktní
nebo	nebo	k8xC	nebo
reálného	reálný	k2eAgInSc2d1	reálný
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
procesu	proces	k1gInSc2	proces
nebo	nebo	k8xC	nebo
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nadávka	nadávka	k1gFnSc1	nadávka
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
prosté	prostý	k2eAgNnSc4d1	prosté
vyjádření	vyjádření	k1gNnSc4	vyjádření
emoce	emoce	k1gFnSc2	emoce
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
citoslovce	citoslovce	k1gNnSc4	citoslovce
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
napodobení	napodobení	k1gNnSc1	napodobení
tohoto	tento	k3xDgNnSc2	tento
vyjádření	vyjádření	k1gNnSc2	vyjádření
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
nějakého	nějaký	k3yIgNnSc2	nějaký
úsilí	úsilí	k1gNnSc2	úsilí
a	a	k8xC	a
také	také	k9	také
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
stresu	stres	k1gInSc2	stres
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgFnS	použít
osobou	osoba	k1gFnSc7	osoba
která	který	k3yIgNnPc4	který
trpí	trpět	k5eAaImIp3nP	trpět
nedostatkem	nedostatek	k1gInSc7	nedostatek
pozornosti	pozornost	k1gFnSc2	pozornost
k	k	k7c3	k
zvýraznění	zvýraznění	k1gNnSc3	zvýraznění
se	se	k3xPyFc4	se
v	v	k7c6	v
kolektivu	kolektiv	k1gInSc6	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
slovního	slovní	k2eAgInSc2d1	slovní
osobního	osobní	k2eAgInSc2d1	osobní
útoku	útok	k1gInSc2	útok
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
aktem	akt	k1gInSc7	akt
agrese	agrese	k1gFnSc2	agrese
nebo	nebo	k8xC	nebo
provokace	provokace	k1gFnSc2	provokace
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
vrčení	vrčení	k1gNnSc4	vrčení
nebo	nebo	k8xC	nebo
štěkání	štěkání	k1gNnSc4	štěkání
u	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
posměšky	posměšek	k1gInPc1	posměšek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nadávky	nadávka	k1gFnPc1	nadávka
se	se	k3xPyFc4	se
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
výhradně	výhradně	k6eAd1	výhradně
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
sprostá	sprostý	k2eAgNnPc1d1	sprosté
slova	slovo	k1gNnPc1	slovo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
použití	použití	k1gNnSc3	použití
nadávky	nadávka	k1gFnSc2	nadávka
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
třeba	třeba	k9	třeba
interpreta	interpret	k1gMnSc4	interpret
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
i	i	k9	i
adresáta	adresát	k1gMnSc4	adresát
<g/>
.	.	kIx.	.
</s>
<s>
Opakované	opakovaný	k2eAgNnSc1d1	opakované
používání	používání	k1gNnSc1	používání
nadávek	nadávka	k1gFnPc2	nadávka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
symptomem	symptom	k1gInSc7	symptom
duševní	duševní	k2eAgFnPc4d1	duševní
choroby	choroba	k1gFnPc4	choroba
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Touretteův	Touretteův	k2eAgInSc1d1	Touretteův
syndrom	syndrom	k1gInSc1	syndrom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
znakem	znak	k1gInSc7	znak
jistého	jistý	k2eAgInSc2d1	jistý
druhu	druh	k1gInSc2	druh
společenského	společenský	k2eAgNnSc2d1	společenské
prostředí	prostředí	k1gNnSc2	prostředí
nebo	nebo	k8xC	nebo
obecně	obecně	k6eAd1	obecně
zaostalosti	zaostalost	k1gFnSc2	zaostalost
<g/>
.	.	kIx.	.
</s>
<s>
Nadávky	nadávka	k1gFnPc1	nadávka
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
oborově	oborově	k6eAd1	oborově
i	i	k9	i
regionálně	regionálně	k6eAd1	regionálně
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
někdy	někdy	k6eAd1	někdy
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
mohou	moct	k5eAaImIp3nP	moct
nadávky	nadávka	k1gFnPc4	nadávka
navíc	navíc	k6eAd1	navíc
měnit	měnit	k5eAaImF	měnit
a	a	k8xC	a
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
nově	nově	k6eAd1	nově
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
šlapka	šlapka	k1gFnSc1	šlapka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
prostitutka	prostitutka	k1gFnSc1	prostitutka
<g/>
)	)	kIx)	)
nepatří	patřit	k5eNaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nadávky	nadávka	k1gFnPc4	nadávka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mezi	mezi	k7c4	mezi
české	český	k2eAgInPc4d1	český
hovorové	hovorový	k2eAgInPc4d1	hovorový
výrazy	výraz	k1gInPc4	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Porozumění	porozumění	k1gNnSc1	porozumění
nadávce	nadávka	k1gFnSc3	nadávka
interpretem	interpret	k1gMnSc7	interpret
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
zásadních	zásadní	k2eAgInPc2d1	zásadní
požadavků	požadavek	k1gInPc2	požadavek
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
použití	použití	k1gNnSc4	použití
<g/>
.	.	kIx.	.
</s>
<s>
Omezení	omezení	k1gNnSc1	omezení
možnosti	možnost	k1gFnSc2	možnost
jejího	její	k3xOp3gNnSc2	její
použití	použití	k1gNnSc2	použití
ja	ja	k?	ja
pak	pak	k6eAd1	pak
dáno	dát	k5eAaPmNgNnS	dát
schopností	schopnost	k1gFnSc7	schopnost
adresáta	adresát	k1gMnSc4	adresát
či	či	k8xC	či
posluchače	posluchač	k1gMnSc4	posluchač
dekódovat	dekódovat	k5eAaBmF	dekódovat
výraz	výraz	k1gInSc4	výraz
jako	jako	k8xS	jako
nadávku	nadávka	k1gFnSc4	nadávka
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
rozeznat	rozeznat	k5eAaPmF	rozeznat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
žert	žert	k1gInSc4	žert
nebo	nebo	k8xC	nebo
o	o	k7c4	o
nadávku	nadávka	k1gFnSc4	nadávka
<g/>
.	.	kIx.	.
</s>
<s>
Porozumění	porozumění	k1gNnSc4	porozumění
nadávce	nadávka	k1gFnSc3	nadávka
<g/>
,	,	kIx,	,
jejímu	její	k3xOp3gInSc3	její
smyslu	smysl	k1gInSc3	smysl
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
vhodná	vhodný	k2eAgFnSc1d1	vhodná
reformulace	reformulace	k1gFnSc1	reformulace
je	být	k5eAaImIp3nS	být
také	také	k9	také
problém	problém	k1gInSc1	problém
překladový	překladový	k2eAgInSc1d1	překladový
a	a	k8xC	a
překladatelský	překladatelský	k2eAgInSc1d1	překladatelský
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nikoliv	nikoliv	k9	nikoliv
pouze	pouze	k6eAd1	pouze
jazykovědný	jazykovědný	k2eAgMnSc1d1	jazykovědný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
dekádách	dekáda	k1gFnPc6	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
preferovány	preferován	k2eAgFnPc4d1	preferována
nadávky	nadávka	k1gFnPc4	nadávka
spojené	spojený	k2eAgFnPc4d1	spojená
se	se	k3xPyFc4	se
sexualitou	sexualita	k1gFnSc7	sexualita
oproti	oproti	k7c3	oproti
dřívější	dřívější	k2eAgFnSc3d1	dřívější
době	doba	k1gFnSc3	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
užívanější	užívaný	k2eAgFnPc4d2	užívanější
nadávky	nadávka	k1gFnPc4	nadávka
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
defekací	defekace	k1gFnSc7	defekace
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvíce	hodně	k6eAd3	hodně
používané	používaný	k2eAgFnPc4d1	používaná
nadávky	nadávka	k1gFnPc4	nadávka
patří	patřit	k5eAaImIp3nP	patřit
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
studie	studie	k1gFnSc2	studie
"	"	kIx"	"
<g/>
trouba	trouba	k1gFnSc1	trouba
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
sakra	sakra	k0	sakra
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
používaný	používaný	k2eAgInSc1d1	používaný
je	být	k5eAaImIp3nS	být
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
košot	košot	k1gInSc1	košot
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiné	jiný	k2eAgFnSc2d1	jiná
studie	studie	k1gFnSc2	studie
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
nejvíce	nejvíce	k6eAd1	nejvíce
používanými	používaný	k2eAgFnPc7d1	používaná
nadávkami	nadávka	k1gFnPc7	nadávka
"	"	kIx"	"
<g/>
blbec	blbec	k1gMnSc1	blbec
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
buzna	buzna	k1gFnSc1	buzna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Slušní	slušný	k2eAgMnPc1d1	slušný
<g/>
"	"	kIx"	"
občané	občan	k1gMnPc1	občan
po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
staletí	staletí	k1gNnPc4	staletí
předstírali	předstírat	k5eAaImAgMnP	předstírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádná	žádný	k3yNgNnPc1	žádný
sprostá	sprostý	k2eAgNnPc1d1	sprosté
slova	slovo	k1gNnPc1	slovo
a	a	k8xC	a
nadávky	nadávka	k1gFnPc1	nadávka
neznají	neznat	k5eAaImIp3nP	neznat
<g/>
.	.	kIx.	.
</s>
<s>
Vulgarismy	vulgarismus	k1gInPc1	vulgarismus
tak	tak	k6eAd1	tak
patřily	patřit	k5eAaImAgInP	patřit
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
"	"	kIx"	"
<g/>
sprostého	sprostý	k2eAgInSc2d1	sprostý
lidu	lid	k1gInSc2	lid
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zločinců	zločinec	k1gMnPc2	zločinec
a	a	k8xC	a
do	do	k7c2	do
hospody	hospody	k?	hospody
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
určité	určitý	k2eAgFnPc4d1	určitá
oblasti	oblast	k1gFnPc4	oblast
a	a	k8xC	a
jazyky	jazyk	k1gInPc4	jazyk
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
určitá	určitý	k2eAgNnPc4d1	určité
kulturní	kulturní	k2eAgNnPc4d1	kulturní
období	období	k1gNnPc4	období
civilizace	civilizace	k1gFnSc2	civilizace
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
typické	typický	k2eAgFnPc4d1	typická
určité	určitý	k2eAgFnPc4d1	určitá
nadávky	nadávka	k1gFnPc4	nadávka
<g/>
,	,	kIx,	,
hanlivá	hanlivý	k2eAgNnPc4d1	hanlivé
označení	označení	k1gNnPc4	označení
a	a	k8xC	a
spojení	spojení	k1gNnPc4	spojení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
negativním	negativní	k2eAgFnPc3d1	negativní
zobrazením	zobrazení	k1gNnSc7	zobrazení
oponentů	oponent	k1gMnPc2	oponent
v	v	k7c4	v
polarizované	polarizovaný	k2eAgFnPc4d1	polarizovaná
společnosti	společnost	k1gFnPc4	společnost
i	i	k8xC	i
prostořeká	prostořeký	k2eAgNnPc4d1	prostořeké
oslovení	oslovení	k1gNnPc4	oslovení
<g/>
,	,	kIx,	,
vězeňské	vězeňský	k2eAgFnPc4d1	vězeňská
<g/>
,	,	kIx,	,
vojenské	vojenský	k2eAgFnPc4d1	vojenská
nebo	nebo	k8xC	nebo
hospodské	hospodský	k2eAgFnPc4d1	hospodská
nadávky	nadávka	k1gFnPc4	nadávka
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
nadávky	nadávka	k1gFnPc4	nadávka
ale	ale	k8xC	ale
přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
věky	věk	k1gInPc4	věk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
z	z	k7c2	z
původně	původně	k6eAd1	původně
starogermánského	starogermánský	k2eAgNnSc2d1	starogermánské
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
schelm	schelm	k6eAd1	schelm
<g/>
"	"	kIx"	"
z	z	k7c2	z
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
němčiny	němčina	k1gFnSc2	němčina
<g/>
,	,	kIx,	,
přijat	přijat	k2eAgInSc1d1	přijat
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
šelma	šelma	k1gFnSc1	šelma
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
označující	označující	k2eAgInPc1d1	označující
původně	původně	k6eAd1	původně
mor	mor	k1gInSc4	mor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
i	i	k9	i
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
získal	získat	k5eAaPmAgInS	získat
výraz	výraz	k1gInSc1	výraz
podobu	podoba	k1gFnSc4	podoba
(	(	kIx(	(
<g/>
dravého	dravý	k2eAgMnSc2d1	dravý
<g/>
)	)	kIx)	)
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
,	,	kIx,	,
antikrista	antikrist	k1gMnSc2	antikrist
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
mršinu	mršina	k1gFnSc4	mršina
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
nadávky	nadávka	k1gFnSc2	nadávka
odpovídal	odpovídat	k5eAaImAgInS	odpovídat
výrazu	výraz	k1gInSc2	výraz
mršina	mršin	k2eAgFnSc1d1	mršina
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
také	také	k9	také
používané	používaný	k2eAgInPc1d1	používaný
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
"	"	kIx"	"
<g/>
mrcha	mrcha	k1gFnSc1	mrcha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
nadávky	nadávka	k1gFnPc1	nadávka
do	do	k7c2	do
"	"	kIx"	"
<g/>
šelem	šelma	k1gFnPc2	šelma
<g/>
"	"	kIx"	"
staly	stát	k5eAaPmAgFnP	stát
předmětem	předmět	k1gInSc7	předmět
žalob	žaloba	k1gFnPc2	žaloba
<g/>
.	.	kIx.	.
</s>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
Winter	Winter	k1gMnSc1	Winter
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
o	o	k7c6	o
konšelích	konšel	k1gMnPc6	konšel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
podle	podle	k7c2	podle
záznamu	záznam	k1gInSc2	záznam
nebyli	být	k5eNaImAgMnP	být
jisti	jist	k2eAgMnPc1d1	jist
významem	význam	k1gInSc7	význam
nadávky	nadávka	k1gFnPc4	nadávka
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
kutnohorští	kutnohorský	k2eAgMnPc1d1	kutnohorský
konšelé	konšel	k1gMnPc1	konšel
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
znamená	znamenat	k5eAaImIp3nS	znamenat
mršinu	mršina	k1gFnSc4	mršina
<g/>
,	,	kIx,	,
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
znamená	znamenat	k5eAaImIp3nS	znamenat
bezectného	bezectný	k2eAgMnSc4d1	bezectný
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
raného	raný	k2eAgInSc2d1	raný
novověku	novověk	k1gInSc2	novověk
bylo	být	k5eAaImAgNnS	být
zdaleka	zdaleka	k6eAd1	zdaleka
nejhojnější	hojný	k2eAgFnSc7d3	nejhojnější
nadávkou	nadávka	k1gFnSc7	nadávka
(	(	kIx(	(
<g/>
alespoň	alespoň	k9	alespoň
podle	podle	k7c2	podle
popisů	popis	k1gInPc2	popis
v	v	k7c6	v
žatecké	žatecký	k2eAgFnSc6d1	Žatecká
knize	kniha	k1gFnSc6	kniha
svědomí	svědomí	k1gNnSc2	svědomí
<g/>
)	)	kIx)	)
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
kurva	kurva	k1gFnSc1	kurva
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
význam	význam	k1gInSc1	význam
se	se	k3xPyFc4	se
od	od	k7c2	od
šestnáctého	šestnáctý	k4xOgNnSc2	šestnáctý
století	století	k1gNnSc2	století
nijak	nijak	k6eAd1	nijak
zásadně	zásadně	k6eAd1	zásadně
nezměnil	změnit	k5eNaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
používána	používat	k5eAaImNgFnS	používat
i	i	k8xC	i
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
v	v	k7c6	v
nověšku	nověšek	k1gInSc6	nověšek
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
zcela	zcela	k6eAd1	zcela
výlučně	výlučně	k6eAd1	výlučně
ženskou	ženský	k2eAgFnSc4d1	ženská
nadávku	nadávka	k1gFnSc4	nadávka
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
zaznamenány	zaznamenán	k2eAgInPc1d1	zaznamenán
tvary	tvar	k1gInPc1	tvar
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
zkurvy	zkurv	k1gInPc1	zkurv
syne	syn	k1gFnSc2	syn
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
vaše	váš	k3xOp2gFnSc1	váš
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
kurva	kurva	k1gFnSc1	kurva
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
kultivovanější	kultivovaný	k2eAgFnSc1d2	kultivovanější
<g/>
"	"	kIx"	"
forma	forma	k1gFnSc1	forma
výrazu	výraz	k1gInSc2	výraz
"	"	kIx"	"
<g/>
tvá	tvůj	k3xOp2gFnSc1	tvůj
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
nechovala	chovat	k5eNaImAgFnS	chovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jest	být	k5eAaImIp3nS	být
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
chovati	chovat	k5eAaImF	chovat
pořádně	pořádně	k6eAd1	pořádně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vyloženě	vyloženě	k6eAd1	vyloženě
mužskou	mužský	k2eAgFnSc7d1	mužská
nadávkou	nadávka	k1gFnSc7	nadávka
raného	raný	k2eAgInSc2d1	raný
novověku	novověk	k1gInSc2	novověk
bylo	být	k5eAaImAgNnS	být
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
zrádce	zrádce	k1gMnSc1	zrádce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
žena	žena	k1gFnSc1	žena
"	"	kIx"	"
<g/>
jest	být	k5eAaImIp3nS	být
mysli	mysl	k1gFnSc2	mysl
vrtkavé	vrtkavý	k2eAgNnSc1d1	vrtkavé
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tvor	tvor	k1gMnSc1	tvor
od	od	k7c2	od
přirozenosti	přirozenost	k1gFnSc2	přirozenost
nestálý	stálý	k2eNgInSc4d1	nestálý
<g/>
,	,	kIx,	,
od	od	k7c2	od
něhož	jenž	k3xRgMnSc2	jenž
nelze	lze	k6eNd1	lze
očekávat	očekávat	k5eAaImF	očekávat
věrnost	věrnost	k1gFnSc1	věrnost
<g/>
,	,	kIx,	,
ctnost	ctnost	k1gFnSc1	ctnost
jaké	jaký	k3yQgNnSc4	jaký
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bylo	být	k5eAaImAgNnS	být
cílem	cíl	k1gInSc7	cíl
nadávky	nadávka	k1gFnSc2	nadávka
ponížit	ponížit	k5eAaPmF	ponížit
adresáta	adresát	k1gMnSc4	adresát
<g/>
,	,	kIx,	,
označit	označit	k5eAaPmF	označit
jej	on	k3xPp3gMnSc4	on
za	za	k7c4	za
člověka	člověk	k1gMnSc4	člověk
příliš	příliš	k6eAd1	příliš
podřadného	podřadný	k2eAgMnSc4d1	podřadný
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
interpret	interpret	k1gMnSc1	interpret
přel	přít	k5eAaImAgMnS	přít
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
nejsi	být	k5eNaImIp2nS	být
hodná	hodný	k2eAgFnSc1d1	hodná
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
tebou	ty	k3xPp2nSc7	ty
vadila	vadit	k5eAaImAgFnS	vadit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
Však	však	k9	však
vědí	vědět	k5eAaImIp3nP	vědět
o	o	k7c6	o
tobě	ty	k3xPp2nSc6	ty
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsi	být	k5eAaImIp2nS	být
jak	jak	k6eAd1	jak
živa	živ	k2eAgFnSc1d1	živa
nic	nic	k6eAd1	nic
dobrýho	dobrýho	k?	dobrýho
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
oceňován	oceňován	k2eAgInSc1d1	oceňován
tvůrčí	tvůrčí	k2eAgInSc1d1	tvůrčí
způsob	způsob	k1gInSc1	způsob
používání	používání	k1gNnSc2	používání
a	a	k8xC	a
kombinování	kombinování	k1gNnSc2	kombinování
nadávek	nadávka	k1gFnPc2	nadávka
dobové	dobový	k2eAgFnSc2d1	dobová
listiny	listina	k1gFnSc2	listina
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
výlev	výlev	k1gInSc4	výlev
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vzyjebená	Vzyjebený	k2eAgFnSc5d1	Vzyjebený
kurvo	kurva	k1gFnSc5	kurva
<g/>
,	,	kIx,	,
šejdířko	šejdířka	k1gFnSc5	šejdířka
<g/>
,	,	kIx,	,
ludařko	ludařka	k1gFnSc5	ludařka
<g/>
,	,	kIx,	,
chásko	cháska	k1gFnSc5	cháska
zvyjebená	zvyjebený	k2eAgNnPc1d1	zvyjebený
zlodějská	zlodějský	k2eAgNnPc1d1	zlodějské
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Časté	častý	k2eAgFnPc1d1	častá
jsou	být	k5eAaImIp3nP	být
proměny	proměna	k1gFnPc1	proměna
nadávek	nadávka	k1gFnPc2	nadávka
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
nadávkou	nadávka	k1gFnSc7	nadávka
pro	pro	k7c4	pro
oponenty	oponent	k1gMnPc4	oponent
režimu	režim	k1gInSc2	režim
(	(	kIx(	(
<g/>
disidenti	disident	k1gMnPc1	disident
<g/>
)	)	kIx)	)
mimo	mimo	k6eAd1	mimo
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
vlastizrádci	vlastizrádce	k1gMnPc1	vlastizrádce
<g/>
"	"	kIx"	"
také	také	k9	také
používán	používán	k2eAgInSc1d1	používán
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
samozvanci	samozvanec	k1gMnPc1	samozvanec
<g/>
"	"	kIx"	"
pocházející	pocházející	k2eAgMnSc1d1	pocházející
ze	z	k7c2	z
sousloví	sousloví	k1gNnSc2	sousloví
"	"	kIx"	"
<g/>
samozvanci	samozvanec	k1gMnPc1	samozvanec
a	a	k8xC	a
zaprodanci	zaprodanec	k1gMnPc1	zaprodanec
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
sabotéři	sabotér	k1gMnPc1	sabotér
komunismu	komunismus	k1gInSc2	komunismus
podle	podle	k7c2	podle
tvrzení	tvrzení	k1gNnSc2	tvrzení
komunistů	komunista	k1gMnPc2	komunista
placení	placení	k1gNnSc2	placení
kapitalistickým	kapitalistický	k2eAgInSc7d1	kapitalistický
Západem	západ	k1gInSc7	západ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
"	"	kIx"	"
<g/>
samozvanci	samozvanec	k1gMnPc1	samozvanec
a	a	k8xC	a
ztroskotanci	ztroskotanec	k1gMnPc1	ztroskotanec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
bylo	být	k5eAaImAgNnS	být
pomocí	pomocí	k7c2	pomocí
propagandy	propaganda	k1gFnSc2	propaganda
šířeno	šířen	k2eAgNnSc4d1	šířeno
a	a	k8xC	a
ujímalo	ujímat	k5eAaImAgNnS	ujímat
se	se	k3xPyFc4	se
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
chartista	chartista	k1gMnSc1	chartista
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
hanlivé	hanlivý	k2eAgFnPc1d1	hanlivá
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
běžné	běžný	k2eAgNnSc4d1	běžné
označení	označení	k1gNnSc4	označení
změněno	změněn	k2eAgNnSc4d1	změněno
v	v	k7c4	v
urážku	urážka	k1gFnSc4	urážka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zdvořilá	zdvořilý	k2eAgFnSc1d1	zdvořilá
komunikace	komunikace	k1gFnSc1	komunikace
týkající	týkající	k2eAgFnSc1d1	týkající
se	se	k3xPyFc4	se
subjektu	subjekt	k1gInSc2	subjekt
obtížnější	obtížný	k2eAgFnSc1d2	obtížnější
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
hodnota	hodnota	k1gFnSc1	hodnota
tak	tak	k9	tak
zcela	zcela	k6eAd1	zcela
degradována	degradován	k2eAgFnSc1d1	degradována
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
stvoření	stvoření	k1gNnSc2	stvoření
nadávky	nadávka	k1gFnSc2	nadávka
změnou	změna	k1gFnSc7	změna
konotace	konotace	k1gFnSc2	konotace
používaného	používaný	k2eAgInSc2d1	používaný
výrazu	výraz	k1gInSc2	výraz
je	být	k5eAaImIp3nS	být
běžný	běžný	k2eAgInSc1d1	běžný
i	i	k9	i
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Opačně	opačně	k6eAd1	opačně
pro	pro	k7c4	pro
příslušníky	příslušník	k1gMnPc4	příslušník
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
používána	používán	k2eAgFnSc1d1	používána
přezdívka	přezdívka	k1gFnSc1	přezdívka
"	"	kIx"	"
<g/>
komančové	komančové	k2eAgFnSc1d1	komančové
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
komáři	komár	k1gMnPc1	komár
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
osoby	osoba	k1gFnPc4	osoba
"	"	kIx"	"
<g/>
kolaboranti	kolaborant	k1gMnPc1	kolaborant
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
estébáci	estébáci	k?	estébáci
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
pro	pro	k7c4	pro
exponenty	exponent	k1gInPc4	exponent
režimu	režim	k1gInSc2	režim
"	"	kIx"	"
<g/>
staré	starý	k2eAgFnPc1d1	stará
struktury	struktura	k1gFnPc1	struktura
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
příznivce	příznivec	k1gMnSc4	příznivec
Václava	Václav	k1gMnSc4	Václav
Havla	Havel	k1gMnSc4	Havel
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
liberální	liberální	k2eAgMnPc4d1	liberální
odpůrce	odpůrce	k1gMnPc4	odpůrce
používají	používat	k5eAaImIp3nP	používat
konzervativní	konzervativní	k2eAgMnPc1d1	konzervativní
<g/>
,	,	kIx,	,
pravicoví	pravicový	k2eAgMnPc1d1	pravicový
a	a	k8xC	a
levicoví	levicový	k2eAgMnPc1d1	levicový
politici	politik	k1gMnPc1	politik
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
příznivci	příznivec	k1gMnPc1	příznivec
jako	jako	k8xC	jako
hanlivé	hanlivý	k2eAgNnSc1d1	hanlivé
označení	označení	k1gNnSc1	označení
havlisti	havlist	k1gMnPc1	havlist
anebo	anebo	k8xC	anebo
novotvar	novotvar	k1gInSc4	novotvar
pravdoláskaři	pravdoláskař	k1gMnPc1	pravdoláskař
z	z	k7c2	z
propagovaného	propagovaný	k2eAgInSc2d1	propagovaný
"	"	kIx"	"
<g/>
Pravda	pravda	k1gFnSc1	pravda
a	a	k8xC	a
láska	láska	k1gFnSc1	láska
musí	muset	k5eAaImIp3nS	muset
zvítězit	zvítězit	k5eAaPmF	zvítězit
nad	nad	k7c7	nad
lží	lež	k1gFnSc7	lež
a	a	k8xC	a
nenávistí	nenávist	k1gFnSc7	nenávist
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nadávka	nadávka	k1gFnSc1	nadávka
"	"	kIx"	"
<g/>
Moskaljaku	Moskaljak	k1gInSc2	Moskaljak
na	na	k7c6	na
hilljaku	hilljak	k1gInSc6	hilljak
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
označují	označovat	k5eAaImIp3nP	označovat
Rusy	Rus	k1gMnPc4	Rus
podporující	podporující	k2eAgInSc4d1	podporující
politický	politický	k2eAgInSc4d1	politický
režim	režim	k1gInSc4	režim
prezidenta	prezident	k1gMnSc2	prezident
Vladimira	Vladimir	k1gInSc2	Vladimir
Putina	putin	k2eAgFnSc1d1	Putina
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
populární	populární	k2eAgFnSc2d1	populární
zejména	zejména	k9	zejména
v	v	k7c6	v
období	období	k1gNnSc6	období
krymské	krymský	k2eAgFnSc2d1	Krymská
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dějin	dějiny	k1gFnPc2	dějiny
lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
mučeni	mučit	k5eAaImNgMnP	mučit
a	a	k8xC	a
zabíjeni	zabíjet	k5eAaImNgMnP	zabíjet
za	za	k7c4	za
kritiku	kritika	k1gFnSc4	kritika
svého	svůj	k3xOyFgNnSc2	svůj
vedení	vedení	k1gNnSc2	vedení
<g/>
,	,	kIx,	,
<g/>
..	..	k?	..
...	...	k?	...
<g/>
ale	ale	k8xC	ale
za	za	k7c4	za
poslední	poslední	k2eAgNnSc4d1	poslední
století	století	k1gNnSc4	století
byly	být	k5eAaImAgFnP	být
nejslavnější	slavný	k2eAgFnPc1d3	nejslavnější
právní	právní	k2eAgFnPc1d1	právní
bitvy	bitva	k1gFnPc1	bitva
o	o	k7c4	o
svobodu	svoboda	k1gFnSc4	svoboda
projevu	projev	k1gInSc2	projev
byly	být	k5eAaImAgFnP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
nikoliv	nikoliv	k9	nikoliv
s	s	k7c7	s
úsilím	úsilí	k1gNnSc7	úsilí
mluvit	mluvit	k5eAaImF	mluvit
pravdu	pravda	k1gFnSc4	pravda
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
používáním	používání	k1gNnSc7	používání
určitých	určitý	k2eAgNnPc2d1	určité
slov	slovo	k1gNnPc2	slovo
pro	pro	k7c4	pro
páření	páření	k1gNnPc4	páření
a	a	k8xC	a
zevní	zevní	k2eAgFnPc4d1	zevní
genitálie	genitálie	k1gFnPc4	genitálie
<g/>
...	...	k?	...
Omezení	omezení	k1gNnSc1	omezení
<g />
.	.	kIx.	.
</s>
<s>
svobody	svoboda	k1gFnPc1	svoboda
projevu	projev	k1gInSc2	projev
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
pocítili	pocítit	k5eAaPmAgMnP	pocítit
i	i	k9	i
Kenneth	Kenneth	k1gMnSc1	Kenneth
Tynan	Tynan	k1gMnSc1	Tynan
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
<g/>
,	,	kIx,	,
Bono	bona	k1gFnSc5	bona
<g/>
,	,	kIx,	,
Bernard	Bernard	k1gMnSc1	Bernard
Malamud	Malamud	k1gMnSc1	Malamud
<g/>
,	,	kIx,	,
Eldridge	Eldridge	k1gFnSc1	Eldridge
Cleaver	Cleaver	k1gMnSc1	Cleaver
<g/>
,	,	kIx,	,
Kurt	Kurt	k1gMnSc1	Kurt
Vonnegut	Vonnegut	k1gMnSc1	Vonnegut
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvůrci	tvůrce	k1gMnPc1	tvůrce
pořadu	pořad	k1gInSc2	pořad
Hair	Hair	k1gInSc1	Hair
(	(	kIx(	(
<g/>
Vlasy	vlas	k1gInPc1	vlas
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
M	M	kA	M
<g/>
*	*	kIx~	*
<g/>
A	A	kA	A
<g/>
*	*	kIx~	*
<g/>
S	s	k7c7	s
<g/>
*	*	kIx~	*
<g/>
H.	H.	kA	H.
Některé	některý	k3yIgInPc4	některý
výrazy	výraz	k1gInPc4	výraz
jsou	být	k5eAaImIp3nP	být
původními	původní	k2eAgNnPc7d1	původní
označeními	označení	k1gNnPc7	označení
pro	pro	k7c4	pro
tutéž	týž	k3xTgFnSc4	týž
činnost	činnost	k1gFnSc4	činnost
nebo	nebo	k8xC	nebo
objekt	objekt	k1gInSc4	objekt
jak	jak	k8xC	jak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
významu	význam	k1gInSc6	význam
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
hovno	hovno	k1gNnSc1	hovno
je	být	k5eAaImIp3nS	být
všeslovanské	všeslovanský	k2eAgNnSc1d1	všeslovanské
slovo	slovo	k1gNnSc1	slovo
téhož	týž	k3xTgInSc2	týž
významu	význam	k1gInSc2	význam
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
národech	národ	k1gInPc6	národ
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
popsány	popsán	k2eAgInPc4d1	popsán
způsoby	způsob	k1gInPc4	způsob
jak	jak	k8xS	jak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
změně	změna	k1gFnSc3	změna
z	z	k7c2	z
výrazů	výraz	k1gInPc2	výraz
běžně	běžně	k6eAd1	běžně
používaných	používaný	k2eAgInPc2d1	používaný
výrazů	výraz	k1gInPc2	výraz
na	na	k7c4	na
nadávky	nadávka	k1gFnPc4	nadávka
(	(	kIx(	(
<g/>
hovado	hovado	k1gNnSc1	hovado
<g/>
,	,	kIx,	,
obcování	obcování	k1gNnSc1	obcování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nadávky	nadávka	k1gFnPc1	nadávka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pochází	pocházet	k5eAaImIp3nP	pocházet
od	od	k7c2	od
názvů	název	k1gInPc2	název
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Českého	český	k2eAgInSc2d1	český
etymologického	etymologický	k2eAgInSc2d1	etymologický
slovníku	slovník	k1gInSc2	slovník
Jiřího	Jiří	k1gMnSc2	Jiří
Rejzka	Rejzka	k1gFnSc1	Rejzka
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
přenesením	přenesení	k1gNnSc7	přenesení
významu	význam	k1gInSc2	význam
(	(	kIx(	(
<g/>
metaforou	metafora	k1gFnSc7	metafora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
společných	společný	k2eAgFnPc2d1	společná
vlastností	vlastnost	k1gFnPc2	vlastnost
s	s	k7c7	s
daným	daný	k2eAgNnSc7d1	dané
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
,	,	kIx,	,
předpokládané	předpokládaný	k2eAgFnPc4d1	předpokládaná
hlouposti	hloupost	k1gFnPc4	hloupost
<g/>
,	,	kIx,	,
nečistotnosti	nečistotnost	k1gFnPc4	nečistotnost
či	či	k8xC	či
neohrabanosti	neohrabanost	k1gFnPc4	neohrabanost
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
nadávky	nadávka	k1gFnPc1	nadávka
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
zkomolením	zkomolení	k1gNnSc7	zkomolení
nebo	nebo	k8xC	nebo
přenesením	přenesení	k1gNnSc7	přenesení
cizojazyčných	cizojazyčný	k2eAgInPc2d1	cizojazyčný
výrazů	výraz	k1gInPc2	výraz
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
slovo	slovo	k1gNnSc1	slovo
kunda	kundo	k1gNnSc2	kundo
pochází	pocházet	k5eAaImIp3nS	pocházet
zřejmě	zřejmě	k6eAd1	zřejmě
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
"	"	kIx"	"
<g/>
cunnus	cunnus	k1gInSc1	cunnus
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vulva	vulva	k1gFnSc1	vulva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Valaši	Valach	k1gMnPc1	Valach
však	však	k9	však
jménem	jméno	k1gNnSc7	jméno
Kunda	Kundo	k1gNnSc2	Kundo
křtili	křtít	k5eAaImAgMnP	křtít
své	svůj	k3xOyFgFnPc4	svůj
dcery	dcera	k1gFnPc4	dcera
<g/>
,	,	kIx,	,
nazývali	nazývat	k5eAaImAgMnP	nazývat
tak	tak	k6eAd1	tak
původně	původně	k6eAd1	původně
mladá	mladý	k2eAgNnPc4d1	mladé
děvčata	děvče	k1gNnPc4	děvče
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nadávky	nadávka	k1gFnSc2	nadávka
píča	píč	k1gInSc2	píč
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
původ	původ	k1gInSc1	původ
ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
slovním	slovní	k2eAgNnSc6d1	slovní
spojení	spojení	k1gNnSc6	spojení
"	"	kIx"	"
<g/>
le	le	k?	le
petit	petit	k1gInSc1	petit
chat	chata	k1gFnPc2	chata
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
malá	malý	k2eAgFnSc1d1	malá
kočka	kočka	k1gFnSc1	kočka
<g/>
,	,	kIx,	,
kočička	kočička	k1gFnSc1	kočička
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
píča	píča	k6eAd1	píča
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
také	také	k9	také
název	název	k1gInSc4	název
pro	pro	k7c4	pro
sýkorku	sýkorka	k1gFnSc4	sýkorka
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
děvka	děvka	k1gFnSc1	děvka
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
běžným	běžný	k2eAgNnSc7d1	běžné
označením	označení	k1gNnSc7	označení
pro	pro	k7c4	pro
dívku	dívka	k1gFnSc4	dívka
<g/>
,	,	kIx,	,
děvče	děvče	k1gNnSc4	děvče
<g/>
,	,	kIx,	,
děvečku	děvečka	k1gFnSc4	děvečka
nebo	nebo	k8xC	nebo
služebnou	služebná	k1gFnSc4	služebná
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tvrzení	tvrzení	k1gNnSc2	tvrzení
některých	některý	k3yIgFnPc2	některý
osob	osoba	k1gFnPc2	osoba
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
artikulované	artikulovaný	k2eAgInPc4d1	artikulovaný
zvuky	zvuk	k1gInPc4	zvuk
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
samy	sám	k3xTgFnPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
špatné	špatný	k2eAgInPc4d1	špatný
nebo	nebo	k8xC	nebo
nadávkami	nadávka	k1gFnPc7	nadávka
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
použití	použití	k1gNnSc1	použití
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
především	především	k9	především
špatný	špatný	k2eAgInSc4d1	špatný
úmysl	úmysl	k1gInSc4	úmysl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
tedy	tedy	k9	tedy
v	v	k7c6	v
USA	USA	kA	USA
poměr	poměr	k1gInSc4	poměr
médiím	médium	k1gNnPc3	médium
povolených	povolený	k2eAgNnPc2d1	povolené
vůči	vůči	k7c3	vůči
nepovoleným	povolený	k2eNgFnPc3d1	nepovolená
výrazů	výraz	k1gInPc2	výraz
asi	asi	k9	asi
399,993	[number]	k4	399,993
ku	k	k7c3	k
sedmi	sedm	k4xCc6	sedm
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
podle	podle	k7c2	podle
úvahy	úvaha	k1gFnSc2	úvaha
těchto	tento	k3xDgFnPc2	tento
osob	osoba	k1gFnPc2	osoba
"	"	kIx"	"
<g/>
musí	muset	k5eAaImIp3nP	muset
jít	jít	k5eAaImF	jít
o	o	k7c4	o
opravdu	opravdu	k6eAd1	opravdu
zlá	zlý	k2eAgNnPc4d1	zlé
slova	slovo	k1gNnPc4	slovo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
výrazy	výraz	k1gInPc4	výraz
"	"	kIx"	"
<g/>
Shit	Shit	k1gInSc1	Shit
<g/>
,	,	kIx,	,
Piss	Piss	k1gInSc1	Piss
<g/>
,	,	kIx,	,
Fuck	Fuck	k1gMnSc1	Fuck
<g/>
,	,	kIx,	,
Cunt	Cunt	k1gMnSc1	Cunt
<g/>
,	,	kIx,	,
Cocksucker	Cocksucker	k1gMnSc1	Cocksucker
<g/>
,	,	kIx,	,
Motherfucker	Motherfucker	k1gMnSc1	Motherfucker
and	and	k?	and
Tits	Tits	k1gInSc1	Tits
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslovně	doslovně	k6eAd1	doslovně
<g/>
:	:	kIx,	:
hovno	hovno	k1gNnSc4	hovno
<g/>
,	,	kIx,	,
chcanky	chcanky	k1gFnPc4	chcanky
<g/>
,	,	kIx,	,
jebat	jebat	k5eAaImF	jebat
<g/>
,	,	kIx,	,
piča	piča	k1gFnSc1	piča
<g/>
,	,	kIx,	,
hulibrk	hulibrk	k1gInSc1	hulibrk
<g/>
,	,	kIx,	,
osoba	osoba	k1gFnSc1	osoba
jenž	jenž	k3xRgMnSc1	jenž
souloží	souložit	k5eAaImIp3nS	souložit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
prsa	prsa	k1gNnPc1	prsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jiné	jiný	k2eAgNnSc1d1	jiné
slova	slovo	k1gNnPc1	slovo
mohou	moct	k5eAaImIp3nP	moct
nadávky	nadávka	k1gFnPc1	nadávka
vznikat	vznikat	k5eAaImF	vznikat
pomocí	pomocí	k7c2	pomocí
onomatopoie	onomatopoie	k1gFnSc2	onomatopoie
<g/>
,	,	kIx,	,
častěji	často	k6eAd2	často
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořen	k2eAgFnPc1d1	tvořena
změnou	změna	k1gFnSc7	změna
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
přejímání	přejímání	k1gNnSc2	přejímání
nebo	nebo	k8xC	nebo
odvozováním	odvozování	k1gNnSc7	odvozování
a	a	k8xC	a
skládáním	skládání	k1gNnSc7	skládání
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
víceslovné	víceslovný	k2eAgFnPc4d1	víceslovná
nadávky	nadávka	k1gFnPc4	nadávka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
namrdaný	namrdaný	k2eAgInSc1d1	namrdaný
medvídku	medvídek	k1gMnSc6	medvídek
<g/>
,	,	kIx,	,
vyjetá	vyjetý	k2eAgFnSc1d1	vyjetá
macocho	macocho	k?	macocho
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
nadávky	nadávka	k1gFnPc1	nadávka
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
vznikají	vznikat	k5eAaImIp3nP	vznikat
vznikají	vznikat	k5eAaImIp3nP	vznikat
jako	jako	k8xS	jako
produkty	produkt	k1gInPc1	produkt
sémiotiky	sémiotika	k1gFnSc2	sémiotika
a	a	k8xC	a
sémantických	sémantický	k2eAgInPc2d1	sémantický
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
nadávek	nadávka	k1gFnPc2	nadávka
mohou	moct	k5eAaImIp3nP	moct
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
sehrávat	sehrávat	k5eAaImF	sehrávat
básnické	básnický	k2eAgInPc4d1	básnický
prostředky	prostředek	k1gInPc4	prostředek
-	-	kIx~	-
perzifláž	perzifláž	k1gFnSc1	perzifláž
<g/>
,	,	kIx,	,
přirovnání	přirovnání	k1gNnSc1	přirovnání
<g/>
,	,	kIx,	,
jinotaj	jinotaj	k1gInSc1	jinotaj
<g/>
,	,	kIx,	,
ironie	ironie	k1gFnSc1	ironie
<g/>
,	,	kIx,	,
hyperbola	hyperbola	k1gFnSc1	hyperbola
<g/>
,	,	kIx,	,
personifikace	personifikace	k1gFnSc1	personifikace
<g/>
,	,	kIx,	,
oxymóron	oxymóron	k1gNnSc1	oxymóron
<g/>
,	,	kIx,	,
významový	významový	k2eAgInSc1d1	významový
přenos	přenos	k1gInSc1	přenos
(	(	kIx(	(
<g/>
metafora	metafora	k1gFnSc1	metafora
a	a	k8xC	a
metonymie	metonymie	k1gFnSc1	metonymie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
významové	významový	k2eAgInPc1d1	významový
posuny	posun	k1gInPc1	posun
(	(	kIx(	(
<g/>
generalizace	generalizace	k1gFnSc1	generalizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nadávky	nadávka	k1gFnPc1	nadávka
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
váží	vážit	k5eAaImIp3nP	vážit
k	k	k7c3	k
nějakému	nějaký	k3yIgInSc3	nějaký
objektu	objekt	k1gInSc3	objekt
nebo	nebo	k8xC	nebo
procesu	proces	k1gInSc3	proces
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
pojímány	pojímat	k5eAaImNgFnP	pojímat
spíše	spíše	k9	spíše
jako	jako	k8xS	jako
znaky	znak	k1gInPc7	znak
<g/>
,	,	kIx,	,
symboly	symbol	k1gInPc7	symbol
popisovaného	popisovaný	k2eAgInSc2d1	popisovaný
objektu	objekt	k1gInSc2	objekt
(	(	kIx(	(
<g/>
piča	piča	k1gMnSc1	piča
<g/>
,	,	kIx,	,
estébák	estébák	k?	estébák
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
procesu	proces	k1gInSc2	proces
(	(	kIx(	(
<g/>
zpičit	zpičit	k5eAaImF	zpičit
<g/>
,	,	kIx,	,
kurvení	kurvení	k1gNnSc1	kurvení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
než	než	k8xS	než
popisy	popis	k1gInPc1	popis
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
pomocí	pomocí	k7c2	pomocí
metafory	metafora	k1gFnSc2	metafora
je	být	k5eAaImIp3nS	být
popisován	popisován	k2eAgMnSc1d1	popisován
jako	jako	k8xC	jako
velmi	velmi	k6eAd1	velmi
častý	častý	k2eAgInSc1d1	častý
<g/>
.	.	kIx.	.
</s>
<s>
Metafora	metafora	k1gFnSc1	metafora
dává	dávat	k5eAaImIp3nS	dávat
výrazům	výraz	k1gInPc3	výraz
jiný	jiný	k1gMnSc1	jiný
význam	význam	k1gInSc4	význam
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vnější	vnější	k2eAgFnSc2d1	vnější
podobnosti	podobnost	k1gFnSc2	podobnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
popisována	popisovat	k5eAaImNgFnS	popisovat
například	například	k6eAd1	například
u	u	k7c2	u
slov	slovo	k1gNnPc2	slovo
označujících	označující	k2eAgNnPc2d1	označující
stárnoucího	stárnoucí	k2eAgMnSc2d1	stárnoucí
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
zombie	zombie	k1gFnSc1	zombie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ošklivého	ošklivý	k2eAgMnSc4d1	ošklivý
člověka	člověk	k1gMnSc4	člověk
(	(	kIx(	(
<g/>
zrůda	zrůda	k1gFnSc1	zrůda
<g/>
)	)	kIx)	)
a	a	k8xC	a
rasistických	rasistický	k2eAgFnPc2d1	rasistická
nadávek	nadávka	k1gFnPc2	nadávka
(	(	kIx(	(
<g/>
kofola	kofola	k1gFnSc1	kofola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skládáním	skládání	k1gNnSc7	skládání
slov	slovo	k1gNnPc2	slovo
vznikají	vznikat	k5eAaImIp3nP	vznikat
nadávky	nadávka	k1gFnPc1	nadávka
jako	jako	k8xS	jako
budižkničemu	budižkničemu	k1gMnSc1	budižkničemu
<g/>
,	,	kIx,	,
černoprdelník	černoprdelník	k1gMnSc1	černoprdelník
<g/>
,	,	kIx,	,
čobolák	čobolák	k1gMnSc1	čobolák
nebo	nebo	k8xC	nebo
zlatokopka	zlatokopka	k1gFnSc1	zlatokopka
<g/>
.	.	kIx.	.
</s>
<s>
Skládání	skládání	k1gNnSc1	skládání
slov	slovo	k1gNnPc2	slovo
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
typické	typický	k2eAgNnSc1d1	typické
spíše	spíše	k9	spíše
pro	pro	k7c4	pro
němčinu	němčina	k1gFnSc4	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
nadávky	nadávka	k1gFnPc1	nadávka
mohou	moct	k5eAaImIp3nP	moct
tvořit	tvořit	k5eAaImF	tvořit
i	i	k9	i
souvislé	souvislý	k2eAgFnPc1d1	souvislá
věty	věta	k1gFnPc1	věta
(	(	kIx(	(
<g/>
smrdí	smrdět	k5eAaImIp3nS	smrdět
ti	ty	k3xPp2nSc3	ty
z	z	k7c2	z
huby	huba	k1gFnSc2	huba
jak	jak	k8xS	jak
žebrákovi	žebrák	k1gMnSc3	žebrák
z	z	k7c2	z
prdele	prdel	k1gFnSc2	prdel
<g/>
)	)	kIx)	)
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
.	.	kIx.	.
</s>
<s>
Nikoliv	nikoliv	k9	nikoliv
zanedbatelným	zanedbatelný	k2eAgMnSc7d1	zanedbatelný
druhem	druh	k1gMnSc7	druh
tvorby	tvorba	k1gFnSc2	tvorba
nadávek	nadávka	k1gFnPc2	nadávka
je	být	k5eAaImIp3nS	být
pozměňování	pozměňování	k1gNnSc2	pozměňování
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
změny	změna	k1gFnSc2	změna
liter	litera	k1gFnPc2	litera
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
jiného	jiný	k2eAgInSc2d1	jiný
předpokládaného	předpokládaný	k2eAgInSc2d1	předpokládaný
vztahu	vztah	k1gInSc2	vztah
(	(	kIx(	(
<g/>
konotace	konotace	k1gFnSc1	konotace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
cílem	cíl	k1gInSc7	cíl
pobavit	pobavit	k5eAaPmF	pobavit
adresáta	adresát	k1gMnSc4	adresát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
jsou	být	k5eAaImIp3nP	být
řazeny	řazen	k2eAgFnPc4d1	řazena
nadávky	nadávka	k1gFnPc4	nadávka
partadement	partadement	k1gInSc1	partadement
=	=	kIx~	=
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
poštěvačka	poštěvačka	k1gFnSc1	poštěvačka
=	=	kIx~	=
pošt	pošta	k1gFnPc2	pošta
<g/>
'	'	kIx"	'
<g/>
ačka	ačka	k1gFnSc1	ačka
<g/>
,	,	kIx,	,
serebrita	serebrita	k1gFnSc1	serebrita
=	=	kIx~	=
celebrita	celebrita	k1gFnSc1	celebrita
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
názvu	název	k1gInSc2	název
slova	slovo	k1gNnSc2	slovo
je	být	k5eAaImIp3nS	být
popsaná	popsaný	k2eAgFnSc1d1	popsaná
často	často	k6eAd1	často
u	u	k7c2	u
nadávek	nadávka	k1gFnPc2	nadávka
používaných	používaný	k2eAgFnPc2d1	používaná
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
,	,	kIx,	,
originální	originální	k2eAgFnPc1d1	originální
nadávky	nadávka	k1gFnPc4	nadávka
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
pozměňovány	pozměňovat	k5eAaImNgInP	pozměňovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gNnSc4	on
vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
neodstranil	odstranit	k5eNaPmAgMnS	odstranit
a	a	k8xC	a
adresát	adresát	k1gMnSc1	adresát
pochopil	pochopit	k5eAaPmAgMnS	pochopit
(	(	kIx(	(
<g/>
perdel	perdlo	k1gNnPc2	perdlo
=	=	kIx~	=
prdel	prdel	k1gFnSc1	prdel
<g/>
,	,	kIx,	,
kua	kua	k?	kua
=	=	kIx~	=
kurva	kurva	k1gFnSc1	kurva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nadávka	nadávka	k1gFnSc1	nadávka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
společnosti	společnost	k1gFnSc6	společnost
hojně	hojně	k6eAd1	hojně
využívána	využívat	k5eAaPmNgFnS	využívat
v	v	k7c6	v
mnoha	mnoho	k4c2	mnoho
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Nadávka	nadávka	k1gFnSc1	nadávka
není	být	k5eNaImIp3nS	být
pouze	pouze	k6eAd1	pouze
útočná	útočný	k2eAgFnSc1d1	útočná
či	či	k8xC	či
obranná	obranný	k2eAgFnSc1d1	obranná
forma	forma	k1gFnSc1	forma
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyjádření	vyjádření	k1gNnSc6	vyjádření
veřejně	veřejně	k6eAd1	veřejně
činných	činný	k2eAgFnPc2d1	činná
osobností	osobnost	k1gFnPc2	osobnost
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
nadávky	nadávka	k1gFnPc1	nadávka
objevují	objevovat	k5eAaImIp3nP	objevovat
běžně	běžně	k6eAd1	běžně
také	také	k9	také
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
měrou	míra	k1gFnSc7wR	míra
může	moct	k5eAaImIp3nS	moct
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
výběr	výběr	k1gInSc4	výběr
nadávky	nadávka	k1gFnSc2	nadávka
kreativita	kreativita	k1gFnSc1	kreativita
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
části	část	k1gFnSc2	část
inteligence	inteligence	k1gFnSc2	inteligence
interpreta	interpret	k1gMnSc2	interpret
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nadávka	nadávka	k1gFnSc1	nadávka
někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
více	hodně	k6eAd2	hodně
než	než	k8xS	než
o	o	k7c6	o
adresátovi	adresát	k1gMnSc6	adresát
vypovídat	vypovídat	k5eAaPmF	vypovídat
o	o	k7c6	o
kvalitách	kvalita	k1gFnPc6	kvalita
interpreta	interpret	k1gMnSc2	interpret
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
překladu	překlad	k1gInSc6	překlad
nadávek	nadávka	k1gFnPc2	nadávka
bývá	bývat	k5eAaImIp3nS	bývat
problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
zachovat	zachovat	k5eAaPmF	zachovat
při	při	k7c6	při
interpretaci	interpretace	k1gFnSc6	interpretace
spíše	spíše	k9	spíše
formu	forma	k1gFnSc4	forma
nebo	nebo	k8xC	nebo
obsah	obsah	k1gInSc4	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
otázka	otázka	k1gFnSc1	otázka
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
morálního	morální	k2eAgInSc2d1	morální
aspektu	aspekt	k1gInSc2	aspekt
překladu	překlad	k1gInSc2	překlad
nadávky	nadávka	k1gFnSc2	nadávka
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
předpokládáno	předpokládán	k2eAgNnSc1d1	předpokládáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
výběr	výběr	k1gInSc1	výběr
textotvorných	textotvorný	k2eAgInPc2d1	textotvorný
prostředků	prostředek	k1gInPc2	prostředek
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgInS	mít
být	být	k5eAaImF	být
narušen	narušit	k5eAaPmNgInS	narušit
obecnými	obecný	k2eAgFnPc7d1	obecná
morálními	morální	k2eAgFnPc7d1	morální
zásadami	zásada	k1gFnPc7	zásada
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
měl	mít	k5eAaImAgInS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
především	především	k6eAd1	především
dodržen	dodržen	k2eAgInSc4d1	dodržen
záměr	záměr	k1gInSc4	záměr
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Typologie	typologie	k1gFnSc1	typologie
nadávek	nadávka	k1gFnPc2	nadávka
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
národy	národ	k1gInPc7	národ
rozdílná	rozdílný	k2eAgNnPc1d1	rozdílné
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
některých	některý	k3yIgInPc2	některý
států	stát	k1gInPc2	stát
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
kletbám	kletba	k1gFnPc3	kletba
análním	anální	k2eAgFnPc3d1	anální
a	a	k8xC	a
fekálním	fekální	k2eAgFnPc3d1	fekální
(	(	kIx(	(
<g/>
koprolalika	koprolalika	k1gFnSc1	koprolalika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obyvatelé	obyvatel	k1gMnPc1	obyvatel
jiných	jiný	k2eAgInPc2d1	jiný
států	stát	k1gInPc2	stát
preferují	preferovat	k5eAaImIp3nP	preferovat
nadávky	nadávka	k1gFnPc1	nadávka
sexuální	sexuální	k2eAgFnSc1d1	sexuální
až	až	k8xS	až
incestní	incestní	k2eAgFnSc1d1	incestní
(	(	kIx(	(
<g/>
pornolalika	pornolalika	k1gFnSc1	pornolalika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Intenzita	intenzita	k1gFnSc1	intenzita
používání	používání	k1gNnSc2	používání
nadávek	nadávka	k1gFnPc2	nadávka
v	v	k7c6	v
národní	národní	k2eAgFnSc6d1	národní
mluvě	mluva	k1gFnSc6	mluva
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
ekvivalenty	ekvivalent	k1gInPc1	ekvivalent
některých	některý	k3yIgFnPc2	některý
nadávek	nadávka	k1gFnPc2	nadávka
používaných	používaný	k2eAgFnPc2d1	používaná
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
jebem	jebem	k?	jebem
ti	ty	k3xPp2nSc3	ty
mať	matit	k5eAaImRp2nS	matit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
urážky	urážka	k1gFnPc1	urážka
Boha	bůh	k1gMnSc2	bůh
a	a	k8xC	a
svatých	svatá	k1gFnPc2	svatá
<g/>
.	.	kIx.	.
</s>
<s>
Neurolog	neurolog	k1gMnSc1	neurolog
Antonio	Antonio	k1gMnSc1	Antonio
Damasio	Damasio	k1gMnSc1	Damasio
poznamenává	poznamenávat	k5eAaImIp3nS	poznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoliv	ačkoliv	k8xS	ačkoliv
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
poranění	poranění	k1gNnSc2	poranění
mozku	mozek	k1gInSc2	mozek
pacientů	pacient	k1gMnPc2	pacient
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
jazykových	jazykový	k2eAgFnPc2d1	jazyková
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
pacienti	pacient	k1gMnPc1	pacient
stále	stále	k6eAd1	stále
schopni	schopen	k2eAgMnPc1d1	schopen
nadávat	nadávat	k5eAaImF	nadávat
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
výraz	výraz	k1gInSc1	výraz
nadávka	nadávka	k1gFnSc1	nadávka
například	například	k6eAd1	například
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
zřejmě	zřejmě	k6eAd1	zřejmě
nemá	mít	k5eNaImIp3nS	mít
obecně	obecně	k6eAd1	obecně
přímo	přímo	k6eAd1	přímo
danou	daný	k2eAgFnSc4d1	daná
obdobu	obdoba	k1gFnSc4	obdoba
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
slovníky	slovník	k1gInPc1	slovník
nadávku	nadávka	k1gFnSc4	nadávka
překládají	překládat	k5eAaImIp3nP	překládat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
abuse	abusus	k1gInSc5	abusus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
tedy	tedy	k9	tedy
významově	významově	k6eAd1	významově
-	-	kIx~	-
zneužívání	zneužívání	k1gNnSc1	zneužívání
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
jiné	jiný	k2eAgNnSc4d1	jiné
slovo	slovo	k1gNnSc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
je	být	k5eAaImIp3nS	být
však	však	k9	však
významově	významově	k6eAd1	významově
nejblíže	blízce	k6eAd3	blízce
samotnému	samotný	k2eAgInSc3d1	samotný
výrazu	výraz	k1gInSc3	výraz
"	"	kIx"	"
<g/>
nadávka	nadávka	k1gFnSc1	nadávka
<g/>
"	"	kIx"	"
zřejmě	zřejmě	k6eAd1	zřejmě
podobný	podobný	k2eAgInSc1d1	podobný
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
rudeness	rudeness	k1gInSc1	rudeness
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
hrubost	hrubost	k1gFnSc1	hrubost
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
zřejmé	zřejmý	k2eAgInPc4d1	zřejmý
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Hrubosti	hrubost	k1gFnSc3	hrubost
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
osoba	osoba	k1gFnSc1	osoba
dopouštět	dopouštět	k5eAaImF	dopouštět
i	i	k9	i
nevědomky	nevědomky	k6eAd1	nevědomky
nebo	nebo	k8xC	nebo
pouhým	pouhý	k2eAgInSc7d1	pouhý
málo	málo	k6eAd1	málo
ohleduplným	ohleduplný	k2eAgNnSc7d1	ohleduplné
chováním	chování	k1gNnSc7	chování
<g/>
.	.	kIx.	.
</s>
<s>
Obsahově	obsahově	k6eAd1	obsahově
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
blízký	blízký	k2eAgInSc1d1	blízký
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
kromě	kromě	k7c2	kromě
jiného	jiný	k2eAgInSc2d1	jiný
výrazu	výraz	k1gInSc2	výraz
-	-	kIx~	-
urážka	urážka	k1gFnSc1	urážka
"	"	kIx"	"
<g/>
insult	insult	k1gInSc1	insult
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
obdobou	obdoba	k1gFnSc7	obdoba
"	"	kIx"	"
<g/>
swearing	swearing	k1gInSc1	swearing
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
swearwords	swearwords	k6eAd1	swearwords
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
,	,	kIx,	,
"	"	kIx"	"
<g/>
curse	curse	k1gFnSc1	curse
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c2	za
synonyma	synonymum	k1gNnSc2	synonymum
výrazu	výraz	k1gInSc2	výraz
"	"	kIx"	"
<g/>
profanity	profanita	k1gFnSc2	profanita
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
bad	bad	k?	bad
words	words	k1gInSc1	words
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
tedy	tedy	k9	tedy
rouhání	rouhání	k1gNnSc1	rouhání
<g/>
,	,	kIx,	,
neuctivost	neuctivost	k1gFnSc1	neuctivost
<g/>
,	,	kIx,	,
používání	používání	k1gNnSc1	používání
sprostých	sprostý	k2eAgNnPc2d1	sprosté
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
vulgarismů	vulgarismus	k1gInPc2	vulgarismus
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
"	"	kIx"	"
<g/>
vulgarity	vulgarita	k1gFnSc2	vulgarita
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
nadávek	nadávka	k1gFnPc2	nadávka
(	(	kIx(	(
<g/>
koprolálie	koprolálie	k1gFnSc1	koprolálie
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
podle	podle	k7c2	podle
psychiatrických	psychiatrický	k2eAgInPc2d1	psychiatrický
rozborů	rozbor	k1gInPc2	rozbor
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
často	často	k6eAd1	často
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
s	s	k7c7	s
přísnou	přísný	k2eAgFnSc7d1	přísná
výchovou	výchova	k1gFnSc7	výchova
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nemají	mít	k5eNaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
ventilovat	ventilovat	k5eAaImF	ventilovat
svou	svůj	k3xOyFgFnSc4	svůj
potřebu	potřeba	k1gFnSc4	potřeba
agrese	agrese	k1gFnSc2	agrese
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
H.	H.	kA	H.
Jacksona	Jackson	k1gMnSc4	Jackson
klení	klení	k1gNnSc2	klení
a	a	k8xC	a
nadávky	nadávka	k1gFnSc2	nadávka
obecně	obecně	k6eAd1	obecně
dodávají	dodávat	k5eAaImIp3nP	dodávat
důraz	důraz	k1gInSc4	důraz
a	a	k8xC	a
sílu	síla	k1gFnSc4	síla
výrazům	výraz	k1gInPc3	výraz
emocí	emoce	k1gFnPc2	emoce
<g/>
,	,	kIx,	,
nejde	jít	k5eNaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
náplň	náplň	k1gFnSc4	náplň
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
její	její	k3xOp3gNnSc4	její
působení	působení	k1gNnSc4	působení
<g/>
,	,	kIx,	,
intenzitu	intenzita	k1gFnSc4	intenzita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc4d1	důležitý
dopad	dopad	k1gInSc4	dopad
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
obsah	obsah	k1gInSc1	obsah
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
mohou	moct	k5eAaImIp3nP	moct
odpovídat	odpovídat	k5eAaImF	odpovídat
i	i	k9	i
další	další	k2eAgInPc4d1	další
projevy	projev	k1gInPc4	projev
během	během	k7c2	během
komunikace	komunikace	k1gFnSc2	komunikace
nebo	nebo	k8xC	nebo
hlasitost	hlasitost	k1gFnSc1	hlasitost
projevu	projev	k1gInSc2	projev
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
pacientů	pacient	k1gMnPc2	pacient
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
uvedených	uvedený	k2eAgNnPc2d1	uvedené
pozorování	pozorování	k1gNnPc2	pozorování
cítí	cítit	k5eAaImIp3nS	cítit
lépe	dobře	k6eAd2	dobře
a	a	k8xC	a
pociťuje	pociťovat	k5eAaImIp3nS	pociťovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
koprolálie	koprolálie	k1gFnSc2	koprolálie
úlevu	úleva	k1gFnSc4	úleva
<g/>
,	,	kIx,	,
když	když	k8xS	když
takto	takto	k6eAd1	takto
původně	původně	k6eAd1	původně
neverbalizované	verbalizovaný	k2eNgInPc1d1	verbalizovaný
tikové	tikový	k2eAgInPc1d1	tikový
projevy	projev	k1gInPc1	projev
získají	získat	k5eAaPmIp3nP	získat
agresivní	agresivní	k2eAgInSc4d1	agresivní
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Psycholog	psycholog	k1gMnSc1	psycholog
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Vybíral	Vybíral	k1gMnSc1	Vybíral
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Každý	každý	k3xTgMnSc1	každý
nadávající	nadávající	k2eAgMnSc1d1	nadávající
člověk	člověk	k1gMnSc1	člověk
prozrazuje	prozrazovat	k5eAaImIp3nS	prozrazovat
sám	sám	k3xTgMnSc1	sám
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
momentálně	momentálně	k6eAd1	momentálně
nějaký	nějaký	k3yIgInSc4	nějaký
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
nějaký	nějaký	k3yIgInSc1	nějaký
konflikt	konflikt	k1gInSc1	konflikt
sám	sám	k3xTgInSc1	sám
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
přehled	přehled	k1gInSc1	přehled
nejčastějších	častý	k2eAgInPc2d3	nejčastější
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
jinak	jinak	k6eAd1	jinak
zdraví	zdravý	k2eAgMnPc1d1	zdravý
lidé	člověk	k1gMnPc1	člověk
uchylují	uchylovat	k5eAaImIp3nP	uchylovat
k	k	k7c3	k
nadávání	nadávání	k1gNnSc3	nadávání
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
uvolnění	uvolnění	k1gNnSc1	uvolnění
napětí	napětí	k1gNnSc2	napětí
a	a	k8xC	a
pocitu	pocit	k1gInSc2	pocit
frustrace	frustrace	k1gFnSc1	frustrace
<g/>
.	.	kIx.	.
sebepovzbuzení	sebepovzbuzení	k1gNnSc1	sebepovzbuzení
<g/>
,	,	kIx,	,
dodání	dodání	k1gNnSc1	dodání
si	se	k3xPyFc3	se
sil	síla	k1gFnPc2	síla
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
začlenění	začlenění	k1gNnSc4	začlenění
se	se	k3xPyFc4	se
úzkost	úzkost	k1gFnSc1	úzkost
a	a	k8xC	a
strach	strach	k1gInSc1	strach
snaha	snaha	k1gFnSc1	snaha
potupit	potupit	k5eAaPmF	potupit
-	-	kIx~	-
"	"	kIx"	"
<g/>
vytváření	vytváření	k1gNnSc1	vytváření
otroků	otrok	k1gMnPc2	otrok
<g/>
"	"	kIx"	"
zbavených	zbavený	k2eAgMnPc2d1	zbavený
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
sebeúcty	sebeúcta	k1gFnSc2	sebeúcta
<g/>
.	.	kIx.	.
zastrašování	zastrašování	k1gNnSc1	zastrašování
snaha	snaha	k1gFnSc1	snaha
odlišit	odlišit	k5eAaPmF	odlišit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
působit	působit	k5eAaImF	působit
"	"	kIx"	"
<g/>
drsně	drsně	k6eAd1	drsně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
snaha	snaha	k1gFnSc1	snaha
být	být	k5eAaImF	být
módní	módní	k2eAgMnPc1d1	módní
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nP	platit
zejména	zejména	k9	zejména
u	u	k7c2	u
dospívajících	dospívající	k2eAgMnPc2d1	dospívající
<g/>
.	.	kIx.	.
z	z	k7c2	z
nápodoby	nápodoba	k1gFnSc2	nápodoba
<g/>
,	,	kIx,	,
převzetím	převzetí	k1gNnSc7	převzetí
normy	norma	k1gFnSc2	norma
z	z	k7c2	z
domova	domov	k1gInSc2	domov
<g/>
,	,	kIx,	,
ve	v	k7c6	v
školce	školka	k1gFnSc6	školka
a	a	k8xC	a
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Vulgární	vulgární	k2eAgInPc4d1	vulgární
výrazy	výraz	k1gInPc4	výraz
podle	podle	k7c2	podle
psychologa	psycholog	k1gMnSc2	psycholog
Vybírala	vybírat	k5eAaImAgFnS	vybírat
u	u	k7c2	u
duševně	duševně	k6eAd1	duševně
zdravých	zdravý	k2eAgFnPc2d1	zdravá
osob	osoba	k1gFnPc2	osoba
slouží	sloužit	k5eAaImIp3nS	sloužit
právě	právě	k9	právě
především	především	k6eAd1	především
jako	jako	k8xC	jako
signál	signál	k1gInSc4	signál
konformity	konformita	k1gFnSc2	konformita
a	a	k8xC	a
loajality	loajalita	k1gFnSc2	loajalita
<g/>
,	,	kIx,	,
začlenění	začlenění	k1gNnSc1	začlenění
se	se	k3xPyFc4	se
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Nadávky	nadávka	k1gFnPc1	nadávka
se	se	k3xPyFc4	se
obsahově	obsahově	k6eAd1	obsahově
vyprazdňují	vyprazdňovat	k5eAaImIp3nP	vyprazdňovat
<g/>
,	,	kIx,	,
stávají	stávat	k5eAaImIp3nP	stávat
se	s	k7c7	s
frázemi	fráze	k1gFnPc7	fráze
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jazykovědce	jazykovědec	k1gMnSc2	jazykovědec
Uličného	Uličný	k2eAgMnSc2d1	Uličný
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
lidé	člověk	k1gMnPc1	člověk
nemají	mít	k5eNaImIp3nP	mít
žádné	žádný	k3yNgInPc4	žádný
skutečné	skutečný	k2eAgInPc4d1	skutečný
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
duševně	duševně	k6eAd1	duševně
nedospívají	dospívat	k5eNaImIp3nP	dospívat
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
infantilní	infantilní	k2eAgFnSc1d1	infantilní
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
i	i	k9	i
nadužíváním	nadužívání	k1gNnSc7	nadužívání
vulgarismů	vulgarismus	k1gInPc2	vulgarismus
<g/>
.	.	kIx.	.
</s>
<s>
Nadávky	nadávka	k1gFnPc1	nadávka
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
psychologickými	psychologický	k2eAgFnPc7d1	psychologická
funkcemi	funkce	k1gFnPc7	funkce
<g/>
,	,	kIx,	,
a	a	k8xC	a
používány	používat	k5eAaImNgFnP	používat
zejména	zejména	k9	zejména
jazykovými	jazykový	k2eAgInPc7d1	jazykový
a	a	k8xC	a
neurologickými	neurologický	k2eAgInPc7d1	neurologický
mechanismy	mechanismus	k1gInPc7	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Funkčně	funkčně	k6eAd1	funkčně
podobné	podobný	k2eAgNnSc4d1	podobné
chování	chování	k1gNnSc4	chování
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
,	,	kIx,	,
u	u	k7c2	u
šimpanzů	šimpanz	k1gMnPc2	šimpanz
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nadávání	nadávání	k1gNnSc1	nadávání
(	(	kIx(	(
<g/>
swearing	swearing	k1gInSc1	swearing
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
rozšířeno	rozšířen	k2eAgNnSc1d1	rozšířeno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
muži	muž	k1gMnPc1	muž
obecně	obecně	k6eAd1	obecně
nadávají	nadávat	k5eAaImIp3nP	nadávat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
univerzitní	univerzitní	k2eAgMnPc1d1	univerzitní
děkani	děkan	k1gMnPc1	děkan
více	hodně	k6eAd2	hodně
než	než	k8xS	než
knihovníci	knihovník	k1gMnPc1	knihovník
nebo	nebo	k8xC	nebo
někteří	některý	k3yIgMnPc1	některý
nižší	nízký	k2eAgMnPc1d2	nižší
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Badatelé	badatel	k1gMnPc1	badatel
Stephens	Stephensa	k1gFnPc2	Stephensa
<g/>
,	,	kIx,	,
Atkins	Atkinsa	k1gFnPc2	Atkinsa
a	a	k8xC	a
Kingston	Kingston	k1gInSc1	Kingston
z	z	k7c2	z
University	universita	k1gFnSc2	universita
of	of	k?	of
Keele	Keele	k1gFnSc2	Keele
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nadávání	nadávání	k1gNnSc1	nadávání
zmírňuje	zmírňovat	k5eAaImIp3nS	zmírňovat
účinky	účinek	k1gInPc4	účinek
fyzické	fyzický	k2eAgFnSc2d1	fyzická
bolesti	bolest	k1gFnSc2	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Stephens	Stephens	k6eAd1	Stephens
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chtěl	chtít	k5eAaImAgMnS	chtít
bych	by	kYmCp1nS	by
poradit	poradit	k5eAaPmF	poradit
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
zraní	zranit	k5eAaPmIp3nP	zranit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nadávali	nadávat	k5eAaImAgMnP	nadávat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
nadužívání	nadužívání	k1gNnSc1	nadužívání
nadávek	nadávka	k1gFnPc2	nadávka
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
snižovat	snižovat	k5eAaImF	snižovat
účinek	účinek	k1gInSc4	účinek
tohoto	tento	k3xDgInSc2	tento
prostředku	prostředek	k1gInSc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
neurologů	neurolog	k1gMnPc2	neurolog
a	a	k8xC	a
psychologů	psycholog	k1gMnPc2	psycholog
na	na	k7c4	na
UCLA	UCLA	kA	UCLA
Easton	Easton	k1gInSc4	Easton
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
Alzheimer	Alzheimer	k1gInSc1	Alzheimer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Disease	Diseas	k1gInSc6	Diseas
Research	Research	k1gInSc4	Research
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nadávky	nadávka	k1gFnPc1	nadávka
mohou	moct	k5eAaImIp3nP	moct
pomoci	pomoct	k5eAaPmF	pomoct
rozlišit	rozlišit	k5eAaPmF	rozlišit
Alzheimerovu	Alzheimerův	k2eAgFnSc4d1	Alzheimerova
chorobu	choroba	k1gFnSc4	choroba
od	od	k7c2	od
frontotemporální	frontotemporální	k2eAgFnSc2d1	frontotemporální
demence	demence	k1gFnSc2	demence
<g/>
.	.	kIx.	.
</s>
<s>
Vnímání	vnímání	k1gNnSc1	vnímání
nadávek	nadávka	k1gFnPc2	nadávka
je	být	k5eAaImIp3nS	být
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
sociálním	sociální	k2eAgNnSc7d1	sociální
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nadávky	nadávka	k1gFnPc1	nadávka
hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
<g/>
.	.	kIx.	.
</s>
<s>
Zdroje	zdroj	k1gInPc1	zdroj
blízké	blízký	k2eAgFnSc6d1	blízká
církvi	církev	k1gFnSc6	církev
popisují	popisovat	k5eAaImIp3nP	popisovat
vtipy	vtip	k1gInPc4	vtip
o	o	k7c6	o
blondýnkách	blondýnka	k1gFnPc6	blondýnka
nebo	nebo	k8xC	nebo
o	o	k7c6	o
homosexuálech	homosexuál	k1gMnPc6	homosexuál
jako	jako	k8xS	jako
urážlivé	urážlivý	k2eAgNnSc4d1	urážlivé
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
vulgární	vulgární	k2eAgNnSc1d1	vulgární
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
sprostá	sprostá	k1gFnSc1	sprostá
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
slavná	slavný	k2eAgFnSc1d1	slavná
korespondence	korespondence	k1gFnSc1	korespondence
V	V	kA	V
+	+	kIx~	+
W	W	kA	W
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
naopak	naopak	k6eAd1	naopak
spoustu	spousta	k1gFnSc4	spousta
sprostých	sprostý	k2eAgNnPc2d1	sprosté
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vulgární	vulgární	k2eAgNnSc1d1	vulgární
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nadávky	nadávka	k1gFnPc1	nadávka
a	a	k8xC	a
sprostá	sprostá	k1gFnSc1	sprostá
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
byla	být	k5eAaImAgFnS	být
spíš	spíš	k9	spíš
takovou	takový	k3xDgFnSc7	takový
jejich	jejich	k3xOp3gFnSc7	jejich
obranou	obrana	k1gFnSc7	obrana
proti	proti	k7c3	proti
patosu	patos	k1gInSc3	patos
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Autorka	autorka	k1gFnSc1	autorka
dodává	dodávat	k5eAaImIp3nS	dodávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nevhodným	vhodný	k2eNgNnSc7d1	nevhodné
oblečením	oblečení	k1gNnSc7	oblečení
dává	dávat	k5eAaImIp3nS	dávat
nositel	nositel	k1gMnSc1	nositel
najevo	najevo	k6eAd1	najevo
pohrdání	pohrdání	k1gNnSc6	pohrdání
druhými	druhý	k4xOgFnPc7	druhý
<g/>
,	,	kIx,	,
vulgárně	vulgárně	k6eAd1	vulgárně
<g/>
,	,	kIx,	,
urážlivě	urážlivě	k6eAd1	urážlivě
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
přitáhnout	přitáhnout	k5eAaPmF	přitáhnout
jejich	jejich	k3xOp3gFnSc4	jejich
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,	,
-	-	kIx~	-
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
použil	použít	k5eAaPmAgMnS	použít
jedinou	jediný	k2eAgFnSc4d1	jediná
nadávku	nadávka	k1gFnSc4	nadávka
<g/>
.	.	kIx.	.
</s>
<s>
Tolerantněji	tolerantně	k6eAd2	tolerantně
vulgaritu	vulgarita	k1gFnSc4	vulgarita
podle	podle	k7c2	podle
výzkumů	výzkum	k1gInPc2	výzkum
vnímají	vnímat	k5eAaImIp3nP	vnímat
voliči	volič	k1gMnPc1	volič
ODS	ODS	kA	ODS
než	než	k8xS	než
voliči	volič	k1gMnPc1	volič
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
Čechové	Čech	k1gMnPc1	Čech
než	než	k8xS	než
Moravané	Moravan	k1gMnPc1	Moravan
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
sociolog	sociolog	k1gMnSc1	sociolog
George	Georg	k1gMnSc2	Georg
Ritzer	Ritzer	k1gMnSc1	Ritzer
v	v	k7c6	v
koncepci	koncepce	k1gFnSc6	koncepce
mcdonaldizace	mcdonaldizace	k1gFnSc2	mcdonaldizace
společnosti	společnost	k1gFnSc2	společnost
uvádí	uvádět	k5eAaImIp3nS	uvádět
řadu	řada	k1gFnSc4	řada
negativních	negativní	k2eAgInPc2d1	negativní
jevů	jev	k1gInPc2	jev
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
řadí	řadit	k5eAaImIp3nS	řadit
i	i	k9	i
obecné	obecný	k2eAgNnSc1d1	obecné
snižování	snižování	k1gNnSc1	snižování
důvěry	důvěra	k1gFnSc2	důvěra
ve	v	k7c4	v
sliby	slib	k1gInPc4	slib
politiků	politik	k1gMnPc2	politik
a	a	k8xC	a
rozčarování	rozčarování	k1gNnSc1	rozčarování
z	z	k7c2	z
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
s	s	k7c7	s
čím	čí	k3xOyRgNnSc7	čí
je	být	k5eAaImIp3nS	být
spojována	spojovat	k5eAaImNgFnS	spojovat
i	i	k9	i
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
aroganci	arogance	k1gFnSc4	arogance
<g/>
,	,	kIx,	,
vulgarity	vulgarita	k1gFnPc4	vulgarita
a	a	k8xC	a
negativismus	negativismus	k1gInSc4	negativismus
staronových	staronový	k2eAgFnPc2d1	staronová
elit	elita	k1gFnPc2	elita
tendujících	tendující	k2eAgInPc2d1	tendující
k	k	k7c3	k
mafiánským	mafiánský	k2eAgFnPc3d1	mafiánská
praktikám	praktika	k1gFnPc3	praktika
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
chování	chování	k1gNnSc1	chování
je	být	k5eAaImIp3nS	být
dáváno	dáván	k2eAgNnSc1d1	dáváno
také	také	k9	také
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
tzv.	tzv.	kA	tzv.
blbé	blbý	k2eAgFnPc4d1	blbá
nálady	nálada	k1gFnPc4	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
zabývající	zabývající	k2eAgMnPc1d1	zabývající
se	se	k3xPyFc4	se
sociologií	sociologie	k1gFnSc7	sociologie
někdy	někdy	k6eAd1	někdy
uvádějí	uvádět	k5eAaImIp3nP	uvádět
že	že	k8xS	že
v	v	k7c6	v
kulturní	kulturní	k2eAgFnSc6d1	kulturní
tvorbě	tvorba	k1gFnSc6	tvorba
(	(	kIx(	(
<g/>
přenášené	přenášený	k2eAgFnSc2d1	přenášená
médii	médium	k1gNnPc7	médium
<g/>
)	)	kIx)	)
narůstá	narůstat	k5eAaImIp3nS	narůstat
disneyfikace	disneyfikace	k1gFnSc1	disneyfikace
nebo	nebo	k8xC	nebo
také	také	k9	také
banalizace	banalizace	k1gFnSc1	banalizace
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
žij	žít	k5eAaImRp2nS	žít
teď	teď	k6eAd1	teď
a	a	k8xC	a
buď	buď	k8xC	buď
v	v	k7c6	v
pohodě	pohoda	k1gFnSc6	pohoda
<g/>
,	,	kIx,	,
důležitý	důležitý	k2eAgInSc1d1	důležitý
je	být	k5eAaImIp3nS	být
dnešek	dnešek	k1gInSc1	dnešek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
popovou	popový	k2eAgFnSc7d1	popová
kulturou	kultura	k1gFnSc7	kultura
jako	jako	k8xS	jako
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
drogou	droga	k1gFnSc7	droga
a	a	k8xC	a
pro	pro	k7c4	pro
udržení	udržení	k1gNnSc4	udržení
pozornosti	pozornost	k1gFnSc2	pozornost
divák	divák	k1gMnSc1	divák
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
další	další	k2eAgInPc4d1	další
nové	nový	k2eAgInPc4d1	nový
podněty	podnět	k1gInPc4	podnět
<g/>
,	,	kIx,	,
porušování	porušování	k1gNnPc4	porušování
tabu	tabu	k2eAgNnPc4d1	tabu
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vulgární	vulgární	k2eAgInPc4d1	vulgární
výrazy	výraz	k1gInPc4	výraz
<g/>
,	,	kIx,	,
nevkus	nevkus	k1gInSc4	nevkus
<g/>
,	,	kIx,	,
perverzitu	perverzita	k1gFnSc4	perverzita
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
příklad	příklad	k1gInSc1	příklad
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
nepřetržitě	přetržitě	k6eNd1	přetržitě
tekoucí	tekoucí	k2eAgInSc1d1	tekoucí
proud	proud	k1gInSc1	proud
bezobsažných	bezobsažný	k2eAgFnPc2d1	bezobsažná
nadávek	nadávka	k1gFnPc2	nadávka
v	v	k7c6	v
přímých	přímý	k2eAgInPc6d1	přímý
přenosech	přenos	k1gInPc6	přenos
<g/>
,	,	kIx,	,
realityshow	realityshow	k?	realityshow
"	"	kIx"	"
<g/>
z	z	k7c2	z
milionové	milionový	k2eAgFnSc2d1	milionová
vily	vila	k1gFnSc2	vila
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nadávka	nadávka	k1gFnSc1	nadávka
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
také	také	k9	také
předmětem	předmět	k1gInSc7	předmět
soudního	soudní	k2eAgInSc2d1	soudní
sporu	spor	k1gInSc2	spor
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
některá	některý	k3yIgFnSc1	některý
osoba	osoba	k1gFnSc1	osoba
cítí	cítit	k5eAaImIp3nS	cítit
poškozena	poškodit	k5eAaPmNgFnS	poškodit
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
veřejnému	veřejný	k2eAgNnSc3d1	veřejné
pohoršení	pohoršení	k1gNnSc3	pohoršení
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
ohrožena	ohrožen	k2eAgFnSc1d1	ohrožena
mravní	mravní	k2eAgFnSc1d1	mravní
výchova	výchova	k1gFnSc1	výchova
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
sledovaným	sledovaný	k2eAgInSc7d1	sledovaný
druhem	druh	k1gInSc7	druh
nadávek	nadávka	k1gFnPc2	nadávka
jsou	být	k5eAaImIp3nP	být
nadávky	nadávka	k1gFnPc4	nadávka
s	s	k7c7	s
rasovým	rasový	k2eAgInSc7d1	rasový
podtextem	podtext	k1gInSc7	podtext
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgNnSc1d1	časté
používání	používání	k1gNnSc1	používání
nadávek	nadávka	k1gFnPc2	nadávka
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
na	na	k7c6	na
pracovištích	pracoviště	k1gNnPc6	pracoviště
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
mobbingem	mobbing	k1gInSc7	mobbing
a	a	k8xC	a
šikanou	šikana	k1gFnSc7	šikana
(	(	kIx(	(
<g/>
bullying	bullying	k1gInSc1	bullying
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
týrání	týrání	k1gNnSc3	týrání
kolegů	kolega	k1gMnPc2	kolega
<g/>
,	,	kIx,	,
podřízených	podřízený	k1gMnPc2	podřízený
<g/>
,	,	kIx,	,
žáků	žák	k1gMnPc2	žák
nebo	nebo	k8xC	nebo
spolužáků	spolužák	k1gMnPc2	spolužák
fyzickými	fyzický	k2eAgInPc7d1	fyzický
a	a	k8xC	a
verbálními	verbální	k2eAgInPc7d1	verbální
útoky	útok	k1gInPc7	útok
-	-	kIx~	-
nadávkami	nadávka	k1gFnPc7	nadávka
či	či	k8xC	či
posměchem	posměch	k1gInSc7	posměch
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
popsán	popsat	k5eAaPmNgInS	popsat
případ	případ	k1gInSc1	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
musela	muset	k5eAaImAgFnS	muset
zaplatit	zaplatit	k5eAaPmF	zaplatit
poškozenému	poškozený	k2eAgMnSc3d1	poškozený
řediteli	ředitel	k1gMnSc3	ředitel
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
Milanu	Milan	k1gMnSc3	Milan
Knížákovi	Knížák	k1gMnSc3	Knížák
náklady	náklad	k1gInPc7	náklad
řízení	řízení	k1gNnSc2	řízení
a	a	k8xC	a
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInSc4	tisíc
korun	koruna	k1gFnPc2	koruna
za	za	k7c4	za
vulgární	vulgární	k2eAgFnSc4d1	vulgární
urážku	urážka	k1gFnSc4	urážka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
čů	čů	k?	čů
<g/>
...	...	k?	...
Nafoukanej	Nafoukanej	k?	Nafoukanej
<g/>
,	,	kIx,	,
kriploidní	kriploidní	k2eAgMnSc1d1	kriploidní
čů	čů	k?	čů
<g/>
...	...	k?	...
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
úplně	úplně	k6eAd1	úplně
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
?	?	kIx.	?
</s>
<s>
To	ten	k3xDgNnSc4	ten
přece	přece	k9	přece
víme	vědět	k5eAaImIp1nP	vědět
všichni	všechen	k3xTgMnPc1	všechen
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
odvysílanou	odvysílaný	k2eAgFnSc4d1	odvysílaná
v	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
ČT	ČT	kA	ČT
<g/>
.	.	kIx.	.
</s>
<s>
Tutéž	týž	k3xTgFnSc4	týž
částku	částka	k1gFnSc4	částka
musel	muset	k5eAaImAgMnS	muset
uhradit	uhradit	k5eAaPmF	uhradit
výtvarník	výtvarník	k1gMnSc1	výtvarník
David	David	k1gMnSc1	David
Černý	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ředitele	ředitel	k1gMnSc4	ředitel
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
urazil	urazit	k5eAaPmAgInS	urazit
<g/>
.	.	kIx.	.
<g/>
ČT	ČT	kA	ČT
i	i	k8xC	i
výtvarník	výtvarník	k1gMnSc1	výtvarník
se	se	k3xPyFc4	se
poškozenému	poškozený	k1gMnSc3	poškozený
omluvili	omluvit	k5eAaPmAgMnP	omluvit
<g/>
,	,	kIx,	,
a	a	k8xC	a
odvolací	odvolací	k2eAgInSc1d1	odvolací
senát	senát	k1gInSc1	senát
tak	tak	k6eAd1	tak
řešil	řešit	k5eAaImAgInS	řešit
nárok	nárok	k1gInSc1	nárok
poškozeného	poškozený	k1gMnSc2	poškozený
na	na	k7c4	na
odškodnění	odškodnění	k1gNnSc4	odškodnění
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgMnSc1d1	známý
americký	americký	k2eAgMnSc1d1	americký
komik	komik	k1gMnSc1	komik
George	Georg	k1gFnSc2	Georg
Carlin	Carlin	k1gInSc1	Carlin
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
monologu	monolog	k1gInSc6	monolog
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
Seven	Sevno	k1gNnPc2	Sevno
dirty	dirta	k1gMnSc2	dirta
words	wordsa	k1gFnPc2	wordsa
(	(	kIx(	(
<g/>
Sedm	sedm	k4xCc1	sedm
ošklivých	ošklivý	k2eAgNnPc2d1	ošklivé
slov	slovo	k1gNnPc2	slovo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
souzen	soudit	k5eAaImNgMnS	soudit
v	v	k7c6	v
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
F.	F.	kA	F.
<g/>
C.C.	C.C.	k1gMnSc1	C.C.
v.	v.	k?	v.
Pacifica	Pacifica	k1gFnSc1	Pacifica
Foundation	Foundation	k1gInSc1	Foundation
<g/>
,	,	kIx,	,
projednávaném	projednávaný	k2eAgInSc6d1	projednávaný
u	u	k7c2	u
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případu	případ	k1gInSc6	případ
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
5	[number]	k4	5
soudců	soudce	k1gMnPc2	soudce
proti	proti	k7c3	proti
4	[number]	k4	4
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
pravomoc	pravomoc	k1gFnSc1	pravomoc
vlády	vláda	k1gFnSc2	vláda
k	k	k7c3	k
regulaci	regulace	k1gFnSc3	regulace
neslušného	slušný	k2eNgInSc2d1	neslušný
obsahu	obsah	k1gInSc2	obsah
ve	v	k7c6	v
veřejných	veřejný	k2eAgFnPc6d1	veřejná
rozhlasových	rozhlasový	k2eAgFnPc6d1	rozhlasová
stanicích	stanice	k1gFnPc6	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
George	Georg	k1gMnSc2	Georg
Carlina	Carlina	k1gFnSc1	Carlina
i	i	k8xC	i
část	část	k1gFnSc1	část
alba	album	k1gNnSc2	album
Class	Classa	k1gFnPc2	Classa
Clown	Clown	k1gMnSc1	Clown
(	(	kIx(	(
<g/>
Třídní	třídní	k1gMnSc1	třídní
klaun	klaun	k1gMnSc1	klaun
<g/>
)	)	kIx)	)
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Seven	Seven	k1gInSc1	Seven
Words	Wordsa	k1gFnPc2	Wordsa
You	You	k1gFnSc2	You
Can	Can	k1gFnPc1	Can
Never	Never	k1gInSc4	Never
Say	Say	k1gFnSc2	Say
on	on	k3xPp3gInSc1	on
Television	Television	k1gInSc1	Television
(	(	kIx(	(
<g/>
Sedm	sedm	k4xCc4	sedm
slov	slovo	k1gNnPc2	slovo
která	který	k3yRgFnSc1	který
nesmíte	smět	k5eNaImIp2nP	smět
říci	říct	k5eAaPmF	říct
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
letním	letní	k2eAgInSc6d1	letní
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Milwaukee	Milwaukee	k1gFnSc6	Milwaukee
byl	být	k5eAaImAgMnS	být
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
po	po	k7c6	po
vystoupení	vystoupení	k1gNnSc6	vystoupení
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
scénkou	scénka	k1gFnSc7	scénka
<g/>
,	,	kIx,	,
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
porušování	porušování	k1gNnSc2	porušování
mravnostních	mravnostní	k2eAgInPc2d1	mravnostní
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
výstup	výstup	k1gInSc4	výstup
byl	být	k5eAaImAgMnS	být
pak	pak	k6eAd1	pak
zatčen	zatknout	k5eAaPmNgMnS	zatknout
ještě	ještě	k6eAd1	ještě
několikrát	několikrát	k6eAd1	několikrát
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
komik	komik	k1gMnSc1	komik
a	a	k8xC	a
sociální	sociální	k2eAgMnSc1d1	sociální
kritik	kritik	k1gMnSc1	kritik
Lenny	Lenny	k?	Lenny
Bruce	Bruce	k1gMnSc1	Bruce
byl	být	k5eAaImAgMnS	být
známý	známý	k1gMnSc1	známý
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
a	a	k8xC	a
kritickou	kritický	k2eAgFnSc4d1	kritická
formu	forma	k1gFnSc4	forma
zábavných	zábavný	k2eAgNnPc2d1	zábavné
vystoupení	vystoupení	k1gNnPc2	vystoupení
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spojovala	spojovat	k5eAaImAgFnS	spojovat
satiru	satira	k1gFnSc4	satira
<g/>
,	,	kIx,	,
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
sex	sex	k1gInSc4	sex
a	a	k8xC	a
vulgárnost	vulgárnost	k1gFnSc4	vulgárnost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
opakovaně	opakovaně	k6eAd1	opakovaně
zatčen	zatknout	k5eAaPmNgMnS	zatknout
pro	pro	k7c4	pro
oplzlost	oplzlost	k1gFnSc4	oplzlost
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
působit	působit	k5eAaImF	působit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
městech	město	k1gNnPc6	město
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Bruce	Bruce	k1gMnSc1	Bruce
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
měl	mít	k5eAaImAgInS	mít
nastoupit	nastoupit	k5eAaPmF	nastoupit
čtyřměsíční	čtyřměsíční	k2eAgInSc1d1	čtyřměsíční
trest	trest	k1gInSc1	trest
uložený	uložený	k2eAgInSc1d1	uložený
soudem	soud	k1gInSc7	soud
v	v	k7c4	v
New	New	k1gFnSc4	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Trest	trest	k1gInSc4	trest
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
prominut	prominut	k2eAgMnSc1d1	prominut
třicet	třicet	k4xCc4	třicet
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jim	on	k3xPp3gMnPc3	on
Morrison	Morrison	k1gInSc1	Morrison
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
několikrát	několikrát	k6eAd1	několikrát
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
za	za	k7c4	za
vulgární	vulgární	k2eAgInPc4d1	vulgární
výrazy	výraz	k1gInPc4	výraz
a	a	k8xC	a
obscénní	obscénní	k2eAgNnSc4d1	obscénní
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Nadávka	nadávka	k1gFnSc1	nadávka
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
není	být	k5eNaImIp3nS	být
trestným	trestný	k2eAgInSc7d1	trestný
činem	čin	k1gInSc7	čin
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
součástí	součást	k1gFnSc7	součást
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
stát	stát	k5eAaImF	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
Walesu	Wales	k1gInSc2	Wales
je	být	k5eAaImIp3nS	být
nadávání	nadávání	k1gNnSc1	nadávání
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
kde	kde	k6eAd1	kde
způsobí	způsobit	k5eAaPmIp3nS	způsobit
obtěžování	obtěžování	k1gNnSc4	obtěžování
nebo	nebo	k8xC	nebo
obavy	obava	k1gFnPc4	obava
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
představovat	představovat	k5eAaImF	představovat
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
<g/>
,	,	kIx,	,
porušení	porušení	k1gNnSc4	porušení
veřejného	veřejný	k2eAgInSc2d1	veřejný
pořádku	pořádek	k1gInSc2	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
se	s	k7c7	s
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
<g/>
,	,	kIx,	,
rasově	rasově	k6eAd1	rasově
zaměřené	zaměřený	k2eAgFnPc4d1	zaměřená
nadávky	nadávka	k1gFnPc4	nadávka
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
směřovány	směřován	k2eAgFnPc1d1	směřována
na	na	k7c4	na
politiky	politik	k1gMnPc4	politik
se	s	k7c7	s
semitskými	semitský	k2eAgInPc7d1	semitský
nebo	nebo	k8xC	nebo
negroidními	negroidní	k2eAgInPc7d1	negroidní
rysy	rys	k1gInPc7	rys
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pokud	pokud	k8xS	pokud
svým	svůj	k3xOyFgNnSc7	svůj
působením	působení	k1gNnSc7	působení
vynikají	vynikat	k5eAaImIp3nP	vynikat
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
účastníky	účastník	k1gMnPc7	účastník
politických	politický	k2eAgFnPc2d1	politická
konfrontací	konfrontace	k1gFnPc2	konfrontace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
poslanců	poslanec	k1gMnPc2	poslanec
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
zaplacení	zaplacení	k1gNnSc3	zaplacení
pokuty	pokuta	k1gFnSc2	pokuta
3000	[number]	k4	3000
eur	euro	k1gNnPc2	euro
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přirovnal	přirovnat	k5eAaPmAgMnS	přirovnat
ministryni	ministryně	k1gFnSc4	ministryně
Christiane	Christian	k1gMnSc5	Christian
Taubirovou	Taubirová	k1gFnSc4	Taubirová
<g/>
,	,	kIx,	,
černošku	černoška	k1gFnSc4	černoška
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
Guyany	Guyana	k1gFnSc2	Guyana
<g/>
,	,	kIx,	,
k	k	k7c3	k
opici	opice	k1gFnSc3	opice
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
Francouzské	francouzský	k2eAgFnSc6d1	francouzská
Guyaně	Guyana	k1gFnSc6	Guyana
byla	být	k5eAaImAgFnS	být
kandidátka	kandidátka	k1gFnSc1	kandidátka
krajně	krajně	k6eAd1	krajně
pravicové	pravicový	k2eAgFnSc2d1	pravicová
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
odsouzena	odsouzet	k5eAaImNgFnS	odsouzet
za	za	k7c4	za
stejný	stejný	k2eAgInSc4d1	stejný
čin	čin	k1gInSc4	čin
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
devítiměsíčního	devítiměsíční	k2eAgNnSc2d1	devítiměsíční
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
KLDR	KLDR	kA	KLDR
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Barack	Baracko	k1gNnPc2	Baracko
Obama	Obama	k?	Obama
přirovnán	přirovnat	k5eAaPmNgInS	přirovnat
k	k	k7c3	k
opici	opice	k1gFnSc3	opice
-	-	kIx~	-
"	"	kIx"	"
<g/>
Bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nP	by
lepší	dobrý	k2eAgNnSc1d2	lepší
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
(	(	kIx(	(
<g/>
Obama	Obama	k?	Obama
<g/>
)	)	kIx)	)
žil	žít	k5eAaImAgInS	žít
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
opicemi	opice	k1gFnPc7	opice
v	v	k7c6	v
přírodní	přírodní	k2eAgFnSc6d1	přírodní
rezervaci	rezervace	k1gFnSc6	rezervace
někde	někde	k6eAd1	někde
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
...	...	k?	...
a	a	k8xC	a
sbíral	sbírat	k5eAaImAgMnS	sbírat
drobky	drobek	k1gInPc4	drobek
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mu	on	k3xPp3gInSc3	on
pohodí	pohodit	k5eAaPmIp3nS	pohodit
návštěvníci	návštěvník	k1gMnPc1	návštěvník
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Prezident	prezident	k1gMnSc1	prezident
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
V.	V.	kA	V.
Putin	putin	k2eAgInSc1d1	putin
zaslal	zaslat	k5eAaPmAgMnS	zaslat
belgickému	belgický	k2eAgInSc3d1	belgický
deníku	deník	k1gInSc3	deník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
fotomontáž	fotomontáž	k1gFnSc1	fotomontáž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
Barack	Barack	k1gInSc4	Barack
Obama	Obama	k?	Obama
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
stylizováni	stylizován	k2eAgMnPc1d1	stylizován
jako	jako	k8xS	jako
opice	opice	k1gFnPc1	opice
<g/>
.	.	kIx.	.
</s>
<s>
Deník	deník	k1gInSc1	deník
fotomontáž	fotomontáž	k1gFnSc4	fotomontáž
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
karikaturu	karikatura	k1gFnSc4	karikatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
narozenin	narozeniny	k1gFnPc2	narozeniny
B.	B.	kA	B.
Obamy	Obama	k1gFnPc4	Obama
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
ruská	ruský	k2eAgFnSc1d1	ruská
pravicová	pravicový	k2eAgFnSc1d1	pravicová
organizace	organizace	k1gFnSc1	organizace
na	na	k7c6	na
zdech	zeď	k1gFnPc6	zeď
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
USA	USA	kA	USA
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
sérii	série	k1gFnSc4	série
laserových	laserový	k2eAgInPc2d1	laserový
obrazců	obrazec	k1gInPc2	obrazec
symbolizujících	symbolizující	k2eAgInPc2d1	symbolizující
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
pojídajícího	pojídající	k2eAgInSc2d1	pojídající
banán	banán	k1gInSc1	banán
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
poslankyně	poslankyně	k1gFnSc1	poslankyně
a	a	k8xC	a
bývalá	bývalý	k2eAgFnSc1d1	bývalá
olympijská	olympijský	k2eAgFnSc1d1	olympijská
šampionka	šampionka	k1gFnSc1	šampionka
v	v	k7c6	v
krasobruslení	krasobruslení	k1gNnSc6	krasobruslení
Irina	Irien	k2eAgFnSc1d1	Irina
Rodninová	Rodninový	k2eAgFnSc1d1	Rodninový
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
fotografii	fotografia	k1gFnSc4	fotografia
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgInPc4	který
je	být	k5eAaImIp3nS	být
B.	B.	kA	B.
Obama	Obama	k?	Obama
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Michelle	Michelle	k1gFnSc2	Michelle
s	s	k7c7	s
rukou	ruka	k1gFnSc7	ruka
držící	držící	k2eAgInSc1d1	držící
banán	banán	k1gInSc1	banán
v	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
<g/>
.	.	kIx.	.
</s>
<s>
Rasistické	rasistický	k2eAgInPc1d1	rasistický
plakáty	plakát	k1gInPc1	plakát
s	s	k7c7	s
B.	B.	kA	B.
Obamou	Obama	k1gFnSc7	Obama
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
i	i	k9	i
na	na	k7c6	na
bilboardech	bilboard	k1gInPc6	bilboard
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Zesměšnění	zesměšnění	k1gNnSc1	zesměšnění
protivníka	protivník	k1gMnSc2	protivník
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
politického	politický	k2eAgNnSc2d1	politické
soupeření	soupeření	k1gNnSc2	soupeření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
může	moct	k5eAaImIp3nS	moct
někdy	někdy	k6eAd1	někdy
překročit	překročit	k5eAaPmF	překročit
hranice	hranice	k1gFnPc4	hranice
vkusu	vkus	k1gInSc2	vkus
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
respektovány	respektován	k2eAgFnPc4d1	respektována
meze	mez	k1gFnPc4	mez
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sportu	sport	k1gInSc6	sport
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
odsuzovány	odsuzován	k2eAgFnPc1d1	odsuzována
i	i	k8xC	i
rasistické	rasistický	k2eAgFnPc1d1	rasistická
urážky	urážka	k1gFnPc1	urážka
soupeře	soupeř	k1gMnPc4	soupeř
fanoušky	fanoušek	k1gMnPc4	fanoušek
<g/>
,	,	kIx,	,
urážky	urážka	k1gFnPc4	urážka
mezi	mezi	k7c7	mezi
soupeři	soupeř	k1gMnPc7	soupeř
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
nesportovní	sportovní	k2eNgInPc4d1	nesportovní
nebo	nebo	k8xC	nebo
nevhodné	vhodný	k2eNgInPc4d1	nevhodný
a	a	k8xC	a
trestány	trestán	k2eAgInPc4d1	trestán
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
South	Southa	k1gFnPc2	Southa
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
naplněné	naplněný	k2eAgFnPc1d1	naplněná
vulgaritami	vulgarita	k1gFnPc7	vulgarita
<g/>
,	,	kIx,	,
nadávkami	nadávka	k1gFnPc7	nadávka
<g/>
,	,	kIx,	,
násilím	násilí	k1gNnSc7	násilí
<g/>
,	,	kIx,	,
zvracejícími	zvracející	k2eAgFnPc7d1	zvracející
a	a	k8xC	a
vyměšujícími	vyměšující	k2eAgFnPc7d1	vyměšující
postavami	postava	k1gFnPc7	postava
byl	být	k5eAaImAgInS	být
oblíben	oblíbit	k5eAaPmNgMnS	oblíbit
voliči	volič	k1gMnPc7	volič
středopravého	středopravý	k2eAgNnSc2d1	středopravé
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
teenagerů	teenager	k1gMnPc2	teenager
a	a	k8xC	a
mladých	mladý	k2eAgMnPc2d1	mladý
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
oblíbili	oblíbit	k5eAaPmAgMnP	oblíbit
pořad	pořad	k1gInSc4	pořad
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
tendenci	tendence	k1gFnSc4	tendence
zesměšňovat	zesměšňovat	k5eAaImF	zesměšňovat
liberální	liberální	k2eAgInPc4d1	liberální
názory	názor	k1gInPc4	názor
a	a	k8xC	a
parodovat	parodovat	k5eAaImF	parodovat
liberální	liberální	k2eAgFnPc4d1	liberální
celebrity	celebrita	k1gFnPc4	celebrita
a	a	k8xC	a
ikony	ikona	k1gFnPc4	ikona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
český	český	k2eAgMnSc1d1	český
premiér	premiér	k1gMnSc1	premiér
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
omlouval	omlouvat	k5eAaImAgMnS	omlouvat
za	za	k7c4	za
zdvižený	zdvižený	k2eAgInSc4d1	zdvižený
prostředníček	prostředníček	k1gInSc4	prostředníček
<g/>
.	.	kIx.	.
</s>
<s>
Nemravné	mravný	k2eNgNnSc4d1	nemravné
gesto	gesto	k1gNnSc4	gesto
udělal	udělat	k5eAaPmAgMnS	udělat
na	na	k7c4	na
poslance	poslanec	k1gMnSc4	poslanec
opozice	opozice	k1gFnSc2	opozice
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
předtím	předtím	k6eAd1	předtím
dožadovali	dožadovat	k5eAaImAgMnP	dožadovat
příchodu	příchod	k1gInSc2	příchod
členů	člen	k1gMnPc2	člen
vlády	vláda	k1gFnSc2	vláda
na	na	k7c6	na
jednání	jednání	k1gNnSc6	jednání
Sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
spojené	spojený	k2eAgNnSc1d1	spojené
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
gesto	gesto	k1gNnSc1	gesto
vlastně	vlastně	k9	vlastně
není	být	k5eNaImIp3nS	být
nadávka	nadávka	k1gFnSc1	nadávka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Jsi	být	k5eAaImIp2nS	být
jednička	jednička	k1gFnSc1	jednička
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Stejné	stejný	k2eAgNnSc4d1	stejné
gesto	gesto	k1gNnSc4	gesto
použil	použít	k5eAaPmAgInS	použít
později	pozdě	k6eAd2	pozdě
i	i	k9	i
jiný	jiný	k2eAgMnSc1d1	jiný
poslanec	poslanec	k1gMnSc1	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
prezident	prezident	k1gMnSc1	prezident
ČR	ČR	kA	ČR
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
po	po	k7c6	po
široké	široký	k2eAgFnSc6d1	široká
mediální	mediální	k2eAgFnSc6d1	mediální
kritice	kritika	k1gFnSc6	kritika
jeho	jeho	k3xOp3gInPc2	jeho
projevů	projev	k1gInPc2	projev
během	během	k7c2	během
návštěvy	návštěva	k1gFnSc2	návštěva
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
v	v	k7c6	v
živě	živě	k6eAd1	živě
vysílaném	vysílaný	k2eAgInSc6d1	vysílaný
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
pořadu	pořad	k1gInSc6	pořad
"	"	kIx"	"
<g/>
Hovory	hovora	k1gMnPc4	hovora
z	z	k7c2	z
Lán	lán	k1gInSc4	lán
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vláda	vláda	k1gFnSc1	vláda
podle	podle	k7c2	podle
mého	můj	k3xOp1gInSc2	můj
názoru	názor	k1gInSc2	názor
dosud	dosud	k6eAd1	dosud
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
konání	konání	k1gNnSc6	konání
učinila	učinit	k5eAaPmAgFnS	učinit
jednu	jeden	k4xCgFnSc4	jeden
základní	základní	k2eAgFnSc4d1	základní
chybu	chyba	k1gFnSc4	chyba
<g/>
,	,	kIx,	,
že	že	k8xS	že
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
panu	pan	k1gMnSc3	pan
Kalouskovi	Kalousek	k1gMnSc3	Kalousek
a	a	k8xC	a
zkurvila	zkurvit	k5eAaPmAgFnS	zkurvit
služební	služební	k2eAgInSc4d1	služební
zákon	zákon	k1gInSc4	zákon
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zkritizoval	zkritizovat	k5eAaPmAgMnS	zkritizovat
ruskou	ruský	k2eAgFnSc4d1	ruská
dívčí	dívčí	k2eAgFnSc4d1	dívčí
punkerskou	punkerský	k2eAgFnSc4d1	punkerská
skupinu	skupina	k1gFnSc4	skupina
Pussy	Pussa	k1gFnSc2	Pussa
Riot	Riot	k1gInSc4	Riot
za	za	k7c4	za
její	její	k3xOp3gFnSc4	její
formu	forma	k1gFnSc4	forma
protestů	protest	k1gInPc2	protest
<g/>
,	,	kIx,	,
označil	označit	k5eAaPmAgMnS	označit
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
"	"	kIx"	"
<g/>
pornografickou	pornografický	k2eAgFnSc4d1	pornografická
skupinku	skupinka	k1gFnSc4	skupinka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
zeptal	zeptat	k5eAaPmAgMnS	zeptat
se	se	k3xPyFc4	se
moderátora	moderátor	k1gMnSc2	moderátor
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
"	"	kIx"	"
<g/>
pussy	puss	k1gInPc7	puss
<g/>
"	"	kIx"	"
a	a	k8xC	a
hned	hned	k6eAd1	hned
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kunda	Kunda	k1gMnSc1	Kunda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
textech	text	k1gInPc6	text
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
kunda	kunda	k1gFnSc1	kunda
sem	sem	k6eAd1	sem
<g/>
,	,	kIx,	,
kunda	kunda	k1gFnSc1	kunda
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
opravdu	opravdu	k6eAd1	opravdu
dokonalý	dokonalý	k2eAgInSc1d1	dokonalý
případ	případ	k1gInSc1	případ
politického	politický	k2eAgMnSc2d1	politický
vězně	vězeň	k1gMnSc2	vězeň
jako	jako	k8xS	jako
vyšitý	vyšitý	k2eAgMnSc1d1	vyšitý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
textu	text	k1gInSc6	text
Pussy	Pussa	k1gFnSc2	Pussa
Riot	Riota	k1gFnPc2	Riota
ani	ani	k8xC	ani
jednou	jednou	k6eAd1	jednou
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
anglické	anglický	k2eAgNnSc1d1	anglické
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
pussy	pussa	k1gFnPc1	pussa
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
vulgárních	vulgární	k2eAgInPc2d1	vulgární
výrazů	výraz	k1gInPc2	výraz
ve	v	k7c6	v
zmíněném	zmíněný	k2eAgInSc6d1	zmíněný
pořadu	pořad	k1gInSc6	pořad
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
promyšleným	promyšlený	k2eAgMnPc3d1	promyšlený
a	a	k8xC	a
předem	předem	k6eAd1	předem
připraveným	připravený	k2eAgInSc7d1	připravený
proslovem	proslov	k1gInSc7	proslov
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
kritika	kritika	k1gFnSc1	kritika
mediálně	mediálně	k6eAd1	mediálně
zcela	zcela	k6eAd1	zcela
zastínila	zastínit	k5eAaPmAgFnS	zastínit
kritiku	kritika	k1gFnSc4	kritika
výpravy	výprava	k1gFnSc2	výprava
českého	český	k2eAgMnSc2d1	český
prezidenta	prezident	k1gMnSc2	prezident
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
tamní	tamní	k2eAgInPc1d1	tamní
projevy	projev	k1gInPc1	projev
<g/>
.	.	kIx.	.
</s>
<s>
Aféra	aféra	k1gFnSc1	aféra
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
ovšem	ovšem	k9	ovšem
i	i	k9	i
veřejné	veřejný	k2eAgInPc4d1	veřejný
protesty	protest	k1gInPc4	protest
a	a	k8xC	a
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
zájem	zájem	k1gInSc4	zájem
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
se	se	k3xPyFc4	se
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
omluvit	omluvit	k5eAaPmF	omluvit
<g/>
,	,	kIx,	,
hájil	hájit	k5eAaImAgMnS	hájit
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
svými	svůj	k3xOyFgMnPc7	svůj
vulgarismy	vulgarismus	k1gInPc1	vulgarismus
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
jeho	jeho	k3xOp3gMnSc1	jeho
protikandidát	protikandidát	k1gMnSc1	protikandidát
z	z	k7c2	z
volby	volba	k1gFnSc2	volba
o	o	k7c4	o
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
post	post	k1gInSc4	post
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
panem	pan	k1gMnSc7	pan
Schwarzenbergem	Schwarzenberg	k1gMnSc7	Schwarzenberg
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
druhé	druhý	k4xOgFnSc6	druhý
větě	věta	k1gFnSc6	věta
říká	říkat	k5eAaImIp3nS	říkat
hovno	hovno	k1gNnSc1	hovno
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Výsledkem	výsledek	k1gInSc7	výsledek
chování	chování	k1gNnSc2	chování
českého	český	k2eAgMnSc2d1	český
prezidenta	prezident	k1gMnSc2	prezident
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgFnP	být
i	i	k9	i
narážky	narážka	k1gFnPc1	narážka
rádia	rádius	k1gInSc2	rádius
Frekvence	frekvence	k1gFnSc1	frekvence
1	[number]	k4	1
v	v	k7c6	v
ranních	ranní	k2eAgInPc6d1	ranní
pořadech	pořad	k1gInPc6	pořad
<g/>
,	,	kIx,	,
vulgarismy	vulgarismus	k1gInPc1	vulgarismus
a	a	k8xC	a
nadávky	nadávka	k1gFnPc1	nadávka
v	v	k7c6	v
ranních	ranní	k2eAgInPc6d1	ranní
telefonátech	telefonát	k1gInPc6	telefonát
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Neboť	neboť	k8xC	neboť
není	být	k5eNaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nadsázku	nadsázka	k1gFnSc4	nadsázka
<g/>
,	,	kIx,	,
pořad	pořad	k1gInSc1	pořad
je	být	k5eAaImIp3nS	být
obtížně	obtížně	k6eAd1	obtížně
rozeznatelnou	rozeznatelný	k2eAgFnSc7d1	rozeznatelná
mystifikací	mystifikace	k1gFnSc7	mystifikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
ranním	ranní	k2eAgInSc7d1	ranní
satirickým	satirický	k2eAgInSc7d1	satirický
pořadem	pořad	k1gInSc7	pořad
je	být	k5eAaImIp3nS	být
zajímavým	zajímavý	k2eAgNnSc7d1	zajímavé
zjištěním	zjištění	k1gNnSc7	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
v	v	k7c6	v
ranním	ranní	k2eAgNnSc6d1	ranní
vysílání	vysílání	k1gNnSc6	vysílání
jsou	být	k5eAaImIp3nP	být
nadávky	nadávka	k1gFnPc4	nadávka
nejméně	málo	k6eAd3	málo
přijatelné	přijatelný	k2eAgFnPc4d1	přijatelná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
politice	politika	k1gFnSc6	politika
nejsou	být	k5eNaImIp3nP	být
ovšem	ovšem	k9	ovšem
nadávky	nadávka	k1gFnPc1	nadávka
a	a	k8xC	a
urážky	urážka	k1gFnPc1	urážka
i	i	k9	i
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
"	"	kIx"	"
<g/>
bonmotů	bonmot	k1gInPc2	bonmot
<g/>
"	"	kIx"	"
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
nijak	nijak	k6eAd1	nijak
vzácným	vzácný	k2eAgInSc7d1	vzácný
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
uvádí	uvádět	k5eAaImIp3nS	uvádět
následující	následující	k2eAgInSc1d1	následující
seznam	seznam	k1gInSc1	seznam
nadávek	nadávka	k1gFnPc2	nadávka
popsaných	popsaný	k2eAgInPc2d1	popsaný
a	a	k8xC	a
užívaný	užívaný	k2eAgInSc1d1	užívaný
Radou	rada	k1gFnSc7	rada
pro	pro	k7c4	pro
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
a	a	k8xC	a
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
:	:	kIx,	:
Existenci	existence	k1gFnSc4	existence
a	a	k8xC	a
pravost	pravost	k1gFnSc4	pravost
seznamu	seznam	k1gInSc2	seznam
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
i	i	k9	i
další	další	k2eAgInPc4d1	další
zdroje	zdroj	k1gInPc4	zdroj
,	,	kIx,	,
avšak	avšak	k8xC	avšak
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
RRTV	RRTV	kA	RRTV
Kateřina	Kateřina	k1gFnSc1	Kateřina
Kalistová	Kalistová	k1gFnSc1	Kalistová
oznámila	oznámit	k5eAaPmAgFnS	oznámit
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
seznam	seznam	k1gInSc4	seznam
nadávek	nadávka	k1gFnPc2	nadávka
zkopírovaný	zkopírovaný	k2eAgMnSc1d1	zkopírovaný
z	z	k7c2	z
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
považuje	považovat	k5eAaImIp3nS	považovat
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
RRTV	RRTV	kA	RRTV
za	za	k7c4	za
nedokonalý	dokonalý	k2eNgInSc4d1	nedokonalý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
provozovatele	provozovatel	k1gMnSc4	provozovatel
vysílání	vysílání	k1gNnSc2	vysílání
podle	podle	k7c2	podle
tvrzení	tvrzení	k1gNnSc2	tvrzení
předsedkyně	předsedkyně	k1gFnSc2	předsedkyně
RRTV	RRTV	kA	RRTV
nemá	mít	k5eNaImIp3nS	mít
nedodržení	nedodržení	k1gNnSc4	nedodržení
seznamu	seznam	k1gInSc2	seznam
žádný	žádný	k3yNgInSc4	žádný
dopad	dopad	k1gInSc4	dopad
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
varován	varován	k2eAgMnSc1d1	varován
<g/>
,	,	kIx,	,
RRTV	RRTV	kA	RRTV
nemůže	moct	k5eNaImIp3nS	moct
přistoupit	přistoupit	k5eAaPmF	přistoupit
k	k	k7c3	k
uložení	uložení	k1gNnSc3	uložení
sankce	sankce	k1gFnSc2	sankce
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
použity	použít	k5eAaPmNgInP	použít
vulgarismy	vulgarismus	k1gInPc1	vulgarismus
v	v	k7c6	v
edukativním	edukativní	k2eAgInSc6d1	edukativní
pořadu	pořad	k1gInSc6	pořad
určeném	určený	k2eAgInSc6d1	určený
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jsou	být	k5eAaImIp3nP	být
děti	dítě	k1gFnPc1	dítě
proti	proti	k7c3	proti
takovému	takový	k3xDgInSc3	takový
činu	čin	k1gInSc3	čin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
narušit	narušit	k5eAaPmF	narušit
jejich	jejich	k3xOp3gInSc4	jejich
zdravý	zdravý	k2eAgInSc4d1	zdravý
mravní	mravní	k2eAgInSc4d1	mravní
či	či	k8xC	či
duševní	duševní	k2eAgInSc4d1	duševní
vývoj	vývoj	k1gInSc4	vývoj
<g/>
,	,	kIx,	,
chráněny	chránit	k5eAaImNgFnP	chránit
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
televizním	televizní	k2eAgNnSc6d1	televizní
a	a	k8xC	a
rozhlasovém	rozhlasový	k2eAgNnSc6d1	rozhlasové
vysílání	vysílání	k1gNnSc6	vysílání
zakazovány	zakazován	k2eAgInPc4d1	zakazován
některé	některý	k3yIgInPc4	některý
výrazy	výraz	k1gInPc4	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
podle	podle	k7c2	podle
uvedených	uvedený	k2eAgNnPc2d1	uvedené
kritérií	kritérion	k1gNnPc2	kritérion
není	být	k5eNaImIp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
zařadit	zařadit	k5eAaPmF	zařadit
některé	některý	k3yIgFnPc4	některý
nadávky	nadávka	k1gFnPc4	nadávka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
čapkovské	čapkovský	k2eAgNnSc1d1	čapkovské
"	"	kIx"	"
<g/>
ťululum	ťululum	k1gNnSc1	ťululum
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
je	být	k5eAaImIp3nS	být
proslulá	proslulý	k2eAgFnSc1d1	proslulá
humorem	humor	k1gInSc7	humor
komediální	komediální	k2eAgFnSc2d1	komediální
skupiny	skupina	k1gFnSc2	skupina
Monty	Monta	k1gFnSc2	Monta
Python	Python	k1gMnSc1	Python
(	(	kIx(	(
<g/>
Monty	Monta	k1gMnSc2	Monta
Python	Python	k1gMnSc1	Python
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Flying	Flying	k1gInSc1	Flying
Circus	Circus	k1gInSc4	Circus
<g/>
)	)	kIx)	)
jenž	jenž	k3xRgMnSc1	jenž
také	také	k6eAd1	také
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
skečů	skeč	k1gInPc2	skeč
často	často	k6eAd1	často
naráží	narážet	k5eAaPmIp3nS	narážet
na	na	k7c4	na
cenzuru	cenzura	k1gFnSc4	cenzura
nadávek	nadávka	k1gFnPc2	nadávka
nebo	nebo	k8xC	nebo
obnažených	obnažený	k2eAgFnPc2d1	obnažená
částí	část	k1gFnPc2	část
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1	relativní
závažnost	závažnost	k1gFnSc1	závažnost
různých	různý	k2eAgInPc2d1	různý
vulgarismů	vulgarismus	k1gInPc2	vulgarismus
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc6	jejich
vnímání	vnímání	k1gNnSc6	vnímání
veřejností	veřejnost	k1gFnSc7	veřejnost
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
posuzována	posuzovat	k5eAaImNgFnS	posuzovat
organizacemi	organizace	k1gFnPc7	organizace
Broadcasting	Broadcasting	k1gInSc4	Broadcasting
Standards	Standards	k1gInSc1	Standards
Commission	Commission	k1gInSc1	Commission
(	(	kIx(	(
<g/>
Office	Office	kA	Office
of	of	k?	of
Communications	Communications	k1gInSc1	Communications
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Advertising	Advertising	k1gInSc1	Advertising
Standards	Standardsa	k1gFnPc2	Standardsa
Authority	Authorita	k1gFnSc2	Authorita
(	(	kIx(	(
<g/>
Independent	independent	k1gMnSc1	independent
Television	Television	k1gInSc4	Television
Commission	Commission	k1gInSc1	Commission
<g/>
)	)	kIx)	)
a	a	k8xC	a
BBC	BBC	kA	BBC
<g/>
.	.	kIx.	.
</s>
<s>
Tématem	téma	k1gNnSc7	téma
výzkumu	výzkum	k1gInSc2	výzkum
bylo	být	k5eAaImAgNnS	být
například	například	k6eAd1	například
zjištění	zjištění	k1gNnSc1	zjištění
vnímání	vnímání	k1gNnSc2	vnímání
nadávek	nadávka	k1gFnPc2	nadávka
ve	v	k7c6	v
skutečném	skutečný	k2eAgInSc6d1	skutečný
životě	život	k1gInSc6	život
(	(	kIx(	(
<g/>
Real	Real	k1gInSc1	Real
life	lif	k1gFnSc2	lif
and	and	k?	and
swearing	swearing	k1gInSc1	swearing
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
výzkumu	výzkum	k1gInSc2	výzkum
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
byly	být	k5eAaImAgFnP	být
zveřejněny	zveřejnit	k5eAaPmNgFnP	zveřejnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Delete	Dele	k1gNnSc2	Dele
expletives	expletives	k1gInSc1	expletives
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Smazat	smazat	k5eAaPmF	smazat
nadávku	nadávka	k1gFnSc4	nadávka
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
respondenti	respondent	k1gMnPc1	respondent
přijmou	přijmout	k5eAaPmIp3nP	přijmout
nebo	nebo	k8xC	nebo
schvalují	schvalovat	k5eAaImIp3nP	schvalovat
obsah	obsah	k1gInSc4	obsah
programu	program	k1gInSc2	program
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
redakčně	redakčně	k6eAd1	redakčně
oprávněný	oprávněný	k2eAgInSc1d1	oprávněný
nebo	nebo	k8xC	nebo
hodnotný	hodnotný	k2eAgInSc1d1	hodnotný
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
nadávky	nadávka	k1gFnPc1	nadávka
zahrnuty	zahrnout	k5eAaPmNgFnP	zahrnout
v	v	k7c6	v
programu	program	k1gInSc6	program
"	"	kIx"	"
<g/>
bezdůvodně	bezdůvodně	k6eAd1	bezdůvodně
<g/>
"	"	kIx"	"
hůře	zle	k6eAd2	zle
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
podporu	podpora	k1gFnSc4	podpora
publika	publikum	k1gNnSc2	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
je	být	k5eAaImIp3nS	být
jasně	jasně	k6eAd1	jasně
rozlišováno	rozlišován	k2eAgNnSc1d1	rozlišováno
médium	médium	k1gNnSc1	médium
v	v	k7c6	v
kterém	který	k3yIgInSc6	který
jsou	být	k5eAaImIp3nP	být
nadávky	nadávka	k1gFnPc1	nadávka
nebo	nebo	k8xC	nebo
silná	silný	k2eAgNnPc1d1	silné
slova	slovo	k1gNnPc1	slovo
používána	používán	k2eAgNnPc1d1	používáno
<g/>
,	,	kIx,	,
nadávky	nadávka	k1gFnPc4	nadávka
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
respondentů	respondent	k1gMnPc2	respondent
negativně	negativně	k6eAd1	negativně
vnímány	vnímat	k5eAaImNgInP	vnímat
na	na	k7c4	na
BBC	BBC	kA	BBC
2	[number]	k4	2
a	a	k8xC	a
malou	malý	k2eAgFnSc7d1	malá
menšinou	menšina	k1gFnSc7	menšina
negativně	negativně	k6eAd1	negativně
na	na	k7c4	na
Channel	Channel	k1gInSc4	Channel
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
tak	tak	k9	tak
ukázal	ukázat	k5eAaPmAgInS	ukázat
že	že	k8xS	že
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
zákaz	zákaz	k1gInSc4	zákaz
nadávek	nadávka	k1gFnPc2	nadávka
a	a	k8xC	a
sprostých	sprostý	k2eAgNnPc2d1	sprosté
slov	slovo	k1gNnPc2	slovo
není	být	k5eNaImIp3nS	být
nejen	nejen	k6eAd1	nejen
vyžadován	vyžadován	k2eAgInSc1d1	vyžadován
ale	ale	k9	ale
ani	ani	k9	ani
vnímán	vnímat	k5eAaImNgInS	vnímat
jako	jako	k8xS	jako
relevantní	relevantní	k2eAgFnSc1d1	relevantní
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zmiňováno	zmiňován	k2eAgNnSc1d1	zmiňováno
že	že	k8xS	že
přitom	přitom	k6eAd1	přitom
není	být	k5eNaImIp3nS	být
významné	významný	k2eAgNnSc1d1	významné
zda	zda	k8xS	zda
jde	jít	k5eAaImIp3nS	jít
přímo	přímo	k6eAd1	přímo
o	o	k7c4	o
večerní	večerní	k2eAgInSc4d1	večerní
pořad	pořad	k1gInSc4	pořad
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
průzkum	průzkum	k1gInSc1	průzkum
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
byly	být	k5eAaImAgInP	být
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2010	[number]	k4	2010
ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
What	What	k1gInSc1	What
Not	nota	k1gFnPc2	nota
to	ten	k3xDgNnSc4	ten
Swear	Swear	k1gMnSc1	Swear
<g/>
"	"	kIx"	"
.	.	kIx.	.
</s>
<s>
Úřady	úřad	k1gInPc1	úřad
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
zjištěními	zjištění	k1gNnPc7	zjištění
jak	jak	k8xC	jak
jsou	být	k5eAaImIp3nP	být
nebo	nebo	k8xC	nebo
nejsou	být	k5eNaImIp3nP	být
akceptovatelné	akceptovatelný	k2eAgFnPc1d1	akceptovatelná
veřejností	veřejnost	k1gFnSc7	veřejnost
určité	určitý	k2eAgFnPc1d1	určitá
výrazy	výraz	k1gInPc7	výraz
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
seznam	seznam	k1gInSc4	seznam
některých	některý	k3yIgNnPc2	některý
slov	slovo	k1gNnPc2	slovo
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Unacceptable	Unacceptable	k1gFnSc2	Unacceptable
Words	Words	k1gInSc1	Words
on	on	k3xPp3gMnSc1	on
Television	Television	k1gInSc1	Television
and	and	k?	and
Radio	radio	k1gNnSc1	radio
(	(	kIx(	(
<g/>
Neakceptovatelné	akceptovatelný	k2eNgInPc1d1	neakceptovatelný
výrazy	výraz	k1gInPc1	výraz
v	v	k7c6	v
Televizním	televizní	k2eAgNnSc6d1	televizní
a	a	k8xC	a
radiovém	radiový	k2eAgNnSc6d1	radiové
vysílání	vysílání	k1gNnSc6	vysílání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
seznam	seznam	k1gInSc1	seznam
se	se	k3xPyFc4	se
však	však	k9	však
stal	stát	k5eAaPmAgMnS	stát
terčem	terč	k1gInSc7	terč
posměšků	posměšek	k1gInPc2	posměšek
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
textů	text	k1gInPc2	text
přepisuje	přepisovat	k5eAaImIp3nS	přepisovat
diváky	divák	k1gMnPc4	divák
neakpcetovatelné	akpcetovatelný	k2eNgInPc1d1	akpcetovatelný
výrazy	výraz	k1gInPc1	výraz
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
jméno	jméno	k1gNnSc1	jméno
syna	syn	k1gMnSc2	syn
křesťanského	křesťanský	k2eAgMnSc2d1	křesťanský
boha	bůh	k1gMnSc2	bůh
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
technicky	technicky	k6eAd1	technicky
vzato	vzít	k5eAaPmNgNnS	vzít
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
vašeho	váš	k3xOp2gMnSc4	váš
otce	otec	k1gMnSc4	otec
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
zkuste	zkusit	k5eAaPmRp2nP	zkusit
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
nemyslet	myslet	k5eNaImF	myslet
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nadávkami	nadávka	k1gFnPc7	nadávka
se	se	k3xPyFc4	se
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
proslavila	proslavit	k5eAaPmAgFnS	proslavit
starší	starý	k2eAgFnSc1d2	starší
paní	paní	k1gFnSc1	paní
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Julie	Julie	k1gFnSc2	Julie
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
metru	metro	k1gNnSc6	metro
<g/>
.	.	kIx.	.
</s>
<s>
Virální	Virální	k2eAgNnSc1d1	Virální
video	video	k1gNnSc1	video
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
uráží	urážet	k5eAaPmIp3nS	urážet
pasažéry	pasažér	k1gMnPc7	pasažér
a	a	k8xC	a
vynucuje	vynucovat	k5eAaImIp3nS	vynucovat
si	se	k3xPyFc3	se
místo	místo	k1gNnSc4	místo
k	k	k7c3	k
sezení	sezení	k1gNnSc3	sezení
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
hitem	hit	k1gInSc7	hit
internetu	internet	k1gInSc2	internet
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
osobu	osoba	k1gFnSc4	osoba
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
nepřizpůsobivá	přizpůsobivý	k2eNgFnSc1d1	nepřizpůsobivá
a	a	k8xC	a
běžně	běžně	k6eAd1	běžně
terorizuje	terorizovat	k5eAaImIp3nS	terorizovat
sousedy	soused	k1gMnPc4	soused
na	na	k7c6	na
vesnici	vesnice	k1gFnSc6	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Tenistka	tenistka	k1gFnSc1	tenistka
Barbora	Barbora	k1gFnSc1	Barbora
Záhlavová-Strýcová	Záhlavová-Strýcová	k1gFnSc1	Záhlavová-Strýcová
je	být	k5eAaImIp3nS	být
mediálně	mediálně	k6eAd1	mediálně
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
sportovkyně	sportovkyně	k1gFnSc1	sportovkyně
široce	široko	k6eAd1	široko
známá	známý	k2eAgFnSc1d1	známá
vulgárními	vulgární	k2eAgInPc7d1	vulgární
projevy	projev	k1gInPc7	projev
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
finále	finále	k1gNnSc6	finále
turnaje	turnaj	k1gInSc2	turnaj
ECM	ECM	kA	ECM
Prague	Prague	k1gInSc1	Prague
Open	Open	k1gInSc1	Open
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rodačka	rodačka	k1gFnSc1	rodačka
z	z	k7c2	z
Plzně	Plzeň	k1gFnSc2	Plzeň
již	již	k6eAd1	již
neobjeví	objevit	k5eNaPmIp3nS	objevit
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
přenosu	přenos	k1gInSc6	přenos
ČT	ČT	kA	ČT
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Radu	rada	k1gFnSc4	rada
pro	pro	k7c4	pro
televizní	televizní	k2eAgNnSc4d1	televizní
a	a	k8xC	a
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
podána	podat	k5eAaPmNgFnS	podat
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
chování	chování	k1gNnSc4	chování
stížnost	stížnost	k1gFnSc1	stížnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
utkání	utkání	k1gNnSc6	utkání
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
se	se	k3xPyFc4	se
během	během	k7c2	během
utkání	utkání	k1gNnSc2	utkání
"	"	kIx"	"
<g/>
začala	začít	k5eAaPmAgFnS	začít
mlátit	mlátit	k5eAaImF	mlátit
raketou	raketa	k1gFnSc7	raketa
<g/>
...	...	k?	...
nadávala	nadávat	k5eAaImAgFnS	nadávat
jako	jako	k8xC	jako
dlaždič	dlaždič	k1gMnSc1	dlaždič
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Sama	sám	k3xTgFnSc1	sám
sportovkyně	sportovkyně	k1gFnSc1	sportovkyně
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
nadávky	nadávka	k1gFnPc4	nadávka
<g/>
,	,	kIx,	,
vulgární	vulgární	k2eAgNnPc4d1	vulgární
gesta	gesto	k1gNnPc4	gesto
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
nedospělé	dospělý	k2eNgInPc4d1	nedospělý
projevy	projev	k1gInPc4	projev
stydí	stydět	k5eAaImIp3nS	stydět
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
své	svůj	k3xOyFgFnPc4	svůj
vulgarity	vulgarita	k1gFnPc4	vulgarita
jako	jako	k8xC	jako
hluboce	hluboko	k6eAd1	hluboko
zakořeněné	zakořeněný	k2eAgFnPc4d1	zakořeněná
reakce	reakce	k1gFnPc4	reakce
opakuje	opakovat	k5eAaImIp3nS	opakovat
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
komunikace	komunikace	k1gFnSc2	komunikace
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
neodmyslitelně	odmyslitelně	k6eNd1	odmyslitelně
patří	patřit	k5eAaImIp3nS	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Proslavil	proslavit	k5eAaPmAgMnS	proslavit
ji	on	k3xPp3gFnSc4	on
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zvána	zván	k2eAgFnSc1d1	zvána
i	i	k9	i
do	do	k7c2	do
zábavných	zábavný	k2eAgInPc2d1	zábavný
pořadů	pořad	k1gInPc2	pořad
<g/>
.	.	kIx.	.
</s>
<s>
Tenistka	tenistka	k1gFnSc1	tenistka
Serena	Serena	k1gFnSc1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
s	s	k7c7	s
Lucií	Lucie	k1gFnSc7	Lucie
Šafářovou	Šafářův	k2eAgFnSc7d1	Šafářova
ve	v	k7c6	v
French	Fren	k1gFnPc6	Fren
Open	Openo	k1gNnPc2	Openo
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
opakovala	opakovat	k5eAaImAgFnS	opakovat
vulgární	vulgární	k2eAgFnPc4d1	vulgární
nadávky	nadávka	k1gFnPc4	nadávka
i	i	k9	i
přes	přes	k7c4	přes
napomenutí	napomenutí	k1gNnSc4	napomenutí
sudího	sudí	k1gMnSc2	sudí
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
nadávkách	nadávka	k1gFnPc6	nadávka
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
stav	stav	k1gInSc1	stav
utkání	utkání	k1gNnSc2	utkání
nebyl	být	k5eNaImAgInS	být
příznivý	příznivý	k2eAgInSc1d1	příznivý
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
opakovala	opakovat	k5eAaImAgFnS	opakovat
hlasitě	hlasitě	k6eAd1	hlasitě
slovo	slovo	k1gNnSc4	slovo
"	"	kIx"	"
<g/>
fuck	fuck	k6eAd1	fuck
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
mj.	mj.	kA	mj.
kurva	kurva	k1gFnSc1	kurva
<g/>
,	,	kIx,	,
do	do	k7c2	do
prdele	prdel	k1gFnSc2	prdel
<g/>
)	)	kIx)	)
také	také	k9	také
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
televizních	televizní	k2eAgFnPc2d1	televizní
kamer	kamera	k1gFnPc2	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Nadávky	nadávka	k1gFnPc1	nadávka
jakožto	jakožto	k8xS	jakožto
specifické	specifický	k2eAgInPc1d1	specifický
výrazy	výraz	k1gInPc1	výraz
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
informativní	informativní	k2eAgFnSc1d1	informativní
hodnota	hodnota	k1gFnSc1	hodnota
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
není	být	k5eNaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
účinek	účinek	k1gInSc1	účinek
spočívá	spočívat	k5eAaImIp3nS	spočívat
především	především	k9	především
v	v	k7c6	v
pochopení	pochopení	k1gNnSc6	pochopení
a	a	k8xC	a
subjektivní	subjektivní	k2eAgFnSc6d1	subjektivní
empatické	empatický	k2eAgFnSc6d1	empatická
reakci	reakce	k1gFnSc6	reakce
příjemce	příjemce	k1gMnPc4	příjemce
nebo	nebo	k8xC	nebo
posluchače	posluchač	k1gMnPc4	posluchač
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
pro	pro	k7c4	pro
snazší	snadný	k2eAgNnSc4d2	snazší
pochopení	pochopení	k1gNnSc4	pochopení
smyslu	smysl	k1gInSc2	smysl
a	a	k8xC	a
použití	použití	k1gNnSc2	použití
povšechně	povšechně	k6eAd1	povšechně
rozdělit	rozdělit	k5eAaPmF	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
společnosti	společnost	k1gFnSc6	společnost
jsou	být	k5eAaImIp3nP	být
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
nebo	nebo	k8xC	nebo
skupiny	skupina	k1gFnPc1	skupina
osob	osoba	k1gFnPc2	osoba
někdy	někdy	k6eAd1	někdy
pronásledovány	pronásledován	k2eAgFnPc1d1	pronásledována
nebo	nebo	k8xC	nebo
utlačovány	utlačován	k2eAgFnPc1d1	utlačována
například	například	k6eAd1	například
pro	pro	k7c4	pro
věk	věk	k1gInSc4	věk
<g/>
,	,	kIx,	,
oděv	oděv	k1gInSc4	oděv
nebo	nebo	k8xC	nebo
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
etnikum	etnikum	k1gNnSc4	etnikum
nebo	nebo	k8xC	nebo
kulturní	kulturní	k2eAgFnSc4d1	kulturní
odlišnost	odlišnost	k1gFnSc4	odlišnost
<g/>
,	,	kIx,	,
rasu	rasa	k1gFnSc4	rasa
nebo	nebo	k8xC	nebo
barvu	barva	k1gFnSc4	barva
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
náboženství	náboženství	k1gNnSc2	náboženství
nebo	nebo	k8xC	nebo
politickou	politický	k2eAgFnSc4d1	politická
příslušnost	příslušnost	k1gFnSc4	příslušnost
<g/>
,	,	kIx,	,
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
rysů	rys	k1gInPc2	rys
spojených	spojený	k2eAgInPc2d1	spojený
s	s	k7c7	s
pohlavím	pohlaví	k1gNnSc7	pohlaví
(	(	kIx(	(
<g/>
např.	např.	kA	např.
těhotenstvím	těhotenství	k1gNnPc3	těhotenství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sexualitu	sexualita	k1gFnSc4	sexualita
a	a	k8xC	a
sexuální	sexuální	k2eAgFnSc4d1	sexuální
orientaci	orientace	k1gFnSc4	orientace
<g/>
,	,	kIx,	,
nemoc	nemoc	k1gFnSc4	nemoc
nebo	nebo	k8xC	nebo
postižení	postižení	k1gNnSc4	postižení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
duševní	duševní	k2eAgFnPc4d1	duševní
poruchy	porucha	k1gFnPc4	porucha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
nebo	nebo	k8xC	nebo
povolání	povolání	k1gNnSc4	povolání
<g/>
,	,	kIx,	,
společenskou	společenský	k2eAgFnSc4d1	společenská
třídu	třída	k1gFnSc4	třída
(	(	kIx(	(
<g/>
kastu	kasta	k1gFnSc4	kasta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc4	hmotnost
nebo	nebo	k8xC	nebo
výšku	výška	k1gFnSc4	výška
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgMnSc3	ten
částečně	částečně	k6eAd1	částečně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
i	i	k9	i
uvedený	uvedený	k2eAgInSc4d1	uvedený
seznam	seznam	k1gInSc4	seznam
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
typy	typ	k1gInPc4	typ
výrazů	výraz	k1gInPc2	výraz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgInPc1d1	používán
jako	jako	k8xC	jako
nadávky	nadávka	k1gFnPc4	nadávka
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Warren	Warrna	k1gFnPc2	Warrna
Sabean	Sabean	k1gMnSc1	Sabean
nadávky	nadávka	k1gFnSc2	nadávka
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
do	do	k7c2	do
následujících	následující	k2eAgFnPc2d1	následující
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
rouhání	rouhání	k1gNnPc1	rouhání
a	a	k8xC	a
kletby	kletba	k1gFnPc1	kletba
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
pána	pán	k1gMnSc4	pán
boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
čert	čert	k1gMnSc1	čert
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jsi	být	k5eAaImIp2nS	být
a	a	k8xC	a
co	co	k3yRnSc1	co
jsi	být	k5eAaImIp2nS	být
<g/>
)	)	kIx)	)
neslušnosti	neslušnost	k1gFnSc3	neslušnost
a	a	k8xC	a
necudnosti	necudnost	k1gFnSc3	necudnost
(	(	kIx(	(
<g/>
kurva	kurva	k1gFnSc1	kurva
<g/>
)	)	kIx)	)
urážky	urážka	k1gFnPc1	urážka
a	a	k8xC	a
znectění	znectění	k1gNnSc1	znectění
(	(	kIx(	(
<g/>
lhář	lhář	k1gMnSc1	lhář
<g/>
,	,	kIx,	,
lotr	lotr	k1gMnSc1	lotr
<g/>
,	,	kIx,	,
zloděj	zloděj	k1gMnSc1	zloděj
<g/>
)	)	kIx)	)
nečistota	nečistota	k1gFnSc1	nečistota
a	a	k8xC	a
skatologie	skatologie	k1gFnSc1	skatologie
Podrobnější	podrobný	k2eAgFnSc4d2	podrobnější
typologii	typologie	k1gFnSc4	typologie
nadávek	nadávka	k1gFnPc2	nadávka
použil	použít	k5eAaPmAgMnS	použít
Michael	Michael	k1gMnSc1	Michael
Frank	Frank	k1gMnSc1	Frank
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
kriminality	kriminalita	k1gFnSc2	kriminalita
ve	v	k7c6	v
vesnické	vesnický	k2eAgFnSc6d1	vesnická
společnosti	společnost	k1gFnSc6	společnost
sedmnáctého	sedmnáctý	k4xOgMnSc4	sedmnáctý
a	a	k8xC	a
osmnáctého	osmnáctý	k4xOgMnSc4	osmnáctý
století	století	k1gNnSc2	století
<g/>
:	:	kIx,	:
sexuální	sexuální	k2eAgFnSc2d1	sexuální
<g/>
,	,	kIx,	,
přirovnávající	přirovnávající	k2eAgFnSc2d1	přirovnávající
ke	k	k7c3	k
zvířatům	zvíře	k1gNnPc3	zvíře
zlodějské	zlodějský	k2eAgFnPc1d1	zlodějská
označující	označující	k2eAgMnSc1d1	označující
za	za	k7c4	za
nespolehlivého	spolehlivý	k2eNgMnSc4d1	nespolehlivý
člověka	člověk	k1gMnSc4	člověk
šelmovské	šelmovský	k2eAgFnSc2d1	šelmovský
čarodějnické	čarodějnický	k2eAgFnSc2d1	čarodějnická
národnostní	národnostní	k2eAgFnSc2d1	národnostní
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Toch	Toch	k1gMnSc1	Toch
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
o	o	k7c6	o
slovních	slovní	k2eAgFnPc6d1	slovní
urážkách	urážka	k1gFnPc6	urážka
v	v	k7c6	v
pozdně	pozdně	k6eAd1	pozdně
středověké	středověký	k2eAgFnSc6d1	středověká
vesnické	vesnický	k2eAgFnSc6d1	vesnická
společnosti	společnost	k1gFnSc6	společnost
použil	použít	k5eAaPmAgMnS	použít
následující	následující	k2eAgFnSc3d1	následující
typologii	typologie	k1gFnSc3	typologie
kriminalizující	kriminalizující	k2eAgFnSc3d1	kriminalizující
náboženské	náboženský	k2eAgNnSc4d1	náboženské
dle	dle	k7c2	dle
povolání	povolání	k1gNnSc2	povolání
anální	anální	k2eAgInSc1d1	anální
sexuální	sexuální	k2eAgInSc1d1	sexuální
dle	dle	k7c2	dle
osobních	osobní	k2eAgFnPc2d1	osobní
vlastností	vlastnost	k1gFnPc2	vlastnost
zvířecí	zvířecí	k2eAgFnSc1d1	zvířecí
V	v	k7c6	v
diplomové	diplomový	k2eAgFnSc6d1	Diplomová
práci	práce	k1gFnSc6	práce
Urážlivé	urážlivý	k2eAgFnSc6d1	urážlivá
<g/>
,	,	kIx,	,
vulgární	vulgární	k2eAgFnSc4d1	vulgární
a	a	k8xC	a
rasistické	rasistický	k2eAgInPc4d1	rasistický
projevy	projev	k1gInPc4	projev
na	na	k7c6	na
internetových	internetový	k2eAgInPc6d1	internetový
diskusních	diskusní	k2eAgInPc6d1	diskusní
fórech	fór	k1gInPc6	fór
<g/>
,	,	kIx,	,
autorka	autorka	k1gFnSc1	autorka
více	hodně	k6eAd2	hodně
než	než	k8xS	než
pět	pět	k4xCc4	pět
set	sto	k4xCgNnPc2	sto
výrazů	výraz	k1gInPc2	výraz
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
dvaceti	dvacet	k4xCc2	dvacet
<g />
.	.	kIx.	.
</s>
<s>
kategorií	kategorie	k1gFnPc2	kategorie
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
podkategorií	podkategorie	k1gFnPc2	podkategorie
<g/>
.	.	kIx.	.
kontextově	kontextově	k6eAd1	kontextově
závislé	závislý	k2eAgInPc1d1	závislý
výroky	výrok	k1gInPc1	výrok
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
efekt	efekt	k1gInSc1	efekt
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
v	v	k7c6	v
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
autorka	autorka	k1gFnSc1	autorka
řadí	řadit	k5eAaImIp3nS	řadit
cynické	cynický	k2eAgFnPc4d1	cynická
poznámky	poznámka	k1gFnPc4	poznámka
<g/>
,	,	kIx,	,
výrazy	výraz	k1gInPc4	výraz
se	s	k7c7	s
sexuální	sexuální	k2eAgFnSc7d1	sexuální
tematikou	tematika	k1gFnSc7	tematika
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dále	daleko	k6eAd2	daleko
člení	členit	k5eAaImIp3nP	členit
výrazy	výraz	k1gInPc1	výraz
urážející	urážející	k2eAgFnSc4d1	urážející
inteligenci	inteligence	k1gFnSc4	inteligence
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
výrazy	výraz	k1gInPc4	výraz
urážející	urážející	k2eAgNnSc4d1	urážející
náboženství	náboženství	k1gNnSc4	náboženství
výrazy	výraz	k1gInPc1	výraz
urážející	urážející	k2eAgInPc1d1	urážející
vzhled	vzhled	k1gInSc4	vzhled
výrazy	výraz	k1gInPc4	výraz
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
urážet	urážet	k5eAaPmF	urážet
člověka	člověk	k1gMnSc4	člověk
pro	pro	k7c4	pro
jeho	on	k3xPp3gInSc4	on
věk	věk	k1gInSc4	věk
výrazy	výraz	k1gInPc4	výraz
vztahující	vztahující	k2eAgInPc4d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
povaze	povaha	k1gFnSc3	povaha
člověka	člověk	k1gMnSc2	člověk
výrazy	výraz	k1gInPc7	výraz
pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
závislé	závislý	k2eAgFnSc2d1	závislá
na	na	k7c6	na
návykových	návykový	k2eAgFnPc6d1	návyková
látkách	látka	k1gFnPc6	látka
výrazy	výraz	k1gInPc4	výraz
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
označují	označovat	k5eAaImIp3nP	označovat
hloupost	hloupost	k1gFnSc4	hloupost
výrazy	výraz	k1gInPc1	výraz
pojmenovávající	pojmenovávající	k2eAgInPc1d1	pojmenovávající
obličej	obličej	k1gInSc4	obličej
výrazy	výraz	k1gInPc7	výraz
označující	označující	k2eAgFnSc2d1	označující
podřadné	podřadný	k2eAgFnSc2d1	podřadná
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
výrazy	výraz	k1gInPc7	výraz
degradující	degradující	k2eAgFnSc1d1	degradující
nějakou	nějaký	k3yIgFnSc4	nějaký
skupinu	skupina	k1gFnSc4	skupina
<g/>
)	)	kIx)	)
výrazy	výraz	k1gInPc7	výraz
pro	pro	k7c4	pro
nemanželské	manželský	k2eNgFnPc4d1	nemanželská
<g/>
,	,	kIx,	,
zlobivé	zlobivý	k2eAgFnPc4d1	zlobivá
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
<g />
.	.	kIx.	.
</s>
<s>
s	s	k7c7	s
negativním	negativní	k2eAgInSc7d1	negativní
podtónem	podtón	k1gInSc7	podtón
politické	politický	k2eAgInPc4d1	politický
výrazy	výraz	k1gInPc4	výraz
výrazy	výraz	k1gInPc4	výraz
označující	označující	k2eAgFnSc4d1	označující
národnost	národnost	k1gFnSc4	národnost
člověka	člověk	k1gMnSc2	člověk
xenofobní	xenofobní	k2eAgInPc1d1	xenofobní
výrazy	výraz	k1gInPc1	výraz
nejčastější	častý	k2eAgInPc1d3	nejčastější
výrazy	výraz	k1gInPc1	výraz
přízviska	přízvisko	k1gNnSc2	přízvisko
známých	známý	k2eAgFnPc2d1	známá
osob	osoba	k1gFnPc2	osoba
výrazy	výraz	k1gInPc1	výraz
představující	představující	k2eAgFnSc4d1	představující
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
hru	hra	k1gFnSc4	hra
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
známých	známý	k2eAgFnPc2d1	známá
osob	osoba	k1gFnPc2	osoba
grafické	grafický	k2eAgFnSc2d1	grafická
značky	značka	k1gFnSc2	značka
(	(	kIx(	(
<g/>
piktogramy	piktogram	k1gInPc1	piktogram
<g/>
)	)	kIx)	)
výrazy	výraz	k1gInPc1	výraz
vytvořené	vytvořený	k2eAgFnSc2d1	vytvořená
kvůli	kvůli	k7c3	kvůli
blokovacím	blokovací	k2eAgInPc3d1	blokovací
automatům	automat	k1gInPc3	automat
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
uvedených	uvedený	k2eAgNnPc2d1	uvedené
rozdělení	rozdělení	k1gNnSc2	rozdělení
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nadávky	nadávka	k1gFnPc4	nadávka
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
jako	jako	k8xS	jako
<g />
.	.	kIx.	.
</s>
<s>
výrazy	výraz	k1gInPc1	výraz
související	související	k2eAgInPc1d1	související
především	především	k6eAd1	především
jazykovým	jazykový	k2eAgInSc7d1	jazykový
původem	původ	k1gInSc7	původ
<g/>
:	:	kIx,	:
nadávky	nadávka	k1gFnPc1	nadávka
přímo	přímo	k6eAd1	přímo
převzaté	převzatý	k2eAgInPc1d1	převzatý
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
gadžo	gadža	k1gMnSc5	gadža
<g/>
,	,	kIx,	,
ďaur	ďaur	k1gMnSc1	ďaur
<g/>
,	,	kIx,	,
shit	shit	k1gMnSc1	shit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
tvary	tvar	k1gInPc1	tvar
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
foneticky	foneticky	k6eAd1	foneticky
přepsané	přepsaný	k2eAgInPc1d1	přepsaný
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
(	(	kIx(	(
<g/>
šit	šit	k1gInSc4	šit
<g/>
,	,	kIx,	,
bulšit	bulšit	k5eAaPmF	bulšit
<g/>
)	)	kIx)	)
ludra	ludra	k?	ludra
(	(	kIx(	(
<g/>
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
<g/>
)	)	kIx)	)
novotvary	novotvar	k1gInPc1	novotvar
<g />
.	.	kIx.	.
</s>
<s>
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
kombinací	kombinace	k1gFnSc7	kombinace
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
úpravou	úprava	k1gFnSc7	úprava
tradiční	tradiční	k2eAgFnSc2d1	tradiční
nadávky	nadávka	k1gFnSc2	nadávka
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
výrazem	výraz	k1gInSc7	výraz
–	–	k?	–
vlezdobruselista	vlezdobruselista	k1gMnSc1	vlezdobruselista
<g/>
,	,	kIx,	,
poštěvačka	poštěvačka	k1gFnSc1	poštěvačka
<g/>
,	,	kIx,	,
serebrita	serebrita	k1gFnSc1	serebrita
české	český	k2eAgFnSc2d1	Česká
nadávky	nadávka	k1gFnSc2	nadávka
a	a	k8xC	a
české	český	k2eAgInPc1d1	český
výrazy	výraz	k1gInPc1	výraz
s	s	k7c7	s
významovým	významový	k2eAgInSc7d1	významový
posunem	posun	k1gInSc7	posun
-	-	kIx~	-
děvka	děvka	k1gFnSc1	děvka
<g/>
,	,	kIx,	,
mrzák	mrzák	k1gMnSc1	mrzák
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
puchýř	puchýř	k1gInSc1	puchýř
<g/>
,	,	kIx,	,
nežit	nežit	k1gInSc1	nežit
nebo	nebo	k8xC	nebo
rozmrzení	rozmrzení	k1gNnSc2	rozmrzení
působící	působící	k2eAgFnSc1d1	působící
osoba	osoba	k1gFnSc1	osoba
<g/>
)	)	kIx)	)
Výrazy	výraz	k1gInPc1	výraz
vytvořené	vytvořený	k2eAgInPc1d1	vytvořený
jako	jako	k8xS	jako
<g />
.	.	kIx.	.
</s>
<s>
metafory	metafora	k1gFnPc1	metafora
<g/>
:	:	kIx,	:
označení	označení	k1gNnSc1	označení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
sexuálním	sexuální	k2eAgNnSc7d1	sexuální
chováním	chování	k1gNnSc7	chování
nebo	nebo	k8xC	nebo
které	který	k3yRgFnPc1	který
přímo	přímo	k6eAd1	přímo
nebo	nebo	k8xC	nebo
nepřímo	přímo	k6eNd1	přímo
označují	označovat	k5eAaImIp3nP	označovat
genitálie	genitálie	k1gFnPc1	genitálie
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
výrazy	výraz	k1gInPc1	výraz
-	-	kIx~	-
čurák	čurák	k1gMnSc1	čurák
<g/>
,	,	kIx,	,
píča	píča	k1gMnSc1	píča
<g/>
,	,	kIx,	,
kunda	kunda	k1gMnSc1	kunda
<g/>
,	,	kIx,	,
kurva	kurva	k1gFnSc1	kurva
výrazy	výraz	k1gInPc1	výraz
související	související	k2eAgMnSc1d1	související
s	s	k7c7	s
vylučováním	vylučování	k1gNnSc7	vylučování
<g/>
,	,	kIx,	,
skatologií	skatologie	k1gFnSc7	skatologie
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jsou	být	k5eAaImIp3nP	být
historickými	historický	k2eAgInPc7d1	historický
nebo	nebo	k8xC	nebo
neslušnými	slušný	k2eNgInPc7d1	neslušný
názvy	název	k1gInPc7	název
pro	pro	k7c4	pro
toaletu	toaleta	k1gFnSc4	toaleta
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
hovno	hovno	k1gNnSc1	hovno
<g/>
,	,	kIx,	,
prd	prd	k1gInSc1	prd
<g/>
,	,	kIx,	,
prevít	prevít	k?	prevít
<g/>
,	,	kIx,	,
hajzl	hajzl	k1gInSc1	hajzl
<g/>
,	,	kIx,	,
prdel	prdel	k1gFnSc1	prdel
označení	označení	k1gNnSc2	označení
přirovnávající	přirovnávající	k2eAgFnSc2d1	přirovnávající
k	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
zvířatům	zvíře	k1gNnPc3	zvíře
–	–	k?	–
skutečným	skutečný	k2eAgInPc3d1	skutečný
či	či	k8xC	či
smyšleným	smyšlený	k2eAgInPc3d1	smyšlený
-	-	kIx~	-
vůl	vůl	k1gMnSc1	vůl
<g/>
,	,	kIx,	,
čuně	čuně	k?	čuně
<g/>
,	,	kIx,	,
prasopes	prasopes	k1gInSc1	prasopes
<g/>
,	,	kIx,	,
vochechule	vochechule	k1gFnSc1	vochechule
<g/>
)	)	kIx)	)
označení	označení	k1gNnSc1	označení
podle	podle	k7c2	podle
odumřelých	odumřelý	k2eAgFnPc2d1	odumřelá
částí	část	k1gFnPc2	část
rostlin	rostlina	k1gFnPc2	rostlina
–	–	k?	–
pařez	pařez	k1gMnSc1	pařez
<g/>
,	,	kIx,	,
stará	starý	k2eAgFnSc1d1	stará
větev	větev	k1gFnSc1	větev
<g/>
,	,	kIx,	,
kořen	kořen	k1gInSc1	kořen
<g />
.	.	kIx.	.
</s>
<s>
označení	označení	k1gNnSc1	označení
mající	mající	k2eAgMnSc1d1	mající
původ	původ	k1gMnSc1	původ
ve	v	k7c6	v
vlastnostech	vlastnost	k1gFnPc6	vlastnost
nějaké	nějaký	k3yIgFnSc2	nějaký
historické	historický	k2eAgFnSc2d1	historická
osoby	osoba	k1gFnSc2	osoba
–	–	k?	–
grázl	grázl	k1gMnSc1	grázl
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Johanna	Johann	k1gMnSc2	Johann
Georga	Georg	k1gMnSc2	Georg
Grasela	Grasel	k1gMnSc2	Grasel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
označení	označení	k1gNnSc4	označení
mající	mající	k2eAgMnSc1d1	mající
původ	původ	k1gMnSc1	původ
v	v	k7c6	v
zaměstnání	zaměstnání	k1gNnSc6	zaměstnání
-	-	kIx~	-
fízl	fízl	k?	fízl
<g/>
,	,	kIx,	,
estébák	estébák	k?	estébák
<g/>
,	,	kIx,	,
gestapák	gestapák	k?	gestapák
<g/>
,	,	kIx,	,
popelář	popelář	k1gMnSc1	popelář
<g/>
,	,	kIx,	,
hérečka	hérečka	k1gFnSc1	hérečka
označení	označení	k1gNnSc2	označení
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
nadávkami	nadávka	k1gFnPc7	nadávka
jen	jen	k6eAd1	jen
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
nebo	nebo	k8xC	nebo
při	při	k7c6	při
<g />
.	.	kIx.	.
</s>
<s>
přirovnání	přirovnání	k1gNnSc1	přirovnání
-	-	kIx~	-
trouba	trouba	k1gFnSc1	trouba
<g/>
,	,	kIx,	,
trubka	trubka	k1gFnSc1	trubka
<g/>
,	,	kIx,	,
vrták	vrták	k1gMnSc1	vrták
<g/>
,	,	kIx,	,
truhlík	truhlík	k1gMnSc1	truhlík
<g/>
,	,	kIx,	,
matěj	matěj	k?	matěj
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgInPc1d1	náboženský
výrazy	výraz	k1gInPc1	výraz
nebo	nebo	k8xC	nebo
osoby	osoba	k1gFnPc1	osoba
–	–	k?	–
boží	božit	k5eAaImIp3nP	božit
hovádko	hovádko	k1gNnSc1	hovádko
<g/>
,	,	kIx,	,
proklatec	proklatec	k1gMnSc1	proklatec
<g/>
,	,	kIx,	,
Jidáš	Jidáš	k1gMnSc1	Jidáš
<g/>
,	,	kIx,	,
Kain	Kain	k1gMnSc1	Kain
Výrazy	výraz	k1gInPc4	výraz
popisující	popisující	k2eAgInPc4d1	popisující
v	v	k7c6	v
negativním	negativní	k2eAgInSc6d1	negativní
významu	význam	k1gInSc6	význam
nebo	nebo	k8xC	nebo
i	i	k9	i
nepravdivě	pravdivě	k6eNd1	pravdivě
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
nadsázky	nadsázka	k1gFnPc4	nadsázka
<g/>
:	:	kIx,	:
označení	označení	k1gNnSc1	označení
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
které	který	k3yIgNnSc1	který
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
nedostatky	nedostatek	k1gInPc4	nedostatek
vzhledu	vzhled	k1gInSc2	vzhled
(	(	kIx(	(
<g/>
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
k	k	k7c3	k
obecně	obecně	k6eAd1	obecně
uznávaným	uznávaný	k2eAgFnPc3d1	uznávaná
normám	norma	k1gFnPc3	norma
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
výraznými	výrazný	k2eAgInPc7d1	výrazný
tělesnými	tělesný	k2eAgInPc7d1	tělesný
znaky	znak	k1gInPc7	znak
a	a	k8xC	a
schopnostmi	schopnost	k1gFnPc7	schopnost
–	–	k?	–
nosáč	nosáč	k1gMnSc1	nosáč
<g/>
,	,	kIx,	,
anorektička	anorektička	k1gFnSc1	anorektička
<g/>
,	,	kIx,	,
buřtík	buřtík	k1gInSc1	buřtík
<g/>
,	,	kIx,	,
špekoun	špekoun	k1gMnSc1	špekoun
označení	označení	k1gNnSc2	označení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
nedostatečným	dostatečný	k2eNgNnSc7d1	nedostatečné
vzděláním	vzdělání	k1gNnSc7	vzdělání
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
sociálním	sociální	k2eAgNnSc7d1	sociální
postavením	postavení	k1gNnSc7	postavení
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
negramot	negramota	k1gFnPc2	negramota
<g/>
,	,	kIx,	,
nula	nula	k1gFnSc1	nula
<g/>
,	,	kIx,	,
strup	strup	k1gMnSc1	strup
<g/>
,	,	kIx,	,
vidlák	vidlák	k1gMnSc1	vidlák
<g/>
,	,	kIx,	,
maloměšťák	maloměšťák	k1gMnSc1	maloměšťák
označení	označení	k1gNnSc2	označení
urážlivým	urážlivý	k2eAgInSc7d1	urážlivý
způsobem	způsob	k1gInSc7	způsob
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
genderovou	genderův	k2eAgFnSc7d1	genderův
orientací	orientace	k1gFnSc7	orientace
<g/>
,	,	kIx,	,
pohlavím	pohlaví	k1gNnSc7	pohlaví
nebo	nebo	k8xC	nebo
s	s	k7c7	s
takovou	takový	k3xDgFnSc7	takový
orientací	orientace	k1gFnSc7	orientace
spojeným	spojený	k2eAgNnSc7d1	spojené
chováním	chování	k1gNnSc7	chování
–	–	k?	–
teplouš	teplouš	k1gMnSc1	teplouš
<g/>
,	,	kIx,	,
buzna	buzna	k1gFnSc1	buzna
<g/>
,	,	kIx,	,
bukvice	bukvice	k1gFnSc1	bukvice
označení	označení	k1gNnSc2	označení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
<g/>
,	,	kIx,	,
mládím	mládí	k1gNnSc7	mládí
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
<g />
.	.	kIx.	.
</s>
<s>
stárnutím	stárnutí	k1gNnSc7	stárnutí
a	a	k8xC	a
znaky	znak	k1gInPc1	znak
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
spojenými	spojený	k2eAgMnPc7d1	spojený
–	–	k?	–
mimino	mimino	k1gNnSc1	mimino
<g/>
,	,	kIx,	,
beďarová	beďarová	k1gFnSc1	beďarová
opice	opice	k1gFnSc2	opice
<g/>
,	,	kIx,	,
senil	senil	k1gInSc4	senil
rasistická	rasistický	k2eAgNnPc4d1	rasistické
označení	označení	k1gNnSc4	označení
–	–	k?	–
cigán	cigán	k1gMnSc1	cigán
<g/>
,	,	kIx,	,
negr	negr	k1gMnSc1	negr
<g/>
,	,	kIx,	,
černá	černý	k2eAgFnSc1d1	černá
huba	huba	k1gFnSc1	huba
rákosník	rákosník	k1gInSc1	rákosník
šovinistické	šovinistický	k2eAgInPc1d1	šovinistický
výrazy	výraz	k1gInPc1	výraz
nebo	nebo	k8xC	nebo
výrazy	výraz	k1gInPc1	výraz
přirovnávající	přirovnávající	k2eAgFnSc2d1	přirovnávající
k	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
cizím	cizí	k2eAgInPc3d1	cizí
národům	národ	k1gInPc3	národ
–	–	k?	–
židák	židák	k?	židák
<g/>
,	,	kIx,	,
tatar	tatar	k1gMnSc1	tatar
<g/>
,	,	kIx,	,
žabožrout	žabožrout	k1gMnSc1	žabožrout
(	(	kIx(	(
<g/>
Francouz	Francouz	k1gMnSc1	Francouz
<g/>
)	)	kIx)	)
výrazy	výraz	k1gInPc1	výraz
<g />
.	.	kIx.	.
</s>
<s>
označující	označující	k2eAgFnPc1d1	označující
negativní	negativní	k2eAgFnPc1d1	negativní
charakterové	charakterový	k2eAgFnPc1d1	charakterová
vlastnosti	vlastnost	k1gFnPc1	vlastnost
–	–	k?	–
nádiva	nádiva	k1gMnSc1	nádiva
<g/>
,	,	kIx,	,
famfárum	famfárum	k1gNnSc1	famfárum
<g/>
,	,	kIx,	,
flákač	flákač	k1gMnSc1	flákač
<g/>
,	,	kIx,	,
floutek	floutek	k1gMnSc1	floutek
urážlivé	urážlivý	k2eAgNnSc4d1	urážlivé
označení	označení	k1gNnSc4	označení
druhých	druhý	k4xOgMnPc2	druhý
jako	jako	k8xS	jako
zastánců	zastánce	k1gMnPc2	zastánce
nějakých	nějaký	k3yIgInPc2	nějaký
politických	politický	k2eAgInPc2d1	politický
a	a	k8xC	a
filosofických	filosofický	k2eAgInPc2d1	filosofický
názorů	názor	k1gInPc2	názor
–	–	k?	–
nácek	nácek	k1gMnSc1	nácek
<g/>
,	,	kIx,	,
komouš	komouš	k1gMnSc1	komouš
<g/>
,	,	kIx,	,
bolševik	bolševik	k1gMnSc1	bolševik
<g/>
,	,	kIx,	,
fašoun	fašoun	k1gMnSc1	fašoun
<g/>
,	,	kIx,	,
socan	socan	k1gMnSc1	socan
výrazy	výraz	k1gInPc7	výraz
pro	pro	k7c4	pro
fyzické	fyzický	k2eAgNnSc4d1	fyzické
nebo	nebo	k8xC	nebo
psychické	psychický	k2eAgNnSc4d1	psychické
postižení	postižení	k1gNnSc4	postižení
-	-	kIx~	-
idiot	idiot	k1gMnSc1	idiot
<g/>
,	,	kIx,	,
debil	debil	k1gMnSc1	debil
<g/>
,	,	kIx,	,
kretén	kretén	k1gMnSc1	kretén
<g/>
,	,	kIx,	,
psychopat	psychopat	k1gMnSc1	psychopat
<g/>
,	,	kIx,	,
pitomec	pitomec	k1gMnSc1	pitomec
Tento	tento	k3xDgInSc4	tento
seznam	seznam	k1gInSc4	seznam
přímo	přímo	k6eAd1	přímo
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
seznamu	seznam	k1gInSc2	seznam
použitého	použitý	k2eAgInSc2d1	použitý
Radou	rada	k1gFnSc7	rada
pro	pro	k7c4	pro
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
a	a	k8xC	a
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výzkumů	výzkum	k1gInPc2	výzkum
prováděných	prováděný	k2eAgInPc2d1	prováděný
Angus	Angus	k1gInSc4	Angus
Reid	Reid	k1gInSc4	Reid
Public	publicum	k1gNnPc2	publicum
Opinion	Opinion	k1gInSc4	Opinion
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kanaďané	Kanaďan	k1gMnPc1	Kanaďan
nadávají	nadávat	k5eAaImIp3nP	nadávat
častěji	často	k6eAd2	často
než	než	k8xS	než
Američané	Američan	k1gMnPc1	Američan
a	a	k8xC	a
Britové	Brit	k1gMnPc1	Brit
<g/>
,	,	kIx,	,
když	když	k8xS	když
mluví	mluvit	k5eAaImIp3nS	mluvit
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Britové	Brit	k1gMnPc1	Brit
nadávají	nadávat	k5eAaImIp3nP	nadávat
cizím	cizí	k2eAgInSc7d1	cizí
jazykem	jazyk	k1gInSc7	jazyk
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
než	než	k8xS	než
Kanaďané	Kanaďan	k1gMnPc1	Kanaďan
a	a	k8xC	a
Američané	Američan	k1gMnPc1	Američan
během	během	k7c2	během
rozhovoru	rozhovor	k1gInSc2	rozhovor
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
použil	použít	k5eAaPmAgInS	použít
poprvé	poprvé	k6eAd1	poprvé
místo	místo	k7c2	místo
oštěpu	oštěp	k1gInSc2	oštěp
nadávky	nadávka	k1gFnSc2	nadávka
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
zakladatelem	zakladatel	k1gMnSc7	zakladatel
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
<g/>
)	)	kIx)	)
Když	když	k8xS	když
jste	být	k5eAaImIp2nP	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
s	s	k7c7	s
argumenty	argument	k1gInPc7	argument
<g/>
,	,	kIx,	,
můžete	moct	k5eAaImIp2nP	moct
ještě	ještě	k6eAd1	ještě
začít	začít	k5eAaPmF	začít
nadávat	nadávat	k5eAaImF	nadávat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Elbert	Elbert	k1gMnSc1	Elbert
Hubbard	Hubbard	k1gMnSc1	Hubbard
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Lidstvo	lidstvo	k1gNnSc1	lidstvo
si	se	k3xPyFc3	se
zasluhuje	zasluhovat	k5eAaImIp3nS	zasluhovat
spíše	spíše	k9	spíše
obdivu	obdiv	k1gInSc2	obdiv
<g/>
,	,	kIx,	,
ba	ba	k9	ba
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
nežli	nežli	k8xS	nežli
nadávek	nadávka	k1gFnPc2	nadávka
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
když	když	k8xS	když
nadávek	nadávka	k1gFnPc2	nadávka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
hodně	hodně	k6eAd1	hodně
upřímných	upřímný	k2eAgFnPc2d1	upřímná
a	a	k8xC	a
hlasitých	hlasitý	k2eAgFnPc2d1	hlasitá
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
)	)	kIx)	)
Sprosté	sprostý	k2eAgNnSc1d1	sprosté
slovo	slovo	k1gNnSc1	slovo
Urážka	urážka	k1gFnSc1	urážka
Pomluva	pomluva	k1gFnSc1	pomluva
Kletba	kletba	k1gFnSc1	kletba
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
nadávka	nadávka	k1gFnSc1	nadávka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
