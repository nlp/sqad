<s>
Daleský	Daleský	k2eAgMnSc1d1
pony	pony	k1gMnSc1
</s>
<s>
Daleský	Daleský	k2eAgMnSc1d1
pony	pony	k1gMnSc1
je	být	k5eAaImIp3nS
plemeno	plemeno	k1gNnSc1
koně	kůň	k1gMnSc2
<g/>
,	,	kIx,
silného	silný	k2eAgMnSc2d1
horského	horský	k2eAgMnSc2d1
poníka	poník	k1gMnSc2
z	z	k7c2
britských	britský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
</s>
<s>
Je	být	k5eAaImIp3nS
blízce	blízce	k6eAd1
příbuzný	příbuzný	k1gMnSc1
fellskému	fellský	k2eAgMnSc3d1
a	a	k8xC
rovněž	rovněž	k9
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
keltského	keltský	k2eAgMnSc2d1
a	a	k8xC
gallowayského	gallowayský	k2eAgMnSc2d1
ponyho	pony	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obývá	obývat	k5eAaImIp3nS
východní	východní	k2eAgInPc1d1
svahy	svah	k1gInPc1
Pennin	Pennina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Podobně	podobně	k6eAd1
jako	jako	k9
fellský	fellský	k2eAgMnSc1d1
pony	pony	k1gMnSc1
je	být	k5eAaImIp3nS
nesmírně	smírně	k6eNd1
výkonný	výkonný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Býval	bývat	k5eAaImAgInS
běžně	běžně	k6eAd1
používán	používat	k5eAaImNgInS
k	k	k7c3
dopravě	doprava	k1gFnSc3
olova	olovo	k1gNnSc2
z	z	k7c2
dolů	dol	k1gInPc2
do	do	k7c2
přístavů	přístav	k1gInPc2
a	a	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
všestranný	všestranný	k2eAgMnSc1d1
nákladní	nákladní	k2eAgMnSc1d1
kůň	kůň	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
utáhne	utáhnout	k5eAaPmIp3nS
těžké	těžký	k2eAgInPc4d1
náklady	náklad	k1gInPc4
<g/>
,	,	kIx,
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
ho	on	k3xPp3gMnSc4
s	s	k7c7
oblibou	obliba	k1gFnSc7
sedláci	sedlák	k1gMnPc1
pro	pro	k7c4
nejrůznější	různý	k2eAgFnPc4d3
těžké	těžký	k2eAgFnPc4d1
zemědělské	zemědělský	k2eAgFnPc4d1
práce	práce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
vhodný	vhodný	k2eAgInSc1d1
i	i	k9
pod	pod	k7c4
sedlo	sedlo	k1gNnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
má	mít	k5eAaImIp3nS
jistý	jistý	k2eAgInSc4d1
a	a	k8xC
živý	živý	k2eAgInSc4d1
krok	krok	k1gInSc4
a	a	k8xC
rychlý	rychlý	k2eAgInSc4d1
klus	klus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byly	být	k5eAaImAgFnP
daleské	daleský	k2eAgFnPc1d1
klisny	klisna	k1gFnPc1
kříženy	křížen	k2eAgFnPc1d1
s	s	k7c7
velšským	velšský	k2eAgInSc7d1
kobem	kob	k1gInSc7
a	a	k8xC
s	s	k7c7
Cometem	Comet	k1gMnSc7
<g/>
,	,	kIx,
šampiónem	šampión	k1gMnSc7
místních	místní	k2eAgInPc2d1
klusáckých	klusácký	k2eAgInPc2d1
dostihů	dostih	k1gInPc2
<g/>
,	,	kIx,
ve	v	k7c6
snaze	snaha	k1gFnSc6
zvýšit	zvýšit	k5eAaPmF
rychlost	rychlost	k1gFnSc4
plemene	plemeno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgMnPc4
daleské	daleský	k2eAgMnPc4d1
poníky	poník	k1gMnPc4
lze	lze	k6eAd1
k	k	k7c3
tomuto	tento	k3xDgNnSc3
hřebci	hřebec	k1gMnPc1
vystopovat	vystopovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
rozvojem	rozvoj	k1gInSc7
motorizované	motorizovaný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
klesly	klesnout	k5eAaPmAgInP
počty	počet	k1gInPc1
těchto	tento	k3xDgMnPc2
koní	kůň	k1gMnPc2
a	a	k8xC
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
bylo	být	k5eAaImAgNnS
plemeno	plemeno	k1gNnSc1
téměř	téměř	k6eAd1
vyhubeno	vyhubit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
ustanovením	ustanovení	k1gNnSc7
Dales	dales	k1gInSc4
Pony	pony	k1gMnSc1
Society	societa	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1963	#num#	k4
však	však	k9
zájem	zájem	k1gInSc1
o	o	k7c4
plemeno	plemeno	k1gNnSc4
i	i	k8xC
jeho	jeho	k3xOp3gInPc1
početní	početní	k2eAgInPc1d1
stavy	stav	k1gInPc1
opět	opět	k6eAd1
stouply	stoupnout	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
ideální	ideální	k2eAgMnSc1d1
pony	pony	k1gMnSc1
pro	pro	k7c4
trekking	trekking	k1gInSc4
(	(	kIx(
<g/>
jízdu	jízda	k1gFnSc4
v	v	k7c6
zápřeži	zápřež	k1gFnSc6
<g/>
)	)	kIx)
<g/>
;	;	kIx,
je	být	k5eAaImIp3nS
vhodný	vhodný	k2eAgInSc1d1
i	i	k9
pro	pro	k7c4
rekreační	rekreační	k2eAgNnPc4d1
ježdění	ježdění	k1gNnPc4
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
je	být	k5eAaImIp3nS
vzhledem	vzhledem	k7c3
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
síle	síla	k1gFnSc3
vhodnější	vhodný	k2eAgFnSc1d2
pro	pro	k7c4
dospělé	dospělí	k1gMnPc4
než	než	k8xS
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přikřížením	přikřížení	k1gNnSc7
anglického	anglický	k2eAgMnSc2d1
plnokrevníka	plnokrevník	k1gMnSc2
dává	dávat	k5eAaImIp3nS
daleský	daleský	k2eAgMnSc1d1
pony	pony	k1gMnSc1
vynikající	vynikající	k2eAgInSc1d1
skokany	skokan	k1gMnPc7
a	a	k8xC
huntery	hunter	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbarvení	zbarvení	k1gNnSc1
<g/>
:	:	kIx,
černá	černý	k2eAgFnSc1d1
nebo	nebo	k8xC
hnědá	hnědý	k2eAgFnSc1d1
<g/>
,	,	kIx,
povolena	povolen	k2eAgFnSc1d1
bílá	bílý	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Výška	výška	k1gFnSc1
<g/>
:	:	kIx,
<g/>
13,2	13,2	k4
<g/>
-	-	kIx~
<g/>
14,2	14,2	k4
<g/>
p.	p.	k?
<g/>
(	(	kIx(
<g/>
134	#num#	k4
<g/>
-	-	kIx~
<g/>
144,2	144,2	k4
<g/>
cm	cm	kA
<g/>
)	)	kIx)
</s>
<s>
Popis	popis	k1gInSc1
<g/>
:	:	kIx,
úhledná	úhledný	k2eAgFnSc1d1
hlava	hlava	k1gFnSc1
<g/>
,	,	kIx,
silný	silný	k2eAgInSc1d1
krk	krk	k1gInSc1
<g/>
,	,	kIx,
mohutné	mohutný	k2eAgNnSc1d1
<g/>
,	,	kIx,
kompaktní	kompaktní	k2eAgNnSc1d1
tělo	tělo	k1gNnSc1
<g/>
,	,	kIx,
krátké	krátký	k2eAgFnPc1d1
nohy	noha	k1gFnPc1
<g/>
,	,	kIx,
husté	hustý	k2eAgFnPc1d1
hříva	hříva	k1gFnSc1
a	a	k8xC
ocas	ocas	k1gInSc1
<g/>
,	,	kIx,
rousy	rous	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Povaha	povaha	k1gFnSc1
<g/>
:	:	kIx,
vnímavý	vnímavý	k2eAgMnSc1d1
<g/>
,	,	kIx,
<g/>
klidný	klidný	k2eAgMnSc1d1
,	,	kIx,
<g/>
výkonný	výkonný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgNnSc1d1
využití	využití	k1gNnSc1
<g/>
:	:	kIx,
ježdění	ježdění	k1gNnSc1
<g/>
,	,	kIx,
zemědělské	zemědělský	k2eAgFnPc1d1
práce	práce	k1gFnPc1
<g/>
,	,	kIx,
trekking	trekking	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
EDWARDS	EDWARDS	kA
<g/>
,	,	kIx,
Elwyn	Elwyn	k1gInSc4
Hartley	Hartlea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
o	o	k7c6
koních	kůň	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Gemini	Gemin	k1gMnPc1
spol	spol	k1gInSc1
<g/>
.	.	kIx.
<g/>
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o	o	k7c4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85265	#num#	k4
<g/>
-	-	kIx~
<g/>
36	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Daleský	Daleský	k2eAgMnSc5d1
pony	pony	k1gMnSc5
<g/>
,	,	kIx,
s.	s.	k?
150	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1
Svojtka	Svojtek	k1gMnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Koně	kůň	k1gMnPc1
</s>
