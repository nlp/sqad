<s>
Biedermeier	biedermeier	k1gInSc1	biedermeier
je	být	k5eAaImIp3nS	být
umělecký	umělecký	k2eAgInSc4d1	umělecký
směr	směr	k1gInSc4	směr
a	a	k8xC	a
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
typický	typický	k2eAgInSc1d1	typický
pro	pro	k7c4	pro
měšťanskou	měšťanský	k2eAgFnSc4d1	měšťanská
kulturu	kultura	k1gFnSc4	kultura
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgFnPc2d1	mluvící
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Časově	časově	k6eAd1	časově
bývá	bývat	k5eAaImIp3nS	bývat
tradičně	tradičně	k6eAd1	tradičně
ohraničován	ohraničovat	k5eAaImNgInS	ohraničovat
Vídeňským	vídeňský	k2eAgInSc7d1	vídeňský
kongresem	kongres	k1gInSc7	kongres
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
<g/>
)	)	kIx)	)
a	a	k8xC	a
revolucí	revoluce	k1gFnSc7	revoluce
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
pojmenování	pojmenování	k1gNnSc1	pojmenování
biedermeier	biedermeier	k1gInSc1	biedermeier
použil	použít	k5eAaPmAgInS	použít
poprvé	poprvé	k6eAd1	poprvé
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
Joseph	Joseph	k1gMnSc1	Joseph
Victor	Victor	k1gMnSc1	Victor
von	von	k1gInSc4	von
Scheffel	Scheffela	k1gFnPc2	Scheffela
v	v	k7c6	v
mnichovském	mnichovský	k2eAgInSc6d1	mnichovský
časopise	časopis	k1gInSc6	časopis
Fliegende	Fliegend	k1gInSc5	Fliegend
Blätter	Blätter	k1gInSc4	Blätter
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
spojením	spojení	k1gNnSc7	spojení
příjmení	příjmení	k1gNnSc4	příjmení
Biedermann	Biedermann	k1gInSc1	Biedermann
a	a	k8xC	a
Bummelmeyer	Bummelmeyer	k1gInSc1	Bummelmeyer
<g/>
.	.	kIx.	.
</s>
<s>
Biedermeier	biedermeier	k1gInSc1	biedermeier
je	být	k5eAaImIp3nS	být
měšťanská	měšťanský	k2eAgFnSc1d1	měšťanská
obdoba	obdoba	k1gFnSc1	obdoba
empíru	empír	k1gInSc2	empír
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
měšťanský	měšťanský	k2eAgInSc1d1	měšťanský
empír	empír	k1gInSc1	empír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stylu	styl	k1gInSc2	styl
odvozeného	odvozený	k2eAgInSc2d1	odvozený
z	z	k7c2	z
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
romantismus	romantismus	k1gInSc4	romantismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zdál	zdát	k5eAaImAgInS	zdát
svým	svůj	k3xOyFgInSc7	svůj
duchovním	duchovní	k2eAgInSc7d1	duchovní
vzletem	vzlet	k1gInSc7	vzlet
přinášet	přinášet	k5eAaImF	přinášet
jen	jen	k9	jen
krvavé	krvavý	k2eAgFnSc2d1	krvavá
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
zdůrazňoval	zdůrazňovat	k5eAaImAgMnS	zdůrazňovat
biedermeier	biedermeier	k1gMnSc1	biedermeier
klid	klid	k1gInSc4	klid
<g/>
,	,	kIx,	,
umírněnost	umírněnost	k1gFnSc4	umírněnost
<g/>
,	,	kIx,	,
měšťanské	měšťanský	k2eAgFnPc4d1	měšťanská
ctnosti	ctnost	k1gFnPc4	ctnost
a	a	k8xC	a
drobnou	drobný	k2eAgFnSc4d1	drobná
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
již	již	k9	již
buržoazie	buržoazie	k1gFnSc1	buržoazie
natolik	natolik	k6eAd1	natolik
majetná	majetný	k2eAgFnSc1d1	majetná
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
mohla	moct	k5eAaImAgFnS	moct
pořizovat	pořizovat	k5eAaImF	pořizovat
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
jí	jíst	k5eAaImIp3nS	jíst
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
náročnost	náročnost	k1gFnSc4	náročnost
byly	být	k5eAaImAgFnP	být
dříve	dříve	k6eAd2	dříve
finančně	finančně	k6eAd1	finančně
nedostupné	dostupný	k2eNgInPc1d1	nedostupný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
čalouněný	čalouněný	k2eAgInSc1d1	čalouněný
nábytek	nábytek	k1gInSc1	nábytek
<g/>
,	,	kIx,	,
porcelán	porcelán	k1gInSc1	porcelán
atd.	atd.	kA	atd.
–	–	k?	–
díky	díky	k7c3	díky
zavádění	zavádění	k1gNnSc3	zavádění
tovární	tovární	k2eAgFnSc2d1	tovární
výroby	výroba	k1gFnSc2	výroba
se	se	k3xPyFc4	se
náklady	náklad	k1gInPc7	náklad
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
zhotovení	zhotovení	k1gNnSc6	zhotovení
snížily	snížit	k5eAaPmAgFnP	snížit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
příjmy	příjem	k1gInPc1	příjem
měšťanů	měšťan	k1gMnPc2	měšťan
nedosahovaly	dosahovat	k5eNaImAgInP	dosahovat
takových	takový	k3xDgInPc6	takový
výší	výšit	k5eAaImIp3nP	výšit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
dovolit	dovolit	k5eAaPmF	dovolit
stavět	stavět	k5eAaImF	stavět
městské	městský	k2eAgInPc4d1	městský
domy	dům	k1gInPc4	dům
či	či	k8xC	či
paláce	palác	k1gInPc4	palác
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgInS	zůstat
biedermeier	biedermeier	k1gInSc1	biedermeier
především	především	k6eAd1	především
stylem	styl	k1gInSc7	styl
užitého	užitý	k2eAgNnSc2d1	užité
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
biedermeier	biedermeier	k1gInSc1	biedermeier
napadán	napadán	k2eAgInSc1d1	napadán
a	a	k8xC	a
odsuzován	odsuzován	k2eAgInSc1d1	odsuzován
jako	jako	k8xC	jako
idyla	idyla	k1gFnSc1	idyla
hřbitova	hřbitov	k1gInSc2	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
tzv.	tzv.	kA	tzv.
metternichovského	metternichovský	k2eAgInSc2d1	metternichovský
absolutismu	absolutismus	k1gInSc2	absolutismus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
občanských	občanský	k2eAgFnPc2d1	občanská
aktivit	aktivita	k1gFnPc2	aktivita
potlačena	potlačen	k2eAgFnSc1d1	potlačena
či	či	k8xC	či
kontrolována	kontrolován	k2eAgFnSc1d1	kontrolována
a	a	k8xC	a
biedermeier	biedermeier	k1gInSc1	biedermeier
znamenal	znamenat	k5eAaImAgInS	znamenat
rovněž	rovněž	k9	rovněž
únik	únik	k1gInSc1	únik
z	z	k7c2	z
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
ke	k	k7c3	k
klidu	klid	k1gInSc3	klid
u	u	k7c2	u
domácího	domácí	k2eAgInSc2d1	domácí
krbu	krb	k1gInSc2	krb
<g/>
.	.	kIx.	.
</s>
<s>
Biedermeier	biedermeier	k1gInSc1	biedermeier
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
i	i	k9	i
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
(	(	kIx(	(
<g/>
velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
právě	právě	k6eAd1	právě
módním	módní	k2eAgInSc7d1	módní
romantismem	romantismus	k1gInSc7	romantismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
malířství	malířství	k1gNnSc6	malířství
(	(	kIx(	(
<g/>
zobrazovány	zobrazován	k2eAgFnPc1d1	zobrazována
byly	být	k5eAaImAgFnP	být
měšťanské	měšťanský	k2eAgInPc4d1	měšťanský
pokoje	pokoj	k1gInPc4	pokoj
<g/>
,	,	kIx,	,
dvorky	dvorek	k1gInPc4	dvorek
a	a	k8xC	a
zátiší	zátiší	k1gNnSc4	zátiší
<g/>
;	;	kIx,	;
oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
portrét	portrét	k1gInSc1	portrét
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
umělců	umělec	k1gMnPc2	umělec
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
biedermeierem	biedermeier	k1gMnSc7	biedermeier
spojování	spojování	k1gNnSc2	spojování
např.	např.	kA	např.
básníci	básník	k1gMnPc1	básník
Annette	Annett	k1gInSc5	Annett
von	von	k1gInSc4	von
Droste-Hülshoff	Droste-Hülshoff	k1gMnSc1	Droste-Hülshoff
či	či	k8xC	či
Eduard	Eduard	k1gMnSc1	Eduard
Mörike	Mörik	k1gFnSc2	Mörik
nebo	nebo	k8xC	nebo
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Franz	Franz	k1gMnSc1	Franz
Schubert	Schubert	k1gMnSc1	Schubert
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
biedermeierovým	biedermeierův	k2eAgMnPc3d1	biedermeierův
dramatikům	dramatik	k1gMnPc3	dramatik
bývají	bývat	k5eAaImIp3nP	bývat
řazeni	řazen	k2eAgMnPc1d1	řazen
Franz	Franz	k1gMnSc1	Franz
Grillparzer	Grillparzer	k1gMnSc1	Grillparzer
a	a	k8xC	a
Johann	Johann	k1gMnSc1	Johann
Nepomuk	Nepomuk	k1gInSc4	Nepomuk
Nestroy	Nestroa	k1gFnSc2	Nestroa
<g/>
.	.	kIx.	.
</s>
<s>
Českým	český	k2eAgInSc7d1	český
příkladem	příklad	k1gInSc7	příklad
biedermeieru	biedermeier	k1gInSc2	biedermeier
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
literární	literární	k2eAgNnSc1d1	literární
dílo	dílo	k1gNnSc1	dílo
Magdaleny	Magdalena	k1gFnSc2	Magdalena
Dobromily	Dobromila	k1gFnSc2	Dobromila
Rettigové	Rettigový	k2eAgFnSc2d1	Rettigová
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
její	její	k3xOp3gFnPc4	její
kuchařky	kuchařka	k1gFnPc4	kuchařka
<g/>
,	,	kIx,	,
či	či	k8xC	či
portrétní	portrétní	k2eAgFnSc1d1	portrétní
tvorba	tvorba	k1gFnSc1	tvorba
Antonína	Antonín	k1gMnSc2	Antonín
Machka	Machek	k1gMnSc2	Machek
<g/>
.	.	kIx.	.
</s>
