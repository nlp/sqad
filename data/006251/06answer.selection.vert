<s>
Biedermeier	biedermeier	k1gInSc1	biedermeier
je	být	k5eAaImIp3nS	být
umělecký	umělecký	k2eAgInSc4d1	umělecký
směr	směr	k1gInSc4	směr
a	a	k8xC	a
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
typický	typický	k2eAgInSc1d1	typický
pro	pro	k7c4	pro
měšťanskou	měšťanský	k2eAgFnSc4d1	měšťanská
kulturu	kultura	k1gFnSc4	kultura
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgFnPc2d1	mluvící
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
