<p>
<s>
"	"	kIx"	"
<g/>
Some	Som	k1gMnSc4	Som
Kinda	Kind	k1gMnSc4	Kind
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
píseň	píseň	k1gFnSc4	píseň
americké	americký	k2eAgFnSc2d1	americká
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc1	The
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
původní	původní	k2eAgFnSc1d1	původní
studiová	studiový	k2eAgFnSc1d1	studiová
verze	verze	k1gFnSc1	verze
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
na	na	k7c6	na
albu	album	k1gNnSc6	album
The	The	k1gFnSc1	The
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
textu	text	k1gInSc2	text
i	i	k8xC	i
hudby	hudba	k1gFnSc2	hudba
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
je	být	k5eAaImIp3nS	být
Lou	Lou	k1gMnSc1	Lou
Reed	Reed	k1gMnSc1	Reed
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
původní	původní	k2eAgFnSc6d1	původní
verzi	verze	k1gFnSc6	verze
rovněž	rovněž	k9	rovněž
zpíval	zpívat	k5eAaImAgMnS	zpívat
<g/>
.	.	kIx.	.
</s>
<s>
Koncertní	koncertní	k2eAgFnPc1d1	koncertní
verze	verze	k1gFnPc1	verze
písně	píseň	k1gFnSc2	píseň
vyšly	vyjít	k5eAaPmAgFnP	vyjít
na	na	k7c6	na
albech	album	k1gNnPc6	album
1969	[number]	k4	1969
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Velvet	Velvet	k1gInSc4	Velvet
Underground	underground	k1gInSc1	underground
Live	Live	k1gFnSc1	Live
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Live	Live	k1gInSc1	Live
MCMXCIII	MCMXCIII	kA	MCMXCIII
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bootleg	Bootleg	k1gMnSc1	Bootleg
Series	Series	k1gMnSc1	Series
Volume	volum	k1gInSc5	volum
1	[number]	k4	1
<g/>
:	:	kIx,	:
The	The	k1gMnSc5	The
Quine	Quin	k1gMnSc5	Quin
Tapes	Tapes	k1gMnSc1	Tapes
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Final	Final	k1gMnSc1	Final
V.U.	V.U.	k1gFnSc2	V.U.
1971-1973	[number]	k4	1971-1973
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
a	a	k8xC	a
The	The	k1gMnSc1	The
Complete	Comple	k1gNnSc2	Comple
Matrix	Matrix	k1gInSc1	Matrix
Tapes	Tapes	k1gInSc1	Tapes
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reed	Reed	k1gInSc1	Reed
vydal	vydat	k5eAaPmAgInS	vydat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
koncertní	koncertní	k2eAgFnSc4d1	koncertní
verzi	verze	k1gFnSc4	verze
písně	píseň	k1gFnSc2	píseň
na	na	k7c6	na
albu	album	k1gNnSc6	album
Live	Liv	k1gInSc2	Liv
in	in	k?	in
Italy	Ital	k1gMnPc7	Ital
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
;	;	kIx,	;
totožná	totožný	k2eAgFnSc1d1	totožná
nahrávka	nahrávka	k1gFnSc1	nahrávka
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
také	také	k9	také
na	na	k7c6	na
novějším	nový	k2eAgNnSc6d2	novější
albu	album	k1gNnSc6	album
Live	Live	k1gNnSc4	Live
in	in	k?	in
Concert	Concert	k1gInSc1	Concert
<g/>
)	)	kIx)	)
Česká	český	k2eAgFnSc1d1	Česká
kapela	kapela	k1gFnSc1	kapela
The	The	k1gFnSc2	The
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gFnSc2	People
of	of	k?	of
the	the	k?	the
Universe	Universe	k1gFnSc1	Universe
vydala	vydat	k5eAaPmAgFnS	vydat
coververzi	coververze	k1gFnSc4	coververze
písně	píseň	k1gFnSc2	píseň
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
koncertním	koncertní	k2eAgNnSc6d1	koncertní
albu	album	k1gNnSc6	album
Trouble	Trouble	k1gMnSc2	Trouble
Every	Evera	k1gMnSc2	Evera
Day	Day	k1gMnSc2	Day
<g/>
.	.	kIx.	.
</s>
<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
kapela	kapela	k1gFnSc1	kapela
Levellers	Levellers	k1gInSc1	Levellers
5	[number]	k4	5
vydala	vydat	k5eAaPmAgFnS	vydat
svou	svůj	k3xOyFgFnSc4	svůj
verzi	verze	k1gFnSc4	verze
písně	píseň	k1gFnSc2	píseň
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
Heaven	Heaven	k2eAgInSc4d1	Heaven
&	&	k?	&
Hell	Hell	k1gInSc4	Hell
<g/>
:	:	kIx,	:
A	a	k9	a
Tribute	tribut	k1gInSc5	tribut
to	ten	k3xDgNnSc4	ten
The	The	k1gMnSc1	The
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
DEMING	DEMING	kA	DEMING
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc1	underground
<g/>
:	:	kIx,	:
Some	Some	k1gFnSc1	Some
Kinda	Kinda	k1gFnSc1	Kinda
Love	lov	k1gInSc5	lov
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Allmusic	Allmusic	k1gMnSc1	Allmusic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc1	underground
–	–	k?	–
see	see	k?	see
the	the	k?	the
video	video	k1gNnSc4	video
for	forum	k1gNnPc2	forum
Some	Som	k1gFnSc2	Som
Kinda	Kinda	k1gFnSc1	Kinda
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
live	liv	k1gInPc4	liv
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Guardian	Guardian	k1gMnSc1	Guardian
<g/>
,	,	kIx,	,
2015-11-16	[number]	k4	2015-11-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
