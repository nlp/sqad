<s>
Irsko	Irsko	k1gNnSc1	Irsko
(	(	kIx(	(
<g/>
irsky	irsky	k6eAd1	irsky
Éire	Éire	k1gFnSc1	Éire
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Ireland	Ireland	k1gInSc1	Ireland
<g/>
,	,	kIx,	,
také	také	k9	také
irsky	irsky	k6eAd1	irsky
Poblacht	Poblacht	k1gInSc4	Poblacht
na	na	k7c4	na
hÉireann	hÉireann	k1gInSc4	hÉireann
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
Republic	Republice	k1gFnPc2	Republice
of	of	k?	of
Ireland	Irelanda	k1gFnPc2	Irelanda
<g/>
,	,	kIx,	,
Irská	irský	k2eAgFnSc1d1	irská
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
zaujímající	zaujímající	k2eAgFnPc4d1	zaujímající
zhruba	zhruba	k6eAd1	zhruba
pět	pět	k4xCc4	pět
šestin	šestina	k1gFnPc2	šestina
povrchu	povrch	k1gInSc2	povrch
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Irsko	Irsko	k1gNnSc1	Irsko
sousedí	sousedit	k5eAaImIp3nS	sousedit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
částí	část	k1gFnSc7	část
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
–	–	k?	–
Severním	severní	k2eAgNnSc7d1	severní
Irskem	Irsko	k1gNnSc7	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
břehy	břeh	k1gInPc1	břeh
omývá	omývat	k5eAaImIp3nS	omývat
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Irské	irský	k2eAgNnSc1d1	irské
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Svatojiřský	svatojiřský	k2eAgInSc4d1	svatojiřský
průliv	průliv	k1gInSc4	průliv
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Keltské	keltský	k2eAgNnSc4d1	keltské
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Irsko	Irsko	k1gNnSc1	Irsko
je	být	k5eAaImIp3nS	být
unitární	unitární	k2eAgFnSc1d1	unitární
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
republika	republika	k1gFnSc1	republika
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Dublin	Dublin	k1gInSc1	Dublin
(	(	kIx(	(
<g/>
irsky	irsky	k6eAd1	irsky
Baile	Bail	k1gMnSc2	Bail
Átha	Áthus	k1gMnSc2	Áthus
Cliath	Cliath	k1gMnSc1	Cliath
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samostatné	samostatný	k2eAgNnSc1d1	samostatné
Irsko	Irsko	k1gNnSc1	Irsko
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
mnoholetého	mnoholetý	k2eAgInSc2d1	mnoholetý
boje	boj	k1gInSc2	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1937	[number]	k4	1937
se	se	k3xPyFc4	se
Irská	irský	k2eAgFnSc1d1	irská
republika	republika	k1gFnSc1	republika
stala	stát	k5eAaPmAgFnS	stát
nástupcem	nástupce	k1gMnSc7	nástupce
Irského	irský	k2eAgInSc2d1	irský
svobodného	svobodný	k2eAgInSc2d1	svobodný
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
irský	irský	k2eAgInSc1d1	irský
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
ve	v	k7c6	v
světovém	světový	k2eAgNnSc6d1	světové
měřítku	měřítko	k1gNnSc6	měřítko
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
12	[number]	k4	12
<g/>
.	.	kIx.	.
až	až	k9	až
17	[number]	k4	17
<g/>
.	.	kIx.	.
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
zdroji	zdroj	k1gInSc6	zdroj
<g/>
,	,	kIx,	,
bývalo	bývat	k5eAaImAgNnS	bývat
Irsko	Irsko	k1gNnSc1	Irsko
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejchudších	chudý	k2eAgMnPc2d3	nejchudší
států	stát	k1gInPc2	stát
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
míru	míra	k1gFnSc4	míra
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
ekonomie	ekonomie	k1gFnSc1	ekonomie
protekcionismu	protekcionismus	k1gInSc2	protekcionismus
a	a	k8xC	a
Irsko	Irsko	k1gNnSc1	Irsko
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
připojilo	připojit	k5eAaPmAgNnS	připojit
k	k	k7c3	k
Evropskému	evropský	k2eAgNnSc3d1	Evropské
společenství	společenství	k1gNnSc3	společenství
(	(	kIx(	(
<g/>
současné	současný	k2eAgFnSc3d1	současná
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vedla	vést	k5eAaImAgFnS	vést
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
krize	krize	k1gFnSc1	krize
Irsko	Irsko	k1gNnSc4	Irsko
k	k	k7c3	k
započetí	započetí	k1gNnSc3	započetí
velkých	velký	k2eAgFnPc2d1	velká
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Následná	následný	k2eAgFnSc1d1	následná
liberální	liberální	k2eAgFnSc1d1	liberální
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
politika	politika	k1gFnSc1	politika
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
výrazné	výrazný	k2eAgNnSc1d1	výrazné
snížení	snížení	k1gNnSc1	snížení
daní	daň	k1gFnPc2	daň
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
zeměmi	zem	k1gFnPc7	zem
EU	EU	kA	EU
<g/>
)	)	kIx)	)
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
celkové	celkový	k2eAgFnSc3d1	celková
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
prosperitě	prosperita	k1gFnSc3	prosperita
v	v	k7c6	v
letech	let	k1gInPc6	let
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
si	se	k3xPyFc3	se
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
vysloužilo	vysloužit	k5eAaPmAgNnS	vysloužit
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
Celtic	Celtice	k1gFnPc2	Celtice
Tiger	Tiger	k1gInSc1	Tiger
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
keltský	keltský	k2eAgMnSc1d1	keltský
tygr	tygr	k1gMnSc1	tygr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
finanční	finanční	k2eAgFnSc4d1	finanční
krizi	krize	k1gFnSc4	krize
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
rychlého	rychlý	k2eAgInSc2d1	rychlý
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
růstu	růst	k1gInSc2	růst
uťato	uťat	k2eAgNnSc1d1	uťat
<g/>
.	.	kIx.	.
</s>
<s>
Irsku	Irsko	k1gNnSc3	Irsko
náleží	náležet	k5eAaImIp3nS	náležet
jedenáctá	jedenáctý	k4xOgFnSc1	jedenáctý
příčka	příčka	k1gFnSc1	příčka
žebříčku	žebříček	k1gInSc2	žebříček
indexu	index	k1gInSc2	index
lidského	lidský	k2eAgInSc2d1	lidský
rozvoje	rozvoj	k1gInSc2	rozvoj
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
pyšní	pyšnit	k5eAaImIp3nS	pyšnit
největší	veliký	k2eAgFnSc7d3	veliký
kvalitou	kvalita	k1gFnSc7	kvalita
života	život	k1gInSc2	život
na	na	k7c6	na
světě	svět	k1gInSc6	svět
–	–	k?	–
drží	držet	k5eAaImIp3nS	držet
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
indexu	index	k1gInSc6	index
kvality	kvalita	k1gFnSc2	kvalita
života	život	k1gInSc2	život
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
–	–	k?	–
a	a	k8xC	a
umístil	umístit	k5eAaPmAgMnS	umístit
se	se	k3xPyFc4	se
na	na	k7c6	na
šestém	šestý	k4xOgNnSc6	šestý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
světovém	světový	k2eAgInSc6d1	světový
indexu	index	k1gInSc6	index
míru	mír	k1gInSc2	mír
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Irsko	Irsko	k1gNnSc1	Irsko
je	být	k5eAaImIp3nS	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
hodnoceno	hodnotit	k5eAaImNgNnS	hodnotit
za	za	k7c4	za
vzdělávací	vzdělávací	k2eAgInSc4d1	vzdělávací
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
politickou	politický	k2eAgFnSc4d1	politická
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
svobodu	svoboda	k1gFnSc4	svoboda
tisku	tisk	k1gInSc2	tisk
a	a	k8xC	a
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
Irsko	Irsko	k1gNnSc1	Irsko
nacházelo	nacházet	k5eAaImAgNnS	nacházet
na	na	k7c4	na
170	[number]	k4	170
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
osmém	osmý	k4xOgInSc6	osmý
místě	místo	k1gNnSc6	místo
odzadu	odzadu	k6eAd1	odzadu
<g/>
)	)	kIx)	)
v	v	k7c6	v
indexu	index	k1gInSc6	index
zhroucených	zhroucený	k2eAgInPc2d1	zhroucený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Irsko	Irsko	k1gNnSc1	Irsko
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
OSN	OSN	kA	OSN
a	a	k8xC	a
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
OECD	OECD	kA	OECD
a	a	k8xC	a
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Irská	irský	k2eAgFnSc1d1	irská
neutrální	neutrální	k2eAgFnSc1d1	neutrální
politika	politika	k1gFnSc1	politika
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
členem	člen	k1gMnSc7	člen
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
mírových	mírový	k2eAgFnPc6d1	mírová
operacích	operace	k1gFnPc6	operace
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
článku	článek	k1gInSc6	článek
Ústavy	ústava	k1gFnSc2	ústava
Irska	Irsko	k1gNnSc2	Irsko
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
irský	irský	k2eAgInSc1d1	irský
název	název	k1gInSc1	název
státu	stát	k1gInSc2	stát
Éire	Éire	k1gFnPc2	Éire
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
anglická	anglický	k2eAgFnSc1d1	anglická
alternativa	alternativa	k1gFnSc1	alternativa
Ireland	Irelanda	k1gFnPc2	Irelanda
<g/>
.	.	kIx.	.
</s>
<s>
Běžnou	běžný	k2eAgFnSc7d1	běžná
praxí	praxe	k1gFnSc7	praxe
je	být	k5eAaImIp3nS	být
však	však	k9	však
omezení	omezení	k1gNnSc1	omezení
používání	používání	k1gNnSc2	používání
názvu	název	k1gInSc2	název
Éire	Éir	k1gFnSc2	Éir
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
irština	irština	k1gFnSc1	irština
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
anglické	anglický	k2eAgInPc4d1	anglický
překlady	překlad	k1gInPc4	překlad
měl	mít	k5eAaImAgMnS	mít
používat	používat	k5eAaImF	používat
(	(	kIx(	(
<g/>
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
název	název	k1gInSc1	název
Ireland	Irelanda	k1gFnPc2	Irelanda
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
upravoval	upravovat	k5eAaImAgInS	upravovat
přenos	přenos	k1gInSc1	přenos
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
povinností	povinnost	k1gFnPc2	povinnost
monarcha	monarcha	k1gMnSc1	monarcha
na	na	k7c4	na
voleného	volený	k2eAgMnSc4d1	volený
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc1	jméno
Republic	Republice	k1gFnPc2	Republice
of	of	k?	of
Ireland	Irelanda	k1gFnPc2	Irelanda
(	(	kIx(	(
<g/>
irsky	irsky	k6eAd1	irsky
Poblacht	Poblacht	k2eAgInSc1d1	Poblacht
na	na	k7c4	na
hÉireann	hÉireann	k1gInSc4	hÉireann
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Irská	irský	k2eAgFnSc1d1	irská
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
popisem	popis	k1gInSc7	popis
státu	stát	k1gInSc2	stát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
jeho	jeho	k3xOp3gNnSc7	jeho
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nadále	nadále	k6eAd1	nadále
jméno	jméno	k1gNnSc1	jméno
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
názvu	název	k1gInSc2	název
státu	stát	k1gInSc2	stát
by	by	kYmCp3nS	by
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
změnu	změna	k1gFnSc4	změna
ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
označení	označení	k1gNnSc2	označení
Éire	Éir	k1gInSc2	Éir
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
i	i	k9	i
Ireland	Ireland	k1gInSc4	Ireland
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
Výklady	výklad	k1gInPc1	výklad
původu	původ	k1gInSc2	původ
názvu	název	k1gInSc2	název
starými	starý	k2eAgFnPc7d1	stará
irskými	irský	k2eAgFnPc7d1	irská
legendami	legenda	k1gFnPc7	legenda
však	však	k9	však
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
etymologický	etymologický	k2eAgInSc4d1	etymologický
výzkum	výzkum	k1gInSc4	výzkum
takřka	takřka	k6eAd1	takřka
bezcenné	bezcenný	k2eAgFnPc1d1	bezcenná
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
nejasnostem	nejasnost	k1gFnPc3	nejasnost
je	být	k5eAaImIp3nS	být
nade	nad	k7c4	nad
všechny	všechen	k3xTgFnPc4	všechen
pochybnosti	pochybnost	k1gFnPc4	pochybnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
název	název	k1gInSc4	název
velmi	velmi	k6eAd1	velmi
starobylý	starobylý	k2eAgMnSc1d1	starobylý
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
v	v	k7c6	v
geografických	geografický	k2eAgInPc6d1	geografický
zdrojích	zdroj	k1gInPc6	zdroj
starověkého	starověký	k2eAgNnSc2d1	starověké
Řecka	Řecko	k1gNnSc2	Řecko
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
název	název	k1gInSc1	název
Ierne	Iern	k1gInSc5	Iern
<g/>
.	.	kIx.	.
</s>
<s>
Ptolemaiova	Ptolemaiův	k2eAgFnSc1d1	Ptolemaiova
mapa	mapa	k1gFnSc1	mapa
světa	svět	k1gInSc2	svět
přibližně	přibližně	k6eAd1	přibližně
z	z	k7c2	z
roku	rok	k1gInSc2	rok
150	[number]	k4	150
n.	n.	k?	n.
l.	l.	k?	l.
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
Irsko	Irsko	k1gNnSc4	Irsko
označení	označení	k1gNnSc2	označení
Iouernia	Iouernium	k1gNnSc2	Iouernium
<g/>
,	,	kIx,	,
kterýžto	kterýžto	k?	kterýžto
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
latiny	latina	k1gFnSc2	latina
transliterován	transliterovat	k5eAaBmNgMnS	transliterovat
jako	jako	k9	jako
Iuverna	Iuverna	k1gFnSc1	Iuverna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Caesarově	Caesarův	k2eAgNnSc6d1	Caesarovo
díle	dílo	k1gNnSc6	dílo
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
už	už	k6eAd1	už
nachází	nacházet	k5eAaImIp3nS	nacházet
název	název	k1gInSc1	název
Hibernia	Hibernium	k1gNnSc2	Hibernium
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
kontaminací	kontaminace	k1gFnSc7	kontaminace
původního	původní	k2eAgNnSc2d1	původní
označení	označení	k1gNnSc2	označení
a	a	k8xC	a
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
hibernus	hibernus	k1gInSc1	hibernus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
česky	česky	k6eAd1	česky
chladný	chladný	k2eAgInSc1d1	chladný
<g/>
,	,	kIx,	,
zimní	zimní	k2eAgInSc1d1	zimní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
irské	irský	k2eAgFnSc6d1	irská
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
objevuje	objevovat	k5eAaImIp3nS	objevovat
staroirský	staroirský	k2eAgInSc4d1	staroirský
termín	termín	k1gInSc4	termín
Ériu	Érius	k1gInSc2	Érius
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
již	již	k6eAd1	již
zmíněný	zmíněný	k2eAgInSc4d1	zmíněný
současný	současný	k2eAgInSc4d1	současný
název	název	k1gInSc4	název
Éire	Éir	k1gFnSc2	Éir
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
základu	základ	k1gInSc2	základ
se	s	k7c7	s
přidáním	přidání	k1gNnSc7	přidání
germánské	germánský	k2eAgFnSc2d1	germánská
koncovky	koncovka	k1gFnSc2	koncovka
-land	anda	k1gFnPc2	-landa
<g/>
,	,	kIx,	,
charakteristické	charakteristický	k2eAgFnSc2d1	charakteristická
pro	pro	k7c4	pro
pojmenování	pojmenování	k1gNnSc4	pojmenování
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
současný	současný	k2eAgInSc1d1	současný
anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
Ireland	Irelanda	k1gFnPc2	Irelanda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
příbuzných	příbuzný	k1gMnPc2	příbuzný
irštině	irština	k1gFnSc6	irština
<g/>
,	,	kIx,	,
velštině	velština	k1gFnSc6	velština
a	a	k8xC	a
bretonštině	bretonština	k1gFnSc6	bretonština
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
Irska	Irsko	k1gNnSc2	Irsko
tvary	tvar	k1gInPc1	tvar
Ywerddon	Ywerddon	k1gNnSc1	Ywerddon
<g/>
,	,	kIx,	,
Iwerdon	Iwerdon	k1gNnSc1	Iwerdon
a	a	k8xC	a
Iverdon	Iverdon	k1gNnSc1	Iverdon
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
protokeltského	protokeltský	k2eAgMnSc2d1	protokeltský
*	*	kIx~	*
<g/>
ɸ	ɸ	k?	ɸ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
tvar	tvar	k1gInSc4	tvar
velšský	velšský	k2eAgInSc4d1	velšský
Iwerddon	Iwerddon	k1gInSc4	Iwerddon
a	a	k8xC	a
bretonský	bretonský	k2eAgInSc4d1	bretonský
Iwerzhon	Iwerzhon	k1gInSc4	Iwerzhon
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
také	také	k9	také
kornsky	kornsky	k6eAd1	kornsky
Iwerdhon	Iwerdhon	k1gMnSc1	Iwerdhon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
irské	irský	k2eAgFnSc6d1	irská
mytologii	mytologie	k1gFnSc6	mytologie
byl	být	k5eAaImAgMnS	být
Ériu	Éria	k1gFnSc4	Éria
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
božských	božský	k2eAgMnPc2d1	božský
eponymů	eponym	k1gMnPc2	eponym
pro	pro	k7c4	pro
Irsko	Irsko	k1gNnSc4	Irsko
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
eponymy	eponym	k1gMnPc7	eponym
Banba	Banb	k1gMnSc2	Banb
a	a	k8xC	a
Fódla	Fódl	k1gMnSc2	Fódl
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Irsko	Irsko	k1gNnSc1	Irsko
je	být	k5eAaImIp3nS	být
nástupcem	nástupce	k1gMnSc7	nástupce
dominia	dominion	k1gNnSc2	dominion
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
Irský	irský	k2eAgInSc1d1	irský
svobodný	svobodný	k2eAgInSc1d1	svobodný
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dominium	dominium	k1gNnSc1	dominium
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1921	[number]	k4	1921
po	po	k7c6	po
irské	irský	k2eAgFnSc6d1	irská
válce	válka	k1gFnSc6	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
když	když	k8xS	když
celý	celý	k2eAgInSc1d1	celý
Irský	irský	k2eAgInSc1d1	irský
ostrov	ostrov	k1gInSc1	ostrov
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
ze	z	k7c2	z
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
hned	hned	k6eAd1	hned
další	další	k2eAgInSc4d1	další
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
parlament	parlament	k1gInSc1	parlament
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
podepsal	podepsat	k5eAaPmAgInS	podepsat
pod	pod	k7c4	pod
angloirskou	angloirský	k2eAgFnSc4d1	angloirský
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
připojilo	připojit	k5eAaPmAgNnS	připojit
zpět	zpět	k6eAd1	zpět
ke	k	k7c3	k
Spojenému	spojený	k2eAgNnSc3d1	spojené
království	království	k1gNnSc3	království
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
rozdělení	rozdělení	k1gNnSc1	rozdělení
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Irský	irský	k2eAgInSc1d1	irský
svobodný	svobodný	k2eAgInSc1d1	svobodný
stát	stát	k1gInSc1	stát
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
v	v	k7c4	v
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
formálně	formálně	k6eAd1	formálně
založeno	založit	k5eAaPmNgNnS	založit
Irsko	Irsko	k1gNnSc1	Irsko
–	–	k?	–
tedy	tedy	k8xC	tedy
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
se	se	k3xPyFc4	se
Irsko	Irsko	k1gNnSc1	Irsko
připojilo	připojit	k5eAaPmAgNnS	připojit
k	k	k7c3	k
Evropskému	evropský	k2eAgNnSc3d1	Evropské
společenství	společenství	k1gNnSc3	společenství
(	(	kIx(	(
<g/>
současné	současný	k2eAgFnSc3d1	současná
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Irsko	Irsko	k1gNnSc1	Irsko
(	(	kIx(	(
<g/>
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Irský	irský	k2eAgInSc1d1	irský
ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
84	[number]	k4	84
412	[number]	k4	412
km2	km2	k4	km2
druhým	druhý	k4xOgNnSc7	druhý
největším	veliký	k2eAgMnSc7d3	veliký
z	z	k7c2	z
Britských	britský	k2eAgMnPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Omývají	omývat	k5eAaImIp3nP	omývat
jej	on	k3xPp3gInSc4	on
vody	voda	k1gFnPc1	voda
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
Keltského	keltský	k2eAgNnSc2d1	keltské
moře	moře	k1gNnSc2	moře
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
a	a	k8xC	a
Irského	irský	k2eAgNnSc2d1	irské
moře	moře	k1gNnSc2	moře
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Irsko	Irsko	k1gNnSc4	Irsko
používá	používat	k5eAaImIp3nS	používat
GMT	GMT	kA	GMT
(	(	kIx(	(
<g/>
UTC	UTC	kA	UTC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
označovaný	označovaný	k2eAgMnSc1d1	označovaný
jako	jako	k8xC	jako
Ireland	Ireland	k1gInSc1	Ireland
Standard	standard	k1gInSc1	standard
Time	Time	k1gFnSc1	Time
(	(	kIx(	(
<g/>
IST	IST	kA	IST
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zemích	zem	k1gFnPc6	zem
EU	EU	kA	EU
zde	zde	k6eAd1	zde
letní	letní	k2eAgInSc1d1	letní
čas	čas	k1gInSc1	čas
začíná	začínat	k5eAaImIp3nS	začínat
poslední	poslední	k2eAgFnSc4d1	poslední
březnovou	březnový	k2eAgFnSc4d1	březnová
neděli	neděle	k1gFnSc4	neděle
v	v	k7c6	v
1.00	[number]	k4	1.00
UTC	UTC	kA	UTC
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
poslední	poslední	k2eAgFnSc4d1	poslední
říjnovou	říjnový	k2eAgFnSc4d1	říjnová
neděli	neděle	k1gFnSc4	neděle
v	v	k7c6	v
2.00	[number]	k4	2.00
UTC	UTC	kA	UTC
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
posouvá	posouvat	k5eAaImIp3nS	posouvat
na	na	k7c6	na
UTC	UTC	kA	UTC
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
Irska	Irsko	k1gNnSc2	Irsko
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
nížinatý	nížinatý	k2eAgMnSc1d1	nížinatý
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
v	v	k7c6	v
centrálních	centrální	k2eAgFnPc6d1	centrální
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Hornatiny	hornatina	k1gFnPc1	hornatina
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
Carrauntoohil	Carrauntoohil	k1gInSc1	Carrauntoohil
(	(	kIx(	(
<g/>
1041	[number]	k4	1041
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
irsky	irsky	k6eAd1	irsky
Carrán	Carrán	k2eAgInSc1d1	Carrán
Tuathail	Tuathail	k1gInSc1	Tuathail
<g/>
)	)	kIx)	)
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Macgillycuddy	Macgillycudda	k1gFnSc2	Macgillycudda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Reeks	Reeksa	k1gFnPc2	Reeksa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
významná	významný	k2eAgNnPc4d1	významné
pohoří	pohoří	k1gNnPc4	pohoří
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Wicklow	Wicklow	k1gFnSc1	Wicklow
Mountains	Mountainsa	k1gFnPc2	Mountainsa
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
Antrim	Antrim	k1gInSc1	Antrim
Plateau	Plateaus	k1gInSc2	Plateaus
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
řekou	řeka	k1gFnSc7	řeka
nejen	nejen	k6eAd1	nejen
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
je	být	k5eAaImIp3nS	být
386	[number]	k4	386
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
řeka	řeka	k1gFnSc1	řeka
Shannon	Shannona	k1gFnPc2	Shannona
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
toku	tok	k1gInSc6	tok
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgNnPc4	tři
velká	velký	k2eAgNnPc4d1	velké
jezera	jezero	k1gNnPc4	jezero
(	(	kIx(	(
<g/>
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Lough	Lough	k1gMnSc1	Lough
Allen	Allen	k1gMnSc1	Allen
<g/>
,	,	kIx,	,
Lough	Lough	k1gMnSc1	Lough
Ree	Rea	k1gFnSc6	Rea
a	a	k8xC	a
Lough	Lough	k1gMnSc1	Lough
Derg	Derg	k1gMnSc1	Derg
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
jezerem	jezero	k1gNnSc7	jezero
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
Lough	Lough	k1gMnSc1	Lough
Neagh	Neagh	k1gMnSc1	Neagh
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc4	ostrov
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
Smaragdový	smaragdový	k2eAgInSc1d1	smaragdový
ostrov	ostrov	k1gInSc1	ostrov
<g/>
"	"	kIx"	"
díky	díky	k7c3	díky
bujné	bujný	k2eAgFnSc3d1	bujná
vegetaci	vegetace	k1gFnSc3	vegetace
<g/>
,	,	kIx,	,
<g/>
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
díky	díky	k7c3	díky
mírnému	mírný	k2eAgNnSc3d1	mírné
klimatu	klima	k1gNnSc3	klima
a	a	k8xC	a
častému	častý	k2eAgInSc3d1	častý
jemnému	jemný	k2eAgInSc3d1	jemný
dešti	dešť	k1gInSc3	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
Irska	Irsko	k1gNnSc2	Irsko
leží	ležet	k5eAaImIp3nS	ležet
louky	louka	k1gFnPc4	louka
a	a	k8xC	a
vřesoviště	vřesoviště	k1gNnPc4	vřesoviště
<g/>
.	.	kIx.	.
15	[number]	k4	15
%	%	kIx~	%
území	území	k1gNnSc2	území
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
rašeliniště	rašeliniště	k1gNnSc4	rašeliniště
a	a	k8xC	a
5	[number]	k4	5
%	%	kIx~	%
rozlohy	rozloha	k1gFnPc1	rozloha
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
původní	původní	k2eAgInPc4d1	původní
listnaté	listnatý	k2eAgInPc4d1	listnatý
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
překvapivě	překvapivě	k6eAd1	překvapivě
rostou	růst	k5eAaImIp3nP	růst
vavříny	vavřín	k1gInPc1	vavřín
<g/>
,	,	kIx,	,
myrty	myrta	k1gFnPc1	myrta
a	a	k8xC	a
planiky	planik	k1gInPc1	planik
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
rostou	růst	k5eAaImIp3nP	růst
především	především	k6eAd1	především
ve	v	k7c6	v
středomoří	středomoří	k1gNnSc6	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Irská	irský	k2eAgFnSc1d1	irská
fauna	fauna	k1gFnSc1	fauna
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
bohatá	bohatý	k2eAgFnSc1d1	bohatá
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bývala	bývat	k5eAaImAgFnS	bývat
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jejich	jejich	k3xOp3gInSc7	jejich
příchodem	příchod	k1gInSc7	příchod
vymizely	vymizet	k5eAaPmAgFnP	vymizet
zdejší	zdejší	k2eAgFnPc1d1	zdejší
populace	populace	k1gFnPc1	populace
bobrů	bobr	k1gMnPc2	bobr
<g/>
,	,	kIx,	,
divokých	divoký	k2eAgFnPc2d1	divoká
koček	kočka	k1gFnPc2	kočka
<g/>
,	,	kIx,	,
medvědů	medvěd	k1gMnPc2	medvěd
a	a	k8xC	a
vlků	vlk	k1gMnPc2	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
zde	zde	k6eAd1	zde
žijí	žít	k5eAaImIp3nP	žít
hlavně	hlavně	k9	hlavně
drobní	drobný	k2eAgMnPc1d1	drobný
hlodavci	hlodavec	k1gMnPc1	hlodavec
<g/>
,	,	kIx,	,
ptáci	pták	k1gMnPc1	pták
a	a	k8xC	a
ovce	ovce	k1gFnPc1	ovce
<g/>
.	.	kIx.	.
</s>
<s>
Vůbec	vůbec	k9	vůbec
tu	tu	k6eAd1	tu
nežijí	žít	k5eNaImIp3nP	žít
hadi	had	k1gMnPc1	had
<g/>
.	.	kIx.	.
</s>
<s>
Nejhůře	zle	k6eAd3	zle
obdělavatelná	obdělavatelný	k2eAgFnSc1d1	obdělavatelná
půda	půda	k1gFnSc1	půda
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jihozápadních	jihozápadní	k2eAgInPc6d1	jihozápadní
a	a	k8xC	a
západních	západní	k2eAgInPc6d1	západní
krajích	kraj	k1gInPc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
části	část	k1gFnPc1	část
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
hornaté	hornatý	k2eAgFnPc1d1	hornatá
<g/>
,	,	kIx,	,
s	s	k7c7	s
nádhernými	nádherný	k2eAgInPc7d1	nádherný
výhledy	výhled	k1gInPc7	výhled
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Podnebí	podnebí	k1gNnSc2	podnebí
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
podnebí	podnebí	k1gNnSc4	podnebí
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
má	mít	k5eAaImIp3nS	mít
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
s	s	k7c7	s
teplým	teplý	k2eAgInSc7d1	teplý
Severoatlantským	severoatlantský	k2eAgInSc7d1	severoatlantský
proudem	proud	k1gInSc7	proud
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
panuje	panovat	k5eAaImIp3nS	panovat
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
podle	podle	k7c2	podle
Köppenovy	Köppenův	k2eAgFnSc2d1	Köppenova
klasifikace	klasifikace	k1gFnSc2	klasifikace
podnebí	podnebí	k1gNnSc2	podnebí
mírné	mírný	k2eAgNnSc4d1	mírné
oceánické	oceánický	k2eAgNnSc4d1	oceánické
podnebí	podnebí	k1gNnSc4	podnebí
–	–	k?	–
klasifikace	klasifikace	k1gFnSc1	klasifikace
Cfb	Cfb	k1gFnSc1	Cfb
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
výkyvy	výkyv	k1gInPc4	výkyv
teplot	teplota	k1gFnPc2	teplota
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
nejsou	být	k5eNaImIp3nP	být
tak	tak	k9	tak
vysoké	vysoký	k2eAgFnPc1d1	vysoká
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
oblastech	oblast	k1gFnPc6	oblast
se	s	k7c7	s
srovnatelnou	srovnatelný	k2eAgFnSc7d1	srovnatelná
zeměpisnou	zeměpisný	k2eAgFnSc7d1	zeměpisná
šířkou	šířka	k1gFnSc7	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Mořský	mořský	k2eAgInSc1d1	mořský
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
teploty	teplota	k1gFnPc4	teplota
je	být	k5eAaImIp3nS	být
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Převládají	převládat	k5eAaImIp3nP	převládat
zde	zde	k6eAd1	zde
teplá	teplý	k2eAgNnPc4d1	teplé
léta	léto	k1gNnPc4	léto
a	a	k8xC	a
mírné	mírný	k2eAgFnPc4d1	mírná
zimy	zima	k1gFnPc4	zima
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
9	[number]	k4	9
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
stoupá	stoupat	k5eAaImIp3nS	stoupat
k	k	k7c3	k
15	[number]	k4	15
°	°	k?	°
<g/>
C.	C.	kA	C.
Roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
800	[number]	k4	800
do	do	k7c2	do
2800	[number]	k4	2800
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
prší	pršet	k5eAaImIp3nS	pršet
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
,	,	kIx,	,
západě	západ	k1gInSc6	západ
a	a	k8xC	a
jihozápadě	jihozápad	k1gInSc6	jihozápad
–	–	k?	–
na	na	k7c6	na
návětrné	návětrný	k2eAgFnSc6d1	návětrná
straně	strana	k1gFnSc6	strana
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
srážek	srážka	k1gFnPc2	srážka
spadne	spadnout	k5eAaPmIp3nS	spadnout
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nejméně	málo	k6eAd3	málo
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
léta	léto	k1gNnSc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Sníh	sníh	k1gInSc1	sníh
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
velmi	velmi	k6eAd1	velmi
neobvyklý	obvyklý	k2eNgInSc1d1	neobvyklý
<g/>
.	.	kIx.	.
</s>
<s>
Silné	silný	k2eAgNnSc1d1	silné
sněžení	sněžení	k1gNnSc1	sněžení
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
potíže	potíž	k1gFnPc4	potíž
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
či	či	k8xC	či
poškození	poškození	k1gNnSc6	poškození
přenosové	přenosový	k2eAgFnSc2d1	přenosová
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
však	však	k9	však
sníh	sníh	k1gInSc1	sníh
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
to	ten	k3xDgNnSc1	ten
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
pak	pak	k6eAd1	pak
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
a	a	k8xC	a
únoru	únor	k1gInSc6	únor
<g/>
;	;	kIx,	;
výjimečně	výjimečně	k6eAd1	výjimečně
byl	být	k5eAaImAgInS	být
sníh	sníh	k1gInSc1	sníh
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
i	i	k9	i
v	v	k7c6	v
září	září	k1gNnSc6	září
a	a	k8xC	a
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
doba	doba	k1gFnSc1	doba
se	s	k7c7	s
sněhovou	sněhový	k2eAgFnSc7d1	sněhová
pokrývkou	pokrývka	k1gFnSc7	pokrývka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
5	[number]	k4	5
dní	den	k1gInPc2	den
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
do	do	k7c2	do
24	[number]	k4	24
dní	den	k1gInPc2	den
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
délka	délka	k1gFnSc1	délka
trvání	trvání	k1gNnSc2	trvání
sněhové	sněhový	k2eAgFnSc2d1	sněhová
pokrývky	pokrývka	k1gFnSc2	pokrývka
hlubší	hluboký	k2eAgFnSc2d2	hlubší
než	než	k8xS	než
1	[number]	k4	1
cm	cm	kA	cm
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
se	se	k3xPyFc4	se
rok	rok	k1gInSc1	rok
od	od	k7c2	od
roku	rok	k1gInSc2	rok
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
jednoho	jeden	k4xCgInSc2	jeden
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
dnů	den	k1gInPc2	den
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
10	[number]	k4	10
dní	den	k1gInPc2	den
<g/>
;	;	kIx,	;
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
sněhovou	sněhový	k2eAgFnSc4d1	sněhová
pokrývku	pokrývka	k1gFnSc4	pokrývka
pak	pak	k6eAd1	pak
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
Wicklow	Wicklow	k1gFnSc4	Wicklow
Mountains	Mountainsa	k1gFnPc2	Mountainsa
–	–	k?	–
až	až	k9	až
30	[number]	k4	30
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
doba	doba	k1gFnSc1	doba
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
3,25	[number]	k4	3,25
<g/>
–	–	k?	–
<g/>
3,75	[number]	k4	3,75
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
celoroční	celoroční	k2eAgFnSc1d1	celoroční
doba	doba	k1gFnSc1	doba
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
1400	[number]	k4	1400
a	a	k8xC	a
1700	[number]	k4	1700
hodinami	hodina	k1gFnPc7	hodina
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
polovinu	polovina	k1gFnSc4	polovina
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
nad	nad	k7c7	nad
Irskem	Irsko	k1gNnSc7	Irsko
zatažená	zatažený	k2eAgFnSc1d1	zatažená
obloha	obloha	k1gFnSc1	obloha
–	–	k?	–
kvůli	kvůli	k7c3	kvůli
již	již	k6eAd1	již
zmíněnému	zmíněný	k2eAgInSc3d1	zmíněný
vlivu	vliv	k1gInSc3	vliv
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
přidružených	přidružený	k2eAgFnPc2d1	přidružená
tlakových	tlakový	k2eAgFnPc2d1	tlaková
níží	níže	k1gFnPc2	níže
<g/>
.	.	kIx.	.
</s>
<s>
Nejslunečnějšími	sluneční	k2eAgInPc7d3	nejslunečnější
měsíci	měsíc	k1gInPc7	měsíc
jsou	být	k5eAaImIp3nP	být
květen	květen	k1gInSc4	květen
a	a	k8xC	a
červen	červen	k1gInSc4	červen
s	s	k7c7	s
průměrným	průměrný	k2eAgInSc7d1	průměrný
denním	denní	k2eAgInSc7d1	denní
slunečním	sluneční	k2eAgInSc7d1	sluneční
svitem	svit	k1gInSc7	svit
mezi	mezi	k7c4	mezi
5	[number]	k4	5
a	a	k8xC	a
6,5	[number]	k4	6,5
hodinami	hodina	k1gFnPc7	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
dopadá	dopadat	k5eAaImIp3nS	dopadat
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
až	až	k9	až
7	[number]	k4	7
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejméně	málo	k6eAd3	málo
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
naopak	naopak	k6eAd1	naopak
na	na	k7c4	na
Irsko	Irsko	k1gNnSc4	Irsko
dopadá	dopadat	k5eAaImIp3nS	dopadat
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
<g/>
,	,	kIx,	,
hodnoty	hodnota	k1gFnPc1	hodnota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
hodiny	hodina	k1gFnSc2	hodina
na	na	k7c6	na
severu	sever	k1gInSc6	sever
po	po	k7c4	po
2	[number]	k4	2
hodiny	hodina	k1gFnSc2	hodina
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Irsko	Irsko	k1gNnSc1	Irsko
je	být	k5eAaImIp3nS	být
unitární	unitární	k2eAgFnSc1d1	unitární
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
parlament	parlament	k1gInSc1	parlament
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Oireachtas	Oireachtas	k1gInSc1	Oireachtas
<g/>
,	,	kIx,	,
horní	horní	k2eAgFnSc1d1	horní
komorou	komora	k1gFnSc7	komora
je	být	k5eAaImIp3nS	být
senát	senát	k1gInSc1	senát
–	–	k?	–
Seanad	Seanad	k1gInSc1	Seanad
Éireann	Éireanna	k1gFnPc2	Éireanna
a	a	k8xC	a
dolní	dolní	k2eAgFnSc7d1	dolní
komorou	komora	k1gFnSc7	komora
je	být	k5eAaImIp3nS	být
poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
–	–	k?	–
Dáil	Dáil	k1gMnSc1	Dáil
Éireann	Éireann	k1gMnSc1	Éireann
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
senát	senát	k1gInSc1	senát
má	mít	k5eAaImIp3nS	mít
60	[number]	k4	60
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
sněmovna	sněmovna	k1gFnSc1	sněmovna
má	mít	k5eAaImIp3nS	mít
166	[number]	k4	166
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
parlamentu	parlament	k1gInSc2	parlament
je	být	k5eAaImIp3nS	být
Leinster	Leinster	k1gInSc4	Leinster
House	house	k1gNnSc1	house
v	v	k7c6	v
Dublině	Dublin	k1gInSc6	Dublin
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Taoiseach	Taoiseach	k1gInSc1	Taoiseach
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
prezidentem	prezident	k1gMnSc7	prezident
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Dáilu	Dáil	k1gInSc2	Dáil
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgInSc7d1	současný
Taoiseachem	Taoiseach	k1gInSc7	Taoiseach
je	být	k5eAaImIp3nS	být
Enda	Enda	k1gFnSc1	Enda
Kenny	Kenna	k1gMnSc2	Kenna
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Fine	Fin	k1gMnSc5	Fin
Gael	Gael	k1gMnSc1	Gael
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
státu	stát	k1gInSc2	stát
stojí	stát	k5eAaImIp3nS	stát
prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
irsky	irsky	k6eAd1	irsky
Uachtarán	Uachtarán	k1gInSc1	Uachtarán
na	na	k7c4	na
hÉireann	hÉireann	k1gNnSc4	hÉireann
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
volený	volený	k2eAgInSc1d1	volený
na	na	k7c4	na
sedmileté	sedmiletý	k2eAgNnSc4d1	sedmileté
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
–	–	k?	–
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
na	na	k7c4	na
maximálně	maximálně	k6eAd1	maximálně
dvě	dva	k4xCgNnPc4	dva
období	období	k1gNnPc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
prezidenta	prezident	k1gMnSc2	prezident
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
reprezentativní	reprezentativní	k2eAgNnSc1d1	reprezentativní
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
prezidentem	prezident	k1gMnSc7	prezident
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
Michael	Michaela	k1gFnPc2	Michaela
D.	D.	kA	D.
Higgins	Higgins	k1gInSc1	Higgins
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Administrativní	administrativní	k2eAgNnPc4d1	administrativní
dělení	dělení	k1gNnPc4	dělení
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
Irské	irský	k2eAgFnSc2d1	irská
provincie	provincie	k1gFnSc2	provincie
a	a	k8xC	a
Irská	irský	k2eAgNnPc4d1	irské
hrabství	hrabství	k1gNnPc4	hrabství
<g/>
.	.	kIx.	.
</s>
<s>
Irsko	Irsko	k1gNnSc1	Irsko
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
provincií	provincie	k1gFnPc2	provincie
–	–	k?	–
Connacht	Connachta	k1gFnPc2	Connachta
(	(	kIx(	(
<g/>
irsky	irsky	k6eAd1	irsky
Connachta	Connacht	k1gMnSc2	Connacht
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Leinster	Leinster	k1gMnSc1	Leinster
(	(	kIx(	(
<g/>
irsky	irsky	k6eAd1	irsky
Laighin	Laighin	k2eAgInSc1d1	Laighin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Munster	Munster	k1gMnSc1	Munster
(	(	kIx(	(
<g/>
irsky	irsky	k6eAd1	irsky
an	an	k?	an
Mhumhain	Mhumhain	k1gInSc1	Mhumhain
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ulster	Ulster	k1gInSc4	Ulster
(	(	kIx(	(
<g/>
irsky	irsky	k6eAd1	irsky
Uladh	Uladh	k1gInSc1	Uladh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
též	též	k9	též
existovala	existovat	k5eAaImAgFnS	existovat
provincie	provincie	k1gFnSc1	provincie
Meath	Meath	k1gMnSc1	Meath
(	(	kIx(	(
<g/>
irsky	irsky	k6eAd1	irsky
an	an	k?	an
Mhí	Mhí	k1gFnSc1	Mhí
<g/>
)	)	kIx)	)
–	–	k?	–
tzv.	tzv.	kA	tzv.
Střední	střední	k2eAgNnSc1d1	střední
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
Provincie	provincie	k1gFnSc1	provincie
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělily	dělit	k5eAaImAgFnP	dělit
na	na	k7c4	na
32	[number]	k4	32
hrabství	hrabství	k1gNnPc2	hrabství
pro	pro	k7c4	pro
administrativní	administrativní	k2eAgFnPc4d1	administrativní
potřeby	potřeba	k1gFnPc4	potřeba
britské	britský	k2eAgFnSc2d1	britská
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
hrabství	hrabství	k1gNnPc2	hrabství
Ulsteru	Ulster	k1gInSc2	Ulster
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
pod	pod	k7c7	pod
britskou	britský	k2eAgFnSc7d1	britská
správou	správa	k1gFnSc7	správa
díky	díky	k7c3	díky
referendu	referendum	k1gNnSc3	referendum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Zbylých	zbylý	k2eAgNnPc2d1	zbylé
26	[number]	k4	26
hrabství	hrabství	k1gNnPc2	hrabství
nyní	nyní	k6eAd1	nyní
tvoří	tvořit	k5eAaImIp3nP	tvořit
Irskou	irský	k2eAgFnSc4d1	irská
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
Zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
samosprávné	samosprávný	k2eAgNnSc4d1	samosprávné
postavení	postavení	k1gNnSc4	postavení
mají	mít	k5eAaImIp3nP	mít
města	město	k1gNnSc2	město
Dublin	Dublin	k1gInSc1	Dublin
<g/>
,	,	kIx,	,
Cork	Cork	k1gInSc1	Cork
<g/>
,	,	kIx,	,
Limerick	Limerick	k1gInSc1	Limerick
<g/>
,	,	kIx,	,
Galway	Galway	k1gInPc1	Galway
a	a	k8xC	a
Waterford	Waterford	k1gInSc1	Waterford
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
hrabství	hrabství	k1gNnSc1	hrabství
Dublin	Dublin	k1gInSc1	Dublin
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
Dún	Dún	k1gFnSc4	Dún
Laoghaire	Laoghair	k1gInSc5	Laoghair
–	–	k?	–
Rathdown	Rathdown	k1gInSc1	Rathdown
<g/>
,	,	kIx,	,
Fingal	Fingal	k1gInSc1	Fingal
a	a	k8xC	a
Jižní	jižní	k2eAgInSc1d1	jižní
Dublin	Dublin	k1gInSc1	Dublin
<g/>
.	.	kIx.	.
</s>
<s>
Hrabství	hrabství	k1gNnSc1	hrabství
Tipperary	Tipperara	k1gFnSc2	Tipperara
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
dělilo	dělit	k5eAaImAgNnS	dělit
na	na	k7c4	na
Severní	severní	k2eAgFnSc4d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Tipperary	Tipperara	k1gFnSc2	Tipperara
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
celistvé	celistvý	k2eAgNnSc1d1	celistvé
<g/>
.	.	kIx.	.
</s>
<s>
Kód	kód	k1gInSc1	kód
popisující	popisující	k2eAgNnPc4d1	popisující
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
hrabství	hrabství	k1gNnPc4	hrabství
je	být	k5eAaImIp3nS	být
ISO	ISO	kA	ISO
3166	[number]	k4	3166
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
IE.	IE.	k1gFnSc1	IE.
Pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
hrabství	hrabství	k1gNnSc4	hrabství
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
IE-AA	IE-AA	k1gFnSc2	IE-AA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
AA	AA	kA	AA
je	být	k5eAaImIp3nS	být
jedno-	jedno-	k?	jedno-
či	či	k8xC	či
dvoupísmenná	dvoupísmenný	k2eAgFnSc1d1	dvoupísmenná
zkratka	zkratka	k1gFnSc1	zkratka
hrabství	hrabství	k1gNnSc2	hrabství
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
nejméně	málo	k6eAd3	málo
rozšířeným	rozšířený	k2eAgNnSc7d1	rozšířené
odvětvím	odvětví	k1gNnSc7	odvětví
irského	irský	k2eAgNnSc2d1	irské
hospodářství	hospodářství	k1gNnSc2	hospodářství
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
asi	asi	k9	asi
5	[number]	k4	5
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
:	:	kIx,	:
služby	služba	k1gFnSc2	služba
49	[number]	k4	49
%	%	kIx~	%
a	a	k8xC	a
průmysl	průmysl	k1gInSc1	průmysl
46	[number]	k4	46
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Irsko	Irsko	k1gNnSc1	Irsko
má	mít	k5eAaImIp3nS	mít
nepříliš	příliš	k6eNd1	příliš
významné	významný	k2eAgNnSc4d1	významné
nerostné	nerostný	k2eAgNnSc4d1	nerostné
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Těží	těžet	k5eAaImIp3nS	těžet
se	se	k3xPyFc4	se
především	především	k9	především
olovo	olovo	k1gNnSc1	olovo
a	a	k8xC	a
zinek	zinek	k1gInSc1	zinek
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
rašelina	rašelina	k1gFnSc1	rašelina
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgInSc1d1	stavební
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc1	stříbro
a	a	k8xC	a
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
se	se	k3xPyFc4	se
především	především	k9	především
průmysl	průmysl	k1gInSc1	průmysl
chemický	chemický	k2eAgInSc1d1	chemický
a	a	k8xC	a
petrochemický	petrochemický	k2eAgInSc1d1	petrochemický
<g/>
,	,	kIx,	,
elektronický	elektronický	k2eAgInSc1d1	elektronický
<g/>
,	,	kIx,	,
kovozpracující	kovozpracující	k2eAgInSc1d1	kovozpracující
<g/>
,	,	kIx,	,
nábytkářský	nábytkářský	k2eAgInSc1d1	nábytkářský
a	a	k8xC	a
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
(	(	kIx(	(
<g/>
Guinness	Guinness	k1gInSc1	Guinness
je	být	k5eAaImIp3nS	být
šestým	šestý	k4xOgInSc7	šestý
největším	veliký	k2eAgInSc7d3	veliký
pivovarem	pivovar	k1gInSc7	pivovar
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
dopravní	dopravní	k2eAgInPc4d1	dopravní
prostředky	prostředek	k1gInPc4	prostředek
a	a	k8xC	a
počítače	počítač	k1gInPc4	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
odvětvím	odvětví	k1gNnSc7	odvětví
zemědělství	zemědělství	k1gNnSc2	zemědělství
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgInSc4d1	tradiční
chov	chov	k1gInSc4	chov
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
ovčí	ovčí	k2eAgFnSc1d1	ovčí
vlna	vlna	k1gFnSc1	vlna
je	být	k5eAaImIp3nS	být
vyvážena	vyvážit	k5eAaPmNgFnS	vyvážit
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
také	také	k9	také
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
brambory	brambora	k1gFnPc1	brambora
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Genetické	genetický	k2eAgInPc1d1	genetický
výzkumy	výzkum	k1gInPc1	výzkum
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgMnPc1	první
osadníci	osadník	k1gMnPc1	osadník
Irska	Irsko	k1gNnSc2	Irsko
migrovali	migrovat	k5eAaImAgMnP	migrovat
z	z	k7c2	z
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
během	během	k7c2	během
konce	konec	k1gInSc2	konec
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mezolitu	mezolit	k1gInSc6	mezolit
přinesli	přinést	k5eAaPmAgMnP	přinést
migranti	migrant	k1gMnPc1	migrant
z	z	k7c2	z
neolitu	neolit	k1gInSc2	neolit
a	a	k8xC	a
doby	doba	k1gFnPc4	doba
bronzové	bronzový	k2eAgFnPc4d1	bronzová
do	do	k7c2	do
Irska	Irsko	k1gNnSc2	Irsko
keltskou	keltský	k2eAgFnSc4d1	keltská
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Kultura	kultura	k1gFnSc1	kultura
se	se	k3xPyFc4	se
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
po	po	k7c6	po
ostrově	ostrov	k1gInSc6	ostrov
a	a	k8xC	a
gaelské	gaelský	k2eAgFnPc1d1	gaelská
tradice	tradice	k1gFnPc1	tradice
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
převládající	převládající	k2eAgMnSc1d1	převládající
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
mají	mít	k5eAaImIp3nP	mít
Irové	Ir	k1gMnPc1	Ir
převážně	převážně	k6eAd1	převážně
gaelský	gaelský	k2eAgInSc4d1	gaelský
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestože	přestože	k8xS	přestože
část	část	k1gFnSc1	část
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
norského	norský	k2eAgInSc2d1	norský
<g/>
,	,	kIx,	,
anglonormanského	anglonormanský	k2eAgInSc2d1	anglonormanský
<g/>
,	,	kIx,	,
anglického	anglický	k2eAgInSc2d1	anglický
<g/>
,	,	kIx,	,
skotského	skotský	k2eAgInSc2d1	skotský
<g/>
,	,	kIx,	,
francouzského	francouzský	k2eAgInSc2d1	francouzský
a	a	k8xC	a
velšského	velšský	k2eAgInSc2d1	velšský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
netvoří	tvořit	k5eNaImIp3nP	tvořit
výrazné	výrazný	k2eAgFnPc4d1	výrazná
menšiny	menšina	k1gFnPc4	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Gaelská	Gaelský	k2eAgFnSc1d1	Gaelská
kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
jazyky	jazyk	k1gInPc1	jazyk
tvoří	tvořit	k5eAaImIp3nP	tvořit
významnou	významný	k2eAgFnSc4d1	významná
část	část	k1gFnSc4	část
národní	národní	k2eAgFnSc2d1	národní
identity	identita	k1gFnSc2	identita
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
jsou	být	k5eAaImIp3nP	být
irští	irský	k2eAgMnPc1d1	irský
kočovníci	kočovník	k1gMnPc1	kočovník
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Travellers	Travellers	k1gInSc1	Travellers
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
etnická	etnický	k2eAgFnSc1d1	etnická
menšina	menšina	k1gFnSc1	menšina
<g/>
,	,	kIx,	,
politicky	politicky	k6eAd1	politicky
spojeni	spojit	k5eAaPmNgMnP	spojit
s	s	k7c7	s
pevninskými	pevninský	k2eAgFnPc7d1	pevninská
romskými	romský	k2eAgFnPc7d1	romská
a	a	k8xC	a
cikánskými	cikánský	k2eAgFnPc7d1	cikánská
skupinami	skupina	k1gFnPc7	skupina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
ne	ne	k9	ne
–	–	k?	–
tam	tam	k6eAd1	tam
jsou	být	k5eAaImIp3nP	být
klasifikováni	klasifikovat	k5eAaImNgMnP	klasifikovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
sociální	sociální	k2eAgFnSc1d1	sociální
skupina	skupina	k1gFnSc1	skupina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Irsko	Irsko	k1gNnSc1	Irsko
má	mít	k5eAaImIp3nS	mít
nejrychleji	rychle	k6eAd3	rychle
rostoucí	rostoucí	k2eAgNnSc1d1	rostoucí
(	(	kIx(	(
<g/>
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
<g/>
)	)	kIx)	)
populaci	populace	k1gFnSc4	populace
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
2,5	[number]	k4	2,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
rok	rok	k1gInSc4	rok
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
byl	být	k5eAaImAgInS	být
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
2	[number]	k4	2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
rychlý	rychlý	k2eAgInSc1d1	rychlý
růst	růst	k1gInSc1	růst
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přičten	přičten	k2eAgInSc4d1	přičten
klesající	klesající	k2eAgFnSc4d1	klesající
úmrtnosti	úmrtnost	k1gFnPc4	úmrtnost
<g/>
,	,	kIx,	,
zvyšující	zvyšující	k2eAgFnPc4d1	zvyšující
se	se	k3xPyFc4	se
porodnosti	porodnost	k1gFnSc2	porodnost
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgFnSc3d1	vysoká
imigraci	imigrace	k1gFnSc3	imigrace
–	–	k?	–
imigranti	imigrant	k1gMnPc1	imigrant
nyní	nyní	k6eAd1	nyní
tvoří	tvořit	k5eAaImIp3nP	tvořit
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
%	%	kIx~	%
irské	irský	k2eAgFnSc2d1	irská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
faktorem	faktor	k1gInSc7	faktor
růstu	růst	k1gInSc2	růst
je	být	k5eAaImIp3nS	být
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Irsko	Irsko	k1gNnSc1	Irsko
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
zemí	zem	k1gFnPc2	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Maltou	Malta	k1gFnSc7	Malta
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
zakázány	zakázán	k2eAgInPc1d1	zakázán
potraty	potrat	k1gInPc1	potrat
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
povoleny	povolen	k2eAgInPc4d1	povolen
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ohrožení	ohrožení	k1gNnSc2	ohrožení
života	život	k1gInSc2	život
matky	matka	k1gFnSc2	matka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInPc1d1	oficiální
jazyky	jazyk	k1gInPc1	jazyk
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
jsou	být	k5eAaImIp3nP	být
irština	irština	k1gFnSc1	irština
a	a	k8xC	a
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
převládá	převládat	k5eAaImIp3nS	převládat
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Lidí	člověk	k1gMnPc2	člověk
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
irsky	irsky	k6eAd1	irsky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
komunitách	komunita	k1gFnPc6	komunita
<g/>
,	,	kIx,	,
Gaeltachtech	Gaeltacht	k1gInPc6	Gaeltacht
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
pouhé	pouhý	k2eAgInPc4d1	pouhý
desetitisíce	desetitisíce	k1gInPc4	desetitisíce
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
izolovaní	izolovaný	k2eAgMnPc1d1	izolovaný
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Silniční	silniční	k2eAgFnPc1d1	silniční
značky	značka	k1gFnPc1	značka
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
dvojjazyčné	dvojjazyčný	k2eAgInPc1d1	dvojjazyčný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
Gaeltachtech	Gaeltacht	k1gInPc6	Gaeltacht
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
irsky	irsky	k6eAd1	irsky
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
veřejných	veřejný	k2eAgFnPc2d1	veřejná
vyhlášek	vyhláška	k1gFnPc2	vyhláška
a	a	k8xC	a
tisknutých	tisknutý	k2eAgNnPc2d1	tisknuté
médií	médium	k1gNnPc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vládních	vládní	k2eAgFnPc2d1	vládní
publikací	publikace	k1gFnPc2	publikace
a	a	k8xC	a
formulářů	formulář	k1gInPc2	formulář
je	být	k5eAaImIp3nS	být
jak	jak	k6eAd1	jak
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
irštině	irština	k1gFnSc6	irština
<g/>
,	,	kIx,	,
a	a	k8xC	a
občané	občan	k1gMnPc1	občan
mohou	moct	k5eAaImIp3nP	moct
se	s	k7c7	s
státem	stát	k1gInSc7	stát
jednat	jednat	k5eAaImF	jednat
i	i	k9	i
v	v	k7c6	v
irštině	irština	k1gFnSc6	irština
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
656	[number]	k4	656
790	[number]	k4	790
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
39	[number]	k4	39
%	%	kIx~	%
<g/>
)	)	kIx)	)
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
schopno	schopen	k2eAgNnSc1d1	schopno
domluvit	domluvit	k5eAaPmF	domluvit
se	se	k3xPyFc4	se
irsky	irsky	k6eAd1	irsky
<g/>
;	;	kIx,	;
přestože	přestože	k8xS	přestože
není	být	k5eNaImIp3nS	být
dostupný	dostupný	k2eAgInSc4d1	dostupný
žádný	žádný	k3yNgInSc4	žádný
průzkum	průzkum	k1gInSc4	průzkum
pro	pro	k7c4	pro
anglicky	anglicky	k6eAd1	anglicky
mluvící	mluvící	k2eAgFnSc4d1	mluvící
<g/>
,	,	kIx,	,
nejspíše	nejspíše	k9	nejspíše
jich	on	k3xPp3gInPc2	on
bude	být	k5eAaImBp3nS	být
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Irská	irský	k2eAgFnSc1d1	irská
populace	populace	k1gFnSc1	populace
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
podstatně	podstatně	k6eAd1	podstatně
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
tohoto	tento	k3xDgInSc2	tento
populačního	populační	k2eAgInSc2d1	populační
růstu	růst	k1gInSc2	růst
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
připisováno	připisovat	k5eAaImNgNnS	připisovat
imigrantům	imigrant	k1gMnPc3	imigrant
a	a	k8xC	a
vracejícím	vracející	k2eAgMnPc3d1	vracející
se	se	k3xPyFc4	se
Irům	Ir	k1gMnPc3	Ir
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
narozenými	narozený	k2eAgFnPc7d1	narozená
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
počtu	počet	k1gInSc6	počet
emigrovali	emigrovat	k5eAaBmAgMnP	emigrovat
v	v	k7c6	v
dřívějších	dřívější	k2eAgNnPc6d1	dřívější
letech	léto	k1gNnPc6	léto
velké	velký	k2eAgFnSc2d1	velká
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
porodnost	porodnost	k1gFnSc1	porodnost
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
dvakrát	dvakrát	k6eAd1	dvakrát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
velmi	velmi	k6eAd1	velmi
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
%	%	kIx~	%
irského	irský	k2eAgNnSc2d1	irské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
tvoří	tvořit	k5eAaImIp3nP	tvořit
obyvatelé	obyvatel	k1gMnPc1	obyvatel
cizí	cizí	k2eAgFnSc2d1	cizí
národnosti	národnost	k1gFnSc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Central	Centrat	k5eAaPmAgInS	Centrat
Statistics	Statistics	k1gInSc1	Statistics
Office	Office	kA	Office
publikovala	publikovat	k5eAaBmAgNnP	publikovat
předběžná	předběžný	k2eAgNnPc1d1	předběžné
zjištění	zjištění	k1gNnPc1	zjištění
z	z	k7c2	z
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Ukazují	ukazovat	k5eAaImIp3nP	ukazovat
toto	tento	k3xDgNnSc4	tento
<g/>
:	:	kIx,	:
Celková	celkový	k2eAgFnSc1d1	celková
populace	populace	k1gFnSc1	populace
Irska	Irsko	k1gNnSc2	Irsko
byla	být	k5eAaImAgFnS	být
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
4	[number]	k4	4
234	[number]	k4	234
925	[number]	k4	925
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
317	[number]	k4	317
722	[number]	k4	722
(	(	kIx(	(
<g/>
8,1	[number]	k4	8,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
nenárodních	národní	k2eNgMnPc2d1	nenárodní
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
občané	občan	k1gMnPc1	občan
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
cizí	cizí	k2eAgFnSc2d1	cizí
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
je	být	k5eAaImIp3nS	být
419	[number]	k4	419
733	[number]	k4	733
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
10	[number]	k4	10
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
1	[number]	k4	1
318	[number]	k4	318
lidí	člověk	k1gMnPc2	člověk
"	"	kIx"	"
<g/>
bez	bez	k7c2	bez
národnosti	národnost	k1gFnSc2	národnost
<g/>
"	"	kIx"	"
a	a	k8xC	a
44	[number]	k4	44
279	[number]	k4	279
lidí	člověk	k1gMnPc2	člověk
bez	bez	k7c2	bez
uvedené	uvedený	k2eAgFnSc2d1	uvedená
národnosti	národnost	k1gFnSc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
imigrantů	imigrant	k1gMnPc2	imigrant
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
(	(	kIx(	(
<g/>
112	[number]	k4	112
548	[number]	k4	548
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následováno	následován	k2eAgNnSc4d1	následováno
Polskem	Polsko	k1gNnSc7	Polsko
(	(	kIx(	(
<g/>
63	[number]	k4	63
267	[number]	k4	267
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Litvou	Litva	k1gFnSc7	Litva
(	(	kIx(	(
<g/>
24	[number]	k4	24
628	[number]	k4	628
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nigérií	Nigérie	k1gFnSc7	Nigérie
(	(	kIx(	(
<g/>
16	[number]	k4	16
300	[number]	k4	300
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lotyšskem	Lotyšsko	k1gNnSc7	Lotyšsko
(	(	kIx(	(
<g/>
13	[number]	k4	13
319	[number]	k4	319
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
(	(	kIx(	(
<g/>
12	[number]	k4	12
475	[number]	k4	475
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čínou	Čína	k1gFnSc7	Čína
(	(	kIx(	(
<g/>
11	[number]	k4	11
161	[number]	k4	161
<g/>
)	)	kIx)	)
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
(	(	kIx(	(
<g/>
10	[number]	k4	10
289	[number]	k4	289
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
naposledy	naposledy	k6eAd1	naposledy
překonána	překonat	k5eAaPmNgFnS	překonat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
bylo	být	k5eAaImAgNnS	být
4,4	[number]	k4	4,4
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgFnSc1d3	nejmenší
populace	populace	k1gFnSc1	populace
Irska	Irsko	k1gNnSc2	Irsko
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
–	–	k?	–
2,8	[number]	k4	2,8
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
provincie	provincie	k1gFnPc1	provincie
Irska	Irsko	k1gNnSc2	Irsko
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
nárůst	nárůst	k1gInSc4	nárůst
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
Leinsteru	Leinster	k1gInSc2	Leinster
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
o	o	k7c4	o
8,9	[number]	k4	8,9
%	%	kIx~	%
a	a	k8xC	a
populace	populace	k1gFnPc1	populace
Munsteru	Munster	k1gInSc2	Munster
o	o	k7c4	o
6,5	[number]	k4	6,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Irsko	Irsko	k1gNnSc1	Irsko
má	mít	k5eAaImIp3nS	mít
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
literární	literární	k2eAgFnSc4d1	literární
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
Irem	Ir	k1gMnSc7	Ir
vůbec	vůbec	k9	vůbec
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
spisovatel	spisovatel	k1gMnSc1	spisovatel
James	James	k1gMnSc1	James
Joyce	Joyce	k1gMnSc1	Joyce
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgMnPc2d1	klíčový
představitelů	představitel	k1gMnPc2	představitel
modernismu	modernismus	k1gInSc2	modernismus
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těsném	těsný	k2eAgInSc6d1	těsný
závěsu	závěs	k1gInSc6	závěs
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nP	držet
spisovatelé	spisovatel	k1gMnPc1	spisovatel
Oscar	Oscar	k1gMnSc1	Oscar
Wilde	Wild	k1gInSc5	Wild
<g/>
,	,	kIx,	,
George	Georg	k1gMnPc4	Georg
Bernard	Bernard	k1gMnSc1	Bernard
Shaw	Shaw	k1gMnSc1	Shaw
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Butler	Butler	k1gInSc1	Butler
Yeats	Yeats	k1gInSc1	Yeats
<g/>
,	,	kIx,	,
Samuel	Samuel	k1gMnSc1	Samuel
Beckett	Beckett	k1gMnSc1	Beckett
<g/>
,	,	kIx,	,
Jonathan	Jonathan	k1gMnSc1	Jonathan
Swift	Swift	k1gMnSc1	Swift
a	a	k8xC	a
Laurence	Laurence	k1gFnSc1	Laurence
Sterne	Stern	k1gInSc5	Stern
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
jmenovaní	jmenovaný	k1gMnPc1	jmenovaný
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
světového	světový	k2eAgInSc2d1	světový
literárního	literární	k2eAgInSc2d1	literární
kánonu	kánon	k1gInSc2	kánon
<g/>
.	.	kIx.	.
</s>
<s>
Seamus	Seamus	k1gInSc1	Seamus
Heaney	Heanea	k1gFnSc2	Heanea
získal	získat	k5eAaPmAgInS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovou	klíčový	k2eAgFnSc7d1	klíčová
postavou	postava	k1gFnSc7	postava
irského	irský	k2eAgNnSc2d1	irské
literárního	literární	k2eAgNnSc2d1	literární
obrození	obrození	k1gNnSc2	obrození
byl	být	k5eAaImAgMnS	být
John	John	k1gMnSc1	John
Millington	Millington	k1gInSc4	Millington
Synge	Syng	k1gInSc2	Syng
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
éře	éra	k1gFnSc6	éra
romantismu	romantismus	k1gInSc2	romantismus
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
básník	básník	k1gMnSc1	básník
Thomas	Thomas	k1gMnSc1	Thomas
Moore	Moor	k1gMnSc5	Moor
<g/>
.	.	kIx.	.
</s>
<s>
Seán	Seán	k1gNnSc4	Seán
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Casey	Casea	k1gFnSc2	Casea
psal	psát	k5eAaImAgMnS	psát
dramata	drama	k1gNnPc4	drama
z	z	k7c2	z
chudinských	chudinský	k2eAgFnPc2d1	chudinská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Brendan	Brendan	k1gMnSc1	Brendan
Behan	Behan	k1gMnSc1	Behan
byl	být	k5eAaImAgMnS	být
i	i	k8xC	i
republikánským	republikánský	k2eAgMnSc7d1	republikánský
politickým	politický	k2eAgMnSc7d1	politický
aktivistou	aktivista	k1gMnSc7	aktivista
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
Iris	iris	k1gFnSc1	iris
Murdochová	Murdochová	k1gFnSc1	Murdochová
<g/>
.	.	kIx.	.
</s>
<s>
Irové	Ir	k1gMnPc1	Ir
zasáhli	zasáhnout	k5eAaPmAgMnP	zasáhnout
ovšem	ovšem	k9	ovšem
i	i	k9	i
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
literatury	literatura	k1gFnSc2	literatura
populární	populární	k2eAgFnSc2d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Bram	brama	k1gFnPc2	brama
Stoker	Stoker	k1gMnSc1	Stoker
stvořil	stvořit	k5eAaPmAgMnS	stvořit
slavnou	slavný	k2eAgFnSc4d1	slavná
postavu	postava	k1gFnSc4	postava
Drákuly	Drákula	k1gMnSc2	Drákula
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
autor	autor	k1gMnSc1	autor
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
Eoin	Eoin	k1gMnSc1	Eoin
Colfer	Colfer	k1gMnSc1	Colfer
<g/>
.	.	kIx.	.
</s>
<s>
Klasikem	klasik	k1gMnSc7	klasik
dobrodružné	dobrodružný	k2eAgFnSc2d1	dobrodružná
literatury	literatura	k1gFnSc2	literatura
je	být	k5eAaImIp3nS	být
Thomas	Thomas	k1gMnSc1	Thomas
Mayne-Reid	Mayne-Reid	k1gInSc1	Mayne-Reid
<g/>
.	.	kIx.	.
</s>
<s>
Klasickými	klasický	k2eAgFnPc7d1	klasická
duchařskými	duchařský	k2eAgFnPc7d1	duchařská
historiemi	historie	k1gFnPc7	historie
proslul	proslout	k5eAaPmAgMnS	proslout
Sheridan	Sheridan	k1gMnSc1	Sheridan
Le	Le	k1gMnSc1	Le
Fanu	Fana	k1gFnSc4	Fana
<g/>
.	.	kIx.	.
</s>
<s>
Dosti	dosti	k6eAd1	dosti
úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
jsou	být	k5eAaImIp3nP	být
Irové	Ir	k1gMnPc1	Ir
v	v	k7c6	v
globální	globální	k2eAgFnSc3d1	globální
populární	populární	k2eAgFnSc3d1	populární
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Bono	bona	k1gFnSc5	bona
Vox	Vox	k1gFnSc5	Vox
<g/>
,	,	kIx,	,
lídr	lídr	k1gMnSc1	lídr
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
skupiny	skupina	k1gFnSc2	skupina
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Enya	Enya	k1gFnSc1	Enya
<g/>
,	,	kIx,	,
Sinéad	Sinéad	k1gInSc1	Sinéad
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Connor	Connor	k1gMnSc1	Connor
<g/>
,	,	kIx,	,
Bob	Bob	k1gMnSc1	Bob
Geldof	Geldof	k1gMnSc1	Geldof
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
Cranberries	Cranberries	k1gInSc1	Cranberries
se	s	k7c7	s
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Dolores	Dolores	k1gInSc4	Dolores
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Riordanovou	Riordanová	k1gFnSc4	Riordanová
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
The	The	k1gFnPc2	The
Corrs	Corrs	k1gInSc4	Corrs
s	s	k7c7	s
Andreou	Andrea	k1gFnSc7	Andrea
Corrovou	Corrová	k1gFnSc7	Corrová
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejpopulárnějším	populární	k2eAgMnPc3d3	nejpopulárnější
umělcům	umělec	k1gMnPc3	umělec
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
slavné	slavný	k2eAgFnSc2d1	slavná
chlapecké	chlapecký	k2eAgFnSc2d1	chlapecká
kapely	kapela	k1gFnSc2	kapela
Boyzone	Boyzon	k1gInSc5	Boyzon
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
nejvíce	nejvíce	k6eAd1	nejvíce
proslavili	proslavit	k5eAaPmAgMnP	proslavit
Ronan	Ronan	k1gMnSc1	Ronan
Keating	Keating	k1gInSc1	Keating
a	a	k8xC	a
Stephen	Stephen	k1gInSc1	Stephen
Gately	Gatela	k1gFnSc2	Gatela
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stopách	stopa	k1gFnPc6	stopa
Boyzone	Boyzon	k1gInSc5	Boyzon
jde	jít	k5eAaImIp3nS	jít
dnes	dnes	k6eAd1	dnes
skupina	skupina	k1gFnSc1	skupina
Westlife	Westlif	k1gMnSc5	Westlif
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
světovým	světový	k2eAgInSc7d1	světový
hitem	hit	k1gInSc7	hit
Take	Tak	k1gFnSc2	Tak
Me	Me	k1gFnSc1	Me
to	ten	k3xDgNnSc4	ten
Church	Church	k1gMnSc1	Church
upozornil	upozornit	k5eAaPmAgMnS	upozornit
zpěvák	zpěvák	k1gMnSc1	zpěvák
Hozier	Hozier	k1gMnSc1	Hozier
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
rockový	rockový	k2eAgMnSc1d1	rockový
kytarista	kytarista	k1gMnSc1	kytarista
proslul	proslout	k5eAaPmAgMnS	proslout
Gary	Gara	k1gFnPc4	Gara
Moore	Moor	k1gMnSc5	Moor
<g/>
.	.	kIx.	.
</s>
<s>
Legendou	legenda	k1gFnSc7	legenda
soulové	soulový	k2eAgFnSc2d1	soulová
hudby	hudba	k1gFnSc2	hudba
je	být	k5eAaImIp3nS	být
Van	van	k1gInSc4	van
Morrison	Morrisona	k1gFnPc2	Morrisona
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
programem	program	k1gInSc7	program
Lord	lord	k1gMnSc1	lord
of	of	k?	of
the	the	k?	the
Dance	Danka	k1gFnSc6	Danka
sbírá	sbírat	k5eAaImIp3nS	sbírat
úspěchy	úspěch	k1gInPc4	úspěch
tanečník	tanečník	k1gMnSc1	tanečník
Michael	Michael	k1gMnSc1	Michael
Flatley	Flatlea	k1gFnSc2	Flatlea
<g/>
.	.	kIx.	.
</s>
<s>
Slavným	slavný	k2eAgMnSc7d1	slavný
harfeníkem	harfeník	k1gMnSc7	harfeník
(	(	kIx(	(
<g/>
harfa	harfa	k1gFnSc1	harfa
byla	být	k5eAaImAgFnS	být
tradičním	tradiční	k2eAgInSc7d1	tradiční
národním	národní	k2eAgInSc7d1	národní
hudebním	hudební	k2eAgInSc7d1	hudební
nástrojem	nástroj	k1gInSc7	nástroj
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
irském	irský	k2eAgInSc6d1	irský
státním	státní	k2eAgInSc6d1	státní
znaku	znak	k1gInSc6	znak
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
Turlough	Turlough	k1gMnSc1	Turlough
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Carolan	Carolan	k1gMnSc1	Carolan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
prosadil	prosadit	k5eAaPmAgMnS	prosadit
Charles	Charles	k1gMnSc1	Charles
Villiers	Villiersa	k1gFnPc2	Villiersa
Stanford	Stanford	k1gMnSc1	Stanford
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
moderním	moderní	k2eAgMnSc7d1	moderní
malířem	malíř	k1gMnSc7	malíř
a	a	k8xC	a
představitelem	představitel	k1gMnSc7	představitel
tzv.	tzv.	kA	tzv.
nové	nový	k2eAgFnSc2d1	nová
figurace	figurace	k1gFnSc2	figurace
je	být	k5eAaImIp3nS	být
Francis	Francis	k1gFnSc4	Francis
Bacon	Bacona	k1gFnPc2	Bacona
<g/>
.	.	kIx.	.
</s>
<s>
Architekt	architekt	k1gMnSc1	architekt
Kevin	Kevin	k1gMnSc1	Kevin
Roche	Roche	k1gNnSc4	Roche
získal	získat	k5eAaPmAgMnS	získat
prestižní	prestižní	k2eAgFnSc4d1	prestižní
Pritzkerovu	Pritzkerův	k2eAgFnSc4d1	Pritzkerova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Držitelem	držitel	k1gMnSc7	držitel
Oscara	Oscar	k1gMnSc4	Oscar
je	být	k5eAaImIp3nS	být
režisér	režisér	k1gMnSc1	režisér
Neil	Neil	k1gMnSc1	Neil
Jordan	Jordan	k1gMnSc1	Jordan
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějšími	známý	k2eAgMnPc7d3	nejznámější
herci	herec	k1gMnPc7	herec
jsou	být	k5eAaImIp3nP	být
Pierce	Pierce	k1gMnSc1	Pierce
Brosnan	Brosnan	k1gMnSc1	Brosnan
<g/>
,	,	kIx,	,
Liam	Liam	k1gMnSc1	Liam
Neeson	Neeson	k1gMnSc1	Neeson
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Harris	Harris	k1gFnSc2	Harris
a	a	k8xC	a
Jonathan	Jonathan	k1gMnSc1	Jonathan
Rhys	Rhysa	k1gFnPc2	Rhysa
Meyers	Meyersa	k1gFnPc2	Meyersa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zlaté	zlatý	k2eAgFnSc6d1	zlatá
éře	éra	k1gFnSc6	éra
Hollywoodu	Hollywood	k1gInSc2	Hollywood
zářila	zářit	k5eAaImAgFnS	zářit
Maureen	Maureen	k1gInSc4	Maureen
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Harová	Harová	k1gFnSc1	Harová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
William	William	k1gInSc1	William
Thomson	Thomson	k1gMnSc1	Thomson
(	(	kIx(	(
<g/>
lord	lord	k1gMnSc1	lord
Kelvin	kelvin	k1gInSc1	kelvin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
jednotka	jednotka	k1gFnSc1	jednotka
tepla	tepnout	k5eAaPmAgFnS	tepnout
Kelvin	kelvin	k1gInSc4	kelvin
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Boyle	Boyle	k1gFnSc2	Boyle
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
prvního	první	k4xOgMnSc4	první
moderního	moderní	k2eAgMnSc4d1	moderní
chemika	chemik	k1gMnSc4	chemik
<g/>
.	.	kIx.	.
</s>
<s>
Fyzik	fyzik	k1gMnSc1	fyzik
William	William	k1gInSc1	William
Rowan	Rowan	k1gInSc1	Rowan
Hamilton	Hamilton	k1gInSc4	Hamilton
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
popsal	popsat	k5eAaPmAgMnS	popsat
kvaterniony	kvaternion	k1gInPc7	kvaternion
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
hydrodynamiky	hydrodynamika	k1gFnSc2	hydrodynamika
byl	být	k5eAaImAgMnS	být
George	Georg	k1gMnSc2	Georg
Gabriel	Gabriel	k1gMnSc1	Gabriel
Stokes	Stokes	k1gMnSc1	Stokes
<g/>
.	.	kIx.	.
</s>
<s>
Stupnice	stupnice	k1gFnSc1	stupnice
síly	síla	k1gFnSc2	síla
větru	vítr	k1gInSc2	vítr
dodnes	dodnes	k6eAd1	dodnes
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
Francise	Francise	k1gFnSc2	Francise
Beauforta	Beauforta	k1gFnSc1	Beauforta
<g/>
.	.	kIx.	.
</s>
<s>
Hans	Hans	k1gMnSc1	Hans
Sloane	Sloan	k1gMnSc5	Sloan
založil	založit	k5eAaPmAgMnS	založit
Britské	britský	k2eAgNnSc1d1	Britské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Ira	Ir	k1gMnSc2	Ir
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
i	i	k8xC	i
Charlese	Charles	k1gMnSc2	Charles
Algernon	Algernona	k1gFnPc2	Algernona
Parsonse	Parsons	k1gMnSc2	Parsons
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc2	vynálezce
přetlakové	přetlakový	k2eAgFnSc2d1	přetlaková
parní	parní	k2eAgFnSc2d1	parní
turbíny	turbína	k1gFnSc2	turbína
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
fyziky	fyzika	k1gFnSc2	fyzika
sehrál	sehrát	k5eAaPmAgInS	sehrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
George	Georg	k1gMnSc2	Georg
Francis	Francis	k1gFnSc2	Francis
FitzGerald	FitzGeralda	k1gFnPc2	FitzGeralda
<g/>
.	.	kIx.	.
</s>
<s>
Astronom	astronom	k1gMnSc1	astronom
Edward	Edward	k1gMnSc1	Edward
Sabine	Sabin	k1gInSc5	Sabin
objevil	objevit	k5eAaPmAgInS	objevit
souvislost	souvislost	k1gFnSc4	souvislost
mezi	mezi	k7c7	mezi
slunečními	sluneční	k2eAgFnPc7d1	sluneční
skvrnami	skvrna	k1gFnPc7	skvrna
a	a	k8xC	a
kolísáním	kolísání	k1gNnSc7	kolísání
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Objevitelkou	objevitelka	k1gFnSc7	objevitelka
pulsaru	pulsar	k1gInSc2	pulsar
byla	být	k5eAaImAgFnS	být
severoirská	severoirský	k2eAgFnSc1d1	severoirská
astronomka	astronomka	k1gFnSc1	astronomka
Jocelyn	Jocelyna	k1gFnPc2	Jocelyna
Bellová	Bellová	k1gFnSc1	Bellová
Burnellová	Burnellová	k1gFnSc1	Burnellová
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Stewart	Stewart	k1gMnSc1	Stewart
Bell	bell	k1gInSc4	bell
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
kvantovou	kvantový	k2eAgFnSc7d1	kvantová
fyzikou	fyzika	k1gFnSc7	fyzika
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
Bellova	Bellův	k2eAgInSc2d1	Bellův
teorému	teorém	k1gInSc2	teorém
<g/>
.	.	kIx.	.
</s>
<s>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
získal	získat	k5eAaPmAgMnS	získat
Ernest	Ernest	k1gMnSc1	Ernest
Thomas	Thomas	k1gMnSc1	Thomas
Sinton	Sinton	k1gInSc4	Sinton
Walton	Walton	k1gInSc4	Walton
<g/>
,	,	kIx,	,
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
William	William	k1gInSc1	William
C.	C.	kA	C.
Campbell	Campbell	k1gInSc1	Campbell
<g/>
.	.	kIx.	.
</s>
<s>
Irské	irský	k2eAgNnSc4d1	irské
občanství	občanství	k1gNnSc4	občanství
získal	získat	k5eAaPmAgMnS	získat
i	i	k9	i
fyzik	fyzik	k1gMnSc1	fyzik
Erwin	Erwin	k1gMnSc1	Erwin
Schrödinger	Schrödinger	k1gMnSc1	Schrödinger
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějšími	slavný	k2eAgMnPc7d3	nejslavnější
filozofy	filozof	k1gMnPc7	filozof
jsou	být	k5eAaImIp3nP	být
George	Georg	k1gFnPc1	Georg
Berkeley	Berkelea	k1gFnPc1	Berkelea
<g/>
,	,	kIx,	,
Clive	Cliev	k1gFnPc1	Cliev
Staples	Staples	k1gInSc1	Staples
Lewis	Lewis	k1gFnSc2	Lewis
a	a	k8xC	a
Edmund	Edmund	k1gMnSc1	Edmund
Burke	Burk	k1gFnSc2	Burk
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tzv.	tzv.	kA	tzv.
karolinské	karolinský	k2eAgFnSc2d1	Karolinská
renesance	renesance	k1gFnSc2	renesance
sehrál	sehrát	k5eAaPmAgInS	sehrát
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
filozof	filozof	k1gMnSc1	filozof
Jan	Jan	k1gMnSc1	Jan
Scotus	Scotus	k1gMnSc1	Scotus
Eriugena	Eriugena	k1gFnSc1	Eriugena
<g/>
.	.	kIx.	.
</s>
<s>
Otcem	otec	k1gMnSc7	otec
tzv.	tzv.	kA	tzv.
skotského	skotský	k2eAgNnSc2d1	skotské
osvícenství	osvícenství	k1gNnSc2	osvícenství
byl	být	k5eAaImAgInS	být
filozof	filozof	k1gMnSc1	filozof
Francis	Francis	k1gFnPc2	Francis
Hutcheson	Hutcheson	k1gMnSc1	Hutcheson
<g/>
.	.	kIx.	.
</s>
<s>
Názorem	názor	k1gInSc7	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
mystického	mystický	k2eAgNnSc2d1	mystické
a	a	k8xC	a
křesťanství	křesťanství	k1gNnSc2	křesťanství
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
plně	plně	k6eAd1	plně
racionální	racionální	k2eAgInSc4d1	racionální
<g/>
,	,	kIx,	,
proslul	proslout	k5eAaPmAgMnS	proslout
protestantský	protestantský	k2eAgMnSc1d1	protestantský
filozof	filozof	k1gMnSc1	filozof
John	John	k1gMnSc1	John
Toland	Tolando	k1gNnPc2	Tolando
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
irským	irský	k2eAgMnSc7d1	irský
ekonomem	ekonom	k1gMnSc7	ekonom
byl	být	k5eAaImAgMnS	být
Francis	Francis	k1gFnSc2	Francis
Ysidro	Ysidra	k1gFnSc5	Ysidra
Edgeworth	Edgewortha	k1gFnPc2	Edgewortha
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgNnSc7d3	nejslavnější
(	(	kIx(	(
<g/>
severo	severo	k1gNnSc1	severo
<g/>
)	)	kIx)	)
<g/>
irským	irský	k2eAgMnSc7d1	irský
fotbalistou	fotbalista	k1gMnSc7	fotbalista
byl	být	k5eAaImAgMnS	být
George	George	k1gFnSc4	George
Best	Best	k1gMnSc1	Best
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Zlatého	zlatý	k2eAgInSc2d1	zlatý
míče	míč	k1gInSc2	míč
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Plavec	plavec	k1gMnSc1	plavec
Michelle	Michelle	k1gFnSc2	Michelle
Smith	Smith	k1gMnSc1	Smith
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
zlaté	zlatý	k2eAgFnPc4d1	zlatá
olympijské	olympijský	k2eAgFnPc4d1	olympijská
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
hadi	had	k1gMnPc1	had
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vysvětleno	vysvětlen	k2eAgNnSc4d1	vysvětleno
legendou	legenda	k1gFnSc7	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
je	být	k5eAaImIp3nS	být
měl	mít	k5eAaImAgMnS	mít
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
vyhnat	vyhnat	k5eAaPmF	vyhnat
svatý	svatý	k2eAgMnSc1d1	svatý
Patrik	Patrik	k1gMnSc1	Patrik
<g/>
,	,	kIx,	,
patron	patron	k1gMnSc1	patron
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
žije	žít	k5eAaImIp3nS	žít
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
irského	irský	k2eAgInSc2d1	irský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
desítky	desítka	k1gFnPc1	desítka
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
než	než	k8xS	než
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
uvedlo	uvést	k5eAaPmAgNnS	uvést
irský	irský	k2eAgInSc4d1	irský
původ	původ	k1gInSc4	původ
33,3	[number]	k4	33,3
milionů	milion	k4xCgInPc2	milion
Američanů	Američan	k1gMnPc2	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
svatého	svatý	k2eAgMnSc2d1	svatý
Patrika	Patrik	k1gMnSc2	Patrik
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
slaví	slavit	k5eAaImIp3nS	slavit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
Den	den	k1gInSc1	den
svatého	svatý	k2eAgMnSc2d1	svatý
Patrika	Patrik	k1gMnSc2	Patrik
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
svatého	svatý	k2eAgMnSc2d1	svatý
Patrika	Patrik	k1gMnSc2	Patrik
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
