<p>
<s>
Kubinka	Kubinka	k1gFnSc1	Kubinka
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
К	К	k?	К
<g/>
́	́	k?	́
<g/>
б	б	k?	б
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
s	s	k7c7	s
přibližně	přibližně	k6eAd1	přibližně
s	s	k7c7	s
dvaceti	dvacet	k4xCc7	dvacet
tisíci	tisíc	k4xCgInPc7	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
Moskevské	moskevský	k2eAgFnSc6d1	Moskevská
oblasti	oblast	k1gFnSc6	oblast
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
63	[number]	k4	63
kilometrů	kilometr	k1gInPc2	kilometr
západně	západně	k6eAd1	západně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c4	na
půl	půl	k1xP	půl
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
Možajsku	Možajsek	k1gInSc2	Možajsek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kubinka	Kubinka	k1gFnSc1	Kubinka
má	mít	k5eAaImIp3nS	mít
významnou	významný	k2eAgFnSc4d1	významná
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
tankové	tankový	k2eAgNnSc4d1	tankové
muzeum	muzeum	k1gNnSc4	muzeum
Kubinka	Kubinka	k1gFnSc1	Kubinka
a	a	k8xC	a
letecká	letecký	k2eAgFnSc1d1	letecká
základna	základna	k1gFnSc1	základna
Kubinka	Kubinka	k1gFnSc1	Kubinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Vesnice	vesnice	k1gFnSc1	vesnice
Kubinka	Kubinka	k1gFnSc1	Kubinka
je	být	k5eAaImIp3nS	být
doložena	doložit	k5eAaPmNgFnS	doložit
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
zdejšího	zdejší	k2eAgMnSc2d1	zdejší
pána	pán	k1gMnSc2	pán
Ivana	Ivan	k1gMnSc2	Ivan
Kubenského	Kubenský	k2eAgMnSc2d1	Kubenský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
Kubinky	Kubinka	k1gFnSc2	Kubinka
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Kubinka	Kubinka	k1gFnSc1	Kubinka
stává	stávat	k5eAaImIp3nS	stávat
stanicí	stanice	k1gFnSc7	stanice
na	na	k7c6	na
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
do	do	k7c2	do
Smolensku	Smolensko	k1gNnSc3	Smolensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Kubinka	Kubinka	k1gFnSc1	Kubinka
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Kubinka	Kubinka	k1gFnSc1	Kubinka
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kubinka	Kubinka	k1gFnSc1	Kubinka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
