<p>
<s>
Morganit	morganit	k1gInSc1	morganit
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc1d1	chemický
vzorec	vzorec	k1gInSc1	vzorec
Be	Be	k1gFnSc2	Be
<g/>
3	[number]	k4	3
<g/>
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
–	–	k?	–
křemičitan	křemičitan	k1gInSc1	křemičitan
diberylnato-trihlinitý	diberylnatorihlinitý	k2eAgInSc1d1	diberylnato-trihlinitý
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
smaragd	smaragd	k1gInSc4	smaragd
a	a	k8xC	a
akvamarín	akvamarín	k1gInSc4	akvamarín
mezi	mezi	k7c7	mezi
beryly	beryl	k1gInPc7	beryl
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
identifikován	identifikovat	k5eAaBmNgInS	identifikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
a	a	k8xC	a
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
Georgem	Georg	k1gMnSc7	Georg
F.	F.	kA	F.
Kunzem	Kunz	k1gMnSc7	Kunz
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
po	po	k7c6	po
známém	známý	k1gMnSc6	známý
finančníkovi	finančník	k1gMnSc6	finančník
<g/>
,	,	kIx,	,
bankéři	bankéř	k1gMnPc1	bankéř
a	a	k8xC	a
vášnivém	vášnivý	k2eAgMnSc6d1	vášnivý
sběrateli	sběratel	k1gMnSc6	sběratel
drahokamů	drahokam	k1gInPc2	drahokam
J.	J.	kA	J.
P.	P.	kA	P.
Morganovi	morgan	k1gMnSc3	morgan
<g/>
.	.	kIx.	.
<g/>
Barevně	barevně	k6eAd1	barevně
variuje	variovat	k5eAaBmIp3nS	variovat
od	od	k7c2	od
růžové	růžový	k2eAgFnSc2d1	růžová
po	po	k7c4	po
purpurovo-růžovou	purpurovoůžový	k2eAgFnSc4d1	purpurovo-růžový
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
někdy	někdy	k6eAd1	někdy
říká	říkat	k5eAaImIp3nS	říkat
růžový	růžový	k2eAgInSc4d1	růžový
smaragd	smaragd	k1gInSc4	smaragd
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
obvyklé	obvyklý	k2eAgInPc4d1	obvyklý
odstíny	odstín	k1gInPc4	odstín
patří	patřit	k5eAaImIp3nS	patřit
bledě	bledě	k6eAd1	bledě
růžová	růžový	k2eAgFnSc1d1	růžová
<g/>
,	,	kIx,	,
fialová	fialový	k2eAgFnSc1d1	fialová
<g/>
,	,	kIx,	,
lososová	lososový	k2eAgFnSc1d1	lososová
nebo	nebo	k8xC	nebo
broskvová	broskvový	k2eAgFnSc1d1	broskvová
<g/>
.	.	kIx.	.
</s>
<s>
Růžové	růžový	k2eAgNnSc1d1	růžové
zbarvení	zbarvení	k1gNnSc1	zbarvení
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
přítomnost	přítomnost	k1gFnSc1	přítomnost
manganových	manganový	k2eAgInPc2d1	manganový
nebo	nebo	k8xC	nebo
cesiových	cesiový	k2eAgInPc2d1	cesiový
kationtů	kation	k1gInPc2	kation
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
tvrdost	tvrdost	k1gFnSc1	tvrdost
na	na	k7c6	na
Mohsově	Mohsův	k2eAgFnSc6d1	Mohsova
škále	škála	k1gFnSc6	škála
<g/>
:	:	kIx,	:
7,5	[number]	k4	7,5
–	–	k?	–
8	[number]	k4	8
<g/>
;	;	kIx,	;
hustota	hustota	k1gFnSc1	hustota
<g/>
:	:	kIx,	:
2,71	[number]	k4	2,71
–	–	k?	–
2,9	[number]	k4	2,9
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
</s>
</p>
<p>
<s>
Optické	optický	k2eAgFnPc1d1	optická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
barva	barva	k1gFnSc1	barva
od	od	k7c2	od
růžové	růžový	k2eAgFnSc2d1	růžová
a	a	k8xC	a
růžovopurpurové	růžovopurpurový	k2eAgFnSc2d1	růžovopurpurový
po	po	k7c4	po
broskvovou	broskvový	k2eAgFnSc4d1	broskvová
a	a	k8xC	a
lososovou	lososový	k2eAgFnSc4d1	lososová
<g/>
;	;	kIx,	;
lesk	lesk	k1gInSc4	lesk
sklený	sklený	k2eAgInSc4d1	sklený
<g/>
;	;	kIx,	;
průhlednost	průhlednost	k1gFnSc4	průhlednost
<g/>
:	:	kIx,	:
průhledný	průhledný	k2eAgInSc1d1	průhledný
<g/>
,	,	kIx,	,
vryp	vryp	k1gInSc1	vryp
<g/>
:	:	kIx,	:
bílý	bílý	k2eAgInSc1d1	bílý
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Morganit	morganit	k1gInSc1	morganit
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
především	především	k9	především
ve	v	k7c6	v
špekařském	špekařský	k2eAgInSc6d1	špekařský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hledí	hledět	k5eAaImIp3nS	hledět
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
čistotu	čistota	k1gFnSc4	čistota
<g/>
,	,	kIx,	,
brus	brus	k1gInSc4	brus
a	a	k8xC	a
karátovou	karátový	k2eAgFnSc4d1	karátová
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejpopulárnější	populární	k2eAgInPc1d3	nejpopulárnější
jsou	být	k5eAaImIp3nP	být
růžové	růžový	k2eAgInPc1d1	růžový
a	a	k8xC	a
růžovofialové	růžovofialový	k2eAgInPc1d1	růžovofialový
morganity	morganit	k1gInPc1	morganit
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
i	i	k9	i
ty	ten	k3xDgFnPc1	ten
broskvové	broskvový	k2eAgFnPc1d1	broskvová
a	a	k8xC	a
lososové	lososový	k2eAgFnPc1d1	lososová
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mnozí	mnohý	k2eAgMnPc1d1	mnohý
šperkaři	šperkař	k1gMnPc1	šperkař
považují	považovat	k5eAaImIp3nP	považovat
neupravené	upravený	k2eNgInPc4d1	neupravený
kameny	kámen	k1gInPc4	kámen
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
barvách	barva	k1gFnPc6	barva
za	za	k7c2	za
cennější	cenný	k2eAgFnSc2d2	cennější
než	než	k8xS	než
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
barva	barva	k1gFnSc1	barva
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
umělou	umělý	k2eAgFnSc7d1	umělá
úpravou	úprava	k1gFnSc7	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Skoro	skoro	k6eAd1	skoro
všechny	všechen	k3xTgInPc1	všechen
morganity	morganit	k1gInPc1	morganit
vynikají	vynikat	k5eAaImIp3nP	vynikat
jemnými	jemný	k2eAgFnPc7d1	jemná
pastelovými	pastelový	k2eAgFnPc7d1	pastelová
barvami	barva	k1gFnPc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc1d2	veliký
drahokamy	drahokam	k1gInPc1	drahokam
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
výraznějšími	výrazný	k2eAgInPc7d2	výraznější
a	a	k8xC	a
sytějšími	sytý	k2eAgInPc7d2	sytější
odstíny	odstín	k1gInPc7	odstín
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejžádanějším	žádaný	k2eAgFnPc3d3	nejžádanější
barvám	barva	k1gFnPc3	barva
patří	patřit	k5eAaImIp3nS	patřit
čistě	čistě	k6eAd1	čistě
růžová	růžový	k2eAgFnSc1d1	růžová
a	a	k8xC	a
zřídka	zřídka	k6eAd1	zřídka
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
fialovorůžové	fialovorůžový	k2eAgInPc1d1	fialovorůžový
madagaskarské	madagaskarský	k2eAgInPc1d1	madagaskarský
morganity	morganit	k1gInPc1	morganit
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejvzácnějším	vzácný	k2eAgFnPc3d3	nejvzácnější
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gNnPc1	jejich
ložiska	ložisko	k1gNnPc1	ložisko
jsou	být	k5eAaImIp3nP	být
už	už	k6eAd1	už
vytěžená	vytěžený	k2eAgNnPc1d1	vytěžené
<g/>
.	.	kIx.	.
</s>
<s>
Morganit	morganit	k1gInSc1	morganit
zpravidla	zpravidla	k6eAd1	zpravidla
nemá	mít	k5eNaImIp3nS	mít
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
viditelné	viditelný	k2eAgFnSc2d1	viditelná
inkluze	inkluze	k1gFnSc2	inkluze
a	a	k8xC	a
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
jedinečnou	jedinečný	k2eAgFnSc7d1	jedinečná
čistotou	čistota	k1gFnSc7	čistota
<g/>
.	.	kIx.	.
</s>
<s>
Inkluze	inkluze	k1gFnPc1	inkluze
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
tekuté	tekutý	k2eAgFnPc1d1	tekutá
nebo	nebo	k8xC	nebo
dvoufázové	dvoufázový	k2eAgFnPc1d1	dvoufázová
<g/>
.	.	kIx.	.
</s>
<s>
Inkluze	inkluze	k1gFnSc1	inkluze
u	u	k7c2	u
morganitu	morganit	k1gInSc2	morganit
připomínají	připomínat	k5eAaImIp3nP	připomínat
buď	buď	k8xC	buď
duté	dutý	k2eAgInPc1d1	dutý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tekutinou	tekutina	k1gFnSc7	tekutina
naplněné	naplněný	k2eAgFnSc2d1	naplněná
jehly	jehla	k1gFnSc2	jehla
nebo	nebo	k8xC	nebo
otisky	otisk	k1gInPc4	otisk
prstů	prst	k1gInPc2	prst
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
řídké	řídký	k2eAgFnSc3d1	řídká
přítomnosti	přítomnost	k1gFnSc3	přítomnost
viditelných	viditelný	k2eAgFnPc2d1	viditelná
inkluzí	inkluze	k1gFnPc2	inkluze
neztrácí	ztrácet	k5eNaImIp3nP	ztrácet
morganity	morganit	k1gInPc1	morganit
"	"	kIx"	"
<g/>
zdobené	zdobený	k2eAgFnPc1d1	zdobená
<g/>
"	"	kIx"	"
drobnými	drobný	k2eAgFnPc7d1	drobná
jehličkami	jehlička	k1gFnPc7	jehlička
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
evokují	evokovat	k5eAaBmIp3nP	evokovat
surové	surový	k2eAgNnSc4d1	surové
hedvábí	hedvábí	k1gNnSc4	hedvábí
<g/>
,	,	kIx,	,
na	na	k7c6	na
hodnotě	hodnota	k1gFnSc6	hodnota
a	a	k8xC	a
sběratelé	sběratel	k1gMnPc1	sběratel
i	i	k8xC	i
zlatníci	zlatník	k1gMnPc1	zlatník
je	on	k3xPp3gInPc4	on
často	často	k6eAd1	často
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
průsvitné	průsvitný	k2eAgInPc1d1	průsvitný
drahokamy	drahokam	k1gInPc1	drahokam
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyřezávají	vyřezávat	k5eAaImIp3nP	vyřezávat
nebo	nebo	k8xC	nebo
brousí	brousit	k5eAaImIp3nP	brousit
jako	jako	k9	jako
kabošony	kabošona	k1gFnPc1	kabošona
<g/>
.	.	kIx.	.
</s>
<s>
Morganit	morganit	k1gInSc1	morganit
vyniká	vynikat	k5eAaImIp3nS	vynikat
zřetelným	zřetelný	k2eAgInSc7d1	zřetelný
pleochroismem	pleochroismus	k1gInSc7	pleochroismus
–	–	k?	–
od	od	k7c2	od
bledých	bledý	k2eAgMnPc2d1	bledý
po	po	k7c4	po
syté	sytý	k2eAgInPc4d1	sytý
modrorůžové	modrorůžový	k2eAgInPc4d1	modrorůžový
odstíny	odstín	k1gInPc4	odstín
–	–	k?	–
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
při	při	k7c6	při
brusu	brus	k1gInSc6	brus
pečlivě	pečlivě	k6eAd1	pečlivě
orientovat	orientovat	k5eAaBmF	orientovat
<g/>
.	.	kIx.	.
</s>
<s>
Výrazné	výrazný	k2eAgFnPc1d1	výrazná
barvy	barva	k1gFnPc1	barva
nejsou	být	k5eNaImIp3nP	být
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
kámen	kámen	k1gInSc4	kámen
typické	typický	k2eAgNnSc1d1	typické
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
mít	mít	k5eAaImF	mít
drahokamy	drahokam	k1gInPc4	drahokam
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgInPc1d1	velký
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
klenotníci	klenotník	k1gMnPc1	klenotník
při	při	k7c6	při
broušení	broušení	k1gNnSc6	broušení
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
vzhledu	vzhled	k1gInSc2	vzhled
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Morganit	morganit	k1gInSc1	morganit
se	se	k3xPyFc4	se
brousí	brousit	k5eAaImIp3nS	brousit
do	do	k7c2	do
všech	všecek	k3xTgInPc2	všecek
standardních	standardní	k2eAgInPc2d1	standardní
tvarů	tvar	k1gInPc2	tvar
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
jako	jako	k8xC	jako
oval	ovalit	k5eAaPmRp2nS	ovalit
<g/>
,	,	kIx,	,
cushion	cushion	k1gInSc1	cushion
<g/>
,	,	kIx,	,
emerald	emerald	k1gInSc1	emerald
<g/>
,	,	kIx,	,
pear	pear	k1gInSc1	pear
a	a	k8xC	a
marquise	marquise	k1gFnSc1	marquise
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
i	i	k9	i
unikátní	unikátní	k2eAgInPc1d1	unikátní
a	a	k8xC	a
neobvyklé	obvyklý	k2eNgInPc1d1	neobvyklý
designy	design	k1gInPc1	design
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Morganit	morganit	k1gInSc1	morganit
se	se	k3xPyFc4	se
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
objevení	objevení	k1gNnSc6	objevení
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začal	začít	k5eAaPmAgMnS	začít
těžit	těžit	k5eAaImF	těžit
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
a	a	k8xC	a
odtamtud	odtamtud	k6eAd1	odtamtud
pochází	pocházet	k5eAaImIp3nS	pocházet
především	především	k9	především
růžově	růžově	k6eAd1	růžově
zbarvené	zbarvený	k2eAgInPc1d1	zbarvený
kameny	kámen	k1gInPc1	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Výzamná	Výzamný	k2eAgNnPc1d1	Výzamný
naleziště	naleziště	k1gNnPc1	naleziště
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
především	především	k9	především
do	do	k7c2	do
broskvova	broskvův	k2eAgInSc2d1	broskvův
zbarvené	zbarvený	k2eAgInPc4d1	zbarvený
drahokamy	drahokam	k1gInPc4	drahokam
<g/>
.	.	kIx.	.
</s>
<s>
Doly	dol	k1gInPc1	dol
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Mozambiku	Mozambik	k1gInSc6	Mozambik
<g/>
,	,	kIx,	,
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Zimbabwe	Zimbabwe	k1gNnSc6	Zimbabwe
a	a	k8xC	a
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
Kalifornie	Kalifornie	k1gFnPc1	Kalifornie
a	a	k8xC	a
Maine	Main	k1gInSc5	Main
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Broušením	broušení	k1gNnSc7	broušení
pastelově	pastelově	k6eAd1	pastelově
zbarvených	zbarvený	k2eAgInPc2d1	zbarvený
morganitů	morganit	k1gInPc2	morganit
proslulo	proslout	k5eAaPmAgNnS	proslout
především	především	k9	především
Thajsko	Thajsko	k1gNnSc1	Thajsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podobné	podobný	k2eAgInPc4d1	podobný
minerály	minerál	k1gInPc4	minerál
==	==	k?	==
</s>
</p>
<p>
<s>
Morganit	morganit	k1gInSc1	morganit
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
významné	významný	k2eAgFnSc2d1	významná
skupiny	skupina	k1gFnSc2	skupina
berylů	beryl	k1gInPc2	beryl
<g/>
.	.	kIx.	.
</s>
<s>
Bazzit	Bazzit	k1gInSc1	Bazzit
a	a	k8xC	a
pezzottait	pezzottait	k1gMnSc1	pezzottait
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
morganitem	morganit	k1gInSc7	morganit
vzhledově	vzhledově	k6eAd1	vzhledově
zaměnitelné	zaměnitelný	k2eAgNnSc1d1	zaměnitelné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
gemologicky	gemologicky	k6eAd1	gemologicky
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
mezi	mezi	k7c7	mezi
beryly	beryl	k1gInPc7	beryl
neřadí	řadit	k5eNaImIp3nS	řadit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Morganit	morganit	k1gInSc1	morganit
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.mindat.org/min-2783.html	[url]	k1gMnSc1	http://www.mindat.org/min-2783.html
</s>
</p>
<p>
<s>
http://www.gemdat.org/gem-2783.html	[url]	k1gMnSc1	http://www.gemdat.org/gem-2783.html
</s>
</p>
<p>
<s>
http://www.minerals.net/gemstone/morganite_gemstone.aspx	[url]	k1gInSc1	http://www.minerals.net/gemstone/morganite_gemstone.aspx
</s>
</p>
<p>
<s>
http://www.gemselect.com/gem-info/morganite/morganite-info.php	[url]	k1gMnSc1	http://www.gemselect.com/gem-info/morganite/morganite-info.php
</s>
</p>
<p>
<s>
https://www.gia.edu/morganite	[url]	k1gInSc5	https://www.gia.edu/morganite
</s>
</p>
<p>
<s>
https://www.eppi.cz/napoveda/drahokamy/morganit	[url]	k1gInSc1	https://www.eppi.cz/napoveda/drahokamy/morganit
</s>
</p>
