<s>
Buzz	Buzz	k1gInSc1
Aldrin	aldrin	k1gInSc1
</s>
<s>
Buzz	Buzz	k1gInSc1
Aldrin	aldrin	k1gInSc1
Astronaut	astronaut	k1gMnSc1
NASA	NASA	kA
Státní	státní	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
</s>
<s>
USA	USA	kA
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1930	#num#	k4
(	(	kIx(
<g/>
91	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Montclair	Montclair	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
Jersey	Jersea	k1gFnSc2
<g/>
,	,	kIx,
USA	USA	kA
Předchozízaměstnání	Předchozízaměstnání	k1gNnSc1
</s>
<s>
bojový	bojový	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
Hodnost	hodnost	k1gFnSc4
</s>
<s>
plukovník	plukovník	k1gMnSc1
USAF	USAF	kA
v.	v.	k?
v.	v.	k?
Čas	čas	k1gInSc1
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
</s>
<s>
12	#num#	k4
<g/>
d	d	k?
1	#num#	k4
<g/>
h	h	k?
52	#num#	k4
<g/>
m	m	kA
Kosmonaut	kosmonaut	k1gMnSc1
od	od	k7c2
</s>
<s>
říjen	říjen	k1gInSc1
1963	#num#	k4
Mise	mise	k1gFnSc2
</s>
<s>
Gemini	Gemin	k1gMnPc1
12	#num#	k4
<g/>
,	,	kIx,
Apollo	Apollo	k1gMnSc1
11	#num#	k4
Znaky	znak	k1gInPc7
misí	mise	k1gFnSc7
</s>
<s>
Kosmonaut	kosmonaut	k1gMnSc1
do	do	k7c2
</s>
<s>
1969	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Buzz	Buzz	k1gInSc1
Aldrin	aldrin	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1930	#num#	k4
<g/>
,	,	kIx,
Montclair	Montclair	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
Jersey	Jersea	k1gFnSc2
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rozený	rozený	k2eAgMnSc1d1
Edwin	Edwin	k1gMnSc1
Eugene	Eugen	k1gInSc5
Aldrin	aldrin	k1gInSc1
<g/>
,	,	kIx,
Jr	Jr	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
plukovník	plukovník	k1gMnSc1
USAF	USAF	kA
v.	v.	k?
v.	v.	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
americký	americký	k2eAgMnSc1d1
vojenský	vojenský	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
a	a	k8xC
astronaut	astronaut	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	s	k7c7
druhým	druhý	k4xOgMnSc7
člověkem	člověk	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
vstoupil	vstoupit	k5eAaPmAgMnS
na	na	k7c4
povrch	povrch	k1gInSc4
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Kariéra	kariéra	k1gFnSc1
v	v	k7c6
armádě	armáda	k1gFnSc6
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS
na	na	k7c4
Montclair	Montclair	k1gInSc4
High	High	k1gInSc4
School	Schoola	k1gFnPc2
a	a	k8xC
poté	poté	k6eAd1
navštěvoval	navštěvovat	k5eAaImAgMnS
vojenskou	vojenský	k2eAgFnSc4d1
akademii	akademie	k1gFnSc4
United	United	k1gMnSc1
States	States	k1gMnSc1
Military	Militara	k1gFnSc2
Academy	Academa	k1gFnSc2
ve	v	k7c4
West	West	k1gInSc4
Point	pointa	k1gFnPc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
absolvoval	absolvovat	k5eAaPmAgMnS
jako	jako	k9
třetí	třetí	k4xOgMnSc1
nejlepší	dobrý	k2eAgMnSc1d3
ve	v	k7c6
třídě	třída	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Korejské	korejský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
létal	létat	k5eAaImAgMnS
na	na	k7c6
strojích	stroj	k1gInPc6
F-86	F-86	k1gFnSc2
Sabre	Sabr	k1gInSc5
a	a	k8xC
připsal	připsat	k5eAaPmAgMnS
si	se	k3xPyFc3
dva	dva	k4xCgInPc4
sestřely	sestřel	k1gInPc1
letounů	letoun	k1gInPc2
MiG-	MiG-	k1gFnSc4
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
opustil	opustit	k5eAaPmAgMnS
Koreu	Korea	k1gFnSc4
<g/>
,	,	kIx,
působil	působit	k5eAaImAgMnS
jako	jako	k9
instruktor	instruktor	k1gMnSc1
na	na	k7c6
základně	základna	k1gFnSc6
Nellis	Nellis	k1gFnSc2
v	v	k7c6
Nevadě	Nevada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působil	působit	k5eAaImAgMnS
také	také	k9
na	na	k7c4
U.	U.	kA
S.	S.	kA
Air	Air	k1gMnSc2
Force	force	k1gFnSc4
Academy	Academa	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
se	se	k3xPyFc4
přesunul	přesunout	k5eAaPmAgMnS
do	do	k7c2
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
na	na	k7c6
základně	základna	k1gFnSc6
Bitburg	Bitburg	k1gMnSc1
létal	létat	k5eAaImAgMnS
na	na	k7c6
strojích	stroj	k1gInPc6
F-100	F-100	k1gFnSc2
Super	super	k2eAgMnSc5d1
Sabre	Sabr	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aldrin	aldrin	k1gInSc1
armádu	armáda	k1gFnSc4
opustil	opustit	k5eAaPmAgInS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohl	moct	k5eAaImAgInS
studovat	studovat	k5eAaImF
na	na	k7c6
MIT	MIT	kA
(	(	kIx(
<g/>
Massachusetts	Massachusetts	k1gNnSc1
Institute	institut	k1gInSc5
of	of	k?
Technology	technolog	k1gMnPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
získal	získat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1963	#num#	k4
doktorát	doktorát	k1gInSc4
z	z	k7c2
astronautiky	astronautika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
ukončení	ukončení	k1gNnSc6
studií	studie	k1gFnPc2
se	se	k3xPyFc4
opět	opět	k6eAd1
vrátil	vrátit	k5eAaPmAgMnS
k	k	k7c3
armádě	armáda	k1gFnSc3
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
ji	on	k3xPp3gFnSc4
opustil	opustit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
po	po	k7c6
21	#num#	k4
letech	léto	k1gNnPc6
služby	služba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Působení	působení	k1gNnSc4
u	u	k7c2
NASA	NASA	kA
</s>
<s>
Buzz	Buzz	k1gInSc1
Aldrin	aldrin	k1gInSc1
na	na	k7c6
povrchu	povrch	k1gInSc6
Měsíce	měsíc	k1gInSc2
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
1963	#num#	k4
byl	být	k5eAaImAgInS
Aldrin	aldrin	k1gInSc1
vybrán	vybrat	k5eAaPmNgInS
do	do	k7c2
třetí	třetí	k4xOgFnSc2
skupiny	skupina	k1gFnSc2
astronautů	astronaut	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
seriózní	seriózní	k2eAgInSc1d1
a	a	k8xC
intelektuální	intelektuální	k2eAgInSc1d1
přístup	přístup	k1gInSc1
byl	být	k5eAaImAgInS
při	při	k7c6
plánování	plánování	k1gNnSc6
misí	mise	k1gFnPc2
Gemini	Gemin	k1gMnPc1
neocenitelný	ocenitelný	k2eNgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
Aldrin	aldrin	k1gInSc1
nebyl	být	k5eNaImAgInS
vybrán	vybrat	k5eAaPmNgInS
do	do	k7c2
žádné	žádný	k3yNgFnSc2
z	z	k7c2
posádek	posádka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
po	po	k7c6
tragické	tragický	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
hlavní	hlavní	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
Gemini	Gemin	k2eAgMnPc1d1
9	#num#	k4
se	se	k3xPyFc4
ze	z	k7c2
záložní	záložní	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
stala	stát	k5eAaPmAgFnS
hlavní	hlavní	k2eAgFnSc1d1
a	a	k8xC
do	do	k7c2
nové	nový	k2eAgFnSc2d1
záložní	záložní	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
byl	být	k5eAaImAgInS
vybrán	vybrán	k2eAgInSc1d1
Aldrin	aldrin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
vesmíru	vesmír	k1gInSc2
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
dostal	dostat	k5eAaPmAgMnS
jako	jako	k8xS,k8xC
člen	člen	k1gMnSc1
posádky	posádka	k1gFnSc2
Gemini	Gemin	k2eAgMnPc1d1
12	#num#	k4
<g/>
,	,	kIx,
poslední	poslední	k2eAgFnSc2d1
mise	mise	k1gFnSc2
projektu	projekt	k1gInSc2
Gemini	Gemin	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
se	se	k3xPyFc4
účastnil	účastnit	k5eAaImAgMnS
jako	jako	k9
pilot	pilot	k1gMnSc1
lunárního	lunární	k2eAgInSc2d1
modulu	modul	k1gInSc2
mise	mise	k1gFnSc2
Apollo	Apollo	k1gNnSc1
11	#num#	k4
a	a	k8xC
stanul	stanout	k5eAaPmAgInS
<g/>
,	,	kIx,
po	po	k7c6
Neilu	Neil	k1gMnSc6
Armstrongovi	Armstrong	k1gMnSc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
druhý	druhý	k4xOgMnSc1
člověk	člověk	k1gMnSc1
na	na	k7c6
povrchu	povrch	k1gInSc6
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Lety	let	k1gInPc1
do	do	k7c2
vesmíru	vesmír	k1gInSc2
</s>
<s>
V	v	k7c6
kosmické	kosmický	k2eAgFnSc6d1
lodi	loď	k1gFnSc6
Gemini	Gemin	k1gMnPc1
12	#num#	k4
byl	být	k5eAaImAgMnS
druhým	druhý	k4xOgMnSc7
pilotem	pilot	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odstartoval	odstartovat	k5eAaPmAgMnS
spolu	spolu	k6eAd1
s	s	k7c7
velitelem	velitel	k1gMnSc7
lodě	loď	k1gFnSc2
Jamesem	James	k1gMnSc7
Lovellem	Lovell	k1gMnSc7
z	z	k7c2
floridského	floridský	k2eAgInSc2d1
kosmodromu	kosmodrom	k1gInSc2
na	na	k7c6
Mysu	mys	k1gInSc6
Canaveral	Canaveral	k1gFnSc2
v	v	k7c6
listopadu	listopad	k1gInSc6
1966	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
své	svůj	k3xOyFgFnSc2
mise	mise	k1gFnSc2
se	se	k3xPyFc4
spojili	spojit	k5eAaPmAgMnP
s	s	k7c7
raketou	raketa	k1gFnSc7
Atlas-Agena	Atlas-Ageno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aldrin	aldrin	k1gInSc1
absolvoval	absolvovat	k5eAaPmAgInS
výstup	výstup	k1gInSc4
do	do	k7c2
vesmíru	vesmír	k1gInSc2
(	(	kIx(
<g/>
EVA	Eva	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
délce	délka	k1gFnSc6
5,5	5,5	k4
hodiny	hodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přistáli	přistát	k5eAaPmAgMnP,k5eAaImAgMnP
s	s	k7c7
kabinou	kabina	k1gFnSc7
lodě	loď	k1gFnSc2
na	na	k7c6
vlnách	vlna	k1gFnPc6
oceánu	oceán	k1gInSc2
po	po	k7c6
letu	let	k1gInSc6
trvajícím	trvající	k2eAgMnSc6d1
94	#num#	k4
hodiny	hodina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
startovala	startovat	k5eAaBmAgFnS
trojčlenná	trojčlenný	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
mise	mise	k1gFnSc2
Apollo	Apollo	k1gNnSc1
11	#num#	k4
opět	opět	k6eAd1
z	z	k7c2
Floridy	Florida	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Devětatřicetiletý	devětatřicetiletý	k2eAgInSc1d1
Aldrin	aldrin	k1gInSc1
měl	mít	k5eAaImAgInS
funkci	funkce	k1gFnSc3
pilota	pilot	k1gMnSc4
měsíčního	měsíční	k2eAgInSc2d1
modulu	modul	k1gInSc2
<g/>
,	,	kIx,
partnery	partner	k1gMnPc7
mu	on	k3xPp3gMnSc3
byli	být	k5eAaImAgMnP
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
a	a	k8xC
Michael	Michael	k1gMnSc1
Collins	Collinsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
s	s	k7c7
Amstrongem	Amstrong	k1gInSc7
přistál	přistát	k5eAaPmAgInS,k5eAaImAgInS
a	a	k8xC
vystoupil	vystoupit	k5eAaPmAgInS
20	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1969	#num#	k4
na	na	k7c4
povrch	povrch	k1gInSc4
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
21	#num#	k4
hodinách	hodina	k1gFnPc6
pobytu	pobyt	k1gInSc2
na	na	k7c6
Měsíci	měsíc	k1gInSc6
oba	dva	k4xCgMnPc1
odletěli	odletět	k5eAaPmAgMnP
zpět	zpět	k6eAd1
k	k	k7c3
lodi	loď	k1gFnSc3
kroužící	kroužící	k2eAgFnSc3d1
na	na	k7c6
orbitě	orbita	k1gFnSc6
Měsíce	měsíc	k1gInSc2
a	a	k8xC
odletěli	odletět	k5eAaPmAgMnP
k	k	k7c3
Zemi	zem	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přistáli	přistát	k5eAaImAgMnP,k5eAaPmAgMnP
s	s	k7c7
kabinou	kabina	k1gFnSc7
lodě	loď	k1gFnSc2
v	v	k7c6
Tichém	tichý	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
po	po	k7c6
letu	let	k1gInSc6
trvajícím	trvající	k2eAgMnSc6d1
195	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Gemini	Gemin	k1gMnPc1
12	#num#	k4
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1966	#num#	k4
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1966	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Apollo	Apollo	k1gNnSc1
11	#num#	k4
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1969	#num#	k4
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1969	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odchod	odchod	k1gInSc1
do	do	k7c2
civilu	civil	k1gMnSc6
</s>
<s>
Poté	poté	k6eAd1
se	se	k3xPyFc4
už	už	k6eAd1
nezúčastnil	zúčastnit	k5eNaPmAgMnS
žádné	žádný	k3yNgFnPc4
další	další	k2eAgFnPc4d1
vesmírné	vesmírný	k2eAgFnPc4d1
mise	mise	k1gFnPc4
a	a	k8xC
vrátil	vrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
zpět	zpět	k6eAd1
k	k	k7c3
letectvu	letectvo	k1gNnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
působil	působit	k5eAaImAgMnS
na	na	k7c6
manažerském	manažerský	k2eAgInSc6d1
postu	post	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc6
kariéře	kariéra	k1gFnSc6
ovšem	ovšem	k9
škodily	škodit	k5eAaImAgInP
jeho	jeho	k3xOp3gNnSc1
osobní	osobní	k2eAgInPc1d1
problémy	problém	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
autobiografii	autobiografie	k1gFnSc6
Return	Return	k1gNnSc1
to	ten	k3xDgNnSc1
Earth	Earth	k1gInSc4
přiznává	přiznávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
po	po	k7c6
odchodu	odchod	k1gInSc6
od	od	k7c2
NASA	NASA	kA
trpěl	trpět	k5eAaImAgMnS
depresemi	deprese	k1gFnPc7
a	a	k8xC
alkoholismem	alkoholismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armádu	armáda	k1gFnSc4
opustil	opustit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
a	a	k8xC
začal	začít	k5eAaPmAgInS
působit	působit	k5eAaImF
v	v	k7c6
soukromém	soukromý	k2eAgInSc6d1
sektoru	sektor	k1gInSc6
<g/>
,	,	kIx,
například	například	k6eAd1
dělal	dělat	k5eAaImAgMnS
reklamy	reklama	k1gFnPc4
automobilce	automobilka	k1gFnSc6
Volkswagen	volkswagen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
ho	on	k3xPp3gMnSc4
prezident	prezident	k1gMnSc1
George	George	k1gNnSc2
W.	W.	kA
Bush	Bush	k1gMnSc1
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
do	do	k7c2
Prezidentské	prezidentský	k2eAgFnSc2d1
komise	komise	k1gFnSc2
pro	pro	k7c4
budoucnost	budoucnost	k1gFnSc4
leteckokosmického	leteckokosmický	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
(	(	kIx(
<g/>
Presidential	Presidential	k1gInSc1
Commission	Commission	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
Future	Futur	k1gMnSc5
of	of	k?
the	the	k?
United	United	k1gInSc1
States	States	k1gMnSc1
Aerospace	Aerospace	k1gFnSc1
Industry	Industra	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
působí	působit	k5eAaImIp3nS
v	v	k7c6
Národní	národní	k2eAgFnSc6d1
vesmírné	vesmírný	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
(	(	kIx(
<g/>
National	National	k1gFnPc6
Space	Space	k1gFnSc2
Society	societa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
majitelem	majitel	k1gMnSc7
společnosti	společnost	k1gFnSc2
Starcraft	Starcraft	k1gMnSc1
Enterprises	Enterprises	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Rodinné	rodinný	k2eAgInPc1d1
poměry	poměr	k1gInPc1
</s>
<s>
Buzz	Buzz	k1gInSc1
Aldrin	aldrin	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
</s>
<s>
Počátkem	počátkem	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
si	se	k3xPyFc3
oficiálně	oficiálně	k6eAd1
změnil	změnit	k5eAaPmAgMnS
křestní	křestní	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
na	na	k7c4
„	„	k?
<g/>
Buzz	Buzz	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
původně	původně	k6eAd1
jeho	jeho	k3xOp3gFnSc7
přezdívkou	přezdívka	k1gFnSc7
z	z	k7c2
dětských	dětský	k2eAgFnPc2d1
dob	doba	k1gFnPc2
a	a	k8xC
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
zkrácením	zkrácení	k1gNnSc7
z	z	k7c2
nesprávné	správný	k2eNgFnSc2d1
výslovnosti	výslovnost	k1gFnSc2
„	„	k?
<g/>
buzzer	buzzer	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
foneticky	foneticky	k6eAd1
„	„	k?
<g/>
baze	baze	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
slova	slovo	k1gNnSc2
„	„	k?
<g/>
brother	brothra	k1gFnPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
tj.	tj.	kA
„	„	k?
<g/>
bratr	bratr	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
výslovnost	výslovnost	k1gFnSc1
„	„	k?
<g/>
braze	braze	k1gFnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
vyslovovala	vyslovovat	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc1
sestra	sestra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
února	únor	k1gInSc2
1988	#num#	k4
byl	být	k5eAaImAgInS
potřetí	potřetí	k4xO
ženatý	ženatý	k2eAgMnSc1d1
a	a	k8xC
to	ten	k3xDgNnSc4
s	s	k7c7
Lois	Lois	k1gInSc1
Driggs	Driggs	k1gInSc1
rozenou	rozený	k2eAgFnSc4d1
Cannonovou	Cannonová	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc7
společná	společný	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
měla	mít	k5eAaImAgFnS
šest	šest	k4xCc4
dospělých	dospělý	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
a	a	k8xC
jednoho	jeden	k4xCgMnSc2
vnuka	vnuk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželství	manželství	k1gNnSc1
vydrželo	vydržet	k5eAaPmAgNnS
23	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
skončilo	skončit	k5eAaPmAgNnS
rozvodem	rozvod	k1gInSc7
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
předchozí	předchozí	k2eAgNnSc1d1
manželství	manželství	k1gNnSc1
s	s	k7c7
Joan	Joana	k1gFnPc2
rozenou	rozený	k2eAgFnSc4d1
Archerovou	Archerová	k1gFnSc4
a	a	k8xC
Beverly	Beverla	k1gFnSc2
rozenou	rozený	k2eAgFnSc4d1
Van	van	k1gInSc1
Zileovou	Zileův	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
I	i	k9
když	když	k8xS
se	se	k3xPyFc4
Buzz	Buzz	k1gMnSc1
Aldrin	aldrin	k1gInSc4
stal	stát	k5eAaPmAgMnS
až	až	k9
„	„	k?
<g/>
druhým	druhý	k4xOgNnSc7
člověkem	člověk	k1gMnSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
otiskl	otisknout	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
nohu	noha	k1gFnSc4
do	do	k7c2
povrchu	povrch	k1gInSc2
Měsíce	měsíc	k1gInSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
kontě	konto	k1gNnSc6
jiné	jiný	k2eAgNnSc4d1
prvenství	prvenství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
historicky	historicky	k6eAd1
prvním	první	k4xOgMnSc7
člověkem	člověk	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
kdy	kdy	k6eAd1
močil	močit	k5eAaImAgMnS
na	na	k7c6
jiném	jiný	k2eAgNnSc6d1
tělese	těleso	k1gNnSc6
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Buzz	Buzz	k1gInSc1
Aldrin	aldrin	k1gInSc1
je	být	k5eAaImIp3nS
věřící	věřící	k2eAgMnSc1d1
(	(	kIx(
<g/>
presbyterián	presbyterián	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
po	po	k7c6
přistání	přistání	k1gNnSc6
na	na	k7c6
Měsíci	měsíc	k1gInSc6
v	v	k7c4
Apollo	Apollo	k1gNnSc4
11	#num#	k4
provedl	provést	k5eAaPmAgInS
osobní	osobní	k2eAgNnSc4d1
svaté	svatý	k2eAgNnSc4d1
přijímání	přijímání	k1gNnSc4
(	(	kIx(
<g/>
záležitost	záležitost	k1gFnSc1
však	však	k9
nebyla	být	k5eNaImAgFnS
zveřejněna	zveřejnit	k5eAaPmNgFnS
kvůli	kvůli	k7c3
kontroverzím	kontroverze	k1gFnPc3
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
čtením	čtení	k1gNnSc7
z	z	k7c2
biblické	biblický	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
Genesis	Genesis	k1gFnSc1
při	při	k7c6
obletu	oblet	k1gInSc6
Měsíce	měsíc	k1gInSc2
Apollem	Apollo	k1gNnSc7
8	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
Aldrinovu	Aldrinův	k2eAgFnSc4d1
počest	počest	k1gFnSc4
byl	být	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
pojmenován	pojmenovat	k5eAaPmNgInS
kráter	kráter	k1gInSc1
Aldrin	aldrin	k1gInSc1
poblíž	poblíž	k7c2
místa	místo	k1gNnSc2
přistání	přistání	k1gNnSc2
Apolla	Apollo	k1gNnSc2
11	#num#	k4
(	(	kIx(
<g/>
selenografické	selenografický	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
1,4	1,4	k4
<g/>
°	°	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
22,1	22,1	k4
<g/>
°	°	k?
v.	v.	k?
d.	d.	k?
<g/>
,	,	kIx,
průměr	průměr	k1gInSc1
3	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gFnSc1
příjmení	příjmení	k1gNnSc4
nese	nést	k5eAaImIp3nS
také	také	k9
planetka	planetka	k1gFnSc1
(	(	kIx(
<g/>
6470	#num#	k4
<g/>
)	)	kIx)
Aldrin	aldrin	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
objevil	objevit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
na	na	k7c6
observatoři	observatoř	k1gFnSc6
na	na	k7c4
Kleti	klet	k2eAgMnPc1d1
Antonín	Antonín	k1gMnSc1
Mrkos	Mrkos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
byl	být	k5eAaImAgInS
zapsán	zapsat	k5eAaPmNgInS
v	v	k7c6
Ohiu	Ohio	k1gNnSc6
do	do	k7c2
National	National	k1gFnSc2
Aviation	Aviation	k1gInSc1
Hall	Hall	k1gMnSc1
of	of	k?
Fame	Fame	k1gFnSc1
(	(	kIx(
<g/>
Národní	národní	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
síň	síň	k1gFnSc1
slávy	sláva	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
herecká	herecký	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
bohatá	bohatý	k2eAgFnSc1d1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Má	mít	k5eAaImIp3nS
hvězdu	hvězda	k1gFnSc4
na	na	k7c6
Hollywoodském	hollywoodský	k2eAgInSc6d1
chodníku	chodník	k1gInSc6
slávy	sláva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
namluvil	namluvit	k5eAaPmAgMnS,k5eAaBmAgMnS
sám	sám	k3xTgMnSc1
sebe	sebe	k3xPyFc4
pro	pro	k7c4
epizodu	epizoda	k1gFnSc4
seriálu	seriál	k1gInSc2
Simpsonovi	Simpson	k1gMnSc3
„	„	k?
<g/>
Deep	Deep	k1gMnSc1
Space	Space	k1gMnSc1
Homer	Homer	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
spolu	spolu	k6eAd1
s	s	k7c7
Homerem	Homer	k1gMnSc7
Simpsonem	Simpson	k1gMnSc7
a	a	k8xC
fiktivním	fiktivní	k2eAgMnSc7d1
astronautem	astronaut	k1gMnSc7
Racem	Rac	k1gMnSc7
Banyonem	Banyon	k1gMnSc7
letí	letět	k5eAaImIp3nS
do	do	k7c2
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
si	se	k3xPyFc3
zahrál	zahrát	k5eAaPmAgInS
ve	v	k7c6
filmu	film	k1gInSc6
Transformers	Transformersa	k1gFnPc2
3	#num#	k4
sám	sám	k3xTgMnSc1
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
hrál	hrát	k5eAaImAgMnS
taktéž	taktéž	k?
sám	sám	k3xTgMnSc1
sebe	sebe	k3xPyFc4
v	v	k7c6
seriálu	seriál	k1gInSc6
Teorie	teorie	k1gFnSc1
velkého	velký	k2eAgInSc2d1
třesku	třesk	k1gInSc2
(	(	kIx(
<g/>
pátá	pátý	k4xOgFnSc1
epizoda	epizoda	k1gFnSc1
šesté	šestý	k4xOgFnSc2
série	série	k1gFnSc2
„	„	k?
<g/>
The	The	k1gFnSc2
Holographic	Holographice	k1gFnPc2
Excitation	Excitation	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dále	daleko	k6eAd2
hrál	hrát	k5eAaImAgMnS
sám	sám	k3xTgMnSc1
sebe	sebe	k3xPyFc4
v	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
epizodě	epizoda	k1gFnSc6
4	#num#	k4
<g/>
.	.	kIx.
řady	řada	k1gFnSc2
seriálu	seriál	k1gInSc2
30	#num#	k4
Rock	rock	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
CODR	CODR	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sto	sto	k4xCgNnSc4
hvězdných	hvězdný	k2eAgMnPc2d1
kapitánů	kapitán	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Práce	práce	k1gFnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Edwin	Edwina	k1gFnPc2
Eugene	Eugen	k1gInSc5
Aldrin	aldrin	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
251	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Personnel	Personnel	k1gInSc1
Announcements	Announcements	k1gInSc1
<g/>
:	:	kIx,
Nominations	Nominations	k1gInSc1
by	by	kYmCp3nS
Name	Name	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílý	bílý	k2eAgInSc1d1
dům	dům	k1gInSc1
<g/>
,	,	kIx,
2001-08-22	2001-08-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
About	About	k1gInSc1
the	the	k?
National	National	k1gFnSc2
Space	Space	k1gFnSc2
Society	societa	k1gFnSc2
(	(	kIx(
<g/>
NSS	NSS	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
NSS	NSS	kA
Organization	Organization	k1gInSc1
and	and	k?
Leadership	Leadership	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Space	Spaec	k1gInSc2
Society	societa	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MINARD	MINARD	kA
<g/>
,	,	kIx,
Anne	Anne	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Buzz	Buzz	k1gInSc1
Aldrin	aldrin	k1gInSc4
<g/>
,	,	kIx,
First	First	k1gFnSc4
Man	mana	k1gFnPc2
(	(	kIx(
<g/>
to	ten	k3xDgNnSc1
Pee	Pee	k1gFnSc1
<g/>
)	)	kIx)
on	on	k3xPp3gMnSc1
the	the	k?
Moon	Moon	k1gNnSc1
<g/>
,	,	kIx,
Sounds	Sounds	k1gInSc1
Off	Off	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gMnSc1
Geographic	Geographic	k1gMnSc1
News	News	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Mše	mše	k1gFnSc1
na	na	k7c6
Měsíci	měsíc	k1gInSc6
a	a	k8xC
Aldrinův	Aldrinův	k2eAgInSc1d1
boj	boj	k1gInSc1
o	o	k7c4
první	první	k4xOgInSc4
krok	krok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběh	příběh	k1gInSc4
lidí	člověk	k1gMnPc2
okolo	okolo	k7c2
Apolla	Apollo	k1gNnSc2
11	#num#	k4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-07-20	2014-07-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BLAKEMORE	BLAKEMORE	kA
<g/>
,	,	kIx,
Erin	Erin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Buzz	Buzz	k1gInSc1
Aldrin	aldrin	k1gInSc4
Took	Took	k1gMnSc1
Holy	hola	k1gFnSc2
Communion	Communion	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
Moon	Moon	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
Kept	Kept	k1gMnSc1
it	it	k?
Quiet	Quiet	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
HISTORY	HISTORY	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-09-06	2019-09-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
RÜKL	RÜKL	kA
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlas	Atlas	k1gInSc1
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Aventinum	Aventinum	k1gNnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85277	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Arago	Arago	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
96	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Crater	Crater	k1gInSc1
Aldrin	aldrin	k1gInSc4
on	on	k3xPp3gMnSc1
Moon	Moon	k1gMnSc1
Gazetteer	Gazetteer	k1gMnSc1
of	of	k?
Planetary	Planetara	k1gFnSc2
Nomenclature	Nomenclatur	k1gMnSc5
<g/>
,	,	kIx,
IAU	IAU	kA
<g/>
,	,	kIx,
USGS	USGS	kA
<g/>
,	,	kIx,
NASA	NASA	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Informace	informace	k1gFnSc1
observatoře	observatoř	k1gFnSc2
Kleť	Kleť	k1gFnSc1
o	o	k7c4
pojmenování	pojmenování	k1gNnSc4
planetky	planetka	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Observatoř	observatoř	k1gFnSc1
Kleť	Kleť	k1gFnSc1
<g/>
,	,	kIx,
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://www.csfd.cz/tvurce/51110-buzz-aldrin/	http://www.csfd.cz/tvurce/51110-buzz-aldrin/	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Buzz	Buzz	k1gInSc1
Aldrin	aldrin	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Buzze	Buzze	k1gFnSc2
Aldrina	Aldrina	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
životopis	životopis	k1gInSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
NASA	NASA	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
|	|	kIx~
Měsíc	měsíc	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
skuk	skuk	k1gInSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
19	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
107714566	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1082	#num#	k4
6680	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
88245653	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
110368892	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
88245653	#num#	k4
</s>
