<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1593	[number]	k4	1593
a	a	k8xC	a
1594	[number]	k4	1594
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
divadla	divadlo	k1gNnSc2	divadlo
kvůli	kvůli	k7c3	kvůli
moru	mor	k1gInSc3	mor
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
<g/>
,	,	kIx,	,
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
publikoval	publikovat	k5eAaBmAgMnS	publikovat
dvě	dva	k4xCgFnPc4	dva
narativní	narativní	k2eAgFnPc4d1	narativní
básně	báseň	k1gFnPc4	báseň
s	s	k7c7	s
erotickou	erotický	k2eAgFnSc7d1	erotická
tematikou	tematika	k1gFnSc7	tematika
<g/>
,	,	kIx,	,
Venuše	Venuše	k1gFnSc1	Venuše
a	a	k8xC	a
Adonis	Adonis	k1gFnSc1	Adonis
a	a	k8xC	a
Znásilnění	znásilnění	k1gNnSc1	znásilnění
Lukrécie	Lukrécie	k1gFnSc2	Lukrécie
<g/>
.	.	kIx.	.
</s>
