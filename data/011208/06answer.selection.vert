<s>
Jezero	jezero	k1gNnSc1	jezero
Titicaca	Titicacum	k1gNnSc2	Titicacum
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
3812	[number]	k4	3812
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
nejvýše	vysoce	k6eAd3	vysoce
položenou	položený	k2eAgFnSc7d1	položená
vodní	vodní	k2eAgFnSc7d1	vodní
nádrží	nádrž	k1gFnSc7	nádrž
s	s	k7c7	s
pravidelnou	pravidelný	k2eAgFnSc7d1	pravidelná
lodní	lodní	k2eAgFnSc7d1	lodní
dopravou	doprava	k1gFnSc7	doprava
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
přes	přes	k7c4	přes
hranici	hranice	k1gFnSc4	hranice
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
Bolívie	Bolívie	k1gFnSc2	Bolívie
<g/>
.	.	kIx.	.
</s>
