<s>
Historicky	historicky	k6eAd1	historicky
byl	být	k5eAaImAgInS	být
fosfor	fosfor	k1gInSc4	fosfor
poprvé	poprvé	k6eAd1	poprvé
izolován	izolovat	k5eAaBmNgMnS	izolovat
německým	německý	k2eAgMnSc7d1	německý
alchymistou	alchymista	k1gMnSc7	alchymista
Heningem	Hening	k1gInSc7	Hening
Brandem	Brand	k1gMnSc7	Brand
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1669	[number]	k4	1669
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
všichni	všechen	k3xTgMnPc1	všechen
alchymisté	alchymista	k1gMnPc1	alchymista
<g/>
,	,	kIx,	,
najít	najít	k5eAaPmF	najít
kámen	kámen	k1gInSc4	kámen
mudrců	mudrc	k1gMnPc2	mudrc
<g/>
.	.	kIx.	.
</s>
