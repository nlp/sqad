<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
dochovaným	dochovaný	k2eAgInSc7d1	dochovaný
dramatickým	dramatický	k2eAgInSc7d1	dramatický
dílem	díl	k1gInSc7	díl
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
češtiny	čeština	k1gFnSc2	čeština
je	být	k5eAaImIp3nS	být
zlomek	zlomek	k1gInSc1	zlomek
česko-latinské	českoatinský	k2eAgFnSc2d1	česko-latinská
hry	hra	k1gFnSc2	hra
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc1d1	zvaná
obvykle	obvykle	k6eAd1	obvykle
Mastičkář	mastičkář	k1gMnSc1	mastičkář
<g/>
.	.	kIx.	.
</s>
