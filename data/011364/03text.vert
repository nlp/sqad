<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Mareš	Mareš	k1gMnSc1	Mareš
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1958	[number]	k4	1958
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
až	až	k9	až
2015	[number]	k4	2015
primátor	primátor	k1gMnSc1	primátor
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Chomutova	Chomutov	k1gInSc2	Chomutov
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2012	[number]	k4	2012
až	až	k9	až
2016	[number]	k4	2016
zastupite	zastupit	k1gInSc5	zastupit
Ústeckého	ústecký	k2eAgInSc2d1	ústecký
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
místopředseda	místopředseda	k1gMnSc1	místopředseda
Svazu	svaz	k1gInSc2	svaz
měst	město	k1gNnPc2	město
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
14.12	[number]	k4	14.12
<g/>
.2016	.2016	k4	.2016
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
náměstkem	náměstek	k1gMnSc7	náměstek
primátora	primátor	k1gMnSc2	primátor
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Chomutova	Chomutov	k1gInSc2	Chomutov
(	(	kIx(	(
<g/>
neuvolněný	uvolněný	k2eNgMnSc1d1	neuvolněný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Pedagogickou	pedagogický	k2eAgFnSc4d1	pedagogická
fakultu	fakulta	k1gFnSc4	fakulta
Západočeské	západočeský	k2eAgFnSc2d1	Západočeská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
výchovu	výchova	k1gFnSc4	výchova
a	a	k8xC	a
sport	sport	k1gInSc4	sport
(	(	kIx(	(
<g/>
získal	získat	k5eAaPmAgInS	získat
tak	tak	k9	tak
titul	titul	k1gInSc1	titul
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
vzdělání	vzdělání	k1gNnSc4	vzdělání
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
na	na	k7c6	na
Central	Central	k1gFnSc6	Central
European	Europeany	k1gInPc2	Europeany
Management	management	k1gInSc1	management
Institut	institut	k1gInSc1	institut
(	(	kIx(	(
<g/>
CEMI	CEMI	kA	CEMI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
titul	titul	k1gInSc1	titul
MBA	MBA	kA	MBA
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
ředitel	ředitel	k1gMnSc1	ředitel
Střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
energetické	energetický	k2eAgInPc1d1	energetický
a	a	k8xC	a
stavební	stavební	k2eAgInSc1d1	stavební
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
primátorské	primátorský	k2eAgFnSc2d1	Primátorská
funkce	funkce	k1gFnSc2	funkce
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
ředitelem	ředitel	k1gMnSc7	ředitel
ESOZ	ESOZ	kA	ESOZ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politické	politický	k2eAgNnSc1d1	politické
působení	působení	k1gNnSc1	působení
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
vstoupit	vstoupit	k5eAaPmF	vstoupit
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
za	za	k7c2	za
ČSSD	ČSSD	kA	ČSSD
do	do	k7c2	do
Zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
města	město	k1gNnSc2	město
Chomutova	Chomutov	k1gInSc2	Chomutov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Městským	městský	k2eAgMnSc7d1	městský
zastupitelem	zastupitel	k1gMnSc7	zastupitel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
až	až	k9	až
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
navíc	navíc	k6eAd1	navíc
zvolen	zvolit	k5eAaPmNgInS	zvolit
radním	radní	k1gMnPc3	radní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
mandát	mandát	k1gInSc4	mandát
zastupitele	zastupitel	k1gMnSc2	zastupitel
města	město	k1gNnSc2	město
obhájil	obhájit	k5eAaPmAgMnS	obhájit
<g/>
,	,	kIx,	,
když	když	k8xS	když
zároveň	zároveň	k6eAd1	zároveň
vedl	vést	k5eAaImAgInS	vést
kandidátku	kandidátka	k1gFnSc4	kandidátka
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
strana	strana	k1gFnSc1	strana
volby	volba	k1gFnSc2	volba
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2010	[number]	k4	2010
zvolen	zvolit	k5eAaPmNgMnS	zvolit
primátorem	primátor	k1gMnSc7	primátor
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Chomutova	Chomutov	k1gInSc2	Chomutov
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
zastupitele	zastupitel	k1gMnSc2	zastupitel
města	město	k1gNnSc2	město
obhájil	obhájit	k5eAaPmAgMnS	obhájit
také	také	k9	také
jak	jak	k6eAd1	jak
v	v	k7c6	v
řádných	řádný	k2eAgFnPc6d1	řádná
<g/>
,	,	kIx,	,
tak	tak	k9	tak
opakovaných	opakovaný	k2eAgFnPc6d1	opakovaná
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
a	a	k8xC	a
2015	[number]	k4	2015
(	(	kIx(	(
<g/>
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
vedl	vést	k5eAaImAgMnS	vést
kandidátku	kandidátka	k1gFnSc4	kandidátka
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sociálním	sociální	k2eAgMnPc3d1	sociální
demokratům	demokrat	k1gMnPc3	demokrat
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
nepodařilo	podařit	k5eNaPmAgNnS	podařit
uzavřít	uzavřít	k5eAaPmF	uzavřít
koalici	koalice	k1gFnSc4	koalice
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Mareš	Mareš	k1gMnSc1	Mareš
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2015	[number]	k4	2015
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
primátora	primátor	k1gMnSc2	primátor
skončil	skončit	k5eAaPmAgMnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odvolání	odvolání	k1gNnSc6	odvolání
primátora	primátor	k1gMnSc2	primátor
Daniela	Daniel	k1gMnSc2	Daniel
Černého	Černý	k1gMnSc2	Černý
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2016	[number]	k4	2016
se	se	k3xPyFc4	se
do	do	k7c2	do
rady	rada	k1gMnSc2	rada
města	město	k1gNnSc2	město
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
dostat	dostat	k5eAaPmF	dostat
za	za	k7c4	za
ČSSD	ČSSD	kA	ČSSD
do	do	k7c2	do
Zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
Ústeckého	ústecký	k2eAgInSc2d1	ústecký
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
Uspěl	uspět	k5eAaPmAgMnS	uspět
až	až	k9	až
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
Výboru	výbor	k1gInSc6	výbor
pro	pro	k7c4	pro
výchovu	výchova	k1gFnSc4	výchova
<g/>
,	,	kIx,	,
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
a	a	k8xC	a
zaměstnanost	zaměstnanost	k1gFnSc4	zaměstnanost
a	a	k8xC	a
v	v	k7c6	v
Komisi	komise	k1gFnSc6	komise
legislativně	legislativně	k6eAd1	legislativně
právní	právní	k2eAgFnSc1d1	právní
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
již	již	k9	již
mandát	mandát	k1gInSc1	mandát
neobhajoval	obhajovat	k5eNaImAgInS	obhajovat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
doplňovacích	doplňovací	k2eAgFnPc6d1	doplňovací
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
za	za	k7c2	za
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
č.	č.	k?	č.
5	[number]	k4	5
–	–	k?	–
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
však	však	k9	však
jen	jen	k9	jen
23,11	[number]	k4	23,11
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
nepostoupil	postoupit	k5eNaPmAgInS	postoupit
tak	tak	k6eAd1	tak
ani	ani	k8xC	ani
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Pusobil	Pusobil	k1gMnSc3	Pusobil
také	také	k9	také
jako	jako	k8xC	jako
místopředseda	místopředseda	k1gMnSc1	místopředseda
Svazu	svaz	k1gInSc2	svaz
měst	město	k1gNnPc2	město
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
ČR	ČR	kA	ČR
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
Komory	komora	k1gFnSc2	komora
statutárních	statutární	k2eAgNnPc2d1	statutární
měst	město	k1gNnPc2	město
SMO	SMO	kA	SMO
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
je	být	k5eAaImIp3nS	být
českým	český	k2eAgMnSc7d1	český
zástupcem	zástupce	k1gMnSc7	zástupce
ve	v	k7c6	v
Výboru	výbor	k1gInSc6	výbor
regionů	region	k1gInPc2	region
EU	EU	kA	EU
(	(	kIx(	(
<g/>
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
města	město	k1gNnSc2	město
a	a	k8xC	a
obce	obec	k1gFnSc2	obec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
do	do	k7c2	do
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
společnosti	společnost	k1gFnSc2	společnost
ČEZ	ČEZ	kA	ČEZ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
