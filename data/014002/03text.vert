<s>
Seznam	seznam	k1gInSc1
hadích	hadí	k2eAgInPc2d1
rekordů	rekord	k1gInPc2
</s>
<s>
Tento	tento	k3xDgInSc1
seznam	seznam	k1gInSc1
hadích	hadí	k2eAgInPc2d1
rekordů	rekord	k1gInPc2
zahrnuje	zahrnovat	k5eAaImIp3nS
údaje	údaj	k1gInPc4
vycházejí	vycházet	k5eAaImIp3nP
z	z	k7c2
dostupné	dostupný	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
a	a	k8xC
zdrojů	zdroj	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
ovšem	ovšem	k9
byly	být	k5eAaImAgInP
jinými	jiný	k2eAgMnPc7d1
autory	autor	k1gMnPc7
zpochybněny	zpochybnit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s>
Velikost	velikost	k1gFnSc1
a	a	k8xC
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
Nejmenší	malý	k2eAgMnSc1d3
had	had	k1gMnSc1
</s>
<s>
Dříve	dříve	k6eAd2
byl	být	k5eAaImAgInS
za	za	k7c2
nejmenšího	malý	k2eAgInSc2d3
považován	považován	k2eAgInSc1d1
zemní	zemní	k2eAgInSc1d1
hádek	hádek	k1gInSc1
Leptotyphlops	Leptotyphlops	k1gInSc1
bilineata	bilineata	k1gFnSc1
patřící	patřící	k2eAgFnSc1d1
do	do	k7c2
čeledi	čeleď	k1gFnSc2
slepanovitých	slepanovitý	k2eAgMnPc2d1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
na	na	k7c6
ostrově	ostrov	k1gInSc6
Sv.	sv.	kA
Lucie	Lucie	k1gFnPc1
<g/>
,	,	kIx,
na	na	k7c6
Martiniku	Martinik	k1gInSc6
a	a	k8xC
Barbadosu	Barbados	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
minihad	minihad	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
zakrnělé	zakrnělý	k2eAgFnPc4d1
oči	oko	k1gNnPc4
a	a	k8xC
růžovou	růžový	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
pokožky	pokožka	k1gFnSc2
<g/>
,	,	kIx,
připomíná	připomínat	k5eAaImIp3nS
více	hodně	k6eAd2
žížalu	žížala	k1gFnSc4
<g/>
,	,	kIx,
než	než	k8xS
hada	had	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdá	zdát	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnSc1
maximální	maximální	k2eAgFnSc1d1
délka	délka	k1gFnSc1
jen	jen	k9
zřídka	zřídka	k6eAd1
přesáhne	přesáhnout	k5eAaPmIp3nS
10	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
se	se	k3xPyFc4
jako	jako	k8xS,k8xC
nejmenší	malý	k2eAgMnSc1d3
had	had	k1gMnSc1
uvádí	uvádět	k5eAaImIp3nS
Tetracheilostoma	Tetracheilostoma	k1gNnSc4
(	(	kIx(
<g/>
Leptotyphlops	Leptotyphlops	k1gInSc1
<g/>
)	)	kIx)
carlae	carlae	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
na	na	k7c4
Barbadosu	Barbadosa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinou	většinou	k6eAd1
měří	měřit	k5eAaImIp3nS
do	do	k7c2
10	#num#	k4
cm	cm	kA
(	(	kIx(
<g/>
největší	veliký	k2eAgInSc1d3
kus	kus	k1gInSc1
měřil	měřit	k5eAaImAgInS
10,4	10,4	k4
cm	cm	kA
<g/>
)	)	kIx)
a	a	k8xC
váží	vážit	k5eAaImIp3nS
okolo	okolo	k7c2
0,6	0,6	k4
gramu	gram	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Nejdelší	dlouhý	k2eAgMnSc1d3
had	had	k1gMnSc1
</s>
<s>
Současnost	současnost	k1gFnSc1
</s>
<s>
Krajta	krajta	k1gFnSc1
mřížkovaná	mřížkovaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
zřejmě	zřejmě	k6eAd1
nejdelší	dlouhý	k2eAgInSc1d3
druh	druh	k1gInSc1
hada	had	k1gMnSc2
</s>
<s>
Rekordní	rekordní	k2eAgFnSc1d1
délka	délka	k1gFnSc1
zatím	zatím	k6eAd1
největšího	veliký	k2eAgMnSc2d3
hada	had	k1gMnSc2
uloveného	ulovený	k2eAgMnSc2d1
v	v	k7c6
přírodě	příroda	k1gFnSc6
je	být	k5eAaImIp3nS
9,76	9,76	k4
m.	m.	k?
Drží	držet	k5eAaImIp3nS
ho	on	k3xPp3gMnSc4
krajta	krajta	k1gFnSc1
mřížkovaná	mřížkovaný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Python	Python	k1gMnSc1
reticulatus	reticulatus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zabitá	zabitý	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1912	#num#	k4
na	na	k7c6
ostrově	ostrov	k1gInSc6
Celebes	Celebes	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
nedostatek	nedostatek	k1gInSc4
důvěryhodných	důvěryhodný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
není	být	k5eNaImIp3nS
však	však	k9
tento	tento	k3xDgInSc4
rekord	rekord	k1gInSc4
mnoha	mnoho	k4c3
autoritami	autorita	k1gFnPc7
oficiálně	oficiálně	k6eAd1
uznáván	uznávat	k5eAaImNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdelším	dlouhý	k2eAgNnSc7d3
hadem	had	k1gMnSc7
chovaným	chovaný	k2eAgInPc3d1
v	v	k7c6
zajetí	zajetí	k1gNnSc6
je	být	k5eAaImIp3nS
opět	opět	k6eAd1
krajta	krajta	k1gFnSc1
mřížkovaná	mřížkovaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zoo	zoo	k1gFnSc6
v	v	k7c6
Pittsburghu	Pittsburgh	k1gInSc6
chovali	chovat	k5eAaImAgMnP
hada	had	k1gMnSc4
jménem	jméno	k1gNnSc7
„	„	k?
<g/>
Colossus	Colossus	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
dosáhl	dosáhnout	k5eAaPmAgInS
délky	délka	k1gFnSc2
9,15	9,15	k4
m.	m.	k?
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
byl	být	k5eAaImAgMnS
ovšem	ovšem	k9
i	i	k9
tento	tento	k3xDgInSc1
rekord	rekord	k1gInSc1
zpochybňován	zpochybňovat	k5eAaImNgInS
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Guinnessova	Guinnessův	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
rekordů	rekord	k1gInPc2
uvádí	uvádět	k5eAaImIp3nS
jako	jako	k9
nejdelšího	dlouhý	k2eAgMnSc2d3
jedince	jedinec	k1gMnSc2
chovaného	chovaný	k2eAgMnSc2d1
v	v	k7c6
zajetí	zajetí	k1gNnSc6
hada	had	k1gMnSc2
jménem	jméno	k1gNnSc7
Medusa	Medusa	k1gFnSc1
z	z	k7c2
Missouri	Missouri	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
o	o	k7c6
délce	délka	k1gFnSc6
7,67	7,67	k4
m.	m.	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Zoologická	zoologický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
vypsala	vypsat	k5eAaPmAgFnS
začátkem	začátkem	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
odměnu	odměna	k1gFnSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
postupně	postupně	k6eAd1
vyrostla	vyrůst	k5eAaPmAgFnS
až	až	k9
na	na	k7c4
50	#num#	k4
000	#num#	k4
dolarů	dolar	k1gInPc2
<g/>
,	,	kIx,
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
přinese	přinést	k5eAaPmIp3nS
hada	had	k1gMnSc2
delšího	dlouhý	k2eAgNnSc2d2
než	než	k8xS
30	#num#	k4
stop	stopa	k1gFnPc2
<g/>
,	,	kIx,
tedy	tedy	k9
než	než	k8xS
9,14	9,14	k4
m.	m.	k?
Dodnes	dodnes	k6eAd1
se	se	k3xPyFc4
o	o	k7c4
tuto	tento	k3xDgFnSc4
sumu	suma	k1gFnSc4
nikdo	nikdo	k3yNnSc1
nepřihlásil	přihlásit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Krajta	krajta	k1gFnSc1
mřížkovaná	mřížkovaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
krajta	krajta	k1gFnSc1
tmavá	tmavý	k2eAgFnSc1d1
<g/>
,	,	kIx,
krajta	krajta	k1gFnSc1
písmenková	písmenkový	k2eAgFnSc1d1
<g/>
,	,	kIx,
krajta	krajta	k1gFnSc1
ametystová	ametystový	k2eAgFnSc1d1
a	a	k8xC
anakonda	anakonda	k1gFnSc1
velká	velká	k1gFnSc1
jsou	být	k5eAaImIp3nP
většinou	většina	k1gFnSc7
autorit	autorita	k1gFnPc2
označovány	označovat	k5eAaImNgInP
za	za	k7c4
nejdelší	dlouhý	k2eAgMnPc4d3
hady	had	k1gMnPc4
světa	svět	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
je	být	k5eAaImIp3nS
těžké	těžký	k2eAgNnSc1d1
určit	určit	k5eAaPmF
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
druh	druh	k1gInSc1
vlastně	vlastně	k9
drží	držet	k5eAaImIp3nS
oficiální	oficiální	k2eAgNnSc4d1
prvenství	prvenství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Historicky	historicky	k6eAd1
</s>
<s>
Za	za	k7c4
rekordní	rekordní	k2eAgInSc4d1
druh	druh	k1gInSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
délku	délka	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
považován	považován	k2eAgMnSc1d1
Titanoboa	Titanoboa	k1gMnSc1
cerrejonensis	cerrejonensis	k1gFnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
zkamenělina	zkamenělina	k1gFnSc1
byla	být	k5eAaImAgFnS
objevena	objevit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
v	v	k7c6
Kolumbii	Kolumbie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žil	žít	k5eAaImAgMnS
v	v	k7c6
paleocénu	paleocén	k1gInSc6
a	a	k8xC
dorůstal	dorůstat	k5eAaImAgMnS
13-14	13-14	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
hmotnost	hmotnost	k1gFnSc4
mohl	moct	k5eAaImAgInS
mít	mít	k5eAaImF
až	až	k9
přes	přes	k7c4
tunu	tuna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Nejdelší	dlouhý	k2eAgMnSc1d3
jedovatý	jedovatý	k2eAgMnSc1d1
had	had	k1gMnSc1
</s>
<s>
Nejdelším	dlouhý	k2eAgMnSc7d3
jedovatým	jedovatý	k2eAgMnSc7d1
hadem	had	k1gMnSc7
je	být	k5eAaImIp3nS
kobra	kobra	k1gFnSc1
královská	královský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Ophiophagus	Ophiophagus	k1gMnSc1
hannah	hannah	k1gMnSc1
<g/>
)	)	kIx)
z	z	k7c2
Jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
exemplář	exemplář	k1gInSc1
chovaný	chovaný	k2eAgInSc1d1
v	v	k7c6
Londýnské	londýnský	k2eAgFnSc6d1
zoo	zoo	k1gFnSc6
dosáhl	dosáhnout	k5eAaPmAgInS
velikosti	velikost	k1gFnSc2
5,71	5,71	k4
m	m	kA
při	při	k7c6
hmotnosti	hmotnost	k1gFnSc6
téměř	téměř	k6eAd1
12	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgMnSc1
nejdelší	dlouhý	k2eAgMnSc1d3
jedovatý	jedovatý	k2eAgMnSc1d1
had	had	k1gMnSc1
je	být	k5eAaImIp3nS
mamba	mamba	k1gFnSc1
černá	černý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Dendroaspis	Dendroaspis	k1gFnSc1
polylepis	polylepis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
dorůstá	dorůstat	k5eAaImIp3nS
až	až	k9
4,5	4,5	k4
metru	metr	k1gInSc2
(	(	kIx(
<g/>
ač	ač	k8xS
v	v	k7c6
průměru	průměr	k1gInSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
2	#num#	k4
až	až	k9
3	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Dříve	dříve	k6eAd2
uváděný	uváděný	k2eAgInSc1d1
taipan	taipan	k1gInSc1
velký	velký	k2eAgInSc1d1
(	(	kIx(
<g/>
Oxyuranus	Oxyuranus	k1gInSc1
scutellatus	scutellatus	k1gInSc1
<g/>
)	)	kIx)
s	s	k7c7
délkou	délka	k1gFnSc7
přes	přes	k7c4
4	#num#	k4
metry	metr	k1gInPc4
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
dosahuje	dosahovat	k5eAaImIp3nS
v	v	k7c6
průměru	průměr	k1gInSc6
jen	jen	k9
okolo	okolo	k7c2
dvou	dva	k4xCgFnPc2
<g/>
,	,	kIx,
zřídka	zřídka	k6eAd1
do	do	k7c2
2,5	2,5	k4
metru	metr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maximální	maximální	k2eAgFnSc1d1
oficiálně	oficiálně	k6eAd1
ověřená	ověřený	k2eAgFnSc1d1
délka	délka	k1gFnSc1
taipana	taipana	k1gFnSc1
byla	být	k5eAaImAgFnS
276	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
neoficiální	oficiální	k2eNgInPc1d1,k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
uvádějí	uvádět	k5eAaImIp3nP
až	až	k9
přes	přes	k7c4
tři	tři	k4xCgInPc4
metry	metr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Anakonda	anakonda	k1gFnSc1
velká	velký	k2eAgFnSc1d1
</s>
<s>
Nejtěžší	těžký	k2eAgMnSc1d3
had	had	k1gMnSc1
</s>
<s>
Nejtěžším	těžký	k2eAgMnSc7d3
hadem	had	k1gMnSc7
světa	svět	k1gInSc2
je	být	k5eAaImIp3nS
anakonda	anakonda	k1gFnSc1
velká	velký	k2eAgFnSc1d1
(	(	kIx(
<g/>
Eunectes	Eunectes	k1gMnSc1
murinus	murinus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
žijící	žijící	k2eAgFnSc1d1
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
a	a	k8xC
na	na	k7c6
Trinidadu	Trinidad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
mohutnosti	mohutnost	k1gFnSc3
svého	svůj	k3xOyFgNnSc2
těla	tělo	k1gNnSc2
v	v	k7c6
tomto	tento	k3xDgInSc6
ohledu	ohled	k1gInSc6
překonává	překonávat	k5eAaImIp3nS
i	i	k9
krajtu	krajta	k1gFnSc4
mřížkovanou	mřížkovaný	k2eAgFnSc4d1
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
je	být	k5eAaImIp3nS
menší	malý	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
zatím	zatím	k6eAd1
největšího	veliký	k2eAgInSc2d3
doloženého	doložený	k2eAgInSc2d1
exempláře	exemplář	k1gInSc2
o	o	k7c6
délce	délka	k1gFnSc6
8,5	8,5	k4
m	m	kA
byla	být	k5eAaImAgFnS
váha	váha	k1gFnSc1
odhadnuta	odhadnout	k5eAaPmNgFnS
na	na	k7c4
500	#num#	k4
liber	libra	k1gFnPc2
(	(	kIx(
<g/>
227	#num#	k4
kg	kg	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejtěžší	těžký	k2eAgMnSc1d3
jedovatý	jedovatý	k2eAgMnSc1d1
had	had	k1gMnSc1
</s>
<s>
Za	za	k7c4
nejtěžšího	těžký	k2eAgMnSc4d3
jedovatého	jedovatý	k2eAgMnSc4d1
hada	had	k1gMnSc4
je	být	k5eAaImIp3nS
považován	považován	k2eAgMnSc1d1
chřestýš	chřestýš	k1gMnSc1
diamantový	diamantový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Crotalus	Crotalus	k1gMnSc1
adamanteus	adamanteus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
při	pře	k1gFnSc4
zhruba	zhruba	k6eAd1
2,5	2,5	k4
metrové	metrový	k2eAgFnSc6d1
délce	délka	k1gFnSc6
může	moct	k5eAaImIp3nS
dosáhnout	dosáhnout	k5eAaPmF
až	až	k9
13	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejdelší	dlouhý	k2eAgMnSc1d3
chřestýšovitý	chřestýšovitý	k2eAgMnSc1d1
had	had	k1gMnSc1
</s>
<s>
Zmije	zmije	k1gFnSc1
gabunská	gabunský	k2eAgFnSc1d1
</s>
<s>
Se	s	k7c7
svými	svůj	k3xOyFgFnPc7
až	až	k9
360	#num#	k4
cm	cm	kA
je	být	k5eAaImIp3nS
nejdelším	dlouhý	k2eAgMnSc7d3
chřestýšovitým	chřestýšovitý	k2eAgMnSc7d1
hadem	had	k1gMnSc7
americký	americký	k2eAgMnSc1d1
křovinář	křovinář	k1gMnSc1
němý	němý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Lachesis	Lachesis	k1gInSc1
muta	mut	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
ještě	ještě	k6eAd1
jeden	jeden	k4xCgInSc4
primát	primát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
jediný	jediný	k2eAgMnSc1d1
chřestýš	chřestýš	k1gMnSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
není	být	k5eNaImIp3nS
živorodý	živorodý	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
klade	klást	k5eAaImIp3nS
vejce	vejce	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obývá	obývat	k5eAaImIp3nS
vlhčí	vlhký	k2eAgInSc4d2
pralesy	prales	k1gInPc4
a	a	k8xC
žije	žít	k5eAaImIp3nS
pozemním	pozemní	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Nejmohutnější	mohutný	k2eAgMnSc1d3
zmijovitý	zmijovitý	k2eAgMnSc1d1
had	had	k1gMnSc1
</s>
<s>
Nejmohutnějším	mohutný	k2eAgMnSc7d3
zmijovitým	zmijovitý	k2eAgMnSc7d1
hadem	had	k1gMnSc7
nejenom	nejenom	k6eAd1
Afriky	Afrika	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
prakticky	prakticky	k6eAd1
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
je	být	k5eAaImIp3nS
bezesporu	bezesporu	k9
zmije	zmije	k1gFnSc1
gabunská	gabunský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Bitis	Bitis	k1gFnSc1
gabonica	gabonica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dorůstá	dorůstat	k5eAaImIp3nS
až	až	k9
přes	přes	k7c4
dva	dva	k4xCgInPc4
metry	metr	k1gInPc4
a	a	k8xC
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
silnější	silný	k2eAgNnSc4d2
než	než	k8xS
lidské	lidský	k2eAgNnSc4d1
stehno	stehno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
má	mít	k5eAaImIp3nS
i	i	k9
nejdelší	dlouhý	k2eAgInPc4d3
zuby	zub	k1gInPc4
mezi	mezi	k7c4
hady	had	k1gMnPc4
<g/>
,	,	kIx,
které	který	k3yQgMnPc4,k3yIgMnPc4,k3yRgMnPc4
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
dlouhé	dlouhý	k2eAgInPc1d1
až	až	k9
5	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stáří	stáří	k1gNnSc1
</s>
<s>
Vývojově	vývojově	k6eAd1
nejstarší	starý	k2eAgMnPc1d3
hadi	had	k1gMnPc1
</s>
<s>
Mezi	mezi	k7c4
vývojově	vývojově	k6eAd1
nejstarší	starý	k2eAgMnPc4d3
a	a	k8xC
nejprimitivnější	primitivní	k2eAgMnPc4d3
hady	had	k1gMnPc4
na	na	k7c6
planetě	planeta	k1gFnSc6
patří	patřit	k5eAaImIp3nP
zástupci	zástupce	k1gMnPc1
slepanovitých	slepanovitý	k2eAgFnPc2d1
(	(	kIx(
<g/>
Leptotyphlopidae	Leptotyphlopidae	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
slepákovitých	slepákovitý	k2eAgFnPc2d1
(	(	kIx(
<g/>
Typhlopidae	Typhlopidae	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vinejšovitých	vinejšovitý	k2eAgFnPc2d1
(	(	kIx(
<g/>
Cylindropheidae	Cylindropheidae	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
říci	říct	k5eAaPmF
ještě	ještě	k6eAd1
hroznýšovitých	hroznýšovitý	k2eAgFnPc2d1
(	(	kIx(
<g/>
Boidae	Boidae	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
krajtovitých	krajtovitý	k2eAgFnPc2d1
(	(	kIx(
<g/>
Pythonidae	Pythonidae	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
některých	některý	k3yIgMnPc2
druhů	druh	k1gInPc2
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc1
fakt	fakt	k1gInSc1
podpořen	podpořit	k5eAaPmNgInS
patrnými	patrný	k2eAgInPc7d1
malými	malý	k2eAgInPc7d1
drápky	drápek	k1gInPc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
ocasu	ocas	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jsou	být	k5eAaImIp3nP
pozůstatky	pozůstatek	k1gInPc1
po	po	k7c6
ještěřích	ještěří	k2eAgInPc6d1
předcích	předek	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3
známý	známý	k2eAgMnSc1d1
had	had	k1gMnSc1
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3
známý	známý	k2eAgMnSc1d1
had	had	k1gMnSc1
byl	být	k5eAaImAgMnS
Eophis	Eophis	k1gInSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
byl	být	k5eAaImAgInS
objeven	objeven	k2eAgInSc1d1
roku	rok	k1gInSc2
2015	#num#	k4
a	a	k8xC
žil	žít	k5eAaImAgInS
před	před	k7c7
167	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Anglii	Anglie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
témže	týž	k3xTgInSc6
roce	rok	k1gInSc6
byli	být	k5eAaImAgMnP
nalezeni	naleznout	k5eAaPmNgMnP,k5eAaBmNgMnP
ještě	ještě	k9
tři	tři	k4xCgMnPc1
další	další	k2eAgMnPc1d1
hadi	had	k1gMnPc1
z	z	k7c2
velmi	velmi	k6eAd1
dávné	dávný	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
Eophis	Eophis	k1gInSc1
zůstává	zůstávat	k5eAaImIp3nS
nejstarším	starý	k2eAgInSc7d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
nálezy	nález	k1gInPc1
posunuly	posunout	k5eAaPmAgFnP
vznik	vznik	k1gInSc4
hadů	had	k1gMnPc2
o	o	k7c4
sto	sto	k4xCgNnSc4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
do	do	k7c2
minulosti	minulost	k1gFnSc2
<g/>
,	,	kIx,
až	až	k9
do	do	k7c2
období	období	k1gNnSc2
střední	střední	k2eAgFnSc2d1
jury	jura	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Předtím	předtím	k6eAd1
byl	být	k5eAaImAgInS
za	za	k7c4
nejstarší	starý	k2eAgInSc4d3
pozemní	pozemní	k2eAgInSc4d1
druh	druh	k1gInSc4
považován	považován	k2eAgInSc1d1
Lapparentophis	Lapparentophis	k1gInSc1
defrennei	defrenne	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
žil	žít	k5eAaImAgMnS
na	na	k7c6
území	území	k1gNnSc6
dnešní	dnešní	k2eAgFnSc2d1
severní	severní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
před	před	k7c7
100	#num#	k4
<g/>
–	–	k?
<g/>
150	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
rekord	rekord	k1gInSc1
drží	držet	k5eAaImIp3nS
mořský	mořský	k2eAgInSc1d1
druh	druh	k1gInSc1
hada	had	k1gMnSc2
Simoliophis	Simoliophis	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgInS
přibližně	přibližně	k6eAd1
před	před	k7c7
100	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
a	a	k8xC
obýval	obývat	k5eAaImAgInS
mořské	mořský	k2eAgNnSc4d1
dno	dno	k1gNnSc4
v	v	k7c6
okolí	okolí	k1gNnSc6
Evropy	Evropa	k1gFnSc2
a	a	k8xC
severní	severní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
nálezy	nález	k1gInPc1
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
Izraele	Izrael	k1gInSc2
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
druhy	druh	k1gInPc4
Pachyrhachis	Pachyrhachis	k1gFnPc2
a	a	k8xC
Haasiophis	Haasiophis	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc4
byli	být	k5eAaImAgMnP
vybaveni	vybaven	k2eAgMnPc1d1
zakrnělýma	zakrnělý	k2eAgFnPc7d1
nohama	noha	k1gFnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
podle	podle	k7c2
výzkumů	výzkum	k1gInPc2
už	už	k6eAd1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejstarší	starý	k2eAgMnPc4d3
známé	známý	k2eAgMnPc4d1
hady	had	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žili	žít	k5eAaImAgMnP
před	před	k7c7
95	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
je	být	k5eAaImIp3nS
vývojová	vývojový	k2eAgFnSc1d1
řada	řada	k1gFnSc1
přerušena	přerušit	k5eAaPmNgFnS
a	a	k8xC
další	další	k2eAgInPc1d1
nálezy	nález	k1gInPc1
pocházejí	pocházet	k5eAaImIp3nP
až	až	k9
z	z	k7c2
doby	doba	k1gFnSc2
mnohem	mnohem	k6eAd1
pozdější	pozdní	k2eAgMnSc1d2
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
před	před	k7c7
65	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Vývojově	vývojově	k6eAd1
nejmladší	mladý	k2eAgMnPc1d3
hadi	had	k1gMnPc1
</s>
<s>
Naopak	naopak	k6eAd1
evolučně	evolučně	k6eAd1
nejmladší	mladý	k2eAgMnPc1d3
jsou	být	k5eAaImIp3nP
zmijovití	zmijovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Viperidae	Viperidae	k1gNnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mezi	mezi	k7c4
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
patří	patřit	k5eAaImIp3nP
i	i	k9
chřestýši	chřestýš	k1gMnPc1
(	(	kIx(
<g/>
Crotalinae	Crotalinae	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zmijovité	zmijovitý	k2eAgInPc1d1
najdeme	najít	k5eAaPmIp1nP
téměř	téměř	k6eAd1
na	na	k7c6
všech	všecek	k3xTgInPc6
světadílech	světadíl	k1gInPc6
kromě	kromě	k7c2
Austrálie	Austrálie	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
svědčí	svědčit	k5eAaImIp3nS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
vyvinuli	vyvinout	k5eAaPmAgMnP
až	až	k9
po	po	k7c6
jejím	její	k3xOp3gNnSc6
oddělení	oddělení	k1gNnSc6
od	od	k7c2
ostatních	ostatní	k2eAgInPc2d1
kontinentů	kontinent	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Hroznýš	hroznýš	k1gMnSc1
královský	královský	k2eAgMnSc1d1
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3
had	had	k1gMnSc1
</s>
<s>
Délka	délka	k1gFnSc1
života	život	k1gInSc2
u	u	k7c2
hadů	had	k1gMnPc2
se	se	k3xPyFc4
různí	různit	k5eAaImIp3nS
druh	druh	k1gInSc1
od	od	k7c2
druhu	druh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
hadi	had	k1gMnPc1
jako	jako	k8xC,k8xS
takoví	takový	k3xDgMnPc1
jsou	být	k5eAaImIp3nP
poměrně	poměrně	k6eAd1
dlouhověcí	dlouhověký	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chřestýši	chřestýš	k1gMnSc3
se	se	k3xPyFc4
v	v	k7c6
zajetí	zajetí	k1gNnSc6
dožívají	dožívat	k5eAaImIp3nP
průměrně	průměrně	k6eAd1
10	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
užovka	užovka	k1gFnSc1
červená	červený	k2eAgFnSc1d1
(	(	kIx(
<g/>
Elaphe	Elaphe	k1gFnSc1
guttata	guttata	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
dožila	dožít	k5eAaPmAgFnS
21	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarším	starý	k2eAgMnSc7d3
hadem	had	k1gMnSc7
ve	v	k7c6
volné	volný	k2eAgFnSc6d1
přírodě	příroda	k1gFnSc6
je	být	k5eAaImIp3nS
také	také	k9
užovka	užovka	k1gFnSc1
<g/>
,	,	kIx,
druh	druh	k1gInSc1
Elaphe	Elaphe	k1gInSc1
obsoleta	obsoleta	k1gFnSc1
(	(	kIx(
<g/>
užovka	užovka	k1gFnSc1
černá	černý	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dožívající	dožívající	k2eAgFnSc4d1
se	se	k3xPyFc4
až	až	k9
30	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Velcí	velký	k2eAgMnPc1d1
hadi	had	k1gMnPc1
se	se	k3xPyFc4
v	v	k7c6
zajetí	zajetí	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
mají	mít	k5eAaImIp3nP
ideální	ideální	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
<g/>
,	,	kIx,
dožívají	dožívat	k5eAaImIp3nP
až	až	k9
několika	několik	k4yIc2
desítek	desítka	k1gFnPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samec	samec	k1gInSc1
běžného	běžný	k2eAgInSc2d1
druhu	druh	k1gInSc2
hroznýš	hroznýš	k1gMnSc1
královský	královský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Boa	boa	k1gFnSc1
constrictor	constrictor	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nazývaný	nazývaný	k2eAgInSc1d1
„	„	k?
<g/>
Popeye	Popeye	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
Pepek	Pepek	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
uhynul	uhynout	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
40	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
3	#num#	k4
měsíce	měsíc	k1gInSc2
a	a	k8xC
14	#num#	k4
dní	den	k1gInPc2
v	v	k7c6
zoo	zoo	k1gFnSc6
Philadelphia	Philadelphia	k1gFnSc1
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Největší	veliký	k2eAgMnSc1d3
pravěký	pravěký	k2eAgMnSc1d1
had	had	k1gMnSc1
</s>
<s>
Je	být	k5eAaImIp3nS
jím	on	k3xPp3gNnSc7
obří	obří	k2eAgFnSc1d1
Titanoboa	Titanobo	k2eAgFnSc1d1
cerrejonensis	cerrejonensis	k1gFnSc1
<g/>
,	,	kIx,
žijící	žijící	k2eAgFnSc1d1
v	v	k7c6
období	období	k1gNnSc6
třetihorního	třetihorní	k2eAgInSc2d1
paleocénu	paleocén	k1gInSc2
(	(	kIx(
<g/>
asi	asi	k9
před	před	k7c7
60	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
)	)	kIx)
v	v	k7c6
jihoamerické	jihoamerický	k2eAgFnSc6d1
Kolumbii	Kolumbie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgMnSc1
ohromný	ohromný	k2eAgMnSc1d1
had	had	k1gMnSc1
dosahoval	dosahovat	k5eAaImAgInS
dle	dle	k7c2
odhadů	odhad	k1gInPc2
paleontologů	paleontolog	k1gMnPc2
délku	délka	k1gFnSc4
asi	asi	k9
12	#num#	k4
až	až	k9
15	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
hmotnost	hmotnost	k1gFnSc1
přes	přes	k7c4
jednu	jeden	k4xCgFnSc4
tunu	tuna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tak	tak	k6eAd1
největším	veliký	k2eAgMnSc7d3
dosud	dosud	k6eAd1
známým	známý	k2eAgMnSc7d1
hadem	had	k1gMnSc7
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
</s>
<s>
Nejvzácnější	vzácný	k2eAgMnSc1d3
hroznýšovitý	hroznýšovitý	k2eAgMnSc1d1
had	had	k1gMnSc1
</s>
<s>
Nejvzácnějším	vzácný	k2eAgMnSc7d3
hroznýšovitým	hroznýšovitý	k2eAgMnSc7d1
hadem	had	k1gMnSc7
je	být	k5eAaImIp3nS
podle	podle	k7c2
IUCN	IUCN	kA
psohlavec	psohlavec	k1gMnSc1
Cropaniův	Cropaniův	k2eAgMnSc1d1
(	(	kIx(
<g/>
Corallus	Corallus	k1gMnSc1
cropanii	cropanie	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
dnešních	dnešní	k2eAgInPc2d1
dnů	den	k1gInPc2
jsou	být	k5eAaImIp3nP
známy	znám	k2eAgInPc1d1
pouhé	pouhý	k2eAgInPc1d1
čtyři	čtyři	k4xCgInPc1
exempláře	exemplář	k1gInPc1
<g/>
,	,	kIx,
ulovené	ulovený	k2eAgFnPc1d1
nedaleko	nedaleko	k7c2
Sao	Sao	k1gMnSc2
Paula	Paul	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejjedovatější	jedovatý	k2eAgMnPc1d3
hadi	had	k1gMnPc1
</s>
<s>
Nejjedovatějším	jedovatý	k2eAgMnSc7d3
hadem	had	k1gMnSc7
světa	svět	k1gInSc2
je	být	k5eAaImIp3nS
australský	australský	k2eAgInSc1d1
taipan	taipan	k1gInSc1
menší	malý	k2eAgInSc1d2
-	-	kIx~
Inland	Inland	k1gInSc1
Taipan	Taipan	k1gMnSc1
(	(	kIx(
<g/>
Oxyuranus	Oxyuranus	k1gMnSc1
microlepidotus	microlepidotus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gInSc1
index	index	k1gInSc1
jedovatosti	jedovatost	k1gFnSc2
LD	LD	kA
<g/>
50	#num#	k4
<g/>
s	s	k7c7
je	být	k5eAaImIp3nS
0,025	0,025	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jed	jed	k1gInSc4
z	z	k7c2
jeho	jeho	k3xOp3gNnPc2
uštknutí	uštknutí	k1gNnPc2
by	by	kYmCp3nS
dokázal	dokázat	k5eAaPmAgInS
zabít	zabít	k5eAaPmF
100	#num#	k4
lidí	člověk	k1gMnPc2
a	a	k8xC
nebo	nebo	k8xC
250	#num#	k4
tisíc	tisíc	k4xCgInPc2
myší	myš	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
uštknutí	uštknutí	k1gNnSc1
by	by	kYmCp3nS
teoreticky	teoreticky	k6eAd1
mohlo	moct	k5eAaImAgNnS
zabít	zabít	k5eAaPmF
člověka	člověk	k1gMnSc4
už	už	k6eAd1
za	za	k7c4
40	#num#	k4
sekund	sekunda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
v	v	k7c6
řídce	řídce	k6eAd1
obydlených	obydlený	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
jihozápadního	jihozápadní	k2eAgInSc2d1
Queenslandu	Queensland	k1gInSc2
<g/>
,	,	kIx,
smrtelný	smrtelný	k2eAgInSc1d1
útok	útok	k1gInSc1
na	na	k7c4
člověka	člověk	k1gMnSc4
není	být	k5eNaImIp3nS
znám	znám	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhým	druhý	k4xOgInSc7
nejjedovatějším	jedovatý	k2eAgMnSc7d3
suchozemským	suchozemský	k2eAgMnSc7d1
hadem	had	k1gMnSc7
je	být	k5eAaImIp3nS
pakobra	pakobra	k6eAd1
východní	východní	k2eAgInSc1d1
-	-	kIx~
Eastern	Eastern	k1gInSc1
Brown	Brown	k1gNnSc1
Snake	Snake	k1gInSc1
(	(	kIx(
<g/>
Pseudonaja	Pseudonaj	k2eAgFnSc1d1
textilis	textilis	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
had	had	k1gMnSc1
žijící	žijící	k2eAgMnSc1d1
po	po	k7c6
celé	celý	k2eAgFnSc6d1
Austrálii	Austrálie	k1gFnSc6
<g/>
,	,	kIx,
již	již	k6eAd1
člověku	člověk	k1gMnSc3
významně	významně	k6eAd1
nebezpečný	bezpečný	k2eNgInSc1d1
s	s	k7c7
občasnými	občasný	k2eAgFnPc7d1
oběťmi	oběť	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
je	být	k5eAaImIp3nS
taipan	taipan	k1gInSc1
velký	velký	k2eAgInSc1d1
-	-	kIx~
Coastal	Coastal	k1gMnSc1
Taipan	Taipan	k1gMnSc1
(	(	kIx(
<g/>
Oxyuranus	Oxyuranus	k1gMnSc1
scutellatus	scutellatus	k1gMnSc1
<g/>
)	)	kIx)
označovaný	označovaný	k2eAgMnSc1d1
často	často	k6eAd1
jen	jen	k6eAd1
Taipan	Taipan	k1gInSc1
je	být	k5eAaImIp3nS
již	již	k6eAd1
čtyřikrát	čtyřikrát	k6eAd1
méně	málo	k6eAd2
jedovatý	jedovatý	k2eAgInSc1d1
než	než	k8xS
taipan	taipan	k1gInSc1
menší	malý	k2eAgInSc1d2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
disponuje	disponovat	k5eAaBmIp3nS
větším	veliký	k2eAgNnSc7d2
množstvím	množství	k1gNnSc7
jedu	jed	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vodnář	vodnář	k1gMnSc1
Belcherův	Belcherův	k2eAgMnSc1d1
</s>
<s>
Ovšem	ovšem	k9
člověku	člověk	k1gMnSc3
nejnebezpečnější	bezpečný	k2eNgMnSc1d3
<g/>
,	,	kIx,
tj.	tj.	kA
hadi	had	k1gMnPc1
s	s	k7c7
nejvíce	hodně	k6eAd3,k6eAd1
oběťmi	oběť	k1gFnPc7
či	či	k8xC
největší	veliký	k2eAgFnPc4d3
úmrtnosti	úmrtnost	k1gFnSc2
po	po	k7c6
uštknutí	uštknutí	k1gNnSc6
jsou	být	k5eAaImIp3nP
v	v	k7c6
žebříčku	žebříček	k1gInSc6
jedovatosti	jedovatost	k1gFnSc2
až	až	k9
na	na	k7c6
dalších	další	k2eAgNnPc6d1
místech	místo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
až	až	k9
na	na	k7c6
čtrnáctém	čtrnáctý	k4xOgInSc6
místě	místo	k1gNnSc6
je	být	k5eAaImIp3nS
had	had	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
budí	budit	k5eAaImIp3nS
největší	veliký	k2eAgFnPc4d3
obavy	obava	k1gFnPc4
a	a	k8xC
úmrtnost	úmrtnost	k1gFnSc4
neléčených	léčený	k2eNgNnPc2d1
uštknutí	uštknutí	k1gNnPc2
je	být	k5eAaImIp3nS
blízká	blízký	k2eAgFnSc1d1
100	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
mambu	mamba	k1gFnSc4
černou	černý	k2eAgFnSc4d1
(	(	kIx(
<g/>
Dendroaspis	Dendroaspis	k1gFnSc4
polylepis	polylepis	k1gFnSc2
<g/>
)	)	kIx)
žijící	žijící	k2eAgMnSc1d1
v	v	k7c6
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smrtící	smrtící	k2eAgInSc4d1
účinek	účinek	k1gInSc4
jedu	jed	k1gInSc2
mamby	mamba	k1gFnSc2
se	se	k3xPyFc4
zpravidla	zpravidla	k6eAd1
projeví	projevit	k5eAaPmIp3nS
od	od	k7c2
15	#num#	k4
minut	minuta	k1gFnPc2
do	do	k7c2
3	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napadení	napadení	k1gNnSc1
člověka	člověk	k1gMnSc2
nejsou	být	k5eNaImIp3nP
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
následujícím	následující	k2eAgMnSc7d1
hadem	had	k1gMnSc7
častá	častý	k2eAgNnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	nejvíce	k6eAd1,k6eAd3
lidských	lidský	k2eAgFnPc2d1
obětí	oběť	k1gFnPc2
má	mít	k5eAaImIp3nS
osmý	osmý	k4xOgMnSc1
nejjedovatější	jedovatý	k2eAgMnSc1d3
suchozemský	suchozemský	k2eAgMnSc1d1
had	had	k1gMnSc1
zmije	zmije	k1gFnSc2
paví	paví	k2eAgFnSc2d1
žijící	žijící	k2eAgFnSc2d1
v	v	k7c6
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ač	ač	k8xS
úmrtnost	úmrtnost	k1gFnSc1
neléčených	léčený	k2eNgNnPc2d1
uštknutí	uštknutí	k1gNnPc2
je	být	k5eAaImIp3nS
„	„	k?
<g/>
jen	jen	k9
<g/>
“	“	k?
asi	asi	k9
30	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
díky	díky	k7c3
častým	častý	k2eAgInPc3d1
útokům	útok	k1gInPc3
na	na	k7c4
člověka	člověk	k1gMnSc4
v	v	k7c6
hustě	hustě	k6eAd1
obydlených	obydlený	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
(	(	kIx(
<g/>
hojně	hojně	k6eAd1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
Indii	Indie	k1gFnSc6
a	a	k8xC
dalších	další	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
)	)	kIx)
se	s	k7c7
špatnou	špatný	k2eAgFnSc7d1
dostupností	dostupnost	k1gFnSc7
lékařské	lékařský	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
,	,	kIx,
způsobí	způsobit	k5eAaPmIp3nS
na	na	k7c6
světě	svět	k1gInSc6
nejvíce	hodně	k6eAd3,k6eAd1
úmrtí	úmrtí	k1gNnSc4
ze	z	k7c2
všech	všecek	k3xTgMnPc2
hadů	had	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
nejjedovatějšího	jedovatý	k2eAgMnSc2d3
hada	had	k1gMnSc2
světa	svět	k1gInSc2
byl	být	k5eAaImAgInS
dříve	dříve	k6eAd2
považován	považován	k2eAgMnSc1d1
mořský	mořský	k2eAgMnSc1d1
had	had	k1gMnSc1
vodnář	vodnář	k1gMnSc1
Belcherův	Belcherův	k2eAgMnSc1d1
(	(	kIx(
<g/>
Hydrophis	Hydrophis	k1gInSc1
belcheri	belcher	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ovšem	ovšem	k9
poslední	poslední	k2eAgFnSc1d1
studie	studie	k1gFnSc1
uvádí	uvádět	k5eAaImIp3nS
nižší	nízký	k2eAgFnSc1d2
jedovatost	jedovatost	k1gFnSc1
než	než	k8xS
nejjedovatější	jedovatý	k2eAgMnPc1d3
suchozemští	suchozemský	k2eAgMnPc1d1
hadi	had	k1gMnPc1
uvedení	uvedení	k1gNnSc4
výše	vysoce	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Ať	ať	k8xC,k8xS
je	být	k5eAaImIp3nS
pravda	pravda	k1gFnSc1
o	o	k7c4
jeho	jeho	k3xOp3gFnPc4
jedovatosti	jedovatost	k1gFnPc4
jakákoliv	jakýkoliv	k3yIgFnSc1
<g/>
,	,	kIx,
mořští	mořský	k2eAgMnPc1d1
hadi	had	k1gMnPc1
mají	mít	k5eAaImIp3nP
pro	pro	k7c4
uštknutí	uštknutí	k1gNnSc4
člověka	člověk	k1gMnSc2
malá	malý	k2eAgNnPc4d1
ústa	ústa	k1gNnPc4
a	a	k8xC
nejsou	být	k5eNaImIp3nP
příliš	příliš	k6eAd1
agresivní	agresivní	k2eAgFnPc1d1
a	a	k8xC
pokud	pokud	k8xS
už	už	k6eAd1
uštknou	uštknout	k5eAaPmIp3nP
<g/>
,	,	kIx,
vpraví	vpravit	k5eAaPmIp3nP
do	do	k7c2
rány	rána	k1gFnSc2
obvykle	obvykle	k6eAd1
malé	malý	k2eAgNnSc1d1
množství	množství	k1gNnSc1
jedu	jet	k5eAaImIp1nS
<g/>
,	,	kIx,
proto	proto	k8xC
mořští	mořský	k2eAgMnPc1d1
hadi	had	k1gMnPc1
nejsou	být	k5eNaImIp3nP
pro	pro	k7c4
člověka	člověk	k1gMnSc4
zdaleka	zdaleka	k6eAd1
tak	tak	k9
nebezpeční	bezpečný	k2eNgMnPc1d1
jako	jako	k8xC,k8xS
suchozemští	suchozemský	k2eAgMnPc1d1
hadi	had	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimkou	výjimka	k1gFnSc7
je	být	k5eAaImIp3nS
vodnář	vodnář	k1gMnSc1
kobří	kobří	k2eAgMnSc1d1
(	(	kIx(
<g/>
Enhydrina	Enhydrin	k2eAgFnSc1d1
schistosa	schistosa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
je	být	k5eAaImIp3nS
agresivní	agresivní	k2eAgMnSc1d1
a	a	k8xC
způsobí	způsobit	k5eAaPmIp3nS
90	#num#	k4
%	%	kIx~
smrtelných	smrtelný	k2eAgInPc2d1
útoků	útok	k1gInPc2
mořskými	mořský	k2eAgFnPc7d1
hady	had	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mořští	mořský	k2eAgMnPc1d1
hadi	had	k1gMnPc1
obvykle	obvykle	k6eAd1
disponují	disponovat	k5eAaBmIp3nP
velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
působícím	působící	k2eAgInSc7d1
jedem	jed	k1gInSc7
především	především	k6eAd1
proto	proto	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jimi	on	k3xPp3gMnPc7
zasažená	zasažený	k2eAgFnSc1d1
kořist	kořist	k1gFnSc1
nestihla	stihnout	k5eNaPmAgFnS
uplavat	uplavat	k5eAaPmF
daleko	daleko	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3
rozšíření	rozšíření	k1gNnSc1
jedovatých	jedovatý	k2eAgMnPc2d1
hadů	had	k1gMnPc2
</s>
<s>
Primát	primát	k1gInSc1
v	v	k7c6
počtu	počet	k1gInSc6
jedovatých	jedovatý	k2eAgMnPc2d1
hadů	had	k1gMnPc2
má	mít	k5eAaImIp3nS
bezesporu	bezesporu	k9
Austrálie	Austrálie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
kontinent	kontinent	k1gInSc1
má	mít	k5eAaImIp3nS
bohatou	bohatý	k2eAgFnSc4d1
a	a	k8xC
neobvyklou	obvyklý	k2eNgFnSc4d1
hadí	hadí	k2eAgFnSc4d1
faunu	fauna	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
diametrálně	diametrálně	k6eAd1
odlišuje	odlišovat	k5eAaImIp3nS
od	od	k7c2
okolního	okolní	k2eAgInSc2d1
světa	svět	k1gInSc2
-	-	kIx~
ostatně	ostatně	k6eAd1
tak	tak	k9
jako	jako	k9
i	i	k9
ostatní	ostatní	k2eAgFnSc1d1
fauna	fauna	k1gFnSc1
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
143	#num#	k4
druhů	druh	k1gInPc2
zde	zde	k6eAd1
žijících	žijící	k2eAgMnPc2d1
hadů	had	k1gMnPc2
je	být	k5eAaImIp3nS
jenom	jenom	k9
11	#num#	k4
druhů	druh	k1gInPc2
užovkovitých	užovkovitý	k2eAgInPc2d1
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
na	na	k7c6
všech	všecek	k3xTgInPc6
ostatních	ostatní	k2eAgInPc6d1
kontinentech	kontinent	k1gInPc6
užovky	užovka	k1gFnSc2
dominují	dominovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zato	zato	k6eAd1
zde	zde	k6eAd1
žije	žít	k5eAaImIp3nS
na	na	k7c4
65	#num#	k4
druhů	druh	k1gInPc2
korálovcovitých	korálovcovitý	k2eAgMnPc2d1
hadů	had	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
plná	plný	k2eAgFnSc1d1
třetina	třetina	k1gFnSc1
jejich	jejich	k3xOp3gInSc2
celosvětového	celosvětový	k2eAgInSc2d1
počtu	počet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
hadi	had	k1gMnPc1
využili	využít	k5eAaPmAgMnP
vhodných	vhodný	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
a	a	k8xC
absenci	absence	k1gFnSc4
přirozených	přirozený	k2eAgMnPc2d1
predátorů	predátor	k1gMnPc2
a	a	k8xC
obsadili	obsadit	k5eAaPmAgMnP
téměř	téměř	k6eAd1
každý	každý	k3xTgInSc4
volný	volný	k2eAgInSc4d1
biotop	biotop	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálie	Austrálie	k1gFnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
stala	stát	k5eAaPmAgFnS
kontinentem	kontinent	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
nejvíce	hodně	k6eAd3,k6eAd1
druhů	druh	k1gInPc2
jedovatých	jedovatý	k2eAgMnPc2d1
hadů	had	k1gMnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Nejdelší	dlouhý	k2eAgInPc1d3
jedové	jedový	k2eAgInPc1d1
zuby	zub	k1gInPc1
</s>
<s>
Nejdelší	dlouhý	k2eAgInPc4d3
jedové	jedový	k2eAgInPc4d1
zuby	zub	k1gInPc4
mají	mít	k5eAaImIp3nP
zmijovití	zmijovitý	k2eAgMnPc1d1
a	a	k8xC
chřestýšovití	chřestýšovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Solenoglypha	Solenoglypha	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nacházejí	nacházet	k5eAaImIp3nP
se	se	k3xPyFc4
vpředu	vpředu	k6eAd1
na	na	k7c6
horní	horní	k2eAgFnSc6d1
čelisti	čelist	k1gFnSc6
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
duté	dutý	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
injekční	injekční	k2eAgFnPc1d1
stříkačka	stříkačka	k1gFnSc1
a	a	k8xC
v	v	k7c6
klidu	klid	k1gInSc6
směřují	směřovat	k5eAaImIp3nP
dozadu	dozadu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
v	v	k7c6
tlamě	tlama	k1gFnSc6
složené	složený	k2eAgInPc1d1
téměř	téměř	k6eAd1
ve	v	k7c6
vodorovné	vodorovný	k2eAgFnSc6d1
poloze	poloha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
při	při	k7c6
kousnutí	kousnutí	k1gNnSc6
<g/>
,	,	kIx,
když	když	k8xS
had	had	k1gMnSc1
otevře	otevřít	k5eAaPmIp3nS
tlamu	tlama	k1gFnSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
zuby	zub	k1gInPc7
vztyčí	vztyčit	k5eAaPmIp3nP
do	do	k7c2
svislé	svislý	k2eAgFnSc2d1
polohy	poloha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rekordmanem	rekordman	k1gMnSc7
v	v	k7c6
délce	délka	k1gFnSc6
jedových	jedový	k2eAgInPc2d1
zubů	zub	k1gInPc2
je	být	k5eAaImIp3nS
zmije	zmije	k1gFnSc1
gabunská	gabunský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Bitis	Bitis	k1gFnSc1
gabonica	gabonica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
mimochodem	mimochodem	k9
i	i	k9
nejmohutnější	mohutný	k2eAgFnSc7d3
zmijí	zmije	k1gFnSc7
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInSc1
zuby	zub	k1gInPc1
dosahují	dosahovat	k5eAaImIp3nP
délky	délka	k1gFnPc4
až	až	k9
50	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
některé	některý	k3yIgInPc1
prameny	pramen	k1gInPc1
uvádějí	uvádět	k5eAaImIp3nP
dokonce	dokonce	k9
až	až	k9
70	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
masivní	masivní	k2eAgFnSc3d1
stavbě	stavba	k1gFnSc3
její	její	k3xOp3gInPc4
zuby	zub	k1gInPc4
snadno	snadno	k6eAd1
proniknou	proniknout	k5eAaPmIp3nP
i	i	k9
tlustou	tlustý	k2eAgFnSc7d1
kůží	kůže	k1gFnSc7
kořisti	kořist	k1gFnSc2
a	a	k8xC
had	had	k1gMnSc1
může	moct	k5eAaImIp3nS
vstříknout	vstříknout	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
jed	jed	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3
imunita	imunita	k1gFnSc1
vůči	vůči	k7c3
hadímu	hadí	k2eAgInSc3d1
jedu	jed	k1gInSc3
</s>
<s>
Kobra	kobra	k1gFnSc1
královská	královský	k2eAgFnSc1d1
požírá	požírat	k5eAaImIp3nS
jiného	jiný	k2eAgMnSc4d1
hada	had	k1gMnSc4
</s>
<s>
Ze	z	k7c2
savců	savec	k1gMnPc2
i	i	k8xC
plazů	plaz	k1gMnPc2
jsou	být	k5eAaImIp3nP
přirozeně	přirozeně	k6eAd1
nejodolnější	odolný	k2eAgInPc4d3
ty	ten	k3xDgInPc4
druhy	druh	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
jedovatými	jedovatý	k2eAgFnPc7d1
hady	had	k1gMnPc4
přímo	přímo	k6eAd1
živí	živit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
proti	proti	k7c3
jedu	jed	k1gInSc3
kobry	kobra	k1gFnSc2
kapské	kapský	k2eAgNnSc1d1
(	(	kIx(
<g/>
Naja	Naj	k2eAgFnSc1d1
nivea	nivea	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
velice	velice	k6eAd1
odolná	odolný	k2eAgFnSc1d1
promyka	promyka	k1gFnSc1
mungo	mungo	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
snese	snést	k5eAaPmIp3nS
dávku	dávka	k1gFnSc4
téměř	téměř	k6eAd1
20	#num#	k4
mg	mg	kA
jedu	jed	k1gInSc2
tohoto	tento	k3xDgMnSc2
hada	had	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
představuje	představovat	k5eAaImIp3nS
dávku	dávka	k1gFnSc4
asi	asi	k9
100	#num#	k4
x	x	k?
větší	veliký	k2eAgMnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
jaká	jaký	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
usmrtí	usmrtit	k5eAaPmIp3nS
morče	morče	k1gNnSc4
o	o	k7c6
stejné	stejný	k2eAgFnSc6d1
hmotnosti	hmotnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejodolnější	odolný	k2eAgInSc1d3
ze	z	k7c2
savců	savec	k1gMnPc2
vůči	vůči	k7c3
jedu	jed	k1gInSc3
korálovcovitých	korálovcovitý	k2eAgInPc2d1
je	být	k5eAaImIp3nS
však	však	k9
jihoafrická	jihoafrický	k2eAgFnSc1d1
šelmička	šelmička	k1gFnSc1
surikata	surikata	k1gFnSc1
(	(	kIx(
<g/>
Suricata	Suricat	k2eAgFnSc1d1
suricata	suricata	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
poměru	poměr	k1gInSc6
k	k	k7c3
hmotnosti	hmotnost	k1gFnSc3
1000	#num#	k4
x	x	k?
odolnější	odolný	k2eAgMnSc1d2
vůči	vůči	k7c3
jedu	jed	k1gInSc3
kobry	kobra	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
třeba	třeba	k9
ovce	ovce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Surikata	surikata	k1gFnSc1
o	o	k7c6
hmotnosti	hmotnost	k1gFnSc6
pouhých	pouhý	k2eAgInPc2d1
500	#num#	k4
g	g	kA
snese	snést	k5eAaPmIp3nS
bez	bez	k7c2
jakýchkoli	jakýkoli	k3yIgInPc2
příznaků	příznak	k1gInPc2
dávku	dávka	k1gFnSc4
až	až	k9
10	#num#	k4
mg	mg	kA
jedu	jed	k1gInSc2
kobry	kobra	k1gFnSc2
kapské	kapský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
ježek	ježek	k1gMnSc1
evropský	evropský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Erinaceus	Erinaceus	k1gMnSc1
europaeus	europaeus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
velice	velice	k6eAd1
často	často	k6eAd1
loví	lovit	k5eAaImIp3nP
zmije	zmije	k1gFnPc1
obecné	obecná	k1gFnPc1
(	(	kIx(
<g/>
Vipera	Vipera	k1gFnSc1
berus	berus	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vůči	vůči	k7c3
jejich	jejich	k3xOp3gNnSc3
jedu	jet	k5eAaImIp1nS
velice	velice	k6eAd1
odolný	odolný	k2eAgInSc1d1
<g/>
,	,	kIx,
a	a	k8xC
snese	snést	k5eAaPmIp3nS
až	až	k9
40	#num#	k4
x	x	k?
větší	veliký	k2eAgFnSc4d2
dávku	dávka	k1gFnSc4
než	než	k8xS
morče	morče	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Značnou	značný	k2eAgFnSc4d1
odolnost	odolnost	k1gFnSc4
má	mít	k5eAaImIp3nS
též	též	k9
medojed	medojed	k1gMnSc1
kapský	kapský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
hadů	had	k1gMnPc2
jsou	být	k5eAaImIp3nP
odolné	odolný	k2eAgFnPc1d1
opět	opět	k6eAd1
druhy	druh	k1gMnPc4
<g/>
,	,	kIx,
lovící	lovící	k2eAgMnPc4d1
jedovaté	jedovatý	k2eAgMnPc4d1
hady	had	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
americké	americký	k2eAgFnSc2d1
korálovky	korálovka	k1gFnSc2
(	(	kIx(
<g/>
Lampropeltis	Lampropeltis	k1gInSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
více	hodně	k6eAd2
než	než	k8xS
100	#num#	k4
x	x	k?
odolnější	odolný	k2eAgMnSc1d2
proti	proti	k7c3
jedu	jed	k1gInSc3
chřestýšů	chřestýš	k1gMnPc2
<g/>
,	,	kIx,
než	než	k8xS
myš	myš	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
stejně	stejně	k6eAd1
odolná	odolný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
jihoamerická	jihoamerický	k2eAgFnSc1d1
musurana	musurana	k1gFnSc1
(	(	kIx(
<g/>
Clelia	Clelia	k1gFnSc1
clelia	clelia	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
ještě	ještě	k9
několik	několik	k4yIc4
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádný	žádný	k1gMnSc1
živočich	živočich	k1gMnSc1
však	však	k9
není	být	k5eNaImIp3nS
absolutně	absolutně	k6eAd1
imunní	imunní	k2eAgMnSc1d1
vůči	vůči	k7c3
hadímu	hadí	k2eAgInSc3d1
jedu	jed	k1gInSc3
a	a	k8xC
dokonce	dokonce	k9
ani	ani	k9
samotní	samotný	k2eAgMnPc1d1
jedovatí	jedovatý	k2eAgMnPc1d1
hadi	had	k1gMnPc1
nejsou	být	k5eNaImIp3nP
úplně	úplně	k6eAd1
imunní	imunní	k2eAgMnSc1d1
vůči	vůči	k7c3
jedu	jed	k1gInSc3
vlastnímu	vlastní	k2eAgInSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
odolnost	odolnost	k1gFnSc4
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
velká	velká	k1gFnSc1
-	-	kIx~
např.	např.	kA
pakobra	pakobra	k6eAd1
páskovaná	páskovaný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Notechis	Notechis	k1gFnSc1
scutatus	scutatus	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
asi	asi	k9
180	#num#	k4
000	#num#	k4
x	x	k?
odolnější	odolný	k2eAgMnSc1d2
vůči	vůči	k7c3
vlastnímu	vlastní	k2eAgInSc3d1
jedu	jed	k1gInSc3
než	než	k8xS
morče	morče	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
už	už	k6eAd1
jen	jen	k9
13	#num#	k4
000	#num#	k4
x	x	k?
odolnější	odolný	k2eAgMnSc1d2
proti	proti	k7c3
jedu	jed	k1gInSc3
kobry	kobra	k1gFnSc2
indické	indický	k2eAgNnSc1d1
(	(	kIx(
<g/>
Naja	Naj	k2eAgFnSc1d1
naja	naja	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkou	velký	k2eAgFnSc4d1
imunitu	imunita	k1gFnSc4
vůči	vůči	k7c3
hadímu	hadí	k2eAgInSc3d1
jedu	jed	k1gInSc3
vykazuje	vykazovat	k5eAaImIp3nS
i	i	k9
největší	veliký	k2eAgMnSc1d3
jedovatý	jedovatý	k2eAgMnSc1d1
had	had	k1gMnSc1
na	na	k7c6
světě	svět	k1gInSc6
-	-	kIx~
kobra	kobra	k1gFnSc1
královská	královský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Ophiophagus	Ophiophagus	k1gMnSc1
hannah	hannah	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
specializuje	specializovat	k5eAaBmIp3nS
výhradně	výhradně	k6eAd1
na	na	k7c4
lov	lov	k1gInSc4
hadů	had	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
jedovatých	jedovatý	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Nejrychlejší	rychlý	k2eAgMnSc1d3
had	had	k1gMnSc1
</s>
<s>
Mamba	Mamba	k1gFnSc1
černá	černý	k2eAgFnSc1d1
</s>
<s>
Mamba	Mamba	k1gFnSc1
černá	černý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Dendroaspis	Dendroaspis	k1gFnSc1
polylepsis	polylepsis	k1gFnSc1
<g/>
)	)	kIx)
z	z	k7c2
Afriky	Afrika	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
údajně	údajně	k6eAd1
nejrychleji	rychle	k6eAd3
se	se	k3xPyFc4
pohybujícím	pohybující	k2eAgMnSc7d1
hadem	had	k1gMnSc7
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umí	umět	k5eAaImIp3nS
vyvinout	vyvinout	k5eAaPmF
na	na	k7c4
krátkou	krátký	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
a	a	k8xC
vzdálenost	vzdálenost	k1gFnSc4
rychlost	rychlost	k1gFnSc4
20	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Nejrychleji	rychle	k6eAd3
plovoucími	plovoucí	k2eAgMnPc7d1
hady	had	k1gMnPc7
jsou	být	k5eAaImIp3nP
mořští	mořský	k2eAgMnPc1d1
vodnáři	vodnář	k1gMnPc1
z	z	k7c2
indopacifické	indopacifický	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
<g/>
,	,	kIx,
vodnář	vodnář	k1gMnSc1
dvoubarevný	dvoubarevný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Pelamis	Pelamis	k1gInSc1
platurus	platurus	k1gInSc1
<g/>
)	)	kIx)
dokáže	dokázat	k5eAaPmIp3nS
krátkodobě	krátkodobě	k6eAd1
při	při	k7c6
pronásledování	pronásledování	k1gNnSc6
kořisti	kořist	k1gFnSc2
plavat	plavat	k5eAaImF
rychlostí	rychlost	k1gFnSc7
až	až	k9
16	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
</s>
<s>
Nejvýše	nejvýše	k6eAd1,k6eAd3
žijící	žijící	k2eAgMnSc1d1
had	had	k1gMnSc1
</s>
<s>
Hadi	had	k1gMnPc1
jsou	být	k5eAaImIp3nP
závislí	závislý	k2eAgMnPc1d1
na	na	k7c6
teplotě	teplota	k1gFnSc6
okolního	okolní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
a	a	k8xC
vysoké	vysoký	k2eAgFnSc2d1
nadmořské	nadmořský	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
jim	on	k3xPp3gMnPc3
tedy	tedy	k9
moc	moc	k6eAd1
nevyhovují	vyhovovat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	se	k3xPyFc4
někteří	některý	k3yIgMnPc1
hadi	had	k1gMnPc1
dokázali	dokázat	k5eAaPmAgMnP
přizpůsobit	přizpůsobit	k5eAaPmF
i	i	k9
těmto	tento	k3xDgFnPc3
podmínkám	podmínka	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlépe	dobře	k6eAd3
se	se	k3xPyFc4
na	na	k7c4
horské	horský	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
dokázali	dokázat	k5eAaPmAgMnP
adaptovat	adaptovat	k5eAaBmF
zmije	zmije	k1gFnSc2
a	a	k8xC
chřestýši	chřestýš	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
zmije	zmije	k1gFnSc1
obecná	obecná	k1gFnSc1
(	(	kIx(
<g/>
Vipera	Vipera	k1gFnSc1
berus	berus	k1gMnSc1
<g/>
)	)	kIx)
žije	žít	k5eAaImIp3nS
ve	v	k7c6
Skandinávii	Skandinávie	k1gFnSc6
a	a	k8xC
v	v	k7c6
Alpách	Alpy	k1gFnPc6
ve	v	k7c4
až	až	k6eAd1
výšce	výška	k1gFnSc3
3000	#num#	k4
m	m	kA
a	a	k8xC
středoamerický	středoamerický	k2eAgMnSc1d1
chřestýš	chřestýš	k1gMnSc1
(	(	kIx(
<g/>
Crotalus	Crotalus	k1gMnSc1
triseriatus	triseriatus	k1gMnSc1
<g/>
)	)	kIx)
vystupuje	vystupovat	k5eAaImIp3nS
až	až	k9
na	na	k7c6
hranici	hranice	k1gFnSc6
4400	#num#	k4
m.	m.	k?
Absolutním	absolutní	k2eAgMnSc7d1
rekordmanem	rekordman	k1gMnSc7
je	být	k5eAaImIp3nS
však	však	k9
jiný	jiný	k2eAgMnSc1d1
chřestýšovitý	chřestýšovitý	k2eAgMnSc1d1
had	had	k1gMnSc1
<g/>
,	,	kIx,
ploskolebec	ploskolebec	k1gMnSc1
himálajský	himálajský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Agkistrodon	Agkistrodon	k1gMnSc1
himalayanus	himalayanus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
žijící	žijící	k2eAgMnPc1d1
na	na	k7c6
úpatí	úpatí	k1gNnSc6
himálajských	himálajský	k2eAgInPc2d1
ledovců	ledovec	k1gInPc2
ve	v	k7c6
výšce	výška	k1gFnSc6
až	až	k9
4900	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
mořem	moře	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zmiji	zmije	k1gFnSc3
obecné	obecná	k1gFnSc2
(	(	kIx(
<g/>
Vipera	Vipera	k1gFnSc1
berus	berus	k1gMnSc1
<g/>
)	)	kIx)
navíc	navíc	k6eAd1
patří	patřit	k5eAaImIp3nS
ještě	ještě	k6eAd1
jeden	jeden	k4xCgInSc4
primát	primát	k1gInSc4
<g/>
:	:	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
hada	had	k1gMnSc4
s	s	k7c7
nejsevernější	severní	k2eAgFnSc7d3
oblastí	oblast	k1gFnSc7
výskytu	výskyt	k1gInSc2
-	-	kIx~
některé	některý	k3yIgFnSc2
její	její	k3xOp3gFnSc2
populace	populace	k1gFnSc2
ve	v	k7c6
Skandinávii	Skandinávie	k1gFnSc6
zasahují	zasahovat	k5eAaImIp3nP
až	až	k9
za	za	k7c4
polární	polární	k2eAgInSc4d1
kruh	kruh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Rekordmani	rekordman	k1gMnPc1
v	v	k7c6
plavání	plavání	k1gNnSc6
</s>
<s>
Všichni	všechen	k3xTgMnPc1
hadi	had	k1gMnPc1
bez	bez	k7c2
výjimky	výjimka	k1gFnSc2
umějí	umět	k5eAaImIp3nP
plavat	plavat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
mořských	mořský	k2eAgMnPc2d1
hadů	had	k1gMnPc2
je	být	k5eAaImIp3nS
plavání	plavání	k1gNnSc1
samozřejmostí	samozřejmost	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
hadi	had	k1gMnPc1
suchozemští	suchozemský	k2eAgMnPc1d1
nebo	nebo	k8xC
stromoví	stromový	k2eAgMnPc1d1
jsou	být	k5eAaImIp3nP
výbornými	výborný	k2eAgMnPc7d1
plavci	plavec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hroznýš	hroznýš	k1gMnSc1
královský	královský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Boa	boa	k1gFnSc1
constrictor	constrictor	k1gMnSc1
<g/>
)	)	kIx)
doplaval	doplavat	k5eAaPmAgInS
z	z	k7c2
jihoamerického	jihoamerický	k2eAgInSc2d1
kontinentu	kontinent	k1gInSc2
až	až	k9
na	na	k7c4
ostrov	ostrov	k1gInSc4
Sv.	sv.	kA
Vincent	Vincent	k1gMnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
vzdálenost	vzdálenost	k1gFnSc4
přes	přes	k7c4
320	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
tomto	tento	k3xDgInSc6
výkonu	výkon	k1gInSc6
byl	být	k5eAaImAgMnS
had	had	k1gMnSc1
ve	v	k7c6
velmi	velmi	k6eAd1
dobré	dobrý	k2eAgFnSc6d1
kondici	kondice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Hladovění	hladovění	k1gNnSc1
</s>
<s>
Hadi	had	k1gMnPc1
po	po	k7c6
nasycení	nasycení	k1gNnSc6
tráví	trávit	k5eAaImIp3nS
pozřený	pozřený	k2eAgInSc1d1
úlovek	úlovek	k1gInSc1
několik	několik	k4yIc1
dnů	den	k1gInPc2
až	až	k8xS
týdnů	týden	k1gInPc2
(	(	kIx(
<g/>
podle	podle	k7c2
hmotnosti	hmotnost	k1gFnSc2
ulovené	ulovený	k2eAgFnSc2d1
kořisti	kořist	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
však	však	k9
nezískávají	získávat	k5eNaImIp3nP
potravu	potrava	k1gFnSc4
pravidelně	pravidelně	k6eAd1
a	a	k8xC
mnohdy	mnohdy	k6eAd1
jsou	být	k5eAaImIp3nP
nuceni	nutit	k5eAaImNgMnP
hladovět	hladovět	k5eAaImF
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velcí	velký	k2eAgMnPc1d1
hadi	had	k1gMnPc1
vydrží	vydržet	k5eAaPmIp3nP
bez	bez	k7c2
potravy	potrava	k1gFnSc2
mnohem	mnohem	k6eAd1
déle	dlouho	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
případem	případ	k1gInSc7
jsou	být	k5eAaImIp3nP
hadi	had	k1gMnPc1
s	s	k7c7
extrémně	extrémně	k6eAd1
pomalým	pomalý	k2eAgInSc7d1
metabolismem	metabolismus	k1gInSc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
pohybují	pohybovat	k5eAaImIp3nP
velmi	velmi	k6eAd1
málo	málo	k6eAd1
a	a	k8xC
pomalu	pomalu	k6eAd1
a	a	k8xC
mají	mít	k5eAaImIp3nP
tak	tak	k6eAd1
minimální	minimální	k2eAgFnSc4d1
spotřebu	spotřeba	k1gFnSc4
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
ně	on	k3xPp3gInPc4
např.	např.	kA
africké	africký	k2eAgFnSc2d1
zmije	zmije	k1gFnSc2
(	(	kIx(
<g/>
gabunská	gabunský	k2eAgFnSc1d1
<g/>
,	,	kIx,
útočná	útočný	k2eAgFnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
hadi	had	k1gMnPc1
i	i	k9
ve	v	k7c6
volné	volný	k2eAgFnSc6d1
přírodě	příroda	k1gFnSc6
běžně	běžně	k6eAd1
hladoví	hladovět	k5eAaImIp3nS
stovky	stovka	k1gFnPc4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zajetí	zajetí	k1gNnSc6
bylo	být	k5eAaImAgNnS
zdokumentováno	zdokumentovat	k5eAaPmNgNnS
mnoho	mnoho	k4c1
případů	případ	k1gInPc2
dlouhých	dlouhý	k2eAgInPc2d1
půstů	půst	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
krajta	krajta	k1gFnSc1
tygrovitá	tygrovitý	k2eAgFnSc1d1
světlá	světlý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Python	Python	k1gMnSc1
molurus	molurus	k1gMnSc1
molurus	molurus	k1gMnSc1
<g/>
)	)	kIx)
nepřijímala	přijímat	k5eNaImAgFnS
potravu	potrava	k1gFnSc4
149	#num#	k4
dní	den	k1gInPc2
a	a	k8xC
přitom	přitom	k6eAd1
ztratila	ztratit	k5eAaPmAgFnS
jen	jen	k9
10	#num#	k4
%	%	kIx~
své	svůj	k3xOyFgFnSc2
hmotnosti	hmotnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pražské	pražský	k2eAgFnSc6d1
zoo	zoo	k1gFnSc6
nepřijímala	přijímat	k5eNaImAgFnS
potravu	potrava	k1gFnSc4
krajta	krajta	k1gFnSc1
mřížkovaná	mřížkovaný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Python	Python	k1gMnSc1
reticulatus	reticulatus	k1gMnSc1
<g/>
)	)	kIx)
270	#num#	k4
dní	den	k1gInPc2
a	a	k8xC
přitom	přitom	k6eAd1
ztratila	ztratit	k5eAaPmAgFnS
pouze	pouze	k6eAd1
12	#num#	k4
%	%	kIx~
své	svůj	k3xOyFgFnSc2
hmotnosti	hmotnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
frankfurtské	frankfurtský	k2eAgFnSc6d1
zoo	zoo	k1gFnSc6
nežrala	žrát	k5eNaImAgFnS
krajta	krajta	k1gFnSc1
mřížkovaná	mřížkovaný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Python	Python	k1gMnSc1
reticulatus	reticulatus	k1gMnSc1
<g/>
)	)	kIx)
plných	plný	k2eAgNnPc2d1
570	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
krátký	krátký	k2eAgInSc4d1
čas	čas	k1gInSc4
potravu	potrava	k1gFnSc4
přijímala	přijímat	k5eAaImAgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
poté	poté	k6eAd1
postila	postila	k1gFnSc1
dalších	další	k2eAgInPc2d1
415	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zmíněná	zmíněný	k2eAgFnSc1d1
zmije	zmije	k1gFnSc1
gabunská	gabunský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Bitis	Bitis	k1gFnSc1
gabonica	gabonica	k1gFnSc1
<g/>
)	)	kIx)
nežrala	žrát	k5eNaImAgFnS
679	#num#	k4
dní	den	k1gInPc2
a	a	k8xC
chřestýš	chřestýš	k1gMnSc1
brazilský	brazilský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Crotalus	Crotalus	k1gMnSc1
durissus	durissus	k1gMnSc1
<g/>
)	)	kIx)
780	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krajta	krajta	k1gFnSc1
písmenková	písmenkový	k2eAgFnSc1d1
(	(	kIx(
<g/>
Python	Python	k1gMnSc1
sebae	seba	k1gMnSc2
<g/>
)	)	kIx)
vydržela	vydržet	k5eAaPmAgFnS
bez	bez	k7c2
potravy	potrava	k1gFnSc2
870	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Světový	světový	k2eAgInSc1d1
rekord	rekord	k1gInSc1
však	však	k9
drží	držet	k5eAaImIp3nS
krajta	krajta	k1gFnSc1
mřížkovaná	mřížkovaný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Python	Python	k1gMnSc1
reticulatus	reticulatus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
měřící	měřící	k2eAgFnSc1d1
6,45	6,45	k4
m	m	kA
o	o	k7c6
hmotnosti	hmotnost	k1gFnSc6
na	na	k7c6
začátku	začátek	k1gInSc6
půstu	půst	k1gInSc2
75	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
had	had	k1gMnSc1
nepřijímal	přijímat	k5eNaImAgMnS
potravu	potrava	k1gFnSc4
celých	celý	k2eAgInPc2d1
910	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
dokud	dokud	k6eAd1
nepošel	pojít	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
své	svůj	k3xOyFgFnSc6
smrti	smrt	k1gFnSc6
vážil	vážit	k5eAaImAgMnS
pouhých	pouhý	k2eAgFnPc2d1
27	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s>
Jednopohlavní	jednopohlavní	k2eAgMnSc1d1
had	had	k1gMnSc1
</s>
<s>
Ne	ne	k9
všichni	všechen	k3xTgMnPc1
hadi	had	k1gMnPc1
se	se	k3xPyFc4
pro	pro	k7c4
úspěšné	úspěšný	k2eAgNnSc4d1
rozmnožení	rozmnožení	k1gNnSc4
musí	muset	k5eAaImIp3nS
nutně	nutně	k6eAd1
pářit	pářit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Indii	Indie	k1gFnSc6
<g/>
,	,	kIx,
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
a	a	k8xC
Austrálii	Austrálie	k1gFnSc6
žije	žít	k5eAaImIp3nS
druh	druh	k1gInSc1
primitivního	primitivní	k2eAgMnSc2d1
hada	had	k1gMnSc2
-	-	kIx~
slepák	slepák	k1gInSc1
brahmánský	brahmánský	k2eAgInSc1d1
(	(	kIx(
<g/>
Ramphotyphlops	Ramphotyphlops	k1gInSc1
braminus	braminus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgMnSc7d1
známým	známý	k1gMnSc7
<g/>
,	,	kIx,
partenogeneticky	partenogeneticky	k6eAd1
se	se	k3xPyFc4
rozmnožujícím	rozmnožující	k2eAgMnSc7d1
hadem	had	k1gMnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
tohoto	tento	k3xDgInSc2
druhu	druh	k1gInSc2
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
samice	samice	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
v	v	k7c6
době	doba	k1gFnSc6
dospívání	dospívání	k1gNnSc2
začnou	začít	k5eAaPmIp3nP
automaticky	automaticky	k6eAd1
klást	klást	k5eAaImF
oplozená	oplozený	k2eAgNnPc4d1
vejce	vejce	k1gNnPc4
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
se	se	k3xPyFc4
předtím	předtím	k6eAd1
pářily	pářit	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
těchto	tento	k3xDgNnPc2
vajec	vejce	k1gNnPc2
se	se	k3xPyFc4
však	však	k9
vylíhnou	vylíhnout	k5eAaPmIp3nP
zase	zase	k9
jenom	jenom	k9
samice	samice	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
dokonalými	dokonalý	k2eAgFnPc7d1
genetickými	genetický	k2eAgFnPc7d1
kopiemi	kopie	k1gFnPc7
svých	svůj	k3xOyFgFnPc2
matek	matka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
zajímavá	zajímavý	k2eAgFnSc1d1
hříčka	hříčka	k1gFnSc1
přírody	příroda	k1gFnSc2
však	však	k9
nemá	mít	k5eNaImIp3nS
z	z	k7c2
dlouhodobého	dlouhodobý	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
příliš	příliš	k6eAd1
naděje	naděje	k1gFnPc1
na	na	k7c4
úspěch	úspěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genetická	genetický	k2eAgFnSc1d1
variabilita	variabilita	k1gFnSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
důležitá	důležitý	k2eAgFnSc1d1
pro	pro	k7c4
přizpůsobování	přizpůsobování	k1gNnSc4
měnících	měnící	k2eAgFnPc2d1
se	se	k3xPyFc4
životních	životní	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
<g/>
,	,	kIx,
zde	zde	k6eAd1
totiž	totiž	k9
úplně	úplně	k6eAd1
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BARKER	BARKER	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
;	;	kIx,
BARTEN	BARTEN	kA
<g/>
,	,	kIx,
Stephen	Stephen	k1gInSc1
<g/>
;	;	kIx,
EHRSAM	EHRSAM	kA
<g/>
,	,	kIx,
Jonas	Jonas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Corrected	Corrected	k1gMnSc1
Lengths	Lengthsa	k1gFnPc2
of	of	k?
Two	Two	k1gMnSc1
Well-known	Well-known	k1gMnSc1
Giant	Giant	k1gMnSc1
Pythons	Pythonsa	k1gFnPc2
and	and	k?
the	the	k?
Establishment	establishment	k1gInSc1
of	of	k?
a	a	k8xC
New	New	k1gMnSc1
Maximum	maximum	k1gNnSc1
Length	Length	k1gMnSc1
Record	Record	k1gMnSc1
for	forum	k1gNnPc2
Burmese	Burmesa	k1gFnSc3
Pythons	Pythons	k1gInSc1
<g/>
,	,	kIx,
Python	Python	k1gMnSc1
bivittatus	bivittatus	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CHS	CHS	kA
Bulletin	bulletin	k1gInSc1
-	-	kIx~
Chicago	Chicago	k1gNnSc1
Herpetological	Herpetological	k1gFnSc2
Society	societa	k1gFnSc2
<g/>
,	,	kIx,
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Longest	Longest	k1gInSc1
snake	snake	k1gFnSc1
-	-	kIx~
ever	ever	k1gInSc1
(	(	kIx(
<g/>
captivity	captivita	k1gFnPc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guinness	Guinnessa	k1gFnPc2
World	Worlda	k1gFnPc2
Records	Records	k1gInSc1
<g/>
,	,	kIx,
2011-10-12	2011-10-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LOVIC	LOVIC	kA
<g/>
,	,	kIx,
Vern	Vern	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
KING	KING	kA
COBRA	COBRA	kA
–	–	k?
LARGEST	LARGEST	kA
VENOMOUS	VENOMOUS	kA
SNAKE	SNAKE	kA
IN	IN	kA
WORLD	WORLD	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thailandsnakes	Thailandsnakes	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2015-05-22	2015-05-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dendroaspis	Dendroaspis	k1gInSc1
polylepis	polylepis	k1gFnSc1
(	(	kIx(
<g/>
Black	Black	k1gMnSc1
Mamba	Mamba	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Animal	animal	k1gMnSc1
Diversity	Diversit	k1gInPc4
Web	web	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Biggest	Biggest	k1gInSc1
Snake	Snake	k1gFnSc1
<g/>
:	:	kIx,
Giant	Giant	k1gInSc1
Anaconda	Anaconda	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Extremescience	Extremescience	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Eastern	Eastern	k1gInSc1
diamond-backed	diamond-backed	k1gMnSc1
rattlesnake	rattlesnake	k1gInSc1
(	(	kIx(
<g/>
Crotalus	Crotalus	k1gMnSc1
adamanteus	adamanteus	k1gMnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arkive	Arkiev	k1gFnSc2
<g/>
,	,	kIx,
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KINDERSLEY	KINDERSLEY	kA
<g/>
,	,	kIx,
Dorling	Dorling	k1gInSc1
<g/>
.	.	kIx.
1001	#num#	k4
otázka	otázka	k1gFnSc1
a	a	k8xC
odpověď	odpověď	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
TIMY	TIMY	kA
spol	spol	k1gInSc1
<g/>
.	.	kIx.
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
88799	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
6	#num#	k4
a	a	k8xC
56	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Paleontologists	Paleontologists	k1gInSc1
Find	Find	k?
World	World	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Oldest	Oldest	k1gMnSc1
Known	Known	k1gMnSc1
Snake	Snake	k1gFnPc2
Fossils	Fossils	k1gInSc1
<g/>
,	,	kIx,
Sci-news	Sci-news	k1gInSc1
<g/>
1	#num#	k4
2	#num#	k4
http://wellsking.tripod.com/Worldrec18.htm	http://wellsking.tripod.com/Worldrec18.htm	k1gMnSc1
<g/>
↑	↑	k?
CARWARDINE	CARWARDINE	kA
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Animal	animal	k1gMnSc1
Records	Records	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Sterling	sterling	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4027	#num#	k4
<g/>
-	-	kIx~
<g/>
5623	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
165	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gFnPc4
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
-	-	kIx~
Corallus	Corallus	k1gInSc1
cropanii	cropanie	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IUCN	IUCN	kA
<g/>
,	,	kIx,
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
10	#num#	k4
Most	most	k1gInSc1
Poisonous	Poisonous	k1gInSc4
&	&	k?
Dangerous	Dangerous	k1gInSc1
Snakes	Snakes	k1gMnSc1
In	In	k1gMnSc1
the	the	k?
World	World	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Conservation	Conservation	k1gInSc1
Institute	institut	k1gInSc5
<g/>
,	,	kIx,
2015-02-06	2015-02-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Zvířata	zvíře	k1gNnPc1
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
-	-	kIx~
Hadi	had	k1gMnPc1
<g/>
,	,	kIx,
<g/>
(	(	kIx(
<g/>
Jiří	Jiří	k1gMnSc1
Felix	Felix	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
,	,	kIx,
SZN	SZN	kA
Praha	Praha	k1gFnSc1
</s>
<s>
Kniha	kniha	k1gFnSc1
o	o	k7c6
hadech	had	k1gMnPc6
<g/>
,	,	kIx,
(	(	kIx(
<g/>
John	John	k1gMnSc1
A.	A.	kA
Burton	Burton	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
Svojtka	Svojtka	k1gFnSc1
<g/>
&	&	k?
<g/>
Co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
text	text	k1gInSc1
(	(	kIx(
<g/>
GFDL	GFDL	kA
licence	licence	k1gFnSc2
<g/>
)	)	kIx)
ze	z	k7c2
stránek	stránka	k1gFnPc2
Richarda	Richard	k1gMnSc2
Horčice	Horčice	k1gFnSc2
o	o	k7c6
hadech	had	k1gMnPc6
<g/>
.	.	kIx.
</s>
