<s>
Nejdelším	dlouhý	k2eAgMnSc7d3
jedovatým	jedovatý	k2eAgMnSc7d1
hadem	had	k1gMnSc7
je	být	k5eAaImIp3nS
kobra	kobra	k1gFnSc1
královská	královský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Ophiophagus	Ophiophagus	k1gMnSc1
hannah	hannah	k1gMnSc1
<g/>
)	)	kIx)
z	z	k7c2
Jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
exemplář	exemplář	k1gInSc1
chovaný	chovaný	k2eAgInSc1d1
v	v	k7c6
Londýnské	londýnský	k2eAgFnSc6d1
zoo	zoo	k1gFnSc6
dosáhl	dosáhnout	k5eAaPmAgInS
velikosti	velikost	k1gFnSc2
5,71	5,71	k4
m	m	kA
při	při	k7c6
hmotnosti	hmotnost	k1gFnSc6
téměř	téměř	k6eAd1
12	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>