<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
přibližně	přibližně	k6eAd1	přibližně
94	[number]	k4	94
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
poblíž	poblíž	k6eAd1	poblíž
břehů	břeh	k1gInPc2	břeh
řeky	řeka	k1gFnSc2	řeka
Nil	Nil	k1gInSc1	Nil
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
40	[number]	k4	40
000	[number]	k4	000
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jediná	jediný	k2eAgFnSc1d1	jediná
orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
