<s>
Řada	řada	k1gFnSc1	řada
technologií	technologie	k1gFnPc2	technologie
–	–	k?	–
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
optické	optický	k2eAgFnPc4d1	optická
a	a	k8xC	a
mechanické	mechanický	k2eAgFnPc4d1	mechanická
hračky	hračka	k1gFnPc4	hračka
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
vize	vize	k1gFnSc1	vize
–	–	k?	–
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
předcházela	předcházet	k5eAaImAgFnS	předcházet
tak	tak	k6eAd1	tak
zrození	zrození	k1gNnSc4	zrození
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
:	:	kIx,	:
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
byla	být	k5eAaImAgFnS	být
vynalezen	vynalezen	k2eAgInSc4d1	vynalezen
Athanasiusem	Athanasius	k1gMnSc7	Athanasius
Kircherem	Kircher	k1gMnSc7	Kircher
první	první	k4xOgFnSc7	první
primitivní	primitivní	k2eAgFnSc1d1	primitivní
"	"	kIx"	"
<g/>
magická	magický	k2eAgFnSc1d1	magická
lucerna	lucerna	k1gFnSc1	lucerna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
