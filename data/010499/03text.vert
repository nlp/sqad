<p>
<s>
Vítr	vítr	k1gInSc1	vítr
je	být	k5eAaImIp3nS	být
vektor	vektor	k1gInSc4	vektor
popisující	popisující	k2eAgInSc1d1	popisující
pohyb	pohyb	k1gInSc1	pohyb
zvolené	zvolený	k2eAgFnSc2d1	zvolená
částice	částice	k1gFnSc2	částice
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
místě	místo	k1gNnSc6	místo
atmosféry	atmosféra	k1gFnSc2	atmosféra
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
časovém	časový	k2eAgInSc6d1	časový
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
rozumí	rozumět	k5eAaImIp3nS	rozumět
horizontální	horizontální	k2eAgFnSc1d1	horizontální
složka	složka	k1gFnSc1	složka
proudění	proudění	k1gNnSc2	proudění
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vyvolaný	vyvolaný	k2eAgInSc4d1	vyvolaný
rozdíly	rozdíl	k1gInPc4	rozdíl
v	v	k7c6	v
tlaku	tlak	k1gInSc6	tlak
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
rotací	rotace	k1gFnPc2	rotace
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
i	i	k9	i
síla	síla	k1gFnSc1	síla
tření	tření	k1gNnSc2	tření
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
popisu	popis	k1gInSc6	popis
nás	my	k3xPp1nPc4	my
zajímá	zajímat	k5eAaImIp3nS	zajímat
jeho	jeho	k3xOp3gNnSc1	jeho
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
ochlazovací	ochlazovací	k2eAgInSc4d1	ochlazovací
účinek	účinek	k1gInSc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
a	a	k8xC	a
směr	směr	k1gInSc1	směr
větru	vítr	k1gInSc2	vítr
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
pomocí	pomocí	k7c2	pomocí
anemometru	anemometr	k1gInSc2	anemometr
či	či	k8xC	či
profileru	profiler	k1gInSc2	profiler
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Směr	směr	k1gInSc1	směr
větru	vítr	k1gInSc2	vítr
==	==	k?	==
</s>
</p>
<p>
<s>
Směr	směr	k1gInSc1	směr
větru	vítr	k1gInSc2	vítr
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
dle	dle	k7c2	dle
směru	směr	k1gInSc2	směr
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
vítr	vítr	k1gInSc1	vítr
vane	vanout	k5eAaImIp3nS	vanout
–	–	k?	–
buď	buď	k8xC	buď
přesněji	přesně	k6eAd2	přesně
pomocí	pomocí	k7c2	pomocí
azimutu	azimut	k1gInSc2	azimut
(	(	kIx(	(
<g/>
0	[number]	k4	0
až	až	k9	až
360	[number]	k4	360
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
meteorologii	meteorologie	k1gFnSc6	meteorologie
pomocí	pomocí	k7c2	pomocí
světových	světový	k2eAgFnPc2d1	světová
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
s	s	k7c7	s
přesností	přesnost	k1gFnSc7	přesnost
na	na	k7c4	na
22,5	[number]	k4	22,5
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
s	s	k7c7	s
rozlišením	rozlišení	k1gNnSc7	rozlišení
na	na	k7c4	na
S	s	k7c7	s
<g/>
,	,	kIx,	,
SSV	SSV	kA	SSV
<g/>
,	,	kIx,	,
SV	sv	kA	sv
<g/>
,	,	kIx,	,
VSV	VSV	kA	VSV
a	a	k8xC	a
V	v	k7c4	v
směr	směr	k1gInSc4	směr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
výškou	výška	k1gFnSc7	výška
(	(	kIx(	(
<g/>
ve	v	k7c6	v
vertikálním	vertikální	k2eAgInSc6d1	vertikální
směru	směr	k1gInSc6	směr
<g/>
)	)	kIx)	)
i	i	k9	i
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
(	(	kIx(	(
<g/>
v	v	k7c6	v
horizontálním	horizontální	k2eAgInSc6d1	horizontální
směru	směr	k1gInSc6	směr
<g/>
)	)	kIx)	)
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
pozorování	pozorování	k1gNnSc2	pozorování
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
směru	směr	k1gInSc2	směr
větru	vítr	k1gInSc2	vítr
–	–	k?	–
stáčení	stáčení	k1gNnSc1	stáčení
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
stáčení	stáčení	k1gNnSc3	stáčení
větru	vítr	k1gInSc2	vítr
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
v	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
místě	místo	k1gNnSc6	místo
pozorování	pozorování	k1gNnPc2	pozorování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
==	==	k?	==
</s>
</p>
<p>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
(	(	kIx(	(
<g/>
síla	síla	k1gFnSc1	síla
<g/>
)	)	kIx)	)
větru	vítr	k1gInSc2	vítr
se	se	k3xPyFc4	se
klasifikuje	klasifikovat	k5eAaImIp3nS	klasifikovat
buďto	buďto	k8xC	buďto
přesným	přesný	k2eAgNnSc7d1	přesné
určením	určení	k1gNnSc7	určení
jeho	jeho	k3xOp3gFnSc2	jeho
rychlosti	rychlost	k1gFnSc2	rychlost
(	(	kIx(	(
<g/>
kilometry	kilometr	k1gInPc1	kilometr
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
,	,	kIx,	,
metry	metr	k1gInPc4	metr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
,	,	kIx,	,
míle	míle	k1gFnSc1	míle
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
stupních	stupeň	k1gInPc6	stupeň
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
určují	určovat	k5eAaImIp3nP	určovat
odhadem	odhad	k1gInSc7	odhad
podle	podle	k7c2	podle
Beaufortovy	Beaufortův	k2eAgFnSc2d1	Beaufortova
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
se	se	k3xPyFc4	se
v	v	k7c6	v
čase	čas	k1gInSc6	čas
výrazně	výrazně	k6eAd1	výrazně
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
často	často	k6eAd1	často
udává	udávat	k5eAaImIp3nS	udávat
průměrná	průměrný	k2eAgFnSc1d1	průměrná
rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
(	(	kIx(	(
<g/>
za	za	k7c4	za
určité	určitý	k2eAgNnSc4d1	určité
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
např.	např.	kA	např.
1	[number]	k4	1
nebo	nebo	k8xC	nebo
5	[number]	k4	5
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
a	a	k8xC	a
nárazová	nárazový	k2eAgFnSc1d1	nárazová
rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
(	(	kIx(	(
<g/>
maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
při	při	k7c6	při
jednorázovém	jednorázový	k2eAgInSc6d1	jednorázový
nárazu	náraz	k1gInSc6	náraz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
v	v	k7c6	v
obecných	obecný	k2eAgFnPc6d1	obecná
podmínkách	podmínka	k1gFnPc6	podmínka
===	===	k?	===
</s>
</p>
<p>
<s>
Spektrum	spektrum	k1gNnSc1	spektrum
rozložení	rozložení	k1gNnSc1	rozložení
hustoty	hustota	k1gFnSc2	hustota
rychlostí	rychlost	k1gFnPc2	rychlost
větru	vítr	k1gInSc2	vítr
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
lokalitě	lokalita	k1gFnSc6	lokalita
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
popsatelné	popsatelný	k2eAgNnSc1d1	popsatelné
Rayleighovým	Rayleighův	k2eAgNnSc7d1	Rayleighovo
rozdělením	rozdělení	k1gNnSc7	rozdělení
jako	jako	k8xS	jako
speciálním	speciální	k2eAgInSc7d1	speciální
případem	případ	k1gInSc7	případ
rozdělení	rozdělení	k1gNnSc4	rozdělení
Weibullova	Weibullův	k2eAgNnSc2d1	Weibullovo
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
funkci	funkce	k1gFnSc4	funkce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
β	β	k?	β
</s>
</p>
<p>
<s>
η	η	k?	η
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
η	η	k?	η
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
β	β	k?	β
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
exp	exp	kA	exp
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
η	η	k?	η
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
β	β	k?	β
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
v	v	k7c6	v
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
-1	-1	k4	-1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
exp	exp	kA	exp
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
[	[	kIx(	[
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
v	v	k7c4	v
je	on	k3xPp3gFnPc4	on
náhodně	náhodně	k6eAd1	náhodně
proměnná	proměnný	k2eAgFnSc1d1	proměnná
rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
β	β	k?	β
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
tvarový	tvarový	k2eAgInSc1d1	tvarový
parametr	parametr	k1gInSc1	parametr
rozložení	rozložení	k1gNnSc2	rozložení
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
η	η	k?	η
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
eta	eta	k?	eta
}	}	kIx)	}
</s>
</p>
<p>
<s>
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
střední	střední	k2eAgFnSc3d1	střední
hodnotě	hodnota	k1gFnSc3	hodnota
rychlosti	rychlost	k1gFnSc2	rychlost
větru	vítr	k1gInSc2	vítr
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
η	η	k?	η
</s>
</p>
<p>
<s>
≈	≈	k?	≈
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
~	~	kIx~	~
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0.886	[number]	k4	0.886
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
eta	eta	k?	eta
\	\	kIx~	\
<g/>
approx	approx	k1gInSc1	approx
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
0.886	[number]	k4	0.886
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
maximum	maximum	k1gNnSc4	maximum
hustoty	hustota	k1gFnSc2	hustota
výskytu	výskyt	k1gInSc2	výskyt
rychlostí	rychlost	k1gFnPc2	rychlost
bude	být	k5eAaImBp3nS	být
vždy	vždy	k6eAd1	vždy
ležet	ležet	k5eAaImF	ležet
vlevo	vlevo	k6eAd1	vlevo
od	od	k7c2	od
hustoty	hustota	k1gFnSc2	hustota
výskytu	výskyt	k1gInSc2	výskyt
střední	střední	k2eAgFnSc2d1	střední
rychlosti	rychlost	k1gFnSc2	rychlost
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
reálné	reálný	k2eAgNnSc4d1	reálné
použití	použití	k1gNnSc4	použití
má	mít	k5eAaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
výskytu	výskyt	k1gInSc6	výskyt
rozsahu	rozsah	k1gInSc6	rozsah
rychlostí	rychlost	k1gFnSc7	rychlost
větru	vítr	k1gInSc2	vítr
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
(	(	kIx(	(
<g/>
v	v	k7c4	v
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
v	v	k7c6	v
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
jako	jako	k8xS	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
∫	∫	k?	∫
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P_	P_	k1gMnPc6	P_
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
v	v	k7c6	v
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
v	v	k7c6	v
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Profil	profil	k1gInSc1	profil
větru	vítr	k1gInSc2	vítr
===	===	k?	===
</s>
</p>
<p>
<s>
Vertikální	vertikální	k2eAgInSc1d1	vertikální
profil	profil	k1gInSc1	profil
větru	vítr	k1gInSc2	vítr
je	být	k5eAaImIp3nS	být
grafické	grafický	k2eAgNnSc1d1	grafické
nebo	nebo	k8xC	nebo
matematické	matematický	k2eAgNnSc1d1	matematické
vyjádření	vyjádření	k1gNnSc1	vyjádření
změny	změna	k1gFnSc2	změna
rychlosti	rychlost	k1gFnSc2	rychlost
nebo	nebo	k8xC	nebo
směru	směr	k1gInSc2	směr
větru	vítr	k1gInSc2	vítr
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
troposféře	troposféra	k1gFnSc6	troposféra
nejčastěji	často	k6eAd3	často
rychlost	rychlost	k1gFnSc4	rychlost
větru	vítr	k1gInSc2	vítr
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
roste	růst	k5eAaImIp3nS	růst
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
odvození	odvození	k1gNnSc3	odvození
různých	různý	k2eAgInPc2d1	různý
modelů	model	k1gInPc2	model
–	–	k?	–
mocninový	mocninový	k2eAgInSc1d1	mocninový
<g/>
,	,	kIx,	,
logaritmický	logaritmický	k2eAgInSc1d1	logaritmický
či	či	k8xC	či
logaritmicko-lineární	logaritmickoineární	k2eAgInSc1d1	logaritmicko-lineární
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skutečný	skutečný	k2eAgInSc1d1	skutečný
vertikální	vertikální	k2eAgInSc1d1	vertikální
profil	profil	k1gInSc1	profil
je	být	k5eAaImIp3nS	být
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
mnoha	mnoho	k4c7	mnoho
faktory	faktor	k1gInPc7	faktor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
matematické	matematický	k2eAgInPc1d1	matematický
modely	model	k1gInPc1	model
deformují	deformovat	k5eAaImIp3nP	deformovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
dynamická	dynamický	k2eAgFnSc1d1	dynamická
deformace	deformace	k1gFnSc1	deformace
–	–	k?	–
tlakový	tlakový	k2eAgInSc1d1	tlakový
gradient	gradient	k1gInSc1	gradient
<g/>
,	,	kIx,	,
odstředivá	odstředivý	k2eAgFnSc1d1	odstředivá
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
uchylující	uchylující	k2eAgFnSc1d1	uchylující
síla	síla	k1gFnSc1	síla
zemské	zemský	k2eAgFnSc2d1	zemská
rotace	rotace	k1gFnSc2	rotace
<g/>
,	,	kIx,	,
drsnost	drsnost	k1gFnSc1	drsnost
povrchu	povrch	k1gInSc2	povrch
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
termodynamická	termodynamický	k2eAgFnSc1d1	termodynamická
deformace	deformace	k1gFnSc1	deformace
–	–	k?	–
teplotní	teplotní	k2eAgNnSc4d1	teplotní
zvrstvení	zvrstvení	k1gNnSc4	zvrstvení
<g/>
.	.	kIx.	.
<g/>
Změny	změna	k1gFnPc1	změna
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
a	a	k8xC	a
rychlosti	rychlost	k1gFnSc3	rychlost
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
ty	ten	k3xDgFnPc4	ten
výrazné	výrazný	k2eAgFnPc4d1	výrazná
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k8xS	jako
střih	střih	k1gInSc1	střih
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Působení	působení	k1gNnSc2	působení
větru	vítr	k1gInSc2	vítr
==	==	k?	==
</s>
</p>
<p>
<s>
Vítr	vítr	k1gInSc1	vítr
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
pradávna	pradávno	k1gNnSc2	pradávno
ničícím	ničící	k2eAgInSc7d1	ničící
živlem	živel	k1gInSc7	živel
i	i	k8xC	i
pomocníkem	pomocník	k1gMnSc7	pomocník
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vichřice	vichřice	k1gFnSc1	vichřice
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
povodněmi	povodeň	k1gFnPc7	povodeň
jsou	být	k5eAaImIp3nP	být
největším	veliký	k2eAgInSc7d3	veliký
zdrojem	zdroj	k1gInSc7	zdroj
škod	škoda	k1gFnPc2	škoda
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
při	při	k7c6	při
poslední	poslední	k2eAgFnSc6d1	poslední
velké	velký	k2eAgFnSc6d1	velká
vichřici	vichřice	k1gFnSc6	vichřice
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2004	[number]	k4	2004
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poničení	poničení	k1gNnSc3	poničení
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
lesů	les	k1gInPc2	les
Vysokých	vysoký	k2eAgFnPc2d1	vysoká
Tater	Tatra	k1gFnPc2	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Vítr	vítr	k1gInSc1	vítr
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
činitelů	činitel	k1gInPc2	činitel
působících	působící	k2eAgInPc2d1	působící
erozi	eroze	k1gFnSc3	eroze
a	a	k8xC	a
zvětrávání	zvětrávání	k1gNnSc3	zvětrávání
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Využití	využití	k1gNnSc1	využití
větrné	větrný	k2eAgFnSc2d1	větrná
energie	energie	k1gFnSc2	energie
===	===	k?	===
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
energie	energie	k1gFnSc1	energie
větru	vítr	k1gInSc2	vítr
využívá	využívat	k5eAaImIp3nS	využívat
pomocí	pomocí	k7c2	pomocí
větrné	větrný	k2eAgFnSc2d1	větrná
turbíny	turbína	k1gFnSc2	turbína
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
turbína	turbína	k1gFnSc1	turbína
konala	konat	k5eAaImAgFnS	konat
přímo	přímo	k6eAd1	přímo
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
práci	práce	k1gFnSc4	práce
-	-	kIx~	-
větrný	větrný	k2eAgInSc1d1	větrný
mlýn	mlýn	k1gInSc1	mlýn
mlel	mlít	k5eAaImAgInS	mlít
obilí	obilí	k1gNnSc4	obilí
<g/>
,	,	kIx,	,
větrnými	větrný	k2eAgInPc7d1	větrný
stroji	stroj	k1gInPc7	stroj
se	se	k3xPyFc4	se
čerpala	čerpat	k5eAaImAgFnS	čerpat
voda	voda	k1gFnSc1	voda
nebo	nebo	k8xC	nebo
poháněly	pohánět	k5eAaImAgInP	pohánět
katry	katr	k1gInPc1	katr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
mechanická	mechanický	k2eAgFnSc1d1	mechanická
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
získaná	získaný	k2eAgFnSc1d1	získaná
turbínou	turbína	k1gFnSc7	turbína
z	z	k7c2	z
energie	energie	k1gFnSc2	energie
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
pohon	pohon	k1gInSc4	pohon
alternátoru	alternátor	k1gInSc2	alternátor
ve	v	k7c6	v
větrných	větrný	k2eAgFnPc6d1	větrná
elektrárnách	elektrárna	k1gFnPc6	elektrárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vítr	vítr	k1gInSc1	vítr
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
pohonu	pohon	k1gInSc3	pohon
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
lodí	loď	k1gFnPc2	loď
(	(	kIx(	(
<g/>
plachetnice	plachetnice	k1gFnSc1	plachetnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vliv	vliv	k1gInSc1	vliv
větru	vítr	k1gInSc2	vítr
na	na	k7c4	na
rostliny	rostlina	k1gFnPc4	rostlina
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
podstatné	podstatný	k2eAgFnPc4d1	podstatná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
proudění	proudění	k1gNnSc2	proudění
vzduchu	vzduch	k1gInSc2	vzduch
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c6	na
poškození	poškození	k1gNnSc6	poškození
rostlin	rostlina	k1gFnPc2	rostlina
patří	patřit	k5eAaImIp3nS	patřit
jeho	jeho	k3xOp3gInSc1	jeho
směr	směr	k1gInSc1	směr
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgInSc1d1	silný
vítr	vítr	k1gInSc1	vítr
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
teplem	teplo	k1gNnSc7	teplo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
slunečním	sluneční	k2eAgNnSc7d1	sluneční
zářením	záření	k1gNnSc7	záření
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
nadměrné	nadměrný	k2eAgFnSc3d1	nadměrná
transpiraci	transpirace	k1gFnSc3	transpirace
<g/>
,	,	kIx,	,
vítr	vítr	k1gInSc1	vítr
nebo	nebo	k8xC	nebo
jím	jíst	k5eAaImIp1nS	jíst
nesený	nesený	k2eAgInSc1d1	nesený
materiál	materiál	k1gInSc1	materiál
může	moct	k5eAaImIp3nS	moct
mechanicky	mechanicky	k6eAd1	mechanicky
poškozovat	poškozovat	k5eAaImF	poškozovat
pletiva	pletivo	k1gNnPc4	pletivo
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlivem	vliv	k1gInSc7	vliv
intenzity	intenzita	k1gFnSc2	intenzita
proudění	proudění	k1gNnSc2	proudění
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přímým	přímý	k2eAgFnPc3d1	přímá
škodám	škoda	k1gFnPc3	škoda
např.	např.	kA	např.
opad	opad	k1gInSc4	opad
plodů	plod	k1gInPc2	plod
<g/>
,	,	kIx,	,
lámání	lámání	k1gNnSc1	lámání
větví	větev	k1gFnPc2	větev
<g/>
,	,	kIx,	,
vyvracení	vyvracení	k1gNnSc1	vyvracení
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
typické	typický	k2eAgFnPc1d1	typická
deformace	deformace	k1gFnPc1	deformace
růstu	růst	k1gInSc2	růst
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
převládajícího	převládající	k2eAgNnSc2d1	převládající
proudění	proudění	k1gNnSc2	proudění
<g/>
.	.	kIx.	.
<g/>
Pozitivním	pozitivní	k2eAgInSc7d1	pozitivní
efektem	efekt	k1gInSc7	efekt
je	být	k5eAaImIp3nS	být
výměna	výměna	k1gFnSc1	výměna
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc4	snížení
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc4	zvýšení
teplot	teplota	k1gFnPc2	teplota
<g/>
,	,	kIx,	,
vítr	vítr	k1gInSc1	vítr
může	moct	k5eAaImIp3nS	moct
zabránit	zabránit	k5eAaPmF	zabránit
chladovému	chladový	k2eAgNnSc3d1	chladové
a	a	k8xC	a
tepelnému	tepelný	k2eAgNnSc3d1	tepelné
poškození	poškození	k1gNnSc3	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Vítr	vítr	k1gInSc1	vítr
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgMnSc1d1	významný
pro	pro	k7c4	pro
rozmnožování	rozmnožování	k1gNnPc4	rozmnožování
anemofilních	anemofilní	k2eAgInPc2d1	anemofilní
a	a	k8xC	a
anemochorních	anemochorní	k2eAgInPc2d1	anemochorní
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vítr	vítr	k1gInSc1	vítr
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
planetách	planeta	k1gFnPc6	planeta
==	==	k?	==
</s>
</p>
<p>
<s>
Vítr	vítr	k1gInSc1	vítr
nevzniká	vznikat	k5eNaImIp3nS	vznikat
jen	jen	k9	jen
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
také	také	k9	také
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
planetách	planeta	k1gFnPc6	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Nejrychlejší	rychlý	k2eAgInSc1d3	nejrychlejší
vítr	vítr	k1gInSc1	vítr
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
planetách	planeta	k1gFnPc6	planeta
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
Saturn	Saturn	k1gMnSc1	Saturn
a	a	k8xC	a
Neptun	Neptun	k1gMnSc1	Neptun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
–	–	k?	–
Na	na	k7c6	na
Jupiteru	Jupiter	k1gInSc6	Jupiter
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
větry	vítr	k1gInPc1	vítr
díky	díky	k7c3	díky
tryskovému	tryskový	k2eAgNnSc3d1	tryskové
proudění	proudění	k1gNnSc3	proudění
rychlostí	rychlost	k1gFnPc2	rychlost
až	až	k9	až
500	[number]	k4	500
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
</s>
</p>
<p>
<s>
Saturn	Saturn	k1gMnSc1	Saturn
–	–	k?	–
Větry	vítr	k1gInPc1	vítr
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Saturn	Saturn	k1gInSc4	Saturn
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
v	v	k7c6	v
troposféře	troposféra	k1gFnSc6	troposféra
rychlostí	rychlost	k1gFnPc2	rychlost
až	až	k9	až
1500	[number]	k4	1500
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
</s>
</p>
<p>
<s>
Neptun	Neptun	k1gInSc1	Neptun
–	–	k?	–
Rekordmanem	rekordman	k1gMnSc7	rekordman
je	být	k5eAaImIp3nS	být
planeta	planeta	k1gFnSc1	planeta
Neptun	Neptun	k1gInSc1	Neptun
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
větry	vítr	k1gInPc1	vítr
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k6eAd1	až
2500	[number]	k4	2500
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BEDNÁŘ	Bednář	k1gMnSc1	Bednář
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
ZIKMUNDA	Zikmund	k1gMnSc4	Zikmund
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
<g/>
.	.	kIx.	.
</s>
<s>
Fyzika	fyzika	k1gFnSc1	fyzika
mezní	mezní	k2eAgFnSc2d1	mezní
vrstvy	vrstva	k1gFnSc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
248	[number]	k4	248
s.	s.	k?	s.
</s>
</p>
<p>
<s>
PECHALA	PECHALA	kA	PECHALA
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
;	;	kIx,	;
BEDNÁŘ	Bednář	k1gMnSc1	Bednář
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Příručka	příručka	k1gFnSc1	příručka
dynamické	dynamický	k2eAgFnSc2d1	dynamická
meteorologie	meteorologie	k1gFnSc2	meteorologie
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
372	[number]	k4	372
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
198	[number]	k4	198
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
sluneční	sluneční	k2eAgInSc1d1	sluneční
vítr	vítr	k1gInSc1	vítr
</s>
</p>
<p>
<s>
řvoucí	řvoucí	k2eAgFnPc1d1	řvoucí
čtyřicítky	čtyřicítka	k1gFnPc1	čtyřicítka
</s>
</p>
<p>
<s>
tropická	tropický	k2eAgFnSc1d1	tropická
cyklóna	cyklóna	k1gFnSc1	cyklóna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vítr	vítr	k1gInSc1	vítr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
vítr	vítr	k1gInSc4	vítr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
vítr	vítr	k1gInSc1	vítr
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Vítr	vítr	k1gInSc1	vítr
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Vertikální	vertikální	k2eAgInSc1d1	vertikální
profil	profil	k1gInSc1	profil
rychlosti	rychlost	k1gFnSc2	rychlost
a	a	k8xC	a
směru	směr	k1gInSc2	směr
větru	vítr	k1gInSc2	vítr
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
ČHMÚ	ČHMÚ	kA	ČHMÚ
</s>
</p>
