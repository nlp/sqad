<s>
Větrná	větrný	k2eAgFnSc1d1	větrná
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
obnovitelná	obnovitelný	k2eAgFnSc1d1	obnovitelná
energie	energie	k1gFnSc1	energie
používaná	používaný	k2eAgFnSc1d1	používaná
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
pomocí	pomocí	k7c2	pomocí
větrných	větrný	k2eAgFnPc2d1	větrná
elektráren	elektrárna	k1gFnPc2	elektrárna
(	(	kIx(	(
<g/>
turbín	turbína	k1gFnPc2	turbína
<g/>
)	)	kIx)	)
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
proudění	proudění	k1gNnSc2	proudění
větru	vítr	k1gInSc2	vítr
jako	jako	k8xC	jako
obnovitelného	obnovitelný	k2eAgInSc2d1	obnovitelný
zdroje	zdroj	k1gInSc2	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
