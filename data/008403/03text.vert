<p>
<s>
Větrná	větrný	k2eAgFnSc1d1	větrná
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
obnovitelná	obnovitelný	k2eAgFnSc1d1	obnovitelná
energie	energie	k1gFnSc1	energie
používaná	používaný	k2eAgFnSc1d1	používaná
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
pomocí	pomocí	k7c2	pomocí
větrných	větrný	k2eAgFnPc2d1	větrná
elektráren	elektrárna	k1gFnPc2	elektrárna
(	(	kIx(	(
<g/>
turbín	turbína	k1gFnPc2	turbína
<g/>
)	)	kIx)	)
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
proudění	proudění	k1gNnSc2	proudění
větru	vítr	k1gInSc2	vítr
jako	jako	k8xC	jako
obnovitelného	obnovitelný	k2eAgInSc2d1	obnovitelný
zdroje	zdroj	k1gInSc2	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejobvyklejším	obvyklý	k2eAgNnSc7d3	nejobvyklejší
využitím	využití	k1gNnSc7	využití
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
větrné	větrný	k2eAgFnPc1d1	větrná
elektrárny	elektrárna	k1gFnPc1	elektrárna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
využívají	využívat	k5eAaImIp3nP	využívat
síly	síla	k1gFnPc4	síla
větru	vítr	k1gInSc2	vítr
k	k	k7c3	k
roztočení	roztočení	k1gNnSc3	roztočení
vrtule	vrtule	k1gFnSc2	vrtule
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
připojen	připojit	k5eAaPmNgInS	připojit
elektrický	elektrický	k2eAgInSc1d1	elektrický
generátor	generátor	k1gInSc1	generátor
<g/>
.	.	kIx.	.
</s>
<s>
Teoreticky	teoreticky	k6eAd1	teoreticky
získatelný	získatelný	k2eAgInSc1d1	získatelný
výkon	výkon	k1gInSc1	výkon
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrný	úměrný	k2eAgInSc1d1	úměrný
třetí	třetí	k4xOgFnSc6	třetí
mocnině	mocnina	k1gFnSc3	mocnina
rychlosti	rychlost	k1gFnSc2	rychlost
proudící	proudící	k2eAgFnSc2d1	proudící
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
masy	masa	k1gFnSc2	masa
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
značně	značně	k6eAd1	značně
kolísá	kolísat	k5eAaImIp3nS	kolísat
<g/>
,	,	kIx,	,
nedosahují	dosahovat	k5eNaImIp3nP	dosahovat
větrné	větrný	k2eAgFnPc4d1	větrná
elektrárny	elektrárna	k1gFnPc4	elektrárna
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
doby	doba	k1gFnSc2	doba
nominálních	nominální	k2eAgFnPc2d1	nominální
hodnot	hodnota	k1gFnPc2	hodnota
generovaného	generovaný	k2eAgInSc2d1	generovaný
výkonu	výkon	k1gInSc2	výkon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
převodu	převod	k1gInSc2	převod
na	na	k7c4	na
elektřinu	elektřina	k1gFnSc4	elektřina
přímo	přímo	k6eAd1	přímo
konala	konat	k5eAaImAgFnS	konat
nějaká	nějaký	k3yIgFnSc1	nějaký
mechanická	mechanický	k2eAgFnSc1d1	mechanická
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Větrný	větrný	k2eAgInSc1d1	větrný
mlýn	mlýn	k1gInSc1	mlýn
například	například	k6eAd1	například
mlel	mlít	k5eAaImAgInS	mlít
obilí	obilí	k1gNnSc4	obilí
<g/>
,	,	kIx,	,
větrnými	větrný	k2eAgInPc7d1	větrný
stroji	stroj	k1gInPc7	stroj
se	se	k3xPyFc4	se
čerpala	čerpat	k5eAaImAgFnS	čerpat
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
lisoval	lisovat	k5eAaImAgMnS	lisovat
olej	olej	k1gInSc4	olej
<g/>
,	,	kIx,	,
stloukala	stloukat	k5eAaImAgFnS	stloukat
plsť	plsť	k1gFnSc4	plsť
nebo	nebo	k8xC	nebo
poháněly	pohánět	k5eAaImAgInP	pohánět
katry	katr	k1gInPc1	katr
<g/>
.	.	kIx.	.
</s>
<s>
Vítr	vítr	k1gInSc1	vítr
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
pohonu	pohon	k1gInSc3	pohon
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
nejvíc	hodně	k6eAd3	hodně
u	u	k7c2	u
lodí	loď	k1gFnPc2	loď
(	(	kIx(	(
<g/>
plachetnice	plachetnice	k1gFnSc1	plachetnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Teorie	teorie	k1gFnSc1	teorie
větrné	větrný	k2eAgFnSc2d1	větrná
elektrárny	elektrárna	k1gFnSc2	elektrárna
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Teoreticky	teoreticky	k6eAd1	teoreticky
dosažitelný	dosažitelný	k2eAgInSc1d1	dosažitelný
výkon	výkon	k1gInSc1	výkon
===	===	k?	===
</s>
</p>
<p>
<s>
Proudící	proudící	k2eAgInSc1d1	proudící
vzduch	vzduch	k1gInSc1	vzduch
předává	předávat	k5eAaImIp3nS	předávat
lopatkám	lopatka	k1gFnPc3	lopatka
větrné	větrný	k2eAgFnSc2d1	větrná
elektrárny	elektrárna	k1gFnSc2	elektrárna
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Albert	Albert	k1gMnSc1	Albert
Betz	Betz	k1gMnSc1	Betz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
odvodil	odvodit	k5eAaPmAgMnS	odvodit
teoreticky	teoreticky	k6eAd1	teoreticky
maximální	maximální	k2eAgFnSc4d1	maximální
dosažitelnou	dosažitelný	k2eAgFnSc4d1	dosažitelná
účinnost	účinnost	k1gFnSc4	účinnost
větrného	větrný	k2eAgInSc2d1	větrný
stroje	stroj	k1gInSc2	stroj
na	na	k7c4	na
59,3	[number]	k4	59,3
%	%	kIx~	%
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Betzovo	Betzův	k2eAgNnSc1d1	Betzův
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
větru	vítr	k1gInSc2	vítr
se	se	k3xPyFc4	se
v	v	k7c6	v
turbíně	turbína	k1gFnSc6	turbína
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
otáčivého	otáčivý	k2eAgInSc2d1	otáčivý
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
následně	následně	k6eAd1	následně
v	v	k7c6	v
generátoru	generátor	k1gInSc6	generátor
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
<g/>
.	.	kIx.	.
</s>
<s>
Teoreticky	teoreticky	k6eAd1	teoreticky
dosažitelný	dosažitelný	k2eAgInSc1d1	dosažitelný
výkon	výkon	k1gInSc1	výkon
činí	činit	k5eAaImIp3nS	činit
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jednotkové	jednotkový	k2eAgFnSc2d1	jednotková
plochy	plocha	k1gFnSc2	plocha
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
ρ	ρ	k?	ρ
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P_	P_	k1gFnSc6	P_
<g/>
{	{	kIx(	{
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
k_	k_	k?	k_
<g/>
{	{	kIx(	{
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
\	\	kIx~	\
<g/>
rho	rho	k?	rho
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kB	kb	kA	kb
je	být	k5eAaImIp3nS	být
Betzův	Betzův	k2eAgInSc1d1	Betzův
koeficient	koeficient	k1gInSc1	koeficient
0,59	[number]	k4	0,59
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
reálné	reálný	k2eAgFnPc4d1	reálná
turbíny	turbína	k1gFnPc4	turbína
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
rotoru	rotor	k1gInSc2	rotor
D	D	kA	D
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
délkou	délka	k1gFnSc7	délka
lopatky	lopatka	k1gFnSc2	lopatka
D	D	kA	D
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
vzorec	vzorec	k1gInSc1	vzorec
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
ρ	ρ	k?	ρ
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
=	=	kIx~	=
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
\	\	kIx~	\
<g/>
rho	rho	k?	rho
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
D	D	kA	D
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
cp	cp	k?	cp
je	být	k5eAaImIp3nS	být
součinitel	součinitel	k1gInSc4	součinitel
výkonnosti	výkonnost	k1gFnSc2	výkonnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
ideálním	ideální	k2eAgInSc6d1	ideální
případě	případ	k1gInSc6	případ
rovný	rovný	k2eAgInSc4d1	rovný
0,59	[number]	k4	0,59
</s>
</p>
<p>
<s>
===	===	k?	===
Účinnost	účinnost	k1gFnSc4	účinnost
===	===	k?	===
</s>
</p>
<p>
<s>
Součinitel	součinitel	k1gInSc1	součinitel
výkonnosti	výkonnost	k1gFnSc2	výkonnost
je	být	k5eAaImIp3nS	být
sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
funkcí	funkce	k1gFnSc7	funkce
rychlosti	rychlost	k1gFnSc2	rychlost
větru	vítr	k1gInSc2	vítr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dán	dán	k2eAgInSc1d1	dán
konstrukčním	konstrukční	k2eAgNnSc7d1	konstrukční
řešením	řešení	k1gNnSc7	řešení
turbíny	turbína	k1gFnSc2	turbína
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
převodní	převodní	k2eAgFnSc7d1	převodní
křivkou	křivka	k1gFnSc7	křivka
úhlu	úhel	k1gInSc2	úhel
natočení	natočení	k1gNnSc2	natočení
lopatek	lopatka	k1gFnPc2	lopatka
turbíny	turbína	k1gFnSc2	turbína
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
kubickou	kubický	k2eAgFnSc7d1	kubická
závislostí	závislost	k1gFnSc7	závislost
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
větru	vítr	k1gInSc2	vítr
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
pronikavou	pronikavý	k2eAgFnSc4d1	pronikavá
závislost	závislost	k1gFnSc4	závislost
skutečného	skutečný	k2eAgInSc2d1	skutečný
výkonu	výkon	k1gInSc2	výkon
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
větru	vítr	k1gInSc2	vítr
(	(	kIx(	(
<g/>
při	při	k7c6	při
poloviční	poloviční	k2eAgFnSc6d1	poloviční
rychlosti	rychlost	k1gFnSc6	rychlost
je	být	k5eAaImIp3nS	být
výkon	výkon	k1gInSc4	výkon
osminový	osminový	k2eAgInSc4d1	osminový
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
podstatnou	podstatný	k2eAgFnSc7d1	podstatná
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
,	,	kIx,	,
definující	definující	k2eAgFnSc4d1	definující
účinnost	účinnost	k1gFnSc4	účinnost
větrného	větrný	k2eAgInSc2d1	větrný
zdroje	zdroj	k1gInSc2	zdroj
je	být	k5eAaImIp3nS	být
koeficient	koeficient	k1gInSc1	koeficient
ročního	roční	k2eAgNnSc2d1	roční
využití	využití	k1gNnSc2	využití
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
definovaný	definovaný	k2eAgInSc4d1	definovaný
jako	jako	k8xC	jako
poměr	poměr	k1gInSc4	poměr
skutečně	skutečně	k6eAd1	skutečně
odvedeného	odvedený	k2eAgInSc2d1	odvedený
výkonu	výkon	k1gInSc2	výkon
k	k	k7c3	k
teoreticky	teoreticky	k6eAd1	teoreticky
možnému	možný	k2eAgInSc3d1	možný
výkonu	výkon	k1gInSc3	výkon
zdroje	zdroj	k1gInSc2	zdroj
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
podmínkách	podmínka	k1gFnPc6	podmínka
se	se	k3xPyFc4	se
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
mezích	mez	k1gFnPc6	mez
0,1	[number]	k4	0,1
<g/>
–	–	k?	–
<g/>
0,2	[number]	k4	0,2
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
velmi	velmi	k6eAd1	velmi
větrné	větrný	k2eAgFnPc4d1	větrná
lokality	lokalita	k1gFnPc4	lokalita
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
teoreticky	teoreticky	k6eAd1	teoreticky
až	až	k9	až
0,28	[number]	k4	0,28
<g/>
.	.	kIx.	.
</s>
<s>
Statisticky	statisticky	k6eAd1	statisticky
podle	podle	k7c2	podle
dat	datum	k1gNnPc2	datum
ČSÚ	ČSÚ	kA	ČSÚ
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2007	[number]	k4	2007
však	však	k9	však
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
koeficient	koeficient	k1gInSc1	koeficient
ročního	roční	k2eAgNnSc2d1	roční
využití	využití	k1gNnSc2	využití
větrných	větrný	k2eAgFnPc2d1	větrná
elektráren	elektrárna	k1gFnPc2	elektrárna
v	v	k7c6	v
ČR	ČR	kA	ČR
pouze	pouze	k6eAd1	pouze
12,71	[number]	k4	12,71
%	%	kIx~	%
(	(	kIx(	(
<g/>
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2005	[number]	k4	2005
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
11	[number]	k4	11
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
ovšem	ovšem	k9	ovšem
značně	značně	k6eAd1	značně
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
zvolené	zvolený	k2eAgFnSc6d1	zvolená
lokalitě	lokalita	k1gFnSc6	lokalita
–	–	k?	–
větrná	větrný	k2eAgFnSc1d1	větrná
farma	farma	k1gFnSc1	farma
Sternwald	Sternwalda	k1gFnPc2	Sternwalda
na	na	k7c6	na
rakousko-českých	rakousko-český	k2eAgFnPc6d1	rakousko-česká
hranicích	hranice	k1gFnPc6	hranice
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Šumavy	Šumava	k1gFnSc2	Šumava
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
se	s	k7c7	s
7	[number]	k4	7
větrnými	větrný	k2eAgFnPc7d1	větrná
elektrárnami	elektrárna	k1gFnPc7	elektrárna
o	o	k7c6	o
instalovaném	instalovaný	k2eAgInSc6d1	instalovaný
výkonu	výkon	k1gInSc6	výkon
14	[number]	k4	14
MW	MW	kA	MW
koeficientu	koeficient	k1gInSc2	koeficient
ročního	roční	k2eAgNnSc2d1	roční
využití	využití	k1gNnSc2	využití
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2006	[number]	k4	2006
21,9	[number]	k4	21,9
%	%	kIx~	%
<g/>
,	,	kIx,	,
za	za	k7c2	za
první	první	k4xOgFnSc2	první
4	[number]	k4	4
měsíce	měsíc	k1gInSc2	měsíc
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
hodnoty	hodnota	k1gFnPc4	hodnota
průměrného	průměrný	k2eAgNnSc2d1	průměrné
využití	využití	k1gNnSc2	využití
32,3	[number]	k4	32,3
%	%	kIx~	%
(	(	kIx(	(
<g/>
přičemž	přičemž	k6eAd1	přičemž
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
necelých	celý	k2eNgNnPc2d1	necelé
20	[number]	k4	20
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgFnPc1d1	další
ztráty	ztráta	k1gFnPc1	ztráta
však	však	k9	však
vznikají	vznikat	k5eAaImIp3nP	vznikat
i	i	k9	i
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
částech	část	k1gFnPc6	část
soustrojí	soustrojí	k1gNnSc2	soustrojí
větrné	větrný	k2eAgFnSc2d1	větrná
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Účinnost	účinnost	k1gFnSc1	účinnost
soustrojí	soustrojí	k1gNnSc2	soustrojí
se	se	k3xPyFc4	se
určí	určit	k5eAaPmIp3nS	určit
součinem	součin	k1gInSc7	součin
účinnosti	účinnost	k1gFnSc2	účinnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
soustrojí	soustrojí	k1gNnSc2	soustrojí
elektrárny	elektrárna	k1gFnSc2	elektrárna
(	(	kIx(	(
<g/>
rotoru	rotor	k1gInSc2	rotor
<g/>
,	,	kIx,	,
převodovky	převodovka	k1gFnSc2	převodovka
a	a	k8xC	a
generátoru	generátor	k1gInSc2	generátor
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
η	η	k?	η
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
η	η	k?	η
</s>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
η	η	k?	η
</s>
</p>
<p>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
\	\	kIx~	\
<g/>
eta	eta	k?	eta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
\	\	kIx~	\
<g/>
eta	eta	k?	eta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
G	G	kA	G
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
v	v	k7c6	v
obecných	obecný	k2eAgFnPc6d1	obecná
podmínkách	podmínka	k1gFnPc6	podmínka
===	===	k?	===
</s>
</p>
<p>
<s>
Spektrum	spektrum	k1gNnSc1	spektrum
rozložení	rozložení	k1gNnSc1	rozložení
hustoty	hustota	k1gFnSc2	hustota
rychlostí	rychlost	k1gFnPc2	rychlost
větru	vítr	k1gInSc2	vítr
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
lokalitě	lokalita	k1gFnSc6	lokalita
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
popsatelné	popsatelný	k2eAgNnSc1d1	popsatelné
Rayleighovým	Rayleighův	k2eAgNnSc7d1	Rayleighovo
rozdělením	rozdělení	k1gNnSc7	rozdělení
(	(	kIx(	(
<g/>
normální	normální	k2eAgNnSc1d1	normální
rozdělení	rozdělení	k1gNnSc1	rozdělení
v	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
souřadnicích	souřadnice	k1gFnPc6	souřadnice
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
speciálním	speciální	k2eAgInSc7d1	speciální
případem	případ	k1gInSc7	případ
rozdělení	rozdělení	k1gNnSc4	rozdělení
Weibullova	Weibullův	k2eAgNnSc2d1	Weibullovo
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
funkci	funkce	k1gFnSc4	funkce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
β	β	k?	β
</s>
</p>
<p>
<s>
η	η	k?	η
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
η	η	k?	η
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
β	β	k?	β
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
η	η	k?	η
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
β	β	k?	β
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
v	v	k7c6	v
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
-1	-1	k4	-1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c4	v
je	on	k3xPp3gFnPc4	on
náhodně	náhodně	k6eAd1	náhodně
proměnná	proměnný	k2eAgFnSc1d1	proměnná
rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
β	β	k?	β
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
tvarový	tvarový	k2eAgInSc1d1	tvarový
parametr	parametr	k1gInSc1	parametr
rozložení	rozložení	k1gNnSc2	rozložení
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
η	η	k?	η
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
eta	eta	k?	eta
}	}	kIx)	}
</s>
</p>
<p>
<s>
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
střední	střední	k2eAgFnSc3d1	střední
hodnotě	hodnota	k1gFnSc3	hodnota
rychlosti	rychlost	k1gFnSc2	rychlost
větru	vítr	k1gInSc2	vítr
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
η	η	k?	η
</s>
</p>
<p>
<s>
≈	≈	k?	≈
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
~	~	kIx~	~
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0.886	[number]	k4	0.886
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
eta	eta	k?	eta
\	\	kIx~	\
<g/>
approx	approx	k1gInSc1	approx
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
0.886	[number]	k4	0.886
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
maximum	maximum	k1gNnSc4	maximum
hustoty	hustota	k1gFnSc2	hustota
výskytu	výskyt	k1gInSc2	výskyt
rychlostí	rychlost	k1gFnPc2	rychlost
bude	být	k5eAaImBp3nS	být
vždy	vždy	k6eAd1	vždy
ležet	ležet	k5eAaImF	ležet
vlevo	vlevo	k6eAd1	vlevo
od	od	k7c2	od
hustoty	hustota	k1gFnSc2	hustota
výskytu	výskyt	k1gInSc2	výskyt
střední	střední	k2eAgFnSc2d1	střední
rychlosti	rychlost	k1gFnSc2	rychlost
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
reálné	reálný	k2eAgNnSc4d1	reálné
použití	použití	k1gNnSc4	použití
má	mít	k5eAaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
výskytu	výskyt	k1gInSc6	výskyt
rozsahu	rozsah	k1gInSc6	rozsah
rychlostí	rychlost	k1gFnSc7	rychlost
větru	vítr	k1gInSc2	vítr
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
(	(	kIx(	(
<g/>
v	v	k7c4	v
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
v	v	k7c6	v
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
jako	jako	k8xC	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
∫	∫	k?	∫
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P_	P_	k1gMnPc6	P_
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
v	v	k7c6	v
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
v	v	k7c6	v
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Hlučnost	hlučnost	k1gFnSc1	hlučnost
větrných	větrný	k2eAgFnPc2d1	větrná
elektráren	elektrárna	k1gFnPc2	elektrárna
==	==	k?	==
</s>
</p>
<p>
<s>
Větrné	větrný	k2eAgFnPc1d1	větrná
elektrárny	elektrárna	k1gFnPc1	elektrárna
jsou	být	k5eAaImIp3nP	být
zdrojem	zdroj	k1gInSc7	zdroj
nežádoucího	žádoucí	k2eNgInSc2d1	nežádoucí
hluku	hluk	k1gInSc2	hluk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
hlavními	hlavní	k2eAgMnPc7d1	hlavní
původci	původce	k1gMnPc7	původce
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
uvažovány	uvažován	k2eAgInPc4d1	uvažován
zdroje	zdroj	k1gInPc4	zdroj
<g/>
,	,	kIx,	,
pracující	pracující	k2eAgInPc4d1	pracující
s	s	k7c7	s
vrtulí	vrtule	k1gFnSc7	vrtule
na	na	k7c4	na
nabíhající	nabíhající	k2eAgInSc4d1	nabíhající
vzdušný	vzdušný	k2eAgInSc4d1	vzdušný
proud	proud	k1gInSc4	proud
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
aerodynamické	aerodynamický	k2eAgInPc4d1	aerodynamický
hluky	hluk	k1gInPc4	hluk
obtékání	obtékání	k1gNnSc2	obtékání
listů	list	k1gInPc2	list
vrtule	vrtule	k1gFnSc2	vrtule
<g/>
,	,	kIx,	,
gondoly	gondola	k1gFnSc2	gondola
a	a	k8xC	a
dříku	dřík	k1gInSc2	dřík
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
turbulence	turbulence	k1gFnSc2	turbulence
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgNnSc1d1	vznikající
obtékáním	obtékání	k1gNnSc7	obtékání
náběžné	náběžný	k2eAgFnSc2d1	náběžná
hrany	hrana	k1gFnSc2	hrana
listu	list	k1gInSc2	list
<g/>
,	,	kIx,	,
víry	vír	k1gInPc1	vír
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
konců	konec	k1gInPc2	konec
vrtulových	vrtulový	k2eAgInPc2d1	vrtulový
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
turbulence	turbulence	k1gFnPc1	turbulence
nad	nad	k7c7	nad
odtokovou	odtokový	k2eAgFnSc7d1	odtoková
hranou	hrana	k1gFnSc7	hrana
listu	list	k1gInSc2	list
a	a	k8xC	a
hluk	hluk	k1gInSc1	hluk
laminárního	laminární	k2eAgNnSc2d1	laminární
proudění	proudění	k1gNnSc2	proudění
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
hluk	hluk	k1gInSc1	hluk
produkován	produkovat	k5eAaImNgInS	produkovat
mechanickými	mechanický	k2eAgFnPc7d1	mechanická
částmi	část	k1gFnPc7	část
konstrukce	konstrukce	k1gFnSc2	konstrukce
(	(	kIx(	(
<g/>
servomotory	servomotor	k1gInPc1	servomotor
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
převody	převod	k1gInPc1	převod
<g/>
,	,	kIx,	,
čerpadla	čerpadlo	k1gNnPc1	čerpadlo
<g/>
,	,	kIx,	,
chladicí	chladicí	k2eAgFnSc1d1	chladicí
ventilátory	ventilátor	k1gInPc4	ventilátor
měničů	měnič	k1gMnPc2	měnič
a	a	k8xC	a
mechanismů	mechanismus	k1gInPc2	mechanismus
<g/>
)	)	kIx)	)
a	a	k8xC	a
generátorem	generátor	k1gInSc7	generátor
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
slyšitelné	slyšitelný	k2eAgNnSc4d1	slyšitelné
pásmo	pásmo	k1gNnSc4	pásmo
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
frekvencí	frekvence	k1gFnSc7	frekvence
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
31,5	[number]	k4	31,5
Hz	Hz	kA	Hz
(	(	kIx(	(
<g/>
infrazvuk	infrazvuk	k1gInSc1	infrazvuk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hluk	hluk	k1gInSc1	hluk
větrných	větrný	k2eAgFnPc2d1	větrná
elektráren	elektrárna	k1gFnPc2	elektrárna
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Šíření	šíření	k1gNnPc1	šíření
hluku	hluk	k1gInSc2	hluk
větrného	větrný	k2eAgInSc2d1	větrný
zdroje	zdroj	k1gInSc2	zdroj
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
model	model	k1gInSc1	model
šíření	šíření	k1gNnSc2	šíření
používána	používán	k2eAgFnSc1d1	používána
náhrada	náhrada	k1gFnSc1	náhrada
prostředí	prostředí	k1gNnSc6	prostředí
hemisférou	hemisféra	k1gFnSc7	hemisféra
s	s	k7c7	s
homogenními	homogenní	k2eAgFnPc7d1	homogenní
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
modelu	model	k1gInSc6	model
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
hlasitost	hlasitost	k1gFnSc4	hlasitost
hluku	hluk	k1gInSc2	hluk
s	s	k7c7	s
danou	daný	k2eAgFnSc7d1	daná
intenzitou	intenzita	k1gFnSc7	intenzita
a	a	k8xC	a
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
dle	dle	k7c2	dle
vzorce	vzorec	k1gInSc2	vzorec
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gFnSc6	L_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
L_	L_	k1gFnSc1	L_
<g/>
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
log_	log_	k?	log_
<g/>
{	{	kIx(	{
<g/>
10	[number]	k4	10
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
cdot	cdot	k2eAgMnSc1d1	cdot
R	R	kA	R
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
R	R	kA	R
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
R	R	kA	R
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
zdroje	zdroj	k1gInSc2	zdroj
hluku	hluk	k1gInSc2	hluk
a	a	k8xC	a
α	α	k?	α
je	být	k5eAaImIp3nS	být
součinitel	součinitel	k1gInSc4	součinitel
absorpce	absorpce	k1gFnSc2	absorpce
<g/>
,	,	kIx,	,
přijímaný	přijímaný	k2eAgInSc1d1	přijímaný
pro	pro	k7c4	pro
suchý	suchý	k2eAgInSc4d1	suchý
vzduch	vzduch	k1gInSc4	vzduch
α	α	k?	α
<g/>
=	=	kIx~	=
<g/>
0.005	[number]	k4	0.005
dBm	dBm	k?	dBm
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zdroj	zdroj	k1gInSc1	zdroj
hluku	hluk	k1gInSc2	hluk
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
bodový	bodový	k2eAgInSc4d1	bodový
<g/>
.	.	kIx.	.
</s>
<s>
Metodika	metodika	k1gFnSc1	metodika
měření	měření	k1gNnSc2	měření
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
IEC	IEC	kA	IEC
61400-11	[number]	k4	61400-11
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
hygienické	hygienický	k2eAgFnPc1d1	hygienická
normy	norma	k1gFnPc1	norma
připouštějí	připouštět	k5eAaImIp3nP	připouštět
maximální	maximální	k2eAgFnSc4d1	maximální
úroveň	úroveň	k1gFnSc4	úroveň
hluku	hluk	k1gInSc2	hluk
v	v	k7c6	v
obytné	obytný	k2eAgFnSc6d1	obytná
zástavbě	zástavba	k1gFnSc6	zástavba
50	[number]	k4	50
dBA	dBA	k?	dBA
ve	v	k7c6	v
dne	den	k1gInSc2	den
a	a	k8xC	a
40	[number]	k4	40
dBA	dBA	k?	dBA
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
přípustná	přípustný	k2eAgFnSc1d1	přípustná
úroveň	úroveň	k1gFnSc1	úroveň
hluku	hluk	k1gInSc2	hluk
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
není	být	k5eNaImIp3nS	být
stanovena	stanoven	k2eAgFnSc1d1	stanovena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Infrazvuky	infrazvuk	k1gInPc4	infrazvuk
===	===	k?	===
</s>
</p>
<p>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
infrazvuků	infrazvuk	k1gInPc2	infrazvuk
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
mechanické	mechanický	k2eAgFnPc1d1	mechanická
části	část	k1gFnPc1	část
konstrukce	konstrukce	k1gFnSc2	konstrukce
větrných	větrný	k2eAgFnPc2d1	větrná
turbín	turbína	k1gFnPc2	turbína
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
stanovení	stanovení	k1gNnSc4	stanovení
jejich	jejich	k3xOp3gFnSc2	jejich
intenzity	intenzita	k1gFnSc2	intenzita
nelze	lze	k6eNd1	lze
používat	používat	k5eAaImF	používat
hlukoměry	hlukoměr	k1gInPc4	hlukoměr
s	s	k7c7	s
filtrem	filtr	k1gInSc7	filtr
křivky	křivka	k1gFnSc2	křivka
A	A	kA	A
(	(	kIx(	(
<g/>
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
citlivosti	citlivost	k1gFnSc2	citlivost
ucha	ucho	k1gNnSc2	ucho
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
infrazvuky	infrazvuk	k1gInPc4	infrazvuk
potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
<g/>
.	.	kIx.	.
</s>
<s>
Hluk	hluk	k1gInSc1	hluk
větrných	větrný	k2eAgFnPc2d1	větrná
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
,	,	kIx,	,
emitovaný	emitovaný	k2eAgInSc1d1	emitovaný
v	v	k7c6	v
infrazvukové	infrazvukový	k2eAgFnSc6d1	infrazvuková
oblasti	oblast	k1gFnSc6	oblast
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
70	[number]	k4	70
dB	db	kA	db
(	(	kIx(	(
<g/>
Vestas	Vestas	k1gMnSc1	Vestas
V-52	V-52	k1gMnSc1	V-52
70	[number]	k4	70
dB	db	kA	db
na	na	k7c4	na
frekvenci	frekvence	k1gFnSc4	frekvence
16	[number]	k4	16
Hz	Hz	kA	Hz
<g/>
,	,	kIx,	,
Vestas	Vestas	k1gInSc1	Vestas
V-80	V-80	k1gFnSc1	V-80
72	[number]	k4	72
dB	db	kA	db
ve	v	k7c6	v
frekvenčním	frekvenční	k2eAgInSc6d1	frekvenční
rozsahu	rozsah	k1gInSc6	rozsah
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
Hz	Hz	kA	Hz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
pásmu	pásmo	k1gNnSc6	pásmo
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přirozenému	přirozený	k2eAgNnSc3d1	přirozené
hlukovému	hlukový	k2eAgNnSc3d1	hlukové
pozadí	pozadí	k1gNnSc3	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Infrazvukové	infrazvukový	k2eAgNnSc1d1	infrazvukový
vlnění	vlnění	k1gNnSc1	vlnění
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
cesty	cesta	k1gFnSc2	cesta
šíří	šíř	k1gFnPc2	šíř
i	i	k8xC	i
konstrukcí	konstrukce	k1gFnPc2	konstrukce
dříku	dřík	k1gInSc2	dřík
a	a	k8xC	a
základovou	základový	k2eAgFnSc7d1	základová
deskou	deska	k1gFnSc7	deska
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hluky	hluk	k1gInPc4	hluk
<g/>
,	,	kIx,	,
typické	typický	k2eAgInPc4d1	typický
pro	pro	k7c4	pro
větrný	větrný	k2eAgInSc4d1	větrný
zdroj	zdroj	k1gInSc4	zdroj
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
==	==	k?	==
</s>
</p>
<p>
<s>
Větrné	větrný	k2eAgFnPc1d1	větrná
elektrárny	elektrárna	k1gFnPc1	elektrárna
působí	působit	k5eAaImIp3nP	působit
na	na	k7c4	na
dravé	dravý	k2eAgMnPc4d1	dravý
ptáky	pták	k1gMnPc4	pták
v	v	k7c6	v
ekosystému	ekosystém	k1gInSc6	ekosystém
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
jejich	jejich	k3xOp3gMnPc1	jejich
predátoři	predátor	k1gMnPc1	predátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Větrné	větrný	k2eAgFnSc2d1	větrná
elektrárny	elektrárna	k1gFnSc2	elektrárna
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
instalovaný	instalovaný	k2eAgInSc1d1	instalovaný
výkon	výkon	k1gInSc1	výkon
větrných	větrný	k2eAgFnPc2d1	větrná
elektráren	elektrárna	k1gFnPc2	elektrárna
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
316	[number]	k4	316
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
větrné	větrný	k2eAgFnSc2d1	větrná
elektrárny	elektrárna	k1gFnSc2	elektrárna
vyrobily	vyrobit	k5eAaPmAgFnP	vyrobit
609	[number]	k4	609
GWh	GWh	k1gFnPc2	GWh
brutto	brutto	k2eAgFnPc2d1	brutto
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
0,8	[number]	k4	0,8
%	%	kIx~	%
hrubé	hrubý	k2eAgFnSc2d1	hrubá
konečné	konečný	k2eAgFnSc2d1	konečná
spotřeby	spotřeba	k1gFnSc2	spotřeba
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
dopočteno	dopočten	k2eAgNnSc1d1	dopočten
podle	podle	k7c2	podle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
to	ten	k3xDgNnSc1	ten
také	také	k6eAd1	také
průměrnému	průměrný	k2eAgInSc3d1	průměrný
výkonu	výkon	k1gInSc3	výkon
69,5	[number]	k4	69,5
MW	MW	kA	MW
(	(	kIx(	(
<g/>
koeficient	koeficient	k1gInSc1	koeficient
ročního	roční	k2eAgNnSc2d1	roční
využití	využití	k1gNnSc2	využití
21,99	[number]	k4	21,99
%	%	kIx~	%
pro	pro	k7c4	pro
skutečně	skutečně	k6eAd1	skutečně
dodanou	dodaný	k2eAgFnSc4d1	dodaná
energii	energie	k1gFnSc4	energie
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
21,68	[number]	k4	21,68
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
fyziky	fyzika	k1gFnSc2	fyzika
atmosféry	atmosféra	k1gFnSc2	atmosféra
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
technický	technický	k2eAgInSc4d1	technický
potenciál	potenciál	k1gInSc4	potenciál
větrné	větrný	k2eAgFnSc2d1	větrná
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
na	na	k7c4	na
29	[number]	k4	29
GW	GW	kA	GW
a	a	k8xC	a
71	[number]	k4	71
TWh	TWh	k1gFnPc2	TWh
ročně	ročně	k6eAd1	ročně
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
96,1	[number]	k4	96,1
%	%	kIx~	%
roční	roční	k2eAgFnSc2d1	roční
hrubé	hrubý	k2eAgFnSc2d1	hrubá
spotřeby	spotřeba	k1gFnSc2	spotřeba
ČR	ČR	kA	ČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
–	–	k?	–
dopočteno	dopočten	k2eAgNnSc4d1	dopočten
podle	podle	k7c2	podle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Technický	technický	k2eAgInSc1d1	technický
odhad	odhad	k1gInSc1	odhad
uvažoval	uvažovat	k5eAaImAgInS	uvažovat
využití	využití	k1gNnSc4	využití
tehdy	tehdy	k6eAd1	tehdy
dostupných	dostupný	k2eAgFnPc2d1	dostupná
větrných	větrný	k2eAgFnPc2d1	větrná
turbín	turbína	k1gFnPc2	turbína
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
kolem	kolem	k7c2	kolem
100	[number]	k4	100
m	m	kA	m
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ignoroval	ignorovat	k5eAaImAgMnS	ignorovat
většinu	většina	k1gFnSc4	většina
jiných	jiný	k2eAgNnPc2d1	jiné
praktických	praktický	k2eAgNnPc2d1	praktické
omezení	omezení	k1gNnPc2	omezení
–	–	k?	–
např.	např.	kA	např.
vlastnická	vlastnický	k2eAgNnPc4d1	vlastnické
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
zastavěnost	zastavěnost	k1gFnSc4	zastavěnost
pozemků	pozemek	k1gInPc2	pozemek
<g/>
,	,	kIx,	,
ochranná	ochranný	k2eAgNnPc4d1	ochranné
pásma	pásmo	k1gNnPc4	pásmo
radarových	radarový	k2eAgInPc2d1	radarový
a	a	k8xC	a
telekomunikačních	telekomunikační	k2eAgNnPc2d1	telekomunikační
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
jiná	jiný	k2eAgNnPc4d1	jiné
omezení	omezení	k1gNnSc4	omezení
–	–	k?	–
jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
teoretické	teoretický	k2eAgNnSc4d1	teoretické
maximum	maximum	k1gNnSc4	maximum
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
by	by	kYmCp3nS	by
šlo	jít	k5eAaImAgNnS	jít
čistě	čistě	k6eAd1	čistě
technicky	technicky	k6eAd1	technicky
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
realizovatelných	realizovatelný	k2eAgFnPc2d1	realizovatelná
větrných	větrný	k2eAgFnPc2d1	větrná
elektráren	elektrárna	k1gFnPc2	elektrárna
Ústavu	ústav	k1gInSc2	ústav
fyziky	fyzika	k1gFnSc2	fyzika
atmosféry	atmosféra	k1gFnSc2	atmosféra
AVČR	AVČR	kA	AVČR
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zapracoval	zapracovat	k5eAaPmAgInS	zapracovat
i	i	k9	i
dodatečná	dodatečný	k2eAgNnPc1d1	dodatečné
praktická	praktický	k2eAgNnPc1d1	praktické
omezení	omezení	k1gNnPc1	omezení
stavby	stavba	k1gFnSc2	stavba
větrných	větrný	k2eAgFnPc2d1	větrná
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
,	,	kIx,	,
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
v	v	k7c6	v
ČR	ČR	kA	ČR
podle	podle	k7c2	podle
nejméně	málo	k6eAd3	málo
příznivého	příznivý	k2eAgInSc2d1	příznivý
scénáře	scénář	k1gInSc2	scénář
potenciál	potenciál	k1gInSc1	potenciál
pro	pro	k7c4	pro
472	[number]	k4	472
větrných	větrný	k2eAgFnPc2d1	větrná
turbín	turbína	k1gFnPc2	turbína
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
991	[number]	k4	991
MW	MW	kA	MW
s	s	k7c7	s
produkcí	produkce	k1gFnSc7	produkce
2,4	[number]	k4	2,4
TWh	TWh	k1gFnPc2	TWh
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
středního	střední	k2eAgInSc2d1	střední
scénáře	scénář	k1gInSc2	scénář
1179	[number]	k4	1179
turbín	turbína	k1gFnPc2	turbína
s	s	k7c7	s
celkovým	celkový	k2eAgInSc7d1	celkový
výkonem	výkon	k1gInSc7	výkon
2516	[number]	k4	2516
MW	MW	kA	MW
a	a	k8xC	a
produkcí	produkce	k1gFnSc7	produkce
5,6	[number]	k4	5,6
TWh	TWh	k1gFnPc2	TWh
za	za	k7c4	za
rok	rok	k1gInSc4	rok
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
pro	pro	k7c4	pro
větrnou	větrný	k2eAgFnSc4d1	větrná
energii	energie	k1gFnSc4	energie
nejpříznivějšího	příznivý	k2eAgInSc2d3	nejpříznivější
scénáře	scénář	k1gInSc2	scénář
potenciál	potenciál	k1gInSc1	potenciál
2736	[number]	k4	2736
turbín	turbína	k1gFnPc2	turbína
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
5972	[number]	k4	5972
MW	MW	kA	MW
s	s	k7c7	s
produkcí	produkce	k1gFnSc7	produkce
14,7	[number]	k4	14,7
TWh	TWh	k1gFnPc2	TWh
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
Ústav	ústav	k1gInSc1	ústav
zpřesnil	zpřesnit	k5eAaPmAgInS	zpřesnit
svůj	svůj	k3xOyFgInSc4	svůj
odhad	odhad	k1gInSc4	odhad
středního	střední	k2eAgInSc2d1	střední
scénáře	scénář	k1gInSc2	scénář
na	na	k7c4	na
potenciál	potenciál	k1gInSc4	potenciál
759	[number]	k4	759
turbín	turbína	k1gFnPc2	turbína
o	o	k7c6	o
celkovém	celkový	k2eAgInSc6d1	celkový
výkonu	výkon	k1gInSc6	výkon
2277	[number]	k4	2277
MW	MW	kA	MW
s	s	k7c7	s
roční	roční	k2eAgFnSc7d1	roční
výrobou	výroba	k1gFnSc7	výroba
5,9	[number]	k4	5,9
TWh	TWh	k1gFnPc2	TWh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Největší	veliký	k2eAgFnSc1d3	veliký
větrná	větrný	k2eAgFnSc1d1	větrná
elektrárna	elektrárna	k1gFnSc1	elektrárna
na	na	k7c6	na
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Zatím	zatím	k6eAd1	zatím
největší	veliký	k2eAgFnSc4d3	veliký
větrnou	větrný	k2eAgFnSc4d1	větrná
farmu	farma	k1gFnSc4	farma
na	na	k7c6	na
světě	svět	k1gInSc6	svět
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
spuštěna	spuštěn	k2eAgFnSc1d1	spuštěna
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Větrná	větrný	k2eAgFnSc1d1	větrná
farma	farma	k1gFnSc1	farma
Roscoe	Rosco	k1gFnSc2	Rosco
má	mít	k5eAaImIp3nS	mít
výkon	výkon	k1gInSc4	výkon
781,5	[number]	k4	781,5
MW	MW	kA	MW
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
627	[number]	k4	627
větrnými	větrný	k2eAgFnPc7d1	větrná
turbínami	turbína	k1gFnPc7	turbína
<g/>
.	.	kIx.	.
</s>
<s>
Roscoe	Roscoe	k1gFnSc1	Roscoe
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
pokrýt	pokrýt	k5eAaPmF	pokrýt
spotřebu	spotřeba	k1gFnSc4	spotřeba
230	[number]	k4	230
000	[number]	k4	000
domácností	domácnost	k1gFnPc2	domácnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
pokrytí	pokrytí	k1gNnSc1	pokrytí
výroby	výroba	k1gFnSc2	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
pomocí	pomocí	k7c2	pomocí
větru	vítr	k1gInSc2	vítr
==	==	k?	==
</s>
</p>
<p>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
energetika	energetika	k1gFnSc1	energetika
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
ráno	ráno	k6eAd1	ráno
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
rekord	rekord	k1gInSc1	rekord
<g/>
,	,	kIx,	,
energie	energie	k1gFnSc1	energie
z	z	k7c2	z
větrných	větrný	k2eAgFnPc2d1	větrná
elektráren	elektrárna	k1gFnPc2	elektrárna
tam	tam	k6eAd1	tam
pokryla	pokrýt	k5eAaPmAgFnS	pokrýt
přes	přes	k7c4	přes
54	[number]	k4	54
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
poptávky	poptávka	k1gFnSc2	poptávka
po	po	k7c6	po
elektřině	elektřina	k1gFnSc6	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
výkonu	výkon	k1gInSc3	výkon
přes	přes	k7c4	přes
10	[number]	k4	10
000	[number]	k4	000
megawattů	megawatt	k1gInPc2	megawatt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejvýkonnější	výkonný	k2eAgFnSc2d3	nejvýkonnější
větrné	větrný	k2eAgFnSc2d1	větrná
turbíny	turbína	k1gFnSc2	turbína
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
nejvýkonnější	výkonný	k2eAgFnPc1d3	nejvýkonnější
větrné	větrný	k2eAgFnPc1d1	větrná
turbíny	turbína	k1gFnPc1	turbína
měly	mít	k5eAaImAgFnP	mít
výkon	výkon	k1gInSc4	výkon
8,8	[number]	k4	8,8
MW	MW	kA	MW
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
fyziky	fyzika	k1gFnSc2	fyzika
atmosféry	atmosféra	k1gFnSc2	atmosféra
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Odhad	odhad	k1gInSc1	odhad
realizovatelného	realizovatelný	k2eAgInSc2d1	realizovatelný
potenciálu	potenciál	k1gInSc2	potenciál
větrné	větrný	k2eAgFnSc2d1	větrná
energie	energie	k1gFnSc2	energie
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
fyziky	fyzika	k1gFnSc2	fyzika
atmosféry	atmosféra	k1gFnSc2	atmosféra
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Aktualizovaný	aktualizovaný	k2eAgInSc1d1	aktualizovaný
odhad	odhad	k1gInSc1	odhad
realizovatelného	realizovatelný	k2eAgInSc2d1	realizovatelný
potenciálu	potenciál	k1gInSc2	potenciál
větrné	větrný	k2eAgFnSc2d1	větrná
energie	energie	k1gFnSc2	energie
z	z	k7c2	z
perspektivy	perspektiva	k1gFnSc2	perspektiva
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Větrná	větrný	k2eAgFnSc1d1	větrná
turbína	turbína	k1gFnSc1	turbína
</s>
</p>
<p>
<s>
Větrná	větrný	k2eAgFnSc1d1	větrná
farma	farma	k1gFnSc1	farma
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
větrných	větrný	k2eAgFnPc2d1	větrná
elektráren	elektrárna	k1gFnPc2	elektrárna
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
větrná	větrný	k2eAgFnSc1d1	větrná
energie	energie	k1gFnSc1	energie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
větrná	větrný	k2eAgFnSc1d1	větrná
energie	energie	k1gFnSc1	energie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
větrnou	větrný	k2eAgFnSc4d1	větrná
energii	energie	k1gFnSc4	energie
</s>
</p>
<p>
<s>
Přeměna	přeměna	k1gFnSc1	přeměna
větrné	větrný	k2eAgFnSc2d1	větrná
energie	energie	k1gFnSc2	energie
na	na	k7c4	na
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
-	-	kIx~	-
větrné	větrný	k2eAgFnPc4d1	větrná
turbíny	turbína	k1gFnPc4	turbína
</s>
</p>
