<s>
Benzín	benzín	k1gInSc1	benzín
(	(	kIx(	(
<g/>
též	též	k9	též
benzin	benzin	k1gInSc1	benzin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kapalina	kapalina	k1gFnSc1	kapalina
ropného	ropný	k2eAgInSc2d1	ropný
původu	původ	k1gInSc2	původ
používaná	používaný	k2eAgFnSc1d1	používaná
hlavně	hlavně	k6eAd1	hlavně
jako	jako	k8xS	jako
palivo	palivo	k1gNnSc4	palivo
v	v	k7c6	v
zážehových	zážehový	k2eAgInPc6d1	zážehový
spalovacích	spalovací	k2eAgInPc6d1	spalovací
motorech	motor	k1gInPc6	motor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jako	jako	k9	jako
rozpouštědlo	rozpouštědlo	k1gNnSc1	rozpouštědlo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
ředění	ředění	k1gNnSc4	ředění
nátěrových	nátěrový	k2eAgFnPc2d1	nátěrová
hmot	hmota	k1gFnPc2	hmota
<g/>
.	.	kIx.	.
</s>
