<p>
<s>
Benzín	benzín	k1gInSc1	benzín
(	(	kIx(	(
<g/>
též	též	k9	též
benzin	benzin	k1gInSc1	benzin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kapalina	kapalina	k1gFnSc1	kapalina
ropného	ropný	k2eAgInSc2d1	ropný
původu	původ	k1gInSc2	původ
používaná	používaný	k2eAgFnSc1d1	používaná
hlavně	hlavně	k6eAd1	hlavně
jako	jako	k8xC	jako
palivo	palivo	k1gNnSc4	palivo
v	v	k7c6	v
zážehových	zážehový	k2eAgInPc6d1	zážehový
spalovacích	spalovací	k2eAgInPc6d1	spalovací
motorech	motor	k1gInPc6	motor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jako	jako	k9	jako
rozpouštědlo	rozpouštědlo	k1gNnSc1	rozpouštědlo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
ředění	ředění	k1gNnSc4	ředění
nátěrových	nátěrový	k2eAgFnPc2d1	nátěrová
hmot	hmota	k1gFnPc2	hmota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
především	především	k9	především
z	z	k7c2	z
alifatických	alifatický	k2eAgInPc2d1	alifatický
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
získávaných	získávaný	k2eAgInPc2d1	získávaný
frakční	frakční	k2eAgFnSc7d1	frakční
destilací	destilace	k1gFnSc7	destilace
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
s	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
isooktanu	isooktan	k1gInSc2	isooktan
nebo	nebo	k8xC	nebo
aromatických	aromatický	k2eAgInPc2d1	aromatický
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
toluenu	toluen	k1gInSc2	toluen
a	a	k8xC	a
benzenu	benzen	k1gInSc2	benzen
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
oktanového	oktanový	k2eAgNnSc2d1	oktanové
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
přidávají	přidávat	k5eAaImIp3nP	přidávat
také	také	k9	také
malá	malý	k2eAgNnPc4d1	malé
množství	množství	k1gNnPc4	množství
různých	různý	k2eAgNnPc2d1	různé
aditiv	aditivum	k1gNnPc2	aditivum
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pro	pro	k7c4	pro
zlepšení	zlepšení	k1gNnSc4	zlepšení
výkonu	výkon	k1gInSc2	výkon
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
snížení	snížení	k1gNnSc4	snížení
škodlivých	škodlivý	k2eAgFnPc2d1	škodlivá
emisí	emise	k1gFnPc2	emise
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
směsi	směs	k1gFnPc1	směs
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
významné	významný	k2eAgNnSc4d1	významné
množství	množství	k1gNnSc4	množství
ethanolu	ethanol	k1gInSc2	ethanol
jakožto	jakožto	k8xS	jakožto
částečně	částečně	k6eAd1	částečně
alternativního	alternativní	k2eAgNnSc2d1	alternativní
paliva	palivo	k1gNnSc2	palivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Benzin	benzin	k1gInSc1	benzin
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
v	v	k7c6	v
ropných	ropný	k2eAgFnPc6d1	ropná
rafineriích	rafinerie	k1gFnPc6	rafinerie
<g/>
.	.	kIx.	.
</s>
<s>
Materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
získá	získat	k5eAaPmIp3nS	získat
oddělením	oddělení	k1gNnSc7	oddělení
ze	z	k7c2	z
surové	surový	k2eAgFnSc2d1	surová
ropy	ropa	k1gFnSc2	ropa
destilací	destilace	k1gFnPc2	destilace
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
"	"	kIx"	"
<g/>
panenský	panenský	k2eAgInSc1d1	panenský
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
přímý	přímý	k2eAgInSc1d1	přímý
benzin	benzin	k1gInSc1	benzin
(	(	kIx(	(
<g/>
přímý	přímý	k2eAgInSc1d1	přímý
ropný	ropný	k2eAgInSc1d1	ropný
destilát	destilát	k1gInSc1	destilát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nesplňuje	splňovat	k5eNaImIp3nS	splňovat
požadavky	požadavek	k1gInPc4	požadavek
pro	pro	k7c4	pro
moderní	moderní	k2eAgInPc4d1	moderní
motory	motor	k1gInPc4	motor
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
oktanové	oktanový	k2eAgNnSc1d1	oktanové
číslo	číslo	k1gNnSc1	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
směsi	směs	k1gFnSc2	směs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základ	základ	k1gInSc1	základ
typického	typický	k2eAgInSc2d1	typický
benzinu	benzin	k1gInSc2	benzin
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
uhlovodíky	uhlovodík	k1gInPc7	uhlovodík
se	s	k7c7	s
čtyřmi	čtyři	k4xCgNnPc7	čtyři
až	až	k6eAd1	až
dvanácti	dvanáct	k4xCc7	dvanáct
atomy	atom	k1gInPc7	atom
uhlíku	uhlík	k1gInSc2	uhlík
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
<g/>
.	.	kIx.	.
<g/>
Mnoho	mnoho	k6eAd1	mnoho
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nebezpečné	bezpečný	k2eNgFnPc4d1	nebezpečná
látky	látka	k1gFnPc4	látka
a	a	k8xC	a
podléhá	podléhat	k5eAaImIp3nS	podléhat
proto	proto	k8xC	proto
regulaci	regulace	k1gFnSc4	regulace
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
list	list	k1gInSc1	list
bezolovnatého	bezolovnatý	k2eAgInSc2d1	bezolovnatý
benzinu	benzin	k1gInSc2	benzin
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
nejméně	málo	k6eAd3	málo
patnáct	patnáct	k4xCc4	patnáct
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
chemikálií	chemikálie	k1gFnPc2	chemikálie
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
množstvích	množství	k1gNnPc6	množství
-	-	kIx~	-
včetně	včetně	k7c2	včetně
benzenu	benzen	k1gInSc2	benzen
(	(	kIx(	(
<g/>
až	až	k9	až
5	[number]	k4	5
%	%	kIx~	%
objemově	objemově	k6eAd1	objemově
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
toluenu	toluen	k1gInSc2	toluen
(	(	kIx(	(
<g/>
až	až	k9	až
35	[number]	k4	35
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naftalenu	naftalen	k1gInSc2	naftalen
(	(	kIx(	(
<g/>
až	až	k9	až
1	[number]	k4	1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trimethylbenzenu	trimethylbenzen	k2eAgFnSc4d1	trimethylbenzen
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
7	[number]	k4	7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
MTBE	MTBE	kA	MTBE
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
až	až	k9	až
18	[number]	k4	18
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
asi	asi	k9	asi
deseti	deset	k4xCc2	deset
dalších	další	k1gNnPc2	další
<g/>
.	.	kIx.	.
<g/>
Každá	každý	k3xTgFnSc1	každý
ze	z	k7c2	z
složek	složka	k1gFnPc2	složka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
benzinu	benzin	k1gInSc2	benzin
mísí	mísit	k5eAaImIp3nP	mísit
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
jiné	jiný	k2eAgFnPc4d1	jiná
charakteristiky	charakteristika	k1gFnPc4	charakteristika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
důležité	důležitý	k2eAgFnPc4d1	důležitá
složky	složka	k1gFnPc4	složka
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
reformát	reformát	k1gMnSc1	reformát
-	-	kIx~	-
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
v	v	k7c6	v
katalytickém	katalytický	k2eAgMnSc6d1	katalytický
reformátoru	reformátor	k1gMnSc6	reformátor
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vysoké	vysoký	k2eAgNnSc1d1	vysoké
oktanové	oktanový	k2eAgNnSc1d1	oktanové
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
vysoký	vysoký	k2eAgInSc1d1	vysoký
obsah	obsah	k1gInSc1	obsah
arenů	aren	k1gInPc2	aren
a	a	k8xC	a
málo	málo	k4c1	málo
alkenů	alken	k1gMnPc2	alken
</s>
</p>
<p>
<s>
katalyticky	katalyticky	k6eAd1	katalyticky
krakovaný	krakovaný	k2eAgInSc1d1	krakovaný
benzin	benzin	k1gInSc1	benzin
nebo	nebo	k8xC	nebo
katalyticky	katalyticky	k6eAd1	katalyticky
krakovaná	krakovaný	k2eAgFnSc1d1	krakovaný
nafta	nafta	k1gFnSc1	nafta
-	-	kIx~	-
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
v	v	k7c6	v
katalytickém	katalytický	k2eAgInSc6d1	katalytický
krakeru	kraker	k1gInSc6	kraker
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgNnSc1d2	menší
oktanové	oktanový	k2eAgNnSc1d1	oktanové
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
vysoký	vysoký	k2eAgInSc1d1	vysoký
obsah	obsah	k1gInSc1	obsah
alkenů	alken	k1gMnPc2	alken
a	a	k8xC	a
mírný	mírný	k2eAgInSc1d1	mírný
obsah	obsah	k1gInSc1	obsah
arenů	aren	k1gMnPc2	aren
</s>
</p>
<p>
<s>
hydrokrakát	hydrokrakát	k1gInSc1	hydrokrakát
(	(	kIx(	(
<g/>
těžký	těžký	k2eAgInSc4d1	těžký
<g/>
,	,	kIx,	,
střední	střední	k2eAgInSc4d1	střední
a	a	k8xC	a
lehký	lehký	k2eAgInSc4d1	lehký
<g/>
)	)	kIx)	)
-	-	kIx~	-
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
v	v	k7c6	v
hydrokrakeru	hydrokraker	k1gInSc6	hydrokraker
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
střední	střední	k2eAgFnSc1d1	střední
až	až	k9	až
nízké	nízký	k2eAgNnSc1d1	nízké
oktanové	oktanový	k2eAgNnSc1d1	oktanové
číslo	číslo	k1gNnSc1	číslo
a	a	k8xC	a
mírný	mírný	k2eAgInSc1d1	mírný
obsah	obsah	k1gInSc1	obsah
aromatických	aromatický	k2eAgInPc2d1	aromatický
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
</s>
</p>
<p>
<s>
panenský	panenský	k2eAgInSc4d1	panenský
čili	čili	k8xC	čili
přímý	přímý	k2eAgInSc4d1	přímý
naftový	naftový	k2eAgInSc4d1	naftový
destilát	destilát	k1gInSc4	destilát
-	-	kIx~	-
destiluje	destilovat	k5eAaImIp3nS	destilovat
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
ze	z	k7c2	z
surové	surový	k2eAgFnSc2d1	surová
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
nízké	nízký	k2eAgNnSc1d1	nízké
oktanové	oktanový	k2eAgNnSc1d1	oktanové
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
nízký	nízký	k2eAgInSc1d1	nízký
obsah	obsah	k1gInSc1	obsah
arenů	aren	k1gInPc2	aren
(	(	kIx(	(
<g/>
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
složení	složení	k1gNnSc6	složení
surové	surový	k2eAgFnSc2d1	surová
ropy	ropa	k1gFnSc2	ropa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
určité	určitý	k2eAgNnSc1d1	určité
množství	množství	k1gNnSc1	množství
naftenů	naften	k1gInPc2	naften
(	(	kIx(	(
<g/>
cykloalkanů	cykloalkan	k1gInPc2	cykloalkan
<g/>
)	)	kIx)	)
a	a	k8xC	a
žádné	žádný	k3yNgInPc4	žádný
olefiny	olefin	k1gInPc4	olefin
(	(	kIx(	(
<g/>
alkeny	alkena	k1gFnPc4	alkena
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
alkylát	alkylát	k1gInSc1	alkylát
-	-	kIx~	-
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
v	v	k7c6	v
alkylační	alkylační	k2eAgFnSc6d1	alkylační
jednotce	jednotka	k1gFnSc6	jednotka
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vysoké	vysoký	k2eAgNnSc1d1	vysoké
oktanové	oktanový	k2eAgNnSc1d1	oktanové
číslo	číslo	k1gNnSc1	číslo
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
čisté	čistý	k2eAgFnPc4d1	čistá
alkany	alkana	k1gFnPc4	alkana
<g/>
,	,	kIx,	,
především	především	k9	především
s	s	k7c7	s
rozvětvenými	rozvětvený	k2eAgInPc7d1	rozvětvený
řetězci	řetězec	k1gInPc7	řetězec
</s>
</p>
<p>
<s>
izomerát	izomerát	k1gInSc1	izomerát
(	(	kIx(	(
<g/>
různých	různý	k2eAgInPc2d1	různý
názvů	název	k1gInPc2	název
<g/>
)	)	kIx)	)
-	-	kIx~	-
získává	získávat	k5eAaImIp3nS	získávat
se	se	k3xPyFc4	se
izomerizací	izomerizace	k1gFnSc7	izomerizace
pentanu	pentan	k1gInSc2	pentan
a	a	k8xC	a
hexanu	hexan	k1gInSc2	hexan
v	v	k7c6	v
lehkém	lehký	k2eAgInSc6d1	lehký
přímém	přímý	k2eAgInSc6d1	přímý
destilátu	destilát	k1gInSc6	destilát
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
získat	získat	k5eAaPmF	získat
vysokooktanové	vysokooktanový	k2eAgInPc4d1	vysokooktanový
izomery	izomer	k1gInPc4	izomer
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
Uvedené	uvedený	k2eAgInPc1d1	uvedený
termíny	termín	k1gInPc1	termín
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
naftařské	naftařský	k2eAgFnSc6d1	naftařská
hantýrce	hantýrka	k1gFnSc6	hantýrka
<g/>
.	.	kIx.	.
</s>
<s>
Přesná	přesný	k2eAgFnSc1d1	přesná
terminologie	terminologie	k1gFnSc1	terminologie
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
podle	podle	k7c2	podle
rafinerie	rafinerie	k1gFnSc2	rafinerie
a	a	k8xC	a
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
benzin	benzin	k1gInSc1	benzin
především	především	k6eAd1	především
směsí	směs	k1gFnSc7	směs
parafinů	parafin	k1gInPc2	parafin
(	(	kIx(	(
<g/>
alkanů	alkan	k1gInPc2	alkan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naftenů	naften	k1gInPc2	naften
(	(	kIx(	(
<g/>
cykloalkanů	cykloalkan	k1gInPc2	cykloalkan
<g/>
)	)	kIx)	)
a	a	k8xC	a
olefinů	olefin	k1gInPc2	olefin
(	(	kIx(	(
<g/>
alkenů	alken	k1gInPc2	alken
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgInPc1d1	přesný
poměry	poměr	k1gInPc1	poměr
závisí	záviset	k5eAaImIp3nP	záviset
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
rafinerii	rafinerie	k1gFnSc4	rafinerie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
benzin	benzin	k1gInSc4	benzin
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
<g/>
;	;	kIx,	;
různé	různý	k2eAgFnPc1d1	různá
rafinerie	rafinerie	k1gFnPc1	rafinerie
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgFnPc1d1	různá
výrobní	výrobní	k2eAgFnPc1d1	výrobní
jednotky	jednotka	k1gFnPc1	jednotka
</s>
</p>
<p>
<s>
surové	surový	k2eAgFnSc3d1	surová
ropě	ropa	k1gFnSc3	ropa
použité	použitý	k2eAgFnSc2d1	použitá
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
</s>
</p>
<p>
<s>
cílové	cílový	k2eAgFnSc3d1	cílová
třídě	třída	k1gFnSc3	třída
benzinu	benzin	k1gInSc2	benzin
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
oktanovém	oktanový	k2eAgNnSc6d1	oktanové
čísle	číslo	k1gNnSc6	číslo
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
mnoho	mnoho	k4c4	mnoho
zemí	zem	k1gFnPc2	zem
určuje	určovat	k5eAaImIp3nS	určovat
limity	limit	k1gInPc4	limit
celkového	celkový	k2eAgInSc2d1	celkový
obsahu	obsah	k1gInSc2	obsah
arenů	aren	k1gInPc2	aren
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
benzenu	benzen	k1gInSc3	benzen
<g/>
)	)	kIx)	)
a	a	k8xC	a
alkenů	alken	k1gInPc2	alken
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
regulace	regulace	k1gFnPc1	regulace
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
preferenci	preference	k1gFnSc3	preference
vysokooktanových	vysokooktanový	k2eAgFnPc2d1	vysokooktanová
alkanových	alkanův	k2eAgFnPc2d1	alkanův
složek	složka	k1gFnPc2	složka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
alkylátů	alkylát	k1gInPc2	alkylát
<g/>
,	,	kIx,	,
a	a	k8xC	a
nutí	nutit	k5eAaImIp3nP	nutit
rafinerie	rafinerie	k1gFnPc1	rafinerie
přidávat	přidávat	k5eAaImF	přidávat
procesní	procesní	k2eAgFnPc4d1	procesní
jednotky	jednotka	k1gFnPc4	jednotka
ke	k	k7c3	k
snižování	snižování	k1gNnSc3	snižování
obsahu	obsah	k1gInSc2	obsah
benzenu	benzen	k1gInSc2	benzen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Benzin	benzin	k1gInSc1	benzin
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ethery	ether	k1gInPc1	ether
(	(	kIx(	(
<g/>
přidávané	přidávaný	k2eAgFnPc1d1	přidávaná
úmyslně	úmyslně	k6eAd1	úmyslně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
malá	malý	k2eAgNnPc4d1	malé
množství	množství	k1gNnPc4	množství
různých	různý	k2eAgInPc2d1	různý
kontaminantů	kontaminant	k1gInPc2	kontaminant
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
sloučenin	sloučenina	k1gFnPc2	sloučenina
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
disulfidů	disulfid	k1gInPc2	disulfid
a	a	k8xC	a
thiofenů	thiofen	k1gInPc2	thiofen
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
kontaminanty	kontaminant	k1gInPc1	kontaminant
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
thioly	thiola	k1gFnSc2	thiola
a	a	k8xC	a
sulfan	sulfana	k1gFnPc2	sulfana
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
korozi	koroze	k1gFnSc4	koroze
motorů	motor	k1gInPc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
síry	síra	k1gFnSc2	síra
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
odstraňují	odstraňovat	k5eAaImIp3nP	odstraňovat
převodem	převod	k1gInSc7	převod
na	na	k7c4	na
sulfan	sulfan	k1gInSc4	sulfan
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
lze	lze	k6eAd1	lze
poté	poté	k6eAd1	poté
transformovat	transformovat	k5eAaBmF	transformovat
na	na	k7c4	na
elementární	elementární	k2eAgFnSc4d1	elementární
síru	síra	k1gFnSc4	síra
pomocí	pomocí	k7c2	pomocí
Clausova	Clausův	k2eAgInSc2d1	Clausův
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hustota	hustota	k1gFnSc1	hustota
===	===	k?	===
</s>
</p>
<p>
<s>
Hustota	hustota	k1gFnSc1	hustota
benzinu	benzin	k1gInSc2	benzin
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
0,64	[number]	k4	0,64
do	do	k7c2	do
0,77	[number]	k4	0,77
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vyšší	vysoký	k2eAgFnSc1d2	vyšší
hustota	hustota	k1gFnSc1	hustota
značí	značit	k5eAaImIp3nS	značit
větší	veliký	k2eAgInSc4d2	veliký
podíl	podíl	k1gInSc4	podíl
arenů	aren	k1gMnPc2	aren
<g/>
.	.	kIx.	.
</s>
<s>
Kvalita	kvalita	k1gFnSc1	kvalita
benzinu	benzin	k1gInSc2	benzin
je	být	k5eAaImIp3nS	být
nepřímo	přímo	k6eNd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
hustotě	hustota	k1gFnSc3	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Benzin	benzin	k1gInSc1	benzin
plave	plavat	k5eAaImIp3nS	plavat
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
<g/>
;	;	kIx,	;
vodu	voda	k1gFnSc4	voda
proto	proto	k8xC	proto
nelze	lze	k6eNd1	lze
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
hašení	hašení	k1gNnSc3	hašení
benzinového	benzinový	k2eAgInSc2d1	benzinový
ohně	oheň	k1gInSc2	oheň
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
rozprašována	rozprašovat	k5eAaImNgFnS	rozprašovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
jemné	jemný	k2eAgFnSc2d1	jemná
mlhy	mlha	k1gFnSc2	mlha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
===	===	k?	===
</s>
</p>
<p>
<s>
Bod	bod	k1gInSc1	bod
varu	var	k1gInSc2	var
benzinu	benzin	k1gInSc2	benzin
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
°	°	k?	°
<g/>
C.	C.	kA	C.
Za	za	k7c4	za
kvalitnější	kvalitní	k2eAgInPc4d2	kvalitnější
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
benzín	benzín	k1gInSc1	benzín
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
teplotou	teplota	k1gFnSc7	teplota
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Těkavost	těkavost	k1gFnSc4	těkavost
===	===	k?	===
</s>
</p>
<p>
<s>
Benzin	benzin	k1gInSc1	benzin
je	být	k5eAaImIp3nS	být
těkavější	těkavý	k2eAgInSc1d2	těkavější
než	než	k8xS	než
nafta	nafta	k1gFnSc1	nafta
nebo	nebo	k8xC	nebo
letecká	letecký	k2eAgNnPc4d1	letecké
paliva	palivo	k1gNnPc4	palivo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
kvůli	kvůli	k7c3	kvůli
hlavním	hlavní	k2eAgFnPc3d1	hlavní
složkám	složka	k1gFnPc3	složka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
přidávaným	přidávaný	k2eAgNnPc3d1	přidávané
aditivům	aditivum	k1gNnPc3	aditivum
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečné	závěrečný	k2eAgNnSc1d1	závěrečné
ovládání	ovládání	k1gNnSc1	ovládání
těkavosti	těkavost	k1gFnSc2	těkavost
se	se	k3xPyFc4	se
často	často	k6eAd1	často
provádí	provádět	k5eAaImIp3nS	provádět
přidáváním	přidávání	k1gNnSc7	přidávání
butanu	butan	k1gInSc2	butan
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zjištění	zjištění	k1gNnSc4	zjištění
těkavosti	těkavost	k1gFnSc2	těkavost
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
měření	měření	k1gNnPc1	měření
Reidova	Reidův	k2eAgInSc2d1	Reidův
tlaku	tlak	k1gInSc2	tlak
par	para	k1gFnPc2	para
(	(	kIx(	(
<g/>
RVP	RVP	kA	RVP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Požadovaná	požadovaný	k2eAgFnSc1d1	požadovaná
těkavost	těkavost	k1gFnSc1	těkavost
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
okolní	okolní	k2eAgFnSc6d1	okolní
teplotě	teplota	k1gFnSc6	teplota
<g/>
:	:	kIx,	:
v	v	k7c6	v
horkém	horký	k2eAgNnSc6d1	horké
podnebí	podnebí	k1gNnSc6	podnebí
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
složky	složka	k1gFnPc1	složka
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
molární	molární	k2eAgFnSc7d1	molární
hmotností	hmotnost	k1gFnSc7	hmotnost
a	a	k8xC	a
tedy	tedy	k9	tedy
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
těkavostí	těkavost	k1gFnSc7	těkavost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chladném	chladný	k2eAgNnSc6d1	chladné
prostředí	prostředí	k1gNnSc6	prostředí
vede	vést	k5eAaImIp3nS	vést
příliš	příliš	k6eAd1	příliš
nízká	nízký	k2eAgFnSc1d1	nízká
těkavost	těkavost	k1gFnSc1	těkavost
k	k	k7c3	k
problémům	problém	k1gInPc3	problém
se	se	k3xPyFc4	se
startováním	startování	k1gNnSc7	startování
motorů	motor	k1gInPc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
horku	horko	k1gNnSc6	horko
působí	působit	k5eAaImIp3nS	působit
nadměrná	nadměrný	k2eAgFnSc1d1	nadměrná
těkavost	těkavost	k1gFnSc1	těkavost
problémy	problém	k1gInPc4	problém
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
palivo	palivo	k1gNnSc1	palivo
vypařuje	vypařovat	k5eAaImIp3nS	vypařovat
již	již	k6eAd1	již
v	v	k7c6	v
potrubí	potrubí	k1gNnSc6	potrubí
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
zabránit	zabránit	k5eAaPmF	zabránit
podávacímu	podávací	k2eAgNnSc3d1	podávací
čerpadlu	čerpadlo	k1gNnSc3	čerpadlo
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
účinném	účinný	k2eAgNnSc6d1	účinné
čerpání	čerpání	k1gNnSc6	čerpání
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Toto	tento	k3xDgNnSc1	tento
nastává	nastávat	k5eAaImIp3nS	nastávat
hlavně	hlavně	k9	hlavně
u	u	k7c2	u
čerpadel	čerpadlo	k1gNnPc2	čerpadlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
poháněna	poháněn	k2eAgNnPc1d1	poháněno
přímo	přímo	k6eAd1	přímo
od	od	k7c2	od
motoru	motor	k1gInSc2	motor
a	a	k8xC	a
nemají	mít	k5eNaImIp3nP	mít
návratové	návratový	k2eAgNnSc4d1	návratové
potrubí	potrubí	k1gNnSc4	potrubí
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
motorů	motor	k1gInPc2	motor
se	s	k7c7	s
vstřikováním	vstřikování	k1gNnSc7	vstřikování
paliva	palivo	k1gNnSc2	palivo
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mělo	mít	k5eAaImAgNnS	mít
palivo	palivo	k1gNnSc4	palivo
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
tlak	tlak	k1gInSc1	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
při	při	k7c6	při
startování	startování	k1gNnSc6	startování
jsou	být	k5eAaImIp3nP	být
otáčky	otáčka	k1gFnPc1	otáčka
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
u	u	k7c2	u
těchto	tento	k3xDgNnPc2	tento
vozidel	vozidlo	k1gNnPc2	vozidlo
elektrická	elektrický	k2eAgNnPc1d1	elektrické
čerpadla	čerpadlo	k1gNnPc1	čerpadlo
umístěná	umístěný	k2eAgNnPc1d1	umístěné
na	na	k7c6	na
palivové	palivový	k2eAgFnSc6d1	palivová
nádrži	nádrž	k1gFnSc6	nádrž
a	a	k8xC	a
palivo	palivo	k1gNnSc4	palivo
může	moct	k5eAaImIp3nS	moct
současně	současně	k6eAd1	současně
chladit	chladit	k5eAaImF	chladit
čerpadlo	čerpadlo	k1gNnSc4	čerpadlo
<g/>
.	.	kIx.	.
</s>
<s>
Regulace	regulace	k1gFnSc1	regulace
tlaku	tlak	k1gInSc2	tlak
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
návratem	návrat	k1gInSc7	návrat
nevyužitého	využitý	k2eNgNnSc2d1	nevyužité
paliva	palivo	k1gNnSc2	palivo
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
takových	takový	k3xDgNnPc2	takový
vozidel	vozidlo	k1gNnPc2	vozidlo
proto	proto	k8xC	proto
problém	problém	k1gInSc1	problém
s	s	k7c7	s
předčasným	předčasný	k2eAgNnSc7d1	předčasné
vypařováním	vypařování	k1gNnSc7	vypařování
paliva	palivo	k1gNnSc2	palivo
prakticky	prakticky	k6eAd1	prakticky
nikdy	nikdy	k6eAd1	nikdy
nenastává	nastávat	k5eNaImIp3nS	nastávat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
těkavost	těkavost	k1gFnSc1	těkavost
v	v	k7c6	v
centrech	centrum	k1gNnPc6	centrum
velkých	velký	k2eAgNnPc6d1	velké
měst	město	k1gNnPc2	město
regulována	regulovat	k5eAaImNgNnP	regulovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
omezily	omezit	k5eAaPmAgFnP	omezit
emise	emise	k1gFnPc1	emise
nespálených	spálený	k2eNgMnPc2d1	nespálený
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
je	být	k5eAaImIp3nS	být
povinný	povinný	k2eAgInSc4d1	povinný
tzv.	tzv.	kA	tzv.
reformulovaný	reformulovaný	k2eAgInSc4d1	reformulovaný
benzin	benzin	k1gInSc4	benzin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
méně	málo	k6eAd2	málo
náchylný	náchylný	k2eAgMnSc1d1	náchylný
na	na	k7c4	na
vypařování	vypařování	k1gNnPc4	vypařování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
jsou	být	k5eAaImIp3nP	být
limity	limit	k1gInPc1	limit
na	na	k7c4	na
těkavost	těkavost	k1gFnSc4	těkavost
benzinu	benzin	k1gInSc2	benzin
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
omezovány	omezován	k2eAgInPc1d1	omezován
vládami	vláda	k1gFnPc7	vláda
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
lišit	lišit	k5eAaImF	lišit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
zemí	zem	k1gFnPc2	zem
má	mít	k5eAaImIp3nS	mít
prostě	prostě	k9	prostě
letní	letní	k2eAgFnSc4d1	letní
<g/>
,	,	kIx,	,
zimní	zimní	k2eAgFnSc4d1	zimní
a	a	k8xC	a
případně	případně	k6eAd1	případně
přechodné	přechodný	k2eAgInPc4d1	přechodný
limity	limit	k1gInPc4	limit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Standardy	standard	k1gInPc1	standard
pro	pro	k7c4	pro
těkavost	těkavost	k1gFnSc4	těkavost
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
uvolněny	uvolněn	k2eAgFnPc1d1	uvolněna
(	(	kIx(	(
<g/>
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
vypařit	vypařit	k5eAaPmF	vypařit
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
složek	složka	k1gFnPc2	složka
benzinu	benzin	k1gInSc2	benzin
<g/>
)	)	kIx)	)
během	během	k7c2	během
nouzových	nouzový	k2eAgFnPc2d1	nouzová
situací	situace	k1gFnPc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
benzinu	benzin	k1gInSc2	benzin
nedostatek	nedostatek	k1gInSc1	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
kvůli	kvůli	k7c3	kvůli
hurikánu	hurikán	k1gInSc3	hurikán
Katrina	Katrino	k1gNnSc2	Katrino
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
povoleno	povolit	k5eAaPmNgNnS	povolit
prodávat	prodávat	k5eAaImF	prodávat
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
městských	městský	k2eAgFnPc6d1	městská
oblastech	oblast	k1gFnPc6	oblast
nereformovaný	reformovaný	k2eNgInSc1d1	nereformovaný
benzin	benzin	k1gInSc1	benzin
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
efektivně	efektivně	k6eAd1	efektivně
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
brzy	brzy	k6eAd1	brzy
přejít	přejít	k5eAaPmF	přejít
z	z	k7c2	z
letního	letní	k2eAgInSc2d1	letní
na	na	k7c4	na
zimní	zimní	k2eAgInSc4d1	zimní
benzin	benzin	k1gInSc4	benzin
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Stephena	Stepheno	k1gNnSc2	Stepheno
L.	L.	kA	L.
Johnsona	Johnson	k1gMnSc4	Johnson
z	z	k7c2	z
americké	americký	k2eAgFnSc2d1	americká
Agentury	agentura	k1gFnSc2	agentura
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
platil	platit	k5eAaImAgInS	platit
tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
snižování	snižování	k1gNnSc2	snižování
těkavosti	těkavost	k1gFnSc2	těkavost
paliva	palivo	k1gNnSc2	palivo
pro	pro	k7c4	pro
omezování	omezování	k1gNnSc4	omezování
emisí	emise	k1gFnPc2	emise
nespálených	spálený	k2eNgInPc2d1	nespálený
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
existují	existovat	k5eAaImIp3nP	existovat
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
způsoby	způsob	k1gInPc1	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
vozidla	vozidlo	k1gNnPc1	vozidlo
prodávaná	prodávaný	k2eAgNnPc1d1	prodávané
v	v	k7c6	v
USA	USA	kA	USA
(	(	kIx(	(
<g/>
přinejmenším	přinejmenším	k6eAd1	přinejmenším
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
již	již	k6eAd1	již
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
nebo	nebo	k8xC	nebo
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
palivový	palivový	k2eAgInSc1d1	palivový
systém	systém	k1gInSc1	systém
uzpůsoben	uzpůsoben	k2eAgInSc1d1	uzpůsoben
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
motor	motor	k1gInSc1	motor
neběží	běžet	k5eNaImIp3nS	běžet
<g/>
,	,	kIx,	,
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
rozpínající	rozpínající	k2eAgMnSc1d1	rozpínající
se	se	k3xPyFc4	se
páry	pára	k1gFnSc2	pára
paliva	palivo	k1gNnSc2	palivo
z	z	k7c2	z
nádrže	nádrž	k1gFnSc2	nádrž
v	v	k7c6	v
zásobníku	zásobník	k1gInSc6	zásobník
s	s	k7c7	s
dřevěným	dřevěný	k2eAgNnSc7d1	dřevěné
uhlím	uhlí	k1gNnSc7	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
za	za	k7c2	za
běhu	běh	k1gInSc2	běh
motoru	motor	k1gInSc2	motor
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
až	až	k9	až
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
normální	normální	k2eAgFnSc2d1	normální
pracovní	pracovní	k2eAgFnSc2d1	pracovní
teploty	teplota	k1gFnSc2	teplota
<g/>
)	)	kIx)	)
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
zachycené	zachycený	k2eAgInPc4d1	zachycený
páry	pár	k1gInPc4	pár
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
"	"	kIx"	"
<g/>
čisticí	čisticí	k2eAgInSc4d1	čisticí
ventil	ventil	k1gInSc4	ventil
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
do	do	k7c2	do
sacího	sací	k2eAgNnSc2d1	sací
potrubí	potrubí	k1gNnSc2	potrubí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
spáleny	spálit	k5eAaPmNgFnP	spálit
v	v	k7c6	v
motoru	motor	k1gInSc6	motor
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
také	také	k9	také
utěsněný	utěsněný	k2eAgInSc4d1	utěsněný
uzávěr	uzávěr	k1gInSc4	uzávěr
nádrže	nádrž	k1gFnSc2	nádrž
bránící	bránící	k2eAgNnSc1d1	bránící
vypařování	vypařování	k1gNnSc1	vypařování
plnicím	plnicí	k2eAgNnSc7d1	plnicí
hrdlem	hrdlo	k1gNnSc7	hrdlo
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgNnPc1d1	moderní
vozidla	vozidlo	k1gNnPc1	vozidlo
s	s	k7c7	s
diagnostickým	diagnostický	k2eAgInSc7d1	diagnostický
systémem	systém	k1gInSc7	systém
OBD-II	OBD-II	k1gFnSc2	OBD-II
indikují	indikovat	k5eAaBmIp3nP	indikovat
jako	jako	k9	jako
poruchový	poruchový	k2eAgInSc4d1	poruchový
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
uzávěr	uzávěr	k1gInSc1	uzávěr
chybí	chybit	k5eAaPmIp3nS	chybit
nebo	nebo	k8xC	nebo
nedostatečně	dostatečně	k6eNd1	dostatečně
těsní	těsnit	k5eAaImIp3nS	těsnit
(	(	kIx(	(
<g/>
obecným	obecný	k2eAgInSc7d1	obecný
účelem	účel	k1gInSc7	účel
tohoto	tento	k3xDgInSc2	tento
indikátoru	indikátor	k1gInSc2	indikátor
je	být	k5eAaImIp3nS	být
vyjádření	vyjádření	k1gNnSc4	vyjádření
<g/>
,	,	kIx,	,
že	že	k8xS	že
kterákoli	kterýkoli	k3yIgFnSc1	kterýkoli
část	část	k1gFnSc1	část
systému	systém	k1gInSc2	systém
pro	pro	k7c4	pro
omezení	omezení	k1gNnSc4	omezení
emisí	emise	k1gFnPc2	emise
nepracuje	pracovat	k5eNaImIp3nS	pracovat
správně	správně	k6eAd1	správně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oktanové	oktanový	k2eAgNnSc1d1	oktanové
číslo	číslo	k1gNnSc1	číslo
===	===	k?	===
</s>
</p>
<p>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
charakteristikou	charakteristika	k1gFnSc7	charakteristika
benzinu	benzin	k1gInSc2	benzin
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
oktanové	oktanový	k2eAgNnSc1d1	oktanové
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
benzin	benzin	k1gInSc1	benzin
odolný	odolný	k2eAgInSc1d1	odolný
proti	proti	k7c3	proti
předčasnému	předčasný	k2eAgInSc3d1	předčasný
detonačnímu	detonační	k2eAgInSc3d1	detonační
zážehu	zážeh	k1gInSc3	zážeh
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
klepání	klepání	k1gNnSc1	klepání
motoru	motor	k1gInSc2	motor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Normálním	normální	k2eAgInSc7d1	normální
typem	typ	k1gInSc7	typ
spalování	spalování	k1gNnSc2	spalování
v	v	k7c6	v
motoru	motor	k1gInSc6	motor
je	být	k5eAaImIp3nS	být
deflagrace	deflagrace	k1gFnSc1	deflagrace
<g/>
.	.	kIx.	.
</s>
<s>
Oktanové	oktanový	k2eAgNnSc1d1	oktanové
číslo	číslo	k1gNnSc1	číslo
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
podle	podle	k7c2	podle
směsi	směs	k1gFnSc2	směs
2,2	[number]	k4	2,2
<g/>
,4	,4	k4	,4
<g/>
-trimethylpentanu	rimethylpentan	k1gInSc2	-trimethylpentan
(	(	kIx(	(
<g/>
isooktan	isooktan	k1gInSc1	isooktan
<g/>
,	,	kIx,	,
izomer	izomer	k1gInSc1	izomer
oktanu	oktan	k1gInSc2	oktan
<g/>
)	)	kIx)	)
a	a	k8xC	a
n-heptanu	neptat	k5eAaPmIp1nS	n-heptat
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
různých	různý	k2eAgFnPc2d1	různá
konvencí	konvence	k1gFnPc2	konvence
pro	pro	k7c4	pro
vyjadřování	vyjadřování	k1gNnSc4	vyjadřování
oktanového	oktanový	k2eAgNnSc2d1	oktanové
čísla	číslo	k1gNnSc2	číslo
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
stejné	stejný	k2eAgNnSc1d1	stejné
palivo	palivo	k1gNnSc1	palivo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
označováno	označovat	k5eAaImNgNnS	označovat
různými	různý	k2eAgNnPc7d1	různé
čísly	číslo	k1gNnPc7	číslo
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
použitého	použitý	k2eAgInSc2d1	použitý
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
výzkumná	výzkumný	k2eAgFnSc1d1	výzkumná
metoda	metoda	k1gFnSc1	metoda
a	a	k8xC	a
motorová	motorový	k2eAgFnSc1d1	motorová
metoda	metoda	k1gFnSc1	metoda
<g/>
;	;	kIx,	;
oktanové	oktanový	k2eAgNnSc1d1	oktanové
číslo	číslo	k1gNnSc1	číslo
zjištěné	zjištěný	k2eAgNnSc1d1	zjištěné
motorovou	motorový	k2eAgFnSc7d1	motorová
metodou	metoda	k1gFnSc7	metoda
bývá	bývat	k5eAaImIp3nS	bývat
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
.	.	kIx.	.
<g/>
Oktanové	oktanový	k2eAgNnSc1d1	oktanové
číslo	číslo	k1gNnSc1	číslo
nabylo	nabýt	k5eAaPmAgNnS	nabýt
důležitosti	důležitost	k1gFnPc4	důležitost
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
způsobu	způsob	k1gInSc2	způsob
zvýšení	zvýšení	k1gNnSc2	zvýšení
výkonu	výkon	k1gInSc2	výkon
u	u	k7c2	u
leteckých	letecký	k2eAgInPc2d1	letecký
motorů	motor	k1gInPc2	motor
koncem	koncem	k7c2	koncem
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgNnSc1d2	vyšší
oktanové	oktanový	k2eAgNnSc1d1	oktanové
číslo	číslo	k1gNnSc1	číslo
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
použít	použít	k5eAaPmF	použít
vyšší	vysoký	k2eAgInSc1d2	vyšší
kompresní	kompresní	k2eAgInSc1d1	kompresní
poměr	poměr	k1gInSc1	poměr
a	a	k8xC	a
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
tak	tak	k6eAd1	tak
vyššího	vysoký	k2eAgInSc2d2	vyšší
výkonu	výkon	k1gInSc2	výkon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Energetický	energetický	k2eAgInSc4d1	energetický
obsah	obsah	k1gInSc4	obsah
(	(	kIx(	(
<g/>
výhřevnost	výhřevnost	k1gFnSc4	výhřevnost
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Benzin	benzin	k1gInSc1	benzin
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
cca	cca	kA	cca
32	[number]	k4	32
MJ	mj	kA	mj
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
46,4	[number]	k4	46,4
MJ	mj	kA	mj
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jinak	jinak	k6eAd1	jinak
vyjádřeno	vyjádřit	k5eAaPmNgNnS	vyjádřit
8,89	[number]	k4	8,89
kWh	kwh	kA	kwh
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
12,9	[number]	k4	12,9
kWh	kwh	kA	kwh
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
snazší	snadný	k2eAgNnSc4d2	snazší
porovnání	porovnání	k1gNnSc4	porovnání
s	s	k7c7	s
bateriemi	baterie	k1gFnPc7	baterie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
;	;	kIx,	;
různé	různý	k2eAgFnPc1d1	různá
směsi	směs	k1gFnPc1	směs
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
<g/>
,	,	kIx,	,
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
energetický	energetický	k2eAgInSc4d1	energetický
obsah	obsah	k1gInSc4	obsah
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
americké	americký	k2eAgFnSc2d1	americká
Agentury	agentura	k1gFnSc2	agentura
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
liší	lišit	k5eAaImIp3nP	lišit
sezónu	sezóna	k1gFnSc4	sezóna
od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
různými	různý	k2eAgFnPc7d1	různá
výrobními	výrobní	k2eAgFnPc7d1	výrobní
sériemi	série	k1gFnPc7	série
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
o	o	k7c4	o
až	až	k6eAd1	až
4	[number]	k4	4
%	%	kIx~	%
oproti	oproti	k7c3	oproti
průměru	průměr	k1gInSc3	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
barelu	barel	k1gInSc2	barel
surové	surový	k2eAgFnSc2d1	surová
ropy	ropa	k1gFnSc2	ropa
(	(	kIx(	(
<g/>
cca	cca	kA	cca
160	[number]	k4	160
l	l	kA	l
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
získá	získat	k5eAaPmIp3nS	získat
asi	asi	k9	asi
74	[number]	k4	74
litrů	litr	k1gInPc2	litr
benzinu	benzin	k1gInSc2	benzin
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zhruba	zhruba	k6eAd1	zhruba
46	[number]	k4	46
%	%	kIx~	%
objemových	objemový	k2eAgNnPc2d1	objemové
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgFnSc1d1	zbývající
část	část	k1gFnSc1	část
ropy	ropa	k1gFnSc2	ropa
jsou	být	k5eAaImIp3nP	být
další	další	k2eAgInPc1d1	další
produkty	produkt	k1gInPc1	produkt
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
asfalt	asfalt	k1gInSc1	asfalt
nebo	nebo	k8xC	nebo
nafta	nafta	k1gFnSc1	nafta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Aditiva	aditivum	k1gNnSc2	aditivum
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
aditivaci	aditivace	k1gFnSc6	aditivace
benzinu	benzin	k1gInSc2	benzin
a	a	k8xC	a
o	o	k7c6	o
výhodách	výhoda	k1gFnPc6	výhoda
provozu	provoz	k1gInSc2	provoz
benzinového	benzinový	k2eAgInSc2d1	benzinový
motoru	motor	k1gInSc2	motor
ošetřovaného	ošetřovaný	k2eAgInSc2d1	ošetřovaný
aditivy	aditivum	k1gNnPc7	aditivum
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
článek	článek	k1gInSc1	článek
Aditiva	aditivum	k1gNnSc2	aditivum
do	do	k7c2	do
paliv	palivo	k1gNnPc2	palivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Olovo	olovo	k1gNnSc1	olovo
===	===	k?	===
</s>
</p>
<p>
<s>
Směs	směs	k1gFnSc1	směs
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
benzin	benzin	k1gInSc1	benzin
<g/>
,	,	kIx,	,
použije	použít	k5eAaPmIp3nS	použít
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
ve	v	k7c6	v
spalovacím	spalovací	k2eAgInSc6d1	spalovací
motoru	motor	k1gInSc6	motor
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
kompresí	komprese	k1gFnSc7	komprese
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
k	k	k7c3	k
samozápalům	samozápal	k1gInPc3	samozápal
(	(	kIx(	(
<g/>
detonacím	detonace	k1gFnPc3	detonace
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
klepání	klepání	k1gNnSc4	klepání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
poškozují	poškozovat	k5eAaImIp3nP	poškozovat
motor	motor	k1gInSc4	motor
<g/>
.	.	kIx.	.
</s>
<s>
Časné	časný	k2eAgInPc1d1	časný
výzkumy	výzkum	k1gInPc1	výzkum
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
vedli	vést	k5eAaImAgMnP	vést
A.	A.	kA	A.
H.	H.	kA	H.
Gibson	Gibsona	k1gFnPc2	Gibsona
a	a	k8xC	a
Harry	Harra	k1gFnSc2	Harra
Ricardo	Ricardo	k1gNnSc1	Ricardo
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
Thomas	Thomas	k1gMnSc1	Thomas
Midgley	Midglea	k1gFnSc2	Midglea
a	a	k8xC	a
Thomas	Thomas	k1gMnSc1	Thomas
Boyd	Boyd	k1gMnSc1	Boyd
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
<g/>
,	,	kIx,	,
že	že	k8xS	že
olovnatá	olovnatý	k2eAgNnPc1d1	olovnaté
aditiva	aditivum	k1gNnPc1	aditivum
toto	tento	k3xDgNnSc4	tento
chování	chování	k1gNnSc4	chování
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
širokému	široký	k2eAgNnSc3d1	široké
přijetí	přijetí	k1gNnSc3	přijetí
jejich	jejich	k3xOp3gNnSc2	jejich
používání	používání	k1gNnSc2	používání
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
k	k	k7c3	k
motorům	motor	k1gInPc3	motor
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
kompresním	kompresní	k2eAgInSc7d1	kompresní
poměrem	poměr	k1gInSc7	poměr
a	a	k8xC	a
vyšším	vysoký	k2eAgInSc7d2	vyšší
výkonem	výkon	k1gInSc7	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Nejpopulárnějším	populární	k2eAgNnSc7d3	nejpopulárnější
aditivem	aditivum	k1gNnSc7	aditivum
bylo	být	k5eAaImAgNnS	být
tetraethylolovo	tetraethylolovo	k1gNnSc1	tetraethylolovo
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
objev	objev	k1gInSc1	objev
škodlivých	škodlivý	k2eAgInPc2d1	škodlivý
účinků	účinek	k1gInPc2	účinek
olova	olovo	k1gNnSc2	olovo
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC	a
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
nekompatibilita	nekompatibilita	k1gFnSc1	nekompatibilita
olova	olovo	k1gNnSc2	olovo
s	s	k7c7	s
katalytickými	katalytický	k2eAgInPc7d1	katalytický
konvertory	konvertor	k1gInPc7	konvertor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
instalovány	instalovat	k5eAaBmNgInP	instalovat
skoro	skoro	k6eAd1	skoro
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
americké	americký	k2eAgInPc4d1	americký
automobily	automobil	k1gInPc4	automobil
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
znamenaly	znamenat	k5eAaImAgFnP	znamenat
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
ústup	ústup	k1gInSc1	ústup
od	od	k7c2	od
jeho	jeho	k3xOp3gNnSc2	jeho
používání	používání	k1gNnSc2	používání
(	(	kIx(	(
<g/>
tím	ten	k3xDgNnSc7	ten
spíš	spíš	k9	spíš
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnohé	mnohý	k2eAgInPc1d1	mnohý
státy	stát	k1gInPc1	stát
zavedly	zavést	k5eAaPmAgInP	zavést
různé	různý	k2eAgInPc1d1	různý
zdanění	zdanění	k1gNnSc3	zdanění
na	na	k7c4	na
paliva	palivo	k1gNnPc4	palivo
s	s	k7c7	s
olovem	olovo	k1gNnSc7	olovo
a	a	k8xC	a
bez	bez	k7c2	bez
něho	on	k3xPp3gMnSc2	on
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
států	stát	k1gInPc2	stát
olovnatá	olovnatý	k2eAgNnPc4d1	olovnaté
paliva	palivo	k1gNnPc4	palivo
zakázala	zakázat	k5eAaPmAgFnS	zakázat
<g/>
;	;	kIx,	;
olovo	olovo	k1gNnSc1	olovo
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
jinými	jiný	k2eAgNnPc7d1	jiné
aditivy	aditivum	k1gNnPc7	aditivum
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejpopulárnější	populární	k2eAgInPc4d3	nejpopulárnější
patří	patřit	k5eAaImIp3nS	patřit
aromatické	aromatický	k2eAgInPc1d1	aromatický
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
<g/>
,	,	kIx,	,
ethery	ether	k1gInPc1	ether
a	a	k8xC	a
alkoholy	alkohol	k1gInPc1	alkohol
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
ethanol	ethanol	k1gInSc1	ethanol
nebo	nebo	k8xC	nebo
methanol	methanol	k1gInSc1	methanol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
olovo	olovo	k1gNnSc1	olovo
přidávalo	přidávat	k5eAaImAgNnS	přidávat
do	do	k7c2	do
benzinu	benzin	k1gInSc2	benzin
(	(	kIx(	(
<g/>
především	především	k9	především
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
oktanového	oktanový	k2eAgNnSc2d1	oktanové
čísla	číslo	k1gNnSc2	číslo
<g/>
)	)	kIx)	)
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
standardy	standard	k1gInPc1	standard
na	na	k7c4	na
odstranění	odstranění	k1gNnSc4	odstranění
olovnatého	olovnatý	k2eAgInSc2d1	olovnatý
benzinu	benzin	k1gInSc2	benzin
poprvé	poprvé	k6eAd1	poprvé
implementovány	implementovat	k5eAaImNgInP	implementovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
z	z	k7c2	z
podstatné	podstatný	k2eAgFnSc2d1	podstatná
části	část	k1gFnSc2	část
kvůli	kvůli	k7c3	kvůli
studiím	studie	k1gFnPc3	studie
vedeným	vedený	k2eAgMnSc7d1	vedený
Philipem	Philip	k1gMnSc7	Philip
J.	J.	kA	J.
Landriganem	Landrigan	k1gMnSc7	Landrigan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
tvořila	tvořit	k5eAaImAgFnS	tvořit
olovnatá	olovnatý	k2eAgNnPc1d1	olovnaté
paliva	palivo	k1gNnPc4	palivo
již	již	k6eAd1	již
jen	jen	k9	jen
0,6	[number]	k4	0,6
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
prodeje	prodej	k1gInSc2	prodej
benzinu	benzin	k1gInSc2	benzin
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
méně	málo	k6eAd2	málo
než	než	k8xS	než
1814	[number]	k4	1814
tun	tuna	k1gFnPc2	tuna
olova	olovo	k1gNnSc2	olovo
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1996	[number]	k4	1996
byl	být	k5eAaImAgInS	být
prodej	prodej	k1gInSc1	prodej
olova	olovo	k1gNnSc2	olovo
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
silničních	silniční	k2eAgNnPc6d1	silniční
vozidlech	vozidlo	k1gNnPc6	vozidlo
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
a	a	k8xC	a
používání	používání	k1gNnSc2	používání
olovnatého	olovnatý	k2eAgInSc2d1	olovnatý
benzinu	benzin	k1gInSc2	benzin
v	v	k7c6	v
běžném	běžný	k2eAgNnSc6d1	běžné
silničním	silniční	k2eAgNnSc6d1	silniční
vozidle	vozidlo	k1gNnSc6	vozidlo
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
USA	USA	kA	USA
ukládá	ukládat	k5eAaImIp3nS	ukládat
pokuta	pokuta	k1gFnSc1	pokuta
až	až	k9	až
10	[number]	k4	10
000	[number]	k4	000
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Paliva	palivo	k1gNnPc1	palivo
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
olova	olovo	k1gNnSc2	olovo
se	se	k3xPyFc4	se
však	však	k9	však
smějí	smát	k5eAaImIp3nP	smát
dál	daleko	k6eAd2	daleko
prodávat	prodávat	k5eAaImF	prodávat
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
mimo	mimo	k7c4	mimo
silnice	silnice	k1gFnPc4	silnice
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pro	pro	k7c4	pro
letadla	letadlo	k1gNnPc4	letadlo
<g/>
,	,	kIx,	,
závodní	závodní	k2eAgInPc4d1	závodní
automobily	automobil	k1gInPc4	automobil
<g/>
,	,	kIx,	,
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
stroje	stroj	k1gInPc4	stroj
a	a	k8xC	a
lodní	lodní	k2eAgInPc4d1	lodní
motory	motor	k1gInPc4	motor
<g/>
.	.	kIx.	.
</s>
<s>
Zákaz	zákaz	k1gInSc1	zákaz
olovnatých	olovnatý	k2eAgNnPc2d1	olovnaté
paliv	palivo	k1gNnPc2	palivo
zabránil	zabránit	k5eAaPmAgMnS	zabránit
uvolnění	uvolnění	k1gNnSc4	uvolnění
tisíců	tisíc	k4xCgInPc2	tisíc
tun	tuna	k1gFnPc2	tuna
olova	olovo	k1gNnSc2	olovo
z	z	k7c2	z
automobilů	automobil	k1gInPc2	automobil
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgInPc1d1	podobný
zákazy	zákaz	k1gInPc1	zákaz
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
vedly	vést	k5eAaImAgFnP	vést
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
hladiny	hladina	k1gFnSc2	hladina
olova	olovo	k1gNnSc2	olovo
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
Vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
účinkem	účinek	k1gInSc7	účinek
olovnatých	olovnatý	k2eAgNnPc2d1	olovnaté
aditiv	aditivum	k1gNnPc2	aditivum
byla	být	k5eAaImAgFnS	být
ochrana	ochrana	k1gFnSc1	ochrana
sedel	sedlo	k1gNnPc2	sedlo
ventilů	ventil	k1gInPc2	ventil
před	před	k7c7	před
korozí	koroze	k1gFnSc7	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
motorů	motor	k1gInPc2	motor
klasických	klasický	k2eAgInPc2d1	klasický
automobilů	automobil	k1gInPc2	automobil
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
modifikováno	modifikován	k2eAgNnSc1d1	modifikováno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
schopny	schopen	k2eAgFnPc1d1	schopna
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c4	na
bezolovnatá	bezolovnatý	k2eAgNnPc4d1	bezolovnaté
paliva	palivo	k1gNnPc4	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
i	i	k9	i
náhrady	náhrada	k1gFnPc4	náhrada
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
s	s	k7c7	s
autodíly	autodíl	k1gInPc7	autodíl
<g/>
,	,	kIx,	,
u	u	k7c2	u
čerpacích	čerpací	k2eAgFnPc2d1	čerpací
stanic	stanice	k1gFnPc2	stanice
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
prodávaného	prodávaný	k2eAgInSc2d1	prodávaný
benzinu	benzin	k1gInSc2	benzin
(	(	kIx(	(
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
takové	takový	k3xDgNnSc1	takový
aditivum	aditivum	k1gNnSc1	aditivum
součástí	součást	k1gFnPc2	součást
benzinu	benzin	k1gInSc2	benzin
Special	Special	k1gMnSc1	Special
91	[number]	k4	91
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
náhradní	náhradní	k2eAgNnPc1d1	náhradní
aditiva	aditivum	k1gNnPc1	aditivum
byla	být	k5eAaImAgNnP	být
vědecky	vědecky	k6eAd1	vědecky
testována	testovat	k5eAaImNgFnS	testovat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
schválena	schválit	k5eAaPmNgFnS	schválit
Federací	federace	k1gFnSc7	federace
britských	britský	k2eAgInPc2d1	britský
klubů	klub	k1gInPc2	klub
historických	historický	k2eAgNnPc2d1	historické
vozidel	vozidlo	k1gNnPc2	vozidlo
při	při	k7c6	při
britské	britský	k2eAgFnSc6d1	britská
Výzkumné	výzkumný	k2eAgFnSc6d1	výzkumná
asociaci	asociace	k1gFnSc6	asociace
motorového	motorový	k2eAgInSc2d1	motorový
průmyslu	průmysl	k1gInSc2	průmysl
(	(	kIx(	(
<g/>
MIRA	Mira	k1gFnSc1	Mira
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
se	se	k3xPyFc4	se
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
benzin	benzin	k1gInSc1	benzin
stále	stále	k6eAd1	stále
používá	používat	k5eAaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
byl	být	k5eAaImAgInS	být
zakázán	zakázat	k5eAaPmNgInS	zakázat
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zemí	zem	k1gFnSc7	zem
navrhlo	navrhnout	k5eAaPmAgNnS	navrhnout
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
budoucnosti	budoucnost	k1gFnSc6	budoucnost
olovnatý	olovnatý	k2eAgInSc4d1	olovnatý
benzin	benzin	k1gInSc4	benzin
zakázat	zakázat	k5eAaPmF	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
nyní	nyní	k6eAd1	nyní
existuje	existovat	k5eAaImIp3nS	existovat
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
trh	trh	k1gInSc4	trh
pro	pro	k7c4	pro
olovnatý	olovnatý	k2eAgInSc4d1	olovnatý
benzin	benzin	k1gInSc4	benzin
Four	Four	k1gInSc1	Four
Star	star	k1gInSc1	star
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
historických	historický	k2eAgInPc6d1	historický
automobilech	automobil	k1gInPc6	automobil
a	a	k8xC	a
motocyklech	motocykl	k1gInPc6	motocykl
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgMnSc1d1	jediný
prodejce	prodejce	k1gMnSc1	prodejce
<g/>
,	,	kIx,	,
Bayford	Bayford	k1gMnSc1	Bayford
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
oprávnění	oprávnění	k1gNnSc4	oprávnění
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
k	k	k7c3	k
distribuci	distribuce	k1gFnSc3	distribuce
olovnatých	olovnatý	k2eAgNnPc2d1	olovnaté
paliv	palivo	k1gNnPc2	palivo
-	-	kIx~	-
tato	tento	k3xDgFnSc1	tento
se	se	k3xPyFc4	se
prodávají	prodávat	k5eAaImIp3nP	prodávat
ve	v	k7c6	v
vybraných	vybraný	k2eAgFnPc6d1	vybraná
prodejnách	prodejna	k1gFnPc6	prodejna
po	po	k7c6	po
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
však	však	k9	však
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
hrabstvích	hrabství	k1gNnPc6	hrabství
a	a	k8xC	a
cena	cena	k1gFnSc1	cena
tohoto	tento	k3xDgNnSc2	tento
paliva	palivo	k1gNnSc2	palivo
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
dvakrát	dvakrát	k6eAd1	dvakrát
vyšší	vysoký	k2eAgFnPc1d2	vyšší
než	než	k8xS	než
u	u	k7c2	u
bezolovnatého	bezolovnatý	k2eAgInSc2d1	bezolovnatý
benzinu	benzin	k1gInSc2	benzin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
MMT	MMT	kA	MMT
===	===	k?	===
</s>
</p>
<p>
<s>
Methylcyklopentadienylmangantrikarbonyl	Methylcyklopentadienylmangantrikarbonyl	k1gInSc1	Methylcyklopentadienylmangantrikarbonyl
(	(	kIx(	(
<g/>
MMT	MMT	kA	MMT
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
mnoho	mnoho	k4c1	mnoho
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
také	také	k9	také
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
oktanového	oktanový	k2eAgNnSc2d1	oktanové
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
jako	jako	k8xC	jako
ochrana	ochrana	k1gFnSc1	ochrana
ventilových	ventilový	k2eAgNnPc2d1	ventilové
sedel	sedlo	k1gNnPc2	sedlo
u	u	k7c2	u
starších	starý	k2eAgInPc2d2	starší
vozů	vůz	k1gInPc2	vůz
navržených	navržený	k2eAgInPc2d1	navržený
pro	pro	k7c4	pro
olovnatý	olovnatý	k2eAgInSc4d1	olovnatý
benzin	benzin	k1gInSc4	benzin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americké	americký	k2eAgInPc1d1	americký
federální	federální	k2eAgInPc1d1	federální
zdroje	zdroj	k1gInPc1	zdroj
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
MMT	MMT	kA	MMT
podezřelý	podezřelý	k2eAgMnSc1d1	podezřelý
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
toxický	toxický	k2eAgInSc1d1	toxický
pro	pro	k7c4	pro
nervový	nervový	k2eAgInSc4d1	nervový
a	a	k8xC	a
dýchací	dýchací	k2eAgInSc4d1	dýchací
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
kanadská	kanadský	k2eAgFnSc1d1	kanadská
studie	studie	k1gFnSc1	studie
navíc	navíc	k6eAd1	navíc
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
MMT	MMT	kA	MMT
narušuje	narušovat	k5eAaImIp3nS	narušovat
účinnost	účinnost	k1gFnSc4	účinnost
systémů	systém	k1gInPc2	systém
pro	pro	k7c4	pro
snižování	snižování	k1gNnSc4	snižování
emisí	emise	k1gFnPc2	emise
automobilů	automobil	k1gInPc2	automobil
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
proto	proto	k8xC	proto
ke	k	k7c3	k
vyšším	vysoký	k2eAgFnPc3d2	vyšší
emisím	emise	k1gFnPc3	emise
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
USA	USA	kA	USA
použití	použití	k1gNnSc2	použití
MMT	MMT	kA	MMT
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
Ethyl	ethyl	k1gInSc1	ethyl
Corporation	Corporation	k1gInSc1	Corporation
neprokáže	prokázat	k5eNaPmIp3nS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
používání	používání	k1gNnSc1	používání
tohoto	tento	k3xDgNnSc2	tento
aditiva	aditivum	k1gNnSc2	aditivum
nemohlo	moct	k5eNaImAgNnS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
selhání	selhání	k1gNnSc3	selhání
systémů	systém	k1gInPc2	systém
snižování	snižování	k1gNnSc2	snižování
emisí	emise	k1gFnPc2	emise
u	u	k7c2	u
nových	nový	k2eAgNnPc2d1	nové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
výsledek	výsledek	k1gInSc1	výsledek
tohoto	tento	k3xDgNnSc2	tento
ustanovení	ustanovení	k1gNnSc2	ustanovení
zahájila	zahájit	k5eAaPmAgFnS	zahájit
Ethyl	ethyl	k1gInSc4	ethyl
Corporation	Corporation	k1gInSc1	Corporation
právní	právní	k2eAgFnSc4d1	právní
bitvu	bitva	k1gFnSc4	bitva
s	s	k7c7	s
EPA	EPA	kA	EPA
a	a	k8xC	a
předkládala	předkládat	k5eAaImAgFnS	předkládat
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
MMT	MMT	kA	MMT
systémům	systém	k1gInPc3	systém
pro	pro	k7c4	pro
snižování	snižování	k1gNnSc4	snižování
emisí	emise	k1gFnPc2	emise
neškodí	škodit	k5eNaImIp3nP	škodit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
americký	americký	k2eAgInSc1d1	americký
odvolací	odvolací	k2eAgInSc1d1	odvolací
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
že	že	k8xS	že
EPA	EPA	kA	EPA
překročila	překročit	k5eAaPmAgFnS	překročit
své	svůj	k3xOyFgFnPc4	svůj
pravomoci	pravomoc	k1gFnPc4	pravomoc
a	a	k8xC	a
MMT	MMT	kA	MMT
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
USA	USA	kA	USA
legálním	legální	k2eAgNnSc7d1	legální
aditivem	aditivum	k1gNnSc7	aditivum
<g/>
.	.	kIx.	.
</s>
<s>
MMT	MMT	kA	MMT
nyní	nyní	k6eAd1	nyní
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
divize	divize	k1gFnSc1	divize
Afton	Aftona	k1gFnPc2	Aftona
Chemical	Chemical	k1gFnSc1	Chemical
Corporation	Corporation	k1gInSc1	Corporation
společnosti	společnost	k1gFnSc2	společnost
Newmarket	Newmarketa	k1gFnPc2	Newmarketa
Corporation	Corporation	k1gInSc1	Corporation
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ethanol	ethanol	k1gInSc4	ethanol
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
ethanol	ethanol	k1gInSc1	ethanol
přidává	přidávat	k5eAaImIp3nS	přidávat
do	do	k7c2	do
benzinu	benzin	k1gInSc2	benzin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
prodává	prodávat	k5eAaImIp3nS	prodávat
bez	bez	k7c2	bez
informace	informace	k1gFnSc2	informace
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ethanol	ethanol	k1gInSc4	ethanol
jeho	jeho	k3xOp3gFnPc2	jeho
součástí	součást	k1gFnPc2	součást
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
ethanol	ethanol	k1gInSc1	ethanol
přidává	přidávat	k5eAaImIp3nS	přidávat
povinně	povinně	k6eAd1	povinně
minimálně	minimálně	k6eAd1	minimálně
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
aktuálně	aktuálně	k6eAd1	aktuálně
5,9	[number]	k4	5,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
čerpacích	čerpací	k2eAgFnPc2d1	čerpací
stanic	stanice	k1gFnPc2	stanice
má	mít	k5eAaImIp3nS	mít
nápis	nápis	k1gInSc1	nápis
<g/>
,	,	kIx,	,
že	že	k8xS	že
palivo	palivo	k1gNnSc1	palivo
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
až	až	k9	až
10	[number]	k4	10
%	%	kIx~	%
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
záměrně	záměrně	k6eAd1	záměrně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
při	při	k7c6	při
případné	případný	k2eAgFnSc6d1	případná
pozdější	pozdní	k2eAgFnSc6d2	pozdější
změně	změna	k1gFnSc6	změna
nemuselo	muset	k5eNaImAgNnS	muset
označení	označení	k1gNnSc1	označení
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
EU	EU	kA	EU
lze	lze	k6eAd1	lze
podle	podle	k7c2	podle
specifikace	specifikace	k1gFnSc2	specifikace
běžného	běžný	k2eAgInSc2d1	běžný
benzinu	benzin	k1gInSc2	benzin
přidávat	přidávat	k5eAaImF	přidávat
5	[number]	k4	5
%	%	kIx~	%
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
.	.	kIx.	.
</s>
<s>
Diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
se	se	k3xPyFc4	se
o	o	k7c6	o
možnosti	možnost	k1gFnSc6	možnost
přidávat	přidávat	k5eAaImF	přidávat
až	až	k9	až
10	[number]	k4	10
%	%	kIx~	%
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
benzinu	benzin	k1gInSc2	benzin
prodávaného	prodávaný	k2eAgInSc2d1	prodávaný
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
5	[number]	k4	5
-	-	kIx~	-
15	[number]	k4	15
%	%	kIx~	%
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
,	,	kIx,	,
prodává	prodávat	k5eAaImIp3nS	prodávat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
směs	směs	k1gFnSc1	směs
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
85	[number]	k4	85
<g/>
)	)	kIx)	)
obsahující	obsahující	k2eAgFnSc1d1	obsahující
85	[number]	k4	85
%	%	kIx~	%
ethanolu	ethanol	k1gInSc2	ethanol
a	a	k8xC	a
15	[number]	k4	15
%	%	kIx~	%
benzinu	benzin	k1gInSc2	benzin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
povinně	povinně	k6eAd1	povinně
přidává	přidávat	k5eAaImIp3nS	přidávat
do	do	k7c2	do
benzinu	benzin	k1gInSc2	benzin
nejméně	málo	k6eAd3	málo
4,1	[number]	k4	4,1
%	%	kIx~	%
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Brazílie	Brazílie	k1gFnSc2	Brazílie
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
Brazilská	brazilský	k2eAgFnSc1d1	brazilská
národní	národní	k2eAgFnSc1d1	národní
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
benzin	benzin	k1gInSc4	benzin
<g/>
,	,	kIx,	,
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
a	a	k8xC	a
biopaliva	biopaliva	k1gFnSc1	biopaliva
(	(	kIx(	(
<g/>
ANP	ANP	kA	ANP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
do	do	k7c2	do
benzinu	benzin	k1gInSc2	benzin
pro	pro	k7c4	pro
automobilové	automobilový	k2eAgNnSc4d1	automobilové
použití	použití	k1gNnSc4	použití
přidávalo	přidávat	k5eAaImAgNnS	přidávat
25	[number]	k4	25
%	%	kIx~	%
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Austrálie	Austrálie	k1gFnSc2	Austrálie
====	====	k?	====
</s>
</p>
<p>
<s>
Legislativa	legislativa	k1gFnSc1	legislativa
omezuje	omezovat	k5eAaImIp3nS	omezovat
použití	použití	k1gNnSc4	použití
ethanolu	ethanol	k1gInSc2	ethanol
v	v	k7c6	v
benzinu	benzin	k1gInSc6	benzin
na	na	k7c4	na
obsah	obsah	k1gInSc4	obsah
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
u	u	k7c2	u
hlavních	hlavní	k2eAgMnPc2d1	hlavní
producentů	producent	k1gMnPc2	producent
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc1	označení
E	E	kA	E
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
směs	směs	k1gFnSc1	směs
je	být	k5eAaImIp3nS	být
levnější	levný	k2eAgInSc4d2	levnější
než	než	k8xS	než
obyčejný	obyčejný	k2eAgInSc4d1	obyčejný
bezolovnatý	bezolovnatý	k2eAgInSc4d1	bezolovnatý
benzin	benzin	k1gInSc4	benzin
<g/>
.	.	kIx.	.
</s>
<s>
Tankovací	tankovací	k2eAgInSc1d1	tankovací
stojan	stojan	k1gInSc1	stojan
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
směsí	směs	k1gFnSc7	směs
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
náležitě	náležitě	k6eAd1	náležitě
označen	označit	k5eAaPmNgInS	označit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Barviva	barvivo	k1gNnSc2	barvivo
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
nejpoužívanější	používaný	k2eAgInSc1d3	nejpoužívanější
letecký	letecký	k2eAgInSc1d1	letecký
benzin	benzin	k1gInSc1	benzin
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
jako	jako	k9	jako
100LL	[number]	k4	100LL
(	(	kIx(	(
<g/>
oktanové	oktanový	k2eAgNnSc1d1	oktanové
číslo	číslo	k1gNnSc1	číslo
100	[number]	k4	100
<g/>
,	,	kIx,	,
nízký	nízký	k2eAgInSc1d1	nízký
obsah	obsah	k1gInSc1	obsah
olova	olovo	k1gNnSc2	olovo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
barví	barvit	k5eAaImIp3nS	barvit
modře	modro	k6eAd1	modro
<g/>
.	.	kIx.	.
</s>
<s>
Červené	Červené	k2eAgNnSc1d1	Červené
barvivo	barvivo	k1gNnSc1	barvivo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
označování	označování	k1gNnSc4	označování
nezdaněného	zdaněný	k2eNgInSc2d1	nezdaněný
benzinu	benzin	k1gInSc2	benzin
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
červené	červený	k2eAgNnSc1d1	červené
barvivo	barvivo	k1gNnSc1	barvivo
pro	pro	k7c4	pro
odlišení	odlišení	k1gNnSc4	odlišení
běžné	běžný	k2eAgFnSc2d1	běžná
motorové	motorový	k2eAgFnSc2d1	motorová
nafty	nafta	k1gFnSc2	nafta
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
označované	označovaný	k2eAgFnSc2d1	označovaná
jako	jako	k8xC	jako
DERV	DERV	kA	DERV
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
znamenající	znamenající	k2eAgNnSc4d1	znamenající
silniční	silniční	k2eAgNnSc4d1	silniční
vozidlo	vozidlo	k1gNnSc4	vozidlo
se	s	k7c7	s
vznětovým	vznětový	k2eAgInSc7d1	vznětový
motorem	motor	k1gInSc7	motor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
není	být	k5eNaImIp3nS	být
obarvena	obarvit	k5eAaPmNgFnS	obarvit
<g/>
,	,	kIx,	,
od	od	k7c2	od
nafty	nafta	k1gFnSc2	nafta
pro	pro	k7c4	pro
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
a	a	k8xC	a
stavební	stavební	k2eAgInPc4d1	stavební
stroje	stroj	k1gInPc4	stroj
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
bagry	bagr	k1gInPc1	bagr
a	a	k8xC	a
buldozery	buldozer	k1gInPc1	buldozer
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
nafta	nafta	k1gFnSc1	nafta
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
občas	občas	k6eAd1	občas
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
těžkotonážní	těžkotonážní	k2eAgFnSc6d1	těžkotonážní
(	(	kIx(	(
<g/>
kamionové	kamionový	k2eAgFnSc6d1	kamionová
<g/>
)	)	kIx)	)
dopravě	doprava	k1gFnSc6	doprava
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
pro	pro	k7c4	pro
samostatné	samostatný	k2eAgInPc4d1	samostatný
motory	motor	k1gInPc4	motor
pohánějící	pohánějící	k2eAgInSc4d1	pohánějící
nakládací	nakládací	k2eAgInSc4d1	nakládací
jeřáb	jeřáb	k1gInSc4	jeřáb
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
však	však	k9	však
postupně	postupně	k6eAd1	postupně
mizí	mizet	k5eAaImIp3nS	mizet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mnoho	mnoho	k4c4	mnoho
takových	takový	k3xDgInPc2	takový
jeřábů	jeřáb	k1gInPc2	jeřáb
je	být	k5eAaImIp3nS	být
poháněno	pohánět	k5eAaImNgNnS	pohánět
již	již	k6eAd1	již
přímo	přímo	k6eAd1	přímo
od	od	k7c2	od
hlavního	hlavní	k2eAgInSc2d1	hlavní
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
běžná	běžný	k2eAgNnPc4d1	běžné
olovnatá	olovnatý	k2eAgNnPc4d1	olovnaté
paliva	palivo	k1gNnPc4	palivo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
benzin	benzin	k1gInSc1	benzin
barví	barvit	k5eAaImIp3nS	barvit
červeně	červeně	k6eAd1	červeně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
barvení	barvení	k1gNnSc1	barvení
"	"	kIx"	"
<g/>
minerálních	minerální	k2eAgInPc2d1	minerální
olejů	olej	k1gInPc2	olej
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
mezi	mezi	k7c4	mezi
které	který	k3yIgInPc4	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
široká	široký	k2eAgFnSc1d1	široká
škála	škála	k1gFnSc1	škála
kapalných	kapalný	k2eAgInPc2d1	kapalný
a	a	k8xC	a
plynných	plynný	k2eAgInPc2d1	plynný
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
směsí	směs	k1gFnPc2	směs
<g/>
)	)	kIx)	)
nařízeno	nařídit	k5eAaPmNgNnS	nařídit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
353	[number]	k4	353
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
spotřebních	spotřební	k2eAgFnPc6d1	spotřební
daních	daň	k1gFnPc6	daň
<g/>
,	,	kIx,	,
konkrétní	konkrétní	k2eAgFnPc1d1	konkrétní
podmínky	podmínka	k1gFnPc1	podmínka
potom	potom	k8xC	potom
definuje	definovat	k5eAaBmIp3nS	definovat
vyhláška	vyhláška	k1gFnSc1	vyhláška
č.	č.	k?	č.
61	[number]	k4	61
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Paliva	palivo	k1gNnPc1	palivo
se	se	k3xPyFc4	se
barví	barvit	k5eAaImIp3nP	barvit
červeně	červeně	k6eAd1	červeně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
konkrétně	konkrétně	k6eAd1	konkrétně
barvivem	barvivo	k1gNnSc7	barvivo
Solvent	Solvent	k1gMnSc1	Solvent
Red	Red	k1gMnSc1	Red
19	[number]	k4	19
(	(	kIx(	(
<g/>
též	též	k9	též
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
červeň	červeň	k1gFnSc1	červeň
19	[number]	k4	19
nebo	nebo	k8xC	nebo
Súdánská	súdánský	k2eAgFnSc1d1	súdánská
červeň	červeň	k1gFnSc1	červeň
7	[number]	k4	7
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
značkování	značkování	k1gNnSc1	značkování
barvivem	barvivo	k1gNnSc7	barvivo
Solvent	Solvent	k1gMnSc1	Solvent
Yellow	Yellow	k1gMnSc1	Yellow
124	[number]	k4	124
(	(	kIx(	(
<g/>
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
žluť	žluť	k1gFnSc1	žluť
124	[number]	k4	124
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Barví	barvit	k5eAaImIp3nP	barvit
a	a	k8xC	a
značkují	značkovat	k5eAaImIp3nP	značkovat
se	se	k3xPyFc4	se
střední	střední	k2eAgInPc1d1	střední
a	a	k8xC	a
těžké	těžký	k2eAgInPc1d1	těžký
plynové	plynový	k2eAgInPc1d1	plynový
oleje	olej	k1gInPc1	olej
KN	KN	kA	KN
2710	[number]	k4	2710
19	[number]	k4	19
25	[number]	k4	25
<g/>
,	,	kIx,	,
2710	[number]	k4	2710
19	[number]	k4	19
29	[number]	k4	29
<g/>
,	,	kIx,	,
2710	[number]	k4	2710
19	[number]	k4	19
41	[number]	k4	41
<g/>
,	,	kIx,	,
2710	[number]	k4	2710
19	[number]	k4	19
45	[number]	k4	45
a	a	k8xC	a
2710	[number]	k4	2710
19	[number]	k4	19
49	[number]	k4	49
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
daňové	daňový	k2eAgInPc1d1	daňový
zvýhodněny	zvýhodněn	k2eAgInPc1d1	zvýhodněn
(	(	kIx(	(
<g/>
osvobození	osvobození	k1gNnSc4	osvobození
od	od	k7c2	od
spotřební	spotřební	k2eAgFnSc2d1	spotřební
daně	daň	k1gFnSc2	daň
nebo	nebo	k8xC	nebo
vrácení	vrácení	k1gNnSc2	vrácení
daně	daň	k1gFnSc2	daň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Okysličovadla	okysličovadlo	k1gNnSc2	okysličovadlo
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
benzinu	benzin	k1gInSc2	benzin
lze	lze	k6eAd1	lze
přidávat	přidávat	k5eAaImF	přidávat
okysličovadla	okysličovadlo	k1gNnPc4	okysličovadlo
<g/>
,	,	kIx,	,
látky	látka	k1gFnPc4	látka
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
MTBE	MTBE	kA	MTBE
<g/>
,	,	kIx,	,
ETBE	ETBE	kA	ETBE
nebo	nebo	k8xC	nebo
ethanol	ethanol	k1gInSc1	ethanol
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
snižovat	snižovat	k5eAaImF	snižovat
množství	množství	k1gNnSc4	množství
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgNnSc2d1	uhelnatý
a	a	k8xC	a
nespáleného	spálený	k2eNgNnSc2d1	nespálené
paliva	palivo	k1gNnSc2	palivo
ve	v	k7c6	v
výfukových	výfukový	k2eAgInPc6d1	výfukový
plynech	plyn	k1gInPc6	plyn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
povinné	povinný	k2eAgNnSc1d1	povinné
používání	používání	k1gNnSc1	používání
takových	takový	k3xDgFnPc2	takový
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
snížit	snížit	k5eAaPmF	snížit
množství	množství	k1gNnSc4	množství
znečišťujících	znečišťující	k2eAgFnPc2d1	znečišťující
látek	látka	k1gFnPc2	látka
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
musí	muset	k5eAaImIp3nS	muset
palivo	palivo	k1gNnSc4	palivo
obsahovat	obsahovat	k5eAaImF	obsahovat
2	[number]	k4	2
%	%	kIx~	%
hmotnostní	hmotnostní	k2eAgFnSc2d1	hmotnostní
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
5,6	[number]	k4	5,6
%	%	kIx~	%
ethanolu	ethanol	k1gInSc2	ethanol
v	v	k7c6	v
benzinu	benzin	k1gInSc6	benzin
<g/>
.	.	kIx.	.
</s>
<s>
Výsledné	výsledný	k2eAgNnSc1d1	výsledné
palivo	palivo	k1gNnSc1	palivo
se	se	k3xPyFc4	se
často	často	k6eAd1	často
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
reformulovaný	reformulovaný	k2eAgInSc1d1	reformulovaný
benzin	benzin	k1gInSc1	benzin
(	(	kIx(	(
<g/>
RFG	RFG	kA	RFG
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
okysličený	okysličený	k2eAgInSc1d1	okysličený
benzin	benzin	k1gInSc1	benzin
<g/>
.	.	kIx.	.
</s>
<s>
Federální	federální	k2eAgInSc1d1	federální
požadavek	požadavek	k1gInSc1	požadavek
<g/>
,	,	kIx,	,
že	že	k8xS	že
RFG	RFG	kA	RFG
musí	muset	k5eAaImIp3nS	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
kyslík	kyslík	k1gInSc4	kyslík
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
reformulovaný	reformulovaný	k2eAgInSc1d1	reformulovaný
benzin	benzin	k1gInSc1	benzin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
snížený	snížený	k2eAgInSc4d1	snížený
obsah	obsah	k1gInSc4	obsah
těkavých	těkavý	k2eAgFnPc2d1	těkavá
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
dodatečné	dodatečný	k2eAgNnSc1d1	dodatečné
přidávání	přidávání	k1gNnSc1	přidávání
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
<g/>
Používání	používání	k1gNnSc1	používání
MTBE	MTBE	kA	MTBE
(	(	kIx(	(
<g/>
methyl-terc-butyletheru	methylercutylethrat	k5eAaPmIp1nS	methyl-terc-butylethrat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
zakazováno	zakazovat	k5eAaImNgNnS	zakazovat
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
kontaminaci	kontaminace	k1gFnSc6	kontaminace
podzemní	podzemní	k2eAgFnPc1d1	podzemní
vody	voda	k1gFnPc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Někde	někde	k6eAd1	někde
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
MTBE	MTBE	kA	MTBE
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Ethanol	ethanol	k1gInSc1	ethanol
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
ETBE	ETBE	kA	ETBE
odvozený	odvozený	k2eAgInSc4d1	odvozený
od	od	k7c2	od
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
běžné	běžný	k2eAgInPc1d1	běžný
substituty	substitut	k1gInPc1	substitut
MTBE	MTBE	kA	MTBE
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
používaný	používaný	k2eAgInSc1d1	používaný
ethanol	ethanol	k1gInSc1	ethanol
bývá	bývat	k5eAaImIp3nS	bývat
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
z	z	k7c2	z
biomasy	biomasa	k1gFnSc2	biomasa
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
z	z	k7c2	z
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
,	,	kIx,	,
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
nebo	nebo	k8xC	nebo
obilí	obilí	k1gNnSc2	obilí
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
bioethanol	bioethanol	k1gInSc1	bioethanol
<g/>
.	.	kIx.	.
</s>
<s>
Směs	směs	k1gFnSc1	směs
obsahující	obsahující	k2eAgFnSc1d1	obsahující
10	[number]	k4	10
%	%	kIx~	%
ethanolu	ethanol	k1gInSc2	ethanol
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
E	E	kA	E
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
směs	směs	k1gFnSc1	směs
s	s	k7c7	s
85	[number]	k4	85
%	%	kIx~	%
ethanolu	ethanol	k1gInSc2	ethanol
jako	jako	k8xS	jako
E	E	kA	E
<g/>
85	[number]	k4	85
<g/>
.	.	kIx.	.
</s>
<s>
Nejšířeji	Nejšíro	k6eAd2	Nejšíro
se	se	k3xPyFc4	se
ethanol	ethanol	k1gInSc1	ethanol
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
vyrobilo	vyrobit	k5eAaPmAgNnS	vyrobit
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
,	,	kIx,	,
13	[number]	k4	13
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
m	m	kA	m
<g/>
3	[number]	k4	3
ethanolu	ethanol	k1gInSc2	ethanol
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
do	do	k7c2	do
paliv	palivo	k1gNnPc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
E85	E85	k4	E85
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
stává	stávat	k5eAaImIp3nS	stávat
dostupný	dostupný	k2eAgInSc1d1	dostupný
po	po	k7c6	po
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
dosud	dosud	k6eAd1	dosud
relativně	relativně	k6eAd1	relativně
malého	malý	k2eAgInSc2d1	malý
počtu	počet	k1gInSc2	počet
stanic	stanice	k1gFnPc2	stanice
nejsou	být	k5eNaImIp3nP	být
otevřené	otevřený	k2eAgInPc1d1	otevřený
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
bioethanolu	bioethanol	k1gInSc2	bioethanol
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
již	již	k6eAd1	již
přímo	přímo	k6eAd1	přímo
nebo	nebo	k8xC	nebo
nepřímo	přímo	k6eNd1	přímo
konverzí	konverze	k1gFnSc7	konverze
na	na	k7c6	na
bio-ETBE	bio-ETBE	k?	bio-ETBE
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
doporučováno	doporučovat	k5eAaImNgNnS	doporučovat
Směrnicí	směrnice	k1gFnSc7	směrnice
2003	[number]	k4	2003
<g/>
/	/	kIx~	/
<g/>
30	[number]	k4	30
<g/>
/	/	kIx~	/
<g/>
EC	EC	kA	EC
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
Rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
využití	využití	k1gNnSc2	využití
biopaliv	biopalit	k5eAaPmDgInS	biopalit
nebo	nebo	k8xC	nebo
dalších	další	k2eAgFnPc2d1	další
obnovitelných	obnovitelný	k2eAgFnPc2d1	obnovitelná
pohonných	pohonný	k2eAgFnPc2d1	pohonná
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
výroba	výroba	k1gFnSc1	výroba
bioethanolu	bioethanol	k1gInSc2	bioethanol
ze	z	k7c2	z
zkvašených	zkvašený	k2eAgInPc2d1	zkvašený
cukrů	cukr	k1gInPc2	cukr
a	a	k8xC	a
škrobů	škrob	k1gInPc2	škrob
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
destilaci	destilace	k1gFnSc4	destilace
<g/>
,	,	kIx,	,
běžní	běžný	k2eAgMnPc1d1	běžný
obyvatelé	obyvatel	k1gMnPc1	obyvatel
většiny	většina	k1gFnSc2	většina
Evropy	Evropa	k1gFnSc2	Evropa
si	se	k3xPyFc3	se
nemohou	moct	k5eNaImIp3nP	moct
legálně	legálně	k6eAd1	legálně
vyrábět	vyrábět	k5eAaImF	vyrábět
vlastní	vlastní	k2eAgInSc4d1	vlastní
bioethanol	bioethanol	k1gInSc4	bioethanol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgNnPc1d1	další
aditiva	aditivum	k1gNnPc1	aditivum
===	===	k?	===
</s>
</p>
<p>
<s>
Benzin	benzin	k1gInSc1	benzin
prodávaný	prodávaný	k2eAgInSc1d1	prodávaný
u	u	k7c2	u
čerpacích	čerpací	k2eAgFnPc2d1	čerpací
stanic	stanice	k1gFnPc2	stanice
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
další	další	k2eAgNnPc4d1	další
aditiva	aditivum	k1gNnPc4	aditivum
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
omezit	omezit	k5eAaPmF	omezit
usazování	usazování	k1gNnSc1	usazování
karbonu	karbon	k1gInSc2	karbon
v	v	k7c6	v
motoru	motor	k1gInSc6	motor
<g/>
,	,	kIx,	,
zlepšit	zlepšit	k5eAaPmF	zlepšit
spalování	spalování	k1gNnSc4	spalování
a	a	k8xC	a
usnadnit	usnadnit	k5eAaPmF	usnadnit
startování	startování	k1gNnSc4	startování
v	v	k7c6	v
chladném	chladný	k2eAgNnSc6d1	chladné
počasí	počasí	k1gNnSc6	počasí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Působení	působení	k1gNnPc4	působení
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
==	==	k?	==
</s>
</p>
<p>
<s>
Mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
nealifatických	alifatický	k2eNgInPc2d1	alifatický
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
přirozeně	přirozeně	k6eAd1	přirozeně
obsažených	obsažený	k2eAgInPc2d1	obsažený
v	v	k7c6	v
benzinu	benzin	k1gInSc6	benzin
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
aromatických	aromatický	k2eAgInPc2d1	aromatický
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
benzen	benzen	k1gInSc1	benzen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
antidetonačních	antidetonační	k2eAgNnPc2d1	antidetonační
aditiv	aditivum	k1gNnPc2	aditivum
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
karcinogenních	karcinogenní	k2eAgFnPc2d1	karcinogenní
<g/>
.	.	kIx.	.
</s>
<s>
IARC	IARC	kA	IARC
klasifikuje	klasifikovat	k5eAaImIp3nS	klasifikovat
benzin	benzin	k1gInSc1	benzin
jako	jako	k8xC	jako
možný	možný	k2eAgInSc1d1	možný
karcinogen	karcinogen	k1gInSc1	karcinogen
(	(	kIx(	(
<g/>
skupina	skupina	k1gFnSc1	skupina
2	[number]	k4	2
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
nebo	nebo	k8xC	nebo
dlouhodobé	dlouhodobý	k2eAgInPc4d1	dlouhodobý
úniky	únik	k1gInPc4	únik
benzinu	benzin	k1gInSc2	benzin
představují	představovat	k5eAaImIp3nP	představovat
hrozbu	hrozba	k1gFnSc4	hrozba
pro	pro	k7c4	pro
zdraví	zdraví	k1gNnSc4	zdraví
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
benzin	benzin	k1gInSc1	benzin
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
veřejného	veřejný	k2eAgInSc2d1	veřejný
zdroje	zdroj	k1gInSc2	zdroj
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
nespočívá	spočívat	k5eNaImIp3nS	spočívat
v	v	k7c6	v
úniku	únik	k1gInSc6	únik
z	z	k7c2	z
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
v	v	k7c6	v
nehodách	nehoda	k1gFnPc6	nehoda
přepravních	přepravní	k2eAgFnPc2d1	přepravní
cisteren	cisterna	k1gFnPc2	cisterna
a	a	k8xC	a
v	v	k7c6	v
úniku	únik	k1gInSc6	únik
ze	z	k7c2	z
zásobních	zásobní	k2eAgFnPc2d1	zásobní
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
(	(	kIx(	(
<g/>
podzemních	podzemní	k2eAgFnPc2d1	podzemní
<g/>
)	)	kIx)	)
nádrží	nádrž	k1gFnPc2	nádrž
nyní	nyní	k6eAd1	nyní
podrobena	podrobit	k5eAaPmNgFnS	podrobit
přísným	přísný	k2eAgInPc3d1	přísný
předpisům	předpis	k1gInPc3	předpis
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
detekovat	detekovat	k5eAaImF	detekovat
případné	případný	k2eAgInPc4d1	případný
úniky	únik	k1gInPc4	únik
a	a	k8xC	a
předcházet	předcházet	k5eAaImF	předcházet
jim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
takových	takový	k3xDgNnPc2	takový
opatření	opatření	k1gNnPc2	opatření
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
použití	použití	k1gNnSc1	použití
obětované	obětovaný	k2eAgFnSc2d1	obětovaná
anody	anoda	k1gFnSc2	anoda
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
těkavosti	těkavost	k1gFnSc3	těkavost
benzinu	benzin	k1gInSc2	benzin
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
zásobní	zásobní	k2eAgFnPc1d1	zásobní
nádrže	nádrž	k1gFnPc1	nádrž
i	i	k8xC	i
nádrže	nádrž	k1gFnPc1	nádrž
ve	v	k7c6	v
vozidlech	vozidlo	k1gNnPc6	vozidlo
dobře	dobře	k6eAd1	dobře
utěsněny	utěsnit	k5eAaPmNgInP	utěsnit
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
těkavost	těkavost	k1gFnSc1	těkavost
také	také	k9	také
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
benzin	benzin	k1gInSc1	benzin
může	moct	k5eAaImIp3nS	moct
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
například	například	k6eAd1	například
od	od	k7c2	od
nafty	nafta	k1gFnSc2	nafta
<g/>
,	,	kIx,	,
v	v	k7c6	v
horkém	horký	k2eAgNnSc6d1	horké
počasí	počasí	k1gNnSc6	počasí
snadno	snadno	k6eAd1	snadno
vznítit	vznítit	k5eAaPmF	vznítit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
tlak	tlak	k1gInSc1	tlak
přibližně	přibližně	k6eAd1	přibližně
stejný	stejný	k2eAgInSc1d1	stejný
uvnitř	uvnitř	k6eAd1	uvnitř
i	i	k9	i
venku	venku	k6eAd1	venku
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vhodné	vhodný	k2eAgNnSc1d1	vhodné
řešení	řešení	k1gNnSc1	řešení
ventilace	ventilace	k1gFnSc2	ventilace
<g/>
.	.	kIx.	.
</s>
<s>
Benzin	benzin	k1gInSc1	benzin
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
nebezpečně	bezpečně	k6eNd1	bezpečně
reagovat	reagovat	k5eAaBmF	reagovat
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
běžných	běžný	k2eAgFnPc2d1	běžná
chemikálií	chemikálie	k1gFnPc2	chemikálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Benzin	benzin	k1gInSc1	benzin
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
zdrojů	zdroj	k1gInPc2	zdroj
znečišťujících	znečišťující	k2eAgInPc2d1	znečišťující
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
benzin	benzin	k1gInSc1	benzin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
olovo	olovo	k1gNnSc4	olovo
ani	ani	k8xC	ani
síru	síra	k1gFnSc4	síra
<g/>
,	,	kIx,	,
produkuje	produkovat	k5eAaImIp3nS	produkovat
do	do	k7c2	do
výfukových	výfukový	k2eAgInPc2d1	výfukový
plynů	plyn	k1gInPc2	plyn
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
,	,	kIx,	,
oxidy	oxid	k1gInPc1	oxid
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
oxid	oxid	k1gInSc1	oxid
uhelnatý	uhelnatý	k2eAgInSc1d1	uhelnatý
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
nespálený	spálený	k2eNgInSc1d1	nespálený
benzin	benzin	k1gInSc1	benzin
ve	v	k7c6	v
výfukových	výfukový	k2eAgInPc6d1	výfukový
plynech	plyn	k1gInPc6	plyn
a	a	k8xC	a
benzin	benzin	k1gInSc1	benzin
odpařený	odpařený	k2eAgInSc1d1	odpařený
z	z	k7c2	z
nádrží	nádrž	k1gFnPc2	nádrž
vede	vést	k5eAaImIp3nS	vést
na	na	k7c6	na
slunečním	sluneční	k2eAgNnSc6d1	sluneční
světle	světlo	k1gNnSc6	světlo
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
fotochemického	fotochemický	k2eAgInSc2d1	fotochemický
smogu	smog	k1gInSc2	smog
<g/>
.	.	kIx.	.
</s>
<s>
Přídavek	přídavek	k1gInSc1	přídavek
ethanolu	ethanol	k1gInSc2	ethanol
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
těkavost	těkavost	k1gFnSc1	těkavost
benzinu	benzin	k1gInSc2	benzin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zneužívání	zneužívání	k1gNnSc1	zneužívání
benzinu	benzin	k1gInSc2	benzin
jako	jako	k8xS	jako
inhalační	inhalační	k2eAgFnSc2d1	inhalační
drogy	droga	k1gFnSc2	droga
může	moct	k5eAaImIp3nS	moct
poškodit	poškodit	k5eAaPmF	poškodit
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
cítí	cítit	k5eAaImIp3nS	cítit
již	již	k6eAd1	již
koncentraci	koncentrace	k1gFnSc3	koncentrace
0,25	[number]	k4	0,25
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
0,000	[number]	k4	0,000
025	[number]	k4	025
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čichání	čichání	k1gNnSc1	čichání
benzinu	benzin	k1gInSc2	benzin
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
lidí	člověk	k1gMnPc2	člověk
běžným	běžný	k2eAgInPc3d1	běžný
způsob	způsob	k1gInSc4	způsob
k	k	k7c3	k
navození	navození	k1gNnSc3	navození
stavu	stav	k1gInSc2	stav
omámení	omámení	k1gNnSc2	omámení
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
chudších	chudý	k2eAgFnPc6d2	chudší
komunitách	komunita	k1gFnPc6	komunita
a	a	k8xC	a
domorodých	domorodý	k2eAgFnPc6d1	domorodá
skupinách	skupina	k1gFnPc6	skupina
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
a	a	k8xC	a
některých	některý	k3yIgInPc6	některý
tichomořských	tichomořský	k2eAgInPc6d1	tichomořský
ostrovech	ostrov	k1gInPc6	ostrov
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
přímo	přímo	k6eAd1	přímo
epidemií	epidemie	k1gFnPc2	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
australské	australský	k2eAgFnSc6d1	australská
rafinerii	rafinerie	k1gFnSc6	rafinerie
firmy	firma	k1gFnSc2	firma
BP	BP	kA	BP
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Kwinana	Kwinan	k1gMnSc2	Kwinan
vyvinut	vyvinut	k2eAgInSc4d1	vyvinut
benzin	benzin	k1gInSc4	benzin
Opal	opal	k1gInSc1	opal
obsahující	obsahující	k2eAgInSc1d1	obsahující
jen	jen	k9	jen
5	[number]	k4	5
%	%	kIx~	%
aromatických	aromatický	k2eAgInPc2d1	aromatický
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
(	(	kIx(	(
<g/>
namísto	namísto	k7c2	namísto
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
25	[number]	k4	25
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
omezuje	omezovat	k5eAaImIp3nS	omezovat
účinky	účinek	k1gInPc4	účinek
inhalace	inhalace	k1gFnSc2	inhalace
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xC	jako
jiné	jiný	k2eAgFnPc1d1	jiná
alkany	alkana	k1gFnPc1	alkana
<g/>
,	,	kIx,	,
benzin	benzin	k1gInSc1	benzin
hoří	hořet	k5eAaImIp3nS	hořet
v	v	k7c6	v
omezeném	omezený	k2eAgInSc6d1	omezený
rozsahu	rozsah	k1gInSc6	rozsah
své	svůj	k3xOyFgFnSc2	svůj
plynné	plynný	k2eAgFnSc2d1	plynná
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
společně	společně	k6eAd1	společně
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
těkavostí	těkavost	k1gFnSc7	těkavost
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
zdrojů	zdroj	k1gInPc2	zdroj
zapálení	zapálený	k2eAgMnPc1d1	zapálený
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1	dolní
mez	mez	k1gFnSc1	mez
výbušnosti	výbušnost	k1gFnSc2	výbušnost
je	být	k5eAaImIp3nS	být
1,4	[number]	k4	1,4
%	%	kIx~	%
objemových	objemový	k2eAgFnPc2d1	objemová
<g/>
,	,	kIx,	,
horní	horní	k2eAgInSc4d1	horní
limit	limit	k1gInSc4	limit
7,6	[number]	k4	7,6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnSc1	koncentrace
pod	pod	k7c7	pod
1,4	[number]	k4	1,4
%	%	kIx~	%
par	para	k1gFnPc2	para
benzinu	benzin	k1gInSc2	benzin
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
chudá	chudý	k2eAgFnSc1d1	chudá
a	a	k8xC	a
nezapálí	zapálit	k5eNaPmIp3nS	zapálit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
koncentrace	koncentrace	k1gFnSc1	koncentrace
nad	nad	k7c7	nad
7,6	[number]	k4	7,6
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
bohatá	bohatý	k2eAgFnSc1d1	bohatá
a	a	k8xC	a
též	též	k9	též
se	se	k3xPyFc4	se
nezapálí	zapálit	k5eNaPmIp3nS	zapálit
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
páry	pár	k1gInPc1	pár
benzinu	benzin	k1gInSc2	benzin
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
mísí	mísit	k5eAaImIp3nP	mísit
se	s	k7c7	s
vzduchem	vzduch	k1gInSc7	vzduch
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozptylují	rozptylovat	k5eAaImIp3nP	rozptylovat
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
mnoha	mnoho	k4c2	mnoho
nehodám	nehoda	k1gFnPc3	nehoda
s	s	k7c7	s
benzinem	benzin	k1gInSc7	benzin
došlo	dojít	k5eAaPmAgNnS	dojít
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
zapálit	zapálit	k5eAaPmF	zapálit
oheň	oheň	k1gInSc4	oheň
<g/>
;	;	kIx,	;
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
benzin	benzin	k1gInSc1	benzin
pomohl	pomoct	k5eAaPmAgInS	pomoct
zapálit	zapálit	k5eAaPmF	zapálit
příslušné	příslušný	k2eAgNnSc4d1	příslušné
palivo	palivo	k1gNnSc4	palivo
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
vypaří	vypařit	k5eAaPmIp3nP	vypařit
a	a	k8xC	a
smísí	smísit	k5eAaPmIp3nP	smísit
se	se	k3xPyFc4	se
se	s	k7c7	s
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
následné	následný	k2eAgFnSc6d1	následná
snaze	snaha	k1gFnSc6	snaha
zapálit	zapálit	k5eAaPmF	zapálit
oheň	oheň	k1gInSc4	oheň
se	se	k3xPyFc4	se
páry	pár	k1gInPc1	pár
prudce	prudko	k6eAd1	prudko
vznítí	vznítit	k5eAaPmIp3nP	vznítit
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
ohnivé	ohnivý	k2eAgFnSc2d1	ohnivá
koule	koule	k1gFnSc2	koule
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
neopatrného	opatrný	k2eNgMnSc4d1	neopatrný
člověka	člověk	k1gMnSc4	člověk
pohltí	pohltit	k5eAaPmIp3nS	pohltit
<g/>
.	.	kIx.	.
</s>
<s>
Páry	pár	k1gInPc1	pár
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
těžší	těžký	k2eAgNnSc4d2	těžší
než	než	k8xS	než
vzduch	vzduch	k1gInSc4	vzduch
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
se	se	k3xPyFc4	se
hromadit	hromadit	k5eAaImF	hromadit
například	například	k6eAd1	například
v	v	k7c6	v
montážních	montážní	k2eAgFnPc6d1	montážní
jámách	jáma	k1gFnPc6	jáma
v	v	k7c6	v
garážích	garáž	k1gFnPc6	garáž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stabilita	stabilita	k1gFnSc1	stabilita
==	==	k?	==
</s>
</p>
<p>
<s>
Kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
benzin	benzin	k1gInSc1	benzin
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
stabilní	stabilní	k2eAgFnSc4d1	stabilní
prakticky	prakticky	k6eAd1	prakticky
po	po	k7c4	po
libovolně	libovolně	k6eAd1	libovolně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
správně	správně	k6eAd1	správně
skladuje	skladovat	k5eAaImIp3nS	skladovat
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
skladování	skladování	k1gNnSc1	skladování
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
ve	v	k7c6	v
vzduchotěsné	vzduchotěsný	k2eAgFnSc6d1	vzduchotěsná
nádobě	nádoba	k1gFnSc6	nádoba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zamezilo	zamezit	k5eAaPmAgNnS	zamezit
oxidaci	oxidace	k1gFnSc4	oxidace
a	a	k8xC	a
pronikání	pronikání	k1gNnSc4	pronikání
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
<s>
Skladovat	skladovat	k5eAaImF	skladovat
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
při	při	k7c6	při
stabilní	stabilní	k2eAgFnSc6d1	stabilní
nízké	nízký	k2eAgFnSc6d1	nízká
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nemohlo	moct	k5eNaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
vlivem	vlivem	k7c2	vlivem
tepelné	tepelný	k2eAgFnSc2d1	tepelná
roztažnosti	roztažnost	k1gFnSc2	roztažnost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
benzin	benzin	k1gInSc1	benzin
neskladuje	skladovat	k5eNaImIp3nS	skladovat
správně	správně	k6eAd1	správně
<g/>
,	,	kIx,	,
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
může	moct	k5eAaImIp3nS	moct
gumovatět	gumovatět	k5eAaImF	gumovatět
a	a	k8xC	a
tvořit	tvořit	k5eAaImF	tvořit
sraženiny	sraženina	k1gFnPc4	sraženina
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
při	při	k7c6	při
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
odstavení	odstavení	k1gNnSc6	odstavení
vozidla	vozidlo	k1gNnSc2	vozidlo
docházet	docházet	k5eAaImF	docházet
v	v	k7c6	v
nádrži	nádrž	k1gFnSc6	nádrž
<g/>
,	,	kIx,	,
potrubí	potrubí	k1gNnSc6	potrubí
nebo	nebo	k8xC	nebo
v	v	k7c6	v
karburátoru	karburátor	k1gInSc6	karburátor
či	či	k8xC	či
součástech	součást	k1gFnPc6	součást
vstřikovacího	vstřikovací	k2eAgInSc2d1	vstřikovací
systému	systém	k1gInSc2	systém
<g/>
;	;	kIx,	;
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
problém	problém	k1gInSc1	problém
nastartovat	nastartovat	k5eAaPmF	nastartovat
motor	motor	k1gInSc4	motor
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
k	k	k7c3	k
běžnému	běžný	k2eAgInSc3d1	běžný
provozu	provoz	k1gInSc3	provoz
vozidla	vozidlo	k1gNnSc2	vozidlo
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgInP	mít
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
nánosy	nános	k1gInPc1	nános
nakonec	nakonec	k6eAd1	nakonec
vyčistit	vyčistit	k5eAaPmF	vyčistit
proudem	proud	k1gInSc7	proud
čerstvého	čerstvý	k2eAgInSc2d1	čerstvý
benzinu	benzin	k1gInSc2	benzin
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prodlužování	prodlužování	k1gNnSc3	prodlužování
životnosti	životnost	k1gFnSc2	životnost
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
není	být	k5eNaImIp3nS	být
nebo	nebo	k8xC	nebo
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
správně	správně	k6eAd1	správně
skladováno	skladován	k2eAgNnSc1d1	skladováno
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
používat	používat	k5eAaImF	používat
stabilizátory	stabilizátor	k1gInPc4	stabilizátor
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
startování	startování	k1gNnSc4	startování
používají	používat	k5eAaImIp3nP	používat
u	u	k7c2	u
malých	malý	k2eAgInPc2d1	malý
motorů	motor	k1gInPc2	motor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
travních	travní	k2eAgFnPc6d1	travní
sekačkách	sekačka	k1gFnPc6	sekačka
a	a	k8xC	a
traktorech	traktor	k1gInPc6	traktor
<g/>
.	.	kIx.	.
</s>
<s>
Doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
ponechávat	ponechávat	k5eAaImF	ponechávat
nádrže	nádrž	k1gFnPc1	nádrž
a	a	k8xC	a
kanystry	kanystr	k1gInPc1	kanystr
naplněné	naplněný	k2eAgInPc1d1	naplněný
více	hodně	k6eAd2	hodně
než	než	k8xS	než
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
uzavřené	uzavřený	k2eAgInPc4d1	uzavřený
proti	proti	k7c3	proti
kontaktu	kontakt	k1gInSc3	kontakt
se	s	k7c7	s
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
,	,	kIx,	,
chráněné	chráněný	k2eAgInPc1d1	chráněný
před	před	k7c7	před
vysokými	vysoký	k2eAgFnPc7d1	vysoká
teplotami	teplota	k1gFnPc7	teplota
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
před	před	k7c7	před
odstavením	odstavení	k1gNnSc7	odstavení
nechat	nechat	k5eAaPmF	nechat
běžet	běžet	k5eAaImF	běžet
motor	motor	k1gInSc4	motor
deset	deset	k4xCc1	deset
minut	minuta	k1gFnPc2	minuta
(	(	kIx(	(
<g/>
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
stabilizátor	stabilizátor	k1gMnSc1	stabilizátor
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
součástí	součást	k1gFnPc2	součást
palivového	palivový	k2eAgInSc2d1	palivový
systému	systém	k1gInSc2	systém
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
pravidelných	pravidelný	k2eAgInPc6d1	pravidelný
intervalech	interval	k1gInPc6	interval
motor	motor	k1gInSc1	motor
nastartovat	nastartovat	k5eAaPmF	nastartovat
(	(	kIx(	(
<g/>
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
odstranilo	odstranit	k5eAaPmAgNnS	odstranit
odstáté	odstátý	k2eAgNnSc1d1	odstáté
palivo	palivo	k1gNnSc1	palivo
z	z	k7c2	z
karburátoru	karburátor	k1gInSc2	karburátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Gumovité	gumovitý	k2eAgFnPc1d1	gumovitá
<g/>
,	,	kIx,	,
lepivé	lepivý	k2eAgFnPc1d1	lepivá
pryskyřičnaté	pryskyřičnatý	k2eAgFnPc1d1	pryskyřičnatá
usazeniny	usazenina	k1gFnPc1	usazenina
vznikají	vznikat	k5eAaImIp3nP	vznikat
oxidativní	oxidativní	k2eAgFnSc7d1	oxidativní
degradací	degradace	k1gFnSc7	degradace
benzinu	benzin	k1gInSc2	benzin
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
degradaci	degradace	k1gFnSc3	degradace
lze	lze	k6eAd1	lze
zabránit	zabránit	k5eAaPmF	zabránit
používáním	používání	k1gNnSc7	používání
antioxidantů	antioxidant	k1gInPc2	antioxidant
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
fenylendiaminů	fenylendiamin	k1gInPc2	fenylendiamin
<g/>
,	,	kIx,	,
alkylendiaminů	alkylendiamin	k1gInPc2	alkylendiamin
(	(	kIx(	(
<g/>
diethylentriaminu	diethylentriamin	k1gInSc2	diethylentriamin
<g/>
,	,	kIx,	,
triethylentetraminu	triethylentetramin	k1gInSc2	triethylentetramin
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
alkylaminů	alkylamin	k1gInPc2	alkylamin
(	(	kIx(	(
<g/>
diethylaminu	diethylamin	k1gInSc2	diethylamin
<g/>
,	,	kIx,	,
tributylaminu	tributylamin	k1gInSc2	tributylamin
<g/>
,	,	kIx,	,
ethylaminu	ethylamin	k1gInSc2	ethylamin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
užitečná	užitečný	k2eAgNnPc4d1	užitečné
aditiva	aditivum	k1gNnPc4	aditivum
patří	patřit	k5eAaImIp3nP	patřit
inhibitory	inhibitor	k1gInPc1	inhibitor
gumovatění	gumovatění	k1gNnSc1	gumovatění
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
N-substituované	Nubstituovaný	k2eAgInPc4d1	N-substituovaný
alkylaminofenoly	alkylaminofenol	k1gInPc4	alkylaminofenol
<g/>
,	,	kIx,	,
a	a	k8xC	a
stabilizátory	stabilizátor	k1gInPc1	stabilizátor
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
N-	N-	k1gFnSc1	N-
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
-aminethyl	minethyl	k1gInSc1	-aminethyl
<g/>
)	)	kIx)	)
<g/>
piperazin	piperazin	k1gInSc1	piperazin
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
,	,	kIx,	,
<g/>
N-diethylhydroxylamin	Niethylhydroxylamin	k2eAgMnSc1d1	N-diethylhydroxylamin
a	a	k8xC	a
triethylenetetramin	triethylenetetramin	k2eAgMnSc1d1	triethylenetetramin
<g/>
.	.	kIx.	.
<g/>
Zlepšení	zlepšení	k1gNnSc1	zlepšení
rafinačních	rafinační	k2eAgFnPc2d1	rafinační
technik	technika	k1gFnPc2	technika
obecně	obecně	k6eAd1	obecně
snížilo	snížit	k5eAaPmAgNnS	snížit
závislost	závislost	k1gFnSc4	závislost
na	na	k7c4	na
katalyticky	katalyticky	k6eAd1	katalyticky
nebo	nebo	k8xC	nebo
tepelně	tepelně	k6eAd1	tepelně
krakovaných	krakovaný	k2eAgFnPc6d1	krakovaný
látkách	látka	k1gFnPc6	látka
nejnáchylnějších	náchylný	k2eAgFnPc2d3	nejnáchylnější
na	na	k7c4	na
oxidaci	oxidace	k1gFnSc4	oxidace
<g/>
.	.	kIx.	.
</s>
<s>
Benzin	benzin	k1gInSc4	benzin
obsahující	obsahující	k2eAgInPc4d1	obsahující
kyselé	kyselý	k2eAgInPc4d1	kyselý
kontaminanty	kontaminant	k1gInPc4	kontaminant
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kyselinu	kyselina	k1gFnSc4	kyselina
naftenovou	naftenový	k2eAgFnSc4d1	naftenový
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ošetřit	ošetřit	k5eAaPmF	ošetřit
aditivy	aditivum	k1gNnPc7	aditivum
včetně	včetně	k7c2	včetně
silně	silně	k6eAd1	silně
zásaditých	zásaditý	k2eAgInPc2d1	zásaditý
organických	organický	k2eAgInPc2d1	organický
aminů	amin	k1gInPc2	amin
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
N	N	kA	N
<g/>
,	,	kIx,	,
<g/>
N-diethylhydroxylaminu	Niethylhydroxylamin	k1gInSc2	N-diethylhydroxylamin
<g/>
;	;	kIx,	;
chrání	chránit	k5eAaImIp3nS	chránit
se	se	k3xPyFc4	se
tak	tak	k9	tak
kovy	kov	k1gInPc4	kov
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
a	a	k8xC	a
antioxidační	antioxidační	k2eAgNnPc1d1	antioxidační
aditiva	aditivum	k1gNnPc1	aditivum
před	před	k7c7	před
rozkladem	rozklad	k1gInSc7	rozklad
působením	působení	k1gNnSc7	působení
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
s	s	k7c7	s
bromovým	bromový	k2eAgNnSc7d1	Bromové
číslem	číslo	k1gNnSc7	číslo
10	[number]	k4	10
a	a	k8xC	a
více	hodně	k6eAd2	hodně
lze	lze	k6eAd1	lze
chránit	chránit	k5eAaImF	chránit
kombinací	kombinace	k1gFnPc2	kombinace
neblokovaných	blokovaný	k2eNgNnPc2d1	neblokované
nebo	nebo	k8xC	nebo
částečně	částečně	k6eAd1	částečně
blokovaných	blokovaný	k2eAgInPc2d1	blokovaný
fenolů	fenol	k1gInPc2	fenol
a	a	k8xC	a
silných	silný	k2eAgFnPc2d1	silná
aminových	aminový	k2eAgFnPc2d1	aminová
zásad	zásada	k1gFnPc2	zásada
rozpustných	rozpustný	k2eAgFnPc2d1	rozpustná
v	v	k7c6	v
olejích	olej	k1gInPc6	olej
(	(	kIx(	(
<g/>
např.	např.	kA	např.
monoethanolamin	monoethanolamin	k1gInSc1	monoethanolamin
<g/>
,	,	kIx,	,
N-	N-	k1gFnSc1	N-
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
-aminoethyl	minoethyl	k1gInSc1	-aminoethyl
<g/>
)	)	kIx)	)
<g/>
piperazin	piperazin	k1gInSc1	piperazin
<g/>
,	,	kIx,	,
cyklohexylamin	cyklohexylamin	k1gInSc1	cyklohexylamin
<g/>
,	,	kIx,	,
1,3	[number]	k4	1,3
<g/>
-cyklohexan-bis	yklohexanis	k1gFnPc2	-cyklohexan-bis
<g/>
(	(	kIx(	(
<g/>
methylamin	methylamin	k1gInSc1	methylamin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2,5	[number]	k4	2,5
<g/>
-dimethylanilin	imethylanilina	k1gFnPc2	-dimethylanilina
<g/>
,	,	kIx,	,
2,6	[number]	k4	2,6
<g/>
-dimethylanilin	imethylanilin	k2eAgInSc1d1	-dimethylanilin
<g/>
,	,	kIx,	,
diethylenetriamin	diethylenetriamin	k2eAgInSc1d1	diethylenetriamin
nebo	nebo	k8xC	nebo
triethylenetetramin	triethylenetetramin	k2eAgInSc1d1	triethylenetetramin
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Odstátý	odstátý	k2eAgInSc1d1	odstátý
<g/>
"	"	kIx"	"
benzin	benzin	k1gInSc1	benzin
lze	lze	k6eAd1	lze
detekovat	detekovat	k5eAaImF	detekovat
kolorimetrickým	kolorimetrický	k2eAgInSc7d1	kolorimetrický
enzymatickým	enzymatický	k2eAgInSc7d1	enzymatický
testem	test	k1gInSc7	test
na	na	k7c4	na
organické	organický	k2eAgInPc4d1	organický
peroxidy	peroxid	k1gInPc4	peroxid
produkované	produkovaný	k2eAgInPc4d1	produkovaný
oxidací	oxidace	k1gFnSc7	oxidace
benzinu	benzin	k1gInSc2	benzin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jiná	jiný	k2eAgNnPc1d1	jiné
paliva	palivo	k1gNnPc1	palivo
==	==	k?	==
</s>
</p>
<p>
<s>
biopaliva	biopaliva	k1gFnSc1	biopaliva
</s>
</p>
<p>
<s>
bionafta	bionafta	k1gFnSc1	bionafta
pro	pro	k7c4	pro
vznětové	vznětový	k2eAgInPc4d1	vznětový
motory	motor	k1gInPc4	motor
</s>
</p>
<p>
<s>
rostlinný	rostlinný	k2eAgInSc1d1	rostlinný
olej	olej	k1gInSc1	olej
</s>
</p>
<p>
<s>
Fischer-Tropschova	Fischer-Tropschův	k2eAgFnSc1d1	Fischer-Tropschova
nafta	nafta	k1gFnSc1	nafta
z	z	k7c2	z
biomasy	biomasa	k1gFnSc2	biomasa
</s>
</p>
<p>
<s>
biobutanol	biobutanol	k1gInSc1	biobutanol
pro	pro	k7c4	pro
zážehové	zážehový	k2eAgInPc4d1	zážehový
motory	motor	k1gInPc4	motor
</s>
</p>
<p>
<s>
bioethanol	bioethanol	k1gInSc1	bioethanol
</s>
</p>
<p>
<s>
biobenzin	biobenzin	k1gMnSc1	biobenzin
</s>
</p>
<p>
<s>
dřevoplyn	dřevoplyn	k1gInSc1	dřevoplyn
</s>
</p>
<p>
<s>
stlačený	stlačený	k2eAgInSc1d1	stlačený
vzduch	vzduch	k1gInSc1	vzduch
</s>
</p>
<p>
<s>
vodík	vodík	k1gInSc1	vodík
</s>
</p>
<p>
<s>
elektřina	elektřina	k1gFnSc1	elektřina
</s>
</p>
<p>
<s>
fosilní	fosilní	k2eAgNnPc1d1	fosilní
paliva	palivo	k1gNnPc1	palivo
</s>
</p>
<p>
<s>
CNG	CNG	kA	CNG
(	(	kIx(	(
<g/>
Compressed	Compressed	k1gMnSc1	Compressed
Natural	Natural	k?	Natural
Gas	Gas	k1gMnSc1	Gas
<g/>
)	)	kIx)	)
-	-	kIx~	-
stlačený	stlačený	k2eAgInSc1d1	stlačený
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
</s>
</p>
<p>
<s>
LNG	LNG	kA	LNG
(	(	kIx(	(
<g/>
Liquefied	Liquefied	k1gMnSc1	Liquefied
Natural	Natural	k?	Natural
Gas	Gas	k1gMnSc1	Gas
<g/>
)	)	kIx)	)
-	-	kIx~	-
zkapalněný	zkapalněný	k2eAgInSc1d1	zkapalněný
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
</s>
</p>
<p>
<s>
LPG	LPG	kA	LPG
(	(	kIx(	(
<g/>
Liquefied	Liquefied	k1gInSc1	Liquefied
Petroleum	Petroleum	k1gNnSc1	Petroleum
Gas	Gas	k1gFnPc2	Gas
<g/>
)	)	kIx)	)
-	-	kIx~	-
zkapalněná	zkapalněný	k2eAgFnSc1d1	zkapalněná
propan-butanová	propanutanový	k2eAgFnSc1d1	propan-butanová
směs	směs	k1gFnSc1	směs
</s>
</p>
<p>
<s>
motorová	motorový	k2eAgFnSc1d1	motorová
nafta	nafta	k1gFnSc1	nafta
</s>
</p>
<p>
<s>
==	==	k?	==
Obchodní	obchodní	k2eAgFnPc1d1	obchodní
záležitosti	záležitost	k1gFnPc1	záležitost
==	==	k?	==
</s>
</p>
<p>
<s>
Ceny	cena	k1gFnPc1	cena
benzínu	benzín	k1gInSc2	benzín
jsou	být	k5eAaImIp3nP	být
určovány	určován	k2eAgInPc1d1	určován
výstupními	výstupní	k2eAgFnPc7d1	výstupní
cenami	cena	k1gFnPc7	cena
rafinérií	rafinérie	k1gFnPc2	rafinérie
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
si	se	k3xPyFc3	se
připočítávají	připočítávat	k5eAaImIp3nP	připočítávat
své	své	k1gNnSc4	své
marže	marže	k1gFnSc2	marže
distributoři	distributor	k1gMnPc1	distributor
a	a	k8xC	a
prodejci	prodejce	k1gMnPc1	prodejce
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
dohody	dohoda	k1gFnSc2	dohoda
prodávají	prodávat	k5eAaImIp3nP	prodávat
všechny	všechen	k3xTgFnPc4	všechen
rafinérie	rafinérie	k1gFnPc4	rafinérie
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
své	svůj	k3xOyFgInPc4	svůj
produkty	produkt	k1gInPc4	produkt
za	za	k7c4	za
stejnou	stejný	k2eAgFnSc4d1	stejná
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
stanovovanou	stanovovaný	k2eAgFnSc4d1	stanovovaná
na	na	k7c6	na
komoditní	komoditní	k2eAgFnSc6d1	komoditní
burze	burza	k1gFnSc6	burza
v	v	k7c6	v
Rotterdamu	Rotterdam	k1gInSc6	Rotterdam
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
tvoří	tvořit	k5eAaImIp3nP	tvořit
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
z	z	k7c2	z
konečné	konečný	k2eAgFnSc2d1	konečná
ceny	cena	k1gFnSc2	cena
benzínu	benzín	k1gInSc2	benzín
daně	daň	k1gFnSc2	daň
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
spotřební	spotřební	k2eAgFnSc1d1	spotřební
daň	daň	k1gFnSc1	daň
a	a	k8xC	a
daň	daň	k1gFnSc1	daň
z	z	k7c2	z
přidané	přidaný	k2eAgFnSc2d1	přidaná
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
průměrná	průměrný	k2eAgFnSc1d1	průměrná
marže	marže	k1gFnSc1	marže
pro	pro	k7c4	pro
provozovatele	provozovatel	k1gMnPc4	provozovatel
čerpací	čerpací	k2eAgFnSc2d1	čerpací
stanice	stanice	k1gFnSc2	stanice
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
2	[number]	k4	2
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
burze	burza	k1gFnSc6	burza
se	se	k3xPyFc4	se
benzín	benzín	k1gInSc1	benzín
jako	jako	k8xC	jako
komodita	komodita	k1gFnSc1	komodita
nabízí	nabízet	k5eAaImIp3nS	nabízet
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
benzín	benzín	k1gInSc1	benzín
RBOB	RBOB	kA	RBOB
(	(	kIx(	(
<g/>
Reformulated	Reformulated	k1gMnSc1	Reformulated
Gasoline	Gasolin	k1gInSc5	Gasolin
Blendstock	Blendstock	k1gInSc1	Blendstock
for	forum	k1gNnPc2	forum
Oxygen	oxygen	k1gInSc4	oxygen
Blending	Blending	k1gInSc1	Blending
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
standardní	standardní	k2eAgInSc4d1	standardní
velkoobchodně	velkoobchodně	k6eAd1	velkoobchodně
dodávaný	dodávaný	k2eAgInSc4d1	dodávaný
benzín	benzín	k1gInSc4	benzín
-	-	kIx~	-
neokysličený	okysličený	k2eNgMnSc1d1	okysličený
o	o	k7c4	o
92	[number]	k4	92
<g/>
%	%	kIx~	%
čistotě	čistota	k1gFnSc6	čistota
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
benzínu	benzín	k1gInSc2	benzín
jakožto	jakožto	k8xS	jakožto
globální	globální	k2eAgFnSc2d1	globální
komodity	komodita	k1gFnSc2	komodita
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
hned	hned	k6eAd1	hned
několika	několik	k4yIc7	několik
faktory	faktor	k1gInPc7	faktor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Trendy	trend	k1gInPc1	trend
v	v	k7c6	v
USA	USA	kA	USA
-	-	kIx~	-
jako	jako	k8xS	jako
největší	veliký	k2eAgMnSc1d3	veliký
konzument	konzument	k1gMnSc1	konzument
má	mít	k5eAaImIp3nS	mít
spotřeba	spotřeba	k1gFnSc1	spotřeba
v	v	k7c4	v
USA	USA	kA	USA
přímý	přímý	k2eAgInSc4d1	přímý
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
benzínu	benzín	k1gInSc2	benzín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alternativní	alternativní	k2eAgNnPc1d1	alternativní
paliva	palivo	k1gNnPc1	palivo
-	-	kIx~	-
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
a	a	k8xC	a
vývojem	vývoj	k1gInSc7	vývoj
alternativních	alternativní	k2eAgNnPc2d1	alternativní
paliv	palivo	k1gNnPc2	palivo
můžeme	moct	k5eAaImIp1nP	moct
očekávat	očekávat	k5eAaImF	očekávat
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
ceně	cena	k1gFnSc6	cena
benzínu	benzín	k1gInSc2	benzín
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
kvůli	kvůli	k7c3	kvůli
snížení	snížení	k1gNnSc3	snížení
poptávky	poptávka	k1gFnSc2	poptávka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cena	cena	k1gFnSc1	cena
ropy	ropa	k1gFnSc2	ropa
-	-	kIx~	-
cena	cena	k1gFnSc1	cena
benzinu	benzin	k1gInSc2	benzin
jako	jako	k8xS	jako
komodity	komodita	k1gFnSc2	komodita
je	být	k5eAaImIp3nS	být
těsně	těsně	k6eAd1	těsně
navázána	navázán	k2eAgFnSc1d1	navázána
na	na	k7c4	na
ropu	ropa	k1gFnSc4	ropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nestabilita	nestabilita	k1gFnSc1	nestabilita
politické	politický	k2eAgFnSc2d1	politická
moci	moc	k1gFnSc2	moc
-	-	kIx~	-
ropa	ropa	k1gFnSc1	ropa
i	i	k8xC	i
benzín	benzín	k1gInSc1	benzín
putují	putovat	k5eAaImIp3nP	putovat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
rozvíjejících	rozvíjející	k2eAgInPc2d1	rozvíjející
se	se	k3xPyFc4	se
trhů	trh	k1gInPc2	trh
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
nestabilní	stabilní	k2eNgFnSc7d1	nestabilní
politickou	politický	k2eAgFnSc7d1	politická
mocí	moc	k1gFnSc7	moc
<g/>
.	.	kIx.	.
</s>
<s>
Napětí	napětí	k1gNnSc1	napětí
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
výkyvy	výkyv	k1gInPc4	výkyv
v	v	k7c6	v
cenách	cena	k1gFnPc6	cena
ropy	ropa	k1gFnSc2	ropa
i	i	k8xC	i
benzinu	benzin	k1gInSc2	benzin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Označení	označení	k1gNnSc1	označení
benzínu	benzín	k1gInSc2	benzín
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
Benzín	benzín	k1gInSc1	benzín
s	s	k7c7	s
přísadou	přísada	k1gFnSc7	přísada
olova	olovo	k1gNnSc2	olovo
(	(	kIx(	(
<g/>
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
benzín	benzín	k1gInSc1	benzín
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
ČR	ČR	kA	ČR
přestal	přestat	k5eAaPmAgInS	přestat
prodávat	prodávat	k5eAaImF	prodávat
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
data	datum	k1gNnSc2	datum
se	se	k3xPyFc4	se
prodává	prodávat	k5eAaImIp3nS	prodávat
pouze	pouze	k6eAd1	pouze
tzv.	tzv.	kA	tzv.
bezolovnatý	bezolovnatý	k2eAgInSc4d1	bezolovnatý
benzín	benzín	k1gInSc4	benzín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
uloženo	uložit	k5eAaPmNgNnS	uložit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
86	[number]	k4	86
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
v	v	k7c6	v
aktuálním	aktuální	k2eAgNnSc6d1	aktuální
znění	znění	k1gNnSc6	znění
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
do	do	k7c2	do
pohonného	pohonný	k2eAgInSc2d1	pohonný
benzinu	benzin	k1gInSc2	benzin
povinně	povinně	k6eAd1	povinně
přidává	přidávat	k5eAaImIp3nS	přidávat
ethanol	ethanol	k1gInSc1	ethanol
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
z	z	k7c2	z
biomasy	biomasa	k1gFnSc2	biomasa
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
bioethanol	bioethanol	k1gInSc1	bioethanol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2018	[number]	k4	2018
je	být	k5eAaImIp3nS	být
povinné	povinný	k2eAgNnSc1d1	povinné
označovat	označovat	k5eAaImF	označovat
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
palivo	palivo	k1gNnSc1	palivo
jednotně	jednotně	k6eAd1	jednotně
<g/>
.	.	kIx.	.
</s>
<s>
Natural	Natural	k?	Natural
95	[number]	k4	95
a	a	k8xC	a
Natural	Natural	k?	Natural
98	[number]	k4	98
tak	tak	k6eAd1	tak
má	mít	k5eAaImIp3nS	mít
nyní	nyní	k6eAd1	nyní
označení	označení	k1gNnSc1	označení
E5	E5	k1gFnSc2	E5
a	a	k8xC	a
nafta	nafta	k1gFnSc1	nafta
B	B	kA	B
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
čerpací	čerpací	k2eAgFnPc1d1	čerpací
stanice	stanice	k1gFnPc1	stanice
si	se	k3xPyFc3	se
však	však	k9	však
vedle	vedle	k7c2	vedle
nového	nový	k2eAgNnSc2d1	nové
označení	označení	k1gNnSc2	označení
ponechají	ponechat	k5eAaPmIp3nP	ponechat
i	i	k9	i
stávající	stávající	k2eAgInPc4d1	stávající
názvy	název	k1gInPc4	název
<g/>
.	.	kIx.	.
<g/>
Jelikož	jelikož	k8xS	jelikož
evropská	evropský	k2eAgFnSc1d1	Evropská
legislativa	legislativa	k1gFnSc1	legislativa
nutí	nutit	k5eAaImIp3nS	nutit
petrolejáře	petrolejář	k1gMnPc4	petrolejář
k	k	k7c3	k
úspoře	úspora	k1gFnSc3	úspora
emisí	emise	k1gFnPc2	emise
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
zmizí	zmizet	k5eAaPmIp3nS	zmizet
Natural	Natural	k?	Natural
95	[number]	k4	95
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
a	a	k8xC	a
nahradí	nahradit	k5eAaPmIp3nS	nahradit
ho	on	k3xPp3gInSc4	on
benzín	benzín	k1gInSc4	benzín
E10	E10	k1gFnSc2	E10
(	(	kIx(	(
<g/>
s	s	k7c7	s
desetiprocentní	desetiprocentní	k2eAgFnSc7d1	desetiprocentní
biosložkou	biosložka	k1gFnSc7	biosložka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
plánovalo	plánovat	k5eAaImAgNnS	plánovat
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2019	[number]	k4	2019
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
se	se	k3xPyFc4	se
však	však	k9	však
počítalo	počítat	k5eAaImAgNnS	počítat
až	až	k9	až
s	s	k7c7	s
rokem	rok	k1gInSc7	rok
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
</s>
<s>
Benzín	benzín	k1gInSc1	benzín
E10	E10	k1gFnSc2	E10
může	moct	k5eAaImIp3nS	moct
poškodit	poškodit	k5eAaPmF	poškodit
motory	motor	k1gInPc4	motor
starších	starý	k2eAgInPc2d2	starší
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
v	v	k7c6	v
bezpečí	bezpečí	k1gNnSc6	bezpečí
jsou	být	k5eAaImIp3nP	být
auta	auto	k1gNnPc1	auto
vyrobená	vyrobený	k2eAgNnPc1d1	vyrobené
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
splňující	splňující	k2eAgFnSc4d1	splňující
normu	norma	k1gFnSc4	norma
Euro	euro	k1gNnSc1	euro
4	[number]	k4	4
a	a	k8xC	a
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
==	==	k?	==
</s>
</p>
<p>
<s>
Benzín	benzín	k1gInSc1	benzín
je	být	k5eAaImIp3nS	být
hořlavina	hořlavina	k1gFnSc1	hořlavina
citlivá	citlivý	k2eAgFnSc1d1	citlivá
na	na	k7c4	na
statickou	statický	k2eAgFnSc4d1	statická
elektřinu	elektřina	k1gFnSc4	elektřina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
používat	používat	k5eAaImF	používat
pouze	pouze	k6eAd1	pouze
bezpečné	bezpečný	k2eAgFnPc1d1	bezpečná
nádoby	nádoba	k1gFnPc1	nádoba
(	(	kIx(	(
<g/>
kanystry	kanystr	k1gInPc1	kanystr
<g/>
)	)	kIx)	)
určené	určený	k2eAgNnSc4d1	určené
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
těchto	tento	k3xDgFnPc2	tento
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Není	být	k5eNaImIp3nS	být
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
používat	používat	k5eAaImF	používat
např.	např.	kA	např.
PETláhve	PETláhev	k1gFnPc4	PETláhev
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
nádoby	nádoba	k1gFnPc4	nádoba
neurčené	určený	k2eNgFnPc4d1	neurčená
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
benzínu	benzín	k1gInSc2	benzín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Gasoline	Gasolin	k1gInSc5	Gasolin
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Oktanové	oktanový	k2eAgNnSc1d1	oktanové
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
<s>
Motorová	motorový	k2eAgFnSc1d1	motorová
nafta	nafta	k1gFnSc1	nafta
</s>
</p>
<p>
<s>
Solventní	solventní	k2eAgFnSc1d1	solventní
nafta	nafta	k1gFnSc1	nafta
</s>
</p>
<p>
<s>
Petrolej	petrolej	k1gInSc1	petrolej
</s>
</p>
<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Midgley	Midglea	k1gFnSc2	Midglea
</s>
</p>
<p>
<s>
Shell	Shell	k1gInSc1	Shell
Spirit	Spirita	k1gFnPc2	Spirita
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
benzín	benzín	k1gInSc1	benzín
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
benzín	benzín	k1gInSc1	benzín
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
benzin	benzin	k1gInSc1	benzin
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Benzín	benzín	k1gInSc4	benzín
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
