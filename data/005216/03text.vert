<s>
Derivace	derivace	k1gFnSc1	derivace
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc4d1	důležitý
pojem	pojem	k1gInSc4	pojem
matematické	matematický	k2eAgFnSc2d1	matematická
analýzy	analýza	k1gFnSc2	analýza
a	a	k8xC	a
základ	základ	k1gInSc4	základ
diferenciálního	diferenciální	k2eAgInSc2d1	diferenciální
počtu	počet	k1gInSc2	počet
<g/>
.	.	kIx.	.
</s>
<s>
Derivace	derivace	k1gFnSc1	derivace
nějaké	nějaký	k3yIgFnSc2	nějaký
funkce	funkce	k1gFnSc2	funkce
je	být	k5eAaImIp3nS	být
změna	změna	k1gFnSc1	změna
(	(	kIx(	(
<g/>
růst	růst	k1gInSc1	růst
či	či	k8xC	či
pokles	pokles	k1gInSc1	pokles
<g/>
)	)	kIx)	)
obrazu	obraz	k1gInSc2	obraz
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
(	(	kIx(	(
<g/>
ideálně	ideálně	k6eAd1	ideálně
<g/>
)	)	kIx)	)
nekonečně	konečně	k6eNd1	konečně
malé	malý	k2eAgFnSc6d1	malá
změně	změna	k1gFnSc6	změna
jejích	její	k3xOp3gInPc2	její
argumentů	argument	k1gInPc2	argument
<g/>
.	.	kIx.	.
</s>
<s>
Opačným	opačný	k2eAgInSc7d1	opačný
procesem	proces	k1gInSc7	proces
k	k	k7c3	k
derivování	derivování	k1gNnSc3	derivování
je	být	k5eAaImIp3nS	být
integrování	integrování	k1gNnSc4	integrování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
dvourozměrného	dvourozměrný	k2eAgInSc2d1	dvourozměrný
grafu	graf	k1gInSc2	graf
funkce	funkce	k1gFnSc2	funkce
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
derivace	derivace	k1gFnSc1	derivace
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
v	v	k7c6	v
libovolném	libovolný	k2eAgInSc6d1	libovolný
bodě	bod	k1gInSc6	bod
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
)	)	kIx)	)
rovna	roven	k2eAgFnSc1d1	rovna
směrnici	směrnice	k1gFnSc4	směrnice
tečny	tečna	k1gFnSc2	tečna
tohoto	tento	k3xDgInSc2	tento
grafu	graf	k1gInSc2	graf
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pokud	pokud	k8xS	pokud
funkce	funkce	k1gFnSc1	funkce
popisuje	popisovat	k5eAaImIp3nS	popisovat
dráhu	dráha	k1gFnSc4	dráha
tělesa	těleso	k1gNnSc2	těleso
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
její	její	k3xOp3gFnSc1	její
derivace	derivace	k1gFnSc1	derivace
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
bodě	bod	k1gInSc6	bod
udávat	udávat	k5eAaImF	udávat
okamžitou	okamžitý	k2eAgFnSc4d1	okamžitá
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
popisuje	popisovat	k5eAaImIp3nS	popisovat
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
derivace	derivace	k1gFnSc1	derivace
udávat	udávat	k5eAaImF	udávat
zrychlení	zrychlení	k1gNnSc4	zrychlení
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
derivace	derivace	k1gFnSc2	derivace
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
pracích	práce	k1gFnPc6	práce
Newtona	Newton	k1gMnSc2	Newton
a	a	k8xC	a
Leibnize	Leibnize	k1gFnSc2	Leibnize
při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
geometrických	geometrický	k2eAgInPc2d1	geometrický
a	a	k8xC	a
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
problému	problém	k1gInSc2	problém
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
nalézt	nalézt	k5eAaPmF	nalézt
rovnici	rovnice	k1gFnSc4	rovnice
tečny	tečna	k1gFnSc2	tečna
ke	k	k7c3	k
grafu	graf	k1gInSc3	graf
funkce	funkce	k1gFnSc2	funkce
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
libovolném	libovolný	k2eAgInSc6d1	libovolný
bodě	bod	k1gInSc6	bod
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
derivace	derivace	k1gFnSc2	derivace
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
a	a	k8xC	a
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k9	jako
odvozenina	odvozenina	k1gFnSc1	odvozenina
n.	n.	k?	n.
odvození	odvození	k1gNnSc1	odvození
<g/>
,	,	kIx,	,
srov.	srov.	kA	srov.
např.	např.	kA	např.
německý	německý	k2eAgInSc4d1	německý
název	název	k1gInSc4	název
pro	pro	k7c4	pro
derivaci	derivace	k1gFnSc4	derivace
"	"	kIx"	"
<g/>
Ableitung	Ableitung	k1gInSc4	Ableitung
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Neříká	říkat	k5eNaImIp3nS	říkat
to	ten	k3xDgNnSc1	ten
sice	sice	k8xC	sice
o	o	k7c6	o
vlastnostech	vlastnost	k1gFnPc6	vlastnost
derivace	derivace	k1gFnSc2	derivace
mnoho	mnoho	k6eAd1	mnoho
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aspoň	aspoň	k9	aspoň
tolik	tolik	k6eAd1	tolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
derivace	derivace	k1gFnSc1	derivace
funkce	funkce	k1gFnSc2	funkce
je	být	k5eAaImIp3nS	být
danou	daný	k2eAgFnSc7d1	daná
funkcí	funkce	k1gFnSc7	funkce
plně	plně	k6eAd1	plně
určena	určen	k2eAgFnSc1d1	určena
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
odvodit	odvodit	k5eAaPmF	odvodit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
"	"	kIx"	"
<g/>
obsažena	obsáhnout	k5eAaPmNgFnS	obsáhnout
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obrázku	obrázek	k1gInSc6	obrázek
je	být	k5eAaImIp3nS	být
graf	graf	k1gInSc1	graf
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
x	x	k?	x
hodnotu	hodnota	k1gFnSc4	hodnota
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bodě	bod	k1gInSc6	bod
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
Δ	Δ	k?	Δ
má	mít	k5eAaImIp3nS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
Δ	Δ	k?	Δ
<g/>
)	)	kIx)	)
a	a	k8xC	a
spojnice	spojnice	k1gFnSc1	spojnice
obou	dva	k4xCgInPc2	dva
bodů	bod	k1gInPc2	bod
tvoří	tvořit	k5eAaImIp3nP	tvořit
sečnu	sečna	k1gFnSc4	sečna
křivky	křivka	k1gFnSc2	křivka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc4	její
směrnici	směrnice	k1gFnSc4	směrnice
(	(	kIx(	(
<g/>
sklon	sklon	k1gInSc4	sklon
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xC	jako
poměr	poměr	k1gInSc1	poměr
(	(	kIx(	(
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
Δ	Δ	k?	Δ
<g/>
)	)	kIx)	)
-	-	kIx~	-
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
))	))	k?	))
/	/	kIx~	/
Δ	Δ	k?	Δ
.	.	kIx.	.
</s>
<s>
Budeme	být	k5eAaImBp1nP	být
<g/>
-li	i	k?	-li
nyní	nyní	k6eAd1	nyní
oba	dva	k4xCgInPc4	dva
body	bod	k1gInPc4	bod
přibližovat	přibližovat	k5eAaImF	přibližovat
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
zmenšovat	zmenšovat	k5eAaImF	zmenšovat
diferenci	diference	k1gFnSc4	diference
Δ	Δ	k?	Δ
až	až	k9	až
k	k	k7c3	k
nule	nula	k1gFnSc3	nula
<g/>
,	,	kIx,	,
přejde	přejít	k5eAaPmIp3nS	přejít
sečna	sečna	k1gFnSc1	sečna
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c4	v
tečnu	tečna	k1gFnSc4	tečna
<g/>
.	.	kIx.	.
</s>
<s>
Tečna	tečna	k1gFnSc1	tečna
svírá	svírat	k5eAaImIp3nS	svírat
úhel	úhel	k1gInSc4	úhel
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
x	x	k?	x
a	a	k8xC	a
tangens	tangens	k1gInSc1	tangens
tohoto	tento	k3xDgInSc2	tento
úhlu	úhel	k1gInSc2	úhel
nazýváme	nazývat	k5eAaImIp1nP	nazývat
směrnicí	směrnice	k1gFnSc7	směrnice
tečny	tečna	k1gFnSc2	tečna
<g/>
.	.	kIx.	.
</s>
<s>
Derivaci	derivace	k1gFnSc4	derivace
funkce	funkce	k1gFnSc2	funkce
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
lze	lze	k6eAd1	lze
s	s	k7c7	s
dostatečnou	dostatečný	k2eAgFnSc7d1	dostatečná
přesností	přesnost	k1gFnSc7	přesnost
aproximovat	aproximovat	k5eAaBmF	aproximovat
právě	právě	k6eAd1	právě
jako	jako	k8xS	jako
tuto	tento	k3xDgFnSc4	tento
směrnici	směrnice	k1gFnSc4	směrnice
tečny	tečna	k1gFnSc2	tečna
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
x	x	k?	x
křivka	křivka	k1gFnSc1	křivka
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
její	její	k3xOp3gFnSc1	její
derivace	derivace	k1gFnSc1	derivace
>	>	kIx)	>
<g/>
0	[number]	k4	0
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
klesající	klesající	k2eAgFnSc1d1	klesající
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
derivace	derivace	k1gFnSc1	derivace
<	<	kIx(	<
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
křivka	křivka	k1gFnSc1	křivka
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
x	x	k?	x
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
maxima	maximum	k1gNnSc2	maximum
nebo	nebo	k8xC	nebo
minima	minimum	k1gNnSc2	minimum
a	a	k8xC	a
tečna	tečna	k1gFnSc1	tečna
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
x	x	k?	x
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
derivace	derivace	k1gFnSc1	derivace
rovna	roven	k2eAgFnSc1d1	rovna
nule	nula	k1gFnSc3	nula
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
obrázku	obrázek	k1gInSc6	obrázek
je	být	k5eAaImIp3nS	být
znázorněná	znázorněný	k2eAgFnSc1d1	znázorněná
grafická	grafický	k2eAgFnSc1d1	grafická
derivace	derivace	k1gFnSc1	derivace
funkce	funkce	k1gFnSc1	funkce
sinus	sinus	k1gInSc4	sinus
pomocí	pomocí	k7c2	pomocí
tečny	tečna	k1gFnSc2	tečna
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgFnPc1d1	historická
definice	definice	k1gFnPc1	definice
vyjadřovaly	vyjadřovat	k5eAaImAgFnP	vyjadřovat
derivaci	derivace	k1gFnSc4	derivace
jako	jako	k8xS	jako
poměr	poměr	k1gInSc4	poměr
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yIgInSc6	jaký
růst	růst	k5eAaImF	růst
či	či	k8xC	či
pokles	pokles	k1gInSc1	pokles
závislé	závislý	k2eAgFnSc2d1	závislá
proměnné	proměnná	k1gFnSc2	proměnná
y	y	k?	y
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
změně	změna	k1gFnSc3	změna
nezávisle	závisle	k6eNd1	závisle
proměnné	proměnná	k1gFnSc2	proměnná
x.	x.	k?	x.
Nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
představa	představa	k1gFnSc1	představa
o	o	k7c6	o
derivaci	derivace	k1gFnSc6	derivace
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
derivace	derivace	k1gFnSc1	derivace
je	být	k5eAaImIp3nS	být
mírou	míra	k1gFnSc7	míra
změny	změna	k1gFnSc2	změna
funkce	funkce	k1gFnSc2	funkce
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
bodě	bod	k1gInSc6	bod
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
bodech	bod	k1gInPc6	bod
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
hodnoty	hodnota	k1gFnSc2	hodnota
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
symbol	symbol	k1gInSc1	symbol
Δ	Δ	k?	Δ
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tento	tento	k3xDgInSc1	tento
poměr	poměr	k1gInSc1	poměr
lze	lze	k6eAd1	lze
symbolicky	symbolicky	k6eAd1	symbolicky
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Derivace	derivace	k1gFnSc1	derivace
je	být	k5eAaImIp3nS	být
hodnota	hodnota	k1gFnSc1	hodnota
podílu	podíl	k1gInSc2	podíl
pro	pro	k7c4	pro
Δ	Δ	k?	Δ
jdoucí	jdoucí	k2eAgInPc1d1	jdoucí
k	k	k7c3	k
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Nahradíme	nahradit	k5eAaPmIp1nP	nahradit
<g/>
-li	i	k?	-li
konečně	konečně	k6eAd1	konečně
malý	malý	k2eAgInSc1d1	malý
rozdíl	rozdíl	k1gInSc1	rozdíl
Δ	Δ	k?	Δ
nekonečně	konečně	k6eNd1	konečně
malou	malý	k2eAgFnSc7d1	malá
změnou	změna	k1gFnSc7	změna
dx	dx	k?	dx
<g/>
,	,	kIx,	,
získáme	získat	k5eAaPmIp1nP	získat
intuitivní	intuitivní	k2eAgFnSc4d1	intuitivní
definici	definice	k1gFnSc4	definice
derivace	derivace	k1gFnSc2	derivace
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
což	což	k3yQnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
poměr	poměr	k1gInSc1	poměr
dvou	dva	k4xCgFnPc2	dva
infinitezimálních	infinitezimální	k2eAgFnPc2d1	infinitezimální
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Derivace	derivace	k1gFnSc1	derivace
vskutku	vskutku	k9	vskutku
je	být	k5eAaImIp3nS	být
podílem	podíl	k1gInSc7	podíl
dvou	dva	k4xCgFnPc2	dva
diferenciálních	diferenciální	k2eAgFnPc2d1	diferenciální
forem	forma	k1gFnPc2	forma
-	-	kIx~	-
diferenciálu	diferenciál	k1gInSc2	diferenciál
závislé	závislý	k2eAgFnSc2d1	závislá
a	a	k8xC	a
diferenciálu	diferenciál	k1gInSc2	diferenciál
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
proměnné	proměnná	k1gFnSc2	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
(	(	kIx(	(
<g/>
Leibnizův	Leibnizův	k2eAgInSc1d1	Leibnizův
<g/>
)	)	kIx)	)
zápis	zápis	k1gInSc1	zápis
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
dy	dy	k?	dy
podle	podle	k7c2	podle
dx	dx	k?	dx
a	a	k8xC	a
chápe	chápat	k5eAaImIp3nS	chápat
buď	buď	k8xC	buď
jako	jako	k8xC	jako
jediný	jediný	k2eAgInSc1d1	jediný
symbol	symbol	k1gInSc1	symbol
<g/>
,	,	kIx,	,
označující	označující	k2eAgInSc1d1	označující
prostě	prostě	k9	prostě
jen	jen	k9	jen
derivování	derivování	k1gNnSc1	derivování
funkce	funkce	k1gFnSc2	funkce
y	y	k?	y
podle	podle	k7c2	podle
proměnné	proměnná	k1gFnSc2	proměnná
x	x	k?	x
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
opravdu	opravdu	k6eAd1	opravdu
i	i	k9	i
jako	jako	k9	jako
zlomek	zlomek	k1gInSc4	zlomek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
lze	lze	k6eAd1	lze
diferenciály	diferenciál	k1gInPc4	diferenciál
chápat	chápat	k5eAaImF	chápat
buď	buď	k8xC	buď
elementárněji	elementárně	k6eAd2	elementárně
jako	jako	k9	jako
diferenciální	diferenciální	k2eAgFnPc1d1	diferenciální
formy	forma	k1gFnPc1	forma
anebo	anebo	k8xC	anebo
jako	jako	k9	jako
nekonečně	konečně	k6eNd1	konečně
malé	malý	k2eAgFnPc4d1	malá
veličiny	veličina	k1gFnPc4	veličina
(	(	kIx(	(
<g/>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tzv.	tzv.	kA	tzv.
nestandardní	standardní	k2eNgFnSc2d1	nestandardní
analýzy	analýza	k1gFnSc2	analýza
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pěstoval	pěstovat	k5eAaImAgMnS	pěstovat
mj.	mj.	kA	mj.
i	i	k8xC	i
český	český	k2eAgMnSc1d1	český
matematik	matematik	k1gMnSc1	matematik
Petr	Petr	k1gMnSc1	Petr
Vopěnka	Vopěnka	k1gFnSc1	Vopěnka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
matematiky	matematika	k1gFnSc2	matematika
se	se	k3xPyFc4	se
intuitivní	intuitivní	k2eAgFnSc1d1	intuitivní
představa	představa	k1gFnSc1	představa
nekonečně	konečně	k6eNd1	konečně
malých	malý	k2eAgFnPc2d1	malá
(	(	kIx(	(
<g/>
infinitezimálních	infinitezimální	k2eAgFnPc2d1	infinitezimální
<g/>
)	)	kIx)	)
hodnot	hodnota	k1gFnPc2	hodnota
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k9	jako
nedostatečně	dostatečně	k6eNd1	dostatečně
přesná	přesný	k2eAgFnSc1d1	přesná
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
nahrazena	nahrazen	k2eAgFnSc1d1	nahrazena
"	"	kIx"	"
<g/>
ε	ε	k?	ε
<g/>
"	"	kIx"	"
formalismem	formalismus	k1gInSc7	formalismus
limit	limit	k1gInSc1	limit
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgFnSc1d3	nejběžnější
moderní	moderní	k2eAgFnSc1d1	moderní
definice	definice	k1gFnSc1	definice
derivace	derivace	k1gFnSc2	derivace
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
'	'	kIx"	'
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
h	h	k?	h
→	→	k?	→
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
+	+	kIx~	+
h	h	k?	h
)	)	kIx)	)
−	−	k?	−
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
h	h	k?	h
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
−	−	k?	−
f	f	k?	f
(	(	kIx(	(
a	a	k8xC	a
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
−	−	k?	−
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
h	h	k?	h
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
-f	-f	k?	-f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
h	h	k?	h
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
-f	-f	k?	-f
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
x-a	x	k?	x-a
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zápis	zápis	k1gInSc1	zápis
derivace	derivace	k1gFnSc2	derivace
<g/>
.	.	kIx.	.
</s>
<s>
Derivace	derivace	k1gFnSc1	derivace
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
několika	několik	k4yIc7	několik
způsoby	způsob	k1gInPc7	způsob
(	(	kIx(	(
<g/>
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
je	být	k5eAaImIp3nS	být
čtení	čtení	k1gNnSc1	čtení
zápisu	zápis	k1gInSc2	zápis
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
'	'	kIx"	'
<g/>
}	}	kIx)	}
[	[	kIx(	[
<g/>
:	:	kIx,	:
<g/>
f	f	k?	f
s	s	k7c7	s
čárkou	čárka	k1gFnSc7	čárka
<g/>
:	:	kIx,	:
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
[	[	kIx(	[
<g/>
:	:	kIx,	:
<g/>
d	d	k?	d
podle	podle	k7c2	podle
d	d	k?	d
x	x	k?	x
z	z	k7c2	z
f	f	k?	f
x	x	k?	x
<g/>
:	:	kIx,	:
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
f	f	k?	f
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
[	[	kIx(	[
<g/>
:	:	kIx,	:
<g/>
d	d	k?	d
f	f	k?	f
podle	podle	k7c2	podle
d	d	k?	d
x	x	k?	x
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
D_	D_	k1gFnSc6	D_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
\	\	kIx~	\
<g/>
quad	quad	k1gMnSc1	quad
}	}	kIx)	}
[	[	kIx(	[
<g/>
:	:	kIx,	:
<g/>
d	d	k?	d
podle	podle	k7c2	podle
x	x	k?	x
f	f	k?	f
<g/>
:	:	kIx,	:
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
[	[	kIx(	[
<g/>
:	:	kIx,	:
<g/>
f	f	k?	f
x	x	k?	x
<g/>
:	:	kIx,	:
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Newtonova	Newtonův	k2eAgFnSc1d1	Newtonova
notace	notace	k1gFnSc1	notace
používá	používat	k5eAaImIp3nS	používat
tečku	tečka	k1gFnSc4	tečka
nad	nad	k7c4	nad
proměnnou	proměnná	k1gFnSc4	proměnná
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
̇	̇	k?	̇
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
x	x	k?	x
'	'	kIx"	'
:	:	kIx,	:
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
dot	dot	k?	dot
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
pro	pro	k7c4	pro
derivování	derivování	k1gNnSc4	derivování
podle	podle	k7c2	podle
proměnné	proměnná	k1gFnSc2	proměnná
vyjadřující	vyjadřující	k2eAgInSc4d1	vyjadřující
čas	čas	k1gInSc4	čas
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
však	však	k9	však
limita	limita	k1gFnSc1	limita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
derivaci	derivace	k1gFnSc4	derivace
definuje	definovat	k5eAaBmIp3nS	definovat
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
konečná	konečný	k2eAgFnSc1d1	konečná
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
ne	ne	k9	ne
každá	každý	k3xTgFnSc1	každý
funkce	funkce	k1gFnSc1	funkce
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
bodě	bod	k1gInSc6	bod
derivaci	derivace	k1gFnSc4	derivace
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
limita	limita	k1gFnSc1	limita
nevlastní	vlastní	k2eNgFnSc1d1	nevlastní
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
derivace	derivace	k1gFnSc1	derivace
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
můžeme	moct	k5eAaImIp1nP	moct
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
bodě	bod	k1gInSc6	bod
derivace	derivace	k1gFnSc2	derivace
nevlastní	vlastnit	k5eNaImIp3nP	vlastnit
<g/>
.	.	kIx.	.
</s>
<s>
Říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
funkce	funkce	k1gFnSc1	funkce
f	f	k?	f
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
x	x	k?	x
diferencovatelná	diferencovatelný	k2eAgFnSc1d1	diferencovatelná
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bodě	bod	k1gInSc6	bod
derivace	derivace	k1gFnSc2	derivace
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
;	;	kIx,	;
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
diferencovatelná	diferencovatelný	k2eAgFnSc1d1	diferencovatelná
na	na	k7c6	na
intervalu	interval	k1gInSc6	interval
I	i	k9	i
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
diferencovatelná	diferencovatelný	k2eAgFnSc1d1	diferencovatelná
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
bodě	bod	k1gInSc6	bod
tohoto	tento	k3xDgInSc2	tento
intervalu	interval	k1gInSc2	interval
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
nemá	mít	k5eNaImIp3nS	mít
derivaci	derivace	k1gFnSc4	derivace
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
spojitá	spojitý	k2eAgFnSc1d1	spojitá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spojitost	spojitost	k1gFnSc1	spojitost
funkce	funkce	k1gFnSc2	funkce
existenci	existence	k1gFnSc3	existence
derivace	derivace	k1gFnSc2	derivace
nezaručuje	zaručovat	k5eNaImIp3nS	zaručovat
–	–	k?	–
funkce	funkce	k1gFnSc1	funkce
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
bodě	bod	k1gInSc6	bod
svislou	svislý	k2eAgFnSc4d1	svislá
tečnu	tečna	k1gFnSc4	tečna
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
nevlastní	vlastní	k2eNgNnSc1d1	nevlastní
<g/>
,	,	kIx,	,
nekonečné	konečný	k2eNgFnSc3d1	nekonečná
derivaci	derivace	k1gFnSc3	derivace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
bodě	bod	k1gInSc6	bod
nemusí	muset	k5eNaImIp3nS	muset
mít	mít	k5eAaImF	mít
tečnu	tečna	k1gFnSc4	tečna
vůbec	vůbec	k9	vůbec
(	(	kIx(	(
<g/>
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
graf	graf	k1gInSc1	graf
funkce	funkce	k1gFnSc2	funkce
"	"	kIx"	"
<g/>
špičku	špička	k1gFnSc4	špička
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dokonce	dokonce	k9	dokonce
funkce	funkce	k1gFnPc1	funkce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
spojité	spojitý	k2eAgInPc1d1	spojitý
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
bodě	bod	k1gInSc6	bod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemají	mít	k5eNaImIp3nP	mít
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
bodě	bod	k1gInSc6	bod
derivaci	derivace	k1gFnSc3	derivace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tzv.	tzv.	kA	tzv.
Weierstrassova	Weierstrassův	k2eAgFnSc1d1	Weierstrassova
funkce	funkce	k1gFnSc1	funkce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
daná	daný	k2eAgFnSc1d1	daná
funkce	funkce	k1gFnSc1	funkce
diferencovatelná	diferencovatelný	k2eAgFnSc1d1	diferencovatelná
na	na	k7c6	na
nějakém	nějaký	k3yIgInSc6	nějaký
intervalu	interval	k1gInSc6	interval
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
intervalu	interval	k1gInSc6	interval
definovat	definovat	k5eAaBmF	definovat
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
bodě	bod	k1gInSc6	bod
tohoto	tento	k3xDgInSc2	tento
intervalu	interval	k1gInSc2	interval
rovná	rovnat	k5eAaImIp3nS	rovnat
příslušné	příslušný	k2eAgFnSc3d1	příslušná
derivaci	derivace	k1gFnSc3	derivace
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
funkce	funkce	k1gFnSc1	funkce
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
označuje	označovat	k5eAaImIp3nS	označovat
prostě	prostě	k9	prostě
jako	jako	k9	jako
derivace	derivace	k1gFnPc1	derivace
funkce	funkce	k1gFnPc1	funkce
f.	f.	k?	f.
Derivací	derivace	k1gFnSc7	derivace
diferencovatelné	diferencovatelný	k2eAgFnSc2d1	diferencovatelná
funkce	funkce	k1gFnSc2	funkce
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
opět	opět	k6eAd1	opět
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ovšem	ovšem	k9	ovšem
někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
diferencovatelná	diferencovatelný	k2eAgFnSc1d1	diferencovatelná
<g/>
.	.	kIx.	.
</s>
<s>
Derivaci	derivace	k1gFnSc4	derivace
derivace	derivace	k1gFnSc2	derivace
funkce	funkce	k1gFnSc2	funkce
nazýváme	nazývat	k5eAaImIp1nP	nazývat
druhá	druhý	k4xOgFnSc1	druhý
derivace	derivace	k1gFnSc1	derivace
<g/>
,	,	kIx,	,
derivaci	derivace	k1gFnSc4	derivace
druhé	druhý	k4xOgFnSc2	druhý
derivace	derivace	k1gFnSc2	derivace
třetí	třetí	k4xOgFnSc1	třetí
derivace	derivace	k1gFnSc1	derivace
atd.	atd.	kA	atd.
Tyto	tento	k3xDgFnPc4	tento
derivace	derivace	k1gFnPc4	derivace
vyšších	vysoký	k2eAgInPc2d2	vyšší
řádů	řád	k1gInPc2	řád
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
značí	značit	k5eAaImIp3nS	značit
f	f	k?	f
<g/>
''	''	k?	''
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
ještě	ještě	k6eAd1	ještě
vyšší	vysoký	k2eAgInPc4d2	vyšší
řády	řád	k1gInPc4	řád
pak	pak	k6eAd1	pak
spíše	spíše	k9	spíše
f	f	k?	f
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
Leibnizovy	Leibnizův	k2eAgFnSc2d1	Leibnizova
notace	notace	k1gFnSc2	notace
se	se	k3xPyFc4	se
derivace	derivace	k1gFnSc1	derivace
vyšších	vysoký	k2eAgInPc2d2	vyšší
řádů	řád	k1gInPc2	řád
označují	označovat	k5eAaImIp3nP	označovat
exponentem	exponent	k1gInSc7	exponent
<g/>
,	,	kIx,	,
např.	např.	kA	např.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}}}	}}}}	k?	}}}}
(	(	kIx(	(
<g/>
d	d	k?	d
třetí	třetí	k4xOgInPc4	třetí
y	y	k?	y
podle	podle	k7c2	podle
d	d	k?	d
x	x	k?	x
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
derivace	derivace	k1gFnSc2	derivace
lze	lze	k6eAd1	lze
naopak	naopak	k6eAd1	naopak
získat	získat	k5eAaPmF	získat
původní	původní	k2eAgFnSc4d1	původní
funkci	funkce	k1gFnSc4	funkce
integrováním	integrování	k1gNnSc7	integrování
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
známe	znát	k5eAaImIp1nP	znát
funkční	funkční	k2eAgFnSc4d1	funkční
hodnotu	hodnota	k1gFnSc4	hodnota
původní	původní	k2eAgFnSc2d1	původní
funkce	funkce	k1gFnSc2	funkce
aspoň	aspoň	k9	aspoň
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
bodě	bod	k1gInSc6	bod
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
počáteční	počáteční	k2eAgFnSc4d1	počáteční
podmínku	podmínka	k1gFnSc4	podmínka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zobecněním	zobecnění	k1gNnSc7	zobecnění
pojmu	pojem	k1gInSc2	pojem
derivace	derivace	k1gFnSc2	derivace
pro	pro	k7c4	pro
funkce	funkce	k1gFnPc4	funkce
více	hodně	k6eAd2	hodně
proměnných	proměnná	k1gFnPc2	proměnná
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
parciální	parciální	k2eAgFnSc1d1	parciální
derivace	derivace	k1gFnSc1	derivace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
u	u	k7c2	u
funkce	funkce	k1gFnSc2	funkce
více	hodně	k6eAd2	hodně
proměnných	proměnný	k2eAgMnPc2d1	proměnný
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
proměnnou	proměnná	k1gFnSc4	proměnná
jenom	jenom	k6eAd1	jenom
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
derivuje	derivovat	k5eAaBmIp3nS	derivovat
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgInPc1d1	ostatní
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
výpočtu	výpočet	k1gInSc6	výpočet
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
konstanty	konstanta	k1gFnPc4	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Parciální	parciální	k2eAgFnSc1d1	parciální
derivace	derivace	k1gFnSc1	derivace
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xS	jako
obyčejné	obyčejný	k2eAgFnPc1d1	obyčejná
derivace	derivace	k1gFnPc1	derivace
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
místo	místo	k7c2	místo
symbolu	symbol	k1gInSc2	symbol
d	d	k?	d
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
symbol	symbol	k1gInSc1	symbol
∂	∂	k?	∂
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
partial	partiat	k5eAaBmAgMnS	partiat
f	f	k?	f
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
y	y	k?	y
<g/>
}}}	}}}	k?	}}}
značí	značit	k5eAaImIp3nS	značit
parciální	parciální	k2eAgFnSc1d1	parciální
derivaci	derivace	k1gFnSc4	derivace
funkce	funkce	k1gFnSc2	funkce
f	f	k?	f
podle	podle	k7c2	podle
proměnné	proměnná	k1gFnSc2	proměnná
y.	y.	k?	y.
Definice	definice	k1gFnSc2	definice
<g/>
:	:	kIx,	:
Nechť	nechť	k9	nechť
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
,	,	kIx,	,
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
Y	Y	kA	Y
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X_	X_	k1gMnSc6	X_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
<g/>
X_	X_	k1gMnSc1	X_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
normované	normovaný	k2eAgInPc4d1	normovaný
prostory	prostor	k1gInPc4	prostor
<g/>
,	,	kIx,	,
Říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zobrazení	zobrazení	k1gNnSc1	zobrazení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
X	X	kA	X
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
×	×	k?	×
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
×	×	k?	×
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
Y	Y	kA	Y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
:	:	kIx,	:
<g/>
X_	X_	k1gMnSc1	X_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
...	...	k?	...
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
X_	X_	k1gMnSc1	X_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
Y	Y	kA	Y
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
Fréchetovsky	Fréchetovsky	k1gMnSc1	Fréchetovsky
(	(	kIx(	(
<g/>
Gatteauxovsky	Gatteauxovsko	k1gNnPc7	Gatteauxovsko
<g/>
)	)	kIx)	)
derivovatelné	derivovatelný	k2eAgNnSc1d1	derivovatelný
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
i	i	k9	i
<g/>
}	}	kIx)	}
-té	é	k?	-té
souřadnici	souřadnice	k1gFnSc4	souřadnice
pokud	pokud	k8xS	pokud
zobrazení	zobrazení	k1gNnSc4	zobrazení
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k8xC	i
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
.	.	kIx.	.
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k9	i
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
Y	Y	kA	Y
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
f	f	k?	f
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k8xC	i
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
~	~	kIx~	~
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k9	i
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i-	i-	k?	i-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
X_	X_	k1gFnSc1	X_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
Y	Y	kA	Y
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i-	i-	k?	i-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
zobrazení	zobrazení	k1gNnSc1	zobrazení
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
souřadnicemi	souřadnice	k1gFnPc7	souřadnice
FIXOVANÝMI	fixovaný	k2eAgFnPc7d1	fixovaná
<g/>
)	)	kIx)	)
je	on	k3xPp3gFnPc4	on
F-	F-	k1gFnPc4	F-
<g/>
(	(	kIx(	(
<g/>
G-	G-	k1gFnPc4	G-
<g/>
)	)	kIx)	)
diferencovatelné	diferencovatelný	k2eAgFnPc4d1	diferencovatelná
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
funkci	funkce	k1gFnSc4	funkce
více	hodně	k6eAd2	hodně
proměnných	proměnná	k1gFnPc2	proměnná
je	být	k5eAaImIp3nS	být
derivace	derivace	k1gFnSc1	derivace
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
vektoru	vektor	k1gInSc2	vektor
v	v	k7c6	v
definována	definovat	k5eAaBmNgFnS	definovat
vztahem	vztah	k1gInSc7	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∇	∇	k?	∇
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
h	h	k?	h
→	→	k?	→
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
+	+	kIx~	+
h	h	k?	h
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
−	−	k?	−
f	f	k?	f
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
h	h	k?	h
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nabla	nablo	k1gNnPc4	nablo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
h	h	k?	h
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
-f	-f	k?	-f
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
h	h	k?	h
<g/>
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
funkce	funkce	k1gFnSc1	funkce
f	f	k?	f
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
x	x	k?	x
diferencovatelná	diferencovatelný	k2eAgFnSc1d1	diferencovatelná
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∇	∇	k?	∇
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
∇	∇	k?	∇
f	f	k?	f
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
⋅	⋅	k?	⋅
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nabla	nablo	k1gNnPc4	nablo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
nabla	nabla	k1gFnSc1	nabla
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c4	v
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∇	∇	k?	∇
f	f	k?	f
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nabla	nablo	k1gNnPc1	nablo
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
gradient	gradient	k1gInSc1	gradient
funkce	funkce	k1gFnSc2	funkce
f	f	k?	f
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
x	x	k?	x
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
}	}	kIx)	}
značí	značit	k5eAaImIp3nS	značit
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
derivace	derivace	k1gFnSc1	derivace
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
vektoru	vektor	k1gInSc2	vektor
v	v	k7c6	v
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
vektoru	vektor	k1gInSc2	vektor
|	|	kIx~	|
<g/>
v	v	k7c4	v
<g/>
|	|	kIx~	|
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
|	|	kIx~	|
<g/>
v	v	k7c6	v
<g/>
|	|	kIx~	|
=	=	kIx~	=
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
definice	definice	k1gFnSc1	definice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
vektoru	vektor	k1gInSc2	vektor
v	v	k7c6	v
nezávisí	záviset	k5eNaImIp3nS	záviset
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∇	∇	k?	∇
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
h	h	k?	h
→	→	k?	→
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
h	h	k?	h
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
−	−	k?	−
f	f	k?	f
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nabla	nablo	k1gNnPc4	nablo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
h	h	k?	h
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
-f	-f	k?	-f
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
h	h	k?	h
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}}	}}	k?	}}
<g/>
|	|	kIx~	|
<g/>
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Totální	totální	k2eAgFnSc1d1	totální
derivace	derivace	k1gFnSc1	derivace
je	být	k5eAaImIp3nS	být
derivace	derivace	k1gFnSc1	derivace
funkce	funkce	k1gFnSc1	funkce
více	hodně	k6eAd2	hodně
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
parciální	parciální	k2eAgFnSc2d1	parciální
derivace	derivace	k1gFnSc2	derivace
zohledňuje	zohledňovat	k5eAaImIp3nS	zohledňovat
závislosti	závislost	k1gFnSc2	závislost
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
proměnnými	proměnná	k1gFnPc7	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
komplexní	komplexní	k2eAgFnSc4d1	komplexní
funkci	funkce	k1gFnSc4	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
z	z	k7c2	z
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
řekneme	říct	k5eAaPmIp1nP	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
derivaci	derivace	k1gFnSc6	derivace
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
existuje	existovat	k5eAaImIp3nS	existovat
limita	limita	k1gFnSc1	limita
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
z	z	k7c2	z
→	→	k?	→
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
z	z	k7c2	z
)	)	kIx)	)
−	−	k?	−
f	f	k?	f
(	(	kIx(	(
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
−	−	k?	−
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
-f	-f	k?	-f
<g/>
(	(	kIx(	(
<g/>
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
z-z_	z_	k?	z-z_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
f	f	k?	f
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
z	z	k7c2	z
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Derivace	derivace	k1gFnSc1	derivace
existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
předchozí	předchozí	k2eAgFnSc1d1	předchozí
limita	limita	k1gFnSc1	limita
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c6	na
směru	směr	k1gInSc6	směr
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
komplexní	komplexní	k2eAgFnSc6d1	komplexní
rovině	rovina	k1gFnSc6	rovina
přibližujeme	přibližovat	k5eAaImIp1nP	přibližovat
ke	k	k7c3	k
komplexnímu	komplexní	k2eAgInSc3d1	komplexní
bodu	bod	k1gInSc3	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
podmínka	podmínka	k1gFnSc1	podmínka
je	být	k5eAaImIp3nS	být
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
Cauchyho-Riemannovými	Cauchyho-Riemannův	k2eAgFnPc7d1	Cauchyho-Riemannův
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
z	z	k7c2	z
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
derivaci	derivace	k1gFnSc6	derivace
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
spojitá	spojitý	k2eAgNnPc4d1	spojité
<g/>
.	.	kIx.	.
</s>
<s>
Komplexní	komplexní	k2eAgFnSc4d1	komplexní
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
derivaci	derivace	k1gFnSc6	derivace
<g/>
,	,	kIx,	,
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
monogenní	monogenní	k2eAgInPc1d1	monogenní
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
z	z	k7c2	z
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
derivaci	derivace	k1gFnSc3	derivace
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
bodě	bod	k1gInSc6	bod
oblasti	oblast	k1gFnSc2	oblast
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
G	G	kA	G
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
G	G	kA	G
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
G	G	kA	G
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
G	G	kA	G
<g/>
}	}	kIx)	}
}	}	kIx)	}
holomorfní	holomorfní	k2eAgFnSc1d1	holomorfní
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
holomorfní	holomorfní	k2eAgFnSc1d1	holomorfní
funkce	funkce	k1gFnSc1	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
z	z	k7c2	z
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
víceznačná	víceznačný	k2eAgFnSc1d1	víceznačná
<g/>
,	,	kIx,	,
označujeme	označovat	k5eAaImIp1nP	označovat
ji	on	k3xPp3gFnSc4	on
jako	jako	k8xC	jako
analytickou	analytický	k2eAgFnSc4d1	analytická
<g/>
.	.	kIx.	.
</s>
<s>
Derivací	derivace	k1gFnSc7	derivace
vektoru	vektor	k1gInSc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
}	}	kIx)	}
podle	podle	k7c2	podle
proměnné	proměnná	k1gFnSc2	proměnná
t	t	k?	t
rozumíme	rozumět	k5eAaImIp1nP	rozumět
vektor	vektor	k1gInSc4	vektor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc2	jehož
složky	složka	k1gFnSc2	složka
získáme	získat	k5eAaPmIp1nP	získat
derivací	derivace	k1gFnSc7	derivace
složek	složka	k1gFnPc2	složka
vektoru	vektor	k1gInSc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
⋯	⋯	k?	⋯
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k1gInSc1	cdots
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Obdobně	obdobně	k6eAd1	obdobně
postupujeme	postupovat	k5eAaImIp1nP	postupovat
při	při	k7c6	při
derivaci	derivace	k1gFnSc6	derivace
tenzorů	tenzor	k1gInPc2	tenzor
<g/>
.	.	kIx.	.
</s>
<s>
Derivaci	derivace	k1gFnSc4	derivace
funkce	funkce	k1gFnSc2	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
také	také	k9	také
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
první	první	k4xOgFnSc4	první
derivaci	derivace	k1gFnSc4	derivace
(	(	kIx(	(
<g/>
derivaci	derivace	k1gFnSc4	derivace
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
lze	lze	k6eAd1	lze
opět	opět	k6eAd1	opět
derivovat	derivovat	k5eAaBmF	derivovat
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
získáme	získat	k5eAaPmIp1nP	získat
druhou	druhý	k4xOgFnSc4	druhý
derivaci	derivace	k1gFnSc4	derivace
(	(	kIx(	(
<g/>
derivaci	derivace	k1gFnSc4	derivace
druhého	druhý	k4xOgInSc2	druhý
<g />
.	.	kIx.	.
</s>
<s hack="1">
řádu	řád	k1gInSc2	řád
<g/>
)	)	kIx)	)
funkce	funkce	k1gFnSc1	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
[	[	kIx(	[
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
h	h	k?	h
→	→	k?	→
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
+	+	kIx~	+
h	h	k?	h
)	)	kIx)	)
−	−	k?	−
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
h	h	k?	h
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
[	[	kIx(	[
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
+	+	kIx~	+
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
-f	-f	k?	-f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
h	h	k?	h
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Dalším	další	k2eAgNnSc7d1	další
derivováním	derivování	k1gNnSc7	derivování
můžeme	moct	k5eAaImIp1nP	moct
získat	získat	k5eAaPmF	získat
vyšší	vysoký	k2eAgFnSc1d2	vyšší
derivace	derivace	k1gFnSc1	derivace
funkce	funkce	k1gFnSc2	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
které	který	k3yRgInPc4	který
značíme	značit	k5eAaImIp1nP	značit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
'	'	kIx"	'
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
4	[number]	k4	4
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
5	[number]	k4	5
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
atd.	atd.	kA	atd.
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
jiné	jiný	k2eAgNnSc1d1	jiné
značení	značení	k1gNnSc1	značení
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
n-tou	nat	k5eAaBmIp3nP	n-tat
derivaci	derivace	k1gFnSc4	derivace
značíme	značit	k5eAaImIp1nP	značit
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
n	n	k0	n
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}}	}}}}	k?	}}}}
,	,	kIx,	,
popř.	popř.	kA	popř.
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
derivace	derivace	k1gFnSc2	derivace
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
a	a	k8xC	a
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
[	[	kIx(	[
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
použít	použít	k5eAaPmF	použít
také	také	k9	také
tzv.	tzv.	kA	tzv.
nultou	nultý	k4xOgFnSc4	nultý
derivaci	derivace	k1gFnSc4	derivace
funkce	funkce	k1gFnSc2	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
za	za	k7c4	za
niž	jenž	k3xRgFnSc4	jenž
považujeme	považovat	k5eAaImIp1nP	považovat
samotnou	samotný	k2eAgFnSc4d1	samotná
funkci	funkce	k1gFnSc4	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
0	[number]	k4	0
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Definici	definice	k1gFnSc4	definice
lze	lze	k6eAd1	lze
rozšířit	rozšířit	k5eAaPmF	rozšířit
i	i	k9	i
na	na	k7c4	na
záporné	záporný	k2eAgFnPc4d1	záporná
a	a	k8xC	a
"	"	kIx"	"
<g/>
necelé	celý	k2eNgInPc4d1	necelý
<g/>
"	"	kIx"	"
řády	řád	k1gInPc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
přirozené	přirozený	k2eAgNnSc1d1	přirozené
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
ztotožnit	ztotožnit	k5eAaPmF	ztotožnit
minus	minus	k1gInSc4	minus
první	první	k4xOgFnSc4	první
derivaci	derivace	k1gFnSc4	derivace
s	s	k7c7	s
integrálem	integrál	k1gInSc7	integrál
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
−	−	k?	−
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}	}	kIx)	}
a	a	k8xC	a
derivaci	derivace	k1gFnSc4	derivace
minus	minus	k1gNnSc2	minus
n-tého	ného	k2eAgInSc2d1	n-tého
řádu	řád	k1gInSc2	řád
s	s	k7c7	s
výrazem	výraz	k1gInSc7	výraz
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
−	−	k?	−
n	n	k0	n
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
(	(	kIx(	(
n	n	k0	n
−	−	k?	−
1	[number]	k4	1
)	)	kIx)	)
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
−	−	k?	−
t	t	k?	t
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
n	n	k0	n
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
-n	-n	k?	-n
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x-t	x	k?	x-t
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}	}	kIx)	}
,	,	kIx,	,
neboť	neboť	k8xC	neboť
prvním	první	k4xOgInSc7	první
resp.	resp.	kA	resp.
n-tým	ní	k1gMnPc3	n-tí
derivováním	derivování	k1gNnSc7	derivování
dostaneme	dostat	k5eAaPmIp1nP	dostat
základní	základní	k2eAgFnSc4d1	základní
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nepřirozené	přirozený	k2eNgNnSc4d1	nepřirozené
s	s	k7c7	s
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
pak	pak	k6eAd1	pak
jen	jen	k9	jen
faktoriál	faktoriál	k1gInSc4	faktoriál
nahradíme	nahradit	k5eAaPmIp1nP	nahradit
gama	gama	k1gNnPc4	gama
funkcí	funkce	k1gFnPc2	funkce
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
−	−	k?	−
s	s	k7c7	s
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
Γ	Γ	k?	Γ
(	(	kIx(	(
s	s	k7c7	s
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
x	x	k?	x
−	−	k?	−
t	t	k?	t
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
s	s	k7c7	s
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
-s	-s	k?	-s
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Gamma	Gamma	k1gFnSc1	Gamma
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x-t	x	k?	x-t
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
s-	s-	k?	s-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Derivace	derivace	k1gFnSc1	derivace
reálného	reálný	k2eAgInSc2d1	reálný
r-tého	rého	k2eAgInSc2d1	r-tého
řádu	řád	k1gInSc2	řád
(	(	kIx(	(
<g/>
r	r	kA	r
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
r	r	kA	r
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
r	r	kA	r
−	−	k?	−
n	n	k0	n
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
r	r	kA	r
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}	}}}	k?	}}}
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
r-n	r	k?	r-n
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k9	kde
n	n	k0	n
je	být	k5eAaImIp3nS	být
nejnižší	nízký	k2eAgNnSc4d3	nejnižší
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
r	r	kA	r
<g/>
;	;	kIx,	;
vše	všechen	k3xTgNnSc1	všechen
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
"	"	kIx"	"
<g/>
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
<g/>
"	"	kIx"	"
derivace	derivace	k1gFnSc1	derivace
záporného	záporný	k2eAgInSc2d1	záporný
(	(	kIx(	(
<g/>
r-n	r	k?	r-n
<g/>
)	)	kIx)	)
<g/>
-tého	é	k1gMnSc2	-té
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Nejnižší	nízký	k2eAgFnPc1d3	nejnižší
n	n	k0	n
se	se	k3xPyFc4	se
bere	brát	k5eAaImIp3nS	brát
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatímco	zatímco	k8xS	zatímco
pro	pro	k7c4	pro
záporné	záporný	k2eAgInPc4d1	záporný
řády	řád	k1gInPc4	řád
je	být	k5eAaImIp3nS	být
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
komutativnost	komutativnost	k1gFnSc1	komutativnost
a	a	k8xC	a
aditivnost	aditivnost	k1gFnSc1	aditivnost
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
dvě	dva	k4xCgFnPc4	dva
postupně	postupně	k6eAd1	postupně
provedené	provedený	k2eAgFnPc4d1	provedená
derivace	derivace	k1gFnPc4	derivace
záporného	záporný	k2eAgInSc2d1	záporný
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
existují	existovat	k5eAaImIp3nP	existovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ekvivalentní	ekvivalentní	k2eAgFnSc6d1	ekvivalentní
jedné	jeden	k4xCgFnSc6	jeden
derivaci	derivace	k1gFnSc6	derivace
s	s	k7c7	s
řádem	řád	k1gInSc7	řád
daným	daný	k2eAgInSc7d1	daný
součtem	součet	k1gInSc7	součet
obou	dva	k4xCgInPc2	dva
řádů	řád	k1gInPc2	řád
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
pořadí	pořadí	k1gNnSc4	pořadí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kladné	kladný	k2eAgInPc4d1	kladný
řády	řád	k1gInPc4	řád
to	ten	k3xDgNnSc1	ten
obecně	obecně	k6eAd1	obecně
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Principiálně	principiálně	k6eAd1	principiálně
základní	základní	k2eAgFnSc7d1	základní
technikou	technika	k1gFnSc7	technika
je	být	k5eAaImIp3nS	být
výpočet	výpočet	k1gInSc4	výpočet
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
definice	definice	k1gFnSc2	definice
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
dosazením	dosazení	k1gNnSc7	dosazení
příslušné	příslušný	k2eAgFnSc2d1	příslušná
funkce	funkce	k1gFnSc2	funkce
do	do	k7c2	do
definující	definující	k2eAgFnSc2d1	definující
limity	limita	k1gFnSc2	limita
a	a	k8xC	a
výpočtem	výpočet	k1gInSc7	výpočet
této	tento	k3xDgFnSc2	tento
limity	limita	k1gFnSc2	limita
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
je	být	k5eAaImIp3nS	být
však	však	k9	však
obvykle	obvykle	k6eAd1	obvykle
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
velice	velice	k6eAd1	velice
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
funkce	funkce	k1gFnPc4	funkce
<g/>
)	)	kIx)	)
dosti	dosti	k6eAd1	dosti
komplikovaný	komplikovaný	k2eAgInSc4d1	komplikovaný
a	a	k8xC	a
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgInSc2	ten
se	se	k3xPyFc4	se
derivace	derivace	k1gFnSc1	derivace
funkcí	funkce	k1gFnPc2	funkce
počítají	počítat	k5eAaImIp3nP	počítat
ze	z	k7c2	z
známých	známý	k2eAgFnPc2d1	známá
derivací	derivace	k1gFnPc2	derivace
několika	několik	k4yIc2	několik
základních	základní	k2eAgFnPc2d1	základní
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
jednoduchých	jednoduchý	k2eAgNnPc2d1	jednoduché
algebraických	algebraický	k2eAgNnPc2d1	algebraické
pravidel	pravidlo	k1gNnPc2	pravidlo
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
skládání	skládání	k1gNnSc4	skládání
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
úpravy	úprava	k1gFnPc4	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Derivace	derivace	k1gFnSc2	derivace
elementárních	elementární	k2eAgFnPc2d1	elementární
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
známých	známý	k2eAgFnPc2d1	známá
derivací	derivace	k1gFnPc2	derivace
elementárních	elementární	k2eAgFnPc2d1	elementární
funkcí	funkce	k1gFnPc2	funkce
se	se	k3xPyFc4	se
derivace	derivace	k1gFnSc1	derivace
složitějších	složitý	k2eAgFnPc2d2	složitější
funkcí	funkce	k1gFnPc2	funkce
sestavují	sestavovat	k5eAaImIp3nP	sestavovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
složitější	složitý	k2eAgFnSc1d2	složitější
funkce	funkce	k1gFnSc1	funkce
rozloží	rozložit	k5eAaPmIp3nS	rozložit
na	na	k7c4	na
jednodušší	jednoduchý	k2eAgFnPc4d2	jednodušší
pomocí	pomocí	k7c2	pomocí
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
algebraických	algebraický	k2eAgFnPc2d1	algebraická
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
derivací	derivace	k1gFnPc2	derivace
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
Linearita	linearita	k1gFnSc1	linearita
derivace	derivace	k1gFnSc1	derivace
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
f	f	k?	f
+	+	kIx~	+
b	b	k?	b
g	g	kA	g
)	)	kIx)	)
:	:	kIx,	:
'	'	kIx"	'
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
=	=	kIx~	=
a	a	k8xC	a
:	:	kIx,	:
f	f	k?	f
'	'	kIx"	'
:	:	kIx,	:
+	+	kIx~	+
b	b	k?	b
:	:	kIx,	:
g	g	kA	g
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	leftum	k1gNnPc2	leftum
<g/>
(	(	kIx(	(
<g/>
af	af	k?	af
<g/>
+	+	kIx~	+
<g/>
bg	bg	k?	bg
<g/>
\	\	kIx~	\
<g/>
right	right	k1gMnSc1	right
<g/>
)	)	kIx)	)
<g/>
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
af	af	k?	af
<g/>
'	'	kIx"	'
<g/>
+	+	kIx~	+
<g/>
bg	bg	k?	bg
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
'	'	kIx"	'
<g/>
}	}	kIx)	}
pro	pro	k7c4	pro
libovolné	libovolný	k2eAgFnPc4d1	libovolná
funkce	funkce	k1gFnPc4	funkce
f	f	k?	f
<g/>
,	,	kIx,	,
g	g	kA	g
a	a	k8xC	a
konstanty	konstanta	k1gFnSc2	konstanta
a	a	k8xC	a
<g/>
,	,	kIx,	,
b.	b.	k?	b.
Speciálně	speciálně	k6eAd1	speciálně
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
f	f	k?	f
)	)	kIx)	)
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
=	=	kIx~	=
a	a	k8xC	a
:	:	kIx,	:
f	f	k?	f
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	leftum	k1gNnPc2	leftum
<g/>
(	(	kIx(	(
<g/>
af	af	k?	af
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
af	af	k?	af
<g/>
'	'	kIx"	'
<g/>
}	}	kIx)	}
a	a	k8xC	a
také	také	k9	také
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
f	f	k?	f
+	+	kIx~	+
g	g	kA	g
)	)	kIx)	)
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
f	f	k?	f
'	'	kIx"	'
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
g	g	kA	g
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	leftum	k1gNnPc2	leftum
<g/>
(	(	kIx(	(
<g/>
f	f	k?	f
<g/>
+	+	kIx~	+
<g/>
g	g	kA	g
<g/>
\	\	kIx~	\
<g/>
right	right	k1gMnSc1	right
<g/>
)	)	kIx)	)
<g/>
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
f	f	k?	f
<g/>
'	'	kIx"	'
<g/>
+	+	kIx~	+
<g/>
g	g	kA	g
<g/>
'	'	kIx"	'
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Derivace	derivace	k1gFnSc1	derivace
součinu	součin	k1gInSc2	součin
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
f	f	k?	f
g	g	kA	g
)	)	kIx)	)
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
f	f	k?	f
'	'	kIx"	'
:	:	kIx,	:
g	g	kA	g
+	+	kIx~	+
f	f	k?	f
:	:	kIx,	:
g	g	kA	g
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	leftum	k1gNnPc2	leftum
<g/>
(	(	kIx(	(
<g/>
fg	fg	k?	fg
<g/>
\	\	kIx~	\
<g/>
right	right	k1gMnSc1	right
<g/>
)	)	kIx)	)
<g/>
'	'	kIx"	'
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
<g/>
f	f	k?	f
<g/>
'	'	kIx"	'
<g/>
g	g	kA	g
<g/>
+	+	kIx~	+
<g/>
fg	fg	k?	fg
<g/>
'	'	kIx"	'
<g/>
}	}	kIx)	}
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
funkce	funkce	k1gFnPc4	funkce
f	f	k?	f
<g/>
,	,	kIx,	,
g.	g.	k?	g.
Derivace	derivace	k1gFnSc1	derivace
podílu	podíl	k1gInSc2	podíl
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
g	g	kA	g
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
'	'	kIx"	'
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
g	g	kA	g
−	−	k?	−
f	f	k?	f
:	:	kIx,	:
g	g	kA	g
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
'	'	kIx"	'
<g/>
g-fg	gg	k1gMnSc1	g-fg
<g/>
'	'	kIx"	'
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
funkce	funkce	k1gFnPc4	funkce
f	f	k?	f
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
g	g	kA	g
≠	≠	k?	≠
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Derivace	derivace	k1gFnSc1	derivace
složené	složený	k2eAgFnSc2d1	složená
funkce	funkce	k1gFnSc2	funkce
<g/>
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
=	=	kIx~	=
h	h	k?	h
:	:	kIx,	:
(	(	kIx(	(
g	g	kA	g
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
right	right	k1gMnSc1	right
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
right	right	k1gMnSc1	right
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
'	'	kIx"	'
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
h	h	k?	h
'	'	kIx"	'
:	:	kIx,	:
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s hack="1">
g	g	kA	g
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
)	)	kIx)	)
⋅	⋅	k?	⋅
:	:	kIx,	:
g	g	kA	g
'	'	kIx"	'
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
h	h	k?	h
<g/>
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
))	))	k?	))
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
g	g	kA	g
<g/>
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Derivace	derivace	k1gFnSc1	derivace
inverzní	inverzní	k2eAgFnSc2d1	inverzní
funkce	funkce	k1gFnSc2	funkce
<g/>
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
i	i	k9	i
f	f	k?	f
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
obě	dva	k4xCgFnPc1	dva
diferencovatelné	diferencovatelný	k2eAgFnPc1d1	diferencovatelná
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Δ	Δ	k?	Δ
≠	≠	k?	≠
0	[number]	k4	0
pokud	pokud	k8xS	pokud
Δ	Δ	k?	Δ
≠	≠	k?	≠
0	[number]	k4	0
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Derivace	derivace	k1gFnPc1	derivace
jedné	jeden	k4xCgFnSc2	jeden
proměnné	proměnná	k1gFnSc2	proměnná
vůči	vůči	k7c3	vůči
druhé	druhý	k4xOgFnSc6	druhý
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
obě	dva	k4xCgFnPc1	dva
jsou	být	k5eAaImIp3nP	být
funkcí	funkce	k1gFnSc7	funkce
třetí	třetí	k4xOgFnSc2	třetí
proměnné	proměnná	k1gFnSc2	proměnná
<g/>
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
x	x	k?	x
=	=	kIx~	=
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
a	a	k8xC	a
y	y	k?	y
=	=	kIx~	=
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Derivace	derivace	k1gFnSc1	derivace
implicitní	implicitní	k2eAgFnSc2d1	implicitní
funkce	funkce	k1gFnSc2	funkce
<g/>
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
)	)	kIx)	)
je	on	k3xPp3gFnPc4	on
implicitní	implicitní	k2eAgFnPc4d1	implicitní
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
∂	∂	k?	∂
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
f	f	k?	f
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
f	f	k?	f
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
y	y	k?	y
<g/>
}}}}	}}}}	k?	}}}}
.	.	kIx.	.
</s>
<s>
Derivace	derivace	k1gFnSc1	derivace
parametricky	parametricky	k6eAd1	parametricky
zadané	zadaný	k2eAgFnSc2d1	zadaná
funkce	funkce	k1gFnSc2	funkce
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
funkce	funkce	k1gFnSc1	funkce
vyjádřena	vyjádřen	k2eAgFnSc1d1	vyjádřena
parametrickými	parametrický	k2eAgFnPc7d1	parametrická
rovnicemi	rovnice	k1gFnPc7	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
φ	φ	k?	φ
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
,	,	kIx,	,
y	y	k?	y
=	=	kIx~	=
ψ	ψ	k?	ψ
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
varphi	varphi	k1gNnSc1	varphi
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
její	její	k3xOp3gMnPc4	její
derivace	derivace	k1gFnSc1	derivace
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
ψ	ψ	k?	ψ
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
φ	φ	k?	φ
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
ψ	ψ	k?	ψ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
φ	φ	k?	φ
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Z	z	k7c2	z
některých	některý	k3yIgNnPc2	některý
předchozích	předchozí	k2eAgNnPc2d1	předchozí
pravidel	pravidlo	k1gNnPc2	pravidlo
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Leibnizova	Leibnizův	k2eAgFnSc1d1	Leibnizova
notace	notace	k1gFnSc1	notace
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
některé	některý	k3yIgFnPc4	některý
manipulace	manipulace	k1gFnPc4	manipulace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
připomínají	připomínat	k5eAaImIp3nP	připomínat
např.	např.	kA	např.
krácení	krácení	k1gNnSc1	krácení
zlomku	zlomek	k1gInSc2	zlomek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
třeba	třeba	k6eAd1	třeba
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
jen	jen	k9	jen
o	o	k7c4	o
symbolické	symbolický	k2eAgFnPc4d1	symbolická
manipulace	manipulace	k1gFnPc4	manipulace
<g/>
,	,	kIx,	,
s	s	k7c7	s
krácením	krácení	k1gNnSc7	krácení
zlomku	zlomek	k1gInSc2	zlomek
nemající	mající	k2eNgNnSc1d1	nemající
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
pak	pak	k6eAd1	pak
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
"	"	kIx"	"
<g/>
krátit	krátit	k5eAaImF	krátit
d	d	k?	d
<g/>
"	"	kIx"	"
stylem	styl	k1gInSc7	styl
dx	dx	k?	dx
<g/>
/	/	kIx~	/
<g/>
dy	dy	k?	dy
=	=	kIx~	=
x	x	k?	x
<g/>
/	/	kIx~	/
<g/>
y.	y.	k?	y.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
sin	sin	kA	sin
:	:	kIx,	:
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
n	n	k0	n
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
sin	sin	kA	sin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
n	n	k0	n
π	π	k?	π
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
cos	cos	kA	cos
:	:	kIx,	:
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s hack="1">
n	n	k0	n
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
cos	cos	kA	cos
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
π	π	k?	π
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
n	n	k0	n
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
m	m	kA	m
(	(	kIx(	(
m	m	kA	m
−	−	k?	−
1	[number]	k4	1
)	)	kIx)	)
⋯	⋯	k?	⋯
(	(	kIx(	(
m	m	kA	m
−	−	k?	−
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
m	m	kA	m
−	−	k?	−
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
m	m	kA	m
<g/>
(	(	kIx(	(
<g/>
m-	m-	k?	m-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k1gInSc1	cdots
(	(	kIx(	(
<g/>
m-n	m	k?	m-n
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
m-n	m	k?	m-n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
}	}	kIx)	}
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
>	>	kIx)	>
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
,	,	kIx,	,
n	n	k0	n
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
a	a	k8xC	a
m	m	kA	m
libovolné	libovolný	k2eAgInPc1d1	libovolný
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
n	n	k0	n
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
ln	ln	k?	ln
:	:	kIx,	:
a	a	k8xC	a
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
}	}	kIx)	}
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
>	>	kIx)	>
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
n	n	k0	n
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
ln	ln	k?	ln
:	:	kIx,	:
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
n	n	k0	n
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
−	−	k?	−
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
n	n	k0	n
−	−	k?	−
1	[number]	k4	1
)	)	kIx)	)
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
}	}	kIx)	}
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
>	>	kIx)	>
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
ln	ln	k?	ln
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
g	g	kA	g
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
}	}	kIx)	}
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
>	>	kIx)	>
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
[	[	kIx(	[
ln	ln	k?	ln
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
x	x	k?	x
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
[	[	kIx(	[
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
{	{	kIx(	{
<g/>
|	|	kIx~	|
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}}}}	}}}}	k?	}}}}
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
≠	≠	k?	≠
0	[number]	k4	0
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
a	a	k8xC	a
>	>	kIx)	>
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
0	[number]	k4	0
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
a	a	k8xC	a
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
arcsin	arcsin	k1gInSc1	arcsin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
{	{	kIx(	{
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
pro	pro	k7c4	pro
:	:	kIx,	:
a	a	k8xC	a
>	>	kIx)	>
0	[number]	k4	0
,	,	kIx,	,
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
<	<	kIx(	<
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
pro	pro	k7c4	pro
:	:	kIx,	:
a	a	k8xC	a
<	<	kIx(	<
0	[number]	k4	0
,	,	kIx,	,
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
>	>	kIx)	>
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
arcsin	arcsin	k1gInSc1	arcsin
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
\	\	kIx~	\
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k2eAgInSc4d1	begin
<g/>
{	{	kIx(	{
<g/>
matrix	matrix	k1gInSc4	matrix
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-x	-x	k?	-x
<g/>
<g />
.	.	kIx.	.
</s>
<s>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
<g/>
,	,	kIx,	,
<g/>
&	&	k?	&
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
pro	pro	k7c4	pro
}}	}}	k?	}}
<g/>
a	a	k8xC	a
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
<g/>
|	|	kIx~	|
<g/>
x	x	k?	x
<g/>
|	|	kIx~	|
<g/>
<	<	kIx(	<
<g/>
a	a	k8xC	a
<g/>
\\	\\	k?	\\
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-x	-x	k?	-x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
<g/>
,	,	kIx,	,
<g/>
&	&	k?	&
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
pro	pro	k7c4	pro
}}	}}	k?	}}
<g/>
a	a	k8xC	a
<g/>
<	<	kIx(	<
<g/>
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
<g/>
|	|	kIx~	|
<g/>
x	x	k?	x
<g/>
|	|	kIx~	|
<g/>
>	>	kIx)	>
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
matrix	matrix	k1gInSc1	matrix
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
[	[	kIx(	[
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
[	[	kIx(	[
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
ln	ln	k?	ln
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
(	(	kIx(	(
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
[	[	kIx(	[
<g/>
g	g	kA	g
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
:	:	kIx,	:
Derivaci	derivace	k1gFnSc4	derivace
součinu	součin	k1gInSc2	součin
n	n	k0	n
funkcí	funkce	k1gFnSc7	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
i	i	k8xC	i
<g/>
}}	}}	k?	}}
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xC	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
⋯	⋯	k?	⋯
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
⋯	⋯	k?	⋯
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s hack="1">
⋯	⋯	k?	⋯
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdots	cdotsit	k5eAaPmRp2nS	cdotsit
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s>
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdots	cdotsit	k5eAaPmRp2nS	cdotsit
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}}	}}}	k?	}}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k6eAd1	cdots
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
:	:	kIx,	:
Pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
n-té	ný	k2eAgFnSc2d1	n-tý
derivace	derivace	k1gFnSc2	derivace
součinu	součin	k1gInSc2	součin
dvou	dva	k4xCgMnPc6	dva
funkci	funkce	k1gFnSc6	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
,	,	kIx,	,
g	g	kA	g
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
tzv.	tzv.	kA	tzv.
<g />
.	.	kIx.	.
</s>
<s hack="1">
Leibnizův	Leibnizův	k2eAgInSc4d1	Leibnizův
vzorec	vzorec	k1gInSc4	vzorec
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
f	f	k?	f
⋅	⋅	k?	⋅
g	g	kA	g
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
n	n	k0	n
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
n	n	k0	n
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
(	(	kIx(	(
0	[number]	k4	0
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
n	n	k0	n
−	−	k?	−
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
(	(	kIx(	(
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
n	n	k0	n
−	−	k?	−
2	[number]	k4	2
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
(	(	kIx(	(
2	[number]	k4	2
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
⋯	⋯	k?	⋯
+	+	kIx~	+
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
0	[number]	k4	0
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
(	(	kIx(	(
n	n	k0	n
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
k	k	k7c3	k
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
n	n	k0	n
−	−	k?	−
k	k	k7c3	k
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
(	(	kIx(	(
k	k	k7c3	k
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
f	f	k?	f
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
g	g	kA	g
<g/>
<g />
.	.	kIx.	.
</s>
<s>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
\	\	kIx~	\
<g/>
choose	choosa	k1gFnSc6	choosa
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
g	g	kA	g
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
\	\	kIx~	\
<g/>
choose	choosa	k1gFnSc6	choosa
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
n-	n-	k?	n-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
g	g	kA	g
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
cdots	cdots	k1gInSc1	cdots
+	+	kIx~	+
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
g	g	kA	g
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
\	\	kIx~	\
<g/>
choose	choosa	k1gFnSc6	choosa
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
n-k	n	k?	n-k
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
g	g	kA	g
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
\	\	kIx~	\
<g/>
choose	choosa	k1gFnSc6	choosa
k	k	k7c3	k
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
binomické	binomický	k2eAgInPc4d1	binomický
koeficienty	koeficient	k1gInPc4	koeficient
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
0	[number]	k4	0
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
f	f	k?	f
,	,	kIx,	,
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
(	(	kIx(	(
2	[number]	k4	2
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
'	'	kIx"	'
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
f	f	k?	f
<g/>
,	,	kIx,	,
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
,	,	kIx,	,
atd.	atd.	kA	atd.
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
3	[number]	k4	3
<g/>
;	;	kIx,	;
f	f	k?	f
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
0	[number]	k4	0
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
x	x	k?	x
<g/>
;	;	kIx,	;
f	f	k?	f
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
1	[number]	k4	1
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
2	[number]	k4	2
<g/>
x	x	k?	x
<g/>
;	;	kIx,	;
f	f	k?	f
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
=	=	kIx~	=
2	[number]	k4	2
·	·	k?	·
1	[number]	k4	1
=	=	kIx~	=
2	[number]	k4	2
<g/>
.	.	kIx.	.
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
5	[number]	k4	5
<g/>
x3	x3	k4	x3
<g/>
;	;	kIx,	;
f	f	k?	f
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
15	[number]	k4	15
<g/>
x2	x2	k4	x2
<g/>
;	;	kIx,	;
f	f	k?	f
<g/>
''	''	k?	''
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
30	[number]	k4	30
<g/>
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
ex	ex	k6eAd1	ex
<g/>
;	;	kIx,	;
f	f	k?	f
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
ex	ex	k6eAd1	ex
<g/>
.	.	kIx.	.
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
ln	ln	k?	ln
x	x	k?	x
<g/>
;	;	kIx,	;
f	f	k?	f
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
x	x	k?	x
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
x3	x3	k4	x3
+	+	kIx~	+
2	[number]	k4	2
<g/>
x2	x2	k4	x2
−	−	k?	−
5	[number]	k4	5
<g/>
x	x	k?	x
+	+	kIx~	+
7	[number]	k4	7
<g/>
;	;	kIx,	;
f	f	k?	f
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
3	[number]	k4	3
<g/>
x2	x2	k4	x2
+	+	kIx~	+
4	[number]	k4	4
<g/>
x	x	k?	x
−	−	k?	−
5	[number]	k4	5
<g/>
.	.	kIx.	.
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
sin	sin	kA	sin
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
·	·	k?	·
cos	cos	kA	cos
x	x	k?	x
<g/>
;	;	kIx,	;
f	f	k?	f
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
cos2	cos2	k4	cos2
x	x	k?	x
−	−	k?	−
sin2	sin2	k4	sin2
x	x	k?	x
(	(	kIx(	(
<g/>
=	=	kIx~	=
cos	cos	kA	cos
2	[number]	k4	2
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
arcsin	arcsin	k1gInSc1	arcsin
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
arcsin	arcsin	k1gInSc1	arcsin
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
;	;	kIx,	;
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
f	f	k?	f
'	'	kIx"	'
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
arcsin	arcsin	k1gInSc1	arcsin
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
1	[number]	k4	1
−	−	k?	−
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
arcsin	arcsin	k1gInSc1	arcsin
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-x	-x	k?	-x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}	}}}}}	k?	}}}}}
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
e	e	k0	e
:	:	kIx,	:
x	x	k?	x
ln	ln	k?	ln
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
ln	ln	k?	ln
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
;	;	kIx,	;
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
'	'	kIx"	'
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
x	x	k?	x
ln	ln	k?	ln
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
:	:	kIx,	:
(	(	kIx(	(
1	[number]	k4	1
⋅	⋅	k?	⋅
ln	ln	k?	ln
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
+	+	kIx~	+
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
:	:	kIx,	:
(	(	kIx(	(
ln	ln	k?	ln
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
+	+	kIx~	+
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
cdot	cdota	k1gFnPc2	cdota
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
ln	ln	k?	ln
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
x	x	k?	x
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
derivace	derivace	k1gFnSc2	derivace
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
obrovském	obrovský	k2eAgNnSc6d1	obrovské
množství	množství	k1gNnSc6	množství
situací	situace	k1gFnPc2	situace
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
samé	samý	k3xTgNnSc1	samý
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
v	v	k7c6	v
jejích	její	k3xOp3gFnPc6	její
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Extrém	extrém	k1gInSc1	extrém
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
daná	daný	k2eAgFnSc1d1	daná
diferencovatelná	diferencovatelný	k2eAgFnSc1d1	diferencovatelná
funkce	funkce	k1gFnSc1	funkce
nějaký	nějaký	k3yIgInSc4	nějaký
lokální	lokální	k2eAgInSc4d1	lokální
extrém	extrém	k1gInSc4	extrém
(	(	kIx(	(
<g/>
lokální	lokální	k2eAgNnSc4d1	lokální
maximum	maximum	k1gNnSc4	maximum
či	či	k8xC	či
minimum	minimum	k1gNnSc4	minimum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
tečna	tečna	k1gFnSc1	tečna
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bodě	bod	k1gInSc6	bod
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vodorovná	vodorovný	k2eAgFnSc1d1	vodorovná
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
derivace	derivace	k1gFnSc1	derivace
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bodě	bod	k1gInSc6	bod
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Pokud	pokud	k8xS	pokud
funkce	funkce	k1gFnSc2	funkce
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
nějakých	nějaký	k3yIgFnPc2	nějaký
bodech	bod	k1gInPc6	bod
tečnu	tečna	k1gFnSc4	tečna
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
derivaci	derivace	k1gFnSc4	derivace
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
derivace	derivace	k1gFnSc1	derivace
o	o	k7c6	o
takových	takový	k3xDgInPc6	takový
bodech	bod	k1gInPc6	bod
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nic	nic	k3yNnSc1	nic
prozradit	prozradit	k5eAaPmF	prozradit
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Pokud	pokud	k8xS	pokud
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bodě	bod	k1gInSc6	bod
lze	lze	k6eAd1	lze
spočítat	spočítat	k5eAaPmF	spočítat
i	i	k9	i
druhou	druhý	k4xOgFnSc4	druhý
derivaci	derivace	k1gFnSc4	derivace
<g/>
,	,	kIx,	,
prozradí	prozradit	k5eAaPmIp3nP	prozradit
její	její	k3xOp3gNnSc4	její
znaménko	znaménko	k1gNnSc4	znaménko
<g/>
,	,	kIx,	,
o	o	k7c4	o
jaký	jaký	k3yRgInSc4	jaký
extrém	extrém	k1gInSc4	extrém
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
:	:	kIx,	:
V	v	k7c6	v
bodech	bod	k1gInPc6	bod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
derivace	derivace	k1gFnSc1	derivace
nula	nula	k1gFnSc1	nula
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
derivace	derivace	k1gFnSc1	derivace
je	být	k5eAaImIp3nS	být
kladná	kladný	k2eAgFnSc1d1	kladná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
lokální	lokální	k2eAgNnSc1d1	lokální
minimum	minimum	k1gNnSc1	minimum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bodech	bod	k1gInPc6	bod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
derivace	derivace	k1gFnSc1	derivace
nula	nula	k1gFnSc1	nula
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
derivace	derivace	k1gFnSc1	derivace
je	být	k5eAaImIp3nS	být
záporná	záporný	k2eAgFnSc1d1	záporná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
lokální	lokální	k2eAgNnSc1d1	lokální
maximum	maximum	k1gNnSc1	maximum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bodech	bod	k1gInPc6	bod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
derivace	derivace	k1gFnSc1	derivace
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tzv.	tzv.	kA	tzv.
stacionární	stacionární	k2eAgInSc1d1	stacionární
bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
a	a	k8xC	a
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
extrémem	extrém	k1gInSc7	extrém
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
bodech	bod	k1gInPc6	bod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
funkce	funkce	k1gFnSc1	funkce
nemá	mít	k5eNaImIp3nS	mít
první	první	k4xOgFnSc4	první
či	či	k8xC	či
druhou	druhý	k4xOgFnSc4	druhý
derivaci	derivace	k1gFnSc4	derivace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
použít	použít	k5eAaPmF	použít
jiná	jiný	k2eAgNnPc4d1	jiné
kritéria	kritérion	k1gNnPc4	kritérion
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Alternativou	alternativa	k1gFnSc7	alternativa
k	k	k7c3	k
rozlišení	rozlišení	k1gNnSc3	rozlišení
pomocí	pomocí	k7c2	pomocí
druhé	druhý	k4xOgFnSc2	druhý
derivace	derivace	k1gFnSc2	derivace
je	být	k5eAaImIp3nS	být
znaménko	znaménko	k1gNnSc4	znaménko
první	první	k4xOgFnSc2	první
derivace	derivace	k1gFnSc2	derivace
<g/>
:	:	kIx,	:
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
funkce	funkce	k1gFnPc4	funkce
lokální	lokální	k2eAgFnSc1d1	lokální
extrém	extrém	k1gInSc1	extrém
<g/>
,	,	kIx,	,
mění	měnit	k5eAaImIp3nS	měnit
první	první	k4xOgFnSc1	první
derivace	derivace	k1gFnSc1	derivace
znaménko	znaménko	k1gNnSc4	znaménko
<g/>
:	:	kIx,	:
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
nějaký	nějaký	k3yIgInSc1	nějaký
bod	bod	k1gInSc1	bod
lokálním	lokální	k2eAgNnSc7d1	lokální
minimem	minimum	k1gNnSc7	minimum
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
levém	levý	k2eAgNnSc6d1	levé
okolí	okolí	k1gNnSc6	okolí
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
derivace	derivace	k1gFnSc1	derivace
záporná	záporný	k2eAgFnSc1d1	záporná
a	a	k8xC	a
v	v	k7c6	v
pravém	pravý	k2eAgNnSc6d1	pravé
okolí	okolí	k1gNnSc6	okolí
kladná	kladný	k2eAgFnSc1d1	kladná
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
levém	levý	k2eAgNnSc6d1	levé
okolí	okolí	k1gNnSc6	okolí
lokálního	lokální	k2eAgNnSc2d1	lokální
maxima	maximum	k1gNnSc2	maximum
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
derivace	derivace	k1gFnSc1	derivace
kladná	kladný	k2eAgFnSc1d1	kladná
a	a	k8xC	a
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
záporná	záporný	k2eAgNnPc4d1	záporné
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
kritéria	kritérion	k1gNnPc1	kritérion
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
optimalizačních	optimalizační	k2eAgFnPc6d1	optimalizační
úlohách	úloha	k1gFnPc6	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
požadováno	požadován	k2eAgNnSc1d1	požadováno
najít	najít	k5eAaPmF	najít
obdélník	obdélník	k1gInSc4	obdélník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
při	při	k7c6	při
zadaném	zadaný	k2eAgInSc6d1	zadaný
obvodu	obvod	k1gInSc6	obvod
má	mít	k5eAaImIp3nS	mít
maximální	maximální	k2eAgFnSc4d1	maximální
plochu	plocha	k1gFnSc4	plocha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
najít	najít	k5eAaPmF	najít
maximum	maximum	k1gNnSc4	maximum
funkce	funkce	k1gFnSc2	funkce
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
x	x	k?	x
⋅	⋅	k?	⋅
(	(	kIx(	(
<g/>
o	o	k7c4	o
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
−	−	k?	−
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
derivací	derivace	k1gFnSc7	derivace
je	být	k5eAaImIp3nS	být
funkce	funkce	k1gFnSc1	funkce
f	f	k?	f
<g/>
'	'	kIx"	'
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
o	o	k7c6	o
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
−	−	k?	−
2	[number]	k4	2
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
nulová	nulový	k2eAgFnSc1d1	nulová
pro	pro	k7c4	pro
x	x	k?	x
=	=	kIx~	=
o	o	k7c4	o
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
derivace	derivace	k1gFnSc1	derivace
funkce	funkce	k1gFnSc2	funkce
f	f	k?	f
je	být	k5eAaImIp3nS	být
f	f	k?	f
<g/>
''	''	k?	''
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
−	−	k?	−
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
je	být	k5eAaImIp3nS	být
všude	všude	k6eAd1	všude
záporná	záporný	k2eAgFnSc1d1	záporná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bodě	bod	k1gInSc6	bod
x	x	k?	x
=	=	kIx~	=
o	o	k7c4	o
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
funkce	funkce	k1gFnSc1	funkce
f	f	k?	f
maximum	maximum	k1gNnSc1	maximum
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
obdélníků	obdélník	k1gInPc2	obdélník
o	o	k7c6	o
zadaném	zadaný	k2eAgInSc6d1	zadaný
obvodu	obvod	k1gInSc6	obvod
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc1d3	veliký
obsah	obsah	k1gInSc1	obsah
ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
všechny	všechen	k3xTgFnPc4	všechen
čtyři	čtyři	k4xCgFnPc4	čtyři
strany	strana	k1gFnPc4	strana
stejně	stejně	k6eAd1	stejně
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
čtverec	čtverec	k1gInSc1	čtverec
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Průběh	průběh	k1gInSc1	průběh
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgInSc1d1	předchozí
odstavec	odstavec	k1gInSc1	odstavec
popisuje	popisovat	k5eAaImIp3nS	popisovat
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
funkci	funkce	k1gFnSc4	funkce
nalézt	nalézt	k5eAaBmF	nalézt
její	její	k3xOp3gInPc4	její
lokální	lokální	k2eAgInPc4d1	lokální
extrémy	extrém	k1gInPc4	extrém
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
kromě	kromě	k7c2	kromě
optimalizačních	optimalizační	k2eAgFnPc2d1	optimalizační
úloh	úloha	k1gFnPc2	úloha
sloužit	sloužit	k5eAaImF	sloužit
také	také	k9	také
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
přehledu	přehled	k1gInSc2	přehled
o	o	k7c4	o
chování	chování	k1gNnSc4	chování
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
ručním	ruční	k2eAgInSc6d1	ruční
náčrtu	náčrt	k1gInSc6	náčrt
jejího	její	k3xOp3gInSc2	její
grafu	graf	k1gInSc2	graf
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
analýzy	analýza	k1gFnSc2	analýza
extrémů	extrém	k1gInPc2	extrém
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
derivací	derivace	k1gFnSc7	derivace
k	k	k7c3	k
následujícím	následující	k2eAgFnPc3d1	následující
pozorováním	pozorování	k1gNnSc7	pozorování
<g/>
:	:	kIx,	:
V	v	k7c6	v
bodech	bod	k1gInPc6	bod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
derivace	derivace	k1gFnSc1	derivace
kladná	kladný	k2eAgFnSc1d1	kladná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
funkce	funkce	k1gFnSc1	funkce
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bodech	bod	k1gInPc6	bod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
derivace	derivace	k1gFnSc1	derivace
záporná	záporný	k2eAgFnSc1d1	záporná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
funkce	funkce	k1gFnSc1	funkce
klesající	klesající	k2eAgFnSc1d1	klesající
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bodech	bod	k1gInPc6	bod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
derivace	derivace	k1gFnSc1	derivace
kladná	kladný	k2eAgFnSc1d1	kladná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
funkce	funkce	k1gFnSc1	funkce
konvexní	konvexní	k2eAgFnSc1d1	konvexní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bodech	bod	k1gInPc6	bod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
derivace	derivace	k1gFnSc1	derivace
záporná	záporný	k2eAgFnSc1d1	záporná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
funkce	funkce	k1gFnSc1	funkce
konkávní	konkávní	k2eAgFnSc1d1	konkávní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bodech	bod	k1gInPc6	bod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
derivace	derivace	k1gFnSc1	derivace
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
inflexní	inflexní	k2eAgInPc4d1	inflexní
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Jednoznačně	jednoznačně	k6eAd1	jednoznačně
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
oblastí	oblast	k1gFnSc7	oblast
použití	použití	k1gNnSc2	použití
derivace	derivace	k1gFnSc2	derivace
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
jsou	být	k5eAaImIp3nP	být
derivace	derivace	k1gFnPc1	derivace
podle	podle	k7c2	podle
časové	časový	k2eAgFnSc2d1	časová
proměnné	proměnná	k1gFnSc2	proměnná
<g/>
,	,	kIx,	,
vyjadřující	vyjadřující	k2eAgFnSc4d1	vyjadřující
rychlost	rychlost	k1gFnSc4	rychlost
změny	změna	k1gFnSc2	změna
nějaké	nějaký	k3yIgFnSc2	nějaký
proměnné	proměnná	k1gFnSc2	proměnná
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgMnPc1d3	Nejběžnější
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
časové	časový	k2eAgFnPc1d1	časová
derivace	derivace	k1gFnPc1	derivace
polohy	poloha	k1gFnSc2	poloha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
kinematice	kinematika	k1gFnSc6	kinematika
<g/>
:	:	kIx,	:
Rychlost	rychlost	k1gFnSc1	rychlost
(	(	kIx(	(
<g/>
okamžitá	okamžitý	k2eAgFnSc1d1	okamžitá
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
koncept	koncept	k1gInSc1	koncept
průměrné	průměrný	k2eAgFnSc2d1	průměrná
rychlosti	rychlost	k1gFnSc2	rychlost
se	se	k3xPyFc4	se
obejde	obejít	k5eAaPmIp3nS	obejít
bez	bez	k7c2	bez
diferenciálního	diferenciální	k2eAgInSc2d1	diferenciální
počtu	počet	k1gInSc2	počet
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
derivace	derivace	k1gFnSc1	derivace
souřadnice	souřadnice	k1gFnSc2	souřadnice
polohy	poloha	k1gFnSc2	poloha
tělesa	těleso	k1gNnSc2	těleso
podle	podle	k7c2	podle
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Zrychlení	zrychlení	k1gNnSc1	zrychlení
je	být	k5eAaImIp3nS	být
derivace	derivace	k1gFnSc1	derivace
rychlosti	rychlost	k1gFnSc2	rychlost
podle	podle	k7c2	podle
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
druhá	druhý	k4xOgFnSc1	druhý
derivace	derivace	k1gFnSc1	derivace
polohy	poloha	k1gFnSc2	poloha
podle	podle	k7c2	podle
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Ryv	Ryv	k?	Ryv
je	být	k5eAaImIp3nS	být
derivace	derivace	k1gFnSc1	derivace
zrychlení	zrychlení	k1gNnSc2	zrychlení
podle	podle	k7c2	podle
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
třetí	třetí	k4xOgFnSc1	třetí
derivace	derivace	k1gFnSc1	derivace
polohy	poloha	k1gFnSc2	poloha
podle	podle	k7c2	podle
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgInPc2	tento
základních	základní	k2eAgInPc2d1	základní
pojmů	pojem	k1gInPc2	pojem
se	se	k3xPyFc4	se
derivace	derivace	k1gFnPc1	derivace
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
teoriích	teorie	k1gFnPc6	teorie
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
polí	pole	k1gFnPc2	pole
<g/>
,	,	kIx,	,
Maxwellových	Maxwellův	k2eAgFnPc6d1	Maxwellova
rovnicích	rovnice	k1gFnPc6	rovnice
atd.	atd.	kA	atd.
Mnoho	mnoho	k4c1	mnoho
vědeckých	vědecký	k2eAgInPc2d1	vědecký
problémů	problém	k1gInPc2	problém
lze	lze	k6eAd1	lze
formulovat	formulovat	k5eAaImF	formulovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
nějaká	nějaký	k3yIgFnSc1	nějaký
funkce	funkce	k1gFnSc1	funkce
i	i	k9	i
její	její	k3xOp3gFnSc1	její
derivace	derivace	k1gFnSc1	derivace
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnSc3	takový
rovnici	rovnice	k1gFnSc3	rovnice
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciální	diferenciální	k2eAgFnPc1d1	diferenciální
rovnice	rovnice	k1gFnPc1	rovnice
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
snad	snad	k9	snad
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
vědeckých	vědecký	k2eAgInPc6d1	vědecký
oborech	obor	k1gInPc6	obor
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
fyziky	fyzika	k1gFnSc2	fyzika
také	také	k9	také
např.	např.	kA	např.
v	v	k7c6	v
chemii	chemie	k1gFnSc6	chemie
<g/>
,	,	kIx,	,
sociologii	sociologie	k1gFnSc6	sociologie
<g/>
,	,	kIx,	,
ekologii	ekologie	k1gFnSc6	ekologie
atd.	atd.	kA	atd.
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
v	v	k7c6	v
rovnici	rovnice	k1gFnSc6	rovnice
objevují	objevovat	k5eAaImIp3nP	objevovat
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
obyčejné	obyčejný	k2eAgFnPc4d1	obyčejná
<g/>
"	"	kIx"	"
derivace	derivace	k1gFnPc4	derivace
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
parciální	parciální	k2eAgFnSc1d1	parciální
derivace	derivace	k1gFnSc1	derivace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
rovnice	rovnice	k1gFnSc1	rovnice
(	(	kIx(	(
<g/>
ODR	odr	k1gInSc1	odr
<g/>
)	)	kIx)	)
a	a	k8xC	a
parciální	parciální	k2eAgFnSc1d1	parciální
diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
rovnice	rovnice	k1gFnSc1	rovnice
(	(	kIx(	(
<g/>
PDR	PDR	kA	PDR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Matematická	matematický	k2eAgFnSc1d1	matematická
analýza	analýza	k1gFnSc1	analýza
Limita	limita	k1gFnSc1	limita
Integrál	integrál	k1gInSc1	integrál
Diferenciál	diferenciál	k1gInSc1	diferenciál
Diferenciální	diferenciální	k2eAgFnSc2d1	diferenciální
rovnice	rovnice	k1gFnSc2	rovnice
Diference	diference	k1gFnPc1	diference
Průběh	průběh	k1gInSc4	průběh
funkce	funkce	k1gFnSc2	funkce
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
derivace	derivace	k1gFnSc2	derivace
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
derivace	derivace	k1gFnSc2	derivace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Derivace	derivace	k1gFnSc1	derivace
v	v	k7c4	v
encyklopedii	encyklopedie	k1gFnSc4	encyklopedie
MathWorld	MathWorlda	k1gFnPc2	MathWorlda
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
WIMS	WIMS	kA	WIMS
Function	Function	k1gInSc1	Function
Calculator	Calculator	k1gInSc1	Calculator
online	onlinout	k5eAaPmIp3nS	onlinout
výpočty	výpočet	k1gInPc4	výpočet
derivací	derivace	k1gFnPc2	derivace
MAW	MAW	kA	MAW
-	-	kIx~	-
matematické	matematický	k2eAgInPc4d1	matematický
výpočty	výpočet	k1gInPc4	výpočet
online	onlinout	k5eAaPmIp3nS	onlinout
online	onlinout	k5eAaPmIp3nS	onlinout
výpočty	výpočet	k1gInPc7	výpočet
derivací	derivace	k1gFnPc2	derivace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
zobrazení	zobrazení	k1gNnSc2	zobrazení
postupu	postup	k1gInSc2	postup
TRIAL	trial	k1gInSc1	trial
<g/>
,	,	kIx,	,
Diferenciální	diferenciální	k2eAgInSc1d1	diferenciální
počet	počet	k1gInSc1	počet
–	–	k?	–
Katedra	katedra	k1gFnSc1	katedra
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
Západočeská	západočeský	k2eAgFnSc1d1	Západočeská
univerzita	univerzita	k1gFnSc1	univerzita
</s>
