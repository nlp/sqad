<s>
Biatec	Biatec	k1gMnSc1
</s>
<s>
Originální	originální	k2eAgInSc1d1
Biatec	Biatec	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
replika	replika	k1gFnSc1
na	na	k7c6
moderní	moderní	k2eAgFnSc6d1
slovenské	slovenský	k2eAgFnSc6d1
pětikoruně	pětikoruna	k1gFnSc6
</s>
<s>
Biatec	Biatec	k1gInSc1
(	(	kIx(
<g/>
taktéž	taktéž	k?
Biatex	Biatex	k1gInSc1
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
jméno	jméno	k1gNnSc1
osoby	osoba	k1gFnSc2
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
vladaře	vladař	k1gMnSc2
<g/>
,	,	kIx,
jehož	jenž	k3xOyRp3gNnSc2
jméno	jméno	k1gNnSc4
se	se	k3xPyFc4
vyskytovalo	vyskytovat	k5eAaImAgNnS
na	na	k7c6
keltských	keltský	k2eAgFnPc6d1
mincích	mince	k1gFnPc6
ražených	ražený	k2eAgFnPc6d1
kmenem	kmen	k1gInSc7
Bójů	Bój	k1gMnPc2
na	na	k7c6
území	území	k1gNnSc6
dnešní	dnešní	k2eAgFnSc2d1
Bratislavy	Bratislava	k1gFnSc2
a	a	k8xC
okolí	okolí	k1gNnSc2
v	v	k7c6
letech	let	k1gInPc6
60-40	60-40	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	kA
l.	l.	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
něj	on	k3xPp3gNnSc2
byl	být	k5eAaImAgInS
pojmenován	pojmenován	k2eAgInSc1d1
typ	typ	k1gInSc1
mincí	mince	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
nichž	jenž	k3xRgFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
literatuře	literatura	k1gFnSc6
se	se	k3xPyFc4
též	též	k9
nazývají	nazývat	k5eAaImIp3nP
„	„	k?
<g/>
hexadrachmy	hexadrachm	k1gMnPc4
bratislavského	bratislavský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
velké	velký	k2eAgFnPc4d1
a	a	k8xC
vysoce	vysoce	k6eAd1
kvalitní	kvalitní	k2eAgFnPc4d1
napodobeniny	napodobenina	k1gFnPc4
řeckých	řecký	k2eAgMnPc2d1
hexadrachem	hexadrach	k1gMnSc7
nebo	nebo	k8xC
tetradrachem	tetradrach	k1gMnSc7
vyrobené	vyrobený	k2eAgNnSc4d1
nejčastěji	často	k6eAd3
ze	z	k7c2
stříbra	stříbro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Počátek	počátek	k1gInSc1
jejich	jejich	k3xOp3gFnSc2
výroby	výroba	k1gFnSc2
souvisí	souviset	k5eAaImIp3nS
pravděpodobně	pravděpodobně	k6eAd1
s	s	k7c7
přesídlením	přesídlení	k1gNnSc7
Bójů	Bój	k1gMnPc2
z	z	k7c2
Čech	Čechy	k1gFnPc2
do	do	k7c2
dunajské	dunajský	k2eAgFnSc2d1
nížiny	nížina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Jejich	jejich	k3xOp3gFnSc1
ražba	ražba	k1gFnSc1
ustala	ustat	k5eAaPmAgFnS
po	po	k7c6
porážce	porážka	k1gFnSc6
Bójů	Bój	k1gMnPc2
Dáky	Dák	k1gMnPc4
v	v	k7c6
polovině	polovina	k1gFnSc6
1	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
ale	ale	k8xC
ještě	ještě	k9
po	po	k7c4
určitou	určitý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
se	se	k3xPyFc4
vyskytovaly	vyskytovat	k5eAaImAgInP
v	v	k7c6
oběhu	oběh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
touto	tento	k3xDgFnSc7
válkou	válka	k1gFnSc7
bylo	být	k5eAaImAgNnS
v	v	k7c6
průběhu	průběh	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
objeveno	objevit	k5eAaPmNgNnS
celkem	celkem	k6eAd1
14	#num#	k4
pokladů	poklad	k1gInPc2
těchto	tento	k3xDgFnPc2
mincí	mince	k1gFnPc2
zakopaných	zakopaný	k2eAgFnPc2d1
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
6	#num#	k4
se	se	k3xPyFc4
vyskytovalo	vyskytovat	k5eAaImAgNnS
na	na	k7c6
území	území	k1gNnSc6
Bratislavy	Bratislava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
nich	on	k3xPp3gInPc2
<g/>
,	,	kIx,
objeveném	objevený	k2eAgInSc6d1
roku	rok	k1gInSc6
1942	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
našlo	najít	k5eAaPmAgNnS
celkem	celkem	k6eAd1
270	#num#	k4
mincí	mince	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
reversu	revers	k1gInSc6
mívaly	mívat	k5eAaImAgInP
uveden	uvést	k5eAaPmNgMnS
latinkou	latinka	k1gFnSc7
psaný	psaný	k2eAgInSc1d1
nápis	nápis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
většiny	většina	k1gFnSc2
z	z	k7c2
nich	on	k3xPp3gMnPc2
jde	jít	k5eAaImIp3nS
o	o	k7c4
jména	jméno	k1gNnPc4
(	(	kIx(
<g/>
například	například	k6eAd1
NONNOS	NONNOS	kA
<g/>
,	,	kIx,
DEVIL	DEVIL	kA
<g/>
,	,	kIx,
BUSU	bus	k1gInSc2
<g/>
,	,	kIx,
BUSSUMARUS	BUSSUMARUS	kA
<g/>
,	,	kIx,
TITTO	TITTO	kA
<g/>
,	,	kIx,
COISA	COISA	kA
<g/>
,	,	kIx,
COUNOS	COUNOS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
jazykového	jazykový	k2eAgInSc2d1
rozboru	rozbor	k1gInSc2
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc4
vesměs	vesměs	k6eAd1
jména	jméno	k1gNnPc1
keltská	keltský	k2eAgNnPc1d1
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
také	také	k6eAd1
keltsko-illyrská	keltsko-illyrský	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Ze	z	k7c2
14	#num#	k4
různých	různý	k2eAgInPc2d1
nápisů	nápis	k1gInPc2
se	se	k3xPyFc4
BIATEC	BIATEC	kA
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
také	také	k9
jen	jen	k9
BIAT	BIAT	kA
<g/>
)	)	kIx)
na	na	k7c6
nich	on	k3xPp3gFnPc6
objevuje	objevovat	k5eAaImIp3nS
nejčastěji	často	k6eAd3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
nápisy	nápis	k1gInPc4
představují	představovat	k5eAaImIp3nP
nejstarší	starý	k2eAgNnSc4d3
použití	použití	k1gNnSc4
písma	písmo	k1gNnSc2
na	na	k7c4
územní	územní	k2eAgNnSc4d1
dnešního	dnešní	k2eAgNnSc2d1
Slovenska	Slovensko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Společně	společně	k6eAd1
s	s	k7c7
nápisem	nápis	k1gInSc7
se	se	k3xPyFc4
na	na	k7c6
reversu	revers	k1gInSc6
obvykle	obvykle	k6eAd1
vyskytoval	vyskytovat	k5eAaImAgMnS
jezdec	jezdec	k1gInSc4
na	na	k7c6
koni	kůň	k1gMnSc6
nebo	nebo	k8xC
různá	různý	k2eAgNnPc4d1
mytologická	mytologický	k2eAgNnPc4d1
či	či	k8xC
reálná	reálný	k2eAgNnPc4d1
zvířata	zvíře	k1gNnPc4
(	(	kIx(
<g/>
gryf	gryf	k1gMnSc1
<g/>
,	,	kIx,
lev	lev	k1gMnSc1
<g/>
,	,	kIx,
kanec	kanec	k1gMnSc1
<g/>
,	,	kIx,
kentaur	kentaur	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
aversu	avers	k1gInSc6
se	se	k3xPyFc4
nacházela	nacházet	k5eAaImAgFnS
jedna	jeden	k4xCgFnSc1
nebo	nebo	k8xC
dvě	dva	k4xCgFnPc4
kryjící	kryjící	k2eAgFnPc4d1
se	se	k3xPyFc4
hlavy	hlava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Mince	mince	k1gFnPc1
typu	typ	k1gInSc2
Biatec	Biatec	k1gInSc1
měly	mít	k5eAaImAgFnP
průměr	průměr	k1gInSc4
25	#num#	k4
milimetrů	milimetr	k1gInPc2
a	a	k8xC
hmotnost	hmotnost	k1gFnSc1
16,5	16,5	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
gramů	gram	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Koncem	koncem	k7c2
laténského	laténský	k2eAgNnSc2d1
období	období	k1gNnSc2
se	se	k3xPyFc4
také	také	k9
razily	razit	k5eAaImAgFnP
v	v	k7c6
menší	malý	k2eAgFnSc6d2
stříbrné	stříbrná	k1gFnSc6
(	(	kIx(
<g/>
tzv.	tzv.	kA
„	„	k?
<g/>
simmeringského	simmeringský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
“	“	k?
podle	podle	k7c2
naleziště	naleziště	k1gNnSc2
Simmering-Wien	Simmering-Wina	k1gFnPc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
ve	v	k7c6
zlaté	zlatý	k2eAgFnSc6d1
mušlovité	mušlovitý	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Biatec	Biatec	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
PODBORSKÝ	PODBORSKÝ	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
pravěku	pravěk	k1gInSc2
a	a	k8xC
rané	raný	k2eAgFnSc2d1
doby	doba	k1gFnSc2
dějinné	dějinný	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
210	#num#	k4
<g/>
-	-	kIx~
<g/>
2159	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
207	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Biatex	Biatex	k1gInSc1
Hexadrachm	Hexadrachm	k1gInSc1
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
FILIP	Filip	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keltská	keltský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gNnPc4
dědictví	dědictví	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1079	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
140	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Biatec	Biatec	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
