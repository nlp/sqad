<s>
Biatec	Biatec	k1gInSc1
(	(	kIx(
<g/>
taktéž	taktéž	k?
Biatex	Biatex	k1gInSc1
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
jméno	jméno	k1gNnSc1
osoby	osoba	k1gFnSc2
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
vladaře	vladař	k1gMnSc2
<g/>
,	,	kIx,
jehož	jenž	k3xOyRp3gNnSc2
jméno	jméno	k1gNnSc4
se	se	k3xPyFc4
vyskytovalo	vyskytovat	k5eAaImAgNnS
na	na	k7c6
keltských	keltský	k2eAgFnPc6d1
mincích	mince	k1gFnPc6
ražených	ražený	k2eAgFnPc6d1
kmenem	kmen	k1gInSc7
Bójů	Bój	k1gMnPc2
na	na	k7c6
území	území	k1gNnSc6
dnešní	dnešní	k2eAgFnSc2d1
Bratislavy	Bratislava	k1gFnSc2
a	a	k8xC
okolí	okolí	k1gNnSc2
v	v	k7c6
letech	let	k1gInPc6
60-40	60-40	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	kA
l.	l.	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
něj	on	k3xPp3gNnSc2
byl	být	k5eAaImAgInS
pojmenován	pojmenován	k2eAgInSc1d1
typ	typ	k1gInSc1
mincí	mince	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
nichž	jenž	k3xRgFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>