<s>
Kyjov	Kyjov	k1gInSc1	Kyjov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Gaya	Gaya	k?	Gaya
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Hodonín	Hodonín	k1gInSc1	Hodonín
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Kyjovce	kyjovka	k1gFnSc6	kyjovka
<g/>
,	,	kIx,	,
25	[number]	k4	25
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obklopeno	obklopen	k2eAgNnSc1d1	obklopeno
Kyjovskou	kyjovský	k2eAgFnSc7d1	Kyjovská
pahorkatinou	pahorkatina	k1gFnSc7	pahorkatina
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
pohoří	pohoří	k1gNnSc2	pohoří
Chřiby	Chřiby	k1gInPc1	Chřiby
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
přes	přes	k7c4	přes
11	[number]	k4	11
400	[number]	k4	400
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
centrem	centr	k1gInSc7	centr
folklórního	folklórní	k2eAgInSc2d1	folklórní
regionu	region	k1gInSc2	region
Kyjovské	kyjovský	k2eAgNnSc4d1	Kyjovské
Dolňácko	Dolňácko	k1gNnSc4	Dolňácko
a	a	k8xC	a
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
zde	zde	k6eAd1	zde
probíhá	probíhat	k5eAaImIp3nS	probíhat
národopisná	národopisný	k2eAgFnSc1d1	národopisná
slavnost	slavnost	k1gFnSc4	slavnost
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
osobního	osobní	k2eAgNnSc2d1	osobní
jména	jméno	k1gNnSc2	jméno
Kyj	kyj	k1gInSc1	kyj
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
jménem	jméno	k1gNnSc7	jméno
Kyj	kyj	k1gInSc1	kyj
mohl	moct	k5eAaImAgInS	moct
mít	mít	k5eAaImF	mít
Kyjov	Kyjov	k1gInSc1	Kyjov
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
osobní	osobní	k2eAgNnSc1d1	osobní
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
slovanské	slovanský	k2eAgFnSc6d1	Slovanská
oblasti	oblast	k1gFnSc6	oblast
poměrně	poměrně	k6eAd1	poměrně
rozšířeno	rozšířen	k2eAgNnSc1d1	rozšířeno
<g/>
.	.	kIx.	.
</s>
<s>
Německy	německy	k6eAd1	německy
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
nazývalo	nazývat	k5eAaImAgNnS	nazývat
Gaya	Gaya	k?	Gaya
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
také	také	k9	také
Geyen	Geyen	k2eAgMnSc1d1	Geyen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
má	mít	k5eAaImIp3nS	mít
město	město	k1gNnSc1	město
pravou	pravý	k2eAgFnSc4d1	pravá
paži	paže	k1gFnSc4	paže
držící	držící	k2eAgInSc4d1	držící
kyj	kyj	k1gInSc4	kyj
na	na	k7c6	na
modrém	modrý	k2eAgNnSc6d1	modré
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
suků	suk	k1gInPc2	suk
na	na	k7c6	na
kyji	kyj	k1gInSc6	kyj
bývá	bývat	k5eAaImIp3nS	bývat
od	od	k7c2	od
tří	tři	k4xCgInPc2	tři
do	do	k7c2	do
pěti	pět	k4xCc2	pět
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
však	však	k9	však
čtyři	čtyři	k4xCgMnPc4	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
ke	k	k7c3	k
znaku	znak	k1gInSc3	znak
přidával	přidávat	k5eAaImAgMnS	přidávat
anděl	anděl	k1gMnSc1	anděl
jako	jako	k8xC	jako
štítonoš	štítonoš	k1gMnSc1	štítonoš
a	a	k8xC	a
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
šachovaná	šachovaný	k2eAgFnSc1d1	šachovaná
orlice	orlice	k1gFnSc1	orlice
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
znaku	znak	k1gInSc3	znak
však	však	k9	však
anděl	anděl	k1gMnSc1	anděl
či	či	k8xC	či
orlice	orlice	k1gFnSc1	orlice
nepatří	patřit	k5eNaImIp3nS	patřit
a	a	k8xC	a
šlo	jít	k5eAaImAgNnS	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
ryteckou	rytecký	k2eAgFnSc4d1	rytecká
ozdobu	ozdoba	k1gFnSc4	ozdoba
pečeti	pečeť	k1gFnSc2	pečeť
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
znaku	znak	k1gInSc2	znak
jsou	být	k5eAaImIp3nP	být
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
je	být	k5eAaImIp3nS	být
použita	použít	k5eAaPmNgFnS	použít
podle	podle	k7c2	podle
znaku	znak	k1gInSc2	znak
z	z	k7c2	z
rejstříku	rejstřík	k1gInSc2	rejstřík
kožešnického	kožešnický	k2eAgInSc2d1	kožešnický
a	a	k8xC	a
kloboučnického	kloboučnický	k2eAgInSc2d1	kloboučnický
cechu	cech	k1gInSc2	cech
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
varianta	varianta	k1gFnSc1	varianta
městského	městský	k2eAgInSc2d1	městský
znaku	znak	k1gInSc2	znak
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1727	[number]	k4	1727
s	s	k7c7	s
červenou	červený	k2eAgFnSc7d1	červená
podkladovou	podkladový	k2eAgFnSc7d1	podkladová
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
znaku	znak	k1gInSc2	znak
není	být	k5eNaImIp3nS	být
doložen	doložen	k2eAgInSc1d1	doložen
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
vyobrazen	vyobrazit	k5eAaPmNgInS	vyobrazit
na	na	k7c6	na
pečeti	pečeť	k1gFnSc6	pečeť
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1526	[number]	k4	1526
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
úvaha	úvaha	k1gFnSc1	úvaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
kyj	kyj	k1gInSc1	kyj
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
ostrev	ostrev	k?	ostrev
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Lichtenburku	Lichtenburk	k1gInSc2	Lichtenburk
<g/>
.	.	kIx.	.
</s>
<s>
Půta	Půta	k1gMnSc1	Půta
z	z	k7c2	z
Lichtenburku	Lichtenburk	k1gInSc2	Lichtenburk
měl	mít	k5eAaImAgInS	mít
Kyjov	Kyjov	k1gInSc1	Kyjov
pronajat	pronajmout	k5eAaPmNgInS	pronajmout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1480	[number]	k4	1480
<g/>
-	-	kIx~	-
<g/>
1482	[number]	k4	1482
<g/>
.	.	kIx.	.
</s>
<s>
Kyj	kyj	k1gInSc1	kyj
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
drží	držet	k5eAaImIp3nP	držet
chráněné	chráněný	k2eAgNnSc1d1	chráněné
rameno	rameno	k1gNnSc4	rameno
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
spíše	spíše	k9	spíše
mluvícím	mluvící	k2eAgNnSc7d1	mluvící
znamením	znamení	k1gNnSc7	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Dokládají	dokládat	k5eAaImIp3nP	dokládat
to	ten	k3xDgNnSc1	ten
starší	starý	k2eAgFnPc1d2	starší
pečeti	pečeť	k1gFnPc1	pečeť
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
kyj	kyj	k1gInSc1	kyj
nemá	mít	k5eNaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
ostrve	ostrve	k?	ostrve
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
zemském	zemský	k2eAgInSc6d1	zemský
archivu	archiv	k1gInSc6	archiv
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
dochován	dochován	k2eAgInSc1d1	dochován
jediný	jediný	k2eAgInSc1d1	jediný
otisk	otisk	k1gInSc1	otisk
městské	městský	k2eAgFnSc2d1	městská
pečeti	pečeť	k1gFnSc2	pečeť
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1562	[number]	k4	1562
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
vyobrazena	vyobrazen	k2eAgFnSc1d1	vyobrazena
městská	městský	k2eAgFnSc1d1	městská
brána	brána	k1gFnSc1	brána
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
věžemi	věž	k1gFnPc7	věž
<g/>
,	,	kIx,	,
latinským	latinský	k2eAgInSc7d1	latinský
nápisem	nápis	k1gInSc7	nápis
SIGILLVM	SIGILLVM	kA	SIGILLVM
CIVITATIS	CIVITATIS	kA	CIVITATIS
GAYENSIS	GAYENSIS	kA	GAYENSIS
a	a	k8xC	a
mezi	mezi	k7c7	mezi
věžemi	věž	k1gFnPc7	věž
je	být	k5eAaImIp3nS	být
umístěno	umístěn	k2eAgNnSc1d1	umístěno
písmeno	písmeno	k1gNnSc1	písmeno
G.	G.	kA	G.
Tuto	tento	k3xDgFnSc4	tento
pečeť	pečeť	k1gFnSc4	pečeť
nechal	nechat	k5eAaPmAgMnS	nechat
vyhotovit	vyhotovit	k5eAaPmF	vyhotovit
první	první	k4xOgInSc1	první
kyjovský	kyjovský	k2eAgMnSc1d1	kyjovský
primátor	primátor	k1gMnSc1	primátor
Václav	Václav	k1gMnSc1	Václav
st.	st.	kA	st.
Bzenecký	bzenecký	k2eAgMnSc1d1	bzenecký
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
nepoužívala	používat	k5eNaImAgFnS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
město	město	k1gNnSc1	město
získalo	získat	k5eAaPmAgNnS	získat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
kulturu	kultura	k1gFnSc4	kultura
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
číslo	číslo	k1gNnSc1	číslo
457	[number]	k4	457
ze	z	k7c2	z
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
modré	modrý	k2eAgFnSc6d1	modrá
vlajce	vlajka	k1gFnSc6	vlajka
je	být	k5eAaImIp3nS	být
zobrazena	zobrazen	k2eAgFnSc1d1	zobrazena
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
obrněná	obrněný	k2eAgFnSc1d1	obrněná
paže	paže	k1gFnSc1	paže
bez	bez	k7c2	bez
rukavice	rukavice	k1gFnSc2	rukavice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
drží	držet	k5eAaImIp3nS	držet
vztyčený	vztyčený	k2eAgInSc4d1	vztyčený
kyj	kyj	k1gInSc4	kyj
hnědé	hnědý	k2eAgFnSc2d1	hnědá
barvy	barva	k1gFnSc2	barva
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
velkými	velký	k2eAgInPc7d1	velký
suky	suk	k1gInPc7	suk
<g/>
.	.	kIx.	.
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1	lidová
pověst	pověst	k1gFnSc1	pověst
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
napadení	napadení	k1gNnSc6	napadení
kyjovské	kyjovský	k2eAgFnSc2d1	Kyjovská
osady	osada	k1gFnSc2	osada
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
obyvatelé	obyvatel	k1gMnPc1	obyvatel
budovali	budovat	k5eAaImAgMnP	budovat
obrannou	obranný	k2eAgFnSc4d1	obranná
palisádu	palisáda	k1gFnSc4	palisáda
z	z	k7c2	z
dubových	dubový	k2eAgInPc2d1	dubový
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
útočníky	útočník	k1gMnPc7	útočník
od	od	k7c2	od
východu	východ	k1gInSc2	východ
se	se	k3xPyFc4	se
neozbrojení	ozbrojený	k2eNgMnPc1d1	neozbrojený
osadníci	osadník	k1gMnPc1	osadník
úspěšně	úspěšně	k6eAd1	úspěšně
ubránili	ubránit	k5eAaPmAgMnP	ubránit
dubovými	dubový	k2eAgInPc7d1	dubový
kyji	kyj	k1gInPc7	kyj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zbyly	zbýt	k5eAaPmAgInP	zbýt
z	z	k7c2	z
opracovaných	opracovaný	k2eAgInPc2d1	opracovaný
dubových	dubový	k2eAgInPc2d1	dubový
kůlů	kůl	k1gInPc2	kůl
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
měl	mít	k5eAaImAgInS	mít
údajně	údajně	k6eAd1	údajně
vzniknout	vzniknout	k5eAaPmF	vzniknout
název	název	k1gInSc4	název
osady	osada	k1gFnSc2	osada
a	a	k8xC	a
do	do	k7c2	do
znaku	znak	k1gInSc2	znak
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
dubový	dubový	k2eAgInSc1d1	dubový
kyj	kyj	k1gInSc1	kyj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jsou	být	k5eAaImIp3nP	být
valy	val	k1gInPc1	val
a	a	k8xC	a
palisády	palisáda	k1gFnPc1	palisáda
písemně	písemně	k6eAd1	písemně
doloženy	doložen	k2eAgFnPc1d1	doložena
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1284	[number]	k4	1284
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
premonstrátský	premonstrátský	k2eAgMnSc1d1	premonstrátský
opat	opat	k1gMnSc1	opat
Budiš	Budiš	k1gMnSc1	Budiš
vymohl	vymoct	k5eAaPmAgMnS	vymoct
od	od	k7c2	od
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
povolení	povolení	k1gNnPc2	povolení
k	k	k7c3	k
opevnění	opevnění	k1gNnSc3	opevnění
měst	město	k1gNnPc2	město
a	a	k8xC	a
městeček	městečko	k1gNnPc2	městečko
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
kláštera	klášter	k1gInSc2	klášter
Hradisko	hradisko	k1gNnSc1	hradisko
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
patřil	patřit	k5eAaImAgInS	patřit
i	i	k9	i
Kyjov	Kyjov	k1gInSc1	Kyjov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
archeologové	archeolog	k1gMnPc1	archeolog
nalezli	naleznout	k5eAaPmAgMnP	naleznout
1	[number]	k4	1
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
dnešního	dnešní	k2eAgNnSc2d1	dnešní
náměstí	náměstí	k1gNnSc2	náměstí
pohřebiště	pohřebiště	k1gNnSc2	pohřebiště
ze	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
250	[number]	k4	250
hroby	hrob	k1gInPc4	hrob
germánského	germánský	k2eAgInSc2d1	germánský
kmene	kmen	k1gInSc2	kmen
Langobardů	Langobard	k1gInPc2	Langobard
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
tak	tak	k9	tak
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
langobardských	langobardský	k2eAgNnPc2d1	langobardský
center	centrum	k1gNnPc2	centrum
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
nález	nález	k1gInSc1	nález
významně	významně	k6eAd1	významně
změnil	změnit	k5eAaPmAgInS	změnit
dosavadní	dosavadní	k2eAgFnPc4d1	dosavadní
znalosti	znalost	k1gFnPc4	znalost
o	o	k7c6	o
pohybu	pohyb	k1gInSc6	pohyb
Langobardů	Langobard	k1gInPc2	Langobard
během	během	k7c2	během
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
písemné	písemný	k2eAgFnSc6d1	písemná
zmínce	zmínka	k1gFnSc6	zmínka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1126	[number]	k4	1126
se	se	k3xPyFc4	se
Kyjov	Kyjov	k1gInSc1	Kyjov
připomíná	připomínat	k5eAaImIp3nS	připomínat
jako	jako	k9	jako
trhová	trhový	k2eAgFnSc1d1	trhová
ves	ves	k1gFnSc1	ves
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
kníže	kníže	k1gMnSc1	kníže
Václav	Václav	k1gMnSc1	Václav
Jindřich	Jindřich	k1gMnSc1	Jindřich
Olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
daroval	darovat	k5eAaPmAgMnS	darovat
benediktinskému	benediktinský	k2eAgInSc3d1	benediktinský
klášteru	klášter	k1gInSc3	klášter
Hradisko	hradisko	k1gNnSc4	hradisko
u	u	k7c2	u
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
Václavova	Václavův	k2eAgMnSc2d1	Václavův
strýce	strýc	k1gMnSc2	strýc
Oty	Ota	k1gMnSc2	Ota
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1126	[number]	k4	1126
zahynul	zahynout	k5eAaPmAgMnS	zahynout
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Chlumce	Chlumec	k1gInSc2	Chlumec
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
Hradisko	hradisko	k1gNnSc4	hradisko
brzy	brzy	k6eAd1	brzy
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
premonstrátů	premonstrát	k1gMnPc2	premonstrát
a	a	k8xC	a
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
za	za	k7c2	za
opata	opat	k1gMnSc2	opat
Michala	Michal	k1gMnSc2	Michal
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
kyjovském	kyjovský	k2eAgNnSc6d1	Kyjovské
návrší	návrší	k1gNnSc6	návrší
postaven	postavit	k5eAaPmNgInS	postavit
kamenný	kamenný	k2eAgInSc1d1	kamenný
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc2	Martin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
správě	správa	k1gFnSc6	správa
kláštera	klášter	k1gInSc2	klášter
byl	být	k5eAaImAgInS	být
Kyjov	Kyjov	k1gInSc1	Kyjov
nejdéle	dlouho	k6eAd3	dlouho
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1382	[number]	k4	1382
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
jej	on	k3xPp3gMnSc4	on
začal	začít	k5eAaPmAgInS	začít
pronajímat	pronajímat	k5eAaImF	pronajímat
nižší	nízký	k2eAgInSc1d2	nižší
šlechtě	šlechta	k1gFnSc3	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
povýšil	povýšit	k5eAaPmAgInS	povýšit
Kyjov	Kyjov	k1gInSc1	Kyjov
privilegiem	privilegium	k1gNnSc7	privilegium
z	z	k7c2	z
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1515	[number]	k4	1515
na	na	k7c4	na
město	město	k1gNnSc4	město
a	a	k8xC	a
udělil	udělit	k5eAaPmAgMnS	udělit
mu	on	k3xPp3gMnSc3	on
právo	právo	k1gNnSc4	právo
pečetit	pečetit	k5eAaImF	pečetit
červeným	červený	k2eAgInSc7d1	červený
voskem	vosk	k1gInSc7	vosk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
husitském	husitský	k2eAgNnSc6d1	husitské
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
utrakvismu	utrakvismus	k1gInSc3	utrakvismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1431	[number]	k4	1431
ho	on	k3xPp3gMnSc4	on
dobyl	dobýt	k5eAaPmAgInS	dobýt
rakouský	rakouský	k2eAgMnSc1d1	rakouský
markrabě	markrabě	k1gMnSc1	markrabě
Albrecht	Albrecht	k1gMnSc1	Albrecht
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
a	a	k8xC	a
podle	podle	k7c2	podle
kronikáře	kronikář	k1gMnSc2	kronikář
Bartoška	Bartošek	k1gMnSc2	Bartošek
z	z	k7c2	z
Drahonic	Drahonice	k1gFnPc2	Drahonice
zde	zde	k6eAd1	zde
nechal	nechat	k5eAaPmAgMnS	nechat
popravit	popravit	k5eAaPmF	popravit
okolo	okolo	k7c2	okolo
50	[number]	k4	50
kyjovských	kyjovský	k2eAgMnPc2d1	kyjovský
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Premonstrátský	premonstrátský	k2eAgInSc1d1	premonstrátský
klášter	klášter	k1gInSc1	klášter
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1539	[number]	k4	1539
po	po	k7c4	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
letech	léto	k1gNnPc6	léto
prodal	prodat	k5eAaPmAgMnS	prodat
město	město	k1gNnSc4	město
Janu	Jan	k1gMnSc3	Jan
Kunovi	Kuna	k1gMnSc3	Kuna
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
Kyjov	Kyjov	k1gInSc4	Kyjov
prodali	prodat	k5eAaPmAgMnP	prodat
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1548	[number]	k4	1548
Janu	Jan	k1gMnSc6	Jan
Kropáčovi	Kropáč	k1gMnSc6	Kropáč
z	z	k7c2	z
Nevědomí	nevědomí	k1gNnSc2	nevědomí
a	a	k8xC	a
na	na	k7c6	na
Litenčicích	Litenčice	k1gFnPc6	Litenčice
za	za	k7c4	za
9	[number]	k4	9
000	[number]	k4	000
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Kyjovští	kyjovský	k2eAgMnPc1d1	kyjovský
poddaní	poddaný	k1gMnPc1	poddaný
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
nebyli	být	k5eNaImAgMnP	být
spokojeni	spokojen	k2eAgMnPc1d1	spokojen
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1548	[number]	k4	1548
vyslali	vyslat	k5eAaPmAgMnP	vyslat
deputaci	deputace	k1gFnSc4	deputace
vedenou	vedený	k2eAgFnSc4d1	vedená
kyjovským	kyjovský	k2eAgMnSc7d1	kyjovský
měšťanem	měšťan	k1gMnSc7	měšťan
Václavem	Václav	k1gMnSc7	Václav
starším	starší	k1gMnSc7	starší
Bzeneckým	bzenecký	k2eAgMnSc7d1	bzenecký
k	k	k7c3	k
císaři	císař	k1gMnSc3	císař
Ferdinandu	Ferdinand	k1gMnSc3	Ferdinand
I.	I.	kA	I.
Habsburskému	habsburský	k2eAgMnSc3d1	habsburský
do	do	k7c2	do
Augsburgu	Augsburg	k1gInSc6	Augsburg
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tam	tam	k6eAd1	tam
pobýval	pobývat	k5eAaImAgMnS	pobývat
na	na	k7c6	na
Říšském	říšský	k2eAgInSc6d1	říšský
sněmu	sněm	k1gInSc6	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
pak	pak	k6eAd1	pak
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1548	[number]	k4	1548
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
vydal	vydat	k5eAaPmAgMnS	vydat
listinu	listina	k1gFnSc4	listina
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
městu	město	k1gNnSc3	město
slibuje	slibovat	k5eAaImIp3nS	slibovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kyjov	Kyjov	k1gInSc1	Kyjov
už	už	k6eAd1	už
nebude	být	k5eNaImBp3nS	být
zastaven	zastavit	k5eAaPmNgInS	zastavit
ani	ani	k8xC	ani
prodán	prodat	k5eAaPmNgInS	prodat
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
komorním	komorní	k2eAgNnSc7d1	komorní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
nesprávně	správně	k6eNd1	správně
označované	označovaný	k2eAgFnSc2d1	označovaná
královským	královský	k2eAgNnSc7d1	královské
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Nastala	nastat	k5eAaPmAgFnS	nastat
doba	doba	k1gFnSc1	doba
rozkvětu	rozkvět	k1gInSc2	rozkvět
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1561	[number]	k4	1561
<g/>
-	-	kIx~	-
<g/>
1562	[number]	k4	1562
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
postavena	postaven	k2eAgFnSc1d1	postavena
renesanční	renesanční	k2eAgFnSc1d1	renesanční
radnice	radnice	k1gFnSc1	radnice
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc1	tři
městské	městský	k2eAgFnPc1d1	městská
brány	brána	k1gFnPc1	brána
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1577	[number]	k4	1577
koupilo	koupit	k5eAaPmAgNnS	koupit
město	město	k1gNnSc1	město
ves	ves	k1gFnSc1	ves
Vřesovice	Vřesovice	k1gFnSc1	Vřesovice
a	a	k8xC	a
1666	[number]	k4	1666
ves	ves	k1gFnSc4	ves
Kelčany	Kelčan	k1gMnPc4	Kelčan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
Kyjov	Kyjov	k1gInSc1	Kyjov
trpěl	trpět	k5eAaImAgInS	trpět
válečnými	válečný	k2eAgFnPc7d1	válečná
událostmi	událost	k1gFnPc7	událost
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1605	[number]	k4	1605
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
vesnicemi	vesnice	k1gFnPc7	vesnice
vyrabováno	vyrabovat	k5eAaPmNgNnS	vyrabovat
a	a	k8xC	a
vypáleno	vypálit	k5eAaPmNgNnS	vypálit
při	při	k7c6	při
nájezdu	nájezd	k1gInSc6	nájezd
tlup	tlupa	k1gFnPc2	tlupa
sedmihradského	sedmihradský	k2eAgNnSc2d1	sedmihradské
knížete	kníže	k1gNnSc2wR	kníže
Štěpána	Štěpán	k1gMnSc2	Štěpán
Bočkaje	Bočkaje	k1gMnSc2	Bočkaje
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Českého	český	k2eAgNnSc2d1	české
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
při	při	k7c6	při
obraně	obrana	k1gFnSc6	obrana
Kyjova	Kyjov	k1gInSc2	Kyjov
proti	proti	k7c3	proti
vojsku	vojsko	k1gNnSc3	vojsko
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Dampierre	Dampierr	k1gInSc5	Dampierr
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
přes	přes	k7c4	přes
150	[number]	k4	150
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
však	však	k9	však
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1619	[number]	k4	1619
vzdalo	vzdát	k5eAaPmAgNnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1623	[number]	k4	1623
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
zpustošeno	zpustošit	k5eAaPmNgNnS	zpustošit
vpádem	vpád	k1gInSc7	vpád
vojska	vojsko	k1gNnSc2	vojsko
Gabriela	Gabriela	k1gFnSc1	Gabriela
Betlena	Betlena	k1gFnSc1	Betlena
a	a	k8xC	a
250	[number]	k4	250
obyvatel	obyvatel	k1gMnPc2	obyvatel
bylo	být	k5eAaImAgNnS	být
zajato	zajmout	k5eAaPmNgNnS	zajmout
a	a	k8xC	a
odvlečeno	odvlečen	k2eAgNnSc1d1	odvlečen
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1638	[number]	k4	1638
celý	celý	k2eAgInSc1d1	celý
Kyjov	Kyjov	k1gInSc1	Kyjov
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1710	[number]	k4	1710
<g/>
-	-	kIx~	-
<g/>
1784	[number]	k4	1784
ve	v	k7c6	v
městě	město	k1gNnSc6	město
působili	působit	k5eAaImAgMnP	působit
kapucíni	kapucín	k1gMnPc1	kapucín
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zde	zde	k6eAd1	zde
z	z	k7c2	z
odkazu	odkaz	k1gInSc2	odkaz
knížete	kníže	k1gMnSc2	kníže
Jana	Jan	k1gMnSc2	Jan
Adama	Adam	k1gMnSc2	Adam
z	z	k7c2	z
Lichtenštejna	Lichtenštejno	k1gNnSc2	Lichtenštejno
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
současný	současný	k2eAgInSc4d1	současný
farní	farní	k2eAgInSc4d1	farní
kostel	kostel	k1gInSc4	kostel
a	a	k8xC	a
malý	malý	k2eAgInSc4d1	malý
klášter	klášter	k1gInSc4	klášter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1760	[number]	k4	1760
zde	zde	k6eAd1	zde
piaristé	piarista	k1gMnPc1	piarista
založili	založit	k5eAaPmAgMnP	založit
nižší	nízký	k2eAgNnSc4d2	nižší
latinské	latinský	k2eAgNnSc4d1	latinské
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslník	průmyslník	k1gMnSc1	průmyslník
Hugo	Hugo	k1gMnSc1	Hugo
Salm-Reifferscheidt	Salm-Reifferscheidt	k1gMnSc1	Salm-Reifferscheidt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1844	[number]	k4	1844
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
otevřel	otevřít	k5eAaPmAgMnS	otevřít
lignitové	lignitový	k2eAgInPc4d1	lignitový
doly	dol	k1gInPc4	dol
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
začalo	začít	k5eAaPmAgNnS	začít
budování	budování	k1gNnSc1	budování
průmyslu	průmysl	k1gInSc2	průmysl
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1858	[number]	k4	1858
založil	založit	k5eAaPmAgMnS	založit
i	i	k9	i
sklárnu	sklárna	k1gFnSc4	sklárna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
sklárnu	sklárna	k1gFnSc4	sklárna
znovu	znovu	k6eAd1	znovu
založil	založit	k5eAaPmAgMnS	založit
Samuel	Samuel	k1gMnSc1	Samuel
Reich	Reich	k?	Reich
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
bratra	bratr	k1gMnSc2	bratr
Solomona	Solomon	k1gMnSc2	Solomon
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
sklárna	sklárna	k1gFnSc1	sklárna
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
průmyslovým	průmyslový	k2eAgInSc7d1	průmyslový
podnikem	podnik	k1gInSc7	podnik
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
spolkový	spolkový	k2eAgInSc4d1	spolkový
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Čtenářsky	čtenářsky	k6eAd1	čtenářsky
spolek	spolek	k1gInSc1	spolek
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
a	a	k8xC	a
tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
Sokol	Sokol	k1gMnSc1	Sokol
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obecních	obecní	k2eAgFnPc6d1	obecní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
bylo	být	k5eAaImAgNnS	být
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
české	český	k2eAgNnSc1d1	české
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
a	a	k8xC	a
prvním	první	k4xOgMnPc3	první
českým	český	k2eAgMnPc3d1	český
starostou	starosta	k1gMnSc7	starosta
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
JUDr.	JUDr.	kA	JUDr.
Josef	Josef	k1gMnSc1	Josef
Galusek	galuska	k1gFnPc2	galuska
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
ke	k	k7c3	k
Kyjovu	Kyjov	k1gInSc3	Kyjov
připojily	připojit	k5eAaPmAgInP	připojit
dříve	dříve	k6eAd2	dříve
samostatné	samostatný	k2eAgFnSc2d1	samostatná
obce	obec	k1gFnSc2	obec
Nětčice	Nětčice	k1gFnSc2	Nětčice
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Boršov	Boršov	k1gInSc1	Boršov
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bohuslavice	Bohuslavice	k1gFnPc1	Bohuslavice
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nP	tvořit
místní	místní	k2eAgFnPc4d1	místní
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
výstavba	výstavba	k1gFnSc1	výstavba
sídlišť	sídliště	k1gNnPc2	sídliště
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
zástavba	zástavba	k1gFnSc1	zástavba
města	město	k1gNnSc2	město
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Kyjov	Kyjov	k1gInSc1	Kyjov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
okresu	okres	k1gInSc2	okres
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
krajského	krajský	k2eAgNnSc2d1	krajské
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
je	být	k5eAaImIp3nS	být
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
43	[number]	k4	43
km	km	kA	km
jihovýchodním	jihovýchodní	k2eAgInSc7d1	jihovýchodní
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
18	[number]	k4	18
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
okresního	okresní	k2eAgInSc2d1	okresní
Hodonína	Hodonín	k1gInSc2	Hodonín
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
celkem	celkem	k6eAd1	celkem
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
samostatných	samostatný	k2eAgInPc2d1	samostatný
katastrů	katastr	k1gInPc2	katastr
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1946	[number]	k4	1946
<g/>
-	-	kIx~	-
<g/>
1980	[number]	k4	1980
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
od	od	k7c2	od
severu	sever	k1gInSc2	sever
postupně	postupně	k6eAd1	postupně
připojily	připojit	k5eAaPmAgFnP	připojit
tři	tři	k4xCgFnPc1	tři
obce	obec	k1gFnPc1	obec
<g/>
.	.	kIx.	.
</s>
<s>
Katastr	katastr	k1gInSc1	katastr
původního	původní	k2eAgNnSc2d1	původní
města	město	k1gNnSc2	město
má	mít	k5eAaImIp3nS	mít
753,73	[number]	k4	753,73
ha	ha	kA	ha
<g/>
,	,	kIx,	,
místní	místní	k2eAgFnSc4d1	místní
část	část	k1gFnSc4	část
a	a	k8xC	a
dříve	dříve	k6eAd2	dříve
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
Nětčice	Nětčice	k1gFnSc2	Nětčice
227,98	[number]	k4	227,98
ha	ha	kA	ha
<g/>
,	,	kIx,	,
Boršov	Boršov	k1gInSc1	Boršov
334,09	[number]	k4	334,09
ha	ha	kA	ha
a	a	k8xC	a
Bohuslavice	Bohuslavice	k1gFnPc1	Bohuslavice
1671,74	[number]	k4	1671,74
ha	ha	kA	ha
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
29,88	[number]	k4	29,88
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
13	[number]	k4	13
základních	základní	k2eAgFnPc2d1	základní
sídelních	sídelní	k2eAgFnPc2d1	sídelní
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
74	[number]	k4	74
pojmenovaných	pojmenovaný	k2eAgFnPc2d1	pojmenovaná
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
místní	místní	k2eAgFnPc4d1	místní
části	část	k1gFnPc4	část
Bohuslavice	Bohuslavice	k1gFnPc4	Bohuslavice
nejsou	být	k5eNaImIp3nP	být
ulice	ulice	k1gFnPc1	ulice
pojmenovány	pojmenován	k2eAgFnPc1d1	pojmenována
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
hranice	hranice	k1gFnSc1	hranice
katastru	katastr	k1gInSc2	katastr
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
hranicí	hranice	k1gFnSc7	hranice
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
a	a	k8xC	a
Zlínského	zlínský	k2eAgInSc2d1	zlínský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
hodonínského	hodonínský	k2eAgInSc2d1	hodonínský
a	a	k8xC	a
kroměřížského	kroměřížský	k2eAgInSc2d1	kroměřížský
okresu	okres	k1gInSc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
Kyjov	Kyjov	k1gInSc1	Kyjov
zde	zde	k6eAd1	zde
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
městem	město	k1gNnSc7	město
Koryčany	Koryčan	k1gMnPc4	Koryčan
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
Kyjova	Kyjov	k1gInSc2	Kyjov
Bohuslavice	Bohuslavice	k1gFnPc1	Bohuslavice
sousedí	sousedit	k5eAaImIp3nP	sousedit
s	s	k7c7	s
místní	místní	k2eAgFnSc7d1	místní
částí	část	k1gFnSc7	část
Koryčan	Koryčan	k1gMnSc1	Koryčan
Jestřabice	Jestřabice	k1gFnSc2	Jestřabice
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
sousední	sousední	k2eAgFnPc1d1	sousední
obce	obec	k1gFnPc1	obec
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Hodonín	Hodonín	k1gInSc1	Hodonín
a	a	k8xC	a
dle	dle	k7c2	dle
směru	směr	k1gInSc2	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Čeložnice	Čeložnice	k1gFnSc1	Čeložnice
<g/>
,	,	kIx,	,
Kostelec	Kostelec	k1gInSc1	Kostelec
<g/>
,	,	kIx,	,
Vlkoš	Vlkoš	k1gMnSc1	Vlkoš
<g/>
,	,	kIx,	,
Skoronice	Skoronice	k1gFnSc1	Skoronice
<g/>
,	,	kIx,	,
Svatobořice-Mistřín	Svatobořice-Mistřín	k1gInSc1	Svatobořice-Mistřín
<g/>
,	,	kIx,	,
Sobůlky	Sobůlek	k1gMnPc4	Sobůlek
<g/>
,	,	kIx,	,
Bukovany	Bukovan	k1gMnPc4	Bukovan
<g/>
,	,	kIx,	,
Nechvalín	Nechvalín	k1gInSc1	Nechvalín
a	a	k8xC	a
Mouchnice	Mouchnice	k1gFnSc1	Mouchnice
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Kyjovky	kyjovka	k1gFnSc2	kyjovka
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
190	[number]	k4	190
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
chřibský	chřibský	k2eAgInSc1d1	chřibský
kopec	kopec	k1gInSc1	kopec
Lenivá	lenivý	k2eAgFnSc1d1	lenivá
hora	hora	k1gFnSc1	hora
v	v	k7c6	v
části	část	k1gFnSc6	část
Bohuslavice	Bohuslavice	k1gFnPc1	Bohuslavice
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
462,7	[number]	k4	462,7
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
místo	místo	k1gNnSc4	místo
původního	původní	k2eAgInSc2d1	původní
Kyjova	Kyjov	k1gInSc2	Kyjov
(	(	kIx(	(
<g/>
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Kyjov	Kyjov	k1gInSc1	Kyjov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
východně	východně	k6eAd1	východně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
257	[number]	k4	257
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
u	u	k7c2	u
kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Rocha	Rocha	k1gMnSc1	Rocha
a	a	k8xC	a
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
54	[number]	k4	54
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejnižším	nízký	k2eAgInSc6d3	nejnižší
bodě	bod	k1gInSc6	bod
183	[number]	k4	183
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
řeka	řeka	k1gFnSc1	řeka
Kyjovka	kyjovka	k1gFnSc1	kyjovka
opouští	opouštět	k5eAaImIp3nS	opouštět
katastr	katastr	k1gInSc4	katastr
<g/>
.	.	kIx.	.
</s>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
kopcovitá	kopcovitý	k2eAgFnSc1d1	kopcovitá
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
zde	zde	k6eAd1	zde
podhůří	podhůří	k1gNnSc1	podhůří
Chřibů	Chřiby	k1gInPc2	Chřiby
a	a	k8xC	a
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
vesnice	vesnice	k1gFnPc4	vesnice
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Bohuslavic	Bohuslavice	k1gFnPc2	Bohuslavice
už	už	k6eAd1	už
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Chřibech	Chřiby	k1gInPc6	Chřiby
<g/>
.	.	kIx.	.
</s>
<s>
Jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
je	být	k5eAaImIp3nS	být
terén	terén	k1gInSc1	terén
zvlněný	zvlněný	k2eAgInSc1d1	zvlněný
a	a	k8xC	a
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
intenzivněji	intenzivně	k6eAd2	intenzivně
zemědělsky	zemědělsky	k6eAd1	zemědělsky
využívaná	využívaný	k2eAgFnSc1d1	využívaná
<g/>
.	.	kIx.	.
</s>
<s>
Obce	obec	k1gFnPc1	obec
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
okolo	okolo	k7c2	okolo
1	[number]	k4	1
000	[number]	k4	000
-	-	kIx~	-
3	[number]	k4	3
500	[number]	k4	500
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
dobře	dobře	k6eAd1	dobře
viditelný	viditelný	k2eAgInSc4d1	viditelný
hřeben	hřeben	k1gInSc4	hřeben
Chřibů	Chřiby	k1gInPc2	Chřiby
s	s	k7c7	s
kopcem	kopec	k1gInSc7	kopec
Bradlo	bradlo	k1gNnSc1	bradlo
(	(	kIx(	(
<g/>
543	[number]	k4	543
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
hřeben	hřeben	k1gInSc1	hřeben
Bílých	bílý	k2eAgInPc2d1	bílý
Karpat	Karpaty	k1gInPc2	Karpaty
s	s	k7c7	s
horou	hora	k1gFnSc7	hora
Velká	velká	k1gFnSc1	velká
Javořina	Javořina	k1gFnSc1	Javořina
(	(	kIx(	(
<g/>
970	[number]	k4	970
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
a	a	k8xC	a
za	za	k7c2	za
dobré	dobrý	k2eAgFnSc2d1	dobrá
viditelnosti	viditelnost	k1gFnSc2	viditelnost
jihozápadním	jihozápadní	k2eAgInSc7d1	jihozápadní
směrem	směr	k1gInSc7	směr
Pavlovské	pavlovský	k2eAgInPc4d1	pavlovský
vrchy	vrch	k1gInPc4	vrch
(	(	kIx(	(
<g/>
549	[number]	k4	549
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
5	[number]	k4	5
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
kopec	kopec	k1gInSc1	kopec
Kyjovské	kyjovský	k2eAgFnSc2d1	Kyjovská
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
Babí	babí	k2eAgInSc1d1	babí
lom	lom	k1gInSc1	lom
(	(	kIx(	(
<g/>
417	[number]	k4	417
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
s	s	k7c7	s
vysílačem	vysílač	k1gInSc7	vysílač
Českých	český	k2eAgFnPc2d1	Česká
radiokomunikací	radiokomunikace	k1gFnPc2	radiokomunikace
<g/>
,	,	kIx,	,
místními	místní	k2eAgFnPc7d1	místní
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Strážovský	Strážovský	k2eAgInSc1d1	Strážovský
kopec	kopec	k1gInSc1	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
povodí	povodí	k1gNnSc2	povodí
Dyje	Dyje	k1gFnSc2	Dyje
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
úmoří	úmoří	k1gNnSc2	úmoří
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
údolí	údolí	k1gNnSc2	údolí
mezi	mezi	k7c7	mezi
Ždánickým	ždánický	k2eAgInSc7d1	ždánický
lesem	les	k1gInSc7	les
a	a	k8xC	a
Chřibami	Chřiba	k1gFnPc7	Chřiba
protéká	protékat	k5eAaImIp3nS	protékat
městem	město	k1gNnSc7	město
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
řeka	řeka	k1gFnSc1	řeka
Kyjovka	kyjovka	k1gFnSc1	kyjovka
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
u	u	k7c2	u
chřibské	chřibský	k2eAgFnSc2d1	Chřibská
obce	obec	k1gFnSc2	obec
Staré	Staré	k2eAgFnSc2d1	Staré
Hutě	huť	k1gFnSc2	huť
pod	pod	k7c7	pod
vrcholem	vrchol	k1gInSc7	vrchol
kopce	kopec	k1gInSc2	kopec
Vlčák	vlčák	k1gMnSc1	vlčák
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
560	[number]	k4	560
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Kyjov	Kyjov	k1gInSc1	Kyjov
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
54	[number]	k4	54
<g/>
.	.	kIx.	.
říčním	říční	k2eAgInSc6d1	říční
kilometru	kilometr	k1gInSc6	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gNnSc1	její
koryto	koryto	k1gNnSc1	koryto
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
vyčištěno	vyčistit	k5eAaPmNgNnS	vyčistit
a	a	k8xC	a
zpevněno	zpevnit	k5eAaPmNgNnS	zpevnit
kamenem	kámen	k1gInSc7	kámen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953	[number]	k4	1953
<g/>
-	-	kIx~	-
<g/>
1958	[number]	k4	1958
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Koryčany	Koryčan	k1gMnPc4	Koryčan
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
25	[number]	k4	25
milionů	milion	k4xCgInPc2	milion
m3	m3	k4	m3
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
zdrojů	zdroj	k1gInPc2	zdroj
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
katastru	katastr	k1gInSc6	katastr
města	město	k1gNnSc2	město
má	můj	k3xOp1gFnSc1	můj
Kyjovka	kyjovka	k1gFnSc1	kyjovka
tři	tři	k4xCgInPc4	tři
levostranné	levostranný	k2eAgInPc4d1	levostranný
přítoky	přítok	k1gInPc4	přítok
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Bohuslavicemi	Bohuslavice	k1gFnPc7	Bohuslavice
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
vlévá	vlévat	k5eAaImIp3nS	vlévat
Kratinka	Kratinka	k1gFnSc1	Kratinka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
6,4	[number]	k4	6,4
km	km	kA	km
a	a	k8xC	a
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
Chřibech	Chřiby	k1gInPc6	Chřiby
pod	pod	k7c7	pod
kopcem	kopec	k1gInSc7	kopec
Velká	velká	k1gFnSc1	velká
Ostrá	ostrý	k2eAgFnSc1d1	ostrá
(	(	kIx(	(
<g/>
532	[number]	k4	532
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pramen	pramen	k1gInSc1	pramen
upravený	upravený	k2eAgInSc1d1	upravený
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
má	mít	k5eAaImIp3nS	mít
název	název	k1gInSc1	název
Železňák	železňák	k1gInSc1	železňák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Kratince	kratinko	k6eAd1	kratinko
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
Jestřabicích	Jestřabice	k1gFnPc6	Jestřabice
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
malá	malý	k2eAgFnSc1d1	malá
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Trdliště	trdliště	k1gNnSc2	trdliště
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Bohuslavic	Bohuslavice	k1gFnPc2	Bohuslavice
a	a	k8xC	a
Boršova	Boršův	k2eAgInSc2d1	Boršův
do	do	k7c2	do
Kyjovky	kyjovka	k1gFnSc2	kyjovka
vtéká	vtékat	k5eAaImIp3nS	vtékat
Bohuslavický	Bohuslavický	k2eAgInSc4d1	Bohuslavický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
4,3	[number]	k4	4,3
km	km	kA	km
a	a	k8xC	a
pramení	pramenit	k5eAaImIp3nS	pramenit
pod	pod	k7c7	pod
Lenivou	lenivý	k2eAgFnSc7d1	lenivá
Horou	hora	k1gFnSc7	hora
v	v	k7c6	v
Bohuslavicích	Bohuslavice	k1gFnPc6	Bohuslavice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nětčicích	Nětčice	k1gFnPc6	Nětčice
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vlévá	vlévat	k5eAaImIp3nS	vlévat
potok	potok	k1gInSc4	potok
Malšinka	Malšinka	k1gFnSc1	Malšinka
<g/>
,	,	kIx,	,
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
5,6	[number]	k4	5,6
km	km	kA	km
a	a	k8xC	a
pramenící	pramenící	k2eAgFnSc1d1	pramenící
v	v	k7c6	v
Čeložnicích	Čeložnice	k1gFnPc6	Čeložnice
<g/>
.	.	kIx.	.
</s>
<s>
Potok	potok	k1gInSc1	potok
Bukovanka	Bukovanka	k1gFnSc1	Bukovanka
a	a	k8xC	a
Sobůlský	Sobůlský	k2eAgInSc1d1	Sobůlský
potok	potok	k1gInSc1	potok
tvoří	tvořit	k5eAaImIp3nS	tvořit
část	část	k1gFnSc4	část
západní	západní	k2eAgFnSc2d1	západní
hranice	hranice	k1gFnSc2	hranice
katastru	katastr	k1gInSc2	katastr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bohuslavicích	Bohuslavice	k1gFnPc6	Bohuslavice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Bohuslavické	Bohuslavický	k2eAgFnSc2d1	Bohuslavický
stráně	stráň	k1gFnSc2	stráň
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
byla	být	k5eAaImAgFnS	být
ochrana	ochrana	k1gFnSc1	ochrana
teplomilných	teplomilný	k2eAgNnPc2d1	teplomilné
společenstev	společenstvo	k1gNnPc2	společenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Bohuslavic	Bohuslavice	k1gFnPc2	Bohuslavice
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
památný	památný	k2eAgInSc1d1	památný
strom	strom	k1gInSc1	strom
jeřáb	jeřáb	k1gInSc1	jeřáb
oskeruše	oskeruše	k1gFnSc1	oskeruše
(	(	kIx(	(
<g/>
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Hausnerova	Hausnerův	k2eAgFnSc1d1	Hausnerova
oskeruše	oskeruše	k1gFnSc2	oskeruše
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
ulic	ulice	k1gFnPc2	ulice
Nádražní	nádražní	k2eAgFnSc2d1	nádražní
a	a	k8xC	a
Dobrovského	Dobrovský	k1gMnSc2	Dobrovský
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
chráněna	chránit	k5eAaImNgFnS	chránit
jako	jako	k9	jako
památný	památný	k2eAgInSc4d1	památný
strom	strom	k1gInSc4	strom
lípa	lípa	k1gFnSc1	lípa
velkolistá	velkolistý	k2eAgFnSc1d1	velkolistá
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgNnSc7d1	důležité
odpočinkovým	odpočinkový	k2eAgNnSc7d1	odpočinkové
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
obyvatele	obyvatel	k1gMnPc4	obyvatel
městský	městský	k2eAgInSc1d1	městský
park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Východně	východně	k6eAd1	východně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
s	s	k7c7	s
katastrem	katastr	k1gInSc7	katastr
města	město	k1gNnSc2	město
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
422	[number]	k4	422
sousedí	sousedit	k5eAaImIp3nS	sousedit
malý	malý	k2eAgInSc1d1	malý
les	les	k1gInSc1	les
Chrast	chrast	k1gFnSc1	chrast
(	(	kIx(	(
<g/>
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
i	i	k8xC	i
Chrastí	chrastit	k5eAaImIp3nS	chrastit
<g/>
)	)	kIx)	)
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
asi	asi	k9	asi
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
250	[number]	k4	250
m	m	kA	m
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgInSc2	který
začíná	začínat	k5eAaImIp3nS	začínat
cyklostezka	cyklostezka	k1gFnSc1	cyklostezka
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
přes	přes	k7c4	přes
Kelčany	Kelčan	k1gMnPc4	Kelčan
<g/>
,	,	kIx,	,
Vlkoš	Vlkoš	k1gMnSc1	Vlkoš
a	a	k8xC	a
Skoronice	Skoronice	k1gFnSc1	Skoronice
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
Kyjova	Kyjov	k1gInSc2	Kyjov
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
tři	tři	k4xCgFnPc1	tři
chráněné	chráněný	k2eAgFnPc1d1	chráněná
lípy	lípa	k1gFnPc1	lípa
malolisté	malolistý	k2eAgFnPc1d1	malolistá
(	(	kIx(	(
<g/>
zvané	zvaný	k2eAgFnPc1d1	zvaná
Kelčanské	Kelčanský	k2eAgFnPc1d1	Kelčanský
lípy	lípa	k1gFnPc1	lípa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byly	být	k5eAaImAgInP	být
Kyjovu	Kyjov	k1gInSc3	Kyjov
vráceny	vrácen	k2eAgInPc1d1	vrácen
městské	městský	k2eAgInPc1d1	městský
lesy	les	k1gInPc1	les
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
631,52	[number]	k4	631,52
ha	ha	kA	ha
v	v	k7c6	v
katastrech	katastr	k1gInPc6	katastr
obcí	obec	k1gFnPc2	obec
Moravany	Moravan	k1gMnPc4	Moravan
a	a	k8xC	a
Vřesovice	Vřesovice	k1gFnPc4	Vřesovice
v	v	k7c6	v
Chřibech	Chřiby	k1gInPc6	Chřiby
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lesích	les	k1gInPc6	les
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
dřevin	dřevina	k1gFnPc2	dřevina
nejvíce	nejvíce	k6eAd1	nejvíce
zastoupen	zastoupen	k2eAgInSc1d1	zastoupen
dub	dub	k1gInSc1	dub
(	(	kIx(	(
<g/>
32	[number]	k4	32
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
smrk	smrk	k1gInSc1	smrk
(	(	kIx(	(
<g/>
21	[number]	k4	21
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
buk	buk	k1gInSc1	buk
(	(	kIx(	(
<g/>
15	[number]	k4	15
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
(	(	kIx(	(
<g/>
14	[number]	k4	14
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modřín	modřín	k1gInSc1	modřín
(	(	kIx(	(
<g/>
9	[number]	k4	9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
habr	habr	k1gInSc1	habr
(	(	kIx(	(
<g/>
4	[number]	k4	4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bříza	bříza	k1gFnSc1	bříza
(	(	kIx(	(
<g/>
2	[number]	k4	2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lípa	lípa	k1gFnSc1	lípa
(	(	kIx(	(
<g/>
1	[number]	k4	1
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
douglaska	douglaska	k1gFnSc1	douglaska
(	(	kIx(	(
<g/>
1	[number]	k4	1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
obyvatel	obyvatel	k1gMnPc2	obyvatel
má	mít	k5eAaImIp3nS	mít
chaty	chata	k1gFnPc1	chata
v	v	k7c6	v
rekreačních	rekreační	k2eAgFnPc6d1	rekreační
oblastech	oblast	k1gFnPc6	oblast
Kameňák	Kameňák	k?	Kameňák
a	a	k8xC	a
Zavadilka	Zavadilka	k1gFnSc1	Zavadilka
v	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
obce	obec	k1gFnSc2	obec
Moravany	Moravan	k1gMnPc4	Moravan
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
budovali	budovat	k5eAaImAgMnP	budovat
už	už	k6eAd1	už
v	v	k7c6	v
době	doba	k1gFnSc6	doba
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přírodní	přírodní	k2eAgInPc1d1	přírodní
parky	park	k1gInPc1	park
Chřiby	Chřiby	k1gInPc4	Chřiby
a	a	k8xC	a
Ždánický	ždánický	k2eAgInSc4d1	ždánický
les	les	k1gInSc4	les
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
Bzenecká	bzenecký	k2eAgFnSc1d1	Bzenecká
doubrava	doubrava	k1gFnSc1	doubrava
a	a	k8xC	a
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Bílé	bílý	k2eAgInPc1d1	bílý
Karpaty	Karpaty	k1gInPc4	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
představitelů	představitel	k1gMnPc2	představitel
Kyjova	Kyjov	k1gInSc2	Kyjov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
27	[number]	k4	27
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
radě	rada	k1gFnSc6	rada
9	[number]	k4	9
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
pracují	pracovat	k5eAaImIp3nP	pracovat
3	[number]	k4	3
výbory	výbor	k1gInPc4	výbor
(	(	kIx(	(
<g/>
finanční	finanční	k2eAgInSc4d1	finanční
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
regionální	regionální	k2eAgNnSc4d1	regionální
středisko	středisko	k1gNnSc4	středisko
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
má	mít	k5eAaImIp3nS	mít
12	[number]	k4	12
komisí	komise	k1gFnPc2	komise
(	(	kIx(	(
<g/>
bytová	bytový	k2eAgFnSc1d1	bytová
<g/>
,	,	kIx,	,
prevence	prevence	k1gFnSc1	prevence
kriminality	kriminalita	k1gFnSc2	kriminalita
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
občanské	občanský	k2eAgFnPc4d1	občanská
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
spolkovou	spolkový	k2eAgFnSc4d1	spolková
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
výstavby	výstavba	k1gFnSc2	výstavba
a	a	k8xC	a
urbanismu	urbanismus	k1gInSc2	urbanismus
<g/>
,	,	kIx,	,
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgFnSc2d1	kulturní
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
podnikání	podnikání	k1gNnSc2	podnikání
a	a	k8xC	a
legislativní	legislativní	k2eAgFnSc2d1	legislativní
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc2d1	sociální
a	a	k8xC	a
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgMnSc1d1	sportovní
<g/>
,	,	kIx,	,
školská	školská	k1gFnSc1	školská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Městský	městský	k2eAgInSc1d1	městský
úřad	úřad	k1gInSc1	úřad
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
odborů	odbor	k1gInPc2	odbor
<g/>
.	.	kIx.	.
</s>
<s>
Voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2014	[number]	k4	2014
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
49	[number]	k4	49
%	%	kIx~	%
voličů	volič	k1gMnPc2	volič
ve	v	k7c6	v
12	[number]	k4	12
volebních	volební	k2eAgInPc6d1	volební
okrscích	okrsek	k1gInPc6	okrsek
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
místní	místní	k2eAgNnSc4d1	místní
uskupení	uskupení	k1gNnSc4	uskupení
Sedmadvacítka	Sedmadvacítko	k1gNnSc2	Sedmadvacítko
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
získalo	získat	k5eAaPmAgNnS	získat
39,21	[number]	k4	39,21
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
12	[number]	k4	12
mandátů	mandát	k1gInPc2	mandát
v	v	k7c6	v
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ČSSD	ČSSD	kA	ČSSD
(	(	kIx(	(
<g/>
11,22	[number]	k4	11,22
%	%	kIx~	%
<g/>
,	,	kIx,	,
3	[number]	k4	3
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gMnSc1	KDU-ČSL
(	(	kIx(	(
<g/>
10,48	[number]	k4	10,48
%	%	kIx~	%
<g/>
,	,	kIx,	,
3	[number]	k4	3
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
KSČM	KSČM	kA	KSČM
(	(	kIx(	(
<g/>
10,27	[number]	k4	10,27
<g />
.	.	kIx.	.
</s>
<s>
%	%	kIx~	%
<g/>
,	,	kIx,	,
3	[number]	k4	3
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ODS	ODS	kA	ODS
(	(	kIx(	(
<g/>
7,84	[number]	k4	7,84
%	%	kIx~	%
<g/>
,	,	kIx,	,
2	[number]	k4	2
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ANO	ano	k9	ano
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
7,38	[number]	k4	7,38
%	%	kIx~	%
<g/>
,	,	kIx,	,
2	[number]	k4	2
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sdružení	sdružení	k1gNnSc1	sdružení
nestraníků	nestraník	k1gMnPc2	nestraník
(	(	kIx(	(
<g/>
7,32	[number]	k4	7,32
%	%	kIx~	%
<g/>
,	,	kIx,	,
2	[number]	k4	2
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
zvolilo	zvolit	k5eAaPmAgNnS	zvolit
5	[number]	k4	5
členů	člen	k1gInPc2	člen
Sedmadvacítky	Sedmadvacítka	k1gFnSc2	Sedmadvacítka
nezávislých	závislý	k2eNgInPc2d1	nezávislý
a	a	k8xC	a
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
členu	člen	k1gInSc6	člen
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
<g/>
,	,	kIx,	,
ODS	ODS	kA	ODS
a	a	k8xC	a
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Starostou	Starosta	k1gMnSc7	Starosta
byl	být	k5eAaImAgMnS	být
znovu	znovu	k6eAd1	znovu
zvolen	zvolen	k2eAgMnSc1d1	zvolen
František	František	k1gMnSc1	František
Lukl	Lukl	k1gMnSc1	Lukl
(	(	kIx(	(
<g/>
Sedmadvacítka	Sedmadvacítka	k1gFnSc1	Sedmadvacítka
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
místostarostou	místostarosta	k1gMnSc7	místostarosta
Antonín	Antonín	k1gMnSc1	Antonín
Kuchař	Kuchař	k1gMnSc1	Kuchař
(	(	kIx(	(
<g/>
Sedmadvacítka	Sedmadvacítka	k1gFnSc1	Sedmadvacítka
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
do	do	k7c2	do
třiadvacetičlenného	třiadvacetičlenný	k2eAgNnSc2d1	třiadvacetičlenný
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
dostalo	dostat	k5eAaPmAgNnS	dostat
9	[number]	k4	9
subjektů	subjekt	k1gInPc2	subjekt
<g/>
:	:	kIx,	:
Českomoravská	českomoravský	k2eAgFnSc1d1	Českomoravská
strana	strana	k1gFnSc1	strana
středu	střed	k1gInSc2	střed
s	s	k7c7	s
nezávislými	závislý	k2eNgMnPc7d1	nezávislý
kandidáty	kandidát	k1gMnPc7	kandidát
získala	získat	k5eAaPmAgFnS	získat
5	[number]	k4	5
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
KSČM	KSČM	kA	KSČM
5	[number]	k4	5
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
ODS	ODS	kA	ODS
4	[number]	k4	4
mandáty	mandát	k1gInPc4	mandát
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
3	[number]	k4	3
mandáty	mandát	k1gInPc1	mandát
<g/>
,	,	kIx,	,
KDS	KDS	kA	KDS
2	[number]	k4	2
mandáty	mandát	k1gInPc4	mandát
a	a	k8xC	a
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
obdržely	obdržet	k5eAaPmAgFnP	obdržet
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
SPR-RSČ	SPR-RSČ	k1gFnSc1	SPR-RSČ
<g/>
,	,	kIx,	,
DEU	DEU	kA	DEU
a	a	k8xC	a
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
a	a	k8xC	a
Liberální	liberální	k2eAgFnSc1d1	liberální
strana	strana	k1gFnSc1	strana
národně	národně	k6eAd1	národně
sociální	sociální	k2eAgFnSc1d1	sociální
s	s	k7c7	s
nezávislými	závislý	k2eNgFnPc7d1	nezávislá
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
v	v	k7c6	v
zastupitelstvu	zastupitelstvo	k1gNnSc6	zastupitelstvo
klesl	klesnout	k5eAaPmAgInS	klesnout
počet	počet	k1gInSc1	počet
subjektů	subjekt	k1gInPc2	subjekt
na	na	k7c4	na
šest	šest	k4xCc4	šest
<g/>
:	:	kIx,	:
Sdružení	sdružení	k1gNnSc1	sdružení
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
7	[number]	k4	7
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
KSČM	KSČM	kA	KSČM
4	[number]	k4	4
mandáty	mandát	k1gInPc1	mandát
<g/>
,	,	kIx,	,
ODS	ODS	kA	ODS
4	[number]	k4	4
mandáty	mandát	k1gInPc4	mandát
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
3	[number]	k4	3
mandáty	mandát	k1gInPc1	mandát
<g/>
,	,	kIx,	,
ČSSD	ČSSD	kA	ČSSD
3	[number]	k4	3
mandáty	mandát	k1gInPc4	mandát
a	a	k8xC	a
Unie	unie	k1gFnSc1	unie
svobody	svoboda	k1gFnSc2	svoboda
2	[number]	k4	2
mandáty	mandát	k1gInPc4	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
volilo	volit	k5eAaImAgNnS	volit
již	již	k6eAd1	již
27	[number]	k4	27
zastupitelů	zastupitel	k1gMnPc2	zastupitel
a	a	k8xC	a
nejúspěšnější	úspěšný	k2eAgMnSc1d3	nejúspěšnější
bylo	být	k5eAaImAgNnS	být
uskupení	uskupení	k1gNnSc1	uskupení
Sedmadvacítka	Sedmadvacítko	k1gNnSc2	Sedmadvacítko
nezávislých	závislý	k2eNgInPc2d1	nezávislý
se	se	k3xPyFc4	se
7	[number]	k4	7
mandáty	mandát	k1gInPc7	mandát
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
KSČM	KSČM	kA	KSČM
5	[number]	k4	5
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
koalice	koalice	k1gFnSc1	koalice
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
a	a	k8xC	a
US-DEU	US-DEU	k1gFnSc1	US-DEU
4	[number]	k4	4
mandáty	mandát	k1gInPc1	mandát
<g/>
,	,	kIx,	,
ČSSD	ČSSD	kA	ČSSD
4	[number]	k4	4
mandáty	mandát	k1gInPc1	mandát
<g/>
,	,	kIx,	,
ODS	ODS	kA	ODS
3	[number]	k4	3
mandáty	mandát	k1gInPc4	mandát
<g/>
,	,	kIx,	,
Sdružení	sdružení	k1gNnSc1	sdružení
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
3	[number]	k4	3
mandáty	mandát	k1gInPc1	mandát
a	a	k8xC	a
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
kandidáti	kandidát	k1gMnPc1	kandidát
Skauti	skaut	k1gMnPc1	skaut
a	a	k8xC	a
skautky	skautka	k1gFnSc2	skautka
Kyjova	Kyjov	k1gInSc2	Kyjov
1	[number]	k4	1
mandát	mandát	k1gInSc4	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc4	šest
subjektů	subjekt	k1gInPc2	subjekt
se	se	k3xPyFc4	se
do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
dostalo	dostat	k5eAaPmAgNnS	dostat
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Sedmadvacítka	Sedmadvacítka	k1gFnSc1	Sedmadvacítka
nezávislých	závislý	k2eNgInPc2d1	nezávislý
11	[number]	k4	11
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
KSČM	KSČM	kA	KSČM
4	[number]	k4	4
mandáty	mandát	k1gInPc1	mandát
<g/>
,	,	kIx,	,
ODS	ODS	kA	ODS
4	[number]	k4	4
mandáty	mandát	k1gInPc4	mandát
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
3	[number]	k4	3
mandáty	mandát	k1gInPc1	mandát
<g/>
,	,	kIx,	,
ČSSD	ČSSD	kA	ČSSD
3	[number]	k4	3
mandáty	mandát	k1gInPc4	mandát
a	a	k8xC	a
Sdružení	sdružení	k1gNnSc4	sdružení
občanů	občan	k1gMnPc2	občan
Kyjova	Kyjov	k1gInSc2	Kyjov
2	[number]	k4	2
mandáty	mandát	k1gInPc4	mandát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
uskupení	uskupení	k1gNnSc1	uskupení
Sedmadvacítka	Sedmadvacítka	k1gFnSc1	Sedmadvacítka
nezávislých	závislý	k2eNgInPc2d1	nezávislý
11	[number]	k4	11
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
ČSSD	ČSSD	kA	ČSSD
5	[number]	k4	5
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
KSČM	KSČM	kA	KSČM
4	[number]	k4	4
mandáty	mandát	k1gInPc1	mandát
<g/>
,	,	kIx,	,
ODS	ODS	kA	ODS
3	[number]	k4	3
mandáty	mandát	k1gInPc4	mandát
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
3	[number]	k4	3
mandáty	mandát	k1gInPc4	mandát
a	a	k8xC	a
SPOZ	SPOZ	kA	SPOZ
1	[number]	k4	1
mandát	mandát	k1gInSc4	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
je	být	k5eAaImIp3nS	být
Kyjov	Kyjov	k1gInSc4	Kyjov
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterou	který	k3yIgFnSc7	který
patří	patřit	k5eAaImIp3nS	patřit
obce	obec	k1gFnPc1	obec
s	s	k7c7	s
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
Bzenec	Bzenec	k1gInSc1	Bzenec
a	a	k8xC	a
Ždánice	Ždánice	k1gFnPc1	Ždánice
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
správu	správa	k1gFnSc4	správa
Kyjova	Kyjov	k1gInSc2	Kyjov
tak	tak	k6eAd1	tak
spadá	spadat	k5eAaImIp3nS	spadat
42	[number]	k4	42
okolních	okolní	k2eAgFnPc2d1	okolní
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
55	[number]	k4	55
800	[number]	k4	800
obyvateli	obyvatel	k1gMnPc7	obyvatel
včetně	včetně	k7c2	včetně
samotného	samotný	k2eAgNnSc2d1	samotné
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
mikroregionu	mikroregion	k1gInSc2	mikroregion
Babí	babit	k5eAaImIp3nS	babit
lom	lom	k1gInSc1	lom
a	a	k8xC	a
Sdružení	sdružení	k1gNnSc1	sdružení
obcí	obec	k1gFnPc2	obec
Severovýchod	severovýchod	k1gInSc1	severovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
senátního	senátní	k2eAgInSc2d1	senátní
obvodu	obvod	k1gInSc2	obvod
č.	č.	k?	č.
79	[number]	k4	79
-	-	kIx~	-
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yQgInSc4	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Škromach	Škromach	k1gMnSc1	Škromach
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1850	[number]	k4	1850
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
byl	být	k5eAaImAgInS	být
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
v	v	k7c6	v
Gottwaldovském	gottwaldovský	k2eAgInSc6d1	gottwaldovský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
okresu	okres	k1gInSc2	okres
Hodonín	Hodonín	k1gInSc1	Hodonín
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1868	[number]	k4	1868
<g/>
-	-	kIx~	-
<g/>
1948	[number]	k4	1948
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
sídlo	sídlo	k1gNnSc1	sídlo
soudního	soudní	k2eAgInSc2d1	soudní
okresu	okres	k1gInSc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
přes	přes	k7c4	přes
11	[number]	k4	11
400	[number]	k4	400
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
tak	tak	k6eAd1	tak
po	po	k7c6	po
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
okresních	okresní	k2eAgNnPc6d1	okresní
městech	město	k1gNnPc6	město
největším	veliký	k2eAgNnSc7d3	veliký
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Kyjovské	kyjovský	k2eAgNnSc1d1	Kyjovské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
tvoří	tvořit	k5eAaImIp3nS	tvořit
7,4	[number]	k4	7,4
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
okresu	okres	k1gInSc2	okres
a	a	k8xC	a
1	[number]	k4	1
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
činil	činit	k5eAaImAgInS	činit
43,8	[number]	k4	43,8
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
Kyjov	Kyjov	k1gInSc4	Kyjov
žilo	žít	k5eAaImAgNnS	žít
8	[number]	k4	8
326	[number]	k4	326
(	(	kIx(	(
<g/>
72,6	[number]	k4	72,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
Nětčicích	Nětčice	k1gFnPc6	Nětčice
1	[number]	k4	1
807	[number]	k4	807
(	(	kIx(	(
<g/>
15,8	[number]	k4	15,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Boršově	Boršův	k2eAgMnSc6d1	Boršův
683	[number]	k4	683
(	(	kIx(	(
<g/>
6	[number]	k4	6
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Bohuslavicích	Bohuslavice	k1gFnPc6	Bohuslavice
646	[number]	k4	646
(	(	kIx(	(
<g/>
5,6	[number]	k4	5,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kyjovští	kyjovský	k2eAgMnPc1d1	kyjovský
Židé	Žid	k1gMnPc1	Žid
měli	mít	k5eAaImAgMnP	mít
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
vlastní	vlastní	k2eAgFnSc4d1	vlastní
politickou	politický	k2eAgFnSc4d1	politická
obec	obec	k1gFnSc4	obec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
z	z	k7c2	z
města	město	k1gNnSc2	město
zmizela	zmizet	k5eAaPmAgFnS	zmizet
početná	početný	k2eAgFnSc1d1	početná
židovská	židovská	k1gFnSc1	židovská
i	i	k8xC	i
německá	německý	k2eAgFnSc1d1	německá
menšina	menšina	k1gFnSc1	menšina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
budováním	budování	k1gNnSc7	budování
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
městských	městský	k2eAgNnPc2d1	Městské
sídlišť	sídliště	k1gNnPc2	sídliště
po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
do	do	k7c2	do
města	město	k1gNnSc2	město
postupně	postupně	k6eAd1	postupně
stěhovalo	stěhovat	k5eAaImAgNnS	stěhovat
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
z	z	k7c2	z
okolního	okolní	k2eAgInSc2d1	okolní
venkova	venkov	k1gInSc2	venkov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
2	[number]	k4	2
300	[number]	k4	300
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
1	[number]	k4	1
967	[number]	k4	967
rodinných	rodinný	k2eAgInPc2d1	rodinný
<g/>
.	.	kIx.	.
</s>
<s>
Bytů	byt	k1gInPc2	byt
bylo	být	k5eAaImAgNnS	být
5	[number]	k4	5
0	[number]	k4	0
<g/>
59	[number]	k4	59
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
4	[number]	k4	4
470	[number]	k4	470
obydlených	obydlený	k2eAgInPc2d1	obydlený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
stále	stále	k6eAd1	stále
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	s	k7c7	s
5	[number]	k4	5
431	[number]	k4	431
(	(	kIx(	(
<g/>
47,4	[number]	k4	47,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnSc1	obyvatel
Kyjova	Kyjov	k1gInSc2	Kyjov
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
2	[number]	k4	2
708	[number]	k4	708
(	(	kIx(	(
<g/>
23,6	[number]	k4	23,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
k	k	k7c3	k
moravské	moravský	k2eAgFnSc3d1	Moravská
<g/>
,	,	kIx,	,
87	[number]	k4	87
(	(	kIx(	(
<g/>
0,8	[number]	k4	0,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
ke	k	k7c3	k
slovenské	slovenský	k2eAgFnSc3d1	slovenská
<g/>
,	,	kIx,	,
16	[number]	k4	16
(	(	kIx(	(
<g/>
0,1	[number]	k4	0,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
k	k	k7c3	k
vietnamské	vietnamský	k2eAgFnSc3d1	vietnamská
a	a	k8xC	a
7	[number]	k4	7
k	k	k7c3	k
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
<g/>
.	.	kIx.	.
2	[number]	k4	2
689	[number]	k4	689
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
23,5	[number]	k4	23,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
svou	svůj	k3xOyFgFnSc4	svůj
národnost	národnost	k1gFnSc4	národnost
neuvedlo	uvést	k5eNaPmAgNnS	uvést
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	s	k7c7	s
3	[number]	k4	3
530	[number]	k4	530
(	(	kIx(	(
<g/>
30,8	[number]	k4	30,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnSc1	obyvatel
Kyjova	Kyjov	k1gInSc2	Kyjov
označilo	označit	k5eAaPmAgNnS	označit
za	za	k7c4	za
věřící	věřící	k1gFnSc4	věřící
<g/>
.	.	kIx.	.
2	[number]	k4	2
192	[number]	k4	192
(	(	kIx(	(
<g/>
19,1	[number]	k4	19,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
Římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
60	[number]	k4	60
(	(	kIx(	(
<g/>
0,5	[number]	k4	0,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
40	[number]	k4	40
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
0,3	[number]	k4	0,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
československých	československý	k2eAgMnPc2d1	československý
husitů	husita	k1gMnPc2	husita
<g/>
,	,	kIx,	,
32	[number]	k4	32
(	(	kIx(	(
<g/>
0,3	[number]	k4	0,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
Svědků	svědek	k1gMnPc2	svědek
Jehovových	Jehovův	k2eAgMnPc2d1	Jehovův
<g/>
,	,	kIx,	,
0,1	[number]	k4	0,1
%	%	kIx~	%
adventistů	adventista	k1gMnPc2	adventista
<g/>
.	.	kIx.	.
3	[number]	k4	3
025	[number]	k4	025
(	(	kIx(	(
<g/>
26,4	[number]	k4	26,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
označilo	označit	k5eAaPmAgNnS	označit
jako	jako	k9	jako
bez	bez	k7c2	bez
náboženské	náboženský	k2eAgFnSc2d1	náboženská
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
4	[number]	k4	4
907	[number]	k4	907
(	(	kIx(	(
<g/>
42,8	[number]	k4	42,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
víry	víra	k1gFnSc2	víra
neodpovědělo	odpovědět	k5eNaPmAgNnS	odpovědět
<g/>
.	.	kIx.	.
</s>
<s>
Kyjov	Kyjov	k1gInSc1	Kyjov
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
kyjovského	kyjovský	k2eAgInSc2d1	kyjovský
děkanátu	děkanát	k1gInSc2	děkanát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
děkanátu	děkanát	k1gInSc6	děkanát
je	být	k5eAaImIp3nS	být
12	[number]	k4	12
farností	farnost	k1gFnPc2	farnost
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
děkanem	děkan	k1gMnSc7	děkan
kyjovský	kyjovský	k2eAgMnSc1d1	kyjovský
farář	farář	k1gMnSc1	farář
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mrázek	Mrázek	k1gMnSc1	Mrázek
<g/>
.	.	kIx.	.
</s>
<s>
Děkanský	děkanský	k2eAgInSc1d1	děkanský
kostel	kostel	k1gInSc1	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
něj	on	k3xPp3gMnSc4	on
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
ještě	ještě	k6eAd1	ještě
kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Josefa	Josef	k1gMnSc2	Josef
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Josefa	Josef	k1gMnSc2	Josef
Kalasanského	Kalasanský	k2eAgMnSc2d1	Kalasanský
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Rocha	Rocha	k1gFnSc1	Rocha
<g/>
,	,	kIx,	,
kaple	kaple	k1gFnSc1	kaple
v	v	k7c6	v
Nětčicích	Nětčice	k1gFnPc6	Nětčice
<g/>
,	,	kIx,	,
morový	morový	k2eAgInSc1d1	morový
sloup	sloup	k1gInSc1	sloup
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
sochy	socha	k1gFnSc2	socha
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
a	a	k8xC	a
sv.	sv.	kA	sv.
Václava	Václava	k1gFnSc1	Václava
a	a	k8xC	a
několik	několik	k4yIc4	několik
křížů	kříž	k1gInPc2	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kyjovské	kyjovský	k2eAgFnSc2d1	Kyjovská
farnosti	farnost	k1gFnSc2	farnost
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
sousední	sousední	k2eAgFnPc1d1	sousední
obce	obec	k1gFnPc1	obec
Sobůlky	Sobůlka	k1gFnSc2	Sobůlka
s	s	k7c7	s
kaplí	kaple	k1gFnSc7	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Nejsvětějšího	nejsvětější	k2eAgNnSc2d1	nejsvětější
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
Bukovany	Bukovan	k1gMnPc4	Bukovan
s	s	k7c7	s
kaplí	kaple	k1gFnSc7	kaple
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslavice	Bohuslavice	k1gFnPc1	Bohuslavice
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
sv.	sv.	kA	sv.
Filipa	Filip	k1gMnSc4	Filip
a	a	k8xC	a
Jakuba	Jakub	k1gMnSc4	Jakub
jsou	být	k5eAaImIp3nP	být
samostatná	samostatný	k2eAgFnSc1d1	samostatná
farnost	farnost	k1gFnSc1	farnost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
hodonínského	hodonínský	k2eAgInSc2d1	hodonínský
děkanátu	děkanát	k1gInSc2	děkanát
a	a	k8xC	a
brněnské	brněnský	k2eAgFnSc2d1	brněnská
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
Farnost	farnost	k1gFnSc1	farnost
spravuje	spravovat	k5eAaImIp3nS	spravovat
jako	jako	k8xC	jako
administrátor	administrátor	k1gMnSc1	administrátor
excurrendo	excurrendo	k1gNnSc1	excurrendo
farář	farář	k1gMnSc1	farář
Miroslav	Miroslav	k1gMnSc1	Miroslav
Sedlák	Sedlák	k1gMnSc1	Sedlák
<g/>
.	.	kIx.	.
</s>
<s>
Modlitebna	modlitebna	k1gFnSc1	modlitebna
Českobratrské	českobratrský	k2eAgFnSc2d1	Českobratrská
církve	církev	k1gFnSc2	církev
evangelické	evangelický	k2eAgFnSc2d1	evangelická
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Dobrovského	Dobrovského	k2eAgFnSc6d1	Dobrovského
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgInSc1d1	zdejší
sbor	sbor	k1gInSc1	sbor
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
kazatelská	kazatelský	k2eAgFnSc1d1	kazatelská
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Sbor	sbor	k1gInSc1	sbor
má	mít	k5eAaImIp3nS	mít
kazatelské	kazatelský	k2eAgFnPc4d1	kazatelská
stanice	stanice	k1gFnPc4	stanice
ve	v	k7c6	v
Bzenci	Bzenec	k1gInSc6	Bzenec
a	a	k8xC	a
Veselí	veselí	k1gNnSc1	veselí
nad	nad	k7c7	nad
Moravou	Morava	k1gFnSc7	Morava
a	a	k8xC	a
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
Východomoravského	východomoravský	k2eAgInSc2d1	východomoravský
seniorátu	seniorát	k1gInSc2	seniorát
<g/>
.	.	kIx.	.
</s>
<s>
Farářkou	farářka	k1gFnSc7	farářka
byla	být	k5eAaImAgFnS	být
Erika	Erika	k1gFnSc1	Erika
Petříčková	Petříčková	k1gFnSc1	Petříčková
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
bratrská	bratrský	k2eAgFnSc1d1	bratrská
měla	mít	k5eAaImAgFnS	mít
modlitebnu	modlitebna	k1gFnSc4	modlitebna
na	na	k7c6	na
Urbanově	Urbanův	k2eAgFnSc6d1	Urbanova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
kazatelem	kazatel	k1gMnSc7	kazatel
Mirko	Mirko	k1gMnSc1	Mirko
Tichý	Tichý	k1gMnSc1	Tichý
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
československá	československý	k2eAgFnSc1d1	Československá
husitská	husitský	k2eAgFnSc1d1	husitská
měla	mít	k5eAaImAgFnS	mít
kostel	kostel	k1gInSc4	kostel
na	na	k7c6	na
Seifertově	Seifertův	k2eAgNnSc6d1	Seifertovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgInSc1d1	zdejší
sbor	sbor	k1gInSc1	sbor
patřil	patřit	k5eAaImAgInS	patřit
do	do	k7c2	do
vikariátu	vikariát	k1gInSc2	vikariát
Východní	východní	k2eAgFnSc1d1	východní
Morava	Morava	k1gFnSc1	Morava
brněnské	brněnský	k2eAgFnSc2d1	brněnská
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
zde	zde	k6eAd1	zde
nebyl	být	k5eNaImAgMnS	být
farář	farář	k1gMnSc1	farář
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
byl	být	k5eAaImAgInS	být
spravován	spravovat	k5eAaImNgInS	spravovat
farářkou	farářka	k1gFnSc7	farářka
Ivou	Iva	k1gFnSc7	Iva
Pospíšilovou	Pospíšilová	k1gFnSc7	Pospíšilová
z	z	k7c2	z
Uherského	uherský	k2eAgInSc2d1	uherský
Brodu	Brod	k1gInSc2	Brod
a	a	k8xC	a
kazatelkou	kazatelka	k1gFnSc7	kazatelka
Janou	Jana	k1gFnSc7	Jana
Rapantovou	Rapantův	k2eAgFnSc7d1	Rapantův
<g/>
.	.	kIx.	.
</s>
<s>
Adventisté	adventista	k1gMnPc1	adventista
sedmého	sedmý	k4xOgInSc2	sedmý
dne	den	k1gInSc2	den
měli	mít	k5eAaImAgMnP	mít
sídlo	sídlo	k1gNnSc4	sídlo
sboru	sbor	k1gInSc2	sbor
na	na	k7c6	na
Svatoborské	svatoborský	k2eAgFnSc6d1	Svatoborská
ulici	ulice	k1gFnSc6	ulice
a	a	k8xC	a
Svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
si	se	k3xPyFc3	se
kolem	kolo	k1gNnSc7	kolo
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
postavili	postavit	k5eAaPmAgMnP	postavit
Sál	sál	k1gInSc4	sál
království	království	k1gNnSc2	království
na	na	k7c6	na
Boršovské	Boršovský	k2eAgFnSc6d1	Boršovský
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
zde	zde	k6eAd1	zde
působila	působit	k5eAaImAgFnS	působit
Apoštolská	apoštolský	k2eAgFnSc1d1	apoštolská
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
scházeli	scházet	k5eAaImAgMnP	scházet
v	v	k7c6	v
Obecním	obecní	k2eAgInSc6d1	obecní
domě	dům	k1gInSc6	dům
na	na	k7c6	na
Nětčické	Nětčický	k2eAgFnSc6d1	Nětčický
ulici	ulice	k1gFnSc6	ulice
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
mateřský	mateřský	k2eAgInSc1d1	mateřský
sbor	sbor	k1gInSc1	sbor
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Hodoníně	Hodonín	k1gInSc6	Hodonín
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všichni	všechen	k3xTgMnPc1	všechen
kyjovští	kyjovský	k2eAgMnPc1d1	kyjovský
židé	žid	k1gMnPc1	žid
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
transportováni	transportovat	k5eAaBmNgMnP	transportovat
do	do	k7c2	do
Uherského	uherský	k2eAgInSc2d1	uherský
Brodu	Brod	k1gInSc2	Brod
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
většinou	většina	k1gFnSc7	většina
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
v	v	k7c6	v
Terezíně	Terezín	k1gInSc6	Terezín
a	a	k8xC	a
Osvětimi	Osvětim	k1gFnSc6	Osvětim
<g/>
.	.	kIx.	.
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1	židovská
synagoga	synagoga	k1gFnSc1	synagoga
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1851	[number]	k4	1851
<g/>
-	-	kIx~	-
<g/>
1852	[number]	k4	1852
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zbourána	zbourán	k2eAgFnSc1d1	zbourána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
místě	místo	k1gNnSc6	místo
u	u	k7c2	u
kulturního	kulturní	k2eAgInSc2d1	kulturní
domu	dům	k1gInSc2	dům
postaven	postavit	k5eAaPmNgInS	postavit
pomník	pomník	k1gInSc1	pomník
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
Davidovy	Davidův	k2eAgFnSc2d1	Davidova
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc1	pomník
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
brněnského	brněnský	k2eAgMnSc2d1	brněnský
architekta	architekt	k1gMnSc2	architekt
Jana	Jan	k1gMnSc2	Jan
Klenovského	Klenovský	k2eAgMnSc2d1	Klenovský
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
sochař	sochař	k1gMnSc1	sochař
Nikos	Nikosa	k1gFnPc2	Nikosa
Armutidis	Armutidis	k1gFnPc2	Armutidis
<g/>
.	.	kIx.	.
</s>
<s>
Židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
také	také	k9	také
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
a	a	k8xC	a
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
kyjovské	kyjovský	k2eAgFnSc2d1	Kyjovská
nemocnice	nemocnice	k1gFnSc2	nemocnice
je	být	k5eAaImIp3nS	být
pomník	pomník	k1gInSc4	pomník
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
2	[number]	k4	2
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
nepřežili	přežít	k5eNaPmAgMnP	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
sídliště	sídliště	k1gNnSc2	sídliště
Lidická	lidický	k2eAgFnSc1d1	Lidická
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
starý	starý	k2eAgInSc4d1	starý
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgInSc1d1	kulturní
život	život	k1gInSc1	život
ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
odehrával	odehrávat	k5eAaImAgMnS	odehrávat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
městských	městský	k2eAgInPc6d1	městský
spolcích	spolek	k1gInPc6	spolek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
členily	členit	k5eAaImAgInP	členit
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
a	a	k8xC	a
německé	německý	k2eAgFnSc6d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Spolky	spolek	k1gInPc1	spolek
se	se	k3xPyFc4	se
scházely	scházet	k5eAaImAgInP	scházet
především	především	k9	především
v	v	k7c6	v
hostincích	hostinec	k1gInPc6	hostinec
-	-	kIx~	-
české	český	k2eAgFnPc4d1	Česká
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Michálek	Michálek	k1gMnSc1	Michálek
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
i	i	k8xC	i
na	na	k7c6	na
Občanské	občanský	k2eAgFnSc6d1	občanská
Záložně	záložna	k1gFnSc6	záložna
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
pošta	pošta	k1gFnSc1	pošta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
tělocvičná	tělocvičný	k2eAgFnSc1d1	Tělocvičná
jednota	jednota	k1gFnSc1	jednota
Sokol	Sokol	k1gMnSc1	Sokol
a	a	k8xC	a
po	po	k7c6	po
vybudování	vybudování	k1gNnSc6	vybudování
sokolovny	sokolovna	k1gFnSc2	sokolovna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
se	se	k3xPyFc4	se
divadelní	divadelní	k2eAgNnPc1d1	divadelní
představení	představení	k1gNnPc1	představení
<g/>
,	,	kIx,	,
přednášky	přednáška	k1gFnPc1	přednáška
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
akce	akce	k1gFnPc1	akce
odehrávaly	odehrávat	k5eAaImAgFnP	odehrávat
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
podobně	podobně	k6eAd1	podobně
využívali	využívat	k5eAaImAgMnP	využívat
hotel	hotel	k1gInSc4	hotel
U	u	k7c2	u
černého	černé	k1gNnSc2	černé
orla	orel	k1gMnSc2	orel
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
přejmenován	přejmenovat	k5eAaPmNgMnS	přejmenovat
na	na	k7c6	na
Slavii	slavie	k1gFnSc6	slavie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Záložna	záložna	k1gFnSc1	záložna
a	a	k8xC	a
sokolovna	sokolovna	k1gFnSc1	sokolovna
byly	být	k5eAaImAgInP	být
centrem	centrum	k1gNnSc7	centrum
kultury	kultura	k1gFnSc2	kultura
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
postaven	postaven	k2eAgInSc1d1	postaven
Dům	dům	k1gInSc1	dům
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
součástí	součást	k1gFnSc7	součást
kultury	kultura	k1gFnSc2	kultura
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
folklór	folklór	k1gInSc1	folklór
<g/>
,	,	kIx,	,
reprezentovaný	reprezentovaný	k2eAgInSc1d1	reprezentovaný
především	především	k6eAd1	především
národopisnou	národopisný	k2eAgFnSc7d1	národopisná
slavností	slavnost	k1gFnSc7	slavnost
Slovácký	slovácký	k2eAgInSc1d1	slovácký
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
je	být	k5eAaImIp3nS	být
pořádána	pořádán	k2eAgFnSc1d1	pořádána
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
letech	let	k1gInPc6	let
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
srpna	srpen	k1gInSc2	srpen
Kyjovské	kyjovský	k2eAgFnSc2d1	Kyjovská
letní	letní	k2eAgFnSc2d1	letní
slavnosti	slavnost	k1gFnSc2	slavnost
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
dožínkami	dožínky	k1gFnPc7	dožínky
a	a	k8xC	a
mariánskou	mariánský	k2eAgFnSc7d1	Mariánská
poutí	pouť	k1gFnSc7	pouť
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
jsou	být	k5eAaImIp3nP	být
tradiční	tradiční	k2eAgInPc4d1	tradiční
Martinské	martinský	k2eAgInPc4d1	martinský
hody	hod	k1gInPc4	hod
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
folklorních	folklorní	k2eAgFnPc2d1	folklorní
akcí	akce	k1gFnPc2	akce
-	-	kIx~	-
fašank	fašank	k?	fašank
<g/>
,	,	kIx,	,
Velikonoční	velikonoční	k2eAgFnSc1d1	velikonoční
beseda	beseda	k1gFnSc1	beseda
u	u	k7c2	u
cimbálu	cimbál	k1gInSc2	cimbál
s	s	k7c7	s
koštem	košt	k1gInSc7	košt
vín	víno	k1gNnPc2	víno
<g/>
,	,	kIx,	,
stavění	stavění	k1gNnSc1	stavění
a	a	k8xC	a
kácení	kácení	k1gNnSc1	kácení
máje	máj	k1gInSc2	máj
<g/>
,	,	kIx,	,
Kateřinská	kateřinský	k2eAgFnSc1d1	Kateřinská
zástěrková	zástěrkový	k2eAgFnSc1d1	zástěrková
zábava	zábava	k1gFnSc1	zábava
<g/>
,	,	kIx,	,
Vánoční	vánoční	k2eAgFnSc1d1	vánoční
beseda	beseda	k1gFnSc1	beseda
u	u	k7c2	u
cimbálu	cimbál	k1gInSc2	cimbál
aj.	aj.	kA	aj.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Slovácký	slovácký	k2eAgInSc1d1	slovácký
krúžek	krúžek	k1gInSc1	krúžek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dodnes	dodnes	k6eAd1	dodnes
působí	působit	k5eAaImIp3nS	působit
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Slovácký	slovácký	k2eAgInSc1d1	slovácký
soubor	soubor	k1gInSc1	soubor
Kyjov	Kyjov	k1gInSc1	Kyjov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
zde	zde	k6eAd1	zde
hraje	hrát	k5eAaImIp3nS	hrát
Cimbálová	cimbálový	k2eAgFnSc1d1	cimbálová
muzika	muzika	k1gFnSc1	muzika
Jury	jury	k1gFnSc1	jury
Petrů	Petrů	k1gFnSc1	Petrů
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
Cimbálová	cimbálový	k2eAgFnSc1d1	cimbálová
muzika	muzika	k1gFnSc1	muzika
Javor	javor	k1gInSc1	javor
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
Cimbálová	cimbálový	k2eAgFnSc1d1	cimbálová
muzika	muzika	k1gFnSc1	muzika
Pavla	Pavel	k1gMnSc2	Pavel
Růžičky	Růžička	k1gMnSc2	Růžička
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
mužský	mužský	k2eAgInSc4d1	mužský
pěvecký	pěvecký	k2eAgInSc4d1	pěvecký
sbor	sbor	k1gInSc4	sbor
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
ženský	ženský	k2eAgInSc4d1	ženský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
pěvecký	pěvecký	k2eAgInSc1d1	pěvecký
sbor	sbor	k1gInSc1	sbor
Tetky	tetka	k1gFnSc2	tetka
z	z	k7c2	z
Kyjova	Kyjov	k1gInSc2	Kyjov
a	a	k8xC	a
pak	pak	k6eAd1	pak
další	další	k2eAgFnPc4d1	další
Tragačnice	Tragačnice	k1gFnPc4	Tragačnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Slováckého	slovácký	k2eAgInSc2d1	slovácký
souboru	soubor	k1gInSc2	soubor
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
dodnes	dodnes	k6eAd1	dodnes
působící	působící	k2eAgInSc1d1	působící
Dětský	dětský	k2eAgInSc1d1	dětský
národopisný	národopisný	k2eAgInSc1d1	národopisný
soubor	soubor	k1gInSc1	soubor
Kyjovánek	Kyjovánka	k1gFnPc2	Kyjovánka
a	a	k8xC	a
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Kyjovánku	Kyjovánek	k1gInSc2	Kyjovánek
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
dětská	dětský	k2eAgFnSc1d1	dětská
Cimbálová	cimbálový	k2eAgFnSc1d1	cimbálová
muzika	muzika	k1gFnSc1	muzika
Friška	Friška	k1gFnSc1	Friška
<g/>
.	.	kIx.	.
</s>
<s>
Dechová	dechový	k2eAgFnSc1d1	dechová
hudba	hudba	k1gFnSc1	hudba
Gloria	Gloria	k1gFnSc1	Gloria
ve	v	k7c6	v
městě	město	k1gNnSc6	město
působí	působit	k5eAaImIp3nS	působit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
folklorní	folklorní	k2eAgFnSc2d1	folklorní
tradice	tradice	k1gFnSc2	tradice
je	být	k5eAaImIp3nS	být
i	i	k9	i
kyjovský	kyjovský	k2eAgInSc1d1	kyjovský
kroj	kroj	k1gInSc1	kroj
<g/>
.	.	kIx.	.
</s>
<s>
Vlastivědné	vlastivědný	k2eAgNnSc1d1	Vlastivědné
muzeum	muzeum	k1gNnSc1	muzeum
Kyjov	Kyjov	k1gInSc1	Kyjov
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
zámečku	zámeček	k1gInSc2	zámeček
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
Palackého	Palackého	k2eAgFnSc6d1	Palackého
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc6d3	nejstarší
budově	budova	k1gFnSc6	budova
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
častých	častý	k2eAgFnPc2d1	častá
tematických	tematický	k2eAgFnPc2d1	tematická
výstav	výstava	k1gFnPc2	výstava
má	mít	k5eAaImIp3nS	mít
stálou	stálý	k2eAgFnSc4d1	stálá
archeologickou	archeologický	k2eAgFnSc4d1	archeologická
<g/>
,	,	kIx,	,
etnografickou	etnografický	k2eAgFnSc4d1	etnografická
a	a	k8xC	a
přírodovědnou	přírodovědný	k2eAgFnSc4d1	přírodovědná
expozici	expozice	k1gFnSc4	expozice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
40	[number]	k4	40
000	[number]	k4	000
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
muzea	muzeum	k1gNnSc2	muzeum
je	být	k5eAaImIp3nS	být
i	i	k9	i
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
základem	základ	k1gInSc7	základ
byla	být	k5eAaImAgFnS	být
někdejší	někdejší	k2eAgFnSc1d1	někdejší
knihovna	knihovna	k1gFnSc1	knihovna
kyjovského	kyjovský	k2eAgNnSc2d1	Kyjovské
piaristického	piaristický	k2eAgNnSc2d1	Piaristické
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přes	přes	k7c4	přes
15	[number]	k4	15
000	[number]	k4	000
svazků	svazek	k1gInPc2	svazek
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
téměř	téměř	k6eAd1	téměř
1	[number]	k4	1
400	[number]	k4	400
starých	starý	k2eAgMnPc2d1	starý
tisků	tisek	k1gMnPc2	tisek
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc4d3	nejstarší
tisky	tisk	k1gInPc4	tisk
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
byl	být	k5eAaImAgMnS	být
pověřen	pověřen	k2eAgMnSc1d1	pověřen
profesor	profesor	k1gMnSc1	profesor
kyjovského	kyjovský	k2eAgNnSc2d1	Kyjovské
gymnázia	gymnázium	k1gNnSc2	gymnázium
Jan	Jan	k1gMnSc1	Jan
Kučera	Kučera	k1gMnSc1	Kučera
uspořádáním	uspořádání	k1gNnSc7	uspořádání
městského	městský	k2eAgInSc2d1	městský
archivu	archiv	k1gInSc2	archiv
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
městském	městský	k2eAgInSc6d1	městský
úřadu	úřad	k1gInSc6	úřad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
růstu	růst	k1gInSc3	růst
byla	být	k5eAaImAgFnS	být
přemístěna	přemístit	k5eAaPmNgFnS	přemístit
do	do	k7c2	do
chudobince	chudobinec	k1gInSc2	chudobinec
Dominika	Dominik	k1gMnSc4	Dominik
Jurovského	Jurovský	k1gMnSc4	Jurovský
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Základní	základní	k2eAgFnSc1d1	základní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Kučera	Kučera	k1gMnSc1	Kučera
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
správcem	správce	k1gMnSc7	správce
muzea	muzeum	k1gNnSc2	muzeum
i	i	k8xC	i
archivu	archiv	k1gInSc2	archiv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
se	s	k7c7	s
správcem	správce	k1gMnSc7	správce
muzea	muzeum	k1gNnSc2	muzeum
stal	stát	k5eAaPmAgMnS	stát
profesor	profesor	k1gMnSc1	profesor
Ludvík	Ludvík	k1gMnSc1	Ludvík
Kalus	kalus	k1gInSc1	kalus
a	a	k8xC	a
během	během	k7c2	během
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
sbírky	sbírka	k1gFnPc4	sbírka
přemístit	přemístit	k5eAaPmF	přemístit
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
zámečku	zámeček	k1gInSc2	zámeček
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
správce	správce	k1gMnSc1	správce
zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
a	a	k8xC	a
poté	poté	k6eAd1	poté
jako	jako	k9	jako
vedoucí	vedoucí	k2eAgFnPc1d1	vedoucí
muzejní	muzejní	k2eAgFnPc1d1	muzejní
knihovny	knihovna	k1gFnPc1	knihovna
a	a	k8xC	a
konzervátor	konzervátor	k1gInSc1	konzervátor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
je	být	k5eAaImIp3nS	být
muzeum	muzeum	k1gNnSc1	muzeum
pobočkou	pobočka	k1gFnSc7	pobočka
Okresního	okresní	k2eAgNnSc2d1	okresní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Hodoníně	Hodonín	k1gInSc6	Hodonín
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Masarykova	Masarykův	k2eAgNnSc2d1	Masarykovo
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Hodoníně	Hodonín	k1gInSc6	Hodonín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
získalo	získat	k5eAaPmAgNnS	získat
novou	nový	k2eAgFnSc4d1	nová
přístavbu	přístavba	k1gFnSc4	přístavba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
umístěny	umístěn	k2eAgInPc1d1	umístěn
depozitáře	depozitář	k1gInPc1	depozitář
<g/>
,	,	kIx,	,
konzervátorská	konzervátorský	k2eAgFnSc1d1	konzervátorská
dílna	dílna	k1gFnSc1	dílna
<g/>
,	,	kIx,	,
knihovna	knihovna	k1gFnSc1	knihovna
a	a	k8xC	a
pracovny	pracovna	k1gFnSc2	pracovna
odborných	odborný	k2eAgMnPc2d1	odborný
pracovníků	pracovník	k1gMnPc2	pracovník
a	a	k8xC	a
také	také	k9	také
víceúčelový	víceúčelový	k2eAgInSc1d1	víceúčelový
sál	sál	k1gInSc1	sál
pro	pro	k7c4	pro
přednášky	přednáška	k1gFnPc4	přednáška
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
léta	léto	k1gNnPc4	léto
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
2018	[number]	k4	2018
je	být	k5eAaImIp3nS	být
plánována	plánovat	k5eAaImNgFnS	plánovat
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
s	s	k7c7	s
bezbariérovým	bezbariérový	k2eAgInSc7d1	bezbariérový
vstupem	vstup	k1gInSc7	vstup
<g/>
,	,	kIx,	,
modernizace	modernizace	k1gFnSc1	modernizace
expozic	expozice	k1gFnPc2	expozice
a	a	k8xC	a
především	především	k6eAd1	především
nová	nový	k2eAgFnSc1d1	nová
expozice	expozice	k1gFnSc1	expozice
<g/>
,	,	kIx,	,
věnující	věnující	k2eAgFnSc1d1	věnující
se	se	k3xPyFc4	se
langobardskému	langobardský	k2eAgNnSc3d1	langobardský
osídlení	osídlení	k1gNnSc3	osídlení
na	na	k7c6	na
území	území	k1gNnSc6	území
Kyjova	Kyjov	k1gInSc2	Kyjov
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Trvale	trvale	k6eAd1	trvale
budou	být	k5eAaImBp3nP	být
vystaveny	vystavit	k5eAaPmNgInP	vystavit
faksimile	faksimile	k1gNnPc3	faksimile
listin	listina	k1gFnPc2	listina
a	a	k8xC	a
prezentována	prezentován	k2eAgFnSc1d1	prezentována
historie	historie	k1gFnSc1	historie
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
spolky	spolek	k1gInPc4	spolek
a	a	k8xC	a
školství	školství	k1gNnSc4	školství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
mělo	mít	k5eAaImAgNnS	mít
muzeum	muzeum	k1gNnSc1	muzeum
okolo	okolo	k7c2	okolo
4	[number]	k4	4
000	[number]	k4	000
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
Stavědlo	stavědlo	k1gNnSc4	stavědlo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
v	v	k7c6	v
nevyužívané	využívaný	k2eNgFnSc6d1	nevyužívaná
budově	budova	k1gFnSc6	budova
u	u	k7c2	u
železničního	železniční	k2eAgInSc2d1	železniční
přejezdu	přejezd	k1gInSc2	přejezd
v	v	k7c6	v
Nětčicích	Nětčice	k1gFnPc6	Nětčice
v	v	k7c6	v
Komenského	Komenského	k2eAgFnSc6d1	Komenského
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Stálá	stálý	k2eAgFnSc1d1	stálá
expozice	expozice	k1gFnSc1	expozice
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
nádraží	nádraží	k1gNnSc2	nádraží
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
<g/>
,	,	kIx,	,
nejbližším	blízký	k2eAgFnPc3d3	nejbližší
stanicím	stanice	k1gFnPc3	stanice
na	na	k7c6	na
Vlárské	vlárský	k2eAgFnSc6d1	Vlárská
dráze	dráha	k1gFnSc6	dráha
a	a	k8xC	a
regionálním	regionální	k2eAgFnPc3d1	regionální
tratím	trať	k1gFnPc3	trať
Kyjovska	Kyjovsko	k1gNnSc2	Kyjovsko
<g/>
.	.	kIx.	.
</s>
<s>
Vystaven	vystaven	k2eAgInSc1d1	vystaven
je	být	k5eAaImIp3nS	být
model	model	k1gInSc1	model
železnice	železnice	k1gFnSc2	železnice
v	v	k7c6	v
měřítku	měřítko	k1gNnSc6	měřítko
H	H	kA	H
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
se	se	k3xPyFc4	se
otevírá	otevírat	k5eAaImIp3nS	otevírat
zpravidla	zpravidla	k6eAd1	zpravidla
první	první	k4xOgFnSc4	první
sobotu	sobota	k1gFnSc4	sobota
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
nebo	nebo	k8xC	nebo
po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Čtenářský	čtenářský	k2eAgInSc1d1	čtenářský
spolek	spolek	k1gInSc1	spolek
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1864	[number]	k4	1864
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
otevřel	otevřít	k5eAaPmAgInS	otevřít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
knihovnu	knihovna	k1gFnSc4	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1881	[number]	k4	1881
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Michálek	Michálek	k1gMnSc1	Michálek
hrálo	hrát	k5eAaImAgNnS	hrát
ve	v	k7c6	v
městě	město	k1gNnSc6	město
první	první	k4xOgFnSc2	první
české	český	k2eAgFnSc2d1	Česká
divadelní	divadelní	k2eAgFnSc2d1	divadelní
představení	představení	k1gNnPc2	představení
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
výtěžek	výtěžek	k1gInSc1	výtěžek
šel	jít	k5eAaImAgInS	jít
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
místního	místní	k2eAgInSc2d1	místní
čtenářského	čtenářský	k2eAgInSc2d1	čtenářský
spolku	spolek	k1gInSc2	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Spolek	spolek	k1gInSc1	spolek
nejdříve	dříve	k6eAd3	dříve
sídlil	sídlit	k5eAaImAgInS	sídlit
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
domě	dům	k1gInSc6	dům
č.	č.	k?	č.
11	[number]	k4	11
<g/>
,	,	kIx,	,
po	po	k7c6	po
vybudování	vybudování	k1gNnSc6	vybudování
Záložny	záložna	k1gFnSc2	záložna
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
pošta	pošta	k1gFnSc1	pošta
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
sem	sem	k6eAd1	sem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
se	se	k3xPyFc4	se
knihovna	knihovna	k1gFnSc1	knihovna
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
přízemí	přízemí	k1gNnSc2	přízemí
bývalého	bývalý	k2eAgInSc2d1	bývalý
hotelu	hotel	k1gInSc2	hotel
Michálek	Michálek	k1gMnSc1	Michálek
(	(	kIx(	(
<g/>
Komenského	Komenského	k2eAgNnPc2d1	Komenského
49	[number]	k4	49
<g/>
)	)	kIx)	)
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1988	[number]	k4	1988
získala	získat	k5eAaPmAgFnS	získat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
novou	nový	k2eAgFnSc4d1	nová
budovu	budova	k1gFnSc4	budova
na	na	k7c6	na
Komenského	Komenského	k2eAgFnSc6d1	Komenského
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
přes	přes	k7c4	přes
53	[number]	k4	53
300	[number]	k4	300
svazků	svazek	k1gInPc2	svazek
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
si	se	k3xPyFc3	se
půjčovalo	půjčovat	k5eAaImAgNnS	půjčovat
přes	přes	k7c4	přes
2	[number]	k4	2
000	[number]	k4	000
registrovaných	registrovaný	k2eAgMnPc2d1	registrovaný
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čítárně	čítárna	k1gFnSc6	čítárna
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
126	[number]	k4	126
periodik	periodikum	k1gNnPc2	periodikum
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
oddělení	oddělení	k1gNnSc1	oddělení
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
dorost	dorost	k1gInSc4	dorost
<g/>
,	,	kIx,	,
čítárnu	čítárna	k1gFnSc4	čítárna
a	a	k8xC	a
pobočku	pobočka	k1gFnSc4	pobočka
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
Bohuslavice	Bohuslavice	k1gFnPc1	Bohuslavice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
je	být	k5eAaImIp3nS	být
také	také	k9	také
zázemí	zázemí	k1gNnSc4	zázemí
Sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
organizace	organizace	k1gFnSc2	organizace
nevidomých	nevidomý	k1gMnPc2	nevidomý
a	a	k8xC	a
slabozrakých	slabozraký	k1gMnPc2	slabozraký
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
knihovna	knihovna	k1gFnSc1	knihovna
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
knihy	kniha	k1gFnPc4	kniha
v	v	k7c6	v
brailleově	brailleův	k2eAgNnSc6d1	brailleův
písmu	písmo	k1gNnSc6	písmo
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
zprostředkovat	zprostředkovat	k5eAaPmF	zprostředkovat
zapůjčení	zapůjčení	k1gNnSc4	zapůjčení
audioknih	audiokniha	k1gFnPc2	audiokniha
z	z	k7c2	z
Knihovny	knihovna	k1gFnSc2	knihovna
K.	K.	kA	K.
E.	E.	kA	E.
Macana	Macan	k1gMnSc2	Macan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
knihovny	knihovna	k1gFnSc2	knihovna
působí	působit	k5eAaImIp3nS	působit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
i	i	k8xC	i
Centrum	centrum	k1gNnSc1	centrum
Svazu	svaz	k1gInSc2	svaz
neslyšících	slyšící	k2eNgMnPc2d1	neslyšící
a	a	k8xC	a
nedoslýchavých	nedoslýchavý	k2eAgMnPc2d1	nedoslýchavý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knihovně	knihovna	k1gFnSc6	knihovna
je	být	k5eAaImIp3nS	být
zdarma	zdarma	k6eAd1	zdarma
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
počítač	počítač	k1gMnSc1	počítač
s	s	k7c7	s
internetem	internet	k1gInSc7	internet
a	a	k8xC	a
připojení	připojení	k1gNnSc4	připojení
Wi-Fi	Wi-F	k1gFnSc2	Wi-F
<g/>
.	.	kIx.	.
</s>
<s>
Katalog	katalog	k1gInSc1	katalog
knih	kniha	k1gFnPc2	kniha
je	být	k5eAaImIp3nS	být
zpracován	zpracovat	k5eAaPmNgInS	zpracovat
v	v	k7c6	v
knihovním	knihovní	k2eAgInSc6d1	knihovní
systému	systém	k1gInSc6	systém
Clavius	Clavius	k1gInSc1	Clavius
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přístupný	přístupný	k2eAgInSc4d1	přístupný
on-line	onin	k1gInSc5	on-lin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
zpřístupněn	zpřístupnit	k5eAaPmNgInS	zpřístupnit
i	i	k9	i
jako	jako	k8xS	jako
mobilní	mobilní	k2eAgFnSc1d1	mobilní
aplikace	aplikace	k1gFnSc1	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc1	první
galerie	galerie	k1gFnSc1	galerie
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
Galerie	galerie	k1gFnSc2	galerie
Doma	doma	k6eAd1	doma
ve	v	k7c6	v
Svatoborské	svatoborský	k2eAgFnSc6d1	Svatoborská
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
rodinném	rodinný	k2eAgInSc6d1	rodinný
domě	dům	k1gInSc6	dům
manželů	manžel	k1gMnPc2	manžel
Ježových	Ježová	k1gFnPc2	Ježová
<g/>
.	.	kIx.	.
</s>
<s>
Městským	městský	k2eAgInSc7d1	městský
úřadem	úřad	k1gInSc7	úřad
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
spravována	spravován	k2eAgFnSc1d1	spravována
Radniční	radniční	k2eAgFnSc1d1	radniční
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
přístavbě	přístavba	k1gFnSc6	přístavba
radnice	radnice	k1gFnSc2	radnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
mlýně	mlýn	k1gInSc6	mlýn
v	v	k7c6	v
Bohuslavicích	Bohuslavice	k1gFnPc6	Bohuslavice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
výstavy	výstava	k1gFnPc4	výstava
<g/>
,	,	kIx,	,
setkávání	setkávání	k1gNnSc4	setkávání
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
drobnými	drobný	k2eAgInPc7d1	drobný
řemeslnými	řemeslný	k2eAgInPc7d1	řemeslný
výrobky	výrobek	k1gInPc7	výrobek
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
podobné	podobný	k2eAgFnPc4d1	podobná
akce	akce	k1gFnPc4	akce
nazvaný	nazvaný	k2eAgMnSc1d1	nazvaný
Art	Art	k1gMnSc1	Art
mlýn	mlýn	k1gInSc4	mlýn
Bohuslavice	Bohuslavice	k1gFnPc4	Bohuslavice
<g/>
.	.	kIx.	.
</s>
<s>
Koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
divadelní	divadelní	k2eAgMnSc1d1	divadelní
festival	festival	k1gInSc4	festival
Mezi	mezi	k7c4	mezi
smrky	smrk	k1gInPc4	smrk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
pořádá	pořádat	k5eAaImIp3nS	pořádat
výstavy	výstava	k1gFnPc4	výstava
Galerie	galerie	k1gFnSc2	galerie
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
J.	J.	kA	J.
A.	A.	kA	A.
Komenského	Komenský	k1gMnSc2	Komenský
na	na	k7c6	na
Seifertově	Seifertův	k2eAgNnSc6d1	Seifertovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Nejmladší	mladý	k2eAgMnSc1d3	nejmladší
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Galerie	galerie	k1gFnSc1	galerie
Čajovna	čajovna	k1gFnSc1	čajovna
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
Domu	dům	k1gInSc2	dům
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
kombinace	kombinace	k1gFnSc1	kombinace
čajovny	čajovna	k1gFnSc2	čajovna
<g/>
,	,	kIx,	,
galerie	galerie	k1gFnSc2	galerie
a	a	k8xC	a
prodejny	prodejna	k1gFnSc2	prodejna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
kino	kino	k1gNnSc1	kino
ve	v	k7c6	v
městě	město	k1gNnSc6	město
s	s	k7c7	s
názvem	název	k1gInSc7	název
Urania	uranium	k1gNnSc2	uranium
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
hotelu	hotel	k1gInSc6	hotel
Michálek	Michálek	k1gMnSc1	Michálek
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
naproti	naproti	k6eAd1	naproti
pošty	pošta	k1gFnSc2	pošta
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
vcházelo	vcházet	k5eAaImAgNnS	vcházet
z	z	k7c2	z
Dobrovského	Dobrovského	k2eAgFnSc2d1	Dobrovského
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
prostor	prostor	k1gInSc1	prostor
využit	využít	k5eAaPmNgInS	využít
pro	pro	k7c4	pro
loutkové	loutkový	k2eAgNnSc4d1	loutkové
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
bylo	být	k5eAaImAgNnS	být
přemístěno	přemístit	k5eAaPmNgNnS	přemístit
do	do	k7c2	do
nově	nově	k6eAd1	nově
postavené	postavený	k2eAgFnSc2d1	postavená
sokolovny	sokolovna	k1gFnSc2	sokolovna
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Kino	kino	k1gNnSc4	kino
Svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1931	[number]	k4	1931
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
promítán	promítán	k2eAgInSc1d1	promítán
zvukový	zvukový	k2eAgInSc1d1	zvukový
film	film	k1gInSc1	film
-	-	kIx~	-
On	on	k3xPp3gMnSc1	on
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc3	jeho
sestra	sestra	k1gFnSc1	sestra
režiséra	režisér	k1gMnSc2	režisér
Martina	Martin	k1gMnSc2	Martin
Friče	Frič	k1gMnSc2	Frič
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
kino	kino	k1gNnSc1	kino
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
v	v	k7c6	v
Lidovém	lidový	k2eAgInSc6d1	lidový
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
Kino	kino	k1gNnSc1	kino
Oko	oko	k1gNnSc1	oko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
bylo	být	k5eAaImAgNnS	být
rekonstruováno	rekonstruovat	k5eAaBmNgNnS	rekonstruovat
a	a	k8xC	a
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
zde	zde	k6eAd1	zde
současné	současný	k2eAgNnSc1d1	současné
kino	kino	k1gNnSc1	kino
Panorama	panorama	k1gNnSc1	panorama
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
letní	letní	k2eAgNnSc1d1	letní
kino	kino	k1gNnSc1	kino
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
městského	městský	k2eAgInSc2d1	městský
parku	park	k1gInSc2	park
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
2	[number]	k4	2
000	[number]	k4	000
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
promítání	promítání	k1gNnSc3	promítání
<g/>
,	,	kIx,	,
koncertům	koncert	k1gInPc3	koncert
a	a	k8xC	a
jiným	jiný	k2eAgFnPc3d1	jiná
společenským	společenský	k2eAgFnPc3d1	společenská
akcím	akce	k1gFnPc3	akce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výstavbě	výstavba	k1gFnSc6	výstavba
Domu	dům	k1gInSc2	dům
kultury	kultura	k1gFnSc2	kultura
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
divadelní	divadelní	k2eAgInSc1d1	divadelní
sál	sál	k1gInSc1	sál
používán	používán	k2eAgInSc1d1	používán
i	i	k9	i
k	k	k7c3	k
promítání	promítání	k1gNnSc3	promítání
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k9	jako
Kino	kino	k1gNnSc1	kino
Odborů	odbor	k1gInPc2	odbor
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Kino	kino	k1gNnSc1	kino
Panorama	panorama	k1gNnSc1	panorama
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
424	[number]	k4	424
míst	místo	k1gNnPc2	místo
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
1970	[number]	k4	1970
promítáním	promítání	k1gNnSc7	promítání
sovětského	sovětský	k2eAgInSc2d1	sovětský
velkofilmu	velkofilm	k1gInSc2	velkofilm
Sergeje	Sergej	k1gMnSc2	Sergej
Bondarčuka	Bondarčuk	k1gMnSc2	Bondarčuk
Vojna	vojna	k1gFnSc1	vojna
a	a	k8xC	a
mír	mír	k1gInSc1	mír
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
možnostem	možnost	k1gFnPc3	možnost
promítat	promítat	k5eAaImF	promítat
zde	zde	k6eAd1	zde
širokoúhlé	širokoúhlý	k2eAgFnPc1d1	širokoúhlá
70	[number]	k4	70
mm	mm	kA	mm
filmy	film	k1gInPc4	film
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
kina	kino	k1gNnSc2	kino
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejmodernějších	moderní	k2eAgInPc2d3	nejmodernější
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
organizace	organizace	k1gFnSc2	organizace
Europa	Europa	k1gFnSc1	Europa
Cinemas	Cinemas	k1gInSc1	Cinemas
<g/>
,	,	kIx,	,
sítě	síť	k1gFnPc1	síť
evropských	evropský	k2eAgNnPc2d1	Evropské
kin	kino	k1gNnPc2	kino
zaměřených	zaměřený	k2eAgNnPc2d1	zaměřené
na	na	k7c4	na
evropský	evropský	k2eAgInSc4d1	evropský
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
nízkou	nízký	k2eAgFnSc4d1	nízká
návštěvnost	návštěvnost	k1gFnSc4	návštěvnost
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
přemístění	přemístění	k1gNnSc4	přemístění
do	do	k7c2	do
Domu	dům	k1gInSc2	dům
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
dalším	další	k2eAgInSc6d1	další
provozu	provoz	k1gInSc6	provoz
a	a	k8xC	a
provedena	proveden	k2eAgFnSc1d1	provedena
digitalizace	digitalizace	k1gFnSc1	digitalizace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
možné	možný	k2eAgNnSc1d1	možné
promítat	promítat	k5eAaImF	promítat
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
3D	[number]	k4	3D
a	a	k8xC	a
plánují	plánovat	k5eAaImIp3nP	plánovat
se	se	k3xPyFc4	se
další	další	k2eAgFnPc1d1	další
investice	investice	k1gFnPc1	investice
do	do	k7c2	do
sedadel	sedadlo	k1gNnPc2	sedadlo
<g/>
,	,	kIx,	,
podlahy	podlaha	k1gFnSc2	podlaha
a	a	k8xC	a
klimatizace	klimatizace	k1gFnSc2	klimatizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
kino	kino	k1gNnSc1	kino
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
20	[number]	k4	20
602	[number]	k4	602
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
bývalé	bývalý	k2eAgFnSc2d1	bývalá
synagogy	synagoga	k1gFnSc2	synagoga
<g/>
,	,	kIx,	,
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
postaven	postaven	k2eAgInSc1d1	postaven
Kulturní	kulturní	k2eAgInSc1d1	kulturní
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Otevřen	otevřen	k2eAgMnSc1d1	otevřen
byl	být	k5eAaImAgMnS	být
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
divadelní	divadelní	k2eAgFnSc7d1	divadelní
hrou	hra	k1gFnSc7	hra
Strakonický	strakonický	k2eAgInSc1d1	strakonický
dudák	dudák	k1gInSc1	dudák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Domě	dům	k1gInSc6	dům
kultury	kultura	k1gFnSc2	kultura
je	být	k5eAaImIp3nS	být
divadelní	divadelní	k2eAgInSc4d1	divadelní
sál	sál	k1gInSc4	sál
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
334	[number]	k4	334
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
estrádní	estrádní	k2eAgInSc1d1	estrádní
sál	sál	k1gInSc1	sál
(	(	kIx(	(
<g/>
400	[number]	k4	400
<g/>
-	-	kIx~	-
<g/>
500	[number]	k4	500
míst	místo	k1gNnPc2	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
loutkový	loutkový	k2eAgInSc1d1	loutkový
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
výstavní	výstavní	k2eAgInPc1d1	výstavní
prostory	prostora	k1gFnPc4	prostora
a	a	k8xC	a
restaurace	restaurace	k1gFnPc4	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
i	i	k9	i
Galerie	galerie	k1gFnSc1	galerie
Čajovna	čajovna	k1gFnSc1	čajovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
celý	celý	k2eAgInSc1d1	celý
dům	dům	k1gInSc1	dům
rekonstruován	rekonstruován	k2eAgInSc1d1	rekonstruován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
působí	působit	k5eAaImIp3nS	působit
Kruh	kruh	k1gInSc1	kruh
přátel	přítel	k1gMnPc2	přítel
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
pravidelně	pravidelně	k6eAd1	pravidelně
koná	konat	k5eAaImIp3nS	konat
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
Concentus	Concentus	k1gInSc4	Concentus
Moraviae	Moravia	k1gFnSc2	Moravia
a	a	k8xC	a
od	od	k7c2	od
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
kině	kino	k1gNnSc6	kino
Žalmanův	Žalmanův	k2eAgInSc4d1	Žalmanův
folkový	folkový	k2eAgInSc4d1	folkový
Kyjov	Kyjov	k1gInSc4	Kyjov
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
i	i	k9	i
dětský	dětský	k2eAgInSc4d1	dětský
pěvecký	pěvecký	k2eAgInSc4d1	pěvecký
sbor	sbor	k1gInSc4	sbor
Modráčci	modráček	k1gMnPc1	modráček
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
založil	založit	k5eAaPmAgMnS	založit
učitel	učitel	k1gMnSc1	učitel
Eduard	Eduard	k1gMnSc1	Eduard
Cvrkal	cvrkat	k5eAaImAgMnS	cvrkat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
country	country	k2eAgFnSc1d1	country
kapela	kapela	k1gFnSc1	kapela
Návrat	návrat	k1gInSc1	návrat
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
Helemese	Helemese	k1gFnSc2	Helemese
<g/>
.	.	kIx.	.
</s>
<s>
Pobočky	pobočka	k1gFnPc1	pobočka
České	český	k2eAgFnSc2d1	Česká
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
akademie	akademie	k1gFnSc2	akademie
a	a	k8xC	a
Moravsko-slezské	moravskolezský	k2eAgFnSc2d1	moravsko-slezská
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
akademie	akademie	k1gFnSc2	akademie
jsou	být	k5eAaImIp3nP	být
zaměřeny	zaměřit	k5eAaPmNgFnP	zaměřit
především	především	k9	především
na	na	k7c4	na
přednáškovou	přednáškový	k2eAgFnSc4d1	přednášková
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
byla	být	k5eAaImAgFnS	být
astronomickým	astronomický	k2eAgInSc7d1	astronomický
kroužkem	kroužek	k1gInSc7	kroužek
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
do	do	k7c2	do
Vlkoše	Vlkoš	k1gMnSc2	Vlkoš
vybudována	vybudován	k2eAgFnSc1d1	vybudována
hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
zbourána	zbourat	k5eAaPmNgFnS	zbourat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
získán	získat	k5eAaPmNgInS	získat
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
garáží	garáž	k1gFnPc2	garáž
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
jsou	být	k5eAaImIp3nP	být
každý	každý	k3xTgInSc4	každý
měsíc	měsíc	k1gInSc4	měsíc
vydávány	vydáván	k2eAgFnPc1d1	vydávána
Kyjovské	kyjovský	k2eAgFnPc1d1	Kyjovská
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
občané	občan	k1gMnPc1	občan
dostávají	dostávat	k5eAaImIp3nP	dostávat
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vycházely	vycházet	k5eAaImAgFnP	vycházet
v	v	k7c6	v
nákladu	náklad	k1gInSc6	náklad
5	[number]	k4	5
500	[number]	k4	500
ks	ks	kA	ks
<g/>
.	.	kIx.	.
</s>
<s>
Dění	dění	k1gNnSc1	dění
ve	v	k7c6	v
městě	město	k1gNnSc6	město
mapuje	mapovat	k5eAaImIp3nS	mapovat
i	i	k9	i
Televize	televize	k1gFnSc1	televize
Slovácko	Slovácko	k1gNnSc4	Slovácko
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
č.	č.	k?	č.
340	[number]	k4	340
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
také	také	k9	také
Vlárská	vlárský	k2eAgFnSc1d1	Vlárská
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
do	do	k7c2	do
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
z	z	k7c2	z
Kyjova	Kyjov	k1gInSc2	Kyjov
do	do	k7c2	do
Bzence	Bzenec	k1gInSc2	Bzenec
byla	být	k5eAaImAgFnS	být
dána	dát	k5eAaPmNgFnS	dát
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
do	do	k7c2	do
Kyjova	Kyjov	k1gInSc2	Kyjov
pak	pak	k6eAd1	pak
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
obchází	obcházet	k5eAaImIp3nS	obcházet
město	město	k1gNnSc4	město
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
západním	západní	k2eAgInSc6d1	západní
a	a	k8xC	a
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
vlak	vlak	k1gInSc1	vlak
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vozy	vůz	k1gInPc7	vůz
do	do	k7c2	do
Kyjova	Kyjov	k1gInSc2	Kyjov
údajně	údajně	k6eAd1	údajně
přijel	přijet	k5eAaPmAgInS	přijet
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
rekonstruována	rekonstruovat	k5eAaBmNgFnS	rekonstruovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
spěšné	spěšný	k2eAgInPc1d1	spěšný
vlaky	vlak	k1gInPc1	vlak
s	s	k7c7	s
přímým	přímý	k2eAgNnSc7d1	přímé
spojením	spojení	k1gNnSc7	spojení
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
Slavkova	Slavkov	k1gInSc2	Slavkov
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
Bučovic	Bučovice	k1gFnPc2	Bučovice
<g/>
,	,	kIx,	,
Nesovic	Nesovice	k1gFnPc2	Nesovice
<g/>
,	,	kIx,	,
Nemotic	Nemotice	k1gFnPc2	Nemotice
<g/>
,	,	kIx,	,
Vracova	Vracov	k1gInSc2	Vracov
<g/>
,	,	kIx,	,
Bzence	Bzenec	k1gInPc1	Bzenec
<g/>
,	,	kIx,	,
Veselí	veselí	k1gNnSc1	veselí
nad	nad	k7c7	nad
Moravou	Morava	k1gFnSc7	Morava
<g/>
,	,	kIx,	,
Uherského	uherský	k2eAgInSc2d1	uherský
Ostrohu	Ostroh	k1gInSc2	Ostroh
<g/>
,	,	kIx,	,
Ostrožské	ostrožský	k2eAgFnSc2d1	Ostrožská
Nové	Nové	k2eAgFnSc2d1	Nové
Vsi	ves	k1gFnSc2	ves
<g/>
,	,	kIx,	,
Kunovic	Kunovice	k1gFnPc2	Kunovice
<g/>
,	,	kIx,	,
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
a	a	k8xC	a
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
u	u	k7c2	u
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
Bohuslavice	Bohuslavice	k1gFnPc1	Bohuslavice
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgFnSc3d1	vlastní
železniční	železniční	k2eAgFnSc3d1	železniční
stanici	stanice	k1gFnSc3	stanice
s	s	k7c7	s
názvem	název	k1gInSc7	název
Bohuslavice	Bohuslavice	k1gFnPc4	Bohuslavice
u	u	k7c2	u
Kyjova	Kyjov	k1gInSc2	Kyjov
a	a	k8xC	a
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
u	u	k7c2	u
šroubáren	šroubárna	k1gFnPc2	šroubárna
je	být	k5eAaImIp3nS	být
železniční	železniční	k2eAgFnSc1d1	železniční
zastávka	zastávka	k1gFnSc1	zastávka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Kyjovem	Kyjov	k1gInSc7	Kyjov
a	a	k8xC	a
Mutěnicemi	Mutěnice	k1gFnPc7	Mutěnice
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1900	[number]	k4	1900
provozována	provozován	k2eAgFnSc1d1	provozována
doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
č.	č.	k?	č.
257	[number]	k4	257
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
měla	mít	k5eAaImAgFnS	mít
tato	tento	k3xDgFnSc1	tento
trať	trať	k1gFnSc1	trať
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
i	i	k8xC	i
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
doprava	doprava	k1gFnSc1	doprava
ukončena	ukončit	k5eAaPmNgFnS	ukončit
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
trať	trať	k1gFnSc1	trať
úředně	úředně	k6eAd1	úředně
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Kyjov	Kyjov	k1gInSc1	Kyjov
tak	tak	k9	tak
ztratil	ztratit	k5eAaPmAgInS	ztratit
přímé	přímý	k2eAgNnSc4d1	přímé
železniční	železniční	k2eAgNnSc4d1	železniční
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
trať	trať	k1gFnSc1	trať
přebudována	přebudovat	k5eAaPmNgFnS	přebudovat
na	na	k7c4	na
cyklostezku	cyklostezka	k1gFnSc4	cyklostezka
<g/>
.	.	kIx.	.
</s>
<s>
Autobusové	autobusový	k2eAgNnSc1d1	autobusové
spojení	spojení	k1gNnSc1	spojení
Kyjova	Kyjov	k1gInSc2	Kyjov
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
obcemi	obec	k1gFnPc7	obec
postupně	postupně	k6eAd1	postupně
vznikalo	vznikat	k5eAaImAgNnS	vznikat
za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Koncese	koncese	k1gFnSc1	koncese
na	na	k7c4	na
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
dopravu	doprava	k1gFnSc4	doprava
udělovala	udělovat	k5eAaImAgFnS	udělovat
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
se	se	k3xPyFc4	se
v	v	k7c6	v
kyjovské	kyjovský	k2eAgFnSc6d1	Kyjovská
kronice	kronika	k1gFnSc6	kronika
udává	udávat	k5eAaImIp3nS	udávat
spoj	spoj	k1gInSc4	spoj
do	do	k7c2	do
Archlebova	Archlebův	k2eAgInSc2d1	Archlebův
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
x	x	k?	x
denně	denně	k6eAd1	denně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hovoran	Hovoran	k1gInSc1	Hovoran
<g/>
,	,	kIx,	,
Ždánic	Ždánice	k1gFnPc2	Ždánice
<g/>
,	,	kIx,	,
Osvětiman	Osvětiman	k1gMnSc1	Osvětiman
<g/>
,	,	kIx,	,
Žeravic	Žeravice	k1gFnPc2	Žeravice
a	a	k8xC	a
Milotic	Milotice	k1gFnPc2	Milotice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
přibylo	přibýt	k5eAaPmAgNnS	přibýt
spojení	spojení	k1gNnSc1	spojení
se	s	k7c7	s
Stupavou	Stupavý	k2eAgFnSc7d1	Stupavý
<g/>
,	,	kIx,	,
Čeložnicemi	Čeložnice	k1gFnPc7	Čeložnice
<g/>
,	,	kIx,	,
Moravanami	Moravana	k1gFnPc7	Moravana
<g/>
,	,	kIx,	,
Hýslemi	Hýsle	k1gFnPc7	Hýsle
<g/>
,	,	kIx,	,
Medlovicemi	Medlovice	k1gFnPc7	Medlovice
a	a	k8xC	a
Újezdcem	Újezdce	k1gMnSc7	Újezdce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
provozována	provozován	k2eAgFnSc1d1	provozována
městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Stanoviště	stanoviště	k1gNnSc1	stanoviště
autobusů	autobus	k1gInPc2	autobus
bývalo	bývat	k5eAaImAgNnS	bývat
na	na	k7c6	na
kyjovském	kyjovský	k2eAgNnSc6d1	Kyjovské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1976	[number]	k4	1976
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Nerudova	Nerudův	k2eAgFnSc1d1	Nerudova
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1998	[number]	k4	1998
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
nádraží	nádraží	k1gNnSc2	nádraží
odbavovací	odbavovací	k2eAgFnSc1d1	odbavovací
hala	hala	k1gFnSc1	hala
s	s	k7c7	s
čekárnou	čekárna	k1gFnSc7	čekárna
a	a	k8xC	a
sociálním	sociální	k2eAgNnSc7d1	sociální
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
mezi	mezi	k7c7	mezi
autobusovým	autobusový	k2eAgNnSc7d1	autobusové
a	a	k8xC	a
vlakovým	vlakový	k2eAgNnSc7d1	vlakové
nádražím	nádraží	k1gNnSc7	nádraží
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1,2	[number]	k4	1,2
km	km	kA	km
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
u	u	k7c2	u
vlakového	vlakový	k2eAgNnSc2d1	vlakové
nádraží	nádraží	k1gNnSc2	nádraží
vybudován	vybudován	k2eAgInSc4d1	vybudován
přestupní	přestupní	k2eAgInSc4d1	přestupní
terminál	terminál	k1gInSc4	terminál
<g/>
.	.	kIx.	.
</s>
<s>
Kyjov	Kyjov	k1gInSc1	Kyjov
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
silnic	silnice	k1gFnPc2	silnice
I	i	k8xC	i
<g/>
/	/	kIx~	/
<g/>
54	[number]	k4	54
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
432	[number]	k4	432
a	a	k8xC	a
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
422	[number]	k4	422
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
I	i	k9	i
<g/>
/	/	kIx~	/
<g/>
54	[number]	k4	54
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
nadjezd	nadjezd	k1gInSc1	nadjezd
přes	přes	k7c4	přes
železniční	železniční	k2eAgFnSc4d1	železniční
trať	trať	k1gFnSc4	trať
a	a	k8xC	a
tak	tak	k6eAd1	tak
podstatně	podstatně	k6eAd1	podstatně
zjednodušen	zjednodušen	k2eAgInSc4d1	zjednodušen
průjezd	průjezd	k1gInSc4	průjezd
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
jezdí	jezdit	k5eAaImIp3nP	jezdit
automobily	automobil	k1gInPc1	automobil
po	po	k7c6	po
obchvatu	obchvat	k1gInSc6	obchvat
mezi	mezi	k7c7	mezi
silnicemi	silnice	k1gFnPc7	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
54	[number]	k4	54
a	a	k8xC	a
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
432	[number]	k4	432
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
ulehčení	ulehčení	k1gNnSc3	ulehčení
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
celostátního	celostátní	k2eAgNnSc2d1	celostátní
sčítání	sčítání	k1gNnSc2	sčítání
dopravy	doprava	k1gFnSc2	doprava
projelo	projet	k5eAaPmAgNnS	projet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
přes	přes	k7c4	přes
nadjezd	nadjezd	k1gInSc4	nadjezd
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
12	[number]	k4	12
891	[number]	k4	891
vozidel	vozidlo	k1gNnPc2	vozidlo
denně	denně	k6eAd1	denně
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
10	[number]	k4	10
994	[number]	k4	994
osobních	osobní	k2eAgMnPc2d1	osobní
<g/>
,	,	kIx,	,
1	[number]	k4	1
771	[number]	k4	771
těžkých	těžký	k2eAgInPc2d1	těžký
nákladních	nákladní	k2eAgInPc2d1	nákladní
a	a	k8xC	a
126	[number]	k4	126
jednostopých	jednostopý	k2eAgNnPc2d1	jednostopé
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obchvatu	obchvat	k1gInSc6	obchvat
projelo	projet	k5eAaPmAgNnS	projet
celkem	celkem	k6eAd1	celkem
3	[number]	k4	3
991	[number]	k4	991
vozidel	vozidlo	k1gNnPc2	vozidlo
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
mezi	mezi	k7c7	mezi
Kyjovem	Kyjov	k1gInSc7	Kyjov
a	a	k8xC	a
Miloticemi	Milotice	k1gFnPc7	Milotice
<g/>
,	,	kIx,	,
3	[number]	k4	3
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
veřejné	veřejný	k2eAgNnSc4d1	veřejné
vnitrostátní	vnitrostátní	k2eAgNnSc4d1	vnitrostátní
letiště	letiště	k1gNnSc4	letiště
Kyjov	Kyjov	k1gInSc1	Kyjov
s	s	k7c7	s
travnatou	travnatý	k2eAgFnSc7d1	travnatá
přistávací	přistávací	k2eAgFnSc7d1	přistávací
dráhou	dráha	k1gFnSc7	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
provozováno	provozovat	k5eAaImNgNnS	provozovat
občanským	občanský	k2eAgNnSc7d1	občanské
sdružením	sdružení	k1gNnSc7	sdružení
Aeroklub	aeroklub	k1gInSc1	aeroklub
Kyjov	Kyjov	k1gInSc1	Kyjov
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
města	město	k1gNnSc2	město
vedou	vést	k5eAaImIp3nP	vést
tři	tři	k4xCgFnPc1	tři
cyklostezky	cyklostezka	k1gFnPc1	cyklostezka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
z	z	k7c2	z
jižního	jižní	k2eAgInSc2d1	jižní
okraje	okraj	k1gInSc2	okraj
města	město	k1gNnSc2	město
do	do	k7c2	do
sousedních	sousední	k2eAgFnPc2d1	sousední
Skoronic	Skoronice	k1gFnPc2	Skoronice
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
1	[number]	k4	1
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vede	vést	k5eAaImIp3nS	vést
do	do	k7c2	do
Mutěnic	Mutěnice	k1gFnPc2	Mutěnice
přes	přes	k7c4	přes
Svatobořice-Mistřín	Svatobořice-Mistřín	k1gInSc4	Svatobořice-Mistřín
a	a	k8xC	a
Dubňany	Dubňan	k1gMnPc4	Dubňan
po	po	k7c6	po
náspu	násep	k1gInSc6	násep
bývalé	bývalý	k2eAgFnSc2d1	bývalá
železniční	železniční	k2eAgFnSc2d1	železniční
trati	trať	k1gFnSc2	trať
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
(	(	kIx(	(
<g/>
14	[number]	k4	14
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
cyklostezka	cyklostezka	k1gFnSc1	cyklostezka
podél	podél	k7c2	podél
železniční	železniční	k2eAgFnSc2d1	železniční
trati	trať	k1gFnSc2	trať
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
místní	místní	k2eAgFnPc4d1	místní
části	část	k1gFnPc4	část
Nětčice	Nětčice	k1gFnPc4	Nětčice
a	a	k8xC	a
Bohuslavice	Bohuslavice	k1gFnPc4	Bohuslavice
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
přes	přes	k7c4	přes
2	[number]	k4	2
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
by	by	kYmCp3nS	by
její	její	k3xOp3gFnSc1	její
výstavba	výstavba	k1gFnSc1	výstavba
měla	mít	k5eAaImAgFnS	mít
pokračovat	pokračovat	k5eAaImF	pokračovat
severně	severně	k6eAd1	severně
údolím	údolí	k1gNnSc7	údolí
Kyjovky	kyjovka	k1gFnSc2	kyjovka
přes	přes	k7c4	přes
Haluzice	Haluzice	k1gInPc4	Haluzice
(	(	kIx(	(
<g/>
rekreační	rekreační	k2eAgFnSc1d1	rekreační
oblast	oblast	k1gFnSc1	oblast
u	u	k7c2	u
Jestřabic	Jestřabice	k1gFnPc2	Jestřabice
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nemotice	Nemotice	k1gFnSc1	Nemotice
do	do	k7c2	do
Mouchnic	Mouchnice	k1gFnPc2	Mouchnice
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
necelých	celý	k2eNgInPc2d1	necelý
10	[number]	k4	10
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
cyklisty	cyklista	k1gMnPc4	cyklista
tak	tak	k9	tak
má	mít	k5eAaImIp3nS	mít
vzniknout	vzniknout	k5eAaPmF	vzniknout
spojení	spojení	k1gNnSc4	spojení
z	z	k7c2	z
města	město	k1gNnSc2	město
do	do	k7c2	do
Chřibů	Chřiby	k1gInPc2	Chřiby
a	a	k8xC	a
na	na	k7c4	na
četné	četný	k2eAgFnPc4d1	četná
lesní	lesní	k2eAgFnPc4d1	lesní
asfaltové	asfaltový	k2eAgFnPc4d1	asfaltová
cesty	cesta	k1gFnPc4	cesta
ve	v	k7c6	v
Ždánickém	ždánický	k2eAgInSc6d1	ždánický
lese	les	k1gInSc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Kyjovem	Kyjov	k1gInSc7	Kyjov
prochází	procházet	k5eAaImIp3nS	procházet
dvě	dva	k4xCgFnPc1	dva
cyklotrasy	cyklotrasa	k1gFnPc1	cyklotrasa
-	-	kIx~	-
Moravská	moravský	k2eAgFnSc1d1	Moravská
vinná	vinný	k2eAgFnSc1d1	vinná
stezka	stezka	k1gFnSc1	stezka
(	(	kIx(	(
<g/>
č.	č.	k?	č.
412	[number]	k4	412
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kyjovská	kyjovský	k2eAgFnSc1d1	Kyjovská
vinařská	vinařský	k2eAgFnSc1d1	vinařská
stezka	stezka	k1gFnSc1	stezka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejbližších	blízký	k2eAgNnPc6d3	nejbližší
letech	léto	k1gNnPc6	léto
by	by	kYmCp3nS	by
město	město	k1gNnSc1	město
chtělo	chtít	k5eAaImAgNnS	chtít
vybudovat	vybudovat	k5eAaPmF	vybudovat
hipostezku	hipostezka	k1gFnSc4	hipostezka
<g/>
.	.	kIx.	.
</s>
<s>
Žlutě	žlutě	k6eAd1	žlutě
značená	značený	k2eAgFnSc1d1	značená
turistická	turistický	k2eAgFnSc1d1	turistická
trasa	trasa	k1gFnSc1	trasa
spojuje	spojovat	k5eAaImIp3nS	spojovat
Kyjov	Kyjov	k1gInSc4	Kyjov
s	s	k7c7	s
Čeložnicemi	Čeložnice	k1gFnPc7	Čeložnice
(	(	kIx(	(
<g/>
5,3	[number]	k4	5,3
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chřiby	Chřiby	k1gInPc7	Chřiby
a	a	k8xC	a
Koryčany	Koryčan	k1gMnPc7	Koryčan
(	(	kIx(	(
<g/>
13	[number]	k4	13
km	km	kA	km
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
Sobůlkami	Sobůlka	k1gFnPc7	Sobůlka
(	(	kIx(	(
<g/>
5,6	[number]	k4	5,6
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Strážovským	Strážovský	k2eAgInSc7d1	Strážovský
kopcem	kopec	k1gInSc7	kopec
(	(	kIx(	(
<g/>
8	[number]	k4	8
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
Kyjov	Kyjov	k1gInSc1	Kyjov
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
necelých	celý	k2eNgInPc2d1	necelý
17	[number]	k4	17
km	km	kA	km
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
9	[number]	k4	9
informačních	informační	k2eAgInPc2d1	informační
panelů	panel	k1gInPc2	panel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
její	její	k3xOp3gNnSc4	její
přebudování	přebudování	k1gNnSc4	přebudování
a	a	k8xC	a
zkrácení	zkrácení	k1gNnSc4	zkrácení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2014	[number]	k4	2014
otevřel	otevřít	k5eAaPmAgInS	otevřít
kyjovský	kyjovský	k2eAgInSc1d1	kyjovský
Český	český	k2eAgInSc1d1	český
svaz	svaz	k1gInSc1	svaz
včelařů	včelař	k1gMnPc2	včelař
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
sdružením	sdružení	k1gNnSc7	sdružení
Kyjovské	kyjovský	k2eAgInPc1d1	kyjovský
Slovácko	Slovácko	k1gNnSc4	Slovácko
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
včelařskou	včelařský	k2eAgFnSc4d1	včelařská
naučnou	naučný	k2eAgFnSc4d1	naučná
stezku	stezka	k1gFnSc4	stezka
v	v	k7c6	v
Bohuslavicích	Bohuslavice	k1gFnPc6	Bohuslavice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
cyklostezku	cyklostezka	k1gFnSc4	cyklostezka
z	z	k7c2	z
Nětčic	Nětčice	k1gFnPc2	Nětčice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
necelých	celý	k2eNgFnPc2d1	necelá
2	[number]	k4	2
km	km	kA	km
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
informačních	informační	k2eAgFnPc2d1	informační
tabulí	tabule	k1gFnPc2	tabule
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
odpočinková	odpočinkový	k2eAgNnPc4d1	odpočinkové
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc6	náměstí
je	být	k5eAaImIp3nS	být
7	[number]	k4	7
měšťanských	měšťanský	k2eAgInPc2d1	měšťanský
domů	dům	k1gInPc2	dům
chráněno	chráněn	k2eAgNnSc1d1	chráněno
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
domy	dům	k1gInPc1	dům
č.	č.	k?	č.
7	[number]	k4	7
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
,	,	kIx,	,
16	[number]	k4	16
a	a	k8xC	a
33	[number]	k4	33
a	a	k8xC	a
také	také	k9	také
budova	budova	k1gFnSc1	budova
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
fary	fara	k1gFnSc2	fara
na	na	k7c6	na
Palackého	Palackého	k2eAgFnSc6d1	Palackého
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Renesanční	renesanční	k2eAgInSc1d1	renesanční
zámek	zámek	k1gInSc1	zámek
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1548	[number]	k4	1548
<g/>
,	,	kIx,	,
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
prošel	projít	k5eAaPmAgMnS	projít
přestavbou	přestavba	k1gFnSc7	přestavba
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
jej	on	k3xPp3gMnSc4	on
vyzdobil	vyzdobit	k5eAaPmAgInS	vyzdobit
sgrafity	sgrafito	k1gNnPc7	sgrafito
Jano	Jano	k1gMnSc1	Jano
Köhler	Köhler	k1gMnSc1	Köhler
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
Vlastivědné	vlastivědný	k2eAgNnSc1d1	Vlastivědné
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Renesanční	renesanční	k2eAgFnSc1d1	renesanční
radnice	radnice	k1gFnSc1	radnice
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1561	[number]	k4	1561
<g/>
-	-	kIx~	-
<g/>
1562	[number]	k4	1562
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
italskými	italský	k2eAgMnPc7d1	italský
staviteli	stavitel	k1gMnPc7	stavitel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dominantou	dominanta	k1gFnSc7	dominanta
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Předsunutá	předsunutý	k2eAgFnSc1d1	předsunutá
věž	věž	k1gFnSc1	věž
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
39	[number]	k4	39
m.	m.	k?	m.
U	u	k7c2	u
hlavního	hlavní	k2eAgInSc2d1	hlavní
vchodu	vchod	k1gInSc2	vchod
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
železný	železný	k2eAgInSc1d1	železný
loket	loket	k1gInSc1	loket
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sloužil	sloužit	k5eAaImAgInS	sloužit
k	k	k7c3	k
přeměřování	přeměřování	k1gNnSc3	přeměřování
zboží	zboží	k1gNnSc2	zboží
na	na	k7c6	na
místním	místní	k2eAgInSc6d1	místní
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Fasáda	fasáda	k1gFnSc1	fasáda
radnice	radnice	k1gFnSc1	radnice
je	být	k5eAaImIp3nS	být
vyzdobena	vyzdobit	k5eAaPmNgFnS	vyzdobit
psaníčkovým	psaníčkový	k2eAgNnSc7d1	psaníčkové
sgrafitem	sgrafito	k1gNnSc7	sgrafito
<g/>
.	.	kIx.	.
</s>
<s>
Mariánský	mariánský	k2eAgInSc1d1	mariánský
sloup	sloup	k1gInSc1	sloup
před	před	k7c7	před
radnicí	radnice	k1gFnSc7	radnice
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
kardinál	kardinál	k1gMnSc1	kardinál
František	František	k1gMnSc1	František
z	z	k7c2	z
Ditrichštejna	Ditrichštejn	k1gInSc2	Ditrichštejn
jako	jako	k8xC	jako
dík	dík	k7c3	dík
městu	město	k1gNnSc3	město
za	za	k7c4	za
zachování	zachování	k1gNnSc4	zachování
věrnosti	věrnost	k1gFnSc2	věrnost
císaři	císař	k1gMnSc3	císař
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
během	během	k7c2	během
povstání	povstání	k1gNnSc2	povstání
českých	český	k2eAgInPc2d1	český
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Kapucínský	kapucínský	k2eAgInSc1d1	kapucínský
kostel	kostel	k1gInSc1	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
sv.	sv.	kA	sv.
Cyrila	Cyril	k1gMnSc4	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc4	Metoděj
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1713	[number]	k4	1713
<g/>
-	-	kIx~	-
<g/>
1720	[number]	k4	1720
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
je	být	k5eAaImIp3nS	být
farním	farní	k2eAgInSc7d1	farní
kostelem	kostel	k1gInSc7	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
hlavním	hlavní	k2eAgInSc7d1	hlavní
vchodem	vchod	k1gInSc7	vchod
je	být	k5eAaImIp3nS	být
freska	freska	k1gFnSc1	freska
Matky	matka	k1gFnSc2	matka
milosrdenství	milosrdenství	k1gNnSc2	milosrdenství
s	s	k7c7	s
Ježíškem	ježíšek	k1gInSc7	ježíšek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
byly	být	k5eAaImAgInP	být
před	před	k7c7	před
kostelem	kostel	k1gInSc7	kostel
umístěny	umístěn	k2eAgFnPc1d1	umístěna
sochy	socha	k1gFnPc1	socha
sv.	sv.	kA	sv.
Cyrila	Cyril	k1gMnSc4	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc4	Metoděj
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
kostelem	kostel	k1gInSc7	kostel
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kapucínská	kapucínský	k2eAgFnSc1d1	Kapucínská
hrobka	hrobka	k1gFnSc1	hrobka
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Josefa	Josefa	k1gFnSc1	Josefa
nad	nad	k7c7	nad
zámkem	zámek	k1gInSc7	zámek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1833	[number]	k4	1833
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1692	[number]	k4	1692
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
zde	zde	k6eAd1	zde
stával	stávat	k5eAaImAgInS	stávat
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Martina	Martina	k1gFnSc1	Martina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1180	[number]	k4	1180
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1807	[number]	k4	1807
vyhořel	vyhořet	k5eAaPmAgMnS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Filipa	Filip	k1gMnSc2	Filip
a	a	k8xC	a
Jakuba	Jakub	k1gMnSc2	Jakub
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
Bohuslavice	Bohuslavice	k1gFnPc1	Bohuslavice
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Josefa	Josef	k1gMnSc2	Josef
Kalasanského	Kalasanský	k2eAgInSc2d1	Kalasanský
zvaná	zvaný	k2eAgFnSc1d1	zvaná
také	také	k9	také
gymnazijní	gymnazijní	k2eAgFnSc1d1	gymnazijní
kaple	kaple	k1gFnSc1	kaple
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Komenského	Komenský	k1gMnSc2	Komenský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
ji	on	k3xPp3gFnSc4	on
nechali	nechat	k5eAaPmAgMnP	nechat
postavit	postavit	k5eAaPmF	postavit
piaristé	piarista	k1gMnPc1	piarista
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
do	do	k7c2	do
města	město	k1gNnSc2	město
přišli	přijít	k5eAaPmAgMnP	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1760	[number]	k4	1760
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
založili	založit	k5eAaPmAgMnP	založit
latinské	latinský	k2eAgNnSc4d1	latinské
gymnázium	gymnázium	k1gNnSc4	gymnázium
a	a	k8xC	a
působili	působit	k5eAaImAgMnP	působit
zde	zde	k6eAd1	zde
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
Chudobinec	chudobinec	k1gInSc1	chudobinec
Dominka	Dominka	k1gFnSc1	Dominka
Jurovského	Jurovský	k1gMnSc2	Jurovský
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Jungmannova	Jungmannov	k1gInSc2	Jungmannov
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1728	[number]	k4	1728
<g/>
-	-	kIx~	-
<g/>
1739	[number]	k4	1739
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
útulek	útulek	k1gInSc4	útulek
pro	pro	k7c4	pro
12	[number]	k4	12
chudých	chudý	k2eAgMnPc2d1	chudý
měšťanů	měšťan	k1gMnPc2	měšťan
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
vchodem	vchod	k1gInSc7	vchod
je	být	k5eAaImIp3nS	být
latinský	latinský	k2eAgInSc1d1	latinský
chronogram	chronogram	k1gInSc1	chronogram
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
Co	co	k3yRnSc4	co
učiníte	učinit	k5eAaImIp2nP	učinit
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
chudých	chudý	k2eAgMnPc2d1	chudý
<g/>
,	,	kIx,	,
učiníte	učinit	k5eAaImIp2nP	učinit
mně	já	k3xPp1nSc6	já
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
letopočet	letopočet	k1gInSc4	letopočet
1739	[number]	k4	1739
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
Základní	základní	k2eAgFnSc1d1	základní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Rocha	Rocha	k1gFnSc1	Rocha
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
do	do	k7c2	do
Vlkoše	Vlkoš	k1gMnSc2	Vlkoš
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1716	[number]	k4	1716
<g/>
.	.	kIx.	.
</s>
<s>
Postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c6	na
nejvyšším	vysoký	k2eAgInSc6d3	Nejvyšší
bodě	bod	k1gInSc6	bod
katastru	katastr	k1gInSc2	katastr
města	město	k1gNnSc2	město
jako	jako	k8xC	jako
poděkování	poděkování	k1gNnSc2	poděkování
za	za	k7c4	za
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
morem	mor	k1gInSc7	mor
<g/>
.	.	kIx.	.
</s>
<s>
Boží	boží	k2eAgFnSc1d1	boží
muka	muka	k1gFnSc1	muka
na	na	k7c4	na
Čelakovského	Čelakovský	k2eAgMnSc4d1	Čelakovský
ulici	ulice	k1gFnSc6	ulice
Novorenesanční	novorenesanční	k2eAgFnSc1d1	novorenesanční
budova	budova	k1gFnSc1	budova
Klvaňova	Klvaňův	k2eAgNnSc2d1	Klvaňovo
gymnázia	gymnázium	k1gNnSc2	gymnázium
postavená	postavený	k2eAgFnSc1d1	postavená
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslavické	Bohuslavický	k2eAgFnPc1d1	Bohuslavický
stráně	stráň	k1gFnPc1	stráň
-	-	kIx~	-
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
Bohuslavice	Bohuslavice	k1gFnPc4	Bohuslavice
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
3,52	[number]	k4	3,52
ha	ha	kA	ha
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
ochrany	ochrana	k1gFnSc2	ochrana
teplomilných	teplomilný	k2eAgNnPc6d1	teplomilné
společenstvech	společenstvo	k1gNnPc6	společenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Městský	městský	k2eAgInSc1d1	městský
park	park	k1gInSc1	park
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
400	[number]	k4	400
m	m	kA	m
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1872	[number]	k4	1872
<g/>
-	-	kIx~	-
<g/>
1873	[number]	k4	1873
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
stavitel	stavitel	k1gMnSc1	stavitel
<g/>
,	,	kIx,	,
zemský	zemský	k2eAgMnSc1d1	zemský
poslanec	poslanec	k1gMnSc1	poslanec
a	a	k8xC	a
místní	místní	k2eAgMnSc1d1	místní
mecenáš	mecenáš	k1gMnSc1	mecenáš
Václav	Václav	k1gMnSc1	Václav
Paterna	Paterna	k1gFnSc1	Paterna
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
-	-	kIx~	-
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
pozemky	pozemek	k1gInPc1	pozemek
pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
rozšíření	rozšíření	k1gNnSc4	rozšíření
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
asi	asi	k9	asi
4,2	[number]	k4	4,2
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
ohraničena	ohraničit	k5eAaPmNgFnS	ohraničit
řekou	řeka	k1gFnSc7	řeka
Kyjovkou	kyjovka	k1gFnSc7	kyjovka
a	a	k8xC	a
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
park	park	k1gInSc4	park
navazuje	navazovat	k5eAaImIp3nS	navazovat
městské	městský	k2eAgNnSc1d1	Městské
koupaliště	koupaliště	k1gNnSc1	koupaliště
a	a	k8xC	a
stadion	stadion	k1gInSc1	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
zde	zde	k6eAd1	zde
pomník	pomník	k1gInSc4	pomník
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Čecha	Čech	k1gMnSc2	Čech
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
od	od	k7c2	od
místního	místní	k2eAgMnSc2d1	místní
kameníka	kameník	k1gMnSc2	kameník
a	a	k8xC	a
sochaře	sochař	k1gMnSc2	sochař
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Petra	Petr	k1gMnSc2	Petr
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
-	-	kIx~	-
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spolužáka	spolužák	k1gMnSc2	spolužák
Julia	Julius	k1gMnSc2	Julius
Pelikána	Pelikán	k1gMnSc2	Pelikán
z	z	k7c2	z
C.	C.	kA	C.
k.	k.	k?	k.
odborné	odborný	k2eAgFnPc4d1	odborná
školy	škola	k1gFnPc4	škola
sochařské	sochařský	k2eAgFnPc4d1	sochařská
a	a	k8xC	a
kamenické	kamenický	k2eAgFnPc4d1	kamenická
v	v	k7c6	v
Hořicích	Hořice	k1gFnPc6	Hořice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Čechovi	Čech	k1gMnSc6	Čech
je	být	k5eAaImIp3nS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
i	i	k8xC	i
ulice	ulice	k1gFnPc1	ulice
přiléhající	přiléhající	k2eAgFnPc1d1	přiléhající
k	k	k7c3	k
parku	park	k1gInSc3	park
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1949	[number]	k4	1949
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
parku	park	k1gInSc2	park
postaven	postavit	k5eAaPmNgInS	postavit
pomník	pomník	k1gInSc1	pomník
Josifa	Josif	k1gMnSc2	Josif
Stalina	Stalin	k1gMnSc2	Stalin
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
jeho	on	k3xPp3gNnSc2	on
70	[number]	k4	70
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc4	pomník
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
Julius	Julius	k1gMnSc1	Julius
Pelikán	Pelikán	k1gMnSc1	Pelikán
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
vězněn	věznit	k5eAaImNgInS	věznit
v	v	k7c6	v
internačním	internační	k2eAgInSc6d1	internační
táboře	tábor	k1gInSc6	tábor
v	v	k7c6	v
sousedních	sousední	k2eAgFnPc6d1	sousední
Svatobořicích	Svatobořice	k1gFnPc6	Svatobořice
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
odtud	odtud	k6eAd1	odtud
najímán	najímán	k2eAgInSc4d1	najímán
na	na	k7c4	na
práce	práce	k1gFnPc4	práce
v	v	k7c6	v
kyjovském	kyjovský	k2eAgNnSc6d1	Kyjovské
kamenictví	kamenictví	k1gNnSc6	kamenictví
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Petra	Petr	k1gMnSc2	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
park	park	k1gInSc1	park
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
jmenovat	jmenovat	k5eAaBmF	jmenovat
Stalinovy	Stalinův	k2eAgFnPc4d1	Stalinova
sady	sada	k1gFnPc4	sada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mezi	mezi	k7c7	mezi
místními	místní	k2eAgFnPc7d1	místní
se	se	k3xPyFc4	se
název	název	k1gInSc4	název
neujal	ujmout	k5eNaPmAgInS	ujmout
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc1	pomník
byl	být	k5eAaImAgInS	být
odstraněn	odstranit	k5eAaPmNgInS	odstranit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
parku	park	k1gInSc6	park
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
požární	požární	k2eAgFnSc1d1	požární
nádrž	nádrž	k1gFnSc1	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
nádrže	nádrž	k1gFnSc2	nádrž
nasazeny	nasazen	k2eAgFnPc4d1	nasazena
okrasné	okrasný	k2eAgFnPc4d1	okrasná
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
lekníny	leknín	k1gInPc4	leknín
a	a	k8xC	a
vybudovány	vybudován	k2eAgInPc4d1	vybudován
vodotrysky	vodotrysk	k1gInPc4	vodotrysk
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
nádrž	nádrž	k1gFnSc1	nádrž
chátrala	chátrat	k5eAaImAgFnS	chátrat
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
byla	být	k5eAaImAgFnS	být
zasypána	zasypán	k2eAgFnSc1d1	zasypána
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
místě	místo	k1gNnSc6	místo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
dětské	dětský	k2eAgNnSc1d1	dětské
hřiště	hřiště	k1gNnSc1	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Středem	středem	k7c2	středem
parku	park	k1gInSc2	park
tekl	téct	k5eAaImAgMnS	téct
kaštanovou	kaštanový	k2eAgFnSc7d1	Kaštanová
alejí	alej	k1gFnSc7	alej
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
mlýnský	mlýnský	k2eAgInSc4d1	mlýnský
náhon	náhon	k1gInSc4	náhon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
park	park	k1gInSc1	park
dělil	dělit	k5eAaImAgInS	dělit
na	na	k7c4	na
starou	starý	k2eAgFnSc4d1	stará
(	(	kIx(	(
<g/>
západní	západní	k2eAgFnSc4d1	západní
<g/>
)	)	kIx)	)
a	a	k8xC	a
novou	nový	k2eAgFnSc4d1	nová
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc4d1	východní
<g/>
)	)	kIx)	)
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
starý	starý	k2eAgMnSc1d1	starý
a	a	k8xC	a
nový	nový	k2eAgInSc1d1	nový
park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
na	na	k7c6	na
konci	konec	k1gInSc6	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
náhon	náhon	k1gInSc1	náhon
zasypán	zasypat	k5eAaPmNgInS	zasypat
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
vede	vést	k5eAaImIp3nS	vést
220	[number]	k4	220
m	m	kA	m
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
cesta	cesta	k1gFnSc1	cesta
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
i	i	k8xC	i
cyklisty	cyklista	k1gMnPc4	cyklista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
prošel	projít	k5eAaPmAgInS	projít
park	park	k1gInSc4	park
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
revitalizací	revitalizace	k1gFnSc7	revitalizace
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
k	k	k7c3	k
pořádání	pořádání	k1gNnSc3	pořádání
kulturních	kulturní	k2eAgFnPc2d1	kulturní
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
např.	např.	kA	např.
divadelního	divadelní	k2eAgInSc2d1	divadelní
festivalu	festival	k1gInSc2	festival
Mezi	mezi	k7c7	mezi
Smrky	smrk	k1gInPc7	smrk
nebo	nebo	k8xC	nebo
každoročního	každoroční	k2eAgInSc2d1	každoroční
Dne	den	k1gInSc2	den
koně	kůň	k1gMnSc4	kůň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
v	v	k7c6	v
parku	park	k1gInSc6	park
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
rostly	růst	k5eAaImAgInP	růst
stromy	strom	k1gInPc1	strom
platan	platan	k1gInSc1	platan
javorolistý	javorolistý	k2eAgInSc1d1	javorolistý
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
vejmutovka	vejmutovka	k1gFnSc1	vejmutovka
<g/>
,	,	kIx,	,
buk	buk	k1gInSc4	buk
lesní	lesní	k2eAgInSc4d1	lesní
<g/>
,	,	kIx,	,
douglaska	douglaska	k1gFnSc1	douglaska
tisolistá	tisolistý	k2eAgFnSc1d1	tisolistá
<g/>
,	,	kIx,	,
dřezovec	dřezovec	k1gInSc1	dřezovec
trojtrnný	trojtrnný	k2eAgInSc1d1	trojtrnný
<g/>
,	,	kIx,	,
dub	dub	k1gInSc1	dub
letní	letní	k2eAgInSc1d1	letní
<g/>
,	,	kIx,	,
lípa	lípa	k1gFnSc1	lípa
malolistá	malolistý	k2eAgFnSc1d1	malolistá
<g/>
,	,	kIx,	,
jírovec	jírovec	k1gInSc1	jírovec
maďal	maďal	k1gInSc1	maďal
<g/>
,	,	kIx,	,
jasan	jasan	k1gInSc1	jasan
ztepilý	ztepilý	k2eAgInSc1d1	ztepilý
<g/>
,	,	kIx,	,
javor	javor	k1gInSc1	javor
mléč	mléč	k1gInSc1	mléč
<g/>
,	,	kIx,	,
habr	habr	k1gInSc1	habr
<g />
.	.	kIx.	.
</s>
<s>
obecný	obecný	k2eAgInSc4d1	obecný
<g/>
,	,	kIx,	,
jilm	jilm	k1gInSc4	jilm
habrolistý	habrolistý	k2eAgInSc4d1	habrolistý
<g/>
,	,	kIx,	,
vrba	vrba	k1gFnSc1	vrba
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
kaštanovník	kaštanovník	k1gInSc1	kaštanovník
setý	setý	k2eAgInSc1d1	setý
<g/>
,	,	kIx,	,
katalpa	katalpa	k1gFnSc1	katalpa
(	(	kIx(	(
<g/>
Catalpa	Catalpa	k1gFnSc1	Catalpa
bignonioides	bignonioides	k1gMnSc1	bignonioides
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
ořešák	ořešák	k1gInSc1	ořešák
černý	černý	k2eAgInSc1d1	černý
<g/>
,	,	kIx,	,
javor	javor	k1gInSc1	javor
babyka	babyka	k1gFnSc1	babyka
<g/>
,	,	kIx,	,
bříza	bříza	k1gFnSc1	bříza
bělokorá	bělokorý	k2eAgFnSc1d1	bělokorá
<g/>
,	,	kIx,	,
líska	líska	k1gFnSc1	líska
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
líska	líska	k1gFnSc1	líska
turecká	turecký	k2eAgFnSc1d1	turecká
a	a	k8xC	a
liliovník	liliovník	k1gInSc1	liliovník
tulipánokvětý	tulipánokvětý	k2eAgInSc1d1	tulipánokvětý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
stromů	strom	k1gInPc2	strom
k	k	k7c3	k
výsadbě	výsadba	k1gFnSc3	výsadba
měl	mít	k5eAaImAgInS	mít
vliv	vliv	k1gInSc1	vliv
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Josef	Josef	k1gMnSc1	Josef
Klvaňa	Klvaňa	k1gMnSc1	Klvaňa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1898	[number]	k4	1898
<g/>
-	-	kIx~	-
<g/>
1919	[number]	k4	1919
ředitelem	ředitel	k1gMnSc7	ředitel
kyjovského	kyjovský	k2eAgNnSc2d1	Kyjovské
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
postaveno	postavit	k5eAaPmNgNnS	postavit
několik	několik	k4yIc1	několik
funkcionalistických	funkcionalistický	k2eAgFnPc2d1	funkcionalistická
vil	vila	k1gFnPc2	vila
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgInP	být
často	často	k6eAd1	často
přestavovány	přestavován	k2eAgInPc1d1	přestavován
a	a	k8xC	a
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
ztratily	ztratit	k5eAaPmAgFnP	ztratit
svůj	svůj	k3xOyFgInSc4	svůj
původní	původní	k2eAgInSc4d1	původní
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
proto	proto	k8xC	proto
nemají	mít	k5eNaImIp3nP	mít
statut	statut	k1gInSc4	statut
kulturní	kulturní	k2eAgFnSc2d1	kulturní
památky	památka	k1gFnSc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
stavby	stavba	k1gFnPc4	stavba
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
Rodinný	rodinný	k2eAgInSc1d1	rodinný
dům	dům	k1gInSc1	dům
s	s	k7c7	s
lékárnou	lékárna	k1gFnSc7	lékárna
Otakara	Otakar	k1gMnSc2	Otakar
Svačiny	Svačina	k1gMnSc2	Svačina
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Komenského	Komenského	k2eAgInSc2d1	Komenského
č.	č.	k?	č.
758	[number]	k4	758
<g/>
.	.	kIx.	.
</s>
<s>
Vilu	vila	k1gFnSc4	vila
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgInS	nechat
postavit	postavit	k5eAaPmF	postavit
člen	člen	k1gInSc1	člen
hodonínského	hodonínský	k2eAgNnSc2d1	hodonínské
Sdružení	sdružení	k1gNnSc2	sdružení
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
moravských	moravský	k2eAgFnPc2d1	Moravská
a	a	k8xC	a
podporovatel	podporovatel	k1gMnSc1	podporovatel
umění	umění	k1gNnSc2	umění
Otakar	Otakar	k1gMnSc1	Otakar
Svačina	Svačina	k1gMnSc1	Svačina
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1927	[number]	k4	1927
<g/>
-	-	kIx~	-
<g/>
1928	[number]	k4	1928
od	od	k7c2	od
architekta	architekt	k1gMnSc2	architekt
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Hilgerta	Hilgert	k1gMnSc2	Hilgert
<g/>
,	,	kIx,	,
kyjovského	kyjovský	k2eAgMnSc2d1	kyjovský
rodáka	rodák	k1gMnSc2	rodák
a	a	k8xC	a
žáka	žák	k1gMnSc2	žák
Jože	Joža	k1gFnSc6	Joža
Plečnika	Plečnik	k1gMnSc2	Plečnik
<g/>
.	.	kIx.	.
</s>
<s>
Rodinný	rodinný	k2eAgInSc1d1	rodinný
dům	dům	k1gInSc1	dům
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
Součkových	Součkových	k2eAgMnSc2d1	Součkových
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
U	u	k7c2	u
Parku	park	k1gInSc2	park
č.	č.	k?	č.
805	[number]	k4	805
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc4	dům
postavil	postavit	k5eAaPmAgMnS	postavit
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
kyjovský	kyjovský	k2eAgMnSc1d1	kyjovský
rodák	rodák	k1gMnSc1	rodák
Josef	Josef	k1gMnSc1	Josef
Polášek	Polášek	k1gMnSc1	Polášek
pro	pro	k7c4	pro
učitele	učitel	k1gMnPc4	učitel
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
sběratele	sběratel	k1gMnSc2	sběratel
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
spolumajitele	spolumajitel	k1gMnSc2	spolumajitel
strojírny	strojírna	k1gFnSc2	strojírna
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
Metoděje	Metoděj	k1gMnSc2	Metoděj
Součka	Souček	k1gMnSc2	Souček
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
domu	dům	k1gInSc2	dům
byla	být	k5eAaImAgFnS	být
i	i	k9	i
obrazová	obrazový	k2eAgFnSc1d1	obrazová
galerie	galerie	k1gFnSc1	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Polášek	Polášek	k1gMnSc1	Polášek
stavbu	stavba	k1gFnSc4	stavba
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
po	po	k7c6	po
studijní	studijní	k2eAgFnSc6d1	studijní
cestě	cesta	k1gFnSc6	cesta
po	po	k7c6	po
Holandsku	Holandsko	k1gNnSc6	Holandsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
vlastním	vlastní	k2eAgInSc7d1	vlastní
domem	dům	k1gInSc7	dům
architekta	architekt	k1gMnSc2	architekt
a	a	k8xC	a
výtvarníka	výtvarník	k1gMnSc2	výtvarník
Henka	Henka	k?	Henka
Wegerifa	Wegerif	k1gMnSc2	Wegerif
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
-	-	kIx~	-
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
u	u	k7c2	u
Haagu	Haag	k1gInSc2	Haag
<g/>
.	.	kIx.	.
</s>
<s>
Vila	vila	k1gFnSc1	vila
byla	být	k5eAaImAgFnS	být
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Rodinný	rodinný	k2eAgInSc1d1	rodinný
dům	dům	k1gInSc1	dům
Stelly	Stella	k1gFnSc2	Stella
a	a	k8xC	a
Arnošta	Arnošt	k1gMnSc2	Arnošt
Hayekových	Hayekových	k2eAgMnSc2d1	Hayekových
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
č.	č.	k?	č.
833	[number]	k4	833
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
postavil	postavit	k5eAaPmAgInS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1931	[number]	k4	1931
<g/>
-	-	kIx~	-
<g/>
1932	[number]	k4	1932
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
kyjovský	kyjovský	k2eAgMnSc1d1	kyjovský
rodák	rodák	k1gMnSc1	rodák
Bohumil	Bohumil	k1gMnSc1	Bohumil
Tureček	Tureček	k1gMnSc1	Tureček
pro	pro	k7c4	pro
majitele	majitel	k1gMnPc4	majitel
kyjovské	kyjovský	k2eAgFnSc2d1	Kyjovská
octárny	octárna	k1gFnSc2	octárna
a	a	k8xC	a
významného	významný	k2eAgMnSc2d1	významný
člena	člen	k1gMnSc2	člen
kyjovské	kyjovský	k2eAgFnSc2d1	Kyjovská
Židovské	židovská	k1gFnSc2	židovská
náboženské	náboženský	k2eAgFnSc2d1	náboženská
obce	obec	k1gFnSc2	obec
Arnošta	Arnošt	k1gMnSc2	Arnošt
Hayeka	Hayeek	k1gMnSc2	Hayeek
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Arnošt	Arnošt	k1gMnSc1	Arnošt
Hayek	Hayek	k1gMnSc1	Hayek
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Terezín	Terezín	k1gInSc1	Terezín
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
a	a	k8xC	a
dcera	dcera	k1gFnSc1	dcera
Sylva	Sylva	k1gFnSc1	Sylva
zahynuly	zahynout	k5eAaPmAgFnP	zahynout
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Osvětim	Osvětim	k1gFnSc4	Osvětim
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
majetkem	majetek	k1gInSc7	majetek
státu	stát	k1gInSc2	stát
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
postupně	postupně	k6eAd1	postupně
přestavován	přestavovat	k5eAaImNgInS	přestavovat
<g/>
.	.	kIx.	.
</s>
<s>
Vila	vila	k1gFnSc1	vila
Rajmunda	Rajmund	k1gMnSc2	Rajmund
Neumayera	Neumayer	k1gMnSc2	Neumayer
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Čecha	Čech	k1gMnSc2	Čech
č.	č.	k?	č.
886	[number]	k4	886
<g/>
.	.	kIx.	.
</s>
<s>
Architekt	architekt	k1gMnSc1	architekt
Bohumil	Bohumil	k1gMnSc1	Bohumil
Tureček	Tureček	k1gMnSc1	Tureček
ji	on	k3xPp3gFnSc4	on
postavil	postavit	k5eAaPmAgMnS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1931	[number]	k4	1931
<g/>
-	-	kIx~	-
<g/>
1933	[number]	k4	1933
pro	pro	k7c4	pro
advokáta	advokát	k1gMnSc4	advokát
Rajmunda	Rajmund	k1gMnSc4	Rajmund
Neumayera	Neumayer	k1gMnSc4	Neumayer
<g/>
,	,	kIx,	,
ředitele	ředitel	k1gMnSc4	ředitel
kyjovské	kyjovský	k2eAgFnSc2d1	Kyjovská
sklárny	sklárna	k1gFnSc2	sklárna
a	a	k8xC	a
člena	člen	k1gMnSc2	člen
obecní	obecní	k2eAgFnSc2d1	obecní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1989	[number]	k4	1989
vila	vila	k1gFnSc1	vila
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
prošla	projít	k5eAaPmAgFnS	projít
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
přestavbou	přestavba	k1gFnSc7	přestavba
<g/>
.	.	kIx.	.
</s>
<s>
Rodinný	rodinný	k2eAgInSc1d1	rodinný
dům	dům	k1gInSc1	dům
Anny	Anna	k1gFnSc2	Anna
a	a	k8xC	a
Josefa	Josef	k1gMnSc2	Josef
Jakerleových	Jakerleův	k2eAgFnPc2d1	Jakerleův
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Čecha	Čech	k1gMnSc2	Čech
č.	č.	k?	č.
896	[number]	k4	896
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
od	od	k7c2	od
architekta	architekt	k1gMnSc2	architekt
Josefa	Josef	k1gMnSc2	Josef
Poláška	Polášek	k1gMnSc2	Polášek
nechal	nechat	k5eAaPmAgInS	nechat
postavit	postavit	k5eAaPmF	postavit
člen	člen	k1gInSc1	člen
městského	městský	k2eAgNnSc2d1	Městské
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
a	a	k8xC	a
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
kyjovského	kyjovský	k2eAgInSc2d1	kyjovský
pivovaru	pivovar	k1gInSc2	pivovar
Josefa	Josef	k1gMnSc2	Josef
Jakerle	Jakerle	k1gFnSc2	Jakerle
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
stavby	stavba	k1gFnSc2	stavba
byl	být	k5eAaImAgInS	být
začleněn	začleněn	k2eAgInSc1d1	začleněn
i	i	k8xC	i
mlýnský	mlýnský	k2eAgInSc1d1	mlýnský
náhon	náhon	k1gInSc1	náhon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tekl	téct	k5eAaImAgMnS	téct
přes	přes	k7c4	přes
pozemek	pozemek	k1gInSc4	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byl	být	k5eAaImAgInS	být
objekt	objekt	k1gInSc1	objekt
přestavěn	přestavět	k5eAaPmNgInS	přestavět
na	na	k7c4	na
bytový	bytový	k2eAgInSc4d1	bytový
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Vila	vila	k1gFnSc1	vila
Gabriely	Gabriela	k1gFnSc2	Gabriela
a	a	k8xC	a
Vladimíra	Vladimíra	k1gFnSc1	Vladimíra
Dunděrových	Dunděrový	k2eAgMnPc2d1	Dunděrový
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Komenského	Komenského	k2eAgInSc2d1	Komenského
č.	č.	k?	č.
616	[number]	k4	616
<g/>
.	.	kIx.	.
</s>
<s>
Kyjovský	kyjovský	k2eAgMnSc1d1	kyjovský
architekt	architekt	k1gMnSc1	architekt
Josef	Josef	k1gMnSc1	Josef
Polášek	Polášek	k1gMnSc1	Polášek
postavil	postavit	k5eAaPmAgMnS	postavit
vilu	vila	k1gFnSc4	vila
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
pro	pro	k7c4	pro
lékaře	lékař	k1gMnPc4	lékař
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Dunděru	Dunděr	k1gInSc2	Dunděr
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Dunděra	Dunděra	k1gFnSc1	Dunděra
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
byli	být	k5eAaImAgMnP	být
popraveni	popravit	k5eAaPmNgMnP	popravit
nacisty	nacista	k1gMnPc7	nacista
v	v	k7c6	v
brněnských	brněnský	k2eAgFnPc6d1	brněnská
Kounicových	Kounicový	k2eAgFnPc6d1	Kounicová
kolejích	kolej	k1gFnPc6	kolej
za	za	k7c4	za
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
odbojem	odboj	k1gInSc7	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
byl	být	k5eAaImAgInS	být
konfiskován	konfiskovat	k5eAaBmNgInS	konfiskovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
přestavěn	přestavět	k5eAaPmNgMnS	přestavět
a	a	k8xC	a
sloužil	sloužit	k5eAaImAgMnS	sloužit
např.	např.	kA	např.
jako	jako	k8xS	jako
dětská	dětský	k2eAgFnSc1d1	dětská
poliklinika	poliklinika	k1gFnSc1	poliklinika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
navrácen	navrácen	k2eAgInSc1d1	navrácen
dědicům	dědic	k1gMnPc3	dědic
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgInS	využívat
ke	k	k7c3	k
komerčním	komerční	k2eAgInPc3d1	komerční
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Automatický	automatický	k2eAgInSc1d1	automatický
abecedně	abecedně	k6eAd1	abecedně
řazený	řazený	k2eAgInSc1d1	řazený
seznam	seznam	k1gInSc1	seznam
existujících	existující	k2eAgFnPc2d1	existující
biografií	biografie	k1gFnPc2	biografie
viz	vidět	k5eAaImRp2nS	vidět
Kategorie	kategorie	k1gFnSc1	kategorie
<g/>
:	:	kIx,	:
<g/>
Osobnosti	osobnost	k1gFnPc1	osobnost
z	z	k7c2	z
Kyjova	Kyjov	k1gInSc2	Kyjov
Moritz	moritz	k1gInSc4	moritz
von	von	k1gInSc1	von
Bader	Bader	k1gInSc1	Bader
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
-	-	kIx~	-
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rytíř	rytíř	k1gMnSc1	rytíř
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgMnSc1d1	stavební
inženýr	inženýr	k1gMnSc1	inženýr
(	(	kIx(	(
<g/>
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
budování	budování	k1gNnSc4	budování
Suezského	suezský	k2eAgInSc2d1	suezský
průplavu	průplav	k1gInSc2	průplav
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
konzul	konzul	k1gMnSc1	konzul
v	v	k7c6	v
Ismailii	Ismailie	k1gFnSc6	Ismailie
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
děd	děd	k1gMnSc1	děd
Alfreda	Alfred	k1gMnSc2	Alfred
Badera	Badero	k1gNnSc2	Badero
<g/>
,	,	kIx,	,
kyjovský	kyjovský	k2eAgMnSc1d1	kyjovský
rodák	rodák	k1gMnSc1	rodák
Jarmila	Jarmila	k1gFnSc1	Jarmila
Bednaříková	Bednaříková	k1gFnSc1	Bednaříková
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historička	historička	k1gFnSc1	historička
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
kyjovská	kyjovský	k2eAgFnSc1d1	Kyjovská
rodačka	rodačka	k1gFnSc1	rodačka
Václav	Václav	k1gMnSc1	Václav
Bzenecký	bzenecký	k2eAgMnSc1d1	bzenecký
starší	starší	k1gMnSc1	starší
(	(	kIx(	(
<g/>
†	†	k?	†
<g/>
1575	[number]	k4	1575
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kyjovský	kyjovský	k2eAgMnSc1d1	kyjovský
primátor	primátor	k1gMnSc1	primátor
Karel	Karel	k1gMnSc1	Karel
Dittler	Dittler	k1gMnSc1	Dittler
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1984	[number]	k4	1984
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
TSO	TSO	kA	TSO
<g/>
,	,	kIx,	,
kyjovský	kyjovský	k2eAgMnSc1d1	kyjovský
rodák	rodák	k1gMnSc1	rodák
a	a	k8xC	a
absolvent	absolvent	k1gMnSc1	absolvent
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
gymnázia	gymnázium	k1gNnSc2	gymnázium
Josef	Josef	k1gMnSc1	Josef
Frýbort	Frýbort	k1gInSc1	Frýbort
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
Radola	Radola	k1gFnSc1	Radola
Gajda	Gajda	k1gMnSc1	Gajda
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
-	-	kIx~	-
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
generál	generál	k1gMnSc1	generál
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
fašistický	fašistický	k2eAgMnSc1d1	fašistický
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
strávil	strávit	k5eAaPmAgMnS	strávit
dětství	dětství	k1gNnSc4	dětství
Ludvík	Ludvík	k1gMnSc1	Ludvík
Hilgert	Hilgert	k1gMnSc1	Hilgert
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
-	-	kIx~	-
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
funkcionalistický	funkcionalistický	k2eAgMnSc1d1	funkcionalistický
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
i.	i.	k?	i.
<g/>
m.	m.	k?	m.
<g/>
,	,	kIx,	,
kyjovský	kyjovský	k2eAgMnSc1d1	kyjovský
rodák	rodák	k1gMnSc1	rodák
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hurt	Hurt	k1gMnSc1	Hurt
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
-	-	kIx~	-
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
autor	autor	k1gMnSc1	autor
knihy	kniha	k1gFnSc2	kniha
Kyjovsko	Kyjovsko	k1gNnSc1	Kyjovsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
Sergěj	Sergěj	k1gMnSc1	Sergěj
Ingr	Ingr	k1gMnSc1	Ingr
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
-	-	kIx~	-
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
generál	generál	k1gMnSc1	generál
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
exilové	exilový	k2eAgFnSc2d1	exilová
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
Ivan	Ivan	k1gMnSc1	Ivan
Jelínek	Jelínek	k1gMnSc1	Jelínek
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
kyjovský	kyjovský	k2eAgMnSc1d1	kyjovský
rodák	rodák	k1gMnSc1	rodák
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
Severin	Severin	k1gMnSc1	Severin
Joklík	Joklík	k1gMnSc1	Joklík
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
-	-	kIx~	-
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
Josef	Josef	k1gMnSc1	Josef
Klvaňa	Klvaňa	k1gMnSc1	Klvaňa
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
-	-	kIx~	-
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
ředitel	ředitel	k1gMnSc1	ředitel
českého	český	k2eAgNnSc2d1	české
<g />
.	.	kIx.	.
</s>
<s>
kyjovského	kyjovský	k2eAgNnSc2d1	Kyjovské
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
,	,	kIx,	,
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
<g/>
,	,	kIx,	,
etnograf	etnograf	k1gMnSc1	etnograf
Jano	Jano	k1gMnSc1	Jano
Köhler	Köhler	k1gMnSc1	Köhler
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
freskami	freska	k1gFnPc7	freska
a	a	k8xC	a
sgrafity	sgrafito	k1gNnPc7	sgrafito
vyzdobil	vyzdobit	k5eAaPmAgInS	vyzdobit
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
gymnazijní	gymnazijní	k2eAgFnSc4d1	gymnazijní
kapli	kaple	k1gFnSc4	kaple
a	a	k8xC	a
zámeček	zámeček	k1gInSc1	zámeček
Josef	Josef	k1gMnSc1	Josef
Kolmaš	Kolmaš	k1gMnSc1	Kolmaš
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
sinolog	sinolog	k1gMnSc1	sinolog
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
tibetolog	tibetolog	k1gMnSc1	tibetolog
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
Karel	Karel	k1gMnSc1	Karel
Kozánek	Kozánek	k1gMnSc1	Kozánek
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
advokát	advokát	k1gMnSc1	advokát
<g/>
,	,	kIx,	,
činovník	činovník	k1gMnSc1	činovník
Sokola	Sokol	k1gMnSc2	Sokol
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
slavnosti	slavnost	k1gFnSc2	slavnost
"	"	kIx"	"
<g/>
Slovácký	slovácký	k2eAgInSc4d1	slovácký
rok	rok	k1gInSc4	rok
<g/>
"	"	kIx"	"
Jan	Jan	k1gMnSc1	Jan
Kučera	Kučera	k1gMnSc1	Kučera
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
-	-	kIx~	-
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
kyjovského	kyjovský	k2eAgNnSc2d1	Kyjovské
muzea	muzeum	k1gNnSc2	muzeum
a	a	k8xC	a
archivu	archiv	k1gInSc2	archiv
František	František	k1gMnSc1	František
Kudláč	Kudláč	k1gMnSc1	Kudláč
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Mareček	Mareček	k1gMnSc1	Mareček
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
disident	disident	k1gMnSc1	disident
<g/>
,	,	kIx,	,
signatář	signatář	k1gMnSc1	signatář
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
a	a	k8xC	a
hladovkář	hladovkář	k1gMnSc1	hladovkář
Miroslav	Miroslav	k1gMnSc1	Miroslav
Novák	Novák	k1gMnSc1	Novák
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
profesor-biblista	profesoriblista	k1gMnSc1	profesor-biblista
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
<g/>
,	,	kIx,	,
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
patriarcha	patriarcha	k1gMnSc1	patriarcha
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
kyjovský	kyjovský	k2eAgMnSc1d1	kyjovský
rodák	rodák	k1gMnSc1	rodák
a	a	k8xC	a
absolvent	absolvent	k1gMnSc1	absolvent
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
gymnázia	gymnázium	k1gNnSc2	gymnázium
Oldřich	Oldřich	k1gMnSc1	Oldřich
Pechal	Pechal	k1gMnSc1	Pechal
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
paradesantní	paradesantní	k2eAgFnSc2d1	paradesantní
skupiny	skupina	k1gFnSc2	skupina
Zinc	Zinc	k1gInSc1	Zinc
<g/>
,	,	kIx,	,
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
Josef	Josef	k1gMnSc1	Josef
Polášek	Polášek	k1gMnSc1	Polášek
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
funkcionalistický	funkcionalistický	k2eAgMnSc1d1	funkcionalistický
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
<g />
.	.	kIx.	.
</s>
<s>
i.	i.	k?	i.
<g/>
m.	m.	k?	m.
<g/>
,	,	kIx,	,
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Boršova	Boršův	k2eAgFnSc1d1	Boršova
Silvia	Silvia	k1gFnSc1	Silvia
Saint	Saint	k1gMnSc1	Saint
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pornoherečka	pornoherečka	k1gFnSc1	pornoherečka
<g/>
,	,	kIx,	,
kyjovská	kyjovský	k2eAgFnSc1d1	Kyjovská
rodačka	rodačka	k1gFnSc1	rodačka
Bohumil	Bohumila	k1gFnPc2	Bohumila
Sekla	seknout	k5eAaPmAgFnS	seknout
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
české	český	k2eAgFnSc2d1	Česká
lékařské	lékařský	k2eAgFnSc2d1	lékařská
genetiky	genetika	k1gFnSc2	genetika
<g/>
,	,	kIx,	,
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Bohuslavic	Bohuslavice	k1gFnPc2	Bohuslavice
<g />
.	.	kIx.	.
</s>
<s>
Ernst	Ernst	k1gMnSc1	Ernst
Schneider	Schneider	k1gMnSc1	Schneider
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
-	-	kIx~	-
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
lodního	lodní	k2eAgInSc2d1	lodní
šroubu	šroub	k1gInSc2	šroub
Voith-Schneider	Voith-Schneider	k1gMnSc1	Voith-Schneider
<g/>
,	,	kIx,	,
kyjovský	kyjovský	k2eAgMnSc1d1	kyjovský
rodák	rodák	k1gMnSc1	rodák
Miroslav	Miroslav	k1gMnSc1	Miroslav
Skála	Skála	k1gMnSc1	Skála
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
<g />
.	.	kIx.	.
</s>
<s>
i.	i.	k?	i.
<g/>
m.	m.	k?	m.
Hugo	Hugo	k1gMnSc1	Hugo
Sonnenschein	Sonnenschein	k1gMnSc1	Sonnenschein
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
kyjovský	kyjovský	k2eAgMnSc1d1	kyjovský
rodák	rodák	k1gMnSc1	rodák
Miroslav	Miroslav	k1gMnSc1	Miroslav
Tichý	Tichý	k1gMnSc1	Tichý
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
fotograf	fotograf	k1gMnSc1	fotograf
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Trtík	Trtík	k1gMnSc1	Trtík
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Husovy	Husův	k2eAgFnSc2d1	Husova
československé	československý	k2eAgFnSc2d1	Československá
bohoslovecké	bohoslovecký	k2eAgFnSc2d1	bohoslovecká
fakulty	fakulta	k1gFnSc2	fakulta
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kyjovský	kyjovský	k2eAgMnSc1d1	kyjovský
rodák	rodák	k1gMnSc1	rodák
a	a	k8xC	a
absolvent	absolvent	k1gMnSc1	absolvent
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
gymnázia	gymnázium	k1gNnSc2	gymnázium
Bohumil	Bohumil	k1gMnSc1	Bohumil
Tureček	Tureček	k1gMnSc1	Tureček
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
-	-	kIx~	-
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
funkcionalistický	funkcionalistický	k2eAgMnSc1d1	funkcionalistický
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
i.	i.	k?	i.
<g/>
m.	m.	k?	m.
<g/>
,	,	kIx,	,
kyjovský	kyjovský	k2eAgMnSc1d1	kyjovský
rodák	rodák	k1gMnSc1	rodák
Joža	Joža	k1gMnSc1	Joža
Uprka	Uprka	k1gMnSc1	Uprka
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
-	-	kIx~	-
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
slováckých	slovácký	k2eAgInPc2d1	slovácký
žánrových	žánrový	k2eAgInPc2d1	žánrový
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
ateliér	ateliér	k1gInSc1	ateliér
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vašíček	Vašíček	k1gMnSc1	Vašíček
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
Jan	Jan	k1gMnSc1	Jan
Znoj	znoj	k1gInSc1	znoj
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
-	-	kIx~	-
<g/>
1950	[number]	k4	1950
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
<g/>
,	,	kIx,	,
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Boršova	Boršův	k2eAgNnSc2d1	Boršův
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
sochy	socha	k1gFnSc2	socha
Miroslava	Miroslav	k1gMnSc2	Miroslav
Tyrše	Tyrš	k1gMnSc2	Tyrš
u	u	k7c2	u
sokolovny	sokolovna	k1gFnSc2	sokolovna
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
město	město	k1gNnSc4	město
Kyjov	Kyjov	k1gInSc4	Kyjov
postupně	postupně	k6eAd1	postupně
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
partnerství	partnerství	k1gNnSc6	partnerství
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
šesti	šest	k4xCc7	šest
partnerskými	partnerský	k2eAgNnPc7d1	partnerské
městy	město	k1gNnPc7	město
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
:	:	kIx,	:
Yvetot	Yvetot	k1gInSc1	Yvetot
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Hollabrunn	Hollabrunn	k1gInSc1	Hollabrunn
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Seravezza	Seravezza	k1gFnSc1	Seravezza
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Biograd	Biograd	k1gInSc1	Biograd
na	na	k7c4	na
Moru	mora	k1gFnSc4	mora
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Luck	Luck	k1gInSc1	Luck
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Prizren	Prizrna	k1gFnPc2	Prizrna
<g/>
,	,	kIx,	,
Kosovo	Kosův	k2eAgNnSc4d1	Kosovo
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
