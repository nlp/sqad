<s>
Na	na	k7c4	na
sliznici	sliznice	k1gFnSc4	sliznice
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
nejprve	nejprve	k6eAd1	nejprve
vzniká	vznikat	k5eAaImIp3nS	vznikat
benigní	benigní	k2eAgInSc4d1	benigní
polyp	polyp	k1gInSc4	polyp
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
změnám	změna	k1gFnPc3	změna
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nakonec	nakonec	k6eAd1	nakonec
vyústí	vyústit	k5eAaPmIp3nP	vyústit
v	v	k7c4	v
karcinom	karcinom	k1gInSc4	karcinom
<g/>
.	.	kIx.	.
</s>
