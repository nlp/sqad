<p>
<s>
Kolorektální	Kolorektální	k2eAgInSc1d1	Kolorektální
karcinom	karcinom	k1gInSc1	karcinom
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
přesně	přesně	k6eAd1	přesně
rakovina	rakovina	k1gFnSc1	rakovina
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
představuje	představovat	k5eAaImIp3nS	představovat
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejčastějších	častý	k2eAgNnPc2d3	nejčastější
nádorových	nádorový	k2eAgNnPc2d1	nádorové
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
používané	používaný	k2eAgNnSc4d1	používané
označení	označení	k1gNnSc4	označení
rakovina	rakovina	k1gFnSc1	rakovina
tlustého	tlusté	k1gNnSc2	tlusté
střeva	střevo	k1gNnSc2	střevo
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
synonymem	synonymum	k1gNnSc7	synonymum
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
MKN-10	MKN-10	k1gFnSc2	MKN-10
nemá	mít	k5eNaImIp3nS	mít
kolorektální	kolorektální	k2eAgInSc1d1	kolorektální
karcinom	karcinom	k1gInSc1	karcinom
jeden	jeden	k4xCgInSc4	jeden
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
diagnózy	diagnóza	k1gFnPc4	diagnóza
C	C	kA	C
<g/>
18	[number]	k4	18
<g/>
,	,	kIx,	,
C19	C19	k1gFnSc1	C19
a	a	k8xC	a
C	C	kA	C
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
nad	nad	k7c4	nad
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
smrt	smrt	k1gFnSc4	smrt
zhruba	zhruba	k6eAd1	zhruba
655	[number]	k4	655
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejpostiženějších	postižený	k2eAgInPc2d3	Nejpostiženější
států	stát	k1gInPc2	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
Česko	Česko	k1gNnSc1	Česko
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
se	s	k7c7	s
Slovenskem	Slovensko	k1gNnSc7	Slovensko
a	a	k8xC	a
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
rizikové	rizikový	k2eAgInPc1d1	rizikový
faktory	faktor	k1gInPc1	faktor
==	==	k?	==
</s>
</p>
<p>
<s>
Kolorektální	Kolorektální	k2eAgInSc1d1	Kolorektální
karcinom	karcinom	k1gInSc1	karcinom
obvykle	obvykle	k6eAd1	obvykle
nevzniká	vznikat	k5eNaImIp3nS	vznikat
náhle	náhle	k6eAd1	náhle
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
sliznici	sliznice	k1gFnSc4	sliznice
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
nejprve	nejprve	k6eAd1	nejprve
vzniká	vznikat	k5eAaImIp3nS	vznikat
benigní	benigní	k2eAgInSc4d1	benigní
polyp	polyp	k1gInSc4	polyp
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
změnám	změna	k1gFnPc3	změna
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nakonec	nakonec	k6eAd1	nakonec
vyústí	vyústit	k5eAaPmIp3nP	vyústit
v	v	k7c4	v
karcinom	karcinom	k1gInSc4	karcinom
<g/>
.	.	kIx.	.
</s>
<s>
Odstranění	odstranění	k1gNnSc1	odstranění
polypu	polyp	k1gInSc2	polyp
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
koloskopickém	koloskopický	k2eAgNnSc6d1	koloskopické
vyšetření	vyšetření	k1gNnSc6	vyšetření
tak	tak	k9	tak
představuje	představovat	k5eAaImIp3nS	představovat
vyléčení	vyléčení	k1gNnSc1	vyléčení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
===	===	k?	===
</s>
</p>
<p>
<s>
Poměrně	poměrně	k6eAd1	poměrně
výrazný	výrazný	k2eAgInSc1d1	výrazný
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
kolorektálního	kolorektální	k2eAgInSc2d1	kolorektální
karcinomu	karcinom	k1gInSc2	karcinom
má	mít	k5eAaImIp3nS	mít
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Nejsignifikantnějším	Nejsignifikantní	k2eAgInSc7d2	Nejsignifikantní
faktorem	faktor	k1gInSc7	faktor
(	(	kIx(	(
<g/>
karcinogenem	karcinogen	k1gInSc7	karcinogen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pití	pití	k1gNnSc1	pití
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgInPc1d1	vysoký
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
statisticky	statisticky	k6eAd1	statisticky
významný	významný	k2eAgInSc4d1	významný
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
množstvím	množství	k1gNnSc7	množství
snědeného	snědený	k2eAgNnSc2d1	snědené
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
rizikem	riziko	k1gNnSc7	riziko
vzniku	vznik	k1gInSc2	vznik
rakoviny	rakovina	k1gFnSc2	rakovina
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
každých	každý	k3xTgInPc2	každý
100	[number]	k4	100
gramů	gram	k1gInPc2	gram
denně	denně	k6eAd1	denně
zkonzumovaného	zkonzumovaný	k2eAgNnSc2d1	zkonzumované
červeného	červený	k2eAgNnSc2d1	červené
masa	maso	k1gNnSc2	maso
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc4	riziko
onemocnění	onemocnění	k1gNnPc2	onemocnění
o	o	k7c4	o
17	[number]	k4	17
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
jsou	být	k5eAaImIp3nP	být
rizikové	rizikový	k2eAgInPc1d1	rizikový
faktory	faktor	k1gInPc1	faktor
jako	jako	k8xS	jako
nedostatek	nedostatek	k1gInSc1	nedostatek
zeleniny	zelenina	k1gFnSc2	zelenina
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
<g/>
,	,	kIx,	,
úprava	úprava	k1gFnSc1	úprava
potravy	potrava	k1gFnSc2	potrava
grilováním	grilování	k1gNnSc7	grilování
a	a	k8xC	a
smažením	smažení	k1gNnSc7	smažení
<g/>
,	,	kIx,	,
konzumace	konzumace	k1gFnSc1	konzumace
uzenin	uzenina	k1gFnPc2	uzenina
a	a	k8xC	a
kouření	kouření	k1gNnSc2	kouření
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Vliv	vliv	k1gInSc1	vliv
dědičnosti	dědičnost	k1gFnSc2	dědičnost
===	===	k?	===
</s>
</p>
<p>
<s>
Kolorektální	Kolorektální	k2eAgInSc1d1	Kolorektální
karcinom	karcinom	k1gInSc1	karcinom
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
rodinách	rodina	k1gFnPc6	rodina
častěji	často	k6eAd2	často
<g/>
,	,	kIx,	,
podkladem	podklad	k1gInSc7	podklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
genetika	genetika	k1gFnSc1	genetika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
faktory	faktor	k1gInPc1	faktor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
stravovací	stravovací	k2eAgInPc4d1	stravovací
návyky	návyk	k1gInPc4	návyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
genetických	genetický	k2eAgFnPc2d1	genetická
poruch	porucha	k1gFnPc2	porucha
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
kolorektální	kolorektální	k2eAgNnSc1d1	kolorektální
karcinom	karcinom	k1gInSc4	karcinom
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nádor	nádor	k1gInSc1	nádor
součástí	součást	k1gFnPc2	součást
nějakého	nějaký	k3yIgInSc2	nějaký
genetického	genetický	k2eAgInSc2d1	genetický
syndromu	syndrom	k1gInSc2	syndrom
svědčí	svědčit	k5eAaImIp3nS	svědčit
především	především	k9	především
nízký	nízký	k2eAgInSc1d1	nízký
věk	věk	k1gInSc1	věk
nemocného	nemocný	k1gMnSc2	nemocný
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
velmi	velmi	k6eAd1	velmi
častý	častý	k2eAgInSc1d1	častý
výskyt	výskyt	k1gInSc1	výskyt
nádorových	nádorový	k2eAgNnPc2d1	nádorové
onemocnění	onemocnění	k1gNnPc2	onemocnění
v	v	k7c6	v
nižším	nízký	k2eAgInSc6d2	nižší
věku	věk	k1gInSc6	věk
u	u	k7c2	u
pokrevních	pokrevní	k2eAgFnPc2d1	pokrevní
příbuzných	příbuzná	k1gFnPc2	příbuzná
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Vliv	vliv	k1gInSc1	vliv
prostředí	prostředí	k1gNnSc2	prostředí
===	===	k?	===
</s>
</p>
<p>
<s>
Vlivem	vliv	k1gInSc7	vliv
prostředí	prostředí	k1gNnSc2	prostředí
jsou	být	k5eAaImIp3nP	být
myšleny	myslet	k5eAaImNgFnP	myslet
především	především	k9	především
klimatické	klimatický	k2eAgFnPc1d1	klimatická
podmínky	podmínka	k1gFnPc1	podmínka
a	a	k8xC	a
znečištění	znečištění	k1gNnSc1	znečištění
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
vlivy	vliv	k1gInPc1	vliv
nejsou	být	k5eNaImIp3nP	být
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
kolorektálního	kolorektální	k2eAgInSc2d1	kolorektální
karcinomu	karcinom	k1gInSc2	karcinom
výraznými	výrazný	k2eAgInPc7d1	výrazný
rizikovými	rizikový	k2eAgInPc7d1	rizikový
faktory	faktor	k1gInPc7	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
jako	jako	k9	jako
rizikové	rizikový	k2eAgFnPc1d1	riziková
jevit	jevit	k5eAaImF	jevit
dusičnany	dusičnan	k1gInPc4	dusičnan
v	v	k7c6	v
pitné	pitný	k2eAgFnSc6d1	pitná
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
====	====	k?	====
Familiární	familiární	k2eAgFnSc1d1	familiární
adenomatózní	adenomatózní	k2eAgFnSc1d1	adenomatózní
polypóza	polypóza	k1gFnSc1	polypóza
(	(	kIx(	(
<g/>
FAP	FAP	kA	FAP
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
FAP	FAP	kA	FAP
je	být	k5eAaImIp3nS	být
nejčastějším	častý	k2eAgMnSc7d3	nejčastější
geneticky	geneticky	k6eAd1	geneticky
podmíněným	podmíněný	k2eAgInSc7d1	podmíněný
syndromem	syndrom	k1gInSc7	syndrom
spojeným	spojený	k2eAgMnSc7d1	spojený
se	se	k3xPyFc4	se
vznikem	vznik	k1gInSc7	vznik
kolorektálního	kolorektální	k2eAgInSc2d1	kolorektální
karcinomu	karcinom	k1gInSc2	karcinom
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
popisuje	popisovat	k5eAaImIp3nS	popisovat
onemocnění	onemocnění	k1gNnSc1	onemocnění
-	-	kIx~	-
již	již	k9	již
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
nízkém	nízký	k2eAgInSc6d1	nízký
věku	věk	k1gInSc6	věk
se	se	k3xPyFc4	se
v	v	k7c6	v
tlustém	tlustý	k2eAgNnSc6d1	tlusté
střevě	střevo	k1gNnSc6	střevo
postiženého	postižený	k1gMnSc2	postižený
objevuje	objevovat	k5eAaImIp3nS	objevovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
polypů	polyp	k1gInPc2	polyp
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dříve	dříve	k6eAd2	dříve
nebo	nebo	k8xC	nebo
později	pozdě	k6eAd2	pozdě
zvrhnout	zvrhnout	k5eAaPmF	zvrhnout
v	v	k7c4	v
maligní	maligní	k2eAgInSc4d1	maligní
karcinom	karcinom	k1gInSc4	karcinom
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
možnou	možný	k2eAgFnSc7d1	možná
terapií	terapie	k1gFnSc7	terapie
je	být	k5eAaImIp3nS	být
sledování	sledování	k1gNnSc4	sledování
a	a	k8xC	a
průběžné	průběžný	k2eAgNnSc4d1	průběžné
odstraňování	odstraňování	k1gNnSc4	odstraňování
polypů	polyp	k1gInPc2	polyp
kolonoskopicky	kolonoskopicky	k6eAd1	kolonoskopicky
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
mutací	mutace	k1gFnSc7	mutace
jediného	jediný	k2eAgInSc2d1	jediný
genu	gen	k1gInSc2	gen
pojmenovaného	pojmenovaný	k2eAgInSc2d1	pojmenovaný
APC	APC	kA	APC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Gardnerův	Gardnerův	k2eAgInSc1d1	Gardnerův
syndrom	syndrom	k1gInSc1	syndrom
====	====	k?	====
</s>
</p>
<p>
<s>
U	u	k7c2	u
postižených	postižený	k1gMnPc2	postižený
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
polypů	polyp	k1gInPc2	polyp
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
osteomy	osteom	k1gInPc1	osteom
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
<g/>
nádory	nádor	k1gInPc1	nádor
kostí	kost	k1gFnPc2	kost
<g/>
)	)	kIx)	)
a	a	k8xC	a
fibromatóza	fibromatóza	k1gFnSc1	fibromatóza
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
mnohočetné	mnohočetný	k2eAgInPc4d1	mnohočetný
benigní	benigní	k2eAgInPc4d1	benigní
nádory	nádor	k1gInPc4	nádor
vaziva	vazivo	k1gNnSc2	vazivo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Turcotův	Turcotův	k2eAgInSc1d1	Turcotův
syndrom	syndrom	k1gInSc1	syndrom
====	====	k?	====
</s>
</p>
<p>
<s>
U	u	k7c2	u
postižených	postižený	k1gMnPc2	postižený
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
polypů	polyp	k1gInPc2	polyp
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
maligní	maligní	k2eAgInPc4d1	maligní
nádory	nádor	k1gInPc4	nádor
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Lynchův	Lynchův	k2eAgInSc1d1	Lynchův
syndrom	syndrom	k1gInSc1	syndrom
====	====	k?	====
</s>
</p>
<p>
<s>
U	u	k7c2	u
postiženého	postižený	k1gMnSc2	postižený
vzniká	vznikat	k5eAaImIp3nS	vznikat
již	již	k6eAd1	již
v	v	k7c6	v
časném	časný	k2eAgInSc6d1	časný
věku	věk	k1gInSc6	věk
kolorektální	kolorektální	k2eAgInSc4d1	kolorektální
karcinom	karcinom	k1gInSc4	karcinom
bez	bez	k7c2	bez
předchozího	předchozí	k2eAgInSc2d1	předchozí
vzniku	vznik	k1gInSc2	vznik
polypů	polyp	k1gMnPc2	polyp
(	(	kIx(	(
<g/>
Lynch	Lynch	k1gMnSc1	Lynch
I	I	kA	I
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Syndrom	syndrom	k1gInSc1	syndrom
Lynch	Lynch	k1gInSc1	Lynch
II	II	kA	II
je	být	k5eAaImIp3nS	být
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
výskytem	výskyt	k1gInSc7	výskyt
dalších	další	k2eAgInPc2d1	další
nádorů	nádor	k1gInPc2	nádor
-	-	kIx~	-
děložní	děložní	k2eAgFnSc2d1	děložní
sliznice	sliznice	k1gFnSc2	sliznice
<g/>
,	,	kIx,	,
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
pankreatu	pankreas	k1gInSc2	pankreas
<g/>
,	,	kIx,	,
kůže	kůže	k1gFnSc2	kůže
i	i	k8xC	i
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
identifikována	identifikován	k2eAgFnSc1d1	identifikována
řada	řada	k1gFnSc1	řada
genů	gen	k1gInPc2	gen
zodpovědných	zodpovědný	k2eAgInPc2d1	zodpovědný
za	za	k7c4	za
vznik	vznik	k1gInSc4	vznik
tohoto	tento	k3xDgInSc2	tento
syndromu	syndrom	k1gInSc2	syndrom
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
však	však	k9	však
příčinou	příčina	k1gFnSc7	příčina
mutace	mutace	k1gFnSc2	mutace
genů	gen	k1gInPc2	gen
MSH2	MSH2	k1gFnSc2	MSH2
a	a	k8xC	a
MLH	mlha	k1gFnPc2	mlha
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
Lynchův	Lynchův	k2eAgInSc1d1	Lynchův
syndrom	syndrom	k1gInSc1	syndrom
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
HNPCC	HNPCC	kA	HNPCC
(	(	kIx(	(
<g/>
Hereditary	Hereditar	k1gInPc1	Hereditar
NonPolyposis	NonPolyposis	k1gFnSc2	NonPolyposis
Colorectal	Colorectal	k1gMnSc1	Colorectal
Cancer	Cancer	k1gMnSc1	Cancer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klinické	klinický	k2eAgInPc1d1	klinický
projevy	projev	k1gInPc1	projev
==	==	k?	==
</s>
</p>
<p>
<s>
Nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
kolorektálního	kolorektální	k2eAgInSc2d1	kolorektální
karcinomu	karcinom	k1gInSc2	karcinom
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
poměrně	poměrně	k6eAd1	poměrně
dlouho	dlouho	k6eAd1	dlouho
skrytě	skrytě	k6eAd1	skrytě
a	a	k8xC	a
manifestovat	manifestovat	k5eAaBmF	manifestovat
se	se	k3xPyFc4	se
až	až	k9	až
v	v	k7c6	v
pokročilejším	pokročilý	k2eAgInSc6d2	pokročilejší
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k8xC	tedy
obtížněji	obtížně	k6eAd2	obtížně
řešitelném	řešitelný	k2eAgNnSc6d1	řešitelné
<g/>
,	,	kIx,	,
stádiu	stádium	k1gNnSc3	stádium
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
manifestuje	manifestovat	k5eAaBmIp3nS	manifestovat
krví	krev	k1gFnSc7	krev
ve	v	k7c6	v
stolici	stolice	k1gFnSc6	stolice
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
bolestí	bolest	k1gFnSc7	bolest
břicha	břich	k1gInSc2	břich
<g/>
,	,	kIx,	,
střídáním	střídání	k1gNnSc7	střídání
zácpy	zácpa	k1gFnSc2	zácpa
a	a	k8xC	a
průjmu	průjem	k1gInSc2	průjem
<g/>
,	,	kIx,	,
hlenem	hlen	k1gInSc7	hlen
ve	v	k7c6	v
stolici	stolice	k1gFnSc6	stolice
<g/>
,	,	kIx,	,
hubnutím	hubnutí	k1gNnSc7	hubnutí
nebo	nebo	k8xC	nebo
změnami	změna	k1gFnPc7	změna
chutě	chuť	k1gFnSc2	chuť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Screening	screening	k1gInSc4	screening
==	==	k?	==
</s>
</p>
<p>
<s>
Screeningově	Screeningově	k6eAd1	Screeningově
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
záchyt	záchyt	k1gInSc4	záchyt
podezření	podezření	k1gNnSc4	podezření
na	na	k7c4	na
kolorektální	kolorektální	k2eAgInSc4d1	kolorektální
karcinom	karcinom	k1gInSc4	karcinom
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
především	především	k9	především
palpační	palpační	k2eAgNnSc4d1	palpační
vyšetření	vyšetření	k1gNnSc4	vyšetření
per	prát	k5eAaImRp2nS	prát
rectum	rectum	k1gNnSc4	rectum
(	(	kIx(	(
<g/>
nádor	nádor	k1gInSc1	nádor
často	často	k6eAd1	často
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
úseku	úsek	k1gInSc6	úsek
střeva	střevo	k1gNnSc2	střevo
v	v	k7c6	v
dosahu	dosah	k1gInSc6	dosah
prstu	prst	k1gInSc2	prst
<g/>
)	)	kIx)	)
a	a	k8xC	a
biochemický	biochemický	k2eAgInSc4d1	biochemický
test	test	k1gInSc4	test
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
krve	krev	k1gFnSc2	krev
ve	v	k7c6	v
stolici	stolice	k1gFnSc6	stolice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současných	současný	k2eAgNnPc2d1	současné
doporučení	doporučení	k1gNnPc2	doporučení
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
screeningu	screening	k1gInSc2	screening
zahrnuti	zahrnut	k2eAgMnPc1d1	zahrnut
všichni	všechen	k3xTgMnPc1	všechen
pacienti	pacient	k1gMnPc1	pacient
od	od	k7c2	od
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
test	test	k1gInSc1	test
na	na	k7c4	na
skryté	skrytý	k2eAgNnSc4d1	skryté
krvácení	krvácení	k1gNnSc4	krvácení
ve	v	k7c6	v
stolici	stolice	k1gFnSc6	stolice
možno	možno	k6eAd1	možno
opakovat	opakovat	k5eAaImF	opakovat
každoročně	každoročně	k6eAd1	každoročně
do	do	k7c2	do
věku	věk	k1gInSc2	věk
55	[number]	k4	55
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
nový	nový	k2eAgInSc1d1	nový
populační	populační	k2eAgInSc1d1	populační
screening	screening	k1gInSc1	screening
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
možnost	možnost	k1gFnSc4	možnost
i	i	k8xC	i
provedení	provedení	k1gNnSc4	provedení
screeningové	screeningový	k2eAgFnSc2d1	screeningová
kolonoskopie	kolonoskopie	k1gFnSc2	kolonoskopie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diagnostika	diagnostika	k1gFnSc1	diagnostika
==	==	k?	==
</s>
</p>
<p>
<s>
Vyšetření	vyšetření	k1gNnSc1	vyšetření
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
metodami	metoda	k1gFnPc7	metoda
endoskopickými	endoskopický	k2eAgFnPc7d1	endoskopická
nebo	nebo	k8xC	nebo
zobrazovacími	zobrazovací	k2eAgFnPc7d1	zobrazovací
<g/>
,	,	kIx,	,
především	především	k9	především
rentgenovými	rentgenový	k2eAgInPc7d1	rentgenový
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
definitivní	definitivní	k2eAgFnSc4d1	definitivní
diagnózu	diagnóza	k1gFnSc4	diagnóza
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
odebrat	odebrat	k5eAaPmF	odebrat
vzorek	vzorek	k1gInSc4	vzorek
na	na	k7c4	na
histologické	histologický	k2eAgNnSc4d1	histologické
vyšetření	vyšetření	k1gNnSc4	vyšetření
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vyšetření	vyšetření	k1gNnSc4	vyšetření
pomocí	pomocí	k7c2	pomocí
endoskopu	endoskop	k1gInSc2	endoskop
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
vzorky	vzorek	k1gInPc1	vzorek
odebírané	odebíraný	k2eAgInPc1d1	odebíraný
přímo	přímo	k6eAd1	přímo
při	při	k7c6	při
operaci	operace	k1gFnSc6	operace
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazovací	zobrazovací	k2eAgFnPc1d1	zobrazovací
metody	metoda	k1gFnPc1	metoda
(	(	kIx(	(
<g/>
irigografie	irigografie	k1gFnPc1	irigografie
-	-	kIx~	-
kontrastní	kontrastní	k2eAgNnSc1d1	kontrastní
rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
vyšetření	vyšetření	k1gNnSc1	vyšetření
<g/>
,	,	kIx,	,
CT	CT	kA	CT
kolografie	kolografie	k1gFnSc1	kolografie
či	či	k8xC	či
MR	MR	kA	MR
kolografie	kolografie	k1gFnSc1	kolografie
<g/>
)	)	kIx)	)
odběr	odběr	k1gInSc1	odběr
vzorků	vzorek	k1gInPc2	vzorek
neumožňují	umožňovat	k5eNaImIp3nP	umožňovat
<g/>
.	.	kIx.	.
</s>
<s>
Histopatolog	Histopatolog	k1gMnSc1	Histopatolog
pak	pak	k6eAd1	pak
určí	určit	k5eAaPmIp3nS	určit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jde	jít	k5eAaImIp3nS	jít
vůbec	vůbec	k9	vůbec
o	o	k7c4	o
nádor	nádor	k1gInSc4	nádor
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
benigní	benigní	k2eAgMnSc1d1	benigní
či	či	k8xC	či
maligní	maligní	k2eAgMnSc1d1	maligní
a	a	k8xC	a
o	o	k7c4	o
jaký	jaký	k3yRgInSc4	jaký
typ	typ	k1gInSc4	typ
nádoru	nádor	k1gInSc2	nádor
jde	jít	k5eAaImIp3nS	jít
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgInPc2	tento
údajů	údaj	k1gInPc2	údaj
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
řídí	řídit	k5eAaImIp3nP	řídit
další	další	k2eAgFnPc1d1	další
terapie	terapie	k1gFnPc1	terapie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
typu	typ	k1gInSc2	typ
nádoru	nádor	k1gInSc2	nádor
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
určit	určit	k5eAaPmF	určit
ještě	ještě	k9	ještě
rozsah	rozsah	k1gInSc4	rozsah
postižení	postižení	k1gNnSc2	postižení
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
RTG	RTG	kA	RTG
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
sonografie	sonografie	k1gFnSc2	sonografie
jater	játra	k1gNnPc2	játra
a	a	k8xC	a
CT	CT	kA	CT
malé	malý	k2eAgFnSc2d1	malá
pánve	pánev	k1gFnSc2	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
sledování	sledování	k1gNnSc4	sledování
dalšího	další	k2eAgInSc2d1	další
vývoje	vývoj	k1gInSc2	vývoj
choroby	choroba	k1gFnSc2	choroba
se	se	k3xPyFc4	se
změří	změřit	k5eAaPmIp3nS	změřit
hladina	hladina	k1gFnSc1	hladina
tumor	tumor	k1gInSc4	tumor
markerů	marker	k1gInPc2	marker
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
markery	marker	k1gInPc4	marker
CEA	CEA	kA	CEA
a	a	k8xC	a
CA	ca	kA	ca
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mikroflóra	mikroflóra	k1gFnSc1	mikroflóra
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
napomoci	napomoct	k5eAaPmF	napomoct
k	k	k7c3	k
diagnostice	diagnostika	k1gFnSc3	diagnostika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Terapie	terapie	k1gFnSc2	terapie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Chirurgická	chirurgický	k2eAgFnSc1d1	chirurgická
terapie	terapie	k1gFnSc1	terapie
===	===	k?	===
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
odstranění	odstranění	k1gNnSc1	odstranění
části	část	k1gFnSc2	část
střeva	střevo	k1gNnSc2	střevo
s	s	k7c7	s
nádorem	nádor	k1gInSc7	nádor
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
veškerá	veškerý	k3xTgFnSc1	veškerý
nádorová	nádorový	k2eAgFnSc1d1	nádorová
masa	masa	k1gFnSc1	masa
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
výkon	výkon	k1gInSc4	výkon
za	za	k7c4	za
radikální	radikální	k2eAgFnSc4d1	radikální
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
uzdravující	uzdravující	k2eAgInPc1d1	uzdravující
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
radikální	radikální	k2eAgInSc4d1	radikální
výkon	výkon	k1gInSc4	výkon
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
i	i	k9	i
odstranění	odstranění	k1gNnSc1	odstranění
metastáz	metastáza	k1gFnPc2	metastáza
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
část	část	k1gFnSc1	část
střeva	střevo	k1gNnSc2	střevo
je	být	k5eAaImIp3nS	být
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
třeba	třeba	k6eAd1	třeba
vytvořit	vytvořit	k5eAaPmF	vytvořit
pacientovi	pacient	k1gMnSc3	pacient
umělý	umělý	k2eAgInSc4d1	umělý
vývod	vývod	k1gInSc4	vývod
–	–	k?	–
stomii	stomie	k1gFnSc6	stomie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Radioterapie	radioterapie	k1gFnSc2	radioterapie
===	===	k?	===
</s>
</p>
<p>
<s>
Radioterapie	radioterapie	k1gFnSc1	radioterapie
se	se	k3xPyFc4	se
u	u	k7c2	u
nádorů	nádor	k1gInPc2	nádor
pravého	pravý	k2eAgInSc2d1	pravý
tračníku	tračník	k1gInSc2	tračník
prakticky	prakticky	k6eAd1	prakticky
neprovádí	provádět	k5eNaImIp3nS	provádět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
střevo	střevo	k1gNnSc1	střevo
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
citlivé	citlivý	k2eAgNnSc1d1	citlivé
na	na	k7c4	na
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
nádorů	nádor	k1gInPc2	nádor
levého	levý	k2eAgInSc2d1	levý
tračníku	tračník	k1gInSc2	tračník
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nádorů	nádor	k1gInPc2	nádor
rekta	rektum	k1gNnSc2	rektum
a	a	k8xC	a
řiti	řiť	k1gFnSc2	řiť
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
radioterapie	radioterapie	k1gFnSc1	radioterapie
zvolena	zvolen	k2eAgFnSc1d1	zvolena
jako	jako	k8xC	jako
neoadjuvantní	oadjuvantní	k2eNgFnSc1d1	neoadjuvantní
terapie	terapie	k1gFnSc1	terapie
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zmenšení	zmenšení	k1gNnSc1	zmenšení
nádoru	nádor	k1gInSc2	nádor
před	před	k7c7	před
dalším	další	k2eAgInSc7d1	další
výkonem	výkon	k1gInSc7	výkon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chemoterapie	chemoterapie	k1gFnSc2	chemoterapie
===	===	k?	===
</s>
</p>
<p>
<s>
Chemoterapie	chemoterapie	k1gFnSc1	chemoterapie
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
buď	buď	k8xC	buď
jako	jako	k8xC	jako
adjuvantní	adjuvantní	k2eAgFnSc1d1	adjuvantní
léčba	léčba	k1gFnSc1	léčba
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
jako	jako	k8xS	jako
další	další	k2eAgFnSc1d1	další
léčba	léčba	k1gFnSc1	léčba
při	při	k7c6	při
operaci	operace	k1gFnSc6	operace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
vyšší	vysoký	k2eAgNnSc4d2	vyšší
riziko	riziko	k1gNnSc4	riziko
metastáz	metastáza	k1gFnPc2	metastáza
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
léčba	léčba	k1gFnSc1	léčba
pokročilého	pokročilý	k2eAgInSc2d1	pokročilý
metastatického	metastatický	k2eAgInSc2d1	metastatický
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
kombinace	kombinace	k1gFnSc1	kombinace
5	[number]	k4	5
<g/>
-fluorouracilu	luorouracil	k1gInSc2	-fluorouracil
a	a	k8xC	a
leukovorinu	leukovorin	k1gInSc2	leukovorin
(	(	kIx(	(
<g/>
protokol	protokol	k1gInSc1	protokol
FUFA	FUFA	kA	FUFA
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
irinotekan-	irinotekan-	k?	irinotekan-
FOLFIRI	FOLFIRI	kA	FOLFIRI
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
trojkombince	trojkombinka	k1gFnSc3	trojkombinka
+	+	kIx~	+
oxaliplatina	oxaliplatina	k1gFnSc1	oxaliplatina
<g/>
(	(	kIx(	(
<g/>
FOLFOX	FOLFOX	kA	FOLFOX
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapecitabin	Kapecitabina	k1gFnPc2	Kapecitabina
je	být	k5eAaImIp3nS	být
proléčivo	proléčivo	k1gNnSc4	proléčivo
5	[number]	k4	5
<g/>
-fluorouracilu	luorouracil	k1gInSc2	-fluorouracil
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
lze	lze	k6eAd1	lze
podávat	podávat	k5eAaImF	podávat
perorálně	perorálně	k6eAd1	perorálně
<g/>
.	.	kIx.	.
</s>
<s>
Cílená	cílený	k2eAgNnPc1d1	cílené
biologická	biologický	k2eAgNnPc1d1	biologické
léčiva	léčivo	k1gNnPc1	léčivo
jsou	být	k5eAaImIp3nP	být
bevacizumab	bevacizumab	k1gInSc4	bevacizumab
<g/>
,	,	kIx,	,
panitumumab	panitumumab	k1gInSc4	panitumumab
a	a	k8xC	a
cetuximab	cetuximab	k1gInSc4	cetuximab
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovějšími	nový	k2eAgFnPc7d3	nejnovější
látkami	látka	k1gFnPc7	látka
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
indikaci	indikace	k1gFnSc6	indikace
jsou	být	k5eAaImIp3nP	být
aflibercept	aflibercept	k1gInSc4	aflibercept
a	a	k8xC	a
regorafenib	regorafenib	k1gInSc4	regorafenib
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prognóza	prognóza	k1gFnSc1	prognóza
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
včasném	včasný	k2eAgInSc6d1	včasný
záchytu	záchyt	k1gInSc6	záchyt
je	být	k5eAaImIp3nS	být
prognóza	prognóza	k1gFnSc1	prognóza
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
,	,	kIx,	,
pětileté	pětiletý	k2eAgNnSc1d1	pětileté
přežití	přežití	k1gNnSc1	přežití
je	být	k5eAaImIp3nS	být
80	[number]	k4	80
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
při	při	k7c6	při
záchytu	záchyt	k1gInSc6	záchyt
pokročilých	pokročilý	k2eAgFnPc2d1	pokročilá
forem	forma	k1gFnPc2	forma
je	být	k5eAaImIp3nS	být
prognóza	prognóza	k1gFnSc1	prognóza
špatná	špatný	k2eAgFnSc1d1	špatná
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
doba	doba	k1gFnSc1	doba
přežití	přežití	k1gNnSc2	přežití
je	být	k5eAaImIp3nS	být
11	[number]	k4	11
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Chemoterapie	chemoterapie	k1gFnPc4	chemoterapie
<g/>
,	,	kIx,	,
cytostatika	cytostatikum	k1gNnPc4	cytostatikum
</s>
</p>
<p>
<s>
Onkologie	onkologie	k1gFnSc1	onkologie
</s>
</p>
<p>
<s>
Radioterapie	radioterapie	k1gFnSc1	radioterapie
</s>
</p>
<p>
<s>
Rakovina	rakovina	k1gFnSc1	rakovina
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
PETRUŽELKA	PETRUŽELKA	kA	PETRUŽELKA
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
<g/>
;	;	kIx,	;
KONOPÁSEK	KONOPÁSEK	kA	KONOPÁSEK
<g/>
,	,	kIx,	,
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
<g/>
.	.	kIx.	.
</s>
<s>
Klinická	klinický	k2eAgFnSc1d1	klinická
onkologie	onkologie	k1gFnSc1	onkologie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
395	[number]	k4	395
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
POVÝŠIL	povýšit	k5eAaPmAgMnS	povýšit
<g/>
,	,	kIx,	,
Ctibor	Ctibor	k1gMnSc1	Ctibor
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
patologie	patologie	k1gFnSc1	patologie
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7184	[number]	k4	7184
<g/>
-	-	kIx~	-
<g/>
484	[number]	k4	484
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kolorektální	kolorektální	k2eAgInSc4d1	kolorektální
karcinom	karcinom	k1gInSc4	karcinom
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Odborný	odborný	k2eAgInSc1d1	odborný
server	server	k1gInSc1	server
</s>
</p>
<p>
<s>
Statistika	statistika	k1gFnSc1	statistika
(	(	kIx(	(
<g/>
počty	počet	k1gInPc1	počet
nemocných	nemocný	k1gMnPc2	nemocný
<g/>
,	,	kIx,	,
zemřelých	zemřelý	k1gMnPc2	zemřelý
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
</s>
</p>
