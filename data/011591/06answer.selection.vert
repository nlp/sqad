<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
opustil	opustit	k5eAaPmAgMnS	opustit
aktivní	aktivní	k2eAgFnSc4d1	aktivní
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
k	k	k7c3	k
tématům	téma	k1gNnPc3	téma
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
i	i	k8xC	i
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
či	či	k8xC	či
Evropské	evropský	k2eAgFnSc3d1	Evropská
migrační	migrační	k2eAgFnSc3d1	migrační
krizi	krize	k1gFnSc3	krize
<g/>
.	.	kIx.	.
</s>
