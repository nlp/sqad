<s>
Manala	Manala	k1gFnSc1
(	(	kIx(
<g/>
album	album	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ManalaInterpretKorpiklaaniDruh	ManalaInterpretKorpiklaaniDruh	k1gInSc1
albastudiové	albastudiový	k2eAgFnSc2d1
albumVydáno	albumVydat	k5eAaImNgNnS,k5eAaPmNgNnS
<g/>
3.8	3.8	k4
<g/>
.2012	.2012	k4
<g/>
Nahráno	nahrát	k5eAaPmNgNnS,k5eAaBmNgNnS
<g/>
2011	#num#	k4
v	v	k7c4
Petrax	Petrax	k1gInSc4
Studios	Studios	k?
v	v	k7c6
Hollola	Hollola	k1gFnSc1
<g/>
,	,	kIx,
Finsko	Finsko	k1gNnSc1
<g/>
;	;	kIx,
Wave	Wave	k1gNnSc1
Studio	studio	k1gNnSc1
a	a	k8xC
Villvox	Villvox	k1gInSc1
Studio	studio	k1gNnSc1
v	v	k7c4
Lahti	Lahť	k1gFnPc4
<g/>
,	,	kIx,
FinskoŽánrFolk	FinskoŽánrFolk	k1gInSc1
metalDélka	metalDélka	k1gFnSc1
<g/>
45	#num#	k4
<g/>
:	:	kIx,
<g/>
48	#num#	k4
<g/>
VydavatelstvíNuclear	VydavatelstvíNuclear	k1gMnSc1
BlastProducentAksu	BlastProducentAksa	k1gFnSc4
HanttuKorpiklaani	HanttuKorpiklaaň	k1gFnSc3
chronologicky	chronologicky	k6eAd1
</s>
<s>
Ukon	Ukon	k1gMnSc1
Wacka	Wacka	k1gMnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Manala	Manala	k1gFnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Noita	Noita	k1gFnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Manala	Manala	k1gFnSc1
je	být	k5eAaImIp3nS
osmé	osmý	k4xOgNnSc1
studiové	studiový	k2eAgNnSc1d1
album	album	k1gNnSc1
finské	finský	k2eAgNnSc1d1
folk	folk	k1gInSc4
metalové	metalový	k2eAgFnSc2d1
kapely	kapela	k1gFnSc2
Korpiklaani	Korpiklaaň	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2012	#num#	k4
prostřednictvím	prostřednictvím	k7c2
Nuclear	Nucleara	k1gFnPc2
Blast	Blast	k1gFnSc1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Manala	Manala	k1gFnSc1
<g/>
"	"	kIx"
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
ve	v	k7c6
dvou	dva	k4xCgFnPc6
verzích	verze	k1gFnPc6
<g/>
,	,	kIx,
ve	v	k7c6
finštině	finština	k1gFnSc6
a	a	k8xC
v	v	k7c6
angličtině	angličtina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
skladeb	skladba	k1gFnPc2
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
NázevHudbaText	NázevHudbaText	k1gMnSc1
</s>
<s>
Délka	délka	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Kunnia	Kunnium	k1gNnSc2
<g/>
“	“	k?
JärveläKeskimäki	JärveläKeskimäki	k1gNnSc2
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Tuonelan	Tuonelan	k1gMnSc1
tuvilla	tuvilla	k1gMnSc1
<g/>
“	“	k?
JärveläJärvelä	JärveläJärvelä	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Rauta	Rauta	k1gFnSc1
<g/>
“	“	k?
JärveläKeskimäki	JärveläKeskimäk	k1gFnSc2
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Ruumiinmultaa	Ruumiinmultaa	k1gMnSc1
<g/>
“	“	k?
JärveläJyrkäs	JärveläJyrkäs	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
37	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Petoeläimen	Petoeläimen	k2eAgMnSc1d1
kuola	kuola	k1gMnSc1
<g/>
“	“	k?
JärveläKeskimäki	JärveläKeskimäk	k1gFnSc2
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Synkkä	Synkkä	k1gFnSc1
<g/>
“	“	k?
JärveläKeskimäki	JärveläKeskimäk	k1gFnSc2
</s>
<s>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Ievan	Ievan	k1gMnSc1
polkka	polkka	k1gMnSc1
<g/>
“	“	k?
folkováKettunen	folkováKettunen	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Husky	Husk	k1gInPc1
Sledge	Sledge	k1gInSc1
<g/>
“	“	k?
Rounakari-instrumentální-	Rounakari-instrumentální-	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
48	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Dolorous	Dolorous	k1gMnSc1
<g/>
“	“	k?
Järvelä-instrumentální-	Järvelä-instrumentální-	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Uni	Uni	k1gMnSc1
<g/>
“	“	k?
Aaltonen	Aaltonen	k1gInSc1
<g/>
,	,	kIx,
JärveläKeskimäki	JärveläKeskimäki	k1gNnSc1
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
49	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Metsälle	Metsälle	k1gInSc1
<g/>
“	“	k?
JärveläJärvelä	JärveläJärvelä	k1gFnSc2
</s>
<s>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
41	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Sumussa	Sumussa	k1gFnSc1
hämärän	hämärän	k1gMnSc1
aamun	aamun	k1gMnSc1
<g/>
“	“	k?
Aaltonen	Aaltonen	k1gInSc1
<g/>
,	,	kIx,
JärveläKeskimäki	JärveläKeskimäki	k1gNnSc1
</s>
<s>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
19	#num#	k4
</s>
<s>
Celková	celkový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
45	#num#	k4
<g/>
:	:	kIx,
<g/>
48	#num#	k4
</s>
