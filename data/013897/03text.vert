<s>
23	#num#	k4
(	(	kIx(
<g/>
číslo	číslo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
←	←	k?
22	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
24	#num#	k4
→	→	k?
</s>
<s>
←	←	k?
</s>
<s>
20	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
→	→	k?
</s>
<s>
Celé	celý	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
23	#num#	k4
</s>
<s>
dvacet	dvacet	k4xCc1
tři	tři	k4xCgFnPc4
</s>
<s>
Rozklad	rozklad	k1gInSc1
</s>
<s>
prvočíslo	prvočíslo	k1gNnSc1
</s>
<s>
Dělitelé	dělitel	k1gMnPc1
</s>
<s>
1	#num#	k4
<g/>
,	,	kIx,
23	#num#	k4
</s>
<s>
Římskými	římský	k2eAgFnPc7d1
číslicemi	číslice	k1gFnPc7
</s>
<s>
XXIII	XXIII	kA
</s>
<s>
Dvojkově	dvojkově	k6eAd1
</s>
<s>
101112	#num#	k4
</s>
<s>
Trojkově	trojkově	k6eAd1
</s>
<s>
2123	#num#	k4
</s>
<s>
Čtyřkově	Čtyřkově	k6eAd1
</s>
<s>
1134	#num#	k4
</s>
<s>
Pětkově	pětkově	k6eAd1
</s>
<s>
435	#num#	k4
</s>
<s>
Šestkově	šestkově	k6eAd1
</s>
<s>
356	#num#	k4
</s>
<s>
Sedmičkově	Sedmičkově	k6eAd1
</s>
<s>
327	#num#	k4
</s>
<s>
Osmičkově	osmičkově	k6eAd1
</s>
<s>
278	#num#	k4
</s>
<s>
Šestnáctkově	šestnáctkově	k6eAd1
</s>
<s>
1716	#num#	k4
</s>
<s>
Dvacet	dvacet	k4xCc4
tři	tři	k4xCgFnPc4
je	být	k5eAaImIp3nS
přirozené	přirozený	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následuje	následovat	k5eAaImIp3nS
po	po	k7c6
číslu	číslo	k1gNnSc3
dvacet	dvacet	k4xCc4
dva	dva	k4xCgMnPc1
a	a	k8xC
předchází	předcházet	k5eAaImIp3nP
číslu	číslo	k1gNnSc3
dvacet	dvacet	k4xCc4
čtyři	čtyři	k4xCgMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řadová	řadový	k2eAgFnSc1d1
číslovka	číslovka	k1gFnSc1
je	být	k5eAaImIp3nS
dvacátý	dvacátý	k4xOgMnSc1
třetí	třetí	k4xOgMnSc1
nebo	nebo	k8xC
třiadvacátý	třiadvacátý	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římskými	římský	k2eAgFnPc7d1
číslicemi	číslice	k1gFnPc7
se	se	k3xPyFc4
zapisuje	zapisovat	k5eAaImIp3nS
XXIII	XXIII	kA
<g/>
.	.	kIx.
</s>
<s>
Matematika	matematika	k1gFnSc1
</s>
<s>
Dvacet	dvacet	k4xCc4
tři	tři	k4xCgFnPc4
je	on	k3xPp3gMnPc4
</s>
<s>
deváté	devátý	k4xOgNnSc4
prvočíslo	prvočíslo	k1gNnSc4
a	a	k8xC
první	první	k4xOgNnSc1
liché	lichý	k2eAgNnSc1d1
prvočíslo	prvočíslo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
není	být	k5eNaImIp3nS
prvočíselnou	prvočíselný	k2eAgFnSc7d1
dvojicí	dvojice	k1gFnSc7
</s>
<s>
páté	pátý	k4xOgNnSc1
prvočíslo	prvočíslo	k1gNnSc1
Sophie	Sophie	k1gFnSc2
Germainové	Germainová	k1gFnSc2
</s>
<s>
jedno	jeden	k4xCgNnSc1
z	z	k7c2
mála	málo	k1gNnSc2
prvočísel	prvočíslo	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
mají	mít	k5eAaImIp3nP
tu	ten	k3xDgFnSc4
vlastnost	vlastnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
součet	součet	k1gInSc1
prvních	první	k4xOgInPc2
x	x	k?
(	(	kIx(
<g/>
23	#num#	k4
<g/>
)	)	kIx)
prvočísel	prvočíslo	k1gNnPc2
je	být	k5eAaImIp3nS
dělitelný	dělitelný	k2eAgInSc1d1
číslem	číslo	k1gNnSc7
x	x	k?
(	(	kIx(
<g/>
874	#num#	k4
je	být	k5eAaImIp3nS
dělitelné	dělitelný	k2eAgNnSc1d1
23	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
počet	počet	k1gInSc1
náhodně	náhodně	k6eAd1
vybraných	vybraný	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
ve	v	k7c6
skupině	skupina	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
je	být	k5eAaImIp3nS
podle	podle	k7c2
narozeninového	narozeninový	k2eAgInSc2d1
problému	problém	k1gInSc2
více	hodně	k6eAd2
než	než	k8xS
50	#num#	k4
<g/>
%	%	kIx~
šance	šance	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
dva	dva	k4xCgMnPc1
členové	člen	k1gMnPc1
mají	mít	k5eAaImIp3nP
narozeniny	narozeniny	k1gFnPc4
ve	v	k7c4
stejný	stejný	k2eAgInSc4d1
den	den	k1gInSc4
</s>
<s>
počet	počet	k1gInSc1
Hilbertových	Hilbertův	k2eAgInPc2d1
problémů	problém	k1gInPc2
</s>
<s>
Chemie	chemie	k1gFnSc1
</s>
<s>
23	#num#	k4
je	být	k5eAaImIp3nS
atomové	atomový	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
vanadu	vanad	k1gInSc2
</s>
<s>
23	#num#	k4
nukleonové	nukleonové	k2eAgInSc4d1
číslo	číslo	k1gNnSc1
stabilního	stabilní	k2eAgInSc2d1
izotopu	izotop	k1gInSc2
sodíku	sodík	k1gInSc2
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Žalm	žalm	k1gInSc1
23	#num#	k4
je	být	k5eAaImIp3nS
nejspíše	nejspíše	k9
nejvíce	nejvíce	k6eAd1,k6eAd3
citovaným	citovaný	k2eAgInSc7d1
žalmem	žalm	k1gInSc7
</s>
<s>
standardní	standardní	k2eAgInSc4d1
port	port	k1gInSc4
pro	pro	k7c4
telnet	telnet	k1gInSc4
</s>
<s>
23	#num#	k4
je	být	k5eAaImIp3nS
německý	německý	k2eAgInSc1d1
film	film	k1gInSc1
o	o	k7c6
hackerovi	hacker	k1gMnSc6
Karlu	Karel	k1gMnSc6
Kochovi	Koch	k1gMnSc6
</s>
<s>
Číslo	číslo	k1gNnSc1
23	#num#	k4
je	být	k5eAaImIp3nS
film	film	k1gInSc1
s	s	k7c7
Jimem	Jim	k1gMnSc7
Carreyem	Carrey	k1gMnSc7
</s>
<s>
Tank	tank	k1gInSc1
číslo	číslo	k1gNnSc1
23	#num#	k4
</s>
<s>
Číslo	číslo	k1gNnSc1
na	na	k7c6
dresu	dres	k1gInSc6
Michaela	Michael	k1gMnSc2
Jordana	Jordan	k1gMnSc2
<g/>
,	,	kIx,
když	když	k8xS
působil	působit	k5eAaImAgMnS
v	v	k7c6
týmu	tým	k1gInSc6
Chicago	Chicago	k1gNnSc4
Bulls	Bullsa	k1gFnPc2
</s>
<s>
23	#num#	k4
ranami	rána	k1gFnPc7
byl	být	k5eAaImAgMnS
zavražděn	zavražděn	k2eAgMnSc1d1
Gaius	Gaius	k1gMnSc1
Julius	Julius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
23	#num#	k4
(	(	kIx(
<g/>
number	numbra	k1gFnPc2
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
23	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Přirozená	přirozený	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
0	#num#	k4
<g/>
–	–	k?
<g/>
99	#num#	k4
</s>
<s>
0	#num#	k4
•	•	k?
1	#num#	k4
•	•	k?
2	#num#	k4
•	•	k?
3	#num#	k4
•	•	k?
4	#num#	k4
•	•	k?
5	#num#	k4
•	•	k?
6	#num#	k4
•	•	k?
7	#num#	k4
•	•	k?
8	#num#	k4
•	•	k?
9	#num#	k4
•	•	k?
10	#num#	k4
•	•	k?
11	#num#	k4
•	•	k?
12	#num#	k4
•	•	k?
13	#num#	k4
•	•	k?
14	#num#	k4
•	•	k?
15	#num#	k4
•	•	k?
16	#num#	k4
•	•	k?
17	#num#	k4
•	•	k?
18	#num#	k4
•	•	k?
19	#num#	k4
•	•	k?
20	#num#	k4
•	•	k?
21	#num#	k4
•	•	k?
22	#num#	k4
•	•	k?
23	#num#	k4
•	•	k?
24	#num#	k4
•	•	k?
25	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
26	#num#	k4
•	•	k?
27	#num#	k4
•	•	k?
28	#num#	k4
•	•	k?
29	#num#	k4
•	•	k?
30	#num#	k4
•	•	k?
31	#num#	k4
•	•	k?
32	#num#	k4
•	•	k?
33	#num#	k4
•	•	k?
34	#num#	k4
•	•	k?
35	#num#	k4
•	•	k?
36	#num#	k4
•	•	k?
37	#num#	k4
•	•	k?
38	#num#	k4
•	•	k?
39	#num#	k4
•	•	k?
40	#num#	k4
•	•	k?
41	#num#	k4
•	•	k?
42	#num#	k4
•	•	k?
43	#num#	k4
•	•	k?
44	#num#	k4
•	•	k?
45	#num#	k4
•	•	k?
46	#num#	k4
•	•	k?
47	#num#	k4
•	•	k?
48	#num#	k4
•	•	k?
49	#num#	k4
•	•	k?
50	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
51	#num#	k4
•	•	k?
52	#num#	k4
•	•	k?
53	#num#	k4
•	•	k?
54	#num#	k4
•	•	k?
55	#num#	k4
•	•	k?
56	#num#	k4
•	•	k?
57	#num#	k4
•	•	k?
58	#num#	k4
•	•	k?
59	#num#	k4
•	•	k?
60	#num#	k4
•	•	k?
61	#num#	k4
•	•	k?
62	#num#	k4
•	•	k?
63	#num#	k4
•	•	k?
64	#num#	k4
•	•	k?
65	#num#	k4
•	•	k?
66	#num#	k4
•	•	k?
67	#num#	k4
•	•	k?
68	#num#	k4
•	•	k?
69	#num#	k4
•	•	k?
70	#num#	k4
•	•	k?
71	#num#	k4
•	•	k?
72	#num#	k4
•	•	k?
73	#num#	k4
•	•	k?
74	#num#	k4
•	•	k?
75	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
76	#num#	k4
•	•	k?
77	#num#	k4
•	•	k?
78	#num#	k4
•	•	k?
79	#num#	k4
•	•	k?
80	#num#	k4
•	•	k?
81	#num#	k4
•	•	k?
82	#num#	k4
•	•	k?
83	#num#	k4
•	•	k?
84	#num#	k4
•	•	k?
85	#num#	k4
•	•	k?
86	#num#	k4
•	•	k?
87	#num#	k4
•	•	k?
88	#num#	k4
•	•	k?
89	#num#	k4
•	•	k?
90	#num#	k4
•	•	k?
91	#num#	k4
•	•	k?
92	#num#	k4
•	•	k?
93	#num#	k4
•	•	k?
94	#num#	k4
•	•	k?
95	#num#	k4
•	•	k?
96	#num#	k4
•	•	k?
97	#num#	k4
•	•	k?
98	#num#	k4
•	•	k?
99	#num#	k4
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
<g/>
99	#num#	k4
•	•	k?
100	#num#	k4
<g/>
–	–	k?
<g/>
199	#num#	k4
•	•	k?
200	#num#	k4
<g/>
–	–	k?
<g/>
299	#num#	k4
•	•	k?
300	#num#	k4
<g/>
–	–	k?
<g/>
399	#num#	k4
•	•	k?
400	#num#	k4
<g/>
–	–	k?
<g/>
499	#num#	k4
•	•	k?
500	#num#	k4
<g/>
–	–	k?
<g/>
599	#num#	k4
•	•	k?
600	#num#	k4
<g/>
–	–	k?
<g/>
699	#num#	k4
•	•	k?
700	#num#	k4
<g/>
–	–	k?
<g/>
799	#num#	k4
•	•	k?
800	#num#	k4
<g/>
–	–	k?
<g/>
899	#num#	k4
•	•	k?
900	#num#	k4
<g/>
–	–	k?
<g/>
999	#num#	k4
•	•	k?
1000	#num#	k4
<g/>
–	–	k?
<g/>
1099	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
