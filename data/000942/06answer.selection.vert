<s>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
udělována	udělovat	k5eAaImNgFnS	udělovat
každoročně	každoročně	k6eAd1	každoročně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
na	na	k7c6	na
základě	základ	k1gInSc6	základ
poslední	poslední	k2eAgFnSc2d1	poslední
vůle	vůle	k1gFnSc2	vůle
švédského	švédský	k2eAgMnSc2d1	švédský
vědce	vědec	k1gMnSc2	vědec
a	a	k8xC	a
průmyslníka	průmyslník	k1gMnSc2	průmyslník
Alfreda	Alfred	k1gMnSc2	Alfred
Nobela	Nobel	k1gMnSc2	Nobel
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc2	vynálezce
dynamitu	dynamit	k1gInSc2	dynamit
<g/>
.	.	kIx.	.
</s>
