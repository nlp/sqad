<s>
Historie	historie	k1gFnSc1	historie
města	město	k1gNnSc2	město
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k6eAd1	až
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
činí	činit	k5eAaImIp3nS	činit
tak	tak	k6eAd1	tak
z	z	k7c2	z
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
měst	město	k1gNnPc2	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
