<s>
U	u	k7c2	u
<g/>
:	:	kIx,	:
<g/>
fon	fon	k?	fon
je	být	k5eAaImIp3nS	být
mobilní	mobilní	k2eAgFnSc1d1	mobilní
CDMA	CDMA	kA	CDMA
síť	síť	k1gFnSc1	síť
(	(	kIx(	(
<g/>
provozovatel	provozovatel	k1gMnSc1	provozovatel
Air	Air	k1gFnSc2	Air
Telecom	Telecom	k1gInSc1	Telecom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
nabízí	nabízet	k5eAaImIp3nS	nabízet
mobilní	mobilní	k2eAgInSc4d1	mobilní
internet	internet	k1gInSc4	internet
<g/>
,	,	kIx,	,
bezdrátovou	bezdrátový	k2eAgFnSc4d1	bezdrátová
pevnou	pevný	k2eAgFnSc4d1	pevná
linku	linka	k1gFnSc4	linka
a	a	k8xC	a
digitální	digitální	k2eAgFnSc2d1	digitální
vysílačky	vysílačka	k1gFnSc2	vysílačka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
mohli	moct	k5eAaImAgMnP	moct
zákazníci	zákazník	k1gMnPc1	zákazník
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
síti	síť	k1gFnSc6	síť
rovněž	rovněž	k9	rovněž
využívat	využívat	k5eAaPmF	využívat
mobilní	mobilní	k2eAgFnPc4d1	mobilní
hlasové	hlasový	k2eAgFnPc4d1	hlasová
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
pak	pak	k6eAd1	pak
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
síť	síť	k1gFnSc1	síť
U	u	k7c2	u
<g/>
:	:	kIx,	:
<g/>
fon	fon	k?	fon
jako	jako	k8xC	jako
první	první	k4xOgInSc1	první
v	v	k7c6	v
ČR	ČR	kA	ČR
své	svůj	k3xOyFgNnSc4	svůj
zázemí	zázemí	k1gNnSc4	zázemí
novým	nový	k2eAgMnSc7d1	nový
virtuálním	virtuální	k2eAgMnSc7d1	virtuální
mobilním	mobilní	k2eAgMnSc7d1	mobilní
operátorům	operátor	k1gMnPc3	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Provozovatel	provozovatel	k1gMnSc1	provozovatel
sítě	síť	k1gFnSc2	síť
U	u	k7c2	u
<g/>
:	:	kIx,	:
<g/>
fon	fon	k?	fon
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
Air	Air	k1gMnSc1	Air
Telecom	Telecom	k1gInSc1	Telecom
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
své	svůj	k3xOyFgFnPc4	svůj
datové	datový	k2eAgFnPc4d1	datová
služby	služba	k1gFnPc4	služba
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
technologií	technologie	k1gFnPc2	technologie
CDMA	CDMA	kA	CDMA
2000	[number]	k4	2000
a	a	k8xC	a
EVDO	EVDO	kA	EVDO
revize	revize	k1gFnSc2	revize
A	A	kA	A
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
datovou	datový	k2eAgFnSc7d1	datová
sítí	síť	k1gFnSc7	síť
3	[number]	k4	3
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
plně	plně	k6eAd1	plně
srovnatelnou	srovnatelný	k2eAgFnSc4d1	srovnatelná
s	s	k7c7	s
UMTS	UMTS	kA	UMTS
<g/>
/	/	kIx~	/
<g/>
HSDPA	HSDPA	kA	HSDPA
<g/>
.	.	kIx.	.
</s>
<s>
Předností	přednost	k1gFnSc7	přednost
této	tento	k3xDgFnSc2	tento
technologie	technologie	k1gFnSc2	technologie
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
GSM	GSM	kA	GSM
sítím	sítí	k1gNnSc7	sítí
zejména	zejména	k9	zejména
zajištění	zajištění	k1gNnSc1	zajištění
širšího	široký	k2eAgNnSc2d2	širší
pokrytí	pokrytí	k1gNnSc2	pokrytí
signálem	signál	k1gInSc7	signál
i	i	k8xC	i
s	s	k7c7	s
nižším	nízký	k2eAgInSc7d2	nižší
počtem	počet	k1gInSc7	počet
základnových	základnový	k2eAgFnPc2d1	základnová
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
nejmladší	mladý	k2eAgInSc4d3	nejmladší
český	český	k2eAgInSc4d1	český
mobilní	mobilní	k2eAgInSc4d1	mobilní
operátor	operátor	k1gInSc4	operátor
U	u	k7c2	u
<g/>
:	:	kIx,	:
<g/>
fon	fon	k?	fon
spustil	spustit	k5eAaPmAgInS	spustit
svoji	svůj	k3xOyFgFnSc4	svůj
síť	síť	k1gFnSc4	síť
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
90	[number]	k4	90
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
provozovatelem	provozovatel	k1gMnSc7	provozovatel
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
MobilKom	MobilKom	k1gInSc1	MobilKom
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
vlastněna	vlastnit	k5eAaImNgFnS	vlastnit
investiční	investiční	k2eAgFnSc7d1	investiční
společností	společnost	k1gFnSc7	společnost
Penta	Pent	k1gInSc2	Pent
Investments	Investments	k1gInSc1	Investments
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
operátor	operátor	k1gInSc1	operátor
odkoupen	odkoupit	k5eAaPmNgInS	odkoupit
společností	společnost	k1gFnSc7	společnost
Divenno	Divenno	k6eAd1	Divenno
Holdings	Holdingsa	k1gFnPc2	Holdingsa
Limited	limited	k2eAgMnSc1d1	limited
a	a	k8xC	a
od	od	k7c2	od
1.11	[number]	k4	1.11
<g/>
.2012	.2012	k4	.2012
je	být	k5eAaImIp3nS	být
novým	nový	k2eAgMnSc7d1	nový
provozovatelem	provozovatel	k1gMnSc7	provozovatel
společnost	společnost	k1gFnSc1	společnost
Air	Air	k1gMnSc1	Air
Telecom	Telecom	k1gInSc1	Telecom
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
operátora	operátor	k1gMnSc4	operátor
koupila	koupit	k5eAaPmAgFnS	koupit
bez	bez	k7c2	bez
historických	historický	k2eAgInPc2d1	historický
dluhů	dluh	k1gInPc2	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Air	Air	k?	Air
Telecom	Telecom	k1gInSc1	Telecom
jako	jako	k8xC	jako
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
mobilní	mobilní	k2eAgInSc4d1	mobilní
operátor	operátor	k1gInSc4	operátor
v	v	k7c6	v
ČR	ČR	kA	ČR
také	také	k9	také
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
nabízí	nabízet	k5eAaImIp3nS	nabízet
služby	služba	k1gFnPc4	služba
v	v	k7c6	v
GSM	GSM	kA	GSM
standardu	standard	k1gInSc6	standard
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
své	svůj	k3xOyFgFnSc2	svůj
sítě	síť	k1gFnSc2	síť
AIR	AIR	kA	AIR
<g/>
.	.	kIx.	.
</s>
<s>
Služby	služba	k1gFnPc1	služba
GSM	GSM	kA	GSM
jsou	být	k5eAaImIp3nP	být
provozovány	provozovat	k5eAaImNgInP	provozovat
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
mobilního	mobilní	k2eAgMnSc2d1	mobilní
operátora	operátor	k1gMnSc2	operátor
T-Mobile	T-Mobila	k1gFnSc6	T-Mobila
<g/>
.	.	kIx.	.
</s>
<s>
Služby	služba	k1gFnPc1	služba
operátora	operátor	k1gMnSc2	operátor
U	U	kA	U
<g/>
:	:	kIx,	:
<g/>
fon	fon	k?	fon
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
využívalo	využívat	k5eAaPmAgNnS	využívat
135	[number]	k4	135
000	[number]	k4	000
zákazníků	zákazník	k1gMnPc2	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
Mobilní	mobilní	k2eAgFnSc1d1	mobilní
síť	síť	k1gFnSc1	síť
U	u	k7c2	u
<g/>
:	:	kIx,	:
<g/>
fon	fon	k?	fon
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
jak	jak	k6eAd1	jak
na	na	k7c4	na
rychlé	rychlý	k2eAgNnSc4d1	rychlé
mobilní	mobilní	k2eAgNnSc4d1	mobilní
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
Internetu	Internet	k1gInSc3	Internet
pomocí	pomocí	k7c2	pomocí
přístupové	přístupový	k2eAgFnSc2d1	přístupová
technologie	technologie	k1gFnSc2	technologie
CDMA	CDMA	kA	CDMA
2000	[number]	k4	2000
EV-DO	EV-DO	k1gFnSc2	EV-DO
Rev	Rev	k1gFnPc2	Rev
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
<g/>
,	,	kIx,	,
tak	tak	k9	tak
na	na	k7c4	na
hlasové	hlasový	k2eAgFnPc4d1	hlasová
služby	služba	k1gFnPc4	služba
,,	,,	k?	,,
<g/>
pevné	pevný	k2eAgFnPc4d1	pevná
<g/>
"	"	kIx"	"
-	-	kIx~	-
(	(	kIx(	(
<g/>
domácí	domácí	k2eAgFnPc4d1	domácí
"	"	kIx"	"
<g/>
pevné	pevný	k2eAgFnPc4d1	pevná
<g/>
"	"	kIx"	"
linky	linka	k1gFnPc4	linka
-	-	kIx~	-
tato	tento	k3xDgFnSc1	tento
domácí	domácí	k2eAgFnSc1d1	domácí
"	"	kIx"	"
<g/>
pevná	pevný	k2eAgFnSc1d1	pevná
<g/>
"	"	kIx"	"
linka	linka	k1gFnSc1	linka
nefunguje	fungovat	k5eNaImIp3nS	fungovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
klasické	klasický	k2eAgFnSc2d1	klasická
pevné	pevný	k2eAgFnSc2d1	pevná
linky	linka	k1gFnSc2	linka
<g/>
)	)	kIx)	)
a	a	k8xC	a
služby	služba	k1gFnSc2	služba
mobilní	mobilní	k2eAgFnSc2d1	mobilní
na	na	k7c6	na
frekvencích	frekvence	k1gFnPc6	frekvence
410	[number]	k4	410
až	až	k8xS	až
430	[number]	k4	430
MHz	Mhz	kA	Mhz
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
mobilní	mobilní	k2eAgFnSc2d1	mobilní
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přístup	přístup	k1gInSc1	přístup
CDMA	CDMA	kA	CDMA
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
kmitočty	kmitočet	k1gInPc4	kmitočet
standardizován	standardizován	k2eAgInSc1d1	standardizován
společností	společnost	k1gFnSc7	společnost
Qualcomm	Qualcomma	k1gFnPc2	Qualcomma
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
CDMA	CDMA	kA	CDMA
<g/>
2000	[number]	k4	2000
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
projektována	projektovat	k5eAaBmNgFnS	projektovat
pro	pro	k7c4	pro
pásma	pásmo	k1gNnPc4	pásmo
okolo	okolo	k7c2	okolo
2	[number]	k4	2
GHz	GHz	k1gFnPc2	GHz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
standardu	standard	k1gInSc6	standard
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
teoretickými	teoretický	k2eAgFnPc7d1	teoretická
maximálními	maximální	k2eAgFnPc7d1	maximální
přenosovými	přenosový	k2eAgFnPc7d1	přenosová
rychlostmi	rychlost	k1gFnPc7	rychlost
až	až	k9	až
3,1	[number]	k4	3,1
Mbit	Mbitum	k1gNnPc2	Mbitum
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
Rev	Rev	k1gFnSc2	Rev
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
až	až	k9	až
s	s	k7c7	s
rychlostmi	rychlost	k1gFnPc7	rychlost
okolo	okolo	k7c2	okolo
4,9	[number]	k4	4,9
Mbit	Mbita	k1gFnPc2	Mbita
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Síť	síť	k1gFnSc1	síť
podporuje	podporovat	k5eAaImIp3nS	podporovat
možnosti	možnost	k1gFnPc4	možnost
využití	využití	k1gNnSc2	využití
QoS	QoS	k1gFnSc2	QoS
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
řízení	řízení	k1gNnSc4	řízení
datových	datový	k2eAgInPc2d1	datový
toků	tok	k1gInPc2	tok
v	v	k7c6	v
přenosových	přenosový	k2eAgFnPc6d1	přenosová
sítích	síť	k1gFnPc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
<g/>
:	:	kIx,	:
<g/>
fon	fon	k?	fon
nabízí	nabízet	k5eAaImIp3nS	nabízet
také	také	k9	také
tarify	tarif	k1gInPc4	tarif
pro	pro	k7c4	pro
domácí	domácí	k1gMnPc4	domácí
"	"	kIx"	"
<g/>
pevné	pevný	k2eAgFnPc4d1	pevná
<g/>
"	"	kIx"	"
linky	linka	k1gFnPc4	linka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
kromě	kromě	k7c2	kromě
volání	volání	k1gNnSc2	volání
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
také	také	k9	také
posílání	posílání	k1gNnSc4	posílání
SMS	SMS	kA	SMS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
aktuálně	aktuálně	k6eAd1	aktuálně
nabízel	nabízet	k5eAaImAgInS	nabízet
tarify	tarifa	k1gFnSc2	tarifa
MINI	mini	k2eAgInSc1d1	mini
<g/>
,	,	kIx,	,
STANDARD	standard	k1gInSc1	standard
a	a	k8xC	a
RELAX	RELAX	kA	RELAX
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Pevné	pevný	k2eAgFnPc1d1	pevná
<g/>
"	"	kIx"	"
linky	linka	k1gFnPc1	linka
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgFnP	určit
jak	jak	k6eAd1	jak
do	do	k7c2	do
domácností	domácnost	k1gFnPc2	domácnost
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
pro	pro	k7c4	pro
firmy	firma	k1gFnPc4	firma
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
domácnosti	domácnost	k1gFnPc4	domácnost
nabízí	nabízet	k5eAaImIp3nS	nabízet
U	u	k7c2	u
<g/>
:	:	kIx,	:
<g/>
fon	fon	k?	fon
především	především	k6eAd1	především
benefit	benefit	k5eAaPmF	benefit
volání	volání	k1gNnSc4	volání
a	a	k8xC	a
SMS	SMS	kA	SMS
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
síti	síť	k1gFnSc6	síť
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
volání	volání	k1gNnSc4	volání
platí	platit	k5eAaImIp3nP	platit
uživatelé	uživatel	k1gMnPc1	uživatel
aktuálně	aktuálně	k6eAd1	aktuálně
1,50	[number]	k4	1,50
Kč	Kč	kA	Kč
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
hovory	hovor	k1gInPc4	hovor
jak	jak	k8xC	jak
na	na	k7c4	na
pevné	pevný	k2eAgFnPc4d1	pevná
linky	linka	k1gFnPc4	linka
<g/>
,	,	kIx,	,
tak	tak	k9	tak
do	do	k7c2	do
mobilních	mobilní	k2eAgFnPc2d1	mobilní
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Stejná	stejný	k2eAgFnSc1d1	stejná
cena	cena	k1gFnSc1	cena
platí	platit	k5eAaImIp3nS	platit
také	také	k9	také
pro	pro	k7c4	pro
telefonování	telefonování	k1gNnSc4	telefonování
do	do	k7c2	do
pevných	pevný	k2eAgFnPc2d1	pevná
sítí	síť	k1gFnPc2	síť
vybraných	vybraný	k2eAgFnPc2d1	vybraná
zemí	zem	k1gFnPc2	zem
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Pevná	pevný	k2eAgFnSc1d1	pevná
linka	linka	k1gFnSc1	linka
<g/>
"	"	kIx"	"
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
U	u	k7c2	u
<g/>
:	:	kIx,	:
<g/>
fon	fon	k?	fon
se	se	k3xPyFc4	se
nepřipojuje	připojovat	k5eNaImIp3nS	připojovat
na	na	k7c4	na
standardní	standardní	k2eAgFnSc4d1	standardní
pevnou	pevný	k2eAgFnSc4d1	pevná
telefonní	telefonní	k2eAgFnSc4d1	telefonní
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
funguje	fungovat	k5eAaImIp3nS	fungovat
bezdrátově	bezdrátově	k6eAd1	bezdrátově
a	a	k8xC	a
využívá	využívat	k5eAaImIp3nS	využívat
CDMA	CDMA	kA	CDMA
technologii	technologie	k1gFnSc6	technologie
(	(	kIx(	(
<g/>
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
pevnou	pevný	k2eAgFnSc4d1	pevná
linku	linka	k1gFnSc4	linka
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
(	(	kIx(	(
<g/>
využití	využití	k1gNnSc1	využití
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
CDMA	CDMA	kA	CDMA
technologie	technologie	k1gFnSc2	technologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
zřídit	zřídit	k5eAaPmF	zřídit
tuto	tento	k3xDgFnSc4	tento
"	"	kIx"	"
<g/>
pevnou	pevný	k2eAgFnSc4d1	pevná
linku	linka	k1gFnSc4	linka
<g/>
"	"	kIx"	"
i	i	k9	i
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
běžná	běžný	k2eAgFnSc1d1	běžná
telefonní	telefonní	k2eAgFnSc1d1	telefonní
přípojka	přípojka	k1gFnSc1	přípojka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
<g/>
:	:	kIx,	:
<g/>
fon	fon	k?	fon
mobil	mobil	k1gInSc1	mobil
<g/>
:	:	kIx,	:
790	[number]	k4	790
U	U	kA	U
<g/>
:	:	kIx,	:
<g/>
fon	fon	k?	fon
internetové	internetový	k2eAgNnSc1d1	internetové
volání	volání	k1gNnSc1	volání
<g/>
:	:	kIx,	:
910	[number]	k4	910
U	U	kA	U
<g/>
:	:	kIx,	:
<g/>
fon	fon	k?	fon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
spustil	spustit	k5eAaPmAgInS	spustit
také	také	k9	také
projekt	projekt	k1gInSc1	projekt
vlastních	vlastní	k2eAgFnPc2d1	vlastní
značkových	značkový	k2eAgFnPc2d1	značková
prodejen	prodejna	k1gFnPc2	prodejna
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
obchodních	obchodní	k2eAgInPc6d1	obchodní
domech	dům	k1gInPc6	dům
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vedení	vedení	k1gNnSc2	vedení
však	však	k9	však
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vydají	vydat	k5eAaPmIp3nP	vydat
cestou	cestou	k7c2	cestou
nízkonákladového	nízkonákladový	k2eAgMnSc2d1	nízkonákladový
operátora	operátor	k1gMnSc2	operátor
a	a	k8xC	a
veškeré	veškerý	k3xTgFnSc2	veškerý
prodejny	prodejna	k1gFnSc2	prodejna
byly	být	k5eAaImAgFnP	být
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
a	a	k8xC	a
nahradily	nahradit	k5eAaPmAgFnP	nahradit
je	on	k3xPp3gNnSc4	on
levnější	levný	k2eAgFnPc1d2	levnější
partnerské	partnerský	k2eAgFnPc1d1	partnerská
prodejny	prodejna	k1gFnPc1	prodejna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zákazník	zákazník	k1gMnSc1	zákazník
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
službu	služba	k1gFnSc4	služba
zakoupit	zakoupit	k5eAaPmF	zakoupit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
osobní	osobní	k2eAgInSc4d1	osobní
kontakt	kontakt	k1gInSc4	kontakt
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
možný	možný	k2eAgInSc1d1	možný
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
