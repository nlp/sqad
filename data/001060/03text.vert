<s>
Republika	republika	k1gFnSc1	republika
Togo	Togo	k1gNnSc4	Togo
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
République	République	k1gFnSc1	République
togolaise	togolaise	k1gFnSc1	togolaise
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
(	(	kIx(	(
<g/>
Beninský	Beninský	k2eAgInSc1d1	Beninský
záliv	záliv	k1gInSc1	záliv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Zlatonosné	zlatonosný	k2eAgNnSc1d1	zlatonosné
pobřeží	pobřeží	k1gNnSc1	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Rozlohou	rozloha	k1gFnSc7	rozloha
56785	[number]	k4	56785
km	km	kA	km
<g/>
2	[number]	k4	2
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
menší	malý	k2eAgInPc4d2	menší
státy	stát	k1gInPc4	stát
afrického	africký	k2eAgInSc2d1	africký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Ghanou	Ghana	k1gFnSc7	Ghana
(	(	kIx(	(
<g/>
877	[number]	k4	877
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Burkinou	Burkina	k1gFnSc7	Burkina
(	(	kIx(	(
<g/>
126	[number]	k4	126
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Beninem	Benino	k1gNnSc7	Benino
(	(	kIx(	(
<g/>
644	[number]	k4	644
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
jen	jen	k6eAd1	jen
56	[number]	k4	56
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
Toga	Togo	k1gNnSc2	Togo
se	se	k3xPyFc4	se
od	od	k7c2	od
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
postupně	postupně	k6eAd1	postupně
stalo	stát	k5eAaPmAgNnS	stát
cílem	cíl	k1gInSc7	cíl
zájmu	zájem	k1gInSc2	zájem
Portugalců	Portugalec	k1gMnPc2	Portugalec
<g/>
,	,	kIx,	,
Nizozemců	Nizozemec	k1gMnPc2	Nizozemec
<g/>
,	,	kIx,	,
Francouzů	Francouz	k1gMnPc2	Francouz
a	a	k8xC	a
Angličanů	Angličan	k1gMnPc2	Angličan
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
obchodníků	obchodník	k1gMnPc2	obchodník
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
kmene	kmen	k1gInSc2	kmen
Ewe	Ewe	k1gMnPc2	Ewe
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
kmenový	kmenový	k2eAgInSc1d1	kmenový
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
sever	sever	k1gInSc1	sever
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
domorodých	domorodý	k2eAgMnPc2d1	domorodý
království	království	k1gNnSc6	království
Dagomba	Dagomb	k1gMnSc4	Dagomb
a	a	k8xC	a
Gondža	Gondžus	k1gMnSc4	Gondžus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
zesílilo	zesílit	k5eAaPmAgNnS	zesílit
pronikání	pronikání	k1gNnSc3	pronikání
německých	německý	k2eAgMnPc2d1	německý
kolonizátorů	kolonizátor	k1gMnPc2	kolonizátor
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
byl	být	k5eAaImAgInS	být
nad	nad	k7c7	nad
jižní	jižní	k2eAgFnSc7d1	jižní
částí	část	k1gFnSc7	část
Toga	Togo	k1gNnSc2	Togo
vyhlášen	vyhlášen	k2eAgInSc1d1	vyhlášen
německý	německý	k2eAgInSc1d1	německý
protektorát	protektorát	k1gInSc1	protektorát
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
se	se	k3xPyFc4	se
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
území	území	k1gNnSc2	území
stala	stát	k5eAaPmAgFnS	stát
německá	německý	k2eAgFnSc1d1	německá
kolonie	kolonie	k1gFnSc1	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Togo	Togo	k1gNnSc4	Togo
obsadila	obsadit	k5eAaPmAgFnS	obsadit
francouzská	francouzský	k2eAgFnSc1d1	francouzská
a	a	k8xC	a
britská	britský	k2eAgNnPc4d1	Britské
vojska	vojsko	k1gNnPc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
bylo	být	k5eAaImAgNnS	být
Togo	Togo	k1gNnSc1	Togo
vyhlášeno	vyhlášen	k2eAgNnSc1d1	vyhlášeno
mandátním	mandátní	k2eAgNnSc7d1	mandátní
územím	území	k1gNnSc7	území
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Dahome	Dahom	k1gInSc5	Dahom
<g/>
)	)	kIx)	)
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
(	(	kIx(	(
<g/>
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
Zlatonosnému	zlatonosný	k2eAgNnSc3d1	zlatonosné
pobřeží	pobřeží	k1gNnSc3	pobřeží
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
francouzská	francouzský	k2eAgFnSc1d1	francouzská
část	část	k1gFnSc1	část
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
loajální	loajální	k2eAgInSc1d1	loajální
vichistickému	vichistický	k2eAgInSc3d1	vichistický
režimu	režim	k1gInSc3	režim
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
Svobodné	svobodný	k2eAgFnSc3d1	svobodná
Francii	Francie	k1gFnSc3	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
také	také	k9	také
Výbor	výbor	k1gInSc1	výbor
tožské	tožský	k2eAgFnSc2d1	tožský
jednoty	jednota	k1gFnSc2	jednota
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
významně	významně	k6eAd1	významně
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
úsilí	úsilí	k1gNnSc3	úsilí
za	za	k7c4	za
dosažení	dosažení	k1gNnSc4	dosažení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
se	se	k3xPyFc4	se
Togo	Togo	k1gNnSc1	Togo
stalo	stát	k5eAaPmAgNnS	stát
poručenským	poručenský	k2eAgNnSc7d1	poručenský
územím	území	k1gNnSc7	území
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
a	a	k8xC	a
francouzské	francouzský	k2eAgFnSc3d1	francouzská
správě	správa	k1gFnSc3	správa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
v	v	k7c4	v
Brity	Brit	k1gMnPc4	Brit
spravované	spravovaný	k2eAgFnSc2d1	spravovaná
části	část	k1gFnSc2	část
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
plebiscit	plebiscit	k1gInSc1	plebiscit
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
základě	základ	k1gInSc6	základ
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
připojení	připojení	k1gNnSc4	připojení
ke	k	k7c3	k
Zlatonosnému	zlatonosný	k2eAgNnSc3d1	zlatonosné
pobřeží	pobřeží	k1gNnSc3	pobřeží
(	(	kIx(	(
<g/>
Ghana	Ghana	k1gFnSc1	Ghana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
části	část	k1gFnSc2	část
byla	být	k5eAaImAgFnS	být
přiznána	přiznán	k2eAgFnSc1d1	přiznána
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
samospráva	samospráva	k1gFnSc1	samospráva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
se	se	k3xPyFc4	se
Togo	Togo	k1gNnSc1	Togo
stalo	stát	k5eAaPmAgNnS	stát
autonomní	autonomní	k2eAgFnSc7d1	autonomní
republikou	republika	k1gFnSc7	republika
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Francouzského	francouzský	k2eAgNnSc2d1	francouzské
společenství	společenství	k1gNnSc2	společenství
<g/>
.	.	kIx.	.
</s>
<s>
Úplná	úplný	k2eAgFnSc1d1	úplná
nezávislost	nezávislost	k1gFnSc1	nezávislost
Tožské	Tožský	k2eAgFnSc2d1	Tožský
republiky	republika	k1gFnSc2	republika
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Sylvanus	Sylvanus	k1gMnSc1	Sylvanus
É.	É.	kA	É.
Olympio	Olympio	k1gMnSc1	Olympio
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
se	se	k3xPyFc4	se
především	především	k6eAd1	především
snažil	snažit	k5eAaImAgMnS	snažit
omezit	omezit	k5eAaPmF	omezit
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c4	na
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Odmítal	odmítat	k5eAaImAgMnS	odmítat
také	také	k9	také
snahy	snaha	k1gFnSc2	snaha
francouzských	francouzský	k2eAgMnPc2d1	francouzský
vojáků	voják	k1gMnPc2	voják
demobilizovaných	demobilizovaný	k2eAgMnPc2d1	demobilizovaný
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
získat	získat	k5eAaPmF	získat
pozice	pozice	k1gFnPc4	pozice
v	v	k7c6	v
tožské	tožský	k2eAgFnSc6d1	tožský
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
skutečnosti	skutečnost	k1gFnPc1	skutečnost
nakonec	nakonec	k6eAd1	nakonec
vedly	vést	k5eAaImAgFnP	vést
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1963	[number]	k4	1963
k	k	k7c3	k
vojenskému	vojenský	k2eAgInSc3d1	vojenský
státnímu	státní	k2eAgInSc3d1	státní
převratu	převrat	k1gInSc3	převrat
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yQgInSc2	který
byl	být	k5eAaImAgMnS	být
Olympio	Olympio	k6eAd1	Olympio
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Nicolas	Nicolas	k1gInSc1	Nicolas
Grunitzky	Grunitzka	k1gFnSc2	Grunitzka
<g/>
,	,	kIx,	,
někdejší	někdejší	k2eAgMnSc1d1	někdejší
politický	politický	k2eAgMnSc1d1	politický
oponent	oponent	k1gMnSc1	oponent
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
hlavní	hlavní	k2eAgFnSc7d1	hlavní
snahou	snaha	k1gFnSc7	snaha
bylo	být	k5eAaImAgNnS	být
utlumit	utlumit	k5eAaPmF	utlumit
rozpory	rozpor	k1gInPc4	rozpor
mezi	mezi	k7c7	mezi
severem	sever	k1gInSc7	sever
a	a	k8xC	a
jihem	jih	k1gInSc7	jih
<g/>
,	,	kIx,	,
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
novou	nový	k2eAgFnSc4d1	nová
ústavu	ústava	k1gFnSc4	ústava
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
a	a	k8xC	a
zavést	zavést	k5eAaPmF	zavést
pluralitní	pluralitní	k2eAgInSc4d1	pluralitní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
nekrvavý	krvavý	k2eNgInSc1d1	nekrvavý
<g/>
,	,	kIx,	,
státní	státní	k2eAgInSc1d1	státní
převrat	převrat	k1gInSc1	převrat
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
puči	puč	k1gInSc6	puč
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gFnSc7	jeho
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
osobností	osobnost	k1gFnSc7	osobnost
Étienne	Étienn	k1gInSc5	Étienn
Eyadéma	Eyadémum	k1gNnPc4	Eyadémum
(	(	kIx(	(
<g/>
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
užíval	užívat	k5eAaImAgInS	užívat
africké	africký	k2eAgNnSc4d1	africké
jméno	jméno	k1gNnSc4	jméno
Gnassingbé	Gnassingbý	k2eAgNnSc1d1	Gnassingbý
Eyadéma	Eyadéma	k1gNnSc1	Eyadéma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
autoritativní	autoritativní	k2eAgInSc1d1	autoritativní
režim	režim	k1gInSc1	režim
<g/>
,	,	kIx,	,
jedinou	jediný	k2eAgFnSc7d1	jediná
povolenou	povolený	k2eAgFnSc7d1	povolená
politickou	politický	k2eAgFnSc7d1	politická
stranou	strana	k1gFnSc7	strana
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Shromáždění	shromáždění	k1gNnSc1	shromáždění
tožského	tožský	k2eAgInSc2d1	tožský
lidu	lid	k1gInSc2	lid
(	(	kIx(	(
<g/>
RPT	RPT	kA	RPT
<g/>
)	)	kIx)	)
a	a	k8xC	a
hlavním	hlavní	k2eAgMnSc7d1	hlavní
zahraničním	zahraniční	k2eAgMnSc7d1	zahraniční
spojencem	spojenec	k1gMnSc7	spojenec
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
francouzská	francouzský	k2eAgFnSc1d1	francouzská
vojenská	vojenský	k2eAgFnSc1d1	vojenská
pomoc	pomoc	k1gFnSc1	pomoc
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
pokusu	pokus	k1gInSc3	pokus
o	o	k7c4	o
státní	státní	k2eAgInSc4d1	státní
převrat	převrat	k1gInSc4	převrat
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
demokratizační	demokratizační	k2eAgFnSc1d1	demokratizační
vlna	vlna	k1gFnSc1	vlna
počátku	počátek	k1gInSc2	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
nátlak	nátlak	k1gInSc1	nátlak
Francie	Francie	k1gFnSc2	Francie
donutily	donutit	k5eAaPmAgInP	donutit
Eyadému	Eyadý	k2eAgInSc3d1	Eyadý
k	k	k7c3	k
povolení	povolení	k1gNnSc3	povolení
činnosti	činnost	k1gFnSc2	činnost
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Eyadéma	Eyadéma	k1gFnSc1	Eyadéma
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
RPT	RPT	kA	RPT
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
volebním	volební	k2eAgFnPc3d1	volební
manipulacím	manipulace	k1gFnPc3	manipulace
a	a	k8xC	a
opozičním	opoziční	k2eAgInSc6d1	opoziční
bojkotu	bojkot	k1gInSc6	bojkot
voleb	volba	k1gFnPc2	volba
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
opakovaně	opakovaně	k6eAd1	opakovaně
poráželi	porážet	k5eAaImAgMnP	porážet
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
své	svůj	k3xOyFgMnPc4	svůj
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
funkci	funkce	k1gFnSc4	funkce
si	se	k3xPyFc3	se
udržel	udržet	k5eAaPmAgMnS	udržet
úctyhodných	úctyhodný	k2eAgFnPc2d1	úctyhodná
38	[number]	k4	38
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
okamžitě	okamžitě	k6eAd1	okamžitě
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
prezidentem	prezident	k1gMnSc7	prezident
Fauru	Faur	k1gInSc2	Faur
Essozimnu	Essozimn	k1gInSc2	Essozimn
Gnassingbého	Gnassingbý	k2eAgInSc2d1	Gnassingbý
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
státníka	státník	k1gMnSc2	státník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
protestech	protest	k1gInPc6	protest
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
společenství	společenství	k1gNnSc2	společenství
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
okolních	okolní	k2eAgInPc2d1	okolní
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Gnassingbé	Gnassingbý	k2eAgFnSc2d1	Gnassingbý
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
dubnových	dubnový	k2eAgFnPc6d1	dubnová
volbách	volba	k1gFnPc6	volba
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
post	post	k1gInSc4	post
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Gnassingbé	Gnassingbý	k2eAgNnSc4d1	Gnassingbý
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
podpoře	podpora	k1gFnSc3	podpora
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
ovládá	ovládat	k5eAaImIp3nS	ovládat
RPT	RPT	kA	RPT
<g/>
.	.	kIx.	.
</s>
<s>
Řadu	řada	k1gFnSc4	řada
vládních	vládní	k2eAgInPc2d1	vládní
postů	post	k1gInPc2	post
však	však	k9	však
získali	získat	k5eAaPmAgMnP	získat
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
opozičních	opoziční	k2eAgFnPc2d1	opoziční
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Togo	Togo	k1gNnSc1	Togo
představuje	představovat	k5eAaImIp3nS	představovat
úzký	úzký	k2eAgInSc4d1	úzký
pruh	pruh	k1gInSc4	pruh
území	území	k1gNnSc2	území
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
asi	asi	k9	asi
600	[number]	k4	600
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
nízké	nízký	k2eAgNnSc1d1	nízké
<g/>
,	,	kIx,	,
s	s	k7c7	s
lagunami	laguna	k1gFnPc7	laguna
a	a	k8xC	a
bažinami	bažina	k1gFnPc7	bažina
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnPc1d1	jižní
plošiny	plošina	k1gFnPc1	plošina
přecházejí	přecházet	k5eAaImIp3nP	přecházet
v	v	k7c4	v
hornatou	hornatý	k2eAgFnSc4d1	hornatá
centrální	centrální	k2eAgFnSc4d1	centrální
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Sever	sever	k1gInSc1	sever
území	území	k1gNnSc2	území
představuje	představovat	k5eAaImIp3nS	představovat
mírně	mírně	k6eAd1	mírně
se	se	k3xPyFc4	se
sklánějící	sklánějící	k2eAgFnSc1d1	sklánějící
savana	savana	k1gFnSc1	savana
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
Mont	Mont	k2eAgInSc1d1	Mont
Agou	aga	k1gMnSc7	aga
(	(	kIx(	(
<g/>
986	[number]	k4	986
m	m	kA	m
<g/>
)	)	kIx)	)
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
tropickým	tropický	k2eAgNnSc7d1	tropické
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
,	,	kIx,	,
celoročně	celoročně	k6eAd1	celoročně
horkým	horký	k2eAgInSc7d1	horký
a	a	k8xC	a
vlhkým	vlhký	k2eAgInSc7d1	vlhký
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
zde	zde	k6eAd1	zde
převládaly	převládat	k5eAaImAgInP	převládat
tropické	tropický	k2eAgInPc1d1	tropický
deštné	deštný	k2eAgInPc1d1	deštný
pralesy	prales	k1gInPc1	prales
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
však	však	k9	však
většinou	většinou	k6eAd1	většinou
ustoupily	ustoupit	k5eAaPmAgFnP	ustoupit
plantážím	plantáž	k1gFnPc3	plantáž
<g/>
.	.	kIx.	.
</s>
<s>
Sever	sever	k1gInSc1	sever
Toga	Togo	k1gNnSc2	Togo
má	mít	k5eAaImIp3nS	mít
podnebí	podnebí	k1gNnSc4	podnebí
monzunového	monzunový	k2eAgInSc2d1	monzunový
typu	typ	k1gInSc2	typ
s	s	k7c7	s
horkým	horký	k2eAgNnSc7d1	horké
a	a	k8xC	a
vlhkým	vlhký	k2eAgNnSc7d1	vlhké
létem	léto	k1gNnSc7	léto
a	a	k8xC	a
suchou	suchý	k2eAgFnSc7d1	suchá
zimou	zima	k1gFnSc7	zima
<g/>
,	,	kIx,	,
typickým	typický	k2eAgInSc7d1	typický
porostem	porost	k1gInSc7	porost
je	být	k5eAaImIp3nS	být
savana	savana	k1gFnSc1	savana
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
toky	toka	k1gFnPc1	toka
jsou	být	k5eAaImIp3nP	být
krátké	krátký	k2eAgFnPc1d1	krátká
a	a	k8xC	a
směřují	směřovat	k5eAaImIp3nP	směřovat
k	k	k7c3	k
Atlantiku	Atlantik	k1gInSc3	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
klimatickým	klimatický	k2eAgFnPc3d1	klimatická
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
5	[number]	k4	5
regionů	region	k1gInPc2	region
<g/>
:	:	kIx,	:
Centrale	Central	k1gMnSc5	Central
<g/>
,	,	kIx,	,
Kara	kara	k1gFnSc1	kara
<g/>
,	,	kIx,	,
Maritime	Maritim	k1gMnSc5	Maritim
<g/>
,	,	kIx,	,
Plateaux	Plateaux	k1gInSc1	Plateaux
<g/>
,	,	kIx,	,
Savanes	Savanes	k1gInSc1	Savanes
<g/>
.	.	kIx.	.
</s>
<s>
Lomé	Lomé	k1gNnSc1	Lomé
-	-	kIx~	-
hlavní	hlavní	k2eAgMnSc1d1	hlavní
(	(	kIx(	(
<g/>
837,437	[number]	k4	837,437
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Sokodé	Sokodý	k2eAgInPc4d1	Sokodý
(	(	kIx(	(
<g/>
113,000	[number]	k4	113,000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Kara	kara	k1gFnSc1	kara
(	(	kIx(	(
<g/>
94,878	[number]	k4	94,878
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
sčítání	sčítání	k1gNnSc1	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
))	))	k?	))
Kpalimé	Kpalimý	k2eAgFnSc2d1	Kpalimý
(	(	kIx(	(
<g/>
95	[number]	k4	95
974	[number]	k4	974
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Atakpamé	Atakpamý	k2eAgInPc4d1	Atakpamý
(	(	kIx(	(
<g/>
80	[number]	k4	80
683	[number]	k4	683
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Bassar	Bassar	k1gMnSc1	Bassar
(	(	kIx(	(
<g/>
61	[number]	k4	61
845	[number]	k4	845
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
činil	činit	k5eAaImAgInS	činit
7	[number]	k4	7
154	[number]	k4	154
000	[number]	k4	000
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
hustotu	hustota	k1gFnSc4	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
106	[number]	k4	106
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
asi	asi	k9	asi
120	[number]	k4	120
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
nakažených	nakažený	k2eAgMnPc2d1	nakažený
virem	vir	k1gInSc7	vir
HIV	HIV	kA	HIV
(	(	kIx(	(
<g/>
38	[number]	k4	38
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
zhruba	zhruba	k6eAd1	zhruba
7	[number]	k4	7
700	[number]	k4	700
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
řadí	řadit	k5eAaImIp3nS	řadit
Togo	Togo	k1gNnSc4	Togo
na	na	k7c4	na
28	[number]	k4	28
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
států	stát	k1gInPc2	stát
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
úmrtností	úmrtnost	k1gFnSc7	úmrtnost
na	na	k7c4	na
AIDS	aids	k1gInSc4	aids
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
negativně	negativně	k6eAd1	negativně
projevit	projevit	k5eAaPmF	projevit
v	v	k7c6	v
nižší	nízký	k2eAgFnSc6d2	nižší
průměrné	průměrný	k2eAgFnSc6d1	průměrná
délce	délka	k1gFnSc6	délka
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnSc6d2	vyšší
úmrtnosti	úmrtnost	k1gFnSc6	úmrtnost
<g/>
,	,	kIx,	,
nižším	nízký	k2eAgInSc6d2	nižší
populačním	populační	k2eAgInSc6d1	populační
růstu	růst	k1gInSc6	růst
a	a	k8xC	a
změnám	změna	k1gFnPc3	změna
rozložení	rozložení	k1gNnSc2	rozložení
populace	populace	k1gFnPc1	populace
podle	podle	k7c2	podle
věku	věk	k1gInSc2	věk
a	a	k8xC	a
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
téměř	téměř	k6eAd1	téměř
30	[number]	k4	30
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
trvalou	trvalý	k2eAgFnSc7d1	trvalá
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věková	věkový	k2eAgFnSc1d1	věková
struktura	struktura	k1gFnSc1	struktura
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
40,8	[number]	k4	40,8
%	%	kIx~	%
(	(	kIx(	(
<g/>
1,006	[number]	k4	1,006
muže	muž	k1gMnSc2	muž
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
žena	žena	k1gFnSc1	žena
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
20	[number]	k4	20
%	%	kIx~	%
(	(	kIx(	(
<g/>
0,984	[number]	k4	0,984
muže	muž	k1gMnSc2	muž
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
žena	žena	k1gFnSc1	žena
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
54	[number]	k4	54
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
31,9	[number]	k4	31,9
%	%	kIx~	%
(	(	kIx(	(
<g/>
0,986	[number]	k4	0,986
muže	muž	k1gMnSc2	muž
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
žena	žena	k1gFnSc1	žena
<g/>
)	)	kIx)	)
55	[number]	k4	55
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
4,2	[number]	k4	4,2
%	%	kIx~	%
(	(	kIx(	(
<g/>
0,974	[number]	k4	0,974
muže	muž	k1gMnSc2	muž
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
žena	žena	k1gFnSc1	žena
<g/>
)	)	kIx)	)
65	[number]	k4	65
a	a	k8xC	a
více	hodně	k6eAd2	hodně
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
3,2	[number]	k4	3,2
<g />
.	.	kIx.	.
</s>
<s>
%	%	kIx~	%
(	(	kIx(	(
<g/>
0,974	[number]	k4	0,974
muže	muž	k1gMnSc2	muž
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
žena	žena	k1gFnSc1	žena
<g/>
)	)	kIx)	)
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
přírůstek	přírůstek	k1gInSc1	přírůstek
<g/>
:	:	kIx,	:
2,73	[number]	k4	2,73
%	%	kIx~	%
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
Porodnost	porodnost	k1gFnSc1	porodnost
<g/>
:	:	kIx,	:
34,9	[number]	k4	34,9
narozených	narozený	k2eAgMnPc2d1	narozený
<g/>
/	/	kIx~	/
<g/>
1000	[number]	k4	1000
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
Úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
<g/>
:	:	kIx,	:
7,6	[number]	k4	7,6
zemřelých	zemřelá	k1gFnPc2	zemřelá
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1000	[number]	k4	1000
obyv	obyv	k1gInSc1	obyv
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
Dětská	dětský	k2eAgFnSc1d1	dětská
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
<g/>
:	:	kIx,	:
56,24	[number]	k4	56,24
zemřelých	zemřelá	k1gFnPc2	zemřelá
<g/>
/	/	kIx~	/
<g/>
1000	[number]	k4	1000
živě	živě	k6eAd1	živě
narozených	narozený	k2eAgMnPc2d1	narozený
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
Fertilita	fertilita	k1gFnSc1	fertilita
<g/>
:	:	kIx,	:
4,79	[number]	k4	4,79
narozených	narozený	k2eAgFnPc2d1	narozená
dětí	dítě	k1gFnPc2	dítě
<g/>
/	/	kIx~	/
<g/>
žena	žena	k1gFnSc1	žena
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
Věkový	věkový	k2eAgInSc1d1	věkový
medián	medián	k1gInSc1	medián
<g/>
:	:	kIx,	:
19,5	[number]	k4	19,5
<g />
.	.	kIx.	.
</s>
<s>
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
muži	muž	k1gMnSc6	muž
<g/>
:	:	kIx,	:
19,2	[number]	k4	19,2
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
<g/>
:	:	kIx,	:
19,7	[number]	k4	19,7
letNaděje	letNadět	k5eAaPmIp3nS	letNadět
na	na	k7c4	na
dožití	dožití	k1gNnSc4	dožití
<g/>
:	:	kIx,	:
63,62	[number]	k4	63,62
let	léto	k1gNnPc2	léto
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
:	:	kIx,	:
61,07	[number]	k4	61,07
let	léto	k1gNnPc2	léto
-	-	kIx~	-
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
66,24	[number]	k4	66,24
let	léto	k1gNnPc2	léto
-	-	kIx~	-
ženy	žena	k1gFnPc1	žena
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Gramotnost	gramotnost	k1gFnSc1	gramotnost
<g/>
:	:	kIx,	:
60,4	[number]	k4	60,4
%	%	kIx~	%
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
muži	muž	k1gMnSc3	muž
74,1	[number]	k4	74,1
%	%	kIx~	%
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
48	[number]	k4	48
%	%	kIx~	%
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Urbanizace	urbanizace	k1gFnSc2	urbanizace
<g/>
:	:	kIx,	:
38	[number]	k4	38
%	%	kIx~	%
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Během	během	k7c2	během
příštích	příští	k2eAgNnPc2d1	příští
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
roční	roční	k2eAgInSc1d1	roční
výkyv	výkyv	k1gInSc1	výkyv
okolo	okolo	k7c2	okolo
3,3	[number]	k4	3,3
%	%	kIx~	%
Etnické	etnický	k2eAgNnSc1d1	etnické
složení	složení	k1gNnSc1	složení
<g/>
:	:	kIx,	:
99	[number]	k4	99
%	%	kIx~	%
<g />
.	.	kIx.	.
</s>
<s>
populace	populace	k1gFnPc1	populace
tvoří	tvořit	k5eAaImIp3nP	tvořit
Afričané	Afričan	k1gMnPc1	Afričan
<g/>
,	,	kIx,	,
z	z	k7c2	z
národností	národnost	k1gFnPc2	národnost
jsou	být	k5eAaImIp3nP	být
nejpočetnější	početní	k2eAgFnPc1d3	nejpočetnější
Ewe	Ewe	k1gFnPc1	Ewe
(	(	kIx(	(
<g/>
22,2	[number]	k4	22,2
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kabre	Kabr	k1gMnSc5	Kabr
(	(	kIx(	(
<g/>
13,4	[number]	k4	13,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Wachi	Wachi	k1gNnSc1	Wachi
(	(	kIx(	(
<g/>
10	[number]	k4	10
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mina	mina	k1gFnSc1	mina
(	(	kIx(	(
<g/>
5,6	[number]	k4	5,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kotokoli	Kotokole	k1gFnSc4	Kotokole
(	(	kIx(	(
<g/>
5,6	[number]	k4	5,6
<g />
.	.	kIx.	.
</s>
<s>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bimoba	Bimoba	k1gFnSc1	Bimoba
(	(	kIx(	(
<g/>
5,2	[number]	k4	5,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Losso	Lossa	k1gFnSc5	Lossa
(	(	kIx(	(
<g/>
4	[number]	k4	4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gurma	Gurma	k1gFnSc1	Gurma
(	(	kIx(	(
<g/>
3,4	[number]	k4	3,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lamba	Lamba	k1gMnSc1	Lamba
(	(	kIx(	(
<g/>
3,2	[number]	k4	3,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Adja	Adja	k1gMnSc1	Adja
(	(	kIx(	(
<g/>
3	[number]	k4	3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
obyvatel	obyvatel	k1gMnPc2	obyvatel
tvoří	tvořit	k5eAaImIp3nP	tvořit
Evropané	Evropan	k1gMnPc1	Evropan
a	a	k8xC	a
Asiaté	Asiat	k1gMnPc1	Asiat
ze	z	k7c2	z
syrsko-libanonské	syrskoibanonský	k2eAgFnSc2d1	syrsko-libanonský
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jazyky	jazyk	k1gInPc1	jazyk
<g/>
:	:	kIx,	:
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Převažují	převažovat	k5eAaImIp3nP	převažovat
však	však	k9	však
domorodé	domorodý	k2eAgInPc1d1	domorodý
jazyky	jazyk	k1gInPc1	jazyk
nigerokonžské	nigerokonžský	k2eAgFnSc2d1	nigerokonžský
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
(	(	kIx(	(
<g/>
jazyky	jazyk	k1gInPc1	jazyk
volta-kongo	voltaongo	k1gNnSc1	volta-kongo
-	-	kIx~	-
jazyky	jazyk	k1gInPc1	jazyk
kvaské	kvaská	k1gFnSc2	kvaská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Náboženství	náboženství	k1gNnSc1	náboženství
<g/>
:	:	kIx,	:
křesťané	křesťan	k1gMnPc1	křesťan
47	[number]	k4	47
%	%	kIx~	%
(	(	kIx(	(
<g/>
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
katolíků	katolík	k1gMnPc2	katolík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
muslimové	muslim	k1gMnPc1	muslim
14	[number]	k4	14
%	%	kIx~	%
<g/>
,	,	kIx,	,
tradiční	tradiční	k2eAgFnSc2d1	tradiční
víry	víra	k1gFnSc2	víra
méně	málo	k6eAd2	málo
než	než	k8xS	než
39	[number]	k4	39
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Togo	Togo	k1gNnSc1	Togo
je	být	k5eAaImIp3nS	být
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
republika	republika	k1gFnSc1	republika
s	s	k7c7	s
jednokomorovým	jednokomorový	k2eAgInSc7d1	jednokomorový
parlamentem	parlament	k1gInSc7	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
volený	volený	k2eAgMnSc1d1	volený
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
mandátů	mandát	k1gInPc2	mandát
není	být	k5eNaImIp3nS	být
omezen	omezit	k5eAaPmNgInS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Vládu	vláda	k1gFnSc4	vláda
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
prezident	prezident	k1gMnSc1	prezident
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
je	být	k5eAaImIp3nS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
má	mít	k5eAaImIp3nS	mít
jednokomorové	jednokomorový	k2eAgNnSc1d1	jednokomorové
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
.	.	kIx.	.
81	[number]	k4	81
poslanců	poslanec	k1gMnPc2	poslanec
je	být	k5eAaImIp3nS	být
voleno	volit	k5eAaImNgNnS	volit
na	na	k7c4	na
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
volby	volba	k1gFnPc1	volba
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
RPT	RPT	kA	RPT
<g/>
)	)	kIx)	)
obsadila	obsadit	k5eAaPmAgFnS	obsadit
50	[number]	k4	50
křesel	křeslo	k1gNnPc2	křeslo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
francouzském	francouzský	k2eAgInSc6d1	francouzský
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
ACP	ACP	kA	ACP
<g/>
,	,	kIx,	,
AfDB	AfDB	k1gFnSc1	AfDB
<g/>
,	,	kIx,	,
AU	au	k0	au
<g/>
,	,	kIx,	,
ECOWAS	ECOWAS	kA	ECOWAS
<g/>
,	,	kIx,	,
Entente	entente	k1gFnSc1	entente
<g/>
,	,	kIx,	,
FAO	FAO	kA	FAO
<g/>
,	,	kIx,	,
FZ	FZ	kA	FZ
<g/>
,	,	kIx,	,
G-	G-	k1gFnSc1	G-
<g/>
77	[number]	k4	77
<g/>
,	,	kIx,	,
IAEA	IAEA	kA	IAEA
<g/>
,	,	kIx,	,
IBRD	IBRD	kA	IBRD
<g/>
,	,	kIx,	,
ICAO	ICAO	kA	ICAO
<g/>
,	,	kIx,	,
ICC	ICC	kA	ICC
<g/>
,	,	kIx,	,
ICRM	ICRM	kA	ICRM
<g/>
,	,	kIx,	,
IDA	ido	k1gNnSc2	ido
<g/>
,	,	kIx,	,
IDB	IDB	kA	IDB
<g/>
,	,	kIx,	,
IFAD	IFAD	kA	IFAD
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
IFC	IFC	kA	IFC
<g/>
,	,	kIx,	,
IFRCS	IFRCS	kA	IFRCS
<g/>
,	,	kIx,	,
ILO	ILO	kA	ILO
<g/>
,	,	kIx,	,
IMF	IMF	kA	IMF
<g/>
,	,	kIx,	,
IMO	IMO	kA	IMO
<g/>
,	,	kIx,	,
Interpol	interpol	k1gInSc1	interpol
<g/>
,	,	kIx,	,
IOC	IOC	kA	IOC
<g/>
,	,	kIx,	,
IOM	IOM	kA	IOM
<g/>
,	,	kIx,	,
IPU	IPU	kA	IPU
<g/>
,	,	kIx,	,
ISO	ISO	kA	ISO
(	(	kIx(	(
<g/>
dopisovatel	dopisovatel	k1gMnSc1	dopisovatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ITSO	ITSO	kA	ITSO
<g/>
,	,	kIx,	,
ITU	ITU	kA	ITU
<g/>
,	,	kIx,	,
ITUC	ITUC	kA	ITUC
<g/>
,	,	kIx,	,
MIGA	MIGA	kA	MIGA
<g/>
,	,	kIx,	,
MINURCAT	MINURCAT	kA	MINURCAT
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
NAM	NAM	kA	NAM
<g/>
,	,	kIx,	,
OIC	OIC	kA	OIC
<g/>
,	,	kIx,	,
OIF	OIF	kA	OIF
<g/>
,	,	kIx,	,
OPCW	OPCW	kA	OPCW
<g/>
,	,	kIx,	,
PCA	PCA	kA	PCA
<g/>
,	,	kIx,	,
UN	UN	kA	UN
<g/>
,	,	kIx,	,
UNAMID	UNAMID	kA	UNAMID
<g/>
,	,	kIx,	,
UNCTAD	UNCTAD	kA	UNCTAD
<g/>
,	,	kIx,	,
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
UNIDO	UNIDO	kA	UNIDO
<g/>
,	,	kIx,	,
UNMIL	UNMIL	kA	UNMIL
<g/>
,	,	kIx,	,
UNOCI	UNOCI	kA	UNOCI
<g/>
,	,	kIx,	,
UNWTO	UNWTO	kA	UNWTO
<g/>
,	,	kIx,	,
UPU	UPU	kA	UPU
<g/>
,	,	kIx,	,
WADB	WADB	kA	WADB
,	,	kIx,	,
WAEMU	WAEMU	kA	WAEMU
<g/>
,	,	kIx,	,
WCL	WCL	kA	WCL
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
WCO	WCO	kA	WCO
<g/>
,	,	kIx,	,
WFTU	WFTU	kA	WFTU
<g/>
,	,	kIx,	,
WHO	WHO	kA	WHO
<g/>
,	,	kIx,	,
WIPO	WIPO	kA	WIPO
<g/>
,	,	kIx,	,
WMO	WMO	kA	WMO
<g/>
,	,	kIx,	,
WTO	WTO	kA	WTO
CUT	CUT	kA	CUT
=	=	kIx~	=
Výbor	výbor	k1gInSc1	výbor
tožské	tožský	k2eAgFnSc2d1	tožský
jednoty	jednota	k1gFnSc2	jednota
(	(	kIx(	(
<g/>
Comité	Comitý	k2eAgFnSc2d1	Comitý
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Unité	unita	k1gMnPc1	unita
Togolaise	Togolaise	k1gFnSc2	Togolaise
<g/>
)	)	kIx)	)
MPT	MPT	kA	MPT
=	=	kIx~	=
Hnutí	hnutí	k1gNnSc1	hnutí
tožského	tožský	k2eAgInSc2d1	tožský
lidu	lid	k1gInSc2	lid
(	(	kIx(	(
<g/>
Mouvement	Mouvement	k1gMnSc1	Mouvement
des	des	k1gNnSc2	des
Personnes	Personnes	k1gMnSc1	Personnes
Togolaises	Togolaises	k1gMnSc1	Togolaises
<g/>
)	)	kIx)	)
RPT	RPT	kA	RPT
=	=	kIx~	=
Shromáždění	shromáždění	k1gNnSc1	shromáždění
tožského	tožské	k1gNnSc2	tožské
<g />
.	.	kIx.	.
</s>
<s>
lidu	lid	k1gInSc2	lid
(	(	kIx(	(
<g/>
Rassemblement	Rassemblement	k1gInSc1	Rassemblement
du	du	k?	du
Peuple	Peuple	k1gFnSc1	Peuple
Togolais	Togolais	k1gFnSc1	Togolais
<g/>
)	)	kIx)	)
UFC	UFC	kA	UFC
=	=	kIx~	=
Unie	unie	k1gFnSc1	unie
sil	síla	k1gFnPc2	síla
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
(	(	kIx(	(
<g/>
Union	union	k1gInSc1	union
des	des	k1gNnSc1	des
Forces	Forces	k1gInSc1	Forces
du	du	k?	du
Changement	Changement	k1gInSc1	Changement
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1960	[number]	k4	1960
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1961	[number]	k4	1961
Sylvanus	Sylvanus	k1gMnSc1	Sylvanus
Épiphanio	Épiphanio	k1gMnSc1	Épiphanio
Olympio	Olympio	k1gMnSc1	Olympio
-	-	kIx~	-
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
<g/>
;	;	kIx,	;
CUT	CUT	kA	CUT
14	[number]	k4	14
<g/>
<g />
.	.	kIx.	.
</s>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
1961	[number]	k4	1961
-	-	kIx~	-
13	[number]	k4	13
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1963	[number]	k4	1963
Sylvanus	Sylvanus	k1gMnSc1	Sylvanus
Épiphanio	Épiphanio	k1gMnSc1	Épiphanio
Olympio	Olympio	k1gMnSc1	Olympio
-	-	kIx~	-
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
CUT	CUT	kA	CUT
13	[number]	k4	13
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1963	[number]	k4	1963
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1963	[number]	k4	1963
Emmanuel	Emmanuel	k1gMnSc1	Emmanuel
Bodjollé	Bodjoll	k1gMnPc1	Bodjoll
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
Vzbouřeneckého	vzbouřenecký	k2eAgInSc2d1	vzbouřenecký
výboru	výbor	k1gInSc2	výbor
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
15	[number]	k4	15
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1963	[number]	k4	1963
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1963	[number]	k4	1963
Nicolas	Nicolas	k1gInSc1	Nicolas
Grunitzky	Grunitzka	k1gFnSc2	Grunitzka
-	-	kIx~	-
úřadující	úřadující	k2eAgMnSc1d1	úřadující
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
MPT	MPT	kA	MPT
6	[number]	k4	6
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1963	[number]	k4	1963
-	-	kIx~	-
13	[number]	k4	13
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1967	[number]	k4	1967
Nicolas	Nicolas	k1gInSc1	Nicolas
Grunitzky	Grunitzka	k1gFnSc2	Grunitzka
-	-	kIx~	-
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
MPT	MPT	kA	MPT
14	[number]	k4	14
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1967	[number]	k4	1967
-	-	kIx~	-
14	[number]	k4	14
<g/>
<g />
.	.	kIx.	.
</s>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
1967	[number]	k4	1967
-	-	kIx~	-
Kléber	Kléber	k1gMnSc1	Kléber
Dadjo	Dadjo	k1gMnSc1	Dadjo
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
Výboru	výbor	k1gInSc2	výbor
národního	národní	k2eAgNnSc2d1	národní
smíření	smíření	k1gNnSc2	smíření
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
14	[number]	k4	14
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1967	[number]	k4	1967
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
-	-	kIx~	-
(	(	kIx(	(
<g/>
Étienne	Étienn	k1gMnSc5	Étienn
<g/>
)	)	kIx)	)
Gnassingbé	Gnassingbý	k2eAgNnSc1d1	Gnassingbý
Eyadéma	Eyadéma	k1gNnSc1	Eyadéma
-	-	kIx~	-
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
<g/>
,	,	kIx,	,
RPT	RPT	kA	RPT
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
-	-	kIx~	-
Faure	Faur	k1gInSc5	Faur
Essozimna	Essozimno	k1gNnSc2	Essozimno
Gnassingbé	Gnassingbý	k2eAgNnSc1d1	Gnassingbý
-	-	kIx~	-
úřadující	úřadující	k2eAgMnSc1d1	úřadující
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
RPT	RPT	kA	RPT
25	[number]	k4	25
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
-	-	kIx~	-
Abass	Abass	k1gInSc1	Abass
Bonfoh	Bonfoh	k1gInSc1	Bonfoh
-	-	kIx~	-
úřadující	úřadující	k2eAgMnSc1d1	úřadující
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
RPT	RPT	kA	RPT
Od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
-	-	kIx~	-
Faure	Faur	k1gInSc5	Faur
Essozimna	Essozimn	k1gMnSc2	Essozimn
Gnassingbé	Gnassingbý	k2eAgFnPc1d1	Gnassingbý
-	-	kIx~	-
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
RPT	RPT	kA	RPT
Togo	Togo	k1gNnSc1	Togo
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
nejméně	málo	k6eAd3	málo
rozvinutých	rozvinutý	k2eAgFnPc2d1	rozvinutá
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
sektorem	sektor	k1gInSc7	sektor
je	být	k5eAaImIp3nS	být
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Trvalým	trvalý	k2eAgInSc7d1	trvalý
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
nedostatek	nedostatek	k1gInSc1	nedostatek
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
vyrobit	vyrobit	k5eAaPmF	vyrobit
jen	jen	k6eAd1	jen
asi	asi	k9	asi
třetinu	třetina	k1gFnSc4	třetina
své	svůj	k3xOyFgFnSc2	svůj
spotřeby	spotřeba	k1gFnSc2	spotřeba
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
dovozem	dovoz	k1gInSc7	dovoz
z	z	k7c2	z
Ghany	Ghana	k1gFnSc2	Ghana
a	a	k8xC	a
Nigérie	Nigérie	k1gFnSc2	Nigérie
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
tvoří	tvořit	k5eAaImIp3nS	tvořit
2,771	[number]	k4	2,771
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
CFA	CFA	kA	CFA
frank	frank	k1gInSc4	frank
(	(	kIx(	(
<g/>
Communaute	Communaut	k1gMnSc5	Communaut
Financiere	financier	k1gMnSc5	financier
Africaine	Africain	k1gMnSc5	Africain
franc	franc	k1gInSc1	franc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Inflace	inflace	k1gFnSc1	inflace
měny	měna	k1gFnSc2	měna
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
2,6	[number]	k4	2,6
%	%	kIx~	%
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
66	[number]	k4	66
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
Zemědělství	zemědělství	k1gNnSc1	zemědělství
je	být	k5eAaImIp3nS	být
páteří	páteř	k1gFnSc7	páteř
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
chronickým	chronický	k2eAgInSc7d1	chronický
nedostatkem	nedostatek	k1gInSc7	nedostatek
financí	finance	k1gFnPc2	finance
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
zavlažovacích	zavlažovací	k2eAgNnPc2d1	zavlažovací
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
hnojiv	hnojivo	k1gNnPc2	hnojivo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
podstatně	podstatně	k6eAd1	podstatně
snižuje	snižovat	k5eAaImIp3nS	snižovat
jeho	jeho	k3xOp3gFnSc4	jeho
výkonnost	výkonnost	k1gFnSc4	výkonnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
tvorbě	tvorba	k1gFnSc6	tvorba
HDP	HDP	kA	HDP
se	se	k3xPyFc4	se
zemědělství	zemědělství	k1gNnSc1	zemědělství
podílí	podílet	k5eAaImIp3nS	podílet
28,2	[number]	k4	28,2
%	%	kIx~	%
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
a	a	k8xC	a
skýtá	skýtat	k5eAaImIp3nS	skýtat
obživu	obživa	k1gFnSc4	obživa
pro	pro	k7c4	pro
49	[number]	k4	49
%	%	kIx~	%
práceschopné	práceschopný	k2eAgFnSc2d1	práceschopná
populace	populace	k1gFnSc2	populace
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
soběstačná	soběstačný	k2eAgFnSc1d1	soběstačná
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
domácí	domácí	k2eAgFnSc4d1	domácí
spotřebu	spotřeba	k1gFnSc4	spotřeba
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
především	především	k9	především
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
kasava	kasava	k1gFnSc1	kasava
<g/>
,	,	kIx,	,
proso	proso	k1gNnSc1	proso
<g/>
,	,	kIx,	,
sorgo	sorgo	k1gNnSc1	sorgo
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnPc1	rýže
a	a	k8xC	a
yamy	yama	k1gFnPc1	yama
<g/>
,	,	kIx,	,
na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
kakao	kakao	k1gNnSc1	kakao
a	a	k8xC	a
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
z	z	k7c2	z
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
plodin	plodina	k1gFnPc2	plodina
pak	pak	k6eAd1	pak
hlavně	hlavně	k9	hlavně
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
Příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
vývozu	vývoz	k1gInSc2	vývoz
uvedených	uvedený	k2eAgFnPc2d1	uvedená
surovin	surovina	k1gFnPc2	surovina
jsou	být	k5eAaImIp3nP	být
závislé	závislý	k2eAgInPc1d1	závislý
na	na	k7c4	na
počasí	počasí	k1gNnSc4	počasí
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
i	i	k8xC	i
úrodě	úroda	k1gFnSc6	úroda
<g/>
)	)	kIx)	)
a	a	k8xC	a
kolísání	kolísání	k1gNnSc1	kolísání
cen	cena	k1gFnPc2	cena
na	na	k7c6	na
světových	světový	k2eAgInPc6d1	světový
trzích	trh	k1gInPc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
živočišné	živočišný	k2eAgFnSc6d1	živočišná
výrobě	výroba	k1gFnSc6	výroba
dominuje	dominovat	k5eAaImIp3nS	dominovat
chov	chov	k1gInSc4	chov
skotu	skot	k1gInSc2	skot
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
je	být	k5eAaImIp3nS	být
i	i	k9	i
pobřežní	pobřežní	k2eAgInSc1d1	pobřežní
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
těžba	těžba	k1gFnSc1	těžba
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
asi	asi	k9	asi
33,9	[number]	k4	33,9
%	%	kIx~	%
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
a	a	k8xC	a
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
12	[number]	k4	12
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
je	být	k5eAaImIp3nS	být
těžba	těžba	k1gFnSc1	těžba
a	a	k8xC	a
zpracování	zpracování	k1gNnSc1	zpracování
fosfátů	fosfát	k1gInPc2	fosfát
<g/>
,	,	kIx,	,
Togo	Togo	k1gNnSc1	Togo
disponuje	disponovat	k5eAaBmIp3nS	disponovat
čtvrtými	čtvrtá	k1gFnPc7	čtvrtá
největšími	veliký	k2eAgNnPc7d3	veliký
nalezišti	naleziště	k1gNnPc7	naleziště
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
však	však	k9	však
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
těžby	těžba	k1gFnSc2	těžba
této	tento	k3xDgFnSc2	tento
suroviny	surovina	k1gFnSc2	surovina
<g/>
,	,	kIx,	,
do	do	k7c2	do
odvětví	odvětví	k1gNnSc2	odvětví
bude	být	k5eAaImBp3nS	být
třeba	třeba	k6eAd1	třeba
silně	silně	k6eAd1	silně
investovat	investovat	k5eAaBmF	investovat
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
zásoby	zásoba	k1gFnPc4	zásoba
vápence	vápenec	k1gInSc2	vápenec
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
perspektivní	perspektivní	k2eAgFnSc1d1	perspektivní
je	být	k5eAaImIp3nS	být
výroba	výroba	k1gFnSc1	výroba
cementu	cement	k1gInSc2	cement
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgNnPc2d1	další
odvětví	odvětví	k1gNnPc2	odvětví
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
potravinářský	potravinářský	k2eAgMnSc1d1	potravinářský
(	(	kIx(	(
<g/>
pivovar	pivovar	k1gInSc1	pivovar
<g/>
)	)	kIx)	)
a	a	k8xC	a
textilní	textilní	k2eAgInSc1d1	textilní
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
roste	růst	k5eAaImIp3nS	růst
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
asi	asi	k9	asi
o	o	k7c4	o
5,5	[number]	k4	5,5
%	%	kIx~	%
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Služby	služba	k1gFnPc1	služba
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
37,9	[number]	k4	37,9
%	%	kIx~	%
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
a	a	k8xC	a
zaměstnávají	zaměstnávat	k5eAaImIp3nP	zaměstnávat
34	[number]	k4	34
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bilance	bilance	k1gFnSc1	bilance
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
je	být	k5eAaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
pasivní	pasivní	k2eAgInSc1d1	pasivní
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
měl	mít	k5eAaImAgInS	mít
export	export	k1gInSc1	export
hodnotu	hodnota	k1gFnSc4	hodnota
777	[number]	k4	777
miliónů	milión	k4xCgInPc2	milión
USD	USD	kA	USD
a	a	k8xC	a
import	import	k1gInSc1	import
1,541	[number]	k4	1,541
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Export	export	k1gInSc1	export
<g/>
:	:	kIx,	:
reexport	reexport	k1gInSc1	reexport
<g/>
,	,	kIx,	,
fosfáty	fosfát	k1gInPc1	fosfát
<g/>
,	,	kIx,	,
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
kakao	kakao	k1gNnSc1	kakao
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgMnPc1d1	obchodní
partneři	partner	k1gMnPc1	partner
-	-	kIx~	-
export	export	k1gInSc1	export
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Ghana	Ghana	k1gFnSc1	Ghana
<g/>
,	,	kIx,	,
Burkina	Burkina	k1gFnSc1	Burkina
<g/>
,	,	kIx,	,
SRN	SRN	kA	SRN
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Benin	Benin	k1gInSc1	Benin
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Mali	Mali	k1gNnSc1	Mali
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Import	import	k1gInSc1	import
<g/>
:	:	kIx,	:
stroje	stroj	k1gInPc1	stroj
a	a	k8xC	a
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgInPc4d1	dopravní
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
potraviny	potravina	k1gFnPc4	potravina
<g/>
,	,	kIx,	,
ropné	ropný	k2eAgInPc4d1	ropný
produkty	produkt	k1gInPc4	produkt
<g/>
,	,	kIx,	,
spotřební	spotřební	k2eAgNnSc4d1	spotřební
zboží	zboží	k1gNnSc4	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgMnPc1d1	obchodní
partneři	partner	k1gMnPc1	partner
-	-	kIx~	-
import	import	k1gInSc1	import
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Thajsko	Thajsko	k1gNnSc1	Thajsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
568	[number]	k4	568
km	km	kA	km
železnic	železnice	k1gFnPc2	železnice
využívaných	využívaný	k2eAgFnPc2d1	využívaná
pro	pro	k7c4	pro
nákladní	nákladní	k2eAgFnSc4d1	nákladní
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
asi	asi	k9	asi
9	[number]	k4	9
000	[number]	k4	000
km	km	kA	km
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
2	[number]	k4	2
500	[number]	k4	500
km	km	kA	km
se	s	k7c7	s
zpevněným	zpevněný	k2eAgInSc7d1	zpevněný
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Lomé	Lomá	k1gFnSc2	Lomá
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Přístav	přístav	k1gInSc1	přístav
v	v	k7c6	v
Lomé	Lomá	k1gFnSc6	Lomá
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgInSc7d1	velký
zdrojem	zdroj	k1gInSc7	zdroj
financí	finance	k1gFnPc2	finance
pro	pro	k7c4	pro
státní	státní	k2eAgFnSc4d1	státní
pokladnu	pokladna	k1gFnSc4	pokladna
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
slouží	sloužit	k5eAaImIp3nS	sloužit
také	také	k9	také
vnitrozemským	vnitrozemský	k2eAgInPc3d1	vnitrozemský
státům	stát	k1gInPc3	stát
(	(	kIx(	(
<g/>
Burkina	Burkina	k1gFnSc1	Burkina
<g/>
,	,	kIx,	,
Mali	Mali	k1gNnSc1	Mali
<g/>
,	,	kIx,	,
Niger	Niger	k1gInSc1	Niger
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
Togu	Togo	k1gNnSc6	Togo
se	se	k3xPyFc4	se
nevyhnul	vyhnout	k5eNaPmAgInS	vyhnout
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
boom	boom	k1gInSc1	boom
telekomunikací	telekomunikace	k1gFnPc2	telekomunikace
a	a	k8xC	a
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
stoupá	stoupat	k5eAaImIp3nS	stoupat
počet	počet	k1gInSc1	počet
uživatelů	uživatel	k1gMnPc2	uživatel
internetu	internet	k1gInSc2	internet
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
356	[number]	k4	356
300	[number]	k4	300
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prudce	prudko	k6eAd1	prudko
roste	růst	k5eAaImIp3nS	růst
podíl	podíl	k1gInSc1	podíl
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
zhruba	zhruba	k6eAd1	zhruba
3,1	[number]	k4	3,1
milionu	milion	k4xCgInSc2	milion
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
je	být	k5eAaImIp3nS	být
zaznamenáván	zaznamenáván	k2eAgInSc4d1	zaznamenáván
pokles	pokles	k1gInSc4	pokles
počtu	počet	k1gInSc2	počet
pevných	pevný	k2eAgFnPc2d1	pevná
telefonních	telefonní	k2eAgFnPc2d1	telefonní
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
rozvoj	rozvoj	k1gInSc4	rozvoj
je	být	k5eAaImIp3nS	být
stupeň	stupeň	k1gInSc1	stupeň
telefonizace	telefonizace	k1gFnSc2	telefonizace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
stále	stále	k6eAd1	stále
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
.	.	kIx.	.
</s>
