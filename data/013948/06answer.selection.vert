<s>
Paralelně	paralelně	k6eAd1
se	se	k3xPyFc4
věnoval	věnovat	k5eAaPmAgInS,k5eAaImAgInS
bezdrátovým	bezdrátový	k2eAgNnSc7d1
sítím	sítí	k1gNnSc7
(	(	kIx(
<g/>
WiFi	WiFi	k1gNnPc1
a	a	k8xC
jiné	jiný	k2eAgFnPc1d1
<g/>
)	)	kIx)
a	a	k8xC
telekomunikacím	telekomunikace	k1gFnPc3
vůbec	vůbec	k9
–	–	k?
provozuje	provozovat	k5eAaImIp3nS
oborový	oborový	k2eAgMnSc1d1
weblog	weblog	k1gMnSc1
Marigold	Marigold	k1gMnSc1
<g/>
,	,	kIx,
založil	založit	k5eAaPmAgMnS
také	také	k9
dvě	dva	k4xCgFnPc4
další	další	k2eAgFnPc4d1
internetové	internetový	k2eAgFnPc4d1
firmy	firma	k1gFnPc4
<g/>
:	:	kIx,
Cinetik	Cinetik	k1gMnSc1
<g/>
,	,	kIx,
zabývající	zabývající	k2eAgMnSc1d1
se	se	k3xPyFc4
půjčováním	půjčování	k1gNnSc7
DVD	DVD	kA
po	po	k7c6
Internetu	Internet	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
Pipeline	Pipelin	k1gInSc5
(	(	kIx(
<g/>
poskytuje	poskytovat	k5eAaImIp3nS
transport	transport	k1gInSc1
SMS	SMS	kA
zpráv	zpráva	k1gFnPc2
mezi	mezi	k7c7
Internetem	Internet	k1gInSc7
a	a	k8xC
sítěmi	síť	k1gFnPc7
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
