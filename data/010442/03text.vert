<p>
<s>
Fourierova	Fourierův	k2eAgFnSc1d1	Fourierova
transformace	transformace	k1gFnSc1	transformace
je	být	k5eAaImIp3nS	být
integrální	integrální	k2eAgFnSc1d1	integrální
transformace	transformace	k1gFnSc1	transformace
převádějící	převádějící	k2eAgFnSc1d1	převádějící
signál	signál	k1gInSc4	signál
mezi	mezi	k7c4	mezi
časově	časově	k6eAd1	časově
a	a	k8xC	a
frekvenčně	frekvenčně	k6eAd1	frekvenčně
závislým	závislý	k2eAgNnSc7d1	závislé
vyjádřením	vyjádření	k1gNnSc7	vyjádření
pomocí	pomocí	k7c2	pomocí
harmonických	harmonický	k2eAgInPc2d1	harmonický
signálů	signál	k1gInPc2	signál
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
funkcí	funkce	k1gFnSc7	funkce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sin	sin	kA	sin
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
cos	cos	kA	cos
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
tedy	tedy	k9	tedy
funkcí	funkce	k1gFnSc7	funkce
komplexní	komplexní	k2eAgInPc1d1	komplexní
exponenciály	exponenciál	k1gInPc1	exponenciál
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
převod	převod	k1gInSc4	převod
signálů	signál	k1gInPc2	signál
z	z	k7c2	z
časové	časový	k2eAgFnSc2d1	časová
oblasti	oblast	k1gFnSc2	oblast
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
frekvenční	frekvenční	k2eAgFnSc2d1	frekvenční
<g/>
.	.	kIx.	.
</s>
<s>
Signál	signál	k1gInSc1	signál
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
ve	v	k7c6	v
spojitém	spojitý	k2eAgInSc6d1	spojitý
či	či	k8xC	či
diskrétním	diskrétní	k2eAgInSc6d1	diskrétní
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Spojitý	spojitý	k2eAgInSc4d1	spojitý
čas	čas	k1gInSc4	čas
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Definice	definice	k1gFnSc2	definice
===	===	k?	===
</s>
</p>
<p>
<s>
Fourierova	Fourierův	k2eAgFnSc1d1	Fourierova
transformace	transformace	k1gFnSc1	transformace
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
funkce	funkce	k1gFnSc1	funkce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
integrálním	integrální	k2eAgInSc7d1	integrální
vztahem	vztah	k1gInSc7	vztah
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
∫	∫	k?	∫
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
}	}	kIx)	}
<g/>
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Funkci	funkce	k1gFnSc4	funkce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
vypočteme	vypočíst	k5eAaPmIp1nP	vypočíst
z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
inverzní	inverzní	k2eAgFnSc7d1	inverzní
Fourierovou	Fourierův	k2eAgFnSc7d1	Fourierova
transformací	transformace	k1gFnSc7	transformace
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∫	∫	k?	∫
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
}	}	kIx)	}
<g/>
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
}	}	kIx)	}
</s>
</p>
<p>
<s>
Nevlastní	vlastní	k2eNgInPc1d1	nevlastní
integrály	integrál	k1gInPc1	integrál
chápeme	chápat	k5eAaImIp1nP	chápat
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
Cauchyovy	Cauchyův	k2eAgFnSc2d1	Cauchyova
hlavní	hlavní	k2eAgFnSc2d1	hlavní
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∫	∫	k?	∫
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
lim	limo	k1gNnPc2	limo
</s>
</p>
<p>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∫	∫	k?	∫
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
[	[	kIx(	[
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc4	inft
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
-T	-T	k?	-T
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}	}	kIx)	}
<g/>
[	[	kIx(	[
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Dvojice	dvojice	k1gFnSc1	dvojice
ve	v	k7c6	v
Fourierově	Fourierův	k2eAgFnSc6d1	Fourierova
transformaci	transformace	k1gFnSc6	transformace
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
originál	originál	k1gMnSc1	originál
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
a	a	k8xC	a
obraz	obraz	k1gInSc4	obraz
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
originálem	originál	k1gInSc7	originál
a	a	k8xC	a
obrazem	obraz	k1gInSc7	obraz
vyjadřujeme	vyjadřovat	k5eAaImIp1nP	vyjadřovat
zápisem	zápis	k1gInSc7	zápis
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
[	[	kIx(	[
<g/>
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
[	[	kIx(	[
<g/>
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
<g/>
V	v	k7c6	v
technické	technický	k2eAgFnSc6d1	technická
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
omega	omega	k1gNnPc3	omega
}	}	kIx)	}
</s>
</p>
<p>
<s>
úhlová	úhlový	k2eAgFnSc1d1	úhlová
frekvence	frekvence	k1gFnSc1	frekvence
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
představuje	představovat	k5eAaImIp3nS	představovat
spektrum	spektrum	k1gNnSc4	spektrum
signálu	signál	k1gInSc2	signál
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Spektrum	spektrum	k1gNnSc1	spektrum
je	být	k5eAaImIp3nS	být
komplexní	komplexní	k2eAgFnSc1d1	komplexní
veličina	veličina	k1gFnSc1	veličina
a	a	k8xC	a
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
|	|	kIx~	|
<g/>
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
arg	arg	k?	arg
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	leftum	k1gNnPc2	leftum
<g/>
|	|	kIx~	|
<g/>
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
nazýváme	nazývat	k5eAaImIp1nP	nazývat
amplitudové	amplitudový	k2eAgNnSc4d1	amplitudové
spektrum	spektrum	k1gNnSc4	spektrum
a	a	k8xC	a
úhel	úhel	k1gInSc4	úhel
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
arg	arg	k?	arg
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
arg	arg	k?	arg
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
fázové	fázový	k2eAgNnSc1d1	fázové
spektrum	spektrum	k1gNnSc1	spektrum
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlastnosti	vlastnost	k1gFnPc4	vlastnost
Fourierovy	Fourierův	k2eAgFnSc2d1	Fourierova
transformace	transformace	k1gFnSc2	transformace
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Věta	věta	k1gFnSc1	věta
o	o	k7c6	o
linearitě	linearita	k1gFnSc6	linearita
====	====	k?	====
</s>
</p>
<p>
<s>
Lineární	lineární	k2eAgFnSc4d1	lineární
kombinaci	kombinace	k1gFnSc4	kombinace
signálů	signál	k1gInPc2	signál
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
lineární	lineární	k2eAgFnSc1d1	lineární
kombinace	kombinace	k1gFnSc1	kombinace
jejich	jejich	k3xOp3gNnPc2	jejich
spekter	spektrum	k1gNnPc2	spektrum
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
[	[	kIx(	[
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
]	]	kIx)	]
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
[	[	kIx(	[
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Věta	věta	k1gFnSc1	věta
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
měřítka	měřítko	k1gNnSc2	měřítko
(	(	kIx(	(
<g/>
Podobnost	podobnost	k1gFnSc1	podobnost
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
signál	signál	k1gInSc1	signál
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
spektrum	spektrum	k1gNnSc1	spektrum
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
signál	signál	k1gInSc1	signál
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
≠	≠	k?	≠
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
at	at	k?	at
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
spektrum	spektrum	k1gNnSc1	spektrum
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
|	|	kIx~	|
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
right	right	k2eAgMnSc1d1	right
<g/>
|	|	kIx~	|
<g/>
}}	}}	k?	}}
<g/>
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
<g/>
Tedy	tedy	k9	tedy
rozšíření	rozšíření	k1gNnSc1	rozšíření
signálu	signál	k1gInSc2	signál
v	v	k7c6	v
časové	časový	k2eAgFnSc6d1	časová
oblasti	oblast	k1gFnSc6	oblast
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
zúžení	zúžení	k1gNnSc1	zúžení
spektra	spektrum	k1gNnSc2	spektrum
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Posun	posun	k1gInSc4	posun
signálu	signál	k1gInSc2	signál
v	v	k7c6	v
čase	čas	k1gInSc6	čas
(	(	kIx(	(
<g/>
Posunutí	posunutí	k1gNnSc2	posunutí
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
signál	signál	k1gInSc1	signál
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
spektrum	spektrum	k1gNnSc1	spektrum
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
Ω	Ω	k?	Ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
signál	signál	k1gInSc1	signál
posunutý	posunutý	k2eAgInSc1d1	posunutý
o	o	k7c4	o
veličinu	veličina	k1gFnSc4	veličina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
spektrum	spektrum	k1gNnSc1	spektrum
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ω	Ω	k?	Ω
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
Ω	Ω	k?	Ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
[	[	kIx(	[
<g/>
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
k-a	k	k?	k-a
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gNnPc1	omega
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Amplitudové	amplitudový	k2eAgNnSc1d1	amplitudové
spektrum	spektrum	k1gNnSc1	spektrum
posunutého	posunutý	k2eAgInSc2d1	posunutý
signálu	signál	k1gInSc2	signál
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
,	,	kIx,	,
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
jen	jen	k9	jen
fázové	fázový	k2eAgNnSc4d1	fázové
spektrum	spektrum	k1gNnSc4	spektrum
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
úměrně	úměrně	k6eAd1	úměrně
zpoždění	zpoždění	k1gNnSc1	zpoždění
a	a	k8xC	a
kmitočtu	kmitočet	k1gInSc2	kmitočet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
věty	věta	k1gFnSc2	věta
o	o	k7c4	o
translaci	translace	k1gFnSc4	translace
v	v	k7c6	v
Laplaceově	Laplaceův	k2eAgFnSc6d1	Laplaceova
transformaci	transformace	k1gFnSc6	transformace
platí	platit	k5eAaImIp3nS	platit
věta	věta	k1gFnSc1	věta
pro	pro	k7c4	pro
libovolné	libovolný	k2eAgNnSc4d1	libovolné
a	a	k8xC	a
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
i	i	k9	i
pro	pro	k7c4	pro
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
<	<	kIx(	<
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
<	<	kIx(	<
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
===	===	k?	===
Spektrum	spektrum	k1gNnSc1	spektrum
reálného	reálný	k2eAgInSc2d1	reálný
signálu	signál	k1gInSc2	signál
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
signál	signál	k1gInSc4	signál
reálný	reálný	k2eAgInSc4d1	reálný
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
spektrum	spektrum	k1gNnSc4	spektrum
platí	platit	k5eAaImIp3nP	platit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
amplitudové	amplitudový	k2eAgNnSc1d1	amplitudové
spektrum	spektrum	k1gNnSc1	spektrum
je	být	k5eAaImIp3nS	být
sudou	sudý	k2eAgFnSc7d1	sudá
funkcí	funkce	k1gFnSc7	funkce
</s>
</p>
<p>
<s>
fázové	fázový	k2eAgNnSc1d1	fázové
spektrum	spektrum	k1gNnSc1	spektrum
je	být	k5eAaImIp3nS	být
lichou	lichý	k2eAgFnSc7d1	lichá
funkcí	funkce	k1gFnSc7	funkce
</s>
</p>
<p>
<s>
spektrum	spektrum	k1gNnSc1	spektrum
sudého	sudý	k2eAgInSc2d1	sudý
signálu	signál	k1gInSc2	signál
je	být	k5eAaImIp3nS	být
sudou	sudý	k2eAgFnSc7d1	sudá
reálnou	reálný	k2eAgFnSc7d1	reálná
funkcí	funkce	k1gFnSc7	funkce
</s>
</p>
<p>
<s>
spektrum	spektrum	k1gNnSc1	spektrum
lichého	lichý	k2eAgInSc2d1	lichý
signálu	signál	k1gInSc2	signál
je	být	k5eAaImIp3nS	být
lichou	lichý	k2eAgFnSc4d1	lichá
ryze	ryze	k6eAd1	ryze
imaginární	imaginární	k2eAgFnSc4d1	imaginární
funkcí	funkce	k1gFnSc7	funkce
</s>
</p>
<p>
<s>
==	==	k?	==
Diskrétní	diskrétní	k2eAgInSc4d1	diskrétní
čas	čas	k1gInSc4	čas
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Definice	definice	k1gFnSc2	definice
===	===	k?	===
</s>
</p>
<p>
<s>
Fourierova	Fourierův	k2eAgFnSc1d1	Fourierova
transformace	transformace	k1gFnSc1	transformace
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
Ω	Ω	k?	Ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
posloupnosti	posloupnost	k1gFnPc1	posloupnost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
vztahem	vztah	k1gInSc7	vztah
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
Ω	Ω	k?	Ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ω	Ω	k?	Ω
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
}	}	kIx)	}
<g/>
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
k	k	k7c3	k
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Posloupnost	posloupnost	k1gFnSc1	posloupnost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
vypočteme	vypočíst	k5eAaPmIp1nP	vypočíst
z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
Ω	Ω	k?	Ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
inverzní	inverzní	k2eAgFnSc7d1	inverzní
Fourierovou	Fourierův	k2eAgFnSc7d1	Fourierova
transformací	transformace	k1gFnSc7	transformace
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∫	∫	k?	∫
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
Ω	Ω	k?	Ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ω	Ω	k?	Ω
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ω	Ω	k?	Ω
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}	}	kIx)	}
<g/>
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
}	}	kIx)	}
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
označují	označovat	k5eAaImIp3nP	označovat
tuto	tento	k3xDgFnSc4	tento
transformaci	transformace	k1gFnSc4	transformace
DtFT	DtFT	k1gFnSc2	DtFT
(	(	kIx(	(
<g/>
discrete-time	discreteimat	k5eAaPmIp3nS	discrete-timat
Fourier	Fourier	k1gMnSc1	Fourier
transformation	transformation	k1gInSc1	transformation
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ji	on	k3xPp3gFnSc4	on
odlišili	odlišit	k5eAaPmAgMnP	odlišit
od	od	k7c2	od
Fourierovy	Fourierův	k2eAgFnSc2d1	Fourierova
transformace	transformace	k1gFnSc2	transformace
spojitého	spojitý	k2eAgInSc2d1	spojitý
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
nebudeme	být	k5eNaImBp1nP	být
značením	značení	k1gNnSc7	značení
nijak	nijak	k6eAd1	nijak
odlišovat	odlišovat	k5eAaImF	odlišovat
Fourierovu	Fourierův	k2eAgFnSc4d1	Fourierova
transformaci	transformace	k1gFnSc4	transformace
spojitého	spojitý	k2eAgInSc2d1	spojitý
a	a	k8xC	a
diskrétního	diskrétní	k2eAgInSc2d1	diskrétní
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
signálem	signál	k1gInSc7	signál
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
spektrem	spektrum	k1gNnSc7	spektrum
budeme	být	k5eAaImBp1nP	být
tedy	tedy	k9	tedy
značit	značit	k5eAaImF	značit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
Ω	Ω	k?	Ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
[	[	kIx(	[
<g/>
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
Ω	Ω	k?	Ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
[	[	kIx(	[
<g/>
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
<g/>
Spektrum	spektrum	k1gNnSc1	spektrum
diskrétního	diskrétní	k2eAgInSc2d1	diskrétní
signálu	signál	k1gInSc2	signál
se	se	k3xPyFc4	se
od	od	k7c2	od
spektra	spektrum	k1gNnSc2	spektrum
spojitého	spojitý	k2eAgInSc2d1	spojitý
signálu	signál	k1gInSc2	signál
liší	lišit	k5eAaImIp3nP	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
periodické	periodický	k2eAgNnSc1d1	periodické
s	s	k7c7	s
periodou	perioda	k1gFnSc7	perioda
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
==	==	k?	==
Diskrétní	diskrétní	k2eAgFnSc1d1	diskrétní
Fourierova	Fourierův	k2eAgFnSc1d1	Fourierova
transformace	transformace	k1gFnSc1	transformace
==	==	k?	==
</s>
</p>
<p>
<s>
Definiční	definiční	k2eAgInPc1d1	definiční
vztahy	vztah	k1gInPc1	vztah
Fourierovy	Fourierův	k2eAgFnSc2d1	Fourierova
transformace	transformace	k1gFnSc2	transformace
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
znalost	znalost	k1gFnSc4	znalost
matematického	matematický	k2eAgNnSc2d1	matematické
vyjádření	vyjádření	k1gNnSc2	vyjádření
signálu	signál	k1gInSc2	signál
či	či	k8xC	či
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
však	však	k9	však
zpracováváme	zpracovávat	k5eAaImIp1nP	zpracovávat
naměřené	naměřený	k2eAgFnPc4d1	naměřená
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
známe	znát	k5eAaImIp1nP	znát
vzorky	vzorek	k1gInPc1	vzorek
signálu	signál	k1gInSc2	signál
či	či	k8xC	či
spektra	spektrum	k1gNnSc2	spektrum
z	z	k7c2	z
konečného	konečný	k2eAgInSc2d1	konečný
intervalu	interval	k1gInSc2	interval
<g/>
,	,	kIx,	,
stojíme	stát	k5eAaImIp1nP	stát
před	před	k7c7	před
problémem	problém	k1gInSc7	problém
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
určit	určit	k5eAaPmF	určit
spektrum	spektrum	k1gNnSc4	spektrum
z	z	k7c2	z
vzorků	vzorek	k1gInPc2	vzorek
signálu	signál	k1gInSc2	signál
či	či	k8xC	či
signál	signál	k1gInSc4	signál
ze	z	k7c2	z
vzorků	vzorek	k1gInPc2	vzorek
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
účelu	účel	k1gInSc3	účel
používáme	používat	k5eAaImIp1nP	používat
numerické	numerický	k2eAgFnSc2d1	numerická
metody	metoda	k1gFnSc2	metoda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xC	jako
diskrétní	diskrétní	k2eAgFnSc1d1	diskrétní
Fourierova	Fourierův	k2eAgFnSc1d1	Fourierova
transformace	transformace	k1gFnSc1	transformace
(	(	kIx(	(
<g/>
DFT	DFT	kA	DFT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Diskrétní	diskrétní	k2eAgFnSc1d1	diskrétní
Fourierova	Fourierův	k2eAgFnSc1d1	Fourierova
transformace	transformace	k1gFnSc1	transformace
mezi	mezi	k7c7	mezi
posloupnostmi	posloupnost	k1gFnPc7	posloupnost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
N-	N-	k1gFnSc1	N-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
</s>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
D	D	kA	D
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
N-	N-	k1gFnSc1	N-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
vztahy	vztah	k1gInPc7	vztah
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
přímá	přímý	k2eAgFnSc1d1	přímá
diskrétní	diskrétní	k2eAgFnSc1d1	diskrétní
Fourierova	Fourierův	k2eAgFnSc1d1	Fourierova
transformace	transformace	k1gFnSc1	transformace
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
D	D	kA	D
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
N-	N-	k1gFnSc1	N-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
d	d	k?	d
<g/>
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
nk	nk	k?	nk
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
/	/	kIx~	/
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
<g/>
N-	N-	k1gFnSc1	N-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
zpětná	zpětný	k2eAgFnSc1d1	zpětná
(	(	kIx(	(
<g/>
inverzní	inverzní	k2eAgFnSc1d1	inverzní
<g/>
)	)	kIx)	)
diskrétní	diskrétní	k2eAgFnSc1d1	diskrétní
Fourierova	Fourierův	k2eAgFnSc1d1	Fourierova
transformace	transformace	k1gFnSc1	transformace
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
d	d	k?	d
<g/>
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
N-	N-	k?	N-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
D	D	kA	D
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
nk	nk	k?	nk
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
/	/	kIx~	/
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
k	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
<g/>
N-	N-	k1gFnSc1	N-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Diskrétní	diskrétní	k2eAgFnSc1d1	diskrétní
Fourierova	Fourierův	k2eAgFnSc1d1	Fourierova
transformace	transformace	k1gFnSc1	transformace
našla	najít	k5eAaPmAgFnS	najít
velké	velký	k2eAgNnSc4d1	velké
uplatnění	uplatnění	k1gNnSc4	uplatnění
zejména	zejména	k9	zejména
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
řady	řada	k1gFnSc2	řada
přístrojů	přístroj	k1gInPc2	přístroj
jsou	být	k5eAaImIp3nP	být
jednoúčelové	jednoúčelový	k2eAgInPc1d1	jednoúčelový
procesory	procesor	k1gInPc1	procesor
realizující	realizující	k2eAgFnSc4d1	realizující
tuto	tento	k3xDgFnSc4	tento
transformaci	transformace	k1gFnSc4	transformace
<g/>
.	.	kIx.	.
</s>
<s>
Výpočet	výpočet	k1gInSc1	výpočet
DFT	DFT	kA	DFT
podle	podle	k7c2	podle
definičního	definiční	k2eAgInSc2d1	definiční
vztahu	vztah	k1gInSc2	vztah
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
komplexních	komplexní	k2eAgInPc2d1	komplexní
součinů	součin	k1gInPc2	součin
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
komplexních	komplexní	k2eAgInPc2d1	komplexní
součtů	součet	k1gInPc2	součet
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
množství	množství	k1gNnSc1	množství
operací	operace	k1gFnPc2	operace
výrazně	výrazně	k6eAd1	výrazně
snižovalo	snižovat	k5eAaImAgNnS	snižovat
možnost	možnost	k1gFnSc4	možnost
aplikace	aplikace	k1gFnSc2	aplikace
DFT	DFT	kA	DFT
na	na	k7c4	na
výpočty	výpočet	k1gInPc4	výpočet
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
J.	J.	kA	J.
W.	W.	kA	W.
Cooley	Coolea	k1gFnSc2	Coolea
a	a	k8xC	a
J.	J.	kA	J.
W.	W.	kA	W.
Tukey	Tukea	k1gMnSc2	Tukea
popsali	popsat	k5eAaPmAgMnP	popsat
velmi	velmi	k6eAd1	velmi
efektivní	efektivní	k2eAgInSc4d1	efektivní
algoritmus	algoritmus	k1gInSc4	algoritmus
výpočtu	výpočet	k1gInSc2	výpočet
DFT	DFT	kA	DFT
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
rychlou	rychlý	k2eAgFnSc4d1	rychlá
Fourierovu	Fourierův	k2eAgFnSc4d1	Fourierova
transformaci	transformace	k1gFnSc4	transformace
(	(	kIx(	(
<g/>
FFT	fft	k0	fft
–	–	k?	–
Fast	Fast	k2eAgInSc1d1	Fast
Fourier	Fourier	k1gInSc1	Fourier
Transform	Transform	k1gInSc1	Transform
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
jen	jen	k9	jen
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
log	log	kA	log
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
komplexních	komplexní	k2eAgInPc2d1	komplexní
součinů	součin	k1gInPc2	součin
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
log	log	kA	log
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
\	\	kIx~	\
<g/>
log	log	kA	log
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
komplexních	komplexní	k2eAgInPc2d1	komplexní
součtů	součet	k1gInPc2	součet
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
algoritmu	algoritmus	k1gInSc3	algoritmus
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
diskrétní	diskrétní	k2eAgFnSc1d1	diskrétní
Fourierova	Fourierův	k2eAgFnSc1d1	Fourierova
transformace	transformace	k1gFnSc2	transformace
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
prostředkem	prostředek	k1gInSc7	prostředek
pro	pro	k7c4	pro
numerický	numerický	k2eAgInSc4d1	numerický
výpočet	výpočet	k1gInSc4	výpočet
Fourierovy	Fourierův	k2eAgFnSc2d1	Fourierova
transformace	transformace	k1gFnSc2	transformace
<g/>
.	.	kIx.	.
</s>
<s>
Algoritmus	algoritmus	k1gInSc1	algoritmus
FFT	fft	k0	fft
je	být	k5eAaImIp3nS	být
také	také	k9	také
implementován	implementovat	k5eAaImNgInS	implementovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
nejrozšířenějších	rozšířený	k2eAgInPc6d3	nejrozšířenější
matematických	matematický	k2eAgInPc6d1	matematický
programech	program	k1gInPc6	program
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
GNU	gnu	k1gMnSc1	gnu
Octave	Octav	k1gInSc5	Octav
<g/>
,	,	kIx,	,
Mathcad	Mathcad	k1gInSc1	Mathcad
<g/>
,	,	kIx,	,
Mathematica	Mathematica	k1gFnSc1	Mathematica
<g/>
,	,	kIx,	,
Maple	Maple	k1gFnSc1	Maple
<g/>
,	,	kIx,	,
Matlab	Matlab	k1gInSc1	Matlab
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Zpětná	zpětný	k2eAgFnSc1d1	zpětná
Fourierova	Fourierův	k2eAgFnSc1d1	Fourierova
transformace	transformace	k1gFnSc1	transformace
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∫	∫	k?	∫
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
^	^	kIx~	^
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
}	}	kIx)	}
</s>
</p>
<p>
<s>
Integrál	integrál	k1gInSc1	integrál
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
chápat	chápat	k5eAaImF	chápat
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
hlavní	hlavní	k2eAgFnSc2d1	hlavní
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úpravách	úprava	k1gFnPc6	úprava
popisuje	popisovat	k5eAaImIp3nS	popisovat
rozložení	rozložení	k1gNnSc1	rozložení
funkce	funkce	k1gFnSc2	funkce
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
f	f	k?	f
<g/>
∈	∈	k?	∈
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
∞	∞	k?	∞
<g/>
,	,	kIx,	,
<g/>
∞	∞	k?	∞
<g/>
)	)	kIx)	)
na	na	k7c4	na
harmonické	harmonický	k2eAgInPc4d1	harmonický
kmity	kmit	k1gInPc4	kmit
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
uhlová	uhlový	k2eAgFnSc1d1	Uhlová
frekvence	frekvence	k1gFnSc1	frekvence
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
od	od	k7c2	od
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
do	do	k7c2	do
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
infty	inft	k1gInPc4	inft
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Fourierova	Fourierův	k2eAgFnSc1d1	Fourierova
řada	řada	k1gFnSc1	řada
</s>
</p>
<p>
<s>
Diskrétní	diskrétní	k2eAgFnSc1d1	diskrétní
kosinová	kosinový	k2eAgFnSc1d1	kosinová
transformace	transformace	k1gFnSc1	transformace
</s>
</p>
<p>
<s>
Rychlá	rychlý	k2eAgFnSc1d1	rychlá
Fourierova	Fourierův	k2eAgFnSc1d1	Fourierova
transformace	transformace	k1gFnSc1	transformace
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Hlaváč	Hlaváč	k1gMnSc1	Hlaváč
<g/>
:	:	kIx,	:
LINEÁRNÍ	lineární	k2eAgFnSc1d1	lineární
INTEGRÁLNÍ	integrální	k2eAgFnSc1d1	integrální
TRANSFORMACE	transformace	k1gFnSc1	transformace
-	-	kIx~	-
Fourierova	Fourierův	k2eAgFnSc1d1	Fourierova
transformace	transformace	k1gFnSc1	transformace
<g/>
,	,	kIx,	,
Fakulta	fakulta	k1gFnSc1	fakulta
elektrotechnická	elektrotechnický	k2eAgFnSc1d1	elektrotechnická
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
katedra	katedra	k1gFnSc1	katedra
kybernetiky	kybernetika	k1gFnSc2	kybernetika
<g/>
,	,	kIx,	,
Centrum	centrum	k1gNnSc1	centrum
strojového	strojový	k2eAgNnSc2d1	strojové
vnímání	vnímání	k1gNnSc2	vnímání
</s>
</p>
<p>
<s>
FOURIEROVA	FOURIEROVA	kA	FOURIEROVA
TRANSFORMACE	transformace	k1gFnSc2	transformace
M.	M.	kA	M.
Hušek	Hušek	k1gMnSc1	Hušek
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Pyrih	Pyrih	k1gInSc1	Pyrih
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Matematicko-fyzikální	matematickoyzikální	k2eAgFnSc1d1	matematicko-fyzikální
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Algovision	Algovision	k1gInSc1	Algovision
-	-	kIx~	-
sada	sada	k1gFnSc1	sada
Java	Javum	k1gNnSc2	Javum
appletů	applet	k1gInPc2	applet
s	s	k7c7	s
vizualizací	vizualizace	k1gFnSc7	vizualizace
datových	datový	k2eAgFnPc2d1	datová
struktur	struktura	k1gFnPc2	struktura
a	a	k8xC	a
práce	práce	k1gFnSc2	práce
algoritmů	algoritmus	k1gInPc2	algoritmus
</s>
</p>
<p>
<s>
Fourierova	Fourierův	k2eAgFnSc1d1	Fourierova
transformace	transformace	k1gFnSc1	transformace
-	-	kIx~	-
názorné	názorný	k2eAgNnSc1d1	názorné
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
</s>
</p>
<p>
<s>
Fourier	Fourier	k1gInSc1	Fourier
Series	Series	k1gInSc1	Series
3D	[number]	k4	3D
-	-	kIx~	-
interaktivní	interaktivní	k2eAgFnSc2d1	interaktivní
demonstrace	demonstrace	k1gFnSc2	demonstrace
principu	princip	k1gInSc2	princip
Fourierových	Fourierův	k2eAgFnPc2d1	Fourierova
řad	řada	k1gFnPc2	řada
HTML5	HTML5	k1gFnSc2	HTML5
a	a	k8xC	a
JavaScript	JavaScript	k1gInSc4	JavaScript
<g/>
:	:	kIx,	:
Unikátní	unikátní	k2eAgFnSc4d1	unikátní
interaktivní	interaktivní	k2eAgNnSc1d1	interaktivní
3D	[number]	k4	3D
zobrazení	zobrazení	k1gNnSc1	zobrazení
propojující	propojující	k2eAgInPc1d1	propojující
časovou	časový	k2eAgFnSc4d1	časová
<g/>
,	,	kIx,	,
frekvenční	frekvenční	k2eAgFnSc4d1	frekvenční
<g/>
,	,	kIx,	,
amplitudovou	amplitudový	k2eAgFnSc4d1	amplitudová
a	a	k8xC	a
fázovou	fázový	k2eAgFnSc4d1	fázová
osu	osa	k1gFnSc4	osa
<g/>
.	.	kIx.	.
</s>
</p>
