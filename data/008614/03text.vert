<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
medicinae	medicinae	k1gInSc1	medicinae
dentium	dentium	k1gNnSc1	dentium
doctor	doctor	k1gInSc1	doctor
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
MDDr	MDDr	k1gMnSc1	MDDr
<g/>
.	.	kIx.	.
psaná	psaný	k2eAgFnSc1d1	psaná
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
udělovaný	udělovaný	k2eAgInSc4d1	udělovaný
absolventům	absolvent	k1gMnPc3	absolvent
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
v	v	k7c6	v
magisterském	magisterský	k2eAgInSc6d1	magisterský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
udělován	udělován	k2eAgInSc4d1	udělován
po	po	k7c6	po
pětiletém	pětiletý	k2eAgNnSc6d1	pětileté
studiu	studio	k1gNnSc6	studio
oboru	obor	k1gInSc2	obor
zubní	zubní	k2eAgNnSc4d1	zubní
lékařství	lékařství	k1gNnSc4	lékařství
na	na	k7c6	na
lékařské	lékařský	k2eAgFnSc6d1	lékařská
fakultě	fakulta	k1gFnSc6	fakulta
a	a	k8xC	a
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
studia	studio	k1gNnSc2	studio
příslušnou	příslušný	k2eAgFnSc7d1	příslušná
státní	státní	k2eAgFnSc7d1	státní
rigorózní	rigorózní	k2eAgFnSc7d1	rigorózní
zkouškou	zkouška	k1gFnSc7	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
získání	získání	k1gNnSc1	získání
ekvivalentního	ekvivalentní	k2eAgInSc2d1	ekvivalentní
titulu	titul	k1gInSc2	titul
nezbytnou	nezbytný	k2eAgFnSc7d1	nezbytná
podmínkou	podmínka	k1gFnSc7	podmínka
k	k	k7c3	k
umožnění	umožnění	k1gNnSc3	umožnění
vykonávání	vykonávání	k1gNnSc2	vykonávání
povolání	povolání	k1gNnSc2	povolání
zubního	zubní	k2eAgMnSc4d1	zubní
lékaře	lékař	k1gMnSc4	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
profesní	profesní	k2eAgInSc4d1	profesní
doktorát	doktorát	k1gInSc4	doktorát
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
o	o	k7c4	o
titul	titul	k1gInSc4	titul
spjatý	spjatý	k2eAgInSc4d1	spjatý
s	s	k7c7	s
výkonem	výkon	k1gInSc7	výkon
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
profese	profes	k1gFnSc2	profes
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dosažený	dosažený	k2eAgInSc1d1	dosažený
stupeň	stupeň	k1gInSc1	stupeň
vzdělání	vzdělání	k1gNnSc2	vzdělání
dle	dle	k7c2	dle
ISCED	ISCED	kA	ISCED
je	být	k5eAaImIp3nS	být
7	[number]	k4	7
(	(	kIx(	(
<g/>
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
,	,	kIx,	,
magisterská	magisterský	k2eAgFnSc1d1	magisterská
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
slovním	slovní	k2eAgNnSc7d1	slovní
vyjádřením	vyjádření	k1gNnSc7	vyjádření
zkratky	zkratka	k1gFnSc2	zkratka
titulu	titul	k1gInSc2	titul
MDDr	MDDr	k1gInSc1	MDDr
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
"	"	kIx"	"
<g/>
obligatorní	obligatorní	k2eAgInSc4d1	obligatorní
<g/>
"	"	kIx"	"
doktorát	doktorát	k1gInSc4	doktorát
(	(	kIx(	(
<g/>
blíže	blízce	k6eAd2	blízce
<g/>
:	:	kIx,	:
malý	malý	k2eAgInSc1d1	malý
doktorát	doktorát	k1gInSc1	doktorát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
řádné	řádný	k2eAgNnSc4d1	řádné
zakončení	zakončení	k1gNnSc4	zakončení
studia	studio	k1gNnSc2	studio
rigorózní	rigorózní	k2eAgFnSc7d1	rigorózní
zkouškou	zkouška	k1gFnSc7	zkouška
(	(	kIx(	(
<g/>
rigo	rigo	k6eAd1	rigo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
o	o	k7c4	o
dodatečnou	dodatečný	k2eAgFnSc4d1	dodatečná
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
toho	ten	k3xDgInSc2	ten
i	i	k8xC	i
pro	pro	k7c4	pro
zubaře	zubař	k1gMnPc4	zubař
užívalo	užívat	k5eAaImAgNnS	užívat
titulu	titul	k1gInSc2	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
medicíny	medicína	k1gFnSc2	medicína
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
MUDr.	MUDr.	kA	MUDr.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
změna	změna	k1gFnSc1	změna
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
harmonizace	harmonizace	k1gFnSc2	harmonizace
výuky	výuka	k1gFnSc2	výuka
s	s	k7c7	s
EU	EU	kA	EU
a	a	k8xC	a
vnáší	vnášet	k5eAaImIp3nS	vnášet
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
více	hodně	k6eAd2	hodně
speciálních	speciální	k2eAgInPc2d1	speciální
praktických	praktický	k2eAgInPc2d1	praktický
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
evropskými	evropský	k2eAgInPc7d1	evropský
standardy	standard	k1gInPc7	standard
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
absolventům	absolvent	k1gMnPc3	absolvent
oboru	obor	k1gInSc2	obor
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
udělován	udělován	k2eAgInSc4d1	udělován
titul	titul	k1gInSc4	titul
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
MDD	MDD	kA	MDD
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Doctor	Doctor	k1gMnSc1	Doctor
of	of	k?	of
Dental	Dental	k1gMnSc1	Dental
Medicine	Medicin	k1gInSc5	Medicin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
celistvé	celistvý	k2eAgInPc4d1	celistvý
prezenční	prezenční	k2eAgInPc4d1	prezenční
(	(	kIx(	(
<g/>
ne	ne	k9	ne
6	[number]	k4	6
<g/>
leté	letý	k2eAgFnSc2d1	letá
<g/>
)	)	kIx)	)
magisterské	magisterský	k2eAgNnSc1d1	magisterské
studium	studium	k1gNnSc1	studium
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
nikoliv	nikoliv	k9	nikoliv
rozdělené	rozdělený	k2eAgInPc1d1	rozdělený
na	na	k7c4	na
bakalářský	bakalářský	k2eAgInSc4d1	bakalářský
a	a	k8xC	a
na	na	k7c4	na
navazující	navazující	k2eAgInSc4d1	navazující
magisterský	magisterský	k2eAgInSc4d1	magisterský
studijní	studijní	k2eAgInSc4d1	studijní
program	program	k1gInSc4	program
<g/>
)	)	kIx)	)
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
a	a	k8xC	a
řádně	řádně	k6eAd1	řádně
se	se	k3xPyFc4	se
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
rigorózní	rigorózní	k2eAgFnSc7d1	rigorózní
zkouškou	zkouška	k1gFnSc7	zkouška
(	(	kIx(	(
<g/>
součástí	součást	k1gFnSc7	součást
není	být	k5eNaImIp3nS	být
povinná	povinný	k2eAgFnSc1d1	povinná
obhajoba	obhajoba	k1gFnSc1	obhajoba
rigorózní	rigorózní	k2eAgFnSc2d1	rigorózní
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Udělování	udělování	k1gNnSc1	udělování
titulu	titul	k1gInSc2	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
řídí	řídit	k5eAaImIp3nS	řídit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
111	[number]	k4	111
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
i	i	k9	i
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
absolvent	absolvent	k1gMnSc1	absolvent
stomatologie	stomatologie	k1gFnSc2	stomatologie
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
případně	případně	k6eAd1	případně
možnost	možnost	k1gFnSc4	možnost
i	i	k9	i
dále	daleko	k6eAd2	daleko
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
další	další	k2eAgFnSc1d1	další
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
roky	rok	k1gInPc4	rok
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
"	"	kIx"	"
<g/>
postgraduální	postgraduální	k2eAgNnSc1d1	postgraduální
studium	studium	k1gNnSc1	studium
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
<g />
.	.	kIx.	.
</s>
<s>
titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
za	za	k7c7	za
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
doctor	doctor	k1gInSc1	doctor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
,	,	kIx,	,
doktorská	doktorský	k2eAgFnSc1d1	doktorská
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
tzv.	tzv.	kA	tzv.
velký	velký	k2eAgInSc4d1	velký
doktorát	doktorát	k1gInSc4	doktorát
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
primárně	primárně	k6eAd1	primárně
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
na	na	k7c4	na
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
činnost	činnost	k1gFnSc4	činnost
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
akademickou	akademický	k2eAgFnSc4d1	akademická
činnost	činnost	k1gFnSc4	činnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
doktorů	doktor	k1gMnPc2	doktor
medicíny	medicína	k1gFnSc2	medicína
(	(	kIx(	(
<g/>
MUDr.	MUDr.	kA	MUDr.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nemusí	muset	k5eNaImIp3nP	muset
zubní	zubní	k2eAgMnPc1d1	zubní
lékaři	lékař	k1gMnPc1	lékař
<g/>
,	,	kIx,	,
doktoři	doktor	k1gMnPc1	doktor
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
(	(	kIx(	(
<g/>
MDDr	MDDr	k1gInSc1	MDDr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
po	po	k7c6	po
kratším	krátký	k2eAgNnSc6d2	kratší
studiu	studio	k1gNnSc6	studio
věnovat	věnovat	k5eAaPmF	věnovat
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
atestacím	atestace	k1gFnPc3	atestace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
doktorát	doktorát	k1gInSc4	doktorát
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
získat	získat	k5eAaPmF	získat
na	na	k7c6	na
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
LF	LF	kA	LF
UK	UK	kA	UK
<g/>
,	,	kIx,	,
LF	LF	kA	LF
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
UK	UK	kA	UK
<g/>
,	,	kIx,	,
LF	LF	kA	LF
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
UK	UK	kA	UK
<g/>
,	,	kIx,	,
LF	LF	kA	LF
MU	MU	kA	MU
<g/>
,	,	kIx,	,
LF	LF	kA	LF
UP	UP	kA	UP
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
na	na	k7c4	na
FVZ	FVZ	kA	FVZ
UO	UO	kA	UO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zápis	zápis	k1gInSc1	zápis
==	==	k?	==
</s>
</p>
<p>
<s>
Doktorát	doktorát	k1gInSc1	doktorát
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
další	další	k2eAgFnPc1d1	další
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
platné	platný	k2eAgInPc1d1	platný
jak	jak	k8xC	jak
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
smlouvě	smlouva	k1gFnSc3	smlouva
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
podstupovat	podstupovat	k5eAaImF	podstupovat
tzv.	tzv.	kA	tzv.
nostrifikaci	nostrifikace	k1gFnSc6	nostrifikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
i	i	k8xC	i
stejný	stejný	k2eAgInSc1d1	stejný
zápis	zápis	k1gInSc1	zápis
–	–	k?	–
zkratka	zkratka	k1gFnSc1	zkratka
titulu	titul	k1gInSc2	titul
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
a	a	k8xC	a
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
se	se	k3xPyFc4	se
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
mezerou	mezera	k1gFnSc7	mezera
<g/>
,	,	kIx,	,
např.	např.	kA	např.
MDDr	MDDr	k1gInSc1	MDDr
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
větného	větný	k2eAgInSc2d1	větný
celku	celek	k1gInSc2	celek
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
<g/>
.	.	kIx.	.
/	/	kIx~	/
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
trvalo	trvat	k5eAaImAgNnS	trvat
studium	studium	k1gNnSc1	studium
stomatologie	stomatologie	k1gFnSc2	stomatologie
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
absolventi	absolvent	k1gMnPc1	absolvent
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ukončili	ukončit	k5eAaPmAgMnP	ukončit
svá	svůj	k3xOyFgNnPc4	svůj
studia	studio	k1gNnPc4	studio
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
pak	pak	k6eAd1	pak
již	již	k6eAd1	již
studium	studium	k1gNnSc1	studium
pětileté	pětiletý	k2eAgNnSc1d1	pětileté
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
studium	studium	k1gNnSc4	studium
šestileté	šestiletý	k2eAgNnSc4d1	šestileté
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
jednalo	jednat	k5eAaImAgNnS	jednat
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
,	,	kIx,	,
o	o	k7c4	o
studium	studium	k1gNnSc4	studium
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
===	===	k?	===
</s>
</p>
<p>
<s>
Doktorát	doktorát	k1gInSc1	doktorát
lékařství	lékařství	k1gNnSc2	lékařství
(	(	kIx(	(
<g/>
medicíny	medicína	k1gFnSc2	medicína
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
MUDr.	MUDr.	kA	MUDr.
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
udílen	udílet	k5eAaImNgInS	udílet
vedle	vedle	k7c2	vedle
dalších	další	k2eAgInPc2d1	další
doktorátů	doktorát	k1gInPc2	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1951	[number]	k4	1951
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
promovali	promovat	k5eAaBmAgMnP	promovat
stomatologové	stomatolog	k1gMnPc1	stomatolog
s	s	k7c7	s
akademickým	akademický	k2eAgInSc7d1	akademický
titulem	titul	k1gInSc7	titul
MSDr	MSDr	k1gMnSc1	MSDr
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
medicinae	medicinae	k1gInSc1	medicinae
stomatologicae	stomatologicaat	k5eAaPmIp3nS	stomatologicaat
doctor	doctor	k1gInSc4	doctor
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
zubní	zubní	k2eAgFnSc2d1	zubní
medicíny	medicína	k1gFnSc2	medicína
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
převzetí	převzetí	k1gNnSc6	převzetí
moci	moc	k1gFnSc2	moc
komunisty	komunista	k1gMnSc2	komunista
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
přijata	přijat	k2eAgFnSc1d1	přijata
nová	nový	k2eAgFnSc1d1	nová
legislativa	legislativa	k1gFnSc1	legislativa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
zrušila	zrušit	k5eAaPmAgFnS	zrušit
tituly	titul	k1gInPc4	titul
a	a	k8xC	a
stavovská	stavovský	k2eAgNnPc4d1	Stavovské
označení	označení	k1gNnPc4	označení
pro	pro	k7c4	pro
nové	nový	k2eAgMnPc4d1	nový
absolventy	absolvent	k1gMnPc4	absolvent
a	a	k8xC	a
těm	ten	k3xDgMnPc3	ten
tak	tak	k9	tak
byly	být	k5eAaImAgInP	být
nově	nově	k6eAd1	nově
udíleny	udílet	k5eAaImNgInP	udílet
pouze	pouze	k6eAd1	pouze
profesní	profesní	k2eAgNnSc4d1	profesní
označení	označení	k1gNnSc4	označení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
typu	typ	k1gInSc2	typ
promovaný	promovaný	k2eAgMnSc1d1	promovaný
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
mnohá	mnohé	k1gNnPc1	mnohé
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zubní	zubní	k2eAgMnPc1d1	zubní
lékaři	lékař	k1gMnPc1	lékař
tak	tak	k9	tak
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
získávali	získávat	k5eAaImAgMnP	získávat
pouze	pouze	k6eAd1	pouze
profesní	profesní	k2eAgNnSc4d1	profesní
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
promovaný	promovaný	k2eAgMnSc1d1	promovaný
zubní	zubní	k2eAgMnSc1d1	zubní
lékař	lékař	k1gMnSc1	lékař
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
lékaři	lékař	k1gMnSc3	lékař
tak	tak	k8xC	tak
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
období	období	k1gNnSc6	období
získávali	získávat	k5eAaImAgMnP	získávat
pouze	pouze	k6eAd1	pouze
profesní	profesní	k2eAgNnSc4d1	profesní
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
promovaný	promovaný	k2eAgMnSc1d1	promovaný
lékař	lékař	k1gMnSc1	lékař
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
V	v	k7c6	v
letech	let	k1gInPc6	let
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1963	[number]	k4	1963
dostali	dostat	k5eAaPmAgMnP	dostat
možnost	možnost	k1gFnSc4	možnost
dálkově	dálkově	k6eAd1	dálkově
vystudovat	vystudovat	k5eAaPmF	vystudovat
lékařské	lékařský	k2eAgFnPc4d1	lékařská
fakulty	fakulta	k1gFnPc4	fakulta
zbylí	zbylý	k2eAgMnPc1d1	zbylý
dentisté	dentista	k1gMnPc1	dentista
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
udělován	udělován	k2eAgInSc1d1	udělován
obecný	obecný	k2eAgInSc1d1	obecný
titul	titul	k1gInSc1	titul
MUDr.	MUDr.	kA	MUDr.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zároveň	zároveň	k6eAd1	zároveň
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
nahradil	nahradit	k5eAaPmAgInS	nahradit
dříve	dříve	k6eAd2	dříve
udělené	udělený	k2eAgInPc4d1	udělený
tituly	titul	k1gInPc4	titul
MSDr	MSDra	k1gFnPc2	MSDra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
(	(	kIx(	(
<g/>
převratu	převrat	k1gInSc6	převrat
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
novým	nový	k2eAgInSc7d1	nový
vysokoškolským	vysokoškolský	k2eAgInSc7d1	vysokoškolský
zákonem	zákon	k1gInSc7	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
udílen	udílen	k2eAgInSc1d1	udílen
titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
medicíny	medicína	k1gFnSc2	medicína
<g/>
"	"	kIx"	"
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
MUDr.	MUDr.	kA	MUDr.
Boloňský	boloňský	k2eAgInSc4d1	boloňský
proces	proces	k1gInSc4	proces
následně	následně	k6eAd1	následně
sjednocuje	sjednocovat	k5eAaImIp3nS	sjednocovat
evropské	evropský	k2eAgNnSc4d1	Evropské
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
vysokoškolský	vysokoškolský	k2eAgInSc1d1	vysokoškolský
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
titulu	titul	k1gInSc3	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
medicíny	medicína	k1gFnSc2	medicína
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
MUDr.	MUDr.	kA	MUDr.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
harmonizace	harmonizace	k1gFnSc2	harmonizace
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
byl	být	k5eAaImAgInS	být
i	i	k9	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
oddělen	oddělit	k5eAaPmNgInS	oddělit
od	od	k7c2	od
titulu	titul	k1gInSc2	titul
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
MUDr.	MUDr.	kA	MUDr.
<g/>
,	,	kIx,	,
titul	titul	k1gInSc4	titul
zvlášť	zvlášť	k6eAd1	zvlášť
pro	pro	k7c4	pro
zubní	zubní	k2eAgMnPc4d1	zubní
lékaře	lékař	k1gMnPc4	lékař
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
tak	tak	k6eAd1	tak
začali	začít	k5eAaPmAgMnP	začít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
získávat	získávat	k5eAaImF	získávat
v	v	k7c6	v
pětiletém	pětiletý	k2eAgInSc6d1	pětiletý
magisterském	magisterský	k2eAgInSc6d1	magisterský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
zubní	zubní	k2eAgNnSc4d1	zubní
lékařství	lékařství	k1gNnSc4	lékařství
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
zubní	zubní	k2eAgMnSc1d1	zubní
lékař	lékař	k1gMnSc1	lékař
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
MDDr	MDDr	k1gInSc1	MDDr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
vysokoškolský	vysokoškolský	k2eAgInSc1d1	vysokoškolský
zákon	zákon	k1gInSc1	zákon
novelizován	novelizován	k2eAgInSc1d1	novelizován
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
MDDr	MDDr	k1gInSc1	MDDr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doktorský	doktorský	k2eAgInSc1d1	doktorský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
"	"	kIx"	"
<g/>
postgraduální	postgraduální	k2eAgNnSc1d1	postgraduální
studium	studium	k1gNnSc1	studium
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
doctor	doctor	k1gMnSc1	doctor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gNnSc7	degree
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
dnes	dnes	k6eAd1	dnes
spjat	spjat	k2eAgMnSc1d1	spjat
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
<g/>
"	"	kIx"	"
v	v	k7c6	v
mezinárodně	mezinárodně	k6eAd1	mezinárodně
standardní	standardní	k2eAgFnSc6d1	standardní
zkratce	zkratka	k1gFnSc6	zkratka
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
(	(	kIx(	(
<g/>
za	za	k7c7	za
jménem	jméno	k1gNnSc7	jméno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
vědce	vědec	k1gMnSc4	vědec
(	(	kIx(	(
<g/>
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
dřívějšího	dřívější	k2eAgMnSc2d1	dřívější
"	"	kIx"	"
<g/>
kandidát	kandidát	k1gMnSc1	kandidát
věd	věda	k1gFnPc2	věda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Harmonizace	harmonizace	k1gFnSc2	harmonizace
výuky	výuka	k1gFnSc2	výuka
s	s	k7c7	s
EU	EU	kA	EU
==	==	k?	==
</s>
</p>
<p>
<s>
Významná	významný	k2eAgFnSc1d1	významná
změna	změna	k1gFnSc1	změna
v	v	k7c6	v
koncepci	koncepce	k1gFnSc6	koncepce
pregraduální	pregraduální	k2eAgFnSc2d1	pregraduální
výuky	výuka	k1gFnSc2	výuka
nastala	nastat	k5eAaPmAgNnP	nastat
od	od	k7c2	od
akademického	akademický	k2eAgInSc2d1	akademický
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
harmonizace	harmonizace	k1gFnSc2	harmonizace
výuky	výuka	k1gFnSc2	výuka
se	s	k7c7	s
státy	stát	k1gInPc7	stát
EU	EU	kA	EU
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
pětiletý	pětiletý	k2eAgInSc1d1	pětiletý
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc4	program
zubní	zubní	k2eAgInSc4d1	zubní
lékařství	lékařství	k1gNnSc4	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
od	od	k7c2	od
původního	původní	k2eAgInSc2d1	původní
programu	program	k1gInSc2	program
stomatologie	stomatologie	k1gFnSc2	stomatologie
liší	lišit	k5eAaImIp3nS	lišit
zejména	zejména	k9	zejména
vyšším	vysoký	k2eAgInSc7d2	vyšší
počtem	počet	k1gInSc7	počet
hodin	hodina	k1gFnPc2	hodina
praktické	praktický	k2eAgFnSc2d1	praktická
výuky	výuka	k1gFnSc2	výuka
a	a	k8xC	a
vyšším	vysoký	k2eAgInSc7d2	vyšší
počtem	počet	k1gInSc7	počet
hodin	hodina	k1gFnPc2	hodina
výuky	výuka	k1gFnSc2	výuka
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
počty	počet	k1gInPc4	počet
hodin	hodina	k1gFnPc2	hodina
výuky	výuka	k1gFnSc2	výuka
ostatních	ostatní	k2eAgInPc2d1	ostatní
lékařských	lékařský	k2eAgInPc2d1	lékařský
oborů	obor	k1gInPc2	obor
byly	být	k5eAaImAgFnP	být
sníženy	snížen	k2eAgFnPc1d1	snížena
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zásadní	zásadní	k2eAgFnSc1d1	zásadní
změna	změna	k1gFnSc1	změna
byla	být	k5eAaImAgFnS	být
realizována	realizovat	k5eAaBmNgFnS	realizovat
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
lékařských	lékařský	k2eAgFnPc6d1	lékařská
fakultách	fakulta	k1gFnPc6	fakulta
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Výuka	výuka	k1gFnSc1	výuka
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostává	dostávat	k5eAaImIp3nS	dostávat
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
západních	západní	k2eAgInPc2d1	západní
států	stát	k1gInPc2	stát
s	s	k7c7	s
vyspělou	vyspělý	k2eAgFnSc7d1	vyspělá
péčí	péče	k1gFnSc7	péče
o	o	k7c4	o
chrup	chrup	k1gInSc4	chrup
a	a	k8xC	a
okolní	okolní	k2eAgFnPc4d1	okolní
tkáně	tkáň	k1gFnPc4	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
je	být	k5eAaImIp3nS	být
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
snadnějším	snadný	k2eAgNnSc7d2	snazší
zařazením	zařazení	k1gNnSc7	zařazení
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
(	(	kIx(	(
<g/>
soukromou	soukromý	k2eAgFnSc4d1	soukromá
praxi	praxe	k1gFnSc4	praxe
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
ihned	ihned	k6eAd1	ihned
i	i	k8xC	i
absolventi	absolvent	k1gMnPc1	absolvent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Změna	změna	k1gFnSc1	změna
byla	být	k5eAaImAgFnS	být
realizována	realizovat	k5eAaBmNgFnS	realizovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Směrnice	směrnice	k1gFnSc2	směrnice
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
Rady	rada	k1gFnSc2	rada
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
<g/>
/	/	kIx~	/
<g/>
ES	ES	kA	ES
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
státy	stát	k1gInPc4	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
určeno	určit	k5eAaPmNgNnS	určit
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
lze	lze	k6eAd1	lze
výhradně	výhradně	k6eAd1	výhradně
studovat	studovat	k5eAaImF	studovat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
a	a	k8xC	a
jakým	jaký	k3yIgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
vydává	vydávat	k5eAaPmIp3nS	vydávat
certifikaci	certifikace	k1gFnSc4	certifikace
a	a	k8xC	a
jaké	jaký	k3yQgFnPc4	jaký
podmínky	podmínka	k1gFnPc4	podmínka
musí	muset	k5eAaImIp3nS	muset
akreditované	akreditovaný	k2eAgNnSc1d1	akreditované
pracoviště	pracoviště	k1gNnSc1	pracoviště
splňovat	splňovat	k5eAaImF	splňovat
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
státy	stát	k1gInPc1	stát
si	se	k3xPyFc3	se
nicméně	nicméně	k8xC	nicméně
značně	značně	k6eAd1	značně
rozdílně	rozdílně	k6eAd1	rozdílně
určují	určovat	k5eAaImIp3nP	určovat
kritéria	kritérion	k1gNnPc1	kritérion
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
titulu	titul	k1gInSc2	titul
<g/>
,	,	kIx,	,
další	další	k2eAgNnPc4d1	další
vzdělávání	vzdělávání	k1gNnPc4	vzdělávání
a	a	k8xC	a
specializace	specializace	k1gFnPc4	specializace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
budou	být	k5eAaImBp3nP	být
vydány	vydat	k5eAaPmNgFnP	vydat
jen	jen	k9	jen
po	po	k7c6	po
splnění	splnění	k1gNnSc6	splnění
podmínek	podmínka	k1gFnPc2	podmínka
platných	platný	k2eAgFnPc2d1	platná
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
kterém	který	k3yRgInSc6	který
státě	stát	k1gInSc6	stát
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
směrnice	směrnice	k1gFnSc1	směrnice
nařizuje	nařizovat	k5eAaImIp3nS	nařizovat
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
uznatelnost	uznatelnost	k1gFnSc4	uznatelnost
vzdělání	vzdělání	k1gNnSc2	vzdělání
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
EU	EU	kA	EU
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Přistoupením	přistoupení	k1gNnSc7	přistoupení
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
k	k	k7c3	k
EU	EU	kA	EU
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
směrnice	směrnice	k1gFnSc1	směrnice
stala	stát	k5eAaPmAgFnS	stát
závaznou	závazný	k2eAgFnSc4d1	závazná
pro	pro	k7c4	pro
český	český	k2eAgInSc4d1	český
právní	právní	k2eAgInSc4d1	právní
řád	řád	k1gInSc4	řád
a	a	k8xC	a
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
titulu	titul	k1gInSc3	titul
zubních	zubní	k2eAgMnPc2d1	zubní
lékařů	lékař	k1gMnPc2	lékař
byly	být	k5eAaImAgFnP	být
její	její	k3xOp3gNnSc1	její
podmínky	podmínka	k1gFnPc1	podmínka
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
převedeny	převeden	k2eAgInPc1d1	převeden
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
121	[number]	k4	121
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
novelizoval	novelizovat	k5eAaBmAgMnS	novelizovat
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgMnS	zavést
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
zubní	zubní	k2eAgMnSc1d1	zubní
lékař	lékař	k1gMnSc1	lékař
<g/>
"	"	kIx"	"
se	s	k7c7	s
zkratkou	zkratka	k1gFnSc7	zkratka
MDDr	MDDra	k1gFnPc2	MDDra
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
"	"	kIx"	"
<g/>
doktorem	doktor	k1gMnSc7	doktor
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
však	však	k9	však
zůstala	zůstat	k5eAaPmAgFnS	zůstat
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
.	.	kIx.	.
<g/>
Změna	změna	k1gFnSc1	změna
v	v	k7c6	v
názvu	název	k1gInSc6	název
titulu	titul	k1gInSc2	titul
nic	nic	k3yNnSc1	nic
nemění	měnit	k5eNaImIp3nS	měnit
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zubní	zubní	k2eAgNnSc1d1	zubní
lékařství	lékařství	k1gNnSc1	lékařství
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
lékařským	lékařský	k2eAgInSc7d1	lékařský
oborem	obor	k1gInSc7	obor
majícím	mající	k2eAgInSc7d1	mající
specializovanou	specializovaný	k2eAgFnSc4d1	specializovaná
náplň	náplň	k1gFnSc4	náplň
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
koncipovat	koncipovat	k5eAaBmF	koncipovat
výuku	výuka	k1gFnSc4	výuka
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
výkon	výkon	k1gInSc4	výkon
odborné	odborný	k2eAgFnSc2d1	odborná
lékařské	lékařský	k2eAgFnSc2d1	lékařská
praxe	praxe	k1gFnSc2	praxe
již	již	k6eAd1	již
od	od	k7c2	od
prvních	první	k4xOgInPc2	první
ročníků	ročník	k1gInPc2	ročník
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
stavovsky	stavovsky	k6eAd1	stavovsky
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
mezi	mezi	k7c4	mezi
podobné	podobný	k2eAgInPc4d1	podobný
absolventy	absolvent	k1gMnPc7	absolvent
universitních	universitní	k2eAgFnPc2d1	universitní
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zubní	zubní	k2eAgNnSc1d1	zubní
lékařství	lékařství	k1gNnSc1	lékařství
má	mít	k5eAaImIp3nS	mít
ovšem	ovšem	k9	ovšem
své	svůj	k3xOyFgFnPc4	svůj
specializace	specializace	k1gFnPc4	specializace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ortodontista	ortodontista	k1gMnSc1	ortodontista
musí	muset	k5eAaImIp3nS	muset
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
třemi	tři	k4xCgInPc7	tři
lety	let	k1gInPc7	let
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
medicíny	medicína	k1gFnSc2	medicína
</s>
</p>
<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
veterinární	veterinární	k2eAgFnSc2d1	veterinární
medicíny	medicína	k1gFnSc2	medicína
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
MDDr	MDDra	k1gFnPc2	MDDra
<g/>
.	.	kIx.	.
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
