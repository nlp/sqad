<s>
Infimum	Infimum	k1gNnSc1	Infimum
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
průsek	průsek	k1gInSc1	průsek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
matematický	matematický	k2eAgInSc1d1	matematický
pojem	pojem	k1gInSc1	pojem
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
teorie	teorie	k1gFnSc2	teorie
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
používán	používat	k5eAaImNgInS	používat
především	především	k9	především
při	při	k7c6	při
zkoumání	zkoumání	k1gNnSc6	zkoumání
vlastností	vlastnost	k1gFnPc2	vlastnost
reálných	reálný	k2eAgFnPc2d1	reálná
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Infimum	Infimum	k1gNnSc1	Infimum
je	být	k5eAaImIp3nS	být
zaváděno	zavádět	k5eAaImNgNnS	zavádět
jako	jako	k8xS	jako
alternativa	alternativa	k1gFnSc1	alternativa
k	k	k7c3	k
pojmu	pojem	k1gInSc3	pojem
nejmenší	malý	k2eAgInSc1d3	nejmenší
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
nejmenšímu	malý	k2eAgInSc3d3	nejmenší
prvku	prvek	k1gInSc3	prvek
je	být	k5eAaImIp3nS	být
však	však	k9	však
dohledatelné	dohledatelný	k2eAgNnSc1d1	dohledatelné
u	u	k7c2	u
více	hodně	k6eAd2	hodně
množin	množina	k1gFnPc2	množina
–	–	k?	–
například	například	k6eAd1	například
omezené	omezený	k2eAgInPc1d1	omezený
otevřené	otevřený	k2eAgInPc1d1	otevřený
intervaly	interval	k1gInPc1	interval
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
nemají	mít	k5eNaImIp3nP	mít
nejmenší	malý	k2eAgInSc4d3	nejmenší
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
infimum	infimum	k1gInSc4	infimum
<g/>
.	.	kIx.	.
</s>
<s>
Duálním	duální	k2eAgInSc7d1	duální
pojmem	pojem	k1gInSc7	pojem
(	(	kIx(	(
<g/>
opakem	opak	k1gInSc7	opak
<g/>
)	)	kIx)	)
infima	infima	k1gFnSc1	infima
je	být	k5eAaImIp3nS	být
supremum	supremum	k1gInSc4	supremum
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
množina	množina	k1gFnSc1	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
relací	relace	k1gFnSc7	relace
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
prvku	prvek	k1gInSc6	prvek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
∈	∈	k?	∈
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
X	X	kA	X
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
řekneme	říct	k5eAaPmIp1nP	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
infimum	infimum	k1gNnSc1	infimum
podmnožiny	podmnožina	k1gFnSc2	podmnožina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Y	Y	kA	Y
⊆	⊆	k?	⊆
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Y	Y	kA	Y
<g/>
\	\	kIx~	\
<g/>
subseteq	subseteq	k?	subseteq
X	X	kA	X
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
největší	veliký	k2eAgInSc1d3	veliký
prvek	prvek	k1gInSc1	prvek
množiny	množina	k1gFnSc2	množina
všech	všecek	k3xTgFnPc2	všecek
dolních	dolní	k2eAgFnPc2d1	dolní
závor	závora	k1gFnPc2	závora
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Y	Y	kA	Y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Y	Y	kA	Y
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
značíme	značit	k5eAaImIp1nP	značit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
=	=	kIx~	=
:	:	kIx,	:
inf	inf	k?	inf
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
Y	Y	kA	Y
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
inf	inf	k?	inf
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Infimum	Infimum	k1gNnSc1	Infimum
má	mít	k5eAaImIp3nS	mít
každá	každý	k3xTgFnSc1	každý
zdola	zdola	k6eAd1	zdola
omezená	omezený	k2eAgFnSc1d1	omezená
množina	množina	k1gFnSc1	množina
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
ne	ne	k9	ne
každá	každý	k3xTgFnSc1	každý
má	mít	k5eAaImIp3nS	mít
minimum	minimum	k1gNnSc4	minimum
(	(	kIx(	(
<g/>
nejmenší	malý	k2eAgFnSc1d3	nejmenší
prvek	prvek	k1gInSc1	prvek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
otevřený	otevřený	k2eAgInSc4d1	otevřený
interval	interval	k1gInSc4	interval
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
I	i	k9	i
=	=	kIx~	=
(	(	kIx(	(
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
I	i	k9	i
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
minimum	minimum	k1gNnSc1	minimum
nemá	mít	k5eNaImIp3nS	mít
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
∈	∈	k?	∈
I	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
I	I	kA	I
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaBmF	nalézt
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
c	c	k0	c
>	>	kIx)	>
d	d	k?	d
>	>	kIx)	>
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
d	d	k?	d
<g/>
:	:	kIx,	:
<g/>
c	c	k0	c
<g/>
>	>	kIx)	>
<g/>
d	d	k?	d
<g/>
>	>	kIx)	>
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jeho	jeho	k3xOp3gNnSc7	jeho
infimem	infimo	k1gNnSc7	infimo
je	být	k5eAaImIp3nS	být
právě	právě	k6eAd1	právě
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dolní	dolní	k2eAgFnSc4d1	dolní
závoru	závora	k1gFnSc4	závora
a	a	k8xC	a
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
větší	veliký	k2eAgNnSc4d2	veliký
číslo	číslo	k1gNnSc4	číslo
již	již	k6eAd1	již
dolní	dolní	k2eAgFnSc7d1	dolní
závorou	závora	k1gFnSc7	závora
není	být	k5eNaImIp3nS	být
–	–	k?	–
lze	lze	k6eAd1	lze
argumentovat	argumentovat	k5eAaImF	argumentovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
minima	minimum	k1gNnSc2	minimum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdola	zdola	k6eAd1	zdola
neomezené	omezený	k2eNgFnPc1d1	neomezená
množiny	množina	k1gFnPc1	množina
infimum	infimum	k1gInSc1	infimum
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
otevřený	otevřený	k2eAgInSc4d1	otevřený
interval	interval	k1gInSc4	interval
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
I	i	k9	i
=	=	kIx~	=
(	(	kIx(	(
−	−	k?	−
∞	∞	k?	∞
,	,	kIx,	,
a	a	k8xC	a
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
I	i	k9	i
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
,	,	kIx,	,
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
nemá	mít	k5eNaImIp3nS	mít
infimum	infimum	k1gInSc4	infimum
v	v	k7c6	v
množině	množina	k1gFnSc6	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
všech	všecek	k3xTgNnPc2	všecek
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
množina	množina	k1gFnSc1	množina
minimum	minimum	k1gNnSc4	minimum
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
infimum	infimum	k1gInSc1	infimum
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
K	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgFnPc4	který
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
K	k	k7c3	k
=	=	kIx~	=
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
M	M	kA	M
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
množině	množina	k1gFnSc6	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obecně	obecně	k6eAd1	obecně
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
množinách	množina	k1gFnPc6	množina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
infimum	infimum	k1gInSc1	infimum
zobecněním	zobecnění	k1gNnSc7	zobecnění
pojmu	pojem	k1gInSc2	pojem
nejmenšího	malý	k2eAgInSc2d3	nejmenší
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
množina	množina	k1gFnSc1	množina
nejmenší	malý	k2eAgFnSc1d3	nejmenší
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
nejmenší	malý	k2eAgInSc4d3	nejmenší
prvek	prvek	k1gInSc4	prvek
zároveň	zároveň	k6eAd1	zároveň
jejím	její	k3xOp3gMnSc7	její
infimem	infim	k1gMnSc7	infim
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
to	ten	k3xDgNnSc1	ten
však	však	k9	však
platit	platit	k5eAaImF	platit
nemusí	muset	k5eNaImIp3nP	muset
–	–	k?	–
prvním	první	k4xOgMnSc6	první
takovým	takový	k3xDgInSc7	takový
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
výše	vysoce	k6eAd2	vysoce
uvedený	uvedený	k2eAgInSc4d1	uvedený
zdola	zdola	k6eAd1	zdola
omezený	omezený	k2eAgInSc4d1	omezený
otevřený	otevřený	k2eAgInSc4d1	otevřený
interval	interval	k1gInSc4	interval
na	na	k7c6	na
množině	množina	k1gFnSc6	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
infimum	infimum	k1gInSc1	infimum
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
jednoznačně	jednoznačně	k6eAd1	jednoznačně
–	–	k?	–
množina	množina	k1gFnSc1	množina
nemůže	moct	k5eNaImIp3nS	moct
mít	mít	k5eAaImF	mít
dvě	dva	k4xCgNnPc4	dva
různá	různý	k2eAgNnPc4d1	různé
infima	infimum	k1gNnPc4	infimum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
největší	veliký	k2eAgInSc1d3	veliký
prvek	prvek	k1gInSc1	prvek
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
i	i	k9	i
největší	veliký	k2eAgInSc1d3	veliký
prvek	prvek	k1gInSc1	prvek
množiny	množina	k1gFnSc2	množina
dolních	dolní	k2eAgFnPc2d1	dolní
závor	závora	k1gFnPc2	závora
–	–	k?	–
infimum	infimum	k1gNnSc4	infimum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
,	,	kIx,	,
jednoznačně	jednoznačně	k6eAd1	jednoznačně
určen	určen	k2eAgInSc1d1	určen
<g/>
.	.	kIx.	.
</s>
<s>
Uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
o	o	k7c6	o
množině	množina	k1gFnSc6	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
všech	všecek	k3xTgNnPc2	všecek
kladných	kladný	k2eAgNnPc2d1	kladné
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
relaci	relace	k1gFnSc6	relace
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
danou	daný	k2eAgFnSc4d1	daná
vztahem	vztah	k1gInSc7	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
≤	≤	k?	≤
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
⇔	⇔	k?	⇔
a	a	k8xC	a
∣	∣	k?	∣
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
Leftrightarrow	Leftrightarrow	k1gMnSc1	Leftrightarrow
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
mid	mid	k?	mid
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
číslo	číslo	k1gNnSc4	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
je	on	k3xPp3gFnPc4	on
menší	malý	k2eAgFnPc4d2	menší
nebo	nebo	k8xC	nebo
rovné	rovný	k2eAgFnPc4d1	rovná
číslu	číslo	k1gNnSc3	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
podle	podle	k7c2	podle
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
,	,	kIx,	,
pokud	pokud	k8xS	pokud
číslo	číslo	k1gNnSc1	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
dělí	dělit	k5eAaImIp3nP	dělit
číslo	číslo	k1gNnSc4	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
konečná	konečný	k2eAgFnSc1d1	konečná
podmnožina	podmnožina	k1gFnSc1	podmnožina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
má	mít	k5eAaImIp3nS	mít
infimum	infimum	k1gInSc1	infimum
–	–	k?	–
infimem	infimo	k1gNnSc7	infimo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
největší	veliký	k2eAgInSc1d3	veliký
společný	společný	k2eAgInSc1d1	společný
dělitel	dělitel	k1gInSc1	dělitel
<g/>
.	.	kIx.	.
</s>
<s>
Zdaleka	zdaleka	k6eAd1	zdaleka
ne	ne	k9	ne
každá	každý	k3xTgFnSc1	každý
množina	množina	k1gFnSc1	množina
má	mít	k5eAaImIp3nS	mít
ale	ale	k8xC	ale
nejmenší	malý	k2eAgInSc1d3	nejmenší
prvek	prvek	k1gInSc1	prvek
–	–	k?	–
například	například	k6eAd1	například
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
4	[number]	k4	4
,	,	kIx,	,
6	[number]	k4	6
,	,	kIx,	,
8	[number]	k4	8
}	}	kIx)	}
⊆	⊆	k?	⊆
:	:	kIx,	:
:	:	kIx,	:
Z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
4,6	[number]	k4	4,6
<g/>
,8	,8	k4	,8
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
subseteq	subseteq	k?	subseteq
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
nemá	mít	k5eNaImIp3nS	mít
nejmenší	malý	k2eAgInSc4d3	nejmenší
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
neplatí	platit	k5eNaImIp3nS	platit
ani	ani	k8xC	ani
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
≤	≤	k?	≤
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
<g/>
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
,	,	kIx,	,
ani	ani	k8xC	ani
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
≤	≤	k?	≤
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
ale	ale	k9	ale
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
inf	inf	k?	inf
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
4	[number]	k4	4
,	,	kIx,	,
6	[number]	k4	6
,	,	kIx,	,
8	[number]	k4	8
}	}	kIx)	}
=	=	kIx~	=
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
inf	inf	k?	inf
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
4,6	[number]	k4	4,6
<g/>
,8	,8	k4	,8
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
každá	každý	k3xTgFnSc1	každý
zdola	zdola	k6eAd1	zdola
omezená	omezený	k2eAgFnSc1d1	omezená
množina	množina	k1gFnSc1	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
infimum	infimum	k1gInSc1	infimum
<g/>
.	.	kIx.	.
</s>
<s>
Zdálo	zdát	k5eAaImAgNnS	zdát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
množina	množina	k1gFnSc1	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
Q	Q	kA	Q
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
racionálních	racionální	k2eAgNnPc2d1	racionální
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
množině	množina	k1gFnSc3	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
hodně	hodně	k6eAd1	hodně
podobná	podobný	k2eAgFnSc1d1	podobná
–	–	k?	–
je	být	k5eAaImIp3nS	být
také	také	k9	také
hustě	hustě	k6eAd1	hustě
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
zdola	zdola	k6eAd1	zdola
omezené	omezený	k2eAgFnPc1d1	omezená
množiny	množina	k1gFnPc1	množina
racionálních	racionální	k2eAgNnPc2d1	racionální
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nemají	mít	k5eNaImIp3nP	mít
(	(	kIx(	(
<g/>
v	v	k7c6	v
množině	množina	k1gFnSc6	množina
racionálních	racionální	k2eAgNnPc2d1	racionální
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
infimum	infimum	k1gInSc1	infimum
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
takové	takový	k3xDgFnSc2	takový
množiny	množina	k1gFnSc2	množina
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
x	x	k?	x
∈	∈	k?	∈
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
>	>	kIx)	>
2	[number]	k4	2
:	:	kIx,	:
∧	∧	k?	∧
:	:	kIx,	:
x	x	k?	x
>	>	kIx)	>
0	[number]	k4	0
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
Q	Q	kA	Q
<g/>
}	}	kIx)	}
:	:	kIx,	:
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
>	>	kIx)	>
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
land	land	k6eAd1	land
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
x	x	k?	x
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Lze	lze	k6eAd1	lze
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
ověřit	ověřit	k5eAaPmF	ověřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
množině	množina	k1gFnSc6	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
Q	Q	kA	Q
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
nemá	mít	k5eNaImIp3nS	mít
tato	tento	k3xDgFnSc1	tento
množina	množina	k1gFnSc1	množina
infimum	infimum	k1gInSc4	infimum
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
uvažovali	uvažovat	k5eAaImAgMnP	uvažovat
o	o	k7c6	o
infimu	infim	k1gInSc6	infim
této	tento	k3xDgFnSc2	tento
množiny	množina	k1gFnSc2	množina
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
všech	všecek	k3xTgNnPc2	všecek
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
o	o	k7c4	o
něco	něco	k3yInSc4	něco
lépe	dobře	k6eAd2	dobře
–	–	k?	–
infimem	infim	k1gInSc7	infim
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
odmocnina	odmocnina	k1gFnSc1	odmocnina
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
<g/>
.	.	kIx.	.
</s>
