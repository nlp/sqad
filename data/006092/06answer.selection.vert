<s>
Infimum	Infimum	k1gNnSc1	Infimum
je	být	k5eAaImIp3nS	být
zaváděno	zavádět	k5eAaImNgNnS	zavádět
jako	jako	k8xS	jako
alternativa	alternativa	k1gFnSc1	alternativa
k	k	k7c3	k
pojmu	pojem	k1gInSc3	pojem
nejmenší	malý	k2eAgInSc1d3	nejmenší
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
nejmenšímu	malý	k2eAgInSc3d3	nejmenší
prvku	prvek	k1gInSc3	prvek
je	být	k5eAaImIp3nS	být
však	však	k9	však
dohledatelné	dohledatelný	k2eAgNnSc1d1	dohledatelné
u	u	k7c2	u
více	hodně	k6eAd2	hodně
množin	množina	k1gFnPc2	množina
–	–	k?	–
například	například	k6eAd1	například
omezené	omezený	k2eAgInPc1d1	omezený
otevřené	otevřený	k2eAgInPc1d1	otevřený
intervaly	interval	k1gInPc1	interval
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
nemají	mít	k5eNaImIp3nP	mít
nejmenší	malý	k2eAgInSc4d3	nejmenší
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
infimum	infimum	k1gInSc4	infimum
<g/>
.	.	kIx.	.
</s>
