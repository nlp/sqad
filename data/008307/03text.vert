<p>
<s>
Mike	Mike	k1gFnSc1	Mike
Smith	Smith	k1gMnSc1	Smith
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
kanadský	kanadský	k2eAgMnSc1d1	kanadský
atlet	atlet	k1gMnSc1	atlet
-	-	kIx~	-
vícebojař	vícebojař	k1gMnSc1	vícebojař
-	-	kIx~	-
původem	původ	k1gInSc7	původ
z	z	k7c2	z
městečka	městečko	k1gNnSc2	městečko
Kenora	Kenor	k1gMnSc2	Kenor
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
.	.	kIx.	.
</s>
<s>
Smith	Smith	k1gMnSc1	Smith
nikdy	nikdy	k6eAd1	nikdy
nedosáhl	dosáhnout	k5eNaPmAgMnS	dosáhnout
na	na	k7c4	na
velké	velký	k2eAgInPc4d1	velký
zlaté	zlatý	k2eAgInPc4d1	zlatý
úspěchy	úspěch	k1gInPc4	úspěch
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
bodový	bodový	k2eAgInSc1d1	bodový
součet	součet	k1gInSc1	součet
jeho	jeho	k3xOp3gInPc2	jeho
osobních	osobní	k2eAgInPc2d1	osobní
rekordů	rekord	k1gInPc2	rekord
v	v	k7c6	v
desetiboji	desetiboj	k1gInSc6	desetiboj
(	(	kIx(	(
<g/>
9362	[number]	k4	9362
b.	b.	k?	b.
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
dosud	dosud	k6eAd1	dosud
třetí	třetí	k4xOgFnSc4	třetí
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
hodnotu	hodnota	k1gFnSc4	hodnota
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgMnPc7	všecek
vícebojaři	vícebojař	k1gMnPc7	vícebojař
(	(	kIx(	(
<g/>
po	po	k7c6	po
Američanech	Američan	k1gMnPc6	Američan
Danu	Dan	k1gMnSc3	Dan
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Brienovi	Brien	k1gMnSc3	Brien
a	a	k8xC	a
Ashtonovi	Ashton	k1gMnSc6	Ashton
Eatonovi	Eaton	k1gMnSc6	Eaton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skvělý	skvělý	k2eAgInSc1d1	skvělý
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gInSc1	jeho
osobní	osobní	k2eAgInSc1d1	osobní
rekord	rekord	k1gInSc1	rekord
ve	v	k7c6	v
vrhu	vrh	k1gInSc6	vrh
koulí	koule	k1gFnPc2	koule
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
činí	činit	k5eAaImIp3nS	činit
18,03	[number]	k4	18,03
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Smith	Smith	k1gMnSc1	Smith
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
největší	veliký	k2eAgInSc4d3	veliký
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
medaile	medaile	k1gFnSc2	medaile
na	na	k7c4	na
MS	MS	kA	MS
1991	[number]	k4	1991
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
a	a	k8xC	a
pak	pak	k6eAd1	pak
na	na	k7c6	na
halovém	halový	k2eAgInSc6d1	halový
šampionátu	šampionát	k1gInSc6	šampionát
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
také	také	k9	také
několikrát	několikrát	k6eAd1	několikrát
na	na	k7c6	na
tradičním	tradiční	k2eAgMnSc6d1	tradiční
Hypo-mítinku	Hypoítinka	k1gFnSc4	Hypo-mítinka
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
Götzisu	Götzis	k1gInSc6	Götzis
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
zde	zde	k6eAd1	zde
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
i	i	k9	i
nový	nový	k2eAgInSc1d1	nový
kanadský	kanadský	k2eAgInSc1d1	kanadský
rekord	rekord	k1gInSc1	rekord
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
8626	[number]	k4	8626
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc1	ten
překonal	překonat	k5eAaPmAgInS	překonat
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
Damian	Damian	k1gMnSc1	Damian
Warner	Warner	k1gMnSc1	Warner
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Smith	Smith	k1gMnSc1	Smith
ukončil	ukončit	k5eAaPmAgMnS	ukončit
svoji	svůj	k3xOyFgFnSc4	svůj
profesionální	profesionální	k2eAgFnSc4d1	profesionální
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
žije	žít	k5eAaImIp3nS	žít
a	a	k8xC	a
podniká	podnikat	k5eAaImIp3nS	podnikat
v	v	k7c6	v
Calgary	Calgary	k1gNnSc6	Calgary
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInPc4d1	osobní
rekordy	rekord	k1gInPc4	rekord
==	==	k?	==
</s>
</p>
<p>
<s>
100	[number]	k4	100
m	m	kA	m
-	-	kIx~	-
10,70	[number]	k4	10,70
s.	s.	k?	s.
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Dálka	dálka	k1gFnSc1	dálka
-	-	kIx~	-
776	[number]	k4	776
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Koule	koule	k1gFnSc1	koule
-	-	kIx~	-
18,03	[number]	k4	18,03
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Výška	výška	k1gFnSc1	výška
-	-	kIx~	-
214	[number]	k4	214
cm	cm	kA	cm
</s>
</p>
<p>
<s>
400	[number]	k4	400
m.	m.	k?	m.
-	-	kIx~	-
47,06	[number]	k4	47,06
s.	s.	k?	s.
</s>
</p>
<p>
<s>
110	[number]	k4	110
m.	m.	k?	m.
př	př	kA	př
<g/>
.	.	kIx.	.
-	-	kIx~	-
14,24	[number]	k4	14,24
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Disk	disk	k1gInSc1	disk
-	-	kIx~	-
52,90	[number]	k4	52,90
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Tyč	tyč	k1gFnSc1	tyč
-	-	kIx~	-
520	[number]	k4	520
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Oštěp	oštěp	k1gInSc1	oštěp
-	-	kIx~	-
71,22	[number]	k4	71,22
m.	m.	k?	m.
</s>
</p>
<p>
<s>
1500	[number]	k4	1500
m.	m.	k?	m.
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
20,04	[number]	k4	20,04
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Desetiboj	desetiboj	k1gInSc1	desetiboj
8626	[number]	k4	8626
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sedmiboj	sedmiboj	k1gInSc1	sedmiboj
6279	[number]	k4	6279
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c6	na
webu	web	k1gInSc6	web
IAAF	IAAF	kA	IAAF
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
