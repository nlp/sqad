<p>
<s>
Osada	osada	k1gFnSc1	osada
Štrbské	štrbský	k2eAgNnSc4d1	Štrbské
Pleso	pleso	k1gNnSc4	pleso
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
obce	obec	k1gFnSc2	obec
Štrba	Štrba	k1gFnSc1	Štrba
(	(	kIx(	(
<g/>
Štrba	Štrba	k1gFnSc1	Štrba
<g/>
,	,	kIx,	,
Tatranská	tatranský	k2eAgFnSc1d1	Tatranská
Štrba	Štrba	k1gFnSc1	Štrba
<g/>
,	,	kIx,	,
Štrbské	štrbský	k2eAgNnSc1d1	Štrbské
Pleso	pleso	k1gNnSc1	pleso
<g/>
)	)	kIx)	)
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
Tatry	Tatra	k1gFnSc2	Tatra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Štrbské	štrbský	k2eAgNnSc1d1	Štrbské
Pleso	pleso	k1gNnSc1	pleso
je	být	k5eAaImIp3nS	být
významné	významný	k2eAgNnSc1d1	významné
centrum	centrum	k1gNnSc1	centrum
turismu	turismus	k1gInSc2	turismus
a	a	k8xC	a
zimních	zimní	k2eAgInPc2d1	zimní
sportů	sport	k1gInPc2	sport
<g/>
,	,	kIx,	,
nejvýše	vysoce	k6eAd3	vysoce
položená	položený	k2eAgFnSc1d1	položená
turisticko-léčebná	turistickoéčebný	k2eAgFnSc1d1	turisticko-léčebný
osada	osada	k1gFnSc1	osada
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
Vysokých	vysoký	k2eAgFnPc6d1	vysoká
Tatrách	Tatra	k1gFnPc6	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
břehu	břeh	k1gInSc6	břeh
jezera	jezero	k1gNnSc2	jezero
Štrbské	štrbský	k2eAgNnSc4d1	Štrbské
pleso	pleso	k1gNnSc4	pleso
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1	[number]	k4	1
346	[number]	k4	346
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Štrbské	štrbský	k2eAgNnSc4d1	Štrbské
Pleso	pleso	k1gNnSc4	pleso
patří	patřit	k5eAaImIp3nS	patřit
mimo	mimo	k7c4	mimo
osady	osada	k1gFnPc4	osada
Štrbské	štrbský	k2eAgNnSc4d1	Štrbské
Pleso	pleso	k1gNnSc4	pleso
i	i	k8xC	i
osady	osada	k1gFnSc2	osada
Podbanské	Podbanský	k2eAgFnSc2d1	Podbanský
(	(	kIx(	(
<g/>
jen	jen	k9	jen
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
obci	obec	k1gFnSc3	obec
Pribylina	Pribylin	k2eAgInSc2d1	Pribylin
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vyšné	vyšný	k2eAgFnPc4d1	Vyšná
Hágy	Hága	k1gFnPc4	Hága
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
osada	osada	k1gFnSc1	osada
Štrbské	štrbský	k2eAgNnSc4d1	Štrbské
Pleso	pleso	k1gNnSc4	pleso
s	s	k7c7	s
historickou	historický	k2eAgFnSc7d1	historická
částí	část	k1gFnSc7	část
katastru	katastr	k1gInSc2	katastr
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
SR	SR	kA	SR
navrácena	navrácen	k2eAgFnSc1d1	navrácena
obci	obec	k1gFnSc3	obec
Štrba	Štrba	k1gFnSc1	Štrba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Štrbské	štrbský	k2eAgNnSc1d1	Štrbské
pleso	pleso	k1gNnSc1	pleso
je	být	k5eAaImIp3nS	být
dostupné	dostupný	k2eAgNnSc1d1	dostupné
jak	jak	k8xS	jak
cestou	cesta	k1gFnSc7	cesta
pro	pro	k7c4	pro
automobily	automobil	k1gInPc4	automobil
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Tatranskou	tatranský	k2eAgFnSc7d1	Tatranská
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
železnicí	železnice	k1gFnSc7	železnice
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
nevšední	všední	k2eNgFnSc7d1	nevšední
ozubnicovou	ozubnicový	k2eAgFnSc7d1	ozubnicová
železnicí	železnice	k1gFnSc7	železnice
<g/>
,	,	kIx,	,
nazývanou	nazývaný	k2eAgFnSc7d1	nazývaná
"	"	kIx"	"
<g/>
Zubačka	zubačka	k1gFnSc1	zubačka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zubačka	zubačka	k1gFnSc1	zubačka
spojuje	spojovat	k5eAaImIp3nS	spojovat
Štrbské	štrbský	k2eAgNnSc4d1	Štrbské
pleso	pleso	k1gNnSc4	pleso
se	s	k7c7	s
Štrbou	Štrba	k1gFnSc7	Štrba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
Žilina	Žilina	k1gFnSc1	Žilina
-	-	kIx~	-
Košice	Košice	k1gInPc1	Košice
<g/>
,	,	kIx,	,
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
mezilehlá	mezilehlý	k2eAgFnSc1d1	mezilehlá
zastávka	zastávka	k1gFnSc1	zastávka
Tatranský	tatranský	k2eAgInSc4d1	tatranský
Lieskovec	Lieskovec	k1gInSc4	Lieskovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
tratě	trať	k1gFnSc2	trať
je	být	k5eAaImIp3nS	být
4,78	[number]	k4	4,78
km	km	kA	km
s	s	k7c7	s
maximálním	maximální	k2eAgNnSc7d1	maximální
stoupaním	stoupaní	k1gNnSc7	stoupaní
150	[number]	k4	150
‰	‰	k?	‰
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Štrba	Štrba	k1gFnSc1	Štrba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
vybudována	vybudován	k2eAgFnSc1d1	vybudována
krytá	krytý	k2eAgFnSc1d1	krytá
hala	hala	k1gFnSc1	hala
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
ozubnicové	ozubnicový	k2eAgFnSc2d1	ozubnicová
dráhy	dráha	k1gFnSc2	dráha
s	s	k7c7	s
potřebnou	potřebný	k2eAgFnSc7d1	potřebná
měnírnou	měnírna	k1gFnSc7	měnírna
el.	el.	k?	el.
proudu	proud	k1gInSc2	proud
na	na	k7c4	na
stejnosměrný	stejnosměrný	k2eAgInSc4d1	stejnosměrný
proud	proud	k1gInSc4	proud
o	o	k7c4	o
napětí	napětí	k1gNnSc4	napětí
1500	[number]	k4	1500
V	V	kA	V
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
dráha	dráha	k1gFnSc1	dráha
využívá	využívat	k5eAaPmIp3nS	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vrcholové	vrcholový	k2eAgFnSc6d1	vrcholová
stanici	stanice	k1gFnSc6	stanice
Štrbské	štrbský	k2eAgNnSc4d1	Štrbské
Pleso	pleso	k1gNnSc4	pleso
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
velká	velký	k2eAgFnSc1d1	velká
výpravna	výpravna	k1gFnSc1	výpravna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jak	jak	k6eAd1	jak
pro	pro	k7c4	pro
TEŽ	TEŽ	kA	TEŽ
tak	tak	k9	tak
pro	pro	k7c4	pro
OŽ	OŽ	kA	OŽ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Štrbského	štrbský	k2eAgNnSc2d1	Štrbské
Plesa	pleso	k1gNnSc2	pleso
byla	být	k5eAaImAgFnS	být
lovecká	lovecký	k2eAgFnSc1d1	lovecká
chata	chata	k1gFnSc1	chata
Jozefa	Jozef	k1gMnSc2	Jozef
Szentiványiho	Szentiványi	k1gMnSc2	Szentiványi
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Štrbského	štrbský	k2eAgNnSc2d1	Štrbské
plesa	pleso	k1gNnSc2	pleso
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
dal	dát	k5eAaPmAgInS	dát
Uherský	uherský	k2eAgInSc1d1	uherský
karpatský	karpatský	k2eAgInSc1d1	karpatský
spolek	spolek	k1gInSc1	spolek
podnět	podnět	k1gInSc1	podnět
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
turistické	turistický	k2eAgFnSc2d1	turistická
ubytovny	ubytovna	k1gFnSc2	ubytovna
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
té	ten	k3xDgFnSc3	ten
zakrátko	zakrátko	k6eAd1	zakrátko
přibyly	přibýt	k5eAaPmAgInP	přibýt
další	další	k2eAgInPc1d1	další
hotely	hotel	k1gInPc1	hotel
a	a	k8xC	a
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
,	,	kIx,	,
lázeňský	lázeňský	k2eAgInSc1d1	lázeňský
dům	dům	k1gInSc1	dům
a	a	k8xC	a
několik	několik	k4yIc1	několik
vilek	vilka	k1gFnPc2	vilka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
obdržela	obdržet	k5eAaPmAgFnS	obdržet
osada	osada	k1gFnSc1	osada
statut	statut	k1gInSc1	statut
léčebných	léčebný	k2eAgFnPc2d1	léčebná
lázní	lázeň	k1gFnPc2	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Štrby	Štrba	k1gFnSc2	Štrba
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
silnice	silnice	k1gFnSc1	silnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c4	o
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Starého	Starého	k2eAgInSc2d1	Starého
Smokovce	Smokovec	k1gInSc2	Smokovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
ozubnicová	ozubnicový	k2eAgFnSc1d1	ozubnicová
železnice	železnice	k1gFnSc1	železnice
paralelně	paralelně	k6eAd1	paralelně
s	s	k7c7	s
cestou	cesta	k1gFnSc7	cesta
do	do	k7c2	do
Štrby	Štrba	k1gFnSc2	Štrba
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
autobusovou	autobusový	k2eAgFnSc7d1	autobusová
dopravou	doprava	k1gFnSc7	doprava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
byly	být	k5eAaImAgFnP	být
postaveny	postavit	k5eAaPmNgInP	postavit
hotely	hotel	k1gInPc7	hotel
Hviezdoslav	Hviezdoslav	k1gMnSc1	Hviezdoslav
a	a	k8xC	a
Kriváň	Kriváň	k1gInSc1	Kriváň
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc4	všechen
tři	tři	k4xCgFnPc4	tři
budovy	budova	k1gFnPc4	budova
součástí	součást	k1gFnPc2	součást
komplexu	komplex	k1gInSc2	komplex
Grand	grand	k1gMnSc1	grand
hotel	hotel	k1gInSc4	hotel
Kempinski	Kempinsk	k1gFnSc2	Kempinsk
Štrbské	štrbský	k2eAgNnSc4d1	Štrbské
Pleso	pleso	k1gNnSc4	pleso
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
vnikl	vniknout	k5eAaPmAgMnS	vniknout
u	u	k7c2	u
břehu	břeh	k1gInSc2	břeh
Nového	Nového	k2eAgNnSc2d1	Nového
Štrbského	štrbský	k2eAgNnSc2d1	Štrbské
plesa	pleso	k1gNnSc2	pleso
např.	např.	kA	např.
i	i	k8xC	i
rodinný	rodinný	k2eAgInSc4d1	rodinný
penzion	penzion	k1gInSc4	penzion
Pleso	pleso	k1gNnSc1	pleso
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
jezdila	jezdit	k5eAaImAgFnS	jezdit
elektrická	elektrický	k2eAgFnSc1d1	elektrická
železnice	železnice	k1gFnSc1	železnice
podél	podél	k7c2	podél
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
Starého	Starého	k2eAgInSc2d1	Starého
Smokovce	Smokovec	k1gInSc2	Smokovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1935	[number]	k4	1935
a	a	k8xC	a
1970	[number]	k4	1970
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
konala	konat	k5eAaImAgNnP	konat
lyžařská	lyžařský	k2eAgNnPc1d1	lyžařské
mistrovství	mistrovství	k1gNnPc1	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byly	být	k5eAaImAgInP	být
hotely	hotel	k1gInPc1	hotel
přeměněny	přeměněn	k2eAgInPc1d1	přeměněn
na	na	k7c4	na
protiastmatická	protiastmatický	k2eAgNnPc4d1	protiastmatický
sanatoria	sanatorium	k1gNnPc4	sanatorium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
mistrovstvím	mistrovství	k1gNnSc7	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
lyžování	lyžování	k1gNnSc6	lyžování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
přibyly	přibýt	k5eAaPmAgFnP	přibýt
nové	nový	k2eAgFnPc1d1	nová
ubytovací	ubytovací	k2eAgFnPc1d1	ubytovací
a	a	k8xC	a
restaurační	restaurační	k2eAgFnPc1d1	restaurační
kapacity	kapacita	k1gFnPc1	kapacita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
také	také	k9	také
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
zubačka	zubačka	k1gFnSc1	zubačka
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Štrba	Štrba	k1gFnSc1	Štrba
<g/>
,	,	kIx,	,
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
a	a	k8xC	a
lyžařský	lyžařský	k2eAgInSc1d1	lyžařský
areál	areál	k1gInSc1	areál
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
Mlynické	Mlynický	k2eAgFnSc2d1	Mlynická
doliny	dolina	k1gFnSc2	dolina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Štrbské	štrbský	k2eAgFnSc2d1	štrbský
Pleso	pleso	k1gNnSc4	pleso
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Mapa	mapa	k1gFnSc1	mapa
Štrbského	štrbský	k2eAgNnSc2d1	Štrbské
Plesa	pleso	k1gNnSc2	pleso
</s>
</p>
