<s>
Štrbské	štrbský	k2eAgNnSc1d1	Štrbské
pleso	pleso	k1gNnSc1	pleso
je	být	k5eAaImIp3nS	být
dostupné	dostupný	k2eAgNnSc1d1	dostupné
jak	jak	k8xC	jak
cestou	cesta	k1gFnSc7	cesta
pro	pro	k7c4	pro
automobily	automobil	k1gInPc4	automobil
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Tatranskou	tatranský	k2eAgFnSc7d1	Tatranská
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
železnicí	železnice	k1gFnSc7	železnice
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
nevšední	všední	k2eNgFnSc7d1	nevšední
ozubnicovou	ozubnicový	k2eAgFnSc7d1	ozubnicová
železnicí	železnice	k1gFnSc7	železnice
<g/>
,	,	kIx,	,
nazývanou	nazývaný	k2eAgFnSc7d1	nazývaná
"	"	kIx"	"
<g/>
Zubačka	zubačka	k1gFnSc1	zubačka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
