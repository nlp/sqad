<p>
<s>
League	League	k1gFnSc1	League
of	of	k?	of
Legends	Legends	k1gInSc1	Legends
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Liga	liga	k1gFnSc1	liga
Legend	legenda	k1gFnPc2	legenda
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
LoL	LoL	k1gFnSc1	LoL
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
žánru	žánr	k1gInSc2	žánr
multiplayer	multiplayra	k1gFnPc2	multiplayra
online	onlinout	k5eAaPmIp3nS	onlinout
battle	battle	k6eAd1	battle
arena	areen	k2eAgFnSc1d1	arena
<g/>
.	.	kIx.	.
</s>
<s>
Vývojářem	vývojář	k1gMnSc7	vývojář
i	i	k8xC	i
hlavním	hlavní	k2eAgMnSc7d1	hlavní
distributorem	distributor	k1gMnSc7	distributor
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
Riot	Riot	k1gMnSc1	Riot
Games	Games	k1gMnSc1	Games
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
Android	android	k1gInSc1	android
<g/>
,	,	kIx,	,
macOS	macOS	k?	macOS
a	a	k8xC	a
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Inspirována	inspirován	k2eAgFnSc1d1	inspirována
byla	být	k5eAaImAgFnS	být
módem	mód	k1gInSc7	mód
Defense	defense	k1gFnSc1	defense
of	of	k?	of
the	the	k?	the
Ancients	Ancients	k1gInSc1	Ancients
(	(	kIx(	(
<g/>
DotA	DotA	k1gFnSc2	DotA
<g/>
)	)	kIx)	)
hry	hra	k1gFnSc2	hra
Warcraft	Warcraft	k1gInSc1	Warcraft
3	[number]	k4	3
společnosti	společnost	k1gFnSc2	společnost
Blizzard	Blizzarda	k1gFnPc2	Blizzarda
Entertainment	Entertainment	k1gMnSc1	Entertainment
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
oznámena	oznámen	k2eAgFnSc1d1	oznámena
dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
a	a	k8xC	a
spuštěna	spuštěn	k2eAgFnSc1d1	spuštěna
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
snížení	snížení	k1gNnSc4	snížení
latence	latence	k1gFnSc2	latence
je	být	k5eAaImIp3nS	být
světová	světový	k2eAgFnSc1d1	světová
herní	herní	k2eAgFnSc1d1	herní
populace	populace	k1gFnSc1	populace
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
geograficky	geograficky	k6eAd1	geograficky
do	do	k7c2	do
asi	asi	k9	asi
tuctu	tucet	k1gInSc2	tucet
regionů	region	k1gInPc2	region
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hráči	hráč	k1gMnPc1	hráč
z	z	k7c2	z
daného	daný	k2eAgInSc2d1	daný
regionu	region	k1gInSc2	region
jsou	být	k5eAaImIp3nP	být
přednosti	přednost	k1gFnPc1	přednost
připojováni	připojovat	k5eAaImNgMnP	připojovat
na	na	k7c4	na
místně	místně	k6eAd1	místně
příslušné	příslušný	k2eAgInPc4d1	příslušný
servery	server	k1gInPc4	server
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
těchto	tento	k3xDgInPc2	tento
serverů	server	k1gInPc2	server
spravuje	spravovat	k5eAaImIp3nS	spravovat
přímo	přímo	k6eAd1	přímo
Riot	Riot	k2eAgInSc1d1	Riot
Games	Games	k1gInSc1	Games
<g/>
,	,	kIx,	,
jen	jen	k9	jen
čínský	čínský	k2eAgMnSc1d1	čínský
provozuje	provozovat	k5eAaImIp3nS	provozovat
Tencent	Tencent	k1gInSc1	Tencent
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
vlastník	vlastník	k1gMnSc1	vlastník
Riot	Riot	k1gMnSc1	Riot
Games	Games	k1gMnSc1	Games
<g/>
)	)	kIx)	)
a	a	k8xC	a
několik	několik	k4yIc4	několik
v	v	k7c6	v
Jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
Garena	Gareno	k1gNnSc2	Gareno
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
spadá	spadat	k5eAaImIp3nS	spadat
pod	pod	k7c4	pod
server	server	k1gInSc4	server
EUNE	EUNE	kA	EUNE
<g/>
,	,	kIx,	,
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
hra	hra	k1gFnSc1	hra
kompletně	kompletně	k6eAd1	kompletně
přeložena	přeložit	k5eAaPmNgFnS	přeložit
a	a	k8xC	a
předabována	předabovat	k5eAaBmNgFnS	předabovat
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přehlednost	přehlednost	k1gFnSc4	přehlednost
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
zde	zde	k6eAd1	zde
uvedeny	uvést	k5eAaPmNgInP	uvést
české	český	k2eAgInPc1d1	český
i	i	k8xC	i
anglické	anglický	k2eAgInPc1d1	anglický
termíny	termín	k1gInPc1	termín
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
herní	herní	k2eAgInSc1d1	herní
slang	slang	k1gInSc1	slang
oba	dva	k4xCgInPc4	dva
jazyky	jazyk	k1gInPc4	jazyk
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnPc4	charakteristikon
hry	hra	k1gFnSc2	hra
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
League	Leagu	k1gInSc2	Leagu
of	of	k?	of
Legends	Legends	k1gInSc1	Legends
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
hráč	hráč	k1gMnSc1	hráč
jako	jako	k8xS	jako
vyvolávač	vyvolávač	k1gMnSc1	vyvolávač
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ovládá	ovládat	k5eAaImIp3nS	ovládat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
zápase	zápas	k1gInSc6	zápas
jediného	jediné	k1gNnSc2	jediné
šampiona	šampion	k1gMnSc2	šampion
s	s	k7c7	s
unikátními	unikátní	k2eAgFnPc7d1	unikátní
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
a	a	k8xC	a
bojuje	bojovat	k5eAaImIp3nS	bojovat
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
týmem	tým	k1gInSc7	tým
proti	proti	k7c3	proti
nepřátelskému	přátelský	k2eNgInSc3d1	nepřátelský
týmu	tým	k1gInSc3	tým
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
mapě	mapa	k1gFnSc6	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Cíl	cíl	k1gInSc1	cíl
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
zničit	zničit	k5eAaPmF	zničit
nepřátelský	přátelský	k2eNgInSc4d1	nepřátelský
Nexus	nexus	k1gInSc4	nexus
<g/>
,	,	kIx,	,
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
konci	konec	k1gInSc6	konec
mapy	mapa	k1gFnPc4	mapa
u	u	k7c2	u
základny	základna	k1gFnSc2	základna
druhého	druhý	k4xOgInSc2	druhý
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
též	též	k9	též
může	moct	k5eAaImIp3nS	moct
skončit	skončit	k5eAaPmF	skončit
kapitulací	kapitulace	k1gFnSc7	kapitulace
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
týmu	tým	k1gInSc6	tým
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
15	[number]	k4	15
minutách	minuta	k1gFnPc6	minuta
(	(	kIx(	(
<g/>
všichni	všechen	k3xTgMnPc1	všechen
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
pro	pro	k7c4	pro
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
po	po	k7c6	po
20	[number]	k4	20
minutách	minuta	k1gFnPc6	minuta
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
pro	pro	k7c4	pro
-	-	kIx~	-
kapitulace	kapitulace	k1gFnSc1	kapitulace
se	se	k3xPyFc4	se
neuzná	uznat	k5eNaPmIp3nS	uznat
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
hlasy	hlas	k1gInPc1	hlas
3	[number]	k4	3
z	z	k7c2	z
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
hrají	hrát	k5eAaImIp3nP	hrát
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
zápasy	zápas	k1gInPc1	zápas
–	–	k?	–
podle	podle	k7c2	podle
režimu	režim	k1gInSc2	režim
trvají	trvat	k5eAaImIp3nP	trvat
20-60	[number]	k4	20-60
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
zápas	zápas	k1gInSc1	zápas
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
začínají	začínat	k5eAaImIp3nP	začínat
šampioni	šampion	k1gMnPc1	šampion
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
schopností	schopnost	k1gFnSc7	schopnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
si	se	k3xPyFc3	se
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
vylepšují	vylepšovat	k5eAaImIp3nP	vylepšovat
a	a	k8xC	a
zpřístupňují	zpřístupňovat	k5eAaImIp3nP	zpřístupňovat
si	se	k3xPyFc3	se
další	další	k2eAgMnPc1d1	další
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
4	[number]	k4	4
unikátní	unikátní	k2eAgFnSc2d1	unikátní
schopnosti	schopnost	k1gFnSc2	schopnost
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkami	výjimka	k1gFnPc7	výjimka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
hry	hra	k1gFnSc2	hra
Level	level	k1gInSc1	level
1	[number]	k4	1
a	a	k8xC	a
své	svůj	k3xOyFgFnPc4	svůj
schopnosti	schopnost	k1gFnPc4	schopnost
má	mít	k5eAaImIp3nS	mít
všechny	všechen	k3xTgMnPc4	všechen
plně	plně	k6eAd1	plně
vyvinuté	vyvinutý	k2eAgMnPc4d1	vyvinutý
až	až	k6eAd1	až
v	v	k7c6	v
pozdní	pozdní	k2eAgFnSc6d1	pozdní
fázi	fáze	k1gFnSc6	fáze
hry	hra	k1gFnSc2	hra
na	na	k7c6	na
Levelu	level	k1gInSc6	level
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
postup	postup	k1gInSc4	postup
do	do	k7c2	do
dalšího	další	k2eAgInSc2d1	další
levelu	level	k1gInSc2	level
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
sbírat	sbírat	k5eAaImF	sbírat
tzv.	tzv.	kA	tzv.
expy	expo	k1gNnPc7	expo
–	–	k?	–
ty	ten	k3xDgMnPc4	ten
se	se	k3xPyFc4	se
získávají	získávat	k5eAaImIp3nP	získávat
pouhým	pouhý	k2eAgInSc7d1	pouhý
pobytem	pobyt	k1gInSc7	pobyt
na	na	k7c6	na
lajně	lajna	k1gFnSc6	lajna
<g/>
,	,	kIx,	,
zabíjením	zabíjení	k1gNnSc7	zabíjení
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
šampionů	šampion	k1gMnPc2	šampion
nebo	nebo	k8xC	nebo
asistencí	asistence	k1gFnPc2	asistence
na	na	k7c4	na
zabití	zabití	k1gNnSc4	zabití
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
do	do	k7c2	do
dalšího	další	k2eAgInSc2d1	další
levelu	level	k1gInSc2	level
se	se	k3xPyFc4	se
jednak	jednak	k8xC	jednak
zpřístupní	zpřístupnit	k5eAaPmIp3nS	zpřístupnit
možnost	možnost	k1gFnSc4	možnost
aktivovat	aktivovat	k5eAaBmF	aktivovat
<g/>
/	/	kIx~	/
<g/>
vylepšit	vylepšit	k5eAaPmF	vylepšit
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
postavě	postava	k1gFnSc3	postava
stoupnou	stoupnout	k5eAaPmIp3nP	stoupnout
tzv.	tzv.	kA	tzv.
staty	status	k1gInPc4	status
–	–	k?	–
hodnoty	hodnota	k1gFnSc2	hodnota
brnění	brnění	k1gNnSc2	brnění
<g/>
,	,	kIx,	,
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
,	,	kIx,	,
many	mana	k1gFnSc2	mana
atd.	atd.	kA	atd.
Snaha	snaha	k1gFnSc1	snaha
mít	mít	k5eAaImF	mít
co	co	k3yInSc4	co
nejrychleji	rychle	k6eAd3	rychle
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
levelů	level	k1gInPc2	level
zajištující	zajištující	k2eAgInSc4d1	zajištující
náskok	náskok	k1gInSc4	náskok
před	před	k7c7	před
protihráči	protihráč	k1gMnPc7	protihráč
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgNnSc1d1	základní
mechanikou	mechanika	k1gFnSc7	mechanika
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
hru	hra	k1gFnSc4	hra
typu	typ	k1gInSc2	typ
freemium	freemium	k1gNnSc4	freemium
–	–	k?	–
za	za	k7c4	za
reálné	reálný	k2eAgInPc4d1	reálný
peníze	peníz	k1gInPc4	peníz
lze	lze	k6eAd1	lze
koupit	koupit	k5eAaPmF	koupit
například	například	k6eAd1	například
skiny	skin	k1gMnPc4	skin
k	k	k7c3	k
postavám	postava	k1gFnPc3	postava
nebo	nebo	k8xC	nebo
samotné	samotný	k2eAgFnPc4d1	samotná
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
ale	ale	k8xC	ale
žádným	žádný	k3yNgInSc7	žádný
způsobem	způsob	k1gInSc7	způsob
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
průběh	průběh	k1gInSc4	průběh
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
výhru	výhra	k1gFnSc4	výhra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyvolávač	vyvolávač	k1gMnSc1	vyvolávač
může	moct	k5eAaImIp3nS	moct
hrát	hrát	k5eAaImF	hrát
za	za	k7c4	za
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
postavu	postava	k1gFnSc4	postava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
má	mít	k5eAaImIp3nS	mít
koupenou	koupený	k2eAgFnSc4d1	koupená
anebo	anebo	k8xC	anebo
za	za	k7c2	za
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
hra	hra	k1gFnSc1	hra
zdarma	zdarma	k6eAd1	zdarma
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
bezplatné	bezplatný	k2eAgFnSc6d1	bezplatná
týdenní	týdenní	k2eAgFnSc6d1	týdenní
rotaci	rotace	k1gFnSc6	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
143	[number]	k4	143
jedinečných	jedinečný	k2eAgFnPc2d1	jedinečná
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
postava	postava	k1gFnSc1	postava
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
svůj	svůj	k3xOyFgInSc4	svůj
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
určitého	určitý	k2eAgInSc2d1	určitý
regionu	region	k1gInSc2	region
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
svoje	svůj	k3xOyFgNnSc4	svůj
4	[number]	k4	4
unikátní	unikátní	k2eAgFnPc4d1	unikátní
schopnosti	schopnost	k1gFnPc4	schopnost
+	+	kIx~	+
pasivní	pasivní	k2eAgFnSc4d1	pasivní
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
(	(	kIx(	(
<g/>
i	i	k9	i
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
postavy	postava	k1gFnPc1	postava
hrají	hrát	k5eAaImIp3nP	hrát
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
regionu	region	k1gInSc2	region
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
příbuzenském	příbuzenský	k2eAgInSc6d1	příbuzenský
vztahu	vztah	k1gInSc6	vztah
<g/>
,	,	kIx,	,
mívají	mívat	k5eAaImIp3nP	mívat
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
hlášky	hláška	k1gFnSc2	hláška
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k8xC	i
posílené	posílený	k2eAgFnPc4d1	posílená
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Strategie	strategie	k1gFnPc1	strategie
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
základní	základní	k2eAgFnPc4d1	základní
strategie	strategie	k1gFnPc4	strategie
výhry	výhra	k1gFnSc2	výhra
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgNnPc2d1	ostatní
MOBA	MOBA	kA	MOBA
her	hra	k1gFnPc2	hra
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zachování	zachování	k1gNnSc1	zachování
chladné	chladný	k2eAgFnSc2d1	chladná
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Agresivní	agresivní	k2eAgMnPc1d1	agresivní
hráči	hráč	k1gMnPc1	hráč
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
nahlašováni	nahlašován	k2eAgMnPc1d1	nahlašován
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
jim	on	k3xPp3gMnPc3	on
být	být	k5eAaImF	být
odepřen	odepřít	k5eAaPmNgInS	odepřít
přístup	přístup	k1gInSc1	přístup
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mít	mít	k5eAaImF	mít
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
tzv.	tzv.	kA	tzv.
vizi	vize	k1gFnSc4	vize
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
mapa	mapa	k1gFnSc1	mapa
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
válečnou	válečný	k2eAgFnSc7d1	válečná
mlhou	mlha	k1gFnSc7	mlha
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Opar	opar	k1gInSc1	opar
neznáma	neznámo	k1gNnSc2	neznámo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
hráči	hráč	k1gMnPc1	hráč
nebyli	být	k5eNaImAgMnP	být
překvapeni	překvapit	k5eAaPmNgMnP	překvapit
výpady	výpad	k1gInPc7	výpad
ze	z	k7c2	z
tmy	tma	k1gFnSc2	tma
<g/>
,	,	kIx,	,
pokládají	pokládat	k5eAaImIp3nP	pokládat
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
totemy	totem	k1gInPc4	totem
(	(	kIx(	(
<g/>
wardy	warda	k1gFnPc4	warda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
oblast	oblast	k1gFnSc4	oblast
odhalí	odhalit	k5eAaPmIp3nS	odhalit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Týmová	týmový	k2eAgFnSc1d1	týmová
spolupráce	spolupráce	k1gFnSc1	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
spolu	spolu	k6eAd1	spolu
hráči	hráč	k1gMnPc1	hráč
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
např.	např.	kA	např.
přes	přes	k7c4	přes
Discord	Discord	k1gInSc4	Discord
nebo	nebo	k8xC	nebo
TeamSpeak	TeamSpeak	k1gInSc4	TeamSpeak
mají	mít	k5eAaImIp3nP	mít
jiný	jiný	k2eAgInSc4d1	jiný
charakter	charakter	k1gInSc4	charakter
než	než	k8xS	než
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
s	s	k7c7	s
neznámými	známý	k2eNgMnPc7d1	neznámý
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
domluva	domluva	k1gFnSc1	domluva
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
přes	přes	k7c4	přes
chat	chata	k1gFnPc2	chata
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
nelze	lze	k6eNd1	lze
vystupovat	vystupovat	k5eAaImF	vystupovat
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
za	za	k7c2	za
jednoho	jeden	k4xCgMnSc2	jeden
ale	ale	k8xC	ale
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k8xS	jako
tým	tým	k1gInSc1	tým
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
schopnosti	schopnost	k1gFnPc1	schopnost
různých	různý	k2eAgMnPc2d1	různý
šampionů	šampion	k1gMnPc2	šampion
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
různě	různě	k6eAd1	různě
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
například	například	k6eAd1	například
pomohou	pomoct	k5eAaPmIp3nP	pomoct
doplňování	doplňování	k1gNnSc3	doplňování
zdraví	zdraví	k1gNnSc3	zdraví
(	(	kIx(	(
<g/>
healování	healování	k1gNnSc3	healování
<g/>
)	)	kIx)	)
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
jen	jen	k9	jen
pár	pár	k4xCyI	pár
dobře	dobře	k6eAd1	dobře
domluvených	domluvený	k2eAgInPc2d1	domluvený
okamžiků	okamžik	k1gInPc2	okamžik
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c6	o
výsledku	výsledek	k1gInSc6	výsledek
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znalost	znalost	k1gFnSc1	znalost
všech	všecek	k3xTgMnPc2	všecek
šampionů	šampion	k1gMnPc2	šampion
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc2	jejich
kouzel	kouzlo	k1gNnPc2	kouzlo
</s>
</p>
<p>
<s>
Nákup	nákup	k1gInSc1	nákup
správných	správný	k2eAgInPc2d1	správný
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
je	být	k5eAaImIp3nS	být
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přidávají	přidávat	k5eAaImIp3nP	přidávat
šampionovi	šampion	k1gMnSc3	šampion
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
funkčnost	funkčnost	k1gFnSc4	funkčnost
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
například	například	k6eAd1	například
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
poškození	poškození	k1gNnPc1	poškození
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
přidávají	přidávat	k5eAaImIp3nP	přidávat
hodnotu	hodnota	k1gFnSc4	hodnota
brnění	brnění	k1gNnSc3	brnění
<g/>
,	,	kIx,	,
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
hodnotu	hodnota	k1gFnSc4	hodnota
léčení	léčení	k1gNnSc2	léčení
<g/>
,	,	kIx,	,
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
štíty	štít	k1gInPc1	štít
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
===	===	k?	===
Ocenění	ocenění	k1gNnSc1	ocenění
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
hry	hra	k1gFnSc2	hra
mohou	moct	k5eAaImIp3nP	moct
hráči	hráč	k1gMnPc1	hráč
získat	získat	k5eAaPmF	získat
ocenění	ocenění	k1gNnSc4	ocenění
několika	několik	k4yIc2	několik
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
za	za	k7c4	za
každé	každý	k3xTgNnSc4	každý
ocenění	ocenění	k1gNnSc4	ocenění
navíc	navíc	k6eAd1	navíc
dostanou	dostat	k5eAaPmIp3nP	dostat
odměnu	odměna	k1gFnSc4	odměna
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
zlaťáků	zlaťák	k1gInPc2	zlaťák
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
zničená	zničený	k2eAgFnSc1d1	zničená
věž	věž	k1gFnSc1	věž
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
prolitá	prolitý	k2eAgFnSc1d1	prolitá
krev	krev	k1gFnSc1	krev
</s>
</p>
<p>
<s>
Série	série	k1gFnSc1	série
zabití	zabití	k1gNnSc2	zabití
beze	beze	k7c2	beze
smrti	smrt	k1gFnSc2	smrt
</s>
</p>
<p>
<s>
Zabití	zabití	k1gNnSc1	zabití
více	hodně	k6eAd2	hodně
nepřátel	nepřítel	k1gMnPc2	nepřítel
najednou	najednou	k6eAd1	najednou
(	(	kIx(	(
<g/>
v	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
časovém	časový	k2eAgInSc6d1	časový
úseku	úsek	k1gInSc6	úsek
<g/>
)	)	kIx)	)
–	–	k?	–
dvoj	dvojit	k5eAaImRp2nS	dvojit
<g/>
/	/	kIx~	/
<g/>
troj	trojit	k5eAaImRp2nS	trojit
<g/>
/	/	kIx~	/
<g/>
čtyř	čtyři	k4xCgMnPc2	čtyři
<g/>
/	/	kIx~	/
<g/>
pětizářez	pětizářeza	k1gFnPc2	pětizářeza
</s>
</p>
<p>
<s>
Zabití	zabití	k1gNnSc1	zabití
posledního	poslední	k2eAgMnSc2d1	poslední
živého	živý	k2eAgMnSc2d1	živý
nepřítele	nepřítel	k1gMnSc2	nepřítel
–	–	k?	–
"	"	kIx"	"
<g/>
Eso	eso	k1gNnSc1	eso
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Herní	herní	k2eAgFnSc2d1	herní
módy	móda	k1gFnSc2	móda
==	==	k?	==
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
hráč	hráč	k1gMnSc1	hráč
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
hrát	hrát	k5eAaImF	hrát
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
si	se	k3xPyFc3	se
zvolit	zvolit	k5eAaPmF	zvolit
z	z	k7c2	z
několika	několik	k4yIc2	několik
herních	herní	k2eAgInPc2d1	herní
režimů	režim	k1gInPc2	režim
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
z	z	k7c2	z
několika	několik	k4yIc2	několik
map	mapa	k1gFnPc2	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
League	League	k1gFnSc6	League
of	of	k?	of
Legends	Legendsa	k1gFnPc2	Legendsa
pojmenovány	pojmenovat	k5eAaPmNgFnP	pojmenovat
Pole	pole	k1gFnPc1	pole
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
(	(	kIx(	(
<g/>
Fields	Fields	k1gInSc1	Fields
of	of	k?	of
Justice	justice	k1gFnSc2	justice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
linií	linie	k1gFnPc2	linie
a	a	k8xC	a
základen	základna	k1gFnPc2	základna
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
také	také	k9	také
tzv.	tzv.	kA	tzv.
džungle	džungle	k1gFnSc1	džungle
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
neutrální	neutrální	k2eAgFnPc1d1	neutrální
nestvůry	nestvůra	k1gFnPc1	nestvůra
<g/>
;	;	kIx,	;
některé	některý	k3yIgNnSc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
po	po	k7c6	po
zabití	zabití	k1gNnSc4	zabití
dávají	dávat	k5eAaImIp3nP	dávat
buff	buffa	k1gFnPc2	buffa
neboli	neboli	k8xC	neboli
posilnění	posilnění	k1gNnSc2	posilnění
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyvolávačův	vyvolávačův	k2eAgInSc1d1	vyvolávačův
žleb	žleb	k1gInSc1	žleb
(	(	kIx(	(
<g/>
Summoners	Summoners	k1gInSc1	Summoners
rift	rift	k1gInSc1	rift
<g/>
)	)	kIx)	)
–	–	k?	–
mapa	mapa	k1gFnSc1	mapa
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
podle	podle	k7c2	podle
DotA	DotA	k1gFnSc2	DotA
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc1	tři
linie	linie	k1gFnPc1	linie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
pět	pět	k4xCc4	pět
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
časových	časový	k2eAgNnPc6d1	časové
obdobích	období	k1gNnPc6	období
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
různé	různý	k2eAgInPc1d1	různý
designy	design	k1gInPc1	design
<g/>
,	,	kIx,	,
např.	např.	kA	např.
letní	letní	k2eAgFnSc1d1	letní
<g/>
,	,	kIx,	,
zimní	zimní	k2eAgFnSc1d1	zimní
<g/>
,	,	kIx,	,
halloweenský	halloweenský	k2eAgInSc1d1	halloweenský
a	a	k8xC	a
vánoční	vánoční	k2eAgInSc1d1	vánoční
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
předělána	předělat	k5eAaPmNgFnS	předělat
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
vizáže	vizáž	k1gFnSc2	vizáž
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
30-45	[number]	k4	30-45
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokroucená	pokroucený	k2eAgFnSc1d1	pokroucená
alej	alej	k1gFnSc1	alej
(	(	kIx(	(
<g/>
Twisted	Twisted	k1gInSc1	Twisted
Treeline	Treelin	k1gInSc5	Treelin
<g/>
)	)	kIx)	)
–	–	k?	–
mapa	mapa	k1gFnSc1	mapa
pro	pro	k7c4	pro
tři	tři	k4xCgMnPc4	tři
hráče	hráč	k1gMnPc4	hráč
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
dvě	dva	k4xCgFnPc1	dva
linie	linie	k1gFnPc1	linie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
pouze	pouze	k6eAd1	pouze
letní	letní	k2eAgInSc4d1	letní
design	design	k1gInSc4	design
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
kompletně	kompletně	k6eAd1	kompletně
předělána	předělán	k2eAgFnSc1d1	předělána
(	(	kIx(	(
<g/>
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
známá	známý	k2eAgFnSc1d1	známá
i	i	k8xC	i
jako	jako	k8xS	jako
Shadow	Shadow	k1gMnSc1	Shadow
Isles	Isles	k1gMnSc1	Isles
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
20-30	[number]	k4	20-30
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Křišťálová	křišťálový	k2eAgFnSc1d1	Křišťálová
jizva	jizva	k1gFnSc1	jizva
(	(	kIx(	(
<g/>
Crystal	Crystal	k1gMnSc1	Crystal
Scar	Scar	k1gMnSc1	Scar
<g/>
)	)	kIx)	)
–	–	k?	–
mapa	mapa	k1gFnSc1	mapa
pro	pro	k7c4	pro
pět	pět	k4xCc4	pět
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
ovládat	ovládat	k5eAaImF	ovládat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Mapa	mapa	k1gFnSc1	mapa
se	se	k3xPyFc4	se
zrušila	zrušit	k5eAaPmAgFnS	zrušit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
šesté	šestý	k4xOgFnSc2	šestý
sezóny	sezóna	k1gFnSc2	sezóna
kvůli	kvůli	k7c3	kvůli
malé	malý	k2eAgFnSc3d1	malá
oblíbenosti	oblíbenost	k1gFnSc3	oblíbenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proving	Proving	k1gInSc1	Proving
Grounds	Grounds	k1gInSc1	Grounds
-	-	kIx~	-
malá	malý	k2eAgFnSc1d1	malá
mapa	mapa	k1gFnSc1	mapa
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
pro	pro	k7c4	pro
tutorial	tutorial	k1gInSc4	tutorial
a	a	k8xC	a
All	All	k1gFnSc1	All
random	random	k1gInSc1	random
all	all	k?	all
mid	mid	k?	mid
(	(	kIx(	(
<g/>
ARAM	Aram	k1gMnSc1	Aram
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Zrušena	zrušen	k2eAgFnSc1d1	zrušena
<g/>
,	,	kIx,	,
přeměněna	přeměněn	k2eAgFnSc1d1	přeměněna
na	na	k7c4	na
Howling	Howling	k1gInSc4	Howling
Abyss	Abyss	k1gInSc4	Abyss
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kvílející	kvílející	k2eAgFnSc1d1	kvílející
propast	propast	k1gFnSc1	propast
(	(	kIx(	(
<g/>
Howling	Howling	k1gInSc1	Howling
abyss	abyss	k1gInSc1	abyss
<g/>
)	)	kIx)	)
–	–	k?	–
mapa	mapa	k1gFnSc1	mapa
podobná	podobný	k2eAgFnSc1d1	podobná
Proving	Proving	k1gInSc4	Proving
grounds	groundsa	k1gFnPc2	groundsa
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
grafickém	grafický	k2eAgInSc6d1	grafický
kabátě	kabát	k1gInSc6	kabát
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
efekty	efekt	k1gInPc7	efekt
etc	etc	k?	etc
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
mód	mód	k1gInSc4	mód
ARAM	Aram	k1gMnSc1	Aram
a	a	k8xC	a
Tutorial	Tutorial	k1gMnSc1	Tutorial
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
20-30	[number]	k4	20-30
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Magma	magma	k1gNnSc1	magma
Chamber	Chamber	k1gMnSc1	Chamber
–	–	k?	–
podle	podle	k7c2	podle
původních	původní	k2eAgFnPc2d1	původní
informací	informace	k1gFnPc2	informace
velká	velká	k1gFnSc1	velká
6	[number]	k4	6
<g/>
v	v	k7c4	v
<g/>
6	[number]	k4	6
mapa	mapa	k1gFnSc1	mapa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
All-Star	All-Star	k1gInSc4	All-Star
šampionátu	šampionát	k1gInSc2	šampionát
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
třetí	třetí	k4xOgFnSc2	třetí
sezóny	sezóna	k1gFnSc2	sezóna
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
její	její	k3xOp3gFnSc1	její
část	část	k1gFnSc1	část
jako	jako	k9	jako
1	[number]	k4	1
<g/>
v	v	k7c4	v
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
v	v	k7c4	v
<g/>
2	[number]	k4	2
mapa	mapa	k1gFnSc1	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Očekávalo	očekávat	k5eAaImAgNnS	očekávat
se	se	k3xPyFc4	se
její	její	k3xOp3gNnSc1	její
uvedení	uvedení	k1gNnSc1	uvedení
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
sezóny	sezóna	k1gFnSc2	sezóna
4	[number]	k4	4
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Riot	Riot	k2eAgInSc4d1	Riot
Games	Games	k1gInSc4	Games
přestali	přestat	k5eAaPmAgMnP	přestat
s	s	k7c7	s
jejím	její	k3xOp3gInSc7	její
vývojem	vývoj	k1gInSc7	vývoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stálé	stálý	k2eAgFnSc2d1	stálá
módy	móda	k1gFnSc2	móda
===	===	k?	===
</s>
</p>
<p>
<s>
Tutorial	Tutorial	k1gInSc1	Tutorial
(	(	kIx(	(
<g/>
Úvod	úvod	k1gInSc1	úvod
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
Klasický	klasický	k2eAgInSc4d1	klasický
úvod	úvod	k1gInSc4	úvod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
naprosté	naprostý	k2eAgMnPc4d1	naprostý
začátečníky	začátečník	k1gMnPc4	začátečník
a	a	k8xC	a
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
úplně	úplně	k6eAd1	úplně
základní	základní	k2eAgFnPc4d1	základní
herní	herní	k2eAgFnPc4d1	herní
mechaniky	mechanika	k1gFnPc4	mechanika
(	(	kIx(	(
<g/>
pohyb	pohyb	k1gInSc1	pohyb
<g/>
,	,	kIx,	,
útoky	útok	k1gInPc1	útok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Válečný	válečný	k2eAgInSc1d1	válečný
trénink	trénink	k1gInSc1	trénink
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hráč	hráč	k1gMnSc1	hráč
naučí	naučit	k5eAaPmIp3nS	naučit
pravidla	pravidlo	k1gNnPc4	pravidlo
a	a	k8xC	a
ovládání	ovládání	k1gNnSc4	ovládání
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Custom	Custom	k1gInSc1	Custom
game	game	k1gInSc1	game
(	(	kIx(	(
<g/>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
hra	hra	k1gFnSc1	hra
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc4	typ
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
zakládají	zakládat	k5eAaImIp3nP	zakládat
sami	sám	k3xTgMnPc1	sám
hráči	hráč	k1gMnPc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Custom	Custom	k1gInSc1	Custom
Games	Games	k1gMnSc1	Games
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Tournament	Tournament	k1gInSc1	Tournament
Draft	draft	k1gInSc1	draft
(	(	kIx(	(
<g/>
turnajový	turnajový	k2eAgInSc1d1	turnajový
draft	draft	k1gInSc1	draft
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
možnost	možnost	k1gFnSc4	možnost
zastavení	zastavení	k1gNnPc2	zastavení
hry	hra	k1gFnSc2	hra
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
turnajové	turnajový	k2eAgNnSc4d1	turnajové
hraní	hraní	k1gNnSc4	hraní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Normal	Normal	k1gInSc1	Normal
game	game	k1gInSc1	game
(	(	kIx(	(
<g/>
Normální	normální	k2eAgFnSc1d1	normální
hra	hra	k1gFnSc1	hra
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
hráč	hráč	k1gMnSc1	hráč
dostává	dostávat	k5eAaImIp3nS	dostávat
obvyklý	obvyklý	k2eAgInSc4d1	obvyklý
počet	počet	k1gInSc4	počet
ME	ME	kA	ME
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
buď	buď	k8xC	buď
připojit	připojit	k5eAaPmF	připojit
sám	sám	k3xTgMnSc1	sám
(	(	kIx(	(
<g/>
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
mu	on	k3xPp3gMnSc3	on
hra	hra	k1gFnSc1	hra
najde	najít	k5eAaPmIp3nS	najít
zbytek	zbytek	k1gInSc4	zbytek
týmu	tým	k1gInSc2	tým
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
připojí	připojit	k5eAaPmIp3nS	připojit
s	s	k7c7	s
až	až	k6eAd1	až
čtyřmi	čtyři	k4xCgInPc7	čtyři
předem	předem	k6eAd1	předem
vybranými	vybraný	k2eAgMnPc7d1	vybraný
spoluhráči	spoluhráč	k1gMnPc7	spoluhráč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ranked	Ranked	k1gInSc1	Ranked
game	game	k1gInSc1	game
(	(	kIx(	(
<g/>
Bodovaná	bodovaný	k2eAgFnSc1d1	bodovaná
hra	hra	k1gFnSc1	hra
<g/>
)	)	kIx)	)
probíhá	probíhat	k5eAaImIp3nS	probíhat
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Normální	normální	k2eAgFnSc1d1	normální
hra	hra	k1gFnSc1	hra
s	s	k7c7	s
draftem	draft	k1gInSc7	draft
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
ji	on	k3xPp3gFnSc4	on
hrát	hrát	k5eAaImF	hrát
pouze	pouze	k6eAd1	pouze
hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
třicáté	třicátý	k4xOgFnPc4	třicátý
úrovně	úroveň	k1gFnPc4	úroveň
a	a	k8xC	a
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
aspoň	aspoň	k9	aspoň
20	[number]	k4	20
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ME	ME	kA	ME
získávají	získávat	k5eAaImIp3nP	získávat
hráči	hráč	k1gMnPc1	hráč
také	také	k9	také
LP	LP	kA	LP
(	(	kIx(	(
<g/>
League	League	k1gFnSc1	League
Points	Pointsa	k1gFnPc2	Pointsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
výhru	výhra	k1gFnSc4	výhra
je	on	k3xPp3gFnPc4	on
získávají	získávat	k5eAaImIp3nP	získávat
<g/>
,	,	kIx,	,
za	za	k7c4	za
prohru	prohra	k1gFnSc4	prohra
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Co-op	Cop	k1gInSc1	Co-op
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
AI	AI	kA	AI
(	(	kIx(	(
<g/>
Hráči	hráč	k1gMnSc3	hráč
vs	vs	k?	vs
<g/>
.	.	kIx.	.
umělá	umělý	k2eAgFnSc1d1	umělá
inteligence	inteligence	k1gFnSc1	inteligence
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
typ	typ	k1gInSc1	typ
jako	jako	k8xC	jako
Normální	normální	k2eAgFnSc1d1	normální
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
proti	proti	k7c3	proti
'	'	kIx"	'
<g/>
lidskému	lidský	k2eAgInSc3d1	lidský
<g/>
'	'	kIx"	'
týmu	tým	k1gInSc2	tým
stojí	stát	k5eAaImIp3nS	stát
počítačem	počítač	k1gInSc7	počítač
ovládaní	ovládaný	k2eAgMnPc1d1	ovládaný
boti	bot	k1gMnPc1	bot
s	s	k7c7	s
nastavitelnou	nastavitelný	k2eAgFnSc7d1	nastavitelná
inteligencí	inteligence	k1gFnSc7	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
hry	hra	k1gFnSc2	hra
lze	lze	k6eAd1	lze
hrát	hrát	k5eAaImF	hrát
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
map	mapa	k1gFnPc2	mapa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ARAM	Aram	k1gInSc1	Aram
(	(	kIx(	(
<g/>
All	All	k1gFnSc1	All
Random	Random	k1gInSc1	Random
All	All	k1gMnSc1	All
Mid	Mid	k1gMnSc1	Mid
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nově	nově	k6eAd1	nově
uznaný	uznaný	k2eAgInSc1d1	uznaný
typ	typ	k1gInSc1	typ
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
hrával	hrávat	k5eAaImAgMnS	hrávat
jako	jako	k9	jako
custom	custom	k1gInSc4	custom
game	game	k1gInSc1	game
<g/>
.	.	kIx.	.
2	[number]	k4	2
týmy	tým	k1gInPc4	tým
po	po	k7c6	po
pěti	pět	k4xCc6	pět
hráčích	hráč	k1gMnPc6	hráč
se	se	k3xPyFc4	se
střetávají	střetávat	k5eAaImIp3nP	střetávat
na	na	k7c6	na
nové	nový	k2eAgFnSc6d1	nová
mapě	mapa	k1gFnSc6	mapa
"	"	kIx"	"
<g/>
Howling	Howling	k1gInSc1	Howling
Abyss	Abyss	k1gInSc1	Abyss
<g/>
"	"	kIx"	"
s	s	k7c7	s
náhodně	náhodně	k6eAd1	náhodně
vybranými	vybraný	k2eAgMnPc7d1	vybraný
hrdiny	hrdina	k1gMnPc7	hrdina
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
linii	linie	k1gFnSc6	linie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
hře	hra	k1gFnSc6	hra
se	se	k3xPyFc4	se
v	v	k7c6	v
základně	základna	k1gFnSc6	základna
neobnovuje	obnovovat	k5eNaImIp3nS	obnovovat
zdraví	zdraví	k1gNnSc4	zdraví
a	a	k8xC	a
lze	lze	k6eAd1	lze
nakupovat	nakupovat	k5eAaBmF	nakupovat
až	až	k9	až
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Practice	Practice	k1gFnSc1	Practice
tool	toola	k1gFnPc2	toola
Mód	móda	k1gFnPc2	móda
určený	určený	k2eAgInSc4d1	určený
k	k	k7c3	k
tréninku	trénink	k1gInSc3	trénink
<g/>
,	,	kIx,	,
hráč	hráč	k1gMnSc1	hráč
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
nastavit	nastavit	k5eAaPmF	nastavit
například	například	k6eAd1	například
vypnutí	vypnutí	k1gNnSc4	vypnutí
přebíjecích	přebíjecí	k2eAgInPc2d1	přebíjecí
časů	čas	k1gInPc2	čas
u	u	k7c2	u
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
neomezené	omezený	k2eNgFnPc1d1	neomezená
finance	finance	k1gFnPc1	finance
<g/>
,	,	kIx,	,
či	či	k8xC	či
neomezenou	omezený	k2eNgFnSc4d1	neomezená
zásobu	zásoba	k1gFnSc4	zásoba
many	mana	k1gFnSc2	mana
a	a	k8xC	a
trénovat	trénovat	k5eAaImF	trénovat
postavy	postava	k1gFnPc4	postava
a	a	k8xC	a
herní	herní	k2eAgInPc4d1	herní
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rotující	rotující	k2eAgFnSc2d1	rotující
módy	móda	k1gFnSc2	móda
===	===	k?	===
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
módy	mód	k1gInPc1	mód
nejsou	být	k5eNaImIp3nP	být
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
stále	stále	k6eAd1	stále
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rotují	rotovat	k5eAaImIp3nP	rotovat
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
zpřístupněn	zpřístupnit	k5eAaPmNgInS	zpřístupnit
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
níže	nízce	k6eAd2	nízce
uvedených	uvedený	k2eAgInPc2d1	uvedený
módů	mód	k1gInPc2	mód
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jednoho	jeden	k4xCgInSc2	jeden
víkendu	víkend	k1gInSc2	víkend
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
módy	móda	k1gFnPc1	móda
jsou	být	k5eAaImIp3nP	být
zpřístupněny	zpřístupnit	k5eAaPmNgFnP	zpřístupnit
i	i	k9	i
na	na	k7c4	na
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
při	při	k7c6	při
různých	různý	k2eAgFnPc6d1	různá
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Legend	legenda	k1gFnPc2	legenda
of	of	k?	of
the	the	k?	the
Poro	Poro	k1gMnSc1	Poro
King	King	k1gMnSc1	King
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Vánočních	vánoční	k2eAgInPc2d1	vánoční
svátků	svátek	k1gInPc2	svátek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
One	One	k?	One
for	forum	k1gNnPc2	forum
all	all	k?	all
(	(	kIx(	(
<g/>
Jeden	jeden	k4xCgMnSc1	jeden
za	za	k7c4	za
všechny	všechen	k3xTgFnPc4	všechen
<g/>
)	)	kIx)	)
-	-	kIx~	-
Každý	každý	k3xTgInSc1	každý
tým	tým	k1gInSc1	tým
zvolí	zvolit	k5eAaPmIp3nP	zvolit
jednoho	jeden	k4xCgMnSc4	jeden
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Hrdina	Hrdina	k1gMnSc1	Hrdina
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
nejvíckrát	jvíckrát	k6eNd1	jvíckrát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
přidělen	přidělen	k2eAgInSc1d1	přidělen
celému	celý	k2eAgInSc3d1	celý
týmu	tým	k1gInSc3	tým
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
remízy	remíza	k1gFnSc2	remíza
je	být	k5eAaImIp3nS	být
vybrán	vybrat	k5eAaPmNgInS	vybrat
losem	los	k1gInSc7	los
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
ale	ale	k9	ale
probíhá	probíhat	k5eAaImIp3nS	probíhat
banování	banování	k1gNnSc4	banování
nepříjemných	příjemný	k2eNgFnPc2d1	nepříjemná
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Showdown	Showdown	k1gNnSc1	Showdown
(	(	kIx(	(
<g/>
Měření	měření	k1gNnSc1	měření
sil	síla	k1gFnPc2	síla
<g/>
)	)	kIx)	)
-	-	kIx~	-
1	[number]	k4	1
<g/>
v	v	k7c4	v
<g/>
1	[number]	k4	1
nebo	nebo	k8xC	nebo
2	[number]	k4	2
<g/>
v	v	k7c4	v
<g/>
2	[number]	k4	2
mód	móda	k1gFnPc2	móda
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
Howling	Howling	k1gInSc1	Howling
Abyss	Abyssa	k1gFnPc2	Abyssa
<g/>
.	.	kIx.	.
</s>
<s>
Mapa	mapa	k1gFnSc1	mapa
byla	být	k5eAaImAgFnS	být
funkční	funkční	k2eAgFnSc1d1	funkční
během	během	k7c2	během
Vánoc	Vánoce	k1gFnPc2	Vánoce
2013	[number]	k4	2013
a	a	k8xC	a
věří	věřit	k5eAaImIp3nS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
jako	jako	k8xC	jako
veřejné	veřejný	k2eAgNnSc1d1	veřejné
testování	testování	k1gNnSc1	testování
pro	pro	k7c4	pro
příchod	příchod	k1gInSc4	příchod
mapy	mapa	k1gFnSc2	mapa
Magma	magma	k1gNnSc1	magma
Chamber	Chamber	k1gInSc1	Chamber
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nízké	nízký	k2eAgFnSc3d1	nízká
účasti	účast	k1gFnSc3	účast
hráčů	hráč	k1gMnPc2	hráč
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
modu	modus	k1gInSc6	modus
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
vývoj	vývoj	k1gInSc1	vývoj
Magma	magma	k1gNnSc1	magma
Chamber	Chambra	k1gFnPc2	Chambra
pozastaven	pozastavit	k5eAaPmNgMnS	pozastavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hexakill	Hexakill	k1gInSc1	Hexakill
(	(	kIx(	(
<g/>
Šestizářez	Šestizářez	k1gInSc1	Šestizářez
6	[number]	k4	6
<g/>
v	v	k7c4	v
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
Tento	tento	k3xDgInSc1	tento
mód	mód	k1gInSc1	mód
byl	být	k5eAaImAgInS	být
přidán	přidat	k5eAaPmNgInS	přidat
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2014	[number]	k4	2014
jako	jako	k8xS	jako
zpestření	zpestření	k1gNnSc4	zpestření
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
normální	normální	k2eAgFnSc1d1	normální
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
hráčů	hráč	k1gMnPc2	hráč
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
týmu	tým	k1gInSc6	tým
byl	být	k5eAaImAgInS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
odstraněn	odstranit	k5eAaPmNgInS	odstranit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ultra	ultra	k2eAgInSc1d1	ultra
Rapid	rapid	k1gInSc1	rapid
Fire	Fire	k1gFnSc1	Fire
(	(	kIx(	(
<g/>
URF	URF	kA	URF
<g/>
)	)	kIx)	)
-	-	kIx~	-
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
módu	mód	k1gInSc6	mód
měli	mít	k5eAaImAgMnP	mít
hráči	hráč	k1gMnPc1	hráč
neomezeně	omezeně	k6eNd1	omezeně
Many	mana	k1gFnPc4	mana
nebo	nebo	k8xC	nebo
Energie	energie	k1gFnPc4	energie
(	(	kIx(	(
<g/>
potřebujete	potřebovat	k5eAaImIp2nP	potřebovat
k	k	k7c3	k
vyvolávání	vyvolávání	k1gNnSc3	vyvolávání
schopností	schopnost	k1gFnPc2	schopnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
měly	mít	k5eAaImAgInP	mít
všechny	všechen	k3xTgFnPc4	všechen
schopnosti	schopnost	k1gFnPc4	schopnost
snížený	snížený	k2eAgInSc1d1	snížený
Cooldown	Cooldown	k1gInSc1	Cooldown
o	o	k7c4	o
80	[number]	k4	80
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
čekací	čekací	k2eAgFnSc1d1	čekací
doba	doba	k1gFnSc1	doba
na	na	k7c4	na
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
hrdinové	hrdina	k1gMnPc1	hrdina
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
módu	mód	k1gInSc6	mód
zakázáni	zakázán	k2eAgMnPc1d1	zakázán
kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgFnPc3	svůj
schopnostem	schopnost	k1gFnPc3	schopnost
(	(	kIx(	(
<g/>
Nidalee	Nidalee	k1gFnSc3	Nidalee
<g/>
,	,	kIx,	,
Hecarim	Hecarim	k1gInSc4	Hecarim
<g/>
,	,	kIx,	,
Soraka	Sorak	k1gMnSc4	Sorak
<g/>
,	,	kIx,	,
Skarner	Skarner	k1gMnSc1	Skarner
<g/>
,	,	kIx,	,
Kassadin	Kassadin	k2eAgMnSc1d1	Kassadin
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ultra	ultra	k2eAgInSc1d1	ultra
Rapid	rapid	k1gInSc1	rapid
Fire	Fir	k1gFnSc2	Fir
byl	být	k5eAaImAgInS	být
zamýšlen	zamýšlen	k2eAgInSc4d1	zamýšlen
jako	jako	k8xC	jako
aprílový	aprílový	k2eAgInSc4d1	aprílový
žert	žert	k1gInSc4	žert
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
Apríla	apríl	k1gInSc2	apríl
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doombots	Doombots	k1gInSc1	Doombots
(	(	kIx(	(
<g/>
Hráči	hráč	k1gMnPc1	hráč
vs	vs	k?	vs
<g/>
.	.	kIx.	.
umělá	umělý	k2eAgFnSc1d1	umělá
inteligence	inteligence	k1gFnSc1	inteligence
<g/>
)	)	kIx)	)
-	-	kIx~	-
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
módu	mód	k1gInSc6	mód
hrají	hrát	k5eAaImIp3nP	hrát
hráči	hráč	k1gMnPc1	hráč
proti	proti	k7c3	proti
botům	botum	k1gNnPc3	botum
(	(	kIx(	(
<g/>
umělé	umělý	k2eAgFnSc3d1	umělá
inteligenci	inteligence	k1gFnSc3	inteligence
<g/>
)	)	kIx)	)
se	s	k7c7	s
silnějšími	silný	k2eAgFnPc7d2	silnější
schopnostmi	schopnost	k1gFnPc7	schopnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mohou	moct	k5eAaImIp3nP	moct
střílet	střílet	k5eAaImF	střílet
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
naráz	naráz	k6eAd1	naráz
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
obtížnostech	obtížnost	k1gFnPc6	obtížnost
dostávají	dostávat	k5eAaImIp3nP	dostávat
kromě	kromě	k7c2	kromě
svých	svůj	k3xOyFgFnPc2	svůj
schopností	schopnost	k1gFnPc2	schopnost
i	i	k8xC	i
schopnosti	schopnost	k1gFnSc2	schopnost
jiných	jiný	k2eAgMnPc2d1	jiný
šampionů	šampion	k1gMnPc2	šampion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Boti	Bot	k1gMnPc1	Bot
měli	mít	k5eAaImAgMnP	mít
tři	tři	k4xCgFnPc4	tři
úrovně	úroveň	k1gFnPc4	úroveň
obtížnosti	obtížnost	k1gFnSc2	obtížnost
<g/>
.	.	kIx.	.
</s>
<s>
Mód	mód	k1gInSc1	mód
byl	být	k5eAaImAgInS	být
odstraněn	odstranit	k5eAaPmNgInS	odstranit
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
dočasně	dočasně	k6eAd1	dočasně
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hexakill	Hexakill	k1gMnSc1	Hexakill
-	-	kIx~	-
Twisted	Twisted	k1gMnSc1	Twisted
Treeline	Treelin	k1gInSc5	Treelin
(	(	kIx(	(
<g/>
Pokroucená	pokroucený	k2eAgFnSc1d1	pokroucená
alej	alej	k1gFnSc1	alej
6	[number]	k4	6
<g/>
v	v	k7c4	v
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
-	-	kIx~	-
Hexakill	Hexakill	k1gInSc1	Hexakill
mód	móda	k1gFnPc2	móda
v	v	k7c6	v
pokroucené	pokroucený	k2eAgFnSc6d1	pokroucená
aleji	alej	k1gFnSc6	alej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Legend	legenda	k1gFnPc2	legenda
of	of	k?	of
the	the	k?	the
Poro	Poro	k1gMnSc1	Poro
King	King	k1gMnSc1	King
(	(	kIx(	(
<g/>
Kvílející	kvílející	k2eAgFnSc1d1	kvílející
propast	propast	k1gFnSc1	propast
5	[number]	k4	5
<g/>
v	v	k7c4	v
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
-	-	kIx~	-
Stejná	stejný	k2eAgNnPc4d1	stejné
pravidla	pravidlo	k1gNnPc4	pravidlo
jako	jako	k8xC	jako
v	v	k7c4	v
ARAM	Aram	k1gInSc4	Aram
až	až	k9	až
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
hry	hra	k1gFnSc2	hra
můžete	moct	k5eAaImIp2nP	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
krále	král	k1gMnSc4	král
porů	por	k1gInPc2	por
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
postupuje	postupovat	k5eAaImIp3nS	postupovat
vpřed	vpřed	k6eAd1	vpřed
<g/>
,	,	kIx,	,
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
věže	věž	k1gFnPc4	věž
a	a	k8xC	a
léčí	léčit	k5eAaImIp3nP	léčit
hráče	hráč	k1gMnPc4	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Přidány	přidán	k2eAgFnPc1d1	přidána
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
summoner	summoner	k1gInSc1	summoner
spelly	spella	k1gFnSc2	spella
<g/>
:	:	kIx,	:
jeden	jeden	k4xCgMnSc1	jeden
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vystřelit	vystřelit	k5eAaPmF	vystřelit
pora	pora	k6eAd1	pora
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
raní	ranit	k5eAaPmIp3nS	ranit
nepřítele	nepřítel	k1gMnSc4	nepřítel
a	a	k8xC	a
pokud	pokud	k8xS	pokud
trefí	trefit	k5eAaPmIp3nP	trefit
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
do	do	k7c2	do
5	[number]	k4	5
sekund	sekunda	k1gFnPc2	sekunda
teleportovat	teleportovat	k5eAaPmF	teleportovat
k	k	k7c3	k
danému	daný	k2eAgMnSc3d1	daný
nepříteli	nepřítel	k1gMnSc3	nepřítel
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
spell	spell	k1gInSc1	spell
teleportuje	teleportovat	k5eAaPmIp3nS	teleportovat
hráče	hráč	k1gMnSc2	hráč
ke	k	k7c3	k
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
porů	por	k1gInPc2	por
se	se	k3xPyFc4	se
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tým	tým	k1gInSc1	tým
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
druhý	druhý	k4xOgInSc1	druhý
tým	tým	k1gInSc1	tým
desetkrát	desetkrát	k6eAd1	desetkrát
porem	porem	k1gInSc1	porem
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
summoner	summoner	k1gInSc1	summoner
spell	spell	k1gInSc1	spell
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
vyvolání	vyvolání	k1gNnSc4	vyvolání
dva	dva	k4xCgMnPc1	dva
porové	porové	k2eAgMnPc1d1	porové
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nemesis	Nemesis	k1gFnSc1	Nemesis
Draft	draft	k1gInSc1	draft
-	-	kIx~	-
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
klasický	klasický	k2eAgInSc4d1	klasický
5	[number]	k4	5
<g/>
v	v	k7c4	v
<g/>
5	[number]	k4	5
draft	draft	k1gInSc4	draft
mód	mód	k1gInSc4	mód
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
důležitou	důležitý	k2eAgFnSc7d1	důležitá
úpravou	úprava	k1gFnSc7	úprava
-	-	kIx~	-
nevybíráte	vybírat	k5eNaImIp2nP	vybírat
šampiony	šampion	k1gMnPc4	šampion
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepřátelskému	přátelský	k2eNgInSc3d1	nepřátelský
týmu	tým	k1gInSc3	tým
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
nepřátelská	přátelský	k2eNgFnSc1d1	nepřátelská
pětice	pětice	k1gFnSc1	pětice
vybírá	vybírat	k5eAaImIp3nS	vybírat
šampiony	šampion	k1gMnPc4	šampion
pro	pro	k7c4	pro
váš	váš	k3xOp2gInSc4	váš
tým	tým	k1gInSc4	tým
<g/>
.	.	kIx.	.
</s>
<s>
Šampiony	šampion	k1gMnPc4	šampion
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
vám	vy	k3xPp2nPc3	vy
vybere	vybrat	k5eAaPmIp3nS	vybrat
nepřátelský	přátelský	k2eNgInSc1d1	nepřátelský
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
následně	následně	k6eAd1	následně
můžete	moct	k5eAaImIp2nP	moct
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spoluhráči	spoluhráč	k1gMnPc7	spoluhráč
vyměňovat	vyměňovat	k5eAaImF	vyměňovat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
můžete	moct	k5eAaImIp2nP	moct
dokonce	dokonce	k9	dokonce
vyměňovat	vyměňovat	k5eAaImF	vyměňovat
a	a	k8xC	a
hrát	hrát	k5eAaImF	hrát
i	i	k9	i
šampiony	šampion	k1gMnPc4	šampion
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
nevlastníte	vlastnit	k5eNaImIp2nP	vlastnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PROJECT	PROJECT	kA	PROJECT
:	:	kIx,	:
Overcharge	Overcharge	k1gFnSc1	Overcharge
mapa	mapa	k1gFnSc1	mapa
<g/>
:	:	kIx,	:
Substructure	Substructur	k1gMnSc5	Substructur
43	[number]	k4	43
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
v	v	k7c4	v
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
K	k	k7c3	k
výhře	výhra	k1gFnSc3	výhra
získej	získat	k5eAaPmRp2nS	získat
50	[number]	k4	50
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Ničte	ničit	k5eAaImRp2nP	ničit
Charge	Charge	k1gInSc4	Charge
Boty	bota	k1gFnSc2	bota
a	a	k8xC	a
ukradněte	ukradnout	k5eAaPmRp2nP	ukradnout
jejich	jejich	k3xOp3gInPc4	jejich
Fragmenty	fragment	k1gInPc4	fragment
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
Přetížili	přetížit	k5eAaPmAgMnP	přetížit
svůj	svůj	k3xOyFgInSc4	svůj
tým	tým	k1gInSc4	tým
a	a	k8xC	a
rozdrtili	rozdrtit	k5eAaPmAgMnP	rozdrtit
své	svůj	k3xOyFgMnPc4	svůj
nepřátele	nepřítel	k1gMnPc4	nepřítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hunt	hunt	k1gInSc1	hunt
of	of	k?	of
the	the	k?	the
Blood	Blood	k1gInSc1	Blood
Moon	Moon	k1gNnSc1	Moon
5	[number]	k4	5
<g/>
v	v	k7c4	v
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
Mapa	mapa	k1gFnSc1	mapa
<g/>
:	:	kIx,	:
Summoner	Summoner	k1gInSc1	Summoner
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Rift	Rifta	k1gFnPc2	Rifta
(	(	kIx(	(
<g/>
upravený	upravený	k2eAgInSc1d1	upravený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
získá	získat	k5eAaPmIp3nS	získat
300	[number]	k4	300
bodů	bod	k1gInPc2	bod
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Zisk	zisk	k1gInSc1	zisk
bodů	bod	k1gInPc2	bod
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
)	)	kIx)	)
Zabití	zabití	k1gNnSc4	zabití
nepřátelského	přátelský	k2eNgMnSc2d1	nepřátelský
šampiona	šampion	k1gMnSc2	šampion
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
)	)	kIx)	)
Zabití	zabití	k1gNnSc4	zabití
ducha	duch	k1gMnSc2	duch
v	v	k7c6	v
jungli	jungle	k1gFnSc6	jungle
<g/>
;	;	kIx,	;
3	[number]	k4	3
<g/>
)	)	kIx)	)
Zabití	zabitý	k1gMnPc1	zabitý
Demon	Demon	k1gMnSc1	Demon
Heralda	Heralda	k1gMnSc1	Heralda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vaše	váš	k3xOp2gFnPc1	váš
summoner	summoner	k1gInSc4	summoner
spelly	spella	k1gFnSc2	spella
mají	mít	k5eAaImIp3nP	mít
50	[number]	k4	50
<g/>
%	%	kIx~	%
CDR	CDR	kA	CDR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
jste	být	k5eAaImIp2nP	být
mimo	mimo	k7c4	mimo
boj	boj	k1gInSc4	boj
<g/>
,	,	kIx,	,
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
rychlost	rychlost	k1gFnSc1	rychlost
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
regenerace	regenerace	k1gFnSc2	regenerace
many	mana	k1gFnSc2	mana
<g/>
,	,	kIx,	,
ultimátka	ultimátka	k1gFnSc1	ultimátka
se	se	k3xPyFc4	se
nabíjí	nabíjet	k5eAaImIp3nS	nabíjet
rychleji	rychle	k6eAd2	rychle
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc4	průběh
hry	hra	k1gFnSc2	hra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mimo	mimo	k7c4	mimo
hru	hra	k1gFnSc4	hra
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
hráč	hráč	k1gMnSc1	hráč
zrovna	zrovna	k6eAd1	zrovna
nehraje	hrát	k5eNaImIp3nS	hrát
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
lobby	lobby	k1gFnSc6	lobby
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
si	se	k3xPyFc3	se
hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
nakupovat	nakupovat	k5eAaBmF	nakupovat
další	další	k2eAgMnPc4d1	další
hrdiny	hrdina	k1gMnPc4	hrdina
<g/>
,	,	kIx,	,
balíčky	balíček	k1gInPc1	balíček
<g/>
,	,	kIx,	,
svazky	svazek	k1gInPc1	svazek
a	a	k8xC	a
prohlížet	prohlížet	k5eAaImF	prohlížet
si	se	k3xPyFc3	se
statistiky	statistika	k1gFnPc1	statistika
jiných	jiný	k2eAgMnPc2d1	jiný
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pozorovat	pozorovat	k5eAaImF	pozorovat
hry	hra	k1gFnPc4	hra
jiných	jiný	k2eAgMnPc2d1	jiný
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Runy	run	k1gInPc4	run
===	===	k?	===
</s>
</p>
<p>
<s>
Runy	Runa	k1gFnPc1	Runa
jsou	být	k5eAaImIp3nP	být
podpůrné	podpůrný	k2eAgInPc4d1	podpůrný
doplňky	doplněk	k1gInPc4	doplněk
šampiona	šampion	k1gMnSc2	šampion
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obvykle	obvykle	k6eAd1	obvykle
zveličují	zveličovat	k5eAaImIp3nP	zveličovat
schopnosti	schopnost	k1gFnPc4	schopnost
a	a	k8xC	a
úlohu	úloha	k1gFnSc4	úloha
šampiona	šampion	k1gMnSc2	šampion
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Platidla	platidlo	k1gNnSc2	platidlo
===	===	k?	===
</s>
</p>
<p>
<s>
ME	ME	kA	ME
–	–	k?	–
Modrá	modrý	k2eAgFnSc1d1	modrá
Esence	esence	k1gFnSc1	esence
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
peníze	peníz	k1gInPc4	peníz
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
League	Leagu	k1gFnSc2	Leagu
of	of	k?	of
Legends	Legends	k1gInSc1	Legends
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
si	se	k3xPyFc3	se
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
může	moct	k5eAaImIp3nS	moct
koupit	koupit	k5eAaPmF	koupit
hrdiny	hrdina	k1gMnPc4	hrdina
nebo	nebo	k8xC	nebo
ikonky	ikonka	k1gFnPc4	ikonka
vyvolávače	vyvolávač	k1gMnSc2	vyvolávač
(	(	kIx(	(
<g/>
ME	ME	kA	ME
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
pomocí	pomoc	k1gFnSc7	pomoc
"	"	kIx"	"
<g/>
Champion	Champion	k1gInSc1	Champion
Capsle	Capsle	k1gFnSc2	Capsle
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
<g/>
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
získá	získat	k5eAaPmIp3nS	získat
za	za	k7c4	za
levely	level	k1gInPc4	level
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RP	RP	kA	RP
–	–	k?	–
Riot	Riot	k2eAgInSc4d1	Riot
Points	Points	k1gInSc4	Points
si	se	k3xPyFc3	se
hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
koupit	koupit	k5eAaPmF	koupit
za	za	k7c4	za
peníze	peníz	k1gInPc4	peníz
pomocí	pomocí	k7c2	pomocí
kreditní	kreditní	k2eAgFnSc2d1	kreditní
karty	karta	k1gFnSc2	karta
<g/>
,	,	kIx,	,
Paypal	Paypal	k1gMnSc1	Paypal
<g/>
,	,	kIx,	,
PaySafeCard	PaySafeCard	k1gMnSc1	PaySafeCard
<g/>
,	,	kIx,	,
SMS	SMS	kA	SMS
<g/>
,	,	kIx,	,
Moneybookers	Moneybookers	k1gInSc1	Moneybookers
<g/>
,	,	kIx,	,
U-kash	Uash	k1gInSc1	U-kash
a	a	k8xC	a
Předplacené	předplacený	k2eAgInPc1d1	předplacený
Riot	Riot	k1gInSc1	Riot
karty	karta	k1gFnSc2	karta
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
způsoby	způsob	k1gInPc1	způsob
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ČR	ČR	kA	ČR
umožněny	umožněn	k2eAgFnPc1d1	umožněna
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
SMS	SMS	kA	SMS
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgMnPc3d1	ostatní
značně	značně	k6eAd1	značně
znevýhodněna	znevýhodnit	k5eAaPmNgFnS	znevýhodnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obchod	obchod	k1gInSc1	obchod
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
si	se	k3xPyFc3	se
hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
zakoupit	zakoupit	k5eAaPmF	zakoupit
věci	věc	k1gFnPc4	věc
za	za	k7c7	za
ME	ME	kA	ME
a	a	k8xC	a
RP	RP	kA	RP
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
věci	věc	k1gFnPc1	věc
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
lze	lze	k6eAd1	lze
koupit	koupit	k5eAaPmF	koupit
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
RP	RP	kA	RP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Champions	Champions	k1gInSc1	Champions
(	(	kIx(	(
<g/>
šampioni	šampion	k1gMnPc1	šampion
<g/>
)	)	kIx)	)
-	-	kIx~	-
zde	zde	k6eAd1	zde
si	se	k3xPyFc3	se
hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
permanentně	permanentně	k6eAd1	permanentně
odemknout	odemknout	k5eAaPmF	odemknout
další	další	k2eAgMnPc4d1	další
hrdiny	hrdina	k1gMnPc4	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
nakupovat	nakupovat	k5eAaBmF	nakupovat
za	za	k7c4	za
ME	ME	kA	ME
i	i	k8xC	i
RP	RP	kA	RP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skins	Skins	k1gInSc1	Skins
(	(	kIx(	(
<g/>
skiny	skin	k1gMnPc7	skin
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jiné	jiný	k2eAgFnPc1d1	jiná
verze	verze	k1gFnPc1	verze
vzhledu	vzhled	k1gInSc2	vzhled
šampionů	šampion	k1gMnPc2	šampion
(	(	kIx(	(
<g/>
nemá	mít	k5eNaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
hru	hra	k1gFnSc4	hra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
zakoupit	zakoupit	k5eAaPmF	zakoupit
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
RP	RP	kA	RP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gifting	Gifting	k1gInSc1	Gifting
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
dárcovské	dárcovský	k2eAgNnSc1d1	dárcovské
centrum	centrum	k1gNnSc1	centrum
<g/>
)	)	kIx)	)
-	-	kIx~	-
zde	zde	k6eAd1	zde
může	moct	k5eAaImIp3nS	moct
hráč	hráč	k1gMnSc1	hráč
svým	svůj	k3xOyFgMnPc3	svůj
přátelům	přítel	k1gMnPc3	přítel
darovat	darovat	k5eAaPmF	darovat
šampiona	šampion	k1gMnSc4	šampion
nebo	nebo	k8xC	nebo
skin	skin	k1gMnSc1	skin
(	(	kIx(	(
<g/>
za	za	k7c4	za
RP	RP	kA	RP
<g/>
)	)	kIx)	)
-	-	kIx~	-
během	během	k7c2	během
tematických	tematický	k2eAgFnPc2d1	tematická
akcí	akce	k1gFnPc2	akce
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
darování	darování	k1gNnSc3	darování
dají	dát	k5eAaPmIp3nP	dát
získat	získat	k5eAaPmF	získat
nové	nový	k2eAgFnPc4d1	nová
ikonky	ikonka	k1gFnPc4	ikonka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Boosts	Boosts	k1gInSc1	Boosts
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
podpory	podpora	k1gFnSc2	podpora
<g/>
)	)	kIx)	)
přidávají	přidávat	k5eAaImIp3nP	přidávat
více	hodně	k6eAd2	hodně
zkušeností	zkušenost	k1gFnPc2	zkušenost
a	a	k8xC	a
ME	ME	kA	ME
vydělaných	vydělaný	k2eAgInPc2d1	vydělaný
za	za	k7c4	za
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
koupit	koupit	k5eAaPmF	koupit
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
počet	počet	k1gInSc4	počet
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
zakoupit	zakoupit	k5eAaPmF	zakoupit
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
RP	RP	kA	RP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Runes	Runes	k1gInSc1	Runes
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
runy	runa	k1gFnSc2	runa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
má	mít	k5eAaImIp3nS	mít
dostupné	dostupný	k2eAgFnPc4d1	dostupná
strany	strana	k1gFnPc4	strana
run	runa	k1gFnPc2	runa
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
možnost	možnost	k1gFnSc4	možnost
nákupu	nákup	k1gInSc2	nákup
dalších	další	k2eAgFnPc2d1	další
stránek	stránka	k1gFnPc2	stránka
na	na	k7c4	na
runy	runa	k1gFnPc4	runa
(	(	kIx(	(
<g/>
za	za	k7c4	za
ME	ME	kA	ME
i	i	k8xC	i
RP	RP	kA	RP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Summoner	Summoner	k1gInSc1	Summoner
Icons	Icons	k1gInSc1	Icons
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
ikony	ikona	k1gFnSc2	ikona
vyvolávače	vyvolávač	k1gMnSc2	vyvolávač
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgFnPc4d1	malá
ikony	ikona	k1gFnPc4	ikona
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
na	na	k7c6	na
načítací	načítací	k2eAgFnSc6d1	načítací
obrazovce	obrazovka	k1gFnSc6	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
koupit	koupit	k5eAaPmF	koupit
loga	logo	k1gNnPc4	logo
oblíbených	oblíbený	k2eAgInPc2d1	oblíbený
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
za	za	k7c7	za
RP	RP	kA	RP
i	i	k8xC	i
ME	ME	kA	ME
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ward	Ward	k1gInSc1	Ward
Skins	Skins	k1gInSc1	Skins
(	(	kIx(	(
<g/>
vzhledy	vzhled	k1gInPc1	vzhled
na	na	k7c4	na
wardy	ward	k1gInPc4	ward
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jiné	jiný	k2eAgFnPc1d1	jiná
verze	verze	k1gFnPc1	verze
hlídačů	hlídač	k1gInPc2	hlídač
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
ward	ward	k1gInSc1	ward
<g/>
.	.	kIx.	.
</s>
<s>
Dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
koupit	koupit	k5eAaPmF	koupit
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
RP	RP	kA	RP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Account	Account	k1gInSc1	Account
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
účet	účet	k1gInSc4	účet
<g/>
)	)	kIx)	)
-	-	kIx~	-
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
zakoupit	zakoupit	k5eAaPmF	zakoupit
přechod	přechod	k1gInSc4	přechod
na	na	k7c4	na
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
jiný	jiný	k2eAgInSc4d1	jiný
dostupný	dostupný	k2eAgInSc4d1	dostupný
server	server	k1gInSc4	server
(	(	kIx(	(
<g/>
za	za	k7c4	za
2600	[number]	k4	2600
RP	RP	kA	RP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
změnu	změna	k1gFnSc4	změna
jména	jméno	k1gNnSc2	jméno
(	(	kIx(	(
<g/>
za	za	k7c4	za
RP	RP	kA	RP
i	i	k8xC	i
ME	ME	kA	ME
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bundles	Bundles	k1gInSc1	Bundles
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
balíčky	balíček	k1gInPc4	balíček
<g/>
)	)	kIx)	)
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
runy	runa	k1gFnPc1	runa
a	a	k8xC	a
šampiony	šampion	k1gMnPc4	šampion
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
zakoupení	zakoupení	k1gNnSc6	zakoupení
hráči	hráč	k1gMnPc1	hráč
odemknou	odemknout	k5eAaPmIp3nP	odemknout
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
zakoupit	zakoupit	k5eAaPmF	zakoupit
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
RP	RP	kA	RP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Riot	Riot	k2eAgInSc1d1	Riot
points	points	k1gInSc1	points
-	-	kIx~	-
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
koupit	koupit	k5eAaPmF	koupit
RP	RP	kA	RP
za	za	k7c4	za
reálné	reálný	k2eAgInPc4d1	reálný
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Blue	Blue	k1gFnSc1	Blue
essence	essence	k1gFnSc1	essence
-	-	kIx~	-
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
získat	získat	k5eAaPmF	získat
za	za	k7c4	za
hraní	hraní	k1gNnSc4	hraní
a	a	k8xC	a
plnění	plnění	k1gNnSc4	plnění
úkolů	úkol	k1gInPc2	úkol
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
</s>
</p>
<p>
<s>
==	==	k?	==
Divize	divize	k1gFnSc2	divize
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c4	za
výhry	výhra	k1gFnPc4	výhra
v	v	k7c6	v
hodnoceném	hodnocený	k2eAgInSc6d1	hodnocený
zápase	zápas	k1gInSc6	zápas
se	se	k3xPyFc4	se
získávají	získávat	k5eAaImIp3nP	získávat
tzv.	tzv.	kA	tzv.
LP	LP	kA	LP
(	(	kIx(	(
<g/>
leveling	leveling	k1gInSc1	leveling
points	points	k1gInSc1	points
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
nichž	jenž	k3xRgMnPc2	jenž
hráči	hráč	k1gMnPc1	hráč
postupují	postupovat	k5eAaImIp3nP	postupovat
po	po	k7c6	po
hodnotícím	hodnotící	k2eAgInSc6d1	hodnotící
žebříčku	žebříček	k1gInSc6	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgMnS	rozdělit
na	na	k7c4	na
devět	devět	k4xCc4	devět
úrovní	úroveň	k1gFnPc2	úroveň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
sestupně	sestupně	k6eAd1	sestupně
na	na	k7c4	na
divize	divize	k1gFnPc4	divize
I	i	k9	i
až	až	k6eAd1	až
V	v	k7c6	v
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
Zlatá	zlatá	k1gFnSc1	zlatá
V	V	kA	V
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
subdivizi	subdivize	k1gFnSc4	subdivize
výše	výše	k1gFnSc2	výše
než	než	k8xS	než
Stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
I	I	kA	I
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Železná	železný	k2eAgFnSc1d1	železná
a	a	k8xC	a
Velmistrovská	velmistrovský	k2eAgFnSc1d1	velmistrovská
úroveň	úroveň	k1gFnSc1	úroveň
byly	být	k5eAaImAgFnP	být
přidány	přidat	k5eAaPmNgFnP	přidat
až	až	k6eAd1	až
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Železná	železný	k2eAgFnSc1d1	železná
(	(	kIx(	(
<g/>
Iron	iron	k1gInSc1	iron
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bronzová	bronzový	k2eAgFnSc1d1	bronzová
(	(	kIx(	(
<g/>
Bronze	bronz	k1gInSc5	bronz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stříbrná	stříbrná	k1gFnSc1	stříbrná
(	(	kIx(	(
<g/>
Silver	Silver	k1gInSc1	Silver
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zlatá	zlatá	k1gFnSc1	zlatá
(	(	kIx(	(
<g/>
Gold	Gold	k1gInSc1	Gold
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Platinová	platinový	k2eAgNnPc1d1	platinové
(	(	kIx(	(
<g/>
Platinum	Platinum	k1gNnSc1	Platinum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Diamantová	diamantový	k2eAgFnSc1d1	Diamantová
(	(	kIx(	(
<g/>
Diamond	Diamond	k1gMnSc1	Diamond
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mistrovská	mistrovský	k2eAgFnSc1d1	mistrovská
(	(	kIx(	(
<g/>
Master	master	k1gMnSc1	master
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Velmistrovská	velmistrovský	k2eAgFnSc1d1	velmistrovská
(	(	kIx(	(
<g/>
Grandmaster	Grandmaster	k1gMnSc1	Grandmaster
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vyzyvatelská	Vyzyvatelský	k2eAgFnSc1d1	Vyzyvatelský
(	(	kIx(	(
<g/>
Challenger	Challenger	k1gMnSc1	Challenger
<g/>
)	)	kIx)	)
<g/>
Po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
100	[number]	k4	100
LP	LP	kA	LP
se	se	k3xPyFc4	se
hráč	hráč	k1gMnSc1	hráč
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
série	série	k1gFnSc2	série
(	(	kIx(	(
<g/>
promo	promo	k6eAd1	promo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
další	další	k2eAgFnPc4d1	další
tři	tři	k4xCgFnPc4	tři
nebo	nebo	k8xC	nebo
pět	pět	k4xCc4	pět
her	hra	k1gFnPc2	hra
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
postoupit	postoupit	k5eAaPmF	postoupit
do	do	k7c2	do
vyšší	vysoký	k2eAgFnSc2d2	vyšší
divize	divize	k1gFnSc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
naopak	naopak	k6eAd1	naopak
0	[number]	k4	0
LP	LP	kA	LP
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
prohraje	prohrát	k5eAaPmIp3nS	prohrát
<g/>
,	,	kIx,	,
sestoupí	sestoupit	k5eAaPmIp3nS	sestoupit
o	o	k7c4	o
divizi	divize	k1gFnSc4	divize
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
hráč	hráč	k1gMnSc1	hráč
úvodních	úvodní	k2eAgInPc2d1	úvodní
deset	deset	k4xCc1	deset
rozřazovacích	rozřazovací	k2eAgInPc2d1	rozřazovací
zápasů	zápas	k1gInPc2	zápas
ukončí	ukončit	k5eAaPmIp3nS	ukončit
např.	např.	kA	např.
s	s	k7c7	s
5	[number]	k4	5
výhrami	výhra	k1gFnPc7	výhra
a	a	k8xC	a
5	[number]	k4	5
prohrami	prohra	k1gFnPc7	prohra
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
Stříbrné	stříbrná	k1gFnSc2	stříbrná
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
nasbírat	nasbírat	k5eAaPmF	nasbírat
100	[number]	k4	100
LP	LP	kA	LP
a	a	k8xC	a
vyhrát	vyhrát	k5eAaPmF	vyhrát
dvě	dva	k4xCgFnPc1	dva
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
her	hra	k1gFnPc2	hra
v	v	k7c6	v
následné	následný	k2eAgFnSc6d1	následná
sérii	série	k1gFnSc6	série
<g/>
,	,	kIx,	,
postoupil	postoupit	k5eAaPmAgMnS	postoupit
by	by	kYmCp3nS	by
do	do	k7c2	do
Stříbrné	stříbrná	k1gFnSc2	stříbrná
II	II	kA	II
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
řadou	řada	k1gFnSc7	řada
proher	prohra	k1gFnPc2	prohra
by	by	kYmCp3nS	by
sestoupil	sestoupit	k5eAaPmAgInS	sestoupit
do	do	k7c2	do
Stříbrné	stříbrná	k1gFnSc2	stříbrná
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Šampioni	šampion	k1gMnPc5	šampion
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
144	[number]	k4	144
šampionů	šampion	k1gMnPc2	šampion
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
postava	postava	k1gFnSc1	postava
má	mít	k5eAaImIp3nS	mít
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
k	k	k7c3	k
jejím	její	k3xOp3gFnPc3	její
schopnostem	schopnost	k1gFnPc3	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
rozmanité	rozmanitý	k2eAgFnPc1d1	rozmanitá
<g/>
:	:	kIx,	:
od	od	k7c2	od
různorodých	různorodý	k2eAgInPc2d1	různorodý
útoků	útok	k1gInPc2	útok
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
léčení	léčení	k1gNnSc4	léčení
<g/>
,	,	kIx,	,
teleportace	teleportace	k1gFnPc4	teleportace
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
podpůrné	podpůrný	k2eAgFnPc4d1	podpůrná
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
speciální	speciální	k2eAgInPc1d1	speciální
útoky	útok	k1gInPc1	útok
dokáží	dokázat	k5eAaPmIp3nP	dokázat
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
cíl	cíl	k1gInSc4	cíl
i	i	k9	i
na	na	k7c6	na
protější	protější	k2eAgFnSc6d1	protější
straně	strana	k1gFnSc6	strana
mapy	mapa	k1gFnSc2	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Daná	daný	k2eAgFnSc1d1	daná
kombinace	kombinace	k1gFnSc1	kombinace
schopností	schopnost	k1gFnSc7	schopnost
šampiona	šampion	k1gMnSc2	šampion
pak	pak	k6eAd1	pak
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
předurčuje	předurčovat	k5eAaImIp3nS	předurčovat
pro	pro	k7c4	pro
hraní	hraní	k1gNnSc4	hraní
určité	určitý	k2eAgFnSc2d1	určitá
role	role	k1gFnSc2	role
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Schopnosti	schopnost	k1gFnPc1	schopnost
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
minimálně	minimálně	k6eAd1	minimálně
jednu	jeden	k4xCgFnSc4	jeden
pasivní	pasivní	k2eAgFnSc4d1	pasivní
a	a	k8xC	a
právě	právě	k6eAd1	právě
čtyři	čtyři	k4xCgInPc4	čtyři
samostatně	samostatně	k6eAd1	samostatně
aktivovatelné	aktivovatelný	k2eAgInPc4d1	aktivovatelný
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
hrají	hrát	k5eAaImIp3nP	hrát
schopnosti	schopnost	k1gFnPc1	schopnost
namapované	namapovaný	k2eAgFnPc1d1	namapovaná
na	na	k7c4	na
klávesy	klávesa	k1gFnPc4	klávesa
QWER	QWER	kA	QWER
<g/>
,	,	kIx,	,
sám	sám	k3xTgInSc1	sám
Riot	Riot	k1gInSc1	Riot
nazývá	nazývat	k5eAaImIp3nS	nazývat
schopnosti	schopnost	k1gFnPc4	schopnost
jako	jako	k9	jako
qéčko	qéčko	k1gNnSc1	qéčko
<g/>
,	,	kIx,	,
wéčko	wéčko	k1gNnSc1	wéčko
<g/>
,	,	kIx,	,
éčko	éčko	k1gNnSc1	éčko
a	a	k8xC	a
erko	erko	k1gNnSc1	erko
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
ale	ale	k9	ale
žádné	žádný	k3yNgNnSc1	žádný
pravidlo	pravidlo	k1gNnSc1	pravidlo
společných	společný	k2eAgInPc2d1	společný
znaků	znak	k1gInPc2	znak
všech	všecek	k3xTgNnPc2	všecek
qéček	qéčko	k1gNnPc2	qéčko
či	či	k8xC	či
ostatních	ostatní	k2eAgFnPc2d1	ostatní
schopností	schopnost	k1gFnPc2	schopnost
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
šampiony	šampion	k1gMnPc7	šampion
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
ale	ale	k8xC	ale
právě	právě	k6eAd1	právě
Q-schopnost	Qchopnost	k1gFnSc1	Q-schopnost
bývá	bývat	k5eAaImIp3nS	bývat
hlavní	hlavní	k2eAgFnSc1d1	hlavní
útočná	útočný	k2eAgFnSc1d1	útočná
schopnost	schopnost	k1gFnSc1	schopnost
většiny	většina	k1gFnSc2	většina
šampionů	šampion	k1gMnPc2	šampion
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
tři	tři	k4xCgFnPc4	tři
schopnosti	schopnost	k1gFnPc4	schopnost
jsou	být	k5eAaImIp3nP	být
hlavní	hlavní	k2eAgNnPc1d1	hlavní
funkční	funkční	k2eAgNnPc1d1	funkční
kouzla	kouzlo	k1gNnPc1	kouzlo
šampionů	šampion	k1gMnPc2	šampion
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mívají	mívat	k5eAaImIp3nP	mívat
krátkou	krátký	k2eAgFnSc4d1	krátká
až	až	k9	až
středně	středně	k6eAd1	středně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
přebíjecí	přebíjecí	k2eAgFnSc4d1	přebíjecí
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
schopnost	schopnost	k1gFnSc1	schopnost
-	-	kIx~	-
namapovaná	namapovaný	k2eAgFnSc1d1	namapovaná
na	na	k7c4	na
R	R	kA	R
-	-	kIx~	-
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ultimátní	ultimátní	k2eAgNnSc1d1	ultimátní
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
šampionova	šampionův	k2eAgFnSc1d1	šampionův
ultimátka	ultimátka	k1gFnSc1	ultimátka
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgNnSc1d1	oficiální
názvosloví	názvosloví	k1gNnSc1	názvosloví
české	český	k2eAgFnSc2d1	Česká
verze	verze	k1gFnSc2	verze
LoL	LoL	k1gFnSc2	LoL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ultimátka	Ultimátka	k1gFnSc1	Ultimátka
obvykle	obvykle	k6eAd1	obvykle
mívá	mívat	k5eAaImIp3nS	mívat
velmi	velmi	k6eAd1	velmi
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
přebíjecí	přebíjecí	k2eAgFnSc4d1	přebíjecí
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
z	z	k7c2	z
názvu	název	k1gInSc2	název
vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
<g/>
,	,	kIx,	,
ultimátka	ultimátka	k1gFnSc1	ultimátka
je	být	k5eAaImIp3nS	být
šampionova	šampionův	k2eAgFnSc1d1	šampionův
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
schopnost	schopnost	k1gFnSc1	schopnost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
působí	působit	k5eAaImIp3nS	působit
největší	veliký	k2eAgNnSc1d3	veliký
poškození	poškození	k1gNnSc1	poškození
<g/>
,	,	kIx,	,
nejdelší	dlouhý	k2eAgInPc1d3	nejdelší
omezující	omezující	k2eAgInPc1d1	omezující
účinky	účinek	k1gInPc1	účinek
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnSc1d3	veliký
léčení	léčení	k1gNnSc1	léčení
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Pasivní	pasivní	k2eAgFnSc1d1	pasivní
schopnost	schopnost	k1gFnSc1	schopnost
je	být	k5eAaImIp3nS	být
unikátní	unikátní	k2eAgInSc4d1	unikátní
doplněk	doplněk	k1gInSc4	doplněk
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
šampiona	šampion	k1gMnSc4	šampion
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
podrtrhnout	podrtrhnout	k5eAaPmF	podrtrhnout
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
(	(	kIx(	(
<g/>
Annie	Annie	k1gFnSc2	Annie
<g/>
,	,	kIx,	,
Soraka	Sorak	k1gMnSc2	Sorak
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
takovou	takový	k3xDgFnSc4	takový
anebo	anebo	k8xC	anebo
přímo	přímo	k6eAd1	přímo
tvoří	tvořit	k5eAaImIp3nP	tvořit
součást	součást	k1gFnSc4	součást
jejich	jejich	k3xOp3gFnSc2	jejich
gameplaye	gameplay	k1gFnSc2	gameplay
(	(	kIx(	(
<g/>
Karthus	Karthus	k1gInSc1	Karthus
<g/>
,	,	kIx,	,
Aurelion	Aurelion	k1gInSc1	Aurelion
Sol	sol	k1gInSc1	sol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
ovládané	ovládaný	k2eAgFnPc1d1	ovládaná
(	(	kIx(	(
<g/>
výjimky	výjimka	k1gFnPc1	výjimka
-	-	kIx~	-
Azir	Azir	k1gInSc1	Azir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dějí	dít	k5eAaBmIp3nP	dít
se	se	k3xPyFc4	se
sami	sám	k3xTgMnPc1	sám
od	od	k7c2	od
sebe	se	k3xPyFc2	se
po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
spouštěcí	spouštěcí	k2eAgFnSc6d1	spouštěcí
akci	akce	k1gFnSc6	akce
(	(	kIx(	(
<g/>
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
jiná	jiný	k2eAgFnSc1d1	jiná
schopnost	schopnost	k1gFnSc1	schopnost
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
šampioni	šampion	k1gMnPc1	šampion
mají	mít	k5eAaImIp3nP	mít
více	hodně	k6eAd2	hodně
pasivních	pasivní	k2eAgFnPc2d1	pasivní
schopností	schopnost	k1gFnPc2	schopnost
(	(	kIx(	(
<g/>
Bard	bard	k1gMnSc1	bard
<g/>
,	,	kIx,	,
Ornn	Ornn	k1gMnSc1	Ornn
<g/>
,	,	kIx,	,
Kassadin	Kassadin	k2eAgMnSc1d1	Kassadin
<g/>
,	,	kIx,	,
Fizz	Fizz	k1gMnSc1	Fizz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
zase	zase	k9	zase
pasivní	pasivní	k2eAgFnSc2d1	pasivní
schopnosti	schopnost	k1gFnSc2	schopnost
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
běžných	běžný	k2eAgFnPc6d1	běžná
aktivovatelných	aktivovatelný	k2eAgFnPc6d1	aktivovatelná
schopnostech	schopnost	k1gFnPc6	schopnost
(	(	kIx(	(
<g/>
Annie	Annie	k1gFnSc1	Annie
<g/>
,	,	kIx,	,
Kassadin	Kassadina	k1gFnPc2	Kassadina
<g/>
,	,	kIx,	,
Sona	Sonus	k1gMnSc2	Sonus
<g/>
,	,	kIx,	,
Soraka	Sorak	k1gMnSc2	Sorak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
572	[number]	k4	572
základních	základní	k2eAgFnPc2d1	základní
schopností	schopnost	k1gFnPc2	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
rozdělit	rozdělit	k5eAaPmF	rozdělit
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějž	jenž	k3xRgMnSc4	jenž
je	být	k5eAaImIp3nS	být
kouzlo	kouzlo	k1gNnSc1	kouzlo
sesláno	seslán	k2eAgNnSc1d1	sesláno
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Mířené	mířený	k2eAgFnPc1d1	mířená
schopnosti	schopnost	k1gFnPc1	schopnost
(	(	kIx(	(
<g/>
skillshot	skillshot	k1gInSc1	skillshot
<g/>
)	)	kIx)	)
–	–	k?	–
šampion	šampion	k1gMnSc1	šampion
vystřelí	vystřelit	k5eAaPmIp3nS	vystřelit
určitým	určitý	k2eAgInSc7d1	určitý
směrem	směr	k1gInSc7	směr
projektil	projektil	k1gInSc1	projektil
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
uletí	uletět	k5eAaPmIp3nS	uletět
určitou	určitý	k2eAgFnSc4d1	určitá
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
šampiona	šampion	k1gMnSc4	šampion
nebo	nebo	k8xC	nebo
poskoka	poskok	k1gMnSc4	poskok
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
provede	provést	k5eAaPmIp3nS	provést
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
akci	akce	k1gFnSc4	akce
(	(	kIx(	(
<g/>
poškodí	poškodit	k5eAaPmIp3nS	poškodit
<g/>
,	,	kIx,	,
znehybní	znehybnit	k5eAaPmIp3nS	znehybnit
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
Luxiino	Luxiin	k2eAgNnSc4d1	Luxiin
Q	Q	kA	Q
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Mundovo	Mundův	k2eAgNnSc4d1	Mundův
Q	Q	kA	Q
<g/>
,	,	kIx,	,
Morgany	morgan	k1gInPc7	morgan
Q	Q	kA	Q
<g/>
,	,	kIx,	,
Caitlynino	Caitlynin	k2eAgNnSc1d1	Caitlynin
Q	Q	kA	Q
</s>
</p>
<p>
<s>
Zaměřované	zaměřovaný	k2eAgFnPc1d1	zaměřovaná
schopnosti	schopnost	k1gFnPc1	schopnost
(	(	kIx(	(
<g/>
aimed	aimed	k1gInSc1	aimed
<g/>
)	)	kIx)	)
–	–	k?	–
vyvolávač	vyvolávač	k1gMnSc1	vyvolávač
vybere	vybrat	k5eAaPmIp3nS	vybrat
cílového	cílový	k2eAgMnSc4d1	cílový
šampiona	šampion	k1gMnSc4	šampion
a	a	k8xC	a
kouzlo	kouzlo	k1gNnSc1	kouzlo
jej	on	k3xPp3gMnSc4	on
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
<g/>
,	,	kIx,	,
oběť	oběť	k1gFnSc1	oběť
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nemůže	moct	k5eNaImIp3nS	moct
vyhnout	vyhnout	k5eAaPmF	vyhnout
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
Annie	Annie	k1gFnSc1	Annie
Q	Q	kA	Q
<g/>
,	,	kIx,	,
Kassadinovo	Kassadinův	k2eAgNnSc4d1	Kassadinův
Q	Q	kA	Q
<g/>
,	,	kIx,	,
Lissandry	Lissandr	k1gInPc7	Lissandr
R	R	kA	R
<g/>
,	,	kIx,	,
Kayly	Kayl	k1gInPc7	Kayl
R	R	kA	R
<g/>
,	,	kIx,	,
Morgany	morgan	k1gInPc7	morgan
E	E	kA	E
</s>
</p>
<p>
<s>
Přepínatelné	přepínatelný	k2eAgFnPc1d1	přepínatelná
plošné	plošný	k2eAgFnPc1d1	plošná
schopnosti	schopnost	k1gFnPc1	schopnost
–	–	k?	–
šampion	šampion	k1gMnSc1	šampion
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
utvoří	utvořit	k5eAaPmIp3nS	utvořit
oblast	oblast	k1gFnSc1	oblast
způsobující	způsobující	k2eAgNnSc4d1	způsobující
poškození	poškození	k1gNnSc4	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
sekundu	sekunda	k1gFnSc4	sekunda
spotřebovává	spotřebovávat	k5eAaImIp3nS	spotřebovávat
manu	mana	k1gFnSc4	mana
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
Amumuovo	Amumuovo	k1gNnSc1	Amumuovo
W	W	kA	W
<g/>
,	,	kIx,	,
Karthusovo	Karthusův	k2eAgNnSc4d1	Karthusův
E	E	kA	E
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Mundovo	Mundův	k2eAgNnSc4d1	Mundův
W	W	kA	W
<g/>
,	,	kIx,	,
Aurelion	Aurelion	k1gInSc4	Aurelion
Sola	Sol	k1gInSc2	Sol
W	W	kA	W
</s>
</p>
<p>
<s>
Schopnosti	schopnost	k1gFnPc1	schopnost
zvyšující	zvyšující	k2eAgFnSc4d1	zvyšující
rychlost	rychlost	k1gFnSc4	rychlost
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
útoků	útok	k1gInPc2	útok
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
schopnosti	schopnost	k1gFnPc4	schopnost
sesílané	sesílaný	k2eAgFnPc4d1	sesílaná
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
Kog	Kog	k1gFnSc1	Kog
<g/>
'	'	kIx"	'
<g/>
mawa	mawa	k1gFnSc1	mawa
W	W	kA	W
<g/>
,	,	kIx,	,
Tristany	Tristan	k1gInPc7	Tristan
Q	Q	kA	Q
<g/>
,	,	kIx,	,
Dravenovo	Dravenův	k2eAgNnSc4d1	Dravenův
W	W	kA	W
<g/>
,	,	kIx,	,
Tryndamera	Tryndamera	k1gFnSc1	Tryndamera
R	R	kA	R
</s>
</p>
<p>
<s>
Schopnosti	schopnost	k1gFnPc1	schopnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
šampion	šampion	k1gMnSc1	šampion
napřahuje	napřahovat	k5eAaImIp3nS	napřahovat
<g/>
,	,	kIx,	,
natahuje	natahovat	k5eAaImIp3nS	natahovat
<g/>
,	,	kIx,	,
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
nějakou	nějaký	k3yIgFnSc4	nějaký
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
Sionovo	Sionův	k2eAgNnSc4d1	Sionův
Q	Q	kA	Q
<g/>
,	,	kIx,	,
Aatroxovo	Aatroxovo	k1gNnSc1	Aatroxovo
Q	Q	kA	Q
<g/>
,	,	kIx,	,
Pykea	Pykea	k1gFnSc1	Pykea
Q	Q	kA	Q
<g/>
,	,	kIx,	,
Xeratha	Xeratha	k1gMnSc1	Xeratha
Q	Q	kA	Q
</s>
</p>
<p>
<s>
Globální	globální	k2eAgFnPc1d1	globální
schopnosti	schopnost	k1gFnPc1	schopnost
–	–	k?	–
působí	působit	k5eAaImIp3nP	působit
na	na	k7c4	na
všechny	všechen	k3xTgMnPc4	všechen
protivníky	protivník	k1gMnPc4	protivník
nebo	nebo	k8xC	nebo
spoluhráče	spoluhráč	k1gMnPc4	spoluhráč
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
Nocturnovo	Nocturnův	k2eAgNnSc4d1	Nocturnův
R	R	kA	R
<g/>
,	,	kIx,	,
Soraky	Sorak	k1gInPc7	Sorak
R	R	kA	R
<g/>
,	,	kIx,	,
Karthusovo	Karthusův	k2eAgNnSc4d1	Karthusův
R	R	kA	R
<g/>
,	,	kIx,	,
Twisted	Twisted	k1gMnSc1	Twisted
Fateho	Fate	k1gMnSc2	Fate
RSchopnosti	RSchopnost	k1gFnSc2	RSchopnost
se	se	k3xPyFc4	se
šampion	šampion	k1gMnSc1	šampion
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
zápasu	zápas	k1gInSc2	zápas
učí	učit	k5eAaImIp3nS	učit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
začíná	začínat	k5eAaImIp3nS	začínat
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
schopností	schopnost	k1gFnSc7	schopnost
a	a	k8xC	a
za	za	k7c4	za
nějakou	nějaký	k3yIgFnSc4	nějaký
chvíli	chvíle	k1gFnSc4	chvíle
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
nasbírá	nasbírat	k5eAaPmIp3nS	nasbírat
dostatek	dostatek	k1gInSc4	dostatek
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
naučit	naučit	k5eAaPmF	naučit
další	další	k2eAgNnSc1d1	další
nebo	nebo	k8xC	nebo
stávající	stávající	k2eAgNnSc1d1	stávající
učinit	učinit	k5eAaPmF	učinit
silnější	silný	k2eAgInSc4d2	silnější
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
kratší	krátký	k2eAgFnSc7d2	kratší
přebíjecí	přebíjecí	k2eAgFnSc7d1	přebíjecí
dobou	doba	k1gFnSc7	doba
(	(	kIx(	(
<g/>
postup	postup	k1gInSc1	postup
do	do	k7c2	do
dalšího	další	k2eAgInSc2d1	další
levelu	level	k1gInSc2	level
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Utimátku	Utimátek	k1gInSc2	Utimátek
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
obvykle	obvykle	k6eAd1	obvykle
naučit	naučit	k5eAaPmF	naučit
až	až	k9	až
v	v	k7c6	v
levelu	level	k1gInSc6	level
6	[number]	k4	6
a	a	k8xC	a
vylepší	vylepšit	k5eAaPmIp3nP	vylepšit
ji	on	k3xPp3gFnSc4	on
nejdříve	dříve	k6eAd3	dříve
na	na	k7c6	na
levelu	level	k1gInSc6	level
11	[number]	k4	11
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
18	[number]	k4	18
levelů	level	k1gInPc2	level
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
pěti	pět	k4xCc2	pět
úrovním	úroveň	k1gFnPc3	úroveň
každé	každý	k3xTgFnSc2	každý
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
tří	tři	k4xCgFnPc2	tři
aktivních	aktivní	k2eAgFnPc2d1	aktivní
schopností	schopnost	k1gFnPc2	schopnost
a	a	k8xC	a
třem	tři	k4xCgFnPc3	tři
úrovním	úroveň	k1gFnPc3	úroveň
ultimátky	ultimátka	k1gFnSc2	ultimátka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
šampionů	šampion	k1gMnPc2	šampion
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
podobnými	podobný	k2eAgFnPc7d1	podobná
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ryze	ryze	k6eAd1	ryze
<g/>
,	,	kIx,	,
Pantheon	Pantheon	k1gInSc4	Pantheon
<g/>
,	,	kIx,	,
Shen	Shen	k1gInSc4	Shen
<g/>
,	,	kIx,	,
Tahmkench	Tahmkench	k1gInSc4	Tahmkench
a	a	k8xC	a
Twisted	Twisted	k1gInSc4	Twisted
Fate	Fat	k1gFnSc2	Fat
mají	mít	k5eAaImIp3nP	mít
jako	jako	k9	jako
ultimátní	ultimátní	k2eAgFnSc4d1	ultimátní
schopnost	schopnost	k1gFnSc4	schopnost
teleport	teleport	k1gInSc4	teleport
na	na	k7c4	na
určenou	určený	k2eAgFnSc4d1	určená
pozici	pozice	k1gFnSc4	pozice
nebo	nebo	k8xC	nebo
k	k	k7c3	k
určitému	určitý	k2eAgMnSc3d1	určitý
šampionovi	šampion	k1gMnSc3	šampion
</s>
</p>
<p>
<s>
Jinx	Jinx	k1gInSc1	Jinx
<g/>
,	,	kIx,	,
Draven	Draven	k2eAgInSc1d1	Draven
<g/>
,	,	kIx,	,
Ashe	Ash	k1gFnPc4	Ash
a	a	k8xC	a
Ezreal	Ezreal	k1gInSc4	Ezreal
mají	mít	k5eAaImIp3nP	mít
dalekonosnou	dalekonosný	k2eAgFnSc4d1	dalekonosná
mířenou	mířený	k2eAgFnSc4d1	mířená
střelu	střela	k1gFnSc4	střela
přes	přes	k7c4	přes
celou	celý	k2eAgFnSc4d1	celá
mapu	mapa	k1gFnSc4	mapa
</s>
</p>
<p>
<s>
===	===	k?	===
Typy	typa	k1gFnPc1	typa
šampionů	šampion	k1gMnPc2	šampion
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Zabiják	zabiják	k1gInSc4	zabiják
====	====	k?	====
</s>
</p>
<p>
<s>
Zabiják	zabiják	k1gInSc1	zabiják
(	(	kIx(	(
<g/>
Assassin	Assassin	k1gInSc1	Assassin
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
také	také	k9	také
vrah	vrah	k1gMnSc1	vrah
je	být	k5eAaImIp3nS	být
šampion	šampion	k1gMnSc1	šampion
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vítězí	vítězit	k5eAaImIp3nS	vítězit
v	v	k7c6	v
boji	boj	k1gInSc6	boj
1	[number]	k4	1
<g/>
v	v	k7c4	v
<g/>
1	[number]	k4	1
napříč	napříč	k6eAd1	napříč
různými	různý	k2eAgFnPc7d1	různá
rolemi	role	k1gFnPc7	role
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vždy	vždy	k6eAd1	vždy
z	z	k7c2	z
relativní	relativní	k2eAgFnSc2d1	relativní
blízkosti	blízkost	k1gFnSc2	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
úkol	úkol	k1gInSc1	úkol
je	být	k5eAaImIp3nS	být
co	co	k9	co
nejrychleji	rychle	k6eAd3	rychle
zabít	zabít	k5eAaPmF	zabít
cíl	cíl	k1gInSc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Kombinují	kombinovat	k5eAaImIp3nP	kombinovat
použití	použití	k1gNnSc4	použití
jejich	jejich	k3xOp3gFnPc2	jejich
schopností	schopnost	k1gFnPc2	schopnost
a	a	k8xC	a
základních	základní	k2eAgInPc2d1	základní
útoků	útok	k1gInPc2	útok
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
obrovské	obrovský	k2eAgInPc1d1	obrovský
<g/>
,	,	kIx,	,
leckdy	leckdy	k6eAd1	leckdy
i	i	k9	i
kombinované	kombinovaný	k2eAgNnSc4d1	kombinované
poškození	poškození	k1gNnSc4	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Mívají	mívat	k5eAaImIp3nP	mívat
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
nějaké	nějaký	k3yIgNnSc4	nějaký
kouzlo	kouzlo	k1gNnSc4	kouzlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jim	on	k3xPp3gMnPc3	on
pomůže	pomoct	k5eAaPmIp3nS	pomoct
se	se	k3xPyFc4	se
nějak	nějak	k6eAd1	nějak
přiblížit	přiblížit	k5eAaPmF	přiblížit
k	k	k7c3	k
oběti	oběť	k1gFnSc3	oběť
(	(	kIx(	(
<g/>
Katarina	Katarina	k1gFnSc1	Katarina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zastřít	zastřít	k5eAaPmF	zastřít
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
Akali	Akale	k1gFnSc4	Akale
<g/>
,	,	kIx,	,
Fizz	Fizz	k1gInSc4	Fizz
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
teleportovat	teleportovat	k5eAaPmF	teleportovat
(	(	kIx(	(
<g/>
Kassadin	Kassadin	k2eAgMnSc1d1	Kassadin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
schopností	schopnost	k1gFnSc7	schopnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jim	on	k3xPp3gMnPc3	on
pomůže	pomoct	k5eAaPmIp3nS	pomoct
se	se	k3xPyFc4	se
k	k	k7c3	k
nepříteli	nepřítel	k1gMnSc3	nepřítel
přiblížit	přiblížit	k5eAaPmF	přiblížit
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
začínají	začínat	k5eAaImIp3nP	začínat
(	(	kIx(	(
<g/>
Nocturne	Nocturn	k1gInSc5	Nocturn
<g/>
,	,	kIx,	,
Akali	Akal	k1gMnPc7	Akal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Působí	působit	k5eAaImIp3nS	působit
obrovské	obrovský	k2eAgNnSc4d1	obrovské
poškození	poškození	k1gNnSc4	poškození
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
až	až	k9	až
tak	tak	k6eAd1	tak
křehcí	křehký	k2eAgMnPc1d1	křehký
jako	jako	k8xC	jako
obyčejní	obyčejný	k2eAgMnPc1d1	obyčejný
mágové	mág	k1gMnPc1	mág
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
mobilní	mobilní	k2eAgInPc1d1	mobilní
(	(	kIx(	(
<g/>
Fizz	Fizz	k1gInSc1	Fizz
<g/>
,	,	kIx,	,
Camille	Camille	k1gFnSc1	Camille
<g/>
,	,	kIx,	,
Katarina	Katarina	k1gFnSc1	Katarina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
jejich	jejich	k3xOp3gFnPc1	jejich
schopnosti	schopnost	k1gFnPc1	schopnost
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgNnSc4d1	dobré
načasování	načasování	k1gNnSc4	načasování
<g/>
,	,	kIx,	,
taktiku	taktika	k1gFnSc4	taktika
a	a	k8xC	a
souslednost	souslednost	k1gFnSc4	souslednost
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
dostatečného	dostatečný	k2eAgInSc2d1	dostatečný
příjmu	příjem	k1gInSc2	příjem
zlaťáků	zlaťák	k1gInPc2	zlaťák
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
předmětů	předmět	k1gInPc2	předmět
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
fázi	fáze	k1gFnSc6	fáze
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
tankové	tankový	k2eAgFnPc1d1	tanková
a	a	k8xC	a
mágové	mág	k1gMnPc1	mág
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
výši	výše	k1gFnSc6	výše
<g/>
)	)	kIx)	)
nepoužitelní	použitelný	k2eNgMnPc1d1	nepoužitelný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Assasini	Assasin	k2eAgMnPc1d1	Assasin
chodí	chodit	k5eAaImIp3nP	chodit
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
mid	mid	k?	mid
nebo	nebo	k8xC	nebo
do	do	k7c2	do
džungle	džungle	k1gFnSc2	džungle
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
na	na	k7c4	na
top	topit	k5eAaImRp2nS	topit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
assasinů	assasin	k1gMnPc2	assasin
<g/>
:	:	kIx,	:
Akali	Akal	k1gMnPc1	Akal
<g/>
,	,	kIx,	,
Ekko	Ekko	k1gMnSc1	Ekko
<g/>
,	,	kIx,	,
Evelynn	Evelynn	k1gMnSc1	Evelynn
<g/>
,	,	kIx,	,
Kayn	Kayn	k1gMnSc1	Kayn
<g/>
,	,	kIx,	,
Zed	Zed	k1gMnSc1	Zed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mág	mág	k1gMnSc1	mág
===	===	k?	===
</s>
</p>
<p>
<s>
Mágové	mág	k1gMnPc1	mág
(	(	kIx(	(
<g/>
Mage	Mage	k1gInSc1	Mage
<g/>
)	)	kIx)	)
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
největší	veliký	k2eAgNnSc1d3	veliký
magické	magický	k2eAgNnSc1d1	magické
poškození	poškození	k1gNnSc1	poškození
z	z	k7c2	z
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Poškození	poškozený	k1gMnPc1	poškozený
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
výhradně	výhradně	k6eAd1	výhradně
svými	svůj	k3xOyFgFnPc7	svůj
schopnostmi	schopnost	k1gFnPc7	schopnost
s	s	k7c7	s
krátkou	krátký	k2eAgFnSc7d1	krátká
<g/>
/	/	kIx~	/
<g/>
středně	středně	k6eAd1	středně
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
přebíjecí	přebíjecí	k2eAgFnSc7d1	přebíjecí
dobou	doba	k1gFnSc7	doba
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
ultimátky	ultimátek	k1gInPc1	ultimátek
mívají	mívat	k5eAaImIp3nP	mívat
ničivý	ničivý	k2eAgInSc4d1	ničivý
chrarakter	chrarakter	k1gInSc4	chrarakter
<g/>
,	,	kIx,	,
schopný	schopný	k2eAgMnSc1d1	schopný
zvrátit	zvrátit	k5eAaPmF	zvrátit
průběh	průběh	k1gInSc4	průběh
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jí	jíst	k5eAaImIp3nS	jíst
vyvolávač	vyvolávač	k1gMnSc1	vyvolávač
dobře	dobře	k6eAd1	dobře
použije	použít	k5eAaPmIp3nS	použít
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
magického	magický	k2eAgNnSc2d1	magické
poškození	poškození	k1gNnSc2	poškození
aplikují	aplikovat	k5eAaBmIp3nP	aplikovat
na	na	k7c4	na
protivníky	protivník	k1gMnPc4	protivník
omezující	omezující	k2eAgInPc1d1	omezující
účinky	účinek	k1gInPc1	účinek
jako	jako	k8xS	jako
umlčení	umlčení	k1gNnSc1	umlčení
<g/>
,	,	kIx,	,
znehybnění	znehybnění	k1gNnSc1	znehybnění
a	a	k8xC	a
uzemnění	uzemnění	k1gNnSc1	uzemnění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jim	on	k3xPp3gMnPc3	on
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
jejich	jejich	k3xOp3gNnSc3	jejich
tzv.	tzv.	kA	tzv.
komb	komb	k1gInSc1	komb
–	–	k?	–
sousled	sousled	k1gInSc1	sousled
po	po	k7c6	po
sobě	se	k3xPyFc3	se
jdoucích	jdoucí	k2eAgFnPc2d1	jdoucí
schopností	schopnost	k1gFnPc2	schopnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zaručují	zaručovat	k5eAaImIp3nP	zaručovat
téměř	téměř	k6eAd1	téměř
jistou	jistý	k2eAgFnSc4d1	jistá
smrt	smrt	k1gFnSc4	smrt
(	(	kIx(	(
<g/>
Ryze	ryze	k6eAd1	ryze
<g/>
,	,	kIx,	,
Lux	Lux	k1gMnSc1	Lux
<g/>
)	)	kIx)	)
v	v	k7c6	v
boji	boj	k1gInSc6	boj
1	[number]	k4	1
<g/>
v	v	k7c4	v
<g/>
1	[number]	k4	1
a	a	k8xC	a
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
schopnost	schopnost	k1gFnSc1	schopnost
působení	působení	k1gNnSc2	působení
obrovského	obrovský	k2eAgNnSc2d1	obrovské
nárazového	nárazový	k2eAgNnSc2d1	nárazové
poškození	poškození	k1gNnSc2	poškození
je	být	k5eAaImIp3nS	být
vyvážena	vyvážen	k2eAgFnSc1d1	vyvážena
jejich	jejich	k3xOp3gFnSc7	jejich
křehkostí	křehkost	k1gFnSc7	křehkost
<g/>
,	,	kIx,	,
nízkým	nízký	k2eAgNnSc7d1	nízké
zdravím	zdraví	k1gNnSc7	zdraví
a	a	k8xC	a
odolnostmi	odolnost	k1gFnPc7	odolnost
a	a	k8xC	a
absencí	absence	k1gFnSc7	absence
escapů	escap	k1gMnPc2	escap
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
mají	mít	k5eAaImIp3nP	mít
jako	jako	k9	jako
sekundární	sekundární	k2eAgFnPc1d1	sekundární
schopnosti	schopnost	k1gFnPc1	schopnost
různých	různý	k2eAgNnPc2d1	různé
kouzel	kouzlo	k1gNnPc2	kouzlo
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
štíty	štít	k1gInPc1	štít
(	(	kIx(	(
<g/>
Annie	Annie	k1gFnSc1	Annie
<g/>
,	,	kIx,	,
Lux	Lux	k1gMnSc1	Lux
<g/>
,	,	kIx,	,
Ryze	ryze	k6eAd1	ryze
<g/>
,	,	kIx,	,
Kasssadin	Kasssadina	k1gFnPc2	Kasssadina
<g/>
,	,	kIx,	,
Malzahar	Malzahara	k1gFnPc2	Malzahara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mágové	mág	k1gMnPc1	mág
chodí	chodit	k5eAaImIp3nP	chodit
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
mid	mid	k?	mid
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkami	výjimka	k1gFnPc7	výjimka
jako	jako	k8xS	jako
podpora	podpora	k1gFnSc1	podpora
nebo	nebo	k8xC	nebo
na	na	k7c6	na
top	topit	k5eAaImRp2nS	topit
<g/>
/	/	kIx~	/
<g/>
džungle	džungle	k1gFnSc2	džungle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
mágů	mág	k1gMnPc2	mág
<g/>
:	:	kIx,	:
Lux	Lux	k1gMnSc1	Lux
<g/>
,	,	kIx,	,
Swain	Swain	k1gMnSc1	Swain
<g/>
,	,	kIx,	,
Ryze	ryze	k6eAd1	ryze
<g/>
,	,	kIx,	,
Karthus	Karthus	k1gInSc1	Karthus
<g/>
,	,	kIx,	,
Aurelion	Aurelion	k1gInSc1	Aurelion
Sol	sol	k1gInSc1	sol
<g/>
,	,	kIx,	,
Syndra	Syndra	k1gFnSc1	Syndra
<g/>
,	,	kIx,	,
Malzahar	Malzahar	k1gInSc1	Malzahar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bojovník	bojovník	k1gMnSc1	bojovník
===	===	k?	===
</s>
</p>
<p>
<s>
Bojovníci	bojovník	k1gMnPc1	bojovník
(	(	kIx(	(
<g/>
Fighter	fighter	k1gMnSc1	fighter
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
šampioni	šampion	k1gMnPc1	šampion
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
jakýsi	jakýsi	k3yIgInSc4	jakýsi
střed	střed	k1gInSc4	střed
mezi	mezi	k7c7	mezi
odolností	odolnost	k1gFnSc7	odolnost
tanků	tank	k1gInPc2	tank
a	a	k8xC	a
schopností	schopnost	k1gFnPc2	schopnost
působit	působit	k5eAaImF	působit
poškození	poškození	k1gNnSc4	poškození
jako	jako	k8xS	jako
assasini	assasin	k2eAgMnPc1d1	assasin
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
není	být	k5eNaImIp3nS	být
tank	tank	k1gInSc4	tank
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
fighter	fighter	k1gMnSc1	fighter
obvykle	obvykle	k6eAd1	obvykle
začíná	začínat	k5eAaImIp3nS	začínat
teamfight	teamfight	k5eAaPmF	teamfight
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
takové	takový	k3xDgNnSc4	takový
zdraví	zdraví	k1gNnSc4	zdraví
jako	jako	k8xC	jako
tank	tank	k1gInSc4	tank
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
disponuje	disponovat	k5eAaBmIp3nS	disponovat
nějakou	nějaký	k3yIgFnSc7	nějaký
schopností	schopnost	k1gFnSc7	schopnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ho	on	k3xPp3gMnSc4	on
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zbavit	zbavit	k5eAaPmF	zbavit
se	se	k3xPyFc4	se
omezujících	omezující	k2eAgInPc2d1	omezující
účinků	účinek	k1gInPc2	účinek
(	(	kIx(	(
<g/>
Gangplank	Gangplank	k1gInSc1	Gangplank
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
se	se	k3xPyFc4	se
učinit	učinit	k5eAaPmF	učinit
nezaměřitelným	zaměřitelný	k2eNgNnSc7d1	zaměřitelný
(	(	kIx(	(
<g/>
Fizz	Fizz	k1gInSc1	Fizz
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
na	na	k7c4	na
do	do	k7c2	do
teamfightu	teamfighto	k1gNnSc3	teamfighto
přinést	přinést	k5eAaPmF	přinést
nějaké	nějaký	k3yIgNnSc4	nějaký
plošné	plošný	k2eAgNnSc4d1	plošné
poškození	poškození	k1gNnSc4	poškození
či	či	k8xC	či
omezující	omezující	k2eAgInSc4d1	omezující
účinek	účinek	k1gInSc4	účinek
(	(	kIx(	(
<g/>
Aurelion	Aurelion	k1gInSc1	Aurelion
Sol	sol	k1gInSc1	sol
<g/>
,	,	kIx,	,
Pantheon	Pantheon	k1gInSc1	Pantheon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yIgInSc3	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
pak	pak	k6eAd1	pak
větší	veliký	k2eAgFnSc4d2	veliký
výdrž	výdrž	k1gFnSc4	výdrž
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
se	se	k3xPyFc4	se
udrží	udržet	k5eAaPmIp3nP	udržet
v	v	k7c6	v
boji	boj	k1gInSc6	boj
déle	dlouho	k6eAd2	dlouho
kvůli	kvůli	k7c3	kvůli
jejich	jejich	k3xOp3gFnPc3	jejich
ultimátkám	ultimátka	k1gFnPc3	ultimátka
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
oživí	oživit	k5eAaPmIp3nS	oživit
nebo	nebo	k8xC	nebo
vyléčí	vyléčit	k5eAaPmIp3nS	vyléčit
či	či	k8xC	či
změní	změnit	k5eAaPmIp3nS	změnit
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mají	mít	k5eAaImIp3nP	mít
více	hodně	k6eAd2	hodně
zdraví	zdraví	k1gNnSc4	zdraví
(	(	kIx(	(
<g/>
Aatrox	Aatrox	k1gInSc1	Aatrox
<g/>
,	,	kIx,	,
Ekko	Ekko	k6eAd1	Ekko
<g/>
,	,	kIx,	,
Shyvana	Shyvan	k1gMnSc4	Shyvan
<g/>
,	,	kIx,	,
Swain	Swain	k1gMnSc1	Swain
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
se	se	k3xPyFc4	se
dokáží	dokázat	k5eAaPmIp3nP	dokázat
rychle	rychle	k6eAd1	rychle
teleportovat	teleportovat	k5eAaImF	teleportovat
mezi	mezi	k7c7	mezi
protivníky	protivník	k1gMnPc7	protivník
a	a	k8xC	a
zahájit	zahájit	k5eAaPmF	zahájit
tak	tak	k6eAd1	tak
teamfight	teamfight	k5eAaPmF	teamfight
<g/>
,	,	kIx,	,
dorazit	dorazit	k5eAaPmF	dorazit
protivníka	protivník	k1gMnSc4	protivník
nebo	nebo	k8xC	nebo
zachránit	zachránit	k5eAaPmF	zachránit
spoluhráče	spoluhráč	k1gMnSc4	spoluhráč
(	(	kIx(	(
<g/>
Pantheon	Pantheon	k1gInSc1	Pantheon
<g/>
,	,	kIx,	,
Twisted	Twisted	k1gInSc1	Twisted
Fate	Fate	k1gInSc1	Fate
<g/>
,	,	kIx,	,
Shen	Shen	k1gInSc1	Shen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fighteři	Fighter	k1gMnPc1	Fighter
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
topu	topa	k1gFnSc4	topa
a	a	k8xC	a
midu	mida	k1gFnSc4	mida
<g/>
,	,	kIx,	,
v	v	k7c6	v
džungli	džungle	k1gFnSc6	džungle
či	či	k8xC	či
jako	jako	k8xS	jako
podpora	podpora	k1gFnSc1	podpora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
typických	typický	k2eAgMnPc2d1	typický
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
:	:	kIx,	:
Pantheon	Pantheon	k1gInSc4	Pantheon
<g/>
,	,	kIx,	,
Shyvana	Shyvan	k1gMnSc4	Shyvan
<g/>
,	,	kIx,	,
Trundle	Trundle	k1gMnSc4	Trundle
<g/>
,	,	kIx,	,
Shen	Shen	k1gMnSc1	Shen
<g/>
,	,	kIx,	,
Gangplank	Gangplank	k1gMnSc1	Gangplank
<g/>
,	,	kIx,	,
Mordekaiser	Mordekaiser	k1gMnSc1	Mordekaiser
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podpora	podpora	k1gFnSc1	podpora
===	===	k?	===
</s>
</p>
<p>
<s>
Podpora	podpora	k1gFnSc1	podpora
(	(	kIx(	(
<g/>
Support	support	k1gInSc1	support
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
League	League	k1gFnSc6	League
of	of	k?	of
Legends	Legends	k1gInSc4	Legends
nejdůležitější	důležitý	k2eAgFnSc2d3	nejdůležitější
role	role	k1gFnSc2	role
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
celému	celý	k2eAgInSc3d1	celý
týmu	tým	k1gInSc3	tým
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
nemá	mít	k5eNaImIp3nS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
mít	mít	k5eAaImF	mít
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
zářezů	zářez	k1gInPc2	zářez
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
)	)	kIx)	)
nežádoucí	žádoucí	k2eNgFnPc4d1	nežádoucí
<g/>
.	.	kIx.	.
</s>
<s>
Chodí	chodit	k5eAaImIp3nP	chodit
totiž	totiž	k9	totiž
na	na	k7c4	na
bot	bota	k1gFnPc2	bota
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ADC-	ADC-	k1gMnSc7	ADC-
střelcem	střelec	k1gMnSc7	střelec
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
právě	právě	k9	právě
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
předmětů	předmět	k1gInPc2	předmět
do	do	k7c2	do
pozdější	pozdní	k2eAgFnSc2d2	pozdější
fáze	fáze	k1gFnSc2	fáze
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dobře	dobře	k6eAd1	dobře
zahraný	zahraný	k2eAgInSc1d1	zahraný
support	support	k1gInSc1	support
chrání	chránit	k5eAaImIp3nS	chránit
své	svůj	k3xOyFgMnPc4	svůj
spoluhráče	spoluhráč	k1gMnPc4	spoluhráč
svým	svůj	k3xOyFgNnSc7	svůj
vlastním	vlastní	k2eAgNnSc7d1	vlastní
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
stoupá	stoupat	k5eAaImIp3nS	stoupat
si	se	k3xPyFc3	se
před	před	k7c4	před
mířené	mířený	k2eAgFnPc4d1	mířená
střely	střela	k1gFnPc4	střela
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc1	ten
ostatní	ostatní	k2eAgNnSc1d1	ostatní
nezabilo	zabít	k5eNaPmAgNnS	zabít
<g/>
,	,	kIx,	,
i	i	k8xC	i
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
flashe	flash	k1gFnSc2	flash
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc7	jeho
důležitým	důležitý	k2eAgInSc7d1	důležitý
úkolem	úkol	k1gInSc7	úkol
tvořit	tvořit	k5eAaImF	tvořit
vizi	vize	k1gFnSc4	vize
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vize	vize	k1gFnSc1	vize
tvoří	tvořit	k5eAaImIp3nS	tvořit
základní	základní	k2eAgInSc4d1	základní
kámen	kámen	k1gInSc4	kámen
k	k	k7c3	k
cestě	cesta	k1gFnSc3	cesta
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Nakupuje	nakupovat	k5eAaBmIp3nS	nakupovat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
totemy	totem	k1gInPc1	totem
<g/>
,	,	kIx,	,
dalekozraké	dalekozraký	k2eAgInPc1d1	dalekozraký
totemy	totem	k1gInPc1	totem
a	a	k8xC	a
čočky	čočka	k1gFnPc1	čočka
k	k	k7c3	k
odstraňování	odstraňování	k1gNnSc3	odstraňování
nepřátelských	přátelský	k2eNgInPc2d1	nepřátelský
totemů	totem	k1gInPc2	totem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
očividnému	očividný	k2eAgNnSc3d1	očividné
dělení	dělení	k1gNnSc3	dělení
druhů	druh	k1gInPc2	druh
podpor	podpora	k1gFnPc2	podpora
i	i	k9	i
podle	podle	k7c2	podle
Riotu	Riot	k1gInSc2	Riot
<g/>
,	,	kIx,	,
uveďme	uvést	k5eAaPmRp1nP	uvést
jejich	jejich	k3xOp3gNnSc4	jejich
členění	členění	k1gNnSc4	členění
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Healer	Healer	k1gInSc1	Healer
–	–	k?	–
Disponuje	disponovat	k5eAaBmIp3nS	disponovat
kouzly	kouzlo	k1gNnPc7	kouzlo
sesílající	sesílající	k2eAgInPc4d1	sesílající
mocné	mocný	k2eAgInPc4d1	mocný
štíty	štít	k1gInPc4	štít
nebo	nebo	k8xC	nebo
dokáže	dokázat	k5eAaPmIp3nS	dokázat
přímo	přímo	k6eAd1	přímo
doplnit	doplnit	k5eAaPmF	doplnit
určité	určitý	k2eAgFnPc4d1	určitá
hodnoty	hodnota	k1gFnPc4	hodnota
zdraví	zdravý	k2eAgMnPc1d1	zdravý
spoluhráči	spoluhráč	k1gMnPc1	spoluhráč
<g/>
.	.	kIx.	.
</s>
<s>
Dokáží	dokázat	k5eAaPmIp3nP	dokázat
také	také	k9	také
zrychlit	zrychlit	k5eAaPmF	zrychlit
rychlost	rychlost	k1gFnSc4	rychlost
útoků	útok	k1gInPc2	útok
a	a	k8xC	a
pohybu	pohyb	k1gInSc2	pohyb
spojenců	spojenec	k1gMnPc2	spojenec
a	a	k8xC	a
jako	jako	k9	jako
svou	svůj	k3xOyFgFnSc7	svůj
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
jedinou	jediný	k2eAgFnSc4d1	jediná
útočnou	útočný	k2eAgFnSc4d1	útočná
schopnost	schopnost	k1gFnSc4	schopnost
mají	mít	k5eAaImIp3nP	mít
relativně	relativně	k6eAd1	relativně
mocnou	mocný	k2eAgFnSc7d1	mocná
ultimátkou	ultimátka	k1gFnSc7	ultimátka
s	s	k7c7	s
omezujícími	omezující	k2eAgInPc7d1	omezující
účinky	účinek	k1gInPc7	účinek
<g/>
.	.	kIx.	.
<g/>
Příklady	příklad	k1gInPc1	příklad
typických	typický	k2eAgInPc2d1	typický
healerů	healer	k1gInPc2	healer
<g/>
:	:	kIx,	:
Sona	Sonus	k1gMnSc4	Sonus
<g/>
,	,	kIx,	,
Soraka	Sorak	k1gMnSc4	Sorak
<g/>
,	,	kIx,	,
Karma	karma	k1gFnSc1	karma
<g/>
,	,	kIx,	,
Nami	Name	k1gFnSc4	Name
<g/>
,	,	kIx,	,
Lulu	lula	k1gFnSc4	lula
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tank	tank	k1gInSc1	tank
–	–	k?	–
tankovitá	tankovitý	k2eAgFnSc1d1	tankovitý
podpora	podpora	k1gFnSc1	podpora
je	být	k5eAaImIp3nS	být
přesně	přesně	k6eAd1	přesně
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
tým	tým	k1gInSc1	tým
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nemá	mít	k5eNaImIp3nS	mít
pravého	pravý	k2eAgMnSc4d1	pravý
tanka	tanek	k1gMnSc4	tanek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
podpory	podpora	k1gFnSc2	podpora
sice	sice	k8xC	sice
nepůsobí	působit	k5eNaImIp3nS	působit
závratné	závratný	k2eAgNnSc1d1	závratné
poškození	poškození	k1gNnSc1	poškození
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
leccos	leccos	k3yInSc4	leccos
vydrží	vydržet	k5eAaPmIp3nS	vydržet
a	a	k8xC	a
disponuje	disponovat	k5eAaBmIp3nS	disponovat
obrovskými	obrovský	k2eAgFnPc7d1	obrovská
možnostmi	možnost	k1gFnPc7	možnost
v	v	k7c6	v
omezujících	omezující	k2eAgInPc6d1	omezující
účincích	účinek	k1gInPc6	účinek
<g/>
.	.	kIx.	.
<g/>
Příklady	příklad	k1gInPc1	příklad
typických	typický	k2eAgFnPc2d1	typická
tankovitých	tankovitý	k2eAgFnPc2d1	tankovitý
podpor	podpora	k1gFnPc2	podpora
<g/>
:	:	kIx,	:
Rakan	Rakan	k1gMnSc1	Rakan
<g/>
,	,	kIx,	,
Alistar	Alistar	k1gMnSc1	Alistar
<g/>
,	,	kIx,	,
Leona	Leona	k1gFnSc1	Leona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fighter	fighter	k1gMnSc1	fighter
–	–	k?	–
podpora	podpora	k1gFnSc1	podpora
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
jednoho	jeden	k4xCgMnSc4	jeden
protivníka	protivník	k1gMnSc4	protivník
omráčit	omráčit	k5eAaPmF	omráčit
a	a	k8xC	a
přitáhnout	přitáhnout	k5eAaPmF	přitáhnout
ho	on	k3xPp3gMnSc4	on
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
<g/>
Příklady	příklad	k1gInPc1	příklad
fighter	fighter	k1gMnSc1	fighter
supportů	support	k1gInPc2	support
–	–	k?	–
Thresh	Thresh	k1gInSc1	Thresh
<g/>
,	,	kIx,	,
Blitzcrank	Blitzcrank	k1gInSc1	Blitzcrank
<g/>
,	,	kIx,	,
Pyke	Pyke	k1gInSc1	Pyke
</s>
</p>
<p>
<s>
Mág	mág	k1gMnSc1	mág
–	–	k?	–
občas	občas	k6eAd1	občas
mágové	mág	k1gMnPc1	mág
z	z	k7c2	z
midu	midus	k1gInSc2	midus
chodí	chodit	k5eAaImIp3nS	chodit
jako	jako	k9	jako
podpora	podpora	k1gFnSc1	podpora
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
poškození	poškození	k1gNnSc4	poškození
sami	sám	k3xTgMnPc1	sám
o	o	k7c4	o
sobě	se	k3xPyFc3	se
a	a	k8xC	a
nepotřebují	potřebovat	k5eNaImIp3nP	potřebovat
tolik	tolik	k4xDc4	tolik
předmětů	předmět	k1gInPc2	předmět
na	na	k7c4	na
zvýšení	zvýšení	k1gNnSc4	zvýšení
své	svůj	k3xOyFgFnSc2	svůj
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
můžou	můžou	k?	můžou
kupovat	kupovat	k5eAaImF	kupovat
typické	typický	k2eAgInPc4d1	typický
předměty	předmět	k1gInPc4	předmět
pro	pro	k7c4	pro
podpory	podpora	k1gFnPc4	podpora
(	(	kIx(	(
<g/>
např.	např.	kA	např.
totemy	totem	k1gInPc4	totem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Příklady	příklad	k1gInPc1	příklad
mágů	mág	k1gMnPc2	mág
<g/>
,	,	kIx,	,
používající-se	používajícíe	k1gFnSc2	používající-se
jako	jako	k8xS	jako
podpory	podpora	k1gFnSc2	podpora
<g/>
:	:	kIx,	:
Annie	Annie	k1gFnSc1	Annie
<g/>
,	,	kIx,	,
Anivia	Anivia	k1gFnSc1	Anivia
<g/>
,	,	kIx,	,
Brand	Brand	k1gMnSc1	Brand
<g/>
,	,	kIx,	,
Lux	Lux	k1gMnSc1	Lux
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Support	support	k1gInSc1	support
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
počáteční	počáteční	k2eAgFnSc6d1	počáteční
fázi	fáze	k1gFnSc6	fáze
vždy	vždy	k6eAd1	vždy
na	na	k7c4	na
botu	bota	k1gFnSc4	bota
se	s	k7c7	s
svým	svůj	k1gMnSc7	svůj
ADC	ADC	kA	ADC
<g/>
,	,	kIx,	,
o	o	k7c6	o
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
samotné	samotný	k2eAgFnSc2d1	samotná
ADC	ADC	kA	ADC
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
křehké	křehký	k2eAgNnSc1d1	křehké
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
fázi	fáze	k1gFnSc6	fáze
hry	hra	k1gFnSc2	hra
přechází	přecházet	k5eAaImIp3nS	přecházet
mezi	mezi	k7c7	mezi
lajnami	lajna	k1gFnPc7	lajna
<g/>
,	,	kIx,	,
pokládá	pokládat	k5eAaImIp3nS	pokládat
totemy	totem	k1gInPc4	totem
<g/>
,	,	kIx,	,
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
ty	ten	k3xDgInPc1	ten
nepřátelské	přátelský	k2eNgInPc1d1	nepřátelský
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
spoluhráčům	spoluhráč	k1gMnPc3	spoluhráč
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
jde	jít	k5eAaImIp3nS	jít
vyhealovat	vyhealovat	k5eAaPmF	vyhealovat
<g/>
,	,	kIx,	,
pomoct	pomoct	k5eAaPmF	pomoct
jim	on	k3xPp3gMnPc3	on
rozbít	rozbít	k5eAaPmF	rozbít
věž	věž	k1gFnSc4	věž
<g/>
,	,	kIx,	,
zabít	zabít	k5eAaPmF	zabít
vlnu	vlna	k1gFnSc4	vlna
poskoků	poskok	k1gInPc2	poskok
<g/>
,	,	kIx,	,
jít	jít	k5eAaImF	jít
zabít	zabít	k5eAaPmF	zabít
nějakého	nějaký	k3yIgMnSc4	nějaký
protivníka	protivník	k1gMnSc4	protivník
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
pozdní	pozdní	k2eAgFnSc6d1	pozdní
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
pozdní	pozdní	k2eAgFnSc6d1	pozdní
fázi	fáze	k1gFnSc6	fáze
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vždy	vždy	k6eAd1	vždy
někde	někde	k6eAd1	někde
kolem	kolem	k7c2	kolem
celého	celý	k2eAgInSc2d1	celý
teamfightu	teamfight	k1gInSc2	teamfight
a	a	k8xC	a
stará	starat	k5eAaImIp3nS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
všechny	všechen	k3xTgMnPc4	všechen
své	svůj	k3xOyFgMnPc4	svůj
spoluhráče	spoluhráč	k1gMnPc4	spoluhráč
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
ostatní	ostatní	k2eAgMnPc1d1	ostatní
starají	starat	k5eAaImIp3nP	starat
o	o	k7c4	o
působení	působení	k1gNnSc4	působení
největšího	veliký	k2eAgNnSc2d3	veliký
poškození	poškození	k1gNnSc2	poškození
a	a	k8xC	a
zabití	zabití	k1gNnSc2	zabití
<g/>
,	,	kIx,	,
support	support	k1gInSc1	support
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
omezujícími	omezující	k2eAgInPc7d1	omezující
účinky	účinek	k1gInPc7	účinek
<g/>
,	,	kIx,	,
healováním	healování	k1gNnSc7	healování
nebo	nebo	k8xC	nebo
také	také	k9	také
působením	působení	k1gNnSc7	působení
(	(	kIx(	(
<g/>
ne	ne	k9	ne
však	však	k9	však
smrtelného	smrtelný	k2eAgMnSc2d1	smrtelný
<g/>
)	)	kIx)	)
poškození	poškození	k1gNnSc2	poškození
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podpora	podpora	k1gFnSc1	podpora
je	být	k5eAaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
podceňovaná	podceňovaný	k2eAgFnSc1d1	podceňovaná
role	role	k1gFnSc1	role
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dokládá	dokládat	k5eAaImIp3nS	dokládat
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
Riot	Riot	k1gMnSc1	Riot
a	a	k8xC	a
kompenzuje	kompenzovat	k5eAaBmIp3nS	kompenzovat
to	ten	k3xDgNnSc1	ten
třeba	třeba	k6eAd1	třeba
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
vyvolávač	vyvolávač	k1gMnSc1	vyvolávač
jde	jít	k5eAaImIp3nS	jít
hrát	hrát	k5eAaImF	hrát
roli	role	k1gFnSc4	role
podpory	podpora	k1gFnSc2	podpora
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
hru	hra	k1gFnSc4	hra
zaručenou	zaručený	k2eAgFnSc4d1	zaručená
takovou	takový	k3xDgFnSc4	takový
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
zvolí	zvolit	k5eAaPmIp3nS	zvolit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
pravidlo	pravidlo	k1gNnSc1	pravidlo
automatického	automatický	k2eAgNnSc2d1	automatické
doplnění	doplnění	k1gNnSc2	doplnění
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Střelec	Střelec	k1gMnSc1	Střelec
===	===	k?	===
</s>
</p>
<p>
<s>
Střelci	Střelec	k1gMnPc1	Střelec
(	(	kIx(	(
<g/>
Marksman	Marksman	k1gMnSc1	Marksman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ADC	ADC	kA	ADC
(	(	kIx(	(
<g/>
attack	attack	k1gMnSc1	attack
damage	damag	k1gFnSc2	damag
carry	carra	k1gFnSc2	carra
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
křehcí	křehký	k2eAgMnPc1d1	křehký
šampioni	šampion	k1gMnPc1	šampion
s	s	k7c7	s
útoky	útok	k1gInPc7	útok
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
působit	působit	k5eAaImF	působit
obrovské	obrovský	k2eAgNnSc4d1	obrovské
poškození	poškození	k1gNnSc4	poškození
svými	svůj	k3xOyFgInPc7	svůj
základními	základní	k2eAgInPc7d1	základní
útoky	útok	k1gInPc7	útok
a	a	k8xC	a
podporovat	podporovat	k5eAaImF	podporovat
je	být	k5eAaImIp3nS	být
svými	svůj	k3xOyFgFnPc7	svůj
schopnostmi	schopnost	k1gFnPc7	schopnost
poskytujícími	poskytující	k2eAgFnPc7d1	poskytující
např.	např.	kA	např.
zpomalení	zpomalení	k1gNnSc1	zpomalení
(	(	kIx(	(
<g/>
Twitch	Twitch	k1gInSc1	Twitch
<g/>
,	,	kIx,	,
Miss	miss	k1gFnSc1	miss
Fortune	Fortun	k1gInSc5	Fortun
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gNnSc4	jejich
dorážení	dorážení	k1gNnSc4	dorážení
na	na	k7c4	na
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
(	(	kIx(	(
<g/>
Ashe	Ashe	k1gFnSc1	Ashe
<g/>
,	,	kIx,	,
Caitlyn	Caitlyn	k1gInSc1	Caitlyn
<g/>
,	,	kIx,	,
Draven	Draven	k2eAgInSc1d1	Draven
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
mít	mít	k5eAaImF	mít
dostatek	dostatek	k1gInSc4	dostatek
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
zvyšujících	zvyšující	k2eAgInPc2d1	zvyšující
jejich	jejich	k3xOp3gNnSc7	jejich
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
kritický	kritický	k2eAgInSc4d1	kritický
zásah	zásah	k1gInSc4	zásah
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc4	rychlost
útoků	útok	k1gInPc2	útok
a	a	k8xC	a
útočné	útočný	k2eAgNnSc4d1	útočné
poškození	poškození	k1gNnSc4	poškození
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ADC	ADC	kA	ADC
mívají	mívat	k5eAaImIp3nP	mívat
tzv.	tzv.	kA	tzv.
escapy	escapa	k1gFnSc2	escapa
–	–	k?	–
rychlé	rychlý	k2eAgInPc4d1	rychlý
přesuny	přesun	k1gInPc4	přesun
<g/>
,	,	kIx,	,
teleportaci	teleportace	k1gFnSc4	teleportace
<g/>
,	,	kIx,	,
přeskoky	přeskok	k1gInPc4	přeskok
a	a	k8xC	a
odpálení	odpálení	k1gNnSc4	odpálení
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc4d2	vyšší
základní	základní	k2eAgFnSc4d1	základní
rychlost	rychlost	k1gFnSc4	rychlost
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
chodí	chodit	k5eAaImIp3nS	chodit
s	s	k7c7	s
výjimkami	výjimka	k1gFnPc7	výjimka
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
bot	bota	k1gFnPc2	bota
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
supportem	support	k1gInSc7	support
-	-	kIx~	-
podporou	podpora	k1gFnSc7	podpora
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
ADC	ADC	kA	ADC
<g/>
:	:	kIx,	:
Ashe	Ash	k1gMnSc2	Ash
<g/>
,	,	kIx,	,
Caitlyn	Caitlyn	k1gNnSc1	Caitlyn
<g/>
,	,	kIx,	,
Miss	miss	k1gFnSc1	miss
Fortune	Fortun	k1gInSc5	Fortun
<g/>
,	,	kIx,	,
Varus	Varus	k1gMnSc1	Varus
<g/>
,	,	kIx,	,
Twitch	Twitch	k1gMnSc1	Twitch
<g/>
,	,	kIx,	,
Tristana	Tristana	k1gFnSc1	Tristana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tank	tank	k1gInSc4	tank
===	===	k?	===
</s>
</p>
<p>
<s>
Tank	tank	k1gInSc1	tank
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
šampiona	šampion	k1gMnSc4	šampion
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
nelze	lze	k6eNd1	lze
tak	tak	k6eAd1	tak
snadno	snadno	k6eAd1	snadno
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k6eAd1	mnoho
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
vysokou	vysoký	k2eAgFnSc4d1	vysoká
regeneraci	regenerace	k1gFnSc4	regenerace
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc4	schopnost
potlačit	potlačit	k5eAaPmF	potlačit
omezující	omezující	k2eAgInPc4d1	omezující
účinky	účinek	k1gInPc4	účinek
a	a	k8xC	a
mívá	mívat	k5eAaImIp3nS	mívat
navíc	navíc	k6eAd1	navíc
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
štíty	štít	k1gInPc1	štít
<g/>
.	.	kIx.	.
</s>
<s>
Uděluje	udělovat	k5eAaImIp3nS	udělovat
jen	jen	k9	jen
malé	malý	k2eAgNnSc1d1	malé
poškození	poškození	k1gNnSc1	poškození
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
s	s	k7c7	s
nějakým	nějaký	k3yIgInSc7	nějaký
omezujícím	omezující	k2eAgInSc7d1	omezující
účinkem	účinek	k1gInSc7	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
působí	působit	k5eAaImIp3nS	působit
plošné	plošný	k2eAgNnSc1d1	plošné
poškození	poškození	k1gNnSc1	poškození
(	(	kIx(	(
<g/>
Singed	Singed	k1gInSc1	Singed
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Mundo	Mundo	k1gNnSc1	Mundo
<g/>
,	,	kIx,	,
Leona	Leona	k1gFnSc1	Leona
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
mobilní	mobilní	k2eAgNnPc1d1	mobilní
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc7	jejich
hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
potankovat	potankovat	k5eAaPmF	potankovat
své	svůj	k3xOyFgMnPc4	svůj
spoluhráče	spoluhráč	k1gMnPc4	spoluhráč
<g/>
;	;	kIx,	;
převést	převést	k5eAaPmF	převést
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
poškození	poškození	k1gNnSc2	poškození
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ostatní	ostatní	k2eAgMnPc1d1	ostatní
nebyli	být	k5eNaImAgMnP	být
tak	tak	k9	tak
poškozováni	poškozován	k2eAgMnPc1d1	poškozován
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
tedy	tedy	k9	tedy
zahajují	zahajovat	k5eAaImIp3nP	zahajovat
teamfighty	teamfighta	k1gFnPc1	teamfighta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vydrží	vydržet	k5eAaPmIp3nS	vydržet
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
protihráče	protihráč	k1gMnSc2	protihráč
plošně	plošně	k6eAd1	plošně
poškozují	poškozovat	k5eAaImIp3nP	poškozovat
a	a	k8xC	a
omezují	omezovat	k5eAaImIp3nP	omezovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gInPc4	on
mohli	moct	k5eAaImAgMnP	moct
ostatní	ostatní	k2eAgMnPc1d1	ostatní
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
i	i	k9	i
oni	onen	k3xDgMnPc1	onen
<g/>
)	)	kIx)	)
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
ultimátky	ultimátek	k1gInPc1	ultimátek
s	s	k7c7	s
nepříliš	příliš	k6eNd1	příliš
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
přebíjecí	přebíjecí	k2eAgFnSc7d1	přebíjecí
dobou	doba	k1gFnSc7	doba
jim	on	k3xPp3gMnPc3	on
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
obvykle	obvykle	k6eAd1	obvykle
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
<g/>
;	;	kIx,	;
tedy	tedy	k8xC	tedy
například	například	k6eAd1	například
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
omezujícím	omezující	k2eAgInPc3d1	omezující
účinkům	účinek	k1gInPc3	účinek
<g/>
,	,	kIx,	,
zvětšují	zvětšovat	k5eAaImIp3nP	zvětšovat
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
jejich	jejich	k3xOp3gNnSc4	jejich
zdraví	zdraví	k1gNnSc4	zdraví
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
regeneraci	regenerace	k1gFnSc4	regenerace
nebo	nebo	k8xC	nebo
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
krátkodobou	krátkodobý	k2eAgFnSc4d1	krátkodobá
nesmrtelnost	nesmrtelnost	k1gFnSc4	nesmrtelnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tankové	tankový	k2eAgNnSc1d1	tankové
chodí	chodit	k5eAaImIp3nS	chodit
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c4	na
top	topit	k5eAaImRp2nS	topit
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
jako	jako	k9	jako
podpory	podpora	k1gFnSc2	podpora
nebo	nebo	k8xC	nebo
do	do	k7c2	do
džungle	džungle	k1gFnSc2	džungle
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
tanků	tank	k1gInPc2	tank
<g/>
:	:	kIx,	:
Sion	Sion	k1gMnSc1	Sion
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Mundo	Mundo	k1gNnSc1	Mundo
<g/>
,	,	kIx,	,
Rammus	Rammus	k1gMnSc1	Rammus
<g/>
,	,	kIx,	,
Singed	Singed	k1gMnSc1	Singed
<g/>
,	,	kIx,	,
Ornn	Ornn	k1gMnSc1	Ornn
<g/>
,	,	kIx,	,
Tahm	Tahm	k1gMnSc1	Tahm
Kench	Kench	k1gMnSc1	Kench
<g/>
,	,	kIx,	,
Zac	Zac	k1gMnSc1	Zac
<g/>
,	,	kIx,	,
Tryndamere	Tryndamer	k1gMnSc5	Tryndamer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Progaming	Progaming	k1gInSc4	Progaming
==	==	k?	==
</s>
</p>
<p>
<s>
League	League	k1gFnSc1	League
of	of	k?	of
Legends	Legends	k1gInSc1	Legends
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejhranějších	hraný	k2eAgFnPc2d3	nejhranější
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
aktivní	aktivní	k2eAgFnSc1d1	aktivní
hráčská	hráčský	k2eAgFnSc1d1	hráčská
základna	základna	k1gFnSc1	základna
světově	světově	k6eAd1	světově
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
nad	nad	k7c4	nad
32	[number]	k4	32
miliónů	milión	k4xCgInPc2	milión
(	(	kIx(	(
<g/>
statistiky	statistika	k1gFnPc1	statistika
z	z	k7c2	z
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
World	World	k1gMnSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
největšího	veliký	k2eAgInSc2d3	veliký
rozkvětu	rozkvět	k1gInSc2	rozkvět
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
hru	hra	k1gFnSc4	hra
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c6	na
PvP	PvP	k1gFnSc1	PvP
(	(	kIx(	(
<g/>
hráč	hráč	k1gMnSc1	hráč
vs	vs	k?	vs
hráč	hráč	k1gMnSc1	hráč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
široká	široký	k2eAgFnSc1d1	široká
kompetetivní	kompetetivní	k2eAgFnSc1d1	kompetetivní
scéna	scéna	k1gFnSc1	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
od	od	k7c2	od
Riot	Riota	k1gFnPc2	Riota
Games	Gamesa	k1gFnPc2	Gamesa
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
pro	pro	k7c4	pro
sezónu	sezóna	k1gFnSc4	sezóna
2	[number]	k4	2
uvolnili	uvolnit	k5eAaPmAgMnP	uvolnit
5	[number]	k4	5
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
ceny	cena	k1gFnPc4	cena
a	a	k8xC	a
aktivnímu	aktivní	k2eAgNnSc3d1	aktivní
zapojení	zapojení	k1gNnSc3	zapojení
všech	všecek	k3xTgInPc2	všecek
velkých	velký	k2eAgInPc2d1	velký
organizátorů	organizátor	k1gInPc2	organizátor
lig	liga	k1gFnPc2	liga
a	a	k8xC	a
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
League	League	k1gInSc1	League
of	of	k?	of
Legends	Legends	k1gInSc1	Legends
největším	veliký	k2eAgMnSc7d3	veliký
elektronickým	elektronický	k2eAgInSc7d1	elektronický
sportem	sport	k1gInSc7	sport
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Denní	denní	k2eAgFnSc1d1	denní
aktivita	aktivita	k1gFnSc1	aktivita
hráčů	hráč	k1gMnPc2	hráč
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
na	na	k7c4	na
neuvěřitelných	uvěřitelný	k2eNgInPc2d1	neuvěřitelný
32	[number]	k4	32
miliónů	milión	k4xCgInPc2	milión
a	a	k8xC	a
počet	počet	k1gInSc1	počet
registrovaných	registrovaný	k2eAgMnPc2d1	registrovaný
uživatelů	uživatel	k1gMnPc2	uživatel
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
miliónů	milión	k4xCgInPc2	milión
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
5	[number]	k4	5
<g/>
×	×	k?	×
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
hráčů	hráč	k1gMnPc2	hráč
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
i	i	k9	i
počet	počet	k1gInSc1	počet
uživatelů	uživatel	k1gMnPc2	uživatel
na	na	k7c6	na
komunikačním	komunikační	k2eAgInSc6d1	komunikační
kanálu	kanál	k1gInSc6	kanál
reddit	reddit	k5eAaPmF	reddit
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
téměř	téměř	k6eAd1	téměř
k	k	k7c3	k
500	[number]	k4	500
000	[number]	k4	000
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
2013	[number]	k4	2013
Riot	Riotum	k1gNnPc2	Riotum
Games	Gamesa	k1gFnPc2	Gamesa
vydělalo	vydělat	k5eAaPmAgNnS	vydělat
víc	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
půl	půl	k1xP	půl
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Progaming	Progaming	k1gInSc4	Progaming
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
===	===	k?	===
</s>
</p>
<p>
<s>
Profesionální	profesionální	k2eAgMnPc1d1	profesionální
hráči	hráč	k1gMnPc1	hráč
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
schopni	schopen	k2eAgMnPc1d1	schopen
se	se	k3xPyFc4	se
poměrně	poměrně	k6eAd1	poměrně
pohodlně	pohodlně	k6eAd1	pohodlně
uživit	uživit	k5eAaPmF	uživit
skrze	skrze	k?	skrze
aktivity	aktivita	k1gFnSc2	aktivita
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
hraním	hraní	k1gNnSc7	hraní
League	Leagu	k1gFnSc2	Leagu
of	of	k?	of
Legends	Legends	k1gInSc1	Legends
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
hráči	hráč	k1gMnPc1	hráč
jsou	být	k5eAaImIp3nP	být
placeni	platit	k5eAaImNgMnP	platit
organizacemi	organizace	k1gFnPc7	organizace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pak	pak	k6eAd1	pak
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jejich	jejich	k3xOp3gInPc4	jejich
další	další	k2eAgInPc4d1	další
příjmy	příjem	k1gInPc4	příjem
pak	pak	k6eAd1	pak
patří	patřit	k5eAaImIp3nP	patřit
především	především	k9	především
výhry	výhra	k1gFnPc1	výhra
z	z	k7c2	z
turnajů	turnaj	k1gInPc2	turnaj
a	a	k8xC	a
výnos	výnos	k1gInSc1	výnos
ze	z	k7c2	z
streamování	streamování	k1gNnSc2	streamování
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
hráče	hráč	k1gMnPc4	hráč
na	na	k7c6	na
světě	svět	k1gInSc6	svět
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Andy	Anda	k1gFnPc4	Anda
"	"	kIx"	"
<g/>
Reginald	Reginald	k1gMnSc1	Reginald
<g/>
"	"	kIx"	"
Dinh	Dinh	k1gMnSc1	Dinh
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
progamingových	progamingový	k2eAgFnPc2d1	progamingová
organizací	organizace	k1gFnPc2	organizace
zabývajících	zabývající	k2eAgFnPc2d1	zabývající
se	se	k3xPyFc4	se
LoL	LoL	k1gFnSc2	LoL
–	–	k?	–
TSM	TSM	kA	TSM
(	(	kIx(	(
<g/>
Team	team	k1gInSc1	team
Solo	Solo	k1gMnSc1	Solo
Mid	Mid	k1gMnSc1	Mid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
přidali	přidat	k5eAaPmAgMnP	přidat
i	i	k9	i
hráči	hráč	k1gMnPc1	hráč
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
jako	jako	k8xC	jako
například	například	k6eAd1	například
Liu	Liu	k1gFnSc1	Liu
"	"	kIx"	"
<g/>
Westdoor	Westdoor	k1gInSc1	Westdoor
<g/>
"	"	kIx"	"
Shu-Wei	Shu-Wei	k1gNnSc1	Shu-Wei
(	(	kIx(	(
<g/>
ahq	ahq	k?	ahq
e-Sports	e-Sports	k1gInSc1	e-Sports
Club	club	k1gInSc1	club
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lee	Lea	k1gFnSc6	Lea
"	"	kIx"	"
<g/>
Faker	Faker	k1gInSc1	Faker
<g/>
"	"	kIx"	"
Sang-hyeok	Sangyeok	k1gInSc1	Sang-hyeok
(	(	kIx(	(
<g/>
SK	Sk	kA	Sk
Telecom	Telecom	k1gInSc1	Telecom
T	T	kA	T
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozkvět	rozkvět	k1gInSc1	rozkvět
progamingové	progamingový	k2eAgFnSc2d1	progamingová
LoL	LoL	k1gFnSc2	LoL
scény	scéna	k1gFnSc2	scéna
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
také	také	k9	také
díky	díky	k7c3	díky
skvělé	skvělý	k2eAgFnSc3d1	skvělá
podpoře	podpora	k1gFnSc3	podpora
organizátorů	organizátor	k1gMnPc2	organizátor
turnajů	turnaj	k1gInPc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
LoL	LoL	k?	LoL
nechybělo	chybět	k5eNaImAgNnS	chybět
na	na	k7c6	na
žádné	žádný	k3yNgFnSc6	žádný
velké	velký	k2eAgFnSc6d1	velká
akci	akce	k1gFnSc6	akce
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
a	a	k8xC	a
z	z	k7c2	z
výsluní	výsluň	k1gFnPc2	výsluň
díky	díky	k7c3	díky
obrovské	obrovský	k2eAgFnSc3d1	obrovská
podpoře	podpora	k1gFnSc3	podpora
fanoušků	fanoušek	k1gMnPc2	fanoušek
vytlačilo	vytlačit	k5eAaPmAgNnS	vytlačit
i	i	k9	i
hlavní	hlavní	k2eAgFnSc4d1	hlavní
hvězdu	hvězda	k1gFnSc4	hvězda
elektronických	elektronický	k2eAgInPc2d1	elektronický
sportů	sport	k1gInPc2	sport
–	–	k?	–
Starcraft	Starcrafta	k1gFnPc2	Starcrafta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
nejpatrněji	patrně	k6eAd3	patrně
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
stalo	stát	k5eAaPmAgNnS	stát
League	League	k1gNnSc1	League
of	of	k?	of
Legends	Legends	k1gInSc4	Legends
nejhranější	hraný	k2eAgFnSc7d3	nejhranější
hrou	hra	k1gFnSc7	hra
a	a	k8xC	a
získalo	získat	k5eAaPmAgNnS	získat
vlastní	vlastní	k2eAgInSc4d1	vlastní
turnaj	turnaj	k1gInSc4	turnaj
na	na	k7c6	na
korejské	korejský	k2eAgFnSc6d1	Korejská
televizi	televize	k1gFnSc6	televize
OGN	OGN	kA	OGN
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Popularita	popularita	k1gFnSc1	popularita
progamingu	progaming	k1gInSc2	progaming
umožnila	umožnit	k5eAaPmAgFnS	umožnit
založení	založení	k1gNnSc4	založení
regionálních	regionální	k2eAgFnPc2d1	regionální
lig	liga	k1gFnPc2	liga
<g/>
:	:	kIx,	:
NA	na	k7c6	na
LCS	LCS	kA	LCS
<g/>
,	,	kIx,	,
EU	EU	kA	EU
LCS	LCS	kA	LCS
<g/>
,	,	kIx,	,
LCK	LCK	kA	LCK
(	(	kIx(	(
<g/>
Korea	Korea	k1gFnSc1	Korea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
LPL	LPL	kA	LPL
(	(	kIx(	(
<g/>
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
a	a	k8xC	a
LMS	LMS	kA	LMS
(	(	kIx(	(
<g/>
Taiwan	Taiwan	k1gMnSc1	Taiwan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
nejlepší	dobrý	k2eAgInPc1d3	nejlepší
týmy	tým	k1gInPc1	tým
pravidelně	pravidelně	k6eAd1	pravidelně
střetávají	střetávat	k5eAaImIp3nP	střetávat
jako	jako	k9	jako
v	v	k7c6	v
tradičních	tradiční	k2eAgInPc6d1	tradiční
sportech	sport	k1gInPc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
každé	každý	k3xTgFnSc2	každý
sezóny	sezóna	k1gFnSc2	sezóna
se	se	k3xPyFc4	se
střetávají	střetávat	k5eAaImIp3nP	střetávat
nejlepší	dobrý	k2eAgInPc1d3	nejlepší
týmy	tým	k1gInPc1	tým
z	z	k7c2	z
regionálních	regionální	k2eAgFnPc2d1	regionální
lig	liga	k1gFnPc2	liga
na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
World	World	k1gMnSc1	World
Championship	Championship	k1gMnSc1	Championship
<g/>
/	/	kIx~	/
<g/>
Worlds	Worlds	k1gInSc1	Worlds
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poměří	poměřit	k5eAaPmIp3nS	poměřit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
a	a	k8xC	a
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
týmem	tým	k1gInSc7	tým
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Světové	světový	k2eAgInPc1d1	světový
týmy	tým	k1gInPc1	tým
===	===	k?	===
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
League	League	k1gFnSc1	League
of	of	k?	of
Legends	Legendsa	k1gFnPc2	Legendsa
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c6	na
regiony	region	k1gInPc1	region
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
velkých	velká	k1gFnPc6	velká
turnajích-	turnajích-	k?	turnajích-
Worlds	Worlds	k1gInSc1	Worlds
<g/>
,	,	kIx,	,
MSI	MSI	kA	MSI
<g/>
,	,	kIx,	,
IEM	IEM	kA	IEM
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
utkají	utkat	k5eAaPmIp3nP	utkat
hráči	hráč	k1gMnPc1	hráč
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
tedy	tedy	k9	tedy
velice	velice	k6eAd1	velice
obtížné	obtížný	k2eAgNnSc1d1	obtížné
poměřovat	poměřovat	k5eAaImF	poměřovat
relativní	relativní	k2eAgFnSc4d1	relativní
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
kvalitu	kvalita	k1gFnSc4	kvalita
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
celků	celek	k1gInPc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc1d1	následující
týmy	tým	k1gInPc1	tým
však	však	k9	však
patří	patřit	k5eAaImIp3nP	patřit
<g/>
/	/	kIx~	/
<g/>
patřily	patřit	k5eAaImAgFnP	patřit
ke	k	k7c3	k
špičce	špička	k1gFnSc3	špička
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
regionech	region	k1gInPc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
regionů	region	k1gInPc2	region
se	se	k3xPyFc4	se
často	často	k6eAd1	často
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
3	[number]	k4	3
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
dominují	dominovat	k5eAaImIp3nP	dominovat
světové	světový	k2eAgFnSc3d1	světová
scéně	scéna	k1gFnSc3	scéna
týmy	tým	k1gInPc4	tým
korejské	korejský	k2eAgFnSc2d1	Korejská
ligy	liga	k1gFnSc2	liga
LCK	LCK	kA	LCK
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
(	(	kIx(	(
<g/>
NA	na	k7c6	na
LCS	LCS	kA	LCS
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Counter	Counter	k1gMnSc1	Counter
Logic	Logic	k1gMnSc1	Logic
Gaming	Gaming	k1gInSc1	Gaming
(	(	kIx(	(
<g/>
CLG	CLG	kA	CLG
<g/>
)	)	kIx)	)
-	-	kIx~	-
http://clgaming.net/	[url]	k?	http://clgaming.net/
Dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
vítěz	vítěz	k1gMnSc1	vítěz
NA	na	k7c6	na
LCS	LCS	kA	LCS
(	(	kIx(	(
<g/>
léto	léto	k1gNnSc4	léto
2015	[number]	k4	2015
a	a	k8xC	a
jaro	jaro	k1gNnSc4	jaro
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
na	na	k7c4	na
MSI	MSI	kA	MSI
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončili	skončit	k5eAaPmAgMnP	skončit
druzí	druhý	k4xOgMnPc1	druhý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Team	team	k1gInSc1	team
Solo	Solo	k1gMnSc1	Solo
Mid	Mid	k1gMnSc1	Mid
(	(	kIx(	(
<g/>
TSM	TSM	kA	TSM
<g/>
)	)	kIx)	)
-	-	kIx~	-
http://www.solomid.net/	[url]	k?	http://www.solomid.net/
Jediný	jediný	k2eAgInSc1d1	jediný
tým	tým	k1gInSc1	tým
LCS	LCS	kA	LCS
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
všech	všecek	k3xTgInPc2	všecek
8	[number]	k4	8
NA	na	k7c6	na
LCS	LCS	kA	LCS
finále	finále	k1gNnSc1	finále
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Team	team	k1gInSc1	team
Immortals	Immortals	k1gInSc1	Immortals
-	-	kIx~	-
Celek	celek	k1gInSc1	celek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
a	a	k8xC	a
poskládán	poskládán	k2eAgMnSc1d1	poskládán
z	z	k7c2	z
hvězd	hvězda	k1gFnPc2	hvězda
severoamerické	severoamerický	k2eAgFnSc2d1	severoamerická
i	i	k8xC	i
evropské	evropský	k2eAgFnSc2d1	Evropská
LCS	LCS	kA	LCS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Team	team	k1gInSc1	team
Liquid	Liquid	k1gInSc1	Liquid
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
znám	znám	k2eAgInSc1d1	znám
jako	jako	k8xS	jako
Team	team	k1gInSc1	team
Curse	Curse	k1gFnSc2	Curse
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
http://www.teamliquid.net/	[url]	k?	http://www.teamliquid.net/
</s>
</p>
<p>
<s>
Cloud	Cloud	k1gInSc1	Cloud
9	[number]	k4	9
-	-	kIx~	-
http://cloud9.gg/	[url]	k4	http://cloud9.gg/
světová	světový	k2eAgFnSc1d1	světová
1	[number]	k4	1
<g/>
Evropa	Evropa	k1gFnSc1	Evropa
(	(	kIx(	(
<g/>
LEC	LEC	kA	LEC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Unicorns	Unicorns	k1gInSc1	Unicorns
Of	Of	k1gFnSc2	Of
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
UoL	UoL	k1gFnSc6	UoL
<g/>
)	)	kIx)	)
-	-	kIx~	-
Německý	německý	k2eAgInSc1d1	německý
team	team	k1gInSc1	team
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
IEM	IEM	kA	IEM
Oakland	Oakland	k1gInSc1	Oakland
(	(	kIx(	(
<g/>
season	season	k1gInSc1	season
XI	XI	kA	XI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
několikanásobný	několikanásobný	k2eAgMnSc1d1	několikanásobný
finalista	finalista	k1gMnSc1	finalista
EU	EU	kA	EU
LCS	LCS	kA	LCS
<g/>
,	,	kIx,	,
Fnatic	Fnatice	k1gFnPc2	Fnatice
–	–	k?	–
http://www.fnatic.com/	[url]	k?	http://www.fnatic.com/
-	-	kIx~	-
Vítěz	vítěz	k1gMnSc1	vítěz
S1	S1	k1gMnSc2	S1
světového	světový	k2eAgInSc2d1	světový
šampionátu	šampionát	k1gInSc2	šampionát
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pětinásobný	pětinásobný	k2eAgMnSc1d1	pětinásobný
vítěz	vítěz	k1gMnSc1	vítěz
evropské	evropský	k2eAgFnSc2d1	Evropská
LCS	LCS	kA	LCS
<g/>
.	.	kIx.	.
</s>
<s>
Držitel	držitel	k1gMnSc1	držitel
perfektní	perfektní	k2eAgFnSc2d1	perfektní
základní	základní	k2eAgFnSc2d1	základní
sezóny	sezóna	k1gFnSc2	sezóna
(	(	kIx(	(
<g/>
léto	léto	k1gNnSc4	léto
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
–	–	k?	–
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
všech	všecek	k3xTgInPc2	všecek
18	[number]	k4	18
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
západní	západní	k2eAgInSc1d1	západní
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kdy	kdy	k6eAd1	kdy
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
na	na	k7c4	na
Worlds	Worlds	k1gInSc4	Worlds
<g/>
.	.	kIx.	.
</s>
<s>
Finalista	finalista	k1gMnSc1	finalista
Worlds	Worlds	k1gInSc4	Worlds
S	s	k7c7	s
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elements	Elements	k1gInSc1	Elements
–	–	k?	–
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
znám	znát	k5eAaImIp1nS	znát
jako	jako	k9	jako
Alliance	Alliance	k1gFnSc1	Alliance
<g/>
)	)	kIx)	)
–	–	k?	–
Vítěz	vítěz	k1gMnSc1	vítěz
Evropské	evropský	k2eAgFnSc2d1	Evropská
LCS	LCS	kA	LCS
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
již	již	k6eAd1	již
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
G2	G2	k4	G2
Esports	Esports	k1gInSc1	Esports
–	–	k?	–
http://www.g2esports.com/	[url]	k4	http://www.g2esports.com/
–	–	k?	–
Evropský	evropský	k2eAgInSc1d1	evropský
tým	tým	k1gInSc1	tým
založený	založený	k2eAgInSc1d1	založený
bývalou	bývalý	k2eAgFnSc7d1	bývalá
hvězdou	hvězda	k1gFnSc7	hvězda
Carlosem	Carlos	k1gMnSc7	Carlos
Rogriguézem	Rogriguéz	k1gMnSc7	Rogriguéz
(	(	kIx(	(
<g/>
ocelote	ocelot	k1gMnSc5	ocelot
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
do	do	k7c2	do
EU	EU	kA	EU
LCS	LCS	kA	LCS
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2015	[number]	k4	2015
přes	přes	k7c4	přes
Challenger	Challenger	k1gInSc4	Challenger
Series	Series	k1gInSc4	Series
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
liga	liga	k1gFnSc1	liga
–	–	k?	–
Vyzyvatelská	Vyzyvatelský	k2eAgFnSc1d1	Vyzyvatelský
série	série	k1gFnSc1	série
<g/>
)	)	kIx)	)
a	a	k8xC	a
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
G2	G2	k4	G2
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
v	v	k7c6	v
EU	EU	kA	EU
LCS	LCS	kA	LCS
jak	jak	k8xC	jak
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
tak	tak	k9	tak
i	i	k9	i
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Origen	Origen	k1gInSc1	Origen
–	–	k?	–
http://origen.gg/	[url]	k?	http://origen.gg/
Tým	tým	k1gInSc4	tým
založený	založený	k2eAgInSc4d1	založený
bývalým	bývalý	k2eAgMnSc7d1	bývalý
hvězdným	hvězdný	k2eAgMnSc7d1	hvězdný
hráčem	hráč	k1gMnSc7	hráč
týmu	tým	k1gInSc2	tým
Fnatic	Fnatice	k1gFnPc2	Fnatice
xPekem	xPekem	k6eAd1	xPekem
(	(	kIx(	(
<g/>
Enrique	Enrique	k1gInSc1	Enrique
Cedeňo	Cedeňo	k1gNnSc1	Cedeňo
Martínéz	Martínéza	k1gFnPc2	Martínéza
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Challenger	Challengra	k1gFnPc2	Challengra
Series	Seriesa	k1gFnPc2	Seriesa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
hráli	hrát	k5eAaImAgMnP	hrát
EU	EU	kA	EU
LCS	LCS	kA	LCS
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
umístili	umístit	k5eAaPmAgMnP	umístit
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
reprezentovali	reprezentovat	k5eAaImAgMnP	reprezentovat
EU	EU	kA	EU
LCS	LCS	kA	LCS
na	na	k7c4	na
Worlds	Worlds	k1gInSc4	Worlds
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tady	tady	k6eAd1	tady
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
vítězi	vítěz	k1gMnPc7	vítěz
celého	celý	k2eAgInSc2d1	celý
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
korejskému	korejský	k2eAgInSc3d1	korejský
celku	celek	k1gInSc3	celek
SKT	SKT	kA	SKT
T	T	kA	T
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
skočili	skočit	k5eAaPmAgMnP	skočit
druzí	druhý	k4xOgMnPc1	druhý
v	v	k7c6	v
EU	EU	kA	EU
LCS	LCS	kA	LCS
<g/>
.	.	kIx.	.
<g/>
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
(	(	kIx(	(
<g/>
LCK	LCK	kA	LCK
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SK	Sk	kA	Sk
Telecom	Telecom	k1gInSc1	Telecom
T1	T1	k1gMnSc2	T1
–	–	k?	–
http://www.sksports.net/T1/main.asp	[url]	k1gMnSc1	http://www.sksports.net/T1/main.asp
-	-	kIx~	-
Vítěz	vítěz	k1gMnSc1	vítěz
S3	S3	k1gFnSc2	S3
Worlds	Worlds	k1gInSc1	Worlds
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
S5	S5	k1gFnSc2	S5
Worlds	Worlds	k1gInSc1	Worlds
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
a	a	k8xC	a
vítěz	vítěz	k1gMnSc1	vítěz
S6	S6	k1gFnSc2	S6
Worlds	Worlds	k1gInSc1	Worlds
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
a	a	k8xC	a
finalista	finalista	k1gMnSc1	finalista
Worlds	Worldsa	k1gFnPc2	Worldsa
S7	S7	k1gMnSc1	S7
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rox	Rox	k?	Rox
Tigers	Tigers	k1gInSc1	Tigers
–	–	k?	–
https://twitter.com/lol_tigers	[url]	k6eAd1	https://twitter.com/lol_tigers
Dominantní	dominantní	k2eAgInSc1d1	dominantní
tým	tým	k1gInSc1	tým
v	v	k7c6	v
korejské	korejský	k2eAgFnSc6d1	Korejská
lize	liga	k1gFnSc6	liga
<g/>
,	,	kIx,	,
finalista	finalista	k1gMnSc1	finalista
S5	S5	k1gFnSc2	S5
Worlds	Worlds	k1gInSc1	Worlds
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samsung	Samsung	kA	Samsung
Galaxy	Galax	k1gInPc4	Galax
(	(	kIx(	(
<g/>
White	Whit	k1gInSc5	Whit
<g/>
)	)	kIx)	)
–	–	k?	–
Vítěz	vítěz	k1gMnSc1	vítěz
S4	S4	k1gMnSc1	S4
Worlds	Worldsa	k1gFnPc2	Worldsa
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
finalista	finalista	k1gMnSc1	finalista
S6	S6	k1gFnSc2	S6
Worlds	Worlds	k1gInSc1	Worlds
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
a	a	k8xC	a
vítěz	vítěz	k1gMnSc1	vítěz
Worlds	Worldsa	k1gFnPc2	Worldsa
S7	S7	k1gMnSc1	S7
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Taiwan	Taiwan	k1gMnSc1	Taiwan
(	(	kIx(	(
<g/>
LMS	LMS	kA	LMS
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Taipei	Taipei	k6eAd1	Taipei
Assassins	Assassins	k1gInSc1	Assassins
–	–	k?	–
http://taipeiassassins.tw/	[url]	k?	http://taipeiassassins.tw/
-	-	kIx~	-
Vítěz	vítěz	k1gMnSc1	vítěz
S2	S2	k1gFnSc2	S2
Worlds	Worlds	k1gInSc1	Worlds
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Flash	Flash	k1gMnSc1	Flash
Wolves	Wolves	k1gMnSc1	Wolves
–	–	k?	–
http://www.flashwolves.com/	[url]	k?	http://www.flashwolves.com/
</s>
</p>
<p>
<s>
ahq	ahq	k?	ahq
e-Sports	e-Sports	k1gInSc1	e-Sports
Club	club	k1gInSc1	club
–	–	k?	–
http://www.ahq.com.tw/Čína	[url]	k1gFnSc1	http://www.ahq.com.tw/Čína
(	(	kIx(	(
<g/>
LPL	LPL	kA	LPL
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Edward	Edward	k1gMnSc1	Edward
Gaming	Gaming	k1gInSc4	Gaming
</s>
</p>
<p>
<s>
Royal	Royal	k1gMnSc1	Royal
Never	Never	k1gMnSc1	Never
Give	Give	k1gFnPc2	Give
Up	Up	k1gMnSc1	Up
</s>
</p>
<p>
<s>
IMay	IMaa	k1gFnPc1	IMaa
</s>
</p>
<p>
<s>
Invictus	Invictus	k1gInSc1	Invictus
Gaming	Gaming	k1gInSc1	Gaming
–	–	k?	–
Vítěz	vítěz	k1gMnSc1	vítěz
Worlds	Worldsa	k1gFnPc2	Worldsa
S8	S8	k1gMnSc1	S8
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
League	Leagu	k1gInSc2	Leagu
of	of	k?	of
Legends	Legends	k1gInSc1	Legends
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Anglickojazyčná	anglickojazyčný	k2eAgFnSc1d1	anglickojazyčná
Wikia	Wikia	k1gFnSc1	Wikia
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
LoL	LoL	k1gMnSc1	LoL
Counter	Counter	k1gMnSc1	Counter
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
server	server	k1gInSc1	server
zabývající	zabývající	k2eAgInSc1d1	zabývající
se	s	k7c7	s
progamingem	progaming	k1gInSc7	progaming
v	v	k7c6	v
League	League	k1gFnSc6	League
of	of	k?	of
Legends	Legendsa	k1gFnPc2	Legendsa
</s>
</p>
<p>
<s>
Listy	list	k1gInPc1	list
League	Leagu	k1gFnSc2	Leagu
of	of	k?	of
Legends	Legends	k1gInSc1	Legends
–	–	k?	–
blog	blog	k1gInSc1	blog
o	o	k7c6	o
novinkách	novinka	k1gFnPc6	novinka
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
Wikia	Wikia	k1gFnSc1	Wikia
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
hry	hra	k1gFnSc2	hra
na	na	k7c6	na
PLAYzone	PLAYzon	k1gInSc5	PLAYzon
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
hry	hra	k1gFnSc2	hra
na	na	k7c6	na
ESL	ESL	kA	ESL
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
hry	hra	k1gFnSc2	hra
LoL	LoL	k1gFnSc2	LoL
na	na	k7c6	na
České	český	k2eAgFnSc6d1	Česká
databázi	databáze	k1gFnSc6	databáze
her	hra	k1gFnPc2	hra
</s>
</p>
