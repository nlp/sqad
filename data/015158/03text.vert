<s>
Mříž	mříž	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
stavebním	stavební	k2eAgInSc6d1
prvku	prvek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
matematické	matematický	k2eAgFnSc6d1
struktuře	struktura	k1gFnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
bodová	bodový	k2eAgFnSc1d1
mříž	mříž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Mřížový	mřížový	k2eAgInSc4d1
plot	plot	k1gInSc4
v	v	k7c6
Madridu	Madrid	k1gInSc6
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Mříž	mříž	k1gFnSc1
je	být	k5eAaImIp3nS
konstrukce	konstrukce	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
kovových	kovový	k2eAgFnPc2d1
tyčí	tyč	k1gFnPc2
<g/>
,	,	kIx,
pásů	pás	k1gInPc2
nebo	nebo	k8xC
profilů	profil	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
vzájemně	vzájemně	k6eAd1
spojené	spojený	k2eAgFnPc1d1
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nP
spolu	spolu	k6eAd1
ochranné	ochranný	k2eAgNnSc4d1
hrazení	hrazení	k1gNnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
výplň	výplň	k1gFnSc1
okenních	okenní	k2eAgInPc2d1
nebo	nebo	k8xC
dveřních	dveřní	k2eAgInPc2d1
otvorů	otvor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Mříž	mříž	k1gFnSc1
se	se	k3xPyFc4
nejčastěji	často	k6eAd3
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
rovnoběžně	rovnoběžně	k6eAd1
uspořádaných	uspořádaný	k2eAgInPc2d1
pásů	pás	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
se	se	k3xPyFc4
vzájemně	vzájemně	k6eAd1
kříží	křížit	k5eAaImIp3nS
a	a	k8xC
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
jakousi	jakýsi	k3yIgFnSc4
síť	síť	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohou	moct	k5eAaImIp3nP
se	se	k3xPyFc4
používat	používat	k5eAaImF
i	i	k9
jiné	jiný	k2eAgInPc1d1
materiály	materiál	k1gInPc1
<g/>
,	,	kIx,
např.	např.	kA
dřevo	dřevo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
mříže	mříž	k1gFnPc1
mají	mít	k5eAaImIp3nP
čistě	čistě	k6eAd1
dekorativní	dekorativní	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
mohou	moct	k5eAaImIp3nP
tvořit	tvořit	k5eAaImF
podpůrnou	podpůrný	k2eAgFnSc4d1
konstrukci	konstrukce	k1gFnSc4
stavby	stavba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
drátěné	drátěný	k2eAgNnSc1d1
pletivo	pletivo	k1gNnSc1
</s>
<s>
drátěnka	drátěnka	k1gFnSc1
(	(	kIx(
<g/>
rošt	rošt	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
mřížka	mřížka	k1gFnSc1
</s>
<s>
rošt	rošt	k1gInSc1
(	(	kIx(
<g/>
součást	součást	k1gFnSc1
topeniště	topeniště	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
síto	síto	k1gNnSc1
</s>
<s>
treláž	treláž	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
mříž	mříž	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
mříž	mříž	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4157374-2	4157374-2	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85057353	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85057353	#num#	k4
</s>
