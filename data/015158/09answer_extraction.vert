Mříž	mříž	k1gFnSc1
se	se	k3xPyFc4
nejčastěji	často	k6eAd3
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
rovnoběžně	rovnoběžně	k6eAd1
uspořádaných	uspořádaný	k2eAgInPc2d1
pásů	pás	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
se	se	k3xPyFc4
vzájemně	vzájemně	k6eAd1
kříží	křížit	k5eAaImIp3nS
a	a	k8xC
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
jakousi	jakýsi	k3yIgFnSc4
síť	síť	k1gFnSc4
<g/>
.	.	kIx.
