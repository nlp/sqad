<p>
<s>
Válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
a	a	k8xC	a
zločiny	zločin	k1gInPc4	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
spáchané	spáchaný	k2eAgNnSc1d1	spáchané
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
91	[number]	k4	91
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
akce	akce	k1gFnPc1	akce
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
NKVD	NKVD	kA	NKVD
či	či	k8xC	či
vnitřních	vnitřní	k2eAgNnPc2d1	vnitřní
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
válečných	válečný	k2eAgInPc2d1	válečný
zločinů	zločin	k1gInPc2	zločin
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Katyňský	Katyňský	k2eAgInSc4d1	Katyňský
masakr	masakr	k1gInSc4	masakr
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
spáchána	spáchat	k5eAaPmNgFnS	spáchat
na	na	k7c4	na
přímý	přímý	k2eAgInSc4d1	přímý
rozkaz	rozkaz	k1gInSc4	rozkaz
Josefa	Josef	k1gMnSc2	Josef
Stalina	Stalin	k1gMnSc2	Stalin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
byly	být	k5eAaImAgInP	být
zločiny	zločin	k1gInPc1	zločin
spáchány	spáchán	k2eAgInPc1d1	spáchán
bez	bez	k7c2	bez
rozkazů	rozkaz	k1gInPc2	rozkaz
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
vojenských	vojenský	k2eAgFnPc2d1	vojenská
jednotek	jednotka	k1gFnPc2	jednotka
proti	proti	k7c3	proti
civilistům	civilista	k1gMnPc3	civilista
nebo	nebo	k8xC	nebo
vojenskému	vojenský	k2eAgInSc3d1	vojenský
personálu	personál	k1gInSc2	personál
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Německa	Německo	k1gNnSc2	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
ozbrojeného	ozbrojený	k2eAgInSc2d1	ozbrojený
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
SSSR	SSSR	kA	SSSR
nebo	nebo	k8xC	nebo
během	během	k7c2	během
partyzánské	partyzánský	k2eAgFnSc2d1	Partyzánská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Sovětští	sovětský	k2eAgMnPc1d1	sovětský
představitelé	představitel	k1gMnPc1	představitel
nebyli	být	k5eNaImAgMnP	být
za	za	k7c4	za
tyto	tento	k3xDgInPc4	tento
zločiny	zločin	k1gInPc4	zločin
souzeni	souzen	k2eAgMnPc1d1	souzen
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
příklad	příklad	k1gInSc4	příklad
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozadí	pozadí	k1gNnSc1	pozadí
==	==	k?	==
</s>
</p>
<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
neuznal	uznat	k5eNaPmAgInS	uznat
závazný	závazný	k2eAgInSc1d1	závazný
podpis	podpis	k1gInSc1	podpis
Haagských	haagský	k2eAgFnPc2d1	Haagská
úmluv	úmluva	k1gFnPc2	úmluva
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1899	[number]	k4	1899
a	a	k8xC	a
1907	[number]	k4	1907
carským	carský	k2eAgNnSc7d1	carské
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
odmítal	odmítat	k5eAaImAgMnS	odmítat
jej	on	k3xPp3gMnSc4	on
uznat	uznat	k5eAaPmF	uznat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byly	být	k5eAaImAgFnP	být
válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
sovětských	sovětský	k2eAgFnPc2d1	sovětská
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
nakonec	nakonec	k6eAd1	nakonec
racionalizovány	racionalizován	k2eAgFnPc1d1	racionalizována
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
uznat	uznat	k5eAaPmF	uznat
Haagské	haagský	k2eAgFnPc4d1	Haagská
úmluvy	úmluva	k1gFnPc4	úmluva
<g/>
,	,	kIx,	,
také	také	k6eAd1	také
dalo	dát	k5eAaPmAgNnS	dát
nacistickému	nacistický	k2eAgMnSc3d1	nacistický
Německu	Německo	k1gNnSc6	Německo
důvody	důvod	k1gInPc4	důvod
k	k	k7c3	k
nelidskému	lidský	k2eNgNnSc3d1	nelidské
zacházení	zacházení	k1gNnSc3	zacházení
se	s	k7c7	s
zajatým	zajatý	k1gMnSc7	zajatý
sovětským	sovětský	k2eAgMnSc7d1	sovětský
vojenským	vojenský	k2eAgInSc7d1	vojenský
personálem	personál	k1gInSc7	personál
<g/>
.	.	kIx.	.
</s>
<s>
Sovětští	sovětský	k2eAgMnPc1d1	sovětský
vojáci	voják	k1gMnPc1	voják
sami	sám	k3xTgMnPc1	sám
často	často	k6eAd1	často
neměli	mít	k5eNaImAgMnP	mít
ani	ani	k8xC	ani
ponětí	ponětí	k1gNnSc4	ponětí
<g/>
,	,	kIx,	,
že	že	k8xS	že
takové	takový	k3xDgFnPc4	takový
smlouvy	smlouva	k1gFnPc4	smlouva
a	a	k8xC	a
zákony	zákon	k1gInPc4	zákon
existují	existovat	k5eAaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Oběti	oběť	k1gFnPc1	oběť
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
===	===	k?	===
</s>
</p>
<p>
<s>
Několik	několik	k4yIc1	několik
historiků	historik	k1gMnPc2	historik
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
počet	počet	k1gInSc1	počet
poprav	poprava	k1gFnPc2	poprava
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
rudého	rudý	k2eAgInSc2d1	rudý
teroru	teror	k1gInSc2	teror
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
prováděla	provádět	k5eAaImAgFnS	provádět
Čeka	čeka	k1gFnSc1	čeka
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc1	předchůdce
NKVD	NKVD	kA	NKVD
<g/>
,	,	kIx,	,
na	na	k7c6	na
asi	asi	k9	asi
250	[number]	k4	250
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
Čekou	čeka	k1gFnSc7	čeka
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
možná	možná	k9	možná
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
než	než	k8xS	než
kolik	kolik	k4yIc1	kolik
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1921	[number]	k4	1921
a	a	k8xC	a
1922	[number]	k4	1922
vedl	vést	k5eAaImAgMnS	vést
Michail	Michail	k1gMnSc1	Michail
Tuchačevskij	Tuchačevskij	k1gMnSc1	Tuchačevskij
<g/>
,	,	kIx,	,
vojenský	vojenský	k2eAgMnSc1d1	vojenský
vůdce	vůdce	k1gMnSc1	vůdce
a	a	k8xC	a
budoucí	budoucí	k2eAgFnSc1d1	budoucí
oběť	oběť	k1gFnSc1	oběť
Stalinovy	Stalinův	k2eAgFnSc2d1	Stalinova
velké	velký	k2eAgFnSc2d1	velká
čistky	čistka	k1gFnSc2	čistka
<g/>
,	,	kIx,	,
kampaň	kampaň	k1gFnSc1	kampaň
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
proti	proti	k7c3	proti
selskému	selské	k1gNnSc3	selské
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
Tambovské	Tambovský	k2eAgFnSc6d1	Tambovský
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Tuchačevskij	Tuchačevskít	k5eAaPmRp2nS	Tuchačevskít
nechal	nechat	k5eAaPmAgMnS	nechat
rutinně	rutinně	k6eAd1	rutinně
popravovat	popravovat	k5eAaImF	popravovat
zajaté	zajatý	k2eAgMnPc4d1	zajatý
rukojmí	rukojmí	k1gMnPc4	rukojmí
bez	bez	k7c2	bez
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
neváhal	váhat	k5eNaImAgMnS	váhat
používat	používat	k5eAaImF	používat
bojový	bojový	k2eAgInSc4d1	bojový
plyn	plyn	k1gInSc4	plyn
proti	proti	k7c3	proti
civilním	civilní	k2eAgInPc3d1	civilní
cílům	cíl	k1gInPc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
Simon	Simon	k1gMnSc1	Simon
Sebag	Sebag	k1gMnSc1	Sebag
Montefiore	Montefior	k1gInSc5	Montefior
Tuchačevského	Tuchačevský	k2eAgInSc2d1	Tuchačevský
viní	vinit	k5eAaImIp3nS	vinit
"	"	kIx"	"
<g/>
tak	tak	k9	tak
nemilosrdného	milosrdný	k2eNgMnSc2d1	nemilosrdný
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
žádného	žádný	k3yNgMnSc4	žádný
bolševika	bolševik	k1gMnSc4	bolševik
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Židovské	židovský	k2eAgFnPc4d1	židovská
oběti	oběť	k1gFnPc4	oběť
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
komunistického	komunistický	k2eAgNnSc2d1	komunistické
hnutí	hnutí	k1gNnSc2	hnutí
působilo	působit	k5eAaImAgNnS	působit
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
židovským	židovský	k2eAgInSc7d1	židovský
původem	původ	k1gInSc7	původ
a	a	k8xC	a
první	první	k4xOgMnPc1	první
sovětští	sovětský	k2eAgMnPc1d1	sovětský
vůdci	vůdce	k1gMnPc1	vůdce
veřejně	veřejně	k6eAd1	veřejně
antisemitismus	antisemitismus	k1gInSc4	antisemitismus
odsuzovali	odsuzovat	k5eAaImAgMnP	odsuzovat
<g/>
.	.	kIx.	.
psáno	psát	k5eAaImNgNnS	psát
Williamem	William	k1gInSc7	William
Koreyem	Koreyem	k1gInSc1	Koreyem
<g/>
:	:	kIx,	:
Protižidovská	protižidovský	k2eAgFnSc1d1	protižidovská
diskriminace	diskriminace	k1gFnSc1	diskriminace
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
sovětské	sovětský	k2eAgFnSc2d1	sovětská
státní	státní	k2eAgFnSc2d1	státní
politiky	politika	k1gFnSc2	politika
už	už	k6eAd1	už
od	od	k7c2	od
konce	konec	k1gInSc2	konec
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Úsilí	úsilí	k1gNnSc1	úsilí
vynaložené	vynaložený	k2eAgInPc1d1	vynaložený
sovětskými	sovětský	k2eAgFnPc7d1	sovětská
autoritami	autorita	k1gFnPc7	autorita
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
protižidovský	protižidovský	k2eAgInSc4d1	protižidovský
náboženský	náboženský	k2eAgInSc4d1	náboženský
fanatismus	fanatismus	k1gInSc4	fanatismus
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
během	během	k7c2	během
případů	případ	k1gInPc2	případ
ruské	ruský	k2eAgFnSc2d1	ruská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jednotky	jednotka	k1gFnPc1	jednotka
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
páchaly	páchat	k5eAaImAgInP	páchat
pogromy	pogrom	k1gInPc1	pogrom
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
např.	např.	kA	např.
během	během	k7c2	během
sovětsko	sovětsko	k6eAd1	sovětsko
<g/>
–	–	k?	–
<g/>
polské	polský	k2eAgFnSc2d1	polská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
letech	let	k1gInPc6	let
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
v	v	k7c6	v
Baranoviči	Baranovič	k1gMnSc6	Baranovič
<g/>
.	.	kIx.	.
</s>
<s>
Rudé	rudý	k2eAgFnSc3d1	rudá
armádě	armáda	k1gFnSc3	armáda
je	být	k5eAaImIp3nS	být
připisován	připisován	k2eAgInSc1d1	připisován
pouze	pouze	k6eAd1	pouze
malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
pogromů	pogrom	k1gInPc2	pogrom
<g/>
,	,	kIx,	,
drtivou	drtivý	k2eAgFnSc4d1	drtivá
většinu	většina	k1gFnSc4	většina
páchaly	páchat	k5eAaImAgInP	páchat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
"	"	kIx"	"
<g/>
kolektivního	kolektivní	k2eAgNnSc2d1	kolektivní
násilí	násilí	k1gNnSc2	násilí
<g/>
"	"	kIx"	"
antikomunistické	antikomunistický	k2eAgFnSc2d1	antikomunistická
a	a	k8xC	a
nacionalistické	nacionalistický	k2eAgFnSc2d1	nacionalistická
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
<g/>
Pogromy	pogrom	k1gInPc1	pogrom
vrchní	vrchní	k1gFnSc2	vrchní
velení	velení	k1gNnSc2	velení
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
odsuzovalo	odsuzovat	k5eAaImAgNnS	odsuzovat
a	a	k8xC	a
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
pogromů	pogrom	k1gInPc2	pogrom
dopustily	dopustit	k5eAaPmAgInP	dopustit
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
odzbrojeny	odzbrojen	k2eAgInPc1d1	odzbrojen
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
byli	být	k5eAaImAgMnP	být
postaveni	postavit	k5eAaPmNgMnP	postavit
před	před	k7c4	před
vojenský	vojenský	k2eAgInSc4d1	vojenský
soud	soud	k1gInSc4	soud
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prokázání	prokázání	k1gNnSc6	prokázání
viny	vina	k1gFnSc2	vina
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
popravám	poprava	k1gFnPc3	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
pogromy	pogrom	k1gInPc1	pogrom
prováděné	prováděný	k2eAgInPc1d1	prováděný
ukrajinskými	ukrajinský	k2eAgFnPc7d1	ukrajinská
jednotkami	jednotka	k1gFnPc7	jednotka
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
trvály	trvála	k1gFnSc2	trvála
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
Židé	Žid	k1gMnPc1	Žid
považovali	považovat	k5eAaImAgMnP	považovat
Rudou	ruda	k1gFnSc7	ruda
armády	armáda	k1gFnSc2	armáda
jako	jako	k8xS	jako
jedinou	jediný	k2eAgFnSc7d1	jediná
sílou	síla	k1gFnSc7	síla
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
<g/>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
3	[number]	k4	3
450	[number]	k4	450
Židů	Žid	k1gMnPc2	Žid
či	či	k8xC	či
2,3	[number]	k4	2,3
procenta	procento	k1gNnSc2	procento
židovských	židovský	k2eAgFnPc2d1	židovská
obětí	oběť	k1gFnPc2	oběť
bylo	být	k5eAaImAgNnS	být
během	během	k7c2	během
ruské	ruský	k2eAgFnSc2d1	ruská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
zabito	zabít	k5eAaPmNgNnS	zabít
bolševickými	bolševický	k2eAgFnPc7d1	bolševická
silami	síla	k1gFnPc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
podle	podle	k7c2	podle
Morgenthauovy	Morgenthauův	k2eAgFnSc2d1	Morgenthauův
zprávy	zpráva	k1gFnSc2	zpráva
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
asi	asi	k9	asi
300	[number]	k4	300
Židů	Žid	k1gMnPc2	Žid
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
incidentech	incident	k1gInPc6	incident
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
polskou	polský	k2eAgFnSc4d1	polská
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
rovněž	rovněž	k9	rovněž
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
polské	polský	k2eAgFnPc1d1	polská
vojenské	vojenský	k2eAgFnPc1d1	vojenská
a	a	k8xC	a
civilní	civilní	k2eAgFnPc1d1	civilní
orgány	orgány	k1gFnPc1	orgány
dělaly	dělat	k5eAaImAgFnP	dělat
co	co	k8xS	co
mohly	moct	k5eAaImAgFnP	moct
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
takovým	takový	k3xDgInPc3	takový
incidentům	incident	k1gInPc3	incident
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
a	a	k8xC	a
také	také	k9	také
jejich	jejich	k3xOp3gNnSc4	jejich
opakování	opakování	k1gNnSc4	opakování
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
Morgenthau	Morgenthaus	k1gInSc2	Morgenthaus
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
formy	forma	k1gFnPc1	forma
diskriminace	diskriminace	k1gFnSc2	diskriminace
Židů	Žid	k1gMnPc2	Žid
byly	být	k5eAaImAgInP	být
spíše	spíše	k9	spíše
politické	politický	k2eAgFnPc1d1	politická
než	než	k8xS	než
antisemitské	antisemitský	k2eAgFnPc1d1	antisemitská
povahy	povaha	k1gFnPc1	povaha
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
se	se	k3xPyFc4	se
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
užívání	užívání	k1gNnSc1	užívání
termínu	termín	k1gInSc2	termín
"	"	kIx"	"
<g/>
pogrom	pogrom	k1gInSc1	pogrom
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
vztahoval	vztahovat	k5eAaImAgInS	vztahovat
na	na	k7c4	na
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
excesů	exces	k1gInPc2	exces
a	a	k8xC	a
neměl	mít	k5eNaImAgMnS	mít
žádnou	žádný	k3yNgFnSc4	žádný
specifickou	specifický	k2eAgFnSc4d1	specifická
definici	definice	k1gFnSc4	definice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nepřátelé	nepřítel	k1gMnPc1	nepřítel
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
postupu	postup	k1gInSc2	postup
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
v	v	k7c4	v
Poláky	Polák	k1gMnPc4	Polák
držených	držený	k2eAgFnPc2d1	držená
územích	území	k1gNnPc6	území
byla	být	k5eAaImAgFnS	být
nasazena	nasazen	k2eAgFnSc1d1	nasazena
Čeka	čeka	k1gFnSc1	čeka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uplatňovala	uplatňovat	k5eAaImAgFnS	uplatňovat
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
doktrínu	doktrína	k1gFnSc4	doktrína
mimo	mimo	k7c4	mimo
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
běžně	běžně	k6eAd1	běžně
hromadně	hromadně	k6eAd1	hromadně
popravovala	popravovat	k5eAaImAgFnS	popravovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
nepřátele	nepřítel	k1gMnPc4	nepřítel
lidu	lid	k1gInSc2	lid
<g/>
"	"	kIx"	"
zajaté	zajatý	k2eAgMnPc4d1	zajatý
důstojníky	důstojník	k1gMnPc4	důstojník
<g/>
,	,	kIx,	,
duchovní	duchovní	k1gMnPc4	duchovní
a	a	k8xC	a
členy	člen	k1gMnPc4	člen
šlechty	šlechta	k1gFnSc2	šlechta
nebo	nebo	k8xC	nebo
kohokoliv	kdokoliv	k3yInSc4	kdokoliv
podezřelého	podezřelý	k2eAgMnSc4d1	podezřelý
<g/>
.	.	kIx.	.
</s>
<s>
Čeka	čeka	k1gFnSc1	čeka
také	také	k9	také
vybízela	vybízet	k5eAaImAgFnS	vybízet
sovětský	sovětský	k2eAgInSc4d1	sovětský
vojenský	vojenský	k2eAgInSc4d1	vojenský
personál	personál	k1gInSc4	personál
páchat	páchat	k5eAaImF	páchat
násilí	násilí	k1gNnSc4	násilí
proti	proti	k7c3	proti
šlechtický	šlechtický	k2eAgInSc1d1	šlechtický
rodinám	rodina	k1gFnPc3	rodina
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
zabavování	zabavování	k1gNnSc2	zabavování
<g/>
,	,	kIx,	,
rabování	rabování	k1gNnSc1	rabování
a	a	k8xC	a
demolování	demolování	k1gNnSc1	demolování
jejich	jejich	k3xOp3gInSc2	jejich
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
znásilňování	znásilňování	k1gNnPc2	znásilňování
a	a	k8xC	a
vražd	vražda	k1gFnPc2	vražda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
NKVD	NKVD	kA	NKVD
==	==	k?	==
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1922	[number]	k4	1922
byla	být	k5eAaImAgFnS	být
Čeka	čeka	k1gFnSc1	čeka
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
státní	státní	k2eAgFnSc7d1	státní
politickou	politický	k2eAgFnSc7d1	politická
správou	správa	k1gFnSc7	správa
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
OGPU	OGPU	kA	OGPU
<g/>
,	,	kIx,	,
úsek	úsek	k1gInSc4	úsek
NKVD	NKVD	kA	NKVD
<g/>
.	.	kIx.	.
</s>
<s>
Deklarovaná	deklarovaný	k2eAgFnSc1d1	deklarovaná
funkce	funkce	k1gFnSc1	funkce
NKVD	NKVD	kA	NKVD
bylo	být	k5eAaImAgNnS	být
chránit	chránit	k5eAaImF	chránit
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
státu	stát	k1gInSc2	stát
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
prováděno	provádět	k5eAaImNgNnS	provádět
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
měřítku	měřítko	k1gNnSc6	měřítko
politickými	politický	k2eAgFnPc7d1	politická
perzekucemi	perzekuce	k1gFnPc7	perzekuce
"	"	kIx"	"
<g/>
třídních	třídní	k2eAgMnPc2d1	třídní
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
často	často	k6eAd1	často
NKVD	NKVD	kA	NKVD
při	při	k7c6	při
provádění	provádění	k1gNnSc6	provádění
politických	politický	k2eAgFnPc2d1	politická
represí	represe	k1gFnPc2	represe
podporovala	podporovat	k5eAaImAgFnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
síla	síla	k1gFnSc1	síla
a	a	k8xC	a
vězeňská	vězeňský	k2eAgFnSc1d1	vězeňská
stráž	stráž	k1gFnSc1	stráž
Gulagů	gulag	k1gInPc2	gulag
byla	být	k5eAaImAgFnS	být
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
vojska	vojsko	k1gNnSc2	vojsko
zapojena	zapojit	k5eAaPmNgFnS	zapojit
do	do	k7c2	do
represí	represe	k1gFnPc2	represe
politických	politický	k2eAgMnPc2d1	politický
disidentů	disident	k1gMnPc2	disident
a	a	k8xC	a
podílela	podílet	k5eAaImAgFnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
válečných	válečný	k2eAgInPc6d1	válečný
zločinech	zločin	k1gInPc6	zločin
během	během	k7c2	během
bojových	bojový	k2eAgFnPc2d1	bojová
akcí	akce	k1gFnPc2	akce
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
zvlášť	zvlášť	k6eAd1	zvlášť
odpovědni	odpověden	k2eAgMnPc1d1	odpověden
za	za	k7c4	za
chod	chod	k1gInSc4	chod
politického	politický	k2eAgInSc2d1	politický
režimu	režim	k1gInSc2	režim
v	v	k7c6	v
Gulagu	gulag	k1gInSc6	gulag
<g/>
,	,	kIx,	,
vedení	vedení	k1gNnSc1	vedení
masových	masový	k2eAgFnPc2d1	masová
deportací	deportace	k1gFnPc2	deportace
a	a	k8xC	a
nuceného	nucený	k2eAgNnSc2d1	nucené
vysídlování	vysídlování	k1gNnSc2	vysídlování
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
zamířeno	zamířit	k5eAaPmNgNnS	zamířit
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	s	k7c7	s
sovětským	sovětský	k2eAgInSc7d1	sovětský
úřadům	úřad	k1gInPc3	úřad
jevily	jevit	k5eAaImAgInP	jevit
jako	jako	k8xS	jako
nepřátelské	přátelský	k2eNgInPc1d1	nepřátelský
vůči	vůči	k7c3	vůči
sovětské	sovětský	k2eAgFnSc3d1	sovětská
politice	politika	k1gFnSc3	politika
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
podezřelé	podezřelý	k2eAgInPc1d1	podezřelý
ze	z	k7c2	z
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Čečenců	Čečenec	k1gInPc2	Čečenec
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
krymských	krymský	k2eAgMnPc2d1	krymský
Tatarů	Tatar	k1gMnPc2	Tatar
a	a	k8xC	a
Korejců	Korejec	k1gMnPc2	Korejec
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
spáchána	spáchat	k5eAaPmNgFnS	spáchat
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
NKVD	NKVD	kA	NKVD
série	série	k1gFnPc4	série
masových	masový	k2eAgFnPc2d1	masová
poprav	poprava	k1gFnPc2	poprava
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Pobaltí	Pobaltí	k1gNnSc6	Pobaltí
<g/>
,	,	kIx,	,
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
částech	část	k1gFnPc6	část
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
SSSR	SSSR	kA	SSSR
obsadil	obsadit	k5eAaPmAgMnS	obsadit
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
po	po	k7c6	po
německém	německý	k2eAgInSc6d1	německý
útoku	útok	k1gInSc6	útok
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
ustupovala	ustupovat	k5eAaImAgFnS	ustupovat
<g/>
,	,	kIx,	,
objevily	objevit	k5eAaPmAgFnP	objevit
se	se	k3xPyFc4	se
četné	četný	k2eAgFnPc1d1	četná
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
válečných	válečný	k2eAgInPc6d1	válečný
zločinech	zločin	k1gInPc6	zločin
spáchaných	spáchaný	k2eAgFnPc2d1	spáchaná
sovětskými	sovětský	k2eAgFnPc7d1	sovětská
ozbrojenými	ozbrojený	k2eAgFnPc7d1	ozbrojená
silami	síla	k1gFnPc7	síla
proti	proti	k7c3	proti
zajatým	zajatý	k2eAgMnPc3d1	zajatý
vojákům	voják	k1gMnPc3	voják
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
a	a	k8xC	a
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
už	už	k9	už
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
začátku	začátek	k1gInSc2	začátek
bojových	bojový	k2eAgFnPc2d1	bojová
akcí	akce	k1gFnPc2	akce
popsané	popsaný	k2eAgFnPc4d1	popsaná
v	v	k7c6	v
tisících	tisící	k4xOgInPc6	tisící
souborů	soubor	k1gInPc2	soubor
z	z	k7c2	z
"	"	kIx"	"
<g/>
Úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
válečných	válečný	k2eAgInPc2d1	válečný
zločinů	zločin	k1gInPc2	zločin
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Wehrmacht-Untersuchungsstelle	Wehrmacht-Untersuchungsstelle	k1gInSc1	Wehrmacht-Untersuchungsstelle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prošetřoval	prošetřovat	k5eAaImAgInS	prošetřovat
porušování	porušování	k1gNnSc4	porušování
ženevských	ženevský	k2eAgFnPc2d1	Ženevská
a	a	k8xC	a
haagských	haagský	k2eAgFnPc2d1	Haagská
úmluv	úmluva	k1gFnPc2	úmluva
nepřátel	nepřítel	k1gMnPc2	nepřítel
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lépe	dobře	k6eAd2	dobře
zdokumentované	zdokumentovaný	k2eAgInPc1d1	zdokumentovaný
sovětské	sovětský	k2eAgInPc1d1	sovětský
masakry	masakr	k1gInPc1	masakr
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgFnPc4	ten
z	z	k7c2	z
Broniki	Bronik	k1gFnSc2	Bronik
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Feodosije	Feodosije	k1gFnSc1	Feodosije
(	(	kIx(	(
<g/>
prosinec	prosinec	k1gInSc1	prosinec
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
a	a	k8xC	a
Grišina	Grišina	k1gFnSc1	Grišina
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgNnPc1d1	vnitřní
vojska	vojsko	k1gNnPc1	vojsko
NKVD	NKVD	kA	NKVD
byla	být	k5eAaImAgFnS	být
zapojena	zapojit	k5eAaPmNgFnS	zapojit
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
do	do	k7c2	do
boje	boj	k1gInSc2	boj
a	a	k8xC	a
jednotky	jednotka	k1gFnPc1	jednotka
NKVD	NKVD	kA	NKVD
byly	být	k5eAaImAgFnP	být
použity	použít	k5eAaPmNgFnP	použít
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
oblastí	oblast	k1gFnPc2	oblast
v	v	k7c6	v
zádi	záď	k1gFnSc6	záď
jako	jako	k9	jako
blokační	blokační	k2eAgFnPc1d1	blokační
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
okupovaném	okupovaný	k2eAgInSc6d1	okupovaný
<g/>
,	,	kIx,	,
či	či	k8xC	či
nově	nově	k6eAd1	nově
obsazených	obsazený	k2eAgNnPc6d1	obsazené
územích	území	k1gNnPc6	území
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
SSSR	SSSR	kA	SSSR
hodlal	hodlat	k5eAaImAgInS	hodlat
připojit	připojit	k5eAaPmF	připojit
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
území	území	k1gNnSc3	území
<g/>
,	,	kIx,	,
území	území	k1gNnSc3	území
prováděla	provádět	k5eAaImAgFnS	provádět
NKVD	NKVD	kA	NKVD
masové	masový	k2eAgNnSc1d1	masové
zatýkání	zatýkání	k1gNnSc1	zatýkání
<g/>
,	,	kIx,	,
deportace	deportace	k1gFnSc1	deportace
<g/>
,	,	kIx,	,
popravy	poprava	k1gFnPc4	poprava
<g/>
;	;	kIx,	;
vraždila	vraždit	k5eAaImAgFnS	vraždit
nepohodlné	pohodlný	k2eNgMnPc4d1	nepohodlný
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Cíle	cíl	k1gInPc1	cíl
zahrnovaly	zahrnovat	k5eAaImAgInP	zahrnovat
kolaboranty	kolaborant	k1gMnPc4	kolaborant
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
členy	člen	k1gInPc7	člen
antikomunistického	antikomunistický	k2eAgInSc2d1	antikomunistický
odboje	odboj	k1gInSc2	odboj
jako	jako	k8xC	jako
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
povstalecká	povstalecký	k2eAgFnSc1d1	povstalecká
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
mj.	mj.	kA	mj.
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
masakru	masakr	k1gInSc6	masakr
50	[number]	k4	50
000	[number]	k4	000
Poláků	Polák	k1gMnPc2	Polák
a	a	k8xC	a
Čechů	Čech	k1gMnPc2	Čech
ve	v	k7c6	v
Volyni	Volyně	k1gFnSc6	Volyně
během	během	k7c2	během
tzv.	tzv.	kA	tzv.
Volyňského	volyňský	k2eAgInSc2d1	volyňský
masakru	masakr	k1gInSc2	masakr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnSc4d1	další
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
Sověti	Sovět	k1gMnPc1	Sovět
pronásledovali	pronásledovat	k5eAaImAgMnP	pronásledovat
<g/>
,	,	kIx,	,
patřili	patřit	k5eAaImAgMnP	patřit
Lesní	lesní	k2eAgMnPc1d1	lesní
bratři	bratr	k1gMnPc1	bratr
v	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
<g/>
,	,	kIx,	,
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
a	a	k8xC	a
Litvě	Litva	k1gFnSc6	Litva
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
záminkou	záminka	k1gFnSc7	záminka
likvidace	likvidace	k1gFnSc2	likvidace
pronacistických	pronacistický	k2eAgFnPc2d1	pronacistická
band	banda	k1gFnPc2	banda
likvidovali	likvidovat	k5eAaBmAgMnP	likvidovat
i	i	k9	i
teoreticky	teoreticky	k6eAd1	teoreticky
spojeneckou	spojenecký	k2eAgFnSc4d1	spojenecká
polskou	polský	k2eAgFnSc4d1	polská
Zemskou	zemský	k2eAgFnSc4d1	zemská
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
se	se	k3xPyFc4	se
vojensky	vojensky	k6eAd1	vojensky
zapojil	zapojit	k5eAaPmAgInS	zapojit
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
přepadením	přepadení	k1gNnSc7	přepadení
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
chtěl	chtít	k5eAaImAgMnS	chtít
navrátit	navrátit	k5eAaPmF	navrátit
ztracená	ztracený	k2eAgNnPc4d1	ztracené
území	území	k1gNnPc4	území
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
sporadických	sporadický	k2eAgInPc2d1	sporadický
bojů	boj	k1gInPc2	boj
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
polských	polský	k2eAgFnPc2d1	polská
jednotek	jednotka	k1gFnPc2	jednotka
bojovala	bojovat	k5eAaImAgFnS	bojovat
s	s	k7c7	s
Wehrmachtem	wehrmacht	k1gInSc7	wehrmacht
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
záboru	zábor	k1gInSc6	zábor
Polska	Polsko	k1gNnSc2	Polsko
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
zajaty	zajat	k2eAgInPc1d1	zajat
tisíce	tisíc	k4xCgInPc1	tisíc
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
doplněni	doplnit	k5eAaPmNgMnP	doplnit
dalšími	další	k2eAgInPc7d1	další
tisíci	tisíc	k4xCgInPc7	tisíc
zajatými	zajatý	k2eAgMnPc7d1	zajatý
civilisty	civilista	k1gMnPc7	civilista
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kněžími	kněz	k1gMnPc7	kněz
a	a	k8xC	a
polskou	polský	k2eAgFnSc7d1	polská
inteligencí	inteligence	k1gFnSc7	inteligence
<g/>
.	.	kIx.	.
22	[number]	k4	22
tisíc	tisíc	k4xCgInSc4	tisíc
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
během	během	k7c2	během
katyňského	katyňský	k2eAgInSc2d1	katyňský
masakru	masakr	k1gInSc2	masakr
mezi	mezi	k7c7	mezi
dubnem	duben	k1gInSc7	duben
a	a	k8xC	a
květnem	květen	k1gInSc7	květen
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
také	také	k6eAd1	také
řídila	řídit	k5eAaImAgFnS	řídit
NKVD	NKVD	kA	NKVD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
konečném	konečný	k2eAgNnSc6d1	konečné
vytlačení	vytlačení	k1gNnSc6	vytlačení
německých	německý	k2eAgFnPc2d1	německá
sil	síla	k1gFnPc2	síla
ze	z	k7c2	z
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
začala	začít	k5eAaPmAgFnS	začít
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
pronikat	pronikat	k5eAaImF	pronikat
do	do	k7c2	do
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
zemí	zem	k1gFnPc2	zem
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
a	a	k8xC	a
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
samotného	samotný	k2eAgNnSc2d1	samotné
<g/>
.	.	kIx.	.
</s>
<s>
Sovětským	sovětský	k2eAgMnPc3d1	sovětský
vojákům	voják	k1gMnPc3	voják
byly	být	k5eAaImAgInP	být
už	už	k9	už
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
známy	znám	k2eAgInPc4d1	znám
německé	německý	k2eAgInPc4d1	německý
válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
a	a	k8xC	a
často	často	k6eAd1	často
popravovali	popravovat	k5eAaImAgMnP	popravovat
vzdávající	vzdávající	k2eAgMnPc1d1	vzdávající
se	se	k3xPyFc4	se
nebo	nebo	k8xC	nebo
zajaté	zajatý	k2eAgMnPc4d1	zajatý
německé	německý	k2eAgMnPc4d1	německý
vojáky	voják	k1gMnPc4	voják
jako	jako	k8xC	jako
odvetu	odveta	k1gFnSc4	odveta
<g/>
.	.	kIx.	.
</s>
<s>
Docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
četným	četný	k2eAgInPc3d1	četný
záznamům	záznam	k1gInPc3	záznam
válečných	válečný	k2eAgFnPc2d1	válečná
zločinů	zločin	k1gInPc2	zločin
sovětských	sovětský	k2eAgFnPc2d1	sovětská
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
–	–	k?	–
loupeže	loupež	k1gFnSc2	loupež
<g/>
,	,	kIx,	,
vraždy	vražda	k1gFnSc2	vražda
civilistů	civilista	k1gMnPc2	civilista
a	a	k8xC	a
znásilňování	znásilňování	k1gNnSc1	znásilňování
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
v	v	k7c6	v
sovětských	sovětský	k2eAgFnPc6d1	sovětská
tak	tak	k6eAd1	tak
i	i	k8xC	i
současných	současný	k2eAgFnPc6d1	současná
ruských	ruský	k2eAgFnPc6d1	ruská
knihách	kniha	k1gFnPc6	kniha
o	o	k7c6	o
"	"	kIx"	"
<g/>
Velké	velký	k2eAgFnSc3d1	velká
vlastenecké	vlastenecký	k2eAgFnSc3d1	vlastenecká
válce	válka	k1gFnSc3	válka
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
zřídka	zřídka	k6eAd1	zřídka
zmiňovány	zmiňován	k2eAgInPc4d1	zmiňován
<g/>
.	.	kIx.	.
<g/>
Válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
sovětských	sovětský	k2eAgFnPc2d1	sovětská
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
proti	proti	k7c3	proti
civilistům	civilista	k1gMnPc3	civilista
a	a	k8xC	a
válečných	válečný	k2eAgInPc2d1	válečný
zajatcům	zajatec	k1gMnPc3	zajatec
na	na	k7c6	na
územích	území	k1gNnPc6	území
okupovaných	okupovaný	k2eAgNnPc6d1	okupované
SSSR	SSSR	kA	SSSR
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939	[number]	k4	1939
a	a	k8xC	a
1941	[number]	k4	1941
v	v	k7c6	v
regionech	region	k1gInPc6	region
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
západní	západní	k2eAgFnSc2d1	západní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
pobaltských	pobaltský	k2eAgInPc2d1	pobaltský
států	stát	k1gInPc2	stát
a	a	k8xC	a
Bessarábie	Bessarábie	k1gFnSc2	Bessarábie
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
válečnými	válečný	k2eAgInPc7d1	válečný
zločiny	zločin	k1gInPc7	zločin
z	z	k7c2	z
letech	let	k1gInPc6	let
1944	[number]	k4	1944
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
pokračujícím	pokračující	k2eAgInSc7d1	pokračující
problémem	problém	k1gInSc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozpuštění	rozpuštění	k1gNnSc3	rozpuštění
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
na	na	k7c6	na
systematičtějším	systematický	k2eAgFnPc3d2	systematičtější
diskuzím	diskuze	k1gFnPc3	diskuze
o	o	k7c6	o
těchto	tento	k3xDgFnPc6	tento
událostech	událost	k1gFnPc6	událost
<g/>
.	.	kIx.	.
<g/>
Sovětské	sovětský	k2eAgFnPc1d1	sovětská
jednotky	jednotka	k1gFnPc1	jednotka
také	také	k9	také
použily	použít	k5eAaPmAgFnP	použít
yperitové	yperitový	k2eAgFnPc1d1	yperitový
pumy	puma	k1gFnPc1	puma
během	během	k7c2	během
invaze	invaze	k1gFnSc2	invaze
do	do	k7c2	do
Sin-ťiangu	Sin-ťiang	k1gInSc2	Sin-ťiang
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
civilistů	civilista	k1gMnPc2	civilista
byli	být	k5eAaImAgMnP	být
zabiti	zabit	k2eAgMnPc1d1	zabit
konvenčními	konvenční	k2eAgFnPc7d1	konvenční
pumami	puma	k1gFnPc7	puma
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
NKVD	NKVD	kA	NKVD
z	z	k7c2	z
území	území	k1gNnSc2	území
osvobozených	osvobozený	k2eAgMnPc2d1	osvobozený
spřátelených	spřátelený	k2eAgMnPc2d1	spřátelený
států	stát	k1gInPc2	stát
nepohodlné	pohodlný	k2eNgFnSc2d1	nepohodlná
lidi	člověk	k1gMnPc4	člověk
odvlékala	odvlékat	k5eAaImAgFnS	odvlékat
(	(	kIx(	(
<g/>
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
se	se	k3xPyFc4	se
často	často	k6eAd1	často
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
únos	únos	k1gInSc4	únos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
bylo	být	k5eAaImAgNnS	být
NKVD	NKVD	kA	NKVD
krátce	krátce	k6eAd1	krátce
po	po	k7c4	po
osvobození	osvobození	k1gNnSc4	osvobození
uneseno	unesen	k2eAgNnSc4d1	uneseno
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInSc1	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
ruských	ruský	k2eAgMnPc2d1	ruský
emigrantů	emigrant	k1gMnPc2	emigrant
z	z	k7c2	z
období	období	k1gNnSc2	období
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
povražděna	povražděn	k2eAgFnSc1d1	povražděna
nebo	nebo	k8xC	nebo
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
Gulagu	gulag	k1gInSc6	gulag
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
generál	generál	k1gMnSc1	generál
Sergej	Sergej	k1gMnSc1	Sergej
Vojcechovský	Vojcechovský	k2eAgMnSc1d1	Vojcechovský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Estonsko	Estonsko	k1gNnSc1	Estonsko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
paktem	pakt	k1gInSc7	pakt
Ribbentrop-Molotov	Ribbentrop-Molotov	k1gInSc4	Ribbentrop-Molotov
bylo	být	k5eAaImAgNnS	být
Estonsko	Estonsko	k1gNnSc1	Estonsko
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1940	[number]	k4	1940
nezákonně	zákonně	k6eNd1	zákonně
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
připojeno	připojit	k5eAaPmNgNnS	připojit
a	a	k8xC	a
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Estonskou	estonský	k2eAgFnSc4d1	Estonská
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
34	[number]	k4	34
000	[number]	k4	000
Estonců	Estonec	k1gMnPc2	Estonec
povolano	povolana	k1gFnSc5	povolana
do	do	k7c2	do
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
válku	válka	k1gFnSc4	válka
přežilo	přežít	k5eAaPmAgNnS	přežít
ani	ani	k8xC	ani
ne	ne	k9	ne
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Sotva	sotva	k6eAd1	sotva
polovina	polovina	k1gFnSc1	polovina
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
mužů	muž	k1gMnPc2	muž
sloužila	sloužit	k5eAaImAgFnS	sloužit
u	u	k7c2	u
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
už	už	k9	už
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
měsících	měsíc	k1gInPc6	měsíc
války	válka	k1gFnSc2	válka
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
v	v	k7c6	v
Gulagách	Gulagách	k?	Gulagách
a	a	k8xC	a
pracovních	pracovní	k2eAgInPc6d1	pracovní
praporech	prapor	k1gInPc6	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
německá	německý	k2eAgFnSc1d1	německá
invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Estonska	Estonsko	k1gNnSc2	Estonsko
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
političtí	politický	k2eAgMnPc1d1	politický
vězni	vězeň	k1gMnPc1	vězeň
místo	místo	k7c2	místo
evakuace	evakuace	k1gFnSc2	evakuace
popraveni	popravit	k5eAaPmNgMnP	popravit
NKVD	NKVD	kA	NKVD
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
poté	poté	k6eAd1	poté
nemohli	moct	k5eNaImAgMnP	moct
navázat	navázat	k5eAaPmF	navázat
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
000	[number]	k4	000
občanů	občan	k1gMnPc2	občan
Estonska	Estonsko	k1gNnSc2	Estonsko
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
třetina	třetina	k1gFnSc1	třetina
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
populace	populace	k1gFnSc2	populace
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
buď	buď	k8xC	buď
deportována	deportován	k2eAgFnSc1d1	deportována
<g/>
,	,	kIx,	,
zatčena	zatčen	k2eAgFnSc1d1	zatčena
<g/>
,	,	kIx,	,
popravena	popravit	k5eAaPmNgFnS	popravit
nebo	nebo	k8xC	nebo
vystavena	vystavit	k5eAaPmNgFnS	vystavit
jiným	jiný	k2eAgFnPc3d1	jiná
represím	represe	k1gFnPc3	represe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
sovětského	sovětský	k2eAgNnSc2d1	sovětské
převzetí	převzetí	k1gNnSc2	převzetí
moci	moc	k1gFnSc2	moc
přišlo	přijít	k5eAaPmAgNnS	přijít
Estonsko	Estonsko	k1gNnSc4	Estonsko
o	o	k7c4	o
minimálně	minimálně	k6eAd1	minimálně
200	[number]	k4	200
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
či	či	k8xC	či
20	[number]	k4	20
%	%	kIx~	%
své	svůj	k3xOyFgFnSc2	svůj
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
represí	represe	k1gFnPc2	represe
<g/>
,	,	kIx,	,
vysídlení	vysídlení	k1gNnSc2	vysídlení
a	a	k8xC	a
války	válka	k1gFnSc2	válka
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
<g/>
Sovětské	sovětský	k2eAgFnSc2d1	sovětská
politické	politický	k2eAgFnSc2d1	politická
represe	represe	k1gFnSc2	represe
v	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
se	se	k3xPyFc4	se
setkaly	setkat	k5eAaPmAgFnP	setkat
s	s	k7c7	s
ozbrojeným	ozbrojený	k2eAgInSc7d1	ozbrojený
odporem	odpor	k1gInSc7	odpor
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Lesních	lesní	k2eAgMnPc2d1	lesní
bratří	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
složených	složený	k2eAgFnPc2d1	složená
z	z	k7c2	z
bývalých	bývalý	k2eAgMnPc2d1	bývalý
branců	branec	k1gMnPc2	branec
do	do	k7c2	do
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
milicí	milice	k1gFnSc7	milice
Omakaitse	Omakaits	k1gMnSc2	Omakaits
a	a	k8xC	a
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
z	z	k7c2	z
finského	finský	k2eAgMnSc2d1	finský
200	[number]	k4	200
<g/>
.	.	kIx.	.
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
až	až	k9	až
do	do	k7c2	do
pozdních	pozdní	k2eAgInPc2d1	pozdní
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vedli	vést	k5eAaImAgMnP	vést
partyzánskou	partyzánský	k2eAgFnSc4d1	Partyzánská
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
očekávaných	očekávaný	k2eAgFnPc2d1	očekávaná
lidských	lidský	k2eAgFnPc2d1	lidská
a	a	k8xC	a
materiálních	materiální	k2eAgFnPc2d1	materiální
ztrát	ztráta	k1gFnPc2	ztráta
utrpěných	utrpěný	k2eAgFnPc2d1	utrpěná
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
bojů	boj	k1gInPc2	boj
vedl	vést	k5eAaImAgInS	vést
tento	tento	k3xDgInSc4	tento
tento	tento	k3xDgInSc4	tento
konflikt	konflikt	k1gInSc4	konflikt
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
konce	konec	k1gInSc2	konec
k	k	k7c3	k
deportacím	deportace	k1gFnPc3	deportace
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
stovkami	stovka	k1gFnPc7	stovka
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
a	a	k8xC	a
tisíců	tisíc	k4xCgInPc2	tisíc
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Masové	masový	k2eAgFnSc2d1	masová
deportace	deportace	k1gFnSc2	deportace
====	====	k?	====
</s>
</p>
<p>
<s>
Desítky	desítka	k1gFnPc1	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
estonských	estonský	k2eAgMnPc2d1	estonský
občanů	občan	k1gMnPc2	občan
prošli	projít	k5eAaPmAgMnP	projít
během	během	k7c2	během
sovětské	sovětský	k2eAgFnSc2d1	sovětská
okupace	okupace	k1gFnSc2	okupace
deportacemi	deportace	k1gFnPc7	deportace
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
a	a	k8xC	a
do	do	k7c2	do
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
pomocí	pomocí	k7c2	pomocí
železnice	železnice	k1gFnSc2	železnice
–	–	k?	–
dobytčáků	dobytčák	k1gInPc2	dobytčák
bez	bez	k7c2	bez
předchozího	předchozí	k2eAgNnSc2d1	předchozí
oznámení	oznámení	k1gNnSc2	oznámení
<g/>
.	.	kIx.	.
</s>
<s>
Deportování	deportování	k1gNnSc4	deportování
dostali	dostat	k5eAaPmAgMnP	dostat
jen	jen	k9	jen
několik	několik	k4yIc4	několik
nočních	noční	k2eAgFnPc2d1	noční
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejlepším	dobrý	k2eAgInSc6d3	nejlepší
případě	případ	k1gInSc6	případ
stačili	stačit	k5eAaBmAgMnP	stačit
sbalit	sbalit	k5eAaPmF	sbalit
své	svůj	k3xOyFgFnPc4	svůj
věci	věc	k1gFnPc4	věc
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
odděleni	oddělit	k5eAaPmNgMnP	oddělit
od	od	k7c2	od
svých	svůj	k3xOyFgFnPc2	svůj
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obvykle	obvykle	k6eAd1	obvykle
také	také	k9	také
skončily	skončit	k5eAaPmAgInP	skončit
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
se	se	k3xPyFc4	se
zakládal	zakládat	k5eAaImAgInS	zakládat
na	na	k7c6	na
Serovových	Serovův	k2eAgFnPc6d1	Serovův
instrukcích	instrukce	k1gFnPc6	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Estonci	Estonec	k1gMnPc1	Estonec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
leningradské	leningradský	k2eAgFnSc6d1	Leningradská
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
s	s	k7c7	s
deportacemi	deportace	k1gFnPc7	deportace
setkávali	setkávat	k5eAaImAgMnP	setkávat
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
paktem	pakt	k1gInSc7	pakt
Ribbentrop-Molotov	Ribbentrop-Molotovo	k1gNnPc2	Ribbentrop-Molotovo
vtrhli	vtrhnout	k5eAaPmAgMnP	vtrhnout
sovětští	sovětský	k2eAgMnPc1d1	sovětský
vojáci	voják	k1gMnPc1	voják
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1940	[number]	k4	1940
do	do	k7c2	do
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
následně	následně	k6eAd1	následně
začleněno	začlenit	k5eAaPmNgNnS	začlenit
do	do	k7c2	do
SSSR	SSSR	kA	SSSR
jako	jako	k8xC	jako
Lotyšská	lotyšský	k2eAgFnSc1d1	lotyšská
sovětská	sovětský	k2eAgFnSc1d1	sovětská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Litva	Litva	k1gFnSc1	Litva
===	===	k?	===
</s>
</p>
<p>
<s>
Litva	Litva	k1gFnSc1	Litva
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
zbytek	zbytek	k1gInSc4	zbytek
Pobaltí	Pobaltí	k1gNnSc4	Pobaltí
zájmu	zájem	k1gInSc2	zájem
neunikla	uniknout	k5eNaPmAgFnS	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
byla	být	k5eAaImAgFnS	být
napadena	napaden	k2eAgFnSc1d1	napadena
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1940	[number]	k4	1940
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1940	[number]	k4	1940
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
začlenění	začlenění	k1gNnSc3	začlenění
do	do	k7c2	do
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
masový	masový	k2eAgInSc4d1	masový
teror	teror	k1gInSc4	teror
<g/>
,	,	kIx,	,
likvidacím	likvidace	k1gFnPc3	likvidace
občanských	občanský	k2eAgFnPc2d1	občanská
svobod	svoboda	k1gFnPc2	svoboda
<g/>
,	,	kIx,	,
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
systému	systém	k1gInSc2	systém
a	a	k8xC	a
litevské	litevský	k2eAgFnSc2d1	Litevská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1940	[number]	k4	1940
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
byly	být	k5eAaImAgFnP	být
tisíce	tisíc	k4xCgInPc1	tisíc
Litevců	Litevec	k1gMnPc2	Litevec
zatčeni	zatčen	k2eAgMnPc1d1	zatčen
a	a	k8xC	a
stovky	stovka	k1gFnPc1	stovka
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
byly	být	k5eAaImAgFnP	být
svévolně	svévolně	k6eAd1	svévolně
popraveni	popravit	k5eAaPmNgMnP	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
17	[number]	k4	17
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1941	[number]	k4	1941
deportováno	deportovat	k5eAaBmNgNnS	deportovat
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
německém	německý	k2eAgInSc6d1	německý
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
byl	být	k5eAaImAgInS	být
začínající	začínající	k2eAgInSc1d1	začínající
sovětský	sovětský	k2eAgInSc1d1	sovětský
politický	politický	k2eAgInSc1d1	politický
aparát	aparát	k1gInSc1	aparát
buď	buď	k8xC	buď
zlikvidován	zlikvidován	k2eAgInSc1d1	zlikvidován
nebo	nebo	k8xC	nebo
ustoupil	ustoupit	k5eAaPmAgInS	ustoupit
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Litva	Litva	k1gFnSc1	Litva
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
cca	cca	kA	cca
3	[number]	k4	3
roky	rok	k1gInPc7	rok
obsazena	obsadit	k5eAaPmNgNnP	obsadit
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
se	se	k3xPyFc4	se
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
a	a	k8xC	a
sovětská	sovětský	k2eAgFnSc1d1	sovětská
okupace	okupace	k1gFnSc1	okupace
Litvy	Litva	k1gFnSc2	Litva
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
následném	následný	k2eAgNnSc6d1	následné
potlačení	potlačení	k1gNnSc6	potlačení
litevských	litevský	k2eAgMnPc2d1	litevský
Lesních	lesní	k2eAgMnPc2d1	lesní
bratří	bratr	k1gMnPc2	bratr
sovětské	sovětský	k2eAgInPc1d1	sovětský
úřady	úřad	k1gInPc1	úřad
popravily	popravit	k5eAaPmAgInP	popravit
tisíce	tisíc	k4xCgInPc1	tisíc
odbojářů	odbojář	k1gMnPc2	odbojář
a	a	k8xC	a
civilistů	civilista	k1gMnPc2	civilista
obviněných	obviněný	k2eAgMnPc2d1	obviněný
z	z	k7c2	z
napomáhání	napomáhání	k1gNnSc2	napomáhání
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
300	[number]	k4	300
000	[number]	k4	000
Litevců	Litevec	k1gMnPc2	Litevec
bylo	být	k5eAaImAgNnS	být
deportováno	deportovat	k5eAaBmNgNnS	deportovat
nebo	nebo	k8xC	nebo
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
odsouzeno	odsoudit	k5eAaPmNgNnS	odsoudit
do	do	k7c2	do
lágrů	lágr	k1gInPc2	lágr
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Litva	Litva	k1gFnSc1	Litva
přišla	přijít	k5eAaPmAgFnS	přijít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
sovětské	sovětský	k2eAgFnSc2d1	sovětská
okupace	okupace	k1gFnSc2	okupace
o	o	k7c4	o
téměř	téměř	k6eAd1	téměř
780	[number]	k4	780
000	[number]	k4	000
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
asi	asi	k9	asi
440	[number]	k4	440
000	[number]	k4	000
byli	být	k5eAaImAgMnP	být
váleční	váleční	k2eAgMnPc1d1	váleční
uprchlíci	uprchlík	k1gMnPc1	uprchlík
<g/>
.	.	kIx.	.
<g/>
Odhadovaný	odhadovaný	k2eAgInSc4d1	odhadovaný
počet	počet	k1gInSc4	počet
obětí	oběť	k1gFnPc2	oběť
v	v	k7c6	v
sovětských	sovětský	k2eAgFnPc6d1	sovětská
věznicích	věznice	k1gFnPc6	věznice
a	a	k8xC	a
táborech	tábor	k1gInPc6	tábor
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1944	[number]	k4	1944
a	a	k8xC	a
1953	[number]	k4	1953
je	být	k5eAaImIp3nS	být
přinejmenším	přinejmenším	k6eAd1	přinejmenším
14	[number]	k4	14
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Odhadovaný	odhadovaný	k2eAgInSc1d1	odhadovaný
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
mezi	mezi	k7c7	mezi
deportovanými	deportovaný	k2eAgFnPc7d1	deportovaná
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
a	a	k8xC	a
1958	[number]	k4	1958
je	být	k5eAaImIp3nS	být
20	[number]	k4	20
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
5000	[number]	k4	5000
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
lednových	lednový	k2eAgFnPc2d1	lednová
událostí	událost	k1gFnPc2	událost
(	(	kIx(	(
<g/>
litevské	litevský	k2eAgNnSc1d1	litevské
obnovení	obnovení	k1gNnSc1	obnovení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
zabila	zabít	k5eAaPmAgFnS	zabít
sovětská	sovětský	k2eAgFnSc1d1	sovětská
armáda	armáda	k1gFnSc1	armáda
ve	v	k7c6	v
Vilniusu	Vilnius	k1gInSc6	Vilnius
13	[number]	k4	13
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Polsko	Polsko	k1gNnSc1	Polsko
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1939	[number]	k4	1939
napadla	napadnout	k5eAaPmAgFnS	napadnout
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
východní	východní	k2eAgNnSc1d1	východní
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
tajným	tajný	k2eAgInSc7d1	tajný
protokolem	protokol	k1gInSc7	protokol
Molotov-Ribbentrop	Molotov-Ribbentrop	k1gInSc4	Molotov-Ribbentrop
zabrala	zabrat	k5eAaPmAgFnS	zabrat
<g/>
.	.	kIx.	.
</s>
<s>
SSSR	SSSR	kA	SSSR
později	pozdě	k6eAd2	pozdě
násilně	násilně	k6eAd1	násilně
obsadil	obsadit	k5eAaPmAgInS	obsadit
i	i	k9	i
pobaltské	pobaltský	k2eAgInPc4d1	pobaltský
státy	stát	k1gInPc4	stát
a	a	k8xC	a
části	část	k1gFnPc4	část
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Besarábie	Besarábie	k1gFnSc2	Besarábie
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Bukoviny	Bukovina	k1gFnSc2	Bukovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německý	německý	k2eAgMnSc1d1	německý
historik	historik	k1gMnSc1	historik
Thomas	Thomas	k1gMnSc1	Thomas
Urban	Urban	k1gMnSc1	Urban
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
sovětská	sovětský	k2eAgFnSc1d1	sovětská
politika	politika	k1gFnSc1	politika
vůči	vůči	k7c3	vůči
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
obydlených	obydlený	k2eAgFnPc6d1	obydlená
oblastech	oblast	k1gFnPc6	oblast
padli	padnout	k5eAaPmAgMnP	padnout
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
drsná	drsný	k2eAgFnSc1d1	drsná
a	a	k8xC	a
vykazovala	vykazovat	k5eAaImAgFnS	vykazovat
silné	silný	k2eAgInPc4d1	silný
prvky	prvek	k1gInPc4	prvek
etnických	etnický	k2eAgFnPc2d1	etnická
čistkek	čistkky	k1gFnPc2	čistkky
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Úkolem	úkol	k1gInSc7	úkol
jednotek	jednotka	k1gFnPc2	jednotka
NKVD	NKVD	kA	NKVD
následujících	následující	k2eAgFnPc2d1	následující
Rudou	rudý	k2eAgFnSc4d1	rudá
armádu	armáda	k1gFnSc4	armáda
bylo	být	k5eAaImAgNnS	být
odstranění	odstranění	k1gNnSc4	odstranění
"	"	kIx"	"
<g/>
nepřátelských	přátelský	k2eNgInPc2d1	nepřátelský
prvků	prvek	k1gInPc2	prvek
<g/>
"	"	kIx"	"
na	na	k7c4	na
dobytých	dobytý	k2eAgFnPc2d1	dobytá
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vešlo	vejít	k5eAaPmAgNnS	vejít
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
revoluce	revoluce	k1gFnSc1	revoluce
oběšením	oběšení	k1gNnSc7	oběšení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Polský	polský	k2eAgMnSc1d1	polský
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Tomasz	Tomasz	k1gMnSc1	Tomasz
Strzembosz	Strzembosz	k1gMnSc1	Strzembosz
<g/>
,	,	kIx,	,
zmínil	zmínit	k5eAaPmAgInS	zmínit
paralely	paralela	k1gFnPc4	paralela
mezi	mezi	k7c7	mezi
nacistickými	nacistický	k2eAgInPc7d1	nacistický
Einsatzgruppen	Einsatzgruppen	k2eAgInSc1d1	Einsatzgruppen
a	a	k8xC	a
těmito	tento	k3xDgFnPc7	tento
sovětskými	sovětský	k2eAgFnPc7d1	sovětská
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
civilistů	civilista	k1gMnPc2	civilista
se	se	k3xPyFc4	se
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
o	o	k7c4	o
útěk	útěk	k1gInSc4	útěk
z	z	k7c2	z
akcí	akce	k1gFnPc2	akce
sovětské	sovětský	k2eAgFnSc2d1	sovětská
NKVD	NKVD	kA	NKVD
známých	známý	k1gMnPc2	známý
jako	jako	k8xS	jako
lapanka	lapanka	k1gFnSc1	lapanka
<g/>
;	;	kIx,	;
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
neunikli	uniknout	k5eNaPmAgMnP	uniknout
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
vzati	vzít	k5eAaPmNgMnP	vzít
do	do	k7c2	do
vazby	vazba	k1gFnSc2	vazba
a	a	k8xC	a
poté	poté	k6eAd1	poté
deportováni	deportovat	k5eAaBmNgMnP	deportovat
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
a	a	k8xC	a
zmizeli	zmizet	k5eAaPmAgMnP	zmizet
v	v	k7c6	v
gulazích	gulag	k1gInPc6	gulag
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
měřítku	měřítko	k1gNnSc6	měřítko
docházelo	docházet	k5eAaImAgNnS	docházet
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
věznicích	věznice	k1gFnPc6	věznice
k	k	k7c3	k
mučení	mučení	k1gNnSc3	mučení
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
menších	malý	k2eAgNnPc6d2	menší
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
Bóbrka	Bóbrk	k1gInSc2	Bóbrk
byli	být	k5eAaImAgMnP	být
vězni	vězeň	k1gMnPc1	vězeň
pařeni	pařit	k5eAaImNgMnP	pařit
vařící	vařící	k2eAgFnSc7d1	vařící
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
v	v	k7c4	v
Peremyšljany	Peremyšljana	k1gFnPc4	Peremyšljana
měli	mít	k5eAaImAgMnP	mít
uřezané	uřezaný	k2eAgInPc4d1	uřezaný
nosy	nos	k1gInPc4	nos
<g/>
,	,	kIx,	,
uši	ucho	k1gNnPc4	ucho
<g/>
,	,	kIx,	,
prsty	prst	k1gInPc4	prst
a	a	k8xC	a
vydloubané	vydloubaný	k2eAgNnSc4d1	vydloubané
oči	oko	k1gNnPc4	oko
<g/>
;	;	kIx,	;
v	v	k7c6	v
Czortkówě	Czortkówa	k1gFnSc6	Czortkówa
měly	mít	k5eAaImAgFnP	mít
ženy	žena	k1gFnPc1	žena
uřezané	uřezaný	k2eAgInPc4d1	uřezaný
prsy	prs	k1gInPc4	prs
a	a	k8xC	a
v	v	k7c6	v
Drohobyči	Drohobyč	k1gInSc6	Drohobyč
byli	být	k5eAaImAgMnP	být
oběti	oběť	k1gFnPc4	oběť
svázány	svázán	k2eAgMnPc4d1	svázán
ostnatým	ostnatý	k2eAgInSc7d1	ostnatý
drátem	drát	k1gInSc7	drát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
podobným	podobný	k2eAgNnPc3d1	podobné
zvěrstvům	zvěrstvo	k1gNnPc3	zvěrstvo
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Samboru	Sambor	k1gInSc6	Sambor
<g/>
,	,	kIx,	,
Stanislawowě	Stanislawowa	k1gFnSc6	Stanislawowa
<g/>
,	,	kIx,	,
Stryji	Stryji	k1gFnSc6	Stryji
a	a	k8xC	a
Zloczowě	Zloczowa	k1gFnSc6	Zloczowa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
historika	historik	k1gMnSc2	historik
<g/>
,	,	kIx,	,
Prof.	prof.	kA	prof.
Jana	Jan	k1gMnSc2	Jan
T.	T.	kA	T.
Grosse	Gross	k1gMnSc2	Gross
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sociologa	sociolog	k1gMnSc2	sociolog
<g/>
,	,	kIx,	,
Prof.	prof.	kA	prof.
Tadeusze	Tadeusze	k1gFnSc2	Tadeusze
Piotrowského	Piotrowský	k1gMnSc2	Piotrowský
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
41	[number]	k4	41
téměř	téměř	k6eAd1	téměř
1,5	[number]	k4	1,5
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
ze	z	k7c2	z
SSSR	SSSR	kA	SSSR
kontrolovaných	kontrolovaný	k2eAgFnPc2d1	kontrolovaná
oblastí	oblast	k1gFnPc2	oblast
bývalého	bývalý	k2eAgNnSc2d1	bývalé
východního	východní	k2eAgNnSc2d1	východní
Polska	Polsko	k1gNnSc2	Polsko
deportováno	deportován	k2eAgNnSc1d1	deportováno
<g/>
.	.	kIx.	.
63,1	[number]	k4	63,1
%	%	kIx~	%
byli	být	k5eAaImAgMnP	být
Poláci	Polák	k1gMnPc1	Polák
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnPc1d1	jiná
národnosti	národnost	k1gFnPc1	národnost
a	a	k8xC	a
7,4	[number]	k4	7,4
%	%	kIx~	%
byli	být	k5eAaImAgMnP	být
Židé	Žid	k1gMnPc1	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
těchto	tento	k3xDgInPc2	tento
deportovaných	deportovaný	k2eAgInPc2d1	deportovaný
válku	válka	k1gFnSc4	válka
přežilo	přežít	k5eAaPmAgNnS	přežít
a	a	k8xC	a
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
amerického	americký	k2eAgMnSc2d1	americký
profesora	profesor	k1gMnSc2	profesor
Carrolla	Carroll	k1gMnSc2	Carroll
Quigleyho	Quigley	k1gMnSc2	Quigley
byla	být	k5eAaImAgFnS	být
alespoň	alespoň	k9	alespoň
třetina	třetina	k1gFnSc1	třetina
z	z	k7c2	z
320	[number]	k4	320
000	[number]	k4	000
polských	polský	k2eAgMnPc2d1	polský
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
zajatých	zajatý	k2eAgMnPc2d1	zajatý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
zavražděna	zavražděn	k2eAgFnSc1d1	zavražděna
<g/>
.	.	kIx.	.
<g/>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
zhruba	zhruba	k6eAd1	zhruba
35	[number]	k4	35
000	[number]	k4	000
polských	polský	k2eAgMnPc2d1	polský
vezňů	vezň	k1gMnPc2	vezň
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
ve	v	k7c6	v
věznicích	věznice	k1gFnPc6	věznice
nebo	nebo	k8xC	nebo
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
veznic	veznice	k1gFnPc2	veznice
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
během	během	k7c2	během
prvních	první	k4xOgInPc2	první
dnů	den	k1gInPc2	den
po	po	k7c4	po
22	[number]	k4	22
<g/>
.	.	kIx.	.
červnu	červen	k1gInSc3	červen
1941	[number]	k4	1941
(	(	kIx(	(
<g/>
věznice	věznice	k1gFnSc2	věznice
<g/>
:	:	kIx,	:
Brygidki	Brygidk	k1gFnSc2	Brygidk
ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
<g/>
,	,	kIx,	,
Zoločiv	Zoločiv	k1gFnSc5	Zoločiv
<g/>
,	,	kIx,	,
Dubno	Dubna	k1gFnSc5	Dubna
<g/>
,	,	kIx,	,
Drohobyč	Drohobyč	k1gMnSc1	Drohobyč
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1944	[number]	k4	1944
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
nacistická	nacistický	k2eAgNnPc1d1	nacistické
zvěrstva	zvěrstvo	k1gNnPc1	zvěrstvo
skončila	skončit	k5eAaPmAgNnP	skončit
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
byla	být	k5eAaImAgFnS	být
hned	hned	k6eAd1	hned
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
sovětskými	sovětský	k2eAgFnPc7d1	sovětská
<g/>
.	.	kIx.	.
</s>
<s>
Sovětští	sovětský	k2eAgMnPc1d1	sovětský
vojáci	voják	k1gMnPc1	voják
často	často	k6eAd1	často
loupili	loupit	k5eAaImAgMnP	loupit
<g/>
,	,	kIx,	,
znásilňovali	znásilňovat	k5eAaImAgMnP	znásilňovat
a	a	k8xC	a
dopouštěli	dopouštět	k5eAaImAgMnP	dopouštět
se	se	k3xPyFc4	se
na	na	k7c6	na
Polácích	Polák	k1gMnPc6	Polák
dalších	další	k2eAgInPc2d1	další
zločinů	zločin	k1gInPc2	zločin
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
populaci	populace	k1gFnSc4	populace
dohnalo	dohnat	k5eAaPmAgNnS	dohnat
ke	k	k7c3	k
strachu	strach	k1gInSc3	strach
a	a	k8xC	a
nenávisti	nenávist	k1gFnSc3	nenávist
k	k	k7c3	k
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
<g/>
Vojáci	voják	k1gMnPc1	voják
polské	polský	k2eAgFnSc2d1	polská
Zemské	zemský	k2eAgFnSc2d1	zemská
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
Armia	Armia	k1gFnSc1	Armia
Krajowa	Krajowa	k1gMnSc1	Krajowa
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
pronásledováni	pronásledován	k2eAgMnPc1d1	pronásledován
a	a	k8xC	a
vězněni	vězněn	k2eAgMnPc1d1	vězněn
ruskými	ruský	k2eAgFnPc7d1	ruská
silami	síla	k1gFnPc7	síla
jako	jako	k8xC	jako
samozřejmost	samozřejmost	k1gFnSc1	samozřejmost
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obětí	oběť	k1gFnPc2	oběť
byla	být	k5eAaImAgFnS	být
odvezena	odvézt	k5eAaPmNgFnS	odvézt
do	do	k7c2	do
gulagů	gulag	k1gInPc2	gulag
poblíž	poblíž	k7c2	poblíž
Doněcka	Doněck	k1gInSc2	Doněck
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
členů	člen	k1gMnPc2	člen
polského	polský	k2eAgInSc2d1	polský
podzemního	podzemní	k2eAgInSc2d1	podzemní
státu	stát	k1gInSc2	stát
deportovaných	deportovaný	k2eAgFnPc2d1	deportovaná
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
a	a	k8xC	a
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
pracovních	pracovní	k2eAgInPc2d1	pracovní
táborů	tábor	k1gInPc2	tábor
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
vyšplhal	vyšplhat	k5eAaPmAgInS	vyšplhat
na	na	k7c4	na
50	[number]	k4	50
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
také	také	k6eAd1	také
prováděly	provádět	k5eAaImAgFnP	provádět
akce	akce	k1gFnSc2	akce
proti	proti	k7c3	proti
polským	polský	k2eAgMnPc3d1	polský
partyzánům	partyzán	k1gMnPc3	partyzán
a	a	k8xC	a
civilistům	civilista	k1gMnPc3	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Augustowského	Augustowský	k2eAgInSc2d1	Augustowský
zátahu	zátah	k1gInSc2	zátah
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2000	[number]	k4	2000
Poláků	Polák	k1gMnPc2	Polák
zajato	zajmout	k5eAaPmNgNnS	zajmout
a	a	k8xC	a
asi	asi	k9	asi
u	u	k7c2	u
600	[number]	k4	600
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
v	v	k7c6	v
sovětském	sovětský	k2eAgNnSc6d1	sovětské
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
poválečném	poválečný	k2eAgInSc6d1	poválečný
odporu	odpor	k1gInSc6	odpor
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
viz	vidět	k5eAaImRp2nS	vidět
článek	článek	k1gInSc4	článek
Prokletí	prokletý	k2eAgMnPc1d1	prokletý
vojáci	voják	k1gMnPc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
běžnou	běžný	k2eAgFnSc4d1	běžná
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
praxi	praxe	k1gFnSc4	praxe
–	–	k?	–
obvinit	obvinit	k5eAaPmF	obvinit
své	svůj	k3xOyFgFnPc4	svůj
oběti	oběť	k1gFnPc4	oběť
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
fašisté	fašista	k1gMnPc1	fašista
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
odůvodnili	odůvodnit	k5eAaPmAgMnP	odůvodnit
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgFnPc1	veškerý
perverze	perverze	k1gFnPc1	perverze
této	tento	k3xDgFnSc2	tento
sovětské	sovětský	k2eAgFnSc2d1	sovětská
taktiky	taktika	k1gFnSc2	taktika
spočívaly	spočívat	k5eAaImAgFnP	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prakticky	prakticky	k6eAd1	prakticky
všichni	všechen	k3xTgMnPc1	všechen
obvinění	obviněný	k1gMnPc1	obviněný
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
bojovali	bojovat	k5eAaImAgMnP	bojovat
od	od	k7c2	od
září	září	k1gNnSc2	září
1939	[number]	k4	1939
proti	proti	k7c3	proti
silám	síla	k1gFnPc3	síla
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Sověti	Sovět	k1gMnPc1	Sovět
ještě	ještě	k9	ještě
s	s	k7c7	s
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
ještě	ještě	k9	ještě
minimálně	minimálně	k6eAd1	minimálně
20	[number]	k4	20
měsíců	měsíc	k1gInPc2	měsíc
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
Operace	operace	k1gFnSc1	operace
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
proto	proto	k8xC	proto
byli	být	k5eAaImAgMnP	být
tito	tento	k3xDgMnPc1	tento
Poláci	Polák	k1gMnPc1	Polák
souzeni	soudit	k5eAaImNgMnP	soudit
Sověty	Sověty	k1gInPc7	Sověty
a	a	k8xC	a
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
bránili	bránit	k5eAaImAgMnP	bránit
před	před	k7c7	před
nacisty	nacista	k1gMnPc7	nacista
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
pod	pod	k7c7	pod
jurisdikcí	jurisdikce	k1gFnSc7	jurisdikce
Polské	polský	k2eAgFnSc2d1	polská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
Sověty	Sovět	k1gMnPc7	Sovět
řízený	řízený	k2eAgInSc4d1	řízený
systém	systém	k1gInSc4	systém
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
modelových	modelový	k2eAgInPc2d1	modelový
soudů	soud	k1gInPc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
byly	být	k5eAaImAgInP	být
organizovány	organizovat	k5eAaBmNgInP	organizovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
oběti	oběť	k1gFnPc4	oběť
byli	být	k5eAaImAgMnP	být
zatčeny	zatknout	k5eAaPmNgFnP	zatknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
falešných	falešný	k2eAgNnPc2d1	falešné
obvinění	obvinění	k1gNnPc2	obvinění
NKVD	NKVD	kA	NKVD
nebo	nebo	k8xC	nebo
jiných	jiný	k1gMnPc2	jiný
<g/>
,	,	kIx,	,
sověty	sovět	k1gInPc4	sovět
řízených	řízený	k2eAgFnPc2d1	řízená
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
bylo	být	k5eAaImAgNnS	být
například	například	k6eAd1	například
polské	polský	k2eAgNnSc1d1	polské
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
veřejné	veřejný	k2eAgFnSc2d1	veřejná
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vyneseno	vynést	k5eAaPmNgNnS	vynést
na	na	k7c4	na
6	[number]	k4	6
000	[number]	k4	000
politických	politický	k2eAgInPc2d1	politický
rozsudků	rozsudek	k1gInPc2	rozsudek
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
provedena	proveden	k2eAgFnSc1d1	provedena
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
komunistických	komunistický	k2eAgFnPc6d1	komunistická
věznicích	věznice	k1gFnPc6	věznice
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k2eAgInPc4d1	známý
příklady	příklad	k1gInPc4	příklad
patří	patřit	k5eAaImIp3nP	patřit
Witold	Witold	k1gInSc4	Witold
Pilecki	Pileck	k1gFnSc2	Pileck
nebo	nebo	k8xC	nebo
Emil	Emil	k1gMnSc1	Emil
August	August	k1gMnSc1	August
Fieldorf	Fieldorf	k1gMnSc1	Fieldorf
<g/>
.	.	kIx.	.
<g/>
Postoj	postoj	k1gInSc1	postoj
sovětských	sovětský	k2eAgFnPc2d1	sovětská
posádek	posádka	k1gFnPc2	posádka
vůči	vůči	k7c3	vůči
etnickým	etnický	k2eAgMnPc3d1	etnický
Polákům	Polák	k1gMnPc3	Polák
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
lepší	dobrý	k2eAgMnSc1d2	lepší
než	než	k8xS	než
vůči	vůči	k7c3	vůči
Němcům	Němec	k1gMnPc3	Němec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
znásilnění	znásilnění	k1gNnSc2	znásilnění
polských	polský	k2eAgFnPc2d1	polská
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
pandemii	pandemie	k1gFnSc3	pandemie
pohlavně	pohlavně	k6eAd1	pohlavně
přenosných	přenosný	k2eAgFnPc2d1	přenosná
chorob	choroba	k1gFnPc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
předmětem	předmět	k1gInSc7	předmět
debat	debata	k1gFnPc2	debata
<g/>
,	,	kIx,	,
z	z	k7c2	z
polských	polský	k2eAgInPc2d1	polský
státních	státní	k2eAgInPc2d1	státní
archivů	archiv	k1gInPc2	archiv
a	a	k8xC	a
statistik	statistik	k1gMnSc1	statistik
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
mohl	moct	k5eAaImAgInS	moct
překročit	překročit	k5eAaPmF	překročit
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
byl	být	k5eAaImAgInS	být
sovětský	sovětský	k2eAgInSc1d1	sovětský
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
města	město	k1gNnSc2	město
doprovázen	doprovázen	k2eAgInSc1d1	doprovázen
masovým	masový	k2eAgNnSc7d1	masové
znásilňováním	znásilňování	k1gNnSc7	znásilňování
polských	polský	k2eAgFnPc2d1	polská
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dívek	dívka	k1gFnPc2	dívka
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
drancováním	drancování	k1gNnSc7	drancování
soukromého	soukromý	k2eAgInSc2d1	soukromý
majetku	majetek	k1gInSc2	majetek
rudoarmějci	rudoarmějec	k1gMnSc3	rudoarmějec
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
chování	chování	k1gNnSc1	chování
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
takového	takový	k3xDgInSc2	takový
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
,	,	kIx,	,
že	že	k8xS	že
polští	polský	k2eAgMnPc1d1	polský
komunisté	komunista	k1gMnPc1	komunista
instalovaní	instalovaný	k2eAgMnPc1d1	instalovaný
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
sepsali	sepsat	k5eAaPmAgMnP	sepsat
samotnému	samotný	k2eAgMnSc3d1	samotný
Stalinovi	Stalin	k1gMnSc3	Stalin
protestní	protestní	k2eAgInSc4d1	protestní
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
církevní	církevní	k2eAgFnPc1d1	církevní
masy	masa	k1gFnPc1	masa
se	se	k3xPyFc4	se
držely	držet	k5eAaImAgFnP	držet
v	v	k7c6	v
očekávaném	očekávaný	k2eAgNnSc6d1	očekávané
stažení	stažení	k1gNnSc6	stažení
Sovětů	Sovět	k1gMnPc2	Sovět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Finsko	Finsko	k1gNnSc1	Finsko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
prováděli	provádět	k5eAaImAgMnP	provádět
sovětští	sovětský	k2eAgMnPc1d1	sovětský
partyzáni	partyzán	k1gMnPc1	partyzán
nájezdy	nájezd	k1gInPc7	nájezd
hluboko	hluboko	k6eAd1	hluboko
uvnitř	uvnitř	k7c2	uvnitř
finského	finský	k2eAgNnSc2d1	finské
území	území	k1gNnSc2	území
a	a	k8xC	a
útočili	útočit	k5eAaImAgMnP	útočit
na	na	k7c4	na
vesnice	vesnice	k1gFnPc4	vesnice
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
civilní	civilní	k2eAgInPc4d1	civilní
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
byly	být	k5eAaImAgFnP	být
finskými	finský	k2eAgInPc7d1	finský
orgány	orgán	k1gInPc7	orgán
odtajněny	odtajnit	k5eAaPmNgInP	odtajnit
fotografie	fotografia	k1gFnSc2	fotografia
ukazující	ukazující	k2eAgNnPc4d1	ukazující
zvěrstva	zvěrstvo	k1gNnPc4	zvěrstvo
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
obrazy	obraz	k1gInPc4	obraz
zabitých	zabitý	k2eAgFnPc2d1	zabitá
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Partyzáni	partyzán	k1gMnPc1	partyzán
zpravidla	zpravidla	k6eAd1	zpravidla
popravovali	popravovat	k5eAaImAgMnP	popravovat
své	svůj	k3xOyFgMnPc4	svůj
vojenské	vojenský	k2eAgMnPc4d1	vojenský
a	a	k8xC	a
civilní	civilní	k2eAgMnPc4d1	civilní
vězně	vězeň	k1gMnPc4	vězeň
po	po	k7c6	po
lehkém	lehký	k2eAgInSc6d1	lehký
výslechu	výslech	k1gInSc6	výslech
<g/>
.	.	kIx.	.
<g/>
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
bylo	být	k5eAaImAgNnS	být
zajato	zajmout	k5eAaPmNgNnS	zajmout
kolem	kolem	k7c2	kolem
3	[number]	k4	3
500	[number]	k4	500
finských	finský	k2eAgMnPc2d1	finský
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
pět	pět	k4xCc4	pět
byly	být	k5eAaImAgInP	být
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
ke	k	k7c3	k
40	[number]	k4	40
procentům	procento	k1gNnPc3	procento
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgFnPc4d3	nejčastější
příčiny	příčina	k1gFnPc4	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
patřily	patřit	k5eAaImAgFnP	patřit
hlad	hlad	k1gInSc4	hlad
<g/>
,	,	kIx,	,
zima	zima	k1gFnSc1	zima
a	a	k8xC	a
nesnesitelný	snesitelný	k2eNgInSc1d1	nesnesitelný
transport	transport	k1gInSc1	transport
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Sovětský	sovětský	k2eAgInSc1d1	sovětský
ústup	ústup	k1gInSc1	ústup
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
====	====	k?	====
</s>
</p>
<p>
<s>
K	k	k7c3	k
deportacím	deportace	k1gFnPc3	deportace
<g/>
,	,	kIx,	,
popravám	poprava	k1gFnPc3	poprava
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
,	,	kIx,	,
pálení	pálení	k1gNnSc3	pálení
venkovských	venkovský	k2eAgInPc2d1	venkovský
skladů	sklad	k1gInPc2	sklad
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
vesnic	vesnice	k1gFnPc2	vesnice
se	se	k3xPyFc4	se
dělo	dít	k5eAaImAgNnS	dít
<g/>
,	,	kIx,	,
když	když	k8xS	když
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
ustupovala	ustupovat	k5eAaImAgFnS	ustupovat
před	před	k7c7	před
postupujícími	postupující	k2eAgFnPc7d1	postupující
silami	síla	k1gFnPc7	síla
Osy	osa	k1gFnSc2	osa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pobaltských	pobaltský	k2eAgFnPc6d1	pobaltská
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
<g/>
,	,	kIx,	,
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
Besarábii	Besarábie	k1gFnSc6	Besarábie
masakrovaly	masakrovat	k5eAaBmAgFnP	masakrovat
prchající	prchající	k2eAgFnPc4d1	prchající
jednotky	jednotka	k1gFnPc4	jednotka
NKVD	NKVD	kA	NKVD
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přidruženými	přidružený	k2eAgFnPc7d1	přidružená
jednotkami	jednotka	k1gFnPc7	jednotka
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
vězně	vězeň	k1gMnPc4	vězeň
a	a	k8xC	a
politické	politický	k2eAgMnPc4d1	politický
oponenty	oponent	k1gMnPc4	oponent
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
měly	mít	k5eAaImAgFnP	mít
dorazit	dorazit	k5eAaPmF	dorazit
síly	síla	k1gFnPc4	síla
Osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pobaltí	Pobaltí	k1gNnSc6	Pobaltí
<g/>
,	,	kIx,	,
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
a	a	k8xC	a
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
NKVD	NKVD	kA	NKVD
a	a	k8xC	a
armáda	armáda	k1gFnSc1	armáda
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
války	válka	k1gFnSc2	válka
na	na	k7c6	na
ústupu	ústup	k1gInSc6	ústup
také	také	k6eAd1	také
místy	místo	k1gNnPc7	místo
zcela	zcela	k6eAd1	zcela
systematicky	systematicky	k6eAd1	systematicky
vraždila	vraždit	k5eAaImAgFnS	vraždit
kvalifikované	kvalifikovaný	k2eAgFnPc4d1	kvalifikovaná
pracovní	pracovní	k2eAgFnPc4d1	pracovní
síly	síla	k1gFnPc4	síla
(	(	kIx(	(
<g/>
především	především	k9	především
dělníky	dělník	k1gMnPc4	dělník
<g/>
,	,	kIx,	,
řemeslníky	řemeslník	k1gMnPc4	řemeslník
<g/>
,	,	kIx,	,
lékaře	lékař	k1gMnPc4	lékař
a	a	k8xC	a
ošetřovatelky	ošetřovatelka	k1gFnPc4	ošetřovatelka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnSc4	on
Němci	Němec	k1gMnPc1	Němec
nemohli	moct	k5eNaImAgMnP	moct
využít	využít	k5eAaPmF	využít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
1943	[number]	k4	1943
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
====	====	k?	====
</s>
</p>
<p>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Stalingradu	Stalingrad	k1gInSc2	Stalingrad
<g/>
,	,	kIx,	,
obratu	obrat	k1gInSc2	obrat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
získávat	získávat	k5eAaImF	získávat
ztracená	ztracený	k2eAgNnPc1d1	ztracené
území	území	k1gNnPc1	území
na	na	k7c4	na
východní	východní	k2eAgFnSc4d1	východní
fronta	fronta	k1gFnSc1	fronta
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
akce	akce	k1gFnPc4	akce
proti	proti	k7c3	proti
jakékoli	jakýkoli	k3yIgFnSc3	jakýkoli
osobě	osoba	k1gFnSc3	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
obviněna	obvinit	k5eAaPmNgFnS	obvinit
z	z	k7c2	z
kolaborace	kolaborace	k1gFnSc2	kolaborace
během	během	k7c2	během
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Civilisté	civilista	k1gMnPc1	civilista
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
sovětské	sovětský	k2eAgFnSc3d1	sovětská
vládě	vláda	k1gFnSc3	vláda
oponovali	oponovat	k5eAaImAgMnP	oponovat
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
kolaboranty	kolaborant	k1gMnPc4	kolaborant
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
například	například	k6eAd1	například
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
historie	historie	k1gFnSc2	historie
celkově	celkově	k6eAd1	celkově
dobře	dobře	k6eAd1	dobře
zdokumentovaná	zdokumentovaný	k2eAgFnSc1d1	zdokumentovaná
<g/>
,	,	kIx,	,
diskutovaná	diskutovaný	k2eAgFnSc1d1	diskutovaná
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
akademických	akademický	k2eAgInPc2d1	akademický
průzkumů	průzkum	k1gInPc2	průzkum
<g/>
,	,	kIx,	,
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
dělo	dít	k5eAaImAgNnS	dít
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Kalmycko	Kalmycko	k1gNnSc1	Kalmycko
====	====	k?	====
</s>
</p>
<p>
<s>
Během	během	k7c2	během
deportací	deportace	k1gFnPc2	deportace
Kalmyků	Kalmyk	k1gMnPc2	Kalmyk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
kódové	kódový	k2eAgNnSc1d1	kódové
jméno	jméno	k1gNnSc1	jméno
Operace	operace	k1gFnSc2	operace
Ulussy	Ulussa	k1gFnSc2	Ulussa
(	(	kIx(	(
<g/>
О	О	k?	О
"	"	kIx"	"
<g/>
У	У	k?	У
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
čistkám	čistka	k1gFnPc3	čistka
a	a	k8xC	a
deportacími	deportací	k2eAgFnPc7d1	deportací
většiny	většina	k1gFnSc2	většina
lidí	člověk	k1gMnPc2	člověk
kalmycké	kalmycký	k2eAgFnSc2d1	Kalmycká
národnosti	národnost	k1gFnSc2	národnost
v	v	k7c6	v
(	(	kIx(	(
<g/>
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
a	a	k8xC	a
ruských	ruský	k2eAgFnPc2d1	ruská
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
provdaných	provdaný	k2eAgInPc2d1	provdaný
za	za	k7c4	za
Kalmyky	Kalmyk	k1gMnPc4	Kalmyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
kalmyckých	kalmycký	k2eAgFnPc2d1	Kalmycká
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
provdaných	provdaný	k2eAgInPc2d1	provdaný
za	za	k7c4	za
muže	muž	k1gMnPc4	muž
jiné	jiný	k2eAgFnSc2d1	jiná
národnosti	národnost	k1gFnSc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
polovina	polovina	k1gFnSc1	polovina
(	(	kIx(	(
<g/>
97	[number]	k4	97
<g/>
–	–	k?	–
<g/>
98	[number]	k4	98
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
Kalmyků	Kalmyk	k1gMnPc2	Kalmyk
vyhoštěných	vyhoštěný	k2eAgMnPc2d1	vyhoštěný
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
jim	on	k3xPp3gMnPc3	on
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
povolen	povolit	k5eAaPmNgInS	povolit
návrat	návrat	k1gInSc1	návrat
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Německo	Německo	k1gNnSc1	Německo
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
historika	historik	k1gMnSc2	historik
Normana	Norman	k1gMnSc2	Norman
Naimarka	Naimarka	k1gFnSc1	Naimarka
nesou	nést	k5eAaImIp3nP	nést
prohlášení	prohlášení	k1gNnPc1	prohlášení
uvedená	uvedený	k2eAgNnPc1d1	uvedené
v	v	k7c6	v
sovětských	sovětský	k2eAgFnPc6d1	sovětská
vojenských	vojenský	k2eAgFnPc6d1	vojenská
novinách	novina	k1gFnPc6	novina
a	a	k8xC	a
rozkazy	rozkaz	k1gInPc7	rozkaz
sovětského	sovětský	k2eAgNnSc2d1	sovětské
vrchního	vrchní	k2eAgNnSc2d1	vrchní
velení	velení	k1gNnSc2	velení
společnou	společný	k2eAgFnSc4d1	společná
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c4	za
excesy	exces	k1gInPc4	exces
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Propaganda	propaganda	k1gFnSc1	propaganda
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
Německo	Německo	k1gNnSc1	Německo
jako	jako	k8xC	jako
mstitel	mstitel	k1gMnSc1	mstitel
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
všechny	všechen	k3xTgMnPc4	všechen
Němce	Němec	k1gMnPc4	Němec
potrestat	potrestat	k5eAaPmF	potrestat
<g/>
.	.	kIx.	.
<g/>
Někteří	některý	k3yIgMnPc1	některý
historici	historik	k1gMnPc1	historik
se	se	k3xPyFc4	se
o	o	k7c4	o
toto	tento	k3xDgNnSc4	tento
přou	přít	k5eAaImIp3nP	přít
a	a	k8xC	a
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
na	na	k7c4	na
nařízení	nařízení	k1gNnSc4	nařízení
vydané	vydaný	k2eAgNnSc4d1	vydané
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyžadovalo	vyžadovat	k5eAaImAgNnS	vyžadovat
prevenci	prevence	k1gFnSc4	prevence
civilisty	civilista	k1gMnSc2	civilista
netýrat	týrat	k5eNaImF	týrat
<g/>
.	.	kIx.	.
</s>
<s>
Rozkaz	rozkaz	k1gInSc1	rozkaz
vojenské	vojenský	k2eAgFnSc2d1	vojenská
rady	rada	k1gFnSc2	rada
1	[number]	k4	1
<g/>
.	.	kIx.	.
běloruského	běloruský	k2eAgInSc2d1	běloruský
frontu	front	k1gInSc2	front
podepsáný	podepsáný	k2eAgInSc1d1	podepsáný
maršálem	maršál	k1gMnSc7	maršál
Rokossovským	Rokossovský	k2eAgFnPc3d1	Rokossovský
nařizoval	nařizovat	k5eAaImAgMnS	nařizovat
střílení	střílení	k1gNnPc4	střílení
lupičů	lupič	k1gMnPc2	lupič
a	a	k8xC	a
násilníků	násilník	k1gMnPc2	násilník
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
činu	čin	k1gInSc2	čin
<g/>
.	.	kIx.	.
</s>
<s>
Rozkaz	rozkaz	k1gInSc1	rozkaz
vydaný	vydaný	k2eAgInSc1d1	vydaný
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
Stavkou	Stavka	k1gMnSc7	Stavka
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
s	s	k7c7	s
německými	německý	k2eAgMnPc7d1	německý
civilisty	civilista	k1gMnPc7	civilista
udržovat	udržovat	k5eAaImF	udržovat
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
snížil	snížit	k5eAaPmAgInS	snížit
odpor	odpor	k1gInSc1	odpor
a	a	k8xC	a
přišlo	přijít	k5eAaPmAgNnS	přijít
rychlejší	rychlý	k2eAgNnSc1d2	rychlejší
zastavení	zastavení	k1gNnSc1	zastavení
nepřátelských	přátelský	k2eNgFnPc2d1	nepřátelská
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vraždy	vražda	k1gFnPc1	vražda
civilistů	civilista	k1gMnPc2	civilista
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
příležitostech	příležitost	k1gFnPc6	příležitost
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
sovětští	sovětský	k2eAgMnPc1d1	sovětský
vojáci	voják	k1gMnPc1	voják
zapálili	zapálit	k5eAaPmAgMnP	zapálit
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
vesnice	vesnice	k1gFnPc4	vesnice
nebo	nebo	k8xC	nebo
částí	část	k1gFnPc2	část
měst	město	k1gNnPc2	město
a	a	k8xC	a
proti	proti	k7c3	proti
místní	místní	k2eAgFnSc1d1	místní
obyvatelům	obyvatel	k1gMnPc3	obyvatel
<g/>
,	,	kIx,	,
snažícím	snažící	k2eAgMnSc7d1	snažící
se	se	k3xPyFc4	se
uhasit	uhasit	k5eAaPmF	uhasit
oheň	oheň	k1gInSc1	oheň
<g/>
,	,	kIx,	,
použili	použít	k5eAaPmAgMnP	použít
smrtící	smrtící	k2eAgFnSc4d1	smrtící
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
zvěrstev	zvěrstvo	k1gNnPc2	zvěrstvo
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
děla	dít	k5eAaImAgFnS	dít
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
územích	území	k1gNnPc6	území
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
byla	být	k5eAaImAgNnP	být
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
nepřátelská	přátelský	k2eNgNnPc4d1	nepřátelské
území	území	k1gNnPc4	území
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
masakr	masakr	k1gInSc4	masakr
v	v	k7c6	v
Przyszowicích	Przyszowice	k1gFnPc6	Przyszowice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
členy	člen	k1gInPc7	člen
NKVD	NKVD	kA	NKVD
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1944	[number]	k4	1944
a	a	k8xC	a
1945	[number]	k4	1945
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
často	často	k6eAd1	často
rabovali	rabovat	k5eAaImAgMnP	rabovat
německé	německý	k2eAgInPc4d1	německý
transportní	transportní	k2eAgInPc4d1	transportní
vlaky	vlak	k1gInPc4	vlak
<g/>
.	.	kIx.	.
<g/>
Evakuace	evakuace	k1gFnSc1	evakuace
německých	německý	k2eAgMnPc2d1	německý
civilistů	civilista	k1gMnPc2	civilista
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
prchali	prchat	k5eAaImAgMnP	prchat
před	před	k7c7	před
postupující	postupující	k2eAgFnSc7d1	postupující
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nacistickou	nacistický	k2eAgFnSc7d1	nacistická
vládou	vláda	k1gFnSc7	vláda
opožděna	opozdit	k5eAaPmNgFnS	opozdit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
demoralizaci	demoralizace	k1gFnSc3	demoralizace
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nyní	nyní	k6eAd1	nyní
bránila	bránit	k5eAaImAgFnS	bránit
vlastní	vlastní	k2eAgNnSc4d1	vlastní
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
němečtí	německý	k2eAgMnPc1d1	německý
civilisté	civilista	k1gMnPc1	civilista
si	se	k3xPyFc3	se
byli	být	k5eAaImAgMnP	být
dobře	dobře	k6eAd1	dobře
vědomi	vědom	k2eAgMnPc1d1	vědom
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
civilisty	civilista	k1gMnPc4	civilista
díky	díky	k7c3	díky
zprávám	zpráva	k1gFnPc3	zpráva
svých	svůj	k3xOyFgMnPc2	svůj
přátel	přítel	k1gMnPc2	přítel
a	a	k8xC	a
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
sloužili	sloužit	k5eAaImAgMnP	sloužit
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Nacistická	nacistický	k2eAgFnSc1d1	nacistická
propaganda	propaganda	k1gFnSc1	propaganda
chtěla	chtít	k5eAaImAgFnS	chtít
původně	původně	k6eAd1	původně
utužit	utužit	k5eAaPmF	utužit
civilní	civilní	k2eAgInSc4d1	civilní
odpor	odpor	k1gInSc4	odpor
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvěrstva	zvěrstvo	k1gNnPc1	zvěrstvo
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
detailně	detailně	k6eAd1	detailně
popisovala	popisovat	k5eAaImAgFnS	popisovat
a	a	k8xC	a
vybarvovala	vybarvovat	k5eAaImAgFnS	vybarvovat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
masakr	masakr	k1gInSc4	masakr
v	v	k7c6	v
Nemmersdorfu	Nemmersdorf	k1gInSc6	Nemmersdorf
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ale	ale	k8xC	ale
často	často	k6eAd1	často
selhalo	selhat	k5eAaPmAgNnS	selhat
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
panika	panika	k1gFnSc1	panika
<g/>
.	.	kIx.	.
</s>
<s>
Kdykoliv	kdykoliv	k6eAd1	kdykoliv
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
a	a	k8xC	a
jakmile	jakmile	k8xS	jakmile
Wehrmacht	wehrmacht	k1gInSc1	wehrmacht
ustoupil	ustoupit	k5eAaPmAgInS	ustoupit
<g/>
,	,	kIx,	,
místní	místní	k2eAgMnPc1d1	místní
civilisté	civilista	k1gMnPc1	civilista
začali	začít	k5eAaPmAgMnP	začít
na	na	k7c4	na
západ	západ	k1gInSc4	západ
prchat	prchat	k5eAaImF	prchat
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
iniciativy	iniciativa	k1gFnSc2	iniciativa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
prchajících	prchající	k2eAgMnPc2d1	prchající
obyvatel	obyvatel	k1gMnPc2	obyvatel
před	před	k7c7	před
postupující	postupující	k2eAgFnSc7d1	postupující
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
z	z	k7c2	z
německých	německý	k2eAgFnPc2d1	německá
provincií	provincie	k1gFnPc2	provincie
východní	východní	k2eAgNnSc1d1	východní
Prusko	Prusko	k1gNnSc1	Prusko
<g/>
,	,	kIx,	,
Slezsko	Slezsko	k1gNnSc1	Slezsko
a	a	k8xC	a
Pomořansko	Pomořansko	k1gNnSc1	Pomořansko
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
evakuace	evakuace	k1gFnSc2	evakuace
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
chladem	chlad	k1gInSc7	chlad
a	a	k8xC	a
hladem	hlad	k1gInSc7	hlad
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
během	během	k7c2	během
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
procento	procento	k1gNnSc1	procento
tohoto	tento	k3xDgInSc2	tento
počtu	počet	k1gInSc2	počet
obětí	oběť	k1gFnPc2	oběť
však	však	k9	však
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
při	při	k7c6	při
střetu	střet	k1gInSc6	střet
s	s	k7c7	s
jednotkami	jednotka	k1gFnPc7	jednotka
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Civilisté	civilista	k1gMnPc1	civilista
byli	být	k5eAaImAgMnP	být
přejížděni	přejíždět	k5eAaImNgMnP	přejíždět
tanky	tank	k1gInPc7	tank
<g/>
,	,	kIx,	,
stříleni	střílen	k2eAgMnPc1d1	střílen
nebo	nebo	k8xC	nebo
jinak	jinak	k6eAd1	jinak
vražděni	vražděn	k2eAgMnPc1d1	vražděn
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
a	a	k8xC	a
mladé	mladý	k2eAgFnPc1d1	mladá
dívky	dívka	k1gFnPc1	dívka
byly	být	k5eAaImAgFnP	být
znásilněny	znásilnit	k5eAaPmNgFnP	znásilnit
a	a	k8xC	a
zabity	zabít	k5eAaPmNgFnP	zabít
<g/>
.	.	kIx.	.
<g/>
Sovětské	sovětský	k2eAgInPc4d1	sovětský
bombardéry	bombardér	k1gInPc4	bombardér
navíc	navíc	k6eAd1	navíc
vyhledávaly	vyhledávat	k5eAaImAgFnP	vyhledávat
kolony	kolona	k1gFnPc1	kolona
uprchlíků	uprchlík	k1gMnPc2	uprchlík
a	a	k8xC	a
intenzivně	intenzivně	k6eAd1	intenzivně
je	být	k5eAaImIp3nS	být
bombardovaly	bombardovat	k5eAaImAgInP	bombardovat
<g/>
.	.	kIx.	.
<g/>
Ačkoli	ačkoli	k8xS	ačkoli
hromadné	hromadný	k2eAgFnSc2d1	hromadná
popravy	poprava	k1gFnSc2	poprava
civilistů	civilista	k1gMnPc2	civilista
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
veřejně	veřejně	k6eAd1	veřejně
oznamovala	oznamovat	k5eAaImAgFnS	oznamovat
<g/>
,	,	kIx,	,
vešel	vejít	k5eAaPmAgInS	vejít
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
incident	incident	k1gInSc4	incident
v	v	k7c4	v
Treuenbrietzenu	Treuenbrietzen	k2eAgFnSc4d1	Treuenbrietzen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
nejméně	málo	k6eAd3	málo
88	[number]	k4	88
mužů	muž	k1gMnPc2	muž
obklíčeno	obklíčit	k5eAaPmNgNnS	obklíčit
a	a	k8xC	a
postříleno	postřílen	k2eAgNnSc1d1	postříleno
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
incidentu	incident	k1gInSc3	incident
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
oslavách	oslava	k1gFnPc6	oslava
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
dívek	dívka	k1gFnPc2	dívka
z	z	k7c2	z
Treuenbrietzenu	Treuenbrietzen	k2eAgFnSc4d1	Treuenbrietzen
znásilněno	znásilněn	k2eAgNnSc1d1	znásilněno
a	a	k8xC	a
sovětský	sovětský	k2eAgMnSc1d1	sovětský
podplukovník	podplukovník	k1gMnSc1	podplukovník
byl	být	k5eAaImAgMnS	být
neznámým	známý	k2eNgMnSc7d1	neznámý
útočníkem	útočník	k1gMnSc7	útočník
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
incidentu	incident	k1gInSc2	incident
padlo	padnout	k5eAaImAgNnS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
1	[number]	k4	1
000	[number]	k4	000
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
<g/>
Walter	Walter	k1gMnSc1	Walter
Kilian	Kilian	k1gMnSc1	Kilian
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
poválečný	poválečný	k2eAgMnSc1d1	poválečný
starosta	starosta	k1gMnSc1	starosta
berlínské	berlínský	k2eAgFnSc2d1	Berlínská
čtvrti	čtvrt	k1gFnSc2	čtvrt
Charlottenburg	Charlottenburg	k1gMnSc1	Charlottenburg
jmenovaný	jmenovaný	k1gMnSc1	jmenovaný
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
Sověty	Sověty	k1gInPc4	Sověty
<g/>
,	,	kIx,	,
hlásil	hlásit	k5eAaImAgMnS	hlásit
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
plenění	plenění	k1gNnSc4	plenění
vojáky	voják	k1gMnPc4	voják
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgMnPc1d1	obchodní
<g />
.	.	kIx.	.
</s>
<s>
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
obchody	obchod	k1gInPc1	obchod
<g/>
,	,	kIx,	,
byty	byt	k1gInPc1	byt
<g/>
...	...	k?	...
<g/>
všechno	všechen	k3xTgNnSc1	všechen
bylo	být	k5eAaImAgNnS	být
vykradeno	vykraden	k2eAgNnSc1d1	vykradeno
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Členové	člen	k1gMnPc1	člen
SED	sed	k1gInSc4	sed
ze	z	k7c2	z
sovětské	sovětský	k2eAgFnSc2d1	sovětská
okupační	okupační	k2eAgFnSc2d1	okupační
zóny	zóna	k1gFnSc2	zóna
Stalinovi	Stalin	k1gMnSc3	Stalin
hlásili	hlásit	k5eAaImAgMnP	hlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
plenění	plenění	k1gNnSc1	plenění
a	a	k8xC	a
znásilňování	znásilňování	k1gNnSc1	znásilňování
sovětskými	sovětský	k2eAgInPc7d1	sovětský
vojáky	voják	k1gMnPc4	voják
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
mít	mít	k5eAaImF	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
negativní	negativní	k2eAgFnSc4d1	negativní
reakci	reakce	k1gFnSc4	reakce
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
vůči	vůči	k7c3	vůči
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
budoucnost	budoucnost	k1gFnSc4	budoucnost
socialismu	socialismus	k1gInSc2	socialismus
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
zlostně	zlostně	k6eAd1	zlostně
reagoval	reagovat	k5eAaBmAgMnS	reagovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nebudu	být	k5eNaImBp1nS	být
tolerovat	tolerovat	k5eAaImF	tolerovat
nikoho	nikdo	k3yNnSc4	nikdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
vláčí	vláčet	k5eAaImIp3nS	vláčet
čest	čest	k1gFnSc4	čest
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
bahnem	bahno	k1gNnSc7	bahno
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgInPc1	všechen
důkazy	důkaz	k1gInPc1	důkaz
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
fotografie	fotografia	k1gFnPc1	fotografia
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
dokumenty	dokument	k1gInPc1	dokument
o	o	k7c4	o
rabování	rabování	k1gNnSc4	rabování
<g/>
,	,	kIx,	,
znásilňování	znásilňování	k1gNnSc1	znásilňování
<g/>
,	,	kIx,	,
vypalování	vypalování	k1gNnSc1	vypalování
farem	farma	k1gFnPc2	farma
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
vesnic	vesnice	k1gFnPc2	vesnice
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
archivů	archiv	k1gInPc2	archiv
budoucí	budoucí	k2eAgFnSc2d1	budoucí
NDR	NDR	kA	NDR
odstraněny	odstraněn	k2eAgInPc1d1	odstraněn
<g/>
.	.	kIx.	.
<g/>
Studie	studie	k1gFnSc1	studie
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
německou	německý	k2eAgFnSc7d1	německá
vládou	vláda	k1gFnSc7	vláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnSc7	oběť
německých	německý	k2eAgMnPc2d1	německý
civilistů	civilista	k1gMnPc2	civilista
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
na	na	k7c4	na
635	[number]	k4	635
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
270	[number]	k4	270
000	[number]	k4	000
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
sovětských	sovětský	k2eAgInPc2d1	sovětský
válečných	válečný	k2eAgInPc2d1	válečný
zločinů	zločin	k1gInPc2	zločin
<g/>
,	,	kIx,	,
160	[number]	k4	160
000	[number]	k4	000
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
různých	různý	k2eAgFnPc2d1	různá
národností	národnost	k1gFnPc2	národnost
během	během	k7c2	během
vysidlování	vysidlování	k1gNnSc2	vysidlování
Němců	Němec	k1gMnPc2	Němec
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
205	[number]	k4	205
000	[number]	k4	000
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
na	na	k7c6	na
nucených	nucený	k2eAgFnPc6d1	nucená
pracích	práce	k1gFnPc6	práce
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
údaje	údaj	k1gInPc1	údaj
minimálně	minimálně	k6eAd1	minimálně
nezahrnují	zahrnovat	k5eNaImIp3nP	zahrnovat
125	[number]	k4	125
000	[number]	k4	000
civilních	civilní	k2eAgFnPc2d1	civilní
obětí	oběť	k1gFnPc2	oběť
během	během	k7c2	během
bitvy	bitva	k1gFnSc2	bitva
o	o	k7c4	o
Berlín	Berlín	k1gInSc4	Berlín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Masové	masový	k2eAgNnSc1d1	masové
znásilňování	znásilňování	k1gNnSc1	znásilňování
====	====	k?	====
</s>
</p>
<p>
<s>
Západní	západní	k2eAgInPc1d1	západní
odhady	odhad	k1gInPc1	odhad
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
obětí	oběť	k1gFnPc2	oběť
znásilnění	znásilnění	k1gNnSc2	znásilnění
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
až	až	k9	až
po	po	k7c4	po
dva	dva	k4xCgInPc4	dva
miliony	milion	k4xCgInPc4	milion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
viselsko-oderskou	viselskoderský	k2eAgFnSc4d1	viselsko-oderský
operaci	operace	k1gFnSc4	operace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
masovému	masový	k2eAgNnSc3d1	masové
znásilňování	znásilňování	k1gNnSc3	znásilňování
Sověty	Sověty	k1gInPc1	Sověty
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
větších	veliký	k2eAgNnPc6d2	veliký
městech	město	k1gNnPc6	město
dobytých	dobytý	k2eAgFnPc2d1	dobytá
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
byly	být	k5eAaImAgFnP	být
masově	masově	k6eAd1	masově
znásilňovány	znásilňován	k2eAgInPc1d1	znásilňován
gangy	gang	k1gInPc1	gang
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
vojáků	voják	k1gMnPc2	voják
při	při	k7c6	při
osvobozování	osvobozování	k1gNnSc6	osvobozování
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
byly	být	k5eAaImAgFnP	být
oběti	oběť	k1gFnPc1	oběť
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc1d1	celý
den	den	k1gInSc1	den
neskrývaly	skrývat	k5eNaImAgInP	skrývat
ve	v	k7c6	v
sklepích	sklep	k1gInPc6	sklep
<g/>
,	,	kIx,	,
znásilněny	znásilněn	k2eAgInPc1d1	znásilněn
až	až	k9	až
15	[number]	k4	15
<g/>
krát	krát	k6eAd1	krát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Antony	Anton	k1gMnPc4	Anton
Beevora	Beevora	k1gFnSc1	Beevora
sovětští	sovětský	k2eAgMnPc1d1	sovětský
vojáci	voják	k1gMnPc1	voják
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
Berlína	Berlín	k1gInSc2	Berlín
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
znásilňovali	znásilňovat	k5eAaImAgMnP	znásilňovat
i	i	k9	i
dívky	dívka	k1gFnPc4	dívka
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
osmi	osm	k4xCc2	osm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
desetiletí	desetiletí	k1gNnPc4	desetiletí
západní	západní	k2eAgFnSc2d1	západní
vědci	vědec	k1gMnSc6	vědec
tato	tento	k3xDgNnPc1	tento
zvěrstva	zvěrstvo	k1gNnPc1	zvěrstvo
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
obecně	obecně	k6eAd1	obecně
omlouvali	omlouvat	k5eAaImAgMnP	omlouvat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
pomstu	pomsta	k1gFnSc4	pomsta
za	za	k7c4	za
německé	německý	k2eAgFnPc4d1	německá
krutostí	krutost	k1gFnSc7	krutost
na	na	k7c4	na
území	území	k1gNnSc4	území
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
za	za	k7c2	za
války	válka	k1gFnSc2	válka
přes	přes	k7c4	přes
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
masové	masový	k2eAgNnSc1d1	masové
zabíjení	zabíjení	k1gNnSc1	zabíjení
sovětských	sovětský	k2eAgMnPc2d1	sovětský
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
(	(	kIx(	(
<g/>
3,6	[number]	k4	3,6
milionů	milion	k4xCgInPc2	milion
mrtvých	mrtvý	k1gMnPc2	mrtvý
z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
5,2	[number]	k4	5,2
milionu	milion	k4xCgInSc2	milion
válečných	válečná	k1gFnPc2	válečná
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
)	)	kIx)	)
zajatých	zajatý	k2eAgFnPc2d1	zajatá
německými	německý	k2eAgFnPc7d1	německá
ozbrojenými	ozbrojený	k2eAgFnPc7d1	ozbrojená
silami	síla	k1gFnPc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
vojenskými	vojenský	k2eAgMnPc7d1	vojenský
historiky	historik	k1gMnPc7	historik
jako	jako	k8xC	jako
Antony	Anton	k1gMnPc7	Anton
Beevor	Beevora	k1gFnPc2	Beevora
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
alespoň	alespoň	k9	alespoň
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
masové	masový	k2eAgNnSc4d1	masové
znásilňování	znásilňování	k1gNnSc4	znásilňování
<g/>
.	.	kIx.	.
</s>
<s>
Beevor	Beevor	k1gMnSc1	Beevor
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vojáci	voják	k1gMnPc1	voják
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
znásilňovali	znásilňovat	k5eAaImAgMnP	znásilňovat
také	také	k6eAd1	také
sovětské	sovětský	k2eAgFnPc4d1	sovětská
a	a	k8xC	a
polské	polský	k2eAgFnPc4d1	polská
ženy	žena	k1gFnPc4	žena
propuštěné	propuštěný	k2eAgFnPc4d1	propuštěná
z	z	k7c2	z
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
a	a	k8xC	a
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
pomstu	pomsta	k1gFnSc4	pomsta
podrývá	podrývat	k5eAaImIp3nS	podrývat
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
Normana	Norman	k1gMnSc2	Norman
Naimarka	Naimarka	k1gFnSc1	Naimarka
byli	být	k5eAaImAgMnP	být
po	po	k7c6	po
létě	léto	k1gNnSc6	léto
1945	[number]	k4	1945
sovětští	sovětský	k2eAgMnPc1d1	sovětský
vojáci	voják	k1gMnPc1	voják
přistižení	přistižený	k2eAgMnPc1d1	přistižený
při	při	k7c6	při
znásilňování	znásilňování	k1gNnSc6	znásilňování
civilistů	civilista	k1gMnPc2	civilista
obvykle	obvykle	k6eAd1	obvykle
potrestáni	potrestat	k5eAaPmNgMnP	potrestat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
zatčení	zatčení	k1gNnSc2	zatčení
po	po	k7c4	po
popravu	poprava	k1gFnSc4	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
dále	daleko	k6eAd2	daleko
Naimark	Naimark	k1gInSc1	Naimark
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
znásilňování	znásilňování	k1gNnSc1	znásilňování
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
až	až	k9	až
do	do	k7c2	do
zimy	zima	k1gFnSc2	zima
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
48	[number]	k4	48
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
sovětské	sovětský	k2eAgFnPc1d1	sovětská
okupační	okupační	k2eAgFnPc1d1	okupační
síly	síla	k1gFnPc1	síla
nakonec	nakonec	k6eAd1	nakonec
vojáky	voják	k1gMnPc7	voják
omezily	omezit	k5eAaPmAgFnP	omezit
na	na	k7c4	na
přísně	přísně	k6eAd1	přísně
střežené	střežený	k2eAgInPc4d1	střežený
posty	post	k1gInPc4	post
a	a	k8xC	a
tábory	tábor	k1gInPc4	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Naimark	Naimark	k1gInSc1	Naimark
dospěl	dochvít	k5eAaPmAgInS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
sociální	sociální	k2eAgFnSc1d1	sociální
psychologie	psychologie	k1gFnSc1	psychologie
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
sovětské	sovětský	k2eAgFnSc6d1	sovětská
okupační	okupační	k2eAgFnSc6d1	okupační
zóně	zóna	k1gFnSc6	zóna
byla	být	k5eAaImAgFnS	být
poznamenána	poznamenat	k5eAaPmNgFnS	poznamenat
zločiny	zločin	k1gInPc4	zločin
znásilnění	znásilnění	k1gNnSc2	znásilnění
od	od	k7c2	od
prvních	první	k4xOgInPc2	první
dnů	den	k1gInPc2	den
okupace	okupace	k1gFnSc2	okupace
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
založení	založení	k1gNnSc4	založení
NDR	NDR	kA	NDR
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c6	po
<g/>
,	,	kIx,	,
dalo	dát	k5eAaPmAgNnS	dát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
současnost	současnost	k1gFnSc4	současnost
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
Beevora	Beevor	k1gMnSc2	Beevor
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
znásilněno	znásilněn	k2eAgNnSc1d1	znásilněno
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
10	[number]	k4	10
tisíc	tisíc	k4xCgInSc4	tisíc
spáchalo	spáchat	k5eAaPmAgNnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
nebo	nebo	k8xC	nebo
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
znásilnění	znásilnění	k1gNnSc2	znásilnění
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
Richard	Richard	k1gMnSc1	Richard
Overyho	Overy	k1gMnSc2	Overy
<g/>
,	,	kIx,	,
Rusové	Rus	k1gMnPc1	Rus
neuznali	uznat	k5eNaPmAgMnP	uznat
sovětské	sovětský	k2eAgInPc4d1	sovětský
válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
"	"	kIx"	"
<g/>
protože	protože	k8xS	protože
cítili	cítit	k5eAaImAgMnP	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
oprávněnou	oprávněný	k2eAgFnSc7d1	oprávněná
pomstou	pomsta	k1gFnSc7	pomsta
proti	proti	k7c3	proti
nepříteli	nepřítel	k1gMnSc3	nepřítel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
prý	prý	k9	prý
dopustil	dopustit	k5eAaPmAgInS	dopustit
mnohem	mnohem	k6eAd1	mnohem
horších	zlý	k2eAgInPc2d2	horší
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
historii	historie	k1gFnSc4	historie
píší	psát	k5eAaImIp3nP	psát
vítězové	vítěz	k1gMnPc1	vítěz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
badatele	badatel	k1gMnSc2	badatel
a	a	k8xC	a
autora	autor	k1gMnSc2	autor
Krisztiána	Krisztián	k1gMnSc2	Krisztián
Ungváryho	Ungváry	k1gMnSc2	Ungváry
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
během	během	k7c2	během
obležení	obležení	k1gNnSc2	obležení
Budapešti	Budapešť	k1gFnSc2	Budapešť
asi	asi	k9	asi
38	[number]	k4	38
000	[number]	k4	000
civilistů	civilista	k1gMnPc2	civilista
<g/>
:	:	kIx,	:
asi	asi	k9	asi
13	[number]	k4	13
000	[number]	k4	000
vojenskými	vojenský	k2eAgFnPc7d1	vojenská
akcemi	akce	k1gFnPc7	akce
a	a	k8xC	a
25	[number]	k4	25
000	[number]	k4	000
z	z	k7c2	z
hladu	hlad	k1gInSc2	hlad
<g/>
,	,	kIx,	,
nemocí	nemoc	k1gFnPc2	nemoc
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
příčin	příčina	k1gFnPc2	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
údaje	údaj	k1gInPc1	údaj
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
asi	asi	k9	asi
15	[number]	k4	15
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
obětí	oběť	k1gFnSc7	oběť
poprav	poprava	k1gFnPc2	poprava
nacistické	nacistický	k2eAgFnSc2d1	nacistická
SS	SS	kA	SS
a	a	k8xC	a
eskader	eskadra	k1gFnPc2	eskadra
smrti	smrt	k1gFnSc2	smrt
strany	strana	k1gFnSc2	strana
šípových	šípový	k2eAgInPc2d1	šípový
křížů	kříž	k1gInPc2	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Ungváry	Ungvár	k1gInPc4	Ungvár
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
Sověti	Sovět	k1gMnPc1	Sovět
nakonec	nakonec	k6eAd1	nakonec
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
,	,	kIx,	,
zahájili	zahájit	k5eAaPmAgMnP	zahájit
poté	poté	k6eAd1	poté
orgie	orgie	k1gFnPc4	orgie
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
ohromných	ohromný	k2eAgFnPc2d1	ohromná
krádeži	krádež	k1gFnSc6	krádež
čehokoli	cokoli	k3yInSc2	cokoli
na	na	k7c4	na
co	co	k3yRnSc4	co
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
sáhnout	sáhnout	k5eAaPmF	sáhnout
<g/>
,	,	kIx,	,
náhodných	náhodný	k2eAgFnPc2d1	náhodná
poprav	poprava	k1gFnPc2	poprava
a	a	k8xC	a
masového	masový	k2eAgNnSc2d1	masové
znásilňování	znásilňování	k1gNnSc2	znásilňování
<g/>
.	.	kIx.	.
</s>
<s>
Odhady	odhad	k1gInPc1	odhad
obětí	oběť	k1gFnPc2	oběť
znásilnění	znásilnění	k1gNnSc2	znásilnění
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nS	různit
od	od	k7c2	od
5	[number]	k4	5
000	[number]	k4	000
do	do	k7c2	do
200	[number]	k4	200
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Normana	Norman	k1gMnSc2	Norman
Naimarka	Naimarka	k1gFnSc1	Naimarka
byly	být	k5eAaImAgFnP	být
maďarské	maďarský	k2eAgFnPc1d1	maďarská
dívky	dívka	k1gFnPc1	dívka
unášeny	unášen	k2eAgFnPc1d1	unášena
do	do	k7c2	do
čtvrtí	čtvrt	k1gFnPc2	čtvrt
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
vězněny	vězněn	k2eAgFnPc1d1	vězněna
<g/>
,	,	kIx,	,
opakovaně	opakovaně	k6eAd1	opakovaně
znásilněny	znásilněn	k2eAgFnPc1d1	znásilněna
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
zavražděny	zavražděn	k2eAgMnPc4d1	zavražděn
<g/>
.	.	kIx.	.
<g/>
Dokonce	dokonce	k9	dokonce
byl	být	k5eAaImAgInS	být
zajat	zajmout	k5eAaPmNgInS	zajmout
a	a	k8xC	a
znásilněn	znásilnit	k5eAaPmNgInS	znásilnit
i	i	k9	i
personál	personál	k1gInSc1	personál
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
neutrálních	neutrální	k2eAgFnPc2d1	neutrální
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dokládá	dokládat	k5eAaImIp3nS	dokládat
sovětský	sovětský	k2eAgInSc4d1	sovětský
útok	útok	k1gInSc4	útok
na	na	k7c4	na
švédské	švédský	k2eAgNnSc4d1	švédské
vyslanectví	vyslanectví	k1gNnSc4	vyslanectví
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
<g/>
Zpráva	zpráva	k1gFnSc1	zpráva
švýcarského	švýcarský	k2eAgNnSc2d1	švýcarské
vyslanectví	vyslanectví	k1gNnSc2	vyslanectví
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
popisuje	popisovat	k5eAaImIp3nS	popisovat
vstup	vstup	k1gInSc4	vstup
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
jugoslávského	jugoslávský	k2eAgMnSc2d1	jugoslávský
politika	politik	k1gMnSc2	politik
Milovana	Milovan	k1gMnSc2	Milovan
Djilase	Djilasa	k1gFnSc3	Djilasa
bylo	být	k5eAaImAgNnS	být
zdokumentováno	zdokumentovat	k5eAaPmNgNnS	zdokumentovat
přinejmenším	přinejmenším	k6eAd1	přinejmenším
121	[number]	k4	121
případů	případ	k1gInPc2	případ
znásilnění	znásilnění	k1gNnPc2	znásilnění
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
111	[number]	k4	111
se	se	k3xPyFc4	se
také	také	k9	také
podílelo	podílet	k5eAaImAgNnS	podílet
na	na	k7c6	na
vraždě	vražda	k1gFnSc6	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
1204	[number]	k4	1204
případů	případ	k1gInPc2	případ
rabování	rabování	k1gNnPc2	rabování
a	a	k8xC	a
loupežných	loupežný	k2eAgNnPc2d1	loupežné
přepadení	přepadení	k1gNnPc2	přepadení
<g/>
.	.	kIx.	.
</s>
<s>
Djilas	Djilas	k1gMnSc1	Djilas
popsal	popsat	k5eAaPmAgMnS	popsat
tyto	tento	k3xDgInPc4	tento
údaje	údaj	k1gInPc4	údaj
za	za	k7c2	za
"	"	kIx"	"
<g/>
těžce	těžce	k6eAd1	těžce
nevýznamné	významný	k2eNgFnPc1d1	nevýznamná
<g/>
,	,	kIx,	,
máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
na	na	k7c6	na
paměti	paměť	k1gFnSc6	paměť
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
překročila	překročit	k5eAaPmAgFnS	překročit
pouze	pouze	k6eAd1	pouze
severovýchodní	severovýchodní	k2eAgInSc4d1	severovýchodní
roh	roh	k1gInSc4	roh
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
jugoslávským	jugoslávský	k2eAgMnPc3d1	jugoslávský
komunistickým	komunistický	k2eAgMnPc3d1	komunistický
partyzánům	partyzán	k1gMnPc3	partyzán
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
starosti	starost	k1gFnPc4	starost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
obávali	obávat	k5eAaImAgMnP	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
zločinech	zločin	k1gInPc6	zločin
spáchaných	spáchaný	k2eAgInPc2d1	spáchaný
jejich	jejich	k3xOp3gMnPc7	jejich
sovětskými	sovětský	k2eAgMnPc7d1	sovětský
spojenci	spojenec	k1gMnPc7	spojenec
oslabí	oslabit	k5eAaPmIp3nS	oslabit
jejich	jejich	k3xOp3gNnSc1	jejich
postavení	postavení	k1gNnSc1	postavení
u	u	k7c2	u
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Djilas	Djilas	k1gMnSc1	Djilas
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
jugoslávský	jugoslávský	k2eAgMnSc1d1	jugoslávský
partyzánský	partyzánský	k2eAgMnSc1d1	partyzánský
vůdce	vůdce	k1gMnSc1	vůdce
Josip	Josip	k1gMnSc1	Josip
Broz	Broz	k1gMnSc1	Broz
Tito	tento	k3xDgMnPc1	tento
v	v	k7c4	v
reakci	reakce	k1gFnSc4	reakce
povolal	povolat	k5eAaPmAgInS	povolat
velitele	velitel	k1gMnSc4	velitel
sovětské	sovětský	k2eAgFnSc2d1	sovětská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
mise	mise	k1gFnSc2	mise
<g/>
,	,	kIx,	,
generála	generál	k1gMnSc2	generál
Kornějeva	Kornějev	k1gMnSc2	Kornějev
a	a	k8xC	a
formálně	formálně	k6eAd1	formálně
protestoval	protestovat	k5eAaBmAgMnS	protestovat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
pozván	pozvat	k5eAaPmNgMnS	pozvat
"	"	kIx"	"
<g/>
jako	jako	k8xC	jako
soudruh	soudruh	k1gMnSc1	soudruh
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Kornějev	Kornějev	k1gMnSc1	Kornějev
prý	prý	k9	prý
na	na	k7c4	na
"	"	kIx"	"
<g/>
takové	takový	k3xDgFnPc4	takový
narážky	narážka	k1gFnPc4	narážka
<g/>
"	"	kIx"	"
proti	proti	k7c3	proti
Rudé	rudý	k2eAgFnSc3d1	rudá
armádě	armáda	k1gFnSc3	armáda
vybouchl	vybouchnout	k5eAaPmAgInS	vybouchnout
<g/>
.	.	kIx.	.
</s>
<s>
Djilas	Djilas	k1gInSc1	Djilas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
přítomen	přítomen	k2eAgMnSc1d1	přítomen
<g/>
,	,	kIx,	,
promluvil	promluvit	k5eAaPmAgMnS	promluvit
a	a	k8xC	a
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
<g/>
,	,	kIx,	,
že	že	k8xS	že
britská	britský	k2eAgFnSc1d1	britská
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
"	"	kIx"	"
<g/>
takových	takový	k3xDgInPc2	takový
excesů	exces	k1gInPc2	exces
<g/>
"	"	kIx"	"
při	při	k7c6	při
osvobozování	osvobozování	k1gNnSc6	osvobozování
ostatních	ostatní	k2eAgFnPc2d1	ostatní
částí	část	k1gFnPc2	část
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
nedopustila	dopustit	k5eNaPmAgFnS	dopustit
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Kornějev	Kornějev	k1gMnSc4	Kornějev
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
řevem	řev	k1gInSc7	řev
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Protestuji	protestovat	k5eAaBmIp1nS	protestovat
co	co	k9	co
nejostřeji	ostro	k6eAd3	ostro
proti	proti	k7c3	proti
této	tento	k3xDgFnSc3	tento
urážce	urážka	k1gFnSc3	urážka
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
porovnávána	porovnávat	k5eAaImNgFnS	porovnávat
s	s	k7c7	s
armádami	armáda	k1gFnPc7	armáda
kapitalistických	kapitalistický	k2eAgFnPc2d1	kapitalistická
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
Kornějevem	Kornějev	k1gInSc7	Kornějev
"	"	kIx"	"
<g/>
neskončilo	skončit	k5eNaPmAgNnS	skončit
bez	bez	k7c2	bez
následků	následek	k1gInPc2	následek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Stalin	Stalin	k1gMnSc1	Stalin
osobně	osobně	k6eAd1	osobně
Djilase	Djilas	k1gInSc5	Djilas
během	během	k7c2	během
své	svůj	k3xOyFgFnSc6	svůj
příští	příští	k2eAgFnSc6d1	příští
návštěvě	návštěva	k1gFnSc6	návštěva
Kremlu	Kreml	k1gInSc2	Kreml
napadl	napadnout	k5eAaPmAgInS	napadnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
slzách	slza	k1gFnPc6	slza
Stalin	Stalin	k1gMnSc1	Stalin
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
"	"	kIx"	"
<g/>
podání	podání	k1gNnSc3	podání
jugoslávské	jugoslávský	k2eAgFnSc2d1	jugoslávská
armády	armáda	k1gFnSc2	armáda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
"	"	kIx"	"
<g/>
vyveden	vyvést	k5eAaPmNgMnS	vyvést
z	z	k7c2	z
míry	míra	k1gFnSc2	míra
hovořil	hovořit	k5eAaImAgMnS	hovořit
o	o	k7c6	o
utrpeních	utrpení	k1gNnPc6	utrpení
a	a	k8xC	a
hrůzách	hrůza	k1gFnPc6	hrůza
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
kterým	který	k3yQgInPc3	který
byla	být	k5eAaImAgFnS	být
nucena	nucen	k2eAgFnSc1d1	nucena
se	se	k3xPyFc4	se
v	v	k7c6	v
boji	boj	k1gInSc6	boj
na	na	k7c4	na
tisíce	tisíc	k4xCgInPc4	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
přes	přes	k7c4	přes
zdevastované	zdevastovaný	k2eAgFnPc4d1	zdevastovaná
země	zem	k1gFnPc4	zem
podrobit	podrobit	k5eAaPmF	podrobit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Stalin	Stalin	k1gMnSc1	Stalin
vyvrcholil	vyvrcholit	k5eAaPmAgMnS	vyvrcholit
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
A	a	k9	a
taková	takový	k3xDgFnSc1	takový
armáda	armáda	k1gFnSc1	armáda
nebyla	být	k5eNaImAgFnS	být
tak	tak	k6eAd1	tak
uražena	uražen	k2eAgFnSc1d1	uražena
nikým	nikdo	k3yNnSc7	nikdo
jiným	jiný	k2eAgMnSc7d1	jiný
než	než	k8xS	než
Djilasem	Djilas	k1gMnSc7	Djilas
<g/>
!	!	kIx.	!
</s>
<s>
Djilas	Djilas	k1gInSc1	Djilas
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yRgNnSc2	který
jsem	být	k5eAaImIp1nS	být
něco	něco	k6eAd1	něco
takového	takový	k3xDgMnSc4	takový
nečekal	čekat	k5eNaImAgMnS	čekat
<g/>
,	,	kIx,	,
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yRgNnSc2	který
jsem	být	k5eAaImIp1nS	být
tolik	tolik	k6eAd1	tolik
očekával	očekávat	k5eAaImAgMnS	očekávat
<g/>
!	!	kIx.	!
</s>
<s>
A	a	k9	a
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pro	pro	k7c4	pro
Vás	vy	k3xPp2nPc4	vy
nešetřila	šetřit	k5eNaImAgFnS	šetřit
krve	krev	k1gFnPc4	krev
<g/>
!	!	kIx.	!
</s>
<s>
Neví	vědět	k5eNaImIp3nS	vědět
snad	snad	k9	snad
Djilas	Djilas	k1gInSc1	Djilas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sám	sám	k3xTgInSc4	sám
je	být	k5eAaImIp3nS	být
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
lidské	lidský	k2eAgNnSc4d1	lidské
utrpení	utrpení	k1gNnSc4	utrpení
a	a	k8xC	a
jaká	jaký	k3yQgNnPc1	jaký
jsou	být	k5eAaImIp3nP	být
lidská	lidský	k2eAgNnPc1d1	lidské
srdce	srdce	k1gNnPc1	srdce
<g/>
?	?	kIx.	?
</s>
<s>
Nechápe	chápat	k5eNaImIp3nS	chápat
<g/>
,	,	kIx,	,
že	že	k8xS	že
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
prošel	projít	k5eAaPmAgInS	projít
tisíce	tisíc	k4xCgInPc1	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
ohně	oheň	k1gInSc2	oheň
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
pobavit	pobavit	k5eAaPmF	pobavit
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
vzít	vzít	k5eAaPmF	vzít
nějakou	nějaký	k3yIgFnSc4	nějaký
tu	tu	k6eAd1	tu
maličkost	maličkost	k1gFnSc4	maličkost
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
Podle	podle	k7c2	podle
Djilase	Djilasa	k1gFnSc3	Djilasa
sovětské	sovětský	k2eAgFnSc2d1	sovětská
odmítnutí	odmítnutí	k1gNnSc3	odmítnutí
zabývat	zabývat	k5eAaImF	zabývat
se	se	k3xPyFc4	se
válečnými	válečný	k2eAgInPc7d1	válečný
zločiny	zločin	k1gInPc7	zločin
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
rozezlila	rozezlít	k5eAaPmAgFnS	rozezlít
Titovu	Titův	k2eAgFnSc4d1	Titova
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
prospěšným	prospěšný	k2eAgInSc7d1	prospěšný
faktorem	faktor	k1gInSc7	faktor
následného	následný	k2eAgInSc2d1	následný
jugoslávského	jugoslávský	k2eAgInSc2d1	jugoslávský
odchodu	odchod	k1gInSc2	odchod
ze	z	k7c2	z
sovětského	sovětský	k2eAgInSc2d1	sovětský
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Československo	Československo	k1gNnSc1	Československo
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Slovenský	slovenský	k2eAgMnSc1d1	slovenský
komunistický	komunistický	k2eAgMnSc1d1	komunistický
vůdce	vůdce	k1gMnSc1	vůdce
Vladimír	Vladimír	k1gMnSc1	Vladimír
Clementis	Clementis	k1gFnSc4	Clementis
si	se	k3xPyFc3	se
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
maršálu	maršál	k1gMnSc3	maršál
Koněvovi	Koněva	k1gMnSc3	Koněva
na	na	k7c6	na
chování	chování	k1gNnSc6	chování
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Koněv	Konít	k5eAaPmDgInS	Konít
ho	on	k3xPp3gMnSc4	on
odbyl	odbýt	k5eAaPmAgInS	odbýt
odpovědí	odpověď	k1gFnSc7	odpověď
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
činy	čin	k1gInPc4	čin
dezertérů	dezertér	k1gMnPc2	dezertér
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Čína	Čína	k1gFnSc1	Čína
===	===	k?	===
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Japonsku	Japonsko	k1gNnSc6	Japonsko
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgMnS	zahájit
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
japonského	japonský	k2eAgInSc2d1	japonský
loutkového	loutkový	k2eAgInSc2d1	loutkový
státu	stát	k1gInSc2	stát
Mandžukuo	Mandžukuo	k1gNnSc1	Mandžukuo
(	(	kIx(	(
<g/>
Mandžusko	Mandžusko	k1gNnSc1	Mandžusko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
Sověti	Sovět	k1gMnPc1	Sovět
předložili	předložit	k5eAaPmAgMnP	předložit
požadavek	požadavek	k1gInSc4	požadavek
na	na	k7c4	na
japonské	japonský	k2eAgInPc4d1	japonský
cenné	cenný	k2eAgInPc4d1	cenný
materiály	materiál	k1gInPc4	materiál
a	a	k8xC	a
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
zařízení	zařízení	k1gNnSc4	zařízení
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
<g/>
Jistý	jistý	k2eAgMnSc1d1	jistý
cizinec	cizinec	k1gMnSc1	cizinec
byl	být	k5eAaImAgMnS	být
svědkem	svědek	k1gMnSc7	svědek
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sovětské	sovětský	k2eAgFnPc4d1	sovětská
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
umístěné	umístěný	k2eAgInPc1d1	umístěný
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
povoleno	povolen	k2eAgNnSc4d1	povoleno
od	od	k7c2	od
sovětské	sovětský	k2eAgFnSc2d1	sovětská
armády	armáda	k1gFnSc2	armáda
vkročit	vkročit	k5eAaPmF	vkročit
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
"	"	kIx"	"
<g/>
po	po	k7c4	po
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
znásilňovali	znásilňovat	k5eAaImAgMnP	znásilňovat
a	a	k8xC	a
drancovali	drancovat	k5eAaImAgMnP	drancovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
Mukdenu	Mukden	k1gInSc2	Mukden
zmizela	zmizet	k5eAaPmAgNnP	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Usvědčení	usvědčený	k2eAgMnPc1d1	usvědčený
vojáci	voják	k1gMnPc1	voják
je	on	k3xPp3gMnPc4	on
nahradili	nahradit	k5eAaPmAgMnP	nahradit
<g/>
;	;	kIx,	;
bylo	být	k5eAaImAgNnS	být
dosvědčeno	dosvědčit	k5eAaPmNgNnS	dosvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
ukradli	ukradnout	k5eAaPmAgMnP	ukradnout
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
dohledu	dohled	k1gInSc6	dohled
<g/>
,	,	kIx,	,
rozmlátili	rozmlátit	k5eAaPmAgMnP	rozmlátit
vany	vana	k1gFnPc4	vana
a	a	k8xC	a
toalety	toaleta	k1gFnPc4	toaleta
kladivy	kladivo	k1gNnPc7	kladivo
<g/>
,	,	kIx,	,
vyrvali	vyrvat	k5eAaPmAgMnP	vyrvat
elektrické	elektrický	k2eAgNnSc4d1	elektrické
vedení	vedení	k1gNnSc4	vedení
z	z	k7c2	z
omítky	omítka	k1gFnSc2	omítka
<g/>
,	,	kIx,	,
zakládali	zakládat	k5eAaImAgMnP	zakládat
požáry	požár	k1gInPc4	požár
a	a	k8xC	a
buď	buď	k8xC	buď
dům	dům	k1gInSc4	dům
spálili	spálit	k5eAaPmAgMnP	spálit
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
nechali	nechat	k5eAaPmAgMnP	nechat
velkou	velký	k2eAgFnSc4d1	velká
díru	díra	k1gFnSc4	díra
v	v	k7c6	v
podlaze	podlaha	k1gFnSc6	podlaha
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
chovali	chovat	k5eAaImAgMnP	chovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
naprostí	naprostý	k2eAgMnPc1d1	naprostý
divoši	divoch	k1gMnPc1	divoch
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Podle	podle	k7c2	podle
několika	několik	k4yIc2	několik
západních	západní	k2eAgInPc2d1	západní
zdrojů	zdroj	k1gInPc2	zdroj
Sověti	Sovět	k1gMnPc1	Sovět
v	v	k7c6	v
Mandžusku	Mandžusko	k1gNnSc6	Mandžusko
prováděli	provádět	k5eAaImAgMnP	provádět
politiku	politika	k1gFnSc4	politika
loupení	loupení	k1gNnSc2	loupení
a	a	k8xC	a
znásilňování	znásilňování	k1gNnSc1	znásilňování
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Stejní	stejný	k2eAgMnPc1d1	stejný
Sovětští	sovětský	k2eAgMnPc1d1	sovětský
vojáci	voják	k1gMnPc1	voják
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
posláni	poslat	k5eAaPmNgMnP	poslat
do	do	k7c2	do
Mandžuska	Mandžusko	k1gNnSc2	Mandžusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
plenili	plenit	k5eAaImAgMnP	plenit
<g/>
,	,	kIx,	,
vraždili	vraždit	k5eAaImAgMnP	vraždit
a	a	k8xC	a
znásilňovali	znásilňovat	k5eAaImAgMnP	znásilňovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Harbinu	Harbina	k1gFnSc4	Harbina
Číňani	Číňan	k1gMnPc1	Číňan
vyvěšovali	vyvěšovat	k5eAaImAgMnP	vyvěšovat
slogany	slogan	k1gInPc4	slogan
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Pryč	pryč	k1gFnSc1	pryč
s	s	k7c7	s
rudým	rudý	k2eAgInSc7d1	rudý
imperialismem	imperialismus	k1gInSc7	imperialismus
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Sovětské	sovětský	k2eAgFnPc1d1	sovětská
síly	síla	k1gFnPc1	síla
protesty	protest	k1gInPc4	protest
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
čínských	čínský	k2eAgMnPc2d1	čínský
komunistických	komunistický	k2eAgMnPc2d1	komunistický
představitelů	představitel	k1gMnPc2	představitel
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
hromadné	hromadný	k2eAgNnSc1d1	hromadné
znásilňování	znásilňování	k1gNnSc1	znásilňování
a	a	k8xC	a
loupení	loupení	k1gNnSc1	loupení
ignorovaly	ignorovat	k5eAaImAgFnP	ignorovat
<g/>
.	.	kIx.	.
<g/>
Ruský	ruský	k2eAgMnSc1d1	ruský
historik	historik	k1gMnSc1	historik
Konstantin	Konstantin	k1gMnSc1	Konstantin
Asmolov	Asmolov	k1gInSc4	Asmolov
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
argumentovat	argumentovat	k5eAaImF	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
západní	západní	k2eAgFnPc1d1	západní
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
sovětském	sovětský	k2eAgInSc6d1	sovětský
násilí	násilí	k1gNnSc2	násilí
proti	proti	k7c3	proti
civilnímu	civilní	k2eAgNnSc3d1	civilní
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
na	na	k7c6	na
Dálném	dálný	k2eAgInSc6d1	dálný
východě	východ	k1gInSc6	východ
přehnané	přehnaný	k2eAgNnSc1d1	přehnané
izolované	izolovaný	k2eAgInPc1d1	izolovaný
incidenty	incident	k1gInPc1	incident
a	a	k8xC	a
dokumenty	dokument	k1gInPc1	dokument
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nepodporují	podporovat	k5eNaImIp3nP	podporovat
tvrzení	tvrzení	k1gNnSc4	tvrzení
masových	masový	k2eAgInPc2d1	masový
zločinů	zločin	k1gInPc2	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Asmolov	Asmolov	k1gInSc1	Asmolov
také	také	k9	také
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sověti	Sovět	k1gMnPc1	Sovět
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Němců	Němec	k1gMnPc2	Němec
a	a	k8xC	a
Japonců	Japonec	k1gMnPc2	Japonec
<g/>
,	,	kIx,	,
prý	prý	k9	prý
své	svůj	k3xOyFgMnPc4	svůj
vojáky	voják	k1gMnPc4	voják
a	a	k8xC	a
důstojníky	důstojník	k1gMnPc4	důstojník
za	za	k7c4	za
takové	takový	k3xDgInPc4	takový
činy	čin	k1gInPc4	čin
stíhali	stíhat	k5eAaImAgMnP	stíhat
<g/>
.	.	kIx.	.
<g/>
Zločiny	zločin	k1gInPc1	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
byly	být	k5eAaImAgFnP	být
páchány	páchán	k2eAgInPc1d1	páchán
také	také	k9	také
na	na	k7c6	na
japonských	japonský	k2eAgMnPc6d1	japonský
civilistech	civilista	k1gMnPc6	civilista
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
.	.	kIx.	.
masakr	masakr	k1gInSc4	masakr
v	v	k7c4	v
Gegenmiao	Gegenmiao	k1gNnSc4	Gegenmiao
provedený	provedený	k2eAgInSc1d1	provedený
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
armádou	armáda	k1gFnSc7	armáda
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
na	na	k7c6	na
skupině	skupina	k1gFnSc6	skupina
asi	asi	k9	asi
1	[number]	k4	1
800	[number]	k4	800
japonských	japonský	k2eAgFnPc2d1	japonská
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
jako	jako	k9	jako
uprchlíci	uprchlík	k1gMnPc1	uprchlík
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
Gegenmiao	Gegenmiao	k1gMnSc1	Gegenmiao
<g/>
/	/	kIx~	/
<g/>
Koken-Miao	Koken-Miao	k1gMnSc1	Koken-Miao
(	(	kIx(	(
<g/>
葛	葛	k?	葛
廟	廟	k?	廟
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Japonsko	Japonsko	k1gNnSc1	Japonsko
===	===	k?	===
</s>
</p>
<p>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
dopustila	dopustit	k5eAaPmAgFnS	dopustit
zločinů	zločin	k1gInPc2	zločin
proti	proti	k7c3	proti
japonskému	japonský	k2eAgNnSc3d1	Japonské
civilnímu	civilní	k2eAgNnSc3d1	civilní
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
a	a	k8xC	a
vzdávajícímu	vzdávající	k2eAgNnSc3d1	vzdávající
se	se	k3xPyFc4	se
vojenského	vojenský	k2eAgInSc2d1	vojenský
personálu	personál	k1gInSc2	personál
v	v	k7c6	v
závěrečných	závěrečný	k2eAgFnPc6d1	závěrečná
fázích	fáze	k1gFnPc6	fáze
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
během	během	k7c2	během
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
Sachalin	Sachalin	k1gInSc4	Sachalin
a	a	k8xC	a
Kurilské	kurilský	k2eAgInPc4d1	kurilský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Zločiny	zločin	k1gInPc1	zločin
proti	proti	k7c3	proti
míru	mír	k1gInSc3	mír
==	==	k?	==
</s>
</p>
<p>
<s>
Zločinem	zločin	k1gInSc7	zločin
proti	proti	k7c3	proti
míru	mír	k1gInSc3	mír
je	být	k5eAaImIp3nS	být
příprava	příprava	k1gFnSc1	příprava
<g/>
,	,	kIx,	,
plánování	plánování	k1gNnSc1	plánování
<g/>
,	,	kIx,	,
rozpoutání	rozpoutání	k1gNnSc1	rozpoutání
a	a	k8xC	a
vedení	vedení	k1gNnSc1	vedení
útočné	útočný	k2eAgFnSc2d1	útočná
války	válka	k1gFnSc2	válka
nebo	nebo	k8xC	nebo
zločinné	zločinný	k2eAgNnSc1d1	zločinné
spiknutí	spiknutí	k1gNnSc1	spiknutí
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
vést	vést	k5eAaImF	vést
útočnou	útočný	k2eAgFnSc4d1	útočná
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Představitelé	představitel	k1gMnPc1	představitel
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
naplnili	naplnit	k5eAaPmAgMnP	naplnit
skutkovou	skutkový	k2eAgFnSc4d1	skutková
podstatu	podstata	k1gFnSc4	podstata
zločinu	zločin	k1gInSc2	zločin
proti	proti	k7c3	proti
míru	mír	k1gInSc3	mír
napadením	napadení	k1gNnSc7	napadení
Finska	Finsko	k1gNnSc2	Finsko
v	v	k7c6	v
zimní	zimní	k2eAgFnSc6d1	zimní
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgInPc2	tento
kroků	krok	k1gInPc2	krok
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
a	a	k8xC	a
po	po	k7c4	po
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
podroben	podrobit	k5eAaPmNgInS	podrobit
embargu	embargo	k1gNnSc3	embargo
od	od	k7c2	od
některých	některý	k3yIgInPc2	některý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
zločin	zločin	k1gInSc4	zločin
proti	proti	k7c3	proti
míru	mír	k1gInSc3	mír
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
uzavření	uzavření	k1gNnSc4	uzavření
tajných	tajný	k2eAgInPc2d1	tajný
dodatků	dodatek	k1gInPc2	dodatek
k	k	k7c3	k
Paktu	pakt	k1gInSc3	pakt
Ribbentrop-Molotov	Ribbentrop-Molotov	k1gInSc1	Ribbentrop-Molotov
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
si	se	k3xPyFc3	se
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
a	a	k8xC	a
nacistické	nacistický	k2eAgNnSc4d1	nacistické
Německo	Německo	k1gNnSc4	Německo
vymezili	vymezit	k5eAaPmAgMnP	vymezit
sféry	sféra	k1gFnPc4	sféra
vlivu	vliv	k1gInSc2	vliv
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgNnSc1d1	následné
napadení	napadení	k1gNnSc1	napadení
Polska	Polsko	k1gNnSc2	Polsko
vojsky	vojsky	k6eAd1	vojsky
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
bylo	být	k5eAaImAgNnS	být
útočnou	útočný	k2eAgFnSc7d1	útočná
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zacházení	zacházení	k1gNnSc4	zacházení
s	s	k7c7	s
válečnými	válečný	k2eAgMnPc7d1	válečný
zajatci	zajatec	k1gMnPc7	zajatec
===	===	k?	===
</s>
</p>
<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
odmítal	odmítat	k5eAaImAgMnS	odmítat
Ženevské	ženevský	k2eAgFnPc4d1	Ženevská
konvence	konvence	k1gFnPc4	konvence
i	i	k8xC	i
další	další	k2eAgFnPc4d1	další
smlouvy	smlouva	k1gFnPc4	smlouva
o	o	k7c4	o
zacházení	zacházení	k1gNnSc4	zacházení
se	s	k7c7	s
zajatci	zajatec	k1gMnPc7	zajatec
jakožto	jakožto	k8xS	jakožto
"	"	kIx"	"
<g/>
buržoazní	buržoazní	k2eAgInSc4d1	buržoazní
přežitek	přežitek	k1gInSc4	přežitek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Odmítl	odmítnout	k5eAaPmAgInS	odmítnout
všechny	všechen	k3xTgFnPc4	všechen
tyto	tento	k3xDgFnPc4	tento
smlouvy	smlouva	k1gFnPc4	smlouva
a	a	k8xC	a
omezení	omezení	k1gNnSc4	omezení
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
schválila	schválit	k5eAaPmAgFnS	schválit
předchozí	předchozí	k2eAgFnSc1d1	předchozí
carská	carský	k2eAgFnSc1d1	carská
vláda	vláda	k1gFnSc1	vláda
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
nabídku	nabídka	k1gFnSc4	nabídka
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
učiněnou	učiněný	k2eAgFnSc4d1	učiněná
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
bojů	boj	k1gInPc2	boj
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
navrhovala	navrhovat	k5eAaImAgFnS	navrhovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
budou	být	k5eAaImBp3nP	být
těmito	tento	k3xDgFnPc7	tento
smlouvami	smlouva	k1gFnPc7	smlouva
řídit	řídit	k5eAaImF	řídit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
K	k	k7c3	k
určité	určitý	k2eAgFnSc3d1	určitá
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
omezené	omezený	k2eAgFnPc1d1	omezená
<g/>
,	,	kIx,	,
změně	změna	k1gFnSc3	změna
přístupu	přístup	k1gInSc2	přístup
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
v	v	k7c6	v
konečných	konečný	k2eAgFnPc6d1	konečná
fázích	fáze	k1gFnPc6	fáze
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Váleční	váleční	k2eAgMnPc1d1	váleční
zajatci	zajatec	k1gMnPc1	zajatec
byli	být	k5eAaImAgMnP	být
bráni	brát	k5eAaImNgMnP	brát
nahodile	nahodile	k6eAd1	nahodile
<g/>
,	,	kIx,	,
zacházeno	zacházen	k2eAgNnSc1d1	zacházeno
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
bylo	být	k5eAaImAgNnS	být
nelidsky	lidsky	k6eNd1	lidsky
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
naprosto	naprosto	k6eAd1	naprosto
nevyhovujících	vyhovující	k2eNgFnPc6d1	nevyhovující
podmínkách	podmínka	k1gFnPc6	podmínka
vězněni	věznit	k5eAaImNgMnP	věznit
a	a	k8xC	a
nuceni	nutit	k5eAaImNgMnP	nutit
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
uplatnil	uplatnit	k5eAaPmAgInS	uplatnit
německé	německý	k2eAgMnPc4d1	německý
zajatce	zajatec	k1gMnPc4	zajatec
jako	jako	k8xC	jako
levnou	levný	k2eAgFnSc4d1	levná
pracovní	pracovní	k2eAgFnSc4d1	pracovní
sílu	síla	k1gFnSc4	síla
(	(	kIx(	(
<g/>
navíc	navíc	k6eAd1	navíc
kvalifikovanější	kvalifikovaný	k2eAgInPc1d2	kvalifikovanější
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
vlastní	vlastní	k2eAgNnSc1d1	vlastní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
váleční	váleční	k2eAgMnPc1d1	váleční
zajatci	zajatec	k1gMnPc1	zajatec
však	však	k9	však
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
pracovat	pracovat	k5eAaImF	pracovat
i	i	k8xC	i
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
státech	stát	k1gInPc6	stát
protinacistické	protinacistický	k2eAgFnSc2d1	protinacistická
koalice	koalice	k1gFnSc2	koalice
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
panoval	panovat	k5eAaImAgInS	panovat
všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
Němci	Němec	k1gMnPc1	Němec
měli	mít	k5eAaImAgMnP	mít
část	část	k1gFnSc4	část
škod	škoda	k1gFnPc2	škoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
napáchali	napáchat	k5eAaBmAgMnP	napáchat
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
odpracovat	odpracovat	k5eAaPmF	odpracovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
však	však	k9	však
byli	být	k5eAaImAgMnP	být
tito	tento	k3xDgMnPc1	tento
zajatci	zajatec	k1gMnPc1	zajatec
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
neúměrně	úměrně	k6eNd1	úměrně
horším	zlý	k2eAgFnPc3d2	horší
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
,	,	kIx,	,
než	než	k8xS	než
němečtí	německý	k2eAgMnPc1d1	německý
zajatci	zajatec	k1gMnPc1	zajatec
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
i	i	k8xC	i
délka	délka	k1gFnSc1	délka
jejich	jejich	k3xOp3gInSc2	jejich
nedobrovolného	dobrovolný	k2eNgInSc2d1	nedobrovolný
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
pracovních	pracovní	k2eAgInPc6d1	pracovní
táborech	tábor	k1gInPc6	tábor
byla	být	k5eAaImAgNnP	být
daleko	daleko	k6eAd1	daleko
delší	dlouhý	k2eAgNnPc1d2	delší
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
už	už	k6eAd1	už
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
nemohl	moct	k5eNaImAgInS	moct
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
scéně	scéna	k1gFnSc6	scéna
dále	daleko	k6eAd2	daleko
obhájit	obhájit	k5eAaPmF	obhájit
jejich	jejich	k3xOp3gNnPc4	jejich
další	další	k2eAgNnPc4d1	další
zadržování	zadržování	k1gNnPc4	zadržování
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
jako	jako	k9	jako
válečné	válečný	k2eAgMnPc4d1	válečný
zločince	zločinec	k1gMnPc4	zločinec
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
na	na	k7c6	na
základě	základ	k1gInSc6	základ
naprosto	naprosto	k6eAd1	naprosto
absurdních	absurdní	k2eAgNnPc2d1	absurdní
obvinění	obvinění	k1gNnPc2	obvinění
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
například	například	k6eAd1	například
Erich	Erich	k1gMnSc1	Erich
Hartmann	Hartmann	k1gMnSc1	Hartmann
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
je	on	k3xPp3gNnSc4	on
mohl	moct	k5eAaImAgInS	moct
ponechat	ponechat	k5eAaPmF	ponechat
ještě	ještě	k9	ještě
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Vyšetřovací	vyšetřovací	k2eAgInSc1d1	vyšetřovací
orgán	orgán	k1gInSc1	orgán
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
pro	pro	k7c4	pro
porušování	porušování	k1gNnSc4	porušování
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
sbíral	sbírat	k5eAaImAgMnS	sbírat
a	a	k8xC	a
vyšetřoval	vyšetřovat	k5eAaImAgMnS	vyšetřovat
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c6	o
zločinech	zločin	k1gInPc6	zločin
proti	proti	k7c3	proti
válečným	válečný	k2eAgMnPc3d1	válečný
zajatcům	zajatec	k1gMnPc3	zajatec
sil	síla	k1gFnPc2	síla
Osy	osa	k1gFnPc4	osa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kubánsko-amerického	kubánskomerický	k2eAgMnSc2d1	kubánsko-americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
Alfreda	Alfred	k1gMnSc2	Alfred
de	de	k?	de
Zayas	Zayas	k1gInSc1	Zayas
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
"	"	kIx"	"
<g/>
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
ruské	ruský	k2eAgFnSc2d1	ruská
kampaně	kampaň	k1gFnSc2	kampaň
nekončily	končit	k5eNaImAgFnP	končit
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c4	o
mučení	mučení	k1gNnSc4	mučení
a	a	k8xC	a
vraždění	vraždění	k1gNnSc4	vraždění
německých	německý	k2eAgMnPc2d1	německý
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Úřad	úřad	k1gInSc1	úřad
měl	mít	k5eAaImAgInS	mít
pět	pět	k4xCc4	pět
hlavních	hlavní	k2eAgInPc2d1	hlavní
zdrojů	zdroj	k1gInPc2	zdroj
informací	informace	k1gFnPc2	informace
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Zabavené	zabavený	k2eAgInPc4d1	zabavený
nepřátelské	přátelský	k2eNgInPc4d1	nepřátelský
dokumenty	dokument	k1gInPc4	dokument
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
rozkazy	rozkaz	k1gInPc1	rozkaz
<g/>
,	,	kIx,	,
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c4	o
činnosti	činnost	k1gFnPc4	činnost
a	a	k8xC	a
propagandistické	propagandistický	k2eAgInPc4d1	propagandistický
letáky	leták	k1gInPc4	leták
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
zachycené	zachycený	k2eAgFnSc2d1	zachycená
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
a	a	k8xC	a
radiové	radiový	k2eAgFnSc2d1	radiová
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Svědectví	svědectví	k1gNnSc1	svědectví
sovětských	sovětský	k2eAgMnPc2d1	sovětský
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
svědectví	svědectví	k1gNnSc4	svědectví
zajatých	zajatá	k1gFnPc2	zajatá
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
unikli	uniknout	k5eAaPmAgMnP	uniknout
a	a	k8xC	a
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
svědectví	svědectví	k1gNnSc1	svědectví
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
viděli	vidět	k5eAaImAgMnP	vidět
mrtvoly	mrtvola	k1gFnPc4	mrtvola
nebo	nebo	k8xC	nebo
zmrzačená	zmrzačený	k2eAgNnPc4d1	zmrzačené
těla	tělo	k1gNnPc4	tělo
popravených	popravený	k2eAgMnPc2d1	popravený
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
úřad	úřad	k1gInSc1	úřad
sestavil	sestavit	k5eAaPmAgInS	sestavit
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
svědectví	svědectví	k1gNnPc2	svědectví
<g/>
,	,	kIx,	,
zprávy	zpráva	k1gFnPc4	zpráva
<g/>
,	,	kIx,	,
a	a	k8xC	a
zabavených	zabavený	k2eAgInPc2d1	zabavený
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
<g/>
,	,	kIx,	,
když	když	k8xS	když
už	už	k9	už
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
,	,	kIx,	,
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zabíjení	zabíjení	k1gNnSc1	zabíjení
německých	německý	k2eAgMnPc2d1	německý
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
zadržení	zadržení	k1gNnSc6	zadržení
nebo	nebo	k8xC	nebo
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
výslechu	výslech	k1gInSc6	výslech
nebyla	být	k5eNaImAgFnS	být
žádná	žádný	k3yNgFnSc1	žádný
izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
věc	věc	k1gFnSc1	věc
<g/>
.	.	kIx.	.
</s>
<s>
Dokumenty	dokument	k1gInPc1	dokument
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
několik	několik	k4yIc4	několik
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c6	o
úmyslném	úmyslný	k2eAgNnSc6d1	úmyslné
zabíjení	zabíjení	k1gNnSc6	zabíjení
německých	německý	k2eAgMnPc2d1	německý
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nelze	lze	k6eNd1	lze
to	ten	k3xDgNnSc4	ten
srovnat	srovnat	k5eAaPmF	srovnat
s	s	k7c7	s
žádnými	žádný	k3yNgFnPc7	žádný
událostmi	událost	k1gFnPc7	událost
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
z	z	k7c2	z
listopadu	listopad	k1gInSc2	listopad
1941	[number]	k4	1941
vyšetřovací	vyšetřovací	k2eAgInSc1d1	vyšetřovací
úřad	úřad	k1gInSc1	úřad
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
viní	vinit	k5eAaImIp3nS	vinit
Rudou	rudý	k2eAgFnSc4d1	rudá
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
že	že	k8xS	že
provádí	provádět	k5eAaImIp3nS	provádět
"	"	kIx"	"
<g/>
teroristickou	teroristický	k2eAgFnSc4d1	teroristická
politiku	politika	k1gFnSc4	politika
<g/>
...	...	k?	...
<g/>
proti	proti	k7c3	proti
bezbranným	bezbranný	k2eAgMnPc3d1	bezbranný
německým	německý	k2eAgMnPc3d1	německý
vojákům	voják	k1gMnPc3	voják
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
padli	padnout	k5eAaPmAgMnP	padnout
do	do	k7c2	do
jejích	její	k3xOp3gFnPc2	její
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
proti	proti	k7c3	proti
členům	člen	k1gMnPc3	člen
německého	německý	k2eAgInSc2d1	německý
lékařského	lékařský	k2eAgInSc2d1	lékařský
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
<g/>
...	...	k?	...
<g/>
učinila	učinit	k5eAaPmAgFnS	učinit
pro	pro	k7c4	pro
využití	využití	k1gNnSc4	využití
těchto	tento	k3xDgInPc2	tento
prostředků	prostředek	k1gInPc2	prostředek
kamufláž	kamufláž	k1gFnSc4	kamufláž
<g/>
:	:	kIx,	:
podle	podle	k7c2	podle
rozkazu	rozkaz	k1gInSc2	rozkaz
Rudé	rudý	k2eAgFnSc3d1	rudá
armádě	armáda	k1gFnSc3	armáda
vyneseného	vynesený	k2eAgInSc2d1	vynesený
se	se	k3xPyFc4	se
schválením	schválení	k1gNnSc7	schválení
rady	rada	k1gFnSc2	rada
lidových	lidový	k2eAgMnPc2d1	lidový
komisařů	komisař	k1gMnPc2	komisař
ze	z	k7c2	z
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1941	[number]	k4	1941
jsou	být	k5eAaImIp3nP	být
normy	norma	k1gFnPc1	norma
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
uveřejněny	uveřejněn	k2eAgInPc4d1	uveřejněn
a	a	k8xC	a
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
je	být	k5eAaImIp3nS	být
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
haagských	haagský	k2eAgNnPc2d1	Haagské
pravidel	pravidlo	k1gNnPc2	pravidlo
v	v	k7c6	v
pozemní	pozemní	k2eAgFnSc6d1	pozemní
válce	válka	k1gFnSc6	válka
následovat	následovat	k5eAaImF	následovat
<g/>
...	...	k?	...
<g/>
tento	tento	k3xDgInSc1	tento
<g/>
...	...	k?	...
<g/>
ruský	ruský	k2eAgInSc1d1	ruský
rozkaz	rozkaz	k1gInSc1	rozkaz
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
téměř	téměř	k6eAd1	téměř
nerozeslán	rozeslán	k2eNgInSc1d1	rozeslán
a	a	k8xC	a
určitě	určitě	k6eAd1	určitě
nebyl	být	k5eNaImAgInS	být
vůbec	vůbec	k9	vůbec
následován	následován	k2eAgInSc1d1	následován
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nS	by
nemohlo	moct	k5eNaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
nevyslovitelným	vyslovitelný	k2eNgMnPc3d1	nevyslovitelný
zločinům	zločin	k1gMnPc3	zločin
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
výpovědi	výpověď	k1gFnSc2	výpověď
sovětské	sovětský	k2eAgInPc4d1	sovětský
masakry	masakr	k1gInPc4	masakr
německých	německý	k2eAgFnPc2d1	německá
<g/>
,	,	kIx,	,
italských	italský	k2eAgFnPc2d1	italská
<g/>
,	,	kIx,	,
španělských	španělský	k2eAgFnPc2d1	španělská
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
válečných	válečná	k1gFnPc2	válečná
zajatců	zajatec	k1gMnPc2	zajatec
Osy	osa	k1gFnSc2	osa
byly	být	k5eAaImAgFnP	být
často	často	k6eAd1	často
podněcovány	podněcován	k2eAgFnPc4d1	podněcována
komisaři	komisař	k1gMnPc7	komisař
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
rozkazy	rozkaz	k1gInPc7	rozkaz
od	od	k7c2	od
Stalina	Stalin	k1gMnSc2	Stalin
a	a	k8xC	a
politbyra	politbyro	k1gNnPc1	politbyro
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
důkaz	důkaz	k1gInSc1	důkaz
utvrdil	utvrdit	k5eAaPmAgInS	utvrdit
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
válečných	válečný	k2eAgInPc2d1	válečný
zločinů	zločin	k1gInPc2	zločin
<g/>
,	,	kIx,	,
že	že	k8xS	že
Stalin	Stalin	k1gMnSc1	Stalin
vydal	vydat	k5eAaPmAgMnS	vydat
tajné	tajný	k2eAgInPc4d1	tajný
rozkazy	rozkaz	k1gInPc4	rozkaz
k	k	k7c3	k
masakru	masakr	k1gInSc3	masakr
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
42	[number]	k4	42
zajala	zajmout	k5eAaPmAgFnS	zajmout
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
každý	každý	k3xTgInSc4	každý
měsíc	měsíc	k1gInSc4	měsíc
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
000	[number]	k4	000
německých	německý	k2eAgMnPc2d1	německý
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
míra	míra	k1gFnSc1	míra
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
stala	stát	k5eAaPmAgFnS	stát
tak	tak	k6eAd1	tak
vysokou	vysoký	k2eAgFnSc4d1	vysoká
<g/>
,	,	kIx,	,
že	že	k8xS	že
absolutní	absolutní	k2eAgInSc1d1	absolutní
počet	počet	k1gInSc1	počet
vězňů	vězeň	k1gMnPc2	vězeň
klesl	klesnout	k5eAaPmAgInS	klesnout
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
byl	být	k5eAaImAgInS	být
úředně	úředně	k6eAd1	úředně
snížen	snížit	k5eAaPmNgInS	snížit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Sovětské	sovětský	k2eAgInPc4d1	sovětský
zdroje	zdroj	k1gInPc4	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
smrt	smrt	k1gFnSc4	smrt
474	[number]	k4	474
967	[number]	k4	967
z	z	k7c2	z
2	[number]	k4	2
652	[number]	k4	652
672	[number]	k4	672
německých	německý	k2eAgMnPc2d1	německý
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Rüdiger	Rüdiger	k1gInSc1	Rüdiger
Overmans	Overmansa	k1gFnPc2	Overmansa
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vypadá	vypadat	k5eAaImIp3nS	vypadat
zcela	zcela	k6eAd1	zcela
věrohodně	věrohodně	k6eAd1	věrohodně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
prokazatelné	prokazatelný	k2eAgNnSc1d1	prokazatelné
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgInSc1d1	další
německý	německý	k2eAgInSc1d1	německý
vojenský	vojenský	k2eAgInSc1d1	vojenský
personál	personál	k1gInSc1	personál
vedený	vedený	k2eAgInSc1d1	vedený
jako	jako	k8xC	jako
nezvěstný	zvěstný	k2eNgMnSc1d1	nezvěstný
vlastně	vlastně	k9	vlastně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
sovětském	sovětský	k2eAgNnSc6d1	sovětské
zajetí	zajetí	k1gNnSc6	zajetí
jako	jako	k8xS	jako
váleční	váleční	k2eAgInSc1d1	váleční
zajatci	zajatec	k1gMnPc7	zajatec
a	a	k8xC	a
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
skutečný	skutečný	k2eAgInSc1d1	skutečný
počet	počet	k1gInSc1	počet
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
německých	německý	k2eAgMnPc2d1	německý
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
na	na	k7c6	na
asi	asi	k9	asi
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Masakr	masakr	k1gInSc4	masakr
ve	v	k7c6	v
Feodosiji	Feodosiji	k1gFnSc6	Feodosiji
====	====	k?	====
</s>
</p>
<p>
<s>
Sovětští	sovětský	k2eAgMnPc1d1	sovětský
vojáci	voják	k1gMnPc1	voják
se	se	k3xPyFc4	se
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
obtěžovali	obtěžovat	k5eAaImAgMnP	obtěžovat
léčit	léčit	k5eAaImF	léčit
zraněné	zraněný	k2eAgMnPc4d1	zraněný
německé	německý	k2eAgMnPc4d1	německý
válečné	válečný	k2eAgMnPc4d1	válečný
zajatce	zajatec	k1gMnPc4	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krymském	krymský	k2eAgNnSc6d1	krymské
městě	město	k1gNnSc6	město
Feodosija	Feodosij	k1gInSc2	Feodosij
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
případu	případ	k1gInSc3	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1942	[number]	k4	1942
nakrátko	nakrátko	k6eAd1	nakrátko
získáno	získat	k5eAaPmNgNnS	získat
sovětskými	sovětský	k2eAgFnPc7d1	sovětská
silami	síla	k1gFnPc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Ustupující	ustupující	k2eAgInSc4d1	ustupující
Wehrmacht	wehrmacht	k1gInSc4	wehrmacht
zde	zde	k6eAd1	zde
ponechal	ponechat	k5eAaPmAgMnS	ponechat
160	[number]	k4	160
zraněných	zraněný	k2eAgMnPc2d1	zraněný
vojáků	voják	k1gMnPc2	voják
ve	v	k7c6	v
vojenských	vojenský	k2eAgFnPc6d1	vojenská
nemocnicích	nemocnice	k1gFnPc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Němci	Němec	k1gMnPc1	Němec
město	město	k1gNnSc4	město
získali	získat	k5eAaPmAgMnP	získat
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
zraněný	zraněný	k2eAgMnSc1d1	zraněný
voják	voják	k1gMnSc1	voják
byl	být	k5eAaImAgMnS	být
příslušníky	příslušník	k1gMnPc4	příslušník
Rudé	rudý	k1gMnPc4	rudý
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
a	a	k8xC	a
NKVD	NKVD	kA	NKVD
zmasakrován	zmasakrovat	k5eAaPmNgInS	zmasakrovat
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
byli	být	k5eAaImAgMnP	být
zastřeleni	zastřelit	k5eAaPmNgMnP	zastřelit
na	na	k7c6	na
nemocničních	nemocniční	k2eAgNnPc6d1	nemocniční
lůžkách	lůžko	k1gNnPc6	lůžko
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
byli	být	k5eAaImAgMnP	být
ubiti	ubít	k5eAaPmNgMnP	ubít
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
nebo	nebo	k8xC	nebo
vyházeni	vyházen	k2eAgMnPc1d1	vyházen
z	z	k7c2	z
oken	okno	k1gNnPc2	okno
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
byli	být	k5eAaImAgMnP	být
namočeni	namočit	k5eAaPmNgMnP	namočit
do	do	k7c2	do
ledové	ledový	k2eAgFnSc2d1	ledová
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
venku	venku	k6eAd1	venku
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
na	na	k7c4	na
podchlazení	podchlazení	k1gNnSc4	podchlazení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Masakr	masakr	k1gInSc4	masakr
v	v	k7c4	v
Grišinu	Grišina	k1gFnSc4	Grišina
====	====	k?	====
</s>
</p>
<p>
<s>
Masakr	masakr	k1gInSc1	masakr
v	v	k7c4	v
Grišinu	Grišina	k1gFnSc4	Grišina
spáchala	spáchat	k5eAaPmAgFnS	spáchat
obrněná	obrněný	k2eAgFnSc1d1	obrněná
divize	divize	k1gFnSc1	divize
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1943	[number]	k4	1943
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Krasnoarmijsk	Krasnoarmijsk	k1gInSc1	Krasnoarmijsk
<g/>
,	,	kIx,	,
Postyševo	Postyševo	k1gNnSc1	Postyševo
a	a	k8xC	a
Grišino	Grišin	k2eAgNnSc1d1	Grišino
<g/>
.	.	kIx.	.
</s>
<s>
Untersuchungsstelle	Untersuchungsstelle	k6eAd1	Untersuchungsstelle
<g/>
,	,	kIx,	,
také	také	k9	také
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
WuSt	WuSt	k2eAgMnSc1d1	WuSt
(	(	kIx(	(
<g/>
Vyšetřovací	vyšetřovací	k2eAgInSc1d1	vyšetřovací
orgán	orgán	k1gInSc1	orgán
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
pro	pro	k7c4	pro
porušování	porušování	k1gNnSc4	porušování
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
oběťmi	oběť	k1gFnPc7	oběť
bylo	být	k5eAaImAgNnS	být
406	[number]	k4	406
vojáků	voják	k1gMnPc2	voják
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
<g/>
,	,	kIx,	,
58	[number]	k4	58
členů	člen	k1gMnPc2	člen
organizace	organizace	k1gFnSc2	organizace
Todt	Todt	k1gMnSc1	Todt
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
dvou	dva	k4xCgMnPc2	dva
dánských	dánský	k2eAgMnPc2d1	dánský
občanů	občan	k1gMnPc2	občan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
89	[number]	k4	89
italských	italský	k2eAgMnPc2d1	italský
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
9	[number]	k4	9
rumunských	rumunský	k2eAgMnPc2d1	rumunský
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
4	[number]	k4	4
maďarští	maďarský	k2eAgMnPc1d1	maďarský
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
15	[number]	k4	15
německých	německý	k2eAgMnPc2d1	německý
civilních	civilní	k2eAgMnPc2d1	civilní
úředníků	úředník	k1gMnPc2	úředník
<g/>
,	,	kIx,	,
7	[number]	k4	7
německých	německý	k2eAgMnPc2d1	německý
civilních	civilní	k2eAgMnPc2d1	civilní
pracovníků	pracovník	k1gMnPc2	pracovník
a	a	k8xC	a
8	[number]	k4	8
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Místa	místo	k1gNnPc1	místo
byla	být	k5eAaImAgNnP	být
napadena	napadnout	k5eAaPmNgNnP	napadnout
sovětským	sovětský	k2eAgMnSc7d1	sovětský
4	[number]	k4	4
<g/>
.	.	kIx.	.
gardovým	gardový	k2eAgInSc7d1	gardový
tankovým	tankový	k2eAgInSc7d1	tankový
sborem	sbor	k1gInSc7	sbor
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
znovudobytí	znovudobytí	k1gNnSc6	znovudobytí
divizí	divize	k1gFnPc2	divize
Wiking	Wiking	k1gInSc4	Wiking
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
333	[number]	k4	333
pěší	pěší	k2eAgFnSc1d1	pěší
divize	divize	k1gFnSc1	divize
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
tankové	tankový	k2eAgFnSc2d1	tanková
divize	divize	k1gFnSc2	divize
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1943	[number]	k4	1943
vojáci	voják	k1gMnPc1	voják
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
objevili	objevit	k5eAaPmAgMnP	objevit
mnoho	mnoho	k4c4	mnoho
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
těl	tělo	k1gNnPc2	tělo
bylo	být	k5eAaImAgNnS	být
odporně	odporně	k6eAd1	odporně
znetvořeno	znetvořen	k2eAgNnSc1d1	znetvořeno
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
uřezané	uřezaný	k2eAgNnSc4d1	uřezané
uši	ucho	k1gNnPc4	ucho
a	a	k8xC	a
nosy	nos	k1gInPc4	nos
a	a	k8xC	a
také	také	k9	také
amputované	amputovaný	k2eAgInPc1d1	amputovaný
pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
orgány	orgán	k1gInPc1	orgán
nacpané	nacpaný	k2eAgInPc1d1	nacpaný
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
<g/>
.	.	kIx.	.
</s>
<s>
Prsy	prs	k1gInPc1	prs
některých	některý	k3yIgFnPc2	některý
sester	sestra	k1gFnPc2	sestra
byly	být	k5eAaImAgInP	být
odříznuty	odříznout	k5eAaPmNgInP	odříznout
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
byly	být	k5eAaImAgFnP	být
brutálně	brutálně	k6eAd1	brutálně
znásilněny	znásilněn	k2eAgFnPc1d1	znásilněna
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgMnSc1d1	německý
vojenský	vojenský	k2eAgMnSc1d1	vojenský
soudce	soudce	k1gMnSc1	soudce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
během	během	k7c2	během
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
že	že	k8xS	že
viděl	vidět	k5eAaImAgMnS	vidět
ženské	ženský	k2eAgNnSc4d1	ženské
tělo	tělo	k1gNnSc4	tělo
s	s	k7c7	s
roztaženýma	roztažený	k2eAgFnPc7d1	roztažená
nohama	noha	k1gFnPc7	noha
a	a	k8xC	a
v	v	k7c6	v
genitáliích	genitálie	k1gFnPc6	genitálie
vražené	vražený	k2eAgNnSc1d1	vražené
koště	koště	k1gNnSc1	koště
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sklepě	sklep	k1gInSc6	sklep
hlavního	hlavní	k2eAgNnSc2d1	hlavní
vlakového	vlakový	k2eAgNnSc2d1	vlakové
nádraží	nádraží	k1gNnSc2	nádraží
bylo	být	k5eAaImAgNnS	být
kolem	kolem	k7c2	kolem
120	[number]	k4	120
Němců	Němec	k1gMnPc2	Němec
nahnáno	nahnat	k5eAaPmNgNnS	nahnat
do	do	k7c2	do
velkého	velký	k2eAgInSc2d1	velký
skladu	sklad	k1gInSc2	sklad
a	a	k8xC	a
pak	pak	k6eAd1	pak
"	"	kIx"	"
<g/>
pokoseno	pokosen	k2eAgNnSc1d1	pokoseno
<g/>
"	"	kIx"	"
kulomety	kulomet	k1gInPc1	kulomet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
====	====	k?	====
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
němečtí	německý	k2eAgMnPc1d1	německý
zajatci	zajatec	k1gMnPc1	zajatec
byli	být	k5eAaImAgMnP	být
propuštěni	propustit	k5eAaPmNgMnP	propustit
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
dalších	další	k1gNnPc2	další
však	však	k9	však
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
v	v	k7c6	v
táborech	tábor	k1gInPc6	tábor
Gulag	gulag	k1gInSc4	gulag
ještě	ještě	k6eAd1	ještě
dlouho	dlouho	k6eAd1	dlouho
po	po	k7c6	po
kapitulaci	kapitulace	k1gFnSc6	kapitulace
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
německé	německý	k2eAgMnPc4d1	německý
válečné	válečný	k2eAgMnPc4d1	válečný
veterány	veterán	k1gMnPc4	veterán
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
sovětském	sovětský	k2eAgNnSc6d1	sovětské
zajetí	zajetí	k1gNnSc6	zajetí
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
kapitán	kapitán	k1gMnSc1	kapitán
Wilm	Wilm	k1gMnSc1	Wilm
Hosenfeld	Hosenfeld	k1gMnSc1	Hosenfeld
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
zraněním	zranění	k1gNnSc7	zranění
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc4	který
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
nejspíš	nejspíš	k9	nejspíš
mučením	mučení	k1gNnSc7	mučení
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
poblíž	poblíž	k7c2	poblíž
Stalingradu	Stalingrad	k1gInSc2	Stalingrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgMnS	být
kapitán	kapitán	k1gMnSc1	kapitán
Hosenfeld	Hosenfeld	k1gMnSc1	Hosenfeld
posmrtně	posmrtně	k6eAd1	posmrtně
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
státem	stát	k1gInSc7	stát
Izrael	Izrael	k1gInSc1	Izrael
za	za	k7c2	za
jeho	on	k3xPp3gNnSc2	on
roli	role	k1gFnSc4	role
při	při	k7c6	při
záchraně	záchrana	k1gFnSc6	záchrana
židovských	židovský	k2eAgInPc2d1	židovský
životů	život	k1gInPc2	život
během	během	k7c2	během
holocaustu	holocaust	k1gInSc2	holocaust
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Civilní	civilní	k2eAgNnSc1d1	civilní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
===	===	k?	===
</s>
</p>
<p>
<s>
Válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
páchala	páchat	k5eAaImAgFnS	páchat
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
sovětské	sovětský	k2eAgFnPc1d1	sovětská
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
složky	složka	k1gFnPc1	složka
na	na	k7c6	na
územích	území	k1gNnPc6	území
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
byla	být	k5eAaImAgNnP	být
obsazena	obsadit	k5eAaPmNgNnP	obsadit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
zemí	zem	k1gFnPc2	zem
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
osvobozených	osvobozený	k2eAgFnPc2d1	osvobozená
od	od	k7c2	od
nacistů	nacista	k1gMnPc2	nacista
včetně	včetně	k7c2	včetně
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Odehrávaly	odehrávat	k5eAaImAgFnP	odehrávat
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
jak	jak	k6eAd1	jak
na	na	k7c4	na
individuální	individuální	k2eAgMnPc4d1	individuální
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
systémové	systémový	k2eAgFnSc3d1	systémová
úrovni	úroveň	k1gFnSc3	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvážnější	vážní	k2eAgFnPc4d3	nejvážnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
patřilo	patřit	k5eAaImAgNnS	patřit
znásilňování	znásilňování	k1gNnPc4	znásilňování
<g/>
,	,	kIx,	,
krádeže	krádež	k1gFnPc4	krádež
<g/>
,	,	kIx,	,
rabování	rabování	k1gNnPc4	rabování
<g/>
,	,	kIx,	,
vraždy	vražda	k1gFnPc4	vražda
a	a	k8xC	a
únosy	únos	k1gInPc4	únos
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nejhůře	zle	k6eAd3	zle
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
samozřejmě	samozřejmě	k6eAd1	samozřejmě
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
samotné	samotný	k2eAgNnSc1d1	samotné
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ruští	ruský	k2eAgMnPc1d1	ruský
historici	historik	k1gMnPc1	historik
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
oprávněné	oprávněný	k2eAgNnSc4d1	oprávněné
rozhořčení	rozhořčení	k1gNnSc4	rozhořčení
sovětských	sovětský	k2eAgMnPc2d1	sovětský
vojáků	voják	k1gMnPc2	voják
ze	z	k7c2	z
zločinů	zločin	k1gInPc2	zločin
spáchaných	spáchaný	k2eAgInPc2d1	spáchaný
nacisty	nacista	k1gMnPc7	nacista
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
a	a	k8xC	a
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
odplatě	odplata	k1gFnSc6	odplata
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
rozsah	rozsah	k1gInSc1	rozsah
výše	výše	k1gFnSc2	výše
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
činů	čin	k1gInPc2	čin
zlehčovat	zlehčovat	k5eAaImF	zlehčovat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
selhávání	selhávání	k1gNnSc1	selhávání
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
své	svůj	k3xOyFgFnSc3	svůj
touze	touha	k1gFnSc3	touha
po	po	k7c6	po
pomstě	pomsta	k1gFnSc6	pomsta
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pohled	pohled	k1gInSc1	pohled
ovšem	ovšem	k9	ovšem
ignoruje	ignorovat	k5eAaImIp3nS	ignorovat
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
složky	složka	k1gFnPc1	složka
a	a	k8xC	a
útvary	útvar	k1gInPc1	útvar
NKVD	NKVD	kA	NKVD
se	se	k3xPyFc4	se
těchto	tento	k3xDgFnPc2	tento
i	i	k9	i
dalších	další	k2eAgInPc2d1	další
zločinů	zločin	k1gInPc2	zločin
dopouštěly	dopouštět	k5eAaImAgInP	dopouštět
systematicky	systematicky	k6eAd1	systematicky
již	již	k6eAd1	již
před	před	k7c7	před
Velkou	velký	k2eAgFnSc7d1	velká
vlasteneckou	vlastenecký	k2eAgFnSc7d1	vlastenecká
válkou	válka	k1gFnSc7	válka
(	(	kIx(	(
<g/>
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Pobaltí	Pobaltí	k1gNnSc6	Pobaltí
i	i	k8xC	i
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
oběťmi	oběť	k1gFnPc7	oběť
byli	být	k5eAaImAgMnP	být
často	často	k6eAd1	často
i	i	k9	i
příslušníci	příslušník	k1gMnPc1	příslušník
spřátelených	spřátelený	k2eAgInPc2d1	spřátelený
národů	národ	k1gInPc2	národ
a	a	k8xC	a
ideologičtí	ideologický	k2eAgMnPc1d1	ideologický
soudruzi	soudruh	k1gMnPc1	soudruh
(	(	kIx(	(
<g/>
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
totožnost	totožnost	k1gFnSc1	totožnost
byla	být	k5eAaImAgFnS	být
vojákům	voják	k1gMnPc3	voják
známa	znám	k2eAgNnPc1d1	známo
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
<g/>
,	,	kIx,	,
že	že	k8xS	že
řada	řada	k1gFnSc1	řada
těchto	tento	k3xDgInPc2	tento
válečných	válečný	k2eAgInPc2d1	válečný
zločinů	zločin	k1gInPc2	zločin
byla	být	k5eAaImAgFnS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
propagandu	propaganda	k1gFnSc4	propaganda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jednoznačných	jednoznačný	k2eAgInPc2d1	jednoznačný
rozkazů	rozkaz	k1gInPc2	rozkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Znásilňování	znásilňování	k1gNnSc2	znásilňování
====	====	k?	====
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
zločiny	zločin	k1gInPc4	zločin
proti	proti	k7c3	proti
civilnímu	civilní	k2eAgNnSc3d1	civilní
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
patřilo	patřit	k5eAaImAgNnS	patřit
masové	masový	k2eAgNnSc1d1	masové
znásilňování	znásilňování	k1gNnSc1	znásilňování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vyvoláno	vyvolat	k5eAaPmNgNnS	vyvolat
propagandou	propaganda	k1gFnSc7	propaganda
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
německou	německý	k2eAgFnSc4d1	německá
propagandu	propaganda	k1gFnSc4	propaganda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
líčila	líčit	k5eAaImAgFnS	líčit
občany	občan	k1gMnPc4	občan
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
jako	jako	k8xS	jako
podlidi	podčlověk	k1gMnPc4	podčlověk
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaBmAgInS	napsat
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
sovětský	sovětský	k2eAgMnSc1d1	sovětský
spisovatel	spisovatel	k1gMnSc1	spisovatel
zabývající	zabývající	k2eAgMnSc1d1	zabývající
se	se	k3xPyFc4	se
protiněmeckou	protiněmecký	k2eAgFnSc7d1	protiněmecká
propagandou	propaganda	k1gFnSc7	propaganda
Ilja	Ilja	k1gMnSc1	Ilja
Grigorjevič	Grigorjevič	k1gMnSc1	Grigorjevič
Erenburg	Erenburg	k1gMnSc1	Erenburg
básně	báseň	k1gFnPc4	báseň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
líčily	líčit	k5eAaImAgFnP	líčit
Němce	Němka	k1gFnSc3	Němka
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
fašisty	fašista	k1gMnSc2	fašista
<g/>
,	,	kIx,	,
přinášející	přinášející	k2eAgMnPc1d1	přinášející
s	s	k7c7	s
sebou	se	k3xPyFc7	se
divošství	divošství	k1gNnPc1	divošství
<g/>
,	,	kIx,	,
zvěrstva	zvěrstvo	k1gNnPc1	zvěrstvo
<g/>
,	,	kIx,	,
kult	kult	k1gInSc1	kult
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
přičemž	přičemž	k6eAd1	přičemž
volal	volat	k5eAaImAgMnS	volat
po	po	k7c6	po
pomstě	pomsta	k1gFnSc6	pomsta
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
"	"	kIx"	"
<g/>
jeho	jeho	k3xOp3gNnSc4	jeho
nejznámější	známý	k2eAgInSc1d3	nejznámější
<g/>
"	"	kIx"	"
text	text	k1gInSc1	text
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ponoukal	ponoukat	k5eAaImAgInS	ponoukat
ke	k	k7c3	k
znásilňování	znásilňování	k1gNnSc3	znásilňování
německých	německý	k2eAgFnPc2d1	německá
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
vymysleli	vymyslet	k5eAaPmAgMnP	vymyslet
nacisté	nacista	k1gMnPc1	nacista
na	na	k7c6	na
Goebbelsově	Goebbelsův	k2eAgNnSc6d1	Goebbelsovo
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
propagandy	propaganda	k1gFnSc2	propaganda
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
otištěn	otisknout	k5eAaPmNgInS	otisknout
v	v	k7c6	v
sovětských	sovětský	k2eAgFnPc6d1	sovětská
novinách	novina	k1gFnPc6	novina
článek	článek	k1gInSc4	článek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
Ilja	Ilja	k1gMnSc1	Ilja
Erenburg	Erenburg	k1gMnSc1	Erenburg
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bývaly	bývat	k5eAaImAgFnP	bývat
doby	doba	k1gFnPc1	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Němci	Němec	k1gMnPc1	Němec
běžně	běžně	k6eAd1	běžně
falšovali	falšovat	k5eAaImAgMnP	falšovat
státní	státní	k2eAgInPc4d1	státní
dokumenty	dokument	k1gInPc4	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Teď	teď	k6eAd1	teď
ale	ale	k8xC	ale
klesli	klesnout	k5eAaPmAgMnP	klesnout
tak	tak	k6eAd1	tak
nízko	nízko	k6eAd1	nízko
<g/>
,	,	kIx,	,
že	že	k8xS	že
falšují	falšovat	k5eAaImIp3nP	falšovat
moje	můj	k3xOp1gInPc4	můj
články	článek	k1gInPc4	článek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
To	ten	k3xDgNnSc1	ten
už	už	k9	už
ovšem	ovšem	k9	ovšem
situace	situace	k1gFnSc1	situace
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
stadiu	stadion	k1gNnSc6	stadion
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kolem	kolem	k7c2	kolem
znásilňování	znásilňování	k1gNnSc2	znásilňování
byla	být	k5eAaImAgFnS	být
vyvolaná	vyvolaný	k2eAgFnSc1d1	vyvolaná
hysterie	hysterie	k1gFnSc1	hysterie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nešla	jít	k5eNaImAgFnS	jít
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
sovětští	sovětský	k2eAgMnPc1d1	sovětský
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
viděli	vidět	k5eAaImAgMnP	vidět
v	v	k7c6	v
osvobozovacích	osvobozovací	k2eAgInPc6d1	osvobozovací
bojích	boj	k1gInPc6	boj
vypálená	vypálený	k2eAgNnPc4d1	vypálené
města	město	k1gNnPc4	město
a	a	k8xC	a
vesnice	vesnice	k1gFnPc4	vesnice
<g/>
,	,	kIx,	,
znásilněné	znásilněný	k2eAgFnPc4d1	znásilněná
a	a	k8xC	a
utýrané	utýraný	k2eAgFnPc4d1	utýraná
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
nesmyslná	smyslný	k2eNgFnSc1d1	nesmyslná
jatka	jatka	k1gFnSc1	jatka
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
provést	provést	k5eAaPmF	provést
odvetu	odveta	k1gFnSc4	odveta
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
jejich	jejich	k3xOp3gNnSc4	jejich
počínání	počínání	k1gNnSc4	počínání
nejprve	nejprve	k6eAd1	nejprve
ignoroval	ignorovat	k5eAaImAgMnS	ignorovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
vydal	vydat	k5eAaPmAgInS	vydat
rozkaz	rozkaz	k1gInSc1	rozkaz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nařizoval	nařizovat	k5eAaImAgInS	nařizovat
"	"	kIx"	"
<g/>
změnit	změnit	k5eAaPmF	změnit
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
Němcům	Němec	k1gMnPc3	Němec
<g/>
...	...	k?	...
a	a	k8xC	a
chovat	chovat	k5eAaImF	chovat
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
lépe	dobře	k6eAd2	dobře
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
však	však	k9	však
začala	začít	k5eAaPmAgFnS	začít
měnit	měnit	k5eAaImF	měnit
k	k	k7c3	k
lepšímu	lepší	k1gNnSc3	lepší
až	až	k6eAd1	až
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
bojů	boj	k1gInPc2	boj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
už	už	k6eAd1	už
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
celá	celý	k2eAgFnSc1d1	celá
věc	věc	k1gFnSc1	věc
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
nepředstavitelných	představitelný	k2eNgMnPc2d1	nepředstavitelný
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
znásilňování	znásilňování	k1gNnSc2	znásilňování
byly	být	k5eAaImAgFnP	být
zejména	zejména	k9	zejména
Němky	Němka	k1gFnPc1	Němka
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
oběťmi	oběť	k1gFnPc7	oběť
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
stávaly	stávat	k5eAaImAgFnP	stávat
i	i	k9	i
ostatní	ostatní	k2eAgInPc4d1	ostatní
národy	národ	k1gInPc4	národ
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
těch	ten	k3xDgFnPc2	ten
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
dívky	dívka	k1gFnPc1	dívka
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
slovanských	slovanský	k2eAgInPc2d1	slovanský
národů	národ	k1gInPc2	národ
nuceně	nuceně	k6eAd1	nuceně
nasazené	nasazený	k2eAgNnSc1d1	nasazené
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
zjistily	zjistit	k5eAaPmAgFnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc4	jejich
národnost	národnost	k1gFnSc4	národnost
neochrání	ochránit	k5eNaPmIp3nS	ochránit
<g/>
.	.	kIx.	.
</s>
<s>
Dívky	dívka	k1gFnPc1	dívka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vzpouzely	vzpouzet	k5eAaImAgFnP	vzpouzet
i	i	k9	i
přes	přes	k7c4	přes
hrozby	hrozba	k1gFnPc4	hrozba
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
zpravidla	zpravidla	k6eAd1	zpravidla
buďto	buďto	k8xC	buďto
přemoženy	přemožen	k2eAgInPc1d1	přemožen
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dostaly	dostat	k5eAaPmAgFnP	dostat
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
mezi	mezi	k7c7	mezi
podvolením	podvolení	k1gNnSc7	podvolení
se	se	k3xPyFc4	se
a	a	k8xC	a
smrtí	smrtit	k5eAaImIp3nS	smrtit
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
například	například	k6eAd1	například
Anka	Anka	k1gFnSc1	Anka
Kolesárová	Kolesárový	k2eAgFnSc1d1	Kolesárová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
je	on	k3xPp3gNnSc4	on
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
bránit	bránit	k5eAaImF	bránit
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
platili	platit	k5eAaImAgMnP	platit
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zemřel	zemřít	k5eAaPmAgMnS	zemřít
například	například	k6eAd1	například
biskup	biskup	k1gMnSc1	biskup
Apor	Apor	k1gMnSc1	Apor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sovětští	sovětský	k2eAgMnPc1d1	sovětský
velitelé	velitel	k1gMnPc1	velitel
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
touto	tento	k3xDgFnSc7	tento
věcí	věc	k1gFnSc7	věc
nechtěli	chtít	k5eNaImAgMnP	chtít
zabývat	zabývat	k5eAaImF	zabývat
<g/>
.	.	kIx.	.
</s>
<s>
Maršál	maršál	k1gMnSc1	maršál
Vasiljevskij	Vasiljevskij	k1gMnSc1	Vasiljevskij
na	na	k7c4	na
upozornění	upozornění	k1gNnSc4	upozornění
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnPc1	jeho
muži	muž	k1gMnPc1	muž
nezřízeně	zřízeně	k6eNd1	zřízeně
znásilňují	znásilňovat	k5eAaImIp3nP	znásilňovat
a	a	k8xC	a
rabují	rabovat	k5eAaImIp3nP	rabovat
<g/>
,	,	kIx,	,
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
sere	srát	k5eAaImIp3nS	srát
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnPc1	jeho
vojáci	voják	k1gMnPc1	voják
mají	mít	k5eAaImIp3nP	mít
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
sporadicky	sporadicky	k6eAd1	sporadicky
zasáhli	zasáhnout	k5eAaPmAgMnP	zasáhnout
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
věc	věc	k1gFnSc1	věc
dle	dle	k7c2	dle
jejich	jejich	k3xOp3gInSc2	jejich
názoru	názor	k1gInSc2	názor
příliš	příliš	k6eAd1	příliš
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
také	také	k9	také
velice	velice	k6eAd1	velice
ostře	ostro	k6eAd1	ostro
reagovali	reagovat	k5eAaBmAgMnP	reagovat
na	na	k7c4	na
znásilnění	znásilnění	k1gNnSc4	znásilnění
malých	malý	k2eAgNnPc2d1	malé
děvčat	děvče	k1gNnPc2	děvče
a	a	k8xC	a
provinilce	provinilec	k1gMnSc4	provinilec
nechávali	nechávat	k5eAaImAgMnP	nechávat
okamžitě	okamžitě	k6eAd1	okamžitě
popravit	popravit	k5eAaPmF	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Dospělé	dospělý	k2eAgFnPc1d1	dospělá
ženy	žena	k1gFnPc1	žena
však	však	k9	však
měly	mít	k5eAaImAgFnP	mít
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
zpočátku	zpočátku	k6eAd1	zpočátku
<g/>
,	,	kIx,	,
takřka	takřka	k6eAd1	takřka
nulovou	nulový	k2eAgFnSc4d1	nulová
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
zastání	zastání	k1gNnSc4	zastání
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
po	po	k7c6	po
německé	německý	k2eAgFnSc6d1	německá
kapitulaci	kapitulace	k1gFnSc6	kapitulace
byla	být	k5eAaImAgNnP	být
zaváděna	zaváděn	k2eAgNnPc1d1	zaváděno
různá	různý	k2eAgNnPc1d1	různé
opatření	opatření	k1gNnPc1	opatření
a	a	k8xC	a
drakonické	drakonický	k2eAgInPc1d1	drakonický
tresty	trest	k1gInPc1	trest
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
výstřednosti	výstřednost	k1gFnPc1	výstřednost
omezily	omezit	k5eAaPmAgFnP	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
odhadů	odhad	k1gInPc2	odhad
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Německu	Německo	k1gNnSc6	Německo
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
ruských	ruský	k2eAgMnPc2d1	ruský
vojáků	voják	k1gMnPc2	voják
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
milióny	milión	k4xCgInPc7	milión
jejich	jejich	k3xOp3gMnPc2	jejich
potomků	potomek	k1gMnPc2	potomek
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
nedobrovolných	dobrovolný	k2eNgFnPc2d1	nedobrovolná
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
patrně	patrně	k6eAd1	patrně
přehnané	přehnaný	k2eAgNnSc1d1	přehnané
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
celková	celkový	k2eAgNnPc4d1	celkové
čísla	číslo	k1gNnPc4	číslo
pak	pak	k6eAd1	pak
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
se	se	k3xPyFc4	se
usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
celkem	celkem	k6eAd1	celkem
byly	být	k5eAaImAgFnP	být
znásilněny	znásilněn	k2eAgInPc1d1	znásilněn
asi	asi	k9	asi
2	[number]	k4	2
miliony	milion	k4xCgInPc7	milion
německých	německý	k2eAgFnPc2d1	německá
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
podstatná	podstatný	k2eAgFnSc1d1	podstatná
menšina	menšina	k1gFnSc1	menšina
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
-li	i	k?	-li
většina	většina	k1gFnSc1	většina
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
znásilněna	znásilnit	k5eAaPmNgFnS	znásilnit
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Maďarská	maďarský	k2eAgFnSc1d1	maďarská
revoluce	revoluce	k1gFnSc1	revoluce
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
výboru	výbor	k1gInSc2	výbor
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
o	o	k7c6	o
problému	problém	k1gInSc6	problém
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Sovětské	sovětský	k2eAgInPc1d1	sovětský
tanky	tank	k1gInPc1	tank
střílely	střílet	k5eAaImAgInP	střílet
bez	bez	k7c2	bez
rozmyslu	rozmysl	k1gInSc2	rozmysl
na	na	k7c4	na
každou	každý	k3xTgFnSc4	každý
budovu	budova	k1gFnSc4	budova
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
předpokládaly	předpokládat	k5eAaImAgFnP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
pod	pod	k7c7	pod
palbou	palba	k1gFnSc7	palba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Komise	komise	k1gFnSc1	komise
OSN	OSN	kA	OSN
obdržela	obdržet	k5eAaPmAgFnS	obdržet
četné	četný	k2eAgFnPc4d1	četná
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
sovětské	sovětský	k2eAgFnSc6d1	sovětská
minometné	minometný	k2eAgFnSc6d1	minometná
a	a	k8xC	a
dělostřelecké	dělostřelecký	k2eAgFnSc6d1	dělostřelecká
palbě	palba	k1gFnSc6	palba
do	do	k7c2	do
obydlených	obydlený	k2eAgFnPc2d1	obydlená
čtvrtí	čtvrt	k1gFnPc2	čtvrt
v	v	k7c6	v
Budíně	Budín	k1gInSc6	Budín
(	(	kIx(	(
<g/>
navzdory	navzdory	k7c3	navzdory
žádné	žádný	k3yNgFnSc3	žádný
opětované	opětovaný	k2eAgFnSc3d1	opětovaná
palbě	palba	k1gFnSc3	palba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
"	"	kIx"	"
<g/>
nahodilé	nahodilý	k2eAgFnSc6d1	nahodilá
střelbě	střelba	k1gFnSc6	střelba
do	do	k7c2	do
bezbranných	bezbranný	k2eAgMnPc2d1	bezbranný
kolemjdoucích	kolemjdoucí	k1gMnPc2	kolemjdoucí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
mnoha	mnoho	k4c2	mnoho
svědků	svědek	k1gMnPc2	svědek
sovětští	sovětský	k2eAgMnPc1d1	sovětský
vojáci	voják	k1gMnPc1	voják
stříleli	střílet	k5eAaImAgMnP	střílet
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
ve	v	k7c6	v
frontách	fronta	k1gFnPc6	fronta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vedly	vést	k5eAaImAgFnP	vést
z	z	k7c2	z
prodejen	prodejna	k1gFnPc2	prodejna
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
obětí	oběť	k1gFnPc2	oběť
byly	být	k5eAaImAgFnP	být
ženy	žena	k1gFnPc1	žena
a	a	k8xC	a
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Československo	Československo	k1gNnSc1	Československo
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
Invaze	invaze	k1gFnSc2	invaze
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
72	[number]	k4	72
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
(	(	kIx(	(
<g/>
19	[number]	k4	19
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
266	[number]	k4	266
těžce	těžce	k6eAd1	těžce
zraněno	zranit	k5eAaPmNgNnS	zranit
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
436	[number]	k4	436
zraněno	zranit	k5eAaPmNgNnS	zranit
lehce	lehko	k6eAd1	lehko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
četné	četný	k2eAgFnPc1d1	četná
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c4	o
použití	použití	k1gNnSc4	použití
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
během	během	k7c2	během
sovětské	sovětský	k2eAgFnSc2d1	sovětská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
proti	proti	k7c3	proti
civilistům	civilista	k1gMnPc3	civilista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Napětí	napětí	k1gNnSc1	napětí
v	v	k7c6	v
Ázerbájdžánu	Ázerbájdžán	k1gInSc6	Ázerbájdžán
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Černý	černý	k2eAgInSc1d1	černý
leden	leden	k1gInSc1	leden
<g/>
,	,	kIx,	,
také	také	k9	také
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
černá	černat	k5eAaImIp3nS	černat
sobotu	sobota	k1gFnSc4	sobota
nebo	nebo	k8xC	nebo
lednový	lednový	k2eAgInSc4d1	lednový
masakr	masakr	k1gInSc4	masakr
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
násilný	násilný	k2eAgInSc1d1	násilný
zásah	zásah	k1gInSc1	zásah
v	v	k7c6	v
Baku	Baku	k1gNnSc6	Baku
19	[number]	k4	19
<g/>
.	.	kIx.	.
až	až	k9	až
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1990	[number]	k4	1990
za	za	k7c2	za
výjimečného	výjimečný	k2eAgInSc2d1	výjimečný
stavu	stav	k1gInSc2	stav
během	během	k7c2	během
doby	doba	k1gFnSc2	doba
rozpadu	rozpad	k1gInSc2	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
po	po	k7c6	po
pogromech	pogrom	k1gInPc6	pogrom
a	a	k8xC	a
násilí	násilí	k1gNnSc1	násilí
proti	proti	k7c3	proti
Arménům	Armén	k1gMnPc3	Armén
v	v	k7c6	v
Baku	Baku	k1gNnSc6	Baku
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
usnesení	usnesení	k1gNnSc6	usnesení
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
Ázerbájdžánské	ázerbájdžánský	k2eAgFnSc2d1	Ázerbájdžánská
SSR	SSR	kA	SSR
ze	z	k7c2	z
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1990	[number]	k4	1990
se	se	k3xPyFc4	se
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyhláška	vyhláška	k1gFnSc1	vyhláška
prezidia	prezidium	k1gNnSc2	prezidium
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
SSSR	SSSR	kA	SSSR
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
výjimečného	výjimečný	k2eAgInSc2d1	výjimečný
stavu	stav	k1gInSc2	stav
v	v	k7c6	v
Baku	Baku	k1gNnSc6	Baku
a	a	k8xC	a
vojenské	vojenský	k2eAgNnSc1d1	vojenské
nasazení	nasazení	k1gNnSc1	nasazení
představovalo	představovat	k5eAaImAgNnS	představovat
akt	akt	k1gInSc4	akt
agrese	agrese	k1gFnSc2	agrese
<g/>
.	.	kIx.	.
</s>
<s>
Černý	černý	k2eAgInSc1d1	černý
leden	leden	k1gInSc1	leden
je	být	k5eAaImIp3nS	být
spojován	spojovat	k5eAaImNgInS	spojovat
se	s	k7c7	s
znovuzrozením	znovuzrození	k1gNnSc7	znovuzrození
Ázerbájdžánské	ázerbájdžánský	k2eAgFnSc2d1	Ázerbájdžánská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
událostí	událost	k1gFnPc2	událost
během	během	k7c2	během
éry	éra	k1gFnSc2	éra
glasnosti	glasnost	k1gFnSc2	glasnost
a	a	k8xC	a
perestrojky	perestrojka	k1gFnSc2	perestrojka
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
SSSR	SSSR	kA	SSSR
proti	proti	k7c3	proti
demonstrantům	demonstrant	k1gMnPc3	demonstrant
využil	využít	k5eAaPmAgMnS	využít
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Zločiny	zločin	k1gInPc4	zločin
Německa	Německo	k1gNnSc2	Německo
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
</s>
</p>
<p>
<s>
Japonské	japonský	k2eAgInPc1d1	japonský
válečné	válečný	k2eAgInPc1d1	válečný
zločiny	zločin	k1gInPc1	zločin
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Gerd	Gerd	k1gMnSc1	Gerd
Kaiser	Kaiser	k1gMnSc1	Kaiser
<g/>
:	:	kIx,	:
Katyň	Katyně	k1gFnPc2	Katyně
<g/>
:	:	kIx,	:
Státní	státní	k2eAgInSc1d1	státní
zločin	zločin	k1gInSc1	zločin
-	-	kIx~	-
státní	státní	k2eAgNnSc1d1	státní
tajemství	tajemství	k1gNnSc1	tajemství
-	-	kIx~	-
kniha	kniha	k1gFnSc1	kniha
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
fotodokumentací	fotodokumentace	k1gFnSc7	fotodokumentace
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
Katyňskému	Katyňský	k2eAgNnSc3d1	Katyňský
masakru	masakr	k1gInSc6	masakr
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Kubík	Kubík	k1gMnSc1	Kubík
<g/>
:	:	kIx,	:
Rudé	rudý	k2eAgInPc1d1	rudý
stíny	stín	k1gInPc1	stín
<g/>
:	:	kIx,	:
<g/>
.	.	kIx.	.
</s>
<s>
Sovětské	sovětský	k2eAgInPc1d1	sovětský
válečné	válečný	k2eAgInPc1d1	válečný
zločiny	zločin	k1gInPc1	zločin
1941-1945	[number]	k4	1941-1945
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
:	:	kIx,	:
Zločin	zločin	k1gInSc1	zločin
genocidy	genocida	k1gFnSc2	genocida
<g/>
.	.	kIx.	.
</s>
<s>
Litva	Litva	k1gFnSc1	Litva
v	v	k7c6	v
drtivém	drtivý	k2eAgInSc6d1	drtivý
obětí	oběť	k1gFnPc2	oběť
velkého	velký	k2eAgMnSc2d1	velký
bratra	bratr	k1gMnSc2	bratr
SSSR	SSSR	kA	SSSR
-	-	kIx~	-
popisuje	popisovat	k5eAaImIp3nS	popisovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
chování	chování	k1gNnSc4	chování
sovětských	sovětský	k2eAgFnPc2d1	sovětská
okupačních	okupační	k2eAgFnPc2d1	okupační
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
a	a	k8xC	a
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
vraždění	vraždění	k1gNnSc4	vraždění
civilistů	civilista	k1gMnPc2	civilista
a	a	k8xC	a
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
při	při	k7c6	při
ústupu	ústup	k1gInSc6	ústup
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
</s>
</p>
<p>
<s>
Antony	anton	k1gInPc1	anton
Beevor	Beevora	k1gFnPc2	Beevora
<g/>
:	:	kIx,	:
Berlín	Berlín	k1gInSc1	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Okamžiky	okamžik	k1gInPc1	okamžik
zkázy	zkáza	k1gFnSc2	zkáza
1945	[number]	k4	1945
-	-	kIx~	-
kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
popis	popis	k1gInSc4	popis
bojů	boj	k1gInPc2	boj
věnuje	věnovat	k5eAaImIp3nS	věnovat
také	také	k9	také
chování	chování	k1gNnSc4	chování
vítězné	vítězný	k2eAgFnSc2d1	vítězná
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
rabování	rabování	k1gNnSc1	rabování
a	a	k8xC	a
znásilňování	znásilňování	k1gNnSc1	znásilňování
</s>
</p>
<p>
<s>
Anonyma	anonym	k1gMnSc4	anonym
<g/>
:	:	kIx,	:
Žena	žena	k1gFnSc1	žena
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
-	-	kIx~	-
svědectví	svědectví	k1gNnSc4	svědectví
Berlíňanky	Berlíňanka	k1gFnSc2	Berlíňanka
o	o	k7c6	o
osudech	osud	k1gInPc6	osud
německých	německý	k2eAgFnPc2d1	německá
žen	žena	k1gFnPc2	žena
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Berlína	Berlín	k1gInSc2	Berlín
</s>
</p>
<p>
<s>
Raymond	Raymond	k1gMnSc1	Raymond
F.	F.	kA	F.
Toliver	Toliver	k1gMnSc1	Toliver
<g/>
,	,	kIx,	,
Trevor	Trevor	k1gMnSc1	Trevor
J.	J.	kA	J.
Constable	Constable	k1gFnSc1	Constable
<g/>
:	:	kIx,	:
Sundejte	sundat	k5eAaPmRp2nP	sundat
Hartmanna	Hartmann	k1gMnSc4	Hartmann
<g/>
!	!	kIx.	!
</s>
<s>
-	-	kIx~	-
kniha	kniha	k1gFnSc1	kniha
popisuje	popisovat	k5eAaImIp3nS	popisovat
nejen	nejen	k6eAd1	nejen
válečné	válečný	k2eAgNnSc4d1	válečné
období	období	k1gNnSc4	období
nejúspěšnějšího	úspěšný	k2eAgNnSc2d3	nejúspěšnější
štíhače	štíhač	k1gMnPc4	štíhač
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jeho	jeho	k3xOp3gNnSc4	jeho
zajatecké	zajatecký	k2eAgNnSc4d1	zajatecké
období	období	k1gNnSc4	období
</s>
</p>
