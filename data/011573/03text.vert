<p>
<s>
Frank	Frank	k1gMnSc1	Frank
Vickery	Vickera	k1gFnSc2	Vickera
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1951	[number]	k4	1951
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
velšský	velšský	k2eAgMnSc1d1	velšský
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Blaencwm	Blaencwma	k1gFnPc2	Blaencwma
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Walesu	Wales	k1gInSc6	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
přibližně	přibližně	k6eAd1	přibližně
tří	tři	k4xCgFnPc2	tři
desítek	desítka	k1gFnPc2	desítka
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
pro	pro	k7c4	pro
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
pro	pro	k7c4	pro
rozhlas	rozhlas	k1gInSc4	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
hrách	hra	k1gFnPc6	hra
někdy	někdy	k6eAd1	někdy
sám	sám	k3xTgInSc4	sám
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
uvedl	uvést	k5eAaPmAgMnS	uvést
jednoaktovou	jednoaktový	k2eAgFnSc4d1	jednoaktová
hru	hra	k1gFnSc4	hra
After	After	k1gMnSc1	After
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Gone	Gone	k1gInSc1	Gone
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
muzikál	muzikál	k1gInSc1	muzikál
Amazing	Amazing	k1gInSc1	Amazing
Grace	Grace	k1gFnSc1	Grace
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Malem	Mal	k1gMnSc7	Mal
Popem	pop	k1gMnSc7	pop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Frank	Frank	k1gMnSc1	Frank
Vickery	Vickera	k1gFnSc2	Vickera
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
