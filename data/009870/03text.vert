<p>
<s>
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc1	Brod
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Tel	tel	kA	tel
Aviv	Aviv	k1gInSc1	Aviv
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
pražský	pražský	k2eAgInSc1d1	pražský
<g/>
,	,	kIx,	,
židovský	židovský	k2eAgInSc1d1	židovský
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
píšící	píšící	k2eAgMnSc1d1	píšící
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Pražského	pražský	k2eAgInSc2d1	pražský
kruhu	kruh	k1gInSc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
publikování	publikování	k1gNnSc4	publikování
literární	literární	k2eAgFnSc2d1	literární
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
a	a	k8xC	a
významnou	významný	k2eAgFnSc7d1	významná
měrou	míra	k1gFnSc7wR	míra
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
celosvětové	celosvětový	k2eAgFnSc3d1	celosvětová
proslulosti	proslulost	k1gFnSc3	proslulost
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
šíření	šíření	k1gNnSc4	šíření
české	český	k2eAgFnSc2d1	Česká
kultury	kultura	k1gFnSc2	kultura
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
díla	dílo	k1gNnSc2	dílo
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
a	a	k8xC	a
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Haška	Hašek	k1gMnSc2	Hašek
<g/>
)	)	kIx)	)
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc4	Brod
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
Adolfa	Adolf	k1gMnSc2	Adolf
Broda	Brod	k1gMnSc2	Brod
<g/>
,	,	kIx,	,
ředitele	ředitel	k1gMnSc2	ředitel
banky	banka	k1gFnSc2	banka
Union	union	k1gInSc1	union
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
ženy	žena	k1gFnPc1	žena
Františky	Františka	k1gFnSc2	Františka
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnSc2d1	rozená
Rosenfeldové	Rosenfeldová	k1gFnSc2	Rosenfeldová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
ho	on	k3xPp3gMnSc4	on
postihlo	postihnout	k5eAaPmAgNnS	postihnout
vážné	vážný	k2eAgNnSc1d1	vážné
onemocnění	onemocnění	k1gNnSc1	onemocnění
páteře	páteř	k1gFnSc2	páteř
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
ještě	ještě	k6eAd1	ještě
mladšího	mladý	k2eAgMnSc4d2	mladší
bratra	bratr	k1gMnSc4	bratr
Ottu	Otta	k1gMnSc4	Otta
Broda	Brod	k1gMnSc4	Brod
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Osvětimi	Osvětim	k1gFnSc6	Osvětim
<g/>
.	.	kIx.	.
<g/>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
uznávaném	uznávaný	k2eAgNnSc6d1	uznávané
Německém	německý	k2eAgNnSc6d1	německé
státním	státní	k2eAgNnSc6d1	státní
reálném	reálný	k2eAgNnSc6d1	reálné
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
práva	právo	k1gNnPc4	právo
na	na	k7c6	na
Německé	německý	k2eAgFnSc6d1	německá
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
doktorát	doktorát	k1gInSc4	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
první	první	k4xOgFnPc1	první
literární	literární	k2eAgFnPc1d1	literární
práce	práce	k1gFnPc1	práce
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
již	již	k6eAd1	již
v	v	k7c6	v
první	první	k4xOgFnSc6	první
dekádě	dekáda	k1gFnSc6	dekáda
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
psaní	psaní	k1gNnSc6	psaní
prózy	próza	k1gFnSc2	próza
i	i	k8xC	i
literárních	literární	k2eAgFnPc2d1	literární
studií	studie	k1gFnPc2	studie
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Zásadním	zásadní	k2eAgInSc7d1	zásadní
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c6	o
publikaci	publikace	k1gFnSc6	publikace
děl	dělo	k1gNnPc2	dělo
svého	svůj	k3xOyFgMnSc4	svůj
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
přítele	přítel	k1gMnSc2	přítel
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
připravil	připravit	k5eAaPmAgInS	připravit
také	také	k9	také
Kafkovy	Kafkův	k2eAgInPc4d1	Kafkův
deníky	deník	k1gInPc4	deník
a	a	k8xC	a
korespondenci	korespondence	k1gFnSc4	korespondence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
byl	být	k5eAaImAgInS	být
Brod	Brod	k1gInSc4	Brod
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
jako	jako	k9	jako
úředník	úředník	k1gMnSc1	úředník
(	(	kIx(	(
<g/>
koncipista	koncipista	k1gMnSc1	koncipista
<g/>
)	)	kIx)	)
u	u	k7c2	u
Pražského	pražský	k2eAgNnSc2d1	Pražské
poštovního	poštovní	k2eAgNnSc2d1	poštovní
ředitelství	ředitelství	k1gNnSc2	ředitelství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
zaměstnání	zaměstnání	k1gNnSc6	zaměstnání
jej	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc4	jeho
přítele	přítel	k1gMnSc4	přítel
<g/>
,	,	kIx,	,
Franze	Franze	k1gFnSc1	Franze
Kafku	Kafka	k1gMnSc4	Kafka
<g/>
,	,	kIx,	,
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
především	především	k9	především
relativně	relativně	k6eAd1	relativně
kratší	krátký	k2eAgFnSc1d2	kratší
pracovní	pracovní	k2eAgFnSc1d1	pracovní
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
mu	on	k3xPp3gMnSc3	on
umožnila	umožnit	k5eAaPmAgFnS	umožnit
věnovat	věnovat	k5eAaPmF	věnovat
se	se	k3xPyFc4	se
i	i	k9	i
literární	literární	k2eAgFnSc3d1	literární
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Pražském	pražský	k2eAgNnSc6d1	Pražské
poštovním	poštovní	k2eAgNnSc6d1	poštovní
ředitelství	ředitelství	k1gNnSc6	ředitelství
pracoval	pracovat	k5eAaImAgMnS	pracovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
kulturním	kulturní	k2eAgMnSc7d1	kulturní
referentem	referent	k1gMnSc7	referent
československé	československý	k2eAgFnSc2d1	Československá
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1929	[number]	k4	1929
až	až	k9	až
1939	[number]	k4	1939
byl	být	k5eAaImAgInS	být
hudebním	hudební	k2eAgMnSc7d1	hudební
a	a	k8xC	a
divadelním	divadelní	k2eAgMnSc7d1	divadelní
kritikem	kritik	k1gMnSc7	kritik
pražských	pražský	k2eAgFnPc2d1	Pražská
německých	německý	k2eAgFnPc2d1	německá
novin	novina	k1gFnPc2	novina
Prager	Prager	k1gMnSc1	Prager
Tagblatt	Tagblatt	k1gMnSc1	Tagblatt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
začal	začít	k5eAaPmAgInS	začít
silně	silně	k6eAd1	silně
pociťovat	pociťovat	k5eAaImF	pociťovat
své	svůj	k3xOyFgInPc4	svůj
židovské	židovský	k2eAgInPc4d1	židovský
kořeny	kořen	k1gInPc4	kořen
a	a	k8xC	a
živě	živě	k6eAd1	živě
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
sionistické	sionistický	k2eAgNnSc4d1	sionistické
hnutí	hnutí	k1gNnSc4	hnutí
Theodora	Theodor	k1gMnSc2	Theodor
Herzla	Herzla	k1gMnSc2	Herzla
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ustavení	ustavení	k1gNnSc6	ustavení
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Československa	Československo	k1gNnSc2	Československo
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
Židovské	židovský	k2eAgFnSc2d1	židovská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1913	[number]	k4	1913
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
o	o	k7c4	o
rok	rok	k1gInSc4	rok
starší	starý	k2eAgInSc4d2	starší
Elsou	Elsa	k1gFnSc7	Elsa
Taussigovou	Taussigův	k2eAgFnSc7d1	Taussigova
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
24.8	[number]	k4	24.8
<g/>
.1883	.1883	k4	.1883
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
Eduarda	Eduard	k1gMnSc2	Eduard
Taussiga	Taussig	k1gMnSc2	Taussig
a	a	k8xC	a
Hermíny	Hermína	k1gFnSc2	Hermína
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Wahleové	Wahleová	k1gFnPc1	Wahleová
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
emigrovali	emigrovat	k5eAaBmAgMnP	emigrovat
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Elsou	Elsa	k1gFnSc7	Elsa
do	do	k7c2	do
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
Tel	tel	kA	tel
Avivu	Aviva	k1gFnSc4	Aviva
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xS	jako
divadelní	divadelní	k2eAgMnSc1d1	divadelní
dramaturg	dramaturg	k1gMnSc1	dramaturg
a	a	k8xC	a
kritik	kritik	k1gMnSc1	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
krátce	krátce	k6eAd1	krátce
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zahájil	zahájit	k5eAaPmAgMnS	zahájit
kafkovskou	kafkovský	k2eAgFnSc4d1	kafkovská
výstavu	výstava	k1gFnSc4	výstava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc2	Aviv
–	–	k?	–
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
ve	v	k7c6	v
státu	stát	k1gInSc6	stát
Izrael	Izrael	k1gInSc1	Izrael
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pražské	pražský	k2eAgFnSc2d1	Pražská
adresy	adresa	k1gFnSc2	adresa
===	===	k?	===
</s>
</p>
<p>
<s>
1884	[number]	k4	1884
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
Haštalská	Haštalský	k2eAgFnSc1d1	Haštalská
čp.	čp.	k?	čp.
1031	[number]	k4	1031
<g/>
/	/	kIx~	/
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
budově	budova	k1gFnSc6	budova
odhalena	odhalen	k2eAgFnSc1d1	odhalena
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
textem	text	k1gInSc7	text
27.5	[number]	k4	27.5
<g/>
.1884	.1884	k4	.1884
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
propagátor	propagátor	k1gMnSc1	propagátor
české	český	k2eAgFnSc2d1	Česká
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
přítel	přítel	k1gMnSc1	přítel
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc2	Aviv
20.12	[number]	k4	20.12
<g/>
.1968	.1968	k4	.1968
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostního	slavnostní	k2eAgInSc2d1	slavnostní
aktu	akt	k1gInSc2	akt
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
a	a	k8xC	a
velvyslanci	velvyslanec	k1gMnPc1	velvyslanec
Izraele	Izrael	k1gInSc2	Izrael
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
<g/>
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
–	–	k?	–
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
?	?	kIx.	?
</s>
<s>
:	:	kIx,	:
Piaristická	piaristický	k2eAgFnSc1d1	piaristická
kolej	kolej	k1gFnSc1	kolej
<g/>
,	,	kIx,	,
Panská	panská	k1gFnSc1	panská
</s>
</p>
<p>
<s>
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
?	?	kIx.	?
</s>
<s>
<g/>
–	–	k?	–
<g/>
1902	[number]	k4	1902
<g/>
:	:	kIx,	:
Německé	německý	k2eAgNnSc1d1	německé
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
Štěpánská	štěpánský	k2eAgFnSc1d1	Štěpánská
–	–	k?	–
zde	zde	k6eAd1	zde
maturoval	maturovat	k5eAaBmAgMnS	maturovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
–	–	k?	–
1902	[number]	k4	1902
<g/>
–	–	k?	–
<g/>
1907	[number]	k4	1907
<g/>
:	:	kIx,	:
Německá	německý	k2eAgFnSc1d1	německá
Karlo-Ferdinandova	Karlo-Ferdinandův	k2eAgFnSc1d1	Karlo-Ferdinandova
univerzita	univerzita	k1gFnSc1	univerzita
–	–	k?	–
studium	studium	k1gNnSc1	studium
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
seznámil	seznámit	k5eAaPmAgInS	seznámit
s	s	k7c7	s
Franzem	Franz	k1gMnSc7	Franz
Kafkou	Kafka	k1gMnSc7	Kafka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
:	:	kIx,	:
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
Mladých	mladý	k2eAgMnPc2d1	mladý
Goliášů	Goliáš	k1gMnPc2	Goliáš
<g/>
,	,	kIx,	,
Skořepka	skořepka	k1gFnSc1	skořepka
čp.	čp.	k?	čp.
527	[number]	k4	527
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
–	–	k?	–
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1912	[number]	k4	1912
seznámil	seznámit	k5eAaPmAgInS	seznámit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
budoucí	budoucí	k2eAgFnSc7d1	budoucí
snoubenkou	snoubenka	k1gFnSc7	snoubenka
Felice	Felice	k1gFnSc1	Felice
Bauer	Bauer	k1gMnSc1	Bauer
</s>
</p>
<p>
<s>
<g/>
:	:	kIx,	:
Skořepka	skořepka	k1gFnSc1	skořepka
čp.	čp.	k?	čp.
1056	[number]	k4	1056
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1913	[number]	k4	1913
<g/>
:	:	kIx,	:
Biskupská	biskupský	k2eAgFnSc1d1	biskupská
čp.	čp.	k?	čp.
1065	[number]	k4	1065
</s>
</p>
<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1914	[number]	k4	1914
<g/>
:	:	kIx,	:
Elišky	Eliška	k1gFnSc2	Eliška
Krásnohorské	krásnohorský	k2eAgFnSc2d1	Krásnohorská
čp.	čp.	k?	čp.
897	[number]	k4	897
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
:	:	kIx,	:
Břehová	břehový	k2eAgFnSc1d1	Břehová
čp.	čp.	k?	čp.
208	[number]	k4	208
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literární	literární	k2eAgFnSc4d1	literární
a	a	k8xC	a
publikační	publikační	k2eAgFnSc4d1	publikační
činnost	činnost	k1gFnSc4	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Kafky	Kafka	k1gMnSc2	Kafka
byl	být	k5eAaImAgInS	být
Brod	Brod	k1gInSc1	Brod
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
autorem	autor	k1gMnSc7	autor
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
první	první	k4xOgInSc1	první
román	román	k1gInSc1	román
a	a	k8xC	a
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
již	již	k6eAd1	již
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
Schloss	Schloss	k1gInSc1	Schloss
Nornepygge	Nornepygg	k1gFnSc2	Nornepygg
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Zámek	zámek	k1gInSc1	zámek
Nornepygge	Nornepygge	k1gNnSc2	Nornepygge
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydaný	vydaný	k2eAgInSc1d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
Brodovi	Brodův	k2eAgMnPc1d1	Brodův
dvacet	dvacet	k4xCc4	dvacet
čtyři	čtyři	k4xCgInPc4	čtyři
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
veřejností	veřejnost	k1gFnSc7	veřejnost
nadšeně	nadšeně	k6eAd1	nadšeně
přijat	přijmout	k5eAaPmNgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
centru	centrum	k1gNnSc6	centrum
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
německé	německý	k2eAgFnSc2d1	německá
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
kniha	kniha	k1gFnSc1	kniha
vysoce	vysoce	k6eAd1	vysoce
oceněna	ocenit	k5eAaPmNgFnS	ocenit
jako	jako	k8xS	jako
významný	významný	k2eAgInSc4d1	významný
příklad	příklad	k1gInSc4	příklad
expresionismu	expresionismus	k1gInSc2	expresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
knihu	kniha	k1gFnSc4	kniha
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
román	román	k1gInSc1	román
Wachposten	Wachposten	k2eAgInSc1d1	Wachposten
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Hlídka	hlídka	k1gFnSc1	hlídka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
spoluautor	spoluautor	k1gMnSc1	spoluautor
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
samotný	samotný	k2eAgMnSc1d1	samotný
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
další	další	k2eAgNnPc4d1	další
díla	dílo	k1gNnPc4	dílo
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
velký	velký	k2eAgInSc4d1	velký
ohlas	ohlas	k1gInSc4	ohlas
a	a	k8xC	a
Brod	Brod	k1gInSc1	Brod
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
jazykových	jazykový	k2eAgFnPc6d1	jazyková
oblastech	oblast	k1gFnPc6	oblast
proslavil	proslavit	k5eAaPmAgMnS	proslavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
Franzem	Franz	k1gMnSc7	Franz
Kafkou	Kafka	k1gMnSc7	Kafka
se	se	k3xPyFc4	se
Brod	Brod	k1gInSc1	Brod
setkal	setkat	k5eAaPmAgInS	setkat
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
studentském	studentský	k2eAgInSc6d1	studentský
spolku	spolek	k1gInSc6	spolek
přednášel	přednášet	k5eAaImAgMnS	přednášet
o	o	k7c6	o
Arthuru	Arthur	k1gMnSc6	Arthur
Schopenhauerovi	Schopenhauer	k1gMnSc6	Schopenhauer
<g/>
.	.	kIx.	.
</s>
<s>
Brod	Brod	k1gInSc1	Brod
a	a	k8xC	a
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
rovněž	rovněž	k9	rovněž
student	student	k1gMnSc1	student
Německé	německý	k2eAgFnSc2d1	německá
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
spřátelili	spřátelit	k5eAaPmAgMnP	spřátelit
a	a	k8xC	a
scházeli	scházet	k5eAaImAgMnP	scházet
se	se	k3xPyFc4	se
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
návštěvě	návštěva	k1gFnSc6	návštěva
u	u	k7c2	u
Maxe	Max	k1gMnSc2	Max
Broda	Brod	k1gMnSc2	Brod
se	se	k3xPyFc4	se
Kafka	Kafka	k1gMnSc1	Kafka
také	také	k9	také
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
příbuznou	příbuzná	k1gFnSc7	příbuzná
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
pozdější	pozdní	k2eAgFnSc7d2	pozdější
snoubenkou	snoubenka	k1gFnSc7	snoubenka
Felice	Felice	k1gFnSc2	Felice
Bauerovou	Bauerová	k1gFnSc7	Bauerová
<g/>
.	.	kIx.	.
</s>
<s>
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc4	Brod
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
nejbližším	blízký	k2eAgFnPc3d3	nejbližší
osobám	osoba	k1gFnPc3	osoba
Kafkova	Kafkův	k2eAgInSc2d1	Kafkův
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gMnSc3	on
oporou	opora	k1gFnSc7	opora
<g/>
,	,	kIx,	,
povzbuzoval	povzbuzovat	k5eAaImAgMnS	povzbuzovat
jej	on	k3xPp3gNnSc4	on
v	v	k7c6	v
literární	literární	k2eAgFnSc6d1	literární
tvorbě	tvorba	k1gFnSc6	tvorba
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
mírnit	mírnit	k5eAaImF	mírnit
jeho	jeho	k3xOp3gFnPc4	jeho
duševní	duševní	k2eAgFnPc4d1	duševní
krize	krize	k1gFnPc4	krize
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
mu	on	k3xPp3gMnSc3	on
publikovat	publikovat	k5eAaBmF	publikovat
jeho	jeho	k3xOp3gInPc4	jeho
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Kafka	Kafka	k1gMnSc1	Kafka
a	a	k8xC	a
Brod	Brod	k1gInSc1	Brod
zůstali	zůstat	k5eAaPmAgMnP	zůstat
blízkými	blízký	k2eAgMnPc7d1	blízký
přáteli	přítel	k1gMnPc7	přítel
až	až	k6eAd1	až
do	do	k7c2	do
Kafkovy	Kafkův	k2eAgFnSc2d1	Kafkova
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
Kafka	Kafka	k1gMnSc1	Kafka
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
ustavil	ustavit	k5eAaPmAgMnS	ustavit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
závěti	závěť	k1gFnSc6	závěť
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
dosud	dosud	k6eAd1	dosud
nepublikovaná	publikovaný	k2eNgNnPc1d1	nepublikované
díla	dílo	k1gNnPc1	dílo
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
zničena	zničit	k5eAaPmNgFnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Vykonavatelem	vykonavatel	k1gMnSc7	vykonavatel
poslední	poslední	k2eAgFnSc2d1	poslední
vůle	vůle	k1gFnSc2	vůle
určil	určit	k5eAaPmAgMnS	určit
Broda	Broda	k1gMnSc1	Broda
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
Kafkovu	Kafkův	k2eAgFnSc4d1	Kafkova
pozůstalost	pozůstalost	k1gFnSc4	pozůstalost
vydat	vydat	k5eAaPmF	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
začal	začít	k5eAaPmAgInS	začít
Brod	Brod	k1gInSc1	Brod
publikovat	publikovat	k5eAaBmF	publikovat
Kafkova	Kafkův	k2eAgNnPc4d1	Kafkovo
díla	dílo	k1gNnPc4	dílo
z	z	k7c2	z
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
romány	román	k1gInPc1	román
Amerika	Amerika	k1gFnSc1	Amerika
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Das	Das	k1gMnSc1	Das
Schloss	Schloss	k1gInSc1	Schloss
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Zámek	zámek	k1gInSc1	zámek
<g/>
)	)	kIx)	)
či	či	k8xC	či
Der	drát	k5eAaImRp2nS	drát
Prozess	Prozess	k1gInSc1	Prozess
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Proces	proces	k1gInSc1	proces
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
třech	tři	k4xCgNnPc6	tři
desetiletích	desetiletí	k1gNnPc6	desetiletí
vydal	vydat	k5eAaPmAgInS	vydat
šest	šest	k4xCc4	šest
svazků	svazek	k1gInPc2	svazek
Kafkovy	Kafkův	k2eAgFnSc2d1	Kafkova
tvorby	tvorba	k1gFnSc2	tvorba
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
biografii	biografie	k1gFnSc4	biografie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
Kafky	Kafka	k1gMnSc2	Kafka
stál	stát	k5eAaImAgMnS	stát
Brod	Brod	k1gInSc4	Brod
i	i	k9	i
za	za	k7c7	za
Franzem	Franz	k1gMnSc7	Franz
Werfelem	Werfel	k1gMnSc7	Werfel
a	a	k8xC	a
Karlem	Karel	k1gMnSc7	Karel
Krausem	Kraus	k1gMnSc7	Kraus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
neodklonili	odklonit	k5eNaPmAgMnP	odklonit
od	od	k7c2	od
judaismu	judaismus	k1gInSc2	judaismus
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
kulturu	kultura	k1gFnSc4	kultura
je	být	k5eAaImIp3nS	být
Brod	Brod	k1gInSc1	Brod
významný	významný	k2eAgInSc1d1	významný
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
přičinil	přičinit	k5eAaPmAgMnS	přičinit
o	o	k7c4	o
divadelní	divadelní	k2eAgNnSc4d1	divadelní
uvedení	uvedení	k1gNnSc4	uvedení
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
Haškova	Haškův	k2eAgMnSc4d1	Haškův
Dobrého	dobrý	k2eAgMnSc4d1	dobrý
vojáka	voják	k1gMnSc4	voják
Švejka	Švejk	k1gMnSc4	Švejk
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
Janáčkovy	Janáčkův	k2eAgFnSc2d1	Janáčkova
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
přeložil	přeložit	k5eAaPmAgMnS	přeložit
do	do	k7c2	do
němčiny	němčina	k1gFnSc2	němčina
libreta	libreto	k1gNnSc2	libreto
Janáčkových	Janáčkových	k2eAgFnPc2d1	Janáčkových
oper	opera	k1gFnPc2	opera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Hudební	hudební	k2eAgNnSc1d1	hudební
dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
šesti	šest	k4xCc2	šest
let	léto	k1gNnPc2	léto
hrál	hrát	k5eAaImAgMnS	hrát
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc4	Brod
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Hudebně	hudebně	k6eAd1	hudebně
jej	on	k3xPp3gInSc4	on
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
houslista	houslista	k1gMnSc1	houslista
Adolf	Adolf	k1gMnSc1	Adolf
Schreiber	Schreiber	k1gMnSc1	Schreiber
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
též	též	k9	též
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Brod	Brod	k1gInSc1	Brod
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
hudebním	hudební	k2eAgMnSc7d1	hudební
skladatelem	skladatel	k1gMnSc7	skladatel
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
38	[number]	k4	38
hudebních	hudební	k2eAgNnPc2d1	hudební
děl	dělo	k1gNnPc2	dělo
či	či	k8xC	či
cyklů	cyklus	k1gInPc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
především	především	k9	především
písně	píseň	k1gFnPc1	píseň
a	a	k8xC	a
cykly	cyklus	k1gInPc1	cyklus
písní	píseň	k1gFnPc2	píseň
na	na	k7c4	na
texty	text	k1gInPc4	text
Heinricha	Heinrich	k1gMnSc2	Heinrich
Heineho	Heine	k1gMnSc2	Heine
<g/>
,	,	kIx,	,
Johanna	Johann	k1gMnSc2	Johann
Wolfganga	Wolfgang	k1gMnSc2	Wolfgang
von	von	k1gInSc4	von
Goethe	Goethe	k1gNnSc2	Goethe
nebo	nebo	k8xC	nebo
Franze	Franze	k1gFnSc1	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
skladby	skladba	k1gFnPc4	skladba
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
díla	dílo	k1gNnSc2	dílo
Maxe	Max	k1gMnSc2	Max
Broda	Brod	k1gMnSc2	Brod
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
vybrána	vybrat	k5eAaPmNgNnP	vybrat
jen	jen	k6eAd1	jen
nejdůležitější	důležitý	k2eAgNnPc1d3	nejdůležitější
díla	dílo	k1gNnPc1	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
próza	próza	k1gFnSc1	próza
===	===	k?	===
</s>
</p>
<p>
<s>
Wachposten	Wachposten	k2eAgMnSc1d1	Wachposten
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Hlídka	hlídka	k1gFnSc1	hlídka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ein	Ein	k?	Ein
Kampf	Kampf	k1gInSc1	Kampf
um	um	k1gInSc1	um
Wahrheit	Wahrheit	k2eAgInSc1d1	Wahrheit
<g/>
.	.	kIx.	.
</s>
<s>
Románová	románový	k2eAgFnSc1d1	románová
trilogie	trilogie	k1gFnSc1	trilogie
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Boj	boj	k1gInSc1	boj
za	za	k7c4	za
pravdu	pravda	k1gFnSc4	pravda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tycho	Tycze	k6eAd1	Tycze
Brahes	Brahes	k1gMnSc1	Brahes
Weg	Weg	k1gMnSc1	Weg
zu	zu	k?	zu
Gott	Gott	k1gMnSc1	Gott
<g/>
,	,	kIx,	,
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Tychona	Tychona	k1gFnSc1	Tychona
Brahe	Brah	k1gFnSc2	Brah
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reubeni	Reuben	k2eAgMnPc1d1	Reuben
<g/>
,	,	kIx,	,
Fürst	Fürst	k1gMnSc1	Fürst
der	drát	k5eAaImRp2nS	drát
Juden	Judna	k1gFnPc2	Judna
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Rëubeni	Rëuben	k2eAgMnPc1d1	Rëuben
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
židovské	židovská	k1gFnSc2	židovská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Galilei	Galilei	k6eAd1	Galilei
in	in	k?	in
Gefangenschaft	Gefangenschaft	k1gInSc4	Gefangenschaft
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Galilei	Galilei	k1gNnSc1	Galilei
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Teoretická	teoretický	k2eAgNnPc1d1	teoretické
pojednání	pojednání	k1gNnPc1	pojednání
===	===	k?	===
</s>
</p>
<p>
<s>
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
:	:	kIx,	:
Leben	Leben	k2eAgMnSc1d1	Leben
und	und	k?	und
Werk	Werk	k1gMnSc1	Werk
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Biografie	biografie	k1gFnSc1	biografie
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
:	:	kIx,	:
Život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Heidentum	Heidentum	k1gNnSc1	Heidentum
<g/>
,	,	kIx,	,
Christentum	Christentum	k1gNnSc1	Christentum
und	und	k?	und
Judentum	Judentum	k1gNnSc1	Judentum
<g/>
,	,	kIx,	,
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Pohanství	pohanství	k1gNnSc1	pohanství
<g/>
,	,	kIx,	,
křesťanství	křesťanství	k1gNnSc1	křesťanství
a	a	k8xC	a
židovství	židovství	k1gNnSc1	židovství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pražské	pražský	k2eAgNnSc1d1	Pražské
hvězdné	hvězdný	k2eAgNnSc1d1	Hvězdné
nebe	nebe	k1gNnSc1	nebe
:	:	kIx,	:
Hudební	hudební	k2eAgInSc4d1	hudební
a	a	k8xC	a
divadelní	divadelní	k2eAgInSc4d1	divadelní
zážitky	zážitek	k1gInPc4	zážitek
z	z	k7c2	z
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Bedřich	Bedřich	k1gMnSc1	Bedřich
Fučík	Fučík	k1gMnSc1	Fučík
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
–	–	k?	–
výbor	výbor	k1gInSc1	výbor
hudebních	hudební	k2eAgInPc2d1	hudební
a	a	k8xC	a
divadelních	divadelní	k2eAgFnPc2d1	divadelní
recenzí	recenze	k1gFnPc2	recenze
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Překlady	překlad	k1gInPc1	překlad
poezie	poezie	k1gFnSc2	poezie
===	===	k?	===
</s>
</p>
<p>
<s>
1916	[number]	k4	1916
Jüngste	Jüngst	k1gMnSc5	Jüngst
tschechische	tschechische	k1gFnSc1	tschechische
Lyrik	lyrik	k1gMnSc1	lyrik
:	:	kIx,	:
eine	einat	k5eAaPmIp3nS	einat
Anthologie	Anthologie	k1gFnSc1	Anthologie
<g/>
,	,	kIx,	,
Berlin-Wilmersdorf	Berlin-Wilmersdorf	k1gInSc1	Berlin-Wilmersdorf
:	:	kIx,	:
Verlag	Verlag	k1gInSc1	Verlag
der	drát	k5eAaImRp2nS	drát
Wochenschrift	Wochenschrift	k1gMnSc1	Wochenschrift
Die	Die	k1gMnSc1	Die
Aktion	Aktion	k1gInSc4	Aktion
<g/>
,	,	kIx,	,
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Petr	Petr	k1gMnSc1	Petr
Bezruč	Bezruč	k1gMnSc1	Bezruč
<g/>
,	,	kIx,	,
Otokar	Otokar	k1gMnSc1	Otokar
Březina	Březina	k1gMnSc1	Březina
<g/>
,	,	kIx,	,
Viktor	Viktor	k1gMnSc1	Viktor
Dyk	Dyk	k?	Dyk
<g/>
,	,	kIx,	,
Otokar	Otokar	k1gMnSc1	Otokar
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Hlaváček	Hlaváček	k1gMnSc1	Hlaváček
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Karásek	Karásek	k1gMnSc1	Karásek
ze	z	k7c2	z
Lvovic	Lvovice	k1gFnPc2	Lvovice
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Kodiček	Kodička	k1gFnPc2	Kodička
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Křička	Křička	k1gMnSc1	Křička
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Machar	Machar	k1gMnSc1	Machar
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
K.	K.	kA	K.
Neumann	Neumann	k1gMnSc1	Neumann
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Sova	Sova	k1gMnSc1	Sova
<g/>
,	,	kIx,	,
Fráňa	Fráňa	k1gMnSc1	Fráňa
Šrámek	Šrámek	k1gMnSc1	Šrámek
<g/>
,	,	kIx,	,
Erwin	Erwin	k1gMnSc1	Erwin
Taussig	Taussig	k1gMnSc1	Taussig
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
Theer	Theer	k1gMnSc1	Theer
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Toman	Toman	k1gMnSc1	Toman
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Weiner	Weiner	k1gMnSc1	Weiner
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
<g/>
:	:	kIx,	:
Max	max	kA	max
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
Pick	Pick	k1gMnSc1	Pick
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Fuchs	Fuchs	k1gMnSc1	Fuchs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Překlady	překlad	k1gInPc4	překlad
libret	libreto	k1gNnPc2	libreto
===	===	k?	===
</s>
</p>
<p>
<s>
Její	její	k3xOp3gFnSc1	její
pastorkyňa	pastorkyňa	k1gFnSc1	pastorkyňa
(	(	kIx(	(
<g/>
Jenůfa	Jenůfa	k1gFnSc1	Jenůfa
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Káťa	Káťa	k1gFnSc1	Káťa
Kabanová	Kabanová	k1gFnSc1	Kabanová
(	(	kIx(	(
<g/>
Katja	Katj	k2eAgFnSc1d1	Katja
Kabanowa	Kabanowa	k1gFnSc1	Kabanowa
<g/>
,	,	kIx,	,
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Příhody	příhoda	k1gFnSc2	příhoda
lišky	liška	k1gFnPc1	liška
Bystroušky	Bystrouška	k1gFnPc1	Bystrouška
(	(	kIx(	(
<g/>
Das	Das	k1gFnSc1	Das
schlaue	schlau	k1gFnSc2	schlau
Füchslein	Füchsleina	k1gFnPc2	Füchsleina
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Věc	věc	k1gFnSc1	věc
Makropulos	Makropulos	k1gMnSc1	Makropulos
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc1	Die
Sache	Sach	k1gFnSc2	Sach
Makropulos	Makropulos	k1gMnSc1	Makropulos
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Z	z	k7c2	z
mrtvého	mrtvý	k2eAgInSc2d1	mrtvý
domu	dům	k1gInSc2	dům
(	(	kIx(	(
<g/>
Aus	Aus	k1gFnSc1	Aus
einem	einem	k1gInSc1	einem
Totenhaus	Totenhaus	k1gInSc1	Totenhaus
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pražští	pražský	k2eAgMnPc1d1	pražský
němečtí	německý	k2eAgMnPc1d1	německý
autoři	autor	k1gMnPc1	autor
===	===	k?	===
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
,	,	kIx,	,
eine	eine	k1gFnSc1	eine
Biographie	Biographie	k1gFnSc1	Biographie
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Biografie	biografie	k1gFnSc1	biografie
(	(	kIx(	(
<g/>
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
:	:	kIx,	:
Životopis	životopis	k1gInSc1	životopis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Prager	Prager	k1gMnSc1	Prager
Kreis	Kreis	k1gFnPc4	Kreis
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Pražský	pražský	k2eAgInSc1d1	pražský
kruh	kruh	k1gInSc1	kruh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Streitbares	Streitbares	k1gInSc1	Streitbares
Leben	Leben	k1gInSc1	Leben
<g/>
:	:	kIx,	:
Autobiographie	Autobiographie	k1gFnSc1	Autobiographie
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Autobiografie	autobiografie	k1gFnSc1	autobiografie
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Život	život	k1gInSc1	život
plný	plný	k2eAgInSc1d1	plný
bojů	boj	k1gInPc2	boj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hudební	hudební	k2eAgNnSc1d1	hudební
dílo	dílo	k1gNnSc1	dílo
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Mir	mir	k1gInSc4	mir
träumte	träumit	k5eAaPmRp2nP	träumit
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
1	[number]	k4	1
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
upraveno	upravit	k5eAaPmNgNnS	upravit
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
Heinrich	Heinrich	k1gMnSc1	Heinrich
Heine	Hein	k1gInSc5	Hein
</s>
</p>
<p>
<s>
Die	Die	k?	Die
Botschaft	Botschaft	k1gMnSc1	Botschaft
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
b	b	k?	b
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
upraveno	upravit	k5eAaPmNgNnS	upravit
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
Heinrich	Heinrich	k1gMnSc1	Heinrich
Heine	Hein	k1gInSc5	Hein
</s>
</p>
<p>
<s>
Drei	Drei	k6eAd1	Drei
Lieder	Lieder	k1gMnSc1	Lieder
des	des	k1gNnSc2	des
Todes	Todes	k1gMnSc1	Todes
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
Heinrich	Heinrich	k1gMnSc1	Heinrich
Heine	Hein	k1gInSc5	Hein
</s>
</p>
<p>
<s>
Drei	Drei	k6eAd1	Drei
philosophisches	philosophisches	k1gInSc1	philosophisches
texte	text	k1gInSc5	text
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
Heinrich	Heinrich	k1gMnSc1	Heinrich
Heine	Hein	k1gMnSc5	Hein
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
Méditerranée	Méditerrané	k1gFnSc2	Méditerrané
<g/>
,	,	kIx,	,
rapsódie	rapsódie	k1gFnSc2	rapsódie
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
,	,	kIx,	,
věnováno	věnovat	k5eAaImNgNnS	věnovat
</s>
</p>
<p>
<s>
Tod	Tod	k?	Tod
und	und	k?	und
Paradies	Paradies	k1gMnSc1	Paradies
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
35	[number]	k4	35
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
dvě	dva	k4xCgFnPc1	dva
písně	píseň	k1gFnPc1	píseň
na	na	k7c4	na
texty	text	k1gInPc4	text
z	z	k7c2	z
deníků	deník	k1gInPc2	deník
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
</s>
</p>
<p>
<s>
Acht	acht	k1gInSc1	acht
Lieder	Lieder	k1gInSc1	Lieder
aus	aus	k?	aus
Goethes	Goethes	k1gInSc1	Goethes
"	"	kIx"	"
<g/>
Chinesisch-Deutschen	Chinesisch-Deutschen	k2eAgMnSc1d1	Chinesisch-Deutschen
Jahres-	Jahres-	k1gMnSc1	Jahres-
und	und	k?	und
Tageszeiten	Tageszeitno	k1gNnPc2	Tageszeitno
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
für	für	k?	für
Ester	ester	k1gInSc1	ester
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
32	[number]	k4	32
<g/>
,	,	kIx,	,
osm	osm	k4xCc1	osm
písní	píseň	k1gFnPc2	píseň
na	na	k7c4	na
verše	verš	k1gInPc4	verš
Johanna	Johann	k1gMnSc2	Johann
Wolfganga	Wolfgang	k1gMnSc2	Wolfgang
von	von	k1gInSc4	von
Goethe	Goethe	k1gNnSc2	Goethe
</s>
</p>
<p>
<s>
klavírní	klavírní	k2eAgInSc1d1	klavírní
kvintet	kvintet	k1gInSc1	kvintet
Elégie	Elégie	k1gFnSc2	Elégie
dramatique	dramatiqu	k1gFnSc2	dramatiqu
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
33	[number]	k4	33
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Beletrie	beletrie	k1gFnSc2	beletrie
===	===	k?	===
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
služka	služka	k1gFnSc1	služka
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
svol	svolit	k5eAaPmRp2nS	svolit
<g/>
.	.	kIx.	.
spisovat	spisovat	k5eAaImF	spisovat
<g/>
.	.	kIx.	.
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Osten	osten	k1gInSc4	osten
<g/>
,	,	kIx,	,
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Alois	Alois	k1gMnSc1	Alois
Lapáček	Lapáček	k1gMnSc1	Lapáček
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
</s>
</p>
<p>
<s>
Tychona	Tychona	k1gFnSc1	Tychona
Brahe	Brah	k1gFnSc2	Brah
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Adolf	Adolf	k1gMnSc1	Adolf
Wenig	Wenig	k1gMnSc1	Wenig
<g/>
,	,	kIx,	,
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
F.	F.	kA	F.
Topič	topič	k1gMnSc1	topič
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
</s>
</p>
<p>
<s>
Franci	Franci	k1gMnSc1	Franci
čili	čili	k8xC	čili
Láska	láska	k1gFnSc1	láska
druhého	druhý	k4xOgInSc2	druhý
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Moravské	moravský	k2eAgFnSc2d1	Moravská
Noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
s	s	k7c7	s
bohyní	bohyně	k1gFnSc7	bohyně
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Pokrok	pokrok	k1gInSc1	pokrok
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
</s>
</p>
<p>
<s>
Osudný	osudný	k2eAgInSc4d1	osudný
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Eisner	Eisner	k1gMnSc1	Eisner
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Sfinx	sfinx	k1gInSc1	sfinx
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Janda	Janda	k1gMnSc1	Janda
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
</s>
</p>
<p>
<s>
Zázračný	zázračný	k2eAgInSc1d1	zázračný
kaktus	kaktus	k1gInSc1	kaktus
(	(	kIx(	(
<g/>
Opuncie	opuncie	k1gFnSc1	opuncie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prominentní	prominentní	k2eAgFnSc1d1	prominentní
komedie	komedie	k1gFnSc1	komedie
o	o	k7c6	o
3	[number]	k4	3
jednáních	jednání	k1gNnPc6	jednání
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Grmela	Grmela	k1gFnSc1	Grmela
<g/>
,	,	kIx,	,
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Evžen	Evžen	k1gMnSc1	Evžen
J.	J.	kA	J.
Rosendorf	Rosendorf	k1gMnSc1	Rosendorf
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
nemajetných	majetný	k2eNgNnPc2d1	nemajetné
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Gabriela	Gabriela	k1gFnSc1	Gabriela
Veselá	veselý	k2eAgFnSc1d1	veselá
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Světová	světový	k2eAgFnSc1d1	světová
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
38	[number]	k4	38
<g/>
,	,	kIx,	,
č.	č.	k?	č.
2	[number]	k4	2
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
123	[number]	k4	123
<g/>
–	–	k?	–
<g/>
128	[number]	k4	128
<g/>
,	,	kIx,	,
pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
Povídka	povídka	k1gFnSc1	povídka
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Experimenty	experiment	k1gInPc1	experiment
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BÄRSCH	BÄRSCH	kA	BÄRSCH
<g/>
,	,	kIx,	,
Claus-Ekkehard	Claus-Ekkehard	k1gInSc1	Claus-Ekkehard
<g/>
.	.	kIx.	.
</s>
<s>
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc1	Brod
im	im	k?	im
"	"	kIx"	"
<g/>
Kampf	Kampf	k1gMnSc1	Kampf
um	uma	k1gFnPc2	uma
das	das	k?	das
Judentum	Judentum	k1gNnSc4	Judentum
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zum	Zum	k?	Zum
Leben	Leben	k1gInSc1	Leben
und	und	k?	und
Werk	Werk	k1gInSc1	Werk
eines	eines	k1gMnSc1	eines
deutsch-jüdischen	deutschüdischna	k1gFnPc2	deutsch-jüdischna
Dichters	Dichters	k1gInSc4	Dichters
aus	aus	k?	aus
Prag	Prag	k1gInSc1	Prag
<g/>
.	.	kIx.	.
</s>
<s>
Wien	Wien	k1gMnSc1	Wien
<g/>
:	:	kIx,	:
Passagen-Verlag	Passagen-Verlag	k1gMnSc1	Passagen-Verlag
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BROD	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
<g/>
.	.	kIx.	.
Život	život	k1gInSc1	život
plný	plný	k2eAgInSc1d1	plný
bojů	boj	k1gInPc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Autobiografie	autobiografie	k1gFnSc1	autobiografie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KROLOP	KROLOP	kA	KROLOP
<g/>
,	,	kIx,	,
Kurt	Kurt	k1gMnSc1	Kurt
<g/>
.	.	kIx.	.
</s>
<s>
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc1	Brod
versus	versus	k7c1	versus
Karl	Karla	k1gFnPc2	Karla
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Alena	Alena	k1gFnSc1	Alena
Bláhová	Bláhová	k1gFnSc1	Bláhová
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgFnPc1d1	literární
noviny	novina	k1gFnPc1	novina
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
50	[number]	k4	50
<g/>
,	,	kIx,	,
s.	s.	k?	s.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
přednáška	přednáška	k1gFnSc1	přednáška
pronesená	pronesený	k2eAgFnSc1d1	pronesená
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1994	[number]	k4	1994
v	v	k7c6	v
Centru	centrum	k1gNnSc6	centrum
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kolokvia	kolokvium	k1gNnSc2	kolokvium
Max	max	kA	max
Brod	Brod	k1gInSc1	Brod
-	-	kIx~	-
prostředník	prostředník	k1gInSc1	prostředník
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
73	[number]	k4	73
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠRÁMKOVÁ	Šrámková	k1gFnSc1	Šrámková
<g/>
,	,	kIx,	,
Barbora	Barbora	k1gFnSc1	Barbora
<g/>
.	.	kIx.	.
</s>
<s>
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc1	Brod
und	und	k?	und
die	die	k?	die
tschechische	tschechische	k1gNnSc2	tschechische
Kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Wuppertal	Wuppertat	k5eAaPmAgMnS	Wuppertat
<g/>
:	:	kIx,	:
Arco	Arco	k6eAd1	Arco
Verlag	Verlag	k1gMnSc1	Verlag
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Arco	Arco	k1gMnSc1	Arco
Wissenschaft	Wissenschaft	k1gMnSc1	Wissenschaft
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3938375273	[number]	k4	3938375273
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
A-	A-	k1gFnSc1	A-
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
634	[number]	k4	634
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
245	[number]	k4	245
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
140	[number]	k4	140
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VESELÝ	Veselý	k1gMnSc1	Veselý
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
<g/>
.	.	kIx.	.
</s>
<s>
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc1	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Maskil	Maskil	k1gInSc1	Maskil
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Prosinec	prosinec	k1gInSc1	prosinec
2008	[number]	k4	2008
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
16	[number]	k4	16
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
7	[number]	k4	7
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
Bra-Brum	Bra-Brum	k1gInSc1	Bra-Brum
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
110-224	[number]	k4	110-224
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
248	[number]	k4	248
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
185	[number]	k4	185
<g/>
–	–	k?	–
<g/>
186	[number]	k4	186
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Max	max	kA	max
Brod	Brod	k1gInSc4	Brod
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc1	Brod
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc1	Brod
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc1	Brod
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
hudebním	hudební	k2eAgInSc6d1	hudební
slovníku	slovník	k1gInSc6	slovník
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
institucí	instituce	k1gFnPc2	instituce
</s>
</p>
