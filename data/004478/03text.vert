<s>
Lev	Lev	k1gMnSc1	Lev
pustinný	pustinný	k2eAgMnSc1d1	pustinný
(	(	kIx(	(
<g/>
Panthera	Panther	k1gMnSc4	Panther
leo	leo	k?	leo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
savec	savec	k1gMnSc1	savec
čeledi	čeleď	k1gFnSc2	čeleď
kočkovitých	kočkovití	k1gMnPc2	kočkovití
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
druhů	druh	k1gInPc2	druh
velkých	velký	k2eAgFnPc2d1	velká
koček	kočka	k1gFnPc2	kočka
rodu	rod	k1gInSc2	rod
Panthera	Panthera	k1gFnSc1	Panthera
<g/>
.	.	kIx.	.
</s>
<s>
Lev	Lev	k1gMnSc1	Lev
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
tygrovi	tygr	k1gMnSc6	tygr
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
kočkovitá	kočkovitý	k2eAgFnSc1d1	kočkovitá
šelma	šelma	k1gFnSc1	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
váží	vážit	k5eAaImIp3nP	vážit
150	[number]	k4	150
<g/>
–	–	k?	–
<g/>
250	[number]	k4	250
kg	kg	kA	kg
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
90	[number]	k4	90
<g/>
–	–	k?	–
<g/>
165	[number]	k4	165
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
divočině	divočina	k1gFnSc6	divočina
se	se	k3xPyFc4	se
lvi	lev	k1gMnPc1	lev
dožívají	dožívat	k5eAaImIp3nP	dožívat
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dožít	dožít	k5eAaPmF	dožít
i	i	k9	i
věku	věk	k1gInSc2	věk
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
lvi	lev	k1gMnPc1	lev
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
společenská	společenský	k2eAgNnPc4d1	společenské
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
loví	lovit	k5eAaImIp3nP	lovit
ve	v	k7c6	v
smečkách	smečka	k1gFnPc6	smečka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lvů	lev	k1gMnPc2	lev
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
výrazný	výrazný	k2eAgInSc1d1	výrazný
pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgInSc7d1	hlavní
a	a	k8xC	a
určujícím	určující	k2eAgInSc7d1	určující
rysem	rys	k1gInSc7	rys
lvích	lví	k2eAgMnPc2d1	lví
samců	samec	k1gMnPc2	samec
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
hříva	hříva	k1gFnSc1	hříva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
pleistocénu	pleistocén	k1gInSc2	pleistocén
byli	být	k5eAaImAgMnP	být
lvi	lev	k1gMnPc1	lev
hojně	hojně	k6eAd1	hojně
rozšířeni	rozšířen	k2eAgMnPc1d1	rozšířen
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
kontinentech	kontinent	k1gInPc6	kontinent
včetně	včetně	k7c2	včetně
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velkým	velký	k2eAgFnPc3d1	velká
změnám	změna	k1gFnPc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
koncem	koncem	k7c2	koncem
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
10	[number]	k4	10
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Lvi	lev	k1gMnPc1	lev
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vymizeli	vymizet	k5eAaPmAgMnP	vymizet
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
velkých	velká	k1gFnPc2	velká
části	část	k1gFnSc2	část
Evropy	Evropa	k1gFnSc2	Evropa
i	i	k8xC	i
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byli	být	k5eAaImAgMnP	být
poslední	poslední	k2eAgMnPc1d1	poslední
lvi	lev	k1gMnPc1	lev
vyhubeni	vyhubit	k5eAaPmNgMnP	vyhubit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
100	[number]	k4	100
n.	n.	k?	n.
l.	l.	k?	l.
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
přežívali	přežívat	k5eAaImAgMnP	přežívat
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
populacích	populace	k1gFnPc6	populace
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgNnSc6d1	pyrenejské
<g/>
,	,	kIx,	,
Apeninském	apeninský	k2eAgNnSc6d1	Apeninské
a	a	k8xC	a
především	především	k6eAd1	především
Balkánském	balkánský	k2eAgInSc6d1	balkánský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Hérodota	Hérodot	k1gMnSc2	Hérodot
byli	být	k5eAaImAgMnP	být
lvi	lev	k1gMnPc1	lev
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
480	[number]	k4	480
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
poměrně	poměrně	k6eAd1	poměrně
běžní	běžný	k2eAgMnPc1d1	běžný
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
napadali	napadat	k5eAaPmAgMnP	napadat
velbloudy	velbloud	k1gMnPc4	velbloud
ze	z	k7c2	z
zásobovacích	zásobovací	k2eAgFnPc2d1	zásobovací
kolon	kolona	k1gFnPc2	kolona
perského	perský	k2eAgMnSc2d1	perský
krále	král	k1gMnSc2	král
Xerxa	Xerxes	k1gMnSc2	Xerxes
I.	I.	kA	I.
během	běh	k1gInSc7	běh
jeho	jeho	k3xOp3gNnSc2	jeho
válečného	válečný	k2eAgNnSc2d1	válečné
tažení	tažení	k1gNnSc2	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
v	v	k7c6	v
roce	rok	k1gInSc6	rok
300	[number]	k4	300
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
již	již	k9	již
lvy	lev	k1gMnPc4	lev
na	na	k7c6	na
území	území	k1gNnSc6	území
Řecka	Řecko	k1gNnSc2	Řecko
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
vzácné	vzácný	k2eAgNnSc4d1	vzácné
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
je	být	k5eAaImIp3nS	být
doložen	doložen	k2eAgInSc1d1	doložen
výskyt	výskyt	k1gInSc1	výskyt
lva	lev	k1gMnSc2	lev
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
byli	být	k5eAaImAgMnP	být
lvi	lev	k1gMnPc1	lev
vyhubeni	vyhubit	k5eAaPmNgMnP	vyhubit
již	již	k6eAd1	již
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
masivním	masivní	k2eAgNnSc7d1	masivní
rozšířením	rozšíření	k1gNnSc7	rozšíření
střelných	střelný	k2eAgFnPc2d1	střelná
zbraní	zbraň	k1gFnPc2	zbraň
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vymizel	vymizet	k5eAaPmAgMnS	vymizet
lev	lev	k1gMnSc1	lev
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
svého	svůj	k3xOyFgInSc2	svůj
asijského	asijský	k2eAgInSc2d1	asijský
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vymřeli	vymřít	k5eAaPmAgMnP	vymřít
lvi	lev	k1gMnPc1	lev
také	také	k9	také
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
byl	být	k5eAaImAgInS	být
poslední	poslední	k2eAgInSc1d1	poslední
živý	živý	k2eAgInSc1d1	živý
lev	lev	k1gInSc1	lev
spatřen	spatřit	k5eAaPmNgInS	spatřit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
eurasijští	eurasijský	k2eAgMnPc1d1	eurasijský
lvi	lev	k1gMnPc1	lev
(	(	kIx(	(
<g/>
poddruh	poddruh	k1gInSc1	poddruh
Panthera	Panther	k1gMnSc2	Panther
leo	leo	k?	leo
persica	persicus	k1gMnSc2	persicus
<g/>
)	)	kIx)	)
dnes	dnes	k6eAd1	dnes
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Sahary	Sahara	k1gFnSc2	Sahara
je	být	k5eAaImIp3nS	být
lev	lev	k1gMnSc1	lev
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
poměrně	poměrně	k6eAd1	poměrně
hojný	hojný	k2eAgMnSc1d1	hojný
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
divokých	divoký	k2eAgInPc2d1	divoký
lvů	lev	k1gInPc2	lev
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
malá	malý	k2eAgFnSc1d1	malá
populace	populace	k1gFnSc1	populace
asijských	asijský	k2eAgInPc2d1	asijský
lvů	lev	k1gInPc2	lev
přežívá	přežívat	k5eAaImIp3nS	přežívat
v	v	k7c6	v
indickém	indický	k2eAgInSc6d1	indický
státě	stát	k1gInSc6	stát
Gudžarát	Gudžarát	k1gInSc1	Gudžarát
<g/>
.	.	kIx.	.
</s>
<s>
Asie	Asie	k1gFnSc1	Asie
Lev	lev	k1gInSc1	lev
byl	být	k5eAaImAgInS	být
vyhuben	vyhubit	k5eAaPmNgInS	vyhubit
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
svého	svůj	k3xOyFgInSc2	svůj
původního	původní	k2eAgInSc2d1	původní
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
žijí	žít	k5eAaImIp3nP	žít
asijští	asijský	k2eAgMnPc1d1	asijský
lvi	lev	k1gMnPc1	lev
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
území	území	k1gNnSc6	území
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Gir	Gir	k1gMnSc1	Gir
Forest	Forest	k1gMnSc1	Forest
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
1412	[number]	k4	1412
km2	km2	k4	km2
v	v	k7c6	v
lesnatém	lesnatý	k2eAgInSc6d1	lesnatý
státě	stát	k1gInSc6	stát
Gudžarát	Gudžarát	k1gInSc1	Gudžarát
žije	žít	k5eAaImIp3nS	žít
zhruba	zhruba	k6eAd1	zhruba
350	[number]	k4	350
lvů	lev	k1gInPc2	lev
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
stavy	stav	k1gInPc1	stav
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
stabilní	stabilní	k2eAgInPc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
Afrika	Afrika	k1gFnSc1	Afrika
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
žije	žít	k5eAaImIp3nS	žít
nejvíce	hodně	k6eAd3	hodně
lvů	lev	k1gMnPc2	lev
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jejich	jejich	k3xOp3gInPc1	jejich
stavy	stav	k1gInPc1	stav
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
snižují	snižovat	k5eAaImIp3nP	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
africká	africký	k2eAgFnSc1d1	africká
lví	lví	k2eAgFnSc1d1	lví
populace	populace	k1gFnSc1	populace
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
cca	cca	kA	cca
23	[number]	k4	23
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
žijících	žijící	k2eAgInPc2d1	žijící
v	v	k7c6	v
divočině	divočina	k1gFnSc6	divočina
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
sice	sice	k8xC	sice
o	o	k7c4	o
nejmenší	malý	k2eAgInSc4d3	nejmenší
možný	možný	k2eAgInSc4d1	možný
počet	počet	k1gInSc4	počet
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
pravděpodobnější	pravděpodobný	k2eAgInSc1d2	Pravděpodobnější
odhad-maximální	odhadaximální	k2eAgInSc1d1	odhad-maximální
počet	počet	k1gInSc1	počet
je	být	k5eAaImIp3nS	být
39	[number]	k4	39
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
stav	stav	k1gInSc1	stav
populace	populace	k1gFnSc2	populace
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c6	na
100	[number]	k4	100
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
zvyšující	zvyšující	k2eAgInSc1d1	zvyšující
kontakt	kontakt	k1gInSc1	kontakt
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
je	být	k5eAaImIp3nS	být
pokládán	pokládat	k5eAaImNgMnS	pokládat
za	za	k7c4	za
hlavní	hlavní	k2eAgInSc4d1	hlavní
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
snižování	snižování	k1gNnSc3	snižování
jejich	jejich	k3xOp3gInPc2	jejich
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgFnPc1d1	zbylá
populace	populace	k1gFnPc1	populace
od	od	k7c2	od
sebe	se	k3xPyFc2	se
bývají	bývat	k5eAaImIp3nP	bývat
geograficky	geograficky	k6eAd1	geograficky
izolovány	izolovat	k5eAaBmNgInP	izolovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
inbreedingu	inbreeding	k1gInSc2	inbreeding
–	–	k?	–
potlačení	potlačení	k1gNnSc2	potlačení
dalšího	další	k2eAgNnSc2d1	další
rozvíjení	rozvíjení	k1gNnSc2	rozvíjení
genofondu	genofond	k1gInSc2	genofond
<g/>
.	.	kIx.	.
</s>
<s>
Lvice	lvice	k1gFnPc1	lvice
loví	lovit	k5eAaImIp3nP	lovit
ve	v	k7c6	v
smečkách	smečka	k1gFnPc6	smečka
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
kořistí	kořist	k1gFnSc7	kořist
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
hlavně	hlavně	k9	hlavně
větší	veliký	k2eAgMnPc1d2	veliký
savci	savec	k1gMnPc1	savec
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
antilopy	antilopa	k1gFnPc1	antilopa
<g/>
,	,	kIx,	,
pakoně	pakůň	k1gMnPc1	pakůň
<g/>
,	,	kIx,	,
buvoli	buvol	k1gMnPc1	buvol
a	a	k8xC	a
zebry	zebra	k1gFnPc1	zebra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
menší	malý	k2eAgNnPc1d2	menší
zvířata	zvíře	k1gNnPc1	zvíře
jako	jako	k8xC	jako
zajíci	zajíc	k1gMnPc1	zajíc
a	a	k8xC	a
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Nepohrdnou	pohrdnout	k5eNaPmIp3nP	pohrdnout
ani	ani	k9	ani
zdechlinami	zdechlina	k1gFnPc7	zdechlina
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
usmrtila	usmrtit	k5eAaPmAgNnP	usmrtit
jiná	jiný	k2eAgNnPc1d1	jiné
zvířata	zvíře	k1gNnPc1	zvíře
jako	jako	k8xC	jako
hyeny	hyena	k1gFnPc1	hyena
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
psovité	psovitý	k2eAgFnPc1d1	psovitá
šelmy	šelma	k1gFnPc1	šelma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
lokalitách	lokalita	k1gFnPc6	lokalita
se	se	k3xPyFc4	se
lvi	lev	k1gMnPc1	lev
začali	začít	k5eAaPmAgMnP	začít
orientovat	orientovat	k5eAaBmF	orientovat
na	na	k7c4	na
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
jinak	jinak	k6eAd1	jinak
nezvyklou	zvyklý	k2eNgFnSc4d1	nezvyklá
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
sloní	sloní	k2eAgNnPc4d1	sloní
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
loví	lovit	k5eAaImIp3nP	lovit
v	v	k7c6	v
území	území	k1gNnSc6	území
řeky	řeka	k1gFnSc2	řeka
Savuti	Savuť	k1gFnSc2	Savuť
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
Linyanti	Linyant	k1gMnPc5	Linyant
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
naučili	naučit	k5eAaPmAgMnP	naučit
lovit	lovit	k5eAaImF	lovit
hrochy	hroch	k1gMnPc4	hroch
(	(	kIx(	(
<g/>
Savuti	Savut	k1gMnPc1	Savut
a	a	k8xC	a
Linyanti	Linyant	k1gMnPc1	Linyant
jsou	být	k5eAaImIp3nP	být
řeky	řeka	k1gFnPc4	řeka
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Chobe	Chob	k1gInSc5	Chob
v	v	k7c6	v
Botswaně	Botswana	k1gFnSc6	Botswana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
extrémní	extrémní	k2eAgInSc1d1	extrémní
hlad	hlad	k1gInSc1	hlad
dovedl	dovést	k5eAaPmAgMnS	dovést
lva	lev	k1gMnSc4	lev
až	až	k9	až
k	k	k7c3	k
usmrcení	usmrcení	k1gNnSc3	usmrcení
mladých	mladý	k2eAgMnPc2d1	mladý
slonů	slon	k1gMnPc2	slon
<g/>
,	,	kIx,	,
časem	časem	k6eAd1	časem
začali	začít	k5eAaPmAgMnP	začít
napadat	napadat	k5eAaBmF	napadat
i	i	k9	i
dospělejší	dospělý	k2eAgMnPc4d2	dospělejší
slony	slon	k1gMnPc4	slon
a	a	k8xC	a
příležitostně	příležitostně	k6eAd1	příležitostně
také	také	k9	také
dospělé	dospělý	k2eAgMnPc4d1	dospělý
jedince	jedinec	k1gMnPc4	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Lvíčata	lvíče	k1gNnPc1	lvíče
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nP	učit
lovit	lovit	k5eAaImF	lovit
už	už	k6eAd1	už
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
měsících	měsíc	k1gInPc6	měsíc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
nestanou	stanout	k5eNaPmIp3nP	stanout
úspěšnými	úspěšný	k2eAgMnPc7d1	úspěšný
lovci	lovec	k1gMnPc1	lovec
před	před	k7c7	před
dosažením	dosažení	k1gNnSc7	dosažení
druhého	druhý	k4xOgInSc2	druhý
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Lvi	lev	k1gMnPc1	lev
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
vyvinou	vyvinout	k5eAaPmIp3nP	vyvinout
rychlost	rychlost	k1gFnSc4	rychlost
80	[number]	k4	80
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
60	[number]	k4	60
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Postrádají	postrádat	k5eAaImIp3nP	postrádat
však	však	k9	však
vytrvalost	vytrvalost	k1gFnSc4	vytrvalost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
musí	muset	k5eAaImIp3nS	muset
za	za	k7c7	za
svou	svůj	k3xOyFgFnSc7	svůj
kořistí	kořist	k1gFnSc7	kořist
plížit	plížit	k5eAaImF	plížit
alespoň	alespoň	k9	alespoň
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
30	[number]	k4	30
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
někteří	některý	k3yIgMnPc1	některý
lvi	lev	k1gMnPc1	lev
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
obklíčit	obklíčit	k5eAaPmF	obklíčit
stádo	stádo	k1gNnSc4	stádo
z	z	k7c2	z
několika	několik	k4yIc2	několik
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
útok	útok	k1gInSc1	útok
je	být	k5eAaImIp3nS	být
rychlý	rychlý	k2eAgInSc1d1	rychlý
a	a	k8xC	a
smrtící	smrtící	k2eAgInSc1d1	smrtící
<g/>
,	,	kIx,	,
lvice	lvice	k1gFnSc1	lvice
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
dohnat	dohnat	k5eAaPmF	dohnat
kořist	kořist	k1gFnSc4	kořist
rychlým	rychlý	k2eAgInSc7d1	rychlý
sprintem	sprint	k1gInSc7	sprint
a	a	k8xC	a
výpadem	výpad	k1gInSc7	výpad
s	s	k7c7	s
vytaženými	vytažený	k2eAgInPc7d1	vytažený
drápy	dráp	k1gInPc7	dráp
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
skoky	skok	k1gInPc4	skok
na	na	k7c4	na
oběť	oběť	k1gFnSc4	oběť
jsou	být	k5eAaImIp3nP	být
mýtus	mýtus	k1gInSc4	mýtus
<g/>
.	.	kIx.	.
</s>
<s>
Kořist	kořist	k1gFnSc1	kořist
většinou	většinou	k6eAd1	většinou
usmrtí	usmrtit	k5eAaPmIp3nS	usmrtit
zastavením	zastavení	k1gNnSc7	zastavení
přívodu	přívod	k1gInSc2	přívod
krve	krev	k1gFnSc2	krev
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
stiskem	stisk	k1gInSc7	stisk
na	na	k7c6	na
hrdle	hrdla	k1gFnSc6	hrdla
nebo	nebo	k8xC	nebo
u	u	k7c2	u
menší	malý	k2eAgFnSc2d2	menší
kořisti	kořist	k1gFnSc2	kořist
zlomením	zlomení	k1gNnSc7	zlomení
vazu	vaz	k1gInSc2	vaz
tlapou	tlapat	k5eAaImIp3nP	tlapat
<g/>
.	.	kIx.	.
</s>
<s>
Lvi	lev	k1gMnPc1	lev
loví	lovit	k5eAaImIp3nP	lovit
na	na	k7c6	na
otevřeném	otevřený	k2eAgNnSc6d1	otevřené
prostranství	prostranství	k1gNnSc6	prostranství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
lehce	lehko	k6eAd1	lehko
zpozorováni	zpozorován	k2eAgMnPc1d1	zpozorován
<g/>
,	,	kIx,	,
týmová	týmový	k2eAgFnSc1d1	týmová
práce	práce	k1gFnSc1	práce
ve	v	k7c6	v
smečce	smečka	k1gFnSc6	smečka
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
úspěšnost	úspěšnost	k1gFnSc4	úspěšnost
lovu	lov	k1gInSc2	lov
<g/>
.	.	kIx.	.
</s>
<s>
Smečka	smečka	k1gFnSc1	smečka
také	také	k9	také
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
ubránění	ubránění	k1gNnSc6	ubránění
již	již	k6eAd1	již
ulovené	ulovený	k2eAgFnSc2d1	ulovená
kořisti	kořist	k1gFnSc2	kořist
před	před	k7c7	před
ostatními	ostatní	k2eAgMnPc7d1	ostatní
predátory	predátor	k1gMnPc7	predátor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
hyenami	hyena	k1gFnPc7	hyena
(	(	kIx(	(
<g/>
ty	ten	k3xDgFnPc1	ten
mohou	moct	k5eAaImIp3nP	moct
zdechlinu	zdechlina	k1gFnSc4	zdechlina
ucítit	ucítit	k5eAaPmF	ucítit
i	i	k9	i
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
několika	několik	k4yIc2	několik
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Viditelně	viditelně	k6eAd1	viditelně
loví	lovit	k5eAaImIp3nS	lovit
jen	jen	k9	jen
lvice	lvice	k1gFnSc1	lvice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pravdou	pravda	k1gFnSc7	pravda
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
samci	samec	k1gMnPc1	samec
loví	lovit	k5eAaImIp3nP	lovit
docela	docela	k6eAd1	docela
aktivně	aktivně	k6eAd1	aktivně
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
ovšem	ovšem	k9	ovšem
rozdílnou	rozdílný	k2eAgFnSc4d1	rozdílná
strategii	strategie	k1gFnSc4	strategie
než	než	k8xS	než
lvice	lvice	k1gFnPc4	lvice
<g/>
.	.	kIx.	.
</s>
<s>
Lvi	lev	k1gMnPc1	lev
loví	lovit	k5eAaImIp3nP	lovit
sami	sám	k3xTgMnPc1	sám
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
potřebu	potřeba	k1gFnSc4	potřeba
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
pokud	pokud	k8xS	pokud
už	už	k6eAd1	už
jdou	jít	k5eAaImIp3nP	jít
na	na	k7c4	na
lov	lov	k1gInSc4	lov
<g/>
,	,	kIx,	,
loví	lovit	k5eAaImIp3nP	lovit
za	za	k7c2	za
šera	šero	k1gNnSc2	šero
až	až	k8xS	až
tmy	tma	k1gFnSc2	tma
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
šelma	šelma	k1gFnSc1	šelma
kočkovitá	kočkovitý	k2eAgFnSc1d1	kočkovitá
mají	mít	k5eAaImIp3nP	mít
lvi	lev	k1gMnPc1	lev
velice	velice	k6eAd1	velice
dobrý	dobrý	k2eAgInSc4d1	dobrý
zrak	zrak	k1gInSc4	zrak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
temnotě	temnota	k1gFnSc6	temnota
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
potichu	potichu	k6eAd1	potichu
připlíží	připlížit	k5eAaPmIp3nS	připlížit
ke	k	k7c3	k
kořisti	kořist	k1gFnSc3	kořist
proti	proti	k7c3	proti
větru	vítr	k1gInSc3	vítr
a	a	k8xC	a
z	z	k7c2	z
1,5	[number]	k4	1,5
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
metrové	metrový	k2eAgFnSc2d1	metrová
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
na	na	k7c4	na
kořist	kořist	k1gFnSc4	kořist
skočí	skočit	k5eAaPmIp3nS	skočit
<g/>
.	.	kIx.	.
</s>
<s>
Kořist	kořist	k1gFnSc4	kořist
usmrcují	usmrcovat	k5eAaImIp3nP	usmrcovat
lvi	lev	k1gMnPc1	lev
i	i	k9	i
lvice	lvice	k1gFnSc2	lvice
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
se	se	k3xPyFc4	se
ve	v	k7c6	v
smečce	smečka	k1gFnSc6	smečka
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
lovu	lov	k1gInSc6	lov
nepodílejí	podílet	k5eNaImIp3nP	podílet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
smečka	smečka	k1gFnSc1	smečka
snaží	snažit	k5eAaImIp3nS	snažit
ulovit	ulovit	k5eAaPmF	ulovit
nějaká	nějaký	k3yIgNnPc4	nějaký
větší	veliký	k2eAgNnPc4d2	veliký
zvířata	zvíře	k1gNnPc4	zvíře
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
buvoli	buvol	k1gMnPc1	buvol
<g/>
,	,	kIx,	,
zapojí	zapojit	k5eAaPmIp3nP	zapojit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Lvi	lev	k1gMnPc1	lev
jsou	být	k5eAaImIp3nP	být
masožraví	masožravý	k2eAgMnPc1d1	masožravý
a	a	k8xC	a
projevují	projevovat	k5eAaImIp3nP	projevovat
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
společenského	společenský	k2eAgNnSc2d1	společenské
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
smečky	smečka	k1gFnSc2	smečka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
příbuzných	příbuzný	k2eAgFnPc2d1	příbuzná
samic	samice	k1gFnPc2	samice
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnPc2	jejich
mláďat	mládě	k1gNnPc2	mládě
a	a	k8xC	a
skupiny	skupina	k1gFnSc2	skupina
jednoho	jeden	k4xCgInSc2	jeden
až	až	k6eAd1	až
čtyř	čtyři	k4xCgInPc2	čtyři
samců	samec	k1gInPc2	samec
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
koalice	koalice	k1gFnSc1	koalice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
páří	pářit	k5eAaImIp3nP	pářit
s	s	k7c7	s
dospělými	dospělý	k2eAgFnPc7d1	dospělá
samicemi	samice	k1gFnPc7	samice
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
nomádi	nomád	k1gMnPc1	nomád
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
jsou	být	k5eAaImIp3nP	být
samotáři	samotář	k1gMnPc1	samotář
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
jedinci	jedinec	k1gMnPc1	jedinec
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c4	v
páru	pára	k1gFnSc4	pára
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
samice	samice	k1gFnPc1	samice
lehčí	lehký	k2eAgFnPc1d2	lehčí
a	a	k8xC	a
menší	malý	k2eAgFnPc1d2	menší
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
hbitější	hbitý	k2eAgFnPc1d2	hbitější
a	a	k8xC	a
daleko	daleko	k6eAd1	daleko
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
samci	samec	k1gMnSc3	samec
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
na	na	k7c6	na
lovu	lov	k1gInSc6	lov
podílejí	podílet	k5eAaImIp3nP	podílet
největší	veliký	k2eAgFnSc7d3	veliký
měrou	míra	k1gFnSc7wR	míra
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
naopak	naopak	k6eAd1	naopak
využívá	využívat	k5eAaImIp3nS	využívat
svou	svůj	k3xOyFgFnSc4	svůj
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
velikost	velikost	k1gFnSc4	velikost
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
teritoria	teritorium	k1gNnSc2	teritorium
a	a	k8xC	a
střežení	střežení	k1gNnSc2	střežení
ulovené	ulovený	k2eAgFnSc2d1	ulovená
kořisti	kořist	k1gFnSc2	kořist
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
tomu	ten	k3xDgInSc3	ten
mají	mít	k5eAaImIp3nP	mít
samci	samec	k1gMnPc1	samec
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
ulovenou	ulovený	k2eAgFnSc4d1	ulovená
potravu	potrava	k1gFnSc4	potrava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odpočívající	odpočívající	k2eAgMnPc1d1	odpočívající
lvi	lev	k1gMnPc1	lev
si	se	k3xPyFc3	se
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
vzájemně	vzájemně	k6eAd1	vzájemně
projevují	projevovat	k5eAaImIp3nP	projevovat
náklonnost	náklonnost	k1gFnSc4	náklonnost
<g/>
,	,	kIx,	,
hladí	hladit	k5eAaImIp3nS	hladit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
čistí	čistit	k5eAaImIp3nS	čistit
si	se	k3xPyFc3	se
srst	srst	k1gFnSc4	srst
a	a	k8xC	a
drbou	drbat	k5eAaImIp3nP	drbat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ale	ale	k8xC	ale
dojde	dojít	k5eAaPmIp3nS	dojít
na	na	k7c4	na
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
každý	každý	k3xTgMnSc1	každý
sám	sám	k3xTgMnSc1	sám
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
s	s	k7c7	s
jediným	jediný	k2eAgInSc7d1	jediný
cílem	cíl	k1gInSc7	cíl
–	–	k?	–
urvat	urvat	k5eAaPmF	urvat
co	co	k9	co
největší	veliký	k2eAgInSc4d3	veliký
kus	kus	k1gInSc4	kus
<g/>
.	.	kIx.	.
</s>
<s>
Handrkování	handrkování	k1gNnSc1	handrkování
a	a	k8xC	a
boje	boj	k1gInPc1	boj
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
pojídání	pojídání	k1gNnSc6	pojídání
kořisti	kořist	k1gFnSc2	kořist
na	na	k7c6	na
denním	denní	k2eAgInSc6d1	denní
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
nejdříve	dříve	k6eAd3	dříve
žerou	žrát	k5eAaImIp3nP	žrát
samci	samec	k1gMnPc1	samec
a	a	k8xC	a
poté	poté	k6eAd1	poté
samice	samice	k1gFnSc1	samice
se	s	k7c7	s
lvíčaty	lvíče	k1gNnPc7	lvíče
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
i	i	k8xC	i
samice	samice	k1gFnPc1	samice
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
snaží	snažit	k5eAaImIp3nP	snažit
ochránit	ochránit	k5eAaPmF	ochránit
svou	svůj	k3xOyFgFnSc4	svůj
ulovenou	ulovený	k2eAgFnSc4d1	ulovená
kořist	kořist	k1gFnSc4	kořist
před	před	k7c7	před
jinými	jiný	k2eAgMnPc7d1	jiný
predátory	predátor	k1gMnPc7	predátor
a	a	k8xC	a
mrchožrouty	mrchožrout	k1gMnPc7	mrchožrout
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
individuálněji	individuálně	k6eAd2	individuálně
založení	založený	k2eAgMnPc1d1	založený
lvi	lev	k1gMnPc1	lev
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
snaží	snažit	k5eAaImIp3nS	snažit
vést	vést	k5eAaImF	vést
hlavní	hlavní	k2eAgFnSc4d1	hlavní
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
ochraně	ochrana	k1gFnSc6	ochrana
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
smečky	smečka	k1gFnSc2	smečka
s	s	k7c7	s
požíráním	požírání	k1gNnSc7	požírání
loudají	loudat	k5eAaImIp3nP	loudat
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
"	"	kIx"	"
<g/>
loudalové	loudal	k1gMnPc1	loudal
<g/>
"	"	kIx"	"
nebývají	bývat	k5eNaImIp3nP	bývat
ostatními	ostatní	k2eAgMnPc7d1	ostatní
členy	člen	k1gMnPc7	člen
trestáni	trestat	k5eAaImNgMnP	trestat
<g/>
,	,	kIx,	,
možnou	možný	k2eAgFnSc7d1	možná
hypotézou	hypotéza	k1gFnSc7	hypotéza
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
smečce	smečka	k1gFnSc6	smečka
jiné	jiný	k2eAgFnPc1d1	jiná
"	"	kIx"	"
<g/>
služby	služba	k1gFnPc1	služba
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jim	on	k3xPp3gMnPc3	on
ostatní	ostatní	k1gNnSc4	ostatní
lvi	lev	k1gMnPc1	lev
odpustí	odpustit	k5eAaPmIp3nP	odpustit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
alternativou	alternativa	k1gFnSc7	alternativa
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
lvi	lev	k1gMnPc1	lev
odměňují	odměňovat	k5eAaImIp3nP	odměňovat
členy	člen	k1gMnPc4	člen
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
brání	bránit	k5eAaImIp3nP	bránit
potravu	potrava	k1gFnSc4	potrava
<g/>
...	...	k?	...
Obyčejně	obyčejně	k6eAd1	obyčejně
lví	lví	k2eAgMnPc1d1	lví
samci	samec	k1gMnPc1	samec
netolerují	tolerovat	k5eNaImIp3nP	tolerovat
samce	samec	k1gInPc4	samec
z	z	k7c2	z
jiných	jiný	k2eAgNnPc2d1	jiné
teritorií	teritorium	k1gNnPc2	teritorium
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
nesnesou	snést	k5eNaPmIp3nP	snést
přítomnost	přítomnost	k1gFnSc4	přítomnost
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
,	,	kIx,	,
cizí	cizí	k2eAgFnPc1d1	cizí
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
tráví	trávit	k5eAaImIp3nS	trávit
odpočíváním	odpočívání	k1gNnSc7	odpočívání
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
20	[number]	k4	20
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Lvi	lev	k1gMnPc1	lev
se	se	k3xPyFc4	se
páří	pářit	k5eAaImIp3nP	pářit
v	v	k7c6	v
jakémkoliv	jakýkoliv	k3yIgNnSc6	jakýkoliv
roční	roční	k2eAgNnSc1d1	roční
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
mívají	mívat	k5eAaImIp3nP	mívat
estrální	estrální	k2eAgInPc4d1	estrální
cykly	cyklus	k1gInPc4	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
souboje	souboj	k1gInSc2	souboj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
páření	páření	k1gNnSc3	páření
doprovází	doprovázet	k5eAaImIp3nP	doprovázet
a	a	k8xC	a
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
i	i	k9	i
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
kopuluje	kopulovat	k5eAaImIp3nS	kopulovat
pár	pár	k4xCyI	pár
pravidelně	pravidelně	k6eAd1	pravidelně
20	[number]	k4	20
<g/>
×	×	k?	×
až	až	k9	až
40	[number]	k4	40
<g/>
×	×	k?	×
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
také	také	k9	také
zřídka	zřídka	k6eAd1	zřídka
pořádají	pořádat	k5eAaImIp3nP	pořádat
větší	veliký	k2eAgInPc4d2	veliký
lovy	lov	k1gInPc4	lov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
lvi	lev	k1gMnPc1	lev
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
velice	velice	k6eAd1	velice
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
samice	samice	k1gFnSc1	samice
trvá	trvat	k5eAaImIp3nS	trvat
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
porodí	porodit	k5eAaPmIp3nS	porodit
1	[number]	k4	1
až	až	k8xS	až
4	[number]	k4	4
mláďata	mládě	k1gNnPc1	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
ve	v	k7c6	v
smečce	smečka	k1gFnSc6	smečka
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
můžou	můžou	k?	můžou
vzájemně	vzájemně	k6eAd1	vzájemně
starat	starat	k5eAaImF	starat
o	o	k7c4	o
krmení	krmení	k1gNnSc4	krmení
a	a	k8xC	a
chování	chování	k1gNnSc4	chování
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Lvíčata	lvíče	k1gNnPc1	lvíče
jsou	být	k5eAaImIp3nP	být
odstavena	odstavit	k5eAaPmNgNnP	odstavit
po	po	k7c4	po
šesti	šest	k4xCc2	šest
až	až	k9	až
sedmi	sedm	k4xCc2	sedm
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
souboje	souboj	k1gInPc1	souboj
o	o	k7c4	o
jídlo	jídlo	k1gNnSc4	jídlo
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
divočině	divočina	k1gFnSc6	divočina
na	na	k7c6	na
denním	denní	k2eAgInSc6d1	denní
pořádku	pořádek	k1gInSc6	pořádek
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
až	až	k9	až
80	[number]	k4	80
%	%	kIx~	%
mláďat	mládě	k1gNnPc2	mládě
nedožije	dožít	k5eNaPmIp3nS	dožít
ani	ani	k8xC	ani
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
<g/>
..	..	k?	..
Lvíčata	lvíče	k1gNnPc1	lvíče
mají	mít	k5eAaImIp3nP	mít
mnoho	mnoho	k6eAd1	mnoho
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Nejnebezpečnější	bezpečný	k2eNgFnSc1d3	nejnebezpečnější
je	být	k5eAaImIp3nS	být
hyena	hyena	k1gFnSc1	hyena
<g/>
.	.	kIx.	.
</s>
<s>
Lvíčata	lvíče	k1gNnPc1	lvíče
jsou	být	k5eAaImIp3nP	být
skvrnitá	skvrnitý	k2eAgNnPc1d1	skvrnité
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupem	postup	k1gInSc7	postup
věku	věk	k1gInSc2	věk
skvrny	skvrna	k1gFnSc2	skvrna
mizí	mizet	k5eAaImIp3nS	mizet
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
začíná	začínat	k5eAaImIp3nS	začínat
mladým	mladý	k2eAgMnPc3d1	mladý
samcům	samec	k1gMnPc3	samec
vyrůstat	vyrůstat	k5eAaImF	vyrůstat
hříva	hříva	k1gFnSc1	hříva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
samce	samec	k1gMnPc4	samec
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
zraněním	zranění	k1gNnSc7	zranění
v	v	k7c6	v
soubojích	souboj	k1gInPc6	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nový	nový	k2eAgInSc1d1	nový
samec	samec	k1gInSc1	samec
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
koalice	koalice	k1gFnSc1	koalice
<g/>
)	)	kIx)	)
převezme	převzít	k5eAaPmIp3nS	převzít
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
smečkou	smečka	k1gFnSc7	smečka
původním	původní	k2eAgMnPc3d1	původní
samcům	samec	k1gInPc3	samec
<g/>
,	,	kIx,	,
zabije	zabít	k5eAaPmIp3nS	zabít
většinou	většinou	k6eAd1	většinou
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
chování	chování	k1gNnSc1	chování
je	být	k5eAaImIp3nS	být
vysvětleno	vysvětlit	k5eAaPmNgNnS	vysvětlit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
samice	samice	k1gFnSc1	samice
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
stanou	stanout	k5eAaPmIp3nP	stanout
plodnými	plodný	k2eAgFnPc7d1	plodná
až	až	k8xS	až
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
jejich	jejich	k3xOp3gNnPc1	jejich
mláďata	mládě	k1gNnPc1	mládě
dospějí	dochvít	k5eAaPmIp3nP	dochvít
nebo	nebo	k8xC	nebo
zemřou	zemřít	k5eAaPmIp3nP	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
kolem	kolem	k7c2	kolem
věku	věk	k1gInSc2	věk
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
a	a	k8xC	a
stávají	stávat	k5eAaImIp3nP	stávat
se	se	k3xPyFc4	se
schopnými	schopný	k2eAgMnPc7d1	schopný
převzít	převzít	k5eAaPmF	převzít
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
jinou	jiný	k2eAgFnSc7d1	jiná
smečkou	smečka	k1gFnSc7	smečka
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Stárnou	stárnout	k5eAaImIp3nP	stárnout
a	a	k8xC	a
slábnou	slábnout	k5eAaImIp3nP	slábnout
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
8	[number]	k4	8
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
věk	věk	k1gInSc1	věk
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
jen	jen	k9	jen
malou	malý	k2eAgFnSc4d1	malá
naději	naděje	k1gFnSc4	naděje
pro	pro	k7c4	pro
narození	narození	k1gNnSc4	narození
a	a	k8xC	a
dospívání	dospívání	k1gNnSc4	dospívání
vlastního	vlastní	k2eAgNnSc2d1	vlastní
potomstva	potomstvo	k1gNnSc2	potomstvo
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
začít	začít	k5eAaPmF	začít
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
hned	hned	k6eAd1	hned
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
smečky	smečka	k1gFnSc2	smečka
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
se	se	k3xPyFc4	se
často	často	k6eAd1	často
snaží	snažit	k5eAaImIp3nS	snažit
svá	svůj	k3xOyFgNnPc4	svůj
mláďata	mládě	k1gNnPc4	mládě
před	před	k7c7	před
lvím	lví	k2eAgInSc7d1	lví
samcem	samec	k1gInSc7	samec
uchránit	uchránit	k5eAaPmF	uchránit
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
málokdy	málokdy	k6eAd1	málokdy
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
většinou	většinou	k6eAd1	většinou
nejdříve	dříve	k6eAd3	dříve
zabije	zabít	k5eAaPmIp3nS	zabít
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
mladší	mladý	k2eAgFnSc4d2	mladší
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
matky	matka	k1gFnPc1	matka
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
slabé	slabý	k2eAgFnPc1d1	slabá
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnSc4	on
ochránily	ochránit	k5eAaPmAgFnP	ochránit
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
uspějí	uspět	k5eAaPmIp3nP	uspět
většinou	většinou	k6eAd1	většinou
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
alespoň	alespoň	k9	alespoň
čtyři	čtyři	k4xCgInPc4	čtyři
proti	proti	k7c3	proti
jednomu	jeden	k4xCgInSc3	jeden
lvímu	lví	k2eAgInSc3d1	lví
samci	samec	k1gInSc3	samec
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgNnPc4	dva
pohlaví	pohlaví	k1gNnPc4	pohlaví
mohou	moct	k5eAaImIp3nP	moct
vykazovat	vykazovat	k5eAaImF	vykazovat
znaky	znak	k1gInPc4	znak
homosexuality	homosexualita	k1gFnSc2	homosexualita
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Lví	lví	k2eAgMnPc1d1	lví
samci	samec	k1gMnPc1	samec
spolu	spolu	k6eAd1	spolu
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
pouto	pouto	k1gNnSc4	pouto
<g/>
,	,	kIx,	,
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
o	o	k7c4	o
sebe	sebe	k3xPyFc4	sebe
starají	starat	k5eAaImIp3nP	starat
a	a	k8xC	a
tisknou	tisknout	k5eAaImIp3nP	tisknout
se	se	k3xPyFc4	se
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Samičí	samičí	k2eAgFnSc1d1	samičí
homosexualita	homosexualita	k1gFnSc1	homosexualita
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpoznatelní	rozpoznatelný	k2eAgMnPc1d1	rozpoznatelný
podle	podle	k7c2	podle
hřívy	hříva	k1gFnSc2	hříva
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
vážit	vážit	k5eAaImF	vážit
150	[number]	k4	150
<g/>
–	–	k?	–
<g/>
250	[number]	k4	250
kg	kg	kA	kg
<g/>
;	;	kIx,	;
samci	samec	k1gInSc6	samec
vážící	vážící	k2eAgNnSc1d1	vážící
přes	přes	k7c4	přes
200	[number]	k4	200
kg	kg	kA	kg
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
poměrně	poměrně	k6eAd1	poměrně
vzácní	vzácný	k2eAgMnPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
lví	lví	k2eAgMnPc1d1	lví
samci	samec	k1gMnPc1	samec
tedy	tedy	k9	tedy
váží	vážit	k5eAaImIp3nP	vážit
průměrně	průměrně	k6eAd1	průměrně
asi	asi	k9	asi
180	[number]	k4	180
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
hmotnosti	hmotnost	k1gFnPc1	hmotnost
90	[number]	k4	90
<g/>
–	–	k?	–
<g/>
165	[number]	k4	165
kg	kg	kA	kg
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k8xC	i
nepatrně	patrně	k6eNd1	patrně
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
africký	africký	k2eAgInSc1d1	africký
lev	lev	k1gInSc1	lev
držený	držený	k2eAgInSc1d1	držený
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
vážil	vážit	k5eAaImAgMnS	vážit
366	[number]	k4	366
kg	kg	kA	kg
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
obézního	obézní	k2eAgMnSc4d1	obézní
jedince	jedinec	k1gMnSc4	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
délky	délka	k1gFnSc2	délka
170	[number]	k4	170
<g/>
–	–	k?	–
<g/>
250	[number]	k4	250
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
zhruba	zhruba	k6eAd1	zhruba
140	[number]	k4	140
<g/>
–	–	k?	–
<g/>
175	[number]	k4	175
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
měří	měřit	k5eAaImIp3nS	měřit
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
cm	cm	kA	cm
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zakončen	zakončit	k5eAaPmNgInS	zakončit
střapcem	střapec	k1gInSc7	střapec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
zhruba	zhruba	k6eAd1	zhruba
5	[number]	k4	5
mm	mm	kA	mm
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
bodec	bodec	k1gInSc4	bodec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
divočině	divočina	k1gFnSc6	divočina
se	se	k3xPyFc4	se
lvi	lev	k1gMnPc1	lev
dožívají	dožívat	k5eAaImIp3nP	dožívat
až	až	k9	až
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Kožich	kožich	k1gInSc1	kožich
bývá	bývat	k5eAaImIp3nS	bývat
zbarven	zbarvit	k5eAaPmNgInS	zbarvit
různě	různě	k6eAd1	různě
–	–	k?	–
Od	od	k7c2	od
světle	světle	k6eAd1	světle
žluté	žlutý	k2eAgFnSc2d1	žlutá
přes	přes	k7c4	přes
načervenalou	načervenalý	k2eAgFnSc4d1	načervenalá
až	až	k6eAd1	až
po	po	k7c4	po
tmavě	tmavě	k6eAd1	tmavě
hnědou	hnědý	k2eAgFnSc4d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Hříva	hříva	k1gFnSc1	hříva
samců	samec	k1gMnPc2	samec
bývá	bývat	k5eAaImIp3nS	bývat
světle	světle	k6eAd1	světle
žlutá	žlutý	k2eAgFnSc1d1	žlutá
až	až	k8xS	až
černá	černý	k2eAgFnSc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Břišní	břišní	k2eAgFnPc1d1	břišní
partie	partie	k1gFnPc1	partie
bývají	bývat	k5eAaImIp3nP	bývat
světlejší	světlý	k2eAgFnPc1d2	světlejší
<g/>
,	,	kIx,	,
střapec	střapec	k1gInSc1	střapec
na	na	k7c6	na
ocasu	ocas	k1gInSc6	ocas
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
černý	černý	k2eAgInSc1d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
vědci	vědec	k1gMnPc1	vědec
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
hustoty	hustota	k1gFnSc2	hustota
hřívy	hříva	k1gFnSc2	hříva
daly	dát	k5eAaPmAgInP	dát
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
poddruhy	poddruh	k1gInPc1	poddruh
odlišit	odlišit	k5eAaPmF	odlišit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opak	opak	k1gInSc1	opak
je	být	k5eAaImIp3nS	být
pravdou	pravda	k1gFnSc7	pravda
<g/>
,	,	kIx,	,
barva	barva	k1gFnSc1	barva
a	a	k8xC	a
velikost	velikost	k1gFnSc1	velikost
závisí	záviset	k5eAaImIp3nS	záviset
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
lev	lev	k1gMnSc1	lev
žije	žít	k5eAaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Lvi	lev	k1gMnPc1	lev
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
teplotou	teplota	k1gFnSc7	teplota
(	(	kIx(	(
<g/>
např.	např.	kA	např.
evropské	evropský	k2eAgFnPc4d1	Evropská
a	a	k8xC	a
severoamerické	severoamerický	k2eAgFnPc4d1	severoamerická
zoo	zoo	k1gFnPc4	zoo
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
hřívu	hříva	k1gFnSc4	hříva
hustější	hustý	k2eAgFnSc1d2	hustší
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
tmavěji	tmavě	k6eAd2	tmavě
zbarvenou	zbarvený	k2eAgFnSc7d1	zbarvená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Tsavo	Tsavo	k1gNnSc4	Tsavo
v	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
a	a	k8xC	a
v	v	k7c6	v
Senegalu	Senegal	k1gInSc6	Senegal
žilo	žít	k5eAaImAgNnS	žít
několik	několik	k4yIc1	několik
samců	samec	k1gInPc2	samec
lva	lev	k1gMnSc2	lev
bez	bez	k7c2	bez
hřívy	hříva	k1gFnSc2	hříva
<g/>
.	.	kIx.	.
</s>
<s>
Absence	absence	k1gFnSc1	absence
hřívy	hříva	k1gFnSc2	hříva
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
jak	jak	k6eAd1	jak
klimatickými	klimatický	k2eAgFnPc7d1	klimatická
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
,	,	kIx,	,
tak	tak	k9	tak
také	také	k9	také
sexuálním	sexuální	k2eAgNnSc7d1	sexuální
dospíváním	dospívání	k1gNnSc7	dospívání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
produkcí	produkce	k1gFnSc7	produkce
testosteronu	testosteron	k1gInSc2	testosteron
<g/>
.	.	kIx.	.
</s>
<s>
Vykastrovaní	vykastrovaný	k2eAgMnPc1d1	vykastrovaný
lvi	lev	k1gMnPc1	lev
mívají	mívat	k5eAaImIp3nP	mívat
hřívy	hříva	k1gFnPc4	hříva
menší	malý	k2eAgFnPc4d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
hříva	hříva	k1gFnSc1	hříva
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
u	u	k7c2	u
lva	lev	k1gMnSc2	lev
také	také	k6eAd1	také
znakem	znak	k1gInSc7	znak
genetického	genetický	k2eAgInSc2d1	genetický
a	a	k8xC	a
fyzického	fyzický	k2eAgNnSc2d1	fyzické
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
,	,	kIx,	,
také	také	k6eAd1	také
mu	on	k3xPp3gMnSc3	on
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
jistou	jistý	k2eAgFnSc4d1	jistá
ochranu	ochrana	k1gFnSc4	ochrana
při	při	k7c6	při
soubojích	souboj	k1gInPc6	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
však	však	k9	však
lev	lev	k1gMnSc1	lev
více	hodně	k6eAd2	hodně
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
poddruhů	poddruh	k1gInPc2	poddruh
dávají	dávat	k5eAaImIp3nP	dávat
samice	samice	k1gFnPc1	samice
přednost	přednost	k1gFnSc4	přednost
lvu	lev	k1gInSc6	lev
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
a	a	k8xC	a
hustou	hustý	k2eAgFnSc7d1	hustá
hřívou	hříva	k1gFnSc7	hříva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
znakem	znak	k1gInSc7	znak
dobré	dobrý	k2eAgFnSc2d1	dobrá
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
páření	páření	k1gNnSc6	páření
se	se	k3xPyFc4	se
samci	samec	k1gMnPc1	samec
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
hřívou	hříva	k1gFnSc7	hříva
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
aktivní	aktivní	k2eAgMnPc1d1	aktivní
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
nebyla	být	k5eNaImAgFnS	být
vědecky	vědecky	k6eAd1	vědecky
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc4d3	nejstarší
fosilní	fosilní	k2eAgInPc4d1	fosilní
nálezy	nález	k1gInPc4	nález
kočky	kočka	k1gFnPc1	kočka
připomínající	připomínající	k2eAgFnPc1d1	připomínající
lva	lev	k1gInSc2	lev
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
z	z	k7c2	z
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
<g/>
,	,	kIx,	,
stáří	stáří	k1gNnSc1	stáří
je	být	k5eAaImIp3nS	být
odhadováno	odhadovat	k5eAaImNgNnS	odhadovat
na	na	k7c4	na
3,5	[number]	k4	3,5
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
původní	původní	k2eAgInSc4d1	původní
druh	druh	k1gInSc4	druh
lva	lev	k1gMnSc2	lev
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
většina	většina	k1gFnSc1	většina
se	se	k3xPyFc4	se
shodla	shodnout	k5eAaBmAgFnS	shodnout
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
šelmu	šelma	k1gFnSc4	šelma
lvu	lev	k1gInSc6	lev
podobnou	podobný	k2eAgFnSc4d1	podobná
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
známé	známý	k2eAgInPc1d1	známý
nálezy	nález	k1gInPc1	nález
druhu	druh	k1gInSc2	druh
Panthera	Panther	k1gMnSc4	Panther
leo	leo	k?	leo
byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
stáří	stáří	k1gNnSc1	stáří
je	být	k5eAaImIp3nS	být
odhadováno	odhadovat	k5eAaImNgNnS	odhadovat
na	na	k7c4	na
700	[number]	k4	700
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
před	před	k7c7	před
300	[number]	k4	300
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
jako	jako	k9	jako
poddruh	poddruh	k1gInSc1	poddruh
Panthera	Panther	k1gMnSc2	Panther
leo	leo	k?	leo
fossilis	fossilis	k1gFnSc2	fossilis
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
nálezu	nález	k1gInSc2	nález
je	být	k5eAaImIp3nS	být
také	také	k9	také
odvozený	odvozený	k2eAgInSc1d1	odvozený
poddruh	poddruh	k1gInSc1	poddruh
Panthera	Panther	k1gMnSc2	Panther
leo	leo	k?	leo
speleae	spelea	k1gMnSc2	spelea
–	–	k?	–
lev	lev	k1gMnSc1	lev
jeskynní	jeskynní	k2eAgMnSc1d1	jeskynní
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
také	také	k9	také
začal	začít	k5eAaPmAgInS	začít
objevovat	objevovat	k5eAaImF	objevovat
před	před	k7c7	před
300	[number]	k4	300
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
pleistocénu	pleistocén	k1gInSc2	pleistocén
se	se	k3xPyFc4	se
lvi	lev	k1gMnPc1	lev
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
také	také	k9	také
na	na	k7c6	na
území	území	k1gNnSc6	území
Severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nový	nový	k2eAgInSc1d1	nový
poddruh	poddruh	k1gInSc1	poddruh
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
atrox	atrox	k1gInSc1	atrox
–	–	k?	–
lev	lev	k1gInSc1	lev
americký	americký	k2eAgMnSc1d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Eurasii	Eurasie	k1gFnSc6	Eurasie
a	a	k8xC	a
Americe	Amerika	k1gFnSc6	Amerika
byli	být	k5eAaImAgMnP	být
lvi	lev	k1gMnPc1	lev
běžní	běžný	k2eAgMnPc1d1	běžný
<g/>
,	,	kIx,	,
vymřeli	vymřít	k5eAaPmAgMnP	vymřít
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnPc1d1	ledová
–	–	k?	–
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
10	[number]	k4	10
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
uznáváno	uznávat	k5eAaImNgNnS	uznávat
osm	osm	k4xCc1	osm
poddruhů	poddruh	k1gInPc2	poddruh
lva	lev	k1gMnSc2	lev
(	(	kIx(	(
<g/>
poddruhy	poddruh	k1gInPc1	poddruh
vyhynulé	vyhynulý	k2eAgInPc1d1	vyhynulý
již	již	k6eAd1	již
v	v	k7c6	v
pleistocénu	pleistocén	k1gInSc6	pleistocén
nezapočítáváme	započítávat	k5eNaImIp1nP	započítávat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
poddruhy	poddruh	k1gInPc7	poddruh
tkví	tkvět	k5eAaImIp3nP	tkvět
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
rozšíření	rozšíření	k1gNnSc6	rozšíření
a	a	k8xC	a
velikosti	velikost	k1gFnSc6	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
však	však	k9	však
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
poddruhy	poddruh	k1gInPc7	poddruh
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
specifické	specifický	k2eAgFnPc1d1	specifická
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
u	u	k7c2	u
tygrů	tygr	k1gMnPc2	tygr
či	či	k8xC	či
levhartů	levhart	k1gMnPc2	levhart
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
poddruhy	poddruh	k1gInPc1	poddruh
lva	lev	k1gMnSc2	lev
neuznávají	uznávat	k5eNaImIp3nP	uznávat
a	a	k8xC	a
považují	považovat	k5eAaImIp3nP	považovat
lva	lev	k1gMnSc4	lev
za	za	k7c4	za
monotypický	monotypický	k2eAgInSc4d1	monotypický
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
azandica	azandica	k1gMnSc1	azandica
–	–	k?	–
lev	lev	k1gMnSc1	lev
severokonžský	severokonžský	k2eAgMnSc1d1	severokonžský
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
nevelké	velký	k2eNgFnSc6d1	nevelká
oblasti	oblast	k1gFnSc6	oblast
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Konga	Kongo	k1gNnSc2	Kongo
<g/>
.	.	kIx.	.
</s>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
bleyenberghi	bleyenbergh	k1gFnSc2	bleyenbergh
–	–	k?	–
lev	lev	k1gMnSc1	lev
jihokonžský	jihokonžský	k2eAgMnSc1d1	jihokonžský
či	či	k8xC	či
lev	lev	k1gMnSc1	lev
katanžský	katanžský	k2eAgMnSc1d1	katanžský
Rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Konga	Kongo	k1gNnSc2	Kongo
(	(	kIx(	(
<g/>
provincie	provincie	k1gFnSc1	provincie
Katanga	Katanga	k1gFnSc1	Katanga
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Angole	Angola	k1gFnSc6	Angola
<g/>
,	,	kIx,	,
Zambii	Zambie	k1gFnSc6	Zambie
<g/>
,	,	kIx,	,
Zimbabwe	Zimbabwe	k1gFnSc6	Zimbabwe
<g/>
,	,	kIx,	,
Botswaně	Botswana	k1gFnSc6	Botswana
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Namibii	Namibie	k1gFnSc6	Namibie
<g/>
.	.	kIx.	.
</s>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
krugeri	kruger	k1gFnSc2	kruger
–	–	k?	–
lev	lev	k1gMnSc1	lev
jihoafrický	jihoafrický	k2eAgMnSc1d1	jihoafrický
či	či	k8xC	či
lev	lev	k1gMnSc1	lev
Krugerův	Krugerův	k2eAgMnSc1d1	Krugerův
Rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
v	v	k7c6	v
severovýchodních	severovýchodní	k2eAgFnPc6d1	severovýchodní
oblastech	oblast	k1gFnPc6	oblast
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
Transvaal	Transvaal	k1gInSc1	Transvaal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Krugerova	Krugerův	k2eAgInSc2d1	Krugerův
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
leo	leo	k?	leo
(	(	kIx(	(
<g/>
P.	P.	kA	P.
l.	l.	k?	l.
berberisca	berberisca	k1gFnSc1	berberisca
<g/>
)	)	kIx)	)
–	–	k?	–
lev	lev	k1gMnSc1	lev
berberský	berberský	k2eAgMnSc1d1	berberský
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyhuben	vyhuben	k2eAgMnSc1d1	vyhuben
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnSc1d1	poslední
divoký	divoký	k2eAgMnSc1d1	divoký
jedinec	jedinec	k1gMnSc1	jedinec
byl	být	k5eAaImAgMnS	být
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
<g/>
.	.	kIx.	.
</s>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
byl	být	k5eAaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
zcela	zcela	k6eAd1	zcela
vyhubený	vyhubený	k2eAgInSc4d1	vyhubený
<g/>
,	,	kIx,	,
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
však	však	k9	však
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
několik	několik	k4yIc1	několik
zvířat	zvíře	k1gNnPc2	zvíře
přežilo	přežít	k5eAaPmAgNnS	přežít
v	v	k7c6	v
soukromém	soukromý	k2eAgInSc6d1	soukromý
zvěřinci	zvěřinec	k1gMnPc1	zvěřinec
marockého	marocký	k2eAgMnSc2d1	marocký
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
záchrannému	záchranný	k2eAgInSc3d1	záchranný
programu	program	k1gInSc3	program
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
lev	lev	k1gInSc4	lev
berberský	berberský	k2eAgInSc4d1	berberský
chován	chován	k2eAgInSc4d1	chován
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zoo	zoo	k1gFnSc2	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
však	však	k9	však
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
čistokrevné	čistokrevný	k2eAgMnPc4d1	čistokrevný
jedince	jedinec	k1gMnPc4	jedinec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c6	o
křížence	kříženka	k1gFnSc6	kříženka
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
poddruhy	poddruh	k1gInPc7	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
melanochaita	melanochaita	k1gMnSc1	melanochaita
–	–	k?	–
lev	lev	k1gMnSc1	lev
kapský	kapský	k2eAgMnSc1d1	kapský
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vymřel	vymřít	k5eAaPmAgMnS	vymřít
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačoval	vyznačovat	k5eAaImAgInS	vyznačovat
se	se	k3xPyFc4	se
mohutným	mohutný	k2eAgInSc7d1	mohutný
vzrůstem	vzrůst	k1gInSc7	vzrůst
a	a	k8xC	a
bohatou	bohatý	k2eAgFnSc7d1	bohatá
hřívou	hříva	k1gFnSc7	hříva
sahající	sahající	k2eAgInSc1d1	sahající
až	až	k6eAd1	až
na	na	k7c4	na
lopatky	lopatka	k1gFnPc4	lopatka
a	a	k8xC	a
přítomnou	přítomný	k2eAgFnSc4d1	přítomná
též	též	k9	též
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
analýzy	analýza	k1gFnSc2	analýza
mitochondriální	mitochondriální	k2eAgFnSc2d1	mitochondriální
DNA	DNA	kA	DNA
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
samostatný	samostatný	k2eAgInSc4d1	samostatný
poddruh	poddruh	k1gInSc4	poddruh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
nejjižnější	jižní	k2eAgFnSc6d3	nejjižnější
populaci	populace	k1gFnSc6	populace
poddruhu	poddruh	k1gInSc2	poddruh
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
krugeri	kruger	k1gFnSc2	kruger
(	(	kIx(	(
<g/>
lev	lev	k1gMnSc1	lev
jihoafrický	jihoafrický	k2eAgMnSc1d1	jihoafrický
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
nubica	nubica	k6eAd1	nubica
–	–	k?	–
lev	lev	k1gInSc1	lev
východoafrický	východoafrický	k2eAgInSc1d1	východoafrický
Nejpočetnější	početní	k2eAgInSc1d3	nejpočetnější
ze	z	k7c2	z
současných	současný	k2eAgInPc2d1	současný
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
východní	východní	k2eAgFnSc4d1	východní
Afriku	Afrika	k1gFnSc4	Afrika
od	od	k7c2	od
Etiopie	Etiopie	k1gFnSc2	Etiopie
až	až	k9	až
po	po	k7c4	po
Mosambik	Mosambik	k1gInSc4	Mosambik
<g/>
.	.	kIx.	.
</s>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
persica	persica	k1gFnSc1	persica
(	(	kIx(	(
<g/>
P.	P.	kA	P.
l.	l.	k?	l.
goojratensis	goojratensis	k1gInSc1	goojratensis
<g/>
)	)	kIx)	)
–	–	k?	–
lev	lev	k1gMnSc1	lev
indický	indický	k2eAgMnSc1d1	indický
nebo	nebo	k8xC	nebo
také	také	k9	také
lev	lev	k1gMnSc1	lev
perský	perský	k2eAgMnSc1d1	perský
či	či	k8xC	či
lev	lev	k1gMnSc1	lev
asijský	asijský	k2eAgMnSc1d1	asijský
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
vyskytoval	vyskytovat	k5eAaImAgMnS	vyskytovat
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
v	v	k7c6	v
Pákistánu	Pákistán	k1gInSc6	Pákistán
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
Bangladéši	Bangladéš	k1gInSc6	Bangladéš
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
loví	lovit	k5eAaImIp3nP	lovit
ve	v	k7c6	v
dne	den	k1gInSc2	den
a	a	k8xC	a
ve	v	k7c6	v
smečkách	smečka	k1gFnPc6	smečka
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
terčem	terč	k1gInSc7	terč
lovců	lovec	k1gMnPc2	lovec
i	i	k8xC	i
pytláků	pytlák	k1gMnPc2	pytlák
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
byl	být	k5eAaImAgInS	být
snadnějším	snadný	k2eAgInSc7d2	snadnější
úlovkem	úlovek	k1gInSc7	úlovek
než	než	k8xS	než
tygr	tygr	k1gMnSc1	tygr
či	či	k8xC	či
leopard	leopard	k1gMnSc1	leopard
<g/>
.	.	kIx.	.
</s>
<s>
Extenzivní	extenzivní	k2eAgInSc1d1	extenzivní
lov	lov	k1gInSc1	lov
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
poddruh	poddruh	k1gInSc1	poddruh
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
svého	svůj	k3xOyFgInSc2	svůj
původního	původní	k2eAgInSc2d1	původní
areálu	areál	k1gInSc2	areál
vyhuben	vyhubit	k5eAaPmNgInS	vyhubit
<g/>
.	.	kIx.	.
</s>
<s>
Posledních	poslední	k2eAgInPc2d1	poslední
350	[number]	k4	350
<g/>
–	–	k?	–
<g/>
400	[number]	k4	400
jedinců	jedinec	k1gMnPc2	jedinec
dnes	dnes	k6eAd1	dnes
přežívá	přežívat	k5eAaImIp3nS	přežívat
poblíž	poblíž	k6eAd1	poblíž
Gir	Gir	k1gMnSc1	Gir
Forest	Forest	k1gMnSc1	Forest
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
senegalensis	senegalensis	k1gFnSc2	senegalensis
–	–	k?	–
lev	lev	k1gMnSc1	lev
senegalský	senegalský	k2eAgMnSc1d1	senegalský
či	či	k8xC	či
lev	lev	k1gMnSc1	lev
západoafrický	západoafrický	k2eAgMnSc1d1	západoafrický
Obývá	obývat	k5eAaImIp3nS	obývat
západní	západní	k2eAgFnSc4d1	západní
Afriku	Afrika	k1gFnSc4	Afrika
od	od	k7c2	od
Senegalu	Senegal	k1gInSc2	Senegal
až	až	k9	až
po	po	k7c4	po
Středoafrickou	středoafrický	k2eAgFnSc4d1	Středoafrická
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
spelaea	spelaea	k6eAd1	spelaea
–	–	k?	–
lev	lev	k1gInSc4	lev
jeskynní	jeskynní	k2eAgFnSc2d1	jeskynní
Obýval	obývat	k5eAaImAgMnS	obývat
Eurasii	Eurasie	k1gFnSc4	Eurasie
v	v	k7c6	v
období	období	k1gNnSc6	období
před	před	k7c7	před
300	[number]	k4	300
000	[number]	k4	000
–	–	k?	–
10	[number]	k4	10
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
kromě	kromě	k7c2	kromě
fosilních	fosilní	k2eAgMnPc2d1	fosilní
nálezů	nález	k1gInPc2	nález
i	i	k9	i
z	z	k7c2	z
jeskynních	jeskynní	k2eAgFnPc2d1	jeskynní
maleb	malba	k1gFnPc2	malba
<g/>
,	,	kIx,	,
slonovinových	slonovinový	k2eAgFnPc2d1	slonovinová
řezeb	řezba	k1gFnPc2	řezba
a	a	k8xC	a
hliněných	hliněný	k2eAgFnPc2d1	hliněná
sošek	soška	k1gFnPc2	soška
pravěkých	pravěký	k2eAgMnPc2d1	pravěký
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nových	nový	k2eAgNnPc2d1	nové
studií	studio	k1gNnPc2	studio
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
Panthera	Panthera	k1gFnSc1	Panthera
spelaea	spelaea	k1gFnSc1	spelaea
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
linie	linie	k1gFnSc1	linie
se	se	k3xPyFc4	se
oddělila	oddělit	k5eAaPmAgFnS	oddělit
od	od	k7c2	od
Panthera	Panther	k1gMnSc2	Panther
leo	leo	k?	leo
již	již	k6eAd1	již
před	před	k7c7	před
necelými	celý	k2eNgNnPc7d1	necelé
2	[number]	k4	2
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
fossilis	fossilis	k1gInSc1	fossilis
Fosilní	fosilní	k2eAgInSc1d1	fosilní
poddruh	poddruh	k1gInSc4	poddruh
známý	známý	k2eAgInSc4d1	známý
z	z	k7c2	z
kosterních	kosterní	k2eAgInPc2d1	kosterní
nálezů	nález	k1gInPc2	nález
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
pleistocénu	pleistocén	k1gInSc6	pleistocén
před	před	k7c7	před
500	[number]	k4	500
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nové	nový	k2eAgFnSc2d1	nová
studie	studie	k1gFnSc2	studie
jde	jít	k5eAaImIp3nS	jít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
formu	forma	k1gFnSc4	forma
druhu	druh	k1gInSc2	druh
Panthera	Panther	k1gMnSc2	Panther
spelaea	spelaeus	k1gMnSc2	spelaeus
<g/>
.	.	kIx.	.
</s>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
atrox	atrox	k1gInSc1	atrox
–	–	k?	–
lev	lev	k1gMnSc1	lev
americký	americký	k2eAgMnSc1d1	americký
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejmohutnějších	mohutný	k2eAgInPc2d3	nejmohutnější
známých	známý	k2eAgInPc2d1	známý
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgFnSc1d1	obývající
v	v	k7c6	v
pleistocénu	pleistocén	k1gInSc6	pleistocén
americký	americký	k2eAgInSc4d1	americký
kontinent	kontinent	k1gInSc4	kontinent
od	od	k7c2	od
Aljašky	Aljaška	k1gFnSc2	Aljaška
až	až	k9	až
po	po	k7c6	po
Peru	Peru	k1gNnSc6	Peru
<g/>
.	.	kIx.	.
</s>
<s>
Vymřel	vymřít	k5eAaPmAgMnS	vymřít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
pleistocénu	pleistocén	k1gInSc2	pleistocén
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
10	[number]	k4	10
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
youngi	young	k1gFnSc2	young
nebo	nebo	k8xC	nebo
Panthera	Panthero	k1gNnSc2	Panthero
youngi	young	k1gFnSc2	young
Známý	známý	k2eAgMnSc1d1	známý
z	z	k7c2	z
fosilních	fosilní	k2eAgInPc2d1	fosilní
nálezů	nález	k1gInPc2	nález
ze	z	k7c2	z
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Obýval	obývat	k5eAaImAgMnS	obývat
tamní	tamní	k2eAgFnSc2d1	tamní
oblasti	oblast	k1gFnSc2	oblast
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
350	[number]	k4	350
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
postavení	postavení	k1gNnSc1	postavení
v	v	k7c6	v
taxonomii	taxonomie	k1gFnSc6	taxonomie
je	být	k5eAaImIp3nS	být
nejasné	jasný	k2eNgFnPc4d1	nejasná
–	–	k?	–
většinou	většinou	k6eAd1	většinou
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
poddruh	poddruh	k1gInSc4	poddruh
lva	lev	k1gMnSc2	lev
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
jej	on	k3xPp3gNnSc4	on
však	však	k9	však
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
primitivního	primitivní	k2eAgMnSc4d1	primitivní
tygra	tygr	k1gMnSc4	tygr
či	či	k8xC	či
levharta	levhart	k1gMnSc4	levhart
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
europaea	europaea	k1gMnSc1	europaea
–	–	k?	–
lev	lev	k1gMnSc1	lev
evropský	evropský	k2eAgMnSc1d1	evropský
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
Balkánský	balkánský	k2eAgInSc1d1	balkánský
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
,	,	kIx,	,
Apeninský	apeninský	k2eAgInSc1d1	apeninský
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Pyrenejský	pyrenejský	k2eAgInSc1d1	pyrenejský
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
loven	loven	k2eAgInSc1d1	loven
starověkými	starověký	k2eAgMnPc7d1	starověký
Řeky	Řek	k1gMnPc7	Řek
a	a	k8xC	a
Římany	Říman	k1gMnPc7	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Vyhuben	vyhuben	k2eAgInSc1d1	vyhuben
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
100	[number]	k4	100
n.	n.	k?	n.
l.	l.	k?	l.
Jako	jako	k8xC	jako
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
poddruh	poddruh	k1gInSc1	poddruh
nepotvrzen	potvrzen	k2eNgInSc1d1	nepotvrzen
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
identický	identický	k2eAgMnSc1d1	identický
buď	buď	k8xC	buď
se	s	k7c7	s
lvem	lev	k1gInSc7	lev
indickým	indický	k2eAgInSc7d1	indický
(	(	kIx(	(
<g/>
Panthera	Panthero	k1gNnPc1	Panthero
leo	leo	k?	leo
persica	persicus	k1gMnSc2	persicus
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
se	s	k7c7	s
lvem	lev	k1gInSc7	lev
jeskynním	jeskynní	k2eAgInSc7d1	jeskynní
(	(	kIx(	(
<g/>
Panthera	Panthero	k1gNnPc1	Panthero
leo	leo	k?	leo
spelaea	spelaeus	k1gMnSc2	spelaeus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Panthera	Panthera	k1gFnSc1	Panthera
leo	leo	k?	leo
sinhaleyus	sinhaleyus	k1gMnSc1	sinhaleyus
Údajně	údajně	k6eAd1	údajně
obýval	obývat	k5eAaImAgMnS	obývat
Srí	Srí	k1gFnSc4	Srí
Lanku	lanko	k1gNnSc3	lanko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
vyhuben	vyhubit	k5eAaPmNgInS	vyhubit
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
39	[number]	k4	39
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Doložen	doložen	k2eAgMnSc1d1	doložen
pouze	pouze	k6eAd1	pouze
dvěma	dva	k4xCgFnPc7	dva
nálezy	nález	k1gInPc1	nález
zubů	zub	k1gInPc2	zub
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
města	město	k1gNnSc2	město
Kuruwita	Kuruwitum	k1gNnSc2	Kuruwitum
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
různých	různý	k2eAgFnPc2d1	různá
genetických	genetický	k2eAgFnPc2d1	genetická
variací	variace	k1gFnPc2	variace
lva	lev	k1gInSc2	lev
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
o	o	k7c4	o
většinu	většina	k1gFnSc4	většina
se	se	k3xPyFc4	se
však	však	k9	však
zasloužily	zasloužit	k5eAaPmAgFnP	zasloužit
zoologické	zoologický	k2eAgFnPc1d1	zoologická
zahrady	zahrada	k1gFnPc1	zahrada
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jsou	být	k5eAaImIp3nP	být
vzácní	vzácný	k2eAgMnPc1d1	vzácný
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
občas	občas	k6eAd1	občas
hlášeni	hlásit	k5eAaImNgMnP	hlásit
v	v	k7c6	v
Timbavati	Timbavati	k1gFnPc6	Timbavati
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
albíny	albín	k1gMnPc4	albín
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
recesivním	recesivní	k2eAgInSc7d1	recesivní
genem	gen	k1gInSc7	gen
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgMnSc1d1	bílý
lev	lev	k1gMnSc1	lev
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nevýhodě	nevýhoda	k1gFnSc6	nevýhoda
při	při	k7c6	při
lovu	lov	k1gInSc3	lov
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
zbarvení	zbarvení	k1gNnSc3	zbarvení
může	moct	k5eAaImIp3nS	moct
totiž	totiž	k9	totiž
být	být	k5eAaImF	být
kořistí	kořistit	k5eAaImIp3nP	kořistit
velice	velice	k6eAd1	velice
lehce	lehko	k6eAd1	lehko
odhalen	odhalit	k5eAaPmNgMnS	odhalit
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
i	i	k8xC	i
dospělí	dospělý	k2eAgMnPc1d1	dospělý
jedinci	jedinec	k1gMnPc1	jedinec
jsou	být	k5eAaImIp3nP	být
čistě	čistě	k6eAd1	čistě
bílí	bílý	k2eAgMnPc1d1	bílý
<g/>
,	,	kIx,	,
ne	ne	k9	ne
jako	jako	k9	jako
běžní	běžný	k2eAgMnPc1d1	běžný
mladí	mladý	k2eAgMnPc1d1	mladý
lvi	lev	k1gMnPc1	lev
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
mají	mít	k5eAaImIp3nP	mít
barevné	barevný	k2eAgFnPc4d1	barevná
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
(	(	kIx(	(
<g/>
z	z	k7c2	z
heraldického	heraldický	k2eAgNnSc2d1	heraldické
hlediska	hledisko	k1gNnSc2	hledisko
stříbrný	stříbrný	k1gInSc1	stříbrný
<g/>
)	)	kIx)	)
lev	lev	k1gMnSc1	lev
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
českým	český	k2eAgInSc7d1	český
symbolem	symbol	k1gInSc7	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Lvi	lev	k1gMnPc1	lev
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
kříženi	křížit	k5eAaImNgMnP	křížit
s	s	k7c7	s
tygry	tygr	k1gMnPc7	tygr
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
sibiřským	sibiřský	k2eAgMnPc3d1	sibiřský
nebo	nebo	k8xC	nebo
bengálským	bengálský	k2eAgMnPc3d1	bengálský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
kříženci	kříženec	k1gMnPc1	kříženec
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
ligeři	liger	k1gMnPc1	liger
a	a	k8xC	a
tigoni	tigon	k1gMnPc1	tigon
(	(	kIx(	(
<g/>
anglické	anglický	k2eAgFnSc2d1	anglická
složeniny	složenina	k1gFnSc2	složenina
slov	slovo	k1gNnPc2	slovo
"	"	kIx"	"
<g/>
tiger	tiger	k1gInSc1	tiger
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
lion	lion	k?	lion
<g/>
"	"	kIx"	"
–	–	k?	–
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
zkratky	zkratka	k1gFnSc2	zkratka
značí	značit	k5eAaImIp3nS	značit
vždy	vždy	k6eAd1	vždy
živočišný	živočišný	k2eAgInSc1d1	živočišný
druh	druh	k1gInSc1	druh
samce	samec	k1gMnSc2	samec
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
značí	značit	k5eAaImIp3nS	značit
druh	druh	k1gInSc1	druh
samice	samice	k1gFnSc2	samice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lvi	lev	k1gMnPc1	lev
byli	být	k5eAaImAgMnP	být
také	také	k9	také
kříženi	křížen	k2eAgMnPc1d1	křížen
s	s	k7c7	s
levharty	levhart	k1gMnPc7	levhart
a	a	k8xC	a
jaguáry	jaguár	k1gMnPc7	jaguár
<g/>
,	,	kIx,	,
tito	tento	k3xDgMnPc1	tento
jedinci	jedinec	k1gMnPc1	jedinec
se	se	k3xPyFc4	se
nazývali	nazývat	k5eAaImAgMnP	nazývat
leoponi	leopon	k1gMnPc1	leopon
a	a	k8xC	a
jaglioni	jaglion	k1gMnPc1	jaglion
<g/>
.	.	kIx.	.
</s>
<s>
Liger	Liger	k1gMnSc1	Liger
je	být	k5eAaImIp3nS	být
kříženec	kříženec	k1gMnSc1	kříženec
samce	samec	k1gMnSc4	samec
lva	lev	k1gMnSc4	lev
a	a	k8xC	a
tygří	tygří	k2eAgFnSc1d1	tygří
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
otce	otec	k1gMnSc2	otec
i	i	k8xC	i
matky	matka	k1gFnSc2	matka
potlačeny	potlačen	k2eAgInPc1d1	potlačen
geny	gen	k1gInPc1	gen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
růst	růst	k1gInSc4	růst
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
liger	liger	k1gInSc1	liger
úctyhodných	úctyhodný	k2eAgInPc2d1	úctyhodný
rozměrů	rozměr	k1gInPc2	rozměr
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
tak	tak	k9	tak
největší	veliký	k2eAgFnSc7d3	veliký
kočkovitou	kočkovitý	k2eAgFnSc7d1	kočkovitá
šelmou	šelma	k1gFnSc7	šelma
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tygrem	tygr	k1gMnSc7	tygr
sdílí	sdílet	k5eAaImIp3nP	sdílet
pruhy	pruh	k1gInPc4	pruh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
se	s	k7c7	s
lvím	lví	k2eAgInSc7d1	lví
samcem	samec	k1gInSc7	samec
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zase	zase	k9	zase
hříva	hříva	k1gFnSc1	hříva
<g/>
.	.	kIx.	.
</s>
<s>
Ligeří	Ligerý	k2eAgMnPc1d1	Ligerý
samci	samec	k1gMnPc1	samec
nejsou	být	k5eNaImIp3nP	být
plodní	plodní	k2eAgMnPc1d1	plodní
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
většinou	většinou	k6eAd1	většinou
ano	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
mají	mít	k5eAaImIp3nP	mít
50	[number]	k4	50
<g/>
%	%	kIx~	%
šanci	šance	k1gFnSc4	šance
že	že	k8xS	že
se	se	k3xPyFc4	se
narodí	narodit	k5eAaPmIp3nS	narodit
s	s	k7c7	s
hřívou	hříva	k1gFnSc7	hříva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
jedinců	jedinec	k1gMnPc2	jedinec
opět	opět	k6eAd1	opět
jen	jen	k9	jen
50	[number]	k4	50
<g/>
%	%	kIx~	%
získá	získat	k5eAaPmIp3nS	získat
hřívu	hříva	k1gFnSc4	hříva
velikostí	velikost	k1gFnPc2	velikost
podobnou	podobný	k2eAgFnSc4d1	podobná
hřívě	hříva	k1gFnSc6	hříva
lva	lev	k1gMnSc2	lev
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
známou	známý	k2eAgFnSc7d1	známá
variací	variace	k1gFnSc7	variace
je	být	k5eAaImIp3nS	být
tigon	tigon	k1gMnSc1	tigon
<g/>
,	,	kIx,	,
kříženec	kříženec	k1gMnSc1	kříženec
tygřího	tygří	k2eAgInSc2d1	tygří
samce	samec	k1gInSc2	samec
a	a	k8xC	a
lví	lví	k2eAgFnSc2d1	lví
samice	samice	k1gFnSc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
se	se	k3xPyFc4	se
uplatnily	uplatnit	k5eAaPmAgInP	uplatnit
geny	gen	k1gInPc1	gen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
potlačují	potlačovat	k5eAaImIp3nP	potlačovat
růst	růst	k1gInSc4	růst
<g/>
,	,	kIx,	,
od	od	k7c2	od
obou	dva	k4xCgMnPc2	dva
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
tigon	tigon	k1gInSc4	tigon
menšího	malý	k2eAgInSc2d2	menší
vzrůstu	vzrůst	k1gInSc2	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
bývají	bývat	k5eAaImIp3nP	bývat
také	také	k9	také
sterilní	sterilní	k2eAgFnPc1d1	sterilní
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
lvi	lev	k1gMnPc1	lev
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
neútočí	útočit	k5eNaImIp3nS	útočit
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jejich	jejich	k3xOp3gFnSc7	jejich
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
kořistí	kořist	k1gFnSc7	kořist
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
hlad	hlad	k1gInSc1	hlad
dovede	dovést	k5eAaPmIp3nS	dovést
i	i	k9	i
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
lidských	lidský	k2eAgNnPc2d1	lidské
obydlí	obydlí	k1gNnPc2	obydlí
a	a	k8xC	a
člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
stává	stávat	k5eAaImIp3nS	stávat
snadno	snadno	k6eAd1	snadno
dostupnou	dostupný	k2eAgFnSc7d1	dostupná
kořistí	kořist	k1gFnSc7	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Lvi	lev	k1gMnPc1	lev
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
agresivní	agresivní	k2eAgFnPc1d1	agresivní
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
bengálští	bengálský	k2eAgMnPc1d1	bengálský
tygři	tygr	k1gMnPc1	tygr
nebo	nebo	k8xC	nebo
levharti	levhart	k1gMnPc1	levhart
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
větší	veliký	k2eAgInSc1d2	veliký
počet	počet	k1gInSc1	počet
lidských	lidský	k2eAgFnPc2d1	lidská
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInSc1d3	nejznámější
útok	útok	k1gInSc1	útok
na	na	k7c4	na
člověka	člověk	k1gMnSc2	člověk
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
keňské	keňský	k2eAgFnSc6d1	keňská
oblasti	oblast	k1gFnSc6	oblast
Tsavo	Tsavo	k1gNnSc4	Tsavo
stavěla	stavět	k5eAaImAgFnS	stavět
železnice	železnice	k1gFnSc1	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
bezhřívnatí	bezhřívnatý	k2eAgMnPc1d1	bezhřívnatý
lvi	lev	k1gMnPc1	lev
zabili	zabít	k5eAaPmAgMnP	zabít
za	za	k7c4	za
pouhé	pouhý	k2eAgInPc4d1	pouhý
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
na	na	k7c4	na
35	[number]	k4	35
afrických	africký	k2eAgMnPc2d1	africký
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Dělníci	dělník	k1gMnPc1	dělník
postavili	postavit	k5eAaPmAgMnP	postavit
obranné	obranný	k2eAgInPc4d1	obranný
ploty	plot	k1gInPc4	plot
s	s	k7c7	s
pochodněmi	pochodeň	k1gFnPc7	pochodeň
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
lvi	lev	k1gMnPc1	lev
nedostali	dostat	k5eNaPmAgMnP	dostat
do	do	k7c2	do
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ani	ani	k8xC	ani
ty	ten	k3xDgInPc4	ten
je	on	k3xPp3gInPc4	on
nedokázaly	dokázat	k5eNaPmAgInP	dokázat
zadržet	zadržet	k5eAaPmF	zadržet
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Henry	Henry	k1gMnSc1	Henry
Patterson	Patterson	k1gMnSc1	Patterson
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vedl	vést	k5eAaImAgMnS	vést
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
vylézt	vylézt	k5eAaPmF	vylézt
na	na	k7c4	na
strom	strom	k1gInSc4	strom
a	a	k8xC	a
lvy	lev	k1gInPc4	lev
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bezúspěšně	bezúspěšně	k6eAd1	bezúspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	s	k7c7	s
lvy	lev	k1gMnPc7	lev
podařilo	podařit	k5eAaPmAgNnS	podařit
zastřelit	zastřelit	k5eAaPmF	zastřelit
<g/>
,	,	kIx,	,
stihli	stihnout	k5eAaPmAgMnP	stihnout
zabít	zabít	k5eAaPmF	zabít
135	[number]	k4	135
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
zaznamenány	zaznamenán	k2eAgInPc1d1	zaznamenán
útoky	útok	k1gInPc1	útok
lvů	lev	k1gMnPc2	lev
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
,	,	kIx,	,
statisticky	statisticky	k6eAd1	statisticky
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
útočí	útočit	k5eAaImIp3nS	útočit
lev	lev	k1gMnSc1	lev
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
častěji	často	k6eAd2	často
než	než	k8xS	než
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Lev	Lev	k1gMnSc1	Lev
je	být	k5eAaImIp3nS	být
zapsán	zapsat	k5eAaPmNgMnS	zapsat
na	na	k7c6	na
červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
IUCN	IUCN	kA	IUCN
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
<g/>
:	:	kIx,	:
zranitelný	zranitelný	k2eAgMnSc1d1	zranitelný
–	–	k?	–
VULNERABLE	VULNERABLE	kA	VULNERABLE
(	(	kIx(	(
<g/>
VU	VU	kA	VU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stupeň	stupeň	k1gInSc1	stupeň
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
2	[number]	k4	2
<g/>
abcd	abcda	k1gFnPc2	abcda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Lion	Lion	k1gMnSc1	Lion
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
lev	lev	k1gInSc1	lev
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
lev	lev	k1gMnSc1	lev
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gNnSc1	téma
Lev	lev	k1gInSc1	lev
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Galerie	galerie	k1gFnSc1	galerie
lev	lev	k1gInSc1	lev
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Africa	Africa	k1gFnSc1	Africa
Online	Onlin	k1gInSc5	Onlin
–	–	k?	–
stránka	stránka	k1gFnSc1	stránka
o	o	k7c6	o
lvech	lev	k1gInPc6	lev
</s>
