<p>
<s>
Operace	operace	k1gFnSc1	operace
Karas	Karas	k1gMnSc1	Karas
byl	být	k5eAaImAgInS	být
krycí	krycí	k2eAgInSc4d1	krycí
název	název	k1gInSc4	název
pro	pro	k7c4	pro
pozemní	pozemní	k2eAgInSc4d1	pozemní
výsadek	výsadek	k1gInSc4	výsadek
vyslaný	vyslaný	k2eAgInSc4d1	vyslaný
během	během	k7c2	během
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
na	na	k7c6	na
území	území	k1gNnSc6	území
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
zpravodajským	zpravodajský	k2eAgInSc7d1	zpravodajský
odborem	odbor	k1gInSc7	odbor
exilového	exilový	k2eAgNnSc2d1	exilové
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Složení	složení	k1gNnPc4	složení
a	a	k8xC	a
úkoly	úkol	k1gInPc4	úkol
==	==	k?	==
</s>
</p>
<p>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
příslušníkem	příslušník	k1gMnSc7	příslušník
výsadku	výsadek	k1gInSc2	výsadek
byl	být	k5eAaImAgMnS	být
kpt.	kpt.	k?	kpt.
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Krátký	Krátký	k1gMnSc1	Krátký
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
pomáhat	pomáhat	k5eAaImF	pomáhat
při	při	k7c6	při
organizování	organizování	k1gNnSc6	organizování
SNP	SNP	kA	SNP
<g/>
,	,	kIx,	,
provádět	provádět	k5eAaImF	provádět
zpravodajskou	zpravodajský	k2eAgFnSc4d1	zpravodajská
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
předávat	předávat	k5eAaImF	předávat
informace	informace	k1gFnPc4	informace
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
styčným	styčný	k2eAgMnSc7d1	styčný
důstojníkem	důstojník	k1gMnSc7	důstojník
vrchního	vrchní	k2eAgNnSc2d1	vrchní
velení	velení	k1gNnSc2	velení
čs	čs	kA	čs
<g/>
.	.	kIx.	.
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
armády	armáda	k1gFnSc2	armáda
u	u	k7c2	u
vedení	vedení	k1gNnSc2	vedení
slovenského	slovenský	k2eAgInSc2d1	slovenský
vojenského	vojenský	k2eAgInSc2d1	vojenský
odboje	odboj	k1gInSc2	odboj
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
u	u	k7c2	u
velení	velení	k1gNnSc2	velení
SNP	SNP	kA	SNP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Činnost	činnost	k1gFnSc4	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
Oproti	oproti	k7c3	oproti
jiným	jiný	k1gMnPc3	jiný
byl	být	k5eAaImAgInS	být
výsadek	výsadek	k1gInSc1	výsadek
proveden	provést	k5eAaPmNgInS	provést
pozemní	pozemní	k2eAgFnSc7d1	pozemní
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
Turecko	Turecko	k1gNnSc4	Turecko
se	se	k3xPyFc4	se
Krátký	Krátký	k1gMnSc1	Krátký
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1944	[number]	k4	1944
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c6	na
Slovensko	Slovensko	k1gNnSc1	Slovensko
železničním	železniční	k2eAgInSc7d1	železniční
expresem	expres	k1gInSc7	expres
na	na	k7c4	na
platný	platný	k2eAgInSc4d1	platný
slovenský	slovenský	k2eAgInSc4d1	slovenský
pas	pas	k1gInSc4	pas
(	(	kIx(	(
<g/>
na	na	k7c4	na
jméno	jméno	k1gNnSc4	jméno
Dezider	Dezider	k1gInSc4	Dezider
Bukovinský	bukovinský	k2eAgInSc4d1	bukovinský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
odbojovou	odbojový	k2eAgFnSc7d1	odbojová
organizací	organizace	k1gFnSc7	organizace
Flora	Floro	k1gNnSc2	Floro
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
odbojovými	odbojový	k2eAgFnPc7d1	odbojová
organizacemi	organizace	k1gFnPc7	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Navázal	navázat	k5eAaPmAgInS	navázat
styk	styk	k1gInSc1	styk
i	i	k8xC	i
s	s	k7c7	s
pplk.	pplk.	kA	pplk.
Golianem	Golian	k1gMnSc7	Golian
a	a	k8xC	a
zapojil	zapojit	k5eAaPmAgInS	zapojit
se	se	k3xPyFc4	se
do	do	k7c2	do
příprav	příprava	k1gFnPc2	příprava
na	na	k7c6	na
povstání	povstání	k1gNnSc6	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
navázat	navázat	k5eAaPmF	navázat
radiové	radiový	k2eAgNnSc4d1	radiové
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
Londýnem	Londýn	k1gInSc7	Londýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
SNP	SNP	kA	SNP
působil	působit	k5eAaImAgInS	působit
u	u	k7c2	u
štábu	štáb	k1gInSc2	štáb
velení	velení	k1gNnSc2	velení
povstání	povstání	k1gNnSc2	povstání
jako	jako	k8xS	jako
styčný	styčný	k2eAgMnSc1d1	styčný
důstojník	důstojník	k1gMnSc1	důstojník
MNO	MNO	kA	MNO
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
vykonával	vykonávat	k5eAaImAgInS	vykonávat
až	až	k6eAd1	až
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
gen.	gen.	kA	gen.
Viesta	Viest	k1gMnSc2	Viest
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
činnosti	činnost	k1gFnSc2	činnost
ve	v	k7c6	v
štábu	štáb	k1gInSc6	štáb
Krátký	Krátký	k1gMnSc1	Krátký
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
i	i	k9	i
s	s	k7c7	s
výsadkovou	výsadkový	k2eAgFnSc7d1	výsadková
skupinou	skupina	k1gFnSc7	skupina
Manganese	Manganese	k1gFnSc2	Manganese
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vojenské	vojenský	k2eAgFnSc6d1	vojenská
porážce	porážka	k1gFnSc6	porážka
SNP	SNP	kA	SNP
ustupoval	ustupovat	k5eAaImAgMnS	ustupovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
velením	velení	k1gNnSc7	velení
z	z	k7c2	z
Banské	banský	k2eAgFnSc2d1	Banská
Bystrice	Bystrica	k1gFnSc2	Bystrica
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
ústupu	ústup	k1gInSc6	ústup
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Donovaly	Donovaly	k1gFnSc4	Donovaly
společně	společně	k6eAd1	společně
s	s	k7c7	s
gen.	gen.	kA	gen.
Viestem	Viest	k1gMnSc7	Viest
a	a	k8xC	a
gen.	gen.	kA	gen.
Golianem	Golian	k1gMnSc7	Golian
u	u	k7c2	u
Bukovce	bukovka	k1gFnSc3	bukovka
zajat	zajat	k2eAgInSc1d1	zajat
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
Bratislavu	Bratislava	k1gFnSc4	Bratislava
a	a	k8xC	a
Prahu	Praha	k1gFnSc4	Praha
byl	být	k5eAaImAgInS	být
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
vězněn	věznit	k5eAaImNgMnS	věznit
a	a	k8xC	a
vyslýchán	vyslýchat	k5eAaImNgMnS	vyslýchat
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
s	s	k7c7	s
generály	generál	k1gMnPc7	generál
Viestem	Viest	k1gMnSc7	Viest
a	a	k8xC	a
Golianem	Golian	k1gMnSc7	Golian
popraven	popravit	k5eAaPmNgInS	popravit
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Flosenbürg	Flosenbürg	k1gInSc1	Flosenbürg
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nepotvrzených	potvrzený	k2eNgInPc2d1	nepotvrzený
zdrojů	zdroj	k1gInPc2	zdroj
válku	válek	k1gInSc2	válek
přežil	přežít	k5eAaPmAgMnS	přežít
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
přepraven	přepravit	k5eAaPmNgMnS	přepravit
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zahynul	zahynout	k5eAaPmAgMnS	zahynout
v	v	k7c6	v
gulagu	gulag	k1gInSc6	gulag
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
REICHL	REICHL	kA	REICHL
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Cesty	cesta	k1gFnPc4	cesta
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
Cheb	Cheb	k1gInSc1	Cheb
<g/>
:	:	kIx,	:
Svět	svět	k1gInSc1	svět
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86808	[number]	k4	86808
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Operace	operace	k1gFnSc1	operace
Karas	Karas	k1gMnSc1	Karas
na	na	k7c4	na
valka	valka	k6eAd1	valka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Operace	operace	k1gFnSc1	operace
Karas	Karas	k1gMnSc1	Karas
a	a	k8xC	a
SNP	SNP	kA	SNP
</s>
</p>
