<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zabít	zabít	k5eAaPmF	zabít
Sekala	Sekal	k1gMnSc4	Sekal
je	být	k5eAaImIp3nS	být
koprodukční	koprodukční	k2eAgInSc1d1	koprodukční
film	film	k1gInSc1	film
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Michálka	Michálek	k1gMnSc2	Michálek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
podle	podle	k7c2	podle
námětu	námět	k1gInSc2	námět
a	a	k8xC	a
scénáře	scénář	k1gInPc4	scénář
Jiřího	Jiří	k1gMnSc4	Jiří
Křižana	Křižan	k1gMnSc4	Křižan
získal	získat	k5eAaPmAgMnS	získat
10	[number]	k4	10
Českých	český	k2eAgMnPc2d1	český
lvů	lev	k1gMnPc2	lev
a	a	k8xC	a
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
českou	český	k2eAgFnSc4d1	Česká
kinematografii	kinematografie	k1gFnSc4	kinematografie
v	v	k7c6	v
klání	klání	k1gNnSc6	klání
o	o	k7c4	o
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
koprodukci	koprodukce	k1gFnSc6	koprodukce
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
Slovenskem	Slovensko	k1gNnSc7	Slovensko
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
