<s>
Citron	citron	k1gInSc1	citron
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
citrón	citrón	k1gInSc1	citrón
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
plod	plod	k1gInSc4	plod
citronovníku	citronovník	k1gInSc2	citronovník
(	(	kIx(	(
<g/>
Citrus	citrus	k1gInSc1	citrus
limon	limon	k1gInSc1	limon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
citronů	citron	k1gInPc2	citron
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
hesperidia	hesperidium	k1gNnSc2	hesperidium
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vejcovitého	vejcovitý	k2eAgInSc2d1	vejcovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
velikosti	velikost	k1gFnSc2	velikost
slepičího	slepičí	k2eAgNnSc2d1	slepičí
vejce	vejce	k1gNnSc2	vejce
či	či	k8xC	či
menší	malý	k2eAgFnPc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Citron	citron	k1gInSc1	citron
je	být	k5eAaImIp3nS	být
nepatrně	nepatrně	k6eAd1	nepatrně
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
limetka	limetka	k1gFnSc1	limetka
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
od	od	k7c2	od
tmavě	tmavě	k6eAd1	tmavě
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
světle	světle	k6eAd1	světle
zelené	zelený	k2eAgInPc4d1	zelený
až	až	k9	až
do	do	k7c2	do
žluta	žluto	k1gNnSc2	žluto
<g/>
.	.	kIx.	.
</s>
<s>
Citron	citron	k1gInSc1	citron
má	mít	k5eAaImIp3nS	mít
výrazně	výrazně	k6eAd1	výrazně
kyselou	kyselý	k2eAgFnSc4d1	kyselá
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Citrony	citron	k1gInPc1	citron
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
kyseliny	kyselina	k1gFnSc2	kyselina
askorbové	askorbový	k2eAgFnSc2d1	askorbová
a	a	k8xC	a
kyseliny	kyselina	k1gFnSc2	kyselina
citronové	citronový	k2eAgFnSc2d1	citronová
(	(	kIx(	(
<g/>
3,5	[number]	k4	3,5
<g/>
-	-	kIx~	-
<g/>
8,0	[number]	k4	8,0
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
přibližně	přibližně	k6eAd1	přibližně
kolem	kolem	k7c2	kolem
87	[number]	k4	87
%	%	kIx~	%
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
jako	jako	k9	jako
přírodní	přírodní	k2eAgNnSc4d1	přírodní
okyselovadlo	okyselovadlo	k1gNnSc4	okyselovadlo
do	do	k7c2	do
řady	řada	k1gFnSc2	řada
pokrmů	pokrm	k1gInPc2	pokrm
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
mačká	mačkat	k5eAaImIp3nS	mačkat
kyselá	kyselý	k2eAgFnSc1d1	kyselá
šťáva	šťáva	k1gFnSc1	šťáva
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
bioflavonoidy	bioflavonoida	k1gFnPc1	bioflavonoida
<g/>
,	,	kIx,	,
pektin	pektin	k1gInSc1	pektin
<g/>
,	,	kIx,	,
vonné	vonný	k2eAgFnPc1d1	vonná
silice	silice	k1gFnPc1	silice
<g/>
,	,	kIx,	,
provitamin	provitamin	k1gInSc1	provitamin
A	A	kA	A
<g/>
,	,	kIx,	,
vitamin	vitamin	k1gInSc1	vitamin
B	B	kA	B
<g/>
,	,	kIx,	,
cca	cca	kA	cca
3	[number]	k4	3
%	%	kIx~	%
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
též	též	k9	též
zdrojem	zdroj	k1gInSc7	zdroj
vápníku	vápník	k1gInSc2	vápník
<g/>
,	,	kIx,	,
draslíku	draslík	k1gInSc2	draslík
<g/>
,	,	kIx,	,
fosforu	fosfor	k1gInSc2	fosfor
<g/>
,	,	kIx,	,
hořčíku	hořčík	k1gInSc2	hořčík
<g/>
,	,	kIx,	,
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
zinku	zinek	k1gInSc2	zinek
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Citrony	citron	k1gInPc4	citron
údajně	údajně	k6eAd1	údajně
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Citrusy	citrus	k1gInPc1	citrus
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
citroníků	citroník	k1gInPc2	citroník
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
subtropů	subtropy	k1gInPc2	subtropy
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
citrus	citrus	k1gInSc1	citrus
lze	lze	k6eAd1	lze
pěstovat	pěstovat	k5eAaImF	pěstovat
i	i	k9	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
v	v	k7c6	v
mírném	mírný	k2eAgNnSc6d1	mírné
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
ve	v	k7c6	v
sklenících	skleník	k1gInPc6	skleník
či	či	k8xC	či
doma	doma	k6eAd1	doma
při	při	k7c6	při
pokojových	pokojový	k2eAgFnPc6d1	pokojová
teplotách	teplota	k1gFnPc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Ideální	ideální	k2eAgInPc1d1	ideální
jsou	být	k5eAaImIp3nP	být
slunné	slunný	k2eAgInPc1d1	slunný
<g/>
,	,	kIx,	,
závětrné	závětrný	k2eAgInPc1d1	závětrný
balkony	balkon	k1gInPc1	balkon
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
průmyslově	průmyslově	k6eAd1	průmyslově
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Vzrostlý	vzrostlý	k2eAgInSc1d1	vzrostlý
strom	strom	k1gInSc1	strom
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
a	a	k8xC	a
lokalitě	lokalita	k1gFnSc3	lokalita
rodit	rodit	k5eAaImF	rodit
ročně	ročně	k6eAd1	ročně
něco	něco	k3yInSc1	něco
mezi	mezi	k7c4	mezi
200	[number]	k4	200
až	až	k9	až
1	[number]	k4	1
500	[number]	k4	500
citrony	citron	k1gInPc7	citron
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nutričních	nutriční	k2eAgInPc2d1	nutriční
parametrů	parametr	k1gInPc2	parametr
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
v	v	k7c6	v
plodech	plod	k1gInPc6	plod
citronu	citron	k1gInSc2	citron
<g/>
.	.	kIx.	.
</s>
