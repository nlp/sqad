<s desamb="1">
Obec	obec	k1gFnSc1
založil	založit	k5eAaPmAgInS
pražský	pražský	k2eAgMnSc1d1
arcibiskup	arcibiskup	k1gMnSc1
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Khünburg	Khünburg	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Zaběhlá	Zaběhlá	k1gFnSc1
nebo	nebo	k8xC
Záběhlá	Záběhlá	k1gFnSc1
někdy	někdy	k6eAd1
zvaná	zvaný	k2eAgFnSc1d1
také	také	k9
Mozolín	Mozolín	k1gInSc1
<g/>
,	,	kIx,
Mozolov	Mozolov	k1gInSc1
je	být	k5eAaImIp3nS
zaniklá	zaniklý	k2eAgFnSc1d1
obec	obec	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
nacházela	nacházet	k5eAaImAgFnS
8	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
km	km	kA
severozápadně	severozápadně	k6eAd1
od	od	k7c2
Rožmitálu	Rožmitál	k1gInSc2
pod	pod	k7c7
Třemšínem	Třemšín	k1gInSc7
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
680	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
pod	pod	k7c7
vrchem	vrch	k1gInSc7
Kočka	kočka	k1gFnSc1
<g/>
.	.	kIx.
</s>