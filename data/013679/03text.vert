<s>
Zaběhlá	zaběhlý	k2eAgFnSc1d1
</s>
<s>
Zaběhlá	zaběhlý	k2eAgFnSc1d1
Obchod	obchod	k1gInSc1
a	a	k8xC
hostinec	hostinec	k1gInSc1
Josefa	Josef	k1gMnSc2
Jaroše	Jaroš	k1gMnSc2
<g/>
,	,	kIx,
1925	#num#	k4
<g/>
Lokalita	lokalita	k1gFnSc1
Charakter	charakter	k1gInSc1
</s>
<s>
zaniklá	zaniklý	k2eAgFnSc1d1
vesnice	vesnice	k1gFnSc1
Okres	okres	k1gInSc1
</s>
<s>
Příbram	Příbram	k1gFnSc1
Kraj	kraj	k1gInSc1
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
40	#num#	k4
<g/>
′	′	k?
<g/>
29	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
46	#num#	k4
<g/>
′	′	k?
<g/>
22	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
680	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
</s>
<s>
Zaběhlá	zaběhlý	k2eAgFnSc1d1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Zaniklé	zaniklý	k2eAgFnSc2d1
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
993	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zaběhlá	Zaběhlá	k1gFnSc1
nebo	nebo	k8xC
Záběhlá	Záběhlá	k1gFnSc1
někdy	někdy	k6eAd1
zvaná	zvaný	k2eAgFnSc1d1
také	také	k9
Mozolín	Mozolín	k1gInSc1
<g/>
,	,	kIx,
Mozolov	Mozolov	k1gInSc1
je	být	k5eAaImIp3nS
zaniklá	zaniklý	k2eAgFnSc1d1
obec	obec	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
nacházela	nacházet	k5eAaImAgFnS
8	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
km	km	kA
severozápadně	severozápadně	k6eAd1
od	od	k7c2
Rožmitálu	Rožmitál	k1gInSc2
pod	pod	k7c7
Třemšínem	Třemšín	k1gInSc7
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
680	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
pod	pod	k7c7
vrchem	vrch	k1gInSc7
Kočka	kočka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaběhlá	zaběhlý	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
politická	politický	k2eAgFnSc1d1
a	a	k8xC
katastrální	katastrální	k2eAgFnSc1d1
obec	obec	k1gFnSc1
s	s	k7c7
plochou	plocha	k1gFnSc7
katastru	katastr	k1gInSc2
1288	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dělila	dělit	k5eAaImAgFnS
se	se	k3xPyFc4
na	na	k7c4
Přední	přední	k2eAgFnSc4d1
Zaběhlou	zaběhlý	k2eAgFnSc4d1
<g/>
,	,	kIx,
Zadní	zadní	k2eAgFnSc4d1
Zaběhlou	zaběhlý	k2eAgFnSc4d1
a	a	k8xC
osadu	osada	k1gFnSc4
Budy	Buda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
nástupu	nástup	k1gInSc6
komunistického	komunistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
rozhodnuto	rozhodnout	k5eAaPmNgNnS
o	o	k7c4
rozšíření	rozšíření	k1gNnSc4
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
Brdy	Brdy	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelé	obyvatel	k1gMnPc1
Zaběhlé	zaběhlý	k2eAgFnPc1d1
byli	být	k5eAaImAgMnP
vystěhováni	vystěhován	k2eAgMnPc1d1
a	a	k8xC
ves	ves	k1gFnSc1
byla	být	k5eAaImAgFnS
zničena	zničit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
zrušením	zrušení	k1gNnSc7
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
Brdy	Brdy	k1gInPc1
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
připadlo	připadnout	k5eAaPmAgNnS
území	území	k1gNnSc1
bývalé	bývalý	k2eAgFnSc2d1
Přední	přední	k2eAgFnSc2d1
Záběhlé	Záběhlý	k2eAgFnSc2d1
k	k	k7c3
obci	obec	k1gFnSc3
Věšín	Věšína	k1gFnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Příbram	Příbram	k1gFnSc1
a	a	k8xC
území	území	k1gNnSc1
bývalé	bývalý	k2eAgFnSc2d1
Zadní	zadní	k2eAgFnSc2d1
Záběhlé	Záběhlý	k2eAgFnSc2d1
k	k	k7c3
obci	obec	k1gFnSc3
Strašice	Strašice	k1gFnPc4
v	v	k7c6
okrese	okres	k1gInSc6
Rokycany	Rokycany	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Území	území	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
Středních	střední	k2eAgInPc6d1
Brdech	Brdy	k1gInPc6
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
okolo	okolo	k7c2
600	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
fytogeografického	fytogeografický	k2eAgInSc2d1
jedinou	jediný	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
oreofytika	oreofytik	k1gMnSc2
(	(	kIx(
<g/>
oblast	oblast	k1gFnSc1
horské	horský	k2eAgFnSc2d1
květeny	květena	k1gFnSc2
<g/>
)	)	kIx)
ve	v	k7c6
středních	střední	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgInPc1d1
za	za	k7c4
jediné	jediný	k2eAgNnSc4d1
území	území	k1gNnSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
lze	lze	k6eAd1
v	v	k7c6
této	tento	k3xDgFnSc6
zeměpisné	zeměpisný	k2eAgFnSc6d1
šířce	šířka	k1gFnSc6
a	a	k8xC
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
ještě	ještě	k6eAd1
setkat	setkat	k5eAaPmF
s	s	k7c7
přechodem	přechod	k1gInSc7
mezi	mezi	k7c7
tajgou	tajga	k1gFnSc7
a	a	k8xC
severskou	severský	k2eAgFnSc7d1
tundrou	tundra	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
tu	tu	k6eAd1
zdokumentovány	zdokumentován	k2eAgInPc1d1
geomorfologické	geomorfologický	k2eAgInPc1d1
jevy	jev	k1gInPc1
<g/>
,	,	kIx,
s	s	k7c7
nimiž	jenž	k3xRgInPc7
se	se	k3xPyFc4
na	na	k7c6
českém	český	k2eAgNnSc6d1
území	území	k1gNnSc6
lze	lze	k6eAd1
setkat	setkat	k5eAaPmF
pouze	pouze	k6eAd1
v	v	k7c6
Krkonoších	Krkonoše	k1gFnPc6
nebo	nebo	k8xC
Jeseníkách	Jeseníky	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
blízkosti	blízkost	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
Padrťské	Padrťský	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
nich	on	k3xPp3gMnPc2
se	se	k3xPyFc4
poblíž	poblíž	k6eAd1
nachází	nacházet	k5eAaImIp3nS
soustava	soustava	k1gFnSc1
rybníků	rybník	k1gInPc2
Výtažník	výtažník	k1gInSc1
<g/>
,	,	kIx,
Šindelka	Šindelka	k1gMnSc1
<g/>
,	,	kIx,
Gricák	Gricák	k1gMnSc1
a	a	k8xC
Ledvinka	Ledvinka	k1gMnSc1
a	a	k8xC
protéká	protékat	k5eAaImIp3nS
zde	zde	k6eAd1
řeka	řeka	k1gFnSc1
Klabava	Klabava	k1gFnSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
místně	místně	k6eAd1
nazývaná	nazývaný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
Padrťský	Padrťský	k2eAgInSc1d1
potok	potok	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Roku	rok	k1gInSc2
1565	#num#	k4
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
zmiňovány	zmiňován	k2eAgFnPc1d1
jen	jen	k9
louky	louka	k1gFnPc1
na	na	k7c4
Zaběhlé	zaběhlý	k2eAgFnPc4d1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
patřily	patřit	k5eAaImAgFnP
rožmitálskému	rožmitálský	k2eAgInSc3d1
dvoru	dvůr	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
nové	nový	k2eAgFnSc6d1
vsi	ves	k1gFnSc6
Zaběhle	zaběhle	k6eAd1
je	být	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1733	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obec	obec	k1gFnSc1
založil	založit	k5eAaPmAgInS
pražský	pražský	k2eAgMnSc1d1
arcibiskup	arcibiskup	k1gMnSc1
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Khünburg	Khünburg	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
založení	založení	k1gNnSc2
obce	obec	k1gFnSc2
byla	být	k5eAaImAgFnS
potřeba	potřeba	k1gFnSc1
přivézt	přivézt	k5eAaPmF
sem	sem	k6eAd1
více	hodně	k6eAd2
pracovních	pracovní	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
,	,	kIx,
především	především	k9
na	na	k7c4
těžké	těžký	k2eAgFnPc4d1
práce	práce	k1gFnPc4
v	v	k7c6
lesích	les	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novým	nový	k2eAgMnSc7d1
12	#num#	k4
osadníkům	osadník	k1gMnPc3
bylo	být	k5eAaImAgNnS
dáno	dán	k2eAgNnSc1d1
místo	místo	k1gNnSc1
na	na	k7c4
chalupu	chalupa	k1gFnSc4
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
mezi	mezi	k7c4
ně	on	k3xPp3gFnPc4
rozděleny	rozdělen	k2eAgFnPc4d1
polnosti	polnost	k1gFnPc4
a	a	k8xC
louky	louka	k1gFnPc4
a	a	k8xC
všem	všecek	k3xTgInPc3
byla	být	k5eAaImAgFnS
na	na	k7c6
12	#num#	k4
let	léto	k1gNnPc2
prominuta	prominut	k2eAgFnSc1d1
robota	robota	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nápomocni	nápomocen	k2eAgMnPc1d1
měli	mít	k5eAaImAgMnP
být	být	k5eAaImF
jen	jen	k9
v	v	k7c6
čase	čas	k1gInSc6
sklizně	sklizeň	k1gFnSc2
obilí	obilí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
se	se	k3xPyFc4
sem	sem	k6eAd1
stěhovalo	stěhovat	k5eAaImAgNnS
více	hodně	k6eAd2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
tolik	tolik	k6eAd1
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
často	často	k6eAd1
místní	místní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
nestačili	stačit	k5eNaBmAgMnP
starat	starat	k5eAaImF
ani	ani	k8xC
o	o	k7c4
vlastní	vlastní	k2eAgNnSc4d1
hospodářství	hospodářství	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zaběhlá	zaběhlý	k2eAgFnSc1d1
zvaná	zvaný	k2eAgFnSc1d1
Mozolov	Mozolov	k1gInSc1
</s>
<s>
Dolejší	Dolejší	k2eAgFnSc1d1,k2eAgFnSc1d2
část	část	k1gFnSc1
Přední	přední	k2eAgFnSc2d1
Zaběhlé	zaběhlý	k2eAgFnSc2d1
pod	pod	k7c7
silnicí	silnice	k1gFnSc7
do	do	k7c2
Rožmitálu	Rožmitál	k1gInSc2
</s>
<s>
Dům	dům	k1gInSc1
Ferdinanda	Ferdinand	k1gMnSc2
Voříška	Voříšek	k1gMnSc2
na	na	k7c4
Zaběhlé	zaběhlý	k2eAgNnSc4d1
<g/>
,	,	kIx,
„	„	k?
<g/>
U	u	k7c2
Krejčů	Krejča	k1gMnPc2
<g/>
“	“	k?
<g/>
Zdejší	zdejší	k2eAgFnPc4d1
tvrdé	tvrdý	k2eAgFnPc4d1
pracovní	pracovní	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
dokresluje	dokreslovat	k5eAaImIp3nS
zmínka	zmínka	k1gFnSc1
o	o	k7c6
vzniku	vznik	k1gInSc6
přezdívky	přezdívka	k1gFnSc2
obce	obec	k1gFnSc2
–	–	k?
Mozolov	Mozolov	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Záběhelšťané	Záběhelšťan	k1gMnPc1
byli	být	k5eAaImAgMnP
honěni	honit	k5eAaImNgMnP
nástiným	nástin	k2eAgMnSc7d1
drábem	dráb	k1gMnSc7
z	z	k7c2
dřiny	dřina	k1gFnSc2
do	do	k7c2
dřiny	dřina	k1gFnSc2
<g/>
,	,	kIx,
ani	ani	k8xC
se	se	k3xPyFc4
nezastavili	zastavit	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
si	se	k3xPyFc3
nemohli	moct	k5eNaImAgMnP
pomoci	pomoct	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šli	jít	k5eAaImAgMnP
si	se	k3xPyFc3
postesknout	postesknout	k5eAaPmF
k	k	k7c3
vrchnosti	vrchnost	k1gFnSc3
a	a	k8xC
poprosit	poprosit	k5eAaPmF
ji	on	k3xPp3gFnSc4
o	o	k7c4
malou	malý	k2eAgFnSc4d1
úlevu	úleva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starosta	Starosta	k1gMnSc1
šel	jít	k5eAaImAgMnS
se	s	k7c7
dvěma	dva	k4xCgFnPc7
radními	radní	k1gFnPc7
k	k	k7c3
vrchnosti	vrchnost	k1gFnSc3
<g/>
,	,	kIx,
promluvil	promluvit	k5eAaPmAgMnS
několik	několik	k4yIc4
slov	slovo	k1gNnPc2
a	a	k8xC
dodal	dodat	k5eAaPmAgInS
<g/>
:	:	kIx,
„	„	k?
<g/>
Tyhle	tenhle	k3xDgInPc4
mozoly	mozol	k1gInPc4
to	ten	k3xDgNnSc1
nejlépe	dobře	k6eAd3
povídají	povídat	k5eAaImIp3nP
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
tři	tři	k4xCgMnPc1
obrátili	obrátit	k5eAaPmAgMnP
tvrdé	tvrdý	k2eAgFnPc4d1
ruce	ruka	k1gFnPc4
dlaněmi	dlaň	k1gFnPc7
vzhůru	vzhůru	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černaly	černat	k5eAaImAgInP
se	se	k3xPyFc4
na	na	k7c6
nich	on	k3xPp3gInPc6
mozolnaté	mozolnatý	k2eAgFnPc1d1
ztvrdliny	ztvrdlina	k1gFnPc1
<g/>
,	,	kIx,
z	z	k7c2
některých	některý	k3yIgFnPc2
tříštila	tříštit	k5eAaImAgFnS
krev	krev	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správce	správce	k1gMnSc1
vrtěl	vrtět	k5eAaImAgMnS
hlavou	hlava	k1gFnSc7
a	a	k8xC
že	že	k8xS
tu	ten	k3xDgFnSc4
věc	věc	k1gFnSc4
napraví	napravit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čekali	čekat	k5eAaImAgMnP
nějaké	nějaký	k3yIgNnSc4
ulehčení	ulehčení	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
zůstal	zůstat	k5eAaPmAgMnS
přísný	přísný	k2eAgMnSc1d1
dráb	dráb	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
týrání	týrání	k1gNnSc1
dále	daleko	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vypravili	vypravit	k5eAaPmAgMnP
se	se	k3xPyFc4
tedy	tedy	k9
k	k	k7c3
vrchnosti	vrchnost	k1gFnSc3
znova	znova	k6eAd1
<g/>
:	:	kIx,
„	„	k?
<g/>
Á	Á	kA
mozoláci	mozolák	k1gMnPc1
jsou	být	k5eAaImIp3nP
tu	tu	k6eAd1
zase	zase	k9
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
povídá	povídat	k5eAaImIp3nS
správce	správce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
tři	tři	k4xCgMnPc1
přišli	přijít	k5eAaPmAgMnP
domů	domů	k6eAd1
a	a	k8xC
vyprávěli	vyprávět	k5eAaImAgMnP
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
jim	on	k3xPp3gMnPc3
řekl	říct	k5eAaPmAgMnS
správce	správce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
„	„	k?
<g/>
Mozoláci	Mozolák	k1gMnPc1
<g/>
“	“	k?
se	se	k3xPyFc4
roznesl	roznést	k5eAaPmAgMnS
po	po	k7c6
okolí	okolí	k1gNnSc6
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
dosud	dosud	k6eAd1
doptáte	doptat	k5eAaPmIp2nP
na	na	k7c4
Mozolov	Mozolov	k1gInSc4
a	a	k8xC
Mozoláky	Mozolák	k1gInPc4
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Později	pozdě	k6eAd2
se	se	k3xPyFc4
obec	obec	k1gFnSc1
dělila	dělit	k5eAaImAgFnS
se	se	k3xPyFc4
na	na	k7c4
Přední	přední	k2eAgFnSc4d1
a	a	k8xC
Zadní	zadní	k2eAgFnSc4d1
Zaběhlou	zaběhlý	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Přední	přední	k2eAgFnSc4d1
Záběhlou	Záběhlý	k2eAgFnSc4d1
se	se	k3xPyFc4
mezi	mezi	k7c7
obyvateli	obyvatel	k1gMnPc7
zažil	zažít	k5eAaPmAgMnS
název	název	k1gInSc4
„	„	k?
<g/>
V	v	k7c6
Chalupách	chalupa	k1gFnPc6
<g/>
“	“	k?
a	a	k8xC
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
byla	být	k5eAaImAgFnS
osada	osada	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
nikdo	nikdo	k3yNnSc1
neřekl	říct	k5eNaPmAgMnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
„	„	k?
<g/>
V	v	k7c6
Budách	Budy	k1gInPc6
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
obce	obec	k1gFnSc2
Kolvín	Kolvín	k1gInSc1
<g/>
,	,	kIx,
Zaběhlá	zaběhlý	k2eAgFnSc1d1
nebyla	být	k5eNaImAgFnS
nikdy	nikdy	k6eAd1
elektrifikována	elektrifikován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
1	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
ze	z	k7c2
Zaběhlé	zaběhlý	k2eAgFnSc2d1
narukovalo	narukovat	k5eAaPmAgNnS
48	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
padlo	padnout	k5eAaPmAgNnS,k5eAaImAgNnS
14	#num#	k4
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
obce	obec	k1gFnSc2
pocházeli	pocházet	k5eAaImAgMnP
3	#num#	k4
legionáři	legionář	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomník	pomník	k1gInSc1
padlým	padlý	k1gMnPc3
stál	stát	k5eAaImAgInS
před	před	k7c7
obecnou	obecný	k2eAgFnSc7d1
školou	škola	k1gFnSc7
v	v	k7c6
Zadní	zadní	k2eAgFnSc6d1
Padrti	padrť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
s	s	k7c7
obcí	obec	k1gFnSc7
byl	být	k5eAaImAgInS
zničen	zničit	k5eAaPmNgInS
po	po	k7c6
vytvoření	vytvoření	k1gNnSc6
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
Brdy	Brdy	k1gInPc1
po	po	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1927	#num#	k4
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
o	o	k7c6
zřízení	zřízení	k1gNnSc6
střelnice	střelnice	k1gFnSc2
v	v	k7c6
Brdech	Brdy	k1gInPc6
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
pro	pro	k7c4
nepřímou	přímý	k2eNgFnSc4d1
dělostřeleckou	dělostřelecký	k2eAgFnSc4d1
střelbu	střelba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozloha	rozloha	k1gFnSc1
střelnice	střelnice	k1gFnSc1
je	být	k5eAaImIp3nS
uváděna	uváděn	k2eAgFnSc1d1
18	#num#	k4
–	–	k?
20	#num#	k4
tisíc	tisíc	k4xCgInPc2
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
do	do	k7c2
ní	on	k3xPp3gFnSc2
ještě	ještě	k6eAd1
nebyly	být	k5eNaImAgFnP
zahrnuty	zahrnut	k2eAgFnPc1d1
obce	obec	k1gFnPc1
Padrť	padrť	k1gFnSc1
<g/>
,	,	kIx,
Přední	přední	k2eAgFnSc1d1
a	a	k8xC
Zadní	zadní	k2eAgFnSc1d1
Záběhlá	Záběhlý	k2eAgFnSc1d1
<g/>
,	,	kIx,
Velcí	velký	k2eAgMnPc1d1
<g/>
,	,	kIx,
Kolvín	Kolvín	k1gMnSc1
a	a	k8xC
Hrachoviště	hrachoviště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
těchto	tento	k3xDgFnPc2
obcí	obec	k1gFnPc2
zabrána	zabrán	k2eAgFnSc1d1
německými	německý	k2eAgMnPc7d1
okupanty	okupant	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
padlo	padnout	k5eAaPmAgNnS,k5eAaImAgNnS
rozhodnutí	rozhodnutí	k1gNnSc1
rozšířit	rozšířit	k5eAaPmF
výcvikovou	výcvikový	k2eAgFnSc4d1
plochu	plocha	k1gFnSc4
střelnice	střelnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelé	obyvatel	k1gMnPc1
obcí	obec	k1gFnPc2
se	se	k3xPyFc4
museli	muset	k5eAaImAgMnP
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
1941	#num#	k4
vystěhovat	vystěhovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týkalo	týkat	k5eAaImAgNnS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
obcí	obec	k1gFnPc2
Zaběhlá	zaběhlý	k2eAgFnSc1d1
<g/>
,	,	kIx,
Padrť	padrť	k1gFnSc1
<g/>
,	,	kIx,
Kolvín	Kolvín	k1gMnSc1
<g/>
,	,	kIx,
Velcí	velký	k2eAgMnPc1d1
a	a	k8xC
Hrachoviště	hrachoviště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
vystěhováno	vystěhovat	k5eAaPmNgNnS
2	#num#	k4
429	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Zůstat	zůstat	k5eAaPmF
směli	smět	k5eAaImAgMnP
jen	jen	k9
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
pracovali	pracovat	k5eAaImAgMnP
v	v	k7c6
lesním	lesní	k2eAgNnSc6d1
hospodářství	hospodářství	k1gNnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1941	#num#	k4
přišla	přijít	k5eAaPmAgFnS
vichřice	vichřice	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
polámala	polámat	k5eAaPmAgFnS
množství	množství	k1gNnSc4
stromů	strom	k1gInPc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
bylo	být	k5eAaImAgNnS
potřeba	potřeba	k6eAd1
vytěžit	vytěžit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Přední	přední	k2eAgFnSc6d1
Zaběhlé	zaběhlý	k2eAgFnSc6d1
byla	být	k5eAaImAgFnS
vybudována	vybudovat	k5eAaPmNgFnS
četnická	četnický	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
a	a	k8xC
na	na	k7c6
lesní	lesní	k2eAgFnSc6d1
louce	louka	k1gFnSc6
studánka	studánka	k1gFnSc1
vznikl	vzniknout	k5eAaPmAgInS
výcvikový	výcvikový	k2eAgInSc1d1
tábor	tábor	k1gInSc1
pro	pro	k7c4
Hitlerjugend	Hitlerjugend	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
osvobození	osvobození	k1gNnSc6
se	se	k3xPyFc4
vystěhovaní	vystěhovaný	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
začali	začít	k5eAaPmAgMnP
vracet	vracet	k5eAaImF
do	do	k7c2
svých	svůj	k3xOyFgInPc2
domovů	domov	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgMnPc4,k3yIgMnPc4,k3yRgMnPc4
Němci	Němec	k1gMnPc1
nechali	nechat	k5eAaPmAgMnP
netknuté	tknutý	k2eNgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Brd	Brdy	k1gInPc2
se	se	k3xPyFc4
vrátila	vrátit	k5eAaPmAgFnS
i	i	k9
československá	československý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
nástupu	nástup	k1gInSc6
komunistického	komunistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
byl	být	k5eAaImAgInS
vydán	vydat	k5eAaPmNgInS
nový	nový	k2eAgInSc1d1
zákon	zákon	k1gInSc1
o	o	k7c6
Vojenských	vojenský	k2eAgInPc6d1
újezdech	újezd	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc4d1
označení	označení	k1gNnSc1
„	„	k?
<g/>
střelnice	střelnice	k1gFnSc2
Brdy	Brdy	k1gInPc4
<g/>
“	“	k?
bylo	být	k5eAaImAgNnS
nahrazeno	nahradit	k5eAaPmNgNnS
novým	nový	k2eAgInSc7d1
názvem	název	k1gInSc7
„	„	k?
<g/>
vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
Brdy	Brdy	k1gInPc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
že	že	k8xS
se	se	k3xPyFc4
vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
Brdy	Brdy	k1gInPc1
rozšíří	rozšířit	k5eAaPmIp3nP
a	a	k8xC
obce	obec	k1gFnPc1
Zaběhlá	zaběhlý	k2eAgFnSc1d1
<g/>
,	,	kIx,
Padrť	padrť	k1gFnSc1
<g/>
,	,	kIx,
Kolvín	Kolvín	k1gInSc1
<g/>
,	,	kIx,
Hrachoviště	hrachoviště	k1gNnSc1
a	a	k8xC
Velcí	velký	k2eAgMnPc1d1
zaniknou	zaniknout	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelé	obyvatel	k1gMnPc1
se	se	k3xPyFc4
museli	muset	k5eAaImAgMnP
vystěhovat	vystěhovat	k5eAaPmF
do	do	k7c2
roku	rok	k1gInSc2
1952	#num#	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
pozemky	pozemek	k1gInPc1
byly	být	k5eAaImAgInP
bez	bez	k7c2
možnosti	možnost	k1gFnSc2
volby	volba	k1gFnSc2
vykupovány	vykupovat	k5eAaImNgFnP
za	za	k7c2
směšně	směšně	k6eAd1
nízké	nízký	k2eAgFnSc2d1
částky	částka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
obyvatele	obyvatel	k1gMnPc4
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
chtěli	chtít	k5eAaImAgMnP
zůstat	zůstat	k5eAaPmF
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
vyvíjen	vyvíjen	k2eAgInSc1d1
nátlak	nátlak	k1gInSc1
<g/>
,	,	kIx,
např.	např.	kA
tím	ten	k3xDgNnSc7
že	že	k8xS
bylo	být	k5eAaImAgNnS
zrušeno	zrušit	k5eAaPmNgNnS
autobusové	autobusový	k2eAgNnSc1d1
spojení	spojení	k1gNnSc1
nebo	nebo	k8xC
místní	místní	k2eAgInPc1d1
obchody	obchod	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Všechny	všechen	k3xTgFnPc1
vesnice	vesnice	k1gFnPc1
byly	být	k5eAaImAgFnP
srovnány	srovnat	k5eAaPmNgFnP
se	s	k7c7
zemí	zem	k1gFnSc7
a	a	k8xC
zhruba	zhruba	k6eAd1
1	#num#	k4
250	#num#	k4
původních	původní	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
si	se	k3xPyFc3
muselo	muset	k5eAaImAgNnS
hledat	hledat	k5eAaImF
nový	nový	k2eAgInSc4d1
domov	domov	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Ani	ani	k8xC
nízké	nízký	k2eAgNnSc4d1
finanční	finanční	k2eAgNnSc4d1
odškodnění	odškodnění	k1gNnSc4
vystěhovaným	vystěhovaný	k2eAgInSc7d1
nikterak	nikterak	k6eAd1
nepomohlo	pomoct	k5eNaPmAgNnS
<g/>
,	,	kIx,
protože	protože	k8xS
krátce	krátce	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
proběhla	proběhnout	k5eAaPmAgFnS
měnová	měnový	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
byly	být	k5eAaImAgFnP
veškeré	veškerý	k3xTgFnPc4
finanční	finanční	k2eAgFnPc4d1
úspory	úspora	k1gFnPc4
znehodnoceny	znehodnocen	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
pocházet	pocházet	k5eAaImF
z	z	k7c2
vystěhované	vystěhovaný	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
je	být	k5eAaImIp3nS
nepříjemný	příjemný	k2eNgInSc1d1
cejch	cejch	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
společnosti	společnost	k1gFnSc6
tabu	tabu	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1992	#num#	k4
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
společenská	společenský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Svaz	svaz	k1gInSc1
vyhnanců	vyhnanec	k1gMnPc2
z	z	k7c2
Brd	Brdy	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
dlouhodobě	dlouhodobě	k6eAd1
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
obnovení	obnovení	k1gNnSc4
obcí	obec	k1gFnPc2
Kolvín	Kolvína	k1gFnPc2
<g/>
,	,	kIx,
Padrť	padrť	k1gFnSc1
a	a	k8xC
Záběhlá	Záběhlý	k2eAgFnSc1d1
a	a	k8xC
především	především	k6eAd1
se	se	k3xPyFc4
neúspěšně	úspěšně	k6eNd1
dožaduje	dožadovat	k5eAaImIp3nS
spravedlivého	spravedlivý	k2eAgNnSc2d1
odškodnění	odškodnění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelé	obyvatel	k1gMnPc1
dotčených	dotčený	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
si	se	k3xPyFc3
prožili	prožít	k5eAaPmAgMnP
nepříjemné	příjemný	k2eNgFnPc4d1
chvíle	chvíle	k1gFnPc4
<g/>
,	,	kIx,
když	když	k8xS
byli	být	k5eAaImAgMnP
v	v	k7c6
průběhu	průběh	k1gInSc6
několika	několik	k4yIc2
let	léto	k1gNnPc2
hned	hned	k6eAd1
dvakrát	dvakrát	k6eAd1
násilně	násilně	k6eAd1
vystěhováni	vystěhovat	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
svazu	svaz	k1gInSc2
hlavně	hlavně	k9
poukazují	poukazovat	k5eAaImIp3nP
zejména	zejména	k9
na	na	k7c4
to	ten	k3xDgNnSc4
jak	jak	k6eAd1
za	za	k7c2
druhé	druhý	k4xOgFnSc2
vlny	vlna	k1gFnSc2
vysidlování	vysidlování	k1gNnSc2
nebyly	být	k5eNaImAgInP
dodržovány	dodržován	k2eAgInPc1d1
platné	platný	k2eAgInPc1d1
zákony	zákon	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2016	#num#	k4
vešel	vejít	k5eAaPmAgInS
v	v	k7c4
platnost	platnost	k1gFnSc4
zákon	zákon	k1gInSc4
<g/>
,	,	kIx,
kterým	který	k3yIgMnPc3,k3yQgMnPc3,k3yRgMnPc3
byl	být	k5eAaImAgInS
zrušen	zrušit	k5eAaPmNgInS
vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
Brdy	Brdy	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
byla	být	k5eAaImAgFnS
přílišná	přílišný	k2eAgFnSc1d1
rozsáhlost	rozsáhlost	k1gFnSc1
prostoru	prostor	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
málo	málo	k6eAd1
využívaný	využívaný	k2eAgInSc1d1
a	a	k8xC
k	k	k7c3
počtu	počet	k1gInSc3
vojáků	voják	k1gMnPc2
příliš	příliš	k6eAd1
velký	velký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
místě	místo	k1gNnSc6
vznikla	vzniknout	k5eAaPmAgFnS
chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Brdy	Brdy	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
bylo	být	k5eAaImAgNnS
u	u	k7c2
příležitosti	příležitost	k1gFnSc2
80	#num#	k4
<g/>
letého	letý	k2eAgNnSc2d1
výročí	výročí	k1gNnSc2
od	od	k7c2
vystěhování	vystěhování	k1gNnSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
vysázeno	vysázet	k5eAaPmNgNnS
šest	šest	k4xCc1
lip	lípa	k1gFnPc2
v	v	k7c6
místech	místo	k1gNnPc6
kde	kde	k6eAd1
stávaly	stávat	k5eAaImAgFnP
zaniklé	zaniklý	k2eAgFnPc1d1
obce	obec	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Demografie	demografie	k1gFnSc1
</s>
<s>
Počet	počet	k1gInSc1
místních	místní	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
stoupal	stoupat	k5eAaImAgInS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1890	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
zaznamenáno	zaznamenat	k5eAaPmNgNnS
nejvíce	hodně	k6eAd3,k6eAd1
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
to	ten	k3xDgNnSc4
565	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
sčítání	sčítání	k1gNnSc6
o	o	k7c4
deset	deset	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
v	v	k7c6
roce	rok	k1gInSc6
1900	#num#	k4
poklesl	poklesnout	k5eAaPmAgInS
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
o	o	k7c4
88	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýraznější	výrazný	k2eAgInSc1d3
úbytek	úbytek	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
nastal	nastat	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
<g/>
,	,	kIx,
při	při	k7c6
prvním	první	k4xOgInSc6
vystěhovávání	vystěhovávání	k1gNnSc6
obyvatel	obyvatel	k1gMnPc2
za	za	k7c2
německé	německý	k2eAgFnSc2d1
okupace	okupace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
vlna	vlna	k1gFnSc1
vystěhování	vystěhování	k1gNnSc2
přišla	přijít	k5eAaPmAgFnS
po	po	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
kompletnímu	kompletní	k2eAgNnSc3d1
vylidnění	vylidnění	k1gNnSc3
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
počtu	počet	k1gInSc2
domů	dům	k1gInPc2
v	v	k7c6
letech	let	k1gInPc6
1790	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
17901837184818701880189019001910192119301950	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
–	–	k?
<g/>
312452553565477493447431225	#num#	k4
</s>
<s>
Rozdíl	rozdíl	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
–	–	k?
<g/>
+	+	kIx~
<g/>
140	#num#	k4
<g/>
+	+	kIx~
<g/>
101	#num#	k4
<g/>
+	+	kIx~
<g/>
12	#num#	k4
<g/>
−	−	k?
<g/>
88	#num#	k4
<g/>
+	+	kIx~
<g/>
16	#num#	k4
<g/>
−	−	k?
<g/>
46	#num#	k4
<g/>
−	−	k?
<g/>
16	#num#	k4
<g/>
−	−	k?
<g/>
206	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
domů	dům	k1gInPc2
</s>
<s>
223434496176797777	#num#	k4
<g/>
–	–	k?
</s>
<s>
Rozdíl	rozdíl	k1gInSc1
počtu	počet	k1gInSc2
domů	dům	k1gInPc2
</s>
<s>
–	–	k?
<g/>
+	+	kIx~
<g/>
120	#num#	k4
<g/>
+	+	kIx~
<g/>
15	#num#	k4
<g/>
+	+	kIx~
<g/>
12	#num#	k4
<g/>
+	+	kIx~
<g/>
15	#num#	k4
<g/>
+	+	kIx~
<g/>
3	#num#	k4
<g/>
−	−	k?
<g/>
20	#num#	k4
<g/>
–	–	k?
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Škola	škola	k1gFnSc1
na	na	k7c6
Zaběhlé	zaběhlý	k2eAgFnSc6d1
vybudovaná	vybudovaný	k2eAgFnSc1d1
v	v	k7c6
letech	let	k1gInPc6
1923	#num#	k4
<g/>
–	–	k?
<g/>
1924	#num#	k4
</s>
<s>
Záběhelská	Záběhelský	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
mladých	mladý	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
u	u	k7c2
pomníku	pomník	k1gInSc2
obětem	oběť	k1gFnPc3
1	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
1926	#num#	k4
</s>
<s>
Hostinec	hostinec	k1gInSc1
u	u	k7c2
Honzíčků	Honzíček	k1gMnPc2
Jana	Jan	k1gMnSc2
Kuníka	Kuník	k1gMnSc2
v	v	k7c6
domě	dům	k1gInSc6
č.	č.	k?
15	#num#	k4
<g/>
,	,	kIx,
Zadní	zadní	k2eAgFnSc1d1
Zaběhlá	zaběhlý	k2eAgFnSc1d1
</s>
<s>
Místní	místní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
kronice	kronika	k1gFnSc6
obce	obec	k1gFnSc2
označováni	označován	k2eAgMnPc1d1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
Záběhelšťané	Záběhelšťan	k1gMnPc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1733	#num#	k4
zde	zde	k6eAd1
byli	být	k5eAaImAgMnP
zapsáni	zapsat	k5eAaPmNgMnP
<g/>
:	:	kIx,
Vavřinec	Vavřinec	k1gMnSc1
Voříšek	Voříšek	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Zíb	Zíb	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Fous	Fous	k?
<g/>
,	,	kIx,
Matěj	Matěj	k1gMnSc1
Čejka	Čejka	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Kozelka	Kozelek	k1gMnSc2
<g/>
,	,	kIx,
Bartoloměj	Bartoloměj	k1gMnSc1
Halla	Halla	k1gMnSc1
<g/>
,	,	kIx,
Matěj	Matěj	k1gMnSc1
Duchoň	Duchoň	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
Červený	Červený	k1gMnSc1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
Ziba	Ziba	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Kunik	Kunik	k1gMnSc1
<g/>
,	,	kIx,
Jak	jak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štěch	Štěch	k1gMnSc1
<g/>
,	,	kIx,
Vojt	vojt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čejka	Čejka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
přibyli	přibýt	k5eAaPmAgMnP
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Feld	Feld	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Voříšek	Voříšek	k1gMnSc1
<g/>
,	,	kIx,
Matěj	Matěj	k1gMnSc1
Feld	Feld	k1gMnSc1
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
Kachlovská	Kachlovský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Vavřinec	Vavřinec	k1gMnSc1
Sazima	Sazima	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1788	#num#	k4
zde	zde	k6eAd1
byli	být	k5eAaImAgMnP
zapsáni	zapsat	k5eAaPmNgMnP
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Loskot	Loskot	k1gMnSc1
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
Voříšek	Voříšek	k1gMnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
Čejka	Čejka	k1gMnSc1
<g/>
,	,	kIx,
Matouš	Matouš	k1gMnSc1
Sazima	Sazimum	k1gNnSc2
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
Fous	Fous	k?
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Duchoň	Duchoň	k1gMnSc1
<g/>
,	,	kIx,
Bartoloměj	Bartoloměj	k1gMnSc1
Blažek	Blažek	k1gMnSc1
<g/>
,	,	kIx,
Matouš	Matouš	k1gMnSc1
Kunik	Kunik	k1gMnSc1
<g/>
,	,	kIx,
Matěj	Matěj	k1gMnSc1
Čejka	Čejka	k1gMnSc1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
Tolma	Tolmum	k1gNnSc2
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Kunik	Kunik	k1gMnSc1
<g/>
,	,	kIx,
Bartoloměj	Bartoloměj	k1gMnSc1
Částka	částka	k1gFnSc1
<g/>
,	,	kIx,
Dorota	Dorota	k1gFnSc1
Kuniková	Kunikový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
Tolma	Tolmum	k1gNnSc2
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
Bacik	Bacik	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Picha	picha	k1gFnSc1
<g/>
,	,	kIx,
Matěj	Matěj	k1gMnSc1
Zeman	Zeman	k1gMnSc1
<g/>
,	,	kIx,
Rozina	Rozina	k1gFnSc1
Čejková	Čejková	k1gFnSc1
<g/>
,	,	kIx,
Vincenc	Vincenc	k1gMnSc1
Krobot	Krobot	k?
(	(	kIx(
<g/>
myslivec	myslivec	k1gMnSc1
panského	panský	k2eAgInSc2d1
revíru	revír	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Náboženský	náboženský	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Obec	obec	k1gFnSc1
farně	farně	k6eAd1
patřila	patřit	k5eAaImAgFnS
ke	k	k7c3
Starému	starý	k2eAgInSc3d1
Rožmitálu	Rožmitál	k1gInSc3
k	k	k7c3
místnímu	místní	k2eAgInSc3d1
farnímu	farní	k2eAgInSc3d1
kostelu	kostel	k1gInSc3
Povýšení	povýšení	k1gNnSc2
svatého	svatý	k2eAgMnSc2d1
Kříže	Kříž	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1930	#num#	k4
zde	zde	k6eAd1
stála	stát	k5eAaImAgFnS
římskokatolická	římskokatolický	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
financovalo	financovat	k5eAaBmAgNnS
pražské	pražský	k2eAgNnSc1d1
arcibiskupství	arcibiskupství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysvěcena	vysvěcen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
19	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1930	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náklady	náklad	k1gInPc1
na	na	k7c4
výstavbu	výstavba	k1gFnSc4
tehdy	tehdy	k6eAd1
činily	činit	k5eAaImAgInP
90	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kaple	kaple	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
oblíbeným	oblíbený	k2eAgNnSc7d1
místem	místo	k1gNnSc7
pro	pro	k7c4
uzavírání	uzavírání	k1gNnSc4
sňatků	sňatek	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
sčítání	sčítání	k1gNnSc6
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
447	#num#	k4
obyvatel	obyvatel	k1gMnPc2
z	z	k7c2
nichž	jenž	k3xRgFnPc2
se	se	k3xPyFc4
se	s	k7c7
349	#num#	k4
hlásilo	hlásit	k5eAaImAgNnS
k	k	k7c3
římskokatolické	římskokatolický	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
<g/>
,	,	kIx,
6	#num#	k4
k	k	k7c3
československé	československý	k2eAgFnSc3d1
husitské	husitský	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
a	a	k8xC
92	#num#	k4
bylo	být	k5eAaImAgNnS
bez	bez	k7c2
vyznání	vyznání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obecní	obecní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
</s>
<s>
Starostové	Starosta	k1gMnPc1
</s>
<s>
Josef	Josef	k1gMnSc1
Jaroš	Jaroš	k1gMnSc1
(	(	kIx(
<g/>
1905	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
Čejka	Čejka	k1gMnSc1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
–	–	k?
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Václav	Václav	k1gMnSc1
Podzimek	Podzimek	k1gMnSc1
(	(	kIx(
<g/>
1924	#num#	k4
<g/>
–	–	k?
<g/>
1932	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Václav	Václav	k1gMnSc1
Kuník	Kuník	k1gMnSc1
(	(	kIx(
<g/>
1932	#num#	k4
<g/>
–	–	k?
<g/>
1940	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Čejka	Čejka	k1gMnSc1
(	(	kIx(
<g/>
1940	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Silvestr	Silvestr	k1gMnSc1
Polák	Polák	k1gMnSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Antonín	Antonín	k1gMnSc1
Blecha	Blecha	k1gMnSc1
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Martin	Martin	k1gMnSc1
Voříšek	Voříšek	k1gMnSc1
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
–	–	k?
<g/>
1947	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Silvestr	Silvestr	k1gMnSc1
Polák	Polák	k1gMnSc1
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
František	František	k1gMnSc1
Voříšek	Voříšek	k1gMnSc1
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
–	–	k?
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Václav	Václav	k1gMnSc1
Vilt	Vilt	k1gMnSc1
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hostinec	hostinec	k1gInSc1
u	u	k7c2
Soukupů	Soukup	k1gMnPc2
<g/>
,	,	kIx,
Přední	přední	k2eAgFnSc1d1
Zaběhlá	zaběhlý	k2eAgFnSc1d1
</s>
<s>
Společnost	společnost	k1gFnSc1
</s>
<s>
Obec	obec	k1gFnSc1
patřila	patřit	k5eAaImAgFnS
poštou	pošta	k1gFnSc7
k	k	k7c3
Rožmitálu	Rožmitál	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelé	obyvatel	k1gMnPc1
živila	živit	k5eAaImAgFnS
především	především	k9
práce	práce	k1gFnSc1
v	v	k7c6
lese	les	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
káceli	kácet	k5eAaImAgMnP
a	a	k8xC
páili	páinout	k5eAaImAgMnP,k5eAaPmAgMnP
milíře	milíř	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
hlavně	hlavně	k9
se	se	k3xPyFc4
živili	živit	k5eAaImAgMnP
povoznictvím	povoznictví	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Zaběhlé	zaběhlý	k2eAgFnSc6d1
byly	být	k5eAaImAgFnP
tři	tři	k4xCgFnPc4
hospody	hospody	k?
<g/>
:	:	kIx,
V	v	k7c6
Zadní	zadní	k2eAgFnSc6d1
Zaběhlé	zaběhlý	k2eAgFnSc6d1
byl	být	k5eAaImAgInS
obchod	obchod	k1gInSc1
a	a	k8xC
hostinec	hostinec	k1gInSc1
Josefa	Josef	k1gMnSc2
Jaroše	Jaroš	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hostinec	hostinec	k1gInSc1
„	„	k?
<g/>
U	u	k7c2
Jarošů	Jaroš	k1gMnPc2
<g/>
“	“	k?
naposled	naposled	k6eAd1
vlastnila	vlastnit	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
paní	paní	k1gFnSc1
Bělohlávková	Bělohlávková	k1gFnSc1
z	z	k7c2
Kolvína	Kolvín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Zadní	zadní	k2eAgFnSc6d1
Zaběhlé	zaběhlý	k2eAgFnSc6d1
v	v	k7c6
domě	dům	k1gInSc6
č.	č.	k?
15	#num#	k4
býval	bývat	k5eAaImAgInS
hostinec	hostinec	k1gInSc1
„	„	k?
<g/>
U	u	k7c2
Honzíčků	honzíček	k1gInPc2
<g/>
“	“	k?
Jana	Jan	k1gMnSc2
Kuníka	Kuník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Přední	přední	k2eAgFnSc6d1
Zaběhlé	zaběhlý	k2eAgFnSc6d1
byl	být	k5eAaImAgInS
hostinec	hostinec	k1gInSc1
„	„	k?
<g/>
U	u	k7c2
Soukupů	Soukup	k1gMnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1879	#num#	k4
na	na	k7c4
Zaběhlé	zaběhlý	k2eAgNnSc4d1
sídlila	sídlit	k5eAaImAgFnS
četnická	četnický	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1926	#num#	k4
byla	být	k5eAaImAgFnS
přesídlena	přesídlet	k5eAaPmNgFnS
do	do	k7c2
Věšína	Věšín	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Školství	školství	k1gNnSc1
</s>
<s>
Do	do	k7c2
školy	škola	k1gFnSc2
děti	dítě	k1gFnPc1
chodili	chodit	k5eAaImAgMnP
v	v	k7c6
Padrti	padrť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastní	vlastní	k2eAgFnSc7d1
dvoutřídní	dvoutřídní	k2eAgFnSc7d1
školu	škola	k1gFnSc4
si	se	k3xPyFc3
v	v	k7c6
Zaběhlé	zaběhlý	k2eAgFnSc6d1
postavili	postavit	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1923	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkým	velký	k2eAgMnSc7d1
propagátorem	propagátor	k1gMnSc7
stavby	stavba	k1gFnSc2
byl	být	k5eAaImAgMnS
majitel	majitel	k1gMnSc1
místního	místní	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
a	a	k8xC
hostince	hostinec	k1gInSc2
Josef	Josef	k1gMnSc1
Jaroš	Jaroš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koulaudace	Koulaudace	k1gFnSc1
školy	škola	k1gFnSc2
proběhla	proběhnout	k5eAaPmAgFnS
25	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1923	#num#	k4
a	a	k8xC
druhého	druhý	k4xOgInSc2
dne	den	k1gInSc2
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
slavnost	slavnost	k1gFnSc1
na	na	k7c4
počest	počest	k1gFnSc4
dokončení	dokončení	k1gNnSc2
stavby	stavba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
čestný	čestný	k2eAgMnSc1d1
host	host	k1gMnSc1
se	se	k3xPyFc4
dostavil	dostavit	k5eAaPmAgMnS
bývalý	bývalý	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
školství	školství	k1gNnSc2
Gustav	Gustav	k1gMnSc1
Habrman	Habrman	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
prostornou	prostorný	k2eAgFnSc4d1
budovu	budova	k1gFnSc4
na	na	k7c6
pěkném	pěkný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průčelí	průčelí	k1gNnSc6
školy	škola	k1gFnSc2
bylo	být	k5eAaImAgNnS
heslo	heslo	k1gNnSc1
<g/>
:	:	kIx,
Vlasti	vlast	k1gFnPc1
k	k	k7c3
slávě	sláva	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národu	národ	k1gInSc2
ku	k	k7c3
pokroku	pokrok	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kultura	kultura	k1gFnSc1
</s>
<s>
Místní	místní	k2eAgMnPc1d1
měli	mít	k5eAaImAgMnP
svůj	svůj	k3xOyFgInSc4
divadelní	divadelní	k2eAgInSc4d1
spolek	spolek	k1gInSc4
a	a	k8xC
kapelu	kapela	k1gFnSc4
a	a	k8xC
také	také	k9
při	při	k7c6
základní	základní	k2eAgFnSc6d1
škole	škola	k1gFnSc6
existoval	existovat	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
divadelní	divadelní	k2eAgInSc4d1
kroužek	kroužek	k1gInSc4
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
zde	zde	k6eAd1
byl	být	k5eAaImAgInS
oddíl	oddíl	k1gInSc1
dělnické	dělnický	k2eAgFnSc2d1
tělocvičné	tělocvičný	k2eAgFnSc2d1
jednoty	jednota	k1gFnSc2
DTJ	DTJ	kA
Zaběhlá	zaběhlý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Dub	dub	k1gInSc1
na	na	k7c4
Záběhlé	Záběhlý	k2eAgNnSc4d1
1	#num#	k4
–	–	k?
dvoják	dvoják	k1gInSc1
</s>
<s>
Dub	dub	k1gInSc1
na	na	k7c4
Záběhlé	Záběhlý	k2eAgFnPc4d1
2	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
PRÁŠIL	Prášil	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
MAKAJ	MAKAJ	k?
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střední	střední	k2eAgInPc1d1
Brdy	Brdy	k1gInPc1
na	na	k7c6
starých	starý	k2eAgFnPc6d1
fotografiích	fotografia	k1gFnPc6
a	a	k8xC
pohlednicích	pohlednice	k1gFnPc6
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Baron	baron	k1gMnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
335	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86914	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Záběhlá	Záběhlý	k2eAgFnSc1d1
<g/>
,	,	kIx,
s.	s.	k?
143	#num#	k4
<g/>
-	-	kIx~
<g/>
149	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
CÍLEK	CÍLEK	kA
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střední	střední	k2eAgInPc1d1
Brdy	Brdy	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příbram	Příbram	k1gFnSc1
<g/>
:	:	kIx,
ČSOP	ČSOP	kA
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7084	#num#	k4
<g/>
-	-	kIx~
<g/>
266	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
VANDRA	VANDRA	kA
<g/>
,	,	kIx,
Radek	Radek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaniklé	zaniklý	k2eAgFnPc4d1
obce	obec	k1gFnPc4
v	v	k7c6
Brdech	brdo	k1gNnPc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
,	,	kIx,
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
SLAVÍK	Slavík	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Augustin	Augustin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rožmitál	Rožmitál	k1gInSc1
pod	pod	k7c7
Třemšínem	Třemšín	k1gInSc7
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
okolí	okolí	k1gNnSc1
<g/>
..	..	k?
Rožmitál	Rožmitál	k1gInSc1
pod	pod	k7c7
Třemšínem	Třemšín	k1gInSc7
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
294	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
239	#num#	k4
<g/>
-	-	kIx~
<g/>
4958	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Zaběhlá	zaběhlý	k2eAgFnSc1d1
<g/>
,	,	kIx,
s.	s.	k?
281	#num#	k4
<g/>
-	-	kIx~
<g/>
283	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
VACEK	Vacek	k1gMnSc1
<g/>
,	,	kIx,
Roman	Roman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fantastický	fantastický	k2eAgInSc1d1
pohled	pohled	k1gInSc1
do	do	k7c2
historie	historie	k1gFnSc2
<g/>
:	:	kIx,
Přední	přední	k2eAgFnSc1d1
a	a	k8xC
Zadní	zadní	k2eAgFnSc1d1
Záběhlá	Záběhlý	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
letech	léto	k1gNnPc6
1914	#num#	k4
až	až	k9
1952	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
objevbrdy	objevbrdy	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2020-6-22	2020-6-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Folklorika	Folklorika	k1gFnSc1
Zaniklé	zaniklý	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT	ČT	kA
<g/>
,	,	kIx,
2018-10-6	2018-10-6	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
VANDRA	VANDRA	kA
<g/>
,	,	kIx,
Radek	Radek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaniklé	zaniklý	k2eAgFnPc4d1
obce	obec	k1gFnPc4
v	v	k7c6
Brdech	brdo	k1gNnPc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
greenmind	greenmind	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2018-10-26	2018-10-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zaniklé	zaniklý	k2eAgFnSc2d1
brdské	brdský	k2eAgFnSc2d1
obce	obec	k1gFnSc2
připomenou	připomenout	k5eAaPmIp3nP
nově	nově	k6eAd1
vysazené	vysazený	k2eAgInPc1d1
stromy	strom	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
ochranaprirody	ochranaprirod	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2020-4-24	2020-4-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zaběhlá	zaběhlý	k2eAgFnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
zanikleobce	zanikleobce	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
F.	F.	kA
A.	A.	kA
Slavík	Slavík	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
36	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ČÁKA	čáka	k1gFnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střední	střední	k1gMnPc1
brdy	brdo	k1gNnPc7
-	-	kIx~
krajina	krajina	k1gFnSc1
neznámá	známý	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
158	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
752	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Ves	ves	k1gFnSc1
přezdívaná	přezdívaný	k2eAgFnSc1d1
Mozolov	Mozolov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
66	#num#	k4
<g/>
-	-	kIx~
<g/>
68	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Záběhlá	Záběhlý	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
