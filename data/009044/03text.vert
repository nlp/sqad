<p>
<s>
Hořčík	hořčík	k1gInSc1	hořčík
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Mg	mg	kA	mg
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Magnesium	magnesium	k1gNnSc1	magnesium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
lehký	lehký	k2eAgInSc1d1	lehký
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
stříbrolesklý	stříbrolesklý	k2eAgInSc1d1	stříbrolesklý
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
lehkých	lehký	k2eAgFnPc2d1	lehká
a	a	k8xC	a
pevných	pevný	k2eAgFnPc2d1	pevná
slitin	slitina	k1gFnPc2	slitina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
redukční	redukční	k2eAgNnSc1d1	redukční
činidlo	činidlo	k1gNnSc1	činidlo
v	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
syntéze	syntéza	k1gFnSc6	syntéza
a	a	k8xC	a
při	při	k7c6	při
pyrotechnických	pyrotechnický	k2eAgFnPc6d1	pyrotechnická
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc1d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Hořčík	hořčík	k1gInSc1	hořčík
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
<g/>
,	,	kIx,	,
lehký	lehký	k2eAgInSc1d1	lehký
<g/>
,	,	kIx,	,
tažný	tažný	k2eAgInSc1d1	tažný
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vyšší	vysoký	k2eAgFnSc4d2	vyšší
hustotu	hustota	k1gFnSc4	hustota
než	než	k8xS	než
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
hůře	zle	k6eAd2	zle
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
a	a	k8xC	a
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Hořčík	hořčík	k1gInSc1	hořčík
lze	lze	k6eAd1	lze
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
dobré	dobrý	k2eAgFnSc3d1	dobrá
tažnosti	tažnost	k1gFnSc3	tažnost
snadno	snadno	k6eAd1	snadno
válcovat	válcovat	k5eAaImF	válcovat
na	na	k7c4	na
plechy	plech	k1gInPc4	plech
a	a	k8xC	a
dráty	drát	k1gInPc4	drát
<g/>
.	.	kIx.	.
</s>
<s>
Hořčík	hořčík	k1gInSc1	hořčík
není	být	k5eNaImIp3nS	být
tolik	tolik	k6eAd1	tolik
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
jako	jako	k8xC	jako
další	další	k2eAgInPc1d1	další
kovy	kov	k1gInPc1	kov
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
neuchovává	uchovávat	k5eNaImIp3nS	uchovávat
pod	pod	k7c7	pod
petrolejem	petrolej	k1gInSc7	petrolej
nebo	nebo	k8xC	nebo
naftou	nafta	k1gFnSc7	nafta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stačí	stačit	k5eAaBmIp3nP	stačit
nádoby	nádoba	k1gFnPc1	nádoba
se	s	k7c7	s
suchým	suchý	k2eAgInSc7d1	suchý
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Hořčík	hořčík	k1gInSc1	hořčík
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
slévá	slévat	k5eAaImIp3nS	slévat
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
kovy	kov	k1gInPc7	kov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
málo	málo	k4c1	málo
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
má	mít	k5eAaImIp3nS	mít
praktické	praktický	k2eAgNnSc1d1	praktické
využití	využití	k1gNnSc1	využití
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
snadno	snadno	k6eAd1	snadno
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hořčík	hořčík	k1gInSc1	hořčík
reaguje	reagovat	k5eAaBmIp3nS	reagovat
za	za	k7c2	za
normální	normální	k2eAgFnSc2d1	normální
teploty	teplota	k1gFnSc2	teplota
pomalu	pomalu	k6eAd1	pomalu
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
a	a	k8xC	a
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
suchém	suchý	k2eAgInSc6d1	suchý
vzduchu	vzduch	k1gInSc6	vzduch
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
pokryje	pokrýt	k5eAaPmIp3nS	pokrýt
vrstvou	vrstva	k1gFnSc7	vrstva
oxidu	oxid	k1gInSc2	oxid
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gMnSc4	on
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
další	další	k2eAgFnSc7d1	další
oxidací	oxidace	k1gFnSc7	oxidace
<g/>
,	,	kIx,	,
a	a	k8xC	a
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
takto	takto	k6eAd1	takto
uchovávat	uchovávat	k5eAaImF	uchovávat
i	i	k9	i
poměrně	poměrně	k6eAd1	poměrně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hoření	hoření	k1gNnSc6	hoření
hořčíku	hořčík	k1gInSc2	hořčík
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
vzniká	vznikat	k5eAaImIp3nS	vznikat
velmi	velmi	k6eAd1	velmi
intenzivní	intenzivní	k2eAgNnSc1d1	intenzivní
bílé	bílý	k2eAgNnSc1d1	bílé
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vodou	voda	k1gFnSc7	voda
reaguje	reagovat	k5eAaBmIp3nS	reagovat
hořčík	hořčík	k1gInSc1	hořčík
za	za	k7c4	za
normální	normální	k2eAgFnPc4d1	normální
teploty	teplota	k1gFnPc4	teplota
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
hydroxidu	hydroxid	k1gInSc2	hydroxid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vyšší	vysoký	k2eAgFnSc6d2	vyšší
teplotě	teplota	k1gFnSc6	teplota
se	se	k3xPyFc4	se
hořčík	hořčík	k1gInSc1	hořčík
slučuje	slučovat	k5eAaImIp3nS	slučovat
velmi	velmi	k6eAd1	velmi
ochotně	ochotně	k6eAd1	ochotně
téměř	téměř	k6eAd1	téměř
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
prvky	prvek	k1gInPc7	prvek
a	a	k8xC	a
i	i	k9	i
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
sloučeninami	sloučenina	k1gFnPc7	sloučenina
–	–	k?	–
např.	např.	kA	např.
při	při	k7c6	při
hoření	hoření	k1gNnSc6	hoření
hořčíku	hořčík	k1gInSc2	hořčík
v	v	k7c6	v
dusíkaté	dusíkatý	k2eAgFnSc6d1	dusíkatá
atmosféře	atmosféra	k1gFnSc6	atmosféra
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
nitrid	nitrid	k1gInSc4	nitrid
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vodou	voda	k1gFnSc7	voda
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxidu	oxid	k1gInSc2	oxid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
a	a	k8xC	a
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
,	,	kIx,	,
a	a	k8xC	a
při	při	k7c6	při
hoření	hoření	k1gNnSc6	hoření
hořčíku	hořčík	k1gInSc2	hořčík
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
oxid	oxid	k1gInSc4	oxid
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
a	a	k8xC	a
uhlík	uhlík	k1gInSc4	uhlík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hořčík	hořčík	k1gInSc1	hořčík
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
běžných	běžný	k2eAgFnPc6d1	běžná
kyselinách	kyselina	k1gFnPc6	kyselina
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
hořečnatých	hořečnatý	k2eAgFnPc2d1	hořečnatá
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rozpouštění	rozpouštění	k1gNnSc6	rozpouštění
v	v	k7c6	v
koncentrované	koncentrovaný	k2eAgFnSc6d1	koncentrovaná
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc6d1	sírová
uniká	unikat	k5eAaImIp3nS	unikat
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rozpouštění	rozpouštění	k1gNnSc6	rozpouštění
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
dusičné	dusičný	k2eAgFnPc1d1	dusičná
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
koncentrace	koncentrace	k1gFnSc2	koncentrace
tvoří	tvořit	k5eAaImIp3nP	tvořit
vedle	vedle	k7c2	vedle
hořečnatých	hořečnatý	k2eAgFnPc2d1	hořečnatá
solí	sůl	k1gFnPc2	sůl
i	i	k8xC	i
další	další	k2eAgFnPc1d1	další
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnSc3d1	nízká
koncentraci	koncentrace	k1gFnSc3	koncentrace
vzniká	vznikat	k5eAaImIp3nS	vznikat
dusičnan	dusičnan	k1gInSc1	dusičnan
amonný	amonný	k2eAgInSc1d1	amonný
(	(	kIx(	(
<g/>
koncentrace	koncentrace	k1gFnSc1	koncentrace
méně	málo	k6eAd2	málo
než	než	k8xS	než
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
s	s	k7c7	s
ředěnou	ředěný	k2eAgFnSc7d1	ředěná
kyselinou	kyselina	k1gFnSc7	kyselina
dusičnou	dusičný	k2eAgFnSc7d1	dusičná
vzniká	vznikat	k5eAaImIp3nS	vznikat
oxid	oxid	k1gInSc1	oxid
dusný	dusný	k2eAgInSc1d1	dusný
(	(	kIx(	(
<g/>
koncentrace	koncentrace	k1gFnSc1	koncentrace
5	[number]	k4	5
%	%	kIx~	%
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
koncentovanější	koncentovaný	k2eAgFnSc7d2	koncentovaný
reaguje	reagovat	k5eAaBmIp3nS	reagovat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxidu	oxid	k1gInSc2	oxid
dusnatého	dusnatý	k2eAgInSc2d1	dusnatý
(	(	kIx(	(
<g/>
koncentrace	koncentrace	k1gFnSc1	koncentrace
10	[number]	k4	10
<g/>
%	%	kIx~	%
<g/>
–	–	k?	–
<g/>
asi	asi	k9	asi
50	[number]	k4	50
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
koncentrovanou	koncentrovaný	k2eAgFnSc7d1	koncentrovaná
kyselinou	kyselina	k1gFnSc7	kyselina
reaguje	reagovat	k5eAaBmIp3nS	reagovat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxidu	oxid	k1gInSc2	oxid
dusičitého	dusičitý	k2eAgInSc2d1	dusičitý
(	(	kIx(	(
<g/>
koncentrace	koncentrace	k1gFnSc1	koncentrace
50	[number]	k4	50
%	%	kIx~	%
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
chlorovodíkovou	chlorovodíkový	k2eAgFnSc7d1	chlorovodíková
a	a	k8xC	a
zředěnou	zředěný	k2eAgFnSc7d1	zředěná
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
reaguje	reagovat	k5eAaBmIp3nS	reagovat
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
hořečnatých	hořečnatý	k2eAgFnPc2d1	hořečnatá
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
alkalickými	alkalický	k2eAgInPc7d1	alkalický
hydroxidy	hydroxid	k1gInPc7	hydroxid
hořčík	hořčík	k1gInSc1	hořčík
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
síranu	síran	k1gInSc3	síran
hořečnatého	hořečnatý	k2eAgNnSc2d1	hořečnaté
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
hořké	hořká	k1gFnSc2	hořká
soli	sůl	k1gFnSc2	sůl
<g/>
)	)	kIx)	)
používalo	používat	k5eAaImAgNnS	používat
v	v	k7c6	v
léčitelství	léčitelství	k1gNnSc6	léčitelství
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
hořečnatý	hořečnatý	k2eAgMnSc1d1	hořečnatý
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nazýván	nazýván	k2eAgMnSc1d1	nazýván
jako	jako	k8xC	jako
hořká	hořký	k2eAgFnSc1d1	hořká
zemina	zemina	k1gFnSc1	zemina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
začal	začít	k5eAaPmAgMnS	začít
rozeznávat	rozeznávat	k5eAaImF	rozeznávat
hořkou	hořký	k2eAgFnSc4d1	hořká
a	a	k8xC	a
vápennou	vápenný	k2eAgFnSc4d1	vápenná
zeminu	zemina	k1gFnSc4	zemina
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Joseph	Joseph	k1gMnSc1	Joseph
Black	Black	k1gMnSc1	Black
roku	rok	k1gInSc2	rok
1755	[number]	k4	1755
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
hořčík	hořčík	k1gInSc1	hořčík
v	v	k7c6	v
elementární	elementární	k2eAgFnSc6d1	elementární
formě	forma	k1gFnSc6	forma
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
taveniny	tavenina	k1gFnSc2	tavenina
chloridu	chlorid	k1gInSc2	chlorid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
MgCl	MgCl	k1gMnSc1	MgCl
<g/>
2	[number]	k4	2
připravil	připravit	k5eAaPmAgMnS	připravit
sir	sir	k1gMnSc1	sir
Humphry	Humphra	k1gFnSc2	Humphra
Davy	Dav	k1gInPc4	Dav
roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
<g/>
.	.	kIx.	.
</s>
<s>
Chemickou	chemický	k2eAgFnSc7d1	chemická
cestou	cesta	k1gFnSc7	cesta
byl	být	k5eAaImAgInS	být
hořčík	hořčík	k1gInSc1	hořčík
poprvé	poprvé	k6eAd1	poprvé
připraven	připravit	k5eAaPmNgInS	připravit
působením	působení	k1gNnSc7	působení
par	para	k1gFnPc2	para
kovového	kovový	k2eAgInSc2d1	kovový
draslíku	draslík	k1gInSc2	draslík
na	na	k7c4	na
bezvodý	bezvodý	k2eAgInSc4d1	bezvodý
chlorid	chlorid	k1gInSc4	chlorid
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
přípravu	příprava	k1gFnSc4	příprava
hořčíku	hořčík	k1gInSc2	hořčík
provedl	provést	k5eAaPmAgInS	provést
Bussy	Bussa	k1gFnSc2	Bussa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgFnSc3d1	velká
reaktivitě	reaktivita	k1gFnSc3	reaktivita
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
hořčík	hořčík	k1gInSc1	hořčík
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
má	mít	k5eAaImIp3nS	mít
mocenství	mocenství	k1gNnSc6	mocenství
Mg	mg	kA	mg
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hořčík	hořčík	k1gInSc1	hořčík
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
jak	jak	k6eAd1	jak
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vzniká	vznikat	k5eAaImIp3nS	vznikat
ve	v	k7c6	v
hvězdách	hvězda	k1gFnPc6	hvězda
těžších	těžký	k2eAgFnPc6d2	těžší
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
fúzí	fúze	k1gFnPc2	fúze
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
2	[number]	k4	2
126	[number]	k4	126
C	C	kA	C
→	→	k?	→
2412	[number]	k4	2412
Mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
posledních	poslední	k2eAgInPc2d1	poslední
dostupných	dostupný	k2eAgInPc2d1	dostupný
údajů	údaj	k1gInPc2	údaj
tvoří	tvořit	k5eAaImIp3nS	tvořit
hořčík	hořčík	k1gInSc1	hořčík
1,9	[number]	k4	1,9
<g/>
–	–	k?	–
<g/>
2,5	[number]	k4	2,5
%	%	kIx~	%
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
podle	podle	k7c2	podle
výskytu	výskyt	k1gInSc2	výskyt
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
procentuální	procentuální	k2eAgInSc1d1	procentuální
obsah	obsah	k1gInSc1	obsah
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
27	[number]	k4	27
640	[number]	k4	640
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
2,764	[number]	k4	2,764
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
parts	parts	k1gInSc1	parts
per	pero	k1gNnPc2	pero
million	million	k1gInSc1	million
=	=	kIx~	=
počet	počet	k1gInSc1	počet
částic	částice	k1gFnPc2	částice
na	na	k7c4	na
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
částic	částice	k1gFnPc2	částice
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
výskytu	výskyt	k1gInSc6	výskyt
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
za	za	k7c4	za
vápník	vápník	k1gInSc4	vápník
a	a	k8xC	a
před	před	k7c4	před
sodík	sodík	k1gInSc4	sodík
a	a	k8xC	a
draslík	draslík	k1gInSc4	draslík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
koncentrace	koncentrace	k1gFnSc1	koncentrace
hořčíkových	hořčíkový	k2eAgInPc2d1	hořčíkový
iontů	ion	k1gInPc2	ion
udává	udávat	k5eAaImIp3nS	udávat
jako	jako	k9	jako
1,35	[number]	k4	1,35
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
po	po	k7c6	po
sodíku	sodík	k1gInSc6	sodík
druhým	druhý	k4xOgInSc7	druhý
nejvíce	hodně	k6eAd3	hodně
zastoupeným	zastoupený	k2eAgInSc7d1	zastoupený
kationtem	kation	k1gInSc7	kation
<g/>
,	,	kIx,	,
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zejména	zejména	k9	zejména
chlorid	chlorid	k1gInSc1	chlorid
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
MgCl	MgCl	k1gInSc4	MgCl
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
bromid	bromid	k1gInSc1	bromid
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
MgBr	MgBr	k1gInSc4	MgBr
<g/>
2	[number]	k4	2
a	a	k8xC	a
síran	síran	k1gInSc4	síran
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
MgSO	MgSO	k1gFnSc7	MgSO
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
hořčíku	hořčík	k1gInSc2	hořčík
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
30	[number]	k4	30
000	[number]	k4	000
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hořčík	hořčík	k1gInSc1	hořčík
je	být	k5eAaImIp3nS	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
významným	významný	k2eAgInSc7d1	významný
biogenním	biogenní	k2eAgInSc7d1	biogenní
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zelených	zelený	k2eAgFnPc6d1	zelená
rostlinách	rostlina	k1gFnPc6	rostlina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
organismech	organismus	k1gInPc6	organismus
živočichů	živočich	k1gMnPc2	živočich
se	se	k3xPyFc4	se
také	také	k9	také
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
významné	významný	k2eAgInPc4d1	významný
biogenní	biogenní	k2eAgInPc4d1	biogenní
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obsah	obsah	k1gInSc1	obsah
hořčíku	hořčík	k1gInSc2	hořčík
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
uváděný	uváděný	k2eAgInSc1d1	uváděný
jako	jako	k8xC	jako
chlorid	chlorid	k1gInSc1	chlorid
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
MgCl	MgCl	k1gInSc4	MgCl
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
tvoří	tvořit	k5eAaImIp3nP	tvořit
významný	významný	k2eAgInSc4d1	významný
podíl	podíl	k1gInSc4	podíl
jeho	on	k3xPp3gNnSc2	on
zastoupení	zastoupení	k1gNnSc2	zastoupení
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
minerálů	minerál	k1gInPc2	minerál
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
hojný	hojný	k2eAgInSc1d1	hojný
dolomit	dolomit	k1gInSc1	dolomit
<g/>
,	,	kIx,	,
směsný	směsný	k2eAgInSc1d1	směsný
uhličitan	uhličitan	k1gInSc1	uhličitan
hořečnato-vápenatý	hořečnatoápenatý	k2eAgInSc4d1	hořečnato-vápenatý
CaMg	CaMg	k1gInSc4	CaMg
<g/>
(	(	kIx(	(
<g/>
CO	co	k6eAd1	co
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnPc1	jehož
ložiska	ložisko	k1gNnPc1	ložisko
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc6d1	jižní
Austrálii	Austrálie	k1gFnSc6	Austrálie
i	i	k8xC	i
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Poněkud	poněkud	k6eAd1	poněkud
vzácněji	vzácně	k6eAd2	vzácně
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
čistý	čistý	k2eAgInSc4d1	čistý
uhličitan	uhličitan	k1gInSc4	uhličitan
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
<g/>
,	,	kIx,	,
MgCO	MgCO	k1gFnSc1	MgCO
<g/>
3	[number]	k4	3
–	–	k?	–
magnezit	magnezit	k1gInSc1	magnezit
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
především	především	k9	především
v	v	k7c6	v
rakouských	rakouský	k2eAgFnPc6d1	rakouská
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
a	a	k8xC	a
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
méně	málo	k6eAd2	málo
významným	významný	k2eAgInPc3d1	významný
minerálům	minerál	k1gInPc3	minerál
patří	patřit	k5eAaImIp3nS	patřit
karnalit	karnalit	k1gInSc1	karnalit
KCl	KCl	k1gFnSc2	KCl
<g/>
.	.	kIx.	.
</s>
<s>
MgCl	MgCl	k1gInSc1	MgCl
<g/>
2.6	[number]	k4	2.6
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
bischofit	bischofit	k2eAgMnSc1d1	bischofit
MgCl	MgCl	k1gMnSc1	MgCl
<g/>
2.6	[number]	k4	2.6
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
periklas	periklas	k1gMnSc1	periklas
MgO	MgO	k1gMnSc1	MgO
<g/>
,	,	kIx,	,
brucit	brucit	k1gMnSc1	brucit
Mg	mg	kA	mg
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
kieserit	kieserit	k1gInSc1	kieserit
MgSO	MgSO	k1gFnSc1	MgSO
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
epsomit	epsomit	k1gInSc1	epsomit
MgSO	MgSO	k1gFnSc1	MgSO
<g/>
4.7	[number]	k4	4.7
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
kainit	kainit	k1gInSc4	kainit
KCl	KCl	k1gFnSc2	KCl
<g/>
.	.	kIx.	.
</s>
<s>
MgSO	MgSO	k?	MgSO
<g/>
4.3	[number]	k4	4.3
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
polyhalit	polyhalit	k1gInSc1	polyhalit
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
MgSO	MgSO	k?	MgSO
<g/>
4.2	[number]	k4	4.2
CaSO	CaSO	k1gFnSc1	CaSO
<g/>
4.2	[number]	k4	4.2
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
boromagnesit	boromagnesit	k1gInSc1	boromagnesit
Mg	mg	kA	mg
<g/>
5	[number]	k4	5
<g/>
B	B	kA	B
<g/>
4	[number]	k4	4
<g/>
O	o	k7c4	o
<g/>
11.2	[number]	k4	11.2
<g/>
1⁄	1⁄	k?	1⁄
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
hydroboracit	hydroboracit	k1gMnSc1	hydroboracit
CaMgB	CaMgB	k1gFnSc2	CaMgB
<g/>
6	[number]	k4	6
<g/>
O	o	k7c4	o
<g/>
11.5	[number]	k4	11.5
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
spinel	spinel	k1gInSc1	spinel
<g />
.	.	kIx.	.
</s>
<s>
MgO	MgO	k?	MgO
<g/>
.	.	kIx.	.
<g/>
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
magnesioferrit	magnesioferrit	k1gInSc1	magnesioferrit
MgO	MgO	k1gFnSc1	MgO
<g/>
.	.	kIx.	.
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
pleonast	pleonast	k1gInSc1	pleonast
(	(	kIx(	(
<g/>
Mg	mg	kA	mg
<g/>
,	,	kIx,	,
Fe	Fe	k1gFnSc1	Fe
<g/>
)	)	kIx)	)
<g/>
O.	O.	kA	O.
<g/>
(	(	kIx(	(
<g/>
Al	ala	k1gFnPc2	ala
<g/>
,	,	kIx,	,
Fe	Fe	k1gFnSc1	Fe
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
picotit	picotit	k5eAaImF	picotit
(	(	kIx(	(
<g/>
Mg	mg	kA	mg
<g/>
,	,	kIx,	,
Fe	Fe	k1gFnSc1	Fe
<g/>
)	)	kIx)	)
<g/>
O.	O.	kA	O.
(	(	kIx(	(
<g/>
Al	ala	k1gFnPc2	ala
<g/>
,	,	kIx,	,
Cr	cr	k0	cr
<g/>
,	,	kIx,	,
Fe	Fe	k1gMnSc1	Fe
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
olivín	olivín	k1gInSc1	olivín
(	(	kIx(	(
<g/>
Mg	mg	kA	mg
<g/>
,	,	kIx,	,
Fe	Fe	k1gFnSc1	Fe
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
[	[	kIx(	[
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
fosforečnanů	fosforečnan	k1gInPc2	fosforečnan
<g/>
,	,	kIx,	,
arseničnanů	arseničnan	k1gInPc2	arseničnan
a	a	k8xC	a
křemičitanů	křemičitan	k1gInPc2	křemičitan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ročně	ročně	k6eAd1	ročně
se	se	k3xPyFc4	se
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
hořčíku	hořčík	k1gInSc2	hořčík
a	a	k8xC	a
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInSc3	jeho
vysokému	vysoký	k2eAgInSc3d1	vysoký
výskytu	výskyt	k1gInSc3	výskyt
v	v	k7c6	v
nerostech	nerost	k1gInPc6	nerost
i	i	k8xC	i
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
lze	lze	k6eAd1	lze
jeho	jeho	k3xOp3gFnPc4	jeho
zásoby	zásoba	k1gFnPc4	zásoba
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
neomezené	omezený	k2eNgFnPc4d1	neomezená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Kovový	kovový	k2eAgInSc1d1	kovový
hořčík	hořčík	k1gInSc1	hořčík
se	se	k3xPyFc4	se
průmyslově	průmyslově	k6eAd1	průmyslově
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
obvykle	obvykle	k6eAd1	obvykle
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
roztavené	roztavený	k2eAgFnSc2d1	roztavená
směsi	směs	k1gFnSc2	směs
chloridu	chlorid	k1gInSc2	chlorid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
a	a	k8xC	a
chloridu	chlorid	k1gInSc2	chlorid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
draselný	draselný	k2eAgInSc1d1	draselný
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k8xC	jako
přísada	přísada	k1gFnSc1	přísada
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
teploty	teplota	k1gFnSc2	teplota
tání	tání	k1gNnSc2	tání
chloridu	chlorid	k1gInSc2	chlorid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc4	chlorid
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
z	z	k7c2	z
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
nebo	nebo	k8xC	nebo
z	z	k7c2	z
koncentrovaných	koncentrovaný	k2eAgInPc2d1	koncentrovaný
roztoků	roztok	k1gInPc2	roztok
mořské	mořský	k2eAgFnSc2d1	mořská
soli	sůl	k1gFnSc2	sůl
(	(	kIx(	(
<g/>
solanka	solanka	k1gFnSc1	solanka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
tavením	tavení	k1gNnSc7	tavení
karnalitu	karnalit	k1gInSc2	karnalit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
elektrolýze	elektrolýza	k1gFnSc6	elektrolýza
se	se	k3xPyFc4	se
na	na	k7c6	na
grafitové	grafitový	k2eAgFnSc6d1	grafitová
anodě	anoda	k1gFnSc6	anoda
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
chlor	chlor	k1gInSc1	chlor
a	a	k8xC	a
na	na	k7c6	na
železné	železný	k2eAgFnSc6d1	železná
katodě	katoda	k1gFnSc6	katoda
hořčík	hořčík	k1gInSc1	hořčík
(	(	kIx(	(
<g/>
chlorid	chlorid	k1gInSc1	chlorid
draselný	draselný	k2eAgInSc1d1	draselný
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
rozkládat	rozkládat	k5eAaImF	rozkládat
až	až	k9	až
po	po	k7c6	po
rozložení	rozložení	k1gNnSc6	rozložení
chloridu	chlorid	k1gInSc2	chlorid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roztavený	roztavený	k2eAgInSc1d1	roztavený
hořčík	hořčík	k1gInSc1	hořčík
stoupá	stoupat	k5eAaImIp3nS	stoupat
v	v	k7c6	v
tavenině	tavenina	k1gFnSc6	tavenina
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
sbírá	sbírat	k5eAaImIp3nS	sbírat
se	se	k3xPyFc4	se
děrovanými	děrovaný	k2eAgFnPc7d1	děrovaná
lžícemi	lžíce	k1gFnPc7	lžíce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInSc1d1	další
termický	termický	k2eAgInSc1d1	termický
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
hojně	hojně	k6eAd1	hojně
využívaný	využívaný	k2eAgInSc1d1	využívaný
<g/>
,	,	kIx,	,
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
redukci	redukce	k1gFnSc6	redukce
oxidu	oxid	k1gInSc2	oxid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
karbidem	karbid	k1gInSc7	karbid
vápenatým	vápenatý	k2eAgInSc7d1	vápenatý
nebo	nebo	k8xC	nebo
uhlíkem	uhlík	k1gInSc7	uhlík
–	–	k?	–
karbotermický	karbotermický	k2eAgInSc1d1	karbotermický
způsob	způsob	k1gInSc1	způsob
nebo	nebo	k8xC	nebo
redukcí	redukce	k1gFnSc7	redukce
oxidu	oxid	k1gInSc2	oxid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
křemíkem	křemík	k1gInSc7	křemík
–	–	k?	–
silikotermický	silikotermický	k2eAgInSc1d1	silikotermický
způsob	způsob	k1gInSc1	způsob
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karbotermický	Karbotermický	k2eAgInSc1d1	Karbotermický
způsob	způsob	k1gInSc1	způsob
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
v	v	k7c6	v
elektrické	elektrický	k2eAgFnSc6d1	elektrická
obloukové	obloukový	k2eAgFnSc6d1	oblouková
peci	pec	k1gFnSc6	pec
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
nad	nad	k7c7	nad
2000	[number]	k4	2000
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
reakci	reakce	k1gFnSc3	reakce
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgNnSc2d1	uhelnatý
s	s	k7c7	s
parami	para	k1gFnPc7	para
hořčíku	hořčík	k1gInSc2	hořčík
<g/>
.	.	kIx.	.
</s>
<s>
Jednodušší	jednoduchý	k2eAgFnSc1d2	jednodušší
je	být	k5eAaImIp3nS	být
redukce	redukce	k1gFnSc1	redukce
oxidu	oxid	k1gInSc2	oxid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
karbidem	karbid	k1gInSc7	karbid
vápenatým	vápenatý	k2eAgInSc7d1	vápenatý
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
1200	[number]	k4	1200
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
nízké	nízký	k2eAgFnSc2d1	nízká
ceny	cena	k1gFnSc2	cena
karbidu	karbid	k1gInSc2	karbid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Silikotermický	Silikotermický	k2eAgInSc1d1	Silikotermický
způsob	způsob	k1gInSc1	způsob
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
reakcí	reakce	k1gFnSc7	reakce
páleného	pálený	k2eAgInSc2d1	pálený
dolomitu	dolomit	k1gInSc2	dolomit
s	s	k7c7	s
křemíkem	křemík	k1gInSc7	křemík
nebo	nebo	k8xC	nebo
ferrosiliciem	ferrosilicium	k1gNnSc7	ferrosilicium
v	v	k7c6	v
ocelolitinových	ocelolitinový	k2eAgFnPc6d1	ocelolitinová
retortách	retorta	k1gFnPc6	retorta
zahřívaných	zahřívaný	k2eAgFnPc6d1	zahřívaná
na	na	k7c4	na
1200	[number]	k4	1200
°	°	k?	°
<g/>
C	C	kA	C
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vysokovakuových	vysokovakuův	k2eAgFnPc6d1	vysokovakuův
pecích	pec	k1gFnPc6	pec
<g/>
.	.	kIx.	.
</s>
<s>
Destilující	destilující	k2eAgInSc1d1	destilující
hořčík	hořčík	k1gInSc1	hořčík
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
čistota	čistota	k1gFnSc1	čistota
je	být	k5eAaImIp3nS	být
98	[number]	k4	98
<g/>
–	–	k?	–
<g/>
99	[number]	k4	99
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
kondenzuje	kondenzovat	k5eAaImIp3nS	kondenzovat
v	v	k7c6	v
předchlazených	předchlazený	k2eAgFnPc6d1	předchlazený
nádobách	nádoba	k1gFnPc6	nádoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
setkáme	setkat	k5eAaPmIp1nP	setkat
se	s	k7c7	s
slitinami	slitina	k1gFnPc7	slitina
hořčíku	hořčík	k1gInSc2	hořčík
s	s	k7c7	s
hliníkem	hliník	k1gInSc7	hliník
<g/>
,	,	kIx,	,
mědí	měď	k1gFnSc7	měď
a	a	k8xC	a
manganem	mangan	k1gInSc7	mangan
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
dural	dural	k1gInSc1	dural
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
značnou	značný	k2eAgFnSc7d1	značná
mechanickou	mechanický	k2eAgFnSc7d1	mechanická
pevností	pevnost	k1gFnSc7	pevnost
a	a	k8xC	a
současně	současně	k6eAd1	současně
nízkou	nízký	k2eAgFnSc7d1	nízká
hustotou	hustota	k1gFnSc7	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
značně	značně	k6eAd1	značně
odolné	odolný	k2eAgFnPc1d1	odolná
vůči	vůči	k7c3	vůči
korozi	koroze	k1gFnSc3	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
vlastnosti	vlastnost	k1gFnPc1	vlastnost
předurčují	předurčovat	k5eAaImIp3nP	předurčovat
dural	dural	k1gInSc4	dural
jako	jako	k8xC	jako
ideální	ideální	k2eAgInSc4d1	ideální
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
letecký	letecký	k2eAgInSc4d1	letecký
a	a	k8xC	a
automobilový	automobilový	k2eAgInSc4d1	automobilový
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
i	i	k9	i
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
výtahů	výtah	k1gInPc2	výtah
<g/>
,	,	kIx,	,
jízdních	jízdní	k2eAgNnPc2d1	jízdní
kol	kolo	k1gNnPc2	kolo
<g/>
,	,	kIx,	,
lehkých	lehký	k2eAgInPc2d1	lehký
žebříků	žebřík	k1gInPc2	žebřík
ap.	ap.	kA	ap.
Ještě	ještě	k6eAd1	ještě
nižší	nízký	k2eAgFnSc4d2	nižší
hustotu	hustota	k1gFnSc4	hustota
než	než	k8xS	než
dural	dural	k1gInSc4	dural
má	mít	k5eAaImIp3nS	mít
slitina	slitina	k1gFnSc1	slitina
s	s	k7c7	s
hliníkem	hliník	k1gInSc7	hliník
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
nejméně	málo	k6eAd3	málo
90	[number]	k4	90
%	%	kIx~	%
hořčíku	hořčík	k1gInSc2	hořčík
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
elektron	elektron	k1gInSc1	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
slitina	slitina	k1gFnSc1	slitina
hořčíku	hořčík	k1gInSc2	hořčík
je	být	k5eAaImIp3nS	být
magnalium	magnalium	k1gNnSc1	magnalium
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
%	%	kIx~	%
hořčíku	hořčík	k1gInSc2	hořčík
a	a	k8xC	a
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
%	%	kIx~	%
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
hořčík	hořčík	k1gInSc1	hořčík
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
silným	silný	k2eAgNnSc7d1	silné
redukčním	redukční	k2eAgNnSc7d1	redukční
činidlem	činidlo	k1gNnSc7	činidlo
a	a	k8xC	a
jemně	jemně	k6eAd1	jemně
rozptýlený	rozptýlený	k2eAgInSc1d1	rozptýlený
kov	kov	k1gInSc1	kov
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
redukcím	redukce	k1gFnPc3	redukce
v	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
syntéze	syntéza	k1gFnSc6	syntéza
ale	ale	k8xC	ale
i	i	k9	i
redukční	redukční	k2eAgFnSc3d1	redukční
výrobě	výroba	k1gFnSc3	výroba
jiných	jiný	k2eAgInPc2d1	jiný
kovů	kov	k1gInPc2	kov
(	(	kIx(	(
<g/>
např.	např.	kA	např.
uranu	uran	k1gInSc2	uran
<g/>
)	)	kIx)	)
z	z	k7c2	z
roztoků	roztok	k1gInPc2	roztok
jejich	jejich	k3xOp3gFnPc2	jejich
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
syntéze	syntéza	k1gFnSc6	syntéza
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
zejména	zejména	k9	zejména
velmi	velmi	k6eAd1	velmi
známá	známý	k2eAgNnPc4d1	známé
Grignardova	Grignardův	k2eAgNnPc4d1	Grignardův
činidla	činidlo	k1gNnPc4	činidlo
<g/>
,	,	kIx,	,
za	za	k7c4	za
jejichž	jejichž	k3xOyRp3gInSc4	jejichž
objev	objev	k1gInSc4	objev
získal	získat	k5eAaPmAgMnS	získat
Victor	Victor	k1gMnSc1	Victor
Grignard	Grignard	k1gMnSc1	Grignard
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
<g/>
Velká	velký	k2eAgFnSc1d1	velká
reaktivita	reaktivita	k1gFnSc1	reaktivita
kovového	kovový	k2eAgInSc2d1	kovový
hořčíku	hořčík	k1gInSc2	hořčík
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
v	v	k7c6	v
metalurgii	metalurgie	k1gFnSc6	metalurgie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gInPc4	jeho
přídavky	přídavek	k1gInPc4	přídavek
odstraňují	odstraňovat	k5eAaImIp3nP	odstraňovat
malá	malý	k2eAgNnPc1d1	malé
množství	množství	k1gNnPc1	množství
síry	síra	k1gFnSc2	síra
nebo	nebo	k8xC	nebo
kyslíku	kyslík	k1gInSc2	kyslík
z	z	k7c2	z
roztavené	roztavený	k2eAgFnSc2d1	roztavená
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
směsí	směs	k1gFnSc7	směs
s	s	k7c7	s
CaO	CaO	k1gMnPc7	CaO
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
k	k	k7c3	k
odsířování	odsířování	k1gNnSc3	odsířování
surového	surový	k2eAgNnSc2d1	surové
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
deoxidaci	deoxidace	k1gFnSc4	deoxidace
neželezných	železný	k2eNgInPc2d1	neželezný
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
<g/>
Reakce	reakce	k1gFnSc1	reakce
hořčíku	hořčík	k1gInSc2	hořčík
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
probíhá	probíhat	k5eAaImIp3nS	probíhat
za	za	k7c2	za
vývoje	vývoj	k1gInSc2	vývoj
značného	značný	k2eAgNnSc2d1	značné
množství	množství	k1gNnSc2	množství
tepla	teplo	k1gNnSc2	teplo
(	(	kIx(	(
<g/>
lze	lze	k6eAd1	lze
tak	tak	k6eAd1	tak
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
teplot	teplota	k1gFnPc2	teplota
kolem	kolem	k7c2	kolem
2	[number]	k4	2
200	[number]	k4	200
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
intenzivního	intenzivní	k2eAgNnSc2d1	intenzivní
světelného	světelný	k2eAgNnSc2d1	světelné
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Zapálit	zapálit	k5eAaPmF	zapálit
hořčík	hořčík	k1gInSc4	hořčík
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tenké	tenký	k2eAgFnSc2d1	tenká
folie	folie	k1gFnSc2	folie
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
snadné	snadný	k2eAgNnSc1d1	snadné
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
startérem	startér	k1gInSc7	startér
hoření	hoření	k1gNnSc2	hoření
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
pyrotechnických	pyrotechnický	k2eAgFnPc6d1	pyrotechnická
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInSc1d3	nejznámější
příklad	příklad	k1gInSc1	příklad
pyrotechnické	pyrotechnický	k2eAgFnSc2d1	pyrotechnická
aplikace	aplikace	k1gFnSc2	aplikace
je	být	k5eAaImIp3nS	být
bengálský	bengálský	k2eAgInSc1d1	bengálský
oheň	oheň	k1gInSc1	oheň
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
i	i	k9	i
dnes	dnes	k6eAd1	dnes
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
různých	různý	k2eAgFnPc6d1	různá
oslavách	oslava	k1gFnPc6	oslava
<g/>
.	.	kIx.	.
</s>
<s>
Hořčík	hořčík	k1gInSc1	hořčík
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
zesílení	zesílení	k1gNnSc3	zesílení
intenzity	intenzita	k1gFnSc2	intenzita
a	a	k8xC	a
zvětšení	zvětšení	k1gNnSc2	zvětšení
plamene	plamen	k1gInSc2	plamen
<g/>
.	.	kIx.	.
</s>
<s>
Popřípadě	popřípadě	k6eAd1	popřípadě
lze	lze	k6eAd1	lze
hořčík	hořčík	k1gInSc4	hořčík
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
pilin	pilina	k1gFnPc2	pilina
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
jiskřivého	jiskřivý	k2eAgInSc2d1	jiskřivý
efektu	efekt	k1gInSc2	efekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
prskavku	prskavka	k1gFnSc4	prskavka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
směsi	směs	k1gFnSc2	směs
práškového	práškový	k2eAgInSc2d1	práškový
hořčíku	hořčík	k1gInSc2	hořčík
s	s	k7c7	s
okysličovadlem	okysličovadlo	k1gNnSc7	okysličovadlo
(	(	kIx(	(
<g/>
KClO	KClO	k1gFnSc2	KClO
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Ba	ba	k9	ba
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
KClO	KClO	k1gFnSc1	KClO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
po	po	k7c6	po
zapálení	zapálení	k1gNnSc6	zapálení
používaly	používat	k5eAaImAgInP	používat
namísto	namísto	k7c2	namísto
dnešních	dnešní	k2eAgInPc2d1	dnešní
fotografických	fotografický	k2eAgInPc2d1	fotografický
blesků	blesk	k1gInPc2	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
množství	množství	k1gNnSc6	množství
možné	možný	k2eAgNnSc1d1	možné
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
téměř	téměř	k6eAd1	téměř
neomezeného	omezený	k2eNgInSc2d1	neomezený
světelného	světelný	k2eAgInSc2d1	světelný
výkonu	výkon	k1gInSc2	výkon
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc4	tento
tzv.	tzv.	kA	tzv.
osvětlovací	osvětlovací	k2eAgFnPc4d1	osvětlovací
slože	slož	k1gFnPc4	slož
používají	používat	k5eAaImIp3nP	používat
ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
při	při	k7c6	při
fotografování	fotografování	k1gNnSc6	fotografování
obrovských	obrovský	k2eAgFnPc2d1	obrovská
podzemních	podzemní	k2eAgFnPc2d1	podzemní
prostor	prostora	k1gFnPc2	prostora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Standardní	standardní	k2eAgNnSc1d1	standardní
a	a	k8xC	a
lehce	lehko	k6eAd1	lehko
připravitelné	připravitelný	k2eAgFnPc1d1	připravitelná
osvětlovací	osvětlovací	k2eAgFnPc1d1	osvětlovací
slože	slož	k1gFnPc1	slož
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
65	[number]	k4	65
%	%	kIx~	%
KNO	KNO	kA	KNO
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
26	[number]	k4	26
%	%	kIx~	%
Mg	mg	kA	mg
<g/>
,	,	kIx,	,
9	[number]	k4	9
%	%	kIx~	%
dřevěné	dřevěný	k2eAgNnSc1d1	dřevěné
uhlí	uhlí	k1gNnSc1	uhlí
</s>
</p>
<p>
<s>
66	[number]	k4	66
%	%	kIx~	%
KClO	KClO	k1gFnPc2	KClO
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
34	[number]	k4	34
%	%	kIx~	%
Mg	mg	kA	mg
</s>
</p>
<p>
<s>
50	[number]	k4	50
%	%	kIx~	%
KClO	KClO	k1gFnPc2	KClO
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
50	[number]	k4	50
%	%	kIx~	%
Mg	mg	kA	mg
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Al	ala	k1gFnPc2	ala
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bezpečnostní	bezpečnostní	k2eAgNnSc1d1	bezpečnostní
upozornění	upozornění	k1gNnSc1	upozornění
<g/>
:	:	kIx,	:
Při	při	k7c6	při
hoření	hoření	k1gNnSc6	hoření
hořčíku	hořčík	k1gInSc2	hořčík
vzniká	vznikat	k5eAaImIp3nS	vznikat
i	i	k9	i
určité	určitý	k2eAgNnSc1d1	určité
množství	množství	k1gNnSc1	množství
UV	UV	kA	UV
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ochranných	ochranný	k2eAgFnPc2d1	ochranná
brýlí	brýle	k1gFnPc2	brýle
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
se	se	k3xPyFc4	se
dívat	dívat	k5eAaImF	dívat
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
plamene	plamen	k1gInSc2	plamen
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
směsí	směs	k1gFnPc2	směs
velmi	velmi	k6eAd1	velmi
jemně	jemně	k6eAd1	jemně
práškovaného	práškovaný	k2eAgInSc2d1	práškovaný
hořčíku	hořčík	k1gInSc2	hořčík
(	(	kIx(	(
<g/>
pudr	pudr	k1gInSc1	pudr
<g/>
)	)	kIx)	)
s	s	k7c7	s
okysličovadly	okysličovadlo	k1gNnPc7	okysličovadlo
je	být	k5eAaImIp3nS	být
hoření	hoření	k2eAgInSc1d1	hoření
tak	tak	k8xS	tak
rychlé	rychlý	k2eAgNnSc1d1	rychlé
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
přejít	přejít	k5eAaPmF	přejít
ve	v	k7c4	v
výbuch	výbuch	k1gInSc4	výbuch
<g/>
.	.	kIx.	.
</s>
<s>
Rizikem	riziko	k1gNnSc7	riziko
je	být	k5eAaImIp3nS	být
i	i	k9	i
nízká	nízký	k2eAgFnSc1d1	nízká
teplota	teplota	k1gFnSc1	teplota
vzplanutí	vzplanutí	k1gNnSc2	vzplanutí
(	(	kIx(	(
<g/>
250	[number]	k4	250
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
samozřejmě	samozřejmě	k6eAd1	samozřejmě
význam	význam	k1gInSc4	význam
v	v	k7c6	v
pyrotechnických	pyrotechnický	k2eAgFnPc6d1	pyrotechnická
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
osvětlovací	osvětlovací	k2eAgInPc4d1	osvětlovací
účely	účel	k1gInPc4	účel
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
použít	použít	k5eAaPmF	použít
hořčíkovou	hořčíkový	k2eAgFnSc4d1	hořčíková
krupici	krupice	k1gFnSc4	krupice
či	či	k8xC	či
šupinky	šupinka	k1gFnPc4	šupinka
<g/>
.	.	kIx.	.
<g/>
Oxid	oxid	k1gInSc1	oxid
hořečnatý	hořečnatý	k2eAgInSc1d1	hořečnatý
má	mít	k5eAaImIp3nS	mít
patrně	patrně	k6eAd1	patrně
největší	veliký	k2eAgNnSc4d3	veliký
praktické	praktický	k2eAgNnSc4d1	praktické
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
uplatnění	uplatnění	k1gNnSc4	uplatnění
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
sloučenin	sloučenina	k1gFnPc2	sloučenina
hořčíku	hořčík	k1gInSc2	hořčík
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sloučenina	sloučenina	k1gFnSc1	sloučenina
má	mít	k5eAaImIp3nS	mít
mimořádně	mimořádně	k6eAd1	mimořádně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
odolnost	odolnost	k1gFnSc4	odolnost
vůči	vůči	k7c3	vůči
vysokým	vysoký	k2eAgFnPc3d1	vysoká
teplotám	teplota	k1gFnPc3	teplota
a	a	k8xC	a
současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
levně	levně	k6eAd1	levně
dostupná	dostupný	k2eAgFnSc1d1	dostupná
ve	v	k7c6	v
stotunových	stotunový	k2eAgNnPc6d1	stotunové
množstvích	množství	k1gNnPc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
ideálním	ideální	k2eAgInSc7d1	ideální
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
vyložení	vyložení	k1gNnSc4	vyložení
vysokých	vysoký	k2eAgFnPc2d1	vysoká
pecí	pec	k1gFnPc2	pec
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
podobné	podobný	k2eAgFnSc2d1	podobná
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
musí	muset	k5eAaImIp3nS	muset
speciálně	speciálně	k6eAd1	speciálně
čistit	čistit	k5eAaImF	čistit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
i	i	k9	i
poměrně	poměrně	k6eAd1	poměrně
malá	malý	k2eAgNnPc4d1	malé
množství	množství	k1gNnPc4	množství
příbuzných	příbuzný	k1gMnPc2	příbuzný
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
vápník	vápník	k1gInSc1	vápník
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
dramaticky	dramaticky	k6eAd1	dramaticky
zhoršovat	zhoršovat	k5eAaImF	zhoršovat
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
stabilitu	stabilita	k1gFnSc4	stabilita
magnezitových	magnezitový	k2eAgFnPc2d1	magnezitový
vysokopecních	vysokopecní	k2eAgFnPc2d1	vysokopecní
vyzdívek	vyzdívka	k1gFnPc2	vyzdívka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hydroxid	hydroxid	k1gInSc4	hydroxid
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
Mg	mg	kA	mg
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
proti	proti	k7c3	proti
překyselení	překyselení	k1gNnSc3	překyselení
žaludku	žaludek	k1gInSc2	žaludek
a	a	k8xC	a
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
gelů	gel	k1gInPc2	gel
na	na	k7c4	na
spáleniny	spálenina	k1gFnPc4	spálenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uhličitan	uhličitan	k1gInSc4	uhličitan
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
do	do	k7c2	do
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
hnojiv	hnojivo	k1gNnPc2	hnojivo
jako	jako	k8xC	jako
složka	složka	k1gFnSc1	složka
dodávající	dodávající	k2eAgFnSc1d1	dodávající
rostlinám	rostlina	k1gFnPc3	rostlina
potřebný	potřebný	k2eAgInSc4d1	potřebný
hořčík	hořčík	k1gInSc4	hořčík
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
a	a	k8xC	a
zdárný	zdárný	k2eAgInSc4d1	zdárný
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Spíše	spíše	k9	spíše
kuriozitou	kuriozita	k1gFnSc7	kuriozita
je	být	k5eAaImIp3nS	být
sportovní	sportovní	k2eAgNnSc4d1	sportovní
uplatnění	uplatnění	k1gNnSc4	uplatnění
–	–	k?	–
gymnasté	gymnast	k1gMnPc1	gymnast
<g/>
,	,	kIx,	,
vzpěrači	vzpěrač	k1gMnPc1	vzpěrač
<g/>
,	,	kIx,	,
atleti	atlet	k1gMnPc1	atlet
a	a	k8xC	a
horolezci	horolezec	k1gMnPc1	horolezec
si	se	k3xPyFc3	se
jemným	jemný	k2eAgInSc7d1	jemný
práškem	prášek	k1gInSc7	prášek
uhličitanu	uhličitan	k1gInSc2	uhličitan
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
ruce	ruka	k1gFnPc4	ruka
pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
uchopení	uchopení	k1gNnSc4	uchopení
některých	některý	k3yIgNnPc2	některý
nářadí	nářadí	k1gNnPc2	nářadí
<g/>
.	.	kIx.	.
<g/>
Síran	síran	k1gInSc1	síran
hořečnatý	hořečnatý	k2eAgInSc1d1	hořečnatý
(	(	kIx(	(
<g/>
MgSO	MgSO	k1gFnSc1	MgSO
<g/>
4.7	[number]	k4	4.7
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
také	také	k9	také
znám	znám	k2eAgMnSc1d1	znám
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
epsomská	epsomský	k2eAgFnSc1d1	epsomská
sůl	sůl	k1gFnSc1	sůl
(	(	kIx(	(
<g/>
nesprávně	správně	k6eNd1	správně
Epsomova	Epsomův	k2eAgFnSc1d1	Epsomův
sůl	sůl	k1gFnSc1	sůl
<g/>
)	)	kIx)	)
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
jako	jako	k8xS	jako
vynikající	vynikající	k2eAgNnSc1d1	vynikající
laxativum	laxativum	k1gNnSc1	laxativum
–	–	k?	–
projímadlo	projímadlo	k1gNnSc1	projímadlo
<g/>
,	,	kIx,	,
zdroj	zdroj	k1gInSc1	zdroj
hořčíku	hořčík	k1gInSc2	hořčík
nebo	nebo	k8xC	nebo
přídavek	přídavka	k1gFnPc2	přídavka
do	do	k7c2	do
léčebných	léčebný	k2eAgFnPc2d1	léčebná
koupelí	koupel	k1gFnPc2	koupel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
ve	v	k7c6	v
vřídelní	vřídelní	k2eAgFnSc6d1	vřídelní
soli	sůl	k1gFnSc6	sůl
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
anglického	anglický	k2eAgNnSc2d1	anglické
města	město	k1gNnSc2	město
Epsomu	Epsom	k1gInSc2	Epsom
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
minerálních	minerální	k2eAgFnPc6d1	minerální
vodách	voda	k1gFnPc6	voda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Zaječická	zaječický	k2eAgNnPc1d1	zaječický
nebo	nebo	k8xC	nebo
Šaratica	Šaratic	k2eAgNnPc1d1	Šaratic
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc1	síran
hořečnatý	hořečnatý	k2eAgMnSc1d1	hořečnatý
můžeme	moct	k5eAaImIp1nP	moct
v	v	k7c6	v
malém	malé	k1gNnSc6	malé
množství	množství	k1gNnSc2	množství
přidat	přidat	k5eAaPmF	přidat
pro	pro	k7c4	pro
zlepšení	zlepšení	k1gNnSc4	zlepšení
chuťových	chuťový	k2eAgFnPc2d1	chuťová
a	a	k8xC	a
biologických	biologický	k2eAgFnPc2d1	biologická
vlastností	vlastnost	k1gFnPc2	vlastnost
do	do	k7c2	do
kuchyňské	kuchyňský	k2eAgFnSc2d1	kuchyňská
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc4d1	podobná
medicínské	medicínský	k2eAgFnPc4d1	medicínská
vlastnosti	vlastnost	k1gFnPc4	vlastnost
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
i	i	k9	i
suspenze	suspenze	k1gFnSc1	suspenze
hydroxidu	hydroxid	k1gInSc2	hydroxid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
hořečnaté	hořečnatý	k2eAgNnSc1d1	hořečnaté
mléko	mléko	k1gNnSc1	mléko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
se	se	k3xPyFc4	se
při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
nad	nad	k7c7	nad
kahanem	kahan	k1gInSc7	kahan
používá	používat	k5eAaImIp3nS	používat
azbestová	azbestový	k2eAgFnSc1d1	azbestová
síťka	síťka	k1gFnSc1	síťka
–	–	k?	–
azbest	azbest	k1gInSc1	azbest
je	být	k5eAaImIp3nS	být
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
křemičitan	křemičitan	k1gInSc4	křemičitan
o	o	k7c6	o
složení	složení	k1gNnSc6	složení
Mg	mg	kA	mg
<g/>
3	[number]	k4	3
<g/>
Si	se	k3xPyFc3	se
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
4	[number]	k4	4
<g/>
O	o	k7c4	o
<g/>
9	[number]	k4	9
nebo	nebo	k8xC	nebo
Mg	mg	kA	mg
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
Si	se	k3xPyFc3	se
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
různých	různý	k2eAgInPc2d1	různý
užitkových	užitkový	k2eAgInPc2d1	užitkový
předmětů	předmět	k1gInPc2	předmět
např.	např.	kA	např.
nástavců	nástavec	k1gInPc2	nástavec
plynových	plynový	k2eAgInPc2d1	plynový
hořáků	hořák	k1gInPc2	hořák
<g/>
,	,	kIx,	,
krejčovské	krejčovský	k2eAgFnSc2d1	krejčovská
křídy	křída	k1gFnSc2	křída
<g/>
,	,	kIx,	,
výrobě	výroba	k1gFnSc3	výroba
mastí	mast	k1gFnPc2	mast
a	a	k8xC	a
líčidel	líčidlo	k1gNnPc2	líčidlo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
mastku	mastek	k1gInSc2	mastek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
hořečnatých	hořečnatý	k2eAgInPc2d1	hořečnatý
minerálů	minerál	k1gInPc2	minerál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xC	jako
mořská	mořský	k2eAgFnSc1d1	mořská
pěna	pěna	k1gFnSc1	pěna
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
meerschaum	meerschaum	k1gInSc1	meerschaum
<g/>
"	"	kIx"	"
neboli	neboli	k8xC	neboli
sepiolit	sepiolit	k1gInSc4	sepiolit
(	(	kIx(	(
<g/>
hydratovaný	hydratovaný	k2eAgInSc4d1	hydratovaný
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
křemičitan	křemičitan	k1gInSc4	křemičitan
o	o	k7c6	o
složení	složení	k1gNnSc6	složení
Mg	mg	kA	mg
<g/>
3	[number]	k4	3
<g/>
[	[	kIx(	[
<g/>
Si	se	k3xPyFc3	se
<g/>
4	[number]	k4	4
<g/>
O	o	k7c4	o
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Mg	mg	kA	mg
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
3	[number]	k4	3
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
kuřáckých	kuřácký	k2eAgFnPc2d1	kuřácká
potřeb	potřeba	k1gFnPc2	potřeba
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
dýmek	dýmka	k1gFnPc2	dýmka
"	"	kIx"	"
<g/>
pěnovek	pěnovka	k1gFnPc2	pěnovka
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
meršánek	meršánek	k?	meršánek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Anorganické	anorganický	k2eAgFnPc1d1	anorganická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
===	===	k?	===
</s>
</p>
<p>
<s>
Hydrid	hydrid	k1gInSc4	hydrid
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
MgH	MgH	k1gFnSc7	MgH
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
pevná	pevný	k2eAgFnSc1d1	pevná
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
trochu	trochu	k6eAd1	trochu
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
etheru	ether	k1gInSc6	ether
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vodou	voda	k1gFnSc7	voda
reaguje	reagovat	k5eAaBmIp3nS	reagovat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
hydroxidu	hydroxid	k1gInSc2	hydroxid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
slučováním	slučování	k1gNnSc7	slučování
prvků	prvek	k1gInPc2	prvek
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
jodidu	jodid	k1gInSc2	jodid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
nebo	nebo	k8xC	nebo
tepelným	tepelný	k2eAgInSc7d1	tepelný
rozkladem	rozklad	k1gInSc7	rozklad
diethylmagnesia	diethylmagnesium	k1gNnSc2	diethylmagnesium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oxid	oxid	k1gInSc1	oxid
hořečnatý	hořečnatý	k2eAgInSc1d1	hořečnatý
MgO	MgO	k1gFnSc4	MgO
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
bílý	bílý	k1gMnSc1	bílý
<g/>
,	,	kIx,	,
jemný	jemný	k2eAgMnSc1d1	jemný
<g/>
,	,	kIx,	,
drsný	drsný	k2eAgInSc1d1	drsný
prášek	prášek	k1gInSc1	prášek
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
teplotou	teplota	k1gFnSc7	teplota
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normální	normální	k2eAgFnSc2d1	normální
teploty	teplota	k1gFnSc2	teplota
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
oxid	oxid	k1gInSc4	oxid
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
polymerní	polymerní	k2eAgFnSc4d1	polymerní
molekulu	molekula	k1gFnSc4	molekula
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
monomeru	monomer	k1gInSc2	monomer
je	být	k5eAaImIp3nS	být
oxid	oxid	k1gInSc1	oxid
hořečnatý	hořečnatý	k2eAgInSc1d1	hořečnatý
červený	červený	k2eAgInSc1d1	červený
prášek	prášek	k1gInSc1	prášek
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc4	oxid
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
spalováním	spalování	k1gNnSc7	spalování
hořčíku	hořčík	k1gInSc2	hořčík
v	v	k7c6	v
kyslíkové	kyslíkový	k2eAgFnSc6d1	kyslíková
atmosféře	atmosféra	k1gFnSc6	atmosféra
nebo	nebo	k8xC	nebo
tepelným	tepelný	k2eAgInSc7d1	tepelný
rozkladem	rozklad	k1gInSc7	rozklad
hydroxidu	hydroxid	k1gInSc2	hydroxid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hydroxid	hydroxid	k1gInSc4	hydroxid
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
Mg	mg	kA	mg
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
v	v	k7c6	v
kyselinách	kyselina	k1gFnPc6	kyselina
vznikají	vznikat	k5eAaImIp3nP	vznikat
hořečnaté	hořečnatý	k2eAgFnPc1d1	hořečnatá
soli	sůl	k1gFnPc1	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
hořčíku	hořčík	k1gInSc2	hořčík
s	s	k7c7	s
vodní	vodní	k2eAgFnSc7d1	vodní
parou	para	k1gFnSc7	para
<g/>
,	,	kIx,	,
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
oxidu	oxid	k1gInSc2	oxid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nebo	nebo	k8xC	nebo
reakcí	reakce	k1gFnSc7	reakce
hořečnatých	hořečnatý	k2eAgFnPc2d1	hořečnatá
solí	sůl	k1gFnPc2	sůl
s	s	k7c7	s
roztokem	roztok	k1gInSc7	roztok
alkalického	alkalický	k2eAgInSc2d1	alkalický
hydroxidu	hydroxid	k1gInSc2	hydroxid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Soli	sůl	k1gFnSc6	sůl
====	====	k?	====
</s>
</p>
<p>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
hořečnatých	hořečnatý	k2eAgFnPc2d1	hořečnatá
solí	sůl	k1gFnPc2	sůl
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
rozpuští	rozpuštit	k5eAaPmIp3nS	rozpuštit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
hůře	zle	k6eAd2	zle
nebo	nebo	k8xC	nebo
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
soli	sůl	k1gFnPc1	sůl
mají	mít	k5eAaImIp3nP	mít
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
bezbarvé	bezbarvý	k2eAgFnPc1d1	bezbarvá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
anion	anion	k1gInSc1	anion
soli	sůl	k1gFnSc2	sůl
barevný	barevný	k2eAgMnSc1d1	barevný
(	(	kIx(	(
<g/>
manganistany	manganistan	k1gInPc1	manganistan
<g/>
,	,	kIx,	,
chromany	chroman	k1gInPc1	chroman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hořečnaté	hořečnatý	k2eAgFnPc4d1	hořečnatá
soli	sůl	k1gFnPc4	sůl
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
snadno	snadno	k6eAd1	snadno
podvojné	podvojný	k2eAgFnPc4d1	podvojná
soli	sůl	k1gFnPc4	sůl
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
i	i	k9	i
komplexy	komplex	k1gInPc1	komplex
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ale	ale	k9	ale
nejsou	být	k5eNaImIp3nP	být
pro	pro	k7c4	pro
hořčík	hořčík	k1gInSc4	hořčík
a	a	k8xC	a
i	i	k9	i
další	další	k2eAgInPc1d1	další
kovy	kov	k1gInPc1	kov
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
typické	typický	k2eAgFnPc1d1	typická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fluorid	fluorid	k1gInSc4	fluorid
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
MgF	MgF	k1gFnSc7	MgF
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
fluoridy	fluorid	k1gInPc7	fluorid
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
vytváří	vytvářet	k5eAaImIp3nP	vytvářet
podvojné	podvojný	k2eAgFnPc1d1	podvojná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
srážením	srážení	k1gNnSc7	srážení
hořečnatých	hořečnatý	k2eAgInPc2d1	hořečnatý
iontů	ion	k1gInPc2	ion
ionty	ion	k1gInPc4	ion
fluoridovými	fluoridový	k2eAgFnPc7d1	fluoridová
nebo	nebo	k8xC	nebo
reakcí	reakce	k1gFnSc7	reakce
hydroxidu	hydroxid	k1gInSc2	hydroxid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
či	či	k8xC	či
uhličitanu	uhličitan	k1gInSc2	uhličitan
hořečnatého	hořečnatý	k2eAgMnSc4d1	hořečnatý
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
fluorovodíkovou	fluorovodíkový	k2eAgFnSc7d1	fluorovodíková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
MgCl	MgCl	k1gInSc4	MgCl
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgNnSc1d1	dobré
projímadlo	projímadlo	k1gNnSc1	projímadlo
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
chloridy	chlorid	k1gInPc7	chlorid
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
podvojné	podvojný	k2eAgFnSc2d1	podvojná
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
chlorovodíku	chlorovodík	k1gInSc2	chlorovodík
s	s	k7c7	s
hydroxidem	hydroxid	k1gInSc7	hydroxid
hořečnatým	hořečnatý	k2eAgInSc7d1	hořečnatý
nebo	nebo	k8xC	nebo
uhličitanem	uhličitan	k1gInSc7	uhličitan
hořečnatým	hořečnatý	k2eAgInSc7d1	hořečnatý
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
reagovat	reagovat	k5eAaBmF	reagovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
dichloridoxidu	dichloridoxid	k1gInSc2	dichloridoxid
dihořečnatého	dihořečnatý	k2eAgInSc2d1	dihořečnatý
a	a	k8xC	a
kyseliny	kyselina	k1gFnSc2	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bromid	bromid	k1gInSc1	bromid
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
MgBr	MgBr	k1gInSc4	MgBr
<g/>
2	[number]	k4	2
a	a	k8xC	a
jodid	jodid	k1gInSc4	jodid
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
MgI	MgI	k1gFnSc7	MgI
<g/>
2	[number]	k4	2
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgFnPc1d1	bílá
krystalické	krystalický	k2eAgFnPc1d1	krystalická
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgNnSc1d1	rozpustné
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jodid	jodid	k1gInSc4	jodid
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
syfilis	syfilis	k1gFnSc1	syfilis
<g/>
,	,	kIx,	,
skrofulógy	skrofulóga	k1gFnSc2	skrofulóga
a	a	k8xC	a
revmatismu	revmatismus	k1gInSc2	revmatismus
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
látky	látka	k1gFnPc1	látka
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
hydroxidu	hydroxid	k1gInSc2	hydroxid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
nebo	nebo	k8xC	nebo
uhličitanu	uhličitan	k1gInSc2	uhličitan
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
v	v	k7c6	v
příslušných	příslušný	k2eAgFnPc6d1	příslušná
kyselinách	kyselina	k1gFnPc6	kyselina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dusičnan	dusičnan	k1gInSc4	dusičnan
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
Mg	mg	kA	mg
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
rozplývavá	rozplývavý	k2eAgFnSc1d1	rozplývavá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
hydroxidu	hydroxid	k1gInSc2	hydroxid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
nebo	nebo	k8xC	nebo
uhličitanu	uhličitan	k1gInSc2	uhličitan
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
dusičné	dusičný	k2eAgFnSc6d1	dusičná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uhličitan	uhličitan	k1gInSc4	uhličitan
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
MgCO	MgCO	k1gFnSc7	MgCO
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
dalších	další	k2eAgFnPc2d1	další
hořečnatých	hořečnatý	k2eAgFnPc2d1	hořečnatá
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
hořečnatých	hořečnatý	k2eAgMnPc2d1	hořečnatý
kationů	kation	k1gInPc2	kation
s	s	k7c7	s
roztokem	roztok	k1gInSc7	roztok
rozpustného	rozpustný	k2eAgInSc2d1	rozpustný
uhličitanu	uhličitan	k1gInSc2	uhličitan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Síran	síran	k1gInSc4	síran
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
MgSO	MgSO	k1gFnSc7	MgSO
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
uhličitanu	uhličitan	k1gInSc2	uhličitan
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
<g/>
,	,	kIx,	,
hydroxidu	hydroxid	k1gInSc2	hydroxid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
nebo	nebo	k8xC	nebo
hořčíku	hořčík	k1gInSc2	hořčík
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
organické	organický	k2eAgFnPc4d1	organická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
hořčíku	hořčík	k1gInSc2	hořčík
patří	patřit	k5eAaImIp3nP	patřit
zejména	zejména	k9	zejména
hořečnaté	hořečnatý	k2eAgFnPc1d1	hořečnatá
soli	sůl	k1gFnPc1	sůl
organických	organický	k2eAgFnPc2d1	organická
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
hořečnaté	hořečnatý	k2eAgInPc4d1	hořečnatý
alkoholáty	alkoholát	k1gInPc4	alkoholát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
hořečnatým	hořečnatý	k2eAgFnPc3d1	hořečnatá
sloučeninám	sloučenina	k1gFnPc3	sloučenina
patří	patřit	k5eAaImIp3nS	patřit
organické	organický	k2eAgInPc4d1	organický
komplexy	komplex	k1gInPc4	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
skupinu	skupina	k1gFnSc4	skupina
organických	organický	k2eAgFnPc2d1	organická
hořečnatých	hořečnatý	k2eAgFnPc2d1	hořečnatá
sloučenin	sloučenina	k1gFnPc2	sloučenina
tvoří	tvořit	k5eAaImIp3nP	tvořit
organokovové	organokovový	k2eAgFnPc1d1	organokovová
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgInPc4	který
patří	patřit	k5eAaImIp3nP	patřit
velmi	velmi	k6eAd1	velmi
známé	známý	k2eAgFnPc1d1	známá
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
používané	používaný	k2eAgNnSc4d1	používané
Grignardovo	Grignardův	k2eAgNnSc4d1	Grignardův
činidlo	činidlo	k1gNnSc4	činidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biologický	biologický	k2eAgInSc4d1	biologický
význam	význam	k1gInSc4	význam
hořčíku	hořčík	k1gInSc2	hořčík
==	==	k?	==
</s>
</p>
<p>
<s>
Dostatek	dostatek	k1gInSc1	dostatek
hořčíku	hořčík	k1gInSc2	hořčík
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
pro	pro	k7c4	pro
správnou	správný	k2eAgFnSc4d1	správná
činnost	činnost	k1gFnSc4	činnost
svalů	sval	k1gInPc2	sval
a	a	k8xC	a
nervů	nerv	k1gInPc2	nerv
(	(	kIx(	(
<g/>
mírní	mírnit	k5eAaImIp3nS	mírnit
podrážděnost	podrážděnost	k1gFnSc4	podrážděnost
a	a	k8xC	a
nervozitu	nervozita	k1gFnSc4	nervozita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
uvolňování	uvolňování	k1gNnSc4	uvolňování
energie	energie	k1gFnSc2	energie
z	z	k7c2	z
glukózy	glukóza	k1gFnSc2	glukóza
a	a	k8xC	a
pro	pro	k7c4	pro
správnou	správný	k2eAgFnSc4d1	správná
stavbu	stavba	k1gFnSc4	stavba
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Udržuje	udržovat	k5eAaImIp3nS	udržovat
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
oběhový	oběhový	k2eAgInSc4d1	oběhový
systém	systém	k1gInSc4	systém
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
prevencí	prevence	k1gFnSc7	prevence
infarktu	infarkt	k1gInSc2	infarkt
<g/>
.	.	kIx.	.
</s>
<s>
Řadě	řada	k1gFnSc3	řada
žen	žena	k1gFnPc2	žena
odstraní	odstranit	k5eAaPmIp3nS	odstranit
potíže	potíž	k1gFnPc4	potíž
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
premenstruálním	premenstruální	k2eAgInSc7d1	premenstruální
syndromem	syndrom	k1gInSc7	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
Mírní	mírnit	k5eAaImIp3nP	mírnit
deprese	deprese	k1gFnPc4	deprese
a	a	k8xC	a
přispívá	přispívat	k5eAaImIp3nS	přispívat
ke	k	k7c3	k
zdravým	zdravý	k2eAgInPc3d1	zdravý
zubům	zub	k1gInPc3	zub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
vápníkem	vápník	k1gInSc7	vápník
působí	působit	k5eAaImIp3nP	působit
hořčík	hořčík	k1gInSc4	hořčík
jako	jako	k8xS	jako
přirozený	přirozený	k2eAgInSc4d1	přirozený
uklidňující	uklidňující	k2eAgInSc4d1	uklidňující
prostředek	prostředek	k1gInSc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
nedostatek	nedostatek	k1gInSc4	nedostatek
často	často	k6eAd1	často
pociťují	pociťovat	k5eAaImIp3nP	pociťovat
sportovci	sportovec	k1gMnPc1	sportovec
<g/>
,	,	kIx,	,
diabetici	diabetik	k1gMnPc1	diabetik
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pijí	pít	k5eAaImIp3nP	pít
příliš	příliš	k6eAd1	příliš
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
podrážděností	podrážděnost	k1gFnSc7	podrážděnost
<g/>
,	,	kIx,	,
nespavostí	nespavost	k1gFnSc7	nespavost
<g/>
,	,	kIx,	,
náladovostí	náladovost	k1gFnSc7	náladovost
<g/>
,	,	kIx,	,
špatným	špatný	k2eAgNnSc7d1	špatné
trávením	trávení	k1gNnSc7	trávení
<g/>
,	,	kIx,	,
bušením	bušení	k1gNnSc7	bušení
srdce	srdce	k1gNnSc2	srdce
nebo	nebo	k8xC	nebo
arytmiemi	arytmie	k1gFnPc7	arytmie
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
hořčíku	hořčík	k1gInSc2	hořčík
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
deprese	deprese	k1gFnPc4	deprese
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
záchvat	záchvat	k1gInSc1	záchvat
astmatu	astma	k1gNnSc2	astma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přirozenými	přirozený	k2eAgInPc7d1	přirozený
zdroji	zdroj	k1gInPc7	zdroj
hořčíku	hořčík	k1gInSc2	hořčík
jsou	být	k5eAaImIp3nP	být
banány	banán	k1gInPc1	banán
<g/>
,	,	kIx,	,
mandle	mandle	k1gFnPc1	mandle
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc1	ořech
<g/>
,	,	kIx,	,
tmavá	tmavý	k2eAgFnSc1d1	tmavá
listová	listový	k2eAgFnSc1d1	listová
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
,	,	kIx,	,
obilí	obilí	k1gNnSc1	obilí
<g/>
,	,	kIx,	,
celozrnné	celozrnný	k2eAgNnSc1d1	celozrnné
pečivo	pečivo	k1gNnSc1	pečivo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
čokoláda	čokoláda	k1gFnSc1	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
průměrný	průměrný	k2eAgInSc1d1	průměrný
příjem	příjem	k1gInSc1	příjem
hořčíku	hořčík	k1gInSc2	hořčík
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
činit	činit	k5eAaImF	činit
asi	asi	k9	asi
300	[number]	k4	300
mg	mg	kA	mg
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Globální	globální	k2eAgInSc1d1	globální
význam	význam	k1gInSc1	význam
hořčíku	hořčík	k1gInSc2	hořčík
je	být	k5eAaImIp3nS	být
však	však	k9	však
dán	dát	k5eAaPmNgInS	dát
jeho	jeho	k3xOp3gInSc7	jeho
výskytem	výskyt	k1gInSc7	výskyt
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
organická	organický	k2eAgFnSc1d1	organická
sloučenina	sloučenina	k1gFnSc1	sloučenina
má	mít	k5eAaImIp3nS	mít
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
schopnost	schopnost	k1gFnSc4	schopnost
přeměňovat	přeměňovat	k5eAaImF	přeměňovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
sluneční	sluneční	k2eAgFnSc4d1	sluneční
energii	energie	k1gFnSc4	energie
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
chemické	chemický	k2eAgFnSc2d1	chemická
vazby	vazba	k1gFnSc2	vazba
sacharidů	sacharid	k1gInPc2	sacharid
vytvářených	vytvářený	k2eAgInPc2d1	vytvářený
z	z	k7c2	z
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
takřka	takřka	k6eAd1	takřka
všechny	všechen	k3xTgFnPc4	všechen
další	další	k2eAgFnPc4d1	další
biochemické	biochemický	k2eAgFnPc4d1	biochemická
a	a	k8xC	a
biologické	biologický	k2eAgFnPc4d1	biologická
reakce	reakce	k1gFnPc4	reakce
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zelené	Zelené	k2eAgNnSc1d1	Zelené
zbarvení	zbarvení	k1gNnSc1	zbarvení
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
právě	právě	k9	právě
přítomností	přítomnost	k1gFnSc7	přítomnost
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nejsilněji	silně	k6eAd3	silně
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
červené	červený	k2eAgNnSc1d1	červené
a	a	k8xC	a
modré	modrý	k2eAgNnSc1d1	modré
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poranění	poranění	k1gNnPc4	poranění
způsobená	způsobený	k2eAgNnPc4d1	způsobené
kovovým	kovový	k2eAgInSc7d1	kovový
hořčíkem	hořčík	k1gInSc7	hořčík
či	či	k8xC	či
slitinami	slitina	k1gFnPc7	slitina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jej	on	k3xPp3gMnSc4	on
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
špatně	špatně	k6eAd1	špatně
hojí	hojit	k5eAaImIp3nS	hojit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Doplněk	doplněk	k1gInSc1	doplněk
stravy	strava	k1gFnSc2	strava
==	==	k?	==
</s>
</p>
<p>
<s>
Vstřebatelnost	vstřebatelnost	k1gFnSc1	vstřebatelnost
hořčíku	hořčík	k1gInSc2	hořčík
z	z	k7c2	z
doplňků	doplněk	k1gInPc2	doplněk
stravy	strava	k1gFnSc2	strava
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
citrátu	citrát	k1gInSc2	citrát
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
hořčíku	hořčík	k1gInSc2	hořčík
byla	být	k5eAaImAgFnS	být
dle	dle	k7c2	dle
měření	měření	k1gNnSc2	měření
koncetraci	koncetrace	k1gFnSc4	koncetrace
v	v	k7c6	v
moči	moč	k1gFnSc6	moč
vyšší	vysoký	k2eAgFnSc6d2	vyšší
u	u	k7c2	u
citrátu	citrát	k1gInSc2	citrát
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
díky	díky	k7c3	díky
lepší	dobrý	k2eAgFnSc3d2	lepší
rozpustnosti	rozpustnost	k1gFnSc3	rozpustnost
<g/>
.	.	kIx.	.
nicméně	nicméně	k8xC	nicméně
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
citrátem	citrát	k1gInSc7	citrát
(	(	kIx(	(
<g/>
7,2	[number]	k4	7,2
±	±	k?	±
1,48	[number]	k4	1,48
mmol	mmol	k1gInSc1	mmol
<g/>
)	)	kIx)	)
a	a	k8xC	a
oxidem	oxid	k1gInSc7	oxid
(	(	kIx(	(
<g/>
6,7	[number]	k4	6,7
±	±	k?	±
1,43	[number]	k4	1,43
mmol	mmol	k1gInSc1	mmol
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
marginální	marginální	k2eAgInSc1d1	marginální
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
0,565	[number]	k4	0,565
mmol	mmonout	k5eAaPmAgInS	mmonout
přepočtený	přepočtený	k2eAgInSc1d1	přepočtený
na	na	k7c4	na
objem	objem	k1gInSc4	objem
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
13,7	[number]	k4	13,7
mg	mg	kA	mg
a	a	k8xC	a
tedy	tedy	k9	tedy
fyziologicky	fyziologicky	k6eAd1	fyziologicky
bezvýznamný	bezvýznamný	k2eAgMnSc1d1	bezvýznamný
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
celkový	celkový	k2eAgInSc4d1	celkový
příjem	příjem	k1gInSc4	příjem
800	[number]	k4	800
mg	mg	kA	mg
během	během	k7c2	během
testovacího	testovací	k2eAgInSc2d1	testovací
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Porovnání	porovnání	k1gNnSc1	porovnání
vstřebatelnosti	vstřebatelnost	k1gFnSc2	vstřebatelnost
chelátu	chelát	k1gInSc2	chelát
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
,	,	kIx,	,
citrátu	citrát	k1gInSc2	citrát
zinku	zinek	k1gInSc2	zinek
a	a	k8xC	a
placeba	placebo	k1gNnSc2	placebo
nezjistilo	zjistit	k5eNaPmAgNnS	zjistit
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
oxidem	oxid	k1gInSc7	oxid
<g/>
,	,	kIx,	,
chelátem	chelát	k1gInSc7	chelát
a	a	k8xC	a
placebem	placebo	k1gNnSc7	placebo
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
citrátu	citrát	k1gInSc2	citrát
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
koncetrace	koncetrace	k1gFnSc1	koncetrace
v	v	k7c6	v
plazmě	plazma	k1gFnSc6	plazma
(	(	kIx(	(
<g/>
akutní	akutní	k2eAgFnSc1d1	akutní
i	i	k8xC	i
chronická	chronický	k2eAgFnSc1d1	chronická
<g/>
)	)	kIx)	)
o	o	k7c4	o
méně	málo	k6eAd2	málo
než	než	k8xS	než
10	[number]	k4	10
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Jursík	Jursík	k1gMnSc1	Jursík
F.	F.	kA	F.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
nekovů	nekov	k1gInPc2	nekov
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7080-504-8	[number]	k4	80-7080-504-8
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwood	k1gInSc1	Greenwood
–	–	k?	–
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc2	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hořčík	hořčík	k1gInSc1	hořčík
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hořčík	hořčík	k1gInSc1	hořčík
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Chemický	chemický	k2eAgInSc1d1	chemický
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
portál	portál	k1gInSc1	portál
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Chemistry	Chemistr	k1gMnPc4	Chemistr
in	in	k?	in
its	its	k?	its
element	element	k1gInSc1	element
podcast	podcast	k1gFnSc1	podcast
(	(	kIx(	(
<g/>
MP	MP	kA	MP
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
from	from	k6eAd1	from
the	the	k?	the
Royal	Royal	k1gMnSc1	Royal
Society	societa	k1gFnSc2	societa
of	of	k?	of
Chemistry	Chemistr	k1gMnPc7	Chemistr
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Chemistry	Chemistr	k1gMnPc7	Chemistr
World	World	k1gInSc1	World
<g/>
:	:	kIx,	:
Magnesium	magnesium	k1gNnSc1	magnesium
</s>
</p>
