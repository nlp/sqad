<s>
Květ	květ	k1gInSc1	květ
rostliny	rostlina	k1gFnSc2	rostlina
(	(	kIx(	(
<g/>
anthos	anthos	k1gInSc1	anthos
<g/>
,	,	kIx,	,
flos	flos	k1gInSc1	flos
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
široké	široký	k2eAgFnSc6d1	široká
definici	definice	k1gFnSc6	definice
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
krátké	krátký	k2eAgInPc4d1	krátký
výhony	výhon	k1gInPc4	výhon
s	s	k7c7	s
omezeným	omezený	k2eAgInSc7d1	omezený
růstem	růst	k1gInSc7	růst
<g/>
,	,	kIx,	,
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
přímo	přímo	k6eAd1	přímo
nebo	nebo	k8xC	nebo
nepřímo	přímo	k6eNd1	přímo
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
:	:	kIx,	:
nepřímo	přímo	k6eNd1	přímo
jako	jako	k8xC	jako
okvětí	okvětí	k1gNnSc4	okvětí
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
přes	přes	k7c4	přes
vytvoření	vytvoření	k1gNnSc4	vytvoření
reprodukčních	reprodukční	k2eAgInPc2d1	reprodukční
orgánů	orgán	k1gInPc2	orgán
(	(	kIx(	(
<g/>
tyčinek	tyčinka	k1gFnPc2	tyčinka
a	a	k8xC	a
pestíků	pestík	k1gInPc2	pestík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Užší	úzký	k2eAgFnSc1d2	užší
definice	definice	k1gFnSc1	definice
pojmu	pojem	k1gInSc2	pojem
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
na	na	k7c4	na
kvetoucí	kvetoucí	k2eAgFnPc4d1	kvetoucí
krytosemenné	krytosemenný	k2eAgFnPc4d1	krytosemenná
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Květ	květ	k1gInSc1	květ
krytosemenných	krytosemenný	k2eAgFnPc2d1	krytosemenná
rostlin	rostlina	k1gFnPc2	rostlina
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
následujících	následující	k2eAgFnPc2d1	následující
částí	část	k1gFnPc2	část
(	(	kIx(	(
<g/>
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
všechny	všechen	k3xTgFnPc4	všechen
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
okvětí	okvětí	k1gNnSc1	okvětí
-	-	kIx~	-
jako	jako	k8xS	jako
okvětí	okvětí	k1gNnSc4	okvětí
nebo	nebo	k8xC	nebo
oddělené	oddělený	k2eAgInPc4d1	oddělený
od	od	k7c2	od
kalichu	kalich	k1gInSc2	kalich
<g/>
,	,	kIx,	,
z	z	k7c2	z
gynecea	gynece	k1gInSc2	gynece
z	z	k7c2	z
pestíků	pestík	k1gInPc2	pestík
<g/>
,	,	kIx,	,
z	z	k7c2	z
tyčinek	tyčinka	k1gFnPc2	tyčinka
a	a	k8xC	a
květinové	květinový	k2eAgFnPc1d1	květinová
osy	osa	k1gFnPc1	osa
(	(	kIx(	(
<g/>
základna	základna	k1gFnSc1	základna
květu	květ	k1gInSc2	květ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinky	tyčinka	k1gFnPc1	tyčinka
produkují	produkovat	k5eAaImIp3nP	produkovat
pyl	pyl	k1gInSc4	pyl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
ve	v	k7c6	v
stigmatu	stigma	k1gNnSc6	stigma
pestíků	pestík	k1gInPc2	pestík
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
klíčí	klíčit	k5eAaImIp3nS	klíčit
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
oplodnění	oplodnění	k1gNnPc4	oplodnění
oplodněná	oplodněný	k2eAgNnPc4d1	oplodněné
ovulves	ovulves	k1gInSc4	ovulves
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
pestíku	pestík	k1gInSc6	pestík
přítomné	přítomný	k2eAgNnSc1d1	přítomné
vajíčko	vajíčko	k1gNnSc1	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
do	do	k7c2	do
embrya	embryo	k1gNnSc2	embryo
<g/>
,	,	kIx,	,
vajíčko	vajíčko	k1gNnSc1	vajíčko
se	se	k3xPyFc4	se
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
do	do	k7c2	do
semena	semeno	k1gNnSc2	semeno
a	a	k8xC	a
rostliny	rostlina	k1gFnSc2	rostlina
v	v	k7c4	v
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Kdybychom	kdyby	kYmCp1nP	kdyby
to	ten	k3xDgNnSc4	ten
vzali	vzít	k5eAaPmAgMnP	vzít
ještě	ještě	k6eAd1	ještě
stručněji	stručně	k6eAd2	stručně
<g/>
,	,	kIx,	,
květ	květ	k1gInSc4	květ
je	být	k5eAaImIp3nS	být
orgán	orgán	k1gInSc1	orgán
semenných	semenný	k2eAgFnPc2d1	semenná
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
krytosemenné	krytosemenný	k2eAgFnPc1d1	krytosemenná
a	a	k8xC	a
nahosemenné	nahosemenný	k2eAgFnPc1d1	nahosemenná
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jim	on	k3xPp3gMnPc3	on
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
pohlavnímu	pohlavní	k2eAgNnSc3d1	pohlavní
rozmnožování	rozmnožování	k1gNnSc3	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
proto	proto	k8xC	proto
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
reprodukční	reprodukční	k2eAgInPc4d1	reprodukční
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
konečnou	konečný	k2eAgFnSc7d1	konečná
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
produkce	produkce	k1gFnSc1	produkce
semen	semeno	k1gNnPc2	semeno
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
představují	představovat	k5eAaImIp3nP	představovat
následující	následující	k2eAgFnSc3d1	následující
generaci	generace	k1gFnSc3	generace
těchto	tento	k3xDgFnPc2	tento
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgInSc4d3	nejmenší
květ	květ	k1gInSc4	květ
má	mít	k5eAaImIp3nS	mít
Wolffia	Wolffia	k1gFnSc1	Wolffia
(	(	kIx(	(
<g/>
asi	asi	k9	asi
0,5	[number]	k4	0,5
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
Rafflesia	Rafflesia	k1gFnSc1	Rafflesia
(	(	kIx(	(
<g/>
až	až	k9	až
1	[number]	k4	1
metr	metr	k1gInSc1	metr
velký	velký	k2eAgInSc1d1	velký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
středoevropských	středoevropský	k2eAgInPc2d1	středoevropský
druhů	druh	k1gInPc2	druh
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc4d3	veliký
květ	květ	k1gInSc4	květ
asi	asi	k9	asi
leknín	leknín	k1gInSc4	leknín
(	(	kIx(	(
<g/>
až	až	k9	až
přes	přes	k7c4	přes
10	[number]	k4	10
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
květ	květ	k1gInSc1	květ
má	mít	k5eAaImIp3nS	mít
pak	pak	k6eAd1	pak
jednoznačně	jednoznačně	k6eAd1	jednoznačně
ocún	ocún	k1gInSc1	ocún
jesenní	jesenní	k2eAgInSc1d1	jesenní
(	(	kIx(	(
<g/>
s	s	k7c7	s
podzemní	podzemní	k2eAgFnSc7d1	podzemní
částí	část	k1gFnSc7	část
trubky	trubka	k1gFnSc2	trubka
až	až	k6eAd1	až
0,5	[number]	k4	0,5
metru	metr	k1gInSc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
orgány	orgán	k1gInPc1	orgán
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
jednoděložním	jednoděložní	k2eAgInSc6d1	jednoděložní
květu	květ	k1gInSc6	květ
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
angiospermním	angiospermní	k2eAgNnSc6d1	angiospermní
<g/>
)	)	kIx)	)
v	v	k7c6	v
určeném	určený	k2eAgNnSc6d1	určené
pořadí	pořadí	k1gNnSc6	pořadí
odzdola	odzdola	k6eAd1	odzdola
nahoru	nahoru	k6eAd1	nahoru
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
protože	protože	k8xS	protože
květinová	květinový	k2eAgFnSc1d1	květinová
osa	osa	k1gFnSc1	osa
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
stlačená	stlačený	k2eAgFnSc1d1	stlačená
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
uspořádáno	uspořádán	k2eAgNnSc1d1	uspořádáno
z	z	k7c2	z
vnějšku	vnějšek	k1gInSc2	vnějšek
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
pořadí	pořadí	k1gNnSc6	pořadí
jsem	být	k5eAaImIp1nS	být
je	být	k5eAaImIp3nS	být
také	také	k9	také
ontogeneticky	ontogeneticky	k6eAd1	ontogeneticky
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
<g/>
.	.	kIx.	.
</s>
<s>
Květní	květní	k2eAgFnPc1d1	květní
části	část	k1gFnPc1	část
můžou	můžou	k?	můžou
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
srůstat	srůstat	k5eAaImF	srůstat
-	-	kIx~	-
může	moct	k5eAaImIp3nS	moct
srůstat	srůstat	k5eAaImF	srůstat
kongenitálně	kongenitálně	k6eAd1	kongenitálně
(	(	kIx(	(
<g/>
srůst	srůst	k1gInSc1	srůst
probíhá	probíhat	k5eAaImIp3nS	probíhat
už	už	k6eAd1	už
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
jejich	jejich	k3xOp3gFnSc2	jejich
ontogeneze	ontogeneze	k1gFnSc2	ontogeneze
<g/>
)	)	kIx)	)
-	-	kIx~	-
např.	např.	kA	např.
koruny	koruna	k1gFnSc2	koruna
čeledi	čeleď	k1gFnSc2	čeleď
hvězdnicovité	hvězdnicovitý	k2eAgFnSc2d1	hvězdnicovitý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
postgenitálně	postgenitálně	k6eAd1	postgenitálně
(	(	kIx(	(
<g/>
orgány	orgán	k1gInPc1	orgán
srůstají	srůstat	k5eAaImIp3nP	srůstat
až	až	k9	až
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
orgánů	orgán	k1gInPc2	orgán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Srůst	srůst	k1gInSc1	srůst
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tangenciální	tangenciální	k2eAgMnSc1d1	tangenciální
(	(	kIx(	(
<g/>
srůstají	srůstat	k5eAaImIp3nP	srůstat
orgány	orgán	k1gInPc4	orgán
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
kruhu	kruh	k1gInSc6	kruh
<g/>
,	,	kIx,	,
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
tečny	tečna	k1gFnSc2	tečna
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
radiální	radiální	k2eAgFnSc1d1	radiální
(	(	kIx(	(
<g/>
srůstají	srůstat	k5eAaImIp3nP	srůstat
orgány	orgán	k1gInPc1	orgán
různých	různý	k2eAgInPc2d1	různý
kruhů	kruh	k1gInPc2	kruh
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
středu	střed	k1gInSc2	střed
květu	květ	k1gInSc2	květ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Květ	květ	k1gInSc1	květ
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
na	na	k7c6	na
květní	květní	k2eAgFnSc6d1	květní
stopce	stopka	k1gFnSc6	stopka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
v	v	k7c6	v
paždí	paždí	k1gNnSc6	paždí
listenu	listen	k1gInSc2	listen
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
být	být	k5eAaImF	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
i	i	k8xC	i
listénce	listénec	k1gInPc1	listénec
<g/>
.	.	kIx.	.
</s>
<s>
Květní	květní	k2eAgFnSc1d1	květní
stopka	stopka	k1gFnSc1	stopka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zkroucena	zkroutit	k5eAaPmNgFnS	zkroutit
např.	např.	kA	např.
o	o	k7c4	o
180	[number]	k4	180
<g/>
°	°	k?	°
-	-	kIx~	-
tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
resupinace	resupinace	k1gFnSc1	resupinace
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Orchidaceae	Orchidacea	k1gFnSc2	Orchidacea
<g/>
.	.	kIx.	.
</s>
<s>
Květní	květní	k2eAgFnSc1d1	květní
stopka	stopka	k1gFnSc1	stopka
přechází	přecházet	k5eAaImIp3nS	přecházet
ve	v	k7c4	v
vyvinuté	vyvinutý	k2eAgNnSc4d1	vyvinuté
květní	květní	k2eAgNnSc4d1	květní
lůžko	lůžko	k1gNnSc4	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
typ	typ	k1gInSc1	typ
květního	květní	k2eAgNnSc2d1	květní
lůžka	lůžko	k1gNnSc2	lůžko
je	být	k5eAaImIp3nS	být
lůžko	lůžko	k1gNnSc1	lůžko
prodloužené	prodloužená	k1gFnSc2	prodloužená
(	(	kIx(	(
<g/>
Magnolia	Magnolia	k1gFnSc1	Magnolia
<g/>
,	,	kIx,	,
Myosurus	Myosurus	k1gInSc1	Myosurus
<g/>
,	,	kIx,	,
<g/>
myší	myší	k2eAgInSc1d1	myší
ocásek	ocásek	k1gInSc1	ocásek
<g/>
,	,	kIx,	,
hlaváček	hlaváček	k1gInSc1	hlaváček
<g/>
,	,	kIx,	,
Adonis	Adonis	k1gInSc1	Adonis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
květní	květní	k2eAgNnSc1d1	květní
lůžko	lůžko	k1gNnSc1	lůžko
přeměněné	přeměněný	k2eAgNnSc1d1	přeměněné
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
částech	část	k1gFnPc6	část
květu	květ	k1gInSc2	květ
a	a	k8xC	a
prodloužené	prodloužená	k1gFnSc2	prodloužená
a	a	k8xC	a
pak	pak	k6eAd1	pak
vznikají	vznikat	k5eAaImIp3nP	vznikat
různé	různý	k2eAgInPc1d1	různý
orgány	orgán	k1gInPc1	orgán
-	-	kIx~	-
anthofor	anthofor	k1gInSc1	anthofor
(	(	kIx(	(
<g/>
nese	nést	k5eAaImIp3nS	nést
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
tyčinky	tyčinka	k1gFnPc4	tyčinka
a	a	k8xC	a
pestík	pestík	k1gInSc4	pestík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
androgynofor	androgynofor	k1gInSc1	androgynofor
(	(	kIx(	(
<g/>
nese	nést	k5eAaImIp3nS	nést
andreceum	andreceum	k1gInSc1	andreceum
a	a	k8xC	a
gyneceum	gyneceum	k1gInSc1	gyneceum
-	-	kIx~	-
mučenka	mučenka	k1gFnSc1	mučenka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gynofor	gynofor	k1gInSc1	gynofor
(	(	kIx(	(
<g/>
nese	nést	k5eAaImIp3nS	nést
jen	jen	k9	jen
gyneceum	gyneceum	k1gInSc1	gyneceum
-	-	kIx~	-
silenka	silenka	k1gFnSc1	silenka
hajní	hajní	k2eAgFnSc1d1	hajní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
květní	květní	k2eAgNnSc1d1	květní
lůžko	lůžko	k1gNnSc1	lůžko
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
orgány	orgány	k1gFnPc1	orgány
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obalují	obalovat	k5eAaImIp3nP	obalovat
některé	některý	k3yIgFnPc1	některý
květní	květní	k2eAgFnPc1d1	květní
části	část	k1gFnPc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Baňkovitě	baňkovitě	k6eAd1	baňkovitě
až	až	k9	až
trubicovitě	trubicovitě	k6eAd1	trubicovitě
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
květní	květní	k2eAgNnSc1d1	květní
lůžko	lůžko	k1gNnSc1	lůžko
obaluje	obalovat	k5eAaImIp3nS	obalovat
z	z	k7c2	z
části	část	k1gFnSc2	část
nebo	nebo	k8xC	nebo
celý	celý	k2eAgInSc4d1	celý
semeník	semeník	k1gInSc4	semeník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
apokarpní	apokarpnit	k5eAaPmIp3nS	apokarpnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
češule	češule	k1gFnSc1	češule
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
receptakulum	receptakulum	k1gInSc1	receptakulum
(	(	kIx(	(
<g/>
dolní	dolní	k2eAgFnSc1d1	dolní
miskovitá	miskovitý	k2eAgFnSc1d1	miskovitá
část	část	k1gFnSc1	část
květu	květ	k1gInSc2	květ
<g/>
)	)	kIx)	)
a	a	k8xC	a
češule	češule	k1gFnSc1	češule
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
receptakulum	receptakulum	k1gInSc1	receptakulum
je	být	k5eAaImIp3nS	být
útvar	útvar	k1gInSc4	útvar
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
květního	květní	k2eAgNnSc2d1	květní
lůžka	lůžko	k1gNnSc2	lůžko
a	a	k8xC	a
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
češule	češule	k1gFnSc2	češule
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
i	i	k9	i
spodní	spodní	k2eAgNnSc1d1	spodní
části	část	k1gFnPc4	část
okvětních	okvětní	k2eAgInPc2d1	okvětní
lístků	lístek	k1gInPc2	lístek
a	a	k8xC	a
tyčinek	tyčinka	k1gFnPc2	tyčinka
<g/>
.	.	kIx.	.
</s>
<s>
Speciálně	speciálně	k6eAd1	speciálně
se	se	k3xPyFc4	se
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
z	z	k7c2	z
květního	květní	k2eAgNnSc2d1	květní
lůžka	lůžko	k1gNnSc2	lůžko
dřevnatý	dřevnatý	k2eAgInSc4d1	dřevnatý
útvar	útvar	k1gInSc4	útvar
u	u	k7c2	u
čeledi	čeleď	k1gFnSc2	čeleď
bukovitých	bukovití	k1gMnPc2	bukovití
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
číška	číška	k1gFnSc1	číška
(	(	kIx(	(
<g/>
cupula	cupula	k1gFnSc1	cupula
-	-	kIx~	-
a	a	k8xC	a
můžeme	moct	k5eAaImIp1nP	moct
ji	on	k3xPp3gFnSc4	on
zpozorovat	zpozorovat	k5eAaPmF	zpozorovat
např.	např.	kA	např.
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
čepičku	čepička	k1gFnSc4	čepička
žaludu	žalud	k1gInSc2	žalud
<g/>
"	"	kIx"	"
či	či	k8xC	či
obal	obal	k1gInSc4	obal
bukvic	bukvice	k1gFnPc2	bukvice
<g/>
)	)	kIx)	)
Okvětí	okvětí	k1gNnSc1	okvětí
(	(	kIx(	(
<g/>
perigon	perigon	k1gInSc1	perigon
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
lístky	lístek	k1gInPc4	lístek
tvořící	tvořící	k2eAgInPc4d1	tvořící
obal	obal	k1gInSc4	obal
květů	květ	k1gInPc2	květ
a	a	k8xC	a
vyrůstající	vyrůstající	k2eAgFnSc4d1	vyrůstající
na	na	k7c6	na
květním	květní	k2eAgNnSc6d1	květní
lůžku	lůžko	k1gNnSc6	lůžko
bez	bez	k7c2	bez
rozlišení	rozlišení	k1gNnSc2	rozlišení
na	na	k7c4	na
kalich	kalich	k1gInSc4	kalich
a	a	k8xC	a
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
v	v	k7c6	v
jednom	jeden	k4xCgMnSc6	jeden
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
<g/>
,	,	kIx,	,
řidčeji	řídce	k6eAd2	řídce
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
kruzích	kruh	k1gInPc6	kruh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
tvarem	tvar	k1gInSc7	tvar
a	a	k8xC	a
barvou	barva	k1gFnSc7	barva
stejné	stejná	k1gFnSc2	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Květ	květ	k1gInSc1	květ
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
buď	buď	k8xC	buď
dva	dva	k4xCgMnPc1	dva
<g/>
,	,	kIx,	,
barevně	barevně	k6eAd1	barevně
či	či	k8xC	či
tvarově	tvarově	k6eAd1	tvarově
rozlišené	rozlišený	k2eAgInPc1d1	rozlišený
květní	květní	k2eAgInPc1d1	květní
obaly	obal	k1gInPc1	obal
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
pak	pak	k6eAd1	pak
nazýváme	nazývat	k5eAaImIp1nP	nazývat
kalichem	kalich	k1gInSc7	kalich
a	a	k8xC	a
korunou	koruna	k1gFnSc7	koruna
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jeden	jeden	k4xCgInSc1	jeden
nerozlišený	rozlišený	k2eNgInSc1d1	nerozlišený
květní	květní	k2eAgInSc1d1	květní
obal	obal	k1gInSc1	obal
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
okvětí	okvětí	k1gNnSc3	okvětí
<g/>
.	.	kIx.	.
</s>
<s>
Kalich	kalich	k1gInSc1	kalich
(	(	kIx(	(
<g/>
calyx	calyx	k1gInSc1	calyx
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vnější	vnější	k2eAgInSc4d1	vnější
obal	obal	k1gInSc4	obal
okvětí	okvětí	k1gNnSc2	okvětí
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
okvětních	okvětní	k2eAgInPc2d1	okvětní
lístků	lístek	k1gInPc2	lístek
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
volných	volný	k2eAgInPc2d1	volný
(	(	kIx(	(
<g/>
pryskyřník	pryskyřník	k1gInSc4	pryskyřník
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
srostlých	srostlý	k2eAgInPc2d1	srostlý
(	(	kIx(	(
<g/>
bobovité	bobovitý	k2eAgFnSc3d1	bobovitá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kalich	kalich	k1gInSc1	kalich
mohl	moct	k5eAaImAgInS	moct
původně	původně	k6eAd1	původně
vzniknout	vzniknout	k5eAaPmF	vzniknout
přeměnou	přeměna	k1gFnSc7	přeměna
listenů	listen	k1gInPc2	listen
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
je	on	k3xPp3gFnPc4	on
rozdělovat	rozdělovat	k5eAaImF	rozdělovat
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
(	(	kIx(	(
<g/>
zelený	zelený	k2eAgInSc4d1	zelený
<g/>
,	,	kIx,	,
korunovitě	korunovitě	k6eAd1	korunovitě
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
-	-	kIx~	-
Calluna	Calluna	k1gFnSc1	Calluna
<g/>
,	,	kIx,	,
vřes	vřes	k1gInSc1	vřes
<g/>
,	,	kIx,	,
Polygalaceae	Polygalaceae	k1gInSc1	Polygalaceae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
konzistence	konzistence	k1gFnSc2	konzistence
(	(	kIx(	(
<g/>
bylinný	bylinný	k2eAgInSc4d1	bylinný
<g/>
,	,	kIx,	,
suchomázdřivý	suchomázdřivý	k2eAgInSc4d1	suchomázdřivý
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vytrvalosti	vytrvalost	k1gFnSc2	vytrvalost
(	(	kIx(	(
<g/>
prchavý	prchavý	k2eAgInSc1d1	prchavý
<g/>
,	,	kIx,	,
opadavý	opadavý	k2eAgInSc1d1	opadavý
<g/>
,	,	kIx,	,
vytrvalý	vytrvalý	k2eAgInSc1d1	vytrvalý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
souměrnosti	souměrnost	k1gFnPc1	souměrnost
(	(	kIx(	(
<g/>
aktinomorfní	aktinomorfní	k2eAgFnSc1d1	aktinomorfní
<g/>
,	,	kIx,	,
zygomorfní	zygomorfní	k2eAgFnSc1d1	zygomorfní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
podle	podle	k7c2	podle
jiných	jiný	k2eAgFnPc2d1	jiná
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Systematicky	systematicky	k6eAd1	systematicky
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
srůst	srůst	k5eAaPmF	srůst
kalichu	kalich	k1gInSc3	kalich
tak	tak	k6eAd1	tak
významný	významný	k2eAgInSc4d1	významný
jako	jako	k8xC	jako
srůst	srůst	k1gInSc4	srůst
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nS	muset
totiž	totiž	k9	totiž
docházet	docházet	k5eAaImF	docházet
jen	jen	k9	jen
k	k	k7c3	k
pravému	pravý	k2eAgInSc3d1	pravý
srůstu	srůst	k1gInSc3	srůst
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kalištní	kalištní	k2eAgInPc1d1	kalištní
lístky	lístek	k1gInPc1	lístek
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zaklesnuty	zaklesnout	k5eAaPmNgInP	zaklesnout
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
i	i	k9	i
papilami	papila	k1gFnPc7	papila
<g/>
,	,	kIx,	,
chlupy	chlup	k1gInPc7	chlup
apod.	apod.	kA	apod.
Mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
i	i	k9	i
různé	různý	k2eAgFnPc4d1	různá
zvláštnosti	zvláštnost	k1gFnPc4	zvláštnost
a	a	k8xC	a
netradiční	tradiční	k2eNgInPc4d1	netradiční
kalichy	kalich	k1gInPc4	kalich
-	-	kIx~	-
čepičkovitý	čepičkovitý	k2eAgInSc4d1	čepičkovitý
kalich	kalich	k1gInSc4	kalich
(	(	kIx(	(
<g/>
Eucalyptus	Eucalyptus	k1gInSc4	Eucalyptus
-	-	kIx~	-
při	při	k7c6	při
kvetení	kvetení	k1gNnSc6	kvetení
opadává	opadávat	k5eAaImIp3nS	opadávat
víčko	víčko	k1gNnSc1	víčko
operculum	operculum	k1gInSc1	operculum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgInSc1d1	vodní
kalich	kalich	k1gInSc1	kalich
(	(	kIx(	(
<g/>
zvonek	zvonek	k1gInSc1	zvonek
<g/>
,	,	kIx,	,
Bignoniaceae	Bignoniaceae	k1gFnSc1	Bignoniaceae
-	-	kIx~	-
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
část	část	k1gFnSc1	část
kalicha	kalich	k1gInSc2	kalich
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
pomocí	pomocí	k7c2	pomocí
hydrotod	hydrotod	k1gInSc4	hydrotod
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
ve	v	k7c6	v
vodní	vodní	k2eAgFnSc6d1	vodní
nádržce	nádržka	k1gFnSc6	nádržka
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
abortaci	abortace	k1gFnSc3	abortace
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
kalich	kalich	k1gInSc1	kalich
může	moct	k5eAaImIp3nS	moct
převzít	převzít	k5eAaPmF	převzít
lákací	lákací	k2eAgFnSc1d1	lákací
funkce	funkce	k1gFnSc1	funkce
koruny	koruna	k1gFnSc2	koruna
(	(	kIx(	(
<g/>
vrabečnicovité	vrabečnicovitý	k2eAgNnSc1d1	vrabečnicovitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Běžná	běžný	k2eAgFnSc1d1	běžná
je	být	k5eAaImIp3nS	být
i	i	k9	i
přeměna	přeměna	k1gFnSc1	přeměna
na	na	k7c4	na
vytrvalý	vytrvalý	k2eAgInSc4d1	vytrvalý
kalich	kalich	k1gInSc4	kalich
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pak	pak	k6eAd1	pak
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
plodů	plod	k1gInPc2	plod
(	(	kIx(	(
<g/>
chmýr	chmýr	k1gInSc1	chmýr
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc1d1	různý
límečky	límeček	k1gInPc1	límeček
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
skupin	skupina	k1gFnPc2	skupina
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
vnější	vnější	k2eAgInSc1d1	vnější
obal	obal	k1gInSc1	obal
kalichu	kalich	k1gInSc2	kalich
-	-	kIx~	-
kalíšek	kalíšek	k1gInSc1	kalíšek
(	(	kIx(	(
<g/>
calyculus	calyculus	k1gInSc1	calyculus
<g/>
,	,	kIx,	,
epicalyx	epicalyx	k1gInSc1	epicalyx
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
bývá	bývat	k5eAaImIp3nS	bývat
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
kalich	kalich	k1gInSc4	kalich
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
třeba	třeba	k6eAd1	třeba
z	z	k7c2	z
palistů	palist	k1gInPc2	palist
kališních	kališní	k2eAgInPc2d1	kališní
lístků	lístek	k1gInPc2	lístek
(	(	kIx(	(
<g/>
Potentilla	Potentilla	k1gFnSc1	Potentilla
<g/>
,	,	kIx,	,
mochna	mochna	k1gFnSc1	mochna
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
z	z	k7c2	z
listenů	listen	k1gInPc2	listen
(	(	kIx(	(
<g/>
Malvaceae	Malvacea	k1gFnPc1	Malvacea
<g/>
,	,	kIx,	,
slézovité	slézovitý	k2eAgFnPc1d1	slézovitý
<g/>
,	,	kIx,	,
Passiflora	Passiflora	k1gFnSc1	Passiflora
<g/>
,	,	kIx,	,
mučenka	mučenka	k1gFnSc1	mučenka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kalich	kalich	k1gInSc1	kalich
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
zveličilých	zveličilý	k2eAgInPc2d1	zveličilý
listénců	listénec	k1gInPc2	listénec
(	(	kIx(	(
<g/>
opletník	opletník	k1gInSc1	opletník
plotní	plotní	k2eAgInSc1d1	plotní
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
listenovitých	listenovitý	k2eAgInPc2d1	listenovitý
útvarů	útvar	k1gInPc2	útvar
u	u	k7c2	u
zástupců	zástupce	k1gMnPc2	zástupce
Ranunculaceae	Ranunculacea	k1gFnSc2	Ranunculacea
(	(	kIx(	(
<g/>
Hepaticae	Hepaticae	k1gInSc1	Hepaticae
<g/>
,	,	kIx,	,
jaterník	jaterník	k1gInSc1	jaterník
podléška	podléška	k1gFnSc1	podléška
-	-	kIx~	-
kalich	kalich	k1gInSc1	kalich
je	být	k5eAaImIp3nS	být
napodoben	napodobit	k5eAaPmNgInS	napodobit
trojicí	trojice	k1gFnSc7	trojice
listenů	listen	k1gInPc2	listen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
corolla	corolla	k1gFnSc1	corolla
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
obal	obal	k1gInSc4	obal
květní	květní	k2eAgInSc4d1	květní
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
nezelený	zelený	k2eNgInSc4d1	nezelený
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc4d1	bílý
nebo	nebo	k8xC	nebo
barevný	barevný	k2eAgInSc4d1	barevný
<g/>
,	,	kIx,	,
jemnější	jemný	k2eAgInSc4d2	jemnější
než	než	k8xS	než
kalich	kalich	k1gInSc4	kalich
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
opadavý	opadavý	k2eAgInSc1d1	opadavý
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
volných	volný	k2eAgInPc2d1	volný
či	či	k8xC	či
srostlých	srostlý	k2eAgInPc2d1	srostlý
korunních	korunní	k2eAgInPc2d1	korunní
lístků	lístek	k1gInPc2	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
několikrát	několikrát	k6eAd1	několikrát
a	a	k8xC	a
nezávisle	závisle	k6eNd1	závisle
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
z	z	k7c2	z
tyčinek	tyčinka	k1gFnPc2	tyčinka
(	(	kIx(	(
<g/>
lze	lze	k6eAd1	lze
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
pozorovat	pozorovat	k5eAaImF	pozorovat
přechody	přechod	k1gInPc4	přechod
mezi	mezi	k7c7	mezi
tyčinkami	tyčinka	k1gFnPc7	tyčinka
a	a	k8xC	a
korunními	korunní	k2eAgInPc7d1	korunní
lístky	lístek	k1gInPc7	lístek
-	-	kIx~	-
leknín	leknín	k1gInSc1	leknín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
používaná	používaný	k2eAgFnSc1d1	používaná
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
koruna	koruna	k1gFnSc1	koruna
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
z	z	k7c2	z
kališních	kališní	k2eAgInPc2d1	kališní
lístků	lístek	k1gInPc2	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
láká	lákat	k5eAaImIp3nS	lákat
opylovače	opylovač	k1gInPc4	opylovač
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
pestrou	pestrý	k2eAgFnSc4d1	pestrá
vůni	vůně	k1gFnSc4	vůně
a	a	k8xC	a
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
ale	ale	k9	ale
může	moct	k5eAaImIp3nS	moct
někdy	někdy	k6eAd1	někdy
i	i	k9	i
opadávat	opadávat	k5eAaImF	opadávat
-	-	kIx~	-
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
prchavou	prchavý	k2eAgFnSc4d1	prchavá
korunu	koruna	k1gFnSc4	koruna
(	(	kIx(	(
<g/>
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Volné	volný	k2eAgInPc1d1	volný
korunní	korunní	k2eAgInPc1d1	korunní
listy	list	k1gInPc1	list
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dále	daleko	k6eAd2	daleko
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
na	na	k7c4	na
nehet	nehet	k1gInSc4	nehet
(	(	kIx(	(
<g/>
unguis	unguis	k1gFnSc4	unguis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čepel	čepel	k1gFnSc1	čepel
(	(	kIx(	(
<g/>
laminula	laminout	k5eAaPmAgFnS	laminout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pakorunku	pakorunka	k1gFnSc4	pakorunka
(	(	kIx(	(
<g/>
paracorolla	paracorolla	k1gFnSc1	paracorolla
-	-	kIx~	-
Silenaceae	Silenaceae	k1gInSc1	Silenaceae
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
u	u	k7c2	u
srostlých	srostlý	k2eAgInPc2d1	srostlý
korunních	korunní	k2eAgInPc2d1	korunní
listů	list	k1gInPc2	list
-	-	kIx~	-
Narcissus	Narcissus	k1gInSc1	Narcissus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
korunní	korunní	k2eAgInPc4d1	korunní
lístky	lístek	k1gInPc4	lístek
srostlé	srostlý	k2eAgInPc4d1	srostlý
-	-	kIx~	-
sympetalní	sympetalný	k2eAgMnPc1d1	sympetalný
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
už	už	k6eAd1	už
jsme	být	k5eAaImIp1nP	být
řekli	říct	k5eAaPmAgMnP	říct
<g/>
,	,	kIx,	,
srůst	srůst	k1gInSc4	srůst
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kongenitální	kongenitální	k2eAgFnSc1d1	kongenitální
či	či	k8xC	či
postgenitální	postgenitální	k2eAgFnSc1d1	postgenitální
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
korunní	korunní	k2eAgFnSc4d1	korunní
trubku	trubka	k1gFnSc4	trubka
a	a	k8xC	a
korunní	korunní	k2eAgInSc4d1	korunní
lem	lem	k1gInSc4	lem
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
v	v	k7c4	v
cípy	cíp	k1gInPc4	cíp
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ještě	ještě	k9	ještě
na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
části	část	k1gFnSc6	část
např.	např.	kA	např.
jícen	jícen	k1gInSc1	jícen
<g />
.	.	kIx.	.
</s>
<s>
<g/>
trubkovitá	trubkovitý	k2eAgFnSc1d1	trubkovitá
(	(	kIx(	(
<g/>
kostival	kostival	k1gInSc1	kostival
lékařský	lékařský	k2eAgInSc1d1	lékařský
<g/>
)	)	kIx)	)
nálevkovitá	nálevkovitý	k2eAgFnSc1d1	nálevkovitá
(	(	kIx(	(
<g/>
durman	durman	k1gInSc1	durman
<g/>
)	)	kIx)	)
zvonkovitá	zvonkovitý	k2eAgFnSc1d1	zvonkovitá
(	(	kIx(	(
<g/>
zvonek	zvonek	k1gInSc1	zvonek
okrouhlolistý	okrouhlolistý	k2eAgInSc1d1	okrouhlolistý
<g/>
)	)	kIx)	)
kulovitá	kulovitý	k2eAgFnSc1d1	kulovitá
(	(	kIx(	(
<g/>
konvalinka	konvalinka	k1gFnSc1	konvalinka
vonná	vonný	k2eAgFnSc1d1	vonná
<g/>
,	,	kIx,	,
kyhanka	kyhanka	k1gFnSc1	kyhanka
<g/>
)	)	kIx)	)
baňkovitá	baňkovitý	k2eAgFnSc1d1	baňkovitá
(	(	kIx(	(
<g/>
vřesovec	vřesovec	k1gInSc1	vřesovec
<g/>
,	,	kIx,	,
medvědice	medvědice	k1gFnSc1	medvědice
<g/>
)	)	kIx)	)
řepicovitá	řepicovitý	k2eAgFnSc1d1	řepicovitá
(	(	kIx(	(
<g/>
dole	dole	k6eAd1	dole
dlouze	dlouho	k6eAd1	dlouho
trubkovitá	trubkovitý	k2eAgFnSc1d1	trubkovitá
<g/>
,	,	kIx,	,
nahoře	nahoře	k6eAd1	nahoře
<g />
.	.	kIx.	.
</s>
<s>
náhle	náhle	k6eAd1	náhle
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
v	v	k7c4	v
široký	široký	k2eAgInSc4d1	široký
plochý	plochý	k2eAgInSc4d1	plochý
okraj	okraj	k1gInSc4	okraj
-	-	kIx~	-
šeřík	šeřík	k1gInSc1	šeřík
<g/>
,	,	kIx,	,
plamenka	plamenka	k1gFnSc1	plamenka
<g/>
,	,	kIx,	,
pilát	pilát	k1gInSc1	pilát
<g/>
)	)	kIx)	)
kolovitá	kolovitý	k2eAgFnSc1d1	kolovitá
(	(	kIx(	(
<g/>
krátká	krátký	k2eAgFnSc1d1	krátká
trubka	trubka	k1gFnSc1	trubka
a	a	k8xC	a
lem	lem	k1gInSc1	lem
rozložený	rozložený	k2eAgInSc1d1	rozložený
do	do	k7c2	do
plochy	plocha	k1gFnSc2	plocha
-	-	kIx~	-
rozrazil	rozrazil	k1gInSc1	rozrazil
<g/>
,	,	kIx,	,
divizna	divizna	k1gFnSc1	divizna
<g/>
)	)	kIx)	)
jednopyská	jednopyská	k1gFnSc1	jednopyská
(	(	kIx(	(
<g/>
ožanka	ožanka	k1gFnSc1	ožanka
dvoupyská	dvoupyská	k1gFnSc1	dvoupyská
(	(	kIx(	(
<g/>
šalvěj	šalvěj	k1gFnSc1	šalvěj
<g/>
)	)	kIx)	)
tlamatá	tlamatý	k2eAgFnSc1d1	tlamatá
(	(	kIx(	(
<g/>
pyskatá	pyskatý	k2eAgFnSc1d1	Pyskatá
koruna	koruna	k1gFnSc1	koruna
se	s	k7c7	s
široce	široko	k6eAd1	široko
otevřeným	otevřený	k2eAgInSc7d1	otevřený
jícnem	jícen	k1gInSc7	jícen
-	-	kIx~	-
hluchavka	hluchavka	k1gFnSc1	hluchavka
<g/>
)	)	kIx)	)
šklebivá	šklebivý	k2eAgFnSc1d1	šklebivá
(	(	kIx(	(
<g/>
dvoupyská	dvoupyská	k1gFnSc1	dvoupyská
s	s	k7c7	s
uzavřeným	uzavřený	k2eAgNnSc7d1	uzavřené
ústím	ústí	k1gNnSc7	ústí
a	a	k8xC	a
vysoko	vysoko	k6eAd1	vysoko
vypouklým	vypouklý	k2eAgNnSc7d1	vypouklé
patrem	patro	k1gNnSc7	patro
dolního	dolní	k2eAgInSc2d1	dolní
pysku	pysk	k1gInSc2	pysk
-	-	kIx~	-
hledík	hledík	k1gInSc1	hledík
<g/>
,	,	kIx,	,
bublinatka	bublinatka	k1gFnSc1	bublinatka
<g/>
)	)	kIx)	)
jazyková	jazykový	k2eAgFnSc1d1	jazyková
(	(	kIx(	(
<g/>
hvězdnicovité	hvězdnicovitý	k2eAgInPc1d1	hvězdnicovitý
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
</s>
<s>
Tyčinka	tyčinka	k1gFnSc1	tyčinka
je	být	k5eAaImIp3nS	být
samčí	samčí	k2eAgInSc4d1	samčí
orgán	orgán	k1gInSc4	orgán
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
k	k	k7c3	k
produkci	produkce	k1gFnSc3	produkce
pylu	pyl	k1gInSc2	pyl
<g/>
.	.	kIx.	.
</s>
<s>
Produkuje	produkovat	k5eAaImIp3nS	produkovat
pylová	pylový	k2eAgNnPc4d1	pylové
zrna	zrno	k1gNnPc4	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
-	-	kIx~	-
nitku	nitka	k1gFnSc4	nitka
a	a	k8xC	a
prašník	prašník	k1gInSc4	prašník
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
mylně	mylně	k6eAd1	mylně
považováno	považován	k2eAgNnSc1d1	považováno
spojidlo	spojidlo	k1gNnSc1	spojidlo
za	za	k7c4	za
další	další	k2eAgFnSc4d1	další
část	část	k1gFnSc4	část
tyčinky	tyčinka	k1gFnSc2	tyčinka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vlastně	vlastně	k9	vlastně
pokračování	pokračování	k1gNnSc1	pokračování
nitky	nitka	k1gFnSc2	nitka
a	a	k8xC	a
také	také	k9	také
hlavně	hlavně	k9	hlavně
pletivo	pletivo	k1gNnSc1	pletivo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
propojuje	propojovat	k5eAaImIp3nS	propojovat
dva	dva	k4xCgInPc4	dva
pylové	pylový	k2eAgInPc4d1	pylový
váčky	váček	k1gInPc4	váček
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
vybíhat	vybíhat	k5eAaImF	vybíhat
na	na	k7c4	na
prašník	prašník	k1gInSc4	prašník
(	(	kIx(	(
<g/>
vraní	vraní	k2eAgNnSc4d1	vraní
oko	oko	k1gNnSc4	oko
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
pod	pod	k7c4	pod
něj	on	k3xPp3gMnSc4	on
a	a	k8xC	a
podobat	podobat	k5eAaImF	podobat
se	se	k3xPyFc4	se
nitce	nitka	k1gFnSc3	nitka
<g/>
,	,	kIx,	,
od	od	k7c2	od
které	který	k3yIgFnSc2	který
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
oddělený	oddělený	k2eAgInSc1d1	oddělený
uzlinou	uzlina	k1gFnSc7	uzlina
(	(	kIx(	(
<g/>
brčál	brčál	k1gInSc1	brčál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinka	tyčinka	k1gFnSc1	tyčinka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
původně	původně	k6eAd1	původně
plochého	plochý	k2eAgInSc2d1	plochý
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinky	tyčinka	k1gFnPc1	tyčinka
nejprimitivnějších	primitivní	k2eAgFnPc2d3	nejprimitivnější
rostlin	rostlina	k1gFnPc2	rostlina
u	u	k7c2	u
rodů	rod	k1gInPc2	rod
Degeneria	Degenerium	k1gNnSc2	Degenerium
a	a	k8xC	a
Drimys	Drimysa	k1gFnPc2	Drimysa
jsou	být	k5eAaImIp3nP	být
ploché	plochý	k2eAgFnPc1d1	plochá
a	a	k8xC	a
lupenité	lupenitý	k2eAgFnPc1d1	lupenitá
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinky	tyčinka	k1gFnPc1	tyčinka
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
rostlin	rostlina	k1gFnPc2	rostlina
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
jednožilové	jednožilový	k2eAgFnPc1d1	jednožilový
<g/>
,	,	kIx,	,
jen	jen	k9	jen
u	u	k7c2	u
čeledi	čeleď	k1gFnSc2	čeleď
šácholonotvarých	šácholonotvarý	k2eAgFnPc2d1	šácholonotvarý
jsou	být	k5eAaImIp3nP	být
trojžilové	trojžilový	k2eAgFnPc1d1	trojžilový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základním	základní	k2eAgInSc6d1	základní
květu	květ	k1gInSc6	květ
vznikají	vznikat	k5eAaImIp3nP	vznikat
tyčinky	tyčinka	k1gFnPc4	tyčinka
jako	jako	k8xS	jako
malé	malý	k2eAgInPc4d1	malý
hrbolky	hrbolek	k1gInPc4	hrbolek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
květu	květ	k1gInSc6	květ
listu	list	k1gInSc2	list
z	z	k7c2	z
pupenu	pupen	k1gInSc2	pupen
se	se	k3xPyFc4	se
tyčinky	tyčinka	k1gFnPc1	tyčinka
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
fáze	fáze	k1gFnSc2	fáze
extalasie	extalasie	k1gFnSc2	extalasie
<g/>
.	.	kIx.	.
</s>
<s>
Gyneceum	Gyneceum	k1gInSc1	Gyneceum
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
plodolistů	plodolist	k1gInPc2	plodolist
v	v	k7c6	v
květu	květ	k1gInSc6	květ
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
sroste	srůst	k5eAaPmIp3nS	srůst
pestík	pestík	k1gInSc4	pestík
díky	díky	k7c3	díky
jedinému	jediný	k2eAgInSc3d1	jediný
plodolistu	plodolist	k1gInSc3	plodolist
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
soubor	soubor	k1gInSc1	soubor
jako	jako	k9	jako
apokarpní	apokarpnit	k5eAaPmIp3nS	apokarpnit
gyneceum	gyneceum	k1gInSc1	gyneceum
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pestík	pestík	k1gInSc1	pestík
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
srůstem	srůst	k1gInSc7	srůst
více	hodně	k6eAd2	hodně
plodolistů	plodolist	k1gInPc2	plodolist
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
cenokarpní	cenokarpnit	k5eAaPmIp3nS	cenokarpnit
gyneceum	gyneceum	k1gInSc4	gyneceum
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
objekt	objekt	k1gInSc1	objekt
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
symetrii	symetrie	k1gFnSc6	symetrie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
alespoň	alespoň	k9	alespoň
jedna	jeden	k4xCgFnSc1	jeden
rovina	rovina	k1gFnSc1	rovina
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
každá	každý	k3xTgFnSc1	každý
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
zrcadlovým	zrcadlový	k2eAgInSc7d1	zrcadlový
obrazem	obraz	k1gInSc7	obraz
druhé	druhý	k4xOgFnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
květy	květ	k1gInPc1	květ
nevykazují	vykazovat	k5eNaImIp3nP	vykazovat
rovinu	rovina	k1gFnSc4	rovina
symetrie	symetrie	k1gFnSc2	symetrie
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jsou	být	k5eAaImIp3nP	být
řekněme	říct	k5eAaPmRp1nP	říct
asymetrické	asymetrický	k2eAgNnSc1d1	asymetrické
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nepravidelné	pravidelný	k2eNgInPc1d1	nepravidelný
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
případě	případ	k1gInSc6	případ
u	u	k7c2	u
rodu	rod	k1gInSc2	rod
Dosna	dosna	k1gFnSc1	dosna
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
ale	ale	k9	ale
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
rovina	rovina	k1gFnSc1	rovina
souměrnosti	souměrnost	k1gFnSc2	souměrnost
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
ne	ne	k9	ne
jedna	jeden	k4xCgFnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
tedy	tedy	k9	tedy
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
bilaterální	bilaterální	k2eAgFnSc4d1	bilaterální
symetrii	symetrie	k1gFnSc4	symetrie
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
jedna	jeden	k4xCgFnSc1	jeden
rovina	rovina	k1gFnSc1	rovina
souměrnosti	souměrnost	k1gFnSc2	souměrnost
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
radiální	radiální	k2eAgFnSc4d1	radiální
symetrii	symetrie	k1gFnSc4	symetrie
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
více	hodně	k6eAd2	hodně
rovin	rovina	k1gFnPc2	rovina
souměrnosti	souměrnost	k1gFnSc2	souměrnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
rostlinu	rostlina	k1gFnSc4	rostlina
rozdělí	rozdělit	k5eAaPmIp3nP	rozdělit
na	na	k7c4	na
stejné	stejný	k2eAgInPc4d1	stejný
zrcadlové	zrcadlový	k2eAgInPc4d1	zrcadlový
obrazy	obraz	k1gInPc4	obraz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
jejich	jejich	k3xOp3gFnPc1	jejich
symetrie	symetrie	k1gFnPc1	symetrie
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
květů	květ	k1gInPc2	květ
<g/>
.	.	kIx.	.
</s>
<s>
Nektaria	nektarium	k1gNnPc1	nektarium
jsou	být	k5eAaImIp3nP	být
žlázy	žláza	k1gFnPc1	žláza
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
nektar	nektar	k1gInSc4	nektar
-	-	kIx~	-
pyl	pyl	k1gInSc1	pyl
-	-	kIx~	-
jako	jako	k8xS	jako
návnadu	návnada	k1gFnSc4	návnada
pro	pro	k7c4	pro
opylovače	opylovač	k1gMnPc4	opylovač
<g/>
.	.	kIx.	.
</s>
<s>
Nektar	nektar	k1gInSc1	nektar
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
cukerného	cukerný	k2eAgInSc2d1	cukerný
roztoku	roztok	k1gInSc2	roztok
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
,	,	kIx,	,
aminokyseliny	aminokyselina	k1gFnPc4	aminokyselina
a	a	k8xC	a
vitamíny	vitamín	k1gInPc4	vitamín
<g/>
.	.	kIx.	.
</s>
<s>
Nektar	nektar	k1gInSc1	nektar
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tvořen	tvořit	k5eAaImNgInS	tvořit
z	z	k7c2	z
okvětí	okvětí	k1gNnSc2	okvětí
<g/>
,	,	kIx,	,
tyčinek	tyčinka	k1gFnPc2	tyčinka
<g/>
,	,	kIx,	,
vaječníku	vaječník	k1gInSc2	vaječník
<g/>
,	,	kIx,	,
květinová	květinový	k2eAgFnSc1d1	květinová
osa	osa	k1gFnSc1	osa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mimo	mimo	k7c4	mimo
vlastní	vlastní	k2eAgInSc4d1	vlastní
květ	květ	k1gInSc4	květ
<g/>
.	.	kIx.	.
</s>
<s>
Tj.	tj.	kA	tj.
podle	podle	k7c2	podle
přítomnosti	přítomnost	k1gFnSc2	přítomnost
samčích	samčí	k2eAgInPc2d1	samčí
a	a	k8xC	a
samičích	samičí	k2eAgInPc2d1	samičí
reprodukčních	reprodukční	k2eAgInPc2d1	reprodukční
orgánů	orgán	k1gInPc2	orgán
-	-	kIx~	-
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
oboupohlavné	oboupohlavný	k2eAgMnPc4d1	oboupohlavný
(	(	kIx(	(
<g/>
monoklinické	monoklinický	k2eAgMnPc4d1	monoklinický
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednopohlavné	jednopohlavný	k2eAgFnPc1d1	jednopohlavný
(	(	kIx(	(
<g/>
diklinické	diklinický	k2eAgFnPc1d1	diklinický
-	-	kIx~	-
samčí	samčí	k2eAgFnPc1d1	samčí
a	a	k8xC	a
samičí	samičí	k2eAgFnPc1d1	samičí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oboupohlavé	Oboupohlavý	k2eAgFnPc1d1	Oboupohlavý
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
v	v	k7c6	v
květu	květ	k1gInSc6	květ
tyčinky	tyčinka	k1gFnSc2	tyčinka
i	i	k9	i
pestík	pestík	k1gInSc4	pestík
<g/>
.	.	kIx.	.
</s>
<s>
Jednopohlavé	Jednopohlavý	k2eAgFnPc1d1	Jednopohlavý
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
buď	buď	k8xC	buď
jen	jen	k6eAd1	jen
tyčinky	tyčinka	k1gFnPc4	tyčinka
(	(	kIx(	(
<g/>
květy	květ	k1gInPc4	květ
samčí	samčí	k2eAgInPc4d1	samčí
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
pestík	pestík	k1gInSc1	pestík
(	(	kIx(	(
<g/>
květy	květ	k1gInPc1	květ
samičí	samičí	k2eAgFnSc2d1	samičí
<g/>
)	)	kIx)	)
Jednopohlavé	Jednopohlavý	k2eAgFnSc2d1	Jednopohlavý
se	se	k3xPyFc4	se
déle	dlouho	k6eAd2	dlouho
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
jednodomé	jednodomý	k2eAgFnSc6d1	jednodomá
(	(	kIx(	(
<g/>
monoecické	monoecická	k1gFnSc6	monoecická
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvoudomé	dvoudomý	k2eAgMnPc4d1	dvoudomý
(	(	kIx(	(
<g/>
diecické	diecický	k2eAgMnPc4d1	diecický
<g/>
)	)	kIx)	)
Acyklinické	Acyklinický	k2eAgMnPc4d1	Acyklinický
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
spirální	spirální	k2eAgInPc4d1	spirální
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
původní	původní	k2eAgFnPc1d1	původní
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
všechny	všechen	k3xTgFnPc4	všechen
organy	organon	k1gNnPc7	organon
vyrůstající	vyrůstající	k2eAgInSc4d1	vyrůstající
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
šroubovice	šroubovice	k1gFnSc2	šroubovice
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
například	například	k6eAd1	například
o	o	k7c4	o
květiny	květina	k1gFnPc4	květina
Calycanthus	Calycanthus	k1gMnSc1	Calycanthus
floridus	floridus	k1gMnSc1	floridus
a	a	k8xC	a
Calycanthaceae	Calycanthaceae	k1gFnSc1	Calycanthaceae
<g/>
.	.	kIx.	.
</s>
<s>
Hemicyklické	Hemicyklický	k2eAgInPc1d1	Hemicyklický
neboli	neboli	k8xC	neboli
spirocyklické	spirocyklický	k2eAgInPc1d1	spirocyklický
květy	květ	k1gInPc1	květ
mají	mít	k5eAaImIp3nP	mít
část	část	k1gFnSc4	část
orgánů	orgán	k1gInPc2	orgán
vyrůstajících	vyrůstající	k2eAgInPc2d1	vyrůstající
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
a	a	k8xC	a
část	část	k1gFnSc1	část
ve	v	k7c6	v
šroubovici	šroubovice	k1gFnSc6	šroubovice
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
například	například	k6eAd1	například
o	o	k7c4	o
Nuphar	Nuphar	k1gInSc4	Nuphar
luteum	luteum	k1gNnSc4	luteum
<g/>
,	,	kIx,	,
stulík	stulík	k1gInSc4	stulík
žlutý	žlutý	k2eAgInSc4d1	žlutý
<g/>
,	,	kIx,	,
Adonis	Adonis	k1gFnSc1	Adonis
vernalis	vernalis	k1gFnSc1	vernalis
a	a	k8xC	a
hlaváček	hlaváček	k1gInSc1	hlaváček
jarní	jarní	k2eAgInSc1d1	jarní
<g/>
.	.	kIx.	.
</s>
<s>
Cyklické	cyklický	k2eAgInPc4d1	cyklický
květy	květ	k1gInPc4	květ
pak	pak	k6eAd1	pak
mají	mít	k5eAaImIp3nP	mít
všechny	všechen	k3xTgInPc4	všechen
orgány	orgán	k1gInPc4	orgán
v	v	k7c6	v
kruzích	kruh	k1gInPc6	kruh
<g/>
,	,	kIx,	,
zástupci	zástupce	k1gMnPc1	zástupce
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
třeba	třeba	k6eAd1	třeba
Primula	Primula	k?	Primula
veris	veris	k1gInSc1	veris
a	a	k8xC	a
prvosenka	prvosenka	k1gFnSc1	prvosenka
jarní	jarní	k2eAgFnSc1d1	jarní
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
u	u	k7c2	u
rostliny	rostlina	k1gFnSc2	rostlina
s	s	k7c7	s
normálně	normálně	k6eAd1	normálně
zygomorfními	zygomorfní	k2eAgInPc7d1	zygomorfní
květy	květ	k1gInPc7	květ
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
zdánlivě	zdánlivě	k6eAd1	zdánlivě
terminální	terminální	k2eAgInSc4d1	terminální
květ	květ	k1gInSc4	květ
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
aktinomorfní	aktinomorfní	k2eAgFnSc1d1	aktinomorfní
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
pelorie	pelorie	k1gFnSc1	pelorie
<g/>
.	.	kIx.	.
</s>
<s>
Souměrné	souměrný	k2eAgFnPc4d1	souměrná
(	(	kIx(	(
<g/>
zygomorfní	zygomorfní	k2eAgFnPc4d1	zygomorfní
<g/>
)	)	kIx)	)
-	-	kIx~	-
např.	např.	kA	např.
květ	květ	k1gInSc4	květ
hluchavky	hluchavka	k1gFnSc2	hluchavka
Bisymetrické	Bisymetrický	k2eAgFnSc2d1	Bisymetrický
-	-	kIx~	-
např.	např.	kA	např.
květ	květ	k1gInSc4	květ
srdcovky	srdcovka	k1gFnSc2	srdcovka
Pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
(	(	kIx(	(
<g/>
aktinomorfní	aktinomorfní	k2eAgFnSc2d1	aktinomorfní
<g/>
)	)	kIx)	)
-	-	kIx~	-
např.	např.	kA	např.
květ	květ	k1gInSc4	květ
zvonku	zvonek	k1gInSc2	zvonek
<g/>
,	,	kIx,	,
třešně	třešeň	k1gFnPc1	třešeň
Nepravidelné	pravidelný	k2eNgFnPc1d1	nepravidelná
(	(	kIx(	(
<g/>
asymetrické	asymetrický	k2eAgFnPc1d1	asymetrická
<g/>
)	)	kIx)	)
-	-	kIx~	-
např.	např.	kA	např.
květ	květ	k1gInSc4	květ
kozlíku	kozlík	k1gInSc2	kozlík
lékařského	lékařský	k2eAgInSc2d1	lékařský
Podle	podle	k7c2	podle
otvírání	otvírání	k1gNnSc2	otvírání
se	s	k7c7	s
květy	květ	k1gInPc7	květ
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c6	na
chasmogamické	chasmogamický	k2eAgFnSc6d1	chasmogamický
(	(	kIx(	(
<g/>
otevřené	otevřený	k2eAgFnSc6d1	otevřená
<g/>
)	)	kIx)	)
a	a	k8xC	a
kleistogamické	kleistogamický	k2eAgFnPc1d1	kleistogamický
(	(	kIx(	(
<g/>
trvale	trvale	k6eAd1	trvale
uzavřené	uzavřený	k2eAgFnPc4d1	uzavřená
-	-	kIx~	-
Epipactis	Epipactis	k1gFnPc4	Epipactis
<g/>
,	,	kIx,	,
krušník	krušník	k1gInSc4	krušník
nebo	nebo	k8xC	nebo
letní	letní	k2eAgInPc4d1	letní
květy	květ	k1gInPc4	květ
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
violek	violka	k1gFnPc2	violka
<g/>
.	.	kIx.	.
</s>
