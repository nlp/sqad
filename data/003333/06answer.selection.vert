<s>
Waters	Waters	k6eAd1	Waters
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1985	[number]	k4	1985
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
opouští	opouštět	k5eAaImIp3nS	opouštět
skupinu	skupina	k1gFnSc4	skupina
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
kreativně	kreativně	k6eAd1	kreativně
vyčerpanou	vyčerpaný	k2eAgFnSc4d1	vyčerpaná
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bez	bez	k7c2	bez
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
hlavního	hlavní	k2eAgMnSc2d1	hlavní
skladatele	skladatel	k1gMnSc2	skladatel
<g/>
,	,	kIx,	,
kapela	kapela	k1gFnSc1	kapela
zanikne	zaniknout	k5eAaPmIp3nS	zaniknout
<g/>
.	.	kIx.	.
</s>
