<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
generických	generický	k2eAgFnPc2d1	generická
domén	doména	k1gFnPc2	doména
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
domény	doména	k1gFnSc2	doména
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
net	net	k?	net
a	a	k8xC	a
.	.	kIx.	.
<g/>
org	org	k?	org
zcela	zcela	k6eAd1	zcela
otevřené	otevřený	k2eAgNnSc1d1	otevřené
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
kdokoli	kdokoli	k3yInSc1	kdokoli
si	se	k3xPyFc3	se
pod	pod	k7c7	pod
nimi	on	k3xPp3gInPc7	on
může	moct	k5eAaImIp3nS	moct
zaregistrovat	zaregistrovat	k5eAaPmF	zaregistrovat
doménu	doména	k1gFnSc4	doména
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
zamýšlenému	zamýšlený	k2eAgInSc3d1	zamýšlený
cíli	cíl	k1gInSc3	cíl
dané	daný	k2eAgFnSc2d1	daná
domény	doména	k1gFnSc2	doména
<g/>
.	.	kIx.	.
</s>
