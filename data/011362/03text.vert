<p>
<s>
Doména	doména	k1gFnSc1	doména
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
(	(	kIx(	(
<g/>
TLD	TLD	kA	TLD
<g/>
,	,	kIx,	,
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
Top	topit	k5eAaImRp2nS	topit
Level	level	k1gInSc4	level
Domain	Domain	k1gInSc1	Domain
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
internetová	internetový	k2eAgFnSc1d1	internetová
doména	doména	k1gFnSc1	doména
na	na	k7c6	na
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
úrovni	úroveň	k1gFnSc6	úroveň
stromu	strom	k1gInSc2	strom
internetových	internetový	k2eAgFnPc2d1	internetová
domén	doména	k1gFnPc2	doména
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
doménovém	doménový	k2eAgNnSc6d1	doménové
jméně	jméno	k1gNnSc6	jméno
je	být	k5eAaImIp3nS	být
doména	doména	k1gFnSc1	doména
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
úrovně	úroveň	k1gFnSc2	úroveň
uvedena	uvést	k5eAaPmNgFnS	uvést
na	na	k7c6	na
konci	konec	k1gInSc6	konec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
cs	cs	k?	cs
<g/>
.	.	kIx.	.
<g/>
wikipedia	wikipedium	k1gNnSc2	wikipedium
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
je	být	k5eAaImIp3nS	být
doménou	doména	k1gFnSc7	doména
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
org	org	k?	org
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
TLD	TLD	kA	TLD
popisuje	popisovat	k5eAaImIp3nS	popisovat
základní	základní	k2eAgFnSc4d1	základní
skupinu	skupina	k1gFnSc4	skupina
doménových	doménový	k2eAgNnPc2d1	doménové
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
např.	např.	kA	např.
všechna	všechen	k3xTgNnPc1	všechen
doménová	doménový	k2eAgNnPc1d1	doménové
jména	jméno	k1gNnPc1	jméno
daného	daný	k2eAgInSc2d1	daný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Domény	doména	k1gFnPc1	doména
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
jsou	být	k5eAaImIp3nP	být
pevně	pevně	k6eAd1	pevně
stanoveny	stanovit	k5eAaPmNgFnP	stanovit
internetovou	internetový	k2eAgFnSc7d1	internetová
standardizační	standardizační	k2eAgFnSc7d1	standardizační
organizací	organizace	k1gFnSc7	organizace
IANA	Ianus	k1gMnSc2	Ianus
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
TLD	TLD	kA	TLD
následujících	následující	k2eAgInPc2d1	následující
tří	tři	k4xCgInPc2	tři
typů	typ	k1gInPc2	typ
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
TLD	TLD	kA	TLD
(	(	kIx(	(
<g/>
country-code	countryod	k1gInSc5	country-cod
TLD	TLD	kA	TLD
<g/>
,	,	kIx,	,
ccTLD	ccTLD	k?	ccTLD
<g/>
)	)	kIx)	)
sdružující	sdružující	k2eAgFnSc2d1	sdružující
domény	doména	k1gFnSc2	doména
jednoho	jeden	k4xCgInSc2	jeden
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
dvoupísmenný	dvoupísmenný	k2eAgMnSc1d1	dvoupísmenný
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
odpovídající	odpovídající	k2eAgFnPc4d1	odpovídající
kódu	kód	k1gInSc3	kód
země	země	k1gFnSc1	země
podle	podle	k7c2	podle
ISO	ISO	kA	ISO
3166	[number]	k4	3166
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
např.	např.	kA	např.
cz	cz	k?	cz
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Generické	generický	k2eAgInPc1d1	generický
TLD	TLD	kA	TLD
(	(	kIx(	(
<g/>
generic	generic	k1gMnSc1	generic
TLD	TLD	kA	TLD
<g/>
,	,	kIx,	,
gTLD	gTLD	k?	gTLD
<g/>
)	)	kIx)	)
sdružující	sdružující	k2eAgFnSc2d1	sdružující
obecné	obecný	k2eAgFnSc2d1	obecná
domény	doména	k1gFnSc2	doména
(	(	kIx(	(
<g/>
např.	např.	kA	např.
org	org	k?	org
pro	pro	k7c4	pro
neziskové	ziskový	k2eNgFnPc4d1	nezisková
organizace	organizace	k1gFnPc4	organizace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nespojené	spojený	k2eNgNnSc1d1	nespojené
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
konkrétním	konkrétní	k2eAgInSc7d1	konkrétní
státem	stát	k1gInSc7	stát
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c6	na
výjimku	výjimek	k1gInSc6	výjimek
TLD	TLD	kA	TLD
mil	míle	k1gFnPc2	míle
a	a	k8xC	a
gov	gov	k?	gov
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
historických	historický	k2eAgInPc2d1	historický
důvodů	důvod	k1gInPc2	důvod
vyhrazeny	vyhradit	k5eAaPmNgInP	vyhradit
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgNnSc4d1	vojenské
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
vládní	vládní	k2eAgFnSc2d1	vládní
počítačové	počítačový	k2eAgFnSc2d1	počítačová
sítě	síť	k1gFnSc2	síť
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Infrastrukturní	infrastrukturní	k2eAgNnSc1d1	infrastrukturní
TLD	TLD	kA	TLD
využívané	využívaný	k2eAgNnSc1d1	využívané
pro	pro	k7c4	pro
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
mechanismy	mechanismus	k1gInPc4	mechanismus
Internetu	Internet	k1gInSc2	Internet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
existuje	existovat	k5eAaImIp3nS	existovat
jediná	jediný	k2eAgFnSc1d1	jediná
taková	takový	k3xDgFnSc1	takový
TLD	TLD	kA	TLD
<g/>
:	:	kIx,	:
arpa	arpa	k1gMnSc1	arpa
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgFnSc1d1	používaná
systémem	systém	k1gInSc7	systém
DNS	DNS	kA	DNS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Geografické	geografický	k2eAgInPc1d1	geografický
TLD	TLD	kA	TLD
(	(	kIx(	(
<g/>
Geo	Geo	k1gMnSc1	Geo
TLD	TLD	kA	TLD
<g/>
)	)	kIx)	)
sdružující	sdružující	k2eAgFnSc2d1	sdružující
domény	doména	k1gFnSc2	doména
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
provincie	provincie	k1gFnSc2	provincie
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Národní	národní	k2eAgFnSc1d1	národní
doména	doména	k1gFnSc1	doména
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
==	==	k?	==
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
doména	doména	k1gFnSc1	doména
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
country-code	countryod	k1gInSc5	country-cod
TLD	TLD	kA	TLD
<g/>
,	,	kIx,	,
ccTLD	ccTLD	k?	ccTLD
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
doména	doména	k1gFnSc1	doména
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
společná	společný	k2eAgFnSc1d1	společná
pro	pro	k7c4	pro
domény	doména	k1gFnPc4	doména
daného	daný	k2eAgInSc2d1	daný
státu	stát	k1gInSc2	stát
či	či	k8xC	či
závislého	závislý	k2eAgNnSc2d1	závislé
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
dvoupísmenný	dvoupísmenný	k2eAgInSc1d1	dvoupísmenný
a	a	k8xC	a
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
kódu	kód	k1gInSc3	kód
země	zem	k1gFnSc2	zem
podle	podle	k7c2	podle
ISO	ISO	kA	ISO
3166	[number]	k4	3166
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
doména	doména	k1gFnSc1	doména
.	.	kIx.	.
<g/>
cz	cz	k?	cz
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
počítačové	počítačový	k2eAgFnPc4d1	počítačová
sítě	síť	k1gFnPc4	síť
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obecné	obecný	k2eAgFnPc4d1	obecná
informace	informace	k1gFnPc4	informace
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
státu	stát	k1gInSc6	stát
určuje	určovat	k5eAaImIp3nS	určovat
příslušný	příslušný	k2eAgMnSc1d1	příslušný
správce	správce	k1gMnSc1	správce
národní	národní	k2eAgFnSc2d1	národní
domény	doména	k1gFnSc2	doména
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
doménová	doménový	k2eAgNnPc1d1	doménové
jména	jméno	k1gNnPc1	jméno
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
TLD	TLD	kA	TLD
přidělují	přidělovat	k5eAaImIp3nP	přidělovat
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
registraci	registrace	k1gFnSc4	registrace
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
národní	národní	k2eAgFnSc6d1	národní
TLD	TLD	kA	TLD
libovolnému	libovolný	k2eAgMnSc3d1	libovolný
zájemci	zájemce	k1gMnSc3	zájemce
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
příslušným	příslušný	k2eAgInSc7d1	příslušný
státem	stát	k1gInSc7	stát
něco	něco	k3yInSc4	něco
společného	společný	k2eAgMnSc4d1	společný
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
takové	takový	k3xDgInPc4	takový
státy	stát	k1gInPc4	stát
a	a	k8xC	a
území	území	k1gNnPc4	území
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
ČR	ČR	kA	ČR
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
doménou	doména	k1gFnSc7	doména
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
také	také	k9	také
např.	např.	kA	např.
Arménie	Arménie	k1gFnSc1	Arménie
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
am	am	k?	am
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
it	it	k?	it
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jersey	Jersey	k1gInPc1	Jersey
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
je	být	k5eAaImIp3nS	být
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kokosové	kokosový	k2eAgFnSc2d1	kokosová
<g />
.	.	kIx.	.
</s>
<s>
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
cc	cc	k?	cc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
de	de	k?	de
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Niue	Niue	k1gFnSc1	Niue
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
nu	nu	k9	nu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
at	at	k?	at
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Samoa	Samoa	k1gFnSc1	Samoa
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
ws	ws	k?	ws
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tonga	Tonga	k1gFnSc1	Tonga
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
to	ten	k3xDgNnSc4	ten
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Turkmenistán	Turkmenistán	k1gInSc1	Turkmenistán
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
tm	tm	k?	tm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mikronésie	Mikronésie	k1gFnSc1	Mikronésie
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
fm	fm	k?	fm
<g/>
)	)	kIx)	)
či	či	k8xC	či
Tuvalu	Tuvala	k1gFnSc4	Tuvala
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
tv	tv	k?	tv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
existenci	existence	k1gFnSc4	existence
"	"	kIx"	"
<g/>
zajímavých	zajímavý	k2eAgNnPc2d1	zajímavé
<g/>
"	"	kIx"	"
doménových	doménový	k2eAgNnPc2d1	doménové
jmen	jméno	k1gNnPc2	jméno
jako	jako	k8xC	jako
např.	např.	kA	např.
coje	coje	k1gFnSc7	coje
<g/>
.	.	kIx.	.
<g/>
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
uloz	uloz	k1gInSc4	uloz
<g/>
.	.	kIx.	.
<g/>
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
barrandov	barrandov	k1gInSc1	barrandov
<g/>
.	.	kIx.	.
<g/>
tv	tv	k?	tv
<g/>
,	,	kIx,	,
rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
<g/>
fm	fm	k?	fm
<g/>
,	,	kIx,	,
I.	I.	kA	I.
<g/>
am	am	k?	am
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
jsem	být	k5eAaImIp1nS	být
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
start	start	k1gInSc1	start
<g/>
.	.	kIx.	.
<g/>
at	at	k?	at
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
začít	začít	k5eAaPmF	začít
u	u	k7c2	u
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
go	go	k?	go
<g/>
.	.	kIx.	.
<g/>
to	ten	k3xDgNnSc4	ten
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
jít	jít	k5eAaImF	jít
na	na	k7c4	na
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
státy	stát	k1gInPc1	stát
a	a	k8xC	a
závislá	závislý	k2eAgNnPc1d1	závislé
území	území	k1gNnPc1	území
povolují	povolovat	k5eAaImIp3nP	povolovat
registraci	registrace	k1gFnSc4	registrace
domény	doména	k1gFnSc2	doména
v	v	k7c6	v
příslušné	příslušný	k2eAgFnSc6d1	příslušná
TLD	TLD	kA	TLD
pouze	pouze	k6eAd1	pouze
občanům	občan	k1gMnPc3	občan
či	či	k8xC	či
podnikům	podnik	k1gInPc3	podnik
té	ten	k3xDgFnSc2	ten
které	který	k3yRgFnSc2	který
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Kanada	Kanada	k1gFnSc1	Kanada
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
ca	ca	kA	ca
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
mn	mn	k?	mn
<g/>
)	)	kIx)	)
či	či	k8xC	či
Slovensko	Slovensko	k1gNnSc4	Slovensko
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Unikátní	unikátní	k2eAgInSc4d1	unikátní
projekt	projekt	k1gInSc4	projekt
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
Tokelau	Tokelaa	k1gFnSc4	Tokelaa
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
tk	tk	k?	tk
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
stránkách	stránka	k1gFnPc6	stránka
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
registraci	registrace	k1gFnSc4	registrace
doménového	doménový	k2eAgNnSc2d1	doménové
jména	jméno	k1gNnSc2	jméno
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
národních	národní	k2eAgFnPc2d1	národní
domén	doména	k1gFnPc2	doména
používá	používat	k5eAaImIp3nS	používat
kód	kód	k1gInSc4	kód
země	zem	k1gFnSc2	zem
podle	podle	k7c2	podle
ISO	ISO	kA	ISO
3166	[number]	k4	3166
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
ccTLD	ccTLD	k?	ccTLD
však	však	k9	však
tuto	tento	k3xDgFnSc4	tento
konvenci	konvence	k1gFnSc4	konvence
nedodržují	dodržovat	k5eNaImIp3nP	dodržovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
ISO	ISO	kA	ISO
3166	[number]	k4	3166
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nejsou	být	k5eNaImIp3nP	být
ccTLD	ccTLD	k?	ccTLD
===	===	k?	===
</s>
</p>
<p>
<s>
<g/>
eh	eh	k0	eh
–	–	k?	–
rezervované	rezervovaný	k2eAgFnPc4d1	rezervovaná
pro	pro	k7c4	pro
Západní	západní	k2eAgFnPc4d1	západní
Saharu	Sahara	k1gFnSc4	Sahara
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
přidělen	přidělit	k5eAaPmNgInS	přidělit
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
zanesen	zanést	k5eAaPmNgInS	zanést
v	v	k7c6	v
systému	systém	k1gInSc6	systém
DNS	DNS	kA	DNS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
<g/>
kp	kp	k?	kp
–	–	k?	–
rezervované	rezervovaný	k2eAgFnSc2d1	rezervovaná
pro	pro	k7c4	pro
Severní	severní	k2eAgFnSc4d1	severní
Koreu	Korea	k1gFnSc4	Korea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
<g/>
tl	tl	k?	tl
–	–	k?	–
Východní	východní	k2eAgInSc4d1	východní
Timor	Timor	k1gInSc4	Timor
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
používá	používat	k5eAaImIp3nS	používat
starší	starý	k2eAgInSc4d2	starší
kód	kód	k1gInSc4	kód
.	.	kIx.	.
<g/>
tp	tp	k?	tp
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
<g/>
cs	cs	k?	cs
–	–	k?	–
Srbsko	Srbsko	k1gNnSc1	Srbsko
a	a	k8xC	a
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
ccTLD	ccTLD	k?	ccTLD
.	.	kIx.	.
<g/>
cs	cs	k?	cs
pro	pro	k7c4	pro
Československo	Československo	k1gNnSc4	Československo
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
nahrazená	nahrazený	k2eAgFnSc1d1	nahrazená
českým	český	k2eAgInSc7d1	český
.	.	kIx.	.
<g/>
cz	cz	k?	cz
a	a	k8xC	a
slovenským	slovenský	k2eAgInSc7d1	slovenský
.	.	kIx.	.
<g/>
sk	sk	k?	sk
<g/>
;	;	kIx,	;
Srbsko	Srbsko	k1gNnSc1	Srbsko
a	a	k8xC	a
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
používá	používat	k5eAaImIp3nS	používat
ccTLD	ccTLD	k?	ccTLD
.	.	kIx.	.
<g/>
yu	yu	k?	yu
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
<g/>
ax	ax	k?	ax
–	–	k?	–
Å	Å	k?	Å
</s>
</p>
<p>
<s>
===	===	k?	===
Nepoužívané	používaný	k2eNgFnSc6d1	nepoužívaná
ccTLD	ccTLD	k?	ccTLD
===	===	k?	===
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
zaneseny	zanést	k5eAaPmNgInP	zanést
v	v	k7c6	v
DNS	DNS	kA	DNS
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neexistují	existovat	k5eNaImIp3nP	existovat
žádné	žádný	k3yNgInPc1	žádný
jejich	jejich	k3xOp3gInPc1	jejich
poddomény	poddomén	k1gInPc1	poddomén
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
ani	ani	k8xC	ani
přijímány	přijímán	k2eAgFnPc1d1	přijímána
registrace	registrace	k1gFnPc1	registrace
domén	doména	k1gFnPc2	doména
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
TLD	TLD	kA	TLD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
<g/>
bv	bv	k?	bv
–	–	k?	–
norské	norský	k2eAgFnSc2d1	norská
závislé	závislý	k2eAgFnSc2d1	závislá
území	území	k1gNnSc6	území
Bouvetův	Bouvetův	k2eAgInSc1d1	Bouvetův
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
neobydlený	obydlený	k2eNgInSc1d1	neobydlený
ostrov	ostrov	k1gInSc1	ostrov
vyhlášený	vyhlášený	k2eAgInSc1d1	vyhlášený
za	za	k7c4	za
přírodní	přírodní	k2eAgFnSc4d1	přírodní
rezervaci	rezervace	k1gFnSc4	rezervace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
<g/>
sj	sj	k?	sj
–	–	k?	–
norské	norský	k2eAgNnSc1d1	norské
závislé	závislý	k2eAgNnSc1d1	závislé
území	území	k1gNnSc1	území
Svalbard	Svalbarda	k1gFnPc2	Svalbarda
</s>
</p>
<p>
<s>
<g/>
gb	gb	k?	gb
–	–	k?	–
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
registrováno	registrován	k2eAgNnSc1d1	registrováno
jediné	jediný	k2eAgNnSc1d1	jediné
doménové	doménový	k2eAgNnSc1d1	doménové
jméno	jméno	k1gNnSc1	jméno
(	(	kIx(	(
<g/>
dra	dřít	k5eAaImSgMnS	dřít
<g/>
.	.	kIx.	.
<g/>
hmg	hmg	k?	hmg
<g/>
.	.	kIx.	.
<g/>
gb	gb	k?	gb
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
e-maily	eail	k1gInPc4	e-mail
<g/>
;	;	kIx,	;
WWW	WWW	kA	WWW
stránka	stránka	k1gFnSc1	stránka
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgFnPc1	žádný
nové	nový	k2eAgFnPc1d1	nová
registrace	registrace	k1gFnPc1	registrace
nejsou	být	k5eNaImIp3nP	být
přijímány	přijímán	k2eAgFnPc1d1	přijímána
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
doména	doména	k1gFnSc1	doména
.	.	kIx.	.
<g/>
uk	uk	k?	uk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
<g/>
so	so	k?	so
–	–	k?	–
Somálsko	Somálsko	k1gNnSc1	Somálsko
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
současné	současný	k2eAgFnSc3d1	současná
nestabilitě	nestabilita	k1gFnSc3	nestabilita
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
neprobíhá	probíhat	k5eNaImIp3nS	probíhat
registrace	registrace	k1gFnSc1	registrace
domén	doména	k1gFnPc2	doména
v	v	k7c6	v
.	.	kIx.	.
<g/>
so	so	k?	so
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
ccTLD	ccTLD	k?	ccTLD
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
ISO	ISO	kA	ISO
3166-1	[number]	k4	3166-1
===	===	k?	===
</s>
</p>
<p>
<s>
<g/>
ac	ac	k?	ac
–	–	k?	–
Ascension	Ascension	k1gInSc1	Ascension
</s>
</p>
<p>
<s>
<g/>
gg	gg	k?	gg
–	–	k?	–
Guernsey	Guernsea	k1gFnSc2	Guernsea
</s>
</p>
<p>
<s>
<g/>
im	im	k?	im
–	–	k?	–
Ostrov	ostrov	k1gInSc1	ostrov
Man	Man	k1gMnSc1	Man
</s>
</p>
<p>
<s>
<g/>
je	být	k5eAaImIp3nS	být
–	–	k?	–
Jersey	Jersea	k1gFnPc4	Jersea
<g/>
:	:	kIx,	:
tyto	tento	k3xDgInPc1	tento
kódy	kód	k1gInPc1	kód
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c7	mezi
kódy	kód	k1gInPc7	kód
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ISO	ISO	kA	ISO
3166-1	[number]	k4	3166-1
alpha-	alpha-	k?	alpha-
<g/>
2	[number]	k4	2
rezervovány	rezervovat	k5eAaBmNgInP	rezervovat
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
poštovní	poštovní	k2eAgFnSc4d1	poštovní
unii	unie	k1gFnSc4	unie
UPU	UPU	kA	UPU
<g/>
.	.	kIx.	.
</s>
<s>
IANA	Ianus	k1gMnSc4	Ianus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
povolila	povolit	k5eAaPmAgFnS	povolit
použití	použití	k1gNnSc4	použití
takových	takový	k3xDgInPc2	takový
kódů	kód	k1gInPc2	kód
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
však	však	k9	však
toto	tento	k3xDgNnSc4	tento
povolení	povolení	k1gNnSc4	povolení
zrušila	zrušit	k5eAaPmAgFnS	zrušit
<g/>
;	;	kIx,	;
tyto	tento	k3xDgFnPc4	tento
TLD	TLD	kA	TLD
jsou	být	k5eAaImIp3nP	být
jediné	jediný	k2eAgFnPc1d1	jediná
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
podle	podle	k7c2	podle
tohoto	tento	k3xDgNnSc2	tento
pravidla	pravidlo	k1gNnSc2	pravidlo
přiděleny	přidělen	k2eAgInPc1d1	přidělen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
<g/>
su	su	k?	su
–	–	k?	–
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
neplatný	platný	k2eNgInSc1d1	neplatný
kód	kód	k1gInSc1	kód
ISO	ISO	kA	ISO
3166-1	[number]	k4	3166-1
pro	pro	k7c4	pro
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgFnPc4d1	další
registrace	registrace	k1gFnPc4	registrace
již	již	k6eAd1	již
nebudou	být	k5eNaImBp3nP	být
přijímány	přijímat	k5eAaImNgInP	přijímat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
doména	doména	k1gFnSc1	doména
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
aktivní	aktivní	k2eAgMnSc1d1	aktivní
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
doména	doména	k1gFnSc1	doména
zaregistrovat	zaregistrovat	k5eAaPmF	zaregistrovat
za	za	k7c4	za
100	[number]	k4	100
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
<g/>
tp	tp	k?	tp
–	–	k?	–
(	(	kIx(	(
<g/>
starší	starý	k2eAgInSc4d2	starší
kód	kód	k1gInSc4	kód
ISO	ISO	kA	ISO
3166-1	[number]	k4	3166-1
pro	pro	k7c4	pro
Východní	východní	k2eAgInSc4d1	východní
Timor	Timor	k1gInSc4	Timor
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Východní	východní	k2eAgInSc1d1	východní
Timor	Timor	k1gInSc1	Timor
používá	používat	k5eAaImIp3nS	používat
tento	tento	k3xDgInSc4	tento
starší	starý	k2eAgInSc4d2	starší
kód	kód	k1gInSc4	kód
namísto	namísto	k7c2	namísto
aktuálně	aktuálně	k6eAd1	aktuálně
platného	platný	k2eAgInSc2d1	platný
kódu	kód	k1gInSc2	kód
tl	tl	k?	tl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
<g/>
uk	uk	k?	uk
–	–	k?	–
Kód	kód	k1gInSc1	kód
ISO	ISO	kA	ISO
3166-1	[number]	k4	3166-1
pro	pro	k7c4	pro
Spojené	spojený	k2eAgNnSc4d1	spojené
království	království	k1gNnSc4	království
je	být	k5eAaImIp3nS	být
gb	gb	k?	gb
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
předchozích	předchozí	k2eAgFnPc6d1	předchozí
počítačových	počítačový	k2eAgFnPc6d1	počítačová
sítích	síť	k1gFnPc6	síť
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
identifikátor	identifikátor	k1gInSc1	identifikátor
.	.	kIx.	.
<g/>
uk	uk	k?	uk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
promítlo	promítnout	k5eAaPmAgNnS	promítnout
i	i	k9	i
do	do	k7c2	do
TLD	TLD	kA	TLD
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
přidělena	přidělen	k2eAgFnSc1d1	přidělena
i	i	k8xC	i
TLD	TLD	kA	TLD
.	.	kIx.	.
<g/>
gb	gb	k?	gb
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
cílem	cíl	k1gInSc7	cíl
byl	být	k5eAaImAgInS	být
postupný	postupný	k2eAgInSc1d1	postupný
přechod	přechod	k1gInSc1	přechod
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ovšem	ovšem	k9	ovšem
nikdy	nikdy	k6eAd1	nikdy
nedošlo	dojít	k5eNaPmAgNnS	dojít
a	a	k8xC	a
široce	široko	k6eAd1	široko
používané	používaný	k2eAgInPc1d1	používaný
.	.	kIx.	.
<g/>
uk	uk	k?	uk
již	již	k6eAd1	již
lze	lze	k6eAd1	lze
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
<g/>
yu	yu	k?	yu
–	–	k?	–
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
neplatný	platný	k2eNgInSc1d1	neplatný
kód	kód	k1gInSc1	kód
ISO	ISO	kA	ISO
3166-1	[number]	k4	3166-1
pro	pro	k7c4	pro
Jugoslávii	Jugoslávie	k1gFnSc4	Jugoslávie
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
používáno	používat	k5eAaImNgNnS	používat
pro	pro	k7c4	pro
Srbsko	Srbsko	k1gNnSc4	Srbsko
a	a	k8xC	a
Černou	černý	k2eAgFnSc4d1	černá
Horu	hora	k1gFnSc4	hora
namísto	namísto	k7c2	namísto
správného	správný	k2eAgInSc2d1	správný
kódu	kód	k1gInSc2	kód
.	.	kIx.	.
<g/>
cs	cs	k?	cs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
<g/>
eu	eu	k?	eu
–	–	k?	–
Na	na	k7c6	na
schůzi	schůze	k1gFnSc6	schůze
ICANN	ICANN	kA	ICANN
se	s	k7c7	s
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2000	[number]	k4	2000
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
povolit	povolit	k5eAaPmF	povolit
používání	používání	k1gNnSc1	používání
kódů	kód	k1gInPc2	kód
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ISO	ISO	kA	ISO
3166-1	[number]	k4	3166-1
rezervovány	rezervován	k2eAgInPc1d1	rezervován
pro	pro	k7c4	pro
libovolné	libovolný	k2eAgNnSc4d1	libovolné
použití	použití	k1gNnSc4	použití
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
takový	takový	k3xDgInSc1	takový
existující	existující	k2eAgInSc1d1	existující
kód	kód	k1gInSc1	kód
je	být	k5eAaImIp3nS	být
eu	eu	k?	eu
vyhrazený	vyhrazený	k2eAgInSc1d1	vyhrazený
pro	pro	k7c4	pro
Evropskou	evropský	k2eAgFnSc4d1	Evropská
unii	unie	k1gFnSc4	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nadále	nadále	k6eAd1	nadále
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
ccTLD	ccTLD	k?	ccTLD
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
ISO	ISO	kA	ISO
3166-1	[number]	k4	3166-1
===	===	k?	===
</s>
</p>
<p>
<s>
<g/>
zr	zr	k?	zr
–	–	k?	–
Zair	Zair	k1gInSc1	Zair
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stát	stát	k1gInSc1	stát
s	s	k7c7	s
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc4	Kongo
používá	používat	k5eAaImIp3nS	používat
doménu	doména	k1gFnSc4	doména
.	.	kIx.	.
<g/>
cd	cd	kA	cd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Generická	generický	k2eAgFnSc1d1	generická
doména	doména	k1gFnSc1	doména
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
==	==	k?	==
</s>
</p>
<p>
<s>
Generická	generický	k2eAgFnSc1d1	generická
doména	doména	k1gFnSc1	doména
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
generic	generic	k1gMnSc1	generic
top-level	topevel	k1gMnSc1	top-level
domain	domain	k1gMnSc1	domain
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
generic	generic	k1gMnSc1	generic
TLD	TLD	kA	TLD
nebo	nebo	k8xC	nebo
gTLD	gTLD	k?	gTLD
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
doména	doména	k1gFnSc1	doména
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
(	(	kIx(	(
<g/>
alespoň	alespoň	k9	alespoň
teoreticky	teoreticky	k6eAd1	teoreticky
<g/>
)	)	kIx)	)
společná	společný	k2eAgFnSc1d1	společná
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
typ	typ	k1gInSc4	typ
subjektů	subjekt	k1gInPc2	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
nejméně	málo	k6eAd3	málo
třípísmenný	třípísmenný	k2eAgInSc1d1	třípísmenný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dělení	dělení	k1gNnSc1	dělení
gTLD	gTLD	k?	gTLD
===	===	k?	===
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
hrubý	hrubý	k2eAgInSc1d1	hrubý
překlad	překlad	k1gInSc1	překlad
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
názvosloví	názvosloví	k1gNnSc2	názvosloví
pro	pro	k7c4	pro
dělení	dělení	k1gNnSc4	dělení
těchto	tento	k3xDgFnPc2	tento
domén	doména	k1gFnPc2	doména
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neomezené	omezený	k2eNgNnSc1d1	neomezené
</s>
</p>
<p>
<s>
jejich	jejich	k3xOp3gNnSc1	jejich
použití	použití	k1gNnSc1	použití
není	být	k5eNaImIp3nS	být
nijak	nijak	k6eAd1	nijak
omezeno	omezit	k5eAaPmNgNnS	omezit
</s>
</p>
<p>
<s>
<g/>
com	com	k?	com
.	.	kIx.	.
<g/>
info	info	k1gMnSc1	info
.	.	kIx.	.
<g/>
net	net	k?	net
.	.	kIx.	.
<g/>
org	org	k?	org
.	.	kIx.	.
<g/>
eu	eu	k?	eu
.	.	kIx.	.
<g/>
pro	pro	k7c4	pro
</s>
</p>
<p>
<s>
Vyhrazené	vyhrazený	k2eAgNnSc1d1	vyhrazené
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgFnP	určit
pro	pro	k7c4	pro
specifický	specifický	k2eAgInSc4d1	specifický
účel	účel	k1gInSc4	účel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vyžadováno	vyžadován	k2eAgNnSc1d1	vyžadováno
</s>
</p>
<p>
<s>
<g/>
name	name	k6eAd1	name
</s>
</p>
<p>
<s>
Vymezené	vymezený	k2eAgInPc1d1	vymezený
</s>
</p>
<p>
<s>
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
účel	účel	k1gInSc4	účel
</s>
</p>
<p>
<s>
<g/>
biz	biz	k?	biz
.	.	kIx.	.
<g/>
int	int	k?	int
.	.	kIx.	.
<g/>
xxx	xxx	k?	xxx
</s>
</p>
<p>
<s>
Garantované	garantovaný	k2eAgNnSc1d1	garantované
</s>
</p>
<p>
<s>
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
účel	účel	k1gInSc4	účel
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
mají	mít	k5eAaImIp3nP	mít
garanta	garant	k1gMnSc4	garant
–	–	k?	–
organizaci	organizace	k1gFnSc3	organizace
stanovující	stanovující	k2eAgFnSc2d1	stanovující
podmínky	podmínka	k1gFnSc2	podmínka
registrace	registrace	k1gFnSc2	registrace
a	a	k8xC	a
dohlížející	dohlížející	k2eAgFnSc2d1	dohlížející
na	na	k7c4	na
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Označují	označovat	k5eAaImIp3nP	označovat
se	se	k3xPyFc4	se
sTLD	sTLD	k?	sTLD
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
sponsored	sponsored	k1gInSc4	sponsored
TLD	TLD	kA	TLD
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sponsor	sponsor	k1gMnSc1	sponsor
znamená	znamenat	k5eAaImIp3nS	znamenat
garant	garant	k1gMnSc1	garant
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
<g/>
aero	aero	k1gNnSc1	aero
.	.	kIx.	.
<g/>
cat	cat	k?	cat
.	.	kIx.	.
<g/>
coop	coop	k1gMnSc1	coop
.	.	kIx.	.
<g/>
jobs	jobs	k1gInSc1	jobs
.	.	kIx.	.
<g/>
mobi	mobi	k1gNnSc1	mobi
.	.	kIx.	.
<g/>
museum	museum	k1gNnSc1	museum
.	.	kIx.	.
<g/>
travel	travel	k1gInSc1	travel
</s>
</p>
<p>
<s>
Exkluzivní	exkluzivní	k2eAgFnSc1d1	exkluzivní
</s>
</p>
<p>
<s>
tyto	tento	k3xDgFnPc1	tento
domény	doména	k1gFnPc1	doména
smějí	smát	k5eAaImIp3nP	smát
používat	používat	k5eAaImF	používat
pouze	pouze	k6eAd1	pouze
subjekty	subjekt	k1gInPc4	subjekt
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
s	s	k7c7	s
většími	veliký	k2eAgFnPc7d2	veliký
či	či	k8xC	či
menšími	malý	k2eAgFnPc7d2	menší
restrikcemi	restrikce	k1gFnPc7	restrikce
</s>
</p>
<p>
<s>
<g/>
edu	edu	k?	edu
.	.	kIx.	.
<g/>
gov	gov	k?	gov
.	.	kIx.	.
<g/>
mil	míle	k1gFnPc2	míle
</s>
</p>
<p>
<s>
Infrastrukturní	infrastrukturní	k2eAgFnSc1d1	infrastrukturní
</s>
</p>
<p>
<s>
neveřejné	veřejný	k2eNgNnSc4d1	neveřejné
</s>
</p>
<p>
<s>
<g/>
arpa	arpa	k1gFnSc1	arpa
.	.	kIx.	.
<g/>
root	root	k1gInSc1	root
</s>
</p>
<p>
<s>
Zrušené	zrušený	k2eAgNnSc1d1	zrušené
</s>
</p>
<p>
<s>
<g/>
nato	nato	k6eAd1	nato
</s>
</p>
<p>
<s>
Rezervované	rezervovaný	k2eAgNnSc1d1	rezervované
</s>
</p>
<p>
<s>
<g/>
example	example	k6eAd1	example
.	.	kIx.	.
<g/>
invalid	invalid	k1gInSc1	invalid
.	.	kIx.	.
<g/>
localhost	localhost	k1gFnSc1	localhost
.	.	kIx.	.
<g/>
test	test	k1gInSc1	test
</s>
</p>
<p>
<s>
Připravované	připravovaný	k2eAgNnSc1d1	připravované
</s>
</p>
<p>
<s>
<g/>
tel	tel	kA	tel
.	.	kIx.	.
<g/>
post	post	k1gInSc4	post
.	.	kIx.	.
<g/>
free	free	k6eAd1	free
</s>
</p>
<p>
<s>
Navržené	navržený	k2eAgNnSc1d1	navržené
</s>
</p>
<p>
<s>
<g/>
asia	asia	k1gFnSc1	asia
.	.	kIx.	.
<g/>
geo	geo	k?	geo
.	.	kIx.	.
<g/>
kid	kid	k?	kid
.	.	kIx.	.
<g/>
mail	mail	k1gInSc1	mail
.	.	kIx.	.
<g/>
sco	sco	k?	sco
.	.	kIx.	.
<g/>
web	web	k1gInSc1	web
</s>
</p>
<p>
<s>
Pseudo-domény	Pseudooména	k1gFnPc1	Pseudo-doména
</s>
</p>
<p>
<s>
<g/>
bitnet	bitnet	k1gMnSc1	bitnet
.	.	kIx.	.
<g/>
csnet	csnet	k1gMnSc1	csnet
.	.	kIx.	.
<g/>
onion	onion	k1gMnSc1	onion
.	.	kIx.	.
<g/>
uucp	uucp	k1gMnSc1	uucp
</s>
</p>
<p>
<s>
===	===	k?	===
Seznam	seznam	k1gInSc1	seznam
existujících	existující	k2eAgFnPc2d1	existující
gTLD	gTLD	k?	gTLD
===	===	k?	===
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tyto	tento	k3xDgInPc1	tento
generické	generický	k2eAgInPc1d1	generický
TLD	TLD	kA	TLD
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
<g/>
aero	aero	k1gNnSc1	aero
–	–	k?	–
pro	pro	k7c4	pro
letecký	letecký	k2eAgInSc4d1	letecký
průmysl	průmysl	k1gInSc4	průmysl
</s>
</p>
<p>
<s>
<g/>
biz	biz	k?	biz
–	–	k?	–
pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
(	(	kIx(	(
<g/>
business	business	k1gInSc4	business
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
<g/>
cat	cat	k?	cat
–	–	k?	–
pro	pro	k7c4	pro
katalánskou	katalánský	k2eAgFnSc4d1	katalánská
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc4d1	kulturní
komunitu	komunita	k1gFnSc4	komunita
(	(	kIx(	(
<g/>
catalan	catalan	k1gInSc4	catalan
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
<g/>
com	com	k?	com
–	–	k?	–
pro	pro	k7c4	pro
komerční	komerční	k2eAgInSc4d1	komerční
(	(	kIx(	(
<g/>
ziskové	ziskový	k2eAgFnPc1d1	zisková
<g/>
)	)	kIx)	)
organizace	organizace	k1gFnPc1	organizace
(	(	kIx(	(
<g/>
commercial	commercial	k1gInSc1	commercial
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
<g/>
coop	coop	k1gInSc1	coop
–	–	k?	–
pro	pro	k7c4	pro
družstva	družstvo	k1gNnPc4	družstvo
a	a	k8xC	a
družstevní	družstevní	k2eAgNnSc4d1	družstevní
sdružení	sdružení	k1gNnSc4	sdružení
(	(	kIx(	(
<g/>
cooperative	cooperativ	k1gInSc5	cooperativ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
<g/>
edu	edu	k?	edu
–	–	k?	–
pro	pro	k7c4	pro
vzdělávací	vzdělávací	k2eAgFnPc4d1	vzdělávací
instituce	instituce	k1gFnPc4	instituce
v	v	k7c6	v
USA	USA	kA	USA
(	(	kIx(	(
<g/>
education	education	k1gInSc1	education
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
<g/>
gov	gov	k?	gov
–	–	k?	–
pro	pro	k7c4	pro
vládu	vláda	k1gFnSc4	vláda
USA	USA	kA	USA
(	(	kIx(	(
<g/>
government	government	k1gMnSc1	government
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
<g/>
info	info	k6eAd1	info
–	–	k?	–
pro	pro	k7c4	pro
servery	server	k1gInPc4	server
s	s	k7c7	s
informacemi	informace	k1gFnPc7	informace
(	(	kIx(	(
<g/>
prakticky	prakticky	k6eAd1	prakticky
neomezené	omezený	k2eNgNnSc1d1	neomezené
využití	využití	k1gNnSc1	využití
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
<g/>
int	int	k?	int
–	–	k?	–
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
organizace	organizace	k1gFnPc4	organizace
založené	založený	k2eAgFnPc4d1	založená
na	na	k7c6	na
základě	základ	k1gInSc6	základ
mezistátních	mezistátní	k2eAgFnPc2d1	mezistátní
smluv	smlouva	k1gFnPc2	smlouva
(	(	kIx(	(
<g/>
international	internationat	k5eAaImAgInS	internationat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
<g/>
jobs	jobs	k1gInSc1	jobs
–	–	k?	–
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
lidských	lidský	k2eAgInPc2d1	lidský
zdrojů	zdroj	k1gInPc2	zdroj
</s>
</p>
<p>
<s>
<g/>
mil	míle	k1gFnPc2	míle
–	–	k?	–
pro	pro	k7c4	pro
armádu	armáda	k1gFnSc4	armáda
USA	USA	kA	USA
(	(	kIx(	(
<g/>
military	militar	k1gInPc1	militar
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
<g/>
mobi	mob	k1gFnSc3	mob
–	–	k?	–
pro	pro	k7c4	pro
poskytovatele	poskytovatel	k1gMnPc4	poskytovatel
mobilních	mobilní	k2eAgInPc2d1	mobilní
produktů	produkt	k1gInPc2	produkt
(	(	kIx(	(
<g/>
mobile	mobile	k1gNnSc1	mobile
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
<g/>
museum	museum	k1gNnSc1	museum
–	–	k?	–
pro	pro	k7c4	pro
muzea	muzeum	k1gNnPc4	muzeum
</s>
</p>
<p>
<s>
<g/>
name	name	k1gFnSc1	name
–	–	k?	–
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnPc4d1	osobní
stránky	stránka	k1gFnPc4	stránka
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
</s>
</p>
<p>
<s>
<g/>
net	net	k?	net
–	–	k?	–
pro	pro	k7c4	pro
organizace	organizace	k1gFnPc4	organizace
zajišťující	zajišťující	k2eAgInSc4d1	zajišťující
provoz	provoz	k1gInSc4	provoz
sítě	síť	k1gFnSc2	síť
(	(	kIx(	(
<g/>
network	network	k1gInSc1	network
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
<g/>
org	org	k?	org
–	–	k?	–
pro	pro	k7c4	pro
neziskové	ziskový	k2eNgFnPc4d1	nezisková
organizace	organizace	k1gFnPc4	organizace
(	(	kIx(	(
<g/>
organization	organization	k1gInSc1	organization
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
<g/>
pro	pro	k7c4	pro
–	–	k?	–
pro	pro	k7c4	pro
profesionály	profesionál	k1gMnPc4	profesionál
<g/>
,	,	kIx,	,
profesní	profesní	k2eAgNnSc4d1	profesní
sdružení	sdružení	k1gNnSc4	sdružení
apod.	apod.	kA	apod.
(	(	kIx(	(
<g/>
professional	professionat	k5eAaImAgMnS	professionat
<g/>
,	,	kIx,	,
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
bez	bez	k7c2	bez
omezení	omezení	k1gNnSc2	omezení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
<g/>
travel	travel	k1gInSc1	travel
–	–	k?	–
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
</s>
</p>
<p>
<s>
<g/>
capital	capital	k1gMnSc1	capital
</s>
</p>
<p>
<s>
<g/>
expertVšeobecně	expertVšeobecně	k6eAd1	expertVšeobecně
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
již	již	k6eAd1	již
velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k6eAd1	mnoho
a	a	k8xC	a
přibývají	přibývat	k5eAaImIp3nP	přibývat
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
===	===	k?	===
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
(	(	kIx(	(
<g/>
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
existovalo	existovat	k5eAaImAgNnS	existovat
pouze	pouze	k6eAd1	pouze
následujících	následující	k2eAgInPc2d1	následující
šest	šest	k4xCc4	šest
gTLD	gTLD	k?	gTLD
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
<g/>
com	com	k?	com
–	–	k?	–
komerční	komerční	k2eAgInPc4d1	komerční
subjekty	subjekt	k1gInPc4	subjekt
</s>
</p>
<p>
<s>
<g/>
edu	edu	k?	edu
–	–	k?	–
vzdělávací	vzdělávací	k2eAgFnSc2d1	vzdělávací
organizace	organizace	k1gFnSc2	organizace
USA	USA	kA	USA
</s>
</p>
<p>
<s>
<g/>
gov	gov	k?	gov
–	–	k?	–
vládní	vládní	k2eAgInPc4d1	vládní
orgány	orgán	k1gInPc4	orgán
USA	USA	kA	USA
</s>
</p>
<p>
<s>
<g/>
net	net	k?	net
–	–	k?	–
subjekty	subjekt	k1gInPc1	subjekt
orientované	orientovaný	k2eAgInPc1d1	orientovaný
na	na	k7c4	na
poskytování	poskytování	k1gNnSc4	poskytování
síťových	síťový	k2eAgFnPc2d1	síťová
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
</s>
</p>
<p>
<s>
<g/>
org	org	k?	org
–	–	k?	–
nekomerční	komerční	k2eNgFnSc2d1	nekomerční
organizace	organizace	k1gFnSc2	organizace
a	a	k8xC	a
občanská	občanský	k2eAgNnPc4d1	občanské
sdružení	sdružení	k1gNnPc4	sdružení
</s>
</p>
<p>
<s>
<g/>
mil	míle	k1gFnPc2	míle
–	–	k?	–
armáda	armáda	k1gFnSc1	armáda
USA	USA	kA	USA
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
generických	generický	k2eAgFnPc2d1	generická
domén	doména	k1gFnPc2	doména
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
domény	doména	k1gFnSc2	doména
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
net	net	k?	net
a	a	k8xC	a
.	.	kIx.	.
<g/>
org	org	k?	org
zcela	zcela	k6eAd1	zcela
otevřené	otevřený	k2eAgNnSc1d1	otevřené
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
kdokoli	kdokoli	k3yInSc1	kdokoli
si	se	k3xPyFc3	se
pod	pod	k7c7	pod
nimi	on	k3xPp3gInPc7	on
může	moct	k5eAaImIp3nS	moct
zaregistrovat	zaregistrovat	k5eAaPmF	zaregistrovat
doménu	doména	k1gFnSc4	doména
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
zamýšlenému	zamýšlený	k2eAgInSc3d1	zamýšlený
cíli	cíl	k1gInSc3	cíl
dané	daný	k2eAgFnSc2d1	daná
domény	doména	k1gFnSc2	doména
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
existují	existovat	k5eAaImIp3nP	existovat
například	například	k6eAd1	například
komerční	komerční	k2eAgFnPc1d1	komerční
firmy	firma	k1gFnPc1	firma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
používají	používat	k5eAaImIp3nP	používat
doménové	doménový	k2eAgNnSc4d1	doménové
jméno	jméno	k1gNnSc4	jméno
s	s	k7c7	s
TLD	TLD	kA	TLD
.	.	kIx.	.
<g/>
net	net	k?	net
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1988	[number]	k4	1988
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
sedmá	sedmý	k4xOgFnSc1	sedmý
gTLD	gTLD	k?	gTLD
<g/>
:	:	kIx,	:
.	.	kIx.	.
<g/>
int	int	k?	int
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgNnPc1	ten
slouží	sloužit	k5eAaImIp3nP	sloužit
mezinárodním	mezinárodní	k2eAgFnPc3d1	mezinárodní
organizacím	organizace	k1gFnPc3	organizace
založeným	založený	k2eAgFnPc3d1	založená
na	na	k7c6	na
základě	základ	k1gInSc6	základ
mezistátních	mezistátní	k2eAgFnPc2d1	mezistátní
smluv	smlouva	k1gFnPc2	smlouva
(	(	kIx(	(
<g/>
např.	např.	kA	např.
eu	eu	k?	eu
<g/>
.	.	kIx.	.
<g/>
int	int	k?	int
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
jejího	její	k3xOp3gInSc2	její
vzniku	vznik	k1gInSc2	vznik
byl	být	k5eAaImAgInS	být
požadavek	požadavek	k1gInSc1	požadavek
NATO	NATO	kA	NATO
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
doménu	doména	k1gFnSc4	doména
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
domény	doména	k1gFnSc2	doména
byly	být	k5eAaImAgFnP	být
také	také	k6eAd1	také
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
některé	některý	k3yIgFnPc1	některý
infrastrukturní	infrastrukturní	k2eAgFnPc1d1	infrastrukturní
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
databáze	databáze	k1gFnPc1	databáze
Internetu	Internet	k1gInSc2	Internet
jako	jako	k8xS	jako
např.	např.	kA	např.
ip	ip	k?	ip
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
int	int	k?	int
<g/>
,	,	kIx,	,
sloužící	sloužící	k1gFnSc1	sloužící
pro	pro	k7c4	pro
zpětný	zpětný	k2eAgInSc4d1	zpětný
překlad	překlad	k1gInSc4	překlad
adres	adresa	k1gFnPc2	adresa
IPv	IPv	k1gMnPc2	IPv
<g/>
6	[number]	k4	6
systémem	systém	k1gInSc7	systém
DNS	DNS	kA	DNS
(	(	kIx(	(
<g/>
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
in-addr	inddr	k1gInSc1	in-addr
<g/>
.	.	kIx.	.
<g/>
arpa	arpa	k1gFnSc1	arpa
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
IPv	IPv	k1gFnSc4	IPv
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2000	[number]	k4	2000
však	však	k8xC	však
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc1	všechen
další	další	k2eAgFnPc1d1	další
podobné	podobný	k2eAgFnPc1d1	podobná
databáze	databáze	k1gFnPc1	databáze
budou	být	k5eAaImBp3nP	být
zakládány	zakládat	k5eAaImNgFnP	zakládat
ve	v	k7c6	v
specializované	specializovaný	k2eAgFnSc6d1	specializovaná
infrastrukturní	infrastrukturní	k2eAgFnSc6d1	infrastrukturní
doméně	doména	k1gFnSc6	doména
.	.	kIx.	.
<g/>
arpa	arpa	k6eAd1	arpa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
zřejmá	zřejmý	k2eAgFnSc1d1	zřejmá
potřeba	potřeba	k1gFnSc1	potřeba
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
doménových	doménový	k2eAgNnPc2d1	doménové
jmen	jméno	k1gNnPc2	jméno
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
gTLD	gTLD	k?	gTLD
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
požadavek	požadavek	k1gInSc4	požadavek
na	na	k7c4	na
zavedení	zavedení	k1gNnSc4	zavedení
nových	nový	k2eAgFnPc2d1	nová
gTLD	gTLD	k?	gTLD
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1997	[number]	k4	1997
bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
sedm	sedm	k4xCc1	sedm
nových	nový	k2eAgFnPc2d1	nová
gTLD	gTLD	k?	gTLD
(	(	kIx(	(
<g/>
.	.	kIx.	.
<g/>
arts	arts	k1gInSc1	arts
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
firm	firm	k1gInSc1	firm
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
info	info	k1gMnSc1	info
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
nom	nom	k?	nom
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
rec	rec	k?	rec
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
store	stor	k1gInSc5	stor
a	a	k8xC	a
.	.	kIx.	.
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
pro	pro	k7c4	pro
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
americké	americký	k2eAgFnSc2d1	americká
vlády	vláda	k1gFnSc2	vláda
proveden	provést	k5eAaPmNgInS	provést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1998	[number]	k4	1998
se	s	k7c7	s
správou	správa	k1gFnSc7	správa
doménových	doménový	k2eAgNnPc2d1	doménové
jmen	jméno	k1gNnPc2	jméno
zabývá	zabývat	k5eAaImIp3nS	zabývat
organizace	organizace	k1gFnSc1	organizace
ICANN	ICANN	kA	ICANN
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
po	po	k7c6	po
veřejné	veřejný	k2eAgFnSc6d1	veřejná
debatě	debata	k1gFnSc6	debata
přistoupila	přistoupit	k5eAaPmAgFnS	přistoupit
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2000	[number]	k4	2000
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
následujících	následující	k2eAgFnPc2d1	následující
sedmi	sedm	k4xCc2	sedm
nových	nový	k2eAgFnPc2d1	nová
gTLD	gTLD	k?	gTLD
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
<g/>
aero	aero	k1gNnSc1	aero
</s>
</p>
<p>
<s>
<g/>
biz	biz	k?	biz
</s>
</p>
<p>
<s>
<g/>
coop	coop	k1gMnSc1	coop
</s>
</p>
<p>
<s>
<g/>
info	info	k6eAd1	info
</s>
</p>
<p>
<s>
<g/>
museum	museum	k1gNnSc1	museum
</s>
</p>
<p>
<s>
<g/>
name	name	k6eAd1	name
</s>
</p>
<p>
<s>
<g/>
proV	proV	k?	proV
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
vytvoření	vytvoření	k1gNnSc3	vytvoření
dalších	další	k2eAgFnPc2d1	další
generických	generický	k2eAgFnPc2d1	generická
domén	doména	k1gFnPc2	doména
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Navrhovány	navrhovat	k5eAaImNgFnP	navrhovat
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgFnPc1d1	následující
domény	doména	k1gFnPc1	doména
<g/>
:	:	kIx,	:
.	.	kIx.	.
<g/>
asia	asia	k1gFnSc1	asia
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
geo	geo	k?	geo
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
kid	kid	k?	kid
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
mail	mail	k1gInSc1	mail
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
post	post	k1gInSc4	post
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
sco	sco	k?	sco
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
tel	tel	kA	tel
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
web	web	k1gInSc1	web
a	a	k8xC	a
.	.	kIx.	.
<g/>
xxx	xxx	k?	xxx
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byly	být	k5eAaImAgFnP	být
schváleny	schválit	k5eAaPmNgFnP	schválit
první	první	k4xOgFnPc1	první
nelatinské	latinský	k2eNgFnPc1d1	latinský
domény	doména	k1gFnPc1	doména
ش	ش	k?	ش
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
о	о	k?	о
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
с	с	k?	с
a	a	k8xC	a
.	.	kIx.	.
<g/>
游	游	k?	游
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Firemní	firemní	k2eAgFnSc3d1	firemní
gTLD	gTLD	k?	gTLD
===	===	k?	===
</s>
</p>
<p>
<s>
Nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
obchodních	obchodní	k2eAgFnPc2d1	obchodní
značek	značka	k1gFnPc2	značka
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
registrovat	registrovat	k5eAaBmF	registrovat
si	se	k3xPyFc3	se
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
gTLD	gTLD	k?	gTLD
(	(	kIx(	(
<g/>
generickou	generický	k2eAgFnSc7d1	generická
TLD	TLD	kA	TLD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
unikátní	unikátní	k2eAgFnSc4d1	unikátní
doménu	doména	k1gFnSc4	doména
s	s	k7c7	s
celosvětovým	celosvětový	k2eAgInSc7d1	celosvětový
dosahem	dosah	k1gInSc7	dosah
<g/>
.	.	kIx.	.
</s>
<s>
IANA	Ianus	k1gMnSc4	Ianus
si	se	k3xPyFc3	se
klade	klást	k5eAaImIp3nS	klást
několik	několik	k4yIc4	několik
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
splnění	splnění	k1gNnSc2	splnění
registračního	registrační	k2eAgInSc2d1	registrační
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
shodu	shoda	k1gFnSc4	shoda
názvu	název	k1gInSc2	název
s	s	k7c7	s
provozovatelem	provozovatel	k1gMnSc7	provozovatel
<g/>
,	,	kIx,	,
technickou	technický	k2eAgFnSc4d1	technická
<g/>
,	,	kIx,	,
právní	právní	k2eAgFnSc4d1	právní
a	a	k8xC	a
smluvní	smluvní	k2eAgFnSc4d1	smluvní
shodu	shoda	k1gFnSc4	shoda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
splnění	splnění	k1gNnSc6	splnění
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
takovou	takový	k3xDgFnSc4	takový
TLD	TLD	kA	TLD
získat	získat	k5eAaPmF	získat
a	a	k8xC	a
provozovat	provozovat	k5eAaImF	provozovat
<g/>
:	:	kIx,	:
IANA	Ianus	k1gMnSc4	Ianus
je	být	k5eAaImIp3nS	být
zahrne	zahrnout	k5eAaPmIp3nS	zahrnout
do	do	k7c2	do
DNS	DNS	kA	DNS
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
pro	pro	k7c4	pro
banku	banka	k1gFnSc4	banka
BNP	BNP	kA	BNP
Paribas	Paribasa	k1gFnPc2	Paribasa
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
schválené	schválený	k2eAgFnSc2d1	schválená
žádosti	žádost	k1gFnSc2	žádost
zavedena	zaveden	k2eAgMnSc4d1	zaveden
gTLD	gTLD	k?	gTLD
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
původní	původní	k2eAgInSc1d1	původní
URL	URL	kA	URL
http://bnpparibas.com	[url]	k1gInSc1	http://bnpparibas.com
je	být	k5eAaImIp3nS	být
přesměrována	přesměrován	k2eAgFnSc1d1	přesměrována
na	na	k7c4	na
https://group.bnpparibas/.	[url]	k?	https://group.bnpparibas/.
Podstatou	podstata	k1gFnSc7	podstata
firemních	firemní	k2eAgFnPc2d1	firemní
gTLD	gTLD	k?	gTLD
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
garantem	garant	k1gMnSc7	garant
takové	takový	k3xDgFnPc4	takový
gTLD	gTLD	k?	gTLD
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
majitel	majitel	k1gMnSc1	majitel
značky	značka	k1gFnPc4	značka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
případě	případ	k1gInSc6	případ
.	.	kIx.	.
<g/>
bnpparibas	bnpparibas	k1gInSc1	bnpparibas
skutečně	skutečně	k6eAd1	skutečně
je	být	k5eAaImIp3nS	být
BNP	BNP	kA	BNP
Paribas	Paribas	k1gInSc1	Paribas
<g/>
.	.	kIx.	.
<g/>
A	a	k9	a
pro	pro	k7c4	pro
účel	účel	k1gInSc4	účel
ochrany	ochrana	k1gFnSc2	ochrana
značek	značka	k1gFnPc2	značka
by	by	kYmCp3nS	by
registrovaná	registrovaný	k2eAgFnSc1d1	registrovaná
gTLD	gTLD	k?	gTLD
neměla	mít	k5eNaImAgFnS	mít
být	být	k5eAaImF	být
zavádějící	zavádějící	k2eAgFnSc1d1	zavádějící
<g/>
:	:	kIx,	:
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
zda	zda	k8xS	zda
IANA	Ianus	k1gMnSc2	Ianus
má	mít	k5eAaImIp3nS	mít
povinnost	povinnost	k1gFnSc4	povinnost
registrovat	registrovat	k5eAaBmF	registrovat
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
formálně	formálně	k6eAd1	formálně
správně	správně	k6eAd1	správně
podanou	podaný	k2eAgFnSc4d1	podaná
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
gTLD	gTLD	k?	gTLD
<g/>
,	,	kIx,	,
či	či	k8xC	či
zda	zda	k8xS	zda
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
libovůli	libovůle	k1gFnSc4	libovůle
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
názorech	názor	k1gInPc6	názor
členů	člen	k1gMnPc2	člen
schvalovací	schvalovací	k2eAgFnSc1d1	schvalovací
komise	komise	k1gFnSc1	komise
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografické	geografický	k2eAgInPc1d1	geografický
TLD	TLD	kA	TLD
==	==	k?	==
</s>
</p>
<p>
<s>
TLD	TLD	kA	TLD
pro	pro	k7c4	pro
určité	určitý	k2eAgNnSc4d1	určité
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
stránky	stránka	k1gFnPc4	stránka
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
oblasti	oblast	k1gFnPc4	oblast
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
<g/>
london	london	k1gInSc1	london
pro	pro	k7c4	pro
Londýn	Londýn	k1gInSc4	Londýn
</s>
</p>
<p>
<s>
<g/>
berlin	berlina	k1gFnPc2	berlina
pro	pro	k7c4	pro
Berlín	Berlín	k1gInSc4	Berlín
</s>
</p>
<p>
<s>
<g/>
amsterdam	amsterdam	k6eAd1	amsterdam
pro	pro	k7c4	pro
Amsterdam	Amsterdam	k1gInSc4	Amsterdam
</s>
</p>
<p>
<s>
<g/>
wien	wien	k1gInSc1	wien
pro	pro	k7c4	pro
Vídeň	Vídeň	k1gFnSc4	Vídeň
</s>
</p>
<p>
<s>
<g/>
nyc	nyc	k?	nyc
pro	pro	k7c4	pro
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
</s>
</p>
<p>
<s>
<g/>
cymru	cymra	k1gFnSc4	cymra
pro	pro	k7c4	pro
stránky	stránka	k1gFnPc4	stránka
ve	v	k7c6	v
velštině	velština	k1gFnSc6	velština
</s>
</p>
<p>
<s>
<g/>
quebec	quebec	k1gInSc1	quebec
pro	pro	k7c4	pro
stránky	stránka	k1gFnPc4	stránka
z	z	k7c2	z
Québecu	Québecus	k1gInSc2	Québecus
</s>
</p>
<p>
<s>
<g/>
nrw	nrw	k?	nrw
pro	pro	k7c4	pro
stránky	stránka	k1gFnPc4	stránka
ze	z	k7c2	z
Severního	severní	k2eAgNnSc2d1	severní
Porýní-Vestfálska	Porýní-Vestfálsko	k1gNnSc2	Porýní-Vestfálsko
</s>
</p>
<p>
<s>
<g/>
cat	cat	k?	cat
pro	pro	k7c4	pro
stránky	stránka	k1gFnPc4	stránka
v	v	k7c6	v
katalátštiněa	katalátštiněa	k6eAd1	katalátštiněa
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
</s>
</p>
<p>
<s>
==	==	k?	==
Ostatní	ostatní	k2eAgMnSc1d1	ostatní
TLD	TLD	kA	TLD
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Historické	historický	k2eAgInPc1d1	historický
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
TLD	TLD	kA	TLD
nato	nato	k6eAd1	nato
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
Severoatlantickou	severoatlantický	k2eAgFnSc7d1	Severoatlantická
aliancí	aliance	k1gFnSc7	aliance
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
tehdy	tehdy	k6eAd1	tehdy
existujících	existující	k2eAgInPc2d1	existující
TLD	TLD	kA	TLD
nevyhovovala	vyhovovat	k5eNaImAgFnS	vyhovovat
statutu	statut	k1gInSc3	statut
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
však	však	k9	však
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
TLD	TLD	kA	TLD
int	int	k?	int
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
právě	právě	k6eAd1	právě
mezinárodním	mezinárodní	k2eAgFnPc3d1	mezinárodní
organizacím	organizace	k1gFnPc3	organizace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
NATO	NATO	kA	NATO
dnes	dnes	k6eAd1	dnes
používá	používat	k5eAaImIp3nS	používat
doménu	doména	k1gFnSc4	doména
nato	nato	k6eAd1	nato
<g/>
.	.	kIx.	.
<g/>
int	int	k?	int
<g/>
.	.	kIx.	.
</s>
<s>
Nepoužívaná	používaný	k2eNgFnSc1d1	nepoužívaná
TLD	TLD	kA	TLD
nato	nato	k6eAd1	nato
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
až	až	k9	až
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
Internet	Internet	k1gInSc1	Internet
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
počítačových	počítačový	k2eAgFnPc2d1	počítačová
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Počítače	počítač	k1gInPc1	počítač
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nebyly	být	k5eNaImAgFnP	být
propojené	propojený	k2eAgInPc1d1	propojený
do	do	k7c2	do
Internetu	Internet	k1gInSc2	Internet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
sítí	síť	k1gFnPc2	síť
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Bitnet	Bitnet	k1gInSc1	Bitnet
či	či	k8xC	či
UUCP	UUCP	kA	UUCP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mohly	moct	k5eAaImAgInP	moct
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
speciálních	speciální	k2eAgFnPc2d1	speciální
internetových	internetový	k2eAgFnPc2d1	internetová
bran	brána	k1gFnPc2	brána
posílat	posílat	k5eAaImF	posílat
e-maily	eail	k1gInPc4	e-mail
i	i	k9	i
do	do	k7c2	do
Internetu	Internet	k1gInSc2	Internet
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
samy	sám	k3xTgFnPc1	sám
nebyly	být	k5eNaImAgFnP	být
připojeny	připojit	k5eAaPmNgFnP	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
takových	takový	k3xDgInPc2	takový
počítačů	počítač	k1gInPc2	počítač
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používaly	používat	k5eAaImAgFnP	používat
pseudodomény	pseudodoména	k1gFnPc1	pseudodoména
jako	jako	k8xS	jako
např.	např.	kA	např.
.	.	kIx.	.
<g/>
bitnet	bitnet	k1gMnSc1	bitnet
či	či	k8xC	či
.	.	kIx.	.
<g/>
uucp	uucp	k1gInSc1	uucp
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
domény	doména	k1gFnPc1	doména
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
skutečně	skutečně	k6eAd1	skutečně
neexistovaly	existovat	k5eNaImAgInP	existovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
systému	systém	k1gInSc2	systém
DNS	DNS	kA	DNS
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
používají	používat	k5eAaImIp3nP	používat
tyto	tento	k3xDgFnPc4	tento
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
sítě	síť	k1gFnPc4	síť
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
již	již	k6eAd1	již
používají	používat	k5eAaImIp3nP	používat
standardní	standardní	k2eAgFnPc4d1	standardní
internetové	internetový	k2eAgFnPc4d1	internetová
domény	doména	k1gFnPc4	doména
a	a	k8xC	a
tyto	tento	k3xDgFnPc1	tento
pseudodomény	pseudodoména	k1gFnPc1	pseudodoména
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
jen	jen	k6eAd1	jen
historickým	historický	k2eAgInSc7d1	historický
reliktem	relikt	k1gInSc7	relikt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rezervované	rezervovaný	k2eAgMnPc4d1	rezervovaný
===	===	k?	===
</s>
</p>
<p>
<s>
RFC	RFC	kA	RFC
2606	[number]	k4	2606
specifikuje	specifikovat	k5eAaBmIp3nS	specifikovat
následující	následující	k2eAgFnSc1d1	následující
čtyři	čtyři	k4xCgMnPc4	čtyři
speciálních	speciální	k2eAgFnPc2d1	speciální
doménová	doménový	k2eAgNnPc4d1	doménové
jména	jméno	k1gNnPc4	jméno
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
vyhrazena	vyhradit	k5eAaPmNgNnP	vyhradit
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
účely	účel	k1gInPc4	účel
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zaručeno	zaručit	k5eAaPmNgNnS	zaručit
<g/>
,	,	kIx,	,
že	že	k8xS	že
takové	takový	k3xDgFnPc4	takový
domény	doména	k1gFnPc4	doména
nikdy	nikdy	k6eAd1	nikdy
nebudou	být	k5eNaImBp3nP	být
v	v	k7c6	v
globálním	globální	k2eAgInSc6d1	globální
doménovém	doménový	k2eAgInSc6d1	doménový
stromu	strom	k1gInSc6	strom
existovat	existovat	k5eAaImF	existovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
<g/>
example	example	k6eAd1	example
–	–	k?	–
vyhrazeno	vyhradit	k5eAaPmNgNnS	vyhradit
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
příkladech	příklad	k1gInPc6	příklad
v	v	k7c6	v
dokumentaci	dokumentace	k1gFnSc6	dokumentace
apod	apod	kA	apod
</s>
</p>
<p>
<s>
<g/>
invalid	invalid	k1gInSc1	invalid
–	–	k?	–
vyhrazeno	vyhradit	k5eAaPmNgNnS	vyhradit
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
jako	jako	k8xC	jako
evidentně	evidentně	k6eAd1	evidentně
neexistující	existující	k2eNgNnSc4d1	neexistující
jméno	jméno	k1gNnSc4	jméno
</s>
</p>
<p>
<s>
<g/>
localhost	localhost	k1gFnSc1	localhost
–	–	k?	–
vyhrazeno	vyhradit	k5eAaPmNgNnS	vyhradit
pro	pro	k7c4	pro
tradiční	tradiční	k2eAgNnSc4d1	tradiční
použití	použití	k1gNnSc4	použití
termínu	termín	k1gInSc2	termín
localhost	localhost	k1gFnSc1	localhost
</s>
</p>
<p>
<s>
<g/>
test	test	k1gInSc1	test
–	–	k?	–
vyhrazeno	vyhradit	k5eAaPmNgNnS	vyhradit
pro	pro	k7c4	pro
testovací	testovací	k2eAgInPc4d1	testovací
účely	účel	k1gInPc4	účel
</s>
</p>
<p>
<s>
===	===	k?	===
Alternativní	alternativní	k2eAgInPc1d1	alternativní
===	===	k?	===
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgInPc1d1	další
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
na	na	k7c4	na
IANA	Ianus	k1gMnSc4	Ianus
<g/>
,	,	kIx,	,
používající	používající	k2eAgMnSc1d1	používající
Alternativní	alternativní	k2eAgMnSc1d1	alternativní
DNS	DNS	kA	DNS
kořen	kořen	k1gInSc4	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
strom	strom	k1gInSc1	strom
může	moct	k5eAaImIp3nS	moct
provozovat	provozovat	k5eAaImF	provozovat
kdokoli	kdokoli	k3yInSc1	kdokoli
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
DNS	DNS	kA	DNS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
síti	síť	k1gFnSc6	síť
Internet	Internet	k1gInSc1	Internet
byla	být	k5eAaImAgNnP	být
sítí	síť	k1gFnPc2	síť
alternativních	alternativní	k2eAgInPc2d1	alternativní
nameserverů	nameserver	k1gInPc2	nameserver
provozována	provozovat	k5eAaImNgFnS	provozovat
například	například	k6eAd1	například
doména	doména	k1gFnSc1	doména
.	.	kIx.	.
<g/>
biz	biz	k?	biz
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
svým	svůj	k3xOyFgNnSc7	svůj
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
schválením	schválení	k1gNnSc7	schválení
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
komunitní	komunitní	k2eAgFnSc1d1	komunitní
síť	síť	k1gFnSc1	síť
CZFree	CZFre	k1gFnSc2	CZFre
<g/>
.	.	kIx.	.
<g/>
Net	Net	k1gMnSc1	Net
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
infrastruktuře	infrastruktura	k1gFnSc6	infrastruktura
používá	používat	k5eAaImIp3nS	používat
top	topit	k5eAaImRp2nS	topit
level	level	k1gInSc4	level
doménu	doména	k1gFnSc4	doména
.	.	kIx.	.
<g/>
czf	czf	k?	czf
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
internetová	internetový	k2eAgFnSc1d1	internetová
doména	doména	k1gFnSc1	doména
</s>
</p>
<p>
<s>
ISO	ISO	kA	ISO
3166-1	[number]	k4	3166-1
–	–	k?	–
Seznam	seznam	k1gInSc1	seznam
dvoupísmenných	dvoupísmenný	k2eAgFnPc2d1	dvoupísmenná
zkratek	zkratka	k1gFnPc2	zkratka
</s>
</p>
<p>
<s>
seznam	seznam	k1gInSc1	seznam
internetových	internetový	k2eAgFnPc2d1	internetová
domén	doména	k1gFnPc2	doména
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
</s>
</p>
<p>
<s>
internetová	internetový	k2eAgFnSc1d1	internetová
horečka	horečka	k1gFnSc1	horečka
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
doména	doména	k1gFnSc1	doména
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
Zelená	zelený	k2eAgFnSc1d1	zelená
pro	pro	k7c4	pro
čtyři	čtyři	k4xCgFnPc4	čtyři
nové	nový	k2eAgFnPc4d1	nová
generické	generický	k2eAgFnPc4d1	generická
domény	doména	k1gFnPc4	doména
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
IANA	Ianus	k1gMnSc4	Ianus
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
seznam	seznam	k1gInSc1	seznam
ccTLD	ccTLD	k?	ccTLD
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ICANN	ICANN	kA	ICANN
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
RFC	RFC	kA	RFC
1591	[number]	k4	1591
–	–	k?	–
Domain	Domain	k2eAgInSc4d1	Domain
Name	Name	k1gInSc4	Name
System	Syst	k1gInSc7	Syst
Structure	Structur	k1gMnSc5	Structur
and	and	k?	and
Delegation	Delegation	k1gInSc1	Delegation
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
seznam	seznam	k1gInSc1	seznam
ISO	ISO	kA	ISO
3166	[number]	k4	3166
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-alpha-	lpha-	k?	-alpha-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
iso	iso	k?	iso
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
domén	doména	k1gFnPc2	doména
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
lupa	lupa	k1gFnSc1	lupa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
