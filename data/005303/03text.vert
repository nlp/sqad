<s>
Nicholas	Nicholas	k1gMnSc1	Nicholas
Rush	Rusha	k1gFnPc2	Rusha
je	být	k5eAaImIp3nS	být
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
postava	postava	k1gFnSc1	postava
sci-fi	scii	k1gFnSc2	sci-fi
seriálu	seriál	k1gInSc2	seriál
Stargate	Stargat	k1gInSc5	Stargat
Universe	Universe	k1gFnPc1	Universe
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
roli	role	k1gFnSc4	role
představuje	představovat	k5eAaImIp3nS	představovat
skotský	skotský	k2eAgMnSc1d1	skotský
herec	herec	k1gMnSc1	herec
Robert	Robert	k1gMnSc1	Robert
Carlyle	Carlyl	k1gInSc5	Carlyl
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Nicholas	Nicholas	k1gMnSc1	Nicholas
Rush	Rush	k1gMnSc1	Rush
je	být	k5eAaImIp3nS	být
vedoucí	vedoucí	k1gMnSc1	vedoucí
projektu	projekt	k1gInSc2	projekt
Ikarus	Ikarus	k1gMnSc1	Ikarus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
manželky	manželka	k1gFnSc2	manželka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
posedlý	posedlý	k2eAgMnSc1d1	posedlý
odhalením	odhalení	k1gNnSc7	odhalení
záhady	záhada	k1gFnSc2	záhada
devátého	devátý	k4xOgInSc2	devátý
symbolu	symbol	k1gInSc2	symbol
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
a	a	k8xC	a
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
ji	on	k3xPp3gFnSc4	on
chtěl	chtít	k5eAaImAgMnS	chtít
vyřešit	vyřešit	k5eAaPmF	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vytočením	vytočení	k1gNnSc7	vytočení
devítimístné	devítimístný	k2eAgFnSc2d1	devítimístná
adresy	adresa	k1gFnSc2	adresa
mu	on	k3xPp3gMnSc3	on
nakonec	nakonec	k9	nakonec
pomůže	pomoct	k5eAaPmIp3nS	pomoct
Eli	Eli	k1gFnSc4	Eli
Wallace	Wallace	k1gFnSc2	Wallace
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
Rush	Rush	k1gMnSc1	Rush
získal	získat	k5eAaPmAgMnS	získat
pro	pro	k7c4	pro
projekt	projekt	k1gInSc4	projekt
Ikarus	Ikarus	k1gMnSc1	Ikarus
<g/>
.	.	kIx.	.
</s>
<s>
Zásluhou	zásluha	k1gFnSc7	zásluha
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Rushe	Rushe	k1gFnSc1	Rushe
se	se	k3xPyFc4	se
ocitá	ocitat	k5eAaImIp3nS	ocitat
většina	většina	k1gFnSc1	většina
personálu	personál	k1gInSc2	personál
základny	základna	k1gFnSc2	základna
Ikarus	Ikarus	k1gMnSc1	Ikarus
ve	v	k7c6	v
vzdálené	vzdálený	k2eAgFnSc6d1	vzdálená
galaxii	galaxie	k1gFnSc6	galaxie
na	na	k7c6	na
antické	antický	k2eAgFnSc6d1	antická
lodi	loď	k1gFnSc6	loď
Destiny	Destina	k1gFnSc2	Destina
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
musí	muset	k5eAaImIp3nP	muset
bojovat	bojovat	k5eAaImF	bojovat
o	o	k7c4	o
vlastní	vlastní	k2eAgNnSc4d1	vlastní
přežití	přežití	k1gNnSc4	přežití
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Rush	Rush	k1gMnSc1	Rush
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
důležitých	důležitý	k2eAgFnPc2d1	důležitá
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
závisí	záviset	k5eAaImIp3nS	záviset
osud	osud	k1gInSc1	osud
celé	celý	k2eAgFnSc2d1	celá
posádky	posádka	k1gFnSc2	posádka
Destiny	Destina	k1gFnSc2	Destina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
výrazným	výrazný	k2eAgInSc7d1	výrazný
rysem	rys	k1gInSc7	rys
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
odhalovaní	odhalovaný	k2eAgMnPc1d1	odhalovaný
tajemství	tajemství	k1gNnPc4	tajemství
vesmíru	vesmír	k1gInSc2	vesmír
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
části	část	k1gFnSc6	část
epizody	epizoda	k1gFnSc2	epizoda
Air	Air	k1gFnSc2	Air
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
prozradil	prozradit	k5eAaPmAgMnS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
loděnici	loděnice	k1gFnSc6	loděnice
v	v	k7c6	v
Glasgow	Glasgow	k1gNnSc6	Glasgow
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
stipendium	stipendium	k1gNnSc4	stipendium
na	na	k7c4	na
Oxford	Oxford	k1gInSc4	Oxford
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
díky	díky	k7c3	díky
dvěma	dva	k4xCgNnPc3	dva
zaměstnáním	zaměstnání	k1gNnPc3	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
plné	plný	k2eAgNnSc4d1	plné
právo	právo	k1gNnSc4	právo
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
komukoliv	kdokoliv	k3yInSc3	kdokoliv
zodpovídat	zodpovídat	k5eAaImF	zodpovídat
<g/>
.	.	kIx.	.
</s>
