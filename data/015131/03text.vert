<s>
Josef	Josef	k1gMnSc1
Formánek	Formánek	k1gMnSc1
(	(	kIx(
<g/>
spisovatel	spisovatel	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
Formánek	Formánek	k1gMnSc1
Josef	Josef	k1gMnSc1
Formanek	formanka	k1gFnPc2
Narození	narození	k1gNnSc2
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1969	#num#	k4
(	(	kIx(
<g/>
51	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Ústí	ústit	k5eAaImIp3nS
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
spisovatel	spisovatel	k1gMnSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
Čech	Čech	k1gMnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Významná	významný	k2eAgFnSc1d1
díla	dílo	k1gNnPc4
</s>
<s>
Prsatý	prsatý	k2eAgMnSc1d1
muž	muž	k1gMnSc1
a	a	k8xC
zloděj	zloděj	k1gMnSc1
příběhů	příběh	k1gInPc2
Mluviti	mluvit	k5eAaImF
pravdu	pravda	k1gFnSc4
Děti	dítě	k1gFnPc1
</s>
<s>
dcera	dcera	k1gFnSc1
Karolína	Karolína	k1gFnSc1
Vlivy	vliv	k1gInPc5
</s>
<s>
Ota	Ota	k1gMnSc1
Pavel	Pavel	k1gMnSc1
</s>
<s>
„	„	k?
<g/>
Už	už	k6eAd1
taky	taky	k6eAd1
asi	asi	k9
vím	vědět	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
všechno	všechen	k3xTgNnSc1
dobré	dobrý	k2eAgNnSc1d1
a	a	k8xC
zlé	zlý	k2eAgNnSc1d1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
člověk	člověk	k1gMnSc1
udělá	udělat	k5eAaPmIp3nS
<g/>
,	,	kIx,
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
vrátí	vrátit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Že	že	k8xS
nic	nic	k3yNnSc1
nemá	mít	k5eNaImIp3nS
smysl	smysl	k1gInSc4
uspěchat	uspěchat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Že	že	k8xS
některé	některý	k3yIgFnPc4
věci	věc	k1gFnPc4
k	k	k7c3
vám	vy	k3xPp2nPc3
přijdou	přijít	k5eAaPmIp3nP
samy	sám	k3xTgFnPc1
<g/>
,	,	kIx,
a	a	k8xC
některé	některý	k3yIgInPc1
ne	ne	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
je	být	k5eAaImIp3nS
sám	sám	k3xTgMnSc1
sobě	se	k3xPyFc3
nejlepším	dobrý	k2eAgMnSc7d3
rádcem	rádce	k1gMnSc7
a	a	k8xC
než	než	k8xS
na	na	k7c6
jiné	jiná	k1gFnSc6
by	by	kYmCp3nS
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
spolehnout	spolehnout	k5eAaPmF
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
intuici	intuice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
že	že	k8xS
když	když	k8xS
často	často	k6eAd1
myslí	myslet	k5eAaImIp3nS
na	na	k7c4
své	svůj	k3xOyFgNnSc4
sny	sen	k1gInPc1
a	a	k8xC
věří	věřit	k5eAaImIp3nP
jim	on	k3xPp3gMnPc3
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
šanci	šance	k1gFnSc4
se	se	k3xPyFc4
jich	on	k3xPp3gMnPc2
dotknout	dotknout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Že	že	k8xS
by	by	kYmCp3nS
neměl	mít	k5eNaImAgInS
tolik	tolik	k6eAd1
myslet	myslet	k5eAaImF
na	na	k7c4
minulost	minulost	k1gFnSc4
a	a	k8xC
budoucnost	budoucnost	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
nás	my	k3xPp1nPc4
okrádá	okrádat	k5eAaImIp3nS
o	o	k7c4
přítomnost	přítomnost	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
jednou	jednou	k6eAd1
určí	určit	k5eAaPmIp3nS
<g/>
,	,	kIx,
co	co	k8xS
s	s	k7c7
námi	my	k3xPp1nPc7
bude	být	k5eAaImBp3nS
<g/>
,	,	kIx,
a	a	k8xC
kdy	kdy	k6eAd1
teprve	teprve	k6eAd1
poznáme	poznat	k5eAaPmIp1nP
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
jsem	být	k5eAaImIp1nS
zažili	zažít	k5eAaPmAgMnP
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
důležité	důležitý	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Formánek	Formánek	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1969	#num#	k4
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
prozaik	prozaik	k1gMnSc1
<g/>
,	,	kIx,
reportér	reportér	k1gMnSc1
<g/>
,	,	kIx,
redaktor	redaktor	k1gMnSc1
a	a	k8xC
cestovatel	cestovatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystudoval	vystudovat	k5eAaPmAgMnS
Střední	střední	k1gMnSc1
zemědělskou	zemědělský	k2eAgFnSc4d1
technickou	technický	k2eAgFnSc4d1
školu	škola	k1gFnSc4
v	v	k7c6
Roudnici	Roudnice	k1gFnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1992	#num#	k4
založil	založit	k5eAaPmAgInS
společně	společně	k6eAd1
s	s	k7c7
Miroslavem	Miroslav	k1gMnSc7
Urbánkem	Urbánek	k1gMnSc7
geografický	geografický	k2eAgInSc4d1
magazín	magazín	k1gInSc4
Koktejl	koktejl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
třináct	třináct	k4xCc4
let	léto	k1gNnPc2
působil	působit	k5eAaImAgMnS
jako	jako	k9
jeho	jeho	k3xOp3gMnSc1
šéfredaktor	šéfredaktor	k1gMnSc1
a	a	k8xC
díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
práci	práce	k1gFnSc3
procestoval	procestovat	k5eAaPmAgInS
přes	přes	k7c4
třicet	třicet	k4xCc4
zemí	zem	k1gFnPc2
světa	svět	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
ho	on	k3xPp3gNnSc4
nejvíce	nejvíce	k6eAd1,k6eAd3
ovlivnil	ovlivnit	k5eAaPmAgInS
ostrov	ostrov	k1gInSc1
Siberut	Siberut	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stál	stát	k5eAaImAgMnS
také	také	k9
u	u	k7c2
zrodu	zrod	k1gInSc2
časopisů	časopis	k1gInPc2
Oceán	oceán	k1gInSc1
<g/>
,	,	kIx,
Everest	Everest	k1gInSc1
<g/>
,	,	kIx,
Fénix	fénix	k1gMnSc1
<g/>
,	,	kIx,
Kleopatra	Kleopatra	k1gFnSc1
<g/>
,	,	kIx,
Nevěsta	nevěsta	k1gFnSc1
a	a	k8xC
časopisu	časopis	k1gInSc2
pro	pro	k7c4
děti	dítě	k1gFnPc4
Říše	říš	k1gFnSc2
divů	div	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inicioval	iniciovat	k5eAaBmAgMnS
realizaci	realizace	k1gFnSc4
rekvalifikačního	rekvalifikační	k2eAgInSc2d1
kurzu	kurz	k1gInSc2
„	„	k?
<g/>
Redaktor	redaktor	k1gMnSc1
časopisu	časopis	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
probíhal	probíhat	k5eAaImAgInS
přímo	přímo	k6eAd1
v	v	k7c6
redakci	redakce	k1gFnSc6
magazínu	magazín	k1gInSc2
Koktejl	koktejl	k1gInSc4
za	za	k7c2
spolupráce	spolupráce	k1gFnSc2
s	s	k7c7
několika	několik	k4yIc7
známými	známý	k2eAgMnPc7d1
fotografy	fotograf	k1gMnPc7
<g/>
,	,	kIx,
cestovateli	cestovatel	k1gMnPc7
a	a	k8xC
autory	autor	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
Formánek	Formánek	k1gMnSc1
věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
výhradně	výhradně	k6eAd1
spisovatelské	spisovatelský	k2eAgFnPc4d1
činnosti	činnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doposud	doposud	k6eAd1
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
čtyři	čtyři	k4xCgFnPc4
knihy	kniha	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formánek	Formánek	k1gMnSc1
má	mít	k5eAaImIp3nS
dceru	dcera	k1gFnSc4
Karolínu	Karolína	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
by	by	kYmCp3nS
jednou	jeden	k4xCgFnSc7
chtěl	chtít	k5eAaImAgInS
vzít	vzít	k5eAaPmF
s	s	k7c7
sebou	se	k3xPyFc7
na	na	k7c4
ostrov	ostrov	k1gInSc4
Siberut	Siberut	k1gMnSc1
a	a	k8xC
ukázat	ukázat	k5eAaPmF
jí	on	k3xPp3gFnSc7
veškeré	veškerý	k3xTgFnPc4
moudro	moudro	k1gNnSc1
přírodních	přírodní	k2eAgInPc2d1
národů	národ	k1gInPc2
a	a	k8xC
tajemné	tajemný	k2eAgNnSc1d1
kouzlo	kouzlo	k1gNnSc1
pralesa	prales	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Autor	autor	k1gMnSc1
a	a	k8xC
ostrov	ostrov	k1gInSc1
Siberut	Siberut	k2eAgInSc1d1
</s>
<s>
Josef	Josef	k1gMnSc1
Formánek	Formánek	k1gMnSc1
navštívil	navštívit	k5eAaPmAgMnS
Siberut	Siberut	k1gInSc4
třikrát	třikrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1997	#num#	k4
a	a	k8xC
2000	#num#	k4
pobýval	pobývat	k5eAaImAgMnS
na	na	k7c6
ostrově	ostrov	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Indickém	indický	k2eAgInSc6d1
oceáně	oceán	k1gInSc6
západně	západně	k6eAd1
od	od	k7c2
Sumatry	Sumatra	k1gFnSc2
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
půl	půl	k1xP
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
další	další	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
dvouměsíční	dvouměsíční	k2eAgFnSc2d1
humanitární	humanitární	k2eAgFnSc2d1
mise	mise	k1gFnSc2
po	po	k7c6
prosincové	prosincový	k2eAgFnSc6d1
vlně	vlna	k1gFnSc6
tsunami	tsunami	k1gNnSc2
roku	rok	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přírodní	přírodní	k2eAgInPc1d1
národy	národ	k1gInPc1
autora	autor	k1gMnSc2
fascinují	fascinovat	k5eAaBmIp3nP
a	a	k8xC
ostrov	ostrov	k1gInSc4
Siberut	Siberut	k1gInSc1
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
druhý	druhý	k4xOgInSc4
domov	domov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zážitky	zážitek	k1gInPc7
z	z	k7c2
pobytu	pobyt	k1gInSc2
a	a	k8xC
setkávání	setkávání	k1gNnSc2
se	se	k3xPyFc4
s	s	k7c7
lidmi	člověk	k1gMnPc7
kmene	kmen	k1gInSc2
Mentawaj	Mentawaj	k1gMnSc1
popsal	popsat	k5eAaPmAgMnS
v	v	k7c6
cestopisných	cestopisný	k2eAgFnPc6d1
pasážích	pasáž	k1gFnPc6
svého	svůj	k3xOyFgInSc2
debutu	debut	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
odjezdem	odjezd	k1gInSc7
se	se	k3xPyFc4
naučil	naučit	k5eAaPmAgInS
základy	základ	k1gInPc4
mentawajského	mentawajský	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
z	z	k7c2
misionářského	misionářský	k2eAgInSc2d1
mentawajsko-německého	mentawajsko-německý	k2eAgInSc2d1
slovníku	slovník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
jel	jet	k5eAaImAgMnS
na	na	k7c4
Siberut	Siberut	k1gInSc4
se	s	k7c7
svými	svůj	k3xOyFgInPc7
dvěma	dva	k4xCgInPc7
kamarády	kamarád	k1gMnPc4
na	na	k7c4
2	#num#	k4
měsíce	měsíc	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podruhé	podruhé	k6eAd1
strávil	strávit	k5eAaPmAgMnS
na	na	k7c6
ostrově	ostrov	k1gInSc6
3	#num#	k4
měsíce	měsíc	k1gInSc2
v	v	k7c6
doprovodu	doprovod	k1gInSc6
studenta	student	k1gMnSc2
indonéštiny	indonéština	k1gFnSc2
z	z	k7c2
Karlovy	Karlův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
této	tento	k3xDgFnSc6
návštěvě	návštěva	k1gFnSc6
sbíral	sbírat	k5eAaImAgMnS
Formánek	Formánek	k1gMnSc1
artefakty	artefakt	k1gInPc1
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
plánovanou	plánovaný	k2eAgFnSc4d1
výstavu	výstava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nechal	nechat	k5eAaPmAgMnS
si	se	k3xPyFc3
udělat	udělat	k5eAaPmF
rituální	rituální	k2eAgNnSc4d1
mentawajské	mentawajský	k2eAgNnSc4d1
tetování	tetování	k1gNnSc4
celé	celý	k2eAgFnSc2d1
horní	horní	k2eAgFnSc2d1
části	část	k1gFnSc2
těla	tělo	k1gNnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
pomocí	pomocí	k7c2
geometrických	geometrický	k2eAgInPc2d1
obrazců	obrazec	k1gInPc2
představuje	představovat	k5eAaImIp3nS
prales	prales	k1gInSc1
a	a	k8xC
Slunce	slunce	k1gNnSc1
–	–	k?
dárce	dárce	k1gMnSc1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deset	deset	k4xCc1
let	léto	k1gNnPc2
také	také	k9
nosí	nosit	k5eAaImIp3nS
na	na	k7c6
rukou	ruka	k1gFnPc6
červené	červená	k1gFnSc2
a	a	k8xC
žluté	žlutý	k2eAgInPc1d1
šamanské	šamanský	k2eAgInPc1d1
korálky	korálek	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
ho	on	k3xPp3gInSc4
mají	mít	k5eAaImIp3nP
ochránit	ochránit	k5eAaPmF
před	před	k7c7
zlými	zlý	k2eAgMnPc7d1
duchy	duch	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
2002	#num#	k4
vystavoval	vystavovat	k5eAaImAgMnS
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
mentawajské	mentawajský	k2eAgInPc1d1
artefakty	artefakt	k1gInPc1
a	a	k8xC
fotografie	fotografia	k1gFnPc1
v	v	k7c6
expozici	expozice	k1gFnSc6
pojmenované	pojmenovaný	k2eAgFnSc2d1
„	„	k?
<g/>
Siberut	Siberut	k1gMnSc1
-	-	kIx~
Oči	oko	k1gNnPc1
duchů	duch	k1gMnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
expozice	expozice	k1gFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
výstavy	výstava	k1gFnSc2
„	„	k?
<g/>
Tajemná	tajemný	k2eAgFnSc1d1
Indonésie	Indonésie	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
Josef	Josef	k1gMnSc1
Formánek	Formánek	k1gMnSc1
pořádal	pořádat	k5eAaImAgMnS
společně	společně	k6eAd1
s	s	k7c7
Jiřím	Jiří	k1gMnSc7
Hanzelkou	Hanzelka	k1gMnSc7
<g/>
,	,	kIx,
Miroslavem	Miroslav	k1gMnSc7
Zikmundem	Zikmund	k1gMnSc7
<g/>
,	,	kIx,
Rudolfem	Rudolf	k1gMnSc7
Švaříčkem	Švaříček	k1gMnSc7
a	a	k8xC
Miloslavem	Miloslav	k1gMnSc7
Stinglem	Stingl	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc4d1
výstavy	výstava	k1gFnPc4
jeho	jeho	k3xOp3gInPc2
exponátů	exponát	k1gInPc2
se	se	k3xPyFc4
konaly	konat	k5eAaImAgInP
v	v	k7c6
Prachaticích	Prachatice	k1gFnPc6
a	a	k8xC
Karlových	Karlův	k2eAgInPc6d1
Varech	Vary	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vlně	vlna	k1gFnSc6
tsunami	tsunami	k1gNnSc2
v	v	k7c6
Indonésii	Indonésie	k1gFnSc6
26	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2004	#num#	k4
Josef	Josef	k1gMnSc1
Formánek	Formánek	k1gMnSc1
realizoval	realizovat	k5eAaBmAgMnS
na	na	k7c6
základě	základ	k1gInSc6
iniciativy	iniciativa	k1gFnSc2
galeristy	galerista	k1gMnSc2
Hrušky	Hruška	k1gMnSc2
projekt	projekt	k1gInSc1
s	s	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Nejsme	být	k5eNaImIp1nP
sami	sám	k3xTgMnPc1
<g/>
“	“	k?
<g/>
,	,	kIx,
kterým	který	k3yQgMnSc7,k3yIgMnSc7,k3yRgMnSc7
chtěl	chtít	k5eAaImAgMnS
podpořit	podpořit	k5eAaPmF
oblasti	oblast	k1gFnPc4
postižené	postižený	k2eAgFnPc4d1
katastrofou	katastrofa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šedesát	šedesát	k4xCc1
šest	šest	k4xCc4
keramických	keramický	k2eAgFnPc2d1
hlav	hlava	k1gFnPc2
českých	český	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
bylo	být	k5eAaImAgNnS
pomalováno	pomalovat	k5eAaPmNgNnS
a	a	k8xC
na	na	k7c6
aukci	aukce	k1gFnSc6
vydraženo	vydražen	k2eAgNnSc4d1
za	za	k7c4
sumu	suma	k1gFnSc4
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
686	#num#	k4
400	#num#	k4
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výtěžek	výtěžek	k1gInSc1
byl	být	k5eAaImAgInS
věnován	věnovat	k5eAaPmNgInS,k5eAaImNgInS
na	na	k7c4
nákup	nákup	k1gInSc4
materiální	materiální	k2eAgFnSc2d1
humanitární	humanitární	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
vybavení	vybavení	k1gNnSc2
pro	pro	k7c4
školy	škola	k1gFnPc4
<g/>
,	,	kIx,
školky	školka	k1gFnSc2
a	a	k8xC
šicí	šicí	k2eAgFnSc2d1
dílny	dílna	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Josef	Josef	k1gMnSc1
Formánek	Formánek	k1gMnSc1
se	se	k3xPyFc4
jako	jako	k9
dobrovolník	dobrovolník	k1gMnSc1
zúčastnil	zúčastnit	k5eAaPmAgMnS
mise	mise	k1gFnPc4
Českého	český	k2eAgInSc2d1
červeného	červený	k2eAgInSc2d1
kříže	kříž	k1gInSc2
a	a	k8xC
zmíněnou	zmíněný	k2eAgFnSc4d1
materiální	materiální	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
odvezl	odvézt	k5eAaPmAgMnS
do	do	k7c2
Acehu	Aceh	k1gInSc2
na	na	k7c6
Sumatře	Sumatra	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c4
Seychelské	seychelský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
a	a	k8xC
na	na	k7c4
ostrov	ostrov	k1gInSc4
Siberut	Siberut	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
této	tento	k3xDgFnSc6
dvouměsíční	dvouměsíční	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
Formánek	Formánek	k1gMnSc1
podstoupil	podstoupit	k5eAaPmAgMnS
na	na	k7c6
Siberutu	Siberut	k1gInSc6
šamanský	šamanský	k2eAgInSc4d1
rituál	rituál	k1gInSc4
zasvěcení	zasvěcení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šamanské	šamanský	k2eAgFnPc4d1
zkoušky	zkouška	k1gFnPc4
prozatím	prozatím	k6eAd1
nevykonal	vykonat	k5eNaPmAgMnS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Formánek	Formánek	k1gMnSc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
zakročit	zakročit	k5eAaPmF
proti	proti	k7c3
působení	působení	k1gNnSc3
misionářů	misionář	k1gMnPc2
v	v	k7c6
necivilizovaných	civilizovaný	k2eNgFnPc6d1
pralesních	pralesní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1995	#num#	k4
napsal	napsat	k5eAaPmAgInS,k5eAaBmAgInS
otevřený	otevřený	k2eAgInSc1d1
dopis	dopis	k1gInSc1
papeži	papež	k1gMnSc3
Janu	Jan	k1gMnSc3
Pavlu	Pavel	k1gMnSc3
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
kterého	který	k3yQgInSc2,k3yRgInSc2,k3yIgInSc2
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgInS
zprostředkovaný	zprostředkovaný	k2eAgInSc4d1
rozhovor	rozhovor	k1gInSc4
s	s	k7c7
pražským	pražský	k2eAgMnSc7d1
nunciem	nuncius	k1gMnSc7
Giovannim	Giovannim	k1gMnSc1
Coppou	Coppa	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
rozhovoru	rozhovor	k1gInSc6
se	se	k3xPyFc4
ale	ale	k9
Formánkovi	Formánkův	k2eAgMnPc1d1
nedostalo	dostat	k5eNaPmAgNnS
pochopení	pochopení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Formánkově	Formánkův	k2eAgFnSc3d1
spisovatelské	spisovatelský	k2eAgFnSc3d1
etapě	etapa	k1gFnSc3
předcházela	předcházet	k5eAaImAgFnS
etapa	etapa	k1gFnSc1
cestovatelská	cestovatelský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevování	objevování	k1gNnSc1
a	a	k8xC
poznávání	poznávání	k1gNnSc1
<g/>
,	,	kIx,
jaké	jaký	k3yRgNnSc4,k3yIgNnSc4,k3yQgNnSc4
znal	znát	k5eAaImAgInS
z	z	k7c2
cestopisných	cestopisný	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
rád	rád	k6eAd1
četl	číst	k5eAaImAgMnS
jako	jako	k9
kluk	kluk	k1gMnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
staly	stát	k5eAaPmAgInP
inspirací	inspirace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nespočetných	spočetný	k2eNgInPc2d1
zážitků	zážitek	k1gInPc2
z	z	k7c2
cestování	cestování	k1gNnSc2
vycházela	vycházet	k5eAaImAgFnS
touha	touha	k1gFnSc1
se	se	k3xPyFc4
o	o	k7c4
to	ten	k3xDgNnSc4
nově	nově	k6eAd1
nabyté	nabytý	k2eAgNnSc1d1
rozdělit	rozdělit	k5eAaPmF
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
v	v	k7c6
reportáži	reportáž	k1gFnSc6
nebo	nebo	k8xC
v	v	k7c6
knize	kniha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
pět	pět	k4xCc4
knih	kniha	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
vyšly	vyjít	k5eAaPmAgFnP
u	u	k7c2
vydavatelství	vydavatelství	k1gNnSc2
Smart	Smarta	k1gFnPc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
Svůj	svůj	k3xOyFgInSc4
debut	debut	k1gInSc4
Prsatý	prsatý	k2eAgMnSc1d1
muž	muž	k1gMnSc1
a	a	k8xC
zloděj	zloděj	k1gMnSc1
příběhů	příběh	k1gInPc2
tvořil	tvořit	k5eAaImAgInS
sedm	sedm	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšel	vyjít	k5eAaPmAgInS
roku	rok	k1gInSc2
2003	#num#	k4
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
nejúspěšnějším	úspěšný	k2eAgInSc7d3
debutem	debut	k1gInSc7
desetiletí	desetiletí	k1gNnSc2
-	-	kIx~
doposud	doposud	k6eAd1
se	se	k3xPyFc4
prodalo	prodat	k5eAaPmAgNnS
více	hodně	k6eAd2
než	než	k8xS
72	#num#	k4
000	#num#	k4
výtisků	výtisk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
díky	díky	k7c3
velké	velký	k2eAgFnSc3d1
čtenářské	čtenářský	k2eAgFnSc3d1
odezvě	odezva	k1gFnSc3
prodává	prodávat	k5eAaImIp3nS
už	už	k9
dvanácté	dvanáctý	k4xOgNnSc1
vydání	vydání	k1gNnSc1
(	(	kIx(
<g/>
červen	červen	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
vznikla	vzniknout	k5eAaPmAgFnS
i	i	k9
audio	audio	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
předčítá	předčítat	k5eAaImIp3nS
herec	herec	k1gMnSc1
Národního	národní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
Jiří	Jiří	k1gMnSc1
Štěpnička	Štěpnička	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
kniha	kniha	k1gFnSc1
Létající	létající	k2eAgFnSc1d1
jaguár	jaguár	k1gMnSc1
vyšla	vyjít	k5eAaPmAgFnS
roku	rok	k1gInSc2
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
rekordním	rekordní	k2eAgNnSc6d1
časovém	časový	k2eAgNnSc6d1
rozmezí	rozmezí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spisovatel	spisovatel	k1gMnSc1
se	se	k3xPyFc4
vsadil	vsadit	k5eAaPmAgMnS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
nakladatelem	nakladatel	k1gMnSc7
Ivem	Ivo	k1gMnSc7
Železným	Železný	k1gMnSc7
<g/>
,	,	kIx,
že	že	k8xS
napíše	napsat	k5eAaBmIp3nS,k5eAaPmIp3nS
novelu	novela	k1gFnSc4
za	za	k7c4
12	#num#	k4
hodin	hodina	k1gFnPc2
(	(	kIx(
<g/>
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
to	ten	k3xDgNnSc1
podařilo	podařit	k5eAaPmAgNnS
v	v	k7c6
čase	čas	k1gInSc6
kratším	krátký	k2eAgInSc6d2
–	–	k?
11	#num#	k4
hodin	hodina	k1gFnPc2
19	#num#	k4
minut	minuta	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakladatel	nakladatel	k1gMnSc1
ji	on	k3xPp3gFnSc4
pak	pak	k6eAd1
za	za	k7c4
dalších	další	k2eAgFnPc2d1
12	#num#	k4
hodin	hodina	k1gFnPc2
vydal	vydat	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výtěžek	výtěžek	k1gInSc1
z	z	k7c2
prvního	první	k4xOgNnSc2
vydání	vydání	k1gNnSc2
(	(	kIx(
<g/>
700	#num#	k4
svazků	svazek	k1gInPc2
<g/>
)	)	kIx)
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
68	#num#	k4
000	#num#	k4
Kč	Kč	kA
byl	být	k5eAaImAgInS
věnován	věnovat	k5eAaPmNgInS,k5eAaImNgInS
nově	nově	k6eAd1
otevřenému	otevřený	k2eAgInSc3d1
pavilonu	pavilon	k1gInSc3
slonů	slon	k1gMnPc2
v	v	k7c6
ústecké	ústecký	k2eAgFnSc6d1
zoo	zoo	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgFnSc1
Formánkova	Formánkův	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
Mluviti	mluvit	k5eAaImF
pravdu	pravda	k1gFnSc4
vyšla	vyjít	k5eAaPmAgFnS
roku	rok	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prodalo	prodat	k5eAaPmAgNnS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
doposud	doposud	k6eAd1
přes	přes	k7c4
26	#num#	k4
000	#num#	k4
výtisků	výtisk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtvrtá	čtvrtá	k1gFnSc1
kniha	kniha	k1gFnSc1
Umřel	umřít	k5eAaPmAgMnS
jsem	být	k5eAaImIp1nS
v	v	k7c4
sobotu	sobota	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
psal	psát	k5eAaImAgMnS
autor	autor	k1gMnSc1
2	#num#	k4
roky	rok	k1gInPc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
vydaná	vydaný	k2eAgFnSc1d1
roku	rok	k1gInSc2
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatím	zatím	k6eAd1
poslední	poslední	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
Syn	syn	k1gMnSc1
větru	vítr	k1gInSc2
a	a	k8xC
prsatý	prsatý	k2eAgMnSc1d1
muž	muž	k1gMnSc1
mělo	mít	k5eAaImAgNnS
premiéru	premiéra	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechna	všechen	k3xTgNnPc4
díla	dílo	k1gNnPc4
spojuje	spojovat	k5eAaImIp3nS
autobiografičnost	autobiografičnost	k1gFnSc4
<g/>
,	,	kIx,
vrstevnatost	vrstevnatost	k1gFnSc4
<g/>
,	,	kIx,
formální	formální	k2eAgNnSc4d1
členění	členění	k1gNnSc4
na	na	k7c4
mnoho	mnoho	k4c4
kapitol	kapitola	k1gFnPc2
a	a	k8xC
podkapitol	podkapitola	k1gFnPc2
<g/>
,	,	kIx,
ilustrace	ilustrace	k1gFnPc1
kreslíře	kreslíř	k1gMnSc2
Dalibora	Dalibor	k1gMnSc2
Nesnídala	Nesnídal	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
hlavní	hlavní	k2eAgNnSc1d1
téma	téma	k1gNnSc1
–	–	k?
hledání	hledání	k1gNnSc1
smyslu	smysl	k1gInSc2
lidského	lidský	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Knihy	kniha	k1gFnPc4
Prsatý	prsatý	k2eAgMnSc1d1
muž	muž	k1gMnSc1
a	a	k8xC
zloděj	zloděj	k1gMnSc1
příběhů	příběh	k1gInPc2
a	a	k8xC
Mluviti	mluvit	k5eAaImF
pravdu	pravda	k1gFnSc4
přeložila	přeložit	k5eAaPmAgFnS
do	do	k7c2
francouzštiny	francouzština	k1gFnSc2
Christina	Christin	k2eAgNnSc2d1
La	la	k1gNnSc2
Ferreira	Ferreir	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formánek	Formánek	k1gMnSc1
je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
scénáře	scénář	k1gInSc2
pro	pro	k7c4
televizní	televizní	k2eAgInSc4d1
film	film	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Bibliografie	bibliografie	k1gFnSc1
</s>
<s>
Prsatý	prsatý	k2eAgMnSc1d1
muž	muž	k1gMnSc1
a	a	k8xC
zloděj	zloděj	k1gMnSc1
příběhů	příběh	k1gInPc2
-	-	kIx~
cestopisný	cestopisný	k2eAgInSc4d1
román	román	k1gInSc4
s	s	k7c7
autobiografickými	autobiografický	k2eAgInPc7d1
prvky	prvek	k1gInPc7
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Létající	létající	k2eAgMnSc1d1
jaguár	jaguár	k1gMnSc1
–	–	k?
novela	novela	k1gFnSc1
na	na	k7c6
základě	základ	k1gInSc6
vylosovaných	vylosovaný	k2eAgNnPc2d1
témat	téma	k1gNnPc2
<g/>
,	,	kIx,
napsaná	napsaný	k2eAgFnSc1d1
a	a	k8xC
vydaná	vydaný	k2eAgFnSc1d1
v	v	k7c6
rekordním	rekordní	k2eAgInSc6d1
čase	čas	k1gInSc6
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mluviti	mluvit	k5eAaImF
pravdu	pravda	k1gFnSc4
–	–	k?
brutální	brutální	k2eAgInSc4d1
román	román	k1gInSc4
o	o	k7c6
lásce	láska	k1gFnSc6
k	k	k7c3
životu	život	k1gInSc3
podle	podle	k7c2
skutečných	skutečný	k2eAgFnPc2d1
událostí	událost	k1gFnPc2
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Umřel	umřít	k5eAaPmAgMnS
jsem	být	k5eAaImIp1nS
v	v	k7c4
sobotu	sobota	k1gFnSc4
–	–	k?
Jeden	jeden	k4xCgInSc1
dům	dům	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgMnSc1
stavitel	stavitel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedno	jeden	k4xCgNnSc1
boží	boží	k2eAgNnSc1d1
oko	oko	k1gNnSc1
a	a	k8xC
jedna	jeden	k4xCgFnSc1
věta	věta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dům	dům	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
podivné	podivný	k2eAgFnPc1d1
věci	věc	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
čeho	co	k3yInSc2,k3yRnSc2,k3yQnSc2
vybírá	vybírat	k5eAaImIp3nS
bůh	bůh	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
je	být	k5eAaImIp3nS
po	po	k7c6
smrti	smrt	k1gFnSc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
–	–	k?
Jediné	jediný	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yIgNnSc4,k3yQgNnSc4
neilustroval	ilustrovat	k5eNaBmAgMnS
výtvarník	výtvarník	k1gMnSc1
a	a	k8xC
Formánkův	Formánkův	k2eAgMnSc1d1
přítel	přítel	k1gMnSc1
Dalibor	Dalibor	k1gMnSc1
Nesnídal	Nesnídal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autor	autor	k1gMnSc1
použil	použít	k5eAaPmAgMnS
fotografie	fotografia	k1gFnPc4
z	z	k7c2
objektu	objekt	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
v	v	k7c6
knize	kniha	k1gFnSc6
popisuje	popisovat	k5eAaImIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Syn	syn	k1gMnSc1
větru	vítr	k1gInSc2
a	a	k8xC
prsatý	prsatý	k2eAgMnSc1d1
muž	muž	k1gMnSc1
–	–	k?
pokračování	pokračování	k1gNnSc4
knihy	kniha	k1gFnSc2
Prsatý	prsatý	k2eAgMnSc1d1
muž	muž	k1gMnSc1
a	a	k8xC
zloděj	zloděj	k1gMnSc1
příběhů	příběh	k1gInPc2
<g/>
;	;	kIx,
depresivní	depresivní	k2eAgInSc1d1
příběh	příběh	k1gInSc1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
David	David	k1gMnSc1
Zajíc	Zajíc	k1gMnSc1
vrátí	vrátit	k5eAaPmIp3nS
na	na	k7c4
ostrov	ostrov	k1gInSc4
Siberut	Siberut	k2eAgInSc4d1
a	a	k8xC
nestačí	stačit	k5eNaBmIp3nP
se	se	k3xPyFc4
divit	divit	k5eAaImF
co	co	k9
vše	všechen	k3xTgNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
zanikla	zaniknout	k5eAaPmAgFnS
kultura	kultura	k1gFnSc1
a	a	k8xC
jak	jak	k8xC,k8xS
nejcennější	cenný	k2eAgFnPc1d3
věci	věc	k1gFnPc1
jsou	být	k5eAaImIp3nP
americké	americký	k2eAgFnPc1d1
cigarety	cigareta	k1gFnPc1
a	a	k8xC
japonské	japonský	k2eAgFnPc1d1
motorky	motorka	k1gFnPc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Úsměvy	úsměv	k1gInPc1
smutných	smutný	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
–	–	k?
vyprávění	vyprávění	k1gNnSc4
z	z	k7c2
protialkoholní	protialkoholní	k2eAgFnSc2d1
léčebny	léčebna	k1gFnSc2
<g/>
,	,	kIx,
ilustrováno	ilustrován	k2eAgNnSc1d1
Daliborem	Dalibor	k1gMnSc7
Nesnídalem	Nesnídal	k1gMnSc7
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dvě	dva	k4xCgNnPc4
slova	slovo	k1gNnPc4
jako	jako	k8xS,k8xC
klíč	klíč	k1gInSc4
–	–	k?
laskavá	laskavý	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
o	o	k7c6
podivuhodných	podivuhodný	k2eAgInPc6d1
osudech	osud	k1gInPc6
dotýkajících	dotýkající	k2eAgInPc6d1
se	s	k7c7
života	život	k1gInSc2
Teodora	Teodor	k1gMnSc2
D.	D.	kA
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
MORAVEC	Moravec	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Formánek	Formánek	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
<g/>
,	,	kIx,
2005-01-13	2005-01-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HÁVOVÁ	HÁVOVÁ	kA
<g/>
,	,	kIx,
Naděžda	Naděžda	k1gFnSc1
<g/>
;	;	kIx,
KROC	KROC	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Formánek	Formánek	k1gMnSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Říkali	říkat	k5eAaImAgMnP
mi	já	k3xPp1nSc3
Si	se	k3xPyFc3
Lap	lap	k1gInSc4
Lap	lap	k1gInSc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2005-02-7	2005-02-7	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Humanitární	humanitární	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Český	český	k2eAgInSc1d1
červený	červený	k2eAgInSc1d1
kříž	kříž	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KRAUS	Kraus	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvolněte	uvolnit	k5eAaPmRp2nP
se	se	k3xPyFc4
prosím	prosit	k5eAaImIp1nS
ze	z	k7c2
dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2006-01-06	2006-01-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
CESTROVÁ	CESTROVÁ	kA
<g/>
,	,	kIx,
Dagmar	Dagmar	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svlékám	svlékat	k5eAaImIp1nS
se	se	k3xPyFc4
do	do	k7c2
naha	naho	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koktejl	koktejl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duben	duben	k1gInSc1
2007	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
XVI	XVI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
32	#num#	k4
<g/>
-	-	kIx~
<g/>
37	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1210	#num#	k4
<g/>
-	-	kIx~
<g/>
4353	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BANZETOVÁ	BANZETOVÁ	kA
<g/>
,	,	kIx,
Jitka	Jitka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mluvit	mluvit	k5eAaImF
pravdu	pravda	k1gFnSc4
o	o	k7c6
zatáčkách	zatáčka	k1gFnPc6
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grand	grand	k1gMnSc1
Biblio	Biblio	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říjen	říjen	k1gInSc1
2008	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
10	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
4	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
3320	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Josef	Josef	k1gMnSc1
Formánek	Formánek	k1gMnSc1
(	(	kIx(
<g/>
spisovatel	spisovatel	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
http://www.josefformanek.com	http://www.josefformanek.com	k1gInSc1
</s>
<s>
http://www.ceskenoviny.cz/zpravy/formanek-snazim-se-muzu-nenastvat/348070&	http://www.ceskenoviny.cz/zpravy/formanek-snazim-se-muzu-nenastvat/348070&	k6eAd1
</s>
<s>
https://web.archive.org/web/20090317083006/http://www.tiscali.cz/ente/ente_center_040420.722596.html	https://web.archive.org/web/20090317083006/http://www.tiscali.cz/ente/ente_center_040420.722596.html	k1gMnSc1
</s>
<s>
http://www.rozhlas.cz/radionaprani/archiv/?p_num_from=720&	http://www.rozhlas.cz/radionaprani/archiv/?p_num_from=720&	k?
</s>
<s>
http://www.rozhlas.cz/radio_cesko/press/_zprava/571848	http://www.rozhlas.cz/radio_cesko/press/_zprava/571848	k4
</s>
<s>
http://www.rozhlas.cz/radio_cesko/sc/_zprava/563992	http://www.rozhlas.cz/radio_cesko/sc/_zprava/563992	k4
</s>
<s>
https://web.archive.org/web/20100618170958/http://www.czechlit.cz/autori/formanek-josef/	https://web.archive.org/web/20100618170958/http://www.czechlit.cz/autori/formanek-josef/	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
14575	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1105587371	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
6406	#num#	k4
9366	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2009093408	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
90554122	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2009093408	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
</s>
