<s>
Phoenix	Phoenix	k1gInSc1	Phoenix
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
státu	stát	k1gInSc2	stát
Arizona	Arizona	k1gFnSc1	Arizona
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
o	o	k7c4	o
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Arizony	Arizona	k1gFnSc2	Arizona
a	a	k8xC	a
sídlo	sídlo	k1gNnSc4	sídlo
okresu	okres	k1gInSc2	okres
Maricopa	Maricop	k1gMnSc2	Maricop
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1881	[number]	k4	1881
a	a	k8xC	a
v	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
indiánů	indián	k1gMnPc2	indián
bylo	být	k5eAaImAgNnS	být
nazvané	nazvaný	k2eAgNnSc4d1	nazvané
Hoozdo	Hoozdo	k1gNnSc4	Hoozdo
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
horoucí	horoucí	k2eAgNnSc4d1	horoucí
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
kmene	kmen	k1gInSc2	kmen
Navajo	Navajo	k6eAd1	Navajo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Fiinigis	Fiinigis	k1gFnSc1	Fiinigis
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
kmene	kmen	k1gInSc2	kmen
Apačů	Apač	k1gMnPc2	Apač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Phoenix	Phoenix	k1gInSc1	Phoenix
je	být	k5eAaImIp3nS	být
pátým	pátý	k4xOgNnSc7	pátý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c4	na
1230,5	[number]	k4	1230,5
km2	km2	k4	km2
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
1	[number]	k4	1
512	[number]	k4	512
986	[number]	k4	986
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
Phoenixu	Phoenix	k1gInSc2	Phoenix
je	být	k5eAaImIp3nS	být
13	[number]	k4	13
<g/>
.	.	kIx.	.
největší	veliký	k2eAgInSc1d3	veliký
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
odhadů	odhad	k1gInPc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
4	[number]	k4	4
039	[number]	k4	039
182	[number]	k4	182
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
1	[number]	k4	1
445	[number]	k4	445
632	[number]	k4	632
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
65,9	[number]	k4	65,9
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
6,5	[number]	k4	6,5
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
2,2	[number]	k4	2,2
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
3,2	[number]	k4	3,2
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,2	[number]	k4	0,2
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
18,5	[number]	k4	18,5
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
3,6	[number]	k4	3,6
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
40,8	[number]	k4	40,8
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
1	[number]	k4	1
321	[number]	k4	321
045	[number]	k4	045
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
865	[number]	k4	865
834	[number]	k4	834
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
407	[number]	k4	407
450	[number]	k4	450
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
byla	být	k5eAaImAgFnS	být
1	[number]	k4	1
0	[number]	k4	0
<g/>
74	[number]	k4	74
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
700	[number]	k4	700
se	se	k3xPyFc4	se
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
Phoenixu	Phoenix	k1gInSc2	Phoenix
nacházela	nacházet	k5eAaImAgFnS	nacházet
sídla	sídlo	k1gNnSc2	sídlo
indiánů	indián	k1gMnPc2	indián
kmene	kmen	k1gInSc2	kmen
Hohokam	Hohokam	k1gInSc1	Hohokam
<g/>
.	.	kIx.	.
</s>
<s>
Indiáni	Indián	k1gMnPc1	Indián
zde	zde	k6eAd1	zde
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
přes	přes	k7c4	přes
200	[number]	k4	200
kilometrů	kilometr	k1gInPc2	kilometr
zavlažovacích	zavlažovací	k2eAgInPc2d1	zavlažovací
kanálů	kanál	k1gInPc2	kanál
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yIgFnPc3	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
úrodnou	úrodný	k2eAgFnSc7d1	úrodná
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
trasy	trasa	k1gFnSc2	trasa
těchto	tento	k3xDgInPc2	tento
kanálů	kanál	k1gInPc2	kanál
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
použita	použit	k2eAgFnSc1d1	použita
při	při	k7c6	při
budování	budování	k1gNnSc6	budování
Arizona	Arizona	k1gFnSc1	Arizona
Canalu	Canal	k1gInSc3	Canal
<g/>
,	,	kIx,	,
Central	Central	k1gFnSc1	Central
Arizona	Arizona	k1gFnSc1	Arizona
Project	Project	k1gInSc1	Project
Canalu	Canal	k1gInSc2	Canal
a	a	k8xC	a
the	the	k?	the
Hayden-Rhodesova	Hayden-Rhodesův	k2eAgInSc2d1	Hayden-Rhodesův
akvaduktu	akvadukt	k1gInSc2	akvadukt
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1300	[number]	k4	1300
až	až	k8xS	až
1450	[number]	k4	1450
vedla	vést	k5eAaImAgFnS	vést
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
období	období	k1gNnSc6	období
sucha	sucho	k1gNnSc2	sucho
<g/>
,	,	kIx,	,
střídaná	střídaný	k2eAgFnSc1d1	střídaná
rozsáhlými	rozsáhlý	k2eAgFnPc7d1	rozsáhlá
záplavami	záplava	k1gFnPc7	záplava
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
civilizace	civilizace	k1gFnSc2	civilizace
Hohokamů	Hohokam	k1gInPc2	Hohokam
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největšímu	veliký	k2eAgInSc3d3	veliký
rozvoji	rozvoj	k1gInSc3	rozvoj
města	město	k1gNnSc2	město
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
měl	mít	k5eAaImAgInS	mít
Phoenix	Phoenix	k1gInSc1	Phoenix
48	[number]	k4	48
118	[number]	k4	118
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
106	[number]	k4	106
818	[number]	k4	818
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtyřnásobek	čtyřnásobek	k1gInSc4	čtyřnásobek
-	-	kIx~	-
439	[number]	k4	439
170	[number]	k4	170
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
přírůstek	přírůstek	k1gInSc4	přírůstek
obyvatel	obyvatel	k1gMnPc2	obyvatel
mělo	mít	k5eAaImAgNnS	mít
také	také	k9	také
vytvoření	vytvoření	k1gNnSc4	vytvoření
letecké	letecký	k2eAgInPc4d1	letecký
základy	základ	k1gInPc4	základ
Luke	Luk	k1gFnSc2	Luk
Air	Air	k1gFnSc7	Air
Force	force	k1gFnSc7	force
Base	basa	k1gFnSc3	basa
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
domovem	domov	k1gInSc7	domov
největšího	veliký	k2eAgNnSc2d3	veliký
stíhacího	stíhací	k2eAgNnSc2d1	stíhací
křídla	křídlo	k1gNnSc2	křídlo
USAF	USAF	kA	USAF
a	a	k8xC	a
především	především	k9	především
vytvoření	vytvoření	k1gNnSc2	vytvoření
mnoha	mnoho	k4c2	mnoho
pracovních	pracovní	k2eAgFnPc2d1	pracovní
příležitostí	příležitost	k1gFnPc2	příležitost
při	při	k7c6	při
příchodu	příchod	k1gInSc6	příchod
firem	firma	k1gFnPc2	firma
jako	jako	k8xS	jako
Honeywell	Honeywella	k1gFnPc2	Honeywella
(	(	kIx(	(
<g/>
letectví	letectví	k1gNnSc2	letectví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Intel	Intel	kA	Intel
(	(	kIx(	(
<g/>
elektronika	elektronika	k1gFnSc1	elektronika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Phelps	Phelps	k1gInSc1	Phelps
Dodge	Dodge	k1gInSc1	Dodge
Corporation	Corporation	k1gInSc1	Corporation
(	(	kIx(	(
<g/>
těžba	těžba	k1gFnSc1	těžba
minerálů	minerál	k1gInPc2	minerál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dochází	docházet	k5eAaImIp3nS	docházet
také	také	k9	také
k	k	k7c3	k
masivnímu	masivní	k2eAgInSc3d1	masivní
rozvoji	rozvoj	k1gInSc3	rozvoj
okolních	okolní	k2eAgFnPc2d1	okolní
měst	město	k1gNnPc2	město
aglomerace	aglomerace	k1gFnSc2	aglomerace
a	a	k8xC	a
přírůstku	přírůstek	k1gInSc2	přírůstek
populace	populace	k1gFnSc2	populace
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
ve	v	k7c6	v
Phoenixu	Phoenix	k1gInSc6	Phoenix
je	být	k5eAaImIp3nS	být
aridního	aridní	k2eAgInSc2d1	aridní
suchého	suchý	k2eAgInSc2d1	suchý
typu	typ	k1gInSc2	typ
-	-	kIx~	-
Phoenix	Phoenix	k1gInSc1	Phoenix
má	mít	k5eAaImIp3nS	mít
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
maximální	maximální	k2eAgFnSc4d1	maximální
roční	roční	k2eAgFnSc4d1	roční
teplotu	teplota	k1gFnSc4	teplota
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
průměrných	průměrný	k2eAgFnPc2d1	průměrná
letních	letní	k2eAgFnPc2d1	letní
teplot	teplota	k1gFnPc2	teplota
-	-	kIx~	-
překonávají	překonávat	k5eAaImIp3nP	překonávat
ho	on	k3xPp3gInSc4	on
pouze	pouze	k6eAd1	pouze
některá	některý	k3yIgNnPc1	některý
města	město	k1gNnPc1	město
v	v	k7c6	v
Perském	perský	k2eAgInSc6d1	perský
zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
překračuje	překračovat	k5eAaImIp3nS	překračovat
100	[number]	k4	100
°	°	k?	°
<g/>
F	F	kA	F
(	(	kIx(	(
<g/>
38	[number]	k4	38
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
více	hodně	k6eAd2	hodně
než	než	k8xS	než
89	[number]	k4	89
dní	den	k1gInPc2	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
teplota	teplota	k1gFnSc1	teplota
50	[number]	k4	50
°	°	k?	°
<g/>
C	C	kA	C
byla	být	k5eAaImAgFnS	být
naměřena	naměřen	k2eAgFnSc1d1	naměřena
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Suchý	suchý	k2eAgInSc1d1	suchý
pouštní	pouštní	k2eAgInSc1d1	pouštní
vzduch	vzduch	k1gInSc1	vzduch
činí	činit	k5eAaImIp3nS	činit
tyto	tento	k3xDgFnPc4	tento
vysoké	vysoký	k2eAgFnPc4d1	vysoká
teploty	teplota	k1gFnPc4	teplota
snesitelnějšími	snesitelný	k2eAgFnPc7d2	snesitelnější
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
července	červenec	k1gInSc2	červenec
začíná	začínat	k5eAaImIp3nS	začínat
monzunová	monzunový	k2eAgFnSc1d1	monzunová
sezóna	sezóna	k1gFnSc1	sezóna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
obvykle	obvykle	k6eAd1	obvykle
do	do	k7c2	do
půlky	půlka	k1gFnSc2	půlka
září	září	k1gNnSc2	září
a	a	k8xC	a
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
vlhkost	vlhkost	k1gFnSc1	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
na	na	k7c4	na
nepříliš	příliš	k6eNd1	příliš
přijatelnou	přijatelný	k2eAgFnSc4d1	přijatelná
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Zima	zima	k1gFnSc1	zima
bývá	bývat	k5eAaImIp3nS	bývat
mírná	mírný	k2eAgFnSc1d1	mírná
<g/>
,	,	kIx,	,
teploty	teplota	k1gFnPc1	teplota
obvykle	obvykle	k6eAd1	obvykle
neklesají	klesat	k5eNaImIp3nP	klesat
pod	pod	k7c4	pod
bod	bod	k1gInSc4	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Phoenix	Phoenix	k1gInSc1	Phoenix
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
15	[number]	k4	15
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
Ahwatukee	Ahwatukee	k1gNnSc4	Ahwatukee
Foothills	Foothillsa	k1gFnPc2	Foothillsa
<g/>
,	,	kIx,	,
Alhambra	Alhambra	k1gMnSc1	Alhambra
<g/>
,	,	kIx,	,
Camelback	Camelback	k1gMnSc1	Camelback
East	East	k1gMnSc1	East
<g/>
,	,	kIx,	,
Central	Central	k1gMnSc1	Central
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
Deer	Deer	k1gInSc4	Deer
Valley	Vallea	k1gFnSc2	Vallea
<g/>
,	,	kIx,	,
Desert	desert	k1gInSc4	desert
View	View	k1gFnSc2	View
<g/>
,	,	kIx,	,
Encanto	Encanta	k1gFnSc5	Encanta
<g/>
,	,	kIx,	,
Estrella	Estrella	k1gMnSc1	Estrella
<g/>
,	,	kIx,	,
Laveen	Laveen	k2eAgMnSc1d1	Laveen
<g/>
,	,	kIx,	,
Maryvale	Maryvala	k1gFnSc3	Maryvala
<g/>
,	,	kIx,	,
North	North	k1gInSc4	North
Gateway	Gatewaa	k1gFnSc2	Gatewaa
<g/>
,	,	kIx,	,
Sunnyslope	Sunnyslop	k1gMnSc5	Sunnyslop
<g/>
,	,	kIx,	,
North	North	k1gMnSc1	North
Mountain	Mountain	k1gMnSc1	Mountain
<g/>
,	,	kIx,	,
Paradise	Paradise	k1gFnSc1	Paradise
Valley	Vallea	k1gFnSc2	Vallea
<g/>
,	,	kIx,	,
South	South	k1gMnSc1	South
Mountain	Mountain	k1gMnSc1	Mountain
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
přibyla	přibýt	k5eAaPmAgFnS	přibýt
další	další	k2eAgFnSc1d1	další
řídce	řídce	k6eAd1	řídce
osídlená	osídlený	k2eAgFnSc1d1	osídlená
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
známá	známá	k1gFnSc1	známá
pod	pod	k7c7	pod
prozatímním	prozatímní	k2eAgInSc7d1	prozatímní
názvem	název	k1gInSc7	název
New	New	k1gMnSc2	New
Village	Villag	k1gMnSc2	Villag
(	(	kIx(	(
<g/>
Nová	nový	k2eAgFnSc1d1	nová
ves	ves	k1gFnSc1	ves
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neoficiálně	neoficiálně	k6eAd1	neoficiálně
<g/>
,	,	kIx,	,
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
komunikaci	komunikace	k1gFnSc6	komunikace
je	být	k5eAaImIp3nS	být
Phoenix	Phoenix	k1gInSc1	Phoenix
dělen	dělen	k2eAgInSc1d1	dělen
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
oblasti	oblast	k1gFnPc4	oblast
<g/>
:	:	kIx,	:
Downtown	Downtown	k1gInSc4	Downtown
West	West	k2eAgInSc4d1	West
Phoenix	Phoenix	k1gInSc4	Phoenix
North	North	k1gInSc1	North
<g/>
/	/	kIx~	/
<g/>
Northwest	Northwest	k1gFnSc1	Northwest
Phoenix	Phoenix	k1gInSc4	Phoenix
Southwest	Southwest	k1gInSc1	Southwest
Phoenix	Phoenix	k1gInSc1	Phoenix
South	Southa	k1gFnPc2	Southa
Phoenix	Phoenix	k1gInSc4	Phoenix
Ahwatukee	Ahwatukee	k1gFnSc4	Ahwatukee
East	East	k2eAgInSc1d1	East
Side	Side	k1gInSc1	Side
(	(	kIx(	(
<g/>
East	East	k1gInSc1	East
Valley	Vallea	k1gFnSc2	Vallea
<g/>
)	)	kIx)	)
Leteckou	letecký	k2eAgFnSc4d1	letecká
dopravní	dopravní	k2eAgFnSc4d1	dopravní
obslužnost	obslužnost	k1gFnSc4	obslužnost
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
především	především	k9	především
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Sky	Sky	k1gFnSc2	Sky
Harbor	Harbora	k1gFnPc2	Harbora
International	International	k1gFnSc2	International
Airport	Airport	k1gInSc1	Airport
(	(	kIx(	(
<g/>
kód	kód	k1gInSc1	kód
IATA	IATA	kA	IATA
<g/>
:	:	kIx,	:
PHX	PHX	kA	PHX
<g/>
,	,	kIx,	,
ICAO	ICAO	kA	ICAO
<g/>
:	:	kIx,	:
KPHX	KPHX	kA	KPHX
<g/>
)	)	kIx)	)
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
středu	střed	k1gInSc2	střed
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
křižovatky	křižovatka	k1gFnSc2	křižovatka
hlavních	hlavní	k2eAgFnPc2d1	hlavní
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Sky	Sky	k?	Sky
Harbor	Harbor	k1gInSc1	Harbor
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
osobní	osobní	k2eAgFnSc2d1	osobní
přepravy	přeprava	k1gFnSc2	přeprava
sedmým	sedmý	k4xOgNnSc7	sedmý
nejrušnějším	rušný	k2eAgNnSc7d3	nejrušnější
letištěm	letiště	k1gNnSc7	letiště
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
nejrušnějším	rušný	k2eAgNnSc7d3	nejrušnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
počet	počet	k1gInSc4	počet
odbavených	odbavený	k2eAgMnPc2d1	odbavený
pasažérů	pasažér	k1gMnPc2	pasažér
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
41	[number]	k4	41
miliónů	milión	k4xCgInPc2	milión
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
letiště	letiště	k1gNnSc2	letiště
odlétají	odlétat	k5eAaImIp3nP	odlétat
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
linky	linka	k1gFnPc1	linka
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
světových	světový	k2eAgNnPc2d1	světové
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Letečtí	letecký	k2eAgMnPc1d1	letecký
přepravci	přepravce	k1gMnPc1	přepravce
jako	jako	k8xC	jako
British	British	k1gInSc1	British
Airways	Airwaysa	k1gFnPc2	Airwaysa
či	či	k8xC	či
Air	Air	k1gFnSc1	Air
Canada	Canada	k1gFnSc1	Canada
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
spojení	spojení	k1gNnPc1	spojení
s	s	k7c7	s
mezinárodními	mezinárodní	k2eAgInPc7d1	mezinárodní
letišti	letiště	k1gNnSc6	letiště
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
nebo	nebo	k8xC	nebo
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Soukromá	soukromý	k2eAgNnPc1d1	soukromé
a	a	k8xC	a
menší	malý	k2eAgNnPc1d2	menší
firemní	firemní	k2eAgNnPc1d1	firemní
letadla	letadlo	k1gNnPc1	letadlo
využívají	využívat	k5eAaPmIp3nP	využívat
menších	malý	k2eAgNnPc2d2	menší
regionálních	regionální	k2eAgNnPc2d1	regionální
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Phoenix	Phoenix	k1gInSc1	Phoenix
Deer	Deera	k1gFnPc2	Deera
Valley	Vallea	k1gFnSc2	Vallea
Airport	Airport	k1gInSc1	Airport
(	(	kIx(	(
<g/>
IATA	IATA	kA	IATA
<g/>
:	:	kIx,	:
DVT	DVT	kA	DVT
<g/>
,	,	kIx,	,
ICAO	ICAO	kA	ICAO
<g/>
:	:	kIx,	:
KDVT	KDVT	kA	KDVT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
severu	sever	k1gInSc6	sever
města	město	k1gNnSc2	město
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Deer	Deera	k1gFnPc2	Deera
Valley	Vallea	k1gFnSc2	Vallea
<g/>
.	.	kIx.	.
</s>
<s>
Městskou	městský	k2eAgFnSc4d1	městská
dopravu	doprava	k1gFnSc4	doprava
ve	v	k7c6	v
Phoenixu	Phoenix	k1gInSc6	Phoenix
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
společnost	společnost	k1gFnSc1	společnost
Valley	Vallea	k1gFnSc2	Vallea
Metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
síť	síť	k1gFnSc4	síť
autobusových	autobusový	k2eAgFnPc2d1	autobusová
linek	linka	k1gFnPc2	linka
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
koordinuje	koordinovat	k5eAaBmIp3nS	koordinovat
sdílení	sdílení	k1gNnSc1	sdílení
soukromých	soukromý	k2eAgNnPc2d1	soukromé
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
rideshare	rideshar	k1gInSc5	rideshar
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výstavě	výstava	k1gFnSc6	výstava
je	být	k5eAaImIp3nS	být
linka	linka	k1gFnSc1	linka
rychlodrážní	rychlodrážní	k2eAgFnSc2d1	rychlodrážní
tramvaje	tramvaj	k1gFnSc2	tramvaj
Valley	Vallea	k1gFnSc2	Vallea
Metro	metro	k1gNnSc1	metro
Rail	Rail	k1gInSc1	Rail
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
Phoenix	Phoenix	k1gInSc1	Phoenix
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
v	v	k7c6	v
USA	USA	kA	USA
bez	bez	k7c2	bez
městské	městský	k2eAgFnSc2d1	městská
kolejové	kolejový	k2eAgFnSc2d1	kolejová
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
bez	bez	k7c2	bez
metra	metro	k1gNnSc2	metro
nebo	nebo	k8xC	nebo
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
vlaková	vlakový	k2eAgFnSc1d1	vlaková
společnost	společnost	k1gFnSc1	společnost
Amtrak	Amtrak	k1gInSc4	Amtrak
zrušila	zrušit	k5eAaPmAgFnS	zrušit
stanici	stanice	k1gFnSc3	stanice
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
Phoenix	Phoenix	k1gInSc1	Phoenix
také	také	k9	také
jediným	jediný	k2eAgNnSc7d1	jediné
velkoměstem	velkoměsto	k1gNnSc7	velkoměsto
v	v	k7c6	v
USA	USA	kA	USA
bez	bez	k7c2	bez
osobní	osobní	k2eAgFnSc2d1	osobní
meziměstské	meziměstská	k1gFnSc2	meziměstská
železniční	železniční	k2eAgFnSc2d1	železniční
přepravy	přeprava	k1gFnSc2	přeprava
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Phoenixu	Phoenix	k1gInSc2	Phoenix
se	se	k3xPyFc4	se
na	na	k7c6	na
24	[number]	k4	24
<g/>
.	.	kIx.	.
ulici	ulice	k1gFnSc6	ulice
nachází	nacházet	k5eAaImIp3nS	nacházet
zastávka	zastávka	k1gFnSc1	zastávka
dálkových	dálkový	k2eAgInPc2d1	dálkový
autobusů	autobus	k1gInPc2	autobus
společnosti	společnost	k1gFnSc2	společnost
Greyhound	Greyhounda	k1gFnPc2	Greyhounda
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
mezistátní	mezistátní	k2eAgFnSc1d1	mezistátní
dálnice	dálnice	k1gFnSc1	dálnice
(	(	kIx(	(
<g/>
interstate	interstat	k1gInSc5	interstat
<g/>
)	)	kIx)	)
I-	I-	k1gFnPc2	I-
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
prochází	procházet	k5eAaImIp3nS	procházet
centrem	centrum	k1gNnSc7	centrum
Phoenixu	Phoenix	k1gInSc2	Phoenix
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
odtud	odtud	k6eAd1	odtud
dál	daleko	k6eAd2	daleko
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
do	do	k7c2	do
Tucsonu	Tucson	k1gInSc2	Tucson
<g/>
.	.	kIx.	.
</s>
<s>
Středem	středem	k7c2	středem
města	město	k1gNnSc2	město
prochází	procházet	k5eAaImIp3nS	procházet
také	také	k9	také
I-	I-	k1gFnSc1	I-
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
Phoenix	Phoenix	k1gInSc4	Phoenix
s	s	k7c7	s
Flagstaffem	Flagstaff	k1gInSc7	Flagstaff
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
I-	I-	k?	I-
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
označena	označen	k2eAgFnSc1d1	označena
jako	jako	k9	jako
mezistátní	mezistátní	k2eAgFnSc3d1	mezistátní
(	(	kIx(	(
<g/>
interstate	interstat	k1gInSc5	interstat
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
celá	celý	k2eAgFnSc1d1	celá
na	na	k7c4	na
území	území	k1gNnSc4	území
Arizony	Arizona	k1gFnSc2	Arizona
<g/>
.	.	kIx.	.
</s>
<s>
Phoenixem	Phoenix	k1gInSc7	Phoenix
dále	daleko	k6eAd2	daleko
prochází	procházet	k5eAaImIp3nS	procházet
severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
směrem	směr	k1gInSc7	směr
státní	státní	k2eAgFnSc2d1	státní
silnice	silnice	k1gFnSc2	silnice
60	[number]	k4	60
(	(	kIx(	(
<g/>
US	US	kA	US
60	[number]	k4	60
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
příměstskou	příměstský	k2eAgFnSc4d1	příměstská
i	i	k8xC	i
tranzitní	tranzitní	k2eAgFnSc4d1	tranzitní
dopravu	doprava	k1gFnSc4	doprava
mají	mít	k5eAaImIp3nP	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
dálniční	dálniční	k2eAgInPc4d1	dálniční
obchvaty	obchvat	k1gInPc4	obchvat
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
smyčky	smyčka	k1gFnPc4	smyčka
-	-	kIx~	-
Loop	Loop	k1gInSc1	Loop
101	[number]	k4	101
(	(	kIx(	(
<g/>
Agua	Agua	k1gFnSc1	Agua
Fria	Frius	k1gMnSc2	Frius
Freeway	Freewaa	k1gMnSc2	Freewaa
<g/>
/	/	kIx~	/
<g/>
Pima	Pimus	k1gMnSc2	Pimus
Freeway	Freewaa	k1gMnSc2	Freewaa
<g/>
/	/	kIx~	/
<g/>
Price	Price	k1gMnSc2	Price
Freeway	Freewaa	k1gMnSc2	Freewaa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Loop	Loop	k1gInSc1	Loop
202	[number]	k4	202
(	(	kIx(	(
<g/>
Red	Red	k1gFnSc7	Red
Mountain	Mountain	k2eAgInSc4d1	Mountain
<g/>
/	/	kIx~	/
<g/>
Santan	Santan	k1gInSc4	Santan
Freeway	Freewaa	k1gFnSc2	Freewaa
<g/>
)	)	kIx)	)
a	a	k8xC	a
Loop	Loop	k1gInSc1	Loop
303	[number]	k4	303
(	(	kIx(	(
<g/>
Estrella	Estrella	k1gFnSc1	Estrella
Freeway	Freewaa	k1gFnSc2	Freewaa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
příměstské	příměstský	k2eAgFnPc4d1	příměstská
silnice	silnice	k1gFnPc4	silnice
patří	patřit	k5eAaImIp3nS	patřit
51	[number]	k4	51
(	(	kIx(	(
<g/>
Piestewa	Piestew	k1gInSc2	Piestew
Freeway	Freewaa	k1gFnSc2	Freewaa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
143	[number]	k4	143
(	(	kIx(	(
<g/>
the	the	k?	the
Hohokam	Hohokam	k1gInSc1	Hohokam
Expressway	Expresswaa	k1gFnSc2	Expresswaa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
153	[number]	k4	153
(	(	kIx(	(
<g/>
the	the	k?	the
Sky	Sky	k1gMnSc4	Sky
Harbor	Harbor	k1gMnSc1	Harbor
Expressway	Expresswaa	k1gFnSc2	Expresswaa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Phoenix	Phoenix	k1gInSc1	Phoenix
je	být	k5eAaImIp3nS	být
také	také	k9	také
domovem	domov	k1gInSc7	domov
řady	řada	k1gFnSc2	řada
sportovních	sportovní	k2eAgInPc2d1	sportovní
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
bude	být	k5eAaImBp3nS	být
jistě	jistě	k9	jistě
mezi	mezi	k7c7	mezi
nejznámější	známý	k2eAgFnSc7d3	nejznámější
patřit	patřit	k5eAaImF	patřit
hokejový	hokejový	k2eAgInSc4d1	hokejový
tým	tým	k1gInSc4	tým
Arizona	Arizona	k1gFnSc1	Arizona
Coyotes	Coyotes	k1gInSc1	Coyotes
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
další	další	k2eAgInSc4d1	další
"	"	kIx"	"
<g/>
bez	bez	k1gInSc4	bez
<g/>
"	"	kIx"	"
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
-	-	kIx~	-
Arizona	Arizona	k1gFnSc1	Arizona
Coyotes	Coyotesa	k1gFnPc2	Coyotesa
je	být	k5eAaImIp3nS	být
tým	tým	k1gInSc1	tým
v	v	k7c6	v
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
si	se	k3xPyFc3	se
zatím	zatím	k6eAd1	zatím
nikdy	nikdy	k6eAd1	nikdy
nesáhl	sáhnout	k5eNaPmAgMnS	sáhnout
na	na	k7c4	na
Stanleyův	Stanleyův	k2eAgInSc4d1	Stanleyův
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
sporty	sport	k1gInPc1	sport
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupen	k2eAgInPc1d1	zastoupen
týmy	tým	k1gInPc1	tým
<g/>
:	:	kIx,	:
Phoenix	Phoenix	k1gInSc1	Phoenix
Suns	Suns	k1gInSc1	Suns
(	(	kIx(	(
<g/>
basketball	basketball	k1gInSc1	basketball
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Arizona	Arizona	k1gFnSc1	Arizona
Diamondbacks	Diamondbacks	k1gInSc1	Diamondbacks
(	(	kIx(	(
<g/>
baseball	baseball	k1gInSc1	baseball
<g/>
)	)	kIx)	)
a	a	k8xC	a
Arizona	Arizona	k1gFnSc1	Arizona
Cardinals	Cardinalsa	k1gFnPc2	Cardinalsa
(	(	kIx(	(
<g/>
americký	americký	k2eAgInSc1d1	americký
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Phoenix	Phoenix	k1gInSc1	Phoenix
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
deset	deset	k4xCc4	deset
partnerských	partnerský	k2eAgNnPc2d1	partnerské
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Phoenix	Phoenix	k1gInSc1	Phoenix
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
