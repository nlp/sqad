<s>
Kord	kord	k1gInSc1	kord
je	být	k5eAaImIp3nS	být
chladná	chladný	k2eAgFnSc1d1	chladná
poboční	poboční	k2eAgFnSc1d1	poboční
bodná	bodný	k2eAgFnSc1d1	bodná
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
z	z	k7c2	z
pozdních	pozdní	k2eAgInPc2d1	pozdní
typů	typ	k1gInPc2	typ
bodných	bodný	k2eAgInPc2d1	bodný
mečů	meč	k1gInPc2	meč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
poprvé	poprvé	k6eAd1	poprvé
doložen	doložit	k5eAaPmNgInS	doložit
počátkem	počátkem	k7c2	počátkem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
štíhlou	štíhlý	k2eAgFnSc7d1	štíhlá
a	a	k8xC	a
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
čepelí	čepel	k1gFnSc7	čepel
postupně	postupně	k6eAd1	postupně
vytlačoval	vytlačovat	k5eAaImAgMnS	vytlačovat
mohutnější	mohutný	k2eAgInPc4d2	mohutnější
meče	meč	k1gInPc4	meč
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
už	už	k6eAd1	už
ruka	ruka	k1gFnSc1	ruka
nebyla	být	k5eNaImAgFnS	být
chráněna	chránit	k5eAaImNgFnS	chránit
železnou	železný	k2eAgFnSc7d1	železná
rukavicí	rukavice	k1gFnSc7	rukavice
(	(	kIx(	(
<g/>
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
snáze	snadno	k6eAd2	snadno
stisknout	stisknout	k5eAaPmF	stisknout
spoušť	spoušť	k1gFnSc4	spoušť
palné	palný	k2eAgFnSc2d1	palná
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chránil	chránit	k5eAaImAgMnS	chránit
ji	on	k3xPp3gFnSc4	on
koš	koš	k1gInSc1	koš
ze	z	k7c2	z
železných	železný	k2eAgInPc2d1	železný
úponků	úponek	k1gInPc2	úponek
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
kordů	kord	k1gInPc2	kord
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
v	v	k7c4	v
subtilnější	subtilní	k2eAgInSc4d2	subtilnější
soubojový	soubojový	k2eAgInSc4d1	soubojový
rapír	rapír	k1gInSc4	rapír
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
souběžně	souběžně	k6eAd1	souběžně
vývoj	vývoj	k1gInSc4	vývoj
kordu	kord	k1gInSc2	kord
jako	jako	k8xC	jako
vojenské	vojenský	k2eAgFnSc2d1	vojenská
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
i	i	k9	i
vojenské	vojenský	k2eAgInPc4d1	vojenský
kordy	kord	k1gInPc4	kord
své	svůj	k3xOyFgFnSc2	svůj
praktické	praktický	k2eAgFnSc2d1	praktická
uplatnění	uplatnění	k1gNnSc3	uplatnění
a	a	k8xC	a
stávají	stávat	k5eAaImIp3nP	stávat
se	se	k3xPyFc4	se
doplňkem	doplněk	k1gInSc7	doplněk
slavnostní	slavnostní	k2eAgFnSc2d1	slavnostní
uniformy	uniforma	k1gFnSc2	uniforma
(	(	kIx(	(
<g/>
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1938	[number]	k4	1938
Finanční	finanční	k2eAgFnPc1d1	finanční
stráže	stráž	k1gFnPc1	stráž
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
u	u	k7c2	u
Hradní	hradní	k2eAgFnSc2d1	hradní
stráže	stráž	k1gFnSc2	stráž
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
odznakem	odznak	k1gInSc7	odznak
důstojnické	důstojnický	k2eAgFnSc2d1	důstojnická
<g/>
,	,	kIx,	,
praporčické	praporčický	k2eAgFnSc2d1	praporčická
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
úřednické	úřednický	k2eAgFnSc2d1	úřednická
hodnosti	hodnost	k1gFnSc2	hodnost
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Šerm	šerm	k1gInSc1	šerm
kordem	kord	k1gInSc7	kord
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
disciplín	disciplína	k1gFnPc2	disciplína
sportovního	sportovní	k2eAgInSc2d1	sportovní
šermu	šerm	k1gInSc2	šerm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
šermu	šerm	k1gInSc2	šerm
fleretem	fleret	k1gInSc7	fleret
či	či	k8xC	či
šavlí	šavle	k1gFnSc7	šavle
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
šermu	šerm	k1gInSc6	šerm
kordem	kord	k1gInSc7	kord
platnou	platný	k2eAgFnSc7d1	platná
zásahovou	zásahový	k2eAgFnSc7d1	zásahová
plochou	plocha	k1gFnSc7	plocha
celé	celý	k2eAgNnSc4d1	celé
tělo	tělo	k1gNnSc4	tělo
šermíře	šermíř	k1gMnSc2	šermíř
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
má	mít	k5eAaImIp3nS	mít
číška	číška	k1gFnSc1	číška
chránící	chránící	k2eAgFnSc1d1	chránící
ruku	ruka	k1gFnSc4	ruka
znatelně	znatelně	k6eAd1	znatelně
větší	veliký	k2eAgInSc4d2	veliký
rozměr	rozměr	k1gInSc4	rozměr
než	než	k8xS	než
u	u	k7c2	u
ostatních	ostatní	k2eAgFnPc2d1	ostatní
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
sportovního	sportovní	k2eAgInSc2d1	sportovní
kordu	kord	k1gInSc2	kord
nesmí	smět	k5eNaImIp3nS	smět
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
110	[number]	k4	110
cm	cm	kA	cm
(	(	kIx(	(
<g/>
20	[number]	k4	20
cm	cm	kA	cm
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
rukojeť	rukojeť	k1gFnSc4	rukojeť
a	a	k8xC	a
záštitu	záštita	k1gFnSc4	záštita
<g/>
)	)	kIx)	)
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc1	hmotnost
770	[number]	k4	770
g.	g.	k?	g.
Kordy	kord	k1gInPc1	kord
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
podle	podle	k7c2	podle
čepele	čepel	k1gFnSc2	čepel
na	na	k7c4	na
kordy	kord	k1gInPc4	kord
s	s	k7c7	s
trojsečnou	trojsečný	k2eAgFnSc7d1	trojsečný
<g/>
,	,	kIx,	,
dvojsečnou	dvojsečný	k2eAgFnSc7d1	dvojsečná
a	a	k8xC	a
jednosečnou	jednosečný	k2eAgFnSc7d1	jednosečná
čepelí	čepel	k1gFnSc7	čepel
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
kordu	kord	k1gInSc2	kord
určného	určné	k1gNnSc2	určné
ke	k	k7c3	k
sportovnímu	sportovní	k2eAgInSc3d1	sportovní
šermu	šerm	k1gInSc3	šerm
<g/>
:	:	kIx,	:
kord	kord	k1gInSc1	kord
začíná	začínat	k5eAaImIp3nS	začínat
rukojetí	rukojeť	k1gFnSc7	rukojeť
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
záštita	záštita	k1gFnSc1	záštita
(	(	kIx(	(
<g/>
číška	číška	k1gFnSc1	číška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
čepel	čepel	k1gInSc1	čepel
a	a	k8xC	a
kord	kord	k1gInSc1	kord
končí	končit	k5eAaImIp3nS	končit
aretem	aret	k1gInSc7	aret
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
čepeli	čepel	k1gFnSc6	čepel
je	být	k5eAaImIp3nS	být
veden	veden	k2eAgInSc1d1	veden
drát	drát	k1gInSc1	drát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
končí	končit	k5eAaImIp3nS	končit
u	u	k7c2	u
aretu	aret	k1gInSc2	aret
<g/>
.	.	kIx.	.
</s>
<s>
Aret	Aret	k1gInSc1	Aret
slouží	sloužit	k5eAaImIp3nS	sloužit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
zásahu	zásah	k1gInSc6	zásah
soupeře	soupeř	k1gMnSc4	soupeř
se	se	k3xPyFc4	se
zamáčkne	zamáčknout	k5eAaPmIp3nS	zamáčknout
a	a	k8xC	a
pošle	poslat	k5eAaPmIp3nS	poslat
signál	signál	k1gInSc1	signál
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
oblečením	oblečení	k1gNnSc7	oblečení
je	být	k5eAaImIp3nS	být
drát	drát	k5eAaImF	drát
<g/>
)	)	kIx)	)
do	do	k7c2	do
přístroje	přístroj	k1gInSc2	přístroj
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgMnPc1	dva
hráči	hráč	k1gMnPc1	hráč
napojeni	napojen	k2eAgMnPc1d1	napojen
<g/>
,	,	kIx,	,
a	a	k8xC	a
zasvítí	zasvítit	k5eAaPmIp3nS	zasvítit
dioda	dioda	k1gFnSc1	dioda
(	(	kIx(	(
<g/>
zelená	zelený	k2eAgFnSc1d1	zelená
nebo	nebo	k8xC	nebo
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
strany	strana	k1gFnSc2	strana
hráče	hráč	k1gMnSc2	hráč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
signál	signál	k1gInSc4	signál
rozhodčímu	rozhodčí	k1gMnSc3	rozhodčí
pro	pro	k7c4	pro
zaznamenání	zaznamenání	k1gNnSc4	zaznamenání
bodu	bod	k1gInSc2	bod
<g/>
.	.	kIx.	.
</s>
