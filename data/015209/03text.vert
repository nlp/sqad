<s>
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
král	král	k1gMnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
</s>
<s>
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
1820	#num#	k4
–	–	k?
26	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1830	#num#	k4
</s>
<s>
Korunovace	korunovace	k1gFnSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1821	#num#	k4
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
1762	#num#	k4
</s>
<s>
St	St	kA
James	James	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Palace	Palace	k1gFnSc1
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1830	#num#	k4
</s>
<s>
Windsorský	windsorský	k2eAgInSc1d1
hrad	hrad	k1gInSc1
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1
</s>
<s>
kaple	kaple	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Jiří	Jiří	k1gMnSc2
na	na	k7c6
Windsorském	windsorský	k2eAgInSc6d1
hradu	hrad	k1gInSc6
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Královna	královna	k1gFnSc1
</s>
<s>
Karolina	Karolinum	k1gNnPc1
Brunšvická	brunšvický	k2eAgNnPc1d1
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Šarlota	Šarlota	k1gFnSc1
Augusta	August	k1gMnSc2
</s>
<s>
Rod	rod	k1gInSc1
</s>
<s>
Welfové	Welf	k1gMnPc1
</s>
<s>
Dynastie	dynastie	k1gFnSc1
</s>
<s>
Hannoverská	hannoverský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Šarlota	Šarlota	k1gFnSc1
von	von	k1gInSc1
Mecklenburg-Strelitz	Mecklenburg-Strelitz	k1gMnSc1
</s>
<s>
Podpis	podpis	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc4
1762	#num#	k4
Londýn	Londýn	k1gInSc1
–	–	k?
26	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc4
1830	#num#	k4
Windsor	Windsor	k1gInSc4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
panovník	panovník	k1gMnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
a	a	k8xC
Hannoveru	Hannover	k1gInSc2
od	od	k7c2
smrti	smrt	k1gFnSc2
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1820	#num#	k4
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1811	#num#	k4
až	až	k9
do	do	k7c2
nástupu	nástup	k1gInSc2
na	na	k7c4
trůn	trůn	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
kdy	kdy	k6eAd1
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
upadl	upadnout	k5eAaPmAgMnS
do	do	k7c2
stavu	stav	k1gInSc2
nepříčetnosti	nepříčetnost	k1gFnSc2
<g/>
,	,	kIx,
vládl	vládnout	k5eAaImAgMnS
jako	jako	k9
princ	princ	k1gMnSc1
regent	regent	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
je	být	k5eAaImIp3nS
připomínán	připomínat	k5eAaImNgMnS
především	především	k9
pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
extravagantní	extravagantní	k2eAgInSc4d1
životní	životní	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1797	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgMnS
tělesné	tělesný	k2eAgFnSc3d1
hmotnosti	hmotnost	k1gFnSc3
přes	přes	k7c4
111	#num#	k4
kg	kg	kA
a	a	k8xC
roku	rok	k1gInSc2
1824	#num#	k4
používal	používat	k5eAaImAgInS
korzet	korzet	k1gInSc1
vyrobený	vyrobený	k2eAgInSc1d1
pro	pro	k7c4
obvod	obvod	k1gInSc4
pasu	pást	k5eAaImIp1nS
127	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
patronem	patron	k1gMnSc7
nového	nový	k2eAgInSc2d1
životního	životní	k2eAgInSc2d1
stylu	styl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Johna	John	k1gMnSc2
Nashe	Nash	k1gMnSc2
pověřil	pověřit	k5eAaPmAgInS
stavbou	stavba	k1gFnSc7
královského	královský	k2eAgInSc2d1
pavilónu	pavilón	k1gInSc2
v	v	k7c6
Brightonu	Brighton	k1gInSc6
a	a	k8xC
přestavbou	přestavba	k1gFnSc7
Buckinghamského	buckinghamský	k2eAgInSc2d1
paláce	palác	k1gInSc2
a	a	k8xC
Jeffryho	Jeffry	k1gMnSc2
Wyatvilleho	Wyatville	k1gMnSc2
rekonstrukcí	rekonstrukce	k1gFnSc7
Windsorského	windsorský	k2eAgInSc2d1
hradu	hrad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
také	také	k9
iniciátorem	iniciátor	k1gMnSc7
vzniku	vznik	k1gInSc2
Národní	národní	k2eAgFnSc2d1
galerie	galerie	k1gFnSc2
a	a	k8xC
King	King	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
College	College	k1gNnSc7
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c4
větší	veliký	k2eAgFnSc4d2
část	část	k1gFnSc4
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
regentství	regentství	k1gNnSc2
a	a	k8xC
vlády	vláda	k1gFnSc2
byl	být	k5eAaImAgMnS
předsedou	předseda	k1gMnSc7
vlády	vláda	k1gFnSc2
Robert	Robert	k1gMnSc1
Jenkinson	Jenkinson	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
Jiří	Jiří	k1gMnSc1
vojensky	vojensky	k6eAd1
nezasahoval	zasahovat	k5eNaImAgMnS
do	do	k7c2
průběhu	průběh	k1gInSc2
napoleonských	napoleonský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
,	,	kIx,
pokoušel	pokoušet	k5eAaImAgInS
se	se	k3xPyFc4
do	do	k7c2
nich	on	k3xPp3gFnPc2
politicky	politicky	k6eAd1
zasahovat	zasahovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdoroval	vzdorovat	k5eAaImAgMnS
emancipaci	emancipace	k1gFnSc4
katolíků	katolík	k1gMnPc2
a	a	k8xC
navrhl	navrhnout	k5eAaPmAgMnS
nepopulární	populární	k2eNgInSc4d1
zákon	zákon	k1gInSc4
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yQgInSc7,k3yIgInSc7
chtěl	chtít	k5eAaImAgMnS
dosáhnout	dosáhnout	k5eAaPmF
rozvodu	rozvod	k1gInSc2
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
nenáviděnou	nenáviděný	k2eAgFnSc7d1
ženou	žena	k1gFnSc7
Karolinou	Karolina	k1gFnSc7
Brunšvickou	brunšvický	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
12	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1762	#num#	k4
v	v	k7c6
St	St	kA
James	James	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Palace	Palace	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
nejstaršímu	starý	k2eAgMnSc3d3
synu	syn	k1gMnSc3
panovníka	panovník	k1gMnSc4
<g/>
,	,	kIx,
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgInS
krátce	krátce	k6eAd1
po	po	k7c6
narození	narození	k1gNnSc6
udělen	udělen	k2eAgInSc1d1
titul	titul	k1gInSc1
prince	princ	k1gMnSc2
z	z	k7c2
Walesu	Wales	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
talentovaným	talentovaný	k2eAgMnSc7d1
studentem	student	k1gMnSc7
a	a	k8xC
rychle	rychle	k6eAd1
se	se	k3xPyFc4
naučil	naučit	k5eAaPmAgMnS
mluvit	mluvit	k5eAaImF
francouzsky	francouzsky	k6eAd1
<g/>
,	,	kIx,
německy	německy	k6eAd1
a	a	k8xC
italsky	italsky	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1783	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgMnS
Jiří	Jiří	k1gMnSc1
21	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
od	od	k7c2
parlamentu	parlament	k1gInSc2
obdržel	obdržet	k5eAaPmAgMnS
60000	#num#	k4
liber	libra	k1gFnPc2
a	a	k8xC
od	od	k7c2
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
pravidelný	pravidelný	k2eAgInSc4d1
příjem	příjem	k1gInSc4
50000	#num#	k4
liber	libra	k1gFnPc2
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zařídil	zařídit	k5eAaPmAgMnS
si	se	k3xPyFc3
svou	svůj	k3xOyFgFnSc4
rezidenci	rezidence	k1gFnSc4
v	v	k7c4
Carlton	Carlton	k1gInSc4
House	house	k1gNnSc1
a	a	k8xC
vedl	vést	k5eAaImAgMnS
rozmařilý	rozmařilý	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
rozporům	rozpor	k1gInPc3
mezi	mezi	k7c7
ním	on	k3xPp3gInSc7
a	a	k8xC
jeho	jeho	k3xOp3gMnSc7
konzervativním	konzervativní	k2eAgMnSc7d1
otcem	otec	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
očekával	očekávat	k5eAaImAgInS
od	od	k7c2
svého	svůj	k3xOyFgMnSc2
dědice	dědic	k1gMnSc2
chování	chování	k1gNnSc2
jednak	jednak	k8xC
skromnější	skromný	k2eAgMnSc1d2
<g/>
,	,	kIx,
jednak	jednak	k8xC
odpovědnější	odpovědný	k2eAgMnPc1d2
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
a	a	k8xC
dcera	dcera	k1gFnSc1
</s>
<s>
Princ	princ	k1gMnSc1
Jiří	Jiří	k1gMnSc1
se	se	k3xPyFc4
totiž	totiž	k9
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
21	#num#	k4
letech	léto	k1gNnPc6
zamiloval	zamilovat	k5eAaPmAgMnS
do	do	k7c2
o	o	k7c4
šest	šest	k4xCc4
let	léto	k1gNnPc2
starší	starý	k2eAgInSc1d2
<g/>
,	,	kIx,
dvakrát	dvakrát	k6eAd1
ovdovělé	ovdovělý	k2eAgFnSc2d1
katoličky	katolička	k1gFnSc2
Marie	Maria	k1gFnSc2
Anny	Anna	k1gFnSc2
Fitzherbert	Fitzherbert	k1gInSc1
<g/>
;	;	kIx,
sňatek	sňatek	k1gInSc1
s	s	k7c7
ní	on	k3xPp3gFnSc7
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgInS
ovšem	ovšem	k9
podle	podle	k7c2
Act	Act	k1gFnSc2
of	of	k?
Settlement	settlement	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1701	#num#	k4
zapovězen	zapovědět	k5eAaPmNgMnS
pod	pod	k7c7
hrozbou	hrozba	k1gFnSc7
ztráty	ztráta	k1gFnSc2
nároku	nárok	k1gInSc2
na	na	k7c4
trůn	trůn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
podle	podle	k7c2
jiného	jiný	k2eAgNnSc2d1
nařízení	nařízení	k1gNnSc2
<g/>
,	,	kIx,
Royal	Royal	k1gMnSc1
Marriages	Marriages	k1gMnSc1
Act	Act	k1gMnSc1
z	z	k7c2
roku	rok	k1gInSc2
1772	#num#	k4
<g/>
,	,	kIx,
potřeboval	potřebovat	k5eAaImAgInS
ke	k	k7c3
sňatku	sňatek	k1gInSc3
souhlas	souhlas	k1gInSc4
krále	král	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
mu	on	k3xPp3gMnSc3
ho	on	k3xPp3gInSc4
z	z	k7c2
pochopitelných	pochopitelný	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
nedal	dát	k5eNaPmAgMnS
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
se	se	k3xPyFc4
však	však	k9
s	s	k7c7
ní	on	k3xPp3gFnSc7
přesto	přesto	k6eAd1
-	-	kIx~
tajně	tajně	k6eAd1
a	a	k8xC
bez	bez	k7c2
souhlasu	souhlas	k1gInSc2
krále	král	k1gMnSc2
-	-	kIx~
15	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
roku	rok	k1gInSc2
1785	#num#	k4
oženil	oženit	k5eAaPmAgMnS
<g/>
,	,	kIx,
podle	podle	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
uvedeného	uvedený	k2eAgInSc2d1
zákona	zákon	k1gInSc2
však	však	k9
sňatek	sňatek	k1gInSc1
byl	být	k5eAaImAgInS
neplatný	platný	k2eNgInSc1d1
<g/>
,	,	kIx,
paní	paní	k1gFnSc1
Fitzherbert	Fitzherbert	k1gInSc1
však	však	k9
věřila	věřit	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
legitimní	legitimní	k2eAgMnSc1d1
<g/>
,	,	kIx,
pokládajíc	pokládat	k5eAaImSgNnS
zákony	zákon	k1gInPc7
katolické	katolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
za	za	k7c4
nadřazené	nadřazený	k2eAgNnSc4d1
zákonům	zákon	k1gInPc3
státu	stát	k1gInSc2
a	a	k8xC
anglikánské	anglikánský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
politických	politický	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
byl	být	k5eAaImAgInS
sňatek	sňatek	k1gInSc1
utajen	utajit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nákladný	nákladný	k2eAgInSc1d1
způsob	způsob	k1gInSc1
života	život	k1gInSc2
prince	princ	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
vedl	vést	k5eAaImAgMnS
nicméně	nicméně	k8xC
k	k	k7c3
postupnému	postupný	k2eAgNnSc3d1
enormnímu	enormní	k2eAgNnSc3d1
narůstání	narůstání	k1gNnSc3
jeho	jeho	k3xOp3gInPc2
dluhů	dluh	k1gInPc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gNnSc4
placení	placení	k1gNnSc4
řešil	řešit	k5eAaImAgInS
několikrát	několikrát	k6eAd1
parlament	parlament	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
král	král	k1gMnSc1
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
rozhodl	rozhodnout	k5eAaPmAgInS
mu	on	k3xPp3gMnSc3
pomoci	pomoct	k5eAaPmF
<g/>
,	,	kIx,
ovšem	ovšem	k9
pouze	pouze	k6eAd1
pod	pod	k7c7
podmínkou	podmínka	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
ožení	oženit	k5eAaPmIp3nS
podle	podle	k7c2
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
nevěstu	nevěsta	k1gFnSc4
mu	on	k3xPp3gMnSc3
byla	být	k5eAaImAgFnS
vybrána	vybrán	k2eAgFnSc1d1
jeho	jeho	k3xOp3gFnSc1
sestřenice	sestřenice	k1gFnSc1
Karolina	Karolinum	k1gNnSc2
Brunšvická	brunšvický	k2eAgFnSc1d1
<g/>
,	,	kIx,
dcera	dcera	k1gFnSc1
Karla	Karel	k1gMnSc2
Viléma	Vilém	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
<g/>
,	,	kIx,
vévody	vévoda	k1gMnSc2
brunšvicko-wolfenbüttelského	brunšvicko-wolfenbüttelský	k2eAgMnSc2d1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
Augusty	Augusta	k1gMnSc2
Frederiky	Frederika	k1gFnSc2
<g/>
,	,	kIx,
sestry	sestra	k1gFnPc1
krále	král	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Svatební	svatební	k2eAgInSc1d1
obřad	obřad	k1gInSc1
Karoliny	Karolinum	k1gNnPc7
a	a	k8xC
Jiřího	Jiří	k1gMnSc2
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgMnS
8	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
roku	rok	k1gInSc2
1795	#num#	k4
v	v	k7c6
St	St	kA
James	James	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Palace	Palace	k1gFnSc2
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgNnSc3
byl	být	k5eAaImAgMnS
ženich	ženich	k1gMnSc1
takto	takto	k6eAd1
donucen	donutit	k5eAaPmNgMnS
<g/>
,	,	kIx,
se	se	k3xPyFc4
zákonitě	zákonitě	k6eAd1
ukázalo	ukázat	k5eAaPmAgNnS
jako	jako	k9
fatálně	fatálně	k6eAd1
nezdařilé	zdařilý	k2eNgNnSc1d1
a	a	k8xC
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
trvání	trvání	k1gNnSc2
Jiří	Jiří	k1gMnSc1
dával	dávat	k5eAaImAgMnS
pocítit	pocítit	k5eAaPmF
manželce	manželka	k1gFnSc6
svou	svůj	k3xOyFgFnSc4
nechuť	nechuť	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
přerostla	přerůst	k5eAaPmAgFnS
v	v	k7c4
nenávist	nenávist	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavěl	stavět	k5eAaImAgMnS
se	se	k3xPyFc4
k	k	k7c3
ní	on	k3xPp3gFnSc3
negativně	negativně	k6eAd1
od	od	k7c2
počátku	počátek	k1gInSc2
<g/>
,	,	kIx,
již	již	k6eAd1
jako	jako	k9
novomanžel	novomanžel	k1gMnSc1
označil	označit	k5eAaPmAgMnS
Karolinu	Karolinum	k1gNnSc6
za	za	k7c4
nepřitažlivou	přitažlivý	k2eNgFnSc4d1
ženu	žena	k1gFnSc4
nedbající	dbající	k2eNgFnSc4d1
o	o	k7c4
hygienu	hygiena	k1gFnSc4
a	a	k8xC
navíc	navíc	k6eAd1
ji	on	k3xPp3gFnSc4
podezíral	podezírat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
okamžiku	okamžik	k1gInSc6
svatby	svatba	k1gFnSc2
nebyla	být	k5eNaImAgFnS
již	již	k9
panna	panna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karolina	Karolinum	k1gNnSc2
rovněž	rovněž	k9
považovala	považovat	k5eAaImAgFnS
Jiřího	Jiří	k1gMnSc4
za	za	k7c4
málo	málo	k1gNnSc4
atraktivního	atraktivní	k2eAgNnSc2d1
<g/>
;	;	kIx,
když	když	k8xS
přesně	přesně	k6eAd1
za	za	k7c4
devět	devět	k4xCc4
měsíců	měsíc	k1gInPc2
porodila	porodit	k5eAaPmAgFnS
dceru	dcera	k1gFnSc4
Šarlotu	Šarlota	k1gFnSc4
Augustu	August	k1gMnSc6
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1796	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
narození	narození	k1gNnSc1
malé	malý	k2eAgFnSc2d1
princezny	princezna	k1gFnSc2
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
"	"	kIx"
<g/>
zázračné	zázračný	k2eAgInPc4d1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
neboť	neboť	k8xC
jak	jak	k8xC,k8xS
vyplynulo	vyplynout	k5eAaPmAgNnS
z	z	k7c2
pozdější	pozdní	k2eAgFnSc2d2
princovy	princův	k2eAgFnSc2d1
korespondence	korespondence	k1gFnSc2
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
Jiří	Jiří	k1gMnSc1
za	za	k7c2
společného	společný	k2eAgInSc2d1
života	život	k1gInSc2
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
manželkou	manželka	k1gFnSc7
pouze	pouze	k6eAd1
tři	tři	k4xCgInPc1
intimní	intimní	k2eAgInPc1d1
kontakty	kontakt	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
dítě	dítě	k1gNnSc4
od	od	k7c2
matky	matka	k1gFnSc2
drasticky	drasticky	k6eAd1
odtrhl	odtrhnout	k5eAaPmAgInS
několik	několik	k4yIc4
dní	den	k1gInPc2
po	po	k7c6
narození	narození	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
měla	mít	k5eAaImAgFnS
Šarlota	Šarlota	k1gFnSc1
Augusta	Augusta	k1gMnSc1
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
<g/>
,	,	kIx,
její	její	k3xOp3gMnPc1
rodiče	rodič	k1gMnPc1
se	se	k3xPyFc4
formálně	formálně	k6eAd1
odloučili	odloučit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Regentství	regentství	k1gNnSc1
</s>
<s>
Jiřího	Jiří	k1gMnSc2
portrét	portrét	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1780-82	1780-82	k4
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
1810	#num#	k4
byl	být	k5eAaImAgMnS
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
po	po	k7c6
smrti	smrt	k1gFnSc6
své	svůj	k3xOyFgFnSc2
nejmladší	mladý	k2eAgFnSc2d3
dcery	dcera	k1gFnSc2
Amálie	Amálie	k1gFnSc2
postižen	postihnout	k5eAaPmNgInS
záchvatem	záchvat	k1gInSc7
nemoci	nemoc	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byla	být	k5eAaImAgFnS
označována	označovat	k5eAaImNgFnS
za	za	k7c4
šílenství	šílenství	k1gNnSc4
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
se	se	k3xPyFc4
soudí	soudit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
šlo	jít	k5eAaImAgNnS
o	o	k7c4
projevy	projev	k1gInPc4
porfyrie	porfyrie	k1gFnSc2
<g/>
,	,	kIx,
vážné	vážný	k2eAgFnSc2d1
metabolické	metabolický	k2eAgFnSc2d1
poruchy	porucha	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlament	parlament	k1gInSc1
jmenoval	jmenovat	k5eAaBmAgInS,k5eAaImAgInS
komisi	komise	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
odsouhlasila	odsouhlasit	k5eAaPmAgFnS
přijetí	přijetí	k1gNnSc3
zákona	zákon	k1gInSc2
o	o	k7c4
regentství	regentství	k1gNnSc4
(	(	kIx(
<g/>
Regency	Regenc	k2eAgMnPc4d1
Act	Act	k1gMnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
určil	určit	k5eAaPmAgMnS
Jiřího	Jiří	k1gMnSc4
IV	IV	kA
<g/>
.	.	kIx.
princem	princ	k1gMnSc7
regentem	regens	k1gMnSc7
a	a	k8xC
stanovil	stanovit	k5eAaPmAgMnS
některá	některý	k3yIgNnPc4
omezení	omezení	k1gNnPc4
jeho	jeho	k3xOp3gFnPc2
pravomocí	pravomoc	k1gFnPc2
jako	jako	k8xC,k8xS
regenta	regens	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
počátku	počátek	k1gInSc6
období	období	k1gNnSc2
jeho	jeho	k3xOp3gNnPc2
regentství	regentství	k1gNnPc2
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgInS
jeden	jeden	k4xCgInSc1
z	z	k7c2
největších	veliký	k2eAgInPc2d3
politických	politický	k2eAgInPc2d1
konfliktů	konflikt	k1gInPc2
spojený	spojený	k2eAgInSc1d1
s	s	k7c7
emancipací	emancipace	k1gFnSc7
katolíků	katolík	k1gMnPc2
a	a	k8xC
snahou	snaha	k1gFnSc7
odstranit	odstranit	k5eAaPmF
některá	některý	k3yIgNnPc4
omezení	omezení	k1gNnPc4
pro	pro	k7c4
členy	člen	k1gInPc4
katolické	katolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
v	v	k7c6
politice	politika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toryové	tory	k1gMnPc1
<g/>
,	,	kIx,
vedení	vedení	k1gNnSc2
premiérem	premiér	k1gMnSc7
Spencerem	Spencer	k1gMnSc7
Percevalem	Perceval	k1gMnSc7
emancipaci	emancipace	k1gFnSc4
katolíků	katolík	k1gMnPc2
odmítali	odmítat	k5eAaImAgMnP
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Whigové	whig	k1gMnPc1
ji	on	k3xPp3gFnSc4
podporovali	podporovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládalo	předpokládat	k5eAaImAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
Jiří	Jiří	k1gMnSc1
bude	být	k5eAaImBp3nS
podporovat	podporovat	k5eAaImF
Whigy	whig	k1gMnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
radu	rada	k1gFnSc4
své	svůj	k3xOyFgFnSc2
matky	matka	k1gFnSc2
tak	tak	k6eAd1
neučinil	učinit	k5eNaPmAgMnS
s	s	k7c7
vysvětlením	vysvětlení	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgFnSc1
změna	změna	k1gFnSc1
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
zhoršit	zhoršit	k5eAaPmF
zdravotní	zdravotní	k2eAgInSc4d1
stav	stav	k1gInSc4
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
otce	otec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Spencer	Spencer	k1gMnSc1
Perceval	Perceval	k1gMnSc1
byl	být	k5eAaImAgMnS
10	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1812	#num#	k4
zavražděn	zavraždit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
chtěl	chtít	k5eAaImAgMnS
ponechat	ponechat	k5eAaPmF
jeho	jeho	k3xOp3gMnPc4
ministry	ministr	k1gMnPc4
ve	v	k7c6
funkci	funkce	k1gFnSc6
a	a	k8xC
jmenovat	jmenovat	k5eAaImF,k5eAaBmF
nového	nový	k2eAgMnSc4d1
předsedu	předseda	k1gMnSc4
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
nátlak	nátlak	k1gInSc4
parlamentu	parlament	k1gInSc2
pověřil	pověřit	k5eAaPmAgInS
sestavením	sestavení	k1gNnSc7
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
obsahovala	obsahovat	k5eAaImAgFnS
zástupce	zástupce	k1gMnPc4
obou	dva	k4xCgFnPc2
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
nejdříve	dříve	k6eAd3
Richarda	Richard	k1gMnSc4
Wellesleye	Wellesley	k1gMnSc4
a	a	k8xC
poté	poté	k6eAd1
Francise	Francise	k1gFnSc1
Rawdona-Hastingse	Rawdona-Hastingse	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgMnPc1
neuspěli	uspět	k5eNaPmAgMnP
<g/>
,	,	kIx,
protože	protože	k8xS
žádná	žádný	k3yNgFnSc1
z	z	k7c2
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
nechtěla	chtít	k5eNaImAgFnS
ustoupit	ustoupit	k5eAaPmF
a	a	k8xC
tak	tak	k6eAd1
Jiří	Jiří	k1gMnSc1
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
předsedou	předseda	k1gMnSc7
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
sestavena	sestavit	k5eAaPmNgFnS
z	z	k7c2
Percevalových	Percevalův	k2eAgMnPc2d1
ministrů	ministr	k1gMnPc2
<g/>
,	,	kIx,
Roberta	Robert	k1gMnSc2
Banks-Jenkinsona	Banks-Jenkinson	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Antifrancouzská	Antifrancouzský	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
porazila	porazit	k5eAaPmAgFnS
napoleonské	napoleonský	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
roku	rok	k1gInSc2
1814	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následný	následný	k2eAgInSc1d1
Vídeňský	vídeňský	k2eAgInSc1d1
kongres	kongres	k1gInSc1
ustanovil	ustanovit	k5eAaPmAgInS
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
<g/>
,	,	kIx,
že	že	k8xS
Hannoverské	hannoverský	k2eAgNnSc1d1
kurfiřtství	kurfiřtství	k1gNnSc1
bude	být	k5eAaImBp3nS
povýšeno	povýšit	k5eAaPmNgNnS
na	na	k7c4
království	království	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napoleon	Napoleon	k1gMnSc1
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1815	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgMnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Waterloo	Waterloo	k1gNnSc2
znovu	znovu	k6eAd1
poražen	porazit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
téže	týž	k3xTgFnSc6,k3xDgFnSc6
době	doba	k1gFnSc6
byla	být	k5eAaImAgFnS
ukončena	ukončit	k5eAaPmNgFnS
bez	bez	k7c2
jasného	jasný	k2eAgMnSc2d1
vítěze	vítěz	k1gMnSc2
i	i	k8xC
britsko-americká	britsko-americký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Korunovační	korunovační	k2eAgFnSc1d1
oslava	oslava	k1gFnSc1
ve	v	k7c4
Westminster	Westminster	k1gInSc4
Hall	Halla	k1gFnPc2
</s>
<s>
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
Jiří	Jiří	k1gMnSc1
začal	začít	k5eAaPmAgMnS
aktivně	aktivně	k6eAd1
zajímat	zajímat	k5eAaImF
o	o	k7c4
styl	styl	k1gInSc4
a	a	k8xC
vkus	vkus	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
společníci	společník	k1gMnPc1
Beau	Beaus	k1gInSc2
Brummel	Brummel	k1gInSc1
a	a	k8xC
John	John	k1gMnSc1
Nash	Nash	k1gMnSc1
vytvořili	vytvořit	k5eAaPmAgMnP
regentský	regentský	k2eAgInSc4d1
sloh	sloh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nash	Nash	k1gMnSc1
vytvořil	vytvořit	k5eAaPmAgMnS
v	v	k7c6
Londýně	Londýn	k1gInSc6
Regent	regent	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Park	park	k1gInSc1
a	a	k8xC
Regent	regent	k1gMnSc1
Street	Street	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiřímu	Jiří	k1gMnSc3
se	se	k3xPyFc4
zalíbila	zalíbit	k5eAaPmAgFnS
myšlenka	myšlenka	k1gFnSc1
přímořských	přímořský	k2eAgFnPc2d1
lázní	lázeň	k1gFnPc2
a	a	k8xC
nechal	nechat	k5eAaPmAgInS
Nashe	Nashe	k1gInSc1
postavit	postavit	k5eAaPmF
v	v	k7c6
Brightonu	Brighton	k1gInSc6
královský	královský	k2eAgInSc4d1
pavilón	pavilón	k1gInSc4
inspirovaný	inspirovaný	k2eAgInSc4d1
indickým	indický	k2eAgMnSc7d1
Tádž	Tádž	k1gFnSc4
Mahalem	Mahal	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Vláda	vláda	k1gFnSc1
</s>
<s>
Po	po	k7c6
smrti	smrt	k1gFnSc6
Jiřího	Jiří	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
roku	rok	k1gInSc2
1820	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
jeho	jeho	k3xOp3gMnPc7
nástupcem	nástupce	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiřího	Jiří	k1gMnSc2
vztahy	vztah	k1gInPc4
s	s	k7c7
jeho	jeho	k3xOp3gFnSc7
manželkou	manželka	k1gFnSc7
byly	být	k5eAaImAgFnP
velmi	velmi	k6eAd1
chladné	chladný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odmítl	odmítnout	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
přiznat	přiznat	k5eAaPmF
práva	právo	k1gNnSc2
manželky	manželka	k1gFnSc2
krále	král	k1gMnSc2
a	a	k8xC
její	její	k3xOp3gNnSc1
jméno	jméno	k1gNnSc1
bylo	být	k5eAaImAgNnS
vynecháno	vynechat	k5eAaPmNgNnS
i	i	k9
v	v	k7c6
knize	kniha	k1gFnSc6
obecných	obecný	k2eAgFnPc2d1
modliteb	modlitba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
se	se	k3xPyFc4
pokoušel	pokoušet	k5eAaImAgMnS
dosáhnout	dosáhnout	k5eAaPmF
jejich	jejich	k3xOp3gInSc3
rozvodu	rozvod	k1gInSc3
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgMnS
od	od	k7c2
toho	ten	k3xDgNnSc2
svými	svůj	k3xOyFgMnPc7
přáteli	přítel	k1gMnPc7
zrazován	zrazován	k2eAgInSc4d1
<g/>
,	,	kIx,
protože	protože	k8xS
by	by	kYmCp3nP
vyšly	vyjít	k5eAaPmAgFnP
najevo	najevo	k6eAd1
podrobnosti	podrobnost	k1gFnPc1
o	o	k7c6
jeho	jeho	k3xOp3gInSc6
rozmařilém	rozmařilý	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1820	#num#	k4
dosáhnout	dosáhnout	k5eAaPmF
rozvodu	rozvod	k1gInSc6
návrhem	návrh	k1gInSc7
zákona	zákon	k1gInSc2
o	o	k7c6
pokutách	pokuta	k1gFnPc6
a	a	k8xC
trestech	trest	k1gInPc6
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
navrhoval	navrhovat	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
parlament	parlament	k1gInSc1
mohl	moct	k5eAaImAgInS
uvalit	uvalit	k5eAaPmF
trest	trest	k1gInSc1
bez	bez	k7c2
projednání	projednání	k1gNnSc2
záležitosti	záležitost	k1gFnSc2
před	před	k7c7
soudem	soud	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
tohoto	tento	k3xDgInSc2
zákona	zákon	k1gInSc2
chtěl	chtít	k5eAaImAgMnS
dosáhnout	dosáhnout	k5eAaPmF
anulování	anulování	k1gNnSc4
svého	svůj	k3xOyFgInSc2
sňatku	sňatek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
zákon	zákon	k1gInSc1
se	se	k3xPyFc4
ale	ale	k8xC
setkal	setkat	k5eAaPmAgMnS
s	s	k7c7
velkým	velký	k2eAgInSc7d1
odporem	odpor	k1gInSc7
u	u	k7c2
veřejnosti	veřejnost	k1gFnSc2
a	a	k8xC
byl	být	k5eAaImAgInS
z	z	k7c2
parlamentu	parlament	k1gInSc2
stažen	stáhnout	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
Karolínu	Karolína	k1gFnSc4
vyloučit	vyloučit	k5eAaPmF
z	z	k7c2
účasti	účast	k1gFnSc2
na	na	k7c6
své	svůj	k3xOyFgFnSc6
korunovaci	korunovace	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1821	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tentýž	týž	k3xTgInSc4
den	den	k1gInSc4
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
onemocněla	onemocnět	k5eAaPmAgFnS
a	a	k8xC
7	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
zemřela	zemřít	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2
část	část	k1gFnSc1
pozdního	pozdní	k2eAgNnSc2d1
období	období	k1gNnSc2
své	svůj	k3xOyFgFnSc2
vlády	vláda	k1gFnSc2
strávil	strávit	k5eAaPmAgMnS
v	v	k7c6
odloučení	odloučení	k1gNnSc6
na	na	k7c6
hradu	hrad	k1gInSc6
Windsor	Windsor	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
stále	stále	k6eAd1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
zasahovat	zasahovat	k5eAaImF
do	do	k7c2
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
se	se	k3xPyFc4
zdálo	zdát	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
podporovat	podporovat	k5eAaImF
emancipaci	emancipace	k1gFnSc3
katolíků	katolík	k1gMnPc2
<g/>
,	,	kIx,
když	když	k8xS
navrhl	navrhnout	k5eAaPmAgInS
zákon	zákon	k1gInSc1
o	o	k7c4
emancipaci	emancipace	k1gFnSc4
katolíků	katolík	k1gMnPc2
v	v	k7c6
Irsku	Irsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc1
protikatolické	protikatolický	k2eAgInPc1d1
názory	názor	k1gInPc1
vyšly	vyjít	k5eAaPmAgInP
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
uvítal	uvítat	k5eAaPmAgMnS
neúspěch	neúspěch	k1gInSc4
přijetí	přijetí	k1gNnSc2
zákona	zákon	k1gInSc2
o	o	k7c6
jejich	jejich	k3xOp3gFnSc6
emancipaci	emancipace	k1gFnSc6
roku	rok	k1gInSc2
1813	#num#	k4
a	a	k8xC
když	když	k8xS
se	se	k3xPyFc4
veřejně	veřejně	k6eAd1
vyjádřil	vyjádřit	k5eAaPmAgMnS
proti	proti	k7c3
katolíkům	katolík	k1gMnPc3
roku	rok	k1gInSc2
1824	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
velkých	velký	k2eAgInPc6d1
politických	politický	k2eAgInPc6d1
sporech	spor	k1gInPc6
byl	být	k5eAaImAgInS
nakonec	nakonec	k6eAd1
zákon	zákon	k1gInSc1
o	o	k7c4
emancipaci	emancipace	k1gFnSc4
katolíků	katolík	k1gMnPc2
přijat	přijat	k2eAgInSc1d1
roku	rok	k1gInSc2
1829	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pozdní	pozdní	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
záliba	záliba	k1gFnSc1
ve	v	k7c6
velkých	velký	k2eAgFnPc6d1
hostinách	hostina	k1gFnPc6
a	a	k8xC
pití	pití	k1gNnSc6
velkého	velký	k2eAgNnSc2d1
množství	množství	k1gNnSc2
alkoholu	alkohol	k1gInSc2
mělo	mít	k5eAaImAgNnS
vliv	vliv	k1gInSc4
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
zdraví	zdraví	k1gNnSc4
a	a	k8xC
otylost	otylost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
výjimečně	výjimečně	k6eAd1
objevil	objevit	k5eAaPmAgMnS
na	na	k7c6
veřejnosti	veřejnost	k1gFnSc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
tloušťku	tloušťka	k1gFnSc4
terčem	terč	k1gInSc7
posměchu	posměch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
časných	časný	k2eAgFnPc6d1
ranních	ranní	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
26	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1830	#num#	k4
na	na	k7c6
windsorském	windsorský	k2eAgInSc6d1
hradu	hrad	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
také	také	k9
v	v	k7c6
kapli	kaple	k1gFnSc6
svatého	svatý	k2eAgMnSc2d1
Jiří	Jiří	k1gMnSc2
pohřben	pohřben	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Následnictví	následnictví	k1gNnSc1
</s>
<s>
Nefunkční	funkční	k2eNgNnSc1d1
manželství	manželství	k1gNnSc1
Jiřího	Jiří	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
znamenalo	znamenat	k5eAaImAgNnS
i	i	k9
ohrožení	ohrožení	k1gNnSc1
trvání	trvání	k1gNnSc2
dynastie	dynastie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
jediná	jediný	k2eAgFnSc1d1
legitimní	legitimní	k2eAgFnSc1d1
dcera	dcera	k1gFnSc1
Šarlota	Šarlota	k1gFnSc1
Augusta	August	k1gMnSc2
zemřela	zemřít	k5eAaPmAgFnS
roku	rok	k1gInSc2
1817	#num#	k4
po	po	k7c6
porodu	porod	k1gInSc6
mrtvého	mrtvý	k2eAgNnSc2d1
dítěte	dítě	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
smrt	smrt	k1gFnSc4
vyvolala	vyvolat	k5eAaPmAgFnS
obrovský	obrovský	k2eAgInSc4d1
národní	národní	k2eAgInSc4d1
smutek	smutek	k1gInSc4
(	(	kIx(
<g/>
bývá	bývat	k5eAaImIp3nS
srovnáván	srovnáván	k2eAgMnSc1d1
se	s	k7c7
smutkem	smutek	k1gInSc7
po	po	k7c6
smrti	smrt	k1gFnSc6
princezny	princezna	k1gFnSc2
Diany	Diana	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šarlota	Šarlota	k1gFnSc1
Augusta	August	k1gMnSc4
byla	být	k5eAaImAgFnS
jediná	jediný	k2eAgFnSc1d1
zákonitá	zákonitý	k2eAgFnSc1d1
dcera	dcera	k1gFnSc1
waleského	waleský	k2eAgMnSc2d1
prince	princ	k1gMnSc2
a	a	k8xC
po	po	k7c6
její	její	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
pozbyl	pozbýt	k5eAaPmAgMnS
naděje	naděje	k1gFnPc4
na	na	k7c4
zákonité	zákonitý	k2eAgMnPc4d1
potomky	potomek	k1gMnPc4
a	a	k8xC
tedy	tedy	k9
následníky	následník	k1gMnPc4
v	v	k7c6
přímé	přímý	k2eAgFnSc6d1
linii	linie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
její	její	k3xOp3gMnSc1
děd	děd	k1gMnSc1
<g/>
,	,	kIx,
neměl	mít	k5eNaImAgMnS
kromě	kromě	k7c2
ní	on	k3xPp3gFnSc2
jiných	jiný	k2eAgMnPc2d1
legitimních	legitimní	k2eAgMnPc2d1
vnuků	vnuk	k1gMnPc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
měl	mít	k5eAaImAgMnS
12	#num#	k4
dospělých	dospělý	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
-	-	kIx~
další	další	k2eAgMnPc1d1
synové	syn	k1gMnPc1
krále	král	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
totiž	totiž	k9
neměli	mít	k5eNaImAgMnP
děti	dítě	k1gFnPc4
nebo	nebo	k8xC
byli	být	k5eAaImAgMnP
svobodní	svobodný	k2eAgMnPc1d1
nebo	nebo	k8xC
jejich	jejich	k3xOp3gFnPc1
děti	dítě	k1gFnPc1
nebyly	být	k5eNaImAgFnP
za	za	k7c4
legitimní	legitimní	k2eAgInPc4d1
uznány	uznán	k2eAgInPc4d1
<g/>
:	:	kIx,
</s>
<s>
Frederik	Frederik	k1gMnSc1
August	August	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
další	další	k2eAgInPc1d1
v	v	k7c6
nástupnické	nástupnický	k2eAgFnSc6d1
linii	linie	k1gFnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
rozvedl	rozvést	k5eAaPmAgMnS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
ženou	žena	k1gFnSc7
po	po	k7c6
27	#num#	k4
letech	let	k1gInPc6
bezdětného	bezdětný	k2eAgNnSc2d1
manželství	manželství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
tři	tři	k4xCgMnPc1
synové	syn	k1gMnPc1
Jiřího	Jiří	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Vilém	Vilém	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
Clarence	Clarence	k1gFnSc2
(	(	kIx(
<g/>
pozdější	pozdní	k2eAgMnSc1d2
král	král	k1gMnSc1
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Adolf	Adolf	k1gMnSc1
Frederik	Frederik	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Cambridge	Cambridge	k1gFnSc2
a	a	k8xC
Eduard	Eduard	k1gMnSc1
August	August	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Kentu	Kent	k1gInSc2
uzavřeli	uzavřít	k5eAaPmAgMnP
rychle	rychle	k6eAd1
manželství	manželství	k1gNnSc4
se	s	k7c7
záměrem	záměr	k1gInSc7
zplodit	zplodit	k5eAaPmF
tolik	tolik	k6eAd1
vytouženého	vytoužený	k2eAgMnSc4d1
následníka	následník	k1gMnSc4
trůnu	trůn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgMnPc1d1
dva	dva	k4xCgMnPc1
bratři	bratr	k1gMnPc1
<g/>
,	,	kIx,
Ernst	Ernst	k1gMnSc1
August	August	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
Cumberladu	Cumberlad	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
August	August	k1gMnSc1
Frederik	Frederik	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
Sussexu	Sussex	k1gInSc2
<g/>
,	,	kIx,
již	již	k6eAd1
sice	sice	k8xC
byli	být	k5eAaImAgMnP
ženatí	ženatý	k2eAgMnPc1d1
<g/>
,	,	kIx,
bohužel	bohužel	k9
v	v	k7c6
tomto	tento	k3xDgInSc6
okamžiku	okamžik	k1gInSc6
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Cumberlandu	Cumberland	k1gInSc2
měl	mít	k5eAaImAgInS
dvě	dva	k4xCgFnPc4
mrtvě	mrtvě	k6eAd1
narozené	narozený	k2eAgFnPc4d1
dcery	dcera	k1gFnPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
děti	dítě	k1gFnPc1
vévody	vévoda	k1gMnSc2
ze	z	k7c2
Sussexu	Sussex	k1gInSc2
byly	být	k5eAaImAgFnP
vyloučeny	vyloučit	k5eAaPmNgFnP
z	z	k7c2
následnictví	následnictví	k1gNnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
manželství	manželství	k1gNnSc1
jejich	jejich	k3xOp3gMnPc2
rodičů	rodič	k1gMnPc2
<g/>
,	,	kIx,
uzavřené	uzavřený	k2eAgFnPc1d1
bez	bez	k7c2
králova	králův	k2eAgInSc2d1
souhlasu	souhlas	k1gInSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
jako	jako	k9
takové	takový	k3xDgNnSc1
podle	podle	k7c2
Royal	Royal	k1gInSc4
Marriages	Marriages	k1gInSc1
Act	Act	k1gFnSc1
1772	#num#	k4
z	z	k7c2
roku	rok	k1gInSc2
1772	#num#	k4
neplatné	platný	k2eNgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
záchranu	záchrana	k1gFnSc4
dynastie	dynastie	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1818	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
50	#num#	k4
let	léto	k1gNnPc2
oženil	oženit	k5eAaPmAgMnS
i	i	k9
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Kentu	Kent	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
s	s	k7c7
princeznou	princezna	k1gFnSc7
Viktorií	Viktoria	k1gFnSc7
z	z	k7c2
dynastie	dynastie	k1gFnSc2
Sachsen-Coburg-Saalfeldské	Sachsen-Coburg-Saalfeldský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
manželství	manželství	k1gNnSc2
se	se	k3xPyFc4
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
roku	rok	k1gInSc2
1819	#num#	k4
narodila	narodit	k5eAaPmAgFnS
dcera	dcera	k1gFnSc1
<g/>
,	,	kIx,
princezna	princezna	k1gFnSc1
Viktorie	Viktorie	k1gFnSc1
z	z	k7c2
Kentu	Kent	k1gInSc2
<g/>
,	,	kIx,
budoucí	budoucí	k2eAgFnSc1d1
královna	královna	k1gFnSc1
Viktorie	Viktoria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
zemřel	zemřít	k5eAaPmAgMnS
ve	v	k7c6
věku	věk	k1gInSc6
68	#num#	k4
let	léto	k1gNnPc2
26	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1830	#num#	k4
ve	v	k7c6
Windsoru	Windsor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
že	že	k8xS
jeho	jeho	k3xOp3gMnSc1
mladší	mladý	k2eAgMnSc1d2
bratr	bratr	k1gMnSc1
princ	princ	k1gMnSc1
Frederik	Frederik	k1gMnSc1
<g/>
,	,	kIx,
druhý	druhý	k4xOgMnSc1
syn	syn	k1gMnSc1
Jiřího	Jiří	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
již	již	k6eAd1
roku	rok	k1gInSc2
1827	#num#	k4
<g/>
,	,	kIx,
nastoupil	nastoupit	k5eAaPmAgMnS
po	po	k7c6
smrti	smrt	k1gFnSc6
Jiřího	Jiří	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
na	na	k7c4
britský	britský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
jeho	jeho	k3xOp3gMnSc1
další	další	k2eAgMnSc1d1
bratr	bratr	k1gMnSc1
<g/>
,	,	kIx,
třetí	třetí	k4xOgMnSc1
syn	syn	k1gMnSc1
Jiřího	Jiří	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
princ	princ	k1gMnSc1
Vilém	Vilém	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Clarence	Clarence	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
vládl	vládnout	k5eAaImAgMnS
jako	jako	k9
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
se	s	k7c7
královnou	královna	k1gFnSc7
stala	stát	k5eAaPmAgFnS
Viktorie	Viktorie	k1gFnSc1
<g/>
,	,	kIx,
jediná	jediný	k2eAgFnSc1d1
dcera	dcera	k1gFnSc1
čtvrtého	čtvrtý	k4xOgInSc2
syna	syn	k1gMnSc4
Jiřího	Jiří	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduarda	Eduard	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Jiří	Jiří	k1gMnSc1
I.	I.	kA
</s>
<s>
Jiří	Jiří	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Žofie	Žofie	k1gFnSc1
Dorotea	Dorote	k1gInSc2
z	z	k7c2
Celle	Celle	k1gFnSc2
</s>
<s>
Frederik	Frederik	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Hannoverský	hannoverský	k2eAgMnSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Braniborsko-Ansbašský	Braniborsko-Ansbašský	k2eAgMnSc1d1
</s>
<s>
Karolina	Karolinum	k1gNnPc1
z	z	k7c2
Ansbachu	Ansbach	k1gInSc2
</s>
<s>
Eleonora	Eleonora	k1gFnSc1
Sasko-Eisenašská	Sasko-Eisenašský	k2eAgFnSc1d1
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc1d1
</s>
<s>
Magdalena	Magdalena	k1gFnSc1
Sibyla	Sibyla	k1gFnSc1
Sasko-Weissenfelská	Sasko-Weissenfelský	k2eAgFnSc1d1
</s>
<s>
Augusta	Augusta	k1gMnSc1
Sasko-Gothajská	sasko-gothajský	k2eAgNnPc4d1
</s>
<s>
Karel	Karel	k1gMnSc1
Anhaltsko-Zerbstský	Anhaltsko-Zerbstský	k2eAgMnSc1d1
</s>
<s>
Magdalena	Magdalena	k1gFnSc1
Augusta	August	k1gMnSc2
Anhaltsko-Zerbstská	Anhaltsko-Zerbstský	k2eAgFnSc5d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Sasko-Weissenfelská	Sasko-Weissenfelský	k2eAgFnSc1d1
</s>
<s>
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Adolf	Adolf	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Meklenburský	meklenburský	k2eAgInSc4d1
</s>
<s>
Adolf	Adolf	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meklenbursko-Střelický	Meklenbursko-Střelický	k2eAgInSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Kateřina	Kateřina	k1gFnSc1
Brunšvicko-Dannenberská	Brunšvicko-Dannenberský	k2eAgFnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Meklenbursko-Střelický	Meklenbursko-Střelický	k2eAgMnSc1d1
</s>
<s>
Kristián	Kristián	k1gMnSc1
Vilém	Vilém	k1gMnSc1
I.	I.	kA
Schwarzbursko-Sondershausenský	Schwarzbursko-Sondershausenský	k2eAgMnSc1d1
</s>
<s>
Christiana	Christian	k1gMnSc4
Emilie	Emilie	k1gFnSc2
Schwarzbursko-Sondershausenská	Schwarzbursko-Sondershausenský	k2eAgFnSc1d1
</s>
<s>
Antonie	Antonie	k1gFnSc1
Sibyla	Sibyla	k1gFnSc1
Barby-Mühlingenská	Barby-Mühlingenský	k2eAgFnSc1d1
</s>
<s>
Šarlota	Šarlota	k1gFnSc1
Meklenbursko-Střelická	Meklenbursko-Střelický	k2eAgFnSc1d1
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
Sasko-Hildburghausenský	Sasko-Hildburghausenský	k2eAgMnSc1d1
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Sasko-Hildburghausenský	Sasko-Hildburghausenský	k2eAgInSc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Henrieta	Henriet	k2eAgFnSc1d1
Waldecká	Waldecký	k2eAgFnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Sasko-Hildburghausenská	Sasko-Hildburghausenský	k2eAgFnSc1d1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
I.	I.	kA
Erbašský	Erbašský	k2eAgMnSc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Albertina	Albertin	k2eAgFnSc1d1
z	z	k7c2
Erbachu	Erbach	k1gInSc2
</s>
<s>
Amálie	Amálie	k1gFnSc1
Kateřina	Kateřina	k1gFnSc1
Waldecko-Eisenberská	Waldecko-Eisenberský	k2eAgFnSc1d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
George	Georg	k1gMnSc2
IV	IV	kA
of	of	k?
the	the	k?
United	United	k1gInSc1
Kingdom	Kingdom	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgInSc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Britští	britský	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
Království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
(	(	kIx(
<g/>
1707	#num#	k4
<g/>
–	–	k?
<g/>
1801	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Anna	Anna	k1gFnSc1
Stuartovna	Stuartovna	k1gFnSc1
(	(	kIx(
<g/>
1702	#num#	k4
<g/>
–	–	k?
<g/>
1714	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1714	#num#	k4
<g/>
–	–	k?
<g/>
1727	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1727	#num#	k4
<g/>
–	–	k?
<g/>
1760	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1760	#num#	k4
<g/>
–	–	k?
<g/>
1801	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Irska	Irsko	k1gNnSc2
(	(	kIx(
<g/>
1801	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1801	#num#	k4
<g/>
–	–	k?
<g/>
1820	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1820	#num#	k4
<g/>
–	–	k?
<g/>
1830	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britský	britský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1830	#num#	k4
<g/>
–	–	k?
<g/>
1837	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
(	(	kIx(
<g/>
1837	#num#	k4
<g/>
–	–	k?
<g/>
1901	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1901	#num#	k4
<g/>
–	–	k?
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Severního	severní	k2eAgNnSc2d1
Irska	Irsko	k1gNnSc2
(	(	kIx(
<g/>
od	od	k7c2
r.	r.	kA
1927	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
–	–	k?
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eduard	Eduard	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
od	od	k7c2
1952	#num#	k4
<g/>
)	)	kIx)
1	#num#	k4
<g/>
Rovněž	rovněž	k9
vládce	vládce	k1gMnSc2
Irska	Irsko	k1gNnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jx	jx	k?
<g/>
20090420002	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118690450	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0003	#num#	k4
8236	#num#	k4
8062	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50047916	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500234768	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
265481029	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50047916	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
