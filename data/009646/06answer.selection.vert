<s>
Západoestonské	Západoestonský	k2eAgNnSc1d1	Západoestonský
souostroví	souostroví	k1gNnSc1	souostroví
(	(	kIx(	(
<g/>
estonsky	estonsky	k6eAd1	estonsky
Lääne-Eesti	Lääne-Eest	k1gFnPc1	Lääne-Eest
saarestik	saarestika	k1gFnPc2	saarestika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Baltském	baltský	k2eAgNnSc6d1	Baltské
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
územně	územně	k6eAd1	územně
náležejících	náležející	k2eAgNnPc2d1	náležející
k	k	k7c3	k
Estonsku	Estonsko	k1gNnSc3	Estonsko
<g/>
.	.	kIx.	.
</s>
