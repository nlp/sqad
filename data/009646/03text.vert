<p>
<s>
Západoestonské	Západoestonský	k2eAgNnSc1d1	Západoestonský
souostroví	souostroví	k1gNnSc1	souostroví
(	(	kIx(	(
<g/>
estonsky	estonsky	k6eAd1	estonsky
Lääne-Eesti	Lääne-Eest	k1gFnPc1	Lääne-Eest
saarestik	saarestika	k1gFnPc2	saarestika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Baltském	baltský	k2eAgNnSc6d1	Baltské
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
územně	územně	k6eAd1	územně
náležejících	náležející	k2eAgNnPc2d1	náležející
k	k	k7c3	k
Estonsku	Estonsko	k1gNnSc3	Estonsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
souostroví	souostroví	k1gNnSc3	souostroví
patří	patřit	k5eAaImIp3nP	patřit
čtyři	čtyři	k4xCgInPc1	čtyři
největší	veliký	k2eAgInPc1d3	veliký
estonské	estonský	k2eAgInPc1d1	estonský
ostrovy	ostrov	k1gInPc1	ostrov
Saaremaa	Saaremaum	k1gNnSc2	Saaremaum
<g/>
,	,	kIx,	,
Hiiumaa	Hiiumaum	k1gNnSc2	Hiiumaum
<g/>
,	,	kIx,	,
Muhu	Muhus	k1gInSc2	Muhus
a	a	k8xC	a
Vormsi	Vormse	k1gFnSc4	Vormse
a	a	k8xC	a
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
menších	malý	k2eAgInPc2d2	menší
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
plocha	plocha	k1gFnSc1	plocha
ostrovů	ostrov	k1gInPc2	ostrov
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
4000	[number]	k4	4000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
přírody	příroda	k1gFnSc2	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
Viidumäe	Viidumä	k1gInSc2	Viidumä
a	a	k8xC	a
Vilsandi	Vilsand	k1gMnPc1	Vilsand
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
bylo	být	k5eAaImAgNnS	být
Západoestonské	Západoestonský	k2eAgNnSc1d1	Západoestonský
souostroví	souostroví	k1gNnSc1	souostroví
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
biosférických	biosférický	k2eAgFnPc2d1	biosférická
rezervací	rezervace	k1gFnPc2	rezervace
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Rezervace	rezervace	k1gFnSc1	rezervace
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
území	území	k1gNnSc4	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
1	[number]	k4	1
518	[number]	k4	518
309,7	[number]	k4	309,7
ha	ha	kA	ha
v	v	k7c6	v
krajích	kraj	k1gInPc6	kraj
Hiiumaa	Hiiumaum	k1gNnSc2	Hiiumaum
<g/>
,	,	kIx,	,
Saaremaa	Saaremaum	k1gNnSc2	Saaremaum
a	a	k8xC	a
Läänemaa	Läänemaum	k1gNnSc2	Läänemaum
<g/>
.	.	kIx.	.
</s>
<s>
Správa	správa	k1gFnSc1	správa
rezervace	rezervace	k1gFnSc2	rezervace
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Kärdla	Kärdla	k1gMnSc2	Kärdla
<g/>
,	,	kIx,	,
správním	správní	k2eAgNnSc6d1	správní
středisku	středisko	k1gNnSc6	středisko
kraje	kraj	k1gInSc2	kraj
Hiiumaa	Hiiuma	k1gInSc2	Hiiuma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ostrovů	ostrov	k1gInPc2	ostrov
Estonska	Estonsko	k1gNnSc2	Estonsko
</s>
</p>
