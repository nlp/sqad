<s>
Kytara	kytara	k1gFnSc1	kytara
je	být	k5eAaImIp3nS	být
drnkací	drnkací	k2eAgInSc1d1	drnkací
strunný	strunný	k2eAgInSc1d1	strunný
nástroj	nástroj	k1gInSc1	nástroj
(	(	kIx(	(
<g/>
chordofon	chordofon	k1gInSc1	chordofon
<g/>
)	)	kIx)	)
s	s	k7c7	s
hmatníkem	hmatník	k1gInSc7	hmatník
<g/>
.	.	kIx.	.
</s>
<s>
Tón	tón	k1gInSc1	tón
vzniká	vznikat	k5eAaImIp3nS	vznikat
rozechvěním	rozechvění	k1gNnSc7	rozechvění
struny	struna	k1gFnSc2	struna
napjaté	napjatý	k2eAgFnSc2d1	napjatá
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
pevnými	pevný	k2eAgInPc7d1	pevný
body	bod	k1gInPc7	bod
–	–	k?	–
nultým	nultý	k4xOgInSc7	nultý
pražcem	pražec	k1gInSc7	pražec
a	a	k8xC	a
vložkou	vložka	k1gFnSc7	vložka
kobylky	kobylka	k1gFnSc2	kobylka
<g/>
.	.	kIx.	.
</s>
<s>
Struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
rozechvívány	rozechvívat	k5eAaImNgFnP	rozechvívat
drnkáním	drnkání	k1gNnSc7	drnkání
prsty	prst	k1gInPc7	prst
nebo	nebo	k8xC	nebo
plektrem	plektron	k1gNnSc7	plektron
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
trsátko	trsátko	k1gNnSc1	trsátko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hmatník	hmatník	k1gInSc1	hmatník
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
získávat	získávat	k5eAaImF	získávat
další	další	k2eAgInPc4d1	další
tóny	tón	k1gInPc4	tón
zkracováním	zkracování	k1gNnSc7	zkracování
chvějné	chvějný	k2eAgFnSc2d1	chvějný
délky	délka	k1gFnSc2	délka
struny	struna	k1gFnSc2	struna
přitlačením	přitlačení	k1gNnSc7	přitlačení
struny	struna	k1gFnSc2	struna
na	na	k7c4	na
pražec	pražec	k1gInSc4	pražec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
akustického	akustický	k2eAgNnSc2d1	akustické
patří	patřit	k5eAaImIp3nS	patřit
kytara	kytara	k1gFnSc1	kytara
mezi	mezi	k7c4	mezi
nástroje	nástroj	k1gInPc4	nástroj
s	s	k7c7	s
doznívajícím	doznívající	k2eAgInSc7d1	doznívající
tónem	tón	k1gInSc7	tón
<g/>
.	.	kIx.	.
</s>
<s>
Kytara	kytara	k1gFnSc1	kytara
je	být	k5eAaImIp3nS	být
akordický	akordický	k2eAgInSc4d1	akordický
nástroj	nástroj	k1gInSc4	nástroj
–	–	k?	–
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jednohlasou	jednohlasý	k2eAgFnSc4d1	jednohlasá
i	i	k8xC	i
vícehlasou	vícehlasý	k2eAgFnSc4d1	vícehlasá
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
kytara	kytara	k1gFnSc1	kytara
má	mít	k5eAaImIp3nS	mít
šest	šest	k4xCc4	šest
strun	struna	k1gFnPc2	struna
povětšinou	povětšinou	k6eAd1	povětšinou
laděných	laděný	k2eAgInPc2d1	laděný
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
ehgdae	ehgdae	k6eAd1	ehgdae
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
snadnější	snadný	k2eAgNnSc4d2	snazší
zapamatování	zapamatování	k1gNnSc4	zapamatování
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
věta	věta	k1gFnSc1	věta
<g/>
:	:	kIx,	:
Emil	Emil	k1gMnSc1	Emil
hodil	hodit	k5eAaImAgMnS	hodit
granát	granát	k1gInSc4	granát
do	do	k7c2	do
atomové	atomový	k2eAgFnSc2d1	atomová
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Notuje	notovat	k5eAaImIp3nS	notovat
se	se	k3xPyFc4	se
v	v	k7c6	v
houslovém	houslový	k2eAgInSc6d1	houslový
klíči	klíč	k1gInSc6	klíč
a	a	k8xC	a
zní	znět	k5eAaImIp3nS	znět
o	o	k7c4	o
oktávu	oktáva	k1gFnSc4	oktáva
níže	nízce	k6eAd2	nízce
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
kytary	kytara	k1gFnSc2	kytara
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ladění	ladění	k1gNnSc2	ladění
E	E	kA	E
A	A	kA	A
d	d	k?	d
g	g	kA	g
h	h	k?	h
e	e	k0	e
<g/>
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
E	E	kA	E
<g/>
–	–	k?	–
<g/>
h	h	k?	h
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáctistrunné	dvanáctistrunný	k2eAgFnPc1d1	dvanáctistrunná
kytary	kytara	k1gFnPc1	kytara
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
také	také	k9	také
používají	používat	k5eAaImIp3nP	používat
kytary	kytara	k1gFnPc1	kytara
dvanáctistrunné	dvanáctistrunný	k2eAgFnPc1d1	dvanáctistrunná
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
každá	každý	k3xTgFnSc1	každý
struna	struna	k1gFnSc1	struna
zdvojena	zdvojen	k2eAgFnSc1d1	zdvojena
<g/>
.	.	kIx.	.
</s>
<s>
Dvojice	dvojice	k1gFnSc1	dvojice
strun	struna	k1gFnPc2	struna
h	h	k?	h
a	a	k8xC	a
e	e	k0	e
<g/>
1	[number]	k4	1
jsou	být	k5eAaImIp3nP	být
laděny	laděn	k2eAgFnPc1d1	laděna
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
u	u	k7c2	u
strun	struna	k1gFnPc2	struna
E	E	kA	E
A	a	k8xC	a
d	d	k?	d
g	g	kA	g
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
druhá	druhý	k4xOgFnSc1	druhý
struna	struna	k1gFnSc1	struna
naladěna	naladěn	k2eAgFnSc1d1	naladěna
o	o	k7c4	o
oktávu	oktáva	k1gFnSc4	oktáva
výš	vysoce	k6eAd2	vysoce
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáctistrunné	dvanáctistrunný	k2eAgFnPc1d1	dvanáctistrunná
kytary	kytara	k1gFnPc1	kytara
mají	mít	k5eAaImIp3nP	mít
bohatší	bohatý	k2eAgInSc4d2	bohatší
zvuk	zvuk	k1gInSc4	zvuk
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
country	country	k2eAgFnSc6d1	country
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
časté	častý	k2eAgNnSc1d1	časté
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zvukově	zvukově	k6eAd1	zvukově
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
zajímavější	zajímavý	k2eAgMnSc1d2	zajímavější
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
též	též	k9	též
ladění	ladění	k1gNnSc4	ladění
do	do	k7c2	do
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
akordu	akord	k1gInSc2	akord
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
G	G	kA	G
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Při	při	k7c6	při
ladění	ladění	k1gNnSc6	ladění
do	do	k7c2	do
G	G	kA	G
se	se	k3xPyFc4	se
struny	struna	k1gFnPc4	struna
E	E	kA	E
<g/>
,	,	kIx,	,
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
<g/>
e	e	k0	e
<g/>
1	[number]	k4	1
podladí	podladit	k5eAaPmIp3nS	podladit
o	o	k7c4	o
celý	celý	k2eAgInSc4d1	celý
tón	tón	k1gInSc4	tón
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
na	na	k7c6	na
D	D	kA	D
<g/>
,	,	kIx,	,
<g/>
G	G	kA	G
<g/>
,	,	kIx,	,
<g/>
d	d	k?	d
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
je	být	k5eAaImIp3nS	být
přirozeně	přirozeně	k6eAd1	přirozeně
třeba	třeba	k6eAd1	třeba
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
akordické	akordický	k2eAgInPc4d1	akordický
hmaty	hmat	k1gInPc4	hmat
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
mj.	mj.	kA	mj.
v	v	k7c6	v
blues	blues	k1gFnSc6	blues
a	a	k8xC	a
country	country	k2eAgInSc6d1	country
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
počty	počet	k1gInPc1	počet
strun	struna	k1gFnPc2	struna
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
některé	některý	k3yIgFnPc1	některý
varianty	varianta	k1gFnPc1	varianta
kytar	kytara	k1gFnPc2	kytara
s	s	k7c7	s
9	[number]	k4	9
nebo	nebo	k8xC	nebo
7	[number]	k4	7
strunami	struna	k1gFnPc7	struna
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
kytary	kytara	k1gFnPc1	kytara
basové	basový	k2eAgFnPc1d1	basová
a	a	k8xC	a
kytary	kytara	k1gFnPc1	kytara
havajské	havajský	k2eAgFnPc1d1	Havajská
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
též	též	k9	též
ladí	ladit	k5eAaImIp3nP	ladit
do	do	k7c2	do
akordu	akord	k1gInSc2	akord
a	a	k8xC	a
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
tedy	tedy	k9	tedy
odlišný	odlišný	k2eAgInSc4d1	odlišný
styl	styl	k1gInSc4	styl
hraní	hraní	k1gNnSc2	hraní
<g/>
.	.	kIx.	.
</s>
<s>
Korpus	korpus	k1gInSc1	korpus
Vrchní	vrchní	k2eAgFnSc1d1	vrchní
deska	deska	k1gFnSc1	deska
je	být	k5eAaImIp3nS	být
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
částí	část	k1gFnSc7	část
korpusu	korpus	k1gInSc2	korpus
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
podstatný	podstatný	k2eAgInSc4d1	podstatný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
zvuku	zvuk	k1gInSc2	zvuk
nástroje	nástroj	k1gInSc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Přejímá	přejímat	k5eAaImIp3nS	přejímat
kobylkou	kobylka	k1gFnSc7	kobylka
chvění	chvění	k1gNnSc2	chvění
strun	struna	k1gFnPc2	struna
a	a	k8xC	a
zesiluje	zesilovat	k5eAaImIp3nS	zesilovat
jejich	jejich	k3xOp3gInSc4	jejich
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
dává	dávat	k5eAaImIp3nS	dávat
zabarvení	zabarvení	k1gNnSc4	zabarvení
<g/>
,	,	kIx,	,
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
určuje	určovat	k5eAaImIp3nS	určovat
délku	délka	k1gFnSc4	délka
tónu	tón	k1gInSc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgFnSc1d1	vrchní
deska	deska	k1gFnSc1	deska
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zhotovena	zhotovit	k5eAaPmNgFnS	zhotovit
z	z	k7c2	z
kvalitního	kvalitní	k2eAgNnSc2d1	kvalitní
dřeva	dřevo	k1gNnSc2	dřevo
hustých	hustý	k2eAgInPc2d1	hustý
<g/>
,	,	kIx,	,
pravidelných	pravidelný	k2eAgNnPc2d1	pravidelné
a	a	k8xC	a
rovných	rovný	k2eAgNnPc2d1	rovné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgNnSc7d3	nejlepší
ozvučným	ozvučný	k2eAgNnSc7d1	ozvučné
dřevem	dřevo	k1gNnSc7	dřevo
je	být	k5eAaImIp3nS	být
rezonanční	rezonanční	k2eAgInSc1d1	rezonanční
smrk	smrk	k1gInSc1	smrk
–	–	k?	–
lískovec	lískovec	k1gInSc1	lískovec
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
cedr	cedr	k1gInSc1	cedr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
vrchní	vrchní	k1gFnSc2	vrchní
–	–	k?	–
rezonanční	rezonanční	k2eAgFnSc2d1	rezonanční
–	–	k?	–
desky	deska	k1gFnSc2	deska
je	být	k5eAaImIp3nS	být
ozvučný	ozvučný	k2eAgInSc1d1	ozvučný
otvor	otvor	k1gInSc1	otvor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
velikost	velikost	k1gFnSc1	velikost
<g/>
,	,	kIx,	,
tvar	tvar	k1gInSc1	tvar
a	a	k8xC	a
umístění	umístění	k1gNnSc1	umístění
má	mít	k5eAaImIp3nS	mít
důležitý	důležitý	k2eAgInSc4d1	důležitý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
zvuk	zvuk	k1gInSc4	zvuk
nástroje	nástroj	k1gInSc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Zevnitř	zevnitř	k6eAd1	zevnitř
desky	deska	k1gFnPc1	deska
jsou	být	k5eAaImIp3nP	být
přiklížena	přiklížen	k2eAgNnPc4d1	přiklížen
ozvučná	ozvučný	k2eAgNnPc4d1	ozvučné
žebra	žebro	k1gNnPc4	žebro
ze	z	k7c2	z
smrkového	smrkový	k2eAgNnSc2d1	smrkové
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podporují	podporovat	k5eAaImIp3nP	podporovat
rezonanční	rezonanční	k2eAgFnPc1d1	rezonanční
vlastnosti	vlastnost	k1gFnPc1	vlastnost
a	a	k8xC	a
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
deska	deska	k1gFnSc1	deska
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
palisandru	palisandr	k1gInSc2	palisandr
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
z	z	k7c2	z
mahagonu	mahagon	k1gInSc2	mahagon
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
z	z	k7c2	z
javoru	javor	k1gInSc2	javor
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
čtyři	čtyři	k4xCgNnPc1	čtyři
příčná	příčný	k2eAgNnPc1d1	příčné
žebra	žebro	k1gNnPc1	žebro
<g/>
.	.	kIx.	.
</s>
<s>
Luby	lub	k1gInPc4	lub
spojují	spojovat	k5eAaImIp3nP	spojovat
vrchní	vrchní	k2eAgMnPc1d1	vrchní
a	a	k8xC	a
spodní	spodní	k2eAgFnSc4d1	spodní
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
materiálu	materiál	k1gInSc2	materiál
jako	jako	k8xS	jako
spodní	spodní	k2eAgFnSc1d1	spodní
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Krk	krk	k1gInSc4	krk
Krk	krk	k1gInSc1	krk
kytary	kytara	k1gFnSc2	kytara
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
základna	základna	k1gFnSc1	základna
pro	pro	k7c4	pro
hmatník	hmatník	k1gInSc4	hmatník
<g/>
.	.	kIx.	.
</s>
<s>
Krk	krk	k1gInSc1	krk
je	být	k5eAaImIp3nS	být
vystaven	vystaven	k2eAgInSc1d1	vystaven
velkému	velký	k2eAgInSc3d1	velký
tahu	tah	k1gInSc3	tah
strun	struna	k1gFnPc2	struna
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
proto	proto	k8xC	proto
zhotoven	zhotovit	k5eAaPmNgInS	zhotovit
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
kusu	kus	k1gInSc2	kus
tvrdého	tvrdé	k1gNnSc2	tvrdé
rovnoletého	rovnoletý	k2eAgNnSc2d1	rovnoletý
dřeva	dřevo	k1gNnSc2	dřevo
–	–	k?	–
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
mahagonu	mahagon	k1gInSc2	mahagon
<g/>
.	.	kIx.	.
</s>
<s>
Příčný	příčný	k2eAgInSc1d1	příčný
profil	profil	k1gInSc1	profil
krku	krk	k1gInSc2	krk
má	mít	k5eAaImIp3nS	mít
podstatný	podstatný	k2eAgInSc4d1	podstatný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
pohodlnost	pohodlnost	k1gFnSc4	pohodlnost
a	a	k8xC	a
techniku	technika	k1gFnSc4	technika
hry	hra	k1gFnSc2	hra
levé	levý	k2eAgFnSc2d1	levá
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
krk	krk	k1gInSc4	krk
kytary	kytara	k1gFnSc2	kytara
je	být	k5eAaImIp3nS	být
přiklížen	přiklížen	k2eAgInSc1d1	přiklížen
hmatník	hmatník	k1gInSc1	hmatník
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgNnSc2	který
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
zapuštěny	zapuštěn	k2eAgInPc4d1	zapuštěn
pražce	pražec	k1gInPc4	pražec
zhotovené	zhotovený	k2eAgInPc4d1	zhotovený
z	z	k7c2	z
drátů	drát	k1gInPc2	drát
kovových	kovový	k2eAgFnPc2d1	kovová
slitin	slitina	k1gFnPc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Hmatník	hmatník	k1gInSc1	hmatník
se	se	k3xPyFc4	se
zhotovuje	zhotovovat	k5eAaImIp3nS	zhotovovat
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
kusu	kus	k1gInSc2	kus
velmi	velmi	k6eAd1	velmi
tvrdého	tvrdý	k2eAgNnSc2d1	tvrdé
dřeva	dřevo	k1gNnSc2	dřevo
–	–	k?	–
nejčastěji	často	k6eAd3	často
to	ten	k3xDgNnSc1	ten
bývá	bývat	k5eAaImIp3nS	bývat
eben	eben	k1gInSc1	eben
popř.	popř.	kA	popř.
palisandr	palisandr	k1gInSc1	palisandr
<g/>
.	.	kIx.	.
</s>
<s>
Nultý	nultý	k4xOgInSc1	nultý
pražec	pražec	k1gInSc1	pražec
–	–	k?	–
ořech	ořech	k1gInSc1	ořech
–	–	k?	–
je	být	k5eAaImIp3nS	být
nejblíže	blízce	k6eAd3	blízce
hlavici	hlavice	k1gFnSc4	hlavice
nástroje	nástroj	k1gInSc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
tvrdého	tvrdý	k2eAgInSc2d1	tvrdý
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
slonovina	slonovina	k1gFnSc1	slonovina
<g/>
)	)	kIx)	)
a	a	k8xC	a
určuje	určovat	k5eAaImIp3nS	určovat
–	–	k?	–
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kobylkou	kobylka	k1gFnSc7	kobylka
–	–	k?	–
výšku	výška	k1gFnSc4	výška
strun	struna	k1gFnPc2	struna
nad	nad	k7c7	nad
hmatníkem	hmatník	k1gInSc7	hmatník
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
rozmístění	rozmístění	k1gNnSc1	rozmístění
po	po	k7c6	po
šířce	šířka	k1gFnSc6	šířka
hmatníku	hmatník	k1gInSc2	hmatník
<g/>
.	.	kIx.	.
</s>
<s>
Kytara	kytara	k1gFnSc1	kytara
je	být	k5eAaImIp3nS	být
laděna	ladit	k5eAaImNgFnS	ladit
v	v	k7c6	v
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
temperovaném	temperovaný	k2eAgNnSc6d1	temperované
ladění	ladění	k1gNnSc6	ladění
<g/>
.	.	kIx.	.
</s>
<s>
Pražce	pražec	k1gInPc1	pražec
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
rozmístěné	rozmístěný	k2eAgFnPc1d1	rozmístěná
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
proti	proti	k7c3	proti
předcházejícímu	předcházející	k2eAgInSc3d1	předcházející
délku	délka	k1gFnSc4	délka
struny	struna	k1gFnSc2	struna
ke	k	k7c3	k
kobylce	kobylka	k1gFnSc3	kobylka
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
12	[number]	k4	12
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Krk	krk	k1gInSc1	krk
kytary	kytara	k1gFnSc2	kytara
je	být	k5eAaImIp3nS	být
zakončen	zakončit	k5eAaPmNgInS	zakončit
hlavicí	hlavice	k1gFnSc7	hlavice
vsazenou	vsazený	k2eAgFnSc4d1	vsazená
pod	pod	k7c7	pod
mírným	mírný	k2eAgInSc7d1	mírný
úhlem	úhel	k1gInSc7	úhel
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Hlavice	hlavice	k1gFnSc1	hlavice
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
umístění	umístění	k1gNnSc3	umístění
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
mechaniky	mechanika	k1gFnSc2	mechanika
se	se	k3xPyFc4	se
napínají	napínat	k5eAaImIp3nP	napínat
struny	struna	k1gFnPc1	struna
<g/>
.	.	kIx.	.
</s>
<s>
Struna	struna	k1gFnSc1	struna
se	se	k3xPyFc4	se
uváže	uvázat	k5eAaPmIp3nS	uvázat
přes	přes	k7c4	přes
dírku	dírka	k1gFnSc4	dírka
na	na	k7c4	na
navíjecí	navíjecí	k2eAgInSc4d1	navíjecí
váleček	váleček	k1gInSc4	váleček
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yRgInSc4	který
je	být	k5eAaImIp3nS	být
navíjena	navíjen	k2eAgFnSc1d1	navíjena
přes	přes	k7c4	přes
šroubový	šroubový	k2eAgInSc4d1	šroubový
převod	převod	k1gInSc4	převod
ladicím	ladicí	k2eAgInSc7d1	ladicí
kolíčkem	kolíček	k1gInSc7	kolíček
<g/>
.	.	kIx.	.
</s>
<s>
Kobylka	kobylka	k1gFnSc1	kobylka
Velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
kytary	kytara	k1gFnSc2	kytara
je	být	k5eAaImIp3nS	být
kobylka	kobylka	k1gFnSc1	kobylka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
uvázání	uvázání	k1gNnSc3	uvázání
strun	struna	k1gFnPc2	struna
na	na	k7c4	na
korpus	korpus	k1gInSc4	korpus
nástroje	nástroj	k1gInSc2	nástroj
(	(	kIx(	(
<g/>
struník	struník	k1gInSc1	struník
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
přenášení	přenášení	k1gNnSc3	přenášení
chvění	chvění	k1gNnSc2	chvění
strun	struna	k1gFnPc2	struna
na	na	k7c4	na
vrchní	vrchní	k2eAgFnSc4d1	vrchní
desku	deska	k1gFnSc4	deska
(	(	kIx(	(
<g/>
vložka	vložka	k1gFnSc1	vložka
kobylky	kobylka	k1gFnSc2	kobylka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kobylka	kobylka	k1gFnSc1	kobylka
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
pevně	pevně	k6eAd1	pevně
přiklížena	přiklížit	k5eAaPmNgFnS	přiklížit
na	na	k7c4	na
vrchní	vrchní	k2eAgFnSc4d1	vrchní
desku	deska	k1gFnSc4	deska
kytary	kytara	k1gFnSc2	kytara
a	a	k8xC	a
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
kusu	kus	k1gInSc2	kus
tvrdého	tvrdý	k2eAgNnSc2d1	tvrdé
dřeva	dřevo	k1gNnSc2	dřevo
–	–	k?	–
eben	eben	k1gInSc1	eben
<g/>
,	,	kIx,	,
palisandr	palisandr	k1gInSc1	palisandr
nebo	nebo	k8xC	nebo
mahagon	mahagon	k1gInSc1	mahagon
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
starších	starý	k2eAgFnPc2d2	starší
kytar	kytara	k1gFnPc2	kytara
je	být	k5eAaImIp3nS	být
kobylka	kobylka	k1gFnSc1	kobylka
položena	položen	k2eAgFnSc1d1	položena
na	na	k7c6	na
vrchní	vrchní	k2eAgFnSc6d1	vrchní
desce	deska	k1gFnSc6	deska
a	a	k8xC	a
drží	držet	k5eAaImIp3nS	držet
jen	jen	k9	jen
tlakem	tlak	k1gInSc7	tlak
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Vyměnitelná	vyměnitelný	k2eAgFnSc1d1	vyměnitelná
vložka	vložka	k1gFnSc1	vložka
kobylky	kobylka	k1gFnSc2	kobylka
se	se	k3xPyFc4	se
zhotovuje	zhotovovat	k5eAaImIp3nS	zhotovovat
ze	z	k7c2	z
slonoviny	slonovina	k1gFnSc2	slonovina
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
drážce	drážka	k1gFnSc6	drážka
(	(	kIx(	(
<g/>
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
nultým	nultý	k4xOgInSc7	nultý
pražcem	pražec	k1gInSc7	pražec
a	a	k8xC	a
vložkou	vložka	k1gFnSc7	vložka
kobylky	kobylka	k1gFnSc2	kobylka
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
menzura	menzura	k1gFnSc1	menzura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
vložkou	vložka	k1gFnSc7	vložka
kobylky	kobylka	k1gFnSc2	kobylka
je	být	k5eAaImIp3nS	být
struník	struník	k1gInSc1	struník
s	s	k7c7	s
šesti	šest	k4xCc7	šest
otvory	otvor	k1gInPc7	otvor
pro	pro	k7c4	pro
uvázání	uvázání	k1gNnSc4	uvázání
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Struny	struna	k1gFnPc4	struna
Konečný	konečný	k2eAgInSc1d1	konečný
zvuk	zvuk	k1gInSc1	zvuk
nástroje	nástroj	k1gInSc2	nástroj
je	být	k5eAaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
strunách	struna	k1gFnPc6	struna
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
klasické	klasický	k2eAgFnSc2d1	klasická
koncertní	koncertní	k2eAgFnSc2d1	koncertní
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
nylonové	nylonový	k2eAgFnPc1d1	nylonová
struny	struna	k1gFnPc1	struna
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
,	,	kIx,	,
h	h	k?	h
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
1	[number]	k4	1
–	–	k?	–
celistvý	celistvý	k2eAgInSc1d1	celistvý
tažený	tažený	k2eAgInSc1d1	tažený
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
E	E	kA	E
A	a	k8xC	a
d	d	k?	d
–	–	k?	–
nylonová	nylonový	k2eAgFnSc1d1	nylonová
vlákna	vlákna	k1gFnSc1	vlákna
opředená	opředený	k2eAgFnSc1d1	opředená
drátkem	drátek	k1gInSc7	drátek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
z	z	k7c2	z
postříbřené	postříbřený	k2eAgFnSc2d1	postříbřená
nebo	nebo	k8xC	nebo
pozlacené	pozlacený	k2eAgFnSc2d1	pozlacená
mědi	měď	k1gFnSc2	měď
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
i	i	k9	i
ty	ten	k3xDgInPc1	ten
nejjemnější	jemný	k2eAgInPc1d3	nejjemnější
odstíny	odstín	k1gInPc1	odstín
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zvukově	zvukově	k6eAd1	zvukově
vyrovnané	vyrovnaný	k2eAgFnPc1d1	vyrovnaná
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
běžných	běžný	k2eAgFnPc2d1	běžná
kytar	kytara	k1gFnPc2	kytara
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
struny	struna	k1gFnPc1	struna
kovové	kovový	k2eAgFnPc1d1	kovová
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
také	také	k9	také
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
drnká	drnkat	k5eAaImIp3nS	drnkat
na	na	k7c4	na
struny	struna	k1gFnPc4	struna
(	(	kIx(	(
<g/>
kytarové	kytarový	k2eAgInPc4d1	kytarový
rejstříky	rejstřík	k1gInPc4	rejstřík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Normální	normální	k2eAgInSc1d1	normální
kytarový	kytarový	k2eAgInSc1d1	kytarový
rejstřík	rejstřík	k1gInSc1	rejstřík
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
rezonančního	rezonanční	k2eAgInSc2d1	rezonanční
otvoru	otvor	k1gInSc2	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Blíže	blízce	k6eAd2	blízce
ke	k	k7c3	k
kobylce	kobylka	k1gFnSc3	kobylka
kytara	kytara	k1gFnSc1	kytara
vydává	vydávat	k5eAaPmIp3nS	vydávat
plechový	plechový	k2eAgInSc4d1	plechový
<g/>
,	,	kIx,	,
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
zvuk	zvuk	k1gInSc4	zvuk
(	(	kIx(	(
<g/>
sul	sout	k5eAaImAgMnS	sout
ponticello	ponticello	k1gNnSc4	ponticello
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
hmatníkem	hmatník	k1gInSc7	hmatník
pak	pak	k6eAd1	pak
měkký	měkký	k2eAgInSc1d1	měkký
zvuk	zvuk	k1gInSc1	zvuk
podobný	podobný	k2eAgInSc1d1	podobný
harfě	harfa	k1gFnSc3	harfa
(	(	kIx(	(
<g/>
sul	sout	k5eAaImAgMnS	sout
tasto	tast	k2eAgNnSc4d1	tast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sul	sout	k5eAaImAgMnS	sout
tasto	tast	k2eAgNnSc4d1	tast
je	on	k3xPp3gNnSc4	on
nejvíce	nejvíce	k6eAd1	nejvíce
výrazné	výrazný	k2eAgNnSc1d1	výrazné
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
délky	délka	k1gFnSc2	délka
struny	struna	k1gFnSc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Kytara	kytara	k1gFnSc1	kytara
bývá	bývat	k5eAaImIp3nS	bývat
zdobena	zdobit	k5eAaImNgFnS	zdobit
dřevěným	dřevěný	k2eAgNnSc7d1	dřevěné
vykládáním	vykládání	k1gNnSc7	vykládání
(	(	kIx(	(
<g/>
intarzie	intarzie	k1gFnSc2	intarzie
<g/>
)	)	kIx)	)
okolo	okolo	k7c2	okolo
zvukového	zvukový	k2eAgInSc2d1	zvukový
otvoru	otvor	k1gInSc2	otvor
a	a	k8xC	a
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
obvodu	obvod	k1gInSc6	obvod
vrchní	vrchní	k2eAgMnSc1d1	vrchní
a	a	k8xC	a
spodní	spodní	k2eAgFnPc1d1	spodní
desky	deska	k1gFnPc1	deska
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
spojů	spoj	k1gInPc2	spoj
s	s	k7c7	s
luby	lub	k1gInPc7	lub
<g/>
.	.	kIx.	.
</s>
<s>
Intarzie	intarzie	k1gFnSc1	intarzie
slouží	sloužit	k5eAaImIp3nS	sloužit
nejen	nejen	k6eAd1	nejen
jako	jako	k9	jako
ozdoba	ozdoba	k1gFnSc1	ozdoba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jako	jako	k9	jako
výztuha	výztuha	k1gFnSc1	výztuha
<g/>
.	.	kIx.	.
</s>
<s>
Zdobení	zdobení	k1gNnSc1	zdobení
bývá	bývat	k5eAaImIp3nS	bývat
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
znakem	znak	k1gInSc7	znak
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
nástrojařů	nástrojař	k1gMnPc2	nástrojař
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
vyřezávaná	vyřezávaný	k2eAgFnSc1d1	vyřezávaná
hlavice	hlavice	k1gFnSc1	hlavice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
dřeva	dřevo	k1gNnSc2	dřevo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
někdy	někdy	k6eAd1	někdy
i	i	k9	i
perleť	perleť	k1gFnSc1	perleť
<g/>
.	.	kIx.	.
</s>
<s>
Kytara	kytara	k1gFnSc1	kytara
je	být	k5eAaImIp3nS	být
povrchově	povrchově	k6eAd1	povrchově
upravena	upraven	k2eAgFnSc1d1	upravena
(	(	kIx(	(
<g/>
broušení	broušení	k1gNnPc1	broušení
<g/>
,	,	kIx,	,
tmelení	tmelení	k1gNnSc1	tmelení
<g/>
,	,	kIx,	,
moření	moření	k1gNnSc1	moření
<g/>
)	)	kIx)	)
a	a	k8xC	a
nalakována	nalakován	k2eAgFnSc1d1	nalakována
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
akustického	akustický	k2eAgNnSc2d1	akustické
hlediska	hledisko	k1gNnSc2	hledisko
kvalita	kvalita	k1gFnSc1	kvalita
laku	lak	k1gInSc2	lak
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
tlumení	tlumení	k1gNnSc6	tlumení
chvění	chvění	k1gNnSc2	chvění
nástroje	nástroj	k1gInSc2	nástroj
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
výsledný	výsledný	k2eAgInSc4d1	výsledný
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
mistrovské	mistrovský	k2eAgInPc4d1	mistrovský
nástroje	nástroj	k1gInPc4	nástroj
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
lihové	lihový	k2eAgFnPc1d1	lihová
laky	laka	k1gFnPc1	laka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
základem	základ	k1gInSc7	základ
jsou	být	k5eAaImIp3nP	být
přírodní	přírodní	k2eAgFnPc4d1	přírodní
pryskyřice	pryskyřice	k1gFnPc4	pryskyřice
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
všech	všecek	k3xTgInPc2	všecek
strunných	strunný	k2eAgInPc2d1	strunný
nástrojů	nástroj	k1gInPc2	nástroj
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
složitý	složitý	k2eAgInSc1d1	složitý
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
druhem	druh	k1gInSc7	druh
strunného	strunný	k2eAgInSc2d1	strunný
nástroje	nástroj	k1gInSc2	nástroj
byl	být	k5eAaImAgMnS	být
kinnór	kinnór	k1gMnSc1	kinnór
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
starověký	starověký	k2eAgInSc1d1	starověký
strunný	strunný	k2eAgInSc1d1	strunný
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
je	být	k5eAaImIp3nS	být
opakovaně	opakovaně	k6eAd1	opakovaně
zmiňován	zmiňován	k2eAgInSc1d1	zmiňován
už	už	k9	už
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
<g/>
,	,	kIx,	,
vzhledu	vzhled	k1gInSc6	vzhled
i	i	k8xC	i
formě	forma	k1gFnSc6	forma
hraní	hraň	k1gFnPc2	hraň
se	se	k3xPyFc4	se
nástroje	nástroj	k1gInPc1	nástroj
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dějinných	dějinný	k2eAgFnPc2d1	dějinná
epoch	epocha	k1gFnPc2	epocha
navzájem	navzájem	k6eAd1	navzájem
ovlivňovaly	ovlivňovat	k5eAaImAgInP	ovlivňovat
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
nástrojových	nástrojový	k2eAgFnPc2d1	nástrojová
odrůd	odrůda	k1gFnPc2	odrůda
a	a	k8xC	a
variant	varianta	k1gFnPc2	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
doložitelné	doložitelný	k2eAgNnSc1d1	doložitelné
<g/>
,	,	kIx,	,
že	že	k8xS	že
drnkací	drnkací	k2eAgInPc1d1	drnkací
nástroje	nástroj	k1gInPc1	nástroj
jsou	být	k5eAaImIp3nP	být
staršího	starší	k1gMnSc4	starší
původu	původ	k1gInSc2	původ
než	než	k8xS	než
nástroje	nástroj	k1gInPc1	nástroj
smyčcové	smyčcový	k2eAgInPc1d1	smyčcový
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
také	také	k9	také
častěji	často	k6eAd2	často
používány	používat	k5eAaImNgFnP	používat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vývoje	vývoj	k1gInSc2	vývoj
docházelo	docházet	k5eAaImAgNnS	docházet
často	často	k6eAd1	často
k	k	k7c3	k
přeměnám	přeměna	k1gFnPc3	přeměna
drnkacích	drnkací	k2eAgMnPc2d1	drnkací
nástrojů	nástroj	k1gInPc2	nástroj
na	na	k7c4	na
smyčcové	smyčcový	k2eAgMnPc4d1	smyčcový
(	(	kIx(	(
<g/>
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nástroje	nástroj	k1gInPc1	nástroj
procházely	procházet	k5eAaImAgFnP	procházet
různými	různý	k2eAgFnPc7d1	různá
konstrukčními	konstrukční	k2eAgFnPc7d1	konstrukční
změnami	změna	k1gFnPc7	změna
–	–	k?	–
měnily	měnit	k5eAaImAgFnP	měnit
se	s	k7c7	s
tvary	tvar	k1gInPc7	tvar
korpusů	korpus	k1gInPc2	korpus
<g/>
,	,	kIx,	,
počty	počet	k1gInPc1	počet
strun	struna	k1gFnPc2	struna
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
ladění	ladění	k1gNnSc2	ladění
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Pro	pro	k7c4	pro
zesílení	zesílení	k1gNnSc4	zesílení
zvuku	zvuk	k1gInSc2	zvuk
nástrojů	nástroj	k1gInPc2	nástroj
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
nejvhodnější	vhodný	k2eAgFnSc1d3	nejvhodnější
začala	začít	k5eAaPmAgFnS	začít
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
tenkostěnná	tenkostěnný	k2eAgFnSc1d1	tenkostěnná
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
rezonanční	rezonanční	k2eAgFnSc1d1	rezonanční
skříň	skříň	k1gFnSc1	skříň
(	(	kIx(	(
<g/>
korpus	korpus	k1gInSc1	korpus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
strunných	strunný	k2eAgInPc6d1	strunný
nástrojích	nástroj	k1gInPc6	nástroj
krk	krk	k1gInSc1	krk
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
upevňování	upevňování	k1gNnSc3	upevňování
strun	struna	k1gFnPc2	struna
ve	v	k7c6	v
struníkovém	struníkový	k2eAgNnSc6d1	struníkový
zařízení	zařízení	k1gNnSc6	zařízení
(	(	kIx(	(
<g/>
vznik	vznik	k1gInSc1	vznik
kobylky	kobylka	k1gFnSc2	kobylka
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
ladění	ladění	k1gNnSc3	ladění
v	v	k7c6	v
ladicím	ladicí	k2eAgNnSc6d1	ladicí
zařízení	zařízení	k1gNnSc6	zařízení
(	(	kIx(	(
<g/>
hlavice	hlavice	k1gFnSc1	hlavice
s	s	k7c7	s
kolíčky	kolíček	k1gInPc7	kolíček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nástrojů	nástroj	k1gInPc2	nástroj
kytarového	kytarový	k2eAgInSc2d1	kytarový
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
korpus	korpus	k1gInSc1	korpus
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
široké	široký	k2eAgFnSc2d1	široká
arabské	arabský	k2eAgFnSc2d1	arabská
osmičky	osmička	k1gFnSc2	osmička
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc4	dva
ploché	plochý	k2eAgFnPc4d1	plochá
desky	deska	k1gFnPc4	deska
(	(	kIx(	(
<g/>
přední	přední	k2eAgFnPc4d1	přední
a	a	k8xC	a
zadní	zadní	k2eAgFnPc4d1	zadní
<g/>
)	)	kIx)	)
korpusu	korpus	k1gInSc2	korpus
byly	být	k5eAaImAgInP	být
spojeny	spojit	k5eAaPmNgInP	spojit
bočními	boční	k2eAgFnPc7d1	boční
deskami	deska	k1gFnPc7	deska
(	(	kIx(	(
<g/>
luby	lub	k1gInPc7	lub
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krk	krk	k1gInSc4	krk
i	i	k8xC	i
korpus	korpus	k1gInSc4	korpus
byly	být	k5eAaImAgInP	být
asi	asi	k9	asi
stejně	stejně	k6eAd1	stejně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
a	a	k8xC	a
hlavice	hlavice	k1gFnPc1	hlavice
se	se	k3xPyFc4	se
mírně	mírně	k6eAd1	mírně
zalamovala	zalamovat	k5eAaImAgFnS	zalamovat
nazad	nazad	k6eAd1	nazad
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
kytary	kytara	k1gFnSc2	kytara
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
arabského	arabský	k2eAgMnSc2d1	arabský
kitára	kitár	k1gMnSc2	kitár
a	a	k8xC	a
řeckého	řecký	k2eAgNnSc2d1	řecké
kithara	kithara	k1gFnSc1	kithara
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
kytara	kytara	k1gFnSc1	kytara
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
vývoji	vývoj	k1gInSc6	vývoj
ustálila	ustálit	k5eAaPmAgFnS	ustálit
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
svojí	svůj	k3xOyFgFnSc2	svůj
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Korpus	korpus	k1gInSc1	korpus
získal	získat	k5eAaPmAgInS	získat
kruhový	kruhový	k2eAgInSc4d1	kruhový
rezonanční	rezonanční	k2eAgInSc4d1	rezonanční
otvor	otvor	k1gInSc4	otvor
a	a	k8xC	a
na	na	k7c6	na
krku	krk	k1gInSc6	krk
byl	být	k5eAaImAgInS	být
hmatník	hmatník	k1gInSc1	hmatník
s	s	k7c7	s
pražci	pražec	k1gInPc7	pražec
<g/>
.	.	kIx.	.
</s>
<s>
Kytara	kytara	k1gFnSc1	kytara
už	už	k6eAd1	už
měla	mít	k5eAaImAgFnS	mít
dnešní	dnešní	k2eAgNnSc4d1	dnešní
ladění	ladění	k1gNnSc4	ladění
a	a	k8xC	a
počet	počet	k1gInSc1	počet
strun	struna	k1gFnPc2	struna
(	(	kIx(	(
<g/>
šest	šest	k4xCc1	šest
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
pro	pro	k7c4	pro
ladění	ladění	k1gNnSc4	ladění
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
šroubového	šroubový	k2eAgInSc2d1	šroubový
mechanismu	mechanismus	k1gInSc2	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
kytaru	kytara	k1gFnSc4	kytara
velmi	velmi	k6eAd1	velmi
vzrostl	vzrůst	k5eAaPmAgMnS	vzrůst
a	a	k8xC	a
kytara	kytara	k1gFnSc1	kytara
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
domácím	domácí	k2eAgInSc7d1	domácí
nástrojem	nástroj	k1gInSc7	nástroj
–	–	k?	–
používala	používat	k5eAaImAgFnS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
nástroj	nástroj	k1gInSc1	nástroj
ke	k	k7c3	k
zpěvu	zpěv	k1gInSc3	zpěv
<g/>
,	,	kIx,	,
uplatňovala	uplatňovat	k5eAaImAgFnS	uplatňovat
se	se	k3xPyFc4	se
v	v	k7c6	v
komorní	komorní	k2eAgFnSc6d1	komorní
hře	hra	k1gFnSc6	hra
i	i	k9	i
sólově	sólově	k6eAd1	sólově
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
kytara	kytara	k1gFnSc1	kytara
částečně	částečně	k6eAd1	částečně
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
klavírem	klavír	k1gInSc7	klavír
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začíná	začínat	k5eAaImIp3nS	začínat
nový	nový	k2eAgInSc4d1	nový
vzestup	vzestup	k1gInSc4	vzestup
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Akustická	akustický	k2eAgFnSc1d1	akustická
kytara	kytara	k1gFnSc1	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
také	také	k9	také
jen	jen	k9	jen
španělka	španělka	k1gFnSc1	španělka
<g/>
)	)	kIx)	)
–	–	k?	–
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgNnPc4d1	charakteristické
kruhovým	kruhový	k2eAgInSc7d1	kruhový
ozvučným	ozvučný	k2eAgInSc7d1	ozvučný
otvorem	otvor	k1gInSc7	otvor
a	a	k8xC	a
nylonovými	nylonový	k2eAgFnPc7d1	nylonová
strunami	struna	k1gFnPc7	struna
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
a	a	k8xC	a
španělské	španělský	k2eAgFnSc6d1	španělská
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
kovové	kovový	k2eAgFnPc4d1	kovová
struny	struna	k1gFnPc4	struna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
není	být	k5eNaImIp3nS	být
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
zátěž	zátěž	k1gFnSc4	zátěž
stavěna	stavěn	k2eAgFnSc1d1	stavěna
a	a	k8xC	a
tah	tah	k1gInSc1	tah
strun	struna	k1gFnPc2	struna
nemusí	muset	k5eNaImIp3nS	muset
vydržet	vydržet	k5eAaPmF	vydržet
<g/>
.	.	kIx.	.
</s>
<s>
Westernová	westernový	k2eAgFnSc1d1	westernová
kytara	kytara	k1gFnSc1	kytara
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgNnSc4d2	veliký
tělo	tělo	k1gNnSc4	tělo
než	než	k8xS	než
španělka	španělka	k1gFnSc1	španělka
<g/>
,	,	kIx,	,
kovové	kovový	k2eAgFnPc4d1	kovová
struny	struna	k1gFnPc4	struna
a	a	k8xC	a
užší	úzký	k2eAgInSc4d2	užší
hmatník	hmatník	k1gInSc4	hmatník
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
oproti	oproti	k7c3	oproti
španělkám	španělka	k1gFnPc3	španělka
orientační	orientační	k2eAgFnSc2d1	orientační
značky	značka	k1gFnSc2	značka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
usnadňují	usnadňovat	k5eAaImIp3nP	usnadňovat
orientaci	orientace	k1gFnSc4	orientace
na	na	k7c6	na
hmatníku	hmatník	k1gInSc6	hmatník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
česku	česko	k1gNnSc6	česko
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
typ	typ	k1gInSc4	typ
kytar	kytara	k1gFnPc2	kytara
zažil	zažít	k5eAaPmAgMnS	zažít
název	název	k1gInSc4	název
Jumbo	Jumba	k1gFnSc5	Jumba
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nicméně	nicméně	k8xC	nicméně
není	být	k5eNaImIp3nS	být
správný	správný	k2eAgMnSc1d1	správný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
kytary	kytara	k1gFnPc1	kytara
s	s	k7c7	s
ještě	ještě	k6eAd1	ještě
jiným	jiný	k2eAgInSc7d1	jiný
tvarem	tvar	k1gInSc7	tvar
korpusu	korpus	k1gInSc2	korpus
<g/>
.	.	kIx.	.
</s>
<s>
Správné	správný	k2eAgNnSc1d1	správné
označení	označení	k1gNnSc1	označení
typického	typický	k2eAgInSc2d1	typický
tvaru	tvar	k1gInSc2	tvar
westernových	westernový	k2eAgFnPc2d1	westernová
kytar	kytara	k1gFnPc2	kytara
je	být	k5eAaImIp3nS	být
Dreadnought	dreadnought	k1gInSc1	dreadnought
(	(	kIx(	(
<g/>
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
se	se	k3xPyFc4	se
[	[	kIx(	[
<g/>
drednat	drednat	k5eAaImF	drednat
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jumbo	Jumba	k1gMnSc5	Jumba
je	být	k5eAaImIp3nS	být
kytara	kytara	k1gFnSc1	kytara
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
kovové	kovový	k2eAgFnPc4d1	kovová
struny	struna	k1gFnPc4	struna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
zvětšenou	zvětšený	k2eAgFnSc4d1	zvětšená
spodní	spodní	k2eAgFnSc4d1	spodní
část	část	k1gFnSc4	část
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Krk	krk	k1gInSc1	krk
je	být	k5eAaImIp3nS	být
přizpůsoben	přizpůsoben	k2eAgInSc1d1	přizpůsoben
většímu	veliký	k2eAgInSc3d2	veliký
tahu	tah	k1gInSc3	tah
kovových	kovový	k2eAgFnPc2d1	kovová
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
rockové	rockový	k2eAgFnSc6d1	rocková
a	a	k8xC	a
country	country	k2eAgFnSc6d1	country
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ve	v	k7c6	v
folkové	folkový	k2eAgFnSc6d1	folková
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Korpus	korpus	k1gInSc1	korpus
kytary	kytara	k1gFnSc2	kytara
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
více	hodně	k6eAd2	hodně
klenutý	klenutý	k2eAgMnSc1d1	klenutý
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
i	i	k9	i
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
zvukové	zvukový	k2eAgFnPc4d1	zvuková
vlastnosti	vlastnost	k1gFnPc4	vlastnost
kytary	kytara	k1gFnPc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
westernové	westernový	k2eAgFnSc3d1	westernová
kytaře	kytara	k1gFnSc3	kytara
je	být	k5eAaImIp3nS	být
zvuk	zvuk	k1gInSc1	zvuk
méně	málo	k6eAd2	málo
basový	basový	k2eAgInSc1d1	basový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lépe	dobře	k6eAd2	dobře
vyniknou	vyniknout	k5eAaPmIp3nP	vyniknout
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
tóny	tón	k1gInPc4	tón
všech	všecek	k3xTgFnPc2	všecek
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Jazzová	jazzový	k2eAgFnSc1d1	jazzová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
také	také	k9	také
Gibsonka	gibsonka	k1gFnSc1	gibsonka
(	(	kIx(	(
<g/>
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
se	se	k3xPyFc4	se
[	[	kIx(	[
<g/>
gibsnka	gibsnka	k6eAd1	gibsnka
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
[	[	kIx(	[
<g/>
džibsonka	džibsonka	k1gFnSc1	džibsonka
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
původního	původní	k2eAgMnSc2d1	původní
výrobce	výrobce	k1gMnSc2	výrobce
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
klenutou	klenutý	k2eAgFnSc7d1	klenutá
horní	horní	k2eAgFnSc7d1	horní
deskou	deska	k1gFnSc7	deska
a	a	k8xC	a
tvarem	tvar	k1gInSc7	tvar
ozvučných	ozvučný	k2eAgInPc2d1	ozvučný
otvorů	otvor	k1gInPc2	otvor
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
<g/>
,	,	kIx,	,
podobného	podobný	k2eAgInSc2d1	podobný
tvaru	tvar	k1gInSc2	tvar
jako	jako	k8xC	jako
u	u	k7c2	u
houslí	housle	k1gFnPc2	housle
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
prvky	prvek	k1gInPc7	prvek
cutaway	cutawaa	k1gFnSc2	cutawaa
a	a	k8xC	a
pickguard	pickguarda	k1gFnPc2	pickguarda
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
silnější	silný	k2eAgMnSc1d2	silnější
a	a	k8xC	a
ostřejší	ostrý	k2eAgInSc1d2	ostřejší
zvuk	zvuk	k1gInSc1	zvuk
než	než	k8xS	než
španělka	španělka	k1gFnSc1	španělka
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
ocelovým	ocelový	k2eAgFnPc3d1	ocelová
strunám	struna	k1gFnPc3	struna
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
slabší	slabý	k2eAgMnSc1d2	slabší
a	a	k8xC	a
tlumenější	tlumený	k2eAgInSc1d2	tlumenější
zvuk	zvuk	k1gInSc1	zvuk
oproti	oproti	k7c3	oproti
Jumbo	Jumba	k1gFnSc5	Jumba
nebo	nebo	k8xC	nebo
Westernové	westernový	k2eAgFnSc6d1	westernová
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
především	především	k9	především
pro	pro	k7c4	pro
jazzovou	jazzový	k2eAgFnSc4d1	jazzová
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Havajská	havajský	k2eAgFnSc1d1	Havajská
kytara	kytara	k1gFnSc1	kytara
je	být	k5eAaImIp3nS	být
konstrukčně	konstrukčně	k6eAd1	konstrukčně
podobná	podobný	k2eAgFnSc1d1	podobná
španělce	španělka	k1gFnSc3	španělka
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
širší	široký	k2eAgInPc4d2	širší
luby	lub	k1gInPc4	lub
a	a	k8xC	a
tedy	tedy	k9	tedy
mohutnější	mohutný	k2eAgInSc4d2	mohutnější
korpus	korpus	k1gInSc4	korpus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
laděna	ladit	k5eAaImNgFnS	ladit
do	do	k7c2	do
durového	durový	k2eAgInSc2d1	durový
akordu	akord	k1gInSc2	akord
a	a	k8xC	a
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
styl	styl	k1gInSc1	styl
<g/>
.	.	kIx.	.
</s>
<s>
Struny	struna	k1gFnPc1	struna
se	se	k3xPyFc4	se
nepřitlačují	přitlačovat	k5eNaImIp3nP	přitlačovat
k	k	k7c3	k
pražcům	pražec	k1gInPc3	pražec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
délka	délka	k1gFnSc1	délka
chvění	chvění	k1gNnSc2	chvění
se	se	k3xPyFc4	se
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
kovovým	kovový	k2eAgMnSc7d1	kovový
"	"	kIx"	"
<g/>
pražcem	pražec	k1gInSc7	pražec
<g/>
"	"	kIx"	"
drženým	držený	k2eAgMnSc7d1	držený
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k9	tak
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
chvějivý	chvějivý	k2eAgInSc4d1	chvějivý
zvuk	zvuk	k1gInSc4	zvuk
s	s	k7c7	s
plynulými	plynulý	k2eAgInPc7d1	plynulý
přechody	přechod	k1gInPc7	přechod
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
tóny	tón	k1gInPc7	tón
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
etnické	etnický	k2eAgFnSc6d1	etnická
a	a	k8xC	a
country	country	k2eAgFnSc6d1	country
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
elektrofonické	elektrofonický	k2eAgFnPc1d1	elektrofonická
varianty	varianta	k1gFnPc1	varianta
havajské	havajský	k2eAgFnSc2d1	Havajská
kytary	kytara	k1gFnSc2	kytara
(	(	kIx(	(
<g/>
např.	např.	kA	např.
steel	steel	k1gInSc1	steel
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
Dobro	dobro	k1gNnSc1	dobro
je	být	k5eAaImIp3nS	být
nástroj	nástroj	k1gInSc4	nástroj
s	s	k7c7	s
podobným	podobný	k2eAgNnSc7d1	podobné
laděním	ladění	k1gNnSc7	ladění
a	a	k8xC	a
stylem	styl	k1gInSc7	styl
hry	hra	k1gFnSc2	hra
jako	jako	k8xS	jako
havajská	havajský	k2eAgFnSc1d1	Havajská
kytara	kytara	k1gFnSc1	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
se	se	k3xPyFc4	se
kovovými	kovový	k2eAgInPc7d1	kovový
rezonátory	rezonátor	k1gInPc7	rezonátor
umístěnými	umístěný	k2eAgInPc7d1	umístěný
v	v	k7c6	v
otvorech	otvor	k1gInPc6	otvor
vrchní	vrchní	k2eAgFnSc2d1	vrchní
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Nástroj	nástroj	k1gInSc1	nástroj
byl	být	k5eAaImAgInS	být
zkonstruován	zkonstruovat	k5eAaPmNgInS	zkonstruovat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
USA	USA	kA	USA
(	(	kIx(	(
<g/>
slovenským	slovenský	k2eAgMnSc7d1	slovenský
emigrantem	emigrant	k1gMnSc7	emigrant
Jánem	Ján	k1gMnSc7	Ján
Dopyerou	Dopyera	k1gMnSc7	Dopyera
–	–	k?	–
DoBro	dobro	k1gNnSc1	dobro
=	=	kIx~	=
Dopyera	Dopyera	k1gFnSc1	Dopyera
Brothers	Brothersa	k1gFnPc2	Brothersa
<g/>
)	)	kIx)	)
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
zvané	zvaný	k2eAgFnPc1d1	zvaná
"	"	kIx"	"
<g/>
bluegrass	bluegrass	k1gInSc1	bluegrass
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
součást	součást	k1gFnSc4	součást
americké	americký	k2eAgFnSc2d1	americká
country	country	k2eAgFnSc2d1	country
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
speciální	speciální	k2eAgInSc4d1	speciální
případ	případ	k1gInSc4	případ
tzv.	tzv.	kA	tzv.
rezofonické	rezofonický	k2eAgFnSc2d1	rezofonická
kytary	kytara	k1gFnSc2	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Elektroakustická	elektroakustický	k2eAgFnSc1d1	elektroakustická
kytara	kytara	k1gFnSc1	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Elektroakustické	elektroakustický	k2eAgFnPc1d1	elektroakustická
kytary	kytara	k1gFnPc1	kytara
konstrukčně	konstrukčně	k6eAd1	konstrukčně
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
akustické	akustický	k2eAgFnSc2d1	akustická
kytary	kytara	k1gFnSc2	kytara
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
opatřeny	opatřen	k2eAgInPc1d1	opatřen
snímačem	snímač	k1gInSc7	snímač
<g/>
,	,	kIx,	,
umístěným	umístěný	k2eAgInSc7d1	umístěný
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
kobylce	kobylka	k1gFnSc6	kobylka
<g/>
.	.	kIx.	.
</s>
<s>
Mechanicko	Mechanicko	k1gNnSc1	Mechanicko
<g/>
–	–	k?	–
<g/>
elektrický	elektrický	k2eAgMnSc1d1	elektrický
převodník	převodník	k1gMnSc1	převodník
snímače	snímač	k1gInSc2	snímač
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
piezoelektrický	piezoelektrický	k2eAgMnSc1d1	piezoelektrický
a	a	k8xC	a
přeměňuje	přeměňovat	k5eAaImIp3nS	přeměňovat
změny	změna	k1gFnSc2	změna
mechanického	mechanický	k2eAgInSc2d1	mechanický
tlaku	tlak	k1gInSc2	tlak
na	na	k7c4	na
elektrický	elektrický	k2eAgInSc4d1	elektrický
signál	signál	k1gInSc4	signál
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
mikrofon	mikrofon	k1gInSc4	mikrofon
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
to	ten	k3xDgNnSc1	ten
zesilovat	zesilovat	k5eAaImF	zesilovat
elektronicky	elektronicky	k6eAd1	elektronicky
zvuk	zvuk	k1gInSc4	zvuk
nástroje	nástroj	k1gInSc2	nástroj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zvuk	zvuk	k1gInSc1	zvuk
si	se	k3xPyFc3	se
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
barvu	barva	k1gFnSc4	barva
klasické	klasický	k2eAgFnSc2d1	klasická
kytary	kytara	k1gFnSc2	kytara
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
elektrofonické	elektrofonický	k2eAgFnSc2d1	elektrofonická
kytary	kytara	k1gFnSc2	kytara
s	s	k7c7	s
elektromagnetickým	elektromagnetický	k2eAgNnSc7d1	elektromagnetické
snímáním	snímání	k1gNnSc7	snímání
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dosažení	dosažení	k1gNnPc4	dosažení
ještě	ještě	k6eAd1	ještě
věrnějšího	věrný	k2eAgInSc2d2	věrnější
zvuku	zvuk	k1gInSc2	zvuk
bývá	bývat	k5eAaImIp3nS	bývat
piezoelektrický	piezoelektrický	k2eAgInSc1d1	piezoelektrický
snímač	snímač	k1gInSc1	snímač
někdy	někdy	k6eAd1	někdy
doplněn	doplnit	k5eAaPmNgInS	doplnit
malým	malý	k2eAgInSc7d1	malý
mikrofonem	mikrofon	k1gInSc7	mikrofon
<g/>
,	,	kIx,	,
umístěným	umístěný	k2eAgInSc7d1	umístěný
uvnitř	uvnitř	k7c2	uvnitř
korpusu	korpus	k1gInSc2	korpus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kytaře	kytara	k1gFnSc6	kytara
bývá	bývat	k5eAaImIp3nS	bývat
vestavěn	vestavěn	k2eAgInSc1d1	vestavěn
předzesilovač	předzesilovač	k1gInSc1	předzesilovač
s	s	k7c7	s
ekvalizérem	ekvalizér	k1gInSc7	ekvalizér
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
kytara	kytara	k1gFnSc1	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Kytara	kytara	k1gFnSc1	kytara
se	se	k3xPyFc4	se
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
i	i	k9	i
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
orchestrech	orchestr	k1gInPc6	orchestr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikal	vznikat	k5eAaImAgInS	vznikat
problém	problém	k1gInSc1	problém
se	s	k7c7	s
sílou	síla	k1gFnSc7	síla
zvuku	zvuk	k1gInSc2	zvuk
–	–	k?	–
hlas	hlas	k1gInSc1	hlas
kytary	kytara	k1gFnSc2	kytara
zanikal	zanikat	k5eAaImAgInS	zanikat
<g/>
.	.	kIx.	.
</s>
<s>
Hledala	hledat	k5eAaImAgFnS	hledat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zvuk	zvuk	k1gInSc1	zvuk
kytary	kytara	k1gFnSc2	kytara
zesílit	zesílit	k5eAaPmF	zesílit
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
zkoušeny	zkoušet	k5eAaImNgInP	zkoušet
různé	různý	k2eAgInPc1d1	různý
principy	princip	k1gInPc1	princip
a	a	k8xC	a
konstrukce	konstrukce	k1gFnPc1	konstrukce
snímačů	snímač	k1gInPc2	snímač
<g/>
.	.	kIx.	.
</s>
<s>
Nejlépe	dobře	k6eAd3	dobře
se	se	k3xPyFc4	se
osvědčil	osvědčit	k5eAaPmAgInS	osvědčit
elektromagnetický	elektromagnetický	k2eAgInSc1d1	elektromagnetický
snímač	snímač	k1gInSc1	snímač
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jej	on	k3xPp3gMnSc4	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
malé	malý	k2eAgFnPc4d1	malá
cívky	cívka	k1gFnPc4	cívka
umístěné	umístěný	k2eAgFnPc4d1	umístěná
pod	pod	k7c7	pod
strunami	struna	k1gFnPc7	struna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
kovových	kovový	k2eAgFnPc2d1	kovová
strun	struna	k1gFnPc2	struna
indukuje	indukovat	k5eAaBmIp3nS	indukovat
malé	malý	k2eAgNnSc1d1	malé
elektrické	elektrický	k2eAgNnSc1d1	elektrické
napětí	napětí	k1gNnSc1	napětí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
zesilováno	zesilovat	k5eAaImNgNnS	zesilovat
zesilovačem	zesilovač	k1gInSc7	zesilovač
<g/>
.	.	kIx.	.
</s>
<s>
Cívka	cívka	k1gFnSc1	cívka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
společná	společný	k2eAgNnPc1d1	společné
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
struny	struna	k1gFnPc4	struna
nebo	nebo	k8xC	nebo
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
cívku	cívka	k1gFnSc4	cívka
každá	každý	k3xTgFnSc1	každý
struna	struna	k1gFnSc1	struna
<g/>
.	.	kIx.	.
</s>
<s>
Elektromagnetický	elektromagnetický	k2eAgInSc1d1	elektromagnetický
snímač	snímač	k1gInSc1	snímač
nesnímá	snímat	k5eNaImIp3nS	snímat
vibrací	vibrace	k1gFnSc7	vibrace
desky	deska	k1gFnSc2	deska
ale	ale	k8xC	ale
chvění	chvění	k1gNnSc4	chvění
strun	struna	k1gFnPc2	struna
<g/>
,	,	kIx,	,
elektrofonické	elektrofonický	k2eAgFnPc1d1	elektrofonická
kytary	kytara	k1gFnPc1	kytara
tedy	tedy	k9	tedy
nepotřebují	potřebovat	k5eNaImIp3nP	potřebovat
korpus	korpus	k1gInSc4	korpus
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
na	na	k7c6	na
konstrukci	konstrukce	k1gFnSc6	konstrukce
–	–	k?	–
tělo	tělo	k1gNnSc1	tělo
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
masivní	masivní	k2eAgFnSc2d1	masivní
desky	deska	k1gFnSc2	deska
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
plastických	plastický	k2eAgFnPc2d1	plastická
hmot	hmota	k1gFnPc2	hmota
či	či	k8xC	či
jiných	jiný	k2eAgInPc2d1	jiný
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
chybět	chybět	k5eAaImF	chybět
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
další	další	k2eAgInPc1d1	další
typické	typický	k2eAgInPc1d1	typický
znaky	znak	k1gInPc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Ladicí	ladicí	k2eAgInPc1d1	ladicí
kolíky	kolík	k1gInPc1	kolík
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
vybavením	vybavení	k1gNnSc7	vybavení
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
páka	páka	k1gFnSc1	páka
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
tremolo	tremolo	k6eAd1	tremolo
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nesprávně	správně	k6eNd1	správně
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vibráto	vibrát	k2eAgNnSc1d1	vibrát
(	(	kIx(	(
<g/>
vibrapáka	vibrapák	k1gMnSc4	vibrapák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pohybovat	pohybovat	k5eAaImF	pohybovat
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
kobylkou	kobylka	k1gFnSc7	kobylka
a	a	k8xC	a
měnit	měnit	k5eAaImF	měnit
tak	tak	k8xC	tak
výšky	výška	k1gFnSc2	výška
znějících	znějící	k2eAgInPc2d1	znějící
tónů	tón	k1gInPc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
v	v	k7c6	v
kytaře	kytara	k1gFnSc6	kytara
vestavěny	vestavěn	k2eAgInPc4d1	vestavěn
elektrické	elektrický	k2eAgInPc4d1	elektrický
obvody	obvod	k1gInPc4	obvod
jako	jako	k8xC	jako
předzesilovač	předzesilovač	k1gInSc4	předzesilovač
nebo	nebo	k8xC	nebo
aktivní	aktivní	k2eAgFnSc2d1	aktivní
korekce	korekce	k1gFnSc2	korekce
<g/>
.	.	kIx.	.
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
kytara	kytara	k1gFnSc1	kytara
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
opatřena	opatřit	k5eAaPmNgFnS	opatřit
konektorem	konektor	k1gInSc7	konektor
Jack	Jacko	k1gNnPc2	Jacko
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
6,35	[number]	k4	6,35
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
připojuje	připojovat	k5eAaImIp3nS	připojovat
k	k	k7c3	k
zesilovači	zesilovač	k1gInSc3	zesilovač
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
zesilovače	zesilovač	k1gInSc2	zesilovač
dává	dávat	k5eAaImIp3nS	dávat
kytaře	kytara	k1gFnSc3	kytara
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
moderní	moderní	k2eAgFnSc2d1	moderní
hudby	hudba	k1gFnSc2	hudba
–	–	k?	–
rock	rock	k1gInSc1	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
roll	roll	k1gInSc1	roll
<g/>
,	,	kIx,	,
big-beat	bigeat	k2eAgInSc1d1	big-beat
atd.	atd.	kA	atd.
Signál	signál	k1gInSc1	signál
z	z	k7c2	z
kytary	kytara	k1gFnSc2	kytara
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
elektronicky	elektronicky	k6eAd1	elektronicky
upravuje	upravovat	k5eAaImIp3nS	upravovat
pomocí	pomocí	k7c2	pomocí
efektů	efekt	k1gInPc2	efekt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
booster	booster	k1gInSc1	booster
<g/>
,	,	kIx,	,
kvákadlo	kvákadlo	k1gNnSc1	kvákadlo
apod.	apod.	kA	apod.
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
nebo	nebo	k8xC	nebo
slangově	slangově	k6eAd1	slangově
basa	basa	k1gFnSc1	basa
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
kytaře	kytara	k1gFnSc6	kytara
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
ale	ale	k9	ale
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
úplně	úplně	k6eAd1	úplně
jiný	jiný	k2eAgInSc4d1	jiný
nástroj	nástroj	k1gInSc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Tónovým	Tónův	k2eAgInSc7d1	Tónův
rozsahem	rozsah	k1gInSc7	rozsah
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vlastně	vlastně	k9	vlastně
obdoba	obdoba	k1gFnSc1	obdoba
elektricky	elektricky	k6eAd1	elektricky
zesíleného	zesílený	k2eAgInSc2d1	zesílený
kontrabasu	kontrabas	k1gInSc2	kontrabas
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
má	mít	k5eAaImIp3nS	mít
shodné	shodný	k2eAgNnSc1d1	shodné
strunění	strunění	k1gNnSc1	strunění
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
provedení	provedení	k1gNnSc4	provedení
hmatníku	hmatník	k1gInSc2	hmatník
(	(	kIx(	(
<g/>
pražce	pražec	k1gInSc2	pražec
<g/>
)	)	kIx)	)
a	a	k8xC	a
způsob	způsob	k1gInSc1	způsob
držení	držení	k1gNnSc2	držení
a	a	k8xC	a
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
ke	k	k7c3	k
kytarám	kytara	k1gFnPc3	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Akustické	akustický	k2eAgFnPc1d1	akustická
baskytary	baskytara	k1gFnPc1	baskytara
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
málokdy	málokdy	k6eAd1	málokdy
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
pouze	pouze	k6eAd1	pouze
4	[number]	k4	4
struny	struna	k1gFnSc2	struna
naladěné	naladěný	k2eAgFnSc2d1	naladěná
do	do	k7c2	do
kvart	kvarta	k1gFnPc2	kvarta
–	–	k?	–
E	E	kA	E
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
,	,	kIx,	,
G	G	kA	G
(	(	kIx(	(
<g/>
struna	struna	k1gFnSc1	struna
E1	E1	k1gFnSc2	E1
–	–	k?	–
é	é	k0	é
kontra	kontra	k2eAgInPc3d1	kontra
–	–	k?	–
zní	znět	k5eAaImIp3nS	znět
o	o	k7c4	o
oktávu	oktáva	k1gFnSc4	oktáva
níže	nízce	k6eAd2	nízce
než	než	k8xS	než
struna	struna	k1gFnSc1	struna
E	E	kA	E
na	na	k7c6	na
kytaře	kytara	k1gFnSc6	kytara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
baskytary	baskytara	k1gFnPc1	baskytara
pěti	pět	k4xCc3	pět
a	a	k8xC	a
vícestrunné	vícestrunný	k2eAgFnSc6d1	vícestrunný
<g/>
.	.	kIx.	.
</s>
<s>
Basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
hraje	hrát	k5eAaImIp3nS	hrát
většinou	většinou	k6eAd1	většinou
jednohlasý	jednohlasý	k2eAgInSc4d1	jednohlasý
part	part	k1gInSc4	part
(	(	kIx(	(
<g/>
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
doprovodných	doprovodný	k2eAgInPc2d1	doprovodný
stylů	styl	k1gInPc2	styl
i	i	k8xC	i
akordy	akord	k1gInPc1	akord
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
basové	basový	k2eAgFnSc2d1	basová
rytmicko	rytmicko	k6eAd1	rytmicko
melodické	melodický	k2eAgFnSc2d1	melodická
linky	linka	k1gFnSc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
baskytaru	baskytara	k1gFnSc4	baskytara
obvykle	obvykle	k6eAd1	obvykle
nehraje	hrát	k5eNaImIp3nS	hrát
pravou	pravý	k2eAgFnSc7d1	pravá
rukou	ruka	k1gFnSc7	ruka
trsátkem	trsátko	k1gNnSc7	trsátko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prsty	prst	k1gInPc4	prst
a	a	k8xC	a
také	také	k9	také
prstoklad	prstoklad	k1gInSc1	prstoklad
levé	levý	k2eAgFnSc2d1	levá
ruky	ruka	k1gFnSc2	ruka
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
lišit	lišit	k5eAaImF	lišit
od	od	k7c2	od
kytarového	kytarový	k2eAgInSc2d1	kytarový
(	(	kIx(	(
<g/>
kontrabasový	kontrabasový	k2eAgMnSc1d1	kontrabasový
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
3	[number]	k4	3
půltóny	půltón	k1gInPc7	půltón
na	na	k7c4	na
polohu	poloha	k1gFnSc4	poloha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mensura	mensura	k1gFnSc1	mensura
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
a	a	k8xC	a
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
pražci	pražec	k1gInPc7	pražec
je	být	k5eAaImIp3nS	být
také	také	k9	také
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
u	u	k7c2	u
kytary	kytara	k1gFnSc2	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Technicky	technicky	k6eAd1	technicky
zdatnější	zdatný	k2eAgMnPc1d2	zdatnější
hráči	hráč	k1gMnPc1	hráč
však	však	k9	však
používají	používat	k5eAaImIp3nP	používat
klasický	klasický	k2eAgInSc4d1	klasický
kytarový	kytarový	k2eAgInSc4d1	kytarový
prstoklad	prstoklad	k1gInSc4	prstoklad
(	(	kIx(	(
<g/>
4	[number]	k4	4
půltony	půlton	k1gInPc7	půlton
na	na	k7c4	na
polohu	poloha	k1gFnSc4	poloha
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc4	každý
prst	prst	k1gInSc4	prst
obsluhuje	obsluhovat	k5eAaImIp3nS	obsluhovat
jedno	jeden	k4xCgNnSc1	jeden
políčko	políčko	k1gNnSc1	políčko
<g/>
)	)	kIx)	)
například	například	k6eAd1	například
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
vícestrunnými	vícestrunný	k2eAgFnPc7d1	vícestrunný
baskytarami	baskytara	k1gFnPc7	baskytara
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
zkušení	zkušený	k2eAgMnPc1d1	zkušený
hráči	hráč	k1gMnPc1	hráč
používají	používat	k5eAaImIp3nP	používat
bezpražcovou	bezpražcův	k2eAgFnSc7d1	bezpražcův
baskytaru	baskytara	k1gFnSc4	baskytara
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
kulatý	kulatý	k2eAgInSc1d1	kulatý
a	a	k8xC	a
jemný	jemný	k2eAgInSc1d1	jemný
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
zvuku	zvuk	k1gInSc3	zvuk
kontrabasu	kontrabas	k1gInSc2	kontrabas
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
baskytary	baskytara	k1gFnPc1	baskytara
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nahradily	nahradit	k5eAaPmAgFnP	nahradit
klasické	klasický	k2eAgInPc4d1	klasický
kontrabasy	kontrabas	k1gInPc4	kontrabas
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
velmi	velmi	k6eAd1	velmi
úzce	úzko	k6eAd1	úzko
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
rozmachem	rozmach	k1gInSc7	rozmach
rock	rock	k1gInSc1	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
rollové	rollový	k2eAgFnSc2d1	rollová
a	a	k8xC	a
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Balalajka	balalajka	k1gFnSc1	balalajka
Brač	Brač	k1gInSc1	Brač
(	(	kIx(	(
<g/>
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
<g/>
)	)	kIx)	)
Citera	citera	k1gFnSc1	citera
Kithara	kithara	k1gFnSc1	kithara
Mandolína	mandolína	k1gFnSc1	mandolína
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kytara	kytara	k1gFnSc1	kytara
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kytara	kytara	k1gFnSc1	kytara
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Kniha	kniha	k1gFnSc1	kniha
Příručka	příručka	k1gFnSc1	příručka
pro	pro	k7c4	pro
kytaristy	kytarista	k1gMnPc4	kytarista
ve	v	k7c6	v
Wikiknihách	Wikiknih	k1gInPc6	Wikiknih
Recenze	recenze	k1gFnSc2	recenze
<g/>
,	,	kIx,	,
novinky	novinka	k1gFnPc1	novinka
<g/>
,	,	kIx,	,
tabulatury	tabulatura	k1gFnPc1	tabulatura
<g/>
,	,	kIx,	,
fórum	fórum	k1gNnSc1	fórum
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
