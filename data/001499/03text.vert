<s>
Albert	Albert	k1gMnSc1	Albert
Abraham	Abraham	k1gMnSc1	Abraham
Michelson	Michelson	k1gMnSc1	Michelson
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1852	[number]	k4	1852
<g/>
,	,	kIx,	,
Strzelno	Strzelna	k1gFnSc5	Strzelna
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
Pasadena	Pasaden	k2eAgFnSc1d1	Pasadena
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
fyzik	fyzik	k1gMnSc1	fyzik
narozený	narozený	k2eAgMnSc1d1	narozený
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
do	do	k7c2	do
židovské	židovský	k2eAgFnSc2d1	židovská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
přesné	přesný	k2eAgInPc4d1	přesný
optické	optický	k2eAgInPc4d1	optický
přístroje	přístroj	k1gInPc4	přístroj
a	a	k8xC	a
výzkum	výzkum	k1gInSc4	výzkum
prováděný	prováděný	k2eAgInSc4d1	prováděný
pomocí	pomocí	k7c2	pomocí
nich	on	k3xPp3gInPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
dvouletý	dvouletý	k2eAgMnSc1d1	dvouletý
odešel	odejít	k5eAaPmAgMnS	odejít
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
San	San	k1gFnSc6	San
Francisku	Francisek	k1gInSc2	Francisek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
studia	studio	k1gNnSc2	studio
vykonal	vykonat	k5eAaPmAgInS	vykonat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1880	[number]	k4	1880
<g/>
-	-	kIx~	-
<g/>
1882	[number]	k4	1882
studijní	studijní	k2eAgFnSc4d1	studijní
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
různých	různý	k2eAgFnPc6d1	různá
vědeckých	vědecký	k2eAgFnPc6d1	vědecká
institucích	instituce	k1gFnPc6	instituce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
universitě	universita	k1gFnSc6	universita
ve	v	k7c6	v
Worcesteru	Worcester	k1gInSc6	Worcester
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1923	[number]	k4	1923
<g/>
-	-	kIx~	-
<g/>
1927	[number]	k4	1927
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
Národní	národní	k2eAgFnSc2d1	národní
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
optiky	optika	k1gFnSc2	optika
a	a	k8xC	a
zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
analýzou	analýza	k1gFnSc7	analýza
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
světelných	světelný	k2eAgInPc2d1	světelný
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
zaujaly	zaujmout	k5eAaPmAgInP	zaujmout
ho	on	k3xPp3gInSc4	on
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
éterovém	éterový	k2eAgInSc6d1	éterový
větru	vítr	k1gInSc6	vítr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
a	a	k8xC	a
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
chová	chovat	k5eAaImIp3nS	chovat
éter	éter	k1gInSc4	éter
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nehybný	hybný	k2eNgMnSc1d1	nehybný
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
vládne	vládnout	k5eAaImIp3nS	vládnout
jakýsi	jakýsi	k3yIgInSc1	jakýsi
éterový	éterový	k2eAgInSc1d1	éterový
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
jej	on	k3xPp3gInSc4	on
Země	země	k1gFnSc1	země
strhává	strhávat	k5eAaImIp3nS	strhávat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
předmětům	předmět	k1gInPc3	předmět
a	a	k8xC	a
přístrojům	přístroj	k1gInPc3	přístroj
na	na	k7c6	na
zemském	zemský	k2eAgNnSc6d1	zemské
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
?	?	kIx.	?
</s>
<s>
Přirozeně	přirozeně	k6eAd1	přirozeně
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mnozí	mnohý	k2eAgMnPc1d1	mnohý
fyzikové	fyzik	k1gMnPc1	fyzik
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
dát	dát	k5eAaPmF	dát
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
otázku	otázka	k1gFnSc4	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Nechyběl	chybět	k5eNaImAgMnS	chybět
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
ani	ani	k9	ani
Michelson	Michelson	k1gMnSc1	Michelson
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgInSc1	žádný
pokus	pokus	k1gInSc1	pokus
nebyl	být	k5eNaImAgInS	být
tak	tak	k6eAd1	tak
teoreticky	teoreticky	k6eAd1	teoreticky
názorný	názorný	k2eAgInSc1d1	názorný
a	a	k8xC	a
současně	současně	k6eAd1	současně
uskutečněný	uskutečněný	k2eAgInSc4d1	uskutečněný
s	s	k7c7	s
takovou	takový	k3xDgFnSc7	takový
jistotou	jistota	k1gFnSc7	jistota
jako	jako	k9	jako
jeho	jeho	k3xOp3gNnSc4	jeho
<g/>
.	.	kIx.	.
</s>
<s>
Srovnával	srovnávat	k5eAaImAgMnS	srovnávat
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
rychlosti	rychlost	k1gFnPc1	rychlost
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
světlo	světlo	k1gNnSc1	světlo
různými	různý	k2eAgInPc7d1	různý
směry	směr	k1gInPc7	směr
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
závěry	závěr	k1gInPc1	závěr
byly	být	k5eAaImAgInP	být
ohromující	ohromující	k2eAgInPc1d1	ohromující
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
existoval	existovat	k5eAaImAgInS	existovat
éterový	éterový	k2eAgInSc1d1	éterový
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgInS	působit
by	by	kYmCp3nS	by
mezi	mezi	k7c7	mezi
naměřenými	naměřený	k2eAgFnPc7d1	naměřená
rychlostmi	rychlost	k1gFnPc7	rychlost
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Michelson	Michelson	k1gInSc1	Michelson
žádné	žádný	k3yNgInPc4	žádný
rozdíly	rozdíl	k1gInPc4	rozdíl
nenaměřil	naměřit	k5eNaBmAgMnS	naměřit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
pokusy	pokus	k1gInPc4	pokus
udělal	udělat	k5eAaPmAgMnS	udělat
na	na	k7c6	na
studijní	studijní	k2eAgFnSc6d1	studijní
cestě	cesta	k1gFnSc6	cesta
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1880	[number]	k4	1880
<g/>
-	-	kIx~	-
<g/>
1881	[number]	k4	1881
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ho	on	k3xPp3gNnSc4	on
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
H.	H.	kA	H.
A.	A.	kA	A.
Lorentz	Lorentz	k1gMnSc1	Lorentz
upozornil	upozornit	k5eAaPmAgMnS	upozornit
na	na	k7c4	na
některé	některý	k3yIgInPc4	některý
nedostatky	nedostatek	k1gInPc4	nedostatek
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
pokus	pokus	k1gInSc1	pokus
opakoval	opakovat	k5eAaImAgInS	opakovat
v	v	k7c6	v
Clevelandu	Cleveland	k1gInSc6	Cleveland
s	s	k7c7	s
americkým	americký	k2eAgMnSc7d1	americký
fyzikem	fyzik	k1gMnSc7	fyzik
E.	E.	kA	E.
W.	W.	kA	W.
Morleyem	Morleyem	k1gInSc4	Morleyem
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgFnSc7d2	veliký
přesností	přesnost	k1gFnSc7	přesnost
<g/>
.	.	kIx.	.
</s>
<s>
Michelson	Michelson	k1gMnSc1	Michelson
vlastně	vlastně	k9	vlastně
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
pohybu	pohyb	k1gInSc2	pohyb
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
kolmém	kolmý	k2eAgInSc6d1	kolmý
na	na	k7c4	na
směr	směr	k1gInSc4	směr
jejího	její	k3xOp3gInSc2	její
pohybu	pohyb	k1gInSc2	pohyb
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
velká	velký	k2eAgFnSc1d1	velká
-	-	kIx~	-
tedy	tedy	k9	tedy
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
všemi	všecek	k3xTgInPc7	všecek
směry	směr	k1gInPc7	směr
stejnou	stejný	k2eAgFnSc7d1	stejná
<g/>
,	,	kIx,	,
neměnnou	neměnný	k2eAgFnSc7d1	neměnná
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
pohybu	pohyb	k1gInSc6	pohyb
jeho	on	k3xPp3gInSc2	on
zdroje	zdroj	k1gInSc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
negativní	negativní	k2eAgInSc4d1	negativní
výsledek	výsledek	k1gInSc4	výsledek
měl	mít	k5eAaImAgMnS	mít
pro	pro	k7c4	pro
fyziku	fyzika	k1gFnSc4	fyzika
obrovský	obrovský	k2eAgInSc4d1	obrovský
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Vedl	vést	k5eAaImAgMnS	vést
A.	A.	kA	A.
Einsteina	Einstein	k1gMnSc4	Einstein
k	k	k7c3	k
formulaci	formulace	k1gFnSc3	formulace
základního	základní	k2eAgInSc2d1	základní
postulátu	postulát	k1gInSc2	postulát
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
-	-	kIx~	-
k	k	k7c3	k
principu	princip	k1gInSc3	princip
konstantní	konstantní	k2eAgFnSc2d1	konstantní
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
pokusu	pokus	k1gInSc3	pokus
Michelson	Michelsona	k1gFnPc2	Michelsona
zkonstruoval	zkonstruovat	k5eAaPmAgInS	zkonstruovat
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
přístroj	přístroj	k1gInSc1	přístroj
-	-	kIx~	-
interferometr	interferometr	k1gInSc1	interferometr
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgNnSc7	který
provedl	provést	k5eAaPmAgInS	provést
i	i	k9	i
další	další	k2eAgInPc4d1	další
významné	významný	k2eAgInPc4d1	významný
experimenty	experiment	k1gInPc4	experiment
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1892	[number]	k4	1892
<g/>
-	-	kIx~	-
<g/>
1893	[number]	k4	1893
jím	jíst	k5eAaImIp1nS	jíst
srovnával	srovnávat	k5eAaImAgInS	srovnávat
délku	délka	k1gFnSc4	délka
metru	metr	k1gInSc2	metr
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
světelné	světelný	k2eAgFnSc2d1	světelná
vlny	vlna	k1gFnSc2	vlna
a	a	k8xC	a
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
strukturu	struktura	k1gFnSc4	struktura
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
čar	čára	k1gFnPc2	čára
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
rozpracoval	rozpracovat	k5eAaPmAgInS	rozpracovat
problémy	problém	k1gInPc4	problém
aplikace	aplikace	k1gFnSc2	aplikace
interferenční	interferenční	k2eAgFnSc2d1	interferenční
metody	metoda	k1gFnSc2	metoda
v	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
a	a	k8xC	a
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
interferometr	interferometr	k1gInSc4	interferometr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
měřit	měřit	k5eAaImF	měřit
úhlové	úhlový	k2eAgInPc4d1	úhlový
průměry	průměr	k1gInPc4	průměr
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Nic	nic	k3yNnSc1	nic
ho	on	k3xPp3gNnSc4	on
však	však	k9	však
již	již	k6eAd1	již
tolik	tolik	k6eAd1	tolik
neproslavilo	proslavit	k5eNaPmAgNnS	proslavit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Michelsonův	Michelsonův	k2eAgInSc1d1	Michelsonův
pokus	pokus	k1gInSc1	pokus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
Albert	Albert	k1gMnSc1	Albert
Abraham	Abraham	k1gMnSc1	Abraham
Michelson	Michelson	k1gMnSc1	Michelson
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Albert	Alberta	k1gFnPc2	Alberta
Abraham	Abraham	k1gMnSc1	Abraham
Michelson	Michelson	k1gMnSc1	Michelson
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
