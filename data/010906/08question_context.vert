<s>
Angličtina	angličtina	k1gFnSc1	angličtina
je	být	k5eAaImIp3nS	být
západogermánský	západogermánský	k2eAgInSc4d1	západogermánský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
na	na	k7c6	na
území	území	k1gNnSc6	území
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třetím	třetí	k4xOgInSc7	třetí
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
mateřským	mateřský	k2eAgInSc7d1	mateřský
jazykem	jazyk	k1gInSc7	jazyk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
