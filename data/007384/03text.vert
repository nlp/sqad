<s>
Daiquiri	Daiquiri	k6eAd1	Daiquiri
je	být	k5eAaImIp3nS	být
koktejl	koktejl	k1gInSc4	koktejl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
rum	rum	k1gInSc4	rum
<g/>
,	,	kIx,	,
limetový	limetový	k2eAgInSc4d1	limetový
džus	džus	k1gInSc4	džus
<g/>
,	,	kIx,	,
cukrový	cukrový	k2eAgInSc4d1	cukrový
sirup	sirup	k1gInSc4	sirup
a	a	k8xC	a
tlučený	tlučený	k2eAgInSc4d1	tlučený
led	led	k1gInSc4	led
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vymyšlen	vymyslet	k5eAaPmNgInS	vymyslet
kolem	kolem	k7c2	kolem
r.	r.	kA	r.
1896	[number]	k4	1896
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Santiago	Santiago	k1gNnSc4	Santiago
de	de	k?	de
Cuba	Cuba	k1gFnSc1	Cuba
americkým	americký	k2eAgMnSc7d1	americký
důlním	důlní	k2eAgMnSc7d1	důlní
inženýrem	inženýr	k1gMnSc7	inženýr
Jenningem	Jenning	k1gInSc7	Jenning
Coxem	Coxem	k1gInSc1	Coxem
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
nedostatku	nedostatek	k1gInSc2	nedostatek
ginu	gin	k1gInSc2	gin
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
dostal	dostat	k5eAaPmAgInS	dostat
podle	podle	k7c2	podle
místního	místní	k2eAgInSc2d1	místní
dolu	dol	k1gInSc2	dol
na	na	k7c4	na
železnou	železný	k2eAgFnSc4d1	železná
rudu	ruda	k1gFnSc4	ruda
a	a	k8xC	a
přilehlé	přilehlý	k2eAgFnPc4d1	přilehlá
pláže	pláž	k1gFnPc4	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
koktejl	koktejl	k1gInSc1	koktejl
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
také	také	k9	také
známý	známý	k1gMnSc1	známý
spisovatel	spisovatel	k1gMnSc1	spisovatel
Ernest	Ernest	k1gMnSc1	Ernest
Hemingway	Hemingwaa	k1gFnSc2	Hemingwaa
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ho	on	k3xPp3gMnSc4	on
ale	ale	k8xC	ale
pil	pít	k5eAaImAgMnS	pít
bez	bez	k7c2	bez
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
