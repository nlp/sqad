<s>
Daiquiri	Daiquiri	k6eAd1	Daiquiri
je	být	k5eAaImIp3nS	být
koktejl	koktejl	k1gInSc4	koktejl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
rum	rum	k1gInSc4	rum
<g/>
,	,	kIx,	,
limetový	limetový	k2eAgInSc4d1	limetový
džus	džus	k1gInSc4	džus
<g/>
,	,	kIx,	,
cukrový	cukrový	k2eAgInSc4d1	cukrový
sirup	sirup	k1gInSc4	sirup
a	a	k8xC	a
tlučený	tlučený	k2eAgInSc4d1	tlučený
led	led	k1gInSc4	led
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vymyšlen	vymyslet	k5eAaPmNgInS	vymyslet
kolem	kolem	k7c2	kolem
r.	r.	kA	r.
1896	[number]	k4	1896
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Santiago	Santiago	k1gNnSc4	Santiago
de	de	k?	de
Cuba	Cuba	k1gFnSc1	Cuba
americkým	americký	k2eAgMnSc7d1	americký
důlním	důlní	k2eAgMnSc7d1	důlní
inženýrem	inženýr	k1gMnSc7	inženýr
Jenningem	Jenning	k1gInSc7	Jenning
Coxem	Coxem	k1gInSc1	Coxem
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
nedostatku	nedostatek	k1gInSc2	nedostatek
ginu	gin	k1gInSc2	gin
<g/>
.	.	kIx.	.
</s>
