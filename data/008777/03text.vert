<p>
<s>
Orangutan	orangutan	k1gMnSc1	orangutan
(	(	kIx(	(
<g/>
Pongo	Pongo	k1gMnSc1	Pongo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
hominida	hominid	k1gMnSc2	hominid
žijící	žijící	k2eAgInSc4d1	žijící
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Borneo	Borneo	k1gNnSc1	Borneo
a	a	k8xC	a
Sumatra	Sumatra	k1gFnSc1	Sumatra
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
zde	zde	k6eAd1	zde
tři	tři	k4xCgInPc4	tři
druhy	druh	k1gInPc4	druh
<g/>
:	:	kIx,	:
orangutana	orangutan	k1gMnSc2	orangutan
sumaterského	sumaterský	k2eAgMnSc2d1	sumaterský
(	(	kIx(	(
<g/>
Pongo	Pongo	k1gMnSc1	Pongo
abelii	abelie	k1gFnSc4	abelie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
orangutana	orangutan	k1gMnSc2	orangutan
tapanulijského	tapanulijský	k2eAgMnSc2d1	tapanulijský
(	(	kIx(	(
<g/>
Pongo	Pongo	k1gNnSc1	Pongo
tapanuliensis	tapanuliensis	k1gFnSc2	tapanuliensis
<g/>
)	)	kIx)	)
a	a	k8xC	a
orangutana	orangutan	k1gMnSc2	orangutan
bornejského	bornejský	k2eAgMnSc2d1	bornejský
(	(	kIx(	(
<g/>
Pongo	Pongo	k1gMnSc1	Pongo
pygmaeus	pygmaeus	k1gMnSc1	pygmaeus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgInSc2	jenž
byly	být	k5eAaImAgInP	být
navíc	navíc	k6eAd1	navíc
zjištěny	zjistit	k5eAaPmNgInP	zjistit
tři	tři	k4xCgInPc1	tři
poddruhy	poddruh	k1gInPc1	poddruh
(	(	kIx(	(
<g/>
subspecie	subspecie	k1gFnSc1	subspecie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
orangutanů	orangutan	k1gMnPc2	orangutan
obvykle	obvykle	k6eAd1	obvykle
měří	měřit	k5eAaImIp3nS	měřit
na	na	k7c4	na
výšku	výška	k1gFnSc4	výška
okolo	okolo	k7c2	okolo
115	[number]	k4	115
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
37	[number]	k4	37
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
velikosti	velikost	k1gFnSc3	velikost
asi	asi	k9	asi
136	[number]	k4	136
cm	cm	kA	cm
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
75	[number]	k4	75
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
červenohnědá	červenohnědý	k2eAgFnSc1d1	červenohnědá
srst	srst	k1gFnSc1	srst
(	(	kIx(	(
<g/>
u	u	k7c2	u
orangutana	orangutan	k1gMnSc2	orangutan
sumaterského	sumaterský	k2eAgInSc2d1	sumaterský
je	být	k5eAaImIp3nS	být
řidší	řídký	k2eAgInSc1d2	řidší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kůže	kůže	k1gFnSc1	kůže
má	mít	k5eAaImIp3nS	mít
šedočerné	šedočerný	k2eAgNnSc4d1	šedočerné
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Orangutan	orangutan	k1gMnSc1	orangutan
má	mít	k5eAaImIp3nS	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
obličej	obličej	k1gInSc4	obličej
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
nepokrývá	pokrývat	k5eNaImIp3nS	pokrývat
srst	srst	k1gFnSc1	srst
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samcům	samec	k1gMnPc3	samec
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
může	moct	k5eAaImIp3nS	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
knír	knír	k1gInSc4	knír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Orangutani	orangutan	k1gMnPc1	orangutan
žijí	žít	k5eAaImIp3nP	žít
více	hodně	k6eAd2	hodně
samotářský	samotářský	k2eAgInSc4d1	samotářský
život	život	k1gInSc4	život
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc1d1	ostatní
velcí	velký	k2eAgMnPc1d1	velký
lidoopi	lidoop	k1gMnPc1	lidoop
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
sociálních	sociální	k2eAgFnPc2d1	sociální
vazeb	vazba	k1gFnPc2	vazba
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
samicemi	samice	k1gFnPc7	samice
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc7	jejich
potomky	potomek	k1gMnPc7	potomek
<g/>
,	,	kIx,	,
dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
jsou	být	k5eAaImIp3nP	být
solitéry	solitéra	k1gFnPc4	solitéra
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
tráví	trávit	k5eAaImIp3nS	trávit
získáváním	získávání	k1gNnSc7	získávání
potravy	potrava	k1gFnSc2	potrava
(	(	kIx(	(
<g/>
65	[number]	k4	65
až	až	k9	až
90	[number]	k4	90
%	%	kIx~	%
stravy	strava	k1gFnPc1	strava
tvoří	tvořit	k5eAaImIp3nP	tvořit
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odpočinkem	odpočinek	k1gInSc7	odpočinek
a	a	k8xC	a
přemisťováním	přemisťování	k1gNnSc7	přemisťování
se	se	k3xPyFc4	se
na	na	k7c4	na
jiná	jiný	k2eAgNnPc4d1	jiné
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc4	den
začínají	začínat	k5eAaImIp3nP	začínat
krmením	krmení	k1gNnPc3	krmení
po	po	k7c6	po
dobu	doba	k1gFnSc4	doba
dvou	dva	k4xCgMnPc6	dva
až	až	k9	až
tří	tři	k4xCgFnPc2	tři
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
poledne	poledne	k1gNnSc2	poledne
odpočívají	odpočívat	k5eAaImIp3nP	odpočívat
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozdních	pozdní	k2eAgFnPc6d1	pozdní
odpoledních	odpolední	k2eAgFnPc6d1	odpolední
hodinách	hodina	k1gFnPc6	hodina
se	se	k3xPyFc4	se
přesouvají	přesouvat	k5eAaImIp3nP	přesouvat
na	na	k7c4	na
jiná	jiný	k2eAgNnPc4d1	jiné
stanoviště	stanoviště	k1gNnPc4	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
si	se	k3xPyFc3	se
začnou	začít	k5eAaPmIp3nP	začít
připravovat	připravovat	k5eAaImF	připravovat
hnízda	hnízdo	k1gNnPc4	hnízdo
na	na	k7c4	na
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samice	samice	k1gFnSc1	samice
nejčastěji	často	k6eAd3	často
rodí	rodit	k5eAaImIp3nS	rodit
jedno	jeden	k4xCgNnSc1	jeden
mládě	mládě	k1gNnSc1	mládě
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
dvojčata	dvojče	k1gNnPc4	dvojče
<g/>
;	;	kIx,	;
za	za	k7c4	za
život	život	k1gInSc4	život
odchová	odchovat	k5eAaPmIp3nS	odchovat
maximálně	maximálně	k6eAd1	maximálně
čtyři	čtyři	k4xCgMnPc4	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
se	se	k3xPyFc4	se
do	do	k7c2	do
výchovy	výchova	k1gFnSc2	výchova
mláděte	mládě	k1gNnSc2	mládě
téměř	téměř	k6eAd1	téměř
nezapojují	zapojovat	k5eNaImIp3nP	zapojovat
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
péče	péče	k1gFnSc2	péče
a	a	k8xC	a
socializaci	socializace	k1gFnSc4	socializace
provádí	provádět	k5eAaImIp3nS	provádět
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jej	on	k3xPp3gMnSc4	on
často	často	k6eAd1	často
vychovává	vychovávat	k5eAaImIp3nS	vychovávat
se	s	k7c7	s
starším	starý	k2eAgMnSc7d2	starší
potomkem	potomek	k1gMnSc7	potomek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
dva	dva	k4xCgInPc1	dva
roky	rok	k1gInPc1	rok
jsou	být	k5eAaImIp3nP	být
mláďata	mládě	k1gNnPc4	mládě
na	na	k7c6	na
matce	matka	k1gFnSc6	matka
plně	plně	k6eAd1	plně
závislá	závislý	k2eAgFnSc1d1	závislá
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nosí	nosit	k5eAaImIp3nP	nosit
<g/>
,	,	kIx,	,
krmí	krmit	k5eAaImIp3nP	krmit
a	a	k8xC	a
spí	spát	k5eAaImIp3nP	spát
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
hnízdě	hnízdo	k1gNnSc6	hnízdo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
první	první	k4xOgInPc4	první
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
se	se	k3xPyFc4	se
mládě	mládě	k1gNnSc1	mládě
jen	jen	k9	jen
drží	držet	k5eAaImIp3nS	držet
matčina	matčin	k2eAgNnPc4d1	matčino
břicha	břicho	k1gNnPc4	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Odstavena	odstaven	k2eAgFnSc1d1	odstavena
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
divočině	divočina	k1gFnSc6	divočina
i	i	k8xC	i
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
orangutani	orangutan	k1gMnPc1	orangutan
mohou	moct	k5eAaImIp3nP	moct
dožít	dožít	k5eAaPmF	dožít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
svaz	svaz	k1gInSc1	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
považuje	považovat	k5eAaImIp3nS	považovat
orangutana	orangutan	k1gMnSc2	orangutan
bornejského	bornejský	k2eAgMnSc2d1	bornejský
i	i	k8xC	i
sumaterského	sumaterský	k2eAgNnSc2d1	sumaterské
za	za	k7c4	za
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgNnSc1d1	ohrožené
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
představuje	představovat	k5eAaImIp3nS	představovat
ztráta	ztráta	k1gFnSc1	ztráta
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
také	také	k9	také
lov	lov	k1gInSc4	lov
<g/>
.	.	kIx.	.
</s>
<s>
Lidoop	lidoop	k1gMnSc1	lidoop
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
i	i	k9	i
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
některé	některý	k3yIgMnPc4	některý
lidové	lidový	k2eAgMnPc4d1	lidový
příběhy	příběh	k1gInPc4	příběh
pojednávají	pojednávat	k5eAaImIp3nP	pojednávat
o	o	k7c6	o
orangutanech	orangutan	k1gMnPc6	orangutan
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
kopulují	kopulovat	k5eAaImIp3nP	kopulovat
a	a	k8xC	a
unáší	unášet	k5eAaImIp3nP	unášet
je	on	k3xPp3gFnPc4	on
<g/>
;	;	kIx,	;
známy	znám	k2eAgFnPc4d1	známa
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
příběhy	příběh	k1gInPc1	příběh
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
jsou	být	k5eAaImIp3nP	být
muži	muž	k1gMnPc1	muž
svedeni	sveden	k2eAgMnPc1d1	sveden
samicemi	samice	k1gFnPc7	samice
orangutanů	orangutan	k1gMnPc2	orangutan
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
lidí	člověk	k1gMnPc2	člověk
žijících	žijící	k2eAgInPc2d1	žijící
v	v	k7c6	v
centrálním	centrální	k2eAgNnSc6d1	centrální
Borneu	Borneo	k1gNnSc6	Borneo
přináší	přinášet	k5eAaImIp3nS	přinášet
smůlu	smůla	k1gFnSc4	smůla
podívat	podívat	k5eAaPmF	podívat
se	se	k3xPyFc4	se
orangutanovi	orangutan	k1gMnSc6	orangutan
do	do	k7c2	do
obličeje	obličej	k1gInSc2	obličej
tváří	tvář	k1gFnPc2	tvář
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
orangutan	orangutan	k1gMnSc1	orangutan
<g/>
,	,	kIx,	,
také	také	k9	také
orang-utan	orangtan	k1gInSc1	orang-utan
<g/>
,	,	kIx,	,
orang	orang	k1gInSc1	orang
utan	utana	k1gFnPc2	utana
<g/>
,	,	kIx,	,
orangutang	orangutanga	k1gFnPc2	orangutanga
či	či	k8xC	či
ourang-outang	ourangutanga	k1gFnPc2	ourang-outanga
je	být	k5eAaImIp3nS	být
složené	složený	k2eAgNnSc1d1	složené
z	z	k7c2	z
malajských	malajský	k2eAgNnPc2d1	Malajské
a	a	k8xC	a
indonéských	indonéský	k2eAgNnPc2d1	indonéské
slov	slovo	k1gNnPc2	slovo
orang	orang	k1gInSc1	orang
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
<g/>
)	)	kIx)	)
a	a	k8xC	a
hutan	hutan	k1gInSc1	hutan
(	(	kIx(	(
<g/>
les	les	k1gInSc1	les
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
lesní	lesní	k2eAgMnSc1d1	lesní
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
banjarštiny	banjarština	k1gFnSc2	banjarština
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
ostrova	ostrov	k1gInSc2	ostrov
Jáva	Jáva	k1gFnSc1	Jáva
jej	on	k3xPp3gMnSc4	on
(	(	kIx(	(
<g/>
možná	možná	k9	možná
<g/>
)	)	kIx)	)
používali	používat	k5eAaImAgMnP	používat
jako	jako	k9	jako
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
kmeny	kmen	k1gInPc4	kmen
domorodců	domorodec	k1gMnPc2	domorodec
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
Evropané	Evropan	k1gMnPc1	Evropan
pak	pak	k6eAd1	pak
použili	použít	k5eAaPmAgMnP	použít
termín	termín	k1gInSc4	termín
jako	jako	k8xC	jako
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
primáta	primát	k1gMnSc4	primát
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
orangutan	orangutan	k1gMnSc1	orangutan
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
Historiae	Historia	k1gFnSc2	Historia
Naturalis	Naturalis	k1gFnSc2	Naturalis
et	et	k?	et
Medicae	Medica	k1gFnSc2	Medica
Indiae	India	k1gFnSc2	India
orientalis	orientalis	k1gFnSc2	orientalis
od	od	k7c2	od
nizozemského	nizozemský	k2eAgMnSc2d1	nizozemský
lékaře	lékař	k1gMnSc2	lékař
Jacoba	Jacoba	k1gFnSc1	Jacoba
Bontia	Bontia	k1gFnSc1	Bontia
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1631	[number]	k4	1631
<g/>
,	,	kIx,	,
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
publikacích	publikace	k1gFnPc6	publikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1691	[number]	k4	1691
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
slovo	slovo	k1gNnSc4	slovo
orang-outang	orangutanga	k1gFnPc2	orang-outanga
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nesprávné	správný	k2eNgNnSc1d1	nesprávné
<g/>
,	,	kIx,	,
varianty	varianta	k1gFnPc1	varianta
s	s	k7c7	s
koncovkou	koncovka	k1gFnSc7	koncovka
-ng	g	k?	-ng
<g/>
.	.	kIx.	.
<g/>
Rodové	rodový	k2eAgNnSc1d1	rodové
jméno	jméno	k1gNnSc1	jméno
Pongo	Pongo	k6eAd1	Pongo
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
od	od	k7c2	od
Andrewa	Andrewus	k1gMnSc2	Andrewus
Battela	Battel	k1gMnSc2	Battel
<g/>
,	,	kIx,	,
anglického	anglický	k2eAgMnSc2d1	anglický
námořníka	námořník	k1gMnSc2	námořník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
portugalském	portugalský	k2eAgNnSc6d1	portugalské
zajetí	zajetí	k1gNnSc6	zajetí
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Angoly	Angola	k1gFnSc2	Angola
<g/>
,	,	kIx,	,
a	a	k8xC	a
popsal	popsat	k5eAaPmAgMnS	popsat
dvě	dva	k4xCgFnPc4	dva
antropoidní	antropoidní	k2eAgFnPc4d1	antropoidní
"	"	kIx"	"
<g/>
obludy	obluda	k1gFnPc4	obluda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Pongo	Pongo	k6eAd1	Pongo
a	a	k8xC	a
Engeco	Engeco	k6eAd1	Engeco
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
gorily	gorila	k1gFnSc2	gorila
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
pak	pak	k6eAd1	pak
jméno	jméno	k1gNnSc4	jméno
Pongo	Pongo	k6eAd1	Pongo
přírodovědci	přírodovědec	k1gMnPc1	přírodovědec
použili	použít	k5eAaPmAgMnP	použít
obecně	obecně	k6eAd1	obecně
pro	pro	k7c4	pro
velké	velký	k2eAgMnPc4d1	velký
lidoopy	lidoop	k1gMnPc4	lidoop
<g/>
.	.	kIx.	.
</s>
<s>
Bernard	Bernard	k1gMnSc1	Bernard
Germain	Germain	k1gMnSc1	Germain
de	de	k?	de
Lacépè	Lacépè	k1gMnSc1	Lacépè
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
<g/>
,	,	kIx,	,
jím	on	k3xPp3gNnSc7	on
poté	poté	k6eAd1	poté
popsal	popsat	k5eAaPmAgMnS	popsat
rod	rod	k1gInSc4	rod
lidoopa	lidoop	k1gMnSc2	lidoop
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
kostru	kostra	k1gFnSc4	kostra
obdržel	obdržet	k5eAaPmAgMnS	obdržet
od	od	k7c2	od
Friedricha	Friedrich	k1gMnSc2	Friedrich
von	von	k1gInSc4	von
Wurmba	Wurmba	k1gMnSc1	Wurmba
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Orangutana	orangutan	k1gMnSc4	orangutan
poprvé	poprvé	k6eAd1	poprvé
vědecky	vědecky	k6eAd1	vědecky
popsal	popsat	k5eAaPmAgMnS	popsat
Carl	Carl	k1gMnSc1	Carl
Linné	Linná	k1gFnSc2	Linná
jako	jako	k8xC	jako
Simia	Simius	k1gMnSc4	Simius
satyrus	satyrus	k1gInSc4	satyrus
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Systema	System	k1gMnSc4	System
naturae	natura	k1gMnSc4	natura
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
populace	populace	k1gFnPc1	populace
orangutanů	orangutan	k1gMnPc2	orangutan
byly	být	k5eAaImAgFnP	být
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
hodnoceny	hodnocen	k2eAgInPc1d1	hodnocen
jako	jako	k8xS	jako
poddruhy	poddruh	k1gInPc1	poddruh
jednoho	jeden	k4xCgInSc2	jeden
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
byly	být	k5eAaImAgInP	být
rozlišeny	rozlišen	k2eAgInPc1d1	rozlišen
druhy	druh	k1gInPc1	druh
dva	dva	k4xCgMnPc1	dva
<g/>
:	:	kIx,	:
orangutan	orangutan	k1gMnSc1	orangutan
bornejský	bornejský	k2eAgMnSc1d1	bornejský
(	(	kIx(	(
<g/>
Pongo	Pongo	k1gMnSc1	Pongo
pygmaeus	pygmaeus	k1gMnSc1	pygmaeus
<g/>
)	)	kIx)	)
a	a	k8xC	a
orangutan	orangutan	k1gMnSc1	orangutan
sumaterský	sumaterský	k2eAgMnSc1d1	sumaterský
(	(	kIx(	(
<g/>
Pongo	Pongo	k1gMnSc1	Pongo
abelii	abelie	k1gFnSc4	abelie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
orangutana	orangutan	k1gMnSc2	orangutan
bornejského	bornejský	k2eAgMnSc4d1	bornejský
navíc	navíc	k6eAd1	navíc
byly	být	k5eAaImAgInP	být
určeny	určen	k2eAgInPc4d1	určen
poddruhy	poddruh	k1gInPc4	poddruh
Pongo	Pongo	k1gMnSc1	Pongo
pygmaeus	pygmaeus	k1gMnSc1	pygmaeus
pygmaeus	pygmaeus	k1gMnSc1	pygmaeus
(	(	kIx(	(
<g/>
severozápadní	severozápadní	k2eAgFnPc1d1	severozápadní
populace	populace	k1gFnPc1	populace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pongo	Pongo	k1gMnSc1	Pongo
pygmaeus	pygmaeus	k1gMnSc1	pygmaeus
morio	morio	k1gMnSc1	morio
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc2d1	východní
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pongo	Pongo	k1gMnSc1	Pongo
pygmaeus	pygmaeus	k1gMnSc1	pygmaeus
wurmbii	wurmbie	k1gFnSc4	wurmbie
(	(	kIx(	(
<g/>
jihozápadní	jihozápadní	k2eAgFnPc1d1	jihozápadní
populace	populace	k1gFnPc1	populace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc1d1	poslední
poddruh	poddruh	k1gInSc1	poddruh
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
příbuznější	příbuzný	k2eAgMnSc1d2	příbuznější
orangutanovi	orangutan	k1gMnSc3	orangutan
sumaterskému	sumaterský	k2eAgInSc3d1	sumaterský
než	než	k8xS	než
prvé	prvý	k4xOgInPc4	prvý
dva	dva	k4xCgInPc1	dva
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
existují	existovat	k5eAaImIp3nP	existovat
fosilie	fosilie	k1gFnPc1	fosilie
z	z	k7c2	z
Vietnamu	Vietnam	k1gInSc2	Vietnam
popisované	popisovaný	k2eAgInPc4d1	popisovaný
jako	jako	k8xS	jako
Pongo	Pongo	k6eAd1	Pongo
hooijeri	hooijeri	k6eAd1	hooijeri
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
poddruh	poddruh	k1gInSc4	poddruh
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
orangutanů	orangutan	k1gMnPc2	orangutan
nebo	nebo	k8xC	nebo
o	o	k7c6	o
samostatný	samostatný	k2eAgMnSc1d1	samostatný
species	species	k1gFnPc4	species
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
analýzy	analýza	k1gFnSc2	analýza
genomu	genom	k1gInSc2	genom
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
genetická	genetický	k2eAgFnSc1d1	genetická
diverzita	diverzita	k1gFnSc1	diverzita
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
u	u	k7c2	u
orangutana	orangutan	k1gMnSc2	orangutan
bornejského	bornejský	k2eAgMnSc2d1	bornejský
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jeho	jeho	k3xOp3gFnSc1	jeho
populace	populace	k1gFnSc1	populace
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
sumaterské	sumaterský	k2eAgMnPc4d1	sumaterský
orangutany	orangutan	k1gMnPc4	orangutan
šestkrát	šestkrát	k6eAd1	šestkrát
až	až	k6eAd1	až
sedmkrát	sedmkrát	k6eAd1	sedmkrát
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
oddělily	oddělit	k5eAaPmAgFnP	oddělit
před	před	k7c7	před
400	[number]	k4	400
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Genetickou	genetický	k2eAgFnSc4d1	genetická
výbavu	výbava	k1gFnSc4	výbava
tvoří	tvořit	k5eAaImIp3nS	tvořit
diploidní	diploidní	k2eAgInSc1d1	diploidní
karyotyp	karyotyp	k1gInSc1	karyotyp
2	[number]	k4	2
<g/>
n	n	k0	n
=	=	kIx~	=
48	[number]	k4	48
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
.	.	kIx.	.
</s>
<s>
Druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
mohou	moct	k5eAaImIp3nP	moct
křížit	křížit	k5eAaImF	křížit
<g/>
,	,	kIx,	,
následní	následní	k2eAgMnPc1d1	následní
hybridi	hybrid	k1gMnPc1	hybrid
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
plodní	plodní	k2eAgMnPc1d1	plodní
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
populace	populace	k1gFnSc1	populace
orangutanů	orangutan	k1gMnPc2	orangutan
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
hoských	hoský	k2eAgFnPc6d1	hoský
oblastech	oblast	k1gFnPc6	oblast
Sumatry	Sumatra	k1gFnSc2	Sumatra
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
jezera	jezero	k1gNnSc2	jezero
Toba	Tob	k1gInSc2	Tob
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
jižněji	jižně	k6eAd2	jižně
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
areál	areál	k1gInSc1	areál
výskytu	výskyt	k1gInSc2	výskyt
orangutana	orangutan	k1gMnSc2	orangutan
sumaterského	sumaterský	k2eAgMnSc2d1	sumaterský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čítající	čítající	k2eAgFnSc1d1	čítající
asi	asi	k9	asi
800	[number]	k4	800
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
identifikována	identifikován	k2eAgFnSc1d1	identifikována
jako	jako	k8xS	jako
odlišný	odlišný	k2eAgInSc1d1	odlišný
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
orangutan	orangutan	k1gMnSc1	orangutan
tapanulijský	tapanulijský	k2eAgMnSc1d1	tapanulijský
(	(	kIx(	(
<g/>
Pongo	Pongo	k1gMnSc1	Pongo
tapanuliensis	tapanuliensis	k1gFnSc2	tapanuliensis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
genomové	genomový	k2eAgFnSc2d1	genomová
analýzy	analýza	k1gFnSc2	analýza
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
linie	linie	k1gFnSc1	linie
oddělila	oddělit	k5eAaPmAgFnS	oddělit
už	už	k6eAd1	už
asi	asi	k9	asi
před	před	k7c7	před
3,38	[number]	k4	3,38
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
zbývající	zbývající	k2eAgInPc1d1	zbývající
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
vzájemně	vzájemně	k6eAd1	vzájemně
příbuznější	příbuzný	k2eAgMnSc1d2	příbuznější
<g/>
.	.	kIx.	.
<g/>
Tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
orangutanů	orangutan	k1gMnPc2	orangutan
jsou	být	k5eAaImIp3nP	být
jediné	jediný	k2eAgInPc1d1	jediný
žijící	žijící	k2eAgInPc1d1	žijící
druhy	druh	k1gInPc1	druh
podčeledi	podčeleď	k1gFnSc2	podčeleď
orangutani	orangutan	k1gMnPc5	orangutan
(	(	kIx(	(
<g/>
Ponginae	Ponginae	k1gFnSc1	Ponginae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
podčeledi	podčeleď	k1gFnSc2	podčeleď
patřily	patřit	k5eAaImAgInP	patřit
i	i	k9	i
vyhynulé	vyhynulý	k2eAgInPc1d1	vyhynulý
rody	rod	k1gInPc1	rod
Lufengpithecus	Lufengpithecus	k1gMnSc1	Lufengpithecus
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
Thajsku	Thajsko	k1gNnSc6	Thajsko
před	před	k7c7	před
2	[number]	k4	2
<g/>
−	−	k?	−
<g/>
8	[number]	k4	8
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
Sivapithecus	Sivapithecus	k1gInSc4	Sivapithecus
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgFnSc4d1	obývající
Indii	Indie	k1gFnSc4	Indie
a	a	k8xC	a
Pákistán	Pákistán	k1gInSc4	Pákistán
před	před	k7c7	před
8,5	[number]	k4	8,5
<g/>
−	−	k?	−
<g/>
12,5	[number]	k4	12,5
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
;	;	kIx,	;
tito	tento	k3xDgMnPc1	tento
hominidé	hominid	k1gMnPc1	hominid
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
chladnějších	chladný	k2eAgFnPc6d2	chladnější
a	a	k8xC	a
sušších	suchý	k2eAgFnPc6d2	sušší
oblastech	oblast	k1gFnPc6	oblast
než	než	k8xS	než
dnešní	dnešní	k2eAgMnPc1d1	dnešní
orangutani	orangutan	k1gMnPc1	orangutan
<g/>
.	.	kIx.	.
</s>
<s>
Podčeleď	podčeleď	k1gFnSc1	podčeleď
se	se	k3xPyFc4	se
oddělila	oddělit	k5eAaPmAgFnS	oddělit
od	od	k7c2	od
afrických	africký	k2eAgMnPc2d1	africký
lidoopů	lidoop	k1gMnPc2	lidoop
před	před	k7c7	před
15,7	[number]	k4	15,7
až	až	k8xS	až
19,3	[number]	k4	19,3
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližším	blízký	k2eAgMnPc3d3	nejbližší
příbuzným	příbuzný	k1gMnPc3	příbuzný
orangutanů	orangutan	k1gMnPc2	orangutan
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Khoratpithecus	Khoratpithecus	k1gInSc4	Khoratpithecus
piriyai	piriya	k1gFnSc2	piriya
obývající	obývající	k2eAgNnSc1d1	obývající
Thajsko	Thajsko	k1gNnSc1	Thajsko
před	před	k7c7	před
5	[number]	k4	5
až	až	k8xS	až
7	[number]	k4	7
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Orangutani	orangutan	k1gMnPc1	orangutan
obývají	obývat	k5eAaImIp3nP	obývat
Borneo	Borneo	k1gNnSc4	Borneo
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnSc4d1	severozápadní
Sumatru	Sumatra	k1gFnSc4	Sumatra
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
tedy	tedy	k9	tedy
jediní	jediný	k2eAgMnPc1d1	jediný
lidoopi	lidoop	k1gMnPc1	lidoop
žijící	žijící	k2eAgMnPc1d1	žijící
mimo	mimo	k7c4	mimo
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
bylo	být	k5eAaImAgNnS	být
jejich	jejich	k3xOp3gFnPc4	jejich
rozšíření	rozšíření	k1gNnSc4	rozšíření
větší	veliký	k2eAgNnSc4d2	veliký
a	a	k8xC	a
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
i	i	k9	i
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
,	,	kIx,	,
Malajském	malajský	k2eAgNnSc6d1	Malajské
poloostrovu	poloostrov	k1gInSc3	poloostrov
či	či	k8xC	či
ostrovu	ostrov	k1gInSc3	ostrov
Jáva	Jáva	k1gFnSc1	Jáva
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
konci	konec	k1gInSc6	konec
pleistocénu	pleistocén	k1gInSc2	pleistocén
<g/>
,	,	kIx,	,
možná	možná	k9	možná
i	i	k9	i
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
holocénu	holocén	k1gInSc2	holocén
<g/>
.	.	kIx.	.
</s>
<s>
Orangutani	orangutan	k1gMnPc1	orangutan
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
primárních	primární	k2eAgInPc6d1	primární
i	i	k8xC	i
sekundárních	sekundární	k2eAgInPc6d1	sekundární
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
horských	horský	k2eAgInPc6d1	horský
i	i	k8xC	i
nížinných	nížinný	k2eAgInPc6d1	nížinný
lesích	les	k1gInPc6	les
tvořených	tvořený	k2eAgInPc6d1	tvořený
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
dvojkřídláčovitých	dvojkřídláčovití	k1gMnPc2	dvojkřídláčovití
i	i	k9	i
v	v	k7c6	v
lužních	lužní	k2eAgInPc6d1	lužní
lesích	les	k1gInPc6	les
v	v	k7c6	v
bažinatých	bažinatý	k2eAgFnPc6d1	bažinatá
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Žít	žít	k5eAaImF	žít
mohou	moct	k5eAaImIp3nP	moct
ale	ale	k8xC	ale
například	například	k6eAd1	například
i	i	k9	i
na	na	k7c6	na
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
,	,	kIx,	,
obdělávaných	obdělávaný	k2eAgNnPc6d1	obdělávané
polích	pole	k1gNnPc6	pole
či	či	k8xC	či
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
mělkých	mělký	k2eAgNnPc2d1	mělké
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
obývají	obývat	k5eAaImIp3nP	obývat
oblasti	oblast	k1gFnPc1	oblast
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
km	km	kA	km
od	od	k7c2	od
vodního	vodní	k2eAgInSc2d1	vodní
zdroje	zdroj	k1gInSc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
veškerý	veškerý	k3xTgInSc4	veškerý
čas	čas	k1gInSc4	čas
tráví	trávit	k5eAaImIp3nP	trávit
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
<g/>
Orangutani	orangutan	k1gMnPc1	orangutan
sumaterští	sumaterský	k2eAgMnPc1d1	sumaterský
žijí	žít	k5eAaImIp3nP	žít
až	až	k9	až
do	do	k7c2	do
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
1500	[number]	k4	1500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
orangutani	orangutan	k1gMnPc1	orangutan
bornejští	bornejský	k2eAgMnPc1d1	bornejský
do	do	k7c2	do
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
nepřesahující	přesahující	k2eNgFnSc2d1	nepřesahující
1000	[number]	k4	1000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Orangutan	orangutan	k1gMnSc1	orangutan
je	být	k5eAaImIp3nS	být
lidoop	lidoop	k1gMnSc1	lidoop
s	s	k7c7	s
černou	černý	k2eAgFnSc7d1	černá
kůží	kůže	k1gFnSc7	kůže
a	a	k8xC	a
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
srstí	srst	k1gFnSc7	srst
načervenalého	načervenalý	k2eAgNnSc2d1	načervenalé
zbarvení	zbarvení	k1gNnSc2	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnPc1d1	horní
končetiny	končetina	k1gFnPc1	končetina
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
dvojnásobné	dvojnásobný	k2eAgFnPc4d1	dvojnásobná
délky	délka	k1gFnPc4	délka
trupu	trup	k1gInSc2	trup
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgInPc1d1	dolní
jsou	být	k5eAaImIp3nP	být
krátké	krátká	k1gFnPc1	krátká
a	a	k8xC	a
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
jen	jen	k9	jen
o	o	k7c4	o
trochu	trocha	k1gFnSc4	trocha
větší	veliký	k2eAgFnSc2d2	veliký
délky	délka	k1gFnSc2	délka
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
horních	horní	k2eAgInPc2d1	horní
<g/>
.	.	kIx.	.
</s>
<s>
Obličej	obličej	k1gInSc1	obličej
není	být	k5eNaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
srstí	srst	k1gFnSc7	srst
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
však	však	k9	však
objevit	objevit	k5eAaPmF	objevit
knír	knír	k1gInSc4	knír
<g/>
.	.	kIx.	.
</s>
<s>
Nos	nos	k1gInSc1	nos
i	i	k8xC	i
uši	ucho	k1gNnPc1	ucho
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgFnPc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Lebka	lebka	k1gFnSc1	lebka
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
s	s	k7c7	s
redukovanými	redukovaný	k2eAgFnPc7d1	redukovaná
nosními	nosní	k2eAgFnPc7d1	nosní
kostmi	kost	k1gFnPc7	kost
a	a	k8xC	a
krátkou	krátký	k2eAgFnSc7d1	krátká
dolní	dolní	k2eAgFnSc7d1	dolní
čelistí	čelist	k1gFnSc7	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
znakem	znak	k1gInSc7	znak
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgInPc1d1	velký
vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
řezáky	řezák	k1gInPc1	řezák
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
mozkovny	mozkovna	k1gFnSc2	mozkovna
činí	činit	k5eAaImIp3nS	činit
průměrně	průměrně	k6eAd1	průměrně
424	[number]	k4	424
cm	cm	kA	cm
<g/>
3	[number]	k4	3
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
a	a	k8xC	a
366	[number]	k4	366
cm	cm	kA	cm
<g/>
3	[number]	k4	3
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Krční	krční	k2eAgFnSc1d1	krční
páteř	páteř	k1gFnSc1	páteř
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
sedmi	sedm	k4xCc7	sedm
obratli	obratel	k1gInPc7	obratel
<g/>
,	,	kIx,	,
hrudních	hrudní	k2eAgInPc2d1	hrudní
obratlů	obratel	k1gInPc2	obratel
je	být	k5eAaImIp3nS	být
dvanáct	dvanáct	k4xCc1	dvanáct
<g/>
,	,	kIx,	,
bederní	bederní	k2eAgMnPc1d1	bederní
čtyři	čtyři	k4xCgMnPc1	čtyři
<g/>
,	,	kIx,	,
křížové	křížový	k2eAgInPc4d1	křížový
čtyři	čtyři	k4xCgInPc4	čtyři
až	až	k6eAd1	až
pět	pět	k4xCc4	pět
a	a	k8xC	a
kostrční	kostrční	k2eAgFnSc1d1	kostrční
dva	dva	k4xCgInPc4	dva
až	až	k9	až
tři	tři	k4xCgInPc4	tři
<g/>
.	.	kIx.	.
<g/>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
tvářích	tvář	k1gFnPc6	tvář
velké	velký	k2eAgInPc4d1	velký
lícní	lícní	k2eAgInPc4d1	lícní
valy	val	k1gInPc4	val
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
imponují	imponovat	k5eAaImIp3nP	imponovat
jiným	jiný	k2eAgInPc3d1	jiný
samcům	samec	k1gInPc3	samec
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
převážně	převážně	k6eAd1	převážně
tukovou	tukový	k2eAgFnSc7d1	tuková
tkání	tkáň	k1gFnSc7	tkáň
a	a	k8xC	a
podporují	podporovat	k5eAaImIp3nP	podporovat
svalstvo	svalstvo	k1gNnSc4	svalstvo
obličeje	obličej	k1gInSc2	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
vybaveni	vybavit	k5eAaPmNgMnP	vybavit
hrdelním	hrdelní	k2eAgInSc7d1	hrdelní
vakem	vak	k1gInSc7	vak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vydávat	vydávat	k5eAaImF	vydávat
hlasité	hlasitý	k2eAgNnSc1d1	hlasité
volání	volání	k1gNnSc1	volání
<g/>
.	.	kIx.	.
</s>
<s>
Orangutan	orangutan	k1gMnSc1	orangutan
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
savcem	savec	k1gMnSc7	savec
žijícím	žijící	k2eAgFnPc3d1	žijící
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
orangutanů	orangutan	k1gMnPc2	orangutan
obvykle	obvykle	k6eAd1	obvykle
měří	měřit	k5eAaImIp3nS	měřit
na	na	k7c4	na
výšku	výška	k1gFnSc4	výška
okolo	okolo	k7c2	okolo
115	[number]	k4	115
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
asi	asi	k9	asi
37	[number]	k4	37
kg	kg	kA	kg
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
mezi	mezi	k7c7	mezi
30	[number]	k4	30
a	a	k8xC	a
50	[number]	k4	50
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
velikosti	velikost	k1gFnSc3	velikost
asi	asi	k9	asi
136	[number]	k4	136
cm	cm	kA	cm
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
přibližně	přibližně	k6eAd1	přibližně
75	[number]	k4	75
kg	kg	kA	kg
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
mezi	mezi	k7c7	mezi
50	[number]	k4	50
a	a	k8xC	a
90	[number]	k4	90
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozpětí	rozpětí	k1gNnSc1	rozpětí
paží	paže	k1gFnPc2	paže
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
délky	délka	k1gFnPc4	délka
až	až	k9	až
dvou	dva	k4xCgInPc2	dva
metrů	metr	k1gInPc2	metr
<g/>
;	;	kIx,	;
u	u	k7c2	u
orangutanů	orangutan	k1gMnPc2	orangutan
je	být	k5eAaImIp3nS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
výrazný	výrazný	k2eAgInSc1d1	výrazný
pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
<g/>
.	.	kIx.	.
</s>
<s>
Orangutan	orangutan	k1gMnSc1	orangutan
sumaterský	sumaterský	k2eAgMnSc1d1	sumaterský
a	a	k8xC	a
bornejský	bornejský	k2eAgMnSc1d1	bornejský
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
například	například	k6eAd1	například
světlejší	světlý	k2eAgFnSc4d2	světlejší
<g/>
,	,	kIx,	,
delší	dlouhý	k2eAgFnSc4d2	delší
a	a	k8xC	a
více	hodně	k6eAd2	hodně
zvlněnou	zvlněný	k2eAgFnSc7d1	zvlněná
srstí	srst	k1gFnSc7	srst
u	u	k7c2	u
orangutana	orangutan	k1gMnSc2	orangutan
sumaterského	sumaterský	k2eAgInSc2d1	sumaterský
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
případě	případ	k1gInSc6	případ
samců	samec	k1gInPc2	samec
menší	malý	k2eAgInSc1d2	menší
hrdelní	hrdelní	k2eAgInSc1d1	hrdelní
vak	vak	k1gInSc1	vak
než	než	k8xS	než
orangutan	orangutan	k1gMnSc1	orangutan
bornejský	bornejský	k2eAgMnSc1d1	bornejský
<g/>
.	.	kIx.	.
</s>
<s>
Orangutan	orangutan	k1gMnSc1	orangutan
tapanulijský	tapanulijský	k2eAgMnSc1d1	tapanulijský
se	se	k3xPyFc4	se
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
dvou	dva	k4xCgInPc2	dva
druhů	druh	k1gInPc2	druh
rovněž	rovněž	k9	rovněž
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
zbarvením	zbarvení	k1gNnSc7	zbarvení
a	a	k8xC	a
délkou	délka	k1gFnSc7	délka
srsti	srst	k1gFnSc2	srst
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgFnSc1d2	menší
lebku	lebka	k1gFnSc4	lebka
a	a	k8xC	a
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
stavbu	stavba	k1gFnSc4	stavba
chrupu	chrup	k1gInSc2	chrup
<g/>
.	.	kIx.	.
<g/>
Ruka	ruka	k1gFnSc1	ruka
orangutana	orangutan	k1gMnSc2	orangutan
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
ruce	ruka	k1gFnPc1	ruka
lidské	lidský	k2eAgFnPc4d1	lidská
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
čtyřmi	čtyři	k4xCgInPc7	čtyři
prsty	prst	k1gInPc7	prst
a	a	k8xC	a
protistojným	protistojný	k2eAgInSc7d1	protistojný
palcem	palec	k1gInSc7	palec
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
kloubů	kloub	k1gInPc2	kloub
a	a	k8xC	a
šlach	šlacha	k1gFnPc2	šlacha
však	však	k9	však
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
adaptace	adaptace	k1gFnPc4	adaptace
významné	významný	k2eAgFnPc4d1	významná
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Prsty	prst	k1gInPc1	prst
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
zakřivené	zakřivený	k2eAgFnSc2d1	zakřivená
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
háku	hák	k1gInSc2	hák
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
uchopení	uchopení	k1gNnSc4	uchopení
větve	větev	k1gFnSc2	větev
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
orangutani	orangutan	k1gMnPc1	orangutan
dokáží	dokázat	k5eAaPmIp3nP	dokázat
pevně	pevně	k6eAd1	pevně
chytit	chytit	k5eAaPmF	chytit
objektů	objekt	k1gInPc2	objekt
i	i	k9	i
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
palců	palec	k1gInPc2	palec
<g/>
.	.	kIx.	.
</s>
<s>
Orangutani	orangutan	k1gMnPc1	orangutan
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
úchopu	úchop	k1gInSc3	úchop
nejen	nejen	k6eAd1	nejen
ruce	ruka	k1gFnPc4	ruka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
opatřeny	opatřen	k2eAgInPc1d1	opatřen
čtyřmi	čtyři	k4xCgInPc7	čtyři
prsty	prst	k1gInPc7	prst
a	a	k8xC	a
protistojným	protistojný	k2eAgInSc7d1	protistojný
palcem	palec	k1gInSc7	palec
<g/>
.	.	kIx.	.
</s>
<s>
Kyčelní	kyčelní	k2eAgInPc1d1	kyčelní
klouby	kloub	k1gInPc1	kloub
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
flexibilitu	flexibilita	k1gFnSc4	flexibilita
jako	jako	k8xC	jako
klouby	kloub	k1gInPc1	kloub
ramenní	ramenní	k2eAgInPc1d1	ramenní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
šimpanzů	šimpanz	k1gMnPc2	šimpanz
nebo	nebo	k8xC	nebo
goril	gorila	k1gFnPc2	gorila
chodí	chodit	k5eAaImIp3nP	chodit
orangutani	orangutan	k1gMnPc1	orangutan
na	na	k7c6	na
pěstech	pěst	k1gFnPc6	pěst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Orangutani	orangutan	k1gMnPc1	orangutan
většinu	většina	k1gFnSc4	většina
dne	den	k1gInSc2	den
tráví	trávit	k5eAaImIp3nP	trávit
získáváním	získávání	k1gNnSc7	získávání
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
odpočinkem	odpočinek	k1gInSc7	odpočinek
a	a	k8xC	a
přemisťováním	přemisťování	k1gNnSc7	přemisťování
se	se	k3xPyFc4	se
na	na	k7c4	na
jiná	jiný	k2eAgNnPc4d1	jiné
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc4	den
začínají	začínat	k5eAaImIp3nP	začínat
krmením	krmení	k1gNnPc3	krmení
po	po	k7c6	po
dobu	doba	k1gFnSc4	doba
dvou	dva	k4xCgMnPc6	dva
až	až	k9	až
tří	tři	k4xCgFnPc2	tři
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
poledne	poledne	k1gNnSc2	poledne
odpočívají	odpočívat	k5eAaImIp3nP	odpočívat
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozdních	pozdní	k2eAgFnPc6d1	pozdní
odpoledních	odpolední	k2eAgFnPc6d1	odpolední
hodinách	hodina	k1gFnPc6	hodina
cestují	cestovat	k5eAaImIp3nP	cestovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
večera	večer	k1gInSc2	večer
si	se	k3xPyFc3	se
staví	stavit	k5eAaBmIp3nS	stavit
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
hnízda	hnízdo	k1gNnSc2	hnízdo
na	na	k7c4	na
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
konstrukce	konstrukce	k1gFnPc4	konstrukce
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hnízda	hnízdo	k1gNnSc2	hnízdo
na	na	k7c4	na
noc	noc	k1gFnSc4	noc
se	se	k3xPyFc4	se
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
od	od	k7c2	od
hnízd	hnízdo	k1gNnPc2	hnízdo
sloužících	sloužící	k2eAgNnPc2d1	sloužící
k	k	k7c3	k
dennímu	denní	k2eAgInSc3d1	denní
odpočinku	odpočinek	k1gInSc3	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
postavením	postavení	k1gNnSc7	postavení
hnízda	hnízdo	k1gNnSc2	hnízdo
si	se	k3xPyFc3	se
orangutani	orangutan	k1gMnPc1	orangutan
vyberou	vybrat	k5eAaPmIp3nP	vybrat
vhodný	vhodný	k2eAgInSc4d1	vhodný
strom	strom	k1gInSc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
hnízda	hnízdo	k1gNnSc2	hnízdo
je	být	k5eAaImIp3nS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
spojením	spojení	k1gNnSc7	spojení
větví	větev	k1gFnPc2	větev
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
orangutani	orangutan	k1gMnPc1	orangutan
do	do	k7c2	do
hnízda	hnízdo	k1gNnSc2	hnízdo
donesou	donést	k5eAaPmIp3nP	donést
menší	malý	k2eAgFnPc1d2	menší
olistěné	olistěný	k2eAgFnPc1d1	olistěná
větve	větev	k1gFnPc1	větev
sloužící	sloužící	k1gFnSc2	sloužící
jako	jako	k8xS	jako
matrace	matrace	k1gFnSc2	matrace
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
větve	větev	k1gFnPc1	větev
následně	následně	k6eAd1	následně
spletou	splést	k5eAaPmIp3nP	splést
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
zvýší	zvýšit	k5eAaPmIp3nP	zvýšit
stabilitu	stabilita	k1gFnSc4	stabilita
hnízda	hnízdo	k1gNnSc2	hnízdo
a	a	k8xC	a
tak	tak	k6eAd1	tak
ho	on	k3xPp3gMnSc4	on
dokončí	dokončit	k5eAaPmIp3nS	dokončit
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
ještě	ještě	k6eAd1	ještě
doplnit	doplnit	k5eAaPmF	doplnit
o	o	k7c4	o
"	"	kIx"	"
<g/>
polštáře	polštář	k1gInPc4	polštář
<g/>
"	"	kIx"	"
či	či	k8xC	či
střechu	střecha	k1gFnSc4	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Techniku	technika	k1gFnSc4	technika
vytváření	vytváření	k1gNnSc2	vytváření
hnízda	hnízdo	k1gNnSc2	hnízdo
se	se	k3xPyFc4	se
orangutani	orangutan	k1gMnPc1	orangutan
učí	učit	k5eAaImIp3nP	učit
od	od	k7c2	od
svých	svůj	k3xOyFgFnPc2	svůj
matek	matka	k1gFnPc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
začínají	začínat	k5eAaImIp3nP	začínat
s	s	k7c7	s
prvními	první	k4xOgInPc7	první
pokusy	pokus	k1gInPc7	pokus
o	o	k7c4	o
stavbu	stavba	k1gFnSc4	stavba
hnízda	hnízdo	k1gNnSc2	hnízdo
již	již	k6eAd1	již
v	v	k7c6	v
šesti	šest	k4xCc6	šest
měsících	měsíc	k1gInPc6	měsíc
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
by	by	kYmCp3nS	by
již	již	k9	již
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
postup	postup	k1gInSc4	postup
plně	plně	k6eAd1	plně
zvládnutý	zvládnutý	k2eAgMnSc1d1	zvládnutý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hierarchie	hierarchie	k1gFnSc2	hierarchie
===	===	k?	===
</s>
</p>
<p>
<s>
Orangutani	orangutan	k1gMnPc1	orangutan
žijí	žít	k5eAaImIp3nP	žít
více	hodně	k6eAd2	hodně
samotářský	samotářský	k2eAgInSc4d1	samotářský
život	život	k1gInSc4	život
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc1d1	ostatní
velcí	velký	k2eAgMnPc1d1	velký
lidoopi	lidoop	k1gMnPc1	lidoop
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
sociálních	sociální	k2eAgFnPc2d1	sociální
vazeb	vazba	k1gFnPc2	vazba
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
samicemi	samice	k1gFnPc7	samice
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc7	jejich
potomky	potomek	k1gMnPc7	potomek
<g/>
,	,	kIx,	,
dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
jsou	být	k5eAaImIp3nP	být
solitéry	solitéra	k1gFnPc4	solitéra
<g/>
.	.	kIx.	.
</s>
<s>
Orangutani	orangutan	k1gMnPc1	orangutan
mohou	moct	k5eAaImIp3nP	moct
trvale	trvale	k6eAd1	trvale
obývat	obývat	k5eAaImF	obývat
jedno	jeden	k4xCgNnSc4	jeden
území	území	k1gNnSc4	území
(	(	kIx(	(
<g/>
rezidentní	rezidentní	k2eAgFnSc1d1	rezidentní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
samice	samice	k1gFnSc1	samice
s	s	k7c7	s
potomky	potomek	k1gMnPc7	potomek
obývá	obývat	k5eAaImIp3nS	obývat
jedno	jeden	k4xCgNnSc4	jeden
teritorium	teritorium	k1gNnSc4	teritorium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
překrývá	překrývat	k5eAaImIp3nS	překrývat
s	s	k7c7	s
územími	území	k1gNnPc7	území
dalších	další	k2eAgFnPc2d1	další
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
teritorií	teritorium	k1gNnPc2	teritorium
samic	samice	k1gFnPc2	samice
pak	pak	k6eAd1	pak
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
teritoriu	teritorium	k1gNnSc6	teritorium
samce	samec	k1gMnSc4	samec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
páří	pářit	k5eAaImIp3nS	pářit
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
těchto	tento	k3xDgInPc2	tento
samců	samec	k1gInPc2	samec
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
překrývat	překrývat	k5eAaImF	překrývat
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
k	k	k7c3	k
sobě	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
chovat	chovat	k5eAaImF	chovat
nepřátelsky	přátelsky	k6eNd1	přátelsky
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
dominují	dominovat	k5eAaImIp3nP	dominovat
nedospělým	nedospělý	k1gMnPc3	nedospělý
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
orangutani	orangutan	k1gMnPc1	orangutan
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
volně	volně	k6eAd1	volně
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
bez	bez	k7c2	bez
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
cestují	cestovat	k5eAaImIp3nP	cestovat
obvykle	obvykle	k6eAd1	obvykle
sami	sám	k3xTgMnPc1	sám
<g/>
,	,	kIx,	,
nedospělí	dospělý	k2eNgMnPc1d1	nedospělý
samci	samec	k1gMnPc1	samec
tvoří	tvořit	k5eAaImIp3nP	tvořit
malé	malý	k2eAgFnPc4d1	malá
skupinky	skupinka	k1gFnPc4	skupinka
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
matky	matka	k1gFnSc2	matka
se	se	k3xPyFc4	se
samice	samice	k1gFnPc1	samice
většinou	většinou	k6eAd1	většinou
usadí	usadit	k5eAaPmIp3nP	usadit
v	v	k7c6	v
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
překrývá	překrývat	k5eAaImIp3nS	překrývat
s	s	k7c7	s
teritoriem	teritorium	k1gNnSc7	teritorium
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
však	však	k9	však
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
vztahy	vztah	k1gInPc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
se	se	k3xPyFc4	se
od	od	k7c2	od
matky	matka	k1gFnSc2	matka
rozptylují	rozptylovat	k5eAaImIp3nP	rozptylovat
dále	daleko	k6eAd2	daleko
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
nabýt	nabýt	k5eAaPmF	nabýt
vlastní	vlastní	k2eAgNnSc4d1	vlastní
území	území	k1gNnSc4	území
vyhnáním	vyhnání	k1gNnSc7	vyhnání
nějakého	nějaký	k3yIgMnSc2	nějaký
dominantního	dominantní	k2eAgMnSc2d1	dominantní
samce	samec	k1gMnSc2	samec
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
.	.	kIx.	.
<g/>
Orangutani	orangutan	k1gMnPc1	orangutan
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
shlukovat	shlukovat	k5eAaImF	shlukovat
na	na	k7c6	na
velkých	velký	k2eAgInPc6d1	velký
stromech	strom	k1gInPc6	strom
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
plodů	plod	k1gInPc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Lidoopi	lidoop	k1gMnPc1	lidoop
zde	zde	k6eAd1	zde
nemusí	muset	k5eNaImIp3nP	muset
soupeřit	soupeřit	k5eAaImF	soupeřit
o	o	k7c4	o
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
tedy	tedy	k8xC	tedy
věnovat	věnovat	k5eAaPmF	věnovat
sociálním	sociální	k2eAgFnPc3d1	sociální
vazbám	vazba	k1gFnPc3	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
skupinky	skupinka	k1gFnPc1	skupinka
přesunující	přesunující	k2eAgFnPc1d1	přesunující
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
zdroji	zdroj	k1gInPc7	zdroj
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
složeny	složit	k5eAaPmNgInP	složit
z	z	k7c2	z
několika	několik	k4yIc2	několik
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
samice	samice	k1gFnPc4	samice
se	s	k7c7	s
samcem	samec	k1gMnSc7	samec
žijící	žijící	k2eAgFnSc1d1	žijící
v	v	k7c6	v
partnerském	partnerský	k2eAgInSc6d1	partnerský
svazku	svazek	k1gInSc6	svazek
<g/>
.	.	kIx.	.
<g/>
Orangutani	orangutan	k1gMnPc1	orangutan
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
pomocí	pomocí	k7c2	pomocí
různých	různý	k2eAgInPc2d1	různý
zvuků	zvuk	k1gInPc2	zvuk
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
nejméně	málo	k6eAd3	málo
třicet	třicet	k4xCc1	třicet
dva	dva	k4xCgInPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
vydávají	vydávat	k5eAaImIp3nP	vydávat
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
volání	volání	k1gNnPc4	volání
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
lákají	lákat	k5eAaImIp3nP	lákat
samice	samice	k1gFnSc1	samice
a	a	k8xC	a
imponují	imponovat	k5eAaImIp3nP	imponovat
dalším	další	k2eAgMnPc3d1	další
samcům	samec	k1gMnPc3	samec
<g/>
.	.	kIx.	.
</s>
<s>
Jedinci	jedinec	k1gMnPc1	jedinec
obou	dva	k4xCgNnPc2	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
pak	pak	k6eAd1	pak
zastrašují	zastrašovat	k5eAaImIp3nP	zastrašovat
případného	případný	k2eAgMnSc4d1	případný
soka	sok	k1gMnSc4	sok
nízkými	nízký	k2eAgInPc7d1	nízký
hrdelními	hrdelní	k2eAgInPc7d1	hrdelní
zvuky	zvuk	k1gInPc7	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
neklidu	neklid	k1gInSc2	neklid
orangutani	orangutan	k1gMnPc1	orangutan
nasávají	nasávat	k5eAaImIp3nP	nasávat
vzduch	vzduch	k1gInSc4	vzduch
přes	přes	k7c4	přes
sevřené	sevřený	k2eAgInPc4d1	sevřený
rty	ret	k1gInPc4	ret
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
produkuje	produkovat	k5eAaImIp3nS	produkovat
zvuk	zvuk	k1gInSc4	zvuk
připomínající	připomínající	k2eAgInSc4d1	připomínající
polibek	polibek	k1gInSc4	polibek
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
v	v	k7c6	v
případě	případ	k1gInSc6	případ
rozrušení	rozrušení	k1gNnSc2	rozrušení
vydávají	vydávat	k5eAaImIp3nP	vydávat
měkké	měkký	k2eAgInPc1d1	měkký
zvonivé	zvonivý	k2eAgInPc1d1	zvonivý
zvuky	zvuk	k1gInPc1	zvuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
===	===	k?	===
</s>
</p>
<p>
<s>
Jídelníček	jídelníček	k1gInSc1	jídelníček
orangutana	orangutan	k1gMnSc2	orangutan
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
měsíci	měsíc	k1gInPc7	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Ovoce	ovoce	k1gNnSc1	ovoce
tvoří	tvořit	k5eAaImIp3nS	tvořit
65	[number]	k4	65
až	až	k9	až
90	[number]	k4	90
%	%	kIx~	%
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
orangutan	orangutan	k1gMnSc1	orangutan
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
například	například	k6eAd1	například
fíky	fík	k1gInPc4	fík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nížinných	nížinný	k2eAgFnPc6d1	nížinná
oblastech	oblast	k1gFnPc6	oblast
preferuje	preferovat	k5eAaImIp3nS	preferovat
lesy	les	k1gInPc1	les
složené	složený	k2eAgInPc1d1	složený
ze	z	k7c2	z
stromů	strom	k1gInPc2	strom
patřících	patřící	k2eAgInPc2d1	patřící
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
dvojkřídláčovitých	dvojkřídláčovití	k1gMnPc2	dvojkřídláčovití
(	(	kIx(	(
<g/>
Dipterocarpaceae	Dipterocarpaceae	k1gNnSc1	Dipterocarpaceae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
plodů	plod	k1gInPc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
orangutana	orangutan	k1gMnSc2	orangutan
bornejského	bornejský	k2eAgMnSc2d1	bornejský
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
minimálně	minimálně	k6eAd1	minimálně
317	[number]	k4	317
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
potravy	potrava	k1gFnSc2	potrava
včetně	včetně	k7c2	včetně
mladých	mladý	k2eAgInPc2d1	mladý
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
medu	med	k1gInSc2	med
či	či	k8xC	či
ptačích	ptačí	k2eAgNnPc2d1	ptačí
holátek	holátko	k1gNnPc2	holátko
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
období	období	k1gNnSc2	období
zralosti	zralost	k1gFnSc2	zralost
velkých	velký	k2eAgInPc2d1	velký
plodů	plod	k1gInPc2	plod
orangutani	orangutan	k1gMnPc1	orangutan
získávají	získávat	k5eAaImIp3nP	získávat
z	z	k7c2	z
potravy	potrava	k1gFnSc2	potrava
až	až	k9	až
11	[number]	k4	11
000	[number]	k4	000
kalorií	kalorie	k1gFnPc2	kalorie
za	za	k7c4	za
den	den	k1gInSc4	den
a	a	k8xC	a
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
samice	samice	k1gFnSc2	samice
také	také	k9	také
rodí	rodit	k5eAaImIp3nP	rodit
<g/>
.	.	kIx.	.
<g/>
Orangutani	orangutan	k1gMnPc1	orangutan
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c2	za
jediného	jediný	k2eAgMnSc2d1	jediný
rozptylovače	rozptylovač	k1gMnSc2	rozptylovač
semen	semeno	k1gNnPc2	semeno
některých	některý	k3yIgMnPc2	některý
druhů	druh	k1gMnPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
včetně	včetně	k7c2	včetně
jedovaté	jedovatý	k2eAgFnSc2d1	jedovatá
rostliny	rostlina	k1gFnSc2	rostlina
Strychnos	Strychnos	k1gInSc1	Strychnos
ignatii	ignatie	k1gFnSc4	ignatie
<g/>
,	,	kIx,	,
produkující	produkující	k2eAgInSc4d1	produkující
strychnin	strychnin	k1gInSc4	strychnin
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
alkaloid	alkaloid	k1gInSc1	alkaloid
nemá	mít	k5eNaImIp3nS	mít
mimo	mimo	k7c4	mimo
nadměrné	nadměrný	k2eAgMnPc4d1	nadměrný
tvorby	tvorba	k1gFnSc2	tvorba
slin	slina	k1gFnPc2	slina
na	na	k7c4	na
tyto	tento	k3xDgMnPc4	tento
lidoopy	lidoop	k1gMnPc4	lidoop
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
<g/>
U	u	k7c2	u
orangutanů	orangutan	k1gMnPc2	orangutan
byla	být	k5eAaImAgFnS	být
zjištěna	zjistit	k5eAaPmNgFnS	zjistit
také	také	k9	také
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
geofagie	geofagie	k1gFnSc1	geofagie
<g/>
,	,	kIx,	,
konzumace	konzumace	k1gFnSc1	konzumace
půdy	půda	k1gFnSc2	půda
nebo	nebo	k8xC	nebo
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
chování	chování	k1gNnSc4	chování
existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc1	tři
důvody	důvod	k1gInPc1	důvod
<g/>
:	:	kIx,	:
doplnění	doplnění	k1gNnSc1	doplnění
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
zkonzumovaný	zkonzumovaný	k2eAgInSc1d1	zkonzumovaný
jíl	jíl	k1gInSc1	jíl
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
některé	některý	k3yIgFnPc4	některý
toxické	toxický	k2eAgFnPc4d1	toxická
látky	látka	k1gFnPc4	látka
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
léčit	léčit	k5eAaImF	léčit
střevní	střevní	k2eAgFnSc2d1	střevní
poruchy	porucha	k1gFnSc2	porucha
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
průjem	průjem	k1gInSc1	průjem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnPc1	rozmnožování
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
===	===	k?	===
</s>
</p>
<p>
<s>
Samci	Samek	k1gMnPc1	Samek
orangutanů	orangutan	k1gMnPc2	orangutan
dospívají	dospívat	k5eAaImIp3nP	dospívat
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
mezi	mezi	k7c7	mezi
osmi	osm	k4xCc7	osm
až	až	k9	až
patnácti	patnáct	k4xCc7	patnáct
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
jim	on	k3xPp3gMnPc3	on
plně	plně	k6eAd1	plně
sestupují	sestupovat	k5eAaImIp3nP	sestupovat
varlata	varle	k1gNnPc1	varle
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
se	se	k3xPyFc4	se
pářit	pářit	k5eAaImF	pářit
<g/>
.	.	kIx.	.
</s>
<s>
Hrdelní	hrdelní	k2eAgInSc1d1	hrdelní
vak	vak	k1gInSc1	vak
<g/>
,	,	kIx,	,
lícní	lícní	k2eAgInPc1d1	lícní
valy	val	k1gInPc1	val
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
srst	srst	k1gFnSc1	srst
a	a	k8xC	a
schopnost	schopnost	k1gFnSc1	schopnost
hlasitého	hlasitý	k2eAgNnSc2d1	hlasité
volání	volání	k1gNnSc2	volání
(	(	kIx(	(
<g/>
sekundární	sekundární	k2eAgInPc1d1	sekundární
pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
znaky	znak	k1gInPc1	znak
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
vyvinou	vyvinout	k5eAaPmIp3nP	vyvinout
mezi	mezi	k7c7	mezi
15	[number]	k4	15
<g/>
.	.	kIx.	.
až	až	k9	až
20	[number]	k4	20
<g/>
.	.	kIx.	.
rokem	rok	k1gInSc7	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
těchto	tento	k3xDgFnPc2	tento
charakteristik	charakteristika	k1gFnPc2	charakteristika
závisí	záviset	k5eAaImIp3nS	záviset
ve	v	k7c6	v
značné	značný	k2eAgFnSc6d1	značná
míře	míra	k1gFnSc6	míra
na	na	k7c6	na
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
rezidentního	rezidentní	k2eAgInSc2d1	rezidentní
samce	samec	k1gInSc2	samec
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gNnSc4	jeho
volání	volání	k1gNnSc4	volání
vývin	vývin	k1gInSc4	vývin
těchto	tento	k3xDgInPc2	tento
znaků	znak	k1gInPc2	znak
potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
<g/>
.	.	kIx.	.
<g/>
Samci	Samek	k1gMnPc1	Samek
bez	bez	k7c2	bez
výše	výše	k1gFnSc2	výše
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
charakteristik	charakteristika	k1gFnPc2	charakteristika
a	a	k8xC	a
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
charakteristikami	charakteristika	k1gFnPc7	charakteristika
mají	mít	k5eAaImIp3nP	mít
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
strategii	strategie	k1gFnSc4	strategie
páření	páření	k1gNnSc2	páření
<g/>
.	.	kIx.	.
</s>
<s>
Plně	plně	k6eAd1	plně
vyvinutí	vyvinutý	k2eAgMnPc1d1	vyvinutý
samci	samec	k1gMnPc1	samec
lákají	lákat	k5eAaImIp3nP	lákat
samice	samice	k1gFnSc1	samice
charakteristickým	charakteristický	k2eAgNnSc7d1	charakteristické
dlouhým	dlouhý	k2eAgNnSc7d1	dlouhé
voláním	volání	k1gNnSc7	volání
<g/>
.	.	kIx.	.
</s>
<s>
Nevyvinutí	vyvinutý	k2eNgMnPc1d1	nevyvinutý
samci	samec	k1gMnPc1	samec
většinou	většina	k1gFnSc7	většina
samice	samice	k1gFnSc2	samice
aktivně	aktivně	k6eAd1	aktivně
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
a	a	k8xC	a
poté	poté	k6eAd1	poté
je	on	k3xPp3gInPc4	on
nutí	nutit	k5eAaImIp3nS	nutit
k	k	k7c3	k
reprodukci	reprodukce	k1gFnSc3	reprodukce
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
pářící	pářící	k2eAgFnPc1d1	pářící
strategie	strategie	k1gFnPc1	strategie
jsou	být	k5eAaImIp3nP	být
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
ale	ale	k8xC	ale
preferují	preferovat	k5eAaImIp3nP	preferovat
plně	plně	k6eAd1	plně
vyvinuté	vyvinutý	k2eAgMnPc4d1	vyvinutý
samce	samec	k1gMnPc4	samec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
je	on	k3xPp3gNnSc4	on
také	také	k9	také
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
útoky	útok	k1gInPc7	útok
nevyvinutých	vyvinutý	k2eNgInPc2d1	nevyvinutý
samců	samec	k1gInPc2	samec
<g/>
.	.	kIx.	.
<g/>
U	u	k7c2	u
samic	samice	k1gFnPc2	samice
orangutanů	orangutan	k1gMnPc2	orangutan
nastává	nastávat	k5eAaImIp3nS	nastávat
první	první	k4xOgFnSc1	první
ovulace	ovulace	k1gFnSc1	ovulace
mezi	mezi	k7c7	mezi
5,8	[number]	k4	5,8
až	až	k8xS	až
11,1	[number]	k4	11,1
lety	let	k1gInPc7	let
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dříve	dříve	k6eAd2	dříve
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
u	u	k7c2	u
těch	ten	k3xDgMnPc2	ten
s	s	k7c7	s
větším	veliký	k2eAgNnSc7d2	veliký
množstvím	množství	k1gNnSc7	množství
tělesného	tělesný	k2eAgInSc2d1	tělesný
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
všech	všecek	k3xTgMnPc2	všecek
velkých	velký	k2eAgMnPc2d1	velký
lidoopů	lidoop	k1gMnPc2	lidoop
jsou	být	k5eAaImIp3nP	být
během	během	k7c2	během
dospívání	dospívání	k1gNnSc2	dospívání
samice	samice	k1gFnSc2	samice
1	[number]	k4	1
až	až	k8xS	až
4	[number]	k4	4
roky	rok	k1gInPc1	rok
neplodné	plodný	k2eNgInPc1d1	neplodný
<g/>
.	.	kIx.	.
</s>
<s>
Menstruační	menstruační	k2eAgInSc1d1	menstruační
cyklus	cyklus	k1gInSc1	cyklus
trvá	trvat	k5eAaImIp3nS	trvat
22	[number]	k4	22
až	až	k9	až
30	[number]	k4	30
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc4	první
potomky	potomek	k1gMnPc4	potomek
samice	samice	k1gFnSc1	samice
rodí	rodit	k5eAaImIp3nS	rodit
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
14	[number]	k4	14
až	až	k9	až
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
březost	březost	k1gFnSc1	březost
trvá	trvat	k5eAaImIp3nS	trvat
233	[number]	k4	233
až	až	k9	až
265	[number]	k4	265
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
narozeními	narození	k1gNnPc7	narození
mláďat	mládě	k1gNnPc2	mládě
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
osmileté	osmiletý	k2eAgInPc1d1	osmiletý
intervaly	interval	k1gInPc1	interval
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgMnPc7	všecek
lidoopy	lidoop	k1gMnPc7	lidoop
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
orangutanů	orangutan	k1gMnPc2	orangutan
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgMnPc2d1	jiný
primátů	primát	k1gMnPc2	primát
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
infanticida	infanticida	k1gFnSc1	infanticida
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
samice	samice	k1gFnSc1	samice
nezačne	začít	k5eNaPmIp3nS	začít
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
mláděte	mládě	k1gNnSc2	mládě
okamžitě	okamžitě	k6eAd1	okamžitě
ovulovat	ovulovat	k5eAaBmF	ovulovat
<g/>
.	.	kIx.	.
<g/>
Samice	samice	k1gFnSc1	samice
nejčastěji	často	k6eAd3	často
rodí	rodit	k5eAaImIp3nS	rodit
jedno	jeden	k4xCgNnSc1	jeden
mládě	mládě	k1gNnSc1	mládě
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
dvojčata	dvojče	k1gNnPc1	dvojče
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jedno	jeden	k4xCgNnSc1	jeden
váží	vážit	k5eAaImIp3nP	vážit
1420	[number]	k4	1420
až	až	k9	až
2040	[number]	k4	2040
g.	g.	k?	g.
Za	za	k7c4	za
život	život	k1gInSc4	život
odchová	odchovat	k5eAaPmIp3nS	odchovat
maximálně	maximálně	k6eAd1	maximálně
čtyři	čtyři	k4xCgMnPc4	čtyři
potomky	potomek	k1gMnPc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
se	se	k3xPyFc4	se
do	do	k7c2	do
výchovy	výchova	k1gFnSc2	výchova
mláďat	mládě	k1gNnPc2	mládě
téměř	téměř	k6eAd1	téměř
nezapojuje	zapojovat	k5eNaImIp3nS	zapojovat
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
péče	péče	k1gFnSc2	péče
a	a	k8xC	a
socializaci	socializace	k1gFnSc4	socializace
provádí	provádět	k5eAaImIp3nS	provádět
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
socializována	socializován	k2eAgFnSc1d1	socializována
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
staršího	starý	k2eAgMnSc4d2	starší
potomka	potomek	k1gMnSc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
dva	dva	k4xCgInPc1	dva
roky	rok	k1gInPc1	rok
jsou	být	k5eAaImIp3nP	být
mláďata	mládě	k1gNnPc4	mládě
na	na	k7c6	na
matce	matka	k1gFnSc6	matka
plně	plně	k6eAd1	plně
závislá	závislý	k2eAgFnSc1d1	závislá
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nosí	nosit	k5eAaImIp3nP	nosit
<g/>
,	,	kIx,	,
krmí	krmit	k5eAaImIp3nP	krmit
a	a	k8xC	a
spí	spát	k5eAaImIp3nP	spát
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
hnízdě	hnízdo	k1gNnSc6	hnízdo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
první	první	k4xOgInPc4	první
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
se	se	k3xPyFc4	se
mládě	mládě	k1gNnSc1	mládě
jen	jen	k9	jen
drží	držet	k5eAaImIp3nS	držet
matčina	matčin	k2eAgNnPc4d1	matčino
břicha	břicho	k1gNnPc4	břicho
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
mláďata	mládě	k1gNnPc1	mládě
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
již	již	k6eAd1	již
netráví	trávit	k5eNaImIp3nS	trávit
tolik	tolik	k6eAd1	tolik
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
<g/>
Asi	asi	k9	asi
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
letech	let	k1gInPc6	let
se	se	k3xPyFc4	se
orangutanům	orangutan	k1gMnPc3	orangutan
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
lezecké	lezecký	k2eAgFnPc1d1	lezecká
dovednosti	dovednost	k1gFnPc1	dovednost
a	a	k8xC	a
putují	putovat	k5eAaImIp3nP	putovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
jedinci	jedinec	k1gMnPc7	jedinec
svého	své	k1gNnSc2	své
druhu	druh	k1gInSc2	druh
držící	držící	k2eAgFnSc2d1	držící
se	se	k3xPyFc4	se
za	za	k7c4	za
ruce	ruka	k1gFnPc4	ruka
po	po	k7c6	po
vrcholcích	vrcholek	k1gInPc6	vrcholek
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
2	[number]	k4	2
až	až	k9	až
5	[number]	k4	5
lety	léto	k1gNnPc7	léto
začnou	začít	k5eAaPmIp3nP	začít
dočasně	dočasně	k6eAd1	dočasně
opouštět	opouštět	k5eAaImF	opouštět
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
asi	asi	k9	asi
ve	v	k7c6	v
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
jsou	být	k5eAaImIp3nP	být
odstavena	odstaven	k2eAgFnSc1d1	odstavena
<g/>
.	.	kIx.	.
</s>
<s>
Orangutani	orangutan	k1gMnPc1	orangutan
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dožít	dožít	k5eAaPmF	dožít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
v	v	k7c6	v
divočině	divočina	k1gFnSc6	divočina
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
věk	věk	k1gInSc1	věk
může	moct	k5eAaImIp3nS	moct
vyšplhat	vyšplhat	k5eAaPmF	vyšplhat
až	až	k9	až
na	na	k7c4	na
45	[number]	k4	45
let	léto	k1gNnPc2	léto
a	a	k8xC	a
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
na	na	k7c4	na
59	[number]	k4	59
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Inteligence	inteligence	k1gFnSc2	inteligence
===	===	k?	===
</s>
</p>
<p>
<s>
Orangutani	orangutan	k1gMnPc1	orangutan
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejinteligentnější	inteligentní	k2eAgInPc4d3	nejinteligentnější
primáty	primát	k1gInPc4	primát
<g/>
.	.	kIx.	.
</s>
<s>
Chápou	chápat	k5eAaImIp3nP	chápat
stálost	stálost	k1gFnSc4	stálost
objektu	objekt	k1gInSc2	objekt
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
že	že	k8xS	že
objekty	objekt	k1gInPc1	objekt
existují	existovat	k5eAaImIp3nP	existovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
nejsou	být	k5eNaImIp3nP	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
studie	studie	k1gFnSc2	studie
prováděné	prováděný	k2eAgFnSc2d1	prováděná
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozmýšlejí	rozmýšlet	k5eAaImIp3nP	rozmýšlet
mezi	mezi	k7c7	mezi
vynaloženým	vynaložený	k2eAgNnSc7d1	vynaložené
úsilím	úsilí	k1gNnSc7	úsilí
a	a	k8xC	a
souvisejícím	související	k2eAgInSc7d1	související
ziskem	zisk	k1gInSc7	zisk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
prvního	první	k4xOgMnSc4	první
tvora	tvor	k1gMnSc4	tvor
mimo	mimo	k7c4	mimo
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgNnSc2	který
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
chování	chování	k1gNnSc1	chování
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
také	také	k9	také
dobří	dobrý	k2eAgMnPc1d1	dobrý
stavitelé	stavitel	k1gMnPc1	stavitel
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
dovedou	dovést	k5eAaPmIp3nP	dovést
postavit	postavit	k5eAaPmF	postavit
hnízdo	hnízdo	k1gNnSc4	hnízdo
během	během	k7c2	během
5	[number]	k4	5
až	až	k9	až
6	[number]	k4	6
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
konstrukci	konstrukce	k1gFnSc6	konstrukce
si	se	k3xPyFc3	se
vybírají	vybírat	k5eAaImIp3nP	vybírat
větve	větev	k1gFnPc4	větev
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
udrží	udržet	k5eAaPmIp3nP	udržet
jejich	jejich	k3xOp3gFnSc4	jejich
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Nástroje	nástroj	k1gInPc1	nástroj
====	====	k?	====
</s>
</p>
<p>
<s>
Důkaz	důkaz	k1gInSc1	důkaz
o	o	k7c6	o
sofistikované	sofistikovaný	k2eAgFnSc6d1	sofistikovaná
výrobě	výroba	k1gFnSc6	výroba
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
použití	použití	k1gNnSc1	použití
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
volné	volný	k2eAgFnSc2d1	volná
přírody	příroda	k1gFnSc2	příroda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Orangutani	orangutan	k1gMnPc1	orangutan
sumaterští	sumaterský	k2eAgMnPc1d1	sumaterský
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Suaq	Suaq	k1gFnPc1	Suaq
Balimbing	Balimbing	k1gInSc4	Balimbing
si	se	k3xPyFc3	se
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
sadu	sada	k1gFnSc4	sada
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
využívali	využívat	k5eAaPmAgMnP	využívat
k	k	k7c3	k
získávání	získávání	k1gNnSc3	získávání
potravy	potrava	k1gFnSc2	potrava
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
nástroje	nástroj	k1gInPc4	nástroj
pro	pro	k7c4	pro
vytahování	vytahování	k1gNnSc4	vytahování
hmyzu	hmyz	k1gInSc2	hmyz
z	z	k7c2	z
dutin	dutina	k1gFnPc2	dutina
stromů	strom	k1gInPc2	strom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
si	se	k3xPyFc3	se
je	on	k3xPp3gFnPc4	on
dovedli	dovést	k5eAaPmAgMnP	dovést
upravit	upravit	k5eAaPmF	upravit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
preferovali	preferovat	k5eAaImAgMnP	preferovat
použití	použití	k1gNnSc2	použití
úchopem	úchop	k1gInSc7	úchop
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
<g/>
.	.	kIx.	.
<g/>
Primatolog	Primatolog	k1gMnSc1	Primatolog
Carel	Carel	k1gMnSc1	Carel
P.	P.	kA	P.
van	van	k1gInSc1	van
Schaik	Schaik	k1gInSc1	Schaik
a	a	k8xC	a
biologická	biologický	k2eAgFnSc1d1	biologická
antropoložka	antropoložka	k1gFnSc1	antropoložka
Cheryl	Cheryl	k1gInSc1	Cheryl
D.	D.	kA	D.
Knottová	Knottová	k1gFnSc1	Knottová
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
rozdíly	rozdíl	k1gInPc4	rozdíl
ve	v	k7c6	v
využívání	využívání	k1gNnSc6	využívání
nástrojů	nástroj	k1gInPc2	nástroj
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
geograficky	geograficky	k6eAd1	geograficky
odlišených	odlišený	k2eAgFnPc6d1	odlišená
populacích	populace	k1gFnPc6	populace
<g/>
.	.	kIx.	.
</s>
<s>
Orangutani	orangutan	k1gMnPc1	orangutan
sumaterští	sumaterský	k2eAgMnPc1d1	sumaterský
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Suaq	Suaq	k1gFnSc2	Suaq
Balimbing	Balimbing	k1gInSc1	Balimbing
používali	používat	k5eAaImAgMnP	používat
nástroje	nástroj	k1gInPc4	nástroj
na	na	k7c4	na
vytahování	vytahování	k1gNnSc4	vytahování
hmyzu	hmyz	k1gInSc2	hmyz
a	a	k8xC	a
semen	semeno	k1gNnPc2	semeno
častěji	často	k6eAd2	často
než	než	k8xS	než
jiné	jiný	k2eAgFnSc2d1	jiná
divoké	divoký	k2eAgFnSc2d1	divoká
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Pozorované	pozorovaný	k2eAgInPc1d1	pozorovaný
rozdíly	rozdíl	k1gInPc1	rozdíl
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
kulturního	kulturní	k2eAgInSc2d1	kulturní
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Lidoopi	lidoop	k1gMnPc1	lidoop
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Suaq	Suaq	k1gFnSc2	Suaq
Balimbing	Balimbing	k1gInSc1	Balimbing
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
skupinách	skupina	k1gFnPc6	skupina
a	a	k8xC	a
navzájem	navzájem	k6eAd1	navzájem
se	se	k3xPyFc4	se
tolerují	tolerovat	k5eAaImIp3nP	tolerovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
dobré	dobrý	k2eAgFnPc4d1	dobrá
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
sociální	sociální	k2eAgInSc4d1	sociální
přenos	přenos	k1gInSc4	přenos
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
důkaz	důkaz	k1gInSc1	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
sociálně	sociálně	k6eAd1	sociálně
žijících	žijící	k2eAgMnPc2d1	žijící
orangutanů	orangutan	k1gMnPc2	orangutan
je	být	k5eAaImIp3nS	být
častěji	často	k6eAd2	často
pozorováno	pozorován	k2eAgNnSc4d1	pozorováno
kulturní	kulturní	k2eAgNnSc4d1	kulturní
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
studie	studie	k1gFnSc2	studie
orangutanů	orangutan	k1gMnPc2	orangutan
dříve	dříve	k6eAd2	dříve
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
vysazeni	vysadit	k5eAaPmNgMnP	vysadit
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Kaja	Kaj	k1gInSc2	Kaj
na	na	k7c6	na
Borneu	Borneo	k1gNnSc6	Borneo
<g/>
.	.	kIx.	.
<g/>
U	u	k7c2	u
orangutanů	orangutan	k1gMnPc2	orangutan
bornejských	bornejský	k2eAgMnPc2d1	bornejský
(	(	kIx(	(
<g/>
poddruh	poddruh	k1gInSc4	poddruh
P.	P.	kA	P.
pygmaeus	pygmaeus	k1gInSc1	pygmaeus
wurmbii	wurmbie	k1gFnSc4	wurmbie
<g/>
)	)	kIx)	)
žijících	žijící	k2eAgInPc2d1	žijící
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Tuanan	Tuanana	k1gFnPc2	Tuanana
bylo	být	k5eAaImAgNnS	být
zase	zase	k9	zase
zjištěno	zjištěn	k2eAgNnSc1d1	zjištěno
používání	používání	k1gNnSc1	používání
nástrojů	nástroj	k1gInPc2	nástroj
napomáhajících	napomáhající	k2eAgInPc2d1	napomáhající
akustické	akustický	k2eAgFnSc3d1	akustická
komunikaci	komunikace	k1gFnSc3	komunikace
<g/>
;	;	kIx,	;
využívají	využívat	k5eAaPmIp3nP	využívat
listy	list	k1gInPc4	list
na	na	k7c4	na
zesílení	zesílení	k1gNnSc4	zesílení
svých	svůj	k3xOyFgInPc2	svůj
zvuků	zvuk	k1gInPc2	zvuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Schopnost	schopnost	k1gFnSc1	schopnost
rozumět	rozumět	k5eAaImF	rozumět
jazyku	jazyk	k1gInSc3	jazyk
====	====	k?	====
</s>
</p>
<p>
<s>
Studie	studie	k1gFnSc1	studie
o	o	k7c4	o
schopnosti	schopnost	k1gFnPc4	schopnost
orangutanů	orangutan	k1gMnPc2	orangutan
chápat	chápat	k5eAaImF	chápat
symboly	symbol	k1gInPc4	symbol
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1973	[number]	k4	1973
až	až	k8xS	až
1975	[number]	k4	1975
zoologem	zoolog	k1gMnSc7	zoolog
Garym	Garym	k1gInSc1	Garym
L.	L.	kA	L.
Shapirem	Shapir	k1gMnSc7	Shapir
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pracoval	pracovat	k5eAaImAgMnS	pracovat
s	s	k7c7	s
orangutaní	orangutaný	k2eAgMnPc1d1	orangutaný
samicí	samice	k1gFnSc7	samice
Aazk	Aazk	k1gInSc1	Aazk
<g/>
.	.	kIx.	.
</s>
<s>
Použita	použit	k2eAgFnSc1d1	použita
byla	být	k5eAaImAgFnS	být
metoda	metoda	k1gFnSc1	metoda
psychologa	psycholog	k1gMnSc2	psycholog
Davida	David	k1gMnSc2	David
Premacka	Premacka	k1gFnSc1	Premacka
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
pro	pro	k7c4	pro
šimpanzí	šimpanzí	k2eAgFnSc4d1	šimpanzí
samici	samice	k1gFnSc4	samice
Sarah	Sarah	k1gFnSc2	Sarah
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
učil	učit	k5eAaImAgMnS	učit
jazykovým	jazykový	k2eAgMnSc7d1	jazykový
dovednostem	dovednost	k1gFnPc3	dovednost
pomocí	pomoc	k1gFnSc7	pomoc
plastových	plastový	k2eAgInPc2d1	plastový
žetonů	žeton	k1gInPc2	žeton
<g/>
.	.	kIx.	.
</s>
<s>
Shapiro	Shapiro	k6eAd1	Shapiro
ve	v	k7c6	v
výzkumech	výzkum	k1gInPc6	výzkum
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
u	u	k7c2	u
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgMnPc2d1	žijící
orangutanů	orangutan	k1gMnPc2	orangutan
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Tanjung	Tanjung	k1gInSc1	Tanjung
Puting	Puting	k1gInSc1	Puting
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Borneo	Borneo	k1gNnSc1	Borneo
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1978	[number]	k4	1978
<g/>
−	−	k?	−
<g/>
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
Shapiro	Shapira	k1gFnSc5	Shapira
učil	učit	k5eAaImAgInS	učit
orangutany	orangutan	k1gMnPc4	orangutan
znakové	znakový	k2eAgFnSc2d1	znaková
řeči	řeč	k1gFnSc2	řeč
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
technik	technika	k1gFnPc2	technika
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
se	se	k3xPyFc4	se
učila	učít	k5eAaPmAgFnS	učít
šimpanzice	šimpanzice	k1gFnSc1	šimpanzice
Washoe	Washoe	k1gFnSc1	Washoe
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
samice	samice	k1gFnSc1	samice
chovaná	chovaný	k2eAgFnSc1d1	chovaná
Shapirem	Shapiro	k1gNnSc7	Shapiro
doma	doma	k6eAd1	doma
se	se	k3xPyFc4	se
naučila	naučit	k5eAaPmAgFnS	naučit
téměř	téměř	k6eAd1	téměř
čtyřicet	čtyřicet	k4xCc4	čtyřicet
znaků	znak	k1gInPc2	znak
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
:	:	kIx,	:
výše	vysoce	k6eAd2	vysoce
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
šimpanzice	šimpanzice	k1gFnSc1	šimpanzice
Washoe	Washoe	k1gFnSc1	Washoe
jich	on	k3xPp3gInPc2	on
ovládala	ovládat	k5eAaImAgFnS	ovládat
kolem	kolem	k7c2	kolem
250	[number]	k4	250
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Predátoři	predátor	k1gMnPc1	predátor
<g/>
,	,	kIx,	,
parazité	parazit	k1gMnPc1	parazit
a	a	k8xC	a
nemoci	nemoc	k1gFnPc1	nemoc
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
přirozené	přirozený	k2eAgMnPc4d1	přirozený
nepřátele	nepřítel	k1gMnPc4	nepřítel
orangutanů	orangutan	k1gMnPc2	orangutan
patří	patřit	k5eAaImIp3nP	patřit
pardáli	pardál	k1gMnPc1	pardál
ostrovní	ostrovní	k2eAgInSc4d1	ostrovní
(	(	kIx(	(
<g/>
Neofelis	Neofelis	k1gInSc4	Neofelis
diardi	diard	k1gMnPc1	diard
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dhoulové	dhoul	k1gMnPc1	dhoul
(	(	kIx(	(
<g/>
Cuon	Cuon	k1gMnSc1	Cuon
alpinus	alpinus	k1gMnSc1	alpinus
<g/>
)	)	kIx)	)
či	či	k8xC	či
krokodýli	krokodýl	k1gMnPc1	krokodýl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Sumatře	Sumatra	k1gFnSc6	Sumatra
je	být	k5eAaImIp3nS	být
úhlavním	úhlavní	k2eAgMnSc7d1	úhlavní
přirozeným	přirozený	k2eAgMnSc7d1	přirozený
nepřítelem	nepřítel	k1gMnSc7	nepřítel
orangutanů	orangutan	k1gMnPc2	orangutan
tygr	tygr	k1gMnSc1	tygr
(	(	kIx(	(
<g/>
Panthera	Panthera	k1gFnSc1	Panthera
tigris	tigris	k1gFnSc1	tigris
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc1	jeho
absence	absence	k1gFnSc1	absence
na	na	k7c6	na
Borneu	Borneo	k1gNnSc6	Borneo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
zde	zde	k6eAd1	zde
orangutani	orangutan	k1gMnPc1	orangutan
žijí	žít	k5eAaImIp3nP	žít
častěji	často	k6eAd2	často
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
než	než	k8xS	než
orangutani	orangutan	k1gMnPc1	orangutan
sumaterští	sumaterský	k2eAgMnPc1d1	sumaterský
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
studie	studie	k1gFnSc2	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
divoce	divoce	k6eAd1	divoce
a	a	k8xC	a
polodivoce	polodivoce	k6eAd1	polodivoce
žijících	žijící	k2eAgMnPc2d1	žijící
orangutanů	orangutan	k1gMnPc2	orangutan
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
z	z	k7c2	z
376	[number]	k4	376
vzorků	vzorek	k1gInPc2	vzorek
výkalů	výkal	k1gInPc2	výkal
celkem	celkem	k6eAd1	celkem
13	[number]	k4	13
různých	různý	k2eAgMnPc2d1	různý
parazitů	parazit	k1gMnPc2	parazit
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
trichomonády	trichomonáda	k1gFnPc4	trichomonáda
náležící	náležící	k2eAgFnPc1d1	náležící
k	k	k7c3	k
druhu	druh	k1gInSc3	druh
Dientamoeba	Dientamoeba	k1gFnSc1	Dientamoeba
fragilis	fragilis	k1gFnSc2	fragilis
či	či	k8xC	či
měňavky	měňavka	k1gFnSc2	měňavka
Entamoeba	Entamoeba	k1gMnSc1	Entamoeba
hartmanni	hartmannit	k5eAaImRp2nS	hartmannit
<g/>
.	.	kIx.	.
</s>
<s>
Parazitární	parazitární	k2eAgFnSc3d1	parazitární
nákaze	nákaza	k1gFnSc3	nákaza
mohou	moct	k5eAaImIp3nP	moct
dopomáhat	dopomáhat	k5eAaImF	dopomáhat
některé	některý	k3yIgInPc4	některý
vnější	vnější	k2eAgInPc4d1	vnější
faktory	faktor	k1gInPc4	faktor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
nedostatek	nedostatek	k1gInSc1	nedostatek
výživného	výživné	k1gNnSc2	výživné
ovoce	ovoce	k1gNnSc2	ovoce
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
objevila	objevit	k5eAaPmAgFnS	objevit
další	další	k2eAgInPc4d1	další
<g/>
,	,	kIx,	,
doposud	doposud	k6eAd1	doposud
vědecky	vědecky	k6eAd1	vědecky
nepopsané	popsaný	k2eNgFnPc1d1	nepopsaná
<g/>
,	,	kIx,	,
druhy	druh	k1gInPc1	druh
parazitů	parazit	k1gMnPc2	parazit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
orangutani	orangutan	k1gMnPc1	orangutan
chovaní	chovaný	k2eAgMnPc1d1	chovaný
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
pro	pro	k7c4	pro
následné	následný	k2eAgNnSc4d1	následné
vypuštění	vypuštění	k1gNnSc4	vypuštění
do	do	k7c2	do
volné	volný	k2eAgFnSc2d1	volná
přírody	příroda	k1gFnSc2	příroda
častěji	často	k6eAd2	často
napadáni	napadat	k5eAaBmNgMnP	napadat
parazity	parazit	k1gMnPc4	parazit
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
poté	poté	k6eAd1	poté
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k8xC	jako
přenašeči	přenašeč	k1gInPc7	přenašeč
na	na	k7c4	na
jedince	jedinec	k1gMnPc4	jedinec
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
malé	malý	k2eAgFnSc3d1	malá
populaci	populace	k1gFnSc3	populace
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
podobná	podobný	k2eAgFnSc1d1	podobná
infekce	infekce	k1gFnSc1	infekce
velký	velký	k2eAgInSc4d1	velký
dopad	dopad	k1gInSc4	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
orangutani	orangutan	k1gMnPc1	orangutan
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
částečně	částečně	k6eAd1	částečně
napadení	napadení	k1gNnSc1	napadení
parazity	parazita	k1gMnSc2	parazita
bránit	bránit	k5eAaImF	bránit
pojídáním	pojídání	k1gNnSc7	pojídání
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
potom	potom	k6eAd1	potom
působí	působit	k5eAaImIp3nP	působit
jako	jako	k9	jako
přírodní	přírodní	k2eAgNnPc4d1	přírodní
antiparazitika	antiparazitikum	k1gNnPc4	antiparazitikum
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
chorobách	choroba	k1gFnPc6	choroba
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
orangutany	orangutan	k1gMnPc4	orangutan
postihují	postihovat	k5eAaImIp3nP	postihovat
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
nebyly	být	k5eNaImAgFnP	být
zjištěny	zjistit	k5eAaPmNgFnP	zjistit
všechny	všechen	k3xTgFnPc1	všechen
potřebné	potřebný	k2eAgFnPc1d1	potřebná
informace	informace	k1gFnPc1	informace
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
nemoci	nemoc	k1gFnPc1	nemoc
respiračního	respirační	k2eAgInSc2d1	respirační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
salmonelóza	salmonelóza	k1gFnSc1	salmonelóza
<g/>
,	,	kIx,	,
malárie	malárie	k1gFnSc1	malárie
či	či	k8xC	či
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Ohrožení	ohrožení	k1gNnSc1	ohrožení
===	===	k?	===
</s>
</p>
<p>
<s>
Orangutan	orangutan	k1gMnSc1	orangutan
bornejský	bornejský	k2eAgMnSc1d1	bornejský
i	i	k8xC	i
sumaterský	sumaterský	k2eAgMnSc1d1	sumaterský
je	být	k5eAaImIp3nS	být
hodnocen	hodnotit	k5eAaImNgInS	hodnotit
Mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
svazem	svaz	k1gInSc7	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
jako	jako	k8xC	jako
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
a	a	k8xC	a
oba	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
zapsány	zapsat	k5eAaPmNgInP	zapsat
na	na	k7c4	na
Úmluvu	úmluva	k1gFnSc4	úmluva
o	o	k7c6	o
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
obchodu	obchod	k1gInSc6	obchod
s	s	k7c7	s
ohroženými	ohrožený	k2eAgInPc7d1	ohrožený
druhy	druh	k1gInPc7	druh
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
příloha	příloha	k1gFnSc1	příloha
I.	I.	kA	I.
<g/>
Primárním	primární	k2eAgNnSc7d1	primární
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
pro	pro	k7c4	pro
orangutany	orangutan	k1gMnPc4	orangutan
je	být	k5eAaImIp3nS	být
ztráta	ztráta	k1gFnSc1	ztráta
jejich	jejich	k3xOp3gNnSc2	jejich
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1973	[number]	k4	1973
až	až	k9	až
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Borneu	Borneo	k1gNnSc6	Borneo
vykáceno	vykácet	k5eAaPmNgNnS	vykácet
hlavně	hlavně	k6eAd1	hlavně
kvůli	kvůli	k7c3	kvůli
těžbě	těžba	k1gFnSc3	těžba
dříví	dříví	k1gNnSc2	dříví
a	a	k8xC	a
přeměně	přeměna	k1gFnSc6	přeměna
na	na	k7c4	na
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
půdu	půda	k1gFnSc4	půda
39	[number]	k4	39
%	%	kIx~	%
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
na	na	k7c6	na
Sumatře	Sumatra	k1gFnSc6	Sumatra
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1985	[number]	k4	1985
až	až	k9	až
2007	[number]	k4	2007
zničeno	zničen	k2eAgNnSc4d1	zničeno
60	[number]	k4	60
%	%	kIx~	%
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
z	z	k7c2	z
88	[number]	k4	88
%	%	kIx~	%
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
ilegální	ilegální	k2eAgNnSc4d1	ilegální
ničení	ničení	k1gNnSc4	ničení
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
kvůli	kvůli	k7c3	kvůli
těžbě	těžba	k1gFnSc3	těžba
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
vytváření	vytváření	k1gNnSc2	vytváření
plantáží	plantáž	k1gFnPc2	plantáž
palmy	palma	k1gFnSc2	palma
olejné	olejný	k2eAgFnSc2d1	olejná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
samotné	samotný	k2eAgNnSc4d1	samotné
kácení	kácení	k1gNnSc4	kácení
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
související	související	k2eAgMnSc1d1	související
úbytek	úbytek	k1gInSc4	úbytek
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
orangutanů	orangutan	k1gMnPc2	orangutan
se	se	k3xPyFc4	se
nabalují	nabalovat	k5eAaImIp3nP	nabalovat
další	další	k2eAgInPc1d1	další
problémy	problém	k1gInPc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Orangutani	orangutan	k1gMnPc1	orangutan
vycházející	vycházející	k2eAgMnPc1d1	vycházející
z	z	k7c2	z
lesů	les	k1gInPc2	les
a	a	k8xC	a
pronikající	pronikající	k2eAgNnSc1d1	pronikající
na	na	k7c4	na
plantáže	plantáž	k1gFnPc4	plantáž
jsou	být	k5eAaImIp3nP	být
vesničany	vesničan	k1gMnPc4	vesničan
zabíjeni	zabíjet	k5eAaImNgMnP	zabíjet
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
jako	jako	k9	jako
zdroj	zdroj	k1gInSc1	zdroj
masa	maso	k1gNnSc2	maso
či	či	k8xC	či
jako	jako	k9	jako
zemědělští	zemědělský	k2eAgMnPc1d1	zemědělský
škůdci	škůdce	k1gMnPc1	škůdce
<g/>
.	.	kIx.	.
</s>
<s>
Vypalování	vypalování	k1gNnSc1	vypalování
lesů	les	k1gInPc2	les
se	se	k3xPyFc4	se
často	často	k6eAd1	často
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
v	v	k7c4	v
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
nekontrolované	kontrolovaný	k2eNgInPc4d1	nekontrolovaný
požáry	požár	k1gInPc4	požár
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yRgInPc6	který
opět	opět	k6eAd1	opět
hyne	hynout	k5eAaImIp3nS	hynout
mnoho	mnoho	k4c1	mnoho
orangutanů	orangutan	k1gMnPc2	orangutan
<g/>
.	.	kIx.	.
</s>
<s>
Ilegální	ilegální	k2eAgNnSc1d1	ilegální
kácení	kácení	k1gNnSc1	kácení
se	se	k3xPyFc4	se
nevyhýbá	vyhýbat	k5eNaImIp3nS	vyhýbat
ani	ani	k8xC	ani
chráněným	chráněný	k2eAgFnPc3d1	chráněná
oblastem	oblast	k1gFnPc3	oblast
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
správy	správa	k1gFnSc2	správa
Programu	program	k1gInSc2	program
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
(	(	kIx(	(
<g/>
UNEP	UNEP	kA	UNEP
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
či	či	k8xC	či
menší	malý	k2eAgFnSc3d2	menší
míře	míra	k1gFnSc3	míra
zasaženo	zasáhnout	k5eAaPmNgNnS	zasáhnout
37	[number]	k4	37
z	z	k7c2	z
41	[number]	k4	41
indonéských	indonéský	k2eAgInPc2d1	indonéský
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
indonéská	indonéský	k2eAgFnSc1d1	Indonéská
vláda	vláda	k1gFnSc1	vláda
desetiletý	desetiletý	k2eAgInSc4d1	desetiletý
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
ilegální	ilegální	k2eAgNnSc4d1	ilegální
odlesňování	odlesňování	k1gNnSc4	odlesňování
omezit	omezit	k5eAaPmF	omezit
a	a	k8xC	a
pokles	pokles	k1gInSc1	pokles
počtů	počet	k1gInPc2	počet
v	v	k7c6	v
orangutaních	orangutaní	k2eAgFnPc6d1	orangutaní
populacích	populace	k1gFnPc6	populace
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
však	však	k9	však
cíle	cíl	k1gInSc2	cíl
nepodařilo	podařit	k5eNaPmAgNnS	podařit
naplnit	naplnit	k5eAaPmF	naplnit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
platí	platit	k5eAaImIp3nS	platit
oficiální	oficiální	k2eAgNnSc4d1	oficiální
moratorium	moratorium	k1gNnSc4	moratorium
na	na	k7c4	na
vydávání	vydávání	k1gNnSc4	vydávání
nových	nový	k2eAgFnPc2d1	nová
těžebních	těžební	k2eAgFnPc2d1	těžební
koncesí	koncese	k1gFnPc2	koncese
<g/>
,	,	kIx,	,
tempo	tempo	k1gNnSc1	tempo
odlesňování	odlesňování	k1gNnPc2	odlesňování
se	se	k3xPyFc4	se
však	však	k9	však
nedaří	dařit	k5eNaImIp3nS	dařit
zpomalit	zpomalit	k5eAaPmF	zpomalit
<g/>
.	.	kIx.	.
</s>
<s>
Pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
vliv	vliv	k1gInSc1	vliv
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
chování	chování	k1gNnSc4	chování
velkých	velký	k2eAgFnPc2d1	velká
nadnárodních	nadnárodní	k2eAgFnPc2d1	nadnárodní
korporací	korporace	k1gFnPc2	korporace
<g/>
,	,	kIx,	,
hlavních	hlavní	k2eAgMnPc2d1	hlavní
odběratelů	odběratel	k1gMnPc2	odběratel
palmového	palmový	k2eAgInSc2d1	palmový
oleje	olej	k1gInSc2	olej
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Procter	Procter	k1gMnSc1	Procter
&	&	k?	&
Gamble	Gamble	k1gMnSc1	Gamble
či	či	k8xC	či
McDonald	McDonald	k1gMnSc1	McDonald
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
světové	světový	k2eAgFnSc2d1	světová
veřejnosti	veřejnost	k1gFnSc2	veřejnost
zavázaly	zavázat	k5eAaPmAgFnP	zavázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
odebírat	odebírat	k5eAaImF	odebírat
suroviny	surovina	k1gFnPc4	surovina
pouze	pouze	k6eAd1	pouze
od	od	k7c2	od
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
nic	nic	k3yNnSc4	nic
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
ilegálním	ilegální	k2eAgNnSc7d1	ilegální
kácením	kácení	k1gNnSc7	kácení
a	a	k8xC	a
vypalováním	vypalování	k1gNnSc7	vypalování
pralesů	prales	k1gInPc2	prales
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
Sumatře	Sumatra	k1gFnSc6	Sumatra
i	i	k8xC	i
Borneu	Borneo	k1gNnSc6	Borneo
působí	působit	k5eAaImIp3nS	působit
řada	řada	k1gFnSc1	řada
dobrovolných	dobrovolný	k2eAgFnPc2d1	dobrovolná
ochranářských	ochranářský	k2eAgFnPc2d1	ochranářská
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
vliv	vliv	k1gInSc1	vliv
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
omezený	omezený	k2eAgInSc1d1	omezený
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgFnSc1d1	vládní
ochrana	ochrana	k1gFnSc1	ochrana
se	se	k3xPyFc4	se
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nedostatku	nedostatek	k1gInSc2	nedostatek
financí	finance	k1gFnPc2	finance
a	a	k8xC	a
lidských	lidský	k2eAgInPc2d1	lidský
zdrojů	zdroj	k1gInPc2	zdroj
hraje	hrát	k5eAaImIp3nS	hrát
zásadní	zásadní	k2eAgFnSc4d1	zásadní
roli	role	k1gFnSc4	role
i	i	k9	i
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
korupce	korupce	k1gFnSc1	korupce
na	na	k7c6	na
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
úrovních	úroveň	k1gFnPc6	úroveň
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
přístup	přístup	k1gInSc1	přístup
prostých	prostý	k2eAgMnPc2d1	prostý
vesničanů	vesničan	k1gMnPc2	vesničan
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgMnPc4	který
je	být	k5eAaImIp3nS	být
ilegální	ilegální	k2eAgNnPc4d1	ilegální
kácení	kácení	k1gNnPc4	kácení
pralesů	prales	k1gInPc2	prales
často	často	k6eAd1	často
jediným	jediný	k2eAgInSc7d1	jediný
zdrojem	zdroj	k1gInSc7	zdroj
příjmů	příjem	k1gInPc2	příjem
<g/>
.	.	kIx.	.
<g/>
Orangutany	orangutan	k1gMnPc4	orangutan
ve	v	k7c6	v
značné	značný	k2eAgFnSc6d1	značná
míře	míra	k1gFnSc6	míra
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
i	i	k9	i
ilegální	ilegální	k2eAgInSc4d1	ilegální
lov	lov	k1gInSc4	lov
<g/>
.	.	kIx.	.
</s>
<s>
Pytláci	pytlák	k1gMnPc1	pytlák
vydělávají	vydělávat	k5eAaImIp3nP	vydělávat
především	především	k9	především
na	na	k7c6	na
prodeji	prodej	k1gFnSc6	prodej
mláďat	mládě	k1gNnPc2	mládě
zabitých	zabitý	k2eAgMnPc2d1	zabitý
orangutanů	orangutan	k1gMnPc2	orangutan
<g/>
.	.	kIx.	.
</s>
<s>
Chov	chov	k1gInSc1	chov
těchto	tento	k3xDgMnPc2	tento
lidoopů	lidoop	k1gMnPc2	lidoop
jako	jako	k8xS	jako
domácích	domácí	k2eAgNnPc2d1	domácí
zvířat	zvíře	k1gNnPc2	zvíře
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
poměrně	poměrně	k6eAd1	poměrně
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnPc1	jejich
kosti	kost	k1gFnPc1	kost
prodávají	prodávat	k5eAaImIp3nP	prodávat
jako	jako	k9	jako
suvenýry	suvenýr	k1gInPc1	suvenýr
<g/>
.	.	kIx.	.
</s>
<s>
Prostými	prostý	k2eAgMnPc7d1	prostý
vesničany	vesničan	k1gMnPc7	vesničan
jsou	být	k5eAaImIp3nP	být
orangutani	orangutan	k1gMnPc1	orangutan
často	často	k6eAd1	často
loveni	lovit	k5eAaImNgMnP	lovit
i	i	k9	i
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Borneu	Borneo	k1gNnSc6	Borneo
představuje	představovat	k5eAaImIp3nS	představovat
právě	právě	k9	právě
ilegální	ilegální	k2eAgInSc1d1	ilegální
lov	lov	k1gInSc1	lov
hlavní	hlavní	k2eAgInSc1d1	hlavní
příčinu	příčina	k1gFnSc4	příčina
poklesu	pokles	k1gInSc2	pokles
zdejší	zdejší	k2eAgFnSc2d1	zdejší
populace	populace	k1gFnSc2	populace
orangutanů	orangutan	k1gMnPc2	orangutan
–	–	k?	–
ročně	ročně	k6eAd1	ročně
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
uloveno	uloven	k2eAgNnSc4d1	uloveno
až	až	k6eAd1	až
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
probíhá	probíhat	k5eAaImIp3nS	probíhat
ilegální	ilegální	k2eAgInSc4d1	ilegální
lov	lov	k1gInSc4	lov
i	i	k9	i
na	na	k7c6	na
Sumatře	Sumatra	k1gFnSc6	Sumatra
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1950	[number]	k4	1950
<g/>
−	−	k?	−
<g/>
2010	[number]	k4	2010
se	se	k3xPyFc4	se
snížil	snížit	k5eAaPmAgInS	snížit
počet	počet	k1gInSc1	počet
bornejských	bornejský	k2eAgMnPc2d1	bornejský
orangutanů	orangutan	k1gMnPc2	orangutan
o	o	k7c4	o
60	[number]	k4	60
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
až	až	k9	až
2025	[number]	k4	2025
je	být	k5eAaImIp3nS	být
očekáván	očekávat	k5eAaImNgInS	očekávat
pokles	pokles	k1gInSc1	pokles
o	o	k7c4	o
dalších	další	k2eAgNnPc2d1	další
22	[number]	k4	22
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
zbývající	zbývající	k2eAgFnSc1d1	zbývající
populace	populace	k1gFnSc1	populace
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
řeky	řeka	k1gFnSc2	řeka
Sabangau	Sabangaus	k1gInSc2	Sabangaus
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
populace	populace	k1gFnSc2	populace
orangutana	orangutan	k1gMnSc2	orangutan
sumaterského	sumaterský	k2eAgNnSc2d1	sumaterské
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nezmění	změnit	k5eNaPmIp3nS	změnit
sestupný	sestupný	k2eAgInSc4d1	sestupný
trend	trend	k1gInSc4	trend
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1985	[number]	k4	1985
<g/>
−	−	k?	−
<g/>
2060	[number]	k4	2060
poklesne	poklesnout	k5eAaPmIp3nS	poklesnout
o	o	k7c4	o
80	[number]	k4	80
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
nejsevernější	severní	k2eAgFnSc6d3	nejsevernější
provincii	provincie	k1gFnSc6	provincie
Aceh	Aceha	k1gFnPc2	Aceha
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
snížil	snížit	k5eAaPmAgInS	snížit
počet	počet	k1gInSc1	počet
orangutanů	orangutan	k1gMnPc2	orangutan
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5300	[number]	k4	5300
na	na	k7c4	na
2500	[number]	k4	2500
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
žije	žít	k5eAaImIp3nS	žít
podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
14	[number]	k4	14
613	[number]	k4	613
sumaterských	sumaterský	k2eAgMnPc2d1	sumaterský
orangutanů	orangutan	k1gMnPc2	orangutan
<g/>
.	.	kIx.	.
</s>
<s>
Odhad	odhad	k1gInSc1	odhad
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozlohy	rozloha	k1gFnSc2	rozloha
obyvatelného	obyvatelný	k2eAgInSc2d1	obyvatelný
habitatu	habitat	k1gInSc2	habitat
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
existenci	existence	k1gFnSc4	existence
asi	asi	k9	asi
55	[number]	k4	55
000	[number]	k4	000
bornejských	bornejský	k2eAgMnPc2d1	bornejský
orangutanů	orangutan	k1gMnPc2	orangutan
<g/>
;	;	kIx,	;
roku	rok	k1gInSc3	rok
2012	[number]	k4	2012
však	však	k9	však
byla	být	k5eAaImAgFnS	být
předpokládaná	předpokládaný	k2eAgFnSc1d1	předpokládaná
rozloha	rozloha	k1gFnSc1	rozloha
habitatu	habitat	k1gInSc2	habitat
výrazně	výrazně	k6eAd1	výrazně
navýšena	navýšen	k2eAgFnSc1d1	navýšena
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
populaci	populace	k1gFnSc4	populace
o	o	k7c4	o
asi	asi	k9	asi
104	[number]	k4	104
700	[number]	k4	700
jedincích	jedinec	k1gMnPc6	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
akční	akční	k2eAgInSc1d1	akční
plán	plán	k1gInSc1	plán
indonéské	indonéský	k2eAgFnSc2d1	Indonéská
vlády	vláda	k1gFnSc2	vláda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
vyčíslil	vyčíslit	k5eAaPmAgInS	vyčíslit
celkovou	celkový	k2eAgFnSc4d1	celková
populaci	populace	k1gFnSc4	populace
na	na	k7c4	na
61	[number]	k4	61
234	[number]	k4	234
orangutanů	orangutan	k1gMnPc2	orangutan
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
54	[number]	k4	54
567	[number]	k4	567
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Borneo	Borneo	k1gNnSc1	Borneo
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
nejnověji	nově	k6eAd3	nově
objeveného	objevený	k2eAgMnSc2d1	objevený
orangutana	orangutan	k1gMnSc2	orangutan
tapanulijského	tapanulijský	k2eAgMnSc2d1	tapanulijský
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
800	[number]	k4	800
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ochrana	ochrana	k1gFnSc1	ochrana
===	===	k?	===
</s>
</p>
<p>
<s>
Orangutan	orangutan	k1gMnSc1	orangutan
sumaterský	sumaterský	k2eAgMnSc1d1	sumaterský
je	být	k5eAaImIp3nS	být
chráněn	chráněn	k2eAgMnSc1d1	chráněn
indonéským	indonéský	k2eAgInSc7d1	indonéský
zákonem	zákon	k1gInSc7	zákon
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
zachování	zachování	k1gNnSc4	zachování
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
ochrana	ochrana	k1gFnSc1	ochrana
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
chráněna	chráněn	k2eAgFnSc1d1	chráněna
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
oblast	oblast	k1gFnSc1	oblast
Leuser	Leusero	k1gNnPc2	Leusero
Ecosystem	Ecosyst	k1gInSc7	Ecosyst
v	v	k7c6	v
Acehu	Aceh	k1gInSc6	Aceh
<g/>
.	.	kIx.	.
</s>
<s>
Orangutan	orangutan	k1gMnSc1	orangutan
bornejský	bornejský	k2eAgMnSc1d1	bornejský
je	být	k5eAaImIp3nS	být
chráněn	chráněn	k2eAgMnSc1d1	chráněn
podle	podle	k7c2	podle
malajských	malajský	k2eAgInPc2d1	malajský
a	a	k8xC	a
indonéských	indonéský	k2eAgInPc2d1	indonéský
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
přirozené	přirozený	k2eAgNnSc4d1	přirozené
prostředí	prostředí	k1gNnSc4	prostředí
však	však	k9	však
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
nepodléhá	podléhat	k5eNaImIp3nS	podléhat
zvláštní	zvláštní	k2eAgFnSc3d1	zvláštní
ochraně	ochrana	k1gFnSc3	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
zachování	zachování	k1gNnSc4	zachování
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
ochrana	ochrana	k1gFnSc1	ochrana
velkých	velký	k2eAgFnPc2d1	velká
lesních	lesní	k2eAgFnPc2d1	lesní
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
zachování	zachování	k1gNnSc6	zachování
orangutanů	orangutan	k1gMnPc2	orangutan
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
některé	některý	k3yIgFnPc1	některý
organizace	organizace	k1gFnPc1	organizace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
australské	australský	k2eAgNnSc4d1	Australské
Borneo	Borneo	k1gNnSc4	Borneo
Orangutan	orangutan	k1gMnSc1	orangutan
Survival	Survival	k1gMnSc1	Survival
<g/>
,	,	kIx,	,
Bornean	Bornean	k1gMnSc1	Bornean
Orangutan	orangutan	k1gMnSc1	orangutan
Survival	Survival	k1gMnSc1	Survival
Foundation	Foundation	k1gInSc4	Foundation
založená	založený	k2eAgNnPc1d1	založené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
Orangutan	orangutan	k1gMnSc1	orangutan
Conservancy	Conservanca	k1gFnSc2	Conservanca
<g/>
,	,	kIx,	,
Orangutan	orangutan	k1gMnSc1	orangutan
Outreach	Outreach	k1gMnSc1	Outreach
<g/>
,	,	kIx,	,
Sumatran	Sumatran	k1gInSc1	Sumatran
Orangutan	orangutan	k1gMnSc1	orangutan
Conservation	Conservation	k1gInSc1	Conservation
Program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
Orang	Orang	k1gInSc1	Orang
Utan	Utana	k1gFnPc2	Utana
Republik	republika	k1gFnPc2	republika
<g/>
,	,	kIx,	,
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
Great	Great	k2eAgInSc1d1	Great
Apes	Apes	k1gInSc1	Apes
<g/>
,	,	kIx,	,
Save	Save	k1gNnSc1	Save
the	the	k?	the
Orangutan	orangutan	k1gMnSc1	orangutan
chránící	chránící	k2eAgMnSc1d1	chránící
hlavně	hlavně	k9	hlavně
bornejské	bornejský	k2eAgMnPc4d1	bornejský
orangutany	orangutan	k1gMnPc4	orangutan
či	či	k8xC	či
Orangutan	orangutan	k1gMnSc1	orangutan
Foundation	Foundation	k1gInSc4	Foundation
UK	UK	kA	UK
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
jsou	být	k5eAaImIp3nP	být
orangutani	orangutan	k1gMnPc1	orangutan
chováni	chován	k2eAgMnPc1d1	chován
ve	v	k7c6	v
speciálních	speciální	k2eAgInPc6d1	speciální
pavilonech	pavilon	k1gInPc6	pavilon
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
23	[number]	k4	23
až	až	k9	až
26	[number]	k4	26
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
vlhkosti	vlhkost	k1gFnSc2	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Trpí	trpět	k5eAaImIp3nP	trpět
sklony	sklon	k1gInPc1	sklon
k	k	k7c3	k
obezitě	obezita	k1gFnSc3	obezita
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
doporučeno	doporučen	k2eAgNnSc1d1	doporučeno
krmit	krmit	k5eAaImF	krmit
je	být	k5eAaImIp3nS	být
pětkrát	pětkrát	k6eAd1	pětkrát
denně	denně	k6eAd1	denně
nekalorickou	kalorický	k2eNgFnSc7d1	nekalorická
potravou	potrava	k1gFnSc7	potrava
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
evidováno	evidovat	k5eAaImNgNnS	evidovat
987	[number]	k4	987
jedinců	jedinec	k1gMnPc2	jedinec
v	v	k7c6	v
217	[number]	k4	217
institucích	instituce	k1gFnPc6	instituce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
chová	chovat	k5eAaImIp3nS	chovat
orangutana	orangutan	k1gMnSc4	orangutan
například	například	k6eAd1	například
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
druh	druh	k1gInSc1	druh
sumaterský	sumaterský	k2eAgInSc1d1	sumaterský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
orangutan	orangutan	k1gMnSc1	orangutan
jménem	jméno	k1gNnSc7	jméno
Kama	Kamum	k1gNnSc2	Kamum
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
a	a	k8xC	a
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgNnSc4	první
narozené	narozený	k2eAgNnSc4d1	narozené
mládě	mládě	k1gNnSc4	mládě
orangutana	orangutan	k1gMnSc2	orangutan
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
zoo	zoo	k1gFnSc6	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
vlastní	vlastní	k2eAgFnSc2d1	vlastní
orangutany	orangutan	k1gMnPc4	orangutan
bornejské	bornejský	k2eAgNnSc1d1	bornejské
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc2	jeden
křížence	kříženec	k1gMnSc2	kříženec
orangutana	orangutan	k1gMnSc2	orangutan
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
v	v	k7c4	v
ZOO	zoo	k1gFnSc4	zoo
Dvůr	Dvůr	k1gInSc4	Dvůr
Králové	Králová	k1gFnSc2	Králová
jsou	být	k5eAaImIp3nP	být
chování	chování	k1gNnSc4	chování
bornejští	bornejský	k2eAgMnPc1d1	bornejský
orangutani	orangutan	k1gMnPc1	orangutan
a	a	k8xC	a
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
narodilo	narodit	k5eAaPmAgNnS	narodit
dvanáct	dvanáct	k4xCc1	dvanáct
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
===	===	k?	===
</s>
</p>
<p>
<s>
Orangutani	orangutan	k1gMnPc1	orangutan
byli	být	k5eAaImAgMnP	být
známí	známý	k1gMnPc1	známý
domorodcům	domorodec	k1gMnPc3	domorodec
Sumatry	Sumatra	k1gFnSc2	Sumatra
a	a	k8xC	a
Bornea	Borneo	k1gNnSc2	Borneo
po	po	k7c6	po
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
je	on	k3xPp3gNnSc4	on
lovili	lovit	k5eAaImAgMnP	lovit
pro	pro	k7c4	pro
obživu	obživa	k1gFnSc4	obživa
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jiné	jiné	k1gNnSc4	jiné
komunity	komunita	k1gFnSc2	komunita
však	však	k9	však
toto	tento	k3xDgNnSc1	tento
chování	chování	k1gNnSc1	chování
znamenalo	znamenat	k5eAaImAgNnS	znamenat
tabu	tabu	k1gNnSc7	tabu
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Bornea	Borneo	k1gNnSc2	Borneo
podle	podle	k7c2	podle
pověr	pověra	k1gFnPc2	pověra
přináší	přinášet	k5eAaImIp3nS	přinášet
smůlu	smůla	k1gFnSc4	smůla
podívat	podívat	k5eAaImF	podívat
se	se	k3xPyFc4	se
orangutanovi	orangutan	k1gMnSc6	orangutan
do	do	k7c2	do
obličeje	obličej	k1gInSc2	obličej
tváří	tvář	k1gFnPc2	tvář
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
lidové	lidový	k2eAgInPc1d1	lidový
příběhy	příběh	k1gInPc1	příběh
pojednávají	pojednávat	k5eAaImIp3nP	pojednávat
o	o	k7c6	o
orangutanech	orangutan	k1gMnPc6	orangutan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
kopulují	kopulovat	k5eAaImIp3nP	kopulovat
a	a	k8xC	a
unáší	unášet	k5eAaImIp3nP	unášet
je	on	k3xPp3gFnPc4	on
<g/>
;	;	kIx,	;
známy	znám	k2eAgFnPc4d1	známa
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
příběhy	příběh	k1gInPc1	příběh
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
jsou	být	k5eAaImIp3nP	být
muži	muž	k1gMnPc1	muž
svedeni	sveden	k2eAgMnPc1d1	sveden
samicemi	samice	k1gFnPc7	samice
orangutanů	orangutan	k1gMnPc2	orangutan
<g/>
.	.	kIx.	.
<g/>
Evropané	Evropan	k1gMnPc1	Evropan
objevili	objevit	k5eAaPmAgMnP	objevit
existenci	existence	k1gFnSc4	existence
orangutanů	orangutan	k1gMnPc2	orangutan
možná	možná	k6eAd1	možná
již	již	k6eAd1	již
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
během	během	k7c2	během
století	století	k1gNnSc2	století
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
je	on	k3xPp3gMnPc4	on
značně	značně	k6eAd1	značně
pronásledovali	pronásledovat	k5eAaImAgMnP	pronásledovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
přesný	přesný	k2eAgInSc4d1	přesný
popis	popis	k1gInSc4	popis
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
anatom	anatom	k1gMnSc1	anatom
Petrus	Petrus	k1gMnSc1	Petrus
Camper	Camper	k1gMnSc1	Camper
<g/>
.	.	kIx.	.
<g/>
O	o	k7c4	o
chování	chování	k1gNnSc4	chování
těchto	tento	k3xDgMnPc2	tento
lidoopů	lidoop	k1gMnPc2	lidoop
bylo	být	k5eAaImAgNnS	být
dlouho	dlouho	k6eAd1	dlouho
známo	znám	k2eAgNnSc1d1	známo
málo	málo	k1gNnSc1	málo
informací	informace	k1gFnPc2	informace
<g/>
;	;	kIx,	;
biologii	biologie	k1gFnSc4	biologie
orangutanů	orangutan	k1gMnPc2	orangutan
začala	začít	k5eAaPmAgFnS	začít
podrobněji	podrobně	k6eAd2	podrobně
studovat	studovat	k5eAaImF	studovat
teprve	teprve	k6eAd1	teprve
odbornice	odbornice	k1gFnSc1	odbornice
na	na	k7c4	na
primáty	primát	k1gInPc4	primát
Birutė	Birutė	k1gFnSc1	Birutė
Galdikasová	Galdikasová	k1gFnSc1	Galdikasová
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
dorazila	dorazit	k5eAaPmAgFnS	dorazit
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Borneo	Borneo	k1gNnSc4	Borneo
<g/>
,	,	kIx,	,
usadila	usadit	k5eAaPmAgFnS	usadit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
primitivní	primitivní	k2eAgFnSc6d1	primitivní
chatrči	chatrč	k1gFnSc6	chatrč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přes	přes	k7c4	přes
potíže	potíž	k1gFnPc4	potíž
zde	zde	k6eAd1	zde
zůstala	zůstat	k5eAaPmAgFnS	zůstat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
orangutany	orangutan	k1gMnPc4	orangutan
obhajovat	obhajovat	k5eAaImF	obhajovat
a	a	k8xC	a
podporovat	podporovat	k5eAaImF	podporovat
ochranu	ochrana	k1gFnSc4	ochrana
ubývajících	ubývající	k2eAgInPc2d1	ubývající
deštných	deštný	k2eAgInPc2d1	deštný
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Galdakisová	Galdakisový	k2eAgFnSc1d1	Galdakisový
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
starala	starat	k5eAaImAgFnS	starat
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
osiřelých	osiřelý	k2eAgMnPc2d1	osiřelý
orangutanů	orangutan	k1gMnPc2	orangutan
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
folklorních	folklorní	k2eAgInPc6d1	folklorní
příbězích	příběh	k1gInPc6	příběh
obyvatel	obyvatel	k1gMnPc2	obyvatel
Sumatry	Sumatra	k1gFnSc2	Sumatra
a	a	k8xC	a
Bornea	Borneo	k1gNnSc2	Borneo
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
kultuře	kultura	k1gFnSc6	kultura
přetrvával	přetrvávat	k5eAaImAgMnS	přetrvávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
samce	samec	k1gInPc1	samec
orangutanů	orangutan	k1gMnPc2	orangutan
přitahují	přitahovat	k5eAaImIp3nP	přitahovat
ženy	žena	k1gFnPc1	žena
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
i	i	k9	i
znásilňují	znásilňovat	k5eAaImIp3nP	znásilňovat
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
neoficiální	oficiální	k2eNgInSc1d1	neoficiální
záznam	záznam	k1gInSc1	záznam
o	o	k7c6	o
sexuálně	sexuálně	k6eAd1	sexuálně
motivovaném	motivovaný	k2eAgInSc6d1	motivovaný
útoku	útok	k1gInSc6	útok
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
primatoložky	primatoložka	k1gFnSc2	primatoložka
Galdakisové	Galdakisový	k2eAgFnSc2d1	Galdakisový
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
napadení	napadení	k1gNnSc3	napadení
jejího	její	k3xOp3gNnSc2	její
kuchaře	kuchař	k1gMnPc4	kuchař
samcem	samec	k1gMnSc7	samec
orangutana	orangutan	k1gMnSc2	orangutan
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
orangutan	orangutan	k1gMnSc1	orangutan
byl	být	k5eAaImAgMnS	být
však	však	k9	však
chován	chován	k2eAgMnSc1d1	chován
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
možná	možná	k9	možná
špatně	špatně	k6eAd1	špatně
identifikoval	identifikovat	k5eAaBmAgInS	identifikovat
druh	druh	k1gInSc1	druh
<g/>
;	;	kIx,	;
nucená	nucený	k2eAgFnSc1d1	nucená
kopulace	kopulace	k1gFnSc1	kopulace
je	být	k5eAaImIp3nS	být
pářící	pářící	k2eAgFnSc7d1	pářící
strategií	strategie	k1gFnSc7	strategie
plně	plně	k6eAd1	plně
nevyvinutých	vyvinutý	k2eNgMnPc2d1	nevyvinutý
orangutaních	orangutaní	k2eAgMnPc2d1	orangutaní
samců	samec	k1gMnPc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
naopak	naopak	k6eAd1	naopak
pochází	pocházet	k5eAaImIp3nS	pocházet
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
orangutaní	orangutaný	k2eAgMnPc1d1	orangutaný
samice	samice	k1gFnSc2	samice
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
Pony	pony	k1gMnPc5	pony
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Kareng	Karenga	k1gFnPc2	Karenga
Pangi	Pang	k1gFnSc2	Pang
využívána	využívat	k5eAaImNgFnS	využívat
ve	v	k7c6	v
zdejším	zdejší	k2eAgInSc6d1	zdejší
nevěstinci	nevěstinec	k1gInSc6	nevěstinec
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
Vraždy	vražda	k1gFnSc2	vražda
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Morgue	Morgu	k1gFnSc2	Morgu
amerického	americký	k2eAgMnSc2d1	americký
básníka	básník	k1gMnSc2	básník
a	a	k8xC	a
spisovatele	spisovatel	k1gMnSc2	spisovatel
Edgara	Edgar	k1gMnSc2	Edgar
Allana	Allan	k1gMnSc2	Allan
Poea	Poeus	k1gMnSc2	Poeus
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
orangutan	orangutan	k1gMnSc1	orangutan
jako	jako	k8xS	jako
vrah	vrah	k1gMnSc1	vrah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Orangutan	orangutan	k1gMnSc1	orangutan
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
PAYNE	PAYNE	kA	PAYNE
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
;	;	kIx,	;
PRUDENTE	PRUDENTE	kA	PRUDENTE
<g/>
,	,	kIx,	,
C.	C.	kA	C.
Orangutans	Orangutans	k1gInSc1	Orangutans
:	:	kIx,	:
behavior	behavior	k1gInSc1	behavior
<g/>
,	,	kIx,	,
ecology	ecolog	k1gInPc1	ecolog
<g/>
,	,	kIx,	,
and	and	k?	and
conservation	conservation	k1gInSc1	conservation
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
New	New	k1gMnSc1	New
Holland	Hollanda	k1gFnPc2	Hollanda
Publishers	Publishersa	k1gFnPc2	Publishersa
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
262	[number]	k4	262
<g/>
-	-	kIx~	-
<g/>
16253	[number]	k4	16253
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VIERINGOVÁ	VIERINGOVÁ	kA	VIERINGOVÁ
<g/>
,	,	kIx,	,
Kerstin	Kerstin	k1gMnSc1	Kerstin
<g/>
;	;	kIx,	;
KNAUER	KNAUER	kA	KNAUER
<g/>
,	,	kIx,	,
Roland	Roland	k1gInSc1	Roland
<g/>
.	.	kIx.	.
</s>
<s>
Ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Helena	Helena	k1gFnSc1	Helena
Kholová	Kholová	k1gFnSc1	Kholová
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
304	[number]	k4	304
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
242	[number]	k4	242
<g/>
-	-	kIx~	-
<g/>
3180	[number]	k4	3180
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
s.	s.	k?	s.
116	[number]	k4	116
<g/>
−	−	k?	−
<g/>
118	[number]	k4	118
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VANČATA	VANČATA	kA	VANČATA
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Primatologie	Primatologie	k1gFnSc1	Primatologie
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
2	[number]	k4	2
<g/>
:	:	kIx,	:
Catarrhina	Catarrhina	k1gFnSc1	Catarrhina
-	-	kIx~	-
opice	opice	k1gFnSc1	opice
a	a	k8xC	a
lidoopi	lidoop	k1gMnPc1	lidoop
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
128	[number]	k4	128
<g/>
−	−	k?	−
<g/>
138	[number]	k4	138
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
orangutan	orangutan	k1gMnSc1	orangutan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Pongo	Pongo	k6eAd1	Pongo
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Primate	Primat	k1gInSc5	Primat
Factsheets	Factsheets	k1gInSc1	Factsheets
<g/>
:	:	kIx,	:
Orangutan	orangutan	k1gMnSc1	orangutan
(	(	kIx(	(
<g/>
Pongo	Pongo	k6eAd1	Pongo
<g/>
)	)	kIx)	)
Taxonomy	Taxonom	k1gInPc7	Taxonom
<g/>
,	,	kIx,	,
Morphology	Morpholog	k1gMnPc7	Morpholog
<g/>
,	,	kIx,	,
&	&	k?	&
Ecology	Ecologa	k1gFnSc2	Ecologa
<g/>
.	.	kIx.	.
pin	pin	k1gInSc1	pin
<g/>
.	.	kIx.	.
<g/>
primate	primat	k1gInSc5	primat
<g/>
.	.	kIx.	.
<g/>
wisc	wisc	k1gFnSc1	wisc
<g/>
.	.	kIx.	.
<g/>
edu	edu	k?	edu
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SODARO	SODARO	kA	SODARO
<g/>
,	,	kIx,	,
Carol	Carol	k1gInSc1	Carol
<g/>
.	.	kIx.	.
</s>
<s>
Orangutan	orangutan	k1gMnSc1	orangutan
Husbandry	Husbandra	k1gFnSc2	Husbandra
Manual	Manual	k1gMnSc1	Manual
<g/>
:	:	kIx,	:
Species	species	k1gFnSc1	species
Survival	Survival	k1gMnSc1	Survival
Plan	plan	k1gInSc1	plan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
Zoological	Zoological	k1gFnSc2	Zoological
Society	societa	k1gFnSc2	societa
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
26	[number]	k4	26
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Pongo	Pongo	k1gNnSc4	Pongo
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
