<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
City	city	k1gNnSc1	city
of	of	k?	of
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
v	v	k7c6	v
běžném	běžný	k2eAgNnSc6d1	běžné
užívání	užívání	k1gNnSc6	užívání
i	i	k8xC	i
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
City	City	k1gFnPc2	City
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejlidnatější	lidnatý	k2eAgNnSc1d3	nejlidnatější
město	město	k1gNnSc1	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
výběžku	výběžek	k1gInSc6	výběžek
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Hudson	Hudsona	k1gFnPc2	Hudsona
do	do	k7c2	do
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
do	do	k7c2	do
sousedních	sousední	k2eAgInPc2d1	sousední
států	stát	k1gInPc2	stát
a	a	k8xC	a
která	který	k3yRgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejlidnatější	lidnatý	k2eAgInSc4d3	nejlidnatější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
Nizozemci	Nizozemec	k1gMnPc7	Nizozemec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1625	[number]	k4	1625
jako	jako	k8xC	jako
Nový	nový	k2eAgInSc1d1	nový
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
,	,	kIx,	,
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Angličanů	Angličan	k1gMnPc2	Angličan
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
roku	rok	k1gInSc2	rok
1664	[number]	k4	1664
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
tvoří	tvořit	k5eAaImIp3nS	tvořit
pět	pět	k4xCc4	pět
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Bronx	Bronx	k1gInSc1	Bronx
<g/>
,	,	kIx,	,
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
<g/>
,	,	kIx,	,
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
,	,	kIx,	,
Queens	Queens	k1gInSc1	Queens
a	a	k8xC	a
Staten	Staten	k2eAgInSc1d1	Staten
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1785	[number]	k4	1785
<g/>
-	-	kIx~	-
<g/>
1790	[number]	k4	1790
byl	být	k5eAaImAgInS	být
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
ze	z	k7c2	z
světových	světový	k2eAgNnPc2d1	světové
center	centrum	k1gNnPc2	centrum
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
finančnictví	finančnictví	k1gNnSc2	finančnictví
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
vliv	vliv	k1gInSc1	vliv
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
médií	médium	k1gNnPc2	médium
<g/>
,	,	kIx,	,
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
zábavy	zábava	k1gFnSc2	zábava
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
reklamy	reklama	k1gFnSc2	reklama
a	a	k8xC	a
módy	móda	k1gFnSc2	móda
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
se	se	k3xPyFc4	se
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2014	[number]	k4	2014
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
první	první	k4xOgFnSc6	první
příčce	příčka	k1gFnSc6	příčka
prestižního	prestižní	k2eAgInSc2d1	prestižní
žebříčku	žebříček	k1gInSc2	žebříček
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
módních	módní	k2eAgFnPc2d1	módní
metropolí	metropol	k1gFnPc2	metropol
světa	svět	k1gInSc2	svět
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
oficiálně	oficiálně	k6eAd1	oficiálně
používat	používat	k5eAaImF	používat
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
módy	móda	k1gFnSc2	móda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
také	také	k9	také
ohniskem	ohnisko	k1gNnSc7	ohnisko
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vztahů	vztah	k1gInPc2	vztah
a	a	k8xC	a
diplomacie	diplomacie	k1gFnSc2	diplomacie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
sídlo	sídlo	k1gNnSc4	sídlo
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Yorku	York	k1gInSc2	York
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
celodennímu	celodenní	k2eAgInSc3d1	celodenní
ruchu	ruch	k1gInSc3	ruch
někdy	někdy	k6eAd1	někdy
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
"	"	kIx"	"
<g/>
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nikdy	nikdy	k6eAd1	nikdy
nespí	spát	k5eNaImIp3nS	spát
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
City	City	k1gFnSc2	City
That	That	k1gMnSc1	That
Never	Never	k1gMnSc1	Never
Sleeps	Sleepsa	k1gFnPc2	Sleepsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
přezdívkami	přezdívka	k1gFnPc7	přezdívka
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
Velké	velký	k2eAgNnSc1d1	velké
jablko	jablko	k1gNnSc1	jablko
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Big	Big	k1gMnSc3	Big
Apple	Apple	kA	Apple
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Gotham	Gotham	k1gInSc1	Gotham
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
objevu	objev	k1gInSc2	objev
Evropany	Evropan	k1gMnPc7	Evropan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1524	[number]	k4	1524
obydlena	obydlet	k5eAaPmNgFnS	obydlet
asi	asi	k9	asi
5000	[number]	k4	5000
indiány	indián	k1gMnPc7	indián
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Lenape	Lenap	k1gInSc5	Lenap
<g/>
.	.	kIx.	.
</s>
<s>
Objevitelem	objevitel	k1gMnSc7	objevitel
oblasti	oblast	k1gFnSc2	oblast
byl	být	k5eAaImAgMnS	být
Giovanni	Giovaneň	k1gFnSc3	Giovaneň
da	da	k?	da
Verrazzano	Verrazzana	k1gFnSc5	Verrazzana
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
cestovatel	cestovatel	k1gMnSc1	cestovatel
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
francouzské	francouzský	k2eAgFnSc2d1	francouzská
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
místo	místo	k1gNnSc4	místo
nazval	nazvat	k5eAaBmAgMnS	nazvat
Nové	Nové	k2eAgFnSc4d1	Nové
Angoulê	Angoulê	k1gFnSc4	Angoulê
(	(	kIx(	(
<g/>
Nouvelle	Nouvelle	k1gFnSc1	Nouvelle
Angoulê	Angoulê	k1gFnSc2	Angoulê
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
evropskou	evropský	k2eAgFnSc4d1	Evropská
osadu	osada	k1gFnSc4	osada
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Nový	nový	k2eAgInSc1d1	nový
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
(	(	kIx(	(
<g/>
Nieuw	Nieuw	k1gFnSc1	Nieuw
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
)	)	kIx)	)
založili	založit	k5eAaPmAgMnP	založit
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
cípu	cíp	k1gInSc6	cíp
Manhattanu	Manhattan	k1gInSc2	Manhattan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1614	[number]	k4	1614
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
kožešinami	kožešina	k1gFnPc7	kožešina
<g/>
.	.	kIx.	.
</s>
<s>
Vůdce	vůdce	k1gMnSc1	vůdce
nizozemských	nizozemský	k2eAgFnPc2d1	nizozemská
kolonií	kolonie	k1gFnPc2	kolonie
Peter	Peter	k1gMnSc1	Peter
Minuit	Minuit	k1gMnSc1	Minuit
koupil	koupit	k5eAaPmAgMnS	koupit
Manhattan	Manhattan	k1gInSc4	Manhattan
od	od	k7c2	od
kmene	kmen	k1gInSc2	kmen
Lenape	Lenap	k1gInSc5	Lenap
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1626	[number]	k4	1626
za	za	k7c4	za
60	[number]	k4	60
guldenů	gulden	k1gInPc2	gulden
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
vyvrácená	vyvrácený	k2eAgFnSc1d1	vyvrácená
legenda	legenda	k1gFnSc1	legenda
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Manhattan	Manhattan	k1gInSc1	Manhattan
byl	být	k5eAaImAgInS	být
koupen	koupit	k5eAaPmNgInS	koupit
za	za	k7c4	za
skleněné	skleněný	k2eAgInPc4d1	skleněný
korálky	korálek	k1gInPc4	korálek
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
24	[number]	k4	24
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1664	[number]	k4	1664
dobyli	dobýt	k5eAaPmAgMnP	dobýt
město	město	k1gNnSc4	město
Angličané	Angličan	k1gMnPc1	Angličan
a	a	k8xC	a
přejmenovali	přejmenovat	k5eAaPmAgMnP	přejmenovat
je	on	k3xPp3gNnSc4	on
na	na	k7c6	na
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
krále	král	k1gMnSc2	král
Jakuba	Jakub	k1gMnSc2	Jakub
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
vévody	vévoda	k1gMnPc4	vévoda
z	z	k7c2	z
Yorku	York	k1gInSc2	York
a	a	k8xC	a
Albany	Albana	k1gFnSc2	Albana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
anglo-nizozemské	angloizozemský	k2eAgFnSc2d1	anglo-nizozemský
války	válka	k1gFnSc2	válka
získali	získat	k5eAaPmAgMnP	získat
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
indonéským	indonéský	k2eAgInSc7d1	indonéský
ostrovem	ostrov	k1gInSc7	ostrov
Run	Runa	k1gFnPc2	Runa
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
mnohem	mnohem	k6eAd1	mnohem
významnější	významný	k2eAgFnPc4d2	významnější
<g/>
,	,	kIx,	,
a	a	k8xC	a
Angličané	Angličan	k1gMnPc1	Angličan
si	se	k3xPyFc3	se
výměnou	výměna	k1gFnSc7	výměna
mohli	moct	k5eAaImAgMnP	moct
ponechat	ponechat	k5eAaPmF	ponechat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
New	New	k1gFnSc7	New
Yorkem	York	k1gInSc7	York
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
klesla	klesnout	k5eAaPmAgFnS	klesnout
populace	populace	k1gFnSc1	populace
kmene	kmen	k1gInSc2	kmen
Lenape	Lenap	k1gInSc5	Lenap
na	na	k7c4	na
200	[number]	k4	200
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
New	New	k1gFnPc2	New
Yorku	York	k1gInSc2	York
jako	jako	k8xS	jako
přístavu	přístav	k1gInSc2	přístav
pod	pod	k7c7	pod
britskou	britský	k2eAgFnSc7d1	britská
vládou	vláda	k1gFnSc7	vláda
rostl	růst	k5eAaImAgMnS	růst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1754	[number]	k4	1754
byla	být	k5eAaImAgFnS	být
chartou	charta	k1gFnSc7	charta
vydanou	vydaný	k2eAgFnSc4d1	vydaná
Jiřím	Jiří	k1gMnSc7	Jiří
II	II	kA	II
<g/>
.	.	kIx.	.
založena	založen	k2eAgFnSc1d1	založena
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
Manhattanu	Manhattan	k1gInSc6	Manhattan
Columbijská	Columbijský	k2eAgFnSc1d1	Columbijská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Kings	Kingsa	k1gFnPc2	Kingsa
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gFnSc7	College
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Americké	americký	k2eAgFnSc2d1	americká
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
odehrála	odehrát	k5eAaPmAgFnS	odehrát
série	série	k1gFnSc1	série
bitev	bitva	k1gFnPc2	bitva
známých	známý	k2eAgFnPc2d1	známá
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Newyorská	newyorský	k2eAgFnSc1d1	newyorská
kampaň	kampaň	k1gFnSc1	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1789	[number]	k4	1789
se	se	k3xPyFc4	se
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
sešel	sejít	k5eAaPmAgInS	sejít
Kongres	kongres	k1gInSc1	kongres
a	a	k8xC	a
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
George	Georg	k1gFnSc2	Georg
Washington	Washington	k1gInSc1	Washington
byl	být	k5eAaImAgMnS	být
inaugurován	inaugurovat	k5eAaBmNgMnS	inaugurovat
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Federal	Federal	k1gMnSc1	Federal
Hall	Hall	k1gMnSc1	Hall
na	na	k7c4	na
Wall	Wall	k1gInSc4	Wall
Street	Streeta	k1gFnPc2	Streeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1790	[number]	k4	1790
předstihl	předstihnout	k5eAaPmAgInS	předstihnout
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Filadelfii	Filadelfie	k1gFnSc4	Filadelfie
jako	jako	k8xS	jako
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
znamenalo	znamenat	k5eAaImAgNnS	znamenat
příliv	příliv	k1gInSc4	příliv
imigrantů	imigrant	k1gMnPc2	imigrant
a	a	k8xC	a
stálý	stálý	k2eAgInSc4d1	stálý
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Vizionářský	vizionářský	k2eAgInSc1d1	vizionářský
plán	plán	k1gInSc1	plán
rozvoje	rozvoj	k1gInSc2	rozvoj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1811	[number]	k4	1811
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
rozšířit	rozšířit	k5eAaPmF	rozšířit
síť	síť	k1gFnSc4	síť
ulic	ulice	k1gFnPc2	ulice
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1819	[number]	k4	1819
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
Erijský	Erijský	k2eAgInSc1d1	Erijský
kanál	kanál	k1gInSc1	kanál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
propojil	propojit	k5eAaPmAgInS	propojit
newyorský	newyorský	k2eAgInSc1d1	newyorský
přístav	přístav	k1gInSc1	přístav
se	s	k7c7	s
zemědělskými	zemědělský	k2eAgFnPc7d1	zemědělská
oblastmi	oblast	k1gFnPc7	oblast
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc4d1	místní
politiku	politika	k1gFnSc4	politika
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
Tammany	Tamman	k1gInPc4	Tamman
Hall	Halla	k1gFnPc2	Halla
<g/>
,	,	kIx,	,
politické	politický	k2eAgNnSc1d1	politické
uskupení	uskupení	k1gNnSc1	uskupení
uvnitř	uvnitř	k7c2	uvnitř
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
kontrolované	kontrolovaný	k2eAgFnSc2d1	kontrolovaná
irskými	irský	k2eAgMnPc7d1	irský
imigranty	imigrant	k1gMnPc7	imigrant
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
obchodnické	obchodnický	k2eAgFnSc2d1	obchodnická
aristokracie	aristokracie	k1gFnSc2	aristokracie
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
zřízení	zřízení	k1gNnSc4	zřízení
Centrálního	centrální	k2eAgInSc2d1	centrální
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nakonec	nakonec	k6eAd1	nakonec
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
i	i	k8xC	i
v	v	k7c6	v
Brooklynu	Brooklyn	k1gInSc6	Brooklyn
žila	žít	k5eAaImAgFnS	žít
výrazná	výrazný	k2eAgFnSc1d1	výrazná
svobodná	svobodný	k2eAgFnSc1d1	svobodná
černá	černý	k2eAgFnSc1d1	černá
menšina	menšina	k1gFnSc1	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Otroctví	otroctví	k1gNnSc1	otroctví
v	v	k7c6	v
New	New	k1gFnPc6	New
Yorku	York	k1gInSc2	York
existovalo	existovat	k5eAaImAgNnS	existovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1827	[number]	k4	1827
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
stalo	stát	k5eAaPmAgNnS	stát
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
center	centrum	k1gNnPc2	centrum
abolicionismu	abolicionismus	k1gInSc2	abolicionismus
<g/>
.	.	kIx.	.
</s>
<s>
Hněv	hněv	k1gInSc4	hněv
nad	nad	k7c7	nad
odvody	odvod	k1gInPc7	odvod
do	do	k7c2	do
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
-	-	kIx~	-
<g/>
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
vedl	vést	k5eAaImAgInS	vést
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
k	k	k7c3	k
sérii	série	k1gFnSc3	série
nepokojů	nepokoj	k1gInPc2	nepokoj
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýraznějších	výrazný	k2eAgInPc2d3	nejvýraznější
případů	případ	k1gInPc2	případ
občanské	občanský	k2eAgFnSc2d1	občanská
neposlušnosti	neposlušnost	k1gFnSc2	neposlušnost
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
byla	být	k5eAaImAgFnS	být
zformována	zformován	k2eAgFnSc1d1	zformována
moderní	moderní	k2eAgFnSc1d1	moderní
podoba	podoba	k1gFnSc1	podoba
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
spojením	spojení	k1gNnSc7	spojení
Brooklynu	Brooklyn	k1gInSc2	Brooklyn
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
samostatného	samostatný	k2eAgNnSc2d1	samostatné
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
okresu	okres	k1gInSc3	okres
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
(	(	kIx(	(
<g/>
které	který	k3yQgNnSc1	který
tehdy	tehdy	k6eAd1	tehdy
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
části	část	k1gFnPc4	část
Bronxu	Bronx	k1gInSc2	Bronx
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
okresu	okres	k1gInSc3	okres
Richmond	Richmonda	k1gFnPc2	Richmonda
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
okresu	okres	k1gInSc2	okres
Queens	Queens	k1gInSc1	Queens
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
propojení	propojení	k1gNnSc3	propojení
města	město	k1gNnSc2	město
přispělo	přispět	k5eAaPmAgNnS	přispět
otevření	otevření	k1gNnSc1	otevření
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
světovým	světový	k2eAgMnSc7d1	světový
centrem	centr	k1gMnSc7	centr
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
však	však	k9	však
postiženo	postihnout	k5eAaPmNgNnS	postihnout
několika	několik	k4yIc7	několik
tragédiemi	tragédie	k1gFnPc7	tragédie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
shořel	shořet	k5eAaPmAgInS	shořet
v	v	k7c6	v
úžině	úžina	k1gFnSc6	úžina
East	Easta	k1gFnPc2	Easta
River	Rivra	k1gFnPc2	Rivra
parník	parník	k1gInSc1	parník
General	General	k1gMnSc1	General
Slocum	Slocum	k1gInSc1	Slocum
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
si	se	k3xPyFc3	se
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
1021	[number]	k4	1021
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
požár	požár	k1gInSc1	požár
firmy	firma	k1gFnSc2	firma
Triangle	triangl	k1gInSc5	triangl
Shirtwaist	Shirtwaist	k1gInSc4	Shirtwaist
Factory	Factor	k1gInPc7	Factor
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
podlehlo	podlehnout	k5eAaPmAgNnS	podlehnout
146	[number]	k4	146
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
především	především	k9	především
šiček	šička	k1gFnPc2	šička
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tragédie	tragédie	k1gFnSc1	tragédie
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
odborů	odbor	k1gInPc2	odbor
International	International	k1gMnSc1	International
Ladies	Ladies	k1gMnSc1	Ladies
<g/>
'	'	kIx"	'
Garment	Garment	k1gInSc1	Garment
Workers	Workers	k1gInSc1	Workers
<g/>
'	'	kIx"	'
Union	union	k1gInSc1	union
a	a	k8xC	a
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
v	v	k7c6	v
továrnách	továrna	k1gFnPc6	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
cílů	cíl	k1gInPc2	cíl
stěhování	stěhování	k1gNnSc2	stěhování
černošského	černošský	k2eAgNnSc2d1	černošské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
z	z	k7c2	z
jižních	jižní	k2eAgInPc2d1	jižní
unijních	unijní	k2eAgInPc2d1	unijní
států	stát	k1gInPc2	stát
během	během	k7c2	během
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
Velké	velký	k2eAgFnSc2d1	velká
migrace	migrace	k1gFnSc2	migrace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
se	se	k3xPyFc4	se
New	New	k1gMnSc1	New
York	York	k1gInSc4	York
stal	stát	k5eAaPmAgMnS	stát
domovem	domov	k1gInSc7	domov
největší	veliký	k2eAgFnSc2d3	veliký
černošské	černošský	k2eAgFnSc2d1	černošská
komunity	komunita	k1gFnSc2	komunita
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Černošský	černošský	k2eAgInSc1d1	černošský
umělecký	umělecký	k2eAgInSc1d1	umělecký
směr	směr	k1gInSc1	směr
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
<g/>
harlemská	harlemský	k2eAgFnSc1d1	harlemský
renesance	renesance	k1gFnSc1	renesance
<g/>
"	"	kIx"	"
kvetl	kvést	k5eAaImAgMnS	kvést
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
prohibice	prohibice	k1gFnSc2	prohibice
a	a	k8xC	a
obrovského	obrovský	k2eAgInSc2d1	obrovský
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
růstu	růst	k1gInSc2	růst
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
projevoval	projevovat	k5eAaImAgInS	projevovat
výstavbou	výstavba	k1gFnSc7	výstavba
pro	pro	k7c4	pro
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
charakteristických	charakteristický	k2eAgMnPc2d1	charakteristický
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
stal	stát	k5eAaPmAgInS	stát
nejlidnatějším	lidnatý	k2eAgNnSc7d3	nejlidnatější
městem	město	k1gNnSc7	město
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
když	když	k8xS	když
předstihl	předstihnout	k5eAaPmAgInS	předstihnout
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
držel	držet	k5eAaImAgInS	držet
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
velké	velký	k2eAgFnSc2d1	velká
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
starostou	starosta	k1gMnSc7	starosta
zvolen	zvolen	k2eAgMnSc1d1	zvolen
reformátor	reformátor	k1gMnSc1	reformátor
Fiorello	Fiorello	k1gNnSc4	Fiorello
H.	H.	kA	H.
La	la	k1gNnPc2	la
Guardia	Guardia	k?	Guardia
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
osmdesátiletá	osmdesátiletý	k2eAgFnSc1d1	osmdesátiletá
nadvláda	nadvláda	k1gFnSc1	nadvláda
Tammany	Tamman	k1gInPc4	Tamman
Hall	Hallum	k1gNnPc2	Hallum
<g/>
.	.	kIx.	.
</s>
<s>
Veteráni	veterán	k1gMnPc1	veterán
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
imigranti	imigrant	k1gMnPc1	imigrant
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c6	na
poválečném	poválečný	k2eAgInSc6d1	poválečný
rozvoji	rozvoj	k1gInSc6	rozvoj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
například	například	k6eAd1	například
řady	řada	k1gFnPc1	řada
domů	dům	k1gInPc2	dům
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Queensu	Queens	k1gInSc6	Queens
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
získal	získat	k5eAaPmAgInS	získat
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
pozici	pozice	k1gFnSc4	pozice
globální	globální	k2eAgFnSc2d1	globální
metropole	metropol	k1gFnSc2	metropol
s	s	k7c7	s
Wall	Wall	k1gInSc1	Wall
Streetem	Street	k1gInSc7	Street
jako	jako	k8xC	jako
ekonomickým	ekonomický	k2eAgNnSc7d1	ekonomické
centrem	centrum	k1gNnSc7	centrum
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
sídlem	sídlo	k1gNnSc7	sídlo
OSN	OSN	kA	OSN
jako	jako	k8xC	jako
centrem	centrum	k1gNnSc7	centrum
světové	světový	k2eAgFnSc2d1	světová
diplomacie	diplomacie	k1gFnSc2	diplomacie
a	a	k8xC	a
díky	díky	k7c3	díky
rozvoji	rozvoj	k1gInSc3	rozvoj
abstraktního	abstraktní	k2eAgInSc2d1	abstraktní
expresionismu	expresionismus	k1gInSc2	expresionismus
se	se	k3xPyFc4	se
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
stalo	stát	k5eAaPmAgNnS	stát
i	i	k9	i
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
světové	světový	k2eAgFnSc2d1	světová
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
postihly	postihnout	k5eAaPmAgFnP	postihnout
město	město	k1gNnSc4	město
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
rasové	rasový	k2eAgNnSc4d1	rasové
napětí	napětí	k1gNnSc4	napětí
a	a	k8xC	a
růst	růst	k1gInSc4	růst
kriminality	kriminalita	k1gFnSc2	kriminalita
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
vrcholu	vrchol	k1gInSc3	vrchol
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1976	[number]	k4	1976
<g/>
-	-	kIx~	-
<g/>
1977	[number]	k4	1977
terorizoval	terorizovat	k5eAaImAgInS	terorizovat
město	město	k1gNnSc4	město
střelbou	střelba	k1gFnSc7	střelba
David	David	k1gMnSc1	David
Berkowitz	Berkowitz	k1gMnSc1	Berkowitz
<g/>
;	;	kIx,	;
zabil	zabít	k5eAaPmAgInS	zabít
6	[number]	k4	6
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
7	[number]	k4	7
zranil	zranit	k5eAaPmAgMnS	zranit
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
na	na	k7c4	na
doživotí	doživotí	k1gNnSc4	doživotí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzkříšení	vzkříšení	k1gNnSc1	vzkříšení
finančního	finanční	k2eAgInSc2d1	finanční
sektoru	sektor	k1gInSc2	sektor
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
zlepšit	zlepšit	k5eAaPmF	zlepšit
městský	městský	k2eAgInSc4d1	městský
rozpočet	rozpočet	k1gInSc4	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
uklidnění	uklidnění	k1gNnSc3	uklidnění
rasového	rasový	k2eAgNnSc2d1	rasové
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
k	k	k7c3	k
dramatickému	dramatický	k2eAgNnSc3d1	dramatické
snížení	snížení	k1gNnSc3	snížení
kriminality	kriminalita	k1gFnSc2	kriminalita
a	a	k8xC	a
k	k	k7c3	k
masovému	masový	k2eAgInSc3d1	masový
přílivu	příliv	k1gInSc3	příliv
imigrantů	imigrant	k1gMnPc2	imigrant
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomiku	ekonomika	k1gFnSc4	ekonomika
města	město	k1gNnSc2	město
začaly	začít	k5eAaPmAgFnP	začít
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
nové	nový	k2eAgInPc4d1	nový
obory	obor	k1gInPc4	obor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
informační	informační	k2eAgFnSc1d1	informační
technologie	technologie	k1gFnSc1	technologie
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
dle	dle	k7c2	dle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
největšího	veliký	k2eAgInSc2d3	veliký
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
teroristických	teroristický	k2eAgInPc2d1	teroristický
útoků	útok	k1gInPc2	útok
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
když	když	k8xS	když
zde	zde	k6eAd1	zde
při	při	k7c6	při
zničení	zničení	k1gNnSc6	zničení
Světového	světový	k2eAgNnSc2d1	světové
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
téměř	téměř	k6eAd1	téměř
3000	[number]	k4	3000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
útoků	útok	k1gInPc2	útok
dokončen	dokončit	k5eAaPmNgInS	dokončit
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
One	One	k1gFnSc2	One
World	Worlda	k1gFnPc2	Worlda
Trade	Trad	k1gInSc5	Trad
Center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
místních	místní	k2eAgFnPc2d1	místní
památek	památka	k1gFnPc2	památka
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
Svobody	svoboda	k1gFnSc2	svoboda
zdravila	zdravit	k5eAaImAgFnS	zdravit
miliony	milion	k4xCgInPc7	milion
imigrantů	imigrant	k1gMnPc2	imigrant
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
připlouvali	připlouvat	k5eAaImAgMnP	připlouvat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Wall	Wall	k1gMnSc1	Wall
Street	Street	k1gMnSc1	Street
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
burzy	burza	k1gFnSc2	burza
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
světovým	světový	k2eAgInSc7d1	světový
finančním	finanční	k2eAgInSc7d1	finanční
centrem	centr	k1gInSc7	centr
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
město	město	k1gNnSc4	město
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
mrakodrapy	mrakodrap	k1gInPc1	mrakodrap
patřící	patřící	k2eAgInPc1d1	patřící
stále	stále	k6eAd1	stále
mezi	mezi	k7c4	mezi
nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
Chrysler	Chrysler	k1gInSc1	Chrysler
Building	Building	k1gInSc1	Building
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
tzv.	tzv.	kA	tzv.
dvojčata	dvojče	k1gNnPc1	dvojče
ve	v	k7c6	v
Světovém	světový	k2eAgNnSc6d1	světové
obchodním	obchodní	k2eAgNnSc6d1	obchodní
centru	centrum	k1gNnSc6	centrum
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
nejlidnatějším	lidnatý	k2eAgNnSc7d3	nejlidnatější
městem	město	k1gNnSc7	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
s	s	k7c7	s
odhadovaným	odhadovaný	k2eAgInSc7d1	odhadovaný
počtem	počet	k1gInSc7	počet
8	[number]	k4	8
274	[number]	k4	274
527	[number]	k4	527
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
7,3	[number]	k4	7,3
milionu	milion	k4xCgInSc2	milion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
města	město	k1gNnSc2	město
tak	tak	k9	tak
tvoří	tvořit	k5eAaImIp3nP	tvořit
asi	asi	k9	asi
40	[number]	k4	40
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
stejný	stejný	k2eAgInSc4d1	stejný
podíl	podíl	k1gInSc4	podíl
obyvatel	obyvatel	k1gMnPc2	obyvatel
newyorské	newyorský	k2eAgFnSc2d1	newyorská
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
dekádě	dekáda	k1gFnSc6	dekáda
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
rostl	růst	k5eAaImAgMnS	růst
a	a	k8xC	a
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2030	[number]	k4	2030
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
9,2	[number]	k4	9,2
až	až	k9	až
9,5	[number]	k4	9,5
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dvěma	dva	k4xCgFnPc7	dva
základními	základní	k2eAgFnPc7d1	základní
demografickými	demografický	k2eAgFnPc7d1	demografická
charakteristikami	charakteristika	k1gFnPc7	charakteristika
jsou	být	k5eAaImIp3nP	být
hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc1	zalidnění
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc1d1	kulturní
diverzita	diverzita	k1gFnSc1	diverzita
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
hustotou	hustota	k1gFnSc7	hustota
zalidnění	zalidnění	k1gNnSc1	zalidnění
dosahující	dosahující	k2eAgFnSc4d1	dosahující
10	[number]	k4	10
194	[number]	k4	194
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
nejhustěji	husto	k6eAd3	husto
zalidněným	zalidněný	k2eAgNnSc7d1	zalidněné
městem	město	k1gNnSc7	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
nad	nad	k7c7	nad
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
66	[number]	k4	66
940	[number]	k4	940
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
amerických	americký	k2eAgFnPc2d1	americká
counties	countiesa	k1gFnPc2	countiesa
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
8,2	[number]	k4	8,2
miliony	milion	k4xCgInPc7	milion
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
žijícími	žijící	k2eAgInPc7d1	žijící
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
1	[number]	k4	1
214,4	[number]	k4	214,4
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
zároveň	zároveň	k6eAd1	zároveň
nejhustěji	husto	k6eAd3	husto
zalidněným	zalidněný	k2eAgNnSc7d1	zalidněné
městem	město	k1gNnSc7	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
kulturně	kulturně	k6eAd1	kulturně
a	a	k8xC	a
etnicky	etnicky	k6eAd1	etnicky
velmi	velmi	k6eAd1	velmi
rozmanitý	rozmanitý	k2eAgInSc1d1	rozmanitý
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
byl	být	k5eAaImAgMnS	být
místem	místo	k1gNnSc7	místo
vstupu	vstup	k1gInSc2	vstup
imigrantů	imigrant	k1gMnPc2	imigrant
na	na	k7c4	na
americkou	americký	k2eAgFnSc4d1	americká
půdu	půda	k1gFnSc4	půda
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
prošli	projít	k5eAaPmAgMnP	projít
kontrolou	kontrola	k1gFnSc7	kontrola
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
na	na	k7c6	na
Ellis	Ellis	k1gFnSc6	Ellis
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
melting	melting	k1gInSc1	melting
pot	pot	k1gInSc4	pot
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
imigranty	imigrant	k1gMnPc7	imigrant
obývaných	obývaný	k2eAgFnPc2d1	obývaná
čtvrtí	čtvrt	k1gFnPc2	čtvrt
na	na	k7c4	na
Lower	Lower	k1gInSc4	Lower
East	East	k2eAgInSc4d1	East
Side	Side	k1gInSc4	Side
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
36	[number]	k4	36
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
narozených	narozený	k2eAgNnPc2d1	narozené
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
a	a	k8xC	a
Miami	Miami	k1gNnSc2	Miami
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
třetí	třetí	k4xOgInSc1	třetí
největší	veliký	k2eAgInSc1d3	veliký
podíl	podíl	k1gInSc1	podíl
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
městech	město	k1gNnPc6	město
jsou	být	k5eAaImIp3nP	být
imigranti	imigrant	k1gMnPc1	imigrant
kulturně	kulturně	k6eAd1	kulturně
homogenní	homogenní	k2eAgFnSc1d1	homogenní
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
dominantní	dominantní	k2eAgFnSc1d1	dominantní
imigranstká	imigranstký	k2eAgFnSc1d1	imigranstký
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
současných	současný	k2eAgMnPc2d1	současný
imigrantů	imigrant	k1gMnPc2	imigrant
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
deseti	deset	k4xCc2	deset
zemí	zem	k1gFnPc2	zem
<g/>
:	:	kIx,	:
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Guyana	Guyana	k1gFnSc1	Guyana
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
<g/>
,	,	kIx,	,
Haiti	Haiti	k1gNnSc1	Haiti
<g/>
,	,	kIx,	,
Trinidad	Trinidad	k1gInSc1	Trinidad
a	a	k8xC	a
Tobago	Tobago	k1gNnSc1	Tobago
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
asi	asi	k9	asi
170	[number]	k4	170
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Newyorská	newyorský	k2eAgFnSc1d1	newyorská
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
také	také	k9	také
domovem	domov	k1gInSc7	domov
početné	početný	k2eAgFnSc2d1	početná
židovské	židovský	k2eAgFnSc2d1	židovská
komunity	komunita	k1gFnSc2	komunita
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc2d3	veliký
mimo	mimo	k7c4	mimo
Izrael	Izrael	k1gInSc4	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žádném	žádný	k3yNgNnSc6	žádný
městě	město	k1gNnSc6	město
světa	svět	k1gInSc2	svět
nežije	žít	k5eNaImIp3nS	žít
tolik	tolik	k4xDc1	tolik
Židů	Žid	k1gMnPc2	Žid
jako	jako	k8xS	jako
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
komunitě	komunita	k1gFnSc3	komunita
patří	patřit	k5eAaImIp3nS	patřit
kolem	kolem	k7c2	kolem
12	[number]	k4	12
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
New	New	k1gMnSc1	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
židovskými	židovský	k2eAgInPc7d1	židovský
předky	předek	k1gInPc7	předek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
žije	žít	k5eAaImIp3nS	žít
také	také	k9	také
asi	asi	k9	asi
jedna	jeden	k4xCgFnSc1	jeden
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
Američanů	Američan	k1gMnPc2	Američan
indického	indický	k2eAgInSc2d1	indický
původu	původ	k1gInSc2	původ
a	a	k8xC	a
největší	veliký	k2eAgFnSc1d3	veliký
černošská	černošský	k2eAgFnSc1d1	černošská
komunita	komunita	k1gFnSc1	komunita
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
existuje	existovat	k5eAaImIp3nS	existovat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
pět	pět	k4xCc1	pět
velkých	velký	k2eAgFnPc2d1	velká
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
Portorikánci	Portorikánec	k1gMnPc1	Portorikánec
<g/>
,	,	kIx,	,
Italové	Ital	k1gMnPc1	Ital
<g/>
,	,	kIx,	,
Karibové	Karib	k1gMnPc1	Karib
<g/>
,	,	kIx,	,
Dominikánci	Dominikánek	k1gMnPc1	Dominikánek
a	a	k8xC	a
Číňané	Číňan	k1gMnPc1	Číňan
<g/>
.	.	kIx.	.
</s>
<s>
Newyorská	newyorský	k2eAgFnSc1d1	newyorská
portotikánská	portotikánský	k2eAgFnSc1d1	portotikánský
komunita	komunita	k1gFnSc1	komunita
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
mimo	mimo	k7c4	mimo
Portoriko	Portoriko	k1gNnSc4	Portoriko
<g/>
.	.	kIx.	.
</s>
<s>
Italové	Ital	k1gMnPc1	Ital
přicházeli	přicházet	k5eAaImAgMnP	přicházet
do	do	k7c2	do
města	město	k1gNnSc2	město
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
počtu	počet	k1gInSc6	počet
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Výraznou	výrazný	k2eAgFnSc7d1	výrazná
a	a	k8xC	a
šestou	šestý	k4xOgFnSc4	šestý
nejpočetnější	početní	k2eAgFnSc4d3	nejpočetnější
komunitu	komunita	k1gFnSc4	komunita
tvoří	tvořit	k5eAaImIp3nP	tvořit
Irové	Ir	k1gMnPc1	Ir
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
v	v	k7c6	v
New	New	k1gFnPc6	New
Yorku	York	k1gInSc2	York
mluvilo	mluvit	k5eAaImAgNnS	mluvit
přibližně	přibližně	k6eAd1	přibližně
170	[number]	k4	170
jazyky	jazyk	k1gInPc4	jazyk
a	a	k8xC	a
36	[number]	k4	36
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
mimo	mimo	k7c4	mimo
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
existuje	existovat	k5eAaImIp3nS	existovat
silná	silný	k2eAgFnSc1d1	silná
disproporce	disproporce	k1gFnSc1	disproporce
mezi	mezi	k7c7	mezi
příjmy	příjem	k1gInPc7	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
nejchudších	chudý	k2eAgNnPc2d3	nejchudší
pět	pět	k4xCc4	pět
procent	procento	k1gNnPc2	procento
obyvatel	obyvatel	k1gMnSc1	obyvatel
Manhattanu	Manhattan	k1gInSc2	Manhattan
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
příjem	příjem	k1gInSc1	příjem
7	[number]	k4	7
047	[number]	k4	047
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
příjem	příjem	k1gInSc1	příjem
nejbohatších	bohatý	k2eAgInPc2d3	nejbohatší
pěti	pět	k4xCc2	pět
procent	procento	k1gNnPc2	procento
byl	být	k5eAaImAgInS	být
52	[number]	k4	52
krát	krát	k6eAd1	krát
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
365	[number]	k4	365
826	[number]	k4	826
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
rozdíly	rozdíl	k1gInPc1	rozdíl
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
částech	část	k1gFnPc6	část
Manhattanu	Manhattan	k1gInSc2	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
uvedeném	uvedený	k2eAgInSc6d1	uvedený
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
mediánový	mediánový	k2eAgInSc1d1	mediánový
příjem	příjem	k1gInSc1	příjem
obyvatel	obyvatel	k1gMnPc2	obyvatel
sídlících	sídlící	k2eAgMnPc2d1	sídlící
v	v	k7c6	v
šesti	šest	k4xCc6	šest
blocích	blok	k1gInPc6	blok
mezi	mezi	k7c7	mezi
ulicemi	ulice	k1gFnPc7	ulice
5	[number]	k4	5
<g/>
th	th	k?	th
Avenue	avenue	k1gFnPc2	avenue
<g/>
,	,	kIx,	,
East	Easta	k1gFnPc2	Easta
56	[number]	k4	56
<g/>
th	th	k?	th
Street	Street	k1gInSc1	Street
<g/>
,	,	kIx,	,
Park	park	k1gInSc1	park
Avenue	avenue	k1gFnSc2	avenue
a	a	k8xC	a
East	East	k1gInSc1	East
59	[number]	k4	59
<g/>
th	th	k?	th
Street	Street	k1gMnSc1	Street
188	[number]	k4	188
697	[number]	k4	697
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
v	v	k7c6	v
části	část	k1gFnSc6	část
East	Easta	k1gFnPc2	Easta
Harlemu	Harlem	k1gInSc2	Harlem
nacházející	nacházející	k2eAgInSc1d1	nacházející
se	se	k3xPyFc4	se
východně	východně	k6eAd1	východně
od	od	k7c2	od
1	[number]	k4	1
<g/>
st	st	kA	st
Avenue	avenue	k1gFnPc2	avenue
a	a	k8xC	a
severně	severně	k6eAd1	severně
od	od	k7c2	od
East	Eastum	k1gNnPc2	Eastum
119	[number]	k4	119
<g/>
th	th	k?	th
Street	Streeta	k1gFnPc2	Streeta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
tvořeno	tvořit	k5eAaImNgNnS	tvořit
převážně	převážně	k6eAd1	převážně
chudými	chudý	k2eAgMnPc7d1	chudý
černochy	černoch	k1gMnPc7	černoch
a	a	k8xC	a
Hispánci	Hispánec	k1gMnPc7	Hispánec
<g/>
,	,	kIx,	,
a	a	k8xC	a
10	[number]	k4	10
procent	procento	k1gNnPc2	procento
místních	místní	k2eAgFnPc2d1	místní
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
mimo	mimo	k7c4	mimo
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
mediánový	mediánový	k2eAgInSc1d1	mediánový
příjem	příjem	k1gInSc1	příjem
pouze	pouze	k6eAd1	pouze
9	[number]	k4	9
320	[number]	k4	320
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
příjmy	příjem	k1gInPc1	příjem
vysoce	vysoce	k6eAd1	vysoce
příjmových	příjmový	k2eAgFnPc2d1	příjmová
skupin	skupina	k1gFnPc2	skupina
rostou	růst	k5eAaImIp3nP	růst
<g/>
,	,	kIx,	,
příjmy	příjem	k1gInPc4	příjem
středně	středně	k6eAd1	středně
a	a	k8xC	a
nízce	nízce	k6eAd1	nízce
příjmových	příjmový	k2eAgFnPc2d1	příjmová
skupin	skupina	k1gFnPc2	skupina
stagnují	stagnovat	k5eAaImIp3nP	stagnovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
průměrný	průměrný	k2eAgInSc1d1	průměrný
týdenní	týdenní	k2eAgInSc1d1	týdenní
příjem	příjem	k1gInSc1	příjem
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
1	[number]	k4	1
453	[number]	k4	453
USD	USD	kA	USD
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
největší	veliký	k2eAgFnSc4d3	veliký
a	a	k8xC	a
také	také	k9	také
nejrychleji	rychle	k6eAd3	rychle
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
příjem	příjem	k1gInSc4	příjem
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
velkých	velká	k1gFnPc6	velká
counties	countiesa	k1gFnPc2	countiesa
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
probíhá	probíhat	k5eAaImIp3nS	probíhat
také	také	k9	také
baby	baba	k1gFnPc1	baba
boom	boom	k1gInSc1	boom
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
o	o	k7c6	o
32	[number]	k4	32
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
bytech	byt	k1gInPc6	byt
v	v	k7c6	v
osobním	osobní	k2eAgNnSc6d1	osobní
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
asi	asi	k9	asi
33	[number]	k4	33
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
polovina	polovina	k1gFnSc1	polovina
amerického	americký	k2eAgInSc2d1	americký
průměru	průměr	k1gInSc2	průměr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
69	[number]	k4	69
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Volných	volný	k2eAgInPc2d1	volný
bytů	byt	k1gInPc2	byt
jsou	být	k5eAaImIp3nP	být
asi	asi	k9	asi
3	[number]	k4	3
až	až	k9	až
4,5	[number]	k4	4,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
pod	pod	k7c7	pod
pětiprocentní	pětiprocentní	k2eAgFnSc7d1	pětiprocentní
hranicí	hranice	k1gFnSc7	hranice
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
které	který	k3yQgNnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nájemné	nájemné	k1gNnSc1	nájemné
regulováno	regulovat	k5eAaImNgNnS	regulovat
<g/>
.	.	kIx.	.
</s>
<s>
Regulované	regulovaný	k2eAgNnSc1d1	regulované
nájemné	nájemné	k1gNnSc1	nájemné
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
u	u	k7c2	u
33	[number]	k4	33
%	%	kIx~	%
bytů	byt	k1gInPc2	byt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
velmi	velmi	k6eAd1	velmi
problematické	problematický	k2eAgNnSc1d1	problematické
nalézt	nalézt	k5eAaBmF	nalézt
bydlení	bydlení	k1gNnSc4	bydlení
za	za	k7c4	za
akceptovatelnou	akceptovatelný	k2eAgFnSc4d1	akceptovatelná
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c6	na
půli	půle	k1gFnSc6	půle
cesty	cesta	k1gFnSc2	cesta
mezi	mezi	k7c7	mezi
Washingtonem	Washington	k1gInSc7	Washington
D.	D.	kA	D.
C.	C.	kA	C.
a	a	k8xC	a
Bostonem	Boston	k1gInSc7	Boston
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Hudson	Hudsona	k1gFnPc2	Hudsona
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zde	zde	k6eAd1	zde
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
malého	malý	k2eAgInSc2d1	malý
přírodního	přírodní	k2eAgInSc2d1	přírodní
zálivu	záliv	k1gInSc2	záliv
a	a	k8xC	a
následně	následně	k6eAd1	následně
do	do	k7c2	do
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
,	,	kIx,	,
Staten	Staten	k2eAgInSc1d1	Staten
Island	Island	k1gInSc1	Island
a	a	k8xC	a
Long	Long	k1gMnSc1	Long
Island	Island	k1gInSc4	Island
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
je	být	k5eAaImIp3nS	být
rozsah	rozsah	k1gInSc1	rozsah
dostupné	dostupný	k2eAgFnSc2d1	dostupná
půdy	půda	k1gFnSc2	půda
omezen	omezit	k5eAaPmNgMnS	omezit
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
hustotě	hustota	k1gFnSc3	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Hudson	Hudsona	k1gFnPc2	Hudsona
protéká	protékat	k5eAaImIp3nS	protékat
údolím	údolí	k1gNnSc7	údolí
Hudson	Hudsona	k1gFnPc2	Hudsona
Valley	Vallea	k1gFnSc2	Vallea
do	do	k7c2	do
newyorského	newyorský	k2eAgInSc2d1	newyorský
zálivu	záliv	k1gInSc2	záliv
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Bay	Bay	k1gFnSc1	Bay
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
New	New	k1gFnSc4	New
Yorkem	York	k1gInSc7	York
a	a	k8xC	a
městem	město	k1gNnSc7	město
Troy	Troa	k1gFnSc2	Troa
má	mít	k5eAaImIp3nS	mít
řeka	řeka	k1gFnSc1	řeka
charakter	charakter	k1gInSc1	charakter
estuáru	estuár	k1gInSc2	estuár
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Hudson	Hudsona	k1gFnPc2	Hudsona
tvoří	tvořit	k5eAaImIp3nS	tvořit
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c4	mezi
státy	stát	k1gInPc4	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
a	a	k8xC	a
New	New	k1gFnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
East	East	k2eAgInSc1d1	East
River	River	k1gInSc1	River
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Východní	východní	k2eAgFnSc1d1	východní
řeka	řeka	k1gFnSc1	řeka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
pouze	pouze	k6eAd1	pouze
přílivová	přílivový	k2eAgFnSc1d1	přílivová
úžina	úžina	k1gFnSc1	úžina
(	(	kIx(	(
<g/>
tidal	tidal	k1gMnSc1	tidal
strait	strait	k1gMnSc1	strait
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
od	od	k7c2	od
Long	Longa	k1gFnPc2	Longa
Island	Island	k1gInSc1	Island
Sound	Sound	k1gMnSc1	Sound
a	a	k8xC	a
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Bronx	Bronx	k1gInSc1	Bronx
a	a	k8xC	a
Manhattan	Manhattan	k1gInSc1	Manhattan
od	od	k7c2	od
Long	Longa	k1gFnPc2	Longa
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
</s>
<s>
Harlem	Harlo	k1gNnSc7	Harlo
River	Rivra	k1gFnPc2	Rivra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
přílivová	přílivový	k2eAgFnSc1d1	přílivová
úžina	úžina	k1gFnSc1	úžina
<g/>
,	,	kIx,	,
spojuje	spojovat	k5eAaImIp3nS	spojovat
řeku	řeka	k1gFnSc4	řeka
Hudson	Hudson	k1gMnSc1	Hudson
a	a	k8xC	a
East	East	k1gMnSc1	East
River	River	k1gMnSc1	River
<g/>
.	.	kIx.	.
</s>
<s>
Pevnina	pevnina	k1gFnSc1	pevnina
byla	být	k5eAaImAgFnS	být
neustále	neustále	k6eAd1	neustále
upravována	upravovat	k5eAaImNgFnS	upravovat
lidskou	lidský	k2eAgFnSc7d1	lidská
činností	činnost	k1gFnSc7	činnost
<g/>
,	,	kIx,	,
nejvýrazněji	výrazně	k6eAd3	výrazně
pak	pak	k6eAd1	pak
vysušováním	vysušování	k1gNnSc7	vysušování
pobřeží	pobřeží	k1gNnSc2	pobřeží
již	již	k6eAd1	již
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
kolonizace	kolonizace	k1gFnSc2	kolonizace
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
intenzívně	intenzívně	k6eAd1	intenzívně
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
děje	dít	k5eAaImIp3nS	dít
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Battery	Batter	k1gInPc7	Batter
Park	park	k1gInSc1	park
City	City	k1gFnSc3	City
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
navezením	navezení	k1gNnSc7	navezení
hlíny	hlína	k1gFnPc1	hlína
a	a	k8xC	a
kamení	kamení	k1gNnSc1	kamení
ze	z	k7c2	z
stavby	stavba	k1gFnSc2	stavba
Světového	světový	k2eAgNnSc2d1	světové
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
byly	být	k5eAaImAgFnP	být
lidskou	lidský	k2eAgFnSc7d1	lidská
činností	činnost	k1gFnSc7	činnost
zarovnány	zarovnán	k2eAgInPc1d1	zarovnán
mnohé	mnohý	k2eAgInPc1d1	mnohý
přírodně	přírodně	k6eAd1	přírodně
vzniklé	vzniklý	k2eAgFnSc3d1	vzniklá
nerovnosti	nerovnost	k1gFnSc3	nerovnost
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
je	být	k5eAaImIp3nS	být
1214,4	[number]	k4	1214,4
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
pevnina	pevnina	k1gFnSc1	pevnina
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
789,5	[number]	k4	789,5
km	km	kA	km
<g/>
2	[number]	k4	2
a	a	k8xC	a
vodní	vodní	k2eAgFnSc2d1	vodní
plochy	plocha	k1gFnSc2	plocha
rozlohu	rozloh	k1gInSc2	rozloh
428,8	[number]	k4	428,8
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
Todt	Todt	k2eAgMnSc1d1	Todt
Hill	Hill	k1gMnSc1	Hill
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Staten	Staten	k2eAgInSc1d1	Staten
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
124,9	[number]	k4	124,9
metrů	metr	k1gInPc2	metr
n.	n.	k?	n.
m.	m.	k?	m.
Vrchol	vrchol	k1gInSc1	vrchol
je	být	k5eAaImIp3nS	být
pokryt	pokryt	k2eAgInSc4d1	pokryt
lesy	les	k1gInPc4	les
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
tamějšího	tamější	k2eAgMnSc2d1	tamější
Staten	Staten	k2eAgInSc4d1	Staten
Island	Island	k1gInSc4	Island
Greenbelt	Greenbelta	k1gFnPc2	Greenbelta
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
pěti	pět	k4xCc7	pět
městskými	městský	k2eAgFnPc7d1	městská
částmi	část	k1gFnPc7	část
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
kryjí	krýt	k5eAaImIp3nP	krýt
s	s	k7c7	s
administrativní	administrativní	k2eAgFnSc7d1	administrativní
úrovní	úroveň	k1gFnSc7	úroveň
county	counta	k1gFnSc2	counta
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
desítek	desítka	k1gFnPc2	desítka
až	až	k8xS	až
stovek	stovka	k1gFnPc2	stovka
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
mnohdy	mnohdy	k6eAd1	mnohdy
svou	svůj	k3xOyFgFnSc4	svůj
specifckou	specifcký	k2eAgFnSc4d1	specifcký
atmosféru	atmosféra	k1gFnSc4	atmosféra
a	a	k8xC	a
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	k9	kdyby
městské	městský	k2eAgFnPc1d1	městská
části	část	k1gFnPc1	část
byly	být	k5eAaImAgFnP	být
samostatnými	samostatný	k2eAgNnPc7d1	samostatné
městy	město	k1gNnPc7	město
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgFnPc1	čtyři
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
(	(	kIx(	(
<g/>
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Bronx	Bronx	k1gInSc1	Bronx
<g/>
,	,	kIx,	,
Manhattan	Manhattan	k1gInSc1	Manhattan
a	a	k8xC	a
Queens	Queens	k1gInSc1	Queens
<g/>
)	)	kIx)	)
by	by	kYmCp3nP	by
patřily	patřit	k5eAaImAgFnP	patřit
mezi	mezi	k7c4	mezi
deset	deset	k4xCc4	deset
nejlidnatějších	lidnatý	k2eAgNnPc2d3	nejlidnatější
měst	město	k1gNnPc2	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Bronx	Bronx	k1gInSc1	Bronx
(	(	kIx(	(
<g/>
Bronx	Bronx	k1gInSc1	Bronx
County	Counta	k1gFnSc2	Counta
<g/>
:	:	kIx,	:
1	[number]	k4	1
373	[number]	k4	373
659	[number]	k4	659
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejsevernějším	severní	k2eAgInSc7d3	nejsevernější
newyorským	newyorský	k2eAgInSc7d1	newyorský
obvodem	obvod	k1gInSc7	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Yankee	yankee	k1gMnSc1	yankee
Stadium	stadium	k1gNnSc1	stadium
<g/>
,	,	kIx,	,
domovský	domovský	k2eAgInSc1d1	domovský
stadion	stadion	k1gInSc1	stadion
týmu	tým	k1gInSc2	tým
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Yankees	Yankees	k1gInSc1	Yankees
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
největší	veliký	k2eAgInSc4d3	veliký
družstevně	družstevně	k6eAd1	družstevně
vlastněný	vlastněný	k2eAgInSc4d1	vlastněný
obytný	obytný	k2eAgInSc4d1	obytný
komplex	komplex	k1gInSc4	komplex
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
Co-op	Cop	k1gInSc1	Co-op
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
malé	malý	k2eAgFnSc2d1	malá
části	část	k1gFnSc2	část
Manhattanu	Manhattan	k1gInSc2	Manhattan
známé	známá	k1gFnSc2	známá
jako	jako	k8xC	jako
Marble	Marble	k1gMnSc1	Marble
Hill	Hill	k1gMnSc1	Hill
je	být	k5eAaImIp3nS	být
Bronx	Bronx	k1gInSc4	Bronx
jedinou	jediný	k2eAgFnSc7d1	jediná
částí	část	k1gFnSc7	část
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
americké	americký	k2eAgFnSc6d1	americká
pevnině	pevnina	k1gFnSc6	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bronxu	Bronx	k1gInSc6	Bronx
sídlí	sídlet	k5eAaImIp3nS	sídlet
Bronx	Bronx	k1gInSc1	Bronx
Zoo	zoo	k1gFnSc2	zoo
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc2d3	veliký
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
zoo	zoo	k1gFnSc2	zoo
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
107,2	[number]	k4	107,2
ha	ha	kA	ha
a	a	k8xC	a
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
6	[number]	k4	6
000	[number]	k4	000
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Bronx	Bronx	k1gInSc1	Bronx
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
vzniku	vznik	k1gInSc2	vznik
rapu	rapa	k1gFnSc4	rapa
a	a	k8xC	a
hip-hopové	hipopový	k2eAgFnSc2d1	hip-hopová
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
(	(	kIx(	(
<g/>
Kings	Kings	k1gInSc1	Kings
County	Counta	k1gFnSc2	Counta
<g/>
:	:	kIx,	:
2528050	[number]	k4	2528050
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejlidnatější	lidnatý	k2eAgFnSc1d3	nejlidnatější
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
byl	být	k5eAaImAgInS	být
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
samostatným	samostatný	k2eAgNnSc7d1	samostatné
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
velká	velký	k2eAgFnSc1d1	velká
kulturní	kulturní	k2eAgFnSc1d1	kulturní
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc1d1	sociální
a	a	k8xC	a
etnická	etnický	k2eAgFnSc1d1	etnická
diverzita	diverzita	k1gFnSc1	diverzita
<g/>
,	,	kIx,	,
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
umělecká	umělecký	k2eAgFnSc1d1	umělecká
scéna	scéna	k1gFnSc1	scéna
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
specifických	specifický	k2eAgFnPc2d1	specifická
čtvrtí	čtvrt	k1gFnPc2	čtvrt
a	a	k8xC	a
unikátní	unikátní	k2eAgNnSc4d1	unikátní
architektonické	architektonický	k2eAgNnSc4d1	architektonické
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
jediná	jediný	k2eAgFnSc1d1	jediná
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Manhattanu	Manhattan	k1gInSc2	Manhattan
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
obchodní	obchodní	k2eAgNnSc4d1	obchodní
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Brooklynu	Brooklyn	k1gInSc3	Brooklyn
náleží	náležet	k5eAaImIp3nS	náležet
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
pláž	pláž	k1gFnSc1	pláž
a	a	k8xC	a
Coney	Conea	k1gFnSc2	Conea
Island	Island	k1gInSc1	Island
založený	založený	k2eAgInSc1d1	založený
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
rekreačních	rekreační	k2eAgFnPc2d1	rekreační
oblastí	oblast	k1gFnPc2	oblast
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
County	Counta	k1gFnSc2	Counta
<g/>
:	:	kIx,	:
1	[number]	k4	1
620	[number]	k4	620
867	[number]	k4	867
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejhustěji	husto	k6eAd3	husto
osídlená	osídlený	k2eAgFnSc1d1	osídlená
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
většina	většina	k1gFnSc1	většina
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
a	a	k8xC	a
také	také	k9	také
Central	Central	k1gFnSc1	Central
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
finančním	finanční	k2eAgInSc7d1	finanční
centrem	centr	k1gInSc7	centr
nejen	nejen	k6eAd1	nejen
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c4	mnoho
významných	významný	k2eAgFnPc2d1	významná
společností	společnost	k1gFnPc2	společnost
a	a	k8xC	a
také	také	k9	také
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Stock	Stock	k1gMnSc1	Stock
Exchange	Exchange	k1gFnSc1	Exchange
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k6eAd1	také
sídlo	sídlo	k1gNnSc1	sídlo
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
galerií	galerie	k1gFnPc2	galerie
<g/>
,	,	kIx,	,
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c4	na
Broadwayi	Broadwaye	k1gFnSc4	Broadwaye
<g/>
,	,	kIx,	,
Greenwich	Greenwich	k1gInSc4	Greenwich
Village	Villag	k1gFnSc2	Villag
a	a	k8xC	a
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc1	square
Garden	Gardna	k1gFnPc2	Gardna
<g/>
.	.	kIx.	.
</s>
<s>
Manhattan	Manhattan	k1gInSc1	Manhattan
se	se	k3xPyFc4	se
neoficiálně	neoficiálně	k6eAd1	neoficiálně
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dolní	dolní	k2eAgFnSc4d1	dolní
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc4d1	střední
a	a	k8xC	a
horní	horní	k2eAgFnSc4d1	horní
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgInSc1d1	horní
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
dělen	dělen	k2eAgMnSc1d1	dělen
Central	Central	k1gMnSc1	Central
Parkem	park	k1gInSc7	park
na	na	k7c4	na
Upper	Upper	k1gInSc4	Upper
East	East	k2eAgInSc4d1	East
Side	Side	k1gInSc4	Side
a	a	k8xC	a
Upper	Upper	k1gInSc4	Upper
West	West	k2eAgInSc1d1	West
Side	Side	k1gInSc1	Side
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
parkem	park	k1gInSc7	park
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
Harlem	Harl	k1gInSc7	Harl
<g/>
.	.	kIx.	.
</s>
<s>
Queens	Queens	k1gInSc1	Queens
(	(	kIx(	(
<g/>
Queens	Queens	k1gInSc1	Queens
County	Counta	k1gFnSc2	Counta
<g/>
:	:	kIx,	:
2	[number]	k4	2
270	[number]	k4	270
338	[number]	k4	338
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejrozlehlejší	rozlehlý	k2eAgMnSc1d3	nejrozlehlejší
a	a	k8xC	a
etnicky	etnicky	k6eAd1	etnicky
nejdiverzifikovanější	diverzifikovaný	k2eAgInSc1d3	diverzifikovaný
okres	okres	k1gInSc1	okres
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
díky	díky	k7c3	díky
rostoucí	rostoucí	k2eAgFnSc3d1	rostoucí
populaci	populace	k1gFnSc3	populace
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
stát	stát	k1gInSc1	stát
nejlidnatější	lidnatý	k2eAgInSc1d3	nejlidnatější
newyorskou	newyorský	k2eAgFnSc7d1	newyorská
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nalézalo	nalézat	k5eAaImAgNnS	nalézat
několik	několik	k4yIc1	několik
malých	malý	k2eAgNnPc2d1	malé
měst	město	k1gNnPc2	město
a	a	k8xC	a
vesnic	vesnice	k1gFnPc2	vesnice
založených	založený	k2eAgFnPc2d1	založená
Nizozemci	Nizozemec	k1gMnPc7	Nizozemec
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
především	především	k9	především
střední	střední	k2eAgFnSc1d1	střední
třída	třída	k1gFnSc1	třída
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jediný	jediný	k2eAgInSc4d1	jediný
velký	velký	k2eAgInSc4d1	velký
okres	okres	k1gInSc4	okres
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
medián	medián	k1gInSc1	medián
příjmů	příjem	k1gInPc2	příjem
černochů	černoch	k1gMnPc2	černoch
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
52	[number]	k4	52
000	[number]	k4	000
USD	USD	kA	USD
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
u	u	k7c2	u
bělochů	běloch	k1gMnPc2	běloch
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Shea	Sheum	k1gNnSc2	Sheum
Stadium	stadium	k1gNnSc1	stadium
<g/>
,	,	kIx,	,
domovský	domovský	k2eAgInSc4d1	domovský
stadion	stadion	k1gInSc4	stadion
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Mets	Metsa	k1gFnPc2	Metsa
a	a	k8xC	a
každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
koná	konat	k5eAaImIp3nS	konat
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Open	Open	k1gMnSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Queensu	Queens	k1gInSc6	Queens
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
také	také	k9	také
dvě	dva	k4xCgFnPc4	dva
největší	veliký	k2eAgFnPc4d3	veliký
newyorská	newyorský	k2eAgNnPc4d1	newyorské
letiště	letiště	k1gNnPc4	letiště
<g/>
:	:	kIx,	:
Kennedyho	Kennedy	k1gMnSc2	Kennedy
a	a	k8xC	a
LaGuardiovo	LaGuardiův	k2eAgNnSc1d1	LaGuardiův
<g/>
.	.	kIx.	.
</s>
<s>
Staten	Staten	k2eAgInSc1d1	Staten
Island	Island	k1gInSc1	Island
(	(	kIx(	(
<g/>
Richmond	Richmond	k1gInSc1	Richmond
County	Counta	k1gFnSc2	Counta
<g/>
:	:	kIx,	:
481	[number]	k4	481
613	[number]	k4	613
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
nejvýraznější	výrazný	k2eAgInSc4d3	nejvýraznější
suburbánní	suburbánní	k2eAgInSc4d1	suburbánní
charakter	charakter	k1gInSc4	charakter
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Brooklynem	Brooklyn	k1gInSc7	Brooklyn
jej	on	k3xPp3gNnSc2	on
spojuje	spojovat	k5eAaImIp3nS	spojovat
Verrazano-Narrows	Verrazano-Narrows	k1gInSc1	Verrazano-Narrows
Bridge	Bridg	k1gFnSc2	Bridg
a	a	k8xC	a
s	s	k7c7	s
Manhattanem	Manhattan	k1gInSc7	Manhattan
Staten	Staten	k2eAgInSc1d1	Staten
Island	Island	k1gInSc1	Island
Ferry	Ferro	k1gNnPc7	Ferro
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
i	i	k9	i
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgFnPc2d3	nejpopulárnější
atrakcí	atrakce	k1gFnPc2	atrakce
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nabízí	nabízet	k5eAaImIp3nS	nabízet
skvělý	skvělý	k2eAgInSc4d1	skvělý
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
Sochu	socha	k1gFnSc4	socha
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
Ellis	Ellis	k1gFnSc1	Ellis
Island	Island	k1gInSc1	Island
a	a	k8xC	a
dolní	dolní	k2eAgInSc1d1	dolní
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Staten	Staten	k2eAgInSc4d1	Staten
Islandu	Island	k1gInSc2	Island
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
parky	park	k1gInPc1	park
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
25	[number]	k4	25
km2	km2	k4	km2
s	s	k7c7	s
56	[number]	k4	56
km	km	kA	km
turistických	turistický	k2eAgFnPc2d1	turistická
stezek	stezka	k1gFnPc2	stezka
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
posledních	poslední	k2eAgInPc2d1	poslední
nedotčených	dotčený	k2eNgInPc2d1	nedotčený
lesů	les	k1gInPc2	les
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
začleněny	začlenit	k5eAaPmNgInP	začlenit
do	do	k7c2	do
systému	systém	k1gInSc2	systém
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
nalézá	nalézat	k5eAaImIp3nS	nalézat
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
New	New	k1gFnSc4	New
York	York	k1gInSc4	York
typické	typický	k2eAgNnSc4d1	typické
vlhké	vlhký	k2eAgNnSc4d1	vlhké
klima	klima	k1gNnSc4	klima
(	(	kIx(	(
<g/>
Köppenova	Köppenův	k2eAgFnSc1d1	Köppenova
klasifikace	klasifikace	k1gFnSc1	klasifikace
podnebí	podnebí	k1gNnSc2	podnebí
-	-	kIx~	-
Dfa	Dfa	k1gFnSc1	Dfa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zimy	zima	k1gFnPc1	zima
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
studené	studený	k2eAgFnSc2d1	studená
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
umístění	umístění	k1gNnSc3	umístění
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
jsou	být	k5eAaImIp3nP	být
zdejší	zdejší	k2eAgFnPc1d1	zdejší
teploty	teplota	k1gFnPc1	teplota
mírně	mírně	k6eAd1	mírně
vyšší	vysoký	k2eAgFnPc1d2	vyšší
než	než	k8xS	než
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
zde	zde	k6eAd1	zde
napadne	napadnout	k5eAaPmIp3nS	napadnout
63,5	[number]	k4	63,5
až	až	k9	až
88,9	[number]	k4	88,9
cm	cm	kA	cm
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
mezi	mezi	k7c7	mezi
posledním	poslední	k2eAgInSc7d1	poslední
mrazivým	mrazivý	k2eAgInSc7d1	mrazivý
dnem	den	k1gInSc7	den
a	a	k8xC	a
prvním	první	k4xOgInSc7	první
mrazivým	mrazivý	k2eAgInSc7d1	mrazivý
dnem	den	k1gInSc7	den
další	další	k2eAgFnSc2d1	další
zimy	zima	k1gFnSc2	zima
trvá	trvat	k5eAaImIp3nS	trvat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
199	[number]	k4	199
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Jara	Jara	k1gFnSc1	Jara
a	a	k8xC	a
podzimy	podzim	k1gInPc1	podzim
jsou	být	k5eAaImIp3nP	být
v	v	k7c4	v
New	New	k1gFnSc4	New
Yorku	York	k1gInSc2	York
různorodá	různorodý	k2eAgFnSc1d1	různorodá
<g/>
,	,	kIx,	,
od	od	k7c2	od
sněžných	sněžný	k2eAgInPc2d1	sněžný
a	a	k8xC	a
studených	studený	k2eAgInPc2d1	studený
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
horké	horký	k2eAgFnPc4d1	horká
a	a	k8xC	a
suché	suchý	k2eAgFnPc4d1	suchá
až	až	k6eAd1	až
po	po	k7c6	po
chladné	chladný	k2eAgFnSc6d1	chladná
a	a	k8xC	a
deštivé	deštivý	k2eAgFnSc6d1	deštivá
<g/>
.	.	kIx.	.
</s>
<s>
Léta	léto	k1gNnPc1	léto
jsou	být	k5eAaImIp3nP	být
horká	horký	k2eAgNnPc1d1	horké
a	a	k8xC	a
suchá	suchý	k2eAgNnPc1d1	suché
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
léto	léto	k1gNnSc1	léto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
v	v	k7c4	v
18	[number]	k4	18
až	až	k9	až
25	[number]	k4	25
dnech	den	k1gInPc6	den
dosažena	dosáhnout	k5eAaPmNgFnS	dosáhnout
teplota	teplota	k1gFnSc1	teplota
32	[number]	k4	32
°	°	k?	°
<g/>
C.	C.	kA	C.
Ačkoliv	ačkoliv	k8xS	ačkoliv
pro	pro	k7c4	pro
New	New	k1gFnPc4	New
York	York	k1gInSc4	York
nejsou	být	k5eNaImIp3nP	být
typické	typický	k2eAgInPc1d1	typický
hurikány	hurikán	k1gInPc1	hurikán
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
jej	on	k3xPp3gInSc4	on
výjimečně	výjimečně	k6eAd1	výjimečně
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
například	například	k6eAd1	například
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1821	[number]	k4	1821
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
zaplaven	zaplaven	k2eAgInSc1d1	zaplaven
dolní	dolní	k2eAgInSc1d1	dolní
Manhattan	Manhattan	k1gInSc1	Manhattan
a	a	k8xC	a
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
poničena	poničen	k2eAgFnSc1d1	poničena
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
vliv	vliv	k1gInSc1	vliv
na	na	k7c6	na
podnebí	podnebí	k1gNnSc6	podnebí
má	mít	k5eAaImIp3nS	mít
Atlantická	atlantický	k2eAgFnSc1d1	Atlantická
multidekadická	multidekadický	k2eAgFnSc1d1	multidekadická
oscilace	oscilace	k1gFnSc1	oscilace
(	(	kIx(	(
<g/>
Atlantic	Atlantice	k1gFnPc2	Atlantice
Multidecadal	Multidecadal	k1gFnPc2	Multidecadal
Oscillation	Oscillation	k1gInSc1	Oscillation
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
sedmdesátiletý	sedmdesátiletý	k2eAgInSc1d1	sedmdesátiletý
cyklus	cyklus	k1gInSc1	cyklus
oteplování	oteplování	k1gNnSc2	oteplování
a	a	k8xC	a
ochlazování	ochlazování	k1gNnSc2	ochlazování
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
například	například	k6eAd1	například
na	na	k7c4	na
frekvenci	frekvence	k1gFnSc4	frekvence
a	a	k8xC	a
sílu	síla	k1gFnSc4	síla
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
bouří	bouř	k1gFnPc2	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
využívána	využívat	k5eAaImNgFnS	využívat
nejvíce	hodně	k6eAd3	hodně
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Spotřeba	spotřeba	k1gFnSc1	spotřeba
benzínu	benzín	k1gInSc2	benzín
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
na	na	k7c4	na
jaké	jaký	k3yRgNnSc4	jaký
byl	být	k5eAaImAgInS	být
celostátní	celostátní	k2eAgInSc1d1	celostátní
průměr	průměr	k1gInSc1	průměr
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
používání	používání	k1gNnSc3	používání
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
ušetřeno	ušetřit	k5eAaPmNgNnS	ušetřit
7	[number]	k4	7
miliard	miliarda	k4xCgFnPc2	miliarda
litrů	litr	k1gInPc2	litr
benzínu	benzín	k1gInSc2	benzín
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
polovina	polovina	k1gFnSc1	polovina
benzínu	benzín	k1gInSc2	benzín
ušetřeného	ušetřený	k2eAgInSc2d1	ušetřený
díky	díky	k7c3	díky
používání	používání	k1gNnSc3	používání
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vysokému	vysoký	k2eAgNnSc3d1	vysoké
zalidnění	zalidnění	k1gNnSc3	zalidnění
<g/>
,	,	kIx,	,
nízkému	nízký	k2eAgNnSc3d1	nízké
využívání	využívání	k1gNnSc3	využívání
automobilů	automobil	k1gInPc2	automobil
a	a	k8xC	a
vysokému	vysoký	k2eAgNnSc3d1	vysoké
využívání	využívání	k1gNnSc3	využívání
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
je	být	k5eAaImIp3nS	být
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
energeticky	energeticky	k6eAd1	energeticky
nejefektivnějších	efektivní	k2eAgNnPc2d3	nejefektivnější
měst	město	k1gNnPc2	město
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
emise	emise	k1gFnPc1	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc2	jeden
obyvatele	obyvatel	k1gMnSc2	obyvatel
města	město	k1gNnSc2	město
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
7,1	[number]	k4	7,1
tuny	tuna	k1gFnPc4	tuna
v	v	k7c4	v
porovnání	porovnání	k1gNnSc4	porovnání
s	s	k7c7	s
celostátním	celostátní	k2eAgInSc7d1	celostátní
průměrem	průměr	k1gInSc7	průměr
24,5	[number]	k4	24,5
tuny	tuna	k1gFnSc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
je	být	k5eAaImIp3nS	být
vyprodukováno	vyprodukovat	k5eAaPmNgNnS	vyprodukovat
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc1	jeden
procento	procento	k1gNnSc1	procento
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
vyprodukovaných	vyprodukovaný	k2eAgInPc2d1	vyprodukovaný
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
2,7	[number]	k4	2,7
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgMnSc1d1	průměrný
obyvatel	obyvatel	k1gMnSc1	obyvatel
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
jen	jen	k9	jen
polovinu	polovina	k1gFnSc4	polovina
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
průměrným	průměrný	k2eAgMnSc7d1	průměrný
obyvatelem	obyvatel	k1gMnSc7	obyvatel
San	San	k1gFnSc2	San
Francisca	Francisc	k1gInSc2	Francisc
a	a	k8xC	a
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
průměrným	průměrný	k2eAgMnSc7d1	průměrný
obyvatelem	obyvatel	k1gMnSc7	obyvatel
Dallasu	Dallas	k1gInSc2	Dallas
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
zaměřilo	zaměřit	k5eAaPmAgNnS	zaměřit
na	na	k7c4	na
snižování	snižování	k1gNnSc4	snižování
dopadů	dopad	k1gInPc2	dopad
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
míra	míra	k1gFnSc1	míra
znečištění	znečištění	k1gNnSc2	znečištění
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
astmatu	astma	k1gNnSc2	astma
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
nemocí	nemoc	k1gFnPc2	nemoc
dýchacího	dýchací	k2eAgNnSc2d1	dýchací
ústrojí	ústrojí	k1gNnSc2	ústrojí
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Politikou	politika	k1gFnSc7	politika
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
kupovat	kupovat	k5eAaImF	kupovat
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
budov	budova	k1gFnPc2	budova
energeticky	energeticky	k6eAd1	energeticky
efektivní	efektivní	k2eAgNnSc4d1	efektivní
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
také	také	k9	také
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
nejvíce	hodně	k6eAd3	hodně
autobusů	autobus	k1gInPc2	autobus
na	na	k7c4	na
zemní	zemní	k2eAgInSc4d1	zemní
plyn	plyn	k1gInSc4	plyn
a	a	k8xC	a
s	s	k7c7	s
hybridními	hybridní	k2eAgInPc7d1	hybridní
motory	motor	k1gInPc7	motor
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
objevily	objevit	k5eAaPmAgFnP	objevit
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
taxíků	taxík	k1gInPc2	taxík
s	s	k7c7	s
hybridním	hybridní	k2eAgInSc7d1	hybridní
pohonem	pohon	k1gInSc7	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
žalobců	žalobce	k1gMnPc2	žalobce
v	v	k7c6	v
přelomovém	přelomový	k2eAgInSc6d1	přelomový
sporu	spor	k1gInSc6	spor
u	u	k7c2	u
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
Supreme	Suprem	k1gInSc5	Suprem
Court	Courta	k1gFnPc2	Courta
of	of	k?	of
the	the	k?	the
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
Massachusetts	Massachusetts	k1gNnSc2	Massachusetts
v.	v.	k?	v.
Environmental	Environmental	k1gMnSc1	Environmental
Protection	Protection	k1gInSc4	Protection
Agency	Agenca	k1gFnSc2	Agenca
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
byla	být	k5eAaImAgFnS	být
Agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
EPA	EPA	kA	EPA
<g/>
)	)	kIx)	)
donucena	donucen	k2eAgFnSc1d1	donucena
regulovat	regulovat	k5eAaImF	regulovat
vypouštění	vypouštění	k1gNnSc4	vypouštění
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
také	také	k9	také
světovým	světový	k2eAgMnSc7d1	světový
lídrem	lídr	k1gMnSc7	lídr
v	v	k7c6	v
budování	budování	k1gNnSc6	budování
energeticky	energeticky	k6eAd1	energeticky
efektivních	efektivní	k2eAgFnPc2d1	efektivní
kancelářských	kancelářský	k2eAgFnPc2d1	kancelářská
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
typickým	typický	k2eAgMnSc7d1	typický
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
Hearst	Hearst	k1gMnSc1	Hearst
Tower	Tower	k1gMnSc1	Tower
<g/>
.	.	kIx.	.
</s>
<s>
Pitnou	pitný	k2eAgFnSc4d1	pitná
vodu	voda	k1gFnSc4	voda
získává	získávat	k5eAaImIp3nS	získávat
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
z	z	k7c2	z
Catskill	Catskilla	k1gFnPc2	Catskilla
Mountains	Mountainsa	k1gFnPc2	Mountainsa
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
<g/>
,	,	kIx,	,
že	že	k8xS	že
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
další	další	k2eAgFnPc4d1	další
chemické	chemický	k2eAgFnPc4d1	chemická
úpravy	úprava	k1gFnPc4	úprava
před	před	k7c7	před
distribucí	distribuce	k1gFnSc7	distribuce
spotřebitelům	spotřebitel	k1gMnPc3	spotřebitel
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
jen	jen	k9	jen
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
čtyřech	čtyři	k4xCgNnPc6	čtyři
velkých	velký	k2eAgNnPc6d1	velké
amerických	americký	k2eAgNnPc6d1	americké
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
znakem	znak	k1gInSc7	znak
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
je	být	k5eAaImIp3nS	být
nesmírné	smírný	k2eNgNnSc1d1	nesmírné
množství	množství	k1gNnSc1	množství
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pak	pak	k6eAd1	pak
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
asi	asi	k9	asi
5600	[number]	k4	5600
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
a	a	k8xC	a
48	[number]	k4	48
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
200	[number]	k4	200
m	m	kA	m
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
počet	počet	k1gInSc1	počet
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
kancelářských	kancelářský	k2eAgInPc2d1	kancelářský
nebo	nebo	k8xC	nebo
obytných	obytný	k2eAgInPc2d1	obytný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
především	především	k9	především
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hustotou	hustota	k1gFnSc7	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
<g/>
,	,	kIx,	,
omezenou	omezený	k2eAgFnSc7d1	omezená
rozlohou	rozloha	k1gFnSc7	rozloha
dostupných	dostupný	k2eAgInPc2d1	dostupný
pozemků	pozemek	k1gInPc2	pozemek
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
jejich	jejich	k3xOp3gFnSc7	jejich
vysokou	vysoký	k2eAgFnSc7d1	vysoká
cenou	cena	k1gFnSc7	cena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
výrazné	výrazný	k2eAgFnSc2d1	výrazná
budovy	budova	k1gFnSc2	budova
mnoha	mnoho	k4c2	mnoho
architektonických	architektonický	k2eAgInPc2d1	architektonický
stylů	styl	k1gInPc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Woolworth	Woolworth	k1gInSc1	Woolworth
Building	Building	k1gInSc1	Building
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
novogotickém	novogotický	k2eAgInSc6d1	novogotický
stylu	styl	k1gInSc6	styl
s	s	k7c7	s
masivními	masivní	k2eAgInPc7d1	masivní
prvky	prvek	k1gInPc7	prvek
gotické	gotický	k2eAgFnSc2d1	gotická
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
rozpoznatelné	rozpoznatelný	k2eAgInPc1d1	rozpoznatelný
z	z	k7c2	z
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
desítky	desítka	k1gFnPc4	desítka
metrů	metr	k1gInPc2	metr
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
omezil	omezit	k5eAaPmAgInS	omezit
výšku	výška	k1gFnSc4	výška
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
stavební	stavební	k2eAgFnSc2d1	stavební
parcely	parcela	k1gFnSc2	parcela
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sluneční	sluneční	k2eAgInSc1d1	sluneční
svit	svit	k1gInSc1	svit
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
na	na	k7c4	na
ulice	ulice	k1gFnPc4	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgMnSc7d1	typický
zástupcem	zástupce	k1gMnSc7	zástupce
stylu	styl	k1gInSc2	styl
Art	Art	k1gFnSc2	Art
Deco	Deco	k6eAd1	Deco
je	být	k5eAaImIp3nS	být
Chrysler	Chrysler	k1gInSc1	Chrysler
Building	Building	k1gInSc1	Building
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
charakteristickou	charakteristický	k2eAgFnSc7d1	charakteristická
zužující	zužující	k2eAgFnSc7d1	zužující
se	se	k3xPyFc4	se
špičkou	špička	k1gFnSc7	špička
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
architekti	architekt	k1gMnPc1	architekt
a	a	k8xC	a
historici	historik	k1gMnPc1	historik
považují	považovat	k5eAaImIp3nP	považovat
Chrysler	Chrysler	k1gInSc4	Chrysler
Building	Building	k1gInSc1	Building
za	za	k7c4	za
architektonicky	architektonicky	k6eAd1	architektonicky
nejhodnotnější	hodnotný	k2eAgInSc4d3	nejhodnotnější
mrakodrap	mrakodrap	k1gInSc4	mrakodrap
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
styl	styl	k1gInSc1	styl
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
Seagram	Seagram	k1gInSc1	Seagram
Building	Building	k1gInSc1	Building
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Condé	Condý	k2eAgNnSc4d1	Condé
Nast	Nast	k2eAgInSc4d1	Nast
Building	Building	k1gInSc4	Building
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zástupcem	zástupce	k1gMnSc7	zástupce
energeticky	energeticky	k6eAd1	energeticky
efektivních	efektivní	k2eAgFnPc2d1	efektivní
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obytné	obytný	k2eAgFnPc4d1	obytná
oblasti	oblast	k1gFnPc4	oblast
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc1d1	typický
honosné	honosný	k2eAgInPc1d1	honosný
řadové	řadový	k2eAgInPc1d1	řadový
domy	dům	k1gInPc1	dům
postavené	postavený	k2eAgInPc1d1	postavený
z	z	k7c2	z
hnědého	hnědý	k2eAgInSc2d1	hnědý
pískovce	pískovec	k1gInSc2	pískovec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
omšelé	omšelý	k2eAgInPc4d1	omšelý
bytové	bytový	k2eAgInPc4d1	bytový
domy	dům	k1gInPc4	dům
postavené	postavený	k2eAgInPc4d1	postavený
v	v	k7c6	v
období	období	k1gNnSc6	období
největšího	veliký	k2eAgInSc2d3	veliký
rozvoje	rozvoj	k1gInSc2	rozvoj
města	město	k1gNnSc2	město
mezi	mezi	k7c4	mezi
lety	let	k1gInPc4	let
1870	[number]	k4	1870
a	a	k8xC	a
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Nejpoužívanějšími	používaný	k2eAgInPc7d3	nejpoužívanější
stavebními	stavební	k2eAgInPc7d1	stavební
materiály	materiál	k1gInPc7	materiál
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
velkého	velký	k2eAgInSc2d1	velký
požáru	požár	k1gInSc2	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
kámen	kámen	k1gInSc4	kámen
a	a	k8xC	a
cihly	cihla	k1gFnPc4	cihla
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgInPc1d1	stavební
materiály	materiál	k1gInPc1	materiál
nebyly	být	k5eNaImAgInP	být
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Paříže	Paříž	k1gFnSc2	Paříž
těženy	těžen	k2eAgFnPc1d1	těžena
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
podloží	podloží	k1gNnSc2	podloží
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
dováženy	dovážit	k5eAaPmNgFnP	dovážit
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
vzdálených	vzdálený	k2eAgInPc2d1	vzdálený
kamenolomů	kamenolom	k1gInPc2	kamenolom
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
mají	mít	k5eAaImIp3nP	mít
odlišné	odlišný	k2eAgInPc4d1	odlišný
odstíny	odstín	k1gInPc4	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Specifikem	specifikon	k1gNnSc7	specifikon
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
je	být	k5eAaImIp3nS	být
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
vodních	vodní	k2eAgFnPc2d1	vodní
nádrží	nádrž	k1gFnPc2	nádrž
umístěných	umístěný	k2eAgFnPc2d1	umístěná
na	na	k7c6	na
střechách	střecha	k1gFnPc6	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
instalace	instalace	k1gFnSc1	instalace
byla	být	k5eAaImAgFnS	být
vyžadována	vyžadovat	k5eAaImNgFnS	vyžadovat
u	u	k7c2	u
budov	budova	k1gFnPc2	budova
majících	mající	k2eAgFnPc2d1	mající
přes	přes	k7c4	přes
šest	šest	k4xCc4	šest
pater	patro	k1gNnPc2	patro
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
omezen	omezit	k5eAaPmNgInS	omezit
tlak	tlak	k1gInSc1	tlak
vody	voda	k1gFnSc2	voda
ve	v	k7c6	v
vodovodech	vodovod	k1gInPc6	vodovod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
vysokým	vysoký	k2eAgInSc7d1	vysoký
tlakem	tlak	k1gInSc7	tlak
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
poškozeny	poškodit	k5eAaPmNgFnP	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
do	do	k7c2	do
módy	móda	k1gFnSc2	móda
dostalo	dostat	k5eAaPmAgNnS	dostat
bydlení	bydlení	k1gNnSc1	bydlení
v	v	k7c6	v
domech	dům	k1gInPc6	dům
v	v	k7c6	v
odlehlých	odlehlý	k2eAgFnPc6d1	odlehlá
oblastech	oblast	k1gFnPc6	oblast
města	město	k1gNnSc2	město
vybudovaných	vybudovaný	k2eAgInPc2d1	vybudovaný
na	na	k7c6	na
základě	základ	k1gInSc6	základ
idejí	idea	k1gFnPc2	idea
hnutí	hnutí	k1gNnSc1	hnutí
Garden	Gardna	k1gFnPc2	Gardna
city	city	k1gFnPc2	city
movement	movement	k1gInSc1	movement
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c4	na
Jackson	Jackson	k1gNnSc4	Jackson
Heights	Heightsa	k1gFnPc2	Heightsa
v	v	k7c6	v
Queensu	Queens	k1gInSc6	Queens
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
dostupnými	dostupný	k2eAgFnPc7d1	dostupná
díky	díky	k7c3	díky
výstavbě	výstavba	k1gFnSc3	výstavba
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
kultura	kultura	k1gFnSc1	kultura
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
počasí	počasí	k1gNnSc2	počasí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgMnS	napsat
o	o	k7c6	o
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
spisovatel	spisovatel	k1gMnSc1	spisovatel
Tom	Tom	k1gMnSc1	Tom
Wolfe	Wolf	k1gMnSc5	Wolf
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgFnPc2d1	významná
amerických	americký	k2eAgFnPc2d1	americká
kulturních	kulturní	k2eAgFnPc2d1	kulturní
hnutí	hnutí	k1gNnPc4	hnutí
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Harlem	Harl	k1gInSc7	Harl
Renaissance	Renaissance	k1gFnSc2	Renaissance
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
formovala	formovat	k5eAaImAgFnS	formovat
černošskou	černošský	k2eAgFnSc4d1	černošská
literaturu	literatura	k1gFnSc4	literatura
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
centrem	centr	k1gInSc7	centr
jazzového	jazzový	k2eAgNnSc2d1	jazzové
hnutí	hnutí	k1gNnSc2	hnutí
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
abstraktního	abstraktní	k2eAgInSc2d1	abstraktní
expresionismu	expresionismus	k1gInSc2	expresionismus
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
místem	místo	k1gNnSc7	místo
zrodu	zrod	k1gInSc2	zrod
hip-hopu	hipop	k1gInSc2	hip-hop
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
punková	punkový	k2eAgFnSc1d1	punková
a	a	k8xC	a
hardcore	hardcor	k1gInSc5	hardcor
scéna	scéna	k1gFnSc1	scéna
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
vrcholného	vrcholný	k2eAgInSc2d1	vrcholný
vlivu	vliv	k1gInSc2	vliv
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
židovská	židovský	k2eAgFnSc1d1	židovská
americká	americký	k2eAgFnSc1d1	americká
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
populárních	populární	k2eAgMnPc2d1	populární
indie	indie	k1gFnPc1	indie
rockových	rockový	k2eAgFnPc2d1	rocková
kapel	kapela	k1gFnPc2	kapela
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
The	The	k1gMnSc1	The
Strokes	Strokes	k1gMnSc1	Strokes
<g/>
,	,	kIx,	,
Interpol	interpol	k1gInSc1	interpol
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Bravery	Bravera	k1gFnSc2	Bravera
<g/>
,	,	kIx,	,
Scissor	Scissor	k1gInSc4	Scissor
Sisters	Sistersa	k1gFnPc2	Sistersa
a	a	k8xC	a
They	Thea	k1gFnSc2	Thea
Might	Mighta	k1gFnPc2	Mighta
Be	Be	k1gFnSc7	Be
Giants	Giantsa	k1gFnPc2	Giantsa
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
místo	místo	k1gNnSc4	místo
zrodu	zrod	k1gInSc2	zrod
mnoha	mnoho	k4c2	mnoho
kulturních	kulturní	k2eAgFnPc2d1	kulturní
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
harlemské	harlemský	k2eAgFnSc2d1	harlemský
renesance	renesance	k1gFnSc2	renesance
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
abstraktního	abstraktní	k2eAgInSc2d1	abstraktní
expresionismu	expresionismus	k1gInSc2	expresionismus
(	(	kIx(	(
<g/>
známého	známý	k2eAgMnSc2d1	známý
také	také	k9	také
jako	jako	k8xC	jako
Newyorská	newyorský	k2eAgFnSc1d1	newyorská
škola	škola	k1gFnSc1	škola
<g/>
)	)	kIx)	)
v	v	k7c6	v
malířství	malířství	k1gNnSc6	malířství
<g/>
,	,	kIx,	,
hip	hip	k0	hip
hopu	hopu	k6eAd1	hopu
<g/>
,	,	kIx,	,
punku	punk	k1gInSc2	punk
<g/>
,	,	kIx,	,
salsy	salsa	k1gFnSc2	salsa
a	a	k8xC	a
Tin	Tina	k1gFnPc2	Tina
Pan	Pan	k1gMnSc1	Pan
Alley	Allea	k1gFnSc2	Allea
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
kulturní	kulturní	k2eAgInSc1d1	kulturní
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgNnPc1d1	významné
střediska	středisko	k1gNnPc1	středisko
jsou	být	k5eAaImIp3nP	být
mj.	mj.	kA	mj.
Lincoln	Lincoln	k1gMnSc1	Lincoln
Center	centrum	k1gNnPc2	centrum
a	a	k8xC	a
Rockefeller	Rockefeller	k1gMnSc1	Rockefeller
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
blízkosti	blízkost	k1gFnSc6	blízkost
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
populární	populární	k2eAgNnSc1d1	populární
Radio	radio	k1gNnSc1	radio
City	City	k1gFnSc2	City
Hall	Halla	k1gFnPc2	Halla
<g/>
.	.	kIx.	.
</s>
<s>
Světoznámá	světoznámý	k2eAgFnSc1d1	světoznámá
jsou	být	k5eAaImIp3nP	být
divadla	divadlo	k1gNnPc4	divadlo
na	na	k7c6	na
Broadwayi	Broadway	k1gFnSc6	Broadway
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
uvádějí	uvádět	k5eAaImIp3nP	uvádět
činohry	činohra	k1gFnPc1	činohra
a	a	k8xC	a
muzikály	muzikál	k1gInPc1	muzikál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
40	[number]	k4	40
divadel	divadlo	k1gNnPc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
dotuje	dotovat	k5eAaBmIp3nS	dotovat
místní	místní	k2eAgFnSc4d1	místní
kulturu	kultura	k1gFnSc4	kultura
více	hodně	k6eAd2	hodně
financemi	finance	k1gFnPc7	finance
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
roční	roční	k2eAgInSc4d1	roční
rozpočet	rozpočet	k1gInSc4	rozpočet
National	National	k1gMnSc1	National
Endowment	Endowment	k1gMnSc1	Endowment
for	forum	k1gNnPc2	forum
the	the	k?	the
Arts	Artsa	k1gFnPc2	Artsa
<g/>
.	.	kIx.	.
</s>
<s>
Bohatí	bohatý	k2eAgMnPc1d1	bohatý
průmyslníci	průmyslník	k1gMnPc1	průmyslník
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
síť	síť	k1gFnSc4	síť
kulturních	kulturní	k2eAgNnPc2d1	kulturní
center	centrum	k1gNnPc2	centrum
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Metropolitan	metropolitan	k1gInSc4	metropolitan
Opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
koncertní	koncertní	k2eAgFnSc1d1	koncertní
síň	síň	k1gFnSc1	síň
Carnegie	Carnegie	k1gFnSc2	Carnegie
Hall	Halla	k1gFnPc2	Halla
(	(	kIx(	(
<g/>
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
a	a	k8xC	a
Metropolitní	metropolitní	k2eAgNnSc1d1	metropolitní
muzeum	muzeum	k1gNnSc1	muzeum
umění	umění	k1gNnSc1	umění
(	(	kIx(	(
<g/>
Metropolitan	metropolitan	k1gInSc1	metropolitan
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gMnSc1	Art
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
celosvětově	celosvětově	k6eAd1	celosvětově
proslavené	proslavený	k2eAgFnPc1d1	proslavená
<g/>
.	.	kIx.	.
</s>
<s>
Neméně	málo	k6eNd2	málo
slavné	slavný	k2eAgFnPc1d1	slavná
instituce	instituce	k1gFnPc1	instituce
jsou	být	k5eAaImIp3nP	být
Muzeum	muzeum	k1gNnSc4	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Modern	Modern	k1gMnSc1	Modern
Art	Art	k1gMnSc1	Art
<g/>
,	,	kIx,	,
MoMA	MoMA	k1gMnSc1	MoMA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
a	a	k8xC	a
Guggenheimovo	Guggenheimův	k2eAgNnSc1d1	Guggenheimovo
muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
Guggenheim	Guggenheim	k1gInSc1	Guggenheim
Museum	museum	k1gNnSc1	museum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
Brooklyn	Brooklyn	k1gInSc4	Brooklyn
Museum	museum	k1gNnSc4	museum
se	s	k7c7	s
sbírkou	sbírka	k1gFnSc7	sbírka
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
uměleckých	umělecký	k2eAgMnPc2d1	umělecký
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
menšími	malý	k2eAgFnPc7d2	menší
muzejními	muzejní	k2eAgFnPc7d1	muzejní
institucemi	instituce	k1gFnPc7	instituce
vyniká	vynikat	k5eAaImIp3nS	vynikat
mj.	mj.	kA	mj.
Frick	Frick	k1gInSc1	Frick
Collection	Collection	k1gInSc1	Collection
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
ocelářským	ocelářský	k2eAgMnSc7d1	ocelářský
magnátem	magnát	k1gMnSc7	magnát
Frickem	Fricek	k1gMnSc7	Fricek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
lze	lze	k6eAd1	lze
poznat	poznat	k5eAaPmF	poznat
vzácná	vzácný	k2eAgNnPc4d1	vzácné
malířská	malířský	k2eAgNnPc4d1	malířské
díla	dílo	k1gNnPc4	dílo
starých	starý	k2eAgMnPc2d1	starý
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Elektrifikace	elektrifikace	k1gFnSc1	elektrifikace
města	město	k1gNnSc2	město
umožnila	umožnit	k5eAaPmAgFnS	umožnit
produkci	produkce	k1gFnSc3	produkce
propracovaných	propracovaný	k2eAgNnPc2d1	propracované
divadelních	divadelní	k2eAgNnPc2d1	divadelní
představení	představení	k1gNnPc2	představení
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
divadla	divadlo	k1gNnPc4	divadlo
na	na	k7c6	na
Broadwayi	Broadway	k1gFnSc6	Broadway
a	a	k8xC	a
podél	podél	k7c2	podél
42	[number]	k4	42
<g/>
.	.	kIx.	.
ulice	ulice	k1gFnSc2	ulice
(	(	kIx(	(
<g/>
42	[number]	k4	42
<g/>
nd	nd	k?	nd
Street	Streeta	k1gFnPc2	Streeta
<g/>
)	)	kIx)	)
výkladní	výkladní	k2eAgFnSc7d1	výkladní
skříní	skříň	k1gFnSc7	skříň
komediálního	komediální	k2eAgMnSc2d1	komediální
<g/>
,	,	kIx,	,
dramatického	dramatický	k2eAgNnSc2d1	dramatické
a	a	k8xC	a
také	také	k9	také
muzikálového	muzikálový	k2eAgNnSc2d1	muzikálové
divadelního	divadelní	k2eAgNnSc2d1	divadelní
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Představení	představení	k1gNnPc1	představení
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
od	od	k7c2	od
Harrigana	Harrigan	k1gMnSc2	Harrigan
a	a	k8xC	a
Harta	Hart	k1gMnSc2	Hart
nebo	nebo	k8xC	nebo
od	od	k7c2	od
George	Georg	k1gMnSc2	Georg
M.	M.	kA	M.
Cohana	Cohan	k1gMnSc2	Cohan
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
ovlivněna	ovlivněn	k2eAgFnSc1d1	ovlivněna
místními	místní	k2eAgMnPc7d1	místní
imigranty	imigrant	k1gMnPc7	imigrant
a	a	k8xC	a
často	často	k6eAd1	často
odrážela	odrážet	k5eAaImAgNnP	odrážet
témata	téma	k1gNnPc1	téma
jejich	jejich	k3xOp3gFnPc2	jejich
nadějí	naděje	k1gFnPc2	naděje
a	a	k8xC	a
ambicí	ambice	k1gFnPc2	ambice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
tvoří	tvořit	k5eAaImIp3nP	tvořit
muzikály	muzikál	k1gInPc1	muzikál
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
newyorské	newyorský	k2eAgFnSc2d1	newyorská
divadelní	divadelní	k2eAgFnSc2d1	divadelní
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Největších	veliký	k2eAgNnPc2d3	veliký
40	[number]	k4	40
divadel	divadlo	k1gNnPc2	divadlo
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
500	[number]	k4	500
místy	místy	k6eAd1	místy
je	být	k5eAaImIp3nS	být
souhrnně	souhrnně	k6eAd1	souhrnně
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
Broadway	Broadwa	k2eAgFnPc1d1	Broadwa
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
Broadway	Broadway	k1gInPc1	Broadway
Theatres	Theatresa	k1gFnPc2	Theatresa
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
nejdelší	dlouhý	k2eAgFnSc2d3	nejdelší
newyorské	newyorský	k2eAgFnSc2d1	newyorská
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
také	také	k9	také
touto	tento	k3xDgFnSc7	tento
divadelní	divadelní	k2eAgFnSc7d1	divadelní
čtvrtí	čtvrt	k1gFnSc7	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
koncentrace	koncentrace	k1gFnSc1	koncentrace
divadel	divadlo	k1gNnPc2	divadlo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
části	část	k1gFnSc6	část
Broadwaye	Broadway	k1gFnSc2	Broadway
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
souhrnně	souhrnně	k6eAd1	souhrnně
říká	říkat	k5eAaImIp3nS	říkat
Times	Times	k1gMnSc1	Times
Square	square	k1gInSc1	square
nebo	nebo	k8xC	nebo
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Lincolnovo	Lincolnův	k2eAgNnSc1d1	Lincolnovo
centrum	centrum	k1gNnSc1	centrum
(	(	kIx(	(
<g/>
plným	plný	k2eAgInSc7d1	plný
názvem	název	k1gInSc7	název
Lincoln	Lincoln	k1gMnSc1	Lincoln
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
the	the	k?	the
Performing	Performing	k1gInSc1	Performing
Arts	Arts	k1gInSc1	Arts
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
budova	budova	k1gFnSc1	budova
Metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
City	City	k1gFnSc1	City
Opera	opera	k1gFnSc1	opera
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Avery	Aver	k1gInPc1	Aver
Fisher	Fishra	k1gFnPc2	Fishra
Hall	Halla	k1gFnPc2	Halla
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
sídlo	sídlo	k1gNnSc1	sídlo
New	New	k1gMnPc2	New
York	York	k1gInSc4	York
Philharmonic	Philharmonice	k1gFnPc2	Philharmonice
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
City	city	k1gNnSc1	city
Ballet	Ballet	k1gInSc1	Ballet
<g/>
,	,	kIx,	,
Vivian	Viviana	k1gFnPc2	Viviana
Beaumont	Beaumonta	k1gFnPc2	Beaumonta
Theatre	Theatr	k1gInSc5	Theatr
<g/>
,	,	kIx,	,
proslulá	proslulý	k2eAgFnSc1d1	proslulá
konzervatoř	konzervatoř	k1gFnSc1	konzervatoř
Juilliard	Juilliarda	k1gFnPc2	Juilliarda
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
Alice	Alice	k1gFnSc2	Alice
Tully	Tulla	k1gFnSc2	Tulla
Hall	Hall	k1gInSc1	Hall
a	a	k8xC	a
Jazz	jazz	k1gInSc1	jazz
at	at	k?	at
Lincoln	Lincoln	k1gMnSc1	Lincoln
Center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Central	Centrat	k5eAaImAgInS	Centrat
Park	park	k1gInSc1	park
SummerStage	SummerStag	k1gInSc2	SummerStag
organizuje	organizovat	k5eAaBmIp3nS	organizovat
během	během	k7c2	během
léta	léto	k1gNnSc2	léto
představení	představení	k1gNnPc2	představení
v	v	k7c6	v
Central	Central	k1gFnSc6	Central
Parku	park	k1gInSc2	park
a	a	k8xC	a
asi	asi	k9	asi
1200	[number]	k4	1200
koncertů	koncert	k1gInPc2	koncert
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
představení	představení	k1gNnPc2	představení
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
center	centrum	k1gNnPc2	centrum
americké	americký	k2eAgFnSc2d1	americká
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Manhatta	Manhatta	k1gFnSc1	Manhatta
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
americký	americký	k2eAgInSc4d1	americký
avantgardní	avantgardní	k2eAgInSc4d1	avantgardní
film	film	k1gInSc4	film
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgMnS	natáčet
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
natáčí	natáčet	k5eAaImIp3nS	natáčet
druhý	druhý	k4xOgInSc1	druhý
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
počet	počet	k1gInSc1	počet
filmů	film	k1gInPc2	film
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
sídlí	sídlet	k5eAaImIp3nS	sídlet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2000	[number]	k4	2000
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
a	a	k8xC	a
kulturních	kulturní	k2eAgFnPc2d1	kulturní
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
500	[number]	k4	500
galerií	galerie	k1gFnPc2	galerie
různých	různý	k2eAgFnPc2d1	různá
velikostí	velikost	k1gFnPc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Turistika	turistika	k1gFnSc1	turistika
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
příjmů	příjem	k1gInPc2	příjem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
město	město	k1gNnSc1	město
ročně	ročně	k6eAd1	ročně
navštíví	navštívit	k5eAaPmIp3nS	navštívit
asi	asi	k9	asi
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Nejpopulárnějšími	populární	k2eAgFnPc7d3	nejpopulárnější
destinacemi	destinace	k1gFnPc7	destinace
jsou	být	k5eAaImIp3nP	být
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
Ellis	Ellis	k1gInSc1	Ellis
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
divadla	divadlo	k1gNnPc1	divadlo
na	na	k7c4	na
Broadwayi	Broadwayi	k1gNnSc4	Broadwayi
<g/>
,	,	kIx,	,
muzea	muzeum	k1gNnPc1	muzeum
jako	jako	k8xC	jako
Metropolitan	metropolitan	k1gInSc1	metropolitan
Museum	museum	k1gNnSc4	museum
of	of	k?	of
Art	Art	k1gFnSc1	Art
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
místa	místo	k1gNnPc1	místo
jako	jako	k8xC	jako
Central	Central	k1gFnPc1	Central
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
Square	square	k1gInSc1	square
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Rockefeller	Rockefeller	k1gMnSc1	Rockefeller
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
Times	Times	k1gInSc1	Times
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
Bronx	Bronx	k1gInSc1	Bronx
Zoo	zoo	k1gFnSc1	zoo
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Botanical	Botanical	k1gMnSc1	Botanical
Garden	Gardna	k1gFnPc2	Gardna
<g/>
,	,	kIx,	,
luxusní	luxusní	k2eAgInPc4d1	luxusní
obchody	obchod	k1gInPc4	obchod
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
Madison	Madison	k1gInSc4	Madison
Avenue	avenue	k1gFnSc2	avenue
a	a	k8xC	a
nebo	nebo	k8xC	nebo
události	událost	k1gFnPc4	událost
jako	jako	k8xS	jako
Halloween	Halloween	k1gInSc4	Halloween
Parade	Parad	k1gInSc5	Parad
v	v	k7c4	v
Greenwich	Greenwich	k1gInSc4	Greenwich
Village	Villag	k1gFnSc2	Villag
<g/>
,	,	kIx,	,
Tribeca	Tribec	k1gInSc2	Tribec
Film	film	k1gInSc1	film
Festival	festival	k1gInSc1	festival
a	a	k8xC	a
vystoupení	vystoupení	k1gNnSc1	vystoupení
v	v	k7c6	v
Central	Central	k1gFnSc6	Central
Parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Nejpopulárnější	populární	k2eAgFnSc7d3	nejpopulárnější
atrakcí	atrakce	k1gFnSc7	atrakce
je	být	k5eAaImIp3nS	být
Socha	socha	k1gFnSc1	socha
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
ikon	ikona	k1gFnPc2	ikona
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Newyorská	newyorský	k2eAgFnSc1d1	newyorská
gastronomie	gastronomie	k1gFnSc1	gastronomie
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
imigranty	imigrant	k1gMnPc7	imigrant
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
a	a	k8xC	a
Italové	Ital	k1gMnPc1	Ital
proslavili	proslavit	k5eAaPmAgMnP	proslavit
místní	místní	k2eAgNnSc4d1	místní
pečivo	pečivo	k1gNnSc4	pečivo
,	,	kIx,	,
tvarohové	tvarohový	k2eAgInPc4d1	tvarohový
koláče	koláč	k1gInPc4	koláč
a	a	k8xC	a
newyorskou	newyorský	k2eAgFnSc4d1	newyorská
pizzu	pizza	k1gFnSc4	pizza
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
4000	[number]	k4	4000
licencovaných	licencovaný	k2eAgMnPc2d1	licencovaný
pouličních	pouliční	k2eAgMnPc2d1	pouliční
prodejců	prodejce	k1gMnPc2	prodejce
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
imigrantů	imigrant	k1gMnPc2	imigrant
<g/>
,	,	kIx,	,
nabízí	nabízet	k5eAaImIp3nS	nabízet
pokrmy	pokrm	k1gInPc4	pokrm
ze	z	k7c2	z
středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
falafel	falafel	k1gInSc4	falafel
a	a	k8xC	a
kebab	kebab	k1gInSc4	kebab
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nejpopulárnější	populární	k2eAgInPc1d3	nejpopulárnější
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
nadále	nadále	k6eAd1	nadále
hot-dogy	hotoga	k1gFnPc4	hot-doga
a	a	k8xC	a
preclíky	preclík	k1gInPc4	preclík
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
s	s	k7c7	s
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nejvyhlášenějších	vyhlášený	k2eAgFnPc2d3	nejvyhlášenější
amerických	americký	k2eAgFnPc2d1	americká
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
parky	park	k1gInPc1	park
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
asi	asi	k9	asi
113	[number]	k4	113
km	km	kA	km
<g/>
2	[number]	k4	2
a	a	k8xC	a
také	také	k9	také
22	[number]	k4	22
km	km	kA	km
veřejných	veřejný	k2eAgFnPc2d1	veřejná
pláží	pláž	k1gFnPc2	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Gateway	Gatewa	k2eAgFnPc1d1	Gatewa
National	National	k1gFnPc1	National
Recration	Recration	k1gInSc4	Recration
Area	Ares	k1gMnSc4	Ares
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
tisíce	tisíc	k4xCgInSc2	tisíc
akrů	akr	k1gInPc2	akr
náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
systému	systém	k1gInSc2	systém
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Jamaica	Jamaica	k1gFnSc1	Jamaica
Bay	Bay	k1gFnSc2	Bay
Wildlife	Wildlif	k1gInSc5	Wildlif
Refuge	Refuge	k1gNnPc3	Refuge
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
36	[number]	k4	36
km	km	kA	km
<g/>
2	[number]	k4	2
a	a	k8xC	a
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
tak	tak	k6eAd1	tak
většinu	většina	k1gFnSc4	většina
Jamaica	Jamaica	k1gFnSc2	Jamaica
Bay	Bay	k1gFnSc2	Bay
<g/>
.	.	kIx.	.
</s>
<s>
Central	Centrat	k5eAaPmAgInS	Centrat
Park	park	k1gInSc1	park
navržený	navržený	k2eAgInSc1d1	navržený
Frederickem	Frederick	k1gInSc7	Frederick
Law	Law	k1gMnSc7	Law
Olmstedem	Olmsted	k1gMnSc7	Olmsted
a	a	k8xC	a
Calvertem	Calvert	k1gMnSc7	Calvert
Vauxem	Vaux	k1gInSc7	Vaux
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
30	[number]	k4	30
miliony	milion	k4xCgInPc1	milion
návštěvníků	návštěvník	k1gMnPc2	návštěvník
nejnavštěvovanějším	navštěvovaný	k2eAgInSc7d3	nejnavštěvovanější
parkem	park	k1gInSc7	park
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
deset	deset	k4xCc4	deset
milionů	milion	k4xCgInPc2	milion
více	hodně	k6eAd2	hodně
než	než	k8xS	než
u	u	k7c2	u
Lincoln	Lincoln	k1gMnSc1	Lincoln
Parku	park	k1gInSc2	park
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgInSc1	druhý
nejnavštěvovanější	navštěvovaný	k2eAgMnSc1d3	nejnavštěvovanější
<g/>
.	.	kIx.	.
</s>
<s>
Prospect	Prospect	k2eAgInSc1d1	Prospect
Park	park	k1gInSc1	park
v	v	k7c6	v
Brooklynu	Brooklyn	k1gInSc6	Brooklyn
navržený	navržený	k2eAgInSc4d1	navržený
taktéž	taktéž	k?	taktéž
Olmstedem	Olmsted	k1gMnSc7	Olmsted
a	a	k8xC	a
Vauxem	Vaux	k1gInSc7	Vaux
má	mít	k5eAaImIp3nS	mít
36	[number]	k4	36
ha	ha	kA	ha
velkou	velký	k2eAgFnSc4d1	velká
louku	louka	k1gFnSc4	louka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
Flushing	Flushing	k1gInSc4	Flushing
Meadows-Corona	Meadows-Coron	k1gMnSc2	Meadows-Coron
Parku	park	k1gInSc2	park
v	v	k7c6	v
Queensu	Queens	k1gInSc6	Queens
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc7	třetí
největší	veliký	k2eAgFnSc7d3	veliký
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
světové	světový	k2eAgFnPc1d1	světová
výstavy	výstava	k1gFnPc1	výstava
1939	[number]	k4	1939
a	a	k8xC	a
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
světovou	světový	k2eAgFnSc7d1	světová
metropolí	metropol	k1gFnSc7	metropol
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
reklamy	reklama	k1gFnSc2	reklama
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
novinových	novinový	k2eAgNnPc2d1	novinové
a	a	k8xC	a
knižních	knižní	k2eAgNnPc2d1	knižní
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
největším	veliký	k2eAgInSc7d3	veliký
mediálním	mediální	k2eAgInSc7d1	mediální
trhem	trh	k1gInSc7	trh
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
následován	následován	k2eAgInSc1d1	následován
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
Chicagem	Chicago	k1gNnSc7	Chicago
a	a	k8xC	a
Torontem	Toronto	k1gNnSc7	Toronto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
místní	místní	k2eAgFnPc4d1	místní
mediální	mediální	k2eAgFnPc4d1	mediální
skupiny	skupina	k1gFnPc4	skupina
patří	patřit	k5eAaImIp3nS	patřit
Time	Time	k1gFnPc7	Time
Warner	Warner	k1gInSc1	Warner
<g/>
,	,	kIx,	,
the	the	k?	the
News	News	k1gInSc1	News
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
the	the	k?	the
Hearst	Hearst	k1gInSc1	Hearst
Corporation	Corporation	k1gInSc1	Corporation
a	a	k8xC	a
Viacom	Viacom	k1gInSc1	Viacom
<g/>
.	.	kIx.	.
</s>
<s>
Sedm	sedm	k4xCc1	sedm
z	z	k7c2	z
deseti	deset	k4xCc2	deset
největších	veliký	k2eAgFnPc2d3	veliký
světových	světový	k2eAgFnPc2d1	světová
reklamních	reklamní	k2eAgFnPc2d1	reklamní
agentur	agentura	k1gFnPc2	agentura
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
největších	veliký	k2eAgInPc2d3	veliký
hudebních	hudební	k2eAgInPc2d1	hudební
labelů	label	k1gInPc2	label
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
se	se	k3xPyFc4	se
točí	točit	k5eAaImIp3nS	točit
třetina	třetina	k1gFnSc1	třetina
amerických	americký	k2eAgInPc2d1	americký
nezávislých	závislý	k2eNgInPc2d1	nezávislý
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
350	[number]	k4	350
časopisů	časopis	k1gInPc2	časopis
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
své	své	k1gNnSc4	své
sídlo	sídlo	k1gNnSc1	sídlo
a	a	k8xC	a
knižní	knižní	k2eAgNnPc1d1	knižní
nakladatelství	nakladatelství	k1gNnPc1	nakladatelství
zaměstnávají	zaměstnávat	k5eAaImIp3nP	zaměstnávat
přes	přes	k7c4	přes
25	[number]	k4	25
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dvoje	dvoje	k4xRgFnSc1	dvoje
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
celostátně	celostátně	k6eAd1	celostátně
vydávaných	vydávaný	k2eAgFnPc2d1	vydávaná
novin	novina	k1gFnPc2	novina
mají	mít	k5eAaImIp3nP	mít
redakci	redakce	k1gFnSc4	redakce
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
The	The	k1gMnSc1	The
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
a	a	k8xC	a
The	The	k1gMnSc1	The
Wall	Wall	k1gMnSc1	Wall
Street	Street	k1gMnSc1	Street
Journal	Journal	k1gMnSc1	Journal
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
bulvární	bulvární	k2eAgInPc4d1	bulvární
deníky	deník	k1gInPc4	deník
vydávané	vydávaný	k2eAgInPc4d1	vydávaný
ve	v	k7c6	v
městě	město	k1gNnSc6	město
patří	patřit	k5eAaImIp3nS	patřit
The	The	k1gMnSc1	The
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Daily	Daila	k1gFnSc2	Daila
News	Newsa	k1gFnPc2	Newsa
a	a	k8xC	a
The	The	k1gFnPc2	The
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Post	post	k1gInSc1	post
založený	založený	k2eAgInSc1d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1801	[number]	k4	1801
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Hamiltonem	Hamilton	k1gInSc7	Hamilton
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
místních	místní	k2eAgFnPc2d1	místní
novin	novina	k1gFnPc2	novina
je	být	k5eAaImIp3nS	být
zaměřeno	zaměřit	k5eAaPmNgNnS	zaměřit
na	na	k7c4	na
etnické	etnický	k2eAgFnPc4d1	etnická
menšiny	menšina	k1gFnPc4	menšina
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
270	[number]	k4	270
časopisů	časopis	k1gInPc2	časopis
a	a	k8xC	a
novin	novina	k1gFnPc2	novina
vychází	vycházet	k5eAaImIp3nS	vycházet
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
El	Ela	k1gFnPc2	Ela
Diario	Diario	k1gNnSc4	Diario
La	la	k1gNnSc2	la
Prensa	Prens	k1gMnSc2	Prens
je	být	k5eAaImIp3nS	být
nejprodávanější	prodávaný	k2eAgNnSc1d3	nejprodávanější
a	a	k8xC	a
nejstarší	starý	k2eAgMnSc1d3	nejstarší
newyorský	newyorský	k2eAgMnSc1d1	newyorský
španělsky	španělsky	k6eAd1	španělsky
psaný	psaný	k2eAgInSc1d1	psaný
deník	deník	k1gInSc1	deník
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
News	News	k1gInSc1	News
sídlící	sídlící	k2eAgInSc1d1	sídlící
v	v	k7c6	v
Harlemu	Harlem	k1gInSc6	Harlem
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgFnSc1d1	populární
v	v	k7c6	v
černošské	černošský	k2eAgFnSc6d1	černošská
komunitě	komunita	k1gFnSc6	komunita
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Village	Village	k1gInSc1	Village
Voice	Voice	k1gMnSc1	Voice
jsou	být	k5eAaImIp3nP	být
nejpopulárnější	populární	k2eAgFnPc4d3	nejpopulárnější
alternativní	alternativní	k2eAgFnPc4d1	alternativní
noviny	novina	k1gFnPc4	novina
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
televizní	televizní	k2eAgFnSc2d1	televizní
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
čtyři	čtyři	k4xCgFnPc4	čtyři
největší	veliký	k2eAgFnPc4d3	veliký
americké	americký	k2eAgFnPc4d1	americká
stanice	stanice	k1gFnPc4	stanice
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
<g/>
,	,	kIx,	,
CBS	CBS	kA	CBS
<g/>
,	,	kIx,	,
FOX	fox	k1gInSc1	fox
a	a	k8xC	a
NBC	NBC	kA	NBC
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
stanic	stanice	k1gFnPc2	stanice
kabelové	kabelový	k2eAgFnSc2d1	kabelová
televize	televize	k1gFnSc2	televize
jako	jako	k8xS	jako
HBO	HBO	kA	HBO
<g/>
,	,	kIx,	,
MTV	MTV	kA	MTV
<g/>
,	,	kIx,	,
Fox	fox	k1gInSc4	fox
News	Newsa	k1gFnPc2	Newsa
a	a	k8xC	a
Comedy	Comeda	k1gMnSc2	Comeda
Central	Central	k1gMnSc2	Central
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
v	v	k7c4	v
New	New	k1gFnSc4	New
Yorku	York	k1gInSc2	York
natáčeno	natáčen	k2eAgNnSc1d1	natáčeno
přes	přes	k7c4	přes
100	[number]	k4	100
televizních	televizní	k2eAgInPc2d1	televizní
pořadů	pořad	k1gInPc2	pořad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
sídlí	sídlet	k5eAaImIp3nS	sídlet
mnoho	mnoho	k4c1	mnoho
nekomerčních	komerční	k2eNgNnPc2d1	nekomerční
médií	médium	k1gNnPc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
nekomerční	komerční	k2eNgFnSc7d1	nekomerční
stanicí	stanice	k1gFnSc7	stanice
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
Manhattan	Manhattan	k1gInSc1	Manhattan
Neighborhood	Neighborhooda	k1gFnPc2	Neighborhooda
Network	network	k1gInSc2	network
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
WNET	WNET	kA	WNET
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
místní	místní	k2eAgFnSc1d1	místní
nekomerční	komerční	k2eNgFnSc1d1	nekomerční
stanice	stanice	k1gFnSc1	stanice
a	a	k8xC	a
primární	primární	k2eAgMnSc1d1	primární
poskytovatel	poskytovatel	k1gMnSc1	poskytovatel
obsahu	obsah	k1gInSc2	obsah
celostátní	celostátní	k2eAgFnSc2d1	celostátní
PBS	PBS	kA	PBS
<g/>
.	.	kIx.	.
</s>
<s>
Nekomerční	komerční	k2eNgInPc1d1	nekomerční
WNYC	WNYC	kA	WNYC
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
vlastnilo	vlastnit	k5eAaImAgNnS	vlastnit
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejposlouchanějším	poslouchaný	k2eAgNnSc7d3	nejposlouchanější
rádiem	rádio	k1gNnSc7	rádio
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
provozuje	provozovat	k5eAaImIp3nS	provozovat
televizní	televizní	k2eAgFnSc4d1	televizní
stanici	stanice	k1gFnSc4	stanice
nyctv	nyctva	k1gFnPc2	nyctva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyprodukovala	vyprodukovat	k5eAaPmAgFnS	vyprodukovat
několik	několik	k4yIc4	několik
pořadů	pořad	k1gInPc2	pořad
o	o	k7c6	o
místní	místní	k2eAgFnSc6d1	místní
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
politice	politika	k1gFnSc6	politika
oceněných	oceněný	k2eAgMnPc2d1	oceněný
cenou	cena	k1gFnSc7	cena
Emmy	Emma	k1gFnPc1	Emma
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgMnPc4	svůj
zástupce	zástupce	k1gMnPc4	zástupce
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
největších	veliký	k2eAgFnPc6d3	veliký
amerických	americký	k2eAgFnPc6d1	americká
sportovních	sportovní	k2eAgFnPc6d1	sportovní
ligách	liga	k1gFnPc6	liga
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
všechny	všechen	k3xTgFnPc1	všechen
ligy	liga	k1gFnPc1	liga
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
amerických	americký	k2eAgNnPc2d1	americké
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
baseball	baseball	k1gInSc4	baseball
populárnější	populární	k2eAgInSc4d2	populárnější
než	než	k8xS	než
americký	americký	k2eAgInSc4d1	americký
fotbal	fotbal	k1gInSc4	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Světové	světový	k2eAgFnSc2d1	světová
série	série	k1gFnSc2	série
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
čtrnáct	čtrnáct	k4xCc4	čtrnáct
duelů	duel	k1gInPc2	duel
mezi	mezi	k7c7	mezi
newyorskými	newyorský	k2eAgInPc7d1	newyorský
týmy	tým	k1gInPc7	tým
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
těmto	tento	k3xDgInPc3	tento
duelům	duel	k1gInPc3	duel
se	se	k3xPyFc4	se
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
Subway	Subway	k1gInPc7	Subway
Series	Seriesa	k1gFnPc2	Seriesa
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
metropolitních	metropolitní	k2eAgFnPc2d1	metropolitní
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
soutěži	soutěž	k1gFnSc6	soutěž
dva	dva	k4xCgInPc4	dva
baseballové	baseballový	k2eAgInPc4d1	baseballový
týmy	tým	k1gInPc4	tým
(	(	kIx(	(
<g/>
ostatní	ostatní	k1gNnSc1	ostatní
jsou	být	k5eAaImIp3nP	být
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
Washington-Baltimore	Washington-Baltimor	k1gMnSc5	Washington-Baltimor
<g/>
,	,	kIx,	,
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
a	a	k8xC	a
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
Bay	Bay	k1gMnSc1	Bay
Area	area	k1gFnSc1	area
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
v	v	k7c6	v
MLB	MLB	kA	MLB
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
New	New	k1gFnSc1	New
York	York	k1gInSc4	York
Mets	Mets	k1gInSc1	Mets
a	a	k8xC	a
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Yankees	Yankees	k1gInSc1	Yankees
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
během	během	k7c2	během
sezóny	sezóna	k1gFnSc2	sezóna
utkají	utkat	k5eAaPmIp3nP	utkat
šestkrát	šestkrát	k6eAd1	šestkrát
<g/>
.	.	kIx.	.
</s>
<s>
Yankees	Yankees	k1gInSc1	Yankees
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
vítězi	vítěz	k1gMnPc7	vítěz
Světové	světový	k2eAgFnSc2d1	světová
série	série	k1gFnSc2	série
šestadvacetkrát	šestadvacetkrát	k6eAd1	šestadvacetkrát
a	a	k8xC	a
Mets	Mets	k1gInSc4	Mets
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlily	sídlit	k5eAaImAgInP	sídlit
týmy	tým	k1gInPc1	tým
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Giants	Giantsa	k1gFnPc2	Giantsa
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
San	San	k1gFnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
Giants	Giants	k1gInSc1	Giants
<g/>
)	)	kIx)	)
a	a	k8xC	a
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
Dodgers	Dodgersa	k1gFnPc2	Dodgersa
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
Dodgers	Dodgersa	k1gFnPc2	Dodgersa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
týmy	tým	k1gInPc1	tým
se	se	k3xPyFc4	se
přestěhovaly	přestěhovat	k5eAaPmAgInP	přestěhovat
do	do	k7c2	do
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
také	také	k9	také
dva	dva	k4xCgInPc4	dva
týmy	tým	k1gInPc1	tým
Minor	minor	k2eAgNnSc2d1	minor
league	league	k1gNnSc2	league
baseball	baseball	k1gInSc1	baseball
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Staten	Staten	k2eAgInSc1d1	Staten
Island	Island	k1gInSc1	Island
Yankees	Yankees	k1gInSc1	Yankees
a	a	k8xC	a
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
Cyclones	Cyclonesa	k1gFnPc2	Cyclonesa
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentanty	reprezentant	k1gInPc1	reprezentant
města	město	k1gNnSc2	město
v	v	k7c6	v
National	National	k1gFnSc6	National
Football	Footballa	k1gFnPc2	Footballa
League	Leagu	k1gFnSc2	Leagu
(	(	kIx(	(
<g/>
americký	americký	k2eAgInSc1d1	americký
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
New	New	k1gMnPc1	New
York	York	k1gInSc4	York
Jets	Jetsa	k1gFnPc2	Jetsa
a	a	k8xC	a
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Giants	Giantsa	k1gFnPc2	Giantsa
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Football	Football	k1gMnSc1	Football
Giants	Giants	k1gInSc1	Giants
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
oba	dva	k4xCgInPc1	dva
týmy	tým	k1gInPc1	tým
hrají	hrát	k5eAaImIp3nP	hrát
své	svůj	k3xOyFgInPc4	svůj
zápasy	zápas	k1gInPc4	zápas
na	na	k7c4	na
Giants	Giants	k1gInSc4	Giants
Stadium	stadium	k1gNnSc1	stadium
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
National	National	k1gFnSc6	National
Hockey	Hockea	k1gMnSc2	Hockea
League	Leagu	k1gMnSc2	Leagu
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
město	město	k1gNnSc4	město
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Rangers	Rangersa	k1gFnPc2	Rangersa
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
hrají	hrát	k5eAaImIp3nP	hrát
své	svůj	k3xOyFgInPc4	svůj
domovské	domovský	k2eAgInPc4d1	domovský
zápasy	zápas	k1gInPc4	zápas
v	v	k7c4	v
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc4	square
Garden	Gardna	k1gFnPc2	Gardna
a	a	k8xC	a
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Islanders	Islandersa	k1gFnPc2	Islandersa
hrající	hrající	k2eAgMnSc1d1	hrající
v	v	k7c6	v
Nassau	Nassaus	k1gInSc6	Nassaus
Veterans	Veterans	k1gInSc1	Veterans
Memorial	Memorial	k1gInSc1	Memorial
Coliseum	Coliseum	k1gInSc1	Coliseum
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Uniondale	Uniondala	k1gFnSc3	Uniondala
na	na	k7c4	na
Long	Long	k1gInSc4	Long
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
Red	Red	k1gMnSc1	Red
Bulls	Bullsa	k1gFnPc2	Bullsa
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
město	město	k1gNnSc1	město
v	v	k7c6	v
Major	major	k1gMnSc1	major
League	League	k1gNnSc1	League
Soccer	Soccer	k1gMnSc1	Soccer
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tento	tento	k3xDgInSc1	tento
tým	tým	k1gInSc1	tým
ovšem	ovšem	k9	ovšem
hraje	hrát	k5eAaImIp3nS	hrát
domovské	domovský	k2eAgInPc4d1	domovský
zápasy	zápas	k1gInPc4	zápas
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
v	v	k7c6	v
Red	Red	k1gFnSc6	Red
Bull	bulla	k1gFnPc2	bulla
Areně	Areeň	k1gFnSc2	Areeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
National	National	k1gFnSc6	National
Basketball	Basketballa	k1gFnPc2	Basketballa
Association	Association	k1gInSc1	Association
jsou	být	k5eAaImIp3nP	být
reprezentantem	reprezentant	k1gMnSc7	reprezentant
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Knicks	Knicks	k1gInSc1	Knicks
a	a	k8xC	a
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
Nets	Netsa	k1gFnPc2	Netsa
a	a	k8xC	a
ve	v	k7c6	v
Women	Womna	k1gFnPc2	Womna
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
National	National	k1gMnSc7	National
Basketball	Basketballa	k1gFnPc2	Basketballa
Association	Association	k1gInSc1	Association
tým	tým	k1gInSc1	tým
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
Liberty	Libert	k1gInPc1	Libert
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
vysokoškolský	vysokoškolský	k2eAgInSc4d1	vysokoškolský
basketbalový	basketbalový	k2eAgInSc4d1	basketbalový
turnaj	turnaj	k1gInSc4	turnaj
<g/>
,	,	kIx,	,
National	National	k1gFnSc1	National
Invitation	Invitation	k1gInSc1	Invitation
Tournament	Tournament	k1gInSc1	Tournament
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
a	a	k8xC	a
koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Rucker	Rucker	k1gInSc1	Rucker
Park	park	k1gInSc1	park
v	v	k7c6	v
Harlemu	Harlem	k1gInSc6	Harlem
je	být	k5eAaImIp3nS	být
slavné	slavný	k2eAgNnSc4d1	slavné
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
sportovců	sportovec	k1gMnPc2	sportovec
účastní	účastnit	k5eAaImIp3nS	účastnit
letní	letní	k2eAgFnSc2d1	letní
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
se	se	k3xPyFc4	se
také	také	k9	také
koná	konat	k5eAaImIp3nS	konat
mnoho	mnoho	k4c1	mnoho
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
sportovních	sportovní	k2eAgFnPc2d1	sportovní
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Queensu	Queens	k1gInSc6	Queens
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
koná	konat	k5eAaImIp3nS	konat
tenisové	tenisový	k2eAgInPc4d1	tenisový
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
Grand	grand	k1gMnSc1	grand
Slamových	Slamův	k2eAgInPc2d1	Slamův
turnajů	turnaj	k1gInPc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Newyorský	newyorský	k2eAgInSc1d1	newyorský
maraton	maraton	k1gInSc1	maraton
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
maratonem	maraton	k1gInSc7	maraton
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
ročníky	ročník	k1gInPc7	ročník
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
prvních	první	k4xOgNnPc6	první
čtyřech	čtyři	k4xCgNnPc6	čtyři
místech	místo	k1gNnPc6	místo
mezi	mezi	k7c7	mezi
maratony	maraton	k1gInPc7	maraton
co	co	k9	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
závodníků	závodník	k1gMnPc2	závodník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
doběhli	doběhnout	k5eAaPmAgMnP	doběhnout
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
závod	závod	k1gInSc1	závod
dokončilo	dokončit	k5eAaPmAgNnS	dokončit
celkem	celkem	k6eAd1	celkem
38557	[number]	k4	38557
běžců	běžec	k1gMnPc2	běžec
<g/>
.	.	kIx.	.
</s>
<s>
Millrose	Millrosa	k1gFnSc3	Millrosa
Games	Games	k1gMnSc1	Games
je	být	k5eAaImIp3nS	být
každoroční	každoroční	k2eAgInSc4d1	každoroční
atletický	atletický	k2eAgInSc4d1	atletický
mítink	mítink	k1gInSc4	mítink
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
se	se	k3xPyFc4	se
běží	běžet	k5eAaImIp3nS	běžet
závod	závod	k1gInSc1	závod
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
míli	míle	k1gFnSc4	míle
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Wanamaker	Wanamaker	k1gInSc1	Wanamaker
Mile	mile	k6eAd1	mile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
je	být	k5eAaImIp3nS	být
také	také	k9	také
populární	populární	k2eAgInSc4d1	populární
box	box	k1gInSc4	box
a	a	k8xC	a
každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
koná	konat	k5eAaImIp3nS	konat
několik	několik	k4yIc1	několik
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Amateur	Amateur	k1gMnSc1	Amateur
Boxing	boxing	k1gInSc1	boxing
Golden	Goldna	k1gFnPc2	Goldna
Gloves	Glovesa	k1gFnPc2	Glovesa
v	v	k7c4	v
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc4	square
Garden	Gardna	k1gFnPc2	Gardna
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
sportovních	sportovní	k2eAgNnPc2d1	sportovní
odvětví	odvětví	k1gNnPc2	odvětví
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
spjato	spjat	k2eAgNnSc1d1	spjato
s	s	k7c7	s
komunitami	komunita	k1gFnPc7	komunita
imigrantů	imigrant	k1gMnPc2	imigrant
<g/>
.	.	kIx.	.
</s>
<s>
Stickball	Stickball	k1gInSc1	Stickball
<g/>
,	,	kIx,	,
pouliční	pouliční	k2eAgFnSc1d1	pouliční
verze	verze	k1gFnSc1	verze
baseballu	baseball	k1gInSc2	baseball
<g/>
,	,	kIx,	,
hráli	hrát	k5eAaImAgMnP	hrát
především	především	k9	především
mladíci	mladík	k1gMnPc1	mladík
z	z	k7c2	z
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
třídy	třída	k1gFnSc2	třída
italského	italský	k2eAgInSc2d1	italský
<g/>
,	,	kIx,	,
německého	německý	k2eAgInSc2d1	německý
a	a	k8xC	a
irského	irský	k2eAgInSc2d1	irský
původu	původ	k1gInSc2	původ
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Stickball	Stickball	k1gMnSc1	Stickball
se	se	k3xPyFc4	se
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
hraje	hrát	k5eAaImIp3nS	hrát
dodnes	dodnes	k6eAd1	dodnes
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
ulice	ulice	k1gFnSc1	ulice
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Bronxu	Bronx	k1gInSc6	Bronx
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
na	na	k7c4	na
Stickball	Stickball	k1gInSc4	Stickball
Blvd	Blvda	k1gFnPc2	Blvda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
komunitách	komunita	k1gFnPc6	komunita
imigrantů	imigrant	k1gMnPc2	imigrant
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Karibiku	Karibik	k1gInSc2	Karibik
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
kriket	kriket	k1gInSc1	kriket
<g/>
.	.	kIx.	.
</s>
<s>
Pouliční	pouliční	k2eAgInSc4d1	pouliční
hokej	hokej	k1gInSc4	hokej
<g/>
,	,	kIx,	,
fotbal	fotbal	k1gInSc4	fotbal
a	a	k8xC	a
basketbal	basketbal	k1gInSc4	basketbal
jsou	být	k5eAaImIp3nP	být
dalšími	další	k2eAgInPc7d1	další
sporty	sport	k1gInPc7	sport
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgFnPc7	který
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
na	na	k7c6	na
newyorských	newyorský	k2eAgFnPc6d1	newyorská
ulicích	ulice	k1gFnPc6	ulice
setkat	setkat	k5eAaPmF	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
ulicích	ulice	k1gFnPc6	ulice
lze	lze	k6eAd1	lze
narazit	narazit	k5eAaPmF	narazit
na	na	k7c4	na
všemožné	všemožný	k2eAgInPc4d1	všemožný
sporty	sport	k1gInPc4	sport
a	a	k8xC	a
sportovce	sportovec	k1gMnPc4	sportovec
všech	všecek	k3xTgFnPc2	všecek
věkových	věkový	k2eAgFnPc2d1	věková
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
občas	občas	k6eAd1	občas
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
největší	veliký	k2eAgNnSc4d3	veliký
městské	městský	k2eAgNnSc4d1	Městské
hřiště	hřiště	k1gNnSc4	hřiště
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
"	"	kIx"	"
Kriminalita	kriminalita	k1gFnSc1	kriminalita
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
na	na	k7c6	na
extrémně	extrémně	k6eAd1	extrémně
vysoké	vysoký	k2eAgFnSc6d1	vysoká
úrovni	úroveň	k1gFnSc6	úroveň
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
probíhala	probíhat	k5eAaImAgFnS	probíhat
epidemie	epidemie	k1gFnSc1	epidemie
užívání	užívání	k1gNnSc2	užívání
drogy	droga	k1gFnSc2	droga
zvané	zvaný	k2eAgFnPc1d1	zvaná
crack	crack	k6eAd1	crack
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
kriminalita	kriminalita	k1gFnSc1	kriminalita
postupně	postupně	k6eAd1	postupně
klesala	klesat	k5eAaImAgFnS	klesat
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
je	být	k5eAaImIp3nS	být
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
nejbezpečnějším	bezpečný	k2eAgFnPc3d3	nejbezpečnější
z	z	k7c2	z
25	[number]	k4	25
největších	veliký	k2eAgNnPc2d3	veliký
amerických	americký	k2eAgNnPc2d1	americké
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
kriminalita	kriminalita	k1gFnSc1	kriminalita
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
úrovni	úroveň	k1gFnSc6	úroveň
jako	jako	k8xC	jako
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Provo	Provo	k1gNnSc4	Provo
v	v	k7c6	v
Utahu	Utah	k1gInSc6	Utah
a	a	k8xC	a
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
byl	být	k5eAaImAgInS	být
až	až	k9	až
na	na	k7c4	na
197	[number]	k4	197
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
z	z	k7c2	z
216	[number]	k4	216
amerických	americký	k2eAgNnPc2d1	americké
měst	město	k1gNnPc2	město
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Násilná	násilný	k2eAgFnSc1d1	násilná
trestná	trestný	k2eAgFnSc1d1	trestná
činnost	činnost	k1gFnSc1	činnost
klesla	klesnout	k5eAaPmAgFnS	klesnout
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1993	[number]	k4	1993
a	a	k8xC	a
2005	[number]	k4	2005
o	o	k7c4	o
75	[number]	k4	75
%	%	kIx~	%
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
federální	federální	k2eAgFnSc6d1	federální
úrovni	úroveň	k1gFnSc6	úroveň
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
nárůstu	nárůst	k1gInSc3	nárůst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
klesl	klesnout	k5eAaPmAgInS	klesnout
počet	počet	k1gInSc1	počet
vražd	vražda	k1gFnPc2	vražda
na	na	k7c4	na
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
úroveň	úroveň	k1gFnSc4	úroveň
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
spácháno	spáchat	k5eAaPmNgNnS	spáchat
méně	málo	k6eAd2	málo
než	než	k8xS	než
500	[number]	k4	500
vražd	vražda	k1gFnPc2	vražda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
publikovány	publikovat	k5eAaBmNgFnP	publikovat
tyto	tento	k3xDgFnPc1	tento
statistiky	statistika	k1gFnPc1	statistika
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
počet	počet	k1gInSc1	počet
vražd	vražda	k1gFnPc2	vražda
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
350	[number]	k4	350
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Sociologové	sociolog	k1gMnPc1	sociolog
a	a	k8xC	a
kriminologové	kriminolog	k1gMnPc1	kriminolog
se	se	k3xPyFc4	se
ohledně	ohledně	k7c2	ohledně
příčin	příčina	k1gFnPc2	příčina
dramatického	dramatický	k2eAgInSc2d1	dramatický
poklesu	pokles	k1gInSc2	pokles
kriminality	kriminalita	k1gFnSc2	kriminalita
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
neshodují	shodovat	k5eNaImIp3nP	shodovat
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
některých	některý	k3yIgNnPc2	některý
tvrzení	tvrzení	k1gNnPc2	tvrzení
nastal	nastat	k5eAaPmAgInS	nastat
tento	tento	k3xDgInSc4	tento
pokles	pokles	k1gInSc4	pokles
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
účinných	účinný	k2eAgNnPc2d1	účinné
opatření	opatření	k1gNnPc2	opatření
vedení	vedení	k1gNnSc2	vedení
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
City	City	k1gFnSc2	City
Police	police	k1gFnSc2	police
Departmentu	department	k1gInSc2	department
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
program	program	k1gInSc1	program
CompStat	CompStat	k1gFnSc2	CompStat
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
teorie	teorie	k1gFnSc1	teorie
rozbitého	rozbitý	k2eAgNnSc2d1	rozbité
okna	okno	k1gNnSc2	okno
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
jiných	jiný	k2eAgInPc2d1	jiný
názorů	názor	k1gInPc2	názor
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgInPc7d1	hlavní
důvody	důvod	k1gInPc7	důvod
jak	jak	k8xS	jak
konec	konec	k1gInSc1	konec
epidemie	epidemie	k1gFnSc2	epidemie
cracku	crack	k1gInSc2	crack
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
příznivě	příznivě	k6eAd1	příznivě
působící	působící	k2eAgFnPc1d1	působící
demografické	demografický	k2eAgFnPc1d1	demografická
změny	změna	k1gFnPc1	změna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnPc6	New
Yorku	York	k1gInSc2	York
působilo	působit	k5eAaImAgNnS	působit
několik	několik	k4yIc1	několik
známých	známý	k2eAgFnPc2d1	známá
skupin	skupina	k1gFnPc2	skupina
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
byli	být	k5eAaImAgMnP	být
Forty	Forty	k?	Forty
Thieves	Thieves	k1gMnSc1	Thieves
a	a	k8xC	a
Roach	Roach	k1gInSc1	Roach
Guards	Guardsa	k1gFnPc2	Guardsa
působící	působící	k2eAgMnSc1d1	působící
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Five	Five	k1gNnSc2	Five
Points	Pointsa	k1gFnPc2	Pointsa
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
působila	působit	k5eAaImAgFnS	působit
mafie	mafie	k1gFnSc1	mafie
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
Pěti	pět	k4xCc7	pět
rodinami	rodina	k1gFnPc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Gangy	Ganges	k1gInPc1	Ganges
jako	jako	k8xS	jako
Black	Black	k1gInSc1	Black
Spades	Spadesa	k1gFnPc2	Spadesa
vznikaly	vznikat	k5eAaImAgInP	vznikat
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
zformována	zformován	k2eAgFnSc1d1	zformována
moderní	moderní	k2eAgFnSc1d1	moderní
podoba	podoba	k1gFnSc1	podoba
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
New	New	k1gFnSc1	New
York	York	k1gInSc4	York
metropolitní	metropolitní	k2eAgFnSc7d1	metropolitní
obcí	obec	k1gFnSc7	obec
se	s	k7c7	s
silným	silný	k2eAgNnSc7d1	silné
postavením	postavení	k1gNnSc7	postavení
starosty	starosta	k1gMnSc2	starosta
(	(	kIx(	(
<g/>
mayor	mayor	k1gInSc1	mayor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Newyorská	newyorský	k2eAgFnSc1d1	newyorská
vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
centralizovanější	centralizovaný	k2eAgFnSc1d2	centralizovanější
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
u	u	k7c2	u
ostatních	ostatní	k2eAgNnPc2d1	ostatní
amerických	americký	k2eAgNnPc2d1	americké
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
zodpovídá	zodpovídat	k5eAaPmIp3nS	zodpovídat
za	za	k7c4	za
veřejné	veřejný	k2eAgNnSc4d1	veřejné
školství	školství	k1gNnSc4	školství
<g/>
,	,	kIx,	,
nápravná	nápravný	k2eAgNnPc4d1	nápravné
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
knihovny	knihovna	k1gFnPc4	knihovna
<g/>
,	,	kIx,	,
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
odpočinková	odpočinkový	k2eAgNnPc4d1	odpočinkové
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
hygienická	hygienický	k2eAgNnPc4d1	hygienické
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
dodávku	dodávka	k1gFnSc4	dodávka
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Starosta	Starosta	k1gMnSc1	Starosta
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
mají	mít	k5eAaImIp3nP	mít
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Newyorské	newyorský	k2eAgNnSc1d1	newyorské
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
je	být	k5eAaImIp3nS	být
jednokomorové	jednokomorový	k2eAgNnSc1d1	jednokomorové
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
51	[number]	k4	51
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Starosta	Starosta	k1gMnSc1	Starosta
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
jen	jen	k9	jen
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
po	po	k7c6	po
sobě	se	k3xPyFc3	se
jdoucí	jdoucí	k2eAgNnSc4d1	jdoucí
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
starostou	starosta	k1gMnSc7	starosta
je	být	k5eAaImIp3nS	být
demokrat	demokrat	k1gMnSc1	demokrat
Bill	Bill	k1gMnSc1	Bill
de	de	k?	de
Blasio	Blasio	k1gMnSc1	Blasio
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
po	po	k7c6	po
Michaelu	Michael	k1gMnSc6	Michael
Bloombergovi	Bloomberg	k1gMnSc6	Bloomberg
<g/>
.	.	kIx.	.
</s>
<s>
Bloomberg	Bloomberg	k1gMnSc1	Bloomberg
byl	být	k5eAaImAgMnS	být
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
demokrat	demokrat	k1gMnSc1	demokrat
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
zvolen	zvolit	k5eAaPmNgMnS	zvolit
starostou	starosta	k1gMnSc7	starosta
New	New	k1gMnSc2	New
Yorku	York	k1gInSc2	York
jako	jako	k8xS	jako
republikán	republikán	k1gMnSc1	republikán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgMnS	být
znovuzvolen	znovuzvolit	k5eAaPmNgMnS	znovuzvolit
<g/>
,	,	kIx,	,
když	když	k8xS	když
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
59	[number]	k4	59
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nezávislým	závislý	k2eNgMnSc7d1	nezávislý
politikem	politik	k1gMnSc7	politik
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
snahami	snaha	k1gFnPc7	snaha
získat	získat	k5eAaPmF	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
vzdělávacím	vzdělávací	k2eAgInSc7d1	vzdělávací
systémem	systém	k1gInSc7	systém
od	od	k7c2	od
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
agresivní	agresivní	k2eAgFnSc7d1	agresivní
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
zdravotnickou	zdravotnický	k2eAgFnSc7d1	zdravotnická
politikou	politika	k1gFnSc7	politika
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
určil	určit	k5eAaPmAgInS	určit
jako	jako	k8xS	jako
priority	priorita	k1gFnSc2	priorita
školskou	školský	k2eAgFnSc4d1	školská
reformu	reforma	k1gFnSc4	reforma
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc4	snížení
chudoby	chudoba	k1gFnSc2	chudoba
a	a	k8xC	a
přísnou	přísný	k2eAgFnSc4d1	přísná
regulaci	regulace	k1gFnSc4	regulace
držení	držení	k1gNnSc2	držení
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
starostou	starosta	k1gMnSc7	starosta
Bostonu	Boston	k1gInSc2	Boston
Thomasem	Thomas	k1gMnSc7	Thomas
Meninem	Menin	k1gMnSc7	Menin
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
organizaci	organizace	k1gFnSc4	organizace
Mayors	Mayorsa	k1gFnPc2	Mayorsa
Against	Against	k1gFnSc1	Against
Illegal	Illegal	k1gInSc1	Illegal
Guns	Guns	k1gInSc1	Guns
Coalition	Coalition	k1gInSc4	Coalition
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
zlepšit	zlepšit	k5eAaPmF	zlepšit
bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
situaci	situace	k1gFnSc4	situace
odstraněním	odstranění	k1gNnSc7	odstranění
nelegálně	legálně	k6eNd1	legálně
držených	držený	k2eAgFnPc2d1	držená
zbraní	zbraň	k1gFnPc2	zbraň
z	z	k7c2	z
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
velmi	velmi	k6eAd1	velmi
silnou	silný	k2eAgFnSc4d1	silná
členskou	členský	k2eAgFnSc4d1	členská
základnu	základna	k1gFnSc4	základna
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
má	mít	k5eAaImIp3nS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
66	[number]	k4	66
%	%	kIx~	%
registrovaných	registrovaný	k2eAgMnPc2d1	registrovaný
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
New	New	k1gFnSc6	New
York	York	k1gInSc4	York
City	city	k1gNnSc2	city
nevyhrál	vyhrát	k5eNaPmAgMnS	vyhrát
republikánský	republikánský	k2eAgMnSc1d1	republikánský
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
nebo	nebo	k8xC	nebo
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Tématy	téma	k1gNnPc7	téma
politických	politický	k2eAgFnPc2d1	politická
kampaní	kampaň	k1gFnPc2	kampaň
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
především	především	k9	především
finančně	finančně	k6eAd1	finančně
dostupné	dostupný	k2eAgNnSc4d1	dostupné
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
rozvoj	rozvoj	k1gInSc4	rozvoj
a	a	k8xC	a
podpora	podpora	k1gFnSc1	podpora
zaměstnanosti	zaměstnanost	k1gFnSc2	zaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
zdrojem	zdroj	k1gInSc7	zdroj
darů	dar	k1gInPc2	dar
politickým	politický	k2eAgFnPc3d1	politická
stranám	strana	k1gFnPc3	strana
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
z	z	k7c2	z
pěti	pět	k4xCc2	pět
poštovních	poštovní	k2eAgFnPc2d1	poštovní
směrovacích	směrovací	k2eAgFnPc2d1	směrovací
adres	adresa	k1gFnPc2	adresa
(	(	kIx(	(
<g/>
ZIP	zip	k1gInSc1	zip
Code	Cod	k1gInSc2	Cod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
obyvatelé	obyvatel	k1gMnPc1	obyvatel
darují	darovat	k5eAaPmIp3nP	darovat
nejvíce	hodně	k6eAd3	hodně
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
Rekordní	rekordní	k2eAgFnSc1d1	rekordní
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
směrovací	směrovací	k2eAgFnSc1d1	směrovací
adresa	adresa	k1gFnSc1	adresa
10021	[number]	k4	10021
na	na	k7c4	na
Upper	Upper	k1gInSc4	Upper
East	East	k2eAgInSc4d1	East
Side	Side	k1gInSc4	Side
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
získali	získat	k5eAaPmAgMnP	získat
při	při	k7c6	při
kampani	kampaň	k1gFnSc6	kampaň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
nejvíce	nejvíce	k6eAd1	nejvíce
peněz	peníze	k1gInPc2	peníze
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
oba	dva	k4xCgMnPc1	dva
prezidentští	prezidentský	k2eAgMnPc1d1	prezidentský
kandidáti	kandidát	k1gMnPc1	kandidát
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
jak	jak	k6eAd1	jak
John	John	k1gMnSc1	John
Kerry	Kerra	k1gFnSc2	Kerra
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
George	George	k1gFnPc2	George
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
nerovnoměrnost	nerovnoměrnost	k1gFnSc1	nerovnoměrnost
v	v	k7c6	v
objemu	objem	k1gInSc6	objem
daní	daň	k1gFnPc2	daň
zaplacených	zaplacený	k2eAgInPc2d1	zaplacený
ať	ať	k8xS	ať
už	už	k6eAd1	už
na	na	k7c4	na
státní	státní	k2eAgFnSc4d1	státní
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
federální	federální	k2eAgFnSc6d1	federální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
každý	každý	k3xTgInSc4	každý
dolar	dolar	k1gInSc4	dolar
fedrálních	fedrální	k2eAgFnPc2d1	fedrální
daní	daň	k1gFnPc2	daň
obdrží	obdržet	k5eAaPmIp3nS	obdržet
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
jen	jen	k9	jen
83	[number]	k4	83
centů	cent	k1gInPc2	cent
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ročně	ročně	k6eAd1	ročně
odevzdá	odevzdat	k5eAaPmIp3nS	odevzdat
na	na	k7c6	na
daních	daň	k1gFnPc6	daň
o	o	k7c4	o
11,4	[number]	k4	11,4
miliardy	miliarda	k4xCgFnSc2	miliarda
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dostane	dostat	k5eAaPmIp3nS	dostat
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
také	také	k9	také
odesílá	odesílat	k5eAaImIp3nS	odesílat
vládě	vláda	k1gFnSc3	vláda
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
o	o	k7c4	o
11	[number]	k4	11
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
obdrží	obdržet	k5eAaPmIp3nS	obdržet
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
radnice	radnice	k1gFnSc2	radnice
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
District	District	k1gMnSc1	District
Court	Courta	k1gFnPc2	Courta
for	forum	k1gNnPc2	forum
the	the	k?	the
Southern	Southern	k1gMnSc1	Southern
District	District	k1gMnSc1	District
of	of	k?	of
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
United	United	k1gInSc1	United
States	Statesa	k1gFnPc2	Statesa
Court	Courta	k1gFnPc2	Courta
of	of	k?	of
Appeals	Appealsa	k1gFnPc2	Appealsa
for	forum	k1gNnPc2	forum
the	the	k?	the
Second	Second	k1gMnSc1	Second
Circuit	Circuit	k1gMnSc1	Circuit
a	a	k8xC	a
Jacob	Jacoba	k1gFnPc2	Jacoba
K.	K.	kA	K.
Javits	Javitsa	k1gFnPc2	Javitsa
Federal	Federal	k1gFnSc1	Federal
Building	Building	k1gInSc1	Building
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
sídlí	sídlet	k5eAaImIp3nS	sídlet
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Supreme	Suprem	k1gInSc5	Suprem
Court	Courta	k1gFnPc2	Courta
<g/>
,	,	kIx,	,
Appellate	Appellat	k1gInSc5	Appellat
Division	Division	k1gInSc1	Division
<g/>
,	,	kIx,	,
First	First	k1gInSc1	First
Department	department	k1gInSc1	department
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brooklynu	Brooklyn	k1gInSc6	Brooklyn
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc1	sídlo
United	United	k1gInSc1	United
States	States	k1gMnSc1	States
District	Districta	k1gFnPc2	Districta
Court	Courta	k1gFnPc2	Courta
for	forum	k1gNnPc2	forum
the	the	k?	the
Eastern	Eastern	k1gMnSc1	Eastern
District	District	k1gMnSc1	District
of	of	k?	of
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
a	a	k8xC	a
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Supreme	Suprem	k1gInSc5	Suprem
Court	Courta	k1gFnPc2	Courta
<g/>
,	,	kIx,	,
Appellate	Appellat	k1gInSc5	Appellat
Division	Division	k1gInSc1	Division
<g/>
,	,	kIx,	,
Second	Second	k1gInSc1	Second
Department	department	k1gInSc1	department
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
sídlí	sídlet	k5eAaImIp3nS	sídlet
pobočka	pobočka	k1gFnSc1	pobočka
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Supreme	Suprem	k1gInSc5	Suprem
Court	Courta	k1gFnPc2	Courta
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
soudů	soud	k1gInPc2	soud
státu	stát	k1gInSc2	stát
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
přítomnosti	přítomnost	k1gFnSc3	přítomnost
sídla	sídlo	k1gNnSc2	sídlo
OSN	OSN	kA	OSN
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
122	[number]	k4	122
diplomatických	diplomatický	k2eAgFnPc2d1	diplomatická
misí	mise	k1gFnPc2	mise
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
veřejných	veřejný	k2eAgFnPc2d1	veřejná
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
spravuje	spravovat	k5eAaImIp3nS	spravovat
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
City	city	k1gNnSc1	city
Department	department	k1gInSc1	department
of	of	k?	of
Education	Education	k1gInSc1	Education
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
1,1	[number]	k4	1,1
milion	milion	k4xCgInSc4	milion
studentů	student	k1gMnPc2	student
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1200	[number]	k4	1200
škol	škola	k1gFnPc2	škola
prvního	první	k4xOgInSc2	první
a	a	k8xC	a
druhého	druhý	k4xOgInSc2	druhý
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
funguje	fungovat	k5eAaImIp3nS	fungovat
přibližně	přibližně	k6eAd1	přibližně
900	[number]	k4	900
dalších	další	k2eAgFnPc2d1	další
soukromých	soukromý	k2eAgFnPc2d1	soukromá
a	a	k8xC	a
církevních	církevní	k2eAgFnPc2d1	církevní
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejprestižnější	prestižní	k2eAgInPc4d3	nejprestižnější
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
běžně	běžně	k6eAd1	běžně
neoznačuje	označovat	k5eNaImIp3nS	označovat
jako	jako	k8xC	jako
univerzitní	univerzitní	k2eAgNnSc1d1	univerzitní
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
studuje	studovat	k5eAaImIp3nS	studovat
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
594	[number]	k4	594
000	[number]	k4	000
studentů	student	k1gMnPc2	student
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
měly	mít	k5eAaImAgFnP	mít
tři	tři	k4xCgFnPc1	tři
pětiny	pětina	k1gFnPc1	pětina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Manhattanu	Manhattan	k1gInSc6	Manhattan
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
navazující	navazující	k2eAgNnSc4d1	navazující
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
patří	patřit	k5eAaImIp3nS	patřit
Manhattan	Manhattan	k1gInSc1	Manhattan
mezi	mezi	k7c4	mezi
oblasti	oblast	k1gFnPc4	oblast
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
podílem	podíl	k1gInSc7	podíl
vysokoškolsky	vysokoškolsky	k6eAd1	vysokoškolsky
vzdělaných	vzdělaný	k2eAgMnPc2d1	vzdělaný
obyvatel	obyvatel	k1gMnPc2	obyvatel
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
City	city	k1gNnSc1	city
University	universita	k1gFnSc2	universita
of	of	k?	of
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc7	třetí
největší	veliký	k2eAgFnSc7d3	veliký
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
působí	působit	k5eAaImIp3nS	působit
veřejná	veřejný	k2eAgFnSc1d1	veřejná
Fashion	Fashion	k1gInSc1	Fashion
Institute	institut	k1gInSc5	institut
of	of	k?	of
Technology	technolog	k1gMnPc7	technolog
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
součást	součást	k1gFnSc4	součást
State	status	k1gInSc5	status
University	universita	k1gFnSc2	universita
of	of	k?	of
New	New	k1gMnSc4	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
soukromé	soukromý	k2eAgFnPc4d1	soukromá
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
patří	patřit	k5eAaImIp3nS	patřit
Barnard	Barnard	k1gInSc1	Barnard
College	Colleg	k1gFnSc2	Colleg
<g/>
,	,	kIx,	,
Columbia	Columbia	k1gFnSc1	Columbia
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
Cooper	Cooper	k1gMnSc1	Cooper
Union	union	k1gInSc1	union
<g/>
,	,	kIx,	,
Fordham	Fordham	k1gInSc1	Fordham
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
New	New	k1gFnPc1	New
York	York	k1gInSc1	York
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
New	New	k1gFnSc2	New
School	School	k1gInSc1	School
a	a	k8xC	a
Yeshiva	Yeshiva	k1gFnSc1	Yeshiva
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
medicínského	medicínský	k2eAgInSc2d1	medicínský
a	a	k8xC	a
biologického	biologický	k2eAgInSc2d1	biologický
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
udělováno	udělovat	k5eAaImNgNnS	udělovat
nejvíce	nejvíce	k6eAd1	nejvíce
postgraduálních	postgraduální	k2eAgInPc2d1	postgraduální
titulů	titul	k1gInPc2	titul
v	v	k7c6	v
biologii	biologie	k1gFnSc6	biologie
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
40	[number]	k4	40
000	[number]	k4	000
lékařů	lékař	k1gMnPc2	lékař
a	a	k8xC	a
127	[number]	k4	127
laureátů	laureát	k1gMnPc2	laureát
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
působilo	působit	k5eAaImAgNnS	působit
v	v	k7c6	v
místních	místní	k2eAgFnPc6d1	místní
institucích	instituce	k1gFnPc6	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
dostává	dostávat	k5eAaImIp3nS	dostávat
druhé	druhý	k4xOgFnPc4	druhý
nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
dotace	dotace	k1gFnPc4	dotace
od	od	k7c2	od
National	National	k1gFnSc2	National
Institutes	Institutes	k1gInSc1	Institutes
of	of	k?	of
Health	Health	k1gInSc4	Health
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
amerických	americký	k2eAgNnPc2d1	americké
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
sídlí	sídlet	k5eAaImIp3nS	sídlet
mnoho	mnoho	k4c1	mnoho
biomedicínských	biomedicínský	k2eAgFnPc2d1	biomedicínská
výzkumných	výzkumný	k2eAgFnPc2d1	výzkumná
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Memorial	Memorial	k1gInSc1	Memorial
Sloan-Kettering	Sloan-Kettering	k1gInSc1	Sloan-Kettering
Cancer	Cancra	k1gFnPc2	Cancra
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
Rockefeller	Rockefeller	k1gMnSc1	Rockefeller
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
SUNY	suna	k1gFnSc2	suna
Downstate	Downstat	k1gInSc5	Downstat
Medical	Medical	k1gFnPc1	Medical
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
College	Colleg	k1gFnSc2	Colleg
of	of	k?	of
Medicine	Medicin	k1gMnSc5	Medicin
<g/>
,	,	kIx,	,
Mount	Mount	k1gMnSc1	Mount
Sinai	Sina	k1gFnSc2	Sina
School	School	k1gInSc1	School
of	of	k?	of
Medicine	Medicin	k1gInSc5	Medicin
a	a	k8xC	a
Weill	Weillum	k1gNnPc2	Weillum
Cornell	Cornella	k1gFnPc2	Cornella
Medical	Medical	k1gFnSc1	Medical
College	College	k1gFnSc1	College
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
Public	publicum	k1gNnPc2	publicum
Library	Librara	k1gFnSc2	Librara
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
veřejná	veřejný	k2eAgFnSc1d1	veřejná
knihovna	knihovna	k1gFnSc1	knihovna
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
pobočky	pobočka	k1gFnPc4	pobočka
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
,	,	kIx,	,
v	v	k7c6	v
Bronxu	Bronx	k1gInSc6	Bronx
a	a	k8xC	a
na	na	k7c4	na
Staten	Staten	k2eAgInSc4d1	Staten
Islandu	Island	k1gInSc3	Island
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Queensu	Queens	k1gInSc6	Queens
sídlí	sídlet	k5eAaImIp3nS	sídlet
Queens	Queens	k1gInSc1	Queens
Borough	Borougha	k1gFnPc2	Borougha
Public	publicum	k1gNnPc2	publicum
Library	Librar	k1gInPc7	Librar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
americká	americký	k2eAgFnSc1d1	americká
veřejná	veřejný	k2eAgFnSc1d1	veřejná
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brooklynu	Brooklyn	k1gInSc6	Brooklyn
sídlí	sídlet	k5eAaImIp3nS	sídlet
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
Public	publicum	k1gNnPc2	publicum
Library	Librara	k1gFnSc2	Librara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Public	publicum	k1gNnPc2	publicum
Library	Librara	k1gFnSc2	Librara
působí	působit	k5eAaImIp3nS	působit
několik	několik	k4yIc4	několik
výzkumných	výzkumný	k2eAgFnPc2d1	výzkumná
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Schomburg	Schomburg	k1gInSc1	Schomburg
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
Research	Research	k1gInSc1	Research
in	in	k?	in
Black	Black	k1gInSc1	Black
Culture	Cultur	k1gInSc5	Cultur
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nejelitnějších	elitní	k2eAgFnPc2d3	nejelitnější
a	a	k8xC	a
nejexkluzivnějších	exkluzivní	k2eAgFnPc2d3	nejexkluzivnější
soukromých	soukromý	k2eAgFnPc2d1	soukromá
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
například	například	k6eAd1	například
Brearley	Brearle	k1gMnPc4	Brearle
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
Dalton	Dalton	k1gInSc1	Dalton
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
Spence	Spenec	k1gInSc2	Spenec
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
The	The	k1gFnSc7	The
Chapin	Chapin	k2eAgInSc1d1	Chapin
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
Nightingale-Bamford	Nightingale-Bamford	k1gInSc1	Nightingale-Bamford
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
Convent	Convent	k1gInSc1	Convent
of	of	k?	of
the	the	k?	the
Sacred	Sacred	k1gInSc1	Sacred
Heart	Hearta	k1gFnPc2	Hearta
na	na	k7c4	na
Upper	Upper	k1gInSc4	Upper
East	East	k2eAgInSc4d1	East
Side	Side	k1gInSc4	Side
<g/>
;	;	kIx,	;
Collegiate	Collegiat	k1gMnSc5	Collegiat
School	School	k1gInSc4	School
a	a	k8xC	a
Trinity	Trinit	k1gInPc4	Trinit
School	Schoola	k1gFnPc2	Schoola
na	na	k7c4	na
Upper	Upper	k1gInSc4	Upper
West	West	k2eAgInSc1d1	West
Side	Side	k1gInSc1	Side
<g/>
;	;	kIx,	;
Horace	Horace	k1gFnSc1	Horace
Mann	Mann	k1gMnSc1	Mann
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
Ethical	Ethical	k1gMnSc5	Ethical
Culture	Cultur	k1gMnSc5	Cultur
Fieldston	Fieldston	k1gInSc4	Fieldston
School	Schoola	k1gFnPc2	Schoola
a	a	k8xC	a
Riverdale	Riverdala	k1gFnSc3	Riverdala
Country	country	k2eAgFnPc2d1	country
School	Schoola	k1gFnPc2	Schoola
v	v	k7c6	v
Riverdale	Riverdala	k1gFnSc6	Riverdala
v	v	k7c6	v
Bronxu	Bronx	k1gInSc6	Bronx
<g/>
;	;	kIx,	;
a	a	k8xC	a
Saint	Saint	k1gMnSc1	Saint
Ann	Ann	k1gMnSc1	Ann
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
School	School	k1gInSc1	School
v	v	k7c4	v
Brooklyn	Brooklyn	k1gInSc4	Brooklyn
Heights	Heightsa	k1gFnPc2	Heightsa
v	v	k7c6	v
Brooklynu	Brooklyn	k1gInSc6	Brooklyn
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
některé	některý	k3yIgFnSc2	některý
veřejné	veřejný	k2eAgFnSc2d1	veřejná
školy	škola	k1gFnSc2	škola
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Hunter	Hunter	k1gMnSc1	Hunter
College	Colleg	k1gFnSc2	Colleg
High	High	k1gMnSc1	High
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
Stuyvesant	Stuyvesant	k1gInSc1	Stuyvesant
High	Higha	k1gFnPc2	Higha
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Bronx	Bronx	k1gInSc1	Bronx
High	High	k1gMnSc1	High
School	School	k1gInSc1	School
of	of	k?	of
Science	Science	k1gFnSc1	Science
<g/>
,	,	kIx,	,
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
Technical	Technical	k1gFnSc2	Technical
High	Higha	k1gFnPc2	Higha
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
Bard	bard	k1gMnSc1	bard
High	High	k1gMnSc1	High
School	School	k1gInSc4	School
Early	earl	k1gMnPc7	earl
College	College	k1gNnSc2	College
<g/>
,	,	kIx,	,
Townsend	Townsend	k1gMnSc1	Townsend
Harris	Harris	k1gFnSc2	Harris
High	High	k1gMnSc1	High
School	School	k1gInSc1	School
a	a	k8xC	a
LaGuardia	LaGuardium	k1gNnSc2	LaGuardium
High	Higha	k1gFnPc2	Higha
School	Schoola	k1gFnPc2	Schoola
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Londýnem	Londýn	k1gInSc7	Londýn
a	a	k8xC	a
Tokiem	Tokio	k1gNnSc7	Tokio
hlavním	hlavní	k2eAgNnSc7d1	hlavní
centrem	centrum	k1gNnSc7	centrum
globální	globální	k2eAgFnSc2d1	globální
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
amerického	americký	k2eAgNnSc2d1	americké
finančnictví	finančnictví	k1gNnSc2	finančnictví
<g/>
,	,	kIx,	,
pojišťovnictví	pojišťovnictví	k1gNnSc2	pojišťovnictví
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
nemovitostmi	nemovitost	k1gFnPc7	nemovitost
<g/>
.	.	kIx.	.
</s>
<s>
Odhadovaný	odhadovaný	k2eAgInSc1d1	odhadovaný
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
newyorské	newyorský	k2eAgFnSc2d1	newyorská
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
952,6	[number]	k4	952,6
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
newyorské	newyorský	k2eAgFnSc2d1	newyorská
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
činí	činit	k5eAaImIp3nS	činit
největší	veliký	k2eAgFnSc4d3	veliký
místní	místní	k2eAgFnSc4d1	místní
ekonomiku	ekonomika	k1gFnSc4	ekonomika
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
koncentrována	koncentrován	k2eAgFnSc1d1	koncentrována
většina	většina	k1gFnSc1	většina
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
aktivity	aktivita	k1gFnSc2	aktivita
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
a	a	k8xC	a
New	New	k1gFnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
sídlí	sídlet	k5eAaImIp3nS	sídlet
mnoho	mnoho	k4c1	mnoho
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
obchodních	obchodní	k2eAgFnPc2d1	obchodní
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
44	[number]	k4	44
společností	společnost	k1gFnPc2	společnost
ze	z	k7c2	z
seznamu	seznam	k1gInSc2	seznam
Fortune	Fortun	k1gInSc5	Fortun
500	[number]	k4	500
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
unikátní	unikátní	k2eAgInSc4d1	unikátní
i	i	k9	i
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
společností	společnost	k1gFnPc2	společnost
působících	působící	k2eAgFnPc2d1	působící
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
desetinu	desetina	k1gFnSc4	desetina
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
soukromém	soukromý	k2eAgInSc6d1	soukromý
sektoru	sektor	k1gInSc6	sektor
tvoří	tvořit	k5eAaImIp3nP	tvořit
zahraniční	zahraniční	k2eAgFnPc1d1	zahraniční
společnosti	společnost	k1gFnPc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
několik	několik	k4yIc1	několik
z	z	k7c2	z
nejdražších	drahý	k2eAgFnPc2d3	nejdražší
světových	světový	k2eAgFnPc2d1	světová
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
prodána	prodán	k2eAgFnSc1d1	prodána
budova	budova	k1gFnSc1	budova
č.	č.	k?	č.
450	[number]	k4	450
na	na	k7c4	na
Park	park	k1gInSc4	park
Avenue	avenue	k1gFnSc2	avenue
za	za	k7c4	za
510	[number]	k4	510
milionů	milion	k4xCgInPc2	milion
USD	USD	kA	USD
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
1	[number]	k4	1
m	m	kA	m
<g/>
2	[number]	k4	2
stál	stát	k5eAaImAgInS	stát
17	[number]	k4	17
104	[number]	k4	104
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgInSc1d1	předchozí
rekord	rekord	k1gInSc1	rekord
držela	držet	k5eAaImAgFnS	držet
jeden	jeden	k4xCgInSc4	jeden
měsíc	měsíc	k1gInSc4	měsíc
budova	budova	k1gFnSc1	budova
č.	č.	k?	č.
660	[number]	k4	660
na	na	k7c4	na
Madison	Madison	k1gNnSc4	Madison
Avenue	avenue	k1gFnSc2	avenue
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
1	[number]	k4	1
m	m	kA	m
<g/>
2	[number]	k4	2
stál	stát	k5eAaImAgInS	stát
15	[number]	k4	15
887	[number]	k4	887
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgInSc1d1	střední
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
obchodním	obchodní	k2eAgNnSc7d1	obchodní
centrem	centrum	k1gNnSc7	centrum
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nejvíce	nejvíce	k6eAd1	nejvíce
newyorských	newyorský	k2eAgInPc2d1	newyorský
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgInSc1d1	dolní
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
třetím	třetí	k4xOgInSc7	třetí
největším	veliký	k2eAgInSc7d3	veliký
americkým	americký	k2eAgInSc7d1	americký
obchodním	obchodní	k2eAgInSc7d1	obchodní
centrem	centr	k1gInSc7	centr
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
Stock	Stock	k1gMnSc1	Stock
Exchange	Exchange	k1gInSc1	Exchange
a	a	k8xC	a
NASDAQ	NASDAQ	kA	NASDAQ
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
největší	veliký	k2eAgFnPc4d3	veliký
světové	světový	k2eAgFnPc4d1	světová
burzy	burza	k1gFnPc4	burza
cenných	cenný	k2eAgInPc2d1	cenný
papírů	papír	k1gInPc2	papír
co	co	k9	co
do	do	k7c2	do
průměrného	průměrný	k2eAgInSc2d1	průměrný
denního	denní	k2eAgInSc2d1	denní
objemu	objem	k1gInSc2	objem
obchodů	obchod	k1gInPc2	obchod
a	a	k8xC	a
celkové	celkový	k2eAgFnSc2d1	celková
tržní	tržní	k2eAgFnSc2d1	tržní
kapitalizace	kapitalizace	k1gFnSc2	kapitalizace
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
35	[number]	k4	35
%	%	kIx~	%
procent	procento	k1gNnPc2	procento
příjmů	příjem	k1gInPc2	příjem
ekonomiky	ekonomika	k1gFnSc2	ekonomika
generuje	generovat	k5eAaImIp3nS	generovat
finanční	finanční	k2eAgInSc1d1	finanční
sektor	sektor	k1gInSc1	sektor
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
faktorem	faktor	k1gInSc7	faktor
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
ekonomice	ekonomika	k1gFnSc6	ekonomika
je	být	k5eAaImIp3nS	být
i	i	k9	i
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
nemovitostmi	nemovitost	k1gFnPc7	nemovitost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
cena	cena	k1gFnSc1	cena
všech	všecek	k3xTgFnPc2	všecek
newyorských	newyorský	k2eAgFnPc2d1	newyorská
nemovitostí	nemovitost	k1gFnPc2	nemovitost
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
odhadovala	odhadovat	k5eAaImAgFnS	odhadovat
na	na	k7c4	na
802,4	[number]	k4	802,4
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Nejhodnotnější	hodnotný	k2eAgFnSc7d3	nejhodnotnější
nemovitostí	nemovitost	k1gFnSc7	nemovitost
je	být	k5eAaImIp3nS	být
Time	Time	k1gNnSc1	Time
Warner	Warnra	k1gFnPc2	Warnra
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
tržní	tržní	k2eAgFnSc1d1	tržní
cena	cena	k1gFnSc1	cena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
1,1	[number]	k4	1,1
miliardu	miliarda	k4xCgFnSc4	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgInSc1d1	místní
televizní	televizní	k2eAgInSc1d1	televizní
a	a	k8xC	a
filmový	filmový	k2eAgInSc1d1	filmový
průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
Hollywoodu	Hollywood	k1gInSc6	Hollywood
druhý	druhý	k4xOgMnSc1	druhý
největší	veliký	k2eAgFnSc4d3	veliký
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Kreativní	kreativní	k2eAgNnPc1d1	kreativní
odvětví	odvětví	k1gNnPc1	odvětví
jako	jako	k8xS	jako
média	médium	k1gNnPc1	médium
<g/>
,	,	kIx,	,
reklama	reklama	k1gFnSc1	reklama
<g/>
,	,	kIx,	,
móda	móda	k1gFnSc1	móda
<g/>
,	,	kIx,	,
design	design	k1gInSc1	design
a	a	k8xC	a
architektura	architektura	k1gFnSc1	architektura
zaměstnávají	zaměstnávat	k5eAaImIp3nP	zaměstnávat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
oborech	obor	k1gInPc6	obor
získává	získávat	k5eAaImIp3nS	získávat
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
městy	město	k1gNnPc7	město
výraznou	výrazný	k2eAgFnSc4d1	výrazná
kompetitivní	kompetitivní	k2eAgFnSc4d1	kompetitivní
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Vzestupnou	vzestupný	k2eAgFnSc4d1	vzestupná
tendenci	tendence	k1gFnSc4	tendence
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
také	také	k9	také
hi-tech	hi	k1gInPc6	hi-t
obory	obora	k1gFnPc1	obora
jako	jako	k8xC	jako
biomechanika	biomechanika	k1gFnSc1	biomechanika
<g/>
,	,	kIx,	,
softwarové	softwarový	k2eAgNnSc1d1	softwarové
inženýrství	inženýrství	k1gNnSc1	inženýrství
<g/>
,	,	kIx,	,
herní	herní	k2eAgInSc1d1	herní
design	design	k1gInSc1	design
a	a	k8xC	a
internetové	internetový	k2eAgFnPc1d1	internetová
služby	služba	k1gFnPc1	služba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ještě	ještě	k9	ještě
zvýrazňuje	zvýrazňovat	k5eAaImIp3nS	zvýrazňovat
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
končí	končit	k5eAaImIp3nS	končit
podmořské	podmořský	k2eAgInPc4d1	podmořský
optické	optický	k2eAgInPc4d1	optický
kabely	kabel	k1gInPc4	kabel
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
postupně	postupně	k6eAd1	postupně
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
produkty	produkt	k1gInPc4	produkt
vyráběné	vyráběný	k2eAgInPc4d1	vyráběný
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
patří	patřit	k5eAaImIp3nS	patřit
oděvy	oděv	k1gInPc4	oděv
<g/>
,	,	kIx,	,
chemikálie	chemikálie	k1gFnPc4	chemikálie
<g/>
,	,	kIx,	,
kovové	kovový	k2eAgFnPc4d1	kovová
výroby	výroba	k1gFnPc4	výroba
<g/>
,	,	kIx,	,
potraviny	potravina	k1gFnPc4	potravina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nábytek	nábytek	k1gInSc1	nábytek
<g/>
.	.	kIx.	.
</s>
<s>
Nejstabilnějším	stabilní	k2eAgInSc7d3	nejstabilnější
sektorem	sektor	k1gInSc7	sektor
je	být	k5eAaImIp3nS	být
potravinářství	potravinářství	k1gNnSc1	potravinářství
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
potravin	potravina	k1gFnPc2	potravina
je	být	k5eAaImIp3nS	být
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
19000	[number]	k4	19000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
imigrantů	imigrant	k1gMnPc2	imigrant
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
znalostí	znalost	k1gFnSc7	znalost
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
roční	roční	k2eAgInSc1d1	roční
obrat	obrat	k1gInSc1	obrat
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
5	[number]	k4	5
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Specifickým	specifický	k2eAgInSc7d1	specifický
newyorským	newyorský	k2eAgInSc7d1	newyorský
exportním	exportní	k2eAgInSc7d1	exportní
artiklem	artikl	k1gInSc7	artikl
je	být	k5eAaImIp3nS	být
čokoláda	čokoláda	k1gFnSc1	čokoláda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
export	export	k1gInSc4	export
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
ročně	ročně	k6eAd1	ročně
hodnoty	hodnota	k1gFnPc4	hodnota
asi	asi	k9	asi
234	[number]	k4	234
milionů	milion	k4xCgInPc2	milion
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
velmi	velmi	k6eAd1	velmi
důležitá	důležitý	k2eAgFnSc1d1	důležitá
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
jedna	jeden	k4xCgFnSc1	jeden
třetina	třetina	k1gFnSc1	třetina
amerických	americký	k2eAgMnPc2d1	americký
uživatelů	uživatel	k1gMnPc2	uživatel
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
třetina	třetina	k1gFnSc1	třetina
uživatelů	uživatel	k1gMnPc2	uživatel
železnice	železnice	k1gFnSc2	železnice
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
okolních	okolní	k2eAgNnPc6d1	okolní
předměstích	předměstí	k1gNnPc6	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
země	zem	k1gFnSc2	zem
naopak	naopak	k6eAd1	naopak
pro	pro	k7c4	pro
dojíždění	dojíždění	k1gNnSc4	dojíždění
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
používá	používat	k5eAaImIp3nS	používat
90	[number]	k4	90
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
automobily	automobil	k1gInPc1	automobil
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgNnSc7d1	jediné
městem	město	k1gNnSc7	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
většina	většina	k1gFnSc1	většina
domácností	domácnost	k1gFnPc2	domácnost
nevlastní	vlastnit	k5eNaImIp3nS	vlastnit
automobil	automobil	k1gInSc1	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
jej	on	k3xPp3gInSc4	on
nevlastní	vlastní	k2eNgInSc4d1	nevlastní
75	[number]	k4	75
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
celostátní	celostátní	k2eAgInSc1d1	celostátní
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
8	[number]	k4	8
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgMnSc1d1	průměrný
obyvatel	obyvatel	k1gMnSc1	obyvatel
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
stráví	strávit	k5eAaPmIp3nS	strávit
dojížděním	dojíždění	k1gNnSc7	dojíždění
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
jeden	jeden	k4xCgInSc4	jeden
týden	týden	k1gInSc4	týden
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
38,4	[number]	k4	38,4
minuty	minuta	k1gFnSc2	minuta
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
z	z	k7c2	z
velkých	velký	k2eAgNnPc2d1	velké
amerických	americký	k2eAgNnPc2d1	americké
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Newyorské	newyorský	k2eAgNnSc1d1	newyorské
metro	metro	k1gNnSc1	metro
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
City	City	k1gFnSc2	City
Subway	Subwaa	k1gFnSc2	Subwaa
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
pouze	pouze	k6eAd1	pouze
subway	subwa	k2eAgInPc1d1	subwa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
systémem	systém	k1gInSc7	systém
metra	metro	k1gNnSc2	metro
na	na	k7c6	na
světě	svět	k1gInSc6	svět
co	co	k9	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
kterých	který	k3yIgFnPc2	který
je	být	k5eAaImIp3nS	být
468	[number]	k4	468
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
metro	metro	k1gNnSc1	metro
přepraví	přepravit	k5eAaPmIp3nS	přepravit
1,5	[number]	k4	1,5
miliardy	miliarda	k4xCgFnSc2	miliarda
cestujících	cestující	k1gFnPc2	cestující
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgMnSc1	třetí
největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Newyorské	newyorský	k2eAgNnSc1d1	newyorské
metro	metro	k1gNnSc1	metro
je	být	k5eAaImIp3nS	být
unikátní	unikátní	k2eAgNnSc1d1	unikátní
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
linek	linka	k1gFnPc2	linka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
kontrastuje	kontrastovat	k5eAaImIp3nS	kontrastovat
s	s	k7c7	s
londýnským	londýnský	k2eAgNnSc7d1	Londýnské
<g/>
,	,	kIx,	,
pařížským	pařížský	k2eAgNnSc7d1	pařížské
či	či	k8xC	či
tokijským	tokijský	k2eAgNnSc7d1	Tokijské
metrem	metro	k1gNnSc7	metro
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
uzavřeny	uzavřen	k2eAgInPc1d1	uzavřen
<g/>
.	.	kIx.	.
</s>
<s>
Verrzano-Narrows	Verrzano-Narrows	k6eAd1	Verrzano-Narrows
Bridge	Bridge	k1gInSc1	Bridge
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
visutý	visutý	k2eAgInSc1d1	visutý
most	most	k1gInSc1	most
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
Hollandův	Hollandův	k2eAgInSc1d1	Hollandův
tunel	tunel	k1gInSc1	tunel
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgNnSc7	první
mechanicky	mechanicky	k6eAd1	mechanicky
odvětrávaným	odvětrávaný	k2eAgInSc7d1	odvětrávaný
tunelem	tunel	k1gInSc7	tunel
určeným	určený	k2eAgInSc7d1	určený
pro	pro	k7c4	pro
automobily	automobil	k1gInPc4	automobil
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
provozováno	provozovat	k5eAaImNgNnS	provozovat
asi	asi	k9	asi
12	[number]	k4	12
000	[number]	k4	000
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
žlutých	žlutý	k2eAgInPc2d1	žlutý
taxíků	taxík	k1gInPc2	taxík
<g/>
,	,	kIx,	,
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
Island	Island	k1gInSc4	Island
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
Manhattanem	Manhattan	k1gInSc7	Manhattan
spojen	spojit	k5eAaPmNgInS	spojit
lanovkou	lanovka	k1gFnSc7	lanovka
a	a	k8xC	a
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
místy	místo	k1gNnPc7	místo
uvnitř	uvnitř	k6eAd1	uvnitř
i	i	k9	i
mimo	mimo	k7c4	mimo
hranice	hranice	k1gFnPc4	hranice
města	město	k1gNnSc2	město
spojen	spojit	k5eAaPmNgInS	spojit
trajekty	trajekt	k1gInPc1	trajekt
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgFnSc4d3	veliký
flotilu	flotila	k1gFnSc4	flotila
autobusů	autobus	k1gInPc2	autobus
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
spojující	spojující	k2eAgFnSc2d1	spojující
předměstské	předměstský	k2eAgFnSc2d1	předměstská
oblasti	oblast	k1gFnSc2	oblast
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
a	a	k8xC	a
Connecticut	Connecticut	k1gInSc1	Connecticut
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
250	[number]	k4	250
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
20	[number]	k4	20
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dvě	dva	k4xCgFnPc4	dva
nejrušnější	rušný	k2eAgFnPc4d3	nejrušnější
železniční	železniční	k2eAgFnPc4d1	železniční
stanice	stanice	k1gFnPc4	stanice
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Grand	grand	k1gMnSc1	grand
Central	Central	k1gFnSc2	Central
Terminal	Terminal	k1gFnSc2	Terminal
a	a	k8xC	a
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
Station	station	k1gInSc1	station
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
směřuje	směřovat	k5eAaImIp3nS	směřovat
nejvíce	hodně	k6eAd3	hodně
pasažérů	pasažér	k1gMnPc2	pasažér
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
leteckých	letecký	k2eAgFnPc2d1	letecká
linek	linka	k1gFnPc2	linka
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
fungují	fungovat	k5eAaImIp3nP	fungovat
tři	tři	k4xCgNnPc1	tři
velká	velký	k2eAgNnPc1d1	velké
letiště	letiště	k1gNnPc1	letiště
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
F.	F.	kA	F.
Kennedy	Kenneda	k1gMnSc2	Kenneda
International	International	k1gMnSc2	International
<g/>
,	,	kIx,	,
Newark	Newark	k1gInSc1	Newark
Liberty	Libert	k1gInPc7	Libert
International	International	k1gFnSc2	International
a	a	k8xC	a
LaGuardia	LaGuardium	k1gNnSc2	LaGuardium
a	a	k8xC	a
plánuje	plánovat	k5eAaImIp3nS	plánovat
se	se	k3xPyFc4	se
rozšíření	rozšíření	k1gNnSc1	rozšíření
Stewart	Stewart	k1gMnSc1	Stewart
International	International	k1gMnSc1	International
Airport	Airport	k1gInSc4	Airport
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
Newburghu	Newburgh	k1gInSc2	Newburgh
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
tři	tři	k4xCgNnPc1	tři
letiště	letiště	k1gNnPc1	letiště
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
využilo	využít	k5eAaPmAgNnS	využít
asi	asi	k9	asi
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
a	a	k8xC	a
vzdušný	vzdušný	k2eAgInSc4d1	vzdušný
prostor	prostor	k1gInSc4	prostor
nad	nad	k7c4	nad
New	New	k1gFnSc4	New
Yorkem	York	k1gInSc7	York
tak	tak	k8xS	tak
byl	být	k5eAaImAgMnS	být
nejhustější	hustý	k2eAgMnSc1d3	nejhustší
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
Američanů	Američan	k1gMnPc2	Američan
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
letěli	letět	k5eAaImAgMnP	letět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
využila	využít	k5eAaPmAgFnS	využít
letiště	letiště	k1gNnSc4	letiště
JFK	JFK	kA	JFK
a	a	k8xC	a
Newark	Newark	k1gInSc1	Newark
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
masivnímu	masivní	k2eAgNnSc3d1	masivní
využívání	využívání	k1gNnSc3	využívání
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
120	[number]	k4	120
000	[number]	k4	000
cyklistům	cyklista	k1gMnPc3	cyklista
a	a	k8xC	a
mnoha	mnoho	k4c3	mnoho
pěším	pěší	k1gMnPc3	pěší
je	být	k5eAaImIp3nS	být
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
energeticky	energeticky	k6eAd1	energeticky
nejefektivnějším	efektivní	k2eAgNnSc7d3	nejefektivnější
velkým	velký	k2eAgNnSc7d1	velké
městem	město	k1gNnSc7	město
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
pěší	pěší	k2eAgFnSc1d1	pěší
chůze	chůze	k1gFnSc1	chůze
a	a	k8xC	a
jízda	jízda	k1gFnSc1	jízda
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
využívána	využívat	k5eAaPmNgFnS	využívat
jako	jako	k8xS	jako
forma	forma	k1gFnSc1	forma
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
8	[number]	k4	8
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
21	[number]	k4	21
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
síť	síť	k1gFnSc4	síť
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
spojují	spojovat	k5eAaImIp3nP	spojovat
město	město	k1gNnSc4	město
s	s	k7c7	s
New	New	k1gFnSc7	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
,	,	kIx,	,
Westchester	Westchester	k1gInSc4	Westchester
County	Counta	k1gFnSc2	Counta
<g/>
,	,	kIx,	,
Long	Long	k1gMnSc1	Long
Islandem	Island	k1gInSc7	Island
a	a	k8xC	a
Connecticutem	Connecticut	k1gInSc7	Connecticut
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dálnice	dálnice	k1gFnPc1	dálnice
využívají	využívat	k5eAaPmIp3nP	využívat
k	k	k7c3	k
dojíždění	dojíždění	k1gNnSc3	dojíždění
miliony	milion	k4xCgInPc4	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
tudíž	tudíž	k8xC	tudíž
běžné	běžný	k2eAgFnPc4d1	běžná
dopravní	dopravní	k2eAgFnPc4d1	dopravní
zácpy	zácpa	k1gFnPc4	zácpa
<g/>
,	,	kIx,	,
především	především	k9	především
během	během	k7c2	během
dopravní	dopravní	k2eAgFnSc2d1	dopravní
špičky	špička	k1gFnSc2	špička
<g/>
.	.	kIx.	.
</s>
<s>
George	George	k6eAd1	George
Washington	Washington	k1gInSc1	Washington
Bridge	Bridg	k1gFnSc2	Bridg
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejvytíženějších	vytížený	k2eAgInPc2d3	nejvytíženější
mostů	most	k1gInPc2	most
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Manhattan	Manhattan	k1gInSc4	Manhattan
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
dělení	dělení	k1gNnSc1	dělení
ulic	ulice	k1gFnPc2	ulice
na	na	k7c4	na
streets	streets	k1gInSc4	streets
vedoucí	vedoucí	k1gMnSc1	vedoucí
z	z	k7c2	z
východu	východ	k1gInSc2	východ
na	na	k7c4	na
západ	západ	k1gInSc4	západ
a	a	k8xC	a
avenues	avenues	k1gMnSc1	avenues
vedoucí	vedoucí	k1gMnSc1	vedoucí
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
newyorských	newyorský	k2eAgFnPc2d1	newyorská
ulic	ulice	k1gFnPc2	ulice
je	být	k5eAaImIp3nS	být
celosvětově	celosvětově	k6eAd1	celosvětově
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Fifth	Fifth	k1gInSc1	Fifth
Avenue	avenue	k1gFnSc2	avenue
s	s	k7c7	s
luxusními	luxusní	k2eAgInPc7d1	luxusní
obchody	obchod	k1gInPc7	obchod
a	a	k8xC	a
obchodním	obchodní	k2eAgInSc7d1	obchodní
domem	dům	k1gInSc7	dům
Macy	Maca	k1gFnSc2	Maca
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
Wall	Wall	k1gInSc4	Wall
Street	Streeta	k1gFnPc2	Streeta
jako	jako	k8xS	jako
sídlo	sídlo	k1gNnSc4	sídlo
burzy	burza	k1gFnSc2	burza
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Broadway	Broadwaa	k1gFnPc1	Broadwaa
jako	jako	k8xS	jako
sídlo	sídlo	k1gNnSc1	sídlo
muzikálových	muzikálový	k2eAgNnPc2d1	muzikálové
divadel	divadlo	k1gNnPc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
má	mít	k5eAaImIp3nS	mít
deset	deset	k4xCc4	deset
partnerských	partnerský	k2eAgNnPc2d1	partnerské
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
devět	devět	k4xCc4	devět
je	být	k5eAaImIp3nS	být
členy	člen	k1gMnPc4	člen
Sister	Sister	k1gMnSc1	Sister
Cities	Cities	k1gMnSc1	Cities
International	International	k1gMnSc1	International
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
City	City	k1gFnSc2	City
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Edwin	Edwin	k1gInSc1	Edwin
G.	G.	kA	G.
Burrows	Burrows	k1gInSc1	Burrows
a	a	k8xC	a
Mike	Mike	k1gFnSc1	Mike
Wallace	Wallace	k1gFnSc1	Wallace
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gotham	Gotham	k1gInSc1	Gotham
<g/>
:	:	kIx,	:
A	a	k8xC	a
History	Histor	k1gInPc1	Histor
of	of	k?	of
New	New	k1gMnSc4	New
York	York	k1gInSc1	York
City	city	k1gNnSc1	city
to	ten	k3xDgNnSc1	ten
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
Anthony	Anthon	k1gInPc1	Anthon
Burgess	Burgessa	k1gFnPc2	Burgessa
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
]	]	kIx)	]
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
Little	Little	k1gFnSc1	Little
<g/>
,	,	kIx,	,
Brown	Brown	k1gInSc1	Brown
&	&	k?	&
Co	co	k3yRnSc4	co
<g/>
.	.	kIx.	.
</s>
<s>
Federal	Federat	k5eAaPmAgInS	Federat
Writers	Writers	k1gInSc1	Writers
<g/>
'	'	kIx"	'
Project	Project	k1gInSc1	Project
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
WPA	WPA	kA	WPA
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc1	ten
New	New	k1gFnPc4	New
York	York	k1gInSc4	York
City	city	k1gNnSc1	city
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
New	New	k1gMnSc1	New
Press	Press	k1gInSc1	Press
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
reissue	reissue	k1gNnPc2	reissue
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kenneth	Kenneth	k1gMnSc1	Kenneth
T.	T.	kA	T.
Jackson	Jackson	k1gMnSc1	Jackson
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
of	of	k?	of
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
City	City	k1gFnSc1	City
<g/>
,	,	kIx,	,
Yale	Yale	k1gNnSc1	Yale
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
Kenneth	Kenneth	k1gMnSc1	Kenneth
T.	T.	kA	T.
Jackson	Jackson	k1gMnSc1	Jackson
and	and	k?	and
David	David	k1gMnSc1	David
S.	S.	kA	S.
Dunbar	Dunbar	k1gMnSc1	Dunbar
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Empire	empir	k1gInSc5	empir
City	city	k1gNnSc1	city
<g/>
:	:	kIx,	:
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Through	Through	k1gMnSc1	Through
the	the	k?	the
Centuries	Centuries	k1gMnSc1	Centuries
<g/>
,	,	kIx,	,
Columbia	Columbia	k1gFnSc1	Columbia
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
E.	E.	kA	E.
B.	B.	kA	B.
White	Whit	k1gInSc5	Whit
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Here	Here	k1gFnSc1	Here
is	is	k?	is
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Little	Little	k1gFnSc1	Little
Bookroom	Bookroom	k1gInSc1	Bookroom
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
reissue	reissue	k1gNnPc2	reissue
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Colson	Colson	k1gInSc1	Colson
Whitehead	Whitehead	k1gInSc1	Whitehead
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Colossus	Colossus	k1gInSc1	Colossus
of	of	k?	of
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
A	a	k9	a
City	city	k1gNnSc1	city
in	in	k?	in
13	[number]	k4	13
Parts	Partsa	k1gFnPc2	Partsa
<g/>
,	,	kIx,	,
Doubleday	Doubleda	k2eAgInPc1d1	Doubleda
<g/>
.	.	kIx.	.
</s>
<s>
E.	E.	kA	E.
Porter	porter	k1gInSc1	porter
Belden	Beldna	k1gFnPc2	Beldna
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Past	past	k1gFnSc1	past
<g/>
,	,	kIx,	,
Present	Present	k1gInSc1	Present
<g/>
,	,	kIx,	,
and	and	k?	and
Future	Futur	k1gMnSc5	Futur
<g/>
:	:	kIx,	:
Comprising	Comprising	k1gInSc4	Comprising
a	a	k8xC	a
History	Histor	k1gInPc4	Histor
of	of	k?	of
the	the	k?	the
City	City	k1gFnSc2	City
of	of	k?	of
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
a	a	k8xC	a
Description	Description	k1gInSc1	Description
of	of	k?	of
its	its	k?	its
Present	Present	k1gInSc1	Present
Condition	Condition	k1gInSc1	Condition
<g/>
,	,	kIx,	,
and	and	k?	and
an	an	k?	an
Estimate	Estimat	k1gInSc5	Estimat
of	of	k?	of
its	its	k?	its
Future	Futur	k1gMnSc5	Futur
Increase	Increas	k1gMnSc5	Increas
<g/>
,	,	kIx,	,
New	New	k1gFnPc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
G.	G.	kA	G.
P.	P.	kA	P.
Putnam	Putnam	k1gInSc4	Putnam
<g/>
.	.	kIx.	.
z	z	k7c2	z
Google	Google	k1gFnSc2	Google
Books	Booksa	k1gFnPc2	Booksa
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gNnSc2	téma
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
NYC	NYC	kA	NYC
<g/>
.	.	kIx.	.
gov	gov	k?	gov
-	-	kIx~	-
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
NYCityMap	NYCityMap	k1gInSc1	NYCityMap
-	-	kIx~	-
Interaktivní	interaktivní	k2eAgFnSc1d1	interaktivní
mapa	mapa	k1gFnSc1	mapa
města	město	k1gNnSc2	město
i	i	k8xC	i
se	s	k7c7	s
sítí	síť	k1gFnSc7	síť
metra	metro	k1gNnSc2	metro
NYCvisit	NYCvisit	k1gInSc4	NYCvisit
<g/>
.	.	kIx.	.
com	com	k?	com
-	-	kIx~	-
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
turistická	turistický	k2eAgFnSc1d1	turistická
prezentace	prezentace	k1gFnSc1	prezentace
města	město	k1gNnSc2	město
</s>
