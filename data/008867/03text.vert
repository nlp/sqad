<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Polska	Polsko	k1gNnSc2	Polsko
má	mít	k5eAaImIp3nS	mít
obdélníkový	obdélníkový	k2eAgInSc4d1	obdélníkový
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
podélně	podélně	k6eAd1	podélně
rozdělená	rozdělená	k1gFnSc1	rozdělená
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
pruhy	pruh	k1gInPc4	pruh
<g/>
:	:	kIx,	:
horní	horní	k2eAgInSc1d1	horní
bílý	bílý	k2eAgInSc1d1	bílý
a	a	k8xC	a
dolní	dolní	k2eAgInSc1d1	dolní
červený	červený	k2eAgInSc1d1	červený
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
polské	polský	k2eAgFnPc1d1	polská
vlajky	vlajka	k1gFnPc1	vlajka
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
polského	polský	k2eAgInSc2d1	polský
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnSc2d1	bílá
orlice	orlice	k1gFnSc2	orlice
s	s	k7c7	s
korunou	koruna	k1gFnSc7	koruna
v	v	k7c6	v
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1228	[number]	k4	1228
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
ustanovené	ustanovený	k2eAgFnPc1d1	ustanovená
v	v	k7c6	v
polské	polský	k2eAgFnSc6d1	polská
ústavě	ústava	k1gFnSc6	ústava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Symbolika	symbolika	k1gFnSc1	symbolika
barev	barva	k1gFnPc2	barva
==	==	k?	==
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
míru	mír	k1gInSc6	mír
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
symbolem	symbol	k1gInSc7	symbol
socialismu	socialismus	k1gInSc2	socialismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Symbolika	symbolika	k1gFnSc1	symbolika
barev	barva	k1gFnPc2	barva
se	se	k3xPyFc4	se
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc1d1	různá
interpretace	interpretace	k1gFnPc1	interpretace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Alegorie	alegorie	k1gFnSc1	alegorie
bílého	bílý	k2eAgMnSc2d1	bílý
orla	orel	k1gMnSc2	orel
vznášejícího	vznášející	k2eAgMnSc2d1	vznášející
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
zapadajícím	zapadající	k2eAgNnSc7d1	zapadající
sluncem	slunce	k1gNnSc7	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spojení	spojení	k1gNnSc1	spojení
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hodnot	hodnota	k1gFnPc2	hodnota
lidského	lidský	k2eAgMnSc2d1	lidský
ducha	duch	k1gMnSc2	duch
s	s	k7c7	s
posvátnou	posvátný	k2eAgFnSc7d1	posvátná
obětí	oběť	k1gFnSc7	oběť
krve	krev	k1gFnSc2	krev
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výraz	výraz	k1gInSc1	výraz
přání	přání	k1gNnSc2	přání
polského	polský	k2eAgInSc2d1	polský
lidu	lid	k1gInSc2	lid
žít	žít	k5eAaImF	žít
v	v	k7c6	v
míru	mír	k1gInSc6	mír
a	a	k8xC	a
socialismu	socialismus	k1gInSc6	socialismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
erbů	erb	k1gInPc2	erb
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oficiálními	oficiální	k2eAgFnPc7d1	oficiální
národními	národní	k2eAgFnPc7d1	národní
barvami	barva	k1gFnPc7	barva
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1831	[number]	k4	1831
<g/>
,	,	kIx,	,
během	během	k7c2	během
listopadového	listopadový	k2eAgNnSc2d1	listopadové
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
schválil	schválit	k5eAaPmAgInS	schválit
polský	polský	k2eAgInSc1d1	polský
parlament	parlament	k1gInSc1	parlament
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
:	:	kIx,	:
Sejm	Sejm	k1gInSc1	Sejm
<g/>
)	)	kIx)	)
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
<g/>
,	,	kIx,	,
že	že	k8xS	že
polská	polský	k2eAgFnSc1d1	polská
vlajka	vlajka	k1gFnSc1	vlajka
bude	být	k5eAaImBp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
barev	barva	k1gFnPc2	barva
znaku	znak	k1gInSc2	znak
Polsko-litevské	polskoitevský	k2eAgFnSc2d1	polsko-litevská
unie	unie	k1gFnSc2	unie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
schválili	schválit	k5eAaPmAgMnP	schválit
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ústavě	ústava	k1gFnSc6	ústava
stojí	stát	k5eAaImIp3nS	stát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Barvami	barva	k1gFnPc7	barva
Polské	polský	k2eAgFnPc1d1	polská
republiky	republika	k1gFnPc1	republika
jsou	být	k5eAaImIp3nP	být
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
v	v	k7c6	v
podélných	podélný	k2eAgInPc6d1	podélný
souběžných	souběžný	k2eAgInPc6d1	souběžný
pásech	pás	k1gInPc6	pás
<g/>
,	,	kIx,	,
horní	horní	k2eAgFnPc4d1	horní
je	být	k5eAaImIp3nS	být
bíly	bílo	k1gNnPc7	bílo
a	a	k8xC	a
dolní	dolní	k2eAgInSc1d1	dolní
červený	červený	k2eAgInSc1d1	červený
<g/>
"	"	kIx"	"
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
nová	nový	k2eAgFnSc1d1	nová
polská	polský	k2eAgFnSc1d1	polská
vláda	vláda	k1gFnSc1	vláda
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zástava	zástava	k1gFnSc1	zástava
zůstane	zůstat	k5eAaPmIp3nS	zůstat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
bez	bez	k7c2	bez
znaku	znak	k1gInSc2	znak
s	s	k7c7	s
orlicí	orlice	k1gFnSc7	orlice
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
dva	dva	k4xCgInPc4	dva
vzory	vzor	k1gInPc4	vzor
vlajek	vlajka	k1gFnPc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
popsaný	popsaný	k2eAgInSc1d1	popsaný
výše	vysoce	k6eAd2	vysoce
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
uprostřed	uprostřed	k7c2	uprostřed
horního	horní	k2eAgInSc2d1	horní
bílého	bílý	k2eAgInSc2d1	bílý
pruhu	pruh	k1gInSc2	pruh
zobrazená	zobrazený	k2eAgFnSc1d1	zobrazená
polská	polský	k2eAgFnSc1d1	polská
orlice	orlice	k1gFnSc1	orlice
<g/>
.	.	kIx.	.
</s>
<s>
Polskou	polský	k2eAgFnSc4d1	polská
vlajku	vlajka	k1gFnSc4	vlajka
s	s	k7c7	s
orlicí	orlice	k1gFnSc7	orlice
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
používají	používat	k5eAaImIp3nP	používat
polští	polský	k2eAgMnPc1d1	polský
námořníci	námořník	k1gMnPc1	námořník
během	během	k7c2	během
plaveb	plavba	k1gFnPc2	plavba
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
taktéž	taktéž	k?	taktéž
schválena	schválit	k5eAaPmNgFnS	schválit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
polské	polský	k2eAgInPc4d1	polský
zastupitelské	zastupitelský	k2eAgInPc4d1	zastupitelský
úřady	úřad	k1gInPc4	úřad
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
a	a	k8xC	a
obchodní	obchodní	k2eAgFnPc4d1	obchodní
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1928	[number]	k4	1928
-	-	kIx~	-
1938	[number]	k4	1938
byla	být	k5eAaImAgFnS	být
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
orlicí	orlice	k1gFnSc7	orlice
jen	jen	k6eAd1	jen
námořní	námořní	k2eAgFnSc7d1	námořní
vlajkou	vlajka	k1gFnSc7	vlajka
na	na	k7c6	na
odlišení	odlišení	k1gNnSc6	odlišení
se	se	k3xPyFc4	se
od	od	k7c2	od
běžné	běžný	k2eAgFnSc2d1	běžná
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
ji	on	k3xPp3gFnSc4	on
opět	opět	k6eAd1	opět
používají	používat	k5eAaImIp3nP	používat
polské	polský	k2eAgInPc4d1	polský
úřady	úřad	k1gInPc4	úřad
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dekret	dekret	k1gInSc1	dekret
ze	z	k7c2	z
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1955	[number]	k4	1955
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
toto	tento	k3xDgNnSc4	tento
užívání	užívání	k1gNnSc4	užívání
vlajky	vlajka	k1gFnSc2	vlajka
a	a	k8xC	a
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
ho	on	k3xPp3gMnSc4	on
o	o	k7c4	o
letiště	letiště	k1gNnSc4	letiště
a	a	k8xC	a
letadla	letadlo	k1gNnPc4	letadlo
létající	létající	k2eAgFnPc4d1	létající
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
z	z	k7c2	z
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1980	[number]	k4	1980
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
používání	používání	k1gNnSc4	používání
vlajky	vlajka	k1gFnSc2	vlajka
o	o	k7c4	o
přístavní	přístavní	k2eAgInPc4d1	přístavní
úřady	úřad	k1gInPc4	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Obrys	obrys	k1gInSc4	obrys
orlice	orlice	k1gFnSc2	orlice
červeným	červený	k2eAgInSc7d1	červený
lemem	lem	k1gInSc7	lem
v	v	k7c6	v
bílém	bílý	k2eAgInSc6d1	bílý
páse	pás	k1gInSc6	pás
se	se	k3xPyFc4	se
také	také	k9	také
změnil	změnit	k5eAaPmAgMnS	změnit
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
s	s	k7c7	s
orlicí	orlice	k1gFnSc7	orlice
Polské	polský	k2eAgFnSc2d1	polská
republiky	republika	k1gFnSc2	republika
používají	používat	k5eAaImIp3nP	používat
podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
8	[number]	k4	8
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Diplomatické	diplomatický	k2eAgFnPc1d1	diplomatická
a	a	k8xC	a
konzulátní	konzulátní	k2eAgNnPc4d1	konzulátní
zastupitelstva	zastupitelstvo	k1gNnPc4	zastupitelstvo
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
oficiální	oficiální	k2eAgInPc4d1	oficiální
úřady	úřad	k1gInPc4	úřad
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
Polska	Polsko	k1gNnSc2	Polsko
na	na	k7c6	na
budovách	budova	k1gFnPc6	budova
nebo	nebo	k8xC	nebo
před	před	k7c7	před
nimi	on	k3xPp3gMnPc7	on
</s>
</p>
<p>
<s>
Představitelé	představitel	k1gMnPc1	představitel
těchto	tento	k3xDgInPc2	tento
úřadů	úřad	k1gInPc2	úřad
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
rezidencích	rezidence	k1gFnPc6	rezidence
a	a	k8xC	a
dopravních	dopravní	k2eAgInPc6d1	dopravní
prostředcích	prostředek	k1gInPc6	prostředek
podle	podle	k7c2	podle
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
práva	právo	k1gNnPc4	právo
</s>
</p>
<p>
<s>
Civilní	civilní	k2eAgNnSc1d1	civilní
letiště	letiště	k1gNnSc1	letiště
</s>
</p>
<p>
<s>
Civilní	civilní	k2eAgNnPc1d1	civilní
letadla	letadlo	k1gNnPc1	letadlo
při	při	k7c6	při
letech	let	k1gInPc6	let
mimo	mimo	k7c4	mimo
Polsko	Polsko	k1gNnSc4	Polsko
</s>
</p>
<p>
<s>
Správní	správní	k2eAgFnPc1d1	správní
úřadovny	úřadovna	k1gFnPc1	úřadovna
přístavů	přístav	k1gInPc2	přístav
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
budovách	budova	k1gFnPc6	budova
či	či	k8xC	či
před	před	k7c7	před
nimi	on	k3xPp3gMnPc7	on
</s>
</p>
<p>
<s>
===	===	k?	===
Den	den	k1gInSc1	den
vlajky	vlajka	k1gFnSc2	vlajka
Polské	polský	k2eAgFnSc2d1	polská
republiky	republika	k1gFnSc2	republika
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
se	s	k7c7	s
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
slaví	slavit	k5eAaImIp3nS	slavit
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
den	den	k1gInSc4	den
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozměry	rozměra	k1gFnPc1	rozměra
vlajky	vlajka	k1gFnPc1	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Obdélník	obdélník	k1gInSc1	obdélník
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
stran	strana	k1gFnPc2	strana
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
rozdělený	rozdělený	k2eAgInSc4d1	rozdělený
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pásy	pás	k1gInPc4	pás
<g/>
:	:	kIx,	:
bílý	bílý	k2eAgInSc1d1	bílý
a	a	k8xC	a
červený	červený	k2eAgInSc1d1	červený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Odstín	odstín	k1gInSc4	odstín
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
vlajce	vlajka	k1gFnSc6	vlajka
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
ústavy	ústava	k1gFnSc2	ústava
přijaté	přijatý	k2eAgFnSc2d1	přijatá
roku	rok	k1gInSc3	rok
1919	[number]	k4	1919
nebylo	být	k5eNaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc4	jaký
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
odstín	odstín	k1gInSc1	odstín
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
vydalo	vydat	k5eAaPmAgNnS	vydat
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
války	válka	k1gFnSc2	válka
brožurku	brožurka	k1gFnSc4	brožurka
Godło	Godło	k1gMnSc1	Godło
i	i	k9	i
barwy	barw	k2eAgFnPc4d1	barw
Rzeczypospolitej	Rzeczypospolitej	k1gFnPc4	Rzeczypospolitej
Polskiej	Polskiej	k1gInSc1	Polskiej
(	(	kIx(	(
<g/>
Znak	znak	k1gInSc1	znak
a	a	k8xC	a
barvy	barva	k1gFnPc1	barva
Republiky	republika	k1gFnSc2	republika
Polské	polský	k2eAgFnPc1d1	polská
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
<g/>
:	:	kIx,	:
Stanisław	Stanisław	k1gMnSc1	Stanisław
Łoz	Łoz	k1gMnSc1	Łoz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zobrazovala	zobrazovat	k5eAaImAgFnS	zobrazovat
barvy	barva	k1gFnPc4	barva
znaku	znak	k1gInSc2	znak
a	a	k8xC	a
vlajky	vlajka	k1gFnSc2	vlajka
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
měla	mít	k5eAaImAgFnS	mít
karmazínový	karmazínový	k2eAgInSc4d1	karmazínový
odstín	odstín	k1gInSc4	odstín
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1927	[number]	k4	1927
prezident	prezident	k1gMnSc1	prezident
Polska	Polsko	k1gNnSc2	Polsko
svým	svůj	k3xOyFgNnSc7	svůj
nařízením	nařízení	k1gNnSc7	nařízení
změnil	změnit	k5eAaPmAgInS	změnit
odstín	odstín	k1gInSc1	odstín
červené	červená	k1gFnSc2	červená
na	na	k7c4	na
rumělkový	rumělkový	k2eAgInSc4d1	rumělkový
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
nabylo	nabýt	k5eAaPmAgNnS	nabýt
účinnost	účinnost	k1gFnSc4	účinnost
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1928	[number]	k4	1928
a	a	k8xC	a
dovolovalo	dovolovat	k5eAaImAgNnS	dovolovat
používání	používání	k1gNnSc4	používání
starších	starý	k2eAgFnPc2d2	starší
vlajek	vlajka	k1gFnPc2	vlajka
do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Rumělkový	rumělkový	k2eAgInSc1d1	rumělkový
odstín	odstín	k1gInSc1	odstín
zůstal	zůstat	k5eAaPmAgInS	zůstat
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
vlajce	vlajka	k1gFnSc6	vlajka
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
ústavě	ústava	k1gFnSc6	ústava
z	z	k7c2	z
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1980	[number]	k4	1980
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
popsaná	popsaný	k2eAgFnSc1d1	popsaná
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
CIELUV	CIELUV	kA	CIELUV
(	(	kIx(	(
<g/>
Barevné	barevný	k2eAgNnSc1d1	barevné
rozhraní	rozhraní	k1gNnSc1	rozhraní
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Trojbarevné	trojbarevný	k2eAgNnSc1d1	trojbarevné
rozdělení	rozdělení	k1gNnSc1	rozdělení
barev	barva	k1gFnPc2	barva
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
složky	složka	k1gFnPc1	složka
Y	Y	kA	Y
a	a	k8xC	a
také	také	k9	také
povolený	povolený	k2eAgInSc1d1	povolený
rozdíl	rozdíl	k1gInSc1	rozdíl
E	E	kA	E
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
barev	barva	k1gFnPc2	barva
CIE	CIE	kA	CIE
1976	[number]	k4	1976
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
*	*	kIx~	*
u	u	k7c2	u
<g/>
*	*	kIx~	*
v	v	k7c6	v
<g/>
*	*	kIx~	*
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stanovený	stanovený	k2eAgInSc1d1	stanovený
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
CIELUV	CIELUV	kA	CIELUV
při	při	k7c6	při
osvětlení	osvětlení	k1gNnSc6	osvětlení
C	C	kA	C
a	a	k8xC	a
poměru	poměr	k1gInSc2	poměr
d	d	k?	d
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Červená	červenat	k5eAaImIp3nS	červenat
na	na	k7c6	na
barevném	barevný	k2eAgInSc6d1	barevný
vzoru	vzor	k1gInSc6	vzor
přijatá	přijatý	k2eAgFnSc1d1	přijatá
v	v	k7c6	v
ústavě	ústava	k1gFnSc6	ústava
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
tmavší	tmavý	k2eAgFnSc1d2	tmavší
než	než	k8xS	než
rumělková	rumělkový	k2eAgFnSc1d1	rumělková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Poľska	Poľska	k1gFnSc1	Poľska
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Polska	Polsko	k1gNnSc2	Polsko
</s>
</p>
<p>
<s>
Polská	polský	k2eAgFnSc1d1	polská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Polska	Polsko	k1gNnSc2	Polsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Polská	polský	k2eAgFnSc1d1	polská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
vlajce	vlajka	k1gFnSc6	vlajka
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
O	o	k7c6	o
polské	polský	k2eAgFnSc6d1	polská
vlajce	vlajka	k1gFnSc6	vlajka
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
Vlajky	vlajka	k1gFnSc2	vlajka
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ALBUM	album	k1gNnSc1	album
POLSKI	POLSKI	kA	POLSKI
-	-	kIx~	-
Interaktivní	interaktivní	k2eAgNnSc1d1	interaktivní
heraldické	heraldický	k2eAgNnSc1d1	heraldické
album	album	k1gNnSc1	album
PR	pr	k0	pr
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
</s>
</p>
