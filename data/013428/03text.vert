<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Kymlička	Kymlička	k1gFnSc1	Kymlička
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1937	[number]	k4	1937
Pelhřimov	Pelhřimov	k1gInSc1	Pelhřimov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
posledním	poslední	k2eAgMnSc7d1	poslední
ministrem	ministr	k1gMnSc7	ministr
kultury	kultura	k1gFnSc2	kultura
České	český	k2eAgFnSc2d1	Česká
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
před	před	k7c7	před
pádem	pád	k1gInSc7	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Pelhřimově	Pelhřimov	k1gInSc6	Pelhřimov
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
státního	státní	k2eAgMnSc2d1	státní
zaměstnance	zaměstnanec	k1gMnSc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
ekonomem	ekonom	k1gMnSc7	ekonom
ve	v	k7c6	v
vydavatelství	vydavatelství	k1gNnPc1	vydavatelství
Naše	náš	k3xOp1gNnSc4	náš
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
studoval	studovat	k5eAaImAgMnS	studovat
Vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
byl	být	k5eAaImAgMnS	být
funkcionářem	funkcionář	k1gMnSc7	funkcionář
Československého	československý	k2eAgInSc2d1	československý
svazu	svaz	k1gInSc2	svaz
mládeže	mládež	k1gFnSc2	mládež
na	na	k7c6	na
obvodní	obvodní	k2eAgFnSc6d1	obvodní
a	a	k8xC	a
městské	městský	k2eAgFnSc6d1	městská
úrovni	úroveň	k1gFnSc6	úroveň
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
krátce	krátce	k6eAd1	krátce
působil	působit	k5eAaImAgInS	působit
na	na	k7c6	na
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
byl	být	k5eAaImAgMnS	být
pracovníkem	pracovník	k1gMnSc7	pracovník
aparátu	aparát	k1gInSc2	aparát
Ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
zde	zde	k6eAd1	zde
zastával	zastávat	k5eAaImAgMnS	zastávat
post	post	k1gInSc4	post
instruktora	instruktor	k1gMnSc2	instruktor
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
vedl	vést	k5eAaImAgInS	vést
odbor	odbor	k1gInSc1	odbor
škol	škola	k1gFnPc2	škola
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Následujících	následující	k2eAgInPc2d1	následující
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
vykonával	vykonávat	k5eAaImAgInS	vykonávat
funkci	funkce	k1gFnSc6	funkce
pomocníka	pomocník	k1gMnSc4	pomocník
tajemníka	tajemník	k1gMnSc2	tajemník
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
pedagogiky	pedagogika	k1gFnSc2	pedagogika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
náměstka	náměstek	k1gMnSc2	náměstek
ministra	ministr	k1gMnSc2	ministr
kultury	kultura	k1gFnSc2	kultura
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
náměstkem	náměstek	k1gMnSc7	náměstek
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
rezortu	rezort	k1gInSc6	rezort
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
výstavbu	výstavba	k1gFnSc4	výstavba
<g/>
.	.	kIx.	.
<g/>
Funkci	funkce	k1gFnSc4	funkce
ministra	ministr	k1gMnSc2	ministr
ve	v	k7c4	v
Ladislava	Ladislav	k1gMnSc4	Ladislav
Adamce	Adamec	k1gMnSc4	Adamec
a	a	k8xC	a
Františka	František	k1gMnSc4	František
Pitry	Pitra	k1gFnSc2	Pitra
zastával	zastávat	k5eAaImAgMnS	zastávat
od	od	k7c2	od
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1988	[number]	k4	1988
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
vedl	vést	k5eAaImAgInS	vést
oficiální	oficiální	k2eAgFnSc4d1	oficiální
československou	československý	k2eAgFnSc4d1	Československá
delegaci	delegace	k1gFnSc4	delegace
na	na	k7c4	na
svatořečení	svatořečení	k1gNnSc4	svatořečení
Anežky	Anežka	k1gFnSc2	Anežka
České	český	k2eAgFnSc2d1	Česká
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
odešel	odejít	k5eAaPmAgInS	odejít
z	z	k7c2	z
vládních	vládní	k2eAgFnPc2d1	vládní
i	i	k8xC	i
politických	politický	k2eAgFnPc2d1	politická
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
společník	společník	k1gMnSc1	společník
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
firmě	firma	k1gFnSc6	firma
Diagnostické	diagnostický	k2eAgNnSc1d1	diagnostické
a	a	k8xC	a
terapeutické	terapeutický	k2eAgNnSc1d1	terapeutické
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgMnS	uvádět
i	i	k9	i
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
osobností	osobnost	k1gFnPc2	osobnost
technologické	technologický	k2eAgFnSc2d1	technologická
společnosti	společnost	k1gFnSc2	společnost
Centrum	centrum	k1gNnSc1	centrum
aplikovaného	aplikovaný	k2eAgInSc2d1	aplikovaný
výzkumu	výzkum	k1gInSc2	výzkum
v	v	k7c6	v
Dobříši	Dobříš	k1gFnSc6	Dobříš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Milan	Milan	k1gMnSc1	Milan
Kymlička	Kymlička	k1gFnSc1	Kymlička
</s>
</p>
