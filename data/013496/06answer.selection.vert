<s>
Krkonošský	krkonošský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
,	,	kIx,
též	též	k9
KRNAP	KRNAP	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
chráněné	chráněný	k2eAgNnSc1d1
území	území	k1gNnSc1
nalézající	nalézající	k2eAgMnSc1d1
se	se	k3xPyFc4
na	na	k7c6
geomorfologickém	geomorfologický	k2eAgInSc6d1
celku	celek	k1gInSc6
Krkonoše	Krkonoše	k1gFnPc1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>