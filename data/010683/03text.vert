<p>
<s>
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
Lennon	Lennon	k1gNnSc1	Lennon
<g/>
,	,	kIx,	,
původním	původní	k2eAgNnSc7d1	původní
jménem	jméno	k1gNnSc7	jméno
japonsky	japonsky	k6eAd1	japonsky
小	小	k?	小
洋	洋	k?	洋
Ono	onen	k3xDgNnSc1	onen
Jóko	Jóko	k1gNnSc1	Jóko
<g/>
,	,	kIx,	,
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
Tokio	Tokio	k1gNnSc1	Tokio
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonsko-americká	japonskomerický	k2eAgFnSc1d1	japonsko-americká
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
umělkyně	umělkyně	k1gFnSc1	umělkyně
a	a	k8xC	a
hudebnice	hudebnice	k1gFnSc1	hudebnice
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
manželství	manželství	k1gNnSc3	manželství
s	s	k7c7	s
britským	britský	k2eAgMnSc7d1	britský
hudebníkem	hudebník	k1gMnSc7	hudebník
a	a	k8xC	a
bývalým	bývalý	k2eAgMnSc7d1	bývalý
členem	člen	k1gMnSc7	člen
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc7	The
Beatles	beatles	k1gMnSc7	beatles
Johnem	John	k1gMnSc7	John
Lennonem	Lennon	k1gMnSc7	Lennon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
vysoce	vysoce	k6eAd1	vysoce
postavené	postavený	k2eAgFnSc3d1	postavená
aristokratické	aristokratický	k2eAgFnSc3d1	aristokratická
japonské	japonský	k2eAgFnSc3d1	japonská
rodině	rodina	k1gFnSc3	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
sester	sestra	k1gFnPc2	sestra
(	(	kIx(	(
<g/>
sestry	sestra	k1gFnPc1	sestra
Eisuke	Eisuk	k1gFnSc2	Eisuk
a	a	k8xC	a
Isoko	Isoko	k1gNnSc4	Isoko
<g/>
)	)	kIx)	)
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
získala	získat	k5eAaPmAgFnS	získat
klasické	klasický	k2eAgNnSc4d1	klasické
hudební	hudební	k2eAgNnSc4d1	hudební
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
hrála	hrát	k5eAaImAgFnS	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
učila	učit	k5eAaImAgFnS	učit
se	se	k3xPyFc4	se
kompozici	kompozice	k1gFnSc3	kompozice
a	a	k8xC	a
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
vzestupu	vzestup	k1gInSc6	vzestup
v	v	k7c6	v
bankovní	bankovní	k2eAgFnSc6d1	bankovní
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
,	,	kIx,	,
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
Japonsko	Japonsko	k1gNnSc1	Japonsko
napadlo	napadnout	k5eAaPmAgNnS	napadnout
Čínu	Čína	k1gFnSc4	Čína
a	a	k8xC	a
v	v	k7c6	v
USA	USA	kA	USA
stoupalo	stoupat	k5eAaImAgNnS	stoupat
protijaponské	protijaponský	k2eAgNnSc1d1	protijaponský
cítění	cítění	k1gNnSc1	cítění
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
malá	malý	k2eAgFnSc1d1	malá
Yoko	Yoko	k6eAd1	Yoko
vrátila	vrátit	k5eAaPmAgFnS	vrátit
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Tokia	Tokio	k1gNnSc2	Tokio
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
z	z	k7c2	z
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
za	za	k7c7	za
otcem	otec	k1gMnSc7	otec
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
studiích	studio	k1gNnPc6	studio
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
věnovat	věnovat	k5eAaPmF	věnovat
hudbě	hudba	k1gFnSc3	hudba
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
prosazovat	prosazovat	k5eAaImF	prosazovat
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
světa	svět	k1gInSc2	svět
avantgardních	avantgardní	k2eAgMnPc2d1	avantgardní
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Toši	Toš	k1gFnSc3	Toš
Ičijanagi	Ičijanag	k1gFnSc2	Ičijanag
(	(	kIx(	(
<g/>
一	一	k?	一
慧	慧	k?	慧
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
provdala	provdat	k5eAaPmAgFnS	provdat
<g/>
.	.	kIx.	.
</s>
<s>
Věnovala	věnovat	k5eAaPmAgNnP	věnovat
se	se	k3xPyFc4	se
umění	umění	k1gNnPc1	umění
<g/>
,	,	kIx,	,
skládání	skládání	k1gNnSc1	skládání
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
scházela	scházet	k5eAaImAgFnS	scházet
se	se	k3xPyFc4	se
s	s	k7c7	s
podobně	podobně	k6eAd1	podobně
zaměřenými	zaměřený	k2eAgMnPc7d1	zaměřený
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
se	se	k3xPyFc4	se
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
vdala	vdát	k5eAaPmAgFnS	vdát
(	(	kIx(	(
<g/>
za	za	k7c4	za
filmového	filmový	k2eAgMnSc4d1	filmový
producenta	producent	k1gMnSc4	producent
Anthony	Anthona	k1gFnSc2	Anthona
Coxe	Cox	k1gFnSc2	Cox
<g/>
)	)	kIx)	)
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1963	[number]	k4	1963
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Kyoko	Kyoko	k1gNnSc4	Kyoko
Chan	Chan	k1gMnSc1	Chan
Cox	Cox	k1gMnSc1	Cox
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
hnutí	hnutí	k1gNnSc2	hnutí
Fluxus	Fluxus	k1gInSc1	Fluxus
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
zabývat	zabývat	k5eAaImF	zabývat
videoartem	videoart	k1gInSc7	videoart
<g/>
,	,	kIx,	,
happeningy	happening	k1gInPc7	happening
<g/>
,	,	kIx,	,
tvorbou	tvorba	k1gFnSc7	tvorba
uměleckých	umělecký	k2eAgInPc2d1	umělecký
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
protestními	protestní	k2eAgFnPc7d1	protestní
mírovými	mírový	k2eAgFnPc7d1	mírová
akcemi	akce	k1gFnPc7	akce
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Vydala	vydat	k5eAaPmAgFnS	vydat
knihu	kniha	k1gFnSc4	kniha
básní	báseň	k1gFnPc2	báseň
Grapefruit	grapefruit	k1gInSc1	grapefruit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
navštívila	navštívit	k5eAaPmAgFnS	navštívit
Londýn	Londýn	k1gInSc4	Londýn
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
dílem	dílem	k6eAd1	dílem
Cut	Cut	k1gFnSc6	Cut
piece	pieca	k1gFnSc6	pieca
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
ustřihnout	ustřihnout	k5eAaPmF	ustřihnout
kousek	kousek	k1gInSc4	kousek
něčeho	něco	k3yInSc2	něco
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
klečela	klečet	k5eAaImAgFnS	klečet
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
a	a	k8xC	a
diváci	divák	k1gMnPc1	divák
z	z	k7c2	z
ní	on	k3xPp3gFnSc6	on
mohli	moct	k5eAaImAgMnP	moct
odstřihovat	odstřihovat	k5eAaImF	odstřihovat
kousky	kousek	k1gInPc4	kousek
ošacení	ošacení	k1gNnSc2	ošacení
<g/>
.	.	kIx.	.
</s>
<s>
Vystoupení	vystoupení	k1gNnSc1	vystoupení
mělo	mít	k5eAaImAgNnS	mít
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
tak	tak	k6eAd1	tak
dostala	dostat	k5eAaPmAgFnS	dostat
nabídku	nabídka	k1gFnSc4	nabídka
na	na	k7c4	na
uspořádání	uspořádání	k1gNnSc4	uspořádání
vlastní	vlastní	k2eAgFnSc2d1	vlastní
výstavy	výstava	k1gFnSc2	výstava
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Nedokončené	dokončený	k2eNgInPc1d1	nedokončený
obrazy	obraz	k1gInPc1	obraz
a	a	k8xC	a
předměty	předmět	k1gInPc1	předmět
v	v	k7c6	v
Indické	indický	k2eAgFnSc6d1	indická
galerii	galerie	k1gFnSc6	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Lennonem	Lennon	k1gMnSc7	Lennon
<g/>
.	.	kIx.	.
</s>
<s>
Lennona	Lennona	k1gFnSc1	Lennona
naprosto	naprosto	k6eAd1	naprosto
očarovala	očarovat	k5eAaPmAgFnS	očarovat
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc1	její
osobní	osobní	k2eAgNnSc1d1	osobní
kouzlo	kouzlo	k1gNnSc1	kouzlo
ho	on	k3xPp3gMnSc4	on
natolik	natolik	k6eAd1	natolik
pohltilo	pohltit	k5eAaPmAgNnS	pohltit
<g/>
,	,	kIx,	,
že	že	k8xS	že
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
teprve	teprve	k6eAd1	teprve
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
láska	láska	k1gFnSc1	láska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1968	[number]	k4	1968
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
nahrál	nahrát	k5eAaPmAgMnS	nahrát
společně	společně	k6eAd1	společně
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
první	první	k4xOgNnSc4	první
společné	společný	k2eAgNnSc4d1	společné
album	album	k1gNnSc4	album
Unfinished	Unfinished	k1gMnSc1	Unfinished
Music	Music	k1gMnSc1	Music
No	no	k9	no
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
:	:	kIx,	:
Two	Two	k1gFnPc2	Two
Virgins	Virgins	k1gInSc1	Virgins
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
obalu	obal	k1gInSc6	obal
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgMnPc1	dva
nazí	nahý	k2eAgMnPc1d1	nahý
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
gramofonová	gramofonový	k2eAgFnSc1d1	gramofonová
deska	deska	k1gFnSc1	deska
musela	muset	k5eAaImAgFnS	muset
prodávat	prodávat	k5eAaImF	prodávat
v	v	k7c6	v
papírovém	papírový	k2eAgInSc6d1	papírový
sáčku	sáček	k1gInSc6	sáček
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
koláž	koláž	k1gFnSc4	koláž
zvuků	zvuk	k1gInPc2	zvuk
a	a	k8xC	a
mluveného	mluvený	k2eAgNnSc2d1	mluvené
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1968	[number]	k4	1968
bylo	být	k5eAaImAgNnS	být
rozvedeno	rozvést	k5eAaPmNgNnS	rozvést
Lennonovo	Lennonův	k2eAgNnSc1d1	Lennonovo
manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
Cynthií	Cynthie	k1gFnSc7	Cynthie
Powellovou	Powellová	k1gFnSc7	Powellová
aby	aby	k9	aby
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1969	[number]	k4	1969
mohli	moct	k5eAaImAgMnP	moct
John	John	k1gMnSc1	John
a	a	k8xC	a
Yoko	Yoko	k1gMnSc1	Yoko
v	v	k7c6	v
tichosti	tichost	k1gFnSc6	tichost
na	na	k7c6	na
Gibraltaru	Gibraltar	k1gInSc6	Gibraltar
uzavřít	uzavřít	k5eAaPmF	uzavřít
sňatek	sňatek	k1gInSc4	sňatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
1969	[number]	k4	1969
je	být	k5eAaImIp3nS	být
kritický	kritický	k2eAgInSc1d1	kritický
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
začal	začít	k5eAaPmAgMnS	začít
na	na	k7c4	na
nahrávání	nahrávání	k1gNnSc4	nahrávání
vodit	vodit	k5eAaImF	vodit
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc4	onen
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
čemuž	což	k3yRnSc3	což
protestoval	protestovat	k5eAaBmAgMnS	protestovat
hlavně	hlavně	k9	hlavně
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
a	a	k8xC	a
Yoko	Yoko	k6eAd1	Yoko
založili	založit	k5eAaPmAgMnP	založit
skupinu	skupina	k1gFnSc4	skupina
Plastic	Plastice	k1gFnPc2	Plastice
Ono	onen	k3xDgNnSc1	onen
Band	band	k1gInSc4	band
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
se	se	k3xPyFc4	se
politicky	politicky	k6eAd1	politicky
angažovat	angažovat	k5eAaBmF	angažovat
a	a	k8xC	a
protestovat	protestovat	k5eAaBmF	protestovat
proti	proti	k7c3	proti
válkám	válka	k1gFnPc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
Pořádali	pořádat	k5eAaImAgMnP	pořádat
různé	různý	k2eAgFnPc4d1	různá
veřejné	veřejný	k2eAgFnPc4d1	veřejná
akce	akce	k1gFnPc4	akce
a	a	k8xC	a
mírové	mírový	k2eAgInPc4d1	mírový
happeningy	happening	k1gInPc4	happening
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgFnSc7d3	nejznámější
-	-	kIx~	-
Bed	Beda	k1gFnPc2	Beda
In	In	k1gMnPc2	In
-	-	kIx~	-
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1969	[number]	k4	1969
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
<g/>
.	.	kIx.	.
</s>
<s>
Novomanželé	novomanžel	k1gMnPc1	novomanžel
Lennonovi	Lennonův	k2eAgMnPc1d1	Lennonův
strávili	strávit	k5eAaPmAgMnP	strávit
8	[number]	k4	8
dní	den	k1gInPc2	den
v	v	k7c6	v
posteli	postel	k1gFnSc6	postel
<g/>
,	,	kIx,	,
vzývali	vzývat	k5eAaImAgMnP	vzývat
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
pořádali	pořádat	k5eAaImAgMnP	pořádat
mírové	mírový	k2eAgInPc4d1	mírový
dýchánky	dýchánek	k1gInPc4	dýchánek
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
Timothy	Timotha	k1gMnSc2	Timotha
Leary	Leara	k1gMnSc2	Leara
<g/>
)	)	kIx)	)
nahráli	nahrát	k5eAaBmAgMnP	nahrát
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
kultovní	kultovní	k2eAgFnSc4d1	kultovní
skladbu	skladba	k1gFnSc4	skladba
Give	Giv	k1gFnSc2	Giv
Peace	Peace	k1gFnSc2	Peace
a	a	k8xC	a
Chance	Chanec	k1gInSc2	Chanec
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
spolu	spolu	k6eAd1	spolu
natočili	natočit	k5eAaBmAgMnP	natočit
několik	několik	k4yIc4	několik
filmových	filmový	k2eAgInPc2d1	filmový
experimentů	experiment	k1gInPc2	experiment
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k6eAd1	hlavně
nahrávali	nahrávat	k5eAaImAgMnP	nahrávat
další	další	k2eAgNnPc4d1	další
hudební	hudební	k2eAgNnPc4d1	hudební
alba	album	k1gNnPc4	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
se	se	k3xPyFc4	se
Beatles	beatles	k1gMnPc1	beatles
oficiálně	oficiálně	k6eAd1	oficiálně
rozpadli	rozpadnout	k5eAaPmAgMnP	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
manželé	manžel	k1gMnPc1	manžel
Lennonovi	Lennonův	k2eAgMnPc1d1	Lennonův
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
natrvalo	natrvalo	k6eAd1	natrvalo
do	do	k7c2	do
USA	USA	kA	USA
a	a	k8xC	a
zabydleli	zabydlet	k5eAaPmAgMnP	zabydlet
se	se	k3xPyFc4	se
v	v	k7c6	v
domě	dům	k1gInSc6	dům
zvaném	zvaný	k2eAgInSc6d1	zvaný
Dakota	Dakota	k1gFnSc1	Dakota
House	house	k1gNnSc1	house
na	na	k7c6	na
newyorském	newyorský	k2eAgInSc6d1	newyorský
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
měl	mít	k5eAaImAgInS	mít
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
problémy	problém	k1gInPc7	problém
s	s	k7c7	s
imigračním	imigrační	k2eAgInSc7d1	imigrační
úřadem	úřad	k1gInSc7	úřad
kvůli	kvůli	k7c3	kvůli
dřívějšímu	dřívější	k2eAgNnSc3d1	dřívější
přechovávání	přechovávání	k1gNnSc3	přechovávání
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
získal	získat	k5eAaPmAgMnS	získat
tzv.	tzv.	kA	tzv.
zelenou	zelený	k2eAgFnSc4d1	zelená
kartu	karta	k1gFnSc4	karta
umožňující	umožňující	k2eAgInSc4d1	umožňující
trvalý	trvalý	k2eAgInSc4d1	trvalý
pobyt	pobyt	k1gInSc4	pobyt
a	a	k8xC	a
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
USA	USA	kA	USA
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
prošlo	projít	k5eAaPmAgNnS	projít
krizí	krize	k1gFnSc7	krize
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
se	se	k3xPyFc4	se
Lennonovi	Lennon	k1gMnSc6	Lennon
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
odloučili	odloučit	k5eAaPmAgMnP	odloučit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1975	[number]	k4	1975
přivedla	přivést	k5eAaPmAgFnS	přivést
na	na	k7c4	na
svět	svět	k1gInSc4	svět
jejich	jejich	k3xOp3gNnSc7	jejich
jediné	jediný	k2eAgNnSc1d1	jediné
společné	společný	k2eAgNnSc1d1	společné
dítě	dítě	k1gNnSc1	dítě
Seana	Sean	k1gInSc2	Sean
Ono	onen	k3xDgNnSc4	onen
Taró	Taró	k1gMnSc1	Taró
Lennona	Lennon	k1gMnSc2	Lennon
(	(	kIx(	(
<g/>
jap.	jap.	k?	jap.
小	小	k?	小
太	太	k?	太
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
narození	narození	k1gNnSc4	narození
syna	syn	k1gMnSc2	syn
si	se	k3xPyFc3	se
Lennon	Lennon	k1gMnSc1	Lennon
dal	dát	k5eAaPmAgMnS	dát
pauzu	pauza	k1gFnSc4	pauza
a	a	k8xC	a
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
se	se	k3xPyFc4	se
před	před	k7c7	před
okolním	okolní	k2eAgInSc7d1	okolní
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
dítěti	dítě	k1gNnSc3	dítě
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Yoko	Yoko	k6eAd1	Yoko
chodila	chodit	k5eAaImAgFnS	chodit
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
věnovala	věnovat	k5eAaPmAgFnS	věnovat
se	se	k3xPyFc4	se
obchodním	obchodní	k2eAgFnPc3d1	obchodní
záležitostem	záležitost	k1gFnPc3	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1980	[number]	k4	1980
začal	začít	k5eAaPmAgMnS	začít
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
po	po	k7c6	po
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
znovu	znovu	k6eAd1	znovu
skládat	skládat	k5eAaImF	skládat
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
natáčet	natáčet	k5eAaImF	natáčet
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
nahrávek	nahrávka	k1gFnPc2	nahrávka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
–	–	k?	–
Double	double	k2eAgInPc7d1	double
Fantasy	fantas	k1gInPc7	fantas
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
a	a	k8xC	a
Milk	Milk	k1gMnSc1	Milk
and	and	k?	and
Honey	Honea	k1gFnSc2	Honea
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1980	[number]	k4	1980
se	se	k3xPyFc4	se
Lennon	Lennon	k1gMnSc1	Lennon
stal	stát	k5eAaPmAgMnS	stát
obětí	oběť	k1gFnSc7	oběť
atentátu	atentát	k1gInSc2	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
před	před	k7c7	před
branou	brána	k1gFnSc7	brána
jeho	on	k3xPp3gInSc2	on
newyorského	newyorský	k2eAgInSc2d1	newyorský
bytu	byt	k1gInSc2	byt
jej	on	k3xPp3gMnSc4	on
postřelil	postřelit	k5eAaPmAgMnS	postřelit
duševně	duševně	k6eAd1	duševně
nemocný	mocný	k2eNgMnSc1d1	nemocný
Mark	Mark	k1gMnSc1	Mark
David	David	k1gMnSc1	David
Chapman	Chapman	k1gMnSc1	Chapman
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
hodin	hodina	k1gFnPc2	hodina
předtím	předtím	k6eAd1	předtím
podepsal	podepsat	k5eAaPmAgMnS	podepsat
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
Double	double	k2eAgInPc4d1	double
Fantasy	fantas	k1gInPc4	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
převozu	převoz	k1gInSc6	převoz
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
John	John	k1gMnSc1	John
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Dakota	Dakota	k1gFnSc1	Dakota
House	house	k1gNnSc1	house
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
začala	začít	k5eAaPmAgFnS	začít
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc4	onen
znovu	znovu	k6eAd1	znovu
tvořit	tvořit	k5eAaImF	tvořit
a	a	k8xC	a
vystavovat	vystavovat	k5eAaImF	vystavovat
své	svůj	k3xOyFgFnPc4	svůj
umělecké	umělecký	k2eAgFnPc4d1	umělecká
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
vydala	vydat	k5eAaPmAgFnS	vydat
několik	několik	k4yIc4	několik
sólových	sólový	k2eAgFnPc2d1	sólová
alb	alba	k1gFnPc2	alba
a	a	k8xC	a
zvukovou	zvukový	k2eAgFnSc4d1	zvuková
nahrávku	nahrávka	k1gFnSc4	nahrávka
jejích	její	k3xOp3gInPc2	její
rozhovorů	rozhovor	k1gInPc2	rozhovor
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Lennonem	Lennon	k1gMnSc7	Lennon
<g/>
.	.	kIx.	.
</s>
<s>
Uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
k	k	k7c3	k
albu	album	k1gNnSc3	album
Rising	Rising	k1gInSc1	Rising
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
ji	on	k3xPp3gFnSc4	on
doprovodila	doprovodit	k5eAaPmAgFnS	doprovodit
skupina	skupina	k1gFnSc1	skupina
jejího	její	k3xOp3gMnSc2	její
syna	syn	k1gMnSc2	syn
Seana	Sean	k1gMnSc2	Sean
<g/>
.	.	kIx.	.
</s>
<s>
Složila	složit	k5eAaPmAgFnS	složit
také	také	k9	také
dva	dva	k4xCgInPc4	dva
muzikály	muzikál	k1gInPc4	muzikál
a	a	k8xC	a
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
několik	několik	k4yIc4	několik
výstav	výstava	k1gFnPc2	výstava
svých	svůj	k3xOyFgNnPc2	svůj
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
Lennonových	Lennonův	k2eAgFnPc2d1	Lennonova
litografií	litografie	k1gFnPc2	litografie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2003	[number]	k4	2003
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
osobně	osobně	k6eAd1	osobně
zahájila	zahájit	k5eAaPmAgFnS	zahájit
výstavu	výstava	k1gFnSc4	výstava
Woman	Womana	k1gFnPc2	Womana
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
room	roo	k1gNnSc7	roo
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Ženský	ženský	k2eAgInSc4d1	ženský
pokoj	pokoj	k1gInSc4	pokoj
<g/>
)	)	kIx)	)
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Kampě	Kampa	k1gFnSc6	Kampa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
a	a	k8xC	a
nominace	nominace	k1gFnSc1	nominace
==	==	k?	==
</s>
</p>
<p>
<s>
cena	cena	k1gFnSc1	cena
Grammy	Gramma	k1gFnSc2	Gramma
za	za	k7c4	za
album	album	k1gNnSc4	album
roku	rok	k1gInSc2	rok
Double	double	k1gInSc1	double
fantasy	fantas	k1gInPc1	fantas
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
cena	cena	k1gFnSc1	cena
Helen	Helena	k1gFnPc2	Helena
Caldicottové	Caldicottové	k2eAgFnPc2d1	Caldicottové
za	za	k7c4	za
propagaci	propagace	k1gFnSc4	propagace
míru	míra	k1gFnSc4	míra
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
čestný	čestný	k2eAgInSc1d1	čestný
doktorát	doktorát	k1gInSc1	doktorát
na	na	k7c6	na
umělecké	umělecký	k2eAgFnSc6d1	umělecká
škole	škola	k1gFnSc6	škola
v	v	k7c4	v
Chicago	Chicago	k1gNnSc4	Chicago
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
cena	cena	k1gFnSc1	cena
Grammy	Gramma	k1gFnSc2	Gramma
za	za	k7c4	za
produkování	produkování	k1gNnSc4	produkování
filmu	film	k1gInSc2	film
Gimme	Gimm	k1gInSc5	Gimm
some	som	k1gInPc4	som
truth	truth	k1gInSc1	truth
-	-	kIx~	-
The	The	k1gFnSc1	The
making	making	k1gInSc1	making
of	of	k?	of
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Imagine	Imagin	k1gInSc5	Imagin
album	album	k1gNnSc4	album
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
čestný	čestný	k2eAgInSc1d1	čestný
doktorát	doktorát	k1gInSc1	doktorát
práv	právo	k1gNnPc2	právo
na	na	k7c6	na
liverpoolské	liverpoolský	k2eAgFnSc6d1	liverpoolská
univerzitě	univerzita	k1gFnSc6	univerzita
za	za	k7c4	za
umělecký	umělecký	k2eAgInSc4d1	umělecký
přínos	přínos	k1gInSc4	přínos
a	a	k8xC	a
založení	založení	k1gNnSc4	založení
stipendijního	stipendijní	k2eAgInSc2d1	stipendijní
fondu	fond	k1gInSc2	fond
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
výstavu	výstava	k1gFnSc4	výstava
YES	YES	kA	YES
od	od	k7c2	od
Americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
uměleckých	umělecký	k2eAgMnPc2d1	umělecký
kritiků	kritik	k1gMnPc2	kritik
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Skowheganova	Skowheganův	k2eAgFnSc1d1	Skowheganův
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
rozmanitých	rozmanitý	k2eAgInPc2d1	rozmanitý
prostředků	prostředek	k1gInPc2	prostředek
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
*	*	kIx~	*
<g/>
]	]	kIx)	]
=	=	kIx~	=
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Lennonem	Lennon	k1gMnSc7	Lennon
</s>
</p>
<p>
<s>
Unfinished	Unfinished	k1gMnSc1	Unfinished
Music	Music	k1gMnSc1	Music
No	no	k9	no
<g/>
.1	.1	k4	.1
<g/>
:	:	kIx,	:
Two	Two	k1gFnSc1	Two
Virgins	Virginsa	k1gFnPc2	Virginsa
[	[	kIx(	[
<g/>
*	*	kIx~	*
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Unfinished	Unfinished	k1gMnSc1	Unfinished
Music	Music	k1gMnSc1	Music
No	no	k9	no
<g/>
.2	.2	k4	.2
<g/>
:	:	kIx,	:
Life	Lif	k1gInSc2	Lif
With	With	k1gInSc1	With
The	The	k1gMnSc1	The
Lions	Lions	k1gInSc1	Lions
[	[	kIx(	[
<g/>
*	*	kIx~	*
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wedding	Wedding	k1gInSc1	Wedding
Album	album	k1gNnSc1	album
[	[	kIx(	[
<g/>
*	*	kIx~	*
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Live	Live	k1gNnSc1	Live
Peace	Peace	k1gFnSc2	Peace
In	In	k1gFnPc1	In
Toronto	Toronto	k1gNnSc1	Toronto
1969	[number]	k4	1969
[	[	kIx(	[
<g/>
*	*	kIx~	*
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
10	[number]	k4	10
</s>
</p>
<p>
<s>
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
<g/>
/	/	kIx~	/
<g/>
Plastic	Plastice	k1gFnPc2	Plastice
Ono	onen	k3xDgNnSc1	onen
Band	banda	k1gFnPc2	banda
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
182	[number]	k4	182
</s>
</p>
<p>
<s>
Fly	Fly	k?	Fly
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
199	[number]	k4	199
</s>
</p>
<p>
<s>
Sometime	Sometimat	k5eAaPmIp3nS	Sometimat
In	In	k1gFnPc7	In
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
City	City	k1gFnSc1	City
[	[	kIx(	[
<g/>
*	*	kIx~	*
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
48	[number]	k4	48
</s>
</p>
<p>
<s>
Approximately	Approximatela	k1gFnPc1	Approximatela
Infinite	Infinit	k1gInSc5	Infinit
Universe	Universe	k1gFnPc1	Universe
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
193	[number]	k4	193
</s>
</p>
<p>
<s>
Feeling	Feeling	k1gInSc1	Feeling
the	the	k?	the
Space	Space	k1gFnSc2	Space
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
A	a	k9	a
Story	story	k1gFnSc1	story
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Double	double	k2eAgInPc1d1	double
Fantasy	fantas	k1gInPc1	fantas
[	[	kIx(	[
<g/>
*	*	kIx~	*
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
1	[number]	k4	1
</s>
</p>
<p>
<s>
Season	Season	k1gInSc1	Season
of	of	k?	of
Glass	Glass	k1gInSc1	Glass
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
49	[number]	k4	49
</s>
</p>
<p>
<s>
It	It	k?	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Alright	Alrighta	k1gFnPc2	Alrighta
(	(	kIx(	(
<g/>
I	i	k9	i
See	See	k1gFnSc4	See
Rainbows	Rainbowsa	k1gFnPc2	Rainbowsa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
98	[number]	k4	98
</s>
</p>
<p>
<s>
Every	Evera	k1gFnPc1	Evera
Man	mana	k1gFnPc2	mana
Has	hasit	k5eAaImRp2nS	hasit
A	a	k9	a
Woman	Woman	k1gInSc1	Woman
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
hudebníky	hudebník	k1gMnPc7	hudebník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Milk	Milk	k1gInSc1	Milk
and	and	k?	and
Honey	Honea	k1gFnSc2	Honea
[	[	kIx(	[
<g/>
*	*	kIx~	*
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
11	[number]	k4	11
</s>
</p>
<p>
<s>
Starpeace	Starpeace	k1gFnSc1	Starpeace
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Onobox	Onobox	k1gInSc1	Onobox
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Walking	Walking	k1gInSc4	Walking
On	on	k3xPp3gMnSc1	on
Thin	Thin	k1gMnSc1	Thin
Ice	Ice	k1gMnSc1	Ice
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
New	New	k?	New
York	York	k1gInSc1	York
Rock	rock	k1gInSc1	rock
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rising	Rising	k1gInSc1	Rising
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rising	Rising	k1gInSc1	Rising
Mixes	Mixes	k1gInSc1	Mixes
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Blueprint	Blueprint	k1gMnSc1	Blueprint
For	forum	k1gNnPc2	forum
A	a	k9	a
Sunrise	Sunrise	k1gFnSc1	Sunrise
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Yokokimthurston	Yokokimthurston	k1gInSc1	Yokokimthurston
-	-	kIx~	-
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
/	/	kIx~	/
Thurston	Thurston	k1gInSc1	Thurston
Moore	Moor	k1gInSc5	Moor
/	/	kIx~	/
Kim	Kim	k1gMnSc1	Kim
Gordon	Gordon	k1gMnSc1	Gordon
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Bibliografie	bibliografie	k1gFnSc2	bibliografie
==	==	k?	==
</s>
</p>
<p>
<s>
Grapefruit	grapefruit	k1gInSc1	grapefruit
</s>
</p>
<p>
<s>
It	It	k?	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Me	Me	k1gMnSc7	Me
(	(	kIx(	(
<g/>
v	v	k7c6	v
japonštině	japonština	k1gFnSc6	japonština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
O	O	kA	O
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc4	onen
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Clayson	Clayson	k1gMnSc1	Clayson
<g/>
,	,	kIx,	,
Alan	Alan	k1gMnSc1	Alan
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Woman	Woman	k1gMnSc1	Woman
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Incredible	Incredible	k1gMnSc2	Incredible
Life	Life	k1gNnSc2	Life
of	of	k?	of
Yoko	Yoko	k1gMnSc1	Yoko
Ono	onen	k3xDgNnSc1	onen
</s>
</p>
<p>
<s>
Goldman	Goldman	k1gMnSc1	Goldman
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Lives	Lives	k1gMnSc1	Lives
of	of	k?	of
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
</s>
</p>
<p>
<s>
Green	Green	k1gInSc1	Green
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
:	:	kIx,	:
Dakota	Dakota	k1gFnSc1	Dakota
Days	Daysa	k1gFnPc2	Daysa
</s>
</p>
<p>
<s>
Hendricks	Hendricks	k1gInSc1	Hendricks
<g/>
,	,	kIx,	,
Geoffrey	Geoffrey	k1gInPc1	Geoffrey
<g/>
:	:	kIx,	:
Fluxus	Fluxus	k1gInSc1	Fluxus
Codex	Codex	k1gInSc1	Codex
</s>
</p>
<p>
<s>
Hendricks	Hendricks	k1gInSc1	Hendricks
<g/>
,	,	kIx,	,
Geoffrey	Geoffrey	k1gInPc1	Geoffrey
<g/>
:	:	kIx,	:
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
<g/>
:	:	kIx,	:
Arias	Arias	k1gInSc1	Arias
and	and	k?	and
Objects	Objects	k1gInSc1	Objects
</s>
</p>
<p>
<s>
Hopkins	Hopkins	k1gInSc1	Hopkins
<g/>
,	,	kIx,	,
Jerry	Jerr	k1gInPc1	Jerr
<g/>
:	:	kIx,	:
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
</s>
</p>
<p>
<s>
Millett	Millett	k1gMnSc1	Millett
<g/>
,	,	kIx,	,
Kate	kat	k1gMnSc5	kat
<g/>
:	:	kIx,	:
Flying	Flying	k1gInSc1	Flying
</s>
</p>
<p>
<s>
Pang	Pang	k1gMnSc1	Pang
<g/>
,	,	kIx,	,
May	May	k1gMnSc1	May
<g/>
:	:	kIx,	:
Loving	Loving	k1gInSc1	Loving
John	John	k1gMnSc1	John
</s>
</p>
<p>
<s>
Rumaker	Rumaker	k1gMnSc1	Rumaker
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Butterfly	butterfly	k1gInPc2	butterfly
</s>
</p>
<p>
<s>
Seaman	Seaman	k1gMnSc1	Seaman
<g/>
,	,	kIx,	,
Frederic	Frederic	k1gMnSc1	Frederic
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Last	Last	k1gMnSc1	Last
Days	Daysa	k1gFnPc2	Daysa
of	of	k?	of
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Poslední	poslední	k2eAgInPc1d1	poslední
dny	den	k1gInPc1	den
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
<g/>
.	.	kIx.	.
</s>
<s>
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
35	[number]	k4	35
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
překl	překl	k1gInSc1	překl
<g/>
.	.	kIx.	.
</s>
<s>
Tamara	Tamara	k1gFnSc1	Tamara
Nathová	Nathová	k1gFnSc1	Nathová
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sheff	Sheff	k1gMnSc1	Sheff
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
and	and	k?	and
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Playboy	playboy	k1gMnSc1	playboy
Interviews	Interviews	k1gInSc1	Interviews
</s>
</p>
<p>
<s>
Weiner	Weiner	k1gMnSc1	Weiner
<g/>
,	,	kIx,	,
Jon	Jon	k1gMnSc1	Jon
<g/>
:	:	kIx,	:
Come	Come	k1gFnSc1	Come
Together	Togethra	k1gFnPc2	Togethra
</s>
</p>
<p>
<s>
Wenner	Wenner	k1gMnSc1	Wenner
<g/>
,	,	kIx,	,
Jann	Jann	k1gMnSc1	Jann
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Ballad	Ballad	k1gInSc1	Ballad
of	of	k?	of
John	John	k1gMnSc1	John
and	and	k?	and
Yoko	Yoko	k1gMnSc1	Yoko
</s>
</p>
<p>
<s>
Yoon	Yoon	k1gMnSc1	Yoon
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
Project	Project	k2eAgMnSc1d1	Project
</s>
</p>
<p>
<s>
==	==	k?	==
Zdroje	zdroj	k1gInPc1	zdroj
==	==	k?	==
</s>
</p>
<p>
<s>
Bairdová	Bairdová	k1gFnSc1	Bairdová
<g/>
,	,	kIx,	,
Julie	Julie	k1gFnSc1	Julie
<g/>
;	;	kIx,	;
Giuliano	Giuliana	k1gFnSc5	Giuliana
<g/>
,	,	kIx,	,
Geoffrey	Geoffrey	k1gInPc1	Geoffrey
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
–	–	k?	–
můj	můj	k1gMnSc1	můj
bratr	bratr	k1gMnSc1	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
filmový	filmový	k2eAgInSc1d1	filmový
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7004-072-6	[number]	k4	80-7004-072-6
</s>
</p>
<p>
<s>
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
</s>
</p>
<p>
<s>
Beatles	Beatles	k1gFnPc1	Beatles
</s>
</p>
<p>
<s>
Sean	Sean	k1gMnSc1	Sean
Lennon	Lennon	k1gMnSc1	Lennon
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc4	onen
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
<g/>
http://www.yoko-ono.com/	[url]	k?	http://www.yoko-ono.com/
</s>
</p>
<p>
<s>
https://web.archive.org/web/20060901184036/http://www.instantkarma.com/yokomenu.html	[url]	k1gMnSc1	https://web.archive.org/web/20060901184036/http://www.instantkarma.com/yokomenu.html
</s>
</p>
<p>
<s>
http://www.radio.cz/cz/clanek/48406	[url]	k4	http://www.radio.cz/cz/clanek/48406
</s>
</p>
<p>
<s>
http://www.myspace.com/yokoono	[url]	k1gFnSc5	http://www.myspace.com/yokoono
</s>
</p>
