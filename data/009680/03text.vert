<p>
<s>
Slovní	slovní	k2eAgInSc1d1	slovní
přízvuk	přízvuk	k1gInSc1	přízvuk
(	(	kIx(	(
<g/>
akcent	akcent	k1gInSc1	akcent
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fonetický	fonetický	k2eAgInSc4d1	fonetický
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
slabiku	slabika	k1gFnSc4	slabika
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
větný	větný	k2eAgInSc1d1	větný
přízvuk	přízvuk	k1gInSc1	přízvuk
(	(	kIx(	(
<g/>
stress	stress	k1gInSc1	stress
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
část	část	k1gFnSc4	část
věty	věta	k1gFnSc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
zesílení	zesílení	k1gNnSc2	zesílení
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zvýšení	zvýšení	k1gNnSc4	zvýšení
hlasu	hlas	k1gInSc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
suprasegmentální	suprasegmentální	k2eAgInPc4d1	suprasegmentální
prvky	prvek	k1gInPc4	prvek
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Umístění	umístění	k1gNnSc1	umístění
slovního	slovní	k2eAgInSc2d1	slovní
přízvuku	přízvuk	k1gInSc2	přízvuk
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
výslovnost	výslovnost	k1gFnSc4	výslovnost
přízvučné	přízvučný	k2eAgFnSc2d1	přízvučná
slabiky	slabika	k1gFnSc2	slabika
i	i	k8xC	i
některých	některý	k3yIgFnPc2	některý
dalších	další	k2eAgFnPc2d1	další
hlásek	hláska	k1gFnPc2	hláska
před	před	k7c7	před
přízvukem	přízvuk	k1gInSc7	přízvuk
či	či	k8xC	či
za	za	k7c7	za
přízvukem	přízvuk	k1gInSc7	přízvuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
se	s	k7c7	s
přízvukem	přízvuk	k1gInSc7	přízvuk
(	(	kIx(	(
<g/>
cizím	cizí	k2eAgInSc7d1	cizí
přízvukem	přízvuk	k1gInSc7	přízvuk
<g/>
)	)	kIx)	)
rozumí	rozumět	k5eAaImIp3nS	rozumět
celkově	celkově	k6eAd1	celkově
pozměněná	pozměněný	k2eAgFnSc1d1	pozměněná
přízvučnost	přízvučnost	k1gFnSc1	přízvučnost
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
ovlivněná	ovlivněný	k2eAgFnSc1d1	ovlivněná
zpravidla	zpravidla	k6eAd1	zpravidla
jiným	jiný	k2eAgInSc7d1	jiný
jazykem	jazyk	k1gInSc7	jazyk
nebo	nebo	k8xC	nebo
dialektem	dialekt	k1gInSc7	dialekt
<g/>
;	;	kIx,	;
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
příznakem	příznak	k1gInSc7	příznak
nedostatečného	dostatečný	k2eNgNnSc2d1	nedostatečné
osvojení	osvojení	k1gNnSc2	osvojení
daného	daný	k2eAgInSc2d1	daný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
neurologické	neurologický	k2eAgFnPc1d1	neurologická
poruchy	porucha	k1gFnPc1	porucha
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
syndrom	syndrom	k1gInSc1	syndrom
cizího	cizí	k2eAgInSc2d1	cizí
přízvuku	přízvuk	k1gInSc2	přízvuk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
s	s	k7c7	s
pevným	pevný	k2eAgInSc7d1	pevný
slovním	slovní	k2eAgInSc7d1	slovní
přízvukem	přízvuk	k1gInSc7	přízvuk
je	být	k5eAaImIp3nS	být
přízvukována	přízvukovat	k5eAaPmNgFnS	přízvukovat
vždy	vždy	k6eAd1	vždy
nebo	nebo	k8xC	nebo
zpravidla	zpravidla	k6eAd1	zpravidla
jen	jen	k9	jen
určitá	určitý	k2eAgFnSc1d1	určitá
slabika	slabika	k1gFnSc1	slabika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
první	první	k4xOgInSc4	první
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
předposlední	předposlední	k2eAgFnSc1d1	předposlední
(	(	kIx(	(
<g/>
penultima	penultima	k1gFnSc1	penultima
<g/>
)	)	kIx)	)
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
latině	latina	k1gFnSc6	latina
je	být	k5eAaImIp3nS	být
přízvučná	přízvučný	k2eAgFnSc1d1	přízvučná
druhá	druhý	k4xOgFnSc1	druhý
slabika	slabika	k1gFnSc1	slabika
od	od	k7c2	od
konce	konec	k1gInSc2	konec
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
přirozeně	přirozeně	k6eAd1	přirozeně
nebo	nebo	k8xC	nebo
polohou	poloha	k1gFnSc7	poloha
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
třetí	třetí	k4xOgFnSc1	třetí
slabika	slabika	k1gFnSc1	slabika
od	od	k7c2	od
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
s	s	k7c7	s
pohyblivým	pohyblivý	k2eAgInSc7d1	pohyblivý
slovním	slovní	k2eAgInSc7d1	slovní
přízvukem	přízvuk	k1gInSc7	přízvuk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přízvučná	přízvučný	k2eAgFnSc1d1	přízvučná
kterákoliv	kterýkoliv	k3yIgFnSc1	kterýkoliv
slabika	slabika	k1gFnSc1	slabika
<g/>
,	,	kIx,	,
různá	různý	k2eAgFnSc1d1	různá
poloha	poloha	k1gFnSc1	poloha
přízvuku	přízvuk	k1gInSc2	přízvuk
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
distinktivní	distinktivní	k2eAgInSc4d1	distinktivní
(	(	kIx(	(
<g/>
význam	význam	k1gInSc4	význam
rozlišující	rozlišující	k2eAgInSc4d1	rozlišující
<g/>
)	)	kIx)	)
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přízvuk	přízvuk	k1gInSc1	přízvuk
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
značen	značen	k2eAgInSc4d1	značen
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
se	se	k3xPyFc4	se
v	v	k7c6	v
učebních	učební	k2eAgInPc6d1	učební
textech	text	k1gInPc6	text
značí	značit	k5eAaImIp3nS	značit
čárkou	čárka	k1gFnSc7	čárka
nad	nad	k7c7	nad
samohláskou	samohláska	k1gFnSc7	samohláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přízvuk	přízvuk	k1gInSc1	přízvuk
může	moct	k5eAaImIp3nS	moct
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
výslovnost	výslovnost	k1gFnSc1	výslovnost
hlásek	hlásek	k1gInSc4	hlásek
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
<g/>
,	,	kIx,	,
angličtině	angličtina	k1gFnSc6	angličtina
či	či	k8xC	či
němčině	němčina	k1gFnSc6	němčina
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
redukované	redukovaný	k2eAgFnSc3d1	redukovaná
výslovnosti	výslovnost	k1gFnSc3	výslovnost
nepřízvučných	přízvučný	k2eNgFnPc2d1	nepřízvučná
samohlásek	samohláska	k1gFnPc2	samohláska
(	(	kIx(	(
<g/>
např.	např.	kA	např.
psané	psaný	k2eAgFnPc1d1	psaná
e	e	k0	e
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
ə	ə	k?	ə
(	(	kIx(	(
<g/>
v	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
literatuře	literatura	k1gFnSc6	literatura
často	často	k6eAd1	často
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
Schwa	Schw	k2eAgFnSc1d1	Schw
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
<g/>
)	)	kIx)	)
/	/	kIx~	/
schwa	schwa	k1gFnSc1	schwa
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
pravopis	pravopis	k1gInSc1	pravopis
i	i	k8xC	i
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g />
.	.	kIx.	.
</s>
<s>
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
německý	německý	k2eAgMnSc1d1	německý
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c6	na
psaní	psaní	k1gNnSc6	psaní
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
označení	označení	k1gNnSc1	označení
šva	šva	k1gNnSc2	šva
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
významu	význam	k1gInSc6	význam
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
tak	tak	k6eAd1	tak
běžné	běžný	k2eAgFnPc4d1	běžná
–	–	k?	–
česky	česky	k6eAd1	česky
nejjednoznačněji	jednoznačně	k6eAd3	jednoznačně
jako	jako	k8xC	jako
redukované	redukovaný	k2eAgInPc1d1	redukovaný
e	e	k0	e
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
též	též	k9	též
Murmellaut	Murmellaut	k1gMnSc1	Murmellaut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
angl.	angl.	k?	angl.
about	about	k1gInSc1	about
[	[	kIx(	[
<g/>
ə	ə	k?	ə
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
obsession	obsession	k1gInSc1	obsession
[	[	kIx(	[
<g/>
ə	ə	k?	ə
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
taken	taken	k1gInSc1	taken
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
Frage	Frage	k1gInSc1	Frage
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
:	:	kIx,	:
<g/>
gə	gə	k?	gə
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
v	v	k7c6	v
neodlučitelných	odlučitelný	k2eNgFnPc6d1	neodlučitelná
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
nepřízvučných	přízvučný	k2eNgFnPc6d1	nepřízvučná
předponách	předpona	k1gFnPc6	předpona
<g/>
,	,	kIx,	,
např.	např.	kA	např.
bekommen	bekommen	k1gInSc1	bekommen
[	[	kIx(	[
<g/>
bə	bə	k?	bə
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přízvuk	přízvuk	k1gInSc1	přízvuk
též	též	k9	též
často	často	k6eAd1	často
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
i	i	k9	i
délku	délka	k1gFnSc4	délka
slabik	slabika	k1gFnPc2	slabika
(	(	kIx(	(
<g/>
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
se	se	k3xPyFc4	se
přízvučné	přízvučný	k2eAgFnPc1d1	přízvučná
samohlásky	samohláska	k1gFnPc1	samohláska
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
delší	dlouhý	k2eAgFnPc1d2	delší
než	než	k8xS	než
nepřízvučné	přízvučný	k2eNgFnPc1d1	nepřízvučná
<g/>
,	,	kIx,	,
ve	v	k7c6	v
švédštině	švédština	k1gFnSc6	švédština
jsou	být	k5eAaImIp3nP	být
přízvučné	přízvučný	k2eAgFnPc1d1	přízvučná
slabiky	slabika	k1gFnPc1	slabika
vždy	vždy	k6eAd1	vždy
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
nepřízvučné	přízvučný	k2eNgFnPc1d1	nepřízvučná
vždy	vždy	k6eAd1	vždy
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
k	k	k7c3	k
takovýmto	takovýto	k3xDgInPc3	takovýto
jevům	jev	k1gInPc3	jev
nedochází	docházet	k5eNaImIp3nS	docházet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
delších	dlouhý	k2eAgNnPc2d2	delší
slov	slovo	k1gNnPc2	slovo
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
hlavního	hlavní	k2eAgInSc2d1	hlavní
přízvuku	přízvuk	k1gInSc2	přízvuk
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
i	i	k9	i
slabší	slabý	k2eAgInSc1d2	slabší
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
přízvuk	přízvuk	k1gInSc1	přízvuk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fonetickém	fonetický	k2eAgInSc6d1	fonetický
přepisu	přepis	k1gInSc6	přepis
výslovnosti	výslovnost	k1gFnSc2	výslovnost
(	(	kIx(	(
<g/>
IPA	IPA	kA	IPA
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
hlavní	hlavní	k2eAgInSc1d1	hlavní
přízvuk	přízvuk	k1gInSc1	přízvuk
označuje	označovat	k5eAaImIp3nS	označovat
horní	horní	k2eAgFnSc7d1	horní
kolmičkou	kolmička	k1gFnSc7	kolmička
(	(	kIx(	(
ˈ	ˈ	k?	ˈ
)	)	kIx)	)
<g/>
,	,	kIx,	,
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
přízvuk	přízvuk	k1gInSc1	přízvuk
dolní	dolní	k2eAgFnSc2d1	dolní
kolmičkou	kolmička	k1gFnSc7	kolmička
(	(	kIx(	(
ˌ	ˌ	k?	ˌ
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
předposlední	předposlední	k2eAgMnSc1d1	předposlední
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
̝	̝	k?	̝
<g/>
̊	̊	k?	̊
<g/>
ɛ	ɛ	k?	ɛ
<g/>
.	.	kIx.	.
<g/>
po	po	k7c4	po
<g/>
.	.	kIx.	.
<g/>
ˌ	ˌ	k?	ˌ
<g/>
.	.	kIx.	.
<g/>
ɲ	ɲ	k?	ɲ
<g/>
:	:	kIx,	:
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některá	některý	k3yIgNnPc1	některý
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
klitika	klitika	k1gFnSc1	klitika
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nepřízvučná	přízvučný	k2eNgNnPc1d1	nepřízvučné
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
přízvučný	přízvučný	k2eAgInSc4d1	přízvučný
celek	celek	k1gInSc4	celek
s	s	k7c7	s
předchozím	předchozí	k2eAgInSc7d1	předchozí
(	(	kIx(	(
<g/>
příklonky	příklonka	k1gFnSc2	příklonka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
následujícím	následující	k2eAgInPc3d1	následující
(	(	kIx(	(
<g/>
předklonky	předklonka	k1gFnSc2	předklonka
<g/>
)	)	kIx)	)
slovem	slovo	k1gNnSc7	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Příklonky	příklonka	k1gFnPc1	příklonka
jsou	být	k5eAaImIp3nP	být
slova	slovo	k1gNnPc4	slovo
trvale	trvale	k6eAd1	trvale
nepřízvučná	přízvučný	k2eNgNnPc4d1	nepřízvučné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
připojují	připojovat	k5eAaImIp3nP	připojovat
za	za	k7c4	za
přízvučné	přízvučný	k2eAgNnSc4d1	přízvučné
slovo	slovo	k1gNnSc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Předklonky	předklonka	k1gFnPc1	předklonka
stojí	stát	k5eAaImIp3nP	stát
před	před	k7c7	před
přízvučným	přízvučný	k2eAgNnSc7d1	přízvučné
slovem	slovo	k1gNnSc7	slovo
a	a	k8xC	a
spojují	spojovat	k5eAaImIp3nP	spojovat
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Přízvuk	přízvuk	k1gInSc1	přízvuk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
přízvuk	přízvuk	k1gInSc1	přízvuk
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
