<p>
<s>
Síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
vektorová	vektorový	k2eAgFnSc1d1	vektorová
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
míru	míra	k1gFnSc4	míra
působení	působení	k1gNnSc3	působení
těles	těleso	k1gNnPc2	těleso
nebo	nebo	k8xC	nebo
polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
statickými	statický	k2eAgInPc7d1	statický
účinky	účinek	k1gInPc7	účinek
–	–	k?	–
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
deformace	deformace	k1gFnSc1	deformace
těles	těleso	k1gNnPc2	těleso
–	–	k?	–
a	a	k8xC	a
dynamickými	dynamický	k2eAgInPc7d1	dynamický
účinky	účinek	k1gInPc7	účinek
–	–	k?	–
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
změny	změna	k1gFnSc2	změna
pohybového	pohybový	k2eAgInSc2d1	pohybový
stavu	stav	k1gInSc2	stav
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
hmotného	hmotný	k2eAgInSc2d1	hmotný
bodu	bod	k1gInSc2	bod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
uvedení	uvedení	k1gNnSc1	uvedení
tělesa	těleso	k1gNnSc2	těleso
z	z	k7c2	z
klidu	klid	k1gInSc2	klid
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
či	či	k8xC	či
změny	změna	k1gFnPc1	změna
velikosti	velikost	k1gFnSc2	velikost
nebo	nebo	k8xC	nebo
směru	směr	k1gInSc2	směr
rychlosti	rychlost	k1gFnSc2	rychlost
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
změna	změna	k1gFnSc1	změna
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
v	v	k7c6	v
inerciální	inerciální	k2eAgFnSc6d1	inerciální
soustavě	soustava	k1gFnSc6	soustava
<g/>
)	)	kIx)	)
vždy	vždy	k6eAd1	vždy
podmíněna	podmínit	k5eAaPmNgFnS	podmínit
působením	působení	k1gNnSc7	působení
jiných	jiný	k2eAgNnPc2d1	jiné
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
přímým	přímý	k2eAgInSc7d1	přímý
dotykem	dotyk	k1gInSc7	dotyk
(	(	kIx(	(
<g/>
nárazem	náraz	k1gInSc7	náraz
<g/>
,	,	kIx,	,
třením	tření	k1gNnSc7	tření
<g/>
,	,	kIx,	,
tažením	tažení	k1gNnSc7	tažení
<g/>
,	,	kIx,	,
tlačením	tlačení	k1gNnSc7	tlačení
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
silového	silový	k2eAgNnSc2d1	silové
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
působení	působení	k1gNnSc1	působení
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Newtonově	Newtonův	k2eAgFnSc6d1	Newtonova
mechanice	mechanika	k1gFnSc6	mechanika
spojováno	spojovat	k5eAaImNgNnS	spojovat
s	s	k7c7	s
existencí	existence	k1gFnSc7	existence
síly	síla	k1gFnSc2	síla
působící	působící	k2eAgFnSc2d1	působící
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgNnPc7	dva
interagujícími	interagující	k2eAgNnPc7d1	interagující
tělesy	těleso	k1gNnPc7	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Síla	síla	k1gFnSc1	síla
není	být	k5eNaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
pohybu	pohyb	k1gInSc2	pohyb
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
příčina	příčina	k1gFnSc1	příčina
pohybu	pohyb	k1gInSc2	pohyb
byla	být	k5eAaImAgFnS	být
síla	síla	k1gFnSc1	síla
chápána	chápat	k5eAaImNgFnS	chápat
v	v	k7c6	v
aristotelské	aristotelský	k2eAgFnSc6d1	aristotelská
filosofii	filosofie	k1gFnSc6	filosofie
přírody	příroda	k1gFnSc2	příroda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
síly	síla	k1gFnSc2	síla
je	být	k5eAaImIp3nS	být
zobecněn	zobecnit	k5eAaPmNgInS	zobecnit
rozšířením	rozšíření	k1gNnSc7	rozšíření
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
zdánlivé	zdánlivý	k2eAgFnPc4d1	zdánlivá
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
původ	původ	k1gInSc4	původ
nikoli	nikoli	k9	nikoli
ve	v	k7c6	v
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
působení	působení	k1gNnSc6	působení
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
zrychleném	zrychlený	k2eAgInSc6d1	zrychlený
pohybu	pohyb	k1gInSc6	pohyb
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
síly	síla	k1gFnSc2	síla
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
pojmem	pojem	k1gInSc7	pojem
pro	pro	k7c4	pro
vektorovou	vektorový	k2eAgFnSc4d1	vektorová
formulaci	formulace	k1gFnSc4	formulace
mechaniky	mechanika	k1gFnSc2	mechanika
a	a	k8xC	a
elektrodynamiky	elektrodynamika	k1gFnSc2	elektrodynamika
<g/>
.	.	kIx.	.
</s>
<s>
Analytická	analytický	k2eAgFnSc1d1	analytická
mechanika	mechanika	k1gFnSc1	mechanika
<g/>
,	,	kIx,	,
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
ani	ani	k8xC	ani
kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
již	již	k9	již
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
pojmu	pojem	k1gInSc2	pojem
nevycházejí	vycházet	k5eNaImIp3nP	vycházet
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
na	na	k7c6	na
základě	základ	k1gInSc6	základ
analogie	analogie	k1gFnSc2	analogie
či	či	k8xC	či
principu	princip	k1gInSc2	princip
korespondence	korespondence	k1gFnSc2	korespondence
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
sílu	síla	k1gFnSc4	síla
nebo	nebo	k8xC	nebo
její	její	k3xOp3gNnSc4	její
zobecnění	zobecnění	k1gNnSc4	zobecnění
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
vektorovou	vektorový	k2eAgFnSc7d1	vektorová
veličinou	veličina	k1gFnSc7	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
působící	působící	k2eAgFnSc1d1	působící
na	na	k7c4	na
hmotný	hmotný	k2eAgInSc4d1	hmotný
bod	bod	k1gInSc4	bod
je	být	k5eAaImIp3nS	být
vázaným	vázaný	k2eAgInSc7d1	vázaný
vektorem	vektor	k1gInSc7	vektor
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
působiště	působiště	k1gNnSc4	působiště
síly	síla	k1gFnSc2	síla
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bodě	bod	k1gInSc6	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
siloměrem	siloměr	k1gInSc7	siloměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Motivace	motivace	k1gFnSc1	motivace
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
pojmu	pojmout	k5eAaPmIp1nS	pojmout
síla	síla	k1gFnSc1	síla
==	==	k?	==
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
síly	síla	k1gFnSc2	síla
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
denní	denní	k2eAgFnSc2d1	denní
zkušenosti	zkušenost	k1gFnSc2	zkušenost
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pohybový	pohybový	k2eAgInSc1d1	pohybový
stav	stav	k1gInSc1	stav
nějakého	nějaký	k3yIgNnSc2	nějaký
tělesa	těleso	k1gNnSc2	těleso
můžeme	moct	k5eAaImIp1nP	moct
měnit	měnit	k5eAaImF	měnit
např.	např.	kA	např.
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
vlastním	vlastnit	k5eAaImIp1nS	vlastnit
dotykem	dotyk	k1gInSc7	dotyk
urychlíme	urychlit	k5eAaPmIp1nP	urychlit
<g/>
,	,	kIx,	,
zastavíme	zastavit	k5eAaPmIp1nP	zastavit
nebo	nebo	k8xC	nebo
odchýlíme	odchýlit	k5eAaPmIp1nP	odchýlit
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
směru	směr	k1gInSc2	směr
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
to	ten	k3xDgNnSc4	ten
lze	lze	k6eAd1	lze
udělat	udělat	k5eAaPmF	udělat
"	"	kIx"	"
<g/>
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
<g/>
"	"	kIx"	"
silovým	silový	k2eAgNnSc7d1	silové
polem	pole	k1gNnSc7	pole
<g/>
,	,	kIx,	,
např.	např.	kA	např.
elektrickým	elektrický	k2eAgNnSc7d1	elektrické
polem	pole	k1gNnSc7	pole
u	u	k7c2	u
nabitého	nabitý	k2eAgNnSc2d1	nabité
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Těleso	těleso	k1gNnSc1	těleso
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
tekutého	tekutý	k2eAgNnSc2d1	tekuté
<g/>
)	)	kIx)	)
také	také	k9	také
můžeme	moct	k5eAaImIp1nP	moct
stlačit	stlačit	k5eAaPmF	stlačit
nebo	nebo	k8xC	nebo
roztáhnout	roztáhnout	k5eAaPmF	roztáhnout
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
deformovat	deformovat	k5eAaImF	deformovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Intuitivně	intuitivně	k6eAd1	intuitivně
chápeme	chápat	k5eAaImIp1nP	chápat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
účinky	účinek	k1gInPc1	účinek
mají	mít	k5eAaImIp3nP	mít
obdobnou	obdobný	k2eAgFnSc4d1	obdobná
příčinu	příčina	k1gFnSc4	příčina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
lze	lze	k6eAd1	lze
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
pojmem	pojem	k1gInSc7	pojem
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
kvantifikovat	kvantifikovat	k5eAaBmF	kvantifikovat
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
fyzikální	fyzikální	k2eAgFnSc4d1	fyzikální
veličinu	veličina	k1gFnSc4	veličina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
síla	síla	k1gFnSc1	síla
působí	působit	k5eAaImIp3nS	působit
<g/>
,	,	kIx,	,
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
různé	různý	k2eAgFnPc1d1	různá
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
např.	např.	kA	např.
elastické	elastický	k2eAgFnPc1d1	elastická
<g/>
,	,	kIx,	,
elektromagnetické	elektromagnetický	k2eAgFnPc1d1	elektromagnetická
<g/>
,	,	kIx,	,
kapilární	kapilární	k2eAgFnPc1d1	kapilární
<g/>
,	,	kIx,	,
třecí	třecí	k2eAgFnPc1d1	třecí
síla	síla	k1gFnSc1	síla
atd.	atd.	kA	atd.
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejběžnějších	běžný	k2eAgFnPc2d3	nejběžnější
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
neustále	neustále	k6eAd1	neustále
(	(	kIx(	(
<g/>
aniž	aniž	k8xS	aniž
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
obvykle	obvykle	k6eAd1	obvykle
uvědomujeme	uvědomovat	k5eAaImIp1nP	uvědomovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
jsme	být	k5eAaImIp1nP	být
přitahováni	přitahován	k2eAgMnPc1d1	přitahován
k	k	k7c3	k
naší	náš	k3xOp1gFnSc3	náš
planetě	planeta	k1gFnSc3	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Značení	značení	k1gNnPc1	značení
a	a	k8xC	a
jednotky	jednotka	k1gFnPc1	jednotka
==	==	k?	==
</s>
</p>
<p>
<s>
Síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
značí	značit	k5eAaImIp3nS	značit
písmenem	písmeno	k1gNnSc7	písmeno
F	F	kA	F
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
force	force	k1gFnSc3	force
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	se	k3xPyFc3	se
má	mít	k5eAaImIp3nS	mít
hlavní	hlavní	k2eAgFnSc4d1	hlavní
jednotku	jednotka	k1gFnSc4	jednotka
newton	newton	k1gInSc1	newton
se	s	k7c7	s
značkou	značka	k1gFnSc7	značka
N	N	kA	N
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
rozměr	rozměr	k1gInSc1	rozměr
síly	síla	k1gFnSc2	síla
je	být	k5eAaImIp3nS	být
kg	kg	kA	kg
<g/>
.	.	kIx.	.
<g/>
m.	m.	k?	m.
<g/>
s	s	k7c7	s
<g/>
−	−	k?	−
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dříve	dříve	k6eAd2	dříve
rozšířené	rozšířený	k2eAgFnSc6d1	rozšířená
technické	technický	k2eAgFnSc6d1	technická
soustavě	soustava	k1gFnSc6	soustava
jednotek	jednotka	k1gFnPc2	jednotka
byl	být	k5eAaImAgMnS	být
jednotkou	jednotka	k1gFnSc7	jednotka
síly	síla	k1gFnSc2	síla
kilopond	kilopond	k1gInSc1	kilopond
(	(	kIx(	(
<g/>
kp	kp	k?	kp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
dokonce	dokonce	k9	dokonce
základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
této	tento	k3xDgFnSc2	tento
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Převodní	převodní	k2eAgInSc1d1	převodní
vztah	vztah	k1gInSc1	vztah
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
kp	kp	k?	kp
=	=	kIx~	=
9,806	[number]	k4	9,806
65	[number]	k4	65
N.	N.	kA	N.
Imperiální	imperiální	k2eAgFnSc7d1	imperiální
jednotkou	jednotka	k1gFnSc7	jednotka
síly	síla	k1gFnSc2	síla
je	být	k5eAaImIp3nS	být
libra	libra	k1gFnSc1	libra
síly	síla	k1gFnSc2	síla
(	(	kIx(	(
<g/>
lbf	lbf	k?	lbf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
platí	platit	k5eAaImIp3nS	platit
převod	převod	k1gInSc1	převod
1	[number]	k4	1
lbf	lbf	k?	lbf
=	=	kIx~	=
4,448	[number]	k4	4,448
22	[number]	k4	22
N.	N.	kA	N.
</s>
</p>
<p>
<s>
Méně	málo	k6eAd2	málo
obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
dekanewton	dekanewton	k1gInSc1	dekanewton
(	(	kIx(	(
<g/>
daN	daN	k?	daN
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
platí	platit	k5eAaImIp3nS	platit
převodní	převodní	k2eAgInSc1d1	převodní
vztah	vztah	k1gInSc1	vztah
1	[number]	k4	1
daN	daN	k?	daN
=	=	kIx~	=
10	[number]	k4	10
N	N	kA	N
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přibližně	přibližně	k6eAd1	přibližně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
1	[number]	k4	1
kp	kp	k?	kp
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
s	s	k7c7	s
dekanewtonem	dekanewton	k1gInSc7	dekanewton
setkat	setkat	k5eAaPmF	setkat
při	pře	k1gFnSc3	pře
stanovení	stanovení	k1gNnSc2	stanovení
přítlaku	přítlak	k1gInSc2	přítlak
elektrod	elektroda	k1gFnPc2	elektroda
odporového	odporový	k2eAgNnSc2d1	odporové
svařování	svařování	k1gNnSc2	svařování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dekanewtonech	dekanewton	k1gInPc6	dekanewton
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
rázová	rázový	k2eAgFnSc1d1	rázová
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
laně	lano	k1gNnSc6	lano
při	při	k7c6	při
pádu	pád	k1gInSc6	pád
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
hodnoty	hodnota	k1gFnPc1	hodnota
jsou	být	k5eAaImIp3nP	být
dosaženy	dosáhnout	k5eAaPmNgFnP	dosáhnout
právě	právě	k6eAd1	právě
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
zastavení	zastavení	k1gNnSc4	zastavení
pádu	pád	k1gInSc2	pád
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
pohlcovat	pohlcovat	k5eAaImF	pohlcovat
energii	energie	k1gFnSc4	energie
pádu	pád	k1gInSc2	pád
a	a	k8xC	a
snižovat	snižovat	k5eAaImF	snižovat
tak	tak	k6eAd1	tak
velikost	velikost	k1gFnSc4	velikost
rázové	rázový	k2eAgFnSc2d1	rázová
síly	síla	k1gFnSc2	síla
v	v	k7c6	v
laně	lano	k1gNnSc6	lano
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
vlastnostech	vlastnost	k1gFnPc6	vlastnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pružnosti	pružnost	k1gFnSc2	pružnost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
normová	normový	k2eAgFnSc1d1	normová
charakteristika	charakteristika	k1gFnSc1	charakteristika
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
rázová	rázový	k2eAgFnSc1d1	rázová
síla	síla	k1gFnSc1	síla
pro	pro	k7c4	pro
kvalitativní	kvalitativní	k2eAgNnSc4d1	kvalitativní
ohodnocení	ohodnocení	k1gNnSc4	ohodnocení
např.	např.	kA	např.
horolezeckých	horolezecký	k2eAgFnPc2d1	horolezecká
lan	lano	k1gNnPc2	lano
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Definice	definice	k1gFnPc4	definice
<g/>
,	,	kIx,	,
základní	základní	k2eAgInPc4d1	základní
vztahy	vztah	k1gInPc4	vztah
a	a	k8xC	a
vlastnosti	vlastnost	k1gFnPc4	vlastnost
síly	síla	k1gFnSc2	síla
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zavedení	zavedení	k1gNnSc1	zavedení
síly	síla	k1gFnSc2	síla
v	v	k7c6	v
Newtonově	Newtonův	k2eAgFnSc6d1	Newtonova
klasické	klasický	k2eAgFnSc6d1	klasická
mechanice	mechanika	k1gFnSc6	mechanika
===	===	k?	===
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
síly	síla	k1gFnSc2	síla
je	být	k5eAaImIp3nS	být
zaveden	zavést	k5eAaPmNgInS	zavést
pomocí	pomocí	k7c2	pomocí
Newtonových	Newtonových	k2eAgInPc2d1	Newtonových
pohybových	pohybový	k2eAgInPc2d1	pohybový
zákonů	zákon	k1gInPc2	zákon
(	(	kIx(	(
<g/>
NZ	NZ	kA	NZ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
platných	platný	k2eAgInPc2d1	platný
pro	pro	k7c4	pro
inerciální	inerciální	k2eAgFnSc4d1	inerciální
vztažnou	vztažný	k2eAgFnSc4d1	vztažná
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
NZ	NZ	kA	NZ
označuje	označovat	k5eAaImIp3nS	označovat
sílu	síla	k1gFnSc4	síla
za	za	k7c4	za
příčinu	příčina	k1gFnSc4	příčina
změn	změna	k1gFnPc2	změna
pohybového	pohybový	k2eAgInSc2d1	pohybový
stavu	stav	k1gInSc2	stav
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
částice	částice	k1gFnSc2	částice
či	či	k8xC	či
hmotného	hmotný	k2eAgInSc2d1	hmotný
bodu	bod	k1gInSc2	bod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
NZ	NZ	kA	NZ
ji	on	k3xPp3gFnSc4	on
kvantifikuje	kvantifikovat	k5eAaBmIp3nS	kvantifikovat
<g/>
:	:	kIx,	:
Síla	síla	k1gFnSc1	síla
působící	působící	k2eAgFnSc1d1	působící
na	na	k7c4	na
volnou	volný	k2eAgFnSc4d1	volná
částici	částice	k1gFnSc4	částice
(	(	kIx(	(
<g/>
při	při	k7c6	při
zanedbání	zanedbání	k1gNnSc6	zanedbání
ostatních	ostatní	k2eAgInPc2d1	ostatní
možných	možný	k2eAgInPc2d1	možný
silových	silový	k2eAgInPc2d1	silový
působení	působení	k1gNnSc4	působení
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
časové	časový	k2eAgFnSc3d1	časová
změně	změna	k1gFnSc3	změna
hybnosti	hybnost	k1gFnSc2	hybnost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
částice	částice	k1gFnSc1	částice
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
síla	síla	k1gFnSc1	síla
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
derivací	derivace	k1gFnSc7	derivace
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}}}	}}}	k?	}}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
<g/>
V	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
lze	lze	k6eAd1	lze
zanedbat	zanedbat	k5eAaPmF	zanedbat
změnu	změna	k1gFnSc4	změna
hmotnosti	hmotnost	k1gFnSc2	hmotnost
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
většiny	většina	k1gFnSc2	většina
pohybů	pohyb	k1gInPc2	pohyb
studovaných	studovaný	k2eAgInPc2d1	studovaný
klasickou	klasický	k2eAgFnSc7d1	klasická
mechanikou	mechanika	k1gFnSc7	mechanika
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
předchozí	předchozí	k2eAgInSc4d1	předchozí
vztah	vztah	k1gInSc4	vztah
rozepsat	rozepsat	k5eAaPmF	rozepsat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
m	m	kA	m
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}}}	}}}	k?	}}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
m	m	kA	m
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m	m	kA	m
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
označuje	označovat	k5eAaImIp3nS	označovat
hmotnost	hmotnost	k1gFnSc4	hmotnost
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
zrychlení	zrychlení	k1gNnSc1	zrychlení
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Definice	definice	k1gFnSc1	definice
síly	síla	k1gFnSc2	síla
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c6	na
pohybové	pohybový	k2eAgFnSc6d1	pohybová
rovnici	rovnice	k1gFnSc6	rovnice
posuvného	posuvný	k2eAgInSc2d1	posuvný
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
NZ	NZ	kA	NZ
pak	pak	k6eAd1	pak
stanoví	stanovit	k5eAaPmIp3nS	stanovit
základní	základní	k2eAgFnSc4d1	základní
vlastnost	vlastnost	k1gFnSc4	vlastnost
pravých	pravý	k2eAgFnPc2d1	pravá
sil	síla	k1gFnPc2	síla
–	–	k?	–
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
<g/>
,	,	kIx,	,
přímé	přímý	k2eAgFnSc2d1	přímá
(	(	kIx(	(
<g/>
centrální	centrální	k2eAgFnSc2d1	centrální
<g/>
)	)	kIx)	)
a	a	k8xC	a
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
působení	působení	k1gNnSc4	působení
ve	v	k7c6	v
dvojici	dvojice	k1gFnSc6	dvojice
akce-reakce	akceeakce	k1gFnSc2	akce-reakce
<g/>
.	.	kIx.	.
</s>
<s>
Poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
tak	tak	k9	tak
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
hmotnosti	hmotnost	k1gFnSc2	hmotnost
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
i	i	k9	i
pro	pro	k7c4	pro
stanovení	stanovení	k1gNnSc4	stanovení
síly	síla	k1gFnSc2	síla
podle	podle	k7c2	podle
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
NZ	NZ	kA	NZ
ze	z	k7c2	z
zrychlení	zrychlení	k1gNnSc2	zrychlení
testovací	testovací	k2eAgFnSc2d1	testovací
částice	částice	k1gFnSc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
vlastností	vlastnost	k1gFnSc7	vlastnost
je	být	k5eAaImIp3nS	být
i	i	k9	i
princip	princip	k1gInSc1	princip
superpozice	superpozice	k1gFnSc2	superpozice
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
označovaný	označovaný	k2eAgMnSc1d1	označovaný
za	za	k7c4	za
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
NZ	NZ	kA	NZ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
síly	síl	k1gInPc1	síl
působící	působící	k2eAgInPc1d1	působící
na	na	k7c4	na
dané	daný	k2eAgNnSc4d1	dané
těleso	těleso	k1gNnSc4	těleso
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
hmotný	hmotný	k2eAgInSc4d1	hmotný
bod	bod	k1gInSc4	bod
<g/>
)	)	kIx)	)
vektorově	vektorově	k6eAd1	vektorově
sčítají	sčítat	k5eAaImIp3nP	sčítat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
neovlivňují	ovlivňovat	k5eNaImIp3nP	ovlivňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
vlastnosti	vlastnost	k1gFnPc1	vlastnost
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
omezenou	omezený	k2eAgFnSc4d1	omezená
platnost	platnost	k1gFnSc4	platnost
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
akce	akce	k1gFnSc2	akce
a	a	k8xC	a
reakce	reakce	k1gFnSc2	reakce
a	a	k8xC	a
centrálnost	centrálnost	k1gFnSc1	centrálnost
působení	působení	k1gNnSc2	působení
např.	např.	kA	např.
obecně	obecně	k6eAd1	obecně
neplatí	platit	k5eNaImIp3nS	platit
u	u	k7c2	u
silového	silový	k2eAgNnSc2d1	silové
působení	působení	k1gNnSc2	působení
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
proměnných	proměnný	k2eAgFnPc2d1	proměnná
silových	silový	k2eAgFnPc2d1	silová
polí	pole	k1gFnPc2	pole
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
část	část	k1gFnSc1	část
hybnosti	hybnost	k1gFnSc2	hybnost
nebo	nebo	k8xC	nebo
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přenášena	přenášet	k5eAaImNgFnS	přenášet
polem	polem	k6eAd1	polem
<g/>
.	.	kIx.	.
</s>
<s>
Názorný	názorný	k2eAgInSc1d1	názorný
je	být	k5eAaImIp3nS	být
příklad	příklad	k1gInSc1	příklad
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
působení	působení	k1gNnSc2	působení
dvou	dva	k4xCgFnPc2	dva
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
pohybujících	pohybující	k2eAgFnPc2d1	pohybující
se	se	k3xPyFc4	se
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
ve	v	k7c6	v
vzájemně	vzájemně	k6eAd1	vzájemně
kolmých	kolmý	k2eAgInPc6d1	kolmý
směrech	směr	k1gInPc6	směr
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
největšího	veliký	k2eAgNnSc2d3	veliký
přiblížení	přiblížení	k1gNnSc2	přiblížení
jedna	jeden	k4xCgFnSc1	jeden
částice	částice	k1gFnSc1	částice
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
pouze	pouze	k6eAd1	pouze
elektrostatickou	elektrostatický	k2eAgFnSc4d1	elektrostatická
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
druhá	druhý	k4xOgFnSc1	druhý
na	na	k7c4	na
první	první	k4xOgNnSc4	první
působí	působit	k5eAaImIp3nS	působit
vedle	vedle	k6eAd1	vedle
stejně	stejně	k6eAd1	stejně
velké	velký	k2eAgFnPc1d1	velká
elektrostatické	elektrostatický	k2eAgFnPc1d1	elektrostatická
reakce	reakce	k1gFnPc1	reakce
také	také	k9	také
silou	síla	k1gFnSc7	síla
magnetickou	magnetický	k2eAgFnSc7d1	magnetická
<g/>
.	.	kIx.	.
</s>
<s>
Silové	silový	k2eAgNnSc1d1	silové
působení	působení	k1gNnSc1	působení
také	také	k9	také
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
okamžité	okamžitý	k2eAgNnSc1d1	okamžité
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
rychlost	rychlost	k1gFnSc1	rychlost
šíření	šíření	k1gNnSc2	šíření
interakce	interakce	k1gFnSc2	interakce
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
omezena	omezen	k2eAgFnSc1d1	omezena
rychlostí	rychlost	k1gFnSc7	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozložení	rozložení	k1gNnSc1	rozložení
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
hybnosti	hybnost	k1gFnSc2	hybnost
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
interakce	interakce	k1gFnSc2	interakce
nelineárně	lineárně	k6eNd1	lineárně
mění	měnit	k5eAaImIp3nP	měnit
metrické	metrický	k2eAgFnPc4d1	metrická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zakřivení	zakřivení	k1gNnSc1	zakřivení
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
časoprostoru	časoprostor	k1gInSc2	časoprostor
a	a	k8xC	a
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
tak	tak	k6eAd1	tak
jiná	jiný	k2eAgNnPc4d1	jiné
působení	působení	k1gNnPc4	působení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
mechanika	mechanika	k1gFnSc1	mechanika
nestanoví	stanovit	k5eNaPmIp3nS	stanovit
žádné	žádný	k3yNgFnPc4	žádný
obecné	obecný	k2eAgFnPc4d1	obecná
zásady	zásada	k1gFnPc4	zásada
pro	pro	k7c4	pro
nezávislé	závislý	k2eNgInPc4d1	nezávislý
zákony	zákon	k1gInPc4	zákon
silového	silový	k2eAgNnSc2d1	silové
působení	působení	k1gNnSc2	působení
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
na	na	k7c6	na
čem	co	k3yQnSc6	co
interakce	interakce	k1gFnPc4	interakce
závisí	záviset	k5eAaImIp3nS	záviset
a	a	k8xC	a
jak	jak	k6eAd1	jak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgNnSc7d1	jediné
omezením	omezení	k1gNnSc7	omezení
je	být	k5eAaImIp3nS	být
platnost	platnost	k1gFnSc4	platnost
Galileiova	Galileiův	k2eAgInSc2d1	Galileiův
principu	princip	k1gInSc2	princip
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
některé	některý	k3yIgFnPc4	některý
závislosti	závislost	k1gFnPc4	závislost
silového	silový	k2eAgNnSc2d1	silové
působení	působení	k1gNnSc2	působení
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
interagujících	interagující	k2eAgFnPc2d1	interagující
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Newton	Newton	k1gMnSc1	Newton
se	se	k3xPyFc4	se
omezil	omezit	k5eAaPmAgMnS	omezit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
silového	silový	k2eAgNnSc2d1	silové
působení	působení	k1gNnSc2	působení
(	(	kIx(	(
<g/>
pravých	pravý	k2eAgFnPc2d1	pravá
sil	síla	k1gFnPc2	síla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgMnPc2	který
stanovil	stanovit	k5eAaPmAgInS	stanovit
i	i	k9	i
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
podobu	podoba	k1gFnSc4	podoba
silového	silový	k2eAgInSc2d1	silový
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
gravitační	gravitační	k2eAgNnSc4d1	gravitační
působení	působení	k1gNnSc4	působení
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
gravitační	gravitační	k2eAgInSc1d1	gravitační
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
pružnou	pružný	k2eAgFnSc4d1	pružná
(	(	kIx(	(
<g/>
elastickou	elastický	k2eAgFnSc4d1	elastická
<g/>
)	)	kIx)	)
sílu	síla	k1gFnSc4	síla
v	v	k7c6	v
tahu	tah	k1gInSc6	tah
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
záporně	záporně	k6eAd1	záporně
vzatá	vzatý	k2eAgFnSc1d1	vzatá
přímá	přímý	k2eAgFnSc1d1	přímá
úměrnost	úměrnost	k1gFnSc1	úměrnost
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cavendish	Cavendish	k1gInSc1	Cavendish
a	a	k8xC	a
Coulomb	coulomb	k1gInSc1	coulomb
nezávisle	závisle	k6eNd1	závisle
na	na	k7c4	na
sobě	se	k3xPyFc3	se
objevili	objevit	k5eAaPmAgMnP	objevit
podobu	podoba	k1gFnSc4	podoba
silového	silový	k2eAgInSc2d1	silový
zákona	zákon	k1gInSc2	zákon
–	–	k?	–
Coulombův	Coulombův	k2eAgInSc1d1	Coulombův
zákon	zákon	k1gInSc1	zákon
–	–	k?	–
pro	pro	k7c4	pro
elektrostatické	elektrostatický	k2eAgNnSc4d1	elektrostatické
působení	působení	k1gNnSc4	působení
nábojů	náboj	k1gInPc2	náboj
(	(	kIx(	(
<g/>
i	i	k9	i
pro	pro	k7c4	pro
magnetostatické	magnetostatický	k2eAgNnSc4d1	magnetostatický
působení	působení	k1gNnSc4	působení
tzv.	tzv.	kA	tzv.
magnetických	magnetický	k2eAgInPc2d1	magnetický
množství	množství	k1gNnSc4	množství
<g/>
;	;	kIx,	;
teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
magnetické	magnetický	k2eAgNnSc1d1	magnetické
působení	působení	k1gNnSc1	působení
identifikováno	identifikován	k2eAgNnSc1d1	identifikováno
jako	jako	k8xS	jako
relativistický	relativistický	k2eAgInSc1d1	relativistický
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
vlastních	vlastní	k2eAgInPc2d1	vlastní
nosičů	nosič	k1gInPc2	nosič
<g/>
,	,	kIx,	,
s	s	k7c7	s
vírovým	vírový	k2eAgNnSc7d1	vírové
silovým	silový	k2eAgNnSc7d1	silové
polem	pole	k1gNnSc7	pole
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
výše	výše	k1gFnPc1	výše
uvedené	uvedený	k2eAgFnSc2d1	uvedená
pravé	pravý	k2eAgFnSc2d1	pravá
síly	síla	k1gFnSc2	síla
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
centrálním	centrální	k2eAgNnSc7d1	centrální
působením	působení	k1gNnSc7	působení
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
při	při	k7c6	při
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
silovém	silový	k2eAgNnSc6d1	silové
působení	působení	k1gNnSc6	působení
dvou	dva	k4xCgInPc2	dva
hmotných	hmotný	k2eAgInPc2d1	hmotný
bodů	bod	k1gInPc2	bod
je	být	k5eAaImIp3nS	být
vektorová	vektorový	k2eAgFnSc1d1	vektorová
přímka	přímka	k1gFnSc1	přímka
akce	akce	k1gFnSc1	akce
i	i	k8xC	i
reakce	reakce	k1gFnSc1	reakce
totožná	totožný	k2eAgFnSc1d1	totožná
se	s	k7c7	s
spojnicí	spojnice	k1gFnSc7	spojnice
těchto	tento	k3xDgInPc2	tento
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
(	(	kIx(	(
<g/>
pravé	pravý	k2eAgFnSc2d1	pravá
<g/>
)	)	kIx)	)
síly	síla	k1gFnSc2	síla
v	v	k7c6	v
Newtonově	Newtonův	k2eAgFnSc6d1	Newtonova
klasické	klasický	k2eAgFnSc6d1	klasická
mechanice	mechanika	k1gFnSc6	mechanika
lze	lze	k6eAd1	lze
proto	proto	k8xC	proto
shrnout	shrnout	k5eAaPmF	shrnout
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
</s>
</p>
<p>
<s>
vyjadřující	vyjadřující	k2eAgFnSc4d1	vyjadřující
míru	míra	k1gFnSc4	míra
působení	působení	k1gNnSc2	působení
hmotného	hmotný	k2eAgInSc2d1	hmotný
objektu	objekt	k1gInSc2	objekt
(	(	kIx(	(
<g/>
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
silového	silový	k2eAgNnSc2d1	silové
pole	pole	k1gNnSc2	pole
<g/>
)	)	kIx)	)
na	na	k7c4	na
jiné	jiný	k2eAgNnSc4d1	jiné
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
účinky	účinek	k1gInPc7	účinek
statickými	statický	k2eAgInPc7d1	statický
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
deformací	deformace	k1gFnPc2	deformace
tělesa	těleso	k1gNnSc2	těleso
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
dynamickými	dynamický	k2eAgInPc7d1	dynamický
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
změny	změna	k1gFnPc4	změna
pohybového	pohybový	k2eAgInSc2d1	pohybový
stavu	stav	k1gInSc2	stav
tělesa	těleso	k1gNnSc2	těleso
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
která	který	k3yQgFnSc1	který
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
<g/>
-li	i	k?	-li
(	(	kIx(	(
<g/>
v	v	k7c6	v
inerciální	inerciální	k2eAgFnSc6d1	inerciální
soustavě	soustava	k1gFnSc6	soustava
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
jediná	jediný	k2eAgFnSc1d1	jediná
na	na	k7c4	na
volnou	volný	k2eAgFnSc4d1	volná
částici	částice	k1gFnSc4	částice
(	(	kIx(	(
<g/>
hmotný	hmotný	k2eAgInSc4d1	hmotný
bod	bod	k1gInSc4	bod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
časové	časový	k2eAgFnSc3d1	časová
derivaci	derivace	k1gFnSc3	derivace
hybnosti	hybnost	k1gFnSc2	hybnost
této	tento	k3xDgFnSc2	tento
částice	částice	k1gFnSc2	částice
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
působí	působit	k5eAaImIp3nS	působit
přímo	přímo	k6eAd1	přímo
(	(	kIx(	(
<g/>
centrálně	centrálně	k6eAd1	centrálně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
,	,	kIx,	,
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
silách	síla	k1gFnPc6	síla
a	a	k8xC	a
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
stejně	stejně	k6eAd1	stejně
velkou	velká	k1gFnSc7	velká
opačně	opačně	k6eAd1	opačně
orientovanou	orientovaný	k2eAgFnSc7d1	orientovaná
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
těleso	těleso	k1gNnSc1	těleso
podrobené	podrobený	k2eAgFnSc3d1	podrobená
síle	síla	k1gFnSc3	síla
zpětně	zpětně	k6eAd1	zpětně
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
daný	daný	k2eAgInSc4d1	daný
hmotný	hmotný	k2eAgInSc4d1	hmotný
objekt	objekt	k1gInSc4	objekt
<g/>
.	.	kIx.	.
<g/>
Newtonovo	Newtonův	k2eAgNnSc4d1	Newtonovo
zavedení	zavedení	k1gNnSc4	zavedení
síly	síla	k1gFnSc2	síla
nelze	lze	k6eNd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
definici	definice	k1gFnSc4	definice
v	v	k7c6	v
matematickém	matematický	k2eAgInSc6d1	matematický
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
Newtonovy	Newtonův	k2eAgInPc1d1	Newtonův
pohybové	pohybový	k2eAgInPc1d1	pohybový
zákony	zákon	k1gInPc1	zákon
totiž	totiž	k9	totiž
současně	současně	k6eAd1	současně
zavádějí	zavádět	k5eAaImIp3nP	zavádět
pojmy	pojem	k1gInPc1	pojem
hybnost	hybnost	k1gFnSc1	hybnost
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
síla	síla	k1gFnSc1	síla
a	a	k8xC	a
implicitně	implicitně	k6eAd1	implicitně
také	také	k9	také
inerciální	inerciální	k2eAgFnSc1d1	inerciální
soustava	soustava	k1gFnSc1	soustava
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
stanoví	stanovit	k5eAaPmIp3nS	stanovit
jejich	jejich	k3xOp3gInPc4	jejich
vzájemné	vzájemný	k2eAgInPc4d1	vzájemný
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Připomínají	připomínat	k5eAaImIp3nP	připomínat
tak	tak	k9	tak
"	"	kIx"	"
<g/>
definici	definice	k1gFnSc4	definice
kruhem	kruh	k1gInSc7	kruh
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
uvažovat	uvažovat	k5eAaImF	uvažovat
mnoho	mnoho	k4c4	mnoho
předpokladů	předpoklad	k1gInPc2	předpoklad
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
intuitivních	intuitivní	k2eAgFnPc2d1	intuitivní
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
formulovaných	formulovaný	k2eAgFnPc2d1	formulovaná
či	či	k8xC	či
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
Newtonově	Newtonův	k2eAgNnSc6d1	Newtonovo
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
ukázaly	ukázat	k5eAaPmAgInP	ukázat
rozbory	rozbor	k1gInPc4	rozbor
fyziků	fyzik	k1gMnPc2	fyzik
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
např.	např.	kA	např.
od	od	k7c2	od
Ernsta	Ernst	k1gMnSc4	Ernst
Macha	Mach	k1gMnSc2	Mach
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
důsledné	důsledný	k2eAgNnSc4d1	důsledné
logické	logický	k2eAgNnSc4d1	logické
zavedení	zavedení	k1gNnSc4	zavedení
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
v	v	k7c6	v
Newtonově	Newtonův	k2eAgMnSc6d1	Newtonův
duchu	duch	k1gMnSc6	duch
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
P.	P.	kA	P.
W.	W.	kA	W.
Bridgman	Bridgman	k1gMnSc1	Bridgman
<g/>
,	,	kIx,	,
intuitivní	intuitivní	k2eAgInPc1d1	intuitivní
předpoklady	předpoklad	k1gInPc1	předpoklad
však	však	k9	však
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
případě	případ	k1gInSc6	případ
nutné	nutný	k2eAgNnSc1d1	nutné
pro	pro	k7c4	pro
směrnici	směrnice	k1gFnSc4	směrnice
působící	působící	k2eAgFnSc2d1	působící
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Plný	plný	k2eAgInSc4d1	plný
výčet	výčet	k1gInSc4	výčet
nutných	nutný	k2eAgInPc2d1	nutný
předpokladů	předpoklad	k1gInPc2	předpoklad
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
např.	např.	kA	např.
v	v	k7c6	v
axiomatické	axiomatický	k2eAgFnSc6d1	axiomatická
formulaci	formulace	k1gFnSc6	formulace
Madelunga	Madelung	k1gMnSc2	Madelung
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
<g/>
,	,	kIx,	,
matematicky	matematicky	k6eAd1	matematicky
formálnějších	formální	k2eAgInPc6d2	formálnější
přístupech	přístup	k1gInPc6	přístup
k	k	k7c3	k
axiomatickému	axiomatický	k2eAgNnSc3d1	axiomatické
zavedení	zavedení	k1gNnSc3	zavedení
klasické	klasický	k2eAgFnSc2d1	klasická
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důkladnější	důkladný	k2eAgInSc1d2	důkladnější
rozbor	rozbor	k1gInSc1	rozbor
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
nabízí	nabízet	k5eAaImIp3nS	nabízet
několik	několik	k4yIc1	několik
učebnic	učebnice	k1gFnPc2	učebnice
<g/>
,,	,,	k?	,,
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
"	"	kIx"	"
<g/>
definici	definice	k1gFnSc4	definice
<g/>
"	"	kIx"	"
síly	síla	k1gFnPc4	síla
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pouze	pouze	k6eAd1	pouze
případ	případ	k1gInSc4	případ
konzervativního	konzervativní	k2eAgNnSc2d1	konzervativní
(	(	kIx(	(
<g/>
potenciálového	potenciálový	k2eAgNnSc2d1	potenciálové
<g/>
)	)	kIx)	)
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
již	již	k9	již
předtím	předtím	k6eAd1	předtím
definovanou	definovaný	k2eAgFnSc4d1	definovaná
potenciální	potenciální	k2eAgFnSc4d1	potenciální
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Konzervativní	konzervativní	k2eAgFnPc1d1	konzervativní
síly	síla	k1gFnPc1	síla
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xS	jako
záporný	záporný	k2eAgInSc1d1	záporný
gradient	gradient	k1gInSc1	gradient
potenciální	potenciální	k2eAgFnSc2d1	potenciální
energie	energie	k1gFnSc2	energie
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V	V	kA	V
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
∇	∇	k?	∇
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
nabla	nabla	k6eAd1	nabla
V	v	k7c6	v
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Zde	zde	k6eAd1	zde
i	i	k9	i
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
vztazích	vztah	k1gInPc6	vztah
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
symbolika	symbolika	k1gFnSc1	symbolika
běžná	běžný	k2eAgFnSc1d1	běžná
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
doporučovaná	doporučovaný	k2eAgNnPc4d1	doporučované
označení	označení	k1gNnPc4	označení
pro	pro	k7c4	pro
energii	energie	k1gFnSc4	energie
Ek	Ek	k1gFnSc2	Ek
<g/>
,	,	kIx,	,
Ep	Ep	k1gFnSc2	Ep
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
indexy	index	k1gInPc1	index
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
plést	plést	k5eAaImF	plést
s	s	k7c7	s
indexy	index	k1gInPc7	index
číslujícími	číslující	k2eAgInPc7d1	číslující
(	(	kIx(	(
<g/>
zobecněné	zobecněný	k2eAgFnPc4d1	zobecněná
<g/>
)	)	kIx)	)
souřadnice	souřadnice	k1gFnPc4	souřadnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Síla	síla	k1gFnSc1	síla
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
mechanice	mechanika	k1gFnSc6	mechanika
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
mechanice	mechanika	k1gFnSc6	mechanika
se	se	k3xPyFc4	se
za	za	k7c4	za
výchozí	výchozí	k2eAgFnSc4d1	výchozí
veličinu	veličina	k1gFnSc4	veličina
zpravidla	zpravidla	k6eAd1	zpravidla
bere	brát	k5eAaImIp3nS	brát
jistá	jistý	k2eAgFnSc1d1	jistá
skalární	skalární	k2eAgFnSc1d1	skalární
veličina	veličina	k1gFnSc1	veličina
(	(	kIx(	(
<g/>
obecně	obecně	k6eAd1	obecně
zvaná	zvaný	k2eAgFnSc1d1	zvaná
též	též	k6eAd1	též
"	"	kIx"	"
<g/>
kinetický	kinetický	k2eAgInSc1d1	kinetický
potenciál	potenciál	k1gInSc1	potenciál
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
základní	základní	k2eAgInSc1d1	základní
zákon	zákon	k1gInSc1	zákon
<g/>
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
mechaniky	mechanika	k1gFnPc1	mechanika
jsou	být	k5eAaImIp3nP	být
pomocí	pomocí	k7c2	pomocí
této	tento	k3xDgFnSc2	tento
veličiny	veličina	k1gFnSc2	veličina
formulovány	formulován	k2eAgInPc1d1	formulován
jako	jako	k8xC	jako
diferenciální	diferenciální	k2eAgInPc1d1	diferenciální
<g/>
,	,	kIx,	,
integrální	integrální	k2eAgInPc1d1	integrální
či	či	k8xC	či
variační	variační	k2eAgInPc1d1	variační
principy	princip	k1gInPc1	princip
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
veličinou	veličina	k1gFnSc7	veličina
bývá	bývat	k5eAaImIp3nS	bývat
např.	např.	kA	např.
kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
potenciální	potenciální	k2eAgMnPc4d1	potenciální
energie	energie	k1gFnSc2	energie
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V	V	kA	V
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
Lagrangeova	Lagrangeův	k2eAgFnSc1d1	Lagrangeova
funkce	funkce	k1gFnSc1	funkce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
Hamiltonova	Hamiltonův	k2eAgFnSc1d1	Hamiltonova
funkce	funkce	k1gFnSc1	funkce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H	H	kA	H
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Pomocí	pomocí	k7c2	pomocí
těchto	tento	k3xDgFnPc2	tento
funkcí	funkce	k1gFnPc2	funkce
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
pohybové	pohybový	k2eAgFnPc4d1	pohybová
rovnice	rovnice	k1gFnPc4	rovnice
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
i	i	k9	i
síly	síla	k1gFnSc2	síla
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c6	na
některé	některý	k3yIgFnSc6	některý
obecné	obecná	k1gFnSc6	obecná
třídy	třída	k1gFnSc2	třída
disipativních	disipativní	k2eAgFnPc2d1	disipativní
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
reakční	reakční	k2eAgFnPc1d1	reakční
síly	síla	k1gFnPc1	síla
neholonomních	holonomní	k2eNgFnPc2d1	holonomní
vazeb	vazba	k1gFnPc2	vazba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
navíc	navíc	k6eAd1	navíc
obecněji	obecně	k6eAd2	obecně
než	než	k8xS	než
u	u	k7c2	u
vektorové	vektorový	k2eAgFnSc2d1	vektorová
mechaniky	mechanika	k1gFnSc2	mechanika
–	–	k?	–
zobecněné	zobecněný	k2eAgFnPc1d1	zobecněná
síly	síla	k1gFnPc1	síla
nemusí	muset	k5eNaImIp3nP	muset
odpovídat	odpovídat	k5eAaImF	odpovídat
pouze	pouze	k6eAd1	pouze
klasické	klasický	k2eAgFnSc3d1	klasická
souřadnici	souřadnice	k1gFnSc3	souřadnice
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
ale	ale	k8xC	ale
libovolné	libovolný	k2eAgFnSc6d1	libovolná
zobecněné	zobecněný	k2eAgFnSc6d1	zobecněná
souřadnici	souřadnice	k1gFnSc6	souřadnice
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
a	a	k8xC	a
nemusí	muset	k5eNaImIp3nS	muset
mít	mít	k5eAaImF	mít
rozměr	rozměr	k1gInSc4	rozměr
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Lagrangeově	Lagrangeův	k2eAgInSc6d1	Lagrangeův
zápisu	zápis	k1gInSc6	zápis
tak	tak	k6eAd1	tak
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
zobecněnou	zobecněný	k2eAgFnSc4d1	zobecněná
sílu	síla	k1gFnSc4	síla
vztah	vztah	k1gInSc1	vztah
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
<s>
̇	̇	k?	̇
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Q_	Q_	k1gFnSc7	Q_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
T	T	kA	T
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
dot	dot	k?	dot
{	{	kIx(	{
<g/>
q	q	k?	q
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
T	T	kA	T
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
<g/>
Oddělíme	oddělit	k5eAaPmIp1nP	oddělit
<g/>
-li	i	k?	-li
nyní	nyní	k6eAd1	nyní
(	(	kIx(	(
<g/>
disipativní	disipativní	k2eAgFnSc4d1	disipativní
<g/>
)	)	kIx)	)
část	část	k1gFnSc4	část
zobecněné	zobecněný	k2eAgFnSc2d1	zobecněná
síly	síla	k1gFnSc2	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Q	Q	kA	Q
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nelze	lze	k6eNd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xC	jako
derivaci	derivace	k1gFnSc4	derivace
zobecněné	zobecněný	k2eAgFnSc2d1	zobecněná
potenciální	potenciální	k2eAgFnSc2d1	potenciální
energie	energie	k1gFnSc2	energie
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V	v	k7c6	v
<g/>
'	'	kIx"	'
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
stanovit	stanovit	k5eAaPmF	stanovit
empiricky	empiricky	k6eAd1	empiricky
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
<s>
̇	̇	k?	̇
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Q_	Q_	k1gFnSc7	Q_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
Q	Q	kA	Q
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
V	V	kA	V
<g/>
'	'	kIx"	'
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
q_	q_	k?	q_
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}}	}}}	k?	}}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
V	V	kA	V
<g/>
'	'	kIx"	'
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
dot	dot	k?	dot
{	{	kIx(	{
<g/>
q	q	k?	q
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
lze	lze	k6eAd1	lze
pohybové	pohybový	k2eAgFnPc4d1	pohybová
rovnice	rovnice	k1gFnPc4	rovnice
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
pomocí	pomocí	k7c2	pomocí
Lagrangeovy	Lagrangeův	k2eAgFnSc2d1	Lagrangeova
funkce	funkce	k1gFnSc2	funkce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
=	=	kIx~	=
<g/>
T-V	T-V	k1gMnSc1	T-V
<g/>
'	'	kIx"	'
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
<s>
̇	̇	k?	̇
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
partial	partial	k1gMnSc1	partial
L	L	kA	L
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
dot	dot	k?	dot
{	{	kIx(	{
<g/>
q	q	k?	q
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
L	L	kA	L
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
Q	Q	kA	Q
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
<g/>
V	v	k7c6	v
Hamiltonově	Hamiltonův	k2eAgInSc6d1	Hamiltonův
zápisu	zápis	k1gInSc6	zápis
mají	mít	k5eAaImIp3nP	mít
pohybové	pohybový	k2eAgFnPc1d1	pohybová
rovnice	rovnice	k1gFnPc1	rovnice
tvar	tvar	k1gInSc4	tvar
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
H	H	kA	H
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}}	}}}	k?	}}}
<g/>
+	+	kIx~	+
<g/>
Q	Q	kA	Q
<g/>
'	'	kIx"	'
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
přičemž	přičemž	k6eAd1	přičemž
pravou	pravý	k2eAgFnSc4d1	pravá
stranu	strana	k1gFnSc4	strana
můžeme	moct	k5eAaImIp1nP	moct
ztotožnit	ztotožnit	k5eAaPmF	ztotožnit
se	s	k7c7	s
zobecněnými	zobecněný	k2eAgFnPc7d1	zobecněná
silami	síla	k1gFnPc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Hamiltonova	Hamiltonův	k2eAgFnSc1d1	Hamiltonova
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
definována	definovat	k5eAaBmNgFnS	definovat
vztahem	vztah	k1gInSc7	vztah
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
<s>
̇	̇	k?	̇
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H	H	kA	H
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
dot	dot	k?	dot
{	{	kIx(	{
<g/>
q	q	k?	q
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
-L	-L	k?	-L
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
zobecněná	zobecněný	k2eAgFnSc1d1	zobecněná
hybnost	hybnost	k1gFnSc1	hybnost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
<s>
̇	̇	k?	̇
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
L	L	kA	L
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
dot	dot	k?	dot
{	{	kIx(	{
<g/>
q	q	k?	q
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
===	===	k?	===
Síla	síla	k1gFnSc1	síla
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
===	===	k?	===
</s>
</p>
<p>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
opouští	opouštět	k5eAaImIp3nS	opouštět
centrální	centrální	k2eAgNnSc4d1	centrální
působení	působení	k1gNnSc4	působení
a	a	k8xC	a
zákon	zákon	k1gInSc4	zákon
akce	akce	k1gFnSc2	akce
a	a	k8xC	a
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zavádí	zavádět	k5eAaImIp3nS	zavádět
konečnou	konečný	k2eAgFnSc4d1	konečná
rychlost	rychlost	k1gFnSc4	rychlost
šíření	šíření	k1gNnSc2	šíření
interakce	interakce	k1gFnSc2	interakce
<g/>
,	,	kIx,	,
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
však	však	k9	však
rovnost	rovnost	k1gFnSc1	rovnost
síly	síla	k1gFnSc2	síla
s	s	k7c7	s
časovou	časový	k2eAgFnSc7d1	časová
změnou	změna	k1gFnSc7	změna
hybnosti	hybnost	k1gFnSc2	hybnost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
souřadné	souřadný	k2eAgFnSc2d1	souřadná
soustavy	soustava	k1gFnSc2	soustava
závisí	záviset	k5eAaImIp3nS	záviset
jak	jak	k6eAd1	jak
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
hmotnost	hmotnost	k1gFnSc1	hmotnost
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
tedy	tedy	k9	tedy
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}}}	}}}	k?	}}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
<g/>
Pohybová	pohybový	k2eAgFnSc1d1	pohybová
rovnice	rovnice	k1gFnSc1	rovnice
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m	m	kA	m
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}}}	}}}	k?	}}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}}}	}}}	k?	}}}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
tělesa	těleso	k1gNnSc2	těleso
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
rychlosti	rychlost	k1gFnSc2	rychlost
tedy	tedy	k9	tedy
obecně	obecně	k6eAd1	obecně
nemá	mít	k5eNaImIp3nS	mít
směr	směr	k1gInSc4	směr
působící	působící	k2eAgFnSc2d1	působící
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
čtyřvektorovém	čtyřvektorový	k2eAgInSc6d1	čtyřvektorový
formalismu	formalismus	k1gInSc6	formalismus
typu	typ	k1gInSc2	typ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
;	;	kIx,	;
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
ct	ct	k?	ct
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
síle	síla	k1gFnSc3	síla
čtyřvektor	čtyřvektor	k1gInSc4	čtyřvektor
síly	síla	k1gFnSc2	síla
(	(	kIx(	(
<g/>
čtyřvektorové	čtyřvektorový	k2eAgInPc1d1	čtyřvektorový
indexy	index	k1gInPc1	index
značeny	značen	k2eAgInPc1d1	značen
řeckými	řecký	k2eAgNnPc7d1	řecké
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
τ	τ	k?	τ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
τ	τ	k?	τ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnPc6	F_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
P_	P_	k1gMnSc1	P_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
tau	tau	k1gNnSc1	tau
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
U_	U_	k1gFnSc1	U_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc1	tau
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P_	P_	k1gMnPc6	P_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
čtyřvektor	čtyřvektor	k1gInSc4	čtyřvektor
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
;	;	kIx,	;
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
U_	U_	k1gMnPc6	U_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}	}}}}}	k?	}}}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}	}}}}}	k?	}}}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
čtyřvektor	čtyřvektor	k1gInSc1	čtyřvektor
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
klidová	klidový	k2eAgFnSc1d1	klidová
hmotnost	hmotnost	k1gFnSc1	hmotnost
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
τ	τ	k?	τ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
tau	tau	k1gNnPc6	tau
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
vlastní	vlastní	k2eAgInSc4d1	vlastní
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Složky	složka	k1gFnPc1	složka
čtyřvektoru	čtyřvektor	k1gInSc2	čtyřvektor
síly	síla	k1gFnSc2	síla
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
pomocí	pomocí	k7c2	pomocí
klasických	klasický	k2eAgInPc2d1	klasický
vektorů	vektor	k1gInPc2	vektor
vztahem	vztah	k1gInSc7	vztah
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
;	;	kIx,	;
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnPc6	F_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}	}}}}}	k?	}}}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
;	;	kIx,	;
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}	}}}}}	k?	}}}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
druhý	druhý	k4xOgMnSc1	druhý
člen	člen	k1gMnSc1	člen
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
složky	složka	k1gFnSc2	složka
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
klidové	klidový	k2eAgFnSc2d1	klidová
hmotnosti	hmotnost	k1gFnSc2	hmotnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
emisí	emise	k1gFnPc2	emise
či	či	k8xC	či
absorpcí	absorpce	k1gFnSc7	absorpce
záření	záření	k1gNnSc2	záření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rovnice	rovnice	k1gFnSc1	rovnice
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
definující	definující	k2eAgFnSc4d1	definující
sílu	síla	k1gFnSc4	síla
lze	lze	k6eAd1	lze
formulovat	formulovat	k5eAaImF	formulovat
i	i	k9	i
pro	pro	k7c4	pro
neinerciální	inerciální	k2eNgFnPc4d1	neinerciální
soustavy	soustava	k1gFnPc4	soustava
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
τ	τ	k?	τ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
τ	τ	k?	τ
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
κ	κ	k?	κ
</s>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
κ	κ	k?	κ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
τ	τ	k?	τ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
τ	τ	k?	τ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
D	D	kA	D
<g/>
}	}	kIx)	}
U	u	k7c2	u
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gNnSc3	on
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc1	tau
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc1	tau
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
Gamma	Gamma	k1gFnSc1	Gamma
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gNnSc3	on
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varkappa	varkappa	k1gFnSc1	varkappa
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varkappa	varkappa	k1gFnSc1	varkappa
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc1	tau
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc1	tau
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
D	D	kA	D
značí	značit	k5eAaImIp3nS	značit
absolutní	absolutní	k2eAgFnSc4d1	absolutní
derivaci	derivace	k1gFnSc4	derivace
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Γ	Γ	k?	Γ
</s>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
κ	κ	k?	κ
</s>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Gamma	Gammum	k1gNnPc4	Gammum
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varkappa	varkappa	k1gFnSc1	varkappa
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Christoffelův	Christoffelův	k2eAgInSc1d1	Christoffelův
symbol	symbol	k1gInSc1	symbol
druhého	druhý	k4xOgInSc2	druhý
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
pohybovou	pohybový	k2eAgFnSc4d1	pohybová
rovnici	rovnice	k1gFnSc4	rovnice
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
popisuje	popisovat	k5eAaImIp3nS	popisovat
interakce	interakce	k1gFnSc1	interakce
ne	ne	k9	ne
pomocí	pomocí	k7c2	pomocí
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pomocí	pomocí	k7c2	pomocí
změny	změna	k1gFnSc2	změna
metrických	metrický	k2eAgFnPc2d1	metrická
vlastností	vlastnost	k1gFnPc2	vlastnost
časoprostoru	časoprostor	k1gInSc2	časoprostor
dané	daný	k2eAgNnSc1d1	dané
rozložením	rozložení	k1gNnSc7	rozložení
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
.	.	kIx.	.
</s>
<s>
Tělesa	těleso	k1gNnPc1	těleso
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
nejpřímějších	přímý	k2eAgFnPc6d3	nejpřímější
trajektoriích	trajektorie	k1gFnPc6	trajektorie
v	v	k7c6	v
takto	takto	k6eAd1	takto
zakřiveném	zakřivený	k2eAgInSc6d1	zakřivený
časoprostoru	časoprostor	k1gInSc6	časoprostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Síla	síla	k1gFnSc1	síla
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
teorii	teorie	k1gFnSc6	teorie
===	===	k?	===
</s>
</p>
<p>
<s>
Schrödingerova	Schrödingerův	k2eAgFnSc1d1	Schrödingerova
formulace	formulace	k1gFnSc1	formulace
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
přiřazuje	přiřazovat	k5eAaImIp3nS	přiřazovat
pozorovatelným	pozorovatelný	k2eAgFnPc3d1	pozorovatelná
veličinám	veličina	k1gFnPc3	veličina
příslušné	příslušný	k2eAgFnSc2d1	příslušná
(	(	kIx(	(
<g/>
lineární	lineární	k2eAgFnSc2d1	lineární
hermiteovské	hermiteovská	k1gFnSc2	hermiteovská
<g/>
)	)	kIx)	)
operátory	operátor	k1gInPc1	operátor
a	a	k8xC	a
stavům	stav	k1gInPc3	stav
systému	systém	k1gInSc6	systém
vektor	vektor	k1gInSc1	vektor
v	v	k7c6	v
Hilbertově	Hilbertův	k2eAgInSc6d1	Hilbertův
prostoru	prostor	k1gInSc6	prostor
(	(	kIx(	(
<g/>
v	v	k7c6	v
souřadnicové	souřadnicový	k2eAgFnSc6d1	souřadnicová
reprezentaci	reprezentace	k1gFnSc6	reprezentace
známý	známý	k1gMnSc1	známý
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
vlnová	vlnový	k2eAgFnSc1d1	vlnová
funkce	funkce	k1gFnSc1	funkce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Časovému	časový	k2eAgInSc3d1	časový
vývoji	vývoj	k1gInSc3	vývoj
podléhá	podléhat	k5eAaImIp3nS	podléhat
stavový	stavový	k2eAgInSc4d1	stavový
vektor	vektor	k1gInSc4	vektor
<g/>
,	,	kIx,	,
rovnicí	rovnice	k1gFnSc7	rovnice
časového	časový	k2eAgInSc2d1	časový
vývoje	vývoj	k1gInSc2	vývoj
je	být	k5eAaImIp3nS	být
Schrödingerova	Schrödingerův	k2eAgFnSc1d1	Schrödingerova
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
částici	částice	k1gFnSc4	částice
v	v	k7c6	v
potenciálovém	potenciálový	k2eAgNnSc6d1	potenciálové
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
pomocí	pomocí	k7c2	pomocí
Ehrenfestova	Ehrenfestův	k2eAgInSc2d1	Ehrenfestův
teorému	teorém	k1gInSc2	teorém
odvodit	odvodit	k5eAaPmF	odvodit
obdobu	obdoba	k1gFnSc4	obdoba
zákona	zákon	k1gInSc2	zákon
síly	síla	k1gFnSc2	síla
pro	pro	k7c4	pro
střední	střední	k2eAgFnPc4d1	střední
hodnoty	hodnota	k1gFnPc4	hodnota
operátorů	operátor	k1gInPc2	operátor
a	a	k8xC	a
zavést	zavést	k5eAaPmF	zavést
tak	tak	k6eAd1	tak
operátor	operátor	k1gInSc4	operátor
síly	síla	k1gFnSc2	síla
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟨	⟨	k?	⟨
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
^	^	kIx~	^
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟩	⟩	k?	⟩
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟨	⟨	k?	⟨
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
<s>
^	^	kIx~	^
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟩	⟩	k?	⟩
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
⟨	⟨	k?	⟨
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
∇	∇	k?	∇
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
⟩	⟩	k?	⟩
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
langle	langla	k1gFnSc6	langla
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k6eAd1	rangle
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
langle	langle	k6eAd1	langle
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k6eAd1	rangle
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
langle	langle	k6eAd1	langle
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
nabla	nabla	k6eAd1	nabla
V	v	k7c6	v
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k6eAd1	rangle
}	}	kIx)	}
</s>
</p>
<p>
<s>
<g/>
I	i	k9	i
hybnost	hybnost	k1gFnSc1	hybnost
lze	lze	k6eAd1	lze
popsat	popsat	k5eAaPmF	popsat
rovnicí	rovnice	k1gFnSc7	rovnice
obdobnou	obdobný	k2eAgFnSc7d1	obdobná
klasické	klasický	k2eAgFnPc1d1	klasická
definici	definice	k1gFnSc3	definice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟨	⟨	k?	⟨
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
<s>
^	^	kIx~	^
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟩	⟩	k?	⟩
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟨	⟨	k?	⟨
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
^	^	kIx~	^
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟩	⟩	k?	⟩
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟨	⟨	k?	⟨
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
^	^	kIx~	^
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟩	⟩	k?	⟩
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	left	k2eAgInSc4d1	left
<g/>
\	\	kIx~	\
<g/>
langle	langle	k1gInSc4	langle
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k1gInSc1	rangle
=	=	kIx~	=
<g/>
m	m	kA	m
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
\	\	kIx~	\
<g/>
langle	langle	k1gFnSc1	langle
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k1gInSc1	rangle
=	=	kIx~	=
<g/>
m	m	kA	m
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
left	left	k2eAgInSc4d1	left
<g/>
\	\	kIx~	\
<g/>
langle	langle	k1gInSc4	langle
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k1gInSc1	rangle
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m	m	kA	m
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc4	hmotnost
částice	částice	k1gFnSc2	částice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Časová	časový	k2eAgFnSc1d1	časová
změna	změna	k1gFnSc1	změna
střední	střední	k2eAgFnSc2d1	střední
polohy	poloha	k1gFnSc2	poloha
souřadnice	souřadnice	k1gFnSc2	souřadnice
tak	tak	k9	tak
bude	být	k5eAaImBp3nS	být
ve	v	k7c6	v
vnějším	vnější	k2eAgNnSc6d1	vnější
potenciálovém	potenciálový	k2eAgNnSc6d1	potenciálové
poli	pole	k1gNnSc6	pole
popsána	popsat	k5eAaPmNgFnS	popsat
klasickou	klasický	k2eAgFnSc7d1	klasická
mechanikou	mechanika	k1gFnSc7	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
třeba	třeba	k6eAd1	třeba
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
rovnice	rovnice	k1gFnPc1	rovnice
jsou	být	k5eAaImIp3nP	být
rovnostmi	rovnost	k1gFnPc7	rovnost
operátorů	operátor	k1gInPc2	operátor
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
středních	střední	k2eAgFnPc2d1	střední
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Chování	chování	k1gNnSc1	chování
podle	podle	k7c2	podle
kvantově	kvantově	k6eAd1	kvantově
mechanického	mechanický	k2eAgInSc2d1	mechanický
vztahu	vztah	k1gInSc2	vztah
bude	být	k5eAaImBp3nS	být
blízké	blízký	k2eAgNnSc1d1	blízké
klasickému	klasický	k2eAgNnSc3d1	klasické
chování	chování	k1gNnSc3	chování
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
bude	být	k5eAaImBp3nS	být
<g/>
-li	i	k?	-li
částici	částice	k1gFnSc6	částice
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
"	"	kIx"	"
<g/>
úzký	úzký	k2eAgInSc4d1	úzký
<g/>
"	"	kIx"	"
vlnový	vlnový	k2eAgInSc4d1	vlnový
balík	balík	k1gInSc4	balík
(	(	kIx(	(
<g/>
velké	velký	k2eAgFnSc2d1	velká
hybnosti	hybnost	k1gFnSc2	hybnost
částice	částice	k1gFnSc2	částice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Časovým	časový	k2eAgInSc7d1	časový
vývojem	vývoj	k1gInSc7	vývoj
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
vlnový	vlnový	k2eAgInSc1d1	vlnový
balík	balík	k1gInSc1	balík
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
stacionárních	stacionární	k2eAgInPc2d1	stacionární
vázaných	vázaný	k2eAgInPc2d1	vázaný
stavů	stav	k1gInPc2	stav
<g/>
)	)	kIx)	)
postupně	postupně	k6eAd1	postupně
rozplývá	rozplývat	k5eAaImIp3nS	rozplývat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
takové	takový	k3xDgNnSc4	takový
klasické	klasický	k2eAgNnSc4d1	klasické
chování	chování	k1gNnSc4	chování
je	být	k5eAaImIp3nS	být
dobrou	dobrý	k2eAgFnSc7d1	dobrá
aproximací	aproximace	k1gFnSc7	aproximace
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
krátké	krátký	k2eAgInPc4d1	krátký
časové	časový	k2eAgInPc4d1	časový
intervaly	interval	k1gInPc4	interval
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uvedené	uvedený	k2eAgInPc1d1	uvedený
vztahy	vztah	k1gInPc1	vztah
jsou	být	k5eAaImIp3nP	být
příkladem	příklad	k1gInSc7	příklad
obecnějšího	obecní	k2eAgInSc2d2	obecní
principu	princip	k1gInSc2	princip
korespondence	korespondence	k1gFnSc2	korespondence
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgInSc2	který
lze	lze	k6eAd1	lze
operátory	operátor	k1gInPc1	operátor
pozorovatelných	pozorovatelný	k2eAgFnPc2d1	pozorovatelná
veličin	veličina	k1gFnPc2	veličina
zavést	zavést	k5eAaPmF	zavést
z	z	k7c2	z
operátorů	operátor	k1gMnPc2	operátor
dvou	dva	k4xCgFnPc2	dva
základních	základní	k2eAgFnPc2d1	základní
kanonických	kanonický	k2eAgFnPc2d1	kanonická
veličin	veličina	k1gFnPc2	veličina
–	–	k?	–
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
hybnosti	hybnost	k1gFnSc2	hybnost
–	–	k?	–
stejnými	stejný	k2eAgInPc7d1	stejný
vztahy	vztah	k1gInPc7	vztah
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
pole	pole	k1gNnSc2	pole
neřeší	řešit	k5eNaImIp3nS	řešit
míru	míra	k1gFnSc4	míra
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
působení	působení	k1gNnSc2	působení
pomocí	pomocí	k7c2	pomocí
pojmu	pojem	k1gInSc2	pojem
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
metody	metoda	k1gFnSc2	metoda
kanonického	kanonický	k2eAgNnSc2d1	kanonické
kvantování	kvantování	k1gNnSc2	kvantování
polí	pole	k1gFnPc2	pole
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
teorie	teorie	k1gFnSc1	teorie
kalibračních	kalibrační	k2eAgFnPc2d1	kalibrační
polí	pole	k1gFnPc2	pole
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
lze	lze	k6eAd1	lze
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
působení	působení	k1gNnSc1	působení
(	(	kIx(	(
<g/>
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
i	i	k8xC	i
slabé	slabý	k2eAgNnSc1d1	slabé
a	a	k8xC	a
silné	silný	k2eAgNnSc1d1	silné
jaderné	jaderný	k2eAgNnSc1d1	jaderné
<g/>
)	)	kIx)	)
popsat	popsat	k5eAaPmF	popsat
pomocí	pomocí	k7c2	pomocí
kreací	kreace	k1gFnPc2	kreace
a	a	k8xC	a
anihilací	anihilace	k1gFnPc2	anihilace
virtuálních	virtuální	k2eAgFnPc2d1	virtuální
intermediálních	intermediální	k2eAgFnPc2d1	intermediální
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
znázornit	znázornit	k5eAaPmF	znázornit
Feynmanovými	Feynmanův	k2eAgInPc7d1	Feynmanův
diagramy	diagram	k1gInPc7	diagram
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristikou	charakteristika	k1gFnSc7	charakteristika
síly	síla	k1gFnSc2	síla
interakce	interakce	k1gFnSc2	interakce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
i	i	k9	i
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
-tého	éze	k6eAd1	-téze
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
příslušný	příslušný	k2eAgInSc1d1	příslušný
"	"	kIx"	"
<g/>
náboj	náboj	k1gInSc1	náboj
<g/>
"	"	kIx"	"
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
značený	značený	k2eAgInSc1d1	značený
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
silnou	silný	k2eAgFnSc4d1	silná
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
'	'	kIx"	'
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
elektroslabou	elektroslabý	k2eAgFnSc4d1	elektroslabá
resp.	resp.	kA	resp.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
g	g	kA	g
<g/>
'	'	kIx"	'
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gMnSc1	sqrt
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
g	g	kA	g
<g/>
'	'	kIx"	'
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
elementární	elementární	k2eAgInSc4d1	elementární
náboj	náboj	k1gInSc4	náboj
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
elektromagnetickou	elektromagnetický	k2eAgFnSc4d1	elektromagnetická
interakci	interakce	k1gFnSc4	interakce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
tzv.	tzv.	kA	tzv.
vazbová	vazbový	k2eAgFnSc1d1	vazbová
konstanta	konstanta	k1gFnSc1	konstanta
interakce	interakce	k1gFnSc1	interakce
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
g	g	kA	g
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ħ	ħ	k?	ħ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
elektromagnetickou	elektromagnetický	k2eAgFnSc4d1	elektromagnetická
interakci	interakce	k1gFnSc4	interakce
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
konstanta	konstanta	k1gFnSc1	konstanta
jemné	jemný	k2eAgFnSc2d1	jemná
struktury	struktura	k1gFnSc2	struktura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
přináší	přinášet	k5eAaImIp3nS	přinášet
i	i	k9	i
nový	nový	k2eAgInSc4d1	nový
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
vakuum	vakuum	k1gNnSc4	vakuum
jako	jako	k8xS	jako
prostředí	prostředí	k1gNnSc4	prostředí
neustále	neustále	k6eAd1	neustále
vznikajících	vznikající	k2eAgInPc2d1	vznikající
a	a	k8xC	a
zanikajících	zanikající	k2eAgInPc2d1	zanikající
párů	pár	k1gInPc2	pár
částice	částice	k1gFnSc2	částice
<g/>
–	–	k?	–
<g/>
antičástice	antičástice	k1gFnPc1	antičástice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
novým	nový	k2eAgInPc3d1	nový
makroskopickým	makroskopický	k2eAgInPc3d1	makroskopický
silovým	silový	k2eAgInPc3d1	silový
projevům	projev	k1gInPc3	projev
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
experimentálně	experimentálně	k6eAd1	experimentálně
prokázaná	prokázaný	k2eAgFnSc1d1	prokázaná
Casimirova	Casimirův	k2eAgFnSc1d1	Casimirova
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
např.	např.	kA	např.
jako	jako	k8xC	jako
přitažlivá	přitažlivý	k2eAgFnSc1d1	přitažlivá
síla	síla	k1gFnSc1	síla
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
blízkými	blízký	k2eAgFnPc7d1	blízká
rovnoběžnými	rovnoběžný	k2eAgFnPc7d1	rovnoběžná
kovovými	kovový	k2eAgFnPc7d1	kovová
deskami	deska	k1gFnPc7	deska
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
nabité	nabitý	k2eAgFnPc1d1	nabitá
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
síla	síla	k1gFnSc1	síla
vzniká	vznikat	k5eAaImIp3nS	vznikat
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
reálné	reálný	k2eAgFnSc2d1	reálná
tekutiny	tekutina	k1gFnSc2	tekutina
mezi	mezi	k7c7	mezi
deskami	deska	k1gFnPc7	deska
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
přídavná	přídavný	k2eAgFnSc1d1	přídavná
síla	síla	k1gFnSc1	síla
k	k	k7c3	k
mezimolekulovým	mezimolekulův	k2eAgFnPc3d1	mezimolekulův
silám	síla	k1gFnPc3	síla
a	a	k8xC	a
tlakové	tlakový	k2eAgFnSc3d1	tlaková
síle	síla	k1gFnSc3	síla
dané	daný	k2eAgFnSc3d1	daná
pohybem	pohyb	k1gInSc7	pohyb
molekul	molekula	k1gFnPc2	molekula
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
výsledný	výsledný	k2eAgInSc1d1	výsledný
efekt	efekt	k1gInSc1	efekt
též	též	k9	též
odpudivý	odpudivý	k2eAgInSc1d1	odpudivý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
sil	síla	k1gFnPc2	síla
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
základní	základní	k2eAgFnSc2d1	základní
interakce	interakce	k1gFnSc2	interakce
===	===	k?	===
</s>
</p>
<p>
<s>
Současná	současný	k2eAgFnSc1d1	současná
fyzika	fyzika	k1gFnSc1	fyzika
zná	znát	k5eAaImIp3nS	znát
4	[number]	k4	4
druhy	druh	k1gInPc1	druh
základních	základní	k2eAgFnPc2d1	základní
interakcí	interakce	k1gFnPc2	interakce
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
lze	lze	k6eAd1	lze
redukovat	redukovat	k5eAaBmF	redukovat
veškeré	veškerý	k3xTgNnSc4	veškerý
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
působení	působení	k1gNnSc4	působení
materiálních	materiální	k2eAgInPc2d1	materiální
objektů	objekt	k1gInPc2	objekt
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
gravitační	gravitační	k2eAgFnSc1d1	gravitační
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
silná	silný	k2eAgFnSc1d1	silná
(	(	kIx(	(
<g/>
též	též	k9	též
zvaná	zvaný	k2eAgFnSc1d1	zvaná
barevná	barevný	k2eAgFnSc1d1	barevná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
slabá	slabý	k2eAgFnSc1d1	slabá
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
základních	základní	k2eAgFnPc2d1	základní
interakcí	interakce	k1gFnPc2	interakce
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
jsou	být	k5eAaImIp3nP	být
dalekodosahové	dalekodosahový	k2eAgInPc1d1	dalekodosahový
a	a	k8xC	a
projevují	projevovat	k5eAaImIp3nP	projevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
makroskopických	makroskopický	k2eAgInPc6d1	makroskopický
(	(	kIx(	(
<g/>
nekvantových	kvantový	k2eNgInPc6d1	nekvantový
<g/>
)	)	kIx)	)
měřítcích	měřítek	k1gInPc6	měřítek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
má	mít	k5eAaImIp3nS	mít
pojem	pojem	k1gInSc1	pojem
síly	síla	k1gFnSc2	síla
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
gravitace	gravitace	k1gFnSc1	gravitace
a	a	k8xC	a
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
působení	působení	k1gNnSc1	působení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
zodpovědné	zodpovědný	k2eAgNnSc1d1	zodpovědné
za	za	k7c4	za
všechny	všechen	k3xTgInPc4	všechen
ostatní	ostatní	k2eAgInPc4d1	ostatní
makroskopické	makroskopický	k2eAgInPc4d1	makroskopický
silové	silový	k2eAgInPc4d1	silový
projevy	projev	k1gInPc4	projev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
působení	působení	k1gNnSc2	působení
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
klasickém	klasický	k2eAgNnSc6d1	klasické
pojetí	pojetí	k1gNnSc6	pojetí
síly	síla	k1gFnSc2	síla
se	se	k3xPyFc4	se
silové	silový	k2eAgNnSc1d1	silové
působení	působení	k1gNnSc1	působení
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
buď	buď	k8xC	buď
přímým	přímý	k2eAgInSc7d1	přímý
stykem	styk	k1gInSc7	styk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
silovým	silový	k2eAgNnSc7d1	silové
polem	pole	k1gNnSc7	pole
"	"	kIx"	"
<g/>
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přímý	přímý	k2eAgInSc1d1	přímý
styk	styk	k1gInSc1	styk
nastává	nastávat	k5eAaImIp3nS	nastávat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
působící	působící	k2eAgNnPc1d1	působící
tělesa	těleso	k1gNnPc1	těleso
vzájemně	vzájemně	k6eAd1	vzájemně
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tlačení	tlačení	k1gNnSc1	tlačení
jednoho	jeden	k4xCgNnSc2	jeden
tělesa	těleso	k1gNnSc2	těleso
druhým	druhý	k4xOgNnSc7	druhý
nebo	nebo	k8xC	nebo
odraz	odraz	k1gInSc4	odraz
jednoho	jeden	k4xCgNnSc2	jeden
tělesa	těleso	k1gNnSc2	těleso
od	od	k7c2	od
druhého	druhý	k4xOgNnSc2	druhý
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
<g/>
"	"	kIx"	"
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
tělesa	těleso	k1gNnPc1	těleso
působí	působit	k5eAaImIp3nP	působit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
silového	silový	k2eAgNnSc2d1	silové
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
tělesa	těleso	k1gNnSc2	těleso
se	se	k3xPyFc4	se
nedotýkají	dotýkat	k5eNaImIp3nP	dotýkat
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
silové	silový	k2eAgNnSc4d1	silové
působení	působení	k1gNnSc4	působení
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgNnPc7	dva
magnety	magneto	k1gNnPc7	magneto
nebo	nebo	k8xC	nebo
gravitační	gravitační	k2eAgNnSc1d1	gravitační
přitahování	přitahování	k1gNnSc1	přitahování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
i	i	k9	i
působení	působení	k1gNnSc1	působení
přímým	přímý	k2eAgInSc7d1	přímý
dotykem	dotyk	k1gInSc7	dotyk
případem	případ	k1gInSc7	případ
působení	působení	k1gNnSc2	působení
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
tvořících	tvořící	k2eAgFnPc2d1	tvořící
strukturu	struktura	k1gFnSc4	struktura
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
pružné	pružný	k2eAgFnPc4d1	pružná
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pravá	pravý	k2eAgFnSc1d1	pravá
a	a	k8xC	a
zdánlivá	zdánlivý	k2eAgFnSc1d1	zdánlivá
síla	síla	k1gFnSc1	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
změně	změna	k1gFnSc6	změna
soustavy	soustava	k1gFnSc2	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
na	na	k7c4	na
neinerciální	inerciální	k2eNgFnSc4d1	neinerciální
vztažnou	vztažný	k2eAgFnSc4d1	vztažná
soustavu	soustava	k1gFnSc4	soustava
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
tvaru	tvar	k1gInSc2	tvar
pohybové	pohybový	k2eAgFnSc2d1	pohybová
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Formální	formální	k2eAgInSc1d1	formální
tvar	tvar	k1gInSc1	tvar
pohybových	pohybový	k2eAgFnPc2d1	pohybová
rovnic	rovnice	k1gFnPc2	rovnice
z	z	k7c2	z
inerciální	inerciální	k2eAgFnSc2d1	inerciální
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
lze	lze	k6eAd1	lze
zachovat	zachovat	k5eAaPmF	zachovat
přidáním	přidání	k1gNnSc7	přidání
nových	nový	k2eAgFnPc2d1	nová
působících	působící	k2eAgFnPc2d1	působící
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
soustavě	soustava	k1gFnSc6	soustava
dynamické	dynamický	k2eAgInPc1d1	dynamický
účinky	účinek	k1gInPc1	účinek
stejné	stejný	k2eAgFnSc2d1	stejná
jako	jako	k8xC	jako
pravé	pravý	k2eAgFnSc2d1	pravá
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
síly	síla	k1gFnSc2	síla
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
o	o	k7c4	o
tyto	tento	k3xDgFnPc4	tento
zdánlivé	zdánlivý	k2eAgFnPc4d1	zdánlivá
<g/>
,	,	kIx,	,
setrvačné	setrvačný	k2eAgFnPc4d1	setrvačná
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
síly	síla	k1gFnPc1	síla
pravé	pravý	k2eAgFnSc2d1	pravá
a	a	k8xC	a
zdánlivé	zdánlivý	k2eAgFnSc2d1	zdánlivá
(	(	kIx(	(
<g/>
setrvačné	setrvačný	k2eAgFnSc2d1	setrvačná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravé	pravý	k2eAgFnPc1d1	pravá
síly	síla	k1gFnPc1	síla
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
přímo	přímo	k6eAd1	přímo
ze	z	k7c2	z
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
působení	působení	k1gNnSc2	působení
materiálních	materiální	k2eAgInPc2d1	materiální
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zdánlivé	zdánlivý	k2eAgFnPc1d1	zdánlivá
<g/>
,	,	kIx,	,
setrvačné	setrvačný	k2eAgFnPc1d1	setrvačná
síly	síla	k1gFnPc1	síla
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
z	z	k7c2	z
volby	volba	k1gFnSc2	volba
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
zdánlivých	zdánlivý	k2eAgFnPc2d1	zdánlivá
sil	síla	k1gFnPc2	síla
jsou	být	k5eAaImIp3nP	být
odstředivá	odstředivý	k2eAgFnSc1d1	odstředivá
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
Eulerova	Eulerův	k2eAgFnSc1d1	Eulerova
síla	síla	k1gFnSc1	síla
nebo	nebo	k8xC	nebo
Coriolisova	Coriolisův	k2eAgFnSc1d1	Coriolisova
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
směru	směr	k1gInSc2	směr
působení	působení	k1gNnSc2	působení
síly	síla	k1gFnSc2	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc1	těleso
působením	působení	k1gNnSc7	působení
síly	síla	k1gFnSc2	síla
ke	k	k7c3	k
"	"	kIx"	"
<g/>
zdroji	zdroj	k1gInSc3	zdroj
síly	síla	k1gFnSc2	síla
<g/>
"	"	kIx"	"
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
nebo	nebo	k8xC	nebo
vzdaluje	vzdalovat	k5eAaImIp3nS	vzdalovat
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
síly	síla	k1gFnPc4	síla
označit	označit	k5eAaPmF	označit
jako	jako	k8xS	jako
přitažlivé	přitažlivý	k2eAgFnPc1d1	přitažlivá
nebo	nebo	k8xC	nebo
odpudivé	odpudivý	k2eAgFnPc1d1	odpudivá
síly	síla	k1gFnPc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
"	"	kIx"	"
<g/>
zdroj	zdroj	k1gInSc1	zdroj
síly	síla	k1gFnSc2	síla
<g/>
"	"	kIx"	"
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
představit	představit	k5eAaPmF	představit
například	například	k6eAd1	například
těleso	těleso	k1gNnSc1	těleso
s	s	k7c7	s
nějakým	nějaký	k3yIgInSc7	nějaký
nábojem	náboj	k1gInSc7	náboj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
dělení	dělení	k1gNnSc1	dělení
nelze	lze	k6eNd1	lze
vždy	vždy	k6eAd1	vždy
aplikovat	aplikovat	k5eAaBmF	aplikovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
často	často	k6eAd1	často
problematické	problematický	k2eAgNnSc1d1	problematické
pro	pro	k7c4	pro
vírová	vírový	k2eAgNnPc4d1	vírové
silová	silový	k2eAgNnPc4d1	silové
pole	pole	k1gNnPc4	pole
(	(	kIx(	(
<g/>
magnetické	magnetický	k2eAgFnPc1d1	magnetická
síly	síla	k1gFnPc1	síla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdánlivé	zdánlivý	k2eAgFnPc1d1	zdánlivá
síly	síla	k1gFnPc1	síla
nelze	lze	k6eNd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
ani	ani	k8xC	ani
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nemají	mít	k5eNaImIp3nP	mít
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
působení	působení	k1gNnSc6	působení
těles	těleso	k1gNnPc2	těleso
či	či	k8xC	či
polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konzervativní	konzervativní	k2eAgFnPc4d1	konzervativní
<g/>
,	,	kIx,	,
disipativní	disipativní	k2eAgFnPc4d1	disipativní
a	a	k8xC	a
gyroskopické	gyroskopický	k2eAgFnPc4d1	gyroskopická
síly	síla	k1gFnPc4	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Konzervativní	konzervativní	k2eAgNnSc1d1	konzervativní
silové	silový	k2eAgNnSc1d1	silové
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
silové	silový	k2eAgNnSc1d1	silové
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
může	moct	k5eAaImIp3nS	moct
konat	konat	k5eAaImF	konat
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
izolovaném	izolovaný	k2eAgInSc6d1	izolovaný
systému	systém	k1gInSc6	systém
na	na	k7c6	na
uzavřené	uzavřený	k2eAgFnSc6d1	uzavřená
křivce	křivka	k1gFnSc6	křivka
je	být	k5eAaImIp3nS	být
celková	celkový	k2eAgFnSc1d1	celková
vykonaná	vykonaný	k2eAgFnSc1d1	vykonaná
práce	práce	k1gFnSc1	práce
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
.	.	kIx.	.
</s>
<s>
Konzervativní	konzervativní	k2eAgFnPc1d1	konzervativní
síly	síla	k1gFnPc1	síla
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xS	jako
záporný	záporný	k2eAgInSc1d1	záporný
gradient	gradient	k1gInSc1	gradient
potenciální	potenciální	k2eAgFnSc2d1	potenciální
energie	energie	k1gFnSc2	energie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
∇	∇	k?	∇
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
nabla	nabla	k6eAd1	nabla
V	v	k7c6	v
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
též	též	k9	též
nazývají	nazývat	k5eAaImIp3nP	nazývat
potenciálové	potenciálový	k2eAgFnPc1d1	potenciálová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
konzervativní	konzervativní	k2eAgFnPc4d1	konzervativní
síly	síla	k1gFnPc4	síla
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
a	a	k8xC	a
elektrostatická	elektrostatický	k2eAgFnSc1d1	elektrostatická
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nekonzervativní	konzervativní	k2eNgFnPc1d1	nekonzervativní
síly	síla	k1gFnPc1	síla
jsou	být	k5eAaImIp3nP	být
silami	síla	k1gFnPc7	síla
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
práce	práce	k1gFnPc1	práce
na	na	k7c6	na
uzavřené	uzavřený	k2eAgFnSc6d1	uzavřená
křivce	křivka	k1gFnSc6	křivka
je	být	k5eAaImIp3nS	být
nenulová	nulový	k2eNgFnSc1d1	nenulová
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
působení	působení	k1gNnSc6	působení
tedy	tedy	k9	tedy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
"	"	kIx"	"
<g/>
rozptýlení	rozptýlení	k1gNnPc2	rozptýlení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
disipaci	disipace	k1gFnSc4	disipace
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
též	též	k9	též
nazývají	nazývat	k5eAaImIp3nP	nazývat
disipativní	disipativní	k2eAgFnPc1d1	disipativní
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
například	například	k6eAd1	například
o	o	k7c4	o
síly	síla	k1gFnPc4	síla
tření	tření	k1gNnSc2	tření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
pole	pole	k1gFnPc4	pole
nelze	lze	k6eNd1	lze
popsat	popsat	k5eAaPmF	popsat
potenciální	potenciální	k2eAgFnSc7d1	potenciální
energií	energie	k1gFnSc7	energie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nekonají	konat	k5eNaImIp3nP	konat
práci	práce	k1gFnSc4	práce
již	již	k9	již
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
podstatě	podstata	k1gFnSc3	podstata
–	–	k?	–
působí	působit	k5eAaImIp3nS	působit
totiž	totiž	k9	totiž
vždy	vždy	k6eAd1	vždy
kolmo	kolmo	k6eAd1	kolmo
ke	k	k7c3	k
směru	směr	k1gInSc3	směr
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Nedochází	docházet	k5eNaImIp3nS	docházet
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
tedy	tedy	k9	tedy
ani	ani	k8xC	ani
k	k	k7c3	k
disipaci	disipace	k1gFnSc3	disipace
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc4	takový
síly	síla	k1gFnPc4	síla
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
gyroskopické	gyroskopický	k2eAgNnSc4d1	gyroskopické
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
působení	působení	k1gNnSc1	působení
stacionárního	stacionární	k2eAgNnSc2d1	stacionární
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
na	na	k7c4	na
pohybující	pohybující	k2eAgFnSc4d1	pohybující
se	se	k3xPyFc4	se
nabitou	nabitý	k2eAgFnSc4d1	nabitá
částici	částice	k1gFnSc4	částice
(	(	kIx(	(
<g/>
magnetická	magnetický	k2eAgFnSc1d1	magnetická
část	část	k1gFnSc1	část
Lorentzovy	Lorentzův	k2eAgFnSc2d1	Lorentzova
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
zdánlivých	zdánlivý	k2eAgFnPc2d1	zdánlivá
sil	síla	k1gFnPc2	síla
pak	pak	k9	pak
Coriolisova	Coriolisův	k2eAgFnSc1d1	Coriolisova
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Měření	měření	k1gNnSc1	měření
síly	síla	k1gFnSc2	síla
==	==	k?	==
</s>
</p>
<p>
<s>
Síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
zpravidla	zpravidla	k6eAd1	zpravidla
pomocí	pomocí	k7c2	pomocí
jejich	jejich	k3xOp3gInPc2	jejich
deformačních	deformační	k2eAgInPc2d1	deformační
účinků	účinek	k1gInPc2	účinek
(	(	kIx(	(
<g/>
měření	měření	k1gNnSc1	měření
síly	síla	k1gFnSc2	síla
se	se	k3xPyFc4	se
převádí	převádět	k5eAaImIp3nS	převádět
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
výchylky	výchylka	k1gFnSc2	výchylka
-	-	kIx~	-
délky	délka	k1gFnSc2	délka
nebo	nebo	k8xC	nebo
úhlu	úhel	k1gInSc2	úhel
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vyvolanými	vyvolaný	k2eAgFnPc7d1	vyvolaná
změnami	změna	k1gFnPc7	změna
elektromagnetických	elektromagnetický	k2eAgFnPc2d1	elektromagnetická
vlastností	vlastnost	k1gFnPc2	vlastnost
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
měření	měření	k1gNnSc1	měření
síly	síla	k1gFnSc2	síla
se	se	k3xPyFc4	se
převádí	převádět	k5eAaImIp3nS	převádět
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
el.	el.	k?	el.
proudu	proud	k1gInSc2	proud
nebo	nebo	k8xC	nebo
el.	el.	k?	el.
napětí	napětí	k1gNnSc1	napětí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Mechanické	mechanický	k2eAgInPc1d1	mechanický
siloměry	siloměr	k1gInPc1	siloměr
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
pružné	pružný	k2eAgFnSc6d1	pružná
deformaci	deformace	k1gFnSc6	deformace
působením	působení	k1gNnSc7	působení
síly	síla	k1gFnSc2	síla
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
pružinový	pružinový	k2eAgInSc1d1	pružinový
siloměr	siloměr	k1gInSc1	siloměr
(	(	kIx(	(
<g/>
využívá	využívat	k5eAaImIp3nS	využívat
změnu	změna	k1gFnSc4	změna
délky	délka	k1gFnSc2	délka
při	při	k7c6	při
zatížení	zatížení	k1gNnSc6	zatížení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
torzní	torzní	k2eAgFnPc1d1	torzní
váhy	váha	k1gFnPc1	váha
(	(	kIx(	(
<g/>
využívají	využívat	k5eAaImIp3nP	využívat
změnu	změna	k1gFnSc4	změna
úhlu	úhel	k1gInSc2	úhel
při	při	k7c6	při
zatížení	zatížení	k1gNnSc6	zatížení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elektrická	elektrický	k2eAgNnPc1d1	elektrické
měření	měření	k1gNnPc1	měření
založená	založený	k2eAgNnPc1d1	založené
na	na	k7c6	na
změně	změna	k1gFnSc6	změna
elektrických	elektrický	k2eAgFnPc2d1	elektrická
vlastností	vlastnost	k1gFnPc2	vlastnost
při	při	k7c6	při
působení	působení	k1gNnSc6	působení
síly	síla	k1gFnSc2	síla
nebo	nebo	k8xC	nebo
napětí	napětí	k1gNnSc2	napětí
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
piezoelektrický	piezoelektrický	k2eAgInSc1d1	piezoelektrický
siloměr	siloměr	k1gInSc1	siloměr
(	(	kIx(	(
<g/>
využívá	využívat	k5eAaImIp3nS	využívat
piezoelektrický	piezoelektrický	k2eAgInSc1d1	piezoelektrický
jev	jev	k1gInSc1	jev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
siloměry	siloměr	k1gInPc1	siloměr
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
změně	změna	k1gFnSc6	změna
stykového	stykový	k2eAgInSc2d1	stykový
odporu	odpor	k1gInSc2	odpor
při	při	k7c6	při
zatížení	zatížení	k1gNnSc6	zatížení
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
odporové	odporový	k2eAgInPc1d1	odporový
a	a	k8xC	a
polovodičové	polovodičový	k2eAgInPc1d1	polovodičový
tenzometry	tenzometr	k1gInPc1	tenzometr
(	(	kIx(	(
<g/>
využívají	využívat	k5eAaPmIp3nP	využívat
změnu	změna	k1gFnSc4	změna
odporu	odpor	k1gInSc2	odpor
při	při	k7c6	při
deformaci	deformace	k1gFnSc6	deformace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
měřiče	měřič	k1gInPc1	měřič
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
změně	změna	k1gFnSc6	změna
indukčnosti	indukčnost	k1gFnSc2	indukčnost
nebo	nebo	k8xC	nebo
kapacity	kapacita	k1gFnSc2	kapacita
při	při	k7c6	při
změně	změna	k1gFnSc6	změna
geometrického	geometrický	k2eAgNnSc2d1	geometrické
uspořádání	uspořádání	k1gNnSc2	uspořádání
vyvolané	vyvolaný	k2eAgFnSc2d1	vyvolaná
zatížením	zatížení	k1gNnSc7	zatížení
<g/>
.	.	kIx.	.
<g/>
Nanotechnologie	nanotechnologie	k1gFnSc2	nanotechnologie
si	se	k3xPyFc3	se
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
potřebu	potřeba	k1gFnSc4	potřeba
detekce	detekce	k1gFnSc2	detekce
stále	stále	k6eAd1	stále
menších	malý	k2eAgFnPc2d2	menší
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
tak	tak	k6eAd1	tak
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
metody	metoda	k1gFnPc1	metoda
měření	měření	k1gNnSc2	měření
velmi	velmi	k6eAd1	velmi
malých	malý	k2eAgFnPc2d1	malá
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
například	například	k6eAd1	například
při	při	k7c6	při
mikroskopii	mikroskopie	k1gFnSc6	mikroskopie
atomárních	atomární	k2eAgFnPc2d1	atomární
sil	síla	k1gFnPc2	síla
(	(	kIx(	(
<g/>
AFM	AFM	kA	AFM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
piezoelektrickém	piezoelektrický	k2eAgInSc6d1	piezoelektrický
jevu	jev	k1gInSc6	jev
<g/>
,	,	kIx,	,
či	či	k8xC	či
pro	pro	k7c4	pro
optické	optický	k2eAgInPc4d1	optický
nebo	nebo	k8xC	nebo
magnetické	magnetický	k2eAgInPc4d1	magnetický
mikromanipulátory	mikromanipulátor	k1gInPc4	mikromanipulátor
(	(	kIx(	(
<g/>
optical	opticat	k5eAaPmAgMnS	opticat
tweezers	tweezers	k6eAd1	tweezers
<g/>
,	,	kIx,	,
magnetic	magnetice	k1gFnPc2	magnetice
tweeezer	tweeezra	k1gFnPc2	tweeezra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
metoda	metoda	k1gFnSc1	metoda
měření	měření	k1gNnSc2	měření
sil	síla	k1gFnPc2	síla
s	s	k7c7	s
citlivostí	citlivost	k1gFnSc7	citlivost
pod	pod	k7c4	pod
200	[number]	k4	200
fN	fN	k?	fN
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
13	[number]	k4	13
N	N	kA	N
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
převodníku	převodník	k1gInSc6	převodník
sil	síla	k1gFnPc2	síla
kvantových	kvantový	k2eAgFnPc2d1	kvantová
interakcí	interakce	k1gFnPc2	interakce
při	při	k7c6	při
průhybu	průhyb	k1gInSc6	průhyb
nanovláken	nanovláken	k1gInSc4	nanovláken
na	na	k7c4	na
optický	optický	k2eAgInSc4d1	optický
signál	signál	k1gInSc4	signál
<g/>
,	,	kIx,	,
využitelná	využitelný	k2eAgFnSc1d1	využitelná
např.	např.	kA	např.
k	k	k7c3	k
detekci	detekce	k1gFnSc3	detekce
sil	síla	k1gFnPc2	síla
infrazvukového	infrazvukový	k2eAgNnSc2d1	infrazvukový
vlnění	vlnění	k1gNnSc2	vlnění
v	v	k7c6	v
kapalinách	kapalina	k1gFnPc6	kapalina
nebo	nebo	k8xC	nebo
i	i	k9	i
v	v	k7c6	v
biologickém	biologický	k2eAgNnSc6d1	biologické
zkoumání	zkoumání	k1gNnSc6	zkoumání
k	k	k7c3	k
monitorování	monitorování	k1gNnSc3	monitorování
bakteriálního	bakteriální	k2eAgInSc2d1	bakteriální
pohybu	pohyb	k1gInSc2	pohyb
či	či	k8xC	či
tepu	tep	k1gInSc2	tep
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
srdečních	srdeční	k2eAgFnPc2d1	srdeční
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Síly	síla	k1gFnPc4	síla
působící	působící	k2eAgFnPc4d1	působící
na	na	k7c4	na
soustavu	soustava	k1gFnSc4	soustava
hmotných	hmotný	k2eAgInPc2d1	hmotný
bodů	bod	k1gInPc2	bod
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
hmotných	hmotný	k2eAgInPc2d1	hmotný
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
lépe	dobře	k6eAd2	dobře
řečeno	říct	k5eAaPmNgNnS	říct
v	v	k7c6	v
jakékoliv	jakýkoliv	k3yIgFnSc6	jakýkoliv
soustavě	soustava	k1gFnSc6	soustava
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
síly	síla	k1gFnPc1	síla
působící	působící	k2eAgFnPc1d1	působící
na	na	k7c4	na
hmotný	hmotný	k2eAgInSc4d1	hmotný
bod	bod	k1gInSc4	bod
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
vnější	vnější	k2eAgFnSc4d1	vnější
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnPc1d1	vnější
síly	síla	k1gFnPc1	síla
mají	mít	k5eAaImIp3nP	mít
zdroj	zdroj	k1gInSc4	zdroj
mimo	mimo	k7c4	mimo
soustavu	soustava	k1gFnSc4	soustava
hmotných	hmotný	k2eAgInPc2d1	hmotný
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
síly	síla	k1gFnPc1	síla
jsou	být	k5eAaImIp3nP	být
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
působí	působit	k5eAaImIp3nP	působit
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
hmotnými	hmotný	k2eAgInPc7d1	hmotný
body	bod	k1gInPc7	bod
uvnitř	uvnitř	k7c2	uvnitř
soustavy	soustava	k1gFnSc2	soustava
hmotných	hmotný	k2eAgInPc2d1	hmotný
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Působení	působení	k1gNnSc3	působení
vnější	vnější	k2eAgFnSc2d1	vnější
síly	síla	k1gFnSc2	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
působení	působení	k1gNnSc4	působení
vnější	vnější	k2eAgFnSc2d1	vnější
síly	síla	k1gFnSc2	síla
za	za	k7c4	za
následek	následek	k1gInSc4	následek
deformaci	deformace	k1gFnSc3	deformace
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
deformačním	deformační	k2eAgInSc6d1	deformační
účinku	účinek	k1gInSc6	účinek
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
stlačování	stlačování	k1gNnSc4	stlačování
gumového	gumový	k2eAgInSc2d1	gumový
míče	míč	k1gInSc2	míč
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
sice	sice	k8xC	sice
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mění	měnit	k5eAaImIp3nP	měnit
se	se	k3xPyFc4	se
jeho	on	k3xPp3gInSc4	on
objem	objem	k1gInSc4	objem
a	a	k8xC	a
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
deformuje	deformovat	k5eAaImIp3nS	deformovat
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
natahování	natahování	k1gNnSc1	natahování
nebo	nebo	k8xC	nebo
stlačování	stlačování	k1gNnSc1	stlačování
pružiny	pružina	k1gFnSc2	pružina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
také	také	k9	také
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
deformaci	deformace	k1gFnSc3	deformace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
působení	působení	k1gNnSc4	působení
vnější	vnější	k2eAgFnSc2d1	vnější
síly	síla	k1gFnSc2	síla
za	za	k7c4	za
následek	následek	k1gInSc4	následek
změnu	změna	k1gFnSc4	změna
pohybového	pohybový	k2eAgInSc2d1	pohybový
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
o	o	k7c6	o
pohybovém	pohybový	k2eAgInSc6d1	pohybový
účinku	účinek	k1gInSc6	účinek
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Udeříme	udeřit	k5eAaPmIp1nP	udeřit
<g/>
-li	i	k?	-li
například	například	k6eAd1	například
do	do	k7c2	do
nějakého	nějaký	k3yIgInSc2	nějaký
(	(	kIx(	(
<g/>
volného	volný	k2eAgInSc2d1	volný
<g/>
)	)	kIx)	)
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
těleso	těleso	k1gNnSc1	těleso
začne	začít	k5eAaPmIp3nS	začít
pohybovat	pohybovat	k5eAaImF	pohybovat
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
změnil	změnit	k5eAaPmAgInS	změnit
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc4	jeho
pohybový	pohybový	k2eAgInSc4d1	pohybový
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vnější	vnější	k2eAgFnPc1d1	vnější
síly	síla	k1gFnPc1	síla
tedy	tedy	k9	tedy
mohou	moct	k5eAaImIp3nP	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
pohyb	pohyb	k1gInSc4	pohyb
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
hmotných	hmotný	k2eAgInPc2d1	hmotný
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Působení	působení	k1gNnSc3	působení
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
síly	síla	k1gFnSc2	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
hmotných	hmotný	k2eAgInPc2d1	hmotný
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
hmotný	hmotný	k2eAgInSc4d1	hmotný
bod	bod	k1gInSc4	bod
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
na	na	k7c4	na
hmotný	hmotný	k2eAgInSc4d1	hmotný
bod	bod	k1gInSc4	bod
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
silou	síla	k1gFnSc7	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
12	[number]	k4	12
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
pak	pak	k6eAd1	pak
podle	podle	k7c2	podle
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Newtonova	Newtonův	k2eAgInSc2d1	Newtonův
pohybového	pohybový	k2eAgInSc2d1	pohybový
zákona	zákon	k1gInSc2	zákon
působí	působit	k5eAaImIp3nS	působit
také	také	k9	také
bod	bod	k1gInSc1	bod
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
na	na	k7c4	na
bod	bod	k1gInSc4	bod
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
silou	síla	k1gFnSc7	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
21	[number]	k4	21
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
21	[number]	k4	21
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
velikost	velikost	k1gFnSc4	velikost
jako	jako	k8xC	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
12	[number]	k4	12
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
ale	ale	k8xC	ale
opačný	opačný	k2eAgInSc1d1	opačný
směr	směr	k1gInSc1	směr
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
12	[number]	k4	12
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
21	[number]	k4	21
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
21	[number]	k4	21
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Vektorový	vektorový	k2eAgInSc1d1	vektorový
součet	součet	k1gInSc1	součet
těchto	tento	k3xDgFnPc2	tento
sil	síla	k1gFnPc2	síla
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nulový	nulový	k2eAgMnSc1d1	nulový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
12	[number]	k4	12
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
21	[number]	k4	21
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
21	[number]	k4	21
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
má	můj	k3xOp1gFnSc1	můj
soustava	soustava	k1gFnSc1	soustava
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dva	dva	k4xCgInPc4	dva
hmotné	hmotný	k2eAgInPc4d1	hmotný
body	bod	k1gInPc4	bod
lze	lze	k6eAd1	lze
psát	psát	k5eAaImF	psát
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
12	[number]	k4	12
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
21	[number]	k4	21
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⏟	⏟	k?	⏟
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
13	[number]	k4	13
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
31	[number]	k4	31
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⏟	⏟	k?	⏟
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
23	[number]	k4	23
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
32	[number]	k4	32
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⏟	⏟	k?	⏟
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
⋯	⋯	k?	⋯
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
underbrace	underbrace	k1gFnPc4	underbrace
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
21	[number]	k4	21
<g/>
}}	}}	k?	}}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
underbrace	underbrace	k1gFnSc1	underbrace
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
13	[number]	k4	13
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
31	[number]	k4	31
<g/>
}}	}}	k?	}}
_	_	kIx~	_
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
underbrace	underbrace	k1gFnSc1	underbrace
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
23	[number]	k4	23
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
32	[number]	k4	32
<g/>
}}	}}	k?	}}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k1gInSc1	cdots
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
V	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
hmotných	hmotný	k2eAgInPc2d1	hmotný
bodů	bod	k1gInPc2	bod
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
všechny	všechen	k3xTgFnPc1	všechen
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
síly	síla	k1gFnPc1	síla
vzájemně	vzájemně	k6eAd1	vzájemně
ruší	rušit	k5eAaImIp3nP	rušit
<g/>
.	.	kIx.	.
</s>
<s>
Výslednice	výslednice	k1gFnSc1	výslednice
všech	všecek	k3xTgFnPc2	všecek
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
sil	síla	k1gFnPc2	síla
soustavy	soustava	k1gFnSc2	soustava
hmotných	hmotný	k2eAgInPc2d1	hmotný
bodů	bod	k1gInPc2	bod
je	být	k5eAaImIp3nS	být
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgInSc1	třetí
pohybový	pohybový	k2eAgInSc1d1	pohybový
zákon	zákon	k1gInSc1	zákon
výslovně	výslovně	k6eAd1	výslovně
nemluví	mluvit	k5eNaImIp3nS	mluvit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
působící	působící	k2eAgFnPc1d1	působící
síly	síla	k1gFnPc1	síla
(	(	kIx(	(
<g/>
akce	akce	k1gFnPc1	akce
a	a	k8xC	a
reakce	reakce	k1gFnPc1	reakce
<g/>
)	)	kIx)	)
měly	mít	k5eAaImAgFnP	mít
ležet	ležet	k5eAaImF	ležet
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
přímce	přímka	k1gFnSc6	přímka
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
mají	mít	k5eAaImIp3nP	mít
opačný	opačný	k2eAgInSc4d1	opačný
směr	směr	k1gInSc4	směr
a	a	k8xC	a
stejnou	stejný	k2eAgFnSc4d1	stejná
velikost	velikost	k1gFnSc4	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
však	však	k9	však
tyto	tento	k3xDgFnPc1	tento
síly	síla	k1gFnPc1	síla
neležely	ležet	k5eNaImAgFnP	ležet
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
přímce	přímka	k1gFnSc6	přímka
<g/>
,	,	kIx,	,
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
vznik	vznik	k1gInSc1	vznik
silového	silový	k2eAgInSc2d1	silový
momentu	moment	k1gInSc2	moment
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
hmotných	hmotný	k2eAgInPc2d1	hmotný
bodů	bod	k1gInPc2	bod
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
dva	dva	k4xCgInPc4	dva
hmotné	hmotný	k2eAgInPc4d1	hmotný
body	bod	k1gInPc4	bod
soustavy	soustava	k1gFnSc2	soustava
působí	působit	k5eAaImIp3nP	působit
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
přímce	přímka	k1gFnSc6	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Silový	silový	k2eAgInSc1d1	silový
moment	moment	k1gInSc1	moment
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
hmotnými	hmotný	k2eAgInPc7d1	hmotný
body	bod	k1gInPc7	bod
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nulový	nulový	k2eAgMnSc1d1	nulový
<g/>
.	.	kIx.	.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
moment	moment	k1gInSc1	moment
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
sil	síla	k1gFnPc2	síla
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
součtem	součet	k1gInSc7	součet
momentů	moment	k1gInPc2	moment
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
hmotnými	hmotný	k2eAgInPc7d1	hmotný
body	bod	k1gInPc7	bod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
libovolnému	libovolný	k2eAgInSc3d1	libovolný
bodu	bod	k1gInSc3	bod
prostoru	prostor	k1gInSc2	prostor
nulový	nulový	k2eAgMnSc1d1	nulový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
síly	síla	k1gFnPc1	síla
tedy	tedy	k9	tedy
nezpůsobují	způsobovat	k5eNaImIp3nP	způsobovat
pohyb	pohyb	k1gInSc4	pohyb
soustavy	soustava	k1gFnSc2	soustava
jako	jako	k8xC	jako
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgFnSc1	první
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
věta	věta	k1gFnSc1	věta
impulsová	impulsový	k2eAgFnSc1d1	impulsová
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
výše	výše	k1gFnSc2	výše
uvedeného	uvedený	k2eAgInSc2d1	uvedený
rozboru	rozbor	k1gInSc2	rozbor
plynou	plynout	k5eAaImIp3nP	plynout
následující	následující	k2eAgFnPc1d1	následující
věty	věta	k1gFnPc1	věta
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Časová	časový	k2eAgFnSc1d1	časová
derivace	derivace	k1gFnSc1	derivace
celkové	celkový	k2eAgFnSc2d1	celková
hybnosti	hybnost	k1gFnSc2	hybnost
soustavy	soustava	k1gFnSc2	soustava
hmotných	hmotný	k2eAgInPc2d1	hmotný
bodů	bod	k1gInPc2	bod
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
výslednici	výslednice	k1gFnSc4	výslednice
vnějších	vnější	k2eAgFnPc2d1	vnější
sil	síla	k1gFnPc2	síla
působících	působící	k2eAgFnPc2d1	působící
na	na	k7c4	na
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
věta	věta	k1gFnSc1	věta
o	o	k7c6	o
hybnosti	hybnost	k1gFnSc6	hybnost
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
věta	věta	k1gFnSc1	věta
impulsová	impulsový	k2eAgFnSc1d1	impulsová
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Časová	časový	k2eAgFnSc1d1	časová
derivace	derivace	k1gFnSc1	derivace
celkového	celkový	k2eAgInSc2d1	celkový
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
soustavy	soustava	k1gFnSc2	soustava
hmotných	hmotný	k2eAgInPc2d1	hmotný
bodů	bod	k1gInPc2	bod
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
výslednému	výsledný	k2eAgNnSc3d1	výsledné
momentu	moment	k1gInSc3	moment
vnějších	vnější	k2eAgFnPc2d1	vnější
sil	síla	k1gFnPc2	síla
působících	působící	k2eAgFnPc2d1	působící
na	na	k7c4	na
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
počítanému	počítaný	k2eAgInSc3d1	počítaný
ke	k	k7c3	k
stejnému	stejný	k2eAgInSc3d1	stejný
bodu	bod	k1gInSc3	bod
jako	jako	k8xS	jako
celkový	celkový	k2eAgInSc4d1	celkový
moment	moment	k1gInSc4	moment
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
věta	věta	k1gFnSc1	věta
o	o	k7c6	o
momentu	moment	k1gInSc6	moment
hybnosti	hybnost	k1gFnSc2	hybnost
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
věta	věta	k1gFnSc1	věta
impulsová	impulsový	k2eAgFnSc1d1	impulsová
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Speciálním	speciální	k2eAgInSc7d1	speciální
případem	případ	k1gInSc7	případ
jsou	být	k5eAaImIp3nP	být
zákon	zákon	k1gInSc4	zákon
zachování	zachování	k1gNnSc2	zachování
hybnosti	hybnost	k1gFnSc2	hybnost
a	a	k8xC	a
zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
izolované	izolovaný	k2eAgFnSc2d1	izolovaná
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Síly	síla	k1gFnPc1	síla
působící	působící	k2eAgFnPc1d1	působící
na	na	k7c4	na
dokonale	dokonale	k6eAd1	dokonale
tuhé	tuhý	k2eAgNnSc4d1	tuhé
těleso	těleso	k1gNnSc4	těleso
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Skládání	skládání	k1gNnPc1	skládání
sil	síla	k1gFnPc2	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Skládání	skládání	k1gNnSc1	skládání
sil	síla	k1gFnPc2	síla
je	být	k5eAaImIp3nS	být
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
se	se	k3xPyFc4	se
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
sil	síla	k1gFnPc2	síla
působících	působící	k2eAgFnPc2d1	působící
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
určí	určit	k5eAaPmIp3nS	určit
výsledná	výsledný	k2eAgFnSc1d1	výsledná
síla	síla	k1gFnSc1	síla
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
výslednice	výslednice	k1gFnSc1	výslednice
sil	síla	k1gFnPc2	síla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Účinek	účinek	k1gInSc1	účinek
všech	všecek	k3xTgFnPc2	všecek
sil	síla	k1gFnPc2	síla
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xS	jako
účinek	účinek	k1gInSc1	účinek
výslednice	výslednice	k1gFnSc2	výslednice
<g/>
.	.	kIx.	.
</s>
<s>
Síly	síla	k1gFnPc1	síla
jsou	být	k5eAaImIp3nP	být
vektorové	vektorový	k2eAgFnPc1d1	vektorová
veličiny	veličina	k1gFnPc1	veličina
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
jejich	jejich	k3xOp3gFnPc6	jejich
velikostech	velikost	k1gFnPc6	velikost
a	a	k8xC	a
směrech	směr	k1gInPc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
skládání	skládání	k1gNnSc6	skládání
sil	síla	k1gFnPc2	síla
působících	působící	k2eAgFnPc2d1	působící
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
může	moct	k5eAaImIp3nS	moct
záležet	záležet	k5eAaImF	záležet
i	i	k9	i
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
síly	síla	k1gFnPc1	síla
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
působí	působit	k5eAaImIp3nP	působit
(	(	kIx(	(
<g/>
na	na	k7c6	na
působištích	působiště	k1gNnPc6	působiště
sil	síla	k1gFnPc2	síla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
působišť	působiště	k1gNnPc2	působiště
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
různé	různý	k2eAgInPc4d1	různý
otáčivé	otáčivý	k2eAgInPc4d1	otáčivý
účinky	účinek	k1gInPc4	účinek
sil	síla	k1gFnPc2	síla
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dvojice	dvojice	k1gFnSc2	dvojice
sil	síla	k1gFnPc2	síla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výslednice	výslednice	k1gFnSc1	výslednice
sil	síla	k1gFnPc2	síla
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
vektorovému	vektorový	k2eAgInSc3d1	vektorový
součtu	součet	k1gInSc2	součet
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
⋯	⋯	k?	⋯
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k6eAd1	cdots
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
z	z	k7c2	z
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
síly	síla	k1gFnPc1	síla
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
neovlivňují	ovlivňovat	k5eNaImIp3nP	ovlivňovat
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
platí	platit	k5eAaImIp3nS	platit
princip	princip	k1gInSc4	princip
superpozice	superpozice	k1gFnSc2	superpozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Speciální	speciální	k2eAgInPc1d1	speciální
případy	případ	k1gInPc1	případ
====	====	k?	====
</s>
</p>
<p>
<s>
Při	při	k7c6	při
skládání	skládání	k1gNnSc6	skládání
sil	síla	k1gFnPc2	síla
stejného	stejný	k2eAgInSc2d1	stejný
směru	směr	k1gInSc2	směr
se	se	k3xPyFc4	se
sečtou	sečíst	k5eAaPmIp3nP	sečíst
velikosti	velikost	k1gFnSc2	velikost
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
směr	směr	k1gInSc1	směr
výslednice	výslednice	k1gFnSc2	výslednice
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xS	jako
směr	směr	k1gInSc1	směr
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
např.	např.	kA	např.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
=	=	kIx~	=
<g/>
F_	F_	k1gMnSc1	F_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
F_	F_	k1gMnSc1	F_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Při	při	k7c6	při
skládání	skládání	k1gNnSc6	skládání
sil	síla	k1gFnPc2	síla
opačného	opačný	k2eAgInSc2d1	opačný
směru	směr	k1gInSc2	směr
se	se	k3xPyFc4	se
velikosti	velikost	k1gFnSc3	velikost
opačných	opačný	k2eAgFnPc2d1	opačná
sil	síla	k1gFnPc2	síla
odečtou	odečíst	k5eAaPmIp3nP	odečíst
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
výslednice	výslednice	k1gFnSc1	výslednice
má	mít	k5eAaImIp3nS	mít
směr	směr	k1gInSc4	směr
větší	veliký	k2eAgInSc4d2	veliký
ze	z	k7c2	z
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
např.	např.	kA	např.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
=	=	kIx~	=
<g/>
|	|	kIx~	|
<g/>
F_	F_	k1gMnSc1	F_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
-F_	-F_	k?	-F_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
kolmých	kolmý	k2eAgFnPc2d1	kolmá
dvou	dva	k4xCgFnPc2	dva
sil	síla	k1gFnPc2	síla
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
velikost	velikost	k1gFnSc4	velikost
výslednice	výslednice	k1gFnSc2	výslednice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gMnSc1	sqrt
{	{	kIx(	{
<g/>
F_	F_	k1gMnSc1	F_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
F_	F_	k1gMnSc1	F_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
Při	při	k7c6	při
skládání	skládání	k1gNnSc6	skládání
dvou	dva	k4xCgFnPc2	dva
sil	síla	k1gFnPc2	síla
různého	různý	k2eAgInSc2d1	různý
směru	směr	k1gInSc2	směr
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
výsledná	výsledný	k2eAgFnSc1d1	výsledná
síla	síla	k1gFnSc1	síla
vektorovým	vektorový	k2eAgInSc7d1	vektorový
součtem	součet	k1gInSc7	součet
<g/>
,	,	kIx,	,
graficky	graficky	k6eAd1	graficky
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
určit	určit	k5eAaPmF	určit
jako	jako	k9	jako
úhlopříčka	úhlopříčka	k1gFnSc1	úhlopříčka
v	v	k7c6	v
rovnoběžníku	rovnoběžník	k1gInSc6	rovnoběžník
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
čtyřúhelníku	čtyřúhelník	k1gInSc6	čtyřúhelník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
dvě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
tvoří	tvořit	k5eAaImIp3nP	tvořit
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
síly	síla	k1gFnPc1	síla
a	a	k8xC	a
zbývající	zbývající	k2eAgFnPc1d1	zbývající
strany	strana	k1gFnPc1	strana
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
stranami	strana	k1gFnPc7	strana
rovnoběžné	rovnoběžný	k2eAgFnPc1d1	rovnoběžná
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
skládání	skládání	k1gNnSc6	skládání
sil	síla	k1gFnPc2	síla
stejného	stejný	k2eAgInSc2d1	stejný
směru	směr	k1gInSc2	směr
působících	působící	k2eAgNnPc2d1	působící
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
tuhého	tuhý	k2eAgNnSc2d1	tuhé
tělesa	těleso	k1gNnSc2	těleso
leží	ležet	k5eAaImIp3nS	ležet
působiště	působiště	k1gNnSc4	působiště
výslednice	výslednice	k1gFnSc2	výslednice
mezi	mezi	k7c7	mezi
působišti	působiště	k1gNnPc7	působiště
sil	síla	k1gFnPc2	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnSc6	F_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnSc6	F_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
od	od	k7c2	od
síly	síla	k1gFnSc2	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnSc6	F_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
F_	F_	k1gMnSc1	F_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
F_	F_	k1gMnSc1	F_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
F_	F_	k1gMnSc1	F_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r	r	kA	r
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
sil	síla	k1gFnPc2	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnSc6	F_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnSc6	F_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Při	při	k7c6	při
skládání	skládání	k1gNnSc6	skládání
sil	síla	k1gFnPc2	síla
opačného	opačný	k2eAgInSc2d1	opačný
směru	směr	k1gInSc2	směr
působících	působící	k2eAgNnPc2d1	působící
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
tuhého	tuhý	k2eAgNnSc2d1	tuhé
tělesa	těleso	k1gNnSc2	těleso
leží	ležet	k5eAaImIp3nS	ležet
působiště	působiště	k1gNnPc4	působiště
výslednice	výslednice	k1gFnSc2	výslednice
na	na	k7c6	na
přímce	přímka	k1gFnSc6	přímka
tvořené	tvořený	k2eAgFnSc2d1	tvořená
působišti	působiště	k1gNnSc3	působiště
za	za	k7c7	za
větší	veliký	k2eAgFnSc7d2	veliký
silou	síla	k1gFnSc7	síla
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
od	od	k7c2	od
síly	síla	k1gFnSc2	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnSc6	F_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
F_	F_	k1gMnSc1	F_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
F_	F_	k1gMnSc1	F_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-F_	-F_	k?	-F_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r	r	kA	r
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
sil	síla	k1gFnPc2	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnSc6	F_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnSc6	F_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Při	při	k7c6	při
skládání	skládání	k1gNnSc6	skládání
sil	síla	k1gFnPc2	síla
různého	různý	k2eAgInSc2d1	různý
směru	směr	k1gInSc2	směr
působících	působící	k2eAgNnPc2d1	působící
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
tuhého	tuhý	k2eAgNnSc2d1	tuhé
tělesa	těleso	k1gNnSc2	těleso
se	se	k3xPyFc4	se
síly	síla	k1gFnPc1	síla
posunou	posunout	k5eAaPmIp3nP	posunout
po	po	k7c6	po
vektorových	vektorový	k2eAgFnPc6d1	vektorová
přímkách	přímka	k1gFnPc6	přímka
do	do	k7c2	do
společného	společný	k2eAgNnSc2d1	společné
působiště	působiště	k1gNnSc2	působiště
<g/>
,	,	kIx,	,
složí	složit	k5eAaPmIp3nS	složit
se	se	k3xPyFc4	se
a	a	k8xC	a
výslednice	výslednice	k1gFnSc1	výslednice
se	se	k3xPyFc4	se
posune	posunout	k5eAaPmIp3nS	posunout
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
vektorové	vektorový	k2eAgFnSc6d1	vektorová
přímce	přímka	k1gFnSc6	přímka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
aby	aby	kYmCp3nS	aby
její	její	k3xOp3gNnSc1	její
působiště	působiště	k1gNnSc1	působiště
leželo	ležet	k5eAaImAgNnS	ležet
na	na	k7c6	na
spojnici	spojnice	k1gFnSc6	spojnice
původních	původní	k2eAgNnPc2d1	původní
působišť	působiště	k1gNnPc2	působiště
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
<g/>
Matematické	matematický	k2eAgNnSc1d1	matematické
řešení	řešení	k1gNnSc1	řešení
sčítání	sčítání	k1gNnSc2	sčítání
více	hodně	k6eAd2	hodně
obecných	obecný	k2eAgFnPc2d1	obecná
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
s	s	k7c7	s
různými	různý	k2eAgNnPc7d1	různé
působišti	působiště	k1gNnPc7	působiště
<g/>
:	:	kIx,	:
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
rozklad	rozklad	k1gInSc1	rozklad
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
sil	síla	k1gFnPc2	síla
do	do	k7c2	do
vzájemně	vzájemně	k6eAd1	vzájemně
kolmých	kolmý	k2eAgInPc2d1	kolmý
směrů	směr	k1gInPc2	směr
(	(	kIx(	(
<g/>
souřadnice	souřadnice	k1gFnSc1	souřadnice
"	"	kIx"	"
<g/>
x	x	k?	x
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
y	y	k?	y
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gFnSc6	F_
<g/>
{	{	kIx(	{
<g/>
xi	xi	k?	xi
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
F	F	kA	F
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
F_	F_	k1gFnSc1	F_
<g/>
{	{	kIx(	{
<g/>
yi	yi	k?	yi
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
F	F	kA	F
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
úhel	úhel	k1gInSc4	úhel
od	od	k7c2	od
kladné	kladný	k2eAgFnSc2d1	kladná
poloosy	poloosa	k1gFnSc2	poloosa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
b	b	k?	b
<g/>
)	)	kIx)	)
sečtení	sečtení	k1gNnSc1	sečtení
složek	složka	k1gFnPc2	složka
sil	síla	k1gFnPc2	síla
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
směrů	směr	k1gInPc2	směr
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gFnSc6	F_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
F_	F_	k1gFnSc1	F_
<g/>
{	{	kIx(	{
<g/>
xi	xi	k?	xi
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
F_	F_	k1gFnSc1	F_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
F_	F_	k1gFnSc1	F_
<g/>
{	{	kIx(	{
<g/>
yi	yi	k?	yi
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
<g/>
)	)	kIx)	)
výsledná	výsledný	k2eAgFnSc1d1	výsledná
velikost	velikost	k1gFnSc1	velikost
síly	síla	k1gFnSc2	síla
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gMnSc1	sqrt
{	{	kIx(	{
<g/>
F_	F_	k1gMnSc1	F_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
F_	F_	k1gFnSc1	F_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
<g/>
)	)	kIx)	)
úhel	úhel	k1gInSc1	úhel
výslednice	výslednice	k1gFnSc2	výslednice
sil	síla	k1gFnPc2	síla
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
tan	tan	k?	tan
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
tan	tan	k?	tan
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
F_	F_	k1gFnSc1	F_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
F_	F_	k1gFnSc1	F_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
pro	pro	k7c4	pro
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
<	<	kIx(	<
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gFnSc6	F_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
<	<	kIx(	<
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
tan	tan	k?	tan
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
180	[number]	k4	180
</s>
</p>
<p>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
tan	tan	k?	tan
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
F_	F_	k1gFnSc1	F_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
F_	F_	k1gFnSc1	F_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
<g/>
+	+	kIx~	+
<g/>
180	[number]	k4	180
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
o	o	k7c4	o
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
pro	pro	k7c4	pro
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gFnSc6	F_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
>	>	kIx)	>
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gFnSc6	F_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
90	[number]	k4	90
</s>
</p>
<p>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
=	=	kIx~	=
<g/>
90	[number]	k4	90
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
o	o	k7c4	o
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
pro	pro	k7c4	pro
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gFnSc6	F_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
<	<	kIx(	<
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gFnSc6	F_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
<	<	kIx(	<
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
270	[number]	k4	270
</s>
</p>
<p>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
=	=	kIx~	=
<g/>
270	[number]	k4	270
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
o	o	k7c4	o
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
<g/>
)	)	kIx)	)
působiště	působiště	k1gNnSc2	působiště
výsledné	výsledný	k2eAgFnSc2d1	výsledná
síly	síla	k1gFnSc2	síla
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
F_	F_	k1gFnSc1	F_
<g/>
{	{	kIx(	{
<g/>
yi	yi	k?	yi
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
F_	F_	k1gFnSc1	F_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
pro	pro	k7c4	pro
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gFnSc6	F_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
libovolné	libovolný	k2eAgInPc4d1	libovolný
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
F_	F_	k1gFnSc1	F_
<g/>
{	{	kIx(	{
<g/>
xi	xi	k?	xi
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
F_	F_	k1gFnSc1	F_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
pro	pro	k7c4	pro
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gFnSc6	F_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
libovolné	libovolný	k2eAgInPc4d1	libovolný
</s>
</p>
<p>
<s>
===	===	k?	===
Rozklad	rozklad	k1gInSc1	rozklad
sil	síla	k1gFnPc2	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Rozklad	rozklad	k1gInSc1	rozklad
sil	síla	k1gFnPc2	síla
je	být	k5eAaImIp3nS	být
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
síla	síla	k1gFnSc1	síla
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
složky	složka	k1gFnPc4	složka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc7	jejichž
složením	složení	k1gNnSc7	složení
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
původní	původní	k2eAgFnSc4d1	původní
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
opačný	opačný	k2eAgInSc1d1	opačný
proces	proces	k1gInSc1	proces
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
skládání	skládání	k1gNnSc4	skládání
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
sílu	síla	k1gFnSc4	síla
rozkládáme	rozkládat	k5eAaImIp1nP	rozkládat
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozklad	rozklad	k1gInSc4	rozklad
sil	síla	k1gFnPc2	síla
jednoduchou	jednoduchý	k2eAgFnSc7d1	jednoduchá
záležitostí	záležitost	k1gFnSc7	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
známy	znám	k2eAgInPc1d1	znám
směry	směr	k1gInPc1	směr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgNnPc6	který
mají	mít	k5eAaImIp3nP	mít
složky	složka	k1gFnPc1	složka
působit	působit	k5eAaImF	působit
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
tyto	tento	k3xDgInPc1	tento
směry	směr	k1gInPc1	směr
tvoří	tvořit	k5eAaImIp3nP	tvořit
směry	směr	k1gInPc1	směr
stran	strana	k1gFnPc2	strana
rovnoběžníku	rovnoběžník	k1gInSc2	rovnoběžník
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
úhlopříčkou	úhlopříčka	k1gFnSc7	úhlopříčka
je	být	k5eAaImIp3nS	být
původní	původní	k2eAgFnSc1d1	původní
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Velikosti	velikost	k1gFnPc1	velikost
stran	strana	k1gFnPc2	strana
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
rovnoběžníku	rovnoběžník	k1gInSc2	rovnoběžník
představují	představovat	k5eAaImIp3nP	představovat
velikosti	velikost	k1gFnSc2	velikost
složek	složka	k1gFnPc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
ale	ale	k8xC	ale
známy	znám	k2eAgFnPc1d1	známa
velikost	velikost	k1gFnSc1	velikost
a	a	k8xC	a
směr	směr	k1gInSc1	směr
první	první	k4xOgFnSc2	první
složky	složka	k1gFnSc2	složka
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
druhou	druhý	k4xOgFnSc4	druhý
složku	složka	k1gFnSc4	složka
představuje	představovat	k5eAaImIp3nS	představovat
vektor	vektor	k1gInSc4	vektor
spojující	spojující	k2eAgInPc4d1	spojující
koncové	koncový	k2eAgInPc4d1	koncový
body	bod	k1gInPc4	bod
vektorů	vektor	k1gInPc2	vektor
první	první	k4xOgFnSc2	první
složky	složka	k1gFnSc2	složka
a	a	k8xC	a
původní	původní	k2eAgFnSc2d1	původní
síly	síla	k1gFnSc2	síla
(	(	kIx(	(
<g/>
v	v	k7c6	v
uvedeném	uvedený	k2eAgNnSc6d1	uvedené
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
rozklad	rozklad	k1gInSc4	rozklad
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
případě	případ	k1gInSc6	případ
kluzného	kluzný	k2eAgInSc2d1	kluzný
dotyku	dotyk	k1gInSc2	dotyk
dvou	dva	k4xCgNnPc2	dva
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
přenést	přenést	k5eAaPmF	přenést
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
x	x	k?	x
pouze	pouze	k6eAd1	pouze
sílu	síla	k1gFnSc4	síla
Fx	Fx	k1gFnSc2	Fx
takové	takový	k3xDgFnSc2	takový
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
třecí	třecí	k2eAgFnSc3d1	třecí
síle	síla	k1gFnSc3	síla
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgNnPc7	dva
tělesy	těleso	k1gNnPc7	těleso
tj.	tj.	kA	tj.
Ft	Ft	k1gFnSc6	Ft
=	=	kIx~	=
kt	kt	k?	kt
x	x	k?	x
Fy	fy	kA	fy
=	=	kIx~	=
-	-	kIx~	-
Fx	Fx	k1gMnSc1	Fx
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
síla	síla	k1gFnSc1	síla
|	|	kIx~	|
<g/>
Fx	Fx	k1gFnSc1	Fx
<g/>
|	|	kIx~	|
>	>	kIx)	>
|	|	kIx~	|
<g/>
Ft	Ft	k1gFnSc1	Ft
<g/>
|	|	kIx~	|
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
smyku	smyk	k1gInSc3	smyk
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
tělesy	těleso	k1gNnPc7	těleso
a	a	k8xC	a
těleso	těleso	k1gNnSc1	těleso
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
pohybovat	pohybovat	k5eAaImF	pohybovat
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
x	x	k?	x
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
podle	podle	k7c2	podle
výslednice	výslednice	k1gFnSc2	výslednice
síly	síla	k1gFnSc2	síla
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
x.	x.	k?	x.
</s>
</p>
<p>
<s>
Matematické	matematický	k2eAgNnSc1d1	matematické
řešení	řešení	k1gNnSc1	řešení
rozkladu	rozklad	k1gInSc2	rozklad
obecné	obecný	k2eAgFnSc2d1	obecná
síly	síla	k1gFnSc2	síla
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
směrů	směr	k1gInPc2	směr
se	s	k7c7	s
společným	společný	k2eAgNnSc7d1	společné
působištěm	působiště	k1gNnSc7	působiště
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnSc6	F_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
F	F	kA	F
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnSc6	F_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
F	F	kA	F
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
uhly	uhel	k1gInPc1	uhel
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
kladné	kladný	k2eAgFnSc2d1	kladná
poloosy	poloosa	k1gFnSc2	poloosa
"	"	kIx"	"
<g/>
x	x	k?	x
<g/>
"	"	kIx"	"
orientované	orientovaný	k2eAgInPc4d1	orientovaný
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
.	.	kIx.	.
</s>
<s>
Úhly	úhel	k1gInPc1	úhel
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
úhly	úhel	k1gInPc1	úhel
požadovaných	požadovaný	k2eAgInPc2d1	požadovaný
směrů	směr	k1gInPc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
síly	síla	k1gFnPc1	síla
vyjdou	vyjít	k5eAaPmIp3nP	vyjít
záporné	záporný	k2eAgInPc4d1	záporný
mají	mít	k5eAaImIp3nP	mít
opačným	opačný	k2eAgNnSc7d1	opačné
směr	směr	k1gInSc4	směr
než	než	k8xS	než
předpokládaly	předpokládat	k5eAaImAgInP	předpokládat
úhly	úhel	k1gInPc1	úhel
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
požadujeme	požadovat	k5eAaImIp1nP	požadovat
rozložit	rozložit	k5eAaPmF	rozložit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
směrů	směr	k1gInPc2	směr
výslednici	výslednice	k1gFnSc4	výslednice
více	hodně	k6eAd2	hodně
sil	síla	k1gFnPc2	síla
pracujeme	pracovat	k5eAaImIp1nP	pracovat
s	s	k7c7	s
výslednými	výsledný	k2eAgFnPc7d1	výsledná
složkami	složka	k1gFnPc7	složka
vstupních	vstupní	k2eAgFnPc2d1	vstupní
sil	síla	k1gFnPc2	síla
do	do	k7c2	do
směrů	směr	k1gInPc2	směr
x	x	k?	x
a	a	k8xC	a
y	y	k?	y
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gFnSc6	F_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
F_	F_	k1gFnSc7	F_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gFnSc6	F_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
F_	F_	k1gFnSc7	F_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnSc6	F_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
F_	F_	k1gFnSc1	F_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-F_	-F_	k?	-F_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F_	F_	k1gMnSc6	F_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
F_	F_	k1gFnSc1	F_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
-F_	-F_	k?	-F_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
síly	síla	k1gFnPc1	síla
vyjdou	vyjít	k5eAaPmIp3nP	vyjít
záporné	záporný	k2eAgInPc4d1	záporný
mají	mít	k5eAaImIp3nP	mít
opačný	opačný	k2eAgInSc4d1	opačný
směr	směr	k1gInSc4	směr
než	než	k8xS	než
předpokládaly	předpokládat	k5eAaImAgInP	předpokládat
úhly	úhel	k1gInPc1	úhel
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
===	===	k?	===
Rovnováha	rovnováha	k1gFnSc1	rovnováha
sil	síla	k1gFnPc2	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Rovnováha	rovnováha	k1gFnSc1	rovnováha
sil	síla	k1gFnPc2	síla
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
působí	působit	k5eAaImIp3nS	působit
více	hodně	k6eAd2	hodně
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
výslednice	výslednice	k1gFnSc1	výslednice
je	být	k5eAaImIp3nS	být
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
,	,	kIx,	,
a	a	k8xC	a
výsledný	výsledný	k2eAgInSc1d1	výsledný
moment	moment	k1gInSc1	moment
sil	síla	k1gFnPc2	síla
vzniklý	vzniklý	k2eAgMnSc1d1	vzniklý
složením	složení	k1gNnSc7	složení
všech	všecek	k3xTgInPc2	všecek
momentů	moment	k1gInPc2	moment
sil	síla	k1gFnPc2	síla
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
nulový	nulový	k2eAgMnSc1d1	nulový
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
⋯	⋯	k?	⋯
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k6eAd1	cdots
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
⋯	⋯	k?	⋯
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k6eAd1	cdots
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
V	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
platí	platit	k5eAaImIp3nS	platit
3	[number]	k4	3
rovnice	rovnice	k1gFnSc2	rovnice
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
F_	F_	k1gFnSc7	F_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
F_	F_	k1gFnSc7	F_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
F_	F_	k1gFnSc7	F_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
-y_	_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Kde	kde	k6eAd1	kde
úhly	úhel	k1gInPc1	úhel
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
α	α	k?	α
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
kladné	kladný	k2eAgFnSc2d1	kladná
poloosy	poloosa	k1gFnSc2	poloosa
x	x	k?	x
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
proti	proti	k7c3	proti
pohybu	pohyb	k1gInSc3	pohyb
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
</s>
</p>
<p>
<s>
Jestliže	jestliže	k8xS	jestliže
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
bodě	bod	k1gInSc6	bod
dvě	dva	k4xCgFnPc1	dva
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
nastane	nastat	k5eAaPmIp3nS	nastat
rovnováha	rovnováha	k1gFnSc1	rovnováha
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
síly	síla	k1gFnPc1	síla
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
velké	velká	k1gFnPc1	velká
opačného	opačný	k2eAgInSc2d1	opačný
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgMnSc2	jenž
jsou	být	k5eAaImIp3nP	být
síly	síla	k1gFnPc1	síla
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
první	první	k4xOgInSc1	první
pohybový	pohybový	k2eAgInSc1d1	pohybový
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yRgNnSc2	který
jsou	být	k5eAaImIp3nP	být
síly	síla	k1gFnPc4	síla
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
a	a	k8xC	a
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nepohybuje	pohybovat	k5eNaImIp3nS	pohybovat
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
v	v	k7c6	v
některé	některý	k3yIgFnSc6	některý
z	z	k7c2	z
rovnovážných	rovnovážný	k2eAgFnPc2d1	rovnovážná
poloh	poloha	k1gFnPc2	poloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
sil	síla	k1gFnPc2	síla
==	==	k?	==
</s>
</p>
<p>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Tento	tento	k3xDgInSc1	tento
přehled	přehled	k1gInSc1	přehled
uvádí	uvádět	k5eAaImIp3nS	uvádět
pouze	pouze	k6eAd1	pouze
stručné	stručný	k2eAgFnSc2d1	stručná
charakteristiky	charakteristika	k1gFnSc2	charakteristika
a	a	k8xC	a
závislosti	závislost	k1gFnSc2	závislost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
spíše	spíše	k9	spíše
jako	jako	k9	jako
rozcestník	rozcestník	k1gInSc4	rozcestník
ke	k	k7c3	k
speciálním	speciální	k2eAgFnPc3d1	speciální
stránkám	stránka	k1gFnPc3	stránka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Základní	základní	k2eAgFnPc4d1	základní
síly	síla	k1gFnPc4	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Základními	základní	k2eAgFnPc7d1	základní
silami	síla	k1gFnPc7	síla
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
makroskopických	makroskopický	k2eAgInPc6d1	makroskopický
měřítcích	měřítek	k1gInPc6	měřítek
síla	síla	k1gFnSc1	síla
gravitační	gravitační	k2eAgFnSc1d1	gravitační
a	a	k8xC	a
síla	síla	k1gFnSc1	síla
elektrostatická	elektrostatický	k2eAgFnSc1d1	elektrostatická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
Newtonovým	Newtonův	k2eAgInSc7d1	Newtonův
gravitačním	gravitační	k2eAgInSc7d1	gravitační
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
je	být	k5eAaImIp3nS	být
úměrná	úměrný	k2eAgFnSc1d1	úměrná
hmotnostem	hmotnost	k1gFnPc3	hmotnost
těles	těleso	k1gNnPc2	těleso
a	a	k8xC	a
nepřímo	přímo	k6eNd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
druhé	druhý	k4xOgFnSc6	druhý
mocnině	mocnina	k1gFnSc3	mocnina
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
přitažlivá	přitažlivý	k2eAgFnSc1d1	přitažlivá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elektrostatická	elektrostatický	k2eAgFnSc1d1	elektrostatická
síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
Coulombovým	Coulombův	k2eAgInSc7d1	Coulombův
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
je	být	k5eAaImIp3nS	být
úměrná	úměrný	k2eAgFnSc1d1	úměrná
nábojům	náboj	k1gInPc3	náboj
těles	těleso	k1gNnPc2	těleso
a	a	k8xC	a
nepřímo	přímo	k6eNd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
druhé	druhý	k4xOgFnSc6	druhý
mocnině	mocnina	k1gFnSc3	mocnina
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přitažlivá	přitažlivý	k2eAgFnSc1d1	přitažlivá
pro	pro	k7c4	pro
náboje	náboj	k1gInPc4	náboj
opačné	opačný	k2eAgFnSc2d1	opačná
polarity	polarita	k1gFnSc2	polarita
a	a	k8xC	a
odpudivá	odpudivý	k2eAgFnSc1d1	odpudivá
pro	pro	k7c4	pro
náboje	náboj	k1gInPc4	náboj
stejné	stejný	k2eAgFnSc2d1	stejná
polarity	polarita	k1gFnSc2	polarita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pružné	pružný	k2eAgFnPc4d1	pružná
(	(	kIx(	(
<g/>
elastické	elastický	k2eAgFnPc4d1	elastická
<g/>
)	)	kIx)	)
síly	síla	k1gFnPc4	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
pružné	pružný	k2eAgInPc4d1	pružný
(	(	kIx(	(
<g/>
elastické	elastický	k2eAgInPc4d1	elastický
<g/>
)	)	kIx)	)
síly	síl	k1gInPc4	síl
řadíme	řadit	k5eAaImIp1nP	řadit
sílu	síla	k1gFnSc4	síla
tahovou	tahový	k2eAgFnSc4d1	tahová
a	a	k8xC	a
tlakovou	tlakový	k2eAgFnSc4d1	tlaková
<g/>
,	,	kIx,	,
ohybovou	ohybový	k2eAgFnSc4d1	ohybová
<g/>
,	,	kIx,	,
smykovou	smykový	k2eAgFnSc4d1	smyková
a	a	k8xC	a
torzní	torzní	k2eAgFnSc4d1	torzní
<g/>
.	.	kIx.	.
</s>
<s>
Deformace	deformace	k1gFnSc1	deformace
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
přímo	přímo	k6eAd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
působící	působící	k2eAgFnSc3d1	působící
síle	síla	k1gFnSc3	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tahová	tahový	k2eAgFnSc1d1	tahová
a	a	k8xC	a
tlaková	tlakový	k2eAgFnSc1d1	tlaková
pružná	pružný	k2eAgFnSc1d1	pružná
síla	síla	k1gFnSc1	síla
jsou	být	k5eAaImIp3nP	být
úměrné	úměrný	k2eAgInPc1d1	úměrný
záporně	záporně	k6eAd1	záporně
vzaté	vzatý	k2eAgFnSc3d1	vzatá
změně	změna	k1gFnSc3	změna
délky	délka	k1gFnSc2	délka
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
působící	působící	k2eAgFnSc2d1	působící
síly	síla	k1gFnSc2	síla
(	(	kIx(	(
<g/>
koeficient	koeficient	k1gInSc1	koeficient
úměrnosti	úměrnost	k1gFnSc2	úměrnost
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
tuhost	tuhost	k1gFnSc1	tuhost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
také	také	k9	také
jako	jako	k9	jako
úměrné	úměrný	k2eAgNnSc1d1	úměrné
relativnímu	relativní	k2eAgNnSc3d1	relativní
prodloužení	prodloužení	k1gNnSc3	prodloužení
<g/>
/	/	kIx~	/
<g/>
zkrácení	zkrácení	k1gNnSc3	zkrácení
a	a	k8xC	a
příčnému	příčný	k2eAgInSc3d1	příčný
průřezu	průřez	k1gInSc3	průřez
(	(	kIx(	(
<g/>
koeficient	koeficient	k1gInSc1	koeficient
úměrnosti	úměrnost	k1gFnSc2	úměrnost
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nazývá	nazývat	k5eAaImIp3nS	nazývat
Youngův	Youngův	k2eAgInSc1d1	Youngův
modul	modul	k1gInSc1	modul
pružnosti	pružnost	k1gFnSc2	pružnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ohybová	ohybový	k2eAgFnSc1d1	ohybová
síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
v	v	k7c6	v
tělese	těleso	k1gNnSc6	těleso
projevuje	projevovat	k5eAaImIp3nS	projevovat
kombinací	kombinace	k1gFnSc7	kombinace
tahu	tah	k1gInSc2	tah
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
malé	malý	k2eAgInPc4d1	malý
ohyby	ohyb	k1gInPc4	ohyb
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
příčné	příčný	k2eAgFnSc3d1	příčná
výchylce	výchylka	k1gFnSc3	výchylka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
kubicky	kubicky	k6eAd1	kubicky
<g/>
)	)	kIx)	)
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
příčném	příčný	k2eAgInSc6d1	příčný
rozměru	rozměr	k1gInSc6	rozměr
tělesa	těleso	k1gNnSc2	těleso
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
výchylky	výchylka	k1gFnSc2	výchylka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smyková	smykový	k2eAgFnSc1d1	smyková
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
úměrná	úměrný	k2eAgFnSc1d1	úměrná
smykovému	smykový	k2eAgNnSc3d1	smykové
úhlu	úhel	k1gInSc2	úhel
a	a	k8xC	a
příčnému	příčný	k2eAgInSc3d1	příčný
průřezu	průřez	k1gInSc3	průřez
(	(	kIx(	(
<g/>
koeficient	koeficient	k1gInSc1	koeficient
úměrnosti	úměrnost	k1gFnSc2	úměrnost
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nazývá	nazývat	k5eAaImIp3nS	nazývat
modul	modul	k1gInSc1	modul
pružnosti	pružnost	k1gFnSc2	pružnost
ve	v	k7c6	v
smyku	smyk	k1gInSc6	smyk
nebo	nebo	k8xC	nebo
modul	modul	k1gInSc1	modul
torze	torze	k1gFnSc2	torze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Speciálním	speciální	k2eAgInSc7d1	speciální
případem	případ	k1gInSc7	případ
je	být	k5eAaImIp3nS	být
torzní	torzní	k2eAgFnSc1d1	torzní
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
silové	silový	k2eAgFnSc6d1	silová
dvojici	dvojice	k1gFnSc6	dvojice
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
pružné	pružný	k2eAgNnSc1d1	pružné
zkroucení	zkroucení	k1gNnSc1	zkroucení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reaktivní	reaktivní	k2eAgFnPc1d1	reaktivní
síly	síla	k1gFnPc1	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yRgInSc2	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
oddělování	oddělování	k1gNnSc3	oddělování
nebo	nebo	k8xC	nebo
připojování	připojování	k1gNnSc4	připojování
částic	částice	k1gFnPc2	částice
zanedbatelné	zanedbatelný	k2eAgFnSc2d1	zanedbatelná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
hmotnosti	hmotnost	k1gFnSc3	hmotnost
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
pohybová	pohybový	k2eAgFnSc1d1	pohybová
rovnice	rovnice	k1gFnSc1	rovnice
takové	takový	k3xDgFnSc2	takový
soustavy	soustava	k1gFnSc2	soustava
přepsat	přepsat	k5eAaPmF	přepsat
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
pohybové	pohybový	k2eAgFnSc2d1	pohybová
rovnice	rovnice	k1gFnSc2	rovnice
tělesa	těleso	k1gNnSc2	těleso
s	s	k7c7	s
proměnnou	proměnný	k2eAgFnSc7d1	proměnná
(	(	kIx(	(
<g/>
klidovou	klidový	k2eAgFnSc7d1	klidová
<g/>
)	)	kIx)	)
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
jeho	jeho	k3xOp3gFnSc2	jeho
hybnosti	hybnost	k1gFnSc2	hybnost
bude	být	k5eAaImBp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
součtu	součet	k1gInSc6	součet
vnější	vnější	k2eAgFnPc4d1	vnější
síly	síla	k1gFnPc4	síla
a	a	k8xC	a
síly	síla	k1gFnPc4	síla
reaktivní	reaktivní	k2eAgFnPc4d1	reaktivní
<g/>
,	,	kIx,	,
rovné	rovný	k2eAgFnPc4d1	rovná
součinu	součin	k1gInSc3	součin
vektoru	vektor	k1gInSc2	vektor
relativní	relativní	k2eAgFnSc2d1	relativní
rychlosti	rychlost	k1gFnSc2	rychlost
oddělovaných	oddělovaný	k2eAgFnPc2d1	oddělovaná
<g/>
/	/	kIx~	/
<g/>
připojovaných	připojovaný	k2eAgFnPc2d1	připojovaná
částic	částice	k1gFnPc2	částice
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tělesu	těleso	k1gNnSc3	těleso
a	a	k8xC	a
časové	časový	k2eAgFnSc2d1	časová
změny	změna	k1gFnSc2	změna
(	(	kIx(	(
<g/>
derivace	derivace	k1gFnSc1	derivace
<g/>
)	)	kIx)	)
hmotnosti	hmotnost	k1gFnPc1	hmotnost
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
při	při	k7c6	při
úbytku	úbytek	k1gInSc6	úbytek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
tedy	tedy	k9	tedy
bude	být	k5eAaImBp3nS	být
působit	působit	k5eAaImF	působit
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
unikání	unikání	k1gNnSc2	unikání
částic	částice	k1gFnPc2	částice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Odporové	odporový	k2eAgFnPc1d1	odporová
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
dynamický	dynamický	k2eAgInSc1d1	dynamický
vztlak	vztlak	k1gInSc1	vztlak
===	===	k?	===
</s>
</p>
<p>
<s>
Odporové	odporový	k2eAgFnPc1d1	odporová
síly	síla	k1gFnPc1	síla
jsou	být	k5eAaImIp3nP	být
typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
disipativních	disipativní	k2eAgFnPc2d1	disipativní
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnPc4	on
síla	síla	k1gFnSc1	síla
smykového	smykový	k2eAgNnSc2d1	smykové
tření	tření	k1gNnSc2	tření
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
pevnými	pevný	k2eAgNnPc7d1	pevné
tělesy	těleso	k1gNnPc7	těleso
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
síla	síla	k1gFnSc1	síla
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
tření	tření	k1gNnSc2	tření
tekutin	tekutina	k1gFnPc2	tekutina
a	a	k8xC	a
odporové	odporový	k2eAgFnSc2d1	odporová
síly	síla	k1gFnSc2	síla
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
těles	těleso	k1gNnPc2	těleso
v	v	k7c6	v
tekutinách	tekutina	k1gFnPc6	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
působí	působit	k5eAaImIp3nP	působit
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
relativního	relativní	k2eAgInSc2d1	relativní
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Síla	síla	k1gFnSc1	síla
smykového	smykový	k2eAgNnSc2d1	smykové
tření	tření	k1gNnSc2	tření
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
kolmé	kolmý	k2eAgFnSc3d1	kolmá
složce	složka	k1gFnSc3	složka
síly	síla	k1gFnSc2	síla
(	(	kIx(	(
<g/>
koeficient	koeficient	k1gInSc1	koeficient
úměrnosti	úměrnost	k1gFnSc2	úměrnost
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
činitel	činitel	k1gInSc1	činitel
smykového	smykový	k2eAgNnSc2d1	smykové
tření	tření	k1gNnSc2	tření
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgInSc1d2	veliký
pro	pro	k7c4	pro
klidové	klidový	k2eAgNnSc4d1	klidové
tření	tření	k1gNnSc4	tření
než	než	k8xS	než
pro	pro	k7c4	pro
tření	tření	k1gNnSc4	tření
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
třecí	třecí	k2eAgFnSc6d1	třecí
ploše	plocha	k1gFnSc6	plocha
a	a	k8xC	a
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
ani	ani	k8xC	ani
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Viskózní	viskózní	k2eAgFnSc1d1	viskózní
síla	síla	k1gFnSc1	síla
(	(	kIx(	(
<g/>
síla	síla	k1gFnSc1	síla
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
tření	tření	k1gNnSc2	tření
<g/>
)	)	kIx)	)
působí	působit	k5eAaImIp3nS	působit
při	při	k7c6	při
laminárním	laminární	k2eAgNnSc6d1	laminární
proudění	proudění	k1gNnSc6	proudění
mezi	mezi	k7c7	mezi
sousedními	sousední	k2eAgFnPc7d1	sousední
elementárními	elementární	k2eAgFnPc7d1	elementární
vrstvami	vrstva	k1gFnPc7	vrstva
tekutiny	tekutina	k1gFnSc2	tekutina
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
vůči	vůči	k7c3	vůči
sobě	se	k3xPyFc3	se
pohybujícími	pohybující	k2eAgMnPc7d1	pohybující
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
velikosti	velikost	k1gFnSc2	velikost
(	(	kIx(	(
<g/>
myšlené	myšlený	k2eAgFnSc2d1	myšlená
<g/>
)	)	kIx)	)
styčné	styčný	k2eAgFnSc2d1	styčná
plochy	plocha	k1gFnSc2	plocha
a	a	k8xC	a
gradientu	gradient	k1gInSc2	gradient
rychlosti	rychlost	k1gFnSc2	rychlost
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
derivaci	derivace	k1gFnSc4	derivace
rychlosti	rychlost	k1gFnSc2	rychlost
proudění	proudění	k1gNnSc2	proudění
s	s	k7c7	s
příčným	příčný	k2eAgInSc7d1	příčný
rozměrem	rozměr	k1gInSc7	rozměr
<g/>
,	,	kIx,	,
kolmým	kolmý	k2eAgNnSc7d1	kolmé
na	na	k7c4	na
styčnou	styčný	k2eAgFnSc4d1	styčná
plochu	plocha	k1gFnSc4	plocha
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
na	na	k7c4	na
směr	směr	k1gInSc4	směr
proudění	proudění	k1gNnSc2	proudění
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
koeficient	koeficient	k1gInSc1	koeficient
úměrnosti	úměrnost	k1gFnSc2	úměrnost
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
dynamická	dynamický	k2eAgFnSc1d1	dynamická
viskozita	viskozita	k1gFnSc1	viskozita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odporová	odporový	k2eAgFnSc1d1	odporová
síla	síla	k1gFnSc1	síla
při	při	k7c6	při
laminárním	laminární	k2eAgNnSc6d1	laminární
obtékání	obtékání	k1gNnSc6	obtékání
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
viskozitě	viskozita	k1gFnSc3	viskozita
tekutiny	tekutina	k1gFnSc2	tekutina
a	a	k8xC	a
první	první	k4xOgFnSc3	první
mocnině	mocnina	k1gFnSc3	mocnina
rychlosti	rychlost	k1gFnSc3	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
geometrickém	geometrický	k2eAgInSc6d1	geometrický
tvaru	tvar	k1gInSc6	tvar
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odporová	odporový	k2eAgFnSc1d1	odporová
síla	síla	k1gFnSc1	síla
při	při	k7c6	při
turbulentním	turbulentní	k2eAgNnSc6d1	turbulentní
obtékání	obtékání	k1gNnSc6	obtékání
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
viskozitě	viskozita	k1gFnSc3	viskozita
tekutiny	tekutina	k1gFnSc2	tekutina
<g/>
,	,	kIx,	,
ploše	ploš	k1gMnSc2	ploš
příčného	příčný	k2eAgInSc2d1	příčný
průřezu	průřez	k1gInSc2	průřez
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
kvadraticky	kvadraticky	k6eAd1	kvadraticky
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
geometrickém	geometrický	k2eAgInSc6d1	geometrický
tvaru	tvar	k1gInSc6	tvar
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odporová	odporový	k2eAgFnSc1d1	odporová
síla	síla	k1gFnSc1	síla
při	při	k7c6	při
obtékání	obtékání	k1gNnSc6	obtékání
rychlostí	rychlost	k1gFnPc2	rychlost
blízkou	blízký	k2eAgFnSc7d1	blízká
rychlostí	rychlost	k1gFnSc7	rychlost
zvuku	zvuk	k1gInSc2	zvuk
má	mít	k5eAaImIp3nS	mít
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
složitější	složitý	k2eAgNnSc1d2	složitější
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
laminárním	laminární	k2eAgNnSc6d1	laminární
obtékání	obtékání	k1gNnSc6	obtékání
tělesa	těleso	k1gNnSc2	těleso
tekutinou	tekutina	k1gFnSc7	tekutina
vznikají	vznikat	k5eAaImIp3nP	vznikat
ještě	ještě	k6eAd1	ještě
(	(	kIx(	(
<g/>
nedisipativní	disipativní	k2eNgFnSc2d1	disipativní
<g/>
)	)	kIx)	)
dynamické	dynamický	k2eAgFnSc2d1	dynamická
vztlakové	vztlakový	k2eAgFnSc2d1	vztlaková
síly	síla	k1gFnSc2	síla
(	(	kIx(	(
<g/>
hydrodynamické	hydrodynamický	k2eAgInPc1d1	hydrodynamický
<g/>
,	,	kIx,	,
aerodynamické	aerodynamický	k2eAgInPc1d1	aerodynamický
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
souvisejí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
rozdíly	rozdíl	k1gInPc7	rozdíl
tlaku	tlak	k1gInSc2	tlak
v	v	k7c6	v
tekutině	tekutina	k1gFnSc6	tekutina
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
stranách	strana	k1gFnPc6	strana
tělesa	těleso	k1gNnSc2	těleso
způsobenými	způsobený	k2eAgFnPc7d1	způsobená
rozdílnou	rozdílný	k2eAgFnSc7d1	rozdílná
rychlostí	rychlost	k1gFnSc7	rychlost
obtékání	obtékání	k1gNnSc4	obtékání
podle	podle	k7c2	podle
Bernoulliovy	Bernoulliův	k2eAgFnSc2d1	Bernoulliova
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Povrchové	povrchový	k2eAgFnPc1d1	povrchová
síly	síla	k1gFnPc1	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Povrchové	povrchový	k2eAgFnPc1d1	povrchová
síly	síla	k1gFnPc1	síla
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc2	rozhraní
dvou	dva	k4xCgNnPc2	dva
prostředí	prostředí	k1gNnPc2	prostředí
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
aspoň	aspoň	k9	aspoň
jedno	jeden	k4xCgNnSc1	jeden
je	být	k5eAaImIp3nS	být
kapalné	kapalný	k2eAgNnSc1d1	kapalné
<g/>
.	.	kIx.	.
</s>
<s>
Nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
též	též	k9	též
kapilární	kapilární	k2eAgFnPc1d1	kapilární
<g/>
.	.	kIx.	.
</s>
<s>
Povrchová	povrchový	k2eAgFnSc1d1	povrchová
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
úměrná	úměrný	k2eAgFnSc1d1	úměrná
délce	délka	k1gFnSc3	délka
myšleného	myšlený	k2eAgInSc2d1	myšlený
řezu	řez	k1gInSc2	řez
povrchem	povrch	k1gInSc7	povrch
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
povrchu	povrch	k1gInSc6	povrch
kolmo	kolmo	k6eAd1	kolmo
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
řezu	řez	k1gInSc3	řez
(	(	kIx(	(
<g/>
koeficient	koeficient	k1gInSc1	koeficient
úměrnosti	úměrnost	k1gFnSc2	úměrnost
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
povrchové	povrchový	k2eAgNnSc1d1	povrchové
napětí	napětí	k1gNnSc1	napětí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Osmotické	osmotický	k2eAgFnPc1d1	osmotická
síly	síla	k1gFnPc1	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
dva	dva	k4xCgInPc1	dva
tekuté	tekutý	k2eAgInPc1d1	tekutý
roztoky	roztok	k1gInPc1	roztok
téže	týž	k3xTgFnSc2	týž
látky	látka	k1gFnSc2	látka
a	a	k8xC	a
nestejné	stejný	k2eNgFnPc1d1	nestejná
koncentrace	koncentrace	k1gFnPc1	koncentrace
odděleny	oddělit	k5eAaPmNgFnP	oddělit
polopropustnou	polopropustný	k2eAgFnSc7d1	polopropustná
přepážkou	přepážka	k1gFnSc7	přepážka
(	(	kIx(	(
<g/>
propustnou	propustný	k2eAgFnSc4d1	propustná
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
molekuly	molekula	k1gFnPc4	molekula
rozpouštědla	rozpouštědlo	k1gNnSc2	rozpouštědlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
difuze	difuze	k1gFnSc1	difuze
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
parciální	parciální	k2eAgInSc4d1	parciální
tlak	tlak	k1gInSc4	tlak
rozpouštědla	rozpouštědlo	k1gNnSc2	rozpouštědlo
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
přepážky	přepážka	k1gFnSc2	přepážka
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
pronikání	pronikání	k1gNnSc3	pronikání
molekul	molekula	k1gFnPc2	molekula
rozpouštědla	rozpouštědlo	k1gNnSc2	rozpouštědlo
do	do	k7c2	do
koncentrovanějšího	koncentrovaný	k2eAgInSc2d2	koncentrovanější
roztoku	roztok	k1gInSc2	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
osmotický	osmotický	k2eAgInSc1d1	osmotický
tlak	tlak	k1gInSc1	tlak
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
osmotickou	osmotický	k2eAgFnSc4d1	osmotická
sílu	síla	k1gFnSc4	síla
přímo	přímo	k6eAd1	přímo
úměrnou	úměrná	k1gFnSc7	úměrná
rozdílu	rozdíl	k1gInSc2	rozdíl
molární	molární	k2eAgFnSc2d1	molární
koncentrace	koncentrace	k1gFnSc2	koncentrace
obou	dva	k4xCgInPc2	dva
roztoků	roztok	k1gInPc2	roztok
<g/>
,	,	kIx,	,
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
ploše	plocha	k1gFnSc6	plocha
přepážky	přepážka	k1gFnSc2	přepážka
<g/>
;	;	kIx,	;
koeficientem	koeficient	k1gInSc7	koeficient
úměrnosti	úměrnost	k1gFnSc2	úměrnost
je	být	k5eAaImIp3nS	být
molární	molární	k2eAgFnSc1d1	molární
plynová	plynový	k2eAgFnSc1d1	plynová
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Setrvačné	setrvačný	k2eAgFnPc1d1	setrvačná
síly	síla	k1gFnPc1	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
setrvačné	setrvačný	k2eAgFnPc4d1	setrvačná
síly	síla	k1gFnPc4	síla
patří	patřit	k5eAaImIp3nS	patřit
zdánlivé	zdánlivý	k2eAgFnPc4d1	zdánlivá
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
působení	působení	k1gNnSc6	působení
materiálních	materiální	k2eAgInPc2d1	materiální
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gFnPc4	on
unášivá	unášivý	k2eAgFnSc1d1	unášivá
síla	síla	k1gFnSc1	síla
a	a	k8xC	a
Coriolisova	Coriolisův	k2eAgFnSc1d1	Coriolisova
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Unášivá	unášivý	k2eAgFnSc1d1	unášivá
síla	síla	k1gFnSc1	síla
uděluje	udělovat	k5eAaImIp3nS	udělovat
tělesům	těleso	k1gNnPc3	těleso
zrychlení	zrychlení	k1gNnSc2	zrychlení
nezávislé	závislý	k2eNgInPc4d1	nezávislý
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
rychlosti	rychlost	k1gFnSc6	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
odstředivá	odstředivý	k2eAgFnSc1d1	odstředivá
síla	síla	k1gFnSc1	síla
a	a	k8xC	a
Eulerova	Eulerův	k2eAgFnSc1d1	Eulerova
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
<s>
Coriolisova	Coriolisův	k2eAgFnSc1d1	Coriolisova
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrná	k1gFnSc1	úměrná
rychlosti	rychlost	k1gFnSc2	rychlost
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
kolmo	kolmo	k6eAd1	kolmo
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
sílu	síla	k1gFnSc4	síla
gyroskopickou	gyroskopický	k2eAgFnSc4d1	gyroskopická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tíhová	tíhový	k2eAgFnSc1d1	tíhová
síla	síla	k1gFnSc1	síla
a	a	k8xC	a
tíha	tíha	k1gFnSc1	tíha
<g/>
,	,	kIx,	,
statický	statický	k2eAgInSc4d1	statický
</s>
<s>
vztlak	vztlak	k1gInSc1	vztlak
===	===	k?	===
</s>
</p>
<p>
<s>
Nejběžnějším	běžný	k2eAgNnSc7d3	nejběžnější
prostředím	prostředí	k1gNnSc7	prostředí
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
povrch	povrch	k7c2wR	povrch
Země	zem	k1gFnSc2	zem
nebo	nebo	k8xC	nebo
místa	místo	k1gNnSc2	místo
nad	nad	k7c7	nad
a	a	k8xC	a
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
povrchem	povrch	k1gInSc7	povrch
Země	zem	k1gFnSc2	zem
proto	proto	k8xC	proto
zpravidla	zpravidla	k6eAd1	zpravidla
spojuje	spojovat	k5eAaImIp3nS	spojovat
svou	svůj	k3xOyFgFnSc4	svůj
souřadnou	souřadný	k2eAgFnSc4d1	souřadná
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
však	však	k9	však
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
zemské	zemský	k2eAgFnSc3d1	zemská
rotaci	rotace	k1gFnSc3	rotace
soustavou	soustava	k1gFnSc7	soustava
neinerciální	inerciální	k2eNgFnSc7d1	neinerciální
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
proto	proto	k6eAd1	proto
nejen	nejen	k6eAd1	nejen
(	(	kIx(	(
<g/>
pravá	pravý	k2eAgFnSc1d1	pravá
<g/>
)	)	kIx)	)
gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
síly	síla	k1gFnPc4	síla
setrvačné	setrvačný	k2eAgFnPc4d1	setrvačná
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
odstředivá	odstředivý	k2eAgFnSc1d1	odstředivá
síla	síla	k1gFnSc1	síla
pro	pro	k7c4	pro
tělesa	těleso	k1gNnPc4	těleso
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
vůči	vůči	k7c3	vůči
zemskému	zemský	k2eAgInSc3d1	zemský
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Výslednici	výslednice	k1gFnSc4	výslednice
těchto	tento	k3xDgFnPc2	tento
sil	síla	k1gFnPc2	síla
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
tíhovou	tíhový	k2eAgFnSc4d1	tíhová
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
pole	pole	k1gNnSc4	pole
jako	jako	k8xS	jako
tíhové	tíhový	k2eAgNnSc4d1	tíhové
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tíha	tíha	k1gFnSc1	tíha
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
vyjadřující	vyjadřující	k2eAgFnSc4d1	vyjadřující
sílu	síla	k1gFnSc4	síla
statického	statický	k2eAgNnSc2d1	statické
působení	působení	k1gNnSc2	působení
tělesa	těleso	k1gNnSc2	těleso
v	v	k7c6	v
tíhovém	tíhový	k2eAgNnSc6d1	tíhové
poli	pole	k1gNnSc6	pole
na	na	k7c4	na
jiné	jiný	k2eAgNnSc4d1	jiné
těleso	těleso	k1gNnSc4	těleso
(	(	kIx(	(
<g/>
např.	např.	kA	např.
podložku	podložka	k1gFnSc4	podložka
nebo	nebo	k8xC	nebo
závěs	závěs	k1gInSc4	závěs
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Některé	některý	k3yIgFnSc2	některý
definice	definice	k1gFnSc2	definice
tíhu	tíha	k1gFnSc4	tíha
od	od	k7c2	od
tíhové	tíhový	k2eAgFnSc2d1	tíhová
síly	síla	k1gFnSc2	síla
nerozlišují	rozlišovat	k5eNaImIp3nP	rozlišovat
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
tíhovým	tíhový	k2eAgNnSc7d1	tíhové
polem	pole	k1gNnSc7	pole
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
vznik	vznik	k1gInSc1	vznik
statické	statický	k2eAgFnSc2d1	statická
vztlakové	vztlakový	k2eAgFnSc2d1	vztlaková
síly	síla	k1gFnSc2	síla
(	(	kIx(	(
<g/>
hydrostatické	hydrostatický	k2eAgInPc1d1	hydrostatický
<g/>
,	,	kIx,	,
aerostatické	aerostatický	k2eAgInPc1d1	aerostatický
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
působící	působící	k2eAgFnSc2d1	působící
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
ponořené	ponořený	k2eAgNnSc4d1	ponořené
do	do	k7c2	do
tekutiny	tekutina	k1gFnSc2	tekutina
v	v	k7c6	v
tíhovém	tíhový	k2eAgNnSc6d1	tíhové
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
součinu	součin	k1gInSc2	součin
objemu	objem	k1gInSc2	objem
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
tíhového	tíhový	k2eAgNnSc2d1	tíhové
zrychlení	zrychlení	k1gNnSc2	zrychlení
a	a	k8xC	a
hustoty	hustota	k1gFnSc2	hustota
tekutiny	tekutina	k1gFnSc2	tekutina
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
ponořeného	ponořený	k2eAgNnSc2d1	ponořené
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
u	u	k7c2	u
stlačitelných	stlačitelný	k2eAgFnPc2d1	stlačitelná
tekutin	tekutina	k1gFnPc2	tekutina
platí	platit	k5eAaImIp3nS	platit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
tělesa	těleso	k1gNnPc4	těleso
malé	malý	k2eAgFnSc2d1	malá
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
lze	lze	k6eAd1	lze
zanedbat	zanedbat	k5eAaPmF	zanedbat
změnu	změna	k1gFnSc4	změna
hustoty	hustota	k1gFnSc2	hustota
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
tělesa	těleso	k1gNnSc2	těleso
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Síly	síla	k1gFnPc1	síla
v	v	k7c6	v
elektromagnetickém	elektromagnetický	k2eAgNnSc6d1	elektromagnetické
poli	pole	k1gNnSc6	pole
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
bodové	bodový	k2eAgInPc4d1	bodový
náboje	náboj	k1gInPc4	náboj
působí	působit	k5eAaImIp3nS	působit
vedle	vedle	k7c2	vedle
základní	základní	k2eAgFnSc2d1	základní
elektrostatické	elektrostatický	k2eAgFnSc2d1	elektrostatická
síly	síla	k1gFnSc2	síla
také	také	k9	také
magnetické	magnetický	k2eAgFnSc2d1	magnetická
stacionární	stacionární	k2eAgFnSc2d1	stacionární
pole	pole	k1gFnSc2	pole
tzv.	tzv.	kA	tzv.
Lorentzovou	Lorentzový	k2eAgFnSc7d1	Lorentzový
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
magnetické	magnetický	k2eAgFnSc3d1	magnetická
indukci	indukce	k1gFnSc3	indukce
a	a	k8xC	a
náboji	náboj	k1gInSc3	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
též	též	k9	též
přímo	přímo	k6eAd1	přímo
úměrná	úměrná	k1gFnSc1	úměrná
rychlosti	rychlost	k1gFnSc2	rychlost
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
kolmo	kolmo	k6eAd1	kolmo
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
sílu	síla	k1gFnSc4	síla
gyroskopickou	gyroskopický	k2eAgFnSc4d1	gyroskopická
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
Elektrické	elektrický	k2eAgFnPc1d1	elektrická
síly	síla	k1gFnPc1	síla
působící	působící	k2eAgFnPc1d1	působící
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
soustavy	soustava	k1gFnPc4	soustava
nábojů	náboj	k1gInPc2	náboj
a	a	k8xC	a
nabitých	nabitý	k2eAgInPc2d1	nabitý
vodičů	vodič	k1gInPc2	vodič
jsou	být	k5eAaImIp3nP	být
dané	daný	k2eAgFnPc1d1	daná
superpozicí	superpozice	k1gFnSc7	superpozice
elektrostatických	elektrostatický	k2eAgFnPc2d1	elektrostatická
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dynamických	dynamický	k2eAgInPc2d1	dynamický
systémů	systém	k1gInPc2	systém
působí	působit	k5eAaImIp3nS	působit
navíc	navíc	k6eAd1	navíc
elektrické	elektrický	k2eAgFnSc2d1	elektrická
síly	síla	k1gFnSc2	síla
vznikající	vznikající	k2eAgFnSc1d1	vznikající
elektromagnetickou	elektromagnetický	k2eAgFnSc7d1	elektromagnetická
indukcí	indukce	k1gFnSc7	indukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Magnetické	magnetický	k2eAgFnPc1d1	magnetická
síly	síla	k1gFnPc1	síla
působí	působit	k5eAaImIp3nP	působit
též	též	k9	též
mezi	mezi	k7c7	mezi
vodiči	vodič	k1gInPc7	vodič
protékanými	protékaný	k2eAgInPc7d1	protékaný
elektrickým	elektrický	k2eAgInSc7d1	elektrický
proudem	proud	k1gInSc7	proud
a	a	k8xC	a
mezi	mezi	k7c7	mezi
magnetickými	magnetický	k2eAgInPc7d1	magnetický
dipóly	dipól	k1gInPc7	dipól
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
elementární	elementární	k2eAgInPc1d1	elementární
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dynamických	dynamický	k2eAgInPc2d1	dynamický
systémů	systém	k1gInPc2	systém
magnetické	magnetický	k2eAgFnSc2d1	magnetická
síly	síla	k1gFnSc2	síla
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
proměnném	proměnný	k2eAgNnSc6d1	proměnné
elektrickém	elektrický	k2eAgNnSc6d1	elektrické
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
nábojů	náboj	k1gInPc2	náboj
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
první	první	k4xOgFnSc2	první
Maxwellovy	Maxwellův	k2eAgFnSc2d1	Maxwellova
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
dielektrika	dielektrikum	k1gNnPc4	dielektrikum
a	a	k8xC	a
magnetika	magnetikum	k1gNnPc4	magnetikum
umístěná	umístěný	k2eAgNnPc4d1	umístěné
do	do	k7c2	do
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
působí	působit	k5eAaImIp3nS	působit
tzv.	tzv.	kA	tzv.
ponderomotorické	ponderomotorický	k2eAgFnPc4d1	ponderomotorický
síly	síla	k1gFnPc4	síla
s	s	k7c7	s
tendencí	tendence	k1gFnSc7	tendence
vtáhnout	vtáhnout	k5eAaPmF	vtáhnout
je	on	k3xPp3gInPc4	on
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
se	s	k7c7	s
silnějším	silný	k2eAgNnSc7d2	silnější
polem	pole	k1gNnSc7	pole
či	či	k8xC	či
vysunout	vysunout	k5eAaPmF	vysunout
je	on	k3xPp3gMnPc4	on
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
přerozdělením	přerozdělení	k1gNnSc7	přerozdělení
nábojů	náboj	k1gInPc2	náboj
a	a	k8xC	a
magnetických	magnetický	k2eAgInPc2d1	magnetický
dipólů	dipól	k1gInPc2	dipól
ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mikroskopické	mikroskopický	k2eAgFnPc1d1	mikroskopická
síly	síla	k1gFnPc1	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
síly	síla	k1gFnSc2	síla
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
pro	pro	k7c4	pro
mikroskopické	mikroskopický	k2eAgInPc4d1	mikroskopický
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
nutný	nutný	k2eAgInSc1d1	nutný
kvantově	kvantově	k6eAd1	kvantově
mechanický	mechanický	k2eAgInSc1d1	mechanický
popis	popis	k1gInSc1	popis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Mezimolekulové	Mezimolekulový	k2eAgFnPc1d1	Mezimolekulový
síly	síla	k1gFnPc1	síla
====	====	k?	====
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
kohezní	kohezní	k2eAgFnPc4d1	kohezní
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgInPc4d1	zvaný
též	též	k9	též
van	van	k1gInSc4	van
der	drát	k5eAaImRp2nS	drát
Waalsovy	Waalsův	k2eAgFnSc2d1	Waalsova
síly	síla	k1gFnPc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgInP	založit
na	na	k7c6	na
elektromagnetické	elektromagnetický	k2eAgFnSc6d1	elektromagnetická
interakci	interakce	k1gFnSc6	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
přiblížit	přiblížit	k5eAaPmF	přiblížit
zbytkovými	zbytkový	k2eAgFnPc7d1	zbytková
(	(	kIx(	(
<g/>
dipólovými	dipólová	k1gFnPc7	dipólová
a	a	k8xC	a
multipólovými	multipólová	k1gFnPc7	multipólová
<g/>
)	)	kIx)	)
elektrostatickými	elektrostatický	k2eAgFnPc7d1	elektrostatická
silami	síla	k1gFnPc7	síla
(	(	kIx(	(
<g/>
molekuly	molekula	k1gFnPc1	molekula
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
elektricky	elektricky	k6eAd1	elektricky
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozložení	rozložení	k1gNnSc1	rozložení
náboje	náboj	k1gInSc2	náboj
není	být	k5eNaImIp3nS	být
rovnoměrné	rovnoměrný	k2eAgNnSc1d1	rovnoměrné
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plný	plný	k2eAgInSc1d1	plný
výklad	výklad	k1gInSc1	výklad
je	být	k5eAaImIp3nS	být
však	však	k9	však
možný	možný	k2eAgInSc1d1	možný
až	až	k9	až
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zodpovědné	zodpovědný	k2eAgInPc1d1	zodpovědný
za	za	k7c4	za
kondenzovaný	kondenzovaný	k2eAgInSc4d1	kondenzovaný
stav	stav	k1gInSc4	stav
kapalin	kapalina	k1gFnPc2	kapalina
a	a	k8xC	a
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
s	s	k7c7	s
molekulovými	molekulový	k2eAgFnPc7d1	molekulová
vazbami	vazba	k1gFnPc7	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Projevují	projevovat	k5eAaImIp3nP	projevovat
se	se	k3xPyFc4	se
i	i	k9	i
u	u	k7c2	u
reálných	reálný	k2eAgInPc2d1	reálný
plynů	plyn	k1gInPc2	plyn
jako	jako	k8xC	jako
jejich	jejich	k3xOp3gInSc1	jejich
kohezní	kohezní	k2eAgInSc1d1	kohezní
tlak	tlak	k1gInSc1	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
makroskopickým	makroskopický	k2eAgInSc7d1	makroskopický
projevem	projev	k1gInSc7	projev
je	být	k5eAaImIp3nS	být
přilnavost	přilnavost	k1gFnSc1	přilnavost
(	(	kIx(	(
<g/>
adheze	adheze	k1gFnSc1	adheze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
druh	druh	k1gInSc1	druh
mezimolekulových	mezimolekulův	k2eAgFnPc2d1	mezimolekulův
sil	síla	k1gFnPc2	síla
se	se	k3xPyFc4	se
vyčleňují	vyčleňovat	k5eAaImIp3nP	vyčleňovat
relativně	relativně	k6eAd1	relativně
silnější	silný	k2eAgFnPc1d2	silnější
síly	síla	k1gFnPc1	síla
vodíkových	vodíkový	k2eAgInPc2d1	vodíkový
můstků	můstek	k1gInPc2	můstek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
přibližují	přibližovat	k5eAaImIp3nP	přibližovat
kovalentním	kovalentní	k2eAgFnPc3d1	kovalentní
meziatomovým	meziatomový	k2eAgFnPc3d1	meziatomová
silám	síla	k1gFnPc3	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Síly	síla	k1gFnPc4	síla
mezi	mezi	k7c7	mezi
atomy	atom	k1gInPc7	atom
a	a	k8xC	a
ionty	ion	k1gInPc7	ion
====	====	k?	====
</s>
</p>
<p>
<s>
Níže	nízce	k6eAd2	nízce
uvedené	uvedený	k2eAgFnPc1d1	uvedená
síly	síla	k1gFnPc1	síla
nemají	mít	k5eNaImIp3nP	mít
pojmově	pojmově	k6eAd1	pojmově
přesné	přesný	k2eAgNnSc4d1	přesné
vymezení	vymezení	k1gNnSc4	vymezení
hranic	hranice	k1gFnPc2	hranice
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
otázkou	otázka	k1gFnSc7	otázka
konvence	konvence	k1gFnSc2	konvence
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přechod	přechod	k1gInSc1	přechod
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
je	být	k5eAaImIp3nS	být
svou	svůj	k3xOyFgFnSc7	svůj
podstatou	podstata	k1gFnSc7	podstata
plynulý	plynulý	k2eAgMnSc1d1	plynulý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
síly	síla	k1gFnPc4	síla
mezi	mezi	k7c7	mezi
atomy	atom	k1gInPc7	atom
v	v	k7c6	v
molekulách	molekula	k1gFnPc6	molekula
a	a	k8xC	a
krystalech	krystal	k1gInPc6	krystal
<g/>
,	,	kIx,	,
projevující	projevující	k2eAgFnSc2d1	projevující
se	se	k3xPyFc4	se
jako	jako	k9	jako
chemická	chemický	k2eAgFnSc1d1	chemická
vazba	vazba	k1gFnSc1	vazba
(	(	kIx(	(
<g/>
kovalentní	kovalentní	k2eAgFnPc1d1	kovalentní
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
koordinačně-kovalentní	koordinačněovalentní	k2eAgFnPc1d1	koordinačně-kovalentní
síly	síla	k1gFnPc1	síla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vazbu	vazba	k1gFnSc4	vazba
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
kvantová	kvantový	k2eAgFnSc1d1	kvantová
mechanika	mechanika	k1gFnSc1	mechanika
na	na	k7c6	na
principu	princip	k1gInSc6	princip
sdílení	sdílení	k1gNnSc2	sdílení
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
extrémně	extrémně	k6eAd1	extrémně
polární	polární	k2eAgFnSc2d1	polární
vazby	vazba	k1gFnSc2	vazba
jsou	být	k5eAaImIp3nP	být
valenční	valenční	k2eAgInPc4d1	valenční
elektrony	elektron	k1gInPc4	elektron
obou	dva	k4xCgInPc2	dva
vázaných	vázaný	k2eAgInPc2d1	vázaný
atomů	atom	k1gInPc2	atom
součástí	součást	k1gFnPc2	součást
elektronového	elektronový	k2eAgInSc2d1	elektronový
obalu	obal	k1gInSc2	obal
pouze	pouze	k6eAd1	pouze
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
atomů	atom	k1gInPc2	atom
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
víceatomových	víceatomový	k2eAgFnPc2d1	víceatomový
částí	část	k1gFnPc2	část
molekuly	molekula	k1gFnSc2	molekula
<g/>
)	)	kIx)	)
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
ionty	ion	k1gInPc4	ion
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
vazbové	vazbový	k2eAgFnPc1d1	vazbová
síly	síla	k1gFnPc1	síla
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
síly	síla	k1gFnPc1	síla
iontové	iontový	k2eAgFnSc2d1	iontová
vazby	vazba	k1gFnSc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
síly	síla	k1gFnPc1	síla
lze	lze	k6eAd1	lze
zpravidla	zpravidla	k6eAd1	zpravidla
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
popsat	popsat	k5eAaPmF	popsat
elektrostatickými	elektrostatický	k2eAgFnPc7d1	elektrostatická
silami	síla	k1gFnPc7	síla
(	(	kIx(	(
<g/>
lze	lze	k6eAd1	lze
zanedbat	zanedbat	k5eAaPmF	zanedbat
kvantově	kvantově	k6eAd1	kvantově
mechanické	mechanický	k2eAgNnSc4d1	mechanické
chování	chování	k1gNnSc4	chování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
případem	případ	k1gInSc7	případ
jsou	být	k5eAaImIp3nP	být
vazebné	vazebný	k2eAgFnPc4d1	vazebná
síly	síla	k1gFnPc4	síla
v	v	k7c6	v
kovovém	kovový	k2eAgInSc6d1	kovový
krystalu	krystal	k1gInSc6	krystal
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
valenční	valenční	k2eAgInPc1d1	valenční
elektrony	elektron	k1gInPc1	elektron
atomy	atom	k1gInPc4	atom
jsou	být	k5eAaImIp3nP	být
sdíleny	sdílen	k2eAgInPc1d1	sdílen
všemi	všecek	k3xTgInPc7	všecek
atomy	atom	k1gInPc7	atom
<g/>
.	.	kIx.	.
</s>
<s>
Kovové	kovový	k2eAgInPc1d1	kovový
kationty	kation	k1gInPc1	kation
jsou	být	k5eAaImIp3nP	být
obklopeny	obklopen	k2eAgMnPc4d1	obklopen
elektronovým	elektronový	k2eAgInSc7d1	elektronový
plynem	plyn	k1gInSc7	plyn
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
silných	silný	k2eAgNnPc6d1	silné
magnetických	magnetický	k2eAgNnPc6d1	magnetické
polích	pole	k1gNnPc6	pole
(	(	kIx(	(
<g/>
105	[number]	k4	105
T	T	kA	T
<g/>
,	,	kIx,	,
přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
vyskytujících	vyskytující	k2eAgFnPc2d1	vyskytující
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
bílých	bílý	k2eAgMnPc2d1	bílý
trpaslíků	trpaslík	k1gMnPc2	trpaslík
i	i	k8xC	i
dalších	další	k2eAgInPc2d1	další
hvězdných	hvězdný	k2eAgInPc2d1	hvězdný
objektů	objekt	k1gInPc2	objekt
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
atomy	atom	k1gInPc1	atom
v	v	k7c6	v
molekulách	molekula	k1gFnPc6	molekula
vázány	vázat	k5eAaImNgFnP	vázat
i	i	k9	i
tzv.	tzv.	kA	tzv.
kolmou	kolmý	k2eAgFnSc7d1	kolmá
paramagnetickou	paramagnetický	k2eAgFnSc7d1	paramagnetická
vazbou	vazba	k1gFnSc7	vazba
<g/>
,	,	kIx,	,
obdobnou	obdobný	k2eAgFnSc7d1	obdobná
kovalentní	kovalentní	k2eAgFnSc6d1	kovalentní
vazbě	vazba	k1gFnSc6	vazba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
stabilizaci	stabilizace	k1gFnSc6	stabilizace
vazebných	vazebný	k2eAgInPc2d1	vazebný
orbitalů	orbital	k1gInPc2	orbital
kolmo	kolmo	k6eAd1	kolmo
k	k	k7c3	k
vnějšímu	vnější	k2eAgNnSc3d1	vnější
magnetickému	magnetický	k2eAgNnSc3d1	magnetické
poli	pole	k1gNnSc3	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Síly	síla	k1gFnPc1	síla
mezi	mezi	k7c7	mezi
částicemi	částice	k1gFnPc7	částice
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
====	====	k?	====
</s>
</p>
<p>
<s>
U	u	k7c2	u
jaderných	jaderný	k2eAgFnPc2d1	jaderná
sil	síla	k1gFnPc2	síla
mezi	mezi	k7c7	mezi
nukleony	nukleon	k1gInPc7	nukleon
<g/>
,	,	kIx,	,
držících	držící	k2eAgFnPc6d1	držící
je	on	k3xPp3gNnSc4	on
vázané	vázané	k1gNnSc4	vázané
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
(	(	kIx(	(
<g/>
i	i	k9	i
přes	přes	k7c4	přes
elektrostatické	elektrostatický	k2eAgNnSc4d1	elektrostatické
odpuzování	odpuzování	k1gNnSc4	odpuzování
protonů	proton	k1gInPc2	proton
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
zbytkové	zbytkový	k2eAgNnSc4d1	zbytkové
barevné	barevný	k2eAgNnSc4d1	barevné
působení	působení	k1gNnSc4	působení
(	(	kIx(	(
<g/>
silnou	silný	k2eAgFnSc4d1	silná
interakci	interakce	k1gFnSc4	interakce
<g/>
)	)	kIx)	)
mezi	mezi	k7c4	mezi
hadrony	hadron	k1gInPc4	hadron
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
barevné	barevný	k2eAgFnSc2d1	barevná
interakce	interakce	k1gFnSc2	interakce
působí	působit	k5eAaImIp3nP	působit
mezi	mezi	k7c7	mezi
kvarky	kvark	k1gInPc7	kvark
uvnitř	uvnitř	k7c2	uvnitř
hadronů	hadron	k1gInPc2	hadron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
krátkodosahové	krátkodosahový	k2eAgFnPc1d1	krátkodosahový
přitažlivé	přitažlivý	k2eAgFnPc1d1	přitažlivá
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
plný	plný	k2eAgInSc1d1	plný
výklad	výklad	k1gInSc1	výklad
je	být	k5eAaImIp3nS	být
však	však	k9	však
možný	možný	k2eAgInSc1d1	možný
až	až	k9	až
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
teorii	teorie	k1gFnSc6	teorie
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lze	lze	k6eAd1	lze
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
představit	představit	k5eAaPmF	představit
jako	jako	k9	jako
jistou	jistý	k2eAgFnSc4d1	jistá
analogii	analogie	k1gFnSc4	analogie
molekulových	molekulový	k2eAgFnPc2d1	molekulová
krystalových	krystalový	k2eAgFnPc2d1	krystalová
vazeb	vazba	k1gFnPc2	vazba
<g/>
,	,	kIx,	,
tvořených	tvořený	k2eAgInPc2d1	tvořený
zbytkovou	zbytkový	k2eAgFnSc7d1	zbytková
elektromagnetickou	elektromagnetický	k2eAgFnSc7d1	elektromagnetická
interakcí	interakce	k1gFnSc7	interakce
mezi	mezi	k7c7	mezi
molekulami	molekula	k1gFnPc7	molekula
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
interakce	interakce	k1gFnSc1	interakce
působí	působit	k5eAaImIp3nS	působit
mezi	mezi	k7c7	mezi
elektrony	elektron	k1gInPc7	elektron
a	a	k8xC	a
jádry	jádro	k1gNnPc7	jádro
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
atomů	atom	k1gInPc2	atom
tvořících	tvořící	k2eAgInPc2d1	tvořící
molekulu	molekula	k1gFnSc4	molekula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příbuzné	příbuzný	k2eAgFnPc1d1	příbuzná
veličiny	veličina	k1gFnPc1	veličina
==	==	k?	==
</s>
</p>
<p>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Tento	tento	k3xDgInSc1	tento
přehled	přehled	k1gInSc1	přehled
uvádí	uvádět	k5eAaImIp3nS	uvádět
pouze	pouze	k6eAd1	pouze
stručné	stručný	k2eAgFnPc4d1	stručná
charakteristiky	charakteristika	k1gFnPc4	charakteristika
a	a	k8xC	a
jednotky	jednotka	k1gFnPc4	jednotka
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
veličin	veličina	k1gFnPc2	veličina
blízkých	blízký	k2eAgFnPc2d1	blízká
síle	síla	k1gFnSc3	síla
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
spíše	spíše	k9	spíše
jako	jako	k9	jako
rozcestník	rozcestník	k1gInSc4	rozcestník
ke	k	k7c3	k
speciálním	speciální	k2eAgFnPc3d1	speciální
stránkám	stránka	k1gFnPc3	stránka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Objemová	objemový	k2eAgFnSc1d1	objemová
síla	síla	k1gFnSc1	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Objemová	objemový	k2eAgFnSc1d1	objemová
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
hustota	hustota	k1gFnSc1	hustota
síly	síla	k1gFnSc2	síla
působící	působící	k2eAgFnSc2d1	působící
v	v	k7c6	v
objemu	objem	k1gInSc6	objem
tělesa	těleso	k1gNnSc2	těleso
a	a	k8xC	a
definuje	definovat	k5eAaBmIp3nS	definovat
se	s	k7c7	s
vztahem	vztah	k1gInSc7	vztah
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
lim	limo	k1gNnPc2	limo
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
V	V	kA	V
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
V	V	kA	V
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
síla	síla	k1gFnSc1	síla
působící	působící	k2eAgFnSc1d1	působící
na	na	k7c4	na
objem	objem	k1gInSc4	objem
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V	V	kA	V
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
limita	limita	k1gFnSc1	limita
se	se	k3xPyFc4	se
míní	mínit	k5eAaImIp3nS	mínit
v	v	k7c6	v
makroskopickém	makroskopický	k2eAgInSc6d1	makroskopický
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotkou	jednotka	k1gFnSc7	jednotka
v	v	k7c6	v
SI	si	k1gNnSc6	si
je	být	k5eAaImIp3nS	být
newton	newton	k1gInSc1	newton
na	na	k7c4	na
metr	metr	k1gInSc4	metr
krychlový	krychlový	k2eAgInSc4d1	krychlový
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
/	/	kIx~	/
<g/>
m3	m3	k4	m3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Práce	práce	k1gFnPc1	práce
===	===	k?	===
</s>
</p>
<p>
<s>
Dráhový	dráhový	k2eAgInSc1d1	dráhový
účinek	účinek	k1gInSc1	účinek
působící	působící	k2eAgFnSc2d1	působící
síly	síla	k1gFnSc2	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Definuje	definovat	k5eAaBmIp3nS	definovat
se	se	k3xPyFc4	se
vztahem	vztah	k1gInSc7	vztah
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
W	W	kA	W
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
∫	∫	k?	∫
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
W	W	kA	W
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
s_	s_	k?	s_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
integrace	integrace	k1gFnSc2	integrace
podél	podél	k7c2	podél
dráhy	dráha	k1gFnSc2	dráha
pohybu	pohyb	k1gInSc2	pohyb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Jednotkou	jednotka	k1gFnSc7	jednotka
v	v	k7c6	v
SI	si	k1gNnSc6	si
je	být	k5eAaImIp3nS	být
joule	joule	k1gInSc1	joule
(	(	kIx(	(
<g/>
J	J	kA	J
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Impuls	impuls	k1gInSc1	impuls
(	(	kIx(	(
<g/>
síly	síla	k1gFnPc1	síla
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Časový	časový	k2eAgInSc1d1	časový
účinek	účinek	k1gInSc1	účinek
působící	působící	k2eAgFnSc2d1	působící
síly	síla	k1gFnSc2	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
impuls	impuls	k1gInSc1	impuls
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Definuje	definovat	k5eAaBmIp3nS	definovat
se	se	k3xPyFc4	se
vztahem	vztah	k1gInSc7	vztah
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
I	i	k9	i
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
∫	∫	k?	∫
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
I	I	kA	I
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
<g/>
Impuls	impuls	k1gInSc1	impuls
síly	síla	k1gFnSc2	síla
působící	působící	k2eAgFnPc1d1	působící
na	na	k7c4	na
volné	volný	k2eAgNnSc4d1	volné
těleso	těleso	k1gNnSc4	těleso
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
změně	změna	k1gFnSc3	změna
jeho	jeho	k3xOp3gFnSc2	jeho
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotkou	jednotka	k1gFnSc7	jednotka
v	v	k7c6	v
SI	si	k1gNnSc6	si
je	být	k5eAaImIp3nS	být
newtonsekunda	newtonsekunda	k1gFnSc1	newtonsekunda
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
·	·	k?	·
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Moment	moment	k1gInSc1	moment
síly	síla	k1gFnSc2	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Míru	Míra	k1gFnSc4	Míra
otáčivých	otáčivý	k2eAgFnPc2d1	otáčivá
schopností	schopnost	k1gFnPc2	schopnost
síly	síla	k1gFnSc2	síla
udává	udávat	k5eAaImIp3nS	udávat
moment	moment	k1gInSc4	moment
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Moment	moment	k1gInSc1	moment
síly	síla	k1gFnSc2	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
síly	síla	k1gFnPc1	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
bodu	bod	k1gInSc3	bod
Q	Q	kA	Q
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
vztahem	vztah	k1gInSc7	vztah
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
Q	Q	kA	Q
<g/>
}	}	kIx)	}
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
polohový	polohový	k2eAgInSc1d1	polohový
vektor	vektor	k1gInSc1	vektor
působiště	působiště	k1gNnSc2	působiště
síly	síla	k1gFnSc2	síla
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
libovolného	libovolný	k2eAgInSc2d1	libovolný
jiného	jiný	k2eAgInSc2d1	jiný
bodu	bod	k1gInSc2	bod
na	na	k7c6	na
vektorové	vektorový	k2eAgFnSc6d1	vektorová
přímce	přímka	k1gFnSc6	přímka
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Q	Q	kA	Q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
Q	Q	kA	Q
<g/>
}	}	kIx)	}
}}	}}	k?	}}
</s>
</p>
<p>
<s>
polohový	polohový	k2eAgInSc1d1	polohový
vektor	vektor	k1gInSc1	vektor
bodu	bod	k1gInSc2	bod
Q.	Q.	kA	Q.
</s>
</p>
<p>
<s>
Jednotkou	jednotka	k1gFnSc7	jednotka
v	v	k7c6	v
SI	si	k1gNnSc6	si
je	být	k5eAaImIp3nS	být
newtonmetr	newtonmetr	k1gInSc1	newtonmetr
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
·	·	k?	·
<g/>
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Napětí	napětí	k1gNnSc1	napětí
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
působí	působit	k5eAaImIp3nS	působit
síla	síla	k1gFnSc1	síla
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
plochu	plocha	k1gFnSc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
o	o	k7c4	o
sílu	síla	k1gFnSc4	síla
působící	působící	k2eAgFnSc4d1	působící
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
nějakého	nějaký	k3yIgNnSc2	nějaký
tuhého	tuhý	k2eAgNnSc2d1	tuhé
tělesa	těleso	k1gNnSc2	těleso
nebo	nebo	k8xC	nebo
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
tekutiny	tekutina	k1gFnSc2	tekutina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sílu	síla	k1gFnSc4	síla
lze	lze	k6eAd1	lze
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
případě	případ	k1gInSc6	případ
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xC	jako
součin	součin	k1gInSc1	součin
napětí	napětí	k1gNnSc2	napětí
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
σ	σ	k?	σ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnPc6	sigma
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
obsahu	obsah	k1gInSc2	obsah
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
dané	daný	k2eAgFnPc1d1	daná
plochy	plocha	k1gFnPc1	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Složky	složka	k1gFnPc1	složka
síly	síla	k1gFnSc2	síla
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
působící	působící	k2eAgFnPc1d1	působící
na	na	k7c4	na
element	element	k1gInSc4	element
plochy	plocha	k1gFnSc2	plocha
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
psát	psát	k5eAaImF	psát
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
σ	σ	k?	σ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ν	ν	k?	ν
</s>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
F_	F_	k1gMnSc1	F_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnSc1	sigma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
ij	ij	k?	ij
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
σ	σ	k?	σ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnSc3	sigma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
ij	ij	k?	ij
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
tenzor	tenzor	k1gInSc1	tenzor
napětí	napětí	k1gNnSc2	napětí
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ν	ν	k?	ν
</s>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nu	nu	k9	nu
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
normála	normála	k1gFnSc1	normála
plochy	plocha	k1gFnSc2	plocha
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
vztah	vztah	k1gInSc1	vztah
zjednoduší	zjednodušet	k5eAaPmIp3nS	zjednodušet
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tenzor	tenzor	k1gInSc1	tenzor
napětí	napětí	k1gNnSc2	napětí
je	být	k5eAaImIp3nS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
skalárním	skalární	k2eAgInSc7d1	skalární
tlakem	tlak	k1gInSc7	tlak
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
p	p	k?	p
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
složka	složka	k1gFnSc1	složka
vektoru	vektor	k1gInSc2	vektor
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
kolmá	kolmý	k2eAgFnSc1d1	kolmá
k	k	k7c3	k
elementu	element	k1gInSc3	element
plochy	plocha	k1gFnSc2	plocha
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
na	na	k7c4	na
který	který	k3yQgInSc4	který
působí	působit	k5eAaImIp3nS	působit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
směr	směr	k1gInSc1	směr
vektoru	vektor	k1gInSc2	vektor
popisujícího	popisující	k2eAgInSc2d1	popisující
element	element	k1gInSc1	element
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
boldsymbol	boldsymbol	k1gInSc1	boldsymbol
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
má	mít	k5eAaImIp3nS	mít
směr	směr	k1gInSc4	směr
normály	normála	k1gFnSc2	normála
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
plošce	ploška	k1gFnSc3	ploška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotkou	jednotka	k1gFnSc7	jednotka
napětí	napětí	k1gNnSc2	napětí
i	i	k8xC	i
tlaku	tlak	k1gInSc2	tlak
v	v	k7c6	v
SI	si	k1gNnSc6	si
je	být	k5eAaImIp3nS	být
pascal	pascal	k1gInSc1	pascal
(	(	kIx(	(
<g/>
Pa	Pa	kA	Pa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
článku	článek	k1gInSc2	článek
byly	být	k5eAaImAgFnP	být
následující	následující	k2eAgFnPc1d1	následující
publikace	publikace	k1gFnPc1	publikace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
====	====	k?	====
Základní	základní	k2eAgFnSc7d1	základní
====	====	k?	====
</s>
</p>
<p>
<s>
Feynman	Feynman	k1gMnSc1	Feynman
R.	R.	kA	R.
P.	P.	kA	P.
<g/>
,	,	kIx,	,
Leighton	Leighton	k1gInSc1	Leighton
R.	R.	kA	R.
B.	B.	kA	B.
<g/>
,	,	kIx,	,
Sands	Sands	k1gInSc1	Sands
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Feynmanovy	Feynmanův	k2eAgFnPc4d1	Feynmanova
přednášky	přednáška	k1gFnPc4	přednáška
z	z	k7c2	z
fyziky	fyzika	k1gFnSc2	fyzika
-	-	kIx~	-
díl	díl	k1gInSc1	díl
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
české	český	k2eAgNnSc1d1	české
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7200	[number]	k4	7200
<g/>
-	-	kIx~	-
<g/>
405	[number]	k4	405
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Horák	Horák	k1gMnSc1	Horák
Z.	Z.	kA	Z.
<g/>
,	,	kIx,	,
Krupka	Krupka	k1gMnSc1	Krupka
F.	F.	kA	F.
<g/>
:	:	kIx,	:
Fyzika	fyzik	k1gMnSc2	fyzik
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
v	v	k7c6	v
koedici	koedice	k1gFnSc6	koedice
s	s	k7c7	s
ALFA	alfa	k1gNnSc7	alfa
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1981	[number]	k4	1981
</s>
</p>
<p>
<s>
Kvasnica	Kvasnica	k1gFnSc1	Kvasnica
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Havránek	Havránek	k1gMnSc1	Havránek
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Lukáč	Lukáč	k1gMnSc1	Lukáč
P.	P.	kA	P.
<g/>
,	,	kIx,	,
Sprušil	Sprušil	k1gMnSc1	Sprušil
B.	B.	kA	B.
<g/>
:	:	kIx,	:
Mechanika	mechanika	k1gFnSc1	mechanika
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1988	[number]	k4	1988
</s>
</p>
<p>
<s>
Kittel	Kittel	k1gMnSc1	Kittel
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Knight	Knight	k1gMnSc1	Knight
W.	W.	kA	W.
D.	D.	kA	D.
<g/>
,	,	kIx,	,
Ruderman	Ruderman	k1gMnSc1	Ruderman
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Mechanics	Mechanics	k1gInSc1	Mechanics
<g/>
.	.	kIx.	.
</s>
<s>
Berkley	Berklea	k1gFnPc1	Berklea
Physics	Physicsa	k1gFnPc2	Physicsa
Course	Course	k1gFnSc2	Course
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
McGraw-Hill	McGraw-Hill	k1gInSc1	McGraw-Hill
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1965	[number]	k4	1965
(	(	kIx(	(
<g/>
existuje	existovat	k5eAaImIp3nS	existovat
slovenský	slovenský	k2eAgInSc1d1	slovenský
překlad	překlad	k1gInSc1	překlad
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Friš	Friš	k1gMnSc1	Friš
S.	S.	kA	S.
E.	E.	kA	E.
<g/>
,	,	kIx,	,
Timorevová	Timorevový	k2eAgFnSc1d1	Timorevový
A.	A.	kA	A.
V.	V.	kA	V.
<g/>
:	:	kIx,	:
Kurs	kurs	k1gInSc1	kurs
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ČSAV	ČSAV	kA	ČSAV
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1962	[number]	k4	1962
</s>
</p>
<p>
<s>
====	====	k?	====
Odborná	odborný	k2eAgNnPc1d1	odborné
a	a	k8xC	a
speciální	speciální	k2eAgNnPc1d1	speciální
====	====	k?	====
</s>
</p>
<p>
<s>
Trkal	trkat	k5eAaImAgMnS	trkat
V.	V.	kA	V.
<g/>
:	:	kIx,	:
Mechanika	mechanika	k1gFnSc1	mechanika
hmotných	hmotný	k2eAgInPc2d1	hmotný
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
tuhého	tuhý	k2eAgNnSc2d1	tuhé
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ČSAV	ČSAV	kA	ČSAV
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1956	[number]	k4	1956
</s>
</p>
<p>
<s>
Brdička	Brdička	k1gFnSc1	Brdička
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Hladík	Hladík	k1gMnSc1	Hladík
A.	A.	kA	A.
<g/>
:	:	kIx,	:
Teoretická	teoretický	k2eAgFnSc1d1	teoretická
mechanika	mechanika	k1gFnSc1	mechanika
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1987	[number]	k4	1987
</s>
</p>
<p>
<s>
Horský	Horský	k1gMnSc1	Horský
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1975	[number]	k4	1975
</s>
</p>
<p>
<s>
Votruba	Votruba	k1gMnSc1	Votruba
V.	V.	kA	V.
<g/>
:	:	kIx,	:
Základy	základ	k1gInPc1	základ
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1977	[number]	k4	1977
</s>
</p>
<p>
<s>
Kuchař	Kuchař	k1gMnSc1	Kuchař
K.	K.	kA	K.
<g/>
:	:	kIx,	:
Základy	základ	k1gInPc1	základ
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1968	[number]	k4	1968
</s>
</p>
<p>
<s>
Formánek	Formánek	k1gMnSc1	Formánek
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
I	I	kA	I
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-200-1176-5	[number]	k4	80-200-1176-5
</s>
</p>
<p>
<s>
Šindelář	Šindelář	k1gMnSc1	Šindelář
V.	V.	kA	V.
<g/>
,	,	kIx,	,
Smrž	smrž	k1gInSc1	smrž
L.	L.	kA	L.
<g/>
,	,	kIx,	,
Beťák	Beťák	k1gMnSc1	Beťák
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
Nová	nový	k2eAgFnSc1d1	nová
soustava	soustava	k1gFnSc1	soustava
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1981	[number]	k4	1981
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dynamika	dynamika	k1gFnSc1	dynamika
</s>
</p>
<p>
<s>
Pohybová	pohybový	k2eAgFnSc1d1	pohybová
rovnice	rovnice	k1gFnSc1	rovnice
</s>
</p>
<p>
<s>
Hybnost	hybnost	k1gFnSc1	hybnost
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
interakce	interakce	k1gFnSc1	interakce
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
síla	síla	k1gFnSc1	síla
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
síla	síla	k1gFnSc1	síla
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
síla	síla	k1gFnSc1	síla
</s>
</p>
