<p>
<s>
Děvčátko	děvčátko	k1gNnSc1	děvčátko
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Benjamina	Benjamin	k1gMnSc2	Benjamin
Tučka	Tuček	k1gMnSc2	Tuček
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jeho	on	k3xPp3gInSc4	on
filmový	filmový	k2eAgInSc4d1	filmový
debut	debut	k1gInSc4	debut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Jana	Jana	k1gFnSc1	Jana
Hubinská	Hubinský	k2eAgFnSc1d1	Hubinská
získala	získat	k5eAaPmAgFnS	získat
Českého	český	k2eAgMnSc4d1	český
lva	lev	k1gMnSc4	lev
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
nominován	nominovat	k5eAaBmNgInS	nominovat
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
střih	střih	k1gInSc4	střih
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Děvčátko	děvčátko	k1gNnSc1	děvčátko
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Děvčátko	děvčátko	k1gNnSc1	děvčátko
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
