<p>
<s>
Rafael	Rafael	k1gMnSc1	Rafael
Pereira	Pereira	k1gMnSc1	Pereira
da	da	k?	da
Silva	Silva	k1gFnSc1	Silva
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Rafael	Rafael	k1gMnSc1	Rafael
nebo	nebo	k8xC	nebo
Rafael	Rafael	k1gMnSc1	Rafael
da	da	k?	da
Silva	Silva	k1gFnSc1	Silva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
brazilský	brazilský	k2eAgMnSc1d1	brazilský
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
obránce	obránce	k1gMnSc1	obránce
a	a	k8xC	a
reprezentant	reprezentant	k1gMnSc1	reprezentant
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
za	za	k7c4	za
Olympique	Olympique	k1gFnSc4	Olympique
Lyon	Lyon	k1gInSc4	Lyon
v	v	k7c6	v
Ligue	Ligue	k1gFnSc6	Ligue
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klubová	klubový	k2eAgFnSc1d1	klubová
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Rafael	Rafael	k1gMnSc1	Rafael
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Petrópolis	Petrópolis	k1gFnSc6	Petrópolis
<g/>
,	,	kIx,	,
asi	asi	k9	asi
hodinu	hodina	k1gFnSc4	hodina
jízdy	jízda	k1gFnSc2	jízda
autem	auto	k1gNnSc7	auto
od	od	k7c2	od
města	město	k1gNnSc2	město
Rio	Rio	k1gFnPc2	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc4	Janeiro
<g/>
.	.	kIx.	.
</s>
<s>
Rafael	Rafael	k1gMnSc1	Rafael
začal	začít	k5eAaPmAgMnS	začít
hrát	hrát	k5eAaImF	hrát
fotbal	fotbal	k1gInSc4	fotbal
od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
jednovaječné	jednovaječný	k2eAgNnSc4d1	jednovaječné
dvojče	dvojče	k1gNnSc4	dvojče
Fábio	Fábio	k6eAd1	Fábio
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
bok	bok	k1gInSc4	bok
po	po	k7c6	po
boku	bok	k1gInSc6	bok
od	od	k7c2	od
mala	mal	k1gInSc2	mal
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
jich	on	k3xPp3gFnPc2	on
všiml	všimnout	k5eAaPmAgMnS	všimnout
zástupce	zástupce	k1gMnSc1	zástupce
Fluminense	Fluminense	k1gFnSc2	Fluminense
a	a	k8xC	a
ten	ten	k3xDgInSc4	ten
jim	on	k3xPp3gMnPc3	on
dal	dát	k5eAaPmAgMnS	dát
možnost	možnost	k1gFnSc4	možnost
si	se	k3xPyFc3	se
hrát	hrát	k5eAaImF	hrát
za	za	k7c4	za
klub	klub	k1gInSc4	klub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
Manchesteru	Manchester	k1gInSc2	Manchester
United	United	k1gInSc1	United
<g/>
,	,	kIx,	,
kontrakt	kontrakt	k1gInSc1	kontrakt
byl	být	k5eAaImAgInS	být
podepsán	podepsat	k5eAaPmNgInS	podepsat
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
Olympique	Olympiqu	k1gFnSc2	Olympiqu
Lyon	Lyon	k1gInSc1	Lyon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úspěchy	úspěch	k1gInPc1	úspěch
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Klubové	klubový	k2eAgMnPc4d1	klubový
===	===	k?	===
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
United	United	k1gInSc1	United
</s>
</p>
<p>
<s>
Premier	Premier	k1gInSc1	Premier
League	Leagu	k1gFnSc2	Leagu
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
</s>
</p>
<p>
<s>
FA	fa	k1gNnSc1	fa
Community	Communita	k1gFnSc2	Communita
Shield	Shielda	k1gFnPc2	Shielda
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
ligový	ligový	k2eAgInSc1d1	ligový
pohár	pohár	k1gInSc1	pohár
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
klubů	klub	k1gInPc2	klub
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
===	===	k?	===
Reprezentační	reprezentační	k2eAgMnSc1d1	reprezentační
===	===	k?	===
</s>
</p>
<p>
<s>
Brazílie	Brazílie	k1gFnSc1	Brazílie
U-17	U-17	k1gFnSc2	U-17
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
U-17	U-17	k1gFnSc2	U-17
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Rafael	Rafael	k1gMnSc1	Rafael
Pereira	Pereira	k1gMnSc1	Pereira
da	da	k?	da
Silva	Silva	k1gFnSc1	Silva
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rafael	Rafaela	k1gFnPc2	Rafaela
Pereira	Pereir	k1gInSc2	Pereir
da	da	k?	da
Silva	Silva	k1gFnSc1	Silva
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
hráče	hráč	k1gMnSc2	hráč
na	na	k7c4	na
Transfermarkt	Transfermarkt	k1gInSc4	Transfermarkt
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
