<s>
Znásilňování	znásilňování	k1gNnSc1	znásilňování
chapadlem	chapadlo	k1gNnSc7	chapadlo
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
触	触	k?	触
<g/>
,	,	kIx,	,
šokušu	šokusat	k5eAaPmIp1nS	šokusat
gókan	gókan	k1gInSc1	gókan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
námět	námět	k1gInSc4	námět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
nacházet	nacházet	k5eAaImF	nacházet
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
hororových	hororový	k2eAgInPc6d1	hororový
hentai	hentai	k6eAd1	hentai
titulech	titul	k1gInPc6	titul
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
chapadlovité	chapadlovitý	k2eAgFnPc4d1	chapadlovitá
kreatury	kreatura	k1gFnPc4	kreatura
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
fiktivní	fiktivní	k2eAgNnPc1d1	fiktivní
monstra	monstrum	k1gNnPc1	monstrum
<g/>
)	)	kIx)	)
znásilňují	znásilňovat	k5eAaImIp3nP	znásilňovat
nebo	nebo	k8xC	nebo
nějak	nějak	k6eAd1	nějak
pronikají	pronikat	k5eAaImIp3nP	pronikat
do	do	k7c2	do
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
antropomorfní	antropomorfní	k2eAgFnPc1d1	antropomorfní
bytosti	bytost	k1gFnPc1	bytost
nebo	nebo	k8xC	nebo
futanari	futanar	k1gFnPc1	futanar
(	(	kIx(	(
<g/>
ženy	žena	k1gFnPc1	žena
s	s	k7c7	s
pohlavním	pohlavní	k2eAgInSc7d1	pohlavní
znakem	znak	k1gInSc7	znak
muže	muž	k1gMnSc2	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
