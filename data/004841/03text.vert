<s>
Znásilňování	znásilňování	k1gNnSc1	znásilňování
chapadlem	chapadlo	k1gNnSc7	chapadlo
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
触	触	k?	触
<g/>
,	,	kIx,	,
šokušu	šokusat	k5eAaPmIp1nS	šokusat
gókan	gókan	k1gInSc1	gókan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
námět	námět	k1gInSc4	námět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
nacházet	nacházet	k5eAaImF	nacházet
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
hororových	hororový	k2eAgInPc6d1	hororový
hentai	hentai	k6eAd1	hentai
titulech	titul	k1gInPc6	titul
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
chapadlovité	chapadlovitý	k2eAgFnPc4d1	chapadlovitá
kreatury	kreatura	k1gFnPc4	kreatura
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
fiktivní	fiktivní	k2eAgNnPc1d1	fiktivní
monstra	monstrum	k1gNnPc1	monstrum
<g/>
)	)	kIx)	)
znásilňují	znásilňovat	k5eAaImIp3nP	znásilňovat
nebo	nebo	k8xC	nebo
nějak	nějak	k6eAd1	nějak
pronikají	pronikat	k5eAaImIp3nP	pronikat
do	do	k7c2	do
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
antropomorfní	antropomorfní	k2eAgFnPc1d1	antropomorfní
bytosti	bytost	k1gFnPc1	bytost
nebo	nebo	k8xC	nebo
futanari	futanar	k1gFnPc1	futanar
(	(	kIx(	(
<g/>
ženy	žena	k1gFnPc1	žena
s	s	k7c7	s
pohlavním	pohlavní	k2eAgInSc7d1	pohlavní
znakem	znak	k1gInSc7	znak
muže	muž	k1gMnSc2	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žánr	žánr	k1gInSc1	žánr
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgInSc1d1	populární
v	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
erotickém	erotický	k2eAgInSc6d1	erotický
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
také	také	k9	také
je	být	k5eAaImIp3nS	být
terčem	terč	k1gInSc7	terč
mnoha	mnoho	k4c2	mnoho
parodií	parodie	k1gFnPc2	parodie
<g/>
.	.	kIx.	.
</s>
<s>
Manga	mango	k1gNnPc1	mango
s	s	k7c7	s
názvem	název	k1gInSc7	název
Urocukidódži	Urocukidódž	k1gFnSc3	Urocukidódž
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
od	od	k7c2	od
Tošio	Tošio	k6eAd1	Tošio
Maedy	Maed	k1gInPc7	Maed
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
moderní	moderní	k2eAgInSc4d1	moderní
příklad	příklad	k1gInSc4	příklad
chapadlového	chapadlový	k2eAgNnSc2d1	chapadlový
porna	porno	k1gNnSc2	porno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
sexuální	sexuální	k2eAgNnSc4d1	sexuální
násilí	násilí	k1gNnSc4	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Maeda	Maeda	k1gFnSc1	Maeda
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
obejít	obejít	k5eAaPmF	obejít
normy	norma	k1gFnPc4	norma
japonské	japonský	k2eAgFnSc2d1	japonská
cenzury	cenzura	k1gFnSc2	cenzura
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
ochlupení	ochlupení	k1gNnSc4	ochlupení
a	a	k8xC	a
penisy	penis	k1gInPc4	penis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
se	se	k3xPyFc4	se
nikde	nikde	k6eAd1	nikde
nezakazuje	zakazovat	k5eNaImIp3nS	zakazovat
zobrazování	zobrazování	k1gNnSc4	zobrazování
sexuálních	sexuální	k2eAgInPc2d1	sexuální
styků	styk	k1gInPc2	styk
s	s	k7c7	s
chapadly	chapadlo	k1gNnPc7	chapadlo
<g/>
,	,	kIx,	,
s	s	k7c7	s
roboty	robot	k1gMnPc7	robot
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Tento	tento	k3xDgInSc4	tento
žánr	žánr	k1gInSc4	žánr
je	být	k5eAaImIp3nS	být
zobrazován	zobrazovat	k5eAaImNgMnS	zobrazovat
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
filmových	filmový	k2eAgNnPc6d1	filmové
dílech	dílo	k1gNnPc6	dílo
Alien	Alien	k2eAgMnSc1d1	Alien
from	from	k1gMnSc1	from
the	the	k?	the
Darkness	Darkness	k1gInSc1	Darkness
a	a	k8xC	a
Advancer	Advancer	k1gInSc1	Advancer
Tina	Tina	k1gFnSc1	Tina
-	-	kIx~	-
filmy	film	k1gInPc1	film
typu	typ	k1gInSc2	typ
hentai	henta	k1gFnSc2	henta
o	o	k7c6	o
ženských	ženský	k2eAgFnPc6d1	ženská
dobrodružkách	dobrodružka	k1gFnPc6	dobrodružka
<g/>
,	,	kIx,	,
bojující	bojující	k2eAgMnSc1d1	bojující
s	s	k7c7	s
chapadlovitými	chapadlovitý	k2eAgMnPc7d1	chapadlovitý
vetřelci	vetřelec	k1gMnPc7	vetřelec
První	první	k4xOgInPc4	první
bakunyuu	bakunyuu	k6eAd1	bakunyuu
díly	díl	k1gInPc4	díl
Sexy	sex	k1gInPc4	sex
Sailor	Sailor	k1gMnSc1	Sailor
Soldiers	Soldiers	k1gInSc4	Soldiers
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgNnPc6	který
jsou	být	k5eAaImIp3nP	být
ženy	žena	k1gFnPc1	žena
znásilněny	znásilněn	k2eAgFnPc1d1	znásilněna
chapadlovitými	chapadlovitý	k2eAgNnPc7d1	chapadlovitý
monstry	monstrum	k1gNnPc7	monstrum
<g/>
.	.	kIx.	.
</s>
<s>
Blood	Blood	k1gInSc1	Blood
Royal	Royal	k1gInSc1	Royal
Princess	Princess	k1gInSc4	Princess
-	-	kIx~	-
hentai	hentai	k6eAd1	hentai
o	o	k7c6	o
pirátovi	pirát	k1gMnSc6	pirát
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
umí	umět	k5eAaImIp3nS	umět
ovládat	ovládat	k5eAaImF	ovládat
chobotnici	chobotnice	k1gFnSc4	chobotnice
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
znásilnila	znásilnit	k5eAaPmAgFnS	znásilnit
dvě	dva	k4xCgFnPc4	dva
princezny	princezna	k1gFnPc4	princezna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
následně	následně	k6eAd1	následně
trénovány	trénován	k2eAgFnPc1d1	trénována
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
sexuálními	sexuální	k2eAgMnPc7d1	sexuální
otroky	otrok	k1gMnPc7	otrok
<g/>
.	.	kIx.	.
</s>
<s>
La	la	k1gNnSc1	la
Blue	Blu	k1gInSc2	Blu
Girl	girl	k1gFnSc2	girl
je	být	k5eAaImIp3nS	být
nechvalně	chvalně	k6eNd1	chvalně
proslulé	proslulý	k2eAgNnSc4d1	proslulé
hentai	hentai	k1gNnSc4	hentai
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
odmítnuto	odmítnout	k5eAaPmNgNnS	odmítnout
britskou	britský	k2eAgFnSc7d1	britská
BBFC	BBFC	kA	BBFC
(	(	kIx(	(
<g/>
British	British	k1gMnSc1	British
Board	Board	k1gMnSc1	Board
of	of	k?	of
Film	film	k1gInSc1	film
Classification	Classification	k1gInSc1	Classification
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Injukyoshi	Injukyoshi	k1gNnSc1	Injukyoshi
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Obscene	Obscen	k1gMnSc5	Obscen
Beast	Beast	k1gFnSc1	Beast
Teacher	Teachra	k1gFnPc2	Teachra
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
či	či	k8xC	či
Angel	angel	k1gMnSc1	angel
of	of	k?	of
Darkness	Darkness	k1gInSc1	Darkness
–	–	k?	–
Čtyřdílná	čtyřdílný	k2eAgFnSc1d1	čtyřdílná
hentai	henta	k1gFnSc2	henta
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
adoptovaná	adoptovaný	k2eAgFnSc1d1	adoptovaná
do	do	k7c2	do
hraného	hraný	k2eAgInSc2d1	hraný
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Znásilňování	znásilňování	k1gNnSc1	znásilňování
chapadlem	chapadlo	k1gNnSc7	chapadlo
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
předmětem	předmět	k1gInSc7	předmět
parodie	parodie	k1gFnSc2	parodie
<g/>
:	:	kIx,	:
Skeč	skeč	k1gInSc1	skeč
v	v	k7c4	v
Robot	robot	k1gInSc4	robot
Chicken	Chicken	k2eAgInSc4d1	Chicken
<g/>
,	,	kIx,	,
epizoda	epizoda	k1gFnSc1	epizoda
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
Maurice	Maurika	k1gFnSc6	Maurika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Chapadlovité	chapadlovitý	k2eAgNnSc1d1	chapadlovitý
monstrum	monstrum	k1gNnSc1	monstrum
zavolá	zavolat	k5eAaPmIp3nS	zavolat
školačku	školačka	k1gFnSc4	školačka
a	a	k8xC	a
zeptá	zeptat	k5eAaPmIp3nS	zeptat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
jestli	jestli	k8xS	jestli
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
půjde	jít	k5eAaImIp3nS	jít
ven	ven	k6eAd1	ven
na	na	k7c4	na
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ona	onen	k3xDgFnSc1	onen
mu	on	k3xPp3gMnSc3	on
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Mnemosyne	Mnemosynout	k5eAaPmIp3nS	Mnemosynout
LLC	LLC	kA	LLC
zveřejnili	zveřejnit	k5eAaPmAgMnP	zveřejnit
hroznově	hroznově	k6eAd1	hroznově
ochucený	ochucený	k2eAgInSc4d1	ochucený
nápoj	nápoj	k1gInSc4	nápoj
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
'	'	kIx"	'
<g/>
Tentacle	Tentacle	k1gFnSc1	Tentacle
Grape	Grape	k?	Grape
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
tentacle	tentacle	k6eAd1	tentacle
znamená	znamenat	k5eAaImIp3nS	znamenat
chapadlo	chapadlo	k1gNnSc1	chapadlo
<g/>
,	,	kIx,	,
grape	grape	k?	grape
anglicky	anglicky	k6eAd1	anglicky
znamená	znamenat	k5eAaImIp3nS	znamenat
hrozno	hrozna	k1gFnSc5	hrozna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgNnSc1d1	podobné
s	s	k7c7	s
rape	rape	k1gNnSc7	rape
(	(	kIx(	(
<g/>
znásilnění	znásilnění	k1gNnPc2	znásilnění
<g/>
))	))	k?	))
Znásilňování	znásilňování	k1gNnPc2	znásilňování
chapadlem	chapadlo	k1gNnSc7	chapadlo
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
objevilo	objevit	k5eAaPmAgNnS	objevit
i	i	k9	i
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
hraném	hraný	k2eAgInSc6d1	hraný
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
Galaxy	Galax	k1gInPc4	Galax
of	of	k?	of
Terror	Terror	k1gInSc1	Terror
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
postava	postava	k1gFnSc1	postava
Dameia	Dameia	k1gFnSc1	Dameia
(	(	kIx(	(
<g/>
Taaffe	Taaff	k1gInSc5	Taaff
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Connell	Connell	k1gInSc4	Connell
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
znásilněna	znásilnit	k5eAaPmNgFnS	znásilnit
velkým	velký	k2eAgNnSc7d1	velké
zeleným	zelené	k1gNnSc7	zelené
červem	červ	k1gMnSc7	červ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Tentacle	Tentacl	k1gMnSc2	Tentacl
erotica	eroticus	k1gMnSc2	eroticus
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
