<s>
Po	po	k7c6	po
Cimrmanovi	Cimrman	k1gMnSc6	Cimrman
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
planetka	planetka	k1gFnSc1	planetka
Járacimrman	Járacimrman	k1gMnSc1	Járacimrman
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
ztracená	ztracený	k2eAgFnSc1d1	ztracená
a	a	k8xC	a
znovuobjevená	znovuobjevený	k2eAgFnSc1d1	znovuobjevená
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
dříve	dříve	k6eAd2	dříve
zapomenutý	zapomenutý	k2eAgMnSc1d1	zapomenutý
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
<g/>
.	.	kIx.	.
</s>
