<s>
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
je	být	k5eAaImIp3nS	být
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
postava	postava	k1gFnSc1	postava
univerzálního	univerzální	k2eAgMnSc2d1	univerzální
českého	český	k2eAgMnSc2d1	český
génia	génius	k1gMnSc2	génius
<g/>
,	,	kIx,	,
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
Jiřím	Jiří	k1gMnSc7	Jiří
Šebánkem	Šebánek	k1gMnSc7	Šebánek
a	a	k8xC	a
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Svěrákem	Svěrák	k1gMnSc7	Svěrák
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
v	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
pořadu	pořad	k1gInSc6	pořad
Vinárna	vinárna	k1gFnSc1	vinárna
U	u	k7c2	u
Pavouka	pavouk	k1gMnSc2	pavouk
v	v	k7c6	v
Československém	československý	k2eAgInSc6d1	československý
rozhlase	rozhlas	k1gInSc6	rozhlas
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
řidič	řidič	k1gMnSc1	řidič
parního	parní	k2eAgInSc2d1	parní
válce	válec	k1gInSc2	válec
a	a	k8xC	a
sochař	sochař	k1gMnSc1	sochař
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
autory	autor	k1gMnPc7	autor
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
založeno	založit	k5eAaPmNgNnS	založit
celé	celý	k2eAgNnSc1d1	celé
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
dodnes	dodnes	k6eAd1	dodnes
uvádí	uvádět	k5eAaImIp3nS	uvádět
hry	hra	k1gFnPc1	hra
přisuzované	přisuzovaný	k2eAgFnPc1d1	přisuzovaná
Járovi	Jára	k1gMnSc3	Jára
Cimrmanovi	Cimrman	k1gMnSc3	Cimrman
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zpracovávalo	zpracovávat	k5eAaImAgNnS	zpracovávat
fiktivní	fiktivní	k2eAgInSc4d1	fiktivní
život	život	k1gInSc4	život
a	a	k8xC	a
dílo	dílo	k1gNnSc4	dílo
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
popisován	popisován	k2eAgMnSc1d1	popisován
jako	jako	k8xC	jako
génius	génius	k1gMnSc1	génius
a	a	k8xC	a
všeuměl	všeuměl	k1gMnSc1	všeuměl
<g/>
.	.	kIx.	.
</s>
<s>
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
kultuře	kultura	k1gFnSc6	kultura
značný	značný	k2eAgInSc4d1	značný
ohlas	ohlas	k1gInSc4	ohlas
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
získal	získat	k5eAaPmAgInS	získat
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
počet	počet	k1gInSc1	počet
hlasů	hlas	k1gInPc2	hlas
v	v	k7c6	v
celonárodní	celonárodní	k2eAgFnSc6d1	celonárodní
anketě	anketa	k1gFnSc6	anketa
Největší	veliký	k2eAgMnSc1d3	veliký
Čech	Čech	k1gMnSc1	Čech
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
existenci	existence	k1gFnSc6	existence
českého	český	k2eAgMnSc2d1	český
velikána	velikán	k1gMnSc2	velikán
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1966	[number]	k4	1966
v	v	k7c6	v
humoristickém	humoristický	k2eAgInSc6d1	humoristický
pořadu	pořad	k1gInSc6	pořad
(	(	kIx(	(
<g/>
Nealkoholická	alkoholický	k2eNgFnSc1d1	nealkoholická
<g/>
)	)	kIx)	)
Vinárna	vinárna	k1gFnSc1	vinárna
U	u	k7c2	u
Pavouka	pavouk	k1gMnSc2	pavouk
v	v	k7c6	v
Československém	československý	k2eAgInSc6d1	československý
rozhlase	rozhlas	k1gInSc6	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
jako	jako	k8xC	jako
řidič	řidič	k1gInSc1	řidič
parního	parní	k2eAgInSc2d1	parní
válce	válec	k1gInSc2	válec
a	a	k8xC	a
sochař	sochař	k1gMnSc1	sochař
<g/>
,	,	kIx,	,
neúspěšně	úspěšně	k6eNd1	úspěšně
vystavující	vystavující	k2eAgMnPc1d1	vystavující
v	v	k7c6	v
nafukovacím	nafukovací	k2eAgInSc6d1	nafukovací
pavilonu	pavilon	k1gInSc6	pavilon
předměty	předmět	k1gInPc4	předmět
zdeformované	zdeformovaný	k2eAgInPc4d1	zdeformovaný
jejich	jejich	k3xOp3gNnSc7	jejich
přejetím	přejetí	k1gNnSc7	přejetí
parním	parní	k2eAgInSc7d1	parní
válcem	válec	k1gInSc7	válec
<g/>
.	.	kIx.	.
</s>
<s>
Reportáž	reportáž	k1gFnSc4	reportáž
vedl	vést	k5eAaImAgMnS	vést
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
jeho	jeho	k3xOp3gInSc2	jeho
údajného	údajný	k2eAgInSc2d1	údajný
života	život	k1gInSc2	život
a	a	k8xC	a
díla	dílo	k1gNnSc2	dílo
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
repertoáru	repertoár	k1gInSc2	repertoár
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Cimrman	Cimrman	k1gMnSc1	Cimrman
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
zřejmě	zřejmě	k6eAd1	zřejmě
Jiří	Jiří	k1gMnSc1	Jiří
Šebánek	Šebánek	k1gMnSc1	Šebánek
podle	podle	k7c2	podle
chomutovského	chomutovský	k2eAgMnSc2d1	chomutovský
hokejisty	hokejista	k1gMnSc2	hokejista
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
jméno	jméno	k1gNnSc4	jméno
slyšel	slyšet	k5eAaImAgMnS	slyšet
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
a	a	k8xC	a
zalíbilo	zalíbit	k5eAaPmAgNnS	zalíbit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Křestní	křestní	k2eAgNnSc1d1	křestní
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Jára	Jára	k1gFnSc1	Jára
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vzbuzovalo	vzbuzovat	k5eAaImAgNnS	vzbuzovat
zdání	zdání	k1gNnSc1	zdání
uměleckosti	uměleckost	k1gFnSc2	uměleckost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
hlavní	hlavní	k2eAgInPc1d1	hlavní
předobrazy	předobraz	k1gInPc1	předobraz
samotné	samotný	k2eAgFnSc2d1	samotná
postavy	postava	k1gFnSc2	postava
uvádí	uvádět	k5eAaImIp3nP	uvádět
autoři	autor	k1gMnPc1	autor
dobrušského	dobrušský	k2eAgMnSc2d1	dobrušský
technologického	technologický	k2eAgMnSc2d1	technologický
nadšence	nadšenec	k1gMnSc2	nadšenec
a	a	k8xC	a
naivistického	naivistický	k2eAgMnSc2d1	naivistický
malíře	malíř	k1gMnSc2	malíř
Aloise	Alois	k1gMnSc4	Alois
Beera	Beer	k1gMnSc4	Beer
(	(	kIx(	(
<g/>
1833	[number]	k4	1833
<g/>
–	–	k?	–
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
okrajového	okrajový	k2eAgMnSc2d1	okrajový
moravského	moravský	k2eAgMnSc2d1	moravský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Václava	Václav	k1gMnSc2	Václav
Svobodu	svoboda	k1gFnSc4	svoboda
Plumlovského	plumlovský	k2eAgMnSc2d1	plumlovský
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gymnaziálního	gymnaziální	k2eAgMnSc2d1	gymnaziální
profesora	profesor	k1gMnSc2	profesor
<g/>
,	,	kIx,	,
amatérského	amatérský	k2eAgMnSc2d1	amatérský
filosofa	filosof	k1gMnSc2	filosof
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc2	vynálezce
Jakuba	Jakub	k1gMnSc2	Jakub
Hrona	Hron	k1gMnSc2	Hron
Metánovského	Metánovský	k2eAgMnSc2d1	Metánovský
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
–	–	k?	–
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
a	a	k8xC	a
cestovatele	cestovatel	k1gMnSc2	cestovatel
a	a	k8xC	a
dobrodruha	dobrodruh	k1gMnSc2	dobrodruh
Jana	Jan	k1gMnSc2	Jan
Eskymo	eskymo	k1gNnSc1	eskymo
Welzla	Welzla	k1gMnSc1	Welzla
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenku	myšlenka	k1gFnSc4	myšlenka
založit	založit	k5eAaPmF	založit
Divadlo	divadlo	k1gNnSc4	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
dostali	dostat	k5eAaPmAgMnP	dostat
Šebánek	Šebánek	k1gMnSc1	Šebánek
<g/>
,	,	kIx,	,
Čepelka	čepelka	k1gFnSc1	čepelka
<g/>
,	,	kIx,	,
Smoljak	Smoljak	k1gMnSc1	Smoljak
a	a	k8xC	a
Svěrák	Svěrák	k1gMnSc1	Svěrák
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
Akt	akt	k1gInSc1	akt
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
celovečerní	celovečerní	k2eAgNnSc4d1	celovečerní
představení	představení	k1gNnSc4	představení
příliš	příliš	k6eAd1	příliš
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
,	,	kIx,	,
přidali	přidat	k5eAaPmAgMnP	přidat
herci	herec	k1gMnPc1	herec
ještě	ještě	k6eAd1	ještě
fingovanou	fingovaný	k2eAgFnSc4d1	fingovaná
přednášku	přednáška	k1gFnSc4	přednáška
o	o	k7c6	o
autorovi	autor	k1gMnSc3	autor
J.	J.	kA	J.
Cimrmanovi	Cimrman	k1gMnSc3	Cimrman
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stala	stát	k5eAaPmAgFnS	stát
pevnou	pevný	k2eAgFnSc7d1	pevná
součástí	součást	k1gFnSc7	součást
všech	všecek	k3xTgNnPc2	všecek
představení	představení	k1gNnPc2	představení
a	a	k8xC	a
setkala	setkat	k5eAaPmAgFnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
značným	značný	k2eAgInSc7d1	značný
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Herci	herec	k1gMnPc1	herec
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stylizovali	stylizovat	k5eAaImAgMnP	stylizovat
do	do	k7c2	do
odborníků	odborník	k1gMnPc2	odborník
(	(	kIx(	(
<g/>
vědců	vědec	k1gMnPc2	vědec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
interpretují	interpretovat	k5eAaBmIp3nP	interpretovat
dílo	dílo	k1gNnSc4	dílo
"	"	kIx"	"
<g/>
mistra	mistr	k1gMnSc4	mistr
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
své	svůj	k3xOyFgFnSc2	svůj
vědecké	vědecký	k2eAgFnSc2d1	vědecká
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnosti	veřejnost	k1gFnPc4	veřejnost
jsou	být	k5eAaImIp3nP	být
nejznámější	známý	k2eAgMnPc1d3	nejznámější
autoři	autor	k1gMnPc1	autor
her	hra	k1gFnPc2	hra
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
a	a	k8xC	a
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
<g/>
.	.	kIx.	.
</s>
<s>
Nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
se	se	k3xPyFc4	se
Cimrmanem	Cimrman	k1gMnSc7	Cimrman
zabýval	zabývat	k5eAaImAgMnS	zabývat
i	i	k9	i
Salón	salón	k1gInSc4	salón
Cimrman	Cimrman	k1gMnSc1	Cimrman
Jiřího	Jiří	k1gMnSc2	Jiří
Šebánka	Šebánek	k1gMnSc2	Šebánek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
byl	být	k5eAaImAgInS	být
natočen	natočen	k2eAgInSc1d1	natočen
pseudodokument	pseudodokument	k1gInSc1	pseudodokument
Stopa	stopa	k1gFnSc1	stopa
vede	vést	k5eAaImIp3nS	vést
do	do	k7c2	do
Liptákova	Liptákův	k2eAgNnSc2d1	Liptákův
<g/>
,	,	kIx,	,
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
podíleli	podílet	k5eAaImAgMnP	podílet
Jiří	Jiří	k1gMnSc1	Jiří
Šebánek	Šebánek	k1gMnSc1	Šebánek
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Velebný	velebný	k2eAgMnSc1d1	velebný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
natočili	natočit	k5eAaBmAgMnP	natočit
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
a	a	k8xC	a
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
divácky	divácky	k6eAd1	divácky
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
film	film	k1gInSc4	film
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
ležící	ležící	k2eAgMnSc1d1	ležící
<g/>
,	,	kIx,	,
spící	spící	k2eAgMnSc1d1	spící
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
Mistrově	mistrův	k2eAgInSc6d1	mistrův
životě	život	k1gInSc6	život
a	a	k8xC	a
díle	díl	k1gInSc6	díl
v	v	k7c6	v
historických	historický	k2eAgFnPc6d1	historická
souvislostech	souvislost	k1gFnPc6	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
pak	pak	k6eAd1	pak
natočili	natočit	k5eAaBmAgMnP	natočit
film	film	k1gInSc4	film
Rozpuštěný	rozpuštěný	k2eAgInSc4d1	rozpuštěný
a	a	k8xC	a
vypuštěný	vypuštěný	k2eAgInSc4d1	vypuštěný
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
sice	sice	k8xC	sice
Cimrmana	Cimrman	k1gMnSc2	Cimrman
přímo	přímo	k6eAd1	přímo
nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
fiktivních	fiktivní	k2eAgNnPc2d1	fiktivní
děl	dělo	k1gNnPc2	dělo
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
Vražda	vražda	k1gFnSc1	vražda
v	v	k7c6	v
salonním	salonní	k2eAgMnSc6d1	salonní
coupé	coupý	k2eAgInPc4d1	coupý
<g/>
)	)	kIx)	)
a	a	k8xC	a
postav	postava	k1gFnPc2	postava
(	(	kIx(	(
<g/>
inspektor	inspektor	k1gMnSc1	inspektor
Trachta	Trachta	k?	Trachta
<g/>
,	,	kIx,	,
továrník	továrník	k1gMnSc1	továrník
Bierhanzl	Bierhanzl	k1gMnSc1	Bierhanzl
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejistá	jistý	k2eNgFnSc1d1	nejistá
sezóna	sezóna	k1gFnSc1	sezóna
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
je	být	k5eAaImIp3nS	být
pohledem	pohled	k1gInSc7	pohled
do	do	k7c2	do
zákulisí	zákulisí	k1gNnSc2	zákulisí
divadelního	divadelní	k2eAgInSc2d1	divadelní
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
připravuje	připravovat	k5eAaImIp3nS	připravovat
premiéru	premiéra	k1gFnSc4	premiéra
nové	nový	k2eAgFnSc2d1	nová
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
vypořádat	vypořádat	k5eAaPmF	vypořádat
se	s	k7c7	s
zásahy	zásah	k1gInPc7	zásah
komunistických	komunistický	k2eAgInPc2d1	komunistický
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrci	tvůrce	k1gMnPc1	tvůrce
literární	literární	k2eAgFnSc2d1	literární
postavy	postava	k1gFnSc2	postava
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
,	,	kIx,	,
především	především	k9	především
protagonisté	protagonista	k1gMnPc1	protagonista
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
Jiří	Jiří	k1gMnSc1	Jiří
Šebánek	Šebánek	k1gMnSc1	Šebánek
<g/>
,	,	kIx,	,
během	během	k7c2	během
let	léto	k1gNnPc2	léto
propracovali	propracovat	k5eAaPmAgMnP	propracovat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
aspekty	aspekt	k1gInPc4	aspekt
života	život	k1gInSc2	život
tohoto	tento	k3xDgMnSc2	tento
fiktivního	fiktivní	k2eAgMnSc2d1	fiktivní
génia	génius	k1gMnSc2	génius
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
narodil	narodit	k5eAaPmAgMnS	narodit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1869	[number]	k4	1869
a	a	k8xC	a
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
krejčí	krejčí	k1gMnSc1	krejčí
Leopold	Leopold	k1gMnSc1	Leopold
Cimrman	Cimrman	k1gMnSc1	Cimrman
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
rakouská	rakouský	k2eAgFnSc1d1	rakouská
herečka	herečka	k1gFnSc1	herečka
Marlen	Marlena	k1gFnPc2	Marlena
Jelinková	Jelinkový	k2eAgFnSc1d1	Jelinková
<g/>
.	.	kIx.	.
</s>
<s>
Cítil	cítit	k5eAaImAgMnS	cítit
se	se	k3xPyFc4	se
však	však	k9	však
být	být	k5eAaImF	být
Čechem	Čech	k1gMnSc7	Čech
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
prý	prý	k9	prý
svědčí	svědčit	k5eAaImIp3nS	svědčit
poslední	poslední	k2eAgInSc1d1	poslední
zápis	zápis	k1gInSc1	zápis
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
deníku	deník	k1gInSc2	deník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
touhu	touha	k1gFnSc4	touha
"	"	kIx"	"
<g/>
uvidět	uvidět	k5eAaPmF	uvidět
svou	svůj	k3xOyFgFnSc4	svůj
vlast	vlast	k1gFnSc4	vlast
Böhmen	Böhmen	k1gInSc1	Böhmen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
narativu	narativ	k1gInSc2	narativ
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
českých	český	k2eAgMnPc2d1	český
dramatiků	dramatik	k1gMnPc2	dramatik
<g/>
,	,	kIx,	,
básníků	básník	k1gMnPc2	básník
<g/>
,	,	kIx,	,
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
učitelů	učitel	k1gMnPc2	učitel
<g/>
,	,	kIx,	,
cestovatelů	cestovatel	k1gMnPc2	cestovatel
<g/>
,	,	kIx,	,
filozofů	filozof	k1gMnPc2	filozof
<g/>
,	,	kIx,	,
vynálezců	vynálezce	k1gMnPc2	vynálezce
<g/>
,	,	kIx,	,
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
kriminalistů	kriminalista	k1gMnPc2	kriminalista
a	a	k8xC	a
sportovců	sportovec	k1gMnPc2	sportovec
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
místo	místo	k1gNnSc4	místo
Cimrmanova	Cimrmanův	k2eAgNnSc2d1	Cimrmanovo
stáří	stáří	k1gNnSc2	stáří
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
semináři	seminář	k1gInSc6	seminář
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
Lijavec	lijavec	k1gInSc1	lijavec
označen	označen	k2eAgInSc4d1	označen
starobinec	starobinec	k1gInSc4	starobinec
ve	v	k7c6	v
Frymburku	Frymburk	k1gInSc6	Frymburk
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
semináře	seminář	k1gInSc2	seminář
k	k	k7c3	k
Poslu	posel	k1gMnSc3	posel
z	z	k7c2	z
Liptákova	Liptákův	k2eAgMnSc2d1	Liptákův
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
počátku	počátek	k1gInSc2	počátek
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Místem	místo	k1gNnSc7	místo
Cimrmanova	Cimrmanův	k2eAgNnSc2d1	Cimrmanovo
stáří	stáří	k1gNnSc2	stáří
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
semináře	seminář	k1gInSc2	seminář
pojizerská	pojizerský	k2eAgFnSc1d1	Pojizerská
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
obec	obec	k1gFnSc1	obec
Liptákov	Liptákov	k1gInSc1	Liptákov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
příběh	příběh	k1gInSc1	příběh
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
herci	herec	k1gMnPc7	herec
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
popisují	popisovat	k5eAaImIp3nP	popisovat
"	"	kIx"	"
<g/>
znovuobjevení	znovuobjevení	k1gNnSc4	znovuobjevení
<g/>
"	"	kIx"	"
osobnosti	osobnost	k1gFnSc2	osobnost
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
–	–	k?	–
právě	právě	k9	právě
tam	tam	k6eAd1	tam
byla	být	k5eAaImAgFnS	být
totiž	totiž	k9	totiž
nalezena	naleznout	k5eAaPmNgFnS	naleznout
truhla	truhla	k1gFnSc1	truhla
s	s	k7c7	s
géniovým	géniův	k2eAgNnSc7d1	géniovo
dědictvím	dědictví	k1gNnSc7	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
uvedená	uvedený	k2eAgFnSc1d1	uvedená
hra	hra	k1gFnSc1	hra
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
,	,	kIx,	,
České	český	k2eAgNnSc1d1	české
nebe	nebe	k1gNnSc1	nebe
<g/>
,	,	kIx,	,
však	však	k9	však
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
tvořil	tvořit	k5eAaImAgInS	tvořit
i	i	k9	i
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
hru	hra	k1gFnSc4	hra
napsal	napsat	k5eAaPmAgMnS	napsat
v	v	k7c6	v
Dymokurech	Dymokur	k1gInPc6	Dymokur
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Cimrmanovo	Cimrmanův	k2eAgNnSc1d1	Cimrmanovo
dílo	dílo	k1gNnSc4	dílo
a	a	k8xC	a
Cimrmanovy	Cimrmanův	k2eAgInPc4d1	Cimrmanův
vynálezy	vynález	k1gInPc4	vynález
<g/>
.	.	kIx.	.
</s>
<s>
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
předložil	předložit	k5eAaPmAgMnS	předložit
americké	americký	k2eAgFnSc3d1	americká
vládě	vláda	k1gFnSc3	vláda
projekt	projekt	k1gInSc4	projekt
Panamského	panamský	k2eAgInSc2d1	panamský
průplavu	průplav	k1gInSc2	průplav
včetně	včetně	k7c2	včetně
libreta	libreto	k1gNnSc2	libreto
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Reformoval	reformovat	k5eAaBmAgMnS	reformovat
haličské	haličský	k2eAgNnSc4d1	haličské
školství	školství	k1gNnSc4	školství
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
hrabětem	hrabě	k1gMnSc7	hrabě
Zeppelinem	Zeppelin	k1gInSc7	Zeppelin
konstruoval	konstruovat	k5eAaImAgInS	konstruovat
první	první	k4xOgFnSc4	první
vzducholoď	vzducholoď	k1gFnSc4	vzducholoď
s	s	k7c7	s
tuhou	tuhý	k2eAgFnSc7d1	tuhá
konstrukcí	konstrukce	k1gFnSc7	konstrukce
ze	z	k7c2	z
švédské	švédský	k2eAgFnSc2d1	švédská
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
s	s	k7c7	s
gondolou	gondola	k1gFnSc7	gondola
z	z	k7c2	z
českého	český	k2eAgNnSc2d1	české
vrbového	vrbový	k2eAgNnSc2d1	vrbové
proutí	proutí	k1gNnSc2	proutí
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
anarchista	anarchista	k1gMnSc1	anarchista
vyhoštěný	vyhoštěný	k2eAgMnSc1d1	vyhoštěný
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
si	se	k3xPyFc3	se
nesl	nést	k5eAaImAgInS	nést
v	v	k7c6	v
osobních	osobní	k2eAgInPc6d1	osobní
dokladech	doklad	k1gInPc6	doklad
záznam	záznam	k1gInSc4	záznam
"	"	kIx"	"
<g/>
strůjce	strůjce	k1gMnSc2	strůjce
nepokojů	nepokoj	k1gInPc2	nepokoj
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
švýcarskou	švýcarský	k2eAgFnSc4d1	švýcarská
firmu	firma	k1gFnSc4	firma
Omega	omega	k1gNnSc2	omega
k	k	k7c3	k
nabídce	nabídka	k1gFnSc3	nabídka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zdokonalil	zdokonalit	k5eAaPmAgInS	zdokonalit
setrvačník	setrvačník	k1gInSc1	setrvačník
dámských	dámský	k2eAgFnPc2d1	dámská
hodinek	hodinka	k1gFnPc2	hodinka
Piccolo	Piccola	k1gFnSc5	Piccola
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
v	v	k7c6	v
těžkých	těžký	k2eAgFnPc6d1	těžká
podmínkách	podmínka	k1gFnPc6	podmínka
tamních	tamní	k2eAgFnPc2d1	tamní
Alp	Alpy	k1gFnPc2	Alpy
zavedl	zavést	k5eAaPmAgInS	zavést
(	(	kIx(	(
<g/>
a	a	k8xC	a
po	po	k7c4	po
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
také	také	k9	také
vykonával	vykonávat	k5eAaImAgInS	vykonávat
<g/>
)	)	kIx)	)
funkci	funkce	k1gFnSc4	funkce
porodního	porodní	k2eAgMnSc4d1	porodní
dědka	dědek	k1gMnSc4	dědek
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
život	život	k1gInSc1	život
polárních	polární	k2eAgMnPc2d1	polární
Samojedů	Samojed	k1gMnPc2	Samojed
a	a	k8xC	a
na	na	k7c6	na
útěku	útěk	k1gInSc6	útěk
před	před	k7c7	před
vyhladovělým	vyhladovělý	k2eAgInSc7d1	vyhladovělý
kmenem	kmen	k1gInSc7	kmen
Mlasků	mlask	k1gInPc2	mlask
minul	minout	k5eAaImAgInS	minout
severní	severní	k2eAgInSc1d1	severní
pól	pól	k1gInSc1	pól
o	o	k7c4	o
pouhých	pouhý	k2eAgInPc2d1	pouhý
sedm	sedm	k4xCc4	sedm
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paraguayi	Paraguay	k1gFnSc6	Paraguay
založil	založit	k5eAaPmAgMnS	založit
loutkové	loutkový	k2eAgNnSc4d1	loutkové
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
založil	založit	k5eAaPmAgMnS	založit
kriminalistickou	kriminalistický	k2eAgFnSc4d1	kriminalistická
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgFnSc4d1	hudební
a	a	k8xC	a
baletní	baletní	k2eAgFnSc4d1	baletní
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Vedl	vést	k5eAaImAgInS	vést
obsáhlou	obsáhlý	k2eAgFnSc4d1	obsáhlá
korespondenci	korespondence	k1gFnSc4	korespondence
s	s	k7c7	s
G.	G.	kA	G.
B.	B.	kA	B.
Shawem	Shaw	k1gMnSc7	Shaw
<g/>
,	,	kIx,	,
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
bohužel	bohužel	k9	bohužel
zarputilý	zarputilý	k2eAgMnSc1d1	zarputilý
Ir	Ir	k1gMnSc1	Ir
neodpovídal	odpovídat	k5eNaImAgMnS	odpovídat
<g/>
.	.	kIx.	.
</s>
<s>
Vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
jogurt	jogurt	k1gInSc1	jogurt
<g/>
.	.	kIx.	.
</s>
<s>
Nezištně	zištně	k6eNd1	zištně
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
řadě	řada	k1gFnSc3	řada
světových	světový	k2eAgMnPc2d1	světový
velikánů	velikán	k1gMnPc2	velikán
<g/>
:	:	kIx,	:
manželům	manžel	k1gMnPc3	manžel
Curieovým	Curieův	k2eAgFnPc3d1	Curieova
nanosil	nanosit	k5eAaBmAgMnS	nanosit
do	do	k7c2	do
sklepa	sklep	k1gInSc2	sklep
na	na	k7c6	na
vlastních	vlastní	k2eAgInPc6d1	vlastní
zádech	záda	k1gNnPc6	záda
pětačtyřicet	pětačtyřicet	k4xCc1	pětačtyřicet
puten	putna	k1gFnPc2	putna
smolince	smolinec	k1gInSc2	smolinec
<g/>
,	,	kIx,	,
prof.	prof.	kA	prof.
Burianovi	Burian	k1gMnSc6	Burian
asistoval	asistovat	k5eAaImAgMnS	asistovat
při	při	k7c6	při
prvních	první	k4xOgFnPc6	první
plastických	plastický	k2eAgFnPc6d1	plastická
operacích	operace	k1gFnPc6	operace
<g/>
,	,	kIx,	,
Edisonovi	Edison	k1gMnSc6	Edison
předělal	předělat	k5eAaPmAgInS	předělat
pupík	pupík	k1gInSc1	pupík
na	na	k7c6	na
objímce	objímka	k1gFnSc6	objímka
jeho	jeho	k3xOp3gFnSc2	jeho
první	první	k4xOgFnSc2	první
žárovky	žárovka	k1gFnSc2	žárovka
<g/>
,	,	kIx,	,
Eiffelovi	Eiffel	k1gMnSc6	Eiffel
sehnal	sehnat	k5eAaPmAgInS	sehnat
podnájem	podnájem	k1gInSc1	podnájem
<g/>
,	,	kIx,	,
J.	J.	kA	J.
V.	V.	kA	V.
Sládkovi	Sládek	k1gMnSc3	Sládek
opravil	opravit	k5eAaPmAgMnS	opravit
původně	původně	k6eAd1	původně
chlípný	chlípný	k2eAgInSc4d1	chlípný
závěr	závěr	k1gInSc4	závěr
jeho	jeho	k3xOp3gFnSc2	jeho
známé	známý	k2eAgFnSc2d1	známá
básně	báseň	k1gFnSc2	báseň
Lesní	lesní	k2eAgFnSc1d1	lesní
studánka	studánka	k1gFnSc1	studánka
<g/>
,	,	kIx,	,
Čechovovi	Čechovův	k2eAgMnPc1d1	Čechovův
pochválil	pochválit	k5eAaPmAgMnS	pochválit
novou	nový	k2eAgFnSc4d1	nová
knihu	kniha	k1gFnSc4	kniha
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
pohnojil	pohnojit	k5eAaPmAgMnS	pohnojit
višňový	višňový	k2eAgInSc4d1	višňový
sad	sad	k1gInSc4	sad
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
tvůrcem	tvůrce	k1gMnSc7	tvůrce
filozofie	filozofie	k1gFnSc2	filozofie
externismu	externismus	k1gInSc2	externismus
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
vášnivý	vášnivý	k2eAgMnSc1d1	vášnivý
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
objevil	objevit	k5eAaPmAgMnS	objevit
monopól	monopól	k1gInSc4	monopól
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
objev	objev	k1gInSc1	objev
však	však	k9	však
upadl	upadnout	k5eAaPmAgInS	upadnout
v	v	k7c6	v
zapomnění	zapomnění	k1gNnSc6	zapomnění
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ekonomy	ekonom	k1gMnPc4	ekonom
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
záměně	záměna	k1gFnSc3	záměna
<g/>
.	.	kIx.	.
</s>
<s>
Vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
sněžného	sněžný	k2eAgMnSc4d1	sněžný
muže	muž	k1gMnSc4	muž
"	"	kIx"	"
<g/>
Játy	Játa	k1gFnSc2	Játa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
Angličany	Angličan	k1gMnPc4	Angličan
zkomolen	zkomolen	k2eAgInSc4d1	zkomolen
a	a	k8xC	a
převzat	převzat	k2eAgInSc4d1	převzat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Yetti	yetti	k1gMnSc1	yetti
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
také	také	k9	také
CD	CD	kA	CD
(	(	kIx(	(
<g/>
Cimrmanův	Cimrmanův	k2eAgInSc1d1	Cimrmanův
disk	disk	k1gInSc1	disk
<g/>
)	)	kIx)	)
znovuobjevený	znovuobjevený	k2eAgInSc1d1	znovuobjevený
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
a	a	k8xC	a
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
kompaktní	kompaktní	k2eAgInSc4d1	kompaktní
disk	disk	k1gInSc4	disk
<g/>
.	.	kIx.	.
</s>
<s>
Cimrman	Cimrman	k1gMnSc1	Cimrman
však	však	k9	však
místo	místo	k1gNnSc4	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
polykarbonátu	polykarbonát	k1gInSc2	polykarbonát
používal	používat	k5eAaImAgInS	používat
ekologický	ekologický	k2eAgInSc1d1	ekologický
včelí	včelí	k2eAgInSc4d1	včelí
vosk	vosk	k1gInSc4	vosk
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
vynálezy	vynález	k1gInPc4	vynález
<g/>
:	:	kIx,	:
dvoudílné	dvoudílný	k2eAgFnPc4d1	dvoudílná
plavky	plavka	k1gFnPc4	plavka
<g/>
,	,	kIx,	,
plnotučné	plnotučný	k2eAgNnSc4d1	plnotučné
mléko	mléko	k1gNnSc4	mléko
<g/>
,	,	kIx,	,
princip	princip	k1gInSc4	princip
internetu	internet	k1gInSc2	internet
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
vizionářství	vizionářství	k1gNnSc1	vizionářství
dokládá	dokládat	k5eAaImIp3nS	dokládat
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
i	i	k8xC	i
mistrův	mistrův	k2eAgInSc1d1	mistrův
výmluvný	výmluvný	k2eAgInSc1d1	výmluvný
epitaf	epitaf	k1gInSc1	epitaf
<g/>
:	:	kIx,	:
K	k	k7c3	k
nesmrtelným	smrtelný	k2eNgNnPc3d1	nesmrtelné
dílům	dílo	k1gNnPc3	dílo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
jeho	jeho	k3xOp3gFnSc2	jeho
opery	opera	k1gFnSc2	opera
Gbely	Gbely	k1gInPc4	Gbely
<g/>
,	,	kIx,	,
Výdřeva	výdřeva	k1gFnSc1	výdřeva
a	a	k8xC	a
Most	most	k1gInSc1	most
o	o	k7c6	o
nosnosti	nosnost	k1gFnSc6	nosnost
23	[number]	k4	23
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
Mistrovým	mistrův	k2eAgInSc7d1	mistrův
operetním	operetní	k2eAgInSc7d1	operetní
dílem	díl	k1gInSc7	díl
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
sedmihodinová	sedmihodinový	k2eAgFnSc1d1	sedmihodinová
operetní	operetní	k2eAgFnSc1d1	operetní
freska	freska	k1gFnSc1	freska
Proso	proso	k1gNnSc4	proso
<g/>
.	.	kIx.	.
</s>
<s>
Jakkoliv	jakkoliv	k8xS	jakkoliv
měl	mít	k5eAaImAgMnS	mít
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
neklidnou	klidný	k2eNgFnSc4d1	neklidná
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
rád	rád	k6eAd1	rád
vracel	vracet	k5eAaImAgMnS	vracet
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Vlastenectví	vlastenectví	k1gNnSc1	vlastenectví
je	být	k5eAaImIp3nS	být
nejvýraznějším	výrazný	k2eAgInSc7d3	nejvýraznější
rysem	rys	k1gInSc7	rys
jeho	jeho	k3xOp3gFnSc2	jeho
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
trpěl	trpět	k5eAaImAgMnS	trpět
v	v	k7c6	v
habsburských	habsburský	k2eAgInPc6d1	habsburský
žalářích	žalář	k1gInPc6	žalář
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
téměř	téměř	k6eAd1	téměř
plynně	plynně	k6eAd1	plynně
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
–	–	k?	–
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejlépe	dobře	k6eAd3	dobře
osvětluje	osvětlovat	k5eAaImIp3nS	osvětlovat
vroucnost	vroucnost	k1gFnSc4	vroucnost
jeho	jeho	k3xOp3gNnPc2	jeho
češství	češství	k1gNnPc2	češství
–	–	k?	–
se	se	k3xPyFc4	se
dokázal	dokázat	k5eAaPmAgInS	dokázat
rozejít	rozejít	k5eAaPmF	rozejít
i	i	k9	i
s	s	k7c7	s
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
přítelem	přítel	k1gMnSc7	přítel
Aloisem	Alois	k1gMnSc7	Alois
Jiráskem	Jirásek	k1gMnSc7	Jirásek
<g/>
.	.	kIx.	.
</s>
<s>
Nemohl	moct	k5eNaImAgInS	moct
mu	on	k3xPp3gNnSc3	on
totiž	totiž	k9	totiž
odpustit	odpustit	k5eAaPmF	odpustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zařadil	zařadit	k5eAaPmAgMnS	zařadit
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
Starých	Starých	k2eAgFnPc2d1	Starých
pověstí	pověst	k1gFnPc2	pověst
českých	český	k2eAgFnPc2d1	Česká
příběh	příběh	k1gInSc4	příběh
o	o	k7c6	o
blanických	blanický	k2eAgMnPc6d1	blanický
rytířích	rytíř	k1gMnPc6	rytíř
<g/>
,	,	kIx,	,
čekajících	čekající	k2eAgMnPc2d1	čekající
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přispěchali	přispěchat	k5eAaPmAgMnP	přispěchat
národu	národ	k1gInSc3	národ
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
bude	být	k5eAaImBp3nS	být
nejhůře	zle	k6eAd3	zle
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Protože	protože	k8xS	protože
za	za	k7c4	za
celých	celý	k2eAgNnPc2d1	celé
tři	tři	k4xCgNnPc4	tři
sta	sto	k4xCgNnPc4	sto
let	léto	k1gNnPc2	léto
habsburského	habsburský	k2eAgInSc2d1	habsburský
útlaku	útlak	k1gInSc2	útlak
nikdy	nikdy	k6eAd1	nikdy
nevyjeli	vyjet	k5eNaPmAgMnP	vyjet
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
<g/>
"	"	kIx"	"
vyčítá	vyčítat	k5eAaImIp3nS	vyčítat
mu	on	k3xPp3gNnSc3	on
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Cimrman	Cimrman	k1gMnSc1	Cimrman
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
deprimuješ	deprimovat	k5eAaBmIp2nS	deprimovat
tak	tak	k9	tak
národ	národ	k1gInSc4	národ
vyhlídkou	vyhlídka	k1gFnSc7	vyhlídka
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
prožívá	prožívat	k5eAaImIp3nS	prožívat
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
ještě	ještě	k9	ještě
nic	nic	k3yNnSc1	nic
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
teprve	teprve	k6eAd1	teprve
přijde	přijít	k5eAaPmIp3nS	přijít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
České	český	k2eAgNnSc1d1	české
vlastenectví	vlastenectví	k1gNnSc1	vlastenectví
je	být	k5eAaImIp3nS	být
dokazováno	dokazovat	k5eAaImNgNnS	dokazovat
i	i	k9	i
zápisem	zápis	k1gInSc7	zápis
v	v	k7c6	v
Cimrmanově	Cimrmanův	k2eAgInSc6d1	Cimrmanův
deníku	deník	k1gInSc6	deník
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chtěl	chtít	k5eAaImAgMnS	chtít
bych	by	kYmCp1nS	by
vidět	vidět	k5eAaImF	vidět
svou	svůj	k3xOyFgFnSc4	svůj
rodnou	rodný	k2eAgFnSc4d1	rodná
vlast	vlast	k1gFnSc4	vlast
Böhmen	Böhmen	k2eAgMnSc1d1	Böhmen
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Teprve	teprve	k6eAd1	teprve
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
v	v	k7c4	v
deset	deset	k4xCc4	deset
hodin	hodina	k1gFnPc2	hodina
pět	pět	k4xCc4	pět
minut	minuta	k1gFnPc2	minuta
dopoledne	dopoledne	k6eAd1	dopoledne
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
pojizerské	pojizerský	k2eAgFnSc6d1	Pojizerská
vesničce	vesnička	k1gFnSc6	vesnička
Liptákov	Liptákov	k1gInSc4	Liptákov
nalezena	naleznout	k5eAaPmNgFnS	naleznout
truhla	truhla	k1gFnSc1	truhla
s	s	k7c7	s
významnou	významný	k2eAgFnSc7d1	významná
částí	část	k1gFnSc7	část
Mistrovy	mistrův	k2eAgFnSc2d1	Mistrova
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
<g/>
.	.	kIx.	.
</s>
<s>
Nalezené	nalezený	k2eAgInPc1d1	nalezený
dokumenty	dokument	k1gInPc1	dokument
inspirovaly	inspirovat	k5eAaBmAgInP	inspirovat
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
vznik	vznik	k1gInSc4	vznik
Společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
rehabilitaci	rehabilitace	k1gFnSc4	rehabilitace
osobnosti	osobnost	k1gFnSc2	osobnost
a	a	k8xC	a
díla	dílo	k1gNnSc2	dílo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
rozhlasu	rozhlas	k1gInSc2	rozhlas
a	a	k8xC	a
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
nesoucího	nesoucí	k2eAgNnSc2d1	nesoucí
Mistrovo	mistrův	k2eAgNnSc4d1	Mistrovo
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
popularizuje	popularizovat	k5eAaImIp3nS	popularizovat
Cimrmanův	Cimrmanův	k2eAgInSc1d1	Cimrmanův
duchovní	duchovní	k2eAgInSc1d1	duchovní
odkaz	odkaz	k1gInSc1	odkaz
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
desetiletích	desetiletí	k1gNnPc6	desetiletí
zaskvěly	zaskvět	k5eAaPmAgInP	zaskvět
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
Mistrovy	mistrův	k2eAgFnSc2d1	Mistrova
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
literárních	literární	k2eAgMnPc2d1	literární
vědců	vědec	k1gMnPc2	vědec
začala	začít	k5eAaPmAgFnS	začít
o	o	k7c6	o
Cimrmanově	Cimrmanův	k2eAgFnSc6d1	Cimrmanova
existenci	existence	k1gFnSc6	existence
pochybovat	pochybovat	k5eAaImF	pochybovat
a	a	k8xC	a
označila	označit	k5eAaPmAgFnS	označit
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
za	za	k7c4	za
liptákovský	liptákovský	k2eAgInSc4d1	liptákovský
podvrh	podvrh	k1gInSc4	podvrh
podobný	podobný	k2eAgInSc4d1	podobný
podvrhu	podvrh	k1gInSc6	podvrh
zelenohorskému	zelenohorský	k2eAgMnSc3d1	zelenohorský
a	a	k8xC	a
královédvorskému	královédvorský	k2eAgInSc3d1	královédvorský
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
odpověď	odpověď	k1gFnSc1	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
Který	který	k3yIgMnSc1	který
z	z	k7c2	z
našich	náš	k3xOp1gMnPc2	náš
soudobých	soudobý	k2eAgMnPc2d1	soudobý
spisovatelů	spisovatel	k1gMnPc2	spisovatel
by	by	kYmCp3nS	by
byl	být	k5eAaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
dílo	dílo	k1gNnSc4	dílo
takové	takový	k3xDgFnSc2	takový
velikosti	velikost	k1gFnSc2	velikost
vytvořit	vytvořit	k5eAaPmF	vytvořit
<g/>
?	?	kIx.	?
</s>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
<g/>
.	.	kIx.	.
</s>
<s>
Cítíme	cítit	k5eAaImIp1nP	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
k	k	k7c3	k
takovému	takový	k3xDgInSc3	takový
činu	čin	k1gInSc3	čin
museli	muset	k5eAaImAgMnP	muset
spojit	spojit	k5eAaPmF	spojit
nejméně	málo	k6eAd3	málo
dva	dva	k4xCgMnPc1	dva
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
ti	ten	k3xDgMnPc1	ten
by	by	kYmCp3nP	by
museli	muset	k5eAaImAgMnP	muset
být	být	k5eAaImF	být
alespoň	alespoň	k9	alespoň
průměrně	průměrně	k6eAd1	průměrně
geniální	geniální	k2eAgInPc1d1	geniální
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
jakkoli	jakkoli	k6eAd1	jakkoli
<g/>
,	,	kIx,	,
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nimiž	jenž	k3xRgMnPc7	jenž
je	být	k5eAaImIp3nS	být
podepsán	podepsat	k5eAaPmNgMnS	podepsat
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
vrcholům	vrchol	k1gInPc3	vrchol
světové	světový	k2eAgFnSc2d1	světová
dramatické	dramatický	k2eAgFnSc2d1	dramatická
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
diskutovat	diskutovat	k5eAaImF	diskutovat
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
vést	vést	k5eAaImF	vést
spory	spor	k1gInPc1	spor
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
i	i	k9	i
nesouhlasit	souhlasit	k5eNaImF	souhlasit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
všechno	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
dá	dát	k5eAaPmIp3nS	dát
dělat	dělat	k5eAaImF	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Cimrman	Cimrman	k1gMnSc1	Cimrman
získal	získat	k5eAaPmAgMnS	získat
rekordně	rekordně	k6eAd1	rekordně
největší	veliký	k2eAgInSc4d3	veliký
počet	počet	k1gInSc4	počet
hlasů	hlas	k1gInPc2	hlas
v	v	k7c6	v
celonárodní	celonárodní	k2eAgFnSc6d1	celonárodní
anketě	anketa	k1gFnSc6	anketa
Největší	veliký	k2eAgMnSc1d3	veliký
Čech	Čech	k1gMnSc1	Čech
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
konečného	konečný	k2eAgNnSc2d1	konečné
pořadí	pořadí	k1gNnSc2	pořadí
vyřazen	vyřazen	k2eAgMnSc1d1	vyřazen
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
mu	on	k3xPp3gMnSc3	on
udělena	udělit	k5eAaPmNgFnS	udělit
jen	jen	k9	jen
čestná	čestný	k2eAgFnSc1d1	čestná
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
získal	získat	k5eAaPmAgMnS	získat
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
nejvíce	nejvíce	k6eAd1	nejvíce
hlasů	hlas	k1gInPc2	hlas
v	v	k7c6	v
televizní	televizní	k2eAgFnSc6d1	televizní
soutěži	soutěž	k1gFnSc6	soutěž
o	o	k7c4	o
sedm	sedm	k4xCc4	sedm
divů	div	k1gInPc2	div
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
porazil	porazit	k5eAaPmAgMnS	porazit
např.	např.	kA	např.
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kontaktní	kontaktní	k2eAgFnSc2d1	kontaktní
čočky	čočka	k1gFnSc2	čočka
nebo	nebo	k8xC	nebo
české	český	k2eAgNnSc4d1	české
pivo	pivo	k1gNnSc4	pivo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2007	[number]	k4	2007
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
idnes	idnesa	k1gFnPc2	idnesa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
čtenáři	čtenář	k1gMnPc1	čtenář
navrhovali	navrhovat	k5eAaImAgMnP	navrhovat
možné	možný	k2eAgMnPc4d1	možný
protikandidáty	protikandidát	k1gMnPc4	protikandidát
Václava	Václav	k1gMnSc4	Václav
Klause	Klaus	k1gMnSc4	Klaus
na	na	k7c6	na
funkci	funkce	k1gFnSc6	funkce
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
druhý	druhý	k4xOgInSc4	druhý
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
počet	počet	k1gInSc4	počet
nominačních	nominační	k2eAgInPc2d1	nominační
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
po	po	k7c6	po
Karlu	Karel	k1gMnSc6	Karel
Schwarzenbergovi	Schwarzenberg	k1gMnSc6	Schwarzenberg
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalšího	další	k2eAgNnSc2d1	další
hlasování	hlasování	k1gNnSc2	hlasování
byl	být	k5eAaImAgInS	být
však	však	k9	však
redakcí	redakce	k1gFnSc7	redakce
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Cimrmanovi	Cimrman	k1gMnSc6	Cimrman
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
planetka	planetka	k1gFnSc1	planetka
Járacimrman	Járacimrman	k1gMnSc1	Járacimrman
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
ztracená	ztracený	k2eAgFnSc1d1	ztracená
a	a	k8xC	a
znovuobjevená	znovuobjevený	k2eAgFnSc1d1	znovuobjevená
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
dříve	dříve	k6eAd2	dříve
zapomenutý	zapomenutý	k2eAgMnSc1d1	zapomenutý
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
několik	několik	k4yIc4	několik
ulic	ulice	k1gFnPc2	ulice
<g/>
:	:	kIx,	:
ulice	ulice	k1gFnSc1	ulice
Járy	Jára	k1gMnSc2	Jára
da	da	k?	da
Cimrmana	Cimrman	k1gMnSc2	Cimrman
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnSc1	ulice
Járy	Jára	k1gMnSc2	Jára
da	da	k?	da
Cimrmana	Cimrman	k1gMnSc2	Cimrman
v	v	k7c6	v
Roztokách	roztoka	k1gFnPc6	roztoka
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnPc1	ulice
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
ulička	ulička	k1gFnSc1	ulička
zvaná	zvaný	k2eAgFnSc1d1	zvaná
"	"	kIx"	"
<g/>
Chodníček	chodníček	k1gInSc1	chodníček
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
"	"	kIx"	"
v	v	k7c6	v
Tišnově	Tišnov	k1gInSc6	Tišnov
<g/>
,	,	kIx,	,
Okružní	okružní	k2eAgFnSc1d1	okružní
třída	třída	k1gFnSc1	třída
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Huti	huť	k1gFnSc6	huť
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnPc1	ulice
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Kunice	Kunice	k1gFnSc2	Kunice
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnPc1	ulice
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Homole	homole	k1gFnSc2	homole
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnPc1	ulice
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
v	v	k7c6	v
Horšovském	horšovský	k2eAgInSc6d1	horšovský
Týně	Týn	k1gInSc6	Týn
a	a	k8xC	a
kuriózní	kuriózní	k2eAgNnSc1d1	kuriózní
nábřeží	nábřeží	k1gNnSc1	nábřeží
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
v	v	k7c6	v
Lipníku	Lipník	k1gInSc6	Lipník
nad	nad	k7c7	nad
Bečvou	Bečva	k1gFnSc7	Bečva
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vůbec	vůbec	k9	vůbec
není	být	k5eNaImIp3nS	být
u	u	k7c2	u
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
v	v	k7c4	v
Březové	březový	k2eAgNnSc4d1	Březové
nad	nad	k7c7	nad
Svitavou	Svitava	k1gFnSc7	Svitava
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Svěráka	Svěrák	k1gMnSc2	Svěrák
<g/>
,	,	kIx,	,
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Weigela	Weigel	k1gMnSc2	Weigel
a	a	k8xC	a
Miloně	Miloň	k1gFnSc2	Miloň
Čepelky	čepelka	k1gFnSc2	čepelka
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřen	k2eAgFnSc1d1	otevřena
Dobře	dobře	k6eAd1	dobře
ukrytá	ukrytý	k2eAgFnSc1d1	ukrytá
rozhledna	rozhledna	k1gFnSc1	rozhledna
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
slavnostnímu	slavnostní	k2eAgNnSc3d1	slavnostní
odhalení	odhalení	k1gNnSc3	odhalení
sochy	socha	k1gFnSc2	socha
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
v	v	k7c6	v
mlze	mlha	k1gFnSc6	mlha
<g/>
"	"	kIx"	"
na	na	k7c6	na
balkóně	balkón	k1gInSc6	balkón
kina	kino	k1gNnSc2	kino
Jas	jas	k1gInSc4	jas
v	v	k7c4	v
Tanvaldu	Tanvalda	k1gFnSc4	Tanvalda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
kino	kino	k1gNnSc4	kino
přejmenováno	přejmenován	k2eAgNnSc4d1	přejmenováno
na	na	k7c4	na
Kino	kino	k1gNnSc4	kino
Jas	Jasy	k1gInPc2	Jasy
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
Cimrmanovi	Cimrman	k1gMnSc6	Cimrman
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
Lysolaje	Lysolaje	k1gFnPc5	Lysolaje
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
rozdělením	rozdělení	k1gNnSc7	rozdělení
lysolajské	lysolajský	k2eAgFnSc2d1	lysolajská
základní	základní	k2eAgFnSc2d1	základní
a	a	k8xC	a
mateřské	mateřský	k2eAgFnSc2d1	mateřská
školy	škola	k1gFnSc2	škola
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
samostatné	samostatný	k2eAgFnPc4d1	samostatná
organizace	organizace	k1gFnPc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Zoolog	zoolog	k1gMnSc1	zoolog
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vohralík	Vohralík	k1gMnSc1	Vohralík
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
po	po	k7c6	po
Járovi	Jára	k1gMnSc6	Jára
Cimrmanovi	Cimrman	k1gMnSc6	Cimrman
poddruh	poddruh	k1gInSc1	poddruh
myšice	myšice	k1gFnSc2	myšice
malooké	malooký	k2eAgFnSc2d1	malooká
žijící	žijící	k2eAgFnSc2d1	žijící
v	v	k7c6	v
severozápadních	severozápadní	k2eAgFnPc6d1	severozápadní
Čechách	Čechy	k1gFnPc6	Čechy
–	–	k?	–
Apodemus	Apodemus	k1gInSc1	Apodemus
uralensis	uralensis	k1gInSc1	uralensis
cimrmani	cimrman	k1gMnPc1	cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediný	jediný	k2eAgInSc1d1	jediný
český	český	k2eAgInSc1d1	český
endemit	endemit	k1gInSc1	endemit
mezi	mezi	k7c7	mezi
obratlovci	obratlovec	k1gMnPc7	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Cimrmanovi	Cimrman	k1gMnSc6	Cimrman
byli	být	k5eAaImAgMnP	být
pojmenováni	pojmenovat	k5eAaPmNgMnP	pojmenovat
i	i	k9	i
štíři	štír	k1gMnPc1	štír
Heterometrus	Heterometrus	k1gMnSc1	Heterometrus
cimrmani	cimrman	k1gMnPc1	cimrman
<g/>
,	,	kIx,	,
Parabuthus	Parabuthus	k1gMnSc1	Parabuthus
cimrmani	cimrman	k1gMnPc1	cimrman
a	a	k8xC	a
Butheoloides	Butheoloides	k1gMnSc1	Butheoloides
cimrmani	cimrman	k1gMnPc1	cimrman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Letohradu	letohrad	k1gInSc6	letohrad
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
odhalen	odhalit	k5eAaPmNgInS	odhalit
pomník	pomník	k1gInSc1	pomník
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
Cimrmanovo	Cimrmanův	k2eAgNnSc1d1	Cimrmanovo
muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
světnička	světnička	k1gFnSc1	světnička
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
jedna	jeden	k4xCgFnSc1	jeden
místní	místní	k2eAgFnSc1d1	místní
ulička	ulička	k1gFnSc1	ulička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
ZUŠ	ZUŠ	kA	ZUŠ
Františkovy	Františkův	k2eAgFnSc2d1	Františkova
Lázně	lázeň	k1gFnSc2	lázeň
na	na	k7c4	na
ZUŠ	ZUŠ	kA	ZUŠ
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
Františkovy	Františkův	k2eAgFnPc1d1	Františkova
Lázně	lázeň	k1gFnPc1	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2007	[number]	k4	2007
zdolala	zdolat	k5eAaPmAgFnS	zdolat
česká	český	k2eAgFnSc1d1	Česká
horolezecká	horolezecký	k2eAgFnSc1d1	horolezecká
expedice	expedice	k1gFnSc1	expedice
Altaj	Altaj	k1gInSc1	Altaj
Cimrman	Cimrman	k1gMnSc1	Cimrman
bezejmenný	bezejmenný	k2eAgInSc4d1	bezejmenný
vrchol	vrchol	k1gInSc4	vrchol
vysoký	vysoký	k2eAgInSc4d1	vysoký
3	[number]	k4	3
610	[number]	k4	610
m	m	kA	m
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Altaj	Altaj	k1gInSc1	Altaj
(	(	kIx(	(
<g/>
nad	nad	k7c7	nad
údolím	údolí	k1gNnSc7	údolí
Tekelju	Tekelju	k1gFnSc2	Tekelju
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
hory	hora	k1gFnSc2	hora
Altaje	Altaj	k1gInSc2	Altaj
Běluchy	Bělucha	k1gFnSc2	Bělucha
<g/>
,	,	kIx,	,
na	na	k7c6	na
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Účastníci	účastník	k1gMnPc1	účastník
expedice	expedice	k1gFnSc2	expedice
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
byli	být	k5eAaImAgMnP	být
i	i	k9	i
herci	herec	k1gMnPc1	herec
Žižkovského	žižkovský	k2eAgNnSc2d1	Žižkovské
divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
,	,	kIx,	,
podali	podat	k5eAaPmAgMnP	podat
oficiální	oficiální	k2eAgFnSc4d1	oficiální
žádost	žádost	k1gFnSc4	žádost
u	u	k7c2	u
altajské	altajský	k2eAgFnSc2d1	Altajská
vlády	vláda	k1gFnSc2	vláda
o	o	k7c4	o
pojmenování	pojmenování	k1gNnSc4	pojmenování
tohoto	tento	k3xDgInSc2	tento
vrcholu	vrchol	k1gInSc2	vrchol
jako	jako	k8xS	jako
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
<g/>
,	,	kIx,	,
žádosti	žádost	k1gFnSc3	žádost
bylo	být	k5eAaImAgNnS	být
vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
<g/>
,	,	kIx,	,
v	v	k7c6	v
podtitulu	podtitul	k1gInSc6	podtitul
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
nést	nést	k5eAaImF	nést
označení	označení	k1gNnSc4	označení
hora	hora	k1gFnSc1	hora
česko-altajského	českoltajský	k2eAgNnSc2d1	česko-altajský
přátelství	přátelství	k1gNnSc2	přátelství
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
přípravy	příprava	k1gFnSc2	příprava
expedice	expedice	k1gFnSc2	expedice
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
byla	být	k5eAaImAgFnS	být
i	i	k9	i
sbírka	sbírka	k1gFnSc1	sbírka
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Národ	národ	k1gInSc1	národ
tobě	ty	k3xPp2nSc3	ty
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
odpor	odpor	k1gInSc4	odpor
proti	proti	k7c3	proti
expedici	expedice	k1gFnSc3	expedice
a	a	k8xC	a
jejímu	její	k3xOp3gInSc3	její
nápadu	nápad	k1gInSc3	nápad
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
snad	snad	k9	snad
dosud	dosud	k6eAd1	dosud
nezlezenou	zlezený	k2eNgFnSc4d1	zlezený
a	a	k8xC	a
nepojmenovanou	pojmenovaný	k2eNgFnSc4d1	nepojmenovaná
altajskou	altajský	k2eAgFnSc4d1	Altajská
horu	hora	k1gFnSc4	hora
po	po	k7c6	po
českém	český	k2eAgMnSc6d1	český
velikánovi	velikán	k1gMnSc6	velikán
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
komerčního	komerční	k2eAgNnSc2d1	komerční
pojetí	pojetí	k1gNnSc2	pojetí
celé	celý	k2eAgFnSc2d1	celá
expedice	expedice	k1gFnSc2	expedice
byla	být	k5eAaImAgFnS	být
důvodem	důvod	k1gInSc7	důvod
nesouhlasu	nesouhlas	k1gInSc2	nesouhlas
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc1	všechen
altajské	altajský	k2eAgFnPc1d1	Altajská
hory	hora	k1gFnPc1	hora
již	již	k6eAd1	již
velmi	velmi	k6eAd1	velmi
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
mají	mít	k5eAaImIp3nP	mít
místní	místní	k2eAgInPc4d1	místní
názvy	název	k1gInPc4	název
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
navíc	navíc	k6eAd1	navíc
podle	podle	k7c2	podle
místních	místní	k2eAgFnPc2d1	místní
zvyklostí	zvyklost	k1gFnPc2	zvyklost
nenesou	nést	k5eNaImIp3nP	nést
jména	jméno	k1gNnPc4	jméno
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
s	s	k7c7	s
výrazně	výrazně	k6eAd1	výrazně
nižšími	nízký	k2eAgInPc7d2	nižší
finančními	finanční	k2eAgInPc7d1	finanční
náklady	náklad	k1gInPc7	náklad
byla	být	k5eAaImAgFnS	být
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
malá	malý	k2eAgFnSc1d1	malá
expedice	expedice	k1gFnSc1	expedice
čtyř	čtyři	k4xCgMnPc2	čtyři
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
na	na	k7c4	na
horu	hora	k1gFnSc4	hora
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
a	a	k8xC	a
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
tuto	tento	k3xDgFnSc4	tento
horu	hora	k1gFnSc4	hora
Tekelju	Tekelju	k1gMnSc2	Tekelju
Bažy	Baža	k1gMnSc2	Baža
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
vydala	vydat	k5eAaPmAgFnS	vydat
Česká	český	k2eAgFnSc1d1	Česká
pošta	pošta	k1gFnSc1	pošta
známky	známka	k1gFnSc2	známka
v	v	k7c6	v
nominální	nominální	k2eAgFnSc6d1	nominální
hodnotě	hodnota	k1gFnSc6	hodnota
A	a	k8xC	a
a	a	k8xC	a
Z	z	k7c2	z
(	(	kIx(	(
<g/>
aktuálně	aktuálně	k6eAd1	aktuálně
16	[number]	k4	16
a	a	k8xC	a
32	[number]	k4	32
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
fiktivní	fiktivní	k2eAgFnSc4d1	fiktivní
podobiznu	podobizna	k1gFnSc4	podobizna
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
a	a	k8xC	a
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
Jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
136	[number]	k4	136
možných	možný	k2eAgFnPc2d1	možná
podob	podoba	k1gFnPc2	podoba
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
–	–	k?	–
vynálezce	vynálezce	k1gMnSc1	vynálezce
kruhové	kruhový	k2eAgFnPc4d1	kruhová
známky	známka	k1gFnPc4	známka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
námětu	námět	k1gInSc2	námět
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
herců	herec	k1gMnPc2	herec
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Weigel	Weigel	k1gMnSc1	Weigel
a	a	k8xC	a
grafickou	grafický	k2eAgFnSc4d1	grafická
podobu	podoba	k1gFnSc4	podoba
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
jeho	jeho	k3xOp3gFnSc4	jeho
syn	syn	k1gMnSc1	syn
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
první	první	k4xOgFnSc4	první
známku	známka	k1gFnSc4	známka
kruhového	kruhový	k2eAgInSc2d1	kruhový
tvaru	tvar	k1gInSc2	tvar
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
českého	český	k2eAgNnSc2d1	české
poštovnictví	poštovnictví	k1gNnSc2	poštovnictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2013	[number]	k4	2013
vydala	vydat	k5eAaPmAgFnS	vydat
Česká	český	k2eAgFnSc1d1	Česká
mincovna	mincovna	k1gFnSc1	mincovna
pamětní	pamětní	k2eAgFnSc2d1	pamětní
medaile	medaile	k1gFnSc2	medaile
k	k	k7c3	k
45	[number]	k4	45
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Křest	křest	k1gInSc1	křest
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
Malostranské	malostranský	k2eAgFnSc6d1	Malostranská
besedě	beseda	k1gFnSc6	beseda
<g/>
.	.	kIx.	.
</s>
<s>
Medaile	medaile	k1gFnPc1	medaile
jsou	být	k5eAaImIp3nP	být
vyraženy	vyražen	k2eAgFnPc1d1	vyražena
ve	v	k7c6	v
zlatě	zlato	k1gNnSc6	zlato
<g/>
,	,	kIx,	,
stříbře	stříbro	k1gNnSc6	stříbro
a	a	k8xC	a
mosazi	mosaz	k1gFnSc6	mosaz
<g/>
.	.	kIx.	.
</s>
<s>
Medaile	medaile	k1gFnSc1	medaile
jsou	být	k5eAaImIp3nP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
emisi	emise	k1gFnSc6	emise
–	–	k?	–
zlatých	zlatá	k1gFnPc2	zlatá
500	[number]	k4	500
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
stříbrných	stříbrný	k2eAgInPc2d1	stříbrný
1000	[number]	k4	1000
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
mosazných	mosazný	k2eAgInPc2d1	mosazný
2000	[number]	k4	2000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
z	z	k7c2	z
výtěžků	výtěžek	k1gInPc2	výtěžek
je	být	k5eAaImIp3nS	být
určena	určen	k2eAgFnSc1d1	určena
organizaci	organizace	k1gFnSc4	organizace
Nadace	nadace	k1gFnSc2	nadace
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
<s>
Autory	autor	k1gMnPc7	autor
medaile	medaile	k1gFnSc2	medaile
jsou	být	k5eAaImIp3nP	být
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Weigel	Weigel	k1gMnSc1	Weigel
a	a	k8xC	a
Milan	Milan	k1gMnSc1	Milan
Sypěna	Sypěna	k1gFnSc1	Sypěna
<g/>
.	.	kIx.	.
</s>
