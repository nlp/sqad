<s>
Verona	Verona	k1gFnSc1	Verona
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
hudebník	hudebník	k1gMnSc1	hudebník
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
Petr	Petr	k1gMnSc1	Petr
Fider	Fider	k1gMnSc1	Fider
(	(	kIx(	(
<g/>
také	také	k9	také
producent	producent	k1gMnSc1	producent
kapel	kapela	k1gFnPc2	kapela
Holki	Holk	k1gFnSc2	Holk
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc5	Le
Monde	Mond	k1gMnSc5	Mond
<g/>
,	,	kIx,	,
Kaamo	Kaama	k1gFnSc5	Kaama
<g/>
)	)	kIx)	)
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Markéta	Markéta	k1gFnSc1	Markéta
Jakšlová	Jakšlová	k1gFnSc1	Jakšlová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
Náhodou	náhodou	k6eAd1	náhodou
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
vyšel	vyjít	k5eAaPmAgMnS	vyjít
jako	jako	k9	jako
druhý	druhý	k4xOgInSc4	druhý
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
singl	singl	k1gInSc4	singl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
ihned	ihned	k6eAd1	ihned
hitem	hit	k1gInSc7	hit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
vyšly	vyjít	k5eAaPmAgInP	vyjít
ještě	ještě	k6eAd1	ještě
další	další	k2eAgInPc1d1	další
tři	tři	k4xCgInPc1	tři
singly	singl	k1gInPc1	singl
Krásnej	Krásnej	k?	Krásnej
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
Co	co	k9	co
nejdýl	nejdýl	k?	nejdýl
<g/>
,	,	kIx,	,
Rovnováha	rovnováha	k1gFnSc1	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Rovnováhy	rovnováha	k1gFnSc2	rovnováha
byl	být	k5eAaImAgInS	být
ke	k	k7c3	k
všem	všecek	k3xTgMnPc3	všecek
natočen	natočen	k2eAgInSc4d1	natočen
videoklip	videoklip	k1gInSc4	videoklip
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
rádiích	rádio	k1gNnPc6	rádio
song	song	k1gInSc4	song
Tvář	tvář	k1gFnSc1	tvář
velkoměsta	velkoměsto	k1gNnSc2	velkoměsto
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
oficiální	oficiální	k2eAgInSc1d1	oficiální
singl	singl	k1gInSc1	singl
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
následovalo	následovat	k5eAaImAgNnS	následovat
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
pilotní	pilotní	k2eAgInSc1d1	pilotní
singl	singl	k1gInSc1	singl
Nejsi	být	k5eNaImIp2nS	být
sám	sám	k3xTgInSc4	sám
poprvé	poprvé	k6eAd1	poprvé
Verona	Verona	k1gFnSc1	Verona
představila	představit	k5eAaPmAgFnS	představit
na	na	k7c4	na
vystoupení	vystoupení	k1gNnSc4	vystoupení
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
předkapely	předkapela	k1gFnSc2	předkapela
koncertu	koncert	k1gInSc2	koncert
německé	německý	k2eAgFnSc2d1	německá
skupiny	skupina	k1gFnSc2	skupina
Scooter	Scootra	k1gFnPc2	Scootra
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
zde	zde	k6eAd1	zde
Verona	Verona	k1gFnSc1	Verona
natočila	natočit	k5eAaBmAgFnS	natočit
záběry	záběr	k1gInPc4	záběr
pro	pro	k7c4	pro
videoklip	videoklip	k1gInSc4	videoklip
k	k	k7c3	k
singlu	singl	k1gInSc3	singl
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
singlem	singl	k1gInSc7	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
Nejsi	být	k5eNaImIp2nS	být
sám	sám	k3xTgMnSc1	sám
je	být	k5eAaImIp3nS	být
Kde	kde	k6eAd1	kde
lásku	láska	k1gFnSc4	láska
brát	brát	k5eAaImF	brát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
kapela	kapela	k1gFnSc1	kapela
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
své	svůj	k3xOyFgNnSc4	svůj
působení	působení	k1gNnSc4	působení
i	i	k9	i
do	do	k7c2	do
sousedního	sousední	k2eAgNnSc2d1	sousední
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
Náhodou	náhodou	k6eAd1	náhodou
nejsi	být	k5eNaImIp2nS	být
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
mix	mix	k1gInSc1	mix
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
jejích	její	k3xOp3gInPc2	její
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
Nejsi	být	k5eNaImIp2nS	být
sám	sám	k3xTgInSc4	sám
nahráli	nahrát	k5eAaPmAgMnP	nahrát
také	také	k6eAd1	také
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Life	Lif	k1gMnSc2	Lif
Is	Is	k1gMnSc2	Is
Fun	Fun	k1gMnSc2	Fun
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
bonusem	bonus	k1gInSc7	bonus
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
třetím	třetí	k4xOgNnSc6	třetí
studiovém	studiový	k2eAgNnSc6d1	studiové
albu	album	k1gNnSc6	album
Jen	jen	k6eAd1	jen
tobě	ty	k3xPp2nSc3	ty
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
jako	jako	k9	jako
pilotní	pilotní	k2eAgFnSc1d1	pilotní
vychází	vycházet	k5eAaImIp3nS	vycházet
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
píseň	píseň	k1gFnSc1	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Videoklip	videoklip	k1gInSc4	videoklip
ke	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
singlu	singl	k1gInSc3	singl
Den	den	k1gInSc4	den
co	co	k8xS	co
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgMnS	natáčet
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
pak	pak	k6eAd1	pak
kapela	kapela	k1gFnSc1	kapela
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
album	album	k1gNnSc4	album
Girotondo	Girotondo	k6eAd1	Girotondo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
singl	singl	k1gInSc1	singl
Girotondo	Girotondo	k6eAd1	Girotondo
byl	být	k5eAaImAgInS	být
nazpívaný	nazpívaný	k2eAgInSc1d1	nazpívaný
v	v	k7c6	v
italštině	italština	k1gFnSc6	italština
<g/>
,	,	kIx,	,
text	text	k1gInSc4	text
napsal	napsat	k5eAaPmAgMnS	napsat
David	David	k1gMnSc1	David
Mattioli	Mattiole	k1gFnSc4	Mattiole
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc4	všechen
ostatní	ostatní	k2eAgFnPc4d1	ostatní
písně	píseň	k1gFnPc4	píseň
otextoval	otextovat	k5eAaPmAgMnS	otextovat
dvorní	dvorní	k2eAgMnSc1d1	dvorní
textař	textař	k1gMnSc1	textař
Verony	Verona	k1gFnSc2	Verona
Viktor	Viktor	k1gMnSc1	Viktor
Dyk	Dyk	k?	Dyk
<g/>
,	,	kIx,	,
ty	ten	k3xDgMnPc4	ten
novější	nový	k2eAgMnSc1d2	novější
zase	zase	k9	zase
Tom	Tom	k1gMnSc1	Tom
Malár	Malár	k1gMnSc1	Malár
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
Verona	Verona	k1gFnSc1	Verona
vydává	vydávat	k5eAaImIp3nS	vydávat
své	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgFnPc4	první
DVD	DVD	kA	DVD
Videokolekce	Videokolekce	k1gFnPc1	Videokolekce
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
8	[number]	k4	8
videoklipů	videoklip	k1gInPc2	videoklip
a	a	k8xC	a
karaoke	karaoke	k1gFnPc2	karaoke
verze	verze	k1gFnSc2	verze
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
také	také	k9	také
singl	singl	k1gInSc1	singl
nazpívaný	nazpívaný	k2eAgInSc1d1	nazpívaný
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
You	You	k1gFnPc2	You
Gotta	Gott	k1gMnSc2	Gott
Move	Mov	k1gInSc2	Mov
On	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
i	i	k9	i
do	do	k7c2	do
hitparád	hitparáda	k1gFnPc2	hitparáda
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
,	,	kIx,	,
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
a	a	k8xC	a
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vychází	vycházet	k5eAaImIp3nS	vycházet
Best	Best	k1gMnSc1	Best
of	of	k?	of
<g/>
,	,	kIx,	,
doplněné	doplněný	k2eAgInPc1d1	doplněný
novými	nový	k2eAgInPc7d1	nový
singly	singl	k1gInPc7	singl
Stay	Staa	k1gFnSc2	Staa
With	With	k1gInSc4	With
Me	Me	k1gFnSc2	Me
a	a	k8xC	a
coververzí	coververze	k1gFnSc7	coververze
písně	píseň	k1gFnSc2	píseň
Lenky	Lenka	k1gFnSc2	Lenka
Filipové	Filipová	k1gFnSc2	Filipová
Za	za	k7c4	za
všechno	všechen	k3xTgNnSc4	všechen
může	moct	k5eAaImIp3nS	moct
čas	čas	k1gInSc1	čas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
pak	pak	k6eAd1	pak
kapela	kapela	k1gFnSc1	kapela
vydává	vydávat	k5eAaImIp3nS	vydávat
singl	singl	k1gInSc4	singl
Do	do	k7c2	do
You	You	k1gFnSc2	You
Really	Realla	k1gMnSc2	Realla
Wanna	Wann	k1gMnSc2	Wann
Know	Know	k1gMnSc2	Know
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgMnSc3	jenž
natočila	natočit	k5eAaBmAgFnS	natočit
videoklip	videoklip	k1gInSc4	videoklip
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
singly	singl	k1gInPc1	singl
Ztracená	ztracený	k2eAgNnPc4d1	ztracené
bloudím	bloudit	k5eAaImIp1nS	bloudit
a	a	k8xC	a
Hey	Hey	k1gMnSc1	Hey
Boy	boy	k1gMnSc1	boy
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dostaly	dostat	k5eAaPmAgInP	dostat
na	na	k7c4	na
první	první	k4xOgFnPc4	první
příčky	příčka	k1gFnPc4	příčka
slovenské	slovenský	k2eAgFnSc2d1	slovenská
i	i	k8xC	i
české	český	k2eAgFnSc2d1	Česká
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Singly	singl	k1gInPc1	singl
od	od	k7c2	od
posledního	poslední	k2eAgNnSc2d1	poslední
alba	album	k1gNnSc2	album
Best	Best	k1gMnSc1	Best
of	of	k?	of
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
nového	nový	k2eAgNnSc2d1	nové
studiového	studiový	k2eAgNnSc2d1	studiové
alba	album	k1gNnSc2	album
Den	dna	k1gFnPc2	dna
otevřených	otevřený	k2eAgFnPc2d1	otevřená
dveří	dveře	k1gFnPc2	dveře
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgMnSc2	který
ještě	ještě	k6eAd1	ještě
vyšel	vyjít	k5eAaPmAgMnS	vyjít
singl	singl	k1gInSc4	singl
Bez	bez	k7c2	bez
tebe	ty	k3xPp2nSc2	ty
<g/>
.	.	kIx.	.
</s>
<s>
Náhodou	náhodou	k6eAd1	náhodou
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Nejsi	být	k5eNaImIp2nS	být
sám	sám	k3xTgMnSc1	sám
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Náhodou	náhodou	k6eAd1	náhodou
nejsi	být	k5eNaImIp2nS	být
sám	sám	k3xTgInSc1	sám
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Jen	jen	k8xS	jen
tobě	ty	k3xPp2nSc3	ty
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Girotondo	Girotondo	k6eAd1	Girotondo
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Best	Best	k1gInSc1	Best
of	of	k?	of
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Komplet	komplet	k2eAgInSc2d1	komplet
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Den	den	k1gInSc1	den
otevřených	otevřený	k2eAgFnPc2d1	otevřená
dveří	dveře	k1gFnPc2	dveře
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Meziprostor	meziprostor	k1gInSc1	meziprostor
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Verona	Verona	k1gFnSc1	Verona
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Verona	Verona	k1gFnSc1	Verona
–	–	k?	–
Stay	Staa	k1gFnSc2	Staa
with	with	k1gMnSc1	with
me	me	k?	me
<g/>
,	,	kIx,	,
YouTube	YouTub	k1gInSc5	YouTub
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
video	video	k1gNnSc1	video
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Diskografie	diskografie	k1gFnSc1	diskografie
</s>
