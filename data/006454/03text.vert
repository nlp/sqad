<s>
Nutella	Nutella	k1gFnSc1	Nutella
je	být	k5eAaImIp3nS	být
obchodní	obchodní	k2eAgFnSc1d1	obchodní
značka	značka	k1gFnSc1	značka
čokoládové	čokoládový	k2eAgFnSc2d1	čokoládová
pomazánky	pomazánka	k1gFnSc2	pomazánka
z	z	k7c2	z
lískových	lískový	k2eAgInPc2d1	lískový
oříšků	oříšek	k1gInPc2	oříšek
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
italská	italský	k2eAgFnSc1d1	italská
společnost	společnost	k1gFnSc1	společnost
Ferrero	Ferrero	k1gNnSc4	Ferrero
<g/>
.	.	kIx.	.
</s>
<s>
Recept	recept	k1gInSc1	recept
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
upravením	upravení	k1gNnSc7	upravení
receptury	receptura	k1gFnSc2	receptura
dříve	dříve	k6eAd2	dříve
vyráběné	vyráběný	k2eAgFnPc1d1	vyráběná
pomazánky	pomazánka	k1gFnPc1	pomazánka
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInSc1d2	starší
produkt	produkt	k1gInSc1	produkt
<g/>
,	,	kIx,	,
Gianduia	Gianduia	k1gFnSc1	Gianduia
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
směs	směs	k1gFnSc1	směs
obsahující	obsahující	k2eAgFnSc1d1	obsahující
přibližně	přibližně	k6eAd1	přibližně
50	[number]	k4	50
%	%	kIx~	%
mandlí	mandle	k1gFnPc2	mandle
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
lískových	lískový	k2eAgInPc2d1	lískový
ořechů	ořech	k1gInPc2	ořech
a	a	k8xC	a
50	[number]	k4	50
%	%	kIx~	%
čokolády	čokoláda	k1gFnSc2	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
vyrábět	vyrábět	k5eAaImF	vyrábět
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
Piemontu	Piemont	k1gInSc6	Piemont
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
zdanění	zdanění	k1gNnSc1	zdanění
kakaových	kakaový	k2eAgInPc2d1	kakaový
bobů	bob	k1gInPc2	bob
ztížilo	ztížit	k5eAaPmAgNnS	ztížit
výrobu	výroba	k1gFnSc4	výroba
běžné	běžný	k2eAgFnSc2d1	běžná
čokolády	čokoláda	k1gFnSc2	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
v	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
změněna	změnit	k5eAaPmNgFnS	změnit
na	na	k7c4	na
krémovitý	krémovitý	k2eAgInSc4d1	krémovitý
produkt	produkt	k1gInSc4	produkt
Supercrema	Supercremum	k1gNnSc2	Supercremum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
bylo	být	k5eAaImAgNnS	být
složení	složení	k1gNnSc1	složení
znovu	znovu	k6eAd1	znovu
upraveno	upravit	k5eAaPmNgNnS	upravit
a	a	k8xC	a
produkt	produkt	k1gInSc1	produkt
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
definitivně	definitivně	k6eAd1	definitivně
přejmenován	přejmenován	k2eAgMnSc1d1	přejmenován
na	na	k7c4	na
Nutella	Nutello	k1gNnPc4	Nutello
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
sklenice	sklenice	k1gFnSc1	sklenice
Nutelly	Nutella	k1gFnSc2	Nutella
opustila	opustit	k5eAaPmAgFnS	opustit
továrnu	továrna	k1gFnSc4	továrna
Ferrero	Ferrero	k1gNnSc4	Ferrero
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
štítku	štítek	k1gInSc2	štítek
výrobku	výrobek	k1gInSc2	výrobek
jsou	být	k5eAaImIp3nP	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
ingredience	ingredience	k1gFnSc1	ingredience
Nutelly	Nutella	k1gFnSc2	Nutella
cukr	cukr	k1gInSc1	cukr
a	a	k8xC	a
rostlinné	rostlinný	k2eAgInPc1d1	rostlinný
oleje	olej	k1gInPc1	olej
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
palmový	palmový	k2eAgInSc1d1	palmový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následují	následovat	k5eAaImIp3nP	následovat
lískové	lískový	k2eAgInPc1d1	lískový
ořechy	ořech	k1gInPc1	ořech
<g/>
,	,	kIx,	,
kakao	kakao	k1gNnSc1	kakao
a	a	k8xC	a
sušené	sušený	k2eAgNnSc1d1	sušené
odstředěné	odstředěný	k2eAgNnSc1d1	odstředěné
mléko	mléko	k1gNnSc1	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Nutella	Nutella	k1gFnSc1	Nutella
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
prodávána	prodávat	k5eAaImNgFnS	prodávat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
oříškový	oříškový	k2eAgInSc4d1	oříškový
krém	krém	k1gInSc4	krém
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
italského	italský	k2eAgNnSc2d1	italské
práva	právo	k1gNnSc2	právo
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
označena	označit	k5eAaPmNgFnS	označit
jako	jako	k8xS	jako
čokoládový	čokoládový	k2eAgInSc4d1	čokoládový
krém	krém	k1gInSc4	krém
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nesplňuje	splňovat	k5eNaImIp3nS	splňovat
minimální	minimální	k2eAgFnSc4d1	minimální
koncentraci	koncentrace	k1gFnSc4	koncentrace
kakaové	kakaový	k2eAgFnSc2d1	kakaová
sušiny	sušina	k1gFnSc2	sušina
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
polovina	polovina	k1gFnSc1	polovina
kalorií	kalorie	k1gFnPc2	kalorie
v	v	k7c6	v
Nutelle	Nutella	k1gFnSc6	Nutella
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
tuku	tuk	k1gInSc2	tuk
(	(	kIx(	(
<g/>
11	[number]	k4	11
g	g	kA	g
v	v	k7c4	v
37	[number]	k4	37
<g/>
g	g	kA	g
porci	porce	k1gFnSc4	porce
či	či	k8xC	či
99	[number]	k4	99
kcal	kcala	k1gFnPc2	kcala
z	z	k7c2	z
200	[number]	k4	200
kcal	kcala	k1gFnPc2	kcala
<g/>
)	)	kIx)	)
a	a	k8xC	a
asi	asi	k9	asi
40	[number]	k4	40
%	%	kIx~	%
kalorií	kalorie	k1gFnPc2	kalorie
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
cukru	cukr	k1gInSc2	cukr
(	(	kIx(	(
<g/>
20	[number]	k4	20
g	g	kA	g
<g/>
,	,	kIx,	,
80	[number]	k4	80
kcal	kcala	k1gFnPc2	kcala
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
spotřebitele	spotřebitel	k1gMnPc4	spotřebitel
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
výrobce	výrobce	k1gMnSc1	výrobce
neuvádí	uvádět	k5eNaImIp3nS	uvádět
zemi	zem	k1gFnSc4	zem
původu	původ	k1gInSc2	původ
výrobku	výrobek	k1gInSc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nejprodávanějších	prodávaný	k2eAgFnPc2d3	nejprodávanější
velikostí	velikost	k1gFnPc2	velikost
balení	balení	k1gNnSc2	balení
lze	lze	k6eAd1	lze
ovšem	ovšem	k9	ovšem
zemi	zem	k1gFnSc4	zem
původu	původ	k1gInSc2	původ
zjistit	zjistit	k5eAaPmF	zjistit
z	z	k7c2	z
razítka	razítko	k1gNnSc2	razítko
na	na	k7c6	na
boční	boční	k2eAgFnSc6d1	boční
straně	strana	k1gFnSc6	strana
plastového	plastový	k2eAgNnSc2d1	plastové
víčka	víčko	k1gNnSc2	víčko
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
písmeno	písmeno	k1gNnSc1	písmeno
kódu	kód	k1gInSc2	kód
umístěného	umístěný	k2eAgInSc2d1	umístěný
pod	pod	k7c7	pod
datem	datum	k1gNnSc7	datum
minimální	minimální	k2eAgFnSc2d1	minimální
trvanlivosti	trvanlivost	k1gFnSc2	trvanlivost
označuje	označovat	k5eAaImIp3nS	označovat
továrnu	továrna	k1gFnSc4	továrna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
byla	být	k5eAaImAgFnS	být
pomazánka	pomazánka	k1gFnSc1	pomazánka
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
<g/>
:	:	kIx,	:
W	W	kA	W
=	=	kIx~	=
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
G	G	kA	G
=	=	kIx~	=
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
A	a	k8xC	a
=	=	kIx~	=
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
pochybností	pochybnost	k1gFnPc2	pochybnost
lze	lze	k6eAd1	lze
zemi	zem	k1gFnSc4	zem
původu	původ	k1gInSc2	původ
vyčíst	vyčíst	k5eAaPmF	vyčíst
v	v	k7c6	v
informaci	informace	k1gFnSc6	informace
určené	určený	k2eAgFnPc1d1	určená
pro	pro	k7c4	pro
rumunský	rumunský	k2eAgInSc4d1	rumunský
trh	trh	k1gInSc4	trh
(	(	kIx(	(
<g/>
RO	RO	kA	RO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
legislativa	legislativa	k1gFnSc1	legislativa
uvedení	uvedení	k1gNnSc2	uvedení
této	tento	k3xDgFnSc2	tento
informace	informace	k1gFnSc2	informace
přikazuje	přikazovat	k5eAaImIp3nS	přikazovat
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
v	v	k7c6	v
chuti	chuť	k1gFnSc6	chuť
i	i	k8xC	i
barvě	barva	k1gFnSc6	barva
je	být	k5eAaImIp3nS	být
prokazatelný	prokazatelný	k2eAgInSc1d1	prokazatelný
v	v	k7c4	v
neprospěch	neprospěch	k1gInSc4	neprospěch
české	český	k2eAgFnSc2d1	Česká
verze	verze	k1gFnSc2	verze
<g/>
,	,	kIx,	,
vyráběné	vyráběný	k2eAgInPc1d1	vyráběný
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Světlejší	světlý	k2eAgFnSc1d2	světlejší
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
intenzivní	intenzivní	k2eAgFnSc4d1	intenzivní
chuť	chuť	k1gFnSc4	chuť
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
i	i	k9	i
vyšší	vysoký	k2eAgFnSc1d2	vyšší
cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
cca	cca	kA	cca
o	o	k7c4	o
40	[number]	k4	40
<g/>
%	%	kIx~	%
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
již	již	k9	již
několik	několik	k4yIc1	několik
průzkumů	průzkum	k1gInPc2	průzkum
v	v	k7c6	v
ČR	ČR	kA	ČR
i	i	k9	i
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
informace	informace	k1gFnPc4	informace
jsou	být	k5eAaImIp3nP	být
dohledatelné	dohledatelný	k2eAgInPc1d1	dohledatelný
včetně	včetně	k7c2	včetně
výsledků	výsledek	k1gInPc2	výsledek
nezávislých	závislý	k2eNgInPc2d1	nezávislý
testů	test	k1gInPc2	test
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
