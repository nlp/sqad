<s>
Úmluva	úmluva	k1gFnSc1	úmluva
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
o	o	k7c6	o
prevenci	prevence	k1gFnSc6	prevence
a	a	k8xC	a
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
násilí	násilí	k1gNnSc3	násilí
na	na	k7c6	na
ženách	žena	k1gFnPc6	žena
a	a	k8xC	a
domácímu	domácí	k2eAgNnSc3d1	domácí
násilí	násilí	k1gNnSc3	násilí
(	(	kIx(	(
<g/>
Istanbulská	istanbulský	k2eAgFnSc1d1	Istanbulská
úmluva	úmluva	k1gFnSc1	úmluva
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
smlouva	smlouva	k1gFnSc1	smlouva
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
posuzující	posuzující	k2eAgFnPc4d1	posuzující
především	především	k6eAd1	především
všechny	všechen	k3xTgInPc4	všechen
druhy	druh	k1gInPc4	druh
násilí	násilí	k1gNnSc2	násilí
vůči	vůči	k7c3	vůči
ženám	žena	k1gFnPc3	žena
jako	jako	k8xS	jako
formu	forma	k1gFnSc4	forma
historicky	historicky	k6eAd1	historicky
a	a	k8xC	a
kulturně	kulturně	k6eAd1	kulturně
podmíněné	podmíněný	k2eAgFnPc4d1	podmíněná
diskriminace	diskriminace	k1gFnPc4	diskriminace
<g/>
.	.	kIx.	.
</s>
