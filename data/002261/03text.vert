<s>
Anglické	anglický	k2eAgNnSc1d1	anglické
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Kingdom	Kingdom	k1gInSc1	Kingdom
of	of	k?	of
England	England	k1gInSc1	England
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
samostatný	samostatný	k2eAgInSc1d1	samostatný
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
počátky	počátek	k1gInPc1	počátek
jako	jako	k8xC	jako
sjednoceného	sjednocený	k2eAgInSc2d1	sjednocený
státu	stát	k1gInSc2	stát
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
9	[number]	k4	9
<g/>
.	.	kIx.	.
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
který	který	k3yIgInSc1	který
existoval	existovat	k5eAaImAgInS	existovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1707	[number]	k4	1707
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
asi	asi	k9	asi
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc4	třetina
území	území	k1gNnSc2	území
jihu	jih	k1gInSc3	jih
ostrova	ostrov	k1gInSc2	ostrov
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
množství	množství	k1gNnSc2	množství
malých	malý	k2eAgInPc2d1	malý
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
927	[number]	k4	927
král	král	k1gMnSc1	král
Ethelstan	Ethelstan	k1gInSc4	Ethelstan
dobyl	dobýt	k5eAaPmAgMnS	dobýt
Northumbrii	Northumbrie	k1gFnSc4	Northumbrie
a	a	k8xC	a
historicky	historicky	k6eAd1	historicky
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
celým	celý	k2eAgNnSc7d1	celé
územím	území	k1gNnSc7	území
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgNnSc1d2	pozdější
ovládnutí	ovládnutí	k1gNnSc1	ovládnutí
Walesu	Wales	k1gInSc2	Wales
Normany	Norman	k1gMnPc7	Norman
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1067	[number]	k4	1067
až	až	k9	až
1283	[number]	k4	1283
(	(	kIx(	(
<g/>
formálně	formálně	k6eAd1	formálně
stvrzené	stvrzený	k2eAgFnPc1d1	stvrzená
roku	rok	k1gInSc2	rok
1284	[number]	k4	1284
<g/>
)	)	kIx)	)
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
jeho	jeho	k3xOp3gFnSc4	jeho
kontrolu	kontrola	k1gFnSc4	kontrola
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
platnost	platnost	k1gFnSc4	platnost
anglických	anglický	k2eAgMnPc2d1	anglický
zákonů	zákon	k1gInPc2	zákon
i	i	k9	i
na	na	k7c6	na
velšském	velšský	k2eAgNnSc6d1	Velšské
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Anglické	anglický	k2eAgNnSc1d1	anglické
království	království	k1gNnSc1	království
se	se	k3xPyFc4	se
sloučilo	sloučit	k5eAaPmAgNnS	sloučit
se	s	k7c7	s
sousedním	sousední	k2eAgNnSc7d1	sousední
Skotským	skotský	k2eAgNnSc7d1	skotské
královstvím	království	k1gNnSc7	království
zákony	zákon	k1gInPc1	zákon
o	o	k7c4	o
sjednocení	sjednocení	k1gNnSc4	sjednocení
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1707	[number]	k4	1707
a	a	k8xC	a
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
tak	tak	k9	tak
Království	království	k1gNnSc1	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Anglické	anglický	k2eAgNnSc1d1	anglické
království	království	k1gNnSc1	království
nemá	mít	k5eNaImIp3nS	mít
přesné	přesný	k2eAgNnSc1d1	přesné
datum	datum	k1gNnSc1	datum
založení	založení	k1gNnSc2	založení
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
anglosaských	anglosaský	k2eAgNnPc2d1	anglosaské
království	království	k1gNnPc2	království
(	(	kIx(	(
<g/>
označovaných	označovaný	k2eAgFnPc2d1	označovaná
také	také	k9	také
jako	jako	k9	jako
Heptarchie	Heptarchie	k1gFnSc1	Heptarchie
<g/>
)	)	kIx)	)
-	-	kIx~	-
Východní	východní	k2eAgFnSc2d1	východní
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Mercie	Mercie	k1gFnSc2	Mercie
<g/>
,	,	kIx,	,
Northumbrie	Northumbrie	k1gFnSc2	Northumbrie
<g/>
,	,	kIx,	,
Kentu	Kent	k1gInSc2	Kent
<g/>
,	,	kIx,	,
Essexu	Essex	k1gInSc2	Essex
<g/>
,	,	kIx,	,
Sussexu	Sussex	k1gInSc2	Sussex
a	a	k8xC	a
Wessexu	Wessex	k1gInSc2	Wessex
<g/>
.	.	kIx.	.
</s>
<s>
Invaze	invaze	k1gFnSc1	invaze
Vikingů	Viking	k1gMnPc2	Viking
rozbila	rozbít	k5eAaPmAgFnS	rozbít
základy	základ	k1gInPc4	základ
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Obnovení	obnovení	k1gNnSc1	obnovení
moci	moc	k1gFnSc2	moc
nad	nad	k7c7	nad
původním	původní	k2eAgNnSc7d1	původní
územím	území	k1gNnSc7	území
v	v	k7c6	v
roku	rok	k1gInSc6	rok
927	[number]	k4	927
králem	král	k1gMnSc7	král
Ethelstanem	Ethelstan	k1gInSc7	Ethelstan
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
sjednoceného	sjednocený	k2eAgNnSc2d1	sjednocené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Anglické	anglický	k2eAgNnSc1d1	anglické
a	a	k8xC	a
skotské	skotský	k2eAgNnSc1d1	skotské
království	království	k1gNnSc1	království
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1603	[number]	k4	1603
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
nástupcem	nástupce	k1gMnSc7	nástupce
Alžběty	Alžběta	k1gFnSc2	Alžběta
I.	I.	kA	I.
stal	stát	k5eAaPmAgMnS	stát
skotský	skotský	k2eAgMnSc1d1	skotský
král	král	k1gMnSc1	král
Jakub	Jakub	k1gMnSc1	Jakub
I.	I.	kA	I.
<g/>
,	,	kIx,	,
staly	stát	k5eAaPmAgFnP	stát
personální	personální	k2eAgFnSc7d1	personální
unií	unie	k1gFnSc7	unie
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
samostatné	samostatný	k2eAgFnPc1d1	samostatná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgInP	být
spojeny	spojit	k5eAaPmNgInP	spojit
jedním	jeden	k4xCgMnSc7	jeden
panovníkem	panovník	k1gMnSc7	panovník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
dva	dva	k4xCgInPc4	dva
tituly	titul	k1gInPc4	titul
-	-	kIx~	-
byl	být	k5eAaImAgMnS	být
králem	král	k1gMnSc7	král
anglickým	anglický	k2eAgMnSc7d1	anglický
a	a	k8xC	a
králem	král	k1gMnSc7	král
skotským	skotský	k2eAgMnSc7d1	skotský
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
skotským	skotský	k2eAgInSc7d1	skotský
a	a	k8xC	a
anglickým	anglický	k2eAgInSc7d1	anglický
parlamentem	parlament	k1gInSc7	parlament
přijaty	přijmout	k5eAaPmNgFnP	přijmout
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1707	[number]	k4	1707
zákony	zákon	k1gInPc1	zákon
o	o	k7c4	o
sjednocení	sjednocení	k1gNnSc4	sjednocení
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
ustaven	ustavit	k5eAaPmNgInS	ustavit
institut	institut	k1gInSc1	institut
jednoho	jeden	k4xCgMnSc2	jeden
panovníka	panovník	k1gMnSc2	panovník
sjednoceného	sjednocený	k2eAgInSc2d1	sjednocený
státu	stát	k1gInSc2	stát
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
parlament	parlament	k1gInSc4	parlament
(	(	kIx(	(
<g/>
parlament	parlament	k1gInSc1	parlament
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Královské	královský	k2eAgNnSc1d1	královské
sídlo	sídlo	k1gNnSc1	sídlo
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
nacházelo	nacházet	k5eAaImAgNnS	nacházet
ve	v	k7c6	v
Winchesteru	Winchester	k1gInSc6	Winchester
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Westminster	Westminster	k1gInSc4	Westminster
a	a	k8xC	a
Gloucester	Gloucester	k1gInSc4	Gloucester
byly	být	k5eAaImAgInP	být
považovány	považován	k2eAgInPc1d1	považován
<g/>
,	,	kIx,	,
za	za	k7c4	za
místo	místo	k1gNnSc4	místo
působení	působení	k1gNnSc2	působení
panovníka	panovník	k1gMnSc2	panovník
srovnatelného	srovnatelný	k2eAgInSc2d1	srovnatelný
významu	význam	k1gInSc2	význam
jako	jako	k8xS	jako
Winchester	Winchester	k1gInSc4	Winchester
<g/>
.	.	kIx.	.
</s>
<s>
Westminster	Westminster	k1gInSc1	Westminster
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
de	de	k?	de
facto	fact	k2eAgNnSc1d1	facto
královským	královský	k2eAgNnSc7d1	královské
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c4	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
tak	tak	k9	tak
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Anglického	anglický	k2eAgNnSc2d1	anglické
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
Království	království	k1gNnSc1	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
(	(	kIx(	(
<g/>
1707	[number]	k4	1707
-	-	kIx~	-
1801	[number]	k4	1801
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
(	(	kIx(	(
<g/>
1801	[number]	k4	1801
-	-	kIx~	-
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
až	až	k9	až
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
i	i	k8xC	i
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
anglických	anglický	k2eAgMnPc2d1	anglický
králů	král	k1gMnPc2	král
Dějiny	dějiny	k1gFnPc1	dějiny
Anglie	Anglie	k1gFnSc1	Anglie
Znak	znak	k1gInSc1	znak
Anglického	anglický	k2eAgNnSc2d1	anglické
království	království	k1gNnSc2	království
Dějiny	dějiny	k1gFnPc4	dějiny
Londýna	Londýn	k1gInSc2	Londýn
Anglie	Anglie	k1gFnSc2	Anglie
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Kingdom	Kingdom	k1gInSc1	Kingdom
of	of	k?	of
England	Englanda	k1gFnPc2	Englanda
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
