<s>
Nihonium	Nihonium	k1gNnSc1
</s>
<s>
Nihonium	Nihonium	k1gNnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
[	[	kIx(
<g/>
Rn	Rn	k1gFnSc1
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
10	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
7	#num#	k4
<g/>
p	p	k?
<g/>
1	#num#	k4
<g/>
(	(	kIx(
<g/>
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
thalliu	thallium	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
284	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nh	Nh	kA
</s>
<s>
113	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Nihonium	Nihonium	k1gNnSc1
<g/>
,	,	kIx,
Nh	Nh	k1gFnSc1
<g/>
,	,	kIx,
113	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
angl.	angl.	k?
Nihonium	Nihonium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
p	p	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
54084-70-7	54084-70-7	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
284,18	284,18	k4
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
[	[	kIx(
<g/>
Rn	Rn	k1gFnSc1
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
10	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
7	#num#	k4
<g/>
p	p	k?
<g/>
1	#num#	k4
<g/>
(	(	kIx(
<g/>
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
thalliu	thallium	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
předpokládané	předpokládaný	k2eAgFnSc3d1
pevné	pevný	k2eAgFnSc3d1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
278	#num#	k4
<g/>
Nh	Nh	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
α	α	k?
</s>
<s>
274	#num#	k4
<g/>
Rg	Rg	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
282	#num#	k4
<g/>
Nh	Nh	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
α	α	k?
</s>
<s>
278	#num#	k4
<g/>
Rg	Rg	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
283	#num#	k4
<g/>
Nh	Nh	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
α	α	k?
</s>
<s>
279	#num#	k4
<g/>
Rg	Rg	k1gFnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
284	#num#	k4
<g/>
Nh	Nh	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
0,48	0,48	k4
s	s	k7c7
</s>
<s>
α	α	k?
</s>
<s>
280	#num#	k4
<g/>
Rg	Rg	k1gFnSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
285	#num#	k4
<g/>
Nh	Nh	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
5,5	5,5	k4
s	s	k7c7
</s>
<s>
α	α	k?
</s>
<s>
281	#num#	k4
<g/>
Rg	Rg	k1gFnSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
286	#num#	k4
<g/>
Nh	Nh	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
20	#num#	k4
s	s	k7c7
</s>
<s>
α	α	k?
</s>
<s>
282	#num#	k4
<g/>
Rg	Rg	k1gFnSc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tl	Tl	k?
<g/>
⋏	⋏	k?
</s>
<s>
Kopernicium	Kopernicium	k1gNnSc1
≺	≺	k?
<g/>
Nh	Nh	k1gMnSc2
<g/>
≻	≻	k?
Flerovium	Flerovium	k1gNnSc1
</s>
<s>
Nihonium	Nihonium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Nh	Nh	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
transuran	transuran	k1gInSc4
připravený	připravený	k2eAgInSc4d1
alfa	alfa	k1gNnSc4
rozpadem	rozpad	k1gInSc7
moscovia	moscovium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2004	#num#	k4
publikoval	publikovat	k5eAaBmAgInS
tým	tým	k1gInSc1
ruských	ruský	k2eAgMnPc2d1
fyziků	fyzik	k1gMnPc2
z	z	k7c2
Dubny	duben	k1gInPc1
a	a	k8xC
amerických	americký	k2eAgMnPc2d1
vědců	vědec	k1gMnPc2
z	z	k7c2
Lawrence	Lawrence	k1gFnSc2
Berkeley	Berkelea	k1gFnSc2
National	National	k1gFnSc2
Laboratory	Laborator	k1gInPc1
zprávu	zpráva	k1gFnSc4
o	o	k7c6
přípravě	příprava	k1gFnSc6
nihonia	nihonium	k1gNnSc2
(	(	kIx(
<g/>
ununtria	ununtrium	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
moscovia	moscovia	k1gFnSc1
(	(	kIx(
<g/>
ununpentia	ununpentia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
4820	#num#	k4
Ca	ca	kA
+	+	kIx~
24395	#num#	k4
Am	Am	k1gFnSc2
→	→	k?
287,288	287,288	k4
Mc	Mc	k1gFnSc1
→	→	k?
283,284	283,284	k4
Nh	Nh	k1gFnSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
existenci	existence	k1gFnSc4
prvku	prvek	k1gInSc2
potvrdili	potvrdit	k5eAaPmAgMnP
japonští	japonský	k2eAgMnPc1d1
vědci	vědec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
provedli	provést	k5eAaPmAgMnP
jadernou	jaderný	k2eAgFnSc4d1
syntézu	syntéza	k1gFnSc4
atomů	atom	k1gInPc2
zinku	zinek	k1gInSc2
a	a	k8xC
bismutu	bismut	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
7030	#num#	k4
Zn	zn	kA
+	+	kIx~
20983	#num#	k4
Bi	Bi	k1gFnSc2
→	→	k?
279113	#num#	k4
Nh	Nh	k1gFnSc1
→	→	k?
278113	#num#	k4
Nh	Nh	k1gFnPc2
+	+	kIx~
10	#num#	k4
n	n	k0
</s>
<s>
Detekce	detekce	k1gFnSc1
278113	#num#	k4
Nh	Nh	k1gMnSc1
však	však	k9
vycházely	vycházet	k5eAaImAgFnP
z	z	k7c2
jeho	jeho	k3xOp3gFnPc2
rozpadových	rozpadový	k2eAgFnPc2d1
řad	řada	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgInPc6,k3yQgInPc6,k3yRgInPc6
se	se	k3xPyFc4
jeden	jeden	k4xCgMnSc1
z	z	k7c2
produktů	produkt	k1gInPc2
kaskády	kaskáda	k1gFnSc2
rozpadů	rozpad	k1gInPc2
alfa	alfa	k1gNnSc1
<g/>
,	,	kIx,
dubnium-	dubnium-	k?
<g/>
262	#num#	k4
<g/>
,	,	kIx,
rozpadal	rozpadat	k5eAaImAgInS,k5eAaPmAgInS
spontánním	spontánní	k2eAgNnSc7d1
štěpením	štěpení	k1gNnSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
neumožňovalo	umožňovat	k5eNaImAgNnS
jeho	jeho	k3xOp3gFnSc4
jednoznačnou	jednoznačný	k2eAgFnSc4d1
identifikaci	identifikace	k1gFnSc4
a	a	k8xC
tedy	tedy	k9
i	i	k9
identifikaci	identifikace	k1gFnSc4
Uut	Uut	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
společná	společný	k2eAgFnSc1d1
komise	komise	k1gFnSc1
IUPAC	IUPAC	kA
a	a	k8xC
IUPAP	IUPAP	kA
dosud	dosud	k6eAd1
Uut	Uut	k1gFnPc1
jako	jako	k9
nově	nově	k6eAd1
objevený	objevený	k2eAgInSc4d1
prvek	prvek	k1gInSc4
neuznala	uznat	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
se	se	k3xPyFc4
však	však	k8xC
vědeckému	vědecký	k2eAgInSc3d1
týmu	tým	k1gInSc3
japonského	japonský	k2eAgInSc2d1
výzkumného	výzkumný	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
RIKEN	RIKEN	kA
podařilo	podařit	k5eAaPmAgNnS
vzniklé	vzniklý	k2eAgNnSc1d1
278113	#num#	k4
Nh	Nh	k1gMnPc2
dostatečně	dostatečně	k6eAd1
dobře	dobře	k6eAd1
identifikovat	identifikovat	k5eAaBmF
postupnou	postupný	k2eAgFnSc7d1
kaskádou	kaskáda	k1gFnSc7
rozpadů	rozpad	k1gInPc2
alfa	alfa	k1gNnSc2
až	až	k9
na	na	k7c4
mendelevium	mendelevium	k1gNnSc4
254	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
2015	#num#	k4
IUPAC	IUPAC	kA
potvrdila	potvrdit	k5eAaPmAgFnS
splnění	splnění	k1gNnSc4
kritérií	kritérion	k1gNnPc2
<g/>
,	,	kIx,
Uut	Uut	k1gFnSc1
uznala	uznat	k5eAaPmAgFnS
za	za	k7c4
objevené	objevený	k2eAgFnPc4d1
týmem	tým	k1gInSc7
RIKEN	RIKEN	kA
a	a	k8xC
vyzvala	vyzvat	k5eAaPmAgFnS
objevitele	objevitel	k1gMnSc4
k	k	k7c3
navržení	navržení	k1gNnSc3
konečného	konečný	k2eAgInSc2d1
názvu	název	k1gInSc2
a	a	k8xC
značky	značka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Tým	tým	k1gInSc1
RIKEN	RIKEN	kA
již	již	k6eAd1
dříve	dříve	k6eAd2
navrhoval	navrhovat	k5eAaImAgMnS
názvy	název	k1gInPc7
japonium	japonium	k1gNnSc1
(	(	kIx(
<g/>
podle	podle	k7c2
Japonska	Japonsko	k1gNnSc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
země	země	k1gFnSc1
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
existence	existence	k1gFnSc1
prvku	prvek	k1gInSc2
potvrzena	potvrdit	k5eAaPmNgFnS
<g/>
)	)	kIx)
a	a	k8xC
rikenium	rikenium	k1gNnSc4
(	(	kIx(
<g/>
podle	podle	k7c2
názvu	název	k1gInSc2
vědeckého	vědecký	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
ji	on	k3xPp3gFnSc4
potvrdil	potvrdit	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
tým	tým	k1gInSc1
z	z	k7c2
Dubny	duben	k1gInPc1
becquerelium	becquerelium	k1gNnSc4
na	na	k7c4
počest	počest	k1gFnSc4
francouzského	francouzský	k2eAgMnSc2d1
fyzika	fyzik	k1gMnSc2
Henriho	Henri	k1gMnSc4
Becquerela	Becquerel	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Konečným	konečný	k2eAgInSc7d1
návrhem	návrh	k1gInSc7
objevitelů	objevitel	k1gMnPc2
byl	být	k5eAaImAgInS
název	název	k1gInSc1
nihonium	nihonium	k1gNnSc1
a	a	k8xC
značka	značka	k1gFnSc1
Nh	Nh	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nihon	Nihon	k1gInSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
ze	z	k7c2
způsobů	způsob	k1gInPc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
v	v	k7c6
japonštině	japonština	k1gFnSc6
říci	říct	k5eAaPmF
jméno	jméno	k1gNnSc4
země	zem	k1gFnSc2
objevitelů	objevitel	k1gMnPc2
<g/>
,	,	kIx,
Japonska	Japonsko	k1gNnSc2
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
„	„	k?
<g/>
země	země	k1gFnSc1
vycházejícího	vycházející	k2eAgNnSc2d1
slunce	slunce	k1gNnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
日	日	k?
<g/>
,	,	kIx,
ni	on	k3xPp3gFnSc4
hon	hon	k1gInSc1
koku	kok	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
návrh	návrh	k1gInSc1
konečného	konečný	k2eAgNnSc2d1
pojmenování	pojmenování	k1gNnSc2
předložila	předložit	k5eAaPmAgFnS
IUPAC	IUPAC	kA
v	v	k7c6
červnu	červen	k1gInSc6
2016	#num#	k4
k	k	k7c3
veřejné	veřejný	k2eAgFnSc3d1
diskusi	diskuse	k1gFnSc3
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
schválila	schválit	k5eAaPmAgFnS
jako	jako	k9
konečné	konečný	k2eAgNnSc4d1
pojmenování	pojmenování	k1gNnSc4
a	a	k8xC
značku	značka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jde	jít	k5eAaImIp3nS
o	o	k7c4
první	první	k4xOgInSc4
prvek	prvek	k1gInSc4
pojmenovaný	pojmenovaný	k2eAgInSc4d1
týmem	tým	k1gInSc7
nebo	nebo	k8xC
vědcem	vědec	k1gMnSc7
z	z	k7c2
Asie	Asie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
Doposud	doposud	k6eAd1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
známy	znám	k2eAgInPc1d1
následující	následující	k2eAgInPc1d1
izotopy	izotop	k1gInPc1
nihonia	nihonium	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
IzotopRok	IzotopRok	k1gInSc1
objevuReakcePoločas	objevuReakcePoločasa	k1gFnPc2
přeměny	přeměna	k1gFnSc2
</s>
<s>
278	#num#	k4
<g/>
Nh	Nh	k1gFnSc1
<g/>
2004209	#num#	k4
<g/>
Bi	Bi	k1gFnPc2
<g/>
(	(	kIx(
<g/>
70	#num#	k4
<g/>
Zn	zn	kA
<g/>
,	,	kIx,
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
0,24	0,24	k4
ms	ms	k?
</s>
<s>
279	#num#	k4
<g/>
Nh	Nh	k1gFnPc2
?	?	kIx.
</s>
<s>
280	#num#	k4
<g/>
Nh	Nh	k1gFnPc2
?	?	kIx.
</s>
<s>
281	#num#	k4
<g/>
Nh	Nh	k1gFnPc2
?	?	kIx.
</s>
<s>
282	#num#	k4
<g/>
Nh	Nh	k1gFnSc1
<g/>
2006237	#num#	k4
<g/>
Np	Np	k1gFnPc2
<g/>
(	(	kIx(
<g/>
48	#num#	k4
<g/>
Ca	ca	kA
<g/>
,3	,3	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
0,07	0,07	k4
s	s	k7c7
</s>
<s>
283	#num#	k4
<g/>
Nh	Nh	k1gFnSc1
<g/>
2003243	#num#	k4
<g/>
Am	Am	k1gFnPc2
<g/>
(	(	kIx(
<g/>
48	#num#	k4
<g/>
Ca	ca	kA
<g/>
,4	,4	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
100	#num#	k4
ms	ms	k?
</s>
<s>
284	#num#	k4
<g/>
Nh	Nh	k1gFnSc1
<g/>
2003243	#num#	k4
<g/>
Am	Am	k1gFnPc2
<g/>
(	(	kIx(
<g/>
48	#num#	k4
<g/>
Ca	ca	kA
<g/>
,3	,3	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
0,48	0,48	k4
s	s	k7c7
</s>
<s>
285	#num#	k4
<g/>
Nh	Nh	k1gFnSc1
<g/>
5,5	5,5	k4
s	s	k7c7
</s>
<s>
286	#num#	k4
<g/>
Nh	Nh	k1gFnSc1
<g/>
20	#num#	k4
s	s	k7c7
</s>
<s>
287	#num#	k4
<g/>
Nh	Nh	k1gFnPc2
?	?	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
NDS	NDS	kA
ENSDF	ENSDF	kA
<g/>
.	.	kIx.
www-nds	www-nds	k1gInSc1
<g/>
.	.	kIx.
<g/>
iaea	iaea	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NDS	NDS	kA
ENSDF	ENSDF	kA
<g/>
.	.	kIx.
www-nds	www-nds	k1gInSc1
<g/>
.	.	kIx.
<g/>
iaea	iaea	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NDS	NDS	kA
ENSDF	ENSDF	kA
<g/>
.	.	kIx.
www-nds	www-nds	k1gInSc1
<g/>
.	.	kIx.
<g/>
iaea	iaea	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NDS	NDS	kA
ENSDF	ENSDF	kA
<g/>
.	.	kIx.
www-nds	www-nds	k1gInSc1
<g/>
.	.	kIx.
<g/>
iaea	iaea	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NDS	NDS	kA
ENSDF	ENSDF	kA
<g/>
.	.	kIx.
www-nds	www-nds	k1gInSc1
<g/>
.	.	kIx.
<g/>
iaea	iaea	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NDS	NDS	kA
ENSDF	ENSDF	kA
<g/>
.	.	kIx.
www-nds	www-nds	k1gInSc1
<g/>
.	.	kIx.
<g/>
iaea	iaea	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
“	“	k?
<g/>
Experiments	Experiments	k1gInSc1
on	on	k3xPp3gInSc1
the	the	k?
synthesis	synthesis	k1gInSc1
of	of	k?
element	element	k1gInSc1
115	#num#	k4
in	in	k?
the	the	k?
reaction	reaction	k1gInSc4
243	#num#	k4
<g/>
Am	Am	k1gFnPc2
<g/>
(	(	kIx(
<g/>
48	#num#	k4
<g/>
Ca	ca	kA
<g/>
,	,	kIx,
<g/>
xn	xn	k?
<g/>
)	)	kIx)
<g/>
291	#num#	k4
<g/>
-x	-x	k?
<g/>
115	#num#	k4
<g/>
”	”	k?
<g/>
,	,	kIx,
Oganessian	Oganessian	k1gInSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
JINR	JINR	kA
Preprints	Preprints	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
on	on	k3xPp3gInSc1
3	#num#	k4
March	March	k1gInSc1
2008	#num#	k4
<g/>
↑	↑	k?
Oganessian	Oganessian	k1gMnSc1
<g/>
,	,	kIx,
Yu	Yu	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ts	ts	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Experiments	Experiments	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
synthesis	synthesis	k1gInSc1
of	of	k?
element	element	k1gInSc1
115	#num#	k4
in	in	k?
the	the	k?
reaction	reaction	k1gInSc4
243	#num#	k4
<g/>
Am	Am	k1gFnPc2
<g/>
(	(	kIx(
<g/>
48	#num#	k4
<g/>
Ca	ca	kA
<g/>
,	,	kIx,
<g/>
xn	xn	k?
<g/>
)	)	kIx)
<g/>
291	#num#	k4
<g/>
-x	-x	k?
<g/>
115	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Physical	Physical	k1gFnSc1
Review	Review	k1gFnSc2
C.	C.	kA
2004	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
69	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
0	#num#	k4
<g/>
21601	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.110	10.110	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
PhysRevC	PhysRevC	k1gFnSc2
<g/>
.69	.69	k4
<g/>
.021601	.021601	k4
<g/>
.	.	kIx.
↑	↑	k?
Morita	Morit	k1gMnSc4
<g/>
,	,	kIx,
Kosuke	Kosuk	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Experiment	experiment	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
Synthesis	Synthesis	k1gInSc1
of	of	k?
Element	element	k1gInSc1
113	#num#	k4
in	in	k?
the	the	k?
Reaction	Reaction	k1gInSc4
209	#num#	k4
<g/>
Bi	Bi	k1gFnPc2
<g/>
(	(	kIx(
<g/>
70	#num#	k4
<g/>
Zn	zn	kA
<g/>
,	,	kIx,
n	n	k0
<g/>
)	)	kIx)
<g/>
278113	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
Physical	Physical	k1gMnSc1
Society	societa	k1gFnSc2
of	of	k?
Japan	japan	k1gInSc1
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
73	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2593	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.114	10.114	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
JPSJ	JPSJ	kA
<g/>
.73	.73	k4
<g/>
.2593	.2593	k4
<g/>
.	.	kIx.
↑	↑	k?
MORITA	MORITA	kA
<g/>
,	,	kIx,
Kosuke	Kosuke	k1gFnSc1
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
Result	Result	k1gMnSc1
in	in	k?
the	the	k?
Production	Production	k1gInSc1
and	and	k?
Decay	Decaa	k1gFnSc2
of	of	k?
an	an	k?
Isotope	isotop	k1gInSc5
<g/>
,	,	kIx,
278113	#num#	k4
<g/>
,	,	kIx,
of	of	k?
the	the	k?
113	#num#	k4
<g/>
th	th	k?
Element	element	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
Phys	Physa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soc	soc	kA
<g/>
.	.	kIx.
Jpn	Jpn	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
ročník	ročník	k1gInSc1
81	#num#	k4
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
e	e	k0
<g/>
103201	#num#	k4
Dostupné	dostupný	k2eAgFnPc1d1
online	onlinout	k5eAaPmIp3nS
PDF	PDF	kA
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.114	10.114	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
JPSJ	JPSJ	kA
<g/>
.81	.81	k4
<g/>
.103201	.103201	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Search	Search	k1gInSc1
for	forum	k1gNnPc2
element	element	k1gInSc4
113	#num#	k4
concluded	concluded	k1gInSc1
at	at	k?
last	last	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
5	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
(	(	kIx(
<g/>
tiskové	tiskový	k2eAgNnSc1d1
prohlášení	prohlášení	k1gNnSc1
k	k	k7c3
předchozí	předchozí	k2eAgFnSc3d1
referenci	reference	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
RIKEN	RIKEN	kA
Press	Press	k1gInSc1
Release	Releasa	k1gFnSc3
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2012	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
WAGNER	Wagner	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
:	:	kIx,
Existence	existence	k1gFnSc1
prvku	prvek	k1gInSc2
s	s	k7c7
protonovým	protonový	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
113	#num#	k4
potvrzena	potvrzen	k2eAgMnSc4d1
(	(	kIx(
<g/>
popularizační	popularizační	k2eAgInSc4d1
článek	článek	k1gInSc4
k	k	k7c3
předchozím	předchozí	k2eAgFnPc3d1
referencím	reference	k1gFnPc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
O.S.	O.S.	k1gMnSc1
<g/>
E.L.	E.L.	k1gMnSc1
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2012	#num#	k4
<g/>
↑	↑	k?
Discovery	Discovera	k1gFnSc2
and	and	k?
Assignment	Assignment	k1gInSc1
of	of	k?
Elements	Elements	k1gInSc1
with	with	k1gMnSc1
Atomic	Atomic	k1gMnSc1
Numbers	Numbers	k1gInSc4
113	#num#	k4
<g/>
,	,	kIx,
115	#num#	k4
<g/>
,	,	kIx,
117	#num#	k4
and	and	k?
118	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
IUPAC	IUPAC	kA
Latest	Latest	k1gInSc1
News	News	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PDF	PDF	kA
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
se	se	k3xPyFc4
rozrostla	rozrůst	k5eAaPmAgFnS
o	o	k7c4
čtyři	čtyři	k4xCgInPc4
nové	nový	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
EMPRESA	EMPRESA	kA
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc4
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Discovering	Discovering	k1gInSc1
element	element	k1gInSc1
113	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Riken	Riken	k2eAgInSc4d1
News	News	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
November	November	k1gInSc1
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
9	#num#	k4
February	Februara	k1gFnSc2
2008	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HELMENSTINE	HELMENSTINE	kA
<g/>
,	,	kIx,
Anne	Anne	k1gFnSc1
Marie	Marie	k1gFnSc1
<g/>
:	:	kIx,
New	New	k1gFnSc1
Element	element	k1gInSc4
Discovery	Discovera	k1gFnSc2
-	-	kIx~
Proposed	Proposed	k1gMnSc1
Names	Names	k1gMnSc1
for	forum	k1gNnPc2
Element	element	k1gInSc4
113	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
About	About	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
Chemistry	Chemistr	k1gMnPc7
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2012	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
IUPAC	IUPAC	kA
News	Newsa	k1gFnPc2
<g/>
:	:	kIx,
IUPAC	IUPAC	kA
is	is	k?
naming	naming	k1gInSc1
the	the	k?
four	four	k1gInSc1
new	new	k?
elements	elements	k6eAd1
nihonium	nihonium	k1gNnSc1
<g/>
,	,	kIx,
moscovium	moscovium	k1gNnSc1
<g/>
,	,	kIx,
tennessine	tennessinout	k5eAaPmIp3nS
<g/>
,	,	kIx,
and	and	k?
oganesson	oganesson	k1gInSc1
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
Tennessee	Tennessee	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
chemické	chemický	k2eAgInPc1d1
prvky	prvek	k1gInPc1
obohatí	obohatit	k5eAaPmIp3nP
tabulku	tabulka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
EMPRESA	EMPRESA	kA
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
IUPAC	IUPAC	kA
News	News	k1gInSc1
<g/>
:	:	kIx,
IUPAC	IUPAC	kA
Announces	Announces	k1gMnSc1
the	the	k?
Names	Names	k1gMnSc1
of	of	k?
the	the	k?
Elements	Elements	k1gInSc1
113	#num#	k4
<g/>
,	,	kIx,
115	#num#	k4
<g/>
,	,	kIx,
117	#num#	k4
<g/>
,	,	kIx,
and	and	k?
118	#num#	k4
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
LÁZŇOVSKÝ	LÁZŇOVSKÝ	kA
<g/>
,	,	kIx,
Matouš	Matouš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Periodické	periodický	k2eAgFnSc6d1
tabulce	tabulka	k1gFnSc6
přibyly	přibýt	k5eAaPmAgInP
čtyři	čtyři	k4xCgInPc1
nové	nový	k2eAgInPc1d1
prvky	prvek	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádný	žádný	k1gMnSc1
dlouho	dlouho	k6eAd1
nevydrží	vydržet	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technet	Technet	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2016-01-04	2016-01-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
</s>
<s>
Jaderná	jaderný	k2eAgFnSc1d1
fyzika	fyzika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
nihonium	nihonium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
nihonium	nihonium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Galerie	galerie	k1gFnPc1
nihonium	nihonium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2012003544	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2012003544	#num#	k4
</s>
