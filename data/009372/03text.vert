<p>
<s>
Skoll	Skoll	k1gInSc1	Skoll
(	(	kIx(	(
<g/>
vyslovováno	vyslovován	k2eAgNnSc1d1	vyslovováno
/	/	kIx~	/
<g/>
ˈ	ˈ	k?	ˈ
<g/>
/	/	kIx~	/
nebo	nebo	k8xC	nebo
norsky	norsky	k6eAd1	norsky
/	/	kIx~	/
<g/>
skœ	skœ	k?	skœ
<g/>
]	]	kIx)	]
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc4d1	malý
měsíc	měsíc	k1gInSc4	měsíc
planety	planeta	k1gFnSc2	planeta
Saturn	Saturn	k1gInSc1	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
objev	objev	k1gInSc1	objev
byl	být	k5eAaImAgInS	být
oznámen	oznámit	k5eAaPmNgInS	oznámit
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
vědeckým	vědecký	k2eAgInSc7d1	vědecký
týmem	tým	k1gInSc7	tým
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc7	jehož
členy	člen	k1gMnPc7	člen
jsou	být	k5eAaImIp3nP	být
Scott	Scott	k1gMnSc1	Scott
S.	S.	kA	S.
Sheppard	Sheppard	k1gMnSc1	Sheppard
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
C.	C.	kA	C.
Jewitt	Jewitt	k1gMnSc1	Jewitt
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kleyna	Kleyn	k1gInSc2	Kleyn
a	a	k8xC	a
Brian	Brian	k1gMnSc1	Brian
G.	G.	kA	G.
Marsden	Marsdna	k1gFnPc2	Marsdna
<g/>
.	.	kIx.	.
</s>
<s>
Nalezen	nalezen	k2eAgInSc1d1	nalezen
byl	být	k5eAaImAgInS	být
během	během	k7c2	během
pozorování	pozorování	k1gNnPc2	pozorování
provedených	provedený	k2eAgNnPc2d1	provedené
mezi	mezi	k7c4	mezi
5	[number]	k4	5
<g/>
.	.	kIx.	.
lednem	leden	k1gInSc7	leden
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubnem	duben	k1gInSc7	duben
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
objevu	objev	k1gInSc6	objev
dostal	dostat	k5eAaPmAgMnS	dostat
dočasné	dočasný	k2eAgNnSc4d1	dočasné
označení	označení	k1gNnSc4	označení
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
S	s	k7c7	s
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
nazván	nazván	k2eAgInSc1d1	nazván
Skoll	Skoll	k1gInSc1	Skoll
<g/>
,	,	kIx,	,
po	po	k7c6	po
obru	obr	k1gMnSc6	obr
jménem	jméno	k1gNnSc7	jméno
Sköll	Skölla	k1gFnPc2	Skölla
<g/>
,	,	kIx,	,
patřícího	patřící	k2eAgMnSc2d1	patřící
do	do	k7c2	do
norské	norský	k2eAgFnSc2d1	norská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
jeho	jeho	k3xOp3gInSc7	jeho
názvem	název	k1gInSc7	název
je	být	k5eAaImIp3nS	být
Saturn	Saturn	k1gInSc1	Saturn
XLVII	XLVII	kA	XLVII
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skoll	Skoll	k1gInSc1	Skoll
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Saturnových	Saturnův	k2eAgInPc2d1	Saturnův
měsíců	měsíc	k1gInPc2	měsíc
nazvaných	nazvaný	k2eAgMnPc2d1	nazvaný
Norové	Norové	k2eAgMnPc2d1	Norové
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
měsíce	měsíc	k1gInSc2	měsíc
==	==	k?	==
</s>
</p>
<p>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
průměr	průměr	k1gInSc1	průměr
měsíce	měsíc	k1gInSc2	měsíc
Skoll	Skolla	k1gFnPc2	Skolla
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
6	[number]	k4	6
kilometrů	kilometr	k1gInPc2	kilometr
(	(	kIx(	(
<g/>
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
jeho	jeho	k3xOp3gMnSc2	jeho
albeda	albed	k1gMnSc2	albed
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
==	==	k?	==
</s>
</p>
<p>
<s>
Skoll	Skoll	k1gInSc1	Skoll
obíhá	obíhat	k5eAaImIp3nS	obíhat
Saturn	Saturn	k1gInSc4	Saturn
po	po	k7c6	po
retrográdní	retrográdní	k2eAgFnSc6d1	retrográdní
dráze	dráha	k1gFnSc6	dráha
v	v	k7c6	v
průměrné	průměrný	k2eAgFnSc6d1	průměrná
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
17,6	[number]	k4	17,6
milionů	milion	k4xCgInPc2	milion
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
869	[number]	k4	869
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Skoll	Skolla	k1gFnPc2	Skolla
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
