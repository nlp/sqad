<s>
Bulat	bulat	k5eAaImF	bulat
Šalvovič	Šalvovič	k1gInSc4	Šalvovič
Okudžava	Okudžava	k1gFnSc1	Okudžava
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
Б	Б	k?	Б
Ш	Ш	k?	Ш
О	О	k?	О
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
šansoniér	šansoniér	k1gMnSc1	šansoniér
arménsko-abchazsko-gruzínského	arménskobchazskoruzínský	k2eAgInSc2d1	arménsko-abchazsko-gruzínský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
