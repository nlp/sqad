<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
I.	I.	kA	I.
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1660	[number]	k4	1660
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1727	[number]	k4	1727
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1698	[number]	k4	1698
vévoda	vévoda	k1gMnSc1	vévoda
brunšvicko-lüneburský	brunšvickoüneburský	k2eAgInSc1d1	brunšvicko-lüneburský
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1708	[number]	k4	1708
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
hannoverský	hannoverský	k2eAgMnSc1d1	hannoverský
a	a	k8xC	a
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1714	[number]	k4	1714
panovník	panovník	k1gMnSc1	panovník
Království	království	k1gNnSc4	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Osnabrücku	Osnabrück	k1gInSc6	Osnabrück
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
zdědil	zdědit	k5eAaPmAgInS	zdědit
panství	panství	k1gNnSc4	panství
vévody	vévoda	k1gMnSc2	vévoda
brunšvicko-lüneburského	brunšvickoüneburský	k2eAgMnSc2d1	brunšvicko-lüneburský
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
54	[number]	k4	54
let	léto	k1gNnPc2	léto
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
královny	královna	k1gFnSc2	královna
Anny	Anna	k1gFnSc2	Anna
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
britským	britský	k2eAgMnSc7d1	britský
panovníkem	panovník	k1gMnSc7	panovník
jako	jako	k8xC	jako
dědic	dědic	k1gMnSc1	dědic
Hannoverské	hannoverský	k2eAgFnSc2d1	hannoverská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInSc4d2	veliký
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
dědictví	dědictví	k1gNnSc4	dědictví
trůnu	trůn	k1gInSc2	trůn
měla	mít	k5eAaImAgFnS	mít
řada	řada	k1gFnSc1	řada
jiných	jiný	k2eAgMnPc2d1	jiný
příbuzných	příbuzný	k1gMnPc2	příbuzný
královny	královna	k1gFnSc2	královna
Anny	Anna	k1gFnSc2	Anna
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
však	však	k9	však
katolického	katolický	k2eAgNnSc2d1	katolické
vyznání	vyznání	k1gNnSc2	vyznání
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
Act	Act	k1gFnSc3	Act
of	of	k?	of
Settlement	settlement	k1gInSc4	settlement
1701	[number]	k4	1701
byla	být	k5eAaImAgFnS	být
tudíž	tudíž	k8xC	tudíž
možnost	možnost	k1gFnSc1	možnost
jejich	jejich	k3xOp3gInSc2	jejich
nástupu	nástup	k1gInSc2	nástup
na	na	k7c4	na
britský	britský	k2eAgInSc4d1	britský
trůn	trůn	k1gInSc4	trůn
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Anniným	Annin	k2eAgMnPc3d1	Annin
nejbližším	blízký	k2eAgMnPc3d3	nejbližší
protestantským	protestantský	k2eAgMnPc3d1	protestantský
příbuzným	příbuzný	k1gMnPc3	příbuzný
byla	být	k5eAaImAgNnP	být
Jiřího	Jiří	k1gMnSc2	Jiří
matka	matka	k1gFnSc1	matka
Žofie	Žofie	k1gFnSc1	Žofie
Hannoverská	hannoverský	k2eAgFnSc1d1	hannoverská
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
Alžběty	Alžběta	k1gFnSc2	Alžběta
Stuartovny	Stuartovna	k1gFnSc2	Stuartovna
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc2d3	nejstarší
dcery	dcera	k1gFnSc2	dcera
krále	král	k1gMnSc2	král
Jakuba	Jakub	k1gMnSc2	Jakub
I.	I.	kA	I.
Stuarta	Stuart	k1gMnSc2	Stuart
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
zemřela	zemřít	k5eAaPmAgFnS	zemřít
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
Anny	Anna	k1gFnSc2	Anna
Stuartovny	Stuartovna	k1gFnSc2	Stuartovna
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
dědicem	dědic	k1gMnSc7	dědic
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
její	její	k3xOp3gMnSc1	její
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
jakobité	jakobita	k1gMnPc1	jakobita
(	(	kIx(	(
<g/>
stoupenci	stoupenec	k1gMnPc1	stoupenec
vyhnaného	vyhnaný	k2eAgMnSc2d1	vyhnaný
katolického	katolický	k2eAgMnSc2d1	katolický
krále	král	k1gMnSc2	král
Jakuba	Jakub	k1gMnSc2	Jakub
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Stuarta	Stuart	k1gMnSc2	Stuart
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
potomků	potomek	k1gMnPc2	potomek
<g/>
)	)	kIx)	)
pokusili	pokusit	k5eAaPmAgMnP	pokusit
Jiřího	Jiří	k1gMnSc4	Jiří
odstranit	odstranit	k5eAaPmF	odstranit
a	a	k8xC	a
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
prosadit	prosadit	k5eAaPmF	prosadit
nástupce	nástupce	k1gMnSc1	nástupce
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Stuartovců	Stuartovec	k1gMnPc2	Stuartovec
po	po	k7c6	po
mužské	mužský	k2eAgFnSc6d1	mužská
linii	linie	k1gFnSc6	linie
<g/>
,	,	kIx,	,
katolického	katolický	k2eAgMnSc2d1	katolický
Jakuba	Jakub	k1gMnSc2	Jakub
Františka	František	k1gMnSc2	František
Stuarta	Stuart	k1gMnSc2	Stuart
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuspěli	uspět	k5eNaPmAgMnP	uspět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
vlády	vláda	k1gFnSc2	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
I.	I.	kA	I.
vliv	vliv	k1gInSc1	vliv
monarchie	monarchie	k1gFnSc2	monarchie
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
a	a	k8xC	a
Británie	Británie	k1gFnSc1	Británie
zahájila	zahájit	k5eAaPmAgFnS	zahájit
přechod	přechod	k1gInSc4	přechod
k	k	k7c3	k
modernímu	moderní	k2eAgInSc3d1	moderní
systému	systém	k1gInSc3	systém
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
moc	moc	k6eAd1	moc
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
vlády	vláda	k1gFnSc2	vláda
vedené	vedený	k2eAgFnSc2d1	vedená
jejím	její	k3xOp3gMnSc7	její
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
Jiřího	Jiří	k1gMnSc2	Jiří
panování	panování	k1gNnSc2	panování
byla	být	k5eAaImAgFnS	být
moc	moc	k6eAd1	moc
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
sira	sir	k1gMnSc2	sir
Roberta	Robert	k1gMnSc2	Robert
Walpole	Walpole	k1gFnSc2	Walpole
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
prvního	první	k4xOgNnSc2	první
de	de	k?	de
facto	facta	k1gFnSc5	facta
předsedu	předseda	k1gMnSc4	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
domovského	domovský	k2eAgInSc2d1	domovský
Hannoveru	Hannover	k1gInSc2	Hannover
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1660	[number]	k4	1660
v	v	k7c6	v
Osnabrücku	Osnabrück	k1gInSc6	Osnabrück
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
součástí	součást	k1gFnSc7	součást
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nejstarším	starý	k2eAgMnSc7d3	nejstarší
synem	syn	k1gMnSc7	syn
Ernesta	Ernest	k1gMnSc2	Ernest
Augusta	August	k1gMnSc2	August
<g/>
,	,	kIx,	,
vévody	vévoda	k1gMnSc2	vévoda
brunšvicko-lüneburského	brunšvickoüneburský	k2eAgMnSc2d1	brunšvicko-lüneburský
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
ženy	žena	k1gFnPc4	žena
Žofie	Žofie	k1gFnSc2	Žofie
Hannoverské	hannoverský	k2eAgFnSc2d1	hannoverská
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
jmenované	jmenovaný	k2eAgFnPc1d1	jmenovaná
také	také	k9	také
jako	jako	k9	jako
Žofie	Žofie	k1gFnSc1	Žofie
Falcká	falcký	k2eAgFnSc1d1	Falcká
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žofie	Žofie	k1gFnPc4	Žofie
byla	být	k5eAaImAgFnS	být
dcera	dcera	k1gFnSc1	dcera
českého	český	k2eAgNnSc2d1	české
"	"	kIx"	"
<g/>
zimního	zimní	k2eAgMnSc2d1	zimní
krále	král	k1gMnSc2	král
<g/>
"	"	kIx"	"
Fridricha	Fridrich	k1gMnSc2	Fridrich
Falckého	falcký	k2eAgMnSc2d1	falcký
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Alžběty	Alžběta	k1gFnSc2	Alžběta
Stuartovny	Stuartovna	k1gFnSc2	Stuartovna
a	a	k8xC	a
přes	přes	k7c4	přes
ni	on	k3xPp3gFnSc4	on
tedy	tedy	k9	tedy
vnučka	vnučka	k1gFnSc1	vnučka
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
Jakuba	Jakub	k1gMnSc2	Jakub
I.	I.	kA	I.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1679	[number]	k4	1679
<g/>
,	,	kIx,	,
po	po	k7c6	po
náhlé	náhlý	k2eAgFnSc6d1	náhlá
smrti	smrt	k1gFnSc6	smrt
jednoho	jeden	k4xCgMnSc2	jeden
příbuzného	příbuzný	k1gMnSc2	příbuzný
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Ernest	Ernest	k1gMnSc1	Ernest
Augustus	Augustus	k1gMnSc1	Augustus
vévodou	vévoda	k1gMnSc7	vévoda
calenbersko-göttingenským	calenberskoöttingenský	k2eAgMnSc7d1	calenbersko-göttingenský
<g/>
,	,	kIx,	,
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
Hannoveru	Hannover	k1gInSc6	Hannover
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1682	[number]	k4	1682
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
dohodla	dohodnout	k5eAaPmAgFnS	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
dědicem	dědic	k1gMnSc7	dědic
všech	všecek	k3xTgNnPc2	všecek
rodinných	rodinný	k2eAgNnPc2d1	rodinné
panství	panství	k1gNnPc2	panství
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
jednotného	jednotný	k2eAgInSc2d1	jednotný
hannoverského	hannoverský	k2eAgInSc2d1	hannoverský
státu	stát	k1gInSc2	stát
a	a	k8xC	a
díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Hannoverský	hannoverský	k2eAgInSc1d1	hannoverský
rod	rod	k1gInSc1	rod
prokazoval	prokazovat	k5eAaImAgInS	prokazovat
císaři	císař	k1gMnPc7	císař
v	v	k7c6	v
devítileté	devítiletý	k2eAgFnSc6d1	devítiletá
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Ernest	Ernest	k1gMnSc1	Ernest
Augustus	Augustus	k1gMnSc1	Augustus
roku	rok	k1gInSc2	rok
1692	[number]	k4	1692
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
dědičným	dědičný	k2eAgMnSc7d1	dědičný
kurfiřtem	kurfiřt	k1gMnSc7	kurfiřt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Manželství	manželství	k1gNnSc1	manželství
a	a	k8xC	a
potomci	potomek	k1gMnPc1	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
roku	rok	k1gInSc2	rok
1679	[number]	k4	1679
se	se	k3xPyFc4	se
Jiří	Jiří	k1gMnSc1	Jiří
oženil	oženit	k5eAaPmAgMnS	oženit
v	v	k7c6	v
Celle	Cella	k1gFnSc6	Cella
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sestřenicí	sestřenice	k1gFnSc7	sestřenice
Žofií	Žofie	k1gFnSc7	Žofie
Doroteou	Dorotea	k1gFnSc7	Dorotea
z	z	k7c2	z
Celle	Celle	k1gFnSc2	Celle
<g/>
,	,	kIx,	,
jedinou	jediný	k2eAgFnSc7d1	jediná
dcerou	dcera	k1gFnSc7	dcera
svého	svůj	k3xOyFgMnSc2	svůj
strýce	strýc	k1gMnSc2	strýc
-	-	kIx~	-
staršího	starý	k2eAgMnSc2d2	starší
bratra	bratr	k1gMnSc2	bratr
svého	své	k1gNnSc2	své
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
vzešly	vzejít	k5eAaPmAgFnP	vzejít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
August	August	k1gMnSc1	August
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1683	[number]	k4	1683
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
Herrenhausen	Herrenhausen	k1gInSc1	Herrenhausen
<g/>
,	,	kIx,	,
Hannover	Hannover	k1gInSc1	Hannover
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1760	[number]	k4	1760
<g/>
,	,	kIx,	,
Kensingtonský	Kensingtonský	k2eAgInSc1d1	Kensingtonský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
následník	následník	k1gMnSc1	následník
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
pozdější	pozdní	k2eAgMnSc1d2	pozdější
král	král	k1gMnSc1	král
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
hannoverský	hannoverský	k2eAgInSc1d1	hannoverský
kurfiřtŽofie	kurfiřtŽofie	k1gFnSc1	kurfiřtŽofie
Dorotea	Dorotea	k1gMnSc1	Dorotea
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g />
.	.	kIx.	.
</s>
<s>
1687	[number]	k4	1687
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
Herrenhausen	Herrenhausen	k1gInSc1	Herrenhausen
<g/>
,	,	kIx,	,
Hannover	Hannover	k1gInSc1	Hannover
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1757	[number]	k4	1757
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
Monbijou	Monbijou	k1gFnSc1	Monbijou
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
provdána	provdán	k2eAgFnSc1d1	provdána
za	za	k7c2	za
pruského	pruský	k2eAgMnSc2d1	pruský
krále	král	k1gMnSc2	král
Fridricha	Fridrich	k1gMnSc4	Fridrich
Viléma	Vilém	k1gMnSc4	Vilém
I.	I.	kA	I.
<g/>
Manželství	manželství	k1gNnSc1	manželství
<g/>
,	,	kIx,	,
uzavřené	uzavřený	k2eAgNnSc1d1	uzavřené
jako	jako	k8xC	jako
dynastický	dynastický	k2eAgInSc1d1	dynastický
svazek	svazek	k1gInSc1	svazek
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
spojit	spojit	k5eAaPmF	spojit
majetky	majetek	k1gInPc4	majetek
a	a	k8xC	a
panství	panství	k1gNnPc4	panství
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
šťastné	šťastný	k2eAgNnSc1d1	šťastné
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
se	se	k3xPyFc4	se
nesnášeli	snášet	k5eNaImAgMnP	snášet
a	a	k8xC	a
Jiří	Jiří	k1gMnPc1	Jiří
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
vrátil	vrátit	k5eAaPmAgMnS	vrátit
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
dlouholeté	dlouholetý	k2eAgFnSc3d1	dlouholetá
milence	milenka	k1gFnSc3	milenka
</s>
</p>
<p>
<s>
Melusine	Melusinout	k5eAaPmIp3nS	Melusinout
von	von	k1gInSc1	von
der	drát	k5eAaImRp2nS	drát
Schulenburg	Schulenburg	k1gMnSc1	Schulenburg
<g/>
,	,	kIx,	,
dvorní	dvorní	k2eAgFnSc6d1	dvorní
dámě	dáma	k1gFnSc6	dáma
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
později	pozdě	k6eAd2	pozdě
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
vévodkyní	vévodkyně	k1gFnSc7	vévodkyně
Munster	Munstra	k1gFnPc2	Munstra
a	a	k8xC	a
Kendal	Kendal	k1gFnPc2	Kendal
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
;	;	kIx,	;
měl	mít	k5eAaImAgInS	mít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
nejméně	málo	k6eAd3	málo
tři	tři	k4xCgFnPc4	tři
nemanželské	manželský	k2eNgFnPc4d1	nemanželská
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
Žofie	Žofie	k1gFnSc1	Žofie
Dorotea	Dorotea	k1gFnSc1	Dorotea
si	se	k3xPyFc3	se
začala	začít	k5eAaPmAgFnS	začít
románek	románek	k1gInSc4	románek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
švédským	švédský	k2eAgMnSc7d1	švédský
hrabětem	hrabě	k1gMnSc7	hrabě
Philipem	Philip	k1gMnSc7	Philip
Christophem	Christoph	k1gInSc7	Christoph
von	von	k1gInSc1	von
Köningsmarck	Köningsmarcka	k1gFnPc2	Köningsmarcka
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
tolerovalo	tolerovat	k5eAaImAgNnS	tolerovat
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
nemohlo	moct	k5eNaImAgNnS	moct
projít	projít	k5eAaPmF	projít
ženě	žena	k1gFnSc3	žena
<g/>
:	:	kIx,	:
odhalení	odhalení	k1gNnSc1	odhalení
jejich	jejich	k3xOp3gInSc2	jejich
vztahu	vztah	k1gInSc2	vztah
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
na	na	k7c6	na
hannoverském	hannoverský	k2eAgInSc6d1	hannoverský
dvoře	dvůr	k1gInSc6	dvůr
skandál	skandál	k1gInSc1	skandál
a	a	k8xC	a
hněv	hněv	k1gInSc1	hněv
Jiřího	Jiří	k1gMnSc2	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
všech	všecek	k3xTgFnPc2	všecek
indicií	indicie	k1gFnPc2	indicie
(	(	kIx(	(
<g/>
důkazy	důkaz	k1gInPc1	důkaz
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
zničeny	zničit	k5eAaPmNgInP	zničit
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
Königsmarck	Königsmarck	k1gMnSc1	Königsmarck
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1694	[number]	k4	1694
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
tělo	tělo	k1gNnSc4	tělo
vhozeno	vhozen	k2eAgNnSc4d1	vhozeno
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
(	(	kIx(	(
<g/>
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
vraždě	vražda	k1gFnSc6	vražda
participovali	participovat	k5eAaImAgMnP	participovat
čtyři	čtyři	k4xCgMnPc4	čtyři
Jiřího	Jiří	k1gMnSc2	Jiří
dvořané	dvořan	k1gMnPc1	dvořan
<g/>
;	;	kIx,	;
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
měl	mít	k5eAaImAgInS	mít
dostat	dostat	k5eAaPmF	dostat
150	[number]	k4	150
000	[number]	k4	000
tolarů	tolar	k1gInPc2	tolar
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
roční	roční	k2eAgInSc1d1	roční
plat	plat	k1gInSc1	plat
vysoce	vysoce	k6eAd1	vysoce
postaveného	postavený	k2eAgMnSc2d1	postavený
ministra	ministr	k1gMnSc2	ministr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
Jiří	Jiří	k1gMnSc1	Jiří
rozvodu	rozvod	k1gInSc2	rozvod
s	s	k7c7	s
Žofií	Žofie	k1gFnSc7	Žofie
Doroteou	Dorotea	k1gFnSc7	Dorotea
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
však	však	k9	však
již	již	k6eAd1	již
neoženil	oženit	k5eNaPmAgMnS	oženit
(	(	kIx(	(
<g/>
Anglií	Anglie	k1gFnSc7	Anglie
nicméně	nicméně	k8xC	nicméně
kolovaly	kolovat	k5eAaImAgFnP	kolovat
"	"	kIx"	"
<g/>
zaručené	zaručený	k2eAgFnPc1d1	zaručená
<g/>
"	"	kIx"	"
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
tajném	tajný	k2eAgInSc6d1	tajný
sňatku	sňatek	k1gInSc6	sňatek
krále	král	k1gMnSc2	král
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
milenky	milenka	k1gFnSc2	milenka
Melusiny	Melusina	k1gFnSc2	Melusina
von	von	k1gInSc1	von
Schulenburg	Schulenburg	k1gInSc1	Schulenburg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žofie	Žofie	k1gFnSc1	Žofie
Dorotea	Dorotea	k1gFnSc1	Dorotea
byla	být	k5eAaImAgFnS	být
internována	internovat	k5eAaBmNgFnS	internovat
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Ahlden	Ahldno	k1gNnPc2	Ahldno
nedaleko	nedaleko	k7c2	nedaleko
jejího	její	k3xOp3gNnSc2	její
rodného	rodný	k2eAgNnSc2d1	rodné
Celle	Celle	k1gNnSc2	Celle
<g/>
.	.	kIx.	.
</s>
<s>
Nesměla	smět	k5eNaImAgFnS	smět
se	se	k3xPyFc4	se
stýkat	stýkat	k5eAaImF	stýkat
ani	ani	k8xC	ani
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
zakázáno	zakázán	k2eAgNnSc4d1	zakázáno
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
provdat	provdat	k5eAaPmF	provdat
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
stanovené	stanovený	k2eAgFnPc4d1	stanovená
pevné	pevný	k2eAgFnPc4d1	pevná
částky	částka	k1gFnPc4	částka
na	na	k7c4	na
zajištění	zajištění	k1gNnSc4	zajištění
svého	svůj	k3xOyFgNnSc2	svůj
živobytí	živobytí	k1gNnSc2	živobytí
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
si	se	k3xPyFc3	se
držet	držet	k5eAaImF	držet
malý	malý	k2eAgInSc4d1	malý
dvůr	dvůr	k1gInSc4	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
jí	on	k3xPp3gFnSc3	on
povoleny	povolen	k2eAgInPc4d1	povolen
pouze	pouze	k6eAd1	pouze
krátké	krátký	k2eAgInPc4d1	krátký
výjezdy	výjezd	k1gInPc4	výjezd
ze	z	k7c2	z
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
dozorem	dozor	k1gInSc7	dozor
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1726	[number]	k4	1726
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
==	==	k?	==
</s>
</p>
<p>
<s>
Ernest	Ernest	k1gMnSc1	Ernest
Augustus	Augustus	k1gMnSc1	Augustus
zemřel	zemřít	k5eAaPmAgMnS	zemřít
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1698	[number]	k4	1698
a	a	k8xC	a
veškerý	veškerý	k3xTgInSc1	veškerý
majetek	majetek	k1gInSc1	majetek
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
biskupství	biskupství	k1gNnSc2	biskupství
Osnabrück	Osnabrück	k1gMnSc1	Osnabrück
<g/>
,	,	kIx,	,
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Jiřímu	Jiří	k1gMnSc3	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
vévodou	vévoda	k1gMnSc7	vévoda
brunšvicko-lüneburským	brunšvickoüneburský	k2eAgMnPc3d1	brunšvicko-lüneburský
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
jako	jako	k9	jako
Hannoverským	hannoverský	k2eAgMnPc3d1	hannoverský
podle	podle	k7c2	podle
centra	centrum	k1gNnSc2	centrum
vévodství	vévodství	k1gNnSc2	vévodství
<g/>
)	)	kIx)	)
a	a	k8xC	a
kurfiřtem	kurfiřt	k1gMnSc7	kurfiřt
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jeho	on	k3xPp3gInSc2	on
dvora	dvůr	k1gInSc2	dvůr
v	v	k7c6	v
Hannoveru	Hannover	k1gInSc6	Hannover
působili	působit	k5eAaImAgMnP	působit
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Gottfried	Gottfried	k1gMnSc1	Gottfried
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Leibniz	Leibniz	k1gMnSc1	Leibniz
nebo	nebo	k8xC	nebo
Georg	Georg	k1gMnSc1	Georg
Friedrich	Friedrich	k1gMnSc1	Friedrich
Händel	Händlo	k1gNnPc2	Händlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
co	co	k3yInSc1	co
převzal	převzít	k5eAaPmAgMnS	převzít
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
otci	otec	k1gMnSc3	otec
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
druhý	druhý	k4xOgMnSc1	druhý
následník	následník	k1gMnSc1	následník
anglického	anglický	k2eAgMnSc2d1	anglický
a	a	k8xC	a
skotského	skotský	k2eAgInSc2d1	skotský
trůnu	trůn	k1gInSc2	trůn
princ	princ	k1gMnSc1	princ
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Gloucesteru	Gloucester	k1gInSc2	Gloucester
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
parlament	parlament	k1gInSc1	parlament
přijal	přijmout	k5eAaPmAgInS	přijmout
zákon	zákon	k1gInSc4	zákon
(	(	kIx(	(
<g/>
Act	Act	k1gFnSc1	Act
of	of	k?	of
Settlement	settlement	k1gInSc1	settlement
1701	[number]	k4	1701
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
určil	určit	k5eAaPmAgInS	určit
Jiřího	Jiří	k1gMnSc2	Jiří
matku	matka	k1gFnSc4	matka
Žofii	Žofie	k1gFnSc6	Žofie
Hannoverskou	hannoverský	k2eAgFnSc7d1	hannoverská
nástupkyní	nástupkyně	k1gFnSc7	nástupkyně
na	na	k7c6	na
anglickém	anglický	k2eAgInSc6d1	anglický
trůnu	trůn	k1gInSc6	trůn
po	po	k7c6	po
Vilémovi	Vilémův	k2eAgMnPc1d1	Vilémův
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
princezně	princezna	k1gFnSc3	princezna
Anně	Anna	k1gFnSc3	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Nástupnictví	nástupnictví	k1gNnSc1	nástupnictví
Žofie	Žofie	k1gFnSc2	Žofie
<g/>
,	,	kIx,	,
jen	jen	k9	jen
vzdálené	vzdálený	k2eAgFnPc1d1	vzdálená
příbuzné	příbuzná	k1gFnPc1	příbuzná
panovnického	panovnický	k2eAgInSc2d1	panovnický
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
určeno	určit	k5eAaPmNgNnS	určit
proto	proto	k6eAd1	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
nejbližší	blízký	k2eAgFnSc4d3	nejbližší
protestantskou	protestantský	k2eAgFnSc4d1	protestantská
příbuznou	příbuzná	k1gFnSc4	příbuzná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nástup	nástup	k1gInSc1	nástup
na	na	k7c4	na
britský	britský	k2eAgInSc4d1	britský
trůn	trůn	k1gInSc4	trůn
==	==	k?	==
</s>
</p>
<p>
<s>
Jiřího	Jiří	k1gMnSc2	Jiří
matka	matka	k1gFnSc1	matka
Žofie	Žofie	k1gFnSc1	Žofie
zemřela	zemřít	k5eAaPmAgFnS	zemřít
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1714	[number]	k4	1714
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
se	se	k3xPyFc4	se
tak	tak	k9	tak
stal	stát	k5eAaPmAgInS	stát
přímým	přímý	k2eAgMnSc7d1	přímý
nástupcem	nástupce	k1gMnSc7	nástupce
Anny	Anna	k1gFnSc2	Anna
Stuartovny	Stuartovna	k1gFnSc2	Stuartovna
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
zemřela	zemřít	k5eAaPmAgFnS	zemřít
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
byl	být	k5eAaImAgMnS	být
následně	následně	k6eAd1	následně
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
králem	král	k1gMnSc7	král
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Částečné	částečný	k2eAgNnSc4d1	částečné
vlivem	vlivem	k7c2	vlivem
nepříznivého	příznivý	k2eNgInSc2d1	nepříznivý
větru	vítr	k1gInSc2	vítr
dorazil	dorazit	k5eAaPmAgMnS	dorazit
Jiří	Jiří	k1gMnSc1	Jiří
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
až	až	k9	až
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byl	být	k5eAaImAgMnS	být
korunován	korunován	k2eAgMnSc1d1	korunován
ve	v	k7c6	v
Westminsterském	Westminsterský	k2eAgNnSc6d1	Westminsterské
opatství	opatství	k1gNnSc6	opatství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
nástupu	nástup	k1gInSc6	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
roku	rok	k1gInSc2	rok
1715	[number]	k4	1715
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
všeobecné	všeobecný	k2eAgFnPc4d1	všeobecná
volby	volba	k1gFnPc4	volba
whigové	whig	k1gMnPc1	whig
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
členů	člen	k1gMnPc2	člen
poražené	poražený	k2eAgFnSc2d1	poražená
strany	strana	k1gFnSc2	strana
toryů	tory	k1gMnPc2	tory
sympatizovalo	sympatizovat	k5eAaImAgNnS	sympatizovat
s	s	k7c7	s
jakobity	jakobita	k1gMnPc7	jakobita
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
nespokojení	spokojený	k2eNgMnPc1d1	nespokojený
toryové	tory	k1gMnPc1	tory
se	se	k3xPyFc4	se
připojili	připojit	k5eAaPmAgMnP	připojit
k	k	k7c3	k
jakobitské	jakobitský	k2eAgFnSc3d1	jakobitská
vzpouře	vzpoura	k1gFnSc3	vzpoura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
The	The	k1gFnSc1	The
Fifteen	Fiftena	k1gFnPc2	Fiftena
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Patnáctka	patnáctka	k1gFnSc1	patnáctka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jakobité	jakobita	k1gMnPc1	jakobita
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
prosadit	prosadit	k5eAaPmF	prosadit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
Annina	Annin	k2eAgMnSc2d1	Annin
katolického	katolický	k2eAgMnSc2d1	katolický
nevlastního	vlastní	k2eNgMnSc2d1	nevlastní
bratra	bratr	k1gMnSc2	bratr
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
.	.	kIx.	.
</s>
<s>
Jakubovi	Jakubův	k2eAgMnPc1d1	Jakubův
podporovatelé	podporovatel	k1gMnPc1	podporovatel
vedení	vedení	k1gNnSc2	vedení
Johnem	John	k1gMnSc7	John
Erskinem	Erskin	k1gMnSc7	Erskin
<g/>
,	,	kIx,	,
nespokojeným	spokojený	k2eNgMnSc7d1	nespokojený
skotským	skotský	k2eAgMnSc7d1	skotský
šlechticem	šlechtic	k1gMnSc7	šlechtic
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
podporoval	podporovat	k5eAaImAgMnS	podporovat
slavnou	slavný	k2eAgFnSc4d1	slavná
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
podnítili	podnítit	k5eAaPmAgMnP	podnítit
vzpouru	vzpoura	k1gFnSc4	vzpoura
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
podpora	podpora	k1gFnSc1	podpora
jakobitů	jakobita	k1gMnPc2	jakobita
silnější	silný	k2eAgFnSc2d2	silnější
než	než	k8xS	než
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rebelie	rebelie	k1gFnSc1	rebelie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neměla	mít	k5eNaImAgFnS	mít
velkou	velký	k2eAgFnSc4d1	velká
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Erskinovy	Erskinův	k2eAgInPc1d1	Erskinův
válečné	válečný	k2eAgInPc1d1	válečný
plány	plán	k1gInPc1	plán
byly	být	k5eAaImAgInP	být
slabé	slabý	k2eAgInPc1d1	slabý
a	a	k8xC	a
Jakub	Jakub	k1gMnSc1	Jakub
dorazil	dorazit	k5eAaPmAgMnS	dorazit
pozdě	pozdě	k6eAd1	pozdě
<g/>
,	,	kIx,	,
s	s	k7c7	s
malými	malý	k2eAgInPc7d1	malý
finančními	finanční	k2eAgInPc7d1	finanční
prostředky	prostředek	k1gInPc7	prostředek
a	a	k8xC	a
příliš	příliš	k6eAd1	příliš
malým	malý	k2eAgNnSc7d1	malé
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
tato	tento	k3xDgFnSc1	tento
akce	akce	k1gFnPc4	akce
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Erskin	Erskin	k1gMnSc1	Erskin
a	a	k8xC	a
Jakub	Jakub	k1gMnSc1	Jakub
Stuart	Stuarta	k1gFnPc2	Stuarta
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
povstání	povstání	k1gNnSc2	povstání
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
osob	osoba	k1gFnPc2	osoba
popraveno	popravit	k5eAaPmNgNnS	popravit
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
majetek	majetek	k1gInSc1	majetek
zabaven	zabavit	k5eAaPmNgInS	zabavit
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
ovlivňoval	ovlivňovat	k5eAaImAgMnS	ovlivňovat
reakci	reakce	k1gFnSc4	reakce
vlády	vláda	k1gFnSc2	vláda
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
projevena	projeven	k2eAgFnSc1d1	projevena
shovívavost	shovívavost	k1gFnSc1	shovívavost
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
příjmů	příjem	k1gInPc2	příjem
ze	z	k7c2	z
zabaveného	zabavený	k2eAgInSc2d1	zabavený
majetku	majetek	k1gInSc2	majetek
věnoval	věnovat	k5eAaImAgMnS	věnovat
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
škol	škola	k1gFnPc2	škola
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
a	a	k8xC	a
část	část	k1gFnSc4	část
na	na	k7c4	na
úhradu	úhrada	k1gFnSc4	úhrada
státního	státní	k2eAgInSc2d1	státní
dluhu	dluh	k1gInSc2	dluh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiřího	Jiří	k1gMnSc4	Jiří
nedůvěra	nedůvěra	k1gFnSc1	nedůvěra
vůči	vůči	k7c3	vůči
Toryům	tory	k1gMnPc3	tory
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
vlivů	vliv	k1gInPc2	vliv
whigů	whig	k1gMnPc2	whig
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
převaha	převaha	k1gFnSc1	převaha
byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
toryové	tory	k1gMnPc1	tory
dostali	dostat	k5eAaPmAgMnP	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
až	až	k6eAd1	až
za	za	k7c4	za
dalších	další	k2eAgNnPc2d1	další
padesát	padesát	k4xCc4	padesát
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1715	[number]	k4	1715
prodloužili	prodloužit	k5eAaPmAgMnP	prodloužit
whigové	whig	k1gMnPc1	whig
maximální	maximální	k2eAgNnPc4d1	maximální
období	období	k1gNnPc4	období
parlamentu	parlament	k1gInSc2	parlament
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gMnPc3	on
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
delší	dlouhý	k2eAgNnSc4d2	delší
období	období	k1gNnSc4	období
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vláda	vláda	k1gFnSc1	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1715	[number]	k4	1715
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
dostali	dostat	k5eAaPmAgMnP	dostat
whigové	whig	k1gMnPc1	whig
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
vlivnými	vlivný	k2eAgMnPc7d1	vlivný
ministry	ministr	k1gMnPc7	ministr
Robert	Robert	k1gMnSc1	Robert
Walpole	Walpole	k1gFnSc1	Walpole
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Townshend	Townshend	k1gMnSc1	Townshend
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Stanhope	Stanhop	k1gInSc5	Stanhop
a	a	k8xC	a
Charles	Charles	k1gMnSc1	Charles
Spencer	Spencer	k1gMnSc1	Spencer
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1717	[number]	k4	1717
byl	být	k5eAaImAgMnS	být
Townshend	Townshend	k1gMnSc1	Townshend
odvolán	odvolat	k5eAaPmNgMnS	odvolat
a	a	k8xC	a
Walpole	Walpole	k1gFnSc1	Walpole
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
rozpory	rozpor	k1gInPc4	rozpor
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
ministry	ministr	k1gMnPc7	ministr
<g/>
,	,	kIx,	,
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Stanhope	Stanhop	k1gMnSc5	Stanhop
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
rozhodujícím	rozhodující	k2eAgMnSc7d1	rozhodující
ministrem	ministr	k1gMnSc7	ministr
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Sunderland	Sunderland	k1gInSc1	Sunderland
ovládal	ovládat	k5eAaImAgInS	ovládat
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sunderlandův	Sunderlandův	k2eAgInSc1d1	Sunderlandův
vliv	vliv	k1gInSc1	vliv
začal	začít	k5eAaPmAgInS	začít
upadat	upadat	k5eAaPmF	upadat
roku	rok	k1gInSc2	rok
1719	[number]	k4	1719
kdy	kdy	k6eAd1	kdy
předložil	předložit	k5eAaPmAgInS	předložit
zákon	zákon	k1gInSc1	zákon
(	(	kIx(	(
<g/>
Peerage	Peerage	k1gFnSc1	Peerage
Bill	Bill	k1gMnSc1	Bill
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
chtěl	chtít	k5eAaImAgMnS	chtít
omezit	omezit	k5eAaPmF	omezit
počet	počet	k1gInSc4	počet
členů	člen	k1gMnPc2	člen
Sněmovny	sněmovna	k1gFnSc2	sněmovna
lordů	lord	k1gMnPc2	lord
omezením	omezení	k1gNnSc7	omezení
vytváření	vytváření	k1gNnSc3	vytváření
nových	nový	k2eAgMnPc2d1	nový
peerů	peer	k1gMnPc2	peer
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
udržel	udržet	k5eAaPmAgMnS	udržet
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
horní	horní	k2eAgFnSc7d1	horní
komorou	komora	k1gFnSc7	komora
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
ale	ale	k9	ale
poražen	porazit	k5eAaPmNgMnS	porazit
Walpolem	Walpol	k1gInSc7	Walpol
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgInS	postavit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
opozice	opozice	k1gFnSc2	opozice
proti	proti	k7c3	proti
přijetí	přijetí	k1gNnSc3	přijetí
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Walpole	Walpole	k1gFnSc1	Walpole
a	a	k8xC	a
Townshend	Townshend	k1gMnSc1	Townshend
byli	být	k5eAaImAgMnP	být
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
znovu	znovu	k6eAd1	znovu
jmenováni	jmenovat	k5eAaBmNgMnP	jmenovat
ministry	ministr	k1gMnPc7	ministr
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
jednotná	jednotný	k2eAgFnSc1d1	jednotná
vláda	vláda	k1gFnSc1	vláda
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
whigů	whig	k1gMnPc2	whig
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
problém	problém	k1gInSc1	problém
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
vlivem	vlivem	k7c2	vlivem
finanční	finanční	k2eAgFnSc2d1	finanční
spekulace	spekulace	k1gFnSc2	spekulace
a	a	k8xC	a
správy	správa	k1gFnSc2	správa
státního	státní	k2eAgInSc2d1	státní
dluhu	dluh	k1gInSc2	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Určitá	určitý	k2eAgFnSc1d1	určitá
část	část	k1gFnSc1	část
státních	státní	k2eAgInPc2d1	státní
dluhopisů	dluhopis	k1gInPc2	dluhopis
nemohla	moct	k5eNaImAgFnS	moct
být	být	k5eAaImF	být
splacena	splatit	k5eAaPmNgFnS	splatit
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
držitele	držitel	k1gMnSc2	držitel
dluhopisu	dluhopis	k1gInSc2	dluhopis
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
úroková	úrokový	k2eAgFnSc1d1	úroková
míra	míra	k1gFnSc1	míra
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
dluhopis	dluhopis	k1gInSc1	dluhopis
tak	tak	k6eAd1	tak
představoval	představovat	k5eAaImAgInS	představovat
pro	pro	k7c4	pro
veřejný	veřejný	k2eAgInSc4d1	veřejný
rozpočet	rozpočet	k1gInSc4	rozpočet
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
zdroj	zdroj	k1gInSc4	zdroj
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dluhopisy	dluhopis	k1gInPc1	dluhopis
byly	být	k5eAaImAgInP	být
málokdy	málokdy	k6eAd1	málokdy
splaceny	splacen	k2eAgInPc1d1	splacen
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1719	[number]	k4	1719
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Společnost	společnost	k1gFnSc1	společnost
Jižního	jižní	k2eAgNnSc2d1	jižní
moře	moře	k1gNnSc2	moře
záměr	záměr	k1gInSc1	záměr
převzít	převzít	k5eAaPmF	převzít
tři	tři	k4xCgFnPc1	tři
pětiny	pětina	k1gFnPc1	pětina
státního	státní	k2eAgInSc2d1	státní
dluhu	dluh	k1gInSc2	dluh
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
vládní	vládní	k2eAgFnPc4d1	vládní
obligace	obligace	k1gFnPc4	obligace
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
podplatila	podplatit	k5eAaPmAgFnS	podplatit
Sunderlanda	Sunderlanda	k1gFnSc1	Sunderlanda
<g/>
,	,	kIx,	,
Melusinu	Melusina	k1gFnSc4	Melusina
von	von	k1gInSc4	von
der	drát	k5eAaImRp2nS	drát
Schulenburg	Schulenburg	k1gInSc4	Schulenburg
a	a	k8xC	a
jednatele	jednatel	k1gMnSc2	jednatel
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
financí	finance	k1gFnPc2	finance
Charlese	Charles	k1gMnSc2	Charles
Stanhopeho	Stanhope	k1gMnSc2	Stanhope
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jejich	jejich	k3xOp3gInSc4	jejich
plán	plán	k1gInSc4	plán
podpořili	podpořit	k5eAaPmAgMnP	podpořit
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
lákala	lákat	k5eAaImAgFnS	lákat
držitele	držitel	k1gMnPc4	držitel
dluhopisů	dluhopis	k1gInPc2	dluhopis
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
převedli	převést	k5eAaPmAgMnP	převést
své	svůj	k3xOyFgInPc4	svůj
neprodejné	prodejný	k2eNgInPc4d1	neprodejný
dluhopisy	dluhopis	k1gInPc4	dluhopis
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
úrokem	úrok	k1gInSc7	úrok
na	na	k7c4	na
méně	málo	k6eAd2	málo
úročené	úročený	k2eAgInPc4d1	úročený
dluhopisy	dluhopis	k1gInPc4	dluhopis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
lépe	dobře	k6eAd2	dobře
obchodovatelné	obchodovatelný	k2eAgFnPc1d1	obchodovatelná
<g/>
.	.	kIx.	.
</s>
<s>
Akcie	akcie	k1gFnPc1	akcie
společnosti	společnost	k1gFnSc2	společnost
prudce	prudko	k6eAd1	prudko
vzrůstaly	vzrůstat	k5eAaImAgFnP	vzrůstat
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
měly	mít	k5eAaImAgFnP	mít
hodnotu	hodnota	k1gFnSc4	hodnota
128	[number]	k4	128
liber	libra	k1gFnPc2	libra
<g/>
,	,	kIx,	,
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
záměru	záměr	k1gInSc2	záměr
převzetí	převzetí	k1gNnSc2	převzetí
části	část	k1gFnSc2	část
státního	státní	k2eAgInSc2d1	státní
dluhu	dluh	k1gInSc2	dluh
stály	stát	k5eAaImAgFnP	stát
500	[number]	k4	500
liber	libra	k1gFnPc2	libra
a	a	k8xC	a
maxima	maxima	k1gFnSc1	maxima
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
ceněny	cenit	k5eAaImNgFnP	cenit
na	na	k7c4	na
1050	[number]	k4	1050
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nárůst	nárůst	k1gInSc1	nárůst
vedl	vést	k5eAaImAgInS	vést
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
spekulativnímu	spekulativní	k2eAgMnSc3d1	spekulativní
<g/>
,	,	kIx,	,
hodnot	hodnota	k1gFnPc2	hodnota
akcií	akcie	k1gFnPc2	akcie
ostatních	ostatní	k2eAgFnPc2d1	ostatní
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
Společnosti	společnost	k1gFnSc2	společnost
Jižního	jižní	k2eAgNnSc2d1	jižní
moře	moře	k1gNnSc2	moře
vydala	vydat	k5eAaPmAgFnS	vydat
zákon	zákon	k1gInSc4	zákon
(	(	kIx(	(
<g/>
Bubble	Bubble	k1gMnSc5	Bubble
Act	Act	k1gMnSc5	Act
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tento	tento	k3xDgInSc1	tento
nárůst	nárůst	k1gInSc1	nárůst
zastavil	zastavit	k5eAaPmAgInS	zastavit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ale	ale	k9	ale
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
nekontrolovatelný	kontrolovatelný	k2eNgInSc4d1	nekontrolovatelný
prodej	prodej	k1gInSc4	prodej
akcií	akcie	k1gFnPc2	akcie
a	a	k8xC	a
pád	pád	k1gInSc1	pád
hodnoty	hodnota	k1gFnSc2	hodnota
akcií	akcie	k1gFnPc2	akcie
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c4	na
150	[number]	k4	150
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
příslušníků	příslušník	k1gMnPc2	příslušník
aristokracie	aristokracie	k1gFnSc2	aristokracie
<g/>
,	,	kIx,	,
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
obrovské	obrovský	k2eAgFnPc4d1	obrovská
finanční	finanční	k2eAgFnPc4d1	finanční
částky	částka	k1gFnPc4	částka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
krize	krize	k1gFnSc1	krize
způsobila	způsobit	k5eAaPmAgFnS	způsobit
extrémní	extrémní	k2eAgFnSc1d1	extrémní
neoblíbenost	neoblíbenost	k1gFnSc1	neoblíbenost
Jiřího	Jiří	k1gMnSc2	Jiří
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
ministrů	ministr	k1gMnPc2	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Sunderland	Sunderland	k1gInSc1	Sunderland
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgInS	těšit
Jiřího	Jiří	k1gMnSc4	Jiří
důvěře	důvěra	k1gFnSc6	důvěra
<g/>
,	,	kIx,	,
podpořil	podpořit	k5eAaPmAgMnS	podpořit
vzestup	vzestup	k1gInSc4	vzestup
Roberta	Robert	k1gMnSc2	Robert
Walpolea	Walpoleus	k1gMnSc2	Walpoleus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
de	de	k?	de
facto	facta	k1gFnSc5	facta
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
nebyl	být	k5eNaImAgInS	být
s	s	k7c7	s
jeho	jeho	k3xOp3gInPc7	jeho
jménem	jméno	k1gNnSc7	jméno
oficiálně	oficiálně	k6eAd1	oficiálně
spojován	spojován	k2eAgMnSc1d1	spojován
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
řešení	řešení	k1gNnPc1	řešení
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
naplánování	naplánování	k1gNnSc1	naplánování
státního	státní	k2eAgInSc2d1	státní
dluhu	dluh	k1gInSc2	dluh
a	a	k8xC	a
přislíbení	přislíbení	k1gNnSc2	přislíbení
částečných	částečný	k2eAgFnPc2d1	částečná
kompenzací	kompenzace	k1gFnPc2	kompenzace
<g/>
,	,	kIx,	,
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
státní	státní	k2eAgFnPc4d1	státní
finance	finance	k1gFnPc4	finance
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
postižených	postižený	k1gMnPc2	postižený
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
krizí	krize	k1gFnSc7	krize
byl	být	k5eAaImAgInS	být
i	i	k9	i
Jiří	Jiří	k1gMnSc1	Jiří
samotný	samotný	k2eAgMnSc1d1	samotný
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
dokumenty	dokument	k1gInPc4	dokument
z	z	k7c2	z
královského	královský	k2eAgInSc2d1	královský
archívu	archív	k1gInSc2	archív
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozdní	pozdní	k2eAgNnSc4d1	pozdní
období	období	k1gNnSc4	období
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Walpoleův	Walpoleův	k2eAgInSc4d1	Walpoleův
požadavek	požadavek	k1gInSc4	požadavek
Jiří	Jiří	k1gMnSc1	Jiří
obnovil	obnovit	k5eAaPmAgMnS	obnovit
roku	rok	k1gInSc2	rok
1725	[number]	k4	1725
Řád	řád	k1gInSc1	řád
lázně	lázeň	k1gFnSc2	lázeň
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
Walpoleovi	Walpoleus	k1gMnSc3	Walpoleus
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
odměnit	odměnit	k5eAaPmF	odměnit
nebo	nebo	k8xC	nebo
získávat	získávat	k5eAaImF	získávat
politické	politický	k2eAgMnPc4d1	politický
spojence	spojenec	k1gMnPc4	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nejvlivnějším	vlivný	k2eAgMnSc7d3	nejvlivnější
mužem	muž	k1gMnSc7	muž
země	zem	k1gFnSc2	zem
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
samostatně	samostatně	k6eAd1	samostatně
jmenovat	jmenovat	k5eAaImF	jmenovat
ministry	ministr	k1gMnPc4	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
předchůdkyně	předchůdkyně	k1gFnSc2	předchůdkyně
Jiří	Jiří	k1gMnSc1	Jiří
téměř	téměř	k6eAd1	téměř
nenavštěvoval	navštěvovat	k5eNaImAgMnS	navštěvovat
jednání	jednání	k1gNnSc4	jednání
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
uplatňoval	uplatňovat	k5eAaImAgInS	uplatňovat
v	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
Jiří	Jiří	k1gMnSc1	Jiří
velmi	velmi	k6eAd1	velmi
závislý	závislý	k2eAgMnSc1d1	závislý
na	na	k7c6	na
Walpoleovi	Walpoleus	k1gMnSc6	Walpoleus
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
mohl	moct	k5eAaImAgMnS	moct
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
vůle	vůle	k1gFnSc2	vůle
odvolávat	odvolávat	k5eAaImF	odvolávat
ministry	ministr	k1gMnPc7	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Walpole	Walpola	k1gFnSc3	Walpola
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
období	období	k1gNnSc2	období
panování	panování	k1gNnSc2	panování
Jiřího	Jiří	k1gMnSc2	Jiří
odvolán	odvolán	k2eAgMnSc1d1	odvolán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tyto	tento	k3xDgFnPc1	tento
obavy	obava	k1gFnPc1	obava
se	se	k3xPyFc4	se
nenaplnily	naplnit	k5eNaPmAgFnP	naplnit
<g/>
,	,	kIx,	,
když	když	k8xS	když
Jiří	Jiří	k1gMnSc1	Jiří
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
rodného	rodný	k2eAgInSc2d1	rodný
Hannoveru	Hannover	k1gInSc2	Hannover
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
prodělal	prodělat	k5eAaPmAgMnS	prodělat
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1727	[number]	k4	1727
záchvat	záchvat	k1gInSc4	záchvat
mrtvice	mrtvice	k1gFnSc2	mrtvice
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
biskupského	biskupský	k2eAgInSc2d1	biskupský
paláce	palác	k1gInSc2	palác
v	v	k7c6	v
Osnabrücku	Osnabrück	k1gInSc6	Osnabrück
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ráno	ráno	k6eAd1	ráno
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
hradu	hrad	k1gInSc2	hrad
Leine	Lein	k1gMnSc5	Lein
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
převezeny	převezen	k2eAgInPc1d1	převezen
do	do	k7c2	do
Herrenhausenu	Herrenhausen	k2eAgFnSc4d1	Herrenhausen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Jiří	Jiří	k1gMnSc1	Jiří
Augustus	Augustus	k1gMnSc1	Augustus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
vlády	vláda	k1gFnSc2	vláda
jako	jako	k8xS	jako
Jiří	Jiří	k1gMnSc1	Jiří
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Walpolea	Walpolea	k1gFnSc1	Walpolea
odvolá	odvolat	k5eAaPmIp3nS	odvolat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
radu	rada	k1gFnSc4	rada
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
Karoliny	Karolinum	k1gNnPc7	Karolinum
ho	on	k3xPp3gMnSc4	on
ponechal	ponechat	k5eAaPmAgMnS	ponechat
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Walpole	Walpole	k1gFnSc1	Walpole
totiž	totiž	k9	totiž
ovládal	ovládat	k5eAaImAgInS	ovládat
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
mírnou	mírný	k2eAgFnSc4d1	mírná
většinu	většina	k1gFnSc4	většina
a	a	k8xC	a
tak	tak	k9	tak
pokud	pokud	k8xS	pokud
Jiří	Jiří	k1gMnSc1	Jiří
II	II	kA	II
<g/>
.	.	kIx.	.
nechtěl	chtít	k5eNaImAgMnS	chtít
riskovat	riskovat	k5eAaBmF	riskovat
politickou	politický	k2eAgFnSc4d1	politická
nestabilitu	nestabilita	k1gFnSc4	nestabilita
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
období	období	k1gNnSc6	období
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
posilování	posilování	k1gNnSc3	posilování
moci	moc	k1gFnSc2	moc
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
George	Georg	k1gMnSc2	Georg
I	I	kA	I
of	of	k?	of
Great	Great	k2eAgInSc4d1	Great
Britain	Britain	k1gInSc4	Britain
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jiří	Jiří	k1gMnSc1	Jiří
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.nndb.com/people/397/000093118/	[url]	k4	http://www.nndb.com/people/397/000093118/
</s>
</p>
<p>
<s>
http://www.thepeerage.com/p10139.htm#i101381	[url]	k4	http://www.thepeerage.com/p10139.htm#i101381
</s>
</p>
