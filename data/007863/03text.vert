<s>
Belgie	Belgie	k1gFnSc1	Belgie
(	(	kIx(	(
<g/>
nizozemsky	nizozemsky	k6eAd1	nizozemsky
België	België	k1gFnSc1	België
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Belgique	Belgique	k1gFnSc1	Belgique
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Belgien	Belgien	k2eAgMnSc1d1	Belgien
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Belgické	belgický	k2eAgNnSc4d1	Belgické
království	království	k1gNnSc4	království
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
federativní	federativní	k2eAgFnSc1d1	federativní
konstituční	konstituční	k2eAgFnSc1d1	konstituční
monarchie	monarchie	k1gFnSc1	monarchie
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Belgie	Belgie	k1gFnSc1	Belgie
hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
(	(	kIx(	(
<g/>
620	[number]	k4	620
km	km	kA	km
<g/>
)	)	kIx)	)
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
s	s	k7c7	s
Lucemburskem	Lucembursko	k1gNnSc7	Lucembursko
(	(	kIx(	(
<g/>
148	[number]	k4	148
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
(	(	kIx(	(
<g/>
167	[number]	k4	167
km	km	kA	km
<g/>
)	)	kIx)	)
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
s	s	k7c7	s
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
(	(	kIx(	(
<g/>
450	[number]	k4	450
km	km	kA	km
<g/>
)	)	kIx)	)
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
severozápadu	severozápad	k1gInSc2	severozápad
omývá	omývat	k5eAaImIp3nS	omývat
Belgii	Belgie	k1gFnSc4	Belgie
Severní	severní	k2eAgNnSc1d1	severní
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
66	[number]	k4	66
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Belgie	Belgie	k1gFnSc1	Belgie
je	být	k5eAaImIp3nS	být
členskou	členský	k2eAgFnSc7d1	členská
zemí	zem	k1gFnSc7	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
(	(	kIx(	(
<g/>
NATO	NATO	kA	NATO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgInPc1	tři
oficiální	oficiální	k2eAgInPc1d1	oficiální
jazyky	jazyk	k1gInPc1	jazyk
<g/>
:	:	kIx,	:
nizozemština	nizozemština	k1gFnSc1	nizozemština
<g/>
,	,	kIx,	,
francouzština	francouzština	k1gFnSc1	francouzština
a	a	k8xC	a
němčina	němčina	k1gFnSc1	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Kulturně	kulturně	k6eAd1	kulturně
<g/>
,	,	kIx,	,
politicky	politicky	k6eAd1	politicky
a	a	k8xC	a
sociologicky	sociologicky	k6eAd1	sociologicky
se	se	k3xPyFc4	se
Belgie	Belgie	k1gFnSc1	Belgie
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
velkých	velký	k2eAgNnPc2d1	velké
společenství	společenství	k1gNnPc2	společenství
<g/>
,	,	kIx,	,
nizozemsky	nizozemsky	k6eAd1	nizozemsky
mluvících	mluvící	k2eAgMnPc2d1	mluvící
Vlámů	Vlám	k1gMnPc2	Vlám
a	a	k8xC	a
frankofonních	frankofonní	k2eAgMnPc2d1	frankofonní
Valonů	Valon	k1gMnPc2	Valon
<g/>
,	,	kIx,	,
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
žije	žít	k5eAaImIp3nS	žít
také	také	k9	také
menší	malý	k2eAgFnSc1d2	menší
německá	německý	k2eAgFnSc1d1	německá
komunita	komunita	k1gFnSc1	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Belgická	belgický	k2eAgFnSc1d1	belgická
federace	federace	k1gFnSc1	federace
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
3	[number]	k4	3
společenství	společenství	k1gNnPc2	společenství
<g/>
,	,	kIx,	,
rozdělených	rozdělený	k2eAgInPc2d1	rozdělený
podle	podle	k7c2	podle
jazykového	jazykový	k2eAgInSc2d1	jazykový
principu	princip	k1gInSc2	princip
<g/>
,	,	kIx,	,
Vlámského	vlámský	k2eAgNnSc2d1	vlámské
společenství	společenství	k1gNnSc2	společenství
(	(	kIx(	(
<g/>
niz.	niz.	k?	niz.
Vlaamse	Vlaamse	k1gFnSc1	Vlaamse
Gemeenschap	Gemeenschap	k1gInSc1	Gemeenschap
<g/>
)	)	kIx)	)
Francouzského	francouzský	k2eAgNnSc2d1	francouzské
společenství	společenství	k1gNnSc2	společenství
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
Communauté	Communautý	k2eAgFnSc2d1	Communautý
francaise	francaise	k1gFnSc2	francaise
<g/>
)	)	kIx)	)
Německojazyčného	německojazyčný	k2eAgNnSc2d1	německojazyčné
společenství	společenství	k1gNnSc2	společenství
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Deutschsprachige	Deutschsprachige	k1gInSc1	Deutschsprachige
Gemeinschaft	Gemeinschaft	k1gInSc1	Gemeinschaft
<g/>
)	)	kIx)	)
a	a	k8xC	a
ze	z	k7c2	z
3	[number]	k4	3
regionů	region	k1gInPc2	region
<g/>
,	,	kIx,	,
rozčleněných	rozčleněný	k2eAgFnPc2d1	rozčleněná
podle	podle	k7c2	podle
<g />
.	.	kIx.	.
</s>
<s>
principu	princip	k1gInSc2	princip
územního	územní	k2eAgInSc2d1	územní
<g/>
:	:	kIx,	:
Vlámský	vlámský	k2eAgInSc1d1	vlámský
region	region	k1gInSc1	region
(	(	kIx(	(
<g/>
Flandry	Flandry	k1gInPc1	Flandry
<g/>
,	,	kIx,	,
niz.	niz.	k?	niz.
Vlaams	Vlaams	k1gInSc1	Vlaams
Gewest	Gewest	k1gInSc1	Gewest
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
Flandern	Flandern	k1gInSc1	Flandern
<g/>
)	)	kIx)	)
Valonský	valonský	k2eAgInSc1d1	valonský
region	region	k1gInSc1	region
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
Région	Région	k1gInSc1	Région
Wallonne	Wallonn	k1gMnSc5	Wallonn
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
Wallonie	Wallonie	k1gFnSc2	Wallonie
<g/>
)	)	kIx)	)
Region	region	k1gInSc4	region
Brusel-hlavní	Brusellavní	k2eAgNnSc1d1	Brusel-hlavní
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
niz.	niz.	k?	niz.
Brussels	Brussels	k1gInSc1	Brussels
Hoofdstedelijk	Hoofdstedelijk	k1gMnSc1	Hoofdstedelijk
Gewest	Gewest	k1gMnSc1	Gewest
<g/>
,	,	kIx,	,
fr.	fr.	k?	fr.
Région	Région	k1gInSc1	Région
de	de	k?	de
Bruxelles-Capitale	Bruxelles-Capital	k1gMnSc5	Bruxelles-Capital
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
něm.	něm.	k?	něm.
Die	Die	k1gFnSc1	Die
Region	region	k1gInSc1	region
Brüssel-Hauptstadt	Brüssel-Hauptstadt	k1gInSc1	Brüssel-Hauptstadt
<g/>
)	)	kIx)	)
Každý	každý	k3xTgInSc1	každý
region	region	k1gInSc1	region
a	a	k8xC	a
každé	každý	k3xTgNnSc1	každý
společenství	společenství	k1gNnSc1	společenství
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
legislativní	legislativní	k2eAgInSc4d1	legislativní
orgán	orgán	k1gInSc4	orgán
–	–	k?	–
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
a	a	k8xC	a
orgán	orgán	k1gInSc4	orgán
exekutivní	exekutivní	k2eAgFnSc4d1	exekutivní
–	–	k?	–
vládu	vláda	k1gFnSc4	vláda
(	(	kIx(	(
<g/>
či	či	k8xC	či
radu	rada	k1gFnSc4	rada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Vlámska	Vlámsko	k1gNnSc2	Vlámsko
a	a	k8xC	a
Vlámského	vlámský	k2eAgNnSc2d1	vlámské
společenství	společenství	k1gNnSc2	společenství
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
vládu	vláda	k1gFnSc4	vláda
i	i	k9	i
parlament	parlament	k1gInSc4	parlament
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Včetně	včetně	k7c2	včetně
orgánů	orgán	k1gInPc2	orgán
federálních	federální	k2eAgInPc2d1	federální
tak	tak	k6eAd1	tak
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
působí	působit	k5eAaImIp3nS	působit
6	[number]	k4	6
vlád	vláda	k1gFnPc2	vláda
a	a	k8xC	a
6	[number]	k4	6
parlamentů	parlament	k1gInPc2	parlament
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Patronem	patron	k1gMnSc7	patron
Belgie	Belgie	k1gFnSc2	Belgie
je	být	k5eAaImIp3nS	být
svatý	svatý	k2eAgMnSc1d1	svatý
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
národní	národní	k2eAgNnSc1d1	národní
heslo	heslo	k1gNnSc1	heslo
zní	znět	k5eAaImIp3nS	znět
Jednota	jednota	k1gFnSc1	jednota
posiluje	posilovat	k5eAaImIp3nS	posilovat
(	(	kIx(	(
<g/>
niz.	niz.	k?	niz.
Eendracht	Eendracht	k2eAgInSc4d1	Eendracht
maakt	maakt	k1gInSc4	maakt
macht	machta	k1gFnPc2	machta
<g/>
,	,	kIx,	,
fr.	fr.	k?	fr.
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
union	union	k1gInSc4	union
fait	fait	k5eAaPmF	fait
la	la	k1gNnSc4	la
force	force	k1gFnSc2	force
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
Einigkeit	Einigkeit	k2eAgInSc1d1	Einigkeit
macht	macht	k2eAgInSc1d1	macht
stark	stark	k1gInSc1	stark
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejstarších	starý	k2eAgFnPc6d3	nejstarší
dobách	doba	k1gFnPc6	doba
obývalo	obývat	k5eAaImAgNnS	obývat
Belgii	Belgie	k1gFnSc6	Belgie
několik	několik	k4yIc1	několik
různých	různý	k2eAgMnPc2d1	různý
keltských	keltský	k2eAgMnPc2d1	keltský
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgInPc1	tento
kmeny	kmen	k1gInPc1	kmen
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgInPc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mezi	mezi	k7c7	mezi
Severním	severní	k2eAgNnSc7d1	severní
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
,	,	kIx,	,
Seinou	Seina	k1gFnSc7	Seina
a	a	k8xC	a
Marnou	marný	k2eAgFnSc7d1	marná
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc6d1	severní
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Německa	Německo	k1gNnSc2	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
označovány	označovat	k5eAaImNgFnP	označovat
latinským	latinský	k2eAgNnSc7d1	latinské
slovem	slovo	k1gNnSc7	slovo
Belgae	Belga	k1gFnSc2	Belga
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
Belgové	Belg	k1gMnPc1	Belg
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
název	název	k1gInSc4	název
státu	stát	k1gInSc2	stát
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
genetického	genetický	k2eAgInSc2d1	genetický
výzkumu	výzkum	k1gInSc2	výzkum
však	však	k9	však
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dnešní	dnešní	k2eAgMnPc1d1	dnešní
Belgičané	Belgičan	k1gMnPc1	Belgičan
mají	mít	k5eAaImIp3nP	mít
s	s	k7c7	s
dávnými	dávný	k2eAgMnPc7d1	dávný
Belgy	Belg	k1gMnPc7	Belg
pramálo	pramálo	k6eAd1	pramálo
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
první	první	k4xOgFnSc4	první
dobře	dobře	k6eAd1	dobře
zdokumentované	zdokumentovaný	k2eAgFnPc1d1	zdokumentovaná
události	událost	k1gFnPc1	událost
patří	patřit	k5eAaImIp3nP	patřit
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
oblasti	oblast	k1gFnSc2	oblast
Římany	Říman	k1gMnPc7	Říman
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
století	století	k1gNnSc6	století
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
,	,	kIx,	,
v	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
století	století	k1gNnSc6	století
pak	pak	k8xC	pak
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Belgie	Belgie	k1gFnSc2	Belgie
osídlil	osídlit	k5eAaPmAgMnS	osídlit
germánský	germánský	k2eAgInSc4d1	germánský
kmen	kmen	k1gInSc4	kmen
Franků	Frank	k1gMnPc2	Frank
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
franské	franský	k2eAgFnSc2d1	Franská
říše	říš	k1gFnSc2	říš
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
území	území	k1gNnSc2	území
mezi	mezi	k7c4	mezi
západofranskou	západofranský	k2eAgFnSc4d1	západofranská
říši	říše	k1gFnSc4	říše
Karla	Karel	k1gMnSc2	Karel
Holého	Holý	k1gMnSc2	Holý
a	a	k8xC	a
říši	říše	k1gFnSc4	říše
Lothara	Lothar	k1gMnSc2	Lothar
I.	I.	kA	I.
Hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
říšemi	říš	k1gFnPc7	říš
tvořil	tvořit	k5eAaImAgInS	tvořit
tok	tok	k1gInSc1	tok
řeky	řeka	k1gFnSc2	řeka
Šeldy	Šelda	k1gFnSc2	Šelda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
středověku	středověk	k1gInSc2	středověk
se	se	k3xPyFc4	se
oblast	oblast	k1gFnSc1	oblast
postupně	postupně	k6eAd1	postupně
rozdrobila	rozdrobit	k5eAaPmAgFnS	rozdrobit
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
drobných	drobný	k2eAgInPc2d1	drobný
feudálních	feudální	k2eAgInPc2d1	feudální
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
řadu	řad	k1gInSc2	řad
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
sjednotili	sjednotit	k5eAaPmAgMnP	sjednotit
burgundští	burgundský	k2eAgMnPc1d1	burgundský
vévodové	vévoda	k1gMnPc1	vévoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tyto	tento	k3xDgInPc1	tento
státy	stát	k1gInPc1	stát
získaly	získat	k5eAaPmAgInP	získat
určitou	určitý	k2eAgFnSc4d1	určitá
autonomii	autonomie	k1gFnSc4	autonomie
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
označovat	označovat	k5eAaImF	označovat
názvem	název	k1gInSc7	název
Sedmnáct	sedmnáct	k4xCc4	sedmnáct
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
klíčovému	klíčový	k2eAgInSc3d1	klíčový
zlomu	zlom	k1gInSc3	zlom
došlo	dojít	k5eAaPmAgNnS	dojít
za	za	k7c2	za
Osmdesátileté	osmdesátiletý	k2eAgFnSc2d1	osmdesátiletá
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1568	[number]	k4	1568
<g/>
–	–	k?	–
<g/>
1648	[number]	k4	1648
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
třiceti	třicet	k4xCc6	třicet
letech	léto	k1gNnPc6	léto
kryla	krýt	k5eAaImAgFnS	krýt
i	i	k9	i
s	s	k7c7	s
nám	my	k3xPp1nPc3	my
dobře	dobře	k6eAd1	dobře
známou	známý	k2eAgFnSc7d1	známá
třicetiletou	třicetiletý	k2eAgFnSc7d1	třicetiletá
válkou	válka	k1gFnSc7	válka
(	(	kIx(	(
<g/>
1618	[number]	k4	1618
<g/>
–	–	k?	–
<g/>
1648	[number]	k4	1648
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
severní	severní	k2eAgNnSc1d1	severní
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
si	se	k3xPyFc3	se
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
uchovat	uchovat	k5eAaPmF	uchovat
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dobyta	dobýt	k5eAaPmNgFnS	dobýt
Španěly	Španěl	k1gMnPc7	Španěl
a	a	k8xC	a
připadla	připadnout	k5eAaPmAgFnS	připadnout
španělské	španělský	k2eAgNnSc4d1	španělské
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
rakouské	rakouský	k2eAgFnSc3d1	rakouská
větvi	větev	k1gFnSc3	větev
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Belgie	Belgie	k1gFnSc1	Belgie
nakrátko	nakrátko	k6eAd1	nakrátko
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1815	[number]	k4	1815
<g/>
,	,	kIx,	,
po	po	k7c6	po
Napoleonově	Napoleonův	k2eAgFnSc6d1	Napoleonova
porážce	porážka	k1gFnSc6	porážka
u	u	k7c2	u
Waterloo	Waterloo	k1gNnSc2	Waterloo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Belgie	Belgie	k1gFnSc1	Belgie
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
nizozemského	nizozemský	k2eAgNnSc2d1	Nizozemské
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
nárazníkový	nárazníkový	k2eAgInSc1d1	nárazníkový
stát	stát	k1gInSc1	stát
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
15	[number]	k4	15
letech	léto	k1gNnPc6	léto
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
v	v	k7c6	v
bruselském	bruselský	k2eAgNnSc6d1	bruselské
operním	operní	k2eAgNnSc6d1	operní
divadle	divadlo	k1gNnSc6	divadlo
La	la	k1gNnPc2	la
Monnaie	Monnaie	k1gFnSc2	Monnaie
revoluce	revoluce	k1gFnSc1	revoluce
a	a	k8xC	a
Belgie	Belgie	k1gFnSc1	Belgie
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
stala	stát	k5eAaPmAgFnS	stát
nezávislou	závislý	k2eNgFnSc7d1	nezávislá
a	a	k8xC	a
neutrální	neutrální	k2eAgFnSc7d1	neutrální
konstituční	konstituční	k2eAgFnSc7d1	konstituční
monarchií	monarchie	k1gFnSc7	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
belgický	belgický	k2eAgMnSc1d1	belgický
král	král	k1gMnSc1	král
usedl	usednout	k5eAaPmAgMnS	usednout
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
Leopold	Leopold	k1gMnSc1	Leopold
I.	I.	kA	I.
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
i	i	k8xC	i
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
Belgie	Belgie	k1gFnSc1	Belgie
přes	přes	k7c4	přes
svoji	svůj	k3xOyFgFnSc4	svůj
deklarovanou	deklarovaný	k2eAgFnSc4d1	deklarovaná
neutralitu	neutralita	k1gFnSc4	neutralita
okupována	okupován	k2eAgFnSc1d1	okupována
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Belgicko-lucemburská	belgickoucemburský	k2eAgFnSc1d1	belgicko-lucemburský
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
do	do	k7c2	do
účinnosti	účinnost	k1gFnSc2	účinnost
přišla	přijít	k5eAaPmAgFnS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráce	spolupráce	k1gFnPc1	spolupráce
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
státy	stát	k1gInPc7	stát
se	se	k3xPyFc4	se
projevovala	projevovat	k5eAaImAgFnS	projevovat
např.	např.	kA	např.
vytvořením	vytvoření	k1gNnSc7	vytvoření
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
používat	používat	k5eAaImF	používat
až	až	k9	až
do	do	k7c2	do
zavedení	zavedení	k1gNnSc2	zavedení
eura	euro	k1gNnSc2	euro
belgický	belgický	k2eAgInSc1d1	belgický
frank	frank	k1gInSc1	frank
na	na	k7c6	na
lucemburském	lucemburský	k2eAgNnSc6d1	lucemburské
území	území	k1gNnSc6	území
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
byla	být	k5eAaImAgFnS	být
Belgie	Belgie	k1gFnSc1	Belgie
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Belgické	belgický	k2eAgNnSc1d1	Belgické
Kongo	Kongo	k1gNnSc1	Kongo
získalo	získat	k5eAaPmAgNnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
a	a	k8xC	a
Ruanda-Urundi	Ruanda-Urund	k1gMnPc1	Ruanda-Urund
o	o	k7c4	o
2	[number]	k4	2
roky	rok	k1gInPc7	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Belgie	Belgie	k1gFnSc1	Belgie
je	být	k5eAaImIp3nS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
Evropského	evropský	k2eAgNnSc2d1	Evropské
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
společenství	společenství	k1gNnSc2	společenství
(	(	kIx(	(
<g/>
EHS	EHS	kA	EHS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Evropského	evropský	k2eAgNnSc2d1	Evropské
společenství	společenství	k1gNnSc2	společenství
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
oceli	ocel	k1gFnSc2	ocel
(	(	kIx(	(
<g/>
ESUO	ESUO	kA	ESUO
<g/>
)	)	kIx)	)
a	a	k8xC	a
Evropského	evropský	k2eAgNnSc2d1	Evropské
společenství	společenství	k1gNnSc2	společenství
pro	pro	k7c4	pro
atomovou	atomový	k2eAgFnSc4d1	atomová
energii	energie	k1gFnSc4	energie
(	(	kIx(	(
<g/>
Euratomu	Euratom	k1gInSc2	Euratom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
daly	dát	k5eAaPmAgFnP	dát
základ	základ	k1gInSc4	základ
budoucí	budoucí	k2eAgFnSc3d1	budoucí
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
více	hodně	k6eAd2	hodně
prosperovalo	prosperovat	k5eAaImAgNnS	prosperovat
Valonsko	Valonsko	k1gNnSc1	Valonsko
s	s	k7c7	s
rozvinutým	rozvinutý	k2eAgInSc7d1	rozvinutý
průmyslem	průmysl	k1gInSc7	průmysl
i	i	k8xC	i
kulturou	kultura	k1gFnSc7	kultura
<g/>
,	,	kIx,	,
Flandry	Flandry	k1gInPc1	Flandry
byly	být	k5eAaImAgInP	být
kulturně	kulturně	k6eAd1	kulturně
i	i	k9	i
ekonomicky	ekonomicky	k6eAd1	ekonomicky
zaostalejší	zaostalý	k2eAgFnSc1d2	zaostalejší
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
však	však	k9	však
situace	situace	k1gFnSc1	situace
postupně	postupně	k6eAd1	postupně
otočila	otočit	k5eAaPmAgFnS	otočit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
různým	různý	k2eAgFnPc3d1	různá
třenicím	třenice	k1gFnPc3	třenice
mezi	mezi	k7c7	mezi
Vlámy	Vlám	k1gMnPc7	Vlám
a	a	k8xC	a
Valony	Valon	k1gMnPc7	Valon
<g/>
.	.	kIx.	.
</s>
<s>
Federalizace	federalizace	k1gFnSc1	federalizace
Belgie	Belgie	k1gFnSc1	Belgie
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
pěti	pět	k4xCc6	pět
etapách	etapa	k1gFnPc6	etapa
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
signálem	signál	k1gInSc7	signál
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
rozpad	rozpad	k1gInSc1	rozpad
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Lovani	Lovaň	k1gFnSc6	Lovaň
na	na	k7c4	na
vlámskou	vlámský	k2eAgFnSc4d1	vlámská
a	a	k8xC	a
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
ústavy	ústava	k1gFnSc2	ústava
zanesen	zanesen	k2eAgInSc1d1	zanesen
princip	princip	k1gInSc1	princip
tří	tři	k4xCgMnPc2	tři
"	"	kIx"	"
<g/>
kulturních	kulturní	k2eAgNnPc2d1	kulturní
společenství	společenství	k1gNnPc2	společenství
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vlámské	vlámský	k2eAgFnSc2d1	vlámská
<g/>
,	,	kIx,	,
frankofonní	frankofonní	k2eAgFnSc2d1	frankofonní
a	a	k8xC	a
germanofonní	germanofonní	k2eAgFnSc2d1	germanofonní
<g/>
)	)	kIx)	)
a	a	k8xC	a
tři	tři	k4xCgInPc1	tři
územní	územní	k2eAgInPc1d1	územní
"	"	kIx"	"
<g/>
regiony	region	k1gInPc1	region
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
z	z	k7c2	z
názvů	název	k1gInPc2	název
společenství	společenství	k1gNnSc2	společenství
odpadlo	odpadnout	k5eAaPmAgNnS	odpadnout
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
kulturní	kulturní	k2eAgFnPc1d1	kulturní
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
všechna	všechen	k3xTgNnPc4	všechen
tři	tři	k4xCgNnPc4	tři
společenství	společenství	k1gNnPc4	společenství
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
vlámský	vlámský	k2eAgInSc4d1	vlámský
i	i	k8xC	i
valonský	valonský	k2eAgInSc4d1	valonský
region	region	k1gInSc4	region
zřídila	zřídit	k5eAaPmAgFnS	zřídit
své	svůj	k3xOyFgFnPc4	svůj
"	"	kIx"	"
<g/>
rady	rada	k1gFnPc1	rada
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
parlamenty	parlament	k1gInPc1	parlament
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
exekutivu	exekutiva	k1gFnSc4	exekutiva
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vlády	vláda	k1gFnSc2	vláda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
i	i	k8xC	i
vláda	vláda	k1gFnSc1	vláda
valonského	valonský	k2eAgNnSc2d1	valonské
společenství	společenství	k1gNnSc2	společenství
se	se	k3xPyFc4	se
však	však	k9	však
sloučila	sloučit	k5eAaPmAgFnS	sloučit
s	s	k7c7	s
radou	rada	k1gFnSc7	rada
a	a	k8xC	a
vládou	vláda	k1gFnSc7	vláda
valonského	valonský	k2eAgInSc2d1	valonský
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
zřídil	zřídit	k5eAaPmAgInS	zřídit
vlastní	vlastní	k2eAgInSc1d1	vlastní
parlament	parlament	k1gInSc4	parlament
a	a	k8xC	a
vládu	vláda	k1gFnSc4	vláda
i	i	k9	i
Region	region	k1gInSc4	region
Brusel	Brusel	k1gInSc1	Brusel
-	-	kIx~	-
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
byla	být	k5eAaImAgFnS	být
regionům	region	k1gInPc3	region
předána	předat	k5eAaPmNgFnS	předat
pravomoc	pravomoc	k1gFnSc1	pravomoc
nad	nad	k7c7	nad
veřejnými	veřejný	k2eAgFnPc7d1	veřejná
pracemi	práce	k1gFnPc7	práce
a	a	k8xC	a
dopravou	doprava	k1gFnSc7	doprava
a	a	k8xC	a
společenstvím	společenství	k1gNnSc7	společenství
působnost	působnost	k1gFnSc4	působnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
do	do	k7c2	do
ústavy	ústava	k1gFnSc2	ústava
dostala	dostat	k5eAaPmAgFnS	dostat
formulace	formulace	k1gFnSc1	formulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Belgie	Belgie	k1gFnSc1	Belgie
je	být	k5eAaImIp3nS	být
federální	federální	k2eAgInSc1d1	federální
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
společenství	společenství	k1gNnPc2	společenství
a	a	k8xC	a
regionů	region	k1gInPc2	region
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Belgie	Belgie	k1gFnSc1	Belgie
federací	federace	k1gFnPc2	federace
de	de	k?	de
iure	iurat	k5eAaPmIp3nS	iurat
<g/>
,	,	kIx,	,
a	a	k8xC	a
společenství	společenství	k1gNnPc1	společenství
a	a	k8xC	a
regiony	region	k1gInPc1	region
získaly	získat	k5eAaPmAgFnP	získat
další	další	k2eAgFnPc4d1	další
kompetence	kompetence	k1gFnPc4	kompetence
<g/>
.	.	kIx.	.
</s>
<s>
Pátou	pátý	k4xOgFnSc7	pátý
etapou	etapa	k1gFnSc7	etapa
federalizace	federalizace	k1gFnSc2	federalizace
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
Lambermontská	Lambermontský	k2eAgFnSc1d1	Lambermontský
a	a	k8xC	a
Lombardská	lombardský	k2eAgFnSc1d1	Lombardská
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
do	do	k7c2	do
kompetence	kompetence	k1gFnSc2	kompetence
regionů	region	k1gInPc2	region
přešlo	přejít	k5eAaPmAgNnS	přejít
i	i	k9	i
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
,	,	kIx,	,
mořský	mořský	k2eAgInSc1d1	mořský
rybolov	rybolov	k1gInSc1	rybolov
a	a	k8xC	a
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
obchod	obchod	k1gInSc1	obchod
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
do	do	k7c2	do
kompetence	kompetence	k1gFnSc2	kompetence
regionů	region	k1gInPc2	region
i	i	k8xC	i
společenství	společenství	k1gNnSc6	společenství
přešly	přejít	k5eAaPmAgFnP	přejít
otázky	otázka	k1gFnPc1	otázka
volebních	volební	k2eAgInPc2d1	volební
výdajů	výdaj	k1gInPc2	výdaj
<g/>
,	,	kIx,	,
financování	financování	k1gNnSc1	financování
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
regionální	regionální	k2eAgInSc4d1	regionální
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Lombardská	lombardský	k2eAgFnSc1d1	Lombardská
dohoda	dohoda	k1gFnSc1	dohoda
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
parlament	parlament	k1gInSc4	parlament
Bruselského	bruselský	k2eAgInSc2d1	bruselský
regionu	region	k1gInSc2	region
na	na	k7c4	na
frankofonní	frankofonní	k2eAgFnSc4d1	frankofonní
a	a	k8xC	a
vlámskou	vlámský	k2eAgFnSc4d1	vlámská
část	část	k1gFnSc4	část
se	s	k7c7	s
zákazem	zákaz	k1gInSc7	zákaz
majorizace	majorizace	k1gFnSc2	majorizace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
sílí	sílet	k5eAaImIp3nS	sílet
na	na	k7c6	na
vlámské	vlámský	k2eAgFnSc6d1	vlámská
straně	strana	k1gFnSc6	strana
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
úplné	úplný	k2eAgNnSc4d1	úplné
rozdělení	rozdělení	k1gNnSc4	rozdělení
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
Dewinter	Dewinter	k1gMnSc1	Dewinter
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
nacionalistické	nacionalistický	k2eAgFnSc2d1	nacionalistická
strany	strana	k1gFnSc2	strana
Vlámský	vlámský	k2eAgInSc4d1	vlámský
zájem	zájem	k1gInSc4	zájem
<g/>
,	,	kIx,	,
požaduje	požadovat	k5eAaImIp3nS	požadovat
"	"	kIx"	"
<g/>
sametový	sametový	k2eAgInSc1d1	sametový
rozvod	rozvod	k1gInSc1	rozvod
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
za	za	k7c4	za
jehož	jehož	k3xOyRp3gInSc4	jehož
vzor	vzor	k1gInSc4	vzor
považuje	považovat	k5eAaImIp3nS	považovat
rozdělení	rozdělení	k1gNnSc1	rozdělení
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
vlámští	vlámský	k2eAgMnPc1d1	vlámský
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
demokraté	demokrat	k1gMnPc1	demokrat
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
Yves	Yvesa	k1gFnPc2	Yvesa
Leterme	Leterme	k1gMnSc1	Leterme
<g/>
)	)	kIx)	)
podporují	podporovat	k5eAaImIp3nP	podporovat
další	další	k2eAgNnPc1d1	další
prohlubování	prohlubování	k1gNnPc1	prohlubování
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
překážkou	překážka	k1gFnSc7	překážka
rozdělení	rozdělení	k1gNnSc2	rozdělení
je	být	k5eAaImIp3nS	být
dvojjazyčné	dvojjazyčný	k2eAgNnSc1d1	dvojjazyčné
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Brusel	Brusel	k1gInSc1	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2006	[number]	k4	2006
belgická	belgický	k2eAgFnSc1d1	belgická
televize	televize	k1gFnSc1	televize
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
fiktivní	fiktivní	k2eAgNnSc4d1	fiktivní
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
vlámské	vlámský	k2eAgFnSc2d1	vlámská
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
žert	žert	k1gInSc4	žert
bylo	být	k5eAaImAgNnS	být
vedení	vedení	k1gNnSc1	vedení
televize	televize	k1gFnSc2	televize
kritizováno	kritizován	k2eAgNnSc1d1	kritizováno
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Geomorfologicky	geomorfologicky	k6eAd1	geomorfologicky
se	se	k3xPyFc4	se
Belgie	Belgie	k1gFnSc1	Belgie
dá	dát	k5eAaPmIp3nS	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c6	na
3	[number]	k4	3
oblasti	oblast	k1gFnSc6	oblast
<g/>
:	:	kIx,	:
pobřežní	pobřežní	k2eAgFnSc2d1	pobřežní
nížiny	nížina	k1gFnSc2	nížina
<g/>
,	,	kIx,	,
rovinu	rovina	k1gFnSc4	rovina
(	(	kIx(	(
<g/>
plató	plató	k1gNnSc2	plató
<g/>
)	)	kIx)	)
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
hornaté	hornatý	k2eAgFnPc4d1	hornatá
Ardenny	Ardenna	k1gFnPc4	Ardenna
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nížiny	nížina	k1gFnPc1	nížina
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
sestávají	sestávat	k5eAaImIp3nP	sestávat
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
písečných	písečný	k2eAgFnPc2d1	písečná
dun	duna	k1gFnPc2	duna
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
polderů	polder	k1gInPc2	polder
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1	centrální
rovina	rovina	k1gFnSc1	rovina
zabírá	zabírat	k5eAaImIp3nS	zabírat
většinu	většina	k1gFnSc4	většina
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jemně	jemně	k6eAd1	jemně
zvlněná	zvlněný	k2eAgFnSc1d1	zvlněná
oblast	oblast	k1gFnSc1	oblast
s	s	k7c7	s
úrodnou	úrodný	k2eAgFnSc7d1	úrodná
půdou	půda	k1gFnSc7	půda
<g/>
,	,	kIx,	,
protkaná	protkaný	k2eAgNnPc1d1	protkané
sítí	síť	k1gFnSc7	síť
mnoha	mnoho	k4c2	mnoho
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
–	–	k?	–
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
patří	patřit	k5eAaImIp3nS	patřit
Máza	Máza	k1gFnSc1	Máza
(	(	kIx(	(
<g/>
niz.	niz.	k?	niz.
Maas	Maas	k1gInSc1	Maas
<g/>
,	,	kIx,	,
fr.	fr.	k?	fr.
Meuse	Meuse	k1gFnSc1	Meuse
<g/>
)	)	kIx)	)
a	a	k8xC	a
Šelda	Šelda	k1gFnSc1	Šelda
(	(	kIx(	(
<g/>
niz.	niz.	k?	niz.
Schelde	Scheld	k1gMnSc5	Scheld
<g/>
,	,	kIx,	,
fr.	fr.	k?	fr.
Escaut	Escaut	k1gInSc1	Escaut
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
Ardenny	Ardenn	k1gInPc1	Ardenn
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hornatá	hornatý	k2eAgFnSc1d1	hornatá
a	a	k8xC	a
zalesněná	zalesněný	k2eAgFnSc1d1	zalesněná
<g/>
,	,	kIx,	,
s	s	k7c7	s
nepříliš	příliš	k6eNd1	příliš
úrodnou	úrodný	k2eAgFnSc7d1	úrodná
půdou	půda	k1gFnSc7	půda
a	a	k8xC	a
nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
pro	pro	k7c4	pro
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Signal	Signal	k1gFnSc1	Signal
de	de	k?	de
Botrange	Botrange	k1gInSc1	Botrange
<g/>
,	,	kIx,	,
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
694	[number]	k4	694
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
se	se	k3xPyFc4	se
Belgie	Belgie	k1gFnSc1	Belgie
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
10	[number]	k4	10
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Vlámsko	Vlámsko	k6eAd1	Vlámsko
na	na	k7c6	na
severu	sever	k1gInSc6	sever
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
(	(	kIx(	(
<g/>
od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
východu	východ	k1gInSc3	východ
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Západní	západní	k2eAgInPc1d1	západní
Flandry	Flandry	k1gInPc1	Flandry
Východní	východní	k2eAgInPc1d1	východní
Flandry	Flandry	k1gInPc1	Flandry
Vlámský	vlámský	k2eAgInSc1d1	vlámský
Brabant	Brabant	k1gInSc4	Brabant
Antverpy	Antverpy	k1gFnPc4	Antverpy
Limburk	Limburk	k1gInSc4	Limburk
Valonsko	Valonsko	k1gNnSc1	Valonsko
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
<g/>
:	:	kIx,	:
Henegavsko	Henegavsko	k1gNnSc1	Henegavsko
Valonský	valonský	k2eAgMnSc1d1	valonský
Brabant	Brabant	k1gMnSc1	Brabant
Namur	Namur	k1gMnSc1	Namur
Lutych	Lutych	k1gMnSc1	Lutych
Lucemburk	Lucemburk	k1gMnSc1	Lucemburk
Belgické	belgický	k2eAgNnSc4d1	Belgické
klima	klima	k1gNnSc4	klima
je	být	k5eAaImIp3nS	být
přímořské	přímořský	k2eAgNnSc1d1	přímořské
<g/>
,	,	kIx,	,
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
mírného	mírný	k2eAgInSc2d1	mírný
podnebného	podnebný	k2eAgInSc2d1	podnebný
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Srážky	srážka	k1gFnPc1	srážka
jsou	být	k5eAaImIp3nP	být
časté	častý	k2eAgMnPc4d1	častý
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
ročních	roční	k2eAgNnPc6d1	roční
obdobích	období	k1gNnPc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
18	[number]	k4	18
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrné	průměrný	k2eAgFnPc4d1	průměrná
srážky	srážka	k1gFnPc4	srážka
činí	činit	k5eAaImIp3nS	činit
65	[number]	k4	65
milimetrů	milimetr	k1gInPc2	milimetr
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
73	[number]	k4	73
milimetrů	milimetr	k1gInPc2	milimetr
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
hustotě	hustota	k1gFnSc3	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
(	(	kIx(	(
<g/>
jedné	jeden	k4xCgFnSc3	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
a	a	k8xC	a
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
umístění	umístění	k1gNnSc1	umístění
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
Belgie	Belgie	k1gFnSc1	Belgie
čelit	čelit	k5eAaImF	čelit
vážným	vážný	k2eAgInPc3d1	vážný
ekologickým	ekologický	k2eAgInPc3d1	ekologický
problémům	problém	k1gInPc3	problém
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
zpráva	zpráva	k1gFnSc1	zpráva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvalita	kvalita	k1gFnSc1	kvalita
říční	říční	k2eAgFnSc2d1	říční
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
je	být	k5eAaImIp3nS	být
nejhorší	zlý	k2eAgNnSc1d3	nejhorší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejhorších	zlý	k2eAgFnPc2d3	nejhorší
v	v	k7c6	v
122	[number]	k4	122
zkoumaných	zkoumaný	k2eAgFnPc2d1	zkoumaná
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
.	.	kIx.	.
10	[number]	k4	10
provincií	provincie	k1gFnPc2	provincie
tvoří	tvořit	k5eAaImIp3nP	tvořit
2	[number]	k4	2
regiony	region	k1gInPc4	region
a	a	k8xC	a
Brusel	Brusel	k1gInSc4	Brusel
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
regionem	region	k1gInSc7	region
<g/>
:	:	kIx,	:
Brusel	Brusel	k1gInSc1	Brusel
(	(	kIx(	(
<g/>
1	[number]	k4	1
018	[number]	k4	018
029	[number]	k4	029
)	)	kIx)	)
Antverpy	Antverpy	k1gFnPc1	Antverpy
(	(	kIx(	(
<g/>
448	[number]	k4	448
709	[number]	k4	709
<g/>
)	)	kIx)	)
Gent	Genta	k1gFnPc2	Genta
(	(	kIx(	(
<g/>
226	[number]	k4	226
220	[number]	k4	220
<g/>
)	)	kIx)	)
Charleroi	Charleroi	k1gNnSc6	Charleroi
(	(	kIx(	(
<g/>
200	[number]	k4	200
578	[number]	k4	578
<g/>
)	)	kIx)	)
Lutych	Lutycha	k1gFnPc2	Lutycha
(	(	kIx(	(
<g/>
195	[number]	k4	195
576	[number]	k4	576
<g/>
)	)	kIx)	)
Bruggy	Bruggy	k1gFnPc1	Bruggy
(	(	kIx(	(
<g/>
117	[number]	k4	117
351	[number]	k4	351
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Belgie	Belgie	k1gFnSc1	Belgie
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
stát	stát	k1gInSc1	stát
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
koncentrací	koncentrace	k1gFnSc7	koncentrace
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
intenzivním	intenzivní	k2eAgNnSc7d1	intenzivní
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
odvětví	odvětví	k1gNnPc1	odvětví
jsou	být	k5eAaImIp3nP	být
hutnictví	hutnictví	k1gNnSc1	hutnictví
<g/>
,	,	kIx,	,
strojírenství	strojírenství	k1gNnSc2	strojírenství
<g/>
,	,	kIx,	,
chemie	chemie	k1gFnSc2	chemie
a	a	k8xC	a
textil	textil	k1gInSc1	textil
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
průmyslovými	průmyslový	k2eAgFnPc7d1	průmyslová
oblastmi	oblast	k1gFnPc7	oblast
jsou	být	k5eAaImIp3nP	být
Brusel	Brusel	k1gInSc4	Brusel
<g/>
,	,	kIx,	,
Antverpy	Antverpy	k1gFnPc4	Antverpy
<g/>
,	,	kIx,	,
Lutych	Lutych	k1gInSc4	Lutych
a	a	k8xC	a
Gent	Gent	k1gInSc4	Gent
<g/>
.	.	kIx.	.
</s>
<s>
Těžba	těžba	k1gFnSc1	těžba
uhlí	uhlí	k1gNnSc2	uhlí
se	se	k3xPyFc4	se
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
zastavila	zastavit	k5eAaPmAgFnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
produktivní	produktivní	k2eAgFnSc1d1	produktivní
<g/>
,	,	kIx,	,
živočišná	živočišný	k2eAgFnSc1d1	živočišná
produkce	produkce	k1gFnSc1	produkce
převažuje	převažovat	k5eAaImIp3nS	převažovat
nad	nad	k7c7	nad
rostlinnou	rostlinný	k2eAgFnSc7d1	rostlinná
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
půdy	půda	k1gFnSc2	půda
<g/>
:	:	kIx,	:
orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
27	[number]	k4	27
%	%	kIx~	%
<g/>
,	,	kIx,	,
louky	louka	k1gFnPc1	louka
a	a	k8xC	a
pastviny	pastvina	k1gFnPc1	pastvina
20	[number]	k4	20
%	%	kIx~	%
<g/>
,	,	kIx,	,
lesy	les	k1gInPc1	les
21	[number]	k4	21
%	%	kIx~	%
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
6	[number]	k4	6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Důležitý	důležitý	k2eAgInSc1d1	důležitý
je	být	k5eAaImIp3nS	být
chov	chov	k1gInSc1	chov
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
brambory	brambor	k1gInPc1	brambor
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
<g/>
,	,	kIx,	,
chmel	chmel	k1gInSc1	chmel
a	a	k8xC	a
jablka	jablko	k1gNnPc1	jablko
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgFnPc1d1	typická
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnPc1d2	menší
farmy	farma	k1gFnPc1	farma
s	s	k7c7	s
vysokými	vysoký	k2eAgInPc7d1	vysoký
hektarovými	hektarový	k2eAgInPc7d1	hektarový
výnosy	výnos	k1gInPc7	výnos
<g/>
.	.	kIx.	.
</s>
<s>
Dopravně	dopravně	k6eAd1	dopravně
nejvýznamnější	významný	k2eAgInPc1d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
přístavy	přístav	k1gInPc1	přístav
Antverpy	Antverpy	k1gFnPc1	Antverpy
a	a	k8xC	a
Oostende	Oostend	k1gInSc5	Oostend
se	se	k3xPyFc4	se
spojením	spojení	k1gNnSc7	spojení
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
ropný	ropný	k2eAgInSc4d1	ropný
terminál	terminál	k1gInSc4	terminál
Zeebrügge	Zeebrügg	k1gFnSc2	Zeebrügg
<g/>
.	.	kIx.	.
</s>
<s>
Nejatraktivnějšími	atraktivní	k2eAgInPc7d3	nejatraktivnější
turistickými	turistický	k2eAgNnPc7d1	turistické
středisky	středisko	k1gNnPc7	středisko
v	v	k7c6	v
oblastmi	oblast	k1gFnPc7	oblast
jsou	být	k5eAaImIp3nP	být
Brusel	Brusel	k1gInSc4	Brusel
<g/>
,	,	kIx,	,
Antverpy	Antverpy	k1gFnPc4	Antverpy
<g/>
,	,	kIx,	,
Bruggy	Bruggy	k1gFnPc4	Bruggy
<g/>
,	,	kIx,	,
Oostende	Oostend	k1gMnSc5	Oostend
<g/>
,	,	kIx,	,
Waterloo	Waterloo	k1gNnSc2	Waterloo
<g/>
,	,	kIx,	,
Spa	Spa	k1gFnSc1	Spa
a	a	k8xC	a
vrchovina	vrchovina	k1gFnSc1	vrchovina
Ardeny	Ardeny	k1gFnPc5	Ardeny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
síť	síť	k1gFnSc1	síť
železnic	železnice	k1gFnPc2	železnice
<g/>
,	,	kIx,	,
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
,	,	kIx,	,
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
vodních	vodní	k2eAgFnPc2d1	vodní
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
<g/>
,	,	kIx,	,
Gentu	Gento	k1gNnSc6	Gento
a	a	k8xC	a
Zeebrügge	Zeebrügge	k1gNnSc6	Zeebrügge
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jedny	jeden	k4xCgInPc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
evropských	evropský	k2eAgInPc2d1	evropský
přístavů	přístav	k1gInPc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
bývala	bývat	k5eAaImAgFnS	bývat
velmi	velmi	k6eAd1	velmi
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
síť	síť	k1gFnSc1	síť
tzv.	tzv.	kA	tzv.
vicinálních	vicinální	k2eAgFnPc2d1	vicinální
drah	draha	k1gFnPc2	draha
–	–	k?	–
kombinace	kombinace	k1gFnSc2	kombinace
železnice	železnice	k1gFnSc2	železnice
a	a	k8xC	a
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
tratí	trať	k1gFnPc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
stále	stále	k6eAd1	stále
patrné	patrný	k2eAgInPc1d1	patrný
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
městských	městský	k2eAgFnPc6d1	městská
a	a	k8xC	a
příměstských	příměstský	k2eAgFnPc6d1	příměstská
tramvajových	tramvajový	k2eAgFnPc6d1	tramvajová
tratích	trať	k1gFnPc6	trať
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
nachází	nacházet	k5eAaImIp3nS	nacházet
metro	metro	k1gNnSc1	metro
a	a	k8xC	a
významné	významný	k2eAgNnSc1d1	významné
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Belgie	Belgie	k1gFnSc1	Belgie
má	mít	k5eAaImIp3nS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
hustotu	hustota	k1gFnSc4	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
(	(	kIx(	(
<g/>
365	[number]	k4	365
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
na	na	k7c4	na
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
má	mít	k5eAaImIp3nS	mít
vyšší	vysoký	k2eAgFnSc1d2	vyšší
pouze	pouze	k6eAd1	pouze
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
a	a	k8xC	a
několik	několik	k4yIc1	několik
malých	malý	k2eAgInPc2d1	malý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Monako	Monako	k1gNnSc1	Monako
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
asi	asi	k9	asi
85	[number]	k4	85
<g/>
%	%	kIx~	%
národa	národ	k1gInSc2	národ
je	být	k5eAaImIp3nS	být
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
hustotu	hustota	k1gFnSc4	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
má	mít	k5eAaImIp3nS	mít
oblast	oblast	k1gFnSc1	oblast
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
vlámský	vlámský	k2eAgInSc1d1	vlámský
diamant	diamant	k1gInSc1	diamant
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vymezují	vymezovat	k5eAaImIp3nP	vymezovat
aglomerace	aglomerace	k1gFnSc1	aglomerace
Bruselu	Brusel	k1gInSc2	Brusel
<g/>
,	,	kIx,	,
Antverp	Antverpy	k1gFnPc2	Antverpy
<g/>
,	,	kIx,	,
Gentu	Gent	k1gInSc2	Gent
a	a	k8xC	a
Lovaně	Lovaň	k1gFnSc2	Lovaň
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
velká	velký	k2eAgNnPc1d1	velké
města	město	k1gNnPc1	město
jsou	být	k5eAaImIp3nP	být
Lutych	Lutych	k1gInSc4	Lutych
<g/>
,	,	kIx,	,
Charleroi	Charleroe	k1gFnSc4	Charleroe
<g/>
,	,	kIx,	,
Mons	Mons	k1gInSc1	Mons
<g/>
,	,	kIx,	,
Kortrijk	Kortrijk	k1gInSc1	Kortrijk
<g/>
,	,	kIx,	,
Bruggy	Bruggy	k1gFnPc1	Bruggy
<g/>
,	,	kIx,	,
Hasselt	Hasselt	k1gMnSc1	Hasselt
a	a	k8xC	a
Namur	Namur	k1gMnSc1	Namur
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnSc4d3	nejnižší
hustotu	hustota	k1gFnSc4	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
mají	mít	k5eAaImIp3nP	mít
Ardenny	Ardenen	k2eAgFnPc1d1	Ardenen
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
10	[number]	k4	10
511	[number]	k4	511
382	[number]	k4	382
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
6	[number]	k4	6
078	[number]	k4	078
600	[number]	k4	600
ve	v	k7c6	v
Vlámsku	Vlámsek	k1gInSc6	Vlámsek
<g/>
,	,	kIx,	,
3	[number]	k4	3
413	[number]	k4	413
978	[number]	k4	978
ve	v	k7c6	v
Valonsku	Valonsko	k1gNnSc6	Valonsko
a	a	k8xC	a
1	[number]	k4	1
018	[number]	k4	018
804	[number]	k4	804
v	v	k7c6	v
Bruselském	bruselský	k2eAgInSc6d1	bruselský
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Belgie	Belgie	k1gFnSc1	Belgie
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc4d1	vysoká
míru	míra	k1gFnSc4	míra
urbanizace	urbanizace	k1gFnSc2	urbanizace
–	–	k?	–
ve	v	k7c6	v
městech	město	k1gNnPc6	město
žije	žít	k5eAaImIp3nS	žít
97,2	[number]	k4	97,2
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
98	[number]	k4	98
%	%	kIx~	%
dospělé	dospělý	k2eAgFnSc2d1	dospělá
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
gramotných	gramotný	k2eAgFnPc2d1	gramotná
<g/>
.	.	kIx.	.
</s>
<s>
Školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
je	být	k5eAaImIp3nS	být
povinná	povinný	k2eAgFnSc1d1	povinná
od	od	k7c2	od
šesti	šest	k4xCc2	šest
do	do	k7c2	do
osmnácti	osmnáct	k4xCc2	osmnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnoho	mnoho	k4c1	mnoho
Belgičanů	Belgičan	k1gMnPc2	Belgičan
studuje	studovat	k5eAaImIp3nS	studovat
až	až	k6eAd1	až
do	do	k7c2	do
23	[number]	k4	23
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zemí	zem	k1gFnPc2	zem
OECD	OECD	kA	OECD
má	mít	k5eAaImIp3nS	mít
Belgie	Belgie	k1gFnSc1	Belgie
3	[number]	k4	3
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
procento	procento	k1gNnSc1	procento
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
21	[number]	k4	21
let	léto	k1gNnPc2	léto
zapsaných	zapsaný	k2eAgMnPc2d1	zapsaný
na	na	k7c4	na
vyšší	vysoký	k2eAgFnSc4d2	vyšší
školu	škola	k1gFnSc4	škola
nebo	nebo	k8xC	nebo
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
funkční	funkční	k2eAgFnSc7d1	funkční
negramotností	negramotnost	k1gFnSc7	negramotnost
je	být	k5eAaImIp3nS	být
však	však	k9	však
znepokojivý	znepokojivý	k2eAgInSc1d1	znepokojivý
–	–	k?	–
v	v	k7c6	v
letech	let	k1gInPc6	let
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
činil	činit	k5eAaImAgInS	činit
18,4	[number]	k4	18,4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Belgické	belgický	k2eAgInPc1d1	belgický
páry	pár	k1gInPc1	pár
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
největší	veliký	k2eAgFnSc4d3	veliký
rozvodovost	rozvodovost	k1gFnSc4	rozvodovost
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
-	-	kIx~	-
neobstojí	obstát	k5eNaPmIp3nS	obstát
zhruba	zhruba	k6eAd1	zhruba
64	[number]	k4	64
ze	z	k7c2	z
100	[number]	k4	100
manželství	manželství	k1gNnPc2	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
belgické	belgický	k2eAgFnSc2d1	belgická
populace	populace	k1gFnSc2	populace
tvoří	tvořit	k5eAaImIp3nP	tvořit
Vlámové	Vlám	k1gMnPc1	Vlám
(	(	kIx(	(
<g/>
58	[number]	k4	58
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Valoni	Valon	k1gMnPc1	Valon
(	(	kIx(	(
<g/>
31	[number]	k4	31
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbývajících	zbývající	k2eAgNnPc2d1	zbývající
11	[number]	k4	11
%	%	kIx~	%
představují	představovat	k5eAaImIp3nP	představovat
příslušníci	příslušník	k1gMnPc1	příslušník
dalších	další	k2eAgInPc2d1	další
evropských	evropský	k2eAgInPc2d1	evropský
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
Italové	Ital	k1gMnPc1	Ital
<g/>
,	,	kIx,	,
Francouzi	Francouz	k1gMnPc1	Francouz
a	a	k8xC	a
Němci	Němec	k1gMnPc1	Němec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
z	z	k7c2	z
Maroka	Maroko	k1gNnSc2	Maroko
a	a	k8xC	a
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jazykové	jazykový	k2eAgFnSc6d1	jazyková
stránce	stránka	k1gFnSc6	stránka
je	být	k5eAaImIp3nS	být
Belgie	Belgie	k1gFnSc1	Belgie
nejednotná	jednotný	k2eNgFnSc1d1	nejednotná
<g/>
.	.	kIx.	.
</s>
<s>
Nizozemsky	nizozemsky	k6eAd1	nizozemsky
mluví	mluvit	k5eAaImIp3nS	mluvit
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
40	[number]	k4	40
%	%	kIx~	%
a	a	k8xC	a
německy	německy	k6eAd1	německy
necelé	celý	k2eNgInPc4d1	necelý
1	[number]	k4	1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Brusel	Brusel	k1gInSc1	Brusel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
žije	žít	k5eAaImIp3nS	žít
8	[number]	k4	8
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
oficiálně	oficiálně	k6eAd1	oficiálně
bilingvní	bilingvní	k2eAgInSc1d1	bilingvní
(	(	kIx(	(
<g/>
francouzsko-nizozemský	francouzskoizozemský	k2eAgInSc1d1	francouzsko-nizozemský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
mluvilo	mluvit	k5eAaImAgNnS	mluvit
převážně	převážně	k6eAd1	převážně
nizozemsky	nizozemsky	k6eAd1	nizozemsky
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Belgie	Belgie	k1gFnSc1	Belgie
získala	získat	k5eAaPmAgFnS	získat
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
převládla	převládnout	k5eAaPmAgFnS	převládnout
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
jediným	jediný	k2eAgInSc7d1	jediný
oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
vlámština	vlámština	k1gFnSc1	vlámština
(	(	kIx(	(
<g/>
belgická	belgický	k2eAgFnSc1d1	belgická
varianta	varianta	k1gFnSc1	varianta
nizozemštiny	nizozemština	k1gFnSc2	nizozemština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
belgická	belgický	k2eAgFnSc1d1	belgická
francouzština	francouzština	k1gFnSc1	francouzština
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
drobné	drobný	k2eAgInPc1d1	drobný
rozdíly	rozdíl	k1gInPc1	rozdíl
ve	v	k7c6	v
slovní	slovní	k2eAgFnSc6d1	slovní
zásobě	zásoba	k1gFnSc6	zásoba
a	a	k8xC	a
sémantice	sémantika	k1gFnSc3	sémantika
oproti	oproti	k7c3	oproti
variantám	varianta	k1gFnPc3	varianta
užívaným	užívaný	k2eAgFnPc3d1	užívaná
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
a	a	k8xC	a
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Francouzsky	francouzsky	k6eAd1	francouzsky
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
vlámsky	vlámsky	k6eAd1	vlámsky
zase	zase	k9	zase
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Němčina	němčina	k1gFnSc1	němčina
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
na	na	k7c6	na
malé	malý	k2eAgFnSc6d1	malá
části	část	k1gFnSc6	část
území	území	k1gNnSc2	území
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgInSc1d1	kulturní
život	život	k1gInSc1	život
Belgie	Belgie	k1gFnSc2	Belgie
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
hlavně	hlavně	k9	hlavně
uvnitř	uvnitř	k7c2	uvnitř
všech	všecek	k3xTgFnPc2	všecek
tří	tři	k4xCgFnPc2	tři
národnostních	národnostní	k2eAgFnPc2d1	národnostní
společenství	společenství	k1gNnPc2	společenství
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Královské	královský	k2eAgFnSc2d1	královská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
akademie	akademie	k1gFnSc2	akademie
totiž	totiž	k9	totiž
nemá	mít	k5eNaImIp3nS	mít
Belgie	Belgie	k1gFnSc1	Belgie
žádné	žádný	k3yNgFnSc2	žádný
dvojjazyčné	dvojjazyčný	k2eAgFnSc2d1	dvojjazyčná
univerzity	univerzita	k1gFnSc2	univerzita
ani	ani	k8xC	ani
jednotná	jednotný	k2eAgNnPc1d1	jednotné
média	médium	k1gNnPc1	médium
<g/>
,	,	kIx,	,
společné	společný	k2eAgInPc1d1	společný
kulturní	kulturní	k2eAgFnSc4d1	kulturní
<g/>
,	,	kIx,	,
či	či	k8xC	či
vědecké	vědecký	k2eAgFnPc1d1	vědecká
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatel	spisovatel	k1gMnSc1	spisovatel
Maurice	Maurika	k1gFnSc6	Maurika
Maeterlinck	Maeterlinck	k1gMnSc1	Maeterlinck
získal	získat	k5eAaPmAgMnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
de	de	k?	de
Coster	Coster	k1gMnSc1	Coster
obnovil	obnovit	k5eAaPmAgMnS	obnovit
slávu	sláva	k1gFnSc4	sláva
postavičky	postavička	k1gFnSc2	postavička
Enšpígla	Enšpígl	k1gMnSc2	Enšpígl
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníkem	průkopník	k1gMnSc7	průkopník
vlámštiny	vlámština	k1gFnSc2	vlámština
v	v	k7c6	v
druhdy	druhdy	k6eAd1	druhdy
převážně	převážně	k6eAd1	převážně
frankofonní	frankofonní	k2eAgFnSc6d1	frankofonní
Belgii	Belgie	k1gFnSc6	Belgie
byl	být	k5eAaImAgMnS	být
Hendrik	Hendrik	k1gMnSc1	Hendrik
Conscience	Conscienec	k1gInSc2	Conscienec
<g/>
.	.	kIx.	.
</s>
<s>
Guido	Guido	k1gNnSc1	Guido
Gezelle	Gezell	k1gMnSc2	Gezell
psal	psát	k5eAaImAgInS	psát
západním	západní	k2eAgInSc7d1	západní
vlámským	vlámský	k2eAgInSc7d1	vlámský
dialektem	dialekt	k1gInSc7	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Claus	Claus	k1gMnSc1	Claus
proslul	proslout	k5eAaPmAgMnS	proslout
vlámsky	vlámsky	k6eAd1	vlámsky
psaným	psaný	k2eAgInSc7d1	psaný
románem	román	k1gInSc7	román
Smutek	smutek	k1gInSc1	smutek
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Francouzsky	francouzsky	k6eAd1	francouzsky
psali	psát	k5eAaImAgMnP	psát
symbolistický	symbolistický	k2eAgMnSc1d1	symbolistický
básník	básník	k1gMnSc1	básník
Emile	Emil	k1gMnSc5	Emil
Verhaeren	Verhaerna	k1gFnPc2	Verhaerna
<g/>
,	,	kIx,	,
Marguerite	Marguerit	k1gInSc5	Marguerit
Yourcenar	Yourcenar	k1gInSc1	Yourcenar
či	či	k8xC	či
Henri	Henri	k1gNnSc1	Henri
Michaux	Michaux	k1gInSc1	Michaux
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
třeba	třeba	k6eAd1	třeba
Jan	Jan	k1gMnSc1	Jan
Theuninck	Theuninck	k1gMnSc1	Theuninck
či	či	k8xC	či
Amélie	Amélie	k1gFnSc1	Amélie
Nothomb	Nothomba	k1gFnPc2	Nothomba
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
neslavnějším	slavný	k2eNgFnPc3d2	neslavnější
detektivkářů	detektivkář	k1gMnPc2	detektivkář
světa	svět	k1gInSc2	svět
byl	být	k5eAaImAgMnS	být
Georges	Georges	k1gMnSc1	Georges
Simenon	Simenon	k1gMnSc1	Simenon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
stvořil	stvořit	k5eAaPmAgInS	stvořit
postavu	postava	k1gFnSc4	postava
komisaře	komisař	k1gMnSc2	komisař
Maigreta	Maigret	k1gMnSc2	Maigret
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
tradici	tradice	k1gFnSc4	tradice
má	mít	k5eAaImIp3nS	mít
belgický	belgický	k2eAgInSc1d1	belgický
komiks	komiks	k1gInSc1	komiks
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrcem	tvůrce	k1gMnSc7	tvůrce
slavného	slavný	k2eAgMnSc2d1	slavný
Tintina	Tintin	k1gMnSc2	Tintin
byl	být	k5eAaImAgMnS	být
Hergé	Hergé	k1gNnSc4	Hergé
<g/>
,	,	kIx,	,
dalšími	další	k2eAgMnPc7d1	další
významnými	významný	k2eAgMnPc7d1	významný
komiksovými	komiksový	k2eAgMnPc7d1	komiksový
autory	autor	k1gMnPc7	autor
byli	být	k5eAaImAgMnP	být
André	André	k1gMnSc1	André
Franquin	Franquin	k1gMnSc1	Franquin
(	(	kIx(	(
<g/>
tvůrce	tvůrce	k1gMnSc1	tvůrce
Gastona	Gaston	k1gMnSc2	Gaston
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Morris	Morris	k1gFnSc1	Morris
(	(	kIx(	(
<g/>
tvůrce	tvůrce	k1gMnSc1	tvůrce
Lucky	lucky	k6eAd1	lucky
Luka	luka	k1gNnPc1	luka
<g/>
)	)	kIx)	)
či	či	k8xC	či
Peyo	Peyo	k1gMnSc1	Peyo
(	(	kIx(	(
<g/>
tvůrce	tvůrce	k1gMnSc1	tvůrce
Šmoulů	šmoula	k1gMnPc2	šmoula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
hudbě	hudba	k1gFnSc6	hudba
vynikl	vyniknout	k5eAaPmAgInS	vyniknout
především	především	k6eAd1	především
renesanční	renesanční	k2eAgMnSc1d1	renesanční
skladatel	skladatel	k1gMnSc1	skladatel
Jacob	Jacoba	k1gFnPc2	Jacoba
Obrecht	Obrecht	k1gMnSc1	Obrecht
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
interpretů	interpret	k1gMnPc2	interpret
houslisté	houslista	k1gMnPc1	houslista
Eugè	Eugè	k1gFnSc3	Eugè
Ysaÿ	Ysaÿ	k1gFnSc4	Ysaÿ
a	a	k8xC	a
Henri	Henre	k1gFnSc4	Henre
Vieuxtemps	Vieuxtempsa	k1gFnPc2	Vieuxtempsa
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgInSc1d1	francouzský
šanson	šanson	k1gInSc1	šanson
obohatil	obohatit	k5eAaPmAgMnS	obohatit
Jacques	Jacques	k1gMnSc1	Jacques
Brel	Brel	k1gMnSc1	Brel
<g/>
.	.	kIx.	.
</s>
<s>
César	César	k1gMnSc1	César
Franck	Franck	k1gMnSc1	Franck
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Lutychu	Lutych	k1gInSc6	Lutych
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinu	většina	k1gFnSc4	většina
života	život	k1gInSc2	život
prožil	prožít	k5eAaPmAgInS	prožít
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
varhaník	varhaník	k1gMnSc1	varhaník
a	a	k8xC	a
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
skladbu	skladba	k1gFnSc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
skladatel	skladatel	k1gMnSc1	skladatel
se	se	k3xPyFc4	se
však	však	k9	však
prosadil	prosadit	k5eAaPmAgMnS	prosadit
až	až	k9	až
v	v	k7c6	v
pozdějším	pozdní	k2eAgInSc6d2	pozdější
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
svou	svůj	k3xOyFgFnSc7	svůj
Symfonií	symfonie	k1gFnSc7	symfonie
d	d	k?	d
moll	moll	k1gNnPc7	moll
a	a	k8xC	a
Sonátou	sonáta	k1gFnSc7	sonáta
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
A	a	k8xC	a
dur	dur	k1gNnSc4	dur
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pop-music	popusic	k1gFnSc6	pop-music
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
věhlasu	věhlas	k1gInSc2	věhlas
dočkal	dočkat	k5eAaPmAgInS	dočkat
francouzsky	francouzsky	k6eAd1	francouzsky
zpívající	zpívající	k2eAgInSc1d1	zpívající
Salvatore	Salvator	k1gMnSc5	Salvator
Adamo	Adama	k1gFnSc5	Adama
<g/>
.	.	kIx.	.
</s>
<s>
Belgičané	Belgičan	k1gMnPc1	Belgičan
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
poměrně	poměrně	k6eAd1	poměrně
hodně	hodně	k6eAd1	hodně
prosazovat	prosazovat	k5eAaImF	prosazovat
v	v	k7c6	v
globálním	globální	k2eAgInSc6d1	globální
popu	pop	k1gInSc6	pop
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětové	celosvětový	k2eAgInPc4d1	celosvětový
hity	hit	k1gInPc4	hit
měli	mít	k5eAaImAgMnP	mít
raper	raper	k1gInSc4	raper
s	s	k7c7	s
rwandskými	rwandský	k2eAgInPc7d1	rwandský
kořeny	kořen	k1gInPc7	kořen
Stromae	Stromae	k1gNnSc1	Stromae
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Alors	Alors	k1gInSc1	Alors
en	en	k?	en
dance	dance	k1gFnSc2	dance
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kate	kat	k1gInSc5	kat
Ryan	Ryan	k1gMnSc1	Ryan
<g/>
,	,	kIx,	,
Lara	Lara	k1gMnSc1	Lara
Fabian	Fabian	k1gMnSc1	Fabian
či	či	k8xC	či
Gotye	Gotye	k1gFnSc1	Gotye
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Transglobal	Transglobal	k1gMnSc2	Transglobal
Underground	underground	k1gInSc4	underground
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Natacha	Natacha	k1gMnSc1	Natacha
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgInSc7d3	nejslavnější
belgickými	belgický	k2eAgMnPc7d1	belgický
filmovými	filmový	k2eAgMnPc7d1	filmový
tvůrci	tvůrce	k1gMnPc7	tvůrce
jsou	být	k5eAaImIp3nP	být
bratři	bratr	k1gMnPc1	bratr
Dardennové	Dardennové	k2eAgMnPc1d1	Dardennové
<g/>
,	,	kIx,	,
již	jenž	k3xRgMnPc1	jenž
prosluli	proslout	k5eAaPmAgMnP	proslout
sociálními	sociální	k2eAgNnPc7d1	sociální
dramaty	drama	k1gNnPc7	drama
<g/>
.	.	kIx.	.
</s>
<s>
Režisérka	režisérka	k1gFnSc1	režisérka
Chantal	Chantal	k1gMnSc1	Chantal
Akermanová	Akermanová	k1gFnSc1	Akermanová
byla	být	k5eAaImAgFnS	být
představitelkou	představitelka	k1gFnSc7	představitelka
feministického	feministický	k2eAgInSc2d1	feministický
avantgardního	avantgardní	k2eAgInSc2d1	avantgardní
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Jean-Claude	Jean-Claude	k6eAd1	Jean-Claude
van	van	k1gInSc1	van
Damme	Damm	k1gInSc5	Damm
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hvězdou	hvězda	k1gFnSc7	hvězda
akčních	akční	k2eAgInPc2d1	akční
snímků	snímek	k1gInPc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
(	(	kIx(	(
<g/>
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
holandské	holandský	k2eAgFnSc2d1	holandská
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
)	)	kIx)	)
i	i	k9	i
hollywoodská	hollywoodský	k2eAgFnSc1d1	hollywoodská
hvězda	hvězda	k1gFnSc1	hvězda
Audrey	Audrea	k1gFnSc2	Audrea
Hepburnová	Hepburnová	k1gFnSc1	Hepburnová
<g/>
.	.	kIx.	.
</s>
<s>
Belgie	Belgie	k1gFnSc1	Belgie
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
vyspělým	vyspělý	k2eAgNnSc7d1	vyspělé
výtvarným	výtvarný	k2eAgNnSc7d1	výtvarné
uměním	umění	k1gNnSc7	umění
a	a	k8xC	a
architekturou	architektura	k1gFnSc7	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
vyvíjely	vyvíjet	k5eAaImAgFnP	vyvíjet
všechny	všechen	k3xTgFnPc1	všechen
významné	významný	k2eAgFnPc1d1	významná
evropské	evropský	k2eAgFnPc1d1	Evropská
umělecké	umělecký	k2eAgFnPc1d1	umělecká
slohy	sloha	k1gFnPc1	sloha
<g/>
,	,	kIx,	,
od	od	k7c2	od
románských	románský	k2eAgFnPc2d1	románská
<g/>
,	,	kIx,	,
gotických	gotický	k2eAgFnPc2d1	gotická
<g/>
,	,	kIx,	,
barokních	barokní	k2eAgFnPc2d1	barokní
a	a	k8xC	a
renesančních	renesanční	k2eAgFnPc2d1	renesanční
architektur	architektura	k1gFnPc2	architektura
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
vlámské	vlámský	k2eAgFnPc4d1	vlámská
renesanční	renesanční	k2eAgFnPc4d1	renesanční
a	a	k8xC	a
barokní	barokní	k2eAgFnPc4d1	barokní
stavby	stavba	k1gFnPc4	stavba
a	a	k8xC	a
obrazy	obraz	k1gInPc4	obraz
<g/>
,	,	kIx,	,
až	až	k9	až
k	k	k7c3	k
renesanční	renesanční	k2eAgFnSc3d1	renesanční
vokální	vokální	k2eAgFnSc3d1	vokální
hudbě	hudba	k1gFnSc3	hudba
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
nížin	nížina	k1gFnPc2	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
osobností	osobnost	k1gFnPc2	osobnost
tohoto	tento	k3xDgInSc2	tento
regionu	region	k1gInSc2	region
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
světové	světový	k2eAgNnSc4d1	světové
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
vědu	věda	k1gFnSc4	věda
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc4	ten
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Pieter	Pieter	k1gMnSc1	Pieter
Brueghel	Brueghel	k1gMnSc1	Brueghel
starší	starší	k1gMnSc1	starší
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
malíř	malíř	k1gMnSc1	malíř
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
malířské	malířský	k2eAgFnSc2d1	malířská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
působící	působící	k2eAgMnSc1d1	působící
významný	významný	k2eAgMnSc1d1	významný
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
)	)	kIx)	)
Peter	Peter	k1gMnSc1	Peter
Paul	Paul	k1gMnSc1	Paul
Rubens	Rubens	k1gInSc4	Rubens
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
významnými	významný	k2eAgMnPc7d1	významný
malíři	malíř	k1gMnPc7	malíř
různých	různý	k2eAgFnPc2d1	různá
epoch	epocha	k1gFnPc2	epocha
na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
belgickém	belgický	k2eAgNnSc6d1	Belgické
území	území	k1gNnSc6	území
byli	být	k5eAaImAgMnP	být
mj.	mj.	kA	mj.
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
vlámští	vlámský	k2eAgMnPc1d1	vlámský
primitivisté	primitivista	k1gMnPc1	primitivista
<g/>
"	"	kIx"	"
Jan	Jan	k1gMnSc1	Jan
van	vana	k1gFnPc2	vana
Eyck	Eyck	k1gMnSc1	Eyck
a	a	k8xC	a
Hans	Hans	k1gMnSc1	Hans
Memling	Memling	k1gInSc4	Memling
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Quinten	Quinten	k2eAgInSc1d1	Quinten
Matsijs	Matsijs	k1gInSc1	Matsijs
<g/>
,	,	kIx,	,
Jacob	Jacoba	k1gFnPc2	Jacoba
Jordaens	Jordaensa	k1gFnPc2	Jordaensa
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
Anthony	Anthon	k1gInPc4	Anthon
van	vana	k1gFnPc2	vana
Dyck	Dycka	k1gFnPc2	Dycka
<g/>
.	.	kIx.	.
</s>
<s>
Představitelem	představitel	k1gMnSc7	představitel
secese	secese	k1gFnSc2	secese
byl	být	k5eAaImAgMnS	být
malíř	malíř	k1gMnSc1	malíř
Henry	Henry	k1gMnSc1	Henry
van	vana	k1gFnPc2	vana
de	de	k?	de
Velde	Veld	k1gInSc5	Veld
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
zmínit	zmínit	k5eAaPmF	zmínit
jména	jméno	k1gNnPc4	jméno
Constant	Constant	k1gMnSc1	Constant
Permeke	Permek	k1gFnPc4	Permek
<g/>
,	,	kIx,	,
René	René	k1gMnSc5	René
Magritte	Magritt	k1gMnSc5	Magritt
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Delvaux	Delvaux	k1gInSc1	Delvaux
a	a	k8xC	a
James	James	k1gMnSc1	James
Ensor	Ensor	k1gMnSc1	Ensor
<g/>
.	.	kIx.	.
</s>
<s>
Symbolistou	symbolista	k1gMnSc7	symbolista
byl	být	k5eAaImAgMnS	být
litograf	litograf	k1gMnSc1	litograf
Félicien	Félicina	k1gFnPc2	Félicina
Rops	Ropsa	k1gFnPc2	Ropsa
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tachismu	tachismus	k1gInSc3	tachismus
a	a	k8xC	a
abstraktnímu	abstraktní	k2eAgInSc3d1	abstraktní
expresionismu	expresionismus	k1gInSc3	expresionismus
náležel	náležet	k5eAaImAgMnS	náležet
Pierre	Pierr	k1gMnSc5	Pierr
Alechinsky	Alechinsky	k1gMnSc5	Alechinsky
<g/>
.	.	kIx.	.
</s>
<s>
Victor	Victor	k1gMnSc1	Victor
Horta	Hort	k1gMnSc4	Hort
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgMnPc2d3	nejdůležitější
představitelů	představitel	k1gMnPc2	představitel
secese	secese	k1gFnSc2	secese
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
<g/>
.	.	kIx.	.
<g/>
Hergé	Hergá	k1gFnSc6	Hergá
(	(	kIx(	(
<g/>
civilním	civilní	k2eAgNnSc7d1	civilní
jménem	jméno	k1gNnSc7	jméno
Georges	Georgesa	k1gFnPc2	Georgesa
Remi	Remi	k1gNnSc2	Remi
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
belgický	belgický	k2eAgMnSc1d1	belgický
kreslíř	kreslíř	k1gMnSc1	kreslíř
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
komiksové	komiksový	k2eAgFnSc2d1	komiksová
postavičky	postavička	k1gFnSc2	postavička
Tintin	Tintina	k1gFnPc2	Tintina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počínajícím	počínající	k2eAgMnSc6d1	počínající
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
známí	známý	k2eAgMnPc1d1	známý
malíři	malíř	k1gMnPc1	malíř
Luc	Luc	k1gFnSc2	Luc
Tuymans	Tuymans	k1gInSc1	Tuymans
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
Borremans	Borremansa	k1gFnPc2	Borremansa
<g/>
.	.	kIx.	.
</s>
<s>
Nositelem	nositel	k1gMnSc7	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
je	být	k5eAaImIp3nS	být
François	François	k1gFnSc1	François
Englert	Englert	k1gInSc4	Englert
<g/>
,	,	kIx,	,
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
Ilja	Ilja	k1gMnSc1	Ilja
Prigogine	Prigogin	k1gMnSc5	Prigogin
<g/>
,	,	kIx,	,
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
Christian	Christian	k1gMnSc1	Christian
de	de	k?	de
Duve	Duve	k1gInSc1	Duve
<g/>
,	,	kIx,	,
Corneille	Corneille	k1gInSc1	Corneille
Heymans	Heymans	k1gInSc1	Heymans
<g/>
,	,	kIx,	,
Jules	Jules	k1gMnSc1	Jules
Bordet	Bordet	k1gMnSc1	Bordet
a	a	k8xC	a
Albert	Albert	k1gMnSc1	Albert
Claude	Claud	k1gMnSc5	Claud
<g/>
.	.	kIx.	.
</s>
<s>
Georges	Georges	k1gMnSc1	Georges
Lemaître	Lemaîtr	k1gInSc5	Lemaîtr
nastolil	nastolit	k5eAaPmAgMnS	nastolit
slavnou	slavný	k2eAgFnSc4d1	slavná
hypotézu	hypotéza	k1gFnSc4	hypotéza
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
vesmíru	vesmír	k1gInSc2	vesmír
Velkým	velký	k2eAgInSc7d1	velký
třeskem	třesk	k1gInSc7	třesk
<g/>
.	.	kIx.	.
</s>
<s>
Andreas	Andreas	k1gMnSc1	Andreas
Vesalius	Vesalius	k1gMnSc1	Vesalius
posunul	posunout	k5eAaPmAgMnS	posunout
poznání	poznání	k1gNnSc3	poznání
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
anatomie	anatomie	k1gFnSc2	anatomie
<g/>
,	,	kIx,	,
chemik	chemik	k1gMnSc1	chemik
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
van	van	k1gInSc4	van
Helmont	Helmont	k1gMnSc1	Helmont
založil	založit	k5eAaPmAgMnS	založit
studium	studium	k1gNnSc4	studium
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
Abraham	Abraham	k1gMnSc1	Abraham
Ortelius	Ortelius	k1gMnSc1	Ortelius
byl	být	k5eAaImAgMnS	být
tvůrcem	tvůrce	k1gMnSc7	tvůrce
prvního	první	k4xOgInSc2	první
atlasu	atlas	k1gInSc2	atlas
<g/>
,	,	kIx,	,
kartografii	kartografie	k1gFnSc4	kartografie
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
i	i	k9	i
matematik	matematik	k1gMnSc1	matematik
Gerhard	Gerhard	k1gMnSc1	Gerhard
Mercator	Mercator	k1gMnSc1	Mercator
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
Mercatorovo	Mercatorův	k2eAgNnSc1d1	Mercatorovo
zobrazení	zobrazení	k1gNnSc1	zobrazení
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Leo	Leo	k1gMnSc1	Leo
Baekeland	Baekelanda	k1gFnPc2	Baekelanda
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
bakelit	bakelit	k1gInSc4	bakelit
a	a	k8xC	a
fotografický	fotografický	k2eAgInSc4d1	fotografický
papír	papír	k1gInSc4	papír
<g/>
,	,	kIx,	,
Étienne	Étienn	k1gInSc5	Étienn
Lenoir	Lenoir	k1gInSc1	Lenoir
první	první	k4xOgMnSc1	první
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
stacionární	stacionární	k2eAgInSc1d1	stacionární
plynový	plynový	k2eAgInSc1d1	plynový
spalovací	spalovací	k2eAgInSc1d1	spalovací
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
Zénobe	Zénob	k1gMnSc5	Zénob
Gramme	Gramme	k1gMnSc5	Gramme
první	první	k4xOgInSc1	první
prakticky	prakticky	k6eAd1	prakticky
využitelný	využitelný	k2eAgInSc1d1	využitelný
motor	motor	k1gInSc1	motor
na	na	k7c4	na
stejnosměrný	stejnosměrný	k2eAgInSc4d1	stejnosměrný
proud	proud	k1gInSc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Solvayův	Solvayův	k2eAgInSc4d1	Solvayův
proces	proces	k1gInSc4	proces
objevil	objevit	k5eAaPmAgMnS	objevit
chemik	chemik	k1gMnSc1	chemik
Ernest	Ernest	k1gMnSc1	Ernest
Solvay	Solvaa	k1gFnSc2	Solvaa
<g/>
.	.	kIx.	.
</s>
<s>
Fyzik	fyzik	k1gMnSc1	fyzik
Joseph	Joseph	k1gMnSc1	Joseph
Plateau	Plateaa	k1gFnSc4	Plateaa
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
představil	představit	k5eAaPmAgMnS	představit
iluzi	iluze	k1gFnSc4	iluze
pohybujícího	pohybující	k2eAgMnSc2d1	pohybující
se	se	k3xPyFc4	se
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Vynálezcem	vynálezce	k1gMnSc7	vynálezce
saxofonu	saxofon	k1gInSc2	saxofon
byl	být	k5eAaImAgMnS	být
Adolphe	Adolphe	k1gNnSc4	Adolphe
Sax	sax	k1gInSc1	sax
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgMnPc7d1	významný
matematiky	matematik	k1gMnPc7	matematik
byli	být	k5eAaImAgMnP	být
Eugè	Eugè	k1gMnSc1	Eugè
Charles	Charles	k1gMnSc1	Charles
Catalan	Catalan	k1gMnSc1	Catalan
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
Bourgain	Bourgain	k1gMnSc1	Bourgain
či	či	k8xC	či
Pierre	Pierr	k1gMnSc5	Pierr
Deligne	Delign	k1gMnSc5	Delign
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Cailliau	Cailliaus	k1gInSc2	Cailliaus
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
vynalézt	vynalézt	k5eAaPmF	vynalézt
systém	systém	k1gInSc4	systém
WWW	WWW	kA	WWW
<g/>
.	.	kIx.	.
</s>
<s>
Středověká	středověký	k2eAgFnSc1d1	středověká
kronika	kronika	k1gFnSc1	kronika
Jeana	Jean	k1gMnSc2	Jean
Froissarta	Froissart	k1gMnSc2	Froissart
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
historiografickým	historiografický	k2eAgMnPc3d1	historiografický
dílům	díl	k1gInPc3	díl
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Henri	Henr	k1gMnSc5	Henr
Pirenne	Pirenn	k1gMnSc5	Pirenn
byl	být	k5eAaImAgInS	být
významným	významný	k2eAgMnSc7d1	významný
historikem-medievalistou	historikemedievalista	k1gMnSc7	historikem-medievalista
<g/>
.	.	kIx.	.
</s>
<s>
Humanista	humanista	k1gMnSc1	humanista
Justus	Justus	k1gMnSc1	Justus
Lipsius	Lipsius	k1gMnSc1	Lipsius
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
vytvořit	vytvořit	k5eAaPmF	vytvořit
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
verzi	verze	k1gFnSc4	verze
stoicismu	stoicismus	k1gInSc2	stoicismus
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
humanistického	humanistický	k2eAgInSc2d1	humanistický
okruhu	okruh	k1gInSc2	okruh
patřil	patřit	k5eAaImAgInS	patřit
i	i	k9	i
nakladatel	nakladatel	k1gMnSc1	nakladatel
a	a	k8xC	a
tiskař	tiskař	k1gMnSc1	tiskař
Christophe	Christophe	k1gFnPc2	Christophe
Plantin	Plantin	k1gMnSc1	Plantin
<g/>
.	.	kIx.	.
</s>
<s>
Descartovo	Descartův	k2eAgNnSc4d1	Descartovo
dílo	dílo	k1gNnSc4	dílo
rozvíjel	rozvíjet	k5eAaImAgMnS	rozvíjet
filozof	filozof	k1gMnSc1	filozof
Arnold	Arnold	k1gMnSc1	Arnold
Geulincx	Geulincx	k1gInSc4	Geulincx
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníkem	průkopník	k1gMnSc7	průkopník
užití	užití	k1gNnSc2	užití
statistických	statistický	k2eAgFnPc2d1	statistická
metod	metoda	k1gFnPc2	metoda
v	v	k7c6	v
sociologii	sociologie	k1gFnSc6	sociologie
byl	být	k5eAaImAgInS	být
Adolphe	Adolphe	k1gInSc1	Adolphe
Quetelet	Quetelet	k1gInSc4	Quetelet
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
též	též	k9	též
astronom	astronom	k1gMnSc1	astronom
<g/>
.	.	kIx.	.
</s>
<s>
Victor	Victor	k1gMnSc1	Victor
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Hondt	Hondt	k1gMnSc1	Hondt
je	být	k5eAaImIp3nS	být
tvůrcem	tvůrce	k1gMnSc7	tvůrce
metody	metoda	k1gFnSc2	metoda
rozdílení	rozdílení	k1gNnSc2	rozdílení
volebních	volební	k2eAgInPc2d1	volební
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
literární	literární	k2eAgMnSc1d1	literární
teoretik	teoretik	k1gMnSc1	teoretik
proslul	proslout	k5eAaPmAgMnS	proslout
Paul	Paul	k1gMnSc1	Paul
de	de	k?	de
Man	Man	k1gMnSc1	Man
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
Otlet	Otlet	k1gMnSc1	Otlet
byl	být	k5eAaImAgMnS	být
průkopníkem	průkopník	k1gMnSc7	průkopník
informační	informační	k2eAgFnSc2d1	informační
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Marxistickým	marxistický	k2eAgMnSc7d1	marxistický
teoretikem	teoretik	k1gMnSc7	teoretik
byl	být	k5eAaImAgMnS	být
Ernest	Ernest	k1gMnSc1	Ernest
Mandel	mandel	k1gInSc4	mandel
<g/>
,	,	kIx,	,
Chantal	Chantal	k1gMnSc1	Chantal
Mouffeová	Mouffeová	k1gFnSc1	Mouffeová
je	být	k5eAaImIp3nS	být
politoložkou	politoložka	k1gFnSc7	politoložka
poststrukturalistické	poststrukturalistický	k2eAgFnSc2d1	poststrukturalistická
a	a	k8xC	a
postmarxistické	postmarxistický	k2eAgFnSc2d1	postmarxistická
orientace	orientace	k1gFnSc2	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Tenistky	tenistka	k1gFnPc1	tenistka
Kim	Kim	k1gFnSc1	Kim
Clijstersová	Clijstersová	k1gFnSc1	Clijstersová
a	a	k8xC	a
Justine	Justin	k1gMnSc5	Justin
Heninová	Heninová	k1gFnSc1	Heninová
byly	být	k5eAaImAgFnP	být
světovými	světový	k2eAgFnPc7d1	světová
jedničkami	jednička	k1gFnPc7	jednička
<g/>
.	.	kIx.	.
</s>
<s>
Eddy	Edda	k1gFnPc1	Edda
Merckx	Merckx	k1gInSc1	Merckx
byl	být	k5eAaImAgMnS	být
legendárním	legendární	k2eAgMnSc7d1	legendární
cyklistou	cyklista	k1gMnSc7	cyklista
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
velký	velký	k2eAgMnSc1d1	velký
soupeř	soupeř	k1gMnSc1	soupeř
Freddy	Fredda	k1gFnSc2	Fredda
Maertens	Maertens	k1gInSc1	Maertens
<g/>
.	.	kIx.	.
</s>
<s>
Legendou	legenda	k1gFnSc7	legenda
motorového	motorový	k2eAgInSc2d1	motorový
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
závodu	závod	k1gInSc2	závod
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
Le	Le	k1gFnSc2	Le
Mans	Mansa	k1gFnPc2	Mansa
<g/>
,	,	kIx,	,
je	on	k3xPp3gFnPc4	on
Jacky	Jacka	k1gFnPc4	Jacka
Ickx	Ickx	k1gInSc1	Ickx
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
zlatých	zlatý	k2eAgFnPc2d1	zlatá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
lukostřelec	lukostřelec	k1gMnSc1	lukostřelec
Hubert	Hubert	k1gMnSc1	Hubert
Van	van	k1gInSc4	van
Innis	Innis	k1gFnSc2	Innis
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
fotbalistů	fotbalista	k1gMnPc2	fotbalista
jsou	být	k5eAaImIp3nP	být
známí	známý	k1gMnPc1	známý
především	především	k6eAd1	především
Eden	Eden	k1gInSc4	Eden
Hazard	hazard	k2eAgInSc4d1	hazard
<g/>
,	,	kIx,	,
Thibaut	Thibaut	k2eAgInSc4d1	Thibaut
Courtois	Courtois	k1gInSc4	Courtois
<g/>
,	,	kIx,	,
Radja	Radja	k1gMnSc1	Radja
Nainggolan	Nainggolan	k1gMnSc1	Nainggolan
<g/>
,	,	kIx,	,
či	či	k8xC	či
Kevin	Kevin	k1gMnSc1	Kevin
De	De	k?	De
Bruyne	Bruyn	k1gInSc5	Bruyn
<g/>
.	.	kIx.	.
</s>
