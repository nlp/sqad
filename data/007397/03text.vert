<s>
Anders	Anders	k1gInSc1	Anders
Celsius	Celsius	k1gMnSc1	Celsius
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1701	[number]	k4	1701
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1744	[number]	k4	1744
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švédský	švédský	k2eAgMnSc1d1	švédský
astronom	astronom	k1gMnSc1	astronom
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
autora	autor	k1gMnSc4	autor
Celsiovy	Celsiův	k2eAgFnSc2d1	Celsiova
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1736	[number]	k4	1736
až	až	k8xS	až
1737	[number]	k4	1737
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
Laponské	laponský	k2eAgFnSc2d1	laponská
expedice	expedice	k1gFnSc2	expedice
vedené	vedený	k2eAgFnSc2d1	vedená
francouzským	francouzský	k2eAgMnSc7d1	francouzský
astronomem	astronom	k1gMnSc7	astronom
Maupertuisem	Maupertuis	k1gInSc7	Maupertuis
do	do	k7c2	do
Tornia	Tornium	k1gNnSc2	Tornium
<g/>
.	.	kIx.	.
</s>
<s>
Expedici	expedice	k1gFnSc3	expedice
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
studijního	studijní	k2eAgInSc2d1	studijní
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
na	na	k7c6	na
tamější	tamější	k2eAgFnSc6d1	tamější
Francouzské	francouzský	k2eAgFnSc6d1	francouzská
akademii	akademie	k1gFnSc6	akademie
věd	věda	k1gFnPc2	věda
jako	jako	k8xC	jako
paralelní	paralelní	k2eAgFnSc4d1	paralelní
expedici	expedice	k1gFnSc4	expedice
k	k	k7c3	k
expedici	expedice	k1gFnSc3	expedice
do	do	k7c2	do
Peru	Peru	k1gNnSc2	Peru
(	(	kIx(	(
<g/>
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
<g/>
)	)	kIx)	)
a	a	k8xC	a
která	který	k3yQgFnSc1	který
postupnými	postupný	k2eAgNnPc7d1	postupné
měřeními	měření	k1gNnPc7	měření
délky	délka	k1gFnSc2	délka
jednoho	jeden	k4xCgInSc2	jeden
stupně	stupeň	k1gInSc2	stupeň
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
šířky	šířka	k1gFnSc2	šířka
dokázala	dokázat	k5eAaPmAgFnS	dokázat
Newtonovu	Newtonův	k2eAgFnSc4d1	Newtonova
hypotézu	hypotéza	k1gFnSc4	hypotéza
o	o	k7c6	o
zploštění	zploštění	k1gNnSc6	zploštění
Země	zem	k1gFnSc2	zem
na	na	k7c6	na
pólech	pól	k1gInPc6	pól
<g/>
.	.	kIx.	.
</s>
<s>
Svojí	svůj	k3xOyFgFnSc7	svůj
účastí	účast	k1gFnSc7	účast
na	na	k7c6	na
expedici	expedice	k1gFnSc6	expedice
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velmi	velmi	k6eAd1	velmi
známým	známý	k2eAgMnSc7d1	známý
a	a	k8xC	a
tak	tak	k6eAd1	tak
mu	on	k3xPp3gMnSc3	on
švédské	švédský	k2eAgInPc1d1	švédský
úřady	úřad	k1gInPc1	úřad
poskytly	poskytnout	k5eAaPmAgInP	poskytnout
finance	finance	k1gFnPc4	finance
na	na	k7c6	na
otevření	otevření	k1gNnSc6	otevření
Celsiovy	Celsiův	k2eAgFnSc2d1	Celsiova
observatoře	observatoř	k1gFnSc2	observatoř
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
observatoř	observatoř	k1gFnSc1	observatoř
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
roku	rok	k1gInSc2	rok
1741	[number]	k4	1741
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
ji	on	k3xPp3gFnSc4	on
vybavil	vybavit	k5eAaPmAgInS	vybavit
nejmodernějšími	moderní	k2eAgInPc7d3	nejmodernější
přístroji	přístroj	k1gInPc7	přístroj
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
získal	získat	k5eAaPmAgInS	získat
při	při	k7c6	při
svých	svůj	k3xOyFgFnPc6	svůj
cestách	cesta	k1gFnPc6	cesta
po	po	k7c6	po
cizině	cizina	k1gFnSc6	cizina
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
i	i	k9	i
polární	polární	k2eAgFnPc4d1	polární
záře	zář	k1gFnPc4	zář
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc4	první
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
souvislosti	souvislost	k1gFnPc4	souvislost
mezi	mezi	k7c7	mezi
polárními	polární	k2eAgFnPc7d1	polární
zářemi	zář	k1gFnPc7	zář
a	a	k8xC	a
poruchami	porucha	k1gFnPc7	porucha
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
také	také	k9	také
změřil	změřit	k5eAaPmAgMnS	změřit
jasnost	jasnost	k1gFnSc4	jasnost
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
hvězd	hvězda	k1gFnPc2	hvězda
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
chybou	chyba	k1gFnSc7	chyba
0,4	[number]	k4	0,4
mag	mag	k?	mag
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
známým	známý	k1gMnSc7	známý
roku	rok	k1gInSc2	rok
1742	[number]	k4	1742
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Celsiovu	Celsiův	k2eAgFnSc4d1	Celsiova
stodílkovou	stodílkový	k2eAgFnSc4d1	stodílkový
teplotní	teplotní	k2eAgFnSc4d1	teplotní
stupnici	stupnice	k1gFnSc4	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
stupnice	stupnice	k1gFnSc1	stupnice
ale	ale	k9	ale
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
obrácená	obrácený	k2eAgFnSc1d1	obrácená
<g/>
,	,	kIx,	,
bod	bod	k1gInSc1	bod
varu	var	k1gInSc2	var
vody	voda	k1gFnSc2	voda
měl	mít	k5eAaImAgInS	mít
0	[number]	k4	0
a	a	k8xC	a
bod	bod	k1gInSc4	bod
tuhnutí	tuhnutí	k1gNnSc4	tuhnutí
+100	+100	k4	+100
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nám	my	k3xPp1nPc3	my
známého	známý	k2eAgInSc2d1	známý
tvaru	tvar	k1gInSc2	tvar
ji	on	k3xPp3gFnSc4	on
obrátil	obrátit	k5eAaPmAgMnS	obrátit
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1745	[number]	k4	1745
Carl	Carl	k1gInSc1	Carl
von	von	k1gInSc4	von
Linné	Linná	k1gFnSc2	Linná
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Celsius	Celsius	k1gMnSc1	Celsius
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
Anders	Andersa	k1gFnPc2	Andersa
Celsius	Celsius	k1gMnSc1	Celsius
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Anders	Andersa	k1gFnPc2	Andersa
Celsius	Celsius	k1gMnSc1	Celsius
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc5	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
