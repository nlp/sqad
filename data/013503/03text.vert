<s>
Bohuslavický	Bohuslavický	k2eAgInSc1d1
tunel	tunel	k1gInSc1
</s>
<s>
Bohuslavický	Bohuslavický	k2eAgInSc1d1
tunel	tunel	k1gInSc1
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc4
Provozní	provozní	k2eAgFnSc1d1
délka	délka	k1gFnSc1
</s>
<s>
187,5	187,5	k4
m	m	kA
Výstavba	výstavba	k1gFnSc1
Lokalizace	lokalizace	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
32	#num#	k4
<g/>
′	′	k?
<g/>
32	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
<g/>
55	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Bohuslavický	Bohuslavický	k2eAgInSc1d1
tunel	tunel	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bohuslavický	Bohuslavický	k2eAgInSc1d1
tunel	tunel	k1gInSc1
je	být	k5eAaImIp3nS
železniční	železniční	k2eAgInSc1d1
tunel	tunel	k1gInSc1
na	na	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
obce	obec	k1gFnSc2
Suchovršice	Suchovršice	k1gFnSc2
na	na	k7c6
regionální	regionální	k2eAgFnSc6d1
železniční	železniční	k2eAgFnSc6d1
trati	trať	k1gFnSc6
Jaroměř	Jaroměř	k1gFnSc1
–	–	k?
Trutnov	Trutnov	k1gInSc1
mezi	mezi	k7c7
zastávkami	zastávka	k1gFnPc7
Suchovršice	Suchovršice	k1gFnPc4
a	a	k8xC
Bohuslavice	Bohuslavice	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1854	#num#	k4
požádali	požádat	k5eAaPmAgMnP
podnikatelé	podnikatel	k1gMnPc1
z	z	k7c2
Liberecka	Liberecko	k1gNnSc2
o	o	k7c4
vydání	vydání	k1gNnSc4
koncese	koncese	k1gFnSc2
na	na	k7c4
stavbu	stavba	k1gFnSc4
železnice	železnice	k1gFnSc2
z	z	k7c2
Liberce	Liberec	k1gInSc2
do	do	k7c2
Pardubic	Pardubice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listem	list	k1gInSc7
povolení	povolení	k1gNnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
ze	z	k7c2
dne	den	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1856	#num#	k4
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
koncese	koncese	k1gFnSc1
pro	pro	k7c4
podnikatele	podnikatel	k1gMnSc4
Liebiega	Liebieg	k1gMnSc4
<g/>
,	,	kIx,
Lannu	Lanna	k1gMnSc4
a	a	k8xC
bratry	bratr	k1gMnPc4
Kleinovy	Kleinův	k2eAgFnSc2d1
na	na	k7c4
výstavbu	výstavba	k1gFnSc4
a	a	k8xC
provoz	provoz	k1gInSc4
železnice	železnice	k1gFnSc2
z	z	k7c2
Pardubic	Pardubice	k1gInPc2
do	do	k7c2
Liberce	Liberec	k1gInSc2
a	a	k8xC
rovněž	rovněž	k9
odbočnou	odbočný	k2eAgFnSc4d1
trať	trať	k1gFnSc4
z	z	k7c2
Josefova	Josefov	k1gInSc2
do	do	k7c2
Malých	Malých	k2eAgFnPc2d1
Svatoňovic	Svatoňovice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trať	trať	k1gFnSc1
stavěla	stavět	k5eAaImAgFnS
a	a	k8xC
zprovozňovala	zprovozňovat	k5eAaImAgFnS
po	po	k7c6
etapách	etapa	k1gFnPc6
společnost	společnost	k1gFnSc4
Jihoseveroněmecká	Jihoseveroněmecký	k2eAgFnSc1d1
spojovací	spojovací	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
(	(	kIx(
<g/>
SNDVB	SNDVB	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
to	ten	k3xDgNnSc1
úsek	úsek	k1gInSc1
Pardubice	Pardubice	k1gInPc1
<g/>
–	–	k?
<g/>
Josefov	Josefov	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1857	#num#	k4
<g/>
,	,	kIx,
úsek	úsek	k1gInSc1
Josefov	Josefov	k1gInSc1
<g/>
–	–	k?
<g/>
Turnov	Turnov	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1858	#num#	k4
a	a	k8xC
úsek	úsek	k1gInSc1
Turnov	Turnov	k1gInSc1
<g/>
–	–	k?
<g/>
Liberec	Liberec	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1859	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohuslavický	Bohuslavický	k2eAgInSc1d1
tunel	tunel	k1gInSc1
byl	být	k5eAaImAgInS
dokončen	dokončit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1868	#num#	k4
a	a	k8xC
byl	být	k5eAaImAgInS
sanován	sanovat	k5eAaBmNgInS
a	a	k8xC
opravován	opravovat	k5eAaImNgInS
v	v	k7c6
roce	rok	k1gInSc6
1874	#num#	k4
<g/>
,	,	kIx,
1926	#num#	k4
a	a	k8xC
1973	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Jednokolejný	jednokolejný	k2eAgInSc1d1
tunel	tunel	k1gInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
na	na	k7c6
železniční	železniční	k2eAgFnSc6d1
trati	trať	k1gFnSc6
Jaroměř	Jaroměř	k1gFnSc1
–	–	k?
Trutnov	Trutnov	k1gInSc1
v	v	k7c6
úseku	úsek	k1gInSc6
mezi	mezi	k7c7
zastávkami	zastávka	k1gFnPc7
Suchovršice	Suchovršice	k1gFnPc4
a	a	k8xC
Bohuslavice	Bohuslavice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
405	#num#	k4
m	m	kA
a	a	k8xC
měří	měřit	k5eAaImIp3nS
187,5	187,5	k4
m.	m.	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Tunel	tunel	k1gInSc1
byl	být	k5eAaImAgInS
ražen	razit	k5eAaImNgInS
v	v	k7c6
hřebeni	hřeben	k1gInSc6
kopce	kopec	k1gInSc2
(	(	kIx(
<g/>
492	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
v	v	k7c6
permských	permský	k2eAgInPc6d1
sedimentech	sediment	k1gInPc6
trutnovského	trutnovský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
dolomitickými	dolomitický	k2eAgInPc7d1
pískovci	pískovec	k1gInPc7
a	a	k8xC
slepenci	slepenec	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Vjezdový	vjezdový	k2eAgInSc1d1
a	a	k8xC
výjezdový	výjezdový	k2eAgInSc1d1
portál	portál	k1gInSc1
je	být	k5eAaImIp3nS
vyzděn	vyzdít	k5eAaPmNgInS
z	z	k7c2
kyklopského	kyklopský	k2eAgNnSc2d1
zdiva	zdivo	k1gNnSc2
z	z	k7c2
pískovcových	pískovcový	k2eAgInPc2d1
kvádrů	kvádr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tunel	tunel	k1gInSc1
má	mít	k5eAaImIp3nS
klenby	klenba	k1gFnPc4
vyzděny	vyzděn	k2eAgFnPc4d1
pískovcovými	pískovcový	k2eAgInPc7d1
kvádry	kvádr	k1gInPc7
<g/>
,	,	kIx,
tunelové	tunelový	k2eAgFnPc1d1
opěry	opěra	k1gFnPc1
mají	mít	k5eAaImIp3nP
řádkové	řádkový	k2eAgNnSc4d1
místy	místy	k6eAd1
kyklopské	kyklopský	k2eAgNnSc4d1
zdivo	zdivo	k1gNnSc4
a	a	k8xC
jsou	být	k5eAaImIp3nP
vyzděné	vyzděný	k2eAgInPc1d1
z	z	k7c2
pískovcových	pískovcový	k2eAgInPc2d1
kvádrů	kvádr	k1gInPc2
a	a	k8xC
kopáků	kopák	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
letech	let	k1gInPc6
2007	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
byl	být	k5eAaImAgInS
tunel	tunel	k1gInSc1
rekonstruován	rekonstruovat	k5eAaBmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
rekonstrukci	rekonstrukce	k1gFnSc6
bylo	být	k5eAaImAgNnS
odstraněno	odstranit	k5eAaPmNgNnS
nevhodné	vhodný	k2eNgNnSc1d1
podskružení	podskružení	k1gNnSc1
z	z	k7c2
opravy	oprava	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
provedena	proveden	k2eAgFnSc1d1
oprava	oprava	k1gFnSc1
a	a	k8xC
zajištění	zajištění	k1gNnSc1
ostění	ostění	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
portálovém	portálový	k2eAgInSc6d1
pasu	pas	k1gInSc6
byla	být	k5eAaImAgFnS
zajištěna	zajištěn	k2eAgFnSc1d1
klenba	klenba	k1gFnSc1
proti	proti	k7c3
průsakům	průsak	k1gInPc3
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Bohuslavický	Bohuslavický	k2eAgInSc4d1
(	(	kIx(
<g/>
tunel	tunel	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.atlasdrah.net	www.atlasdrah.net	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Bohuslavický	Bohuslavický	k2eAgInSc4d1
tunel	tunel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mapy	mapa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Geologické	geologický	k2eAgFnSc2d1
lokality	lokalita	k1gFnSc2
-	-	kIx~
Vyhledávání	vyhledávání	k1gNnSc1
-	-	kIx~
Bohuslavice	Bohuslavice	k1gFnPc1
-	-	kIx~
skalky	skalka	k1gFnPc1
(	(	kIx(
<g/>
thuring	thuring	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
lokality	lokalita	k1gFnPc4
<g/>
.	.	kIx.
<g/>
geology	geolog	k1gMnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
LACINA	Lacina	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
;	;	kIx,
MATĚJÍČEK	Matějíček	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohuslavický	Bohuslavický	k2eAgInSc1d1
tunel	tunel	k1gInSc1
<g/>
–	–	k?
<g/>
zajištění	zajištění	k1gNnSc1
ostění	ostění	k1gNnSc1
<g/>
.	.	kIx.
mosty	most	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-9-5	2011-9-5	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
CIGLER	CIGLER	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
MELICHAŘÍK	MELICHAŘÍK	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praktické	praktický	k2eAgFnPc1d1
zkušenosti	zkušenost	k1gFnPc1
získané	získaný	k2eAgFnPc1d1
při	při	k7c6
zajišťování	zajišťování	k1gNnSc6
ostění	ostění	k1gNnSc2
Bohuslavického	Bohuslavický	k2eAgNnSc2d1
ostění	ostění	k1gNnSc2
2009	#num#	k4
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
DRIENOVSKÝ	DRIENOVSKÝ	kA
<g/>
,	,	kIx,
Zbyněk	Zbyněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgInPc1d1
materiály	materiál	k1gInPc1
pro	pro	k7c4
efektivní	efektivní	k2eAgFnPc4d1
sanace	sanace	k1gFnPc4
ostění	ostění	k1gNnSc2
tunelů	tunel	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tunel	tunel	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
26	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
Jaroměř	Jaroměř	k1gFnSc1
<g/>
–	–	k?
<g/>
Trutnov	Trutnov	k1gInSc1
</s>
<s>
Seznam	seznam	k1gInSc1
železničních	železniční	k2eAgInPc2d1
tunelů	tunel	k1gInPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Železnice	železnice	k1gFnSc1
</s>
