<s desamb="1">
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
405	#num#	k4
m	m	kA
a	a	k8xC
měří	měřit	k5eAaImIp3nS
187,5	187,5	k4
m.	m.	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Tunel	tunel	k1gInSc1
byl	být	k5eAaImAgInS
ražen	razit	k5eAaImNgInS
v	v	k7c6
hřebeni	hřeben	k1gInSc6
kopce	kopec	k1gInSc2
(	(	kIx(
<g/>
492	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
v	v	k7c6
permských	permský	k2eAgInPc6d1
sedimentech	sediment	k1gInPc6
trutnovského	trutnovský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
<g/>
.	.	kIx.
</s>