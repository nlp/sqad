<s>
Biologická	biologický	k2eAgFnSc1d1	biologická
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vědecké	vědecký	k2eAgNnSc1d1	vědecké
názvosloví	názvosloví	k1gNnSc1	názvosloví
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
biologických	biologický	k2eAgInPc2d1	biologický
taxonů	taxon	k1gInPc2	taxon
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
dorozumívání	dorozumívání	k1gNnSc1	dorozumívání
mezi	mezi	k7c7	mezi
odborníky	odborník	k1gMnPc7	odborník
hovořícími	hovořící	k2eAgMnPc7d1	hovořící
různými	různý	k2eAgMnPc7d1	různý
jazyky	jazyk	k1gMnPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
binominálního	binominální	k2eAgNnSc2d1	binominální
pojmenování	pojmenování	k1gNnSc2	pojmenování
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
stanoví	stanovit	k5eAaPmIp3nS	stanovit
zásady	zásada	k1gFnPc4	zásada
pro	pro	k7c4	pro
pojmenování	pojmenování	k1gNnSc4	pojmenování
ostatních	ostatní	k2eAgInPc2d1	ostatní
nižších	nízký	k2eAgInPc2d2	nižší
i	i	k8xC	i
vyšších	vysoký	k2eAgInPc2d2	vyšší
taxonů	taxon	k1gInPc2	taxon
<g/>
.	.	kIx.	.
</s>
<s>
Binominální	Binominální	k2eAgFnSc1d1	Binominální
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
je	být	k5eAaImIp3nS	být
dvouslovná	dvouslovný	k2eAgFnSc1d1	dvouslovná
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
binomen	binomen	k2eAgMnSc1d1	binomen
=	=	kIx~	=
dvě	dva	k4xCgNnPc4	dva
jména	jméno	k1gNnPc4	jméno
<g/>
)	)	kIx)	)
soustava	soustava	k1gFnSc1	soustava
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
jmen	jméno	k1gNnPc2	jméno
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
označování	označování	k1gNnSc4	označování
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
binominální	binominální	k2eAgFnSc2d1	binominální
nomenklatury	nomenklatura	k1gFnSc2	nomenklatura
je	být	k5eAaImIp3nS	být
švédský	švédský	k2eAgMnSc1d1	švédský
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Carl	Carl	k1gMnSc1	Carl
Linné	Linné	k2eAgMnSc1d1	Linné
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zavedl	zavést	k5eAaPmAgInS	zavést
její	její	k3xOp3gNnSc4	její
důsledné	důsledný	k2eAgNnSc4d1	důsledné
používání	používání	k1gNnSc4	používání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
binominální	binominální	k2eAgFnSc6d1	binominální
nomenklatuře	nomenklatura	k1gFnSc6	nomenklatura
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc1	každý
druh	druh	k1gInSc1	druh
popsán	popsat	k5eAaPmNgInS	popsat
dvouslovným	dvouslovný	k2eAgInSc7d1	dvouslovný
latinským	latinský	k2eAgInSc7d1	latinský
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
polatinštěným	polatinštěný	k2eAgNnSc7d1	polatinštěný
<g/>
)	)	kIx)	)
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
odborně	odborně	k6eAd1	odborně
binomickým	binomický	k2eAgNnSc7d1	binomické
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Aquila	Aquil	k1gMnSc2	Aquil
chrysaetos	chrysaetos	k1gMnSc1	chrysaetos
-	-	kIx~	-
orel	orel	k1gMnSc1	orel
skalní	skalní	k2eAgMnSc1d1	skalní
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc1	jméno
rodové	rodový	k2eAgNnSc1d1	rodové
(	(	kIx(	(
<g/>
Aquila	Aquila	k1gFnSc1	Aquila
<g/>
,	,	kIx,	,
orel	orel	k1gMnSc1	orel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
druhé	druhý	k4xOgNnSc1	druhý
je	být	k5eAaImIp3nS	být
druhové	druhový	k2eAgNnSc1d1	druhové
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
druhový	druhový	k2eAgInSc1d1	druhový
přívlastek	přívlastek	k1gInSc1	přívlastek
(	(	kIx(	(
<g/>
chrysaetos	chrysaetos	k1gInSc1	chrysaetos
<g/>
,	,	kIx,	,
skalní	skalní	k2eAgInPc1d1	skalní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Binominální	Binominální	k2eAgFnSc1d1	Binominální
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
příbuznost	příbuznost	k1gFnSc1	příbuznost
určitého	určitý	k2eAgInSc2d1	určitý
druhu	druh	k1gInSc2	druh
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
druhy	druh	k1gInPc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
jména	jméno	k1gNnPc4	jméno
Aquila	Aquila	k1gFnSc1	Aquila
chrysaetos	chrysaetos	k1gMnSc1	chrysaetos
-	-	kIx~	-
orel	orel	k1gMnSc1	orel
skalní	skalní	k2eAgMnPc1d1	skalní
a	a	k8xC	a
Aquila	Aquila	k1gFnSc1	Aquila
heliaca	heliaca	k1gFnSc1	heliaca
-	-	kIx~	-
orel	orel	k1gMnSc1	orel
královský	královský	k2eAgMnSc1d1	královský
určují	určovat	k5eAaImIp3nP	určovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
stejného	stejný	k2eAgInSc2d1	stejný
rodu	rod	k1gInSc2	rod
(	(	kIx(	(
<g/>
Aquila	Aquila	k1gFnSc1	Aquila
<g/>
,	,	kIx,	,
orel	orel	k1gMnSc1	orel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Binominální	Binominální	k2eAgFnSc1d1	Binominální
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
pro	pro	k7c4	pro
nebuněčné	buněčný	k2eNgInPc4d1	nebuněčný
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
vědecká	vědecký	k2eAgNnPc1d1	vědecké
pojmenování	pojmenování	k1gNnSc4	pojmenování
rodů	rod	k1gInPc2	rod
virů	vir	k1gInPc2	vir
jsou	být	k5eAaImIp3nP	být
unifikovaně	unifikovaně	k6eAd1	unifikovaně
zakončena	zakončen	k2eAgFnSc1d1	zakončena
-virus	irus	k1gInSc1	-virus
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Mimivirus	Mimivirus	k1gInSc1	Mimivirus
<g/>
)	)	kIx)	)
a	a	k8xC	a
vědecká	vědecký	k2eAgNnPc4d1	vědecké
pojmenování	pojmenování	k1gNnSc4	pojmenování
rodů	rod	k1gInPc2	rod
viroidů	viroid	k1gInPc2	viroid
-viroid	iroid	k1gInSc1	-viroid
(	(	kIx(	(
<g/>
Pospiviroid	Pospiviroid	k1gInSc1	Pospiviroid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhová	druhový	k2eAgNnPc1d1	druhové
jména	jméno	k1gNnPc1	jméno
však	však	k9	však
rodové	rodový	k2eAgNnSc1d1	rodové
jméno	jméno	k1gNnSc1	jméno
vůbec	vůbec	k9	vůbec
obsahovat	obsahovat	k5eAaImF	obsahovat
nemusejí	muset	k5eNaImIp3nP	muset
<g/>
,	,	kIx,	,
a	a	k8xC	a
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
<g/>
-li	i	k?	-li
ho	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
zpravidla	zpravidla	k6eAd1	zpravidla
až	až	k9	až
na	na	k7c6	na
posledním	poslední	k2eAgNnSc6d1	poslední
místě	místo	k1gNnSc6	místo
víceslovného	víceslovný	k2eAgNnSc2d1	víceslovné
pojmenování	pojmenování	k1gNnSc2	pojmenování
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Acanthamoeba	Acanthamoeb	k1gMnSc2	Acanthamoeb
polyphaga	polyphag	k1gMnSc2	polyphag
mimivirus	mimivirus	k1gMnSc1	mimivirus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
binominální	binominální	k2eAgFnSc3d1	binominální
nomenklatuře	nomenklatura	k1gFnSc3	nomenklatura
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
poddruhů	poddruh	k1gInPc2	poddruh
připojuje	připojovat	k5eAaImIp3nS	připojovat
ještě	ještě	k9	ještě
třetí	třetí	k4xOgNnSc4	třetí
rozlišovací	rozlišovací	k2eAgNnSc4d1	rozlišovací
slovo	slovo	k1gNnSc4	slovo
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k6eAd1	tak
trinomické	trinomický	k2eAgNnSc4d1	trinomický
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
tříslovné	tříslovný	k2eAgNnSc1d1	tříslovné
pojmenování	pojmenování	k1gNnSc1	pojmenování
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Ursus	Ursus	k1gInSc1	Ursus
arctos	arctos	k1gInSc1	arctos
horribillis	horribillis	k1gFnSc1	horribillis
-	-	kIx~	-
medvěd	medvěd	k1gMnSc1	medvěd
grizzly	grizzly	k1gMnSc1	grizzly
a	a	k8xC	a
Ursus	Ursus	k1gMnSc1	Ursus
arctos	arctos	k1gMnSc1	arctos
middendorffi	middendorff	k1gFnSc2	middendorff
-	-	kIx~	-
medvěd	medvěd	k1gMnSc1	medvěd
kodiak	kodiak	k6eAd1	kodiak
jsou	být	k5eAaImIp3nP	být
poddruhy	poddruh	k1gInPc1	poddruh
medvěda	medvěd	k1gMnSc2	medvěd
hnědého	hnědý	k2eAgMnSc2d1	hnědý
(	(	kIx(	(
<g/>
Ursus	Ursus	k1gInSc1	Ursus
arctos	arctos	k1gInSc1	arctos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
před	před	k7c4	před
třetí	třetí	k4xOgNnSc4	třetí
jméno	jméno	k1gNnSc4	jméno
píše	psát	k5eAaImIp3nS	psát
zkratka	zkratka	k1gFnSc1	zkratka
"	"	kIx"	"
<g/>
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
ssp	ssp	k?	ssp
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Avena	Aven	k1gMnSc2	Aven
sativa	sativ	k1gMnSc2	sativ
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
byzantina	byzantina	k1gFnSc1	byzantina
-	-	kIx~	-
oves	oves	k1gInSc1	oves
byzantský	byzantský	k2eAgInSc1d1	byzantský
<g/>
.	.	kIx.	.
</s>
<s>
Trinomické	trinomický	k2eAgNnSc1d1	trinomický
jméno	jméno	k1gNnSc1	jméno
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
variety	varieta	k1gFnSc2	varieta
či	či	k8xC	či
formy	forma	k1gFnSc2	forma
<g/>
,	,	kIx,	,
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
před	před	k7c4	před
třetí	třetí	k4xOgNnSc4	třetí
jméno	jméno	k1gNnSc4	jméno
připojuje	připojovat	k5eAaImIp3nS	připojovat
zkratka	zkratka	k1gFnSc1	zkratka
"	"	kIx"	"
<g/>
var.	var.	k?	var.
<g/>
"	"	kIx"	"
resp.	resp.	kA	resp.
"	"	kIx"	"
<g/>
f.	f.	k?	f.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Amanita	Amanit	k2eAgFnSc1d1	Amanita
phalloides	phalloides	k1gInSc1	phalloides
var.	var.	k?	var.
alba	album	k1gNnSc2	album
-	-	kIx~	-
muchomůrka	muchomůrka	k?	muchomůrka
bílá	bílé	k1gNnPc1	bílé
<g/>
;	;	kIx,	;
Equus	Equus	k1gInSc1	Equus
asinus	asinus	k1gMnSc1	asinus
f.	f.	k?	f.
domestica	domestica	k1gMnSc1	domestica
-	-	kIx~	-
osel	osel	k1gMnSc1	osel
domácí	domácí	k1gMnSc1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Latinská	latinský	k2eAgFnSc1d1	Latinská
(	(	kIx(	(
<g/>
vědecká	vědecký	k2eAgNnPc4d1	vědecké
<g/>
)	)	kIx)	)
jména	jméno	k1gNnPc4	jméno
taxonů	taxon	k1gInPc2	taxon
vyšších	vysoký	k2eAgInPc2d2	vyšší
než	než	k8xS	než
rod	rod	k1gInSc4	rod
se	se	k3xPyFc4	se
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
od	od	k7c2	od
typizujícího	typizující	k2eAgInSc2d1	typizující
taxonu	taxon	k1gInSc2	taxon
nejbližší	blízký	k2eAgFnSc2d3	nejbližší
hlavní	hlavní	k2eAgFnSc2d1	hlavní
nižší	nízký	k2eAgFnSc2d2	nižší
úrovně	úroveň	k1gFnSc2	úroveň
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
jméno	jméno	k1gNnSc4	jméno
podčeledi	podčeleď	k1gFnSc2	podčeleď
nebo	nebo	k8xC	nebo
čeledi	čeleď	k1gFnSc2	čeleď
ze	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jméno	jméno	k1gNnSc4	jméno
podřádu	podřád	k1gInSc2	podřád
nebo	nebo	k8xC	nebo
řádu	řád	k1gInSc2	řád
ze	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
)	)	kIx)	)
oddělením	oddělení	k1gNnSc7	oddělení
původní	původní	k2eAgFnSc2d1	původní
přípony	přípona	k1gFnSc2	přípona
a	a	k8xC	a
přidáním	přidání	k1gNnSc7	přidání
přípony	přípona	k1gFnSc2	přípona
nové	nový	k2eAgFnSc2d1	nová
<g/>
,	,	kIx,	,
uvedené	uvedený	k2eAgFnSc3d1	uvedená
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
pravidla	pravidlo	k1gNnPc1	pravidlo
botanické	botanický	k2eAgFnSc2d1	botanická
a	a	k8xC	a
mykologické	mykologický	k2eAgFnSc2d1	mykologická
nomenklatury	nomenklatura	k1gFnSc2	nomenklatura
definují	definovat	k5eAaBmIp3nP	definovat
způsob	způsob	k1gInSc4	způsob
tvorby	tvorba	k1gFnSc2	tvorba
těchto	tento	k3xDgNnPc2	tento
jmen	jméno	k1gNnPc2	jméno
až	až	k9	až
do	do	k7c2	do
úrovně	úroveň	k1gFnSc2	úroveň
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
,	,	kIx,	,
pravidla	pravidlo	k1gNnPc1	pravidlo
zoologická	zoologický	k2eAgNnPc1d1	zoologické
jdou	jít	k5eAaImIp3nP	jít
jen	jen	k9	jen
do	do	k7c2	do
úrovně	úroveň	k1gFnSc2	úroveň
nadčeledi	nadčeleď	k1gFnSc2	nadčeleď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nomenklatuře	nomenklatura	k1gFnSc6	nomenklatura
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
virů	vir	k1gInPc2	vir
jsou	být	k5eAaImIp3nP	být
zatím	zatím	k6eAd1	zatím
tato	tento	k3xDgNnPc1	tento
pravidla	pravidlo	k1gNnPc1	pravidlo
stanovena	stanovit	k5eAaPmNgNnP	stanovit
jen	jen	k6eAd1	jen
do	do	k7c2	do
úrovně	úroveň	k1gFnSc2	úroveň
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Poznámky	poznámka	k1gFnPc4	poznámka
k	k	k7c3	k
tabulce	tabulka	k1gFnSc3	tabulka
1	[number]	k4	1
V	V	kA	V
zoologii	zoologie	k1gFnSc4	zoologie
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
taxonomické	taxonomický	k2eAgFnSc6d1	Taxonomická
úrovni	úroveň	k1gFnSc6	úroveň
kmen	kmen	k1gInSc1	kmen
<g/>
;	;	kIx,	;
v	v	k7c6	v
botanice	botanika	k1gFnSc6	botanika
je	být	k5eAaImIp3nS	být
kmen	kmen	k1gInSc1	kmen
položen	položit	k5eAaPmNgInS	položit
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
úroveň	úroveň	k1gFnSc4	úroveň
jako	jako	k8xS	jako
oddělení	oddělení	k1gNnSc4	oddělení
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
je	být	k5eAaImIp3nS	být
dávána	dáván	k2eAgFnSc1d1	dávána
přednost	přednost	k1gFnSc1	přednost
<g/>
.2	.2	k4	.2
Nedefinováno	definovat	k5eNaBmNgNnS	definovat
v	v	k7c6	v
Pravidlech	pravidlo	k1gNnPc6	pravidlo
nomenklatury	nomenklatura	k1gFnSc2	nomenklatura
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
navrhováno	navrhovat	k5eAaImNgNnS	navrhovat
<g/>
.3	.3	k4	.3
Pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
ptáky	pták	k1gMnPc4	pták
a	a	k8xC	a
ryby	ryba	k1gFnPc4	ryba
<g/>
.4	.4	k4	.4
Užívání	užívání	k1gNnSc4	užívání
této	tento	k3xDgFnSc2	tento
taxonomické	taxonomický	k2eAgFnSc2d1	Taxonomická
úrovně	úroveň	k1gFnSc2	úroveň
pro	pro	k7c4	pro
bakterie	bakterie	k1gFnPc4	bakterie
se	se	k3xPyFc4	se
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
<g/>
.5	.5	k4	.5
Klasifikace	klasifikace	k1gFnSc2	klasifikace
podle	podle	k7c2	podle
ICTV	ICTV	kA	ICTV
<g/>
.6	.6	k4	.6
Klasifikace	klasifikace	k1gFnSc2	klasifikace
podle	podle	k7c2	podle
Florese	Florese	k1gFnSc2	Florese
<g/>
.	.	kIx.	.
</s>
<s>
Stanovení	stanovení	k1gNnSc1	stanovení
pevných	pevný	k2eAgInPc2d1	pevný
mezinárodně	mezinárodně	k6eAd1	mezinárodně
platných	platný	k2eAgFnPc2d1	platná
zásad	zásada	k1gFnPc2	zásada
odborné	odborný	k2eAgFnSc2d1	odborná
nomenklatury	nomenklatura	k1gFnSc2	nomenklatura
všech	všecek	k3xTgInPc2	všecek
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
kódu	kód	k1gInSc2	kód
bionomenklatury	bionomenklatura	k1gFnSc2	bionomenklatura
(	(	kIx(	(
<g/>
International	International	k1gMnSc1	International
code	cod	k1gFnSc2	cod
of	of	k?	of
bionomenclature	bionomenclatur	k1gMnSc5	bionomenclatur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
Biokódu	Biokóda	k1gFnSc4	Biokóda
(	(	kIx(	(
<g/>
BioCode	BioCod	k1gMnSc5	BioCod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stadiu	stadion	k1gNnSc6	stadion
přípravy	příprava	k1gFnSc2	příprava
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
přijetí	přijetí	k1gNnSc1	přijetí
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
jako	jako	k9	jako
problematické	problematický	k2eAgNnSc1d1	problematické
kvůli	kvůli	k7c3	kvůli
sjednocení	sjednocení	k1gNnSc3	sjednocení
opustit	opustit	k5eAaPmF	opustit
zažité	zažitý	k2eAgInPc4d1	zažitý
názvoslovné	názvoslovný	k2eAgInPc4d1	názvoslovný
kodexy	kodex	k1gInPc4	kodex
vytvořené	vytvořený	k2eAgInPc4d1	vytvořený
pro	pro	k7c4	pro
speciální	speciální	k2eAgFnPc4d1	speciální
skupiny	skupina	k1gFnPc4	skupina
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
kód	kód	k1gInSc4	kód
nomenklatury	nomenklatura	k1gFnSc2	nomenklatura
řas	řasa	k1gFnPc2	řasa
<g/>
,	,	kIx,	,
hub	houba	k1gFnPc2	houba
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
kód	kód	k1gInSc4	kód
nomenklatury	nomenklatura	k1gFnSc2	nomenklatura
kulturních	kulturní	k2eAgFnPc2d1	kulturní
rostlin	rostlina	k1gFnPc2	rostlina
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
kód	kód	k1gInSc4	kód
zoologické	zoologický	k2eAgFnSc2d1	zoologická
nomenklatury	nomenklatura	k1gFnSc2	nomenklatura
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
kód	kód	k1gInSc1	kód
nomenklatury	nomenklatura	k1gFnSc2	nomenklatura
bakterií	bakterie	k1gFnPc2	bakterie
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
pojmenování	pojmenování	k1gNnSc4	pojmenování
patovarů	patovar	k1gInPc2	patovar
fytopatologických	fytopatologický	k2eAgFnPc2d1	fytopatologická
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
kód	kód	k1gInSc4	kód
pro	pro	k7c4	pro
klasifikaci	klasifikace	k1gFnSc4	klasifikace
a	a	k8xC	a
nomenklaturu	nomenklatura	k1gFnSc4	nomenklatura
virů	vir	k1gInPc2	vir
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
připraven	připravit	k5eAaPmNgMnS	připravit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
na	na	k7c4	na
sjednocený	sjednocený	k2eAgInSc4d1	sjednocený
kód	kód	k1gInSc4	kód
navazoval	navazovat	k5eAaImAgInS	navazovat
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
se	se	k3xPyFc4	se
u	u	k7c2	u
druhového	druhový	k2eAgNnSc2d1	druhové
názvosloví	názvosloví	k1gNnSc2	názvosloví
virů	vir	k1gInPc2	vir
nepoužívá	používat	k5eNaImIp3nS	používat
binominální	binominální	k2eAgNnSc4d1	binominální
pojmenování	pojmenování	k1gNnSc4	pojmenování
<g/>
.	.	kIx.	.
</s>
<s>
Připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
Biokód	Biokód	k1gInSc1	Biokód
si	se	k3xPyFc3	se
neklade	klást	k5eNaImIp3nS	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
ani	ani	k8xC	ani
částečně	částečně	k6eAd1	částečně
aplikovat	aplikovat	k5eAaBmF	aplikovat
některé	některý	k3yIgFnPc4	některý
zásady	zásada	k1gFnPc4	zásada
Fylokódu	Fylokód	k1gInSc2	Fylokód
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
názvoslovného	názvoslovný	k2eAgInSc2d1	názvoslovný
kódu	kód	k1gInSc2	kód
založeného	založený	k2eAgInSc2d1	založený
na	na	k7c6	na
kladistické	kladistický	k2eAgFnSc6d1	kladistická
metodě	metoda	k1gFnSc6	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Biologická	biologický	k2eAgFnSc1d1	biologická
klasifikace	klasifikace	k1gFnSc1	klasifikace
Zkratky	zkratka	k1gFnSc2	zkratka
binomické	binomický	k2eAgFnSc2d1	binomická
nomenklatury	nomenklatura	k1gFnSc2	nomenklatura
Vědecká	vědecký	k2eAgFnSc1d1	vědecká
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
Botanická	botanický	k2eAgFnSc1d1	botanická
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
Seznam	seznam	k1gInSc4	seznam
botaniků	botanik	k1gMnPc2	botanik
a	a	k8xC	a
mykologů	mykolog	k1gMnPc2	mykolog
dle	dle	k7c2	dle
zkratek	zkratka	k1gFnPc2	zkratka
</s>
