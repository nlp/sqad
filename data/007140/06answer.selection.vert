<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
pojmem	pojem	k1gInSc7	pojem
myslí	myslet	k5eAaImIp3nS	myslet
rychlost	rychlost	k1gFnSc1	rychlost
zvuku	zvuk	k1gInSc2	zvuk
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
atmosférických	atmosférický	k2eAgFnPc6d1	atmosférická
podmínkách	podmínka	k1gFnPc6	podmínka
-	-	kIx~	-
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
hodnotu	hodnota	k1gFnSc4	hodnota
má	mít	k5eAaImIp3nS	mít
teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
