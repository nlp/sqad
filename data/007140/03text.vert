<s>
Rychlost	rychlost	k1gFnSc1	rychlost
zvuku	zvuk	k1gInSc2	zvuk
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc7	jaký
se	se	k3xPyFc4	se
zvukové	zvukový	k2eAgFnSc2d1	zvuková
vlny	vlna	k1gFnSc2	vlna
šíří	šířit	k5eAaImIp3nP	šířit
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
pojmem	pojem	k1gInSc7	pojem
myslí	myslet	k5eAaImIp3nS	myslet
rychlost	rychlost	k1gFnSc1	rychlost
zvuku	zvuk	k1gInSc2	zvuk
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
atmosférických	atmosférický	k2eAgFnPc6d1	atmosférická
podmínkách	podmínka	k1gFnPc6	podmínka
-	-	kIx~	-
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
hodnotu	hodnota	k1gFnSc4	hodnota
má	mít	k5eAaImIp3nS	mít
teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ideálním	ideální	k2eAgInSc6d1	ideální
plynu	plyn	k1gInSc6	plyn
pro	pro	k7c4	pro
rychlost	rychlost	k1gFnSc4	rychlost
zvuku	zvuk	k1gInSc2	zvuk
platí	platit	k5eAaImIp3nS	platit
vzorec	vzorec	k1gInSc1	vzorec
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
o	o	k7c4	o
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
1	[number]	k4	1
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
γ	γ	k?	γ
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc1	kappa
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
o	o	k7c4	o
<g/>
}}}}}	}}}}}	k?	}}}}}
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
right	right	k1gMnSc1	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
tlak	tlak	k1gInSc1	tlak
plynu	plyn	k1gInSc2	plyn
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
příslušná	příslušný	k2eAgFnSc1d1	příslušná
hustota	hustota	k1gFnSc1	hustota
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
γ	γ	k?	γ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
}	}	kIx)	}
Teplotní	teplotní	k2eAgFnSc1d1	teplotní
rozpínavost	rozpínavost	k1gFnSc1	rozpínavost
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
plyn	plyn	k1gInSc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
změřit	změřit	k5eAaPmF	změřit
rychlost	rychlost	k1gFnSc4	rychlost
zvuku	zvuk	k1gInSc2	zvuk
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Marin	Marina	k1gFnPc2	Marina
Mersenne	Mersenn	k1gInSc5	Mersenn
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pokusech	pokus	k1gInPc6	pokus
s	s	k7c7	s
kanónem	kanón	k1gInSc7	kanón
naměřil	naměřit	k5eAaBmAgInS	naměřit
rychlost	rychlost	k1gFnSc4	rychlost
428	[number]	k4	428
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	kA	s
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
zvuku	zvuk	k1gInSc2	zvuk
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
poprvé	poprvé	k6eAd1	poprvé
přesně	přesně	k6eAd1	přesně
měřili	měřit	k5eAaImAgMnP	měřit
Jean-Daniel	Jean-Daniel	k1gMnSc1	Jean-Daniel
Colladon	Colladon	k1gMnSc1	Colladon
a	a	k8xC	a
Charles	Charles	k1gMnSc1	Charles
Sturm	Sturm	k1gInSc1	Sturm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ženevském	ženevský	k2eAgNnSc6d1	Ženevské
jezeře	jezero	k1gNnSc6	jezero
postavili	postavit	k5eAaPmAgMnP	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1827	[number]	k4	1827
dvě	dva	k4xCgFnPc4	dva
loďky	loďka	k1gFnPc1	loďka
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
13487	[number]	k4	13487
m.	m.	k?	m.
Speciální	speciální	k2eAgNnSc1d1	speciální
zařízení	zařízení	k1gNnSc1	zařízení
zároveň	zároveň	k6eAd1	zároveň
uhodilo	uhodit	k5eAaPmAgNnS	uhodit
do	do	k7c2	do
zvonu	zvon	k1gInSc2	zvon
<g/>
,	,	kIx,	,
ponořeného	ponořený	k2eAgInSc2d1	ponořený
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
odpálilo	odpálit	k5eAaPmAgNnS	odpálit
nálož	nálož	k1gFnSc4	nálož
střelného	střelný	k2eAgInSc2d1	střelný
prachu	prach	k1gInSc2	prach
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
loďce	loďka	k1gFnSc6	loďka
naměřil	naměřit	k5eAaBmAgMnS	naměřit
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
akustickým	akustický	k2eAgInSc7d1	akustický
a	a	k8xC	a
optickým	optický	k2eAgInSc7d1	optický
signálem	signál	k1gInSc7	signál
9,4	[number]	k4	9,4
s	s	k7c7	s
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
1435	[number]	k4	1435
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
U	u	k7c2	u
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
záleží	záležet	k5eAaImIp3nS	záležet
měření	měření	k1gNnSc1	měření
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
podélné	podélný	k2eAgNnSc4d1	podélné
vlnění	vlnění	k1gNnSc4	vlnění
v	v	k7c6	v
kompaktní	kompaktní	k2eAgFnSc6d1	kompaktní
hmotě	hmota	k1gFnSc6	hmota
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
příčné	příčný	k2eAgNnSc4d1	příčné
vlnění	vlnění	k1gNnSc4	vlnění
na	na	k7c6	na
tyči	tyč	k1gFnSc6	tyč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kompaktní	kompaktní	k2eAgFnSc6d1	kompaktní
hmotě	hmota	k1gFnSc6	hmota
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
vzorce	vzorec	k1gInSc2	vzorec
pro	pro	k7c4	pro
rychlost	rychlost	k1gFnSc4	rychlost
zvuku	zvuk	k1gInSc2	zvuk
v	v	k7c6	v
ideálním	ideální	k2eAgInSc6d1	ideální
plynu	plyn	k1gInSc6	plyn
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
rychlost	rychlost	k1gFnSc4	rychlost
zvuku	zvuk	k1gInSc2	zvuk
v	v	k7c6	v
suchém	suchý	k2eAgInSc6d1	suchý
vzduchu	vzduch	k1gInSc6	vzduch
platí	platit	k5eAaImIp3nS	platit
zhruba	zhruba	k6eAd1	zhruba
následující	následující	k2eAgInSc1d1	následující
vztah	vztah	k1gInSc1	vztah
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
=	=	kIx~	=
:	:	kIx,	:
(	(	kIx(	(
331	[number]	k4	331
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
57	[number]	k4	57
+	+	kIx~	+
0,607	[number]	k4	0,607
⋅	⋅	k?	⋅
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ms	ms	k?	ms
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
331	[number]	k4	331
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
57	[number]	k4	57
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
607	[number]	k4	607
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
right	right	k1gMnSc1	right
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
ms	ms	k?	ms
<g/>
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
t	t	k?	t
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
ve	v	k7c6	v
stupních	stupeň	k1gInPc6	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
zvuku	zvuk	k1gInSc2	zvuk
tedy	tedy	k9	tedy
závisí	záviset	k5eAaImIp3nS	záviset
jen	jen	k9	jen
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
na	na	k7c6	na
tlaku	tlak	k1gInSc6	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zjednodušený	zjednodušený	k2eAgInSc1d1	zjednodušený
vzorec	vzorec	k1gInSc1	vzorec
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
zanedbatelnou	zanedbatelný	k2eAgFnSc7d1	zanedbatelná
chybou	chyba	k1gFnSc7	chyba
použitelný	použitelný	k2eAgInSc1d1	použitelný
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
od	od	k7c2	od
−	−	k?	−
<g/>
°	°	k?	°
<g/>
C	C	kA	C
do	do	k7c2	do
100	[number]	k4	100
<g/>
°	°	k?	°
<g/>
C.	C.	kA	C.
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
přibližné	přibližný	k2eAgFnSc2d1	přibližná
rychlosti	rychlost	k1gFnSc2	rychlost
zvuku	zvuk	k1gInSc2	zvuk
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
<g/>
:	:	kIx,	:
K	k	k7c3	k
šíření	šíření	k1gNnSc3	šíření
zvuku	zvuk	k1gInSc2	zvuk
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
nějakého	nějaký	k3yIgNnSc2	nějaký
látkového	látkový	k2eAgNnSc2d1	látkové
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
takové	takový	k3xDgNnSc4	takový
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
jsou	být	k5eAaImIp3nP	být
nějaké	nějaký	k3yIgFnPc1	nějaký
částice	částice	k1gFnPc1	částice
-	-	kIx~	-
například	například	k6eAd1	například
částice	částice	k1gFnPc1	částice
plynů	plyn	k1gInPc2	plyn
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
zvuk	zvuk	k1gInSc1	zvuk
nešíří	šířit	k5eNaImIp3nS	šířit
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
ideálním	ideální	k2eAgInSc6d1	ideální
případě	případ	k1gInSc6	případ
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádné	žádný	k3yNgNnSc1	žádný
částice	částice	k1gFnSc1	částice
<g/>
.	.	kIx.	.
</s>
<s>
Akustický	akustický	k2eAgInSc1d1	akustický
třesk	třesk	k1gInSc1	třesk
–	–	k?	–
Jev	jev	k1gInSc4	jev
nastávající	nastávající	k2eAgInSc4d1	nastávající
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
rychlost	rychlost	k1gFnSc1	rychlost
zvuku	zvuk	k1gInSc2	zvuk
a	a	k8xC	a
rychlost	rychlost	k1gFnSc4	rychlost
zdroje	zdroj	k1gInSc2	zdroj
zvuku	zvuk	k1gInSc2	zvuk
rovnají	rovnat	k5eAaImIp3nP	rovnat
<g/>
.	.	kIx.	.
</s>
<s>
Machovo	Machův	k2eAgNnSc1d1	Machovo
číslo	číslo	k1gNnSc1	číslo
</s>
