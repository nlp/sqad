<p>
<s>
Pazourek	pazourek	k1gInSc1	pazourek
je	být	k5eAaImIp3nS	být
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
sedimentární	sedimentární	k2eAgFnSc1d1	sedimentární
kryptokrystalická	kryptokrystalický	k2eAgFnSc1d1	kryptokrystalická
forma	forma	k1gFnSc1	forma
křemene	křemen	k1gInSc2	křemen
zařazená	zařazený	k2eAgFnSc1d1	zařazená
mezi	mezi	k7c4	mezi
odrůdy	odrůda	k1gFnPc4	odrůda
chalcedonu	chalcedon	k1gInSc2	chalcedon
<g/>
.	.	kIx.	.
</s>
<s>
Pazourek	pazourek	k1gInSc1	pazourek
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
tmavošedý	tmavošedý	k2eAgInSc1d1	tmavošedý
<g/>
,	,	kIx,	,
modrý	modrý	k2eAgInSc1d1	modrý
<g/>
,	,	kIx,	,
černý	černý	k2eAgInSc1d1	černý
nebo	nebo	k8xC	nebo
temně	temně	k6eAd1	temně
hnědý	hnědý	k2eAgInSc1d1	hnědý
a	a	k8xC	a
často	často	k6eAd1	často
se	s	k7c7	s
skelným	skelný	k2eAgInSc7d1	skelný
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
pecek	pecka	k1gFnPc2	pecka
(	(	kIx(	(
<g/>
mandlí	mandle	k1gFnPc2	mandle
<g/>
,	,	kIx,	,
konkrecí	konkrecí	k2eAgFnSc1d1	konkrecí
<g/>
,	,	kIx,	,
uzlin	uzlina	k1gFnPc2	uzlina
<g/>
)	)	kIx)	)
i	i	k8xC	i
větších	veliký	k2eAgInPc2d2	veliký
objemů	objem	k1gInPc2	objem
uvnitř	uvnitř	k7c2	uvnitř
usazených	usazený	k2eAgFnPc2d1	usazená
(	(	kIx(	(
<g/>
sedimentárních	sedimentární	k2eAgFnPc2d1	sedimentární
<g/>
)	)	kIx)	)
hornin	hornina	k1gFnPc2	hornina
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
křídy	křída	k1gFnPc4	křída
a	a	k8xC	a
vápence	vápenec	k1gInPc4	vápenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
pazourku	pazourek	k1gInSc2	pazourek
==	==	k?	==
</s>
</p>
<p>
<s>
Způsob	způsob	k1gInSc1	způsob
vzniku	vznik	k1gInSc2	vznik
pazourku	pazourek	k1gInSc2	pazourek
není	být	k5eNaImIp3nS	být
dosud	dosud	k6eAd1	dosud
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
potvrzeno	potvrzen	k2eAgNnSc1d1	potvrzeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
chemických	chemický	k2eAgFnPc2d1	chemická
změn	změna	k1gFnPc2	změna
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
sedimentárních	sedimentární	k2eAgFnPc2d1	sedimentární
hornin	hornina	k1gFnPc2	hornina
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
–	–	k?	–
při	při	k7c6	při
procesu	proces	k1gInSc6	proces
diageneze	diageneze	k1gFnSc2	diageneze
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
teorie	teorie	k1gFnSc1	teorie
praví	pravit	k5eAaBmIp3nS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
gelovitý	gelovitý	k2eAgInSc1d1	gelovitý
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
materiál	materiál	k1gInSc1	materiál
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
dutiny	dutina	k1gFnPc4	dutina
v	v	k7c6	v
sedimentu	sediment	k1gInSc6	sediment
a	a	k8xC	a
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
"	"	kIx"	"
<g/>
zkřemení	zkřemenět	k5eAaPmIp3nP	zkřemenět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
vysvětlovalo	vysvětlovat	k5eAaImAgNnS	vysvětlovat
složité	složitý	k2eAgInPc4d1	složitý
tvary	tvar	k1gInPc4	tvar
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
přirozený	přirozený	k2eAgInSc4d1	přirozený
pazourek	pazourek	k1gInSc4	pazourek
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
pazourku	pazourek	k1gInSc2	pazourek
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
pazourek	pazourek	k1gInSc1	pazourek
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
nástrojů	nástroj	k1gInPc2	nástroj
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
v	v	k7c6	v
Obourgu	Obourg	k1gInSc6	Obourg
v	v	k7c6	v
pazourkových	pazourkový	k2eAgInPc6d1	pazourkový
dolech	dol	k1gInPc6	dol
Spiennes	Spiennes	k1gInSc1	Spiennes
<g/>
,	,	kIx,	,
v	v	k7c6	v
křídách	křída	k1gFnPc6	křída
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
průlivu	průliv	k1gInSc2	průliv
La	la	k1gNnSc2	la
Manche	Manch	k1gFnSc2	Manch
<g/>
,	,	kIx,	,
v	v	k7c6	v
Pařížské	pařížský	k2eAgFnSc6d1	Pařížská
pánvi	pánev	k1gFnSc6	pánev
<g/>
,	,	kIx,	,
v	v	k7c6	v
ložiskách	ložisko	k1gNnPc6	ložisko
na	na	k7c6	na
Rujáně	Rujána	k1gFnSc6	Rujána
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c6	na
mysu	mys	k1gInSc6	mys
Arkona	Arkona	k1gFnSc1	Arkona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
anglických	anglický	k2eAgInPc6d1	anglický
Grimes	Grimesa	k1gFnPc2	Grimesa
Graves	Graves	k1gInSc1	Graves
<g/>
,	,	kIx,	,
v	v	k7c6	v
dánských	dánský	k2eAgFnPc6d1	dánská
Mø	Mø	k1gFnPc6	Mø
Klint	Klinta	k1gFnPc2	Klinta
a	a	k8xC	a
v	v	k7c6	v
jurských	jurský	k2eAgFnPc6d1	jurská
vrstvách	vrstva	k1gFnPc6	vrstva
krakovské	krakovský	k2eAgFnSc2d1	Krakovská
oblasti	oblast	k1gFnSc2	oblast
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
pazourku	pazourek	k1gInSc2	pazourek
==	==	k?	==
</s>
</p>
<p>
<s>
Pazourek	pazourek	k1gInSc1	pazourek
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
relativně	relativně	k6eAd1	relativně
snadné	snadný	k2eAgNnSc1d1	snadné
opracování	opracování	k1gNnSc1	opracování
a	a	k8xC	a
lasturnatý	lasturnatý	k2eAgInSc1d1	lasturnatý
lom	lom	k1gInSc1	lom
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvíce	hodně	k6eAd3	hodně
užívaných	užívaný	k2eAgInPc2d1	užívaný
materiálů	materiál	k1gInPc2	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
kamenných	kamenný	k2eAgInPc2d1	kamenný
nástrojů	nástroj	k1gInPc2	nástroj
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kamenné	kamenný	k2eAgInPc1d1	kamenný
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zpracováván	zpracovávat	k5eAaImNgInS	zpracovávat
štípáním	štípání	k1gNnSc7	štípání
na	na	k7c4	na
tenké	tenký	k2eAgFnPc4d1	tenká
ostré	ostrý	k2eAgFnPc4d1	ostrá
štěpiny	štěpina	k1gFnPc4	štěpina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tvořily	tvořit	k5eAaImAgFnP	tvořit
ostří	ostří	k1gNnSc4	ostří
primitivních	primitivní	k2eAgInPc2d1	primitivní
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
hroty	hrot	k1gInPc4	hrot
šípů	šíp	k1gInPc2	šíp
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgMnS	používat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
pěstních	pěstní	k2eAgInPc2d1	pěstní
klínů	klín	k1gInPc2	klín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
pravěku	pravěk	k1gInSc6	pravěk
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
pazourek	pazourka	k1gFnPc2	pazourka
dostával	dostávat	k5eAaImAgMnS	dostávat
z	z	k7c2	z
pobřeží	pobřeží	k1gNnSc2	pobřeží
Baltu	Balt	k1gInSc2	Balt
<g/>
,	,	kIx,	,
posunem	posun	k1gInSc7	posun
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
má	mít	k5eAaImIp3nS	mít
přízvisko	přízvisko	k1gNnSc1	přízvisko
baltský	baltský	k2eAgMnSc1d1	baltský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
německý	německý	k2eAgMnSc1d1	německý
<g/>
)	)	kIx)	)
název	název	k1gInSc1	název
pazourku	pazourek	k1gInSc2	pazourek
–	–	k?	–
flint	flint	k1gInSc1	flint
–	–	k?	–
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
z	z	k7c2	z
obecného	obecný	k2eAgInSc2d1	obecný
názvu	název	k1gInSc2	název
pro	pro	k7c4	pro
pušku	puška	k1gFnSc4	puška
–	–	k?	–
flinta	flinta	k1gFnSc1	flinta
<g/>
.	.	kIx.	.
</s>
<s>
Pazourek	pazourek	k1gInSc1	pazourek
byl	být	k5eAaImAgInS	být
totiž	totiž	k9	totiž
užíván	užíván	k2eAgInSc1d1	užíván
jako	jako	k8xC	jako
křesací	křesací	k2eAgInSc1d1	křesací
kamínek	kamínek	k1gInSc1	kamínek
(	(	kIx(	(
<g/>
nárazem	náraz	k1gInSc7	náraz
oceli	ocel	k1gFnSc2	ocel
s	s	k7c7	s
pazourkem	pazourek	k1gInSc7	pazourek
vznikají	vznikat	k5eAaImIp3nP	vznikat
jiskry	jiskra	k1gFnPc1	jiskra
<g/>
)	)	kIx)	)
v	v	k7c6	v
puškách	puška	k1gFnPc6	puška
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
úlohou	úloha	k1gFnSc7	úloha
bylo	být	k5eAaImAgNnS	být
zažehnout	zažehnout	k5eAaPmF	zažehnout
střelný	střelný	k2eAgInSc4d1	střelný
prach	prach	k1gInSc4	prach
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vypálil	vypálit	k5eAaPmAgMnS	vypálit
střelu	střela	k1gFnSc4	střela
(	(	kIx(	(
<g/>
projektil	projektil	k1gInSc4	projektil
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vojenské	vojenský	k2eAgNnSc1d1	vojenské
užití	užití	k1gNnSc1	užití
pazourku	pazourek	k1gInSc2	pazourek
v	v	k7c6	v
mušketách	mušketa	k1gFnPc6	mušketa
britské	britský	k2eAgFnSc2d1	britská
armády	armáda	k1gFnSc2	armáda
je	být	k5eAaImIp3nS	být
doloženo	doložit	k5eAaPmNgNnS	doložit
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1842	[number]	k4	1842
<g/>
.	.	kIx.	.
</s>
<s>
Skláři	sklář	k1gMnPc1	sklář
<g/>
,	,	kIx,	,
optici	optik	k1gMnPc1	optik
a	a	k8xC	a
astronomové	astronom	k1gMnPc1	astronom
taky	taky	k6eAd1	taky
znají	znát	k5eAaImIp3nP	znát
flintové	flintový	k2eAgNnSc4d1	flintové
sklo	sklo	k1gNnSc4	sklo
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
indexem	index	k1gInSc7	index
lomu	lom	k1gInSc2	lom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Baltské	baltský	k2eAgNnSc1d1	Baltské
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
Rohovec	rohovec	k1gInSc1	rohovec
</s>
</p>
<p>
<s>
Septárie	Septárie	k1gFnSc1	Septárie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pazourek	pazourka	k1gFnPc2	pazourka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
pazourek	pazourka	k1gFnPc2	pazourka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pazourek	pazourka	k1gFnPc2	pazourka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Aleš	Aleš	k1gMnSc1	Aleš
Uhlíř	Uhlíř	k1gMnSc1	Uhlíř
<g/>
,	,	kIx,	,
Pazourek	pazourek	k1gInSc1	pazourek
nejen	nejen	k6eAd1	nejen
jako	jako	k9	jako
pravěká	pravěký	k2eAgFnSc1d1	pravěká
nástrojová	nástrojový	k2eAgFnSc1d1	nástrojová
surovina	surovina	k1gFnSc1	surovina
</s>
</p>
<p>
<s>
Aleš	Aleš	k1gMnSc1	Aleš
Uhlíř	Uhlíř	k1gMnSc1	Uhlíř
<g/>
,	,	kIx,	,
Pazourek	pazourek	k1gInSc1	pazourek
-	-	kIx~	-
kámen	kámen	k1gInSc1	kámen
všech	všecek	k3xTgInPc2	všecek
kamenů	kámen	k1gInPc2	kámen
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
na	na	k7c4	na
pazourek	pazourek	k1gInSc4	pazourek
-	-	kIx~	-
stránky	stránka	k1gFnPc4	stránka
o	o	k7c6	o
technikách	technika	k1gFnPc6	technika
štípání	štípání	k1gNnSc2	štípání
pazourku	pazourka	k1gFnSc4	pazourka
prvního	první	k4xOgMnSc2	první
českého	český	k2eAgMnSc2d1	český
profesionálního	profesionální	k2eAgMnSc2d1	profesionální
flintknappera	flintknapper	k1gMnSc2	flintknapper
Petra	Petr	k1gMnSc2	Petr
Zítky	zítka	k1gMnSc2	zítka
</s>
</p>
