<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Martinů	Martinů	k1gMnSc1	Martinů
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1890	[number]	k4	1890
Polička	Polička	k1gFnSc1	Polička
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1959	[number]	k4	1959
Liestal	Liestal	k1gFnPc2	Liestal
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
světově	světově	k6eAd1	světově
proslulý	proslulý	k2eAgMnSc1d1	proslulý
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
hudební	hudební	k2eAgFnSc2d1	hudební
moderny	moderna	k1gFnSc2	moderna
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
umělecký	umělecký	k2eAgInSc1d1	umělecký
vývoj	vývoj	k1gInSc1	vývoj
prošel	projít	k5eAaPmAgInS	projít
od	od	k7c2	od
impresionismu	impresionismus	k1gInSc2	impresionismus
přes	přes	k7c4	přes
neoklasicismus	neoklasicismus	k1gInSc4	neoklasicismus
<g/>
,	,	kIx,	,
expresionismus	expresionismus	k1gInSc4	expresionismus
a	a	k8xC	a
inspiraci	inspirace	k1gFnSc4	inspirace
jazzem	jazz	k1gInSc7	jazz
až	až	k8xS	až
k	k	k7c3	k
ryze	ryze	k6eAd1	ryze
vlastnímu	vlastní	k2eAgInSc3d1	vlastní
kompozičnímu	kompoziční	k2eAgInSc3d1	kompoziční
stylu	styl	k1gInSc3	styl
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
poličského	poličský	k2eAgMnSc2d1	poličský
obuvníka	obuvník	k1gMnSc2	obuvník
a	a	k8xC	a
pověžného	pověžný	k1gMnSc2	pověžný
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Martinů	Martinů	k1gMnSc2	Martinů
<g/>
,	,	kIx,	,
narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
věžní	věžní	k2eAgFnSc6d1	věžní
místnosti	místnost	k1gFnSc6	místnost
zdejšího	zdejší	k2eAgInSc2d1	zdejší
chrámu	chrám	k1gInSc2	chrám
svatého	svatý	k2eAgMnSc2d1	svatý
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1906	[number]	k4	1906
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
prošel	projít	k5eAaPmAgMnS	projít
několika	několik	k4yIc3	několik
odděleními	oddělení	k1gNnPc7	oddělení
-	-	kIx~	-
studoval	studovat	k5eAaImAgMnS	studovat
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
varhany	varhany	k1gFnPc4	varhany
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
houslista	houslista	k1gMnSc1	houslista
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
filharmonii	filharmonie	k1gFnSc6	filharmonie
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
ale	ale	k8xC	ale
skladbu	skladba	k1gFnSc4	skladba
studoval	studovat	k5eAaImAgMnS	studovat
soukromě	soukromě	k6eAd1	soukromě
<g/>
;	;	kIx,	;
ovlivněn	ovlivnit	k5eAaPmNgMnS	ovlivnit
českou	český	k2eAgFnSc7d1	Česká
hudební	hudební	k2eAgFnSc7d1	hudební
tradicí	tradice	k1gFnSc7	tradice
<g/>
,	,	kIx,	,
impresionismem	impresionismus	k1gInSc7	impresionismus
Claude	Claud	k1gInSc5	Claud
Debussyho	Debussy	k1gMnSc2	Debussy
a	a	k8xC	a
literární	literární	k2eAgFnSc7d1	literární
dekadencí	dekadence	k1gFnSc7	dekadence
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
když	když	k8xS	když
měl	mít	k5eAaImAgMnS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
první	první	k4xOgFnPc4	první
závažné	závažný	k2eAgFnPc4d1	závažná
kompozice	kompozice	k1gFnPc4	kompozice
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
žákem	žák	k1gMnSc7	žák
Josefa	Josef	k1gMnSc2	Josef
Suka	Suk	k1gMnSc2	Suk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
žil	žít	k5eAaImAgMnS	žít
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Martinů	Martinů	k1gMnSc1	Martinů
trvale	trvale	k6eAd1	trvale
mimo	mimo	k7c4	mimo
vlast	vlast	k1gFnSc4	vlast
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Charlotte	Charlott	k1gInSc5	Charlott
Quennehen	Quennehna	k1gFnPc2	Quennehna
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
-	-	kIx~	-
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzali	vzít	k5eAaPmAgMnP	vzít
se	s	k7c7	s
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
budoucí	budoucí	k2eAgFnSc7d1	budoucí
ženou	žena	k1gFnSc7	žena
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
v	v	k7c6	v
cirkuse	cirkus	k1gInSc6	cirkus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
představení	představení	k1gNnSc6	představení
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
zašel	zajít	k5eAaPmAgMnS	zajít
a	a	k8xC	a
beze	beze	k7c2	beze
slova	slovo	k1gNnSc2	slovo
jí	jíst	k5eAaImIp3nS	jíst
strčil	strčit	k5eAaPmAgMnS	strčit
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
papírek	papírek	k1gInSc4	papírek
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
adresou	adresa	k1gFnSc7	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
v	v	k7c6	v
knížce	knížka	k1gFnSc6	knížka
Můj	můj	k3xOp1gInSc4	můj
život	život	k1gInSc4	život
s	s	k7c7	s
Bohuslavem	Bohuslav	k1gMnSc7	Bohuslav
Martinů	Martinů	k1gMnSc7	Martinů
napsala	napsat	k5eAaPmAgFnS	napsat
že	že	k8xS	že
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgInS	být
jeho	on	k3xPp3gInSc4	on
"	"	kIx"	"
<g/>
nejodvážnější	odvážný	k2eAgInSc4d3	nejodvážnější
čin	čin	k1gInSc4	čin
jaký	jaký	k9	jaký
kdy	kdy	k6eAd1	kdy
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
u	u	k7c2	u
skladatele	skladatel	k1gMnSc2	skladatel
Alberta	Albert	k1gMnSc2	Albert
Roussela	Roussel	k1gMnSc2	Roussel
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
francouzské	francouzský	k2eAgNnSc4d1	francouzské
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
jeho	jeho	k3xOp3gInSc4	jeho
řád	řád	k1gInSc4	řád
<g/>
,	,	kIx,	,
čistotu	čistota	k1gFnSc4	čistota
<g/>
,	,	kIx,	,
rovnováhu	rovnováha	k1gFnSc4	rovnováha
a	a	k8xC	a
vytříbený	vytříbený	k2eAgInSc4d1	vytříbený
vkus	vkus	k1gInSc4	vkus
<g/>
.	.	kIx.	.
</s>
<s>
Kosmopolitní	kosmopolitní	k2eAgFnSc1d1	kosmopolitní
Paříž	Paříž	k1gFnSc1	Paříž
se	se	k3xPyFc4	se
však	však	k9	však
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Impresionismus	impresionismus	k1gInSc1	impresionismus
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
nevládl	vládnout	k5eNaImAgMnS	vládnout
hudební	hudební	k2eAgFnSc3d1	hudební
scéně	scéna	k1gFnSc3	scéna
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
hudba	hudba	k1gFnSc1	hudba
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
šestky	šestka	k1gFnSc2	šestka
<g/>
,	,	kIx,	,
jazz	jazz	k1gInSc4	jazz
a	a	k8xC	a
především	především	k9	především
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mu	on	k3xPp3gMnSc3	on
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
do	do	k7c2	do
hudebního	hudební	k2eAgNnSc2d1	hudební
umění	umění	k1gNnSc2	umění
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
zpracovány	zpracovat	k5eAaPmNgInP	zpracovat
folklórní	folklórní	k2eAgInPc1d1	folklórní
motivy	motiv	k1gInPc1	motiv
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jej	on	k3xPp3gMnSc4	on
přivedlo	přivést	k5eAaPmAgNnS	přivést
ke	k	k7c3	k
komponování	komponování	k1gNnSc3	komponování
skladeb	skladba	k1gFnPc2	skladba
inspirovaných	inspirovaný	k2eAgFnPc2d1	inspirovaná
lidovou	lidový	k2eAgFnSc7d1	lidová
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
pantomimickým	pantomimický	k2eAgInSc7d1	pantomimický
baletem	balet	k1gInSc7	balet
Špalíček	špalíček	k1gInSc1	špalíček
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
jako	jako	k9	jako
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
i	i	k8xC	i
on	on	k3xPp3gMnSc1	on
záhy	záhy	k6eAd1	záhy
a	a	k8xC	a
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
koketoval	koketovat	k5eAaImAgMnS	koketovat
s	s	k7c7	s
jazzem	jazz	k1gInSc7	jazz
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Jazzové	jazzový	k2eAgFnSc6d1	jazzová
suitě	suita	k1gFnSc6	suita
a	a	k8xC	a
baletu	balet	k1gInSc6	balet
Kuchyňská	kuchyňská	k1gFnSc1	kuchyňská
revue	revue	k1gFnSc1	revue
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
Tři	tři	k4xCgFnPc4	tři
přání	přání	k1gNnPc2	přání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
obrátil	obrátit	k5eAaPmAgInS	obrátit
svou	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
ke	k	k7c3	k
komorní	komorní	k2eAgFnSc3d1	komorní
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýrazněji	výrazně	k6eAd3	výrazně
jej	on	k3xPp3gMnSc4	on
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
novoklasicismus	novoklasicismus	k1gInSc1	novoklasicismus
s	s	k7c7	s
hudební	hudební	k2eAgFnSc7d1	hudební
formou	forma	k1gFnSc7	forma
concerta	concert	k1gMnSc2	concert
grossa	gross	k1gMnSc2	gross
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgMnPc1d1	pocházející
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
nových	nový	k2eAgNnPc2d1	nové
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
podepsání	podepsání	k1gNnSc2	podepsání
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jej	on	k3xPp3gMnSc4	on
trvale	trvale	k6eAd1	trvale
oddělila	oddělit	k5eAaPmAgFnS	oddělit
od	od	k7c2	od
rodné	rodný	k2eAgFnSc2d1	rodná
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
zkomponoval	zkomponovat	k5eAaPmAgInS	zkomponovat
i	i	k9	i
přes	přes	k7c4	přes
velké	velký	k2eAgNnSc4d1	velké
pracovní	pracovní	k2eAgNnSc4d1	pracovní
vytížení	vytížení	k1gNnSc4	vytížení
Polní	polní	k2eAgFnSc4d1	polní
mši	mše	k1gFnSc4	mše
na	na	k7c4	na
texty	text	k1gInPc4	text
Jiřího	Jiří	k1gMnSc2	Jiří
Muchy	Mucha	k1gMnSc2	Mucha
a	a	k8xC	a
žalmů	žalm	k1gInPc2	žalm
<g/>
.	.	kIx.	.
</s>
<s>
Odkryl	odkrýt	k5eAaPmAgMnS	odkrýt
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
své	svůj	k3xOyFgFnPc4	svůj
hluboké	hluboký	k2eAgFnPc4d1	hluboká
emoce	emoce	k1gFnPc4	emoce
a	a	k8xC	a
bolest	bolest	k1gFnSc4	bolest
i	i	k9	i
přes	přes	k7c4	přes
zdánlivě	zdánlivě	k6eAd1	zdánlivě
nezaujatý	zaujatý	k2eNgInSc4d1	nezaujatý
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
politické	politický	k2eAgFnSc3d1	politická
situaci	situace	k1gFnSc3	situace
<g/>
.	.	kIx.	.
</s>
<s>
Výmluvné	výmluvný	k2eAgNnSc1d1	výmluvné
je	být	k5eAaImIp3nS	být
věnování	věnování	k1gNnSc1	věnování
Československým	československý	k2eAgMnPc3d1	československý
dobrovolníkům	dobrovolník	k1gMnPc3	dobrovolník
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
klíčovým	klíčový	k2eAgInSc7d1	klíčový
dílem	díl	k1gInSc7	díl
třicátých	třicátý	k4xOgInPc2	třicátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
"	"	kIx"	"
<g/>
opera	opera	k1gFnSc1	opera
snů	sen	k1gInPc2	sen
<g/>
"	"	kIx"	"
Julietta	Julietta	k1gMnSc1	Julietta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	dílo	k1gNnSc6	dílo
opouští	opouštět	k5eAaImIp3nS	opouštět
svůj	svůj	k3xOyFgInSc4	svůj
geometrický	geometrický	k2eAgInSc4d1	geometrický
novoklasicistní	novoklasicistní	k2eAgInSc4d1	novoklasicistní
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prozkoumal	prozkoumat	k5eAaPmAgInS	prozkoumat
iracionální	iracionální	k2eAgInSc1d1	iracionální
svět	svět	k1gInSc1	svět
představ	představa	k1gFnPc2	představa
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
na	na	k7c4	na
fantazii	fantazie	k1gFnSc4	fantazie
založenou	založený	k2eAgFnSc4d1	založená
"	"	kIx"	"
<g/>
snovou	snový	k2eAgFnSc7d1	snová
logikou	logika	k1gFnSc7	logika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
se	s	k7c7	s
stále	stále	k6eAd1	stále
unikající	unikající	k2eAgFnSc7d1	unikající
krásnou	krásný	k2eAgFnSc7d1	krásná
Juliettou	Julietta	k1gFnSc7	Julietta
<g/>
,	,	kIx,	,
použil	použít	k5eAaPmAgMnS	použít
harmonický	harmonický	k2eAgInSc4d1	harmonický
postup	postup	k1gInSc4	postup
moravské	moravský	k2eAgFnSc2d1	Moravská
modulace	modulace	k1gFnSc2	modulace
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
v	v	k7c6	v
martinůovské	martinůovský	k2eAgFnSc6d1	martinůovský
literatuře	literatura	k1gFnSc6	literatura
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Juliettin	Juliettin	k2eAgInSc4d1	Juliettin
spoj	spoj	k1gInSc4	spoj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
spoj	spoj	k1gInSc1	spoj
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
např.	např.	kA	např.
i	i	k9	i
v	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
části	část	k1gFnSc6	část
Tarase	taras	k1gInSc6	taras
Bulby	bulbus	k1gInPc7	bulbus
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgInSc7d1	velký
momentem	moment	k1gInSc7	moment
jeho	jeho	k3xOp3gFnPc2	jeho
pozdních	pozdní	k2eAgFnPc2d1	pozdní
skladeb	skladba	k1gFnPc2	skladba
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
ideé	ideé	k1gNnSc1	ideé
fixe	fix	k1gInSc5	fix
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Spoj	spoj	k1gFnSc1	spoj
se	se	k3xPyFc4	se
ve	v	k7c6	v
skladbách	skladba	k1gFnPc6	skladba
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
příliš	příliš	k6eAd1	příliš
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svou	svůj	k3xOyFgFnSc7	svůj
nápadností	nápadnost	k1gFnSc7	nápadnost
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
strhává	strhávat	k5eAaImIp3nS	strhávat
pozornost	pozornost	k1gFnSc1	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
pak	pak	k6eAd1	pak
usiloval	usilovat	k5eAaImAgInS	usilovat
o	o	k7c4	o
nový	nový	k2eAgInSc4d1	nový
lyrismus	lyrismus	k1gInSc4	lyrismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nebyl	být	k5eNaImAgInS	být
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
soudobé	soudobý	k2eAgFnSc6d1	soudobá
hudbě	hudba	k1gFnSc6	hudba
nijak	nijak	k6eAd1	nijak
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc2	období
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
nabýval	nabývat	k5eAaImAgInS	nabývat
fantazijní	fantazijní	k2eAgInSc1d1	fantazijní
prvek	prvek	k1gInSc1	prvek
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc2d2	veliký
důležitosti	důležitost	k1gFnSc2	důležitost
<g/>
.	.	kIx.	.
</s>
<s>
Symfonie	symfonie	k1gFnSc1	symfonie
z	z	k7c2	z
válečného	válečný	k2eAgNnSc2d1	válečné
období	období	k1gNnSc2	období
komponované	komponovaný	k2eAgFnSc2d1	komponovaná
pro	pro	k7c4	pro
americké	americký	k2eAgInPc4d1	americký
orchestry	orchestr	k1gInPc4	orchestr
jsou	být	k5eAaImIp3nP	být
pozoruhodné	pozoruhodný	k2eAgInPc4d1	pozoruhodný
svou	svůj	k3xOyFgFnSc7	svůj
spontánností	spontánnost	k1gFnSc7	spontánnost
<g/>
,	,	kIx,	,
logickým	logický	k2eAgNnSc7d1	logické
uspořádáním	uspořádání	k1gNnSc7	uspořádání
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
rytmickou	rytmický	k2eAgFnSc7d1	rytmická
svěžestí	svěžest	k1gFnSc7	svěžest
a	a	k8xC	a
silně	silně	k6eAd1	silně
synkopovanou	synkopovaný	k2eAgFnSc7d1	synkopovaná
melodií	melodie	k1gFnSc7	melodie
<g/>
.	.	kIx.	.
</s>
<s>
Vzácnou	vzácný	k2eAgFnSc4d1	vzácná
harmonickou	harmonický	k2eAgFnSc4d1	harmonická
čistotu	čistota	k1gFnSc4	čistota
a	a	k8xC	a
především	především	k6eAd1	především
nedefinovatelnou	definovatelný	k2eNgFnSc7d1	nedefinovatelná
"	"	kIx"	"
<g/>
českou	český	k2eAgFnSc7d1	Česká
aurou	aura	k1gFnSc7	aura
<g/>
"	"	kIx"	"
připomíná	připomínat	k5eAaImIp3nS	připomínat
pozdní	pozdní	k2eAgFnSc4d1	pozdní
tvorbu	tvorba	k1gFnSc4	tvorba
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Francie	Francie	k1gFnSc2	Francie
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgInS	zůstat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
(	(	kIx(	(
<g/>
s	s	k7c7	s
návratem	návrat	k1gInSc7	návrat
v	v	k7c6	v
období	období	k1gNnSc6	období
1955	[number]	k4	1955
<g/>
-	-	kIx~	-
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
získal	získat	k5eAaPmAgMnS	získat
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
odvahu	odvaha	k1gFnSc4	odvaha
a	a	k8xC	a
našel	najít	k5eAaPmAgInS	najít
i	i	k9	i
technické	technický	k2eAgInPc4d1	technický
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mu	on	k3xPp3gMnSc3	on
umožnily	umožnit	k5eAaPmAgFnP	umožnit
vytvářet	vytvářet	k5eAaImF	vytvářet
díla	dílo	k1gNnPc4	dílo
plná	plný	k2eAgFnSc1d1	plná
fantazie	fantazie	k1gFnSc1	fantazie
a	a	k8xC	a
nespoutaného	spoutaný	k2eNgInSc2d1	nespoutaný
výrazu	výraz	k1gInSc2	výraz
i	i	k9	i
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
smyslu	smysl	k1gInSc2	smysl
pro	pro	k7c4	pro
formu	forma	k1gFnSc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
umělecký	umělecký	k2eAgInSc1d1	umělecký
přístup	přístup	k1gInSc1	přístup
obohatil	obohatit	k5eAaPmAgInS	obohatit
jeho	jeho	k3xOp3gFnPc4	jeho
pozdní	pozdní	k2eAgFnPc4d1	pozdní
velké	velký	k2eAgFnPc4d1	velká
kompozice	kompozice	k1gFnPc4	kompozice
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Symfonické	symfonický	k2eAgFnPc1d1	symfonická
fantazie	fantazie	k1gFnPc1	fantazie
(	(	kIx(	(
<g/>
Symfonie	symfonie	k1gFnSc1	symfonie
č.	č.	k?	č.
6	[number]	k4	6
<g/>
)	)	kIx)	)
a	a	k8xC	a
průzračné	průzračný	k2eAgNnSc1d1	průzračné
dílo	dílo	k1gNnSc1	dílo
Fresky	freska	k1gFnSc2	freska
Pierra	Pierr	k1gMnSc2	Pierr
della	dell	k1gMnSc2	dell
Francesca	Francescus	k1gMnSc2	Francescus
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
kultivovaná	kultivovaný	k2eAgFnSc1d1	kultivovaná
neoimpresionistická	neoimpresionistický	k2eAgFnSc1d1	neoimpresionistický
struktura	struktura	k1gFnSc1	struktura
předchází	předcházet	k5eAaImIp3nS	předcházet
principy	princip	k1gInPc4	princip
aleatoriky	aleatorika	k1gFnSc2	aleatorika
a	a	k8xC	a
navozuje	navozovat	k5eAaImIp3nS	navozovat
nekonečný	konečný	k2eNgInSc1d1	nekonečný
svět	svět	k1gInSc1	svět
imaginace	imaginace	k1gFnSc2	imaginace
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
díly	díl	k1gInPc7	díl
komponoval	komponovat	k5eAaImAgMnS	komponovat
řadu	řada	k1gFnSc4	řada
skladeb	skladba	k1gFnPc2	skladba
s	s	k7c7	s
lidovými	lidový	k2eAgInPc7d1	lidový
náměty	námět	k1gInPc7	námět
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
pozdravy	pozdrav	k1gInPc1	pozdrav
domů	dům	k1gInPc2	dům
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ilustrují	ilustrovat	k5eAaBmIp3nP	ilustrovat
jednoduchost	jednoduchost	k1gFnSc4	jednoduchost
a	a	k8xC	a
neobyčejnou	obyčejný	k2eNgFnSc4d1	neobyčejná
čistotu	čistota	k1gFnSc4	čistota
výrazu	výraz	k1gInSc2	výraz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
přehnané	přehnaný	k2eAgFnSc2d1	přehnaná
a	a	k8xC	a
závažné	závažný	k2eAgFnSc2d1	závažná
sentimentality	sentimentalita	k1gFnSc2	sentimentalita
začínal	začínat	k5eAaImAgInS	začínat
znovu	znovu	k6eAd1	znovu
obracet	obracet	k5eAaImF	obracet
k	k	k7c3	k
základním	základní	k2eAgFnPc3d1	základní
otázkám	otázka	k1gFnPc3	otázka
lidské	lidský	k2eAgFnSc2d1	lidská
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tendence	tendence	k1gFnSc1	tendence
je	být	k5eAaImIp3nS	být
zřejmá	zřejmý	k2eAgFnSc1d1	zřejmá
v	v	k7c6	v
Řeckých	řecký	k2eAgFnPc6d1	řecká
Pašijích	pašije	k1gFnPc6	pašije
a	a	k8xC	a
v	v	k7c6	v
"	"	kIx"	"
<g/>
Eposu	epos	k1gInSc6	epos
o	o	k7c6	o
Gilgamešovi	Gilgameš	k1gMnSc6	Gilgameš
<g/>
"	"	kIx"	"
a	a	k8xC	a
především	především	k9	především
v	v	k7c6	v
instrumentálních	instrumentální	k2eAgFnPc6d1	instrumentální
skladbách	skladba	k1gFnPc6	skladba
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
Paraboly	parabola	k1gFnPc4	parabola
<g/>
"	"	kIx"	"
a	a	k8xC	a
Koncert	koncert	k1gInSc1	koncert
č.	č.	k?	č.
4	[number]	k4	4
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
"	"	kIx"	"
<g/>
Inkantace	Inkantace	k1gFnSc1	Inkantace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdním	pozdní	k2eAgNnSc6d1	pozdní
období	období	k1gNnSc6	období
vyslovil	vyslovit	k5eAaPmAgInS	vyslovit
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pro	pro	k7c4	pro
posluchače	posluchač	k1gMnPc4	posluchač
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
nabývá	nabývat	k5eAaImIp3nS	nabývat
na	na	k7c6	na
významu	význam	k1gInSc6	význam
<g/>
:	:	kIx,	:
Umělec	umělec	k1gMnSc1	umělec
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
ptá	ptat	k5eAaImIp3nS	ptat
po	po	k7c6	po
smyslu	smysl	k1gInSc6	smysl
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgNnSc2	svůj
vlastního	vlastní	k2eAgNnSc2d1	vlastní
i	i	k8xC	i
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
hledá	hledat	k5eAaImIp3nS	hledat
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
neurčitosti	neurčitost	k1gFnSc2	neurčitost
otevírá	otevírat	k5eAaImIp3nS	otevírat
naše	náš	k3xOp1gInPc4	náš
každodenní	každodenní	k2eAgInPc4d1	každodenní
životy	život	k1gInPc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
mechanizace	mechanizace	k1gFnSc2	mechanizace
a	a	k8xC	a
uniformity	uniformita	k1gFnSc2	uniformita
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
impulsem	impuls	k1gInSc7	impuls
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
protestoval	protestovat	k5eAaBmAgMnS	protestovat
a	a	k8xC	a
umělec	umělec	k1gMnSc1	umělec
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
tento	tento	k3xDgInSc4	tento
protest	protest	k1gInSc4	protest
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
na	na	k7c6	na
Berkshire	Berkshir	k1gInSc5	Berkshir
Music	Musice	k1gFnPc2	Musice
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
těžký	těžký	k2eAgInSc1d1	těžký
úraz	úraz	k1gInSc1	úraz
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc4	jehož
následky	následek	k1gInPc4	následek
pociťoval	pociťovat	k5eAaImAgMnS	pociťovat
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tragické	tragický	k2eAgFnSc6d1	tragická
smrti	smrt	k1gFnSc6	smrt
Jana	Jan	k1gMnSc2	Jan
Masaryka	Masaryk	k1gMnSc2	Masaryk
opustil	opustit	k5eAaPmAgMnS	opustit
od	od	k7c2	od
myšlenky	myšlenka	k1gFnSc2	myšlenka
na	na	k7c4	na
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
emigraci	emigrace	k1gFnSc6	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
skladby	skladba	k1gFnSc2	skladba
na	na	k7c4	na
Princeton	Princeton	k1gInSc4	Princeton
University	universita	k1gFnSc2	universita
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
-	-	kIx~	-
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
na	na	k7c4	na
Mannes	Mannes	k1gInSc4	Mannes
College	Colleg	k1gFnSc2	Colleg
of	of	k?	of
Music	Music	k1gMnSc1	Music
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
období	období	k1gNnSc6	období
1948	[number]	k4	1948
-	-	kIx~	-
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
získal	získat	k5eAaPmAgMnS	získat
americké	americký	k2eAgNnSc4d1	americké
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
uděleno	udělit	k5eAaPmNgNnS	udělit
stipendium	stipendium	k1gNnSc4	stipendium
od	od	k7c2	od
Guggenheimovy	Guggenheimův	k2eAgFnSc2d1	Guggenheimova
nadace	nadace	k1gFnSc2	nadace
<g/>
,	,	kIx,	,
opustil	opustit	k5eAaPmAgMnS	opustit
USA	USA	kA	USA
a	a	k8xC	a
s	s	k7c7	s
Charlotte	Charlott	k1gInSc5	Charlott
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgInS	usadit
v	v	k7c6	v
Nice	Nice	k1gFnSc6	Nice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
žil	žít	k5eAaImAgMnS	žít
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
členem	člen	k1gInSc7	člen
amerického	americký	k2eAgMnSc2d1	americký
National	National	k1gMnSc2	National
Institute	institut	k1gInSc5	institut
of	of	k?	of
Arts	Arts	k1gInSc1	Arts
and	and	k?	and
Letters	Letters	k1gInSc1	Letters
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
na	na	k7c4	na
Curtis	Curtis	k1gFnSc4	Curtis
Institute	institut	k1gInSc5	institut
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
a	a	k8xC	a
opět	opět	k6eAd1	opět
na	na	k7c4	na
Mannes	Mannes	k1gInSc4	Mannes
School	Schoola	k1gFnPc2	Schoola
of	of	k?	of
Music	Music	k1gMnSc1	Music
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1956	[number]	k4	1956
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
composer	composer	k1gInSc1	composer
in	in	k?	in
residence	residence	k1gFnSc2	residence
<g/>
"	"	kIx"	"
na	na	k7c6	na
Americké	americký	k2eAgFnSc6d1	americká
akademii	akademie	k1gFnSc6	akademie
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
opět	opět	k6eAd1	opět
obdržel	obdržet	k5eAaPmAgMnS	obdržet
stipendium	stipendium	k1gNnSc4	stipendium
od	od	k7c2	od
Guggenheimovy	Guggenheimův	k2eAgFnSc2d1	Guggenheimova
nadace	nadace	k1gFnSc2	nadace
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
a	a	k8xC	a
Maja	Maja	k1gFnSc1	Maja
Sacherovi	Sacherovi	k1gRnPc1	Sacherovi
zvou	zvát	k5eAaImIp3nP	zvát
Bohuslava	Bohuslav	k1gMnSc4	Bohuslav
Martinů	Martinů	k1gFnSc1	Martinů
na	na	k7c4	na
Schönenberg	Schönenberg	k1gInSc4	Schönenberg
poblíž	poblíž	k7c2	poblíž
Basileje	Basilej	k1gFnSc2	Basilej
<g/>
,	,	kIx,	,
od	od	k7c2	od
září	září	k1gNnSc2	září
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
s	s	k7c7	s
Charlotte	Charlott	k1gInSc5	Charlott
usídlili	usídlit	k5eAaPmAgMnP	usídlit
natrvalo	natrvalo	k6eAd1	natrvalo
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1959	[number]	k4	1959
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgNnSc6d1	švýcarské
Liestalu	Liestal	k1gMnSc3	Liestal
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
vlastního	vlastní	k2eAgNnSc2d1	vlastní
přání	přání	k1gNnSc2	přání
<g/>
,	,	kIx,	,
na	na	k7c6	na
Sacherově	Sacherův	k2eAgInSc6d1	Sacherův
pozemku	pozemek	k1gInSc6	pozemek
na	na	k7c6	na
Schönenbergu	Schönenberg	k1gInSc6	Schönenberg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
převezeny	převezen	k2eAgInPc1d1	převezen
do	do	k7c2	do
rodné	rodný	k2eAgFnSc2d1	rodná
Poličky	Polička	k1gFnSc2	Polička
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
vedle	vedle	k7c2	vedle
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
Charlotty	Charlotta	k1gFnSc2	Charlotta
<g/>
,	,	kIx,	,
zemřelé	zemřelý	k2eAgFnSc2d1	zemřelá
v	v	k7c6	v
předcházejícím	předcházející	k2eAgInSc6d1	předcházející
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
také	také	k9	také
zbudován	zbudován	k2eAgInSc1d1	zbudován
jeho	jeho	k3xOp3gInSc1	jeho
památník	památník	k1gInSc1	památník
a	a	k8xC	a
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
žáky	žák	k1gMnPc7	žák
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Martinů	Martinů	k1gMnSc2	Martinů
vynikli	vyniknout	k5eAaPmAgMnP	vyniknout
zvláště	zvláště	k6eAd1	zvláště
Alan	Alan	k1gMnSc1	Alan
Hovhaness	Hovhanessa	k1gFnPc2	Hovhanessa
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
-	-	kIx~	-
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Herbert	Herbert	k1gMnSc1	Herbert
Owen	Owen	k1gMnSc1	Owen
Reed	Reed	k1gMnSc1	Reed
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
-	-	kIx~	-
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Novák	Novák	k1gMnSc1	Novák
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
-	-	kIx~	-
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vítězslava	Vítězslava	k1gFnSc1	Vítězslava
Kaprálová	Kaprálový	k2eAgFnSc1d1	Kaprálová
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
-	-	kIx~	-
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Howard	Howard	k1gMnSc1	Howard
Shanet	Shanet	k1gMnSc1	Shanet
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
-	-	kIx~	-
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chou	Choa	k1gFnSc4	Choa
Wen-chung	Wenhunga	k1gFnPc2	Wen-chunga
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
a	a	k8xC	a
Burt	Burt	k2eAgInSc1d1	Burt
Bacharach	Bacharach	k1gInSc1	Bacharach
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
založila	založit	k5eAaPmAgFnS	založit
vdova	vdova	k1gFnSc1	vdova
Charlotte	Charlott	k1gInSc5	Charlott
Společnost	společnost	k1gFnSc1	společnost
Bohuslava	Bohuslav	k1gMnSc4	Bohuslav
Martinů	Martinů	k1gMnSc4	Martinů
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
převedla	převést	k5eAaPmAgFnS	převést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
autorská	autorský	k2eAgNnPc4d1	autorské
práva	právo	k1gNnPc4	právo
děl	dít	k5eAaImAgInS	dít
svého	svůj	k3xOyFgMnSc4	svůj
manžela	manžel	k1gMnSc4	manžel
na	na	k7c4	na
nově	nově	k6eAd1	nově
založenou	založený	k2eAgFnSc4d1	založená
Nadaci	nadace	k1gFnSc4	nadace
Bohuslava	Bohuslav	k1gMnSc4	Bohuslav
Martinů	Martinů	k1gFnSc4	Martinů
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
také	také	k9	také
na	na	k7c4	na
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Martinů	Martinů	k2eAgInSc4d1	Martinů
Stiftung	Stiftung	k1gInSc4	Stiftung
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
kvartet	kvartet	k1gInSc1	kvartet
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
Istar	Istar	k1gInSc1	Istar
<g/>
,	,	kIx,	,
balet	balet	k1gInSc1	balet
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
Loutky	loutka	k1gFnPc1	loutka
I	i	k8xC	i
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
III	III	kA	III
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
Half-Time	Half-Tim	k1gInSc5	Half-Tim
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
2	[number]	k4	2
<g/>
.	.	kIx.	.
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
kvartet	kvartet	k1gInSc1	kvartet
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
Kuchyňská	kuchyňský	k2eAgFnSc1d1	kuchyňská
revue	revue	k1gFnSc1	revue
<g/>
,	,	kIx,	,
jazzový	jazzový	k2eAgInSc1d1	jazzový
balet	balet	k1gInSc1	balet
o	o	k7c6	o
1	[number]	k4	1
dějství	dějství	k1gNnSc6	dějství
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
baletní	baletní	k2eAgNnSc1d1	baletní
libreto	libreto	k1gNnSc1	libreto
Jarmila	Jarmila	k1gFnSc1	Jarmila
Kröschlová	Kröschlová	k1gFnSc1	Kröschlová
<g/>
,	,	kIx,	,
verše	verš	k1gInPc1	verš
J.	J.	kA	J.
L.	L.	kA	L.
Budín	Budín	k1gMnSc1	Budín
(	(	kIx(	(
<g/>
=	=	kIx~	=
JUDr.	JUDr.	kA	JUDr.
Jan	Jan	k1gMnSc1	Jan
Löwenbach	Löwenbach	k1gMnSc1	Löwenbach
<g/>
)	)	kIx)	)
Ruce	ruka	k1gFnPc1	ruka
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
mains	mains	k1gInSc1	mains
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
balet	balet	k1gInSc1	balet
<g/>
.	.	kIx.	.
</s>
<s>
Skadba	Skadba	k1gFnSc1	Skadba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
a	a	k8xC	a
objednávku	objednávka	k1gFnSc4	objednávka
tanečnice	tanečnice	k1gFnPc1	tanečnice
a	a	k8xC	a
choreografky	choreografka	k1gFnPc1	choreografka
Jelizavety	Jelizaveta	k1gFnSc2	Jelizaveta
Nikolské	Nikolský	k2eAgFnPc1d1	Nikolská
pro	pro	k7c4	pro
její	její	k3xOp3gInSc4	její
taneční	taneční	k2eAgInSc4d1	taneční
soubor	soubor	k1gInSc4	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc4	místo
a	a	k8xC	a
datum	datum	k1gNnSc4	datum
premiéry	premiéra	k1gFnSc2	premiéra
nejsou	být	k5eNaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
<g/>
.	.	kIx.	.
</s>
<s>
Balet	balet	k1gInSc1	balet
byl	být	k5eAaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Provedení	provedení	k1gNnSc1	provedení
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
byl	být	k5eAaImAgInS	být
balet	balet	k1gInSc1	balet
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
ztracenou	ztracený	k2eAgFnSc4d1	ztracená
<g/>
.	.	kIx.	.
</s>
<s>
Partitura	partitura	k1gFnSc1	partitura
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
objevena	objevit	k5eAaPmNgFnS	objevit
v	v	k7c4	v
pozůstalosti	pozůstalost	k1gFnPc4	pozůstalost
po	po	k7c6	po
Jelizavetě	Jelizaveta	k1gFnSc6	Jelizaveta
Nikolské	Nikolský	k2eAgFnSc6d1	Nikolská
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kompletní	kompletní	k2eAgFnSc4d1	kompletní
partituru	partitura	k1gFnSc4	partitura
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
opisy	opis	k1gInPc4	opis
partů	part	k1gInPc2	part
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
byla	být	k5eAaImAgFnS	být
zrekonstruována	zrekonstruovat	k5eAaPmNgFnS	zrekonstruovat
a	a	k8xC	a
koncertně	koncertně	k6eAd1	koncertně
provedena	provést	k5eAaPmNgFnS	provést
Pražskou	pražský	k2eAgFnSc7d1	Pražská
komorní	komorní	k2eAgFnSc7d1	komorní
filharmonií	filharmonie	k1gFnSc7	filharmonie
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jakuba	Jakub	k1gMnSc2	Jakub
Hrůši	Hrůše	k1gFnSc6	Hrůše
na	na	k7c6	na
koncertu	koncert	k1gInSc6	koncert
v	v	k7c6	v
Rudolfinu	Rudolfinum	k1gNnSc6	Rudolfinum
dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Voják	voják	k1gMnSc1	voják
a	a	k8xC	a
tanečnice	tanečnice	k1gFnSc1	tanečnice
<g/>
,	,	kIx,	,
komická	komický	k2eAgFnSc1d1	komická
opera	opera	k1gFnSc1	opera
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
dějstvích	dějství	k1gNnPc6	dějství
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
libreto	libreto	k1gNnSc1	libreto
J.	J.	kA	J.
L.	L.	kA	L.
Budín	Budín	k1gMnSc1	Budín
(	(	kIx(	(
<g/>
=	=	kIx~	=
JUDr.	JUDr.	kA	JUDr.
Jan	Jan	k1gMnSc1	Jan
Löwenbach	Löwenbach	k1gMnSc1	Löwenbach
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
Plautovy	Plautův	k2eAgFnSc2d1	Plautova
komedie	komedie	k1gFnSc2	komedie
Pseudolus	Pseudolus	k1gInSc4	Pseudolus
Tři	tři	k4xCgNnPc4	tři
přání	přání	k1gNnPc4	přání
<g/>
,	,	kIx,	,
opera-film	operailma	k1gFnPc2	opera-filma
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
Violoncellový	violoncellový	k2eAgInSc1d1	violoncellový
koncert	koncert	k1gInSc1	koncert
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
Špalíček	špalíček	k1gInSc1	špalíček
<g/>
,	,	kIx,	,
balet	balet	k1gInSc1	balet
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1931	[number]	k4	1931
<g/>
-	-	kIx~	-
<g/>
32	[number]	k4	32
Hry	hra	k1gFnSc2	hra
o	o	k7c4	o
Marii	Maria	k1gFnSc4	Maria
<g/>
,	,	kIx,	,
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
34	[number]	k4	34
Julietta	Julietto	k1gNnSc2	Julietto
(	(	kIx(	(
<g/>
Snář	snář	k1gInSc1	snář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
37	[number]	k4	37
4	[number]	k4	4
<g/>
.	.	kIx.	.
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
kvartet	kvartet	k1gInSc1	kvartet
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
Concerto	Concerta	k1gFnSc5	Concerta
grosso	grossa	k1gFnSc5	grossa
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
Dvojkoncert	dvojkoncert	k1gInSc1	dvojkoncert
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
<g />
.	.	kIx.	.
</s>
<s>
smyčcové	smyčcový	k2eAgInPc1d1	smyčcový
orchestry	orchestr	k1gInPc1	orchestr
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc1	klavír
a	a	k8xC	a
tympány	tympán	k1gInPc1	tympán
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
Sinfonietta	Sinfonietta	k1gFnSc1	Sinfonietta
giocossa	giocossa	k1gFnSc1	giocossa
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
1	[number]	k4	1
<g/>
.	.	kIx.	.
symfonie	symfonie	k1gFnSc2	symfonie
<g/>
,	,	kIx,	,
1942	[number]	k4	1942
2	[number]	k4	2
<g/>
.	.	kIx.	.
symfonie	symfonie	k1gFnSc2	symfonie
<g/>
,	,	kIx,	,
1943	[number]	k4	1943
Památník	památník	k1gInSc1	památník
Lidicím	Lidice	k1gInPc3	Lidice
<g/>
,	,	kIx,	,
1943	[number]	k4	1943
3	[number]	k4	3
<g/>
.	.	kIx.	.
symfonie	symfonie	k1gFnSc2	symfonie
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
4	[number]	k4	4
<g/>
.	.	kIx.	.
symfonie	symfonie	k1gFnSc1	symfonie
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1945	[number]	k4	1945
5	[number]	k4	5
<g/>
.	.	kIx.	.
symfonie	symfonie	k1gFnSc2	symfonie
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
6	[number]	k4	6
<g/>
.	.	kIx.	.
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
kvartet	kvartet	k1gInSc1	kvartet
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
Ženitba	ženitba	k1gFnSc1	ženitba
<g/>
,	,	kIx,	,
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
6	[number]	k4	6
<g/>
.	.	kIx.	.
symfonie	symfonie	k1gFnSc2	symfonie
(	(	kIx(	(
<g/>
Symfonické	symfonický	k2eAgFnSc2d1	symfonická
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
1955	[number]	k4	1955
Gilgameš	Gilgameš	k1gFnPc2	Gilgameš
<g/>
,	,	kIx,	,
oratorium	oratorium	k1gNnSc1	oratorium
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
55	[number]	k4	55
Otvírání	otvírání	k1gNnSc1	otvírání
studánek	studánka	k1gFnPc2	studánka
<g/>
,	,	kIx,	,
kantátový	kantátový	k2eAgInSc1d1	kantátový
cyklus	cyklus	k1gInSc1	cyklus
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
Fresky	freska	k1gFnSc2	freska
Piera	Piera	k1gFnSc1	Piera
della	della	k1gFnSc1	della
Francesca	Francesca	k1gFnSc1	Francesca
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
4	[number]	k4	4
<g/>
.	.	kIx.	.
klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
Inkantace	Inkantace	k1gFnSc2	Inkantace
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
5	[number]	k4	5
<g/>
.	.	kIx.	.
klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
Fantasia	Fantasium	k1gNnSc2	Fantasium
concertante	concertant	k1gMnSc5	concertant
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
Mirandolina	Mirandolina	k1gFnSc1	Mirandolina
<g/>
,	,	kIx,	,
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
Řecké	řecký	k2eAgFnPc1d1	řecká
pašije	pašije	k1gFnPc1	pašije
<g/>
,	,	kIx,	,
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
1954	[number]	k4	1954
<g/>
-	-	kIx~	-
<g/>
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
verze	verze	k1gFnSc1	verze
1957	[number]	k4	1957
<g/>
-	-	kIx~	-
<g/>
1959	[number]	k4	1959
Ariadna	Ariadna	k1gFnSc1	Ariadna
<g/>
,	,	kIx,	,
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
Rytiny	rytina	k1gFnPc1	rytina
<g/>
,	,	kIx,	,
suita	suita	k1gFnSc1	suita
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
</s>
