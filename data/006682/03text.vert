<s>
Subtropický	subtropický	k2eAgInSc1d1	subtropický
podnebný	podnebný	k2eAgInSc1d1	podnebný
pás	pás	k1gInSc1	pás
neboli	neboli	k8xC	neboli
subtropy	subtropy	k1gInPc1	subtropy
jsou	být	k5eAaImIp3nP	být
oblasti	oblast	k1gFnPc4	oblast
se	s	k7c7	s
subtropickým	subtropický	k2eAgNnSc7d1	subtropické
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Subtropy	subtropy	k1gInPc1	subtropy
leží	ležet	k5eAaImIp3nP	ležet
mezi	mezi	k7c7	mezi
oblastmi	oblast	k1gFnPc7	oblast
s	s	k7c7	s
tropickým	tropický	k2eAgNnSc7d1	tropické
podnebím	podnebí	k1gNnSc7	podnebí
a	a	k8xC	a
oblastmi	oblast	k1gFnPc7	oblast
s	s	k7c7	s
mírným	mírný	k2eAgNnSc7d1	mírné
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
subtropy	subtropy	k1gInPc4	subtropy
tedy	tedy	k9	tedy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nahlíženo	nahlížen	k2eAgNnSc1d1	nahlíženo
jako	jako	k9	jako
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
přechod	přechod	k1gInSc4	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
leží	ležet	k5eAaImIp3nP	ležet
subtropy	subtropy	k1gInPc1	subtropy
mezi	mezi	k7c7	mezi
30	[number]	k4	30
<g/>
°	°	k?	°
a	a	k8xC	a
45	[number]	k4	45
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
leží	ležet	k5eAaImIp3nP	ležet
subtropy	subtropy	k1gInPc1	subtropy
mezi	mezi	k7c7	mezi
obratníkem	obratník	k1gInSc7	obratník
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
a	a	k8xC	a
40	[number]	k4	40
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Alisově	Alisův	k2eAgFnSc6d1	Alisův
klasifikaci	klasifikace	k1gFnSc6	klasifikace
podnebí	podnebí	k1gNnSc2	podnebí
jsou	být	k5eAaImIp3nP	být
hranice	hranice	k1gFnPc4	hranice
vymezeny	vymezen	k2eAgInPc1d1	vymezen
klimatickými	klimatický	k2eAgFnPc7d1	klimatická
polohami	poloha	k1gFnPc7	poloha
polární	polární	k2eAgFnSc2d1	polární
a	a	k8xC	a
tropické	tropický	k2eAgFnSc2d1	tropická
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
severní	severní	k2eAgFnSc4d1	severní
polokouli	polokoule	k1gFnSc4	polokoule
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
severní	severní	k2eAgFnSc1d1	severní
hranice	hranice	k1gFnSc1	hranice
subtropů	subtropy	k1gInPc2	subtropy
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
nejsevernější	severní	k2eAgFnSc3d3	nejsevernější
klimatické	klimatický	k2eAgFnSc3d1	klimatická
poloze	poloha	k1gFnSc3	poloha
tropické	tropický	k2eAgFnSc2d1	tropická
fronty	fronta	k1gFnSc2	fronta
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
hranice	hranice	k1gFnSc2	hranice
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
nejjižnější	jižní	k2eAgFnSc3d3	nejjižnější
klimatické	klimatický	k2eAgFnSc3d1	klimatická
poloze	poloha	k1gFnSc3	poloha
polární	polární	k2eAgFnSc2d1	polární
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
v	v	k7c6	v
subtropech	subtropy	k1gInPc6	subtropy
tropická	tropický	k2eAgFnSc1d1	tropická
vzduchová	vzduchový	k2eAgFnSc1d1	vzduchová
hmota	hmota	k1gFnSc1	hmota
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
subtropech	subtropy	k1gInPc6	subtropy
polární	polární	k2eAgFnSc1d1	polární
vzduchová	vzduchový	k2eAgFnSc1d1	vzduchová
hmota	hmota	k1gFnSc1	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
subtropické	subtropický	k2eAgNnSc1d1	subtropické
podnebí	podnebí	k1gNnSc1	podnebí
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
4	[number]	k4	4
typy	typ	k1gInPc4	typ
<g/>
:	:	kIx,	:
pevninské	pevninský	k2eAgInPc4d1	pevninský
(	(	kIx(	(
<g/>
kontinentální	kontinentální	k2eAgInPc4d1	kontinentální
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mořské	mořský	k2eAgInPc1d1	mořský
(	(	kIx(	(
<g/>
maritimní	maritimní	k2eAgInSc1d1	maritimní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
západních	západní	k2eAgInPc2d1	západní
břehů	břeh	k1gInPc2	břeh
pevnin	pevnina	k1gFnPc2	pevnina
(	(	kIx(	(
<g/>
středomořské	středomořský	k2eAgInPc1d1	středomořský
<g/>
)	)	kIx)	)
a	a	k8xC	a
východních	východní	k2eAgInPc2d1	východní
břehů	břeh	k1gInPc2	břeh
pevnin	pevnina	k1gFnPc2	pevnina
(	(	kIx(	(
<g/>
monzunové	monzunový	k2eAgInPc4d1	monzunový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
subtropech	subtropy	k1gInPc6	subtropy
vegetují	vegetovat	k5eAaImIp3nP	vegetovat
společenstva	společenstvo	k1gNnPc1	společenstvo
vždy	vždy	k6eAd1	vždy
zelených	zelený	k2eAgMnPc2d1	zelený
tvrdolistých	tvrdolistý	k2eAgMnPc2d1	tvrdolistý
keřů	keř	k1gInPc2	keř
(	(	kIx(	(
<g/>
makchie	makchie	k1gFnSc1	makchie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střídavě	střídavě	k6eAd1	střídavě
vlhké	vlhký	k2eAgInPc1d1	vlhký
lesy	les	k1gInPc1	les
subtropů	subtropy	k1gInPc2	subtropy
<g/>
,	,	kIx,	,
rostlinstva	rostlinstvo	k1gNnSc2	rostlinstvo
pouští	poušť	k1gFnPc2	poušť
a	a	k8xC	a
polopouští	polopoušť	k1gFnPc2	polopoušť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
subtropech	subtropy	k1gInPc6	subtropy
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
např.	např.	kA	např.
bavlníky	bavlník	k1gInPc1	bavlník
<g/>
,	,	kIx,	,
tabák	tabák	k1gInSc1	tabák
<g/>
,	,	kIx,	,
olivy	oliva	k1gFnPc1	oliva
<g/>
,	,	kIx,	,
pomeranče	pomeranč	k1gInPc1	pomeranč
či	či	k8xC	či
jiné	jiný	k2eAgInPc1d1	jiný
citrusy	citrus	k1gInPc1	citrus
a	a	k8xC	a
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
.	.	kIx.	.
</s>
<s>
Atlanta	Atlanta	k1gFnSc1	Atlanta
<g/>
,	,	kIx,	,
Georgie	Georgie	k1gFnSc1	Georgie
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
(	(	kIx(	(
<g/>
33,9	[number]	k4	33,9
<g/>
°	°	k?	°
s.	s.	k?	s.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
Brisbane	Brisban	k1gMnSc5	Brisban
<g/>
,	,	kIx,	,
Queensland	Queenslanda	k1gFnPc2	Queenslanda
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
(	(	kIx(	(
<g/>
27,3	[number]	k4	27,3
<g/>
o	o	k7c6	o
j.	j.	k?	j.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
(	(	kIx(	(
<g/>
34,6	[number]	k4	34,6
<g/>
o	o	k7c6	o
j.	j.	k?	j.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Johannesburg	Johannesburg	k1gInSc1	Johannesburg
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
26,1	[number]	k4	26,1
<g/>
o	o	k7c6	o
j.	j.	k?	j.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
33,9	[number]	k4	33,9
<g/>
°	°	k?	°
j.	j.	k?	j.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
Tallahassee	Tallahassee	k1gFnSc1	Tallahassee
<g/>
,	,	kIx,	,
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
(	(	kIx(	(
<g/>
30,4	[number]	k4	30,4
<g/>
o	o	k7c6	o
s.	s.	k?	s.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Nové	Nové	k2eAgNnSc1d1	Nové
Dillí	Dillí	k1gNnSc1	Dillí
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
(	(	kIx(	(
<g/>
28,6	[number]	k4	28,6
<g/>
o	o	k7c6	o
s.	s.	k?	s.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
New	New	k1gFnSc1	New
Orleans	Orleans	k1gInSc1	Orleans
<g/>
,	,	kIx,	,
Louisiana	Louisiana	k1gFnSc1	Louisiana
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
(	(	kIx(	(
<g/>
29,9	[number]	k4	29,9
<g/>
o	o	k7c6	o
s.	s.	k?	s.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
Pensacola	Pensacola	k1gFnSc1	Pensacola
<g/>
,	,	kIx,	,
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
(	(	kIx(	(
<g/>
30,2	[number]	k4	30,2
<g/>
°	°	k?	°
<g />
.	.	kIx.	.
</s>
<s>
s.	s.	k?	s.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
(	(	kIx(	(
<g/>
37,6	[number]	k4	37,6
<g/>
°	°	k?	°
s.	s.	k?	s.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
Soči	Soči	k1gNnSc1	Soči
<g/>
,	,	kIx,	,
Krasnodarský	krasnodarský	k2eAgInSc1d1	krasnodarský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
43,6	[number]	k4	43,6
<g/>
°	°	k?	°
s.	s.	k?	s.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
Sydney	Sydney	k1gNnSc1	Sydney
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Jižní	jižní	k2eAgInSc1d1	jižní
Wales	Wales	k1gInSc1	Wales
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
(	(	kIx(	(
<g/>
34,0	[number]	k4	34,0
<g/>
o	o	k7c6	o
j.	j.	k?	j.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
(	(	kIx(	(
<g/>
31,2	[number]	k4	31,2
<g/>
°	°	k?	°
s.	s.	k?	s.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
Tokio	Tokio	k1gNnSc1	Tokio
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
(	(	kIx(	(
<g/>
35,5	[number]	k4	35,5
<g/>
o	o	k7c6	o
s.	s.	k?	s.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
(	(	kIx(	(
<g/>
34,0	[number]	k4	34,0
<g/>
°	°	k?	°
s.	s.	k?	s.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
</s>
