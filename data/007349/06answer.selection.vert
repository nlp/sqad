<s>
Na	na	k7c6	na
úseku	úsek	k1gInSc6	úsek
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
začala	začít	k5eAaPmAgFnS	začít
výroba	výroba	k1gFnSc1	výroba
krajek	krajka	k1gFnPc2	krajka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
v	v	k7c6	v
Letovicích	Letovice	k1gFnPc6	Letovice
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
(	(	kIx(	(
<g/>
na	na	k7c6	na
bobinetových	bobinetový	k2eAgInPc6d1	bobinetový
strojích	stroj	k1gInPc6	stroj
pašovaných	pašovaný	k2eAgInPc6d1	pašovaný
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
továrna	továrna	k1gFnSc1	továrna
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentě	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
