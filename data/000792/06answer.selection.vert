<s>
Halloween	Halloween	k1gInSc4	Halloween
(	(	kIx(	(
<g/>
čti	číst	k5eAaImRp2nS	číst
helouvín	helouvín	k1gInSc1	helouvín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
anglosaský	anglosaský	k2eAgInSc4d1	anglosaský
lidový	lidový	k2eAgInSc4d1	lidový
svátek	svátek	k1gInSc4	svátek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
den	den	k1gInSc4	den
před	před	k7c7	před
křesťanským	křesťanský	k2eAgInSc7d1	křesťanský
svátkem	svátek	k1gInSc7	svátek
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
<g/>
,	,	kIx,	,
z	z	k7c2	z
jehož	jehož	k3xOyRp3gFnPc2	jehož
oslav	oslava	k1gFnPc2	oslava
se	se	k3xPyFc4	se
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
<g/>
.	.	kIx.	.
</s>
