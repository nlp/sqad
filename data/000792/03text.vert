<s>
Halloween	Halloween	k1gInSc4	Halloween
(	(	kIx(	(
<g/>
čti	číst	k5eAaImRp2nS	číst
helouvín	helouvín	k1gInSc1	helouvín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
anglosaský	anglosaský	k2eAgInSc4d1	anglosaský
lidový	lidový	k2eAgInSc4d1	lidový
svátek	svátek	k1gInSc4	svátek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
den	den	k1gInSc4	den
před	před	k7c7	před
křesťanským	křesťanský	k2eAgInSc7d1	křesťanský
svátkem	svátek	k1gInSc7	svátek
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
<g/>
,	,	kIx,	,
z	z	k7c2	z
jehož	jehož	k3xOyRp3gFnPc2	jehož
oslav	oslava	k1gFnPc2	oslava
se	se	k3xPyFc4	se
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
se	se	k3xPyFc4	se
oblékají	oblékat	k5eAaImIp3nP	oblékat
do	do	k7c2	do
strašidelných	strašidelný	k2eAgInPc2d1	strašidelný
kostýmů	kostým	k1gInPc2	kostým
a	a	k8xC	a
chodí	chodit	k5eAaImIp3nP	chodit
od	od	k7c2	od
domu	dům	k1gInSc2	dům
k	k	k7c3	k
domu	dům	k1gInSc3	dům
s	s	k7c7	s
tradičním	tradiční	k2eAgNnSc7d1	tradiční
pořekadlem	pořekadlo	k1gNnSc7	pořekadlo
Trick	Trick	k1gInSc1	Trick
or	or	k?	or
treat	treat	k1gInSc4	treat
(	(	kIx(	(
<g/>
Koledu	koleda	k1gFnSc4	koleda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vám	vy	k3xPp2nPc3	vy
něco	něco	k3yInSc1	něco
provedu	provést	k5eAaPmIp1nS	provést
-	-	kIx~	-
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
typické	typický	k2eAgNnSc1d1	typické
pomalovávání	pomalovávání	k1gNnSc1	pomalovávání
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
dveří	dveře	k1gFnPc2	dveře
<g/>
,	,	kIx,	,
zubní	zubní	k2eAgFnSc1d1	zubní
pasta	pasta	k1gFnSc1	pasta
na	na	k7c6	na
dveřích	dveře	k1gFnPc6	dveře
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
koledují	koledovat	k5eAaImIp3nP	koledovat
<g/>
"	"	kIx"	"
o	o	k7c4	o
sladkosti	sladkost	k1gFnPc4	sladkost
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	svátek	k1gInSc1	svátek
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
aj.	aj.	kA	aj.
Název	název	k1gInSc1	název
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zkrácením	zkrácení	k1gNnSc7	zkrácení
anglického	anglický	k2eAgNnSc2d1	anglické
"	"	kIx"	"
<g/>
All	All	k1gFnSc4	All
Hallows	Hallowsa	k1gFnPc2	Hallowsa
<g/>
'	'	kIx"	'
Evening	Evening	k1gInSc1	Evening
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
Předvečer	předvečer	k1gInSc4	předvečer
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zvyky	zvyk	k1gInPc1	zvyk
Halloweenu	Halloween	k1gInSc2	Halloween
se	se	k3xPyFc4	se
rozvinuly	rozvinout	k5eAaPmAgFnP	rozvinout
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zvyků	zvyk	k1gInPc2	zvyk
oslavy	oslava	k1gFnSc2	oslava
svátku	svátek	k1gInSc2	svátek
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
přenesen	přenést	k5eAaPmNgInS	přenést
do	do	k7c2	do
Irska	Irsko	k1gNnSc2	Irsko
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Opírají	opírat	k5eAaImIp3nP	opírat
se	se	k3xPyFc4	se
o	o	k7c4	o
původně	původně	k6eAd1	původně
židovskou	židovský	k2eAgFnSc4d1	židovská
tradici	tradice	k1gFnSc4	tradice
slavení	slavení	k1gNnSc2	slavení
svátků	svátek	k1gInPc2	svátek
již	již	k6eAd1	již
od	od	k7c2	od
předchozího	předchozí	k2eAgInSc2d1	předchozí
večera	večer	k1gInSc2	večer
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Židé	Žid	k1gMnPc1	Žid
počítali	počítat	k5eAaImAgMnP	počítat
začátek	začátek	k1gInSc4	začátek
dne	den	k1gInSc2	den
již	již	k6eAd1	již
předchozím	předchozí	k2eAgInSc7d1	předchozí
západem	západ	k1gInSc7	západ
slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Vystěhovalci	vystěhovalec	k1gMnPc1	vystěhovalec
zvyk	zvyk	k1gInSc4	zvyk
přenesli	přenést	k5eAaPmAgMnP	přenést
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
všeobecně	všeobecně	k6eAd1	všeobecně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g/>
,	,	kIx,	,
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
ho	on	k3xPp3gNnSc2	on
s	s	k7c7	s
expanzí	expanze	k1gFnSc7	expanze
americké	americký	k2eAgFnSc2d1	americká
kultury	kultura	k1gFnSc2	kultura
přejímaly	přejímat	k5eAaImAgFnP	přejímat
další	další	k2eAgFnPc1d1	další
země	zem	k1gFnPc1	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
irského	irský	k2eAgNnSc2d1	irské
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Halloween	Halloween	k1gInSc1	Halloween
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
předkřesťanských	předkřesťanský	k2eAgFnPc2d1	předkřesťanská
keltských	keltský	k2eAgFnPc2d1	keltská
tradic	tradice	k1gFnPc2	tradice
<g/>
,	,	kIx,	,
např.	např.	kA	např.
svátku	svátek	k1gInSc2	svátek
Samhain	Samhaina	k1gFnPc2	Samhaina
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
pojetí	pojetí	k1gNnSc4	pojetí
přijal	přijmout	k5eAaPmAgMnS	přijmout
i	i	k9	i
etnolog	etnolog	k1gMnSc1	etnolog
James	James	k1gMnSc1	James
Frazer	Frazer	k1gMnSc1	Frazer
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Halloween	Halloween	k1gInSc1	Halloween
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
ratolest	ratolest	k1gFnSc1	ratolest
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k9	jako
pohanský	pohanský	k2eAgInSc1d1	pohanský
svátek	svátek	k1gInSc1	svátek
s	s	k7c7	s
tenkou	tenký	k2eAgFnSc7d1	tenká
křesťanskou	křesťanský	k2eAgFnSc7d1	křesťanská
slupkou	slupka	k1gFnSc7	slupka
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
romantické	romantický	k2eAgFnPc1d1	romantická
teorie	teorie	k1gFnPc1	teorie
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
často	často	k6eAd1	často
citují	citovat	k5eAaBmIp3nP	citovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
překonané	překonaný	k2eAgNnSc4d1	překonané
<g/>
.	.	kIx.	.
</s>
<s>
Tradičními	tradiční	k2eAgInPc7d1	tradiční
znaky	znak	k1gInPc7	znak
Halloweenu	Halloween	k1gInSc2	Halloween
jsou	být	k5eAaImIp3nP	být
vyřezané	vyřezaný	k2eAgFnPc1d1	vyřezaná
dýně	dýně	k1gFnPc1	dýně
se	s	k7c7	s
svíčkou	svíčka	k1gFnSc7	svíčka
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgNnSc1d1	zvané
jack-o	jack	k1gNnSc1	jack-o
<g/>
'	'	kIx"	'
<g/>
-lantern	antern	k1gNnSc1	-lantern
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
čarodějky	čarodějka	k1gFnPc1	čarodějka
<g/>
,	,	kIx,	,
duchové	duchový	k2eAgFnPc1d1	duchová
<g/>
,	,	kIx,	,
černé	černý	k2eAgFnPc1d1	černá
kočky	kočka	k1gFnPc1	kočka
<g/>
,	,	kIx,	,
košťata	koště	k1gNnPc1	koště
<g/>
,	,	kIx,	,
oheň	oheň	k1gInSc1	oheň
<g/>
,	,	kIx,	,
příšery	příšera	k1gFnPc1	příšera
<g/>
,	,	kIx,	,
kostlivci	kostlivec	k1gMnPc1	kostlivec
<g/>
,	,	kIx,	,
sovy	sova	k1gFnPc1	sova
<g/>
,	,	kIx,	,
výři	výr	k1gMnPc1	výr
atd.	atd.	kA	atd.
Typickými	typický	k2eAgFnPc7d1	typická
barvami	barva	k1gFnPc7	barva
jsou	být	k5eAaImIp3nP	být
černá	černý	k2eAgFnSc1d1	černá
a	a	k8xC	a
oranžová	oranžový	k2eAgFnSc1d1	oranžová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
tradiční	tradiční	k2eAgInSc4d1	tradiční
svátek	svátek	k1gInSc4	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
domácnosti	domácnost	k1gFnPc1	domácnost
si	se	k3xPyFc3	se
před	před	k7c4	před
dům	dům	k1gInSc4	dům
symbolicky	symbolicky	k6eAd1	symbolicky
dávají	dávat	k5eAaImIp3nP	dávat
jack-o	jack	k1gNnSc4	jack-o
<g/>
'	'	kIx"	'
<g/>
-lantern	antern	k1gInSc1	-lantern
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
tyto	tento	k3xDgFnPc1	tento
domácnosti	domácnost	k1gFnPc1	domácnost
nejsou	být	k5eNaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
připravené	připravený	k2eAgFnPc1d1	připravená
na	na	k7c4	na
trick-or-treating	trickrreating	k1gInSc4	trick-or-treating
<g/>
,	,	kIx,	,
a	a	k8xC	a
do	do	k7c2	do
Halloweenu	Halloween	k1gInSc2	Halloween
se	se	k3xPyFc4	se
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
nezapojují	zapojovat	k5eNaImIp3nP	zapojovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Halloween	Hallowena	k1gFnPc2	Hallowena
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Halloween	Hallowena	k1gFnPc2	Hallowena
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Halloween	Halloween	k1gInSc1	Halloween
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Halloween	Hallowena	k1gFnPc2	Hallowena
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Halloween	Hallowena	k1gFnPc2	Hallowena
anebo	anebo	k8xC	anebo
Dušičky	dušička	k1gFnSc2	dušička
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
velkaepocha	velkaepoch	k1gMnSc2	velkaepoch
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
<g/>
)	)	kIx)	)
Halloween	Halloween	k1gInSc1	Halloween
se	se	k3xPyFc4	se
v	v	k7c6	v
ČR	ČR	kA	ČR
chytl	chytnout	k5eAaPmAgMnS	chytnout
<g/>
,	,	kIx,	,
Poláci	Polák	k1gMnPc1	Polák
před	před	k7c4	před
ním-varují	nímarovat	k5eAaPmIp3nP	ním-varovat
(	(	kIx(	(
<g/>
lidovky	lidovky	k1gFnPc1	lidovky
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
Halloween	Halloween	k1gInSc1	Halloween
je	být	k5eAaImIp3nS	být
morbidní	morbidní	k2eAgInSc1d1	morbidní
<g/>
,	,	kIx,	,
náš	náš	k3xOp1gInSc1	náš
svátek	svátek	k1gInSc1	svátek
ticha	ticho	k1gNnSc2	ticho
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgFnSc4d2	veliký
hloubku	hloubka	k1gFnSc4	hloubka
(	(	kIx(	(
<g/>
idnes	idnes	k1gInSc4	idnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
Dušičky	dušička	k1gFnSc2	dušička
<g/>
,	,	kIx,	,
čas	čas	k1gInSc4	čas
symboilckého	symboilckého	k2eAgInSc4d1	symboilckého
prolínání	prolínání	k1gNnSc4	prolínání
světa	svět	k1gInSc2	svět
živých	živý	k1gMnPc2	živý
a	a	k8xC	a
mrtvých	mrtvý	k1gMnPc2	mrtvý
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
)	)	kIx)	)
</s>
