<s>
Infekční	infekční	k2eAgFnSc1d1	infekční
mononukleóza	mononukleóza	k1gFnSc1	mononukleóza
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
polibková	polibkový	k2eAgFnSc1d1	polibková
nemoc	nemoc	k1gFnSc1	nemoc
či	či	k8xC	či
nemoc	nemoc	k1gFnSc1	nemoc
z	z	k7c2	z
líbání	líbání	k1gNnSc2	líbání
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
virové	virový	k2eAgNnSc1d1	virové
onemocnění	onemocnění	k1gNnSc1	onemocnění
způsobené	způsobený	k2eAgFnSc2d1	způsobená
viry	vira	k1gFnSc2	vira
Epsteina	Epsteino	k1gNnSc2	Epsteino
a	a	k8xC	a
Barrové	Barrová	k1gFnSc2	Barrová
(	(	kIx(	(
<g/>
Epstein	Epstein	k1gMnSc1	Epstein
Barr	Barr	k1gMnSc1	Barr
/	/	kIx~	/
EBV	EBV	kA	EBV
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
cytomegalovirem	cytomegalovirem	k6eAd1	cytomegalovirem
(	(	kIx(	(
<g/>
CMV	CMV	kA	CMV
infekce	infekce	k1gFnSc2	infekce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postihuje	postihovat	k5eAaImIp3nS	postihovat
vnitřní	vnitřní	k2eAgFnPc4d1	vnitřní
žlázy	žláza	k1gFnPc4	žláza
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
slezinu	slezina	k1gFnSc4	slezina
a	a	k8xC	a
játra	játra	k1gNnPc1	játra
a	a	k8xC	a
uzliny	uzlina	k1gFnPc1	uzlina
<g/>
,	,	kIx,	,
hrtanové	hrtanový	k2eAgFnPc1d1	hrtanová
a	a	k8xC	a
týlní	týlní	k2eAgFnPc1d1	týlní
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
zpočátku	zpočátku	k6eAd1	zpočátku
vypadá	vypadat	k5eAaPmIp3nS	vypadat
jako	jako	k9	jako
angína	angína	k1gFnSc1	angína
či	či	k8xC	či
chřipka	chřipka	k1gFnSc1	chřipka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
i	i	k9	i
často	často	k6eAd1	často
diagnostikována	diagnostikovat	k5eAaBmNgFnS	diagnostikovat
<g/>
,	,	kIx,	,
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
neúčinkují	účinkovat	k5eNaImIp3nP	účinkovat
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pacient	pacient	k1gMnSc1	pacient
testován	testovat	k5eAaImNgMnS	testovat
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
virů	vir	k1gInPc2	vir
<g/>
.	.	kIx.	.
</s>
<s>
Infekční	infekční	k2eAgFnSc3d1	infekční
mononukleóze	mononukleóza	k1gFnSc3	mononukleóza
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
Nemoc	nemoc	k1gFnSc1	nemoc
z	z	k7c2	z
líbání	líbání	k1gNnSc2	líbání
nebo	nebo	k8xC	nebo
Dětská	dětský	k2eAgFnSc1d1	dětská
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nejčastěji	často	k6eAd3	často
jí	on	k3xPp3gFnSc2	on
onemocní	onemocnět	k5eAaPmIp3nP	onemocnět
malé	malý	k2eAgFnPc1d1	malá
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgInPc2	který
je	být	k5eAaImIp3nS	být
průběh	průběh	k1gInSc1	průběh
většinou	většina	k1gFnSc7	většina
bez	bez	k7c2	bez
příznaků	příznak	k1gInPc2	příznak
<g/>
,	,	kIx,	,
a	a	k8xC	a
dospívající	dospívající	k2eAgMnPc1d1	dospívající
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
slin	slina	k1gFnPc2	slina
-	-	kIx~	-
buď	buď	k8xC	buď
přímo	přímo	k6eAd1	přímo
nejčastěji	často	k6eAd3	často
líbáním	líbání	k1gNnSc7	líbání
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nepřímo	přímo	k6eNd1	přímo
rukama	ruka	k1gFnPc7	ruka
a	a	k8xC	a
předměty	předmět	k1gInPc7	předmět
čerstvě	čerstvě	k6eAd1	čerstvě
potřísněnými	potřísněný	k2eAgFnPc7d1	potřísněná
slinami	slina	k1gFnPc7	slina
(	(	kIx(	(
<g/>
hračky	hračka	k1gFnPc1	hračka
<g/>
,	,	kIx,	,
sklenky	sklenka	k1gFnPc1	sklenka
<g/>
,	,	kIx,	,
ručníky	ručník	k1gInPc1	ručník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
nejčastější	častý	k2eAgInPc1d3	nejčastější
možné	možný	k2eAgInPc1d1	možný
spouštěcí	spouštěcí	k2eAgInPc1d1	spouštěcí
mechanismy	mechanismus	k1gInPc1	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
jedinců	jedinec	k1gMnPc2	jedinec
není	být	k5eNaImIp3nS	být
onemocnění	onemocnění	k1gNnSc1	onemocnění
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
<g/>
,	,	kIx,	,
mívá	mívat	k5eAaImIp3nS	mívat
však	však	k9	však
těžší	těžký	k2eAgInSc4d2	těžší
průběh	průběh	k1gInSc4	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
slovo	slovo	k1gNnSc1	slovo
infekční	infekční	k2eAgNnSc1d1	infekční
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nemoc	nemoc	k1gFnSc4	nemoc
málo	málo	k6eAd1	málo
nakažlivou	nakažlivý	k2eAgFnSc4d1	nakažlivá
<g/>
.	.	kIx.	.
</s>
<s>
EB	EB	kA	EB
virus	virus	k1gInSc1	virus
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
přibližně	přibližně	k6eAd1	přibližně
95	[number]	k4	95
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
záleží	záležet	k5eAaImIp3nS	záležet
jen	jen	k9	jen
na	na	k7c6	na
okolnostech	okolnost	k1gFnPc6	okolnost
zda	zda	k8xS	zda
propukne	propuknout	k5eAaPmIp3nS	propuknout
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
či	či	k8xC	či
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jím	on	k3xPp3gInSc7	on
nakazí	nakazit	k5eAaPmIp3nS	nakazit
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Mononukleóza	mononukleóza	k1gFnSc1	mononukleóza
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
již	již	k6eAd1	již
zmíněnými	zmíněný	k2eAgFnPc7d1	zmíněná
slinami	slina	k1gFnPc7	slina
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
k	k	k7c3	k
propuknutí	propuknutí	k1gNnSc3	propuknutí
onemocnění	onemocnění	k1gNnSc2	onemocnění
přispívá	přispívat	k5eAaImIp3nS	přispívat
snížená	snížený	k2eAgFnSc1d1	snížená
obranyschopnost	obranyschopnost	k1gFnSc1	obranyschopnost
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
stres	stres	k1gInSc1	stres
nebo	nebo	k8xC	nebo
totální	totální	k2eAgNnSc1d1	totální
nervové	nervový	k2eAgNnSc1d1	nervové
vyčerpání	vyčerpání	k1gNnSc1	vyčerpání
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
typické	typický	k2eAgInPc4d1	typický
příznaky	příznak	k1gInPc4	příznak
patří	patřit	k5eAaImIp3nS	patřit
počáteční	počáteční	k2eAgFnSc1d1	počáteční
celková	celkový	k2eAgFnSc1d1	celková
únava	únava	k1gFnSc1	únava
<g/>
,	,	kIx,	,
trvající	trvající	k2eAgFnSc1d1	trvající
až	až	k9	až
několik	několik	k4yIc1	několik
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
teplota	teplota	k1gFnSc1	teplota
až	až	k6eAd1	až
horečky	horečka	k1gFnSc2	horečka
(	(	kIx(	(
<g/>
běžně	běžně	k6eAd1	běžně
kolem	kolem	k7c2	kolem
39	[number]	k4	39
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zánět	zánět	k1gInSc4	zánět
nosohltanu	nosohltan	k1gInSc2	nosohltan
a	a	k8xC	a
zvětšení	zvětšení	k1gNnSc1	zvětšení
uzlin	uzlina	k1gFnPc2	uzlina
(	(	kIx(	(
<g/>
podčelistních	podčelistní	k2eAgFnPc2d1	podčelistní
a	a	k8xC	a
krčních	krční	k2eAgFnPc2d1	krční
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
příznaky	příznak	k1gInPc1	příznak
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zvětšená	zvětšený	k2eAgFnSc1d1	zvětšená
slezina	slezina	k1gFnSc1	slezina
a	a	k8xC	a
játra	játra	k1gNnPc1	játra
(	(	kIx(	(
<g/>
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
zánět	zánět	k1gInSc1	zánět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
otoky	otok	k1gInPc4	otok
víček	víčko	k1gNnPc2	víčko
a	a	k8xC	a
obličeje	obličej	k1gInSc2	obličej
<g/>
,	,	kIx,	,
nechutenství	nechutenství	k1gNnSc2	nechutenství
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
4-6	[number]	k4	4-6
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Pacient	pacient	k1gMnSc1	pacient
získává	získávat	k5eAaImIp3nS	získávat
po	po	k7c6	po
onemocnění	onemocnění	k1gNnSc6	onemocnění
IM	IM	kA	IM
doživotní	doživotní	k2eAgFnSc4d1	doživotní
imunitu	imunita	k1gFnSc4	imunita
vůči	vůči	k7c3	vůči
této	tento	k3xDgFnSc3	tento
nemoci	nemoc	k1gFnSc3	nemoc
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
onemocnět	onemocnět	k5eAaPmF	onemocnět
může	moct	k5eAaImIp3nS	moct
člověk	člověk	k1gMnSc1	člověk
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
život	život	k1gInSc4	život
určitým	určitý	k2eAgInSc7d1	určitý
typem	typ	k1gInSc7	typ
mononukleózy	mononukleóza	k1gFnSc2	mononukleóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dostat	dostat	k5eAaPmF	dostat
ji	on	k3xPp3gFnSc4	on
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
oslabena	oslaben	k2eAgFnSc1d1	oslabena
imunita	imunita	k1gFnSc1	imunita
EB	EB	kA	EB
virus	virus	k1gInSc1	virus
se	se	k3xPyFc4	se
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
dostává	dostávat	k5eAaImIp3nS	dostávat
ústy	ústa	k1gNnPc7	ústa
a	a	k8xC	a
vniká	vnikat	k5eAaImIp3nS	vnikat
do	do	k7c2	do
buněk	buňka	k1gFnPc2	buňka
sliznice	sliznice	k1gFnSc2	sliznice
nosohltanu	nosohltan	k1gInSc2	nosohltan
<g/>
,	,	kIx,	,
slinných	slinný	k2eAgFnPc2d1	slinná
žláz	žláza	k1gFnPc2	žláza
a	a	k8xC	a
lymfatické	lymfatický	k2eAgFnSc2d1	lymfatická
tkáně	tkáň	k1gFnSc2	tkáň
mandlí	mandle	k1gFnPc2	mandle
<g/>
.	.	kIx.	.
</s>
<s>
Pomnoží	pomnožit	k5eAaPmIp3nS	pomnožit
se	se	k3xPyFc4	se
a	a	k8xC	a
krví	krev	k1gFnPc2	krev
se	se	k3xPyFc4	se
roznese	roznést	k5eAaPmIp3nS	roznést
do	do	k7c2	do
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lymfatické	lymfatický	k2eAgFnSc6d1	lymfatická
tkáni	tkáň	k1gFnSc6	tkáň
virus	virus	k1gInSc1	virus
napadá	napadat	k5eAaBmIp3nS	napadat
hlavně	hlavně	k9	hlavně
B-lymfocyty	Bymfocyt	k1gInPc4	B-lymfocyt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
chovat	chovat	k5eAaImF	chovat
jako	jako	k8xS	jako
cizí	cizí	k2eAgInSc1d1	cizí
organismus	organismus	k1gInSc1	organismus
a	a	k8xC	a
tělo	tělo	k1gNnSc1	tělo
zahájí	zahájit	k5eAaPmIp3nS	zahájit
protiútok	protiútok	k1gInSc4	protiútok
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
zvané	zvaný	k2eAgInPc4d1	zvaný
T-lymfocyty	Tymfocyt	k1gInPc4	T-lymfocyt
mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
takto	takto	k6eAd1	takto
zdegenerované	zdegenerovaný	k2eAgFnSc2d1	zdegenerovaná
buňky	buňka	k1gFnSc2	buňka
ničit	ničit	k5eAaImF	ničit
a	a	k8xC	a
onemocnění	onemocnění	k1gNnSc4	onemocnění
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Podstatou	podstata	k1gFnSc7	podstata
onemocnění	onemocnění	k1gNnSc1	onemocnění
mononukleózou	mononukleóza	k1gFnSc7	mononukleóza
je	být	k5eAaImIp3nS	být
obranná	obranný	k2eAgFnSc1d1	obranná
reakce	reakce	k1gFnSc1	reakce
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jedna	jeden	k4xCgFnSc1	jeden
složka	složka	k1gFnSc1	složka
(	(	kIx(	(
<g/>
T-lymfocyty	Tymfocyt	k1gInPc1	T-lymfocyt
<g/>
)	)	kIx)	)
musí	muset	k5eAaImIp3nP	muset
vyvinout	vyvinout	k5eAaPmF	vyvinout
takovou	takový	k3xDgFnSc4	takový
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabránila	zabránit	k5eAaPmAgFnS	zabránit
množení	množení	k1gNnSc4	množení
se	se	k3xPyFc4	se
a	a	k8xC	a
nesprávné	správný	k2eNgFnSc3d1	nesprávná
funkci	funkce	k1gFnSc3	funkce
složky	složka	k1gFnSc2	složka
druhé	druhý	k4xOgFnPc4	druhý
(	(	kIx(	(
<g/>
B-lymfocyty	Bymfocyt	k1gInPc7	B-lymfocyt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celku	celek	k1gInSc6	celek
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
narušení	narušení	k1gNnSc3	narušení
rovnováhy	rovnováha	k1gFnSc2	rovnováha
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
složkami	složka	k1gFnPc7	složka
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
u	u	k7c2	u
těžkého	těžký	k2eAgInSc2d1	těžký
průběhu	průběh	k1gInSc2	průběh
a	a	k8xC	a
u	u	k7c2	u
dospělých	dospělý	k2eAgFnPc2d1	dospělá
osob	osoba	k1gFnPc2	osoba
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
ke	k	k7c3	k
komplikacím	komplikace	k1gFnPc3	komplikace
<g/>
:	:	kIx,	:
Ucpání	ucpání	k1gNnSc4	ucpání
horních	horní	k2eAgFnPc2d1	horní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
Obtížné	obtížný	k2eAgNnSc4d1	obtížné
polykání	polykání	k1gNnSc4	polykání
Zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
Zánět	zánět	k1gInSc4	zánět
srdečního	srdeční	k2eAgInSc2d1	srdeční
svalu	sval	k1gInSc2	sval
Zánět	zánět	k1gInSc1	zánět
mozkových	mozkový	k2eAgFnPc2d1	mozková
blan	blána	k1gFnPc2	blána
Oslabení	oslabení	k1gNnSc2	oslabení
imunity	imunita	k1gFnSc2	imunita
Onemocnění	onemocnění	k1gNnSc2	onemocnění
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
žloutenka	žloutenka	k1gFnSc1	žloutenka
Streptokoková	streptokokový	k2eAgFnSc1d1	streptokoková
angína	angína	k1gFnSc1	angína
Vzácná	vzácný	k2eAgFnSc1d1	vzácná
komplikace	komplikace	k1gFnSc1	komplikace
<g/>
:	:	kIx,	:
protržení	protržení	k1gNnSc1	protržení
sleziny	slezina	k1gFnSc2	slezina
U	u	k7c2	u
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
onemocnění	onemocnění	k1gNnSc2	onemocnění
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
často	často	k6eAd1	často
bez	bez	k7c2	bez
příznaků	příznak	k1gInPc2	příznak
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
s	s	k7c7	s
lehkým	lehký	k2eAgInSc7d1	lehký
průběhem	průběh	k1gInSc7	průběh
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pacient	pacient	k1gMnSc1	pacient
dochází	docházet	k5eAaImIp3nS	docházet
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
infekční	infekční	k2eAgNnSc4d1	infekční
oddělení	oddělení	k1gNnSc4	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vážnějšího	vážní	k2eAgInSc2d2	vážnější
průběhu	průběh	k1gInSc2	průběh
(	(	kIx(	(
<g/>
neklesající	klesající	k2eNgFnSc1d1	neklesající
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
větší	veliký	k2eAgInSc1d2	veliký
zánět	zánět	k1gInSc1	zánět
jater	játra	k1gNnPc2	játra
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
hospitalizaci	hospitalizace	k1gFnSc3	hospitalizace
<g/>
.	.	kIx.	.
</s>
<s>
Specifický	specifický	k2eAgInSc1d1	specifický
lék	lék	k1gInSc1	lék
na	na	k7c4	na
infekční	infekční	k2eAgFnSc4d1	infekční
mononukleózu	mononukleóza	k1gFnSc4	mononukleóza
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
virovou	virový	k2eAgFnSc4d1	virová
infekci	infekce	k1gFnSc4	infekce
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
vliv	vliv	k1gInSc1	vliv
ani	ani	k8xC	ani
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
podávají	podávat	k5eAaImIp3nP	podávat
pouze	pouze	k6eAd1	pouze
ke	k	k7c3	k
zmírnění	zmírnění	k1gNnSc3	zmírnění
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
průběhů	průběh	k1gInPc2	průběh
nemoci	nemoc	k1gFnSc2	nemoc
(	(	kIx(	(
<g/>
např.	např.	kA	např.
při	při	k7c6	při
zánětu	zánět	k1gInSc6	zánět
krku	krk	k1gInSc2	krk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
snižování	snižování	k1gNnSc3	snižování
horečky	horečka	k1gFnSc2	horečka
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
různá	různý	k2eAgNnPc1d1	různé
antipyretika	antipyretikum	k1gNnPc1	antipyretikum
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgNnSc1d3	nejdůležitější
pro	pro	k7c4	pro
zdárné	zdárný	k2eAgNnSc4d1	zdárné
uzdravení	uzdravení	k1gNnSc4	uzdravení
je	být	k5eAaImIp3nS	být
klid	klid	k1gInSc1	klid
a	a	k8xC	a
dieta	dieta	k1gFnSc1	dieta
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
měsíc	měsíc	k1gInSc4	měsíc
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
pacient	pacient	k1gMnSc1	pacient
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
držet	držet	k5eAaImF	držet
dietu	dieta	k1gFnSc4	dieta
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
(	(	kIx(	(
<g/>
minimálně	minimálně	k6eAd1	minimálně
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
vyléčení	vyléčení	k1gNnSc6	vyléčení
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
fyzicky	fyzicky	k6eAd1	fyzicky
šetřit	šetřit	k5eAaImF	šetřit
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
ale	ale	k9	ale
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
výsledcích	výsledek	k1gInPc6	výsledek
testů	test	k1gInPc2	test
a	a	k8xC	a
celkovém	celkový	k2eAgInSc6d1	celkový
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
pacienta	pacient	k1gMnSc2	pacient
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
některá	některý	k3yIgNnPc1	některý
období	období	k1gNnPc1	období
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
zkrátit	zkrátit	k5eAaPmF	zkrátit
nebo	nebo	k8xC	nebo
prodloužit	prodloužit	k5eAaPmF	prodloužit
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
krevního	krevní	k2eAgInSc2d1	krevní
obrazu	obraz	k1gInSc2	obraz
a	a	k8xC	a
jaterní	jaterní	k2eAgFnSc2d1	jaterní
testy	testa	k1gFnSc2	testa
se	se	k3xPyFc4	se
normalizují	normalizovat	k5eAaBmIp3nP	normalizovat
do	do	k7c2	do
2-3	[number]	k4	2-3
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Trvalé	trvalý	k2eAgNnSc1d1	trvalé
poškození	poškození	k1gNnSc1	poškození
jater	játra	k1gNnPc2	játra
se	se	k3xPyFc4	se
u	u	k7c2	u
léčených	léčený	k2eAgMnPc2d1	léčený
pacientů	pacient	k1gMnPc2	pacient
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
týdnů	týden	k1gInPc2	týden
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
potlačení	potlačení	k1gNnSc1	potlačení
obranyschopnosti	obranyschopnost	k1gFnSc2	obranyschopnost
organismu	organismus	k1gInSc2	organismus
proti	proti	k7c3	proti
infekcím	infekce	k1gFnPc3	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
je	být	k5eAaImIp3nS	být
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
vnímavost	vnímavost	k1gFnSc1	vnímavost
vůči	vůči	k7c3	vůči
nejrůznějším	různý	k2eAgFnPc3d3	nejrůznější
nákazám	nákaza	k1gFnPc3	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Rekonvalescent	rekonvalescent	k1gMnSc1	rekonvalescent
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
vyhnout	vyhnout	k5eAaPmF	vyhnout
možnosti	možnost	k1gFnPc4	možnost
se	se	k3xPyFc4	se
nakazit	nakazit	k5eAaPmF	nakazit
nebo	nebo	k8xC	nebo
prochladnout	prochladnout	k5eAaPmF	prochladnout
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
neměl	mít	k5eNaImAgInS	mít
fyzicky	fyzicky	k6eAd1	fyzicky
zatěžovat	zatěžovat	k5eAaImF	zatěžovat
<g/>
.	.	kIx.	.
</s>
<s>
Fyzická	fyzický	k2eAgFnSc1d1	fyzická
zátěž	zátěž	k1gFnSc1	zátěž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
postupně	postupně	k6eAd1	postupně
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholovým	vrcholový	k2eAgMnPc3d1	vrcholový
sportovcům	sportovec	k1gMnPc3	sportovec
se	se	k3xPyFc4	se
povolují	povolovat	k5eAaImIp3nP	povolovat
plné	plný	k2eAgInPc1d1	plný
tréninky	trénink	k1gInPc1	trénink
až	až	k6eAd1	až
3	[number]	k4	3
měsíce	měsíc	k1gInSc2	měsíc
po	po	k7c6	po
normalizaci	normalizace	k1gFnSc6	normalizace
laboratorních	laboratorní	k2eAgInPc2d1	laboratorní
testů	test	k1gInPc2	test
<g/>
.	.	kIx.	.
</s>
<s>
Jaterní	jaterní	k2eAgFnSc4d1	jaterní
dietu	dieta	k1gFnSc4	dieta
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
dodržovat	dodržovat	k5eAaImF	dodržovat
3-6	[number]	k4	3-6
měsíců	měsíc	k1gInPc2	měsíc
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
hodnot	hodnota	k1gFnPc2	hodnota
jaterních	jaterní	k2eAgInPc2d1	jaterní
testů	test	k1gInPc2	test
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nutné	nutný	k2eAgFnPc1d1	nutná
jsou	být	k5eAaImIp3nP	být
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
kontroly	kontrola	k1gFnPc1	kontrola
na	na	k7c6	na
infekčním	infekční	k2eAgNnSc6d1	infekční
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
Nedodržením	nedodržení	k1gNnSc7	nedodržení
doporučeného	doporučený	k2eAgInSc2d1	doporučený
režimu	režim	k1gInSc2	režim
se	se	k3xPyFc4	se
pacient	pacient	k1gMnSc1	pacient
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
riziku	riziko	k1gNnSc3	riziko
znovupropuknutí	znovupropuknutí	k1gNnSc4	znovupropuknutí
onemocnění	onemocnění	k1gNnPc2	onemocnění
s	s	k7c7	s
mnohem	mnohem	k6eAd1	mnohem
vážnějším	vážní	k2eAgInSc7d2	vážnější
průběhem	průběh	k1gInSc7	průběh
<g/>
.	.	kIx.	.
</s>
