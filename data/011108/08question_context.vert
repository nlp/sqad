<s>
Třinec	Třinec	k1gInSc1	Třinec
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Trzyniec	Trzyniec	k1gInSc1	Trzyniec
<g/>
;	;	kIx,	;
německy	německy	k6eAd1	německy
Trzynietz	Trzynietz	k1gInSc1	Trzynietz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
statutární	statutární	k2eAgMnSc1d1	statutární
město	město	k1gNnSc4	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Frýdek-Místek	Frýdek-Místek	k1gInSc1	Frýdek-Místek
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
32	[number]	k4	32
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
historického	historický	k2eAgNnSc2d1	historické
Těšínského	Těšínského	k2eAgNnSc2d1	Těšínského
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
35	[number]	k4	35
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
8	[number]	k4	8
541	[number]	k4	541
ha	ha	kA	ha
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
306	[number]	k4	306
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
téměř	téměř	k6eAd1	téměř
1000	[number]	k4	1000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
(	(	kIx(	(
<g/>
televizní	televizní	k2eAgInSc1d1	televizní
vysílač	vysílač	k1gInSc1	vysílač
na	na	k7c6	na
Javorovém	javorový	k2eAgInSc6d1	javorový
vrchu	vrch	k1gInSc6	vrch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třincem	Třinec	k1gInSc7	Třinec
protéká	protékat	k5eAaImIp3nS	protékat
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
na	na	k7c4	na
sever	sever	k1gInSc4	sever
řeka	řeka	k1gFnSc1	řeka
Olše	olše	k1gFnSc1	olše
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
výtoku	výtok	k1gInSc2	výtok
z	z	k7c2	z
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
Jablunkovu	Jablunkův	k2eAgFnSc4d1	Jablunkova
je	být	k5eAaImIp3nS	být
Třinec	Třinec	k1gInSc1	Třinec
druhým	druhý	k4xOgInSc7	druhý
nejvýchodnějším	východní	k2eAgNnSc7d3	nejvýchodnější
městem	město	k1gNnSc7	město
celého	celý	k2eAgNnSc2d1	celé
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nP	sídlet
významný	významný	k2eAgMnSc1d1	významný
český	český	k2eAgMnSc1d1	český
výrobce	výrobce	k1gMnSc1	výrobce
ocelových	ocelový	k2eAgInPc2d1	ocelový
válcovaných	válcovaný	k2eAgInPc2d1	válcovaný
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
Třinecké	třinecký	k2eAgFnPc1d1	třinecká
železárny	železárna	k1gFnPc1	železárna
<g/>
.	.	kIx.	.
</s>

