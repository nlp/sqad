<p>
<s>
Elektrokardiogram	elektrokardiogram	k1gInSc1	elektrokardiogram
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
EKG	EKG	kA	EKG
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
záznam	záznam	k1gInSc4	záznam
časové	časový	k2eAgFnSc2d1	časová
změny	změna	k1gFnSc2	změna
elektrického	elektrický	k2eAgInSc2d1	elektrický
potenciálu	potenciál	k1gInSc2	potenciál
způsobeného	způsobený	k2eAgInSc2d1	způsobený
srdeční	srdeční	k2eAgInSc4d1	srdeční
aktivitou	aktivita	k1gFnSc7	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
záznam	záznam	k1gInSc1	záznam
je	být	k5eAaImIp3nS	být
pořízen	pořídit	k5eAaPmNgInS	pořídit
elektrokardiografem	elektrokardiograf	k1gInSc7	elektrokardiograf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Elektrokardiografie	elektrokardiografie	k1gFnSc2	elektrokardiografie
==	==	k?	==
</s>
</p>
<p>
<s>
EKG	EKG	kA	EKG
je	být	k5eAaImIp3nS	být
standardní	standardní	k2eAgFnSc7d1	standardní
neinvazivní	invazivní	k2eNgFnSc7d1	neinvazivní
metodou	metoda	k1gFnSc7	metoda
funkčního	funkční	k2eAgNnSc2d1	funkční
vyšetření	vyšetření	k1gNnSc2	vyšetření
elektrické	elektrický	k2eAgFnSc2d1	elektrická
aktivity	aktivita	k1gFnSc2	aktivita
myokardu	myokard	k1gInSc2	myokard
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
CNS	CNS	kA	CNS
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
práce	práce	k1gFnSc1	práce
srdce	srdce	k1gNnSc2	srdce
daleko	daleko	k6eAd1	daleko
větší	veliký	k2eAgFnSc4d2	veliký
synchronicitu	synchronicita	k1gFnSc4	synchronicita
a	a	k8xC	a
periodicitu	periodicita	k1gFnSc4	periodicita
<g/>
.	.	kIx.	.
</s>
<s>
Signál	signál	k1gInSc1	signál
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
z	z	k7c2	z
myokardu	myokard	k1gInSc2	myokard
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
všemi	všecek	k3xTgInPc7	všecek
směry	směr	k1gInPc7	směr
do	do	k7c2	do
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
výrazněji	výrazně	k6eAd2	výrazně
zeslabován	zeslabovat	k5eAaImNgInS	zeslabovat
<g/>
.	.	kIx.	.
</s>
<s>
EKG	EKG	kA	EKG
signál	signál	k1gInSc1	signál
proto	proto	k8xC	proto
můžeme	moct	k5eAaImIp1nP	moct
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgFnSc3d1	velká
amplitudě	amplituda	k1gFnSc3	amplituda
(	(	kIx(	(
<g/>
jednotky	jednotka	k1gFnPc1	jednotka
až	až	k8xS	až
desítky	desítka	k1gFnPc1	desítka
mV	mV	k?	mV
<g/>
)	)	kIx)	)
prakticky	prakticky	k6eAd1	prakticky
na	na	k7c6	na
libovolném	libovolný	k2eAgNnSc6d1	libovolné
místě	místo	k1gNnSc6	místo
tělesného	tělesný	k2eAgInSc2d1	tělesný
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
průběh	průběh	k1gInSc1	průběh
EKG	EKG	kA	EKG
signálu	signál	k1gInSc2	signál
==	==	k?	==
</s>
</p>
<p>
<s>
Impuls	impuls	k1gInSc1	impuls
pro	pro	k7c4	pro
kontrakci	kontrakce	k1gFnSc4	kontrakce
myokardu	myokard	k1gInSc2	myokard
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
sinoatriálním	sinoatriální	k2eAgNnSc6d1	sinoatriální
(	(	kIx(	(
<g/>
SA	SA	kA	SA
<g/>
)	)	kIx)	)
uzlu	uzel	k1gInSc2	uzel
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pravé	pravý	k2eAgFnSc2d1	pravá
předsíně	předsíň	k1gFnSc2	předsíň
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účel	účel	k1gInSc4	účel
našeho	náš	k3xOp1gInSc2	náš
stručného	stručný	k2eAgInSc2d1	stručný
výkladu	výklad	k1gInSc2	výklad
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
primární	primární	k2eAgInSc1d1	primární
signál	signál	k1gInSc1	signál
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
slabý	slabý	k2eAgMnSc1d1	slabý
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
při	při	k7c6	při
běžném	běžný	k2eAgInSc6d1	běžný
záznamu	záznam	k1gInSc6	záznam
EKG	EKG	kA	EKG
prakticky	prakticky	k6eAd1	prakticky
nezaznamenáme	zaznamenat	k5eNaPmIp1nP	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
vlna	vlna	k1gFnSc1	vlna
signálu	signál	k1gInSc2	signál
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
můžeme	moct	k5eAaImIp1nP	moct
na	na	k7c6	na
EKG	EKG	kA	EKG
záznamu	záznam	k1gInSc2	záznam
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vlna	vlna	k1gFnSc1	vlna
P	P	kA	P
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
depolarizaci	depolarizace	k1gFnSc6	depolarizace
předsíní	předsíní	k1gNnSc2	předsíní
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
počínající	počínající	k2eAgFnSc4d1	počínající
kontrakci	kontrakce	k1gFnSc4	kontrakce
<g/>
.	.	kIx.	.
</s>
<s>
Samotnou	samotný	k2eAgFnSc4d1	samotná
repolarizaci	repolarizace	k1gFnSc4	repolarizace
předsíní	předsíní	k1gNnSc2	předsíní
na	na	k7c6	na
EKG	EKG	kA	EKG
nejsme	být	k5eNaImIp1nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
příslušný	příslušný	k2eAgInSc1d1	příslušný
biosignál	biosignál	k1gInSc1	biosignál
je	být	k5eAaImIp3nS	být
zastíněn	zastínit	k5eAaPmNgInS	zastínit
daleko	daleko	k6eAd1	daleko
vyšším	vysoký	k2eAgInSc7d2	vyšší
signálem	signál	k1gInSc7	signál
<g/>
,	,	kIx,	,
pocházejícím	pocházející	k2eAgFnPc3d1	pocházející
od	od	k7c2	od
depolarizace	depolarizace	k1gFnSc2	depolarizace
komor	komora	k1gFnPc2	komora
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
signál	signál	k1gInSc1	signál
je	být	k5eAaImIp3nS	být
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
komplexem	komplex	k1gInSc7	komplex
vln	vlna	k1gFnPc2	vlna
QRS	QRS	kA	QRS
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
vlna	vlna	k1gFnSc1	vlna
T	T	kA	T
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
následné	následný	k2eAgFnSc6d1	následná
repolarizaci	repolarizace	k1gFnSc6	repolarizace
komor	komora	k1gFnPc2	komora
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Nemůžeme	moct	k5eNaImIp1nP	moct
se	se	k3xPyFc4	se
zabývat	zabývat	k5eAaImF	zabývat
podrobně	podrobně	k6eAd1	podrobně
interpretací	interpretace	k1gFnSc7	interpretace
<g/>
,	,	kIx,	,
fyziologií	fyziologie	k1gFnSc7	fyziologie
či	či	k8xC	či
patofyziologií	patofyziologie	k1gFnSc7	patofyziologie
EKG	EKG	kA	EKG
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
omezujeme	omezovat	k5eAaImIp1nP	omezovat
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
základní	základní	k2eAgInSc4d1	základní
popis	popis	k1gInSc4	popis
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Einthovenovy	Einthovenův	k2eAgInPc4d1	Einthovenův
(	(	kIx(	(
<g/>
bipolární	bipolární	k2eAgInPc4d1	bipolární
<g/>
)	)	kIx)	)
svody	svod	k1gInPc4	svod
==	==	k?	==
</s>
</p>
<p>
<s>
Historicky	historicky	k6eAd1	historicky
zavedl	zavést	k5eAaPmAgInS	zavést
elektrokardiografii	elektrokardiografie	k1gFnSc4	elektrokardiografie
jako	jako	k8xS	jako
klinickou	klinický	k2eAgFnSc4d1	klinická
metodu	metoda	k1gFnSc4	metoda
r.	r.	kA	r.
1906	[number]	k4	1906
holandský	holandský	k2eAgInSc1d1	holandský
lékař	lékař	k1gMnSc1	lékař
Willem	Willo	k1gNnSc7	Willo
Einthoven	Einthovna	k1gFnPc2	Einthovna
(	(	kIx(	(
<g/>
čti	číst	k5eAaImRp2nS	číst
<g/>
:	:	kIx,	:
Einthofen	Einthofen	k1gInSc1	Einthofen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
EKG	EKG	kA	EKG
signál	signál	k1gInSc1	signál
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
strunovým	strunový	k2eAgInSc7d1	strunový
galvanometrem	galvanometr	k1gInSc7	galvanometr
mezi	mezi	k7c7	mezi
horními	horní	k2eAgFnPc7d1	horní
končetinami	končetina	k1gFnPc7	končetina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
snadnost	snadnost	k1gFnSc4	snadnost
připojení	připojení	k1gNnSc4	připojení
elektrod	elektroda	k1gFnPc2	elektroda
na	na	k7c4	na
zápěstí	zápěstí	k1gNnSc4	zápěstí
<g/>
.	.	kIx.	.
</s>
<s>
Měřený	měřený	k2eAgInSc1d1	měřený
signál	signál	k1gInSc1	signál
pak	pak	k6eAd1	pak
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
rozdílu	rozdíl	k1gInSc2	rozdíl
potenciálů	potenciál	k1gInPc2	potenciál
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
elektrodami	elektroda	k1gFnPc7	elektroda
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
o	o	k7c4	o
bipolární	bipolární	k2eAgNnSc4d1	bipolární
zapojení	zapojení	k1gNnSc4	zapojení
<g/>
.	.	kIx.	.
</s>
<s>
Označíme	označit	k5eAaPmIp1nP	označit
<g/>
-li	i	k?	-li
pravou	pravý	k2eAgFnSc4d1	pravá
ruku	ruka	k1gFnSc4	ruka
písmenem	písmeno	k1gNnSc7	písmeno
R	R	kA	R
(	(	kIx(	(
<g/>
right	right	k1gMnSc1	right
<g/>
,	,	kIx,	,
standardně	standardně	k6eAd1	standardně
označena	označen	k2eAgFnSc1d1	označena
červenou	červený	k2eAgFnSc7d1	červená
barvou	barva	k1gFnSc7	barva
<g/>
)	)	kIx)	)
a	a	k8xC	a
levou	levý	k2eAgFnSc7d1	levá
L	L	kA	L
(	(	kIx(	(
<g/>
left	left	k1gMnSc1	left
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
signál	signál	k1gInSc4	signál
L-R	L-R	k1gFnSc2	L-R
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
I.	I.	kA	I.
Einthovenův	Einthovenův	k2eAgInSc4d1	Einthovenův
svod	svod	k1gInSc4	svod
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
další	další	k2eAgFnSc1d1	další
elektroda	elektroda	k1gFnSc1	elektroda
připevněna	připevnit	k5eAaPmNgFnS	připevnit
poblíž	poblíž	k7c2	poblíž
kotníku	kotník	k1gInSc2	kotník
levé	levý	k2eAgFnSc2d1	levá
nohy	noha	k1gFnSc2	noha
F	F	kA	F
(	(	kIx(	(
<g/>
foot	foot	k1gMnSc1	foot
<g/>
,	,	kIx,	,
zelená	zelenat	k5eAaImIp3nS	zelenat
<g/>
)	)	kIx)	)
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
možnost	možnost	k1gFnSc4	možnost
měřit	měřit	k5eAaImF	měřit
rozdíl	rozdíl	k1gInSc4	rozdíl
potenciálů	potenciál	k1gInPc2	potenciál
F-R	F-R	k1gMnSc1	F-R
(	(	kIx(	(
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Einthovenův	Einthovenův	k2eAgInSc1d1	Einthovenův
svod	svod	k1gInSc1	svod
<g/>
)	)	kIx)	)
a	a	k8xC	a
F-L	F-L	k1gMnSc1	F-L
(	(	kIx(	(
<g/>
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Einthovenův	Einthovenův	k2eAgInSc1d1	Einthovenův
svod	svod	k1gInSc1	svod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Elektroda	elektroda	k1gFnSc1	elektroda
N	N	kA	N
(	(	kIx(	(
<g/>
neutrální	neutrální	k2eAgFnSc1d1	neutrální
-	-	kIx~	-
černá	černý	k2eAgFnSc1d1	černá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
připojuje	připojovat	k5eAaImIp3nS	připojovat
na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
nohu	noha	k1gFnSc4	noha
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
do	do	k7c2	do
vlastního	vlastní	k2eAgNnSc2d1	vlastní
snímání	snímání	k1gNnSc2	snímání
nezapočítává	započítávat	k5eNaImIp3nS	započítávat
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
uzemnění	uzemněný	k2eAgMnPc1d1	uzemněný
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Pouze	pouze	k6eAd1	pouze
<g/>
"	"	kIx"	"
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
ji	on	k3xPp3gFnSc4	on
beztrestně	beztrestně	k6eAd1	beztrestně
vynechat	vynechat	k5eAaPmF	vynechat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
pak	pak	k6eAd1	pak
by	by	kYmCp3nS	by
měření	měření	k1gNnSc1	měření
bylo	být	k5eAaImAgNnS	být
narušeno	narušit	k5eAaPmNgNnS	narušit
různými	různý	k2eAgFnPc7d1	různá
poruchami	porucha	k1gFnPc7	porucha
a	a	k8xC	a
hrozilo	hrozit	k5eAaImAgNnS	hrozit
by	by	kYmCp3nS	by
i	i	k9	i
poškození	poškození	k1gNnSc1	poškození
citlivých	citlivý	k2eAgInPc2d1	citlivý
vstupních	vstupní	k2eAgInPc2d1	vstupní
zesilovačů	zesilovač	k1gInPc2	zesilovač
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vektor	vektor	k1gInSc4	vektor
srdeční	srdeční	k2eAgFnSc2d1	srdeční
osy	osa	k1gFnSc2	osa
==	==	k?	==
</s>
</p>
<p>
<s>
Jaký	jaký	k3yQgInSc1	jaký
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
sledovat	sledovat	k5eAaImF	sledovat
signál	signál	k1gInSc4	signál
od	od	k7c2	od
jednoho	jeden	k4xCgInSc2	jeden
zdroje	zdroj	k1gInSc2	zdroj
(	(	kIx(	(
<g/>
myokardu	myokard	k1gInSc2	myokard
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
snímaný	snímaný	k2eAgMnSc1d1	snímaný
zároveň	zároveň	k6eAd1	zároveň
z	z	k7c2	z
několika	několik	k4yIc2	několik
elektrod	elektroda	k1gFnPc2	elektroda
<g/>
?	?	kIx.	?
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
si	se	k3xPyFc3	se
představit	představit	k5eAaPmF	představit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sumační	sumační	k2eAgInSc1d1	sumační
potenciál	potenciál	k1gInSc1	potenciál
všech	všecek	k3xTgFnPc2	všecek
buněk	buňka	k1gFnPc2	buňka
myokardu	myokard	k1gInSc2	myokard
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
jakýsi	jakýsi	k3yIgInSc4	jakýsi
elektrický	elektrický	k2eAgInSc4d1	elektrický
dipól	dipól	k1gInSc4	dipól
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
srdeční	srdeční	k2eAgFnSc2d1	srdeční
periody	perioda	k1gFnSc2	perioda
mění	měnit	k5eAaImIp3nS	měnit
svůj	svůj	k3xOyFgInSc4	svůj
směr	směr	k1gInSc4	směr
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
velikost	velikost	k1gFnSc4	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
pomyslný	pomyslný	k2eAgInSc4d1	pomyslný
vektor	vektor	k1gInSc4	vektor
nazýváme	nazývat	k5eAaImIp1nP	nazývat
vektorem	vektor	k1gInSc7	vektor
elektrické	elektrický	k2eAgFnSc2d1	elektrická
srdeční	srdeční	k2eAgFnSc2d1	srdeční
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc4	jeho
velikost	velikost	k1gFnSc4	velikost
i	i	k8xC	i
směr	směr	k1gInSc4	směr
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nabývají	nabývat	k5eAaImIp3nP	nabývat
maxima	maximum	k1gNnSc2	maximum
různé	různý	k2eAgFnSc2d1	různá
vlny	vlna	k1gFnSc2	vlna
EKG	EKG	kA	EKG
záznamu	záznam	k1gInSc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
a	a	k8xC	a
nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
je	být	k5eAaImIp3nS	být
směr	směr	k1gInSc1	směr
vektoru	vektor	k1gInSc2	vektor
elektrické	elektrický	k2eAgFnSc2d1	elektrická
srdeční	srdeční	k2eAgFnSc2d1	srdeční
osy	osa	k1gFnSc2	osa
pro	pro	k7c4	pro
komplex	komplex	k1gInSc4	komplex
QRS	QRS	kA	QRS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Einthovenův	Einthovenův	k2eAgInSc1d1	Einthovenův
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
==	==	k?	==
</s>
</p>
<p>
<s>
Představíme	představit	k5eAaPmIp1nP	představit
<g/>
-li	i	k?	-li
si	se	k3xPyFc3	se
nyní	nyní	k6eAd1	nyní
bipolárně	bipolárně	k6eAd1	bipolárně
zapojené	zapojený	k2eAgInPc4d1	zapojený
Einthovenovy	Einthovenův	k2eAgInPc4d1	Einthovenův
svody	svod	k1gInPc4	svod
I	I	kA	I
<g/>
,	,	kIx,	,
II	II	kA	II
a	a	k8xC	a
III	III	kA	III
jako	jako	k9	jako
strany	strana	k1gFnSc2	strana
rovnostranného	rovnostranný	k2eAgInSc2d1	rovnostranný
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Einthovenova	Einthovenův	k2eAgInSc2d1	Einthovenův
<g/>
)	)	kIx)	)
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInPc6	jehož
vrcholech	vrchol	k1gInPc6	vrchol
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
elektrody	elektroda	k1gFnPc1	elektroda
R	R	kA	R
<g/>
,	,	kIx,	,
L	L	kA	L
a	a	k8xC	a
F	F	kA	F
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
nám	my	k3xPp1nPc3	my
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
souřadný	souřadný	k2eAgInSc1d1	souřadný
systém	systém	k1gInSc1	systém
tří	tři	k4xCgFnPc2	tři
os	osa	k1gFnPc2	osa
<g/>
,	,	kIx,	,
vzájemně	vzájemně	k6eAd1	vzájemně
natočených	natočený	k2eAgInPc2d1	natočený
o	o	k7c6	o
60	[number]	k4	60
stupňů	stupeň	k1gInPc2	stupeň
(	(	kIx(	(
<g/>
počítáme	počítat	k5eAaImIp1nP	počítat
i	i	k9	i
opačné	opačný	k2eAgInPc1d1	opačný
směry	směr	k1gInPc1	směr
os	osa	k1gFnPc2	osa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
promítá	promítat	k5eAaImIp3nS	promítat
vektor	vektor	k1gInSc1	vektor
srdeční	srdeční	k2eAgFnSc2d1	srdeční
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
polarity	polarita	k1gFnSc2	polarita
a	a	k8xC	a
velikostí	velikost	k1gFnSc7	velikost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
vln	vlna	k1gFnPc2	vlna
EKG	EKG	kA	EKG
záznamu	záznam	k1gInSc2	záznam
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
svodech	svod	k1gInPc6	svod
pak	pak	k6eAd1	pak
můžeme	moct	k5eAaImIp1nP	moct
spočítat	spočítat	k5eAaPmF	spočítat
<g/>
,	,	kIx,	,
či	či	k8xC	či
alespoň	alespoň	k9	alespoň
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
,	,	kIx,	,
natočení	natočení	k1gNnSc1	natočení
vektoru	vektor	k1gInSc2	vektor
elektrické	elektrický	k2eAgFnSc2d1	elektrická
srdeční	srdeční	k2eAgFnSc2d1	srdeční
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
např.	např.	kA	např.
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vlna	vlna	k1gFnSc1	vlna
R	R	kA	R
jeví	jevit	k5eAaImIp3nS	jevit
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
ve	v	k7c6	v
II	II	kA	II
<g/>
.	.	kIx.	.
svodu	svod	k1gInSc2	svod
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
můžeme	moct	k5eAaImIp1nP	moct
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
vektor	vektor	k1gInSc1	vektor
elektrické	elektrický	k2eAgFnSc2d1	elektrická
srdeční	srdeční	k2eAgFnSc2d1	srdeční
osy	osa	k1gFnSc2	osa
leží	ležet	k5eAaImIp3nS	ležet
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
strany	strana	k1gFnSc2	strana
Einthovenova	Einthovenův	k2eAgInSc2d1	Einthovenův
trojúhelníka	trojúhelník	k1gInSc2	trojúhelník
<g/>
,	,	kIx,	,
representující	representující	k2eAgFnSc1d1	representující
II	II	kA	II
<g/>
.	.	kIx.	.
svod	svod	k1gInSc1	svod
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
vpravo	vpravo	k6eAd1	vpravo
dolů	dolů	k6eAd1	dolů
(	(	kIx(	(
<g/>
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
proti	proti	k7c3	proti
pacientovi	pacient	k1gMnSc6	pacient
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
normální	normální	k2eAgInSc4d1	normální
(	(	kIx(	(
<g/>
obvyklý	obvyklý	k2eAgInSc4d1	obvyklý
<g/>
)	)	kIx)	)
sklon	sklon	k1gInSc4	sklon
elektrické	elektrický	k2eAgFnSc2d1	elektrická
srdeční	srdeční	k2eAgFnSc2d1	srdeční
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Směr	směr	k1gInSc1	směr
vodorovně	vodorovně	k6eAd1	vodorovně
vpravo	vpravo	k6eAd1	vpravo
označuje	označovat	k5eAaImIp3nS	označovat
0	[number]	k4	0
stupňů	stupeň	k1gInPc2	stupeň
a	a	k8xC	a
úhlové	úhlový	k2eAgInPc1d1	úhlový
stupně	stupeň	k1gInPc1	stupeň
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nP	měřit
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
směru	směr	k1gInSc2	směr
po	po	k7c6	po
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
směr	směr	k1gInSc1	směr
II	II	kA	II
<g/>
.	.	kIx.	.
kanálu	kanál	k1gInSc2	kanál
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
sklonu	sklon	k1gInSc3	sklon
srdeční	srdeční	k2eAgFnSc2d1	srdeční
osy	osa	k1gFnSc2	osa
+60	+60	k4	+60
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Odchylky	odchylka	k1gFnPc4	odchylka
od	od	k7c2	od
normy	norma	k1gFnSc2	norma
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
stočení	stočení	k1gNnSc4	stočení
elektrické	elektrický	k2eAgFnSc2d1	elektrická
osy	osa	k1gFnSc2	osa
doprava	doprava	k6eAd1	doprava
či	či	k8xC	či
doleva	doleva	k6eAd1	doleva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Goldbergerovy	Goldbergerův	k2eAgInPc4d1	Goldbergerův
(	(	kIx(	(
<g/>
unipolární	unipolární	k2eAgInPc4d1	unipolární
<g/>
)	)	kIx)	)
svody	svod	k1gInPc4	svod
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
rozlišení	rozlišení	k1gNnSc4	rozlišení
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
doplněny	doplněn	k2eAgInPc1d1	doplněn
Einthovenovy	Einthovenův	k2eAgInPc1d1	Einthovenův
svody	svod	k1gInPc1	svod
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
směry	směr	k1gInPc4	směr
<g/>
:	:	kIx,	:
Spojením	spojení	k1gNnSc7	spojení
končetinových	končetinový	k2eAgFnPc2d1	končetinová
elektrod	elektroda	k1gFnPc2	elektroda
přes	přes	k7c4	přes
stejně	stejně	k6eAd1	stejně
velké	velký	k2eAgInPc4d1	velký
odpory	odpor	k1gInPc4	odpor
byl	být	k5eAaImAgInS	být
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
virtuální	virtuální	k2eAgInSc1d1	virtuální
střed	střed	k1gInSc1	střed
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Wilsonova	Wilsonův	k2eAgFnSc1d1	Wilsonova
svorka	svorka	k1gFnSc1	svorka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
byly	být	k5eAaImAgFnP	být
zapojeny	zapojen	k2eAgInPc4d1	zapojen
referenční	referenční	k2eAgInPc4d1	referenční
vstupy	vstup	k1gInPc4	vstup
tří	tři	k4xCgInPc2	tři
dalších	další	k2eAgInPc2d1	další
diferenčních	diferenční	k2eAgInPc2d1	diferenční
zesilovačů	zesilovač	k1gInPc2	zesilovač
<g/>
.	.	kIx.	.
</s>
<s>
Vektory	vektor	k1gInPc1	vektor
nových	nový	k2eAgFnPc2d1	nová
souřadných	souřadný	k2eAgFnPc2d1	souřadná
os	osa	k1gFnPc2	osa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tak	tak	k6eAd1	tak
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
můžeme	moct	k5eAaImIp1nP	moct
představit	představit	k5eAaPmF	představit
jako	jako	k9	jako
šipky	šipka	k1gFnPc1	šipka
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgFnPc1d1	vedoucí
ze	z	k7c2	z
středu	střed	k1gInSc2	střed
(	(	kIx(	(
<g/>
z	z	k7c2	z
těžiště	těžiště	k1gNnSc2	těžiště
<g/>
)	)	kIx)	)
rovnostranného	rovnostranný	k2eAgInSc2d1	rovnostranný
Einthovenova	Einthovenův	k2eAgInSc2d1	Einthovenův
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
vrcholům	vrchol	k1gInPc3	vrchol
<g/>
,	,	kIx,	,
reprezentujícím	reprezentující	k2eAgInPc3d1	reprezentující
elektrody	elektroda	k1gFnPc4	elektroda
R	R	kA	R
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
;	;	kIx,	;
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
svody	svod	k1gInPc1	svod
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
pojmenovány	pojmenovat	k5eAaPmNgInP	pojmenovat
VR	vr	k0	vr
<g/>
,	,	kIx,	,
VL	VL	kA	VL
a	a	k8xC	a
VF	VF	kA	VF
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
historickém	historický	k2eAgInSc6d1	historický
okamžiku	okamžik	k1gInSc6	okamžik
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
ještě	ještě	k6eAd1	ještě
nepoužívaly	používat	k5eNaImAgInP	používat
elektronické	elektronický	k2eAgInPc1d1	elektronický
zesilovače	zesilovač	k1gInPc1	zesilovač
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
závadu	závada	k1gFnSc4	závada
<g/>
,	,	kIx,	,
že	že	k8xS	že
těžnice	těžnice	k1gFnSc1	těžnice
trojúhelníka	trojúhelník	k1gInSc2	trojúhelník
VR	vr	k0	vr
<g/>
,	,	kIx,	,
VL	VL	kA	VL
a	a	k8xC	a
VF	VF	kA	VF
jsou	být	k5eAaImIp3nP	být
kratší	krátký	k2eAgFnPc1d2	kratší
než	než	k8xS	než
jeho	jeho	k3xOp3gFnPc1	jeho
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
i	i	k8xC	i
získaný	získaný	k2eAgInSc1d1	získaný
signál	signál	k1gInSc1	signál
byl	být	k5eAaImAgInS	být
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
.	.	kIx.	.
</s>
<s>
Vylepšením	vylepšení	k1gNnSc7	vylepšení
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
zapojení	zapojení	k1gNnSc1	zapojení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nevytvořil	vytvořit	k5eNaPmAgInS	vytvořit
centrální	centrální	k2eAgInSc1d1	centrální
bod	bod	k1gInSc1	bod
uprostřed	uprostřed	k7c2	uprostřed
trojúhelníka	trojúhelník	k1gInSc2	trojúhelník
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
elektrody	elektroda	k1gFnPc4	elektroda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
referenční	referenční	k2eAgInSc4d1	referenční
bod	bod	k1gInSc4	bod
byl	být	k5eAaImAgInS	být
vytvořen	vytvořen	k2eAgInSc1d1	vytvořen
bod	bod	k1gInSc1	bod
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
odporů	odpor	k1gInPc2	odpor
<g/>
,	,	kIx,	,
spojujících	spojující	k2eAgMnPc6d1	spojující
zbývající	zbývající	k2eAgFnPc4d1	zbývající
elektrody	elektroda	k1gFnPc4	elektroda
<g/>
.	.	kIx.	.
</s>
<s>
Geometricky	geometricky	k6eAd1	geometricky
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šipky	šipka	k1gFnPc1	šipka
vektorů	vektor	k1gInPc2	vektor
nevycházejí	vycházet	k5eNaImIp3nP	vycházet
se	se	k3xPyFc4	se
středu	středa	k1gFnSc4	středa
(	(	kIx(	(
<g/>
těžiště	těžiště	k1gNnSc2	těžiště
<g/>
)	)	kIx)	)
trojúhelníka	trojúhelník	k1gInSc2	trojúhelník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ze	z	k7c2	z
středů	střed	k1gInPc2	střed
protilehlých	protilehlý	k2eAgFnPc2d1	protilehlá
stran	strana	k1gFnPc2	strana
<g/>
;	;	kIx,	;
nejsou	být	k5eNaImIp3nP	být
to	ten	k3xDgNnSc1	ten
tudíž	tudíž	k8xC	tudíž
těžnice	těžnice	k1gFnSc1	těžnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výšky	výška	k1gFnSc2	výška
trojúhelníka	trojúhelník	k1gInSc2	trojúhelník
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gInSc1	jejich
směr	směr	k1gInSc1	směr
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc2	jejich
délky	délka	k1gFnSc2	délka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
i	i	k8xC	i
velikost	velikost	k1gFnSc1	velikost
získaného	získaný	k2eAgInSc2d1	získaný
signálu	signál	k1gInSc2	signál
<g/>
,	,	kIx,	,
o	o	k7c6	o
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
vyšší	vysoký	k2eAgFnSc7d2	vyšší
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
písmenkem	písmenko	k1gNnSc7	písmenko
'	'	kIx"	'
<g/>
a	a	k8xC	a
<g/>
'	'	kIx"	'
jakožto	jakožto	k8xS	jakožto
'	'	kIx"	'
<g/>
augmentované	augmentovaný	k2eAgInPc4d1	augmentovaný
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
prodloužené	prodloužená	k1gFnSc2	prodloužená
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
osvětlujeme	osvětlovat	k5eAaImIp1nP	osvětlovat
dodnes	dodnes	k6eAd1	dodnes
používané	používaný	k2eAgNnSc4d1	používané
označení	označení	k1gNnSc4	označení
odpovídajících	odpovídající	k2eAgInPc2d1	odpovídající
svodů	svod	k1gInPc2	svod
jako	jako	k8xC	jako
aVR	aVR	k?	aVR
<g/>
,	,	kIx,	,
aVL	aVL	k?	aVL
<g/>
,	,	kIx,	,
aVF	aVF	k?	aVF
<g/>
.	.	kIx.	.
</s>
<s>
Říkáme	říkat	k5eAaImIp1nP	říkat
jim	on	k3xPp3gFnPc3	on
Goldbergerovy	Goldbergerův	k2eAgInPc1d1	Goldbergerův
svody	svod	k1gInPc1	svod
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Einthovenových	Einthovenův	k2eAgInPc2d1	Einthovenův
bipolárních	bipolární	k2eAgInPc2d1	bipolární
svodů	svod	k1gInPc2	svod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
každý	každý	k3xTgInSc1	každý
svod	svod	k1gInSc1	svod
representuje	representovat	k5eAaImIp3nS	representovat
rozdíl	rozdíl	k1gInSc4	rozdíl
potenciálu	potenciál	k1gInSc2	potenciál
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
elektrodami	elektroda	k1gFnPc7	elektroda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
svody	svod	k1gInPc4	svod
unipolární	unipolární	k2eAgInPc4d1	unipolární
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
každý	každý	k3xTgInSc1	každý
svod	svod	k1gInSc1	svod
representuje	representovat	k5eAaImIp3nS	representovat
potenciál	potenciál	k1gInSc4	potenciál
jen	jen	k9	jen
jedné	jeden	k4xCgFnSc2	jeden
příslušné	příslušný	k2eAgFnSc2d1	příslušná
elektrody	elektroda	k1gFnSc2	elektroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Standardní	standardní	k2eAgInPc1d1	standardní
končetinové	končetinový	k2eAgInPc1d1	končetinový
svody	svod	k1gInPc1	svod
==	==	k?	==
</s>
</p>
<p>
<s>
Doplněním	doplnění	k1gNnSc7	doplnění
Einthovenových	Einthovenův	k2eAgInPc2d1	Einthovenův
bipolárních	bipolární	k2eAgInPc2d1	bipolární
svodů	svod	k1gInPc2	svod
I	I	kA	I
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
,	,	kIx,	,
III	III	kA	III
o	o	k7c4	o
Goldbergerovy	Goldbergerův	k2eAgInPc4d1	Goldbergerův
unipolární	unipolární	k2eAgInPc4d1	unipolární
svody	svod	k1gInPc4	svod
aVR	aVR	k?	aVR
<g/>
,	,	kIx,	,
aVL	aVL	k?	aVL
a	a	k8xC	a
aVF	aVF	k?	aVF
získáme	získat	k5eAaPmIp1nP	získat
celkem	celkem	k6eAd1	celkem
6	[number]	k4	6
os	osa	k1gFnPc2	osa
<g/>
,	,	kIx,	,
vzájemně	vzájemně	k6eAd1	vzájemně
natočených	natočený	k2eAgInPc2d1	natočený
o	o	k7c6	o
30	[number]	k4	30
stupňů	stupeň	k1gInPc2	stupeň
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
promítat	promítat	k5eAaImF	promítat
vektor	vektor	k1gInSc4	vektor
elektrické	elektrický	k2eAgFnSc2d1	elektrická
srdeční	srdeční	k2eAgFnSc2d1	srdeční
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
všech	všecek	k3xTgInPc2	všecek
šest	šest	k4xCc4	šest
uvedených	uvedený	k2eAgInPc2d1	uvedený
svodů	svod	k1gInPc2	svod
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
potenciálu	potenciál	k1gInSc2	potenciál
tří	tři	k4xCgFnPc2	tři
končetinových	končetinový	k2eAgFnPc2d1	končetinová
elektrod	elektroda	k1gFnPc2	elektroda
<g/>
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
je	on	k3xPp3gInPc4	on
šesti	šest	k4xCc7	šest
standardními	standardní	k2eAgInPc7d1	standardní
končetinovými	končetinový	k2eAgInPc7d1	končetinový
svody	svod	k1gInPc7	svod
<g/>
.	.	kIx.	.
</s>
<s>
Rovina	rovina	k1gFnSc1	rovina
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
odpovídající	odpovídající	k2eAgFnPc4d1	odpovídající
souřadné	souřadný	k2eAgFnPc4d1	souřadná
osy	osa	k1gFnPc4	osa
leží	ležet	k5eAaImIp3nP	ležet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
s	s	k7c7	s
plochou	plocha	k1gFnSc7	plocha
stolu	stol	k1gInSc2	stol
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgNnSc6	který
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
zádech	zádech	k1gInSc4	zádech
vyšetřovaný	vyšetřovaný	k1gMnSc1	vyšetřovaný
pacient	pacient	k1gMnSc1	pacient
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hrudní	hrudní	k2eAgInPc1d1	hrudní
svody	svod	k1gInPc1	svod
==	==	k?	==
</s>
</p>
<p>
<s>
Průběhem	průběh	k1gInSc7	průběh
doby	doba	k1gFnSc2	doba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
potřeba	potřeba	k1gFnSc1	potřeba
vyšetřovat	vyšetřovat	k5eAaImF	vyšetřovat
pohyb	pohyb	k1gInSc4	pohyb
elektrického	elektrický	k2eAgInSc2d1	elektrický
srdečního	srdeční	k2eAgInSc2d1	srdeční
vektoru	vektor	k1gInSc2	vektor
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
umístit	umístit	k5eAaPmF	umístit
elektrody	elektroda	k1gFnPc4	elektroda
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
kolmé	kolmý	k2eAgFnPc1d1	kolmá
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
rovinu	rovina	k1gFnSc4	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
pomocí	pomocí	k7c2	pomocí
šesti	šest	k4xCc2	šest
elektrod	elektroda	k1gFnPc2	elektroda
V1	V1	k1gFnPc2	V1
až	až	k9	až
V	v	k7c4	v
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
umístěných	umístěný	k2eAgFnPc2d1	umístěná
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
hrudníku	hrudník	k1gInSc6	hrudník
vyšetřované	vyšetřovaný	k2eAgFnSc2d1	vyšetřovaná
osoby	osoba	k1gFnSc2	osoba
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
elektrody	elektroda	k1gFnPc4	elektroda
V1	V1	k1gMnPc2	V1
a	a	k8xC	a
V2	V2	k1gMnPc2	V2
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
mezižebří	mezižebří	k1gNnSc6	mezižebří
vpravo	vpravo	k6eAd1	vpravo
a	a	k8xC	a
vlevo	vlevo	k6eAd1	vlevo
od	od	k7c2	od
sterna	sternum	k1gNnSc2	sternum
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vlevo	vlevo	k6eAd1	vlevo
elektroda	elektroda	k1gFnSc1	elektroda
V3	V3	k1gFnSc2	V3
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
stále	stále	k6eAd1	stále
ekvidistantně	ekvidistantně	k6eAd1	ekvidistantně
umísťované	umísťovaný	k2eAgFnPc4d1	umísťovaná
elektrody	elektroda	k1gFnPc4	elektroda
V	v	k7c4	v
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
V5	V5	k1gFnSc1	V5
a	a	k8xC	a
V6	V6	k1gFnSc1	V6
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
pátém	pátý	k4xOgNnSc6	pátý
mezižebří	mezižebří	k1gNnSc6	mezižebří
<g/>
:	:	kIx,	:
V4	V4	k1gMnSc1	V4
v	v	k7c6	v
čáře	čára	k1gFnSc6	čára
probíhající	probíhající	k2eAgFnSc6d1	probíhající
středem	středem	k7c2	středem
levého	levý	k2eAgInSc2d1	levý
klíčku	klíček	k1gInSc2	klíček
<g/>
,	,	kIx,	,
V5	V5	k1gFnSc1	V5
v	v	k7c6	v
čáře	čára	k1gFnSc6	čára
probíhající	probíhající	k2eAgFnSc7d1	probíhající
přední	přední	k2eAgFnSc7d1	přední
řasou	řasa	k1gFnSc7	řasa
podpažní	podpažní	k2eAgFnSc2d1	podpažní
jamky	jamka	k1gFnSc2	jamka
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
V6	V6	k1gMnPc1	V6
v	v	k7c6	v
čáře	čára	k1gFnSc6	čára
pod	pod	k7c7	pod
středem	střed	k1gInSc7	střed
podpažní	podpažní	k2eAgFnSc2d1	podpažní
jamky	jamka	k1gFnSc2	jamka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Posouzení	posouzení	k1gNnSc1	posouzení
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
srdeční	srdeční	k2eAgFnSc1d1	srdeční
aktivita	aktivita	k1gFnSc1	aktivita
normální	normální	k2eAgFnSc1d1	normální
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejím	její	k3xOp3gFnPc3	její
poruchám	porucha	k1gFnPc3	porucha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
akutní	akutní	k2eAgMnSc1d1	akutní
či	či	k8xC	či
proběhlé	proběhlý	k2eAgNnSc1d1	proběhlé
poškození	poškození	k1gNnSc1	poškození
srdečního	srdeční	k2eAgInSc2d1	srdeční
svalu	sval	k1gInSc2	sval
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
infarkt	infarkt	k1gInSc4	infarkt
myokardu	myokard	k1gInSc2	myokard
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Může	moct	k5eAaImIp3nS	moct
odhalit	odhalit	k5eAaPmF	odhalit
poruchy	porucha	k1gFnPc4	porucha
distribuce	distribuce	k1gFnSc2	distribuce
elektrolytů	elektrolyt	k1gInPc2	elektrolyt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Detekce	detekce	k1gFnSc1	detekce
poruchy	porucha	k1gFnSc2	porucha
převodního	převodní	k2eAgInSc2d1	převodní
systému	systém	k1gInSc2	systém
srdečního	srdeční	k2eAgNnSc2d1	srdeční
a	a	k8xC	a
blokád	blokáda	k1gFnPc2	blokáda
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nástroj	nástroj	k1gInSc1	nástroj
pro	pro	k7c4	pro
screening	screening	k1gInSc4	screening
ischemické	ischemický	k2eAgFnSc2d1	ischemická
choroby	choroba	k1gFnSc2	choroba
srdeční	srdeční	k2eAgFnSc2d1	srdeční
během	během	k7c2	během
zátěžových	zátěžový	k2eAgInPc2d1	zátěžový
testů	test	k1gInPc2	test
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Elektroencefalograf	elektroencefalograf	k1gInSc1	elektroencefalograf
</s>
</p>
<p>
<s>
Převodní	převodní	k2eAgInSc1d1	převodní
systém	systém	k1gInSc1	systém
srdeční	srdeční	k2eAgFnSc2d1	srdeční
</s>
</p>
<p>
<s>
Srdeční	srdeční	k2eAgFnSc1d1	srdeční
arytmie	arytmie	k1gFnSc1	arytmie
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
EKG	EKG	kA	EKG
kniha	kniha	k1gFnSc1	kniha
(	(	kIx(	(
<g/>
1400	[number]	k4	1400
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Elektrokardiogram	elektrokardiogram	k1gInSc1	elektrokardiogram
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
EKG	EKG	kA	EKG
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Výukový	výukový	k2eAgInSc4d1	výukový
kurs	kurs	k1gInSc4	kurs
EKG	EKG	kA	EKG
vyšetření	vyšetření	k1gNnSc1	vyšetření
ve	v	k7c6	v
Wikiverzitě	Wikiverzita	k1gFnSc6	Wikiverzita
</s>
</p>
