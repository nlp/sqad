<s>
Elektrokardiogram	elektrokardiogram	k1gInSc1	elektrokardiogram
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
EKG	EKG	kA	EKG
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
záznam	záznam	k1gInSc4	záznam
časové	časový	k2eAgFnSc2d1	časová
změny	změna	k1gFnSc2	změna
elektrického	elektrický	k2eAgInSc2d1	elektrický
potenciálu	potenciál	k1gInSc2	potenciál
způsobeného	způsobený	k2eAgInSc2d1	způsobený
srdeční	srdeční	k2eAgInSc4d1	srdeční
aktivitou	aktivita	k1gFnSc7	aktivita
<g/>
.	.	kIx.	.
</s>
