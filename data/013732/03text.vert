<s>
Vladimír	Vladimír	k1gMnSc1
Krejčík	Krejčík	k1gMnSc1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
KrejčíkZákladní	KrejčíkZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Narození	narození	k1gNnPc4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1931	#num#	k4
Uhříněves	Uhříněves	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2013	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
81	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Brno	Brno	k1gNnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
operní	operní	k2eAgMnSc1d1
pěvec	pěvec	k1gMnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
lidský	lidský	k2eAgInSc1d1
hlas	hlas	k1gInSc1
Hlasový	hlasový	k2eAgInSc4d1
obor	obor	k1gInSc4
</s>
<s>
tenor	tenor	k1gInSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
Thálie	Thálie	k1gFnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Krejčík	Krejčík	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1931	#num#	k4
Uhříněves	Uhříněves	k1gFnSc1
–	–	k?
21	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2013	#num#	k4
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
operní	operní	k2eAgMnSc1d1
pěvec	pěvec	k1gMnSc1
(	(	kIx(
<g/>
tenor	tenor	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Uhříněvsi	Uhříněves	k1gFnSc6
a	a	k8xC
od	od	k7c2
dětství	dětství	k1gNnSc2
zpíval	zpívat	k5eAaImAgMnS
ve	v	k7c6
sboru	sbor	k1gInSc6
při	při	k7c6
evangelickém	evangelický	k2eAgInSc6d1
kostele	kostel	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dokončení	dokončení	k1gNnSc6
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
však	však	k9
studoval	studovat	k5eAaImAgMnS
v	v	k7c6
letech	léto	k1gNnPc6
1948	#num#	k4
až	až	k9
1952	#num#	k4
na	na	k7c6
Vyšší	vysoký	k2eAgFnSc6d2
průmyslové	průmyslový	k2eAgFnSc6d1
škole	škola	k1gFnSc6
chemické	chemický	k2eAgFnSc6d1
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
když	když	k8xS
nastoupil	nastoupit	k5eAaPmAgMnS
do	do	k7c2
vojenské	vojenský	k2eAgFnSc2d1
službu	služba	k1gFnSc4
ve	v	k7c6
Znojmě	Znojmo	k1gNnSc6
<g/>
,	,	kIx,
zapojil	zapojit	k5eAaPmAgMnS
se	se	k3xPyFc4
zde	zde	k6eAd1
do	do	k7c2
činnosti	činnost	k1gFnSc2
armádního	armádní	k2eAgInSc2d1
uměleckého	umělecký	k2eAgInSc2d1
souboru	soubor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osudným	osudný	k2eAgNnSc7d1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
účast	účast	k1gFnSc1
v	v	k7c6
pěvecké	pěvecký	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
ho	on	k3xPp3gNnSc4
povšiml	povšimnout	k5eAaPmAgMnS
profesor	profesor	k1gMnSc1
Pražské	pražský	k2eAgFnSc2d1
konzervatoře	konzervatoř	k1gFnSc2
a	a	k8xC
hlasový	hlasový	k2eAgMnSc1d1
poradce	poradce	k1gMnSc1
Armádního	armádní	k2eAgInSc2d1
uměleckého	umělecký	k2eAgInSc2d1
souboru	soubor	k1gInSc2
Víta	Vít	k1gMnSc2
Nejedlého	Nejedlý	k1gMnSc2
Vojtěch	Vojtěch	k1gMnSc1
Bořivoj	Bořivoj	k1gMnSc1
Aim	Aim	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
ukončení	ukončení	k1gNnSc6
vojenské	vojenský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
byl	být	k5eAaImAgInS
na	na	k7c4
Aimovo	Aimův	k2eAgNnSc4d1
doporučení	doporučení	k1gNnSc4
angažován	angažovat	k5eAaBmNgMnS
do	do	k7c2
operního	operní	k2eAgInSc2d1
sboru	sbor	k1gInSc2
tehdejšího	tehdejší	k2eAgNnSc2d1
Smetanova	Smetanův	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
soukromě	soukromě	k6eAd1
studoval	studovat	k5eAaImAgMnS
zpěv	zpěv	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
sólistou	sólista	k1gMnSc7
Divadla	divadlo	k1gNnSc2
F.	F.	kA
X.	X.	kA
Šaldy	Šalda	k1gMnSc2
v	v	k7c6
Liberci	Liberec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dvouletém	dvouletý	k2eAgNnSc6d1
angažmá	angažmá	k1gNnSc6
se	se	k3xPyFc4
zde	zde	k6eAd1
představil	představit	k5eAaPmAgMnS
jako	jako	k8xS,k8xC
Smetanův	Smetanův	k2eAgMnSc1d1
Jeník	Jeník	k1gMnSc1
z	z	k7c2
Prodané	prodaný	k2eAgFnSc2d1
nevěsty	nevěsta	k1gFnSc2
a	a	k8xC
Vítet	Vítet	k1gInSc1
z	z	k7c2
Dalibora	Dalibor	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
třeba	třeba	k9
Rechtora	rechtor	k1gMnSc4
z	z	k7c2
Janáčkových	Janáčkových	k2eAgFnPc2d1
Příhod	příhoda	k1gFnPc2
lišky	liška	k1gFnSc2
Bystroušky	Bystrouška	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
přešel	přejít	k5eAaPmAgMnS
do	do	k7c2
brněnského	brněnský	k2eAgNnSc2d1
Státního	státní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působil	působit	k5eAaImAgMnS
zde	zde	k6eAd1
jako	jako	k8xS,k8xC
sólista	sólista	k1gMnSc1
do	do	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
důchodu	důchod	k1gInSc2
a	a	k8xC
během	během	k7c2
devadesátých	devadesátý	k4xOgNnPc2
let	léto	k1gNnPc2
se	se	k3xPyFc4
ještě	ještě	k6eAd1
příležitostně	příležitostně	k6eAd1
objevoval	objevovat	k5eAaImAgInS
v	v	k7c6
menších	malý	k2eAgFnPc6d2
rolích	role	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Brně	Brno	k1gNnSc6
vytvořil	vytvořit	k5eAaPmAgMnS
dlouhou	dlouhý	k2eAgFnSc4d1
plejádu	plejáda	k1gFnSc4
různorodých	různorodý	k2eAgFnPc2d1
rolí	role	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
patřily	patřit	k5eAaImAgFnP
významné	významný	k2eAgFnPc1d1
postavy	postava	k1gFnPc1
české	český	k2eAgFnSc2d1
i	i	k8xC
světové	světový	k2eAgFnSc2d1
operní	operní	k2eAgFnSc2d1
scény	scéna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
českých	český	k2eAgFnPc2d1
zejména	zejména	k9
Jeník	Jeník	k1gMnSc1
z	z	k7c2
Prodané	prodaný	k2eAgFnSc2d1
nevěsty	nevěsta	k1gFnSc2
<g/>
,	,	kIx,
Števa	Števo	k1gNnSc2
z	z	k7c2
Její	její	k3xOp3gFnSc2
pastorkyně	pastorkyně	k1gFnSc2
<g/>
,	,	kIx,
Princ	princ	k1gMnSc1
z	z	k7c2
Rusalky	rusalka	k1gFnSc2
<g/>
,	,	kIx,
Tichon	Tichon	k1gInSc4
z	z	k7c2
Káti	Káťa	k1gFnSc2
Kabanové	Kabanová	k1gFnSc2
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
z	z	k7c2
Jakobína	jakobín	k1gMnSc2
či	či	k8xC
Rechtor	rechtor	k1gMnSc1
z	z	k7c2
Příhod	příhoda	k1gFnPc2
lišky	liška	k1gFnSc2
Bystroušky	Bystrouška	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světovou	světový	k2eAgFnSc4d1
operu	opera	k1gFnSc4
zpíval	zpívat	k5eAaImAgMnS
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
jako	jako	k8xS,k8xC
Lenskij	Lenskij	k1gFnSc2
v	v	k7c6
Evženu	Evžen	k1gMnSc6
Oněginovi	Oněgin	k1gMnSc6
<g/>
,	,	kIx,
Don	Don	k1gMnSc1
Ottavio	Ottavio	k6eAd1
v	v	k7c6
Donu	Don	k1gMnSc6
Giovannim	Giovannim	k1gInSc4
<g/>
,	,	kIx,
Tamino	Tamino	k1gNnSc4
v	v	k7c6
Kouzelné	kouzelný	k2eAgFnSc6d1
flétně	flétna	k1gFnSc6
<g/>
,	,	kIx,
Don	Don	k1gMnSc1
Carlos	Carlos	k1gMnSc1
nebo	nebo	k8xC
Cavaradossi	Cavaradosse	k1gFnSc4
v	v	k7c6
Tosce	Toska	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hostoval	hostovat	k5eAaImAgMnS
také	také	k9
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
tehdy	tehdy	k6eAd1
na	na	k7c6
výjezdu	výjezd	k1gInSc6
s	s	k7c7
brněnským	brněnský	k2eAgInSc7d1
souborem	soubor	k1gInSc7
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
v	v	k7c6
Athénách	Athéna	k1gFnPc6
<g/>
,	,	kIx,
Lucembursku	Lucembursko	k1gNnSc6
nebo	nebo	k8xC
Barceloně	Barcelona	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolupracoval	spolupracovat	k5eAaImAgMnS
také	také	k9
s	s	k7c7
pražskou	pražský	k2eAgFnSc7d1
Laternou	laterna	k1gFnSc7
magikou	magika	k1gFnSc7
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
byl	být	k5eAaImAgMnS
dokonce	dokonce	k9
na	na	k7c6
zájezdě	zájezd	k1gInSc6
v	v	k7c6
New	New	k1gFnPc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
březnu	březen	k1gInSc6
2010	#num#	k4
převzal	převzít	k5eAaPmAgMnS
cenu	cena	k1gFnSc4
Thálie	Thálie	k1gFnSc2
za	za	k7c4
rok	rok	k1gInSc4
2009	#num#	k4
za	za	k7c4
celoživotní	celoživotní	k2eAgNnSc4d1
mistrovství	mistrovství	k1gNnSc4
v	v	k7c6
oboru	obor	k1gInSc6
opera	opera	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
brněnské	brněnský	k2eAgFnSc6d1
Nemocnici	nemocnice	k1gFnSc6
Milosrdných	milosrdný	k2eAgMnPc2d1
bratří	bratr	k1gMnPc2
ve	v	k7c6
středu	střed	k1gInSc6
21	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2013	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
81	#num#	k4
let	léto	k1gNnPc2
poslední	poslední	k2eAgNnSc4d1
rozloučení	rozloučení	k1gNnSc4
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
ve	v	k7c4
čtvrtek	čtvrtek	k1gInSc4
29	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
ve	v	k7c6
12	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
v	v	k7c6
obřadní	obřadní	k2eAgFnSc6d1
síni	síň	k1gFnSc6
krematoria	krematorium	k1gNnSc2
v	v	k7c6
Jihlavské	jihlavský	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Zemřel	zemřít	k5eAaPmAgMnS
tenorista	tenorista	k1gMnSc1
Vladimír	Vladimír	k1gMnSc1
Krejčík	Krejčík	k1gMnSc1
<g/>
.	.	kIx.
mestohudby	mestohudb	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
/	/	kIx~
<g/>
↑	↑	k?
Prodanou	prodaný	k2eAgFnSc4d1
nevěstu	nevěsta	k1gFnSc4
9	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
věnujeme	věnovat	k5eAaPmIp1nP,k5eAaImIp1nP
Vladimíru	Vladimír	k1gMnSc3
Krejčíkovi	Krejčík	k1gMnSc3
<g/>
.	.	kIx.
ndbrno	ndbrno	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
/	/	kIx~
<g/>
↑	↑	k?
Vladimír	Vladimír	k1gMnSc1
Krejčík	Krejčík	k1gMnSc1
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
operaplus	operaplus	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
/	/	kIx~
<g/>
↑	↑	k?
V	v	k7c6
Brně	Brno	k1gNnSc6
zemřel	zemřít	k5eAaPmAgMnS
sólista	sólista	k1gMnSc1
Janáčkovy	Janáčkův	k2eAgFnSc2d1
opery	opera	k1gFnSc2
Vladimír	Vladimír	k1gMnSc1
Krejčík	Krejčík	k1gMnSc1
<g/>
.	.	kIx.
denik	denik	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
/	/	kIx~
<g/>
↑	↑	k?
V	v	k7c6
Národním	národní	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
se	se	k3xPyFc4
udílely	udílet	k5eAaImAgFnP
ceny	cena	k1gFnPc1
Thálie	Thálie	k1gFnSc2
<g/>
.	.	kIx.
irozhlas	irozhlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
/	/	kIx~
<g/>
↑	↑	k?
Zemřel	zemřít	k5eAaPmAgMnS
pěvec	pěvec	k1gMnSc1
Vladimír	Vladimír	k1gMnSc1
Krejčík	Krejčík	k1gMnSc1
<g/>
.	.	kIx.
ct	ct	k?
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
/	/	kIx~
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000710334	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
92756526	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Česko	Česko	k1gNnSc1
</s>
