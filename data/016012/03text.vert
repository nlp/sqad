<s>
Veřejná	veřejný	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Příslušník	příslušník	k1gMnSc1
VB	VB	kA
během	během	k7c2
událostí	událost	k1gFnPc2
srpna	srpen	k1gInSc2
1968	#num#	k4
čte	číst	k5eAaImIp3nS
necenzurovaný	cenzurovaný	k2eNgInSc4d1
tisk	tisk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Škoda	škoda	k6eAd1
1202	#num#	k4
používaná	používaný	k2eAgFnSc1d1
Sborem	sborem	k6eAd1
národní	národní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
v	v	k7c6
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Lada	Lada	k1gFnSc1
2103	#num#	k4
v	v	k7c6
historických	historický	k2eAgFnPc6d1
barvách	barva	k1gFnPc6
Veřejné	veřejný	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
(	(	kIx(
<g/>
fotka	fotka	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Listopad	listopad	k1gInSc1
1985	#num#	k4
<g/>
,	,	kIx,
Veřejná	veřejný	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
asistuje	asistovat	k5eAaImIp3nS
při	při	k7c6
otevírání	otevírání	k1gNnSc6
trasy	trasa	k1gFnSc2
B	B	kA
pražského	pražský	k2eAgNnSc2d1
metra	metro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
Verejná	Verejný	k2eAgFnSc1d1
bezpečnosť	bezpečnostit	k5eAaPmRp2nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
označována	označován	k2eAgNnPc4d1
VB	VB	kA
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
v	v	k7c6
letech	let	k1gInPc6
1947	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
složkou	složka	k1gFnSc7
československého	československý	k2eAgInSc2d1
Sboru	sbor	k1gInSc2
národní	národní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
a	a	k8xC
vykonávala	vykonávat	k5eAaImAgFnS
policejní	policejní	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
ochrany	ochrana	k1gFnSc2
života	život	k1gInSc2
a	a	k8xC
zdraví	zdraví	k1gNnSc4
občanů	občan	k1gMnPc2
<g/>
,	,	kIx,
ochrany	ochrana	k1gFnSc2
veřejného	veřejný	k2eAgInSc2d1
pořádku	pořádek	k1gInSc2
<g/>
,	,	kIx,
vyšetřování	vyšetřování	k1gNnSc4
trestných	trestný	k2eAgInPc2d1
činů	čin	k1gInPc2
<g/>
,	,	kIx,
dohledu	dohled	k1gInSc2
nad	nad	k7c7
silničním	silniční	k2eAgInSc7d1
provozem	provoz	k1gInSc7
apod.	apod.	kA
VB	VB	kA
přímo	přímo	k6eAd1
podléhala	podléhat	k5eAaImAgFnS
československému	československý	k2eAgNnSc3d1
Ministerstvu	ministerstvo	k1gNnSc3
vnitra	vnitro	k1gNnSc2
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1969	#num#	k4
potom	potom	k6eAd1
jednotlivým	jednotlivý	k2eAgNnPc3d1
národním	národní	k2eAgNnPc3d1
ministerstvům	ministerstvo	k1gNnPc3
vnitra	vnitro	k1gNnSc2
(	(	kIx(
<g/>
MV	MV	kA
ČSR	ČSR	kA
a	a	k8xC
MV	MV	kA
SSR	SSR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Někdy	někdy	k6eAd1
používané	používaný	k2eAgNnSc1d1
označení	označení	k1gNnSc1
„	„	k?
<g/>
uniformovaná	uniformovaný	k2eAgFnSc1d1
složka	složka	k1gFnSc1
<g/>
“	“	k?
není	být	k5eNaImIp3nS
přesné	přesný	k2eAgNnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
řada	řada	k1gFnSc1
příslušníků	příslušník	k1gMnPc2
VB	VB	kA
z	z	k7c2
titulu	titul	k1gInSc2
funkce	funkce	k1gFnSc2
používala	používat	k5eAaImAgFnS
civilní	civilní	k2eAgInSc4d1
oděv	oděv	k1gInSc4
(	(	kIx(
<g/>
např.	např.	kA
kriminalisté	kriminalista	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Členství	členství	k1gNnSc1
příslušníků	příslušník	k1gMnPc2
VB	VB	kA
v	v	k7c6
KSČ	KSČ	kA
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
1948	#num#	k4
bylo	být	k5eAaImAgNnS
členy	člen	k1gMnPc7
KSČ	KSČ	kA
zhruba	zhruba	k6eAd1
32	#num#	k4
%	%	kIx~
příslušníků	příslušník	k1gMnPc2
VB	VB	kA
<g/>
,	,	kIx,
po	po	k7c6
Únoru	únor	k1gInSc6
1948	#num#	k4
byl	být	k5eAaImAgInS
vstup	vstup	k1gInSc1
do	do	k7c2
KSČ	KSČ	kA
otevřen	otevřít	k5eAaPmNgInS
všem	všecek	k3xTgMnPc3
příslušníkům	příslušník	k1gMnPc3
SNB	SNB	kA
a	a	k8xC
stranická	stranický	k2eAgFnSc1d1
organizovanost	organizovanost	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
95,7	95,7	k4
%	%	kIx~
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1949	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
pak	pak	k6eAd1
klesala	klesat	k5eAaImAgFnS
v	v	k7c6
důsledku	důsledek	k1gInSc6
reorganizací	reorganizace	k1gFnPc2
<g/>
,	,	kIx,
propouštění	propouštění	k1gNnSc1
politicky	politicky	k6eAd1
nevhodných	vhodný	k2eNgMnPc2d1
příslušníků	příslušník	k1gMnPc2
<g/>
,	,	kIx,
přeregistrací	přeregistrace	k1gFnPc2
členů	člen	k1gInPc2
strany	strana	k1gFnSc2
apod.	apod.	kA
Přesto	přesto	k8xC
zřejmě	zřejmě	k6eAd1
nikdy	nikdy	k6eAd1
neklesla	klesnout	k5eNaPmAgFnS
pod	pod	k7c4
50	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vysoké	vysoký	k2eAgInPc4d1
počty	počet	k1gInPc4
příslušníků	příslušník	k1gMnPc2
VB	VB	kA
(	(	kIx(
<g/>
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
konkrétní	konkrétní	k2eAgNnSc4d1
zařazení	zařazení	k1gNnSc4
<g/>
)	)	kIx)
v	v	k7c6
KSČ	KSČ	kA
jsou	být	k5eAaImIp3nP
udávány	udávat	k5eAaImNgFnP
i	i	k9
v	v	k7c6
období	období	k1gNnSc6
normalizace	normalizace	k1gFnSc2
<g/>
:	:	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
KSČ	KSČ	kA
65	#num#	k4
%	%	kIx~
příslušníků	příslušník	k1gMnPc2
VB	VB	kA
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
68	#num#	k4
%	%	kIx~
a	a	k8xC
počty	počet	k1gInPc1
zvolna	zvolna	k6eAd1
narůstaly	narůstat	k5eAaImAgInP
až	až	k9
k	k	k7c3
rekordnímu	rekordní	k2eAgNnSc3d1
číslu	číslo	k1gNnSc3
75,6	75,6	k4
%	%	kIx~
příslušníků	příslušník	k1gMnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
VB	VB	kA
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
členy	člen	k1gInPc4
KSČ	KSČ	kA
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1988	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Novela	novela	k1gFnSc1
Zákona	zákon	k1gInSc2
o	o	k7c6
SNB	SNB	kA
ze	z	k7c2
14	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1990	#num#	k4
doplnila	doplnit	k5eAaPmAgFnS
ustanovení	ustanovení	k1gNnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
naopak	naopak	k6eAd1
všem	všecek	k3xTgMnPc3
příslušníkům	příslušník	k1gMnPc3
SNB	SNB	kA
po	po	k7c4
dobu	doba	k1gFnSc4
trvání	trvání	k1gNnSc2
jejich	jejich	k3xOp3gInSc2
služebního	služební	k2eAgInSc2d1
poměru	poměr	k1gInSc2
přerušila	přerušit	k5eAaPmAgFnS
členství	členství	k1gNnSc4
a	a	k8xC
činnost	činnost	k1gFnSc4
v	v	k7c6
politických	politický	k2eAgFnPc6d1
stranách	strana	k1gFnPc6
a	a	k8xC
hnutích	hnutí	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Složky	složka	k1gFnPc1
VB	VB	kA
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
pořádková	pořádkový	k2eAgFnSc1d1
služba	služba	k1gFnSc1
–	–	k?
pochůzková	pochůzkový	k2eAgFnSc1d1
a	a	k8xC
hlídková	hlídkový	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
<g/>
,	,	kIx,
zejména	zejména	k9
s	s	k7c7
dohledem	dohled	k1gInSc7
nad	nad	k7c7
dodržováním	dodržování	k1gNnSc7
veřejného	veřejný	k2eAgInSc2d1
pořádku	pořádek	k1gInSc2
</s>
<s>
dopravní	dopravní	k2eAgFnSc1d1
služba	služba	k1gFnSc1
–	–	k?
dopravní	dopravní	k2eAgInPc4d1
hlidky	hlidek	k1gInPc4
<g/>
,	,	kIx,
kontrola	kontrola	k1gFnSc1
pořádku	pořádek	k1gInSc2
v	v	k7c6
dopravě	doprava	k1gFnSc6
přímo	přímo	k6eAd1
v	v	k7c6
provozu	provoz	k1gInSc6
<g/>
;	;	kIx,
mimo	mimo	k7c4
to	ten	k3xDgNnSc4
pak	pak	k6eAd1
evidence	evidence	k1gFnSc1
řidičů	řidič	k1gMnPc2
a	a	k8xC
evidence	evidence	k1gFnSc1
motorových	motorový	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
tzv.	tzv.	kA
Dopravního	dopravní	k2eAgInSc2d1
inspektorátu	inspektorát	k1gInSc2
na	na	k7c6
okresním	okresní	k2eAgNnSc6d1
ředitelství	ředitelství	k1gNnSc6
</s>
<s>
správní	správní	k2eAgFnSc1d1
služba	služba	k1gFnSc1
–	–	k?
evidence	evidence	k1gFnSc2
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
,	,	kIx,
vydávání	vydávání	k1gNnSc1
občanských	občanský	k2eAgInPc2d1
průkazů	průkaz	k1gInPc2
<g/>
,	,	kIx,
zbrojních	zbrojní	k2eAgInPc2d1
průkazů	průkaz	k1gInPc2
apod.	apod.	kA
</s>
<s>
kriminální	kriminální	k2eAgFnSc1d1
služba	služba	k1gFnSc1
–	–	k?
vyšetřování	vyšetřování	k1gNnSc2
trestných	trestný	k2eAgInPc2d1
činů	čin	k1gInPc2
a	a	k8xC
shromažďovaní	shromažďovaný	k2eAgMnPc1d1
důkazního	důkazní	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
pro	pro	k7c4
trestní	trestní	k2eAgNnSc4d1
stíhání	stíhání	k1gNnSc4
</s>
<s>
Kriminalistický	kriminalistický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
–	–	k?
podpůrná	podpůrný	k2eAgFnSc1d1
složka	složka	k1gFnSc1
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
součásti	součást	k1gFnPc4
SNB	SNB	kA
<g/>
,	,	kIx,
pracující	pracující	k2eAgFnSc1d1
na	na	k7c6
rozborech	rozbor	k1gInPc6
a	a	k8xC
spolupracující	spolupracující	k2eAgFnSc3d1
při	pře	k1gFnSc3
rozkrývání	rozkrývání	k1gNnSc2
obecné	obecný	k2eAgFnSc2d1
a	a	k8xC
ekonomické	ekonomický	k2eAgFnSc2d1
kriminality	kriminalita	k1gFnSc2
</s>
<s>
Pohotovostní	pohotovostní	k2eAgInSc1d1
pluk	pluk	k1gInSc1
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
–	–	k?
represivní	represivní	k2eAgFnSc1d1
složka	složka	k1gFnSc1
určená	určený	k2eAgFnSc1d1
k	k	k7c3
přímému	přímý	k2eAgNnSc3d1
potlačování	potlačování	k1gNnSc3
občanských	občanský	k2eAgInPc2d1
nepokojů	nepokoj	k1gInPc2
a	a	k8xC
demonstrací	demonstrace	k1gFnPc2
</s>
<s>
Označení	označení	k1gNnSc1
příslušníků	příslušník	k1gMnPc2
VB	VB	kA
</s>
<s>
Předpis	předpis	k1gInSc1
o	o	k7c6
služebním	služební	k2eAgInSc6d1
stejnokroji	stejnokroj	k1gInSc6
<g/>
,	,	kIx,
odznacích	odznak	k1gInPc6
a	a	k8xC
jiné	jiný	k2eAgFnSc3d1
výstroji	výstroj	k1gFnSc3
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
o	o	k7c4
povinnosti	povinnost	k1gFnPc4
příslušníků	příslušník	k1gMnPc2
je	být	k5eAaImIp3nS
nosit	nosit	k5eAaImF
<g/>
,	,	kIx,
vydával	vydávat	k5eAaPmAgMnS,k5eAaImAgMnS
přímo	přímo	k6eAd1
ministr	ministr	k1gMnSc1
vnitra	vnitro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
)	)	kIx)
Od	od	k7c2
vzniku	vznik	k1gInSc2
SNB	SNB	kA
byli	být	k5eAaImAgMnP
členové	člen	k1gMnPc1
VB	VB	kA
organizováni	organizován	k2eAgMnPc1d1
vojensky	vojensky	k6eAd1
a	a	k8xC
nosili	nosit	k5eAaImAgMnP
upravené	upravený	k2eAgFnPc4d1
vojenské	vojenský	k2eAgFnPc4d1
uniformy	uniforma	k1gFnPc4
„	„	k?
<g/>
britského	britský	k2eAgMnSc4d1
<g/>
“	“	k?
střihu	střih	k1gInSc2
vzor	vzor	k1gInSc1
1946	#num#	k4
v	v	k7c6
barvě	barva	k1gFnSc6
khaki	khaki	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
)	)	kIx)
Rozkazem	rozkaz	k1gInSc7
ministra	ministr	k1gMnSc2
národní	národní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
č.	č.	k?
32	#num#	k4
<g/>
/	/	kIx~
<g/>
1952	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1952	#num#	k4
byl	být	k5eAaImAgInS
„	„	k?
<g/>
s	s	k7c7
okamžitou	okamžitý	k2eAgFnSc7d1
platností	platnost	k1gFnSc7
<g/>
“	“	k?
zaveden	zavést	k5eAaPmNgInS
stejnokroj	stejnokroj	k1gInSc1
vzor	vzor	k1gInSc1
1952	#num#	k4
<g/>
,	,	kIx,
modré	modrý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
a	a	k8xC
vojenského	vojenský	k2eAgInSc2d1
střihu	střih	k1gInSc2
–	–	k?
mj.	mj.	kA
jezdecké	jezdecký	k2eAgFnPc4d1
kalhoty	kalhoty	k1gFnPc4
a	a	k8xC
vysoké	vysoký	k2eAgFnPc4d1
boty	bota	k1gFnPc4
<g/>
,	,	kIx,
všechny	všechen	k3xTgInPc4
knoflíky	knoflík	k1gInPc4
zlaté	zlatá	k1gFnSc2
s	s	k7c7
pěticípou	pěticípý	k2eAgFnSc7d1
hvězdou	hvězda	k1gFnSc7
<g/>
,	,	kIx,
jednotná	jednotný	k2eAgFnSc1d1
čepice	čepice	k1gFnSc1
<g/>
–	–	k?
<g/>
brigadýrka	brigadýrka	k1gFnSc1
byla	být	k5eAaImAgFnS
modré	modrý	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
s	s	k7c7
višňovým	višňový	k2eAgInSc7d1
okolkem	okolek	k1gInSc7
a	a	k8xC
na	na	k7c6
ní	on	k3xPp3gFnSc6
byl	být	k5eAaImAgInS
umístěn	umístit	k5eAaPmNgInS
odznak	odznak	k1gInSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
pěticípé	pěticípý	k2eAgFnSc2d1
zlatě	zlatě	k6eAd1
lemované	lemovaný	k2eAgFnSc2d1
rudé	rudý	k2eAgFnSc2d1
hvězdy	hvězda	k1gFnSc2
a	a	k8xC
se	s	k7c7
stříbrným	stříbrný	k2eAgMnSc7d1
lvem	lev	k1gMnSc7
malého	malý	k2eAgInSc2d1
státního	státní	k2eAgInSc2d1
znaku	znak	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
rozkaz	rozkaz	k1gInSc1
byl	být	k5eAaImAgInS
velmi	velmi	k6eAd1
obecný	obecný	k2eAgInSc1d1
<g/>
,	,	kIx,
podrobný	podrobný	k2eAgInSc1d1
popis	popis	k1gInSc1
byl	být	k5eAaImAgInS
pak	pak	k6eAd1
uveden	uvést	k5eAaPmNgInS
ve	v	k7c6
stejnokrojovém	stejnokrojový	k2eAgInSc6d1
předpisu	předpis	k1gInSc6
ministerstva	ministerstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
nebylo	být	k5eNaImAgNnS
v	v	k7c6
silách	síla	k1gFnPc6
oděvních	oděvní	k2eAgInPc2d1
závodů	závod	k1gInPc2
vyrábět	vyrábět	k5eAaImF
nové	nový	k2eAgInPc4d1
stejnokroje	stejnokroj	k1gInPc4
v	v	k7c6
dostatečném	dostatečný	k2eAgNnSc6d1
množství	množství	k1gNnSc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
rozkazem	rozkaz	k1gInSc7
ministra	ministr	k1gMnSc2
národní	národní	k2eAgFnSc2d1
bezpečnostni	bezpečnostnout	k5eAaPmRp2nS,k5eAaImRp2nS
č.	č.	k?
15	#num#	k4
<g/>
/	/	kIx~
<g/>
1953	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1953	#num#	k4
zaveden	zavést	k5eAaPmNgInS
výkon	výkon	k1gInSc1
služby	služba	k1gFnSc2
v	v	k7c6
modrých	modrý	k2eAgFnPc6d1
uniformách	uniforma	k1gFnPc6
v	v	k7c6
Praze	Praha	k1gFnSc6
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1953	#num#	k4
a	a	k8xC
v	v	k7c6
ostatních	ostatní	k2eAgInPc6d1
krajích	kraj	k1gInPc6
podle	podle	k7c2
možností	možnost	k1gFnPc2
<g/>
,	,	kIx,
vždy	vždy	k6eAd1
však	však	k9
až	až	k9
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
nové	nový	k2eAgInPc4d1
stejnokroje	stejnokroj	k1gInPc4
budou	být	k5eAaImBp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
příslušníky	příslušník	k1gMnPc4
VB	VB	kA
v	v	k7c6
uvedeném	uvedený	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejnokroj	stejnokroj	k1gInSc1
vzor	vzor	k1gInSc1
1952	#num#	k4
byl	být	k5eAaImAgInS
tmavě	tmavě	k6eAd1
modrý	modrý	k2eAgMnSc1d1
(	(	kIx(
<g/>
tzv.	tzv.	kA
„	„	k?
<g/>
švestky	švestka	k1gFnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
a	a	k8xC
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
zaváděl	zavádět	k5eAaImAgMnS
tuhé	tuhý	k2eAgInPc4d1
nárameníky	nárameník	k1gInPc4
podle	podle	k7c2
sovětského	sovětský	k2eAgInSc2d1
vzoru	vzor	k1gInSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
pojmenování	pojmenování	k1gNnSc2
některých	některý	k3yIgFnPc2
hodností	hodnost	k1gFnPc2
-	-	kIx~
např.	např.	kA
hodnost	hodnost	k1gFnSc4
staršina	staršina	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
)	)	kIx)
Počátkem	počátkem	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
byl	být	k5eAaImAgInS
zaveden	zavést	k5eAaPmNgInS
stejnokroj	stejnokroj	k1gInSc1
vzoru	vzor	k1gInSc2
1961	#num#	k4
<g/>
,	,	kIx,
podobného	podobný	k2eAgInSc2d1
střihu	střih	k1gInSc2
jako	jako	k8xC,k8xS
předchozí	předchozí	k2eAgFnSc2d1
<g/>
,	,	kIx,
ale	ale	k8xC
základní	základní	k2eAgFnSc1d1
barva	barva	k1gFnSc1
se	se	k3xPyFc4
změnila	změnit	k5eAaPmAgFnS
na	na	k7c4
pastelově	pastelově	k6eAd1
modrou	modrý	k2eAgFnSc4d1
(	(	kIx(
<g/>
tzv.	tzv.	kA
„	„	k?
<g/>
pastelky	pastelka	k1gFnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
a	a	k8xC
tuhé	tuhý	k2eAgInPc1d1
nárameníky	nárameník	k1gInPc1
byly	být	k5eAaImAgInP
nahrazovány	nahrazovat	k5eAaImNgInP
látkovými	látkový	k2eAgFnPc7d1
podle	podle	k7c2
československých	československý	k2eAgFnPc2d1
zvyklostí	zvyklost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novým	nový	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
stejnokroje	stejnokroj	k1gInSc2
byla	být	k5eAaImAgFnS
tzv.	tzv.	kA
bundokošile	bundokošile	k6eAd1
v	v	k7c6
moderním	moderní	k2eAgInSc6d1
střihu	střih	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
nezastrkovala	zastrkovat	k5eNaImAgFnS
do	do	k7c2
kalhot	kalhoty	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
)	)	kIx)
Koncem	koncem	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgInP
zavádět	zavádět	k5eAaImF
stejnokroje	stejnokroj	k1gInPc1
vzor	vzor	k1gInSc1
1967	#num#	k4
v	v	k7c6
zelené	zelený	k2eAgFnSc6d1
barvě	barva	k1gFnSc6
(	(	kIx(
<g/>
tzv.	tzv.	kA
„	„	k?
<g/>
olivy	oliva	k1gFnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
vydržely	vydržet	k5eAaPmAgFnP
ve	v	k7c6
výstroji	výstroj	k1gFnSc6
až	až	k9
do	do	k7c2
zrušení	zrušení	k1gNnSc2
SNB	SNB	kA
a	a	k8xC
VB	VB	kA
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
bylo	být	k5eAaImAgNnS
na	na	k7c4
nárameníky	nárameník	k1gInPc4
zavedeno	zaveden	k2eAgNnSc4d1
označení	označení	k1gNnSc4
SNB	SNB	kA
(	(	kIx(
<g/>
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
ZNB	ZNB	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
5	#num#	k4
<g/>
)	)	kIx)
Pohotovostní	pohotovostní	k2eAgFnSc1d1
motorizovaná	motorizovaný	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
Praha	Praha	k1gFnSc1
byla	být	k5eAaImAgFnS
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jediná	jediný	k2eAgFnSc1d1
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
zkušebně	zkušebně	k6eAd1
vystrojena	vystrojen	k2eAgFnSc1d1
uniformami	uniforma	k1gFnPc7
petrolejové	petrolejový	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
vzoru	vzor	k1gInSc2
1988	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgMnPc4,k3yRgMnPc4,k3yIgMnPc4
byly	být	k5eAaImAgInP
šité	šitý	k2eAgInPc1d1
z	z	k7c2
materiálu	materiál	k1gInSc2
původně	původně	k6eAd1
určeného	určený	k2eAgInSc2d1
na	na	k7c4
slavnostní	slavnostní	k2eAgFnPc4d1
uniformy	uniforma	k1gFnPc4
příslušníků	příslušník	k1gMnPc2
VB	VB	kA
<g/>
.	.	kIx.
</s>
<s>
Služební	služební	k2eAgFnSc2d1
hodnosti	hodnost	k1gFnSc2
příslušníků	příslušník	k1gMnPc2
VB	VB	kA
</s>
<s>
Podle	podle	k7c2
§	§	k?
8	#num#	k4
Zákona	zákon	k1gInSc2
100	#num#	k4
<g/>
/	/	kIx~
<g/>
1970	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
služebním	služební	k2eAgInSc6d1
poměru	poměr	k1gInSc6
příslušníků	příslušník	k1gMnPc2
SNB	SNB	kA
<g/>
,	,	kIx,
platily	platit	k5eAaImAgInP
pro	pro	k7c4
příslušníky	příslušník	k1gMnPc4
VB	VB	kA
následující	následující	k2eAgFnSc6d1
služební	služební	k2eAgFnSc6d1
hodnosti	hodnost	k1gFnSc6
<g/>
:	:	kIx,
</s>
<s>
a	a	k8xC
<g/>
)	)	kIx)
hodnosti	hodnost	k1gFnPc4
praporčické	praporčický	k2eAgFnPc4d1
<g/>
:	:	kIx,
</s>
<s>
Rotný	rotný	k1gMnSc1
(	(	kIx(
<g/>
stříbrná	stříbrný	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Strážmistr	strážmistr	k1gMnSc1
(	(	kIx(
<g/>
dvě	dva	k4xCgFnPc1
stříbrné	stříbrný	k2eAgFnPc1d1
hvězdy	hvězda	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Nadstrážmistr	nadstrážmistr	k1gMnSc1
(	(	kIx(
<g/>
tři	tři	k4xCgFnPc1
stříbrné	stříbrný	k2eAgFnPc1d1
hvězdy	hvězda	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Podpraporčík	podpraporčík	k1gMnSc1
(	(	kIx(
<g/>
stříbrná	stříbrný	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
ve	v	k7c6
stříbrném	stříbrný	k1gInSc6
lemování	lemování	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Praporčík	praporčík	k1gMnSc1
(	(	kIx(
<g/>
dvě	dva	k4xCgFnPc1
stříbrné	stříbrný	k2eAgFnPc1d1
hvězdy	hvězda	k1gFnPc1
ve	v	k7c6
stříbrném	stříbrný	k2eAgNnSc6d1
lemování	lemování	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
Nadpraporčík	nadpraporčík	k1gMnSc1
(	(	kIx(
<g/>
tři	tři	k4xCgFnPc1
stříbrné	stříbrný	k2eAgFnPc1d1
hvězdy	hvězda	k1gFnPc1
ve	v	k7c6
stříbrném	stříbrný	k2eAgNnSc6d1
lemování	lemování	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
b	b	k?
<g/>
)	)	kIx)
hodnosti	hodnost	k1gFnSc2
důstojnické	důstojnický	k2eAgFnSc2d1
<g/>
:	:	kIx,
</s>
<s>
Podporučík	podporučík	k1gMnSc1
(	(	kIx(
<g/>
zlatá	zlatý	k2eAgFnSc1d1
pěticípá	pěticípý	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Poručík	poručík	k1gMnSc1
(	(	kIx(
<g/>
dvě	dva	k4xCgFnPc1
zlaté	zlatý	k2eAgFnPc1d1
hvězdy	hvězda	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Nadporučík	nadporučík	k1gMnSc1
(	(	kIx(
<g/>
tři	tři	k4xCgFnPc1
zlaté	zlatý	k2eAgFnPc1d1
hvězdy	hvězda	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Kapitán	kapitán	k1gMnSc1
(	(	kIx(
<g/>
čtyři	čtyři	k4xCgFnPc1
zlaté	zlatý	k2eAgFnPc1d1
hvězdy	hvězda	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Major	major	k1gMnSc1
(	(	kIx(
<g/>
zlatá	zlatý	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
ve	v	k7c6
zlatém	zlatý	k1gInSc6
lemování	lemování	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Podplukovník	podplukovník	k1gMnSc1
(	(	kIx(
<g/>
dvě	dva	k4xCgFnPc1
zlaté	zlatý	k2eAgFnPc1d1
hvězdy	hvězda	k1gFnPc1
ve	v	k7c6
zlatém	zlatý	k2eAgNnSc6d1
lemování	lemování	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
Plukovník	plukovník	k1gMnSc1
(	(	kIx(
<g/>
tři	tři	k4xCgFnPc1
zlaté	zlatý	k2eAgFnPc1d1
hvězdy	hvězda	k1gFnPc1
ve	v	k7c6
zlatém	zlatý	k2eAgNnSc6d1
lemování	lemování	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
c	c	k0
<g/>
)	)	kIx)
hodnosti	hodnost	k1gFnPc4
generálské	generálský	k2eAgFnPc4d1
<g/>
:	:	kIx,
</s>
<s>
Generálmajor	generálmajor	k1gMnSc1
(	(	kIx(
<g/>
zlatá	zlatý	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
a	a	k8xC
dvojité	dvojitý	k2eAgNnSc4d1
zlaté	zlatý	k2eAgNnSc4d1
lemování	lemování	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
Generálporučík	generálporučík	k1gMnSc1
(	(	kIx(
<g/>
dvě	dva	k4xCgFnPc1
zlaté	zlatý	k2eAgFnPc1d1
hvězdy	hvězda	k1gFnPc1
ve	v	k7c6
dvojitém	dvojitý	k2eAgNnSc6d1
lemování	lemování	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
Generálplukovník	generálplukovník	k1gMnSc1
(	(	kIx(
<g/>
tři	tři	k4xCgFnPc1
zlaté	zlatý	k2eAgFnPc1d1
hvězdy	hvězda	k1gFnPc1
ve	v	k7c6
dvojitém	dvojitý	k2eAgNnSc6d1
lemování	lemování	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
hodnost	hodnost	k1gFnSc1
ve	v	k7c6
složkách	složka	k1gFnPc6
SNB	SNB	kA
nepoužívala	používat	k5eNaImAgFnS
<g/>
)	)	kIx)
</s>
<s>
Zánik	zánik	k1gInSc1
Veřejné	veřejný	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Státní	státní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
zanikla	zaniknout	k5eAaPmAgFnS
rozkazem	rozkaz	k1gInSc7
ministra	ministr	k1gMnSc2
vnitra	vnitro	k1gNnSc2
Richarda	Richard	k1gMnSc2
Sachera	Sacher	k1gMnSc2
1	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1990	#num#	k4
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
části	část	k1gFnPc1
Veřejné	veřejný	k2eAgFnPc1d1
bezpečnosti	bezpečnost	k1gFnSc2
na	na	k7c6
základě	základ	k1gInSc6
Zákona	zákon	k1gInSc2
č.	č.	k?
283	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
ze	z	k7c2
dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1991	#num#	k4
<g/>
,	,	kIx,
o	o	k7c6
Policii	policie	k1gFnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
přeměněny	přeměněn	k2eAgFnPc1d1
dne	den	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1991	#num#	k4
v	v	k7c6
Policii	policie	k1gFnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
obdobným	obdobný	k2eAgInSc7d1
zákonem	zákon	k1gInSc7
SNR	SNR	kA
v	v	k7c4
Policejní	policejní	k2eAgInSc4d1
sbor	sbor	k1gInSc4
Slovenské	slovenský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
zákonem	zákon	k1gInSc7
Federálního	federální	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
333	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
ve	v	k7c4
Federální	federální	k2eAgInSc4d1
policejní	policejní	k2eAgInSc4d1
sbor	sbor	k1gInSc4
a	a	k8xC
Sbor	sbor	k1gInSc4
hradní	hradní	k2eAgFnSc2d1
policie	policie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
PEJČOCH	PEJČOCH	kA
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
<g/>
;	;	kIx,
TOMEK	Tomek	k1gMnSc1
<g/>
,	,	kIx,
Prokop	Prokop	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policisté	policista	k1gMnPc1
na	na	k7c6
popravišti	popraviště	k1gNnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
VHÚ	VHÚ	kA
Praha	Praha	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VANĚK	Vaněk	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
;	;	kIx,
KRÁTKÁ	Krátká	k1gFnSc1
<g/>
,	,	kIx,
Lenka	Lenka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběhy	příběh	k1gInPc4
(	(	kIx(
<g/>
ne	ne	k9
<g/>
)	)	kIx)
<g/>
obyčejných	obyčejný	k2eAgFnPc2d1
profesí	profes	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
158-159	158-159	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karolinum	Karolinum	k1gNnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Veřejná	veřejný	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Plné	plný	k2eAgNnSc1d1
znění	znění	k1gNnSc1
zákona	zákon	k1gInSc2
č.	č.	k?
283	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
na	na	k7c6
webu	web	k1gInSc6
iBadatelna	iBadatelna	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2010590668	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
143561935	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Komunistický	komunistický	k2eAgInSc1d1
režim	režim	k1gInSc1
v	v	k7c6
Československu	Československo	k1gNnSc6
</s>
