<s>
Veřejná	veřejný	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
Verejná	Verejný	k2eAgFnSc1d1
bezpečnosť	bezpečnostit	k5eAaPmRp2nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
označována	označován	k2eAgNnPc4d1
VB	VB	kA
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
v	v	k7c6
letech	let	k1gInPc6
1947	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
složkou	složka	k1gFnSc7
československého	československý	k2eAgInSc2d1
Sboru	sbor	k1gInSc2
národní	národní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
a	a	k8xC
vykonávala	vykonávat	k5eAaImAgFnS
policejní	policejní	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
ochrany	ochrana	k1gFnSc2
života	život	k1gInSc2
a	a	k8xC
zdraví	zdraví	k1gNnSc4
občanů	občan	k1gMnPc2
<g/>
,	,	kIx,
ochrany	ochrana	k1gFnSc2
veřejného	veřejný	k2eAgInSc2d1
pořádku	pořádek	k1gInSc2
<g/>
,	,	kIx,
vyšetřování	vyšetřování	k1gNnSc4
trestných	trestný	k2eAgInPc2d1
činů	čin	k1gInPc2
<g/>
,	,	kIx,
dohledu	dohled	k1gInSc2
nad	nad	k7c7
silničním	silniční	k2eAgInSc7d1
provozem	provoz	k1gInSc7
apod.	apod.	kA
VB	VB	kA
přímo	přímo	k6eAd1
podléhala	podléhat	k5eAaImAgFnS
československému	československý	k2eAgNnSc3d1
Ministerstvu	ministerstvo	k1gNnSc3
vnitra	vnitro	k1gNnSc2
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1969	#num#	k4
potom	potom	k6eAd1
jednotlivým	jednotlivý	k2eAgNnPc3d1
národním	národní	k2eAgNnPc3d1
ministerstvům	ministerstvo	k1gNnPc3
vnitra	vnitro	k1gNnSc2
(	(	kIx(
<g/>
MV	MV	kA
ČSR	ČSR	kA
a	a	k8xC
MV	MV	kA
SSR	SSR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>