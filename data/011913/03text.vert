<p>
<s>
Anto	Anto	k?	Anto
Babić	Babić	k1gFnSc1	Babić
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1899	[number]	k4	1899
Grahovik	Grahovik	k1gInSc1	Grahovik
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Travnik	Travnik	k1gInSc1	Travnik
<g/>
,	,	kIx,	,
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1974	[number]	k4	1974
Sarajevo	Sarajevo	k1gNnSc4	Sarajevo
<g/>
,	,	kIx,	,
Federativní	federativní	k2eAgFnSc1d1	federativní
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
bosenskohercegovský	bosenskohercegovský	k2eAgMnSc1d1	bosenskohercegovský
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
chorvatského	chorvatský	k2eAgInSc2d1	chorvatský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Dolac	Dolac	k1gInSc4	Dolac
u	u	k7c2	u
Travniku	Travnik	k1gInSc2	Travnik
<g/>
,	,	kIx,	,
gymnázium	gymnázium	k1gNnSc1	gymnázium
pak	pak	k6eAd1	pak
v	v	k7c6	v
Travniku	Travnik	k1gInSc6	Travnik
a	a	k8xC	a
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
odmaturoval	odmaturovat	k5eAaPmAgMnS	odmaturovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
geografii	geografie	k1gFnSc4	geografie
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zaměstnal	zaměstnat	k5eAaPmAgMnS	zaměstnat
jako	jako	k9	jako
profesor	profesor	k1gMnSc1	profesor
v	v	k7c6	v
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Sušaku	Sušak	k1gInSc6	Sušak
v	v	k7c6	v
nynějším	nynější	k2eAgNnSc6d1	nynější
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1925	[number]	k4	1925
a	a	k8xC	a
1943	[number]	k4	1943
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
profesor	profesor	k1gMnSc1	profesor
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgMnS	připojit
ke	k	k7c3	k
komunistickým	komunistický	k2eAgMnPc3d1	komunistický
partyzánům	partyzán	k1gMnPc3	partyzán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
zasedání	zasedání	k1gNnSc6	zasedání
Zemského	zemský	k2eAgInSc2d1	zemský
antifašistického	antifašistický	k2eAgInSc2d1	antifašistický
výboru	výbor	k1gInSc2	výbor
národního	národní	k2eAgNnSc2d1	národní
osvobození	osvobození	k1gNnSc2	osvobození
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
jako	jako	k9	jako
zástupce	zástupce	k1gMnSc1	zástupce
Chorvatů	Chorvat	k1gMnPc2	Chorvat
do	do	k7c2	do
Předsednictva	předsednictvo	k1gNnSc2	předsednictvo
tohoto	tento	k3xDgInSc2	tento
orgánu	orgán	k1gInSc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
též	též	k9	též
jako	jako	k9	jako
delegát	delegát	k1gMnSc1	delegát
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
zasedání	zasedání	k1gNnSc6	zasedání
Antifašistické	antifašistický	k2eAgFnSc2d1	Antifašistická
rady	rada	k1gFnSc2	rada
národního	národní	k2eAgNnSc2d1	národní
osvobození	osvobození	k1gNnSc2	osvobození
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
vykonával	vykonávat	k5eAaImAgInS	vykonávat
úřad	úřad	k1gInSc1	úřad
tajemníka	tajemník	k1gMnSc2	tajemník
(	(	kIx(	(
<g/>
ministra	ministr	k1gMnSc2	ministr
<g/>
)	)	kIx)	)
školství	školství	k1gNnSc2	školství
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
,	,	kIx,	,
místopředseda	místopředseda	k1gMnSc1	místopředseda
Prezídia	prezídium	k1gNnSc2	prezídium
Lidového	lidový	k2eAgNnSc2d1	lidové
shromáždění	shromáždění	k1gNnSc2	shromáždění
Lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
a	a	k8xC	a
poslanec	poslanec	k1gMnSc1	poslanec
Rady	rada	k1gFnSc2	rada
národů	národ	k1gInPc2	národ
Prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
Ústavodárného	ústavodárný	k2eAgNnSc2d1	Ústavodárné
shromáždění	shromáždění	k1gNnSc2	shromáždění
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
federativní	federativní	k2eAgFnSc2d1	federativní
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
získal	získat	k5eAaPmAgMnS	získat
místo	místo	k1gNnSc4	místo
profesora	profesor	k1gMnSc2	profesor
ve	v	k7c6	v
Vyšší	vysoký	k2eAgFnSc6d2	vyšší
pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
předsedy	předseda	k1gMnSc2	předseda
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
a	a	k8xC	a
věděcké	věděcký	k2eAgFnPc4d1	věděcká
instituce	instituce	k1gFnPc4	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c4	za
řádného	řádný	k2eAgMnSc4d1	řádný
profesora	profesor	k1gMnSc4	profesor
a	a	k8xC	a
prvního	první	k4xOgMnSc2	první
děkana	děkan	k1gMnSc2	děkan
právě	právě	k9	právě
zřízené	zřízený	k2eAgFnSc2d1	zřízená
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
Katedry	katedra	k1gFnSc2	katedra
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
přednášel	přednášet	k5eAaImAgMnS	přednášet
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Babić	Babić	k?	Babić
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
Historické	historický	k2eAgFnSc2d1	historická
společnosti	společnost	k1gFnSc2	společnost
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
právě	právě	k6eAd1	právě
zřízené	zřízený	k2eAgFnSc2d1	zřízená
Učené	učený	k2eAgFnSc2d1	učená
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
přerostla	přerůst	k5eAaPmAgFnS	přerůst
v	v	k7c6	v
Akademii	akademie	k1gFnSc6	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
byl	být	k5eAaImAgInS	být
dopisujícím	dopisující	k2eAgInSc7d1	dopisující
členem	člen	k1gInSc7	člen
Srbské	srbský	k2eAgFnSc2d1	Srbská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
dopisujícím	dopisující	k2eAgInSc7d1	dopisující
členem	člen	k1gInSc7	člen
Makedonské	makedonský	k2eAgFnSc2d1	makedonská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
členem	člen	k1gMnSc7	člen
redakční	redakční	k2eAgFnSc2d1	redakční
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
monumentálního	monumentální	k2eAgNnSc2d1	monumentální
díla	dílo	k1gNnSc2	dílo
Enciklopedija	Enciklopedija	k1gMnSc1	Enciklopedija
Jugoslavije	Jugoslavije	k1gMnSc1	Jugoslavije
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odborně	odborně	k6eAd1	odborně
se	se	k3xPyFc4	se
zbýval	zbývat	k5eAaImAgInS	zbývat
převážně	převážně	k6eAd1	převážně
studiem	studio	k1gNnSc7	studio
středověkých	středověký	k2eAgFnPc2d1	středověká
dějin	dějiny	k1gFnPc2	dějiny
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgMnSc4	ten
založil	založit	k5eAaPmAgMnS	založit
a	a	k8xC	a
vedl	vést	k5eAaImAgMnS	vést
Ročenku	ročenka	k1gFnSc4	ročenka
Historické	historický	k2eAgFnSc2d1	historická
společnosti	společnost	k1gFnSc2	společnost
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
Godišnjak	Godišnjak	k1gInSc4	Godišnjak
Društva	Društv	k1gMnSc2	Društv
istoričara	istoričar	k1gMnSc2	istoričar
Bosne	Bosn	k1gMnSc5	Bosn
i	i	k8xC	i
Hercegovine	Hercegovin	k1gMnSc5	Hercegovin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hojně	hojně	k6eAd1	hojně
publikoval	publikovat	k5eAaBmAgMnS	publikovat
odborné	odborný	k2eAgFnPc4d1	odborná
stati	stať	k1gFnPc4	stať
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Istorija	Istorija	k1gMnSc1	Istorija
naroda	naroda	k1gMnSc1	naroda
Jugoslavije	Jugoslavije	k1gMnSc1	Jugoslavije
<g/>
.	.	kIx.	.
</s>
<s>
Dio	Dio	k?	Dio
1	[number]	k4	1
(	(	kIx(	(
<g/>
Dějiny	dějiny	k1gFnPc4	dějiny
národů	národ	k1gInPc2	národ
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
<g/>
,	,	kIx,	,
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
monografie	monografie	k1gFnSc1	monografie
</s>
</p>
<p>
<s>
Istorija	Istorija	k1gMnSc1	Istorija
naroda	naroda	k1gMnSc1	naroda
Jugoslavije	Jugoslavije	k1gMnSc1	Jugoslavije
(	(	kIx(	(
<g/>
Dějiny	dějiny	k1gFnPc1	dějiny
národů	národ	k1gInPc2	národ
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
,	,	kIx,	,
red	red	k?	red
<g/>
.	.	kIx.	.
komise	komise	k1gFnSc1	komise
Anto	Anto	k?	Anto
Babić	Babić	k1gFnSc2	Babić
aj.	aj.	kA	aj.
<g/>
,	,	kIx,	,
ekavská	ekavský	k2eAgFnSc1d1	ekavský
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
Beograd	Beograd	k1gInSc1	Beograd
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Historija	Historija	k1gFnSc1	Historija
naroda	naroda	k1gFnSc1	naroda
Jugoslavije	Jugoslavije	k1gFnSc1	Jugoslavije
(	(	kIx(	(
<g/>
ijekavská	ijekavský	k2eAgFnSc1d1	ijekavský
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
Zagreb	Zagreb	k1gInSc1	Zagreb
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kolektivní	kolektivní	k2eAgFnSc2d1	kolektivní
monografie	monografie	k1gFnSc2	monografie
</s>
</p>
<p>
<s>
O	o	k7c4	o
odnosima	odnosima	k1gNnSc4	odnosima
vazaliteta	vazaliteta	k1gFnSc1	vazaliteta
u	u	k7c2	u
srednjevjekovnoj	srednjevjekovnoj	k1gInSc4	srednjevjekovnoj
Bosni	Bosn	k1gMnPc1	Bosn
(	(	kIx(	(
<g/>
O	o	k7c6	o
vazalských	vazalský	k2eAgInPc6d1	vazalský
vztazích	vztah	k1gInPc6	vztah
ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Bosně	Bosna	k1gFnSc6	Bosna
<g/>
,	,	kIx,	,
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
monografie	monografie	k1gFnSc1	monografie
</s>
</p>
<p>
<s>
O	o	k7c4	o
pitanju	pitanju	k5eAaPmIp1nS	pitanju
formiranja	formiranj	k2eAgFnSc1d1	formiranj
srednjevjekovne	srednjevjekovnout	k5eAaPmIp3nS	srednjevjekovnout
bosanske	bosanske	k6eAd1	bosanske
države	državat	k5eAaPmIp3nS	državat
(	(	kIx(	(
<g/>
O	o	k7c6	o
otázce	otázka	k1gFnSc6	otázka
formování	formování	k1gNnSc2	formování
středověkého	středověký	k2eAgInSc2d1	středověký
bosenského	bosenský	k2eAgInSc2d1	bosenský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
separát	separát	k1gInSc1	separát
odborného	odborný	k2eAgInSc2d1	odborný
článku	článek	k1gInSc2	článek
</s>
</p>
<p>
<s>
Bosanski	Bosanski	k6eAd1	Bosanski
heretici	heretik	k1gMnPc1	heretik
(	(	kIx(	(
<g/>
Bosenští	bosenský	k2eAgMnPc1d1	bosenský
heretici	heretik	k1gMnPc1	heretik
<g/>
,	,	kIx,	,
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
monografie	monografie	k1gFnSc1	monografie
</s>
</p>
<p>
<s>
Iz	Iz	k?	Iz
istorije	istorít	k5eAaPmIp3nS	istorít
srednjovjekovne	srednjovjekovnout	k5eAaPmIp3nS	srednjovjekovnout
Bosne	Bosn	k1gInSc5	Bosn
(	(	kIx(	(
<g/>
Z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
středověké	středověký	k2eAgFnSc2d1	středověká
Bosny	Bosna	k1gFnSc2	Bosna
<g/>
,	,	kIx,	,
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
monografie	monografie	k1gFnSc1	monografie
</s>
</p>
<p>
<s>
Diplomatska	Diplomatska	k1gFnSc1	Diplomatska
služba	služba	k1gFnSc1	služba
u	u	k7c2	u
srednjovjekovnoj	srednjovjekovnoj	k1gInSc4	srednjovjekovnoj
Bosni	Boseň	k1gFnSc3	Boseň
(	(	kIx(	(
<g/>
Diplomatická	diplomatický	k2eAgFnSc1d1	diplomatická
služba	služba	k1gFnSc1	služba
ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Bosně	Bosna	k1gFnSc6	Bosna
<g/>
,	,	kIx,	,
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
monografie	monografie	k1gFnSc1	monografie
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
