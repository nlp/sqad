<s>
Prof.	prof.	kA	prof.
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1940	[number]	k4	1940
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
výtvarník	výtvarník	k1gMnSc1	výtvarník
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
a	a	k8xC	a
performer	performer	k1gMnSc1	performer
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
do	do	k7c2	do
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
ředitelem	ředitel	k1gMnSc7	ředitel
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990-1997	[number]	k4	1990-1997
rektor	rektor	k1gMnSc1	rektor
Akademie	akademie	k1gFnSc2	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
tamtéž	tamtéž	k6eAd1	tamtéž
profesorem	profesor	k1gMnSc7	profesor
Intermediálního	intermediální	k2eAgInSc2d1	intermediální
ateliéru	ateliér	k1gInSc2	ateliér
dosud	dosud	k6eAd1	dosud
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1940	[number]	k4	1940
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Karel	Karel	k1gMnSc1	Karel
Knížák	Knížák	k1gMnSc1	Knížák
byl	být	k5eAaImAgMnS	být
řídicím	řídicí	k2eAgMnSc7d1	řídicí
učitelem	učitel	k1gMnSc7	učitel
na	na	k7c6	na
měšťanské	měšťanský	k2eAgFnSc6d1	měšťanská
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Blovicích	Blovik	k1gInPc6	Blovik
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
také	také	k9	také
kreslení	kreslení	k1gNnSc4	kreslení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
odešel	odejít	k5eAaPmAgInS	odejít
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
na	na	k7c4	na
výzvu	výzva	k1gFnSc4	výzva
vlády	vláda	k1gFnSc2	vláda
učit	učit	k5eAaImF	učit
do	do	k7c2	do
pohraničí	pohraničí	k1gNnSc2	pohraničí
-	-	kIx~	-
do	do	k7c2	do
Mariánských	mariánský	k2eAgFnPc2d1	Mariánská
Lázní	lázeň	k1gFnPc2	lázeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgMnS	začít
chodit	chodit	k5eAaImF	chodit
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Knížák	Knížák	k1gMnSc1	Knížák
byl	být	k5eAaImAgMnS	být
také	také	k9	také
malířem	malíř	k1gMnSc7	malíř
-	-	kIx~	-
krajinářem	krajinář	k1gMnSc7	krajinář
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
úřednicí	úřednice	k1gFnSc7	úřednice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
maturoval	maturovat	k5eAaBmAgMnS	maturovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Plané	Planá	k1gFnSc6	Planá
u	u	k7c2	u
Mariánských	mariánský	k2eAgFnPc2d1	Mariánská
Lázní	lázeň	k1gFnPc2	lázeň
a	a	k8xC	a
poté	poté	k6eAd1	poté
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
Vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
pedagogickou	pedagogický	k2eAgFnSc4d1	pedagogická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
obor	obor	k1gInSc4	obor
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
výchova	výchova	k1gFnSc1	výchova
-	-	kIx~	-
ruština	ruština	k1gFnSc1	ruština
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
kompromis	kompromis	k1gInSc4	kompromis
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gNnSc7	jeho
přáním	přání	k1gNnSc7	přání
malovat	malovat	k5eAaImF	malovat
a	a	k8xC	a
přáním	přání	k1gNnSc7	přání
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
školu	škola	k1gFnSc4	škola
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
pomocným	pomocný	k2eAgMnSc7d1	pomocný
dělníkem	dělník	k1gMnSc7	dělník
v	v	k7c6	v
PKOJF	PKOJF	kA	PKOJF
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
složil	složit	k5eAaPmAgMnS	složit
zkoušky	zkouška	k1gFnPc4	zkouška
na	na	k7c6	na
Akademii	akademie	k1gFnSc6	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
také	také	k9	také
později	pozdě	k6eAd2	pozdě
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
rok	rok	k1gInSc1	rok
studoval	studovat	k5eAaImAgInS	studovat
matematickou	matematický	k2eAgFnSc4d1	matematická
analýzu	analýza	k1gFnSc4	analýza
na	na	k7c6	na
Matematicko-fyzikální	matematickoyzikální	k2eAgFnSc6d1	matematicko-fyzikální
fakultě	fakulta	k1gFnSc6	fakulta
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Studia	studio	k1gNnPc1	studio
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
zanechal	zanechat	k5eAaPmAgMnS	zanechat
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
vystavoval	vystavovat	k5eAaImAgMnS	vystavovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
v	v	k7c6	v
Mariánských	mariánský	k2eAgFnPc6d1	Mariánská
Lázních	lázeň	k1gFnPc6	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
začal	začít	k5eAaPmAgInS	začít
vytvářet	vytvářet	k5eAaImF	vytvářet
své	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgFnPc4	první
aktivity	aktivita	k1gFnPc4	aktivita
-	-	kIx~	-
happeningy	happening	k1gInPc4	happening
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Ceremonie	ceremonie	k1gFnSc1	ceremonie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
a	a	k8xC	a
Kamenný	kamenný	k2eAgInSc4d1	kamenný
obřad	obřad	k1gInSc4	obřad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
tvorbě	tvorba	k1gFnSc3	tvorba
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Tělo	tělo	k1gNnSc1	tělo
a	a	k8xC	a
socha	socha	k1gFnSc1	socha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
a	a	k8xC	a
různým	různý	k2eAgFnPc3d1	různá
instalacím	instalace	k1gFnPc3	instalace
tzv.	tzv.	kA	tzv.
environments	environments	k1gInSc1	environments
na	na	k7c6	na
ulicích	ulice	k1gFnPc6	ulice
a	a	k8xC	a
ve	v	k7c6	v
dvorech	dvůr	k1gInPc6	dvůr
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
především	především	k9	především
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tvořil	tvořit	k5eAaImAgInS	tvořit
v	v	k7c6	v
atelieru	atelier	k1gInSc6	atelier
v	v	k7c6	v
domě	dům	k1gInSc6	dům
č.	č.	k?	č.
19	[number]	k4	19
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
založil	založit	k5eAaPmAgMnS	založit
skupinu	skupina	k1gFnSc4	skupina
Aktuální	aktuální	k2eAgNnSc1d1	aktuální
umění	umění	k1gNnSc1	umění
(	(	kIx(	(
<g/>
Actual	Actual	k1gMnSc1	Actual
Art	Art	k1gMnSc1	Art
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
slovo	slovo	k1gNnSc4	slovo
umění	umění	k1gNnSc2	umění
vypustila	vypustit	k5eAaPmAgNnP	vypustit
z	z	k7c2	z
názvu	název	k1gInSc2	název
a	a	k8xC	a
užívala	užívat	k5eAaImAgFnS	užívat
jen	jen	k9	jen
označení	označení	k1gNnSc4	označení
Aktual	Aktual	k1gMnSc1	Aktual
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
a	a	k8xC	a
již	již	k6eAd1	již
zdokumentované	zdokumentovaný	k2eAgFnPc1d1	zdokumentovaná
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gFnPc1	jejich
akce	akce	k1gFnPc1	akce
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Světě	svět	k1gInSc6	svět
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Demonstrace	demonstrace	k1gFnPc4	demonstrace
Jednoho	jeden	k4xCgInSc2	jeden
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
Svazu	svaz	k1gInSc2	svaz
československých	československý	k2eAgMnPc2d1	československý
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
Jindřich	Jindřich	k1gMnSc1	Jindřich
Chalupecký	Chalupecký	k2eAgInSc1d1	Chalupecký
upozornil	upozornit	k5eAaPmAgInS	upozornit
na	na	k7c4	na
Knížákovy	Knížákův	k2eAgFnPc4d1	Knížákova
aktivity	aktivita	k1gFnPc4	aktivita
členy	člen	k1gMnPc7	člen
hnutí	hnutí	k1gNnSc3	hnutí
Fluxus	Fluxus	k1gInSc4	Fluxus
a	a	k8xC	a
zprostředkoval	zprostředkovat	k5eAaPmAgMnS	zprostředkovat
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
Georgem	Georg	k1gMnSc7	Georg
Maciunasem	Maciunas	k1gMnSc7	Maciunas
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
Milana	Milan	k1gMnSc4	Milan
Knížáka	Knížák	k1gMnSc4	Knížák
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
Director	Director	k1gMnSc1	Director
Fluxus	Fluxus	k1gMnSc1	Fluxus
East	East	k1gMnSc1	East
<g/>
.	.	kIx.	.
</s>
<s>
George	Georg	k1gMnSc2	Georg
Maciunas	Maciunas	k1gMnSc1	Maciunas
pozval	pozvat	k5eAaPmAgMnS	pozvat
Knížáka	Knížák	k1gMnSc4	Knížák
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vízum	vízum	k1gNnSc1	vízum
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
uděleno	udělit	k5eAaPmNgNnS	udělit
až	až	k9	až
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
událostí	událost	k1gFnSc7	událost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejenom	nejenom	k6eAd1	nejenom
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
beatnickou	beatnický	k2eAgFnSc4d1	beatnická
<g/>
"	"	kIx"	"
komunitu	komunita	k1gFnSc4	komunita
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
zavítal	zavítat	k5eAaPmAgMnS	zavítat
Allen	Allen	k1gMnSc1	Allen
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
<g/>
.	.	kIx.	.
</s>
<s>
KSČ	KSČ	kA	KSČ
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
svolila	svolit	k5eAaPmAgFnS	svolit
uspořádat	uspořádat	k5eAaPmF	uspořádat
oficiální	oficiální	k2eAgInSc4d1	oficiální
studentský	studentský	k2eAgInSc4d1	studentský
majáles	majáles	k1gInSc4	majáles
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
stal	stát	k5eAaPmAgMnS	stát
nepřímou	přímý	k2eNgFnSc7d1	nepřímá
manifestací	manifestace	k1gFnSc7	manifestace
protivládních	protivládní	k2eAgNnPc2d1	protivládní
stanovisek	stanovisko	k1gNnPc2	stanovisko
<g/>
.	.	kIx.	.
</s>
<s>
Vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
volba	volba	k1gFnSc1	volba
Krále	Král	k1gMnSc2	Král
majálesu	majáles	k1gInSc2	majáles
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Allen	Allen	k1gMnSc1	Allen
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
jeho	jeho	k3xOp3gFnSc4	jeho
báseň	báseň	k1gFnSc1	báseň
Král	Král	k1gMnSc1	Král
majáles	majáles	k1gInSc1	majáles
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
stál	stát	k5eAaImAgMnS	stát
mimo	mimo	k7c4	mimo
underground	underground	k1gInSc4	underground
i	i	k8xC	i
další	další	k2eAgFnSc2d1	další
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
iniciativy	iniciativa	k1gFnSc2	iniciativa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vliv	vliv	k1gInSc1	vliv
jeho	jeho	k3xOp3gInSc2	jeho
Aktualu	Aktual	k1gInSc2	Aktual
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc2	jeho
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
na	na	k7c4	na
utváření	utváření	k1gNnSc4	utváření
životních	životní	k2eAgInPc2d1	životní
a	a	k8xC	a
uměleckých	umělecký	k2eAgInPc2d1	umělecký
postojů	postoj	k1gInPc2	postoj
"	"	kIx"	"
<g/>
vlasaté	vlasatý	k2eAgFnSc2d1	vlasatá
<g/>
"	"	kIx"	"
komunity	komunita	k1gFnSc2	komunita
byl	být	k5eAaImAgInS	být
značný	značný	k2eAgInSc1d1	značný
a	a	k8xC	a
Knížák	Knížák	k1gInSc1	Knížák
byl	být	k5eAaImAgInS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
"	"	kIx"	"
<g/>
Králem	Král	k1gMnSc7	Král
mániček	mánička	k1gFnPc2	mánička
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Místem	místem	k6eAd1	místem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
máničky	mánička	k1gFnPc1	mánička
scházívaly	scházívat	k5eAaImAgFnP	scházívat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
prostor	prostor	k1gInSc4	prostor
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
Václavského	václavský	k2eAgNnSc2d1	Václavské
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
jakýsi	jakýsi	k3yIgInSc1	jakýsi
český	český	k2eAgInSc1d1	český
Hyde	Hyde	k1gInSc1	Hyde
park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
George	George	k1gNnSc2	George
Maciunase	Maciunasa	k1gFnSc3	Maciunasa
již	již	k6eAd1	již
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
odletěl	odletět	k5eAaPmAgMnS	odletět
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
nedostal	dostat	k5eNaPmAgMnS	dostat
pas	pas	k1gInSc4	pas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
akcí	akce	k1gFnSc7	akce
skupiny	skupina	k1gFnSc2	skupina
Fluxus	Fluxus	k1gInSc1	Fluxus
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
ustanoven	ustanoven	k2eAgMnSc1d1	ustanoven
"	"	kIx"	"
<g/>
director	director	k1gMnSc1	director
of	of	k?	of
Fluxus	Fluxus	k1gMnSc1	Fluxus
East	East	k1gMnSc1	East
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
New	New	k1gMnSc1	New
Brunswick	Brunswick	k1gMnSc1	Brunswick
realizoval	realizovat	k5eAaBmAgMnS	realizovat
svůj	svůj	k3xOyFgInSc4	svůj
Lying	Lying	k1gInSc4	Lying
Ceremony	Ceremona	k1gFnSc2	Ceremona
a	a	k8xC	a
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
The	The	k1gFnSc2	The
Difficult	Difficulta	k1gFnPc2	Difficulta
Ceremony	Ceremona	k1gFnSc2	Ceremona
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnPc1	jeho
akce	akce	k1gFnPc1	akce
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
americké	americký	k2eAgNnSc4d1	americké
publikum	publikum	k1gNnSc4	publikum
příliš	příliš	k6eAd1	příliš
náročné	náročný	k2eAgFnPc1d1	náročná
<g/>
.	.	kIx.	.
</s>
<s>
George	Georg	k1gMnSc4	Georg
Maciunas	Maciunas	k1gInSc1	Maciunas
připravil	připravit	k5eAaPmAgInS	připravit
publikaci	publikace	k1gFnSc3	publikace
Knížák	Knížák	k1gInSc1	Knížák
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
collected	collected	k1gInSc1	collected
works	works	k1gInSc1	works
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
Fluxus	Fluxus	k1gInSc4	Fluxus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
festivalu	festival	k1gInSc2	festival
Performa	Performa	k1gFnSc1	Performa
11	[number]	k4	11
přiipomínka	přiipomínka	k1gFnSc1	přiipomínka
Knížákova	Knížákův	k2eAgNnSc2d1	Knížákovo
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
USA	USA	kA	USA
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Ginger	Ginger	k1gInSc4	Ginger
Island	Island	k1gInSc1	Island
Project	Project	k1gInSc1	Project
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
snaze	snaha	k1gFnSc6	snaha
hnutí	hnutí	k1gNnSc2	hnutí
Fluxus	Fluxus	k1gInSc4	Fluxus
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
fluxus	fluxus	k1gInSc1	fluxus
kolonie	kolonie	k1gFnSc2	kolonie
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
v	v	k7c6	v
Karibiku	Karibik	k1gInSc6	Karibik
<g/>
.	.	kIx.	.
</s>
<s>
Expedice	expedice	k1gFnSc1	expedice
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
<g/>
:	:	kIx,	:
Milan	Milan	k1gMnSc1	Milan
Knizák	Knizák	k1gMnSc1	Knizák
<g/>
,	,	kIx,	,
Yoshi	Yosh	k1gMnPc1	Yosh
Wada	Wada	k1gMnSc1	Wada
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Robert	Robert	k1gMnSc1	Robert
De	De	k?	De
Niro	Niro	k1gMnSc1	Niro
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
nedorazil	dorazit	k5eNaPmAgMnS	dorazit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
nezveřejněný	zveřejněný	k2eNgInSc1d1	nezveřejněný
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgInSc1d1	filmový
materiál	materiál	k1gInSc1	materiál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Maciunas	Maciunas	k1gMnSc1	Maciunas
<g/>
,	,	kIx,	,
Maciunasův	Maciunasův	k2eAgMnSc1d1	Maciunasův
pomocník	pomocník	k1gMnSc1	pomocník
Igor	Igor	k1gMnSc1	Igor
Damian	Damian	k1gMnSc1	Damian
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k9	jako
Igor	Igor	k1gMnSc1	Igor
Danien	Danien	k2eAgMnSc1d1	Danien
<g/>
,	,	kIx,	,
opustil	opustit	k5eAaPmAgMnS	opustit
ČSSR	ČSSR	kA	ČSSR
na	na	k7c4	na
pas	pas	k1gInSc4	pas
Serge	Serg	k1gMnSc2	Serg
Oldenberga	Oldenberg	k1gMnSc2	Oldenberg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
pozvané	pozvaný	k2eAgNnSc4d1	pozvané
patřil	patřit	k5eAaImAgMnS	patřit
i	i	k8xC	i
Robert	Robert	k1gMnSc1	Robert
Wittman	Wittman	k1gMnSc1	Wittman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
se	se	k3xPyFc4	se
Knížák	Knížák	k1gMnSc1	Knížák
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Saudkovou	Saudkový	k2eAgFnSc7d1	Saudková
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
možnost	možnost	k1gFnSc4	možnost
oficiálně	oficiálně	k6eAd1	oficiálně
pracovat	pracovat	k5eAaImF	pracovat
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
ho	on	k3xPp3gNnSc4	on
proto	proto	k8xC	proto
živila	živit	k5eAaImAgFnS	živit
manželka	manželka	k1gFnSc1	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Přivydělával	přivydělávat	k5eAaImAgMnS	přivydělávat
si	se	k3xPyFc3	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
profesích	profes	k1gFnPc6	profes
<g/>
,	,	kIx,	,
prodejem	prodej	k1gInSc7	prodej
obrazů	obraz	k1gInPc2	obraz
(	(	kIx(	(
<g/>
vlastních	vlastní	k2eAgFnPc2d1	vlastní
<g/>
)	)	kIx)	)
a	a	k8xC	a
restaurováním	restaurování	k1gNnSc7	restaurování
loutek	loutka	k1gFnPc2	loutka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
na	na	k7c6	na
stipendijním	stipendijní	k2eAgInSc6d1	stipendijní
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
programu	program	k1gInSc6	program
DAAD	DAAD	kA	DAAD
<g/>
.	.	kIx.	.
</s>
<s>
Výjezd	výjezd	k1gInSc4	výjezd
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
povolen	povolen	k2eAgMnSc1d1	povolen
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
z	z	k7c2	z
livechatu	livechat	k1gInSc2	livechat
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
jiným	jiný	k2eAgMnPc3d1	jiný
disidentům	disident	k1gMnPc3	disident
<g/>
,	,	kIx,	,
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
Pavlu	Pavel	k1gMnSc3	Pavel
Kohoutovi	Kohout	k1gMnSc3	Kohout
či	či	k8xC	či
Jaroslavu	Jaroslav	k1gMnSc3	Jaroslav
Kořánovi	Kořán	k1gMnSc3	Kořán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
přednáší	přednášet	k5eAaImIp3nS	přednášet
na	na	k7c6	na
Hochschule	Hochschula	k1gFnSc6	Hochschula
für	für	k?	für
Bildende	Bildend	k1gInSc5	Bildend
Kunst	Kunst	k1gMnSc1	Kunst
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
hudebním	hudební	k2eAgMnSc7d1	hudební
a	a	k8xC	a
architektonickým	architektonický	k2eAgInPc3d1	architektonický
experimentům	experiment	k1gInPc3	experiment
<g/>
,	,	kIx,	,
designu	design	k1gInSc2	design
šperků	šperk	k1gInPc2	šperk
a	a	k8xC	a
nábytku	nábytek	k1gInSc2	nábytek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
obdržel	obdržet	k5eAaPmAgMnS	obdržet
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
pomalování	pomalování	k1gNnSc4	pomalování
berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001	[number]	k4	2001
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
Rady	rada	k1gFnSc2	rada
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
byl	být	k5eAaImAgMnS	být
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
také	také	k6eAd1	také
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
moderátorů	moderátor	k1gMnPc2	moderátor
pořadu	pořad	k1gInSc2	pořad
18	[number]	k4	18
minut	minuta	k1gFnPc2	minuta
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
komerční	komerční	k2eAgFnSc6d1	komerční
zpravodajské	zpravodajský	k2eAgFnSc6d1	zpravodajská
televizi	televize	k1gFnSc6	televize
Z	z	k7c2	z
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
sbírání	sbírání	k1gNnSc2	sbírání
<g/>
,	,	kIx,	,
opravě	oprava	k1gFnSc3	oprava
a	a	k8xC	a
dokumentaci	dokumentace	k1gFnSc3	dokumentace
loutek	loutka	k1gFnPc2	loutka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
sbírka	sbírka	k1gFnSc1	sbírka
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Děčíně	Děčín	k1gInSc6	Děčín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vydal	vydat	k5eAaPmAgInS	vydat
dvoudílnou	dvoudílný	k2eAgFnSc4d1	dvoudílná
loutkářskou	loutkářský	k2eAgFnSc4d1	Loutkářská
encyklopedii	encyklopedie	k1gFnSc4	encyklopedie
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
scénografii	scénografie	k1gFnSc4	scénografie
a	a	k8xC	a
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
pojetí	pojetí	k1gNnSc4	pojetí
loutkového	loutkový	k2eAgNnSc2d1	loutkové
divadla	divadlo	k1gNnSc2	divadlo
s	s	k7c7	s
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
výtvarníků	výtvarník	k1gMnPc2	výtvarník
loutkového	loutkový	k2eAgNnSc2d1	loutkové
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
od	od	k7c2	od
vystopovatelné	vystopovatelný	k2eAgFnSc2d1	vystopovatelná
minulosti	minulost	k1gFnSc2	minulost
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c6	v
předmluvě	předmluva	k1gFnSc6	předmluva
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc1	jeho
třicetileté	třicetiletý	k2eAgFnPc1d1	třicetiletá
práce	práce	k1gFnPc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Obsáhlá	obsáhlý	k2eAgFnSc1d1	obsáhlá
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
má	mít	k5eAaImIp3nS	mít
2850	[number]	k4	2850
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
1600	[number]	k4	1600
hesel	heslo	k1gNnPc2	heslo
<g/>
.	.	kIx.	.
</s>
<s>
Odbornou	odborný	k2eAgFnSc7d1	odborná
kritikou	kritika	k1gFnSc7	kritika
byla	být	k5eAaImAgFnS	být
pranýřována	pranýřovat	k5eAaImNgFnS	pranýřovat
především	především	k9	především
metodologie	metodologie	k1gFnSc1	metodologie
publikace	publikace	k1gFnSc2	publikace
a	a	k8xC	a
množství	množství	k1gNnSc2	množství
věcných	věcný	k2eAgFnPc2d1	věcná
chyb	chyba	k1gFnPc2	chyba
<g/>
,	,	kIx,	,
také	také	k9	také
jakési	jakýsi	k3yIgNnSc4	jakýsi
"	"	kIx"	"
<g/>
renesanční	renesanční	k2eAgNnSc4d1	renesanční
<g/>
"	"	kIx"	"
pojetí	pojetí	k1gNnSc4	pojetí
publikace	publikace	k1gFnSc2	publikace
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
na	na	k7c6	na
takové	takový	k3xDgFnSc6	takový
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
pracuje	pracovat	k5eAaImIp3nS	pracovat
především	především	k9	především
celý	celý	k2eAgInSc1d1	celý
tým	tým	k1gInSc1	tým
odborníků	odborník	k1gMnPc2	odborník
<g/>
.	.	kIx.	.
</s>
<s>
Nepopiratelnou	popiratelný	k2eNgFnSc4d1	nepopiratelná
hodnotu	hodnota	k1gFnSc4	hodnota
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
její	její	k3xOp3gFnSc1	její
obrazová	obrazový	k2eAgFnSc1d1	obrazová
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
byl	být	k5eAaImAgMnS	být
během	během	k7c2	během
totalitního	totalitní	k2eAgInSc2d1	totalitní
režimu	režim	k1gInSc2	režim
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
zhruba	zhruba	k6eAd1	zhruba
300	[number]	k4	300
<g/>
krát	krát	k6eAd1	krát
zadržen	zadržet	k5eAaPmNgMnS	zadržet
policií	policie	k1gFnSc7	policie
<g/>
,	,	kIx,	,
vězněn	vězněn	k2eAgMnSc1d1	vězněn
<g/>
,	,	kIx,	,
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
za	za	k7c2	za
napadení	napadení	k1gNnSc2	napadení
policisty	policista	k1gMnSc2	policista
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
za	za	k7c4	za
poškozování	poškozování	k1gNnSc4	poškozování
zájmů	zájem	k1gInPc2	zájem
ČSSR	ČSSR	kA	ČSSR
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
za	za	k7c4	za
nezabránění	nezabránění	k1gNnSc4	nezabránění
v	v	k7c6	v
emigraci	emigrace	k1gFnSc6	emigrace
atd.	atd.	kA	atd.
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
byl	být	k5eAaImAgInS	být
evidován	evidovat	k5eAaImNgMnS	evidovat
jako	jako	k9	jako
nepřítel	nepřítel	k1gMnSc1	nepřítel
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
napsal	napsat	k5eAaPmAgInS	napsat
telegram	telegram	k1gInSc4	telegram
generálnímu	generální	k2eAgMnSc3d1	generální
tajemníkovi	tajemník	k1gMnSc3	tajemník
KSČ	KSČ	kA	KSČ
Husákovi	Husák	k1gMnSc3	Husák
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
ho	on	k3xPp3gMnSc4	on
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
omluvil	omluvit	k5eAaPmAgMnS	omluvit
za	za	k7c2	za
tvrzení	tvrzení	k1gNnSc2	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
vydělává	vydělávat	k5eAaImIp3nS	vydělávat
malováním	malování	k1gNnSc7	malování
pornografie	pornografie	k1gFnSc2	pornografie
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
upozornil	upozornit	k5eAaPmAgMnS	upozornit
při	při	k7c6	při
soudním	soudní	k2eAgNnSc6d1	soudní
sporu	spor	k1gInSc6	spor
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
The	The	k1gMnSc2	The
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gMnSc2	People
of	of	k?	of
the	the	k?	the
Universe	Universe	k1gFnSc2	Universe
za	za	k7c4	za
protistátní	protistátní	k2eAgFnSc4d1	protistátní
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
když	když	k8xS	když
prokuratura	prokuratura	k1gFnSc1	prokuratura
předložila	předložit	k5eAaPmAgFnS	předložit
text	text	k1gInSc4	text
písně	píseň	k1gFnSc2	píseň
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gMnSc1	People
<g/>
,	,	kIx,	,
převzatý	převzatý	k2eAgMnSc1d1	převzatý
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Aktual	Aktual	k1gInSc1	Aktual
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
důkazů	důkaz	k1gInPc2	důkaz
žaloby	žaloba	k1gFnSc2	žaloba
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
žalobce	žalobce	k1gMnSc1	žalobce
napadnul	napadnout	k5eAaPmAgMnS	napadnout
formulaci	formulace	k1gFnSc4	formulace
v	v	k7c6	v
textu	text	k1gInSc6	text
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
kulturu	kultura	k1gFnSc4	kultura
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
která	který	k3yRgFnSc1	který
připomínala	připomínat	k5eAaImAgFnS	připomínat
útok	útok	k1gInSc4	útok
na	na	k7c4	na
"	"	kIx"	"
<g/>
Spisy	spis	k1gInPc4	spis
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
se	se	k3xPyFc4	se
k	k	k7c3	k
autorství	autorství	k1gNnSc3	autorství
textu	text	k1gInSc2	text
písemně	písemně	k6eAd1	písemně
sám	sám	k3xTgMnSc1	sám
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
a	a	k8xC	a
petici	petice	k1gFnSc4	petice
za	za	k7c2	za
jeho	jeho	k3xOp3gNnSc2	jeho
propuštění	propuštění	k1gNnSc2	propuštění
podepsal	podepsat	k5eAaPmAgMnS	podepsat
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
řadu	řada	k1gFnSc4	řada
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
<g/>
)	)	kIx)	)
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
básník	básník	k1gMnSc1	básník
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
o	o	k7c6	o
kapele	kapela	k1gFnSc6	kapela
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gFnSc2	People
mluví	mluvit	k5eAaImIp3nS	mluvit
jako	jako	k9	jako
o	o	k7c6	o
kavárenské	kavárenský	k2eAgFnSc6d1	kavárenská
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2007	[number]	k4	2007
protestoval	protestovat	k5eAaBmAgMnS	protestovat
proti	proti	k7c3	proti
stavbě	stavba	k1gFnSc3	stavba
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
Národní	národní	k2eAgFnSc2d1	národní
knihovny	knihovna	k1gFnSc2	knihovna
dle	dle	k7c2	dle
návrhu	návrh	k1gInSc2	návrh
Jana	Jan	k1gMnSc2	Jan
Kaplického	Kaplický	k2eAgMnSc2d1	Kaplický
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
neúspěšně	úspěšně	k6eNd1	úspěšně
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
do	do	k7c2	do
senátu	senát	k1gInSc2	senát
v	v	k7c6	v
Berounském	berounský	k2eAgInSc6d1	berounský
obvodu	obvod	k1gInSc6	obvod
za	za	k7c4	za
ODS	ODS	kA	ODS
(	(	kIx(	(
<g/>
30,5	[number]	k4	30,5
%	%	kIx~	%
a	a	k8xC	a
37,8	[number]	k4	37,8
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
oznámil	oznámit	k5eAaPmAgInS	oznámit
záměr	záměr	k1gInSc1	záměr
kandidovat	kandidovat	k5eAaImF	kandidovat
za	za	k7c4	za
Národní	národní	k2eAgFnSc4d1	národní
koalici	koalice	k1gFnSc4	koalice
(	(	kIx(	(
<g/>
koalice	koalice	k1gFnSc1	koalice
Národní	národní	k2eAgFnSc2d1	národní
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
národní	národní	k2eAgFnSc2d1	národní
sociální	sociální	k2eAgInSc4d1	sociální
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
z	z	k7c2	z
kandidátky	kandidátka	k1gFnSc2	kandidátka
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
organizoval	organizovat	k5eAaBmAgMnS	organizovat
demonstraci	demonstrace	k1gFnSc4	demonstrace
za	za	k7c4	za
záchranu	záchrana	k1gFnSc4	záchrana
kubistického	kubistický	k2eAgInSc2d1	kubistický
domu	dům	k1gInSc2	dům
od	od	k7c2	od
Josefa	Josef	k1gMnSc2	Josef
Chochola	Chochola	k1gFnSc1	Chochola
v	v	k7c6	v
Neklanově	klanově	k6eNd1	klanově
ulici	ulice	k1gFnSc6	ulice
č.	č.	k?	č.
p.	p.	k?	p.
98	[number]	k4	98
pod	pod	k7c7	pod
Vyšehradem	Vyšehrad	k1gInSc7	Vyšehrad
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
studenty	student	k1gMnPc7	student
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
demonstrace	demonstrace	k1gFnSc2	demonstrace
proti	proti	k7c3	proti
nevkusným	vkusný	k2eNgFnPc3d1	nevkusná
sochám	socha	k1gFnPc3	socha
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Upozornil	upozornit	k5eAaPmAgMnS	upozornit
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
také	také	k9	také
kontroverzními	kontroverzní	k2eAgInPc7d1	kontroverzní
názory	názor	k1gInPc7	názor
na	na	k7c4	na
romskou	romský	k2eAgFnSc4d1	romská
otázku	otázka	k1gFnSc4	otázka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Čunek	Čunek	k1gMnSc1	Čunek
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
Romům	Rom	k1gMnPc3	Rom
příliš	příliš	k6eAd1	příliš
vstřícný	vstřícný	k2eAgMnSc1d1	vstřícný
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
bych	by	kYmCp1nS	by
jim	on	k3xPp3gMnPc3	on
náhradní	náhradní	k2eAgInSc4d1	náhradní
byty	byt	k1gInPc7	byt
nenabídl	nabídnout	k5eNaPmAgMnS	nabídnout
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
rektor	rektor	k1gMnSc1	rektor
Akademie	akademie	k1gFnSc2	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
vedoucím	vedoucí	k2eAgMnSc7d1	vedoucí
pedagogem	pedagog	k1gMnSc7	pedagog
v	v	k7c6	v
ateliéru	ateliér	k1gInSc6	ateliér
intermediální	intermediální	k2eAgFnSc2d1	intermediální
školy	škola	k1gFnSc2	škola
profesora	profesor	k1gMnSc2	profesor
Milana	Milan	k1gMnSc2	Milan
Knížáka	Knížák	k1gMnSc2	Knížák
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
nikdy	nikdy	k6eAd1	nikdy
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gNnSc2	jeho
rektorského	rektorský	k2eAgNnSc2d1	rektorské
působení	působení	k1gNnSc2	působení
založila	založit	k5eAaPmAgFnS	založit
Akademie	akademie	k1gFnSc1	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
doktorandské	doktorandský	k2eAgNnSc4d1	doktorandské
studium	studium	k1gNnSc4	studium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
Akademie	akademie	k1gFnSc2	akademie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
započal	započnout	k5eAaPmAgInS	započnout
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
ministr	ministr	k1gMnSc1	ministr
kultury	kultura	k1gFnSc2	kultura
Pavel	Pavel	k1gMnSc1	Pavel
Dostál	Dostál	k1gMnSc1	Dostál
Milana	Milan	k1gMnSc4	Milan
Knížáka	Knížák	k1gMnSc4	Knížák
generálním	generální	k2eAgMnSc7d1	generální
ředitelem	ředitel	k1gMnSc7	ředitel
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nahradil	nahradit	k5eAaPmAgInS	nahradit
Martina	Martin	k1gMnSc4	Martin
Zlatohlávka	zlatohlávek	k1gMnSc4	zlatohlávek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
vyřešil	vyřešit	k5eAaPmAgInS	vyřešit
vysoké	vysoký	k2eAgFnSc3d1	vysoká
a	a	k8xC	a
dlouho	dlouho	k6eAd1	dlouho
odkládané	odkládaný	k2eAgInPc1d1	odkládaný
restituční	restituční	k2eAgInPc1d1	restituční
nároky	nárok	k1gInPc1	nárok
<g/>
:	:	kIx,	:
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
Waldesovy	Waldesův	k2eAgFnSc2d1	Waldesova
rodiny	rodina	k1gFnSc2	rodina
na	na	k7c4	na
sbírku	sbírka	k1gFnSc4	sbírka
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Waldese	Waldese	k1gFnSc2	Waldese
<g/>
,	,	kIx,	,
změnil	změnit	k5eAaPmAgInS	změnit
systém	systém	k1gInSc1	systém
řízení	řízení	k1gNnSc2	řízení
a	a	k8xC	a
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
zachoval	zachovat	k5eAaPmAgInS	zachovat
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc4	dva
účty	účet	k1gInPc4	účet
jeden	jeden	k4xCgInSc4	jeden
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
v	v	k7c6	v
cizí	cizí	k2eAgFnSc6d1	cizí
měně	měna	k1gFnSc6	měna
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
ji	on	k3xPp3gFnSc4	on
oddlužil	oddlužit	k5eAaPmAgMnS	oddlužit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc4	všechen
uveřejnila	uveřejnit	k5eAaPmAgFnS	uveřejnit
České	český	k2eAgFnPc4d1	Česká
televize	televize	k1gFnPc4	televize
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
a	a	k8xC	a
přednesl	přednést	k5eAaPmAgMnS	přednést
sám	sám	k3xTgMnSc1	sám
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
při	při	k7c6	při
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
NG	NG	kA	NG
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Fajt	Fajt	k1gMnSc1	Fajt
<g/>
,	,	kIx,	,
někdejší	někdejší	k2eAgMnSc1d1	někdejší
ředitel	ředitel	k1gMnSc1	ředitel
oddělení	oddělení	k1gNnSc2	oddělení
starých	starý	k2eAgFnPc2d1	stará
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
,	,	kIx,	,
však	však	k9	však
předchozí	předchozí	k2eAgNnSc4d1	předchozí
hospodaření	hospodaření	k1gNnSc4	hospodaření
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
Martina	Martin	k1gMnSc2	Martin
Zlatohlávka	zlatohlávek	k1gMnSc2	zlatohlávek
hájí	hájit	k5eAaImIp3nP	hájit
jako	jako	k9	jako
méně	málo	k6eAd2	málo
autoritativní	autoritativní	k2eAgFnSc4d1	autoritativní
a	a	k8xC	a
kritiku	kritika	k1gFnSc4	kritika
současného	současný	k2eAgNnSc2d1	současné
Knížákova	Knížákův	k2eAgNnSc2d1	Knížákovo
vedení	vedení	k1gNnSc2	vedení
nepřijímá	přijímat	k5eNaImIp3nS	přijímat
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
ji	on	k3xPp3gFnSc4	on
za	za	k7c4	za
dezinformaci	dezinformace	k1gFnSc4	dezinformace
<g/>
.	.	kIx.	.
</s>
<s>
Knížákovo	Knížákův	k2eAgNnSc4d1	Knížákovo
působení	působení	k1gNnSc4	působení
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
Knížákovu	Knížákův	k2eAgFnSc4d1	Knížákova
normalizaci	normalizace	k1gFnSc4	normalizace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
vrhl	vrhnout	k5eAaPmAgMnS	vrhnout
i	i	k9	i
do	do	k7c2	do
systému	systém	k1gInSc2	systém
výstavních	výstavní	k2eAgFnPc2d1	výstavní
expozic	expozice	k1gFnPc2	expozice
ve	v	k7c6	v
Veletržním	veletržní	k2eAgInSc6d1	veletržní
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
upravil	upravit	k5eAaPmAgMnS	upravit
podle	podle	k7c2	podle
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
podle	podle	k7c2	podle
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
směry	směr	k1gInPc4	směr
<g/>
,	,	kIx,	,
školy	škola	k1gFnPc4	škola
a	a	k8xC	a
proudy	proud	k1gInPc4	proud
<g/>
;	;	kIx,	;
předchozí	předchozí	k2eAgFnSc1d1	předchozí
expozice	expozice	k1gFnSc1	expozice
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
souvislosti	souvislost	k1gFnPc4	souvislost
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgFnPc4d1	nová
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kombinovala	kombinovat	k5eAaImAgNnP	kombinovat
díla	dílo	k1gNnPc1	dílo
od	od	k7c2	od
různých	různý	k2eAgMnPc2d1	různý
autorů	autor	k1gMnPc2	autor
a	a	k8xC	a
skládala	skládat	k5eAaImAgFnS	skládat
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
prázdných	prázdný	k2eAgFnPc6d1	prázdná
bílých	bílý	k2eAgFnPc6d1	bílá
kójích	kóje	k1gFnPc6	kóje
či	či	k8xC	či
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
chodbách	chodba	k1gFnPc6	chodba
do	do	k7c2	do
nových	nový	k2eAgInPc2d1	nový
celků	celek	k1gInPc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
<g/>
:	:	kIx,	:
Obrazy	obraz	k1gInPc1	obraz
Jana	Jan	k1gMnSc2	Jan
Zrzavého	zrzavý	k2eAgMnSc2d1	zrzavý
byly	být	k5eAaImAgFnP	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
na	na	k7c6	na
třech	tři	k4xCgNnPc6	tři
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
<g/>
:	:	kIx,	:
První	první	k4xOgMnSc1	první
místo	místo	k7c2	místo
na	na	k7c6	na
protilehlé	protilehlý	k2eAgFnSc6d1	protilehlá
stěně	stěna	k1gFnSc6	stěna
v	v	k7c6	v
kóji	kóje	k1gFnSc6	kóje
Františka	František	k1gMnSc2	František
Kupky	Kupka	k1gMnSc2	Kupka
<g/>
,	,	kIx,	,
Zrzavého	zrzavý	k2eAgInSc2d1	zrzavý
drobné	drobný	k2eAgInPc4d1	drobný
portréty	portrét	k1gInPc4	portrét
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vynořovaly	vynořovat	k5eAaImAgFnP	vynořovat
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
Kupkovy	Kupkův	k2eAgFnSc2d1	Kupkova
aleje	alej	k1gFnSc2	alej
sfing	sfinga	k1gFnPc2	sfinga
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
mystické	mystický	k2eAgFnSc6d1	mystická
konkluzi	konkluze	k1gFnSc6	konkluze
jej	on	k3xPp3gNnSc2	on
dále	daleko	k6eAd2	daleko
čekalo	čekat	k5eAaImAgNnS	čekat
kubistické	kubistický	k2eAgNnSc1d1	kubistické
sousedství	sousedství	k1gNnSc1	sousedství
vedle	vedle	k7c2	vedle
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
a	a	k8xC	a
třetí	třetí	k4xOgFnSc7	třetí
sousedskou	sousedský	k2eAgFnSc7d1	sousedská
katarzí	katarze	k1gFnSc7	katarze
byla	být	k5eAaImAgFnS	být
stěna	stěna	k1gFnSc1	stěna
sdílená	sdílený	k2eAgFnSc1d1	sdílená
s	s	k7c7	s
Josefem	Josef	k1gMnSc7	Josef
Váchalem	Váchal	k1gMnSc7	Váchal
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
klinické	klinický	k2eAgFnSc6d1	klinická
expozici	expozice	k1gFnSc6	expozice
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
ozvláštněné	ozvláštněný	k2eAgInPc1d1	ozvláštněný
živými	živý	k2eAgFnPc7d1	živá
květinami	květina	k1gFnPc7	květina
<g/>
.	.	kIx.	.
</s>
<s>
Kritik	kritik	k1gMnSc1	kritik
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kesner	Kesner	k1gMnSc1	Kesner
ml.	ml.	kA	ml.
tuto	tento	k3xDgFnSc4	tento
expozici	expozice	k1gFnSc4	expozice
nazval	nazvat	k5eAaPmAgMnS	nazvat
ztělesněním	ztělesnění	k1gNnPc3	ztělesnění
i	i	k9	i
parodií	parodie	k1gFnSc7	parodie
moderny	moderna	k1gFnSc2	moderna
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Expozice	expozice	k1gFnSc1	expozice
ve	v	k7c6	v
VP	VP	kA	VP
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
být	být	k5eAaImF	být
jak	jak	k6eAd1	jak
ztělesněním	ztělesnění	k1gNnSc7	ztělesnění
<g/>
,	,	kIx,	,
tak	tak	k9	tak
parodií	parodie	k1gFnSc7	parodie
modernistické	modernistický	k2eAgFnSc2d1	modernistická
instalace	instalace	k1gFnSc2	instalace
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
MK	MK	kA	MK
do	do	k7c2	do
expozice	expozice	k1gFnSc2	expozice
zapojil	zapojit	k5eAaPmAgMnS	zapojit
také	také	k9	také
produkci	produkce	k1gFnSc4	produkce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
kritiky	kritika	k1gFnSc2	kritika
sice	sice	k8xC	sice
efektní	efektní	k2eAgNnSc1d1	efektní
ale	ale	k8xC	ale
spadá	spadat	k5eAaImIp3nS	spadat
spíše	spíše	k9	spíše
<g />
.	.	kIx.	.
</s>
<s>
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
českého	český	k2eAgNnSc2d1	české
uměleckoprůmyslového	uměleckoprůmyslový	k2eAgNnSc2d1	Uměleckoprůmyslové
muzea	muzeum	k1gNnSc2	muzeum
<g/>
:	:	kIx,	:
jako	jako	k8xC	jako
např.	např.	kA	např.
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
fotografie	fotografia	k1gFnSc2	fotografia
-	-	kIx~	-
akty	akt	k1gInPc4	akt
Františka	Františka	k1gFnSc1	Františka
Drtikola	Drtikola	k1gFnSc1	Drtikola
<g/>
,	,	kIx,	,
dobový	dobový	k2eAgInSc1d1	dobový
experimentální	experimentální	k2eAgInSc1d1	experimentální
film	film	k1gInSc1	film
<g/>
:	:	kIx,	:
část	část	k1gFnSc1	část
scén	scéna	k1gFnPc2	scéna
z	z	k7c2	z
filmu	film	k1gInSc2	film
Extase	extase	k1gFnSc2	extase
od	od	k7c2	od
Gustava	Gustav	k1gMnSc2	Gustav
Machatého	Machatý	k1gMnSc2	Machatý
<g/>
,	,	kIx,	,
reklamní	reklamní	k2eAgInPc1d1	reklamní
snímky	snímek	k1gInPc1	snímek
od	od	k7c2	od
Alexandera	Alexandero	k1gNnSc2	Alexandero
Hackenschmieda	Hackenschmied	k1gMnSc2	Hackenschmied
<g/>
,	,	kIx,	,
známého	známý	k1gMnSc2	známý
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Hammid	Hammida	k1gFnPc2	Hammida
<g/>
,	,	kIx,	,
a	a	k8xC	a
dobový	dobový	k2eAgInSc4d1	dobový
design	design	k1gInSc4	design
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
<g/>
:	:	kIx,	:
Tatru	Tatra	k1gFnSc4	Tatra
Hanse	Hans	k1gMnSc2	Hans
Ledwinky	Ledwinka	k1gFnSc2	Ledwinka
a	a	k8xC	a
grafický	grafický	k2eAgInSc1d1	grafický
design	design	k1gInSc1	design
<g/>
:	:	kIx,	:
Ladislav	Ladislav	k1gMnSc1	Ladislav
Sutnar	Sutnar	k1gMnSc1	Sutnar
<g/>
.	.	kIx.	.
</s>
<s>
Lehce	lehko	k6eAd1	lehko
kontroverzní	kontroverzní	k2eAgInPc1d1	kontroverzní
projevy	projev	k1gInPc1	projev
socialistického	socialistický	k2eAgNnSc2d1	socialistické
umění	umění	k1gNnSc2	umění
<g/>
:	:	kIx,	:
budovatelské	budovatelský	k2eAgInPc4d1	budovatelský
plakáty	plakát	k1gInPc4	plakát
a	a	k8xC	a
portrét	portrét	k1gInSc4	portrét
Julia	Julius	k1gMnSc2	Julius
Fučíka	Fučík	k1gMnSc2	Fučík
od	od	k7c2	od
Maxe	Max	k1gMnSc2	Max
Švabinského	Švabinský	k2eAgMnSc2d1	Švabinský
<g/>
.	.	kIx.	.
</s>
<s>
Expozici	expozice	k1gFnSc4	expozice
doplnil	doplnit	k5eAaPmAgMnS	doplnit
znovuvytvořením	znovuvytvoření	k1gNnPc3	znovuvytvoření
kinetických	kinetický	k2eAgInPc2d1	kinetický
a	a	k8xC	a
světelných	světelný	k2eAgInPc2d1	světelný
modelů	model	k1gInPc2	model
pozapomenutého	pozapomenutý	k2eAgMnSc2d1	pozapomenutý
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Pešánka	Pešánek	k1gMnSc2	Pešánek
a	a	k8xC	a
fotografií	fotografia	k1gFnSc7	fotografia
obchodního	obchodní	k2eAgInSc2d1	obchodní
domu	dům	k1gInSc2	dům
Baťa	Baťa	k1gMnSc1	Baťa
s	s	k7c7	s
Pešánkovou	Pešánkův	k2eAgFnSc7d1	Pešánkův
světelnou	světelný	k2eAgFnSc7d1	světelná
reklamou	reklama	k1gFnSc7	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Národ	národ	k1gInSc1	národ
bohužel	bohužel	k9	bohužel
stále	stále	k6eAd1	stále
musí	muset	k5eAaImIp3nP	muset
čekat	čekat	k5eAaImF	čekat
na	na	k7c6	na
vzkříšení	vzkříšení	k1gNnSc6	vzkříšení
ztraceného	ztracený	k2eAgInSc2d1	ztracený
Pešánkova	Pešánkův	k2eAgInSc2d1	Pešánkův
barevného	barevný	k2eAgInSc2d1	barevný
klavíru	klavír	k1gInSc2	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zařadil	zařadit	k5eAaPmAgInS	zařadit
díla	dílo	k1gNnPc4	dílo
tzv.	tzv.	kA	tzv.
outsiderů	outsider	k1gMnPc2	outsider
<g/>
;	;	kIx,	;
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
zařazení	zařazení	k1gNnSc6	zařazení
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
zařazení	zařazení	k1gNnSc1	zařazení
tzv.	tzv.	kA	tzv.
outsiderů	outsider	k1gMnPc2	outsider
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
práce	práce	k1gFnSc1	práce
se	se	k3xPyFc4	se
v	v	k7c6	v
instalacích	instalace	k1gFnPc6	instalace
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
doposud	doposud	k6eAd1	doposud
neobjevila	objevit	k5eNaPmAgFnS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
např.	např.	kA	např.
o	o	k7c4	o
Pavla	Pavel	k1gMnSc4	Pavel
Brázdu	Brázda	k1gMnSc4	Brázda
<g/>
,	,	kIx,	,
Zdeňka	Zdeněk	k1gMnSc4	Zdeněk
Macka	Macek	k1gMnSc4	Macek
<g/>
,	,	kIx,	,
Vladimíra	Vladimír	k1gMnSc4	Vladimír
Modrého	modrý	k2eAgMnSc4d1	modrý
a	a	k8xC	a
Karla	Karel	k1gMnSc4	Karel
Šlengra	šlengr	k1gMnSc4	šlengr
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
autoři	autor	k1gMnPc1	autor
se	se	k3xPyFc4	se
z	z	k7c2	z
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
důvodů	důvod	k1gInPc2	důvod
pohybovali	pohybovat	k5eAaImAgMnP	pohybovat
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
umělecké	umělecký	k2eAgFnSc2d1	umělecká
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Teoretici	teoretik	k1gMnPc1	teoretik
si	se	k3xPyFc3	se
jejich	jejich	k3xOp3gFnPc4	jejich
práce	práce	k1gFnPc4	práce
nevšímali	všímat	k5eNaImAgMnP	všímat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
okrajově	okrajově	k6eAd1	okrajově
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
expozice	expozice	k1gFnSc2	expozice
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
obraz	obraz	k1gInSc4	obraz
ukřižování	ukřižování	k1gNnSc2	ukřižování
Václava	Václav	k1gMnSc4	Václav
Chada	Chad	k1gMnSc4	Chad
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
smrti	smrt	k1gFnSc6	smrt
ve	v	k7c6	v
dvaadvaceti	dvaadvacet	k4xCc6	dvaadvacet
letech	let	k1gInPc6	let
zanechal	zanechat	k5eAaPmAgMnS	zanechat
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
na	na	k7c6	na
absolutním	absolutní	k2eAgInSc6d1	absolutní
začátku	začátek	k1gInSc6	začátek
-	-	kIx~	-
mimo	mimo	k7c4	mimo
několika	několik	k4yIc2	několik
Goyových	Goyův	k2eAgFnPc2d1	Goyova
parafrází	parafráze	k1gFnPc2	parafráze
či	či	k8xC	či
volných	volný	k2eAgFnPc2d1	volná
skic	skica	k1gFnPc2	skica
podle	podle	k7c2	podle
obrazů	obraz	k1gInPc2	obraz
Gogha	Gogha	k1gFnSc1	Gogha
vlastně	vlastně	k9	vlastně
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
větší	veliký	k2eAgInSc4d2	veliký
dokončený	dokončený	k2eAgInSc4d1	dokončený
obraz	obraz	k1gInSc4	obraz
a	a	k8xC	a
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
dvě	dva	k4xCgFnPc4	dva
skici	skica	k1gFnPc1	skica
podle	podle	k7c2	podle
Rubensova	Rubensův	k2eAgNnSc2d1	Rubensovo
plátna	plátno	k1gNnSc2	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
Václav	Václav	k1gMnSc1	Václav
Chad	Chad	k1gMnSc1	Chad
patrně	patrně	k6eAd1	patrně
trávil	trávit	k5eAaImAgMnS	trávit
odbojovou	odbojový	k2eAgFnSc7d1	odbojová
prací	práce	k1gFnSc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
vyřazení	vyřazení	k1gNnSc4	vyřazení
odborná	odborný	k2eAgFnSc1d1	odborná
kritika	kritika	k1gFnSc1	kritika
zcela	zcela	k6eAd1	zcela
pominula	pominout	k5eAaPmAgFnS	pominout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
čela	čelo	k1gNnSc2	čelo
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
odvolán	odvolat	k5eAaPmNgMnS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
NG	NG	kA	NG
ho	on	k3xPp3gNnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Rösel	Rösel	k1gMnSc1	Rösel
<g/>
.	.	kIx.	.
</s>
<s>
Samo	sám	k3xTgNnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Milana	Milan	k1gMnSc2	Milan
Knížáka	Knížák	k1gMnSc2	Knížák
posláním	poslání	k1gNnSc7	poslání
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
být	být	k5eAaImF	být
muzeem	muzeum	k1gNnSc7	muzeum
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
Kunsthalle	Kunsthalle	k1gFnSc1	Kunsthalle
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Odborná	odborný	k2eAgFnSc1d1	odborná
kritika	kritika	k1gFnSc1	kritika
instalaci	instalace	k1gFnSc4	instalace
ve	v	k7c6	v
Veletržním	veletržní	k2eAgInSc6d1	veletržní
paláci	palác	k1gInSc6	palác
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
needukativní	edukativní	k2eNgInSc4d1	edukativní
<g/>
,	,	kIx,	,
matoucí	matoucí	k2eAgInSc4d1	matoucí
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
lokální	lokální	k2eAgFnSc4d1	lokální
<g/>
,	,	kIx,	,
českou	český	k2eAgFnSc4d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
problém	problém	k1gInSc4	problém
hlavně	hlavně	k9	hlavně
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
návštěvníci	návštěvník	k1gMnPc1	návštěvník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
na	na	k7c4	na
české	český	k2eAgNnSc4d1	české
umění	umění	k1gNnSc4	umění
zvyklí	zvyklý	k2eAgMnPc1d1	zvyklý
<g/>
,	,	kIx,	,
nevědí	vědět	k5eNaImIp3nP	vědět
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
si	se	k3xPyFc3	se
díla	dílo	k1gNnPc1	dílo
zařadit	zařadit	k5eAaPmF	zařadit
<g/>
,	,	kIx,	,
a	a	k8xC	a
ocitají	ocitat	k5eAaImIp3nP	ocitat
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
neznámém	známý	k2eNgInSc6d1	neznámý
terénu	terén	k1gInSc6	terén
<g/>
;	;	kIx,	;
cit	cit	k1gInSc4	cit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
do	do	k7c2	do
Veletržního	veletržní	k2eAgInSc2d1	veletržní
paláce	palác	k1gInSc2	palác
přijde	přijít	k5eAaPmIp3nS	přijít
někdo	někdo	k3yInSc1	někdo
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
udiven	udiven	k2eAgMnSc1d1	udiven
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
českému	český	k2eAgNnSc3d1	české
umění	umění	k1gNnSc3	umění
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
vůbec	vůbec	k9	vůbec
nerozumí	rozumět	k5eNaImIp3nS	rozumět
<g/>
,	,	kIx,	,
vidí	vidět	k5eAaImIp3nS	vidět
tam	tam	k6eAd1	tam
sice	sice	k8xC	sice
spoustu	spousta	k1gFnSc4	spousta
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
orientaci	orientace	k1gFnSc4	orientace
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Sám	sám	k3xTgMnSc1	sám
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
expozice	expozice	k1gFnSc1	expozice
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nikoho	nikdo	k3yNnSc4	nikdo
z	z	k7c2	z
kritiků	kritik	k1gMnPc2	kritik
a	a	k8xC	a
ani	ani	k8xC	ani
z	z	k7c2	z
novinářů	novinář	k1gMnPc2	novinář
nezajímá	zajímat	k5eNaImIp3nS	zajímat
a	a	k8xC	a
galerií	galerie	k1gFnSc7	galerie
předkládané	předkládaný	k2eAgInPc1d1	předkládaný
materiály	materiál	k1gInPc1	materiál
vůbec	vůbec	k9	vůbec
nechtějí	chtít	k5eNaImIp3nP	chtít
číst	číst	k5eAaImF	číst
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
O	o	k7c4	o
poslání	poslání	k1gNnSc4	poslání
Národní	národní	k2eAgFnSc2d1	národní
Galerie	galerie	k1gFnSc2	galerie
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
kurátor	kurátor	k1gMnSc1	kurátor
na	na	k7c6	na
volné	volný	k2eAgFnSc6d1	volná
noze	noha	k1gFnSc6	noha
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Felix	Felix	k1gMnSc1	Felix
jako	jako	k8xS	jako
o	o	k7c6	o
místě	místo	k1gNnSc6	místo
edukace	edukace	k1gFnSc2	edukace
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
sběratelů	sběratel	k1gMnPc2	sběratel
<g/>
,	,	kIx,	,
lokálních	lokální	k2eAgMnPc2d1	lokální
politiků	politik	k1gMnPc2	politik
i	i	k8xC	i
ministrů	ministr	k1gMnPc2	ministr
<g/>
,	,	kIx,	,
celou	celý	k2eAgFnSc4d1	celá
tuto	tento	k3xDgFnSc4	tento
elitu	elita	k1gFnSc4	elita
ale	ale	k8xC	ale
čeká	čekat	k5eAaImIp3nS	čekat
jen	jen	k9	jen
domácí	domácí	k2eAgNnSc4d1	domácí
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
odvíjející	odvíjející	k2eAgInSc4d1	odvíjející
čechocentrismus	čechocentrismus	k1gInSc4	čechocentrismus
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
založil	založit	k5eAaPmAgMnS	založit
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
po	po	k7c6	po
zahraničním	zahraniční	k2eAgInSc6d1	zahraniční
vzoru	vzor	k1gInSc6	vzor
novou	nový	k2eAgFnSc4d1	nová
tradici	tradice	k1gFnSc4	tradice
"	"	kIx"	"
<g/>
Pražské	pražský	k2eAgFnPc1d1	Pražská
biennale	biennale	k6eAd1	biennale
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
přehlídku	přehlídka	k1gFnSc4	přehlídka
současného	současný	k2eAgNnSc2d1	současné
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
organizaci	organizace	k1gFnSc6	organizace
prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
však	však	k8xC	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
neshodám	neshoda	k1gFnPc3	neshoda
se	s	k7c7	s
spoluorganizátory	spoluorganizátor	k1gMnPc7	spoluorganizátor
<g/>
:	:	kIx,	:
Giancarlem	Giancarl	k1gMnSc7	Giancarl
Politim	Politim	k1gMnSc1	Politim
z	z	k7c2	z
uměleckého	umělecký	k2eAgInSc2d1	umělecký
časopisu	časopis	k1gInSc2	časopis
Flash	Flasha	k1gFnPc2	Flasha
Art	Art	k1gFnSc2	Art
a	a	k8xC	a
Helenou	Helena	k1gFnSc7	Helena
Kontovou	kontový	k2eAgFnSc7d1	Kontová
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
tohoto	tento	k3xDgNnSc2	tento
schizmatu	schizma	k1gNnSc2	schizma
byla	být	k5eAaImAgNnP	být
následující	následující	k2eAgNnPc1d1	následující
dvě	dva	k4xCgFnPc4	dva
Pražská	pražský	k2eAgFnSc1d1	Pražská
biennale	biennale	k6eAd1	biennale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
-	-	kIx~	-
jedno	jeden	k4xCgNnSc1	jeden
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
galerii	galerie	k1gFnSc6	galerie
a	a	k8xC	a
druhé	druhý	k4xOgFnSc6	druhý
více	hodně	k6eAd2	hodně
provizorní	provizorní	k2eAgFnSc6d1	provizorní
<g/>
,	,	kIx,	,
pořádané	pořádaný	k2eAgFnSc6d1	pořádaná
Flash-artem	Flashrt	k1gInSc7	Flash-art
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
hale	hala	k1gFnSc6	hala
ČKD	ČKD	kA	ČKD
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
-	-	kIx~	-
zahájené	zahájený	k2eAgFnSc2d1	zahájená
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Malíř	malíř	k1gMnSc1	malíř
Roman	Roman	k1gMnSc1	Roman
Franta	Franta	k1gMnSc1	Franta
ironický	ironický	k2eAgInSc4d1	ironický
paradox	paradox	k1gInSc4	paradox
situace	situace	k1gFnSc1	situace
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
trefně	trefně	k6eAd1	trefně
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
Prague	Pragu	k1gFnSc2	Pragu
Abnormale	Abnormala	k1gFnSc3	Abnormala
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
poté	poté	k6eAd1	poté
změnil	změnit	k5eAaPmAgMnS	změnit
biennale	biennale	k6eAd1	biennale
na	na	k7c6	na
triennale	triennale	k6eAd1	triennale
-	-	kIx~	-
International	International	k1gFnSc1	International
Triennale	Triennale	k1gFnSc2	Triennale
of	of	k?	of
Contemporary	Contemporara	k1gFnSc2	Contemporara
Art	Art	k1gMnSc4	Art
Prague	Pragu	k1gMnSc4	Pragu
<g/>
.	.	kIx.	.
</s>
<s>
Protesty	protest	k1gInPc1	protest
a	a	k8xC	a
kritika	kritika	k1gFnSc1	kritika
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
Milanu	Milan	k1gMnSc3	Milan
Knížákovi	Knížák	k1gMnSc3	Knížák
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
státních	státní	k2eAgFnPc2d1	státní
sbírkových	sbírkový	k2eAgFnPc2d1	sbírková
institucí	instituce	k1gFnPc2	instituce
opakovaně	opakovaně	k6eAd1	opakovaně
ozývaly	ozývat	k5eAaImAgFnP	ozývat
z	z	k7c2	z
úst	ústa	k1gNnPc2	ústa
některých	některý	k3yIgMnPc2	některý
odborníků	odborník	k1gMnPc2	odborník
a	a	k8xC	a
umělců	umělec	k1gMnPc2	umělec
<g/>
:	:	kIx,	:
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
odchází	odcházet	k5eAaImIp3nS	odcházet
z	z	k7c2	z
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
Katarína	Katarína	k1gFnSc1	Katarína
Rusnáková	Rusnáková	k1gFnSc1	Rusnáková
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
odvolání	odvolání	k1gNnSc3	odvolání
ve	v	k7c6	v
spisu	spis	k1gInSc6	spis
"	"	kIx"	"
<g/>
VÝROČNÍ	výroční	k2eAgFnSc1d1	výroční
ZPRÁVA	zpráva	k1gFnSc1	zpráva
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
ZA	za	k7c4	za
ROK	rok	k1gInSc4	rok
2000	[number]	k4	2000
<g/>
"	"	kIx"	"
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Během	během	k7c2	během
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
prezentacích	prezentace	k1gFnPc6	prezentace
ve	v	k7c6	v
Veletržním	veletržní	k2eAgInSc6d1	veletržní
paláci	palác	k1gInSc6	palác
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
nedorozumění	nedorozumění	k1gNnSc2	nedorozumění
s	s	k7c7	s
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
ředitelkou	ředitelka	k1gFnSc7	ředitelka
Sbírky	sbírka	k1gFnSc2	sbírka
moderního	moderní	k2eAgMnSc2d1	moderní
a	a	k8xC	a
současného	současný	k2eAgNnSc2d1	současné
umění	umění	k1gNnSc2	umění
Katarínou	Katarína	k1gFnSc7	Katarína
Rusnákovou	Rusnáková	k1gFnSc7	Rusnáková
<g/>
.	.	kIx.	.
</s>
<s>
Nezbývá	zbývat	k5eNaImIp3nS	zbývat
mi	já	k3xPp1nSc3	já
než	než	k8xS	než
přiznat	přiznat	k5eAaPmF	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
výběr	výběr	k1gInSc1	výběr
této	tento	k3xDgFnSc2	tento
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
pracovnice	pracovnice	k1gFnSc2	pracovnice
byl	být	k5eAaImAgInS	být
nešťastný	šťastný	k2eNgMnSc1d1	nešťastný
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
a	a	k8xC	a
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
jsem	být	k5eAaImIp1nS	být
někoho	někdo	k3yInSc4	někdo
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
mimo	mimo	k7c4	mimo
Národní	národní	k2eAgFnSc4d1	národní
galerii	galerie	k1gFnSc4	galerie
a	a	k8xC	a
věřil	věřit	k5eAaImAgMnS	věřit
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Katarína	Katarína	k1gFnSc1	Katarína
Rusnáková	Rusnáková	k1gFnSc1	Rusnáková
bude	být	k5eAaImBp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
náročnou	náročný	k2eAgFnSc4d1	náročná
úlohu	úloha	k1gFnSc4	úloha
ředitelky	ředitelka	k1gFnSc2	ředitelka
nesmírně	smírně	k6eNd1	smírně
zanedbané	zanedbaný	k2eAgFnPc4d1	zanedbaná
sbírky	sbírka	k1gFnPc4	sbírka
splnit	splnit	k5eAaPmF	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
<g/>
,	,	kIx,	,
nestalo	stát	k5eNaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
se	s	k7c7	s
sbírkovými	sbírkový	k2eAgInPc7d1	sbírkový
předměty	předmět	k1gInPc7	předmět
<g/>
,	,	kIx,	,
pečování	pečování	k1gNnPc1	pečování
o	o	k7c4	o
ně	on	k3xPp3gMnPc4	on
a	a	k8xC	a
vědecká	vědecký	k2eAgFnSc1d1	vědecká
práce	práce	k1gFnSc1	práce
směřující	směřující	k2eAgFnSc1d1	směřující
dovnitř	dovnitř	k7c2	dovnitř
galerie	galerie	k1gFnSc2	galerie
<g/>
,	,	kIx,	,
jí	on	k3xPp3gFnSc7	on
byly	být	k5eAaImAgFnP	být
cizí	cizí	k2eAgMnPc4d1	cizí
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
asi	asi	k9	asi
všichni	všechen	k3xTgMnPc1	všechen
dosavadní	dosavadní	k2eAgMnPc1d1	dosavadní
ředitelé	ředitel	k1gMnPc1	ředitel
<g/>
,	,	kIx,	,
touhu	touha	k1gFnSc4	touha
změnit	změnit	k5eAaPmF	změnit
moderní	moderní	k2eAgFnSc4d1	moderní
sbírku	sbírka	k1gFnSc4	sbírka
v	v	k7c4	v
jakousi	jakýsi	k3yIgFnSc4	jakýsi
"	"	kIx"	"
<g/>
Kunsthalle	Kunsthalle	k1gNnPc3	Kunsthalle
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
především	především	k6eAd1	především
pořádají	pořádat	k5eAaImIp3nP	pořádat
krátkodobé	krátkodobý	k2eAgFnPc4d1	krátkodobá
výstavy	výstava	k1gFnPc4	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
akcentovala	akcentovat	k5eAaImAgFnS	akcentovat
své	svůj	k3xOyFgInPc4	svůj
osobní	osobní	k2eAgInPc4d1	osobní
záliby	zálib	k1gInPc4	zálib
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
preferovala	preferovat	k5eAaImAgFnS	preferovat
některé	některý	k3yIgMnPc4	některý
autory	autor	k1gMnPc4	autor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
i	i	k8xC	i
určité	určitý	k2eAgNnSc4d1	určité
zneužití	zneužití	k1gNnSc4	zneužití
pravomocí	pravomoc	k1gFnPc2	pravomoc
v	v	k7c6	v
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
impulsem	impuls	k1gInSc7	impuls
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
odchodu	odchod	k1gInSc3	odchod
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Katarina	Katarina	k1gFnSc1	Katarina
Rusnaková	Rusnakový	k2eAgFnSc1d1	Rusnakový
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
příchodu	příchod	k1gInSc6	příchod
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
rozhovorů	rozhovor	k1gInPc2	rozhovor
sama	sám	k3xTgFnSc1	sám
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Každá	každý	k3xTgFnSc1	každý
instituce	instituce	k1gFnSc1	instituce
je	být	k5eAaImIp3nS	být
nástrojem	nástroj	k1gInSc7	nástroj
násilí	násilí	k1gNnSc2	násilí
a	a	k8xC	a
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
si	se	k3xPyFc3	se
musím	muset	k5eAaImIp1nS	muset
dát	dát	k5eAaPmF	dát
pozor	pozor	k1gInSc4	pozor
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
nebyla	být	k5eNaImAgFnS	být
sebestředná	sebestředný	k2eAgFnSc1d1	sebestředná
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
také	také	k9	také
její	její	k3xOp3gFnSc4	její
výstavu	výstava	k1gFnSc4	výstava
"	"	kIx"	"
<g/>
Lidická	lidický	k2eAgFnSc1d1	Lidická
sbírka	sbírka	k1gFnSc1	sbírka
<g/>
"	"	kIx"	"
ve	v	k7c6	v
Veletržním	veletržní	k2eAgInSc6d1	veletržní
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2000	[number]	k4	2000
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
spor	spor	k1gInSc1	spor
mezi	mezi	k7c7	mezi
Národní	národní	k2eAgFnSc7d1	národní
galerií	galerie	k1gFnSc7	galerie
a	a	k8xC	a
Společností	společnost	k1gFnSc7	společnost
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
její	její	k3xOp3gFnSc7	její
obchodní	obchodní	k2eAgFnSc7d1	obchodní
společností	společnost	k1gFnSc7	společnost
"	"	kIx"	"
<g/>
Centrum	centrum	k1gNnSc1	centrum
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zastupovala	zastupovat	k5eAaImAgFnS	zastupovat
Marta	Marta	k1gFnSc1	Marta
Železná	železný	k2eAgFnSc1d1	železná
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
kvůli	kvůli	k7c3	kvůli
platnosti	platnost	k1gFnSc3	platnost
smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
symbolickém	symbolický	k2eAgInSc6d1	symbolický
pronájmu	pronájem	k1gInSc6	pronájem
prostoru	prostor	k1gInSc2	prostor
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Kinských	Kinská	k1gFnPc2	Kinská
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
měla	mít	k5eAaImAgFnS	mít
Společnost	společnost	k1gFnSc1	společnost
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
za	za	k7c4	za
symbolický	symbolický	k2eAgInSc4d1	symbolický
roční	roční	k2eAgInSc4d1	roční
nájem	nájem	k1gInSc4	nájem
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Milana	Milan	k1gMnSc2	Milan
Knížáka	Knížák	k1gMnSc2	Knížák
kolem	kolem	k7c2	kolem
16	[number]	k4	16
000	[number]	k4	000
Kč	Kč	kA	Kč
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
v	v	k7c6	v
neprůstřelné	průstřelný	k2eNgFnSc6d1	neprůstřelná
vestě	vesta	k1gFnSc6	vesta
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kustody	kustod	k1gInPc7	kustod
a	a	k8xC	a
za	za	k7c2	za
asistence	asistence	k1gFnSc2	asistence
policie	policie	k1gFnSc2	policie
obsadil	obsadit	k5eAaPmAgInS	obsadit
"	"	kIx"	"
<g/>
Knihkupectví	knihkupectví	k1gNnSc3	knihkupectví
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
"	"	kIx"	"
jak	jak	k6eAd1	jak
zněl	znět	k5eAaImAgInS	znět
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sídlil	sídlit	k5eAaImAgInS	sídlit
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Kinských	Kinská	k1gFnPc2	Kinská
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
společnost	společnost	k1gFnSc1	společnost
"	"	kIx"	"
<g/>
Centrum	centrum	k1gNnSc1	centrum
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
"	"	kIx"	"
jej	on	k3xPp3gInSc4	on
pronajímala	pronajímat	k5eAaImAgFnS	pronajímat
již	již	k6eAd1	již
ne	ne	k9	ne
za	za	k7c4	za
symbolický	symbolický	k2eAgInSc4d1	symbolický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
tržní	tržní	k2eAgInSc4d1	tržní
nájem	nájem	k1gInSc4	nájem
kolem	kolem	k7c2	kolem
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
měsíčně	měsíčně	k6eAd1	měsíčně
jednomu	jeden	k4xCgNnSc3	jeden
pražskému	pražský	k2eAgMnSc3d1	pražský
knihkupci	knihkupec	k1gMnSc3	knihkupec
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
uvedl	uvést	k5eAaPmAgMnS	uvést
na	na	k7c6	na
BBC	BBC	kA	BBC
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
počítejte	počítat	k5eAaImRp2nP	počítat
ten	ten	k3xDgInSc4	ten
rozdíl	rozdíl	k1gInSc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nebetyčný	nebetyčný	k2eAgInSc1d1	nebetyčný
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
asi	asi	k9	asi
o	o	k7c4	o
milion	milion	k4xCgInSc4	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
figuroval	figurovat	k5eAaImAgMnS	figurovat
i	i	k9	i
Vladimír	Vladimír	k1gMnSc1	Vladimír
Železný	Železný	k1gMnSc1	Železný
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
svou	svůj	k3xOyFgFnSc7	svůj
sbírkou	sbírka	k1gFnSc7	sbírka
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
jeho	jeho	k3xOp3gFnSc4	jeho
nabídku	nabídka	k1gFnSc4	nabídka
na	na	k7c4	na
vyřešení	vyřešení	k1gNnSc4	vyřešení
sporu	spor	k1gInSc2	spor
nechtěl	chtít	k5eNaImAgMnS	chtít
komentovat	komentovat	k5eAaBmF	komentovat
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Knihkupectví	knihkupectví	k1gNnSc1	knihkupectví
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
měsících	měsíc	k1gInPc6	měsíc
<g/>
,	,	kIx,	,
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
pod	pod	k7c7	pod
pozměněným	pozměněný	k2eAgInSc7d1	pozměněný
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Kafkovo	Kafkův	k2eAgNnSc1d1	Kafkovo
knihkupectví	knihkupectví	k1gNnSc1	knihkupectví
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
si	se	k3xPyFc3	se
nechala	nechat	k5eAaPmAgFnS	nechat
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
zaregistrovat	zaregistrovat	k5eAaPmF	zaregistrovat
<g/>
.	.	kIx.	.
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
ministr	ministr	k1gMnSc1	ministr
Pavel	Pavel	k1gMnSc1	Pavel
Dostál	Dostál	k1gMnSc1	Dostál
slavnostně	slavnostně	k6eAd1	slavnostně
odhalil	odhalit	k5eAaPmAgMnS	odhalit
na	na	k7c6	na
"	"	kIx"	"
<g/>
Kafkově	Kafkův	k2eAgNnSc6d1	Kafkovo
knihkupectví	knihkupectví	k1gNnSc6	knihkupectví
<g/>
"	"	kIx"	"
nese	nést	k5eAaImIp3nS	nést
nápis	nápis	k1gInSc1	nápis
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zde	zde	k6eAd1	zde
býval	bývat	k5eAaImAgInS	bývat
obchod	obchod	k1gInSc1	obchod
se	s	k7c7	s
střižním	střižní	k2eAgNnSc7d1	střižní
a	a	k8xC	a
galanterním	galanterní	k2eAgNnSc7d1	galanterní
zbožím	zboží	k1gNnSc7	zboží
Hermanna	Hermann	k1gMnSc2	Hermann
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
,	,	kIx,	,
otce	otec	k1gMnSc2	otec
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
a	a	k8xC	a
ani	ani	k8xC	ani
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
nejbližší	blízký	k2eAgFnSc2d3	nejbližší
rodiny	rodina	k1gFnSc2	rodina
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
knihy	kniha	k1gFnSc2	kniha
neprodával	prodávat	k5eNaImAgMnS	prodávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
a	a	k8xC	a
2001	[number]	k4	2001
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
získal	získat	k5eAaPmAgMnS	získat
pro	pro	k7c4	pro
Národní	národní	k2eAgFnSc4d1	národní
galerii	galerie	k1gFnSc4	galerie
řadu	řada	k1gFnSc4	řada
různých	různý	k2eAgInPc2d1	různý
darů	dar	k1gInPc2	dar
<g/>
:	:	kIx,	:
Platzerovu	Platzerův	k2eAgFnSc4d1	Platzerova
plastiku	plastika	k1gFnSc4	plastika
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
a	a	k8xC	a
především	především	k9	především
unikátní	unikátní	k2eAgFnSc4d1	unikátní
sbírku	sbírka	k1gFnSc4	sbírka
1250	[number]	k4	1250
anatolských	anatolský	k2eAgInPc2d1	anatolský
koberců	koberec	k1gInPc2	koberec
a	a	k8xC	a
156	[number]	k4	156
předmětů	předmět	k1gInPc2	předmět
asijské	asijský	k2eAgFnSc2d1	asijská
provenience	provenience	k1gFnSc2	provenience
<g/>
;	;	kIx,	;
vše	všechen	k3xTgNnSc4	všechen
daroval	darovat	k5eAaPmAgMnS	darovat
německý	německý	k2eAgMnSc1d1	německý
sběratel	sběratel	k1gMnSc1	sběratel
Rainer	Rainer	k1gMnSc1	Rainer
Kreissl	Kreissl	k1gInSc4	Kreissl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
dostala	dostat	k5eAaPmAgFnS	dostat
darem	dar	k1gInSc7	dar
též	též	k9	též
od	od	k7c2	od
pana	pan	k1gMnSc2	pan
R.	R.	kA	R.
Kreissla	Kreissla	k1gMnSc2	Kreissla
čtyři	čtyři	k4xCgNnPc1	čtyři
plátna	plátno	k1gNnPc1	plátno
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
ruský	ruský	k2eAgMnSc1d1	ruský
abstraktní	abstraktní	k2eAgMnSc1d1	abstraktní
malíř	malíř	k1gMnSc1	malíř
Alexandr	Alexandr	k1gMnSc1	Alexandr
Rodčenko	Rodčenka	k1gFnSc5	Rodčenka
<g/>
.	.	kIx.	.
</s>
<s>
Díla	dílo	k1gNnPc1	dílo
bez	bez	k7c2	bez
dokumentace	dokumentace	k1gFnSc2	dokumentace
vystavila	vystavit	k5eAaPmAgFnS	vystavit
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
expertíze	expertíza	k1gFnSc6	expertíza
se	se	k3xPyFc4	se
prokázalo	prokázat	k5eAaPmAgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
bezcenná	bezcenný	k2eAgNnPc4d1	bezcenné
poválečná	poválečný	k2eAgNnPc4d1	poválečné
falza	falzum	k1gNnPc4	falzum
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
dílech	dílo	k1gNnPc6	dílo
ruské	ruský	k2eAgFnSc2d1	ruská
moderny	moderna	k1gFnSc2	moderna
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
celkem	celkem	k6eAd1	celkem
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
do	do	k7c2	do
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
dílo	dílo	k1gNnSc1	dílo
kdysi	kdysi	k6eAd1	kdysi
kontroverzního	kontroverzní	k2eAgMnSc2d1	kontroverzní
německého	německý	k2eAgMnSc2d1	německý
umělce-revolucionáře	umělceevolucionář	k1gMnSc2	umělce-revolucionář
<g/>
,	,	kIx,	,
Josepha	Joseph	k1gMnSc2	Joseph
Beuyse	Beuys	k1gMnSc2	Beuys
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
čítankového	čítankový	k2eAgMnSc2d1	čítankový
klasika	klasik	k1gMnSc2	klasik
<g/>
.	.	kIx.	.
</s>
<s>
Instalace	instalace	k1gFnSc1	instalace
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Auto	auto	k1gNnSc1	auto
<g/>
"	"	kIx"	"
a	a	k8xC	a
stála	stát	k5eAaImAgFnS	stát
15	[number]	k4	15
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Nákup	nákup	k1gInSc4	nákup
byl	být	k5eAaImAgMnS	být
často	často	k6eAd1	často
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
jako	jako	k9	jako
předražený	předražený	k2eAgMnSc1d1	předražený
<g/>
,	,	kIx,	,
lišila	lišit	k5eAaImAgFnS	lišit
se	se	k3xPyFc4	se
jen	jen	k9	jen
míra	míra	k1gFnSc1	míra
předražení	předražení	k1gNnSc2	předražení
<g/>
:	:	kIx,	:
od	od	k7c2	od
trojnásobného	trojnásobný	k2eAgMnSc2d1	trojnásobný
po	po	k7c4	po
desetinásobné	desetinásobný	k2eAgMnPc4d1	desetinásobný
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
uměleckém	umělecký	k2eAgNnSc6d1	umělecké
díle	dílo	k1gNnSc6	dílo
se	se	k3xPyFc4	se
často	často	k6eAd1	často
i	i	k9	i
v	v	k7c6	v
seriozním	seriozní	k2eAgInSc6d1	seriozní
tisku	tisk	k1gInSc6	tisk
či	či	k8xC	či
veřejnoprávní	veřejnoprávní	k2eAgFnSc3d1	veřejnoprávní
televizi	televize	k1gFnSc3	televize
mluvilo	mluvit	k5eAaImAgNnS	mluvit
jako	jako	k9	jako
o	o	k7c4	o
"	"	kIx"	"
<g/>
třech	tři	k4xCgFnPc6	tři
kovových	kovový	k2eAgFnPc6d1	kovová
obručích	obruč	k1gFnPc6	obruč
s	s	k7c7	s
názvem	název	k1gInSc7	název
Auto	auto	k1gNnSc1	auto
<g/>
"	"	kIx"	"
za	za	k7c4	za
500	[number]	k4	500
000	[number]	k4	000
euro	euro	k1gNnSc1	euro
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Podobně	podobně	k6eAd1	podobně
absurdní	absurdní	k2eAgMnSc1d1	absurdní
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
tvrdit	tvrdit	k5eAaImF	tvrdit
o	o	k7c6	o
Picassově	Picassův	k2eAgInSc6d1	Picassův
obraze	obraz	k1gInSc6	obraz
"	"	kIx"	"
<g/>
Musketeer	Musketeer	k1gMnSc1	Musketeer
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
olejovou	olejový	k2eAgFnSc7d1	olejová
barvou	barva	k1gFnSc7	barva
skromně	skromně	k6eAd1	skromně
potřená	potřený	k2eAgFnSc1d1	potřená
nevelká	velký	k2eNgFnSc1d1	nevelká
lněná	lněný	k2eAgFnSc1d1	lněná
látka	látka	k1gFnSc1	látka
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Mušketýr	mušketýr	k1gMnSc1	mušketýr
<g/>
"	"	kIx"	"
za	za	k7c4	za
11,5	[number]	k4	11,5
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Po	po	k7c6	po
anonymním	anonymní	k2eAgNnSc6d1	anonymní
udání	udání	k1gNnSc6	udání
na	na	k7c4	na
předraženost	předraženost	k1gFnSc4	předraženost
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
o	o	k7c4	o
nákup	nákup	k1gInSc4	nákup
začala	začít	k5eAaPmAgFnS	začít
zajímat	zajímat	k5eAaImF	zajímat
také	také	k9	také
policie	policie	k1gFnSc1	policie
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kultury	kultura	k1gFnSc2	kultura
muselo	muset	k5eAaImAgNnS	muset
dokonce	dokonce	k9	dokonce
vydat	vydat	k5eAaPmF	vydat
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
stanovisko	stanovisko	k1gNnSc4	stanovisko
zpracované	zpracovaný	k2eAgNnSc4d1	zpracované
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dotazů	dotaz	k1gInPc2	dotaz
ze	z	k7c2	z
sdělovacích	sdělovací	k2eAgInPc2d1	sdělovací
prostředků	prostředek	k1gInPc2	prostředek
o	o	k7c6	o
postupu	postup	k1gInSc6	postup
nákupu	nákup	k1gInSc2	nákup
děl	dít	k5eAaImAgMnS	dít
Národní	národní	k2eAgFnSc7d1	národní
galerií	galerie	k1gFnSc7	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
Cena	cena	k1gFnSc1	cena
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Chalupeckého	Chalupecký	k2eAgMnSc2d1	Chalupecký
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sporu	spor	k1gInSc6	spor
mezi	mezi	k7c7	mezi
Milanem	Milan	k1gMnSc7	Milan
Knížákem	Knížák	k1gMnSc7	Knížák
a	a	k8xC	a
Davidem	David	k1gMnSc7	David
Černým	Černý	k1gMnSc7	Černý
o	o	k7c6	o
umístění	umístění	k1gNnSc6	umístění
Černého	Černého	k2eAgFnPc2d1	Černého
soch	socha	k1gFnPc2	socha
ve	v	k7c6	v
Veletržním	veletržní	k2eAgInSc6d1	veletržní
paláci	palác	k1gInSc6	palác
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vyvedení	vyvedení	k1gNnSc3	vyvedení
laureáta	laureát	k1gMnSc2	laureát
ceny	cena	k1gFnSc2	cena
Davida	David	k1gMnSc2	David
Černého	Černý	k1gMnSc2	Černý
ochrankou	ochranka	k1gFnSc7	ochranka
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
cenu	cena	k1gFnSc4	cena
předat	předat	k5eAaPmF	předat
na	na	k7c6	na
chodníku	chodník	k1gInSc6	chodník
před	před	k7c7	před
palácem	palác	k1gInSc7	palác
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
galerii	galerie	k1gFnSc4	galerie
mamutích	mamutí	k2eAgFnPc2d1	mamutí
soch	socha	k1gFnPc2	socha
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
David	David	k1gMnSc1	David
Černý	Černý	k1gMnSc1	Černý
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
do	do	k7c2	do
galerie	galerie	k1gFnSc2	galerie
vstoupit	vstoupit	k5eAaPmF	vstoupit
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
čele	čelo	k1gNnSc6	čelo
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
dále	daleko	k6eAd2	daleko
pronesl	pronést	k5eAaPmAgMnS	pronést
podobenství	podobenství	k1gNnSc3	podobenství
o	o	k7c6	o
Mohamedovi	Mohamed	k1gMnSc6	Mohamed
a	a	k8xC	a
hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
spor	spor	k1gInSc1	spor
dědiců	dědic	k1gMnPc2	dědic
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
k	k	k7c3	k
dílu	dílo	k1gNnSc3	dílo
Josefa	Josef	k1gMnSc2	Josef
Lady	lady	k1gFnSc1	lady
s	s	k7c7	s
umělcem	umělec	k1gMnSc7	umělec
Tomášem	Tomáš	k1gMnSc7	Tomáš
Vaňkem	Vaněk	k1gMnSc7	Vaněk
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc7	jeho
obrazy	obraz	k1gInPc7	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
nechal	nechat	k5eAaPmAgMnS	nechat
Vaňkovy	Vaňkův	k2eAgInPc4d1	Vaňkův
obrazy	obraz	k1gInPc4	obraz
na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
zakrýt	zakrýt	k5eAaPmF	zakrýt
plachtou	plachta	k1gFnSc7	plachta
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
spor	spor	k1gInSc1	spor
nevyřeší	vyřešit	k5eNaPmIp3nS	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
teoretička	teoretička	k1gFnSc1	teoretička
Noemi	Noe	k1gFnPc7	Noe
Smolíková	Smolíková	k1gFnSc1	Smolíková
razantně	razantně	k6eAd1	razantně
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
proti	proti	k7c3	proti
vedení	vedení	k1gNnSc3	vedení
galerie	galerie	k1gFnSc2	galerie
v	v	k7c6	v
kauze	kauza	k1gFnSc6	kauza
nezapůjčení	nezapůjčení	k1gNnSc2	nezapůjčení
obrazu	obraz	k1gInSc2	obraz
Františka	František	k1gMnSc2	František
Kupky	kupka	k1gFnSc2	kupka
Fuga	fuga	k1gFnSc1	fuga
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kupkův	Kupkův	k2eAgInSc1d1	Kupkův
obraz	obraz	k1gInSc1	obraz
nebyl	být	k5eNaImAgMnS	být
zapůjčen	zapůjčit	k5eAaPmNgMnS	zapůjčit
na	na	k7c4	na
výstavu	výstava	k1gFnSc4	výstava
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
abstrakce	abstrakce	k1gFnSc2	abstrakce
v	v	k7c6	v
Centre	centr	k1gInSc5	centr
Pompidou	Pompida	k1gFnSc7	Pompida
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
dokonce	dokonce	k9	dokonce
stěžejním	stěžejní	k2eAgNnSc7d1	stěžejní
dílem	dílo	k1gNnSc7	dílo
a	a	k8xC	a
umístěn	umístit	k5eAaPmNgInS	umístit
na	na	k7c6	na
obálce	obálka	k1gFnSc6	obálka
katalogu	katalog	k1gInSc2	katalog
<g/>
;	;	kIx,	;
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Připomenu	připomenout	k5eAaPmIp1nS	připomenout
jenom	jenom	k9	jenom
odmítnutí	odmítnutí	k1gNnSc1	odmítnutí
zápůjčky	zápůjčka	k1gFnSc2	zápůjčka
Kupkových	Kupkových	k2eAgInPc2d1	Kupkových
obrazů	obraz	k1gInPc2	obraz
pařížskému	pařížský	k2eAgInSc3d1	pařížský
Centre	centr	k1gInSc5	centr
Pompidou	Pompida	k1gFnSc7	Pompida
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
chtělo	chtít	k5eAaImAgNnS	chtít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
výstavě	výstava	k1gFnSc6	výstava
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
abstrakce	abstrakce	k1gFnSc2	abstrakce
Kupkovi	Kupka	k1gMnSc3	Kupka
přidělit	přidělit	k5eAaPmF	přidělit
stěžejní	stěžejní	k2eAgFnSc4d1	stěžejní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Kupkův	Kupkův	k2eAgInSc4d1	Kupkův
obraz	obraz	k1gInSc4	obraz
Fuga	fuga	k1gFnSc1	fuga
měl	mít	k5eAaImAgInS	mít
dokonce	dokonce	k9	dokonce
být	být	k5eAaImF	být
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
na	na	k7c6	na
obalu	obal	k1gInSc6	obal
katalogu	katalog	k1gInSc2	katalog
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgMnS	být
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
Knížákově	Knížákův	k2eAgFnSc6d1	Knížákova
zcela	zcela	k6eAd1	zcela
nepochopitelné	pochopitelný	k2eNgFnSc3d1	nepochopitelná
zarputilosti	zarputilost	k1gFnSc3	zarputilost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Kolíbal	kolíbat	k5eAaImAgMnS	kolíbat
o	o	k7c6	o
situaci	situace	k1gFnSc6	situace
s	s	k7c7	s
dílem	dílo	k1gNnSc7	dílo
Františka	František	k1gMnSc2	František
Kupky	Kupka	k1gMnSc2	Kupka
hovoří	hovořit	k5eAaImIp3nS	hovořit
zcela	zcela	k6eAd1	zcela
jinak	jinak	k6eAd1	jinak
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
nějakou	nějaký	k3yIgFnSc4	nějaký
Fugu	fuga	k1gFnSc4	fuga
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
o	o	k7c4	o
Dvoubarevnou	dvoubarevný	k2eAgFnSc4d1	dvoubarevná
fugu	fuga	k1gFnSc4	fuga
čili	čili	k8xC	čili
Amorfu	Amorf	k1gMnSc3	Amorf
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
nazývá	nazývat	k5eAaImIp3nS	nazývat
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
zapůjčení	zapůjčení	k1gNnSc1	zapůjčení
požádalo	požádat	k5eAaPmAgNnS	požádat
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
žádné	žádný	k3yNgNnSc1	žádný
pařížské	pařížský	k2eAgNnSc1d1	pařížské
Centre	centr	k1gInSc5	centr
Pompidou	Pompida	k1gFnSc7	Pompida
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
Musée	Musée	k1gInSc1	Musée
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Orsay	Orsaa	k1gMnSc2	Orsaa
-	-	kIx~	-
konkrétně	konkrétně	k6eAd1	konkrétně
jeho	jeho	k3xOp3gMnSc1	jeho
ředitel	ředitel	k1gMnSc1	ředitel
Serge	Sergus	k1gMnSc5	Sergus
Lemoine	Lemoin	k1gMnSc5	Lemoin
<g/>
.	.	kIx.	.
</s>
<s>
Znal	znát	k5eAaImAgMnS	znát
jsem	být	k5eAaImIp1nS	být
ho	on	k3xPp3gMnSc4	on
dobře	dobře	k6eAd1	dobře
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
ředitelem	ředitel	k1gMnSc7	ředitel
Muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Grenoblu	Grenoble	k1gInSc6	Grenoble
<g/>
...	...	k?	...
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
panu	pan	k1gMnSc3	pan
Lemoinovi	Lemoinův	k2eAgMnPc1d1	Lemoinův
náhradou	náhrada	k1gFnSc7	náhrada
jiný	jiný	k2eAgInSc1d1	jiný
Kupkův	Kupkův	k2eAgInSc1d1	Kupkův
velmi	velmi	k6eAd1	velmi
důležitý	důležitý	k2eAgInSc1d1	důležitý
obraz	obraz	k1gInSc1	obraz
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Klávesy	kláves	k1gInPc1	kláves
piana	piano	k1gNnSc2	piano
-	-	kIx~	-
Jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zapůjčování	zapůjčování	k1gNnSc1	zapůjčování
<g/>
:	:	kIx,	:
o	o	k7c6	o
zapůjčování	zapůjčování	k1gNnSc6	zapůjčování
děl	dělo	k1gNnPc2	dělo
ze	z	k7c2	z
sbírek	sbírka	k1gFnPc2	sbírka
Stanislav	Stanislav	k1gMnSc1	Stanislav
Kolíbal	kolíbat	k5eAaImAgMnS	kolíbat
dále	daleko	k6eAd2	daleko
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Právě	právě	k9	právě
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
dnech	den	k1gInPc6	den
visí	viset	k5eAaImIp3nS	viset
jiný	jiný	k2eAgInSc4d1	jiný
Kupkův	Kupkův	k2eAgInSc4d1	Kupkův
obraz	obraz	k1gInSc4	obraz
(	(	kIx(	(
<g/>
Stroje	stroj	k1gInSc2	stroj
-	-	kIx~	-
půjčený	půjčený	k2eAgMnSc1d1	půjčený
z	z	k7c2	z
instalace	instalace	k1gFnSc2	instalace
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
v	v	k7c6	v
Palazzo	Palazza	k1gFnSc5	Palazza
Ducale	Ducala	k1gFnSc6	Ducala
v	v	k7c6	v
Janově	Janov	k1gInSc6	Janov
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Celník	celník	k1gMnSc1	celník
Rousseau	Rousseau	k1gMnSc1	Rousseau
<g/>
:	:	kIx,	:
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
portrét	portrét	k1gInSc1	portrét
-	-	kIx~	-
krajina	krajina	k1gFnSc1	krajina
v	v	k7c6	v
pařížském	pařížský	k2eAgInSc6d1	pařížský
Grand	grand	k1gMnSc1	grand
Palais	Palais	k1gInSc1	Palais
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
svou	svůj	k3xOyFgFnSc4	svůj
politiku	politika	k1gFnSc4	politika
zapůjčování	zapůjčování	k1gNnSc2	zapůjčování
děl	dělo	k1gNnPc2	dělo
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Také	také	k9	také
jsem	být	k5eAaImIp1nS	být
zavedl	zavést	k5eAaPmAgMnS	zavést
přísný	přísný	k2eAgInSc4d1	přísný
režim	režim	k1gInSc4	režim
půjčování	půjčování	k1gNnSc2	půjčování
děl	dít	k5eAaBmAgMnS	dít
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
komunistů	komunista	k1gMnPc2	komunista
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vůbec	vůbec	k9	vůbec
měla	mít	k5eAaImAgFnS	mít
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
nějaký	nějaký	k3yIgInSc4	nějaký
kontakt	kontakt	k1gInSc4	kontakt
se	s	k7c7	s
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
udělala	udělat	k5eAaPmAgFnS	udělat
výpůjčovna	výpůjčovna	k1gFnSc1	výpůjčovna
<g/>
.	.	kIx.	.
</s>
<s>
My	my	k3xPp1nPc1	my
ale	ale	k9	ale
chceme	chtít	k5eAaImIp1nP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
lidé	člověk	k1gMnPc1	člověk
jezdili	jezdit	k5eAaImAgMnP	jezdit
do	do	k7c2	do
naší	náš	k3xOp1gFnSc2	náš
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
abychom	aby	kYmCp1nP	aby
ty	ten	k3xDgFnPc4	ten
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
věci	věc	k1gFnPc4	věc
posílali	posílat	k5eAaImAgMnP	posílat
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
Rafani	Rafaň	k1gFnSc6	Rafaň
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
Veletržního	veletržní	k2eAgInSc2d1	veletržní
paláce	palác	k1gInSc2	palác
společně	společně	k6eAd1	společně
a	a	k8xC	a
veřejně	veřejně	k6eAd1	veřejně
vykálela	vykálet	k5eAaPmAgFnS	vykálet
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
v	v	k7c6	v
expozici	expozice	k1gFnSc6	expozice
českého	český	k2eAgNnSc2d1	české
a	a	k8xC	a
slovenského	slovenský	k2eAgNnSc2d1	slovenské
umění	umění	k1gNnSc2	umění
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
způsobu	způsob	k1gInSc3	způsob
vedení	vedení	k1gNnSc2	vedení
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
akci	akce	k1gFnSc4	akce
označili	označit	k5eAaPmAgMnP	označit
za	za	k7c4	za
performanci	performance	k1gFnSc4	performance
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
o	o	k7c6	o
nich	on	k3xPp3gInPc6	on
mluví	mluvit	k5eAaImIp3nS	mluvit
stylem	styl	k1gInSc7	styl
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Rafani	Rafan	k1gMnPc1	Rafan
jsou	být	k5eAaImIp3nP	být
největší	veliký	k2eAgFnSc7d3	veliký
konvencí	konvence	k1gFnSc7	konvence
pod	pod	k7c7	pod
sluncem	slunce	k1gNnSc7	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
opatrní	opatrný	k2eAgMnPc1d1	opatrný
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Rok	rok	k1gInSc1	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
333	[number]	k4	333
je	být	k5eAaImIp3nS	být
udělována	udělovat	k5eAaImNgFnS	udělovat
Národní	národní	k2eAgFnSc7d1	národní
galerií	galerie	k1gFnSc7	galerie
mladým	mladý	k2eAgMnPc3d1	mladý
výtvarníkům	výtvarník	k1gMnPc3	výtvarník
<g/>
.	.	kIx.	.
</s>
<s>
Podporuje	podporovat	k5eAaImIp3nS	podporovat
ji	on	k3xPp3gFnSc4	on
nadace	nadace	k1gFnSc1	nadace
ČEZ	ČEZ	kA	ČEZ
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
získala	získat	k5eAaPmAgFnS	získat
skupina	skupina	k1gFnSc1	skupina
Ztohoven	Ztohovna	k1gFnPc2	Ztohovna
<g/>
.	.	kIx.	.
</s>
<s>
Knížák	Knížák	k1gMnSc1	Knížák
ocenil	ocenit	k5eAaPmAgMnS	ocenit
jejich	jejich	k3xOp3gInSc4	jejich
riskantní	riskantní	k2eAgInSc4d1	riskantní
zásah	zásah	k1gInSc4	zásah
do	do	k7c2	do
vysílání	vysílání	k1gNnSc2	vysílání
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nesprávně	správně	k6eNd1	správně
označovaný	označovaný	k2eAgInSc1d1	označovaný
za	za	k7c4	za
hacking	hacking	k1gInSc4	hacking
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgNnSc6	který
nelegálně	legálně	k6eNd1	legálně
vnikli	vniknout	k5eAaPmAgMnP	vniknout
do	do	k7c2	do
soukromého	soukromý	k2eAgInSc2d1	soukromý
objektu	objekt	k1gInSc2	objekt
českých	český	k2eAgFnPc2d1	Česká
radiokomunikací	radiokomunikace	k1gFnPc2	radiokomunikace
<g/>
,	,	kIx,	,
vyšplhali	vyšplhat	k5eAaPmAgMnP	vyšplhat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
vysílačů	vysílač	k1gInPc2	vysílač
a	a	k8xC	a
přes	přes	k7c4	přes
instalovanou	instalovaný	k2eAgFnSc4d1	instalovaná
kameru	kamera	k1gFnSc4	kamera
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
živého	živý	k2eAgNnSc2d1	živé
vysílání	vysílání	k1gNnSc2	vysílání
jejich	jejich	k3xOp3gInSc1	jejich
atomový	atomový	k2eAgInSc1d1	atomový
výbuch	výbuch	k1gInSc1	výbuch
<g/>
.	.	kIx.	.
</s>
<s>
Nesouhlas	nesouhlas	k1gInSc1	nesouhlas
s	s	k7c7	s
Knížákovými	Knížákův	k2eAgInPc7d1	Knížákův
názory	názor	k1gInPc7	názor
a	a	k8xC	a
činy	čin	k1gInPc7	čin
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
skupina	skupina	k1gFnSc1	skupina
Guma	guma	k1gFnSc1	guma
guar	guar	k1gInSc1	guar
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
výstavu	výstava	k1gFnSc4	výstava
v	v	k7c6	v
galerii	galerie	k1gFnSc6	galerie
Vernon	Vernon	k1gInSc4	Vernon
s	s	k7c7	s
názvem	název	k1gInSc7	název
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
<g/>
:	:	kIx,	:
Podivný	podivný	k2eAgMnSc1d1	podivný
Kelt	Kelt	k1gMnSc1	Kelt
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
označil	označit	k5eAaPmAgMnS	označit
celou	celý	k2eAgFnSc4d1	celá
skupinu	skupina	k1gFnSc4	skupina
Guma	guma	k1gFnSc1	guma
guar	guara	k1gFnPc2	guara
a	a	k8xC	a
jejího	její	k3xOp3gMnSc2	její
hlavního	hlavní	k2eAgMnSc2d1	hlavní
představitele	představitel	k1gMnSc2	představitel
Milana	Milan	k1gMnSc2	Milan
Mikuláštíka	Mikuláštík	k1gMnSc2	Mikuláštík
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgMnSc4	svůj
bývalého	bývalý	k2eAgMnSc4d1	bývalý
žáka	žák	k1gMnSc4	žák
<g/>
,	,	kIx,	,
za	za	k7c4	za
snad	snad	k9	snad
ještě	ještě	k6eAd1	ještě
horší	zlý	k2eAgInSc4d2	horší
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
Rafani	Rafan	k1gMnPc1	Rafan
<g/>
,	,	kIx,	,
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dělají	dělat	k5eAaImIp3nP	dělat
všechno	všechen	k3xTgNnSc4	všechen
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nemá	mít	k5eNaImIp3nS	mít
se	s	k7c7	s
mnou	já	k3xPp1nSc7	já
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
,	,	kIx,	,
"	"	kIx"	"
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
tam	tam	k6eAd1	tam
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
NĚ	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
zviditelňování	zviditelňování	k1gNnSc3	zviditelňování
vhodný	vhodný	k2eAgInSc4d1	vhodný
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
teď	teď	k6eAd1	teď
propůjčili	propůjčit	k5eAaPmAgMnP	propůjčit
svůj	svůj	k3xOyFgInSc4	svůj
prostor	prostor	k1gInSc4	prostor
na	na	k7c4	na
Staroměstské	staroměstský	k2eAgNnSc4d1	Staroměstské
radnici	radnice	k1gFnSc4	radnice
Komunistickému	komunistický	k2eAgInSc3d1	komunistický
svazu	svaz	k1gInSc3	svaz
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
bohužel	bohužel	k6eAd1	bohužel
si	se	k3xPyFc3	se
neuvědomují	uvědomovat	k5eNaImIp3nP	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
legitimizují	legitimizovat	k5eAaBmIp3nP	legitimizovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mě	já	k3xPp1nSc4	já
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
plytká	plytký	k2eAgFnSc1d1	plytká
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jde	jít	k5eAaImIp3nS	jít
jenom	jenom	k6eAd1	jenom
a	a	k8xC	a
výhradně	výhradně	k6eAd1	výhradně
o	o	k7c4	o
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
slávu	sláva	k1gFnSc4	sláva
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
podal	podat	k5eAaPmAgMnS	podat
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
akci	akce	k1gFnSc4	akce
trestní	trestní	k2eAgNnSc1d1	trestní
oznámení	oznámení	k1gNnSc1	oznámení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
opět	opět	k6eAd1	opět
začaly	začít	k5eAaPmAgInP	začít
sílit	sílit	k5eAaImF	sílit
hlasy	hlas	k1gInPc1	hlas
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
si	se	k3xPyFc3	se
přejí	přát	k5eAaImIp3nP	přát
Knížákovo	Knížákův	k2eAgNnSc4d1	Knížákovo
odvolání	odvolání	k1gNnSc4	odvolání
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
ředitele	ředitel	k1gMnSc2	ředitel
NG	NG	kA	NG
a	a	k8xC	a
vypsání	vypsání	k1gNnSc4	vypsání
nového	nový	k2eAgInSc2d1	nový
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
konkurzu	konkurz	k1gInSc2	konkurz
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
post	post	k1gInSc4	post
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
představitelem	představitel	k1gMnSc7	představitel
tohoto	tento	k3xDgInSc2	tento
požadavku	požadavek	k1gInSc2	požadavek
byla	být	k5eAaImAgFnS	být
iniciativa	iniciativa	k1gFnSc1	iniciativa
s	s	k7c7	s
názvem	název	k1gInSc7	název
Čas	čas	k1gInSc1	čas
na	na	k7c4	na
změnu	změna	k1gFnSc4	změna
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
výtvarníkem	výtvarník	k1gMnSc7	výtvarník
Jiřím	Jiří	k1gMnSc7	Jiří
Davidem	David	k1gMnSc7	David
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Jiřím	Jiří	k1gMnSc6	Jiří
Davidovi	David	k1gMnSc6	David
se	se	k3xPyFc4	se
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
velmi	velmi	k6eAd1	velmi
stručně	stručně	k6eAd1	stručně
a	a	k8xC	a
s	s	k7c7	s
jistým	jistý	k2eAgInSc7d1	jistý
náznakem	náznak	k1gInSc7	náznak
omluvy	omluva	k1gFnSc2	omluva
-	-	kIx~	-
ať	ať	k8xC	ať
se	se	k3xPyFc4	se
na	na	k7c4	na
mě	já	k3xPp1nSc4	já
nikdo	nikdo	k3yNnSc1	nikdo
nezlobí	zlobit	k5eNaImIp3nS	zlobit
-	-	kIx~	-
jej	on	k3xPp3gNnSc4	on
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k8xC	jako
psychicky	psychicky	k6eAd1	psychicky
narušenou	narušený	k2eAgFnSc4d1	narušená
osobnost	osobnost	k1gFnSc4	osobnost
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
mindrákem	mindrák	k1gInSc7	mindrák
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2009	[number]	k4	2009
ministr	ministr	k1gMnSc1	ministr
kultury	kultura	k1gFnSc2	kultura
Václav	Václav	k1gMnSc1	Václav
Jehlička	jehlička	k1gFnSc1	jehlička
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodlá	hodlat	k5eAaImIp3nS	hodlat
odvolat	odvolat	k5eAaPmF	odvolat
všechny	všechen	k3xTgMnPc4	všechen
ředitele	ředitel	k1gMnPc4	ředitel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
příspěvkových	příspěvkový	k2eAgFnPc2d1	příspěvková
organizací	organizace	k1gFnPc2	organizace
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
a	a	k8xC	a
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
konkurz	konkurz	k1gInSc4	konkurz
na	na	k7c4	na
jejich	jejich	k3xOp3gNnPc4	jejich
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
i	i	k9	i
Milana	Milan	k1gMnSc4	Milan
Knížáka	Knížák	k1gMnSc4	Knížák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
devátým	devátý	k4xOgInSc7	devátý
rokem	rok	k1gInSc7	rok
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Není	být	k5eNaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
nic	nic	k3yNnSc1	nic
osobního	osobní	k2eAgMnSc2d1	osobní
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
tisk	tisk	k1gInSc4	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
mezi	mezi	k7c7	mezi
Milanem	Milan	k1gMnSc7	Milan
Knížákem	Knížák	k1gMnSc7	Knížák
a	a	k8xC	a
Davidem	David	k1gMnSc7	David
Černým	Černý	k1gMnSc7	Černý
další	další	k2eAgInSc4d1	další
konflikt	konflikt	k1gInSc4	konflikt
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Černý	Černý	k1gMnSc1	Černý
Knížáka	Knížák	k1gMnSc2	Knížák
v	v	k7c6	v
lednovém	lednový	k2eAgInSc6d1	lednový
dokumentu	dokument	k1gInSc6	dokument
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
"	"	kIx"	"
<g/>
Cena	cena	k1gFnSc1	cena
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Chalupeckého	Chalupecký	k2eAgMnSc2d1	Chalupecký
<g/>
"	"	kIx"	"
počastoval	počastovat	k5eAaPmAgMnS	počastovat
výrokem	výrok	k1gInSc7	výrok
"	"	kIx"	"
<g/>
kriploidní	kriploidní	k2eAgInSc1d1	kriploidní
čurák	čurák	k1gInSc1	čurák
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Knížák	Knížák	k1gMnSc1	Knížák
následně	následně	k6eAd1	následně
podal	podat	k5eAaPmAgMnS	podat
na	na	k7c4	na
Černého	Černého	k2eAgMnSc4d1	Černého
i	i	k8xC	i
Českou	český	k2eAgFnSc4d1	Česká
televizi	televize	k1gFnSc4	televize
trestní	trestní	k2eAgFnSc4d1	trestní
oznámení	oznámení	k1gNnSc4	oznámení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Knížáka	Knížák	k1gMnSc2	Knížák
ho	on	k3xPp3gMnSc4	on
tím	ten	k3xDgNnSc7	ten
chtěl	chtít	k5eAaImAgMnS	chtít
Černý	Černý	k1gMnSc1	Černý
úmyslně	úmyslně	k6eAd1	úmyslně
znevážit	znevážit	k5eAaPmF	znevážit
<g/>
,	,	kIx,	,
a	a	k8xC	a
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
tak	tak	k6eAd1	tak
do	do	k7c2	do
jeho	on	k3xPp3gNnSc2	on
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
čest	čest	k1gFnSc4	čest
a	a	k8xC	a
důstojnost	důstojnost	k1gFnSc4	důstojnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
přesunula	přesunout	k5eAaPmAgFnS	přesunout
monumentální	monumentální	k2eAgFnPc4d1	monumentální
sochy	socha	k1gFnPc4	socha
umístěné	umístěný	k2eAgFnPc4d1	umístěná
před	před	k7c7	před
budovou	budova	k1gFnSc7	budova
Veletržního	veletržní	k2eAgInSc2d1	veletržní
Paláce	palác	k1gInSc2	palác
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
objemného	objemný	k2eAgNnSc2d1	objemné
zábradlí	zábradlí	k1gNnSc2	zábradlí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
limitovalo	limitovat	k5eAaBmAgNnS	limitovat
pohyb	pohyb	k1gInSc4	pohyb
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Zábradlí	zábradlí	k1gNnSc1	zábradlí
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
kvůli	kvůli	k7c3	kvůli
slepcům	slepec	k1gMnPc3	slepec
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
praví	pravit	k5eAaImIp3nS	pravit
vyhláška	vyhláška	k1gFnSc1	vyhláška
398	[number]	k4	398
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
se	s	k7c7	s
stěhováním	stěhování	k1gNnSc7	stěhování
svých	svůj	k3xOyFgNnPc2	svůj
děl	dělo	k1gNnPc2	dělo
nesouhlasil	souhlasit	k5eNaImAgInS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
Jiřího	Jiří	k1gMnSc2	Jiří
Koláře	Kolář	k1gMnSc2	Kolář
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
na	na	k7c6	na
V.	V.	kA	V.
mezinárodním	mezinárodnět	k5eAaPmIp1nS	mezinárodnět
Triennale	Triennale	k1gFnSc4	Triennale
kresby	kresba	k1gFnSc2	kresba
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
Wroclav	Wroclav	k1gFnSc1	Wroclav
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Medaile	medaile	k1gFnSc2	medaile
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
1	[number]	k4	1
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
za	za	k7c4	za
pedagogickou	pedagogický	k2eAgFnSc4d1	pedagogická
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
Historická	historický	k2eAgFnSc1d1	historická
pečeť	pečeť	k1gFnSc4	pečeť
města	město	k1gNnSc2	město
Plzně	Plzeň	k1gFnSc2	Plzeň
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
Cena	cena	k1gFnSc1	cena
roku	rok	k1gInSc2	rok
za	za	k7c4	za
celoživotní	celoživotní	k2eAgFnSc4d1	celoživotní
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ARTeon	ARTeon	k1gInSc1	ARTeon
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
Poznan	Poznan	k1gInSc1	Poznan
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Udělení	udělení	k1gNnSc1	udělení
ceny	cena	k1gFnSc2	cena
Nadace	nadace	k1gFnSc2	nadace
Universitas	Universitas	k1gMnSc1	Universitas
Masarykiana	Masarykiana	k1gFnSc1	Masarykiana
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Vynikající	vynikající	k2eAgInSc1d1	vynikající
výrobek	výrobek	k1gInSc1	výrobek
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
kolekce	kolekce	k1gFnSc2	kolekce
klik	klika	k1gFnPc2	klika
New	New	k1gFnPc2	New
Generation	Generation	k1gInSc1	Generation
<g/>
,	,	kIx,	,
Design	design	k1gInSc1	design
<g />
.	.	kIx.	.
</s>
<s>
centrum	centrum	k1gNnSc1	centrum
ČR	ČR	kA	ČR
Medaile	medaile	k1gFnSc2	medaile
Za	za	k7c2	za
zásluhy	zásluha	k1gFnSc2	zásluha
I.	I.	kA	I.
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
University	universita	k1gFnSc2	universita
of	of	k?	of
Kentucky	Kentucka	k1gFnSc2	Kentucka
<g/>
,	,	kIx,	,
Lexington	Lexington	k1gInSc1	Lexington
(	(	kIx(	(
<g/>
přednášel	přednášet	k5eAaImAgInS	přednášet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
KY	KY	kA	KY
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
USA	USA	kA	USA
UCLA	UCLA	kA	UCLA
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
(	(	kIx(	(
<g/>
přednášel	přednášet	k5eAaImAgMnS	přednášet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
CA	ca	kA	ca
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
USA	USA	kA	USA
HfBK	HfBK	k1gFnSc1	HfBK
<g />
.	.	kIx.	.
</s>
<s>
Hamburg	Hamburg	k1gInSc1	Hamburg
(	(	kIx(	(
<g/>
přednášel	přednášet	k5eAaImAgInS	přednášet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Profesor	profesor	k1gMnSc1	profesor
International	International	k1gMnSc2	International
Sommerakademie	Sommerakademie	k1gFnSc2	Sommerakademie
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
Rektor	rektor	k1gMnSc1	rektor
Akademie	akademie	k1gFnSc2	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g />
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
Intermediálního	intermediální	k2eAgInSc2d1	intermediální
ateliéru	ateliér	k1gInSc2	ateliér
AVU	AVU	kA	AVU
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1990	[number]	k4	1990
až	až	k9	až
dosud	dosud	k6eAd1	dosud
Profesor	profesor	k1gMnSc1	profesor
Sommeracademie	Sommeracademie	k1gFnSc2	Sommeracademie
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
Berlin	berlina	k1gFnPc2	berlina
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Profesor	profesor	k1gMnSc1	profesor
Summerakademie	Summerakademie	k1gFnSc1	Summerakademie
Gomera	Gomera	k1gFnSc1	Gomera
<g/>
,	,	kIx,	,
Kanárské	kanárský	k2eAgInPc4d1	kanárský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
1995,1996	[number]	k4	1995,1996
<g/>
,1997	,1997	k4	,1997
Profesor	profesor	k1gMnSc1	profesor
Ecal	Ecal	k1gMnSc1	Ecal
école	école	k6eAd1	école
cantonale	cantonale	k6eAd1	cantonale
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
art	art	k?	art
de	de	k?	de
Lausanne	Lausanne	k1gNnPc2	Lausanne
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gInSc4	Knížák
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
na	na	k7c6	na
doméně	doména	k1gFnSc6	doména
milanknizak	milanknizak	k1gInSc1	milanknizak
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
v	v	k7c6	v
IS	IS	kA	IS
abART	abART	k?	abART
Rozhovor	rozhovor	k1gInSc1	rozhovor
se	s	k7c7	s
čtenáři	čtenář	k1gMnPc7	čtenář
časopisu	časopis	k1gInSc2	časopis
Reflexu	reflex	k1gInSc2	reflex
Knížák	Knížák	k1gMnSc1	Knížák
<g />
.	.	kIx.	.
</s>
<s>
za	za	k7c4	za
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
NG	NG	kA	NG
počáteční	počáteční	k2eAgFnSc2d1	počáteční
naděje	naděje	k1gFnSc2	naděje
nenaplnil	naplnit	k5eNaPmAgMnS	naplnit
<g/>
,	,	kIx,	,
ČTK	ČTK	kA	ČTK
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
ve	v	k7c6	v
Výstavní	výstavní	k2eAgFnSc6d1	výstavní
síni	síň	k1gFnSc6	síň
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Škola	škola	k1gFnSc1	škola
Intermediální	intermediální	k2eAgFnSc2d1	intermediální
tvorby	tvorba	k1gFnSc2	tvorba
Milana	Milan	k1gMnSc2	Milan
Knížáka	Knížák	k1gMnSc2	Knížák
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Akademii	akademie	k1gFnSc6	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
ve	v	k7c6	v
videoreportážích	videoreportáž	k1gFnPc6	videoreportáž
na	na	k7c4	na
Artyčok	artyčok	k1gInSc4	artyčok
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
TV	TV	kA	TV
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
ČT	ČT	kA	ČT
Hydepark	Hydepark	k1gInSc1	Hydepark
<g/>
,	,	kIx,	,
6.2	[number]	k4	6.2
<g/>
.2012	.2012	k4	.2012
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
MOMA	MOMA	kA	MOMA
<g/>
,	,	kIx,	,
NY	NY	kA	NY
GINGER	GINGER	kA	GINGER
ISLAND	Island	k1gInSc4	Island
PROJECT	PROJECT	kA	PROJECT
<g/>
,	,	kIx,	,
NY	NY	kA	NY
<g/>
,	,	kIx,	,
USA	USA	kA	USA
2011	[number]	k4	2011
rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
kuratorem	kurator	k1gInSc7	kurator
/	/	kIx~	/
<g/>
Liutauras	Liutauras	k1gInSc1	Liutauras
Psibilskis	Psibilskis	k1gFnSc2	Psibilskis
<g/>
/	/	kIx~	/
o	o	k7c6	o
projektu	projekt	k1gInSc6	projekt
GI	GI	kA	GI
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
performa	performum	k1gNnSc2	performum
11	[number]	k4	11
<g/>
,	,	kIx,	,
festival	festival	k1gInSc1	festival
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
o	o	k7c6	o
projektu	projekt	k1gInSc6	projekt
GI	GI	kA	GI
fotografie	fotografia	k1gFnPc1	fotografia
z	z	k7c2	z
výstavy	výstava	k1gFnSc2	výstava
Ginger	Ginger	k1gInSc1	Ginger
Island	Island	k1gInSc1	Island
project	project	k1gInSc1	project
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
NY	NY	kA	NY
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
Adventure	Adventur	k1gMnSc5	Adventur
Ginger	Gingra	k1gFnPc2	Gingra
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Maciunas	Maciunas	k1gInSc1	Maciunas
Foundation	Foundation	k1gInSc1	Foundation
Fluxus	Fluxus	k1gInSc1	Fluxus
50	[number]	k4	50
<g/>
,	,	kIx,	,
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
KUAD	KUAD	kA	KUAD
galerie	galerie	k1gFnPc1	galerie
</s>
