<p>
<s>
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
překladech	překlad	k1gInPc6	překlad
Stará	starý	k2eAgFnSc1d1	stará
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgFnSc4d2	delší
a	a	k8xC	a
starší	starý	k2eAgFnSc4d2	starší
část	část	k1gFnSc4	část
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
o	o	k7c4	o
sbírku	sbírka	k1gFnSc4	sbírka
posvátných	posvátný	k2eAgFnPc2d1	posvátná
židovských	židovský	k2eAgFnPc2d1	židovská
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
přejalo	přejmout	k5eAaPmAgNnS	přejmout
z	z	k7c2	z
židovství	židovství	k1gNnSc2	židovství
vzniklé	vzniklý	k2eAgNnSc4d1	vzniklé
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tuto	tento	k3xDgFnSc4	tento
sbírku	sbírka	k1gFnSc4	sbírka
užívají	užívat	k5eAaImIp3nP	užívat
jak	jak	k8xS	jak
židé	žid	k1gMnPc1	žid
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
různé	různý	k2eAgInPc1d1	různý
proudy	proud	k1gInPc1	proud
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
nepanuje	panovat	k5eNaImIp3nS	panovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
otázkách	otázka	k1gFnPc6	otázka
shoda	shoda	k1gFnSc1	shoda
–	–	k?	–
ani	ani	k8xC	ani
v	v	k7c6	v
tak	tak	k6eAd1	tak
základních	základní	k2eAgFnPc6d1	základní
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
rozsah	rozsah	k1gInSc4	rozsah
či	či	k8xC	či
název	název	k1gInSc4	název
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Otázka	otázka	k1gFnSc1	otázka
kánonu	kánon	k1gInSc2	kánon
<g/>
,	,	kIx,	,
označení	označení	k1gNnSc2	označení
a	a	k8xC	a
dělení	dělení	k1gNnSc2	dělení
==	==	k?	==
</s>
</p>
<p>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
biblického	biblický	k2eAgInSc2d1	biblický
kánonu	kánon	k1gInSc2	kánon
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
Starý	starý	k2eAgInSc4d1	starý
zákon	zákon	k1gInSc4	zákon
užívají	užívat	k5eAaImIp3nP	užívat
<g/>
,	,	kIx,	,
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
uznávají	uznávat	k5eAaImIp3nP	uznávat
39	[number]	k4	39
samostatných	samostatný	k2eAgFnPc2d1	samostatná
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
24	[number]	k4	24
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
nerozdělují	rozdělovat	k5eNaImIp3nP	rozdělovat
některé	některý	k3yIgFnPc4	některý
knihy	kniha	k1gFnPc4	kniha
na	na	k7c4	na
více	hodně	k6eAd2	hodně
částí	část	k1gFnPc2	část
jako	jako	k8xC	jako
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protestanti	protestant	k1gMnPc1	protestant
přejali	přejmout	k5eAaPmAgMnP	přejmout
židovský	židovský	k2eAgInSc4d1	židovský
kánon	kánon	k1gInSc4	kánon
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
ustálil	ustálit	k5eAaPmAgMnS	ustálit
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
uznává	uznávat	k5eAaImIp3nS	uznávat
kánon	kánon	k1gInSc1	kánon
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
i	i	k9	i
o	o	k7c4	o
deuterokanonické	deuterokanonický	k2eAgFnPc4d1	deuterokanonická
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
alexandrijské	alexandrijský	k2eAgFnSc2d1	Alexandrijská
židovské	židovský	k2eAgFnSc2d1	židovská
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
a	a	k8xC	a
považuje	považovat	k5eAaImIp3nS	považovat
tak	tak	k6eAd1	tak
za	za	k7c4	za
závazných	závazný	k2eAgNnPc2d1	závazné
celkem	celkem	k6eAd1	celkem
46	[number]	k4	46
spisů	spis	k1gInPc2	spis
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
knih	kniha	k1gFnPc2	kniha
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
kánoně	kánon	k1gInSc6	kánon
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
ortodoxní	ortodoxní	k2eAgFnSc1d1	ortodoxní
církev	církev	k1gFnSc1	církev
–	–	k?	–
54	[number]	k4	54
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Označení	označení	k1gNnSc4	označení
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
je	být	k5eAaImIp3nS	být
křesťanské	křesťanský	k2eAgNnSc4d1	křesťanské
a	a	k8xC	a
"	"	kIx"	"
<g/>
starý	starý	k2eAgMnSc1d1	starý
<g/>
"	"	kIx"	"
zde	zde	k6eAd1	zde
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
či	či	k8xC	či
jako	jako	k8xC	jako
doplnění	doplnění	k1gNnSc4	doplnění
k	k	k7c3	k
Novému	nový	k2eAgInSc3d1	nový
zákonu	zákon	k1gInSc3	zákon
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgFnSc2	druhý
části	část	k1gFnSc2	část
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pojmenování	pojmenování	k1gNnSc1	pojmenování
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
problematické	problematický	k2eAgNnSc1d1	problematické
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
svádí	svádět	k5eAaImIp3nS	svádět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
bylo	být	k5eAaImAgNnS	být
chápáno	chápat	k5eAaImNgNnS	chápat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
zastaralý	zastaralý	k2eAgInSc1d1	zastaralý
zákon	zákon	k1gInSc1	zákon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
i	i	k9	i
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
autoři	autor	k1gMnPc1	autor
navrhují	navrhovat	k5eAaImIp3nP	navrhovat
používat	používat	k5eAaImF	používat
buď	buď	k8xC	buď
židovské	židovský	k2eAgNnSc4d1	Židovské
označení	označení	k1gNnSc4	označení
Tanach	Tanacha	k1gFnPc2	Tanacha
(	(	kIx(	(
<g/>
akronym	akronym	k1gInSc4	akronym
složený	složený	k2eAgInSc4d1	složený
z	z	k7c2	z
Tóra	tóra	k1gFnSc1	tóra
<g/>
,	,	kIx,	,
Nevi	Nevi	k?	Nevi
<g/>
'	'	kIx"	'
<g/>
im	im	k?	im
(	(	kIx(	(
<g/>
proroci	prorok	k1gMnPc1	prorok
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ktuvim	Ktuvim	k1gInSc4	Ktuvim
(	(	kIx(	(
<g/>
spisy	spis	k1gInPc4	spis
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
však	však	k9	však
nepostihuje	postihovat	k5eNaImIp3nS	postihovat
deuterokanonické	deuterokanonický	k2eAgFnPc4d1	deuterokanonická
knihy	kniha	k1gFnPc4	kniha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
např.	např.	kA	např.
První	první	k4xOgInSc4	první
zákon	zákon	k1gInSc4	zákon
(	(	kIx(	(
<g/>
a	a	k8xC	a
pro	pro	k7c4	pro
Nový	nový	k2eAgInSc4d1	nový
zákon	zákon	k1gInSc4	zákon
pak	pak	k6eAd1	pak
užívat	užívat	k5eAaImF	užívat
Druhý	druhý	k4xOgInSc1	druhý
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
klasického	klasický	k2eAgNnSc2d1	klasické
židovského	židovský	k2eAgNnSc2d1	Židovské
dělení	dělení	k1gNnSc2	dělení
má	mít	k5eAaImIp3nS	mít
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc1	část
<g/>
:	:	kIx,	:
Tóra	tóra	k1gFnSc1	tóra
<g/>
,	,	kIx,	,
Proroci	prorok	k1gMnPc1	prorok
a	a	k8xC	a
Spisy	spis	k1gInPc7	spis
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
jej	on	k3xPp3gMnSc4	on
obvykle	obvykle	k6eAd1	obvykle
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
pět	pět	k4xCc4	pět
knih	kniha	k1gFnPc2	kniha
Mojžíšových	Mojžíšová	k1gFnPc2	Mojžíšová
(	(	kIx(	(
<g/>
Pentateuch	pentateuch	k1gInSc1	pentateuch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historické	historický	k2eAgInPc4d1	historický
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
mudroslovné	mudroslovný	k2eAgFnPc4d1	mudroslovná
knihy	kniha	k1gFnPc4	kniha
a	a	k8xC	a
prorocké	prorocký	k2eAgFnPc4d1	prorocká
knihy	kniha	k1gFnPc4	kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
charakter	charakter	k1gInSc1	charakter
==	==	k?	==
</s>
</p>
<p>
<s>
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
vznikal	vznikat	k5eAaImAgInS	vznikat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
přibližně	přibližně	k6eAd1	přibližně
1000	[number]	k4	1000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
texty	text	k1gInPc1	text
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
předmonarchického	předmonarchický	k2eAgNnSc2d1	předmonarchický
izraelského	izraelský	k2eAgNnSc2d1	izraelské
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
snad	snad	k9	snad
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1000	[number]	k4	1000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
i	i	k9	i
teorie	teorie	k1gFnPc1	teorie
opírající	opírající	k2eAgFnPc1d1	opírající
se	se	k3xPyFc4	se
o	o	k7c4	o
literární	literární	k2eAgFnSc4d1	literární
skladbu	skladba	k1gFnSc4	skladba
a	a	k8xC	a
historické	historický	k2eAgFnPc1d1	historická
skutečnosti	skutečnost	k1gFnPc1	skutečnost
první	první	k4xOgFnSc2	první
části	část	k1gFnSc2	část
knihy	kniha	k1gFnSc2	kniha
Genesis	Genesis	k1gFnSc1	Genesis
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
posunují	posunovat	k5eAaImIp3nP	posunovat
stáří	stáří	k1gNnSc4	stáří
těchto	tento	k3xDgInPc2	tento
textů	text	k1gInPc2	text
před	před	k7c4	před
rok	rok	k1gInSc4	rok
1500	[number]	k4	1500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvrdící	tvrdící	k2eAgInPc1d1	tvrdící
<g/>
,	,	kIx,	,
že	že	k8xS	že
zápisy	zápis	k1gInPc1	zápis
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
zvykem	zvyk	k1gInSc7	zvyk
a	a	k8xC	a
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
důležité	důležitý	k2eAgInPc4d1	důležitý
<g/>
,	,	kIx,	,
pořizovali	pořizovat	k5eAaImAgMnP	pořizovat
již	již	k6eAd1	již
praotcové	praotec	k1gMnPc1	praotec
(	(	kIx(	(
<g/>
doba	doba	k1gFnSc1	doba
patriarchů	patriarcha	k1gMnPc2	patriarcha
bývá	bývat	k5eAaImIp3nS	bývat
umísťována	umísťovat	k5eAaImNgFnS	umísťovat
do	do	k7c2	do
období	období	k1gNnSc2	období
kolem	kolem	k7c2	kolem
r.	r.	kA	r.
2000	[number]	k4	2000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
historici	historik	k1gMnPc1	historik
ji	on	k3xPp3gFnSc4	on
ale	ale	k8xC	ale
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pravděpodobnější	pravděpodobný	k2eAgFnSc1d2	pravděpodobnější
alespoň	alespoň	k9	alespoň
pro	pro	k7c4	pro
skupiny	skupina	k1gFnPc4	skupina
kolem	kolem	k7c2	kolem
Abraháma	Abrahám	k1gMnSc2	Abrahám
a	a	k8xC	a
Izáka	Izák	k1gMnSc2	Izák
<g/>
)	)	kIx)	)
nejspíše	nejspíše	k9	nejspíše
na	na	k7c4	na
hliněné	hliněný	k2eAgFnPc4d1	hliněná
tabulky	tabulka	k1gFnPc4	tabulka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
předáváním	předávání	k1gNnSc7	předávání
dostaly	dostat	k5eAaPmAgFnP	dostat
až	až	k9	až
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Mojžíšových	Mojžíšová	k1gFnPc2	Mojžíšová
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
udělal	udělat	k5eAaPmAgMnS	udělat
soubor	soubor	k1gInSc4	soubor
<g/>
,	,	kIx,	,
opatřil	opatřit	k5eAaPmAgMnS	opatřit
vysvětlivkami	vysvětlivka	k1gFnPc7	vysvětlivka
<g/>
,	,	kIx,	,
převedl	převést	k5eAaPmAgMnS	převést
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
místní	místní	k2eAgInPc4d1	místní
názvy	název	k1gInPc4	název
a	a	k8xC	a
Boží	boží	k2eAgNnPc4d1	boží
jména	jméno	k1gNnPc4	jméno
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
obecně	obecně	k6eAd1	obecně
známé	známý	k2eAgFnPc4d1	známá
a	a	k8xC	a
doplnil	doplnit	k5eAaPmAgMnS	doplnit
tento	tento	k3xDgInSc4	tento
soupis	soupis	k1gInSc4	soupis
vyprávěním	vyprávění	k1gNnSc7	vyprávění
o	o	k7c6	o
Josefovi	Josef	k1gMnSc6	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Nejmladší	mladý	k2eAgInPc1d3	nejmladší
texty	text	k1gInPc1	text
byly	být	k5eAaImAgInP	být
sepsány	sepsat	k5eAaPmNgInP	sepsat
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
staletích	staletí	k1gNnPc6	staletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
začíná	začínat	k5eAaImIp3nS	začínat
vyprávěním	vyprávění	k1gNnSc7	vyprávění
o	o	k7c4	o
stvoření	stvoření	k1gNnSc4	stvoření
světa	svět	k1gInSc2	svět
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
sledováním	sledování	k1gNnSc7	sledování
dějin	dějiny	k1gFnPc2	dějiny
židovských	židovský	k2eAgMnPc2d1	židovský
praotců	praotec	k1gMnPc2	praotec
a	a	k8xC	a
židovského	židovský	k2eAgInSc2d1	židovský
národa	národ	k1gInSc2	národ
jako	jako	k8xS	jako
zkušenosti	zkušenost	k1gFnSc2	zkušenost
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
Hospodinem	Hospodin	k1gMnSc7	Hospodin
až	až	k6eAd1	až
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
(	(	kIx(	(
<g/>
makabejské	makabejský	k2eAgNnSc1d1	Makabejské
povstání	povstání	k1gNnSc1	povstání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obsažené	obsažený	k2eAgFnPc1d1	obsažená
knihy	kniha	k1gFnPc1	kniha
jsou	být	k5eAaImIp3nP	být
nejrůznějšího	různý	k2eAgInSc2d3	nejrůznější
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
výpravné	výpravný	k2eAgFnSc2d1	výpravná
<g/>
,	,	kIx,	,
historické	historický	k2eAgFnSc2d1	historická
<g/>
,	,	kIx,	,
sbírky	sbírka	k1gFnPc4	sbírka
výroků	výrok	k1gInPc2	výrok
<g/>
,	,	kIx,	,
vidění	vidění	k1gNnSc2	vidění
<g/>
,	,	kIx,	,
úvahy	úvaha	k1gFnSc2	úvaha
<g/>
,	,	kIx,	,
modlitby	modlitba	k1gFnSc2	modlitba
<g/>
,	,	kIx,	,
písně	píseň	k1gFnSc2	píseň
apod.	apod.	kA	apod.
Je	být	k5eAaImIp3nS	být
napsán	napsat	k5eAaBmNgInS	napsat
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnSc2	některý
části	část	k1gFnSc2	část
spisů	spis	k1gInPc2	spis
aramejsky	aramejsky	k6eAd1	aramejsky
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
deuterokanonických	deuterokanonický	k2eAgFnPc2d1	deuterokanonická
knih	kniha	k1gFnPc2	kniha
řecky	řecky	k6eAd1	řecky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
správnému	správný	k2eAgNnSc3d1	správné
porozumění	porozumění	k1gNnSc3	porozumění
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
záměr	záměr	k1gInSc4	záměr
textu	text	k1gInSc2	text
a	a	k8xC	a
okolnosti	okolnost	k1gFnSc2	okolnost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
text	text	k1gInSc1	text
vznikal	vznikat	k5eAaImAgInS	vznikat
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
tvůrců	tvůrce	k1gMnPc2	tvůrce
totiž	totiž	k9	totiž
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
nebylo	být	k5eNaImAgNnS	být
objektivně	objektivně	k6eAd1	objektivně
vylíčit	vylíčit	k5eAaPmF	vylíčit
historické	historický	k2eAgFnPc4d1	historická
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
tyto	tento	k3xDgFnPc4	tento
události	událost	k1gFnPc4	událost
interpretovat	interpretovat	k5eAaBmF	interpretovat
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c4	po
uchopení	uchopení	k1gNnSc4	uchopení
záměru	záměr	k1gInSc2	záměr
textu	text	k1gInSc2	text
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
přiblížit	přiblížit	k5eAaPmF	přiblížit
skutečnému	skutečný	k2eAgNnSc3d1	skutečné
průběhu	průběh	k1gInSc6	průběh
historické	historický	k2eAgFnPc4d1	historická
události	událost	k1gFnPc4	událost
<g/>
.	.	kIx.	.
</s>
<s>
Problematika	problematika	k1gFnSc1	problematika
historicity	historicita	k1gFnSc2	historicita
událostí	událost	k1gFnPc2	událost
je	být	k5eAaImIp3nS	být
ztížena	ztížit	k5eAaPmNgFnS	ztížit
navíc	navíc	k6eAd1	navíc
ještě	ještě	k6eAd1	ještě
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
je	být	k5eAaImIp3nS	být
mnohdy	mnohdy	k6eAd1	mnohdy
jediným	jediný	k2eAgInSc7d1	jediný
pramenem	pramen	k1gInSc7	pramen
židovských	židovský	k2eAgFnPc2d1	židovská
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
<g/>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
knihy	kniha	k1gFnPc1	kniha
také	také	k9	také
původně	původně	k6eAd1	původně
nebyly	být	k5eNaImAgFnP	být
určeny	určit	k5eAaPmNgFnP	určit
k	k	k7c3	k
literárnímu	literární	k2eAgNnSc3d1	literární
užití	užití	k1gNnSc3	užití
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
určeny	určit	k5eAaPmNgFnP	určit
k	k	k7c3	k
určité	určitý	k2eAgFnSc3d1	určitá
příležitosti	příležitost	k1gFnSc3	příležitost
(	(	kIx(	(
<g/>
liturgie	liturgie	k1gFnSc1	liturgie
<g/>
,	,	kIx,	,
běžný	běžný	k2eAgInSc1d1	běžný
život	život	k1gInSc1	život
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
procházely	procházet	k5eAaImAgFnP	procházet
mnohdy	mnohdy	k6eAd1	mnohdy
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
vývojem	vývoj	k1gInSc7	vývoj
a	a	k8xC	a
v	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
byly	být	k5eAaImAgFnP	být
předávány	předávat	k5eAaImNgFnP	předávat
ústně	ústně	k6eAd1	ústně
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
doby	doba	k1gFnSc2	doba
však	však	k9	však
vznikala	vznikat	k5eAaImAgFnS	vznikat
potřeba	potřeba	k6eAd1	potřeba
tyto	tento	k3xDgInPc4	tento
texty	text	k1gInPc4	text
zapisovat	zapisovat	k5eAaImF	zapisovat
–	–	k?	–
zmínky	zmínka	k1gFnSc2	zmínka
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
samotný	samotný	k2eAgInSc1d1	samotný
text	text	k1gInSc1	text
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Převedením	převedení	k1gNnSc7	převedení
ústní	ústní	k2eAgFnSc2d1	ústní
tradice	tradice	k1gFnSc2	tradice
do	do	k7c2	do
písemné	písemný	k2eAgFnSc2d1	písemná
podoby	podoba	k1gFnSc2	podoba
se	se	k3xPyFc4	se
však	však	k9	však
většinou	většinou	k6eAd1	většinou
modifikoval	modifikovat	k5eAaBmAgMnS	modifikovat
žánr	žánr	k1gInSc4	žánr
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
původní	původní	k2eAgInSc1d1	původní
text	text	k1gInSc1	text
byl	být	k5eAaImAgInS	být
vytržen	vytrhnout	k5eAaPmNgInS	vytrhnout
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
kontextu	kontext	k1gInSc2	kontext
a	a	k8xC	a
vložen	vložit	k5eAaPmNgMnS	vložit
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
nového	nový	k2eAgNnSc2d1	nové
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byl	být	k5eAaImAgInS	být
text	text	k1gInSc1	text
také	také	k9	také
ucelován	ucelován	k2eAgInSc1d1	ucelován
–	–	k?	–
tato	tento	k3xDgNnPc4	tento
redakce	redakce	k1gFnPc4	redakce
<g/>
,	,	kIx,	,
probíhající	probíhající	k2eAgInSc4d1	probíhající
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
pol.	pol.	k?	pol.
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
textu	text	k1gInSc2	text
dala	dát	k5eAaPmAgFnS	dát
jeho	jeho	k3xOp3gFnSc4	jeho
konečnou	konečný	k2eAgFnSc4d1	konečná
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
snahu	snaha	k1gFnSc4	snaha
text	text	k1gInSc1	text
sjednotit	sjednotit	k5eAaPmF	sjednotit
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
lze	lze	k6eAd1	lze
často	často	k6eAd1	často
rozlišit	rozlišit	k5eAaPmF	rozlišit
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
původní	původní	k2eAgFnPc4d1	původní
části	část	k1gFnPc4	část
a	a	k8xC	a
švy	šev	k1gInPc4	šev
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
RENDTORFF	RENDTORFF	kA	RENDTORFF
<g/>
,	,	kIx,	,
Rolf	Rolf	k1gInSc1	Rolf
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejská	hebrejský	k2eAgFnSc1d1	hebrejská
bible	bible	k1gFnSc1	bible
a	a	k8xC	a
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
:	:	kIx,	:
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
starozákonní	starozákonní	k2eAgFnSc2d1	starozákonní
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
376	[number]	k4	376
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
634	[number]	k4	634
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SEGERT	SEGERT	kA	SEGERT
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
<g/>
.	.	kIx.	.
</s>
<s>
Biblický	biblický	k2eAgInSc1d1	biblický
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ústřední	ústřední	k2eAgNnSc1d1	ústřední
církevní	církevní	k2eAgNnSc1d1	církevní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Biblický	biblický	k2eAgInSc1d1	biblický
kánon	kánon	k1gInSc1	kánon
</s>
</p>
<p>
<s>
Tanach	Tanach	k1gMnSc1	Tanach
</s>
</p>
<p>
<s>
Septuaginta	Septuaginta	k1gFnSc1	Septuaginta
</s>
</p>
<p>
<s>
Biblická	biblický	k2eAgFnSc1d1	biblická
kritika	kritika	k1gFnSc1	kritika
</s>
</p>
<p>
<s>
Literární	literární	k2eAgInPc4d1	literární
žánry	žánr	k1gInPc4	žánr
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Starý	starý	k2eAgInSc4d1	starý
zákon	zákon	k1gInSc4	zákon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Starý	Starý	k1gMnSc1	Starý
zákon	zákon	k1gInSc1	zákon
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
