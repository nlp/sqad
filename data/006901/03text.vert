<s>
Dotek	dotek	k1gInSc1	dotek
Medúzy	Medúza	k1gFnSc2	Medúza
je	být	k5eAaImIp3nS	být
britsko-francouzský	britskorancouzský	k2eAgInSc1d1	britsko-francouzský
barevný	barevný	k2eAgInSc1d1	barevný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc4	scénář
filmu	film	k1gInSc2	film
napsal	napsat	k5eAaPmAgMnS	napsat
John	John	k1gMnSc1	John
Briley	Brilea	k1gMnSc2	Brilea
podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
Dotek	dotek	k1gInSc1	dotek
Medúzy	Medúza	k1gFnSc2	Medúza
spisovatele	spisovatel	k1gMnSc2	spisovatel
Petera	Peter	k1gMnSc2	Peter
Van	van	k1gInSc4	van
Greenwaye	Greenwaye	k1gNnSc2	Greenwaye
<g/>
.	.	kIx.	.
</s>
<s>
Filmovou	filmový	k2eAgFnSc4d1	filmová
adaptaci	adaptace	k1gFnSc4	adaptace
režíroval	režírovat	k5eAaImAgMnS	režírovat
Jack	Jack	k1gMnSc1	Jack
Gold	Gold	k1gMnSc1	Gold
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
detektiv	detektiv	k1gMnSc1	detektiv
Brunel	Brunel	k1gMnSc1	Brunel
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Brunel	Brunel	k1gInSc1	Brunel
je	být	k5eAaImIp3nS	být
přidělen	přidělit	k5eAaPmNgInS	přidělit
k	k	k7c3	k
vyšetřování	vyšetřování	k1gNnSc3	vyšetřování
vraždy	vražda	k1gFnSc2	vražda
britského	britský	k2eAgMnSc2d1	britský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Johna	John	k1gMnSc2	John
Morlara	Morlar	k1gMnSc2	Morlar
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ohledání	ohledání	k1gNnSc2	ohledání
místa	místo	k1gNnSc2	místo
činu	čin	k1gInSc2	čin
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
oběť	oběť	k1gFnSc1	oběť
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
naživu	naživu	k6eAd1	naživu
i	i	k9	i
přes	přes	k7c4	přes
vážná	vážný	k2eAgNnPc4d1	vážné
zranění	zranění	k1gNnPc4	zranění
a	a	k8xC	a
dostanou	dostat	k5eAaPmIp3nP	dostat
ho	on	k3xPp3gNnSc4	on
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pracovně	pracovna	k1gFnSc6	pracovna
spisovatele	spisovatel	k1gMnSc2	spisovatel
visí	viset	k5eAaImIp3nS	viset
obraz	obraz	k1gInSc4	obraz
bájné	bájný	k2eAgFnSc2d1	bájná
Medúzy	Medúza	k1gFnSc2	Medúza
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Morlarova	Morlarův	k2eAgInSc2d1	Morlarův
deníku	deník	k1gInSc2	deník
a	a	k8xC	a
doktorky	doktorka	k1gFnSc2	doktorka
Zonfeldové	Zonfeldová	k1gFnSc2	Zonfeldová
<g/>
,	,	kIx,	,
psychiatričky	psychiatrička	k1gFnSc2	psychiatrička
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
spisovatel	spisovatel	k1gMnSc1	spisovatel
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
<g/>
,	,	kIx,	,
Brunel	Brunel	k1gInSc1	Brunel
zrekonstruuje	zrekonstruovat	k5eAaPmIp3nS	zrekonstruovat
Morlarovu	Morlarův	k2eAgFnSc4d1	Morlarův
minulost	minulost	k1gFnSc4	minulost
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
plná	plný	k2eAgFnSc1d1	plná
nevysvětlitelných	vysvětlitelný	k2eNgFnPc2d1	nevysvětlitelná
katastrof	katastrofa	k1gFnPc2	katastrofa
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
tragické	tragický	k2eAgFnSc2d1	tragická
smrti	smrt	k1gFnSc2	smrt
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
neměl	mít	k5eNaImAgMnS	mít
rád	rád	k2eAgMnSc1d1	rád
nebo	nebo	k8xC	nebo
ho	on	k3xPp3gMnSc4	on
urazili	urazit	k5eAaPmAgMnP	urazit
<g/>
.	.	kIx.	.
</s>
<s>
Morlar	Morlar	k1gMnSc1	Morlar
je	být	k5eAaImIp3nS	být
psychik	psychika	k1gFnPc2	psychika
s	s	k7c7	s
mocnými	mocný	k2eAgFnPc7d1	mocná
telekinetickými	telekinetický	k2eAgFnPc7d1	telekinetický
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Zhnusený	zhnusený	k2eAgInSc1d1	zhnusený
světem	svět	k1gInSc7	svět
(	(	kIx(	(
<g/>
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Nightmare	Nightmar	k1gMnSc5	Nightmar
Movies	Moviesa	k1gFnPc2	Moviesa
Kim	Kim	k1gMnSc1	Kim
Newman	Newman	k1gMnSc1	Newman
popíše	popsat	k5eAaPmIp3nS	popsat
Morlarův	Morlarův	k2eAgInSc4d1	Morlarův
dialog	dialog	k1gInSc4	dialog
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
misantropický	misantropický	k2eAgInSc1d1	misantropický
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Morlar	Morlar	k1gMnSc1	Morlar
způsobil	způsobit	k5eAaPmAgMnS	způsobit
dvě	dva	k4xCgNnPc4	dva
současná	současný	k2eAgNnPc4d1	současné
neštěstí	neštěstí	k1gNnPc4	neštěstí
<g/>
:	:	kIx,	:
leteckou	letecký	k2eAgFnSc4d1	letecká
nehodu	nehoda	k1gFnSc4	nehoda
a	a	k8xC	a
ztrátu	ztráta	k1gFnSc4	ztráta
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
s	s	k7c7	s
posádkou	posádka	k1gFnSc7	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
postele	postel	k1gFnSc2	postel
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
svými	svůj	k3xOyFgFnPc7	svůj
schopnostmi	schopnost	k1gFnPc7	schopnost
způsobí	způsobit	k5eAaPmIp3nS	způsobit
pád	pád	k1gInSc1	pád
katedrály	katedrála	k1gFnSc2	katedrála
na	na	k7c4	na
"	"	kIx"	"
<g/>
hanebné	hanebný	k2eAgFnPc4d1	hanebná
hlavy	hlava	k1gFnPc4	hlava
<g/>
"	"	kIx"	"
VIP	VIP	kA	VIP
kongregace	kongregace	k1gFnSc2	kongregace
vzdávající	vzdávající	k2eAgFnSc2d1	vzdávající
díky	díky	k7c3	díky
za	za	k7c4	za
uchování	uchování	k1gNnSc4	uchování
stavby	stavba	k1gFnSc2	stavba
a	a	k8xC	a
udrží	udržet	k5eAaPmIp3nS	udržet
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
naživu	naživu	k6eAd1	naživu
jenom	jenom	k6eAd1	jenom
mocí	moc	k1gFnSc7	moc
své	svůj	k3xOyFgFnSc2	svůj
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Brunel	Brunel	k1gInSc1	Brunel
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
Morlara	Morlara	k1gFnSc1	Morlara
zabít	zabít	k5eAaPmF	zabít
odpojením	odpojení	k1gNnSc7	odpojení
lékařských	lékařský	k2eAgInPc2d1	lékařský
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
selže	selhat	k5eAaPmIp3nS	selhat
<g/>
.	.	kIx.	.
</s>
<s>
Morlar	Morlar	k1gInSc1	Morlar
napíše	napsat	k5eAaPmIp3nS	napsat
na	na	k7c4	na
papír	papír	k1gInSc1	papír
svůj	svůj	k3xOyFgInSc4	svůj
další	další	k2eAgInSc4d1	další
cíl	cíl	k1gInSc4	cíl
-	-	kIx~	-
jadernou	jaderný	k2eAgFnSc4d1	jaderná
elektrárnu	elektrárna	k1gFnSc4	elektrárna
ve	v	k7c6	v
Windscale	Windscala	k1gFnSc6	Windscala
<g/>
.	.	kIx.	.
</s>
<s>
Jack	Jack	k1gMnSc1	Jack
Gold	Gold	k1gMnSc1	Gold
film	film	k1gInSc4	film
pojal	pojmout	k5eAaPmAgMnS	pojmout
psychologicky	psychologicky	k6eAd1	psychologicky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepříliš	příliš	k6eNd1	příliš
zdařile	zdařile	k6eAd1	zdařile
<g/>
.	.	kIx.	.
</s>
<s>
Nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vytvořit	vytvořit	k5eAaPmF	vytvořit
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
napětí	napětí	k1gNnSc4	napětí
<g/>
,	,	kIx,	,
gradaci	gradace	k1gFnSc4	gradace
děje	děj	k1gInSc2	děj
a	a	k8xC	a
děj	děj	k1gInSc1	děj
občas	občas	k6eAd1	občas
postrádá	postrádat	k5eAaImIp3nS	postrádat
souvislost	souvislost	k1gFnSc4	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
108	[number]	k4	108
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
inspirován	inspirován	k2eAgInSc4d1	inspirován
Van	van	k1gInSc4	van
Greenawayovou	Greenawayový	k2eAgFnSc7d1	Greenawayový
knihou	kniha	k1gFnSc7	kniha
<g/>
,	,	kIx,	,
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
několika	několik	k4yIc6	několik
detailech	detail	k1gInPc6	detail
<g/>
:	:	kIx,	:
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
detektiv	detektiv	k1gMnSc1	detektiv
není	být	k5eNaImIp3nS	být
Francouz	Francouz	k1gMnSc1	Francouz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Angličan	Angličan	k1gMnSc1	Angličan
inspektor	inspektor	k1gMnSc1	inspektor
Cherry	Cherra	k1gMnSc2	Cherra
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
knihách	kniha	k1gFnPc6	kniha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
je	být	k5eAaImIp3nS	být
Zonfeld	Zonfeld	k1gMnSc1	Zonfeld
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přežil	přežít	k5eAaPmAgMnS	přežít
holokaust	holokaust	k1gInSc4	holokaust
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
zážitky	zážitek	k1gInPc4	zážitek
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
přispějí	přispět	k5eAaPmIp3nP	přispět
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
sebevraždě	sebevražda	k1gFnSc3	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
knihy	kniha	k1gFnSc2	kniha
Morlarova	Morlarův	k2eAgFnSc1d1	Morlarův
ruka	ruka	k1gFnSc1	ruka
napíše	napsat	k5eAaPmIp3nS	napsat
Holy	hola	k1gFnPc4	hola
Loch	loch	k1gInSc4	loch
<g/>
,	,	kIx,	,
americkou	americký	k2eAgFnSc4d1	americká
základnu	základna	k1gFnSc4	základna
jaderných	jaderný	k2eAgFnPc2d1	jaderná
ponorek	ponorka	k1gFnPc2	ponorka
místo	místo	k7c2	místo
Windscale	Windscala	k1gFnSc6	Windscala
</s>
