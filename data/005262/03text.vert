<s>
Marie	Marie	k1gFnSc1	Marie
Tereza	Tereza	k1gFnSc1	Tereza
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1638	[number]	k4	1638
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1683	[number]	k4	1683
<g/>
,	,	kIx,	,
Versailles	Versailles	k1gFnSc1	Versailles
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
španělská	španělský	k2eAgFnSc1d1	španělská
infantka	infantka	k1gFnSc1	infantka
a	a	k8xC	a
manželka	manželka	k1gFnSc1	manželka
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
španělského	španělský	k2eAgMnSc4d1	španělský
krále	král	k1gMnSc4	král
Filipa	Filip	k1gMnSc2	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
s	s	k7c7	s
Izabelou	Izabela	k1gFnSc7	Izabela
Bourbonskou	bourbonský	k2eAgFnSc7d1	Bourbonská
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
manželky	manželka	k1gFnPc4	manželka
Markéty	Markéta	k1gFnSc2	Markéta
Štýrské	štýrský	k2eAgFnSc2d1	štýrská
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc4	dva
prarodiče	prarodič	k1gMnPc4	prarodič
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
náleželi	náležet	k5eAaImAgMnP	náležet
k	k	k7c3	k
Habsburské	habsburský	k2eAgFnSc3d1	habsburská
dynastii	dynastie	k1gFnSc3	dynastie
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
dědové	děd	k1gMnPc1	děd
byli	být	k5eAaImAgMnP	být
bratři	bratr	k1gMnPc1	bratr
<g/>
,	,	kIx,	,
císařové	císař	k1gMnPc1	císař
Karel	Karla	k1gFnPc2	Karla
V.	V.	kA	V.
a	a	k8xC	a
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Izabela	Izabela	k1gFnSc1	Izabela
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
hugenotského	hugenotský	k2eAgMnSc2d1	hugenotský
krále	král	k1gMnSc2	král
Francie	Francie	k1gFnSc2	Francie
Jindřicha	Jindřich	k1gMnSc2	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Bourbonského	bourbonský	k2eAgMnSc4d1	bourbonský
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Medicejskou	Medicejský	k2eAgFnSc7d1	Medicejská
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
bratrem	bratr	k1gMnSc7	bratr
byl	být	k5eAaImAgMnS	být
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
manžela	manžel	k1gMnSc2	manžel
Marie	Maria	k1gFnSc2	Maria
Terezy	Tereza	k1gFnSc2	Tereza
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Tereza	Tereza	k1gFnSc1	Tereza
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
narodila	narodit	k5eAaPmAgFnS	narodit
již	již	k6eAd1	již
jako	jako	k8xS	jako
osmý	osmý	k4xOgMnSc1	osmý
potomek	potomek	k1gMnSc1	potomek
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
manželství	manželství	k1gNnSc2	manželství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měla	mít	k5eAaImAgFnS	mít
pouze	pouze	k6eAd1	pouze
jediného	jediný	k2eAgMnSc4d1	jediný
bratra	bratr	k1gMnSc4	bratr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
dožil	dožít	k5eAaPmAgMnS	dožít
"	"	kIx"	"
<g/>
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Baltazarově	Baltazarův	k2eAgInSc6d1	Baltazarův
(	(	kIx(	(
<g/>
1629	[number]	k4	1629
<g/>
–	–	k?	–
<g/>
1646	[number]	k4	1646
<g/>
)	)	kIx)	)
smrti	smrt	k1gFnSc3	smrt
se	se	k3xPyFc4	se
otec	otec	k1gMnSc1	otec
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1649	[number]	k4	1649
oženil	oženit	k5eAaPmAgMnS	oženit
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1644	[number]	k4	1644
<g/>
)	)	kIx)	)
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Annou	Anna	k1gFnSc7	Anna
Habsburskou	habsburský	k2eAgFnSc7d1	habsburská
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
o	o	k7c4	o
Markétu	Markéta	k1gFnSc4	Markéta
(	(	kIx(	(
<g/>
1651	[number]	k4	1651
<g/>
–	–	k?	–
<g/>
1673	[number]	k4	1673
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
pozdější	pozdní	k2eAgFnSc4d2	pozdější
první	první	k4xOgFnSc4	první
manželku	manželka	k1gFnSc4	manželka
císaře	císař	k1gMnSc2	císař
Leopolda	Leopold	k1gMnSc2	Leopold
I.	I.	kA	I.
<g/>
,	,	kIx,	,
a	a	k8xC	a
Karla	Karel	k1gMnSc4	Karel
(	(	kIx(	(
<g/>
1661	[number]	k4	1661
<g/>
–	–	k?	–
<g/>
1700	[number]	k4	1700
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
smrtí	smrt	k1gFnSc7	smrt
skončila	skončit	k5eAaPmAgFnS	skončit
vláda	vláda	k1gFnSc1	vláda
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
dynastie	dynastie	k1gFnSc2	dynastie
nad	nad	k7c7	nad
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1660	[number]	k4	1660
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
svého	svůj	k3xOyFgMnSc2	svůj
bratrance	bratranec	k1gMnSc2	bratranec
1	[number]	k4	1
<g/>
.	.	kIx.	.
st.	st.	kA	st.
(	(	kIx(	(
<g/>
rodiče	rodič	k1gMnSc2	rodič
manželů	manžel	k1gMnPc2	manžel
byli	být	k5eAaImAgMnP	být
sourozenci	sourozenec	k1gMnPc1	sourozenec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
(	(	kIx(	(
<g/>
1661	[number]	k4	1661
<g/>
–	–	k?	–
<g/>
1711	[number]	k4	1711
<g/>
)	)	kIx)	)
Anna	Anna	k1gFnSc1	Anna
Alžběta	Alžběta	k1gFnSc1	Alžběta
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
/	/	kIx~	/
<g/>
†	†	k?	†
1662	[number]	k4	1662
<g/>
)	)	kIx)	)
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
/	/	kIx~	/
<g/>
†	†	k?	†
1664	[number]	k4	1664
<g/>
)	)	kIx)	)
Marie	Marie	k1gFnSc1	Marie
Tereza	Tereza	k1gFnSc1	Tereza
(	(	kIx(	(
<g/>
1667	[number]	k4	1667
<g/>
–	–	k?	–
<g/>
1672	[number]	k4	1672
<g/>
)	)	kIx)	)
Filip	Filip	k1gMnSc1	Filip
(	(	kIx(	(
<g/>
1668	[number]	k4	1668
<g/>
–	–	k?	–
<g/>
1671	[number]	k4	1671
<g/>
)	)	kIx)	)
Ludvík	Ludvík	k1gMnSc1	Ludvík
František	František	k1gMnSc1	František
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
/	/	kIx~	/
<g/>
†	†	k?	†
1672	[number]	k4	1672
<g/>
)	)	kIx)	)
HAMANNOVÁ	HAMANNOVÁ	kA	HAMANNOVÁ
<g/>
,	,	kIx,	,
Brigitte	Brigitte	k1gFnSc1	Brigitte
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Životopisná	životopisný	k2eAgFnSc1d1	životopisná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Brána	brána	k1gFnSc1	brána
;	;	kIx,	;
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
408	[number]	k4	408
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85946	[number]	k4	85946
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Marie	Maria	k1gFnSc2	Maria
Tereza	Tereza	k1gFnSc1	Tereza
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Ženy	žena	k1gFnSc2	žena
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
