<s>
Vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
horního	horní	k2eAgInSc2d1	horní
pruhu	pruh	k1gInSc2	pruh
bílého	bílý	k2eAgInSc2d1	bílý
a	a	k8xC	a
dolního	dolní	k2eAgInSc2d1	dolní
pruhu	pruh	k1gInSc2	pruh
červeného	červené	k1gNnSc2	červené
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
je	být	k5eAaImIp3nS	být
vsunut	vsunut	k2eAgInSc1d1	vsunut
žerďový	žerďový	k2eAgInSc1d1	žerďový
modrý	modrý	k2eAgInSc1d1	modrý
klín	klín	k1gInSc1	klín
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
délky	délka	k1gFnSc2	délka
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
