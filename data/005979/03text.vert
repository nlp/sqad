<s>
Ge'ez	Ge'ez	k1gInSc1	Ge'ez
neboli	neboli	k8xC	neboli
klasická	klasický	k2eAgFnSc1d1	klasická
etiopština	etiopština	k1gFnSc1	etiopština
je	být	k5eAaImIp3nS	být
semitský	semitský	k2eAgInSc4d1	semitský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
odštěpený	odštěpený	k2eAgMnSc1d1	odštěpený
od	od	k7c2	od
jihoarabštiny	jihoarabština	k1gFnSc2	jihoarabština
<g/>
.	.	kIx.	.
</s>
<s>
Hovořilo	hovořit	k5eAaImAgNnS	hovořit
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
běžně	běžně	k6eAd1	běžně
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
až	až	k9	až
do	do	k7c2	do
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
jazyk	jazyk	k1gInSc4	jazyk
nahrazen	nahrazen	k2eAgInSc4d1	nahrazen
amharštinou	amharština	k1gFnSc7	amharština
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
liturgii	liturgie	k1gFnSc6	liturgie
etiopské	etiopský	k2eAgFnSc2d1	etiopská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
se	se	k3xPyFc4	se
etiopským	etiopský	k2eAgNnSc7d1	etiopské
písmem	písmo	k1gNnSc7	písmo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
jazyce	jazyk	k1gInSc6	jazyk
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
rané	raný	k2eAgFnSc2d1	raná
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
židovské	židovský	k2eAgFnSc2d1	židovská
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
spisů	spis	k1gInPc2	spis
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jmenovat	jmenovat	k5eAaBmF	jmenovat
1	[number]	k4	1
<g/>
.	.	kIx.	.
knihu	kniha	k1gFnSc4	kniha
Henochovu	Henochův	k2eAgFnSc4d1	Henochova
či	či	k8xC	či
Knihu	kniha	k1gFnSc4	kniha
jubileí	jubileum	k1gNnPc2	jubileum
<g/>
.	.	kIx.	.
</s>
