<s>
Přítomnost	přítomnost	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
časovém	časový	k2eAgNnSc6d1
určení	určení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Přítomnost	přítomnost	k1gFnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
„	„	k?
<g/>
při	při	k7c6
čem	co	k3yRnSc6
<g/>
“	“	k?
každý	každý	k3xTgMnSc1
člověk	člověk	k1gMnSc1
právě	právě	k9
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
složitý	složitý	k2eAgInSc1d1
fenomén	fenomén	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
zkoumá	zkoumat	k5eAaImIp3nS
fenomenologie	fenomenologie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
E.	E.	kA
Husserla	Husserla	k1gFnSc1
tvoří	tvořit	k5eAaImIp3nS
vědomí	vědomí	k1gNnSc4
přítomnosti	přítomnost	k1gFnSc2
časový	časový	k2eAgInSc1d1
dvorec	dvorec	k1gInSc1
<g/>
,	,	kIx,
totiž	totiž	k9
bezprostřední	bezprostřední	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
</s>
<s>
retence	retence	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
francouzského	francouzský	k2eAgMnSc2d1
retenir	retenir	k1gMnSc1
<g/>
,	,	kIx,
podržovat	podržovat	k5eAaImF
v	v	k7c6
paměti	paměť	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čili	čili	k8xC
podržování	podržování	k1gNnSc1
právě	právě	k6eAd1
uplynulého	uplynulý	k2eAgInSc2d1
v	v	k7c6
širším	široký	k2eAgNnSc6d2
vědomí	vědomí	k1gNnSc6
přítomnosti	přítomnost	k1gFnSc2
<g/>
;	;	kIx,
</s>
<s>
prezentace	prezentace	k1gFnSc1
<g/>
,	,	kIx,
čili	čít	k5eAaImAgMnP
toho	ten	k3xDgMnSc4
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
přítomnému	přítomný	k1gMnSc3
vědomí	vědomí	k1gNnSc1
právě	právě	k9
představuje	představovat	k5eAaImIp3nS
<g/>
,	,	kIx,
</s>
<s>
a	a	k8xC
protence	protence	k1gFnSc1
<g/>
,	,	kIx,
čili	čili	k8xC
předjímání	předjímání	k1gNnSc1
bezprostředně	bezprostředně	k6eAd1
očekávaného	očekávaný	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
vnímáme	vnímat	k5eAaImIp1nP
jako	jako	k9
přítomné	přítomný	k1gMnPc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
živý	živý	k2eAgInSc4d1
okraj	okraj	k1gInSc4
paměti	paměť	k1gFnSc2
s	s	k7c7
nádechem	nádech	k1gInSc7
očekávání	očekávání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
Alfred	Alfred	k1gMnSc1
North	North	k1gMnSc1
Whitehead	Whitehead	k1gInSc4
</s>
<s>
Jan	Jan	k1gMnSc1
Patočka	Patočka	k1gMnSc1
mluví	mluvit	k5eAaImIp3nS
o	o	k7c6
"	"	kIx"
<g/>
jednotné	jednotný	k2eAgFnSc6d1
funkci	funkce	k1gFnSc6
očekávání	očekávání	k1gNnSc2
<g/>
,	,	kIx,
zpozorování	zpozorování	k1gNnSc2
a	a	k8xC
podržování	podržování	k1gNnSc2
jsoucna	jsoucno	k1gNnSc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
</s>
<s>
Fenomén	fenomén	k1gInSc1
přítomnosti	přítomnost	k1gFnSc2
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
každý	každý	k3xTgMnSc1
člověk	člověk	k1gMnSc1
neustále	neustále	k6eAd1
žije	žít	k5eAaImIp3nS
<g/>
,	,	kIx,
neboť	neboť	k8xC
ji	on	k3xPp3gFnSc4
svým	svůj	k3xOyFgNnSc7
vědomím	vědomí	k1gNnSc7
určuje	určovat	k5eAaImIp3nS
<g/>
,	,	kIx,
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
jen	jen	k9
bezrozměrný	bezrozměrný	k2eAgInSc4d1
bod	bod	k1gInSc4
na	na	k7c6
časové	časový	k2eAgFnSc6d1
ose	osa	k1gFnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
„	„	k?
<g/>
v	v	k7c6
ní	on	k3xPp3gFnSc6
<g/>
“	“	k?
probíhá	probíhat	k5eAaImIp3nS
každé	každý	k3xTgNnSc1
vnímání	vnímání	k1gNnSc1
i	i	k8xC
jednání	jednání	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
nějakou	nějaký	k3yIgFnSc4
dobu	doba	k1gFnSc4
trvá	trvat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
tento	tento	k3xDgInSc4
fenomén	fenomén	k1gInSc4
upozornil	upozornit	k5eAaPmAgMnS
už	už	k9
Augustin	Augustin	k1gMnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
analýze	analýza	k1gFnSc6
času	čas	k1gInSc2
<g/>
:	:	kIx,
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
recituje	recitovat	k5eAaImIp3nS
báseň	báseň	k1gFnSc4
<g/>
,	,	kIx,
ji	on	k3xPp3gFnSc4
musí	muset	k5eAaImIp3nS
celou	celá	k1gFnSc4
umět	umět	k5eAaImF
čili	čili	k8xC
mít	mít	k5eAaImF
v	v	k7c6
paměti	paměť	k1gFnSc6
<g/>
,	,	kIx,
recituje	recitovat	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
však	však	k9
verš	verš	k1gInSc1
za	za	k7c7
veršem	verš	k1gInSc7
a	a	k8xC
slovo	slovo	k1gNnSc1
za	za	k7c7
slovem	slovo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posluchač	posluchač	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
ji	on	k3xPp3gFnSc4
nezná	znát	k5eNaImIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
si	se	k3xPyFc3
skládá	skládat	k5eAaImIp3nS
slova	slovo	k1gNnPc4
a	a	k8xC
verše	verš	k1gInPc4
<g/>
,	,	kIx,
až	až	k6eAd1
má	mít	k5eAaImIp3nS
nakonec	nakonec	k6eAd1
stejné	stejný	k2eAgNnSc4d1
vědění	vědění	k1gNnSc4
jako	jako	k8xC,k8xS
přednášející	přednášející	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Něco	něco	k3yInSc1
podobného	podobný	k2eAgNnSc2d1
se	se	k3xPyFc4
ovšem	ovšem	k9
děje	dít	k5eAaImIp3nS
už	už	k6eAd1
na	na	k7c6
úrovni	úroveň	k1gFnSc6
věty	věta	k1gFnSc2
a	a	k8xC
slova	slovo	k1gNnSc2
<g/>
:	:	kIx,
jednotlivé	jednotlivý	k2eAgInPc1d1
hlásky	hlásek	k1gInPc1
netrvají	trvat	k5eNaImIp3nP
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
okamžitě	okamžitě	k6eAd1
odezní	odeznět	k5eAaPmIp3nP
a	a	k8xC
jsou	být	k5eAaImIp3nP
nahrazeny	nahradit	k5eAaPmNgFnP
jinými	jiná	k1gFnPc7
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
si	se	k3xPyFc3
je	být	k5eAaImIp3nS
posluchač	posluchač	k1gMnSc1
bez	bez	k7c2
námahy	námaha	k1gFnSc2
skládá	skládat	k5eAaImIp3nS
do	do	k7c2
slov	slovo	k1gNnPc2
a	a	k8xC
vět	věta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Retence	retence	k1gFnPc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
od	od	k7c2
paměti	paměť	k1gFnSc2
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
vzpomínky	vzpomínka	k1gFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
vyvolávat	vyvolávat	k5eAaImF
zvláštním	zvláštní	k2eAgInSc7d1
aktem	akt	k1gInSc7
intence	intence	k1gFnSc2
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
právě	právě	k6eAd1
uplynulé	uplynulý	k2eAgNnSc1d1
má	mít	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
přímo	přímo	k6eAd1
<g/>
;	;	kIx,
jinak	jinak	k6eAd1
by	by	kYmCp3nS
řeči	řeč	k1gFnSc2
nemohl	moct	k5eNaImAgMnS
rozumět	rozumět	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
člověk	člověk	k1gMnSc1
podržuje	podržovat	k5eAaImIp3nS
čili	čili	k8xC
retinuje	retinovat	k5eAaImIp3nS,k5eAaPmIp3nS,k5eAaBmIp3nS
třeba	třeba	k6eAd1
souvislost	souvislost	k1gFnSc4
filmu	film	k1gInSc2
nebo	nebo	k8xC
příběhu	příběh	k1gInSc2
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
začátek	začátek	k1gInSc4
neviděl	vidět	k5eNaImAgMnS
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
souvislosti	souvislost	k1gFnSc6
vyptávat	vyptávat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Protence	Protence	k1gFnSc1
</s>
<s>
V	v	k7c6
této	tento	k3xDgFnSc6
širší	široký	k2eAgFnSc6d2
přítomnosti	přítomnost	k1gFnSc6
je	být	k5eAaImIp3nS
však	však	k9
obsaženo	obsáhnout	k5eAaPmNgNnS
i	i	k8xC
očekávání	očekávání	k1gNnSc2
příštího	příští	k2eAgNnSc2d1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
Husserl	Husserl	k1gInSc1
nazývá	nazývat	k5eAaImIp3nS
protence	protence	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedině	jedině	k6eAd1
tak	tak	k8xS,k8xC
lze	lze	k6eAd1
totiž	totiž	k9
vysvětlit	vysvětlit	k5eAaPmF
podivuhodný	podivuhodný	k2eAgInSc1d1
fenomén	fenomén	k1gInSc1
překvapení	překvapení	k1gNnSc1
<g/>
:	:	kIx,
něco	něco	k3yInSc1
mne	já	k3xPp1nSc4
může	moct	k5eAaImIp3nS
překvapit	překvapit	k5eAaPmF
jen	jen	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
jsem	být	k5eAaImIp1nS
očekával	očekávat	k5eAaImAgMnS
něco	něco	k3yInSc1
jiného	jiný	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
člověka	člověk	k1gMnSc4
dráždí	dráždit	k5eAaImIp3nP
<g/>
,	,	kIx,
když	když	k8xS
poslouchá	poslouchat	k5eAaImIp3nS
pianistu	pianista	k1gMnSc4
začátečníka	začátečník	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
opakovaně	opakovaně	k6eAd1
sahá	sahat	k5eAaImIp3nS
vedle	vedle	k7c2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvůrci	tvůrce	k1gMnPc1
filmů	film	k1gInPc2
dbají	dbát	k5eAaImIp3nP
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
scéna	scéna	k1gFnSc1
před	před	k7c7
nějakým	nějaký	k3yIgInSc7
dramatickým	dramatický	k2eAgInSc7d1
okamžikem	okamžik	k1gInSc7
diváka	divák	k1gMnSc2
spíše	spíše	k9
uklidnila	uklidnit	k5eAaPmAgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
účinek	účinek	k1gInSc1
překvapení	překvapení	k1gNnSc2
byl	být	k5eAaImAgInS
větší	veliký	k2eAgInSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dobře	dobře	k6eAd1
známém	známý	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
člověk	člověk	k1gMnSc1
ví	vědět	k5eAaImIp3nS
<g/>
,	,	kIx,
s	s	k7c7
čím	co	k3yInSc7,k3yRnSc7,k3yQnSc7
může	moct	k5eAaImIp3nS
počítat	počítat	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
směleji	směle	k6eAd2
než	než	k8xS
v	v	k7c6
neznámém	známý	k2eNgMnSc6d1
nebo	nebo	k8xC
neosvětleném	osvětlený	k2eNgMnSc6d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
nemůže	moct	k5eNaImIp3nS
nic	nic	k3yNnSc1
předpokládat	předpokládat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1
vnímané	vnímaný	k2eAgInPc1d1
a	a	k8xC
zakoušené	zakoušený	k2eAgInPc1d1
(	(	kIx(
<g/>
prezentace	prezentace	k1gFnSc1
<g/>
)	)	kIx)
spolu	spolu	k6eAd1
s	s	k7c7
retencí	retence	k1gFnSc7
a	a	k8xC
protencí	protence	k1gFnSc7
tvoří	tvořit	k5eAaImIp3nS
časový	časový	k2eAgInSc1d1
dvorec	dvorec	k1gInSc1
<g/>
,	,	kIx,
zkušenostní	zkušenostní	k2eAgFnSc1d1
přítomnost	přítomnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Že	že	k8xS
není	být	k5eNaImIp3nS
bodová	bodový	k2eAgFnSc1d1
<g/>
,	,	kIx,
dokládá	dokládat	k5eAaImIp3nS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
kdykoli	kdykoli	k6eAd1
potřebujeme	potřebovat	k5eAaImIp1nP
své	svůj	k3xOyFgFnPc4
činnosti	činnost	k1gFnPc4
přesně	přesně	k6eAd1
synchronizovat	synchronizovat	k5eAaBmF
<g/>
,	,	kIx,
musíme	muset	k5eAaImIp1nP
si	se	k3xPyFc3
v	v	k7c6
této	tento	k3xDgFnSc6
přítomnosti	přítomnost	k1gFnSc6
vytvořit	vytvořit	k5eAaPmF
zvláštní	zvláštní	k2eAgInSc4d1
pokud	pokud	k8xS
možno	možno	k6eAd1
bodovou	bodový	k2eAgFnSc4d1
značku	značka	k1gFnSc4
<g/>
,	,	kIx,
například	například	k6eAd1
výstřel	výstřel	k1gInSc1
startéra	startér	k1gMnSc2
v	v	k7c6
běžeckém	běžecký	k2eAgInSc6d1
závodě	závod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
podobným	podobný	k2eAgMnPc3d1
závěrům	závěr	k1gInPc3
došel	dojít	k5eAaPmAgInS
i	i	k9
americký	americký	k2eAgMnSc1d1
psycholog	psycholog	k1gMnSc1
William	William	k1gInSc4
James	James	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
mluvil	mluvit	k5eAaImAgMnS
o	o	k7c6
„	„	k?
<g/>
rozlehlé	rozlehlý	k2eAgFnSc6d1
přítomnosti	přítomnost	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
spacious	spacious	k1gMnSc1
present	present	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1
a	a	k8xC
pozornost	pozornost	k1gFnSc1
</s>
<s>
Retence	retence	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
přítomném	přítomný	k2eAgNnSc6d1
vědomí	vědomí	k1gNnSc6
reprodukují	reprodukovat	k5eAaBmIp3nP
<g/>
,	,	kIx,
vytvářejí	vytvářet	k5eAaImIp3nP
retence	retence	k1gFnPc1
retencí	retence	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
přitom	přitom	k6eAd1
slábnou	slábnout	k5eAaImIp3nP
<g/>
,	,	kIx,
ztrácejí	ztrácet	k5eAaImIp3nP
na	na	k7c6
obsahu	obsah	k1gInSc6
a	a	k8xC
určitosti	určitost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Husserl	Husserl	k1gInSc1
to	ten	k3xDgNnSc4
přirovnává	přirovnávat	k5eAaImIp3nS
k	k	k7c3
ohonu	ohon	k1gInSc3
komety	kometa	k1gFnSc2
a	a	k8xC
mluví	mluvit	k5eAaImIp3nS
o	o	k7c6
„	„	k?
<g/>
ohonu	ohon	k1gInSc6
retencí	retence	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
protence	protence	k1gFnSc1
není	být	k5eNaImIp3nS
časově	časově	k6eAd1
přesně	přesně	k6eAd1
vymezena	vymezit	k5eAaPmNgFnS
<g/>
,	,	kIx,
takže	takže	k8xS
hranice	hranice	k1gFnSc1
přítomného	přítomný	k2eAgNnSc2d1
vědomí	vědomí	k1gNnSc2
nejsou	být	k5eNaImIp3nP
ostré	ostrý	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
je	být	k5eAaImIp3nS
lze	lze	k6eAd1
přibližně	přibližně	k6eAd1
vymezit	vymezit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
psychologických	psychologický	k2eAgInPc6d1
pokusech	pokus	k1gInPc6
se	se	k3xPyFc4
testuje	testovat	k5eAaImIp3nS
tzv.	tzv.	kA
„	„	k?
<g/>
krátkodobá	krátkodobý	k2eAgFnSc1d1
paměť	paměť	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
jakou	jaký	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
člověk	člověk	k1gMnSc1
potřebuje	potřebovat	k5eAaImIp3nS
na	na	k7c4
rozumění	rozumění	k1gNnSc4
řeči	řeč	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
rozsahu	rozsah	k1gInSc6
kolem	kolem	k7c2
deseti	deset	k4xCc2
sekund	sekunda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
schopen	schopen	k2eAgMnSc1d1
vnímat	vnímat	k5eAaImF
film	film	k1gInSc4
nebo	nebo	k8xC
–	–	k?
s	s	k7c7
jistým	jistý	k2eAgNnSc7d1
úsilím	úsilí	k1gNnSc7
–	–	k?
celou	celý	k2eAgFnSc4d1
přednášku	přednáška	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
schopnost	schopnost	k1gFnSc1
pozornosti	pozornost	k1gFnSc2
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
trénovat	trénovat	k5eAaImF
<g/>
,	,	kIx,
vyžaduje	vyžadovat	k5eAaImIp3nS
úsilí	úsilí	k1gNnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
není	být	k5eNaImIp3nS
neomezená	omezený	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závisí	záviset	k5eAaImIp3nS
také	také	k9
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
silně	silně	k6eAd1
člověka	člověk	k1gMnSc4
sledovaný	sledovaný	k2eAgInSc1d1
děj	děj	k1gInSc1
či	či	k8xC
příběh	příběh	k1gInSc1
zajímá	zajímat	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
silně	silně	k6eAd1
mu	on	k3xPp3gMnSc3
na	na	k7c6
něm	on	k3xPp3gInSc6
záleží	záležet	k5eAaImIp3nS
atd.	atd.	kA
Společenský	společenský	k2eAgInSc4d1
provoz	provoz	k1gInSc4
a	a	k8xC
zkušenost	zkušenost	k1gFnSc4
ukázaly	ukázat	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
nárok	nárok	k1gInSc1
na	na	k7c4
pozornost	pozornost	k1gFnSc4
běžného	běžný	k2eAgMnSc2d1
diváka	divák	k1gMnSc2
a	a	k8xC
posluchače	posluchač	k1gMnSc2
se	se	k3xPyFc4
nemá	mít	k5eNaImIp3nS
přepínat	přepínat	k5eAaImF
a	a	k8xC
nemá	mít	k5eNaImIp3nS
překračovat	překračovat	k5eAaImF
jednu	jeden	k4xCgFnSc4
až	až	k9
dvě	dva	k4xCgFnPc4
hodiny	hodina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
trvá	trvat	k5eAaImIp3nS
školní	školní	k2eAgFnSc1d1
hodina	hodina	k1gFnSc1
40	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
koncert	koncert	k1gInSc1
<g/>
,	,	kIx,
fotbalový	fotbalový	k2eAgInSc1d1
zápas	zápas	k1gInSc1
nebo	nebo	k8xC
přednáška	přednáška	k1gFnSc1
zhruba	zhruba	k6eAd1
dvakrát	dvakrát	k6eAd1
tolik	tolik	k6eAd1
a	a	k8xC
za	za	k7c2
mimořádných	mimořádný	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
třeba	třeba	k9
chirurg	chirurg	k1gMnSc1
schopen	schopen	k2eAgMnSc1d1
udržet	udržet	k5eAaPmF
souvislou	souvislý	k2eAgFnSc4d1
pozornost	pozornost	k1gFnSc4
i	i	k8xC
několik	několik	k4yIc4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
však	však	k9
velmi	velmi	k6eAd1
namáhavé	namáhavý	k2eAgNnSc1d1
a	a	k8xC
vyčerpávající	vyčerpávající	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Sokol	Sokol	k1gMnSc1
<g/>
,	,	kIx,
Čas	čas	k1gInSc1
a	a	k8xC
rytmus	rytmus	k1gInSc1
<g/>
.	.	kIx.
↑	↑	k?
Sokol	Sokol	k1gMnSc1
<g/>
,	,	kIx,
Malá	malý	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Bernet	Bernet	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
aj.	aj.	kA
<g/>
,	,	kIx,
Úvod	úvod	k1gInSc1
do	do	k7c2
myšlení	myšlení	k1gNnSc2
Edmunda	Edmund	k1gMnSc2
Husserla	Husserl	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2004	#num#	k4
</s>
<s>
Biemel	Biemel	k1gMnSc1
<g/>
,	,	kIx,
W.	W.	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Heidegger	Heidegger	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1995	#num#	k4
</s>
<s>
Blecha	Blecha	k1gMnSc1
<g/>
,	,	kIx,
I.	I.	kA
<g/>
,	,	kIx,
Husserl	Husserl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
1997	#num#	k4
</s>
<s>
Dasturová	Dasturový	k2eAgFnSc1d1
<g/>
,	,	kIx,
F.	F.	kA
<g/>
,	,	kIx,
Čas	čas	k1gInSc4
a	a	k8xC
druhý	druhý	k4xOgMnSc1
u	u	k7c2
Husserla	Husserlo	k1gNnSc2
a	a	k8xC
Heideggera	Heideggero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1992	#num#	k4
</s>
<s>
Heidegger	Heidegger	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
<g/>
,	,	kIx,
Bytí	bytí	k1gNnSc4
a	a	k8xC
čas	čas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2002	#num#	k4
</s>
<s>
Husserl	Husserl	k1gMnSc1
<g/>
,	,	kIx,
E.	E.	kA
<g/>
,	,	kIx,
Přednášky	přednáška	k1gFnPc1
k	k	k7c3
fenomenologii	fenomenologie	k1gFnSc3
časového	časový	k2eAgNnSc2d1
vědomí	vědomí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1996	#num#	k4
</s>
<s>
Lyotard	Lyotard	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
-	-	kIx~
<g/>
F.	F.	kA
<g/>
,	,	kIx,
Fenomenologie	fenomenologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1995	#num#	k4
</s>
<s>
Patočka	Patočka	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
,	,	kIx,
Tělo	tělo	k1gNnSc1
–	–	k?
společenství	společenství	k1gNnPc2
–	–	k?
jazyk	jazyk	k1gMnSc1
–	–	k?
svět	svět	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1995	#num#	k4
</s>
<s>
Patočka	Patočka	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
,	,	kIx,
Úvod	úvod	k1gInSc1
do	do	k7c2
fenomenologické	fenomenologický	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1993	#num#	k4
</s>
<s>
Sokol	Sokol	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
,	,	kIx,
Malá	malý	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2004	#num#	k4
</s>
<s>
Sokol	Sokol	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
,	,	kIx,
Čas	čas	k1gInSc1
a	a	k8xC
rytmus	rytmus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2004	#num#	k4
</s>
<s>
Eckhart	Eckhart	k1gInSc1
Tolle	Tolle	k1gFnSc2
Moc	moc	k6eAd1
přítomného	přítomný	k2eAgInSc2d1
okamžiku	okamžik	k1gInSc2
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Fenomenologie	fenomenologie	k1gFnSc1
</s>
<s>
Čas	čas	k1gInSc1
(	(	kIx(
<g/>
filosofie	filosofie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Edmund	Edmund	k1gMnSc1
Husserl	Husserl	k1gMnSc1
</s>
<s>
Eckhart	Eckhart	k1gInSc1
Tolle	Tolle	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
přítomnost	přítomnost	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Téma	téma	k1gNnSc1
Přítomnost	přítomnost	k1gFnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Heslo	heslo	k1gNnSc1
Fenomenologie	fenomenologie	k1gFnSc2
vědomí	vědomí	k1gNnSc2
v	v	k7c6
SEP	SEP	kA
-	-	kIx~
en	en	k?
</s>
